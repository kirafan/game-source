﻿using System;
using System.IO;

// Token: 0x02000066 RID: 102
public class CriFsWebInstallRequest : CriFsInstallRequest
{
	// Token: 0x06000337 RID: 823 RVA: 0x00007E68 File Offset: 0x00006268
	public CriFsWebInstallRequest(string srcPath, string dstPath, CriFsRequest.DoneDelegate doneDelegate)
	{
		base.sourcePath = srcPath;
		base.destinationPath = dstPath;
		base.doneDelegate = doneDelegate;
		base.progress = 0f;
		this.installer = new CriFsWebInstaller();
		File.Delete(dstPath);
		this.installer.Copy(srcPath, dstPath);
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Fs);
	}

	// Token: 0x06000338 RID: 824 RVA: 0x00007EC0 File Offset: 0x000062C0
	public override void Stop()
	{
		if (this.installer != null)
		{
			this.installer.Stop();
		}
	}

	// Token: 0x06000339 RID: 825 RVA: 0x00007ED8 File Offset: 0x000062D8
	public override void Update()
	{
		if (this.installer == null)
		{
			return;
		}
		CriFsWebInstaller.StatusInfo statusInfo = this.installer.GetStatusInfo();
		base.progress = (float)statusInfo.receivedSize / (float)((statusInfo.contentsSize <= 0L) ? 1L : statusInfo.contentsSize);
		if (statusInfo.status != CriFsWebInstaller.Status.Busy)
		{
			if (statusInfo.status == CriFsWebInstaller.Status.Error)
			{
				base.progress = -1f;
				base.error = "[CriFsWebInstallerError]" + statusInfo.error.ToString();
			}
			this.installer.Dispose();
			this.installer = null;
			base.Done();
		}
	}

	// Token: 0x0600033A RID: 826 RVA: 0x00007F88 File Offset: 0x00006388
	protected override void Dispose(bool disposing)
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.installer != null)
		{
			this.installer.Dispose();
			this.installer = null;
		}
	}

	// Token: 0x040001C8 RID: 456
	private CriFsWebInstaller installer;
}
