﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using AOT;
using UnityEngine;

// Token: 0x02000007 RID: 7
[AddComponentMenu("CRIWARE/CRI Atom")]
public class CriAtom : MonoBehaviour
{
	// Token: 0x17000004 RID: 4
	// (get) Token: 0x06000052 RID: 82 RVA: 0x00002E49 File Offset: 0x00001249
	// (set) Token: 0x06000053 RID: 83 RVA: 0x00002E50 File Offset: 0x00001250
	private static CriAtom instance { get; set; }

	// Token: 0x06000054 RID: 84 RVA: 0x00002E58 File Offset: 0x00001258
	public static void AttachDspBusSetting(string settingName)
	{
		CriAtom.instance.dspBusSetting = settingName;
		if (!string.IsNullOrEmpty(settingName))
		{
			CriAtomEx.AttachDspBusSetting(settingName);
		}
		else
		{
			CriAtomEx.DetachDspBusSetting();
		}
	}

	// Token: 0x06000055 RID: 85 RVA: 0x00002E80 File Offset: 0x00001280
	public static void DetachDspBusSetting()
	{
		CriAtom.instance.dspBusSetting = string.Empty;
		CriAtomEx.DetachDspBusSetting();
	}

	// Token: 0x06000056 RID: 86 RVA: 0x00002E96 File Offset: 0x00001296
	public static CriAtomCueSheet GetCueSheet(string name)
	{
		return CriAtom.instance.GetCueSheetInternal(name);
	}

	// Token: 0x06000057 RID: 87 RVA: 0x00002EA4 File Offset: 0x000012A4
	public static CriAtomCueSheet AddCueSheet(string name, string acbFile, string awbFile, CriFsBinder binder = null)
	{
		CriAtomCueSheet criAtomCueSheet = CriAtom.instance.AddCueSheetInternal(name, acbFile, awbFile, binder);
		if (Application.isPlaying)
		{
			criAtomCueSheet.acb = CriAtom.instance.LoadAcbFile(binder, acbFile, awbFile);
		}
		return criAtomCueSheet;
	}

	// Token: 0x06000058 RID: 88 RVA: 0x00002EE0 File Offset: 0x000012E0
	public static CriAtomCueSheet AddCueSheetAsync(string name, string acbFile, string awbFile, CriFsBinder binder = null, bool loadAwbOnMemory = false)
	{
		CriAtomCueSheet criAtomCueSheet = CriAtom.instance.AddCueSheetInternal(name, acbFile, awbFile, binder);
		if (Application.isPlaying)
		{
			CriAtom.instance.LoadAcbFileAsync(criAtomCueSheet, binder, acbFile, awbFile, loadAwbOnMemory);
		}
		return criAtomCueSheet;
	}

	// Token: 0x06000059 RID: 89 RVA: 0x00002F18 File Offset: 0x00001318
	public static CriAtomCueSheet AddCueSheet(string name, byte[] acbData, string awbFile, CriFsBinder awbBinder = null)
	{
		CriAtomCueSheet criAtomCueSheet = CriAtom.instance.AddCueSheetInternal(name, string.Empty, awbFile, awbBinder);
		if (Application.isPlaying)
		{
			criAtomCueSheet.acb = CriAtom.instance.LoadAcbData(acbData, awbBinder, awbFile);
		}
		return criAtomCueSheet;
	}

	// Token: 0x0600005A RID: 90 RVA: 0x00002F58 File Offset: 0x00001358
	public static CriAtomCueSheet AddCueSheetAsync(string name, byte[] acbData, string awbFile, CriFsBinder awbBinder = null, bool loadAwbOnMemory = false)
	{
		CriAtomCueSheet criAtomCueSheet = CriAtom.instance.AddCueSheetInternal(name, string.Empty, awbFile, awbBinder);
		if (Application.isPlaying)
		{
			CriAtom.instance.LoadAcbDataAsync(criAtomCueSheet, acbData, awbBinder, awbFile, loadAwbOnMemory);
		}
		return criAtomCueSheet;
	}

	// Token: 0x0600005B RID: 91 RVA: 0x00002F93 File Offset: 0x00001393
	public static void RemoveCueSheet(string name)
	{
		if (CriAtom.instance == null)
		{
			return;
		}
		CriAtom.instance.RemoveCueSheetInternal(name);
	}

	// Token: 0x17000005 RID: 5
	// (get) Token: 0x0600005C RID: 92 RVA: 0x00002FB4 File Offset: 0x000013B4
	public static bool CueSheetsAreLoading
	{
		get
		{
			if (CriAtom.instance == null)
			{
				return false;
			}
			foreach (CriAtomCueSheet criAtomCueSheet in CriAtom.instance.cueSheets)
			{
				if (criAtomCueSheet.IsLoading)
				{
					return true;
				}
			}
			return false;
		}
	}

	// Token: 0x0600005D RID: 93 RVA: 0x00003004 File Offset: 0x00001404
	public static CriAtomExAcb GetAcb(string cueSheetName)
	{
		foreach (CriAtomCueSheet criAtomCueSheet in CriAtom.instance.cueSheets)
		{
			if (cueSheetName == criAtomCueSheet.name)
			{
				return criAtomCueSheet.acb;
			}
		}
		Debug.LogWarning(cueSheetName + " is not loaded.");
		return null;
	}

	// Token: 0x0600005E RID: 94 RVA: 0x0000305D File Offset: 0x0000145D
	public static void SetCategoryVolume(string name, float volume)
	{
		CriAtomExCategory.SetVolume(name, volume);
	}

	// Token: 0x0600005F RID: 95 RVA: 0x00003066 File Offset: 0x00001466
	public static void SetCategoryVolume(int id, float volume)
	{
		CriAtomExCategory.SetVolume(id, volume);
	}

	// Token: 0x06000060 RID: 96 RVA: 0x0000306F File Offset: 0x0000146F
	public static float GetCategoryVolume(string name)
	{
		return CriAtomExCategory.GetVolume(name);
	}

	// Token: 0x06000061 RID: 97 RVA: 0x00003077 File Offset: 0x00001477
	public static float GetCategoryVolume(int id)
	{
		return CriAtomExCategory.GetVolume(id);
	}

	// Token: 0x06000062 RID: 98 RVA: 0x0000307F File Offset: 0x0000147F
	public static void SetBusAnalyzer(bool sw)
	{
		if (sw)
		{
			CriAtomExAsr.AttachBusAnalyzer(50, 1000);
		}
		else
		{
			CriAtomExAsr.DetachBusAnalyzer();
		}
	}

	// Token: 0x06000063 RID: 99 RVA: 0x000030A0 File Offset: 0x000014A0
	public static CriAtomExAsr.BusAnalyzerInfo GetBusAnalyzerInfo(int bus)
	{
		CriAtomExAsr.BusAnalyzerInfo result;
		CriAtomExAsr.GetBusAnalyzerInfo(bus, out result);
		return result;
	}

	// Token: 0x06000064 RID: 100 RVA: 0x000030B8 File Offset: 0x000014B8
	private void Setup()
	{
		CriAtom.instance = this;
		CriAtomPlugin.InitializeLibrary();
		if (!string.IsNullOrEmpty(this.acfFile))
		{
			string acfPath = Path.Combine(CriWare.streamingAssetsPath, this.acfFile);
			CriAtomEx.RegisterAcf(null, acfPath);
		}
		if (!string.IsNullOrEmpty(this.dspBusSetting))
		{
			CriAtom.AttachDspBusSetting(this.dspBusSetting);
		}
		foreach (CriAtomCueSheet criAtomCueSheet in this.cueSheets)
		{
			criAtomCueSheet.acb = this.LoadAcbFile(null, criAtomCueSheet.acbFile, criAtomCueSheet.awbFile);
		}
		if (this.dontDestroyOnLoad)
		{
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		}
	}

	// Token: 0x06000065 RID: 101 RVA: 0x00003164 File Offset: 0x00001564
	private void Shutdown()
	{
		foreach (CriAtomCueSheet criAtomCueSheet in this.cueSheets)
		{
			if (criAtomCueSheet.acb != null)
			{
				criAtomCueSheet.acb.Dispose();
				criAtomCueSheet.acb = null;
			}
		}
		CriAtomPlugin.FinalizeLibrary();
		CriAtom.instance = null;
	}

	// Token: 0x06000066 RID: 102 RVA: 0x000031B8 File Offset: 0x000015B8
	private void Awake()
	{
		if (CriAtom.instance != null)
		{
			if (CriAtom.instance.acfFile != this.acfFile)
			{
				GameObject gameObject = CriAtom.instance.gameObject;
				CriAtom.instance.Shutdown();
				CriAtomEx.UnregisterAcf();
				UnityEngine.Object.Destroy(gameObject);
				return;
			}
			if (CriAtom.instance.dspBusSetting != this.dspBusSetting)
			{
				CriAtom.AttachDspBusSetting(this.dspBusSetting);
			}
			CriAtom.instance.MargeCueSheet(this.cueSheets, this.dontRemoveExistsCueSheet);
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	// Token: 0x06000067 RID: 103 RVA: 0x00003256 File Offset: 0x00001656
	private void OnEnable()
	{
		if (CriAtom.instance != null)
		{
			return;
		}
		this.Setup();
	}

	// Token: 0x06000068 RID: 104 RVA: 0x0000326F File Offset: 0x0000166F
	private void OnDestroy()
	{
		if (this != CriAtom.instance)
		{
			return;
		}
		this.Shutdown();
	}

	// Token: 0x06000069 RID: 105 RVA: 0x00003288 File Offset: 0x00001688
	private void Update()
	{
		CriAtomPlugin.criAtomUnitySequencer_ExecuteQueuedEventCallbacks();
		CriAtomPlugin.criAtomUnity_ExecuteQueuedBeatSyncCallbacks();
	}

	// Token: 0x0600006A RID: 106 RVA: 0x00003294 File Offset: 0x00001694
	public CriAtomCueSheet GetCueSheetInternal(string name)
	{
		for (int i = 0; i < this.cueSheets.Length; i++)
		{
			CriAtomCueSheet criAtomCueSheet = this.cueSheets[i];
			if (criAtomCueSheet.name == name)
			{
				return criAtomCueSheet;
			}
		}
		return null;
	}

	// Token: 0x0600006B RID: 107 RVA: 0x000032D8 File Offset: 0x000016D8
	public CriAtomCueSheet AddCueSheetInternal(string name, string acbFile, string awbFile, CriFsBinder binder)
	{
		CriAtomCueSheet[] array = new CriAtomCueSheet[this.cueSheets.Length + 1];
		this.cueSheets.CopyTo(array, 0);
		this.cueSheets = array;
		CriAtomCueSheet criAtomCueSheet = new CriAtomCueSheet();
		this.cueSheets[this.cueSheets.Length - 1] = criAtomCueSheet;
		if (string.IsNullOrEmpty(name))
		{
			criAtomCueSheet.name = Path.GetFileNameWithoutExtension(acbFile);
		}
		else
		{
			criAtomCueSheet.name = name;
		}
		criAtomCueSheet.acbFile = acbFile;
		criAtomCueSheet.awbFile = awbFile;
		return criAtomCueSheet;
	}

	// Token: 0x0600006C RID: 108 RVA: 0x00003354 File Offset: 0x00001754
	public void RemoveCueSheetInternal(string name)
	{
		int num = -1;
		for (int i = 0; i < this.cueSheets.Length; i++)
		{
			if (name == this.cueSheets[i].name)
			{
				num = i;
			}
		}
		if (num < 0)
		{
			return;
		}
		CriAtomCueSheet criAtomCueSheet = this.cueSheets[num];
		if (criAtomCueSheet.acb != null)
		{
			criAtomCueSheet.acb.Dispose();
			criAtomCueSheet.acb = null;
		}
		CriAtomCueSheet[] destinationArray = new CriAtomCueSheet[this.cueSheets.Length - 1];
		Array.Copy(this.cueSheets, 0, destinationArray, 0, num);
		Array.Copy(this.cueSheets, num + 1, destinationArray, num, this.cueSheets.Length - num - 1);
		this.cueSheets = destinationArray;
	}

	// Token: 0x0600006D RID: 109 RVA: 0x00003408 File Offset: 0x00001808
	private void MargeCueSheet(CriAtomCueSheet[] newCueSheets, bool newDontRemoveExistsCueSheet)
	{
		if (!newDontRemoveExistsCueSheet)
		{
			int i = 0;
			while (i < this.cueSheets.Length)
			{
				int num = Array.FindIndex<CriAtomCueSheet>(newCueSheets, (CriAtomCueSheet sheet) => sheet.name == this.cueSheets[i].name);
				if (num < 0)
				{
					CriAtom.RemoveCueSheet(this.cueSheets[i].name);
				}
				else
				{
					i++;
				}
			}
		}
		foreach (CriAtomCueSheet criAtomCueSheet in newCueSheets)
		{
			if (this.GetCueSheetInternal(criAtomCueSheet.name) == null)
			{
				CriAtom.AddCueSheet(null, criAtomCueSheet.acbFile, criAtomCueSheet.awbFile, null);
			}
		}
	}

	// Token: 0x0600006E RID: 110 RVA: 0x000034D4 File Offset: 0x000018D4
	private CriAtomExAcb LoadAcbFile(CriFsBinder binder, string acbFile, string awbFile)
	{
		if (string.IsNullOrEmpty(acbFile))
		{
			return null;
		}
		string text = acbFile;
		if (binder == null && CriWare.IsStreamingAssetsPath(text))
		{
			text = Path.Combine(CriWare.streamingAssetsPath, text);
		}
		string text2 = awbFile;
		if (!string.IsNullOrEmpty(text2) && binder == null && CriWare.IsStreamingAssetsPath(text2))
		{
			text2 = Path.Combine(CriWare.streamingAssetsPath, text2);
		}
		return CriAtomExAcb.LoadAcbFile(binder, text, text2);
	}

	// Token: 0x0600006F RID: 111 RVA: 0x00003540 File Offset: 0x00001940
	private CriAtomExAcb LoadAcbData(byte[] acbData, CriFsBinder binder, string awbFile)
	{
		if (acbData == null)
		{
			return null;
		}
		string text = awbFile;
		if (!string.IsNullOrEmpty(text) && binder == null && CriWare.IsStreamingAssetsPath(text))
		{
			text = Path.Combine(CriWare.streamingAssetsPath, text);
		}
		return CriAtomExAcb.LoadAcbData(acbData, binder, text);
	}

	// Token: 0x06000070 RID: 112 RVA: 0x00003587 File Offset: 0x00001987
	private void LoadAcbFileAsync(CriAtomCueSheet cueSheet, CriFsBinder binder, string acbFile, string awbFile, bool loadAwbOnMemory)
	{
		if (string.IsNullOrEmpty(acbFile))
		{
			return;
		}
		base.StartCoroutine(this.LoadAcbFileCoroutine(cueSheet, binder, acbFile, awbFile, loadAwbOnMemory));
	}

	// Token: 0x06000071 RID: 113 RVA: 0x000035AC File Offset: 0x000019AC
	private IEnumerator LoadAcbFileCoroutine(CriAtomCueSheet cueSheet, CriFsBinder binder, string acbPath, string awbPath, bool loadAwbOnMemory)
	{
		cueSheet.loaderStatus = CriAtomExAcbLoader.Status.Loading;
		if (binder == null && CriWare.IsStreamingAssetsPath(acbPath))
		{
			acbPath = Path.Combine(CriWare.streamingAssetsPath, acbPath);
		}
		if (!string.IsNullOrEmpty(awbPath) && binder == null && CriWare.IsStreamingAssetsPath(awbPath))
		{
			awbPath = Path.Combine(CriWare.streamingAssetsPath, awbPath);
		}
		while (this.acfIsLoading)
		{
			yield return new WaitForEndOfFrame();
		}
		using (CriAtomExAcbLoader asyncLoader = CriAtomExAcbLoader.LoadAcbFileAsync(binder, acbPath, awbPath, loadAwbOnMemory))
		{
			if (asyncLoader == null)
			{
				cueSheet.loaderStatus = CriAtomExAcbLoader.Status.Error;
				yield break;
			}
			for (;;)
			{
				CriAtomExAcbLoader.Status status = asyncLoader.GetStatus();
				cueSheet.loaderStatus = status;
				if (status == CriAtomExAcbLoader.Status.Complete)
				{
					break;
				}
				if (status == CriAtomExAcbLoader.Status.Error)
				{
					goto Block_11;
				}
				yield return new WaitForEndOfFrame();
			}
			cueSheet.acb = asyncLoader.MoveAcb();
			Block_11:;
		}
		yield break;
	}

	// Token: 0x06000072 RID: 114 RVA: 0x000035EC File Offset: 0x000019EC
	private void LoadAcbDataAsync(CriAtomCueSheet cueSheet, byte[] acbData, CriFsBinder awbBinder, string awbFile, bool loadAwbOnMemory)
	{
		base.StartCoroutine(this.LoadAcbDataCoroutine(cueSheet, acbData, awbBinder, awbFile, loadAwbOnMemory));
	}

	// Token: 0x06000073 RID: 115 RVA: 0x00003604 File Offset: 0x00001A04
	private IEnumerator LoadAcbDataCoroutine(CriAtomCueSheet cueSheet, byte[] acbData, CriFsBinder awbBinder, string awbPath, bool loadAwbOnMemory)
	{
		cueSheet.loaderStatus = CriAtomExAcbLoader.Status.Loading;
		if (!string.IsNullOrEmpty(awbPath) && awbBinder == null && CriWare.IsStreamingAssetsPath(awbPath))
		{
			awbPath = Path.Combine(CriWare.streamingAssetsPath, awbPath);
		}
		while (this.acfIsLoading)
		{
			yield return new WaitForEndOfFrame();
		}
		using (CriAtomExAcbLoader asyncLoader = CriAtomExAcbLoader.LoadAcbDataAsync(acbData, awbBinder, awbPath, loadAwbOnMemory))
		{
			if (asyncLoader == null)
			{
				cueSheet.loaderStatus = CriAtomExAcbLoader.Status.Error;
				yield break;
			}
			for (;;)
			{
				CriAtomExAcbLoader.Status status = asyncLoader.GetStatus();
				cueSheet.loaderStatus = status;
				if (status == CriAtomExAcbLoader.Status.Complete)
				{
					break;
				}
				if (status == CriAtomExAcbLoader.Status.Error)
				{
					goto Block_9;
				}
				yield return new WaitForEndOfFrame();
			}
			cueSheet.acb = asyncLoader.MoveAcb();
			Block_9:;
		}
		yield break;
	}

	// Token: 0x06000074 RID: 116 RVA: 0x00003644 File Offset: 0x00001A44
	[MonoPInvokeCallback(typeof(CriAtomExSequencer.EventCbFunc))]
	public static void SequenceEventCallbackFromNative(string eventString)
	{
		if (CriAtom.eventUserCbFunc != null)
		{
			CriAtom.eventUserCbFunc(eventString);
		}
	}

	// Token: 0x06000075 RID: 117 RVA: 0x0000365B File Offset: 0x00001A5B
	[MonoPInvokeCallback(typeof(CriAtomExBeatSync.CbFunc))]
	public static void BeatSyncCallbackFromNative(ref CriAtomExBeatSync.Info info)
	{
		if (CriAtom.beatsyncUserCbFunc != null)
		{
			CriAtom.beatsyncUserCbFunc(ref info);
		}
	}

	// Token: 0x06000076 RID: 118 RVA: 0x00003674 File Offset: 0x00001A74
	public static void SetEventCallback(CriAtomExSequencer.EventCbFunc func, string separator)
	{
		IntPtr cbfunc = IntPtr.Zero;
		CriAtom.eventUserCbFunc = func;
		if (func != null)
		{
			CriAtomExSequencer.EventCbFunc d = new CriAtomExSequencer.EventCbFunc(CriAtom.SequenceEventCallbackFromNative);
			cbfunc = Marshal.GetFunctionPointerForDelegate(d);
		}
		CriAtomPlugin.criAtomUnitySequencer_SetEventCallback(cbfunc, separator);
	}

	// Token: 0x06000077 RID: 119 RVA: 0x000036B0 File Offset: 0x00001AB0
	public static void SetBeatSyncCallback(CriAtomExBeatSync.CbFunc func)
	{
		IntPtr cbfunc = IntPtr.Zero;
		CriAtom.beatsyncUserCbFunc = func;
		if (func != null)
		{
			CriAtomExBeatSync.CbFunc d = new CriAtomExBeatSync.CbFunc(CriAtom.BeatSyncCallbackFromNative);
			cbfunc = Marshal.GetFunctionPointerForDelegate(d);
		}
		CriAtomPlugin.criAtomUnity_SetBeatSyncCallback(cbfunc);
	}

	// Token: 0x04000051 RID: 81
	public string acfFile = string.Empty;

	// Token: 0x04000052 RID: 82
	private bool acfIsLoading;

	// Token: 0x04000053 RID: 83
	public CriAtomCueSheet[] cueSheets = new CriAtomCueSheet[0];

	// Token: 0x04000054 RID: 84
	public string dspBusSetting = string.Empty;

	// Token: 0x04000055 RID: 85
	public bool dontDestroyOnLoad;

	// Token: 0x04000056 RID: 86
	private static CriAtomExSequencer.EventCbFunc eventUserCbFunc;

	// Token: 0x04000057 RID: 87
	private static CriAtomExBeatSync.CbFunc beatsyncUserCbFunc;

	// Token: 0x04000059 RID: 89
	public bool dontRemoveExistsCueSheet;
}
