﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

// Token: 0x020000B9 RID: 185
public class HockeyAppIOS : MonoBehaviour
{
	// Token: 0x06000532 RID: 1330 RVA: 0x0000CDDD File Offset: 0x0000B1DD
	private void Awake()
	{
	}

	// Token: 0x06000533 RID: 1331 RVA: 0x0000CDDF File Offset: 0x0000B1DF
	private void OnEnable()
	{
	}

	// Token: 0x06000534 RID: 1332 RVA: 0x0000CDE1 File Offset: 0x0000B1E1
	private void OnDisable()
	{
	}

	// Token: 0x06000535 RID: 1333 RVA: 0x0000CDE3 File Offset: 0x0000B1E3
	private void StartPlugin()
	{
	}

	// Token: 0x06000536 RID: 1334 RVA: 0x0000CDE5 File Offset: 0x0000B1E5
	[Obsolete("This is an obsolete method")]
	public static void ShowFeedbackForm()
	{
	}

	// Token: 0x06000537 RID: 1335 RVA: 0x0000CDE7 File Offset: 0x0000B1E7
	public static void CheckForUpdate()
	{
	}

	// Token: 0x06000538 RID: 1336 RVA: 0x0000CDEC File Offset: 0x0000B1EC
	protected virtual List<string> GetLogHeaders()
	{
		return new List<string>();
	}

	// Token: 0x06000539 RID: 1337 RVA: 0x0000CE00 File Offset: 0x0000B200
	protected virtual WWWForm CreateForm(string log)
	{
		return new WWWForm();
	}

	// Token: 0x0600053A RID: 1338 RVA: 0x0000CE14 File Offset: 0x0000B214
	protected virtual void CreateLogDirectory()
	{
	}

	// Token: 0x0600053B RID: 1339 RVA: 0x0000CE18 File Offset: 0x0000B218
	protected virtual List<string> GetLogFiles()
	{
		return new List<string>();
	}

	// Token: 0x0600053C RID: 1340 RVA: 0x0000CE2C File Offset: 0x0000B22C
	protected virtual IEnumerator SendLogs(List<string> logs)
	{
		string crashPath = "api/2/apps/[APPID]/crashes/upload";
		string url = this.GetBaseURL() + crashPath.Replace("[APPID]", this.appID);
		foreach (string log in logs)
		{
			WWWForm postForm = this.CreateForm(log);
			string lContent = postForm.headers["Content-Type"].ToString();
			lContent = lContent.Replace("\"", string.Empty);
			Dictionary<string, string> headers = new Dictionary<string, string>();
			headers.Add("Content-Type", lContent);
			using (WWW www = new WWW(url, postForm.data, headers))
			{
				yield return www;
				if (string.IsNullOrEmpty(www.error))
				{
					try
					{
						File.Delete(log);
					}
					catch (Exception arg)
					{
						if (Debug.isDebugBuild)
						{
							Debug.Log("Failed to delete exception log: " + arg);
						}
					}
				}
			}
		}
		yield break;
	}

	// Token: 0x0600053D RID: 1341 RVA: 0x0000CE4E File Offset: 0x0000B24E
	protected virtual void WriteLogToDisk(string logString, string stackTrace)
	{
	}

	// Token: 0x0600053E RID: 1342 RVA: 0x0000CE50 File Offset: 0x0000B250
	protected virtual string GetBaseURL()
	{
		return string.Empty;
	}

	// Token: 0x0600053F RID: 1343 RVA: 0x0000CE64 File Offset: 0x0000B264
	protected virtual string GetAuthenticatorTypeString()
	{
		return string.Empty;
	}

	// Token: 0x06000540 RID: 1344 RVA: 0x0000CE78 File Offset: 0x0000B278
	protected virtual bool IsConnected()
	{
		return false;
	}

	// Token: 0x06000541 RID: 1345 RVA: 0x0000CE88 File Offset: 0x0000B288
	protected virtual void HandleException(string logString, string stackTrace)
	{
	}

	// Token: 0x06000542 RID: 1346 RVA: 0x0000CE8A File Offset: 0x0000B28A
	public void OnHandleLogCallback(string logString, string stackTrace, LogType type)
	{
	}

	// Token: 0x06000543 RID: 1347 RVA: 0x0000CE8C File Offset: 0x0000B28C
	public void OnHandleUnresolvedException(object sender, UnhandledExceptionEventArgs args)
	{
	}

	// Token: 0x04000356 RID: 854
	protected const string HOCKEYAPP_BASEURL = "https://rink.hockeyapp.net/";

	// Token: 0x04000357 RID: 855
	protected const string HOCKEYAPP_CRASHESPATH = "api/2/apps/[APPID]/crashes/upload";

	// Token: 0x04000358 RID: 856
	protected const string LOG_FILE_DIR = "/logs/";

	// Token: 0x04000359 RID: 857
	protected const int MAX_CHARS = 199800;

	// Token: 0x0400035A RID: 858
	private static HockeyAppIOS instance;

	// Token: 0x0400035B RID: 859
	[Header("HockeyApp Setup")]
	public string appID = "your-hockey-app-id";

	// Token: 0x0400035C RID: 860
	public string serverURL = "your-custom-server-url";

	// Token: 0x0400035D RID: 861
	[Header("Authentication")]
	public HockeyAppIOS.AuthenticatorType authenticatorType;

	// Token: 0x0400035E RID: 862
	public string secret = "your-hockey-app-secret";

	// Token: 0x0400035F RID: 863
	[Header("Crashes & Exceptions")]
	public bool autoUploadCrashes;

	// Token: 0x04000360 RID: 864
	public bool exceptionLogging = true;

	// Token: 0x04000361 RID: 865
	[Header("Metrics")]
	public bool userMetrics = true;

	// Token: 0x04000362 RID: 866
	[Header("Version Updates")]
	public bool updateAlert = true;

	// Token: 0x020000BA RID: 186
	public enum AuthenticatorType
	{
		// Token: 0x04000364 RID: 868
		Anonymous,
		// Token: 0x04000365 RID: 869
		Device,
		// Token: 0x04000366 RID: 870
		HockeyAppUser,
		// Token: 0x04000367 RID: 871
		HockeyAppEmail,
		// Token: 0x04000368 RID: 872
		WebAuth
	}
}
