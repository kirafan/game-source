﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Threading;
using MsgPack.Compiler;

namespace MsgPack
{
	// Token: 0x020000BD RID: 189
	public class CompiledPacker
	{
		// Token: 0x06000550 RID: 1360 RVA: 0x0000D790 File Offset: 0x0000BB90
		public CompiledPacker() : this(false)
		{
		}

		// Token: 0x06000551 RID: 1361 RVA: 0x0000D799 File Offset: 0x0000BB99
		public CompiledPacker(bool packPrivateField)
		{
			this._packer = ((!packPrivateField) ? CompiledPacker._publicFieldPacker : CompiledPacker._allFieldPacker);
		}

		// Token: 0x06000552 RID: 1362 RVA: 0x0000D7BC File Offset: 0x0000BBBC
		public void Prepare<T>()
		{
			this._packer.CreatePacker<T>();
			this._packer.CreateUnpacker<T>();
		}

		// Token: 0x06000553 RID: 1363 RVA: 0x0000D7D8 File Offset: 0x0000BBD8
		public byte[] Pack<T>(T o)
		{
			byte[] result;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				this.Pack<T>(memoryStream, o);
				result = memoryStream.ToArray();
			}
			return result;
		}

		// Token: 0x06000554 RID: 1364 RVA: 0x0000D820 File Offset: 0x0000BC20
		public void Pack<T>(Stream strm, T o)
		{
			this._packer.CreatePacker<T>()(new MsgPackWriter(strm), o);
		}

		// Token: 0x06000555 RID: 1365 RVA: 0x0000D839 File Offset: 0x0000BC39
		public T Unpack<T>(byte[] buf)
		{
			return this.Unpack<T>(buf, 0, buf.Length);
		}

		// Token: 0x06000556 RID: 1366 RVA: 0x0000D848 File Offset: 0x0000BC48
		public T Unpack<T>(byte[] buf, int offset, int size)
		{
			T result;
			using (MemoryStream memoryStream = new MemoryStream(buf, offset, size))
			{
				result = this.Unpack<T>(memoryStream);
			}
			return result;
		}

		// Token: 0x06000557 RID: 1367 RVA: 0x0000D88C File Offset: 0x0000BC8C
		public T Unpack<T>(Stream strm)
		{
			return this._packer.CreateUnpacker<T>()(new MsgPackReader(strm));
		}

		// Token: 0x06000558 RID: 1368 RVA: 0x0000D8A4 File Offset: 0x0000BCA4
		public byte[] Pack(object o)
		{
			byte[] result;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				this.Pack(memoryStream, o);
				result = memoryStream.ToArray();
			}
			return result;
		}

		// Token: 0x06000559 RID: 1369 RVA: 0x0000D8EC File Offset: 0x0000BCEC
		public void Pack(Stream strm, object o)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600055A RID: 1370 RVA: 0x0000D8F3 File Offset: 0x0000BCF3
		public object Unpack(Type t, byte[] buf)
		{
			return this.Unpack(t, buf, 0, buf.Length);
		}

		// Token: 0x0600055B RID: 1371 RVA: 0x0000D904 File Offset: 0x0000BD04
		public object Unpack(Type t, byte[] buf, int offset, int size)
		{
			object result;
			using (MemoryStream memoryStream = new MemoryStream(buf, offset, size))
			{
				result = this.Unpack(t, memoryStream);
			}
			return result;
		}

		// Token: 0x0600055C RID: 1372 RVA: 0x0000D948 File Offset: 0x0000BD48
		public object Unpack(Type t, Stream strm)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0400036A RID: 874
		private static CompiledPacker.PackerBase _publicFieldPacker = new CompiledPacker.MethodBuilderPacker();

		// Token: 0x0400036B RID: 875
		private static CompiledPacker.PackerBase _allFieldPacker = new CompiledPacker.DynamicMethodPacker();

		// Token: 0x0400036C RID: 876
		private CompiledPacker.PackerBase _packer;

		// Token: 0x020000BE RID: 190
		public abstract class PackerBase
		{
			// Token: 0x0600055D RID: 1373 RVA: 0x0000D950 File Offset: 0x0000BD50
			protected PackerBase()
			{
				CompiledPacker.DefaultPackMethods.Register(this._packMethods, this._unpackMethods);
			}

			// Token: 0x0600055E RID: 1374 RVA: 0x0000D9A0 File Offset: 0x0000BDA0
			public Action<MsgPackWriter, T> CreatePacker<T>()
			{
				object packers = this._packers;
				Delegate @delegate;
				lock (packers)
				{
					if (!this._packers.TryGetValue(typeof(T), out @delegate))
					{
						@delegate = this.CreatePacker_Internal<T>();
						this._packers.Add(typeof(T), @delegate);
					}
				}
				return (Action<MsgPackWriter, T>)@delegate;
			}

			// Token: 0x0600055F RID: 1375 RVA: 0x0000DA18 File Offset: 0x0000BE18
			public Func<MsgPackReader, T> CreateUnpacker<T>()
			{
				object unpackers = this._unpackers;
				Delegate @delegate;
				lock (unpackers)
				{
					if (!this._unpackers.TryGetValue(typeof(T), out @delegate))
					{
						@delegate = this.CreateUnpacker_Internal<T>();
						this._unpackers.Add(typeof(T), @delegate);
					}
				}
				return (Func<MsgPackReader, T>)@delegate;
			}

			// Token: 0x06000560 RID: 1376
			protected abstract Action<MsgPackWriter, T> CreatePacker_Internal<T>();

			// Token: 0x06000561 RID: 1377
			protected abstract Func<MsgPackReader, T> CreateUnpacker_Internal<T>();

			// Token: 0x0400036D RID: 877
			private Dictionary<Type, Delegate> _packers = new Dictionary<Type, Delegate>();

			// Token: 0x0400036E RID: 878
			private Dictionary<Type, Delegate> _unpackers = new Dictionary<Type, Delegate>();

			// Token: 0x0400036F RID: 879
			protected Dictionary<Type, MethodInfo> _packMethods = new Dictionary<Type, MethodInfo>();

			// Token: 0x04000370 RID: 880
			protected Dictionary<Type, MethodInfo> _unpackMethods = new Dictionary<Type, MethodInfo>();
		}

		// Token: 0x020000BF RID: 191
		public sealed class DynamicMethodPacker : CompiledPacker.PackerBase
		{
			// Token: 0x06000562 RID: 1378 RVA: 0x0000DA90 File Offset: 0x0000BE90
			static DynamicMethodPacker()
			{
				CompiledPacker.DynamicMethodPacker.LookupMemberMappingMethod = typeof(CompiledPacker.DynamicMethodPacker).GetMethod("LookupMemberMapping", BindingFlags.Static | BindingFlags.NonPublic);
			}

			// Token: 0x06000564 RID: 1380 RVA: 0x0000DAC0 File Offset: 0x0000BEC0
			protected override Action<MsgPackWriter, T> CreatePacker_Internal<T>()
			{
				DynamicMethod dynamicMethod = this.CreatePacker(typeof(T), CompiledPacker.DynamicMethodPacker.CreatePackDynamicMethod(typeof(T)));
				return (Action<MsgPackWriter, T>)dynamicMethod.CreateDelegate(typeof(Action<MsgPackWriter, T>));
			}

			// Token: 0x06000565 RID: 1381 RVA: 0x0000DB04 File Offset: 0x0000BF04
			protected override Func<MsgPackReader, T> CreateUnpacker_Internal<T>()
			{
				DynamicMethod dynamicMethod = this.CreateUnpacker(typeof(T), CompiledPacker.DynamicMethodPacker.CreateUnpackDynamicMethod(typeof(T)));
				return (Func<MsgPackReader, T>)dynamicMethod.CreateDelegate(typeof(Func<MsgPackReader, T>));
			}

			// Token: 0x06000566 RID: 1382 RVA: 0x0000DB48 File Offset: 0x0000BF48
			private DynamicMethod CreatePacker(Type t, DynamicMethod dm)
			{
				ILGenerator ilgenerator = dm.GetILGenerator();
				this._packMethods.Add(t, dm);
				ILGenerator il = ilgenerator;
				if (CompiledPacker.DynamicMethodPacker.<>f__mg$cache0 == null)
				{
					CompiledPacker.DynamicMethodPacker.<>f__mg$cache0 = new Func<Type, MemberInfo[]>(CompiledPacker.DynamicMethodPacker.LookupMembers);
				}
				Func<Type, MemberInfo[]> targetMemberSelector = CompiledPacker.DynamicMethodPacker.<>f__mg$cache0;
				if (CompiledPacker.DynamicMethodPacker.<>f__mg$cache1 == null)
				{
					CompiledPacker.DynamicMethodPacker.<>f__mg$cache1 = new Func<MemberInfo, string>(CompiledPacker.DynamicMethodPacker.FormatMemberName);
				}
				PackILGenerator.EmitPackCode(t, dm, il, targetMemberSelector, CompiledPacker.DynamicMethodPacker.<>f__mg$cache1, new Func<Type, MethodInfo>(this.LookupPackMethod));
				return dm;
			}

			// Token: 0x06000567 RID: 1383 RVA: 0x0000DBB8 File Offset: 0x0000BFB8
			private DynamicMethod CreateUnpacker(Type t, DynamicMethod dm)
			{
				ILGenerator ilgenerator = dm.GetILGenerator();
				this._unpackMethods.Add(t, dm);
				ILGenerator il = ilgenerator;
				if (CompiledPacker.DynamicMethodPacker.<>f__mg$cache2 == null)
				{
					CompiledPacker.DynamicMethodPacker.<>f__mg$cache2 = new Func<Type, MemberInfo[]>(CompiledPacker.DynamicMethodPacker.LookupMembers);
				}
				Func<Type, MemberInfo[]> targetMemberSelector = CompiledPacker.DynamicMethodPacker.<>f__mg$cache2;
				if (CompiledPacker.DynamicMethodPacker.<>f__mg$cache3 == null)
				{
					CompiledPacker.DynamicMethodPacker.<>f__mg$cache3 = new Func<MemberInfo, string>(CompiledPacker.DynamicMethodPacker.FormatMemberName);
				}
				Func<MemberInfo, string> memberNameFormatter = CompiledPacker.DynamicMethodPacker.<>f__mg$cache3;
				Func<Type, MethodInfo> lookupUnpackMethod = new Func<Type, MethodInfo>(this.LookupUnpackMethod);
				if (CompiledPacker.DynamicMethodPacker.<>f__mg$cache4 == null)
				{
					CompiledPacker.DynamicMethodPacker.<>f__mg$cache4 = new Func<Type, IDictionary<string, int>>(CompiledPacker.DynamicMethodPacker.LookupMemberMapping);
				}
				PackILGenerator.EmitUnpackCode(t, dm, il, targetMemberSelector, memberNameFormatter, lookupUnpackMethod, CompiledPacker.DynamicMethodPacker.<>f__mg$cache4, CompiledPacker.DynamicMethodPacker.LookupMemberMappingMethod);
				return dm;
			}

			// Token: 0x06000568 RID: 1384 RVA: 0x0000DC4A File Offset: 0x0000C04A
			private static DynamicMethod CreatePackDynamicMethod(Type t)
			{
				return CompiledPacker.DynamicMethodPacker.CreateDynamicMethod(typeof(void), new Type[]
				{
					typeof(MsgPackWriter),
					t
				});
			}

			// Token: 0x06000569 RID: 1385 RVA: 0x0000DC72 File Offset: 0x0000C072
			private static DynamicMethod CreateUnpackDynamicMethod(Type t)
			{
				return CompiledPacker.DynamicMethodPacker.CreateDynamicMethod(t, new Type[]
				{
					typeof(MsgPackReader)
				});
			}

			// Token: 0x0600056A RID: 1386 RVA: 0x0000DC90 File Offset: 0x0000C090
			private static MemberInfo[] LookupMembers(Type t)
			{
				BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
				List<MemberInfo> list = new List<MemberInfo>();
				list.AddRange(t.GetFields(bindingAttr));
				return list.ToArray();
			}

			// Token: 0x0600056B RID: 1387 RVA: 0x0000DCBC File Offset: 0x0000C0BC
			private MethodInfo LookupPackMethod(Type t)
			{
				MethodInfo result;
				if (this._packMethods.TryGetValue(t, out result))
				{
					return result;
				}
				DynamicMethod dm = CompiledPacker.DynamicMethodPacker.CreatePackDynamicMethod(t);
				return this.CreatePacker(t, dm);
			}

			// Token: 0x0600056C RID: 1388 RVA: 0x0000DCF0 File Offset: 0x0000C0F0
			private MethodInfo LookupUnpackMethod(Type t)
			{
				MethodInfo result;
				if (this._unpackMethods.TryGetValue(t, out result))
				{
					return result;
				}
				DynamicMethod dm = CompiledPacker.DynamicMethodPacker.CreateUnpackDynamicMethod(t);
				return this.CreateUnpacker(t, dm);
			}

			// Token: 0x0600056D RID: 1389 RVA: 0x0000DD24 File Offset: 0x0000C124
			private static string FormatMemberName(MemberInfo m)
			{
				if (m.MemberType != MemberTypes.Field)
				{
					return m.Name;
				}
				string text = m.Name;
				int num;
				if (text[0] == '<' && (num = text.IndexOf('>')) > 1)
				{
					text = text.Substring(1, num - 1);
				}
				return text;
			}

			// Token: 0x0600056E RID: 1390 RVA: 0x0000DD78 File Offset: 0x0000C178
			private static DynamicMethod CreateDynamicMethod(Type returnType, Type[] parameterTypes)
			{
				string name = "_" + Interlocked.Increment(ref CompiledPacker.DynamicMethodPacker._dynamicMethodIdx).ToString();
				return new DynamicMethod(name, returnType, parameterTypes, true);
			}

			// Token: 0x0600056F RID: 1391 RVA: 0x0000DDB4 File Offset: 0x0000C1B4
			internal static IDictionary<string, int> LookupMemberMapping(Type t)
			{
				object unpackMemberMappings = CompiledPacker.DynamicMethodPacker.UnpackMemberMappings;
				IDictionary<string, int> dictionary;
				lock (unpackMemberMappings)
				{
					if (!CompiledPacker.DynamicMethodPacker.UnpackMemberMappings.TryGetValue(t, out dictionary))
					{
						dictionary = new Dictionary<string, int>();
						CompiledPacker.DynamicMethodPacker.UnpackMemberMappings.Add(t, dictionary);
					}
				}
				return dictionary;
			}

			// Token: 0x04000371 RID: 881
			private static MethodInfo LookupMemberMappingMethod;

			// Token: 0x04000372 RID: 882
			private static Dictionary<Type, IDictionary<string, int>> UnpackMemberMappings = new Dictionary<Type, IDictionary<string, int>>();

			// Token: 0x04000373 RID: 883
			private static int _dynamicMethodIdx;

			// Token: 0x04000374 RID: 884
			[CompilerGenerated]
			private static Func<Type, MemberInfo[]> <>f__mg$cache0;

			// Token: 0x04000375 RID: 885
			[CompilerGenerated]
			private static Func<MemberInfo, string> <>f__mg$cache1;

			// Token: 0x04000376 RID: 886
			[CompilerGenerated]
			private static Func<Type, MemberInfo[]> <>f__mg$cache2;

			// Token: 0x04000377 RID: 887
			[CompilerGenerated]
			private static Func<MemberInfo, string> <>f__mg$cache3;

			// Token: 0x04000378 RID: 888
			[CompilerGenerated]
			private static Func<Type, IDictionary<string, int>> <>f__mg$cache4;
		}

		// Token: 0x020000C0 RID: 192
		public sealed class MethodBuilderPacker : CompiledPacker.PackerBase
		{
			// Token: 0x06000570 RID: 1392 RVA: 0x0000DE10 File Offset: 0x0000C210
			static MethodBuilderPacker()
			{
				CompiledPacker.MethodBuilderPacker.LookupMemberMappingMethod = typeof(CompiledPacker.MethodBuilderPacker).GetMethod("LookupMemberMapping", BindingFlags.Static | BindingFlags.NonPublic);
				CompiledPacker.MethodBuilderPacker.DynamicAsmName = new AssemblyName("MessagePackInternalAssembly");
				CompiledPacker.MethodBuilderPacker.DynamicAsmBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(CompiledPacker.MethodBuilderPacker.DynamicAsmName, AssemblyBuilderAccess.Run);
				CompiledPacker.MethodBuilderPacker.DynamicModuleBuilder = CompiledPacker.MethodBuilderPacker.DynamicAsmBuilder.DefineDynamicModule(CompiledPacker.MethodBuilderPacker.DynamicAsmName.Name);
			}

			// Token: 0x06000572 RID: 1394 RVA: 0x0000DE88 File Offset: 0x0000C288
			protected override Action<MsgPackWriter, T> CreatePacker_Internal<T>()
			{
				TypeBuilder tb;
				MethodBuilder methodBuilder;
				CompiledPacker.MethodBuilderPacker.CreatePackMethodBuilder(typeof(T), out tb, out methodBuilder);
				this._packMethods.Add(typeof(T), methodBuilder);
				this.CreatePacker(typeof(T), methodBuilder);
				MethodInfo method = this.ToCallableMethodInfo(typeof(T), tb, true);
				return (Action<MsgPackWriter, T>)Delegate.CreateDelegate(typeof(Action<MsgPackWriter, T>), method);
			}

			// Token: 0x06000573 RID: 1395 RVA: 0x0000DEF8 File Offset: 0x0000C2F8
			protected override Func<MsgPackReader, T> CreateUnpacker_Internal<T>()
			{
				TypeBuilder tb;
				MethodBuilder methodBuilder;
				CompiledPacker.MethodBuilderPacker.CreateUnpackMethodBuilder(typeof(T), out tb, out methodBuilder);
				this._unpackMethods.Add(typeof(T), methodBuilder);
				this.CreateUnpacker(typeof(T), methodBuilder);
				MethodInfo method = this.ToCallableMethodInfo(typeof(T), tb, false);
				return (Func<MsgPackReader, T>)Delegate.CreateDelegate(typeof(Func<MsgPackReader, T>), method);
			}

			// Token: 0x06000574 RID: 1396 RVA: 0x0000DF68 File Offset: 0x0000C368
			private void CreatePacker(Type t, MethodBuilder mb)
			{
				ILGenerator ilgenerator = mb.GetILGenerator();
				ILGenerator il = ilgenerator;
				if (CompiledPacker.MethodBuilderPacker.<>f__mg$cache0 == null)
				{
					CompiledPacker.MethodBuilderPacker.<>f__mg$cache0 = new Func<Type, MemberInfo[]>(CompiledPacker.MethodBuilderPacker.LookupMembers);
				}
				Func<Type, MemberInfo[]> targetMemberSelector = CompiledPacker.MethodBuilderPacker.<>f__mg$cache0;
				if (CompiledPacker.MethodBuilderPacker.<>f__mg$cache1 == null)
				{
					CompiledPacker.MethodBuilderPacker.<>f__mg$cache1 = new Func<MemberInfo, string>(CompiledPacker.MethodBuilderPacker.FormatMemberName);
				}
				PackILGenerator.EmitPackCode(t, mb, il, targetMemberSelector, CompiledPacker.MethodBuilderPacker.<>f__mg$cache1, new Func<Type, MethodInfo>(this.LookupPackMethod));
			}

			// Token: 0x06000575 RID: 1397 RVA: 0x0000DFCC File Offset: 0x0000C3CC
			private void CreateUnpacker(Type t, MethodBuilder mb)
			{
				ILGenerator ilgenerator = mb.GetILGenerator();
				ILGenerator il = ilgenerator;
				if (CompiledPacker.MethodBuilderPacker.<>f__mg$cache2 == null)
				{
					CompiledPacker.MethodBuilderPacker.<>f__mg$cache2 = new Func<Type, MemberInfo[]>(CompiledPacker.MethodBuilderPacker.LookupMembers);
				}
				Func<Type, MemberInfo[]> targetMemberSelector = CompiledPacker.MethodBuilderPacker.<>f__mg$cache2;
				if (CompiledPacker.MethodBuilderPacker.<>f__mg$cache3 == null)
				{
					CompiledPacker.MethodBuilderPacker.<>f__mg$cache3 = new Func<MemberInfo, string>(CompiledPacker.MethodBuilderPacker.FormatMemberName);
				}
				Func<MemberInfo, string> memberNameFormatter = CompiledPacker.MethodBuilderPacker.<>f__mg$cache3;
				Func<Type, MethodInfo> lookupUnpackMethod = new Func<Type, MethodInfo>(this.LookupUnpackMethod);
				if (CompiledPacker.MethodBuilderPacker.<>f__mg$cache4 == null)
				{
					CompiledPacker.MethodBuilderPacker.<>f__mg$cache4 = new Func<Type, IDictionary<string, int>>(CompiledPacker.MethodBuilderPacker.LookupMemberMapping);
				}
				PackILGenerator.EmitUnpackCode(t, mb, il, targetMemberSelector, memberNameFormatter, lookupUnpackMethod, CompiledPacker.MethodBuilderPacker.<>f__mg$cache4, CompiledPacker.MethodBuilderPacker.LookupMemberMappingMethod);
			}

			// Token: 0x06000576 RID: 1398 RVA: 0x0000E050 File Offset: 0x0000C450
			private MethodInfo ToCallableMethodInfo(Type t, TypeBuilder tb, bool isPacker)
			{
				Type type = tb.CreateType();
				MethodInfo method = type.GetMethod((!isPacker) ? "Unpack" : "Pack", BindingFlags.Static | BindingFlags.Public);
				if (isPacker)
				{
					this._packMethods[t] = method;
				}
				else
				{
					this._unpackMethods[t] = method;
				}
				return method;
			}

			// Token: 0x06000577 RID: 1399 RVA: 0x0000E0A8 File Offset: 0x0000C4A8
			private MethodInfo LookupPackMethod(Type t)
			{
				MethodInfo result;
				if (this._packMethods.TryGetValue(t, out result))
				{
					return result;
				}
				TypeBuilder tb;
				MethodBuilder methodBuilder;
				CompiledPacker.MethodBuilderPacker.CreatePackMethodBuilder(t, out tb, out methodBuilder);
				this._packMethods.Add(t, methodBuilder);
				this.CreatePacker(t, methodBuilder);
				return this.ToCallableMethodInfo(t, tb, true);
			}

			// Token: 0x06000578 RID: 1400 RVA: 0x0000E0F4 File Offset: 0x0000C4F4
			private MethodInfo LookupUnpackMethod(Type t)
			{
				MethodInfo result;
				if (this._unpackMethods.TryGetValue(t, out result))
				{
					return result;
				}
				TypeBuilder tb;
				MethodBuilder methodBuilder;
				CompiledPacker.MethodBuilderPacker.CreateUnpackMethodBuilder(t, out tb, out methodBuilder);
				this._unpackMethods.Add(t, methodBuilder);
				this.CreateUnpacker(t, methodBuilder);
				return this.ToCallableMethodInfo(t, tb, false);
			}

			// Token: 0x06000579 RID: 1401 RVA: 0x0000E13E File Offset: 0x0000C53E
			private static string FormatMemberName(MemberInfo m)
			{
				return m.Name;
			}

			// Token: 0x0600057A RID: 1402 RVA: 0x0000E148 File Offset: 0x0000C548
			private static MemberInfo[] LookupMembers(Type t)
			{
				BindingFlags bindingAttr = BindingFlags.Instance | BindingFlags.Public;
				List<MemberInfo> list = new List<MemberInfo>();
				list.AddRange(t.GetFields(bindingAttr));
				return list.ToArray();
			}

			// Token: 0x0600057B RID: 1403 RVA: 0x0000E174 File Offset: 0x0000C574
			private static void CreatePackMethodBuilder(Type t, out TypeBuilder tb, out MethodBuilder mb)
			{
				tb = CompiledPacker.MethodBuilderPacker.DynamicModuleBuilder.DefineType(t.Name + "PackerType", TypeAttributes.Public);
				mb = tb.DefineMethod("Pack", MethodAttributes.FamANDAssem | MethodAttributes.Family | MethodAttributes.Static, typeof(void), new Type[]
				{
					typeof(MsgPackWriter),
					t
				});
			}

			// Token: 0x0600057C RID: 1404 RVA: 0x0000E1D0 File Offset: 0x0000C5D0
			private static void CreateUnpackMethodBuilder(Type t, out TypeBuilder tb, out MethodBuilder mb)
			{
				tb = CompiledPacker.MethodBuilderPacker.DynamicModuleBuilder.DefineType(t.Name + "UnpackerType", TypeAttributes.Public);
				mb = tb.DefineMethod("Unpack", MethodAttributes.FamANDAssem | MethodAttributes.Family | MethodAttributes.Static, t, new Type[]
				{
					typeof(MsgPackReader)
				});
			}

			// Token: 0x0600057D RID: 1405 RVA: 0x0000E220 File Offset: 0x0000C620
			internal static IDictionary<string, int> LookupMemberMapping(Type t)
			{
				object unpackMemberMappings = CompiledPacker.MethodBuilderPacker.UnpackMemberMappings;
				IDictionary<string, int> dictionary;
				lock (unpackMemberMappings)
				{
					if (!CompiledPacker.MethodBuilderPacker.UnpackMemberMappings.TryGetValue(t, out dictionary))
					{
						dictionary = new Dictionary<string, int>();
						CompiledPacker.MethodBuilderPacker.UnpackMemberMappings.Add(t, dictionary);
					}
				}
				return dictionary;
			}

			// Token: 0x04000379 RID: 889
			public const string AssemblyName = "MessagePackInternalAssembly";

			// Token: 0x0400037A RID: 890
			private static AssemblyName DynamicAsmName;

			// Token: 0x0400037B RID: 891
			private static AssemblyBuilder DynamicAsmBuilder;

			// Token: 0x0400037C RID: 892
			private static ModuleBuilder DynamicModuleBuilder;

			// Token: 0x0400037D RID: 893
			private static MethodInfo LookupMemberMappingMethod;

			// Token: 0x0400037E RID: 894
			private static Dictionary<Type, IDictionary<string, int>> UnpackMemberMappings = new Dictionary<Type, IDictionary<string, int>>();

			// Token: 0x0400037F RID: 895
			[CompilerGenerated]
			private static Func<Type, MemberInfo[]> <>f__mg$cache0;

			// Token: 0x04000380 RID: 896
			[CompilerGenerated]
			private static Func<MemberInfo, string> <>f__mg$cache1;

			// Token: 0x04000381 RID: 897
			[CompilerGenerated]
			private static Func<Type, MemberInfo[]> <>f__mg$cache2;

			// Token: 0x04000382 RID: 898
			[CompilerGenerated]
			private static Func<MemberInfo, string> <>f__mg$cache3;

			// Token: 0x04000383 RID: 899
			[CompilerGenerated]
			private static Func<Type, IDictionary<string, int>> <>f__mg$cache4;
		}

		// Token: 0x020000C1 RID: 193
		internal static class DefaultPackMethods
		{
			// Token: 0x0600057E RID: 1406 RVA: 0x0000E27C File Offset: 0x0000C67C
			public static void Register(Dictionary<Type, MethodInfo> packMethods, Dictionary<Type, MethodInfo> unpackMethods)
			{
				CompiledPacker.DefaultPackMethods.RegisterPackMethods(packMethods);
				CompiledPacker.DefaultPackMethods.RegisterUnpackMethods(unpackMethods);
			}

			// Token: 0x0600057F RID: 1407 RVA: 0x0000E28C File Offset: 0x0000C68C
			private static void RegisterPackMethods(Dictionary<Type, MethodInfo> packMethods)
			{
				Type typeFromHandle = typeof(CompiledPacker.DefaultPackMethods);
				MethodInfo[] methods = typeFromHandle.GetMethods(BindingFlags.Static | BindingFlags.NonPublic);
				string text = "Pack";
				for (int i = 0; i < methods.Length; i++)
				{
					if (text.Equals(methods[i].Name))
					{
						ParameterInfo[] parameters = methods[i].GetParameters();
						if (parameters.Length == 2 && parameters[0].ParameterType == typeof(MsgPackWriter))
						{
							packMethods.Add(parameters[1].ParameterType, methods[i]);
						}
					}
				}
			}

			// Token: 0x06000580 RID: 1408 RVA: 0x0000E322 File Offset: 0x0000C722
			internal static void Pack(MsgPackWriter writer, string x)
			{
				if (x == null)
				{
					writer.WriteNil();
				}
				else
				{
					writer.Write(x, false);
				}
			}

			// Token: 0x06000581 RID: 1409 RVA: 0x0000E340 File Offset: 0x0000C740
			private static void RegisterUnpackMethods(Dictionary<Type, MethodInfo> unpackMethods)
			{
				BindingFlags bindingAttr = BindingFlags.Static | BindingFlags.NonPublic;
				Type typeFromHandle = typeof(CompiledPacker.DefaultPackMethods);
				MethodInfo method = typeFromHandle.GetMethod("Unpack_Signed", bindingAttr);
				unpackMethods.Add(typeof(sbyte), method);
				unpackMethods.Add(typeof(short), method);
				unpackMethods.Add(typeof(int), method);
				method = typeFromHandle.GetMethod("Unpack_Signed64", bindingAttr);
				unpackMethods.Add(typeof(long), method);
				method = typeFromHandle.GetMethod("Unpack_Unsigned", bindingAttr);
				unpackMethods.Add(typeof(byte), method);
				unpackMethods.Add(typeof(ushort), method);
				unpackMethods.Add(typeof(char), method);
				unpackMethods.Add(typeof(uint), method);
				method = typeFromHandle.GetMethod("Unpack_Unsigned64", bindingAttr);
				unpackMethods.Add(typeof(ulong), method);
				method = typeFromHandle.GetMethod("Unpack_Boolean", bindingAttr);
				unpackMethods.Add(typeof(bool), method);
				method = typeFromHandle.GetMethod("Unpack_Float", bindingAttr);
				unpackMethods.Add(typeof(float), method);
				method = typeFromHandle.GetMethod("Unpack_Double", bindingAttr);
				unpackMethods.Add(typeof(double), method);
				method = typeFromHandle.GetMethod("Unpack_String", bindingAttr);
				unpackMethods.Add(typeof(string), method);
			}

			// Token: 0x06000582 RID: 1410 RVA: 0x0000E4A0 File Offset: 0x0000C8A0
			internal static int Unpack_Signed(MsgPackReader reader)
			{
				if (!reader.Read() || !reader.IsSigned())
				{
					CompiledPacker.DefaultPackMethods.UnpackFailed();
				}
				return reader.ValueSigned;
			}

			// Token: 0x06000583 RID: 1411 RVA: 0x0000E4C3 File Offset: 0x0000C8C3
			internal static long Unpack_Signed64(MsgPackReader reader)
			{
				if (!reader.Read())
				{
					CompiledPacker.DefaultPackMethods.UnpackFailed();
				}
				if (reader.IsSigned())
				{
					return (long)reader.ValueSigned;
				}
				if (reader.IsSigned64())
				{
					return reader.ValueSigned64;
				}
				CompiledPacker.DefaultPackMethods.UnpackFailed();
				return 0L;
			}

			// Token: 0x06000584 RID: 1412 RVA: 0x0000E501 File Offset: 0x0000C901
			internal static uint Unpack_Unsigned(MsgPackReader reader)
			{
				if (!reader.Read() || !reader.IsUnsigned())
				{
					CompiledPacker.DefaultPackMethods.UnpackFailed();
				}
				return reader.ValueUnsigned;
			}

			// Token: 0x06000585 RID: 1413 RVA: 0x0000E524 File Offset: 0x0000C924
			internal static ulong Unpack_Unsigned64(MsgPackReader reader)
			{
				if (!reader.Read())
				{
					CompiledPacker.DefaultPackMethods.UnpackFailed();
				}
				if (reader.IsUnsigned())
				{
					return (ulong)reader.ValueUnsigned;
				}
				if (reader.IsUnsigned64())
				{
					return reader.ValueUnsigned64;
				}
				CompiledPacker.DefaultPackMethods.UnpackFailed();
				return 0UL;
			}

			// Token: 0x06000586 RID: 1414 RVA: 0x0000E562 File Offset: 0x0000C962
			internal static bool Unpack_Boolean(MsgPackReader reader)
			{
				if (!reader.Read() || !reader.IsBoolean())
				{
					CompiledPacker.DefaultPackMethods.UnpackFailed();
				}
				return reader.ValueBoolean;
			}

			// Token: 0x06000587 RID: 1415 RVA: 0x0000E585 File Offset: 0x0000C985
			internal static float Unpack_Float(MsgPackReader reader)
			{
				if (!reader.Read() || reader.Type != TypePrefixes.Float)
				{
					CompiledPacker.DefaultPackMethods.UnpackFailed();
				}
				return reader.ValueFloat;
			}

			// Token: 0x06000588 RID: 1416 RVA: 0x0000E5AD File Offset: 0x0000C9AD
			internal static double Unpack_Double(MsgPackReader reader)
			{
				if (!reader.Read() || reader.Type != TypePrefixes.Double)
				{
					CompiledPacker.DefaultPackMethods.UnpackFailed();
				}
				return reader.ValueDouble;
			}

			// Token: 0x06000589 RID: 1417 RVA: 0x0000E5D5 File Offset: 0x0000C9D5
			internal static string Unpack_String(MsgPackReader reader)
			{
				if (!reader.Read() || !reader.IsRaw())
				{
					CompiledPacker.DefaultPackMethods.UnpackFailed();
				}
				return reader.ReadRawString();
			}

			// Token: 0x0600058A RID: 1418 RVA: 0x0000E5F8 File Offset: 0x0000C9F8
			internal static void UnpackFailed()
			{
				throw new FormatException();
			}
		}
	}
}
