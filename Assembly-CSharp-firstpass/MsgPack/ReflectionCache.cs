﻿using System;
using System.Collections.Generic;

namespace MsgPack
{
	// Token: 0x020000CB RID: 203
	public static class ReflectionCache
	{
		// Token: 0x060005F6 RID: 1526 RVA: 0x00011064 File Offset: 0x0000F464
		public static ReflectionCacheEntry Lookup(Type type)
		{
			object cache = ReflectionCache._cache;
			ReflectionCacheEntry reflectionCacheEntry;
			lock (cache)
			{
				if (ReflectionCache._cache.TryGetValue(type, out reflectionCacheEntry))
				{
					return reflectionCacheEntry;
				}
			}
			reflectionCacheEntry = new ReflectionCacheEntry(type);
			object cache2 = ReflectionCache._cache;
			lock (cache2)
			{
				ReflectionCache._cache[type] = reflectionCacheEntry;
			}
			return reflectionCacheEntry;
		}

		// Token: 0x060005F7 RID: 1527 RVA: 0x000110EC File Offset: 0x0000F4EC
		public static void RemoveCache(Type type)
		{
			object cache = ReflectionCache._cache;
			lock (cache)
			{
				ReflectionCache._cache.Remove(type);
			}
		}

		// Token: 0x060005F8 RID: 1528 RVA: 0x00011130 File Offset: 0x0000F530
		public static void Clear()
		{
			object cache = ReflectionCache._cache;
			lock (cache)
			{
				ReflectionCache._cache.Clear();
			}
		}

		// Token: 0x040003A0 RID: 928
		private static Dictionary<Type, ReflectionCacheEntry> _cache = new Dictionary<Type, ReflectionCacheEntry>();
	}
}
