﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace MsgPack
{
	// Token: 0x020000BC RID: 188
	public class BoxingPacker
	{
		// Token: 0x06000548 RID: 1352 RVA: 0x0000D19C File Offset: 0x0000B59C
		public void Pack(Stream strm, object o)
		{
			MsgPackWriter writer = new MsgPackWriter(strm);
			this.Pack(writer, o);
		}

		// Token: 0x06000549 RID: 1353 RVA: 0x0000D1B8 File Offset: 0x0000B5B8
		public byte[] Pack(object o)
		{
			byte[] result;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				this.Pack(memoryStream, o);
				result = memoryStream.ToArray();
			}
			return result;
		}

		// Token: 0x0600054A RID: 1354 RVA: 0x0000D200 File Offset: 0x0000B600
		private void Pack(MsgPackWriter writer, object o)
		{
			if (o == null)
			{
				writer.WriteNil();
				return;
			}
			Type type = o.GetType();
			if (type.IsPrimitive)
			{
				if (type.Equals(typeof(int)))
				{
					writer.Write((int)o);
				}
				else if (type.Equals(typeof(uint)))
				{
					writer.Write((uint)o);
				}
				else if (type.Equals(typeof(float)))
				{
					writer.Write((float)o);
				}
				else if (type.Equals(typeof(double)))
				{
					writer.Write((double)o);
				}
				else if (type.Equals(typeof(long)))
				{
					writer.Write((long)o);
				}
				else if (type.Equals(typeof(ulong)))
				{
					writer.Write((ulong)o);
				}
				else if (type.Equals(typeof(bool)))
				{
					writer.Write((bool)o);
				}
				else if (type.Equals(typeof(byte)))
				{
					writer.Write((byte)o);
				}
				else if (type.Equals(typeof(sbyte)))
				{
					writer.Write((sbyte)o);
				}
				else if (type.Equals(typeof(short)))
				{
					writer.Write((short)o);
				}
				else
				{
					if (!type.Equals(typeof(ushort)))
					{
						throw new NotSupportedException();
					}
					writer.Write((ushort)o);
				}
				return;
			}
			IDictionary dictionary = o as IDictionary;
			if (dictionary != null)
			{
				writer.WriteMapHeader(dictionary.Count);
				IDictionaryEnumerator enumerator = dictionary.GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
						this.Pack(writer, dictionaryEntry.Key);
						this.Pack(writer, dictionaryEntry.Value);
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				return;
			}
			if (!type.IsArray)
			{
				return;
			}
			Array array = (Array)o;
			Type elementType = type.GetElementType();
			if (elementType.IsGenericType && elementType.GetGenericTypeDefinition().Equals(BoxingPacker.KeyValuePairDefinitionType))
			{
				PropertyInfo property = elementType.GetProperty("Key");
				PropertyInfo property2 = elementType.GetProperty("Value");
				writer.WriteMapHeader(array.Length);
				for (int i = 0; i < array.Length; i++)
				{
					object value = array.GetValue(i);
					this.Pack(writer, property.GetValue(value, null));
					this.Pack(writer, property2.GetValue(value, null));
				}
				return;
			}
			writer.WriteArrayHeader(array.Length);
			for (int j = 0; j < array.Length; j++)
			{
				this.Pack(writer, array.GetValue(j));
			}
		}

		// Token: 0x0600054B RID: 1355 RVA: 0x0000D544 File Offset: 0x0000B944
		public object Unpack(Stream strm)
		{
			MsgPackReader reader = new MsgPackReader(strm);
			return this.Unpack(reader);
		}

		// Token: 0x0600054C RID: 1356 RVA: 0x0000D560 File Offset: 0x0000B960
		public object Unpack(byte[] buf, int offset, int size)
		{
			object result;
			using (MemoryStream memoryStream = new MemoryStream(buf, offset, size))
			{
				result = this.Unpack(memoryStream);
			}
			return result;
		}

		// Token: 0x0600054D RID: 1357 RVA: 0x0000D5A4 File Offset: 0x0000B9A4
		public object Unpack(byte[] buf)
		{
			return this.Unpack(buf, 0, buf.Length);
		}

		// Token: 0x0600054E RID: 1358 RVA: 0x0000D5B4 File Offset: 0x0000B9B4
		private object Unpack(MsgPackReader reader)
		{
			if (!reader.Read())
			{
				throw new FormatException();
			}
			TypePrefixes type = reader.Type;
			switch (type)
			{
			case TypePrefixes.Float:
				return reader.ValueFloat;
			case TypePrefixes.Double:
				return reader.ValueDouble;
			case TypePrefixes.UInt8:
			case TypePrefixes.UInt16:
			case TypePrefixes.UInt32:
				return reader.ValueUnsigned;
			case TypePrefixes.UInt64:
				return reader.ValueUnsigned64;
			case TypePrefixes.Int8:
			case TypePrefixes.Int16:
			case TypePrefixes.Int32:
			case TypePrefixes.NegativeFixNum:
				break;
			case TypePrefixes.Int64:
				return reader.ValueSigned64;
			default:
				switch (type)
				{
				case TypePrefixes.Nil:
					return null;
				default:
					if (type != TypePrefixes.PositiveFixNum)
					{
						if (type == TypePrefixes.FixMap)
						{
							goto IL_168;
						}
						if (type == TypePrefixes.FixArray)
						{
							goto IL_13B;
						}
						if (type != TypePrefixes.FixRaw)
						{
							throw new FormatException();
						}
						goto IL_120;
					}
					break;
				case TypePrefixes.False:
					return false;
				case TypePrefixes.True:
					return true;
				}
				break;
			case TypePrefixes.Raw16:
			case TypePrefixes.Raw32:
				goto IL_120;
			case TypePrefixes.Array16:
			case TypePrefixes.Array32:
				goto IL_13B;
			case TypePrefixes.Map16:
			case TypePrefixes.Map32:
				goto IL_168;
			}
			return reader.ValueSigned;
			IL_120:
			byte[] array = new byte[reader.Length];
			reader.ReadValueRaw(array, 0, array.Length);
			return array;
			IL_13B:
			object[] array2 = new object[reader.Length];
			for (int i = 0; i < array2.Length; i++)
			{
				array2[i] = this.Unpack(reader);
			}
			return array2;
			IL_168:
			IDictionary<object, object> dictionary = new Dictionary<object, object>((int)reader.Length);
			int length = (int)reader.Length;
			for (int j = 0; j < length; j++)
			{
				object key = this.Unpack(reader);
				object value = this.Unpack(reader);
				dictionary.Add(key, value);
			}
			return dictionary;
		}

		// Token: 0x04000369 RID: 873
		private static Type KeyValuePairDefinitionType = typeof(KeyValuePair<object, object>).GetGenericTypeDefinition();
	}
}
