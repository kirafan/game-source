﻿using System;
using System.IO;
using System.Text;

namespace MsgPack
{
	// Token: 0x020000C7 RID: 199
	public class MsgPackWriter
	{
		// Token: 0x060005C7 RID: 1479 RVA: 0x0000FE9B File Offset: 0x0000E29B
		public MsgPackWriter(Stream strm)
		{
			this._strm = strm;
		}

		// Token: 0x060005C8 RID: 1480 RVA: 0x0000FED4 File Offset: 0x0000E2D4
		public void Write(byte x)
		{
			if (x < 128)
			{
				this._strm.WriteByte(x);
			}
			else
			{
				byte[] tmp = this._tmp;
				tmp[0] = 204;
				tmp[1] = x;
				this._strm.Write(tmp, 0, 2);
			}
		}

		// Token: 0x060005C9 RID: 1481 RVA: 0x0000FF20 File Offset: 0x0000E320
		public void Write(ushort x)
		{
			if (x < 256)
			{
				this.Write((byte)x);
			}
			else
			{
				byte[] tmp = this._tmp;
				tmp[0] = 205;
				tmp[1] = (byte)(x >> 8);
				tmp[2] = (byte)x;
				this._strm.Write(tmp, 0, 3);
			}
		}

		// Token: 0x060005CA RID: 1482 RVA: 0x0000FF6E File Offset: 0x0000E36E
		public void Write(char x)
		{
			this.Write((ushort)x);
		}

		// Token: 0x060005CB RID: 1483 RVA: 0x0000FF78 File Offset: 0x0000E378
		public void Write(uint x)
		{
			if (x < 65536U)
			{
				this.Write((ushort)x);
			}
			else
			{
				byte[] tmp = this._tmp;
				tmp[0] = 206;
				tmp[1] = (byte)(x >> 24);
				tmp[2] = (byte)(x >> 16);
				tmp[3] = (byte)(x >> 8);
				tmp[4] = (byte)x;
				this._strm.Write(tmp, 0, 5);
			}
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x0000FFD8 File Offset: 0x0000E3D8
		public void Write(ulong x)
		{
			if (x < 4294967296UL)
			{
				this.Write((uint)x);
			}
			else
			{
				byte[] tmp = this._tmp;
				tmp[0] = 207;
				tmp[1] = (byte)(x >> 56);
				tmp[2] = (byte)(x >> 48);
				tmp[3] = (byte)(x >> 40);
				tmp[4] = (byte)(x >> 32);
				tmp[5] = (byte)(x >> 24);
				tmp[6] = (byte)(x >> 16);
				tmp[7] = (byte)(x >> 8);
				tmp[8] = (byte)x;
				this._strm.Write(tmp, 0, 9);
			}
		}

		// Token: 0x060005CD RID: 1485 RVA: 0x0001005C File Offset: 0x0000E45C
		public void Write(sbyte x)
		{
			if ((int)x >= -32 && (int)x <= -1)
			{
				this._strm.WriteByte(224 | (byte)x);
			}
			else if ((int)x >= 0 && (int)x <= 127)
			{
				this._strm.WriteByte((byte)x);
			}
			else
			{
				byte[] tmp = this._tmp;
				tmp[0] = 208;
				tmp[1] = (byte)x;
				this._strm.Write(tmp, 0, 2);
			}
		}

		// Token: 0x060005CE RID: 1486 RVA: 0x000100D8 File Offset: 0x0000E4D8
		public void Write(short x)
		{
			if (x >= -128 && x <= 127)
			{
				this.Write((sbyte)x);
			}
			else
			{
				byte[] tmp = this._tmp;
				tmp[0] = 209;
				tmp[1] = (byte)(x >> 8);
				tmp[2] = (byte)x;
				this._strm.Write(tmp, 0, 3);
			}
		}

		// Token: 0x060005CF RID: 1487 RVA: 0x0001012C File Offset: 0x0000E52C
		public void Write(int x)
		{
			if (x >= -32768 && x <= 32767)
			{
				this.Write((short)x);
			}
			else
			{
				byte[] tmp = this._tmp;
				tmp[0] = 210;
				tmp[1] = (byte)(x >> 24);
				tmp[2] = (byte)(x >> 16);
				tmp[3] = (byte)(x >> 8);
				tmp[4] = (byte)x;
				this._strm.Write(tmp, 0, 5);
			}
		}

		// Token: 0x060005D0 RID: 1488 RVA: 0x00010198 File Offset: 0x0000E598
		public void Write(long x)
		{
			if (x >= -2147483648L && x <= 2147483647L)
			{
				this.Write((int)x);
			}
			else
			{
				byte[] tmp = this._tmp;
				tmp[0] = 211;
				tmp[1] = (byte)(x >> 56);
				tmp[2] = (byte)(x >> 48);
				tmp[3] = (byte)(x >> 40);
				tmp[4] = (byte)(x >> 32);
				tmp[5] = (byte)(x >> 24);
				tmp[6] = (byte)(x >> 16);
				tmp[7] = (byte)(x >> 8);
				tmp[8] = (byte)x;
				this._strm.Write(tmp, 0, 9);
			}
		}

		// Token: 0x060005D1 RID: 1489 RVA: 0x00010224 File Offset: 0x0000E624
		public void WriteNil()
		{
			this._strm.WriteByte(192);
		}

		// Token: 0x060005D2 RID: 1490 RVA: 0x00010236 File Offset: 0x0000E636
		public void Write(bool x)
		{
			this._strm.WriteByte((!x) ? 194 : 195);
		}

		// Token: 0x060005D3 RID: 1491 RVA: 0x0001025C File Offset: 0x0000E65C
		public void Write(float x)
		{
			byte[] bytes = BitConverter.GetBytes(x);
			byte[] tmp = this._tmp;
			tmp[0] = 202;
			if (BitConverter.IsLittleEndian)
			{
				tmp[1] = bytes[3];
				tmp[2] = bytes[2];
				tmp[3] = bytes[1];
				tmp[4] = bytes[0];
			}
			else
			{
				tmp[1] = bytes[0];
				tmp[2] = bytes[1];
				tmp[3] = bytes[2];
				tmp[4] = bytes[3];
			}
			this._strm.Write(tmp, 0, 5);
		}

		// Token: 0x060005D4 RID: 1492 RVA: 0x000102CC File Offset: 0x0000E6CC
		public void Write(double x)
		{
			byte[] bytes = BitConverter.GetBytes(x);
			byte[] tmp = this._tmp;
			tmp[0] = 203;
			if (BitConverter.IsLittleEndian)
			{
				tmp[1] = bytes[7];
				tmp[2] = bytes[6];
				tmp[3] = bytes[5];
				tmp[4] = bytes[4];
				tmp[5] = bytes[3];
				tmp[6] = bytes[2];
				tmp[7] = bytes[1];
				tmp[8] = bytes[0];
			}
			else
			{
				tmp[1] = bytes[0];
				tmp[2] = bytes[1];
				tmp[3] = bytes[2];
				tmp[4] = bytes[3];
				tmp[5] = bytes[4];
				tmp[6] = bytes[5];
				tmp[7] = bytes[6];
				tmp[8] = bytes[7];
			}
			this._strm.Write(tmp, 0, 9);
		}

		// Token: 0x060005D5 RID: 1493 RVA: 0x0001036D File Offset: 0x0000E76D
		public void Write(byte[] bytes)
		{
			this.WriteRawHeader(bytes.Length);
			this._strm.Write(bytes, 0, bytes.Length);
		}

		// Token: 0x060005D6 RID: 1494 RVA: 0x00010388 File Offset: 0x0000E788
		public void WriteRawHeader(int N)
		{
			this.WriteLengthHeader(N, 32, 160, 218, 219);
		}

		// Token: 0x060005D7 RID: 1495 RVA: 0x000103A2 File Offset: 0x0000E7A2
		public void WriteArrayHeader(int N)
		{
			this.WriteLengthHeader(N, 16, 144, 220, 221);
		}

		// Token: 0x060005D8 RID: 1496 RVA: 0x000103BC File Offset: 0x0000E7BC
		public void WriteMapHeader(int N)
		{
			this.WriteLengthHeader(N, 16, 128, 222, 223);
		}

		// Token: 0x060005D9 RID: 1497 RVA: 0x000103D8 File Offset: 0x0000E7D8
		private void WriteLengthHeader(int N, int fix_length, byte fix_prefix, byte len16bit_prefix, byte len32bit_prefix)
		{
			if (N < fix_length)
			{
				this._strm.WriteByte((byte)((int)fix_prefix | N));
			}
			else
			{
				byte[] tmp = this._tmp;
				int count;
				if (N < 65536)
				{
					tmp[0] = len16bit_prefix;
					tmp[1] = (byte)(N >> 8);
					tmp[2] = (byte)N;
					count = 3;
				}
				else
				{
					tmp[0] = len32bit_prefix;
					tmp[1] = (byte)(N >> 24);
					tmp[2] = (byte)(N >> 16);
					tmp[3] = (byte)(N >> 8);
					tmp[4] = (byte)N;
					count = 5;
				}
				this._strm.Write(tmp, 0, count);
			}
		}

		// Token: 0x060005DA RID: 1498 RVA: 0x0001045B File Offset: 0x0000E85B
		public void Write(string x)
		{
			this.Write(x, false);
		}

		// Token: 0x060005DB RID: 1499 RVA: 0x00010465 File Offset: 0x0000E865
		public void Write(string x, bool highProbAscii)
		{
			this.Write(x, this._buf, highProbAscii);
		}

		// Token: 0x060005DC RID: 1500 RVA: 0x00010475 File Offset: 0x0000E875
		public void Write(string x, byte[] buf)
		{
			this.Write(x, buf, false);
		}

		// Token: 0x060005DD RID: 1501 RVA: 0x00010480 File Offset: 0x0000E880
		public void Write(string x, byte[] buf, bool highProbAscii)
		{
			Encoder encoder = this._encoder;
			char[] chars = x.ToCharArray();
			if (highProbAscii && x.Length <= buf.Length)
			{
				bool flag = true;
				for (int i = 0; i < x.Length; i++)
				{
					int num = (int)x[i];
					if (num > 127)
					{
						flag = false;
						break;
					}
					buf[i] = (byte)num;
				}
				if (flag)
				{
					this.WriteRawHeader(x.Length);
					this._strm.Write(buf, 0, x.Length);
					return;
				}
			}
			this.WriteRawHeader(encoder.GetByteCount(chars, 0, x.Length, true));
			int num2 = x.Length;
			bool flag2 = true;
			int num3 = 0;
			while (num2 > 0 || !flag2)
			{
				int num4;
				int count;
				encoder.Convert(chars, num3, num2, buf, 0, buf.Length, false, out num4, out count, out flag2);
				this._strm.Write(buf, 0, count);
				num2 -= num4;
				num3 += num4;
			}
		}

		// Token: 0x04000397 RID: 919
		private Stream _strm;

		// Token: 0x04000398 RID: 920
		private Encoder _encoder = Encoding.UTF8.GetEncoder();

		// Token: 0x04000399 RID: 921
		private byte[] _tmp = new byte[9];

		// Token: 0x0400039A RID: 922
		private byte[] _buf = new byte[64];
	}
}
