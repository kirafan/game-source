﻿using System;

namespace MsgPack.Compiler
{
	// Token: 0x020000C5 RID: 197
	public enum VariableType
	{
		// Token: 0x04000387 RID: 903
		Local,
		// Token: 0x04000388 RID: 904
		Arg
	}
}
