﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace MsgPack.Compiler
{
	// Token: 0x020000C2 RID: 194
	public static class EmitExtensions
	{
		// Token: 0x0600058B RID: 1419 RVA: 0x0000E600 File Offset: 0x0000CA00
		public static void EmitLd(this ILGenerator il, Variable v)
		{
			VariableType varType = v.VarType;
			if (varType != VariableType.Arg)
			{
				if (varType != VariableType.Local)
				{
					throw new ArgumentException();
				}
				il.EmitLdloc(v);
			}
			else
			{
				il.EmitLdarg(v);
			}
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x0000E644 File Offset: 0x0000CA44
		public static void EmitLd(this ILGenerator il, params Variable[] list)
		{
			for (int i = 0; i < list.Length; i++)
			{
				il.EmitLd(list[i]);
			}
		}

		// Token: 0x0600058D RID: 1421 RVA: 0x0000E670 File Offset: 0x0000CA70
		public static void EmitLdarg(this ILGenerator il, Variable v)
		{
			if (v.VarType != VariableType.Arg)
			{
				throw new ArgumentException();
			}
			switch (v.Index)
			{
			case 0:
				il.Emit(OpCodes.Ldarg_0);
				return;
			case 1:
				il.Emit(OpCodes.Ldarg_1);
				return;
			case 2:
				il.Emit(OpCodes.Ldarg_2);
				return;
			case 3:
				il.Emit(OpCodes.Ldarg_3);
				return;
			default:
				if (v.Index <= 255)
				{
					il.Emit(OpCodes.Ldarg_S, (byte)v.Index);
				}
				else
				{
					if (v.Index > 32767)
					{
						throw new FormatException();
					}
					il.Emit(OpCodes.Ldarg, v.Index);
				}
				return;
			}
		}

		// Token: 0x0600058E RID: 1422 RVA: 0x0000E734 File Offset: 0x0000CB34
		public static void EmitLdloc(this ILGenerator il, Variable v)
		{
			if (v.VarType != VariableType.Local)
			{
				throw new ArgumentException();
			}
			switch (v.Index)
			{
			case 0:
				il.Emit(OpCodes.Ldloc_0);
				return;
			case 1:
				il.Emit(OpCodes.Ldloc_1);
				return;
			case 2:
				il.Emit(OpCodes.Ldloc_2);
				return;
			case 3:
				il.Emit(OpCodes.Ldloc_3);
				return;
			default:
				if (v.Index <= 255)
				{
					il.Emit(OpCodes.Ldloc_S, (byte)v.Index);
				}
				else
				{
					if (v.Index > 32767)
					{
						throw new FormatException();
					}
					il.Emit(OpCodes.Ldloc, v.Index);
				}
				return;
			}
		}

		// Token: 0x0600058F RID: 1423 RVA: 0x0000E7F8 File Offset: 0x0000CBF8
		public static void EmitSt(this ILGenerator il, Variable v)
		{
			VariableType varType = v.VarType;
			if (varType != VariableType.Arg)
			{
				if (varType != VariableType.Local)
				{
					throw new ArgumentException();
				}
				il.EmitStloc(v);
			}
			else
			{
				il.EmitStarg(v);
			}
		}

		// Token: 0x06000590 RID: 1424 RVA: 0x0000E83C File Offset: 0x0000CC3C
		public static void EmitStarg(this ILGenerator il, Variable v)
		{
			if (v.VarType != VariableType.Arg)
			{
				throw new ArgumentException();
			}
			if (v.Index <= 255)
			{
				il.Emit(OpCodes.Starg_S, (byte)v.Index);
			}
			else
			{
				if (v.Index > 32767)
				{
					throw new FormatException();
				}
				il.Emit(OpCodes.Starg, v.Index);
			}
		}

		// Token: 0x06000591 RID: 1425 RVA: 0x0000E8B0 File Offset: 0x0000CCB0
		public static void EmitStloc(this ILGenerator il, Variable v)
		{
			if (v.VarType != VariableType.Local)
			{
				throw new ArgumentException();
			}
			switch (v.Index)
			{
			case 0:
				il.Emit(OpCodes.Stloc_0);
				return;
			case 1:
				il.Emit(OpCodes.Stloc_1);
				return;
			case 2:
				il.Emit(OpCodes.Stloc_2);
				return;
			case 3:
				il.Emit(OpCodes.Stloc_3);
				return;
			default:
				if (v.Index <= 255)
				{
					il.Emit(OpCodes.Stloc_S, (byte)v.Index);
				}
				else
				{
					if (v.Index > 32767)
					{
						throw new FormatException();
					}
					il.Emit(OpCodes.Stloc, v.Index);
				}
				return;
			}
		}

		// Token: 0x06000592 RID: 1426 RVA: 0x0000E974 File Offset: 0x0000CD74
		public static void EmitLdc(this ILGenerator il, int v)
		{
			switch (v + 1)
			{
			case 0:
				il.Emit(OpCodes.Ldc_I4_M1);
				return;
			case 1:
				il.Emit(OpCodes.Ldc_I4_0);
				return;
			case 2:
				il.Emit(OpCodes.Ldc_I4_1);
				return;
			case 3:
				il.Emit(OpCodes.Ldc_I4_2);
				return;
			case 4:
				il.Emit(OpCodes.Ldc_I4_3);
				return;
			case 5:
				il.Emit(OpCodes.Ldc_I4_4);
				return;
			case 6:
				il.Emit(OpCodes.Ldc_I4_5);
				return;
			case 7:
				il.Emit(OpCodes.Ldc_I4_6);
				return;
			case 8:
				il.Emit(OpCodes.Ldc_I4_7);
				return;
			case 9:
				il.Emit(OpCodes.Ldc_I4_8);
				return;
			default:
				if (v <= 127 && v >= -128)
				{
					il.Emit(OpCodes.Ldc_I4_S, (sbyte)v);
				}
				else
				{
					il.Emit(OpCodes.Ldc_I4, v);
				}
				return;
			}
		}

		// Token: 0x06000593 RID: 1427 RVA: 0x0000EA5C File Offset: 0x0000CE5C
		public static void EmitLd_False(this ILGenerator il)
		{
			il.Emit(OpCodes.Ldc_I4_1);
		}

		// Token: 0x06000594 RID: 1428 RVA: 0x0000EA69 File Offset: 0x0000CE69
		public static void EmitLd_True(this ILGenerator il)
		{
			il.Emit(OpCodes.Ldc_I4_1);
		}

		// Token: 0x06000595 RID: 1429 RVA: 0x0000EA76 File Offset: 0x0000CE76
		public static void EmitLdstr(this ILGenerator il, string v)
		{
			il.Emit(OpCodes.Ldstr, v);
		}

		// Token: 0x06000596 RID: 1430 RVA: 0x0000EA84 File Offset: 0x0000CE84
		public static void EmitLdMember(this ILGenerator il, MemberInfo m)
		{
			if (m.MemberType == MemberTypes.Field)
			{
				il.Emit(OpCodes.Ldfld, (FieldInfo)m);
			}
			else
			{
				if (m.MemberType != MemberTypes.Property)
				{
					throw new ArgumentException();
				}
				il.Emit(OpCodes.Callvirt, ((PropertyInfo)m).GetGetMethod(true));
			}
		}

		// Token: 0x06000597 RID: 1431 RVA: 0x0000EAE4 File Offset: 0x0000CEE4
		public static void EmitStMember(this ILGenerator il, MemberInfo m)
		{
			if (m.MemberType == MemberTypes.Field)
			{
				il.Emit(OpCodes.Stfld, (FieldInfo)m);
			}
			else
			{
				if (m.MemberType != MemberTypes.Property)
				{
					throw new ArgumentException();
				}
				il.Emit(OpCodes.Callvirt, ((PropertyInfo)m).GetSetMethod(true));
			}
		}
	}
}
