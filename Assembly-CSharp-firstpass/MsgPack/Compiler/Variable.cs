﻿using System;
using System.Reflection.Emit;

namespace MsgPack.Compiler
{
	// Token: 0x020000C4 RID: 196
	public class Variable
	{
		// Token: 0x060005A1 RID: 1441 RVA: 0x0000F646 File Offset: 0x0000DA46
		private Variable(VariableType type, int index)
		{
			this.VarType = type;
			this.Index = index;
		}

		// Token: 0x060005A2 RID: 1442 RVA: 0x0000F65C File Offset: 0x0000DA5C
		public static Variable CreateLocal(LocalBuilder local)
		{
			return new Variable(VariableType.Local, local.LocalIndex);
		}

		// Token: 0x060005A3 RID: 1443 RVA: 0x0000F66A File Offset: 0x0000DA6A
		public static Variable CreateArg(int idx)
		{
			return new Variable(VariableType.Arg, idx);
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060005A4 RID: 1444 RVA: 0x0000F673 File Offset: 0x0000DA73
		// (set) Token: 0x060005A5 RID: 1445 RVA: 0x0000F67B File Offset: 0x0000DA7B
		public VariableType VarType { get; set; }

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060005A6 RID: 1446 RVA: 0x0000F684 File Offset: 0x0000DA84
		// (set) Token: 0x060005A7 RID: 1447 RVA: 0x0000F68C File Offset: 0x0000DA8C
		public int Index { get; set; }
	}
}
