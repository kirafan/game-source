﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.Serialization;

namespace MsgPack.Compiler
{
	// Token: 0x020000C3 RID: 195
	internal static class PackILGenerator
	{
		// Token: 0x06000598 RID: 1432 RVA: 0x0000EB44 File Offset: 0x0000CF44
		public static void EmitPackCode(Type type, MethodInfo mi, ILGenerator il, Func<Type, MemberInfo[]> targetMemberSelector, Func<MemberInfo, string> memberNameFormatter, Func<Type, MethodInfo> lookupPackMethod)
		{
			if (type.IsPrimitive || type.IsInterface)
			{
				throw new NotSupportedException();
			}
			Variable variable = Variable.CreateArg(0);
			Variable variable2 = Variable.CreateArg(1);
			Variable var_loop = Variable.CreateLocal(il.DeclareLocal(typeof(int)));
			if (!type.IsValueType)
			{
				Label label = il.DefineLabel();
				il.EmitLd(variable2);
				il.Emit(OpCodes.Brtrue_S, label);
				il.EmitLd(variable);
				il.Emit(OpCodes.Call, typeof(MsgPackWriter).GetMethod("WriteNil", new Type[0]));
				il.Emit(OpCodes.Ret);
				il.MarkLabel(label);
			}
			if (type.IsArray)
			{
				PackILGenerator.EmitPackArrayCode(mi, il, type, variable, variable2, var_loop, lookupPackMethod);
			}
			else
			{
				MemberInfo[] array = targetMemberSelector(type);
				il.EmitLd(variable);
				il.EmitLdc(array.Length);
				il.Emit(OpCodes.Callvirt, typeof(MsgPackWriter).GetMethod("WriteMapHeader", new Type[]
				{
					typeof(int)
				}));
				foreach (MemberInfo memberInfo in array)
				{
					Type memberType = memberInfo.GetMemberType();
					il.EmitLd(variable);
					il.EmitLdstr(memberNameFormatter(memberInfo));
					il.EmitLd_True();
					il.Emit(OpCodes.Call, typeof(MsgPackWriter).GetMethod("Write", new Type[]
					{
						typeof(string),
						typeof(bool)
					}));
					PackILGenerator.EmitPackMemberValueCode(memberType, il, variable, variable2, memberInfo, null, type, mi, lookupPackMethod);
				}
			}
			il.Emit(OpCodes.Ret);
		}

		// Token: 0x06000599 RID: 1433 RVA: 0x0000ED00 File Offset: 0x0000D100
		private static void EmitPackArrayCode(MethodInfo mi, ILGenerator il, Type t, Variable var_writer, Variable var_obj, Variable var_loop, Func<Type, MethodInfo> lookupPackMethod)
		{
			Type elementType = t.GetElementType();
			il.EmitLd(new Variable[]
			{
				var_writer,
				var_obj
			});
			il.Emit(OpCodes.Ldlen);
			il.Emit(OpCodes.Call, typeof(MsgPackWriter).GetMethod("WriteArrayHeader", new Type[]
			{
				typeof(int)
			}));
			Label label = il.DefineLabel();
			Label label2 = il.DefineLabel();
			il.EmitLdc(0);
			il.EmitSt(var_loop);
			il.Emit(OpCodes.Br_S, label2);
			il.MarkLabel(label);
			PackILGenerator.EmitPackMemberValueCode(elementType, il, var_writer, var_obj, null, var_loop, t, mi, lookupPackMethod);
			il.EmitLd(var_loop);
			il.Emit(OpCodes.Ldc_I4_1);
			il.Emit(OpCodes.Add);
			il.EmitSt(var_loop);
			il.MarkLabel(label2);
			il.EmitLd(new Variable[]
			{
				var_loop,
				var_obj
			});
			il.Emit(OpCodes.Ldlen);
			il.Emit(OpCodes.Blt_S, label);
		}

		// Token: 0x0600059A RID: 1434 RVA: 0x0000EE04 File Offset: 0x0000D204
		private static void EmitPackMemberValueCode(Type type, ILGenerator il, Variable var_writer, Variable var_obj, MemberInfo m, Variable elementIdx, Type currentType, MethodInfo currentMethod, Func<Type, MethodInfo> lookupPackMethod)
		{
			il.EmitLd(new Variable[]
			{
				var_writer,
				var_obj
			});
			if (m != null)
			{
				il.EmitLdMember(m);
			}
			if (elementIdx != null)
			{
				il.EmitLd(elementIdx);
				il.Emit(OpCodes.Ldelem, type);
			}
			MethodInfo meth;
			if (type.IsPrimitive)
			{
				meth = typeof(MsgPackWriter).GetMethod("Write", new Type[]
				{
					type
				});
			}
			else if (currentType == type)
			{
				meth = currentMethod;
			}
			else
			{
				meth = lookupPackMethod(type);
			}
			il.Emit(OpCodes.Call, meth);
		}

		// Token: 0x0600059B RID: 1435 RVA: 0x0000EEA3 File Offset: 0x0000D2A3
		public static void EmitUnpackCode(Type type, MethodInfo mi, ILGenerator il, Func<Type, MemberInfo[]> targetMemberSelector, Func<MemberInfo, string> memberNameFormatter, Func<Type, MethodInfo> lookupUnpackMethod, Func<Type, IDictionary<string, int>> lookupMemberMapping, MethodInfo lookupMemberMappingMethod)
		{
			if (type.IsArray)
			{
				PackILGenerator.EmitUnpackArrayCode(type, mi, il, targetMemberSelector, memberNameFormatter, lookupUnpackMethod);
			}
			else
			{
				PackILGenerator.EmitUnpackMapCode(type, mi, il, targetMemberSelector, memberNameFormatter, lookupUnpackMethod, lookupMemberMapping, lookupMemberMappingMethod);
			}
		}

		// Token: 0x0600059C RID: 1436 RVA: 0x0000EED4 File Offset: 0x0000D2D4
		private static void EmitUnpackMapCode(Type type, MethodInfo mi, ILGenerator il, Func<Type, MemberInfo[]> targetMemberSelector, Func<MemberInfo, string> memberNameFormatter, Func<Type, MethodInfo> lookupUnpackMethod, Func<Type, IDictionary<string, int>> lookupMemberMapping, MethodInfo lookupMemberMappingMethod)
		{
			MethodInfo method = typeof(PackILGenerator).GetMethod("UnpackFailed", BindingFlags.Static | BindingFlags.NonPublic);
			MemberInfo[] array = targetMemberSelector(type);
			IDictionary<string, int> dictionary = lookupMemberMapping(type);
			for (int i = 0; i < array.Length; i++)
			{
				dictionary.Add(memberNameFormatter(array[i]), i);
			}
			Variable variable = Variable.CreateArg(0);
			Variable v = Variable.CreateLocal(il.DeclareLocal(type));
			Variable v2 = Variable.CreateLocal(il.DeclareLocal(typeof(int)));
			Variable v3 = Variable.CreateLocal(il.DeclareLocal(typeof(int)));
			Variable v4 = Variable.CreateLocal(il.DeclareLocal(typeof(IDictionary<string, int>)));
			Variable variable2 = Variable.CreateLocal(il.DeclareLocal(typeof(int)));
			Variable v5 = Variable.CreateLocal(il.DeclareLocal(typeof(Type)));
			PackILGenerator.EmitUnpackReadAndTypeCheckCode(il, variable, typeof(MsgPackReader).GetMethod("IsMap"), method, true);
			il.Emit(OpCodes.Ldtoken, type);
			il.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle"));
			il.EmitSt(v5);
			il.EmitLd(v5);
			il.Emit(OpCodes.Call, lookupMemberMappingMethod);
			il.EmitSt(v4);
			il.EmitLd(v5);
			il.Emit(OpCodes.Call, typeof(FormatterServices).GetMethod("GetUninitializedObject"));
			il.Emit(OpCodes.Castclass, type);
			il.EmitSt(v);
			il.EmitLd(variable);
			il.Emit(OpCodes.Call, typeof(MsgPackReader).GetProperty("Length").GetGetMethod());
			il.EmitSt(v2);
			Label label = il.DefineLabel();
			Label label2 = il.DefineLabel();
			il.EmitLdc(0);
			il.EmitSt(v3);
			il.Emit(OpCodes.Br, label2);
			il.MarkLabel(label);
			PackILGenerator.EmitUnpackReadAndTypeCheckCode(il, variable, typeof(MsgPackReader).GetMethod("IsRaw"), method, false);
			Label label3 = il.DefineLabel();
			il.EmitLd(v4);
			il.EmitLd(variable);
			il.Emit(OpCodes.Call, typeof(MsgPackReader).GetMethod("ReadRawString", new Type[0]));
			il.Emit(OpCodes.Ldloca_S, (byte)variable2.Index);
			il.Emit(OpCodes.Callvirt, typeof(IDictionary<string, int>).GetMethod("TryGetValue"));
			il.Emit(OpCodes.Brtrue, label3);
			il.Emit(OpCodes.Call, method);
			il.MarkLabel(label3);
			Label[] array2 = new Label[array.Length];
			for (int j = 0; j < array2.Length; j++)
			{
				array2[j] = il.DefineLabel();
			}
			Label label4 = il.DefineLabel();
			il.EmitLd(variable2);
			il.Emit(OpCodes.Switch, array2);
			il.Emit(OpCodes.Call, method);
			for (int k = 0; k < array2.Length; k++)
			{
				il.MarkLabel(array2[k]);
				MemberInfo memberInfo = array[k];
				Type memberType = memberInfo.GetMemberType();
				MethodInfo meth = lookupUnpackMethod(memberType);
				il.EmitLd(v);
				il.EmitLd(variable);
				il.Emit(OpCodes.Call, meth);
				il.EmitStMember(memberInfo);
				il.Emit(OpCodes.Br, label4);
			}
			il.MarkLabel(label4);
			il.EmitLd(v3);
			il.EmitLdc(1);
			il.Emit(OpCodes.Add);
			il.EmitSt(v3);
			il.MarkLabel(label2);
			il.EmitLd(v3);
			il.EmitLd(v2);
			il.Emit(OpCodes.Blt, label);
			il.EmitLd(v);
			il.Emit(OpCodes.Ret);
		}

		// Token: 0x0600059D RID: 1437 RVA: 0x0000F2B4 File Offset: 0x0000D6B4
		private static void EmitUnpackArrayCode(Type arrayType, MethodInfo mi, ILGenerator il, Func<Type, MemberInfo[]> targetMemberSelector, Func<MemberInfo, string> memberNameFormatter, Func<Type, MethodInfo> lookupUnpackMethod)
		{
			Type elementType = arrayType.GetElementType();
			MethodInfo method = typeof(PackILGenerator).GetMethod("UnpackFailed", BindingFlags.Static | BindingFlags.NonPublic);
			Variable variable = Variable.CreateArg(0);
			Variable variable2 = Variable.CreateLocal(il.DeclareLocal(arrayType));
			Variable v = Variable.CreateLocal(il.DeclareLocal(typeof(int)));
			Variable variable3 = Variable.CreateLocal(il.DeclareLocal(typeof(int)));
			Variable v2 = Variable.CreateLocal(il.DeclareLocal(typeof(Type)));
			PackILGenerator.EmitUnpackReadAndTypeCheckCode(il, variable, typeof(MsgPackReader).GetMethod("IsArray"), method, true);
			il.Emit(OpCodes.Ldtoken, elementType);
			il.Emit(OpCodes.Call, typeof(Type).GetMethod("GetTypeFromHandle"));
			il.EmitSt(v2);
			il.EmitLd(variable);
			il.Emit(OpCodes.Call, typeof(MsgPackReader).GetProperty("Length").GetGetMethod());
			il.EmitSt(v);
			il.EmitLd(v2);
			il.EmitLd(v);
			il.Emit(OpCodes.Call, typeof(Array).GetMethod("CreateInstance", new Type[]
			{
				typeof(Type),
				typeof(int)
			}));
			il.Emit(OpCodes.Castclass, arrayType);
			il.EmitSt(variable2);
			MethodInfo meth = lookupUnpackMethod(elementType);
			Label label = il.DefineLabel();
			Label label2 = il.DefineLabel();
			il.EmitLdc(0);
			il.EmitSt(variable3);
			il.Emit(OpCodes.Br, label2);
			il.MarkLabel(label);
			il.EmitLd(new Variable[]
			{
				variable2,
				variable3
			});
			il.EmitLd(variable);
			il.Emit(OpCodes.Call, meth);
			il.Emit(OpCodes.Stelem, elementType);
			il.EmitLd(variable3);
			il.EmitLdc(1);
			il.Emit(OpCodes.Add);
			il.EmitSt(variable3);
			il.MarkLabel(label2);
			il.EmitLd(variable3);
			il.EmitLd(v);
			il.Emit(OpCodes.Blt, label);
			il.EmitLd(variable2);
			il.Emit(OpCodes.Ret);
		}

		// Token: 0x0600059E RID: 1438 RVA: 0x0000F4EC File Offset: 0x0000D8EC
		private static void EmitUnpackReadAndTypeCheckCode(ILGenerator il, Variable msgpackReader, MethodInfo typeCheckMethod, MethodInfo failedMethod, bool nullCheckAndReturn)
		{
			Label label = il.DefineLabel();
			Label label2 = (!nullCheckAndReturn) ? default(Label) : il.DefineLabel();
			Label label3 = il.DefineLabel();
			il.EmitLd(msgpackReader);
			il.Emit(OpCodes.Call, typeof(MsgPackReader).GetMethod("Read"));
			il.Emit(OpCodes.Brfalse_S, label);
			if (nullCheckAndReturn)
			{
				il.EmitLd(msgpackReader);
				il.Emit(OpCodes.Call, typeof(MsgPackReader).GetProperty("Type").GetGetMethod());
				il.EmitLdc(192);
				il.Emit(OpCodes.Beq_S, label2);
			}
			il.EmitLd(msgpackReader);
			il.Emit(OpCodes.Call, typeCheckMethod);
			il.Emit(OpCodes.Brtrue_S, label3);
			il.Emit(OpCodes.Br, label);
			if (nullCheckAndReturn)
			{
				il.MarkLabel(label2);
				il.Emit(OpCodes.Ldnull);
				il.Emit(OpCodes.Ret);
			}
			il.MarkLabel(label);
			il.Emit(OpCodes.Call, failedMethod);
			il.MarkLabel(label3);
		}

		// Token: 0x0600059F RID: 1439 RVA: 0x0000F607 File Offset: 0x0000DA07
		internal static void UnpackFailed()
		{
			throw new FormatException();
		}

		// Token: 0x060005A0 RID: 1440 RVA: 0x0000F60E File Offset: 0x0000DA0E
		private static Type GetMemberType(this MemberInfo mi)
		{
			if (mi.MemberType == MemberTypes.Field)
			{
				return ((FieldInfo)mi).FieldType;
			}
			if (mi.MemberType == MemberTypes.Property)
			{
				return ((PropertyInfo)mi).PropertyType;
			}
			throw new ArgumentException();
		}
	}
}
