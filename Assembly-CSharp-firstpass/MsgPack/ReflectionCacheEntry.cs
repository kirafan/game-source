﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace MsgPack
{
	// Token: 0x020000CC RID: 204
	public class ReflectionCacheEntry
	{
		// Token: 0x060005F9 RID: 1529 RVA: 0x00011170 File Offset: 0x0000F570
		public ReflectionCacheEntry(Type t)
		{
			FieldInfo[] fields = t.GetFields(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.SetField);
			IDictionary<string, FieldInfo> dictionary = new Dictionary<string, FieldInfo>(fields.Length);
			foreach (FieldInfo fieldInfo in fields)
			{
				string text = fieldInfo.Name;
				int num;
				if (text[0] == '<' && (num = text.IndexOf('>')) > 1)
				{
					text = text.Substring(1, num - 1);
				}
				dictionary[text] = fieldInfo;
			}
			this.FieldMap = dictionary;
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060005FA RID: 1530 RVA: 0x000111F7 File Offset: 0x0000F5F7
		// (set) Token: 0x060005FB RID: 1531 RVA: 0x000111FF File Offset: 0x0000F5FF
		public IDictionary<string, FieldInfo> FieldMap { get; private set; }

		// Token: 0x040003A1 RID: 929
		private const BindingFlags FieldBindingFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.GetField | BindingFlags.SetField;
	}
}
