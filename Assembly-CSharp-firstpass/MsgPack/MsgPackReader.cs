﻿using System;
using System.IO;
using System.Text;

namespace MsgPack
{
	// Token: 0x020000C6 RID: 198
	public class MsgPackReader
	{
		// Token: 0x060005A8 RID: 1448 RVA: 0x0000F695 File Offset: 0x0000DA95
		public MsgPackReader(Stream strm)
		{
			this._strm = strm;
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060005A9 RID: 1449 RVA: 0x0000F6D4 File Offset: 0x0000DAD4
		// (set) Token: 0x060005AA RID: 1450 RVA: 0x0000F6DC File Offset: 0x0000DADC
		public TypePrefixes Type { get; private set; }

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060005AB RID: 1451 RVA: 0x0000F6E5 File Offset: 0x0000DAE5
		// (set) Token: 0x060005AC RID: 1452 RVA: 0x0000F6ED File Offset: 0x0000DAED
		public bool ValueBoolean { get; private set; }

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060005AD RID: 1453 RVA: 0x0000F6F6 File Offset: 0x0000DAF6
		// (set) Token: 0x060005AE RID: 1454 RVA: 0x0000F6FE File Offset: 0x0000DAFE
		public uint Length { get; private set; }

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060005AF RID: 1455 RVA: 0x0000F707 File Offset: 0x0000DB07
		// (set) Token: 0x060005B0 RID: 1456 RVA: 0x0000F70F File Offset: 0x0000DB0F
		public uint ValueUnsigned { get; private set; }

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060005B1 RID: 1457 RVA: 0x0000F718 File Offset: 0x0000DB18
		// (set) Token: 0x060005B2 RID: 1458 RVA: 0x0000F720 File Offset: 0x0000DB20
		public ulong ValueUnsigned64 { get; private set; }

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060005B3 RID: 1459 RVA: 0x0000F729 File Offset: 0x0000DB29
		// (set) Token: 0x060005B4 RID: 1460 RVA: 0x0000F731 File Offset: 0x0000DB31
		public int ValueSigned { get; private set; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060005B5 RID: 1461 RVA: 0x0000F73A File Offset: 0x0000DB3A
		// (set) Token: 0x060005B6 RID: 1462 RVA: 0x0000F742 File Offset: 0x0000DB42
		public long ValueSigned64 { get; private set; }

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060005B7 RID: 1463 RVA: 0x0000F74B File Offset: 0x0000DB4B
		// (set) Token: 0x060005B8 RID: 1464 RVA: 0x0000F753 File Offset: 0x0000DB53
		public float ValueFloat { get; private set; }

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060005B9 RID: 1465 RVA: 0x0000F75C File Offset: 0x0000DB5C
		// (set) Token: 0x060005BA RID: 1466 RVA: 0x0000F764 File Offset: 0x0000DB64
		public double ValueDouble { get; private set; }

		// Token: 0x060005BB RID: 1467 RVA: 0x0000F770 File Offset: 0x0000DB70
		public bool IsSigned()
		{
			return this.Type == TypePrefixes.NegativeFixNum || this.Type == TypePrefixes.PositiveFixNum || this.Type == TypePrefixes.Int8 || this.Type == TypePrefixes.Int16 || this.Type == TypePrefixes.Int32;
		}

		// Token: 0x060005BC RID: 1468 RVA: 0x0000F7C8 File Offset: 0x0000DBC8
		public bool IsBoolean()
		{
			return this.Type == TypePrefixes.True || this.Type == TypePrefixes.False;
		}

		// Token: 0x060005BD RID: 1469 RVA: 0x0000F7EA File Offset: 0x0000DBEA
		public bool IsSigned64()
		{
			return this.Type == TypePrefixes.Int64;
		}

		// Token: 0x060005BE RID: 1470 RVA: 0x0000F7F9 File Offset: 0x0000DBF9
		public bool IsUnsigned()
		{
			return this.Type == TypePrefixes.PositiveFixNum || this.Type == TypePrefixes.UInt8 || this.Type == TypePrefixes.UInt16 || this.Type == TypePrefixes.UInt32;
		}

		// Token: 0x060005BF RID: 1471 RVA: 0x0000F836 File Offset: 0x0000DC36
		public bool IsUnsigned64()
		{
			return this.Type == TypePrefixes.UInt64;
		}

		// Token: 0x060005C0 RID: 1472 RVA: 0x0000F845 File Offset: 0x0000DC45
		public bool IsRaw()
		{
			return this.Type == TypePrefixes.FixRaw || this.Type == TypePrefixes.Raw16 || this.Type == TypePrefixes.Raw32;
		}

		// Token: 0x060005C1 RID: 1473 RVA: 0x0000F877 File Offset: 0x0000DC77
		public bool IsArray()
		{
			return this.Type == TypePrefixes.FixArray || this.Type == TypePrefixes.Array16 || this.Type == TypePrefixes.Array32;
		}

		// Token: 0x060005C2 RID: 1474 RVA: 0x0000F8A9 File Offset: 0x0000DCA9
		public bool IsMap()
		{
			return this.Type == TypePrefixes.FixMap || this.Type == TypePrefixes.Map16 || this.Type == TypePrefixes.Map32;
		}

		// Token: 0x060005C3 RID: 1475 RVA: 0x0000F8DC File Offset: 0x0000DCDC
		public bool Read()
		{
			byte[] tmp = this._tmp0;
			byte[] tmp2 = this._tmp1;
			int num = this._strm.ReadByte();
			if (num < 0)
			{
				return false;
			}
			if (num >= 0 && num <= 127)
			{
				this.Type = TypePrefixes.PositiveFixNum;
			}
			else if (num >= 224 && num <= 255)
			{
				this.Type = TypePrefixes.NegativeFixNum;
			}
			else if (num >= 160 && num <= 191)
			{
				this.Type = TypePrefixes.FixRaw;
			}
			else if (num >= 144 && num <= 159)
			{
				this.Type = TypePrefixes.FixArray;
			}
			else if (num >= 128 && num <= 143)
			{
				this.Type = TypePrefixes.FixMap;
			}
			else
			{
				this.Type = (TypePrefixes)num;
			}
			TypePrefixes type = this.Type;
			switch (type)
			{
			case TypePrefixes.Float:
				this._strm.Read(tmp, 0, 4);
				if (BitConverter.IsLittleEndian)
				{
					tmp2[0] = tmp[3];
					tmp2[1] = tmp[2];
					tmp2[2] = tmp[1];
					tmp2[3] = tmp[0];
					this.ValueFloat = BitConverter.ToSingle(tmp2, 0);
				}
				else
				{
					this.ValueFloat = BitConverter.ToSingle(tmp, 0);
				}
				break;
			case TypePrefixes.Double:
				this._strm.Read(tmp, 0, 8);
				if (BitConverter.IsLittleEndian)
				{
					tmp2[0] = tmp[7];
					tmp2[1] = tmp[6];
					tmp2[2] = tmp[5];
					tmp2[3] = tmp[4];
					tmp2[4] = tmp[3];
					tmp2[5] = tmp[2];
					tmp2[6] = tmp[1];
					tmp2[7] = tmp[0];
					this.ValueDouble = BitConverter.ToDouble(tmp2, 0);
				}
				else
				{
					this.ValueDouble = BitConverter.ToDouble(tmp, 0);
				}
				break;
			case TypePrefixes.UInt8:
				num = this._strm.ReadByte();
				if (num < 0)
				{
					throw new FormatException();
				}
				this.ValueUnsigned = (uint)num;
				break;
			case TypePrefixes.UInt16:
				if (this._strm.Read(tmp, 0, 2) != 2)
				{
					throw new FormatException();
				}
				this.ValueUnsigned = (uint)((int)tmp[0] << 8 | (int)tmp[1]);
				break;
			case TypePrefixes.UInt32:
				if (this._strm.Read(tmp, 0, 4) != 4)
				{
					throw new FormatException();
				}
				this.ValueUnsigned = (uint)((int)tmp[0] << 24 | (int)tmp[1] << 16 | (int)tmp[2] << 8 | (int)tmp[3]);
				break;
			case TypePrefixes.UInt64:
				if (this._strm.Read(tmp, 0, 8) != 8)
				{
					throw new FormatException();
				}
				this.ValueUnsigned64 = ((ulong)tmp[0] << 56 | (ulong)tmp[1] << 48 | (ulong)tmp[2] << 40 | (ulong)tmp[3] << 32 | (ulong)tmp[4] << 24 | (ulong)tmp[5] << 16 | (ulong)tmp[6] << 8 | (ulong)tmp[7]);
				break;
			case TypePrefixes.Int8:
				num = this._strm.ReadByte();
				if (num < 0)
				{
					throw new FormatException();
				}
				this.ValueSigned = (int)((sbyte)num);
				break;
			case TypePrefixes.Int16:
				if (this._strm.Read(tmp, 0, 2) != 2)
				{
					throw new FormatException();
				}
				this.ValueSigned = (int)((short)((int)tmp[0] << 8 | (int)tmp[1]));
				break;
			case TypePrefixes.Int32:
				if (this._strm.Read(tmp, 0, 4) != 4)
				{
					throw new FormatException();
				}
				this.ValueSigned = ((int)tmp[0] << 24 | (int)tmp[1] << 16 | (int)tmp[2] << 8 | (int)tmp[3]);
				break;
			case TypePrefixes.Int64:
				if (this._strm.Read(tmp, 0, 8) != 8)
				{
					throw new FormatException();
				}
				this.ValueSigned64 = ((long)tmp[0] << 56 | (long)tmp[1] << 48 | (long)tmp[2] << 40 | (long)tmp[3] << 32 | (long)tmp[4] << 24 | (long)tmp[5] << 16 | (long)tmp[6] << 8 | (long)tmp[7]);
				break;
			default:
				switch (type)
				{
				case TypePrefixes.Nil:
					break;
				default:
					if (type != TypePrefixes.PositiveFixNum)
					{
						if (type != TypePrefixes.FixMap && type != TypePrefixes.FixArray)
						{
							if (type != TypePrefixes.FixRaw)
							{
								throw new FormatException();
							}
							this.Length = (uint)(num & 31);
						}
						else
						{
							this.Length = (uint)(num & 15);
						}
					}
					else
					{
						this.ValueSigned = (num & 127);
						this.ValueUnsigned = (uint)this.ValueSigned;
					}
					break;
				case TypePrefixes.False:
					this.ValueBoolean = false;
					break;
				case TypePrefixes.True:
					this.ValueBoolean = true;
					break;
				}
				break;
			case TypePrefixes.Raw16:
			case TypePrefixes.Array16:
			case TypePrefixes.Map16:
				if (this._strm.Read(tmp, 0, 2) != 2)
				{
					throw new FormatException();
				}
				this.Length = (uint)((int)tmp[0] << 8 | (int)tmp[1]);
				break;
			case TypePrefixes.Raw32:
			case TypePrefixes.Array32:
			case TypePrefixes.Map32:
				if (this._strm.Read(tmp, 0, 4) != 4)
				{
					throw new FormatException();
				}
				this.Length = (uint)((int)tmp[0] << 24 | (int)tmp[1] << 16 | (int)tmp[2] << 8 | (int)tmp[3]);
				break;
			case TypePrefixes.NegativeFixNum:
				this.ValueSigned = (num & 31) - 32;
				break;
			}
			return true;
		}

		// Token: 0x060005C4 RID: 1476 RVA: 0x0000FDFA File Offset: 0x0000E1FA
		public int ReadValueRaw(byte[] buf, int offset, int count)
		{
			return this._strm.Read(buf, offset, count);
		}

		// Token: 0x060005C5 RID: 1477 RVA: 0x0000FE0A File Offset: 0x0000E20A
		public string ReadRawString()
		{
			return this.ReadRawString(this._buf);
		}

		// Token: 0x060005C6 RID: 1478 RVA: 0x0000FE18 File Offset: 0x0000E218
		public string ReadRawString(byte[] buf)
		{
			if ((ulong)this.Length < (ulong)((long)buf.Length))
			{
				if ((long)this.ReadValueRaw(buf, 0, (int)this.Length) != (long)((ulong)this.Length))
				{
					throw new FormatException();
				}
				return this._encoding.GetString(buf, 0, (int)this.Length);
			}
			else
			{
				byte[] array = new byte[this.Length];
				if (this.ReadValueRaw(array, 0, array.Length) != array.Length)
				{
					throw new FormatException();
				}
				return this._encoding.GetString(array);
			}
		}

		// Token: 0x04000389 RID: 905
		private Stream _strm;

		// Token: 0x0400038A RID: 906
		private byte[] _tmp0 = new byte[8];

		// Token: 0x0400038B RID: 907
		private byte[] _tmp1 = new byte[8];

		// Token: 0x0400038C RID: 908
		private Encoding _encoding = Encoding.UTF8;

		// Token: 0x0400038D RID: 909
		private byte[] _buf = new byte[64];
	}
}
