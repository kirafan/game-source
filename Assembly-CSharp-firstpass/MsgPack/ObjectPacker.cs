﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using System.Text;

namespace MsgPack
{
	// Token: 0x020000C8 RID: 200
	public class ObjectPacker
	{
		// Token: 0x060005DE RID: 1502 RVA: 0x00010578 File Offset: 0x0000E978
		static ObjectPacker()
		{
			Dictionary<Type, ObjectPacker.PackDelegate> packerMapping = ObjectPacker.PackerMapping;
			Type typeFromHandle = typeof(string);
			if (ObjectPacker.<>f__mg$cache0 == null)
			{
				ObjectPacker.<>f__mg$cache0 = new ObjectPacker.PackDelegate(ObjectPacker.StringPacker);
			}
			packerMapping.Add(typeFromHandle, ObjectPacker.<>f__mg$cache0);
			Dictionary<Type, ObjectPacker.UnpackDelegate> unpackerMapping = ObjectPacker.UnpackerMapping;
			Type typeFromHandle2 = typeof(string);
			if (ObjectPacker.<>f__mg$cache1 == null)
			{
				ObjectPacker.<>f__mg$cache1 = new ObjectPacker.UnpackDelegate(ObjectPacker.StringUnpacker);
			}
			unpackerMapping.Add(typeFromHandle2, ObjectPacker.<>f__mg$cache1);
		}

		// Token: 0x060005E0 RID: 1504 RVA: 0x00010610 File Offset: 0x0000EA10
		public byte[] Pack(object o)
		{
			byte[] result;
			using (MemoryStream memoryStream = new MemoryStream())
			{
				this.Pack(memoryStream, o);
				result = memoryStream.ToArray();
			}
			return result;
		}

		// Token: 0x060005E1 RID: 1505 RVA: 0x00010658 File Offset: 0x0000EA58
		public void Pack(Stream strm, object o)
		{
			if (o != null && o.GetType().IsPrimitive)
			{
				throw new NotSupportedException();
			}
			MsgPackWriter writer = new MsgPackWriter(strm);
			this.Pack(writer, o);
		}

		// Token: 0x060005E2 RID: 1506 RVA: 0x00010690 File Offset: 0x0000EA90
		private void Pack(MsgPackWriter writer, object o)
		{
			if (o == null)
			{
				writer.WriteNil();
				return;
			}
			Type type = o.GetType();
			if (type.IsPrimitive)
			{
				if (type.Equals(typeof(int)))
				{
					writer.Write((int)o);
				}
				else if (type.Equals(typeof(uint)))
				{
					writer.Write((uint)o);
				}
				else if (type.Equals(typeof(float)))
				{
					writer.Write((float)o);
				}
				else if (type.Equals(typeof(double)))
				{
					writer.Write((double)o);
				}
				else if (type.Equals(typeof(long)))
				{
					writer.Write((long)o);
				}
				else if (type.Equals(typeof(ulong)))
				{
					writer.Write((ulong)o);
				}
				else if (type.Equals(typeof(bool)))
				{
					writer.Write((bool)o);
				}
				else if (type.Equals(typeof(byte)))
				{
					writer.Write((byte)o);
				}
				else if (type.Equals(typeof(sbyte)))
				{
					writer.Write((sbyte)o);
				}
				else if (type.Equals(typeof(short)))
				{
					writer.Write((short)o);
				}
				else if (type.Equals(typeof(ushort)))
				{
					writer.Write((ushort)o);
				}
				else
				{
					if (!type.Equals(typeof(char)))
					{
						throw new NotSupportedException();
					}
					writer.Write((ushort)((char)o));
				}
				return;
			}
			ObjectPacker.PackDelegate packDelegate;
			if (ObjectPacker.PackerMapping.TryGetValue(type, out packDelegate))
			{
				packDelegate(this, writer, o);
				return;
			}
			if (type.IsArray)
			{
				Array array = (Array)o;
				writer.WriteArrayHeader(array.Length);
				for (int i = 0; i < array.Length; i++)
				{
					this.Pack(writer, array.GetValue(i));
				}
				return;
			}
			ReflectionCacheEntry reflectionCacheEntry = ReflectionCache.Lookup(type);
			writer.WriteMapHeader(reflectionCacheEntry.FieldMap.Count);
			foreach (KeyValuePair<string, FieldInfo> keyValuePair in reflectionCacheEntry.FieldMap)
			{
				writer.Write(keyValuePair.Key, this._buf);
				object value = keyValuePair.Value.GetValue(o);
				if (keyValuePair.Value.FieldType.IsInterface && value != null)
				{
					writer.WriteArrayHeader(2);
					writer.Write(value.GetType().FullName);
				}
				this.Pack(writer, value);
			}
		}

		// Token: 0x060005E3 RID: 1507 RVA: 0x000109B4 File Offset: 0x0000EDB4
		public T Unpack<T>(byte[] buf)
		{
			return this.Unpack<T>(buf, 0, buf.Length);
		}

		// Token: 0x060005E4 RID: 1508 RVA: 0x000109C4 File Offset: 0x0000EDC4
		public T Unpack<T>(byte[] buf, int offset, int size)
		{
			T result;
			using (MemoryStream memoryStream = new MemoryStream(buf, offset, size))
			{
				result = this.Unpack<T>(memoryStream);
			}
			return result;
		}

		// Token: 0x060005E5 RID: 1509 RVA: 0x00010A08 File Offset: 0x0000EE08
		public T Unpack<T>(Stream strm)
		{
			if (typeof(T).IsPrimitive)
			{
				throw new NotSupportedException();
			}
			MsgPackReader reader = new MsgPackReader(strm);
			return (T)((object)this.Unpack(reader, typeof(T)));
		}

		// Token: 0x060005E6 RID: 1510 RVA: 0x00010A4C File Offset: 0x0000EE4C
		public object Unpack(Type type, byte[] buf)
		{
			return this.Unpack(type, buf, 0, buf.Length);
		}

		// Token: 0x060005E7 RID: 1511 RVA: 0x00010A5C File Offset: 0x0000EE5C
		public object Unpack(Type type, byte[] buf, int offset, int size)
		{
			object result;
			using (MemoryStream memoryStream = new MemoryStream(buf, offset, size))
			{
				result = this.Unpack(type, memoryStream);
			}
			return result;
		}

		// Token: 0x060005E8 RID: 1512 RVA: 0x00010AA0 File Offset: 0x0000EEA0
		public object Unpack(Type type, Stream strm)
		{
			if (type.IsPrimitive)
			{
				throw new NotSupportedException();
			}
			MsgPackReader reader = new MsgPackReader(strm);
			return this.Unpack(reader, type);
		}

		// Token: 0x060005E9 RID: 1513 RVA: 0x00010AD0 File Offset: 0x0000EED0
		private object Unpack(MsgPackReader reader, Type t)
		{
			if (t.IsPrimitive)
			{
				if (!reader.Read())
				{
					throw new FormatException();
				}
				if (t.Equals(typeof(int)) && reader.IsSigned())
				{
					return reader.ValueSigned;
				}
				if (t.Equals(typeof(uint)) && reader.IsUnsigned())
				{
					return reader.ValueUnsigned;
				}
				if (t.Equals(typeof(float)) && reader.Type == TypePrefixes.Float)
				{
					return reader.ValueFloat;
				}
				if (t.Equals(typeof(double)) && reader.Type == TypePrefixes.Double)
				{
					return reader.ValueDouble;
				}
				if (t.Equals(typeof(long)))
				{
					if (reader.IsSigned64())
					{
						return reader.ValueSigned64;
					}
					if (reader.IsSigned())
					{
						return (long)reader.ValueSigned;
					}
				}
				else if (t.Equals(typeof(ulong)))
				{
					if (reader.IsUnsigned64())
					{
						return reader.ValueUnsigned64;
					}
					if (reader.IsUnsigned())
					{
						return (ulong)reader.ValueUnsigned;
					}
				}
				else
				{
					if (t.Equals(typeof(bool)) && reader.IsBoolean())
					{
						return reader.Type == TypePrefixes.True;
					}
					if (t.Equals(typeof(byte)) && reader.IsUnsigned())
					{
						return (byte)reader.ValueUnsigned;
					}
					if (t.Equals(typeof(sbyte)) && reader.IsSigned())
					{
						return (sbyte)reader.ValueSigned;
					}
					if (t.Equals(typeof(short)) && reader.IsSigned())
					{
						return (short)reader.ValueSigned;
					}
					if (t.Equals(typeof(ushort)) && reader.IsUnsigned())
					{
						return (ushort)reader.ValueUnsigned;
					}
					if (t.Equals(typeof(char)) && reader.IsUnsigned())
					{
						return (char)reader.ValueUnsigned;
					}
					throw new NotSupportedException();
				}
			}
			ObjectPacker.UnpackDelegate unpackDelegate;
			if (ObjectPacker.UnpackerMapping.TryGetValue(t, out unpackDelegate))
			{
				return unpackDelegate(this, reader);
			}
			if (t.IsArray)
			{
				if (!reader.Read() || (!reader.IsArray() && reader.Type != TypePrefixes.Nil))
				{
					throw new FormatException();
				}
				if (reader.Type == TypePrefixes.Nil)
				{
					return null;
				}
				Type elementType = t.GetElementType();
				Array array = Array.CreateInstance(elementType, (int)reader.Length);
				for (int i = 0; i < array.Length; i++)
				{
					array.SetValue(this.Unpack(reader, elementType), i);
				}
				return array;
			}
			else
			{
				if (!reader.Read())
				{
					throw new FormatException();
				}
				if (reader.Type == TypePrefixes.Nil)
				{
					return null;
				}
				if (t.IsInterface)
				{
					if (reader.Type != TypePrefixes.FixArray && reader.Length != 2U)
					{
						throw new FormatException();
					}
					if (!reader.Read() || !reader.IsRaw())
					{
						throw new FormatException();
					}
					this.CheckBufferSize((int)reader.Length);
					reader.ReadValueRaw(this._buf, 0, (int)reader.Length);
					t = Type.GetType(Encoding.UTF8.GetString(this._buf, 0, (int)reader.Length));
					if (!reader.Read() || reader.Type == TypePrefixes.Nil)
					{
						throw new FormatException();
					}
				}
				if (!reader.IsMap())
				{
					throw new FormatException();
				}
				object uninitializedObject = FormatterServices.GetUninitializedObject(t);
				ReflectionCacheEntry reflectionCacheEntry = ReflectionCache.Lookup(t);
				int length = (int)reader.Length;
				for (int j = 0; j < length; j++)
				{
					if (!reader.Read() || !reader.IsRaw())
					{
						throw new FormatException();
					}
					this.CheckBufferSize((int)reader.Length);
					reader.ReadValueRaw(this._buf, 0, (int)reader.Length);
					string @string = Encoding.UTF8.GetString(this._buf, 0, (int)reader.Length);
					FieldInfo fieldInfo;
					if (!reflectionCacheEntry.FieldMap.TryGetValue(@string, out fieldInfo))
					{
						throw new FormatException();
					}
					fieldInfo.SetValue(uninitializedObject, this.Unpack(reader, fieldInfo.FieldType));
				}
				IDeserializationCallback deserializationCallback = uninitializedObject as IDeserializationCallback;
				if (deserializationCallback != null)
				{
					deserializationCallback.OnDeserialization(this);
				}
				return uninitializedObject;
			}
		}

		// Token: 0x060005EA RID: 1514 RVA: 0x00010FAB File Offset: 0x0000F3AB
		private void CheckBufferSize(int size)
		{
			if (this._buf.Length < size)
			{
				Array.Resize<byte>(ref this._buf, size);
			}
		}

		// Token: 0x060005EB RID: 1515 RVA: 0x00010FC7 File Offset: 0x0000F3C7
		private static void StringPacker(ObjectPacker packer, MsgPackWriter writer, object o)
		{
			writer.Write(Encoding.UTF8.GetBytes((string)o));
		}

		// Token: 0x060005EC RID: 1516 RVA: 0x00010FE0 File Offset: 0x0000F3E0
		private static object StringUnpacker(ObjectPacker packer, MsgPackReader reader)
		{
			if (!reader.Read())
			{
				throw new FormatException();
			}
			if (reader.Type == TypePrefixes.Nil)
			{
				return null;
			}
			if (!reader.IsRaw())
			{
				throw new FormatException();
			}
			packer.CheckBufferSize((int)reader.Length);
			reader.ReadValueRaw(packer._buf, 0, (int)reader.Length);
			return Encoding.UTF8.GetString(packer._buf, 0, (int)reader.Length);
		}

		// Token: 0x0400039B RID: 923
		private byte[] _buf = new byte[64];

		// Token: 0x0400039C RID: 924
		private static Dictionary<Type, ObjectPacker.PackDelegate> PackerMapping = new Dictionary<Type, ObjectPacker.PackDelegate>();

		// Token: 0x0400039D RID: 925
		private static Dictionary<Type, ObjectPacker.UnpackDelegate> UnpackerMapping = new Dictionary<Type, ObjectPacker.UnpackDelegate>();

		// Token: 0x0400039E RID: 926
		[CompilerGenerated]
		private static ObjectPacker.PackDelegate <>f__mg$cache0;

		// Token: 0x0400039F RID: 927
		[CompilerGenerated]
		private static ObjectPacker.UnpackDelegate <>f__mg$cache1;

		// Token: 0x020000C9 RID: 201
		// (Invoke) Token: 0x060005EE RID: 1518
		private delegate void PackDelegate(ObjectPacker packer, MsgPackWriter writer, object o);

		// Token: 0x020000CA RID: 202
		// (Invoke) Token: 0x060005F2 RID: 1522
		private delegate object UnpackDelegate(ObjectPacker packer, MsgPackReader reader);
	}
}
