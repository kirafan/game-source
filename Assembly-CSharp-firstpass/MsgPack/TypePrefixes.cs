﻿using System;

namespace MsgPack
{
	// Token: 0x020000CD RID: 205
	public enum TypePrefixes : byte
	{
		// Token: 0x040003A4 RID: 932
		PositiveFixNum,
		// Token: 0x040003A5 RID: 933
		NegativeFixNum = 224,
		// Token: 0x040003A6 RID: 934
		Nil = 192,
		// Token: 0x040003A7 RID: 935
		False = 194,
		// Token: 0x040003A8 RID: 936
		True,
		// Token: 0x040003A9 RID: 937
		Float = 202,
		// Token: 0x040003AA RID: 938
		Double,
		// Token: 0x040003AB RID: 939
		UInt8,
		// Token: 0x040003AC RID: 940
		UInt16,
		// Token: 0x040003AD RID: 941
		UInt32,
		// Token: 0x040003AE RID: 942
		UInt64,
		// Token: 0x040003AF RID: 943
		Int8,
		// Token: 0x040003B0 RID: 944
		Int16,
		// Token: 0x040003B1 RID: 945
		Int32,
		// Token: 0x040003B2 RID: 946
		Int64,
		// Token: 0x040003B3 RID: 947
		Raw16 = 218,
		// Token: 0x040003B4 RID: 948
		Raw32,
		// Token: 0x040003B5 RID: 949
		Array16,
		// Token: 0x040003B6 RID: 950
		Array32,
		// Token: 0x040003B7 RID: 951
		Map16,
		// Token: 0x040003B8 RID: 952
		Map32,
		// Token: 0x040003B9 RID: 953
		FixRaw = 160,
		// Token: 0x040003BA RID: 954
		FixArray = 144,
		// Token: 0x040003BB RID: 955
		FixMap = 128
	}
}
