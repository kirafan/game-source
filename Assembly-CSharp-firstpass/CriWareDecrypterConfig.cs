﻿using System;

// Token: 0x0200009E RID: 158
[Serializable]
public class CriWareDecrypterConfig
{
	// Token: 0x040002DF RID: 735
	public string key = string.Empty;

	// Token: 0x040002E0 RID: 736
	public string authenticationFile = string.Empty;

	// Token: 0x040002E1 RID: 737
	public bool enableAtomDecryption = true;

	// Token: 0x040002E2 RID: 738
	public bool enableManaDecryption = true;
}
