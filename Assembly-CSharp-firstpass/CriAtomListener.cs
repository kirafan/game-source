﻿using System;
using UnityEngine;

// Token: 0x02000008 RID: 8
[AddComponentMenu("CRIWARE/CRI Atom Listener")]
public class CriAtomListener : MonoBehaviour
{
	// Token: 0x17000006 RID: 6
	// (get) Token: 0x0600007A RID: 122 RVA: 0x00003BCF File Offset: 0x00001FCF
	// (set) Token: 0x0600007B RID: 123 RVA: 0x00003BD6 File Offset: 0x00001FD6
	public static CriAtomListener activeListener { get; private set; }

	// Token: 0x17000007 RID: 7
	// (get) Token: 0x0600007C RID: 124 RVA: 0x00003BDE File Offset: 0x00001FDE
	// (set) Token: 0x0600007D RID: 125 RVA: 0x00003BE5 File Offset: 0x00001FE5
	public static CriAtomEx3dListener sharedNativeListener { get; private set; }

	// Token: 0x0600007E RID: 126 RVA: 0x00003BED File Offset: 0x00001FED
	public static void CreateSharedNativeListener()
	{
		if (CriAtomListener.sharedNativeListener == null)
		{
			CriAtomListener.sharedNativeListener = new CriAtomEx3dListener();
		}
	}

	// Token: 0x0600007F RID: 127 RVA: 0x00003C03 File Offset: 0x00002003
	public static void DestroySharedNativeListener()
	{
		if (CriAtomListener.sharedNativeListener != null)
		{
			CriAtomListener.sharedNativeListener.Dispose();
			CriAtomListener.sharedNativeListener = null;
		}
	}

	// Token: 0x06000080 RID: 128 RVA: 0x00003C1F File Offset: 0x0000201F
	private void OnEnable()
	{
		if (CriAtomListener.activeListener == null || this.activateListenerOnEnable)
		{
			this.ActivateListener();
		}
	}

	// Token: 0x06000081 RID: 129 RVA: 0x00003C42 File Offset: 0x00002042
	private void OnDisable()
	{
		if (CriAtomListener.activeListener == this)
		{
			if (CriAtomListener.sharedNativeListener != null)
			{
				CriAtomListener.sharedNativeListener.ResetParameters();
				CriAtomListener.sharedNativeListener.Update();
			}
			CriAtomListener.activeListener = null;
		}
	}

	// Token: 0x06000082 RID: 130 RVA: 0x00003C78 File Offset: 0x00002078
	private void LateUpdate()
	{
		if (CriAtomListener.activeListener != this)
		{
			return;
		}
		Vector3 position = base.transform.position;
		Vector3 vector = (position - this.lastPosition) / Time.deltaTime;
		Vector3 forward = base.transform.forward;
		Vector3 up = base.transform.up;
		this.lastPosition = position;
		if (CriAtomListener.sharedNativeListener != null)
		{
			CriAtomListener.sharedNativeListener.SetPosition(position.x, position.y, position.z);
			CriAtomListener.sharedNativeListener.SetVelocity(vector.x, vector.y, vector.z);
			CriAtomListener.sharedNativeListener.SetOrientation(forward.x, forward.y, forward.z, up.x, up.y, up.z);
			CriAtomListener.sharedNativeListener.Update();
		}
	}

	// Token: 0x06000083 RID: 131 RVA: 0x00003D60 File Offset: 0x00002160
	public void ActivateListener()
	{
		CriAtomListener.activeListener = this;
		Vector3 position = base.transform.position;
		Vector3 forward = base.transform.forward;
		Vector3 up = base.transform.up;
		this.lastPosition = position;
		if (CriAtomListener.sharedNativeListener != null)
		{
			CriAtomListener.sharedNativeListener.SetPosition(position.x, position.y, position.z);
			CriAtomListener.sharedNativeListener.SetVelocity(0f, 0f, 0f);
			CriAtomListener.sharedNativeListener.SetOrientation(forward.x, forward.y, forward.z, up.x, up.y, up.z);
			CriAtomListener.sharedNativeListener.Update();
		}
	}

	// Token: 0x0400005C RID: 92
	public bool activateListenerOnEnable;

	// Token: 0x0400005D RID: 93
	private Vector3 lastPosition;
}
