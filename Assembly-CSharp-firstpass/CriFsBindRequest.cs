﻿using System;

// Token: 0x02000067 RID: 103
public class CriFsBindRequest : CriFsRequest
{
	// Token: 0x0600033B RID: 827 RVA: 0x00007FB0 File Offset: 0x000063B0
	public CriFsBindRequest(CriFsBindRequest.BindType type, CriFsBinder targetBinder, CriFsBinder srcBinder, string path)
	{
		this.path = path;
		switch (type)
		{
		case CriFsBindRequest.BindType.Cpk:
			this.bindId = targetBinder.BindCpk(srcBinder, path);
			break;
		case CriFsBindRequest.BindType.Directory:
			this.bindId = targetBinder.BindDirectory(srcBinder, path);
			break;
		case CriFsBindRequest.BindType.File:
			this.bindId = targetBinder.BindFile(srcBinder, path);
			break;
		default:
			throw new Exception("Invalid bind type.");
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Fs);
	}

	// Token: 0x17000038 RID: 56
	// (get) Token: 0x0600033C RID: 828 RVA: 0x00008030 File Offset: 0x00006430
	// (set) Token: 0x0600033D RID: 829 RVA: 0x00008038 File Offset: 0x00006438
	public string path { get; private set; }

	// Token: 0x17000039 RID: 57
	// (get) Token: 0x0600033E RID: 830 RVA: 0x00008041 File Offset: 0x00006441
	// (set) Token: 0x0600033F RID: 831 RVA: 0x00008049 File Offset: 0x00006449
	public uint bindId { get; private set; }

	// Token: 0x06000340 RID: 832 RVA: 0x00008052 File Offset: 0x00006452
	public override void Stop()
	{
	}

	// Token: 0x06000341 RID: 833 RVA: 0x00008054 File Offset: 0x00006454
	public override void Update()
	{
		if (base.isDone)
		{
			return;
		}
		CriFsBinder.Status status = CriFsBinder.GetStatus(this.bindId);
		if (status == CriFsBinder.Status.Analyze)
		{
			return;
		}
		if (status == CriFsBinder.Status.Error)
		{
			base.error = "Error occurred.";
		}
		base.Done();
	}

	// Token: 0x06000342 RID: 834 RVA: 0x00008099 File Offset: 0x00006499
	protected override void Dispose(bool disposing)
	{
		CriDisposableObjectManager.Unregister(this);
	}

	// Token: 0x02000068 RID: 104
	public enum BindType
	{
		// Token: 0x040001CC RID: 460
		Cpk,
		// Token: 0x040001CD RID: 461
		Directory,
		// Token: 0x040001CE RID: 462
		File
	}
}
