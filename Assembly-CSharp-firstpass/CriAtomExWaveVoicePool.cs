﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000057 RID: 87
public class CriAtomExWaveVoicePool : CriAtomExVoicePool
{
	// Token: 0x060002C7 RID: 711 RVA: 0x00006DAC File Offset: 0x000051AC
	public CriAtomExWaveVoicePool(int numVoices, int maxChannels, int maxSamplingRate, bool streamingFlag, uint identifier = 0U)
	{
		this._identifier = identifier;
		this._identifier = identifier;
		this._numVoices = numVoices;
		this._maxChannels = maxChannels;
		this._maxSamplingRate = maxSamplingRate;
		CriAtomExVoicePool.VoicePoolConfig voicePoolConfig = default(CriAtomExVoicePool.VoicePoolConfig);
		voicePoolConfig.identifier = identifier;
		voicePoolConfig.numVoices = numVoices;
		voicePoolConfig.playerConfig.maxChannels = maxChannels;
		voicePoolConfig.playerConfig.maxSamplingRate = maxSamplingRate;
		voicePoolConfig.playerConfig.streamingFlag = streamingFlag;
		voicePoolConfig.playerConfig.soundRendererType = 2;
		voicePoolConfig.playerConfig.decodeLatency = 0;
		this._handle = CriAtomExWaveVoicePool.criAtomExVoicePool_AllocateWaveVoicePool(ref voicePoolConfig, IntPtr.Zero, 0);
		if (this._handle == IntPtr.Zero)
		{
			throw new Exception("CriAtomExWaveVoicePool() failed.");
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x060002C8 RID: 712
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExVoicePool_AllocateWaveVoicePool(ref CriAtomExVoicePool.VoicePoolConfig config, IntPtr work, int work_size);
}
