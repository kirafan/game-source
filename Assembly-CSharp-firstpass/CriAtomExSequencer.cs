﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000027 RID: 39
public static class CriAtomExSequencer
{
	// Token: 0x06000121 RID: 289 RVA: 0x000050C9 File Offset: 0x000034C9
	public static void SetEventCallback(CriAtomExSequencer.EventCbFunc func, string separator = "\t")
	{
		CriAtom.SetEventCallback(func, separator);
	}

	// Token: 0x02000028 RID: 40
	// (Invoke) Token: 0x06000123 RID: 291
	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void EventCbFunc(string eventParamsString);
}
