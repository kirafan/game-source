﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000026 RID: 38
public static class CriAtomExCategory
{
	// Token: 0x060000FB RID: 251 RVA: 0x00005016 File Offset: 0x00003416
	public static void SetVolume(string name, float volume)
	{
		CriAtomExCategory.criAtomExCategory_SetVolumeByName(name, volume);
	}

	// Token: 0x060000FC RID: 252 RVA: 0x0000501F File Offset: 0x0000341F
	public static void SetVolume(int id, float volume)
	{
		CriAtomExCategory.criAtomExCategory_SetVolumeById(id, volume);
	}

	// Token: 0x060000FD RID: 253 RVA: 0x00005028 File Offset: 0x00003428
	public static float GetVolume(string name)
	{
		return CriAtomExCategory.criAtomExCategory_GetVolumeByName(name);
	}

	// Token: 0x060000FE RID: 254 RVA: 0x00005030 File Offset: 0x00003430
	public static float GetVolume(int id)
	{
		return CriAtomExCategory.criAtomExCategory_GetVolumeById(id);
	}

	// Token: 0x060000FF RID: 255 RVA: 0x00005038 File Offset: 0x00003438
	public static void Mute(string name, bool mute)
	{
		CriAtomExCategory.criAtomExCategory_MuteByName(name, mute);
	}

	// Token: 0x06000100 RID: 256 RVA: 0x00005041 File Offset: 0x00003441
	public static void Mute(int id, bool mute)
	{
		CriAtomExCategory.criAtomExCategory_MuteById(id, mute);
	}

	// Token: 0x06000101 RID: 257 RVA: 0x0000504A File Offset: 0x0000344A
	public static bool IsMuted(string name)
	{
		return CriAtomExCategory.criAtomExCategory_IsMutedByName(name);
	}

	// Token: 0x06000102 RID: 258 RVA: 0x00005052 File Offset: 0x00003452
	public static bool IsMuted(int id)
	{
		return CriAtomExCategory.criAtomExCategory_IsMutedById(id);
	}

	// Token: 0x06000103 RID: 259 RVA: 0x0000505A File Offset: 0x0000345A
	public static void Solo(string name, bool solo, float muteVolume)
	{
		CriAtomExCategory.criAtomExCategory_SoloByName(name, solo, muteVolume);
	}

	// Token: 0x06000104 RID: 260 RVA: 0x00005064 File Offset: 0x00003464
	public static void Solo(int id, bool solo, float muteVolume)
	{
		CriAtomExCategory.criAtomExCategory_SoloById(id, solo, muteVolume);
	}

	// Token: 0x06000105 RID: 261 RVA: 0x0000506E File Offset: 0x0000346E
	public static bool IsSoloed(string name)
	{
		return CriAtomExCategory.criAtomExCategory_IsSoloedByName(name);
	}

	// Token: 0x06000106 RID: 262 RVA: 0x00005076 File Offset: 0x00003476
	public static bool IsSoloed(int id)
	{
		return CriAtomExCategory.criAtomExCategory_IsSoloedById(id);
	}

	// Token: 0x06000107 RID: 263 RVA: 0x0000507E File Offset: 0x0000347E
	public static void Pause(string name, bool pause)
	{
		CriAtomExCategory.criAtomExCategory_PauseByName(name, pause);
	}

	// Token: 0x06000108 RID: 264 RVA: 0x00005087 File Offset: 0x00003487
	public static void Pause(int id, bool pause)
	{
		CriAtomExCategory.criAtomExCategory_PauseById(id, pause);
	}

	// Token: 0x06000109 RID: 265 RVA: 0x00005090 File Offset: 0x00003490
	public static bool IsPaused(string name)
	{
		return CriAtomExCategory.criAtomExCategory_IsPausedByName(name);
	}

	// Token: 0x0600010A RID: 266 RVA: 0x00005098 File Offset: 0x00003498
	public static bool IsPaused(int id)
	{
		return CriAtomExCategory.criAtomExCategory_IsPausedById(id);
	}

	// Token: 0x0600010B RID: 267 RVA: 0x000050A0 File Offset: 0x000034A0
	public static void SetAisacControl(string name, string controlName, float value)
	{
		CriAtomExCategory.criAtomExCategory_SetAisacControlByName(name, controlName, value);
	}

	// Token: 0x0600010C RID: 268 RVA: 0x000050AA File Offset: 0x000034AA
	[Obsolete("Use CriAtomExCategory.SetAisacControl")]
	public static void SetAisac(string name, string controlName, float value)
	{
		CriAtomExCategory.SetAisacControl(name, controlName, value);
	}

	// Token: 0x0600010D RID: 269 RVA: 0x000050B4 File Offset: 0x000034B4
	public static void SetAisacControl(int id, int controlId, float value)
	{
		CriAtomExCategory.criAtomExCategory_SetAisacControlById(id, (ushort)controlId, value);
	}

	// Token: 0x0600010E RID: 270 RVA: 0x000050BF File Offset: 0x000034BF
	[Obsolete("Use CriAtomExCategory.SetAisacControl")]
	public static void SetAisac(int id, int controlId, float value)
	{
		CriAtomExCategory.SetAisacControl(id, controlId, value);
	}

	// Token: 0x0600010F RID: 271
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_SetVolumeByName(string name, float volume);

	// Token: 0x06000110 RID: 272
	[DllImport("cri_ware_unity")]
	private static extern float criAtomExCategory_GetVolumeByName(string name);

	// Token: 0x06000111 RID: 273
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_SetVolumeById(int id, float volume);

	// Token: 0x06000112 RID: 274
	[DllImport("cri_ware_unity")]
	private static extern float criAtomExCategory_GetVolumeById(int id);

	// Token: 0x06000113 RID: 275
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_MuteById(int id, bool mute);

	// Token: 0x06000114 RID: 276
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExCategory_IsMutedById(int id);

	// Token: 0x06000115 RID: 277
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_MuteByName(string name, bool mute);

	// Token: 0x06000116 RID: 278
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExCategory_IsMutedByName(string name);

	// Token: 0x06000117 RID: 279
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_SoloById(int id, bool solo, float volume);

	// Token: 0x06000118 RID: 280
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExCategory_IsSoloedById(int id);

	// Token: 0x06000119 RID: 281
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_SoloByName(string name, bool solo, float volume);

	// Token: 0x0600011A RID: 282
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExCategory_IsSoloedByName(string name);

	// Token: 0x0600011B RID: 283
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_PauseById(int id, bool pause);

	// Token: 0x0600011C RID: 284
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExCategory_IsPausedById(int id);

	// Token: 0x0600011D RID: 285
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_PauseByName(string name, bool pause);

	// Token: 0x0600011E RID: 286
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExCategory_IsPausedByName(string name);

	// Token: 0x0600011F RID: 287
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_SetAisacControlById(int id, ushort controlId, float value);

	// Token: 0x06000120 RID: 288
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExCategory_SetAisacControlByName(string name, string controlName, float value);
}
