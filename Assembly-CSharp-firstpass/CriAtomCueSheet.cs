﻿using System;

// Token: 0x02000006 RID: 6
[Serializable]
public class CriAtomCueSheet
{
	// Token: 0x17000002 RID: 2
	// (get) Token: 0x0600004F RID: 79 RVA: 0x00002DEC File Offset: 0x000011EC
	public bool IsLoading
	{
		get
		{
			return this.loaderStatus == CriAtomExAcbLoader.Status.Loading;
		}
	}

	// Token: 0x17000003 RID: 3
	// (get) Token: 0x06000050 RID: 80 RVA: 0x00002DF7 File Offset: 0x000011F7
	public bool IsError
	{
		get
		{
			return this.loaderStatus == CriAtomExAcbLoader.Status.Error || (!this.IsLoading && this.acb == null);
		}
	}

	// Token: 0x0400004C RID: 76
	public string name = string.Empty;

	// Token: 0x0400004D RID: 77
	public string acbFile = string.Empty;

	// Token: 0x0400004E RID: 78
	public string awbFile = string.Empty;

	// Token: 0x0400004F RID: 79
	public CriAtomExAcb acb;

	// Token: 0x04000050 RID: 80
	public CriAtomExAcbLoader.Status loaderStatus;
}
