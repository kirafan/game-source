﻿using System;

// Token: 0x02000064 RID: 100
public class CriFsInstallRequest : CriFsRequest
{
	// Token: 0x17000035 RID: 53
	// (get) Token: 0x0600032D RID: 813 RVA: 0x00007D27 File Offset: 0x00006127
	// (set) Token: 0x0600032E RID: 814 RVA: 0x00007D2F File Offset: 0x0000612F
	public string sourcePath { get; protected set; }

	// Token: 0x17000036 RID: 54
	// (get) Token: 0x0600032F RID: 815 RVA: 0x00007D38 File Offset: 0x00006138
	// (set) Token: 0x06000330 RID: 816 RVA: 0x00007D40 File Offset: 0x00006140
	public string destinationPath { get; protected set; }

	// Token: 0x17000037 RID: 55
	// (get) Token: 0x06000331 RID: 817 RVA: 0x00007D49 File Offset: 0x00006149
	// (set) Token: 0x06000332 RID: 818 RVA: 0x00007D51 File Offset: 0x00006151
	public float progress { get; protected set; }
}
