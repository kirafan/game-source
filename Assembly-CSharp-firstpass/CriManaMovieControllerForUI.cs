﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x0200007C RID: 124
[AddComponentMenu("CRIWARE/CriManaMovieControllerForUI")]
public class CriManaMovieControllerForUI : CriManaMovieMaterial
{
	// Token: 0x060003BF RID: 959 RVA: 0x000094C4 File Offset: 0x000078C4
	protected override void Start()
	{
		base.Start();
		if (this.target == null)
		{
			this.target = base.gameObject.GetComponent<Graphic>();
		}
		if (this.target == null)
		{
			Debug.LogError("[CRIWARE] error");
			UnityEngine.Object.Destroy(this);
			return;
		}
		this.originalMaterial = this.target.material;
		if (!this.useOriginalMaterial)
		{
			this.target.enabled = false;
		}
	}

	// Token: 0x060003C0 RID: 960 RVA: 0x00009544 File Offset: 0x00007944
	protected override void Update()
	{
		base.Update();
		if (this.renderMode == CriManaMovieMaterial.RenderMode.OnVisibility && !base.HaveRendererOwner && this.target != null && this.target.IsActive())
		{
			base.player.OnWillRenderObject(this);
		}
	}

	// Token: 0x060003C1 RID: 961 RVA: 0x0000959B File Offset: 0x0000799B
	protected override void OnDestroy()
	{
		this.target.material = this.originalMaterial;
		if (!this.useOriginalMaterial)
		{
			this.target.enabled = false;
		}
		this.originalMaterial = null;
		base.OnDestroy();
	}

	// Token: 0x060003C2 RID: 962 RVA: 0x000095D4 File Offset: 0x000079D4
	protected override void OnMaterialAvailableChanged()
	{
		if (base.isMaterialAvailable)
		{
			this.target.material = base.material;
			this.target.enabled = true;
		}
		else
		{
			this.target.material = this.originalMaterial;
			if (!this.useOriginalMaterial)
			{
				this.target.enabled = false;
			}
		}
	}

	// Token: 0x04000231 RID: 561
	public Graphic target;

	// Token: 0x04000232 RID: 562
	public bool useOriginalMaterial;

	// Token: 0x04000233 RID: 563
	private Material originalMaterial;
}
