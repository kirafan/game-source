﻿using System;

// Token: 0x02000065 RID: 101
public class CriFsInstallRequestLegacy : CriFsInstallRequest
{
	// Token: 0x06000333 RID: 819 RVA: 0x00007D5C File Offset: 0x0000615C
	public CriFsInstallRequestLegacy(CriFsBinder srcBinder, string srcPath, string dstPath, CriFsRequest.DoneDelegate doneDelegate, int installBufferSize)
	{
		base.sourcePath = srcPath;
		base.destinationPath = dstPath;
		base.doneDelegate = doneDelegate;
		base.progress = 0f;
		this.installer = new CriFsInstaller();
		this.installer.Copy(srcBinder, srcPath, dstPath, installBufferSize);
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Fs);
	}

	// Token: 0x06000334 RID: 820 RVA: 0x00007DB2 File Offset: 0x000061B2
	public override void Stop()
	{
		if (this.installer != null)
		{
			this.installer.Stop();
		}
	}

	// Token: 0x06000335 RID: 821 RVA: 0x00007DCC File Offset: 0x000061CC
	public override void Update()
	{
		if (this.installer == null)
		{
			return;
		}
		base.progress = this.installer.GetProgress();
		CriFsInstaller.Status status = this.installer.GetStatus();
		if (status == CriFsInstaller.Status.Busy)
		{
			return;
		}
		if (status == CriFsInstaller.Status.Error)
		{
			base.progress = -1f;
			base.error = "Error occurred.";
		}
		this.installer.Dispose();
		this.installer = null;
		base.Done();
	}

	// Token: 0x06000336 RID: 822 RVA: 0x00007E3F File Offset: 0x0000623F
	protected override void Dispose(bool disposing)
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.installer != null)
		{
			this.installer.Dispose();
			this.installer = null;
		}
	}

	// Token: 0x040001C7 RID: 455
	private CriFsInstaller installer;
}
