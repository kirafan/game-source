﻿using System;
using System.Runtime.InteropServices;

// Token: 0x0200002F RID: 47
public static class CriAtomExLatencyEstimator
{
	// Token: 0x06000142 RID: 322 RVA: 0x000052C9 File Offset: 0x000036C9
	public static void InitializeModule()
	{
		CriAtomExLatencyEstimator.criAtomLatencyEstimator_Initialize_ANDROID();
	}

	// Token: 0x06000143 RID: 323 RVA: 0x000052D0 File Offset: 0x000036D0
	public static void FinalizeModule()
	{
		CriAtomExLatencyEstimator.criAtomLatencyEstimator_Finalize_ANDROID();
	}

	// Token: 0x06000144 RID: 324 RVA: 0x000052D7 File Offset: 0x000036D7
	public static CriAtomExLatencyEstimator.EstimatorInfo GetCurrentInfo()
	{
		return CriAtomExLatencyEstimator.criAtomLatencyEstimator_GetCurrentInfo_ANDROID();
	}

	// Token: 0x06000145 RID: 325
	[DllImport("cri_ware_unity")]
	private static extern void criAtomLatencyEstimator_Initialize_ANDROID();

	// Token: 0x06000146 RID: 326
	[DllImport("cri_ware_unity")]
	private static extern void criAtomLatencyEstimator_Finalize_ANDROID();

	// Token: 0x06000147 RID: 327
	[DllImport("cri_ware_unity")]
	private static extern CriAtomExLatencyEstimator.EstimatorInfo criAtomLatencyEstimator_GetCurrentInfo_ANDROID();

	// Token: 0x02000030 RID: 48
	public enum Status
	{
		// Token: 0x04000119 RID: 281
		Stop,
		// Token: 0x0400011A RID: 282
		Processing,
		// Token: 0x0400011B RID: 283
		Done,
		// Token: 0x0400011C RID: 284
		Error
	}

	// Token: 0x02000031 RID: 49
	public struct EstimatorInfo
	{
		// Token: 0x0400011D RID: 285
		public CriAtomExLatencyEstimator.Status status;

		// Token: 0x0400011E RID: 286
		public uint estimated_latency;
	}
}
