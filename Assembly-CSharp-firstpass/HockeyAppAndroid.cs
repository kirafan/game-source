﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

// Token: 0x020000B7 RID: 183
public class HockeyAppAndroid : MonoBehaviour
{
	// Token: 0x06000519 RID: 1305 RVA: 0x0000BFF0 File Offset: 0x0000A3F0
	private void Awake()
	{
		if (HockeyAppAndroid.instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		this.CreateLogDirectory();
		this.appID = "567d78011b4c4748bda94b6a3ddb0f49";
		this.secret = "9fb12de39ea76c50713852f520d5415c";
		if (this.exceptionLogging && this.IsConnected())
		{
			List<string> logFiles = this.GetLogFiles();
			if (logFiles.Count > 0)
			{
				base.StartCoroutine(this.SendLogs(logFiles));
			}
		}
		this.serverURL = this.GetBaseURL();
		int authType = (int)this.authenticatorType;
		this.StartCrashManager(this.serverURL, this.appID, this.secret, authType, this.updateAlert, this.userMetrics, this.autoUploadCrashes);
	}

	// Token: 0x0600051A RID: 1306 RVA: 0x0000C0B5 File Offset: 0x0000A4B5
	private void OnEnable()
	{
		if (this.exceptionLogging)
		{
			AppDomain.CurrentDomain.UnhandledException += this.OnHandleUnresolvedException;
			Application.logMessageReceived += this.OnHandleLogCallback;
		}
	}

	// Token: 0x0600051B RID: 1307 RVA: 0x0000C0E9 File Offset: 0x0000A4E9
	private void OnDisable()
	{
		if (this.exceptionLogging)
		{
			AppDomain.CurrentDomain.UnhandledException -= this.OnHandleUnresolvedException;
			Application.logMessageReceived -= this.OnHandleLogCallback;
		}
	}

	// Token: 0x0600051C RID: 1308 RVA: 0x0000C120 File Offset: 0x0000A520
	protected void StartCrashManager(string urlString, string appID, string secret, int authType, bool updateManagerEnabled, bool userMetricsEnabled, bool autoSendEnabled)
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
		androidJavaClass2.CallStatic("startHockeyAppManager", new object[]
		{
			@static,
			urlString,
			appID,
			secret,
			authType,
			updateManagerEnabled,
			userMetricsEnabled,
			autoSendEnabled
		});
		HockeyAppAndroid.instance = this;
	}

	// Token: 0x0600051D RID: 1309 RVA: 0x0000C1A0 File Offset: 0x0000A5A0
	public static void CheckForUpdate()
	{
		if (HockeyAppAndroid.instance != null)
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
			androidJavaClass2.CallStatic("checkForUpdate", new object[]
			{
				@static,
				HockeyAppAndroid.instance.serverURL,
				HockeyAppAndroid.instance.appID
			});
		}
		else
		{
			Debug.Log("Failed to check for update. SDK has not been initialized, yet.");
		}
	}

	// Token: 0x0600051E RID: 1310 RVA: 0x0000C220 File Offset: 0x0000A620
	public static void ShowFeedbackForm()
	{
		if (HockeyAppAndroid.instance != null)
		{
			AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
			androidJavaClass2.CallStatic("startFeedbackForm", new object[]
			{
				@static
			});
		}
		else
		{
			Debug.Log("Failed to present feedback form. SDK has not been initialized, yet.");
		}
	}

	// Token: 0x0600051F RID: 1311 RVA: 0x0000C284 File Offset: 0x0000A684
	protected string GetVersionCode()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
		return androidJavaClass.CallStatic<string>("getVersionCode", new object[0]);
	}

	// Token: 0x06000520 RID: 1312 RVA: 0x0000C2B4 File Offset: 0x0000A6B4
	protected string GetVersionName()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
		return androidJavaClass.CallStatic<string>("getVersionName", new object[0]);
	}

	// Token: 0x06000521 RID: 1313 RVA: 0x0000C2E4 File Offset: 0x0000A6E4
	protected string GetSdkVersion()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
		return androidJavaClass.CallStatic<string>("getSdkVersion", new object[0]);
	}

	// Token: 0x06000522 RID: 1314 RVA: 0x0000C314 File Offset: 0x0000A714
	protected string GetSdkName()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
		return androidJavaClass.CallStatic<string>("getSdkName", new object[0]);
	}

	// Token: 0x06000523 RID: 1315 RVA: 0x0000C344 File Offset: 0x0000A744
	protected string GetManufacturer()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
		return androidJavaClass.CallStatic<string>("getManufacturer", new object[0]);
	}

	// Token: 0x06000524 RID: 1316 RVA: 0x0000C374 File Offset: 0x0000A774
	protected string GetModel()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
		return androidJavaClass.CallStatic<string>("getModel", new object[0]);
	}

	// Token: 0x06000525 RID: 1317 RVA: 0x0000C3A4 File Offset: 0x0000A7A4
	protected string GetCrashReporterKey()
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("net.hockeyapp.unity.HockeyUnityPlugin");
		return androidJavaClass.CallStatic<string>("getCrashReporterKey", new object[0]);
	}

	// Token: 0x06000526 RID: 1318 RVA: 0x0000C3D4 File Offset: 0x0000A7D4
	protected virtual List<string> GetLogHeaders()
	{
		List<string> list = new List<string>();
		list.Add("Package: " + this.packageID);
		string versionCode = this.GetVersionCode();
		list.Add("Version Code: " + versionCode);
		string versionName = this.GetVersionName();
		list.Add("Version Name: " + versionName);
		string[] array = SystemInfo.operatingSystem.Split(new char[]
		{
			'/'
		});
		string item = "Android: " + array[0].Replace("Android OS ", string.Empty);
		list.Add(item);
		string manufacturer = this.GetManufacturer();
		list.Add("Manufacturer: " + manufacturer);
		string model = this.GetModel();
		list.Add("Model: " + model);
		string crashReporterKey = this.GetCrashReporterKey();
		list.Add("CrashReporter Key: " + crashReporterKey);
		list.Add("Date: " + DateTime.UtcNow.ToString("ddd MMM dd HH:mm:ss {}zzzz yyyy").Replace("{}", "GMT"));
		return list;
	}

	// Token: 0x06000527 RID: 1319 RVA: 0x0000C4EC File Offset: 0x0000A8EC
	protected virtual WWWForm CreateForm(string log)
	{
		WWWForm wwwform = new WWWForm();
		byte[] array = null;
		using (FileStream fileStream = File.OpenRead(log))
		{
			if (fileStream.Length > 199800L)
			{
				string text = null;
				using (StreamReader streamReader = new StreamReader(fileStream))
				{
					streamReader.BaseStream.Seek(fileStream.Length - 199800L, SeekOrigin.Begin);
					text = streamReader.ReadToEnd();
				}
				List<string> logHeaders = this.GetLogHeaders();
				string str = string.Empty;
				foreach (string str2 in logHeaders)
				{
					str = str + str2 + "\n";
				}
				text = str + "\n[...]" + text;
				try
				{
					array = Encoding.Default.GetBytes(text);
				}
				catch (ArgumentException arg)
				{
					if (Debug.isDebugBuild)
					{
						Debug.Log("Failed to read bytes of log file: " + arg);
					}
				}
			}
			else
			{
				try
				{
					array = File.ReadAllBytes(log);
				}
				catch (SystemException arg2)
				{
					if (Debug.isDebugBuild)
					{
						Debug.Log("Failed to read bytes of log file: " + arg2);
					}
				}
			}
		}
		if (array != null)
		{
			wwwform.AddBinaryData("log", array, log, "text/plain");
		}
		return wwwform;
	}

	// Token: 0x06000528 RID: 1320 RVA: 0x0000C6CC File Offset: 0x0000AACC
	protected virtual void CreateLogDirectory()
	{
		string text = Application.persistentDataPath + "/logs/";
		try
		{
			Directory.CreateDirectory(text);
		}
		catch (Exception ex)
		{
			if (Debug.isDebugBuild)
			{
				Debug.Log(string.Concat(new object[]
				{
					"Failed to create log directory at ",
					text,
					": ",
					ex
				}));
			}
		}
	}

	// Token: 0x06000529 RID: 1321 RVA: 0x0000C740 File Offset: 0x0000AB40
	protected virtual List<string> GetLogFiles()
	{
		List<string> list = new List<string>();
		string path = Application.persistentDataPath + "/logs/";
		try
		{
			DirectoryInfo directoryInfo = new DirectoryInfo(path);
			FileInfo[] files = directoryInfo.GetFiles();
			if (files.Length > 0)
			{
				foreach (FileInfo fileInfo in files)
				{
					if (fileInfo.Extension == ".log")
					{
						list.Add(fileInfo.FullName);
					}
					else
					{
						File.Delete(fileInfo.FullName);
					}
				}
			}
		}
		catch (Exception arg)
		{
			if (Debug.isDebugBuild)
			{
				Debug.Log("Failed to write exception log to file: " + arg);
			}
		}
		return list;
	}

	// Token: 0x0600052A RID: 1322 RVA: 0x0000C80C File Offset: 0x0000AC0C
	protected virtual IEnumerator SendLogs(List<string> logs)
	{
		string crashPath = "api/2/apps/[APPID]/crashes/upload";
		string url = this.GetBaseURL() + crashPath.Replace("[APPID]", this.appID);
		string sdkName = this.GetSdkName();
		if (sdkName != null)
		{
			url = url + "?sdk=" + WWW.EscapeURL(sdkName);
		}
		foreach (string log in logs)
		{
			WWWForm postForm = this.CreateForm(log);
			string lContent = postForm.headers["Content-Type"].ToString();
			lContent = lContent.Replace("\"", string.Empty);
			Dictionary<string, string> headers = new Dictionary<string, string>();
			headers.Add("Content-Type", lContent);
			using (WWW www = new WWW(url, postForm.data, headers))
			{
				yield return www;
				if (string.IsNullOrEmpty(www.error))
				{
					try
					{
						File.Delete(log);
					}
					catch (Exception arg)
					{
						if (Debug.isDebugBuild)
						{
							Debug.Log("Failed to delete exception log: " + arg);
						}
					}
				}
			}
		}
		yield break;
	}

	// Token: 0x0600052B RID: 1323 RVA: 0x0000C830 File Offset: 0x0000AC30
	protected virtual void WriteLogToDisk(string logString, string stackTrace)
	{
		string str = DateTime.Now.ToString("yyyy-MM-dd-HH_mm_ss_fff");
		string text = logString.Replace("\n", " ");
		string[] array = stackTrace.Split(new char[]
		{
			'\n'
		});
		text = "\n" + text + "\n";
		foreach (string text2 in array)
		{
			if (text2.Length > 0)
			{
				text = text + "  at " + text2 + "\n";
			}
		}
		List<string> logHeaders = this.GetLogHeaders();
		using (StreamWriter streamWriter = new StreamWriter(Application.persistentDataPath + "/logs/LogFile_" + str + ".log", true))
		{
			foreach (string value in logHeaders)
			{
				streamWriter.WriteLine(value);
			}
			streamWriter.WriteLine(text);
		}
	}

	// Token: 0x0600052C RID: 1324 RVA: 0x0000C964 File Offset: 0x0000AD64
	protected virtual string GetBaseURL()
	{
		string text = string.Empty;
		string text2 = this.serverURL.Trim();
		if (text2.Length > 0)
		{
			text = text2;
			if (!text[text.Length - 1].Equals("/"))
			{
				text += "/";
			}
		}
		else
		{
			text = "https://rink.hockeyapp.net/";
		}
		return text;
	}

	// Token: 0x0600052D RID: 1325 RVA: 0x0000C9D0 File Offset: 0x0000ADD0
	protected virtual bool IsConnected()
	{
		bool result = false;
		if (Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork || Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork)
		{
			result = true;
		}
		return result;
	}

	// Token: 0x0600052E RID: 1326 RVA: 0x0000C9F8 File Offset: 0x0000ADF8
	protected virtual void HandleException(string logString, string stackTrace)
	{
		this.WriteLogToDisk(logString, stackTrace);
	}

	// Token: 0x0600052F RID: 1327 RVA: 0x0000CA02 File Offset: 0x0000AE02
	public void OnHandleLogCallback(string logString, string stackTrace, LogType type)
	{
		if (type == LogType.Assert || type == LogType.Exception || type == LogType.Error)
		{
			this.HandleException(logString, stackTrace);
		}
	}

	// Token: 0x06000530 RID: 1328 RVA: 0x0000CA20 File Offset: 0x0000AE20
	public void OnHandleUnresolvedException(object sender, UnhandledExceptionEventArgs args)
	{
		if (args == null || args.ExceptionObject == null)
		{
			return;
		}
		if (args.ExceptionObject.GetType() == typeof(Exception))
		{
			Exception ex = (Exception)args.ExceptionObject;
			this.HandleException(ex.Source, ex.StackTrace);
		}
	}

	// Token: 0x04000343 RID: 835
	protected const string HOCKEYAPP_BASEURL = "https://rink.hockeyapp.net/";

	// Token: 0x04000344 RID: 836
	protected const string HOCKEYAPP_CRASHESPATH = "api/2/apps/[APPID]/crashes/upload";

	// Token: 0x04000345 RID: 837
	protected const int MAX_CHARS = 199800;

	// Token: 0x04000346 RID: 838
	protected const string LOG_FILE_DIR = "/logs/";

	// Token: 0x04000347 RID: 839
	private static HockeyAppAndroid instance;

	// Token: 0x04000348 RID: 840
	[Header("HockeyApp Setup")]
	public string appID = "your-hockey-app-id";

	// Token: 0x04000349 RID: 841
	public string packageID = "your-package-identifier";

	// Token: 0x0400034A RID: 842
	public string serverURL = "your-custom-server-url";

	// Token: 0x0400034B RID: 843
	[Header("Authentication")]
	public HockeyAppAndroid.AuthenticatorType authenticatorType;

	// Token: 0x0400034C RID: 844
	public string secret = "your-hockey-app-secret";

	// Token: 0x0400034D RID: 845
	[Header("Crashes & Exceptions")]
	public bool autoUploadCrashes;

	// Token: 0x0400034E RID: 846
	public bool exceptionLogging = true;

	// Token: 0x0400034F RID: 847
	[Header("Metrics")]
	public bool userMetrics = true;

	// Token: 0x04000350 RID: 848
	[Header("Version Updates")]
	public bool updateAlert = true;

	// Token: 0x020000B8 RID: 184
	public enum AuthenticatorType
	{
		// Token: 0x04000352 RID: 850
		Anonymous,
		// Token: 0x04000353 RID: 851
		HockeyAppEmail,
		// Token: 0x04000354 RID: 852
		HockeyAppUser,
		// Token: 0x04000355 RID: 853
		Validate
	}
}
