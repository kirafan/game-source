﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000032 RID: 50
public class CriAtomEx3dListener : IDisposable
{
	// Token: 0x06000148 RID: 328 RVA: 0x000052E0 File Offset: 0x000036E0
	public CriAtomEx3dListener()
	{
		CriAtomEx3dListener.Config config = default(CriAtomEx3dListener.Config);
		this.handle = CriAtomEx3dListener.criAtomEx3dListener_Create(ref config, IntPtr.Zero, 0);
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x06000149 RID: 329 RVA: 0x00005320 File Offset: 0x00003720
	public void Dispose()
	{
		this.Dispose(true);
	}

	// Token: 0x0600014A RID: 330 RVA: 0x00005329 File Offset: 0x00003729
	private void Dispose(bool disposing)
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.handle != IntPtr.Zero)
		{
			CriAtomEx3dListener.criAtomEx3dListener_Destroy(this.handle);
			this.handle = IntPtr.Zero;
		}
		if (disposing)
		{
			GC.SuppressFinalize(this);
		}
	}

	// Token: 0x17000019 RID: 25
	// (get) Token: 0x0600014B RID: 331 RVA: 0x00005369 File Offset: 0x00003769
	public IntPtr nativeHandle
	{
		get
		{
			return this.handle;
		}
	}

	// Token: 0x0600014C RID: 332 RVA: 0x00005371 File Offset: 0x00003771
	public void Update()
	{
		CriAtomEx3dListener.criAtomEx3dListener_Update(this.handle);
	}

	// Token: 0x0600014D RID: 333 RVA: 0x0000537E File Offset: 0x0000377E
	public void ResetParameters()
	{
		CriAtomEx3dListener.criAtomEx3dListener_ResetParameters(this.handle);
	}

	// Token: 0x0600014E RID: 334 RVA: 0x0000538C File Offset: 0x0000378C
	public void SetPosition(float x, float y, float z)
	{
		CriAtomEx3dListener.CriAtomExVector criAtomExVector;
		criAtomExVector.x = x;
		criAtomExVector.y = y;
		criAtomExVector.z = z;
		CriAtomEx3dListener.criAtomEx3dListener_SetPosition(this.handle, ref criAtomExVector);
	}

	// Token: 0x0600014F RID: 335 RVA: 0x000053C0 File Offset: 0x000037C0
	public void SetVelocity(float x, float y, float z)
	{
		CriAtomEx3dListener.CriAtomExVector criAtomExVector;
		criAtomExVector.x = x;
		criAtomExVector.y = y;
		criAtomExVector.z = z;
		CriAtomEx3dListener.criAtomEx3dListener_SetVelocity(this.handle, ref criAtomExVector);
	}

	// Token: 0x06000150 RID: 336 RVA: 0x000053F4 File Offset: 0x000037F4
	public void SetOrientation(float fx, float fy, float fz, float ux, float uy, float uz)
	{
		CriAtomEx3dListener.CriAtomExVector criAtomExVector;
		criAtomExVector.x = fx;
		criAtomExVector.y = fy;
		criAtomExVector.z = fz;
		CriAtomEx3dListener.CriAtomExVector criAtomExVector2;
		criAtomExVector2.x = ux;
		criAtomExVector2.y = uy;
		criAtomExVector2.z = uz;
		CriAtomEx3dListener.criAtomEx3dListener_SetOrientation(this.handle, ref criAtomExVector, ref criAtomExVector2);
	}

	// Token: 0x06000151 RID: 337 RVA: 0x00005443 File Offset: 0x00003843
	public void SetDistanceFactor(float distanceFactor)
	{
		CriAtomEx3dListener.criAtomEx3dListener_SetDistanceFactor(this.handle, distanceFactor);
	}

	// Token: 0x06000152 RID: 338 RVA: 0x00005454 File Offset: 0x00003854
	public void SetFocusPoint(float x, float y, float z)
	{
		CriAtomEx3dListener.CriAtomExVector criAtomExVector;
		criAtomExVector.x = x;
		criAtomExVector.y = y;
		criAtomExVector.z = z;
		CriAtomEx3dListener.criAtomEx3dListener_SetFocusPoint(this.handle, ref criAtomExVector);
	}

	// Token: 0x06000153 RID: 339 RVA: 0x00005486 File Offset: 0x00003886
	public void SetDistanceFocusLevel(float distanceFocusLevel)
	{
		CriAtomEx3dListener.criAtomEx3dListener_SetDistanceFocusLevel(this.handle, distanceFocusLevel);
	}

	// Token: 0x06000154 RID: 340 RVA: 0x00005494 File Offset: 0x00003894
	public void SetDirectionFocusLevel(float directionFocusLevel)
	{
		CriAtomEx3dListener.criAtomEx3dListener_SetDirectionFocusLevel(this.handle, directionFocusLevel);
	}

	// Token: 0x06000155 RID: 341 RVA: 0x000054A4 File Offset: 0x000038A4
	~CriAtomEx3dListener()
	{
		this.Dispose(false);
	}

	// Token: 0x06000156 RID: 342
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomEx3dListener_Create(ref CriAtomEx3dListener.Config config, IntPtr work, int work_size);

	// Token: 0x06000157 RID: 343
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_Destroy(IntPtr ex_3d_listener);

	// Token: 0x06000158 RID: 344
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_Update(IntPtr ex_3d_listener);

	// Token: 0x06000159 RID: 345
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_ResetParameters(IntPtr ex_3d_listener);

	// Token: 0x0600015A RID: 346
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_SetPosition(IntPtr ex_3d_listener, ref CriAtomEx3dListener.CriAtomExVector position);

	// Token: 0x0600015B RID: 347
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_SetVelocity(IntPtr ex_3d_listener, ref CriAtomEx3dListener.CriAtomExVector velocity);

	// Token: 0x0600015C RID: 348
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_SetOrientation(IntPtr ex_3d_listener, ref CriAtomEx3dListener.CriAtomExVector front, ref CriAtomEx3dListener.CriAtomExVector top);

	// Token: 0x0600015D RID: 349
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_SetDistanceFactor(IntPtr ex_3d_listener, float distance_factor);

	// Token: 0x0600015E RID: 350
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_SetFocusPoint(IntPtr ex_3d_listener, ref CriAtomEx3dListener.CriAtomExVector focus_point);

	// Token: 0x0600015F RID: 351
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_SetDistanceFocusLevel(IntPtr ex_3d_listener, float distance_focus_level);

	// Token: 0x06000160 RID: 352
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dListener_SetDirectionFocusLevel(IntPtr ex_3d_listener, float direction_focus_level);

	// Token: 0x0400011F RID: 287
	private IntPtr handle = IntPtr.Zero;

	// Token: 0x02000033 RID: 51
	public struct Config
	{
		// Token: 0x04000120 RID: 288
		public int reserved;
	}

	// Token: 0x02000034 RID: 52
	private struct CriAtomExVector
	{
		// Token: 0x04000121 RID: 289
		public float x;

		// Token: 0x04000122 RID: 290
		public float y;

		// Token: 0x04000123 RID: 291
		public float z;
	}
}
