﻿using System;
using UnityEngine;

// Token: 0x0200000E RID: 14
public class CriAtomServer : MonoBehaviour
{
	// Token: 0x17000008 RID: 8
	// (get) Token: 0x0600008C RID: 140 RVA: 0x00004021 File Offset: 0x00002421
	public static CriAtomServer instance
	{
		get
		{
			CriAtomServer.CreateInstance();
			return CriAtomServer._instance;
		}
	}

	// Token: 0x0600008D RID: 141 RVA: 0x0000402D File Offset: 0x0000242D
	public static void CreateInstance()
	{
		if (CriAtomServer._instance == null)
		{
			CriWare.managerObject.AddComponent<CriAtomServer>();
		}
	}

	// Token: 0x0600008E RID: 142 RVA: 0x0000404A File Offset: 0x0000244A
	public static void DestroyInstance()
	{
		if (CriAtomServer._instance != null)
		{
			UnityEngine.Object.Destroy(CriAtomServer._instance);
		}
	}

	// Token: 0x0600008F RID: 143 RVA: 0x00004066 File Offset: 0x00002466
	private void Awake()
	{
		if (CriAtomServer._instance == null)
		{
			CriAtomServer._instance = this;
		}
		else
		{
			UnityEngine.Object.Destroy(this);
		}
	}

	// Token: 0x06000090 RID: 144 RVA: 0x00004089 File Offset: 0x00002489
	private void OnEnable()
	{
	}

	// Token: 0x06000091 RID: 145 RVA: 0x0000408B File Offset: 0x0000248B
	private void OnDisable()
	{
		if (CriAtomServer._instance == this)
		{
			CriAtomServer._instance = null;
		}
	}

	// Token: 0x06000092 RID: 146 RVA: 0x000040A3 File Offset: 0x000024A3
	private void OnApplicationPause(bool appPause)
	{
		this.ProcessApplicationPause(appPause);
	}

	// Token: 0x06000093 RID: 147 RVA: 0x000040AC File Offset: 0x000024AC
	private void ProcessApplicationPause(bool appPause)
	{
		if (this.onApplicationPausePreProcess != null)
		{
			this.onApplicationPausePreProcess(appPause);
		}
		CriAtomPlugin.Pause(appPause);
		if (this.onApplicationPausePostProcess != null)
		{
			this.onApplicationPausePostProcess(appPause);
		}
	}

	// Token: 0x0400006C RID: 108
	private static CriAtomServer _instance;

	// Token: 0x0400006D RID: 109
	public Action<bool> onApplicationPausePreProcess;

	// Token: 0x0400006E RID: 110
	public Action<bool> onApplicationPausePostProcess;
}
