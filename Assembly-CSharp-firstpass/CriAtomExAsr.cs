﻿using System;
using System.Runtime.InteropServices;

// Token: 0x0200002C RID: 44
public class CriAtomExAsr
{
	// Token: 0x0600012C RID: 300 RVA: 0x000050E4 File Offset: 0x000034E4
	public static void AttachBusAnalyzer(int interval, int peakHoldTime)
	{
		CriAtomExAsr.BusAnalyzerConfig busAnalyzerConfig;
		busAnalyzerConfig.interval = 50;
		busAnalyzerConfig.peakHoldTime = 1000;
		for (int i = 0; i < 8; i++)
		{
			CriAtomExAsr.criAtomExAsr_AttachBusAnalyzer(i, ref busAnalyzerConfig);
		}
	}

	// Token: 0x0600012D RID: 301 RVA: 0x00005120 File Offset: 0x00003520
	public static void DetachBusAnalyzer()
	{
		for (int i = 0; i < 8; i++)
		{
			CriAtomExAsr.criAtomExAsr_DetachBusAnalyzer(i);
		}
	}

	// Token: 0x0600012E RID: 302 RVA: 0x00005148 File Offset: 0x00003548
	public static void GetBusAnalyzerInfo(int bus, out CriAtomExAsr.BusAnalyzerInfo info)
	{
		using (CriStructMemory<CriAtomExAsr.BusAnalyzerInfo> criStructMemory = new CriStructMemory<CriAtomExAsr.BusAnalyzerInfo>())
		{
			CriAtomExAsr.criAtomExAsr_GetBusAnalyzerInfo(bus, criStructMemory.ptr);
			info = new CriAtomExAsr.BusAnalyzerInfo(criStructMemory.bytes);
		}
	}

	// Token: 0x0600012F RID: 303 RVA: 0x00005198 File Offset: 0x00003598
	public static void SetBusVolume(int bus, float volume)
	{
		CriAtomExAsr.criAtomExAsr_SetBusVolume(bus, volume);
	}

	// Token: 0x06000130 RID: 304 RVA: 0x000051A1 File Offset: 0x000035A1
	public static void SetBusSendLevel(int bus, int sendTo, float level)
	{
		CriAtomExAsr.criAtomExAsr_SetBusSendLevel(bus, sendTo, level);
	}

	// Token: 0x06000131 RID: 305 RVA: 0x000051AB File Offset: 0x000035AB
	public static void SetBusMatrix(int bus, int inputChannels, int outputChannels, float[] matrix)
	{
		CriAtomExAsr.criAtomExAsr_SetBusMatrix(bus, inputChannels, outputChannels, matrix);
	}

	// Token: 0x06000132 RID: 306 RVA: 0x000051B6 File Offset: 0x000035B6
	public static void SetEffectParameter(string busName, string effectName, uint parameterIndex, float parameterValue)
	{
		CriAtomExAsr.criAtomExAsr_SetEffectParameter(busName, effectName, parameterIndex, parameterValue);
		CriAtomExAsr.criAtomExAsr_UpdateEffectParameters(busName, effectName);
	}

	// Token: 0x06000133 RID: 307 RVA: 0x000051C8 File Offset: 0x000035C8
	public static float GetEffectParameter(string busName, string effectName, uint parameterIndex)
	{
		return CriAtomExAsr.criAtomExAsr_GetEffectParameter(busName, effectName, parameterIndex);
	}

	// Token: 0x06000134 RID: 308 RVA: 0x000051D2 File Offset: 0x000035D2
	public static bool RegisterEffectInterface(IntPtr afx_interface)
	{
		return CriAtomExAsr.criAtomExAsr_RegisterEffectInterface(afx_interface);
	}

	// Token: 0x06000135 RID: 309 RVA: 0x000051DA File Offset: 0x000035DA
	public static void UnregisterEffectInterface(IntPtr afx_interface)
	{
		CriAtomExAsr.criAtomExAsr_UnregisterEffectInterface(afx_interface);
	}

	// Token: 0x06000136 RID: 310
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsr_AttachBusAnalyzer(int bus_no, ref CriAtomExAsr.BusAnalyzerConfig config);

	// Token: 0x06000137 RID: 311
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsr_DetachBusAnalyzer(int bus_no);

	// Token: 0x06000138 RID: 312
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsr_GetBusAnalyzerInfo(int bus_no, IntPtr info);

	// Token: 0x06000139 RID: 313
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsr_SetBusVolume(int bus_no, float volume);

	// Token: 0x0600013A RID: 314
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsr_SetBusSendLevel(int bus_no, int sendto_no, float level);

	// Token: 0x0600013B RID: 315
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsr_SetBusMatrix(int bus_no, int input_channels, int output_channels, [MarshalAs(UnmanagedType.LPArray)] float[] matrix);

	// Token: 0x0600013C RID: 316
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsr_UpdateEffectParameters(string busName, string effectName);

	// Token: 0x0600013D RID: 317
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsr_SetEffectParameter(string busName, string effectName, uint parameterIndex, float parameterValue);

	// Token: 0x0600013E RID: 318
	[DllImport("cri_ware_unity")]
	private static extern float criAtomExAsr_GetEffectParameter(string busName, string effectName, uint parameterIndex);

	// Token: 0x0600013F RID: 319
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAsr_RegisterEffectInterface(IntPtr afx_interface);

	// Token: 0x06000140 RID: 320
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsr_UnregisterEffectInterface(IntPtr afx_interface);

	// Token: 0x0200002D RID: 45
	private struct BusAnalyzerConfig
	{
		// Token: 0x04000112 RID: 274
		public int interval;

		// Token: 0x04000113 RID: 275
		public int peakHoldTime;
	}

	// Token: 0x0200002E RID: 46
	public struct BusAnalyzerInfo
	{
		// Token: 0x06000141 RID: 321 RVA: 0x000051E4 File Offset: 0x000035E4
		public BusAnalyzerInfo(byte[] data)
		{
			if (data != null)
			{
				this.numChannels = BitConverter.ToInt32(data, 0);
				this.rmsLevels = new float[8];
				for (int i = 0; i < 8; i++)
				{
					this.rmsLevels[i] = BitConverter.ToSingle(data, 4 + i * 4);
				}
				this.peakLevels = new float[8];
				for (int j = 0; j < 8; j++)
				{
					this.peakLevels[j] = BitConverter.ToSingle(data, 36 + j * 4);
				}
				this.peakHoldLevels = new float[8];
				for (int k = 0; k < 8; k++)
				{
					this.peakHoldLevels[k] = BitConverter.ToSingle(data, 68 + k * 4);
				}
			}
			else
			{
				this.numChannels = 0;
				this.rmsLevels = new float[8];
				this.peakLevels = new float[8];
				this.peakHoldLevels = new float[8];
			}
		}

		// Token: 0x04000114 RID: 276
		public int numChannels;

		// Token: 0x04000115 RID: 277
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public float[] rmsLevels;

		// Token: 0x04000116 RID: 278
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public float[] peakLevels;

		// Token: 0x04000117 RID: 279
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public float[] peakHoldLevels;
	}
}
