﻿using System;
using System.Runtime.InteropServices;

// Token: 0x0200005D RID: 93
public class CriFsBinder : IDisposable
{
	// Token: 0x060002ED RID: 749 RVA: 0x00007448 File Offset: 0x00005848
	public CriFsBinder()
	{
		if (!CriFsPlugin.isInitialized)
		{
			throw new Exception("CriFsPlugin is not initialized.");
		}
		this.handle = IntPtr.Zero;
		CriFsBinder.criFsBinder_Create(out this.handle);
		if (this.handle == IntPtr.Zero)
		{
			throw new Exception("criFsBinder_Create() failed.");
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Fs);
	}

	// Token: 0x060002EE RID: 750 RVA: 0x000074AE File Offset: 0x000058AE
	public void Dispose()
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.handle != IntPtr.Zero)
		{
			CriFsBinder.criFsBinder_Destroy(this.handle);
			this.handle = IntPtr.Zero;
		}
		GC.SuppressFinalize(this);
	}

	// Token: 0x060002EF RID: 751 RVA: 0x000074EC File Offset: 0x000058EC
	public uint BindCpk(CriFsBinder srcBinder, string path)
	{
		uint result = 0U;
		if (this.handle != IntPtr.Zero)
		{
			CriFsBinder.criFsBinder_BindCpk(this.handle, (srcBinder == null) ? IntPtr.Zero : srcBinder.nativeHandle, path, IntPtr.Zero, 0, out result);
		}
		return result;
	}

	// Token: 0x060002F0 RID: 752 RVA: 0x0000753C File Offset: 0x0000593C
	public uint BindDirectory(CriFsBinder srcBinder, string path)
	{
		uint result = 0U;
		if (this.handle != IntPtr.Zero)
		{
			CriFsBinder.criFsBinder_BindDirectory(this.handle, (srcBinder == null) ? IntPtr.Zero : srcBinder.nativeHandle, path, IntPtr.Zero, 0, out result);
		}
		return result;
	}

	// Token: 0x060002F1 RID: 753 RVA: 0x0000758C File Offset: 0x0000598C
	public uint BindFile(CriFsBinder srcBinder, string path)
	{
		uint result = 0U;
		CriFsBinder.criFsBinder_BindFile(this.handle, (srcBinder == null) ? IntPtr.Zero : srcBinder.nativeHandle, path, IntPtr.Zero, 0, out result);
		return result;
	}

	// Token: 0x060002F2 RID: 754 RVA: 0x000075C8 File Offset: 0x000059C8
	public uint BindFileSection(CriFsBinder srcBinder, string path, ulong offset, int size, string sectionName)
	{
		uint result = 0U;
		if (this.handle != IntPtr.Zero)
		{
			CriFsBinder.criFsBinder_BindFileSection(this.handle, (srcBinder == null) ? IntPtr.Zero : srcBinder.nativeHandle, path, offset, size, sectionName, IntPtr.Zero, 0, out result);
		}
		return result;
	}

	// Token: 0x060002F3 RID: 755 RVA: 0x0000761D File Offset: 0x00005A1D
	public static void Unbind(uint bindId)
	{
		if (CriFsPlugin.isInitialized)
		{
			CriFsBinder.criFsBinder_Unbind(bindId);
		}
	}

	// Token: 0x060002F4 RID: 756 RVA: 0x00007630 File Offset: 0x00005A30
	public static CriFsBinder.Status GetStatus(uint bindId)
	{
		CriFsBinder.Status result = CriFsBinder.Status.Removed;
		if (CriFsPlugin.isInitialized)
		{
			CriFsBinder.criFsBinder_GetStatus(bindId, out result);
		}
		return result;
	}

	// Token: 0x060002F5 RID: 757 RVA: 0x00007654 File Offset: 0x00005A54
	public long GetFileSize(string path)
	{
		long result = -1L;
		if (this.handle != IntPtr.Zero)
		{
			int num = CriFsBinder.criFsBinder_GetFileSize(this.handle, path, out result);
			if (num != 0)
			{
				return -1L;
			}
		}
		return result;
	}

	// Token: 0x060002F6 RID: 758 RVA: 0x00007694 File Offset: 0x00005A94
	public long GetFileSize(int id)
	{
		long result = -1L;
		if (this.handle != IntPtr.Zero)
		{
			int num = CriFsBinder.criFsBinder_GetFileSizeById(this.handle, id, out result);
			if (num != 0)
			{
				return -1L;
			}
		}
		return result;
	}

	// Token: 0x060002F7 RID: 759 RVA: 0x000076D2 File Offset: 0x00005AD2
	public static void SetPriority(uint bindId, int priority)
	{
		if (CriFsPlugin.isInitialized)
		{
			CriFsBinder.criFsBinder_SetPriority(bindId, priority);
		}
	}

	// Token: 0x1700002C RID: 44
	// (get) Token: 0x060002F8 RID: 760 RVA: 0x000076E6 File Offset: 0x00005AE6
	public IntPtr nativeHandle
	{
		get
		{
			return this.handle;
		}
	}

	// Token: 0x060002F9 RID: 761 RVA: 0x000076F0 File Offset: 0x00005AF0
	~CriFsBinder()
	{
		this.Dispose();
	}

	// Token: 0x060002FA RID: 762
	[DllImport("cri_ware_unity")]
	private static extern uint criFsBinder_Create(out IntPtr binder);

	// Token: 0x060002FB RID: 763
	[DllImport("cri_ware_unity")]
	private static extern uint criFsBinder_Destroy(IntPtr binder);

	// Token: 0x060002FC RID: 764
	[DllImport("cri_ware_unity")]
	private static extern uint criFsBinder_BindCpk(IntPtr binder, IntPtr srcBinder, string path, IntPtr work, int worksize, out uint bindId);

	// Token: 0x060002FD RID: 765
	[DllImport("cri_ware_unity")]
	private static extern uint criFsBinder_BindDirectory(IntPtr binder, IntPtr srcBinder, string path, IntPtr work, int worksize, out uint bindId);

	// Token: 0x060002FE RID: 766
	[DllImport("cri_ware_unity")]
	private static extern uint criFsBinder_BindFile(IntPtr binder, IntPtr srcBinder, string path, IntPtr work, int worksize, out uint bindId);

	// Token: 0x060002FF RID: 767
	[DllImport("cri_ware_unity")]
	private static extern uint criFsBinder_BindFileSection(IntPtr binder, IntPtr srcBinder, string path, ulong offset, int size, string sectionName, IntPtr work, int worksize, out uint bindId);

	// Token: 0x06000300 RID: 768
	[DllImport("cri_ware_unity")]
	private static extern int criFsBinder_Unbind(uint bindId);

	// Token: 0x06000301 RID: 769
	[DllImport("cri_ware_unity")]
	private static extern int criFsBinder_GetStatus(uint bindId, out CriFsBinder.Status status);

	// Token: 0x06000302 RID: 770
	[DllImport("cri_ware_unity")]
	private static extern int criFsBinder_GetFileSize(IntPtr binder, string path, out long size);

	// Token: 0x06000303 RID: 771
	[DllImport("cri_ware_unity")]
	private static extern int criFsBinder_GetFileSizeById(IntPtr binder, int id, out long size);

	// Token: 0x06000304 RID: 772
	[DllImport("cri_ware_unity")]
	private static extern int criFsBinder_SetPriority(uint bindId, int priority);

	// Token: 0x040001A4 RID: 420
	private IntPtr handle;

	// Token: 0x0200005E RID: 94
	public enum Status
	{
		// Token: 0x040001A6 RID: 422
		None,
		// Token: 0x040001A7 RID: 423
		Analyze,
		// Token: 0x040001A8 RID: 424
		Complete,
		// Token: 0x040001A9 RID: 425
		Unbind,
		// Token: 0x040001AA RID: 426
		Removed,
		// Token: 0x040001AB RID: 427
		Invalid,
		// Token: 0x040001AC RID: 428
		Error
	}
}
