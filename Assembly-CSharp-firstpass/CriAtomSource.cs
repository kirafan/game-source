﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200000F RID: 15
[AddComponentMenu("CRIWARE/CRI Atom Source")]
public class CriAtomSource : MonoBehaviour
{
	// Token: 0x17000009 RID: 9
	// (get) Token: 0x06000097 RID: 151 RVA: 0x00004124 File Offset: 0x00002524
	// (set) Token: 0x06000096 RID: 150 RVA: 0x0000411B File Offset: 0x0000251B
	public CriAtomExPlayer player { get; protected set; }

	// Token: 0x1700000A RID: 10
	// (get) Token: 0x06000098 RID: 152 RVA: 0x0000412C File Offset: 0x0000252C
	// (set) Token: 0x06000099 RID: 153 RVA: 0x00004134 File Offset: 0x00002534
	public bool playOnStart
	{
		get
		{
			return this._playOnStart;
		}
		set
		{
			this._playOnStart = value;
		}
	}

	// Token: 0x1700000B RID: 11
	// (get) Token: 0x0600009A RID: 154 RVA: 0x0000413D File Offset: 0x0000253D
	// (set) Token: 0x0600009B RID: 155 RVA: 0x00004145 File Offset: 0x00002545
	public string cueName
	{
		get
		{
			return this._cueName;
		}
		set
		{
			this._cueName = value;
		}
	}

	// Token: 0x1700000C RID: 12
	// (get) Token: 0x0600009C RID: 156 RVA: 0x0000414E File Offset: 0x0000254E
	// (set) Token: 0x0600009D RID: 157 RVA: 0x00004156 File Offset: 0x00002556
	public string cueSheet
	{
		get
		{
			return this._cueSheet;
		}
		set
		{
			this._cueSheet = value;
		}
	}

	// Token: 0x1700000D RID: 13
	// (get) Token: 0x0600009F RID: 159 RVA: 0x0000419B File Offset: 0x0000259B
	// (set) Token: 0x0600009E RID: 158 RVA: 0x0000415F File Offset: 0x0000255F
	public bool use3dPositioning
	{
		get
		{
			return this._use3dPositioning;
		}
		set
		{
			this._use3dPositioning = value;
			if (this.player != null)
			{
				this.player.Set3dSource((!this.use3dPositioning) ? null : this.source);
				this.SetNeedToPlayerUpdateAll();
			}
		}
	}

	// Token: 0x1700000E RID: 14
	// (get) Token: 0x060000A1 RID: 161 RVA: 0x000041AC File Offset: 0x000025AC
	// (set) Token: 0x060000A0 RID: 160 RVA: 0x000041A3 File Offset: 0x000025A3
	public bool loop
	{
		get
		{
			return this._loop;
		}
		set
		{
			this._loop = value;
		}
	}

	// Token: 0x1700000F RID: 15
	// (get) Token: 0x060000A3 RID: 163 RVA: 0x000041DF File Offset: 0x000025DF
	// (set) Token: 0x060000A2 RID: 162 RVA: 0x000041B4 File Offset: 0x000025B4
	public float volume
	{
		get
		{
			return this._volume;
		}
		set
		{
			this._volume = value;
			if (this.player != null)
			{
				this.player.SetVolume(this._volume);
				this.SetNeedToPlayerUpdateAll();
			}
		}
	}

	// Token: 0x17000010 RID: 16
	// (get) Token: 0x060000A5 RID: 165 RVA: 0x00004212 File Offset: 0x00002612
	// (set) Token: 0x060000A4 RID: 164 RVA: 0x000041E7 File Offset: 0x000025E7
	public float pitch
	{
		get
		{
			return this._pitch;
		}
		set
		{
			this._pitch = value;
			if (this.player != null)
			{
				this.player.SetPitch(this._pitch);
				this.SetNeedToPlayerUpdateAll();
			}
		}
	}

	// Token: 0x17000011 RID: 17
	// (get) Token: 0x060000A7 RID: 167 RVA: 0x0000422E File Offset: 0x0000262E
	// (set) Token: 0x060000A6 RID: 166 RVA: 0x0000421A File Offset: 0x0000261A
	public float pan3dAngle
	{
		get
		{
			return this.player.GetParameterFloat32(CriAtomEx.Parameter.Pan3dAngle);
		}
		set
		{
			this.player.SetPan3dAngle(value);
			this.SetNeedToPlayerUpdateAll();
		}
	}

	// Token: 0x17000012 RID: 18
	// (get) Token: 0x060000A9 RID: 169 RVA: 0x00004250 File Offset: 0x00002650
	// (set) Token: 0x060000A8 RID: 168 RVA: 0x0000423C File Offset: 0x0000263C
	public float pan3dDistance
	{
		get
		{
			return this.player.GetParameterFloat32(CriAtomEx.Parameter.Pan3dDistance);
		}
		set
		{
			this.player.SetPan3dInteriorDistance(value);
			this.SetNeedToPlayerUpdateAll();
		}
	}

	// Token: 0x17000013 RID: 19
	// (get) Token: 0x060000AB RID: 171 RVA: 0x00004273 File Offset: 0x00002673
	// (set) Token: 0x060000AA RID: 170 RVA: 0x0000425E File Offset: 0x0000265E
	public int startTime
	{
		get
		{
			return this.player.GetParameterSint32(CriAtomEx.Parameter.StartTime);
		}
		set
		{
			this.player.SetStartTime((long)value);
			this.SetNeedToPlayerUpdateAll();
		}
	}

	// Token: 0x17000014 RID: 20
	// (get) Token: 0x060000AC RID: 172 RVA: 0x00004282 File Offset: 0x00002682
	public long time
	{
		get
		{
			return (this.player == null) ? 0L : this.player.GetTime();
		}
	}

	// Token: 0x17000015 RID: 21
	// (get) Token: 0x060000AD RID: 173 RVA: 0x000042A1 File Offset: 0x000026A1
	public CriAtomSource.Status status
	{
		get
		{
			return (CriAtomSource.Status)((this.player == null) ? CriAtomExPlayer.Status.Error : this.player.GetStatus());
		}
	}

	// Token: 0x17000016 RID: 22
	// (get) Token: 0x060000AE RID: 174 RVA: 0x000042BF File Offset: 0x000026BF
	// (set) Token: 0x060000AF RID: 175 RVA: 0x000042C7 File Offset: 0x000026C7
	public bool androidUseLowLatencyVoicePool
	{
		get
		{
			return this._androidUseLowLatencyVoicePool;
		}
		set
		{
			this._androidUseLowLatencyVoicePool = value;
		}
	}

	// Token: 0x060000B0 RID: 176 RVA: 0x000042D0 File Offset: 0x000026D0
	protected void SetNeedToPlayerUpdateAll()
	{
		this.need_to_player_update_all = true;
	}

	// Token: 0x060000B1 RID: 177 RVA: 0x000042D9 File Offset: 0x000026D9
	protected virtual void InternalInitialize()
	{
		CriAtomPlugin.InitializeLibrary();
		this.player = new CriAtomExPlayer();
		this.source = new CriAtomEx3dSource();
	}

	// Token: 0x060000B2 RID: 178 RVA: 0x000042F6 File Offset: 0x000026F6
	protected virtual void InternalFinalize()
	{
		this.player.Dispose();
		this.player = null;
		this.source.Dispose();
		this.source = null;
		CriAtomPlugin.FinalizeLibrary();
	}

	// Token: 0x060000B3 RID: 179 RVA: 0x00004321 File Offset: 0x00002721
	private void Awake()
	{
		this.InternalInitialize();
	}

	// Token: 0x060000B4 RID: 180 RVA: 0x00004329 File Offset: 0x00002729
	private void OnEnable()
	{
		this.hasValidPosition = false;
		this.SetInitialParameters();
		this.SetNeedToPlayerUpdateAll();
	}

	// Token: 0x060000B5 RID: 181 RVA: 0x0000433E File Offset: 0x0000273E
	private void OnDestroy()
	{
		this.InternalFinalize();
	}

	// Token: 0x060000B6 RID: 182 RVA: 0x00004348 File Offset: 0x00002748
	protected bool SetInitialSourcePosition()
	{
		Vector3 position = base.transform.position;
		this.lastPosition = position;
		if (this.source != null)
		{
			this.source.SetPosition(position.x, position.y, position.z);
			this.source.Update();
			return true;
		}
		return false;
	}

	// Token: 0x060000B7 RID: 183 RVA: 0x000043A4 File Offset: 0x000027A4
	protected virtual void SetInitialParameters()
	{
		this.use3dPositioning = this.use3dPositioning;
		this.player.Set3dListener(CriAtomListener.sharedNativeListener);
		if (!this.SetInitialSourcePosition())
		{
			Debug.LogError("[ADX2][SetInitialParameters] source is null.", this);
		}
		this.player.SetVolume(this._volume);
		this.player.SetPitch(this._pitch);
	}

	// Token: 0x060000B8 RID: 184 RVA: 0x00004405 File Offset: 0x00002805
	private void Start()
	{
		this.PlayOnStart();
	}

	// Token: 0x060000B9 RID: 185 RVA: 0x00004410 File Offset: 0x00002810
	private void LateUpdate()
	{
		Vector3 position = base.transform.position;
		Vector3 vector = (position - this.lastPosition) / Time.deltaTime;
		this.lastPosition = position;
		this.source.SetPosition(position.x, position.y, position.z);
		if (this.hasValidPosition)
		{
			this.source.SetVelocity(vector.x, vector.y, vector.z);
		}
		this.source.Update();
		this.hasValidPosition = true;
		if (this.need_to_player_update_all)
		{
			this.player.UpdateAll();
			this.need_to_player_update_all = false;
		}
	}

	// Token: 0x060000BA RID: 186 RVA: 0x000044C4 File Offset: 0x000028C4
	public void OnDrawGizmos()
	{
		if (Application.isPlaying && this.status == CriAtomSource.Status.Playing)
		{
			Gizmos.DrawIcon(base.transform.position, "Criware/VoiceOn.png");
		}
		else
		{
			Gizmos.DrawIcon(base.transform.position, "Criware/VoiceOff.png");
		}
	}

	// Token: 0x060000BB RID: 187 RVA: 0x00004516 File Offset: 0x00002916
	public CriAtomExPlayback Play()
	{
		return this.Play(this.cueName);
	}

	// Token: 0x060000BC RID: 188 RVA: 0x00004524 File Offset: 0x00002924
	public CriAtomExPlayback Play(string cueName)
	{
		CriAtomExAcb acb = null;
		if (!string.IsNullOrEmpty(this.cueSheet))
		{
			acb = CriAtom.GetAcb(this.cueSheet);
		}
		this.player.SetCue(acb, cueName);
		if (this.androidUseLowLatencyVoicePool)
		{
			this.player.SetSoundRendererType(CriAtomEx.SoundRendererType.Native);
		}
		else
		{
			this.player.SetSoundRendererType(CriAtomEx.SoundRendererType.Asr);
		}
		if (!this.hasValidPosition)
		{
			this.SetInitialSourcePosition();
			this.hasValidPosition = true;
		}
		if (this.status == CriAtomSource.Status.Stop)
		{
			this.player.Loop(this._loop);
		}
		return this.player.Start();
	}

	// Token: 0x060000BD RID: 189 RVA: 0x000045C4 File Offset: 0x000029C4
	public CriAtomExPlayback Play(int cueId)
	{
		CriAtomExAcb acb = null;
		if (!string.IsNullOrEmpty(this.cueSheet))
		{
			acb = CriAtom.GetAcb(this.cueSheet);
		}
		this.player.SetCue(acb, cueId);
		if (this.androidUseLowLatencyVoicePool)
		{
			this.player.SetSoundRendererType(CriAtomEx.SoundRendererType.Native);
		}
		else
		{
			this.player.SetSoundRendererType(CriAtomEx.SoundRendererType.Asr);
		}
		if (!this.hasValidPosition)
		{
			this.SetInitialSourcePosition();
			this.hasValidPosition = true;
		}
		if (this.status == CriAtomSource.Status.Stop)
		{
			this.player.Loop(this._loop);
		}
		return this.player.Start();
	}

	// Token: 0x060000BE RID: 190 RVA: 0x00004664 File Offset: 0x00002A64
	private void PlayOnStart()
	{
		if (this.playOnStart && !string.IsNullOrEmpty(this.cueName))
		{
			base.StartCoroutine(this.PlayAsync(this.cueName));
		}
	}

	// Token: 0x060000BF RID: 191 RVA: 0x00004694 File Offset: 0x00002A94
	private IEnumerator PlayAsync(string cueName)
	{
		CriAtomExAcb acb = null;
		while (acb == null)
		{
			if (!string.IsNullOrEmpty(this.cueSheet))
			{
				acb = CriAtom.GetAcb(this.cueSheet);
			}
			if (acb == null)
			{
				yield return null;
			}
		}
		this.player.SetCue(acb, cueName);
		if (this.androidUseLowLatencyVoicePool)
		{
			this.player.SetSoundRendererType(CriAtomEx.SoundRendererType.Native);
		}
		else
		{
			this.player.SetSoundRendererType(CriAtomEx.SoundRendererType.Asr);
		}
		if (!this.hasValidPosition)
		{
			this.SetInitialSourcePosition();
			this.hasValidPosition = true;
		}
		if (this.status == CriAtomSource.Status.Stop)
		{
			this.player.Loop(this._loop);
		}
		this.player.Start();
		yield break;
	}

	// Token: 0x060000C0 RID: 192 RVA: 0x000046B6 File Offset: 0x00002AB6
	public void Stop()
	{
		this.player.Stop();
	}

	// Token: 0x060000C1 RID: 193 RVA: 0x000046C3 File Offset: 0x00002AC3
	public void Pause(bool sw)
	{
		if (!sw)
		{
			this.player.Resume(CriAtomEx.ResumeMode.PausedPlayback);
		}
		else
		{
			this.player.Pause();
		}
	}

	// Token: 0x060000C2 RID: 194 RVA: 0x000046E7 File Offset: 0x00002AE7
	public bool IsPaused()
	{
		return this.player.IsPaused();
	}

	// Token: 0x060000C3 RID: 195 RVA: 0x000046F4 File Offset: 0x00002AF4
	public void SetBusSendLevel(string busName, float level)
	{
		if (this.player != null)
		{
			this.player.SetBusSendLevel(busName, level);
			this.SetNeedToPlayerUpdateAll();
		}
	}

	// Token: 0x060000C4 RID: 196 RVA: 0x00004714 File Offset: 0x00002B14
	public void SetBusSendLevel(int busId, float level)
	{
		if (this.player != null)
		{
			this.player.SetBusSendLevel(busId, level);
			this.SetNeedToPlayerUpdateAll();
		}
	}

	// Token: 0x060000C5 RID: 197 RVA: 0x00004734 File Offset: 0x00002B34
	public void SetBusSendLevelOffset(string busName, float levelOffset)
	{
		if (this.player != null)
		{
			this.player.SetBusSendLevelOffset(busName, levelOffset);
			this.SetNeedToPlayerUpdateAll();
		}
	}

	// Token: 0x060000C6 RID: 198 RVA: 0x00004754 File Offset: 0x00002B54
	public void SetBusSendLevelOffset(int busId, float levelOffset)
	{
		if (this.player != null)
		{
			this.player.SetBusSendLevelOffset(busId, levelOffset);
			this.SetNeedToPlayerUpdateAll();
		}
	}

	// Token: 0x060000C7 RID: 199 RVA: 0x00004774 File Offset: 0x00002B74
	public void SetAisacControl(string controlName, float value)
	{
		if (this.player != null)
		{
			this.player.SetAisacControl(controlName, value);
			this.SetNeedToPlayerUpdateAll();
		}
	}

	// Token: 0x060000C8 RID: 200 RVA: 0x00004794 File Offset: 0x00002B94
	[Obsolete("Use CriAtomSource.SetAisacControl")]
	public void SetAisac(string controlName, float value)
	{
		this.SetAisacControl(controlName, value);
	}

	// Token: 0x060000C9 RID: 201 RVA: 0x0000479E File Offset: 0x00002B9E
	public void SetAisacControl(uint controlId, float value)
	{
		if (this.player != null)
		{
			this.player.SetAisacControl(controlId, value);
			this.SetNeedToPlayerUpdateAll();
		}
	}

	// Token: 0x060000CA RID: 202 RVA: 0x000047BE File Offset: 0x00002BBE
	[Obsolete("Use CriAtomSource.SetAisacControl")]
	public void SetAisac(uint controlId, float value)
	{
		this.SetAisacControl(controlId, value);
	}

	// Token: 0x060000CB RID: 203 RVA: 0x000047C8 File Offset: 0x00002BC8
	public void AttachToAnalyzer(CriAtomExPlayerOutputAnalyzer analyzer)
	{
		if (this.player != null)
		{
			analyzer.AttachExPlayer(this.player);
		}
	}

	// Token: 0x060000CC RID: 204 RVA: 0x000047E2 File Offset: 0x00002BE2
	public void DetachFromAnalyzer(CriAtomExPlayerOutputAnalyzer analyzer)
	{
		analyzer.DetachExPlayer();
	}

	// Token: 0x04000070 RID: 112
	protected CriAtomEx3dSource source;

	// Token: 0x04000071 RID: 113
	private Vector3 lastPosition;

	// Token: 0x04000072 RID: 114
	private bool hasValidPosition;

	// Token: 0x04000073 RID: 115
	[SerializeField]
	private bool _playOnStart;

	// Token: 0x04000074 RID: 116
	[SerializeField]
	private string _cueName = string.Empty;

	// Token: 0x04000075 RID: 117
	[SerializeField]
	private string _cueSheet = string.Empty;

	// Token: 0x04000076 RID: 118
	[SerializeField]
	private bool _use3dPositioning = true;

	// Token: 0x04000077 RID: 119
	[SerializeField]
	private bool _loop;

	// Token: 0x04000078 RID: 120
	[SerializeField]
	private float _volume = 1f;

	// Token: 0x04000079 RID: 121
	[SerializeField]
	private float _pitch;

	// Token: 0x0400007A RID: 122
	[SerializeField]
	private bool _androidUseLowLatencyVoicePool;

	// Token: 0x0400007B RID: 123
	[SerializeField]
	private bool need_to_player_update_all = true;

	// Token: 0x02000010 RID: 16
	public enum Status
	{
		// Token: 0x0400007D RID: 125
		Stop,
		// Token: 0x0400007E RID: 126
		Prep,
		// Token: 0x0400007F RID: 127
		Playing,
		// Token: 0x04000080 RID: 128
		PlayEnd,
		// Token: 0x04000081 RID: 129
		Error
	}
}
