﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000056 RID: 86
public class CriAtomExStandardVoicePool : CriAtomExVoicePool
{
	// Token: 0x060002C5 RID: 709 RVA: 0x00006CE4 File Offset: 0x000050E4
	public CriAtomExStandardVoicePool(int numVoices, int maxChannels, int maxSamplingRate, bool streamingFlag, uint identifier = 0U)
	{
		this._identifier = identifier;
		this._numVoices = numVoices;
		this._maxChannels = maxChannels;
		this._maxSamplingRate = maxSamplingRate;
		CriAtomExVoicePool.VoicePoolConfig voicePoolConfig = default(CriAtomExVoicePool.VoicePoolConfig);
		voicePoolConfig.identifier = identifier;
		voicePoolConfig.numVoices = numVoices;
		voicePoolConfig.playerConfig.maxChannels = maxChannels;
		voicePoolConfig.playerConfig.maxSamplingRate = maxSamplingRate;
		voicePoolConfig.playerConfig.streamingFlag = streamingFlag;
		voicePoolConfig.playerConfig.soundRendererType = 2;
		voicePoolConfig.playerConfig.decodeLatency = 0;
		this._handle = CriAtomExStandardVoicePool.criAtomExVoicePool_AllocateStandardVoicePool(ref voicePoolConfig, IntPtr.Zero, 0);
		if (this._handle == IntPtr.Zero)
		{
			throw new Exception("CriAtomExStandardVoicePool() failed.");
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x060002C6 RID: 710
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExVoicePool_AllocateStandardVoicePool(ref CriAtomExVoicePool.VoicePoolConfig config, IntPtr work, int work_size);
}
