﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Store;
using UnityEngine.UI;

// Token: 0x020000D6 RID: 214
[AddComponentMenu("Unity IAP/Demo")]
public class IAPDemo : MonoBehaviour, IStoreListener
{
	// Token: 0x06000620 RID: 1568 RVA: 0x00011AC0 File Offset: 0x0000FEC0
	public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
	{
		this.m_Controller = controller;
		this.m_AppleExtensions = extensions.GetExtension<IAppleExtensions>();
		this.m_SamsungExtensions = extensions.GetExtension<ISamsungAppsExtensions>();
		this.m_MoolahExtensions = extensions.GetExtension<IMoolahExtension>();
		this.m_MicrosoftExtensions = extensions.GetExtension<IMicrosoftExtensions>();
		this.m_UnityChannelExtensions = extensions.GetExtension<IUnityChannelExtensions>();
		this.InitUI(controller.products.all);
		this.m_AppleExtensions.RegisterPurchaseDeferredListener(new Action<Product>(this.OnDeferred));
		Debug.Log("Available items:");
		foreach (Product product in controller.products.all)
		{
			if (product.availableToPurchase)
			{
				Debug.Log(string.Join(" - ", new string[]
				{
					product.metadata.localizedTitle,
					product.metadata.localizedDescription,
					product.metadata.isoCurrencyCode,
					product.metadata.localizedPrice.ToString(),
					product.metadata.localizedPriceString,
					product.transactionID,
					product.receipt
				}));
			}
		}
		if (this.m_Controller.products.all.Length > 0)
		{
			this.m_SelectedItemIndex = 0;
		}
		for (int j = 0; j < this.m_Controller.products.all.Length; j++)
		{
			Product product2 = this.m_Controller.products.all[j];
			string text = string.Format("{0} | {1} => {2}", product2.metadata.localizedTitle, product2.metadata.localizedPriceString, product2.metadata.localizedPrice);
			this.GetDropdown().options[j] = new Dropdown.OptionData(text);
		}
		this.GetDropdown().RefreshShownValue();
		this.UpdateHistoryUI();
		this.LogProductDefinitions();
	}

	// Token: 0x06000621 RID: 1569 RVA: 0x00011CAC File Offset: 0x000100AC
	public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
	{
		Debug.Log("Purchase OK: " + e.purchasedProduct.definition.id);
		Debug.Log("Receipt: " + e.purchasedProduct.receipt);
		this.m_LastTransationID = e.purchasedProduct.transactionID;
		this.m_LastReceipt = e.purchasedProduct.receipt;
		this.m_PurchaseInProgress = false;
		if (this.m_IsUnityChannelSelected)
		{
			UnifiedReceipt unifiedReceipt = JsonUtility.FromJson<UnifiedReceipt>(e.purchasedProduct.receipt);
			if (unifiedReceipt != null && !string.IsNullOrEmpty(unifiedReceipt.Payload))
			{
				UnityChannelPurchaseReceipt unityChannelPurchaseReceipt = JsonUtility.FromJson<UnityChannelPurchaseReceipt>(unifiedReceipt.Payload);
				Debug.LogFormat("UnityChannel receipt: storeSpecificId = {0}, transactionId = {1}, orderQueryToken = {2}", new object[]
				{
					unityChannelPurchaseReceipt.storeSpecificId,
					unityChannelPurchaseReceipt.transactionId,
					unityChannelPurchaseReceipt.orderQueryToken
				});
			}
		}
		bool isCloudMoolahStoreSelected = this.m_IsCloudMoolahStoreSelected;
		if (isCloudMoolahStoreSelected)
		{
			if (this.m_IsCloudMoolahStoreSelected)
			{
				this.m_MoolahExtensions.RequestPayOut(e.purchasedProduct.transactionID, delegate(string transactionID, RequestPayOutState state, string message)
				{
					if (state == RequestPayOutState.RequestPayOutSucceed)
					{
						this.m_Controller.ConfirmPendingPurchase(e.purchasedProduct);
					}
					else
					{
						Debug.Log(string.Concat(new object[]
						{
							"RequestPayOut: failed. transactionID: ",
							transactionID,
							", state: ",
							state,
							", message: ",
							message
						}));
					}
				});
			}
			return PurchaseProcessingResult.Pending;
		}
		this.UpdateHistoryUI();
		return PurchaseProcessingResult.Complete;
	}

	// Token: 0x06000622 RID: 1570 RVA: 0x00011DFC File Offset: 0x000101FC
	public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
	{
		Debug.Log("Purchase failed: " + item.definition.id);
		Debug.Log(r);
		if (this.m_IsUnityChannelSelected)
		{
			string lastPurchaseError = this.m_UnityChannelExtensions.GetLastPurchaseError();
			IAPDemo.UnityChannelPurchaseError unityChannelPurchaseError = JsonUtility.FromJson<IAPDemo.UnityChannelPurchaseError>(lastPurchaseError);
			if (unityChannelPurchaseError != null && unityChannelPurchaseError.purchaseInfo != null)
			{
				IAPDemo.UnityChannelPurchaseInfo purchaseInfo = unityChannelPurchaseError.purchaseInfo;
				Debug.LogFormat("UnityChannel purchaseInfo: productCode = {0}, gameOrderId = {1}, orderQueryToken = {2}", new object[]
				{
					purchaseInfo.productCode,
					purchaseInfo.gameOrderId,
					purchaseInfo.orderQueryToken
				});
			}
			if (r == PurchaseFailureReason.Unknown && unityChannelPurchaseError != null && unityChannelPurchaseError.error != null && unityChannelPurchaseError.error.Equals("DuplicateTransaction"))
			{
				Debug.Log("Duplicate transaction detected, unlock this item");
			}
		}
		this.m_PurchaseInProgress = false;
	}

	// Token: 0x06000623 RID: 1571 RVA: 0x00011ED0 File Offset: 0x000102D0
	public void OnInitializeFailed(InitializationFailureReason error)
	{
		Debug.Log("Billing failed to initialize!");
		if (error != InitializationFailureReason.AppNotKnown)
		{
			if (error != InitializationFailureReason.PurchasingUnavailable)
			{
				if (error == InitializationFailureReason.NoProductsAvailable)
				{
					Debug.Log("No products available for purchase!");
				}
			}
			else
			{
				Debug.Log("Billing disabled!");
			}
		}
		else
		{
			Debug.LogError("Is your App correctly uploaded on the relevant publisher console?");
		}
	}

	// Token: 0x06000624 RID: 1572 RVA: 0x00011F30 File Offset: 0x00010330
	public void Awake()
	{
		StandardPurchasingModule standardPurchasingModule = StandardPurchasingModule.Instance();
		standardPurchasingModule.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
		ConfigurationBuilder builder = ConfigurationBuilder.Instance(standardPurchasingModule, new IPurchasingModule[0]);
		builder.Configure<IMicrosoftConfiguration>().useMockBillingSystem = true;
		this.m_IsGooglePlayStoreSelected = (Application.platform == RuntimePlatform.Android && standardPurchasingModule.androidStore == AndroidStore.GooglePlay);
		builder.Configure<IMoolahConfiguration>().appKey = "d93f4564c41d463ed3d3cd207594ee1b";
		builder.Configure<IMoolahConfiguration>().hashKey = "cc";
		builder.Configure<IMoolahConfiguration>().SetMode(CloudMoolahMode.AlwaysSucceed);
		this.m_IsCloudMoolahStoreSelected = (Application.platform == RuntimePlatform.Android && standardPurchasingModule.androidStore == AndroidStore.CloudMoolah);
		this.m_IsUnityChannelSelected = (Application.platform == RuntimePlatform.Android && standardPurchasingModule.androidStore == AndroidStore.XiaomiMiPay);
		builder.Configure<IUnityChannelConfiguration>().fetchReceiptPayloadOnPurchase = this.m_FetchReceiptPayloadOnPurchase;
		ProductCatalog productCatalog = ProductCatalog.LoadDefaultCatalog();
		foreach (ProductCatalogItem productCatalogItem in productCatalog.allProducts)
		{
			if (productCatalogItem.allStoreIDs.Count > 0)
			{
				IDs ds = new IDs();
				foreach (StoreID storeID in productCatalogItem.allStoreIDs)
				{
					ds.Add(storeID.id, new string[]
					{
						storeID.store
					});
				}
				builder.AddProduct(productCatalogItem.id, productCatalogItem.type, ds);
			}
			else
			{
				builder.AddProduct(productCatalogItem.id, productCatalogItem.type);
			}
		}
		builder.AddProduct("100.gold.coins", ProductType.Consumable, new IDs
		{
			{
				"100.gold.coins.mac",
				new string[]
				{
					"MacAppStore"
				}
			},
			{
				"000000596586",
				new string[]
				{
					"TizenStore"
				}
			},
			{
				"com.ff",
				new string[]
				{
					"MoolahAppStore"
				}
			}
		});
		builder.AddProduct("500.gold.coins", ProductType.Consumable, new IDs
		{
			{
				"500.gold.coins.mac",
				new string[]
				{
					"MacAppStore"
				}
			},
			{
				"000000596581",
				new string[]
				{
					"TizenStore"
				}
			},
			{
				"com.ee",
				new string[]
				{
					"MoolahAppStore"
				}
			}
		});
		builder.AddProduct("sword", ProductType.NonConsumable, new IDs
		{
			{
				"sword.mac",
				new string[]
				{
					"MacAppStore"
				}
			},
			{
				"000000596583",
				new string[]
				{
					"TizenStore"
				}
			}
		});
		builder.AddProduct("subscription", ProductType.Subscription, new IDs
		{
			{
				"subscription.mac",
				new string[]
				{
					"MacAppStore"
				}
			}
		});
		builder.Configure<IAmazonConfiguration>().WriteSandboxJSON(builder.products);
		builder.Configure<ISamsungAppsConfiguration>().SetMode(SamsungAppsMode.AlwaysSucceed);
		this.m_IsSamsungAppsStoreSelected = (Application.platform == RuntimePlatform.Android && standardPurchasingModule.androidStore == AndroidStore.SamsungApps);
		builder.Configure<ITizenStoreConfiguration>().SetGroupId("100000085616");
		Action initializeUnityIap = delegate()
		{
			UnityPurchasing.Initialize(this, builder);
		};
		if (!this.m_IsUnityChannelSelected)
		{
			initializeUnityIap();
		}
		else
		{
			AppInfo appInfo = new AppInfo();
			appInfo.appId = "abc123appId";
			appInfo.appKey = "efg456appKey";
			appInfo.clientId = "hij789clientId";
			appInfo.clientKey = "klm012clientKey";
			appInfo.debug = false;
			this.unityChannelLoginHandler = new IAPDemo.UnityChannelLoginHandler();
			this.unityChannelLoginHandler.initializeFailedAction = delegate(string message)
			{
				Debug.LogError("Failed to initialize and login to UnityChannel: " + message);
			};
			this.unityChannelLoginHandler.initializeSucceededAction = delegate()
			{
				initializeUnityIap();
			};
			StoreService.Initialize(appInfo, this.unityChannelLoginHandler);
		}
	}

	// Token: 0x06000625 RID: 1573 RVA: 0x000123A8 File Offset: 0x000107A8
	private void OnTransactionsRestored(bool success)
	{
		Debug.Log("Transactions restored.");
	}

	// Token: 0x06000626 RID: 1574 RVA: 0x000123B4 File Offset: 0x000107B4
	private void OnDeferred(Product item)
	{
		Debug.Log("Purchase deferred: " + item.definition.id);
	}

	// Token: 0x06000627 RID: 1575 RVA: 0x000123D0 File Offset: 0x000107D0
	private void InitUI(IEnumerable<Product> items)
	{
		this.m_InteractableSelectable = this.GetDropdown();
		if (!this.NeedRestoreButton())
		{
			this.GetRestoreButton().gameObject.SetActive(false);
		}
		this.GetRegisterButton().gameObject.SetActive(this.NeedRegisterButton());
		this.GetLoginButton().gameObject.SetActive(this.NeedLoginButton());
		this.GetValidateButton().gameObject.SetActive(this.NeedValidateButton());
		foreach (Product product in items)
		{
			string text = string.Format("{0} - {1}", product.definition.id, product.definition.type);
			this.GetDropdown().options.Add(new Dropdown.OptionData(text));
		}
		this.GetDropdown().RefreshShownValue();
		this.GetDropdown().onValueChanged.AddListener(delegate(int selectedItem)
		{
			Debug.Log("OnClickDropdown item " + selectedItem);
			this.m_SelectedItemIndex = selectedItem;
		});
		this.GetBuyButton().onClick.AddListener(delegate()
		{
			if (this.m_PurchaseInProgress)
			{
				Debug.Log("Please wait, purchasing ...");
				return;
			}
			if (this.NeedLoginButton() && !this.m_IsLoggedIn)
			{
				Debug.LogWarning("Purchase notifications will not be forwarded server-to-server. Login incomplete.");
			}
			this.m_PurchaseInProgress = true;
			this.m_Controller.InitiatePurchase(this.m_Controller.products.all[this.m_SelectedItemIndex], "aDemoDeveloperPayload");
		});
		if (this.GetRestoreButton() != null)
		{
			this.GetRestoreButton().onClick.AddListener(delegate()
			{
				if (this.m_IsCloudMoolahStoreSelected)
				{
					if (!this.m_IsLoggedIn)
					{
						Debug.LogError("CloudMoolah purchase restoration aborted. Login incomplete.");
					}
					else
					{
						this.m_MoolahExtensions.RestoreTransactionID(delegate(RestoreTransactionIDState restoreTransactionIDState)
						{
							Debug.Log("restoreTransactionIDState = " + restoreTransactionIDState.ToString());
							bool success = restoreTransactionIDState != RestoreTransactionIDState.RestoreFailed && restoreTransactionIDState != RestoreTransactionIDState.NotKnown;
							this.OnTransactionsRestored(success);
						});
					}
				}
				else if (this.m_IsSamsungAppsStoreSelected)
				{
					this.m_SamsungExtensions.RestoreTransactions(new Action<bool>(this.OnTransactionsRestored));
				}
				else if (Application.platform == RuntimePlatform.MetroPlayerX86 || Application.platform == RuntimePlatform.MetroPlayerX64 || Application.platform == RuntimePlatform.MetroPlayerARM)
				{
					this.m_MicrosoftExtensions.RestoreTransactions();
				}
				else
				{
					this.m_AppleExtensions.RestoreTransactions(new Action<bool>(this.OnTransactionsRestored));
				}
			});
		}
		if (this.GetRegisterButton() != null)
		{
			this.GetRegisterButton().onClick.AddListener(delegate()
			{
				this.m_MoolahExtensions.FastRegister("CMPassword", delegate(string cmUserName)
				{
					Debug.Log("RegisterSucceeded: cmUserName = " + cmUserName);
					this.m_CloudMoolahUserName = cmUserName;
				}, delegate(FastRegisterError error, string errorMessage)
				{
					Debug.Log("RegisterFailed: error = " + error.ToString() + ", errorMessage = " + errorMessage);
				});
			});
		}
		if (this.GetLoginButton() != null)
		{
			if (this.m_IsCloudMoolahStoreSelected)
			{
				this.GetLoginButton().onClick.AddListener(delegate()
				{
					this.m_MoolahExtensions.Login(this.m_CloudMoolahUserName, "CMPassword", delegate(LoginResultState state, string errorMsg)
					{
						if (state == LoginResultState.LoginSucceed)
						{
							this.m_IsLoggedIn = true;
						}
						else
						{
							this.m_IsLoggedIn = false;
						}
						Debug.Log("LoginResult: state: " + state.ToString() + " errorMsg: " + errorMsg);
					});
				});
			}
			else if (this.m_IsUnityChannelSelected)
			{
				this.GetLoginButton().onClick.AddListener(delegate()
				{
					this.unityChannelLoginHandler.loginSucceededAction = delegate(UserInfo userInfo)
					{
						this.m_IsLoggedIn = true;
						Debug.LogFormat("Succeeded logging into UnityChannel. channel {0}, userId {1}, userLoginToken {2} ", new object[]
						{
							userInfo.channel,
							userInfo.userId,
							userInfo.userLoginToken
						});
					};
					this.unityChannelLoginHandler.loginFailedAction = delegate(string message)
					{
						this.m_IsLoggedIn = false;
						Debug.LogError("Failed logging into UnityChannel. " + message);
					};
					StoreService.Login(this.unityChannelLoginHandler);
				});
			}
		}
		if (this.GetValidateButton() != null)
		{
			if (this.m_IsCloudMoolahStoreSelected)
			{
				this.GetValidateButton().onClick.AddListener(delegate()
				{
					this.m_MoolahExtensions.ValidateReceipt(this.m_LastTransationID, this.m_LastReceipt, delegate(string transactionID, ValidateReceiptState state, string message)
					{
						Debug.Log(string.Concat(new string[]
						{
							"ValidtateReceipt transactionID:",
							transactionID,
							", state:",
							state.ToString(),
							", message:",
							message
						}));
					});
				});
			}
			else if (this.m_IsUnityChannelSelected)
			{
				this.GetValidateButton().onClick.AddListener(delegate()
				{
					string txId = this.m_LastTransationID;
					this.m_UnityChannelExtensions.ValidateReceipt(txId, delegate(bool success, string signData, string signature)
					{
						Debug.LogFormat("ValidateReceipt transactionId {0}, success {1}, signData {2}, signature {3}", new object[]
						{
							txId,
							success,
							signData,
							signature
						});
					});
				});
			}
		}
	}

	// Token: 0x06000628 RID: 1576 RVA: 0x00012628 File Offset: 0x00010A28
	public void UpdateHistoryUI()
	{
		if (this.m_Controller == null)
		{
			return;
		}
		string text = "Item\n\n";
		string text2 = "Purchased\n\n";
		foreach (Product product in this.m_Controller.products.all)
		{
			text = text + "\n\n" + product.definition.id;
			text2 += "\n\n";
			text2 += product.hasReceipt.ToString();
		}
		this.GetText(false).text = text;
		this.GetText(true).text = text2;
	}

	// Token: 0x06000629 RID: 1577 RVA: 0x000126D4 File Offset: 0x00010AD4
	protected void UpdateInteractable()
	{
		if (this.m_InteractableSelectable == null)
		{
			return;
		}
		bool flag = this.m_Controller != null;
		if (flag != this.m_InteractableSelectable.interactable)
		{
			if (this.GetRestoreButton() != null)
			{
				this.GetRestoreButton().interactable = flag;
			}
			this.GetBuyButton().interactable = flag;
			this.GetDropdown().interactable = flag;
			this.GetRegisterButton().interactable = flag;
			this.GetLoginButton().interactable = flag;
		}
	}

	// Token: 0x0600062A RID: 1578 RVA: 0x0001275E File Offset: 0x00010B5E
	public void Update()
	{
		this.UpdateInteractable();
	}

	// Token: 0x0600062B RID: 1579 RVA: 0x00012768 File Offset: 0x00010B68
	private bool NeedRestoreButton()
	{
		return Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.tvOS || Application.platform == RuntimePlatform.MetroPlayerX86 || Application.platform == RuntimePlatform.MetroPlayerX64 || Application.platform == RuntimePlatform.MetroPlayerARM || this.m_IsSamsungAppsStoreSelected || this.m_IsCloudMoolahStoreSelected;
	}

	// Token: 0x0600062C RID: 1580 RVA: 0x000127CF File Offset: 0x00010BCF
	private bool NeedRegisterButton()
	{
		return this.m_IsCloudMoolahStoreSelected;
	}

	// Token: 0x0600062D RID: 1581 RVA: 0x000127D7 File Offset: 0x00010BD7
	private bool NeedLoginButton()
	{
		return this.m_IsCloudMoolahStoreSelected || this.m_IsUnityChannelSelected;
	}

	// Token: 0x0600062E RID: 1582 RVA: 0x000127ED File Offset: 0x00010BED
	private bool NeedValidateButton()
	{
		return this.m_IsCloudMoolahStoreSelected || this.m_IsUnityChannelSelected;
	}

	// Token: 0x0600062F RID: 1583 RVA: 0x00012804 File Offset: 0x00010C04
	private Text GetText(bool right)
	{
		string name = (!right) ? "TextL" : "TextR";
		return GameObject.Find(name).GetComponent<Text>();
	}

	// Token: 0x06000630 RID: 1584 RVA: 0x00012832 File Offset: 0x00010C32
	private Dropdown GetDropdown()
	{
		return GameObject.Find("Dropdown").GetComponent<Dropdown>();
	}

	// Token: 0x06000631 RID: 1585 RVA: 0x00012843 File Offset: 0x00010C43
	private Button GetBuyButton()
	{
		return GameObject.Find("Buy").GetComponent<Button>();
	}

	// Token: 0x06000632 RID: 1586 RVA: 0x00012854 File Offset: 0x00010C54
	private Button GetRestoreButton()
	{
		return this.GetButton("Restore");
	}

	// Token: 0x06000633 RID: 1587 RVA: 0x00012861 File Offset: 0x00010C61
	private Button GetRegisterButton()
	{
		return this.GetButton("Register");
	}

	// Token: 0x06000634 RID: 1588 RVA: 0x0001286E File Offset: 0x00010C6E
	private Button GetLoginButton()
	{
		return this.GetButton("Login");
	}

	// Token: 0x06000635 RID: 1589 RVA: 0x0001287B File Offset: 0x00010C7B
	private Button GetValidateButton()
	{
		return this.GetButton("Validate");
	}

	// Token: 0x06000636 RID: 1590 RVA: 0x00012888 File Offset: 0x00010C88
	private Button GetButton(string buttonName)
	{
		GameObject gameObject = GameObject.Find(buttonName);
		if (gameObject != null)
		{
			return gameObject.GetComponent<Button>();
		}
		return null;
	}

	// Token: 0x06000637 RID: 1591 RVA: 0x000128B0 File Offset: 0x00010CB0
	private void LogProductDefinitions()
	{
		Product[] all = this.m_Controller.products.all;
		foreach (Product product in all)
		{
			Debug.Log(string.Format("id: {0}\nstore-specific id: {1}\ntype: {2}\n", product.definition.id, product.definition.storeSpecificId, product.definition.type.ToString()));
		}
	}

	// Token: 0x040003D4 RID: 980
	private IStoreController m_Controller;

	// Token: 0x040003D5 RID: 981
	private IAppleExtensions m_AppleExtensions;

	// Token: 0x040003D6 RID: 982
	private IMoolahExtension m_MoolahExtensions;

	// Token: 0x040003D7 RID: 983
	private ISamsungAppsExtensions m_SamsungExtensions;

	// Token: 0x040003D8 RID: 984
	private IMicrosoftExtensions m_MicrosoftExtensions;

	// Token: 0x040003D9 RID: 985
	private IUnityChannelExtensions m_UnityChannelExtensions;

	// Token: 0x040003DA RID: 986
	private bool m_IsGooglePlayStoreSelected;

	// Token: 0x040003DB RID: 987
	private bool m_IsSamsungAppsStoreSelected;

	// Token: 0x040003DC RID: 988
	private bool m_IsCloudMoolahStoreSelected;

	// Token: 0x040003DD RID: 989
	private bool m_IsUnityChannelSelected;

	// Token: 0x040003DE RID: 990
	private string m_LastTransationID;

	// Token: 0x040003DF RID: 991
	private string m_LastReceipt;

	// Token: 0x040003E0 RID: 992
	private string m_CloudMoolahUserName;

	// Token: 0x040003E1 RID: 993
	private bool m_IsLoggedIn;

	// Token: 0x040003E2 RID: 994
	private IAPDemo.UnityChannelLoginHandler unityChannelLoginHandler;

	// Token: 0x040003E3 RID: 995
	private bool m_FetchReceiptPayloadOnPurchase;

	// Token: 0x040003E4 RID: 996
	private int m_SelectedItemIndex = -1;

	// Token: 0x040003E5 RID: 997
	private bool m_PurchaseInProgress;

	// Token: 0x040003E6 RID: 998
	private Selectable m_InteractableSelectable;

	// Token: 0x020000D7 RID: 215
	[Serializable]
	public class UnityChannelPurchaseError
	{
		// Token: 0x040003EA RID: 1002
		public string error;

		// Token: 0x040003EB RID: 1003
		public IAPDemo.UnityChannelPurchaseInfo purchaseInfo;
	}

	// Token: 0x020000D8 RID: 216
	[Serializable]
	public class UnityChannelPurchaseInfo
	{
		// Token: 0x040003EC RID: 1004
		public string productCode;

		// Token: 0x040003ED RID: 1005
		public string gameOrderId;

		// Token: 0x040003EE RID: 1006
		public string orderQueryToken;
	}

	// Token: 0x020000D9 RID: 217
	private class UnityChannelLoginHandler : ILoginListener
	{
		// Token: 0x0600064B RID: 1611 RVA: 0x00012D09 File Offset: 0x00011109
		public void OnInitialized()
		{
			this.initializeSucceededAction();
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x00012D16 File Offset: 0x00011116
		public void OnInitializeFailed(string message)
		{
			this.initializeFailedAction(message);
		}

		// Token: 0x0600064D RID: 1613 RVA: 0x00012D24 File Offset: 0x00011124
		public void OnLogin(UserInfo userInfo)
		{
			this.loginSucceededAction(userInfo);
		}

		// Token: 0x0600064E RID: 1614 RVA: 0x00012D32 File Offset: 0x00011132
		public void OnLoginFailed(string message)
		{
			this.loginFailedAction(message);
		}

		// Token: 0x040003EF RID: 1007
		internal Action initializeSucceededAction;

		// Token: 0x040003F0 RID: 1008
		internal Action<string> initializeFailedAction;

		// Token: 0x040003F1 RID: 1009
		internal Action<UserInfo> loginSucceededAction;

		// Token: 0x040003F2 RID: 1010
		internal Action<string> loginFailedAction;
	}
}
