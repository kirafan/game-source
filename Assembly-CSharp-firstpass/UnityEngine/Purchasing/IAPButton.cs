﻿using System;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.Purchasing.Extension;
using UnityEngine.UI;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000D1 RID: 209
	[RequireComponent(typeof(Button))]
	[AddComponentMenu("Unity IAP/IAP Button")]
	[HelpURL("https://docs.unity3d.com/Manual/UnityIAP.html")]
	public class IAPButton : MonoBehaviour
	{
		// Token: 0x06000605 RID: 1541 RVA: 0x0001132C File Offset: 0x0000F72C
		private void Start()
		{
			Button component = base.GetComponent<Button>();
			if (this.buttonType == IAPButton.ButtonType.Purchase)
			{
				if (component)
				{
					component.onClick.AddListener(new UnityAction(this.PurchaseProduct));
				}
				if (string.IsNullOrEmpty(this.productId))
				{
					Debug.LogError("IAPButton productId is empty");
				}
				if (!IAPButton.IAPButtonStoreManager.Instance.HasProductInCatalog(this.productId))
				{
					Debug.LogWarning("The product catalog has no product with the ID \"" + this.productId + "\"");
				}
			}
			else if (this.buttonType == IAPButton.ButtonType.Restore && component)
			{
				component.onClick.AddListener(new UnityAction(this.Restore));
			}
		}

		// Token: 0x06000606 RID: 1542 RVA: 0x000113E9 File Offset: 0x0000F7E9
		private void OnEnable()
		{
			if (this.buttonType == IAPButton.ButtonType.Purchase)
			{
				IAPButton.IAPButtonStoreManager.Instance.AddButton(this);
				this.UpdateText();
			}
		}

		// Token: 0x06000607 RID: 1543 RVA: 0x00011407 File Offset: 0x0000F807
		private void OnDisable()
		{
			if (this.buttonType == IAPButton.ButtonType.Purchase)
			{
				IAPButton.IAPButtonStoreManager.Instance.RemoveButton(this);
			}
		}

		// Token: 0x06000608 RID: 1544 RVA: 0x0001141F File Offset: 0x0000F81F
		private void PurchaseProduct()
		{
			if (this.buttonType == IAPButton.ButtonType.Purchase)
			{
				Debug.Log("IAPButton.PurchaseProduct() with product ID: " + this.productId);
				IAPButton.IAPButtonStoreManager.Instance.InitiatePurchase(this.productId);
			}
		}

		// Token: 0x06000609 RID: 1545 RVA: 0x00011454 File Offset: 0x0000F854
		private void Restore()
		{
			if (this.buttonType == IAPButton.ButtonType.Restore)
			{
				if (Application.platform == RuntimePlatform.MetroPlayerX86 || Application.platform == RuntimePlatform.MetroPlayerX64 || Application.platform == RuntimePlatform.MetroPlayerARM)
				{
					IAPButton.IAPButtonStoreManager.Instance.ExtensionProvider.GetExtension<IMicrosoftExtensions>().RestoreTransactions();
				}
				else if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.tvOS)
				{
					IAPButton.IAPButtonStoreManager.Instance.ExtensionProvider.GetExtension<IAppleExtensions>().RestoreTransactions(new Action<bool>(this.OnTransactionsRestored));
				}
				else if (Application.platform == RuntimePlatform.Android && StandardPurchasingModule.Instance().androidStore == AndroidStore.SamsungApps)
				{
					IAPButton.IAPButtonStoreManager.Instance.ExtensionProvider.GetExtension<ISamsungAppsExtensions>().RestoreTransactions(new Action<bool>(this.OnTransactionsRestored));
				}
				else if (Application.platform == RuntimePlatform.Android && StandardPurchasingModule.Instance().androidStore == AndroidStore.CloudMoolah)
				{
					IAPButton.IAPButtonStoreManager.Instance.ExtensionProvider.GetExtension<IMoolahExtension>().RestoreTransactionID(delegate(RestoreTransactionIDState restoreTransactionIDState)
					{
						this.OnTransactionsRestored(restoreTransactionIDState != RestoreTransactionIDState.RestoreFailed && restoreTransactionIDState != RestoreTransactionIDState.NotKnown);
					});
				}
				else
				{
					Debug.LogWarning(Application.platform.ToString() + " is not a supported platform for the Codeless IAP restore button");
				}
			}
		}

		// Token: 0x0600060A RID: 1546 RVA: 0x00011595 File Offset: 0x0000F995
		private void OnTransactionsRestored(bool success)
		{
			Debug.Log("Transactions restored: " + success);
		}

		// Token: 0x0600060B RID: 1547 RVA: 0x000115AC File Offset: 0x0000F9AC
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
		{
			Debug.Log(string.Format("IAPButton.ProcessPurchase(PurchaseEventArgs {0} - {1})", e, e.purchasedProduct.definition.id));
			this.onPurchaseComplete.Invoke(e.purchasedProduct);
			return (!this.consumePurchase) ? PurchaseProcessingResult.Pending : PurchaseProcessingResult.Complete;
		}

		// Token: 0x0600060C RID: 1548 RVA: 0x000115FC File Offset: 0x0000F9FC
		public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
		{
			Debug.Log(string.Format("IAPButton.OnPurchaseFailed(Product {0}, PurchaseFailureReason {1})", product, reason));
			this.onPurchaseFailed.Invoke(product, reason);
		}

		// Token: 0x0600060D RID: 1549 RVA: 0x00011624 File Offset: 0x0000FA24
		private void UpdateText()
		{
			Product product = IAPButton.IAPButtonStoreManager.Instance.GetProduct(this.productId);
			if (product != null)
			{
				if (this.titleText != null)
				{
					this.titleText.text = product.metadata.localizedTitle;
				}
				if (this.descriptionText != null)
				{
					this.descriptionText.text = product.metadata.localizedDescription;
				}
				if (this.priceText != null)
				{
					this.priceText.text = product.metadata.localizedPriceString;
				}
			}
		}

		// Token: 0x040003C4 RID: 964
		[HideInInspector]
		public string productId;

		// Token: 0x040003C5 RID: 965
		[Tooltip("The type of this button, can be either a purchase or a restore button")]
		public IAPButton.ButtonType buttonType;

		// Token: 0x040003C6 RID: 966
		[Tooltip("Consume the product immediately after a successful purchase")]
		public bool consumePurchase = true;

		// Token: 0x040003C7 RID: 967
		[Tooltip("Event fired after a successful purchase of this product")]
		public IAPButton.OnPurchaseCompletedEvent onPurchaseComplete;

		// Token: 0x040003C8 RID: 968
		[Tooltip("Event fired after a failed purchase of this product")]
		public IAPButton.OnPurchaseFailedEvent onPurchaseFailed;

		// Token: 0x040003C9 RID: 969
		[Tooltip("[Optional] Displays the localized title from the app store")]
		public Text titleText;

		// Token: 0x040003CA RID: 970
		[Tooltip("[Optional] Displays the localized description from the app store")]
		public Text descriptionText;

		// Token: 0x040003CB RID: 971
		[Tooltip("[Optional] Displays the localized price from the app store")]
		public Text priceText;

		// Token: 0x020000D2 RID: 210
		public enum ButtonType
		{
			// Token: 0x040003CD RID: 973
			Purchase,
			// Token: 0x040003CE RID: 974
			Restore
		}

		// Token: 0x020000D3 RID: 211
		[Serializable]
		public class OnPurchaseCompletedEvent : UnityEvent<Product>
		{
		}

		// Token: 0x020000D4 RID: 212
		[Serializable]
		public class OnPurchaseFailedEvent : UnityEvent<Product, PurchaseFailureReason>
		{
		}

		// Token: 0x020000D5 RID: 213
		public class IAPButtonStoreManager : IStoreListener
		{
			// Token: 0x06000611 RID: 1553 RVA: 0x000116E8 File Offset: 0x0000FAE8
			private IAPButtonStoreManager()
			{
				this.catalog = ProductCatalog.LoadDefaultCatalog();
				StandardPurchasingModule standardPurchasingModule = StandardPurchasingModule.Instance();
				standardPurchasingModule.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;
				ConfigurationBuilder configurationBuilder = ConfigurationBuilder.Instance(standardPurchasingModule, new IPurchasingModule[0]);
				foreach (ProductCatalogItem productCatalogItem in this.catalog.allProducts)
				{
					if (productCatalogItem.allStoreIDs.Count > 0)
					{
						IDs ds = new IDs();
						foreach (StoreID storeID in productCatalogItem.allStoreIDs)
						{
							ds.Add(storeID.id, new string[]
							{
								storeID.store
							});
						}
						configurationBuilder.AddProduct(productCatalogItem.id, productCatalogItem.type, ds);
					}
					else
					{
						configurationBuilder.AddProduct(productCatalogItem.id, productCatalogItem.type);
					}
				}
				UnityPurchasing.Initialize(this, configurationBuilder);
			}

			// Token: 0x17000068 RID: 104
			// (get) Token: 0x06000612 RID: 1554 RVA: 0x00011828 File Offset: 0x0000FC28
			public static IAPButton.IAPButtonStoreManager Instance
			{
				get
				{
					return IAPButton.IAPButtonStoreManager.instance;
				}
			}

			// Token: 0x17000069 RID: 105
			// (get) Token: 0x06000613 RID: 1555 RVA: 0x0001182F File Offset: 0x0000FC2F
			public IStoreController StoreController
			{
				get
				{
					return this.controller;
				}
			}

			// Token: 0x1700006A RID: 106
			// (get) Token: 0x06000614 RID: 1556 RVA: 0x00011837 File Offset: 0x0000FC37
			public IExtensionProvider ExtensionProvider
			{
				get
				{
					return this.extensions;
				}
			}

			// Token: 0x06000615 RID: 1557 RVA: 0x00011840 File Offset: 0x0000FC40
			public bool HasProductInCatalog(string productID)
			{
				foreach (ProductCatalogItem productCatalogItem in this.catalog.allProducts)
				{
					if (productCatalogItem.id == productID)
					{
						return true;
					}
				}
				return false;
			}

			// Token: 0x06000616 RID: 1558 RVA: 0x000118B4 File Offset: 0x0000FCB4
			public Product GetProduct(string productID)
			{
				if (this.controller != null)
				{
					return this.controller.products.WithID(productID);
				}
				return null;
			}

			// Token: 0x06000617 RID: 1559 RVA: 0x000118D4 File Offset: 0x0000FCD4
			public void AddButton(IAPButton button)
			{
				this.activeButtons.Add(button);
			}

			// Token: 0x06000618 RID: 1560 RVA: 0x000118E2 File Offset: 0x0000FCE2
			public void RemoveButton(IAPButton button)
			{
				this.activeButtons.Remove(button);
			}

			// Token: 0x06000619 RID: 1561 RVA: 0x000118F1 File Offset: 0x0000FCF1
			public void InitiatePurchase(string productID)
			{
				if (this.controller == null)
				{
					Debug.LogError("Purchase failed because Purchasing was not initialized correctly");
					return;
				}
				this.controller.InitiatePurchase(productID);
			}

			// Token: 0x0600061A RID: 1562 RVA: 0x00011918 File Offset: 0x0000FD18
			public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
			{
				this.controller = controller;
				this.extensions = extensions;
				foreach (IAPButton iapbutton in this.activeButtons)
				{
					iapbutton.UpdateText();
				}
			}

			// Token: 0x0600061B RID: 1563 RVA: 0x00011984 File Offset: 0x0000FD84
			public void OnInitializeFailed(InitializationFailureReason error)
			{
				Debug.LogError(string.Format("Purchasing failed to initialize. Reason: {0}", error.ToString()));
			}

			// Token: 0x0600061C RID: 1564 RVA: 0x000119A4 File Offset: 0x0000FDA4
			public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
			{
				foreach (IAPButton iapbutton in this.activeButtons)
				{
					if (iapbutton.productId == e.purchasedProduct.definition.id)
					{
						return iapbutton.ProcessPurchase(e);
					}
				}
				return PurchaseProcessingResult.Complete;
			}

			// Token: 0x0600061D RID: 1565 RVA: 0x00011A2C File Offset: 0x0000FE2C
			public void OnPurchaseFailed(Product product, PurchaseFailureReason reason)
			{
				foreach (IAPButton iapbutton in this.activeButtons)
				{
					if (iapbutton.productId == product.definition.id)
					{
						iapbutton.OnPurchaseFailed(product, reason);
					}
				}
			}

			// Token: 0x040003CF RID: 975
			private static IAPButton.IAPButtonStoreManager instance = new IAPButton.IAPButtonStoreManager();

			// Token: 0x040003D0 RID: 976
			private ProductCatalog catalog;

			// Token: 0x040003D1 RID: 977
			private List<IAPButton> activeButtons = new List<IAPButton>();

			// Token: 0x040003D2 RID: 978
			protected IStoreController controller;

			// Token: 0x040003D3 RID: 979
			protected IExtensionProvider extensions;
		}
	}
}
