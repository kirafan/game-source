﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x020000D0 RID: 208
	[AddComponentMenu("")]
	public class DemoInventory : MonoBehaviour
	{
		// Token: 0x06000603 RID: 1539 RVA: 0x000112D0 File Offset: 0x0000F6D0
		public void Fulfill(string productId)
		{
			if (productId != null)
			{
				if (productId == "100.gold.coins")
				{
					Debug.Log("You Got Money!");
					return;
				}
			}
			Debug.Log(string.Format("Unrecognized productId \"{0}\"", productId));
		}
	}
}
