﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x020000CF RID: 207
	public class GooglePlayTangle
	{
		// Token: 0x06000600 RID: 1536 RVA: 0x0001126F File Offset: 0x0000F66F
		public static byte[] Data()
		{
			if (!GooglePlayTangle.IsPopulated)
			{
				return null;
			}
			return Obfuscator.DeObfuscate(GooglePlayTangle.data, GooglePlayTangle.order, GooglePlayTangle.key);
		}

		// Token: 0x040003C0 RID: 960
		private static byte[] data = Convert.FromBase64String("SuZ91NSKZp3u1QzZBiDPrzfaOhVC2TVtVFXj3oL4JUKIEeVI8XOq5UpFn/9h32kg3lep+u68Q0o0CxzsafLuB9DieVRHftu13PKs+bJ0pS9iBNVCv75Uq36MP6FGtP5apDwAvhZbr9jlIV5aahyw8uWp/06YKpysmRoUGyuZGhEZmRoaG87PdFMVBVGF+VFulnDUreoTZ6vH0RWFVPhz+oKf4zUReN+Q+AQ8rrh69QvU4S4pjL/EH3QBDF2pEMK9p6EygD6RAjlveGjVJ1i4Uw81tcVAIv+GO2BNzyuZGjkrFh0SMZ1TnewWGhoaHhsYtnIPYXK3XU17CP7kOUkShukYDrflRttR3k/tGnbn3tBz8sJPuZZ2S1tCDBj+fnH/7BkYGhsa");

		// Token: 0x040003C1 RID: 961
		private static int[] order = new int[]
		{
			13,
			6,
			5,
			10,
			11,
			10,
			6,
			9,
			8,
			12,
			11,
			13,
			12,
			13,
			14
		};

		// Token: 0x040003C2 RID: 962
		private static int key = 27;

		// Token: 0x040003C3 RID: 963
		public static readonly bool IsPopulated = true;
	}
}
