﻿using System;

namespace UnityEngine.Purchasing.Security
{
	// Token: 0x020000CE RID: 206
	public class AppleTangle
	{
		// Token: 0x060005FD RID: 1533 RVA: 0x00011210 File Offset: 0x0000F610
		public static byte[] Data()
		{
			if (!AppleTangle.IsPopulated)
			{
				return null;
			}
			return Obfuscator.DeObfuscate(AppleTangle.data, AppleTangle.order, AppleTangle.key);
		}

		// Token: 0x040003BC RID: 956
		private static byte[] data = Convert.FromBase64String("PiIrbg0rPDonKCctLzonISBuDzt9eBR+LH9FfkdITRtKSF1MGx1/XWF+z41IRmVIT0tLSUxMfs/4VM/9YA7ouQkDMUYQflFITRtTbUpWflhzaCluxH0kuUPMgZCl7WG3HSQVKinBRvpuuYXiYm4hPvhxT37C+Q2Bfl9ITRtKRF1EDz4+IituByAtYH//fhaiFEp8wib9wVOQKz2xKRAr8n7MSvV+zE3t7k1MT0xMT0x+Q0hH8Lo91aCcKkGFNwF6luxwtzaxJYY6JygnLS86K24sN24vIDduPi88OkhNG1NASlhKWmWeJwnaOEewuiXDCzBRAiUe2A/HijosRV7ND8l9xM85OWAvPj4iK2AtISNhLz4+IistL1HflVAJHqVLoxA3ymOleOwZAhuiPC8tOictK249Oi86KyMrIDo9YH4HljjRfVor7znah2NMTU9OT+3MT+aSMGx7hGubl0GYJZrsam1fue/iLCIrbj06LyAqLzwqbjorPCM9bi/OWmWeJwnaOEewuiXDYA7ouQkDMYdXPLsTQJsxEdW8a030G8EDE0O/RhB+zE9fSE0bU25KzE9GfsxPSn4gKm4tISAqJzonISA9biEobjs9K0miM3fNxR1unXaK//HUAUQlsWWyaH5qSE0bSkVdUw8+PiIrbg0rPDpuLyAqbi0rPDonKCctLzonISBuPkpIXUwbHX9dfl9ITRtKRF1EDz4+aqyln/k+kUELr2mEvyM2o6n7WVnl7T/cCR0bj+FhD/22ta0+g6jtAsVXx5C3BSK7SeVsfkymVnC2Hkedl3gxj8kbl+nX93wMtZabP9Aw7xxkyAbIuUNPT0tLTn4sf0V+R0hNG44tfTm5dEliGKWUQW9AlPQ9VwH7+3TjukFATtxF/29YYDqbckOVLFhuIShuOiYrbjomKyBuLz4+IictLxfpS0cyWQ4YX1A6nfnFbXUJ7ZshPiIrbhwhITpuDQ9+UFlDfnh+enxGZUhPS0tJTE9YUCY6Oj49dGFhOTR+zE84fkBITRtTQU9PsUpKTUxP29A0QuoJxRWaWHl9hYpBA4BaJ58nKCctLzonISBuDzs6JiE8Jzo3f0tOTcxPQU5+zE9ETMxPT06q3+dHMQ/m1refhCjSaiVfnu31qlVkjVFRy83LVddzCXm859UOwGKa/95clm4ND37MT2x+Q0hHZMgGyLlDT09PSH5BSE0bU11PT7FKS35NT0+xflN7fH96fn14FFlDfXt+fH53fH96fjomITwnOjd/WH5aSE0bSk1dQw8+wT3PLohVFUdh3Py2Cga+LnbQW7siK24HIC1gf2h+akhNG0pFXVMPPsxPTkhHZMgGyLktKktPfs+8fmRIKnttWwVbF1P92rm40tCBHvSPFh5ibi0rPDonKCctLzorbj4hIictN1h+WkhNG0pNXUMPPj4iK24cISE6QdNzvWUHZlSGsID790CXEFKYhXN41wJjNvmjwtWSvTnVvDicOX4BjxwrIicvIC0rbiEgbjomJz1uLSs8+VXz3QxqXGSJQVP4A9IQLYYFzlk3bi89PTsjKz1uLy0tKz46LyAtK0NIR2TIBsi5Q09PS0tOTcxPT04SHuTEm5Sqsp5HSXn+Oztv");

		// Token: 0x040003BD RID: 957
		private static int[] order = new int[]
		{
			4,
			8,
			37,
			31,
			56,
			21,
			9,
			26,
			56,
			50,
			39,
			30,
			25,
			36,
			15,
			46,
			27,
			54,
			42,
			32,
			32,
			33,
			43,
			51,
			39,
			45,
			37,
			53,
			39,
			53,
			58,
			56,
			57,
			51,
			41,
			52,
			50,
			52,
			39,
			42,
			58,
			46,
			54,
			49,
			50,
			53,
			52,
			51,
			55,
			56,
			52,
			57,
			52,
			53,
			56,
			55,
			56,
			59,
			58,
			59,
			60
		};

		// Token: 0x040003BE RID: 958
		private static int key = 78;

		// Token: 0x040003BF RID: 959
		public static readonly bool IsPopulated = true;
	}
}
