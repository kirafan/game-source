﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x02000005 RID: 5
public static class CriAtomPlugin
{
	// Token: 0x0600002A RID: 42 RVA: 0x00002A17 File Offset: 0x00000E17
	public static void Log(string log)
	{
	}

	// Token: 0x17000001 RID: 1
	// (get) Token: 0x0600002B RID: 43 RVA: 0x00002A19 File Offset: 0x00000E19
	public static bool isInitialized
	{
		get
		{
			return CriAtomPlugin.initializationCount > 0;
		}
	}

	// Token: 0x0600002C RID: 44 RVA: 0x00002A23 File Offset: 0x00000E23
	public static bool GetAudioEffectInterfaceList(out List<IntPtr> effect_interface_list)
	{
		if (CriAtomPlugin.isInitialized)
		{
			effect_interface_list = null;
			return false;
		}
		if (CriAtomPlugin.effectInterfaceList == null)
		{
			CriAtomPlugin.effectInterfaceList = new List<IntPtr>();
		}
		effect_interface_list = CriAtomPlugin.effectInterfaceList;
		return true;
	}

	// Token: 0x0600002D RID: 45 RVA: 0x00002A50 File Offset: 0x00000E50
	private static IntPtr GetSpatializerCoreInterfaceFromAtomOculusAudioBridge()
	{
		Type type = Type.GetType("CriAtomOculusAudio");
		if (type == null)
		{
			Debug.LogError("[CRIWARE] ERROR: Cri Atom Oculus Audio Bridge Plugin is not imported.");
		}
		else
		{
			MethodInfo method = type.GetMethod("SupportCurrentPlatform");
			if (method == null)
			{
				Debug.LogError("[CRIWARE] ERROR: CriAtomOculusAudio.SupportCurrentPlatform method is not found.");
				return IntPtr.Zero;
			}
			bool flag = (bool)method.Invoke(null, null);
			if (flag)
			{
				MethodInfo method2 = type.GetMethod("GetSpatializerCoreInterface");
				if (method2 == null)
				{
					Debug.LogError("[CRIWARE] ERROR: CriAtomOculusAudio.GetSpatializerCoreInterface method is not found.");
					return IntPtr.Zero;
				}
				return (IntPtr)method2.Invoke(null, null);
			}
		}
		return IntPtr.Zero;
	}

	// Token: 0x0600002E RID: 46 RVA: 0x00002AE8 File Offset: 0x00000EE8
	public static void SetConfigParameters(int max_virtual_voices, int max_voice_limit_groups, int max_categories, int max_sequence_events_per_frame, int max_beatsync_callbacks_per_frame, int num_standard_memory_voices, int num_standard_streaming_voices, int num_hca_mx_memory_voices, int num_hca_mx_streaming_voices, int output_sampling_rate, int num_asr_output_channels, bool uses_in_game_preview, float server_frequency, int max_parameter_blocks, int categories_per_playback, int num_buses, bool vr_mode)
	{
		IntPtr spatializer_core_interface = IntPtr.Zero;
		if (vr_mode)
		{
			spatializer_core_interface = CriAtomPlugin.GetSpatializerCoreInterfaceFromAtomOculusAudioBridge();
		}
		CriAtomPlugin.criAtomUnity_SetConfigParameters(max_virtual_voices, max_voice_limit_groups, max_categories, max_sequence_events_per_frame, max_beatsync_callbacks_per_frame, num_standard_memory_voices, num_standard_streaming_voices, num_hca_mx_memory_voices, num_hca_mx_streaming_voices, output_sampling_rate, num_asr_output_channels, uses_in_game_preview, server_frequency, max_parameter_blocks, categories_per_playback, num_buses, vr_mode, spatializer_core_interface);
	}

	// Token: 0x0600002F RID: 47 RVA: 0x00002B2C File Offset: 0x00000F2C
	public static void SetConfigAdditionalParameters_PC(long buffering_time_pc)
	{
		CriAtomPlugin.criAtomUnity_SetConfigAdditionalParameters_PC(buffering_time_pc);
	}

	// Token: 0x06000030 RID: 48 RVA: 0x00002B34 File Offset: 0x00000F34
	public static void SetConfigAdditionalParameters_IOS(uint buffering_time_ios, bool override_ipod_music_ios)
	{
		CriAtomPlugin.criAtomUnity_SetConfigAdditionalParameters_IOS(buffering_time_ios, override_ipod_music_ios);
	}

	// Token: 0x06000031 RID: 49 RVA: 0x00002B3D File Offset: 0x00000F3D
	public static void SetConfigAdditionalParameters_ANDROID(int num_low_delay_memory_voices, int num_low_delay_streaming_voices, int sound_buffering_time, int sound_start_buffering_time, IntPtr android_context)
	{
		CriAtomPlugin.criAtomUnity_SetConfigAdditionalParameters_ANDROID(num_low_delay_memory_voices, num_low_delay_streaming_voices, sound_buffering_time, sound_start_buffering_time, android_context);
	}

	// Token: 0x06000032 RID: 50 RVA: 0x00002B4A File Offset: 0x00000F4A
	public static void SetConfigAdditionalParameters_VITA(int num_atrac9_memory_voices, int num_atrac9_streaming_voices, int num_mana_decoders)
	{
	}

	// Token: 0x06000033 RID: 51 RVA: 0x00002B4C File Offset: 0x00000F4C
	public static void SetConfigAdditionalParameters_PS4(int num_atrac9_memory_voices, int num_atrac9_streaming_voices, bool use_audio3d, int num_audio3d_memory_voices, int num_audio3d_streaming_voices)
	{
	}

	// Token: 0x06000034 RID: 52 RVA: 0x00002B4E File Offset: 0x00000F4E
	public static void SetConfigAdditionalParameters_WEBGL(int num_webaudio_voices)
	{
	}

	// Token: 0x06000035 RID: 53 RVA: 0x00002B50 File Offset: 0x00000F50
	public static void SetMaxSamplingRateForStandardVoicePool(int sampling_rate_for_memory, int sampling_rate_for_streaming)
	{
		CriAtomPlugin.criAtomUnity_SetMaxSamplingRateForStandardVoicePool(sampling_rate_for_memory, sampling_rate_for_streaming);
	}

	// Token: 0x06000036 RID: 54 RVA: 0x00002B5C File Offset: 0x00000F5C
	public static int GetRequiredMaxVirtualVoices(CriAtomConfig atomConfig)
	{
		int num = 0;
		num += atomConfig.standardVoicePoolConfig.memoryVoices;
		num += atomConfig.standardVoicePoolConfig.streamingVoices;
		num += atomConfig.hcaMxVoicePoolConfig.memoryVoices;
		num += atomConfig.hcaMxVoicePoolConfig.streamingVoices;
		num += atomConfig.androidLowLatencyStandardVoicePoolConfig.memoryVoices;
		return num + atomConfig.androidLowLatencyStandardVoicePoolConfig.streamingVoices;
	}

	// Token: 0x06000037 RID: 55 RVA: 0x00002BC0 File Offset: 0x00000FC0
	public static void InitializeLibrary()
	{
		CriAtomPlugin.initializationCount++;
		if (CriAtomPlugin.initializationCount != 1)
		{
			return;
		}
		if (!CriWareInitializer.IsInitialized())
		{
			Debug.Log("[CRIWARE] CriWareInitializer is not working. Initializes Atom by default parameters.");
		}
		CriFsPlugin.InitializeLibrary();
		CriAtomPlugin.criAtomUnity_Initialize();
		if (CriAtomPlugin.effectInterfaceList != null)
		{
			for (int i = 0; i < CriAtomPlugin.effectInterfaceList.Count; i++)
			{
				CriAtomExAsr.RegisterEffectInterface(CriAtomPlugin.effectInterfaceList[i]);
			}
		}
		CriAtomServer.CreateInstance();
		CriAtomListener.CreateSharedNativeListener();
	}

	// Token: 0x06000038 RID: 56 RVA: 0x00002C48 File Offset: 0x00001048
	public static void FinalizeLibrary()
	{
		CriAtomPlugin.initializationCount--;
		if (CriAtomPlugin.initializationCount < 0)
		{
			Debug.LogError("[CRIWARE] ERROR: Atom library is already finalized.");
			return;
		}
		if (CriAtomPlugin.initializationCount != 0)
		{
			return;
		}
		CriAtomListener.DestroySharedNativeListener();
		CriAtomServer.DestroyInstance();
		CriDisposableObjectManager.CallOnModuleFinalization(CriDisposableObjectManager.ModuleType.Atom);
		if (CriAtomPlugin.effectInterfaceList != null)
		{
			CriAtomPlugin.effectInterfaceList.Clear();
			CriAtomPlugin.effectInterfaceList = null;
		}
		CriAtomPlugin.criAtomUnity_Finalize();
		CriFsPlugin.FinalizeLibrary();
	}

	// Token: 0x06000039 RID: 57 RVA: 0x00002CB6 File Offset: 0x000010B6
	public static void Pause(bool pause)
	{
		if (CriAtomPlugin.isInitialized)
		{
			CriAtomPlugin.criAtomUnity_Pause(pause);
		}
	}

	// Token: 0x0600003A RID: 58 RVA: 0x00002CC8 File Offset: 0x000010C8
	public static CriWare.CpuUsage GetCpuUsage()
	{
		float realtimeSinceStartup = Time.realtimeSinceStartup;
		if (realtimeSinceStartup - CriAtomPlugin.timeSinceStartup > 1f)
		{
			CriAtomEx.PerformanceInfo performanceInfo;
			CriAtomEx.GetPerformanceInfo(out performanceInfo);
			CriAtomPlugin.cpuUsage.last = performanceInfo.lastServerTime * 100f / performanceInfo.averageServerInterval;
			CriAtomPlugin.cpuUsage.average = performanceInfo.averageServerTime * 100f / performanceInfo.averageServerInterval;
			CriAtomPlugin.cpuUsage.peak = performanceInfo.maxServerTime * 100f / performanceInfo.averageServerInterval;
			CriAtomEx.ResetPerformanceMonitor();
			CriAtomPlugin.timeSinceStartup = realtimeSinceStartup;
		}
		return CriAtomPlugin.cpuUsage;
	}

	// Token: 0x0600003B RID: 59 RVA: 0x00002D6C File Offset: 0x0000116C
	public static ushort GetLoopCountParameterId()
	{
		ushort num = CriAtomPlugin.criAtomUnity_GetNativeParameterId(CriAtomPlugin.CRIATOMUNITY_PARAMETER_ID_LOOP_COUNT);
		if (num == CriAtomPlugin.CRIATOMPARAMETER2_ID_INVALID)
		{
			throw new Exception("GetNativeParameterId failed.");
		}
		return num;
	}

	// Token: 0x0600003C RID: 60
	[DllImport("cri_ware_unity")]
	private static extern void criAtomUnity_SetConfigParameters(int max_virtual_voices, int max_voice_limit_groups, int max_categories, int max_sequence_events_per_frame, int max_beatsync_callbacks_per_frame, int num_standard_memory_voices, int num_standard_streaming_voices, int num_hca_mx_memory_voices, int num_hca_mx_streaming_voices, int output_sampling_rate, int num_asr_output_channels, bool uses_in_game_preview, float server_frequency, int max_parameter_blocks, int categories_per_playback, int num_buses, bool use_ambisonics, IntPtr spatializer_core_interface);

	// Token: 0x0600003D RID: 61
	[DllImport("cri_ware_unity")]
	private static extern void criAtomUnity_SetConfigAdditionalParameters_PC(long buffering_time_pc);

	// Token: 0x0600003E RID: 62
	[DllImport("cri_ware_unity")]
	private static extern void criAtomUnity_SetConfigAdditionalParameters_IOS(uint buffering_time_ios, bool override_ipod_music_ios);

	// Token: 0x0600003F RID: 63
	[DllImport("cri_ware_unity")]
	private static extern void criAtomUnity_SetConfigAdditionalParameters_ANDROID(int num_low_delay_memory_voices, int num_low_delay_streaming_voices, int sound_buffering_time, int sound_start_buffering_time, IntPtr android_context);

	// Token: 0x06000040 RID: 64
	[DllImport("cri_ware_unity")]
	private static extern void criAtomUnity_Initialize();

	// Token: 0x06000041 RID: 65
	[DllImport("cri_ware_unity")]
	private static extern void criAtomUnity_Finalize();

	// Token: 0x06000042 RID: 66
	[DllImport("cri_ware_unity")]
	private static extern void criAtomUnity_Pause(bool pause);

	// Token: 0x06000043 RID: 67
	[DllImport("cri_ware_unity")]
	public static extern uint criAtomUnity_GetAllocatedHeapSize();

	// Token: 0x06000044 RID: 68
	[DllImport("cri_ware_unity")]
	public static extern void criAtomUnity_ControlDataCompatibility(int code);

	// Token: 0x06000045 RID: 69
	[DllImport("cri_ware_unity")]
	public static extern void criAtomUnitySequencer_SetEventCallback(IntPtr cbfunc, string separator_string);

	// Token: 0x06000046 RID: 70
	[DllImport("cri_ware_unity")]
	public static extern void criAtomUnitySequencer_ExecuteQueuedEventCallbacks();

	// Token: 0x06000047 RID: 71
	[DllImport("cri_ware_unity")]
	public static extern void criAtomUnity_SetBeatSyncCallback(IntPtr cbfunc);

	// Token: 0x06000048 RID: 72
	[DllImport("cri_ware_unity")]
	public static extern void criAtomUnity_ExecuteQueuedBeatSyncCallbacks();

	// Token: 0x06000049 RID: 73
	[DllImport("cri_ware_unity")]
	private static extern void criAtomUnity_SetMaxSamplingRateForStandardVoicePool(int sampling_rate_for_memory, int sampling_rate_for_streaming);

	// Token: 0x0600004A RID: 74
	[DllImport("cri_ware_unity")]
	public static extern void criAtomUnity_BeginLeCompatibleMode();

	// Token: 0x0600004B RID: 75
	[DllImport("cri_ware_unity")]
	public static extern void criAtomUnity_EndLeCompatibleMode();

	// Token: 0x0600004C RID: 76
	[DllImport("cri_ware_unity")]
	public static extern ushort criAtomUnity_GetNativeParameterId(int id);

	// Token: 0x04000045 RID: 69
	public const string criAtomUnityEditorVersion = "Ver.0.21.07";

	// Token: 0x04000046 RID: 70
	private static int initializationCount = 0;

	// Token: 0x04000047 RID: 71
	private static List<IntPtr> effectInterfaceList = null;

	// Token: 0x04000048 RID: 72
	private static float timeSinceStartup = Time.realtimeSinceStartup;

	// Token: 0x04000049 RID: 73
	private static CriWare.CpuUsage cpuUsage;

	// Token: 0x0400004A RID: 74
	private static int CRIATOMUNITY_PARAMETER_ID_LOOP_COUNT = 0;

	// Token: 0x0400004B RID: 75
	private static ushort CRIATOMPARAMETER2_ID_INVALID = ushort.MaxValue;
}
