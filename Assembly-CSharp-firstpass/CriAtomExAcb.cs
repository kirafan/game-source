﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000038 RID: 56
public class CriAtomExAcb : IDisposable
{
	// Token: 0x0600017E RID: 382 RVA: 0x00005704 File Offset: 0x00003B04
	internal CriAtomExAcb(IntPtr handle, byte[] data)
	{
		this.handle = handle;
		if (data != null)
		{
			this.data = data;
			this.dataHandle = GCHandle.Alloc(this.data, GCHandleType.Pinned);
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x1700001B RID: 27
	// (get) Token: 0x0600017F RID: 383 RVA: 0x00005744 File Offset: 0x00003B44
	public IntPtr nativeHandle
	{
		get
		{
			return this.handle;
		}
	}

	// Token: 0x1700001C RID: 28
	// (get) Token: 0x06000180 RID: 384 RVA: 0x0000574C File Offset: 0x00003B4C
	public bool isAvailable
	{
		get
		{
			return this.handle != IntPtr.Zero;
		}
	}

	// Token: 0x06000181 RID: 385 RVA: 0x00005760 File Offset: 0x00003B60
	public static CriAtomExAcb LoadAcbFile(CriFsBinder binder, string acbPath, string awbPath)
	{
		IntPtr intPtr = (binder == null) ? IntPtr.Zero : binder.nativeHandle;
		IntPtr value = CriAtomExAcb.criAtomExAcb_LoadAcbFile(intPtr, acbPath, intPtr, awbPath, IntPtr.Zero, 0);
		if (value == IntPtr.Zero)
		{
			return null;
		}
		return new CriAtomExAcb(value, null);
	}

	// Token: 0x06000182 RID: 386 RVA: 0x000057B0 File Offset: 0x00003BB0
	public static CriAtomExAcb LoadAcbData(byte[] acbData, CriFsBinder awbBinder, string awbPath)
	{
		IntPtr awb_binder = (awbBinder == null) ? IntPtr.Zero : awbBinder.nativeHandle;
		IntPtr value = CriAtomExAcb.criAtomExAcb_LoadAcbData(acbData, acbData.Length, awb_binder, awbPath, IntPtr.Zero, 0);
		if (value == IntPtr.Zero)
		{
			return null;
		}
		return new CriAtomExAcb(value, acbData);
	}

	// Token: 0x06000183 RID: 387 RVA: 0x00005800 File Offset: 0x00003C00
	public void Dispose()
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.isAvailable)
		{
			CriAtomExAcb.criAtomExAcb_Release(this.handle);
			this.handle = IntPtr.Zero;
		}
		if (this.data != null)
		{
			this.dataHandle.Free();
			this.data = null;
		}
		GC.SuppressFinalize(this);
	}

	// Token: 0x06000184 RID: 388 RVA: 0x00005858 File Offset: 0x00003C58
	public bool Exists(string cueName)
	{
		return CriAtomExAcb.criAtomExAcb_ExistsName(this.handle, cueName);
	}

	// Token: 0x06000185 RID: 389 RVA: 0x00005866 File Offset: 0x00003C66
	public bool Exists(int cueId)
	{
		return CriAtomExAcb.criAtomExAcb_ExistsId(this.handle, cueId);
	}

	// Token: 0x06000186 RID: 390 RVA: 0x00005874 File Offset: 0x00003C74
	public bool GetCueInfo(string cueName, out CriAtomEx.CueInfo info)
	{
		bool result;
		using (CriStructMemory<CriAtomEx.CueInfo> criStructMemory = new CriStructMemory<CriAtomEx.CueInfo>())
		{
			bool flag = CriAtomExAcb.criAtomExAcb_GetCueInfoByName(this.handle, cueName, criStructMemory.ptr);
			info = new CriAtomEx.CueInfo(criStructMemory.bytes, 0);
			result = flag;
		}
		return result;
	}

	// Token: 0x06000187 RID: 391 RVA: 0x000058CC File Offset: 0x00003CCC
	public bool GetCueInfo(int cueId, out CriAtomEx.CueInfo info)
	{
		bool result;
		using (CriStructMemory<CriAtomEx.CueInfo> criStructMemory = new CriStructMemory<CriAtomEx.CueInfo>())
		{
			bool flag = CriAtomExAcb.criAtomExAcb_GetCueInfoById(this.handle, cueId, criStructMemory.ptr);
			info = new CriAtomEx.CueInfo(criStructMemory.bytes, 0);
			result = flag;
		}
		return result;
	}

	// Token: 0x06000188 RID: 392 RVA: 0x00005924 File Offset: 0x00003D24
	public bool GetCueInfoByIndex(int index, out CriAtomEx.CueInfo info)
	{
		bool result;
		using (CriStructMemory<CriAtomEx.CueInfo> criStructMemory = new CriStructMemory<CriAtomEx.CueInfo>())
		{
			bool flag = CriAtomExAcb.criAtomExAcb_GetCueInfoByIndex(this.handle, index, criStructMemory.ptr);
			info = new CriAtomEx.CueInfo(criStructMemory.bytes, 0);
			result = flag;
		}
		return result;
	}

	// Token: 0x06000189 RID: 393 RVA: 0x0000597C File Offset: 0x00003D7C
	public CriAtomEx.CueInfo[] GetCueInfoList()
	{
		int num = CriAtomExAcb.criAtomExAcb_GetNumCues(this.handle);
		CriAtomEx.CueInfo[] array = new CriAtomEx.CueInfo[num];
		for (int i = 0; i < num; i++)
		{
			this.GetCueInfoByIndex(i, out array[i]);
		}
		return array;
	}

	// Token: 0x0600018A RID: 394 RVA: 0x000059C0 File Offset: 0x00003DC0
	public bool GetWaveFormInfo(string cueName, out CriAtomEx.WaveformInfo info)
	{
		bool result;
		using (CriStructMemory<CriAtomEx.WaveformInfo> criStructMemory = new CriStructMemory<CriAtomEx.WaveformInfo>())
		{
			bool flag = CriAtomExAcb.criAtomExAcb_GetWaveformInfoByName(this.handle, cueName, criStructMemory.ptr);
			info = new CriAtomEx.WaveformInfo(criStructMemory.bytes, 0);
			result = flag;
		}
		return result;
	}

	// Token: 0x0600018B RID: 395 RVA: 0x00005A18 File Offset: 0x00003E18
	public bool GetWaveFormInfo(int cueId, out CriAtomEx.WaveformInfo info)
	{
		bool result;
		using (CriStructMemory<CriAtomEx.WaveformInfo> criStructMemory = new CriStructMemory<CriAtomEx.WaveformInfo>())
		{
			bool flag = CriAtomExAcb.criAtomExAcb_GetWaveformInfoById(this.handle, cueId, criStructMemory.ptr);
			info = new CriAtomEx.WaveformInfo(criStructMemory.bytes, 0);
			result = flag;
		}
		return result;
	}

	// Token: 0x0600018C RID: 396 RVA: 0x00005A70 File Offset: 0x00003E70
	public int GetNumCuePlaying(string name)
	{
		return CriAtomExAcb.criAtomExAcb_GetNumCuePlayingCountByName(this.handle, name);
	}

	// Token: 0x0600018D RID: 397 RVA: 0x00005A7E File Offset: 0x00003E7E
	public int GetNumCuePlaying(int id)
	{
		return CriAtomExAcb.criAtomExAcb_GetNumCuePlayingCountById(this.handle, id);
	}

	// Token: 0x0600018E RID: 398 RVA: 0x00005A8C File Offset: 0x00003E8C
	public int GetBlockIndex(string cueName, string blockName)
	{
		return CriAtomExAcb.criAtomExAcb_GetBlockIndexByName(this.handle, cueName, blockName);
	}

	// Token: 0x0600018F RID: 399 RVA: 0x00005A9B File Offset: 0x00003E9B
	public int GetBlockIndex(int cueId, string blockName)
	{
		return CriAtomExAcb.criAtomExAcb_GetBlockIndexById(this.handle, cueId, blockName);
	}

	// Token: 0x06000190 RID: 400 RVA: 0x00005AAA File Offset: 0x00003EAA
	public int GetNumUsableAisacControls(string cueName)
	{
		return CriAtomExAcb.criAtomExAcb_GetNumUsableAisacControlsByName(this.handle, cueName);
	}

	// Token: 0x06000191 RID: 401 RVA: 0x00005AB8 File Offset: 0x00003EB8
	public int GetNumUsableAisacControls(int cueId)
	{
		return CriAtomExAcb.criAtomExAcb_GetNumUsableAisacControlsById(this.handle, cueId);
	}

	// Token: 0x06000192 RID: 402 RVA: 0x00005AC8 File Offset: 0x00003EC8
	public bool GetUsableAisacControl(string cueName, int index, out CriAtomEx.AisacControlInfo info)
	{
		bool result;
		using (CriStructMemory<CriAtomEx.AisacControlInfo> criStructMemory = new CriStructMemory<CriAtomEx.AisacControlInfo>())
		{
			bool flag = CriAtomExAcb.criAtomExAcb_GetUsableAisacControlByName(this.handle, cueName, (ushort)index, criStructMemory.ptr);
			info = new CriAtomEx.AisacControlInfo(criStructMemory.bytes, 0);
			result = flag;
		}
		return result;
	}

	// Token: 0x06000193 RID: 403 RVA: 0x00005B24 File Offset: 0x00003F24
	public bool GetUsableAisacControl(int cueId, int index, out CriAtomEx.AisacControlInfo info)
	{
		bool result;
		using (CriStructMemory<CriAtomEx.AisacControlInfo> criStructMemory = new CriStructMemory<CriAtomEx.AisacControlInfo>())
		{
			bool flag = CriAtomExAcb.criAtomExAcb_GetUsableAisacControlById(this.handle, cueId, (ushort)index, criStructMemory.ptr);
			info = new CriAtomEx.AisacControlInfo(criStructMemory.bytes, 0);
			result = flag;
		}
		return result;
	}

	// Token: 0x06000194 RID: 404 RVA: 0x00005B80 File Offset: 0x00003F80
	public CriAtomEx.AisacControlInfo[] GetUsableAisacControlList(string cueName)
	{
		int numUsableAisacControls = this.GetNumUsableAisacControls(cueName);
		CriAtomEx.AisacControlInfo[] array = new CriAtomEx.AisacControlInfo[numUsableAisacControls];
		for (int i = 0; i < numUsableAisacControls; i++)
		{
			this.GetUsableAisacControl(cueName, i, out array[i]);
		}
		return array;
	}

	// Token: 0x06000195 RID: 405 RVA: 0x00005BC0 File Offset: 0x00003FC0
	public CriAtomEx.AisacControlInfo[] GetUsableAisacControlList(int cueId)
	{
		int numUsableAisacControls = this.GetNumUsableAisacControls(cueId);
		CriAtomEx.AisacControlInfo[] array = new CriAtomEx.AisacControlInfo[numUsableAisacControls];
		for (int i = 0; i < numUsableAisacControls; i++)
		{
			this.GetUsableAisacControl(cueId, i, out array[i]);
		}
		return array;
	}

	// Token: 0x06000196 RID: 406 RVA: 0x00005BFF File Offset: 0x00003FFF
	public void ResetCueTypeState(string cueName)
	{
		CriAtomExAcb.criAtomExAcb_ResetCueTypeStateByName(this.handle, cueName);
	}

	// Token: 0x06000197 RID: 407 RVA: 0x00005C0D File Offset: 0x0000400D
	public void ResetCueTypeState(int cueId)
	{
		CriAtomExAcb.criAtomExAcb_ResetCueTypeStateById(this.handle, cueId);
	}

	// Token: 0x06000198 RID: 408 RVA: 0x00005C1C File Offset: 0x0000401C
	public void AttachAwbFile(CriFsBinder awb_binder, string awb_path, string awb_name)
	{
		IntPtr awb_binder2 = (awb_binder == null) ? IntPtr.Zero : awb_binder.nativeHandle;
		CriAtomExAcb.criAtomExAcb_AttachAwbFile(this.handle, awb_binder2, awb_path, awb_name, IntPtr.Zero, 0);
	}

	// Token: 0x06000199 RID: 409 RVA: 0x00005C54 File Offset: 0x00004054
	public void DetachAwbFile(string awb_name)
	{
		if (this.isAvailable)
		{
			CriAtomExAcb.criAtomExAcb_DetachAwbFile(this.handle, awb_name);
		}
	}

	// Token: 0x0600019A RID: 410 RVA: 0x00005C6D File Offset: 0x0000406D
	public float GetLoadProgress()
	{
		return 1f;
	}

	// Token: 0x0600019B RID: 411 RVA: 0x00005C74 File Offset: 0x00004074
	~CriAtomExAcb()
	{
		this.Dispose();
	}

	// Token: 0x0600019C RID: 412
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExAcb_LoadAcbFile(IntPtr acb_binder, string acb_path, IntPtr awb_binder, string awb_path, IntPtr work, int work_size);

	// Token: 0x0600019D RID: 413
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExAcb_LoadAcbData(byte[] acb_data, int acb_data_size, IntPtr awb_binder, string awb_path, IntPtr work, int work_size);

	// Token: 0x0600019E RID: 414
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAcb_Release(IntPtr acb_hn);

	// Token: 0x0600019F RID: 415
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcb_GetNumCues(IntPtr acb_hn);

	// Token: 0x060001A0 RID: 416
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcb_ExistsId(IntPtr acb_hn, int id);

	// Token: 0x060001A1 RID: 417
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcb_ExistsName(IntPtr acb_hn, string name);

	// Token: 0x060001A2 RID: 418
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcb_GetNumUsableAisacControlsById(IntPtr acb_hn, int id);

	// Token: 0x060001A3 RID: 419
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcb_GetNumUsableAisacControlsByName(IntPtr acb_hn, string name);

	// Token: 0x060001A4 RID: 420
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcb_GetUsableAisacControlById(IntPtr acb_hn, int id, ushort index, IntPtr info);

	// Token: 0x060001A5 RID: 421
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcb_GetUsableAisacControlByName(IntPtr acb_hn, string name, ushort index, IntPtr info);

	// Token: 0x060001A6 RID: 422
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcb_GetWaveformInfoById(IntPtr acb_hn, int id, IntPtr waveform_info);

	// Token: 0x060001A7 RID: 423
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcb_GetWaveformInfoByName(IntPtr acb_hn, string name, IntPtr waveform_info);

	// Token: 0x060001A8 RID: 424
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcb_GetCueInfoByName(IntPtr acb_hn, string name, IntPtr info);

	// Token: 0x060001A9 RID: 425
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcb_GetCueInfoById(IntPtr acb_hn, int id, IntPtr info);

	// Token: 0x060001AA RID: 426
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcb_GetCueInfoByIndex(IntPtr acb_hn, int index, IntPtr info);

	// Token: 0x060001AB RID: 427
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcb_GetNumCuePlayingCountByName(IntPtr acb_hn, string name);

	// Token: 0x060001AC RID: 428
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcb_GetNumCuePlayingCountById(IntPtr acb_hn, int id);

	// Token: 0x060001AD RID: 429
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcb_GetBlockIndexById(IntPtr acb_hn, int id, string block_name);

	// Token: 0x060001AE RID: 430
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcb_GetBlockIndexByName(IntPtr acb_hn, string name, string block_name);

	// Token: 0x060001AF RID: 431
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAcb_ResetCueTypeStateByName(IntPtr acb_hn, string name);

	// Token: 0x060001B0 RID: 432
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAcb_ResetCueTypeStateById(IntPtr acb_hn, int id);

	// Token: 0x060001B1 RID: 433
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAcb_AttachAwbFile(IntPtr acb_hn, IntPtr awb_binder, string awb_path, string awb_name, IntPtr work, int work_size);

	// Token: 0x060001B2 RID: 434
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAcb_DetachAwbFile(IntPtr acb_hn, string awb_name);

	// Token: 0x04000129 RID: 297
	private IntPtr handle = IntPtr.Zero;

	// Token: 0x0400012A RID: 298
	private byte[] data;

	// Token: 0x0400012B RID: 299
	private GCHandle dataHandle;
}
