﻿using System;

// Token: 0x02000002 RID: 2
public class AFInAppEvents
{
	// Token: 0x04000001 RID: 1
	public const string LEVEL_ACHIEVED = "af_level_achieved";

	// Token: 0x04000002 RID: 2
	public const string ADD_PAYMENT_INFO = "af_add_payment_info";

	// Token: 0x04000003 RID: 3
	public const string ADD_TO_CART = "af_add_to_cart";

	// Token: 0x04000004 RID: 4
	public const string ADD_TO_WISH_LIST = "af_add_to_wishlist";

	// Token: 0x04000005 RID: 5
	public const string COMPLETE_REGISTRATION = "af_complete_registration";

	// Token: 0x04000006 RID: 6
	public const string TUTORIAL_COMPLETION = "af_tutorial_completion";

	// Token: 0x04000007 RID: 7
	public const string INITIATED_CHECKOUT = "af_initiated_checkout";

	// Token: 0x04000008 RID: 8
	public const string PURCHASE = "af_purchase";

	// Token: 0x04000009 RID: 9
	public const string RATE = "af_rate";

	// Token: 0x0400000A RID: 10
	public const string SEARCH = "af_search";

	// Token: 0x0400000B RID: 11
	public const string SPENT_CREDIT = "af_spent_credits";

	// Token: 0x0400000C RID: 12
	public const string ACHIEVEMENT_UNLOCKED = "af_achievement_unlocked";

	// Token: 0x0400000D RID: 13
	public const string CONTENT_VIEW = "af_content_view";

	// Token: 0x0400000E RID: 14
	public const string TRAVEL_BOOKING = "af_travel_booking";

	// Token: 0x0400000F RID: 15
	public const string SHARE = "af_share";

	// Token: 0x04000010 RID: 16
	public const string INVITE = "af_invite";

	// Token: 0x04000011 RID: 17
	public const string LOGIN = "af_login";

	// Token: 0x04000012 RID: 18
	public const string RE_ENGAGE = "af_re_engage";

	// Token: 0x04000013 RID: 19
	public const string UPDATE = "af_update";

	// Token: 0x04000014 RID: 20
	public const string OPENED_FROM_PUSH_NOTIFICATION = "af_opened_from_push_notification";

	// Token: 0x04000015 RID: 21
	public const string LOCATION_CHANGED = "af_location_changed";

	// Token: 0x04000016 RID: 22
	public const string LOCATION_COORDINATES = "af_location_coordinates";

	// Token: 0x04000017 RID: 23
	public const string ORDER_ID = "af_order_id";

	// Token: 0x04000018 RID: 24
	public const string LEVEL = "af_level";

	// Token: 0x04000019 RID: 25
	public const string SCORE = "af_score";

	// Token: 0x0400001A RID: 26
	public const string SUCCESS = "af_success";

	// Token: 0x0400001B RID: 27
	public const string PRICE = "af_price";

	// Token: 0x0400001C RID: 28
	public const string CONTENT_TYPE = "af_content_type";

	// Token: 0x0400001D RID: 29
	public const string CONTENT_ID = "af_content_id";

	// Token: 0x0400001E RID: 30
	public const string CONTENT_LIST = "af_content_list";

	// Token: 0x0400001F RID: 31
	public const string CURRENCY = "af_currency";

	// Token: 0x04000020 RID: 32
	public const string QUANTITY = "af_quantity";

	// Token: 0x04000021 RID: 33
	public const string REGSITRATION_METHOD = "af_registration_method";

	// Token: 0x04000022 RID: 34
	public const string PAYMENT_INFO_AVAILIBLE = "af_payment_info_available";

	// Token: 0x04000023 RID: 35
	public const string MAX_RATING_VALUE = "af_max_rating_value";

	// Token: 0x04000024 RID: 36
	public const string RATING_VALUE = "af_rating_value";

	// Token: 0x04000025 RID: 37
	public const string SEARCH_STRING = "af_search_string";

	// Token: 0x04000026 RID: 38
	public const string DATE_A = "af_date_a";

	// Token: 0x04000027 RID: 39
	public const string DATE_B = "af_date_b";

	// Token: 0x04000028 RID: 40
	public const string DESTINATION_A = "af_destination_a";

	// Token: 0x04000029 RID: 41
	public const string DESTINATION_B = "af_destination_b";

	// Token: 0x0400002A RID: 42
	public const string DESCRIPTION = "af_description";

	// Token: 0x0400002B RID: 43
	public const string CLASS = "af_class";

	// Token: 0x0400002C RID: 44
	public const string EVENT_START = "af_event_start";

	// Token: 0x0400002D RID: 45
	public const string EVENT_END = "af_event_end";

	// Token: 0x0400002E RID: 46
	public const string LATITUDE = "af_lat";

	// Token: 0x0400002F RID: 47
	public const string LONGTITUDE = "af_long";

	// Token: 0x04000030 RID: 48
	public const string CUSTOMER_USER_ID = "af_customer_user_id";

	// Token: 0x04000031 RID: 49
	public const string VALIDATED = "af_validated";

	// Token: 0x04000032 RID: 50
	public const string REVENUE = "af_revenue";

	// Token: 0x04000033 RID: 51
	public const string RECEIPT_ID = "af_receipt_id";

	// Token: 0x04000034 RID: 52
	public const string PARAM_1 = "af_param_1";

	// Token: 0x04000035 RID: 53
	public const string PARAM_2 = "af_param_2";

	// Token: 0x04000036 RID: 54
	public const string PARAM_3 = "af_param_3";

	// Token: 0x04000037 RID: 55
	public const string PARAM_4 = "af_param_4";

	// Token: 0x04000038 RID: 56
	public const string PARAM_5 = "af_param_5";

	// Token: 0x04000039 RID: 57
	public const string PARAM_6 = "af_param_6";

	// Token: 0x0400003A RID: 58
	public const string PARAM_7 = "af_param_7";

	// Token: 0x0400003B RID: 59
	public const string PARAM_8 = "af_param_8";

	// Token: 0x0400003C RID: 60
	public const string PARAM_9 = "af_param_9";

	// Token: 0x0400003D RID: 61
	public const string PARAM_10 = "af_param_10";
}
