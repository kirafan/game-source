﻿using System;
using System.Runtime.InteropServices;

// Token: 0x0200003F RID: 63
[StructLayout(LayoutKind.Sequential, Size = 1)]
public struct CriAtomExPlayback
{
	// Token: 0x060001CC RID: 460 RVA: 0x00005F62 File Offset: 0x00004362
	public CriAtomExPlayback(uint id)
	{
		this = default(CriAtomExPlayback);
		this.id = id;
	}

	// Token: 0x060001CD RID: 461 RVA: 0x00005F72 File Offset: 0x00004372
	public void Stop(bool ignoresReleaseTime)
	{
		if (!ignoresReleaseTime)
		{
			CriAtomExPlayback.criAtomExPlayback_Stop(this.id);
		}
		else
		{
			CriAtomExPlayback.criAtomExPlayback_StopWithoutReleaseTime(this.id);
		}
	}

	// Token: 0x060001CE RID: 462 RVA: 0x00005F95 File Offset: 0x00004395
	public void Pause()
	{
		CriAtomExPlayback.criAtomExPlayback_Pause(this.id, true);
	}

	// Token: 0x060001CF RID: 463 RVA: 0x00005FA3 File Offset: 0x000043A3
	public void Resume(CriAtomEx.ResumeMode mode)
	{
		CriAtomExPlayback.criAtomExPlayback_Resume(this.id, mode);
	}

	// Token: 0x060001D0 RID: 464 RVA: 0x00005FB1 File Offset: 0x000043B1
	public bool IsPaused()
	{
		return CriAtomExPlayback.criAtomExPlayback_IsPaused(this.id);
	}

	// Token: 0x060001D1 RID: 465 RVA: 0x00005FBE File Offset: 0x000043BE
	public bool GetFormatInfo(out CriAtomEx.FormatInfo info)
	{
		return CriAtomExPlayback.criAtomExPlayback_GetFormatInfo(this.id, out info);
	}

	// Token: 0x060001D2 RID: 466 RVA: 0x00005FCC File Offset: 0x000043CC
	public CriAtomExPlayback.Status GetStatus()
	{
		return CriAtomExPlayback.criAtomExPlayback_GetStatus(this.id);
	}

	// Token: 0x060001D3 RID: 467 RVA: 0x00005FD9 File Offset: 0x000043D9
	public long GetTime()
	{
		return CriAtomExPlayback.criAtomExPlayback_GetTime(this.id);
	}

	// Token: 0x060001D4 RID: 468 RVA: 0x00005FE6 File Offset: 0x000043E6
	public long GetTimeSyncedWithAudio()
	{
		return CriAtomExPlayback.criAtomExPlayback_GetTimeSyncedWithAudio(this.id);
	}

	// Token: 0x060001D5 RID: 469 RVA: 0x00005FF3 File Offset: 0x000043F3
	public bool GetNumPlayedSamples(out long numSamples, out int samplingRate)
	{
		return CriAtomExPlayback.criAtomExPlayback_GetNumPlayedSamples(this.id, out numSamples, out samplingRate);
	}

	// Token: 0x060001D6 RID: 470 RVA: 0x00006002 File Offset: 0x00004402
	public long GetSequencePosition()
	{
		return CriAtomExPlayback.criAtomExPlayback_GetSequencePosition(this.id);
	}

	// Token: 0x060001D7 RID: 471 RVA: 0x0000600F File Offset: 0x0000440F
	public int GetCurrentBlockIndex()
	{
		return CriAtomExPlayback.criAtomExPlayback_GetCurrentBlockIndex(this.id);
	}

	// Token: 0x060001D8 RID: 472 RVA: 0x0000601C File Offset: 0x0000441C
	public bool GetTrackInfo(out CriAtomExPlayback.TrackInfo info)
	{
		return CriAtomExPlayback.criAtomExPlayback_GetPlaybackTrackInfo(this.id, out info);
	}

	// Token: 0x060001D9 RID: 473 RVA: 0x0000602A File Offset: 0x0000442A
	public void SetNextBlockIndex(int index)
	{
		CriAtomExPlayback.criAtomExPlayback_SetNextBlockIndex(this.id, index);
	}

	// Token: 0x1700001F RID: 31
	// (get) Token: 0x060001DA RID: 474 RVA: 0x00006038 File Offset: 0x00004438
	// (set) Token: 0x060001DB RID: 475 RVA: 0x00006040 File Offset: 0x00004440
	public uint id { get; private set; }

	// Token: 0x17000020 RID: 32
	// (get) Token: 0x060001DC RID: 476 RVA: 0x00006049 File Offset: 0x00004449
	public CriAtomExPlayback.Status status
	{
		get
		{
			return this.GetStatus();
		}
	}

	// Token: 0x17000021 RID: 33
	// (get) Token: 0x060001DD RID: 477 RVA: 0x00006051 File Offset: 0x00004451
	public long time
	{
		get
		{
			return this.GetTime();
		}
	}

	// Token: 0x17000022 RID: 34
	// (get) Token: 0x060001DE RID: 478 RVA: 0x00006059 File Offset: 0x00004459
	public long timeSyncedWithAudio
	{
		get
		{
			return this.GetTimeSyncedWithAudio();
		}
	}

	// Token: 0x060001DF RID: 479 RVA: 0x00006061 File Offset: 0x00004461
	public void Stop()
	{
		CriAtomExPlayback.criAtomExPlayback_Stop(this.id);
	}

	// Token: 0x060001E0 RID: 480 RVA: 0x0000606E File Offset: 0x0000446E
	public void StopWithoutReleaseTime()
	{
		CriAtomExPlayback.criAtomExPlayback_StopWithoutReleaseTime(this.id);
	}

	// Token: 0x060001E1 RID: 481 RVA: 0x0000607B File Offset: 0x0000447B
	public void Pause(bool sw)
	{
		CriAtomExPlayback.criAtomExPlayback_Pause(this.id, sw);
	}

	// Token: 0x060001E2 RID: 482
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayback_Stop(uint id);

	// Token: 0x060001E3 RID: 483
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayback_StopWithoutReleaseTime(uint id);

	// Token: 0x060001E4 RID: 484
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayback_Pause(uint id, bool sw);

	// Token: 0x060001E5 RID: 485
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayback_Resume(uint id, CriAtomEx.ResumeMode mode);

	// Token: 0x060001E6 RID: 486
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExPlayback_IsPaused(uint id);

	// Token: 0x060001E7 RID: 487
	[DllImport("cri_ware_unity")]
	private static extern CriAtomExPlayback.Status criAtomExPlayback_GetStatus(uint id);

	// Token: 0x060001E8 RID: 488
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExPlayback_GetFormatInfo(uint id, out CriAtomEx.FormatInfo info);

	// Token: 0x060001E9 RID: 489
	[DllImport("cri_ware_unity")]
	private static extern long criAtomExPlayback_GetTime(uint id);

	// Token: 0x060001EA RID: 490
	[DllImport("cri_ware_unity")]
	private static extern long criAtomExPlayback_GetTimeSyncedWithAudio(uint id);

	// Token: 0x060001EB RID: 491
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExPlayback_GetNumPlayedSamples(uint id, out long num_samples, out int sampling_rate);

	// Token: 0x060001EC RID: 492
	[DllImport("cri_ware_unity")]
	private static extern long criAtomExPlayback_GetSequencePosition(uint id);

	// Token: 0x060001ED RID: 493
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayback_SetNextBlockIndex(uint id, int index);

	// Token: 0x060001EE RID: 494
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExPlayback_GetCurrentBlockIndex(uint id);

	// Token: 0x060001EF RID: 495
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExPlayback_GetPlaybackTrackInfo(uint id, out CriAtomExPlayback.TrackInfo info);

	// Token: 0x02000040 RID: 64
	public enum Status
	{
		// Token: 0x0400013F RID: 319
		Prep = 1,
		// Token: 0x04000140 RID: 320
		Playing,
		// Token: 0x04000141 RID: 321
		Removed
	}

	// Token: 0x02000041 RID: 65
	public struct TrackInfo
	{
		// Token: 0x04000142 RID: 322
		public uint id;

		// Token: 0x04000143 RID: 323
		public CriAtomEx.CueType sequenceType;

		// Token: 0x04000144 RID: 324
		public IntPtr playerHn;

		// Token: 0x04000145 RID: 325
		public ushort trackNo;

		// Token: 0x04000146 RID: 326
		public ushort reserved;
	}
}
