﻿using System;
using System.Runtime.InteropServices;

// Token: 0x020000B3 RID: 179
public static class CriAtomExPlaybackDebug
{
	// Token: 0x06000505 RID: 1285 RVA: 0x0000BE02 File Offset: 0x0000A202
	public static bool GetParameter(CriAtomExPlayback playback, CriAtomEx.Parameter parameterId, out float value)
	{
		return CriAtomExPlaybackDebug.criAtomExPlayback_GetParameterFloat32(playback.id, (int)parameterId, out value) == 1;
	}

	// Token: 0x06000506 RID: 1286 RVA: 0x0000BE15 File Offset: 0x0000A215
	public static bool GetParameter(CriAtomExPlayback playback, CriAtomEx.Parameter parameterId, out uint value)
	{
		return CriAtomExPlaybackDebug.criAtomExPlayback_GetParameterUint32(playback.id, (int)parameterId, out value) == 1;
	}

	// Token: 0x06000507 RID: 1287 RVA: 0x0000BE28 File Offset: 0x0000A228
	public static bool GetParameter(CriAtomExPlayback playback, CriAtomEx.Parameter parameterId, out int value)
	{
		return CriAtomExPlaybackDebug.criAtomExPlayback_GetParameterSint32(playback.id, (int)parameterId, out value) == 1;
	}

	// Token: 0x06000508 RID: 1288 RVA: 0x0000BE3B File Offset: 0x0000A23B
	public static bool GetAisacControl(CriAtomExPlayback playback, uint controlId, out float value)
	{
		return CriAtomExPlaybackDebug.criAtomExPlayback_GetAisacControlById(playback.id, controlId, out value) == 1;
	}

	// Token: 0x06000509 RID: 1289 RVA: 0x0000BE4E File Offset: 0x0000A24E
	public static bool GetAisacControl(CriAtomExPlayback playback, string controlName, out float value)
	{
		return CriAtomExPlaybackDebug.criAtomExPlayback_GetAisacControlByName(playback.id, controlName, out value) == 1;
	}

	// Token: 0x0600050A RID: 1290
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExPlayback_GetParameterFloat32(uint id, int parameterId, out float value);

	// Token: 0x0600050B RID: 1291
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExPlayback_GetParameterUint32(uint id, int parameterId, out uint value);

	// Token: 0x0600050C RID: 1292
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExPlayback_GetParameterSint32(uint id, int parameterId, out int value);

	// Token: 0x0600050D RID: 1293
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExPlayback_GetAisacControlById(uint id, uint controlId, out float value);

	// Token: 0x0600050E RID: 1294
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExPlayback_GetAisacControlByName(uint id, string controlName, out float value);
}
