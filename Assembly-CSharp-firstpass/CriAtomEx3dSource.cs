﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x02000035 RID: 53
public class CriAtomEx3dSource : IDisposable
{
	// Token: 0x06000161 RID: 353 RVA: 0x000054D4 File Offset: 0x000038D4
	public CriAtomEx3dSource()
	{
		CriAtomEx3dSource.Config config = default(CriAtomEx3dSource.Config);
		this.handle = CriAtomEx3dSource.criAtomEx3dSource_Create(ref config, IntPtr.Zero, 0);
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x06000162 RID: 354 RVA: 0x00005514 File Offset: 0x00003914
	public void Dispose()
	{
		this.Dispose(true);
	}

	// Token: 0x06000163 RID: 355 RVA: 0x0000551D File Offset: 0x0000391D
	private void Dispose(bool disposing)
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.handle != IntPtr.Zero)
		{
			CriAtomEx3dSource.criAtomEx3dSource_Destroy(this.handle);
			this.handle = IntPtr.Zero;
		}
		if (disposing)
		{
			GC.SuppressFinalize(this);
		}
	}

	// Token: 0x1700001A RID: 26
	// (get) Token: 0x06000164 RID: 356 RVA: 0x0000555D File Offset: 0x0000395D
	public IntPtr nativeHandle
	{
		get
		{
			return this.handle;
		}
	}

	// Token: 0x06000165 RID: 357 RVA: 0x00005565 File Offset: 0x00003965
	public void Update()
	{
		CriAtomEx3dSource.criAtomEx3dSource_Update(this.handle);
	}

	// Token: 0x06000166 RID: 358 RVA: 0x00005572 File Offset: 0x00003972
	public void ResetParameters()
	{
		CriAtomEx3dSource.criAtomEx3dSource_ResetParameters(this.handle);
	}

	// Token: 0x06000167 RID: 359 RVA: 0x00005580 File Offset: 0x00003980
	public void SetPosition(float x, float y, float z)
	{
		CriAtomEx3dSource.CriAtomExVector criAtomExVector;
		criAtomExVector.x = x;
		criAtomExVector.y = y;
		criAtomExVector.z = z;
		CriAtomEx3dSource.criAtomEx3dSource_SetPosition(this.handle, ref criAtomExVector);
	}

	// Token: 0x06000168 RID: 360 RVA: 0x000055B4 File Offset: 0x000039B4
	public void SetVelocity(float x, float y, float z)
	{
		CriAtomEx3dSource.CriAtomExVector criAtomExVector;
		criAtomExVector.x = x;
		criAtomExVector.y = y;
		criAtomExVector.z = z;
		CriAtomEx3dSource.criAtomEx3dSource_SetVelocity(this.handle, ref criAtomExVector);
	}

	// Token: 0x06000169 RID: 361 RVA: 0x000055E8 File Offset: 0x000039E8
	public void SetOrientation(Vector3 front, Vector3 top)
	{
		CriAtomEx3dSource.CriAtomExVector criAtomExVector;
		criAtomExVector.x = front.x;
		criAtomExVector.y = front.y;
		criAtomExVector.z = front.z;
		CriAtomEx3dSource.CriAtomExVector criAtomExVector2;
		criAtomExVector2.x = top.x;
		criAtomExVector2.y = top.y;
		criAtomExVector2.z = top.z;
		CriAtomEx3dSource.criAtomEx3dSource_SetOrientation(this.handle, ref criAtomExVector, ref criAtomExVector2);
	}

	// Token: 0x0600016A RID: 362 RVA: 0x00005658 File Offset: 0x00003A58
	public void SetConeOrientation(float x, float y, float z)
	{
		CriAtomEx3dSource.CriAtomExVector criAtomExVector;
		criAtomExVector.x = x;
		criAtomExVector.y = y;
		criAtomExVector.z = z;
		CriAtomEx3dSource.criAtomEx3dSource_SetConeOrientation(this.handle, ref criAtomExVector);
	}

	// Token: 0x0600016B RID: 363 RVA: 0x0000568A File Offset: 0x00003A8A
	public void SetConeParameter(float insideAngle, float outsideAngle, float outsideVolume)
	{
		CriAtomEx3dSource.criAtomEx3dSource_SetConeParameter(this.handle, insideAngle, outsideAngle, outsideVolume);
	}

	// Token: 0x0600016C RID: 364 RVA: 0x0000569A File Offset: 0x00003A9A
	public void SetMinMaxDistance(float minDistance, float maxDistance)
	{
		CriAtomEx3dSource.criAtomEx3dSource_SetMinMaxAttenuationDistance(this.handle, minDistance, maxDistance);
	}

	// Token: 0x0600016D RID: 365 RVA: 0x000056A9 File Offset: 0x00003AA9
	public void SetDopplerFactor(float dopplerFactor)
	{
		CriAtomEx3dSource.criAtomEx3dSource_SetDopplerFactor(this.handle, dopplerFactor);
	}

	// Token: 0x0600016E RID: 366 RVA: 0x000056B7 File Offset: 0x00003AB7
	public void SetVolume(float volume)
	{
		CriAtomEx3dSource.criAtomEx3dSource_SetVolume(this.handle, volume);
	}

	// Token: 0x0600016F RID: 367 RVA: 0x000056C5 File Offset: 0x00003AC5
	public void SetMaxAngleAisacDelta(float maxDelta)
	{
		CriAtomEx3dSource.criAtomEx3dSource_SetMaxAngleAisacDelta(this.handle, maxDelta);
	}

	// Token: 0x06000170 RID: 368 RVA: 0x000056D4 File Offset: 0x00003AD4
	~CriAtomEx3dSource()
	{
		this.Dispose(false);
	}

	// Token: 0x06000171 RID: 369
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomEx3dSource_Create(ref CriAtomEx3dSource.Config config, IntPtr work, int work_size);

	// Token: 0x06000172 RID: 370
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_Destroy(IntPtr ex_3d_source);

	// Token: 0x06000173 RID: 371
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_Update(IntPtr ex_3d_source);

	// Token: 0x06000174 RID: 372
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_ResetParameters(IntPtr ex_3d_source);

	// Token: 0x06000175 RID: 373
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_SetPosition(IntPtr ex_3d_source, ref CriAtomEx3dSource.CriAtomExVector position);

	// Token: 0x06000176 RID: 374
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_SetVelocity(IntPtr ex_3d_source, ref CriAtomEx3dSource.CriAtomExVector velocity);

	// Token: 0x06000177 RID: 375
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_SetOrientation(IntPtr ex_3d_source, ref CriAtomEx3dSource.CriAtomExVector front, ref CriAtomEx3dSource.CriAtomExVector top);

	// Token: 0x06000178 RID: 376
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_SetConeOrientation(IntPtr ex_3d_source, ref CriAtomEx3dSource.CriAtomExVector cone_orient);

	// Token: 0x06000179 RID: 377
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_SetConeParameter(IntPtr ex_3d_source, float inside_angle, float outside_angle, float outside_volume);

	// Token: 0x0600017A RID: 378
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_SetMinMaxAttenuationDistance(IntPtr ex_3d_source, float min_distance, float max_distance);

	// Token: 0x0600017B RID: 379
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_SetDopplerFactor(IntPtr ex_3d_source, float doppler_factor);

	// Token: 0x0600017C RID: 380
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_SetVolume(IntPtr ex_3d_source, float volume);

	// Token: 0x0600017D RID: 381
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx3dSource_SetMaxAngleAisacDelta(IntPtr ex_3d_source, float max_delta);

	// Token: 0x04000124 RID: 292
	private IntPtr handle = IntPtr.Zero;

	// Token: 0x02000036 RID: 54
	public struct Config
	{
		// Token: 0x04000125 RID: 293
		public int reserved;
	}

	// Token: 0x02000037 RID: 55
	private struct CriAtomExVector
	{
		// Token: 0x04000126 RID: 294
		public float x;

		// Token: 0x04000127 RID: 295
		public float y;

		// Token: 0x04000128 RID: 296
		public float z;
	}
}
