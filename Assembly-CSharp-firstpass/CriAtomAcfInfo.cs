﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

// Token: 0x02000009 RID: 9
public class CriAtomAcfInfo
{
	// Token: 0x06000085 RID: 133 RVA: 0x00003E28 File Offset: 0x00002228
	public static bool GetCueInfo(bool forceReload, string searchPath)
	{
		if (CriAtomAcfInfo.acfInfo == null || forceReload)
		{
			string[] files = Directory.GetFiles(searchPath);
			foreach (string text in files)
			{
				if (Path.GetExtension(text.Replace("\\", "/")) == ".acf")
				{
					CriAtomAcfInfo.acfInfo = new CriAtomAcfInfo.AcfInfo(Path.GetFileNameWithoutExtension(text), 0, string.Empty, Path.GetFileName(text), string.Empty, string.Empty);
					CriAtomAcfInfo.acfInfo.acfFilePath = text;
					break;
				}
			}
			if (CriAtomAcfInfo.acfInfo == null)
			{
				Debug.Log("CriAtomAcfInfo.acfInfo is null. \"" + searchPath + "\"");
			}
		}
		return CriAtomAcfInfo.acfInfo != null;
	}

	// Token: 0x0400005E RID: 94
	public static CriAtomAcfInfo.AcfInfo acfInfo;

	// Token: 0x0200000A RID: 10
	public class InfoBase
	{
		// Token: 0x0400005F RID: 95
		public string name = "dummyCueSheet";

		// Token: 0x04000060 RID: 96
		public int id;

		// Token: 0x04000061 RID: 97
		public string comment = string.Empty;
	}

	// Token: 0x0200000B RID: 11
	public class AcfInfo : CriAtomAcfInfo.InfoBase
	{
		// Token: 0x06000088 RID: 136 RVA: 0x00003F0C File Offset: 0x0000230C
		public AcfInfo(string n, int inId, string com, string inAcfPath, string _guid, string _dspBusSetting)
		{
			this.name = n;
			this.id = inId;
			this.comment = com;
			this.acfPath = inAcfPath;
			this.atomGuid = _guid;
			this.dspBusSetting = _dspBusSetting;
		}

		// Token: 0x04000062 RID: 98
		public string acfPath = "dummyCueSheet.acf";

		// Token: 0x04000063 RID: 99
		public List<CriAtomAcfInfo.AcbInfo> acbInfoList = new List<CriAtomAcfInfo.AcbInfo>();

		// Token: 0x04000064 RID: 100
		public string atomGuid = string.Empty;

		// Token: 0x04000065 RID: 101
		public string dspBusSetting = "DspBusSetting_0";

		// Token: 0x04000066 RID: 102
		public List<string> aisacControlNameList = new List<string>();

		// Token: 0x04000067 RID: 103
		public string acfFilePath = string.Empty;
	}

	// Token: 0x0200000C RID: 12
	public class AcbInfo : CriAtomAcfInfo.InfoBase
	{
		// Token: 0x06000089 RID: 137 RVA: 0x00003F90 File Offset: 0x00002390
		public AcbInfo(string n, int inId, string com, string inAcbPath, string inAwbPath, string _guid)
		{
			this.name = n;
			this.id = inId;
			this.comment = com;
			this.acbPath = inAcbPath;
			this.awbPath = inAwbPath;
			this.atomGuid = _guid;
		}

		// Token: 0x04000068 RID: 104
		public string acbPath = "dummyCueSheet.acb";

		// Token: 0x04000069 RID: 105
		public string awbPath = "dummyCueSheet_streamfiles.awb";

		// Token: 0x0400006A RID: 106
		public string atomGuid = string.Empty;

		// Token: 0x0400006B RID: 107
		public Dictionary<int, CriAtomAcfInfo.CueInfo> cueInfoList = new Dictionary<int, CriAtomAcfInfo.CueInfo>();
	}

	// Token: 0x0200000D RID: 13
	public class CueInfo : CriAtomAcfInfo.InfoBase
	{
		// Token: 0x0600008A RID: 138 RVA: 0x00003FFC File Offset: 0x000023FC
		public CueInfo(string n, int inId, string com)
		{
			this.name = n;
			this.id = inId;
			this.comment = com;
		}
	}
}
