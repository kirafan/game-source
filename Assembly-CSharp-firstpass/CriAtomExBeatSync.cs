﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000029 RID: 41
public static class CriAtomExBeatSync
{
	// Token: 0x06000126 RID: 294 RVA: 0x000050D2 File Offset: 0x000034D2
	public static void SetCallback(CriAtomExBeatSync.CbFunc func)
	{
		CriAtom.SetBeatSyncCallback(func);
	}

	// Token: 0x0200002A RID: 42
	public struct Info
	{
		// Token: 0x0400010E RID: 270
		public IntPtr playerHn;

		// Token: 0x0400010F RID: 271
		public uint playbackId;

		// Token: 0x04000110 RID: 272
		public uint barCount;

		// Token: 0x04000111 RID: 273
		public uint beatCount;
	}

	// Token: 0x0200002B RID: 43
	// (Invoke) Token: 0x06000128 RID: 296
	[UnmanagedFunctionPointer(CallingConvention.Cdecl)]
	public delegate void CbFunc(ref CriAtomExBeatSync.Info info);
}
