﻿using System;
using UnityEngine;

// Token: 0x02000063 RID: 99
public class CriFsLoadAssetBundleRequest : CriFsRequest
{
	// Token: 0x06000325 RID: 805 RVA: 0x00007C05 File Offset: 0x00006005
	public CriFsLoadAssetBundleRequest(CriFsBinder binder, string path, int readUnitSize)
	{
		this.path = path;
		this.loadFileReq = CriFsUtility.LoadFile(binder, path, readUnitSize);
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Fs);
	}

	// Token: 0x17000033 RID: 51
	// (get) Token: 0x06000326 RID: 806 RVA: 0x00007C29 File Offset: 0x00006029
	// (set) Token: 0x06000327 RID: 807 RVA: 0x00007C31 File Offset: 0x00006031
	public string path { get; private set; }

	// Token: 0x17000034 RID: 52
	// (get) Token: 0x06000328 RID: 808 RVA: 0x00007C3A File Offset: 0x0000603A
	// (set) Token: 0x06000329 RID: 809 RVA: 0x00007C42 File Offset: 0x00006042
	public AssetBundle assetBundle { get; private set; }

	// Token: 0x0600032A RID: 810 RVA: 0x00007C4C File Offset: 0x0000604C
	public override void Update()
	{
		if (this.loadFileReq != null)
		{
			if (this.loadFileReq.isDone)
			{
				if (this.loadFileReq.error != null)
				{
					base.error = "Error occurred.";
					base.Done();
				}
				else
				{
					this.assetBundleReq = AssetBundle.LoadFromMemoryAsync(this.loadFileReq.bytes);
				}
				this.loadFileReq = null;
			}
		}
		else if (this.assetBundleReq != null)
		{
			if (this.assetBundleReq.isDone)
			{
				this.assetBundle = this.assetBundleReq.assetBundle;
				base.Done();
			}
		}
		else
		{
			base.Done();
		}
	}

	// Token: 0x0600032B RID: 811 RVA: 0x00007CF9 File Offset: 0x000060F9
	protected override void Dispose(bool disposing)
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.loadFileReq != null)
		{
			this.loadFileReq.Dispose();
			this.loadFileReq = null;
		}
	}

	// Token: 0x040001C2 RID: 450
	private CriFsLoadFileRequest loadFileReq;

	// Token: 0x040001C3 RID: 451
	private AssetBundleCreateRequest assetBundleReq;
}
