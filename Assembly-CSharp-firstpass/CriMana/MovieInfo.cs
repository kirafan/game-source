﻿using System;
using System.Runtime.InteropServices;

namespace CriMana
{
	// Token: 0x02000075 RID: 117
	[StructLayout(LayoutKind.Sequential)]
	public class MovieInfo
	{
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x06000396 RID: 918 RVA: 0x000087E2 File Offset: 0x00006BE2
		// (set) Token: 0x06000397 RID: 919 RVA: 0x000087ED File Offset: 0x00006BED
		public bool hasAlpha
		{
			get
			{
				return this._hasAlpha == 1U;
			}
			set
			{
				this._hasAlpha = ((!value) ? 0U : 1U);
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000398 RID: 920 RVA: 0x00008802 File Offset: 0x00006C02
		// (set) Token: 0x06000399 RID: 921 RVA: 0x0000880A File Offset: 0x00006C0A
		public CodecType codecType
		{
			get
			{
				return (CodecType)this._codecType;
			}
			set
			{
				this._codecType = (uint)value;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x0600039A RID: 922 RVA: 0x00008813 File Offset: 0x00006C13
		// (set) Token: 0x0600039B RID: 923 RVA: 0x0000881B File Offset: 0x00006C1B
		public CodecType alphaCodecType
		{
			get
			{
				return (CodecType)this._alphaCodecType;
			}
			set
			{
				this._codecType = (uint)value;
			}
		}

		// Token: 0x04000203 RID: 515
		private uint _reserved1;

		// Token: 0x04000204 RID: 516
		private uint _hasAlpha;

		// Token: 0x04000205 RID: 517
		public uint width;

		// Token: 0x04000206 RID: 518
		public uint height;

		// Token: 0x04000207 RID: 519
		public uint dispWidth;

		// Token: 0x04000208 RID: 520
		public uint dispHeight;

		// Token: 0x04000209 RID: 521
		public uint framerateN;

		// Token: 0x0400020A RID: 522
		public uint framerateD;

		// Token: 0x0400020B RID: 523
		public uint totalFrames;

		// Token: 0x0400020C RID: 524
		private uint _codecType;

		// Token: 0x0400020D RID: 525
		private uint _alphaCodecType;

		// Token: 0x0400020E RID: 526
		public uint numAudioStreams;

		// Token: 0x0400020F RID: 527
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 32)]
		public AudioInfo[] audioPrm;

		// Token: 0x04000210 RID: 528
		public uint numSubtitleChannels;

		// Token: 0x04000211 RID: 529
		public uint maxSubtitleSize;
	}
}
