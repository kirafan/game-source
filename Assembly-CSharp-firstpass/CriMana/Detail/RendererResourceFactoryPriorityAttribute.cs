﻿using System;

namespace CriMana.Detail
{
	// Token: 0x02000081 RID: 129
	[AttributeUsage(AttributeTargets.Class)]
	public class RendererResourceFactoryPriorityAttribute : Attribute
	{
		// Token: 0x06000406 RID: 1030 RVA: 0x0000986E File Offset: 0x00007C6E
		public RendererResourceFactoryPriorityAttribute(int priority)
		{
			this.priority = priority;
		}

		// Token: 0x0400024A RID: 586
		public readonly int priority;
	}
}
