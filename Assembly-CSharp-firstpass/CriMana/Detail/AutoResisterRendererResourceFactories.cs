﻿using System;
using System.Reflection;
using UnityEngine;

namespace CriMana.Detail
{
	// Token: 0x02000082 RID: 130
	public static class AutoResisterRendererResourceFactories
	{
		// Token: 0x06000407 RID: 1031 RVA: 0x00009880 File Offset: 0x00007C80
		public static void InvokeAutoRegister()
		{
			Type[] nestedTypes = typeof(AutoResisterRendererResourceFactories).GetNestedTypes(BindingFlags.Public);
			foreach (Type type in nestedTypes)
			{
				if (!type.IsSubclassOf(typeof(RendererResourceFactory)))
				{
					Debug.LogError("[CRIWARE] internal logic error. " + type.Name + " is required to be a subclass of RendererResourceFactory.");
				}
				else
				{
					RendererResourceFactoryPriorityAttribute rendererResourceFactoryPriorityAttribute = (RendererResourceFactoryPriorityAttribute)Attribute.GetCustomAttribute(type, typeof(RendererResourceFactoryPriorityAttribute));
					if (rendererResourceFactoryPriorityAttribute == null)
					{
						Debug.LogError("[CRIWARE] internal logic error. need priority attribute. (" + type.Name + ")");
					}
					else
					{
						RendererResourceFactory.RegisterFactory((RendererResourceFactory)Activator.CreateInstance(type), rendererResourceFactoryPriorityAttribute.priority);
					}
				}
			}
		}

		// Token: 0x02000083 RID: 131
		[RendererResourceFactoryPriority(7000)]
		public class RendererResourceFactoryAndroidH264Rgb : RendererResourceFactory
		{
			// Token: 0x06000409 RID: 1033 RVA: 0x0000994C File Offset: 0x00007D4C
			public override RendererResource CreateRendererResource(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
			{
				bool flag = movieInfo.codecType == CodecType.H264;
				bool flag2 = !movieInfo.hasAlpha;
				bool flag3 = flag && flag2;
				return (!flag3) ? null : new RendererResourceAndroidH264Rgb(playerId, movieInfo, additive, userShader);
			}

			// Token: 0x0600040A RID: 1034 RVA: 0x0000998E File Offset: 0x00007D8E
			protected override void OnDisposeManaged()
			{
			}

			// Token: 0x0600040B RID: 1035 RVA: 0x00009990 File Offset: 0x00007D90
			protected override void OnDisposeUnmanaged()
			{
			}
		}

		// Token: 0x02000085 RID: 133
		[RendererResourceFactoryPriority(5050)]
		public class RendererResourceFactoryAndroidSofdecPrimeYuv : RendererResourceFactory
		{
			// Token: 0x0600041D RID: 1053 RVA: 0x0000999C File Offset: 0x00007D9C
			public override RendererResource CreateRendererResource(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
			{
				bool flag = movieInfo.codecType == CodecType.SofdecPrime;
				bool flag2 = flag;
				return (!flag2) ? null : new RendererResourceAndroidSofdecPrimeYuv(playerId, movieInfo, additive, userShader);
			}

			// Token: 0x0600041E RID: 1054 RVA: 0x000099CB File Offset: 0x00007DCB
			protected override void OnDisposeManaged()
			{
			}

			// Token: 0x0600041F RID: 1055 RVA: 0x000099CD File Offset: 0x00007DCD
			protected override void OnDisposeUnmanaged()
			{
			}
		}
	}
}
