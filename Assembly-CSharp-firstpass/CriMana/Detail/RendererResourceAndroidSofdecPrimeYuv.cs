﻿using System;
using UnityEngine;

namespace CriMana.Detail
{
	// Token: 0x02000086 RID: 134
	public class RendererResourceAndroidSofdecPrimeYuv : RendererResource
	{
		// Token: 0x06000420 RID: 1056 RVA: 0x00009CF0 File Offset: 0x000080F0
		public RendererResourceAndroidSofdecPrimeYuv(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
		{
			this.width = RendererResource.NextPowerOfTwo(RendererResource.Ceiling64((int)movieInfo.width));
			this.height = RendererResource.NextPowerOfTwo(RendererResource.Ceiling16((int)movieInfo.height));
			this.hasAlpha = movieInfo.hasAlpha;
			this.additive = additive;
			this.useUserShader = (userShader != null);
			if (this.useUserShader)
			{
				this.shader = userShader;
			}
			else
			{
				string name = (!this.hasAlpha) ? ((!additive) ? "CriMana/SofdecPrimeYuv" : "CriMana/SofdecPrimeYuvAdditive") : ((!additive) ? "CriMana/SofdecPrimeYuva" : "CriMana/SofdecPrimeYuvaAdditive");
				this.shader = Shader.Find(name);
			}
			this.UpdateMovieTextureST(movieInfo.dispWidth, movieInfo.dispHeight);
			TextureFormat format = TextureFormat.Alpha8;
			this.textureY = new Texture2D[this.numTextureSets];
			this.textureU = new Texture2D[this.numTextureSets];
			this.textureV = new Texture2D[this.numTextureSets];
			this.textureA = new Texture2D[this.numTextureSets];
			this.nativeTextures = new IntPtr[this.numTextureSets][];
			for (int i = 0; i < this.numTextureSets; i++)
			{
				this.textureY[i] = new Texture2D(this.width, this.height, format, false, true);
				this.textureY[i].wrapMode = TextureWrapMode.Clamp;
				this.textureU[i] = new Texture2D(this.width / 2, this.height / 2, format, false, true);
				this.textureU[i].wrapMode = TextureWrapMode.Clamp;
				this.textureV[i] = new Texture2D(this.width / 2, this.height / 2, format, false, true);
				this.textureV[i].wrapMode = TextureWrapMode.Clamp;
				this.nativeTextures[i] = new IntPtr[4];
				this.nativeTextures[i][0] = this.textureY[i].GetNativeTexturePtr();
				this.nativeTextures[i][1] = this.textureU[i].GetNativeTexturePtr();
				this.nativeTextures[i][2] = this.textureV[i].GetNativeTexturePtr();
				if (this.hasAlpha)
				{
					this.textureA[i] = new Texture2D(this.width, this.height, format, false, true);
					this.textureA[i].wrapMode = TextureWrapMode.Clamp;
					this.nativeTextures[i][3] = this.textureA[i].GetNativeTexturePtr();
				}
				else
				{
					this.textureA[i] = null;
					this.nativeTextures[i][3] = IntPtr.Zero;
				}
			}
			this.playerID = playerId;
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x00009FA3 File Offset: 0x000083A3
		protected override void OnDisposeManaged()
		{
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x00009FA8 File Offset: 0x000083A8
		protected override void OnDisposeUnmanaged()
		{
			for (int i = 0; i < this.numTextureSets; i++)
			{
				if (this.textureY[i] != null)
				{
					UnityEngine.Object.Destroy(this.textureY[i]);
					this.textureY[i] = null;
				}
				if (this.textureU[i] != null)
				{
					UnityEngine.Object.Destroy(this.textureU[i]);
					this.textureU[i] = null;
				}
				if (this.textureV[i] != null)
				{
					UnityEngine.Object.Destroy(this.textureV[i]);
					this.textureV[i] = null;
				}
				if (this.textureA[i] != null)
				{
					UnityEngine.Object.Destroy(this.textureA[i]);
					this.textureA[i] = null;
				}
			}
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x0000A070 File Offset: 0x00008470
		public override bool IsPrepared()
		{
			return true;
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x0000A073 File Offset: 0x00008473
		public override bool ContinuePreparing()
		{
			return true;
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x0000A078 File Offset: 0x00008478
		public override bool IsSuitable(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
		{
			bool flag = movieInfo.codecType == CodecType.SofdecPrime;
			bool flag2 = this.width >= (int)movieInfo.width && this.height >= (int)movieInfo.height;
			bool flag3 = this.hasAlpha == movieInfo.hasAlpha;
			bool flag4 = this.additive == additive;
			bool flag5 = !this.useUserShader || userShader == this.shader;
			return flag && flag2 && flag3 && flag4 && flag5;
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x0000A10B File Offset: 0x0000850B
		public override void AttachToPlayer(int playerId)
		{
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x0000A110 File Offset: 0x00008510
		public override bool UpdateFrame(int playerId, FrameInfo frameInfo)
		{
			bool flag = RendererResource.criManaUnityPlayer_UpdateFrame(playerId, 0, null, frameInfo);
			if (flag)
			{
				this.UpdateMovieTextureST(frameInfo.dispWidth, frameInfo.dispHeight);
				this.drawTextureSet = this.currentTextureSet;
				this.currentTextureSet = (this.currentTextureSet + 1) % this.numTextureSets;
			}
			return flag;
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x0000A164 File Offset: 0x00008564
		public override bool UpdateMaterial(Material material)
		{
			material.shader = this.shader;
			material.SetTexture("_TextureY", this.textureY[this.drawTextureSet]);
			material.SetTexture("_TextureU", this.textureU[this.drawTextureSet]);
			material.SetTexture("_TextureV", this.textureV[this.drawTextureSet]);
			material.SetInt("_IsLinearColorSpace", (QualitySettings.activeColorSpace != ColorSpace.Linear) ? 0 : 1);
			if (this.hasAlpha)
			{
				material.SetTexture("_TextureA", this.textureA[this.drawTextureSet]);
			}
			material.SetVector("_MovieTexture_ST", this.movieTextureST);
			return true;
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x0000A218 File Offset: 0x00008618
		private void UpdateMovieTextureST(uint dispWidth, uint dispHeight)
		{
			float x = ((ulong)dispWidth == (ulong)((long)this.width)) ? 1f : ((dispWidth - 1U) / (float)this.width);
			float num = ((ulong)dispHeight == (ulong)((long)this.height)) ? 1f : ((dispHeight - 1U) / (float)this.height);
			this.movieTextureST.x = x;
			this.movieTextureST.y = -num;
			this.movieTextureST.z = 0f;
			this.movieTextureST.w = num;
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x0000A2A8 File Offset: 0x000086A8
		public override void UpdateTextures()
		{
			int num_textures = 3;
			if (this.hasAlpha)
			{
				num_textures = 4;
			}
			RendererResource.criManaUnityPlayer_UpdateTextures(this.playerID, num_textures, this.nativeTextures[this.drawTextureSet]);
		}

		// Token: 0x04000257 RID: 599
		private int width;

		// Token: 0x04000258 RID: 600
		private int height;

		// Token: 0x04000259 RID: 601
		private bool hasAlpha;

		// Token: 0x0400025A RID: 602
		private bool additive;

		// Token: 0x0400025B RID: 603
		private bool useUserShader;

		// Token: 0x0400025C RID: 604
		private Shader shader;

		// Token: 0x0400025D RID: 605
		private Vector4 movieTextureST = Vector4.zero;

		// Token: 0x0400025E RID: 606
		private Texture2D[] textureY;

		// Token: 0x0400025F RID: 607
		private Texture2D[] textureU;

		// Token: 0x04000260 RID: 608
		private Texture2D[] textureV;

		// Token: 0x04000261 RID: 609
		private Texture2D[] textureA;

		// Token: 0x04000262 RID: 610
		private IntPtr[][] nativeTextures;

		// Token: 0x04000263 RID: 611
		private int numTextureSets = 2;

		// Token: 0x04000264 RID: 612
		private int currentTextureSet;

		// Token: 0x04000265 RID: 613
		private int drawTextureSet;

		// Token: 0x04000266 RID: 614
		private int playerID;

		// Token: 0x04000267 RID: 615
		private static int kcnt;
	}
}
