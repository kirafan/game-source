﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace CriMana.Detail
{
	// Token: 0x02000080 RID: 128
	public abstract class RendererResourceFactory : IDisposable
	{
		// Token: 0x060003FC RID: 1020 RVA: 0x00009705 File Offset: 0x00007B05
		public static void RegisterFactory(RendererResourceFactory factory, int priority)
		{
			RendererResourceFactory.factoryList.Add(priority, factory);
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x00009714 File Offset: 0x00007B14
		public static void DisposeAllFactories()
		{
			foreach (KeyValuePair<int, RendererResourceFactory> keyValuePair in RendererResourceFactory.factoryList)
			{
				keyValuePair.Value.Dispose();
			}
			RendererResourceFactory.factoryList.Clear();
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x0000977C File Offset: 0x00007B7C
		public static RendererResource DispatchAndCreate(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
		{
			foreach (KeyValuePair<int, RendererResourceFactory> keyValuePair in RendererResourceFactory.factoryList)
			{
				RendererResource rendererResource = keyValuePair.Value.CreateRendererResource(playerId, movieInfo, additive, userShader);
				if (rendererResource != null)
				{
					return rendererResource;
				}
			}
			Debug.LogError("[CRIWARE] unsupported movie.");
			return null;
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x000097FC File Offset: 0x00007BFC
		~RendererResourceFactory()
		{
			this.Dispose(false);
		}

		// Token: 0x06000400 RID: 1024 RVA: 0x0000982C File Offset: 0x00007C2C
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x0000983B File Offset: 0x00007C3B
		private void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				this.OnDisposeManaged();
			}
			this.OnDisposeUnmanaged();
			this.disposed = true;
		}

		// Token: 0x06000402 RID: 1026
		protected abstract void OnDisposeManaged();

		// Token: 0x06000403 RID: 1027
		protected abstract void OnDisposeUnmanaged();

		// Token: 0x06000404 RID: 1028
		public abstract RendererResource CreateRendererResource(int playerId, MovieInfo movieInfo, bool additive, Shader userShader);

		// Token: 0x04000248 RID: 584
		private static SortedList<int, RendererResourceFactory> factoryList = new SortedList<int, RendererResourceFactory>();

		// Token: 0x04000249 RID: 585
		private bool disposed;
	}
}
