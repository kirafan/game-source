﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace CriMana.Detail
{
	// Token: 0x02000084 RID: 132
	public class RendererResourceAndroidH264Rgb : RendererResource
	{
		// Token: 0x0600040C RID: 1036 RVA: 0x000099D0 File Offset: 0x00007DD0
		public RendererResourceAndroidH264Rgb(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
		{
			if (movieInfo.hasAlpha)
			{
				Debug.LogError("[CRIWARE] H.264 with Alpha is unsupported");
			}
			this.playerId = playerId;
			this.width = (int)movieInfo.width;
			this.height = (int)movieInfo.height;
			this.hasAlpha = movieInfo.hasAlpha;
			this.additive = additive;
			this.useUserShader = (userShader != null);
			if (userShader != null)
			{
				this.shader = userShader;
			}
			else
			{
				string name = (!this.hasAlpha) ? ((!additive) ? "CriMana/AndroidH264Rgb" : "CriMana/AndroidH264RgbAdditive") : ((!additive) ? "Diffuse" : "Diffuse");
				this.shader = Shader.Find(name);
			}
			this.oesTexture = RendererResourceAndroidH264Rgb.criManaUnity_MediaCodecCreateTexture_ANDROID();
			this.texture = Texture2D.CreateExternalTexture(this.width, this.height, TextureFormat.ARGB32, false, false, (IntPtr)((long)((ulong)this.oesTexture)));
		}

		// Token: 0x0600040D RID: 1037 RVA: 0x00009AE1 File Offset: 0x00007EE1
		protected override void OnDisposeManaged()
		{
		}

		// Token: 0x0600040E RID: 1038 RVA: 0x00009AE4 File Offset: 0x00007EE4
		protected override void OnDisposeUnmanaged()
		{
			if (this.texture != null)
			{
				UnityEngine.Object.Destroy(this.texture);
				if (this.attached)
				{
					RendererResourceAndroidH264Rgb.criManaUnityPlayer_MediaCodecDetachTexture_ANDROID(this.playerId, this.oesTexture);
					this.attached = false;
				}
				RendererResourceAndroidH264Rgb.criManaUnity_MediaCodecDeleteTexture_ANDROID(this.oesTexture);
				this.oesTexture = 0U;
			}
			this.texture = null;
			this.movieTextureSTRaw = null;
		}

		// Token: 0x0600040F RID: 1039 RVA: 0x00009B50 File Offset: 0x00007F50
		public override bool IsPrepared()
		{
			return true;
		}

		// Token: 0x06000410 RID: 1040 RVA: 0x00009B53 File Offset: 0x00007F53
		public override bool ContinuePreparing()
		{
			return true;
		}

		// Token: 0x06000411 RID: 1041 RVA: 0x00009B58 File Offset: 0x00007F58
		public override bool IsSuitable(int playerId, MovieInfo movieInfo, bool additive, Shader userShader)
		{
			bool flag = playerId == this.playerId;
			bool flag2 = movieInfo.codecType == CodecType.H264;
			bool flag3 = this.width == (int)movieInfo.width && this.height == (int)movieInfo.height;
			bool flag4 = this.hasAlpha == movieInfo.hasAlpha;
			bool flag5 = this.additive == additive;
			bool flag6 = !this.useUserShader || userShader == this.shader;
			return flag && flag2 && flag3 && flag4 && flag5 && flag6;
		}

		// Token: 0x06000412 RID: 1042 RVA: 0x00009BFA File Offset: 0x00007FFA
		public override void AttachToPlayer(int playerId)
		{
			if (this.playerId == playerId)
			{
				RendererResourceAndroidH264Rgb.criManaUnityPlayer_MediaCodecAttachTexture_ANDROID(playerId, this.oesTexture);
				this.attached = true;
			}
			else
			{
				Debug.LogError("[CRIWARE] Internal logic error");
			}
		}

		// Token: 0x06000413 RID: 1043 RVA: 0x00009C2C File Offset: 0x0000802C
		public override bool UpdateFrame(int playerId, FrameInfo frameInfo)
		{
			bool flag = RendererResourceAndroidH264Rgb.criManaUnityPlayer_MediaCodecUpdateTexture_ANDROID(playerId, frameInfo, this.movieTextureSTRaw);
			if (flag)
			{
				this.UpdateMovieTextureST();
			}
			return flag;
		}

		// Token: 0x06000414 RID: 1044 RVA: 0x00009C54 File Offset: 0x00008054
		public override bool UpdateMaterial(Material material)
		{
			material.shader = this.shader;
			material.mainTexture = this.texture;
			material.SetTexture("_TextureRGB", this.texture);
			material.SetVector("_MovieTexture_ST", this.movieTextureST);
			return true;
		}

		// Token: 0x06000415 RID: 1045 RVA: 0x00009C94 File Offset: 0x00008094
		private void UpdateMovieTextureST()
		{
			this.movieTextureST.x = this.movieTextureSTRaw[0];
			this.movieTextureST.y = this.movieTextureSTRaw[1];
			this.movieTextureST.z = this.movieTextureSTRaw[2];
			this.movieTextureST.w = this.movieTextureSTRaw[3];
		}

		// Token: 0x06000416 RID: 1046 RVA: 0x00009CED File Offset: 0x000080ED
		public override void UpdateTextures()
		{
		}

		// Token: 0x06000417 RID: 1047
		[DllImport("cri_ware_unity")]
		private static extern uint criManaUnity_MediaCodecCreateTexture_ANDROID();

		// Token: 0x06000418 RID: 1048
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnity_MediaCodecDeleteTexture_ANDROID(uint oes_texture);

		// Token: 0x06000419 RID: 1049
		[DllImport("cri_ware_unity")]
		private static extern bool criManaUnityPlayer_MediaCodecAttachTexture_ANDROID(int player_id, uint oes_texture);

		// Token: 0x0600041A RID: 1050
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_MediaCodecDetachTexture_ANDROID(int player_id, uint oes_texture);

		// Token: 0x0600041B RID: 1051
		[DllImport("cri_ware_unity")]
		private static extern bool criManaUnityPlayer_MediaCodecUpdateTexture_ANDROID(int player_id, [In] [Out] FrameInfo frame_info, [MarshalAs(UnmanagedType.LPArray)] float[] movieTextureST);

		// Token: 0x0400024B RID: 587
		private int playerId;

		// Token: 0x0400024C RID: 588
		private bool attached;

		// Token: 0x0400024D RID: 589
		private int width;

		// Token: 0x0400024E RID: 590
		private int height;

		// Token: 0x0400024F RID: 591
		private bool hasAlpha;

		// Token: 0x04000250 RID: 592
		private bool additive;

		// Token: 0x04000251 RID: 593
		private bool useUserShader;

		// Token: 0x04000252 RID: 594
		private Shader shader;

		// Token: 0x04000253 RID: 595
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
		private float[] movieTextureSTRaw = new float[4];

		// Token: 0x04000254 RID: 596
		private Vector4 movieTextureST = Vector4.zero;

		// Token: 0x04000255 RID: 597
		private uint oesTexture;

		// Token: 0x04000256 RID: 598
		private Texture2D texture;
	}
}
