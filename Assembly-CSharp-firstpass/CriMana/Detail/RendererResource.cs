﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

namespace CriMana.Detail
{
	// Token: 0x0200007F RID: 127
	public abstract class RendererResource : IDisposable
	{
		// Token: 0x060003E8 RID: 1000 RVA: 0x00009640 File Offset: 0x00007A40
		~RendererResource()
		{
			this.Dispose(false);
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x00009670 File Offset: 0x00007A70
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x0000967F File Offset: 0x00007A7F
		private void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				this.OnDisposeManaged();
			}
			this.OnDisposeUnmanaged();
			this.disposed = true;
		}

		// Token: 0x060003EB RID: 1003
		protected abstract void OnDisposeManaged();

		// Token: 0x060003EC RID: 1004
		protected abstract void OnDisposeUnmanaged();

		// Token: 0x060003ED RID: 1005
		public abstract bool IsPrepared();

		// Token: 0x060003EE RID: 1006
		public abstract bool ContinuePreparing();

		// Token: 0x060003EF RID: 1007
		public abstract void AttachToPlayer(int playerId);

		// Token: 0x060003F0 RID: 1008
		public abstract bool UpdateFrame(int playerId, FrameInfo frameInfo);

		// Token: 0x060003F1 RID: 1009
		public abstract bool UpdateMaterial(Material material);

		// Token: 0x060003F2 RID: 1010
		public abstract void UpdateTextures();

		// Token: 0x060003F3 RID: 1011
		public abstract bool IsSuitable(int playerId, MovieInfo movieInfo, bool additive, Shader userShader);

		// Token: 0x060003F4 RID: 1012 RVA: 0x000096A6 File Offset: 0x00007AA6
		public static uint NextPowerOfTwo(uint x)
		{
			x -= 1U;
			x |= x >> 1;
			x |= x >> 2;
			x |= x >> 4;
			x |= x >> 8;
			x |= x >> 16;
			return x + 1U;
		}

		// Token: 0x060003F5 RID: 1013 RVA: 0x000096D4 File Offset: 0x00007AD4
		public static int NextPowerOfTwo(int x)
		{
			return (int)RendererResource.NextPowerOfTwo((uint)x);
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x000096DC File Offset: 0x00007ADC
		public static int Ceiling16(int x)
		{
			return x + 15 & -16;
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x000096E5 File Offset: 0x00007AE5
		public static int Ceiling64(int x)
		{
			return x + 63 & -64;
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x000096EE File Offset: 0x00007AEE
		public static int Ceiling256(int x)
		{
			return x + 255 & -256;
		}

		// Token: 0x060003F9 RID: 1017
		[DllImport("cri_ware_unity")]
		protected static extern bool criManaUnityPlayer_UpdateFrame(int player_id, int num_textures, IntPtr[] tex_ptrs, [In] [Out] FrameInfo frame_info);

		// Token: 0x060003FA RID: 1018
		[DllImport("cri_ware_unity")]
		protected static extern bool criManaUnityPlayer_UpdateTextures(int player_id, int num_textures, [In] [Out] IntPtr[] tex_ptrs);

		// Token: 0x04000247 RID: 583
		private bool disposed;
	}
}
