﻿using System;

namespace CriMana
{
	// Token: 0x02000073 RID: 115
	public enum AlphaType
	{
		// Token: 0x040001FC RID: 508
		CompoOpaq,
		// Token: 0x040001FD RID: 509
		CompoAlphaFull,
		// Token: 0x040001FE RID: 510
		CompoAlpha3Step,
		// Token: 0x040001FF RID: 511
		CompoAlpha32Bit
	}
}
