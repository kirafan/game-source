﻿using System;

namespace CriMana
{
	// Token: 0x02000077 RID: 119
	public struct EventPoint
	{
		// Token: 0x0400021F RID: 543
		public IntPtr cueName;

		// Token: 0x04000220 RID: 544
		public uint cueNameSize;

		// Token: 0x04000221 RID: 545
		public ulong time;

		// Token: 0x04000222 RID: 546
		public ulong tunit;

		// Token: 0x04000223 RID: 547
		public int type;

		// Token: 0x04000224 RID: 548
		public IntPtr paramString;

		// Token: 0x04000225 RID: 549
		public uint paramStringSize;

		// Token: 0x04000226 RID: 550
		public uint cntCallback;
	}
}
