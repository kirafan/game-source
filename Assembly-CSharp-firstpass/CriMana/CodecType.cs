﻿using System;

namespace CriMana
{
	// Token: 0x02000072 RID: 114
	public enum CodecType
	{
		// Token: 0x040001F8 RID: 504
		Unknown,
		// Token: 0x040001F9 RID: 505
		SofdecPrime,
		// Token: 0x040001FA RID: 506
		H264 = 5
	}
}
