﻿using System;
using System.Runtime.InteropServices;

namespace CriMana
{
	// Token: 0x02000076 RID: 118
	[StructLayout(LayoutKind.Sequential)]
	public class FrameInfo
	{
		// Token: 0x04000212 RID: 530
		public int frameNo;

		// Token: 0x04000213 RID: 531
		public int frameNoPerFile;

		// Token: 0x04000214 RID: 532
		public uint width;

		// Token: 0x04000215 RID: 533
		public uint height;

		// Token: 0x04000216 RID: 534
		public uint dispWidth;

		// Token: 0x04000217 RID: 535
		public uint dispHeight;

		// Token: 0x04000218 RID: 536
		public uint framerateN;

		// Token: 0x04000219 RID: 537
		public uint framerateD;

		// Token: 0x0400021A RID: 538
		public ulong time;

		// Token: 0x0400021B RID: 539
		public ulong tunit;

		// Token: 0x0400021C RID: 540
		public uint cntConcatenatedMovie;

		// Token: 0x0400021D RID: 541
		private AlphaType alphaType;

		// Token: 0x0400021E RID: 542
		public uint cntSkippedFrames;
	}
}
