﻿using System;

namespace CriMana
{
	// Token: 0x02000074 RID: 116
	public struct AudioInfo
	{
		// Token: 0x04000200 RID: 512
		public uint samplingRate;

		// Token: 0x04000201 RID: 513
		public uint numChannels;

		// Token: 0x04000202 RID: 514
		public uint totalSamples;
	}
}
