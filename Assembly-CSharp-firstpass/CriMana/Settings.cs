﻿using System;

namespace CriMana
{
	// Token: 0x02000078 RID: 120
	public static class Settings
	{
		// Token: 0x0600039D RID: 925 RVA: 0x0000882C File Offset: 0x00006C2C
		public static void SetDecodeSkippingEnabled(bool enabled)
		{
			CriManaPlugin.criManaUnity_SetDecodeSkipFlag(enabled);
		}
	}
}
