﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using AOT;
using CriMana.Detail;
using UnityEngine;

namespace CriMana
{
	// Token: 0x02000087 RID: 135
	public class Player : IDisposable
	{
		// Token: 0x0600042C RID: 1068 RVA: 0x0000A2E0 File Offset: 0x000086E0
		public Player()
		{
			if (!CriManaPlugin.isInitialized)
			{
				throw new Exception("CriManaPlugin is not initialized.");
			}
			this.playerId = Player.criManaUnityPlayer_Create();
			this.additiveMode = false;
			CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Mana);
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x0000A340 File Offset: 0x00008740
		public Player(bool advanced_audio_mode, bool ambisonics_mode)
		{
			if (!CriManaPlugin.isInitialized)
			{
				throw new Exception("CriFsPlugin is not initialized.");
			}
			if (advanced_audio_mode)
			{
				this.playerId = Player.criManaUnityPlayer_CreateWithAtomExPlayer();
				this._atomExPlayer = new CriAtomExPlayer(Player.criManaUnityPlayer_GetAtomExPlayerHn(this.playerId));
			}
			if (ambisonics_mode)
			{
				this._atomEx3Dsource = new CriAtomEx3dSource();
				this._atomExPlayer.Set3dSource(this._atomEx3Dsource);
				this._atomExPlayer.Set3dListener(CriAtomListener.sharedNativeListener);
				this._atomExPlayer.SetPanType(CriAtomEx.PanType.Pos3d);
				this._atomExPlayer.UpdateAll();
			}
			else
			{
				this.playerId = Player.criManaUnityPlayer_Create();
			}
			this.additiveMode = false;
			CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Mana);
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600042E RID: 1070 RVA: 0x0000A413 File Offset: 0x00008813
		// (set) Token: 0x0600042F RID: 1071 RVA: 0x0000A41B File Offset: 0x0000881B
		public bool additiveMode { get; set; }

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000430 RID: 1072 RVA: 0x0000A424 File Offset: 0x00008824
		public bool isFrameAvailable
		{
			get
			{
				return this.isFrameInfoAvailable;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000431 RID: 1073 RVA: 0x0000A42C File Offset: 0x0000882C
		public MovieInfo movieInfo
		{
			get
			{
				return (!this.isMovieInfoAvailable) ? null : this._movieInfo;
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000432 RID: 1074 RVA: 0x0000A445 File Offset: 0x00008845
		public FrameInfo frameInfo
		{
			get
			{
				return (!this.isFrameAvailable) ? null : this._frameInfo;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000433 RID: 1075 RVA: 0x0000A460 File Offset: 0x00008860
		public Player.Status status
		{
			get
			{
				if (this.requiredStatus == Player.Status.Stop && this.nativeStatus != Player.Status.Stop)
				{
					return Player.Status.StopProcessing;
				}
				if (this.nativeStatus == Player.Status.Ready)
				{
					return (this.rendererResource == null || !this.rendererResource.IsPrepared()) ? Player.Status.Prep : Player.Status.Ready;
				}
				return this.nativeStatus;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000434 RID: 1076 RVA: 0x0000A4BA File Offset: 0x000088BA
		public int numberOfEntries
		{
			get
			{
				return Player.criManaUnityPlayer_GetNumberOfEntry(this.playerId);
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000435 RID: 1077 RVA: 0x0000A4C7 File Offset: 0x000088C7
		// (set) Token: 0x06000436 RID: 1078 RVA: 0x0000A4CF File Offset: 0x000088CF
		public IntPtr subtitleBuffer { get; private set; }

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000437 RID: 1079 RVA: 0x0000A4D8 File Offset: 0x000088D8
		// (set) Token: 0x06000438 RID: 1080 RVA: 0x0000A4E0 File Offset: 0x000088E0
		public int subtitleSize { get; private set; }

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000439 RID: 1081 RVA: 0x0000A4E9 File Offset: 0x000088E9
		public CriAtomExPlayer atomExPlayer
		{
			get
			{
				return this._atomExPlayer;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600043A RID: 1082 RVA: 0x0000A4F1 File Offset: 0x000088F1
		public CriAtomEx3dSource atomEx3DsourceForAmbisonics
		{
			get
			{
				return this._atomEx3Dsource;
			}
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x0000A4FC File Offset: 0x000088FC
		~Player()
		{
			this.Dispose(false);
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x0000A52C File Offset: 0x0000892C
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x0000A53C File Offset: 0x0000893C
		public void CreateRendererResource(int width, int height, bool alpha)
		{
			MovieInfo movieInfo = new MovieInfo();
			movieInfo.hasAlpha = alpha;
			movieInfo.width = (uint)width;
			movieInfo.height = (uint)height;
			movieInfo.dispWidth = (uint)width;
			movieInfo.dispHeight = (uint)height;
			movieInfo.codecType = CodecType.SofdecPrime;
			movieInfo.alphaCodecType = CodecType.SofdecPrime;
			Shader userShader = (this._shaderDispatchCallback != null) ? this._shaderDispatchCallback(this.movieInfo, this.additiveMode) : null;
			if (this.rendererResource != null && !this.rendererResource.IsSuitable(this.playerId, movieInfo, this.additiveMode, userShader))
			{
				this.rendererResource.Dispose();
				this.rendererResource = null;
			}
			if (this.rendererResource == null)
			{
				this.rendererResource = RendererResourceFactory.DispatchAndCreate(this.playerId, movieInfo, this.additiveMode, userShader);
			}
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x0000A608 File Offset: 0x00008A08
		public void DisposeRendererResource()
		{
			if (this.rendererResource != null)
			{
				this.rendererResource.Dispose();
				this.rendererResource = null;
			}
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x0000A627 File Offset: 0x00008A27
		public void Prepare()
		{
			this.PrepareNativePlayer();
			this.UpdateNativePlayer();
			this.DisableInfos();
			this.requiredStatus = Player.Status.Ready;
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x0000A642 File Offset: 0x00008A42
		public void Start()
		{
			if (this.nativeStatus == Player.Status.Stop || this.nativeStatus == Player.Status.PlayEnd)
			{
				this.PrepareNativePlayer();
				this.DisableInfos();
				this.UpdateNativePlayer();
			}
			this.requiredStatus = Player.Status.Playing;
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x0000A674 File Offset: 0x00008A74
		public void Stop()
		{
			if (this.playerId != -1)
			{
				Player.criManaUnityPlayer_Stop(this.playerId);
				this.UpdateNativePlayer();
			}
			this.DisableInfos();
			this.requiredStatus = Player.Status.Stop;
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x0000A6A0 File Offset: 0x00008AA0
		public void Pause(bool sw)
		{
			Player.criManaUnityPlayer_Pause(this.playerId, (!sw) ? 0 : 1);
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x0000A6BA File Offset: 0x00008ABA
		public bool IsPaused()
		{
			return Player.criManaUnityPlayer_IsPaused(this.playerId);
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x0000A6C8 File Offset: 0x00008AC8
		public bool SetFile(CriFsBinder binder, string moviePath, Player.SetMode setMode = Player.SetMode.New)
		{
			IntPtr binder2 = (binder != null) ? binder.nativeHandle : IntPtr.Zero;
			if (binder == null && CriWare.IsStreamingAssetsPath(moviePath))
			{
				moviePath = Path.Combine(CriWare.streamingAssetsPath, moviePath);
			}
			if (setMode == Player.SetMode.New)
			{
				Player.criManaUnityPlayer_SetFile(this.playerId, binder2, moviePath);
				return true;
			}
			return Player.criManaUnityPlayer_EntryFile(this.playerId, binder2, moviePath, setMode == Player.SetMode.AppendRepeatedly);
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x0000A730 File Offset: 0x00008B30
		public bool SetData(byte[] data, long datasize, Player.SetMode setMode = Player.SetMode.New)
		{
			if (setMode == Player.SetMode.New)
			{
				Player.criManaUnityPlayer_SetData(this.playerId, data, datasize);
				return true;
			}
			return Player.criManaUnityPlayer_EntryData(this.playerId, data, datasize, setMode == Player.SetMode.AppendRepeatedly);
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x0000A758 File Offset: 0x00008B58
		public bool SetContentId(CriFsBinder binder, int contentId, Player.SetMode setMode = Player.SetMode.New)
		{
			if (binder == null)
			{
				Debug.LogError("[CRIWARE] CriFsBinder is null");
				return false;
			}
			if (setMode == Player.SetMode.New)
			{
				Player.criManaUnityPlayer_SetContentId(this.playerId, binder.nativeHandle, contentId);
				return true;
			}
			return Player.criManaUnityPlayer_EntryContentId(this.playerId, binder.nativeHandle, contentId, setMode == Player.SetMode.AppendRepeatedly);
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x0000A7A7 File Offset: 0x00008BA7
		public bool SetFileRange(string filePath, ulong offset, long range, Player.SetMode setMode = Player.SetMode.New)
		{
			if (setMode == Player.SetMode.New)
			{
				Player.criManaUnityPlayer_SetFileRange(this.playerId, filePath, offset, range);
				return true;
			}
			return Player.criManaUnityPlayer_EntryFileRange(this.playerId, filePath, offset, range, setMode == Player.SetMode.AppendRepeatedly);
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x0000A7D3 File Offset: 0x00008BD3
		public void Loop(bool sw)
		{
			Player.criManaUnityPlayer_Loop(this.playerId, (!sw) ? 0 : 1);
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x0000A7ED File Offset: 0x00008BED
		public void SetSeekPosition(int frameNumber)
		{
			Player.criManaUnityPlayer_SetSeekPosition(this.playerId, frameNumber);
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x0000A7FB File Offset: 0x00008BFB
		public void SetSpeed(float speed)
		{
			Player.criManaUnityPlayer_SetSpeed(this.playerId, speed);
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x0000A809 File Offset: 0x00008C09
		public void SetMaxPictureDataSize(uint maxDataSize)
		{
			Player.criManaUnityPlayer_SetMaxPictureDataSize(this.playerId, maxDataSize);
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x0000A817 File Offset: 0x00008C17
		public void SetBufferingTime(float sec)
		{
			Player.criManaUnityPlayer_SetBufferingTime(this.playerId, sec);
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x0000A825 File Offset: 0x00008C25
		public void SetMinBufferSize(int min_buffer_size)
		{
			Player.criManaUnityPlayer_SetMinBufferSize(this.playerId, min_buffer_size);
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x0000A833 File Offset: 0x00008C33
		public void SetAudioTrack(int track)
		{
			Player.criManaUnityPlayer_SetAudioTrack(this.playerId, track);
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x0000A841 File Offset: 0x00008C41
		public void SetAudioTrack(Player.AudioTrack track)
		{
			if (track == Player.AudioTrack.Off)
			{
				Player.criManaUnityPlayer_SetAudioTrack(this.playerId, -1);
			}
			else if (track == Player.AudioTrack.Auto)
			{
				Player.criManaUnityPlayer_SetAudioTrack(this.playerId, 100);
			}
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x0000A86E File Offset: 0x00008C6E
		public void SetSubAudioTrack(int track)
		{
			Player.criManaUnityPlayer_SetSubAudioTrack(this.playerId, track);
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x0000A87C File Offset: 0x00008C7C
		public void SetSubAudioTrack(Player.AudioTrack track)
		{
			if (track == Player.AudioTrack.Off)
			{
				Player.criManaUnityPlayer_SetSubAudioTrack(this.playerId, -1);
			}
			else if (track == Player.AudioTrack.Auto)
			{
				Player.criManaUnityPlayer_SetSubAudioTrack(this.playerId, 100);
			}
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x0000A8A9 File Offset: 0x00008CA9
		public void SetExtraAudioTrack(int track)
		{
			Player.criManaUnityPlayer_SetExtraAudioTrack(this.playerId, track);
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x0000A8B7 File Offset: 0x00008CB7
		public void SetExtraAudioTrack(Player.AudioTrack track)
		{
			if (track == Player.AudioTrack.Off)
			{
				Player.criManaUnityPlayer_SetExtraAudioTrack(this.playerId, -1);
			}
			else if (track == Player.AudioTrack.Auto)
			{
				Player.criManaUnityPlayer_SetExtraAudioTrack(this.playerId, 100);
			}
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x0000A8E4 File Offset: 0x00008CE4
		public void SetVolume(float volume)
		{
			Player.criManaUnityPlayer_SetVolume(this.playerId, volume);
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x0000A8F2 File Offset: 0x00008CF2
		public void SetSubAudioVolume(float volume)
		{
			Player.criManaUnityPlayer_SetSubAudioVolume(this.playerId, volume);
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x0000A900 File Offset: 0x00008D00
		public void SetExtraAudioVolume(float volume)
		{
			Player.criManaUnityPlayer_SetExtraAudioVolume(this.playerId, volume);
		}

		// Token: 0x06000457 RID: 1111 RVA: 0x0000A90E File Offset: 0x00008D0E
		public void SetBusSendLevel(string bus_name, float level)
		{
			Player.criManaUnityPlayer_SetBusSendLevelByName(this.playerId, bus_name, level);
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x0000A91D File Offset: 0x00008D1D
		public void SetSubAudioBusSendLevel(string bus_name, float volume)
		{
			Player.criManaUnityPlayer_SetSubAudioBusSendLevelByName(this.playerId, bus_name, volume);
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x0000A92C File Offset: 0x00008D2C
		public void SetExtraAudioBusSendLevel(string bus_name, float volume)
		{
			Player.criManaUnityPlayer_SetExtraAudioBusSendLevelByName(this.playerId, bus_name, volume);
		}

		// Token: 0x0600045A RID: 1114 RVA: 0x0000A93C File Offset: 0x00008D3C
		public void SetSubtitleChannel(int channel)
		{
			this.enableSubtitle = (channel != -1);
			if (this.enableSubtitle)
			{
				if (this.isMovieInfoAvailable)
				{
					this.AllocateSubtitleBuffer((int)this.movieInfo.maxSubtitleSize);
				}
			}
			else
			{
				this.DeallocateSubtitleBuffer();
			}
			Player.criManaUnityPlayer_SetSubtitleChannel(this.playerId, channel);
		}

		// Token: 0x0600045B RID: 1115 RVA: 0x0000A994 File Offset: 0x00008D94
		public void SetShaderDispatchCallback(Player.ShaderDispatchCallback shaderDispatchCallback)
		{
			this._shaderDispatchCallback = shaderDispatchCallback;
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x0000A99D File Offset: 0x00008D9D
		public long GetTime()
		{
			return Player.criManaUnityPlayer_GetTime(this.playerId);
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x0000A9AA File Offset: 0x00008DAA
		public void SetAsrRackId(int asrRackId)
		{
			Player.criManaUnityPlayer_SetAsrRackId(this.playerId, asrRackId);
		}

		// Token: 0x0600045E RID: 1118 RVA: 0x0000A9B8 File Offset: 0x00008DB8
		public void Update()
		{
			if (this.requiredStatus == Player.Status.Stop)
			{
				if (this.nativeStatus != Player.Status.Stop)
				{
					this.UpdateNativePlayer();
				}
				return;
			}
			switch (this.nativeStatus)
			{
			case Player.Status.Stop:
				goto IL_2EB;
			case Player.Status.Dechead:
				this.UpdateNativePlayer();
				if (this.nativeStatus != Player.Status.WaitPrep)
				{
					goto IL_2EB;
				}
				break;
			case Player.Status.WaitPrep:
				break;
			case Player.Status.Prep:
				goto IL_1FB;
			case Player.Status.Ready:
				goto IL_228;
			case Player.Status.Playing:
				goto IL_283;
			case Player.Status.PlayEnd:
				goto IL_2DB;
			case Player.Status.Error:
				this.UpdateNativePlayer();
				goto IL_2EB;
			default:
				goto IL_2EB;
			}
			bool flag = !this.isMovieInfoAvailable;
			if (flag)
			{
				Player.criManaUnityPlayer_GetMovieInfo(this.playerId, this._movieInfo);
				this.isMovieInfoAvailable = true;
				if (this.enableSubtitle)
				{
					this.AllocateSubtitleBuffer((int)this.movieInfo.maxSubtitleSize);
				}
				Shader userShader = (this._shaderDispatchCallback != null) ? this._shaderDispatchCallback(this.movieInfo, this.additiveMode) : null;
				if (this.rendererResource != null && !this.rendererResource.IsSuitable(this.playerId, this._movieInfo, this.additiveMode, userShader))
				{
					this.rendererResource.Dispose();
					this.rendererResource = null;
				}
				if (this.rendererResource == null)
				{
					this.rendererResource = RendererResourceFactory.DispatchAndCreate(this.playerId, this._movieInfo, this.additiveMode, userShader);
					if (this.rendererResource == null)
					{
						this.Stop();
						return;
					}
				}
			}
			if (!this.rendererResource.IsPrepared())
			{
				this.rendererResource.ContinuePreparing();
				if (!this.rendererResource.IsPrepared())
				{
					goto IL_2EB;
				}
			}
			this.rendererResource.AttachToPlayer(this.playerId);
			if (this.requiredStatus != Player.Status.Ready)
			{
				if (this.requiredStatus != Player.Status.Playing)
				{
					goto IL_2EB;
				}
				Player.criManaUnityPlayer_Start(this.playerId);
				this.isNativeStartInvoked = true;
				if (this.isNativeInitialized)
				{
					this.IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.DESTROY);
				}
				this.IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.INITIALIZE);
				this.isNativeInitialized = true;
			}
			IL_1FB:
			this.UpdateNativePlayer();
			if (this.nativeStatus != Player.Status.Ready)
			{
				if (this.nativeStatus == Player.Status.Playing)
				{
					goto IL_283;
				}
				goto IL_2EB;
			}
			IL_228:
			if (this.requiredStatus != Player.Status.Playing)
			{
				goto IL_2EB;
			}
			if (!this.isNativeStartInvoked)
			{
				Player.criManaUnityPlayer_Start(this.playerId);
				this.isNativeStartInvoked = true;
				if (this.isNativeInitialized)
				{
					this.IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.DESTROY);
				}
				this.IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.INITIALIZE);
				this.isNativeInitialized = true;
			}
			IL_283:
			this.UpdateNativePlayer();
			if (this.nativeStatus == Player.Status.Playing)
			{
				this.isFrameInfoAvailable |= this.rendererResource.UpdateFrame(this.playerId, this._frameInfo);
				this.IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.UPDATE);
			}
			else if (this.nativeStatus == Player.Status.PlayEnd)
			{
			}
			IL_2DB:
			IL_2EB:
			if (this.nativeStatus == Player.Status.Error)
			{
				this.DisableInfos();
			}
		}

		// Token: 0x0600045F RID: 1119 RVA: 0x0000ACC2 File Offset: 0x000090C2
		public void OnWillRenderObject(CriManaMovieMaterial sender)
		{
			if (this.status == Player.Status.Ready || this.status == Player.Status.Playing)
			{
				this.rendererResource.UpdateTextures();
				this.IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.RENDER);
			}
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x0000ACF2 File Offset: 0x000090F2
		public bool UpdateMaterial(Material material)
		{
			return this.rendererResource != null && this.isFrameInfoAvailable && this.rendererResource.UpdateMaterial(material);
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x0000AD18 File Offset: 0x00009118
		public void IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction renderEventAction)
		{
			int eventID = CriManaPlugin.renderingEventOffset | (int)renderEventAction | this.playerId;
			GL.IssuePluginEvent(Player.criWareUnity_GetRenderEventFunc(), eventID);
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x0000AD40 File Offset: 0x00009140
		private void Dispose(bool disposing)
		{
			CriDisposableObjectManager.Unregister(this);
			if (this.playerId != -1)
			{
				if (this.atomExPlayer != null)
				{
					this._atomExPlayer.Dispose();
					this._atomExPlayer = null;
				}
				if (this.atomEx3DsourceForAmbisonics != null)
				{
					this._atomEx3Dsource.Dispose();
					this._atomEx3Dsource = null;
				}
				Player.criManaUnityPlayer_Destroy(this.playerId);
				this.playerId = -1;
			}
			this.DisposeRendererResource();
			this.DeallocateSubtitleBuffer();
			this.cuePointCallback = null;
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x0000ADBF File Offset: 0x000091BF
		private void DisableInfos()
		{
			this.isMovieInfoAvailable = false;
			this.isFrameInfoAvailable = false;
			this.isNativeStartInvoked = false;
			this.subtitleSize = 0;
		}

		// Token: 0x06000464 RID: 1124 RVA: 0x0000ADDD File Offset: 0x000091DD
		private void PrepareNativePlayer()
		{
			if (this.cuePointCallback != null)
			{
				int player_id = this.playerId;
				if (Player.<>f__mg$cache0 == null)
				{
					Player.<>f__mg$cache0 = new Player.CuePointCallbackFromNativeDelegate(Player.CuePointCallbackFromNative);
				}
				Player.criManaUnityPlayer_SetCuePointCallback(player_id, Player.<>f__mg$cache0);
			}
			Player.criManaUnityPlayer_Prepare(this.playerId);
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x0000AE20 File Offset: 0x00009220
		private void UpdateNativePlayer()
		{
			Player.updatingPlayer = this;
			uint subtitleSize = (uint)this.subtitleBufferSize;
			this.nativeStatus = (Player.Status)Player.criManaUnityPlayer_Update(this.playerId, this.subtitleBuffer, ref subtitleSize);
			this.subtitleSize = (int)subtitleSize;
			Player.updatingPlayer = null;
			if (this.isNativeInitialized && (this.nativeStatus == Player.Status.StopProcessing || this.nativeStatus == Player.Status.Stop))
			{
				this.isNativeInitialized = false;
				this.IssuePluginEvent(Player.CriManaUnityPlayer_RenderEventAction.DESTROY);
			}
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x0000AE94 File Offset: 0x00009294
		private void AllocateSubtitleBuffer(int size)
		{
			if (this.subtitleBufferSize < size)
			{
				this.DeallocateSubtitleBuffer();
				this.subtitleBuffer = Marshal.AllocHGlobal(size);
				this.subtitleBufferSize = size;
				this.subtitleSize = 0;
			}
		}

		// Token: 0x06000467 RID: 1127 RVA: 0x0000AEC2 File Offset: 0x000092C2
		private void DeallocateSubtitleBuffer()
		{
			if (this.subtitleBuffer != IntPtr.Zero)
			{
				Marshal.FreeHGlobal(this.subtitleBuffer);
				this.subtitleBuffer = IntPtr.Zero;
				this.subtitleBufferSize = 0;
				this.subtitleSize = 0;
			}
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x0000AEFD File Offset: 0x000092FD
		[MonoPInvokeCallback(typeof(Player.CuePointCallbackFromNativeDelegate))]
		private static void CuePointCallbackFromNative(IntPtr ptr1, IntPtr ptr2, [In] ref EventPoint eventPoint)
		{
			if (Player.updatingPlayer.cuePointCallback != null)
			{
				Player.updatingPlayer.cuePointCallback(ref eventPoint);
			}
		}

		// Token: 0x06000469 RID: 1129
		[DllImport("cri_ware_unity")]
		private static extern int criManaUnityPlayer_Create();

		// Token: 0x0600046A RID: 1130
		[DllImport("cri_ware_unity")]
		private static extern int criManaUnityPlayer_CreateWithAtomExPlayer();

		// Token: 0x0600046B RID: 1131
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_Destroy(int player_id);

		// Token: 0x0600046C RID: 1132
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetFile(int player_id, IntPtr binder, string path);

		// Token: 0x0600046D RID: 1133
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetContentId(int player_id, IntPtr binder, int content_id);

		// Token: 0x0600046E RID: 1134
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetFileRange(int player_id, string path, ulong offset, long range);

		// Token: 0x0600046F RID: 1135
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetData(int player_id, byte[] data, long datasize);

		// Token: 0x06000470 RID: 1136
		[DllImport("cri_ware_unity")]
		private static extern bool criManaUnityPlayer_EntryFile(int player_id, IntPtr binder, string path, bool repeat);

		// Token: 0x06000471 RID: 1137
		[DllImport("cri_ware_unity")]
		private static extern bool criManaUnityPlayer_EntryContentId(int player_id, IntPtr binder, int content_id, bool repeat);

		// Token: 0x06000472 RID: 1138
		[DllImport("cri_ware_unity")]
		private static extern bool criManaUnityPlayer_EntryFileRange(int player_id, string path, ulong offset, long range, bool repeat);

		// Token: 0x06000473 RID: 1139
		[DllImport("cri_ware_unity")]
		private static extern bool criManaUnityPlayer_EntryData(int player_id, byte[] data, long datasize, bool repeat);

		// Token: 0x06000474 RID: 1140
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_ClearEntry(int player_id);

		// Token: 0x06000475 RID: 1141
		[DllImport("cri_ware_unity")]
		private static extern int criManaUnityPlayer_GetNumberOfEntry(int player_id);

		// Token: 0x06000476 RID: 1142
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetCuePointCallback(int player_id, Player.CuePointCallbackFromNativeDelegate cbfunc);

		// Token: 0x06000477 RID: 1143
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_GetMovieInfo(int player_id, [Out] MovieInfo movie_info);

		// Token: 0x06000478 RID: 1144
		[DllImport("cri_ware_unity")]
		private static extern int criManaUnityPlayer_Update(int player_id, IntPtr subtitle_buffer, ref uint subtitle_size);

		// Token: 0x06000479 RID: 1145
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_Prepare(int player_id);

		// Token: 0x0600047A RID: 1146
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_Start(int player_id);

		// Token: 0x0600047B RID: 1147
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_Stop(int player_id);

		// Token: 0x0600047C RID: 1148
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetSeekPosition(int player_id, int seek_frame_no);

		// Token: 0x0600047D RID: 1149
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_Pause(int player_id, int sw);

		// Token: 0x0600047E RID: 1150
		[DllImport("cri_ware_unity")]
		private static extern bool criManaUnityPlayer_IsPaused(int player_id);

		// Token: 0x0600047F RID: 1151
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_Loop(int player_id, int sw);

		// Token: 0x06000480 RID: 1152
		[DllImport("cri_ware_unity")]
		private static extern long criManaUnityPlayer_GetTime(int player_id);

		// Token: 0x06000481 RID: 1153
		[DllImport("cri_ware_unity")]
		private static extern int criManaUnityPlayer_GetStatus(int player_id);

		// Token: 0x06000482 RID: 1154
		[DllImport("cri_ware_unity")]
		private static extern IntPtr criManaUnityPlayer_GetAtomExPlayerHn(int player_id);

		// Token: 0x06000483 RID: 1155
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetAudioTrack(int player_id, int track);

		// Token: 0x06000484 RID: 1156
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetVolume(int player_id, float vol);

		// Token: 0x06000485 RID: 1157
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetSubAudioTrack(int player_id, int track);

		// Token: 0x06000486 RID: 1158
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetSubAudioVolume(int player_id, float vol);

		// Token: 0x06000487 RID: 1159
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetExtraAudioTrack(int player_id, int track);

		// Token: 0x06000488 RID: 1160
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetExtraAudioVolume(int player_id, float vol);

		// Token: 0x06000489 RID: 1161
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetBusSendLevelByName(int player_id, string bus_name, float level);

		// Token: 0x0600048A RID: 1162
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetSubAudioBusSendLevelByName(int player_id, string bus_name, float level);

		// Token: 0x0600048B RID: 1163
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetExtraAudioBusSendLevelByName(int player_id, string bus_name, float level);

		// Token: 0x0600048C RID: 1164
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetSubtitleChannel(int player_id, int channel);

		// Token: 0x0600048D RID: 1165
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetSpeed(int player_id, float speed);

		// Token: 0x0600048E RID: 1166
		[DllImport("cri_ware_unity")]
		private static extern void criManaUnityPlayer_SetMaxPictureDataSize(int player_id, uint max_data_size);

		// Token: 0x0600048F RID: 1167
		[DllImport("cri_ware_unity")]
		public static extern void criManaUnityPlayer_SetBufferingTime(int player_id, float sec);

		// Token: 0x06000490 RID: 1168
		[DllImport("cri_ware_unity")]
		public static extern void criManaUnityPlayer_SetMinBufferSize(int player_id, int min_buffer_size);

		// Token: 0x06000491 RID: 1169
		[DllImport("cri_ware_unity")]
		public static extern void criManaUnityPlayer_SetAsrRackId(int player_id, int asr_rack_id);

		// Token: 0x06000492 RID: 1170
		[DllImport("cri_ware_unity")]
		private static extern IntPtr criWareUnity_GetRenderEventFunc();

		// Token: 0x04000268 RID: 616
		private const int InvalidPlayerId = -1;

		// Token: 0x04000269 RID: 617
		private static Player updatingPlayer;

		// Token: 0x0400026A RID: 618
		private int playerId = -1;

		// Token: 0x0400026B RID: 619
		private Player.Status requiredStatus;

		// Token: 0x0400026C RID: 620
		private Player.Status nativeStatus;

		// Token: 0x0400026D RID: 621
		private bool isNativeStartInvoked;

		// Token: 0x0400026E RID: 622
		private bool isNativeInitialized;

		// Token: 0x0400026F RID: 623
		private RendererResource rendererResource;

		// Token: 0x04000270 RID: 624
		private MovieInfo _movieInfo = new MovieInfo();

		// Token: 0x04000271 RID: 625
		private FrameInfo _frameInfo = new FrameInfo();

		// Token: 0x04000272 RID: 626
		private bool isMovieInfoAvailable;

		// Token: 0x04000273 RID: 627
		private bool isFrameInfoAvailable;

		// Token: 0x04000274 RID: 628
		private Player.ShaderDispatchCallback _shaderDispatchCallback;

		// Token: 0x04000275 RID: 629
		private bool enableSubtitle;

		// Token: 0x04000276 RID: 630
		private int subtitleBufferSize;

		// Token: 0x04000277 RID: 631
		private CriAtomExPlayer _atomExPlayer;

		// Token: 0x04000278 RID: 632
		private CriAtomEx3dSource _atomEx3Dsource;

		// Token: 0x04000279 RID: 633
		public Player.CuePointCallback cuePointCallback;

		// Token: 0x0400027D RID: 637
		[CompilerGenerated]
		private static Player.CuePointCallbackFromNativeDelegate <>f__mg$cache0;

		// Token: 0x02000088 RID: 136
		public enum Status
		{
			// Token: 0x0400027F RID: 639
			Stop,
			// Token: 0x04000280 RID: 640
			Dechead,
			// Token: 0x04000281 RID: 641
			WaitPrep,
			// Token: 0x04000282 RID: 642
			Prep,
			// Token: 0x04000283 RID: 643
			Ready,
			// Token: 0x04000284 RID: 644
			Playing,
			// Token: 0x04000285 RID: 645
			PlayEnd,
			// Token: 0x04000286 RID: 646
			Error,
			// Token: 0x04000287 RID: 647
			StopProcessing
		}

		// Token: 0x02000089 RID: 137
		public enum SetMode
		{
			// Token: 0x04000289 RID: 649
			New,
			// Token: 0x0400028A RID: 650
			Append,
			// Token: 0x0400028B RID: 651
			AppendRepeatedly
		}

		// Token: 0x0200008A RID: 138
		public enum AudioTrack
		{
			// Token: 0x0400028D RID: 653
			Off,
			// Token: 0x0400028E RID: 654
			Auto
		}

		// Token: 0x0200008B RID: 139
		// (Invoke) Token: 0x06000495 RID: 1173
		public delegate void CuePointCallback(ref EventPoint eventPoint);

		// Token: 0x0200008C RID: 140
		// (Invoke) Token: 0x06000499 RID: 1177
		public delegate Shader ShaderDispatchCallback(MovieInfo movieInfo, bool additiveMode);

		// Token: 0x0200008D RID: 141
		// (Invoke) Token: 0x0600049D RID: 1181
		private delegate void CuePointCallbackFromNativeDelegate(IntPtr ptr1, IntPtr ptr2, [In] ref EventPoint eventPoint);

		// Token: 0x0200008E RID: 142
		public enum CriManaUnityPlayer_RenderEventAction
		{
			// Token: 0x04000290 RID: 656
			UPDATE,
			// Token: 0x04000291 RID: 657
			INITIALIZE = 256,
			// Token: 0x04000292 RID: 658
			RENDER = 512,
			// Token: 0x04000293 RID: 659
			DESTROY = 768
		}
	}
}
