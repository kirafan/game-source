﻿using System;
using System.Runtime.InteropServices;

// Token: 0x0200005A RID: 90
public class CriFsInstaller : IDisposable
{
	// Token: 0x060002DE RID: 734 RVA: 0x000071E8 File Offset: 0x000055E8
	public CriFsInstaller()
	{
		if (!CriFsPlugin.isInitialized)
		{
			throw new Exception("CriFsPlugin is not initialized.");
		}
		this.handle = IntPtr.Zero;
		CriFsInstaller.criFsInstaller_Create(out this.handle, CriFsInstaller.CopyPolicy.Always);
		if (this.handle == IntPtr.Zero)
		{
			throw new Exception("criFsInstaller_Create() failed.");
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Fs);
	}

	// Token: 0x060002DF RID: 735 RVA: 0x00007250 File Offset: 0x00005650
	public void Dispose()
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.handle != IntPtr.Zero)
		{
			CriFsInstaller.criFsInstaller_Destroy(this.handle);
			this.handle = IntPtr.Zero;
		}
		if (this.installBuffer != null)
		{
			this.installBufferGch.Free();
			this.installBuffer = null;
		}
		GC.SuppressFinalize(this);
	}

	// Token: 0x060002E0 RID: 736 RVA: 0x000072B4 File Offset: 0x000056B4
	public void Copy(CriFsBinder binder, string srcPath, string dstPath, int installBufferSize)
	{
		string text = srcPath;
		if (text.StartsWith("http:") || text.StartsWith("https:"))
		{
			text = "net2:" + text;
		}
		if (installBufferSize > 0)
		{
			this.installBuffer = new byte[installBufferSize];
			this.installBufferGch = GCHandle.Alloc(this.installBuffer, GCHandleType.Pinned);
			CriFsInstaller.criFsInstaller_Copy(this.handle, (binder == null) ? IntPtr.Zero : binder.nativeHandle, text, dstPath, this.installBufferGch.AddrOfPinnedObject(), (long)this.installBuffer.Length);
		}
		else
		{
			CriFsInstaller.criFsInstaller_Copy(this.handle, (binder == null) ? IntPtr.Zero : binder.nativeHandle, text, dstPath, IntPtr.Zero, 0L);
		}
	}

	// Token: 0x060002E1 RID: 737 RVA: 0x0000737E File Offset: 0x0000577E
	public void Stop()
	{
		if (this.handle != IntPtr.Zero)
		{
			CriFsInstaller.criFsInstaller_Stop(this.handle);
		}
	}

	// Token: 0x060002E2 RID: 738 RVA: 0x000073A4 File Offset: 0x000057A4
	public CriFsInstaller.Status GetStatus()
	{
		CriFsInstaller.Status result = CriFsInstaller.Status.Stop;
		if (this.handle != IntPtr.Zero)
		{
			CriFsInstaller.criFsInstaller_GetStatus(this.handle, out result);
		}
		return result;
	}

	// Token: 0x060002E3 RID: 739 RVA: 0x000073D8 File Offset: 0x000057D8
	public float GetProgress()
	{
		float result = 0f;
		if (this.handle != IntPtr.Zero)
		{
			CriFsInstaller.criFsInstaller_GetProgress(this.handle, out result);
		}
		return result;
	}

	// Token: 0x060002E4 RID: 740 RVA: 0x0000740F File Offset: 0x0000580F
	public static void ExecuteMain()
	{
		CriFsInstaller.criFsInstaller_ExecuteMain();
	}

	// Token: 0x060002E5 RID: 741 RVA: 0x00007418 File Offset: 0x00005818
	~CriFsInstaller()
	{
		this.Dispose();
	}

	// Token: 0x060002E6 RID: 742
	[DllImport("cri_ware_unity")]
	private static extern void criFsInstaller_ExecuteMain();

	// Token: 0x060002E7 RID: 743
	[DllImport("cri_ware_unity")]
	private static extern int criFsInstaller_Create(out IntPtr installer, CriFsInstaller.CopyPolicy option);

	// Token: 0x060002E8 RID: 744
	[DllImport("cri_ware_unity")]
	private static extern int criFsInstaller_Destroy(IntPtr installer);

	// Token: 0x060002E9 RID: 745
	[DllImport("cri_ware_unity")]
	private static extern int criFsInstaller_Copy(IntPtr installer, IntPtr binder, string src_path, string dst_path, IntPtr buffer, long buffer_size);

	// Token: 0x060002EA RID: 746
	[DllImport("cri_ware_unity")]
	private static extern int criFsInstaller_Stop(IntPtr installer);

	// Token: 0x060002EB RID: 747
	[DllImport("cri_ware_unity")]
	private static extern int criFsInstaller_GetStatus(IntPtr installer, out CriFsInstaller.Status status);

	// Token: 0x060002EC RID: 748
	[DllImport("cri_ware_unity")]
	private static extern int criFsInstaller_GetProgress(IntPtr installer, out float progress);

	// Token: 0x0400019A RID: 410
	private byte[] installBuffer;

	// Token: 0x0400019B RID: 411
	private GCHandle installBufferGch;

	// Token: 0x0400019C RID: 412
	private IntPtr handle;

	// Token: 0x0200005B RID: 91
	public enum Status
	{
		// Token: 0x0400019E RID: 414
		Stop,
		// Token: 0x0400019F RID: 415
		Busy,
		// Token: 0x040001A0 RID: 416
		Complete,
		// Token: 0x040001A1 RID: 417
		Error
	}

	// Token: 0x0200005C RID: 92
	private enum CopyPolicy
	{
		// Token: 0x040001A3 RID: 419
		Always
	}
}
