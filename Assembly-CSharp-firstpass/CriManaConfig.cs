﻿using System;

// Token: 0x0200009C RID: 156
[Serializable]
public class CriManaConfig
{
	// Token: 0x040002D7 RID: 727
	public int numberOfDecoders = 8;

	// Token: 0x040002D8 RID: 728
	public int numberOfMaxEntries = 4;

	// Token: 0x040002D9 RID: 729
	public readonly bool graphicsMultiThreaded = true;

	// Token: 0x040002DA RID: 730
	public CriManaConfig.VitaH264PlaybackConfig vitaH264PlaybackConfig = new CriManaConfig.VitaH264PlaybackConfig();

	// Token: 0x0200009D RID: 157
	[Serializable]
	public class VitaH264PlaybackConfig
	{
		// Token: 0x040002DB RID: 731
		public bool useH264Playback;

		// Token: 0x040002DC RID: 732
		public int maxWidth = 960;

		// Token: 0x040002DD RID: 733
		public int maxHeight = 544;

		// Token: 0x040002DE RID: 734
		public bool getMemoryFromTexture;
	}
}
