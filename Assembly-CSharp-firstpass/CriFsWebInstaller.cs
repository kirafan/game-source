﻿using System;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;

// Token: 0x0200006C RID: 108
public class CriFsWebInstaller : IDisposable
{
	// Token: 0x0600037F RID: 895 RVA: 0x00008554 File Offset: 0x00006954
	public CriFsWebInstaller()
	{
		CriFsWebInstaller.criFsWebInstaller_Create(out this.handle, IntPtr.Zero);
		if (this.handle == IntPtr.Zero)
		{
			throw new Exception("criFsWebInstaller_Create() failed.");
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.FsWeb);
	}

	// Token: 0x1700003D RID: 61
	// (get) Token: 0x06000380 RID: 896 RVA: 0x000085AA File Offset: 0x000069AA
	// (set) Token: 0x06000381 RID: 897 RVA: 0x000085B1 File Offset: 0x000069B1
	public static bool isInitialized { get; private set; }

	// Token: 0x1700003E RID: 62
	// (get) Token: 0x06000382 RID: 898 RVA: 0x000085BC File Offset: 0x000069BC
	public static CriFsWebInstaller.ModuleConfig defaultModuleConfig
	{
		get
		{
			CriFsWebInstaller.ModuleConfig result;
			result.numInstallers = 2U;
			result.proxyHost = null;
			result.proxyPort = 0;
			result.userAgent = null;
			result.inactiveTimeoutSec = 300U;
			result.allowInsecureSSL = false;
			result.platformConfig = CriFsWebInstaller.ModulePlatformConfig.defaultConfig;
			return result;
		}
	}

	// Token: 0x06000383 RID: 899 RVA: 0x0000860C File Offset: 0x00006A0C
	~CriFsWebInstaller()
	{
		this.Dispose(false);
	}

	// Token: 0x06000384 RID: 900 RVA: 0x0000863C File Offset: 0x00006A3C
	public void Dispose()
	{
		this.Dispose(true);
		GC.SuppressFinalize(this);
	}

	// Token: 0x06000385 RID: 901 RVA: 0x0000864B File Offset: 0x00006A4B
	public void Copy(string url, string dstPath)
	{
		CriFsWebInstaller.criFsWebInstaller_Copy(this.handle, url, dstPath);
	}

	// Token: 0x06000386 RID: 902 RVA: 0x0000865B File Offset: 0x00006A5B
	public void Stop()
	{
		if (this.handle != IntPtr.Zero)
		{
			CriFsWebInstaller.criFsWebInstaller_Stop(this.handle);
		}
	}

	// Token: 0x06000387 RID: 903 RVA: 0x00008680 File Offset: 0x00006A80
	public CriFsWebInstaller.StatusInfo GetStatusInfo()
	{
		CriFsWebInstaller.StatusInfo result;
		if (this.handle != IntPtr.Zero)
		{
			CriFsWebInstaller.criFsWebInstaller_GetStatusInfo(this.handle, out result);
		}
		else
		{
			result.status = CriFsWebInstaller.Status.Stop;
			result.error = CriFsWebInstaller.Error.Internal;
			result.httpStatusCode = -1;
			result.contentsSize = -1L;
			result.receivedSize = 0L;
		}
		return result;
	}

	// Token: 0x06000388 RID: 904 RVA: 0x000086E0 File Offset: 0x00006AE0
	public static void InitializeModule(CriFsWebInstaller.ModuleConfig config)
	{
		if (CriFsWebInstaller.isInitialized)
		{
			Debug.LogError("[CRIWARE] CriFsWebInstaller module is already initialized.");
			return;
		}
		CriFsPlugin.InitializeLibrary();
		CriFsWebInstaller.criFsWebInstaller_Initialize(ref config);
		CriFsWebInstaller.isInitialized = true;
	}

	// Token: 0x06000389 RID: 905 RVA: 0x00008709 File Offset: 0x00006B09
	public static void FinalizeModule()
	{
		if (!CriFsWebInstaller.isInitialized)
		{
			Debug.LogError("[CRIWARE] CriFsWebInstaller module is not initialized.");
			return;
		}
		CriDisposableObjectManager.CallOnModuleFinalization(CriDisposableObjectManager.ModuleType.FsWeb);
		CriFsWebInstaller.criFsWebInstaller_Finalize();
		CriFsPlugin.FinalizeLibrary();
		CriFsWebInstaller.isInitialized = false;
	}

	// Token: 0x0600038A RID: 906 RVA: 0x00008736 File Offset: 0x00006B36
	public static void ExecuteMain()
	{
		CriFsWebInstaller.criFsWebInstaller_ExecuteMain();
	}

	// Token: 0x0600038B RID: 907 RVA: 0x00008740 File Offset: 0x00006B40
	private void Dispose(bool disposing)
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.handle != IntPtr.Zero)
		{
			if (this.GetStatusInfo().status != CriFsWebInstaller.Status.Stop)
			{
				this.Stop();
				for (;;)
				{
					CriFsWebInstaller.ExecuteMain();
					if (this.GetStatusInfo().status == CriFsWebInstaller.Status.Stop)
					{
						break;
					}
					Thread.Sleep(1);
				}
			}
			CriFsWebInstaller.criFsWebInstaller_Destroy(this.handle);
			this.handle = IntPtr.Zero;
		}
	}

	// Token: 0x0600038C RID: 908
	[DllImport("cri_ware_unity")]
	private static extern void criFsWebInstaller_Initialize([In] ref CriFsWebInstaller.ModuleConfig config);

	// Token: 0x0600038D RID: 909
	[DllImport("cri_ware_unity")]
	private static extern void criFsWebInstaller_Finalize();

	// Token: 0x0600038E RID: 910
	[DllImport("cri_ware_unity")]
	private static extern void criFsWebInstaller_ExecuteMain();

	// Token: 0x0600038F RID: 911
	[DllImport("cri_ware_unity")]
	private static extern int criFsWebInstaller_Create(out IntPtr installer, IntPtr option);

	// Token: 0x06000390 RID: 912
	[DllImport("cri_ware_unity")]
	private static extern int criFsWebInstaller_Destroy(IntPtr installer);

	// Token: 0x06000391 RID: 913
	[DllImport("cri_ware_unity")]
	private static extern int criFsWebInstaller_Copy(IntPtr installer, string url, string dstPath);

	// Token: 0x06000392 RID: 914
	[DllImport("cri_ware_unity")]
	private static extern int criFsWebInstaller_Stop(IntPtr installer);

	// Token: 0x06000393 RID: 915
	[DllImport("cri_ware_unity")]
	private static extern int criFsWebInstaller_GetStatusInfo(IntPtr installer, out CriFsWebInstaller.StatusInfo status);

	// Token: 0x040001D8 RID: 472
	public const int InvalidHttpStatusCode = -1;

	// Token: 0x040001D9 RID: 473
	public const long InvalidContentsSize = -1L;

	// Token: 0x040001DA RID: 474
	private IntPtr handle = IntPtr.Zero;

	// Token: 0x0200006D RID: 109
	public enum Status
	{
		// Token: 0x040001DC RID: 476
		Stop,
		// Token: 0x040001DD RID: 477
		Busy,
		// Token: 0x040001DE RID: 478
		Complete,
		// Token: 0x040001DF RID: 479
		Error
	}

	// Token: 0x0200006E RID: 110
	public enum Error
	{
		// Token: 0x040001E1 RID: 481
		None,
		// Token: 0x040001E2 RID: 482
		Timeout,
		// Token: 0x040001E3 RID: 483
		Memory,
		// Token: 0x040001E4 RID: 484
		LocalFs,
		// Token: 0x040001E5 RID: 485
		DNS,
		// Token: 0x040001E6 RID: 486
		Connection,
		// Token: 0x040001E7 RID: 487
		SSL,
		// Token: 0x040001E8 RID: 488
		HTTP,
		// Token: 0x040001E9 RID: 489
		Internal
	}

	// Token: 0x0200006F RID: 111
	public struct StatusInfo
	{
		// Token: 0x040001EA RID: 490
		public CriFsWebInstaller.Status status;

		// Token: 0x040001EB RID: 491
		public CriFsWebInstaller.Error error;

		// Token: 0x040001EC RID: 492
		public int httpStatusCode;

		// Token: 0x040001ED RID: 493
		public long contentsSize;

		// Token: 0x040001EE RID: 494
		public long receivedSize;
	}

	// Token: 0x02000070 RID: 112
	public struct ModuleConfig
	{
		// Token: 0x040001EF RID: 495
		public uint numInstallers;

		// Token: 0x040001F0 RID: 496
		[MarshalAs(UnmanagedType.LPStr)]
		public string proxyHost;

		// Token: 0x040001F1 RID: 497
		public ushort proxyPort;

		// Token: 0x040001F2 RID: 498
		[MarshalAs(UnmanagedType.LPStr)]
		public string userAgent;

		// Token: 0x040001F3 RID: 499
		public uint inactiveTimeoutSec;

		// Token: 0x040001F4 RID: 500
		public bool allowInsecureSSL;

		// Token: 0x040001F5 RID: 501
		public CriFsWebInstaller.ModulePlatformConfig platformConfig;
	}

	// Token: 0x02000071 RID: 113
	public struct ModulePlatformConfig
	{
		// Token: 0x1700003F RID: 63
		// (get) Token: 0x06000394 RID: 916 RVA: 0x000087C4 File Offset: 0x00006BC4
		public static CriFsWebInstaller.ModulePlatformConfig defaultConfig
		{
			get
			{
				CriFsWebInstaller.ModulePlatformConfig result;
				result.reserved = 0;
				return result;
			}
		}

		// Token: 0x040001F6 RID: 502
		public byte reserved;
	}
}
