﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x02000091 RID: 145
[AddComponentMenu("CRIWARE/Error Handler")]
public class CriWareErrorHandler : MonoBehaviour
{
	// Token: 0x1700005B RID: 91
	// (get) Token: 0x060004B3 RID: 1203 RVA: 0x0000B02B File Offset: 0x0000942B
	// (set) Token: 0x060004B4 RID: 1204 RVA: 0x0000B032 File Offset: 0x00009432
	public static string errorMessage { get; set; }

	// Token: 0x060004B5 RID: 1205 RVA: 0x0000B03C File Offset: 0x0000943C
	private void Awake()
	{
		CriWareErrorHandler.initializationCount++;
		if (CriWareErrorHandler.initializationCount != 1)
		{
			UnityEngine.Object.Destroy(this);
			return;
		}
		CriWareErrorHandler.criWareUnity_Initialize();
		CriWareErrorHandler.criWareUnity_SetForceCrashFlagOnError(this.enableForceCrashOnError);
		CriWareErrorHandler.criWareUnity_ControlLogOutput(this.enableDebugPrintOnTerminal);
		if (this.dontDestroyOnLoad)
		{
			UnityEngine.Object.DontDestroyOnLoad(base.transform.gameObject);
		}
	}

	// Token: 0x060004B6 RID: 1206 RVA: 0x0000B09D File Offset: 0x0000949D
	private void OnEnable()
	{
	}

	// Token: 0x060004B7 RID: 1207 RVA: 0x0000B09F File Offset: 0x0000949F
	private void Start()
	{
	}

	// Token: 0x060004B8 RID: 1208 RVA: 0x0000B0A1 File Offset: 0x000094A1
	private void Update()
	{
		if (!this.enableDebugPrintOnTerminal)
		{
			CriWareErrorHandler.OutputErrorMessage();
		}
	}

	// Token: 0x060004B9 RID: 1209 RVA: 0x0000B0B3 File Offset: 0x000094B3
	private void OnDestroy()
	{
		CriWareErrorHandler.initializationCount--;
		if (CriWareErrorHandler.initializationCount != 0)
		{
			return;
		}
		CriWareErrorHandler.criWareUnity_Finalize();
	}

	// Token: 0x060004BA RID: 1210 RVA: 0x0000B0D4 File Offset: 0x000094D4
	private static void OutputErrorMessage()
	{
		IntPtr intPtr = CriWareErrorHandler.criWareUnity_GetFirstError();
		if (intPtr == IntPtr.Zero)
		{
			return;
		}
		string text = Marshal.PtrToStringAnsi(intPtr);
		if (text != string.Empty)
		{
			CriWareErrorHandler.OutputLog(text);
			CriWareErrorHandler.criWareUnity_ResetError();
		}
		if (CriWareErrorHandler.errorMessage == null)
		{
			CriWareErrorHandler.errorMessage = text.Substring(0);
		}
	}

	// Token: 0x060004BB RID: 1211 RVA: 0x0000B130 File Offset: 0x00009530
	private static void OutputLog(string errmsg)
	{
		if (errmsg == null)
		{
			return;
		}
		if (errmsg.StartsWith("E"))
		{
			Debug.LogError("[CRIWARE] Error:" + errmsg);
		}
		else if (errmsg.StartsWith("W"))
		{
			Debug.LogWarning("[CRIWARE] Warning:" + errmsg);
		}
		else
		{
			Debug.Log("[CRIWARE]" + errmsg);
		}
	}

	// Token: 0x060004BC RID: 1212
	[DllImport("cri_ware_unity")]
	private static extern void criWareUnity_Initialize();

	// Token: 0x060004BD RID: 1213
	[DllImport("cri_ware_unity")]
	private static extern void criWareUnity_Finalize();

	// Token: 0x060004BE RID: 1214
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criWareUnity_GetFirstError();

	// Token: 0x060004BF RID: 1215
	[DllImport("cri_ware_unity")]
	private static extern void criWareUnity_ResetError();

	// Token: 0x060004C0 RID: 1216
	[DllImport("cri_ware_unity")]
	private static extern void criWareUnity_ControlLogOutput(bool sw);

	// Token: 0x060004C1 RID: 1217
	[DllImport("cri_ware_unity")]
	private static extern void criWareUnity_SetForceCrashFlagOnError(bool sw);

	// Token: 0x0400029E RID: 670
	public bool enableDebugPrintOnTerminal;

	// Token: 0x0400029F RID: 671
	public bool enableForceCrashOnError;

	// Token: 0x040002A0 RID: 672
	public bool dontDestroyOnLoad = true;

	// Token: 0x040002A2 RID: 674
	private static int initializationCount;
}
