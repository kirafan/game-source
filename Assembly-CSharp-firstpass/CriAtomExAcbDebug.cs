﻿using System;
using System.Runtime.InteropServices;
using CriAtomDebugDetail;

// Token: 0x020000B0 RID: 176
public static class CriAtomExAcbDebug
{
	// Token: 0x06000502 RID: 1282 RVA: 0x0000BD80 File Offset: 0x0000A180
	public static bool GetAcbInfo(CriAtomExAcb acb, out CriAtomExAcbDebug.AcbInfo acbInfo)
	{
		CriAtomExAcbDebug.AcbInfoForMarshaling acbInfoForMarshaling;
		bool result = CriAtomExAcbDebug.criAtomExAcb_GetAcbInfo(acb.nativeHandle, out acbInfoForMarshaling) == 1;
		acbInfoForMarshaling.Convert(out acbInfo);
		return result;
	}

	// Token: 0x06000503 RID: 1283
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcb_GetAcbInfo(IntPtr acbHn, out CriAtomExAcbDebug.AcbInfoForMarshaling acbInfo);

	// Token: 0x020000B1 RID: 177
	public struct AcbInfo
	{
		// Token: 0x04000330 RID: 816
		public string name;

		// Token: 0x04000331 RID: 817
		public uint size;

		// Token: 0x04000332 RID: 818
		public uint version;

		// Token: 0x04000333 RID: 819
		public CriAtomEx.CharacterEncoding characterEncoding;

		// Token: 0x04000334 RID: 820
		public float volume;

		// Token: 0x04000335 RID: 821
		public int numCues;
	}

	// Token: 0x020000B2 RID: 178
	private struct AcbInfoForMarshaling
	{
		// Token: 0x06000504 RID: 1284 RVA: 0x0000BDA8 File Offset: 0x0000A1A8
		public void Convert(out CriAtomExAcbDebug.AcbInfo x)
		{
			x.name = Utility.PtrToStringAutoOrNull(this.namePtr);
			x.size = this.size;
			x.version = this.version;
			x.characterEncoding = this.characterEncoding;
			x.volume = this.volume;
			x.numCues = this.numCues;
		}

		// Token: 0x04000336 RID: 822
		public IntPtr namePtr;

		// Token: 0x04000337 RID: 823
		public uint size;

		// Token: 0x04000338 RID: 824
		public uint version;

		// Token: 0x04000339 RID: 825
		public CriAtomEx.CharacterEncoding characterEncoding;

		// Token: 0x0400033A RID: 826
		public float volume;

		// Token: 0x0400033B RID: 827
		public int numCues;
	}
}
