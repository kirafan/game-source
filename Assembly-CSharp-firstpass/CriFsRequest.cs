﻿using System;
using System.Collections;
using UnityEngine;

// Token: 0x0200005F RID: 95
public class CriFsRequest : IDisposable
{
	// Token: 0x1700002D RID: 45
	// (get) Token: 0x06000306 RID: 774 RVA: 0x00007728 File Offset: 0x00005B28
	// (set) Token: 0x06000307 RID: 775 RVA: 0x00007730 File Offset: 0x00005B30
	public CriFsRequest.DoneDelegate doneDelegate { get; protected set; }

	// Token: 0x1700002E RID: 46
	// (get) Token: 0x06000308 RID: 776 RVA: 0x00007739 File Offset: 0x00005B39
	// (set) Token: 0x06000309 RID: 777 RVA: 0x00007741 File Offset: 0x00005B41
	public bool isDone { get; private set; }

	// Token: 0x1700002F RID: 47
	// (get) Token: 0x0600030A RID: 778 RVA: 0x0000774A File Offset: 0x00005B4A
	// (set) Token: 0x0600030B RID: 779 RVA: 0x00007752 File Offset: 0x00005B52
	public string error { get; protected set; }

	// Token: 0x17000030 RID: 48
	// (get) Token: 0x0600030C RID: 780 RVA: 0x0000775B File Offset: 0x00005B5B
	// (set) Token: 0x0600030D RID: 781 RVA: 0x00007763 File Offset: 0x00005B63
	public bool isDisposed { get; protected set; }

	// Token: 0x0600030E RID: 782 RVA: 0x0000776C File Offset: 0x00005B6C
	public void Dispose()
	{
		if (!this.isDisposed)
		{
			this.Dispose(true);
			this.isDisposed = true;
			GC.SuppressFinalize(this);
		}
	}

	// Token: 0x0600030F RID: 783 RVA: 0x0000778D File Offset: 0x00005B8D
	public virtual void Stop()
	{
	}

	// Token: 0x06000310 RID: 784 RVA: 0x0000778F File Offset: 0x00005B8F
	public YieldInstruction WaitForDone(MonoBehaviour mb)
	{
		return mb.StartCoroutine(this.CheckDone());
	}

	// Token: 0x06000311 RID: 785 RVA: 0x0000779D File Offset: 0x00005B9D
	protected virtual void Dispose(bool disposing)
	{
	}

	// Token: 0x06000312 RID: 786 RVA: 0x0000779F File Offset: 0x00005B9F
	public virtual void Update()
	{
	}

	// Token: 0x06000313 RID: 787 RVA: 0x000077A1 File Offset: 0x00005BA1
	protected void Done()
	{
		this.isDone = true;
		if (this.doneDelegate != null)
		{
			this.doneDelegate(this);
		}
	}

	// Token: 0x06000314 RID: 788 RVA: 0x000077C4 File Offset: 0x00005BC4
	private IEnumerator CheckDone()
	{
		while (!this.isDone)
		{
			yield return null;
		}
		yield break;
	}

	// Token: 0x06000315 RID: 789 RVA: 0x000077E0 File Offset: 0x00005BE0
	~CriFsRequest()
	{
		if (!this.isDisposed)
		{
			this.Dispose(false);
			this.isDisposed = true;
		}
	}

	// Token: 0x02000060 RID: 96
	// (Invoke) Token: 0x06000317 RID: 791
	public delegate void DoneDelegate(CriFsRequest request);
}
