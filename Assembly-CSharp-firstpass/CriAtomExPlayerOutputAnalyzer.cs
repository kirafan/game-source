﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x02000047 RID: 71
public class CriAtomExPlayerOutputAnalyzer : IDisposable
{
	// Token: 0x06000296 RID: 662 RVA: 0x00006770 File Offset: 0x00004B70
	public CriAtomExPlayerOutputAnalyzer(CriAtomExPlayerOutputAnalyzer.Type[] types, CriAtomExPlayerOutputAnalyzer.Config[] configs = null)
	{
		this.handle = CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_Create(types.Length, types, configs);
		if (this.handle == IntPtr.Zero)
		{
			throw new Exception("criAtomExPlayerOutputAnalyzer_Create() failed.");
		}
		if (configs != null)
		{
			this.numBands = configs[0].num_spectrum_analyzer_bands;
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x17000025 RID: 37
	// (get) Token: 0x06000297 RID: 663 RVA: 0x000067E4 File Offset: 0x00004BE4
	public IntPtr nativeHandle
	{
		get
		{
			return this.handle;
		}
	}

	// Token: 0x06000298 RID: 664 RVA: 0x000067EC File Offset: 0x00004BEC
	public void Dispose()
	{
		this.Dispose(true);
	}

	// Token: 0x06000299 RID: 665 RVA: 0x000067F8 File Offset: 0x00004BF8
	private void Dispose(bool disposing)
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.handle != IntPtr.Zero)
		{
			this.DetachExPlayer();
			CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_Destroy(this.handle);
			this.handle = IntPtr.Zero;
		}
		if (disposing)
		{
			GC.SuppressFinalize(this);
		}
	}

	// Token: 0x0600029A RID: 666 RVA: 0x0000684C File Offset: 0x00004C4C
	public bool AttachExPlayer(CriAtomExPlayer player)
	{
		if (player == null || this.handle == IntPtr.Zero)
		{
			return false;
		}
		this.DetachExPlayer();
		CriAtomExPlayer.Status status = player.GetStatus();
		if (status != CriAtomExPlayer.Status.Stop)
		{
			return false;
		}
		CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_AttachExPlayer(this.handle, player.nativeHandle);
		this.player = player;
		return true;
	}

	// Token: 0x0600029B RID: 667 RVA: 0x000068A4 File Offset: 0x00004CA4
	public void DetachExPlayer()
	{
		if (this.player == null || this.handle == IntPtr.Zero)
		{
			return;
		}
		CriAtomExPlayer.Status status = this.player.GetStatus();
		if (status != CriAtomExPlayer.Status.Stop)
		{
			Debug.LogWarning("[CRIWARE] Warning: CriAtomExPlayer is forced to stop for detaching CriAtomExPlayerOutputAnalyzer.");
			this.player.StopWithoutReleaseTime();
		}
		CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_DetachExPlayer(this.handle, this.player.nativeHandle);
		this.player = null;
	}

	// Token: 0x0600029C RID: 668 RVA: 0x00006918 File Offset: 0x00004D18
	public float GetRms(int channel)
	{
		if (this.player == null || this.handle == IntPtr.Zero)
		{
			return 0f;
		}
		if (this.player.GetStatus() != CriAtomExPlayer.Status.Playing && this.player.GetStatus() != CriAtomExPlayer.Status.Prep)
		{
			return 0f;
		}
		return CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_GetRms(this.handle, channel);
	}

	// Token: 0x0600029D RID: 669 RVA: 0x00006980 File Offset: 0x00004D80
	public void GetSpectrumLevels(ref float[] levels)
	{
		if (this.player == null || this.handle == IntPtr.Zero)
		{
			return;
		}
		if (levels == null || levels.Length < this.numBands)
		{
			levels = new float[this.numBands];
		}
		IntPtr source = CriAtomExPlayerOutputAnalyzer.criAtomExPlayerOutputAnalyzer_GetSpectrumLevels(this.handle);
		Marshal.Copy(source, levels, 0, this.numBands);
	}

	// Token: 0x0600029E RID: 670 RVA: 0x000069EC File Offset: 0x00004DEC
	~CriAtomExPlayerOutputAnalyzer()
	{
		this.Dispose(false);
	}

	// Token: 0x0600029F RID: 671
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExPlayerOutputAnalyzer_Create(int numTypes, CriAtomExPlayerOutputAnalyzer.Type[] types, CriAtomExPlayerOutputAnalyzer.Config[] configs);

	// Token: 0x060002A0 RID: 672
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExPlayerOutputAnalyzer_Destroy(IntPtr analyzer);

	// Token: 0x060002A1 RID: 673
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayerOutputAnalyzer_AttachExPlayer(IntPtr analyzer, IntPtr player);

	// Token: 0x060002A2 RID: 674
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayerOutputAnalyzer_DetachExPlayer(IntPtr analyzer, IntPtr player);

	// Token: 0x060002A3 RID: 675
	[DllImport("cri_ware_unity")]
	private static extern float criAtomExPlayerOutputAnalyzer_GetRms(IntPtr analyzer, int channel);

	// Token: 0x060002A4 RID: 676
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExPlayerOutputAnalyzer_GetSpectrumLevels(IntPtr analyzer);

	// Token: 0x0400015C RID: 348
	public const int MaximumSpectrumBands = 512;

	// Token: 0x0400015D RID: 349
	private IntPtr handle = IntPtr.Zero;

	// Token: 0x0400015E RID: 350
	private CriAtomExPlayer player;

	// Token: 0x0400015F RID: 351
	private int numBands = 8;

	// Token: 0x02000048 RID: 72
	public enum Type
	{
		// Token: 0x04000161 RID: 353
		LevelMeter,
		// Token: 0x04000162 RID: 354
		SpectrumAnalyzer
	}

	// Token: 0x02000049 RID: 73
	public struct Config
	{
		// Token: 0x060002A5 RID: 677 RVA: 0x00006A1C File Offset: 0x00004E1C
		public Config(int num_spectrum_analyzer_bands = 8)
		{
			this.num_spectrum_analyzer_bands = num_spectrum_analyzer_bands;
		}

		// Token: 0x04000163 RID: 355
		public int num_spectrum_analyzer_bands;
	}
}
