﻿using System;

// Token: 0x02000061 RID: 97
public class CriFsLoadFileRequest : CriFsRequest
{
	// Token: 0x0600031A RID: 794 RVA: 0x000078BC File Offset: 0x00005CBC
	public CriFsLoadFileRequest(CriFsBinder srcBinder, string path, CriFsRequest.DoneDelegate doneDelegate, int readUnitSize)
	{
		this.path = path;
		base.doneDelegate = doneDelegate;
		this.readUnitSize = readUnitSize;
		if (srcBinder == null)
		{
			this.newBinder = new CriFsBinder();
			this.refBinder = this.newBinder;
			this.bindId = this.newBinder.BindFile(srcBinder, path);
			this.phase = CriFsLoadFileRequest.Phase.Bind;
		}
		else
		{
			this.newBinder = null;
			this.refBinder = srcBinder;
			this.fileSize = srcBinder.GetFileSize(path);
			if (this.fileSize < 0L)
			{
				this.phase = CriFsLoadFileRequest.Phase.Error;
			}
			else
			{
				this.phase = CriFsLoadFileRequest.Phase.Load;
			}
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Fs);
	}

	// Token: 0x17000031 RID: 49
	// (get) Token: 0x0600031B RID: 795 RVA: 0x00007963 File Offset: 0x00005D63
	// (set) Token: 0x0600031C RID: 796 RVA: 0x0000796B File Offset: 0x00005D6B
	public string path { get; private set; }

	// Token: 0x17000032 RID: 50
	// (get) Token: 0x0600031D RID: 797 RVA: 0x00007974 File Offset: 0x00005D74
	// (set) Token: 0x0600031E RID: 798 RVA: 0x0000797C File Offset: 0x00005D7C
	public byte[] bytes { get; private set; }

	// Token: 0x0600031F RID: 799 RVA: 0x00007988 File Offset: 0x00005D88
	protected override void Dispose(bool disposing)
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.loader != null)
		{
			this.loader.Dispose();
			this.loader = null;
		}
		if (this.newBinder != null)
		{
			this.newBinder.Dispose();
			this.newBinder = null;
		}
		this.bytes = null;
	}

	// Token: 0x06000320 RID: 800 RVA: 0x000079DD File Offset: 0x00005DDD
	public override void Stop()
	{
		if (this.phase == CriFsLoadFileRequest.Phase.Load && this.loader != null)
		{
			this.loader.Stop();
		}
	}

	// Token: 0x06000321 RID: 801 RVA: 0x00007A01 File Offset: 0x00005E01
	public override void Update()
	{
		if (this.phase == CriFsLoadFileRequest.Phase.Bind)
		{
			this.UpdateBinder();
		}
		if (this.phase == CriFsLoadFileRequest.Phase.Load)
		{
			this.UpdateLoader();
		}
		if (this.phase == CriFsLoadFileRequest.Phase.Error)
		{
			this.OnError();
		}
	}

	// Token: 0x06000322 RID: 802 RVA: 0x00007A3C File Offset: 0x00005E3C
	private void UpdateBinder()
	{
		CriFsBinder.Status status = CriFsBinder.GetStatus(this.bindId);
		if (status == CriFsBinder.Status.Analyze)
		{
			return;
		}
		if (status != CriFsBinder.Status.Complete)
		{
			this.fileSize = -1L;
		}
		else
		{
			this.fileSize = this.refBinder.GetFileSize(this.path);
		}
		if (this.fileSize < 0L)
		{
			this.phase = CriFsLoadFileRequest.Phase.Error;
			return;
		}
		this.phase = CriFsLoadFileRequest.Phase.Load;
	}

	// Token: 0x06000323 RID: 803 RVA: 0x00007AB0 File Offset: 0x00005EB0
	private void UpdateLoader()
	{
		if (this.loader == null)
		{
			this.loader = new CriFsLoader();
			this.loader.SetReadUnitSize(this.readUnitSize);
			this.bytes = new byte[this.fileSize];
			this.loader.Load(this.refBinder, this.path, 0L, this.fileSize, this.bytes);
		}
		CriFsLoader.Status status = this.loader.GetStatus();
		if (status == CriFsLoader.Status.Loading)
		{
			return;
		}
		if (status != CriFsLoader.Status.Stop)
		{
			if (status == CriFsLoader.Status.Error)
			{
				this.phase = CriFsLoadFileRequest.Phase.Error;
				return;
			}
		}
		else
		{
			this.bytes = null;
		}
		this.phase = CriFsLoadFileRequest.Phase.Done;
		this.loader.Dispose();
		this.loader = null;
		if (this.newBinder != null)
		{
			this.newBinder.Dispose();
			this.newBinder = null;
		}
		base.Done();
	}

	// Token: 0x06000324 RID: 804 RVA: 0x00007B98 File Offset: 0x00005F98
	private void OnError()
	{
		this.bytes = null;
		base.error = "Error occurred.";
		this.refBinder = null;
		if (this.newBinder != null)
		{
			this.newBinder.Dispose();
			this.newBinder = null;
		}
		if (this.loader != null)
		{
			this.loader.Dispose();
			this.loader = null;
		}
		this.phase = CriFsLoadFileRequest.Phase.Done;
		base.Done();
	}

	// Token: 0x040001B3 RID: 435
	private CriFsLoadFileRequest.Phase phase;

	// Token: 0x040001B4 RID: 436
	private CriFsBinder refBinder;

	// Token: 0x040001B5 RID: 437
	private CriFsBinder newBinder;

	// Token: 0x040001B6 RID: 438
	private uint bindId;

	// Token: 0x040001B7 RID: 439
	private CriFsLoader loader;

	// Token: 0x040001B8 RID: 440
	private int readUnitSize;

	// Token: 0x040001B9 RID: 441
	private long fileSize;

	// Token: 0x02000062 RID: 98
	private enum Phase
	{
		// Token: 0x040001BB RID: 443
		Stop,
		// Token: 0x040001BC RID: 444
		Bind,
		// Token: 0x040001BD RID: 445
		Load,
		// Token: 0x040001BE RID: 446
		Done,
		// Token: 0x040001BF RID: 447
		Error
	}
}
