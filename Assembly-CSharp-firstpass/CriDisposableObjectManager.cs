﻿using System;
using System.Collections.Generic;

// Token: 0x020000B5 RID: 181
public static class CriDisposableObjectManager
{
	// Token: 0x06000511 RID: 1297 RVA: 0x0000BE87 File Offset: 0x0000A287
	public static void Register(IDisposable disposable, CriDisposableObjectManager.ModuleType type)
	{
		if (CriDisposableObjectManager.disposables.Contains(disposable))
		{
			return;
		}
		CriDisposableObjectManager.disposables.Add(disposable);
		CriDisposableObjectManager.types.Add(type);
	}

	// Token: 0x06000512 RID: 1298 RVA: 0x0000BEB0 File Offset: 0x0000A2B0
	public static bool Unregister(IDisposable disposable)
	{
		int num = CriDisposableObjectManager.disposables.IndexOf(disposable);
		bool flag = num >= 0;
		if (flag)
		{
			CriDisposableObjectManager.disposables.RemoveAt(num);
			CriDisposableObjectManager.types.RemoveAt(num);
		}
		return flag;
	}

	// Token: 0x06000513 RID: 1299 RVA: 0x0000BEEE File Offset: 0x0000A2EE
	public static bool IsDisposed(IDisposable disposable)
	{
		return !CriDisposableObjectManager.disposables.Contains(disposable);
	}

	// Token: 0x06000514 RID: 1300 RVA: 0x0000BEFE File Offset: 0x0000A2FE
	public static void CallOnModuleFinalization(CriDisposableObjectManager.ModuleType type)
	{
		CriDisposableObjectManager.DisposeAll(type);
	}

	// Token: 0x06000515 RID: 1301 RVA: 0x0000BF08 File Offset: 0x0000A308
	private static int GetLastIndex(CriDisposableObjectManager.ModuleType type)
	{
		int count = CriDisposableObjectManager.disposables.Count;
		for (int i = count - 1; i >= 0; i--)
		{
			CriDisposableObjectManager.ModuleType moduleType = CriDisposableObjectManager.types[i];
			if (moduleType == type)
			{
				return i;
			}
		}
		return -1;
	}

	// Token: 0x06000516 RID: 1302 RVA: 0x0000BF4C File Offset: 0x0000A34C
	public static void DisposeAll(CriDisposableObjectManager.ModuleType type)
	{
		for (;;)
		{
			int lastIndex = CriDisposableObjectManager.GetLastIndex(type);
			if (lastIndex < 0)
			{
				break;
			}
			IDisposable disposable = CriDisposableObjectManager.disposables[lastIndex];
			disposable.Dispose();
		}
	}

	// Token: 0x0400033C RID: 828
	private static List<IDisposable> disposables = new List<IDisposable>();

	// Token: 0x0400033D RID: 829
	private static List<CriDisposableObjectManager.ModuleType> types = new List<CriDisposableObjectManager.ModuleType>();

	// Token: 0x020000B6 RID: 182
	public enum ModuleType
	{
		// Token: 0x0400033F RID: 831
		Atom,
		// Token: 0x04000340 RID: 832
		Fs,
		// Token: 0x04000341 RID: 833
		FsWeb,
		// Token: 0x04000342 RID: 834
		Mana
	}
}
