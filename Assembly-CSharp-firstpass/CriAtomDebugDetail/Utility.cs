﻿using System;
using System.Runtime.InteropServices;

namespace CriAtomDebugDetail
{
	// Token: 0x020000B4 RID: 180
	public class Utility
	{
		// Token: 0x06000510 RID: 1296 RVA: 0x0000BE69 File Offset: 0x0000A269
		public static string PtrToStringAutoOrNull(IntPtr stringPtr)
		{
			return (!(stringPtr == IntPtr.Zero)) ? Marshal.PtrToStringAuto(stringPtr) : null;
		}
	}
}
