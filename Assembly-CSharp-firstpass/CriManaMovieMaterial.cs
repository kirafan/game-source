﻿using System;
using CriMana;
using UnityEngine;

// Token: 0x0200007D RID: 125
[AddComponentMenu("CRIWARE/CriManaMovieMaterial")]
public class CriManaMovieMaterial : MonoBehaviour
{
	// Token: 0x17000045 RID: 69
	// (get) Token: 0x060003C4 RID: 964 RVA: 0x00008D95 File Offset: 0x00007195
	// (set) Token: 0x060003C5 RID: 965 RVA: 0x00008D9D File Offset: 0x0000719D
	public string moviePath
	{
		get
		{
			return this._moviePath;
		}
		set
		{
			if (this.isMonoBehaviourStartCalled)
			{
				Debug.LogError("[CRIWARE] moviePath can not be changed. Use CriMana::Player::SetFile method.");
			}
			else
			{
				this._moviePath = value;
			}
		}
	}

	// Token: 0x17000046 RID: 70
	// (get) Token: 0x060003C6 RID: 966 RVA: 0x00008DC0 File Offset: 0x000071C0
	// (set) Token: 0x060003C7 RID: 967 RVA: 0x00008DC8 File Offset: 0x000071C8
	public bool loop
	{
		get
		{
			return this._loop;
		}
		set
		{
			if (this.isMonoBehaviourStartCalled)
			{
				Debug.LogError("[CRIWARE] loop property can not be changed. Use CriMana::Player::Loop method.");
			}
			else
			{
				this._loop = value;
			}
		}
	}

	// Token: 0x17000047 RID: 71
	// (get) Token: 0x060003C8 RID: 968 RVA: 0x00008DEB File Offset: 0x000071EB
	// (set) Token: 0x060003C9 RID: 969 RVA: 0x00008DF3 File Offset: 0x000071F3
	public bool advancedAudio
	{
		get
		{
			return this._advancedAudio;
		}
		set
		{
			if (this.isMonoBehaviourStartCalled)
			{
				Debug.LogError("[CRIWARE] advancedAudio property can not be changed in running.");
			}
			else
			{
				if (!value)
				{
					this.ambisonics = false;
				}
				this._advancedAudio = value;
			}
		}
	}

	// Token: 0x17000048 RID: 72
	// (get) Token: 0x060003CA RID: 970 RVA: 0x00008E23 File Offset: 0x00007223
	// (set) Token: 0x060003CB RID: 971 RVA: 0x00008E2C File Offset: 0x0000722C
	public bool ambisonics
	{
		get
		{
			return this._ambisonics;
		}
		set
		{
			if (this.isMonoBehaviourStartCalled)
			{
				Debug.LogError("[CRIWARE] ambisonics property can not be changed in running.");
			}
			else if (!this._advancedAudio)
			{
				Debug.LogError("[CRIWARE] ambisonics property needs for advancedAudio property to be true.");
			}
			else
			{
				if (!value)
				{
					if (base.gameObject.transform.childCount > 0)
					{
						GameObject gameObject = (!(this.ambisonicSource != null)) ? base.gameObject.transform.Find("Ambisonic Source").gameObject : this.ambisonicSource;
						if (gameObject != null)
						{
							UnityEngine.Object.DestroyImmediate(gameObject);
						}
					}
				}
				else if (this.ambisonicSource == null)
				{
					this.ambisonicSource = new GameObject();
					this.ambisonicSource.name = "Ambisonic Source";
					this.ambisonicSource.transform.parent = base.gameObject.transform;
					this.ambisonicSource.transform.position = base.gameObject.transform.position;
					this.ambisonicSource.transform.rotation = base.gameObject.transform.rotation;
					this.ambisonicSource.transform.localScale = base.gameObject.transform.localScale;
					this.ambisonicSource.AddComponent<CriManaAmbisonicSource>();
				}
				this._ambisonics = value;
			}
		}
	}

	// Token: 0x17000049 RID: 73
	// (get) Token: 0x060003CC RID: 972 RVA: 0x00008F95 File Offset: 0x00007395
	// (set) Token: 0x060003CD RID: 973 RVA: 0x00008F9D File Offset: 0x0000739D
	public bool additiveMode
	{
		get
		{
			return this._additiveMode;
		}
		set
		{
			if (this.isMonoBehaviourStartCalled)
			{
				Debug.LogError("[CRIWARE] additiveMode can not be changed. Use CriMana::Player::additiveMode method.");
			}
			else
			{
				this._additiveMode = value;
			}
		}
	}

	// Token: 0x1700004A RID: 74
	// (get) Token: 0x060003CE RID: 974 RVA: 0x00008FC0 File Offset: 0x000073C0
	// (set) Token: 0x060003CF RID: 975 RVA: 0x00008FC8 File Offset: 0x000073C8
	public bool isMaterialAvailable { get; private set; }

	// Token: 0x1700004B RID: 75
	// (get) Token: 0x060003D0 RID: 976 RVA: 0x00008FD1 File Offset: 0x000073D1
	// (set) Token: 0x060003D1 RID: 977 RVA: 0x00008FD9 File Offset: 0x000073D9
	public Player player { get; private set; }

	// Token: 0x1700004C RID: 76
	// (get) Token: 0x060003D2 RID: 978 RVA: 0x00008FE2 File Offset: 0x000073E2
	// (set) Token: 0x060003D3 RID: 979 RVA: 0x00008FEA File Offset: 0x000073EA
	public Material material
	{
		get
		{
			return this._material;
		}
		set
		{
			if (value != this._material)
			{
				if (this.materialOwn)
				{
					UnityEngine.Object.Destroy(this._material);
					this.materialOwn = false;
				}
				this._material = value;
				this.isMaterialAvailable = false;
			}
		}
	}

	// Token: 0x1700004D RID: 77
	// (get) Token: 0x060003D4 RID: 980 RVA: 0x00009028 File Offset: 0x00007428
	// (set) Token: 0x060003D5 RID: 981 RVA: 0x00009030 File Offset: 0x00007430
	private protected bool HaveRendererOwner { protected get; private set; }

	// Token: 0x060003D6 RID: 982 RVA: 0x00009039 File Offset: 0x00007439
	public void Play()
	{
		this.player.Start();
	}

	// Token: 0x060003D7 RID: 983 RVA: 0x00009046 File Offset: 0x00007446
	public void Stop()
	{
		this.player.Stop();
		if (this.isMaterialAvailable)
		{
			this.isMaterialAvailable = false;
			this.OnMaterialAvailableChanged();
		}
	}

	// Token: 0x060003D8 RID: 984 RVA: 0x0000906B File Offset: 0x0000746B
	public void Pause(bool sw)
	{
		this.player.Pause(sw);
	}

	// Token: 0x060003D9 RID: 985 RVA: 0x00009079 File Offset: 0x00007479
	protected virtual void OnMaterialAvailableChanged()
	{
	}

	// Token: 0x060003DA RID: 986 RVA: 0x0000907B File Offset: 0x0000747B
	protected virtual void OnMaterialUpdated()
	{
	}

	// Token: 0x060003DB RID: 987 RVA: 0x0000907D File Offset: 0x0000747D
	protected virtual void Awake()
	{
		this.player = new Player(this._advancedAudio, this._ambisonics);
		this.isMaterialAvailable = false;
	}

	// Token: 0x060003DC RID: 988 RVA: 0x0000909D File Offset: 0x0000749D
	protected virtual void OnEnable()
	{
	}

	// Token: 0x060003DD RID: 989 RVA: 0x0000909F File Offset: 0x0000749F
	protected virtual void OnDisable()
	{
	}

	// Token: 0x060003DE RID: 990 RVA: 0x000090A1 File Offset: 0x000074A1
	protected virtual void OnDestroy()
	{
		this.player.Dispose();
		this.player = null;
		this.material = null;
	}

	// Token: 0x060003DF RID: 991 RVA: 0x000090BC File Offset: 0x000074BC
	protected virtual void Start()
	{
		this.HaveRendererOwner = (base.GetComponent<Renderer>() != null);
		if (this._material == null)
		{
			this.CreateMaterial();
		}
		if (!string.IsNullOrEmpty(this.moviePath))
		{
			this.player.SetFile(null, this.moviePath, Player.SetMode.New);
		}
		this.player.Loop(this.loop);
		this.player.additiveMode = this.additiveMode;
		if (this.playOnStart)
		{
			this.player.Start();
		}
		this.isMonoBehaviourStartCalled = true;
	}

	// Token: 0x060003E0 RID: 992 RVA: 0x00009158 File Offset: 0x00007558
	protected virtual void Update()
	{
		this.player.Update();
		bool flag;
		if (this.player.isFrameAvailable)
		{
			flag = this.player.UpdateMaterial(this.material);
			if (flag)
			{
				this.OnMaterialUpdated();
			}
		}
		else
		{
			flag = false;
		}
		if (this.isMaterialAvailable != flag)
		{
			this.isMaterialAvailable = flag;
			this.OnMaterialAvailableChanged();
		}
		if (this.renderMode == CriManaMovieMaterial.RenderMode.Always)
		{
			this.player.OnWillRenderObject(this);
		}
	}

	// Token: 0x060003E1 RID: 993 RVA: 0x000091D5 File Offset: 0x000075D5
	public virtual void RenderMovie()
	{
		this.player.OnWillRenderObject(this);
	}

	// Token: 0x060003E2 RID: 994 RVA: 0x000091E3 File Offset: 0x000075E3
	protected virtual void OnWillRenderObject()
	{
		if (this.renderMode == CriManaMovieMaterial.RenderMode.OnVisibility)
		{
			this.player.OnWillRenderObject(this);
		}
	}

	// Token: 0x060003E3 RID: 995 RVA: 0x000091FD File Offset: 0x000075FD
	private void OnApplicationPause(bool appPause)
	{
		this.ProcessApplicationPause(appPause);
	}

	// Token: 0x060003E4 RID: 996 RVA: 0x00009208 File Offset: 0x00007608
	private void ProcessApplicationPause(bool appPause)
	{
		if (appPause)
		{
			this.unpauseOnApplicationUnpause = !this.player.IsPaused();
			if (this.unpauseOnApplicationUnpause)
			{
				this.player.Pause(true);
			}
		}
		else
		{
			if (this.unpauseOnApplicationUnpause)
			{
				this.player.Pause(false);
			}
			this.unpauseOnApplicationUnpause = false;
		}
	}

	// Token: 0x060003E5 RID: 997 RVA: 0x0000926C File Offset: 0x0000766C
	protected virtual void OnDrawGizmos()
	{
		if (this.player != null && this.player.status == Player.Status.Playing)
		{
			Gizmos.color = new Color(1f, 1f, 1f, 0.8f);
		}
		else
		{
			Gizmos.color = new Color(1f, 1f, 1f, 0.5f);
		}
		Gizmos.DrawIcon(base.transform.position, "CriWare/film.png");
		Gizmos.DrawLine(base.transform.position, new Vector3(0f, 0f, 0f));
	}

	// Token: 0x060003E6 RID: 998 RVA: 0x0000930F File Offset: 0x0000770F
	private void CreateMaterial()
	{
		this._material = new Material(Shader.Find("VertexLit"));
		this._material.name = "CriMana-MovieMaterial";
		this.materialOwn = true;
	}

	// Token: 0x04000234 RID: 564
	public bool playOnStart;

	// Token: 0x04000237 RID: 567
	public CriManaMovieMaterial.RenderMode renderMode;

	// Token: 0x04000238 RID: 568
	[SerializeField]
	private Material _material;

	// Token: 0x04000239 RID: 569
	[SerializeField]
	private string _moviePath;

	// Token: 0x0400023A RID: 570
	[SerializeField]
	private bool _loop;

	// Token: 0x0400023B RID: 571
	[SerializeField]
	private bool _additiveMode;

	// Token: 0x0400023C RID: 572
	[SerializeField]
	private bool _advancedAudio;

	// Token: 0x0400023D RID: 573
	[SerializeField]
	private bool _ambisonics;

	// Token: 0x0400023E RID: 574
	private bool materialOwn;

	// Token: 0x0400023F RID: 575
	private bool isMonoBehaviourStartCalled;

	// Token: 0x04000240 RID: 576
	private GameObject ambisonicSource;

	// Token: 0x04000241 RID: 577
	private bool unpauseOnApplicationUnpause;

	// Token: 0x0200007E RID: 126
	public enum RenderMode
	{
		// Token: 0x04000244 RID: 580
		Always,
		// Token: 0x04000245 RID: 581
		OnVisibility,
		// Token: 0x04000246 RID: 582
		Never
	}
}
