﻿using System;
using UnityEngine;

// Token: 0x0200007A RID: 122
public class CriManaAmbisonicSource : MonoBehaviour
{
	// Token: 0x060003B4 RID: 948 RVA: 0x000089A6 File Offset: 0x00006DA6
	private void Update()
	{
		this.UpdateAmbisonicSourceOrientation();
	}

	// Token: 0x060003B5 RID: 949 RVA: 0x000089B0 File Offset: 0x00006DB0
	private void OnEnable()
	{
		this.atomEx3DsourceForAmbisonics = base.gameObject.transform.parent.GetComponent<CriManaMovieMaterial>().player.atomEx3DsourceForAmbisonics;
		if (this.atomEx3DsourceForAmbisonics == null)
		{
			Debug.LogError("atomEx3DsourceForAmbisonics == null");
			return;
		}
		this.ForceUpdateAmbisonicSourceOrientation();
	}

	// Token: 0x060003B6 RID: 950 RVA: 0x00008A00 File Offset: 0x00006E00
	private void ForceUpdateAmbisonicSourceOrientation()
	{
		this.lastEulerOfAmbisonicSource = base.transform.eulerAngles;
		this.RoatateAmbisonicSourceOrientationByTransformOfChild(ref this.lastEulerOfAmbisonicSource);
		this.atomEx3DsourceForAmbisonics.SetOrientation(this.ambisonicSourceOrientationFront, this.ambisonicSourceOrientationTop);
		this.atomEx3DsourceForAmbisonics.Update();
	}

	// Token: 0x060003B7 RID: 951 RVA: 0x00008A4C File Offset: 0x00006E4C
	private void UpdateAmbisonicSourceOrientation()
	{
		if (this.lastEulerOfAmbisonicSource != base.transform.eulerAngles)
		{
			this.ForceUpdateAmbisonicSourceOrientation();
		}
	}

	// Token: 0x060003B8 RID: 952 RVA: 0x00008A70 File Offset: 0x00006E70
	private void RoatateAmbisonicSourceOrientationByTransformOfChild(ref Vector3 input_euler)
	{
		Quaternion quaternion = Quaternion.Euler(input_euler);
		float num = quaternion.x * quaternion.x + quaternion.y * quaternion.y + quaternion.z * quaternion.z + quaternion.w * quaternion.w;
		float num2;
		if (num <= 0f)
		{
			num2 = 0f;
		}
		else
		{
			num2 = 2f / num;
		}
		float[] array = new float[]
		{
			1f - num2 * (quaternion.y * quaternion.y + quaternion.z * quaternion.z),
			num2 * (quaternion.x * quaternion.y - quaternion.w * quaternion.z),
			num2 * (quaternion.x * quaternion.z + quaternion.w * quaternion.y),
			num2 * (quaternion.x * quaternion.y + quaternion.w * quaternion.z),
			1f - num2 * (quaternion.x * quaternion.x + quaternion.z * quaternion.z),
			num2 * (quaternion.y * quaternion.z - quaternion.w * quaternion.x),
			num2 * (quaternion.x * quaternion.z - quaternion.w * quaternion.y),
			num2 * (quaternion.y * quaternion.z + quaternion.w * quaternion.x),
			1f - num2 * (quaternion.x * quaternion.x + quaternion.y * quaternion.y)
		};
		Vector3 vector = new Vector3(0f, 0f, 1f);
		Vector3 vector2 = this.ambisonicSourceOrientationFront;
		vector2.x = array[0] * vector.x + array[1] * vector.y + array[2] * vector.z;
		vector2.y = array[3] * vector.x + array[4] * vector.y + array[5] * vector.z;
		vector2.z = array[6] * vector.x + array[7] * vector.y + array[8] * vector.z;
		this.ambisonicSourceOrientationFront = vector2;
		Vector3 vector3 = new Vector3(0f, 1f, 0f);
		Vector3 vector4 = this.ambisonicSourceOrientationTop;
		vector4.x = array[0] * vector3.x + array[1] * vector3.y + array[2] * vector3.z;
		vector4.y = array[3] * vector3.x + array[4] * vector3.y + array[5] * vector3.z;
		vector4.z = array[6] * vector3.x + array[7] * vector3.y + array[8] * vector3.z;
		this.ambisonicSourceOrientationTop = vector4;
	}

	// Token: 0x0400022A RID: 554
	private CriAtomEx3dSource atomEx3DsourceForAmbisonics;

	// Token: 0x0400022B RID: 555
	private Vector3 ambisonicSourceOrientationFront;

	// Token: 0x0400022C RID: 556
	private Vector3 ambisonicSourceOrientationTop;

	// Token: 0x0400022D RID: 557
	private Vector3 lastEulerOfAmbisonicSource;
}
