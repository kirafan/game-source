﻿using System;

// Token: 0x02000093 RID: 147
[Serializable]
public class CriAtomConfig
{
	// Token: 0x040002AC RID: 684
	public string acfFileName = string.Empty;

	// Token: 0x040002AD RID: 685
	public int maxVirtualVoices = 32;

	// Token: 0x040002AE RID: 686
	public int maxVoiceLimitGroups = 32;

	// Token: 0x040002AF RID: 687
	public int maxCategories = 32;

	// Token: 0x040002B0 RID: 688
	public int maxSequenceEventsPerFrame = 2;

	// Token: 0x040002B1 RID: 689
	public int maxBeatSyncCallbacksPerFrame = 1;

	// Token: 0x040002B2 RID: 690
	public CriAtomConfig.StandardVoicePoolConfig standardVoicePoolConfig = new CriAtomConfig.StandardVoicePoolConfig();

	// Token: 0x040002B3 RID: 691
	public CriAtomConfig.HcaMxVoicePoolConfig hcaMxVoicePoolConfig = new CriAtomConfig.HcaMxVoicePoolConfig();

	// Token: 0x040002B4 RID: 692
	public int outputSamplingRate;

	// Token: 0x040002B5 RID: 693
	public bool usesInGamePreview;

	// Token: 0x040002B6 RID: 694
	public float serverFrequency = 60f;

	// Token: 0x040002B7 RID: 695
	public int asrOutputChannels;

	// Token: 0x040002B8 RID: 696
	public bool useRandomSeedWithTime;

	// Token: 0x040002B9 RID: 697
	public int categoriesPerPlayback = 4;

	// Token: 0x040002BA RID: 698
	public int maxBuses = 8;

	// Token: 0x040002BB RID: 699
	public int maxParameterBlocks = 1024;

	// Token: 0x040002BC RID: 700
	public bool vrMode;

	// Token: 0x040002BD RID: 701
	public int pcBufferingTime;

	// Token: 0x040002BE RID: 702
	public int iosBufferingTime = 50;

	// Token: 0x040002BF RID: 703
	public bool iosOverrideIPodMusic;

	// Token: 0x040002C0 RID: 704
	public int androidBufferingTime = 133;

	// Token: 0x040002C1 RID: 705
	public int androidStartBufferingTime = 100;

	// Token: 0x040002C2 RID: 706
	public CriAtomConfig.AndroidLowLatencyStandardVoicePoolConfig androidLowLatencyStandardVoicePoolConfig = new CriAtomConfig.AndroidLowLatencyStandardVoicePoolConfig();

	// Token: 0x040002C3 RID: 707
	public bool androidUsesAndroidFastMixer = true;

	// Token: 0x040002C4 RID: 708
	public CriAtomConfig.VitaAtrac9VoicePoolConfig vitaAtrac9VoicePoolConfig = new CriAtomConfig.VitaAtrac9VoicePoolConfig();

	// Token: 0x040002C5 RID: 709
	public CriAtomConfig.Ps4Atrac9VoicePoolConfig ps4Atrac9VoicePoolConfig = new CriAtomConfig.Ps4Atrac9VoicePoolConfig();

	// Token: 0x040002C6 RID: 710
	public CriAtomConfig.Ps4Audio3dConfig ps4Audio3dConfig = new CriAtomConfig.Ps4Audio3dConfig();

	// Token: 0x040002C7 RID: 711
	public CriAtomConfig.WebGLWebAudioVoicePoolConfig webglWebAudioVoicePoolConfig = new CriAtomConfig.WebGLWebAudioVoicePoolConfig();

	// Token: 0x02000094 RID: 148
	[Serializable]
	public class StandardVoicePoolConfig
	{
		// Token: 0x040002C8 RID: 712
		public int memoryVoices = 16;

		// Token: 0x040002C9 RID: 713
		public int streamingVoices = 8;
	}

	// Token: 0x02000095 RID: 149
	[Serializable]
	public class HcaMxVoicePoolConfig
	{
		// Token: 0x040002CA RID: 714
		public int memoryVoices;

		// Token: 0x040002CB RID: 715
		public int streamingVoices;
	}

	// Token: 0x02000096 RID: 150
	[Serializable]
	public class AndroidLowLatencyStandardVoicePoolConfig
	{
		// Token: 0x040002CC RID: 716
		public int memoryVoices;

		// Token: 0x040002CD RID: 717
		public int streamingVoices;
	}

	// Token: 0x02000097 RID: 151
	[Serializable]
	public class VitaAtrac9VoicePoolConfig
	{
		// Token: 0x040002CE RID: 718
		public int memoryVoices;

		// Token: 0x040002CF RID: 719
		public int streamingVoices;
	}

	// Token: 0x02000098 RID: 152
	[Serializable]
	public class Ps4Atrac9VoicePoolConfig
	{
		// Token: 0x040002D0 RID: 720
		public int memoryVoices;

		// Token: 0x040002D1 RID: 721
		public int streamingVoices;
	}

	// Token: 0x02000099 RID: 153
	[Serializable]
	public class Ps4Audio3dConfig
	{
		// Token: 0x040002D2 RID: 722
		public bool useAudio3D;

		// Token: 0x040002D3 RID: 723
		public CriAtomConfig.Ps4Audio3dConfig.VoicePoolConfig voicePoolConfig = new CriAtomConfig.Ps4Audio3dConfig.VoicePoolConfig();

		// Token: 0x0200009A RID: 154
		[Serializable]
		public class VoicePoolConfig
		{
			// Token: 0x040002D4 RID: 724
			public int memoryVoices;

			// Token: 0x040002D5 RID: 725
			public int streamingVoices;
		}
	}

	// Token: 0x0200009B RID: 155
	[Serializable]
	public class WebGLWebAudioVoicePoolConfig
	{
		// Token: 0x040002D6 RID: 726
		public int voices = 16;
	}
}
