﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000012 RID: 18
public static class CriAtomEx
{
	// Token: 0x060000D2 RID: 210 RVA: 0x000049D4 File Offset: 0x00002DD4
	public static void RegisterAcf(CriFsBinder binder, string acfPath)
	{
		IntPtr binder2 = (binder == null) ? IntPtr.Zero : binder.nativeHandle;
		CriAtomEx.criAtomEx_RegisterAcfFile(binder2, acfPath, IntPtr.Zero, 0);
	}

	// Token: 0x060000D3 RID: 211 RVA: 0x00004A06 File Offset: 0x00002E06
	public static void RegisterAcf(byte[] acfData)
	{
		CriAtomEx.criAtomEx_RegisterAcfData(acfData, acfData.Length, IntPtr.Zero, 0);
	}

	// Token: 0x060000D4 RID: 212 RVA: 0x00004A18 File Offset: 0x00002E18
	public static void UnregisterAcf()
	{
		CriAtomEx.criAtomEx_UnregisterAcf();
	}

	// Token: 0x060000D5 RID: 213 RVA: 0x00004A1F File Offset: 0x00002E1F
	public static void AttachDspBusSetting(string settingName)
	{
		CriAtomEx.criAtomEx_AttachDspBusSetting(settingName, IntPtr.Zero, 0);
	}

	// Token: 0x060000D6 RID: 214 RVA: 0x00004A2D File Offset: 0x00002E2D
	public static void DetachDspBusSetting()
	{
		CriAtomEx.criAtomEx_DetachDspBusSetting();
	}

	// Token: 0x060000D7 RID: 215 RVA: 0x00004A34 File Offset: 0x00002E34
	public static void ApplyDspBusSnapshot(string snapshot_name, int time_ms)
	{
		CriAtomEx.criAtomEx_ApplyDspBusSnapshot(snapshot_name, time_ms);
	}

	// Token: 0x060000D8 RID: 216 RVA: 0x00004A3D File Offset: 0x00002E3D
	public static int GetNumGameVariables()
	{
		return CriAtomEx.criAtomEx_GetNumGameVariables();
	}

	// Token: 0x060000D9 RID: 217 RVA: 0x00004A44 File Offset: 0x00002E44
	public static bool GetGameVariableInfo(ushort index, out CriAtomEx.GameVariableInfo info)
	{
		bool result;
		using (CriStructMemory<CriAtomEx.GameVariableInfo> criStructMemory = new CriStructMemory<CriAtomEx.GameVariableInfo>())
		{
			bool flag = CriAtomEx.criAtomEx_GetGameVariableInfo(index, criStructMemory.ptr);
			info = new CriAtomEx.GameVariableInfo(criStructMemory.bytes, 0);
			result = flag;
		}
		return result;
	}

	// Token: 0x060000DA RID: 218 RVA: 0x00004A98 File Offset: 0x00002E98
	public static float GetGameVariable(uint game_variable_id)
	{
		return CriAtomEx.criAtomEx_GetGameVariableById(game_variable_id);
	}

	// Token: 0x060000DB RID: 219 RVA: 0x00004AA0 File Offset: 0x00002EA0
	public static float GetGameVariable(string game_variable_name)
	{
		return CriAtomEx.criAtomEx_GetGameVariableByName(game_variable_name);
	}

	// Token: 0x060000DC RID: 220 RVA: 0x00004AA8 File Offset: 0x00002EA8
	public static void SetGameVariable(uint game_variable_id, float game_variable_value)
	{
		CriAtomEx.criAtomEx_SetGameVariableById(game_variable_id, game_variable_value);
	}

	// Token: 0x060000DD RID: 221 RVA: 0x00004AB1 File Offset: 0x00002EB1
	public static void SetGameVariable(string game_variable_name, float game_variable_value)
	{
		CriAtomEx.criAtomEx_SetGameVariableByName(game_variable_name, game_variable_value);
	}

	// Token: 0x060000DE RID: 222 RVA: 0x00004ABA File Offset: 0x00002EBA
	public static void SetRandomSeed(uint seed)
	{
		CriAtomEx.criAtomEx_SetRandomSeed(seed);
	}

	// Token: 0x060000DF RID: 223 RVA: 0x00004AC2 File Offset: 0x00002EC2
	public static void ResetPerformanceMonitor()
	{
		CriAtomEx.criAtom_ResetPerformanceMonitor();
	}

	// Token: 0x060000E0 RID: 224 RVA: 0x00004AC9 File Offset: 0x00002EC9
	public static void GetPerformanceInfo(out CriAtomEx.PerformanceInfo info)
	{
		CriAtomEx.criAtom_GetPerformanceInfo(out info);
	}

	// Token: 0x060000E1 RID: 225 RVA: 0x00004AD1 File Offset: 0x00002ED1
	public static void SetOutputVolume_VITA(float volume)
	{
	}

	// Token: 0x060000E2 RID: 226 RVA: 0x00004AD3 File Offset: 0x00002ED3
	public static bool IsBgmPortAcquired_VITA()
	{
		return true;
	}

	// Token: 0x060000E3 RID: 227 RVA: 0x00004AD6 File Offset: 0x00002ED6
	public static bool IsSoundStopped_IOS()
	{
		return false;
	}

	// Token: 0x060000E4 RID: 228
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomEx_RegisterAcfFile(IntPtr binder, string path, IntPtr work, int workSize);

	// Token: 0x060000E5 RID: 229
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomEx_RegisterAcfData(byte[] acfData, int acfDataSize, IntPtr work, int workSize);

	// Token: 0x060000E6 RID: 230
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx_UnregisterAcf();

	// Token: 0x060000E7 RID: 231
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx_AttachDspBusSetting(string settingName, IntPtr work, int workSize);

	// Token: 0x060000E8 RID: 232
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx_DetachDspBusSetting();

	// Token: 0x060000E9 RID: 233
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx_ApplyDspBusSnapshot(string snapshot_name, int time_ms);

	// Token: 0x060000EA RID: 234
	[DllImport("cri_ware_unity")]
	private static extern int criAtomEx_GetNumGameVariables();

	// Token: 0x060000EB RID: 235
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomEx_GetGameVariableInfo(ushort index, IntPtr game_variable_info);

	// Token: 0x060000EC RID: 236
	[DllImport("cri_ware_unity")]
	private static extern float criAtomEx_GetGameVariableById(uint game_variable_id);

	// Token: 0x060000ED RID: 237
	[DllImport("cri_ware_unity")]
	private static extern float criAtomEx_GetGameVariableByName(string game_variable_name);

	// Token: 0x060000EE RID: 238
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx_SetGameVariableById(uint game_variable_id, float game_variable_value);

	// Token: 0x060000EF RID: 239
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx_SetGameVariableByName(string game_variable_name, float game_variable_value);

	// Token: 0x060000F0 RID: 240
	[DllImport("cri_ware_unity")]
	private static extern void criAtomEx_SetRandomSeed(uint seed);

	// Token: 0x060000F1 RID: 241
	[DllImport("cri_ware_unity")]
	private static extern void criAtom_ResetPerformanceMonitor();

	// Token: 0x060000F2 RID: 242
	[DllImport("cri_ware_unity")]
	private static extern void criAtom_GetPerformanceInfo(out CriAtomEx.PerformanceInfo info);

	// Token: 0x060000F3 RID: 243
	[DllImport("cri_ware_unity")]
	public static extern void criAtom_EnableSlLatencyCheck_ANDROID(bool sw);

	// Token: 0x060000F4 RID: 244
	[DllImport("cri_ware_unity")]
	public static extern int criAtom_GetSlBufferConsumptionLatency_ANDROID();

	// Token: 0x04000084 RID: 132
	public const uint InvalidAisacControlId = 65535U;

	// Token: 0x02000013 RID: 19
	public enum CharacterEncoding
	{
		// Token: 0x04000086 RID: 134
		Utf8,
		// Token: 0x04000087 RID: 135
		Sjis
	}

	// Token: 0x02000014 RID: 20
	public enum SoundRendererType
	{
		// Token: 0x04000089 RID: 137
		Default,
		// Token: 0x0400008A RID: 138
		Native,
		// Token: 0x0400008B RID: 139
		Asr,
		// Token: 0x0400008C RID: 140
		Hw1 = 1,
		// Token: 0x0400008D RID: 141
		Hw2 = 9
	}

	// Token: 0x02000015 RID: 21
	public enum VoiceAllocationMethod
	{
		// Token: 0x0400008F RID: 143
		Once,
		// Token: 0x04000090 RID: 144
		Retry
	}

	// Token: 0x02000016 RID: 22
	public enum BiquadFilterType
	{
		// Token: 0x04000092 RID: 146
		Off,
		// Token: 0x04000093 RID: 147
		LowPass,
		// Token: 0x04000094 RID: 148
		HighPass,
		// Token: 0x04000095 RID: 149
		Notch,
		// Token: 0x04000096 RID: 150
		LowShelf,
		// Token: 0x04000097 RID: 151
		HighShelf,
		// Token: 0x04000098 RID: 152
		Peaking
	}

	// Token: 0x02000017 RID: 23
	public enum ResumeMode
	{
		// Token: 0x0400009A RID: 154
		AllPlayback,
		// Token: 0x0400009B RID: 155
		PausedPlayback,
		// Token: 0x0400009C RID: 156
		PreparedPlayback
	}

	// Token: 0x02000018 RID: 24
	public enum PanType
	{
		// Token: 0x0400009E RID: 158
		Pan3d,
		// Token: 0x0400009F RID: 159
		Pos3d,
		// Token: 0x040000A0 RID: 160
		Auto
	}

	// Token: 0x02000019 RID: 25
	public enum VoiceControlMethod
	{
		// Token: 0x040000A2 RID: 162
		PreferLast,
		// Token: 0x040000A3 RID: 163
		PreferFirst
	}

	// Token: 0x0200001A RID: 26
	public enum Parameter
	{
		// Token: 0x040000A5 RID: 165
		Volume,
		// Token: 0x040000A6 RID: 166
		Pitch,
		// Token: 0x040000A7 RID: 167
		Pan3dAngle,
		// Token: 0x040000A8 RID: 168
		Pan3dDistance,
		// Token: 0x040000A9 RID: 169
		Pan3dVolume,
		// Token: 0x040000AA RID: 170
		BusSendLevel0 = 9,
		// Token: 0x040000AB RID: 171
		BusSendLevel1,
		// Token: 0x040000AC RID: 172
		BusSendLevel2,
		// Token: 0x040000AD RID: 173
		BusSendLevel3,
		// Token: 0x040000AE RID: 174
		BusSendLevel4,
		// Token: 0x040000AF RID: 175
		BusSendLevel5,
		// Token: 0x040000B0 RID: 176
		BusSendLevel6,
		// Token: 0x040000B1 RID: 177
		BusSendLevel7,
		// Token: 0x040000B2 RID: 178
		BandPassFilterCofLow,
		// Token: 0x040000B3 RID: 179
		BandPassFilterCofHigh,
		// Token: 0x040000B4 RID: 180
		BiquadFilterType,
		// Token: 0x040000B5 RID: 181
		BiquadFilterFreq,
		// Token: 0x040000B6 RID: 182
		BiquadFIlterQ,
		// Token: 0x040000B7 RID: 183
		BiquadFilterGain,
		// Token: 0x040000B8 RID: 184
		EnvelopeAttackTime,
		// Token: 0x040000B9 RID: 185
		EnvelopeHoldTime,
		// Token: 0x040000BA RID: 186
		EnvelopeDecayTime,
		// Token: 0x040000BB RID: 187
		EnvelopeReleaseTime,
		// Token: 0x040000BC RID: 188
		EnvelopeSustainLevel,
		// Token: 0x040000BD RID: 189
		StartTime,
		// Token: 0x040000BE RID: 190
		Priority = 31
	}

	// Token: 0x0200001B RID: 27
	public enum Speaker
	{
		// Token: 0x040000C0 RID: 192
		FrontLeft,
		// Token: 0x040000C1 RID: 193
		FrontRight,
		// Token: 0x040000C2 RID: 194
		FrontCenter,
		// Token: 0x040000C3 RID: 195
		LowFrequency,
		// Token: 0x040000C4 RID: 196
		SurroundLeft,
		// Token: 0x040000C5 RID: 197
		SurroundRight,
		// Token: 0x040000C6 RID: 198
		SurroundBackLeft,
		// Token: 0x040000C7 RID: 199
		SurroundBackRight
	}

	// Token: 0x0200001C RID: 28
	public enum Format : uint
	{
		// Token: 0x040000C9 RID: 201
		ADX = 1U,
		// Token: 0x040000CA RID: 202
		HCA = 3U,
		// Token: 0x040000CB RID: 203
		HCA_MX,
		// Token: 0x040000CC RID: 204
		WAVE
	}

	// Token: 0x0200001D RID: 29
	public struct FormatInfo
	{
		// Token: 0x040000CD RID: 205
		public CriAtomEx.Format format;

		// Token: 0x040000CE RID: 206
		public int samplingRate;

		// Token: 0x040000CF RID: 207
		public long numSamples;

		// Token: 0x040000D0 RID: 208
		public long loopOffset;

		// Token: 0x040000D1 RID: 209
		public long loopLength;

		// Token: 0x040000D2 RID: 210
		public int numChannels;

		// Token: 0x040000D3 RID: 211
		public uint reserved;
	}

	// Token: 0x0200001E RID: 30
	public struct AisacControlInfo
	{
		// Token: 0x060000F5 RID: 245 RVA: 0x00004ADC File Offset: 0x00002EDC
		public AisacControlInfo(byte[] data, int startIndex)
		{
			if (IntPtr.Size == 4)
			{
				this.name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt32(data, startIndex)));
				this.id = BitConverter.ToUInt32(data, startIndex + 4);
			}
			else
			{
				this.name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt64(data, startIndex)));
				this.id = BitConverter.ToUInt32(data, startIndex + 8);
			}
		}

		// Token: 0x040000D4 RID: 212
		[MarshalAs(UnmanagedType.LPStr)]
		public readonly string name;

		// Token: 0x040000D5 RID: 213
		public uint id;
	}

	// Token: 0x0200001F RID: 31
	public struct CuePos3dInfo
	{
		// Token: 0x060000F6 RID: 246 RVA: 0x00004B48 File Offset: 0x00002F48
		public CuePos3dInfo(byte[] data, int startIndex)
		{
			this.coneInsideAngle = BitConverter.ToSingle(data, startIndex);
			this.coneOutsideAngle = BitConverter.ToSingle(data, startIndex + 4);
			this.minAttenuationDistance = BitConverter.ToSingle(data, startIndex + 8);
			this.maxAttenuationDistance = BitConverter.ToSingle(data, startIndex + 12);
			this.sourceRadius = BitConverter.ToSingle(data, startIndex + 16);
			this.interiorDistance = BitConverter.ToSingle(data, startIndex + 20);
			this.dopplerFactor = BitConverter.ToSingle(data, startIndex + 24);
			this.distanceAisacControl = BitConverter.ToUInt16(data, startIndex + 28);
			this.listenerBaseAngleAisacControl = BitConverter.ToUInt16(data, startIndex + 30);
			this.sourceBaseAngleAisacControl = BitConverter.ToUInt16(data, startIndex + 32);
			this.reserved = new ushort[1];
			for (int i = 0; i < 1; i++)
			{
				this.reserved[i] = BitConverter.ToUInt16(data, startIndex + 34 + 2 * i);
			}
		}

		// Token: 0x040000D6 RID: 214
		public float coneInsideAngle;

		// Token: 0x040000D7 RID: 215
		public float coneOutsideAngle;

		// Token: 0x040000D8 RID: 216
		public float minAttenuationDistance;

		// Token: 0x040000D9 RID: 217
		public float maxAttenuationDistance;

		// Token: 0x040000DA RID: 218
		public float sourceRadius;

		// Token: 0x040000DB RID: 219
		public float interiorDistance;

		// Token: 0x040000DC RID: 220
		public float dopplerFactor;

		// Token: 0x040000DD RID: 221
		public ushort distanceAisacControl;

		// Token: 0x040000DE RID: 222
		public ushort listenerBaseAngleAisacControl;

		// Token: 0x040000DF RID: 223
		public ushort sourceBaseAngleAisacControl;

		// Token: 0x040000E0 RID: 224
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
		public ushort[] reserved;
	}

	// Token: 0x02000020 RID: 32
	public struct GameVariableInfo
	{
		// Token: 0x060000F7 RID: 247 RVA: 0x00004C24 File Offset: 0x00003024
		public GameVariableInfo(byte[] data, int startIndex)
		{
			if (IntPtr.Size == 4)
			{
				this.name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt32(data, startIndex)));
				this.id = BitConverter.ToUInt32(data, startIndex + 4);
				this.gameValue = BitConverter.ToSingle(data, startIndex + 8);
			}
			else
			{
				this.name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt64(data, startIndex)));
				this.id = BitConverter.ToUInt32(data, startIndex + 8);
				this.gameValue = BitConverter.ToSingle(data, startIndex + 12);
			}
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x00004CAC File Offset: 0x000030AC
		public GameVariableInfo(string name, uint id, float gameValue)
		{
			this.name = name;
			this.id = id;
			this.gameValue = gameValue;
		}

		// Token: 0x040000E1 RID: 225
		[MarshalAs(UnmanagedType.LPStr)]
		public readonly string name;

		// Token: 0x040000E2 RID: 226
		public uint id;

		// Token: 0x040000E3 RID: 227
		public float gameValue;
	}

	// Token: 0x02000021 RID: 33
	public enum CueType
	{
		// Token: 0x040000E5 RID: 229
		Polyphonic,
		// Token: 0x040000E6 RID: 230
		Sequential,
		// Token: 0x040000E7 RID: 231
		Shuffle,
		// Token: 0x040000E8 RID: 232
		Random,
		// Token: 0x040000E9 RID: 233
		RandomNoRepeat,
		// Token: 0x040000EA RID: 234
		SwitchGameVariable,
		// Token: 0x040000EB RID: 235
		ComboSequential,
		// Token: 0x040000EC RID: 236
		SwitchSelector,
		// Token: 0x040000ED RID: 237
		TrackTransitionBySelector
	}

	// Token: 0x02000022 RID: 34
	public struct CueInfo
	{
		// Token: 0x060000F9 RID: 249 RVA: 0x00004CC4 File Offset: 0x000030C4
		public CueInfo(byte[] data, int startIndex)
		{
			if (IntPtr.Size == 4)
			{
				this.id = BitConverter.ToInt32(data, startIndex);
				this.type = (CriAtomEx.CueType)BitConverter.ToInt32(data, startIndex + 4);
				this.name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt32(data, startIndex + 8)));
				this.userData = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt32(data, startIndex + 12)));
				this.length = BitConverter.ToInt64(data, startIndex + 16);
				this.categories = new ushort[16];
				for (int i = 0; i < 16; i++)
				{
					this.categories[i] = BitConverter.ToUInt16(data, startIndex + 24 + 2 * i);
				}
				this.numLimits = BitConverter.ToInt16(data, startIndex + 56);
				this.numBlocks = BitConverter.ToUInt16(data, startIndex + 58);
				this.numTracks = BitConverter.ToUInt16(data, startIndex + 60);
				this.reserved = new ushort[1];
				for (int j = 0; j < 1; j++)
				{
					this.reserved[j] = BitConverter.ToUInt16(data, startIndex + 62 + 2 * j);
				}
				this.priority = data[startIndex + 64];
				this.headerVisibility = data[startIndex + 65];
				this.ignore_player_parameter = data[startIndex + 66];
				this.probability = data[startIndex + 67];
				this.pos3dInfo = new CriAtomEx.CuePos3dInfo(data, startIndex + 68);
				this.gameVariableInfo = new CriAtomEx.GameVariableInfo(data, startIndex + 104);
			}
			else
			{
				this.id = BitConverter.ToInt32(data, startIndex);
				this.type = (CriAtomEx.CueType)BitConverter.ToInt32(data, startIndex + 4);
				this.name = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt64(data, startIndex + 8)));
				this.userData = Marshal.PtrToStringAnsi(new IntPtr(BitConverter.ToInt64(data, startIndex + 16)));
				this.length = BitConverter.ToInt64(data, startIndex + 24);
				this.categories = new ushort[16];
				for (int k = 0; k < 16; k++)
				{
					this.categories[k] = BitConverter.ToUInt16(data, startIndex + 32 + 2 * k);
				}
				this.numLimits = BitConverter.ToInt16(data, startIndex + 64);
				this.numBlocks = BitConverter.ToUInt16(data, startIndex + 66);
				this.numTracks = BitConverter.ToUInt16(data, startIndex + 68);
				this.reserved = new ushort[1];
				for (int l = 0; l < 1; l++)
				{
					this.reserved[l] = BitConverter.ToUInt16(data, startIndex + 70 + 2 * l);
				}
				this.priority = data[startIndex + 72];
				this.headerVisibility = data[startIndex + 73];
				this.ignore_player_parameter = data[startIndex + 74];
				this.probability = data[startIndex + 75];
				this.pos3dInfo = new CriAtomEx.CuePos3dInfo(data, startIndex + 76);
				this.gameVariableInfo = new CriAtomEx.GameVariableInfo(data, startIndex + 112);
			}
		}

		// Token: 0x040000EE RID: 238
		public int id;

		// Token: 0x040000EF RID: 239
		public CriAtomEx.CueType type;

		// Token: 0x040000F0 RID: 240
		[MarshalAs(UnmanagedType.LPStr)]
		public readonly string name;

		// Token: 0x040000F1 RID: 241
		[MarshalAs(UnmanagedType.LPStr)]
		public readonly string userData;

		// Token: 0x040000F2 RID: 242
		public long length;

		// Token: 0x040000F3 RID: 243
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 16)]
		public ushort[] categories;

		// Token: 0x040000F4 RID: 244
		public short numLimits;

		// Token: 0x040000F5 RID: 245
		public ushort numBlocks;

		// Token: 0x040000F6 RID: 246
		public ushort numTracks;

		// Token: 0x040000F7 RID: 247
		public ushort[] reserved;

		// Token: 0x040000F8 RID: 248
		public byte priority;

		// Token: 0x040000F9 RID: 249
		public byte headerVisibility;

		// Token: 0x040000FA RID: 250
		public byte ignore_player_parameter;

		// Token: 0x040000FB RID: 251
		public byte probability;

		// Token: 0x040000FC RID: 252
		public CriAtomEx.CuePos3dInfo pos3dInfo;

		// Token: 0x040000FD RID: 253
		public CriAtomEx.GameVariableInfo gameVariableInfo;
	}

	// Token: 0x02000023 RID: 35
	public struct WaveformInfo
	{
		// Token: 0x060000FA RID: 250 RVA: 0x00004F74 File Offset: 0x00003374
		public WaveformInfo(byte[] data, int startIndex)
		{
			this.waveId = BitConverter.ToInt32(data, startIndex);
			this.format = BitConverter.ToUInt32(data, startIndex + 4);
			this.samplingRate = BitConverter.ToInt32(data, startIndex + 8);
			this.numChannels = BitConverter.ToInt32(data, startIndex + 12);
			this.numSamples = BitConverter.ToInt64(data, startIndex + 16);
			this.streamingFlag = (BitConverter.ToInt32(data, startIndex + 24) != 0);
			this.reserved = new uint[1];
			for (int i = 0; i < 1; i++)
			{
				this.reserved[i] = BitConverter.ToUInt32(data, startIndex + 28 + 4 * i);
			}
		}

		// Token: 0x040000FE RID: 254
		public int waveId;

		// Token: 0x040000FF RID: 255
		public uint format;

		// Token: 0x04000100 RID: 256
		public int samplingRate;

		// Token: 0x04000101 RID: 257
		public int numChannels;

		// Token: 0x04000102 RID: 258
		public long numSamples;

		// Token: 0x04000103 RID: 259
		public bool streamingFlag;

		// Token: 0x04000104 RID: 260
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 1)]
		public uint[] reserved;
	}

	// Token: 0x02000024 RID: 36
	public struct PerformanceInfo
	{
		// Token: 0x04000105 RID: 261
		public uint serverProcessCount;

		// Token: 0x04000106 RID: 262
		public uint lastServerTime;

		// Token: 0x04000107 RID: 263
		public uint maxServerTime;

		// Token: 0x04000108 RID: 264
		public uint averageServerTime;

		// Token: 0x04000109 RID: 265
		public uint lastServerInterval;

		// Token: 0x0400010A RID: 266
		public uint maxServerInterval;

		// Token: 0x0400010B RID: 267
		public uint averageServerInterval;
	}

	// Token: 0x02000025 RID: 37
	public struct ResourceUsage
	{
		// Token: 0x0400010C RID: 268
		public uint useCount;

		// Token: 0x0400010D RID: 269
		public uint limit;
	}
}
