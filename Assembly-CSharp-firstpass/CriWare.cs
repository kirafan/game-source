﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x0200008F RID: 143
public class CriWare
{
	// Token: 0x17000058 RID: 88
	// (get) Token: 0x060004A1 RID: 1185 RVA: 0x0000AF28 File Offset: 0x00009328
	public static string streamingAssetsPath
	{
		get
		{
			if (Application.platform == RuntimePlatform.Android)
			{
				return string.Empty;
			}
			return Application.streamingAssetsPath;
		}
	}

	// Token: 0x17000059 RID: 89
	// (get) Token: 0x060004A2 RID: 1186 RVA: 0x0000AF41 File Offset: 0x00009341
	public static string installTargetPath
	{
		get
		{
			if (Application.platform == RuntimePlatform.IPhonePlayer)
			{
				return Application.temporaryCachePath;
			}
			return Application.persistentDataPath;
		}
	}

	// Token: 0x060004A3 RID: 1187 RVA: 0x0000AF59 File Offset: 0x00009359
	public static bool IsStreamingAssetsPath(string path)
	{
		return !Path.IsPathRooted(path) && path.IndexOf(':') < 0;
	}

	// Token: 0x1700005A RID: 90
	// (get) Token: 0x060004A4 RID: 1188 RVA: 0x0000AF74 File Offset: 0x00009374
	public static GameObject managerObject
	{
		get
		{
			if (CriWare._managerObject == null)
			{
				CriWare._managerObject = GameObject.Find("/CRIWARE");
				if (CriWare._managerObject == null)
				{
					CriWare._managerObject = new GameObject("CRIWARE");
				}
			}
			return CriWare._managerObject;
		}
	}

	// Token: 0x060004A5 RID: 1189
	[DllImport("cri_ware_unity")]
	public static extern int criWareUnity_SetDecryptionKey(ulong key, string authentication_file, bool enable_atom_decryption, bool enable_mana_decryption);

	// Token: 0x060004A6 RID: 1190
	[DllImport("cri_ware_unity")]
	public static extern int criWareUnity_GetVersionNumber();

	// Token: 0x060004A7 RID: 1191
	[DllImport("cri_ware_unity")]
	public static extern void criWareUnity_SetRenderingEventOffsetForMana(int offset);

	// Token: 0x060004A8 RID: 1192 RVA: 0x0000AFC4 File Offset: 0x000093C4
	public static string GetScriptVersionString()
	{
		return "2.26.00b1";
	}

	// Token: 0x060004A9 RID: 1193 RVA: 0x0000AFCB File Offset: 0x000093CB
	public static int GetScriptVersionNumber()
	{
		return 36044801;
	}

	// Token: 0x060004AA RID: 1194 RVA: 0x0000AFD2 File Offset: 0x000093D2
	public static int GetBinaryVersionNumber()
	{
		return CriWare.criWareUnity_GetVersionNumber();
	}

	// Token: 0x060004AB RID: 1195 RVA: 0x0000AFD9 File Offset: 0x000093D9
	public static int GetRequiredBinaryVersionNumber()
	{
		return 36044801;
	}

	// Token: 0x060004AC RID: 1196 RVA: 0x0000AFE0 File Offset: 0x000093E0
	public static bool CheckBinaryVersionCompatibility()
	{
		if (CriWare.GetBinaryVersionNumber() == CriWare.GetRequiredBinaryVersionNumber())
		{
			return true;
		}
		Debug.LogError("CRI runtime library is not compatible. Confirm the version number.");
		return false;
	}

	// Token: 0x060004AD RID: 1197 RVA: 0x0000AFFE File Offset: 0x000093FE
	public static uint GetFsMemoryUsage()
	{
		return CriFsPlugin.criFsUnity_GetAllocatedHeapSize();
	}

	// Token: 0x060004AE RID: 1198 RVA: 0x0000B005 File Offset: 0x00009405
	public static uint GetAtomMemoryUsage()
	{
		return CriAtomPlugin.criAtomUnity_GetAllocatedHeapSize();
	}

	// Token: 0x060004AF RID: 1199 RVA: 0x0000B00C File Offset: 0x0000940C
	public static uint GetManaMemoryUsage()
	{
		return CriManaPlugin.criManaUnity_GetAllocatedHeapSize();
	}

	// Token: 0x060004B0 RID: 1200 RVA: 0x0000B013 File Offset: 0x00009413
	public static CriWare.CpuUsage GetAtomCpuUsage()
	{
		return CriAtomPlugin.GetCpuUsage();
	}

	// Token: 0x04000294 RID: 660
	private const string scriptVersionString = "2.26.00b1";

	// Token: 0x04000295 RID: 661
	private const int scriptVersionNumber = 36044801;

	// Token: 0x04000296 RID: 662
	public const bool supportsCriFsInstaller = true;

	// Token: 0x04000297 RID: 663
	public const bool supportsCriFsWebInstaller = true;

	// Token: 0x04000298 RID: 664
	public const string pluginName = "cri_ware_unity";

	// Token: 0x04000299 RID: 665
	public const CallingConvention pluginCallingConvention = CallingConvention.Winapi;

	// Token: 0x0400029A RID: 666
	private static GameObject _managerObject;

	// Token: 0x02000090 RID: 144
	public struct CpuUsage
	{
		// Token: 0x0400029B RID: 667
		public float last;

		// Token: 0x0400029C RID: 668
		public float average;

		// Token: 0x0400029D RID: 669
		public float peak;
	}
}
