﻿using System;
using System.Runtime.InteropServices;
using CriMana.Detail;
using UnityEngine;

// Token: 0x02000079 RID: 121
public class CriManaPlugin
{
	// Token: 0x17000043 RID: 67
	// (get) Token: 0x0600039F RID: 927 RVA: 0x0000883C File Offset: 0x00006C3C
	public static bool isInitialized
	{
		get
		{
			return CriManaPlugin.initializationCount > 0;
		}
	}

	// Token: 0x17000044 RID: 68
	// (get) Token: 0x060003A0 RID: 928 RVA: 0x00008846 File Offset: 0x00006C46
	public static bool isMultithreadedRenderingEnabled
	{
		get
		{
			return CriManaPlugin.enabledMultithreadedRendering;
		}
	}

	// Token: 0x060003A1 RID: 929 RVA: 0x00008850 File Offset: 0x00006C50
	public static void SetConfigParameters(bool graphicsMultiThreaded, int num_decoders, int max_num_of_entries)
	{
		int graphicsDeviceType = (int)SystemInfo.graphicsDeviceType;
		CriManaPlugin.enabledMultithreadedRendering = graphicsMultiThreaded;
		CriWare.criWareUnity_SetRenderingEventOffsetForMana(CriManaPlugin.renderingEventOffset);
		CriManaPlugin.criManaUnity_SetConfigParameters(graphicsDeviceType, CriManaPlugin.enabledMultithreadedRendering, num_decoders, max_num_of_entries);
	}

	// Token: 0x060003A2 RID: 930 RVA: 0x00008880 File Offset: 0x00006C80
	[Obsolete("Use CriWareVITA.EnableH264Playback and CriWareVITA.SetH264DecoderMaxSize instead.")]
	public static void SetConfigAdditonalParameters_VITA(bool use_h264_playback, int width, int height)
	{
	}

	// Token: 0x060003A3 RID: 931 RVA: 0x00008884 File Offset: 0x00006C84
	public static void SetConfigAdditonalParameters_ANDROID(bool use_h264_playback)
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("android.os.Build$VERSION"))
		{
			if (androidJavaClass.GetStatic<int>("SDK_INT") >= 16)
			{
				CriManaPlugin.criManaUnity_SetConfigAdditionalParameters_ANDROID(use_h264_playback);
			}
		}
	}

	// Token: 0x060003A4 RID: 932 RVA: 0x000088D8 File Offset: 0x00006CD8
	public static void InitializeLibrary()
	{
		CriManaPlugin.initializationCount++;
		if (CriManaPlugin.initializationCount != 1)
		{
			return;
		}
		if (!CriWareInitializer.IsInitialized())
		{
			Debug.Log("[CRIWARE] CriWareInitializer is not working. Initializes Mana by default parameters.");
		}
		CriFsPlugin.InitializeLibrary();
		CriAtomPlugin.InitializeLibrary();
		CriManaPlugin.criManaUnity_Initialize();
		AutoResisterRendererResourceFactories.InvokeAutoRegister();
	}

	// Token: 0x060003A5 RID: 933 RVA: 0x00008928 File Offset: 0x00006D28
	public static void FinalizeLibrary()
	{
		CriManaPlugin.initializationCount--;
		if (CriManaPlugin.initializationCount < 0)
		{
			Debug.LogError("[CRIWARE] ERROR: Mana library is already finalized.");
			return;
		}
		if (CriManaPlugin.initializationCount != 0)
		{
			return;
		}
		CriDisposableObjectManager.CallOnModuleFinalization(CriDisposableObjectManager.ModuleType.Mana);
		CriManaPlugin.criManaUnity_Finalize();
		RendererResourceFactory.DisposeAllFactories();
		CriAtomPlugin.FinalizeLibrary();
		CriFsPlugin.FinalizeLibrary();
	}

	// Token: 0x060003A6 RID: 934 RVA: 0x0000897C File Offset: 0x00006D7C
	public static void SetDecodeThreadPriorityAndroidExperimental(int prio)
	{
		CriManaPlugin.criManaUnity_SetDecodeThreadPriority_ANDROID(prio);
	}

	// Token: 0x060003A7 RID: 935 RVA: 0x00008984 File Offset: 0x00006D84
	public static void Lock()
	{
		CriManaPlugin.criManaUnity_Lock();
	}

	// Token: 0x060003A8 RID: 936 RVA: 0x0000898B File Offset: 0x00006D8B
	public static void Unlock()
	{
		CriManaPlugin.criManaUnity_Unlock();
	}

	// Token: 0x060003A9 RID: 937
	[DllImport("cri_ware_unity")]
	private static extern void criManaUnity_SetConfigParameters(int graphics_api, bool graphics_multi_threaded, int num_decoders, int num_of_max_entries);

	// Token: 0x060003AA RID: 938
	[DllImport("cri_ware_unity")]
	private static extern void criManaUnity_Initialize();

	// Token: 0x060003AB RID: 939
	[DllImport("cri_ware_unity")]
	private static extern void criManaUnity_Finalize();

	// Token: 0x060003AC RID: 940
	[DllImport("cri_ware_unity")]
	public static extern void criManaUnity_SetDecodeSkipFlag(bool flag);

	// Token: 0x060003AD RID: 941
	[DllImport("cri_ware_unity")]
	public static extern void criManaUnity_Lock();

	// Token: 0x060003AE RID: 942
	[DllImport("cri_ware_unity")]
	public static extern void criManaUnity_Unlock();

	// Token: 0x060003AF RID: 943
	[DllImport("cri_ware_unity")]
	public static extern uint criManaUnity_GetAllocatedHeapSize();

	// Token: 0x060003B0 RID: 944
	[DllImport("cri_ware_unity")]
	public static extern void criManaUnity_SetDecodeThreadPriority_ANDROID(int prio);

	// Token: 0x060003B1 RID: 945
	[DllImport("cri_ware_unity")]
	private static extern void criManaUnity_SetConfigAdditionalParameters_ANDROID(bool enable_h264_playback);

	// Token: 0x04000227 RID: 551
	private static int initializationCount;

	// Token: 0x04000228 RID: 552
	private static bool enabledMultithreadedRendering;

	// Token: 0x04000229 RID: 553
	public static int renderingEventOffset = 1129775104;
}
