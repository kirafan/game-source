﻿using System;
using UnityEngine;
using UnityEngine.UI;

// Token: 0x02000004 RID: 4
public class AppsFlyerTrackerCallbacks : MonoBehaviour
{
	// Token: 0x06000020 RID: 32 RVA: 0x0000290E File Offset: 0x00000D0E
	private void Start()
	{
		MonoBehaviour.print("AppsFlyerTrackerCallbacks on Start");
	}

	// Token: 0x06000021 RID: 33 RVA: 0x0000291A File Offset: 0x00000D1A
	public void didReceiveConversionData(string conversionData)
	{
		if (conversionData == null)
		{
			return;
		}
		this.printCallback("AppsFlyerTrackerCallbacks:: got conversion data = " + conversionData);
	}

	// Token: 0x06000022 RID: 34 RVA: 0x00002934 File Offset: 0x00000D34
	public void didReceiveConversionDataWithError(string error)
	{
		if (error == null)
		{
			return;
		}
		this.printCallback("AppsFlyerTrackerCallbacks:: got conversion data error = " + error);
	}

	// Token: 0x06000023 RID: 35 RVA: 0x0000294E File Offset: 0x00000D4E
	public void didFinishValidateReceipt(string validateResult)
	{
		if (validateResult == null)
		{
			return;
		}
		this.printCallback("AppsFlyerTrackerCallbacks:: got didFinishValidateReceipt  = " + validateResult);
	}

	// Token: 0x06000024 RID: 36 RVA: 0x00002968 File Offset: 0x00000D68
	public void didFinishValidateReceiptWithError(string error)
	{
		if (error == null)
		{
			return;
		}
		this.printCallback("AppsFlyerTrackerCallbacks:: got idFinishValidateReceiptWithError error = " + error);
	}

	// Token: 0x06000025 RID: 37 RVA: 0x00002982 File Offset: 0x00000D82
	public void onAppOpenAttribution(string validateResult)
	{
		if (validateResult == null)
		{
			return;
		}
		this.printCallback("AppsFlyerTrackerCallbacks:: got onAppOpenAttribution  = " + validateResult);
	}

	// Token: 0x06000026 RID: 38 RVA: 0x0000299C File Offset: 0x00000D9C
	public void onAppOpenAttributionFailure(string error)
	{
		if (error == null)
		{
			return;
		}
		this.printCallback("AppsFlyerTrackerCallbacks:: got onAppOpenAttributionFailure error = " + error);
	}

	// Token: 0x06000027 RID: 39 RVA: 0x000029B6 File Offset: 0x00000DB6
	public void onInAppBillingSuccess()
	{
		this.printCallback("AppsFlyerTrackerCallbacks:: got onInAppBillingSuccess succcess");
	}

	// Token: 0x06000028 RID: 40 RVA: 0x000029C3 File Offset: 0x00000DC3
	public void onInAppBillingFailure(string error)
	{
		if (error == null)
		{
			return;
		}
		this.printCallback("AppsFlyerTrackerCallbacks:: got onInAppBillingFailure error = " + error);
	}

	// Token: 0x06000029 RID: 41 RVA: 0x000029DD File Offset: 0x00000DDD
	private void printCallback(string str)
	{
		if (this.callbacks != null && !string.IsNullOrEmpty(str))
		{
			Text text = this.callbacks;
			text.text = text.text + str + "\n";
		}
	}

	// Token: 0x04000044 RID: 68
	public Text callbacks;
}
