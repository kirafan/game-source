﻿using System;
using System.Runtime.InteropServices;

// Token: 0x0200004C RID: 76
public abstract class CriAtomExVoicePool : IDisposable
{
	// Token: 0x060002B3 RID: 691 RVA: 0x00006B40 File Offset: 0x00004F40
	public static CriAtomExVoicePool.UsedVoicesInfo GetNumUsedVoices(CriAtomExVoicePool.VoicePoolId voicePoolId)
	{
		CriAtomExVoicePool.UsedVoicesInfo result;
		CriAtomExVoicePool.criAtomUnity_GetNumUsedVoices((int)voicePoolId, out result.numUsedVoices, out result.numPoolVoices);
		return result;
	}

	// Token: 0x17000027 RID: 39
	// (get) Token: 0x060002B4 RID: 692 RVA: 0x00006B62 File Offset: 0x00004F62
	public IntPtr nativeHandle
	{
		get
		{
			return this._handle;
		}
	}

	// Token: 0x17000028 RID: 40
	// (get) Token: 0x060002B5 RID: 693 RVA: 0x00006B6A File Offset: 0x00004F6A
	public uint identifier
	{
		get
		{
			return this._identifier;
		}
	}

	// Token: 0x17000029 RID: 41
	// (get) Token: 0x060002B6 RID: 694 RVA: 0x00006B72 File Offset: 0x00004F72
	public int numVoices
	{
		get
		{
			return this._numVoices;
		}
	}

	// Token: 0x1700002A RID: 42
	// (get) Token: 0x060002B7 RID: 695 RVA: 0x00006B7A File Offset: 0x00004F7A
	public int maxChannels
	{
		get
		{
			return this._maxChannels;
		}
	}

	// Token: 0x1700002B RID: 43
	// (get) Token: 0x060002B8 RID: 696 RVA: 0x00006B82 File Offset: 0x00004F82
	public int maxSamplingRate
	{
		get
		{
			return this._maxSamplingRate;
		}
	}

	// Token: 0x060002B9 RID: 697 RVA: 0x00006B8A File Offset: 0x00004F8A
	public void Dispose()
	{
		CriDisposableObjectManager.Unregister(this);
		if (this._handle != IntPtr.Zero)
		{
			CriAtomExVoicePool.criAtomExVoicePool_Free(this._handle);
		}
		GC.SuppressFinalize(this);
	}

	// Token: 0x060002BA RID: 698 RVA: 0x00006BBC File Offset: 0x00004FBC
	public CriAtomExVoicePool.UsedVoicesInfo GetNumUsedVoices()
	{
		CriAtomExVoicePool.UsedVoicesInfo result;
		CriAtomExVoicePool.criAtomExVoicePool_GetNumUsedVoices(this._handle, out result.numUsedVoices, out result.numPoolVoices);
		return result;
	}

	// Token: 0x060002BB RID: 699 RVA: 0x00006BE4 File Offset: 0x00004FE4
	public void AttachDspTimeStretch()
	{
		CriAtomExVoicePool.ExTimeStretchConfig exTimeStretchConfig;
		exTimeStretchConfig.numDsp = this._numVoices;
		exTimeStretchConfig.maxChannels = this._maxChannels;
		exTimeStretchConfig.maxSamplingRate = this._maxSamplingRate;
		exTimeStretchConfig.config.reserved = 0;
		CriAtomExVoicePool.criAtomExVoicePool_AttachDspTimeStretch(this._handle, ref exTimeStretchConfig, IntPtr.Zero, 0);
	}

	// Token: 0x060002BC RID: 700 RVA: 0x00006C38 File Offset: 0x00005038
	public void AttachDspPitchShifter(CriAtomExVoicePool.PitchShifterMode mode = CriAtomExVoicePool.PitchShifterMode.Music, int windosSize = 1024, int overlapTimes = 4)
	{
		CriAtomExVoicePool.ExPitchShifterConfig exPitchShifterConfig;
		exPitchShifterConfig.numDsp = this._numVoices;
		exPitchShifterConfig.maxChannels = this._maxChannels;
		exPitchShifterConfig.maxSamplingRate = this._maxSamplingRate;
		exPitchShifterConfig.config.mode = (int)mode;
		exPitchShifterConfig.config.windowSize = windosSize;
		exPitchShifterConfig.config.overlapTimes = overlapTimes;
		CriAtomExVoicePool.criAtomExVoicePool_AttachDspPitchShifter(this._handle, ref exPitchShifterConfig, IntPtr.Zero, 0);
	}

	// Token: 0x060002BD RID: 701 RVA: 0x00006CA6 File Offset: 0x000050A6
	public void DetachDsp()
	{
		CriAtomExVoicePool.criAtomExVoicePool_DetachDsp(this._handle);
	}

	// Token: 0x060002BE RID: 702 RVA: 0x00006CB4 File Offset: 0x000050B4
	~CriAtomExVoicePool()
	{
		this.Dispose();
	}

	// Token: 0x060002BF RID: 703
	[DllImport("cri_ware_unity")]
	private static extern void criAtomUnity_GetNumUsedVoices(int voice_pool_id, out int num_used_voices, out int num_pool_voices);

	// Token: 0x060002C0 RID: 704
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExVoicePool_GetNumUsedVoices(IntPtr pool, out int num_used_voices, out int num_pool_voices);

	// Token: 0x060002C1 RID: 705
	[DllImport("cri_ware_unity")]
	public static extern void criAtomExVoicePool_Free(IntPtr pool);

	// Token: 0x060002C2 RID: 706
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExVoicePool_AttachDspTimeStretch(IntPtr pool, ref CriAtomExVoicePool.ExTimeStretchConfig config, IntPtr work, int work_size);

	// Token: 0x060002C3 RID: 707
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExVoicePool_AttachDspPitchShifter(IntPtr pool, ref CriAtomExVoicePool.ExPitchShifterConfig config, IntPtr work, int work_size);

	// Token: 0x060002C4 RID: 708
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExVoicePool_DetachDsp(IntPtr pool);

	// Token: 0x04000167 RID: 359
	public const int StandardMemoryAsrVoicePoolId = 0;

	// Token: 0x04000168 RID: 360
	public const int StandardStreamingAsrVoicePoolId = 1;

	// Token: 0x04000169 RID: 361
	public const int StandardMemoryNsrVoicePoolId = 2;

	// Token: 0x0400016A RID: 362
	public const int StandardStreamingNsrVoicePoolId = 3;

	// Token: 0x0400016B RID: 363
	protected IntPtr _handle = IntPtr.Zero;

	// Token: 0x0400016C RID: 364
	protected uint _identifier;

	// Token: 0x0400016D RID: 365
	protected int _numVoices;

	// Token: 0x0400016E RID: 366
	protected int _maxChannels;

	// Token: 0x0400016F RID: 367
	protected int _maxSamplingRate;

	// Token: 0x0200004D RID: 77
	public enum VoicePoolId
	{
		// Token: 0x04000171 RID: 369
		StandardMemory,
		// Token: 0x04000172 RID: 370
		StandardStreaming,
		// Token: 0x04000173 RID: 371
		HcaMxMemory = 4,
		// Token: 0x04000174 RID: 372
		HcaMxStreaming,
		// Token: 0x04000175 RID: 373
		LowLatencyMemory = 2,
		// Token: 0x04000176 RID: 374
		LowLatencyStreaming
	}

	// Token: 0x0200004E RID: 78
	public enum PitchShifterMode
	{
		// Token: 0x04000178 RID: 376
		Music,
		// Token: 0x04000179 RID: 377
		Vocal,
		// Token: 0x0400017A RID: 378
		SoundEffect,
		// Token: 0x0400017B RID: 379
		Speech
	}

	// Token: 0x0200004F RID: 79
	public struct UsedVoicesInfo
	{
		// Token: 0x0400017C RID: 380
		public int numUsedVoices;

		// Token: 0x0400017D RID: 381
		public int numPoolVoices;
	}

	// Token: 0x02000050 RID: 80
	protected struct PlayerConfig
	{
		// Token: 0x0400017E RID: 382
		public int maxChannels;

		// Token: 0x0400017F RID: 383
		public int maxSamplingRate;

		// Token: 0x04000180 RID: 384
		public bool streamingFlag;

		// Token: 0x04000181 RID: 385
		public int soundRendererType;

		// Token: 0x04000182 RID: 386
		public int decodeLatency;
	}

	// Token: 0x02000051 RID: 81
	protected struct VoicePoolConfig
	{
		// Token: 0x04000183 RID: 387
		public uint identifier;

		// Token: 0x04000184 RID: 388
		public int numVoices;

		// Token: 0x04000185 RID: 389
		public CriAtomExVoicePool.PlayerConfig playerConfig;
	}

	// Token: 0x02000052 RID: 82
	private struct PitchShifterConfig
	{
		// Token: 0x04000186 RID: 390
		public int mode;

		// Token: 0x04000187 RID: 391
		public int windowSize;

		// Token: 0x04000188 RID: 392
		public int overlapTimes;
	}

	// Token: 0x02000053 RID: 83
	private struct ExPitchShifterConfig
	{
		// Token: 0x04000189 RID: 393
		public int numDsp;

		// Token: 0x0400018A RID: 394
		public int maxChannels;

		// Token: 0x0400018B RID: 395
		public int maxSamplingRate;

		// Token: 0x0400018C RID: 396
		public CriAtomExVoicePool.PitchShifterConfig config;
	}

	// Token: 0x02000054 RID: 84
	private struct TimeStretchConfig
	{
		// Token: 0x0400018D RID: 397
		public int reserved;
	}

	// Token: 0x02000055 RID: 85
	private struct ExTimeStretchConfig
	{
		// Token: 0x0400018E RID: 398
		public int numDsp;

		// Token: 0x0400018F RID: 399
		public int maxChannels;

		// Token: 0x04000190 RID: 400
		public int maxSamplingRate;

		// Token: 0x04000191 RID: 401
		public CriAtomExVoicePool.TimeStretchConfig config;
	}
}
