﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000039 RID: 57
public class CriAtomExAcbLoader : IDisposable
{
	// Token: 0x060001B3 RID: 435 RVA: 0x00005CA4 File Offset: 0x000040A4
	private CriAtomExAcbLoader(IntPtr handle)
	{
		this.handle = handle;
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x060001B4 RID: 436 RVA: 0x00005CBC File Offset: 0x000040BC
	public static CriAtomExAcbLoader LoadAcbFileAsync(CriFsBinder binder, string acbPath, string awbPath, bool loadAwbOnMemory = false)
	{
		IntPtr intPtr = (binder == null) ? IntPtr.Zero : binder.nativeHandle;
		CriAtomExAcbLoader.LoaderConfig loaderConfig = default(CriAtomExAcbLoader.LoaderConfig);
		loaderConfig.shouldLoadAwbOnMemory = loadAwbOnMemory;
		IntPtr intPtr2 = CriAtomExAcbLoader.criAtomExAcbLoader_Create(ref loaderConfig);
		if (intPtr2 == IntPtr.Zero)
		{
			return null;
		}
		if (!CriAtomExAcbLoader.criAtomExAcbLoader_LoadAcbFileAsync(intPtr2, intPtr, acbPath, intPtr, awbPath))
		{
			return null;
		}
		return new CriAtomExAcbLoader(intPtr2);
	}

	// Token: 0x060001B5 RID: 437 RVA: 0x00005D24 File Offset: 0x00004124
	public static CriAtomExAcbLoader LoadAcbDataAsync(byte[] acbData, CriFsBinder awbBinder, string awbPath, bool loadAwbOnMemory = false)
	{
		IntPtr awb_binder = (awbBinder == null) ? IntPtr.Zero : awbBinder.nativeHandle;
		CriAtomExAcbLoader.LoaderConfig loaderConfig = default(CriAtomExAcbLoader.LoaderConfig);
		loaderConfig.shouldLoadAwbOnMemory = loadAwbOnMemory;
		IntPtr intPtr = CriAtomExAcbLoader.criAtomExAcbLoader_Create(ref loaderConfig);
		if (intPtr == IntPtr.Zero)
		{
			return null;
		}
		if (!CriAtomExAcbLoader.criAtomExAcbLoader_LoadAcbDataAsync(intPtr, acbData, acbData.Length, awb_binder, awbPath))
		{
			return null;
		}
		return new CriAtomExAcbLoader(intPtr);
	}

	// Token: 0x060001B6 RID: 438 RVA: 0x00005D8D File Offset: 0x0000418D
	public CriAtomExAcbLoader.Status GetStatus()
	{
		return CriAtomExAcbLoader.criAtomExAcbLoader_GetStatus(this.handle);
	}

	// Token: 0x060001B7 RID: 439 RVA: 0x00005D9C File Offset: 0x0000419C
	public CriAtomExAcb MoveAcb()
	{
		IntPtr value = CriAtomExAcbLoader.criAtomExAcbLoader_MoveAcbHandle(this.handle);
		return (!(value != IntPtr.Zero)) ? null : new CriAtomExAcb(value, null);
	}

	// Token: 0x060001B8 RID: 440 RVA: 0x00005DD2 File Offset: 0x000041D2
	public void Dispose()
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.handle != IntPtr.Zero)
		{
			CriAtomExAcbLoader.criAtomExAcbLoader_Destroy(this.handle);
			this.handle = IntPtr.Zero;
		}
		GC.SuppressFinalize(this);
	}

	// Token: 0x060001B9 RID: 441 RVA: 0x00005E0C File Offset: 0x0000420C
	~CriAtomExAcbLoader()
	{
		this.Dispose();
	}

	// Token: 0x060001BA RID: 442
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExAcbLoader_Create([In] ref CriAtomExAcbLoader.LoaderConfig config);

	// Token: 0x060001BB RID: 443
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAcbLoader_Destroy(IntPtr acb_loader);

	// Token: 0x060001BC RID: 444
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcbLoader_LoadAcbFileAsync(IntPtr acb_loader, IntPtr acb_binder, string acb_path, IntPtr awb_binder, string awb_path);

	// Token: 0x060001BD RID: 445
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcbLoader_LoadAcbDataAsync(IntPtr acb_loader, byte[] acb_data, int acb_size, IntPtr awb_binder, string awb_path);

	// Token: 0x060001BE RID: 446
	[DllImport("cri_ware_unity")]
	private static extern CriAtomExAcbLoader.Status criAtomExAcbLoader_GetStatus(IntPtr acb_loader);

	// Token: 0x060001BF RID: 447
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExAcbLoader_WaitForCompletion(IntPtr acb_loader);

	// Token: 0x060001C0 RID: 448
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExAcbLoader_MoveAcbHandle(IntPtr acb_loader);

	// Token: 0x0400012C RID: 300
	private IntPtr handle;

	// Token: 0x0200003A RID: 58
	public enum Status
	{
		// Token: 0x0400012E RID: 302
		Stop,
		// Token: 0x0400012F RID: 303
		Loading,
		// Token: 0x04000130 RID: 304
		Complete,
		// Token: 0x04000131 RID: 305
		Error
	}

	// Token: 0x0200003B RID: 59
	private struct LoaderConfig
	{
		// Token: 0x04000132 RID: 306
		public bool shouldLoadAwbOnMemory;
	}
}
