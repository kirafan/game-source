﻿using System;
using System.Runtime.InteropServices;

// Token: 0x0200004A RID: 74
public class CriAtomExSoundObject : IDisposable
{
	// Token: 0x060002A6 RID: 678 RVA: 0x00006A28 File Offset: 0x00004E28
	public CriAtomExSoundObject(bool enableVoiceLimitScope, bool enableCategoryCueLimitScope)
	{
		if (!CriAtomPlugin.isInitialized)
		{
			throw new Exception("CriAtomPlugin is not initialized.");
		}
		CriAtomExSoundObject.Config config;
		config.enableVoiceLimitScope = enableVoiceLimitScope;
		config.enableCategoryCueLimitScope = enableCategoryCueLimitScope;
		this.handle = CriAtomExSoundObject.criAtomExSoundObject_Create(ref config, IntPtr.Zero, 0);
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x17000026 RID: 38
	// (get) Token: 0x060002A7 RID: 679 RVA: 0x00006A85 File Offset: 0x00004E85
	public IntPtr nativeHandle
	{
		get
		{
			return this.handle;
		}
	}

	// Token: 0x060002A8 RID: 680 RVA: 0x00006A8D File Offset: 0x00004E8D
	public void Dispose()
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.handle != IntPtr.Zero)
		{
			CriAtomExSoundObject.criAtomExSoundObject_Destroy(this.handle);
			this.handle = IntPtr.Zero;
		}
		GC.SuppressFinalize(this);
	}

	// Token: 0x060002A9 RID: 681 RVA: 0x00006AC7 File Offset: 0x00004EC7
	public void AddPlayer(CriAtomExPlayer player)
	{
		CriAtomExSoundObject.criAtomExSoundObject_AddPlayer(this.handle, player.nativeHandle);
	}

	// Token: 0x060002AA RID: 682 RVA: 0x00006ADA File Offset: 0x00004EDA
	public void DeletePlayer(CriAtomExPlayer player)
	{
		CriAtomExSoundObject.criAtomExSoundObject_DeletePlayer(this.handle, player.nativeHandle);
	}

	// Token: 0x060002AB RID: 683 RVA: 0x00006AED File Offset: 0x00004EED
	public void DeleteAllPlayers()
	{
		CriAtomExSoundObject.criAtomExSoundObject_DeleteAllPlayers(this.handle);
	}

	// Token: 0x060002AC RID: 684 RVA: 0x00006AFC File Offset: 0x00004EFC
	~CriAtomExSoundObject()
	{
		this.Dispose();
	}

	// Token: 0x060002AD RID: 685
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExSoundObject_Create(ref CriAtomExSoundObject.Config config, IntPtr work, int work_size);

	// Token: 0x060002AE RID: 686
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExSoundObject_Destroy(IntPtr soundObject);

	// Token: 0x060002AF RID: 687
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExSoundObject_AddPlayer(IntPtr soundObject, IntPtr player);

	// Token: 0x060002B0 RID: 688
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExSoundObject_DeletePlayer(IntPtr soundObject, IntPtr player);

	// Token: 0x060002B1 RID: 689
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExSoundObject_DeleteAllPlayers(IntPtr soundObject);

	// Token: 0x04000164 RID: 356
	private IntPtr handle = IntPtr.Zero;

	// Token: 0x0200004B RID: 75
	private struct Config
	{
		// Token: 0x04000165 RID: 357
		public bool enableVoiceLimitScope;

		// Token: 0x04000166 RID: 358
		public bool enableCategoryCueLimitScope;
	}
}
