﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;

// Token: 0x0200006A RID: 106
public static class CriFsPlugin
{
	// Token: 0x1700003A RID: 58
	// (get) Token: 0x06000359 RID: 857 RVA: 0x000081A1 File Offset: 0x000065A1
	public static bool isInitialized
	{
		get
		{
			return CriFsPlugin.initializationCount > 0;
		}
	}

	// Token: 0x0600035A RID: 858 RVA: 0x000081AB File Offset: 0x000065AB
	public static void SetConfigParameters(int num_loaders, int num_binders, int num_installers, int argInstallBufferSize, int max_path, bool minimize_file_descriptor_usage)
	{
		CriFsPlugin.criFsUnity_SetConfigParameters(num_loaders, num_binders, num_installers, max_path, minimize_file_descriptor_usage);
		CriFsPlugin.installBufferSize = argInstallBufferSize;
	}

	// Token: 0x0600035B RID: 859 RVA: 0x000081BF File Offset: 0x000065BF
	public static void SetConfigAdditionalParameters_ANDROID(int device_read_bps)
	{
		CriFsPlugin.criFsUnity_SetConfigAdditionalParameters_ANDROID(device_read_bps);
	}

	// Token: 0x0600035C RID: 860 RVA: 0x000081C7 File Offset: 0x000065C7
	public static void SetMemoryFileSystemThreadPriorityExperimentalAndroid(int prio)
	{
		CriFsPlugin.criFsUnity_SetMemoryFileSystemThreadPriority_ANDROID(prio);
	}

	// Token: 0x0600035D RID: 861 RVA: 0x000081CF File Offset: 0x000065CF
	public static void SetDataDecompressionThreadPriorityExperimentalAndroid(int prio)
	{
		CriFsPlugin.criFsUnity_SetDataDecompressionThreadPriority_ANDROID(prio);
	}

	// Token: 0x0600035E RID: 862 RVA: 0x000081D8 File Offset: 0x000065D8
	public static void InitializeLibrary()
	{
		CriFsPlugin.initializationCount++;
		if (CriFsPlugin.initializationCount != 1)
		{
			return;
		}
		if (!CriWareInitializer.IsInitialized())
		{
			Debug.Log("[CRIWARE] CriWareInitializer is not working. Initializes FileSystem by default parameters.");
		}
		CriFsPlugin.criFsUnity_Initialize();
	}

	// Token: 0x0600035F RID: 863 RVA: 0x00008218 File Offset: 0x00006618
	public static void FinalizeLibrary()
	{
		CriFsPlugin.initializationCount--;
		if (CriFsPlugin.initializationCount < 0)
		{
			Debug.LogError("[CRIWARE] ERROR: FileSystem library is already finalized.");
			return;
		}
		if (CriFsPlugin.initializationCount != 0)
		{
			return;
		}
		CriFsServer.DestroyInstance();
		CriFsPlugin.installBufferSize = CriFsPlugin.defaultInstallBufferSize;
		CriDisposableObjectManager.CallOnModuleFinalization(CriDisposableObjectManager.ModuleType.Fs);
		CriFsPlugin.criFsUnity_Finalize();
	}

	// Token: 0x06000360 RID: 864
	[DllImport("cri_ware_unity")]
	private static extern void criFsUnity_SetConfigParameters(int num_loaders, int num_binders, int num_installers, int max_path, bool minimize_file_descriptor_usage);

	// Token: 0x06000361 RID: 865
	[DllImport("cri_ware_unity")]
	private static extern void criFsUnity_Initialize();

	// Token: 0x06000362 RID: 866
	[DllImport("cri_ware_unity")]
	private static extern void criFsUnity_Finalize();

	// Token: 0x06000363 RID: 867
	[DllImport("cri_ware_unity")]
	public static extern uint criFsUnity_GetAllocatedHeapSize();

	// Token: 0x06000364 RID: 868
	[DllImport("cri_ware_unity")]
	public static extern uint criFsLoader_GetRetryCount();

	// Token: 0x06000365 RID: 869
	[DllImport("cri_ware_unity")]
	public static extern int criFs_GetNumBinds(ref int cur, ref int max, ref int limit);

	// Token: 0x06000366 RID: 870
	[DllImport("cri_ware_unity")]
	public static extern int criFs_GetNumUsedLoaders(ref int cur, ref int max, ref int limit);

	// Token: 0x06000367 RID: 871
	[DllImport("cri_ware_unity")]
	public static extern int criFs_GetNumUsedInstallers(ref int cur, ref int max, ref int limit);

	// Token: 0x06000368 RID: 872
	[DllImport("cri_ware_unity")]
	private static extern void criFsUnity_SetConfigAdditionalParameters_ANDROID(int device_read_bps);

	// Token: 0x06000369 RID: 873
	[DllImport("cri_ware_unity")]
	public static extern void criFsUnity_SetMemoryFileSystemThreadPriority_ANDROID(int prio);

	// Token: 0x0600036A RID: 874
	[DllImport("cri_ware_unity")]
	public static extern void criFsUnity_SetDataDecompressionThreadPriority_ANDROID(int prio);

	// Token: 0x040001D0 RID: 464
	private static int initializationCount = 0;

	// Token: 0x040001D1 RID: 465
	public static int defaultInstallBufferSize = 4194304;

	// Token: 0x040001D2 RID: 466
	public static int installBufferSize = CriFsPlugin.defaultInstallBufferSize;
}
