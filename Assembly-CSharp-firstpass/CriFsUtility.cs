﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000069 RID: 105
public static class CriFsUtility
{
	// Token: 0x06000343 RID: 835 RVA: 0x000080A2 File Offset: 0x000064A2
	public static CriFsLoadFileRequest LoadFile(string path, int readUnitSize = 1048576)
	{
		return CriFsServer.instance.LoadFile(null, path, null, readUnitSize);
	}

	// Token: 0x06000344 RID: 836 RVA: 0x000080B2 File Offset: 0x000064B2
	public static CriFsLoadFileRequest LoadFile(string path, CriFsRequest.DoneDelegate doneDelegate, int readUnitSize = 1048576)
	{
		return CriFsServer.instance.LoadFile(null, path, doneDelegate, readUnitSize);
	}

	// Token: 0x06000345 RID: 837 RVA: 0x000080C2 File Offset: 0x000064C2
	public static CriFsLoadFileRequest LoadFile(CriFsBinder binder, string path, int readUnitSize = 1048576)
	{
		return CriFsServer.instance.LoadFile(binder, path, null, readUnitSize);
	}

	// Token: 0x06000346 RID: 838 RVA: 0x000080D2 File Offset: 0x000064D2
	public static CriFsLoadAssetBundleRequest LoadAssetBundle(string path, int readUnitSize = 1048576)
	{
		return CriFsUtility.LoadAssetBundle(null, path, readUnitSize);
	}

	// Token: 0x06000347 RID: 839 RVA: 0x000080DC File Offset: 0x000064DC
	public static CriFsLoadAssetBundleRequest LoadAssetBundle(CriFsBinder binder, string path, int readUnitSize = 1048576)
	{
		return CriFsServer.instance.LoadAssetBundle(binder, path, readUnitSize);
	}

	// Token: 0x06000348 RID: 840 RVA: 0x000080EB File Offset: 0x000064EB
	public static CriFsInstallRequest Install(string srcPath, string dstPath)
	{
		return CriFsUtility.Install(null, srcPath, dstPath, null);
	}

	// Token: 0x06000349 RID: 841 RVA: 0x000080F6 File Offset: 0x000064F6
	public static CriFsInstallRequest Install(string srcPath, string dstPath, CriFsRequest.DoneDelegate doneDeleagate)
	{
		return CriFsUtility.Install(null, srcPath, dstPath, doneDeleagate);
	}

	// Token: 0x0600034A RID: 842 RVA: 0x00008101 File Offset: 0x00006501
	public static CriFsInstallRequest Install(CriFsBinder srcBinder, string srcPath, string dstPath)
	{
		return CriFsServer.instance.Install(srcBinder, srcPath, dstPath, null);
	}

	// Token: 0x0600034B RID: 843 RVA: 0x00008111 File Offset: 0x00006511
	public static CriFsInstallRequest Install(CriFsBinder srcBinder, string srcPath, string dstPath, CriFsRequest.DoneDelegate doneDeleagate)
	{
		return CriFsServer.instance.Install(srcBinder, srcPath, dstPath, doneDeleagate);
	}

	// Token: 0x0600034C RID: 844 RVA: 0x00008121 File Offset: 0x00006521
	public static CriFsInstallRequest WebInstall(string srcPath, string dstPath, CriFsRequest.DoneDelegate doneDeleagate)
	{
		return CriFsServer.instance.WebInstall(srcPath, dstPath, doneDeleagate);
	}

	// Token: 0x0600034D RID: 845 RVA: 0x00008130 File Offset: 0x00006530
	public static CriFsBindRequest BindCpk(CriFsBinder targetBinder, string srcPath)
	{
		return CriFsUtility.BindCpk(targetBinder, null, srcPath);
	}

	// Token: 0x0600034E RID: 846 RVA: 0x0000813A File Offset: 0x0000653A
	public static CriFsBindRequest BindCpk(CriFsBinder targetBinder, CriFsBinder srcBinder, string srcPath)
	{
		return CriFsServer.instance.BindCpk(targetBinder, srcBinder, srcPath);
	}

	// Token: 0x0600034F RID: 847 RVA: 0x00008149 File Offset: 0x00006549
	public static CriFsBindRequest BindDirectory(CriFsBinder targetBinder, string srcPath)
	{
		return CriFsServer.instance.BindDirectory(targetBinder, null, srcPath);
	}

	// Token: 0x06000350 RID: 848 RVA: 0x00008158 File Offset: 0x00006558
	public static CriFsBindRequest BindDirectory(CriFsBinder targetBinder, CriFsBinder srcBinder, string srcPath)
	{
		return CriFsServer.instance.BindDirectory(targetBinder, srcBinder, srcPath);
	}

	// Token: 0x06000351 RID: 849 RVA: 0x00008167 File Offset: 0x00006567
	public static CriFsBindRequest BindFile(CriFsBinder targetBinder, string srcPath)
	{
		return CriFsServer.instance.BindFile(targetBinder, null, srcPath);
	}

	// Token: 0x06000352 RID: 850 RVA: 0x00008176 File Offset: 0x00006576
	public static CriFsBindRequest BindFile(CriFsBinder targetBinder, CriFsBinder srcBinder, string srcPath)
	{
		return CriFsServer.instance.BindFile(targetBinder, srcBinder, srcPath);
	}

	// Token: 0x06000353 RID: 851 RVA: 0x00008185 File Offset: 0x00006585
	public static void SetUserAgentString(string userAgentString)
	{
		CriFsUtility.criFsUnity_SetUserAgentString(userAgentString);
	}

	// Token: 0x06000354 RID: 852 RVA: 0x0000818E File Offset: 0x0000658E
	public static void SetProxyServer(string proxyPath, ushort proxyPort)
	{
		CriFsUtility.criFsUnity_SetProxyServer(proxyPath, proxyPort);
	}

	// Token: 0x06000355 RID: 853 RVA: 0x00008198 File Offset: 0x00006598
	public static void SetPathSeparator(string filter)
	{
		CriFsUtility.criFsUnity_SetPathSeparator(filter);
	}

	// Token: 0x06000356 RID: 854
	[DllImport("cri_ware_unity")]
	private static extern bool criFsUnity_SetUserAgentString(string userAgentString);

	// Token: 0x06000357 RID: 855
	[DllImport("cri_ware_unity")]
	private static extern bool criFsUnity_SetProxyServer(string proxyPath, ushort proxyPort);

	// Token: 0x06000358 RID: 856
	[DllImport("cri_ware_unity")]
	private static extern bool criFsUnity_SetPathSeparator(string filter);

	// Token: 0x040001CF RID: 463
	public const int DefaultReadUnitSize = 1048576;
}
