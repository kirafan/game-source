﻿using System;
using UnityEngine;

// Token: 0x020000DA RID: 218
public class WebViewObject : MonoBehaviour
{
	// Token: 0x06000650 RID: 1616 RVA: 0x00012E18 File Offset: 0x00011218
	public void SetKeyboardVisible(string pIsVisible)
	{
		this.mIsKeyboardVisible = (pIsVisible == "true");
	}

	// Token: 0x1700006B RID: 107
	// (get) Token: 0x06000651 RID: 1617 RVA: 0x00012E2B File Offset: 0x0001122B
	public bool IsKeyboardVisible
	{
		get
		{
			return this.mIsKeyboardVisible;
		}
	}

	// Token: 0x06000652 RID: 1618 RVA: 0x00012E34 File Offset: 0x00011234
	public void Init(Action<string> cb = null, bool transparent = false, string ua = "Mozilla/5.0 (iPhone; CPU iPhone OS 7_1_2 like Mac OS X) AppleWebKit/537.51.2 (KHTML, like Gecko) Version/7.0 Mobile/11D257 Safari/9537.53", Action<string> err = null, Action<string> ld = null, bool enableWKWebView = false)
	{
		this.onJS = cb;
		this.onError = err;
		this.onLoaded = ld;
		this.webView = new AndroidJavaObject("net.gree.unitywebview.CWebViewPlugin", new object[0]);
		this.webView.Call("Init", new object[]
		{
			base.name,
			transparent
		});
	}

	// Token: 0x06000653 RID: 1619 RVA: 0x00012E96 File Offset: 0x00011296
	protected virtual void OnDestroy()
	{
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("Destroy", new object[0]);
		this.webView = null;
	}

	// Token: 0x06000654 RID: 1620 RVA: 0x00012EC1 File Offset: 0x000112C1
	public void SetCenterPositionWithScale(Vector2 center, Vector2 scale)
	{
	}

	// Token: 0x06000655 RID: 1621 RVA: 0x00012EC4 File Offset: 0x000112C4
	public void SetMargins(int left, int top, int right, int bottom)
	{
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("SetMargins", new object[]
		{
			left,
			top,
			right,
			bottom
		});
	}

	// Token: 0x06000656 RID: 1622 RVA: 0x00012F18 File Offset: 0x00011318
	public void SetVisibility(bool v)
	{
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("SetVisibility", new object[]
		{
			v
		});
		this.visibility = v;
	}

	// Token: 0x06000657 RID: 1623 RVA: 0x00012F4C File Offset: 0x0001134C
	public bool GetVisibility()
	{
		return this.visibility;
	}

	// Token: 0x06000658 RID: 1624 RVA: 0x00012F54 File Offset: 0x00011354
	public void LoadURL(string url)
	{
		if (string.IsNullOrEmpty(url))
		{
			return;
		}
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("LoadURL", new object[]
		{
			url
		});
	}

	// Token: 0x06000659 RID: 1625 RVA: 0x00012F88 File Offset: 0x00011388
	public void LoadHTML(string html, string baseUrl)
	{
		if (string.IsNullOrEmpty(html))
		{
			return;
		}
		if (string.IsNullOrEmpty(baseUrl))
		{
			baseUrl = string.Empty;
		}
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("LoadHTML", new object[]
		{
			html,
			baseUrl
		});
	}

	// Token: 0x0600065A RID: 1626 RVA: 0x00012FDD File Offset: 0x000113DD
	public void EvaluateJS(string js)
	{
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("LoadURL", new object[]
		{
			"javascript:" + js
		});
	}

	// Token: 0x0600065B RID: 1627 RVA: 0x0001300F File Offset: 0x0001140F
	public bool CanGoBack()
	{
		return this.webView != null && this.webView.Get<bool>("canGoBack");
	}

	// Token: 0x0600065C RID: 1628 RVA: 0x0001302E File Offset: 0x0001142E
	public bool CanGoForward()
	{
		return this.webView != null && this.webView.Get<bool>("canGoForward");
	}

	// Token: 0x0600065D RID: 1629 RVA: 0x0001304D File Offset: 0x0001144D
	public void GoBack()
	{
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("GoBack", new object[0]);
	}

	// Token: 0x0600065E RID: 1630 RVA: 0x00013071 File Offset: 0x00011471
	public void GoForward()
	{
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("GoForward", new object[0]);
	}

	// Token: 0x0600065F RID: 1631 RVA: 0x00013095 File Offset: 0x00011495
	public void CallOnError(string error)
	{
		if (this.onError != null)
		{
			this.onError(error);
		}
	}

	// Token: 0x06000660 RID: 1632 RVA: 0x000130AE File Offset: 0x000114AE
	public void CallOnLoaded(string url)
	{
		if (this.onLoaded != null)
		{
			this.onLoaded(url);
		}
	}

	// Token: 0x06000661 RID: 1633 RVA: 0x000130C7 File Offset: 0x000114C7
	public void CallFromJS(string message)
	{
		if (this.onJS != null)
		{
			this.onJS(message);
		}
	}

	// Token: 0x06000662 RID: 1634 RVA: 0x000130E0 File Offset: 0x000114E0
	public void AddCustomHeader(string headerKey, string headerValue)
	{
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("AddCustomHeader", new object[]
		{
			headerKey,
			headerValue
		});
	}

	// Token: 0x06000663 RID: 1635 RVA: 0x0001310C File Offset: 0x0001150C
	public string GetCustomHeaderValue(string headerKey)
	{
		if (this.webView == null)
		{
			return null;
		}
		return this.webView.Call<string>("GetCustomHeaderValue", new object[]
		{
			headerKey
		});
	}

	// Token: 0x06000664 RID: 1636 RVA: 0x00013135 File Offset: 0x00011535
	public void RemoveCustomHeader(string headerKey)
	{
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("RemoveCustomHeader", new object[]
		{
			headerKey
		});
	}

	// Token: 0x06000665 RID: 1637 RVA: 0x0001315D File Offset: 0x0001155D
	public void ClearCustomHeader()
	{
		if (this.webView == null)
		{
			return;
		}
		this.webView.Call("ClearCustomHeader", new object[0]);
	}

	// Token: 0x040003F3 RID: 1011
	private Action<string> onJS;

	// Token: 0x040003F4 RID: 1012
	private Action<string> onError;

	// Token: 0x040003F5 RID: 1013
	private Action<string> onLoaded;

	// Token: 0x040003F6 RID: 1014
	private bool visibility;

	// Token: 0x040003F7 RID: 1015
	private AndroidJavaObject webView;

	// Token: 0x040003F8 RID: 1016
	private bool mIsKeyboardVisible;
}
