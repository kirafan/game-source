﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000058 RID: 88
public class CriFsLoader : IDisposable
{
	// Token: 0x060002C9 RID: 713 RVA: 0x00006E7C File Offset: 0x0000527C
	public CriFsLoader()
	{
		if (!CriFsPlugin.isInitialized)
		{
			throw new Exception("CriFsPlugin is not initialized.");
		}
		this.handle = IntPtr.Zero;
		CriFsLoader.criFsLoader_Create(out this.handle);
		if (this.handle == IntPtr.Zero)
		{
			throw new Exception("criFsLoader_Create() failed.");
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Fs);
	}

	// Token: 0x060002CA RID: 714 RVA: 0x00006EE4 File Offset: 0x000052E4
	public void Dispose()
	{
		CriDisposableObjectManager.Unregister(this);
		if (this.handle != IntPtr.Zero)
		{
			CriFsLoader.criFsLoader_Destroy(this.handle);
			this.handle = IntPtr.Zero;
		}
		if (this.dstGch.IsAllocated)
		{
			this.dstGch.Free();
		}
		if (this.srcGch.IsAllocated)
		{
			this.srcGch.Free();
		}
		GC.SuppressFinalize(this);
	}

	// Token: 0x060002CB RID: 715 RVA: 0x00006F60 File Offset: 0x00005360
	public void Load(CriFsBinder binder, string path, long fileOffset, long loadSize, byte[] buffer)
	{
		this.dstGch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
		CriFsLoader.criFsLoader_Load(this.handle, (binder == null) ? IntPtr.Zero : binder.nativeHandle, path, fileOffset, loadSize, this.dstGch.AddrOfPinnedObject(), (long)buffer.Length);
	}

	// Token: 0x060002CC RID: 716 RVA: 0x00006FB4 File Offset: 0x000053B4
	public void LoadById(CriFsBinder binder, int id, long fileOffset, long loadSize, byte[] buffer)
	{
		this.dstGch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
		CriFsLoader.criFsLoader_LoadById(this.handle, (binder == null) ? IntPtr.Zero : binder.nativeHandle, id, fileOffset, loadSize, this.dstGch.AddrOfPinnedObject(), (long)buffer.Length);
	}

	// Token: 0x060002CD RID: 717 RVA: 0x00007008 File Offset: 0x00005408
	public void LoadWithoutDecompression(CriFsBinder binder, string path, long fileOffset, long loadSize, byte[] buffer)
	{
		this.dstGch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
		CriFsLoader.criFsLoader_LoadWithoutDecompression(this.handle, (binder == null) ? IntPtr.Zero : binder.nativeHandle, path, fileOffset, loadSize, this.dstGch.AddrOfPinnedObject(), (long)buffer.Length);
	}

	// Token: 0x060002CE RID: 718 RVA: 0x0000705C File Offset: 0x0000545C
	public void LoadWithoutDecompressionById(CriFsBinder binder, int id, long fileOffset, long loadSize, byte[] buffer)
	{
		this.dstGch = GCHandle.Alloc(buffer, GCHandleType.Pinned);
		CriFsLoader.criFsLoader_LoadWithoutDecompressionById(this.handle, (binder == null) ? IntPtr.Zero : binder.nativeHandle, id, fileOffset, loadSize, this.dstGch.AddrOfPinnedObject(), (long)buffer.Length);
	}

	// Token: 0x060002CF RID: 719 RVA: 0x000070B0 File Offset: 0x000054B0
	public void DecompressData(long srcSize, byte[] srcBuffer, long dstSize, byte[] dstBuffer)
	{
		this.srcGch = GCHandle.Alloc(srcBuffer, GCHandleType.Pinned);
		this.dstGch = GCHandle.Alloc(dstBuffer, GCHandleType.Pinned);
		CriFsLoader.criFsLoader_DecompressData(this.handle, this.srcGch.AddrOfPinnedObject(), srcSize, this.dstGch.AddrOfPinnedObject(), dstSize);
	}

	// Token: 0x060002D0 RID: 720 RVA: 0x000070FC File Offset: 0x000054FC
	public void Stop()
	{
		if (this.handle != IntPtr.Zero)
		{
			CriFsLoader.criFsLoader_Stop(this.handle);
		}
	}

	// Token: 0x060002D1 RID: 721 RVA: 0x00007120 File Offset: 0x00005520
	public CriFsLoader.Status GetStatus()
	{
		CriFsLoader.Status status = CriFsLoader.Status.Stop;
		if (this.handle != IntPtr.Zero)
		{
			CriFsLoader.criFsLoader_GetStatus(this.handle, out status);
		}
		if (status != CriFsLoader.Status.Loading)
		{
			if (this.dstGch.IsAllocated)
			{
				this.dstGch.Free();
			}
			if (this.srcGch.IsAllocated)
			{
				this.srcGch.Free();
			}
		}
		return status;
	}

	// Token: 0x060002D2 RID: 722 RVA: 0x00007190 File Offset: 0x00005590
	public void SetReadUnitSize(int unit_size)
	{
		if (this.handle != IntPtr.Zero)
		{
			CriFsLoader.criFsLoader_SetReadUnitSize(this.handle, (long)unit_size);
		}
	}

	// Token: 0x060002D3 RID: 723 RVA: 0x000071B8 File Offset: 0x000055B8
	~CriFsLoader()
	{
		this.Dispose();
	}

	// Token: 0x060002D4 RID: 724
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_Create(out IntPtr loader);

	// Token: 0x060002D5 RID: 725
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_Destroy(IntPtr loader);

	// Token: 0x060002D6 RID: 726
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_Load(IntPtr loader, IntPtr binder, string path, long offset, long load_size, IntPtr buffer, long buffer_size);

	// Token: 0x060002D7 RID: 727
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_LoadById(IntPtr loader, IntPtr binder, int id, long offset, long load_size, IntPtr buffer, long buffer_size);

	// Token: 0x060002D8 RID: 728
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_Stop(IntPtr loader);

	// Token: 0x060002D9 RID: 729
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_GetStatus(IntPtr loader, out CriFsLoader.Status status);

	// Token: 0x060002DA RID: 730
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_SetReadUnitSize(IntPtr loader, long unit_size);

	// Token: 0x060002DB RID: 731
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_LoadWithoutDecompression(IntPtr loader, IntPtr binder, string path, long offset, long load_size, IntPtr buffer, long buffer_size);

	// Token: 0x060002DC RID: 732
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_LoadWithoutDecompressionById(IntPtr loader, IntPtr binder, int id, long offset, long load_size, IntPtr buffer, long buffer_size);

	// Token: 0x060002DD RID: 733
	[DllImport("cri_ware_unity")]
	private static extern int criFsLoader_DecompressData(IntPtr loader, IntPtr src, long src_size, IntPtr dst, long dst_size);

	// Token: 0x04000192 RID: 402
	private IntPtr handle;

	// Token: 0x04000193 RID: 403
	private GCHandle dstGch;

	// Token: 0x04000194 RID: 404
	private GCHandle srcGch;

	// Token: 0x02000059 RID: 89
	public enum Status
	{
		// Token: 0x04000196 RID: 406
		Stop,
		// Token: 0x04000197 RID: 407
		Loading,
		// Token: 0x04000198 RID: 408
		Complete,
		// Token: 0x04000199 RID: 409
		Error
	}
}
