﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x02000003 RID: 3
public class AppsFlyer : MonoBehaviour
{
	// Token: 0x06000003 RID: 3 RVA: 0x00002060 File Offset: 0x00000460
	public static void trackEvent(string eventName, string eventValue)
	{
		MonoBehaviour.print("AF.cs this is deprecated method. please use trackRichEvent instead. nothing is sent.");
	}

	// Token: 0x06000004 RID: 4 RVA: 0x0000206C File Offset: 0x0000046C
	public static void setCurrencyCode(string currencyCode)
	{
		AppsFlyer.cls_AppsFlyer.Call("setCurrencyCode", new object[]
		{
			currencyCode
		});
	}

	// Token: 0x06000005 RID: 5 RVA: 0x00002087 File Offset: 0x00000487
	public static void setCustomerUserID(string customerUserID)
	{
		AppsFlyer.cls_AppsFlyer.Call("setAppUserId", new object[]
		{
			customerUserID
		});
	}

	// Token: 0x06000006 RID: 6 RVA: 0x000020A4 File Offset: 0x000004A4
	public static void loadConversionData(string callbackObject)
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				AppsFlyer.cls_AppsFlyerHelper.CallStatic("createConversionDataListener", new object[]
				{
					@static,
					callbackObject
				});
			}
		}
	}

	// Token: 0x06000007 RID: 7 RVA: 0x00002128 File Offset: 0x00000528
	[Obsolete("Use loadConversionData(string callbackObject)")]
	public static void loadConversionData(string callbackObject, string callbackMethod, string callbackFailedMethod)
	{
		AppsFlyer.loadConversionData(callbackObject);
	}

	// Token: 0x06000008 RID: 8 RVA: 0x00002130 File Offset: 0x00000530
	public static void setCollectIMEI(bool shouldCollect)
	{
		AppsFlyer.cls_AppsFlyer.Call("setCollectIMEI", new object[]
		{
			shouldCollect
		});
	}

	// Token: 0x06000009 RID: 9 RVA: 0x00002150 File Offset: 0x00000550
	public static void setCollectAndroidID(bool shouldCollect)
	{
		MonoBehaviour.print("AF.cs setCollectAndroidID");
		AppsFlyer.cls_AppsFlyer.Call("setCollectAndroidID", new object[]
		{
			shouldCollect
		});
	}

	// Token: 0x0600000A RID: 10 RVA: 0x0000217A File Offset: 0x0000057A
	public static void init(string key, string callbackObject)
	{
		AppsFlyer.init(key);
		if (callbackObject != null)
		{
			AppsFlyer.loadConversionData(callbackObject);
		}
	}

	// Token: 0x0600000B RID: 11 RVA: 0x00002190 File Offset: 0x00000590
	public static void init(string key)
	{
		MonoBehaviour.print("AF.cs init");
		AppsFlyer.devKey = key;
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				@static.Call("runOnUiThread", new object[]
				{
					new AndroidJavaRunnable(AppsFlyer.init_cb)
				});
			}
		}
	}

	// Token: 0x0600000C RID: 12 RVA: 0x00002224 File Offset: 0x00000624
	private static void init_cb()
	{
		MonoBehaviour.print("AF.cs start tracking");
		AppsFlyer.trackAppLaunch();
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				AndroidJavaObject androidJavaObject = @static.Call<AndroidJavaObject>("getApplication", new object[0]);
				AppsFlyer.cls_AppsFlyer.Call("startTracking", new object[]
				{
					androidJavaObject,
					AppsFlyer.devKey
				});
			}
		}
	}

	// Token: 0x0600000D RID: 13 RVA: 0x000022CC File Offset: 0x000006CC
	public static void setAppsFlyerKey(string key)
	{
		MonoBehaviour.print("AF.cs setAppsFlyerKey");
	}

	// Token: 0x0600000E RID: 14 RVA: 0x000022D8 File Offset: 0x000006D8
	public static void trackAppLaunch()
	{
		MonoBehaviour.print("AF.cs trackAppLaunch");
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				AppsFlyer.cls_AppsFlyer.Call("trackAppLaunch", new object[]
				{
					@static,
					AppsFlyer.devKey
				});
			}
		}
	}

	// Token: 0x0600000F RID: 15 RVA: 0x00002368 File Offset: 0x00000768
	public static void setAppID(string packageName)
	{
		AppsFlyer.cls_AppsFlyer.Call("setAppId", new object[]
		{
			packageName
		});
	}

	// Token: 0x06000010 RID: 16 RVA: 0x00002384 File Offset: 0x00000784
	public static void createValidateInAppListener(string aObject, string callbackMethod, string callbackFailedMethod)
	{
		MonoBehaviour.print("AF.cs createValidateInAppListener called");
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				AppsFlyer.cls_AppsFlyerHelper.CallStatic("createValidateInAppListener", new object[]
				{
					@static,
					aObject,
					callbackMethod,
					callbackFailedMethod
				});
			}
		}
	}

	// Token: 0x06000011 RID: 17 RVA: 0x00002418 File Offset: 0x00000818
	public static void validateReceipt(string publicKey, string purchaseData, string signature, string price, string currency, Dictionary<string, string> extraParams)
	{
		MonoBehaviour.print(string.Concat(new string[]
		{
			"AF.cs validateReceipt pk = ",
			publicKey,
			" data = ",
			purchaseData,
			"sig = ",
			signature
		}));
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				AndroidJavaObject androidJavaObject = null;
				if (extraParams != null)
				{
					androidJavaObject = AppsFlyer.ConvertHashMap(extraParams);
				}
				MonoBehaviour.print("inside cls_activity");
				AppsFlyer.cls_AppsFlyer.Call("validateAndTrackInAppPurchase", new object[]
				{
					@static,
					publicKey,
					signature,
					purchaseData,
					price,
					currency,
					androidJavaObject
				});
			}
		}
	}

	// Token: 0x06000012 RID: 18 RVA: 0x00002500 File Offset: 0x00000900
	public static void trackRichEvent(string eventName, Dictionary<string, string> eventValues)
	{
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				AndroidJavaObject androidJavaObject = AppsFlyer.ConvertHashMap(eventValues);
				AppsFlyer.cls_AppsFlyer.Call("trackEvent", new object[]
				{
					@static,
					eventName,
					androidJavaObject
				});
			}
		}
	}

	// Token: 0x06000013 RID: 19 RVA: 0x0000258C File Offset: 0x0000098C
	private static AndroidJavaObject ConvertHashMap(Dictionary<string, string> dict)
	{
		AndroidJavaObject androidJavaObject = new AndroidJavaObject("java.util.HashMap", new object[0]);
		IntPtr methodID = AndroidJNIHelper.GetMethodID(androidJavaObject.GetRawClass(), "put", "(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;");
		object[] array = new object[2];
		foreach (KeyValuePair<string, string> keyValuePair in dict)
		{
			using (AndroidJavaObject androidJavaObject2 = new AndroidJavaObject("java.lang.String", new object[]
			{
				keyValuePair.Key
			}))
			{
				using (AndroidJavaObject androidJavaObject3 = new AndroidJavaObject("java.lang.String", new object[]
				{
					keyValuePair.Value
				}))
				{
					array[0] = androidJavaObject2;
					array[1] = androidJavaObject3;
					AndroidJNI.CallObjectMethod(androidJavaObject.GetRawObject(), methodID, AndroidJNIHelper.CreateJNIArgArray(array));
				}
			}
		}
		return androidJavaObject;
	}

	// Token: 0x06000014 RID: 20 RVA: 0x000026A4 File Offset: 0x00000AA4
	public static void setImeiData(string imeiData)
	{
		MonoBehaviour.print("AF.cs setImeiData");
		AppsFlyer.cls_AppsFlyer.Call("setImeiData", new object[]
		{
			imeiData
		});
	}

	// Token: 0x06000015 RID: 21 RVA: 0x000026C9 File Offset: 0x00000AC9
	public static void setAndroidIdData(string androidIdData)
	{
		MonoBehaviour.print("AF.cs setImeiData");
		AppsFlyer.cls_AppsFlyer.Call("setAndroidIdData", new object[]
		{
			androidIdData
		});
	}

	// Token: 0x06000016 RID: 22 RVA: 0x000026EE File Offset: 0x00000AEE
	public static void setIsDebug(bool isDebug)
	{
		MonoBehaviour.print("AF.cs setDebugLog");
		AppsFlyer.cls_AppsFlyer.Call("setDebugLog", new object[]
		{
			isDebug
		});
	}

	// Token: 0x06000017 RID: 23 RVA: 0x00002718 File Offset: 0x00000B18
	public static void setIsSandbox(bool isSandbox)
	{
	}

	// Token: 0x06000018 RID: 24 RVA: 0x0000271A File Offset: 0x00000B1A
	public static void getConversionData()
	{
	}

	// Token: 0x06000019 RID: 25 RVA: 0x0000271C File Offset: 0x00000B1C
	public static void handleOpenUrl(string url, string sourceApplication, string annotation)
	{
	}

	// Token: 0x0600001A RID: 26 RVA: 0x00002720 File Offset: 0x00000B20
	public static string getAppsFlyerId()
	{
		string result;
		using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				result = AppsFlyer.cls_AppsFlyer.Call<string>("getAppsFlyerUID", new object[]
				{
					@static
				});
			}
		}
		return result;
	}

	// Token: 0x0600001B RID: 27 RVA: 0x000027A0 File Offset: 0x00000BA0
	public static void setGCMProjectNumber(string googleGCMNumber)
	{
		AppsFlyer.cls_AppsFlyer.Call("setGCMProjectNumber", new object[]
		{
			googleGCMNumber
		});
	}

	// Token: 0x0600001C RID: 28 RVA: 0x000027BC File Offset: 0x00000BBC
	public static void updateServerUninstallToken(string token)
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.appsflyer.AppsFlyerLib");
		AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("getInstance", new object[0]);
		using (AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
		{
			using (AndroidJavaObject @static = androidJavaClass2.GetStatic<AndroidJavaObject>("currentActivity"))
			{
				androidJavaObject.Call("updateServerUninstallToken", new object[]
				{
					@static,
					token
				});
			}
		}
	}

	// Token: 0x0600001D RID: 29 RVA: 0x00002858 File Offset: 0x00000C58
	public static void enableUninstallTracking(string senderId)
	{
		AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.appsflyer.AppsFlyerLib");
		AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("getInstance", new object[0]);
		androidJavaObject.Call("updateServerUninstallToken", new object[]
		{
			senderId
		});
	}

	// Token: 0x0400003E RID: 62
	private static AndroidJavaClass obj = new AndroidJavaClass("com.appsflyer.AppsFlyerLib");

	// Token: 0x0400003F RID: 63
	private static AndroidJavaObject cls_AppsFlyer = AppsFlyer.obj.CallStatic<AndroidJavaObject>("getInstance", new object[0]);

	// Token: 0x04000040 RID: 64
	private static AndroidJavaClass propertiesClass = new AndroidJavaClass("com.appsflyer.AppsFlyerProperties");

	// Token: 0x04000041 RID: 65
	private static AndroidJavaObject afPropertiesInstance = AppsFlyer.propertiesClass.CallStatic<AndroidJavaObject>("getInstance", new object[0]);

	// Token: 0x04000042 RID: 66
	private static AndroidJavaClass cls_AppsFlyerHelper = new AndroidJavaClass("com.appsflyer.AppsFlyerUnityHelper");

	// Token: 0x04000043 RID: 67
	private static string devKey;
}
