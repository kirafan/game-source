﻿using System;
using System.Collections.Generic;
using UnityEngine;

// Token: 0x0200006B RID: 107
public class CriFsServer : MonoBehaviour
{
	// Token: 0x1700003B RID: 59
	// (get) Token: 0x0600036D RID: 877 RVA: 0x00008290 File Offset: 0x00006690
	public static CriFsServer instance
	{
		get
		{
			CriFsServer.CreateInstance();
			return CriFsServer._instance;
		}
	}

	// Token: 0x1700003C RID: 60
	// (get) Token: 0x0600036E RID: 878 RVA: 0x0000829C File Offset: 0x0000669C
	// (set) Token: 0x0600036F RID: 879 RVA: 0x000082A4 File Offset: 0x000066A4
	public int installBufferSize { get; private set; }

	// Token: 0x06000370 RID: 880 RVA: 0x000082AD File Offset: 0x000066AD
	public static void CreateInstance()
	{
		if (CriFsServer._instance == null)
		{
			CriWare.managerObject.AddComponent<CriFsServer>();
			CriFsServer._instance.installBufferSize = CriFsPlugin.installBufferSize;
		}
	}

	// Token: 0x06000371 RID: 881 RVA: 0x000082D9 File Offset: 0x000066D9
	public static void DestroyInstance()
	{
		if (CriFsServer._instance != null)
		{
			UnityEngine.Object.Destroy(CriFsServer._instance);
		}
	}

	// Token: 0x06000372 RID: 882 RVA: 0x000082F8 File Offset: 0x000066F8
	private void Awake()
	{
		if (CriFsServer._instance == null)
		{
			CriFsServer._instance = this;
			this.requestList = new List<CriFsRequest>();
			CriFsRequest item = new CriFsRequest();
			this.requestList.Add(item);
			this.requestList.RemoveAt(0);
		}
		else
		{
			UnityEngine.Object.Destroy(this);
		}
	}

	// Token: 0x06000373 RID: 883 RVA: 0x00008350 File Offset: 0x00006750
	private void OnDestroy()
	{
		if (CriFsServer._instance == this)
		{
			foreach (CriFsRequest criFsRequest in this.requestList)
			{
				criFsRequest.Dispose();
			}
			CriFsServer._instance = null;
		}
	}

	// Token: 0x06000374 RID: 884 RVA: 0x000083C4 File Offset: 0x000067C4
	private void Update()
	{
		CriFsInstaller.ExecuteMain();
		if (CriFsWebInstaller.isInitialized)
		{
			CriFsWebInstaller.ExecuteMain();
		}
		for (int i = 0; i < this.requestList.Count; i++)
		{
			CriFsRequest criFsRequest = this.requestList[i];
			criFsRequest.Update();
		}
		this.requestList.RemoveAll((CriFsRequest request) => request.isDone || request.isDisposed);
	}

	// Token: 0x06000375 RID: 885 RVA: 0x0000843D File Offset: 0x0000683D
	public void AddRequest(CriFsRequest request)
	{
		this.requestList.Add(request);
	}

	// Token: 0x06000376 RID: 886 RVA: 0x0000844C File Offset: 0x0000684C
	public CriFsLoadFileRequest LoadFile(CriFsBinder binder, string path, CriFsRequest.DoneDelegate doneDelegate, int readUnitSize)
	{
		CriFsLoadFileRequest criFsLoadFileRequest = new CriFsLoadFileRequest(binder, path, doneDelegate, readUnitSize);
		this.AddRequest(criFsLoadFileRequest);
		return criFsLoadFileRequest;
	}

	// Token: 0x06000377 RID: 887 RVA: 0x0000846C File Offset: 0x0000686C
	public CriFsLoadAssetBundleRequest LoadAssetBundle(CriFsBinder binder, string path, int readUnitSize)
	{
		CriFsLoadAssetBundleRequest criFsLoadAssetBundleRequest = new CriFsLoadAssetBundleRequest(binder, path, readUnitSize);
		this.AddRequest(criFsLoadAssetBundleRequest);
		return criFsLoadAssetBundleRequest;
	}

	// Token: 0x06000378 RID: 888 RVA: 0x0000848C File Offset: 0x0000688C
	public CriFsInstallRequest Install(CriFsBinder srcBinder, string srcPath, string dstPath, CriFsRequest.DoneDelegate doneDelegate)
	{
		CriFsInstallRequestLegacy criFsInstallRequestLegacy = new CriFsInstallRequestLegacy(srcBinder, srcPath, dstPath, doneDelegate, this.installBufferSize);
		this.requestList.Add(criFsInstallRequestLegacy);
		return criFsInstallRequestLegacy;
	}

	// Token: 0x06000379 RID: 889 RVA: 0x000084B8 File Offset: 0x000068B8
	public CriFsInstallRequest WebInstall(string srcPath, string dstPath, CriFsRequest.DoneDelegate doneDelegate)
	{
		CriFsWebInstallRequest criFsWebInstallRequest = new CriFsWebInstallRequest(srcPath, dstPath, doneDelegate);
		this.requestList.Add(criFsWebInstallRequest);
		return criFsWebInstallRequest;
	}

	// Token: 0x0600037A RID: 890 RVA: 0x000084DC File Offset: 0x000068DC
	public CriFsBindRequest BindCpk(CriFsBinder targetBinder, CriFsBinder srcBinder, string path)
	{
		CriFsBindRequest criFsBindRequest = new CriFsBindRequest(CriFsBindRequest.BindType.Cpk, targetBinder, srcBinder, path);
		this.AddRequest(criFsBindRequest);
		return criFsBindRequest;
	}

	// Token: 0x0600037B RID: 891 RVA: 0x000084FC File Offset: 0x000068FC
	public CriFsBindRequest BindDirectory(CriFsBinder targetBinder, CriFsBinder srcBinder, string path)
	{
		CriFsBindRequest criFsBindRequest = new CriFsBindRequest(CriFsBindRequest.BindType.Directory, targetBinder, srcBinder, path);
		this.AddRequest(criFsBindRequest);
		return criFsBindRequest;
	}

	// Token: 0x0600037C RID: 892 RVA: 0x0000851C File Offset: 0x0000691C
	public CriFsBindRequest BindFile(CriFsBinder targetBinder, CriFsBinder srcBinder, string path)
	{
		CriFsBindRequest criFsBindRequest = new CriFsBindRequest(CriFsBindRequest.BindType.File, targetBinder, srcBinder, path);
		this.AddRequest(criFsBindRequest);
		return criFsBindRequest;
	}

	// Token: 0x040001D3 RID: 467
	private static CriFsServer _instance;

	// Token: 0x040001D4 RID: 468
	private List<CriFsRequest> requestList;
}
