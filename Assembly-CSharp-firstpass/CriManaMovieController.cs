﻿using System;
using UnityEngine;

// Token: 0x0200007B RID: 123
[AddComponentMenu("CRIWARE/CriManaMovieController")]
public class CriManaMovieController : CriManaMovieMaterial
{
	// Token: 0x060003BA RID: 954 RVA: 0x00009348 File Offset: 0x00007748
	protected override void Start()
	{
		base.Start();
		if (this.target == null)
		{
			this.target = base.gameObject.GetComponent<Renderer>();
		}
		if (this.target == null)
		{
			Debug.LogError("[CRIWARE] error");
			UnityEngine.Object.Destroy(this);
			return;
		}
		this.originalMaterial = this.target.material;
		if (!this.useOriginalMaterial)
		{
			this.target.enabled = false;
		}
	}

	// Token: 0x060003BB RID: 955 RVA: 0x000093C8 File Offset: 0x000077C8
	protected override void Update()
	{
		base.Update();
		if (this.renderMode == CriManaMovieMaterial.RenderMode.OnVisibility && !base.HaveRendererOwner && this.target != null && this.target.isVisible)
		{
			base.player.OnWillRenderObject(this);
		}
	}

	// Token: 0x060003BC RID: 956 RVA: 0x0000941F File Offset: 0x0000781F
	protected override void OnDestroy()
	{
		this.target.material = this.originalMaterial;
		if (!this.useOriginalMaterial)
		{
			this.target.enabled = false;
		}
		this.originalMaterial = null;
		base.OnDestroy();
	}

	// Token: 0x060003BD RID: 957 RVA: 0x00009458 File Offset: 0x00007858
	protected override void OnMaterialAvailableChanged()
	{
		if (base.isMaterialAvailable)
		{
			this.target.material = base.material;
			this.target.enabled = true;
		}
		else
		{
			this.target.material = this.originalMaterial;
			if (!this.useOriginalMaterial)
			{
				this.target.enabled = false;
			}
		}
	}

	// Token: 0x0400022E RID: 558
	public Renderer target;

	// Token: 0x0400022F RID: 559
	public bool useOriginalMaterial;

	// Token: 0x04000230 RID: 560
	private Material originalMaterial;
}
