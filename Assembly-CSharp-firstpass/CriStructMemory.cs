﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000011 RID: 17
public class CriStructMemory<Type> : IDisposable
{
	// Token: 0x060000CD RID: 205 RVA: 0x00004972 File Offset: 0x00002D72
	public CriStructMemory()
	{
		this.bytes = new byte[Marshal.SizeOf(typeof(Type))];
		this.gch = GCHandle.Alloc(this.bytes, GCHandleType.Pinned);
	}

	// Token: 0x17000017 RID: 23
	// (get) Token: 0x060000CE RID: 206 RVA: 0x000049A6 File Offset: 0x00002DA6
	// (set) Token: 0x060000CF RID: 207 RVA: 0x000049AE File Offset: 0x00002DAE
	public byte[] bytes { get; private set; }

	// Token: 0x17000018 RID: 24
	// (get) Token: 0x060000D0 RID: 208 RVA: 0x000049B7 File Offset: 0x00002DB7
	public IntPtr ptr
	{
		get
		{
			return this.gch.AddrOfPinnedObject();
		}
	}

	// Token: 0x060000D1 RID: 209 RVA: 0x000049C4 File Offset: 0x00002DC4
	public void Dispose()
	{
		this.gch.Free();
	}

	// Token: 0x04000083 RID: 131
	private GCHandle gch;
}
