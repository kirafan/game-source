﻿using System;
using System.Runtime.InteropServices;
using CriAtomDebugDetail;

// Token: 0x020000A2 RID: 162
public static class CriAtomExAcfDebug
{
	// Token: 0x060004DC RID: 1244 RVA: 0x0000B9DD File Offset: 0x00009DDD
	public static int GetNumCategories()
	{
		return CriAtomExAcfDebug.criAtomExAcf_GetNumCategories();
	}

	// Token: 0x060004DD RID: 1245 RVA: 0x0000B9E4 File Offset: 0x00009DE4
	public static bool GetCategoryInfoByIndex(ushort index, out CriAtomExAcfDebug.CategoryInfo categoryInfo)
	{
		CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetCategoryInfo(index, out categoryInfoForMarshaling) != 0;
		categoryInfoForMarshaling.Convert(out categoryInfo);
		return result;
	}

	// Token: 0x060004DE RID: 1246 RVA: 0x0000BA0C File Offset: 0x00009E0C
	public static bool GetCategoryInfoByName(string name, out CriAtomExAcfDebug.CategoryInfo categoryInfo)
	{
		CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetCategoryInfoByName(name, out categoryInfoForMarshaling) != 0;
		categoryInfoForMarshaling.Convert(out categoryInfo);
		return result;
	}

	// Token: 0x060004DF RID: 1247 RVA: 0x0000BA34 File Offset: 0x00009E34
	public static bool GetCategoryInfoById(uint id, out CriAtomExAcfDebug.CategoryInfo categoryInfo)
	{
		CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetCategoryInfoById(id, out categoryInfoForMarshaling) != 0;
		categoryInfoForMarshaling.Convert(out categoryInfo);
		return result;
	}

	// Token: 0x060004E0 RID: 1248 RVA: 0x0000BA5C File Offset: 0x00009E5C
	public static bool GetDspBusInformation(ushort index, out CriAtomExAcfDebug.DspBusInfo dspBusInfo)
	{
		CriAtomExAcfDebug.DspBusInfoForMarshaling dspBusInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetDspBusInformation(index, out dspBusInfoForMarshaling) != 0;
		dspBusInfoForMarshaling.Convert(out dspBusInfo);
		return result;
	}

	// Token: 0x060004E1 RID: 1249 RVA: 0x0000BA81 File Offset: 0x00009E81
	public static int GetNumAisacControls()
	{
		return CriAtomExAcfDebug.criAtomExAcf_GetNumAisacControls();
	}

	// Token: 0x060004E2 RID: 1250 RVA: 0x0000BA88 File Offset: 0x00009E88
	public static bool GetAisacControlInfo(ushort index, out CriAtomExAcfDebug.AisacControlInfo info)
	{
		CriAtomExAcfDebug.AisacControlInfoForMarshaling aisacControlInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetAisacControlInfo(index, out aisacControlInfoForMarshaling) != 0;
		aisacControlInfoForMarshaling.Convert(out info);
		return result;
	}

	// Token: 0x060004E3 RID: 1251 RVA: 0x0000BAAD File Offset: 0x00009EAD
	public static uint GetAisacControlIdByName(string name)
	{
		return CriAtomExAcfDebug.criAtomExAcf_GetAisacControlIdByName(name);
	}

	// Token: 0x060004E4 RID: 1252 RVA: 0x0000BAB8 File Offset: 0x00009EB8
	public static string GetAisacControlNameById(uint id)
	{
		IntPtr stringPtr = CriAtomExAcfDebug.criAtomExAcf_GetAisacControlNameById(id);
		return Utility.PtrToStringAutoOrNull(stringPtr);
	}

	// Token: 0x060004E5 RID: 1253 RVA: 0x0000BAD2 File Offset: 0x00009ED2
	public static int GetNumGlobalAisacs()
	{
		return CriAtomExAcfDebug.criAtomExAcf_GetNumGlobalAisacs();
	}

	// Token: 0x060004E6 RID: 1254 RVA: 0x0000BADC File Offset: 0x00009EDC
	public static bool GetGlobalAisacInfo(ushort index, out CriAtomExAcfDebug.GlobalAisacInfo info)
	{
		CriAtomExAcfDebug.GlobalAisacInfoForMarshaling globalAisacInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetGlobalAisacInfo(index, out globalAisacInfoForMarshaling) != 0;
		globalAisacInfoForMarshaling.Convert(out info);
		return result;
	}

	// Token: 0x060004E7 RID: 1255 RVA: 0x0000BB04 File Offset: 0x00009F04
	public static bool GetGlobalAisacInfoByName(string name, out CriAtomExAcfDebug.GlobalAisacInfo info)
	{
		CriAtomExAcfDebug.GlobalAisacInfoForMarshaling globalAisacInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetGlobalAisacInfoByName(name, out globalAisacInfoForMarshaling) != 0;
		globalAisacInfoForMarshaling.Convert(out info);
		return result;
	}

	// Token: 0x060004E8 RID: 1256 RVA: 0x0000BB29 File Offset: 0x00009F29
	public static int GetNumSelectors()
	{
		return CriAtomExAcfDebug.criAtomExAcf_GetNumSelectors();
	}

	// Token: 0x060004E9 RID: 1257 RVA: 0x0000BB30 File Offset: 0x00009F30
	public static bool GetSelectorInfoByIndex(ushort index, out CriAtomExAcfDebug.SelectorInfo info)
	{
		CriAtomExAcfDebug.SelectorInfoForMarshaling selectorInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetSelectorInfoByIndex(index, out selectorInfoForMarshaling) != 0;
		selectorInfoForMarshaling.Convert(out info);
		return result;
	}

	// Token: 0x060004EA RID: 1258 RVA: 0x0000BB58 File Offset: 0x00009F58
	public static bool GetSelectorInfoByName(string name, out CriAtomExAcfDebug.SelectorInfo info)
	{
		CriAtomExAcfDebug.SelectorInfoForMarshaling selectorInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetSelectorInfoByName(name, out selectorInfoForMarshaling) != 0;
		selectorInfoForMarshaling.Convert(out info);
		return result;
	}

	// Token: 0x060004EB RID: 1259 RVA: 0x0000BB80 File Offset: 0x00009F80
	public static bool GetSelectorLabelInfo(ref CriAtomExAcfDebug.SelectorInfo selectorInfo, ushort index, out CriAtomExAcfDebug.SelectorLabelInfo labelInfo)
	{
		CriAtomExAcfDebug.SelectorInfoForMarshaling selectorInfoForMarshaling = default(CriAtomExAcfDebug.SelectorInfoForMarshaling);
		selectorInfoForMarshaling.index = selectorInfo.index;
		selectorInfoForMarshaling.numLabels = selectorInfo.numLabels;
		CriAtomExAcfDebug.SelectorLabelInfoForMarshaling selectorLabelInfoForMarshaling;
		bool result = CriAtomExAcfDebug.criAtomExAcf_GetSelectorLabelInfo(ref selectorInfoForMarshaling, index, out selectorLabelInfoForMarshaling) != 0;
		selectorLabelInfoForMarshaling.Convert(out labelInfo);
		return result;
	}

	// Token: 0x060004EC RID: 1260
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetNumCategories();

	// Token: 0x060004ED RID: 1261
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetCategoryInfo(ushort index, out CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfo);

	// Token: 0x060004EE RID: 1262
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetCategoryInfoByName(string name, out CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfo);

	// Token: 0x060004EF RID: 1263
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetCategoryInfoById(uint id, out CriAtomExAcfDebug.CategoryInfoForMarshaling categoryInfo);

	// Token: 0x060004F0 RID: 1264
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetDspBusInformation(ushort index, out CriAtomExAcfDebug.DspBusInfoForMarshaling dspBusInfo);

	// Token: 0x060004F1 RID: 1265
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetNumAisacControls();

	// Token: 0x060004F2 RID: 1266
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetAisacControlInfo(ushort index, out CriAtomExAcfDebug.AisacControlInfoForMarshaling info);

	// Token: 0x060004F3 RID: 1267
	[DllImport("cri_ware_unity")]
	private static extern uint criAtomExAcf_GetAisacControlIdByName(string name);

	// Token: 0x060004F4 RID: 1268
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExAcf_GetAisacControlNameById(uint id);

	// Token: 0x060004F5 RID: 1269
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetNumGlobalAisacs();

	// Token: 0x060004F6 RID: 1270
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetGlobalAisacInfo(ushort index, out CriAtomExAcfDebug.GlobalAisacInfoForMarshaling info);

	// Token: 0x060004F7 RID: 1271
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetGlobalAisacInfoByName(string name, out CriAtomExAcfDebug.GlobalAisacInfoForMarshaling info);

	// Token: 0x060004F8 RID: 1272
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetNumSelectors();

	// Token: 0x060004F9 RID: 1273
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetSelectorInfoByIndex(ushort index, out CriAtomExAcfDebug.SelectorInfoForMarshaling info);

	// Token: 0x060004FA RID: 1274
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetSelectorInfoByName(string name, out CriAtomExAcfDebug.SelectorInfoForMarshaling info);

	// Token: 0x060004FB RID: 1275
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExAcf_GetSelectorLabelInfo(ref CriAtomExAcfDebug.SelectorInfoForMarshaling info, ushort labelIndex, out CriAtomExAcfDebug.SelectorLabelInfoForMarshaling label_info);

	// Token: 0x020000A3 RID: 163
	public struct CategoryInfo
	{
		// Token: 0x040002F2 RID: 754
		public uint groupNo;

		// Token: 0x040002F3 RID: 755
		public uint id;

		// Token: 0x040002F4 RID: 756
		public string name;

		// Token: 0x040002F5 RID: 757
		public uint numCueLimits;

		// Token: 0x040002F6 RID: 758
		public float volume;
	}

	// Token: 0x020000A4 RID: 164
	public struct DspBusInfo
	{
		// Token: 0x040002F7 RID: 759
		public string name;

		// Token: 0x040002F8 RID: 760
		public float volume;

		// Token: 0x040002F9 RID: 761
		public float pan3dVolume;

		// Token: 0x040002FA RID: 762
		public float pan3dAngle;

		// Token: 0x040002FB RID: 763
		public float pan3dDistance;

		// Token: 0x040002FC RID: 764
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public ushort[] fxIndexes;

		// Token: 0x040002FD RID: 765
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
		public ushort[] busLinkIndexes;

		// Token: 0x040002FE RID: 766
		public ushort busNo;

		// Token: 0x040002FF RID: 767
		public byte numFxes;

		// Token: 0x04000300 RID: 768
		public byte numBusLinks;
	}

	// Token: 0x020000A5 RID: 165
	public struct AisacControlInfo
	{
		// Token: 0x04000301 RID: 769
		public string name;

		// Token: 0x04000302 RID: 770
		public uint id;
	}

	// Token: 0x020000A6 RID: 166
	public enum AisacType
	{
		// Token: 0x04000304 RID: 772
		Normal,
		// Token: 0x04000305 RID: 773
		AutoModulation
	}

	// Token: 0x020000A7 RID: 167
	public struct GlobalAisacInfo
	{
		// Token: 0x04000306 RID: 774
		public string name;

		// Token: 0x04000307 RID: 775
		public ushort index;

		// Token: 0x04000308 RID: 776
		public ushort numGraphs;

		// Token: 0x04000309 RID: 777
		public CriAtomExAcfDebug.AisacType type;

		// Token: 0x0400030A RID: 778
		public float randomRange;

		// Token: 0x0400030B RID: 779
		public ushort controlId;
	}

	// Token: 0x020000A8 RID: 168
	public struct SelectorInfo
	{
		// Token: 0x0400030C RID: 780
		public string name;

		// Token: 0x0400030D RID: 781
		public ushort index;

		// Token: 0x0400030E RID: 782
		public ushort numLabels;

		// Token: 0x0400030F RID: 783
		public ushort globalLabelIndex;
	}

	// Token: 0x020000A9 RID: 169
	public struct SelectorLabelInfo
	{
		// Token: 0x04000310 RID: 784
		public string selectorName;

		// Token: 0x04000311 RID: 785
		public string labelName;
	}

	// Token: 0x020000AA RID: 170
	private struct CategoryInfoForMarshaling
	{
		// Token: 0x060004FC RID: 1276 RVA: 0x0000BBCC File Offset: 0x00009FCC
		public void Convert(out CriAtomExAcfDebug.CategoryInfo x)
		{
			x.groupNo = this.groupNo;
			x.id = this.id;
			x.name = Utility.PtrToStringAutoOrNull(this.namePtr);
			x.numCueLimits = this.numCueLimits;
			x.volume = this.volume;
		}

		// Token: 0x04000312 RID: 786
		public uint groupNo;

		// Token: 0x04000313 RID: 787
		public uint id;

		// Token: 0x04000314 RID: 788
		public IntPtr namePtr;

		// Token: 0x04000315 RID: 789
		public uint numCueLimits;

		// Token: 0x04000316 RID: 790
		public float volume;
	}

	// Token: 0x020000AB RID: 171
	private struct DspBusInfoForMarshaling
	{
		// Token: 0x060004FD RID: 1277 RVA: 0x0000BC1C File Offset: 0x0000A01C
		public void Convert(out CriAtomExAcfDebug.DspBusInfo x)
		{
			x.name = Utility.PtrToStringAutoOrNull(this.namePtr);
			x.volume = this.volume;
			x.pan3dVolume = this.pan3dVolume;
			x.pan3dAngle = this.pan3dAngle;
			x.pan3dDistance = this.pan3dDistance;
			x.fxIndexes = this.fxIndexes;
			x.busLinkIndexes = this.busLinkIndexes;
			x.busNo = this.busNo;
			x.numFxes = this.numFxes;
			x.numBusLinks = this.numBusLinks;
		}

		// Token: 0x04000317 RID: 791
		public IntPtr namePtr;

		// Token: 0x04000318 RID: 792
		public float volume;

		// Token: 0x04000319 RID: 793
		public float pan3dVolume;

		// Token: 0x0400031A RID: 794
		public float pan3dAngle;

		// Token: 0x0400031B RID: 795
		public float pan3dDistance;

		// Token: 0x0400031C RID: 796
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
		public ushort[] fxIndexes;

		// Token: 0x0400031D RID: 797
		[MarshalAs(UnmanagedType.ByValArray, SizeConst = 64)]
		public ushort[] busLinkIndexes;

		// Token: 0x0400031E RID: 798
		public ushort busNo;

		// Token: 0x0400031F RID: 799
		public byte numFxes;

		// Token: 0x04000320 RID: 800
		public byte numBusLinks;
	}

	// Token: 0x020000AC RID: 172
	private struct AisacControlInfoForMarshaling
	{
		// Token: 0x060004FE RID: 1278 RVA: 0x0000BCA6 File Offset: 0x0000A0A6
		public void Convert(out CriAtomExAcfDebug.AisacControlInfo x)
		{
			x.name = Utility.PtrToStringAutoOrNull(this.namePtr);
			x.id = this.id;
		}

		// Token: 0x04000321 RID: 801
		public IntPtr namePtr;

		// Token: 0x04000322 RID: 802
		public uint id;
	}

	// Token: 0x020000AD RID: 173
	private struct GlobalAisacInfoForMarshaling
	{
		// Token: 0x060004FF RID: 1279 RVA: 0x0000BCC8 File Offset: 0x0000A0C8
		public void Convert(out CriAtomExAcfDebug.GlobalAisacInfo x)
		{
			x.name = Utility.PtrToStringAutoOrNull(this.namePtr);
			x.index = this.index;
			x.numGraphs = this.numGraphs;
			x.type = (CriAtomExAcfDebug.AisacType)this.type;
			x.randomRange = this.randomRange;
			x.controlId = this.controlId;
		}

		// Token: 0x04000323 RID: 803
		public IntPtr namePtr;

		// Token: 0x04000324 RID: 804
		public ushort index;

		// Token: 0x04000325 RID: 805
		public ushort numGraphs;

		// Token: 0x04000326 RID: 806
		public uint type;

		// Token: 0x04000327 RID: 807
		public float randomRange;

		// Token: 0x04000328 RID: 808
		public ushort controlId;

		// Token: 0x04000329 RID: 809
		public ushort dummy;
	}

	// Token: 0x020000AE RID: 174
	private struct SelectorInfoForMarshaling
	{
		// Token: 0x06000500 RID: 1280 RVA: 0x0000BD22 File Offset: 0x0000A122
		public void Convert(out CriAtomExAcfDebug.SelectorInfo x)
		{
			x.name = Utility.PtrToStringAutoOrNull(this.namePtr);
			x.index = this.index;
			x.numLabels = this.numLabels;
			x.globalLabelIndex = this.globalLabelIndex;
		}

		// Token: 0x0400032A RID: 810
		public IntPtr namePtr;

		// Token: 0x0400032B RID: 811
		public ushort index;

		// Token: 0x0400032C RID: 812
		public ushort numLabels;

		// Token: 0x0400032D RID: 813
		public ushort globalLabelIndex;
	}

	// Token: 0x020000AF RID: 175
	private struct SelectorLabelInfoForMarshaling
	{
		// Token: 0x06000501 RID: 1281 RVA: 0x0000BD59 File Offset: 0x0000A159
		public void Convert(out CriAtomExAcfDebug.SelectorLabelInfo x)
		{
			x.selectorName = Utility.PtrToStringAutoOrNull(this.selectorNamePtr);
			x.labelName = Utility.PtrToStringAutoOrNull(this.labelNamePtr);
		}

		// Token: 0x0400032E RID: 814
		public IntPtr selectorNamePtr;

		// Token: 0x0400032F RID: 815
		public IntPtr labelNamePtr;
	}
}
