﻿using System;
using System.Runtime.InteropServices;

// Token: 0x0200003C RID: 60
public class CriAtomExAsrRack : IDisposable
{
	// Token: 0x060001C1 RID: 449 RVA: 0x00005E3C File Offset: 0x0000423C
	public CriAtomExAsrRack(CriAtomExAsrRack.Config config, CriAtomExAsrRack.PlatformConfig platformConfig)
	{
		this._rackId = CriAtomExAsrRack.criAtomUnityAsrRack_Create(ref config, ref platformConfig);
		if (config.context != IntPtr.Zero)
		{
			Marshal.FreeHGlobal(config.context);
		}
		if (this._rackId == -1)
		{
			throw new Exception("CriAtomExAsrRack() failed.");
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x060001C2 RID: 450 RVA: 0x00005EA5 File Offset: 0x000042A5
	public void Dispose()
	{
		CriDisposableObjectManager.Unregister(this);
		if (this._rackId != -1)
		{
			CriAtomExAsrRack.criAtomExAsrRack_Destroy(this._rackId);
			this._rackId = -1;
		}
		GC.SuppressFinalize(this);
	}

	// Token: 0x1700001D RID: 29
	// (get) Token: 0x060001C3 RID: 451 RVA: 0x00005ED2 File Offset: 0x000042D2
	public int rackId
	{
		get
		{
			return this._rackId;
		}
	}

	// Token: 0x1700001E RID: 30
	// (get) Token: 0x060001C4 RID: 452 RVA: 0x00005EDC File Offset: 0x000042DC
	public static CriAtomExAsrRack.Config defaultConfig
	{
		get
		{
			CriAtomExAsrRack.Config result;
			result.serverFrequency = 60f;
			result.numBuses = 8;
			result.soundRendererType = CriAtomEx.SoundRendererType.Native;
			result.outputRackId = 0;
			result.context = IntPtr.Zero;
			result.outputChannels = 2;
			result.outputSamplingRate = 44100;
			return result;
		}
	}

	// Token: 0x060001C5 RID: 453 RVA: 0x00005F30 File Offset: 0x00004330
	~CriAtomExAsrRack()
	{
		this.Dispose();
	}

	// Token: 0x060001C6 RID: 454
	[DllImport("cri_ware_unity")]
	private static extern int criAtomUnityAsrRack_Create([In] ref CriAtomExAsrRack.Config config, [In] ref CriAtomExAsrRack.PlatformConfig platformConfig);

	// Token: 0x060001C7 RID: 455
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsrRack_Destroy(int rackId);

	// Token: 0x060001C8 RID: 456
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsrRack_AttachDspBusSetting(int rackId, string setting, IntPtr work, int workSize);

	// Token: 0x060001C9 RID: 457
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsrRack_DetachDspBusSetting(int rackId);

	// Token: 0x060001CA RID: 458
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExAsrRack_ApplyDspBusSnapshot(int rackId, string snapshotName, int timeMs);

	// Token: 0x04000133 RID: 307
	public static int defaultRackId;

	// Token: 0x04000134 RID: 308
	private int _rackId = -1;

	// Token: 0x0200003D RID: 61
	public struct Config
	{
		// Token: 0x04000135 RID: 309
		public float serverFrequency;

		// Token: 0x04000136 RID: 310
		public int numBuses;

		// Token: 0x04000137 RID: 311
		public int outputChannels;

		// Token: 0x04000138 RID: 312
		public int outputSamplingRate;

		// Token: 0x04000139 RID: 313
		public CriAtomEx.SoundRendererType soundRendererType;

		// Token: 0x0400013A RID: 314
		public int outputRackId;

		// Token: 0x0400013B RID: 315
		public IntPtr context;
	}

	// Token: 0x0200003E RID: 62
	public struct PlatformConfig
	{
		// Token: 0x0400013C RID: 316
		public byte reserved;
	}
}
