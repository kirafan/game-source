﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000042 RID: 66
public class CriAtomExPlayer : IDisposable
{
	// Token: 0x060001F0 RID: 496 RVA: 0x00006089 File Offset: 0x00004489
	public CriAtomExPlayer() : this(0, 0, false, IntPtr.Zero)
	{
	}

	// Token: 0x060001F1 RID: 497 RVA: 0x00006099 File Offset: 0x00004499
	public CriAtomExPlayer(int maxPath, int maxPathStrings) : this(maxPath, maxPathStrings, false, IntPtr.Zero)
	{
	}

	// Token: 0x060001F2 RID: 498 RVA: 0x000060A9 File Offset: 0x000044A9
	public CriAtomExPlayer(bool enableAudioSyncedTimer) : this(0, 0, enableAudioSyncedTimer, IntPtr.Zero)
	{
	}

	// Token: 0x060001F3 RID: 499 RVA: 0x000060B9 File Offset: 0x000044B9
	public CriAtomExPlayer(int maxPath, int maxPathStrings, bool enableAudioSyncedTimer) : this(maxPath, maxPathStrings, enableAudioSyncedTimer, IntPtr.Zero)
	{
	}

	// Token: 0x060001F4 RID: 500 RVA: 0x000060C9 File Offset: 0x000044C9
	public CriAtomExPlayer(IntPtr existingNativeHandle) : this(0, 0, false, existingNativeHandle)
	{
	}

	// Token: 0x060001F5 RID: 501 RVA: 0x000060D8 File Offset: 0x000044D8
	public CriAtomExPlayer(int maxPath, int maxPathStrings, bool enableAudioSyncedTimer, IntPtr existingNativeHandle)
	{
		if (!CriAtomPlugin.isInitialized)
		{
			throw new Exception("CriAtomPlugin is not initialized.");
		}
		CriAtomExPlayer.Config config;
		config.voiceAllocationMethod = CriAtomEx.VoiceAllocationMethod.Once;
		config.maxPath = maxPath;
		config.maxPathStrings = maxPathStrings;
		config.updatesTime = true;
		config.enableAudioSyncedTimer = enableAudioSyncedTimer;
		this.hasExistingNativeHandle = (existingNativeHandle != IntPtr.Zero);
		if (this.hasExistingNativeHandle)
		{
			this.handle = existingNativeHandle;
		}
		else
		{
			this.handle = CriAtomExPlayer.criAtomExPlayer_Create(ref config, IntPtr.Zero, 0);
		}
		CriDisposableObjectManager.Register(this, CriDisposableObjectManager.ModuleType.Atom);
	}

	// Token: 0x17000023 RID: 35
	// (get) Token: 0x060001F6 RID: 502 RVA: 0x00006177 File Offset: 0x00004577
	public IntPtr nativeHandle
	{
		get
		{
			return this.handle;
		}
	}

	// Token: 0x17000024 RID: 36
	// (get) Token: 0x060001F7 RID: 503 RVA: 0x0000617F File Offset: 0x0000457F
	public bool isAvailable
	{
		get
		{
			return this.handle != IntPtr.Zero;
		}
	}

	// Token: 0x060001F8 RID: 504 RVA: 0x00006191 File Offset: 0x00004591
	public void Dispose()
	{
		CriDisposableObjectManager.Unregister(this);
		if (!this.hasExistingNativeHandle && this.isAvailable)
		{
			CriAtomExPlayer.criAtomExPlayer_Destroy(this.handle);
		}
		this.handle = IntPtr.Zero;
		GC.SuppressFinalize(this);
	}

	// Token: 0x060001F9 RID: 505 RVA: 0x000061CC File Offset: 0x000045CC
	public void SetCue(CriAtomExAcb acb, string name)
	{
		CriAtomExPlayer.criAtomExPlayer_SetCueName(this.handle, (acb == null) ? IntPtr.Zero : acb.nativeHandle, name);
	}

	// Token: 0x060001FA RID: 506 RVA: 0x000061F0 File Offset: 0x000045F0
	public void SetCue(CriAtomExAcb acb, int id)
	{
		CriAtomExPlayer.criAtomExPlayer_SetCueId(this.handle, (acb == null) ? IntPtr.Zero : acb.nativeHandle, id);
	}

	// Token: 0x060001FB RID: 507 RVA: 0x00006214 File Offset: 0x00004614
	public void SetCueIndex(CriAtomExAcb acb, int index)
	{
		CriAtomExPlayer.criAtomExPlayer_SetCueIndex(this.handle, (acb == null) ? IntPtr.Zero : acb.nativeHandle, index);
	}

	// Token: 0x060001FC RID: 508 RVA: 0x00006238 File Offset: 0x00004638
	public void SetContentId(CriFsBinder binder, int contentId)
	{
		CriAtomExPlayer.criAtomExPlayer_SetContentId(this.handle, (binder == null) ? IntPtr.Zero : binder.nativeHandle, contentId);
	}

	// Token: 0x060001FD RID: 509 RVA: 0x0000625C File Offset: 0x0000465C
	public void SetFile(CriFsBinder binder, string path)
	{
		CriAtomExPlayer.criAtomExPlayer_SetFile(this.handle, (binder == null) ? IntPtr.Zero : binder.nativeHandle, path);
	}

	// Token: 0x060001FE RID: 510 RVA: 0x00006280 File Offset: 0x00004680
	public void SetData(byte[] buffer, int size)
	{
		CriAtomExPlayer.criAtomExPlayer_SetData(this.handle, buffer, size);
	}

	// Token: 0x060001FF RID: 511 RVA: 0x0000628F File Offset: 0x0000468F
	public void SetFormat(CriAtomEx.Format format)
	{
		CriAtomExPlayer.criAtomExPlayer_SetFormat(this.handle, format);
	}

	// Token: 0x06000200 RID: 512 RVA: 0x0000629D File Offset: 0x0000469D
	public void SetNumChannels(int numChannels)
	{
		CriAtomExPlayer.criAtomExPlayer_SetNumChannels(this.handle, numChannels);
	}

	// Token: 0x06000201 RID: 513 RVA: 0x000062AB File Offset: 0x000046AB
	public void SetSamplingRate(int samplingRate)
	{
		CriAtomExPlayer.criAtomExPlayer_SetSamplingRate(this.handle, samplingRate);
	}

	// Token: 0x06000202 RID: 514 RVA: 0x000062B9 File Offset: 0x000046B9
	public CriAtomExPlayback Start()
	{
		return new CriAtomExPlayback(CriAtomExPlayer.criAtomExPlayer_Start(this.handle));
	}

	// Token: 0x06000203 RID: 515 RVA: 0x000062CB File Offset: 0x000046CB
	public CriAtomExPlayback Prepare()
	{
		return new CriAtomExPlayback(CriAtomExPlayer.criAtomExPlayer_Prepare(this.handle));
	}

	// Token: 0x06000204 RID: 516 RVA: 0x000062DD File Offset: 0x000046DD
	public void Stop(bool ignoresReleaseTime)
	{
		if (!this.isAvailable)
		{
			return;
		}
		if (!ignoresReleaseTime)
		{
			CriAtomExPlayer.criAtomExPlayer_Stop(this.handle);
		}
		else
		{
			CriAtomExPlayer.criAtomExPlayer_StopWithoutReleaseTime(this.handle);
		}
	}

	// Token: 0x06000205 RID: 517 RVA: 0x0000630C File Offset: 0x0000470C
	public void Pause()
	{
		CriAtomExPlayer.criAtomExPlayer_Pause(this.handle, true);
	}

	// Token: 0x06000206 RID: 518 RVA: 0x0000631A File Offset: 0x0000471A
	public void Resume(CriAtomEx.ResumeMode mode)
	{
		CriAtomExPlayer.criAtomExPlayer_Resume(this.handle, mode);
	}

	// Token: 0x06000207 RID: 519 RVA: 0x00006328 File Offset: 0x00004728
	public bool IsPaused()
	{
		return CriAtomExPlayer.criAtomExPlayer_IsPaused(this.handle);
	}

	// Token: 0x06000208 RID: 520 RVA: 0x00006335 File Offset: 0x00004735
	public void SetVolume(float volume)
	{
		CriAtomExPlayer.criAtomExPlayer_SetVolume(this.handle, volume);
	}

	// Token: 0x06000209 RID: 521 RVA: 0x00006343 File Offset: 0x00004743
	public void SetPitch(float pitch)
	{
		CriAtomExPlayer.criAtomExPlayer_SetPitch(this.handle, pitch);
	}

	// Token: 0x0600020A RID: 522 RVA: 0x00006351 File Offset: 0x00004751
	public void SetPan3dAngle(float angle)
	{
		CriAtomExPlayer.criAtomExPlayer_SetPan3dAngle(this.handle, angle);
	}

	// Token: 0x0600020B RID: 523 RVA: 0x0000635F File Offset: 0x0000475F
	public void SetPan3dInteriorDistance(float distance)
	{
		CriAtomExPlayer.criAtomExPlayer_SetPan3dInteriorDistance(this.handle, distance);
	}

	// Token: 0x0600020C RID: 524 RVA: 0x0000636D File Offset: 0x0000476D
	public void SetPan3dVolume(float volume)
	{
		CriAtomExPlayer.criAtomExPlayer_SetPan3dVolume(this.handle, volume);
	}

	// Token: 0x0600020D RID: 525 RVA: 0x0000637B File Offset: 0x0000477B
	public void SetPanType(CriAtomEx.PanType panType)
	{
		CriAtomExPlayer.criAtomExPlayer_SetPanType(this.handle, panType);
	}

	// Token: 0x0600020E RID: 526 RVA: 0x00006389 File Offset: 0x00004789
	public void SetSendLevel(int channel, CriAtomEx.Speaker id, float level)
	{
		CriAtomExPlayer.criAtomExPlayer_SetSendLevel(this.handle, channel, id, level);
	}

	// Token: 0x0600020F RID: 527 RVA: 0x00006399 File Offset: 0x00004799
	public void SetBiquadFilterParameters(CriAtomEx.BiquadFilterType type, float frequency, float gain, float q)
	{
		CriAtomExPlayer.criAtomExPlayer_SetBiquadFilterParameters(this.handle, type, frequency, gain, q);
	}

	// Token: 0x06000210 RID: 528 RVA: 0x000063AB File Offset: 0x000047AB
	public void SetBandpassFilterParameters(float cofLow, float cofHigh)
	{
		CriAtomExPlayer.criAtomExPlayer_SetBandpassFilterParameters(this.handle, cofLow, cofHigh);
	}

	// Token: 0x06000211 RID: 529 RVA: 0x000063BA File Offset: 0x000047BA
	public void SetBusSendLevel(string busName, float level)
	{
		CriAtomExPlayer.criAtomExPlayer_SetBusSendLevelByName(this.handle, busName, level);
	}

	// Token: 0x06000212 RID: 530 RVA: 0x000063C9 File Offset: 0x000047C9
	public void SetBusSendLevel(int busId, float level)
	{
		CriAtomExPlayer.criAtomExPlayer_SetBusSendLevel(this.handle, busId, level);
	}

	// Token: 0x06000213 RID: 531 RVA: 0x000063D8 File Offset: 0x000047D8
	public void SetBusSendLevelOffset(string busName, float levelOffset)
	{
		CriAtomExPlayer.criAtomExPlayer_SetBusSendLevelOffsetByName(this.handle, busName, levelOffset);
	}

	// Token: 0x06000214 RID: 532 RVA: 0x000063E7 File Offset: 0x000047E7
	public void SetBusSendLevelOffset(int busId, float levelOffset)
	{
		CriAtomExPlayer.criAtomExPlayer_SetBusSendLevelOffset(this.handle, busId, levelOffset);
	}

	// Token: 0x06000215 RID: 533 RVA: 0x000063F6 File Offset: 0x000047F6
	public void SetAisacControl(string controlName, float value)
	{
		CriAtomExPlayer.criAtomExPlayer_SetAisacControlByName(this.handle, controlName, value);
	}

	// Token: 0x06000216 RID: 534 RVA: 0x00006405 File Offset: 0x00004805
	[Obsolete("Use CriAtomExPlayer.SetAisacControl")]
	public void SetAisac(string controlName, float value)
	{
		this.SetAisacControl(controlName, value);
	}

	// Token: 0x06000217 RID: 535 RVA: 0x0000640F File Offset: 0x0000480F
	public void SetAisacControl(uint controlId, float value)
	{
		CriAtomExPlayer.criAtomExPlayer_SetAisacControlById(this.handle, (ushort)controlId, value);
	}

	// Token: 0x06000218 RID: 536 RVA: 0x0000641F File Offset: 0x0000481F
	[Obsolete("Use SetAisacControl")]
	public void SetAisac(uint controlId, float value)
	{
		CriAtomExPlayer.criAtomExPlayer_SetAisacControlById(this.handle, (ushort)controlId, value);
	}

	// Token: 0x06000219 RID: 537 RVA: 0x0000642F File Offset: 0x0000482F
	public void Set3dSource(CriAtomEx3dSource source)
	{
		CriAtomExPlayer.criAtomExPlayer_Set3dSourceHn(this.handle, (source != null) ? source.nativeHandle : IntPtr.Zero);
	}

	// Token: 0x0600021A RID: 538 RVA: 0x00006452 File Offset: 0x00004852
	public void Set3dListener(CriAtomEx3dListener listener)
	{
		CriAtomExPlayer.criAtomExPlayer_Set3dListenerHn(this.handle, (listener != null) ? listener.nativeHandle : IntPtr.Zero);
	}

	// Token: 0x0600021B RID: 539 RVA: 0x00006475 File Offset: 0x00004875
	public void SetStartTime(long startTimeMs)
	{
		CriAtomExPlayer.criAtomExPlayer_SetStartTime(this.handle, startTimeMs);
	}

	// Token: 0x0600021C RID: 540 RVA: 0x00006483 File Offset: 0x00004883
	public void SetFirstBlockIndex(int index)
	{
		CriAtomExPlayer.criAtomExPlayer_SetFirstBlockIndex(this.handle, index);
	}

	// Token: 0x0600021D RID: 541 RVA: 0x00006491 File Offset: 0x00004891
	public void SetSelectorLabel(string selector, string label)
	{
		CriAtomExPlayer.criAtomExPlayer_SetSelectorLabel(this.handle, selector, label);
	}

	// Token: 0x0600021E RID: 542 RVA: 0x000064A0 File Offset: 0x000048A0
	public void ClearSelectorLabels()
	{
		CriAtomExPlayer.criAtomExPlayer_ClearSelectorLabels(this.handle);
	}

	// Token: 0x0600021F RID: 543 RVA: 0x000064AD File Offset: 0x000048AD
	public void SetCategory(int categoryId)
	{
		CriAtomExPlayer.criAtomExPlayer_SetCategoryById(this.handle, (uint)categoryId);
	}

	// Token: 0x06000220 RID: 544 RVA: 0x000064BB File Offset: 0x000048BB
	public void SetCategory(string categoryName)
	{
		CriAtomExPlayer.criAtomExPlayer_SetCategoryByName(this.handle, categoryName);
	}

	// Token: 0x06000221 RID: 545 RVA: 0x000064C9 File Offset: 0x000048C9
	public void UnsetCategory()
	{
		CriAtomExPlayer.criAtomExPlayer_UnsetCategory(this.handle);
	}

	// Token: 0x06000222 RID: 546 RVA: 0x000064D6 File Offset: 0x000048D6
	public void SetCuePriority(int priority)
	{
		CriAtomExPlayer.criAtomExPlayer_SetCuePriority(this.handle, priority);
	}

	// Token: 0x06000223 RID: 547 RVA: 0x000064E4 File Offset: 0x000048E4
	public void SetVoicePriority(int priority)
	{
		CriAtomExPlayer.criAtomExPlayer_SetVoicePriority(this.handle, priority);
	}

	// Token: 0x06000224 RID: 548 RVA: 0x000064F2 File Offset: 0x000048F2
	public void SetVoiceControlMethod(CriAtomEx.VoiceControlMethod method)
	{
		CriAtomExPlayer.criAtomExPlayer_SetVoiceControlMethod(this.handle, method);
	}

	// Token: 0x06000225 RID: 549 RVA: 0x00006500 File Offset: 0x00004900
	public void SetEnvelopeAttackTime(float time)
	{
		CriAtomExPlayer.criAtomExPlayer_SetEnvelopeAttackTime(this.handle, time);
	}

	// Token: 0x06000226 RID: 550 RVA: 0x0000650E File Offset: 0x0000490E
	public void SetEnvelopeHoldTime(float time)
	{
		CriAtomExPlayer.criAtomExPlayer_SetEnvelopeHoldTime(this.handle, time);
	}

	// Token: 0x06000227 RID: 551 RVA: 0x0000651C File Offset: 0x0000491C
	public void SetEnvelopeDecayTime(float time)
	{
		CriAtomExPlayer.criAtomExPlayer_SetEnvelopeDecayTime(this.handle, time);
	}

	// Token: 0x06000228 RID: 552 RVA: 0x0000652A File Offset: 0x0000492A
	public void SetEnvelopeReleaseTime(float time)
	{
		CriAtomExPlayer.criAtomExPlayer_SetEnvelopeReleaseTime(this.handle, time);
	}

	// Token: 0x06000229 RID: 553 RVA: 0x00006538 File Offset: 0x00004938
	public void SetEnvelopeSustainLevel(float level)
	{
		CriAtomExPlayer.criAtomExPlayer_SetEnvelopeSustainLevel(this.handle, level);
	}

	// Token: 0x0600022A RID: 554 RVA: 0x00006546 File Offset: 0x00004946
	public void AttachFader()
	{
		CriAtomExPlayer.criAtomExPlayer_AttachFader(this.handle, IntPtr.Zero, IntPtr.Zero, 0);
	}

	// Token: 0x0600022B RID: 555 RVA: 0x0000655E File Offset: 0x0000495E
	public void DetachFader()
	{
		CriAtomExPlayer.criAtomExPlayer_DetachFader(this.handle);
	}

	// Token: 0x0600022C RID: 556 RVA: 0x0000656B File Offset: 0x0000496B
	public void SetFadeOutTime(int ms)
	{
		CriAtomExPlayer.criAtomExPlayer_SetFadeOutTime(this.handle, ms);
	}

	// Token: 0x0600022D RID: 557 RVA: 0x00006579 File Offset: 0x00004979
	public void SetFadeInTime(int ms)
	{
		CriAtomExPlayer.criAtomExPlayer_SetFadeInTime(this.handle, ms);
	}

	// Token: 0x0600022E RID: 558 RVA: 0x00006587 File Offset: 0x00004987
	public void SetFadeInStartOffset(int ms)
	{
		CriAtomExPlayer.criAtomExPlayer_SetFadeInStartOffset(this.handle, ms);
	}

	// Token: 0x0600022F RID: 559 RVA: 0x00006595 File Offset: 0x00004995
	public void SetFadeOutEndDelay(int ms)
	{
		CriAtomExPlayer.criAtomExPlayer_SetFadeOutEndDelay(this.handle, ms);
	}

	// Token: 0x06000230 RID: 560 RVA: 0x000065A3 File Offset: 0x000049A3
	public bool IsFading()
	{
		return CriAtomExPlayer.criAtomExPlayer_IsFading(this.handle);
	}

	// Token: 0x06000231 RID: 561 RVA: 0x000065B0 File Offset: 0x000049B0
	public void ResetFaderParameters()
	{
		CriAtomExPlayer.criAtomExPlayer_ResetFaderParameters(this.handle);
	}

	// Token: 0x06000232 RID: 562 RVA: 0x000065BD File Offset: 0x000049BD
	public void SetGroupNumber(int group_no)
	{
		CriAtomExPlayer.criAtomExPlayer_SetGroupNumber(this.handle, group_no);
	}

	// Token: 0x06000233 RID: 563 RVA: 0x000065CB File Offset: 0x000049CB
	public void Update(CriAtomExPlayback playback)
	{
		CriAtomExPlayer.criAtomExPlayer_Update(this.handle, playback.id);
	}

	// Token: 0x06000234 RID: 564 RVA: 0x000065DF File Offset: 0x000049DF
	public void UpdateAll()
	{
		CriAtomExPlayer.criAtomExPlayer_UpdateAll(this.handle);
	}

	// Token: 0x06000235 RID: 565 RVA: 0x000065EC File Offset: 0x000049EC
	public void ResetParameters()
	{
		CriAtomExPlayer.criAtomExPlayer_ResetParameters(this.handle);
	}

	// Token: 0x06000236 RID: 566 RVA: 0x000065F9 File Offset: 0x000049F9
	public long GetTime()
	{
		return CriAtomExPlayer.criAtomExPlayer_GetTime(this.handle);
	}

	// Token: 0x06000237 RID: 567 RVA: 0x00006606 File Offset: 0x00004A06
	public CriAtomExPlayer.Status GetStatus()
	{
		return CriAtomExPlayer.criAtomExPlayer_GetStatus(this.handle);
	}

	// Token: 0x06000238 RID: 568 RVA: 0x00006613 File Offset: 0x00004A13
	public float GetParameterFloat32(CriAtomEx.Parameter id)
	{
		return CriAtomExPlayer.criAtomExPlayer_GetParameterFloat32(this.handle, id);
	}

	// Token: 0x06000239 RID: 569 RVA: 0x00006621 File Offset: 0x00004A21
	public uint GetParameterUint32(CriAtomEx.Parameter id)
	{
		return CriAtomExPlayer.criAtomExPlayer_GetParameterUint32(this.handle, id);
	}

	// Token: 0x0600023A RID: 570 RVA: 0x0000662F File Offset: 0x00004A2F
	public int GetParameterSint32(CriAtomEx.Parameter id)
	{
		return CriAtomExPlayer.criAtomExPlayer_GetParameterSint32(this.handle, id);
	}

	// Token: 0x0600023B RID: 571 RVA: 0x0000663D File Offset: 0x00004A3D
	public void SetSoundRendererType(CriAtomEx.SoundRendererType type)
	{
		CriAtomExPlayer.criAtomExPlayer_SetSoundRendererType(this.handle, type);
	}

	// Token: 0x0600023C RID: 572 RVA: 0x0000664B File Offset: 0x00004A4B
	public void SetRandomSeed(uint seed)
	{
		CriAtomExPlayer.criAtomExPlayer_SetRandomSeed(this.handle, seed);
	}

	// Token: 0x0600023D RID: 573 RVA: 0x0000665C File Offset: 0x00004A5C
	public void Loop(bool sw)
	{
		if (sw)
		{
			CriAtomExPlayer.criAtomExPlayer_LimitLoopCount(this.handle, -3);
		}
		else
		{
			ushort loopCountParameterId = CriAtomPlugin.GetLoopCountParameterId();
			IntPtr player_parameter = CriAtomExPlayer.criAtomExPlayer_GetPlayerParameter(this.handle);
			CriAtomExPlayer.criAtomExPlayerParameter_RemoveParameter(player_parameter, loopCountParameterId);
		}
	}

	// Token: 0x0600023E RID: 574 RVA: 0x0000669A File Offset: 0x00004A9A
	public void SetAsrRackId(int asr_rack_id)
	{
		CriAtomExPlayer.criAtomExPlayer_SetAsrRackId(this.handle, asr_rack_id);
	}

	// Token: 0x0600023F RID: 575 RVA: 0x000066A8 File Offset: 0x00004AA8
	public void SetVoicePoolIdentifier(uint identifier)
	{
		CriAtomExPlayer.criAtomExPlayer_SetVoicePoolIdentifier(this.handle, identifier);
	}

	// Token: 0x06000240 RID: 576 RVA: 0x000066B6 File Offset: 0x00004AB6
	public void SetDspTimeStretchRatio(float ratio)
	{
		this.SetDspParameter(0, ratio);
	}

	// Token: 0x06000241 RID: 577 RVA: 0x000066C0 File Offset: 0x00004AC0
	public void SetDspPitchShifterPitch(float pitch)
	{
		float value = pitch / 4800f + 0.5f;
		this.SetDspParameter(0, value);
	}

	// Token: 0x06000242 RID: 578 RVA: 0x000066E3 File Offset: 0x00004AE3
	public void SetDspParameter(int id, float value)
	{
		CriAtomExPlayer.criAtomExPlayer_SetDspParameter(this.handle, id, value);
	}

	// Token: 0x06000243 RID: 579 RVA: 0x000066F2 File Offset: 0x00004AF2
	public void SetSequencePrepareTime(uint ms)
	{
		CriAtomExPlayer.criAtomExPlayer_SetSequencePrepareTime(this.handle, ms);
	}

	// Token: 0x06000244 RID: 580 RVA: 0x00006700 File Offset: 0x00004B00
	public void Stop()
	{
		if (this.isAvailable)
		{
			CriAtomExPlayer.criAtomExPlayer_Stop(this.handle);
		}
	}

	// Token: 0x06000245 RID: 581 RVA: 0x00006718 File Offset: 0x00004B18
	public void StopWithoutReleaseTime()
	{
		if (this.isAvailable)
		{
			CriAtomExPlayer.criAtomExPlayer_StopWithoutReleaseTime(this.handle);
		}
	}

	// Token: 0x06000246 RID: 582 RVA: 0x00006730 File Offset: 0x00004B30
	public void Pause(bool sw)
	{
		CriAtomExPlayer.criAtomExPlayer_Pause(this.handle, sw);
	}

	// Token: 0x06000247 RID: 583 RVA: 0x00006740 File Offset: 0x00004B40
	~CriAtomExPlayer()
	{
		this.Dispose();
	}

	// Token: 0x06000248 RID: 584
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExPlayer_Create(ref CriAtomExPlayer.Config config, IntPtr work, int work_size);

	// Token: 0x06000249 RID: 585
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_Destroy(IntPtr player);

	// Token: 0x0600024A RID: 586
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetCueId(IntPtr player, IntPtr acb_hn, int id);

	// Token: 0x0600024B RID: 587
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetCueName(IntPtr player, IntPtr acb_hn, string cue_name);

	// Token: 0x0600024C RID: 588
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetCueIndex(IntPtr player, IntPtr acb_hn, int index);

	// Token: 0x0600024D RID: 589
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetFile(IntPtr player, IntPtr binder, string path);

	// Token: 0x0600024E RID: 590
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetData(IntPtr player, byte[] buffer, int size);

	// Token: 0x0600024F RID: 591
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetContentId(IntPtr player, IntPtr binder, int id);

	// Token: 0x06000250 RID: 592
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetVoicePoolIdentifier(IntPtr player, uint identifier);

	// Token: 0x06000251 RID: 593
	[DllImport("cri_ware_unity")]
	private static extern uint criAtomExPlayer_Start(IntPtr player);

	// Token: 0x06000252 RID: 594
	[DllImport("cri_ware_unity")]
	private static extern uint criAtomExPlayer_Prepare(IntPtr player);

	// Token: 0x06000253 RID: 595
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_Stop(IntPtr player);

	// Token: 0x06000254 RID: 596
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_StopWithoutReleaseTime(IntPtr player);

	// Token: 0x06000255 RID: 597
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_Pause(IntPtr player, bool sw);

	// Token: 0x06000256 RID: 598
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_Resume(IntPtr player, CriAtomEx.ResumeMode mode);

	// Token: 0x06000257 RID: 599
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExPlayer_IsPaused(IntPtr player);

	// Token: 0x06000258 RID: 600
	[DllImport("cri_ware_unity")]
	private static extern CriAtomExPlayer.Status criAtomExPlayer_GetStatus(IntPtr player);

	// Token: 0x06000259 RID: 601
	[DllImport("cri_ware_unity")]
	private static extern long criAtomExPlayer_GetTime(IntPtr player);

	// Token: 0x0600025A RID: 602
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetFormat(IntPtr player, CriAtomEx.Format format);

	// Token: 0x0600025B RID: 603
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetNumChannels(IntPtr player, int num_channels);

	// Token: 0x0600025C RID: 604
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetSamplingRate(IntPtr player, int sampling_rate);

	// Token: 0x0600025D RID: 605
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetStartTime(IntPtr player, long start_time_ms);

	// Token: 0x0600025E RID: 606
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetSequencePrepareTime(IntPtr player, uint seq_prep_time_ms);

	// Token: 0x0600025F RID: 607
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_LimitLoopCount(IntPtr player, int count);

	// Token: 0x06000260 RID: 608
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_Update(IntPtr player, uint id);

	// Token: 0x06000261 RID: 609
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_UpdateAll(IntPtr player);

	// Token: 0x06000262 RID: 610
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_ResetParameters(IntPtr player);

	// Token: 0x06000263 RID: 611
	[DllImport("cri_ware_unity")]
	private static extern float criAtomExPlayer_GetParameterFloat32(IntPtr player, CriAtomEx.Parameter id);

	// Token: 0x06000264 RID: 612
	[DllImport("cri_ware_unity")]
	private static extern uint criAtomExPlayer_GetParameterUint32(IntPtr player, CriAtomEx.Parameter id);

	// Token: 0x06000265 RID: 613
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExPlayer_GetParameterSint32(IntPtr player, CriAtomEx.Parameter id);

	// Token: 0x06000266 RID: 614
	[DllImport("cri_ware_unity")]
	private static extern IntPtr criAtomExPlayer_GetPlayerParameter(IntPtr player);

	// Token: 0x06000267 RID: 615
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayerParameter_RemoveParameter(IntPtr player_parameter, ushort id);

	// Token: 0x06000268 RID: 616
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetVolume(IntPtr player, float volume);

	// Token: 0x06000269 RID: 617
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetPitch(IntPtr player, float pitch);

	// Token: 0x0600026A RID: 618
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetPan3dAngle(IntPtr player, float pan3d_angle);

	// Token: 0x0600026B RID: 619
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetPan3dInteriorDistance(IntPtr player, float pan3d_interior_distance);

	// Token: 0x0600026C RID: 620
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetPan3dVolume(IntPtr player, float pan3d_volume);

	// Token: 0x0600026D RID: 621
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetPanType(IntPtr player, CriAtomEx.PanType panType);

	// Token: 0x0600026E RID: 622
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetSendLevel(IntPtr player, int channel, CriAtomEx.Speaker id, float level);

	// Token: 0x0600026F RID: 623
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetBusSendLevel(IntPtr player, int bus_id, float level);

	// Token: 0x06000270 RID: 624
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetBusSendLevelByName(IntPtr player, string bus_name, float level);

	// Token: 0x06000271 RID: 625
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetBusSendLevelOffset(IntPtr player, int bus_id, float level_offset);

	// Token: 0x06000272 RID: 626
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetBusSendLevelOffsetByName(IntPtr player, string bus_name, float level_offset);

	// Token: 0x06000273 RID: 627
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetBandpassFilterParameters(IntPtr player, float cof_low, float cof_high);

	// Token: 0x06000274 RID: 628
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetBiquadFilterParameters(IntPtr player, CriAtomEx.BiquadFilterType type, float frequency, float gain, float q);

	// Token: 0x06000275 RID: 629
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetVoicePriority(IntPtr player, int priority);

	// Token: 0x06000276 RID: 630
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetVoiceControlMethod(IntPtr player, CriAtomEx.VoiceControlMethod method);

	// Token: 0x06000277 RID: 631
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetAisacControlById(IntPtr player, ushort control_id, float control_value);

	// Token: 0x06000278 RID: 632
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetAisacControlByName(IntPtr player, string control_name, float control_value);

	// Token: 0x06000279 RID: 633
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_Set3dSourceHn(IntPtr player, IntPtr source);

	// Token: 0x0600027A RID: 634
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_Set3dListenerHn(IntPtr player, IntPtr listener);

	// Token: 0x0600027B RID: 635
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetCategoryById(IntPtr player, uint category_id);

	// Token: 0x0600027C RID: 636
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetCategoryByName(IntPtr player, string category_name);

	// Token: 0x0600027D RID: 637
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_UnsetCategory(IntPtr player);

	// Token: 0x0600027E RID: 638
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetCuePriority(IntPtr player, int cue_priority);

	// Token: 0x0600027F RID: 639
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetEnvelopeAttackTime(IntPtr player, float attack_time_ms);

	// Token: 0x06000280 RID: 640
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetEnvelopeHoldTime(IntPtr player, float hold_time_ms);

	// Token: 0x06000281 RID: 641
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetEnvelopeDecayTime(IntPtr player, float decay_time_ms);

	// Token: 0x06000282 RID: 642
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetEnvelopeReleaseTime(IntPtr player, float release_time_ms);

	// Token: 0x06000283 RID: 643
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetEnvelopeSustainLevel(IntPtr player, float susutain_level);

	// Token: 0x06000284 RID: 644
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_AttachFader(IntPtr player, IntPtr config, IntPtr work, int work_size);

	// Token: 0x06000285 RID: 645
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_DetachFader(IntPtr player);

	// Token: 0x06000286 RID: 646
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetFadeOutTime(IntPtr player, int ms);

	// Token: 0x06000287 RID: 647
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetFadeInTime(IntPtr player, int ms);

	// Token: 0x06000288 RID: 648
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetFadeInStartOffset(IntPtr player, int ms);

	// Token: 0x06000289 RID: 649
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetFadeOutEndDelay(IntPtr player, int ms);

	// Token: 0x0600028A RID: 650
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExPlayer_IsFading(IntPtr player);

	// Token: 0x0600028B RID: 651
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_ResetFaderParameters(IntPtr player);

	// Token: 0x0600028C RID: 652
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetGroupNumber(IntPtr player, int group_no);

	// Token: 0x0600028D RID: 653
	[DllImport("cri_ware_unity")]
	private static extern bool criAtomExPlayer_GetAttachedAisacInfo(IntPtr player, int aisac_attached_index, IntPtr aisac_info);

	// Token: 0x0600028E RID: 654
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetFirstBlockIndex(IntPtr player, int index);

	// Token: 0x0600028F RID: 655
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetSelectorLabel(IntPtr player, string selector, string label);

	// Token: 0x06000290 RID: 656
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_ClearSelectorLabels(IntPtr player);

	// Token: 0x06000291 RID: 657
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetSoundRendererType(IntPtr player, CriAtomEx.SoundRendererType type);

	// Token: 0x06000292 RID: 658
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetRandomSeed(IntPtr player, uint seed);

	// Token: 0x06000293 RID: 659
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_Loop(IntPtr player, bool sw);

	// Token: 0x06000294 RID: 660
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetAsrRackId(IntPtr player, int asr_rack_id);

	// Token: 0x06000295 RID: 661
	[DllImport("cri_ware_unity")]
	private static extern void criAtomExPlayer_SetDspParameter(IntPtr player, int id, float value);

	// Token: 0x04000147 RID: 327
	private bool hasExistingNativeHandle;

	// Token: 0x04000148 RID: 328
	private IntPtr handle = IntPtr.Zero;

	// Token: 0x02000043 RID: 67
	public enum Status
	{
		// Token: 0x0400014A RID: 330
		Stop,
		// Token: 0x0400014B RID: 331
		Prep,
		// Token: 0x0400014C RID: 332
		Playing,
		// Token: 0x0400014D RID: 333
		PlayEnd,
		// Token: 0x0400014E RID: 334
		Error
	}

	// Token: 0x02000044 RID: 68
	private struct Config
	{
		// Token: 0x0400014F RID: 335
		public CriAtomEx.VoiceAllocationMethod voiceAllocationMethod;

		// Token: 0x04000150 RID: 336
		public int maxPathStrings;

		// Token: 0x04000151 RID: 337
		public int maxPath;

		// Token: 0x04000152 RID: 338
		public bool updatesTime;

		// Token: 0x04000153 RID: 339
		public bool enableAudioSyncedTimer;
	}

	// Token: 0x02000045 RID: 69
	public enum TimeStretchParameterId
	{
		// Token: 0x04000155 RID: 341
		Ratio,
		// Token: 0x04000156 RID: 342
		FrameTime,
		// Token: 0x04000157 RID: 343
		Quality
	}

	// Token: 0x02000046 RID: 70
	public enum PitchShifterParameterId
	{
		// Token: 0x04000159 RID: 345
		Pitch,
		// Token: 0x0400015A RID: 346
		Formant,
		// Token: 0x0400015B RID: 347
		Mode
	}
}
