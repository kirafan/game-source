﻿using System;
using System.Runtime.InteropServices;

// Token: 0x020000A0 RID: 160
public static class CriAtomExDebug
{
	// Token: 0x060004DA RID: 1242 RVA: 0x0000B9D4 File Offset: 0x00009DD4
	public static void GetResourcesInfo(out CriAtomExDebug.ResourcesInfo resourcesInfo)
	{
		CriAtomExDebug.criAtomExDebug_GetResourcesInfo(out resourcesInfo);
	}

	// Token: 0x060004DB RID: 1243
	[DllImport("cri_ware_unity")]
	private static extern int criAtomExDebug_GetResourcesInfo(out CriAtomExDebug.ResourcesInfo resourcesInfo);

	// Token: 0x020000A1 RID: 161
	public struct ResourcesInfo
	{
		// Token: 0x040002EE RID: 750
		public CriAtomEx.ResourceUsage virtualVoiceUsage;

		// Token: 0x040002EF RID: 751
		public CriAtomEx.ResourceUsage sequenceUsage;

		// Token: 0x040002F0 RID: 752
		public CriAtomEx.ResourceUsage sequenceTrackUsage;

		// Token: 0x040002F1 RID: 753
		public CriAtomEx.ResourceUsage sequenceTrackItemUsage;
	}
}
