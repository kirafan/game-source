﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

// Token: 0x0200009F RID: 159
[AddComponentMenu("CRIWARE/Library Initializer")]
public class CriWareInitializer : MonoBehaviour
{
	// Token: 0x060004D1 RID: 1233 RVA: 0x0000B3FC File Offset: 0x000097FC
	private void Awake()
	{
		CriWare.CheckBinaryVersionCompatibility();
		if (this.dontInitializeOnAwake)
		{
			return;
		}
		this.Initialize();
	}

	// Token: 0x060004D2 RID: 1234 RVA: 0x0000B416 File Offset: 0x00009816
	private void OnEnable()
	{
	}

	// Token: 0x060004D3 RID: 1235 RVA: 0x0000B418 File Offset: 0x00009818
	private void Start()
	{
	}

	// Token: 0x060004D4 RID: 1236 RVA: 0x0000B41A File Offset: 0x0000981A
	private void Update()
	{
	}

	// Token: 0x060004D5 RID: 1237 RVA: 0x0000B41C File Offset: 0x0000981C
	public void Initialize()
	{
		CriWareInitializer.initializationCount++;
		if (CriWareInitializer.initializationCount != 1)
		{
			UnityEngine.Object.Destroy(this);
			return;
		}
		if (this.initializesFileSystem)
		{
			CriFsPlugin.SetConfigParameters(this.fileSystemConfig.numberOfLoaders, this.fileSystemConfig.numberOfBinders, this.fileSystemConfig.numberOfInstallers, this.fileSystemConfig.installBufferSize * 1024, this.fileSystemConfig.maxPath, this.fileSystemConfig.minimizeFileDescriptorUsage);
			if (this.fileSystemConfig.androidDeviceReadBitrate == 0)
			{
				this.fileSystemConfig.androidDeviceReadBitrate = 50000000;
			}
			CriFsPlugin.SetConfigAdditionalParameters_ANDROID(this.fileSystemConfig.androidDeviceReadBitrate);
			CriFsPlugin.InitializeLibrary();
			if (this.fileSystemConfig.userAgentString.Length != 0)
			{
				CriFsUtility.SetUserAgentString(this.fileSystemConfig.userAgentString);
			}
		}
		if (this.initializesAtom)
		{
			CriAtomPlugin.SetConfigParameters(Math.Max(this.atomConfig.maxVirtualVoices, CriAtomPlugin.GetRequiredMaxVirtualVoices(this.atomConfig)), this.atomConfig.maxVoiceLimitGroups, this.atomConfig.maxCategories, this.atomConfig.maxSequenceEventsPerFrame, this.atomConfig.maxBeatSyncCallbacksPerFrame, this.atomConfig.standardVoicePoolConfig.memoryVoices, this.atomConfig.standardVoicePoolConfig.streamingVoices, this.atomConfig.hcaMxVoicePoolConfig.memoryVoices, this.atomConfig.hcaMxVoicePoolConfig.streamingVoices, this.atomConfig.outputSamplingRate, this.atomConfig.asrOutputChannels, this.atomConfig.usesInGamePreview, this.atomConfig.serverFrequency, this.atomConfig.maxParameterBlocks, this.atomConfig.categoriesPerPlayback, this.atomConfig.maxBuses, this.atomConfig.vrMode);
			CriAtomPlugin.SetConfigAdditionalParameters_PC((long)this.atomConfig.pcBufferingTime);
			CriAtomPlugin.SetConfigAdditionalParameters_IOS((uint)Math.Max(this.atomConfig.iosBufferingTime, 16), this.atomConfig.iosOverrideIPodMusic);
			if (this.atomConfig.androidBufferingTime == 0)
			{
				this.atomConfig.androidBufferingTime = (int)(4000.0 / (double)this.atomConfig.serverFrequency);
			}
			if (this.atomConfig.androidStartBufferingTime == 0)
			{
				this.atomConfig.androidStartBufferingTime = (int)(3000.0 / (double)this.atomConfig.serverFrequency);
			}
			IntPtr android_context = IntPtr.Zero;
			if (this.atomConfig.androidUsesAndroidFastMixer)
			{
				AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject @static = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
				android_context = @static.GetRawObject();
			}
			CriAtomPlugin.SetConfigAdditionalParameters_ANDROID(this.atomConfig.androidLowLatencyStandardVoicePoolConfig.memoryVoices, this.atomConfig.androidLowLatencyStandardVoicePoolConfig.streamingVoices, this.atomConfig.androidBufferingTime, this.atomConfig.androidStartBufferingTime, android_context);
			CriAtomPlugin.SetConfigAdditionalParameters_VITA(this.atomConfig.vitaAtrac9VoicePoolConfig.memoryVoices, this.atomConfig.vitaAtrac9VoicePoolConfig.streamingVoices, (!this.initializesMana) ? 0 : this.manaConfig.numberOfDecoders);
			this.atomConfig.ps4Audio3dConfig.useAudio3D |= this.atomConfig.vrMode;
			CriAtomPlugin.SetConfigAdditionalParameters_PS4(this.atomConfig.ps4Atrac9VoicePoolConfig.memoryVoices, this.atomConfig.ps4Atrac9VoicePoolConfig.streamingVoices, this.atomConfig.ps4Audio3dConfig.useAudio3D, this.atomConfig.ps4Audio3dConfig.voicePoolConfig.memoryVoices, this.atomConfig.ps4Audio3dConfig.voicePoolConfig.streamingVoices);
			CriAtomPlugin.SetConfigAdditionalParameters_WEBGL(this.atomConfig.webglWebAudioVoicePoolConfig.voices);
			CriAtomPlugin.InitializeLibrary();
			if (this.atomConfig.useRandomSeedWithTime)
			{
				CriAtomEx.SetRandomSeed((uint)DateTime.Now.Ticks);
			}
			if (this.atomConfig.acfFileName.Length != 0)
			{
				string text = this.atomConfig.acfFileName;
				if (CriWare.IsStreamingAssetsPath(text))
				{
					text = Path.Combine(CriWare.streamingAssetsPath, text);
				}
				CriAtomEx.RegisterAcf(null, text);
			}
		}
		if (this.initializesMana)
		{
			CriManaPlugin.SetConfigParameters(this.manaConfig.graphicsMultiThreaded, this.manaConfig.numberOfDecoders, this.manaConfig.numberOfMaxEntries);
			CriManaPlugin.SetConfigAdditonalParameters_ANDROID(true);
			CriManaPlugin.InitializeLibrary();
		}
		if (this.useDecrypter)
		{
			ulong key = (this.decrypterConfig.key.Length != 0) ? Convert.ToUInt64(this.decrypterConfig.key) : 0UL;
			string text2 = this.decrypterConfig.authenticationFile;
			if (CriWare.IsStreamingAssetsPath(text2))
			{
				text2 = Path.Combine(CriWare.streamingAssetsPath, text2);
			}
			CriWare.criWareUnity_SetDecryptionKey(key, text2, this.decrypterConfig.enableAtomDecryption, this.decrypterConfig.enableManaDecryption);
		}
		else
		{
			CriWare.criWareUnity_SetDecryptionKey(0UL, string.Empty, false, false);
		}
		if (this.dontDestroyOnLoad)
		{
			UnityEngine.Object.DontDestroyOnLoad(base.transform.gameObject);
			UnityEngine.Object.DontDestroyOnLoad(CriWare.managerObject);
		}
	}

	// Token: 0x060004D6 RID: 1238 RVA: 0x0000B928 File Offset: 0x00009D28
	private void OnDestroy()
	{
		CriWareInitializer.initializationCount--;
		if (CriWareInitializer.initializationCount != 0)
		{
			return;
		}
		if (this.initializesMana)
		{
			CriManaPlugin.FinalizeLibrary();
		}
		if (this.initializesAtom)
		{
			while (CriAtomExLatencyEstimator.GetCurrentInfo().status != CriAtomExLatencyEstimator.Status.Stop)
			{
				CriAtomExLatencyEstimator.FinalizeModule();
			}
			CriAtomPlugin.FinalizeLibrary();
		}
		if (this.initializesFileSystem)
		{
			CriFsPlugin.FinalizeLibrary();
		}
	}

	// Token: 0x060004D7 RID: 1239 RVA: 0x0000B998 File Offset: 0x00009D98
	public static bool IsInitialized()
	{
		if (CriWareInitializer.initializationCount > 0)
		{
			return true;
		}
		CriWare.CheckBinaryVersionCompatibility();
		return false;
	}

	// Token: 0x060004D8 RID: 1240 RVA: 0x0000B9B0 File Offset: 0x00009DB0
	public static void AddAudioEffectInterface(IntPtr effect_interface)
	{
		List<IntPtr> list = null;
		if (CriAtomPlugin.GetAudioEffectInterfaceList(out list))
		{
			list.Add(effect_interface);
		}
	}

	// Token: 0x040002E3 RID: 739
	public bool initializesFileSystem = true;

	// Token: 0x040002E4 RID: 740
	public CriFsConfig fileSystemConfig = new CriFsConfig();

	// Token: 0x040002E5 RID: 741
	public bool initializesAtom = true;

	// Token: 0x040002E6 RID: 742
	public CriAtomConfig atomConfig = new CriAtomConfig();

	// Token: 0x040002E7 RID: 743
	public bool initializesMana = true;

	// Token: 0x040002E8 RID: 744
	public CriManaConfig manaConfig = new CriManaConfig();

	// Token: 0x040002E9 RID: 745
	public bool useDecrypter;

	// Token: 0x040002EA RID: 746
	public CriWareDecrypterConfig decrypterConfig = new CriWareDecrypterConfig();

	// Token: 0x040002EB RID: 747
	public bool dontInitializeOnAwake;

	// Token: 0x040002EC RID: 748
	public bool dontDestroyOnLoad;

	// Token: 0x040002ED RID: 749
	private static int initializationCount;
}
