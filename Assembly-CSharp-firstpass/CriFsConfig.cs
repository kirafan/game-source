﻿using System;

// Token: 0x02000092 RID: 146
[Serializable]
public class CriFsConfig
{
	// Token: 0x040002A3 RID: 675
	public const int defaultAndroidDeviceReadBitrate = 50000000;

	// Token: 0x040002A4 RID: 676
	public int numberOfLoaders = 16;

	// Token: 0x040002A5 RID: 677
	public int numberOfBinders = 8;

	// Token: 0x040002A6 RID: 678
	public int numberOfInstallers = 2;

	// Token: 0x040002A7 RID: 679
	public int installBufferSize = CriFsPlugin.defaultInstallBufferSize / 1024;

	// Token: 0x040002A8 RID: 680
	public int maxPath = 256;

	// Token: 0x040002A9 RID: 681
	public string userAgentString = string.Empty;

	// Token: 0x040002AA RID: 682
	public bool minimizeFileDescriptorUsage;

	// Token: 0x040002AB RID: 683
	public int androidDeviceReadBitrate = 50000000;
}
