﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000151 RID: 337
	public sealed class SphereCollider : Collider
	{
		// Token: 0x170005A4 RID: 1444
		// (get) Token: 0x0600187E RID: 6270 RVA: 0x0001EA1C File Offset: 0x0001CC1C
		// (set) Token: 0x0600187F RID: 6271 RVA: 0x0001EA3C File Offset: 0x0001CC3C
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_center(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_center(ref value);
			}
		}

		// Token: 0x06001880 RID: 6272
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_center(out Vector3 value);

		// Token: 0x06001881 RID: 6273
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_center(ref Vector3 value);

		// Token: 0x170005A5 RID: 1445
		// (get) Token: 0x06001882 RID: 6274
		// (set) Token: 0x06001883 RID: 6275
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
