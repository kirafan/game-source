﻿using System;

namespace UnityEngine
{
	// Token: 0x020000A6 RID: 166
	[Flags]
	public enum DrivenTransformProperties
	{
		// Token: 0x0400017B RID: 379
		None = 0,
		// Token: 0x0400017C RID: 380
		All = -1,
		// Token: 0x0400017D RID: 381
		AnchoredPositionX = 2,
		// Token: 0x0400017E RID: 382
		AnchoredPositionY = 4,
		// Token: 0x0400017F RID: 383
		AnchoredPositionZ = 8,
		// Token: 0x04000180 RID: 384
		Rotation = 16,
		// Token: 0x04000181 RID: 385
		ScaleX = 32,
		// Token: 0x04000182 RID: 386
		ScaleY = 64,
		// Token: 0x04000183 RID: 387
		ScaleZ = 128,
		// Token: 0x04000184 RID: 388
		AnchorMinX = 256,
		// Token: 0x04000185 RID: 389
		AnchorMinY = 512,
		// Token: 0x04000186 RID: 390
		AnchorMaxX = 1024,
		// Token: 0x04000187 RID: 391
		AnchorMaxY = 2048,
		// Token: 0x04000188 RID: 392
		SizeDeltaX = 4096,
		// Token: 0x04000189 RID: 393
		SizeDeltaY = 8192,
		// Token: 0x0400018A RID: 394
		PivotX = 16384,
		// Token: 0x0400018B RID: 395
		PivotY = 32768,
		// Token: 0x0400018C RID: 396
		AnchoredPosition = 6,
		// Token: 0x0400018D RID: 397
		AnchoredPosition3D = 14,
		// Token: 0x0400018E RID: 398
		Scale = 224,
		// Token: 0x0400018F RID: 399
		AnchorMin = 768,
		// Token: 0x04000190 RID: 400
		AnchorMax = 3072,
		// Token: 0x04000191 RID: 401
		Anchors = 3840,
		// Token: 0x04000192 RID: 402
		SizeDelta = 12288,
		// Token: 0x04000193 RID: 403
		Pivot = 49152
	}
}
