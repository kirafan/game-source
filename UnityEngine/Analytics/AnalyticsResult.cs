﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000287 RID: 647
	public enum AnalyticsResult
	{
		// Token: 0x040009D7 RID: 2519
		Ok,
		// Token: 0x040009D8 RID: 2520
		NotInitialized,
		// Token: 0x040009D9 RID: 2521
		AnalyticsDisabled,
		// Token: 0x040009DA RID: 2522
		TooManyItems,
		// Token: 0x040009DB RID: 2523
		SizeLimitReached,
		// Token: 0x040009DC RID: 2524
		TooManyRequests,
		// Token: 0x040009DD RID: 2525
		InvalidData,
		// Token: 0x040009DE RID: 2526
		UnsupportedPlatform
	}
}
