﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine.Analytics
{
	// Token: 0x02000285 RID: 645
	[StructLayout(LayoutKind.Sequential)]
	internal class CustomEventData : IDisposable
	{
		// Token: 0x06002A0F RID: 10767 RVA: 0x0003E354 File Offset: 0x0003C554
		private CustomEventData()
		{
		}

		// Token: 0x06002A10 RID: 10768 RVA: 0x0003E360 File Offset: 0x0003C560
		public CustomEventData(string name)
		{
			this.InternalCreate(name);
		}

		// Token: 0x06002A11 RID: 10769 RVA: 0x0003E370 File Offset: 0x0003C570
		~CustomEventData()
		{
			this.InternalDestroy();
		}

		// Token: 0x06002A12 RID: 10770 RVA: 0x0003E3A0 File Offset: 0x0003C5A0
		public void Dispose()
		{
			this.InternalDestroy();
			GC.SuppressFinalize(this);
		}

		// Token: 0x06002A13 RID: 10771 RVA: 0x0003E3B0 File Offset: 0x0003C5B0
		public bool Add(string key, string value)
		{
			return this.AddString(key, value);
		}

		// Token: 0x06002A14 RID: 10772 RVA: 0x0003E3D0 File Offset: 0x0003C5D0
		public bool Add(string key, bool value)
		{
			return this.AddBool(key, value);
		}

		// Token: 0x06002A15 RID: 10773 RVA: 0x0003E3F0 File Offset: 0x0003C5F0
		public bool Add(string key, char value)
		{
			return this.AddChar(key, value);
		}

		// Token: 0x06002A16 RID: 10774 RVA: 0x0003E410 File Offset: 0x0003C610
		public bool Add(string key, byte value)
		{
			return this.AddByte(key, value);
		}

		// Token: 0x06002A17 RID: 10775 RVA: 0x0003E430 File Offset: 0x0003C630
		public bool Add(string key, sbyte value)
		{
			return this.AddSByte(key, value);
		}

		// Token: 0x06002A18 RID: 10776 RVA: 0x0003E450 File Offset: 0x0003C650
		public bool Add(string key, short value)
		{
			return this.AddInt16(key, value);
		}

		// Token: 0x06002A19 RID: 10777 RVA: 0x0003E470 File Offset: 0x0003C670
		public bool Add(string key, ushort value)
		{
			return this.AddUInt16(key, value);
		}

		// Token: 0x06002A1A RID: 10778 RVA: 0x0003E490 File Offset: 0x0003C690
		public bool Add(string key, int value)
		{
			return this.AddInt32(key, value);
		}

		// Token: 0x06002A1B RID: 10779 RVA: 0x0003E4B0 File Offset: 0x0003C6B0
		public bool Add(string key, uint value)
		{
			return this.AddUInt32(key, value);
		}

		// Token: 0x06002A1C RID: 10780 RVA: 0x0003E4D0 File Offset: 0x0003C6D0
		public bool Add(string key, long value)
		{
			return this.AddInt64(key, value);
		}

		// Token: 0x06002A1D RID: 10781 RVA: 0x0003E4F0 File Offset: 0x0003C6F0
		public bool Add(string key, ulong value)
		{
			return this.AddUInt64(key, value);
		}

		// Token: 0x06002A1E RID: 10782 RVA: 0x0003E510 File Offset: 0x0003C710
		public bool Add(string key, float value)
		{
			return this.AddDouble(key, (double)Convert.ToDecimal(value));
		}

		// Token: 0x06002A1F RID: 10783 RVA: 0x0003E538 File Offset: 0x0003C738
		public bool Add(string key, double value)
		{
			return this.AddDouble(key, value);
		}

		// Token: 0x06002A20 RID: 10784 RVA: 0x0003E558 File Offset: 0x0003C758
		public bool Add(string key, decimal value)
		{
			return this.AddDouble(key, (double)Convert.ToDecimal(value));
		}

		// Token: 0x06002A21 RID: 10785 RVA: 0x0003E580 File Offset: 0x0003C780
		public bool Add(IDictionary<string, object> eventData)
		{
			foreach (KeyValuePair<string, object> keyValuePair in eventData)
			{
				string key = keyValuePair.Key;
				object value = keyValuePair.Value;
				if (value == null)
				{
					this.Add(key, "null");
				}
				else
				{
					Type type = value.GetType();
					if (type == typeof(string))
					{
						this.Add(key, (string)value);
					}
					else if (type == typeof(char))
					{
						this.Add(key, (char)value);
					}
					else if (type == typeof(sbyte))
					{
						this.Add(key, (sbyte)value);
					}
					else if (type == typeof(byte))
					{
						this.Add(key, (byte)value);
					}
					else if (type == typeof(short))
					{
						this.Add(key, (short)value);
					}
					else if (type == typeof(ushort))
					{
						this.Add(key, (ushort)value);
					}
					else if (type == typeof(int))
					{
						this.Add(key, (int)value);
					}
					else if (type == typeof(uint))
					{
						this.Add(keyValuePair.Key, (uint)value);
					}
					else if (type == typeof(long))
					{
						this.Add(key, (long)value);
					}
					else if (type == typeof(ulong))
					{
						this.Add(key, (ulong)value);
					}
					else if (type == typeof(bool))
					{
						this.Add(key, (bool)value);
					}
					else if (type == typeof(float))
					{
						this.Add(key, (float)value);
					}
					else if (type == typeof(double))
					{
						this.Add(key, (double)value);
					}
					else if (type == typeof(decimal))
					{
						this.Add(key, (decimal)value);
					}
					else
					{
						if (!type.IsValueType)
						{
							throw new ArgumentException(string.Format("Invalid type: {0} passed", type));
						}
						this.Add(key, value.ToString());
					}
				}
			}
			return true;
		}

		// Token: 0x06002A22 RID: 10786
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InternalCreate(string name);

		// Token: 0x06002A23 RID: 10787
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalDestroy();

		// Token: 0x06002A24 RID: 10788
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddString(string key, string value);

		// Token: 0x06002A25 RID: 10789
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddBool(string key, bool value);

		// Token: 0x06002A26 RID: 10790
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddChar(string key, char value);

		// Token: 0x06002A27 RID: 10791
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddByte(string key, byte value);

		// Token: 0x06002A28 RID: 10792
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddSByte(string key, sbyte value);

		// Token: 0x06002A29 RID: 10793
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddInt16(string key, short value);

		// Token: 0x06002A2A RID: 10794
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddUInt16(string key, ushort value);

		// Token: 0x06002A2B RID: 10795
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddInt32(string key, int value);

		// Token: 0x06002A2C RID: 10796
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddUInt32(string key, uint value);

		// Token: 0x06002A2D RID: 10797
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddInt64(string key, long value);

		// Token: 0x06002A2E RID: 10798
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddUInt64(string key, ulong value);

		// Token: 0x06002A2F RID: 10799
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddDouble(string key, double value);

		// Token: 0x040009D1 RID: 2513
		[NonSerialized]
		internal IntPtr m_Ptr;
	}
}
