﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Analytics
{
	// Token: 0x02000288 RID: 648
	public static class Analytics
	{
		// Token: 0x06002A30 RID: 10800 RVA: 0x0003E848 File Offset: 0x0003CA48
		internal static UnityAnalyticsHandler GetUnityAnalyticsHandler()
		{
			if (Analytics.s_UnityAnalyticsHandler == null)
			{
				Analytics.s_UnityAnalyticsHandler = new UnityAnalyticsHandler();
			}
			return Analytics.s_UnityAnalyticsHandler;
		}

		// Token: 0x06002A31 RID: 10801 RVA: 0x0003E878 File Offset: 0x0003CA78
		public static AnalyticsResult FlushEvents()
		{
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (unityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = unityAnalyticsHandler.FlushEvents();
			}
			return result;
		}

		// Token: 0x06002A32 RID: 10802 RVA: 0x0003E8A8 File Offset: 0x0003CAA8
		public static AnalyticsResult SetUserId(string userId)
		{
			if (string.IsNullOrEmpty(userId))
			{
				throw new ArgumentException("Cannot set userId to an empty or null string");
			}
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (unityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = unityAnalyticsHandler.SetUserId(userId);
			}
			return result;
		}

		// Token: 0x06002A33 RID: 10803 RVA: 0x0003E8F0 File Offset: 0x0003CAF0
		public static AnalyticsResult SetUserGender(Gender gender)
		{
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (unityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = unityAnalyticsHandler.SetUserGender(gender);
			}
			return result;
		}

		// Token: 0x06002A34 RID: 10804 RVA: 0x0003E920 File Offset: 0x0003CB20
		public static AnalyticsResult SetUserBirthYear(int birthYear)
		{
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (Analytics.s_UnityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = unityAnalyticsHandler.SetUserBirthYear(birthYear);
			}
			return result;
		}

		// Token: 0x06002A35 RID: 10805 RVA: 0x0003E954 File Offset: 0x0003CB54
		public static AnalyticsResult Transaction(string productId, decimal amount, string currency)
		{
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (unityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = unityAnalyticsHandler.Transaction(productId, Convert.ToDouble(amount), currency, null, null);
			}
			return result;
		}

		// Token: 0x06002A36 RID: 10806 RVA: 0x0003E98C File Offset: 0x0003CB8C
		public static AnalyticsResult Transaction(string productId, decimal amount, string currency, string receiptPurchaseData, string signature)
		{
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (unityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = unityAnalyticsHandler.Transaction(productId, Convert.ToDouble(amount), currency, receiptPurchaseData, signature);
			}
			return result;
		}

		// Token: 0x06002A37 RID: 10807 RVA: 0x0003E9C8 File Offset: 0x0003CBC8
		internal static AnalyticsResult Transaction(string productId, decimal amount, string currency, string receiptPurchaseData, string signature, bool usingIAPService)
		{
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (unityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = unityAnalyticsHandler.Transaction(productId, Convert.ToDouble(amount), currency, receiptPurchaseData, signature, usingIAPService);
			}
			return result;
		}

		// Token: 0x06002A38 RID: 10808 RVA: 0x0003EA04 File Offset: 0x0003CC04
		public static AnalyticsResult CustomEvent(string customEventName)
		{
			if (string.IsNullOrEmpty(customEventName))
			{
				throw new ArgumentException("Cannot set custom event name to an empty or null string");
			}
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (unityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				result = unityAnalyticsHandler.CustomEvent(customEventName);
			}
			return result;
		}

		// Token: 0x06002A39 RID: 10809 RVA: 0x0003EA4C File Offset: 0x0003CC4C
		public static AnalyticsResult CustomEvent(string customEventName, Vector3 position)
		{
			if (string.IsNullOrEmpty(customEventName))
			{
				throw new ArgumentException("Cannot set custom event name to an empty or null string");
			}
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (unityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else
			{
				CustomEventData customEventData = new CustomEventData(customEventName);
				customEventData.Add("x", (double)Convert.ToDecimal(position.x));
				customEventData.Add("y", (double)Convert.ToDecimal(position.y));
				customEventData.Add("z", (double)Convert.ToDecimal(position.z));
				result = unityAnalyticsHandler.CustomEvent(customEventData);
			}
			return result;
		}

		// Token: 0x06002A3A RID: 10810 RVA: 0x0003EAF0 File Offset: 0x0003CCF0
		public static AnalyticsResult CustomEvent(string customEventName, IDictionary<string, object> eventData)
		{
			if (string.IsNullOrEmpty(customEventName))
			{
				throw new ArgumentException("Cannot set custom event name to an empty or null string");
			}
			UnityAnalyticsHandler unityAnalyticsHandler = Analytics.GetUnityAnalyticsHandler();
			AnalyticsResult result;
			if (unityAnalyticsHandler == null)
			{
				result = AnalyticsResult.NotInitialized;
			}
			else if (eventData == null)
			{
				result = unityAnalyticsHandler.CustomEvent(customEventName);
			}
			else
			{
				CustomEventData customEventData = new CustomEventData(customEventName);
				customEventData.Add(eventData);
				result = unityAnalyticsHandler.CustomEvent(customEventData);
			}
			return result;
		}

		// Token: 0x040009DF RID: 2527
		private static UnityAnalyticsHandler s_UnityAnalyticsHandler;
	}
}
