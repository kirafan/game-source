﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine.Analytics
{
	// Token: 0x02000289 RID: 649
	[StructLayout(LayoutKind.Sequential)]
	internal sealed class UnityAnalyticsHandler : IDisposable
	{
		// Token: 0x06002A3B RID: 10811 RVA: 0x0003EB58 File Offset: 0x0003CD58
		public UnityAnalyticsHandler()
		{
			this.InternalCreate();
		}

		// Token: 0x06002A3C RID: 10812
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalCreate();

		// Token: 0x06002A3D RID: 10813
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalDestroy();

		// Token: 0x06002A3E RID: 10814 RVA: 0x0003EB68 File Offset: 0x0003CD68
		~UnityAnalyticsHandler()
		{
			this.InternalDestroy();
		}

		// Token: 0x06002A3F RID: 10815 RVA: 0x0003EB98 File Offset: 0x0003CD98
		public void Dispose()
		{
			this.InternalDestroy();
			GC.SuppressFinalize(this);
		}

		// Token: 0x06002A40 RID: 10816
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnalyticsResult FlushEvents();

		// Token: 0x06002A41 RID: 10817
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnalyticsResult SetUserId(string userId);

		// Token: 0x06002A42 RID: 10818
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnalyticsResult SetUserGender(Gender gender);

		// Token: 0x06002A43 RID: 10819
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnalyticsResult SetUserBirthYear(int birthYear);

		// Token: 0x06002A44 RID: 10820
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnalyticsResult Transaction(string productId, double amount, string currency);

		// Token: 0x06002A45 RID: 10821 RVA: 0x0003EBA8 File Offset: 0x0003CDA8
		public AnalyticsResult Transaction(string productId, double amount, string currency, string receiptPurchaseData, string signature)
		{
			return this.Transaction(productId, amount, currency, receiptPurchaseData, signature, false);
		}

		// Token: 0x06002A46 RID: 10822 RVA: 0x0003EBCC File Offset: 0x0003CDCC
		internal AnalyticsResult Transaction(string productId, double amount, string currency, string receiptPurchaseData, string signature, bool usingIAPService)
		{
			if (receiptPurchaseData == null)
			{
				receiptPurchaseData = string.Empty;
			}
			if (signature == null)
			{
				signature = string.Empty;
			}
			return this.InternalTransaction(productId, amount, currency, receiptPurchaseData, signature, usingIAPService);
		}

		// Token: 0x06002A47 RID: 10823
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnalyticsResult InternalTransaction(string productId, double amount, string currency, string receiptPurchaseData, string signature, bool usingIAPService);

		// Token: 0x06002A48 RID: 10824 RVA: 0x0003EC0C File Offset: 0x0003CE0C
		public AnalyticsResult CustomEvent(string customEventName)
		{
			return this.SendCustomEventName(customEventName);
		}

		// Token: 0x06002A49 RID: 10825 RVA: 0x0003EC28 File Offset: 0x0003CE28
		public AnalyticsResult CustomEvent(CustomEventData eventData)
		{
			return this.SendCustomEvent(eventData);
		}

		// Token: 0x06002A4A RID: 10826
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnalyticsResult SendCustomEventName(string customEventName);

		// Token: 0x06002A4B RID: 10827
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnalyticsResult SendCustomEvent(CustomEventData eventData);

		// Token: 0x040009E0 RID: 2528
		[NonSerialized]
		internal IntPtr m_Ptr;
	}
}
