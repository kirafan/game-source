﻿using System;

namespace UnityEngine.Analytics
{
	// Token: 0x02000286 RID: 646
	public enum Gender
	{
		// Token: 0x040009D3 RID: 2515
		Male,
		// Token: 0x040009D4 RID: 2516
		Female,
		// Token: 0x040009D5 RID: 2517
		Unknown
	}
}
