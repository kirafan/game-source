﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200002B RID: 43
	public sealed class CrashReport
	{
		// Token: 0x0600035E RID: 862 RVA: 0x00005F80 File Offset: 0x00004180
		private CrashReport(string id, DateTime time, string text)
		{
			this.id = id;
			this.time = time;
			this.text = text;
		}

		// Token: 0x0600035F RID: 863 RVA: 0x00005FA0 File Offset: 0x000041A0
		private static int Compare(CrashReport c1, CrashReport c2)
		{
			long ticks = c1.time.Ticks;
			long ticks2 = c2.time.Ticks;
			int result;
			if (ticks > ticks2)
			{
				result = 1;
			}
			else if (ticks < ticks2)
			{
				result = -1;
			}
			else
			{
				result = 0;
			}
			return result;
		}

		// Token: 0x06000360 RID: 864 RVA: 0x00005FF4 File Offset: 0x000041F4
		private static void PopulateReports()
		{
			object obj = CrashReport.reportsLock;
			lock (obj)
			{
				if (CrashReport.internalReports == null)
				{
					string[] reports = CrashReport.GetReports();
					CrashReport.internalReports = new List<CrashReport>(reports.Length);
					foreach (string text in reports)
					{
						double value;
						string reportData = CrashReport.GetReportData(text, out value);
						DateTime dateTime = new DateTime(1970, 1, 1);
						DateTime dateTime2 = dateTime.AddSeconds(value);
						CrashReport.internalReports.Add(new CrashReport(text, dateTime2, reportData));
					}
					List<CrashReport> list = CrashReport.internalReports;
					if (CrashReport.<>f__mg$cache0 == null)
					{
						CrashReport.<>f__mg$cache0 = new Comparison<CrashReport>(CrashReport.Compare);
					}
					list.Sort(CrashReport.<>f__mg$cache0);
				}
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000361 RID: 865 RVA: 0x000060D0 File Offset: 0x000042D0
		public static CrashReport[] reports
		{
			get
			{
				CrashReport.PopulateReports();
				object obj = CrashReport.reportsLock;
				CrashReport[] result;
				lock (obj)
				{
					result = CrashReport.internalReports.ToArray();
				}
				return result;
			}
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000362 RID: 866 RVA: 0x00006118 File Offset: 0x00004318
		public static CrashReport lastReport
		{
			get
			{
				CrashReport.PopulateReports();
				object obj = CrashReport.reportsLock;
				lock (obj)
				{
					if (CrashReport.internalReports.Count > 0)
					{
						return CrashReport.internalReports[CrashReport.internalReports.Count - 1];
					}
				}
				return null;
			}
		}

		// Token: 0x06000363 RID: 867 RVA: 0x0000618C File Offset: 0x0000438C
		public static void RemoveAll()
		{
			foreach (CrashReport crashReport in CrashReport.reports)
			{
				crashReport.Remove();
			}
		}

		// Token: 0x06000364 RID: 868 RVA: 0x000061C0 File Offset: 0x000043C0
		public void Remove()
		{
			if (CrashReport.RemoveReport(this.id))
			{
				object obj = CrashReport.reportsLock;
				lock (obj)
				{
					CrashReport.internalReports.Remove(this);
				}
			}
		}

		// Token: 0x06000365 RID: 869
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetReports();

		// Token: 0x06000366 RID: 870
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetReportData(string id, out double secondsSinceUnixEpoch);

		// Token: 0x06000367 RID: 871
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool RemoveReport(string id);

		// Token: 0x0400003A RID: 58
		private static List<CrashReport> internalReports;

		// Token: 0x0400003B RID: 59
		private static object reportsLock = new object();

		// Token: 0x0400003C RID: 60
		private readonly string id;

		// Token: 0x0400003D RID: 61
		public readonly DateTime time;

		// Token: 0x0400003E RID: 62
		public readonly string text;

		// Token: 0x0400003F RID: 63
		[CompilerGenerated]
		private static Comparison<CrashReport> <>f__mg$cache0;
	}
}
