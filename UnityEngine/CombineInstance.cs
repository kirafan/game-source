﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000044 RID: 68
	public struct CombineInstance
	{
		// Token: 0x17000116 RID: 278
		// (get) Token: 0x06000500 RID: 1280 RVA: 0x00007610 File Offset: 0x00005810
		// (set) Token: 0x06000501 RID: 1281 RVA: 0x00007634 File Offset: 0x00005834
		public Mesh mesh
		{
			get
			{
				return this.InternalGetMesh(this.m_MeshInstanceID);
			}
			set
			{
				this.m_MeshInstanceID = ((!(value != null)) ? 0 : value.GetInstanceID());
			}
		}

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x06000502 RID: 1282 RVA: 0x00007658 File Offset: 0x00005858
		// (set) Token: 0x06000503 RID: 1283 RVA: 0x00007674 File Offset: 0x00005874
		public int subMeshIndex
		{
			get
			{
				return this.m_SubMeshIndex;
			}
			set
			{
				this.m_SubMeshIndex = value;
			}
		}

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x06000504 RID: 1284 RVA: 0x00007680 File Offset: 0x00005880
		// (set) Token: 0x06000505 RID: 1285 RVA: 0x0000769C File Offset: 0x0000589C
		public Matrix4x4 transform
		{
			get
			{
				return this.m_Transform;
			}
			set
			{
				this.m_Transform = value;
			}
		}

		// Token: 0x06000506 RID: 1286
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Mesh InternalGetMesh(int instanceID);

		// Token: 0x04000061 RID: 97
		private int m_MeshInstanceID;

		// Token: 0x04000062 RID: 98
		private int m_SubMeshIndex;

		// Token: 0x04000063 RID: 99
		private Matrix4x4 m_Transform;
	}
}
