﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000084 RID: 132
	[UsedByNativeCode]
	public struct Quaternion
	{
		// Token: 0x060008B3 RID: 2227 RVA: 0x0000B11C File Offset: 0x0000931C
		public Quaternion(float x, float y, float z, float w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		// Token: 0x060008B4 RID: 2228 RVA: 0x0000B13C File Offset: 0x0000933C
		[ThreadAndSerializationSafe]
		public static Quaternion AngleAxis(float angle, Vector3 axis)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_AngleAxis(angle, ref axis, out result);
			return result;
		}

		// Token: 0x060008B5 RID: 2229
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AngleAxis(float angle, ref Vector3 axis, out Quaternion value);

		// Token: 0x060008B6 RID: 2230 RVA: 0x0000B15C File Offset: 0x0000935C
		public void ToAngleAxis(out float angle, out Vector3 axis)
		{
			Quaternion.Internal_ToAxisAngleRad(this, out axis, out angle);
			angle *= 57.29578f;
		}

		// Token: 0x060008B7 RID: 2231 RVA: 0x0000B178 File Offset: 0x00009378
		public static Quaternion FromToRotation(Vector3 fromDirection, Vector3 toDirection)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_FromToRotation(ref fromDirection, ref toDirection, out result);
			return result;
		}

		// Token: 0x060008B8 RID: 2232
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_FromToRotation(ref Vector3 fromDirection, ref Vector3 toDirection, out Quaternion value);

		// Token: 0x060008B9 RID: 2233 RVA: 0x0000B19C File Offset: 0x0000939C
		public void SetFromToRotation(Vector3 fromDirection, Vector3 toDirection)
		{
			this = Quaternion.FromToRotation(fromDirection, toDirection);
		}

		// Token: 0x060008BA RID: 2234 RVA: 0x0000B1AC File Offset: 0x000093AC
		public static Quaternion LookRotation(Vector3 forward, [DefaultValue("Vector3.up")] Vector3 upwards)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_LookRotation(ref forward, ref upwards, out result);
			return result;
		}

		// Token: 0x060008BB RID: 2235 RVA: 0x0000B1D0 File Offset: 0x000093D0
		[ExcludeFromDocs]
		public static Quaternion LookRotation(Vector3 forward)
		{
			Vector3 up = Vector3.up;
			Quaternion result;
			Quaternion.INTERNAL_CALL_LookRotation(ref forward, ref up, out result);
			return result;
		}

		// Token: 0x060008BC RID: 2236
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_LookRotation(ref Vector3 forward, ref Vector3 upwards, out Quaternion value);

		// Token: 0x060008BD RID: 2237 RVA: 0x0000B1F8 File Offset: 0x000093F8
		public static Quaternion Slerp(Quaternion a, Quaternion b, float t)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_Slerp(ref a, ref b, t, out result);
			return result;
		}

		// Token: 0x060008BE RID: 2238
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Slerp(ref Quaternion a, ref Quaternion b, float t, out Quaternion value);

		// Token: 0x060008BF RID: 2239 RVA: 0x0000B21C File Offset: 0x0000941C
		public static Quaternion SlerpUnclamped(Quaternion a, Quaternion b, float t)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_SlerpUnclamped(ref a, ref b, t, out result);
			return result;
		}

		// Token: 0x060008C0 RID: 2240
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SlerpUnclamped(ref Quaternion a, ref Quaternion b, float t, out Quaternion value);

		// Token: 0x060008C1 RID: 2241 RVA: 0x0000B240 File Offset: 0x00009440
		public static Quaternion Lerp(Quaternion a, Quaternion b, float t)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_Lerp(ref a, ref b, t, out result);
			return result;
		}

		// Token: 0x060008C2 RID: 2242
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Lerp(ref Quaternion a, ref Quaternion b, float t, out Quaternion value);

		// Token: 0x060008C3 RID: 2243 RVA: 0x0000B264 File Offset: 0x00009464
		public static Quaternion LerpUnclamped(Quaternion a, Quaternion b, float t)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_LerpUnclamped(ref a, ref b, t, out result);
			return result;
		}

		// Token: 0x060008C4 RID: 2244
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_LerpUnclamped(ref Quaternion a, ref Quaternion b, float t, out Quaternion value);

		// Token: 0x060008C5 RID: 2245 RVA: 0x0000B288 File Offset: 0x00009488
		public static Quaternion RotateTowards(Quaternion from, Quaternion to, float maxDegreesDelta)
		{
			float num = Quaternion.Angle(from, to);
			Quaternion result;
			if (num == 0f)
			{
				result = to;
			}
			else
			{
				float t = Mathf.Min(1f, maxDegreesDelta / num);
				result = Quaternion.SlerpUnclamped(from, to, t);
			}
			return result;
		}

		// Token: 0x060008C6 RID: 2246 RVA: 0x0000B2D0 File Offset: 0x000094D0
		public static Quaternion Inverse(Quaternion rotation)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_Inverse(ref rotation, out result);
			return result;
		}

		// Token: 0x060008C7 RID: 2247
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Inverse(ref Quaternion rotation, out Quaternion value);

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x060008C8 RID: 2248 RVA: 0x0000B2F0 File Offset: 0x000094F0
		// (set) Token: 0x060008C9 RID: 2249 RVA: 0x0000B320 File Offset: 0x00009520
		public Vector3 eulerAngles
		{
			get
			{
				return Quaternion.Internal_MakePositive(Quaternion.Internal_ToEulerRad(this) * 57.29578f);
			}
			set
			{
				this = Quaternion.Internal_FromEulerRad(value * 0.017453292f);
			}
		}

		// Token: 0x060008CA RID: 2250 RVA: 0x0000B33C File Offset: 0x0000953C
		public static Quaternion Euler(float x, float y, float z)
		{
			return Quaternion.Internal_FromEulerRad(new Vector3(x, y, z) * 0.017453292f);
		}

		// Token: 0x060008CB RID: 2251 RVA: 0x0000B368 File Offset: 0x00009568
		public static Quaternion Euler(Vector3 euler)
		{
			return Quaternion.Internal_FromEulerRad(euler * 0.017453292f);
		}

		// Token: 0x060008CC RID: 2252 RVA: 0x0000B390 File Offset: 0x00009590
		private static Vector3 Internal_ToEulerRad(Quaternion rotation)
		{
			Vector3 result;
			Quaternion.INTERNAL_CALL_Internal_ToEulerRad(ref rotation, out result);
			return result;
		}

		// Token: 0x060008CD RID: 2253
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_ToEulerRad(ref Quaternion rotation, out Vector3 value);

		// Token: 0x060008CE RID: 2254 RVA: 0x0000B3B0 File Offset: 0x000095B0
		private static Quaternion Internal_FromEulerRad(Vector3 euler)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_Internal_FromEulerRad(ref euler, out result);
			return result;
		}

		// Token: 0x060008CF RID: 2255
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_FromEulerRad(ref Vector3 euler, out Quaternion value);

		// Token: 0x060008D0 RID: 2256 RVA: 0x0000B3D0 File Offset: 0x000095D0
		private static void Internal_ToAxisAngleRad(Quaternion q, out Vector3 axis, out float angle)
		{
			Quaternion.INTERNAL_CALL_Internal_ToAxisAngleRad(ref q, out axis, out angle);
		}

		// Token: 0x060008D1 RID: 2257
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_ToAxisAngleRad(ref Quaternion q, out Vector3 axis, out float angle);

		// Token: 0x060008D2 RID: 2258 RVA: 0x0000B3DC File Offset: 0x000095DC
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion EulerRotation(float x, float y, float z)
		{
			return Quaternion.Internal_FromEulerRad(new Vector3(x, y, z));
		}

		// Token: 0x060008D3 RID: 2259 RVA: 0x0000B400 File Offset: 0x00009600
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion EulerRotation(Vector3 euler)
		{
			return Quaternion.Internal_FromEulerRad(euler);
		}

		// Token: 0x060008D4 RID: 2260 RVA: 0x0000B41C File Offset: 0x0000961C
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetEulerRotation(float x, float y, float z)
		{
			this = Quaternion.Internal_FromEulerRad(new Vector3(x, y, z));
		}

		// Token: 0x060008D5 RID: 2261 RVA: 0x0000B434 File Offset: 0x00009634
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetEulerRotation(Vector3 euler)
		{
			this = Quaternion.Internal_FromEulerRad(euler);
		}

		// Token: 0x060008D6 RID: 2262 RVA: 0x0000B444 File Offset: 0x00009644
		[Obsolete("Use Quaternion.eulerAngles instead. This function was deprecated because it uses radians instead of degrees")]
		public Vector3 ToEuler()
		{
			return Quaternion.Internal_ToEulerRad(this);
		}

		// Token: 0x060008D7 RID: 2263 RVA: 0x0000B464 File Offset: 0x00009664
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion EulerAngles(float x, float y, float z)
		{
			return Quaternion.Internal_FromEulerRad(new Vector3(x, y, z));
		}

		// Token: 0x060008D8 RID: 2264 RVA: 0x0000B488 File Offset: 0x00009688
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion EulerAngles(Vector3 euler)
		{
			return Quaternion.Internal_FromEulerRad(euler);
		}

		// Token: 0x060008D9 RID: 2265 RVA: 0x0000B4A4 File Offset: 0x000096A4
		[Obsolete("Use Quaternion.ToAngleAxis instead. This function was deprecated because it uses radians instead of degrees")]
		public void ToAxisAngle(out Vector3 axis, out float angle)
		{
			Quaternion.Internal_ToAxisAngleRad(this, out axis, out angle);
		}

		// Token: 0x060008DA RID: 2266 RVA: 0x0000B4B4 File Offset: 0x000096B4
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetEulerAngles(float x, float y, float z)
		{
			this.SetEulerRotation(new Vector3(x, y, z));
		}

		// Token: 0x060008DB RID: 2267 RVA: 0x0000B4C8 File Offset: 0x000096C8
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetEulerAngles(Vector3 euler)
		{
			this = Quaternion.EulerRotation(euler);
		}

		// Token: 0x060008DC RID: 2268 RVA: 0x0000B4D8 File Offset: 0x000096D8
		[Obsolete("Use Quaternion.eulerAngles instead. This function was deprecated because it uses radians instead of degrees")]
		public static Vector3 ToEulerAngles(Quaternion rotation)
		{
			return Quaternion.Internal_ToEulerRad(rotation);
		}

		// Token: 0x060008DD RID: 2269 RVA: 0x0000B4F4 File Offset: 0x000096F4
		[Obsolete("Use Quaternion.eulerAngles instead. This function was deprecated because it uses radians instead of degrees")]
		public Vector3 ToEulerAngles()
		{
			return Quaternion.Internal_ToEulerRad(this);
		}

		// Token: 0x060008DE RID: 2270 RVA: 0x0000B514 File Offset: 0x00009714
		[Obsolete("Use Quaternion.AngleAxis instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion AxisAngle(Vector3 axis, float angle)
		{
			Quaternion result;
			Quaternion.INTERNAL_CALL_AxisAngle(ref axis, angle, out result);
			return result;
		}

		// Token: 0x060008DF RID: 2271
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AxisAngle(ref Vector3 axis, float angle, out Quaternion value);

		// Token: 0x060008E0 RID: 2272 RVA: 0x0000B534 File Offset: 0x00009734
		[Obsolete("Use Quaternion.AngleAxis instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetAxisAngle(Vector3 axis, float angle)
		{
			this = Quaternion.AxisAngle(axis, angle);
		}

		// Token: 0x17000224 RID: 548
		public float this[int index]
		{
			get
			{
				float result;
				switch (index)
				{
				case 0:
					result = this.x;
					break;
				case 1:
					result = this.y;
					break;
				case 2:
					result = this.z;
					break;
				case 3:
					result = this.w;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Quaternion index!");
				}
				return result;
			}
			set
			{
				switch (index)
				{
				case 0:
					this.x = value;
					break;
				case 1:
					this.y = value;
					break;
				case 2:
					this.z = value;
					break;
				case 3:
					this.w = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Quaternion index!");
				}
			}
		}

		// Token: 0x060008E3 RID: 2275 RVA: 0x0000B610 File Offset: 0x00009810
		public void Set(float new_x, float new_y, float new_z, float new_w)
		{
			this.x = new_x;
			this.y = new_y;
			this.z = new_z;
			this.w = new_w;
		}

		// Token: 0x17000225 RID: 549
		// (get) Token: 0x060008E4 RID: 2276 RVA: 0x0000B630 File Offset: 0x00009830
		public static Quaternion identity
		{
			get
			{
				return new Quaternion(0f, 0f, 0f, 1f);
			}
		}

		// Token: 0x060008E5 RID: 2277 RVA: 0x0000B660 File Offset: 0x00009860
		public static Quaternion operator *(Quaternion lhs, Quaternion rhs)
		{
			return new Quaternion(lhs.w * rhs.x + lhs.x * rhs.w + lhs.y * rhs.z - lhs.z * rhs.y, lhs.w * rhs.y + lhs.y * rhs.w + lhs.z * rhs.x - lhs.x * rhs.z, lhs.w * rhs.z + lhs.z * rhs.w + lhs.x * rhs.y - lhs.y * rhs.x, lhs.w * rhs.w - lhs.x * rhs.x - lhs.y * rhs.y - lhs.z * rhs.z);
		}

		// Token: 0x060008E6 RID: 2278 RVA: 0x0000B778 File Offset: 0x00009978
		public static Vector3 operator *(Quaternion rotation, Vector3 point)
		{
			float num = rotation.x * 2f;
			float num2 = rotation.y * 2f;
			float num3 = rotation.z * 2f;
			float num4 = rotation.x * num;
			float num5 = rotation.y * num2;
			float num6 = rotation.z * num3;
			float num7 = rotation.x * num2;
			float num8 = rotation.x * num3;
			float num9 = rotation.y * num3;
			float num10 = rotation.w * num;
			float num11 = rotation.w * num2;
			float num12 = rotation.w * num3;
			Vector3 result;
			result.x = (1f - (num5 + num6)) * point.x + (num7 - num12) * point.y + (num8 + num11) * point.z;
			result.y = (num7 + num12) * point.x + (1f - (num4 + num6)) * point.y + (num9 - num10) * point.z;
			result.z = (num8 - num11) * point.x + (num9 + num10) * point.y + (1f - (num4 + num5)) * point.z;
			return result;
		}

		// Token: 0x060008E7 RID: 2279 RVA: 0x0000B8C0 File Offset: 0x00009AC0
		public static bool operator ==(Quaternion lhs, Quaternion rhs)
		{
			return Quaternion.Dot(lhs, rhs) > 0.999999f;
		}

		// Token: 0x060008E8 RID: 2280 RVA: 0x0000B8E4 File Offset: 0x00009AE4
		public static bool operator !=(Quaternion lhs, Quaternion rhs)
		{
			return Quaternion.Dot(lhs, rhs) <= 0.999999f;
		}

		// Token: 0x060008E9 RID: 2281 RVA: 0x0000B90C File Offset: 0x00009B0C
		public static float Dot(Quaternion a, Quaternion b)
		{
			return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
		}

		// Token: 0x060008EA RID: 2282 RVA: 0x0000B960 File Offset: 0x00009B60
		[ExcludeFromDocs]
		public void SetLookRotation(Vector3 view)
		{
			Vector3 up = Vector3.up;
			this.SetLookRotation(view, up);
		}

		// Token: 0x060008EB RID: 2283 RVA: 0x0000B97C File Offset: 0x00009B7C
		public void SetLookRotation(Vector3 view, [DefaultValue("Vector3.up")] Vector3 up)
		{
			this = Quaternion.LookRotation(view, up);
		}

		// Token: 0x060008EC RID: 2284 RVA: 0x0000B98C File Offset: 0x00009B8C
		public static float Angle(Quaternion a, Quaternion b)
		{
			float f = Quaternion.Dot(a, b);
			return Mathf.Acos(Mathf.Min(Mathf.Abs(f), 1f)) * 2f * 57.29578f;
		}

		// Token: 0x060008ED RID: 2285 RVA: 0x0000B9CC File Offset: 0x00009BCC
		private static Vector3 Internal_MakePositive(Vector3 euler)
		{
			float num = -0.005729578f;
			float num2 = 360f + num;
			if (euler.x < num)
			{
				euler.x += 360f;
			}
			else if (euler.x > num2)
			{
				euler.x -= 360f;
			}
			if (euler.y < num)
			{
				euler.y += 360f;
			}
			else if (euler.y > num2)
			{
				euler.y -= 360f;
			}
			if (euler.z < num)
			{
				euler.z += 360f;
			}
			else if (euler.z > num2)
			{
				euler.z -= 360f;
			}
			return euler;
		}

		// Token: 0x060008EE RID: 2286 RVA: 0x0000BAC0 File Offset: 0x00009CC0
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode() << 2 ^ this.z.GetHashCode() >> 2 ^ this.w.GetHashCode() >> 1;
		}

		// Token: 0x060008EF RID: 2287 RVA: 0x0000BB24 File Offset: 0x00009D24
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Quaternion))
			{
				result = false;
			}
			else
			{
				Quaternion quaternion = (Quaternion)other;
				result = (this.x.Equals(quaternion.x) && this.y.Equals(quaternion.y) && this.z.Equals(quaternion.z) && this.w.Equals(quaternion.w));
			}
			return result;
		}

		// Token: 0x060008F0 RID: 2288 RVA: 0x0000BBAC File Offset: 0x00009DAC
		public override string ToString()
		{
			return UnityString.Format("({0:F1}, {1:F1}, {2:F1}, {3:F1})", new object[]
			{
				this.x,
				this.y,
				this.z,
				this.w
			});
		}

		// Token: 0x060008F1 RID: 2289 RVA: 0x0000BC0C File Offset: 0x00009E0C
		public string ToString(string format)
		{
			return UnityString.Format("({0}, {1}, {2}, {3})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format),
				this.z.ToString(format),
				this.w.ToString(format)
			});
		}

		// Token: 0x040000FD RID: 253
		public float x;

		// Token: 0x040000FE RID: 254
		public float y;

		// Token: 0x040000FF RID: 255
		public float z;

		// Token: 0x04000100 RID: 256
		public float w;

		// Token: 0x04000101 RID: 257
		public const float kEpsilon = 1E-06f;
	}
}
