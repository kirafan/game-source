﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000D9 RID: 217
	public struct Hash128
	{
		// Token: 0x06000F56 RID: 3926 RVA: 0x000148E8 File Offset: 0x00012AE8
		public Hash128(uint u32_0, uint u32_1, uint u32_2, uint u32_3)
		{
			this.m_u32_0 = u32_0;
			this.m_u32_1 = u32_1;
			this.m_u32_2 = u32_2;
			this.m_u32_3 = u32_3;
		}

		// Token: 0x1700034C RID: 844
		// (get) Token: 0x06000F57 RID: 3927 RVA: 0x00014908 File Offset: 0x00012B08
		public bool isValid
		{
			get
			{
				return this.m_u32_0 != 0U || this.m_u32_1 != 0U || this.m_u32_2 != 0U || this.m_u32_3 != 0U;
			}
		}

		// Token: 0x06000F58 RID: 3928 RVA: 0x00014950 File Offset: 0x00012B50
		public override string ToString()
		{
			return Hash128.Internal_Hash128ToString(this.m_u32_0, this.m_u32_1, this.m_u32_2, this.m_u32_3);
		}

		// Token: 0x06000F59 RID: 3929 RVA: 0x00014984 File Offset: 0x00012B84
		public static Hash128 Parse(string hashString)
		{
			Hash128 result;
			Hash128.INTERNAL_CALL_Parse(hashString, out result);
			return result;
		}

		// Token: 0x06000F5A RID: 3930
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Parse(string hashString, out Hash128 value);

		// Token: 0x06000F5B RID: 3931
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string Internal_Hash128ToString(uint d0, uint d1, uint d2, uint d3);

		// Token: 0x06000F5C RID: 3932 RVA: 0x000149A4 File Offset: 0x00012BA4
		public override bool Equals(object obj)
		{
			return obj is Hash128 && this == (Hash128)obj;
		}

		// Token: 0x06000F5D RID: 3933 RVA: 0x000149D8 File Offset: 0x00012BD8
		public override int GetHashCode()
		{
			return this.m_u32_0.GetHashCode() ^ this.m_u32_1.GetHashCode() ^ this.m_u32_2.GetHashCode() ^ this.m_u32_3.GetHashCode();
		}

		// Token: 0x06000F5E RID: 3934 RVA: 0x00014A34 File Offset: 0x00012C34
		public static bool operator ==(Hash128 hash1, Hash128 hash2)
		{
			return hash1.m_u32_0 == hash2.m_u32_0 && hash1.m_u32_1 == hash2.m_u32_1 && hash1.m_u32_2 == hash2.m_u32_2 && hash1.m_u32_3 == hash2.m_u32_3;
		}

		// Token: 0x06000F5F RID: 3935 RVA: 0x00014A98 File Offset: 0x00012C98
		public static bool operator !=(Hash128 hash1, Hash128 hash2)
		{
			return !(hash1 == hash2);
		}

		// Token: 0x0400021F RID: 543
		private uint m_u32_0;

		// Token: 0x04000220 RID: 544
		private uint m_u32_1;

		// Token: 0x04000221 RID: 545
		private uint m_u32_2;

		// Token: 0x04000222 RID: 546
		private uint m_u32_3;
	}
}
