﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200009D RID: 157
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class HostData
	{
		// Token: 0x1700027F RID: 639
		// (get) Token: 0x06000AFF RID: 2815 RVA: 0x0000F8E8 File Offset: 0x0000DAE8
		// (set) Token: 0x06000B00 RID: 2816 RVA: 0x0000F90C File Offset: 0x0000DB0C
		public bool useNat
		{
			get
			{
				return this.m_Nat != 0;
			}
			set
			{
				this.m_Nat = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x17000280 RID: 640
		// (get) Token: 0x06000B01 RID: 2817 RVA: 0x0000F924 File Offset: 0x0000DB24
		// (set) Token: 0x06000B02 RID: 2818 RVA: 0x0000F940 File Offset: 0x0000DB40
		public string gameType
		{
			get
			{
				return this.m_GameType;
			}
			set
			{
				this.m_GameType = value;
			}
		}

		// Token: 0x17000281 RID: 641
		// (get) Token: 0x06000B03 RID: 2819 RVA: 0x0000F94C File Offset: 0x0000DB4C
		// (set) Token: 0x06000B04 RID: 2820 RVA: 0x0000F968 File Offset: 0x0000DB68
		public string gameName
		{
			get
			{
				return this.m_GameName;
			}
			set
			{
				this.m_GameName = value;
			}
		}

		// Token: 0x17000282 RID: 642
		// (get) Token: 0x06000B05 RID: 2821 RVA: 0x0000F974 File Offset: 0x0000DB74
		// (set) Token: 0x06000B06 RID: 2822 RVA: 0x0000F990 File Offset: 0x0000DB90
		public int connectedPlayers
		{
			get
			{
				return this.m_ConnectedPlayers;
			}
			set
			{
				this.m_ConnectedPlayers = value;
			}
		}

		// Token: 0x17000283 RID: 643
		// (get) Token: 0x06000B07 RID: 2823 RVA: 0x0000F99C File Offset: 0x0000DB9C
		// (set) Token: 0x06000B08 RID: 2824 RVA: 0x0000F9B8 File Offset: 0x0000DBB8
		public int playerLimit
		{
			get
			{
				return this.m_PlayerLimit;
			}
			set
			{
				this.m_PlayerLimit = value;
			}
		}

		// Token: 0x17000284 RID: 644
		// (get) Token: 0x06000B09 RID: 2825 RVA: 0x0000F9C4 File Offset: 0x0000DBC4
		// (set) Token: 0x06000B0A RID: 2826 RVA: 0x0000F9E0 File Offset: 0x0000DBE0
		public string[] ip
		{
			get
			{
				return this.m_IP;
			}
			set
			{
				this.m_IP = value;
			}
		}

		// Token: 0x17000285 RID: 645
		// (get) Token: 0x06000B0B RID: 2827 RVA: 0x0000F9EC File Offset: 0x0000DBEC
		// (set) Token: 0x06000B0C RID: 2828 RVA: 0x0000FA08 File Offset: 0x0000DC08
		public int port
		{
			get
			{
				return this.m_Port;
			}
			set
			{
				this.m_Port = value;
			}
		}

		// Token: 0x17000286 RID: 646
		// (get) Token: 0x06000B0D RID: 2829 RVA: 0x0000FA14 File Offset: 0x0000DC14
		// (set) Token: 0x06000B0E RID: 2830 RVA: 0x0000FA38 File Offset: 0x0000DC38
		public bool passwordProtected
		{
			get
			{
				return this.m_PasswordProtected != 0;
			}
			set
			{
				this.m_PasswordProtected = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x17000287 RID: 647
		// (get) Token: 0x06000B0F RID: 2831 RVA: 0x0000FA50 File Offset: 0x0000DC50
		// (set) Token: 0x06000B10 RID: 2832 RVA: 0x0000FA6C File Offset: 0x0000DC6C
		public string comment
		{
			get
			{
				return this.m_Comment;
			}
			set
			{
				this.m_Comment = value;
			}
		}

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x06000B11 RID: 2833 RVA: 0x0000FA78 File Offset: 0x0000DC78
		// (set) Token: 0x06000B12 RID: 2834 RVA: 0x0000FA94 File Offset: 0x0000DC94
		public string guid
		{
			get
			{
				return this.m_GUID;
			}
			set
			{
				this.m_GUID = value;
			}
		}

		// Token: 0x04000168 RID: 360
		private int m_Nat;

		// Token: 0x04000169 RID: 361
		private string m_GameType;

		// Token: 0x0400016A RID: 362
		private string m_GameName;

		// Token: 0x0400016B RID: 363
		private int m_ConnectedPlayers;

		// Token: 0x0400016C RID: 364
		private int m_PlayerLimit;

		// Token: 0x0400016D RID: 365
		private string[] m_IP;

		// Token: 0x0400016E RID: 366
		private int m_Port;

		// Token: 0x0400016F RID: 367
		private int m_PasswordProtected;

		// Token: 0x04000170 RID: 368
		private string m_Comment;

		// Token: 0x04000171 RID: 369
		private string m_GUID;
	}
}
