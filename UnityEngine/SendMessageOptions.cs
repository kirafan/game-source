﻿using System;

namespace UnityEngine
{
	// Token: 0x020002C4 RID: 708
	public enum SendMessageOptions
	{
		// Token: 0x04000A47 RID: 2631
		RequireReceiver,
		// Token: 0x04000A48 RID: 2632
		DontRequireReceiver
	}
}
