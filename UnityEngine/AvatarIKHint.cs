﻿using System;

namespace UnityEngine
{
	// Token: 0x020001C7 RID: 455
	public enum AvatarIKHint
	{
		// Token: 0x040004DB RID: 1243
		LeftKnee,
		// Token: 0x040004DC RID: 1244
		RightKnee,
		// Token: 0x040004DD RID: 1245
		LeftElbow,
		// Token: 0x040004DE RID: 1246
		RightElbow
	}
}
