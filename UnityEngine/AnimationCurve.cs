﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000089 RID: 137
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class AnimationCurve
	{
		// Token: 0x0600098A RID: 2442 RVA: 0x0000E294 File Offset: 0x0000C494
		public AnimationCurve(params Keyframe[] keys)
		{
			this.Init(keys);
		}

		// Token: 0x0600098B RID: 2443 RVA: 0x0000E2A4 File Offset: 0x0000C4A4
		[RequiredByNativeCode]
		public AnimationCurve()
		{
			this.Init(null);
		}

		// Token: 0x0600098C RID: 2444
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Cleanup();

		// Token: 0x0600098D RID: 2445 RVA: 0x0000E2B4 File Offset: 0x0000C4B4
		~AnimationCurve()
		{
			this.Cleanup();
		}

		// Token: 0x0600098E RID: 2446
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float Evaluate(float time);

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x0600098F RID: 2447 RVA: 0x0000E2E4 File Offset: 0x0000C4E4
		// (set) Token: 0x06000990 RID: 2448 RVA: 0x0000E300 File Offset: 0x0000C500
		public Keyframe[] keys
		{
			get
			{
				return this.GetKeys();
			}
			set
			{
				this.SetKeys(value);
			}
		}

		// Token: 0x06000991 RID: 2449
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int AddKey(float time, float value);

		// Token: 0x06000992 RID: 2450 RVA: 0x0000E30C File Offset: 0x0000C50C
		public int AddKey(Keyframe key)
		{
			return this.AddKey_Internal(key);
		}

		// Token: 0x06000993 RID: 2451 RVA: 0x0000E328 File Offset: 0x0000C528
		private int AddKey_Internal(Keyframe key)
		{
			return AnimationCurve.INTERNAL_CALL_AddKey_Internal(this, ref key);
		}

		// Token: 0x06000994 RID: 2452
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_AddKey_Internal(AnimationCurve self, ref Keyframe key);

		// Token: 0x06000995 RID: 2453 RVA: 0x0000E348 File Offset: 0x0000C548
		public int MoveKey(int index, Keyframe key)
		{
			return AnimationCurve.INTERNAL_CALL_MoveKey(this, index, ref key);
		}

		// Token: 0x06000996 RID: 2454
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_MoveKey(AnimationCurve self, int index, ref Keyframe key);

		// Token: 0x06000997 RID: 2455
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveKey(int index);

		// Token: 0x17000239 RID: 569
		public Keyframe this[int index]
		{
			get
			{
				return this.GetKey_Internal(index);
			}
		}

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06000999 RID: 2457
		public extern int length { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600099A RID: 2458
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetKeys(Keyframe[] keys);

		// Token: 0x0600099B RID: 2459 RVA: 0x0000E384 File Offset: 0x0000C584
		private Keyframe GetKey_Internal(int index)
		{
			Keyframe result;
			AnimationCurve.INTERNAL_CALL_GetKey_Internal(this, index, out result);
			return result;
		}

		// Token: 0x0600099C RID: 2460
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetKey_Internal(AnimationCurve self, int index, out Keyframe value);

		// Token: 0x0600099D RID: 2461
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Keyframe[] GetKeys();

		// Token: 0x0600099E RID: 2462
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SmoothTangents(int index, float weight);

		// Token: 0x0600099F RID: 2463 RVA: 0x0000E3A4 File Offset: 0x0000C5A4
		public static AnimationCurve Linear(float timeStart, float valueStart, float timeEnd, float valueEnd)
		{
			float num = (valueEnd - valueStart) / (timeEnd - timeStart);
			Keyframe[] keys = new Keyframe[]
			{
				new Keyframe(timeStart, valueStart, 0f, num),
				new Keyframe(timeEnd, valueEnd, num, 0f)
			};
			return new AnimationCurve(keys);
		}

		// Token: 0x060009A0 RID: 2464 RVA: 0x0000E400 File Offset: 0x0000C600
		public static AnimationCurve EaseInOut(float timeStart, float valueStart, float timeEnd, float valueEnd)
		{
			Keyframe[] keys = new Keyframe[]
			{
				new Keyframe(timeStart, valueStart, 0f, 0f),
				new Keyframe(timeEnd, valueEnd, 0f, 0f)
			};
			return new AnimationCurve(keys);
		}

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x060009A1 RID: 2465
		// (set) Token: 0x060009A2 RID: 2466
		public extern WrapMode preWrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x060009A3 RID: 2467
		// (set) Token: 0x060009A4 RID: 2468
		public extern WrapMode postWrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060009A5 RID: 2469
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Init(Keyframe[] keys);

		// Token: 0x0400011E RID: 286
		internal IntPtr m_Ptr;
	}
}
