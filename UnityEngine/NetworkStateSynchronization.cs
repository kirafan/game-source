﻿using System;

namespace UnityEngine
{
	// Token: 0x02000094 RID: 148
	public enum NetworkStateSynchronization
	{
		// Token: 0x04000157 RID: 343
		Off,
		// Token: 0x04000158 RID: 344
		ReliableDeltaCompressed,
		// Token: 0x04000159 RID: 345
		Unreliable
	}
}
