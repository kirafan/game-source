﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000CA RID: 202
	public class TextAsset : Object
	{
		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x06000DB8 RID: 3512
		public extern string text { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06000DB9 RID: 3513
		public extern byte[] bytes { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000DBA RID: 3514 RVA: 0x00013140 File Offset: 0x00011340
		public override string ToString()
		{
			return this.text;
		}
	}
}
