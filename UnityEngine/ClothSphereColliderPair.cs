﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000188 RID: 392
	[UsedByNativeCode]
	public struct ClothSphereColliderPair
	{
		// Token: 0x06001BDF RID: 7135 RVA: 0x00021B7C File Offset: 0x0001FD7C
		public ClothSphereColliderPair(SphereCollider a)
		{
			this.m_First = null;
			this.m_Second = null;
			this.first = a;
			this.second = null;
		}

		// Token: 0x06001BE0 RID: 7136 RVA: 0x00021B9C File Offset: 0x0001FD9C
		public ClothSphereColliderPair(SphereCollider a, SphereCollider b)
		{
			this.m_First = null;
			this.m_Second = null;
			this.first = a;
			this.second = b;
		}

		// Token: 0x170006AE RID: 1710
		// (get) Token: 0x06001BE1 RID: 7137 RVA: 0x00021BBC File Offset: 0x0001FDBC
		// (set) Token: 0x06001BE2 RID: 7138 RVA: 0x00021BD8 File Offset: 0x0001FDD8
		public SphereCollider first
		{
			get
			{
				return this.m_First;
			}
			set
			{
				this.m_First = value;
			}
		}

		// Token: 0x170006AF RID: 1711
		// (get) Token: 0x06001BE3 RID: 7139 RVA: 0x00021BE4 File Offset: 0x0001FDE4
		// (set) Token: 0x06001BE4 RID: 7140 RVA: 0x00021C00 File Offset: 0x0001FE00
		public SphereCollider second
		{
			get
			{
				return this.m_Second;
			}
			set
			{
				this.m_Second = value;
			}
		}

		// Token: 0x04000413 RID: 1043
		private SphereCollider m_First;

		// Token: 0x04000414 RID: 1044
		private SphereCollider m_Second;
	}
}
