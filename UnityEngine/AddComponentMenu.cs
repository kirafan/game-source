﻿using System;

namespace UnityEngine
{
	// Token: 0x020002BC RID: 700
	public sealed class AddComponentMenu : Attribute
	{
		// Token: 0x06002C35 RID: 11317 RVA: 0x00045060 File Offset: 0x00043260
		public AddComponentMenu(string menuName)
		{
			this.m_AddComponentMenu = menuName;
			this.m_Ordering = 0;
		}

		// Token: 0x06002C36 RID: 11318 RVA: 0x00045078 File Offset: 0x00043278
		public AddComponentMenu(string menuName, int order)
		{
			this.m_AddComponentMenu = menuName;
			this.m_Ordering = order;
		}

		// Token: 0x17000A69 RID: 2665
		// (get) Token: 0x06002C37 RID: 11319 RVA: 0x00045090 File Offset: 0x00043290
		public string componentMenu
		{
			get
			{
				return this.m_AddComponentMenu;
			}
		}

		// Token: 0x17000A6A RID: 2666
		// (get) Token: 0x06002C38 RID: 11320 RVA: 0x000450AC File Offset: 0x000432AC
		public int componentOrder
		{
			get
			{
				return this.m_Ordering;
			}
		}

		// Token: 0x04000A3B RID: 2619
		private string m_AddComponentMenu;

		// Token: 0x04000A3C RID: 2620
		private int m_Ordering;
	}
}
