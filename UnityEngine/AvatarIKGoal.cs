﻿using System;

namespace UnityEngine
{
	// Token: 0x020001C6 RID: 454
	public enum AvatarIKGoal
	{
		// Token: 0x040004D6 RID: 1238
		LeftFoot,
		// Token: 0x040004D7 RID: 1239
		RightFoot,
		// Token: 0x040004D8 RID: 1240
		LeftHand,
		// Token: 0x040004D9 RID: 1241
		RightHand
	}
}
