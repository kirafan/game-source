﻿using System;
using System.Reflection;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000B2 RID: 178
	public sealed class Security
	{
		// Token: 0x06000C4C RID: 3148 RVA: 0x00011074 File Offset: 0x0000F274
		[ExcludeFromDocs]
		[Obsolete("Security.PrefetchSocketPolicy is no longer supported, since the Unity Web Player is no longer supported by Unity.")]
		public static bool PrefetchSocketPolicy(string ip, int atPort)
		{
			int timeout = 3000;
			return Security.PrefetchSocketPolicy(ip, atPort, timeout);
		}

		// Token: 0x06000C4D RID: 3149 RVA: 0x00011098 File Offset: 0x0000F298
		[Obsolete("Security.PrefetchSocketPolicy is no longer supported, since the Unity Web Player is no longer supported by Unity.")]
		public static bool PrefetchSocketPolicy(string ip, int atPort, [DefaultValue("3000")] int timeout)
		{
			return false;
		}

		// Token: 0x06000C4E RID: 3150 RVA: 0x000110B0 File Offset: 0x0000F2B0
		[Obsolete("This was an internal method which is no longer used", true)]
		public static Assembly LoadAndVerifyAssembly(byte[] assemblyData, string authorizationKey)
		{
			return null;
		}

		// Token: 0x06000C4F RID: 3151 RVA: 0x000110C8 File Offset: 0x0000F2C8
		[Obsolete("This was an internal method which is no longer used", true)]
		public static Assembly LoadAndVerifyAssembly(byte[] assemblyData)
		{
			return null;
		}
	}
}
