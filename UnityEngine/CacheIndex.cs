﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000021 RID: 33
	[UsedByNativeCode]
	[Obsolete("this API is not for public use.")]
	public struct CacheIndex
	{
		// Token: 0x0400002C RID: 44
		public string name;

		// Token: 0x0400002D RID: 45
		public int bytesUsed;

		// Token: 0x0400002E RID: 46
		public int expires;
	}
}
