﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000232 RID: 562
	[RequiredByNativeCode]
	[ExecuteInEditMode]
	[Serializable]
	public sealed class GUISkin : ScriptableObject
	{
		// Token: 0x060025D4 RID: 9684 RVA: 0x00032F00 File Offset: 0x00031100
		public GUISkin()
		{
			this.m_CustomStyles = new GUIStyle[1];
		}

		// Token: 0x060025D5 RID: 9685 RVA: 0x00032F28 File Offset: 0x00031128
		internal void OnEnable()
		{
			this.Apply();
		}

		// Token: 0x060025D6 RID: 9686 RVA: 0x00032F34 File Offset: 0x00031134
		internal static void CleanupRoots()
		{
			GUISkin.current = null;
			GUISkin.ms_Error = null;
		}

		// Token: 0x17000923 RID: 2339
		// (get) Token: 0x060025D7 RID: 9687 RVA: 0x00032F44 File Offset: 0x00031144
		// (set) Token: 0x060025D8 RID: 9688 RVA: 0x00032F60 File Offset: 0x00031160
		public Font font
		{
			get
			{
				return this.m_Font;
			}
			set
			{
				this.m_Font = value;
				if (GUISkin.current == this)
				{
					GUIStyle.SetDefaultFont(this.m_Font);
				}
				this.Apply();
			}
		}

		// Token: 0x17000924 RID: 2340
		// (get) Token: 0x060025D9 RID: 9689 RVA: 0x00032F8C File Offset: 0x0003118C
		// (set) Token: 0x060025DA RID: 9690 RVA: 0x00032FA8 File Offset: 0x000311A8
		public GUIStyle box
		{
			get
			{
				return this.m_box;
			}
			set
			{
				this.m_box = value;
				this.Apply();
			}
		}

		// Token: 0x17000925 RID: 2341
		// (get) Token: 0x060025DB RID: 9691 RVA: 0x00032FB8 File Offset: 0x000311B8
		// (set) Token: 0x060025DC RID: 9692 RVA: 0x00032FD4 File Offset: 0x000311D4
		public GUIStyle label
		{
			get
			{
				return this.m_label;
			}
			set
			{
				this.m_label = value;
				this.Apply();
			}
		}

		// Token: 0x17000926 RID: 2342
		// (get) Token: 0x060025DD RID: 9693 RVA: 0x00032FE4 File Offset: 0x000311E4
		// (set) Token: 0x060025DE RID: 9694 RVA: 0x00033000 File Offset: 0x00031200
		public GUIStyle textField
		{
			get
			{
				return this.m_textField;
			}
			set
			{
				this.m_textField = value;
				this.Apply();
			}
		}

		// Token: 0x17000927 RID: 2343
		// (get) Token: 0x060025DF RID: 9695 RVA: 0x00033010 File Offset: 0x00031210
		// (set) Token: 0x060025E0 RID: 9696 RVA: 0x0003302C File Offset: 0x0003122C
		public GUIStyle textArea
		{
			get
			{
				return this.m_textArea;
			}
			set
			{
				this.m_textArea = value;
				this.Apply();
			}
		}

		// Token: 0x17000928 RID: 2344
		// (get) Token: 0x060025E1 RID: 9697 RVA: 0x0003303C File Offset: 0x0003123C
		// (set) Token: 0x060025E2 RID: 9698 RVA: 0x00033058 File Offset: 0x00031258
		public GUIStyle button
		{
			get
			{
				return this.m_button;
			}
			set
			{
				this.m_button = value;
				this.Apply();
			}
		}

		// Token: 0x17000929 RID: 2345
		// (get) Token: 0x060025E3 RID: 9699 RVA: 0x00033068 File Offset: 0x00031268
		// (set) Token: 0x060025E4 RID: 9700 RVA: 0x00033084 File Offset: 0x00031284
		public GUIStyle toggle
		{
			get
			{
				return this.m_toggle;
			}
			set
			{
				this.m_toggle = value;
				this.Apply();
			}
		}

		// Token: 0x1700092A RID: 2346
		// (get) Token: 0x060025E5 RID: 9701 RVA: 0x00033094 File Offset: 0x00031294
		// (set) Token: 0x060025E6 RID: 9702 RVA: 0x000330B0 File Offset: 0x000312B0
		public GUIStyle window
		{
			get
			{
				return this.m_window;
			}
			set
			{
				this.m_window = value;
				this.Apply();
			}
		}

		// Token: 0x1700092B RID: 2347
		// (get) Token: 0x060025E7 RID: 9703 RVA: 0x000330C0 File Offset: 0x000312C0
		// (set) Token: 0x060025E8 RID: 9704 RVA: 0x000330DC File Offset: 0x000312DC
		public GUIStyle horizontalSlider
		{
			get
			{
				return this.m_horizontalSlider;
			}
			set
			{
				this.m_horizontalSlider = value;
				this.Apply();
			}
		}

		// Token: 0x1700092C RID: 2348
		// (get) Token: 0x060025E9 RID: 9705 RVA: 0x000330EC File Offset: 0x000312EC
		// (set) Token: 0x060025EA RID: 9706 RVA: 0x00033108 File Offset: 0x00031308
		public GUIStyle horizontalSliderThumb
		{
			get
			{
				return this.m_horizontalSliderThumb;
			}
			set
			{
				this.m_horizontalSliderThumb = value;
				this.Apply();
			}
		}

		// Token: 0x1700092D RID: 2349
		// (get) Token: 0x060025EB RID: 9707 RVA: 0x00033118 File Offset: 0x00031318
		// (set) Token: 0x060025EC RID: 9708 RVA: 0x00033134 File Offset: 0x00031334
		public GUIStyle verticalSlider
		{
			get
			{
				return this.m_verticalSlider;
			}
			set
			{
				this.m_verticalSlider = value;
				this.Apply();
			}
		}

		// Token: 0x1700092E RID: 2350
		// (get) Token: 0x060025ED RID: 9709 RVA: 0x00033144 File Offset: 0x00031344
		// (set) Token: 0x060025EE RID: 9710 RVA: 0x00033160 File Offset: 0x00031360
		public GUIStyle verticalSliderThumb
		{
			get
			{
				return this.m_verticalSliderThumb;
			}
			set
			{
				this.m_verticalSliderThumb = value;
				this.Apply();
			}
		}

		// Token: 0x1700092F RID: 2351
		// (get) Token: 0x060025EF RID: 9711 RVA: 0x00033170 File Offset: 0x00031370
		// (set) Token: 0x060025F0 RID: 9712 RVA: 0x0003318C File Offset: 0x0003138C
		public GUIStyle horizontalScrollbar
		{
			get
			{
				return this.m_horizontalScrollbar;
			}
			set
			{
				this.m_horizontalScrollbar = value;
				this.Apply();
			}
		}

		// Token: 0x17000930 RID: 2352
		// (get) Token: 0x060025F1 RID: 9713 RVA: 0x0003319C File Offset: 0x0003139C
		// (set) Token: 0x060025F2 RID: 9714 RVA: 0x000331B8 File Offset: 0x000313B8
		public GUIStyle horizontalScrollbarThumb
		{
			get
			{
				return this.m_horizontalScrollbarThumb;
			}
			set
			{
				this.m_horizontalScrollbarThumb = value;
				this.Apply();
			}
		}

		// Token: 0x17000931 RID: 2353
		// (get) Token: 0x060025F3 RID: 9715 RVA: 0x000331C8 File Offset: 0x000313C8
		// (set) Token: 0x060025F4 RID: 9716 RVA: 0x000331E4 File Offset: 0x000313E4
		public GUIStyle horizontalScrollbarLeftButton
		{
			get
			{
				return this.m_horizontalScrollbarLeftButton;
			}
			set
			{
				this.m_horizontalScrollbarLeftButton = value;
				this.Apply();
			}
		}

		// Token: 0x17000932 RID: 2354
		// (get) Token: 0x060025F5 RID: 9717 RVA: 0x000331F4 File Offset: 0x000313F4
		// (set) Token: 0x060025F6 RID: 9718 RVA: 0x00033210 File Offset: 0x00031410
		public GUIStyle horizontalScrollbarRightButton
		{
			get
			{
				return this.m_horizontalScrollbarRightButton;
			}
			set
			{
				this.m_horizontalScrollbarRightButton = value;
				this.Apply();
			}
		}

		// Token: 0x17000933 RID: 2355
		// (get) Token: 0x060025F7 RID: 9719 RVA: 0x00033220 File Offset: 0x00031420
		// (set) Token: 0x060025F8 RID: 9720 RVA: 0x0003323C File Offset: 0x0003143C
		public GUIStyle verticalScrollbar
		{
			get
			{
				return this.m_verticalScrollbar;
			}
			set
			{
				this.m_verticalScrollbar = value;
				this.Apply();
			}
		}

		// Token: 0x17000934 RID: 2356
		// (get) Token: 0x060025F9 RID: 9721 RVA: 0x0003324C File Offset: 0x0003144C
		// (set) Token: 0x060025FA RID: 9722 RVA: 0x00033268 File Offset: 0x00031468
		public GUIStyle verticalScrollbarThumb
		{
			get
			{
				return this.m_verticalScrollbarThumb;
			}
			set
			{
				this.m_verticalScrollbarThumb = value;
				this.Apply();
			}
		}

		// Token: 0x17000935 RID: 2357
		// (get) Token: 0x060025FB RID: 9723 RVA: 0x00033278 File Offset: 0x00031478
		// (set) Token: 0x060025FC RID: 9724 RVA: 0x00033294 File Offset: 0x00031494
		public GUIStyle verticalScrollbarUpButton
		{
			get
			{
				return this.m_verticalScrollbarUpButton;
			}
			set
			{
				this.m_verticalScrollbarUpButton = value;
				this.Apply();
			}
		}

		// Token: 0x17000936 RID: 2358
		// (get) Token: 0x060025FD RID: 9725 RVA: 0x000332A4 File Offset: 0x000314A4
		// (set) Token: 0x060025FE RID: 9726 RVA: 0x000332C0 File Offset: 0x000314C0
		public GUIStyle verticalScrollbarDownButton
		{
			get
			{
				return this.m_verticalScrollbarDownButton;
			}
			set
			{
				this.m_verticalScrollbarDownButton = value;
				this.Apply();
			}
		}

		// Token: 0x17000937 RID: 2359
		// (get) Token: 0x060025FF RID: 9727 RVA: 0x000332D0 File Offset: 0x000314D0
		// (set) Token: 0x06002600 RID: 9728 RVA: 0x000332EC File Offset: 0x000314EC
		public GUIStyle scrollView
		{
			get
			{
				return this.m_ScrollView;
			}
			set
			{
				this.m_ScrollView = value;
				this.Apply();
			}
		}

		// Token: 0x17000938 RID: 2360
		// (get) Token: 0x06002601 RID: 9729 RVA: 0x000332FC File Offset: 0x000314FC
		// (set) Token: 0x06002602 RID: 9730 RVA: 0x00033318 File Offset: 0x00031518
		public GUIStyle[] customStyles
		{
			get
			{
				return this.m_CustomStyles;
			}
			set
			{
				this.m_CustomStyles = value;
				this.Apply();
			}
		}

		// Token: 0x17000939 RID: 2361
		// (get) Token: 0x06002603 RID: 9731 RVA: 0x00033328 File Offset: 0x00031528
		public GUISettings settings
		{
			get
			{
				return this.m_Settings;
			}
		}

		// Token: 0x1700093A RID: 2362
		// (get) Token: 0x06002604 RID: 9732 RVA: 0x00033344 File Offset: 0x00031544
		internal static GUIStyle error
		{
			get
			{
				if (GUISkin.ms_Error == null)
				{
					GUISkin.ms_Error = new GUIStyle();
				}
				return GUISkin.ms_Error;
			}
		}

		// Token: 0x06002605 RID: 9733 RVA: 0x00033374 File Offset: 0x00031574
		internal void Apply()
		{
			if (this.m_CustomStyles == null)
			{
				Debug.Log("custom styles is null");
			}
			this.BuildStyleCache();
		}

		// Token: 0x06002606 RID: 9734 RVA: 0x00033394 File Offset: 0x00031594
		private void BuildStyleCache()
		{
			if (this.m_box == null)
			{
				this.m_box = new GUIStyle();
			}
			if (this.m_button == null)
			{
				this.m_button = new GUIStyle();
			}
			if (this.m_toggle == null)
			{
				this.m_toggle = new GUIStyle();
			}
			if (this.m_label == null)
			{
				this.m_label = new GUIStyle();
			}
			if (this.m_window == null)
			{
				this.m_window = new GUIStyle();
			}
			if (this.m_textField == null)
			{
				this.m_textField = new GUIStyle();
			}
			if (this.m_textArea == null)
			{
				this.m_textArea = new GUIStyle();
			}
			if (this.m_horizontalSlider == null)
			{
				this.m_horizontalSlider = new GUIStyle();
			}
			if (this.m_horizontalSliderThumb == null)
			{
				this.m_horizontalSliderThumb = new GUIStyle();
			}
			if (this.m_verticalSlider == null)
			{
				this.m_verticalSlider = new GUIStyle();
			}
			if (this.m_verticalSliderThumb == null)
			{
				this.m_verticalSliderThumb = new GUIStyle();
			}
			if (this.m_horizontalScrollbar == null)
			{
				this.m_horizontalScrollbar = new GUIStyle();
			}
			if (this.m_horizontalScrollbarThumb == null)
			{
				this.m_horizontalScrollbarThumb = new GUIStyle();
			}
			if (this.m_horizontalScrollbarLeftButton == null)
			{
				this.m_horizontalScrollbarLeftButton = new GUIStyle();
			}
			if (this.m_horizontalScrollbarRightButton == null)
			{
				this.m_horizontalScrollbarRightButton = new GUIStyle();
			}
			if (this.m_verticalScrollbar == null)
			{
				this.m_verticalScrollbar = new GUIStyle();
			}
			if (this.m_verticalScrollbarThumb == null)
			{
				this.m_verticalScrollbarThumb = new GUIStyle();
			}
			if (this.m_verticalScrollbarUpButton == null)
			{
				this.m_verticalScrollbarUpButton = new GUIStyle();
			}
			if (this.m_verticalScrollbarDownButton == null)
			{
				this.m_verticalScrollbarDownButton = new GUIStyle();
			}
			if (this.m_ScrollView == null)
			{
				this.m_ScrollView = new GUIStyle();
			}
			this.m_Styles = new Dictionary<string, GUIStyle>(StringComparer.OrdinalIgnoreCase);
			this.m_Styles["box"] = this.m_box;
			this.m_box.name = "box";
			this.m_Styles["button"] = this.m_button;
			this.m_button.name = "button";
			this.m_Styles["toggle"] = this.m_toggle;
			this.m_toggle.name = "toggle";
			this.m_Styles["label"] = this.m_label;
			this.m_label.name = "label";
			this.m_Styles["window"] = this.m_window;
			this.m_window.name = "window";
			this.m_Styles["textfield"] = this.m_textField;
			this.m_textField.name = "textfield";
			this.m_Styles["textarea"] = this.m_textArea;
			this.m_textArea.name = "textarea";
			this.m_Styles["horizontalslider"] = this.m_horizontalSlider;
			this.m_horizontalSlider.name = "horizontalslider";
			this.m_Styles["horizontalsliderthumb"] = this.m_horizontalSliderThumb;
			this.m_horizontalSliderThumb.name = "horizontalsliderthumb";
			this.m_Styles["verticalslider"] = this.m_verticalSlider;
			this.m_verticalSlider.name = "verticalslider";
			this.m_Styles["verticalsliderthumb"] = this.m_verticalSliderThumb;
			this.m_verticalSliderThumb.name = "verticalsliderthumb";
			this.m_Styles["horizontalscrollbar"] = this.m_horizontalScrollbar;
			this.m_horizontalScrollbar.name = "horizontalscrollbar";
			this.m_Styles["horizontalscrollbarthumb"] = this.m_horizontalScrollbarThumb;
			this.m_horizontalScrollbarThumb.name = "horizontalscrollbarthumb";
			this.m_Styles["horizontalscrollbarleftbutton"] = this.m_horizontalScrollbarLeftButton;
			this.m_horizontalScrollbarLeftButton.name = "horizontalscrollbarleftbutton";
			this.m_Styles["horizontalscrollbarrightbutton"] = this.m_horizontalScrollbarRightButton;
			this.m_horizontalScrollbarRightButton.name = "horizontalscrollbarrightbutton";
			this.m_Styles["verticalscrollbar"] = this.m_verticalScrollbar;
			this.m_verticalScrollbar.name = "verticalscrollbar";
			this.m_Styles["verticalscrollbarthumb"] = this.m_verticalScrollbarThumb;
			this.m_verticalScrollbarThumb.name = "verticalscrollbarthumb";
			this.m_Styles["verticalscrollbarupbutton"] = this.m_verticalScrollbarUpButton;
			this.m_verticalScrollbarUpButton.name = "verticalscrollbarupbutton";
			this.m_Styles["verticalscrollbardownbutton"] = this.m_verticalScrollbarDownButton;
			this.m_verticalScrollbarDownButton.name = "verticalscrollbardownbutton";
			this.m_Styles["scrollview"] = this.m_ScrollView;
			this.m_ScrollView.name = "scrollview";
			if (this.m_CustomStyles != null)
			{
				for (int i = 0; i < this.m_CustomStyles.Length; i++)
				{
					if (this.m_CustomStyles[i] != null)
					{
						this.m_Styles[this.m_CustomStyles[i].name] = this.m_CustomStyles[i];
					}
				}
			}
			GUISkin.error.stretchHeight = true;
			GUISkin.error.normal.textColor = Color.red;
		}

		// Token: 0x06002607 RID: 9735 RVA: 0x000338DC File Offset: 0x00031ADC
		public GUIStyle GetStyle(string styleName)
		{
			GUIStyle guistyle = this.FindStyle(styleName);
			GUIStyle result;
			if (guistyle != null)
			{
				result = guistyle;
			}
			else
			{
				Debug.LogWarning(string.Concat(new object[]
				{
					"Unable to find style '",
					styleName,
					"' in skin '",
					base.name,
					"' ",
					Event.current.type
				}));
				result = GUISkin.error;
			}
			return result;
		}

		// Token: 0x06002608 RID: 9736 RVA: 0x00033954 File Offset: 0x00031B54
		public GUIStyle FindStyle(string styleName)
		{
			GUIStyle result;
			if (this == null)
			{
				Debug.LogError("GUISkin is NULL");
				result = null;
			}
			else
			{
				if (this.m_Styles == null)
				{
					this.BuildStyleCache();
				}
				GUIStyle guistyle;
				if (this.m_Styles.TryGetValue(styleName, out guistyle))
				{
					result = guistyle;
				}
				else
				{
					result = null;
				}
			}
			return result;
		}

		// Token: 0x06002609 RID: 9737 RVA: 0x000339B4 File Offset: 0x00031BB4
		internal void MakeCurrent()
		{
			GUISkin.current = this;
			GUIStyle.SetDefaultFont(this.font);
			if (GUISkin.m_SkinChanged != null)
			{
				GUISkin.m_SkinChanged();
			}
		}

		// Token: 0x0600260A RID: 9738 RVA: 0x000339DC File Offset: 0x00031BDC
		public IEnumerator GetEnumerator()
		{
			if (this.m_Styles == null)
			{
				this.BuildStyleCache();
			}
			return this.m_Styles.Values.GetEnumerator();
		}

		// Token: 0x04000830 RID: 2096
		[SerializeField]
		private Font m_Font;

		// Token: 0x04000831 RID: 2097
		[SerializeField]
		private GUIStyle m_box;

		// Token: 0x04000832 RID: 2098
		[SerializeField]
		private GUIStyle m_button;

		// Token: 0x04000833 RID: 2099
		[SerializeField]
		private GUIStyle m_toggle;

		// Token: 0x04000834 RID: 2100
		[SerializeField]
		private GUIStyle m_label;

		// Token: 0x04000835 RID: 2101
		[SerializeField]
		private GUIStyle m_textField;

		// Token: 0x04000836 RID: 2102
		[SerializeField]
		private GUIStyle m_textArea;

		// Token: 0x04000837 RID: 2103
		[SerializeField]
		private GUIStyle m_window;

		// Token: 0x04000838 RID: 2104
		[SerializeField]
		private GUIStyle m_horizontalSlider;

		// Token: 0x04000839 RID: 2105
		[SerializeField]
		private GUIStyle m_horizontalSliderThumb;

		// Token: 0x0400083A RID: 2106
		[SerializeField]
		private GUIStyle m_verticalSlider;

		// Token: 0x0400083B RID: 2107
		[SerializeField]
		private GUIStyle m_verticalSliderThumb;

		// Token: 0x0400083C RID: 2108
		[SerializeField]
		private GUIStyle m_horizontalScrollbar;

		// Token: 0x0400083D RID: 2109
		[SerializeField]
		private GUIStyle m_horizontalScrollbarThumb;

		// Token: 0x0400083E RID: 2110
		[SerializeField]
		private GUIStyle m_horizontalScrollbarLeftButton;

		// Token: 0x0400083F RID: 2111
		[SerializeField]
		private GUIStyle m_horizontalScrollbarRightButton;

		// Token: 0x04000840 RID: 2112
		[SerializeField]
		private GUIStyle m_verticalScrollbar;

		// Token: 0x04000841 RID: 2113
		[SerializeField]
		private GUIStyle m_verticalScrollbarThumb;

		// Token: 0x04000842 RID: 2114
		[SerializeField]
		private GUIStyle m_verticalScrollbarUpButton;

		// Token: 0x04000843 RID: 2115
		[SerializeField]
		private GUIStyle m_verticalScrollbarDownButton;

		// Token: 0x04000844 RID: 2116
		[SerializeField]
		private GUIStyle m_ScrollView;

		// Token: 0x04000845 RID: 2117
		[SerializeField]
		internal GUIStyle[] m_CustomStyles;

		// Token: 0x04000846 RID: 2118
		[SerializeField]
		private GUISettings m_Settings = new GUISettings();

		// Token: 0x04000847 RID: 2119
		internal static GUIStyle ms_Error;

		// Token: 0x04000848 RID: 2120
		private Dictionary<string, GUIStyle> m_Styles = null;

		// Token: 0x04000849 RID: 2121
		internal static GUISkin.SkinChangedDelegate m_SkinChanged;

		// Token: 0x0400084A RID: 2122
		internal static GUISkin current;

		// Token: 0x02000233 RID: 563
		// (Invoke) Token: 0x0600260C RID: 9740
		internal delegate void SkinChangedDelegate();
	}
}
