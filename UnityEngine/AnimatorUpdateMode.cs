﻿using System;

namespace UnityEngine
{
	// Token: 0x020001CD RID: 461
	public enum AnimatorUpdateMode
	{
		// Token: 0x040004F3 RID: 1267
		Normal,
		// Token: 0x040004F4 RID: 1268
		AnimatePhysics,
		// Token: 0x040004F5 RID: 1269
		UnscaledTime
	}
}
