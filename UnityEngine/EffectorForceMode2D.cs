﻿using System;

namespace UnityEngine
{
	// Token: 0x0200017E RID: 382
	public enum EffectorForceMode2D
	{
		// Token: 0x04000406 RID: 1030
		Constant,
		// Token: 0x04000407 RID: 1031
		InverseLinear,
		// Token: 0x04000408 RID: 1032
		InverseSquared
	}
}
