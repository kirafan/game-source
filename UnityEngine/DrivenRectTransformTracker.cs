﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000A7 RID: 167
	public struct DrivenRectTransformTracker
	{
		// Token: 0x06000B5D RID: 2909
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool CanRecordModifications();

		// Token: 0x06000B5E RID: 2910 RVA: 0x0000FEB0 File Offset: 0x0000E0B0
		public void Add(Object driver, RectTransform rectTransform, DrivenTransformProperties drivenProperties)
		{
		}

		// Token: 0x06000B5F RID: 2911 RVA: 0x0000FEB4 File Offset: 0x0000E0B4
		public void Clear()
		{
		}
	}
}
