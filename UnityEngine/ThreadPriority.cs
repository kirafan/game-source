﻿using System;

namespace UnityEngine
{
	// Token: 0x020002CC RID: 716
	public enum ThreadPriority
	{
		// Token: 0x04000AB1 RID: 2737
		Low,
		// Token: 0x04000AB2 RID: 2738
		BelowNormal,
		// Token: 0x04000AB3 RID: 2739
		Normal,
		// Token: 0x04000AB4 RID: 2740
		High = 4
	}
}
