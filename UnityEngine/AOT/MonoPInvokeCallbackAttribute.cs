﻿using System;

namespace AOT
{
	// Token: 0x020002B7 RID: 695
	[AttributeUsage(AttributeTargets.Method)]
	public class MonoPInvokeCallbackAttribute : Attribute
	{
		// Token: 0x06002C29 RID: 11305 RVA: 0x00044D58 File Offset: 0x00042F58
		public MonoPInvokeCallbackAttribute(Type type)
		{
		}
	}
}
