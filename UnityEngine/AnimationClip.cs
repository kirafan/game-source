﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001BC RID: 444
	public sealed class AnimationClip : Motion
	{
		// Token: 0x06001E46 RID: 7750 RVA: 0x00022E28 File Offset: 0x00021028
		public AnimationClip()
		{
			AnimationClip.Internal_CreateAnimationClip(this);
		}

		// Token: 0x06001E47 RID: 7751
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SampleAnimation(GameObject go, float time);

		// Token: 0x06001E48 RID: 7752
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateAnimationClip([Writable] AnimationClip self);

		// Token: 0x1700078D RID: 1933
		// (get) Token: 0x06001E49 RID: 7753
		public extern float length { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700078E RID: 1934
		// (get) Token: 0x06001E4A RID: 7754
		internal extern float startTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700078F RID: 1935
		// (get) Token: 0x06001E4B RID: 7755
		internal extern float stopTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000790 RID: 1936
		// (get) Token: 0x06001E4C RID: 7756
		// (set) Token: 0x06001E4D RID: 7757
		public extern float frameRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001E4E RID: 7758
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetCurve(string relativePath, Type type, string propertyName, AnimationCurve curve);

		// Token: 0x06001E4F RID: 7759 RVA: 0x00022E38 File Offset: 0x00021038
		public void EnsureQuaternionContinuity()
		{
			AnimationClip.INTERNAL_CALL_EnsureQuaternionContinuity(this);
		}

		// Token: 0x06001E50 RID: 7760
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_EnsureQuaternionContinuity(AnimationClip self);

		// Token: 0x06001E51 RID: 7761 RVA: 0x00022E44 File Offset: 0x00021044
		public void ClearCurves()
		{
			AnimationClip.INTERNAL_CALL_ClearCurves(this);
		}

		// Token: 0x06001E52 RID: 7762
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ClearCurves(AnimationClip self);

		// Token: 0x17000791 RID: 1937
		// (get) Token: 0x06001E53 RID: 7763
		// (set) Token: 0x06001E54 RID: 7764
		public extern WrapMode wrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000792 RID: 1938
		// (get) Token: 0x06001E55 RID: 7765 RVA: 0x00022E50 File Offset: 0x00021050
		// (set) Token: 0x06001E56 RID: 7766 RVA: 0x00022E70 File Offset: 0x00021070
		public Bounds localBounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_localBounds(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localBounds(ref value);
			}
		}

		// Token: 0x06001E57 RID: 7767
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localBounds(out Bounds value);

		// Token: 0x06001E58 RID: 7768
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localBounds(ref Bounds value);

		// Token: 0x17000793 RID: 1939
		// (get) Token: 0x06001E59 RID: 7769
		// (set) Token: 0x06001E5A RID: 7770
		public new extern bool legacy { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000794 RID: 1940
		// (get) Token: 0x06001E5B RID: 7771
		public extern bool humanMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001E5C RID: 7772 RVA: 0x00022E7C File Offset: 0x0002107C
		public void AddEvent(AnimationEvent evt)
		{
			if (evt == null)
			{
				throw new ArgumentNullException("evt");
			}
			this.AddEventInternal(evt);
		}

		// Token: 0x06001E5D RID: 7773
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void AddEventInternal(object evt);

		// Token: 0x17000795 RID: 1941
		// (get) Token: 0x06001E5E RID: 7774 RVA: 0x00022E98 File Offset: 0x00021098
		// (set) Token: 0x06001E5F RID: 7775 RVA: 0x00022EB8 File Offset: 0x000210B8
		public AnimationEvent[] events
		{
			get
			{
				return (AnimationEvent[])this.GetEventsInternal();
			}
			set
			{
				this.SetEventsInternal(value);
			}
		}

		// Token: 0x06001E60 RID: 7776
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetEventsInternal(Array value);

		// Token: 0x06001E61 RID: 7777
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Array GetEventsInternal();
	}
}
