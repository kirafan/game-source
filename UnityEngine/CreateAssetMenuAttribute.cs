﻿using System;

namespace UnityEngine
{
	// Token: 0x020002BD RID: 701
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	public sealed class CreateAssetMenuAttribute : Attribute
	{
		// Token: 0x17000A6B RID: 2667
		// (get) Token: 0x06002C3A RID: 11322 RVA: 0x000450D0 File Offset: 0x000432D0
		// (set) Token: 0x06002C3B RID: 11323 RVA: 0x000450EC File Offset: 0x000432EC
		public string menuName { get; set; }

		// Token: 0x17000A6C RID: 2668
		// (get) Token: 0x06002C3C RID: 11324 RVA: 0x000450F8 File Offset: 0x000432F8
		// (set) Token: 0x06002C3D RID: 11325 RVA: 0x00045114 File Offset: 0x00043314
		public string fileName { get; set; }

		// Token: 0x17000A6D RID: 2669
		// (get) Token: 0x06002C3E RID: 11326 RVA: 0x00045120 File Offset: 0x00043320
		// (set) Token: 0x06002C3F RID: 11327 RVA: 0x0004513C File Offset: 0x0004333C
		public int order { get; set; }
	}
}
