﻿using System;

namespace UnityEngine
{
	// Token: 0x0200013E RID: 318
	public enum PhysicMaterialCombine
	{
		// Token: 0x0400038C RID: 908
		Average,
		// Token: 0x0400038D RID: 909
		Minimum = 2,
		// Token: 0x0400038E RID: 910
		Multiply = 1,
		// Token: 0x0400038F RID: 911
		Maximum = 3
	}
}
