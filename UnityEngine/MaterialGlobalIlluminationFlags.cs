﻿using System;

namespace UnityEngine
{
	// Token: 0x02000302 RID: 770
	[Flags]
	public enum MaterialGlobalIlluminationFlags
	{
		// Token: 0x04000BB8 RID: 3000
		None = 0,
		// Token: 0x04000BB9 RID: 3001
		RealtimeEmissive = 1,
		// Token: 0x04000BBA RID: 3002
		BakedEmissive = 2,
		// Token: 0x04000BBB RID: 3003
		EmissiveIsBlack = 4
	}
}
