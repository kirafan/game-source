﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x0200005A RID: 90
	public sealed class Screen
	{
		// Token: 0x1700017B RID: 379
		// (get) Token: 0x060006B4 RID: 1716
		public static extern Resolution[] resolutions { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700017C RID: 380
		// (get) Token: 0x060006B5 RID: 1717 RVA: 0x00009118 File Offset: 0x00007318
		// (set) Token: 0x060006B6 RID: 1718 RVA: 0x00009138 File Offset: 0x00007338
		[Obsolete("Property lockCursor has been deprecated. Use Cursor.lockState and Cursor.visible instead.")]
		[EditorBrowsable(EditorBrowsableState.Never)]
		public static bool lockCursor
		{
			get
			{
				return CursorLockMode.Locked == Cursor.lockState;
			}
			set
			{
				if (value)
				{
					Cursor.visible = false;
					Cursor.lockState = CursorLockMode.Locked;
				}
				else
				{
					Cursor.lockState = CursorLockMode.None;
					Cursor.visible = true;
				}
			}
		}

		// Token: 0x1700017D RID: 381
		// (get) Token: 0x060006B7 RID: 1719
		public static extern Resolution currentResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060006B8 RID: 1720
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetResolution(int width, int height, bool fullscreen, [UnityEngine.Internal.DefaultValue("0")] int preferredRefreshRate);

		// Token: 0x060006B9 RID: 1721 RVA: 0x00009164 File Offset: 0x00007364
		[ExcludeFromDocs]
		public static void SetResolution(int width, int height, bool fullscreen)
		{
			int preferredRefreshRate = 0;
			Screen.SetResolution(width, height, fullscreen, preferredRefreshRate);
		}

		// Token: 0x1700017E RID: 382
		// (get) Token: 0x060006BA RID: 1722
		[ThreadAndSerializationSafe]
		public static extern int width { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700017F RID: 383
		// (get) Token: 0x060006BB RID: 1723
		[ThreadAndSerializationSafe]
		public static extern int height { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000180 RID: 384
		// (get) Token: 0x060006BC RID: 1724
		public static extern float dpi { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x060006BD RID: 1725
		// (set) Token: 0x060006BE RID: 1726
		public static extern bool fullScreen { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000182 RID: 386
		// (get) Token: 0x060006BF RID: 1727
		// (set) Token: 0x060006C0 RID: 1728
		public static extern bool autorotateToPortrait { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000183 RID: 387
		// (get) Token: 0x060006C1 RID: 1729
		// (set) Token: 0x060006C2 RID: 1730
		public static extern bool autorotateToPortraitUpsideDown { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000184 RID: 388
		// (get) Token: 0x060006C3 RID: 1731
		// (set) Token: 0x060006C4 RID: 1732
		public static extern bool autorotateToLandscapeLeft { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000185 RID: 389
		// (get) Token: 0x060006C5 RID: 1733
		// (set) Token: 0x060006C6 RID: 1734
		public static extern bool autorotateToLandscapeRight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x060006C7 RID: 1735
		// (set) Token: 0x060006C8 RID: 1736
		public static extern ScreenOrientation orientation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x060006C9 RID: 1737
		// (set) Token: 0x060006CA RID: 1738
		public static extern int sleepTimeout { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
