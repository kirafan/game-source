﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001E6 RID: 486
	public sealed class HumanPoseHandler : IDisposable
	{
		// Token: 0x06002029 RID: 8233 RVA: 0x0002475C File Offset: 0x0002295C
		public HumanPoseHandler(Avatar avatar, Transform root)
		{
			this.m_Ptr = IntPtr.Zero;
			if (root == null)
			{
				throw new ArgumentNullException("HumanPoseHandler root Transform is null");
			}
			if (avatar == null)
			{
				throw new ArgumentNullException("HumanPoseHandler avatar is null");
			}
			if (!avatar.isValid)
			{
				throw new ArgumentException("HumanPoseHandler avatar is invalid");
			}
			if (!avatar.isHuman)
			{
				throw new ArgumentException("HumanPoseHandler avatar is not human");
			}
			this.Internal_HumanPoseHandler(avatar, root);
		}

		// Token: 0x0600202A RID: 8234
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Dispose();

		// Token: 0x0600202B RID: 8235
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_HumanPoseHandler(Avatar avatar, Transform root);

		// Token: 0x0600202C RID: 8236 RVA: 0x000247E0 File Offset: 0x000229E0
		private bool Internal_GetHumanPose(ref Vector3 bodyPosition, ref Quaternion bodyRotation, float[] muscles)
		{
			return HumanPoseHandler.INTERNAL_CALL_Internal_GetHumanPose(this, ref bodyPosition, ref bodyRotation, muscles);
		}

		// Token: 0x0600202D RID: 8237
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Internal_GetHumanPose(HumanPoseHandler self, ref Vector3 bodyPosition, ref Quaternion bodyRotation, float[] muscles);

		// Token: 0x0600202E RID: 8238 RVA: 0x00024800 File Offset: 0x00022A00
		public void GetHumanPose(ref HumanPose humanPose)
		{
			humanPose.Init();
			if (!this.Internal_GetHumanPose(ref humanPose.bodyPosition, ref humanPose.bodyRotation, humanPose.muscles))
			{
				Debug.LogWarning("HumanPoseHandler is not initialized properly");
			}
		}

		// Token: 0x0600202F RID: 8239 RVA: 0x00024834 File Offset: 0x00022A34
		private bool Internal_SetHumanPose(ref Vector3 bodyPosition, ref Quaternion bodyRotation, float[] muscles)
		{
			return HumanPoseHandler.INTERNAL_CALL_Internal_SetHumanPose(this, ref bodyPosition, ref bodyRotation, muscles);
		}

		// Token: 0x06002030 RID: 8240
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Internal_SetHumanPose(HumanPoseHandler self, ref Vector3 bodyPosition, ref Quaternion bodyRotation, float[] muscles);

		// Token: 0x06002031 RID: 8241 RVA: 0x00024854 File Offset: 0x00022A54
		public void SetHumanPose(ref HumanPose humanPose)
		{
			humanPose.Init();
			if (!this.Internal_SetHumanPose(ref humanPose.bodyPosition, ref humanPose.bodyRotation, humanPose.muscles))
			{
				Debug.LogWarning("HumanPoseHandler is not initialized properly");
			}
		}

		// Token: 0x040005AE RID: 1454
		internal IntPtr m_Ptr;
	}
}
