﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F2 RID: 754
	[Flags]
	public enum DepthTextureMode
	{
		// Token: 0x04000B27 RID: 2855
		None = 0,
		// Token: 0x04000B28 RID: 2856
		Depth = 1,
		// Token: 0x04000B29 RID: 2857
		DepthNormals = 2,
		// Token: 0x04000B2A RID: 2858
		MotionVectors = 4
	}
}
