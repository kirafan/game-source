﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000086 RID: 134
	[UsedByNativeCode]
	public struct Bounds
	{
		// Token: 0x0600091B RID: 2331 RVA: 0x0000D048 File Offset: 0x0000B248
		public Bounds(Vector3 center, Vector3 size)
		{
			this.m_Center = center;
			this.m_Extents = size * 0.5f;
		}

		// Token: 0x0600091C RID: 2332 RVA: 0x0000D064 File Offset: 0x0000B264
		[ThreadAndSerializationSafe]
		private static bool Internal_Contains(Bounds m, Vector3 point)
		{
			return Bounds.INTERNAL_CALL_Internal_Contains(ref m, ref point);
		}

		// Token: 0x0600091D RID: 2333
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Internal_Contains(ref Bounds m, ref Vector3 point);

		// Token: 0x0600091E RID: 2334 RVA: 0x0000D084 File Offset: 0x0000B284
		public bool Contains(Vector3 point)
		{
			return Bounds.Internal_Contains(this, point);
		}

		// Token: 0x0600091F RID: 2335 RVA: 0x0000D0A8 File Offset: 0x0000B2A8
		private static float Internal_SqrDistance(Bounds m, Vector3 point)
		{
			return Bounds.INTERNAL_CALL_Internal_SqrDistance(ref m, ref point);
		}

		// Token: 0x06000920 RID: 2336
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float INTERNAL_CALL_Internal_SqrDistance(ref Bounds m, ref Vector3 point);

		// Token: 0x06000921 RID: 2337 RVA: 0x0000D0C8 File Offset: 0x0000B2C8
		public float SqrDistance(Vector3 point)
		{
			return Bounds.Internal_SqrDistance(this, point);
		}

		// Token: 0x06000922 RID: 2338 RVA: 0x0000D0EC File Offset: 0x0000B2EC
		private static bool Internal_IntersectRay(ref Ray ray, ref Bounds bounds, out float distance)
		{
			return Bounds.INTERNAL_CALL_Internal_IntersectRay(ref ray, ref bounds, out distance);
		}

		// Token: 0x06000923 RID: 2339
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Internal_IntersectRay(ref Ray ray, ref Bounds bounds, out float distance);

		// Token: 0x06000924 RID: 2340 RVA: 0x0000D10C File Offset: 0x0000B30C
		public bool IntersectRay(Ray ray)
		{
			float num;
			return Bounds.Internal_IntersectRay(ref ray, ref this, out num);
		}

		// Token: 0x06000925 RID: 2341 RVA: 0x0000D12C File Offset: 0x0000B32C
		public bool IntersectRay(Ray ray, out float distance)
		{
			return Bounds.Internal_IntersectRay(ref ray, ref this, out distance);
		}

		// Token: 0x06000926 RID: 2342 RVA: 0x0000D14C File Offset: 0x0000B34C
		private static Vector3 Internal_GetClosestPoint(ref Bounds bounds, ref Vector3 point)
		{
			Vector3 result;
			Bounds.INTERNAL_CALL_Internal_GetClosestPoint(ref bounds, ref point, out result);
			return result;
		}

		// Token: 0x06000927 RID: 2343
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_GetClosestPoint(ref Bounds bounds, ref Vector3 point, out Vector3 value);

		// Token: 0x06000928 RID: 2344 RVA: 0x0000D16C File Offset: 0x0000B36C
		public Vector3 ClosestPoint(Vector3 point)
		{
			return Bounds.Internal_GetClosestPoint(ref this, ref point);
		}

		// Token: 0x06000929 RID: 2345 RVA: 0x0000D18C File Offset: 0x0000B38C
		public override int GetHashCode()
		{
			return this.center.GetHashCode() ^ this.extents.GetHashCode() << 2;
		}

		// Token: 0x0600092A RID: 2346 RVA: 0x0000D1CC File Offset: 0x0000B3CC
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Bounds))
			{
				result = false;
			}
			else
			{
				Bounds bounds = (Bounds)other;
				result = (this.center.Equals(bounds.center) && this.extents.Equals(bounds.extents));
			}
			return result;
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x0600092B RID: 2347 RVA: 0x0000D244 File Offset: 0x0000B444
		// (set) Token: 0x0600092C RID: 2348 RVA: 0x0000D260 File Offset: 0x0000B460
		public Vector3 center
		{
			get
			{
				return this.m_Center;
			}
			set
			{
				this.m_Center = value;
			}
		}

		// Token: 0x1700022F RID: 559
		// (get) Token: 0x0600092D RID: 2349 RVA: 0x0000D26C File Offset: 0x0000B46C
		// (set) Token: 0x0600092E RID: 2350 RVA: 0x0000D294 File Offset: 0x0000B494
		public Vector3 size
		{
			get
			{
				return this.m_Extents * 2f;
			}
			set
			{
				this.m_Extents = value * 0.5f;
			}
		}

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x0600092F RID: 2351 RVA: 0x0000D2A8 File Offset: 0x0000B4A8
		// (set) Token: 0x06000930 RID: 2352 RVA: 0x0000D2C4 File Offset: 0x0000B4C4
		public Vector3 extents
		{
			get
			{
				return this.m_Extents;
			}
			set
			{
				this.m_Extents = value;
			}
		}

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x06000931 RID: 2353 RVA: 0x0000D2D0 File Offset: 0x0000B4D0
		// (set) Token: 0x06000932 RID: 2354 RVA: 0x0000D2F8 File Offset: 0x0000B4F8
		public Vector3 min
		{
			get
			{
				return this.center - this.extents;
			}
			set
			{
				this.SetMinMax(value, this.max);
			}
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x06000933 RID: 2355 RVA: 0x0000D308 File Offset: 0x0000B508
		// (set) Token: 0x06000934 RID: 2356 RVA: 0x0000D330 File Offset: 0x0000B530
		public Vector3 max
		{
			get
			{
				return this.center + this.extents;
			}
			set
			{
				this.SetMinMax(this.min, value);
			}
		}

		// Token: 0x06000935 RID: 2357 RVA: 0x0000D340 File Offset: 0x0000B540
		public static bool operator ==(Bounds lhs, Bounds rhs)
		{
			return lhs.center == rhs.center && lhs.extents == rhs.extents;
		}

		// Token: 0x06000936 RID: 2358 RVA: 0x0000D384 File Offset: 0x0000B584
		public static bool operator !=(Bounds lhs, Bounds rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x06000937 RID: 2359 RVA: 0x0000D3A4 File Offset: 0x0000B5A4
		public void SetMinMax(Vector3 min, Vector3 max)
		{
			this.extents = (max - min) * 0.5f;
			this.center = min + this.extents;
		}

		// Token: 0x06000938 RID: 2360 RVA: 0x0000D3D0 File Offset: 0x0000B5D0
		public void Encapsulate(Vector3 point)
		{
			this.SetMinMax(Vector3.Min(this.min, point), Vector3.Max(this.max, point));
		}

		// Token: 0x06000939 RID: 2361 RVA: 0x0000D3F4 File Offset: 0x0000B5F4
		public void Encapsulate(Bounds bounds)
		{
			this.Encapsulate(bounds.center - bounds.extents);
			this.Encapsulate(bounds.center + bounds.extents);
		}

		// Token: 0x0600093A RID: 2362 RVA: 0x0000D42C File Offset: 0x0000B62C
		public void Expand(float amount)
		{
			amount *= 0.5f;
			this.extents += new Vector3(amount, amount, amount);
		}

		// Token: 0x0600093B RID: 2363 RVA: 0x0000D454 File Offset: 0x0000B654
		public void Expand(Vector3 amount)
		{
			this.extents += amount * 0.5f;
		}

		// Token: 0x0600093C RID: 2364 RVA: 0x0000D474 File Offset: 0x0000B674
		public bool Intersects(Bounds bounds)
		{
			return this.min.x <= bounds.max.x && this.max.x >= bounds.min.x && this.min.y <= bounds.max.y && this.max.y >= bounds.min.y && this.min.z <= bounds.max.z && this.max.z >= bounds.min.z;
		}

		// Token: 0x0600093D RID: 2365 RVA: 0x0000D564 File Offset: 0x0000B764
		public override string ToString()
		{
			return UnityString.Format("Center: {0}, Extents: {1}", new object[]
			{
				this.m_Center,
				this.m_Extents
			});
		}

		// Token: 0x0600093E RID: 2366 RVA: 0x0000D5A8 File Offset: 0x0000B7A8
		public string ToString(string format)
		{
			return UnityString.Format("Center: {0}, Extents: {1}", new object[]
			{
				this.m_Center.ToString(format),
				this.m_Extents.ToString(format)
			});
		}

		// Token: 0x04000112 RID: 274
		private Vector3 m_Center;

		// Token: 0x04000113 RID: 275
		private Vector3 m_Extents;
	}
}
