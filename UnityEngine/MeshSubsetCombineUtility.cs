﻿using System;
using System.Collections.Generic;

namespace UnityEngine
{
	// Token: 0x020003B7 RID: 951
	internal class MeshSubsetCombineUtility
	{
		// Token: 0x020003B8 RID: 952
		public struct MeshInstance
		{
			// Token: 0x04000DC9 RID: 3529
			public int meshInstanceID;

			// Token: 0x04000DCA RID: 3530
			public int rendererInstanceID;

			// Token: 0x04000DCB RID: 3531
			public int additionalVertexStreamsMeshInstanceID;

			// Token: 0x04000DCC RID: 3532
			public Matrix4x4 transform;

			// Token: 0x04000DCD RID: 3533
			public Vector4 lightmapScaleOffset;

			// Token: 0x04000DCE RID: 3534
			public Vector4 realtimeLightmapScaleOffset;
		}

		// Token: 0x020003B9 RID: 953
		public struct SubMeshInstance
		{
			// Token: 0x04000DCF RID: 3535
			public int meshInstanceID;

			// Token: 0x04000DD0 RID: 3536
			public int vertexOffset;

			// Token: 0x04000DD1 RID: 3537
			public int gameObjectInstanceID;

			// Token: 0x04000DD2 RID: 3538
			public int subMeshIndex;

			// Token: 0x04000DD3 RID: 3539
			public Matrix4x4 transform;
		}

		// Token: 0x020003BA RID: 954
		public struct MeshContainer
		{
			// Token: 0x04000DD4 RID: 3540
			public GameObject gameObject;

			// Token: 0x04000DD5 RID: 3541
			public MeshSubsetCombineUtility.MeshInstance instance;

			// Token: 0x04000DD6 RID: 3542
			public List<MeshSubsetCombineUtility.SubMeshInstance> subMeshInstances;
		}
	}
}
