﻿using System;

namespace UnityEngine
{
	// Token: 0x020001F6 RID: 502
	public enum TerrainRenderFlags
	{
		// Token: 0x040005E6 RID: 1510
		heightmap = 1,
		// Token: 0x040005E7 RID: 1511
		trees,
		// Token: 0x040005E8 RID: 1512
		details = 4,
		// Token: 0x040005E9 RID: 1513
		all = 7
	}
}
