﻿using System;

namespace UnityEngine
{
	// Token: 0x020002D2 RID: 722
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
	internal class CppIncludeAttribute : Attribute
	{
		// Token: 0x06002C82 RID: 11394 RVA: 0x00046364 File Offset: 0x00044564
		public CppIncludeAttribute(string header)
		{
		}
	}
}
