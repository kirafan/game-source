﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000D0 RID: 208
	public sealed class CubemapArray : Texture
	{
		// Token: 0x06000E40 RID: 3648 RVA: 0x00013824 File Offset: 0x00011A24
		public CubemapArray(int faceSize, int cubemapCount, TextureFormat format, bool mipmap)
		{
			CubemapArray.Internal_Create(this, faceSize, cubemapCount, format, mipmap, false);
		}

		// Token: 0x06000E41 RID: 3649 RVA: 0x0001383C File Offset: 0x00011A3C
		public CubemapArray(int faceSize, int cubemapCount, TextureFormat format, bool mipmap, bool linear)
		{
			CubemapArray.Internal_Create(this, faceSize, cubemapCount, format, mipmap, linear);
		}

		// Token: 0x1700030F RID: 783
		// (get) Token: 0x06000E42 RID: 3650
		public extern int cubemapCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000310 RID: 784
		// (get) Token: 0x06000E43 RID: 3651
		public extern TextureFormat format { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000E44 RID: 3652
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Apply([DefaultValue("true")] bool updateMipmaps, [DefaultValue("false")] bool makeNoLongerReadable);

		// Token: 0x06000E45 RID: 3653 RVA: 0x00013854 File Offset: 0x00011A54
		[ExcludeFromDocs]
		public void Apply(bool updateMipmaps)
		{
			bool makeNoLongerReadable = false;
			this.Apply(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06000E46 RID: 3654 RVA: 0x0001386C File Offset: 0x00011A6C
		[ExcludeFromDocs]
		public void Apply()
		{
			bool makeNoLongerReadable = false;
			bool updateMipmaps = true;
			this.Apply(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06000E47 RID: 3655
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] CubemapArray mono, int faceSize, int cubemapCount, TextureFormat format, bool mipmap, bool linear);

		// Token: 0x06000E48 RID: 3656
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels(Color[] colors, CubemapFace face, int arrayElement, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E49 RID: 3657 RVA: 0x00013888 File Offset: 0x00011A88
		[ExcludeFromDocs]
		public void SetPixels(Color[] colors, CubemapFace face, int arrayElement)
		{
			int miplevel = 0;
			this.SetPixels(colors, face, arrayElement, miplevel);
		}

		// Token: 0x06000E4A RID: 3658
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels32(Color32[] colors, CubemapFace face, int arrayElement, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E4B RID: 3659 RVA: 0x000138A4 File Offset: 0x00011AA4
		[ExcludeFromDocs]
		public void SetPixels32(Color32[] colors, CubemapFace face, int arrayElement)
		{
			int miplevel = 0;
			this.SetPixels32(colors, face, arrayElement, miplevel);
		}

		// Token: 0x06000E4C RID: 3660
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(CubemapFace face, int arrayElement, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E4D RID: 3661 RVA: 0x000138C0 File Offset: 0x00011AC0
		[ExcludeFromDocs]
		public Color[] GetPixels(CubemapFace face, int arrayElement)
		{
			int miplevel = 0;
			return this.GetPixels(face, arrayElement, miplevel);
		}

		// Token: 0x06000E4E RID: 3662
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color32[] GetPixels32(CubemapFace face, int arrayElement, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E4F RID: 3663 RVA: 0x000138E0 File Offset: 0x00011AE0
		[ExcludeFromDocs]
		public Color32[] GetPixels32(CubemapFace face, int arrayElement)
		{
			int miplevel = 0;
			return this.GetPixels32(face, arrayElement, miplevel);
		}
	}
}
