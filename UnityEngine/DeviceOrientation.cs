﻿using System;

namespace UnityEngine
{
	// Token: 0x02000070 RID: 112
	public enum DeviceOrientation
	{
		// Token: 0x040000C9 RID: 201
		Unknown,
		// Token: 0x040000CA RID: 202
		Portrait,
		// Token: 0x040000CB RID: 203
		PortraitUpsideDown,
		// Token: 0x040000CC RID: 204
		LandscapeLeft,
		// Token: 0x040000CD RID: 205
		LandscapeRight,
		// Token: 0x040000CE RID: 206
		FaceUp,
		// Token: 0x040000CF RID: 207
		FaceDown
	}
}
