﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine.SceneManagement
{
	// Token: 0x020000ED RID: 237
	public struct Scene
	{
		// Token: 0x17000376 RID: 886
		// (get) Token: 0x06001058 RID: 4184 RVA: 0x00016AE4 File Offset: 0x00014CE4
		internal int handle
		{
			get
			{
				return this.m_Handle;
			}
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x06001059 RID: 4185 RVA: 0x00016B00 File Offset: 0x00014D00
		internal Scene.LoadingState loadingState
		{
			get
			{
				return Scene.GetLoadingStateInternal(this.handle);
			}
		}

		// Token: 0x0600105A RID: 4186 RVA: 0x00016B20 File Offset: 0x00014D20
		public bool IsValid()
		{
			return Scene.IsValidInternal(this.handle);
		}

		// Token: 0x17000378 RID: 888
		// (get) Token: 0x0600105B RID: 4187 RVA: 0x00016B40 File Offset: 0x00014D40
		public string path
		{
			get
			{
				return Scene.GetPathInternal(this.handle);
			}
		}

		// Token: 0x17000379 RID: 889
		// (get) Token: 0x0600105C RID: 4188 RVA: 0x00016B60 File Offset: 0x00014D60
		// (set) Token: 0x0600105D RID: 4189 RVA: 0x00016B80 File Offset: 0x00014D80
		public string name
		{
			get
			{
				return Scene.GetNameInternal(this.handle);
			}
			internal set
			{
				Scene.SetNameInternal(this.handle, value);
			}
		}

		// Token: 0x1700037A RID: 890
		// (get) Token: 0x0600105E RID: 4190 RVA: 0x00016B90 File Offset: 0x00014D90
		public bool isLoaded
		{
			get
			{
				return Scene.GetIsLoadedInternal(this.handle);
			}
		}

		// Token: 0x1700037B RID: 891
		// (get) Token: 0x0600105F RID: 4191 RVA: 0x00016BB0 File Offset: 0x00014DB0
		public int buildIndex
		{
			get
			{
				return Scene.GetBuildIndexInternal(this.handle);
			}
		}

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06001060 RID: 4192 RVA: 0x00016BD0 File Offset: 0x00014DD0
		public bool isDirty
		{
			get
			{
				return Scene.GetIsDirtyInternal(this.handle);
			}
		}

		// Token: 0x1700037D RID: 893
		// (get) Token: 0x06001061 RID: 4193 RVA: 0x00016BF0 File Offset: 0x00014DF0
		public int rootCount
		{
			get
			{
				return Scene.GetRootCountInternal(this.handle);
			}
		}

		// Token: 0x06001062 RID: 4194 RVA: 0x00016C10 File Offset: 0x00014E10
		public GameObject[] GetRootGameObjects()
		{
			List<GameObject> list = new List<GameObject>(this.rootCount);
			this.GetRootGameObjects(list);
			return list.ToArray();
		}

		// Token: 0x06001063 RID: 4195 RVA: 0x00016C40 File Offset: 0x00014E40
		public void GetRootGameObjects(List<GameObject> rootGameObjects)
		{
			if (rootGameObjects.Capacity < this.rootCount)
			{
				rootGameObjects.Capacity = this.rootCount;
			}
			rootGameObjects.Clear();
			if (!this.IsValid())
			{
				throw new ArgumentException("The scene is invalid.");
			}
			if (!this.isLoaded)
			{
				throw new ArgumentException("The scene is not loaded.");
			}
			if (this.rootCount != 0)
			{
				Scene.GetRootGameObjectsInternal(this.handle, rootGameObjects);
			}
		}

		// Token: 0x06001064 RID: 4196 RVA: 0x00016CBC File Offset: 0x00014EBC
		public static bool operator ==(Scene lhs, Scene rhs)
		{
			return lhs.handle == rhs.handle;
		}

		// Token: 0x06001065 RID: 4197 RVA: 0x00016CE4 File Offset: 0x00014EE4
		public static bool operator !=(Scene lhs, Scene rhs)
		{
			return lhs.handle != rhs.handle;
		}

		// Token: 0x06001066 RID: 4198 RVA: 0x00016D0C File Offset: 0x00014F0C
		public override int GetHashCode()
		{
			return this.m_Handle;
		}

		// Token: 0x06001067 RID: 4199 RVA: 0x00016D28 File Offset: 0x00014F28
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Scene))
			{
				result = false;
			}
			else
			{
				Scene scene = (Scene)other;
				result = (this.handle == scene.handle);
			}
			return result;
		}

		// Token: 0x06001068 RID: 4200
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsValidInternal(int sceneHandle);

		// Token: 0x06001069 RID: 4201
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetPathInternal(int sceneHandle);

		// Token: 0x0600106A RID: 4202
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetNameInternal(int sceneHandle);

		// Token: 0x0600106B RID: 4203
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetNameInternal(int sceneHandle, string name);

		// Token: 0x0600106C RID: 4204
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetIsLoadedInternal(int sceneHandle);

		// Token: 0x0600106D RID: 4205
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Scene.LoadingState GetLoadingStateInternal(int sceneHandle);

		// Token: 0x0600106E RID: 4206
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetIsDirtyInternal(int sceneHandle);

		// Token: 0x0600106F RID: 4207
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetBuildIndexInternal(int sceneHandle);

		// Token: 0x06001070 RID: 4208
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetRootCountInternal(int sceneHandle);

		// Token: 0x06001071 RID: 4209
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRootGameObjectsInternal(int sceneHandle, object resultRootList);

		// Token: 0x04000271 RID: 625
		private int m_Handle;

		// Token: 0x020000EE RID: 238
		internal enum LoadingState
		{
			// Token: 0x04000273 RID: 627
			NotLoaded,
			// Token: 0x04000274 RID: 628
			Loading,
			// Token: 0x04000275 RID: 629
			Loaded
		}
	}
}
