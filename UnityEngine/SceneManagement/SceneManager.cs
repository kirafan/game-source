﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Events;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine.SceneManagement
{
	// Token: 0x020000F0 RID: 240
	[RequiredByNativeCode]
	public class SceneManager
	{
		// Token: 0x1700037E RID: 894
		// (get) Token: 0x06001073 RID: 4211
		public static extern int sceneCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700037F RID: 895
		// (get) Token: 0x06001074 RID: 4212
		public static extern int sceneCountInBuildSettings { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001075 RID: 4213 RVA: 0x00016D70 File Offset: 0x00014F70
		public static Scene GetActiveScene()
		{
			Scene result;
			SceneManager.INTERNAL_CALL_GetActiveScene(out result);
			return result;
		}

		// Token: 0x06001076 RID: 4214
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetActiveScene(out Scene value);

		// Token: 0x06001077 RID: 4215 RVA: 0x00016D90 File Offset: 0x00014F90
		public static bool SetActiveScene(Scene scene)
		{
			return SceneManager.INTERNAL_CALL_SetActiveScene(ref scene);
		}

		// Token: 0x06001078 RID: 4216
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_SetActiveScene(ref Scene scene);

		// Token: 0x06001079 RID: 4217 RVA: 0x00016DAC File Offset: 0x00014FAC
		public static Scene GetSceneByPath(string scenePath)
		{
			Scene result;
			SceneManager.INTERNAL_CALL_GetSceneByPath(scenePath, out result);
			return result;
		}

		// Token: 0x0600107A RID: 4218
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetSceneByPath(string scenePath, out Scene value);

		// Token: 0x0600107B RID: 4219 RVA: 0x00016DCC File Offset: 0x00014FCC
		public static Scene GetSceneByName(string name)
		{
			Scene result;
			SceneManager.INTERNAL_CALL_GetSceneByName(name, out result);
			return result;
		}

		// Token: 0x0600107C RID: 4220
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetSceneByName(string name, out Scene value);

		// Token: 0x0600107D RID: 4221 RVA: 0x00016DEC File Offset: 0x00014FEC
		public static Scene GetSceneByBuildIndex(int buildIndex)
		{
			Scene result;
			SceneManager.INTERNAL_CALL_GetSceneByBuildIndex(buildIndex, out result);
			return result;
		}

		// Token: 0x0600107E RID: 4222
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetSceneByBuildIndex(int buildIndex, out Scene value);

		// Token: 0x0600107F RID: 4223 RVA: 0x00016E0C File Offset: 0x0001500C
		public static Scene GetSceneAt(int index)
		{
			Scene result;
			SceneManager.INTERNAL_CALL_GetSceneAt(index, out result);
			return result;
		}

		// Token: 0x06001080 RID: 4224
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetSceneAt(int index, out Scene value);

		// Token: 0x06001081 RID: 4225 RVA: 0x00016E2C File Offset: 0x0001502C
		[Obsolete("Use SceneManager.sceneCount and SceneManager.GetSceneAt(int index) to loop the all scenes instead.")]
		public static Scene[] GetAllScenes()
		{
			Scene[] array = new Scene[SceneManager.sceneCount];
			for (int i = 0; i < SceneManager.sceneCount; i++)
			{
				array[i] = SceneManager.GetSceneAt(i);
			}
			return array;
		}

		// Token: 0x06001082 RID: 4226 RVA: 0x00016E78 File Offset: 0x00015078
		[ExcludeFromDocs]
		public static void LoadScene(string sceneName)
		{
			LoadSceneMode mode = LoadSceneMode.Single;
			SceneManager.LoadScene(sceneName, mode);
		}

		// Token: 0x06001083 RID: 4227 RVA: 0x00016E90 File Offset: 0x00015090
		public static void LoadScene(string sceneName, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
		{
			SceneManager.LoadSceneAsyncNameIndexInternal(sceneName, -1, mode == LoadSceneMode.Additive, true);
		}

		// Token: 0x06001084 RID: 4228 RVA: 0x00016EAC File Offset: 0x000150AC
		[ExcludeFromDocs]
		public static void LoadScene(int sceneBuildIndex)
		{
			LoadSceneMode mode = LoadSceneMode.Single;
			SceneManager.LoadScene(sceneBuildIndex, mode);
		}

		// Token: 0x06001085 RID: 4229 RVA: 0x00016EC4 File Offset: 0x000150C4
		public static void LoadScene(int sceneBuildIndex, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
		{
			SceneManager.LoadSceneAsyncNameIndexInternal(null, sceneBuildIndex, mode == LoadSceneMode.Additive, true);
		}

		// Token: 0x06001086 RID: 4230 RVA: 0x00016EE0 File Offset: 0x000150E0
		[ExcludeFromDocs]
		public static AsyncOperation LoadSceneAsync(string sceneName)
		{
			LoadSceneMode mode = LoadSceneMode.Single;
			return SceneManager.LoadSceneAsync(sceneName, mode);
		}

		// Token: 0x06001087 RID: 4231 RVA: 0x00016F00 File Offset: 0x00015100
		public static AsyncOperation LoadSceneAsync(string sceneName, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
		{
			return SceneManager.LoadSceneAsyncNameIndexInternal(sceneName, -1, mode == LoadSceneMode.Additive, false);
		}

		// Token: 0x06001088 RID: 4232 RVA: 0x00016F2C File Offset: 0x0001512C
		[ExcludeFromDocs]
		public static AsyncOperation LoadSceneAsync(int sceneBuildIndex)
		{
			LoadSceneMode mode = LoadSceneMode.Single;
			return SceneManager.LoadSceneAsync(sceneBuildIndex, mode);
		}

		// Token: 0x06001089 RID: 4233 RVA: 0x00016F4C File Offset: 0x0001514C
		public static AsyncOperation LoadSceneAsync(int sceneBuildIndex, [DefaultValue("LoadSceneMode.Single")] LoadSceneMode mode)
		{
			return SceneManager.LoadSceneAsyncNameIndexInternal(null, sceneBuildIndex, mode == LoadSceneMode.Additive, false);
		}

		// Token: 0x0600108A RID: 4234
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AsyncOperation LoadSceneAsyncNameIndexInternal(string sceneName, int sceneBuildIndex, bool isAdditive, bool mustCompleteNextFrame);

		// Token: 0x0600108B RID: 4235 RVA: 0x00016F78 File Offset: 0x00015178
		public static Scene CreateScene(string sceneName)
		{
			Scene result;
			SceneManager.INTERNAL_CALL_CreateScene(sceneName, out result);
			return result;
		}

		// Token: 0x0600108C RID: 4236
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_CreateScene(string sceneName, out Scene value);

		// Token: 0x0600108D RID: 4237 RVA: 0x00016F98 File Offset: 0x00015198
		[Obsolete("Use SceneManager.UnloadSceneAsync. This function is not safe to use during triggers and under other circumstances. See Scripting reference for more details.")]
		public static bool UnloadScene(Scene scene)
		{
			return SceneManager.UnloadSceneInternal(scene);
		}

		// Token: 0x0600108E RID: 4238 RVA: 0x00016FB4 File Offset: 0x000151B4
		private static bool UnloadSceneInternal(Scene scene)
		{
			return SceneManager.INTERNAL_CALL_UnloadSceneInternal(ref scene);
		}

		// Token: 0x0600108F RID: 4239
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_UnloadSceneInternal(ref Scene scene);

		// Token: 0x06001090 RID: 4240 RVA: 0x00016FD0 File Offset: 0x000151D0
		[Obsolete("Use SceneManager.UnloadSceneAsync. This function is not safe to use during triggers and under other circumstances. See Scripting reference for more details.")]
		public static bool UnloadScene(int sceneBuildIndex)
		{
			bool result;
			SceneManager.UnloadSceneNameIndexInternal("", sceneBuildIndex, true, out result);
			return result;
		}

		// Token: 0x06001091 RID: 4241 RVA: 0x00016FF8 File Offset: 0x000151F8
		[Obsolete("Use SceneManager.UnloadSceneAsync. This function is not safe to use during triggers and under other circumstances. See Scripting reference for more details.")]
		public static bool UnloadScene(string sceneName)
		{
			bool result;
			SceneManager.UnloadSceneNameIndexInternal(sceneName, -1, true, out result);
			return result;
		}

		// Token: 0x06001092 RID: 4242 RVA: 0x0001701C File Offset: 0x0001521C
		public static AsyncOperation UnloadSceneAsync(int sceneBuildIndex)
		{
			bool flag;
			return SceneManager.UnloadSceneNameIndexInternal("", sceneBuildIndex, false, out flag);
		}

		// Token: 0x06001093 RID: 4243 RVA: 0x00017040 File Offset: 0x00015240
		public static AsyncOperation UnloadSceneAsync(string sceneName)
		{
			bool flag;
			return SceneManager.UnloadSceneNameIndexInternal(sceneName, -1, false, out flag);
		}

		// Token: 0x06001094 RID: 4244 RVA: 0x00017060 File Offset: 0x00015260
		public static AsyncOperation UnloadSceneAsync(Scene scene)
		{
			return SceneManager.UnloadSceneAsyncInternal(scene);
		}

		// Token: 0x06001095 RID: 4245 RVA: 0x0001707C File Offset: 0x0001527C
		private static AsyncOperation UnloadSceneAsyncInternal(Scene scene)
		{
			return SceneManager.INTERNAL_CALL_UnloadSceneAsyncInternal(ref scene);
		}

		// Token: 0x06001096 RID: 4246
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AsyncOperation INTERNAL_CALL_UnloadSceneAsyncInternal(ref Scene scene);

		// Token: 0x06001097 RID: 4247
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AsyncOperation UnloadSceneNameIndexInternal(string sceneName, int sceneBuildIndex, bool immediately, out bool outSuccess);

		// Token: 0x06001098 RID: 4248 RVA: 0x00017098 File Offset: 0x00015298
		public static void MergeScenes(Scene sourceScene, Scene destinationScene)
		{
			SceneManager.INTERNAL_CALL_MergeScenes(ref sourceScene, ref destinationScene);
		}

		// Token: 0x06001099 RID: 4249
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_MergeScenes(ref Scene sourceScene, ref Scene destinationScene);

		// Token: 0x0600109A RID: 4250 RVA: 0x000170A4 File Offset: 0x000152A4
		public static void MoveGameObjectToScene(GameObject go, Scene scene)
		{
			SceneManager.INTERNAL_CALL_MoveGameObjectToScene(go, ref scene);
		}

		// Token: 0x0600109B RID: 4251
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_MoveGameObjectToScene(GameObject go, ref Scene scene);

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x0600109C RID: 4252 RVA: 0x000170B0 File Offset: 0x000152B0
		// (remove) Token: 0x0600109D RID: 4253 RVA: 0x000170E4 File Offset: 0x000152E4
		public static event UnityAction<Scene, LoadSceneMode> sceneLoaded;

		// Token: 0x0600109E RID: 4254 RVA: 0x00017118 File Offset: 0x00015318
		[RequiredByNativeCode]
		private static void Internal_SceneLoaded(Scene scene, LoadSceneMode mode)
		{
			if (SceneManager.sceneLoaded != null)
			{
				SceneManager.sceneLoaded(scene, mode);
			}
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x0600109F RID: 4255 RVA: 0x00017134 File Offset: 0x00015334
		// (remove) Token: 0x060010A0 RID: 4256 RVA: 0x00017168 File Offset: 0x00015368
		public static event UnityAction<Scene> sceneUnloaded;

		// Token: 0x060010A1 RID: 4257 RVA: 0x0001719C File Offset: 0x0001539C
		[RequiredByNativeCode]
		private static void Internal_SceneUnloaded(Scene scene)
		{
			if (SceneManager.sceneUnloaded != null)
			{
				SceneManager.sceneUnloaded(scene);
			}
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x060010A2 RID: 4258 RVA: 0x000171B8 File Offset: 0x000153B8
		// (remove) Token: 0x060010A3 RID: 4259 RVA: 0x000171EC File Offset: 0x000153EC
		public static event UnityAction<Scene, Scene> activeSceneChanged;

		// Token: 0x060010A4 RID: 4260 RVA: 0x00017220 File Offset: 0x00015420
		[RequiredByNativeCode]
		private static void Internal_ActiveSceneChanged(Scene previousActiveScene, Scene newActiveScene)
		{
			if (SceneManager.activeSceneChanged != null)
			{
				SceneManager.activeSceneChanged(previousActiveScene, newActiveScene);
			}
		}
	}
}
