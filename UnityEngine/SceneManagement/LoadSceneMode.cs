﻿using System;

namespace UnityEngine.SceneManagement
{
	// Token: 0x020000EF RID: 239
	public enum LoadSceneMode
	{
		// Token: 0x04000277 RID: 631
		Single,
		// Token: 0x04000278 RID: 632
		Additive
	}
}
