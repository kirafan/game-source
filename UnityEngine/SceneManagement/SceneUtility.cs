﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.SceneManagement
{
	// Token: 0x020000F1 RID: 241
	public static class SceneUtility
	{
		// Token: 0x060010A5 RID: 4261
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetScenePathByBuildIndex(int buildIndex);

		// Token: 0x060010A6 RID: 4262
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetBuildIndexByScenePath(string scenePath);
	}
}
