﻿using System;

namespace UnityEngine
{
	// Token: 0x020000FF RID: 255
	public enum ParticleSystemMeshShapeType
	{
		// Token: 0x040002BB RID: 699
		Vertex,
		// Token: 0x040002BC RID: 700
		Edge,
		// Token: 0x040002BD RID: 701
		Triangle
	}
}
