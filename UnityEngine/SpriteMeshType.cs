﻿using System;

namespace UnityEngine
{
	// Token: 0x020000BE RID: 190
	public enum SpriteMeshType
	{
		// Token: 0x040001D5 RID: 469
		FullRect,
		// Token: 0x040001D6 RID: 470
		Tight
	}
}
