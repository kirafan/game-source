﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000231 RID: 561
	[Serializable]
	public sealed class GUISettings
	{
		// Token: 0x1700091E RID: 2334
		// (get) Token: 0x060025C9 RID: 9673 RVA: 0x00032E1C File Offset: 0x0003101C
		// (set) Token: 0x060025CA RID: 9674 RVA: 0x00032E38 File Offset: 0x00031038
		public bool doubleClickSelectsWord
		{
			get
			{
				return this.m_DoubleClickSelectsWord;
			}
			set
			{
				this.m_DoubleClickSelectsWord = value;
			}
		}

		// Token: 0x1700091F RID: 2335
		// (get) Token: 0x060025CB RID: 9675 RVA: 0x00032E44 File Offset: 0x00031044
		// (set) Token: 0x060025CC RID: 9676 RVA: 0x00032E60 File Offset: 0x00031060
		public bool tripleClickSelectsLine
		{
			get
			{
				return this.m_TripleClickSelectsLine;
			}
			set
			{
				this.m_TripleClickSelectsLine = value;
			}
		}

		// Token: 0x17000920 RID: 2336
		// (get) Token: 0x060025CD RID: 9677 RVA: 0x00032E6C File Offset: 0x0003106C
		// (set) Token: 0x060025CE RID: 9678 RVA: 0x00032E88 File Offset: 0x00031088
		public Color cursorColor
		{
			get
			{
				return this.m_CursorColor;
			}
			set
			{
				this.m_CursorColor = value;
			}
		}

		// Token: 0x17000921 RID: 2337
		// (get) Token: 0x060025CF RID: 9679 RVA: 0x00032E94 File Offset: 0x00031094
		// (set) Token: 0x060025D0 RID: 9680 RVA: 0x00032ECC File Offset: 0x000310CC
		public float cursorFlashSpeed
		{
			get
			{
				float result;
				if (this.m_CursorFlashSpeed >= 0f)
				{
					result = this.m_CursorFlashSpeed;
				}
				else
				{
					result = GUISettings.Internal_GetCursorFlashSpeed();
				}
				return result;
			}
			set
			{
				this.m_CursorFlashSpeed = value;
			}
		}

		// Token: 0x17000922 RID: 2338
		// (get) Token: 0x060025D1 RID: 9681 RVA: 0x00032ED8 File Offset: 0x000310D8
		// (set) Token: 0x060025D2 RID: 9682 RVA: 0x00032EF4 File Offset: 0x000310F4
		public Color selectionColor
		{
			get
			{
				return this.m_SelectionColor;
			}
			set
			{
				this.m_SelectionColor = value;
			}
		}

		// Token: 0x060025D3 RID: 9683
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float Internal_GetCursorFlashSpeed();

		// Token: 0x0400082B RID: 2091
		[SerializeField]
		private bool m_DoubleClickSelectsWord = true;

		// Token: 0x0400082C RID: 2092
		[SerializeField]
		private bool m_TripleClickSelectsLine = true;

		// Token: 0x0400082D RID: 2093
		[SerializeField]
		private Color m_CursorColor = Color.white;

		// Token: 0x0400082E RID: 2094
		[SerializeField]
		private float m_CursorFlashSpeed = -1f;

		// Token: 0x0400082F RID: 2095
		[SerializeField]
		private Color m_SelectionColor = new Color(0.5f, 0.5f, 1f);
	}
}
