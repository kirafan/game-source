﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E5 RID: 485
	public struct HumanPose
	{
		// Token: 0x06002028 RID: 8232 RVA: 0x000246A0 File Offset: 0x000228A0
		internal void Init()
		{
			if (this.muscles != null)
			{
				if (this.muscles.Length != HumanTrait.MuscleCount)
				{
					throw new ArgumentException("Bad array size for HumanPose.muscles. Size must equal HumanTrait.MuscleCount");
				}
			}
			if (this.muscles == null)
			{
				this.muscles = new float[HumanTrait.MuscleCount];
				if (this.bodyRotation.x == 0f && this.bodyRotation.y == 0f && this.bodyRotation.z == 0f && this.bodyRotation.w == 0f)
				{
					this.bodyRotation.w = 1f;
				}
			}
		}

		// Token: 0x040005AB RID: 1451
		public Vector3 bodyPosition;

		// Token: 0x040005AC RID: 1452
		public Quaternion bodyRotation;

		// Token: 0x040005AD RID: 1453
		public float[] muscles;
	}
}
