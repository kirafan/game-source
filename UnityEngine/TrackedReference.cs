﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine
{
	// Token: 0x02000377 RID: 887
	[StructLayout(LayoutKind.Sequential)]
	public class TrackedReference
	{
		// Token: 0x06002E63 RID: 11875 RVA: 0x0004A7D4 File Offset: 0x000489D4
		protected TrackedReference()
		{
		}

		// Token: 0x06002E64 RID: 11876 RVA: 0x0004A7E0 File Offset: 0x000489E0
		public static bool operator ==(TrackedReference x, TrackedReference y)
		{
			bool result;
			if (y == null && x == null)
			{
				result = true;
			}
			else if (y == null)
			{
				result = (x.m_Ptr == IntPtr.Zero);
			}
			else if (x == null)
			{
				result = (y.m_Ptr == IntPtr.Zero);
			}
			else
			{
				result = (x.m_Ptr == y.m_Ptr);
			}
			return result;
		}

		// Token: 0x06002E65 RID: 11877 RVA: 0x0004A858 File Offset: 0x00048A58
		public static bool operator !=(TrackedReference x, TrackedReference y)
		{
			return !(x == y);
		}

		// Token: 0x06002E66 RID: 11878 RVA: 0x0004A878 File Offset: 0x00048A78
		public override bool Equals(object o)
		{
			return o as TrackedReference == this;
		}

		// Token: 0x06002E67 RID: 11879 RVA: 0x0004A89C File Offset: 0x00048A9C
		public override int GetHashCode()
		{
			return (int)this.m_Ptr;
		}

		// Token: 0x06002E68 RID: 11880 RVA: 0x0004A8BC File Offset: 0x00048ABC
		public static implicit operator bool(TrackedReference exists)
		{
			return exists != null;
		}

		// Token: 0x04000D5A RID: 3418
		internal IntPtr m_Ptr;
	}
}
