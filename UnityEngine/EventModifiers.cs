﻿using System;

namespace UnityEngine
{
	// Token: 0x02000215 RID: 533
	[Flags]
	public enum EventModifiers
	{
		// Token: 0x040007A8 RID: 1960
		None = 0,
		// Token: 0x040007A9 RID: 1961
		Shift = 1,
		// Token: 0x040007AA RID: 1962
		Control = 2,
		// Token: 0x040007AB RID: 1963
		Alt = 4,
		// Token: 0x040007AC RID: 1964
		Command = 8,
		// Token: 0x040007AD RID: 1965
		Numeric = 16,
		// Token: 0x040007AE RID: 1966
		CapsLock = 32,
		// Token: 0x040007AF RID: 1967
		FunctionKey = 64
	}
}
