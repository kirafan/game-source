﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace SimpleJson.Reflection
{
	// Token: 0x02000294 RID: 660
	[GeneratedCode("reflection-utils", "1.0.0")]
	internal class ReflectionUtils
	{
		// Token: 0x06002AB6 RID: 10934 RVA: 0x00040E80 File Offset: 0x0003F080
		public static Attribute GetAttribute(MemberInfo info, Type type)
		{
			Attribute result;
			if (info == null || type == null || !Attribute.IsDefined(info, type))
			{
				result = null;
			}
			else
			{
				result = Attribute.GetCustomAttribute(info, type);
			}
			return result;
		}

		// Token: 0x06002AB7 RID: 10935 RVA: 0x00040EBC File Offset: 0x0003F0BC
		public static Attribute GetAttribute(Type objectType, Type attributeType)
		{
			Attribute result;
			if (objectType == null || attributeType == null || !Attribute.IsDefined(objectType, attributeType))
			{
				result = null;
			}
			else
			{
				result = Attribute.GetCustomAttribute(objectType, attributeType);
			}
			return result;
		}

		// Token: 0x06002AB8 RID: 10936 RVA: 0x00040EF8 File Offset: 0x0003F0F8
		public static Type[] GetGenericTypeArguments(Type type)
		{
			return type.GetGenericArguments();
		}

		// Token: 0x06002AB9 RID: 10937 RVA: 0x00040F14 File Offset: 0x0003F114
		public static bool IsTypeGenericeCollectionInterface(Type type)
		{
			bool result;
			if (!type.IsGenericType)
			{
				result = false;
			}
			else
			{
				Type genericTypeDefinition = type.GetGenericTypeDefinition();
				result = (genericTypeDefinition == typeof(IList<>) || genericTypeDefinition == typeof(ICollection<>) || genericTypeDefinition == typeof(IEnumerable<>));
			}
			return result;
		}

		// Token: 0x06002ABA RID: 10938 RVA: 0x00040F74 File Offset: 0x0003F174
		public static bool IsAssignableFrom(Type type1, Type type2)
		{
			return type1.IsAssignableFrom(type2);
		}

		// Token: 0x06002ABB RID: 10939 RVA: 0x00040F90 File Offset: 0x0003F190
		public static bool IsTypeDictionary(Type type)
		{
			bool result;
			if (typeof(IDictionary).IsAssignableFrom(type))
			{
				result = true;
			}
			else if (!type.IsGenericType)
			{
				result = false;
			}
			else
			{
				Type genericTypeDefinition = type.GetGenericTypeDefinition();
				result = (genericTypeDefinition == typeof(IDictionary<, >));
			}
			return result;
		}

		// Token: 0x06002ABC RID: 10940 RVA: 0x00040FE8 File Offset: 0x0003F1E8
		public static bool IsNullableType(Type type)
		{
			return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
		}

		// Token: 0x06002ABD RID: 10941 RVA: 0x00041020 File Offset: 0x0003F220
		public static object ToNullableType(object obj, Type nullableType)
		{
			return (obj != null) ? Convert.ChangeType(obj, Nullable.GetUnderlyingType(nullableType), CultureInfo.InvariantCulture) : null;
		}

		// Token: 0x06002ABE RID: 10942 RVA: 0x00041054 File Offset: 0x0003F254
		public static bool IsValueType(Type type)
		{
			return type.IsValueType;
		}

		// Token: 0x06002ABF RID: 10943 RVA: 0x00041070 File Offset: 0x0003F270
		public static IEnumerable<ConstructorInfo> GetConstructors(Type type)
		{
			return type.GetConstructors();
		}

		// Token: 0x06002AC0 RID: 10944 RVA: 0x0004108C File Offset: 0x0003F28C
		public static ConstructorInfo GetConstructorInfo(Type type, params Type[] argsType)
		{
			IEnumerable<ConstructorInfo> constructors = ReflectionUtils.GetConstructors(type);
			foreach (ConstructorInfo constructorInfo in constructors)
			{
				ParameterInfo[] parameters = constructorInfo.GetParameters();
				if (argsType.Length == parameters.Length)
				{
					int num = 0;
					bool flag = true;
					foreach (ParameterInfo parameterInfo in constructorInfo.GetParameters())
					{
						if (parameterInfo.ParameterType != argsType[num])
						{
							flag = false;
							break;
						}
					}
					if (flag)
					{
						return constructorInfo;
					}
				}
			}
			return null;
		}

		// Token: 0x06002AC1 RID: 10945 RVA: 0x00041160 File Offset: 0x0003F360
		public static IEnumerable<PropertyInfo> GetProperties(Type type)
		{
			return type.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		// Token: 0x06002AC2 RID: 10946 RVA: 0x00041180 File Offset: 0x0003F380
		public static IEnumerable<FieldInfo> GetFields(Type type)
		{
			return type.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
		}

		// Token: 0x06002AC3 RID: 10947 RVA: 0x000411A0 File Offset: 0x0003F3A0
		public static MethodInfo GetGetterMethodInfo(PropertyInfo propertyInfo)
		{
			return propertyInfo.GetGetMethod(true);
		}

		// Token: 0x06002AC4 RID: 10948 RVA: 0x000411BC File Offset: 0x0003F3BC
		public static MethodInfo GetSetterMethodInfo(PropertyInfo propertyInfo)
		{
			return propertyInfo.GetSetMethod(true);
		}

		// Token: 0x06002AC5 RID: 10949 RVA: 0x000411D8 File Offset: 0x0003F3D8
		public static ReflectionUtils.ConstructorDelegate GetContructor(ConstructorInfo constructorInfo)
		{
			return ReflectionUtils.GetConstructorByReflection(constructorInfo);
		}

		// Token: 0x06002AC6 RID: 10950 RVA: 0x000411F4 File Offset: 0x0003F3F4
		public static ReflectionUtils.ConstructorDelegate GetContructor(Type type, params Type[] argsType)
		{
			return ReflectionUtils.GetConstructorByReflection(type, argsType);
		}

		// Token: 0x06002AC7 RID: 10951 RVA: 0x00041210 File Offset: 0x0003F410
		public static ReflectionUtils.ConstructorDelegate GetConstructorByReflection(ConstructorInfo constructorInfo)
		{
			return (object[] args) => constructorInfo.Invoke(args);
		}

		// Token: 0x06002AC8 RID: 10952 RVA: 0x00041240 File Offset: 0x0003F440
		public static ReflectionUtils.ConstructorDelegate GetConstructorByReflection(Type type, params Type[] argsType)
		{
			ConstructorInfo constructorInfo = ReflectionUtils.GetConstructorInfo(type, argsType);
			return (constructorInfo != null) ? ReflectionUtils.GetConstructorByReflection(constructorInfo) : null;
		}

		// Token: 0x06002AC9 RID: 10953 RVA: 0x00041270 File Offset: 0x0003F470
		public static ReflectionUtils.GetDelegate GetGetMethod(PropertyInfo propertyInfo)
		{
			return ReflectionUtils.GetGetMethodByReflection(propertyInfo);
		}

		// Token: 0x06002ACA RID: 10954 RVA: 0x0004128C File Offset: 0x0003F48C
		public static ReflectionUtils.GetDelegate GetGetMethod(FieldInfo fieldInfo)
		{
			return ReflectionUtils.GetGetMethodByReflection(fieldInfo);
		}

		// Token: 0x06002ACB RID: 10955 RVA: 0x000412A8 File Offset: 0x0003F4A8
		public static ReflectionUtils.GetDelegate GetGetMethodByReflection(PropertyInfo propertyInfo)
		{
			MethodInfo methodInfo = ReflectionUtils.GetGetterMethodInfo(propertyInfo);
			return (object source) => methodInfo.Invoke(source, ReflectionUtils.EmptyObjects);
		}

		// Token: 0x06002ACC RID: 10956 RVA: 0x000412DC File Offset: 0x0003F4DC
		public static ReflectionUtils.GetDelegate GetGetMethodByReflection(FieldInfo fieldInfo)
		{
			return (object source) => fieldInfo.GetValue(source);
		}

		// Token: 0x06002ACD RID: 10957 RVA: 0x0004130C File Offset: 0x0003F50C
		public static ReflectionUtils.SetDelegate GetSetMethod(PropertyInfo propertyInfo)
		{
			return ReflectionUtils.GetSetMethodByReflection(propertyInfo);
		}

		// Token: 0x06002ACE RID: 10958 RVA: 0x00041328 File Offset: 0x0003F528
		public static ReflectionUtils.SetDelegate GetSetMethod(FieldInfo fieldInfo)
		{
			return ReflectionUtils.GetSetMethodByReflection(fieldInfo);
		}

		// Token: 0x06002ACF RID: 10959 RVA: 0x00041344 File Offset: 0x0003F544
		public static ReflectionUtils.SetDelegate GetSetMethodByReflection(PropertyInfo propertyInfo)
		{
			MethodInfo methodInfo = ReflectionUtils.GetSetterMethodInfo(propertyInfo);
			return delegate(object source, object value)
			{
				methodInfo.Invoke(source, new object[]
				{
					value
				});
			};
		}

		// Token: 0x06002AD0 RID: 10960 RVA: 0x00041378 File Offset: 0x0003F578
		public static ReflectionUtils.SetDelegate GetSetMethodByReflection(FieldInfo fieldInfo)
		{
			return delegate(object source, object value)
			{
				fieldInfo.SetValue(source, value);
			};
		}

		// Token: 0x040009F8 RID: 2552
		private static readonly object[] EmptyObjects = new object[0];

		// Token: 0x02000295 RID: 661
		// (Invoke) Token: 0x06002AD3 RID: 10963
		public delegate object GetDelegate(object source);

		// Token: 0x02000296 RID: 662
		// (Invoke) Token: 0x06002AD7 RID: 10967
		public delegate void SetDelegate(object source, object value);

		// Token: 0x02000297 RID: 663
		// (Invoke) Token: 0x06002ADB RID: 10971
		public delegate object ConstructorDelegate(params object[] args);

		// Token: 0x02000298 RID: 664
		// (Invoke) Token: 0x06002ADF RID: 10975
		public delegate TValue ThreadSafeDictionaryValueFactory<TKey, TValue>(TKey key);

		// Token: 0x02000299 RID: 665
		public sealed class ThreadSafeDictionary<TKey, TValue> : IDictionary<TKey, TValue>, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IEnumerable
		{
			// Token: 0x06002AE2 RID: 10978 RVA: 0x000413B8 File Offset: 0x0003F5B8
			public ThreadSafeDictionary(ReflectionUtils.ThreadSafeDictionaryValueFactory<TKey, TValue> valueFactory)
			{
				this._valueFactory = valueFactory;
			}

			// Token: 0x06002AE3 RID: 10979 RVA: 0x000413D4 File Offset: 0x0003F5D4
			private TValue Get(TKey key)
			{
				TValue result;
				TValue tvalue;
				if (this._dictionary == null)
				{
					result = this.AddValue(key);
				}
				else if (!this._dictionary.TryGetValue(key, out tvalue))
				{
					result = this.AddValue(key);
				}
				else
				{
					result = tvalue;
				}
				return result;
			}

			// Token: 0x06002AE4 RID: 10980 RVA: 0x00041424 File Offset: 0x0003F624
			private TValue AddValue(TKey key)
			{
				TValue tvalue = this._valueFactory(key);
				object @lock = this._lock;
				lock (@lock)
				{
					if (this._dictionary == null)
					{
						this._dictionary = new Dictionary<TKey, TValue>();
						this._dictionary[key] = tvalue;
					}
					else
					{
						TValue result;
						if (this._dictionary.TryGetValue(key, out result))
						{
							return result;
						}
						Dictionary<TKey, TValue> dictionary = new Dictionary<TKey, TValue>(this._dictionary);
						dictionary[key] = tvalue;
						this._dictionary = dictionary;
					}
				}
				return tvalue;
			}

			// Token: 0x06002AE5 RID: 10981 RVA: 0x000414D8 File Offset: 0x0003F6D8
			public void Add(TKey key, TValue value)
			{
				throw new NotImplementedException();
			}

			// Token: 0x06002AE6 RID: 10982 RVA: 0x000414E0 File Offset: 0x0003F6E0
			public bool ContainsKey(TKey key)
			{
				return this._dictionary.ContainsKey(key);
			}

			// Token: 0x17000A22 RID: 2594
			// (get) Token: 0x06002AE7 RID: 10983 RVA: 0x00041504 File Offset: 0x0003F704
			public ICollection<TKey> Keys
			{
				get
				{
					return this._dictionary.Keys;
				}
			}

			// Token: 0x06002AE8 RID: 10984 RVA: 0x00041524 File Offset: 0x0003F724
			public bool Remove(TKey key)
			{
				throw new NotImplementedException();
			}

			// Token: 0x06002AE9 RID: 10985 RVA: 0x0004152C File Offset: 0x0003F72C
			public bool TryGetValue(TKey key, out TValue value)
			{
				value = this[key];
				return true;
			}

			// Token: 0x17000A23 RID: 2595
			// (get) Token: 0x06002AEA RID: 10986 RVA: 0x00041550 File Offset: 0x0003F750
			public ICollection<TValue> Values
			{
				get
				{
					return this._dictionary.Values;
				}
			}

			// Token: 0x17000A24 RID: 2596
			public TValue this[TKey key]
			{
				get
				{
					return this.Get(key);
				}
				set
				{
					throw new NotImplementedException();
				}
			}

			// Token: 0x06002AED RID: 10989 RVA: 0x00041594 File Offset: 0x0003F794
			public void Add(KeyValuePair<TKey, TValue> item)
			{
				throw new NotImplementedException();
			}

			// Token: 0x06002AEE RID: 10990 RVA: 0x0004159C File Offset: 0x0003F79C
			public void Clear()
			{
				throw new NotImplementedException();
			}

			// Token: 0x06002AEF RID: 10991 RVA: 0x000415A4 File Offset: 0x0003F7A4
			public bool Contains(KeyValuePair<TKey, TValue> item)
			{
				throw new NotImplementedException();
			}

			// Token: 0x06002AF0 RID: 10992 RVA: 0x000415AC File Offset: 0x0003F7AC
			public void CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
			{
				throw new NotImplementedException();
			}

			// Token: 0x17000A25 RID: 2597
			// (get) Token: 0x06002AF1 RID: 10993 RVA: 0x000415B4 File Offset: 0x0003F7B4
			public int Count
			{
				get
				{
					return this._dictionary.Count;
				}
			}

			// Token: 0x17000A26 RID: 2598
			// (get) Token: 0x06002AF2 RID: 10994 RVA: 0x000415D4 File Offset: 0x0003F7D4
			public bool IsReadOnly
			{
				get
				{
					throw new NotImplementedException();
				}
			}

			// Token: 0x06002AF3 RID: 10995 RVA: 0x000415DC File Offset: 0x0003F7DC
			public bool Remove(KeyValuePair<TKey, TValue> item)
			{
				throw new NotImplementedException();
			}

			// Token: 0x06002AF4 RID: 10996 RVA: 0x000415E4 File Offset: 0x0003F7E4
			public IEnumerator<KeyValuePair<TKey, TValue>> GetEnumerator()
			{
				return this._dictionary.GetEnumerator();
			}

			// Token: 0x06002AF5 RID: 10997 RVA: 0x0004160C File Offset: 0x0003F80C
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this._dictionary.GetEnumerator();
			}

			// Token: 0x040009F9 RID: 2553
			private readonly object _lock = new object();

			// Token: 0x040009FA RID: 2554
			private readonly ReflectionUtils.ThreadSafeDictionaryValueFactory<TKey, TValue> _valueFactory;

			// Token: 0x040009FB RID: 2555
			private Dictionary<TKey, TValue> _dictionary;
		}
	}
}
