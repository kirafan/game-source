﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;

namespace SimpleJson
{
	// Token: 0x0200028F RID: 655
	[GeneratedCode("simple-json", "1.0.0")]
	[EditorBrowsable(EditorBrowsableState.Never)]
	internal class JsonArray : List<object>
	{
		// Token: 0x06002A73 RID: 10867 RVA: 0x0003ED6C File Offset: 0x0003CF6C
		public JsonArray()
		{
		}

		// Token: 0x06002A74 RID: 10868 RVA: 0x0003ED78 File Offset: 0x0003CF78
		public JsonArray(int capacity) : base(capacity)
		{
		}

		// Token: 0x06002A75 RID: 10869 RVA: 0x0003ED84 File Offset: 0x0003CF84
		public override string ToString()
		{
			return SimpleJson.SerializeObject(this) ?? string.Empty;
		}
	}
}
