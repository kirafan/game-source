﻿using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;

namespace SimpleJson
{
	// Token: 0x02000290 RID: 656
	[GeneratedCode("simple-json", "1.0.0")]
	[EditorBrowsable(EditorBrowsableState.Never)]
	internal class JsonObject : IDictionary<string, object>, ICollection<KeyValuePair<string, object>>, IEnumerable<KeyValuePair<string, object>>, IEnumerable
	{
		// Token: 0x06002A76 RID: 10870 RVA: 0x0003EDAC File Offset: 0x0003CFAC
		public JsonObject()
		{
			this._members = new Dictionary<string, object>();
		}

		// Token: 0x06002A77 RID: 10871 RVA: 0x0003EDC0 File Offset: 0x0003CFC0
		public JsonObject(IEqualityComparer<string> comparer)
		{
			this._members = new Dictionary<string, object>(comparer);
		}

		// Token: 0x17000A1A RID: 2586
		public object this[int index]
		{
			get
			{
				return JsonObject.GetAtIndex(this._members, index);
			}
		}

		// Token: 0x06002A79 RID: 10873 RVA: 0x0003EDFC File Offset: 0x0003CFFC
		internal static object GetAtIndex(IDictionary<string, object> obj, int index)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (index >= obj.Count)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			int num = 0;
			foreach (KeyValuePair<string, object> keyValuePair in obj)
			{
				if (num++ == index)
				{
					return keyValuePair.Value;
				}
			}
			return null;
		}

		// Token: 0x06002A7A RID: 10874 RVA: 0x0003EE98 File Offset: 0x0003D098
		public void Add(string key, object value)
		{
			this._members.Add(key, value);
		}

		// Token: 0x06002A7B RID: 10875 RVA: 0x0003EEA8 File Offset: 0x0003D0A8
		public bool ContainsKey(string key)
		{
			return this._members.ContainsKey(key);
		}

		// Token: 0x17000A1B RID: 2587
		// (get) Token: 0x06002A7C RID: 10876 RVA: 0x0003EECC File Offset: 0x0003D0CC
		public ICollection<string> Keys
		{
			get
			{
				return this._members.Keys;
			}
		}

		// Token: 0x06002A7D RID: 10877 RVA: 0x0003EEEC File Offset: 0x0003D0EC
		public bool Remove(string key)
		{
			return this._members.Remove(key);
		}

		// Token: 0x06002A7E RID: 10878 RVA: 0x0003EF10 File Offset: 0x0003D110
		public bool TryGetValue(string key, out object value)
		{
			return this._members.TryGetValue(key, out value);
		}

		// Token: 0x17000A1C RID: 2588
		// (get) Token: 0x06002A7F RID: 10879 RVA: 0x0003EF34 File Offset: 0x0003D134
		public ICollection<object> Values
		{
			get
			{
				return this._members.Values;
			}
		}

		// Token: 0x17000A1D RID: 2589
		public object this[string key]
		{
			get
			{
				return this._members[key];
			}
			set
			{
				this._members[key] = value;
			}
		}

		// Token: 0x06002A82 RID: 10882 RVA: 0x0003EF88 File Offset: 0x0003D188
		public void Add(KeyValuePair<string, object> item)
		{
			this._members.Add(item.Key, item.Value);
		}

		// Token: 0x06002A83 RID: 10883 RVA: 0x0003EFA4 File Offset: 0x0003D1A4
		public void Clear()
		{
			this._members.Clear();
		}

		// Token: 0x06002A84 RID: 10884 RVA: 0x0003EFB4 File Offset: 0x0003D1B4
		public bool Contains(KeyValuePair<string, object> item)
		{
			return this._members.ContainsKey(item.Key) && this._members[item.Key] == item.Value;
		}

		// Token: 0x06002A85 RID: 10885 RVA: 0x0003F000 File Offset: 0x0003D200
		public void CopyTo(KeyValuePair<string, object>[] array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			int num = this.Count;
			foreach (KeyValuePair<string, object> keyValuePair in this)
			{
				array[arrayIndex++] = keyValuePair;
				if (--num <= 0)
				{
					break;
				}
			}
		}

		// Token: 0x17000A1E RID: 2590
		// (get) Token: 0x06002A86 RID: 10886 RVA: 0x0003F08C File Offset: 0x0003D28C
		public int Count
		{
			get
			{
				return this._members.Count;
			}
		}

		// Token: 0x17000A1F RID: 2591
		// (get) Token: 0x06002A87 RID: 10887 RVA: 0x0003F0AC File Offset: 0x0003D2AC
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06002A88 RID: 10888 RVA: 0x0003F0C4 File Offset: 0x0003D2C4
		public bool Remove(KeyValuePair<string, object> item)
		{
			return this._members.Remove(item.Key);
		}

		// Token: 0x06002A89 RID: 10889 RVA: 0x0003F0EC File Offset: 0x0003D2EC
		public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
		{
			return this._members.GetEnumerator();
		}

		// Token: 0x06002A8A RID: 10890 RVA: 0x0003F114 File Offset: 0x0003D314
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this._members.GetEnumerator();
		}

		// Token: 0x06002A8B RID: 10891 RVA: 0x0003F13C File Offset: 0x0003D33C
		public override string ToString()
		{
			return SimpleJson.SerializeObject(this);
		}

		// Token: 0x040009E2 RID: 2530
		private readonly Dictionary<string, object> _members;
	}
}
