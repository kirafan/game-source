﻿using System;
using System.CodeDom.Compiler;

namespace SimpleJson
{
	// Token: 0x02000292 RID: 658
	[GeneratedCode("simple-json", "1.0.0")]
	internal interface IJsonSerializerStrategy
	{
		// Token: 0x06002AA8 RID: 10920
		bool TrySerializeNonPrimitiveObject(object input, out object output);

		// Token: 0x06002AA9 RID: 10921
		object DeserializeObject(object value, Type type);
	}
}
