﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Audio
{
	// Token: 0x020001B2 RID: 434
	public class AudioMixerSnapshot : Object
	{
		// Token: 0x06001DF5 RID: 7669 RVA: 0x00022884 File Offset: 0x00020A84
		internal AudioMixerSnapshot()
		{
		}

		// Token: 0x1700076F RID: 1903
		// (get) Token: 0x06001DF6 RID: 7670
		public extern AudioMixer audioMixer { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001DF7 RID: 7671
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void TransitionTo(float timeToReach);
	}
}
