﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Audio
{
	// Token: 0x020001B3 RID: 435
	public class AudioMixerGroup : Object
	{
		// Token: 0x06001DF8 RID: 7672 RVA: 0x00022890 File Offset: 0x00020A90
		internal AudioMixerGroup()
		{
		}

		// Token: 0x17000770 RID: 1904
		// (get) Token: 0x06001DF9 RID: 7673
		public extern AudioMixer audioMixer { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
