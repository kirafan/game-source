﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Audio
{
	// Token: 0x020001B1 RID: 433
	public class AudioMixer : Object
	{
		// Token: 0x06001DE9 RID: 7657 RVA: 0x000227F0 File Offset: 0x000209F0
		internal AudioMixer()
		{
		}

		// Token: 0x1700076D RID: 1901
		// (get) Token: 0x06001DEA RID: 7658
		// (set) Token: 0x06001DEB RID: 7659
		public extern AudioMixerGroup outputAudioMixerGroup { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001DEC RID: 7660
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AudioMixerGroup[] FindMatchingGroups(string subPath);

		// Token: 0x06001DED RID: 7661
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AudioMixerSnapshot FindSnapshot(string name);

		// Token: 0x06001DEE RID: 7662 RVA: 0x000227FC File Offset: 0x000209FC
		private void TransitionToSnapshot(AudioMixerSnapshot snapshot, float timeToReach)
		{
			if (snapshot == null)
			{
				throw new ArgumentException("null Snapshot passed to AudioMixer.TransitionToSnapshot of AudioMixer '" + base.name + "'");
			}
			if (snapshot.audioMixer != this)
			{
				throw new ArgumentException(string.Concat(new string[]
				{
					"Snapshot '",
					snapshot.name,
					"' passed to AudioMixer.TransitionToSnapshot is not a snapshot from AudioMixer '",
					base.name,
					"'"
				}));
			}
			snapshot.TransitionTo(timeToReach);
		}

		// Token: 0x06001DEF RID: 7663
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void TransitionToSnapshots(AudioMixerSnapshot[] snapshots, float[] weights, float timeToReach);

		// Token: 0x1700076E RID: 1902
		// (get) Token: 0x06001DF0 RID: 7664
		// (set) Token: 0x06001DF1 RID: 7665
		public extern AudioMixerUpdateMode updateMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001DF2 RID: 7666
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SetFloat(string name, float value);

		// Token: 0x06001DF3 RID: 7667
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool ClearFloat(string name);

		// Token: 0x06001DF4 RID: 7668
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetFloat(string name, out float value);
	}
}
