﻿using System;

namespace UnityEngine.Audio
{
	// Token: 0x020001B0 RID: 432
	public enum AudioMixerUpdateMode
	{
		// Token: 0x0400049C RID: 1180
		Normal,
		// Token: 0x0400049D RID: 1181
		UnscaledTime
	}
}
