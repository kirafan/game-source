﻿using System;

namespace UnityEngine
{
	// Token: 0x02000185 RID: 389
	public struct WheelHit
	{
		// Token: 0x17000697 RID: 1687
		// (get) Token: 0x06001BA8 RID: 7080 RVA: 0x00021980 File Offset: 0x0001FB80
		// (set) Token: 0x06001BA9 RID: 7081 RVA: 0x0002199C File Offset: 0x0001FB9C
		public Collider collider
		{
			get
			{
				return this.m_Collider;
			}
			set
			{
				this.m_Collider = value;
			}
		}

		// Token: 0x17000698 RID: 1688
		// (get) Token: 0x06001BAA RID: 7082 RVA: 0x000219A8 File Offset: 0x0001FBA8
		// (set) Token: 0x06001BAB RID: 7083 RVA: 0x000219C4 File Offset: 0x0001FBC4
		public Vector3 point
		{
			get
			{
				return this.m_Point;
			}
			set
			{
				this.m_Point = value;
			}
		}

		// Token: 0x17000699 RID: 1689
		// (get) Token: 0x06001BAC RID: 7084 RVA: 0x000219D0 File Offset: 0x0001FBD0
		// (set) Token: 0x06001BAD RID: 7085 RVA: 0x000219EC File Offset: 0x0001FBEC
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
			set
			{
				this.m_Normal = value;
			}
		}

		// Token: 0x1700069A RID: 1690
		// (get) Token: 0x06001BAE RID: 7086 RVA: 0x000219F8 File Offset: 0x0001FBF8
		// (set) Token: 0x06001BAF RID: 7087 RVA: 0x00021A14 File Offset: 0x0001FC14
		public Vector3 forwardDir
		{
			get
			{
				return this.m_ForwardDir;
			}
			set
			{
				this.m_ForwardDir = value;
			}
		}

		// Token: 0x1700069B RID: 1691
		// (get) Token: 0x06001BB0 RID: 7088 RVA: 0x00021A20 File Offset: 0x0001FC20
		// (set) Token: 0x06001BB1 RID: 7089 RVA: 0x00021A3C File Offset: 0x0001FC3C
		public Vector3 sidewaysDir
		{
			get
			{
				return this.m_SidewaysDir;
			}
			set
			{
				this.m_SidewaysDir = value;
			}
		}

		// Token: 0x1700069C RID: 1692
		// (get) Token: 0x06001BB2 RID: 7090 RVA: 0x00021A48 File Offset: 0x0001FC48
		// (set) Token: 0x06001BB3 RID: 7091 RVA: 0x00021A64 File Offset: 0x0001FC64
		public float force
		{
			get
			{
				return this.m_Force;
			}
			set
			{
				this.m_Force = value;
			}
		}

		// Token: 0x1700069D RID: 1693
		// (get) Token: 0x06001BB4 RID: 7092 RVA: 0x00021A70 File Offset: 0x0001FC70
		// (set) Token: 0x06001BB5 RID: 7093 RVA: 0x00021A8C File Offset: 0x0001FC8C
		public float forwardSlip
		{
			get
			{
				return this.m_ForwardSlip;
			}
			set
			{
				this.m_Force = this.m_ForwardSlip;
			}
		}

		// Token: 0x1700069E RID: 1694
		// (get) Token: 0x06001BB6 RID: 7094 RVA: 0x00021A9C File Offset: 0x0001FC9C
		// (set) Token: 0x06001BB7 RID: 7095 RVA: 0x00021AB8 File Offset: 0x0001FCB8
		public float sidewaysSlip
		{
			get
			{
				return this.m_SidewaysSlip;
			}
			set
			{
				this.m_SidewaysSlip = value;
			}
		}

		// Token: 0x04000409 RID: 1033
		private Vector3 m_Point;

		// Token: 0x0400040A RID: 1034
		private Vector3 m_Normal;

		// Token: 0x0400040B RID: 1035
		private Vector3 m_ForwardDir;

		// Token: 0x0400040C RID: 1036
		private Vector3 m_SidewaysDir;

		// Token: 0x0400040D RID: 1037
		private float m_Force;

		// Token: 0x0400040E RID: 1038
		private float m_ForwardSlip;

		// Token: 0x0400040F RID: 1039
		private float m_SidewaysSlip;

		// Token: 0x04000410 RID: 1040
		private Collider m_Collider;
	}
}
