﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200012D RID: 301
	public sealed class ParticleSystemRenderer : Renderer
	{
		// Token: 0x170004E4 RID: 1252
		// (get) Token: 0x060015B8 RID: 5560
		// (set) Token: 0x060015B9 RID: 5561
		public extern ParticleSystemRenderMode renderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004E5 RID: 1253
		// (get) Token: 0x060015BA RID: 5562
		// (set) Token: 0x060015BB RID: 5563
		public extern float lengthScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004E6 RID: 1254
		// (get) Token: 0x060015BC RID: 5564
		// (set) Token: 0x060015BD RID: 5565
		public extern float velocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004E7 RID: 1255
		// (get) Token: 0x060015BE RID: 5566
		// (set) Token: 0x060015BF RID: 5567
		public extern float cameraVelocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004E8 RID: 1256
		// (get) Token: 0x060015C0 RID: 5568
		// (set) Token: 0x060015C1 RID: 5569
		public extern float normalDirection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004E9 RID: 1257
		// (get) Token: 0x060015C2 RID: 5570
		// (set) Token: 0x060015C3 RID: 5571
		public extern ParticleSystemRenderSpace alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004EA RID: 1258
		// (get) Token: 0x060015C4 RID: 5572 RVA: 0x0001BC1C File Offset: 0x00019E1C
		// (set) Token: 0x060015C5 RID: 5573 RVA: 0x0001BC3C File Offset: 0x00019E3C
		public Vector3 pivot
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_pivot(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_pivot(ref value);
			}
		}

		// Token: 0x060015C6 RID: 5574
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_pivot(out Vector3 value);

		// Token: 0x060015C7 RID: 5575
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_pivot(ref Vector3 value);

		// Token: 0x170004EB RID: 1259
		// (get) Token: 0x060015C8 RID: 5576
		// (set) Token: 0x060015C9 RID: 5577
		public extern ParticleSystemSortMode sortMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004EC RID: 1260
		// (get) Token: 0x060015CA RID: 5578
		// (set) Token: 0x060015CB RID: 5579
		public extern float sortingFudge { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004ED RID: 1261
		// (get) Token: 0x060015CC RID: 5580
		// (set) Token: 0x060015CD RID: 5581
		public extern float minParticleSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004EE RID: 1262
		// (get) Token: 0x060015CE RID: 5582
		// (set) Token: 0x060015CF RID: 5583
		public extern float maxParticleSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004EF RID: 1263
		// (get) Token: 0x060015D0 RID: 5584
		// (set) Token: 0x060015D1 RID: 5585
		public extern Mesh mesh { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170004F0 RID: 1264
		// (get) Token: 0x060015D2 RID: 5586 RVA: 0x0001BC48 File Offset: 0x00019E48
		public int meshCount
		{
			get
			{
				return this.Internal_GetMeshCount();
			}
		}

		// Token: 0x060015D3 RID: 5587
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int Internal_GetMeshCount();

		// Token: 0x060015D4 RID: 5588
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetMeshes(Mesh[] meshes);

		// Token: 0x060015D5 RID: 5589 RVA: 0x0001BC64 File Offset: 0x00019E64
		public void SetMeshes(Mesh[] meshes)
		{
			this.SetMeshes(meshes, meshes.Length);
		}

		// Token: 0x060015D6 RID: 5590
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetMeshes(Mesh[] meshes, int size);

		// Token: 0x170004F1 RID: 1265
		// (get) Token: 0x060015D7 RID: 5591
		// (set) Token: 0x060015D8 RID: 5592
		public extern Material trailMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060015D9 RID: 5593 RVA: 0x0001BC74 File Offset: 0x00019E74
		public void EnableVertexStreams(ParticleSystemVertexStreams streams)
		{
			this.Internal_SetVertexStreams(streams, true);
		}

		// Token: 0x060015DA RID: 5594 RVA: 0x0001BC80 File Offset: 0x00019E80
		public void DisableVertexStreams(ParticleSystemVertexStreams streams)
		{
			this.Internal_SetVertexStreams(streams, false);
		}

		// Token: 0x060015DB RID: 5595 RVA: 0x0001BC8C File Offset: 0x00019E8C
		public bool AreVertexStreamsEnabled(ParticleSystemVertexStreams streams)
		{
			return this.Internal_GetEnabledVertexStreams(streams) == streams;
		}

		// Token: 0x060015DC RID: 5596 RVA: 0x0001BCAC File Offset: 0x00019EAC
		public ParticleSystemVertexStreams GetEnabledVertexStreams(ParticleSystemVertexStreams streams)
		{
			return this.Internal_GetEnabledVertexStreams(streams);
		}

		// Token: 0x060015DD RID: 5597
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetVertexStreams(ParticleSystemVertexStreams streams, bool enabled);

		// Token: 0x060015DE RID: 5598
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern ParticleSystemVertexStreams Internal_GetEnabledVertexStreams(ParticleSystemVertexStreams streams);
	}
}
