﻿using System;

namespace UnityEngine.VR.WSA
{
	// Token: 0x020002A5 RID: 677
	public enum PositionalLocatorState
	{
		// Token: 0x04000A11 RID: 2577
		Unavailable,
		// Token: 0x04000A12 RID: 2578
		OrientationOnly,
		// Token: 0x04000A13 RID: 2579
		Activating,
		// Token: 0x04000A14 RID: 2580
		Active,
		// Token: 0x04000A15 RID: 2581
		Inhibited
	}
}
