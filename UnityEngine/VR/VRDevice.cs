﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.VR
{
	// Token: 0x020002A2 RID: 674
	public static class VRDevice
	{
		// Token: 0x17000A32 RID: 2610
		// (get) Token: 0x06002B13 RID: 11027
		public static extern bool isPresent { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000A33 RID: 2611
		// (get) Token: 0x06002B14 RID: 11028
		[Obsolete("family is deprecated.  Use VRSettings.loadedDeviceName instead.")]
		public static extern string family { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000A34 RID: 2612
		// (get) Token: 0x06002B15 RID: 11029
		public static extern string model { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000A35 RID: 2613
		// (get) Token: 0x06002B16 RID: 11030
		public static extern float refreshRate { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06002B17 RID: 11031 RVA: 0x000417C4 File Offset: 0x0003F9C4
		public static IntPtr GetNativePtr()
		{
			IntPtr result;
			VRDevice.INTERNAL_CALL_GetNativePtr(out result);
			return result;
		}

		// Token: 0x06002B18 RID: 11032
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetNativePtr(out IntPtr value);
	}
}
