﻿using System;

namespace UnityEngine.VR
{
	// Token: 0x020002A0 RID: 672
	public enum VRNode
	{
		// Token: 0x04000A0A RID: 2570
		LeftEye,
		// Token: 0x04000A0B RID: 2571
		RightEye,
		// Token: 0x04000A0C RID: 2572
		CenterEye,
		// Token: 0x04000A0D RID: 2573
		Head,
		// Token: 0x04000A0E RID: 2574
		LeftHand,
		// Token: 0x04000A0F RID: 2575
		RightHand
	}
}
