﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.VR
{
	// Token: 0x020002A1 RID: 673
	public static class VRSettings
	{
		// Token: 0x17000A27 RID: 2599
		// (get) Token: 0x06002B00 RID: 11008
		// (set) Token: 0x06002B01 RID: 11009
		public static extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A28 RID: 2600
		// (get) Token: 0x06002B02 RID: 11010
		public static extern bool isDeviceActive { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000A29 RID: 2601
		// (get) Token: 0x06002B03 RID: 11011
		// (set) Token: 0x06002B04 RID: 11012
		public static extern bool showDeviceView { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A2A RID: 2602
		// (get) Token: 0x06002B05 RID: 11013
		// (set) Token: 0x06002B06 RID: 11014
		public static extern float renderScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A2B RID: 2603
		// (get) Token: 0x06002B07 RID: 11015
		public static extern int eyeTextureWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000A2C RID: 2604
		// (get) Token: 0x06002B08 RID: 11016
		public static extern int eyeTextureHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000A2D RID: 2605
		// (get) Token: 0x06002B09 RID: 11017 RVA: 0x000416F8 File Offset: 0x0003F8F8
		// (set) Token: 0x06002B0A RID: 11018 RVA: 0x00041714 File Offset: 0x0003F914
		public static float renderViewportScale
		{
			get
			{
				return VRSettings.renderViewportScaleInternal;
			}
			set
			{
				if (value < 0f || value > 1f)
				{
					throw new ArgumentOutOfRangeException("value", "Render viewport scale should be between 0 and 1.");
				}
				VRSettings.renderViewportScaleInternal = value;
			}
		}

		// Token: 0x17000A2E RID: 2606
		// (get) Token: 0x06002B0B RID: 11019
		// (set) Token: 0x06002B0C RID: 11020
		internal static extern float renderViewportScaleInternal { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A2F RID: 2607
		// (get) Token: 0x06002B0D RID: 11021 RVA: 0x00041744 File Offset: 0x0003F944
		// (set) Token: 0x06002B0E RID: 11022 RVA: 0x00041798 File Offset: 0x0003F998
		[Obsolete("loadedDevice is deprecated.  Use loadedDeviceName and LoadDeviceByName instead.")]
		public static VRDeviceType loadedDevice
		{
			get
			{
				VRDeviceType result = VRDeviceType.Unknown;
				try
				{
					result = (VRDeviceType)Enum.Parse(typeof(VRDeviceType), VRSettings.loadedDeviceName, true);
				}
				catch (Exception)
				{
				}
				return result;
			}
			set
			{
				VRSettings.LoadDeviceByName(value.ToString());
			}
		}

		// Token: 0x17000A30 RID: 2608
		// (get) Token: 0x06002B0F RID: 11023
		public static extern string loadedDeviceName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06002B10 RID: 11024 RVA: 0x000417B0 File Offset: 0x0003F9B0
		public static void LoadDeviceByName(string deviceName)
		{
			VRSettings.LoadDeviceByName(new string[]
			{
				deviceName
			});
		}

		// Token: 0x06002B11 RID: 11025
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void LoadDeviceByName(string[] prioritizedDeviceNameList);

		// Token: 0x17000A31 RID: 2609
		// (get) Token: 0x06002B12 RID: 11026
		public static extern string[] supportedDevices { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
