﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.VR
{
	// Token: 0x020002A3 RID: 675
	public static class VRStats
	{
		// Token: 0x17000A36 RID: 2614
		// (get) Token: 0x06002B19 RID: 11033
		public static extern float gpuTimeLastFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
