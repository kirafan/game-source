﻿using System;

namespace UnityEngine.VR
{
	// Token: 0x0200029F RID: 671
	[Obsolete("VRDeviceType is deprecated. Use VRSettings.supportedDevices instead.")]
	public enum VRDeviceType
	{
		// Token: 0x04000A02 RID: 2562
		[Obsolete("Enum member VRDeviceType.Morpheus has been deprecated. Use VRDeviceType.PlayStationVR instead (UnityUpgradable) -> PlayStationVR", true)]
		Morpheus = -1,
		// Token: 0x04000A03 RID: 2563
		None,
		// Token: 0x04000A04 RID: 2564
		Stereo,
		// Token: 0x04000A05 RID: 2565
		Split,
		// Token: 0x04000A06 RID: 2566
		Oculus,
		// Token: 0x04000A07 RID: 2567
		PlayStationVR,
		// Token: 0x04000A08 RID: 2568
		Unknown
	}
}
