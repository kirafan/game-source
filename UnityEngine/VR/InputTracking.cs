﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.VR
{
	// Token: 0x020002A4 RID: 676
	public static class InputTracking
	{
		// Token: 0x06002B1A RID: 11034 RVA: 0x000417E4 File Offset: 0x0003F9E4
		public static Vector3 GetLocalPosition(VRNode node)
		{
			Vector3 result;
			InputTracking.INTERNAL_CALL_GetLocalPosition(node, out result);
			return result;
		}

		// Token: 0x06002B1B RID: 11035
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetLocalPosition(VRNode node, out Vector3 value);

		// Token: 0x06002B1C RID: 11036 RVA: 0x00041804 File Offset: 0x0003FA04
		public static Quaternion GetLocalRotation(VRNode node)
		{
			Quaternion result;
			InputTracking.INTERNAL_CALL_GetLocalRotation(node, out result);
			return result;
		}

		// Token: 0x06002B1D RID: 11037
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetLocalRotation(VRNode node, out Quaternion value);

		// Token: 0x06002B1E RID: 11038
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Recenter();
	}
}
