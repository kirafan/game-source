﻿using System;

namespace UnityEngine
{
	// Token: 0x020000BB RID: 187
	public enum SpriteAlignment
	{
		// Token: 0x040001C4 RID: 452
		Center,
		// Token: 0x040001C5 RID: 453
		TopLeft,
		// Token: 0x040001C6 RID: 454
		TopCenter,
		// Token: 0x040001C7 RID: 455
		TopRight,
		// Token: 0x040001C8 RID: 456
		LeftCenter,
		// Token: 0x040001C9 RID: 457
		RightCenter,
		// Token: 0x040001CA RID: 458
		BottomLeft,
		// Token: 0x040001CB RID: 459
		BottomCenter,
		// Token: 0x040001CC RID: 460
		BottomRight,
		// Token: 0x040001CD RID: 461
		Custom
	}
}
