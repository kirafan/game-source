﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200013D RID: 317
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class ControllerColliderHit
	{
		// Token: 0x17000510 RID: 1296
		// (get) Token: 0x06001627 RID: 5671 RVA: 0x0001C2CC File Offset: 0x0001A4CC
		public CharacterController controller
		{
			get
			{
				return this.m_Controller;
			}
		}

		// Token: 0x17000511 RID: 1297
		// (get) Token: 0x06001628 RID: 5672 RVA: 0x0001C2E8 File Offset: 0x0001A4E8
		public Collider collider
		{
			get
			{
				return this.m_Collider;
			}
		}

		// Token: 0x17000512 RID: 1298
		// (get) Token: 0x06001629 RID: 5673 RVA: 0x0001C304 File Offset: 0x0001A504
		public Rigidbody rigidbody
		{
			get
			{
				return this.m_Collider.attachedRigidbody;
			}
		}

		// Token: 0x17000513 RID: 1299
		// (get) Token: 0x0600162A RID: 5674 RVA: 0x0001C324 File Offset: 0x0001A524
		public GameObject gameObject
		{
			get
			{
				return this.m_Collider.gameObject;
			}
		}

		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x0600162B RID: 5675 RVA: 0x0001C344 File Offset: 0x0001A544
		public Transform transform
		{
			get
			{
				return this.m_Collider.transform;
			}
		}

		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x0600162C RID: 5676 RVA: 0x0001C364 File Offset: 0x0001A564
		public Vector3 point
		{
			get
			{
				return this.m_Point;
			}
		}

		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x0600162D RID: 5677 RVA: 0x0001C380 File Offset: 0x0001A580
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
		}

		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x0600162E RID: 5678 RVA: 0x0001C39C File Offset: 0x0001A59C
		public Vector3 moveDirection
		{
			get
			{
				return this.m_MoveDirection;
			}
		}

		// Token: 0x17000518 RID: 1304
		// (get) Token: 0x0600162F RID: 5679 RVA: 0x0001C3B8 File Offset: 0x0001A5B8
		public float moveLength
		{
			get
			{
				return this.m_MoveLength;
			}
		}

		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x06001630 RID: 5680 RVA: 0x0001C3D4 File Offset: 0x0001A5D4
		// (set) Token: 0x06001631 RID: 5681 RVA: 0x0001C3F8 File Offset: 0x0001A5F8
		private bool push
		{
			get
			{
				return this.m_Push != 0;
			}
			set
			{
				this.m_Push = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x04000384 RID: 900
		internal CharacterController m_Controller;

		// Token: 0x04000385 RID: 901
		internal Collider m_Collider;

		// Token: 0x04000386 RID: 902
		internal Vector3 m_Point;

		// Token: 0x04000387 RID: 903
		internal Vector3 m_Normal;

		// Token: 0x04000388 RID: 904
		internal Vector3 m_MoveDirection;

		// Token: 0x04000389 RID: 905
		internal float m_MoveLength;

		// Token: 0x0400038A RID: 906
		internal int m_Push;
	}
}
