﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001E4 RID: 484
	public sealed class HumanTrait
	{
		// Token: 0x17000806 RID: 2054
		// (get) Token: 0x0600201C RID: 8220
		public static extern int MuscleCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000807 RID: 2055
		// (get) Token: 0x0600201D RID: 8221
		public static extern string[] MuscleName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000808 RID: 2056
		// (get) Token: 0x0600201E RID: 8222
		public static extern int BoneCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000809 RID: 2057
		// (get) Token: 0x0600201F RID: 8223
		public static extern string[] BoneName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06002020 RID: 8224
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int MuscleFromBone(int i, int dofIndex);

		// Token: 0x06002021 RID: 8225
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int BoneFromMuscle(int i);

		// Token: 0x06002022 RID: 8226
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool RequiredBone(int i);

		// Token: 0x1700080A RID: 2058
		// (get) Token: 0x06002023 RID: 8227
		public static extern int RequiredBoneCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06002024 RID: 8228
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool HasCollider(Avatar avatar, int i);

		// Token: 0x06002025 RID: 8229
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetMuscleDefaultMin(int i);

		// Token: 0x06002026 RID: 8230
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetMuscleDefaultMax(int i);

		// Token: 0x06002027 RID: 8231
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetParentBone(int i);
	}
}
