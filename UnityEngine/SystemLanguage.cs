﻿using System;

namespace UnityEngine
{
	// Token: 0x020002C9 RID: 713
	public enum SystemLanguage
	{
		// Token: 0x04000A7A RID: 2682
		Afrikaans,
		// Token: 0x04000A7B RID: 2683
		Arabic,
		// Token: 0x04000A7C RID: 2684
		Basque,
		// Token: 0x04000A7D RID: 2685
		Belarusian,
		// Token: 0x04000A7E RID: 2686
		Bulgarian,
		// Token: 0x04000A7F RID: 2687
		Catalan,
		// Token: 0x04000A80 RID: 2688
		Chinese,
		// Token: 0x04000A81 RID: 2689
		Czech,
		// Token: 0x04000A82 RID: 2690
		Danish,
		// Token: 0x04000A83 RID: 2691
		Dutch,
		// Token: 0x04000A84 RID: 2692
		English,
		// Token: 0x04000A85 RID: 2693
		Estonian,
		// Token: 0x04000A86 RID: 2694
		Faroese,
		// Token: 0x04000A87 RID: 2695
		Finnish,
		// Token: 0x04000A88 RID: 2696
		French,
		// Token: 0x04000A89 RID: 2697
		German,
		// Token: 0x04000A8A RID: 2698
		Greek,
		// Token: 0x04000A8B RID: 2699
		Hebrew,
		// Token: 0x04000A8C RID: 2700
		Icelandic = 19,
		// Token: 0x04000A8D RID: 2701
		Indonesian,
		// Token: 0x04000A8E RID: 2702
		Italian,
		// Token: 0x04000A8F RID: 2703
		Japanese,
		// Token: 0x04000A90 RID: 2704
		Korean,
		// Token: 0x04000A91 RID: 2705
		Latvian,
		// Token: 0x04000A92 RID: 2706
		Lithuanian,
		// Token: 0x04000A93 RID: 2707
		Norwegian,
		// Token: 0x04000A94 RID: 2708
		Polish,
		// Token: 0x04000A95 RID: 2709
		Portuguese,
		// Token: 0x04000A96 RID: 2710
		Romanian,
		// Token: 0x04000A97 RID: 2711
		Russian,
		// Token: 0x04000A98 RID: 2712
		SerboCroatian,
		// Token: 0x04000A99 RID: 2713
		Slovak,
		// Token: 0x04000A9A RID: 2714
		Slovenian,
		// Token: 0x04000A9B RID: 2715
		Spanish,
		// Token: 0x04000A9C RID: 2716
		Swedish,
		// Token: 0x04000A9D RID: 2717
		Thai,
		// Token: 0x04000A9E RID: 2718
		Turkish,
		// Token: 0x04000A9F RID: 2719
		Ukrainian,
		// Token: 0x04000AA0 RID: 2720
		Vietnamese,
		// Token: 0x04000AA1 RID: 2721
		ChineseSimplified,
		// Token: 0x04000AA2 RID: 2722
		ChineseTraditional,
		// Token: 0x04000AA3 RID: 2723
		Unknown,
		// Token: 0x04000AA4 RID: 2724
		Hungarian = 18
	}
}
