﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000006 RID: 6
	[UsedByNativeCode]
	public sealed class AndroidJNIHelper
	{
		// Token: 0x0600002E RID: 46 RVA: 0x00003A2C File Offset: 0x00001C2C
		private AndroidJNIHelper()
		{
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600002F RID: 47
		// (set) Token: 0x06000030 RID: 48
		public static extern bool debug { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000031 RID: 49 RVA: 0x00003A38 File Offset: 0x00001C38
		[ExcludeFromDocs]
		public static IntPtr GetConstructorID(IntPtr javaClass)
		{
			string signature = "";
			return AndroidJNIHelper.GetConstructorID(javaClass, signature);
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00003A5C File Offset: 0x00001C5C
		public static IntPtr GetConstructorID(IntPtr javaClass, [DefaultValue("\"\"")] string signature)
		{
			return _AndroidJNIHelper.GetConstructorID(javaClass, signature);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00003A78 File Offset: 0x00001C78
		[ExcludeFromDocs]
		public static IntPtr GetMethodID(IntPtr javaClass, string methodName, string signature)
		{
			bool isStatic = false;
			return AndroidJNIHelper.GetMethodID(javaClass, methodName, signature, isStatic);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00003A98 File Offset: 0x00001C98
		[ExcludeFromDocs]
		public static IntPtr GetMethodID(IntPtr javaClass, string methodName)
		{
			bool isStatic = false;
			string signature = "";
			return AndroidJNIHelper.GetMethodID(javaClass, methodName, signature, isStatic);
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00003AC0 File Offset: 0x00001CC0
		public static IntPtr GetMethodID(IntPtr javaClass, string methodName, [DefaultValue("\"\"")] string signature, [DefaultValue("false")] bool isStatic)
		{
			return _AndroidJNIHelper.GetMethodID(javaClass, methodName, signature, isStatic);
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00003AE0 File Offset: 0x00001CE0
		[ExcludeFromDocs]
		public static IntPtr GetFieldID(IntPtr javaClass, string fieldName, string signature)
		{
			bool isStatic = false;
			return AndroidJNIHelper.GetFieldID(javaClass, fieldName, signature, isStatic);
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00003B00 File Offset: 0x00001D00
		[ExcludeFromDocs]
		public static IntPtr GetFieldID(IntPtr javaClass, string fieldName)
		{
			bool isStatic = false;
			string signature = "";
			return AndroidJNIHelper.GetFieldID(javaClass, fieldName, signature, isStatic);
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00003B28 File Offset: 0x00001D28
		public static IntPtr GetFieldID(IntPtr javaClass, string fieldName, [DefaultValue("\"\"")] string signature, [DefaultValue("false")] bool isStatic)
		{
			return _AndroidJNIHelper.GetFieldID(javaClass, fieldName, signature, isStatic);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00003B48 File Offset: 0x00001D48
		public static IntPtr CreateJavaRunnable(AndroidJavaRunnable jrunnable)
		{
			return _AndroidJNIHelper.CreateJavaRunnable(jrunnable);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00003B64 File Offset: 0x00001D64
		[ThreadAndSerializationSafe]
		public static IntPtr CreateJavaProxy(AndroidJavaProxy proxy)
		{
			IntPtr result;
			AndroidJNIHelper.INTERNAL_CALL_CreateJavaProxy(proxy, out result);
			return result;
		}

		// Token: 0x0600003B RID: 59
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_CreateJavaProxy(AndroidJavaProxy proxy, out IntPtr value);

		// Token: 0x0600003C RID: 60 RVA: 0x00003B84 File Offset: 0x00001D84
		public static IntPtr ConvertToJNIArray(Array array)
		{
			return _AndroidJNIHelper.ConvertToJNIArray(array);
		}

		// Token: 0x0600003D RID: 61 RVA: 0x00003BA0 File Offset: 0x00001DA0
		public static jvalue[] CreateJNIArgArray(object[] args)
		{
			return _AndroidJNIHelper.CreateJNIArgArray(args);
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00003BBC File Offset: 0x00001DBC
		public static void DeleteJNIArgArray(object[] args, jvalue[] jniArgs)
		{
			_AndroidJNIHelper.DeleteJNIArgArray(args, jniArgs);
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00003BC8 File Offset: 0x00001DC8
		public static IntPtr GetConstructorID(IntPtr jclass, object[] args)
		{
			return _AndroidJNIHelper.GetConstructorID(jclass, args);
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00003BE4 File Offset: 0x00001DE4
		public static IntPtr GetMethodID(IntPtr jclass, string methodName, object[] args, bool isStatic)
		{
			return _AndroidJNIHelper.GetMethodID(jclass, methodName, args, isStatic);
		}

		// Token: 0x06000041 RID: 65 RVA: 0x00003C04 File Offset: 0x00001E04
		public static string GetSignature(object obj)
		{
			return _AndroidJNIHelper.GetSignature(obj);
		}

		// Token: 0x06000042 RID: 66 RVA: 0x00003C20 File Offset: 0x00001E20
		public static string GetSignature(object[] args)
		{
			return _AndroidJNIHelper.GetSignature(args);
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00003C3C File Offset: 0x00001E3C
		public static ArrayType ConvertFromJNIArray<ArrayType>(IntPtr array)
		{
			return _AndroidJNIHelper.ConvertFromJNIArray<ArrayType>(array);
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00003C58 File Offset: 0x00001E58
		public static IntPtr GetMethodID<ReturnType>(IntPtr jclass, string methodName, object[] args, bool isStatic)
		{
			return _AndroidJNIHelper.GetMethodID<ReturnType>(jclass, methodName, args, isStatic);
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00003C78 File Offset: 0x00001E78
		public static IntPtr GetFieldID<FieldType>(IntPtr jclass, string fieldName, bool isStatic)
		{
			return _AndroidJNIHelper.GetFieldID<FieldType>(jclass, fieldName, isStatic);
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00003C98 File Offset: 0x00001E98
		public static string GetSignature<ReturnType>(object[] args)
		{
			return _AndroidJNIHelper.GetSignature<ReturnType>(args);
		}
	}
}
