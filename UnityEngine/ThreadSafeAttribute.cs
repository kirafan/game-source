﻿using System;

namespace UnityEngine
{
	// Token: 0x020002D8 RID: 728
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = false)]
	public class ThreadSafeAttribute : Attribute
	{
	}
}
