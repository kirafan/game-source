﻿using System;

namespace UnityEngine
{
	// Token: 0x0200014B RID: 331
	public enum RotationDriveMode
	{
		// Token: 0x040003B4 RID: 948
		XYAndZ,
		// Token: 0x040003B5 RID: 949
		Slerp
	}
}
