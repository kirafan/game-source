﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000B4 RID: 180
	public sealed class Shader : Object
	{
		// Token: 0x06000C51 RID: 3153
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Shader Find(string name);

		// Token: 0x06000C52 RID: 3154
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Shader FindBuiltin(string name);

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x06000C53 RID: 3155
		public extern bool isSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000C54 RID: 3156
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void EnableKeyword(string keyword);

		// Token: 0x06000C55 RID: 3157
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DisableKeyword(string keyword);

		// Token: 0x06000C56 RID: 3158
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsKeywordEnabled(string keyword);

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06000C57 RID: 3159
		// (set) Token: 0x06000C58 RID: 3160
		public extern int maximumLOD { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x06000C59 RID: 3161
		// (set) Token: 0x06000C5A RID: 3162
		public static extern int globalMaximumLOD { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x06000C5B RID: 3163
		public extern int renderQueue { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x06000C5C RID: 3164
		internal extern DisableBatchingType disableBatching { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000C5D RID: 3165 RVA: 0x000110E8 File Offset: 0x0000F2E8
		public static void SetGlobalVector(string propertyName, Vector4 vec)
		{
			Shader.SetGlobalVector(Shader.PropertyToID(propertyName), vec);
		}

		// Token: 0x06000C5E RID: 3166 RVA: 0x000110F8 File Offset: 0x0000F2F8
		public static void SetGlobalVector(int nameID, Vector4 vec)
		{
			Shader.INTERNAL_CALL_SetGlobalVector(nameID, ref vec);
		}

		// Token: 0x06000C5F RID: 3167
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetGlobalVector(int nameID, ref Vector4 vec);

		// Token: 0x06000C60 RID: 3168 RVA: 0x00011104 File Offset: 0x0000F304
		public static void SetGlobalColor(string propertyName, Color color)
		{
			Shader.SetGlobalColor(Shader.PropertyToID(propertyName), color);
		}

		// Token: 0x06000C61 RID: 3169 RVA: 0x00011114 File Offset: 0x0000F314
		public static void SetGlobalColor(int nameID, Color color)
		{
			Shader.SetGlobalVector(nameID, color);
		}

		// Token: 0x06000C62 RID: 3170 RVA: 0x00011124 File Offset: 0x0000F324
		public static void SetGlobalFloat(string propertyName, float value)
		{
			Shader.SetGlobalFloat(Shader.PropertyToID(propertyName), value);
		}

		// Token: 0x06000C63 RID: 3171
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetGlobalFloat(int nameID, float value);

		// Token: 0x06000C64 RID: 3172 RVA: 0x00011134 File Offset: 0x0000F334
		public static void SetGlobalInt(string propertyName, int value)
		{
			Shader.SetGlobalFloat(propertyName, (float)value);
		}

		// Token: 0x06000C65 RID: 3173 RVA: 0x00011140 File Offset: 0x0000F340
		public static void SetGlobalInt(int nameID, int value)
		{
			Shader.SetGlobalFloat(nameID, (float)value);
		}

		// Token: 0x06000C66 RID: 3174 RVA: 0x0001114C File Offset: 0x0000F34C
		public static void SetGlobalMatrix(string propertyName, Matrix4x4 mat)
		{
			Shader.SetGlobalMatrix(Shader.PropertyToID(propertyName), mat);
		}

		// Token: 0x06000C67 RID: 3175 RVA: 0x0001115C File Offset: 0x0000F35C
		public static void SetGlobalMatrix(int nameID, Matrix4x4 mat)
		{
			Shader.INTERNAL_CALL_SetGlobalMatrix(nameID, ref mat);
		}

		// Token: 0x06000C68 RID: 3176
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetGlobalMatrix(int nameID, ref Matrix4x4 mat);

		// Token: 0x06000C69 RID: 3177 RVA: 0x00011168 File Offset: 0x0000F368
		public static void SetGlobalTexture(string propertyName, Texture tex)
		{
			Shader.SetGlobalTexture(Shader.PropertyToID(propertyName), tex);
		}

		// Token: 0x06000C6A RID: 3178
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetGlobalTexture(int nameID, Texture tex);

		// Token: 0x06000C6B RID: 3179 RVA: 0x00011178 File Offset: 0x0000F378
		public static void SetGlobalFloatArray(string name, List<float> values)
		{
			Shader.SetGlobalFloatArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000C6C RID: 3180 RVA: 0x00011188 File Offset: 0x0000F388
		public static void SetGlobalFloatArray(int nameID, List<float> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			Shader.SetGlobalFloatArrayImplList(nameID, values);
		}

		// Token: 0x06000C6D RID: 3181
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetGlobalFloatArrayImplList(int nameID, object values);

		// Token: 0x06000C6E RID: 3182 RVA: 0x000111BC File Offset: 0x0000F3BC
		public static void SetGlobalFloatArray(string propertyName, float[] values)
		{
			Shader.SetGlobalFloatArray(Shader.PropertyToID(propertyName), values);
		}

		// Token: 0x06000C6F RID: 3183 RVA: 0x000111CC File Offset: 0x0000F3CC
		public static void SetGlobalFloatArray(int nameID, float[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			Shader.SetGlobalFloatArrayImpl(nameID, values);
		}

		// Token: 0x06000C70 RID: 3184
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetGlobalFloatArrayImpl(int nameID, float[] values);

		// Token: 0x06000C71 RID: 3185 RVA: 0x000111FC File Offset: 0x0000F3FC
		public static void SetGlobalVectorArray(string name, List<Vector4> values)
		{
			Shader.SetGlobalVectorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000C72 RID: 3186 RVA: 0x0001120C File Offset: 0x0000F40C
		public static void SetGlobalVectorArray(int nameID, List<Vector4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			Shader.SetGlobalVectorArrayImplList(nameID, values);
		}

		// Token: 0x06000C73 RID: 3187
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetGlobalVectorArrayImplList(int nameID, object values);

		// Token: 0x06000C74 RID: 3188 RVA: 0x00011240 File Offset: 0x0000F440
		public static void SetGlobalVectorArray(string propertyName, Vector4[] values)
		{
			Shader.SetGlobalVectorArray(Shader.PropertyToID(propertyName), values);
		}

		// Token: 0x06000C75 RID: 3189 RVA: 0x00011250 File Offset: 0x0000F450
		public static void SetGlobalVectorArray(int nameID, Vector4[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			Shader.SetGlobalVectorArrayImpl(nameID, values);
		}

		// Token: 0x06000C76 RID: 3190
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetGlobalVectorArrayImpl(int nameID, Vector4[] values);

		// Token: 0x06000C77 RID: 3191 RVA: 0x00011280 File Offset: 0x0000F480
		public static void SetGlobalMatrixArray(string name, List<Matrix4x4> values)
		{
			Shader.SetGlobalMatrixArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000C78 RID: 3192 RVA: 0x00011290 File Offset: 0x0000F490
		public static void SetGlobalMatrixArray(int nameID, List<Matrix4x4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			Shader.SetGlobalMatrixArrayImplList(nameID, values);
		}

		// Token: 0x06000C79 RID: 3193
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetGlobalMatrixArrayImplList(int nameID, object values);

		// Token: 0x06000C7A RID: 3194 RVA: 0x000112C4 File Offset: 0x0000F4C4
		public static void SetGlobalMatrixArray(string propertyName, Matrix4x4[] values)
		{
			Shader.SetGlobalMatrixArray(Shader.PropertyToID(propertyName), values);
		}

		// Token: 0x06000C7B RID: 3195 RVA: 0x000112D4 File Offset: 0x0000F4D4
		public static void SetGlobalMatrixArray(int nameID, Matrix4x4[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			Shader.SetGlobalMatrixArrayImpl(nameID, values);
		}

		// Token: 0x06000C7C RID: 3196
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetGlobalMatrixArrayImpl(int nameID, Matrix4x4[] values);

		// Token: 0x06000C7D RID: 3197 RVA: 0x00011304 File Offset: 0x0000F504
		public static void SetGlobalBuffer(string name, ComputeBuffer buffer)
		{
			Shader.SetGlobalBuffer(Shader.PropertyToID(name), buffer);
		}

		// Token: 0x06000C7E RID: 3198
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetGlobalBuffer(int nameID, ComputeBuffer buffer);

		// Token: 0x06000C7F RID: 3199 RVA: 0x00011314 File Offset: 0x0000F514
		public static float GetGlobalFloat(string name)
		{
			return Shader.GetGlobalFloat(Shader.PropertyToID(name));
		}

		// Token: 0x06000C80 RID: 3200
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetGlobalFloat(int nameID);

		// Token: 0x06000C81 RID: 3201 RVA: 0x00011334 File Offset: 0x0000F534
		public static int GetGlobalInt(string name)
		{
			return Shader.GetGlobalInt(Shader.PropertyToID(name));
		}

		// Token: 0x06000C82 RID: 3202
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetGlobalInt(int nameID);

		// Token: 0x06000C83 RID: 3203 RVA: 0x00011354 File Offset: 0x0000F554
		public static Vector4 GetGlobalVector(string name)
		{
			return Shader.GetGlobalVector(Shader.PropertyToID(name));
		}

		// Token: 0x06000C84 RID: 3204 RVA: 0x00011374 File Offset: 0x0000F574
		public static Vector4 GetGlobalVector(int nameID)
		{
			Vector4 result;
			Shader.INTERNAL_CALL_GetGlobalVector(nameID, out result);
			return result;
		}

		// Token: 0x06000C85 RID: 3205
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetGlobalVector(int nameID, out Vector4 value);

		// Token: 0x06000C86 RID: 3206 RVA: 0x00011394 File Offset: 0x0000F594
		public static Color GetGlobalColor(string name)
		{
			return Shader.GetGlobalColor(Shader.PropertyToID(name));
		}

		// Token: 0x06000C87 RID: 3207 RVA: 0x000113B4 File Offset: 0x0000F5B4
		public static Color GetGlobalColor(int nameID)
		{
			Color result;
			Shader.INTERNAL_CALL_GetGlobalColor(nameID, out result);
			return result;
		}

		// Token: 0x06000C88 RID: 3208
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetGlobalColor(int nameID, out Color value);

		// Token: 0x06000C89 RID: 3209 RVA: 0x000113D4 File Offset: 0x0000F5D4
		public static Matrix4x4 GetGlobalMatrix(string name)
		{
			return Shader.GetGlobalMatrix(Shader.PropertyToID(name));
		}

		// Token: 0x06000C8A RID: 3210 RVA: 0x000113F4 File Offset: 0x0000F5F4
		public static Matrix4x4 GetGlobalMatrix(int nameID)
		{
			Matrix4x4 result;
			Shader.INTERNAL_CALL_GetGlobalMatrix(nameID, out result);
			return result;
		}

		// Token: 0x06000C8B RID: 3211
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetGlobalMatrix(int nameID, out Matrix4x4 value);

		// Token: 0x06000C8C RID: 3212 RVA: 0x00011414 File Offset: 0x0000F614
		public static Texture GetGlobalTexture(string name)
		{
			return Shader.GetGlobalTexture(Shader.PropertyToID(name));
		}

		// Token: 0x06000C8D RID: 3213
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Texture GetGlobalTexture(int nameID);

		// Token: 0x06000C8E RID: 3214 RVA: 0x00011434 File Offset: 0x0000F634
		public static void GetGlobalFloatArray(string name, List<float> values)
		{
			Shader.GetGlobalFloatArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000C8F RID: 3215 RVA: 0x00011444 File Offset: 0x0000F644
		public static void GetGlobalFloatArray(int nameID, List<float> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			Shader.GetGlobalFloatArrayImplList(nameID, values);
		}

		// Token: 0x06000C90 RID: 3216
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetGlobalFloatArrayImplList(int nameID, object list);

		// Token: 0x06000C91 RID: 3217 RVA: 0x00011460 File Offset: 0x0000F660
		public static float[] GetGlobalFloatArray(string name)
		{
			return Shader.GetGlobalFloatArray(Shader.PropertyToID(name));
		}

		// Token: 0x06000C92 RID: 3218 RVA: 0x00011480 File Offset: 0x0000F680
		public static float[] GetGlobalFloatArray(int nameID)
		{
			return Shader.GetGlobalFloatArrayImpl(nameID);
		}

		// Token: 0x06000C93 RID: 3219
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float[] GetGlobalFloatArrayImpl(int nameID);

		// Token: 0x06000C94 RID: 3220 RVA: 0x0001149C File Offset: 0x0000F69C
		public static void GetGlobalVectorArray(string name, List<Vector4> values)
		{
			Shader.GetGlobalVectorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000C95 RID: 3221 RVA: 0x000114AC File Offset: 0x0000F6AC
		public static void GetGlobalVectorArray(int nameID, List<Vector4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			Shader.GetGlobalVectorArrayImplList(nameID, values);
		}

		// Token: 0x06000C96 RID: 3222
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetGlobalVectorArrayImplList(int nameID, object list);

		// Token: 0x06000C97 RID: 3223 RVA: 0x000114C8 File Offset: 0x0000F6C8
		public static Vector4[] GetGlobalVectorArray(string name)
		{
			return Shader.GetGlobalVectorArray(Shader.PropertyToID(name));
		}

		// Token: 0x06000C98 RID: 3224 RVA: 0x000114E8 File Offset: 0x0000F6E8
		public static Vector4[] GetGlobalVectorArray(int nameID)
		{
			return Shader.GetGlobalVectorArrayImpl(nameID);
		}

		// Token: 0x06000C99 RID: 3225
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Vector4[] GetGlobalVectorArrayImpl(int nameID);

		// Token: 0x06000C9A RID: 3226 RVA: 0x00011504 File Offset: 0x0000F704
		public static void GetGlobalMatrixArray(string name, List<Matrix4x4> values)
		{
			Shader.GetGlobalMatrixArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000C9B RID: 3227 RVA: 0x00011514 File Offset: 0x0000F714
		public static void GetGlobalMatrixArray(int nameID, List<Matrix4x4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			Shader.GetGlobalMatrixArrayImplList(nameID, values);
		}

		// Token: 0x06000C9C RID: 3228
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetGlobalMatrixArrayImplList(int nameID, object list);

		// Token: 0x06000C9D RID: 3229 RVA: 0x00011530 File Offset: 0x0000F730
		public static Matrix4x4[] GetGlobalMatrixArray(string name)
		{
			return Shader.GetGlobalMatrixArray(Shader.PropertyToID(name));
		}

		// Token: 0x06000C9E RID: 3230 RVA: 0x00011550 File Offset: 0x0000F750
		public static Matrix4x4[] GetGlobalMatrixArray(int nameID)
		{
			return Shader.GetGlobalMatrixArrayImpl(nameID);
		}

		// Token: 0x06000C9F RID: 3231
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Matrix4x4[] GetGlobalMatrixArrayImpl(int nameID);

		// Token: 0x06000CA0 RID: 3232
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int PropertyToID(string name);

		// Token: 0x06000CA1 RID: 3233
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WarmupAllShaders();
	}
}
