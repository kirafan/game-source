﻿using System;

namespace UnityEngine
{
	// Token: 0x020003AC RID: 940
	public class Logger : ILogger, ILogHandler
	{
		// Token: 0x0600300B RID: 12299 RVA: 0x0004ECF8 File Offset: 0x0004CEF8
		private Logger()
		{
		}

		// Token: 0x0600300C RID: 12300 RVA: 0x0004ED04 File Offset: 0x0004CF04
		public Logger(ILogHandler logHandler)
		{
			this.logHandler = logHandler;
			this.logEnabled = true;
			this.filterLogType = LogType.Log;
		}

		// Token: 0x17000B38 RID: 2872
		// (get) Token: 0x0600300D RID: 12301 RVA: 0x0004ED24 File Offset: 0x0004CF24
		// (set) Token: 0x0600300E RID: 12302 RVA: 0x0004ED40 File Offset: 0x0004CF40
		public ILogHandler logHandler { get; set; }

		// Token: 0x17000B39 RID: 2873
		// (get) Token: 0x0600300F RID: 12303 RVA: 0x0004ED4C File Offset: 0x0004CF4C
		// (set) Token: 0x06003010 RID: 12304 RVA: 0x0004ED68 File Offset: 0x0004CF68
		public bool logEnabled { get; set; }

		// Token: 0x17000B3A RID: 2874
		// (get) Token: 0x06003011 RID: 12305 RVA: 0x0004ED74 File Offset: 0x0004CF74
		// (set) Token: 0x06003012 RID: 12306 RVA: 0x0004ED90 File Offset: 0x0004CF90
		public LogType filterLogType { get; set; }

		// Token: 0x06003013 RID: 12307 RVA: 0x0004ED9C File Offset: 0x0004CF9C
		public bool IsLogTypeAllowed(LogType logType)
		{
			return this.logEnabled && (logType <= this.filterLogType || logType == LogType.Exception);
		}

		// Token: 0x06003014 RID: 12308 RVA: 0x0004EDD8 File Offset: 0x0004CFD8
		private static string GetString(object message)
		{
			return (message == null) ? "Null" : message.ToString();
		}

		// Token: 0x06003015 RID: 12309 RVA: 0x0004EE04 File Offset: 0x0004D004
		public void Log(LogType logType, object message)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, null, "{0}", new object[]
				{
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x06003016 RID: 12310 RVA: 0x0004EE34 File Offset: 0x0004D034
		public void Log(LogType logType, object message, Object context)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, context, "{0}", new object[]
				{
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x06003017 RID: 12311 RVA: 0x0004EE64 File Offset: 0x0004D064
		public void Log(LogType logType, string tag, object message)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, null, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x06003018 RID: 12312 RVA: 0x0004EE98 File Offset: 0x0004D098
		public void Log(LogType logType, string tag, object message, Object context)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, context, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x06003019 RID: 12313 RVA: 0x0004EED0 File Offset: 0x0004D0D0
		public void Log(object message)
		{
			if (this.IsLogTypeAllowed(LogType.Log))
			{
				this.logHandler.LogFormat(LogType.Log, null, "{0}", new object[]
				{
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x0600301A RID: 12314 RVA: 0x0004EF00 File Offset: 0x0004D100
		public void Log(string tag, object message)
		{
			if (this.IsLogTypeAllowed(LogType.Log))
			{
				this.logHandler.LogFormat(LogType.Log, null, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x0600301B RID: 12315 RVA: 0x0004EF34 File Offset: 0x0004D134
		public void Log(string tag, object message, Object context)
		{
			if (this.IsLogTypeAllowed(LogType.Log))
			{
				this.logHandler.LogFormat(LogType.Log, context, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x0600301C RID: 12316 RVA: 0x0004EF68 File Offset: 0x0004D168
		public void LogWarning(string tag, object message)
		{
			if (this.IsLogTypeAllowed(LogType.Warning))
			{
				this.logHandler.LogFormat(LogType.Warning, null, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x0600301D RID: 12317 RVA: 0x0004EF9C File Offset: 0x0004D19C
		public void LogWarning(string tag, object message, Object context)
		{
			if (this.IsLogTypeAllowed(LogType.Warning))
			{
				this.logHandler.LogFormat(LogType.Warning, context, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x0600301E RID: 12318 RVA: 0x0004EFD0 File Offset: 0x0004D1D0
		public void LogError(string tag, object message)
		{
			if (this.IsLogTypeAllowed(LogType.Error))
			{
				this.logHandler.LogFormat(LogType.Error, null, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x0600301F RID: 12319 RVA: 0x0004F004 File Offset: 0x0004D204
		public void LogError(string tag, object message, Object context)
		{
			if (this.IsLogTypeAllowed(LogType.Error))
			{
				this.logHandler.LogFormat(LogType.Error, context, "{0}: {1}", new object[]
				{
					tag,
					Logger.GetString(message)
				});
			}
		}

		// Token: 0x06003020 RID: 12320 RVA: 0x0004F038 File Offset: 0x0004D238
		public void LogFormat(LogType logType, string format, params object[] args)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, null, format, args);
			}
		}

		// Token: 0x06003021 RID: 12321 RVA: 0x0004F058 File Offset: 0x0004D258
		public void LogException(Exception exception)
		{
			if (this.logEnabled)
			{
				this.logHandler.LogException(exception, null);
			}
		}

		// Token: 0x06003022 RID: 12322 RVA: 0x0004F074 File Offset: 0x0004D274
		public void LogFormat(LogType logType, Object context, string format, params object[] args)
		{
			if (this.IsLogTypeAllowed(logType))
			{
				this.logHandler.LogFormat(logType, context, format, args);
			}
		}

		// Token: 0x06003023 RID: 12323 RVA: 0x0004F094 File Offset: 0x0004D294
		public void LogException(Exception exception, Object context)
		{
			if (this.logEnabled)
			{
				this.logHandler.LogException(exception, context);
			}
		}

		// Token: 0x04000DBB RID: 3515
		private const string kNoTagFormat = "{0}";

		// Token: 0x04000DBC RID: 3516
		private const string kTagFormat = "{0}: {1}";
	}
}
