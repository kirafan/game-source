﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;

namespace UnityEngine
{
	// Token: 0x0200002A RID: 42
	public sealed class ComputeBuffer : IDisposable
	{
		// Token: 0x0600034C RID: 844 RVA: 0x00005E24 File Offset: 0x00004024
		public ComputeBuffer(int count, int stride) : this(count, stride, ComputeBufferType.Default)
		{
		}

		// Token: 0x0600034D RID: 845 RVA: 0x00005E30 File Offset: 0x00004030
		public ComputeBuffer(int count, int stride, ComputeBufferType type)
		{
			if (count <= 0)
			{
				throw new ArgumentException("Attempting to create a zero length compute buffer", "count");
			}
			if (stride < 0)
			{
				throw new ArgumentException("Attempting to create a compute buffer with a negative stride", "stride");
			}
			this.m_Ptr = IntPtr.Zero;
			ComputeBuffer.InitBuffer(this, count, stride, type);
		}

		// Token: 0x0600034E RID: 846 RVA: 0x00005E88 File Offset: 0x00004088
		~ComputeBuffer()
		{
			this.Dispose(false);
		}

		// Token: 0x0600034F RID: 847 RVA: 0x00005EBC File Offset: 0x000040BC
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000350 RID: 848 RVA: 0x00005ECC File Offset: 0x000040CC
		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				ComputeBuffer.DestroyBuffer(this);
			}
			else if (this.m_Ptr != IntPtr.Zero)
			{
				Debug.LogWarning("GarbageCollector disposing of ComputeBuffer. Please use ComputeBuffer.Release() or .Dispose() to manually release the buffer.");
			}
			this.m_Ptr = IntPtr.Zero;
		}

		// Token: 0x06000351 RID: 849
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InitBuffer(ComputeBuffer buf, int count, int stride, ComputeBufferType type);

		// Token: 0x06000352 RID: 850
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void DestroyBuffer(ComputeBuffer buf);

		// Token: 0x06000353 RID: 851 RVA: 0x00005F1C File Offset: 0x0000411C
		public void Release()
		{
			this.Dispose();
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x06000354 RID: 852
		public extern int count { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x06000355 RID: 853
		public extern int stride { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000356 RID: 854 RVA: 0x00005F28 File Offset: 0x00004128
		[SecuritySafeCritical]
		public void SetData(Array data)
		{
			this.InternalSetData(data, Marshal.SizeOf(data.GetType().GetElementType()));
		}

		// Token: 0x06000357 RID: 855
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InternalSetData(Array data, int elemSize);

		// Token: 0x06000358 RID: 856
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetCounterValue(uint counterValue);

		// Token: 0x06000359 RID: 857 RVA: 0x00005F44 File Offset: 0x00004144
		[SecuritySafeCritical]
		public void GetData(Array data)
		{
			this.InternalGetData(data, Marshal.SizeOf(data.GetType().GetElementType()));
		}

		// Token: 0x0600035A RID: 858
		[SecurityCritical]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InternalGetData(Array data, int elemSize);

		// Token: 0x0600035B RID: 859
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void CopyCount(ComputeBuffer src, ComputeBuffer dst, int dstOffset);

		// Token: 0x0600035C RID: 860 RVA: 0x00005F60 File Offset: 0x00004160
		public IntPtr GetNativeBufferPtr()
		{
			IntPtr result;
			ComputeBuffer.INTERNAL_CALL_GetNativeBufferPtr(this, out result);
			return result;
		}

		// Token: 0x0600035D RID: 861
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetNativeBufferPtr(ComputeBuffer self, out IntPtr value);

		// Token: 0x04000039 RID: 57
		internal IntPtr m_Ptr;
	}
}
