﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Sprites
{
	// Token: 0x020000C1 RID: 193
	public sealed class DataUtility
	{
		// Token: 0x06000D79 RID: 3449 RVA: 0x00013044 File Offset: 0x00011244
		public static Vector4 GetInnerUV(Sprite sprite)
		{
			Vector4 result;
			DataUtility.INTERNAL_CALL_GetInnerUV(sprite, out result);
			return result;
		}

		// Token: 0x06000D7A RID: 3450
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetInnerUV(Sprite sprite, out Vector4 value);

		// Token: 0x06000D7B RID: 3451 RVA: 0x00013064 File Offset: 0x00011264
		public static Vector4 GetOuterUV(Sprite sprite)
		{
			Vector4 result;
			DataUtility.INTERNAL_CALL_GetOuterUV(sprite, out result);
			return result;
		}

		// Token: 0x06000D7C RID: 3452
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetOuterUV(Sprite sprite, out Vector4 value);

		// Token: 0x06000D7D RID: 3453 RVA: 0x00013084 File Offset: 0x00011284
		public static Vector4 GetPadding(Sprite sprite)
		{
			Vector4 result;
			DataUtility.INTERNAL_CALL_GetPadding(sprite, out result);
			return result;
		}

		// Token: 0x06000D7E RID: 3454
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPadding(Sprite sprite, out Vector4 value);

		// Token: 0x06000D7F RID: 3455 RVA: 0x000130A4 File Offset: 0x000112A4
		public static Vector2 GetMinSize(Sprite sprite)
		{
			Vector2 result;
			DataUtility.Internal_GetMinSize(sprite, out result);
			return result;
		}

		// Token: 0x06000D80 RID: 3456
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_GetMinSize(Sprite sprite, out Vector2 output);
	}
}
