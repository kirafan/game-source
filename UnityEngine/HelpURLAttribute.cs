﻿using System;

namespace UnityEngine
{
	// Token: 0x020002C1 RID: 705
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public sealed class HelpURLAttribute : Attribute
	{
		// Token: 0x06002C45 RID: 11333 RVA: 0x00045194 File Offset: 0x00043394
		public HelpURLAttribute(string url)
		{
			this.URL = url;
		}

		// Token: 0x17000A6E RID: 2670
		// (get) Token: 0x06002C46 RID: 11334 RVA: 0x000451A4 File Offset: 0x000433A4
		// (set) Token: 0x06002C47 RID: 11335 RVA: 0x000451C0 File Offset: 0x000433C0
		public string URL { get; private set; }
	}
}
