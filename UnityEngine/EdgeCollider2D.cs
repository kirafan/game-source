﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000164 RID: 356
	public sealed class EdgeCollider2D : Collider2D
	{
		// Token: 0x06001A7C RID: 6780
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Reset();

		// Token: 0x17000616 RID: 1558
		// (get) Token: 0x06001A7D RID: 6781
		public extern int edgeCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000617 RID: 1559
		// (get) Token: 0x06001A7E RID: 6782
		public extern int pointCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000618 RID: 1560
		// (get) Token: 0x06001A7F RID: 6783
		// (set) Token: 0x06001A80 RID: 6784
		public extern Vector2[] points { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
