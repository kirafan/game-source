﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001FD RID: 509
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class TextGenerator : IDisposable
	{
		// Token: 0x06002284 RID: 8836 RVA: 0x00027E78 File Offset: 0x00026078
		public TextGenerator() : this(50)
		{
		}

		// Token: 0x06002285 RID: 8837 RVA: 0x00027E84 File Offset: 0x00026084
		public TextGenerator(int initialCapacity)
		{
			this.m_Verts = new List<UIVertex>((initialCapacity + 1) * 4);
			this.m_Characters = new List<UICharInfo>(initialCapacity + 1);
			this.m_Lines = new List<UILineInfo>(20);
			this.Init();
		}

		// Token: 0x06002286 RID: 8838 RVA: 0x00027EC0 File Offset: 0x000260C0
		~TextGenerator()
		{
			((IDisposable)this).Dispose();
		}

		// Token: 0x06002287 RID: 8839 RVA: 0x00027EF0 File Offset: 0x000260F0
		void IDisposable.Dispose()
		{
			this.Dispose_cpp();
		}

		// Token: 0x06002288 RID: 8840 RVA: 0x00027EFC File Offset: 0x000260FC
		private TextGenerationSettings ValidatedSettings(TextGenerationSettings settings)
		{
			TextGenerationSettings result;
			if (settings.font != null && settings.font.dynamic)
			{
				result = settings;
			}
			else
			{
				if (settings.fontSize != 0 || settings.fontStyle != FontStyle.Normal)
				{
					if (settings.font != null)
					{
						Debug.LogWarningFormat(settings.font, "Font size and style overrides are only supported for dynamic fonts. Font '{0}' is not dynamic.", new object[]
						{
							settings.font.name
						});
					}
					settings.fontSize = 0;
					settings.fontStyle = FontStyle.Normal;
				}
				if (settings.resizeTextForBestFit)
				{
					if (settings.font != null)
					{
						Debug.LogWarningFormat(settings.font, "BestFit is only supported for dynamic fonts. Font '{0}' is not dynamic.", new object[]
						{
							settings.font.name
						});
					}
					settings.resizeTextForBestFit = false;
				}
				result = settings;
			}
			return result;
		}

		// Token: 0x06002289 RID: 8841 RVA: 0x00027FEC File Offset: 0x000261EC
		public void Invalidate()
		{
			this.m_HasGenerated = false;
		}

		// Token: 0x0600228A RID: 8842 RVA: 0x00027FF8 File Offset: 0x000261F8
		public void GetCharacters(List<UICharInfo> characters)
		{
			this.GetCharactersInternal(characters);
		}

		// Token: 0x0600228B RID: 8843 RVA: 0x00028004 File Offset: 0x00026204
		public void GetLines(List<UILineInfo> lines)
		{
			this.GetLinesInternal(lines);
		}

		// Token: 0x0600228C RID: 8844 RVA: 0x00028010 File Offset: 0x00026210
		public void GetVertices(List<UIVertex> vertices)
		{
			this.GetVerticesInternal(vertices);
		}

		// Token: 0x0600228D RID: 8845 RVA: 0x0002801C File Offset: 0x0002621C
		public float GetPreferredWidth(string str, TextGenerationSettings settings)
		{
			settings.horizontalOverflow = HorizontalWrapMode.Overflow;
			settings.verticalOverflow = VerticalWrapMode.Overflow;
			settings.updateBounds = true;
			this.Populate(str, settings);
			return this.rectExtents.width;
		}

		// Token: 0x0600228E RID: 8846 RVA: 0x00028060 File Offset: 0x00026260
		public float GetPreferredHeight(string str, TextGenerationSettings settings)
		{
			settings.verticalOverflow = VerticalWrapMode.Overflow;
			settings.updateBounds = true;
			this.Populate(str, settings);
			return this.rectExtents.height;
		}

		// Token: 0x0600228F RID: 8847 RVA: 0x0002809C File Offset: 0x0002629C
		public bool PopulateWithErrors(string str, TextGenerationSettings settings, GameObject context)
		{
			TextGenerationError textGenerationError = this.PopulateWithError(str, settings);
			bool result;
			if (textGenerationError == TextGenerationError.None)
			{
				result = true;
			}
			else
			{
				if ((textGenerationError & TextGenerationError.CustomSizeOnNonDynamicFont) != TextGenerationError.None)
				{
					Debug.LogErrorFormat(context, "Font '{0}' is not dynamic, which is required to override its size", new object[]
					{
						settings.font
					});
				}
				if ((textGenerationError & TextGenerationError.CustomStyleOnNonDynamicFont) != TextGenerationError.None)
				{
					Debug.LogErrorFormat(context, "Font '{0}' is not dynamic, which is required to override its style", new object[]
					{
						settings.font
					});
				}
				result = false;
			}
			return result;
		}

		// Token: 0x06002290 RID: 8848 RVA: 0x00028110 File Offset: 0x00026310
		public bool Populate(string str, TextGenerationSettings settings)
		{
			TextGenerationError textGenerationError = this.PopulateWithError(str, settings);
			return textGenerationError == TextGenerationError.None;
		}

		// Token: 0x06002291 RID: 8849 RVA: 0x00028134 File Offset: 0x00026334
		private TextGenerationError PopulateWithError(string str, TextGenerationSettings settings)
		{
			TextGenerationError lastValid;
			if (this.m_HasGenerated && str == this.m_LastString && settings.Equals(this.m_LastSettings))
			{
				lastValid = this.m_LastValid;
			}
			else
			{
				this.m_LastValid = this.PopulateAlways(str, settings);
				lastValid = this.m_LastValid;
			}
			return lastValid;
		}

		// Token: 0x06002292 RID: 8850 RVA: 0x00028198 File Offset: 0x00026398
		private TextGenerationError PopulateAlways(string str, TextGenerationSettings settings)
		{
			this.m_LastString = str;
			this.m_HasGenerated = true;
			this.m_CachedVerts = false;
			this.m_CachedCharacters = false;
			this.m_CachedLines = false;
			this.m_LastSettings = settings;
			TextGenerationSettings textGenerationSettings = this.ValidatedSettings(settings);
			TextGenerationError textGenerationError;
			this.Populate_Internal(str, textGenerationSettings.font, textGenerationSettings.color, textGenerationSettings.fontSize, textGenerationSettings.scaleFactor, textGenerationSettings.lineSpacing, textGenerationSettings.fontStyle, textGenerationSettings.richText, textGenerationSettings.resizeTextForBestFit, textGenerationSettings.resizeTextMinSize, textGenerationSettings.resizeTextMaxSize, textGenerationSettings.verticalOverflow, textGenerationSettings.horizontalOverflow, textGenerationSettings.updateBounds, textGenerationSettings.textAnchor, textGenerationSettings.generationExtents, textGenerationSettings.pivot, textGenerationSettings.generateOutOfBounds, textGenerationSettings.alignByGeometry, out textGenerationError);
			this.m_LastValid = textGenerationError;
			return textGenerationError;
		}

		// Token: 0x1700088F RID: 2191
		// (get) Token: 0x06002293 RID: 8851 RVA: 0x00028270 File Offset: 0x00026470
		public IList<UIVertex> verts
		{
			get
			{
				if (!this.m_CachedVerts)
				{
					this.GetVertices(this.m_Verts);
					this.m_CachedVerts = true;
				}
				return this.m_Verts;
			}
		}

		// Token: 0x17000890 RID: 2192
		// (get) Token: 0x06002294 RID: 8852 RVA: 0x000282AC File Offset: 0x000264AC
		public IList<UICharInfo> characters
		{
			get
			{
				if (!this.m_CachedCharacters)
				{
					this.GetCharacters(this.m_Characters);
					this.m_CachedCharacters = true;
				}
				return this.m_Characters;
			}
		}

		// Token: 0x17000891 RID: 2193
		// (get) Token: 0x06002295 RID: 8853 RVA: 0x000282E8 File Offset: 0x000264E8
		public IList<UILineInfo> lines
		{
			get
			{
				if (!this.m_CachedLines)
				{
					this.GetLines(this.m_Lines);
					this.m_CachedLines = true;
				}
				return this.m_Lines;
			}
		}

		// Token: 0x06002296 RID: 8854
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Init();

		// Token: 0x06002297 RID: 8855
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Dispose_cpp();

		// Token: 0x06002298 RID: 8856 RVA: 0x00028324 File Offset: 0x00026524
		internal bool Populate_Internal(string str, Font font, Color color, int fontSize, float scaleFactor, float lineSpacing, FontStyle style, bool richText, bool resizeTextForBestFit, int resizeTextMinSize, int resizeTextMaxSize, VerticalWrapMode verticalOverFlow, HorizontalWrapMode horizontalOverflow, bool updateBounds, TextAnchor anchor, Vector2 extents, Vector2 pivot, bool generateOutOfBounds, bool alignByGeometry, out TextGenerationError error)
		{
			uint num = 0U;
			bool result;
			if (font == null)
			{
				error = TextGenerationError.NoFont;
				result = false;
			}
			else
			{
				bool flag = this.Populate_Internal_cpp(str, font, color, fontSize, scaleFactor, lineSpacing, style, richText, resizeTextForBestFit, resizeTextMinSize, resizeTextMaxSize, (int)verticalOverFlow, (int)horizontalOverflow, updateBounds, anchor, extents.x, extents.y, pivot.x, pivot.y, generateOutOfBounds, alignByGeometry, out num);
				error = (TextGenerationError)num;
				result = flag;
			}
			return result;
		}

		// Token: 0x06002299 RID: 8857 RVA: 0x000283A0 File Offset: 0x000265A0
		internal bool Populate_Internal_cpp(string str, Font font, Color color, int fontSize, float scaleFactor, float lineSpacing, FontStyle style, bool richText, bool resizeTextForBestFit, int resizeTextMinSize, int resizeTextMaxSize, int verticalOverFlow, int horizontalOverflow, bool updateBounds, TextAnchor anchor, float extentsX, float extentsY, float pivotX, float pivotY, bool generateOutOfBounds, bool alignByGeometry, out uint error)
		{
			return TextGenerator.INTERNAL_CALL_Populate_Internal_cpp(this, str, font, ref color, fontSize, scaleFactor, lineSpacing, style, richText, resizeTextForBestFit, resizeTextMinSize, resizeTextMaxSize, verticalOverFlow, horizontalOverflow, updateBounds, anchor, extentsX, extentsY, pivotX, pivotY, generateOutOfBounds, alignByGeometry, out error);
		}

		// Token: 0x0600229A RID: 8858
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Populate_Internal_cpp(TextGenerator self, string str, Font font, ref Color color, int fontSize, float scaleFactor, float lineSpacing, FontStyle style, bool richText, bool resizeTextForBestFit, int resizeTextMinSize, int resizeTextMaxSize, int verticalOverFlow, int horizontalOverflow, bool updateBounds, TextAnchor anchor, float extentsX, float extentsY, float pivotX, float pivotY, bool generateOutOfBounds, bool alignByGeometry, out uint error);

		// Token: 0x17000892 RID: 2194
		// (get) Token: 0x0600229B RID: 8859 RVA: 0x000283E8 File Offset: 0x000265E8
		public Rect rectExtents
		{
			get
			{
				Rect result;
				this.INTERNAL_get_rectExtents(out result);
				return result;
			}
		}

		// Token: 0x0600229C RID: 8860
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rectExtents(out Rect value);

		// Token: 0x17000893 RID: 2195
		// (get) Token: 0x0600229D RID: 8861
		public extern int vertexCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600229E RID: 8862
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVerticesInternal(object vertices);

		// Token: 0x0600229F RID: 8863
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern UIVertex[] GetVerticesArray();

		// Token: 0x17000894 RID: 2196
		// (get) Token: 0x060022A0 RID: 8864
		public extern int characterCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000895 RID: 2197
		// (get) Token: 0x060022A1 RID: 8865 RVA: 0x00028408 File Offset: 0x00026608
		public int characterCountVisible
		{
			get
			{
				return this.characterCount - 1;
			}
		}

		// Token: 0x060022A2 RID: 8866
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetCharactersInternal(object characters);

		// Token: 0x060022A3 RID: 8867
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern UICharInfo[] GetCharactersArray();

		// Token: 0x17000896 RID: 2198
		// (get) Token: 0x060022A4 RID: 8868
		public extern int lineCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060022A5 RID: 8869
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetLinesInternal(object lines);

		// Token: 0x060022A6 RID: 8870
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern UILineInfo[] GetLinesArray();

		// Token: 0x17000897 RID: 2199
		// (get) Token: 0x060022A7 RID: 8871
		public extern int fontSizeUsedForBestFit { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x04000606 RID: 1542
		internal IntPtr m_Ptr;

		// Token: 0x04000607 RID: 1543
		private string m_LastString;

		// Token: 0x04000608 RID: 1544
		private TextGenerationSettings m_LastSettings;

		// Token: 0x04000609 RID: 1545
		private bool m_HasGenerated;

		// Token: 0x0400060A RID: 1546
		private TextGenerationError m_LastValid;

		// Token: 0x0400060B RID: 1547
		private readonly List<UIVertex> m_Verts;

		// Token: 0x0400060C RID: 1548
		private readonly List<UICharInfo> m_Characters;

		// Token: 0x0400060D RID: 1549
		private readonly List<UILineInfo> m_Lines;

		// Token: 0x0400060E RID: 1550
		private bool m_CachedVerts;

		// Token: 0x0400060F RID: 1551
		private bool m_CachedCharacters;

		// Token: 0x04000610 RID: 1552
		private bool m_CachedLines;
	}
}
