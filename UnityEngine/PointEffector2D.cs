﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000182 RID: 386
	public sealed class PointEffector2D : Effector2D
	{
		// Token: 0x17000682 RID: 1666
		// (get) Token: 0x06001B7C RID: 7036
		// (set) Token: 0x06001B7D RID: 7037
		public extern float forceMagnitude { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000683 RID: 1667
		// (get) Token: 0x06001B7E RID: 7038
		// (set) Token: 0x06001B7F RID: 7039
		public extern float forceVariation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000684 RID: 1668
		// (get) Token: 0x06001B80 RID: 7040
		// (set) Token: 0x06001B81 RID: 7041
		public extern float distanceScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000685 RID: 1669
		// (get) Token: 0x06001B82 RID: 7042
		// (set) Token: 0x06001B83 RID: 7043
		public extern float drag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000686 RID: 1670
		// (get) Token: 0x06001B84 RID: 7044
		// (set) Token: 0x06001B85 RID: 7045
		public extern float angularDrag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000687 RID: 1671
		// (get) Token: 0x06001B86 RID: 7046
		// (set) Token: 0x06001B87 RID: 7047
		public extern EffectorSelection2D forceSource { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000688 RID: 1672
		// (get) Token: 0x06001B88 RID: 7048
		// (set) Token: 0x06001B89 RID: 7049
		public extern EffectorSelection2D forceTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000689 RID: 1673
		// (get) Token: 0x06001B8A RID: 7050
		// (set) Token: 0x06001B8B RID: 7051
		public extern EffectorForceMode2D forceMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
