﻿using System;

namespace UnityEngine
{
	// Token: 0x02000134 RID: 308
	public enum JointProjectionMode
	{
		// Token: 0x04000363 RID: 867
		None,
		// Token: 0x04000364 RID: 868
		PositionAndRotation,
		// Token: 0x04000365 RID: 869
		[Obsolete("JointProjectionMode.PositionOnly is no longer supported", true)]
		PositionOnly
	}
}
