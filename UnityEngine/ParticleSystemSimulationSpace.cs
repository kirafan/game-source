﻿using System;

namespace UnityEngine
{
	// Token: 0x02000104 RID: 260
	public enum ParticleSystemSimulationSpace
	{
		// Token: 0x040002CC RID: 716
		Local,
		// Token: 0x040002CD RID: 717
		World,
		// Token: 0x040002CE RID: 718
		Custom
	}
}
