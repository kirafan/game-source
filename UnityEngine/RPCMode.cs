﻿using System;

namespace UnityEngine
{
	// Token: 0x0200008F RID: 143
	public enum RPCMode
	{
		// Token: 0x0400012C RID: 300
		Server,
		// Token: 0x0400012D RID: 301
		Others,
		// Token: 0x0400012E RID: 302
		OthersBuffered = 5,
		// Token: 0x0400012F RID: 303
		All = 2,
		// Token: 0x04000130 RID: 304
		AllBuffered = 6
	}
}
