﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200012E RID: 302
	[RequiredByNativeCode(Optional = true)]
	public struct ParticleCollisionEvent
	{
		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x060015DF RID: 5599 RVA: 0x0001BCC8 File Offset: 0x00019EC8
		public Vector3 intersection
		{
			get
			{
				return this.m_Intersection;
			}
		}

		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x060015E0 RID: 5600 RVA: 0x0001BCE4 File Offset: 0x00019EE4
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
		}

		// Token: 0x170004F4 RID: 1268
		// (get) Token: 0x060015E1 RID: 5601 RVA: 0x0001BD00 File Offset: 0x00019F00
		public Vector3 velocity
		{
			get
			{
				return this.m_Velocity;
			}
		}

		// Token: 0x170004F5 RID: 1269
		// (get) Token: 0x060015E2 RID: 5602 RVA: 0x0001BD1C File Offset: 0x00019F1C
		[Obsolete("collider property is deprecated. Use colliderComponent instead, which supports Collider and Collider2D components.")]
		public Collider collider
		{
			get
			{
				return ParticleCollisionEvent.InstanceIDToCollider(this.m_ColliderInstanceID);
			}
		}

		// Token: 0x170004F6 RID: 1270
		// (get) Token: 0x060015E3 RID: 5603 RVA: 0x0001BD3C File Offset: 0x00019F3C
		public Component colliderComponent
		{
			get
			{
				return ParticleCollisionEvent.InstanceIDToColliderComponent(this.m_ColliderInstanceID);
			}
		}

		// Token: 0x060015E4 RID: 5604
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider InstanceIDToCollider(int instanceID);

		// Token: 0x060015E5 RID: 5605
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Component InstanceIDToColliderComponent(int instanceID);

		// Token: 0x04000349 RID: 841
		private Vector3 m_Intersection;

		// Token: 0x0400034A RID: 842
		private Vector3 m_Normal;

		// Token: 0x0400034B RID: 843
		private Vector3 m_Velocity;

		// Token: 0x0400034C RID: 844
		private int m_ColliderInstanceID;
	}
}
