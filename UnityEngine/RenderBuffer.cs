﻿using System;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x020002E1 RID: 737
	public struct RenderBuffer
	{
		// Token: 0x06002C9C RID: 11420 RVA: 0x00046590 File Offset: 0x00044790
		internal void SetLoadAction(RenderBufferLoadAction action)
		{
			RenderBufferHelper.SetLoadAction(out this, (int)action);
		}

		// Token: 0x06002C9D RID: 11421 RVA: 0x0004659C File Offset: 0x0004479C
		internal void SetStoreAction(RenderBufferStoreAction action)
		{
			RenderBufferHelper.SetStoreAction(out this, (int)action);
		}

		// Token: 0x17000A83 RID: 2691
		// (get) Token: 0x06002C9E RID: 11422 RVA: 0x000465A8 File Offset: 0x000447A8
		// (set) Token: 0x06002C9F RID: 11423 RVA: 0x000465C4 File Offset: 0x000447C4
		internal RenderBufferLoadAction loadAction
		{
			get
			{
				return (RenderBufferLoadAction)RenderBufferHelper.GetLoadAction(out this);
			}
			set
			{
				this.SetLoadAction(value);
			}
		}

		// Token: 0x17000A84 RID: 2692
		// (get) Token: 0x06002CA0 RID: 11424 RVA: 0x000465D0 File Offset: 0x000447D0
		// (set) Token: 0x06002CA1 RID: 11425 RVA: 0x000465EC File Offset: 0x000447EC
		internal RenderBufferStoreAction storeAction
		{
			get
			{
				return (RenderBufferStoreAction)RenderBufferHelper.GetStoreAction(out this);
			}
			set
			{
				this.SetStoreAction(value);
			}
		}

		// Token: 0x06002CA2 RID: 11426 RVA: 0x000465F8 File Offset: 0x000447F8
		public IntPtr GetNativeRenderBufferPtr()
		{
			return RenderBufferHelper.GetNativeRenderBufferPtr(this.m_BufferPtr);
		}

		// Token: 0x04000AD1 RID: 2769
		internal int m_RenderTextureInstanceID;

		// Token: 0x04000AD2 RID: 2770
		internal IntPtr m_BufferPtr;
	}
}
