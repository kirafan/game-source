﻿using System;

namespace UnityEngine
{
	// Token: 0x02000096 RID: 150
	public enum NetworkLogLevel
	{
		// Token: 0x04000160 RID: 352
		Off,
		// Token: 0x04000161 RID: 353
		Informational,
		// Token: 0x04000162 RID: 354
		Full = 3
	}
}
