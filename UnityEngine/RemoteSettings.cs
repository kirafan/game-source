﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200028C RID: 652
	public sealed class RemoteSettings
	{
		// Token: 0x1400000E RID: 14
		// (add) Token: 0x06002A58 RID: 10840 RVA: 0x0003EC54 File Offset: 0x0003CE54
		// (remove) Token: 0x06002A59 RID: 10841 RVA: 0x0003EC88 File Offset: 0x0003CE88
		public static event RemoteSettings.UpdatedEventHandler Updated;

		// Token: 0x06002A5A RID: 10842 RVA: 0x0003ECBC File Offset: 0x0003CEBC
		[RequiredByNativeCode]
		public static void CallOnUpdate()
		{
			RemoteSettings.UpdatedEventHandler updated = RemoteSettings.Updated;
			if (updated != null)
			{
				updated();
			}
		}

		// Token: 0x06002A5B RID: 10843
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetInt(string key, [DefaultValue("0")] int defaultValue);

		// Token: 0x06002A5C RID: 10844 RVA: 0x0003ECDC File Offset: 0x0003CEDC
		[ExcludeFromDocs]
		public static int GetInt(string key)
		{
			int defaultValue = 0;
			return RemoteSettings.GetInt(key, defaultValue);
		}

		// Token: 0x06002A5D RID: 10845
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetFloat(string key, [DefaultValue("0.0F")] float defaultValue);

		// Token: 0x06002A5E RID: 10846 RVA: 0x0003ECFC File Offset: 0x0003CEFC
		[ExcludeFromDocs]
		public static float GetFloat(string key)
		{
			float defaultValue = 0f;
			return RemoteSettings.GetFloat(key, defaultValue);
		}

		// Token: 0x06002A5F RID: 10847
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetString(string key, [DefaultValue("\"\"")] string defaultValue);

		// Token: 0x06002A60 RID: 10848 RVA: 0x0003ED20 File Offset: 0x0003CF20
		[ExcludeFromDocs]
		public static string GetString(string key)
		{
			string defaultValue = "";
			return RemoteSettings.GetString(key, defaultValue);
		}

		// Token: 0x06002A61 RID: 10849
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetBool(string key, [DefaultValue("false")] bool defaultValue);

		// Token: 0x06002A62 RID: 10850 RVA: 0x0003ED44 File Offset: 0x0003CF44
		[ExcludeFromDocs]
		public static bool GetBool(string key)
		{
			bool defaultValue = false;
			return RemoteSettings.GetBool(key, defaultValue);
		}

		// Token: 0x06002A63 RID: 10851
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool HasKey(string key);

		// Token: 0x0200028D RID: 653
		// (Invoke) Token: 0x06002A65 RID: 10853
		public delegate void UpdatedEventHandler();
	}
}
