﻿using System;

namespace UnityEngine
{
	// Token: 0x020001D7 RID: 471
	public struct HumanDescription
	{
		// Token: 0x170007FB RID: 2043
		// (get) Token: 0x06001FF4 RID: 8180 RVA: 0x00024444 File Offset: 0x00022644
		// (set) Token: 0x06001FF5 RID: 8181 RVA: 0x00024460 File Offset: 0x00022660
		public float upperArmTwist
		{
			get
			{
				return this.m_ArmTwist;
			}
			set
			{
				this.m_ArmTwist = value;
			}
		}

		// Token: 0x170007FC RID: 2044
		// (get) Token: 0x06001FF6 RID: 8182 RVA: 0x0002446C File Offset: 0x0002266C
		// (set) Token: 0x06001FF7 RID: 8183 RVA: 0x00024488 File Offset: 0x00022688
		public float lowerArmTwist
		{
			get
			{
				return this.m_ForeArmTwist;
			}
			set
			{
				this.m_ForeArmTwist = value;
			}
		}

		// Token: 0x170007FD RID: 2045
		// (get) Token: 0x06001FF8 RID: 8184 RVA: 0x00024494 File Offset: 0x00022694
		// (set) Token: 0x06001FF9 RID: 8185 RVA: 0x000244B0 File Offset: 0x000226B0
		public float upperLegTwist
		{
			get
			{
				return this.m_UpperLegTwist;
			}
			set
			{
				this.m_UpperLegTwist = value;
			}
		}

		// Token: 0x170007FE RID: 2046
		// (get) Token: 0x06001FFA RID: 8186 RVA: 0x000244BC File Offset: 0x000226BC
		// (set) Token: 0x06001FFB RID: 8187 RVA: 0x000244D8 File Offset: 0x000226D8
		public float lowerLegTwist
		{
			get
			{
				return this.m_LegTwist;
			}
			set
			{
				this.m_LegTwist = value;
			}
		}

		// Token: 0x170007FF RID: 2047
		// (get) Token: 0x06001FFC RID: 8188 RVA: 0x000244E4 File Offset: 0x000226E4
		// (set) Token: 0x06001FFD RID: 8189 RVA: 0x00024500 File Offset: 0x00022700
		public float armStretch
		{
			get
			{
				return this.m_ArmStretch;
			}
			set
			{
				this.m_ArmStretch = value;
			}
		}

		// Token: 0x17000800 RID: 2048
		// (get) Token: 0x06001FFE RID: 8190 RVA: 0x0002450C File Offset: 0x0002270C
		// (set) Token: 0x06001FFF RID: 8191 RVA: 0x00024528 File Offset: 0x00022728
		public float legStretch
		{
			get
			{
				return this.m_LegStretch;
			}
			set
			{
				this.m_LegStretch = value;
			}
		}

		// Token: 0x17000801 RID: 2049
		// (get) Token: 0x06002000 RID: 8192 RVA: 0x00024534 File Offset: 0x00022734
		// (set) Token: 0x06002001 RID: 8193 RVA: 0x00024550 File Offset: 0x00022750
		public float feetSpacing
		{
			get
			{
				return this.m_FeetSpacing;
			}
			set
			{
				this.m_FeetSpacing = value;
			}
		}

		// Token: 0x17000802 RID: 2050
		// (get) Token: 0x06002002 RID: 8194 RVA: 0x0002455C File Offset: 0x0002275C
		// (set) Token: 0x06002003 RID: 8195 RVA: 0x00024578 File Offset: 0x00022778
		public bool hasTranslationDoF
		{
			get
			{
				return this.m_HasTranslationDoF;
			}
			set
			{
				this.m_HasTranslationDoF = value;
			}
		}

		// Token: 0x04000519 RID: 1305
		public HumanBone[] human;

		// Token: 0x0400051A RID: 1306
		public SkeletonBone[] skeleton;

		// Token: 0x0400051B RID: 1307
		internal float m_ArmTwist;

		// Token: 0x0400051C RID: 1308
		internal float m_ForeArmTwist;

		// Token: 0x0400051D RID: 1309
		internal float m_UpperLegTwist;

		// Token: 0x0400051E RID: 1310
		internal float m_LegTwist;

		// Token: 0x0400051F RID: 1311
		internal float m_ArmStretch;

		// Token: 0x04000520 RID: 1312
		internal float m_LegStretch;

		// Token: 0x04000521 RID: 1313
		internal float m_FeetSpacing;

		// Token: 0x04000522 RID: 1314
		internal bool m_HasTranslationDoF;
	}
}
