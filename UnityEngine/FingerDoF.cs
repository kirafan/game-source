﻿using System;

namespace UnityEngine
{
	// Token: 0x020001DE RID: 478
	internal enum FingerDoF
	{
		// Token: 0x0400054F RID: 1359
		ProximalDownUp,
		// Token: 0x04000550 RID: 1360
		ProximalInOut,
		// Token: 0x04000551 RID: 1361
		IntermediateCloseOpen,
		// Token: 0x04000552 RID: 1362
		DistalCloseOpen,
		// Token: 0x04000553 RID: 1363
		LastFingerDoF
	}
}
