﻿using System;

namespace UnityEngine
{
	// Token: 0x02000196 RID: 406
	public enum AudioSpeakerMode
	{
		// Token: 0x04000438 RID: 1080
		Raw,
		// Token: 0x04000439 RID: 1081
		Mono,
		// Token: 0x0400043A RID: 1082
		Stereo,
		// Token: 0x0400043B RID: 1083
		Quad,
		// Token: 0x0400043C RID: 1084
		Surround,
		// Token: 0x0400043D RID: 1085
		Mode5point1,
		// Token: 0x0400043E RID: 1086
		Mode7point1,
		// Token: 0x0400043F RID: 1087
		Prologic
	}
}
