﻿using System;

namespace UnityEngine
{
	// Token: 0x02000067 RID: 103
	public enum FullScreenMovieControlMode
	{
		// Token: 0x0400009C RID: 156
		Full,
		// Token: 0x0400009D RID: 157
		Minimal,
		// Token: 0x0400009E RID: 158
		CancelOnInput,
		// Token: 0x0400009F RID: 159
		Hidden
	}
}
