﻿using System;
using System.Runtime.Serialization;

namespace UnityEngine
{
	// Token: 0x02000372 RID: 882
	[Serializable]
	public class MissingReferenceException : SystemException
	{
		// Token: 0x06002E4A RID: 11850 RVA: 0x0004A690 File Offset: 0x00048890
		public MissingReferenceException() : base("A Unity Runtime error occurred!")
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E4B RID: 11851 RVA: 0x0004A6AC File Offset: 0x000488AC
		public MissingReferenceException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E4C RID: 11852 RVA: 0x0004A6C4 File Offset: 0x000488C4
		public MissingReferenceException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E4D RID: 11853 RVA: 0x0004A6DC File Offset: 0x000488DC
		protected MissingReferenceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000D4D RID: 3405
		private const int Result = -2147467261;

		// Token: 0x04000D4E RID: 3406
		private string unityStackTrace;
	}
}
