﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200023B RID: 571
	public class GUIUtility
	{
		// Token: 0x1700095C RID: 2396
		// (get) Token: 0x060026A2 RID: 9890 RVA: 0x00034828 File Offset: 0x00032A28
		internal static float pixelsPerPoint
		{
			get
			{
				return GUIUtility.Internal_GetPixelsPerPoint();
			}
		}

		// Token: 0x060026A3 RID: 9891 RVA: 0x00034844 File Offset: 0x00032A44
		public static int GetControlID(FocusType focus)
		{
			return GUIUtility.GetControlID(0, focus);
		}

		// Token: 0x060026A4 RID: 9892 RVA: 0x00034860 File Offset: 0x00032A60
		public static int GetControlID(GUIContent contents, FocusType focus)
		{
			return GUIUtility.GetControlID(contents.hash, focus);
		}

		// Token: 0x060026A5 RID: 9893 RVA: 0x00034884 File Offset: 0x00032A84
		public static int GetControlID(FocusType focus, Rect position)
		{
			return GUIUtility.Internal_GetNextControlID2(0, focus, position);
		}

		// Token: 0x060026A6 RID: 9894 RVA: 0x000348A4 File Offset: 0x00032AA4
		public static int GetControlID(int hint, FocusType focus, Rect position)
		{
			return GUIUtility.Internal_GetNextControlID2(hint, focus, position);
		}

		// Token: 0x060026A7 RID: 9895 RVA: 0x000348C4 File Offset: 0x00032AC4
		public static int GetControlID(GUIContent contents, FocusType focus, Rect position)
		{
			return GUIUtility.Internal_GetNextControlID2(contents.hash, focus, position);
		}

		// Token: 0x060026A8 RID: 9896 RVA: 0x000348E8 File Offset: 0x00032AE8
		public static object GetStateObject(Type t, int controlID)
		{
			return GUIStateObjects.GetStateObject(t, controlID);
		}

		// Token: 0x060026A9 RID: 9897 RVA: 0x00034904 File Offset: 0x00032B04
		public static object QueryStateObject(Type t, int controlID)
		{
			return GUIStateObjects.QueryStateObject(t, controlID);
		}

		// Token: 0x1700095D RID: 2397
		// (get) Token: 0x060026AA RID: 9898 RVA: 0x00034920 File Offset: 0x00032B20
		// (set) Token: 0x060026AB RID: 9899 RVA: 0x0003493C File Offset: 0x00032B3C
		internal static bool guiIsExiting { get; set; }

		// Token: 0x1700095E RID: 2398
		// (get) Token: 0x060026AC RID: 9900 RVA: 0x00034944 File Offset: 0x00032B44
		// (set) Token: 0x060026AD RID: 9901 RVA: 0x00034960 File Offset: 0x00032B60
		public static int hotControl
		{
			get
			{
				return GUIUtility.Internal_GetHotControl();
			}
			set
			{
				GUIUtility.Internal_SetHotControl(value);
			}
		}

		// Token: 0x060026AE RID: 9902 RVA: 0x0003496C File Offset: 0x00032B6C
		public static void ExitGUI()
		{
			GUIUtility.guiIsExiting = true;
			throw new ExitGUIException();
		}

		// Token: 0x060026AF RID: 9903 RVA: 0x0003497C File Offset: 0x00032B7C
		internal static GUISkin GetDefaultSkin()
		{
			return GUIUtility.Internal_GetDefaultSkin(GUIUtility.s_SkinMode);
		}

		// Token: 0x060026B0 RID: 9904 RVA: 0x0003499C File Offset: 0x00032B9C
		internal static GUISkin GetBuiltinSkin(int skin)
		{
			return GUIUtility.Internal_GetBuiltinSkin(skin) as GUISkin;
		}

		// Token: 0x060026B1 RID: 9905 RVA: 0x000349BC File Offset: 0x00032BBC
		internal static void CleanupRoots()
		{
		}

		// Token: 0x060026B2 RID: 9906 RVA: 0x000349C0 File Offset: 0x00032BC0
		[RequiredByNativeCode]
		internal static void BeginGUI(int skinMode, int instanceID, int useGUILayout)
		{
			GUIUtility.s_SkinMode = skinMode;
			GUIUtility.s_OriginalID = instanceID;
			GUI.skin = null;
			GUIUtility.guiIsExiting = false;
			if (useGUILayout != 0)
			{
				GUILayoutUtility.SelectIDList(instanceID, false);
				GUILayoutUtility.Begin(instanceID);
			}
			GUI.changed = false;
		}

		// Token: 0x060026B3 RID: 9907 RVA: 0x000349F8 File Offset: 0x00032BF8
		[RequiredByNativeCode]
		internal static void EndGUI(int layoutType)
		{
			try
			{
				if (Event.current.type == EventType.Layout)
				{
					if (layoutType != 0)
					{
						if (layoutType != 1)
						{
							if (layoutType == 2)
							{
								GUILayoutUtility.LayoutFromEditorWindow();
							}
						}
						else
						{
							GUILayoutUtility.Layout();
						}
					}
				}
				GUILayoutUtility.SelectIDList(GUIUtility.s_OriginalID, false);
				GUIContent.ClearStaticCache();
			}
			finally
			{
				GUIUtility.Internal_ExitGUI();
			}
		}

		// Token: 0x060026B4 RID: 9908 RVA: 0x00034A7C File Offset: 0x00032C7C
		[RequiredByNativeCode]
		internal static bool EndGUIFromException(Exception exception)
		{
			bool result;
			if (!GUIUtility.ShouldRethrowException(exception))
			{
				result = false;
			}
			else
			{
				GUIUtility.Internal_ExitGUI();
				result = true;
			}
			return result;
		}

		// Token: 0x060026B5 RID: 9909 RVA: 0x00034AAC File Offset: 0x00032CAC
		internal static bool ShouldRethrowException(Exception exception)
		{
			while (exception is TargetInvocationException && exception.InnerException != null)
			{
				exception = exception.InnerException;
			}
			return exception is ExitGUIException;
		}

		// Token: 0x060026B6 RID: 9910 RVA: 0x00034AF0 File Offset: 0x00032CF0
		internal static void CheckOnGUI()
		{
			if (GUIUtility.Internal_GetGUIDepth() <= 0)
			{
				throw new ArgumentException("You can only call GUI functions from inside OnGUI.");
			}
		}

		// Token: 0x060026B7 RID: 9911 RVA: 0x00034B0C File Offset: 0x00032D0C
		public static Vector2 GUIToScreenPoint(Vector2 guiPoint)
		{
			return GUIClip.Unclip(guiPoint) + GUIUtility.s_EditorScreenPointOffset;
		}

		// Token: 0x060026B8 RID: 9912 RVA: 0x00034B34 File Offset: 0x00032D34
		internal static Rect GUIToScreenRect(Rect guiRect)
		{
			Vector2 vector = GUIUtility.GUIToScreenPoint(new Vector2(guiRect.x, guiRect.y));
			guiRect.x = vector.x;
			guiRect.y = vector.y;
			return guiRect;
		}

		// Token: 0x060026B9 RID: 9913 RVA: 0x00034B80 File Offset: 0x00032D80
		public static Vector2 ScreenToGUIPoint(Vector2 screenPoint)
		{
			return GUIClip.Clip(screenPoint) - GUIUtility.s_EditorScreenPointOffset;
		}

		// Token: 0x060026BA RID: 9914 RVA: 0x00034BA8 File Offset: 0x00032DA8
		public static Rect ScreenToGUIRect(Rect screenRect)
		{
			Vector2 vector = GUIUtility.ScreenToGUIPoint(new Vector2(screenRect.x, screenRect.y));
			screenRect.x = vector.x;
			screenRect.y = vector.y;
			return screenRect;
		}

		// Token: 0x060026BB RID: 9915 RVA: 0x00034BF4 File Offset: 0x00032DF4
		public static void RotateAroundPivot(float angle, Vector2 pivotPoint)
		{
			Matrix4x4 matrix = GUI.matrix;
			GUI.matrix = Matrix4x4.identity;
			Vector2 vector = GUIClip.Unclip(pivotPoint);
			Matrix4x4 lhs = Matrix4x4.TRS(vector, Quaternion.Euler(0f, 0f, angle), Vector3.one) * Matrix4x4.TRS(-vector, Quaternion.identity, Vector3.one);
			GUI.matrix = lhs * matrix;
		}

		// Token: 0x060026BC RID: 9916 RVA: 0x00034C68 File Offset: 0x00032E68
		public static void ScaleAroundPivot(Vector2 scale, Vector2 pivotPoint)
		{
			Matrix4x4 matrix = GUI.matrix;
			Vector2 vector = GUIClip.Unclip(pivotPoint);
			Matrix4x4 lhs = Matrix4x4.TRS(vector, Quaternion.identity, new Vector3(scale.x, scale.y, 1f)) * Matrix4x4.TRS(-vector, Quaternion.identity, Vector3.one);
			GUI.matrix = lhs * matrix;
		}

		// Token: 0x060026BD RID: 9917
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float Internal_GetPixelsPerPoint();

		// Token: 0x060026BE RID: 9918
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetControlID(int hint, FocusType focus);

		// Token: 0x060026BF RID: 9919 RVA: 0x00034CD8 File Offset: 0x00032ED8
		private static int Internal_GetNextControlID2(int hint, FocusType focusType, Rect rect)
		{
			return GUIUtility.INTERNAL_CALL_Internal_GetNextControlID2(hint, focusType, ref rect);
		}

		// Token: 0x060026C0 RID: 9920
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_Internal_GetNextControlID2(int hint, FocusType focusType, ref Rect rect);

		// Token: 0x060026C1 RID: 9921
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetPermanentControlID();

		// Token: 0x060026C2 RID: 9922
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetHotControl();

		// Token: 0x060026C3 RID: 9923
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_SetHotControl(int value);

		// Token: 0x060026C4 RID: 9924
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void UpdateUndoName();

		// Token: 0x060026C5 RID: 9925
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void GrabMouseControl(int id);

		// Token: 0x060026C6 RID: 9926
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void ReleaseMouseControl();

		// Token: 0x060026C7 RID: 9927
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool HasMouseControl(int id);

		// Token: 0x060026C8 RID: 9928
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool GetChanged();

		// Token: 0x060026C9 RID: 9929
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetChanged(bool changed);

		// Token: 0x1700095F RID: 2399
		// (get) Token: 0x060026CA RID: 9930
		// (set) Token: 0x060026CB RID: 9931
		public static extern int keyboardControl { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060026CC RID: 9932
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetDidGUIWindowsEatLastEvent(bool value);

		// Token: 0x17000960 RID: 2400
		// (get) Token: 0x060026CD RID: 9933
		// (set) Token: 0x060026CE RID: 9934
		public static extern string systemCopyBuffer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060026CF RID: 9935
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern GUISkin Internal_GetDefaultSkin(int skinMode);

		// Token: 0x060026D0 RID: 9936
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Object Internal_GetBuiltinSkin(int skin);

		// Token: 0x060026D1 RID: 9937
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_ExitGUI();

		// Token: 0x060026D2 RID: 9938
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int Internal_GetGUIDepth();

		// Token: 0x17000961 RID: 2401
		// (get) Token: 0x060026D3 RID: 9939
		// (set) Token: 0x060026D4 RID: 9940
		internal static extern bool mouseUsed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000962 RID: 2402
		// (get) Token: 0x060026D5 RID: 9941
		public static extern bool hasModalWindow { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000963 RID: 2403
		// (get) Token: 0x060026D6 RID: 9942
		// (set) Token: 0x060026D7 RID: 9943
		internal static extern bool textFieldInput { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0400086C RID: 2156
		internal static int s_SkinMode;

		// Token: 0x0400086D RID: 2157
		internal static int s_OriginalID;

		// Token: 0x0400086F RID: 2159
		internal static Vector2 s_EditorScreenPointOffset = Vector2.zero;

		// Token: 0x04000870 RID: 2160
		internal static bool s_HasKeyboardFocus = false;
	}
}
