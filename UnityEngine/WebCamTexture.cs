﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020001B6 RID: 438
	public sealed class WebCamTexture : Texture
	{
		// Token: 0x06001DFC RID: 7676 RVA: 0x000228D8 File Offset: 0x00020AD8
		public WebCamTexture(string deviceName, int requestedWidth, int requestedHeight, int requestedFPS)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, deviceName, requestedWidth, requestedHeight, requestedFPS);
		}

		// Token: 0x06001DFD RID: 7677 RVA: 0x000228EC File Offset: 0x00020AEC
		public WebCamTexture(string deviceName, int requestedWidth, int requestedHeight)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, deviceName, requestedWidth, requestedHeight, 0);
		}

		// Token: 0x06001DFE RID: 7678 RVA: 0x00022900 File Offset: 0x00020B00
		public WebCamTexture(string deviceName)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, deviceName, 0, 0, 0);
		}

		// Token: 0x06001DFF RID: 7679 RVA: 0x00022914 File Offset: 0x00020B14
		public WebCamTexture(int requestedWidth, int requestedHeight, int requestedFPS)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, "", requestedWidth, requestedHeight, requestedFPS);
		}

		// Token: 0x06001E00 RID: 7680 RVA: 0x0002292C File Offset: 0x00020B2C
		public WebCamTexture(int requestedWidth, int requestedHeight)
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, "", requestedWidth, requestedHeight, 0);
		}

		// Token: 0x06001E01 RID: 7681 RVA: 0x00022944 File Offset: 0x00020B44
		public WebCamTexture()
		{
			WebCamTexture.Internal_CreateWebCamTexture(this, "", 0, 0, 0);
		}

		// Token: 0x06001E02 RID: 7682
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateWebCamTexture([Writable] WebCamTexture self, string scriptingDevice, int requestedWidth, int requestedHeight, int maxFramerate);

		// Token: 0x06001E03 RID: 7683 RVA: 0x0002295C File Offset: 0x00020B5C
		public void Play()
		{
			WebCamTexture.INTERNAL_CALL_Play(this);
		}

		// Token: 0x06001E04 RID: 7684
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Play(WebCamTexture self);

		// Token: 0x06001E05 RID: 7685 RVA: 0x00022968 File Offset: 0x00020B68
		public void Pause()
		{
			WebCamTexture.INTERNAL_CALL_Pause(this);
		}

		// Token: 0x06001E06 RID: 7686
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Pause(WebCamTexture self);

		// Token: 0x06001E07 RID: 7687 RVA: 0x00022974 File Offset: 0x00020B74
		public void Stop()
		{
			WebCamTexture.INTERNAL_CALL_Stop(this);
		}

		// Token: 0x06001E08 RID: 7688
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Stop(WebCamTexture self);

		// Token: 0x17000773 RID: 1907
		// (get) Token: 0x06001E09 RID: 7689
		public extern bool isPlaying { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000774 RID: 1908
		// (get) Token: 0x06001E0A RID: 7690
		// (set) Token: 0x06001E0B RID: 7691
		public extern string deviceName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000775 RID: 1909
		// (get) Token: 0x06001E0C RID: 7692
		// (set) Token: 0x06001E0D RID: 7693
		public extern float requestedFPS { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000776 RID: 1910
		// (get) Token: 0x06001E0E RID: 7694
		// (set) Token: 0x06001E0F RID: 7695
		public extern int requestedWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000777 RID: 1911
		// (get) Token: 0x06001E10 RID: 7696
		// (set) Token: 0x06001E11 RID: 7697
		public extern int requestedHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000778 RID: 1912
		// (get) Token: 0x06001E12 RID: 7698
		public static extern WebCamDevice[] devices { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001E13 RID: 7699 RVA: 0x00022980 File Offset: 0x00020B80
		public Color GetPixel(int x, int y)
		{
			Color result;
			WebCamTexture.INTERNAL_CALL_GetPixel(this, x, y, out result);
			return result;
		}

		// Token: 0x06001E14 RID: 7700
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPixel(WebCamTexture self, int x, int y, out Color value);

		// Token: 0x06001E15 RID: 7701 RVA: 0x000229A0 File Offset: 0x00020BA0
		public Color[] GetPixels()
		{
			return this.GetPixels(0, 0, this.width, this.height);
		}

		// Token: 0x06001E16 RID: 7702
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(int x, int y, int blockWidth, int blockHeight);

		// Token: 0x06001E17 RID: 7703
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color32[] GetPixels32([DefaultValue("null")] Color32[] colors);

		// Token: 0x06001E18 RID: 7704 RVA: 0x000229CC File Offset: 0x00020BCC
		[ExcludeFromDocs]
		public Color32[] GetPixels32()
		{
			Color32[] colors = null;
			return this.GetPixels32(colors);
		}

		// Token: 0x17000779 RID: 1913
		// (get) Token: 0x06001E19 RID: 7705
		public extern int videoRotationAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700077A RID: 1914
		// (get) Token: 0x06001E1A RID: 7706
		public extern bool videoVerticallyMirrored { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700077B RID: 1915
		// (get) Token: 0x06001E1B RID: 7707
		public extern bool didUpdateThisFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
