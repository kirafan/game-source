﻿using System;

namespace UnityEngine
{
	// Token: 0x02000093 RID: 147
	public enum MasterServerEvent
	{
		// Token: 0x04000151 RID: 337
		RegistrationFailedGameName,
		// Token: 0x04000152 RID: 338
		RegistrationFailedGameType,
		// Token: 0x04000153 RID: 339
		RegistrationFailedNoServer,
		// Token: 0x04000154 RID: 340
		RegistrationSucceeded,
		// Token: 0x04000155 RID: 341
		HostListReceived
	}
}
