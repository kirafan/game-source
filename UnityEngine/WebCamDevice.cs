﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001B5 RID: 437
	[UsedByNativeCode]
	public struct WebCamDevice
	{
		// Token: 0x17000771 RID: 1905
		// (get) Token: 0x06001DFA RID: 7674 RVA: 0x0002289C File Offset: 0x00020A9C
		public string name
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x17000772 RID: 1906
		// (get) Token: 0x06001DFB RID: 7675 RVA: 0x000228B8 File Offset: 0x00020AB8
		public bool isFrontFacing
		{
			get
			{
				return (this.m_Flags & 1) == 1;
			}
		}

		// Token: 0x040004A0 RID: 1184
		internal string m_Name;

		// Token: 0x040004A1 RID: 1185
		internal int m_Flags;
	}
}
