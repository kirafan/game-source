﻿using System;

namespace UnityEngine
{
	// Token: 0x0200022A RID: 554
	internal class GUILayoutEntry
	{
		// Token: 0x06002585 RID: 9605 RVA: 0x00031374 File Offset: 0x0002F574
		public GUILayoutEntry(float _minWidth, float _maxWidth, float _minHeight, float _maxHeight, GUIStyle _style)
		{
			this.minWidth = _minWidth;
			this.maxWidth = _maxWidth;
			this.minHeight = _minHeight;
			this.maxHeight = _maxHeight;
			if (_style == null)
			{
				_style = GUIStyle.none;
			}
			this.style = _style;
		}

		// Token: 0x06002586 RID: 9606 RVA: 0x000313E8 File Offset: 0x0002F5E8
		public GUILayoutEntry(float _minWidth, float _maxWidth, float _minHeight, float _maxHeight, GUIStyle _style, GUILayoutOption[] options)
		{
			this.minWidth = _minWidth;
			this.maxWidth = _maxWidth;
			this.minHeight = _minHeight;
			this.maxHeight = _maxHeight;
			this.style = _style;
			this.ApplyOptions(options);
		}

		// Token: 0x17000919 RID: 2329
		// (get) Token: 0x06002587 RID: 9607 RVA: 0x00031454 File Offset: 0x0002F654
		// (set) Token: 0x06002588 RID: 9608 RVA: 0x00031470 File Offset: 0x0002F670
		public GUIStyle style
		{
			get
			{
				return this.m_Style;
			}
			set
			{
				this.m_Style = value;
				this.ApplyStyleSettings(value);
			}
		}

		// Token: 0x1700091A RID: 2330
		// (get) Token: 0x06002589 RID: 9609 RVA: 0x00031484 File Offset: 0x0002F684
		public virtual RectOffset margin
		{
			get
			{
				return this.style.margin;
			}
		}

		// Token: 0x0600258A RID: 9610 RVA: 0x000314A4 File Offset: 0x0002F6A4
		public virtual void CalcWidth()
		{
		}

		// Token: 0x0600258B RID: 9611 RVA: 0x000314A8 File Offset: 0x0002F6A8
		public virtual void CalcHeight()
		{
		}

		// Token: 0x0600258C RID: 9612 RVA: 0x000314AC File Offset: 0x0002F6AC
		public virtual void SetHorizontal(float x, float width)
		{
			this.rect.x = x;
			this.rect.width = width;
		}

		// Token: 0x0600258D RID: 9613 RVA: 0x000314C8 File Offset: 0x0002F6C8
		public virtual void SetVertical(float y, float height)
		{
			this.rect.y = y;
			this.rect.height = height;
		}

		// Token: 0x0600258E RID: 9614 RVA: 0x000314E4 File Offset: 0x0002F6E4
		protected virtual void ApplyStyleSettings(GUIStyle style)
		{
			this.stretchWidth = ((style.fixedWidth != 0f || !style.stretchWidth) ? 0 : 1);
			this.stretchHeight = ((style.fixedHeight != 0f || !style.stretchHeight) ? 0 : 1);
			this.m_Style = style;
		}

		// Token: 0x0600258F RID: 9615 RVA: 0x0003154C File Offset: 0x0002F74C
		public virtual void ApplyOptions(GUILayoutOption[] options)
		{
			if (options != null)
			{
				foreach (GUILayoutOption guilayoutOption in options)
				{
					switch (guilayoutOption.type)
					{
					case GUILayoutOption.Type.fixedWidth:
						this.minWidth = (this.maxWidth = (float)guilayoutOption.value);
						this.stretchWidth = 0;
						break;
					case GUILayoutOption.Type.fixedHeight:
						this.minHeight = (this.maxHeight = (float)guilayoutOption.value);
						this.stretchHeight = 0;
						break;
					case GUILayoutOption.Type.minWidth:
						this.minWidth = (float)guilayoutOption.value;
						if (this.maxWidth < this.minWidth)
						{
							this.maxWidth = this.minWidth;
						}
						break;
					case GUILayoutOption.Type.maxWidth:
						this.maxWidth = (float)guilayoutOption.value;
						if (this.minWidth > this.maxWidth)
						{
							this.minWidth = this.maxWidth;
						}
						this.stretchWidth = 0;
						break;
					case GUILayoutOption.Type.minHeight:
						this.minHeight = (float)guilayoutOption.value;
						if (this.maxHeight < this.minHeight)
						{
							this.maxHeight = this.minHeight;
						}
						break;
					case GUILayoutOption.Type.maxHeight:
						this.maxHeight = (float)guilayoutOption.value;
						if (this.minHeight > this.maxHeight)
						{
							this.minHeight = this.maxHeight;
						}
						this.stretchHeight = 0;
						break;
					case GUILayoutOption.Type.stretchWidth:
						this.stretchWidth = (int)guilayoutOption.value;
						break;
					case GUILayoutOption.Type.stretchHeight:
						this.stretchHeight = (int)guilayoutOption.value;
						break;
					}
				}
				if (this.maxWidth != 0f && this.maxWidth < this.minWidth)
				{
					this.maxWidth = this.minWidth;
				}
				if (this.maxHeight != 0f && this.maxHeight < this.minHeight)
				{
					this.maxHeight = this.minHeight;
				}
			}
		}

		// Token: 0x06002590 RID: 9616 RVA: 0x00031764 File Offset: 0x0002F964
		public override string ToString()
		{
			string text = "";
			for (int i = 0; i < GUILayoutEntry.indent; i++)
			{
				text += " ";
			}
			return string.Concat(new object[]
			{
				text,
				UnityString.Format("{1}-{0} (x:{2}-{3}, y:{4}-{5})", new object[]
				{
					(this.style == null) ? "NULL" : this.style.name,
					base.GetType(),
					this.rect.x,
					this.rect.xMax,
					this.rect.y,
					this.rect.yMax
				}),
				"   -   W: ",
				this.minWidth,
				"-",
				this.maxWidth,
				(this.stretchWidth == 0) ? "" : "+",
				", H: ",
				this.minHeight,
				"-",
				this.maxHeight,
				(this.stretchHeight == 0) ? "" : "+"
			});
		}

		// Token: 0x0400080B RID: 2059
		public float minWidth;

		// Token: 0x0400080C RID: 2060
		public float maxWidth;

		// Token: 0x0400080D RID: 2061
		public float minHeight;

		// Token: 0x0400080E RID: 2062
		public float maxHeight;

		// Token: 0x0400080F RID: 2063
		public Rect rect = new Rect(0f, 0f, 0f, 0f);

		// Token: 0x04000810 RID: 2064
		public int stretchWidth;

		// Token: 0x04000811 RID: 2065
		public int stretchHeight;

		// Token: 0x04000812 RID: 2066
		private GUIStyle m_Style = GUIStyle.none;

		// Token: 0x04000813 RID: 2067
		internal static Rect kDummyRect = new Rect(0f, 0f, 1f, 1f);

		// Token: 0x04000814 RID: 2068
		protected static int indent = 0;
	}
}
