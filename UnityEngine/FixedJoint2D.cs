﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000178 RID: 376
	public sealed class FixedJoint2D : AnchoredJoint2D
	{
		// Token: 0x17000662 RID: 1634
		// (get) Token: 0x06001B2E RID: 6958
		// (set) Token: 0x06001B2F RID: 6959
		public extern float dampingRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000663 RID: 1635
		// (get) Token: 0x06001B30 RID: 6960
		// (set) Token: 0x06001B31 RID: 6961
		public extern float frequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000664 RID: 1636
		// (get) Token: 0x06001B32 RID: 6962
		public extern float referenceAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
