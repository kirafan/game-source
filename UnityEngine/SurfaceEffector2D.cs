﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000184 RID: 388
	public sealed class SurfaceEffector2D : Effector2D
	{
		// Token: 0x17000691 RID: 1681
		// (get) Token: 0x06001B9C RID: 7068
		// (set) Token: 0x06001B9D RID: 7069
		public extern float speed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000692 RID: 1682
		// (get) Token: 0x06001B9E RID: 7070
		// (set) Token: 0x06001B9F RID: 7071
		public extern float speedVariation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000693 RID: 1683
		// (get) Token: 0x06001BA0 RID: 7072
		// (set) Token: 0x06001BA1 RID: 7073
		public extern float forceScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000694 RID: 1684
		// (get) Token: 0x06001BA2 RID: 7074
		// (set) Token: 0x06001BA3 RID: 7075
		public extern bool useContactForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000695 RID: 1685
		// (get) Token: 0x06001BA4 RID: 7076
		// (set) Token: 0x06001BA5 RID: 7077
		public extern bool useFriction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000696 RID: 1686
		// (get) Token: 0x06001BA6 RID: 7078
		// (set) Token: 0x06001BA7 RID: 7079
		public extern bool useBounce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
