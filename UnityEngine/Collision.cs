﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200013F RID: 319
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class Collision
	{
		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x06001633 RID: 5683 RVA: 0x0001C418 File Offset: 0x0001A618
		public Vector3 relativeVelocity
		{
			get
			{
				return this.m_RelativeVelocity;
			}
		}

		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x06001634 RID: 5684 RVA: 0x0001C434 File Offset: 0x0001A634
		public Rigidbody rigidbody
		{
			get
			{
				return this.m_Rigidbody;
			}
		}

		// Token: 0x1700051C RID: 1308
		// (get) Token: 0x06001635 RID: 5685 RVA: 0x0001C450 File Offset: 0x0001A650
		public Collider collider
		{
			get
			{
				return this.m_Collider;
			}
		}

		// Token: 0x1700051D RID: 1309
		// (get) Token: 0x06001636 RID: 5686 RVA: 0x0001C46C File Offset: 0x0001A66C
		public Transform transform
		{
			get
			{
				return (!(this.rigidbody != null)) ? this.collider.transform : this.rigidbody.transform;
			}
		}

		// Token: 0x1700051E RID: 1310
		// (get) Token: 0x06001637 RID: 5687 RVA: 0x0001C4B0 File Offset: 0x0001A6B0
		public GameObject gameObject
		{
			get
			{
				return (!(this.m_Rigidbody != null)) ? this.m_Collider.gameObject : this.m_Rigidbody.gameObject;
			}
		}

		// Token: 0x1700051F RID: 1311
		// (get) Token: 0x06001638 RID: 5688 RVA: 0x0001C4F4 File Offset: 0x0001A6F4
		public ContactPoint[] contacts
		{
			get
			{
				return this.m_Contacts;
			}
		}

		// Token: 0x06001639 RID: 5689 RVA: 0x0001C510 File Offset: 0x0001A710
		public virtual IEnumerator GetEnumerator()
		{
			return this.contacts.GetEnumerator();
		}

		// Token: 0x17000520 RID: 1312
		// (get) Token: 0x0600163A RID: 5690 RVA: 0x0001C530 File Offset: 0x0001A730
		public Vector3 impulse
		{
			get
			{
				return this.m_Impulse;
			}
		}

		// Token: 0x17000521 RID: 1313
		// (get) Token: 0x0600163B RID: 5691 RVA: 0x0001C54C File Offset: 0x0001A74C
		[Obsolete("Use Collision.relativeVelocity instead.", false)]
		public Vector3 impactForceSum
		{
			get
			{
				return this.relativeVelocity;
			}
		}

		// Token: 0x17000522 RID: 1314
		// (get) Token: 0x0600163C RID: 5692 RVA: 0x0001C568 File Offset: 0x0001A768
		[Obsolete("Will always return zero.", false)]
		public Vector3 frictionForceSum
		{
			get
			{
				return Vector3.zero;
			}
		}

		// Token: 0x17000523 RID: 1315
		// (get) Token: 0x0600163D RID: 5693 RVA: 0x0001C584 File Offset: 0x0001A784
		[Obsolete("Please use Collision.rigidbody, Collision.transform or Collision.collider instead", false)]
		public Component other
		{
			get
			{
				return (!(this.m_Rigidbody != null)) ? this.m_Collider : this.m_Rigidbody;
			}
		}

		// Token: 0x04000390 RID: 912
		internal Vector3 m_Impulse;

		// Token: 0x04000391 RID: 913
		internal Vector3 m_RelativeVelocity;

		// Token: 0x04000392 RID: 914
		internal Rigidbody m_Rigidbody;

		// Token: 0x04000393 RID: 915
		internal Collider m_Collider;

		// Token: 0x04000394 RID: 916
		internal ContactPoint[] m_Contacts;
	}
}
