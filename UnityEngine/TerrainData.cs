﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001F4 RID: 500
	public sealed class TerrainData : Object
	{
		// Token: 0x060021C7 RID: 8647 RVA: 0x00027410 File Offset: 0x00025610
		public TerrainData()
		{
			this.Internal_Create(this);
		}

		// Token: 0x060021C8 RID: 8648
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetMaximumResolution();

		// Token: 0x060021C9 RID: 8649
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetMinimumDetailResolutionPerPatch();

		// Token: 0x060021CA RID: 8650
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetMaximumDetailResolutionPerPatch();

		// Token: 0x060021CB RID: 8651
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetMaximumDetailPatchCount();

		// Token: 0x060021CC RID: 8652
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetMinimumAlphamapResolution();

		// Token: 0x060021CD RID: 8653
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetMaximumAlphamapResolution();

		// Token: 0x060021CE RID: 8654
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetMinimumBaseMapResolution();

		// Token: 0x060021CF RID: 8655
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetMaximumBaseMapResolution();

		// Token: 0x060021D0 RID: 8656
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void Internal_Create([Writable] TerrainData terrainData);

		// Token: 0x060021D1 RID: 8657
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool HasUser(GameObject user);

		// Token: 0x060021D2 RID: 8658
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void AddUser(GameObject user);

		// Token: 0x060021D3 RID: 8659
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void RemoveUser(GameObject user);

		// Token: 0x17000853 RID: 2131
		// (get) Token: 0x060021D4 RID: 8660
		public extern int heightmapWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000854 RID: 2132
		// (get) Token: 0x060021D5 RID: 8661
		public extern int heightmapHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000855 RID: 2133
		// (get) Token: 0x060021D6 RID: 8662 RVA: 0x00027420 File Offset: 0x00025620
		// (set) Token: 0x060021D7 RID: 8663 RVA: 0x0002743C File Offset: 0x0002563C
		public int heightmapResolution
		{
			get
			{
				return this.Internal_heightmapResolution;
			}
			set
			{
				int internal_heightmapResolution = value;
				if (value < 0 || value > TerrainData.kMaximumResolution)
				{
					Debug.LogWarning("heightmapResolution is clamped to the range of [0, " + TerrainData.kMaximumResolution + "].");
					internal_heightmapResolution = Math.Min(TerrainData.kMaximumResolution, Math.Max(value, 0));
				}
				this.Internal_heightmapResolution = internal_heightmapResolution;
			}
		}

		// Token: 0x17000856 RID: 2134
		// (get) Token: 0x060021D8 RID: 8664
		// (set) Token: 0x060021D9 RID: 8665
		private extern int Internal_heightmapResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000857 RID: 2135
		// (get) Token: 0x060021DA RID: 8666 RVA: 0x00027498 File Offset: 0x00025698
		public Vector3 heightmapScale
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_heightmapScale(out result);
				return result;
			}
		}

		// Token: 0x060021DB RID: 8667
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_heightmapScale(out Vector3 value);

		// Token: 0x17000858 RID: 2136
		// (get) Token: 0x060021DC RID: 8668 RVA: 0x000274B8 File Offset: 0x000256B8
		// (set) Token: 0x060021DD RID: 8669 RVA: 0x000274D8 File Offset: 0x000256D8
		public Vector3 size
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_size(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_size(ref value);
			}
		}

		// Token: 0x060021DE RID: 8670
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_size(out Vector3 value);

		// Token: 0x060021DF RID: 8671
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_size(ref Vector3 value);

		// Token: 0x17000859 RID: 2137
		// (get) Token: 0x060021E0 RID: 8672 RVA: 0x000274E4 File Offset: 0x000256E4
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_bounds(out result);
				return result;
			}
		}

		// Token: 0x060021E1 RID: 8673
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_bounds(out Bounds value);

		// Token: 0x1700085A RID: 2138
		// (get) Token: 0x060021E2 RID: 8674
		// (set) Token: 0x060021E3 RID: 8675
		public extern float thickness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060021E4 RID: 8676
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetHeight(int x, int y);

		// Token: 0x060021E5 RID: 8677
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetInterpolatedHeight(float x, float y);

		// Token: 0x060021E6 RID: 8678
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float[,] GetHeights(int xBase, int yBase, int width, int height);

		// Token: 0x060021E7 RID: 8679 RVA: 0x00027504 File Offset: 0x00025704
		public void SetHeights(int xBase, int yBase, float[,] heights)
		{
			if (heights == null)
			{
				throw new NullReferenceException();
			}
			if (xBase + heights.GetLength(1) > this.heightmapWidth || xBase + heights.GetLength(1) < 0 || yBase + heights.GetLength(0) < 0 || xBase < 0 || yBase < 0 || yBase + heights.GetLength(0) > this.heightmapHeight)
			{
				throw new ArgumentException(UnityString.Format("X or Y base out of bounds. Setting up to {0}x{1} while map size is {2}x{3}", new object[]
				{
					xBase + heights.GetLength(1),
					yBase + heights.GetLength(0),
					this.heightmapWidth,
					this.heightmapHeight
				}));
			}
			this.Internal_SetHeights(xBase, yBase, heights.GetLength(1), heights.GetLength(0), heights);
		}

		// Token: 0x060021E8 RID: 8680
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetHeights(int xBase, int yBase, int width, int height, float[,] heights);

		// Token: 0x060021E9 RID: 8681
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetHeightsDelayLOD(int xBase, int yBase, int width, int height, float[,] heights);

		// Token: 0x060021EA RID: 8682 RVA: 0x000275E0 File Offset: 0x000257E0
		public void SetHeightsDelayLOD(int xBase, int yBase, float[,] heights)
		{
			if (heights == null)
			{
				throw new ArgumentNullException("heights");
			}
			int length = heights.GetLength(0);
			int length2 = heights.GetLength(1);
			if (xBase < 0 || xBase + length2 < 0 || xBase + length2 > this.heightmapWidth)
			{
				throw new ArgumentException(UnityString.Format("X out of bounds - trying to set {0}-{1} but the terrain ranges from 0-{2}", new object[]
				{
					xBase,
					xBase + length2,
					this.heightmapWidth
				}));
			}
			if (yBase < 0 || yBase + length < 0 || yBase + length > this.heightmapHeight)
			{
				throw new ArgumentException(UnityString.Format("Y out of bounds - trying to set {0}-{1} but the terrain ranges from 0-{2}", new object[]
				{
					yBase,
					yBase + length,
					this.heightmapHeight
				}));
			}
			this.Internal_SetHeightsDelayLOD(xBase, yBase, length2, length, heights);
		}

		// Token: 0x060021EB RID: 8683
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetSteepness(float x, float y);

		// Token: 0x060021EC RID: 8684 RVA: 0x000276C8 File Offset: 0x000258C8
		public Vector3 GetInterpolatedNormal(float x, float y)
		{
			Vector3 result;
			TerrainData.INTERNAL_CALL_GetInterpolatedNormal(this, x, y, out result);
			return result;
		}

		// Token: 0x060021ED RID: 8685
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetInterpolatedNormal(TerrainData self, float x, float y, out Vector3 value);

		// Token: 0x060021EE RID: 8686
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetAdjustedSize(int size);

		// Token: 0x1700085B RID: 2139
		// (get) Token: 0x060021EF RID: 8687
		// (set) Token: 0x060021F0 RID: 8688
		public extern float wavingGrassStrength { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700085C RID: 2140
		// (get) Token: 0x060021F1 RID: 8689
		// (set) Token: 0x060021F2 RID: 8690
		public extern float wavingGrassAmount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700085D RID: 2141
		// (get) Token: 0x060021F3 RID: 8691
		// (set) Token: 0x060021F4 RID: 8692
		public extern float wavingGrassSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700085E RID: 2142
		// (get) Token: 0x060021F5 RID: 8693 RVA: 0x000276E8 File Offset: 0x000258E8
		// (set) Token: 0x060021F6 RID: 8694 RVA: 0x00027708 File Offset: 0x00025908
		public Color wavingGrassTint
		{
			get
			{
				Color result;
				this.INTERNAL_get_wavingGrassTint(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_wavingGrassTint(ref value);
			}
		}

		// Token: 0x060021F7 RID: 8695
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_wavingGrassTint(out Color value);

		// Token: 0x060021F8 RID: 8696
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_wavingGrassTint(ref Color value);

		// Token: 0x1700085F RID: 2143
		// (get) Token: 0x060021F9 RID: 8697
		public extern int detailWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000860 RID: 2144
		// (get) Token: 0x060021FA RID: 8698
		public extern int detailHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060021FB RID: 8699 RVA: 0x00027714 File Offset: 0x00025914
		public void SetDetailResolution(int detailResolution, int resolutionPerPatch)
		{
			if (detailResolution < 0)
			{
				Debug.LogWarning("detailResolution must not be negative.");
				detailResolution = 0;
			}
			if (resolutionPerPatch < TerrainData.kMinimumDetailResolutionPerPatch || resolutionPerPatch > TerrainData.kMaximumDetailResolutionPerPatch)
			{
				Debug.LogWarning(string.Concat(new object[]
				{
					"resolutionPerPatch is clamped to the range of [",
					TerrainData.kMinimumDetailResolutionPerPatch,
					", ",
					TerrainData.kMaximumDetailResolutionPerPatch,
					"]."
				}));
				resolutionPerPatch = Math.Min(TerrainData.kMaximumDetailResolutionPerPatch, Math.Max(resolutionPerPatch, TerrainData.kMinimumDetailResolutionPerPatch));
			}
			int num = detailResolution / resolutionPerPatch;
			if (num > TerrainData.kMaximumDetailPatchCount)
			{
				Debug.LogWarning("Patch count (detailResolution / resolutionPerPatch) is clamped to the range of [0, " + TerrainData.kMaximumDetailPatchCount + "].");
				num = Math.Min(TerrainData.kMaximumDetailPatchCount, Math.Max(num, 0));
			}
			this.Internal_SetDetailResolution(num, resolutionPerPatch);
		}

		// Token: 0x060021FC RID: 8700
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetDetailResolution(int patchCount, int resolutionPerPatch);

		// Token: 0x17000861 RID: 2145
		// (get) Token: 0x060021FD RID: 8701
		public extern int detailResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000862 RID: 2146
		// (get) Token: 0x060021FE RID: 8702
		internal extern int detailResolutionPerPatch { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060021FF RID: 8703
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void ResetDirtyDetails();

		// Token: 0x06002200 RID: 8704
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RefreshPrototypes();

		// Token: 0x17000863 RID: 2147
		// (get) Token: 0x06002201 RID: 8705
		// (set) Token: 0x06002202 RID: 8706
		public extern DetailPrototype[] detailPrototypes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002203 RID: 8707
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int[] GetSupportedLayers(int xBase, int yBase, int totalWidth, int totalHeight);

		// Token: 0x06002204 RID: 8708
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int[,] GetDetailLayer(int xBase, int yBase, int width, int height, int layer);

		// Token: 0x06002205 RID: 8709 RVA: 0x000277F4 File Offset: 0x000259F4
		public void SetDetailLayer(int xBase, int yBase, int layer, int[,] details)
		{
			this.Internal_SetDetailLayer(xBase, yBase, details.GetLength(1), details.GetLength(0), layer, details);
		}

		// Token: 0x06002206 RID: 8710
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetDetailLayer(int xBase, int yBase, int totalWidth, int totalHeight, int detailIndex, int[,] data);

		// Token: 0x17000864 RID: 2148
		// (get) Token: 0x06002207 RID: 8711
		// (set) Token: 0x06002208 RID: 8712
		public extern TreeInstance[] treeInstances { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002209 RID: 8713 RVA: 0x00027814 File Offset: 0x00025A14
		public TreeInstance GetTreeInstance(int index)
		{
			TreeInstance result;
			TerrainData.INTERNAL_CALL_GetTreeInstance(this, index, out result);
			return result;
		}

		// Token: 0x0600220A RID: 8714
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetTreeInstance(TerrainData self, int index, out TreeInstance value);

		// Token: 0x0600220B RID: 8715 RVA: 0x00027834 File Offset: 0x00025A34
		public void SetTreeInstance(int index, TreeInstance instance)
		{
			TerrainData.INTERNAL_CALL_SetTreeInstance(this, index, ref instance);
		}

		// Token: 0x0600220C RID: 8716
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetTreeInstance(TerrainData self, int index, ref TreeInstance instance);

		// Token: 0x17000865 RID: 2149
		// (get) Token: 0x0600220D RID: 8717
		public extern int treeInstanceCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000866 RID: 2150
		// (get) Token: 0x0600220E RID: 8718
		// (set) Token: 0x0600220F RID: 8719
		public extern TreePrototype[] treePrototypes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002210 RID: 8720
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void RemoveTreePrototype(int index);

		// Token: 0x06002211 RID: 8721
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void RecalculateTreePositions();

		// Token: 0x06002212 RID: 8722
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void RemoveDetailPrototype(int index);

		// Token: 0x06002213 RID: 8723
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool NeedUpgradeScaledTreePrototypes();

		// Token: 0x06002214 RID: 8724
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void UpgradeScaledTreePrototype();

		// Token: 0x17000867 RID: 2151
		// (get) Token: 0x06002215 RID: 8725
		public extern int alphamapLayers { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06002216 RID: 8726
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float[,,] GetAlphamaps(int x, int y, int width, int height);

		// Token: 0x17000868 RID: 2152
		// (get) Token: 0x06002217 RID: 8727 RVA: 0x00027840 File Offset: 0x00025A40
		// (set) Token: 0x06002218 RID: 8728 RVA: 0x0002785C File Offset: 0x00025A5C
		public int alphamapResolution
		{
			get
			{
				return this.Internal_alphamapResolution;
			}
			set
			{
				int internal_alphamapResolution = value;
				if (value < TerrainData.kMinimumAlphamapResolution || value > TerrainData.kMaximumAlphamapResolution)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"alphamapResolution is clamped to the range of [",
						TerrainData.kMinimumAlphamapResolution,
						", ",
						TerrainData.kMaximumAlphamapResolution,
						"]."
					}));
					internal_alphamapResolution = Math.Min(TerrainData.kMaximumAlphamapResolution, Math.Max(value, TerrainData.kMinimumAlphamapResolution));
				}
				this.Internal_alphamapResolution = internal_alphamapResolution;
			}
		}

		// Token: 0x17000869 RID: 2153
		// (get) Token: 0x06002219 RID: 8729
		// (set) Token: 0x0600221A RID: 8730
		private extern int Internal_alphamapResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700086A RID: 2154
		// (get) Token: 0x0600221B RID: 8731 RVA: 0x000278E4 File Offset: 0x00025AE4
		public int alphamapWidth
		{
			get
			{
				return this.alphamapResolution;
			}
		}

		// Token: 0x1700086B RID: 2155
		// (get) Token: 0x0600221C RID: 8732 RVA: 0x00027900 File Offset: 0x00025B00
		public int alphamapHeight
		{
			get
			{
				return this.alphamapResolution;
			}
		}

		// Token: 0x1700086C RID: 2156
		// (get) Token: 0x0600221D RID: 8733 RVA: 0x0002791C File Offset: 0x00025B1C
		// (set) Token: 0x0600221E RID: 8734 RVA: 0x00027938 File Offset: 0x00025B38
		public int baseMapResolution
		{
			get
			{
				return this.Internal_baseMapResolution;
			}
			set
			{
				int internal_baseMapResolution = value;
				if (value < TerrainData.kMinimumBaseMapResolution || value > TerrainData.kMaximumBaseMapResolution)
				{
					Debug.LogWarning(string.Concat(new object[]
					{
						"baseMapResolution is clamped to the range of [",
						TerrainData.kMinimumBaseMapResolution,
						", ",
						TerrainData.kMaximumBaseMapResolution,
						"]."
					}));
					internal_baseMapResolution = Math.Min(TerrainData.kMaximumBaseMapResolution, Math.Max(value, TerrainData.kMinimumBaseMapResolution));
				}
				this.Internal_baseMapResolution = internal_baseMapResolution;
			}
		}

		// Token: 0x1700086D RID: 2157
		// (get) Token: 0x0600221F RID: 8735
		// (set) Token: 0x06002220 RID: 8736
		private extern int Internal_baseMapResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002221 RID: 8737 RVA: 0x000279C0 File Offset: 0x00025BC0
		public void SetAlphamaps(int x, int y, float[,,] map)
		{
			if (map.GetLength(2) != this.alphamapLayers)
			{
				throw new Exception(UnityString.Format("Float array size wrong (layers should be {0})", new object[]
				{
					this.alphamapLayers
				}));
			}
			this.Internal_SetAlphamaps(x, y, map.GetLength(1), map.GetLength(0), map);
		}

		// Token: 0x06002222 RID: 8738
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetAlphamaps(int x, int y, int width, int height, float[,,] map);

		// Token: 0x06002223 RID: 8739
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void RecalculateBasemapIfDirty();

		// Token: 0x06002224 RID: 8740
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetBasemapDirty(bool dirty);

		// Token: 0x06002225 RID: 8741
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Texture2D GetAlphamapTexture(int index);

		// Token: 0x1700086E RID: 2158
		// (get) Token: 0x06002226 RID: 8742
		private extern int alphamapTextureCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700086F RID: 2159
		// (get) Token: 0x06002227 RID: 8743 RVA: 0x00027A1C File Offset: 0x00025C1C
		public Texture2D[] alphamapTextures
		{
			get
			{
				Texture2D[] array = new Texture2D[this.alphamapTextureCount];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = this.GetAlphamapTexture(i);
				}
				return array;
			}
		}

		// Token: 0x17000870 RID: 2160
		// (get) Token: 0x06002228 RID: 8744
		// (set) Token: 0x06002229 RID: 8745
		public extern SplatPrototype[] splatPrototypes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600222A RID: 8746
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void AddTree(out TreeInstance tree);

		// Token: 0x0600222B RID: 8747 RVA: 0x00027A5C File Offset: 0x00025C5C
		internal int RemoveTrees(Vector2 position, float radius, int prototypeIndex)
		{
			return TerrainData.INTERNAL_CALL_RemoveTrees(this, ref position, radius, prototypeIndex);
		}

		// Token: 0x0600222C RID: 8748
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_RemoveTrees(TerrainData self, ref Vector2 position, float radius, int prototypeIndex);

		// Token: 0x040005D6 RID: 1494
		private static readonly int kMaximumResolution = TerrainData.Internal_GetMaximumResolution();

		// Token: 0x040005D7 RID: 1495
		private static readonly int kMinimumDetailResolutionPerPatch = TerrainData.Internal_GetMinimumDetailResolutionPerPatch();

		// Token: 0x040005D8 RID: 1496
		private static readonly int kMaximumDetailResolutionPerPatch = TerrainData.Internal_GetMaximumDetailResolutionPerPatch();

		// Token: 0x040005D9 RID: 1497
		private static readonly int kMaximumDetailPatchCount = TerrainData.Internal_GetMaximumDetailPatchCount();

		// Token: 0x040005DA RID: 1498
		private static readonly int kMinimumAlphamapResolution = TerrainData.Internal_GetMinimumAlphamapResolution();

		// Token: 0x040005DB RID: 1499
		private static readonly int kMaximumAlphamapResolution = TerrainData.Internal_GetMaximumAlphamapResolution();

		// Token: 0x040005DC RID: 1500
		private static readonly int kMinimumBaseMapResolution = TerrainData.Internal_GetMinimumBaseMapResolution();

		// Token: 0x040005DD RID: 1501
		private static readonly int kMaximumBaseMapResolution = TerrainData.Internal_GetMaximumBaseMapResolution();
	}
}
