﻿using System;

namespace UnityEngine
{
	// Token: 0x020002BB RID: 699
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
	public sealed class RequireComponent : Attribute
	{
		// Token: 0x06002C32 RID: 11314 RVA: 0x00045018 File Offset: 0x00043218
		public RequireComponent(Type requiredComponent)
		{
			this.m_Type0 = requiredComponent;
		}

		// Token: 0x06002C33 RID: 11315 RVA: 0x00045028 File Offset: 0x00043228
		public RequireComponent(Type requiredComponent, Type requiredComponent2)
		{
			this.m_Type0 = requiredComponent;
			this.m_Type1 = requiredComponent2;
		}

		// Token: 0x06002C34 RID: 11316 RVA: 0x00045040 File Offset: 0x00043240
		public RequireComponent(Type requiredComponent, Type requiredComponent2, Type requiredComponent3)
		{
			this.m_Type0 = requiredComponent;
			this.m_Type1 = requiredComponent2;
			this.m_Type2 = requiredComponent3;
		}

		// Token: 0x04000A38 RID: 2616
		public Type m_Type0;

		// Token: 0x04000A39 RID: 2617
		public Type m_Type1;

		// Token: 0x04000A3A RID: 2618
		public Type m_Type2;
	}
}
