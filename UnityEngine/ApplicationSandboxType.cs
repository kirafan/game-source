﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000F RID: 15
	public enum ApplicationSandboxType
	{
		// Token: 0x04000022 RID: 34
		Unknown,
		// Token: 0x04000023 RID: 35
		NotSandboxed,
		// Token: 0x04000024 RID: 36
		Sandboxed,
		// Token: 0x04000025 RID: 37
		SandboxBroken
	}
}
