﻿using System;

namespace UnityEngine
{
	// Token: 0x0200015F RID: 351
	public enum RigidbodyType2D
	{
		// Token: 0x040003E5 RID: 997
		Dynamic,
		// Token: 0x040003E6 RID: 998
		Kinematic,
		// Token: 0x040003E7 RID: 999
		Static
	}
}
