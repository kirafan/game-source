﻿using System;

namespace UnityEngine
{
	// Token: 0x02000230 RID: 560
	internal enum PlatformSelection
	{
		// Token: 0x04000828 RID: 2088
		Native,
		// Token: 0x04000829 RID: 2089
		Mac,
		// Token: 0x0400082A RID: 2090
		Windows
	}
}
