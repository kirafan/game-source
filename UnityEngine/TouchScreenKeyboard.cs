﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x0200006B RID: 107
	public sealed class TouchScreenKeyboard
	{
		// Token: 0x06000748 RID: 1864 RVA: 0x000095D8 File Offset: 0x000077D8
		public TouchScreenKeyboard(string text, TouchScreenKeyboardType keyboardType, bool autocorrection, bool multiline, bool secure, bool alert, string textPlaceholder)
		{
			TouchScreenKeyboard_InternalConstructorHelperArguments touchScreenKeyboard_InternalConstructorHelperArguments = default(TouchScreenKeyboard_InternalConstructorHelperArguments);
			touchScreenKeyboard_InternalConstructorHelperArguments.keyboardType = Convert.ToUInt32(keyboardType);
			touchScreenKeyboard_InternalConstructorHelperArguments.autocorrection = Convert.ToUInt32(autocorrection);
			touchScreenKeyboard_InternalConstructorHelperArguments.multiline = Convert.ToUInt32(multiline);
			touchScreenKeyboard_InternalConstructorHelperArguments.secure = Convert.ToUInt32(secure);
			touchScreenKeyboard_InternalConstructorHelperArguments.alert = Convert.ToUInt32(alert);
			this.TouchScreenKeyboard_InternalConstructorHelper(ref touchScreenKeyboard_InternalConstructorHelperArguments, text, textPlaceholder);
		}

		// Token: 0x06000749 RID: 1865
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Destroy();

		// Token: 0x0600074A RID: 1866 RVA: 0x00009648 File Offset: 0x00007848
		~TouchScreenKeyboard()
		{
			this.Destroy();
		}

		// Token: 0x0600074B RID: 1867
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void TouchScreenKeyboard_InternalConstructorHelper(ref TouchScreenKeyboard_InternalConstructorHelperArguments arguments, string text, string textPlaceholder);

		// Token: 0x17000198 RID: 408
		// (get) Token: 0x0600074C RID: 1868 RVA: 0x00009678 File Offset: 0x00007878
		public static bool isSupported
		{
			get
			{
				RuntimePlatform platform = Application.platform;
				switch (platform)
				{
				case RuntimePlatform.MetroPlayerX86:
				case RuntimePlatform.MetroPlayerX64:
				case RuntimePlatform.MetroPlayerARM:
					return false;
				default:
					switch (platform)
					{
					case RuntimePlatform.IPhonePlayer:
					case RuntimePlatform.Android:
						break;
					default:
						if (platform != RuntimePlatform.WiiU && platform != RuntimePlatform.tvOS)
						{
							return false;
						}
						break;
					}
					break;
				case RuntimePlatform.TizenPlayer:
				case RuntimePlatform.PSM:
					break;
				}
				return true;
			}
		}

		// Token: 0x0600074D RID: 1869 RVA: 0x000096FC File Offset: 0x000078FC
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType, bool autocorrection, bool multiline, bool secure, bool alert)
		{
			string textPlaceholder = "";
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder);
		}

		// Token: 0x0600074E RID: 1870 RVA: 0x00009728 File Offset: 0x00007928
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType, bool autocorrection, bool multiline, bool secure)
		{
			string textPlaceholder = "";
			bool alert = false;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder);
		}

		// Token: 0x0600074F RID: 1871 RVA: 0x00009754 File Offset: 0x00007954
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType, bool autocorrection, bool multiline)
		{
			string textPlaceholder = "";
			bool alert = false;
			bool secure = false;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder);
		}

		// Token: 0x06000750 RID: 1872 RVA: 0x00009780 File Offset: 0x00007980
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType, bool autocorrection)
		{
			string textPlaceholder = "";
			bool alert = false;
			bool secure = false;
			bool multiline = false;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder);
		}

		// Token: 0x06000751 RID: 1873 RVA: 0x000097B0 File Offset: 0x000079B0
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text, TouchScreenKeyboardType keyboardType)
		{
			string textPlaceholder = "";
			bool alert = false;
			bool secure = false;
			bool multiline = false;
			bool autocorrection = true;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder);
		}

		// Token: 0x06000752 RID: 1874 RVA: 0x000097E4 File Offset: 0x000079E4
		[ExcludeFromDocs]
		public static TouchScreenKeyboard Open(string text)
		{
			string textPlaceholder = "";
			bool alert = false;
			bool secure = false;
			bool multiline = false;
			bool autocorrection = true;
			TouchScreenKeyboardType keyboardType = TouchScreenKeyboardType.Default;
			return TouchScreenKeyboard.Open(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder);
		}

		// Token: 0x06000753 RID: 1875 RVA: 0x0000981C File Offset: 0x00007A1C
		public static TouchScreenKeyboard Open(string text, [DefaultValue("TouchScreenKeyboardType.Default")] TouchScreenKeyboardType keyboardType, [DefaultValue("true")] bool autocorrection, [DefaultValue("false")] bool multiline, [DefaultValue("false")] bool secure, [DefaultValue("false")] bool alert, [DefaultValue("\"\"")] string textPlaceholder)
		{
			return new TouchScreenKeyboard(text, keyboardType, autocorrection, multiline, secure, alert, textPlaceholder);
		}

		// Token: 0x17000199 RID: 409
		// (get) Token: 0x06000754 RID: 1876
		// (set) Token: 0x06000755 RID: 1877
		public extern string text { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700019A RID: 410
		// (get) Token: 0x06000756 RID: 1878
		// (set) Token: 0x06000757 RID: 1879
		public static extern bool hideInput { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700019B RID: 411
		// (get) Token: 0x06000758 RID: 1880
		// (set) Token: 0x06000759 RID: 1881
		public extern bool active { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700019C RID: 412
		// (get) Token: 0x0600075A RID: 1882
		public extern bool done { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700019D RID: 413
		// (get) Token: 0x0600075B RID: 1883
		public extern bool wasCanceled { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700019E RID: 414
		// (get) Token: 0x0600075C RID: 1884
		public extern bool canGetSelection { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700019F RID: 415
		// (get) Token: 0x0600075D RID: 1885 RVA: 0x00009840 File Offset: 0x00007A40
		public RangeInt selection
		{
			get
			{
				RangeInt result;
				this.GetSelectionInternal(out result.start, out result.length);
				return result;
			}
		}

		// Token: 0x0600075E RID: 1886
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetSelectionInternal(out int start, out int length);

		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x0600075F RID: 1887
		// (set) Token: 0x06000760 RID: 1888
		public extern int targetDisplay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x06000761 RID: 1889 RVA: 0x0000986C File Offset: 0x00007A6C
		public static Rect area
		{
			get
			{
				Rect result;
				TouchScreenKeyboard.INTERNAL_get_area(out result);
				return result;
			}
		}

		// Token: 0x06000762 RID: 1890
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_area(out Rect value);

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x06000763 RID: 1891
		public static extern bool visible { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x040000AB RID: 171
		[NonSerialized]
		internal IntPtr m_Ptr;
	}
}
