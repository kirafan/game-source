﻿using System;

namespace UnityEngine
{
	// Token: 0x020002FD RID: 765
	public enum TextureFormat
	{
		// Token: 0x04000B5D RID: 2909
		Alpha8 = 1,
		// Token: 0x04000B5E RID: 2910
		ARGB4444,
		// Token: 0x04000B5F RID: 2911
		RGB24,
		// Token: 0x04000B60 RID: 2912
		RGBA32,
		// Token: 0x04000B61 RID: 2913
		ARGB32,
		// Token: 0x04000B62 RID: 2914
		RGB565 = 7,
		// Token: 0x04000B63 RID: 2915
		R16 = 9,
		// Token: 0x04000B64 RID: 2916
		DXT1,
		// Token: 0x04000B65 RID: 2917
		DXT5 = 12,
		// Token: 0x04000B66 RID: 2918
		RGBA4444,
		// Token: 0x04000B67 RID: 2919
		BGRA32,
		// Token: 0x04000B68 RID: 2920
		RHalf,
		// Token: 0x04000B69 RID: 2921
		RGHalf,
		// Token: 0x04000B6A RID: 2922
		RGBAHalf,
		// Token: 0x04000B6B RID: 2923
		RFloat,
		// Token: 0x04000B6C RID: 2924
		RGFloat,
		// Token: 0x04000B6D RID: 2925
		RGBAFloat,
		// Token: 0x04000B6E RID: 2926
		YUY2,
		// Token: 0x04000B6F RID: 2927
		BC4 = 26,
		// Token: 0x04000B70 RID: 2928
		BC5,
		// Token: 0x04000B71 RID: 2929
		BC6H = 24,
		// Token: 0x04000B72 RID: 2930
		BC7,
		// Token: 0x04000B73 RID: 2931
		DXT1Crunched = 28,
		// Token: 0x04000B74 RID: 2932
		DXT5Crunched,
		// Token: 0x04000B75 RID: 2933
		PVRTC_RGB2,
		// Token: 0x04000B76 RID: 2934
		PVRTC_RGBA2,
		// Token: 0x04000B77 RID: 2935
		PVRTC_RGB4,
		// Token: 0x04000B78 RID: 2936
		PVRTC_RGBA4,
		// Token: 0x04000B79 RID: 2937
		ETC_RGB4,
		// Token: 0x04000B7A RID: 2938
		ATC_RGB4,
		// Token: 0x04000B7B RID: 2939
		ATC_RGBA8,
		// Token: 0x04000B7C RID: 2940
		EAC_R = 41,
		// Token: 0x04000B7D RID: 2941
		EAC_R_SIGNED,
		// Token: 0x04000B7E RID: 2942
		EAC_RG,
		// Token: 0x04000B7F RID: 2943
		EAC_RG_SIGNED,
		// Token: 0x04000B80 RID: 2944
		ETC2_RGB,
		// Token: 0x04000B81 RID: 2945
		ETC2_RGBA1,
		// Token: 0x04000B82 RID: 2946
		ETC2_RGBA8,
		// Token: 0x04000B83 RID: 2947
		ASTC_RGB_4x4,
		// Token: 0x04000B84 RID: 2948
		ASTC_RGB_5x5,
		// Token: 0x04000B85 RID: 2949
		ASTC_RGB_6x6,
		// Token: 0x04000B86 RID: 2950
		ASTC_RGB_8x8,
		// Token: 0x04000B87 RID: 2951
		ASTC_RGB_10x10,
		// Token: 0x04000B88 RID: 2952
		ASTC_RGB_12x12,
		// Token: 0x04000B89 RID: 2953
		ASTC_RGBA_4x4,
		// Token: 0x04000B8A RID: 2954
		ASTC_RGBA_5x5,
		// Token: 0x04000B8B RID: 2955
		ASTC_RGBA_6x6,
		// Token: 0x04000B8C RID: 2956
		ASTC_RGBA_8x8,
		// Token: 0x04000B8D RID: 2957
		ASTC_RGBA_10x10,
		// Token: 0x04000B8E RID: 2958
		ASTC_RGBA_12x12,
		// Token: 0x04000B8F RID: 2959
		ETC_RGB4_3DS,
		// Token: 0x04000B90 RID: 2960
		ETC_RGBA8_3DS
	}
}
