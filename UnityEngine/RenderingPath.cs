﻿using System;

namespace UnityEngine
{
	// Token: 0x020002E3 RID: 739
	public enum RenderingPath
	{
		// Token: 0x04000ADD RID: 2781
		UsePlayerSettings = -1,
		// Token: 0x04000ADE RID: 2782
		VertexLit,
		// Token: 0x04000ADF RID: 2783
		Forward,
		// Token: 0x04000AE0 RID: 2784
		DeferredLighting,
		// Token: 0x04000AE1 RID: 2785
		DeferredShading
	}
}
