﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001D6 RID: 470
	[RequiredByNativeCode]
	public struct HumanBone
	{
		// Token: 0x170007F9 RID: 2041
		// (get) Token: 0x06001FF0 RID: 8176 RVA: 0x000243F4 File Offset: 0x000225F4
		// (set) Token: 0x06001FF1 RID: 8177 RVA: 0x00024410 File Offset: 0x00022610
		public string boneName
		{
			get
			{
				return this.m_BoneName;
			}
			set
			{
				this.m_BoneName = value;
			}
		}

		// Token: 0x170007FA RID: 2042
		// (get) Token: 0x06001FF2 RID: 8178 RVA: 0x0002441C File Offset: 0x0002261C
		// (set) Token: 0x06001FF3 RID: 8179 RVA: 0x00024438 File Offset: 0x00022638
		public string humanName
		{
			get
			{
				return this.m_HumanName;
			}
			set
			{
				this.m_HumanName = value;
			}
		}

		// Token: 0x04000516 RID: 1302
		private string m_BoneName;

		// Token: 0x04000517 RID: 1303
		private string m_HumanName;

		// Token: 0x04000518 RID: 1304
		public HumanLimit limit;
	}
}
