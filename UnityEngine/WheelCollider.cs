﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000186 RID: 390
	public sealed class WheelCollider : Collider
	{
		// Token: 0x1700069F RID: 1695
		// (get) Token: 0x06001BB9 RID: 7097 RVA: 0x00021ACC File Offset: 0x0001FCCC
		// (set) Token: 0x06001BBA RID: 7098 RVA: 0x00021AEC File Offset: 0x0001FCEC
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_center(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_center(ref value);
			}
		}

		// Token: 0x06001BBB RID: 7099
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_center(out Vector3 value);

		// Token: 0x06001BBC RID: 7100
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_center(ref Vector3 value);

		// Token: 0x170006A0 RID: 1696
		// (get) Token: 0x06001BBD RID: 7101
		// (set) Token: 0x06001BBE RID: 7102
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006A1 RID: 1697
		// (get) Token: 0x06001BBF RID: 7103
		// (set) Token: 0x06001BC0 RID: 7104
		public extern float suspensionDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006A2 RID: 1698
		// (get) Token: 0x06001BC1 RID: 7105 RVA: 0x00021AF8 File Offset: 0x0001FCF8
		// (set) Token: 0x06001BC2 RID: 7106 RVA: 0x00021B18 File Offset: 0x0001FD18
		public JointSpring suspensionSpring
		{
			get
			{
				JointSpring result;
				this.INTERNAL_get_suspensionSpring(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_suspensionSpring(ref value);
			}
		}

		// Token: 0x06001BC3 RID: 7107
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_suspensionSpring(out JointSpring value);

		// Token: 0x06001BC4 RID: 7108
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_suspensionSpring(ref JointSpring value);

		// Token: 0x170006A3 RID: 1699
		// (get) Token: 0x06001BC5 RID: 7109
		// (set) Token: 0x06001BC6 RID: 7110
		public extern float forceAppPointDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006A4 RID: 1700
		// (get) Token: 0x06001BC7 RID: 7111
		// (set) Token: 0x06001BC8 RID: 7112
		public extern float mass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006A5 RID: 1701
		// (get) Token: 0x06001BC9 RID: 7113
		// (set) Token: 0x06001BCA RID: 7114
		public extern float wheelDampingRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006A6 RID: 1702
		// (get) Token: 0x06001BCB RID: 7115 RVA: 0x00021B24 File Offset: 0x0001FD24
		// (set) Token: 0x06001BCC RID: 7116 RVA: 0x00021B44 File Offset: 0x0001FD44
		public WheelFrictionCurve forwardFriction
		{
			get
			{
				WheelFrictionCurve result;
				this.INTERNAL_get_forwardFriction(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_forwardFriction(ref value);
			}
		}

		// Token: 0x06001BCD RID: 7117
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_forwardFriction(out WheelFrictionCurve value);

		// Token: 0x06001BCE RID: 7118
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_forwardFriction(ref WheelFrictionCurve value);

		// Token: 0x170006A7 RID: 1703
		// (get) Token: 0x06001BCF RID: 7119 RVA: 0x00021B50 File Offset: 0x0001FD50
		// (set) Token: 0x06001BD0 RID: 7120 RVA: 0x00021B70 File Offset: 0x0001FD70
		public WheelFrictionCurve sidewaysFriction
		{
			get
			{
				WheelFrictionCurve result;
				this.INTERNAL_get_sidewaysFriction(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_sidewaysFriction(ref value);
			}
		}

		// Token: 0x06001BD1 RID: 7121
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_sidewaysFriction(out WheelFrictionCurve value);

		// Token: 0x06001BD2 RID: 7122
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_sidewaysFriction(ref WheelFrictionCurve value);

		// Token: 0x170006A8 RID: 1704
		// (get) Token: 0x06001BD3 RID: 7123
		// (set) Token: 0x06001BD4 RID: 7124
		public extern float motorTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006A9 RID: 1705
		// (get) Token: 0x06001BD5 RID: 7125
		// (set) Token: 0x06001BD6 RID: 7126
		public extern float brakeTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006AA RID: 1706
		// (get) Token: 0x06001BD7 RID: 7127
		// (set) Token: 0x06001BD8 RID: 7128
		public extern float steerAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006AB RID: 1707
		// (get) Token: 0x06001BD9 RID: 7129
		public extern bool isGrounded { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001BDA RID: 7130
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ConfigureVehicleSubsteps(float speedThreshold, int stepsBelowThreshold, int stepsAboveThreshold);

		// Token: 0x170006AC RID: 1708
		// (get) Token: 0x06001BDB RID: 7131
		public extern float sprungMass { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001BDC RID: 7132
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetGroundHit(out WheelHit hit);

		// Token: 0x06001BDD RID: 7133
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GetWorldPose(out Vector3 pos, out Quaternion quat);

		// Token: 0x170006AD RID: 1709
		// (get) Token: 0x06001BDE RID: 7134
		public extern float rpm { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
