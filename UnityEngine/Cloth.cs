﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000189 RID: 393
	public sealed class Cloth : Component
	{
		// Token: 0x170006B0 RID: 1712
		// (get) Token: 0x06001BE6 RID: 7142
		// (set) Token: 0x06001BE7 RID: 7143
		public extern float sleepThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006B1 RID: 1713
		// (get) Token: 0x06001BE8 RID: 7144
		// (set) Token: 0x06001BE9 RID: 7145
		public extern float bendingStiffness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006B2 RID: 1714
		// (get) Token: 0x06001BEA RID: 7146
		// (set) Token: 0x06001BEB RID: 7147
		public extern float stretchingStiffness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006B3 RID: 1715
		// (get) Token: 0x06001BEC RID: 7148
		// (set) Token: 0x06001BED RID: 7149
		public extern float damping { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006B4 RID: 1716
		// (get) Token: 0x06001BEE RID: 7150 RVA: 0x00021C14 File Offset: 0x0001FE14
		// (set) Token: 0x06001BEF RID: 7151 RVA: 0x00021C34 File Offset: 0x0001FE34
		public Vector3 externalAcceleration
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_externalAcceleration(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_externalAcceleration(ref value);
			}
		}

		// Token: 0x06001BF0 RID: 7152
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_externalAcceleration(out Vector3 value);

		// Token: 0x06001BF1 RID: 7153
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_externalAcceleration(ref Vector3 value);

		// Token: 0x170006B5 RID: 1717
		// (get) Token: 0x06001BF2 RID: 7154 RVA: 0x00021C40 File Offset: 0x0001FE40
		// (set) Token: 0x06001BF3 RID: 7155 RVA: 0x00021C60 File Offset: 0x0001FE60
		public Vector3 randomAcceleration
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_randomAcceleration(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_randomAcceleration(ref value);
			}
		}

		// Token: 0x06001BF4 RID: 7156
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_randomAcceleration(out Vector3 value);

		// Token: 0x06001BF5 RID: 7157
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_randomAcceleration(ref Vector3 value);

		// Token: 0x170006B6 RID: 1718
		// (get) Token: 0x06001BF6 RID: 7158
		// (set) Token: 0x06001BF7 RID: 7159
		public extern bool useGravity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006B7 RID: 1719
		// (get) Token: 0x06001BF8 RID: 7160
		// (set) Token: 0x06001BF9 RID: 7161
		[Obsolete("Deprecated. Cloth.selfCollisions is no longer supported since Unity 5.0.", true)]
		public extern bool selfCollision { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006B8 RID: 1720
		// (get) Token: 0x06001BFA RID: 7162
		// (set) Token: 0x06001BFB RID: 7163
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006B9 RID: 1721
		// (get) Token: 0x06001BFC RID: 7164
		public extern Vector3[] vertices { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170006BA RID: 1722
		// (get) Token: 0x06001BFD RID: 7165
		public extern Vector3[] normals { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170006BB RID: 1723
		// (get) Token: 0x06001BFE RID: 7166
		// (set) Token: 0x06001BFF RID: 7167
		public extern float friction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006BC RID: 1724
		// (get) Token: 0x06001C00 RID: 7168
		// (set) Token: 0x06001C01 RID: 7169
		public extern float collisionMassScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006BD RID: 1725
		// (get) Token: 0x06001C02 RID: 7170
		// (set) Token: 0x06001C03 RID: 7171
		public extern float useContinuousCollision { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006BE RID: 1726
		// (get) Token: 0x06001C04 RID: 7172
		// (set) Token: 0x06001C05 RID: 7173
		public extern float useVirtualParticles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001C06 RID: 7174 RVA: 0x00021C6C File Offset: 0x0001FE6C
		public void ClearTransformMotion()
		{
			Cloth.INTERNAL_CALL_ClearTransformMotion(this);
		}

		// Token: 0x06001C07 RID: 7175
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ClearTransformMotion(Cloth self);

		// Token: 0x170006BF RID: 1727
		// (get) Token: 0x06001C08 RID: 7176
		// (set) Token: 0x06001C09 RID: 7177
		public extern ClothSkinningCoefficient[] coefficients { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006C0 RID: 1728
		// (get) Token: 0x06001C0A RID: 7178
		// (set) Token: 0x06001C0B RID: 7179
		public extern float worldVelocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006C1 RID: 1729
		// (get) Token: 0x06001C0C RID: 7180
		// (set) Token: 0x06001C0D RID: 7181
		public extern float worldAccelerationScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001C0E RID: 7182
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetEnabledFading(bool enabled, [DefaultValue("0.5f")] float interpolationTime);

		// Token: 0x06001C0F RID: 7183 RVA: 0x00021C78 File Offset: 0x0001FE78
		[ExcludeFromDocs]
		public void SetEnabledFading(bool enabled)
		{
			float interpolationTime = 0.5f;
			this.SetEnabledFading(enabled, interpolationTime);
		}

		// Token: 0x170006C2 RID: 1730
		// (get) Token: 0x06001C10 RID: 7184
		// (set) Token: 0x06001C11 RID: 7185
		public extern bool solverFrequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006C3 RID: 1731
		// (get) Token: 0x06001C12 RID: 7186
		// (set) Token: 0x06001C13 RID: 7187
		public extern CapsuleCollider[] capsuleColliders { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006C4 RID: 1732
		// (get) Token: 0x06001C14 RID: 7188
		// (set) Token: 0x06001C15 RID: 7189
		public extern ClothSphereColliderPair[] sphereColliders { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
