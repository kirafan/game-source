﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E2 RID: 482
	internal enum HumanParameter
	{
		// Token: 0x040005A4 RID: 1444
		UpperArmTwist,
		// Token: 0x040005A5 RID: 1445
		LowerArmTwist,
		// Token: 0x040005A6 RID: 1446
		UpperLegTwist,
		// Token: 0x040005A7 RID: 1447
		LowerLegTwist,
		// Token: 0x040005A8 RID: 1448
		ArmStretch,
		// Token: 0x040005A9 RID: 1449
		LegStretch,
		// Token: 0x040005AA RID: 1450
		FeetSpacing
	}
}
