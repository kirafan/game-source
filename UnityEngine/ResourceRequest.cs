﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020000AF RID: 175
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class ResourceRequest : AsyncOperation
	{
		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x06000C35 RID: 3125 RVA: 0x00010E94 File Offset: 0x0000F094
		public Object asset
		{
			get
			{
				return Resources.Load(this.m_Path, this.m_Type);
			}
		}

		// Token: 0x0400019E RID: 414
		internal string m_Path;

		// Token: 0x0400019F RID: 415
		internal Type m_Type;
	}
}
