﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001A2 RID: 418
	public sealed class AudioListener : Behaviour
	{
		// Token: 0x17000717 RID: 1815
		// (get) Token: 0x06001D13 RID: 7443
		// (set) Token: 0x06001D14 RID: 7444
		public static extern float volume { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000718 RID: 1816
		// (get) Token: 0x06001D15 RID: 7445
		// (set) Token: 0x06001D16 RID: 7446
		public static extern bool pause { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000719 RID: 1817
		// (get) Token: 0x06001D17 RID: 7447
		// (set) Token: 0x06001D18 RID: 7448
		public extern AudioVelocityUpdateMode velocityUpdateMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001D19 RID: 7449
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetOutputDataHelper(float[] samples, int channel);

		// Token: 0x06001D1A RID: 7450
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetSpectrumDataHelper(float[] samples, int channel, FFTWindow window);

		// Token: 0x06001D1B RID: 7451 RVA: 0x000225EC File Offset: 0x000207EC
		[Obsolete("GetOutputData returning a float[] is deprecated, use GetOutputData and pass a pre allocated array instead.")]
		public static float[] GetOutputData(int numSamples, int channel)
		{
			float[] array = new float[numSamples];
			AudioListener.GetOutputDataHelper(array, channel);
			return array;
		}

		// Token: 0x06001D1C RID: 7452 RVA: 0x00022610 File Offset: 0x00020810
		public static void GetOutputData(float[] samples, int channel)
		{
			AudioListener.GetOutputDataHelper(samples, channel);
		}

		// Token: 0x06001D1D RID: 7453 RVA: 0x0002261C File Offset: 0x0002081C
		[Obsolete("GetSpectrumData returning a float[] is deprecated, use GetOutputData and pass a pre allocated array instead.")]
		public static float[] GetSpectrumData(int numSamples, int channel, FFTWindow window)
		{
			float[] array = new float[numSamples];
			AudioListener.GetSpectrumDataHelper(array, channel, window);
			return array;
		}

		// Token: 0x06001D1E RID: 7454 RVA: 0x00022644 File Offset: 0x00020844
		public static void GetSpectrumData(float[] samples, int channel, FFTWindow window)
		{
			AudioListener.GetSpectrumDataHelper(samples, channel, window);
		}
	}
}
