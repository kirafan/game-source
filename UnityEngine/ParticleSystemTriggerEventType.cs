﻿using System;

namespace UnityEngine
{
	// Token: 0x02000108 RID: 264
	public enum ParticleSystemTriggerEventType
	{
		// Token: 0x040002DA RID: 730
		Inside,
		// Token: 0x040002DB RID: 731
		Outside,
		// Token: 0x040002DC RID: 732
		Enter,
		// Token: 0x040002DD RID: 733
		Exit
	}
}
