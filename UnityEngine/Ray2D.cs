﻿using System;

namespace UnityEngine
{
	// Token: 0x02000363 RID: 867
	public struct Ray2D
	{
		// Token: 0x06002DF0 RID: 11760 RVA: 0x00049118 File Offset: 0x00047318
		public Ray2D(Vector2 origin, Vector2 direction)
		{
			this.m_Origin = origin;
			this.m_Direction = direction.normalized;
		}

		// Token: 0x17000AE2 RID: 2786
		// (get) Token: 0x06002DF1 RID: 11761 RVA: 0x00049130 File Offset: 0x00047330
		// (set) Token: 0x06002DF2 RID: 11762 RVA: 0x0004914C File Offset: 0x0004734C
		public Vector2 origin
		{
			get
			{
				return this.m_Origin;
			}
			set
			{
				this.m_Origin = value;
			}
		}

		// Token: 0x17000AE3 RID: 2787
		// (get) Token: 0x06002DF3 RID: 11763 RVA: 0x00049158 File Offset: 0x00047358
		// (set) Token: 0x06002DF4 RID: 11764 RVA: 0x00049174 File Offset: 0x00047374
		public Vector2 direction
		{
			get
			{
				return this.m_Direction;
			}
			set
			{
				this.m_Direction = value.normalized;
			}
		}

		// Token: 0x06002DF5 RID: 11765 RVA: 0x00049184 File Offset: 0x00047384
		public Vector2 GetPoint(float distance)
		{
			return this.m_Origin + this.m_Direction * distance;
		}

		// Token: 0x06002DF6 RID: 11766 RVA: 0x000491B0 File Offset: 0x000473B0
		public override string ToString()
		{
			return UnityString.Format("Origin: {0}, Dir: {1}", new object[]
			{
				this.m_Origin,
				this.m_Direction
			});
		}

		// Token: 0x06002DF7 RID: 11767 RVA: 0x000491F4 File Offset: 0x000473F4
		public string ToString(string format)
		{
			return UnityString.Format("Origin: {0}, Dir: {1}", new object[]
			{
				this.m_Origin.ToString(format),
				this.m_Direction.ToString(format)
			});
		}

		// Token: 0x04000D3A RID: 3386
		private Vector2 m_Origin;

		// Token: 0x04000D3B RID: 3387
		private Vector2 m_Direction;
	}
}
