﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000175 RID: 373
	public sealed class RelativeJoint2D : Joint2D
	{
		// Token: 0x1700064B RID: 1611
		// (get) Token: 0x06001AF5 RID: 6901
		// (set) Token: 0x06001AF6 RID: 6902
		public extern float maxForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700064C RID: 1612
		// (get) Token: 0x06001AF7 RID: 6903
		// (set) Token: 0x06001AF8 RID: 6904
		public extern float maxTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700064D RID: 1613
		// (get) Token: 0x06001AF9 RID: 6905
		// (set) Token: 0x06001AFA RID: 6906
		public extern float correctionScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700064E RID: 1614
		// (get) Token: 0x06001AFB RID: 6907
		// (set) Token: 0x06001AFC RID: 6908
		public extern bool autoConfigureOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700064F RID: 1615
		// (get) Token: 0x06001AFD RID: 6909 RVA: 0x0002171C File Offset: 0x0001F91C
		// (set) Token: 0x06001AFE RID: 6910 RVA: 0x0002173C File Offset: 0x0001F93C
		public Vector2 linearOffset
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_linearOffset(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_linearOffset(ref value);
			}
		}

		// Token: 0x06001AFF RID: 6911
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_linearOffset(out Vector2 value);

		// Token: 0x06001B00 RID: 6912
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_linearOffset(ref Vector2 value);

		// Token: 0x17000650 RID: 1616
		// (get) Token: 0x06001B01 RID: 6913
		// (set) Token: 0x06001B02 RID: 6914
		public extern float angularOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000651 RID: 1617
		// (get) Token: 0x06001B03 RID: 6915 RVA: 0x00021748 File Offset: 0x0001F948
		public Vector2 target
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_target(out result);
				return result;
			}
		}

		// Token: 0x06001B04 RID: 6916
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_target(out Vector2 value);
	}
}
