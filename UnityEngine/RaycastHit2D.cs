﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000157 RID: 343
	[UsedByNativeCode]
	public struct RaycastHit2D
	{
		// Token: 0x170005D0 RID: 1488
		// (get) Token: 0x060018DE RID: 6366 RVA: 0x0001EEB4 File Offset: 0x0001D0B4
		// (set) Token: 0x060018DF RID: 6367 RVA: 0x0001EED0 File Offset: 0x0001D0D0
		public Vector2 centroid
		{
			get
			{
				return this.m_Centroid;
			}
			set
			{
				this.m_Centroid = value;
			}
		}

		// Token: 0x170005D1 RID: 1489
		// (get) Token: 0x060018E0 RID: 6368 RVA: 0x0001EEDC File Offset: 0x0001D0DC
		// (set) Token: 0x060018E1 RID: 6369 RVA: 0x0001EEF8 File Offset: 0x0001D0F8
		public Vector2 point
		{
			get
			{
				return this.m_Point;
			}
			set
			{
				this.m_Point = value;
			}
		}

		// Token: 0x170005D2 RID: 1490
		// (get) Token: 0x060018E2 RID: 6370 RVA: 0x0001EF04 File Offset: 0x0001D104
		// (set) Token: 0x060018E3 RID: 6371 RVA: 0x0001EF20 File Offset: 0x0001D120
		public Vector2 normal
		{
			get
			{
				return this.m_Normal;
			}
			set
			{
				this.m_Normal = value;
			}
		}

		// Token: 0x170005D3 RID: 1491
		// (get) Token: 0x060018E4 RID: 6372 RVA: 0x0001EF2C File Offset: 0x0001D12C
		// (set) Token: 0x060018E5 RID: 6373 RVA: 0x0001EF48 File Offset: 0x0001D148
		public float distance
		{
			get
			{
				return this.m_Distance;
			}
			set
			{
				this.m_Distance = value;
			}
		}

		// Token: 0x170005D4 RID: 1492
		// (get) Token: 0x060018E6 RID: 6374 RVA: 0x0001EF54 File Offset: 0x0001D154
		// (set) Token: 0x060018E7 RID: 6375 RVA: 0x0001EF70 File Offset: 0x0001D170
		public float fraction
		{
			get
			{
				return this.m_Fraction;
			}
			set
			{
				this.m_Fraction = value;
			}
		}

		// Token: 0x170005D5 RID: 1493
		// (get) Token: 0x060018E8 RID: 6376 RVA: 0x0001EF7C File Offset: 0x0001D17C
		public Collider2D collider
		{
			get
			{
				return this.m_Collider;
			}
		}

		// Token: 0x170005D6 RID: 1494
		// (get) Token: 0x060018E9 RID: 6377 RVA: 0x0001EF98 File Offset: 0x0001D198
		public Rigidbody2D rigidbody
		{
			get
			{
				return (!(this.collider != null)) ? null : this.collider.attachedRigidbody;
			}
		}

		// Token: 0x170005D7 RID: 1495
		// (get) Token: 0x060018EA RID: 6378 RVA: 0x0001EFD0 File Offset: 0x0001D1D0
		public Transform transform
		{
			get
			{
				Rigidbody2D rigidbody = this.rigidbody;
				Transform result;
				if (rigidbody != null)
				{
					result = rigidbody.transform;
				}
				else if (this.collider != null)
				{
					result = this.collider.transform;
				}
				else
				{
					result = null;
				}
				return result;
			}
		}

		// Token: 0x060018EB RID: 6379 RVA: 0x0001F028 File Offset: 0x0001D228
		public static implicit operator bool(RaycastHit2D hit)
		{
			return hit.collider != null;
		}

		// Token: 0x060018EC RID: 6380 RVA: 0x0001F04C File Offset: 0x0001D24C
		public int CompareTo(RaycastHit2D other)
		{
			int result;
			if (this.collider == null)
			{
				result = 1;
			}
			else if (other.collider == null)
			{
				result = -1;
			}
			else
			{
				result = this.fraction.CompareTo(other.fraction);
			}
			return result;
		}

		// Token: 0x040003C0 RID: 960
		private Vector2 m_Centroid;

		// Token: 0x040003C1 RID: 961
		private Vector2 m_Point;

		// Token: 0x040003C2 RID: 962
		private Vector2 m_Normal;

		// Token: 0x040003C3 RID: 963
		private float m_Distance;

		// Token: 0x040003C4 RID: 964
		private float m_Fraction;

		// Token: 0x040003C5 RID: 965
		private Collider2D m_Collider;
	}
}
