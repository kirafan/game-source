﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000A4 RID: 164
	public sealed class Random
	{
		// Token: 0x17000294 RID: 660
		// (get) Token: 0x06000B41 RID: 2881
		// (set) Token: 0x06000B42 RID: 2882
		[Obsolete("Deprecated. Use InitState() function or Random.state property instead.")]
		public static extern int seed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000B43 RID: 2883
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void InitState(int seed);

		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06000B44 RID: 2884 RVA: 0x0000FC50 File Offset: 0x0000DE50
		// (set) Token: 0x06000B45 RID: 2885 RVA: 0x0000FC70 File Offset: 0x0000DE70
		public static Random.State state
		{
			get
			{
				Random.State result;
				Random.INTERNAL_get_state(out result);
				return result;
			}
			set
			{
				Random.INTERNAL_set_state(ref value);
			}
		}

		// Token: 0x06000B46 RID: 2886
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_state(out Random.State value);

		// Token: 0x06000B47 RID: 2887
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_state(ref Random.State value);

		// Token: 0x06000B48 RID: 2888
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float Range(float min, float max);

		// Token: 0x06000B49 RID: 2889 RVA: 0x0000FC7C File Offset: 0x0000DE7C
		public static int Range(int min, int max)
		{
			return Random.RandomRangeInt(min, max);
		}

		// Token: 0x06000B4A RID: 2890
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int RandomRangeInt(int min, int max);

		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06000B4B RID: 2891
		public static extern float value { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06000B4C RID: 2892 RVA: 0x0000FC98 File Offset: 0x0000DE98
		public static Vector3 insideUnitSphere
		{
			get
			{
				Vector3 result;
				Random.INTERNAL_get_insideUnitSphere(out result);
				return result;
			}
		}

		// Token: 0x06000B4D RID: 2893
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_insideUnitSphere(out Vector3 value);

		// Token: 0x06000B4E RID: 2894
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRandomUnitCircle(out Vector2 output);

		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06000B4F RID: 2895 RVA: 0x0000FCB8 File Offset: 0x0000DEB8
		public static Vector2 insideUnitCircle
		{
			get
			{
				Vector2 result;
				Random.GetRandomUnitCircle(out result);
				return result;
			}
		}

		// Token: 0x17000299 RID: 665
		// (get) Token: 0x06000B50 RID: 2896 RVA: 0x0000FCD8 File Offset: 0x0000DED8
		public static Vector3 onUnitSphere
		{
			get
			{
				Vector3 result;
				Random.INTERNAL_get_onUnitSphere(out result);
				return result;
			}
		}

		// Token: 0x06000B51 RID: 2897
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_onUnitSphere(out Vector3 value);

		// Token: 0x1700029A RID: 666
		// (get) Token: 0x06000B52 RID: 2898 RVA: 0x0000FCF8 File Offset: 0x0000DEF8
		public static Quaternion rotation
		{
			get
			{
				Quaternion result;
				Random.INTERNAL_get_rotation(out result);
				return result;
			}
		}

		// Token: 0x06000B53 RID: 2899
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_rotation(out Quaternion value);

		// Token: 0x1700029B RID: 667
		// (get) Token: 0x06000B54 RID: 2900 RVA: 0x0000FD18 File Offset: 0x0000DF18
		public static Quaternion rotationUniform
		{
			get
			{
				Quaternion result;
				Random.INTERNAL_get_rotationUniform(out result);
				return result;
			}
		}

		// Token: 0x06000B55 RID: 2901
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_rotationUniform(out Quaternion value);

		// Token: 0x06000B56 RID: 2902 RVA: 0x0000FD38 File Offset: 0x0000DF38
		[Obsolete("Use Random.Range instead")]
		public static float RandomRange(float min, float max)
		{
			return Random.Range(min, max);
		}

		// Token: 0x06000B57 RID: 2903 RVA: 0x0000FD54 File Offset: 0x0000DF54
		[Obsolete("Use Random.Range instead")]
		public static int RandomRange(int min, int max)
		{
			return Random.Range(min, max);
		}

		// Token: 0x06000B58 RID: 2904 RVA: 0x0000FD70 File Offset: 0x0000DF70
		public static Color ColorHSV()
		{
			return Random.ColorHSV(0f, 1f, 0f, 1f, 0f, 1f, 1f, 1f);
		}

		// Token: 0x06000B59 RID: 2905 RVA: 0x0000FDB4 File Offset: 0x0000DFB4
		public static Color ColorHSV(float hueMin, float hueMax)
		{
			return Random.ColorHSV(hueMin, hueMax, 0f, 1f, 0f, 1f, 1f, 1f);
		}

		// Token: 0x06000B5A RID: 2906 RVA: 0x0000FDF0 File Offset: 0x0000DFF0
		public static Color ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax)
		{
			return Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax, 0f, 1f, 1f, 1f);
		}

		// Token: 0x06000B5B RID: 2907 RVA: 0x0000FE24 File Offset: 0x0000E024
		public static Color ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax, float valueMin, float valueMax)
		{
			return Random.ColorHSV(hueMin, hueMax, saturationMin, saturationMax, valueMin, valueMax, 1f, 1f);
		}

		// Token: 0x06000B5C RID: 2908 RVA: 0x0000FE50 File Offset: 0x0000E050
		public static Color ColorHSV(float hueMin, float hueMax, float saturationMin, float saturationMax, float valueMin, float valueMax, float alphaMin, float alphaMax)
		{
			float h = Mathf.Lerp(hueMin, hueMax, Random.value);
			float s = Mathf.Lerp(saturationMin, saturationMax, Random.value);
			float v = Mathf.Lerp(valueMin, valueMax, Random.value);
			Color result = Color.HSVToRGB(h, s, v, true);
			result.a = Mathf.Lerp(alphaMin, alphaMax, Random.value);
			return result;
		}

		// Token: 0x020000A5 RID: 165
		[Serializable]
		public struct State
		{
			// Token: 0x04000176 RID: 374
			[SerializeField]
			private int s0;

			// Token: 0x04000177 RID: 375
			[SerializeField]
			private int s1;

			// Token: 0x04000178 RID: 376
			[SerializeField]
			private int s2;

			// Token: 0x04000179 RID: 377
			[SerializeField]
			private int s3;
		}
	}
}
