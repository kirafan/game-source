﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020002C2 RID: 706
	[UsedByNativeCode]
	[AttributeUsage(AttributeTargets.Class)]
	public class DefaultExecutionOrder : Attribute
	{
		// Token: 0x06002C48 RID: 11336 RVA: 0x000451CC File Offset: 0x000433CC
		public DefaultExecutionOrder(int order)
		{
			this.order = order;
		}

		// Token: 0x17000A6F RID: 2671
		// (get) Token: 0x06002C49 RID: 11337 RVA: 0x000451DC File Offset: 0x000433DC
		// (set) Token: 0x06002C4A RID: 11338 RVA: 0x000451F8 File Offset: 0x000433F8
		public int order { get; private set; }
	}
}
