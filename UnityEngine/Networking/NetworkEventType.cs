﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000279 RID: 633
	public enum NetworkEventType
	{
		// Token: 0x0400098D RID: 2445
		DataEvent,
		// Token: 0x0400098E RID: 2446
		ConnectEvent,
		// Token: 0x0400098F RID: 2447
		DisconnectEvent,
		// Token: 0x04000990 RID: 2448
		Nothing,
		// Token: 0x04000991 RID: 2449
		BroadcastEvent
	}
}
