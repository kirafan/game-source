﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine.Networking
{
	// Token: 0x02000250 RID: 592
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DownloadHandlerBuffer : DownloadHandler
	{
		// Token: 0x0600280A RID: 10250 RVA: 0x00039B54 File Offset: 0x00037D54
		public DownloadHandlerBuffer()
		{
			base.InternalCreateBuffer();
		}

		// Token: 0x0600280B RID: 10251 RVA: 0x00039B64 File Offset: 0x00037D64
		protected override byte[] GetData()
		{
			return this.InternalGetData();
		}

		// Token: 0x0600280C RID: 10252 RVA: 0x00039B80 File Offset: 0x00037D80
		protected override string GetText()
		{
			return this.InternalGetText();
		}

		// Token: 0x0600280D RID: 10253
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern byte[] InternalGetData();

		// Token: 0x0600280E RID: 10254
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string InternalGetText();

		// Token: 0x0600280F RID: 10255 RVA: 0x00039B9C File Offset: 0x00037D9C
		public static string GetContent(UnityWebRequest www)
		{
			return DownloadHandler.GetCheckedDownloader<DownloadHandlerBuffer>(www).text;
		}
	}
}
