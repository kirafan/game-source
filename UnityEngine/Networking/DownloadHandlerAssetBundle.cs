﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine.Networking
{
	// Token: 0x02000253 RID: 595
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DownloadHandlerAssetBundle : DownloadHandler
	{
		// Token: 0x0600281B RID: 10267 RVA: 0x00039D10 File Offset: 0x00037F10
		public DownloadHandlerAssetBundle(string url, uint crc)
		{
			base.InternalCreateAssetBundle(url, crc);
		}

		// Token: 0x0600281C RID: 10268 RVA: 0x00039D24 File Offset: 0x00037F24
		public DownloadHandlerAssetBundle(string url, uint version, uint crc)
		{
			Hash128 hash = new Hash128(0U, 0U, 0U, version);
			base.InternalCreateAssetBundle(url, hash, crc);
		}

		// Token: 0x0600281D RID: 10269 RVA: 0x00039D4C File Offset: 0x00037F4C
		public DownloadHandlerAssetBundle(string url, Hash128 hash, uint crc)
		{
			base.InternalCreateAssetBundle(url, hash, crc);
		}

		// Token: 0x0600281E RID: 10270 RVA: 0x00039D60 File Offset: 0x00037F60
		protected override byte[] GetData()
		{
			throw new NotSupportedException("Raw data access is not supported for asset bundles");
		}

		// Token: 0x0600281F RID: 10271 RVA: 0x00039D70 File Offset: 0x00037F70
		protected override string GetText()
		{
			throw new NotSupportedException("String access is not supported for asset bundles");
		}

		// Token: 0x17000994 RID: 2452
		// (get) Token: 0x06002820 RID: 10272
		public extern AssetBundle assetBundle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06002821 RID: 10273 RVA: 0x00039D80 File Offset: 0x00037F80
		public static AssetBundle GetContent(UnityWebRequest www)
		{
			return DownloadHandler.GetCheckedDownloader<DownloadHandlerAssetBundle>(www).assetBundle;
		}
	}
}
