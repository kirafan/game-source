﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200025C RID: 604
	internal class CreateMatchResponse : BasicResponse
	{
		// Token: 0x170009A6 RID: 2470
		// (get) Token: 0x0600286A RID: 10346 RVA: 0x0003A72C File Offset: 0x0003892C
		// (set) Token: 0x0600286B RID: 10347 RVA: 0x0003A748 File Offset: 0x00038948
		public string address { get; set; }

		// Token: 0x170009A7 RID: 2471
		// (get) Token: 0x0600286C RID: 10348 RVA: 0x0003A754 File Offset: 0x00038954
		// (set) Token: 0x0600286D RID: 10349 RVA: 0x0003A770 File Offset: 0x00038970
		public int port { get; set; }

		// Token: 0x170009A8 RID: 2472
		// (get) Token: 0x0600286E RID: 10350 RVA: 0x0003A77C File Offset: 0x0003897C
		// (set) Token: 0x0600286F RID: 10351 RVA: 0x0003A798 File Offset: 0x00038998
		public int domain { get; set; }

		// Token: 0x170009A9 RID: 2473
		// (get) Token: 0x06002870 RID: 10352 RVA: 0x0003A7A4 File Offset: 0x000389A4
		// (set) Token: 0x06002871 RID: 10353 RVA: 0x0003A7C0 File Offset: 0x000389C0
		public NetworkID networkId { get; set; }

		// Token: 0x170009AA RID: 2474
		// (get) Token: 0x06002872 RID: 10354 RVA: 0x0003A7CC File Offset: 0x000389CC
		// (set) Token: 0x06002873 RID: 10355 RVA: 0x0003A7E8 File Offset: 0x000389E8
		public string accessTokenString { get; set; }

		// Token: 0x170009AB RID: 2475
		// (get) Token: 0x06002874 RID: 10356 RVA: 0x0003A7F4 File Offset: 0x000389F4
		// (set) Token: 0x06002875 RID: 10357 RVA: 0x0003A810 File Offset: 0x00038A10
		public NodeID nodeId { get; set; }

		// Token: 0x170009AC RID: 2476
		// (get) Token: 0x06002876 RID: 10358 RVA: 0x0003A81C File Offset: 0x00038A1C
		// (set) Token: 0x06002877 RID: 10359 RVA: 0x0003A838 File Offset: 0x00038A38
		public bool usingRelay { get; set; }

		// Token: 0x06002878 RID: 10360 RVA: 0x0003A844 File Offset: 0x00038A44
		public override string ToString()
		{
			return UnityString.Format("[{0}]-address:{1},port:{2},networkId:0x{3},accessTokenString.IsEmpty:{4},nodeId:0x{5},usingRelay:{6}", new object[]
			{
				base.ToString(),
				this.address,
				this.port,
				this.networkId.ToString("X"),
				string.IsNullOrEmpty(this.accessTokenString),
				this.nodeId.ToString("X"),
				this.usingRelay
			});
		}

		// Token: 0x06002879 RID: 10361 RVA: 0x0003A8E4 File Offset: 0x00038AE4
		public override void Parse(object obj)
		{
			base.Parse(obj);
			IDictionary<string, object> dictionary = obj as IDictionary<string, object>;
			if (dictionary != null)
			{
				this.address = base.ParseJSONString("address", obj, dictionary);
				this.port = base.ParseJSONInt32("port", obj, dictionary);
				this.networkId = (NetworkID)base.ParseJSONUInt64("networkId", obj, dictionary);
				this.accessTokenString = base.ParseJSONString("accessTokenString", obj, dictionary);
				this.nodeId = (NodeID)base.ParseJSONUInt16("nodeId", obj, dictionary);
				this.usingRelay = base.ParseJSONBool("usingRelay", obj, dictionary);
				return;
			}
			throw new FormatException("While parsing JSON response, found obj is not of type IDictionary<string,object>:" + obj.ToString());
		}
	}
}
