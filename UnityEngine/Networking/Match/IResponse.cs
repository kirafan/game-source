﻿using System;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000258 RID: 600
	internal interface IResponse
	{
		// Token: 0x0600284A RID: 10314
		void SetSuccess();

		// Token: 0x0600284B RID: 10315
		void SetFailure(string info);
	}
}
