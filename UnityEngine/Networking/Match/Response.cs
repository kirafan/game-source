﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000259 RID: 601
	internal abstract class Response : ResponseBase, IResponse
	{
		// Token: 0x1700099C RID: 2460
		// (get) Token: 0x0600284D RID: 10317 RVA: 0x0003A390 File Offset: 0x00038590
		// (set) Token: 0x0600284E RID: 10318 RVA: 0x0003A3AC File Offset: 0x000385AC
		public bool success { get; private set; }

		// Token: 0x1700099D RID: 2461
		// (get) Token: 0x0600284F RID: 10319 RVA: 0x0003A3B8 File Offset: 0x000385B8
		// (set) Token: 0x06002850 RID: 10320 RVA: 0x0003A3D4 File Offset: 0x000385D4
		public string extendedInfo { get; private set; }

		// Token: 0x06002851 RID: 10321 RVA: 0x0003A3E0 File Offset: 0x000385E0
		public void SetSuccess()
		{
			this.success = true;
			this.extendedInfo = "";
		}

		// Token: 0x06002852 RID: 10322 RVA: 0x0003A3F8 File Offset: 0x000385F8
		public void SetFailure(string info)
		{
			this.success = false;
			this.extendedInfo += info;
		}

		// Token: 0x06002853 RID: 10323 RVA: 0x0003A414 File Offset: 0x00038614
		public override string ToString()
		{
			return UnityString.Format("[{0}]-success:{1}-extendedInfo:{2}", new object[]
			{
				base.ToString(),
				this.success,
				this.extendedInfo
			});
		}

		// Token: 0x06002854 RID: 10324 RVA: 0x0003A45C File Offset: 0x0003865C
		public override void Parse(object obj)
		{
			IDictionary<string, object> dictionary = obj as IDictionary<string, object>;
			if (dictionary != null)
			{
				this.success = base.ParseJSONBool("success", obj, dictionary);
				this.extendedInfo = base.ParseJSONString("extendedInfo", obj, dictionary);
				if (!this.success)
				{
					throw new FormatException("FAILURE Returned from server: " + this.extendedInfo);
				}
			}
		}
	}
}
