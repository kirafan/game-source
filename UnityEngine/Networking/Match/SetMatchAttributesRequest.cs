﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000267 RID: 615
	internal class SetMatchAttributesRequest : Request
	{
		// Token: 0x170009D4 RID: 2516
		// (get) Token: 0x060028E6 RID: 10470 RVA: 0x0003B8BC File Offset: 0x00039ABC
		// (set) Token: 0x060028E7 RID: 10471 RVA: 0x0003B8D8 File Offset: 0x00039AD8
		public NetworkID networkId { get; set; }

		// Token: 0x170009D5 RID: 2517
		// (get) Token: 0x060028E8 RID: 10472 RVA: 0x0003B8E4 File Offset: 0x00039AE4
		// (set) Token: 0x060028E9 RID: 10473 RVA: 0x0003B900 File Offset: 0x00039B00
		public bool isListed { get; set; }

		// Token: 0x060028EA RID: 10474 RVA: 0x0003B90C File Offset: 0x00039B0C
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:{1},isListed:{2}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X"),
				this.isListed
			});
		}

		// Token: 0x060028EB RID: 10475 RVA: 0x0003B964 File Offset: 0x00039B64
		public override bool IsValid()
		{
			return base.IsValid() && this.networkId != NetworkID.Invalid;
		}
	}
}
