﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200025D RID: 605
	internal class JoinMatchRequest : Request
	{
		// Token: 0x170009AD RID: 2477
		// (get) Token: 0x0600287B RID: 10363 RVA: 0x0003A9A0 File Offset: 0x00038BA0
		// (set) Token: 0x0600287C RID: 10364 RVA: 0x0003A9BC File Offset: 0x00038BBC
		public NetworkID networkId { get; set; }

		// Token: 0x170009AE RID: 2478
		// (get) Token: 0x0600287D RID: 10365 RVA: 0x0003A9C8 File Offset: 0x00038BC8
		// (set) Token: 0x0600287E RID: 10366 RVA: 0x0003A9E4 File Offset: 0x00038BE4
		public string publicAddress { get; set; }

		// Token: 0x170009AF RID: 2479
		// (get) Token: 0x0600287F RID: 10367 RVA: 0x0003A9F0 File Offset: 0x00038BF0
		// (set) Token: 0x06002880 RID: 10368 RVA: 0x0003AA0C File Offset: 0x00038C0C
		public string privateAddress { get; set; }

		// Token: 0x170009B0 RID: 2480
		// (get) Token: 0x06002881 RID: 10369 RVA: 0x0003AA18 File Offset: 0x00038C18
		// (set) Token: 0x06002882 RID: 10370 RVA: 0x0003AA34 File Offset: 0x00038C34
		public int eloScore { get; set; }

		// Token: 0x170009B1 RID: 2481
		// (get) Token: 0x06002883 RID: 10371 RVA: 0x0003AA40 File Offset: 0x00038C40
		// (set) Token: 0x06002884 RID: 10372 RVA: 0x0003AA5C File Offset: 0x00038C5C
		public string password { get; set; }

		// Token: 0x06002885 RID: 10373 RVA: 0x0003AA68 File Offset: 0x00038C68
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:0x{1},publicAddress:{2},privateAddress:{3},eloScore:{4},HasPassword:{5}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X"),
				this.publicAddress,
				this.privateAddress,
				this.eloScore,
				(!string.IsNullOrEmpty(this.password)) ? "YES" : "NO"
			});
		}

		// Token: 0x06002886 RID: 10374 RVA: 0x0003AAF4 File Offset: 0x00038CF4
		public override bool IsValid()
		{
			return base.IsValid() && this.networkId != NetworkID.Invalid;
		}
	}
}
