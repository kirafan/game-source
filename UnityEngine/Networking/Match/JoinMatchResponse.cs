﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200025E RID: 606
	internal class JoinMatchResponse : BasicResponse
	{
		// Token: 0x170009B2 RID: 2482
		// (get) Token: 0x06002888 RID: 10376 RVA: 0x0003AB2C File Offset: 0x00038D2C
		// (set) Token: 0x06002889 RID: 10377 RVA: 0x0003AB48 File Offset: 0x00038D48
		public string address { get; set; }

		// Token: 0x170009B3 RID: 2483
		// (get) Token: 0x0600288A RID: 10378 RVA: 0x0003AB54 File Offset: 0x00038D54
		// (set) Token: 0x0600288B RID: 10379 RVA: 0x0003AB70 File Offset: 0x00038D70
		public int port { get; set; }

		// Token: 0x170009B4 RID: 2484
		// (get) Token: 0x0600288C RID: 10380 RVA: 0x0003AB7C File Offset: 0x00038D7C
		// (set) Token: 0x0600288D RID: 10381 RVA: 0x0003AB98 File Offset: 0x00038D98
		public int domain { get; set; }

		// Token: 0x170009B5 RID: 2485
		// (get) Token: 0x0600288E RID: 10382 RVA: 0x0003ABA4 File Offset: 0x00038DA4
		// (set) Token: 0x0600288F RID: 10383 RVA: 0x0003ABC0 File Offset: 0x00038DC0
		public NetworkID networkId { get; set; }

		// Token: 0x170009B6 RID: 2486
		// (get) Token: 0x06002890 RID: 10384 RVA: 0x0003ABCC File Offset: 0x00038DCC
		// (set) Token: 0x06002891 RID: 10385 RVA: 0x0003ABE8 File Offset: 0x00038DE8
		public string accessTokenString { get; set; }

		// Token: 0x170009B7 RID: 2487
		// (get) Token: 0x06002892 RID: 10386 RVA: 0x0003ABF4 File Offset: 0x00038DF4
		// (set) Token: 0x06002893 RID: 10387 RVA: 0x0003AC10 File Offset: 0x00038E10
		public NodeID nodeId { get; set; }

		// Token: 0x170009B8 RID: 2488
		// (get) Token: 0x06002894 RID: 10388 RVA: 0x0003AC1C File Offset: 0x00038E1C
		// (set) Token: 0x06002895 RID: 10389 RVA: 0x0003AC38 File Offset: 0x00038E38
		public bool usingRelay { get; set; }

		// Token: 0x06002896 RID: 10390 RVA: 0x0003AC44 File Offset: 0x00038E44
		public override string ToString()
		{
			return UnityString.Format("[{0}]-address:{1},port:{2},networkId:0x{3},accessTokenString.IsEmpty:{4},nodeId:0x{5},usingRelay:{6}", new object[]
			{
				base.ToString(),
				this.address,
				this.port,
				this.networkId.ToString("X"),
				string.IsNullOrEmpty(this.accessTokenString),
				this.nodeId.ToString("X"),
				this.usingRelay
			});
		}

		// Token: 0x06002897 RID: 10391 RVA: 0x0003ACE4 File Offset: 0x00038EE4
		public override void Parse(object obj)
		{
			base.Parse(obj);
			IDictionary<string, object> dictionary = obj as IDictionary<string, object>;
			if (dictionary != null)
			{
				this.address = base.ParseJSONString("address", obj, dictionary);
				this.port = base.ParseJSONInt32("port", obj, dictionary);
				this.networkId = (NetworkID)base.ParseJSONUInt64("networkId", obj, dictionary);
				this.accessTokenString = base.ParseJSONString("accessTokenString", obj, dictionary);
				this.nodeId = (NodeID)base.ParseJSONUInt16("nodeId", obj, dictionary);
				this.usingRelay = base.ParseJSONBool("usingRelay", obj, dictionary);
				return;
			}
			throw new FormatException("While parsing JSON response, found obj is not of type IDictionary<string,object>:" + obj.ToString());
		}
	}
}
