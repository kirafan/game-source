﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000263 RID: 611
	internal class MatchDirectConnectInfo : ResponseBase
	{
		// Token: 0x170009C6 RID: 2502
		// (get) Token: 0x060028BF RID: 10431 RVA: 0x0003B31C File Offset: 0x0003951C
		// (set) Token: 0x060028C0 RID: 10432 RVA: 0x0003B338 File Offset: 0x00039538
		public NodeID nodeId { get; set; }

		// Token: 0x170009C7 RID: 2503
		// (get) Token: 0x060028C1 RID: 10433 RVA: 0x0003B344 File Offset: 0x00039544
		// (set) Token: 0x060028C2 RID: 10434 RVA: 0x0003B360 File Offset: 0x00039560
		public string publicAddress { get; set; }

		// Token: 0x170009C8 RID: 2504
		// (get) Token: 0x060028C3 RID: 10435 RVA: 0x0003B36C File Offset: 0x0003956C
		// (set) Token: 0x060028C4 RID: 10436 RVA: 0x0003B388 File Offset: 0x00039588
		public string privateAddress { get; set; }

		// Token: 0x170009C9 RID: 2505
		// (get) Token: 0x060028C5 RID: 10437 RVA: 0x0003B394 File Offset: 0x00039594
		// (set) Token: 0x060028C6 RID: 10438 RVA: 0x0003B3B0 File Offset: 0x000395B0
		public HostPriority hostPriority { get; set; }

		// Token: 0x060028C7 RID: 10439 RVA: 0x0003B3BC File Offset: 0x000395BC
		public override string ToString()
		{
			return UnityString.Format("[{0}]-nodeId:{1},publicAddress:{2},privateAddress:{3},hostPriority:{4}", new object[]
			{
				base.ToString(),
				this.nodeId,
				this.publicAddress,
				this.privateAddress,
				this.hostPriority
			});
		}

		// Token: 0x060028C8 RID: 10440 RVA: 0x0003B418 File Offset: 0x00039618
		public override void Parse(object obj)
		{
			IDictionary<string, object> dictionary = obj as IDictionary<string, object>;
			if (dictionary != null)
			{
				this.nodeId = (NodeID)base.ParseJSONUInt16("nodeId", obj, dictionary);
				this.publicAddress = base.ParseJSONString("publicAddress", obj, dictionary);
				this.privateAddress = base.ParseJSONString("privateAddress", obj, dictionary);
				this.hostPriority = (HostPriority)base.ParseJSONInt32("hostPriority", obj, dictionary);
				return;
			}
			throw new FormatException("While parsing JSON response, found obj is not of type IDictionary<string,object>:" + obj.ToString());
		}
	}
}
