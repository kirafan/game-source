﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000256 RID: 598
	internal abstract class Request
	{
		// Token: 0x17000996 RID: 2454
		// (get) Token: 0x0600282E RID: 10286 RVA: 0x00039E50 File Offset: 0x00038050
		// (set) Token: 0x0600282F RID: 10287 RVA: 0x00039E6C File Offset: 0x0003806C
		public int version { get; set; }

		// Token: 0x17000997 RID: 2455
		// (get) Token: 0x06002830 RID: 10288 RVA: 0x00039E78 File Offset: 0x00038078
		// (set) Token: 0x06002831 RID: 10289 RVA: 0x00039E94 File Offset: 0x00038094
		public SourceID sourceId { get; set; }

		// Token: 0x17000998 RID: 2456
		// (get) Token: 0x06002832 RID: 10290 RVA: 0x00039EA0 File Offset: 0x000380A0
		// (set) Token: 0x06002833 RID: 10291 RVA: 0x00039EBC File Offset: 0x000380BC
		public string projectId { get; set; }

		// Token: 0x17000999 RID: 2457
		// (get) Token: 0x06002834 RID: 10292 RVA: 0x00039EC8 File Offset: 0x000380C8
		// (set) Token: 0x06002835 RID: 10293 RVA: 0x00039EE4 File Offset: 0x000380E4
		public AppID appId { get; set; }

		// Token: 0x1700099A RID: 2458
		// (get) Token: 0x06002836 RID: 10294 RVA: 0x00039EF0 File Offset: 0x000380F0
		// (set) Token: 0x06002837 RID: 10295 RVA: 0x00039F0C File Offset: 0x0003810C
		public string accessTokenString { get; set; }

		// Token: 0x1700099B RID: 2459
		// (get) Token: 0x06002838 RID: 10296 RVA: 0x00039F18 File Offset: 0x00038118
		// (set) Token: 0x06002839 RID: 10297 RVA: 0x00039F34 File Offset: 0x00038134
		public int domain { get; set; }

		// Token: 0x0600283A RID: 10298 RVA: 0x00039F40 File Offset: 0x00038140
		public virtual bool IsValid()
		{
			return this.sourceId != SourceID.Invalid;
		}

		// Token: 0x0600283B RID: 10299 RVA: 0x00039F64 File Offset: 0x00038164
		public override string ToString()
		{
			return UnityString.Format("[{0}]-SourceID:0x{1},projectId:{2},accessTokenString.IsEmpty:{3},domain:{4}", new object[]
			{
				base.ToString(),
				this.sourceId.ToString("X"),
				this.projectId,
				string.IsNullOrEmpty(this.accessTokenString),
				this.domain
			});
		}

		// Token: 0x0400091C RID: 2332
		public static readonly int currentVersion = 3;
	}
}
