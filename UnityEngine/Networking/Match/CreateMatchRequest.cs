﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200025B RID: 603
	internal class CreateMatchRequest : Request
	{
		// Token: 0x1700099E RID: 2462
		// (get) Token: 0x06002857 RID: 10327 RVA: 0x0003A4D4 File Offset: 0x000386D4
		// (set) Token: 0x06002858 RID: 10328 RVA: 0x0003A4F0 File Offset: 0x000386F0
		public string name { get; set; }

		// Token: 0x1700099F RID: 2463
		// (get) Token: 0x06002859 RID: 10329 RVA: 0x0003A4FC File Offset: 0x000386FC
		// (set) Token: 0x0600285A RID: 10330 RVA: 0x0003A518 File Offset: 0x00038718
		public uint size { get; set; }

		// Token: 0x170009A0 RID: 2464
		// (get) Token: 0x0600285B RID: 10331 RVA: 0x0003A524 File Offset: 0x00038724
		// (set) Token: 0x0600285C RID: 10332 RVA: 0x0003A540 File Offset: 0x00038740
		public string publicAddress { get; set; }

		// Token: 0x170009A1 RID: 2465
		// (get) Token: 0x0600285D RID: 10333 RVA: 0x0003A54C File Offset: 0x0003874C
		// (set) Token: 0x0600285E RID: 10334 RVA: 0x0003A568 File Offset: 0x00038768
		public string privateAddress { get; set; }

		// Token: 0x170009A2 RID: 2466
		// (get) Token: 0x0600285F RID: 10335 RVA: 0x0003A574 File Offset: 0x00038774
		// (set) Token: 0x06002860 RID: 10336 RVA: 0x0003A590 File Offset: 0x00038790
		public int eloScore { get; set; }

		// Token: 0x170009A3 RID: 2467
		// (get) Token: 0x06002861 RID: 10337 RVA: 0x0003A59C File Offset: 0x0003879C
		// (set) Token: 0x06002862 RID: 10338 RVA: 0x0003A5B8 File Offset: 0x000387B8
		public bool advertise { get; set; }

		// Token: 0x170009A4 RID: 2468
		// (get) Token: 0x06002863 RID: 10339 RVA: 0x0003A5C4 File Offset: 0x000387C4
		// (set) Token: 0x06002864 RID: 10340 RVA: 0x0003A5E0 File Offset: 0x000387E0
		public string password { get; set; }

		// Token: 0x170009A5 RID: 2469
		// (get) Token: 0x06002865 RID: 10341 RVA: 0x0003A5EC File Offset: 0x000387EC
		// (set) Token: 0x06002866 RID: 10342 RVA: 0x0003A608 File Offset: 0x00038808
		public Dictionary<string, long> matchAttributes { get; set; }

		// Token: 0x06002867 RID: 10343 RVA: 0x0003A614 File Offset: 0x00038814
		public override string ToString()
		{
			return UnityString.Format("[{0}]-name:{1},size:{2},publicAddress:{3},privateAddress:{4},eloScore:{5},advertise:{6},HasPassword:{7},matchAttributes.Count:{8}", new object[]
			{
				base.ToString(),
				this.name,
				this.size,
				this.publicAddress,
				this.privateAddress,
				this.eloScore,
				this.advertise,
				(!string.IsNullOrEmpty(this.password)) ? "YES" : "NO",
				(this.matchAttributes != null) ? this.matchAttributes.Count : 0
			});
		}

		// Token: 0x06002868 RID: 10344 RVA: 0x0003A6D0 File Offset: 0x000388D0
		public override bool IsValid()
		{
			return base.IsValid() && this.size >= 2U && (this.matchAttributes == null || this.matchAttributes.Count <= 10);
		}
	}
}
