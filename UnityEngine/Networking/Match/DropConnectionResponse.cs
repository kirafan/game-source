﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000261 RID: 609
	internal class DropConnectionResponse : Response
	{
		// Token: 0x170009BC RID: 2492
		// (get) Token: 0x060028A5 RID: 10405 RVA: 0x0003AF48 File Offset: 0x00039148
		// (set) Token: 0x060028A6 RID: 10406 RVA: 0x0003AF64 File Offset: 0x00039164
		public NetworkID networkId { get; set; }

		// Token: 0x170009BD RID: 2493
		// (get) Token: 0x060028A7 RID: 10407 RVA: 0x0003AF70 File Offset: 0x00039170
		// (set) Token: 0x060028A8 RID: 10408 RVA: 0x0003AF8C File Offset: 0x0003918C
		public NodeID nodeId { get; set; }

		// Token: 0x060028A9 RID: 10409 RVA: 0x0003AF98 File Offset: 0x00039198
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:{1}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X")
			});
		}

		// Token: 0x060028AA RID: 10410 RVA: 0x0003AFE4 File Offset: 0x000391E4
		public override void Parse(object obj)
		{
			base.Parse(obj);
			IDictionary<string, object> dictionary = obj as IDictionary<string, object>;
			if (dictionary != null)
			{
				this.networkId = (NetworkID)base.ParseJSONUInt64("networkId", obj, dictionary);
				this.nodeId = (NodeID)base.ParseJSONUInt16("nodeId", obj, dictionary);
				return;
			}
			throw new FormatException("While parsing JSON response, found obj is not of type IDictionary<string,object>:" + obj.ToString());
		}
	}
}
