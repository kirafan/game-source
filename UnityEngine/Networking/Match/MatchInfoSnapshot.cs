﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000271 RID: 625
	public class MatchInfoSnapshot
	{
		// Token: 0x0600290C RID: 10508 RVA: 0x0003BDD0 File Offset: 0x00039FD0
		public MatchInfoSnapshot()
		{
		}

		// Token: 0x0600290D RID: 10509 RVA: 0x0003BDDC File Offset: 0x00039FDC
		internal MatchInfoSnapshot(MatchDesc matchDesc)
		{
			this.networkId = matchDesc.networkId;
			this.hostNodeId = matchDesc.hostNodeId;
			this.name = matchDesc.name;
			this.averageEloScore = matchDesc.averageEloScore;
			this.maxSize = matchDesc.maxSize;
			this.currentSize = matchDesc.currentSize;
			this.isPrivate = matchDesc.isPrivate;
			this.matchAttributes = matchDesc.matchAttributes;
			this.directConnectInfos = new List<MatchInfoSnapshot.MatchInfoDirectConnectSnapshot>();
			foreach (MatchDirectConnectInfo matchDirectConnectInfo in matchDesc.directConnectInfos)
			{
				this.directConnectInfos.Add(new MatchInfoSnapshot.MatchInfoDirectConnectSnapshot(matchDirectConnectInfo));
			}
		}

		// Token: 0x170009DE RID: 2526
		// (get) Token: 0x0600290E RID: 10510 RVA: 0x0003BEB8 File Offset: 0x0003A0B8
		// (set) Token: 0x0600290F RID: 10511 RVA: 0x0003BED4 File Offset: 0x0003A0D4
		public NetworkID networkId { get; private set; }

		// Token: 0x170009DF RID: 2527
		// (get) Token: 0x06002910 RID: 10512 RVA: 0x0003BEE0 File Offset: 0x0003A0E0
		// (set) Token: 0x06002911 RID: 10513 RVA: 0x0003BEFC File Offset: 0x0003A0FC
		public NodeID hostNodeId { get; private set; }

		// Token: 0x170009E0 RID: 2528
		// (get) Token: 0x06002912 RID: 10514 RVA: 0x0003BF08 File Offset: 0x0003A108
		// (set) Token: 0x06002913 RID: 10515 RVA: 0x0003BF24 File Offset: 0x0003A124
		public string name { get; private set; }

		// Token: 0x170009E1 RID: 2529
		// (get) Token: 0x06002914 RID: 10516 RVA: 0x0003BF30 File Offset: 0x0003A130
		// (set) Token: 0x06002915 RID: 10517 RVA: 0x0003BF4C File Offset: 0x0003A14C
		public int averageEloScore { get; private set; }

		// Token: 0x170009E2 RID: 2530
		// (get) Token: 0x06002916 RID: 10518 RVA: 0x0003BF58 File Offset: 0x0003A158
		// (set) Token: 0x06002917 RID: 10519 RVA: 0x0003BF74 File Offset: 0x0003A174
		public int maxSize { get; private set; }

		// Token: 0x170009E3 RID: 2531
		// (get) Token: 0x06002918 RID: 10520 RVA: 0x0003BF80 File Offset: 0x0003A180
		// (set) Token: 0x06002919 RID: 10521 RVA: 0x0003BF9C File Offset: 0x0003A19C
		public int currentSize { get; private set; }

		// Token: 0x170009E4 RID: 2532
		// (get) Token: 0x0600291A RID: 10522 RVA: 0x0003BFA8 File Offset: 0x0003A1A8
		// (set) Token: 0x0600291B RID: 10523 RVA: 0x0003BFC4 File Offset: 0x0003A1C4
		public bool isPrivate { get; private set; }

		// Token: 0x170009E5 RID: 2533
		// (get) Token: 0x0600291C RID: 10524 RVA: 0x0003BFD0 File Offset: 0x0003A1D0
		// (set) Token: 0x0600291D RID: 10525 RVA: 0x0003BFEC File Offset: 0x0003A1EC
		public Dictionary<string, long> matchAttributes { get; private set; }

		// Token: 0x170009E6 RID: 2534
		// (get) Token: 0x0600291E RID: 10526 RVA: 0x0003BFF8 File Offset: 0x0003A1F8
		// (set) Token: 0x0600291F RID: 10527 RVA: 0x0003C014 File Offset: 0x0003A214
		public List<MatchInfoSnapshot.MatchInfoDirectConnectSnapshot> directConnectInfos { get; private set; }

		// Token: 0x02000272 RID: 626
		public class MatchInfoDirectConnectSnapshot
		{
			// Token: 0x06002920 RID: 10528 RVA: 0x0003C020 File Offset: 0x0003A220
			public MatchInfoDirectConnectSnapshot()
			{
			}

			// Token: 0x06002921 RID: 10529 RVA: 0x0003C02C File Offset: 0x0003A22C
			internal MatchInfoDirectConnectSnapshot(MatchDirectConnectInfo matchDirectConnectInfo)
			{
				this.nodeId = matchDirectConnectInfo.nodeId;
				this.publicAddress = matchDirectConnectInfo.publicAddress;
				this.privateAddress = matchDirectConnectInfo.privateAddress;
				this.hostPriority = matchDirectConnectInfo.hostPriority;
			}

			// Token: 0x170009E7 RID: 2535
			// (get) Token: 0x06002922 RID: 10530 RVA: 0x0003C068 File Offset: 0x0003A268
			// (set) Token: 0x06002923 RID: 10531 RVA: 0x0003C084 File Offset: 0x0003A284
			public NodeID nodeId { get; private set; }

			// Token: 0x170009E8 RID: 2536
			// (get) Token: 0x06002924 RID: 10532 RVA: 0x0003C090 File Offset: 0x0003A290
			// (set) Token: 0x06002925 RID: 10533 RVA: 0x0003C0AC File Offset: 0x0003A2AC
			public string publicAddress { get; private set; }

			// Token: 0x170009E9 RID: 2537
			// (get) Token: 0x06002926 RID: 10534 RVA: 0x0003C0B8 File Offset: 0x0003A2B8
			// (set) Token: 0x06002927 RID: 10535 RVA: 0x0003C0D4 File Offset: 0x0003A2D4
			public string privateAddress { get; private set; }

			// Token: 0x170009EA RID: 2538
			// (get) Token: 0x06002928 RID: 10536 RVA: 0x0003C0E0 File Offset: 0x0003A2E0
			// (set) Token: 0x06002929 RID: 10537 RVA: 0x0003C0FC File Offset: 0x0003A2FC
			public HostPriority hostPriority { get; private set; }
		}
	}
}
