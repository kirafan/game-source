﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000270 RID: 624
	public class MatchInfo
	{
		// Token: 0x060028FA RID: 10490 RVA: 0x0003BB64 File Offset: 0x00039D64
		public MatchInfo()
		{
		}

		// Token: 0x060028FB RID: 10491 RVA: 0x0003BB70 File Offset: 0x00039D70
		internal MatchInfo(CreateMatchResponse matchResponse)
		{
			this.address = matchResponse.address;
			this.port = matchResponse.port;
			this.domain = matchResponse.domain;
			this.networkId = matchResponse.networkId;
			this.accessToken = new NetworkAccessToken(matchResponse.accessTokenString);
			this.nodeId = matchResponse.nodeId;
			this.usingRelay = matchResponse.usingRelay;
		}

		// Token: 0x060028FC RID: 10492 RVA: 0x0003BBE0 File Offset: 0x00039DE0
		internal MatchInfo(JoinMatchResponse matchResponse)
		{
			this.address = matchResponse.address;
			this.port = matchResponse.port;
			this.domain = matchResponse.domain;
			this.networkId = matchResponse.networkId;
			this.accessToken = new NetworkAccessToken(matchResponse.accessTokenString);
			this.nodeId = matchResponse.nodeId;
			this.usingRelay = matchResponse.usingRelay;
		}

		// Token: 0x170009D7 RID: 2519
		// (get) Token: 0x060028FD RID: 10493 RVA: 0x0003BC50 File Offset: 0x00039E50
		// (set) Token: 0x060028FE RID: 10494 RVA: 0x0003BC6C File Offset: 0x00039E6C
		public string address { get; private set; }

		// Token: 0x170009D8 RID: 2520
		// (get) Token: 0x060028FF RID: 10495 RVA: 0x0003BC78 File Offset: 0x00039E78
		// (set) Token: 0x06002900 RID: 10496 RVA: 0x0003BC94 File Offset: 0x00039E94
		public int port { get; private set; }

		// Token: 0x170009D9 RID: 2521
		// (get) Token: 0x06002901 RID: 10497 RVA: 0x0003BCA0 File Offset: 0x00039EA0
		// (set) Token: 0x06002902 RID: 10498 RVA: 0x0003BCBC File Offset: 0x00039EBC
		public int domain { get; private set; }

		// Token: 0x170009DA RID: 2522
		// (get) Token: 0x06002903 RID: 10499 RVA: 0x0003BCC8 File Offset: 0x00039EC8
		// (set) Token: 0x06002904 RID: 10500 RVA: 0x0003BCE4 File Offset: 0x00039EE4
		public NetworkID networkId { get; private set; }

		// Token: 0x170009DB RID: 2523
		// (get) Token: 0x06002905 RID: 10501 RVA: 0x0003BCF0 File Offset: 0x00039EF0
		// (set) Token: 0x06002906 RID: 10502 RVA: 0x0003BD0C File Offset: 0x00039F0C
		public NetworkAccessToken accessToken { get; private set; }

		// Token: 0x170009DC RID: 2524
		// (get) Token: 0x06002907 RID: 10503 RVA: 0x0003BD18 File Offset: 0x00039F18
		// (set) Token: 0x06002908 RID: 10504 RVA: 0x0003BD34 File Offset: 0x00039F34
		public NodeID nodeId { get; private set; }

		// Token: 0x170009DD RID: 2525
		// (get) Token: 0x06002909 RID: 10505 RVA: 0x0003BD40 File Offset: 0x00039F40
		// (set) Token: 0x0600290A RID: 10506 RVA: 0x0003BD5C File Offset: 0x00039F5C
		public bool usingRelay { get; private set; }

		// Token: 0x0600290B RID: 10507 RVA: 0x0003BD68 File Offset: 0x00039F68
		public override string ToString()
		{
			return UnityString.Format("{0} @ {1}:{2} [{3},{4}]", new object[]
			{
				this.networkId,
				this.address,
				this.port,
				this.nodeId,
				this.usingRelay
			});
		}
	}
}
