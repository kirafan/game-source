﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000264 RID: 612
	internal class MatchDesc : ResponseBase
	{
		// Token: 0x170009CA RID: 2506
		// (get) Token: 0x060028CA RID: 10442 RVA: 0x0003B4A8 File Offset: 0x000396A8
		// (set) Token: 0x060028CB RID: 10443 RVA: 0x0003B4C4 File Offset: 0x000396C4
		public NetworkID networkId { get; set; }

		// Token: 0x170009CB RID: 2507
		// (get) Token: 0x060028CC RID: 10444 RVA: 0x0003B4D0 File Offset: 0x000396D0
		// (set) Token: 0x060028CD RID: 10445 RVA: 0x0003B4EC File Offset: 0x000396EC
		public string name { get; set; }

		// Token: 0x170009CC RID: 2508
		// (get) Token: 0x060028CE RID: 10446 RVA: 0x0003B4F8 File Offset: 0x000396F8
		// (set) Token: 0x060028CF RID: 10447 RVA: 0x0003B514 File Offset: 0x00039714
		public int averageEloScore { get; set; }

		// Token: 0x170009CD RID: 2509
		// (get) Token: 0x060028D0 RID: 10448 RVA: 0x0003B520 File Offset: 0x00039720
		// (set) Token: 0x060028D1 RID: 10449 RVA: 0x0003B53C File Offset: 0x0003973C
		public int maxSize { get; set; }

		// Token: 0x170009CE RID: 2510
		// (get) Token: 0x060028D2 RID: 10450 RVA: 0x0003B548 File Offset: 0x00039748
		// (set) Token: 0x060028D3 RID: 10451 RVA: 0x0003B564 File Offset: 0x00039764
		public int currentSize { get; set; }

		// Token: 0x170009CF RID: 2511
		// (get) Token: 0x060028D4 RID: 10452 RVA: 0x0003B570 File Offset: 0x00039770
		// (set) Token: 0x060028D5 RID: 10453 RVA: 0x0003B58C File Offset: 0x0003978C
		public bool isPrivate { get; set; }

		// Token: 0x170009D0 RID: 2512
		// (get) Token: 0x060028D6 RID: 10454 RVA: 0x0003B598 File Offset: 0x00039798
		// (set) Token: 0x060028D7 RID: 10455 RVA: 0x0003B5B4 File Offset: 0x000397B4
		public Dictionary<string, long> matchAttributes { get; set; }

		// Token: 0x170009D1 RID: 2513
		// (get) Token: 0x060028D8 RID: 10456 RVA: 0x0003B5C0 File Offset: 0x000397C0
		// (set) Token: 0x060028D9 RID: 10457 RVA: 0x0003B5DC File Offset: 0x000397DC
		public NodeID hostNodeId { get; set; }

		// Token: 0x170009D2 RID: 2514
		// (get) Token: 0x060028DA RID: 10458 RVA: 0x0003B5E8 File Offset: 0x000397E8
		// (set) Token: 0x060028DB RID: 10459 RVA: 0x0003B604 File Offset: 0x00039804
		public List<MatchDirectConnectInfo> directConnectInfos { get; set; }

		// Token: 0x060028DC RID: 10460 RVA: 0x0003B610 File Offset: 0x00039810
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:0x{1},name:{2},averageEloScore:{3},maxSize:{4},currentSize:{5},isPrivate:{6},matchAttributes.Count:{7},hostNodeId:{8},directConnectInfos.Count:{9}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X"),
				this.name,
				this.averageEloScore,
				this.maxSize,
				this.currentSize,
				this.isPrivate,
				(this.matchAttributes != null) ? this.matchAttributes.Count : 0,
				this.hostNodeId,
				this.directConnectInfos.Count
			});
		}

		// Token: 0x060028DD RID: 10461 RVA: 0x0003B6E4 File Offset: 0x000398E4
		public override void Parse(object obj)
		{
			IDictionary<string, object> dictionary = obj as IDictionary<string, object>;
			if (dictionary != null)
			{
				this.networkId = (NetworkID)base.ParseJSONUInt64("networkId", obj, dictionary);
				this.name = base.ParseJSONString("name", obj, dictionary);
				this.averageEloScore = base.ParseJSONInt32("averageEloScore", obj, dictionary);
				this.maxSize = base.ParseJSONInt32("maxSize", obj, dictionary);
				this.currentSize = base.ParseJSONInt32("currentSize", obj, dictionary);
				this.isPrivate = base.ParseJSONBool("isPrivate", obj, dictionary);
				this.hostNodeId = (NodeID)base.ParseJSONUInt16("hostNodeId", obj, dictionary);
				this.directConnectInfos = base.ParseJSONList<MatchDirectConnectInfo>("directConnectInfos", obj, dictionary);
				return;
			}
			throw new FormatException("While parsing JSON response, found obj is not of type IDictionary<string,object>:" + obj.ToString());
		}
	}
}
