﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000257 RID: 599
	internal abstract class ResponseBase
	{
		// Token: 0x0600283E RID: 10302
		public abstract void Parse(object obj);

		// Token: 0x0600283F RID: 10303 RVA: 0x00039FE8 File Offset: 0x000381E8
		public string ParseJSONString(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				return obj as string;
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}

		// Token: 0x06002840 RID: 10304 RVA: 0x0003A024 File Offset: 0x00038224
		public short ParseJSONInt16(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				return Convert.ToInt16(obj);
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}

		// Token: 0x06002841 RID: 10305 RVA: 0x0003A060 File Offset: 0x00038260
		public int ParseJSONInt32(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				return Convert.ToInt32(obj);
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}

		// Token: 0x06002842 RID: 10306 RVA: 0x0003A09C File Offset: 0x0003829C
		public long ParseJSONInt64(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				return Convert.ToInt64(obj);
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}

		// Token: 0x06002843 RID: 10307 RVA: 0x0003A0D8 File Offset: 0x000382D8
		public ushort ParseJSONUInt16(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				return Convert.ToUInt16(obj);
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}

		// Token: 0x06002844 RID: 10308 RVA: 0x0003A114 File Offset: 0x00038314
		public uint ParseJSONUInt32(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				return Convert.ToUInt32(obj);
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}

		// Token: 0x06002845 RID: 10309 RVA: 0x0003A150 File Offset: 0x00038350
		public ulong ParseJSONUInt64(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				return Convert.ToUInt64(obj);
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}

		// Token: 0x06002846 RID: 10310 RVA: 0x0003A18C File Offset: 0x0003838C
		public bool ParseJSONBool(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				return Convert.ToBoolean(obj);
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}

		// Token: 0x06002847 RID: 10311 RVA: 0x0003A1C8 File Offset: 0x000383C8
		public DateTime ParseJSONDateTime(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			throw new FormatException(name + " DateTime not yet supported");
		}

		// Token: 0x06002848 RID: 10312 RVA: 0x0003A1DC File Offset: 0x000383DC
		public List<string> ParseJSONListOfStrings(string name, object obj, IDictionary<string, object> dictJsonObj)
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				List<object> list = obj as List<object>;
				if (list != null)
				{
					List<string> list2 = new List<string>();
					foreach (object obj2 in list)
					{
						IDictionary<string, object> dictionary = (IDictionary<string, object>)obj2;
						foreach (KeyValuePair<string, object> keyValuePair in dictionary)
						{
							string item = (string)keyValuePair.Value;
							list2.Add(item);
						}
					}
					return list2;
				}
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}

		// Token: 0x06002849 RID: 10313 RVA: 0x0003A2D0 File Offset: 0x000384D0
		public List<T> ParseJSONList<T>(string name, object obj, IDictionary<string, object> dictJsonObj) where T : ResponseBase, new()
		{
			if (dictJsonObj.TryGetValue(name, out obj))
			{
				List<object> list = obj as List<object>;
				if (list != null)
				{
					List<T> list2 = new List<T>();
					foreach (object obj2 in list)
					{
						IDictionary<string, object> obj3 = (IDictionary<string, object>)obj2;
						T item = Activator.CreateInstance<T>();
						item.Parse(obj3);
						list2.Add(item);
					}
					return list2;
				}
			}
			throw new FormatException(name + " not found in JSON dictionary");
		}
	}
}
