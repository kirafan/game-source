﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000262 RID: 610
	internal class ListMatchRequest : Request
	{
		// Token: 0x170009BE RID: 2494
		// (get) Token: 0x060028AC RID: 10412 RVA: 0x0003B054 File Offset: 0x00039254
		// (set) Token: 0x060028AD RID: 10413 RVA: 0x0003B070 File Offset: 0x00039270
		public int pageSize { get; set; }

		// Token: 0x170009BF RID: 2495
		// (get) Token: 0x060028AE RID: 10414 RVA: 0x0003B07C File Offset: 0x0003927C
		// (set) Token: 0x060028AF RID: 10415 RVA: 0x0003B098 File Offset: 0x00039298
		public int pageNum { get; set; }

		// Token: 0x170009C0 RID: 2496
		// (get) Token: 0x060028B0 RID: 10416 RVA: 0x0003B0A4 File Offset: 0x000392A4
		// (set) Token: 0x060028B1 RID: 10417 RVA: 0x0003B0C0 File Offset: 0x000392C0
		public string nameFilter { get; set; }

		// Token: 0x170009C1 RID: 2497
		// (get) Token: 0x060028B2 RID: 10418 RVA: 0x0003B0CC File Offset: 0x000392CC
		// (set) Token: 0x060028B3 RID: 10419 RVA: 0x0003B0E8 File Offset: 0x000392E8
		public bool filterOutPrivateMatches { get; set; }

		// Token: 0x170009C2 RID: 2498
		// (get) Token: 0x060028B4 RID: 10420 RVA: 0x0003B0F4 File Offset: 0x000392F4
		// (set) Token: 0x060028B5 RID: 10421 RVA: 0x0003B110 File Offset: 0x00039310
		public int eloScore { get; set; }

		// Token: 0x170009C3 RID: 2499
		// (get) Token: 0x060028B6 RID: 10422 RVA: 0x0003B11C File Offset: 0x0003931C
		// (set) Token: 0x060028B7 RID: 10423 RVA: 0x0003B138 File Offset: 0x00039338
		public Dictionary<string, long> matchAttributeFilterLessThan { get; set; }

		// Token: 0x170009C4 RID: 2500
		// (get) Token: 0x060028B8 RID: 10424 RVA: 0x0003B144 File Offset: 0x00039344
		// (set) Token: 0x060028B9 RID: 10425 RVA: 0x0003B160 File Offset: 0x00039360
		public Dictionary<string, long> matchAttributeFilterEqualTo { get; set; }

		// Token: 0x170009C5 RID: 2501
		// (get) Token: 0x060028BA RID: 10426 RVA: 0x0003B16C File Offset: 0x0003936C
		// (set) Token: 0x060028BB RID: 10427 RVA: 0x0003B188 File Offset: 0x00039388
		public Dictionary<string, long> matchAttributeFilterGreaterThan { get; set; }

		// Token: 0x060028BC RID: 10428 RVA: 0x0003B194 File Offset: 0x00039394
		public override string ToString()
		{
			return UnityString.Format("[{0}]-pageSize:{1},pageNum:{2},nameFilter:{3}, filterOutPrivateMatches:{4}, eloScore:{5}, matchAttributeFilterLessThan.Count:{6}, matchAttributeFilterEqualTo.Count:{7}, matchAttributeFilterGreaterThan.Count:{8}", new object[]
			{
				base.ToString(),
				this.pageSize,
				this.pageNum,
				this.nameFilter,
				this.filterOutPrivateMatches,
				this.eloScore,
				(this.matchAttributeFilterLessThan != null) ? this.matchAttributeFilterLessThan.Count : 0,
				(this.matchAttributeFilterEqualTo != null) ? this.matchAttributeFilterEqualTo.Count : 0,
				(this.matchAttributeFilterGreaterThan != null) ? this.matchAttributeFilterGreaterThan.Count : 0
			});
		}

		// Token: 0x060028BD RID: 10429 RVA: 0x0003B270 File Offset: 0x00039470
		public override bool IsValid()
		{
			int num = (this.matchAttributeFilterLessThan != null) ? this.matchAttributeFilterLessThan.Count : 0;
			num += ((this.matchAttributeFilterEqualTo != null) ? this.matchAttributeFilterEqualTo.Count : 0);
			num += ((this.matchAttributeFilterGreaterThan != null) ? this.matchAttributeFilterGreaterThan.Count : 0);
			return base.IsValid() && (this.pageSize >= 1 || this.pageSize <= 1000) && num <= 10;
		}

		// Token: 0x0400094D RID: 2381
		[Obsolete("This bool is deprecated in favor of filterOutPrivateMatches")]
		public bool includePasswordMatches;
	}
}
