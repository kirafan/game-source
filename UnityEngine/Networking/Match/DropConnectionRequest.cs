﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000260 RID: 608
	internal class DropConnectionRequest : Request
	{
		// Token: 0x170009BA RID: 2490
		// (get) Token: 0x0600289E RID: 10398 RVA: 0x0003AE4C File Offset: 0x0003904C
		// (set) Token: 0x0600289F RID: 10399 RVA: 0x0003AE68 File Offset: 0x00039068
		public NetworkID networkId { get; set; }

		// Token: 0x170009BB RID: 2491
		// (get) Token: 0x060028A0 RID: 10400 RVA: 0x0003AE74 File Offset: 0x00039074
		// (set) Token: 0x060028A1 RID: 10401 RVA: 0x0003AE90 File Offset: 0x00039090
		public NodeID nodeId { get; set; }

		// Token: 0x060028A2 RID: 10402 RVA: 0x0003AE9C File Offset: 0x0003909C
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:0x{1},nodeId:0x{2}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X"),
				this.nodeId.ToString("X")
			});
		}

		// Token: 0x060028A3 RID: 10403 RVA: 0x0003AF04 File Offset: 0x00039104
		public override bool IsValid()
		{
			return base.IsValid() && this.networkId != NetworkID.Invalid && this.nodeId != NodeID.Invalid;
		}
	}
}
