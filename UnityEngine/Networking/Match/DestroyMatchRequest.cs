﻿using System;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking.Match
{
	// Token: 0x0200025F RID: 607
	internal class DestroyMatchRequest : Request
	{
		// Token: 0x170009B9 RID: 2489
		// (get) Token: 0x06002899 RID: 10393 RVA: 0x0003ADA0 File Offset: 0x00038FA0
		// (set) Token: 0x0600289A RID: 10394 RVA: 0x0003ADBC File Offset: 0x00038FBC
		public NetworkID networkId { get; set; }

		// Token: 0x0600289B RID: 10395 RVA: 0x0003ADC8 File Offset: 0x00038FC8
		public override string ToString()
		{
			return UnityString.Format("[{0}]-networkId:0x{1}", new object[]
			{
				base.ToString(),
				this.networkId.ToString("X")
			});
		}

		// Token: 0x0600289C RID: 10396 RVA: 0x0003AE14 File Offset: 0x00039014
		public override bool IsValid()
		{
			return base.IsValid() && this.networkId != NetworkID.Invalid;
		}
	}
}
