﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking.Match
{
	// Token: 0x02000265 RID: 613
	internal class ListMatchResponse : BasicResponse
	{
		// Token: 0x060028DE RID: 10462 RVA: 0x0003B7B8 File Offset: 0x000399B8
		public ListMatchResponse()
		{
			this.matches = new List<MatchDesc>();
		}

		// Token: 0x060028DF RID: 10463 RVA: 0x0003B7CC File Offset: 0x000399CC
		public ListMatchResponse(List<MatchDesc> otherMatches)
		{
			this.matches = otherMatches;
		}

		// Token: 0x170009D3 RID: 2515
		// (get) Token: 0x060028E0 RID: 10464 RVA: 0x0003B7DC File Offset: 0x000399DC
		// (set) Token: 0x060028E1 RID: 10465 RVA: 0x0003B7F8 File Offset: 0x000399F8
		public List<MatchDesc> matches { get; set; }

		// Token: 0x060028E2 RID: 10466 RVA: 0x0003B804 File Offset: 0x00039A04
		public override string ToString()
		{
			return UnityString.Format("[{0}]-matches.Count:{1}", new object[]
			{
				base.ToString(),
				(this.matches != null) ? this.matches.Count : 0
			});
		}

		// Token: 0x060028E3 RID: 10467 RVA: 0x0003B858 File Offset: 0x00039A58
		public override void Parse(object obj)
		{
			base.Parse(obj);
			IDictionary<string, object> dictionary = obj as IDictionary<string, object>;
			if (dictionary != null)
			{
				this.matches = base.ParseJSONList<MatchDesc>("matches", obj, dictionary);
				return;
			}
			throw new FormatException("While parsing JSON response, found obj is not of type IDictionary<string,object>:" + obj.ToString());
		}
	}
}
