﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200027D RID: 637
	[Serializable]
	public class ChannelQOS
	{
		// Token: 0x06002994 RID: 10644 RVA: 0x0003D540 File Offset: 0x0003B740
		public ChannelQOS(QosType value)
		{
			this.m_Type = value;
		}

		// Token: 0x06002995 RID: 10645 RVA: 0x0003D550 File Offset: 0x0003B750
		public ChannelQOS()
		{
			this.m_Type = QosType.Unreliable;
		}

		// Token: 0x06002996 RID: 10646 RVA: 0x0003D560 File Offset: 0x0003B760
		public ChannelQOS(ChannelQOS channel)
		{
			if (channel == null)
			{
				throw new NullReferenceException("channel is not defined");
			}
			this.m_Type = channel.m_Type;
		}

		// Token: 0x170009EF RID: 2543
		// (get) Token: 0x06002997 RID: 10647 RVA: 0x0003D588 File Offset: 0x0003B788
		public QosType QOS
		{
			get
			{
				return this.m_Type;
			}
		}

		// Token: 0x040009AC RID: 2476
		[SerializeField]
		internal QosType m_Type;
	}
}
