﻿using System;
using System.Text;

namespace UnityEngine.Networking
{
	// Token: 0x0200024B RID: 587
	public class MultipartFormFileSection : IMultipartFormSection
	{
		// Token: 0x060027D1 RID: 10193 RVA: 0x0003951C File Offset: 0x0003771C
		public MultipartFormFileSection(string name, byte[] data, string fileName, string contentType)
		{
			if (data == null || data.Length < 1)
			{
				throw new ArgumentException("Cannot create a multipart form file section without body data");
			}
			if (string.IsNullOrEmpty(fileName))
			{
				fileName = "file.dat";
			}
			if (string.IsNullOrEmpty(contentType))
			{
				contentType = "application/octet-stream";
			}
			this.Init(name, data, fileName, contentType);
		}

		// Token: 0x060027D2 RID: 10194 RVA: 0x00039580 File Offset: 0x00037780
		public MultipartFormFileSection(byte[] data) : this(null, data, null, null)
		{
		}

		// Token: 0x060027D3 RID: 10195 RVA: 0x00039590 File Offset: 0x00037790
		public MultipartFormFileSection(string fileName, byte[] data) : this(null, data, fileName, null)
		{
		}

		// Token: 0x060027D4 RID: 10196 RVA: 0x000395A0 File Offset: 0x000377A0
		public MultipartFormFileSection(string name, string data, Encoding dataEncoding, string fileName)
		{
			if (data == null || data.Length < 1)
			{
				throw new ArgumentException("Cannot create a multipart form file section without body data");
			}
			if (dataEncoding == null)
			{
				dataEncoding = Encoding.UTF8;
			}
			byte[] bytes = dataEncoding.GetBytes(data);
			if (string.IsNullOrEmpty(fileName))
			{
				fileName = "file.txt";
			}
			if (string.IsNullOrEmpty(this.content))
			{
				this.content = "text/plain; charset=" + dataEncoding.WebName;
			}
			this.Init(name, bytes, fileName, this.content);
		}

		// Token: 0x060027D5 RID: 10197 RVA: 0x00039638 File Offset: 0x00037838
		public MultipartFormFileSection(string data, Encoding dataEncoding, string fileName) : this(null, data, dataEncoding, fileName)
		{
		}

		// Token: 0x060027D6 RID: 10198 RVA: 0x00039648 File Offset: 0x00037848
		public MultipartFormFileSection(string data, string fileName) : this(data, null, fileName)
		{
		}

		// Token: 0x060027D7 RID: 10199 RVA: 0x00039654 File Offset: 0x00037854
		private void Init(string name, byte[] data, string fileName, string contentType)
		{
			this.name = name;
			this.data = data;
			this.file = fileName;
			this.content = contentType;
		}

		// Token: 0x17000989 RID: 2441
		// (get) Token: 0x060027D8 RID: 10200 RVA: 0x00039674 File Offset: 0x00037874
		public string sectionName
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x1700098A RID: 2442
		// (get) Token: 0x060027D9 RID: 10201 RVA: 0x00039690 File Offset: 0x00037890
		public byte[] sectionData
		{
			get
			{
				return this.data;
			}
		}

		// Token: 0x1700098B RID: 2443
		// (get) Token: 0x060027DA RID: 10202 RVA: 0x000396AC File Offset: 0x000378AC
		public string fileName
		{
			get
			{
				return this.file;
			}
		}

		// Token: 0x1700098C RID: 2444
		// (get) Token: 0x060027DB RID: 10203 RVA: 0x000396C8 File Offset: 0x000378C8
		public string contentType
		{
			get
			{
				return this.content;
			}
		}

		// Token: 0x04000912 RID: 2322
		private string name;

		// Token: 0x04000913 RID: 2323
		private byte[] data;

		// Token: 0x04000914 RID: 2324
		private string file;

		// Token: 0x04000915 RID: 2325
		private string content;
	}
}
