﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200027C RID: 636
	public enum ReactorModel
	{
		// Token: 0x040009AA RID: 2474
		SelectReactor,
		// Token: 0x040009AB RID: 2475
		FixRateReactor
	}
}
