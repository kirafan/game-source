﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x0200027E RID: 638
	[Serializable]
	public class ConnectionConfig
	{
		// Token: 0x06002998 RID: 10648 RVA: 0x0003D5A4 File Offset: 0x0003B7A4
		public ConnectionConfig()
		{
			this.m_PacketSize = 1500;
			this.m_FragmentSize = 500;
			this.m_ResendTimeout = 1200U;
			this.m_DisconnectTimeout = 2000U;
			this.m_ConnectTimeout = 2000U;
			this.m_MinUpdateTimeout = 10U;
			this.m_PingTimeout = 500U;
			this.m_ReducedPingTimeout = 100U;
			this.m_AllCostTimeout = 20U;
			this.m_NetworkDropThreshold = 5;
			this.m_OverflowDropThreshold = 5;
			this.m_MaxConnectionAttempt = 10;
			this.m_AckDelay = 33U;
			this.m_MaxCombinedReliableMessageSize = 100;
			this.m_MaxCombinedReliableMessageCount = 10;
			this.m_MaxSentMessageQueueSize = 128;
			this.m_IsAcksLong = false;
			this.m_UsePlatformSpecificProtocols = false;
			this.m_WebSocketReceiveBufferMaxSize = 0;
		}

		// Token: 0x06002999 RID: 10649 RVA: 0x0003D66C File Offset: 0x0003B86C
		public ConnectionConfig(ConnectionConfig config)
		{
			if (config == null)
			{
				throw new NullReferenceException("config is not defined");
			}
			this.m_PacketSize = config.m_PacketSize;
			this.m_FragmentSize = config.m_FragmentSize;
			this.m_ResendTimeout = config.m_ResendTimeout;
			this.m_DisconnectTimeout = config.m_DisconnectTimeout;
			this.m_ConnectTimeout = config.m_ConnectTimeout;
			this.m_MinUpdateTimeout = config.m_MinUpdateTimeout;
			this.m_PingTimeout = config.m_PingTimeout;
			this.m_ReducedPingTimeout = config.m_ReducedPingTimeout;
			this.m_AllCostTimeout = config.m_AllCostTimeout;
			this.m_NetworkDropThreshold = config.m_NetworkDropThreshold;
			this.m_OverflowDropThreshold = config.m_OverflowDropThreshold;
			this.m_MaxConnectionAttempt = config.m_MaxConnectionAttempt;
			this.m_AckDelay = config.m_AckDelay;
			this.m_MaxCombinedReliableMessageSize = config.MaxCombinedReliableMessageSize;
			this.m_MaxCombinedReliableMessageCount = config.m_MaxCombinedReliableMessageCount;
			this.m_MaxSentMessageQueueSize = config.m_MaxSentMessageQueueSize;
			this.m_IsAcksLong = config.m_IsAcksLong;
			this.m_UsePlatformSpecificProtocols = config.m_UsePlatformSpecificProtocols;
			this.m_WebSocketReceiveBufferMaxSize = config.m_WebSocketReceiveBufferMaxSize;
			foreach (ChannelQOS channel in config.m_Channels)
			{
				this.m_Channels.Add(new ChannelQOS(channel));
			}
		}

		// Token: 0x0600299A RID: 10650 RVA: 0x0003D7DC File Offset: 0x0003B9DC
		public static void Validate(ConnectionConfig config)
		{
			if (config.m_PacketSize < 128)
			{
				throw new ArgumentOutOfRangeException("PacketSize should be > " + 128.ToString());
			}
			if (config.m_FragmentSize >= config.m_PacketSize - 128)
			{
				throw new ArgumentOutOfRangeException("FragmentSize should be < PacketSize - " + 128.ToString());
			}
			if (config.m_Channels.Count > 255)
			{
				throw new ArgumentOutOfRangeException("Channels number should be less than 256");
			}
		}

		// Token: 0x170009F0 RID: 2544
		// (get) Token: 0x0600299B RID: 10651 RVA: 0x0003D878 File Offset: 0x0003BA78
		// (set) Token: 0x0600299C RID: 10652 RVA: 0x0003D894 File Offset: 0x0003BA94
		public ushort PacketSize
		{
			get
			{
				return this.m_PacketSize;
			}
			set
			{
				this.m_PacketSize = value;
			}
		}

		// Token: 0x170009F1 RID: 2545
		// (get) Token: 0x0600299D RID: 10653 RVA: 0x0003D8A0 File Offset: 0x0003BAA0
		// (set) Token: 0x0600299E RID: 10654 RVA: 0x0003D8BC File Offset: 0x0003BABC
		public ushort FragmentSize
		{
			get
			{
				return this.m_FragmentSize;
			}
			set
			{
				this.m_FragmentSize = value;
			}
		}

		// Token: 0x170009F2 RID: 2546
		// (get) Token: 0x0600299F RID: 10655 RVA: 0x0003D8C8 File Offset: 0x0003BAC8
		// (set) Token: 0x060029A0 RID: 10656 RVA: 0x0003D8E4 File Offset: 0x0003BAE4
		public uint ResendTimeout
		{
			get
			{
				return this.m_ResendTimeout;
			}
			set
			{
				this.m_ResendTimeout = value;
			}
		}

		// Token: 0x170009F3 RID: 2547
		// (get) Token: 0x060029A1 RID: 10657 RVA: 0x0003D8F0 File Offset: 0x0003BAF0
		// (set) Token: 0x060029A2 RID: 10658 RVA: 0x0003D90C File Offset: 0x0003BB0C
		public uint DisconnectTimeout
		{
			get
			{
				return this.m_DisconnectTimeout;
			}
			set
			{
				this.m_DisconnectTimeout = value;
			}
		}

		// Token: 0x170009F4 RID: 2548
		// (get) Token: 0x060029A3 RID: 10659 RVA: 0x0003D918 File Offset: 0x0003BB18
		// (set) Token: 0x060029A4 RID: 10660 RVA: 0x0003D934 File Offset: 0x0003BB34
		public uint ConnectTimeout
		{
			get
			{
				return this.m_ConnectTimeout;
			}
			set
			{
				this.m_ConnectTimeout = value;
			}
		}

		// Token: 0x170009F5 RID: 2549
		// (get) Token: 0x060029A5 RID: 10661 RVA: 0x0003D940 File Offset: 0x0003BB40
		// (set) Token: 0x060029A6 RID: 10662 RVA: 0x0003D95C File Offset: 0x0003BB5C
		public uint MinUpdateTimeout
		{
			get
			{
				return this.m_MinUpdateTimeout;
			}
			set
			{
				if (value == 0U)
				{
					throw new ArgumentOutOfRangeException("Minimal update timeout should be > 0");
				}
				this.m_MinUpdateTimeout = value;
			}
		}

		// Token: 0x170009F6 RID: 2550
		// (get) Token: 0x060029A7 RID: 10663 RVA: 0x0003D978 File Offset: 0x0003BB78
		// (set) Token: 0x060029A8 RID: 10664 RVA: 0x0003D994 File Offset: 0x0003BB94
		public uint PingTimeout
		{
			get
			{
				return this.m_PingTimeout;
			}
			set
			{
				this.m_PingTimeout = value;
			}
		}

		// Token: 0x170009F7 RID: 2551
		// (get) Token: 0x060029A9 RID: 10665 RVA: 0x0003D9A0 File Offset: 0x0003BBA0
		// (set) Token: 0x060029AA RID: 10666 RVA: 0x0003D9BC File Offset: 0x0003BBBC
		public uint ReducedPingTimeout
		{
			get
			{
				return this.m_ReducedPingTimeout;
			}
			set
			{
				this.m_ReducedPingTimeout = value;
			}
		}

		// Token: 0x170009F8 RID: 2552
		// (get) Token: 0x060029AB RID: 10667 RVA: 0x0003D9C8 File Offset: 0x0003BBC8
		// (set) Token: 0x060029AC RID: 10668 RVA: 0x0003D9E4 File Offset: 0x0003BBE4
		public uint AllCostTimeout
		{
			get
			{
				return this.m_AllCostTimeout;
			}
			set
			{
				this.m_AllCostTimeout = value;
			}
		}

		// Token: 0x170009F9 RID: 2553
		// (get) Token: 0x060029AD RID: 10669 RVA: 0x0003D9F0 File Offset: 0x0003BBF0
		// (set) Token: 0x060029AE RID: 10670 RVA: 0x0003DA0C File Offset: 0x0003BC0C
		public byte NetworkDropThreshold
		{
			get
			{
				return this.m_NetworkDropThreshold;
			}
			set
			{
				this.m_NetworkDropThreshold = value;
			}
		}

		// Token: 0x170009FA RID: 2554
		// (get) Token: 0x060029AF RID: 10671 RVA: 0x0003DA18 File Offset: 0x0003BC18
		// (set) Token: 0x060029B0 RID: 10672 RVA: 0x0003DA34 File Offset: 0x0003BC34
		public byte OverflowDropThreshold
		{
			get
			{
				return this.m_OverflowDropThreshold;
			}
			set
			{
				this.m_OverflowDropThreshold = value;
			}
		}

		// Token: 0x170009FB RID: 2555
		// (get) Token: 0x060029B1 RID: 10673 RVA: 0x0003DA40 File Offset: 0x0003BC40
		// (set) Token: 0x060029B2 RID: 10674 RVA: 0x0003DA5C File Offset: 0x0003BC5C
		public byte MaxConnectionAttempt
		{
			get
			{
				return this.m_MaxConnectionAttempt;
			}
			set
			{
				this.m_MaxConnectionAttempt = value;
			}
		}

		// Token: 0x170009FC RID: 2556
		// (get) Token: 0x060029B3 RID: 10675 RVA: 0x0003DA68 File Offset: 0x0003BC68
		// (set) Token: 0x060029B4 RID: 10676 RVA: 0x0003DA84 File Offset: 0x0003BC84
		public uint AckDelay
		{
			get
			{
				return this.m_AckDelay;
			}
			set
			{
				this.m_AckDelay = value;
			}
		}

		// Token: 0x170009FD RID: 2557
		// (get) Token: 0x060029B5 RID: 10677 RVA: 0x0003DA90 File Offset: 0x0003BC90
		// (set) Token: 0x060029B6 RID: 10678 RVA: 0x0003DAAC File Offset: 0x0003BCAC
		public ushort MaxCombinedReliableMessageSize
		{
			get
			{
				return this.m_MaxCombinedReliableMessageSize;
			}
			set
			{
				this.m_MaxCombinedReliableMessageSize = value;
			}
		}

		// Token: 0x170009FE RID: 2558
		// (get) Token: 0x060029B7 RID: 10679 RVA: 0x0003DAB8 File Offset: 0x0003BCB8
		// (set) Token: 0x060029B8 RID: 10680 RVA: 0x0003DAD4 File Offset: 0x0003BCD4
		public ushort MaxCombinedReliableMessageCount
		{
			get
			{
				return this.m_MaxCombinedReliableMessageCount;
			}
			set
			{
				this.m_MaxCombinedReliableMessageCount = value;
			}
		}

		// Token: 0x170009FF RID: 2559
		// (get) Token: 0x060029B9 RID: 10681 RVA: 0x0003DAE0 File Offset: 0x0003BCE0
		// (set) Token: 0x060029BA RID: 10682 RVA: 0x0003DAFC File Offset: 0x0003BCFC
		public ushort MaxSentMessageQueueSize
		{
			get
			{
				return this.m_MaxSentMessageQueueSize;
			}
			set
			{
				this.m_MaxSentMessageQueueSize = value;
			}
		}

		// Token: 0x17000A00 RID: 2560
		// (get) Token: 0x060029BB RID: 10683 RVA: 0x0003DB08 File Offset: 0x0003BD08
		// (set) Token: 0x060029BC RID: 10684 RVA: 0x0003DB24 File Offset: 0x0003BD24
		public bool IsAcksLong
		{
			get
			{
				return this.m_IsAcksLong;
			}
			set
			{
				this.m_IsAcksLong = value;
			}
		}

		// Token: 0x17000A01 RID: 2561
		// (get) Token: 0x060029BD RID: 10685 RVA: 0x0003DB30 File Offset: 0x0003BD30
		// (set) Token: 0x060029BE RID: 10686 RVA: 0x0003DB4C File Offset: 0x0003BD4C
		public bool UsePlatformSpecificProtocols
		{
			get
			{
				return this.m_UsePlatformSpecificProtocols;
			}
			set
			{
				if (value && Application.platform != RuntimePlatform.PS4 && Application.platform != RuntimePlatform.PSP2)
				{
					throw new ArgumentOutOfRangeException("Platform specific protocols are not supported on this platform");
				}
				this.m_UsePlatformSpecificProtocols = value;
			}
		}

		// Token: 0x17000A02 RID: 2562
		// (get) Token: 0x060029BF RID: 10687 RVA: 0x0003DB80 File Offset: 0x0003BD80
		// (set) Token: 0x060029C0 RID: 10688 RVA: 0x0003DB9C File Offset: 0x0003BD9C
		public ushort WebSocketReceiveBufferMaxSize
		{
			get
			{
				return this.m_WebSocketReceiveBufferMaxSize;
			}
			set
			{
				this.m_WebSocketReceiveBufferMaxSize = value;
			}
		}

		// Token: 0x17000A03 RID: 2563
		// (get) Token: 0x060029C1 RID: 10689 RVA: 0x0003DBA8 File Offset: 0x0003BDA8
		public int ChannelCount
		{
			get
			{
				return this.m_Channels.Count;
			}
		}

		// Token: 0x060029C2 RID: 10690 RVA: 0x0003DBC8 File Offset: 0x0003BDC8
		public byte AddChannel(QosType value)
		{
			if (this.m_Channels.Count > 255)
			{
				throw new ArgumentOutOfRangeException("Channels Count should be less than 256");
			}
			if (!Enum.IsDefined(typeof(QosType), value))
			{
				throw new ArgumentOutOfRangeException("requested qos type doesn't exist: " + (int)value);
			}
			ChannelQOS item = new ChannelQOS(value);
			this.m_Channels.Add(item);
			return (byte)(this.m_Channels.Count - 1);
		}

		// Token: 0x060029C3 RID: 10691 RVA: 0x0003DC50 File Offset: 0x0003BE50
		public QosType GetChannel(byte idx)
		{
			if ((int)idx >= this.m_Channels.Count)
			{
				throw new ArgumentOutOfRangeException("requested index greater than maximum channels count");
			}
			return this.m_Channels[(int)idx].QOS;
		}

		// Token: 0x17000A04 RID: 2564
		// (get) Token: 0x060029C4 RID: 10692 RVA: 0x0003DC94 File Offset: 0x0003BE94
		public List<ChannelQOS> Channels
		{
			get
			{
				return this.m_Channels;
			}
		}

		// Token: 0x040009AD RID: 2477
		private const int g_MinPacketSize = 128;

		// Token: 0x040009AE RID: 2478
		[SerializeField]
		private ushort m_PacketSize;

		// Token: 0x040009AF RID: 2479
		[SerializeField]
		private ushort m_FragmentSize;

		// Token: 0x040009B0 RID: 2480
		[SerializeField]
		private uint m_ResendTimeout;

		// Token: 0x040009B1 RID: 2481
		[SerializeField]
		private uint m_DisconnectTimeout;

		// Token: 0x040009B2 RID: 2482
		[SerializeField]
		private uint m_ConnectTimeout;

		// Token: 0x040009B3 RID: 2483
		[SerializeField]
		private uint m_MinUpdateTimeout;

		// Token: 0x040009B4 RID: 2484
		[SerializeField]
		private uint m_PingTimeout;

		// Token: 0x040009B5 RID: 2485
		[SerializeField]
		private uint m_ReducedPingTimeout;

		// Token: 0x040009B6 RID: 2486
		[SerializeField]
		private uint m_AllCostTimeout;

		// Token: 0x040009B7 RID: 2487
		[SerializeField]
		private byte m_NetworkDropThreshold;

		// Token: 0x040009B8 RID: 2488
		[SerializeField]
		private byte m_OverflowDropThreshold;

		// Token: 0x040009B9 RID: 2489
		[SerializeField]
		private byte m_MaxConnectionAttempt;

		// Token: 0x040009BA RID: 2490
		[SerializeField]
		private uint m_AckDelay;

		// Token: 0x040009BB RID: 2491
		[SerializeField]
		private ushort m_MaxCombinedReliableMessageSize;

		// Token: 0x040009BC RID: 2492
		[SerializeField]
		private ushort m_MaxCombinedReliableMessageCount;

		// Token: 0x040009BD RID: 2493
		[SerializeField]
		private ushort m_MaxSentMessageQueueSize;

		// Token: 0x040009BE RID: 2494
		[SerializeField]
		private bool m_IsAcksLong;

		// Token: 0x040009BF RID: 2495
		[SerializeField]
		private bool m_UsePlatformSpecificProtocols;

		// Token: 0x040009C0 RID: 2496
		[SerializeField]
		private ushort m_WebSocketReceiveBufferMaxSize;

		// Token: 0x040009C1 RID: 2497
		[SerializeField]
		internal List<ChannelQOS> m_Channels = new List<ChannelQOS>();
	}
}
