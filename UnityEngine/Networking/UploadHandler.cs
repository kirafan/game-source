﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine.Networking
{
	// Token: 0x0200024D RID: 589
	[StructLayout(LayoutKind.Sequential)]
	public class UploadHandler : IDisposable
	{
		// Token: 0x060027DF RID: 10207 RVA: 0x00039854 File Offset: 0x00037A54
		internal UploadHandler()
		{
		}

		// Token: 0x060027E0 RID: 10208
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalCreateRaw(byte[] data);

		// Token: 0x060027E1 RID: 10209
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InternalDestroy();

		// Token: 0x060027E2 RID: 10210 RVA: 0x00039860 File Offset: 0x00037A60
		~UploadHandler()
		{
			this.InternalDestroy();
		}

		// Token: 0x060027E3 RID: 10211 RVA: 0x00039890 File Offset: 0x00037A90
		public void Dispose()
		{
			this.InternalDestroy();
			GC.SuppressFinalize(this);
		}

		// Token: 0x1700098D RID: 2445
		// (get) Token: 0x060027E4 RID: 10212 RVA: 0x000398A0 File Offset: 0x00037AA0
		public byte[] data
		{
			get
			{
				return this.GetData();
			}
		}

		// Token: 0x1700098E RID: 2446
		// (get) Token: 0x060027E5 RID: 10213 RVA: 0x000398BC File Offset: 0x00037ABC
		// (set) Token: 0x060027E6 RID: 10214 RVA: 0x000398D8 File Offset: 0x00037AD8
		public string contentType
		{
			get
			{
				return this.GetContentType();
			}
			set
			{
				this.SetContentType(value);
			}
		}

		// Token: 0x1700098F RID: 2447
		// (get) Token: 0x060027E7 RID: 10215 RVA: 0x000398E4 File Offset: 0x00037AE4
		public float progress
		{
			get
			{
				return this.GetProgress();
			}
		}

		// Token: 0x060027E8 RID: 10216 RVA: 0x00039900 File Offset: 0x00037B00
		internal virtual byte[] GetData()
		{
			return null;
		}

		// Token: 0x060027E9 RID: 10217 RVA: 0x00039918 File Offset: 0x00037B18
		internal virtual string GetContentType()
		{
			return "text/plain";
		}

		// Token: 0x060027EA RID: 10218 RVA: 0x00039934 File Offset: 0x00037B34
		internal virtual void SetContentType(string newContentType)
		{
		}

		// Token: 0x060027EB RID: 10219 RVA: 0x00039938 File Offset: 0x00037B38
		internal virtual float GetProgress()
		{
			return 0.5f;
		}

		// Token: 0x04000917 RID: 2327
		[NonSerialized]
		internal IntPtr m_Ptr;
	}
}
