﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine.Networking
{
	// Token: 0x02000251 RID: 593
	[StructLayout(LayoutKind.Sequential)]
	public class DownloadHandlerScript : DownloadHandler
	{
		// Token: 0x06002810 RID: 10256 RVA: 0x00039BBC File Offset: 0x00037DBC
		public DownloadHandlerScript()
		{
			base.InternalCreateScript();
		}

		// Token: 0x06002811 RID: 10257 RVA: 0x00039BCC File Offset: 0x00037DCC
		public DownloadHandlerScript(byte[] preallocatedBuffer)
		{
			if (preallocatedBuffer == null || preallocatedBuffer.Length < 1)
			{
				throw new ArgumentException("Cannot create a preallocated-buffer DownloadHandlerScript backed by a null or zero-length array");
			}
			base.InternalCreateScript();
			this.InternalSetPreallocatedBuffer(preallocatedBuffer);
		}

		// Token: 0x06002812 RID: 10258
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InternalSetPreallocatedBuffer(byte[] buffer);
	}
}
