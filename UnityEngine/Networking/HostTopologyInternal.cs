﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Networking
{
	// Token: 0x02000283 RID: 643
	internal sealed class HostTopologyInternal : IDisposable
	{
		// Token: 0x060029FC RID: 10748 RVA: 0x0003E1F4 File Offset: 0x0003C3F4
		public HostTopologyInternal(HostTopology topology)
		{
			ConnectionConfigInternal config = new ConnectionConfigInternal(topology.DefaultConfig);
			this.InitWrapper(config, topology.MaxDefaultConnections);
			for (int i = 1; i <= topology.SpecialConnectionConfigsCount; i++)
			{
				ConnectionConfig specialConnectionConfig = topology.GetSpecialConnectionConfig(i);
				ConnectionConfigInternal config2 = new ConnectionConfigInternal(specialConnectionConfig);
				this.AddSpecialConnectionConfig(config2);
			}
			this.InitOtherParameters(topology);
		}

		// Token: 0x060029FD RID: 10749
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitWrapper(ConnectionConfigInternal config, int maxDefaultConnections);

		// Token: 0x060029FE RID: 10750 RVA: 0x0003E258 File Offset: 0x0003C458
		private int AddSpecialConnectionConfig(ConnectionConfigInternal config)
		{
			return this.AddSpecialConnectionConfigWrapper(config);
		}

		// Token: 0x060029FF RID: 10751
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int AddSpecialConnectionConfigWrapper(ConnectionConfigInternal config);

		// Token: 0x06002A00 RID: 10752 RVA: 0x0003E274 File Offset: 0x0003C474
		private void InitOtherParameters(HostTopology topology)
		{
			this.InitReceivedPoolSize(topology.ReceivedMessagePoolSize);
			this.InitSentMessagePoolSize(topology.SentMessagePoolSize);
			this.InitMessagePoolSizeGrowthFactor(topology.MessagePoolSizeGrowthFactor);
		}

		// Token: 0x06002A01 RID: 10753
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitReceivedPoolSize(ushort pool);

		// Token: 0x06002A02 RID: 10754
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitSentMessagePoolSize(ushort pool);

		// Token: 0x06002A03 RID: 10755
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitMessagePoolSizeGrowthFactor(float factor);

		// Token: 0x06002A04 RID: 10756
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Dispose();

		// Token: 0x06002A05 RID: 10757 RVA: 0x0003E29C File Offset: 0x0003C49C
		~HostTopologyInternal()
		{
			this.Dispose();
		}

		// Token: 0x040009CF RID: 2511
		internal IntPtr m_Ptr;
	}
}
