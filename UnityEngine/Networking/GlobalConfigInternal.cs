﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Networking
{
	// Token: 0x02000284 RID: 644
	internal sealed class GlobalConfigInternal : IDisposable
	{
		// Token: 0x06002A06 RID: 10758 RVA: 0x0003E2CC File Offset: 0x0003C4CC
		public GlobalConfigInternal(GlobalConfig config)
		{
			this.InitWrapper();
			this.InitThreadAwakeTimeout(config.ThreadAwakeTimeout);
			this.InitReactorModel((byte)config.ReactorModel);
			this.InitReactorMaximumReceivedMessages(config.ReactorMaximumReceivedMessages);
			this.InitReactorMaximumSentMessages(config.ReactorMaximumSentMessages);
			this.InitMaxPacketSize(config.MaxPacketSize);
		}

		// Token: 0x06002A07 RID: 10759
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitWrapper();

		// Token: 0x06002A08 RID: 10760
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitThreadAwakeTimeout(uint ms);

		// Token: 0x06002A09 RID: 10761
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitReactorModel(byte model);

		// Token: 0x06002A0A RID: 10762
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitReactorMaximumReceivedMessages(ushort size);

		// Token: 0x06002A0B RID: 10763
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitReactorMaximumSentMessages(ushort size);

		// Token: 0x06002A0C RID: 10764
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitMaxPacketSize(ushort size);

		// Token: 0x06002A0D RID: 10765
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Dispose();

		// Token: 0x06002A0E RID: 10766 RVA: 0x0003E324 File Offset: 0x0003C524
		~GlobalConfigInternal()
		{
			this.Dispose();
		}

		// Token: 0x040009D0 RID: 2512
		internal IntPtr m_Ptr;
	}
}
