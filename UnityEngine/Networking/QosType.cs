﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200027A RID: 634
	public enum QosType
	{
		// Token: 0x04000993 RID: 2451
		Unreliable,
		// Token: 0x04000994 RID: 2452
		UnreliableFragmented,
		// Token: 0x04000995 RID: 2453
		UnreliableSequenced,
		// Token: 0x04000996 RID: 2454
		Reliable,
		// Token: 0x04000997 RID: 2455
		ReliableFragmented,
		// Token: 0x04000998 RID: 2456
		ReliableSequenced,
		// Token: 0x04000999 RID: 2457
		StateUpdate,
		// Token: 0x0400099A RID: 2458
		ReliableStateUpdate,
		// Token: 0x0400099B RID: 2459
		AllCostDelivery
	}
}
