﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000280 RID: 640
	[Serializable]
	public class GlobalConfig
	{
		// Token: 0x060029D3 RID: 10707 RVA: 0x0003DF40 File Offset: 0x0003C140
		public GlobalConfig()
		{
			this.m_ThreadAwakeTimeout = 1U;
			this.m_ReactorModel = ReactorModel.SelectReactor;
			this.m_ReactorMaximumReceivedMessages = 1024;
			this.m_ReactorMaximumSentMessages = 1024;
			this.m_MaxPacketSize = 2000;
		}

		// Token: 0x17000A0C RID: 2572
		// (get) Token: 0x060029D4 RID: 10708 RVA: 0x0003DF78 File Offset: 0x0003C178
		// (set) Token: 0x060029D5 RID: 10709 RVA: 0x0003DF94 File Offset: 0x0003C194
		public uint ThreadAwakeTimeout
		{
			get
			{
				return this.m_ThreadAwakeTimeout;
			}
			set
			{
				if (value == 0U)
				{
					throw new ArgumentOutOfRangeException("Minimal thread awake timeout should be > 0");
				}
				this.m_ThreadAwakeTimeout = value;
			}
		}

		// Token: 0x17000A0D RID: 2573
		// (get) Token: 0x060029D6 RID: 10710 RVA: 0x0003DFB0 File Offset: 0x0003C1B0
		// (set) Token: 0x060029D7 RID: 10711 RVA: 0x0003DFCC File Offset: 0x0003C1CC
		public ReactorModel ReactorModel
		{
			get
			{
				return this.m_ReactorModel;
			}
			set
			{
				this.m_ReactorModel = value;
			}
		}

		// Token: 0x17000A0E RID: 2574
		// (get) Token: 0x060029D8 RID: 10712 RVA: 0x0003DFD8 File Offset: 0x0003C1D8
		// (set) Token: 0x060029D9 RID: 10713 RVA: 0x0003DFF4 File Offset: 0x0003C1F4
		public ushort ReactorMaximumReceivedMessages
		{
			get
			{
				return this.m_ReactorMaximumReceivedMessages;
			}
			set
			{
				this.m_ReactorMaximumReceivedMessages = value;
			}
		}

		// Token: 0x17000A0F RID: 2575
		// (get) Token: 0x060029DA RID: 10714 RVA: 0x0003E000 File Offset: 0x0003C200
		// (set) Token: 0x060029DB RID: 10715 RVA: 0x0003E01C File Offset: 0x0003C21C
		public ushort ReactorMaximumSentMessages
		{
			get
			{
				return this.m_ReactorMaximumSentMessages;
			}
			set
			{
				this.m_ReactorMaximumSentMessages = value;
			}
		}

		// Token: 0x17000A10 RID: 2576
		// (get) Token: 0x060029DC RID: 10716 RVA: 0x0003E028 File Offset: 0x0003C228
		// (set) Token: 0x060029DD RID: 10717 RVA: 0x0003E044 File Offset: 0x0003C244
		public ushort MaxPacketSize
		{
			get
			{
				return this.m_MaxPacketSize;
			}
			set
			{
				this.m_MaxPacketSize = value;
			}
		}

		// Token: 0x040009C8 RID: 2504
		[SerializeField]
		private uint m_ThreadAwakeTimeout;

		// Token: 0x040009C9 RID: 2505
		[SerializeField]
		private ReactorModel m_ReactorModel;

		// Token: 0x040009CA RID: 2506
		[SerializeField]
		private ushort m_ReactorMaximumReceivedMessages;

		// Token: 0x040009CB RID: 2507
		[SerializeField]
		private ushort m_ReactorMaximumSentMessages;

		// Token: 0x040009CC RID: 2508
		[SerializeField]
		private ushort m_MaxPacketSize;
	}
}
