﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine.Scripting;

namespace UnityEngine.Networking
{
	// Token: 0x0200024F RID: 591
	[StructLayout(LayoutKind.Sequential)]
	public class DownloadHandler : IDisposable
	{
		// Token: 0x060027F5 RID: 10229 RVA: 0x000399C4 File Offset: 0x00037BC4
		internal DownloadHandler()
		{
		}

		// Token: 0x060027F6 RID: 10230
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalCreateBuffer();

		// Token: 0x060027F7 RID: 10231
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalCreateScript();

		// Token: 0x060027F8 RID: 10232
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalCreateTexture(bool readable);

		// Token: 0x060027F9 RID: 10233
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalCreateAssetBundle(string url, uint crc);

		// Token: 0x060027FA RID: 10234 RVA: 0x000399D0 File Offset: 0x00037BD0
		internal void InternalCreateAssetBundle(string url, Hash128 hash, uint crc)
		{
			DownloadHandler.INTERNAL_CALL_InternalCreateAssetBundle(this, url, ref hash, crc);
		}

		// Token: 0x060027FB RID: 10235
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_InternalCreateAssetBundle(DownloadHandler self, string url, ref Hash128 hash, uint crc);

		// Token: 0x060027FC RID: 10236
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalCreateAudioClip(string url, AudioType audioType);

		// Token: 0x060027FD RID: 10237
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InternalDestroy();

		// Token: 0x060027FE RID: 10238 RVA: 0x000399E0 File Offset: 0x00037BE0
		~DownloadHandler()
		{
			this.InternalDestroy();
		}

		// Token: 0x060027FF RID: 10239 RVA: 0x00039A10 File Offset: 0x00037C10
		public void Dispose()
		{
			this.InternalDestroy();
			GC.SuppressFinalize(this);
		}

		// Token: 0x17000990 RID: 2448
		// (get) Token: 0x06002800 RID: 10240
		public extern bool isDone { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000991 RID: 2449
		// (get) Token: 0x06002801 RID: 10241 RVA: 0x00039A20 File Offset: 0x00037C20
		public byte[] data
		{
			get
			{
				return this.GetData();
			}
		}

		// Token: 0x17000992 RID: 2450
		// (get) Token: 0x06002802 RID: 10242 RVA: 0x00039A3C File Offset: 0x00037C3C
		public string text
		{
			get
			{
				return this.GetText();
			}
		}

		// Token: 0x06002803 RID: 10243 RVA: 0x00039A58 File Offset: 0x00037C58
		protected virtual byte[] GetData()
		{
			return null;
		}

		// Token: 0x06002804 RID: 10244 RVA: 0x00039A70 File Offset: 0x00037C70
		protected virtual string GetText()
		{
			byte[] data = this.GetData();
			string result;
			if (data != null && data.Length > 0)
			{
				result = Encoding.UTF8.GetString(data, 0, data.Length);
			}
			else
			{
				result = "";
			}
			return result;
		}

		// Token: 0x06002805 RID: 10245 RVA: 0x00039AB8 File Offset: 0x00037CB8
		[UsedByNativeCode]
		protected virtual bool ReceiveData(byte[] data, int dataLength)
		{
			return true;
		}

		// Token: 0x06002806 RID: 10246 RVA: 0x00039AD0 File Offset: 0x00037CD0
		[UsedByNativeCode]
		protected virtual void ReceiveContentLength(int contentLength)
		{
		}

		// Token: 0x06002807 RID: 10247 RVA: 0x00039AD4 File Offset: 0x00037CD4
		[UsedByNativeCode]
		protected virtual void CompleteContent()
		{
		}

		// Token: 0x06002808 RID: 10248 RVA: 0x00039AD8 File Offset: 0x00037CD8
		[UsedByNativeCode]
		protected virtual float GetProgress()
		{
			return 0f;
		}

		// Token: 0x06002809 RID: 10249 RVA: 0x00039AF4 File Offset: 0x00037CF4
		protected static T GetCheckedDownloader<T>(UnityWebRequest www) where T : DownloadHandler
		{
			if (www == null)
			{
				throw new NullReferenceException("Cannot get content from a null UnityWebRequest object");
			}
			if (!www.isDone)
			{
				throw new InvalidOperationException("Cannot get content from an unfinished UnityWebRequest object");
			}
			if (www.isError)
			{
				throw new InvalidOperationException(www.error);
			}
			return (T)((object)www.downloadHandler);
		}

		// Token: 0x04000918 RID: 2328
		[NonSerialized]
		internal IntPtr m_Ptr;
	}
}
