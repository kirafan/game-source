﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngineInternal;

namespace UnityEngine.Networking
{
	// Token: 0x02000246 RID: 582
	[StructLayout(LayoutKind.Sequential)]
	public sealed class UnityWebRequest : IDisposable
	{
		// Token: 0x06002772 RID: 10098 RVA: 0x0003851C File Offset: 0x0003671C
		public UnityWebRequest()
		{
			this.InternalCreate();
			this.InternalSetDefaults();
		}

		// Token: 0x06002773 RID: 10099 RVA: 0x00038534 File Offset: 0x00036734
		public UnityWebRequest(string url)
		{
			this.InternalCreate();
			this.InternalSetDefaults();
			this.url = url;
		}

		// Token: 0x06002774 RID: 10100 RVA: 0x00038550 File Offset: 0x00036750
		public UnityWebRequest(string url, string method)
		{
			this.InternalCreate();
			this.InternalSetDefaults();
			this.url = url;
			this.method = method;
		}

		// Token: 0x06002775 RID: 10101 RVA: 0x00038574 File Offset: 0x00036774
		public UnityWebRequest(string url, string method, DownloadHandler downloadHandler, UploadHandler uploadHandler)
		{
			this.InternalCreate();
			this.InternalSetDefaults();
			this.url = url;
			this.method = method;
			this.downloadHandler = downloadHandler;
			this.uploadHandler = uploadHandler;
		}

		// Token: 0x06002776 RID: 10102 RVA: 0x000385A8 File Offset: 0x000367A8
		public static UnityWebRequest Get(string uri)
		{
			return new UnityWebRequest(uri, "GET", new DownloadHandlerBuffer(), null);
		}

		// Token: 0x06002777 RID: 10103 RVA: 0x000385D0 File Offset: 0x000367D0
		public static UnityWebRequest Delete(string uri)
		{
			return new UnityWebRequest(uri, "DELETE");
		}

		// Token: 0x06002778 RID: 10104 RVA: 0x000385F4 File Offset: 0x000367F4
		public static UnityWebRequest Head(string uri)
		{
			return new UnityWebRequest(uri, "HEAD");
		}

		// Token: 0x06002779 RID: 10105 RVA: 0x00038618 File Offset: 0x00036818
		public static UnityWebRequest GetTexture(string uri)
		{
			return UnityWebRequest.GetTexture(uri, false);
		}

		// Token: 0x0600277A RID: 10106 RVA: 0x00038634 File Offset: 0x00036834
		public static UnityWebRequest GetTexture(string uri, bool nonReadable)
		{
			return new UnityWebRequest(uri, "GET", new DownloadHandlerTexture(!nonReadable), null);
		}

		// Token: 0x0600277B RID: 10107 RVA: 0x00038660 File Offset: 0x00036860
		public static UnityWebRequest GetAudioClip(string uri, AudioType audioType)
		{
			return new UnityWebRequest(uri, "GET", new DownloadHandlerAudioClip(uri, audioType), null);
		}

		// Token: 0x0600277C RID: 10108 RVA: 0x0003868C File Offset: 0x0003688C
		public static UnityWebRequest GetAssetBundle(string uri)
		{
			return UnityWebRequest.GetAssetBundle(uri, 0U);
		}

		// Token: 0x0600277D RID: 10109 RVA: 0x000386A8 File Offset: 0x000368A8
		public static UnityWebRequest GetAssetBundle(string uri, uint crc)
		{
			return new UnityWebRequest(uri, "GET", new DownloadHandlerAssetBundle(uri, crc), null);
		}

		// Token: 0x0600277E RID: 10110 RVA: 0x000386D4 File Offset: 0x000368D4
		public static UnityWebRequest GetAssetBundle(string uri, uint version, uint crc)
		{
			return new UnityWebRequest(uri, "GET", new DownloadHandlerAssetBundle(uri, version, crc), null);
		}

		// Token: 0x0600277F RID: 10111 RVA: 0x00038700 File Offset: 0x00036900
		public static UnityWebRequest GetAssetBundle(string uri, Hash128 hash, uint crc)
		{
			return new UnityWebRequest(uri, "GET", new DownloadHandlerAssetBundle(uri, hash, crc), null);
		}

		// Token: 0x06002780 RID: 10112 RVA: 0x0003872C File Offset: 0x0003692C
		public static UnityWebRequest Put(string uri, byte[] bodyData)
		{
			return new UnityWebRequest(uri, "PUT", new DownloadHandlerBuffer(), new UploadHandlerRaw(bodyData));
		}

		// Token: 0x06002781 RID: 10113 RVA: 0x0003875C File Offset: 0x0003695C
		public static UnityWebRequest Put(string uri, string bodyData)
		{
			return new UnityWebRequest(uri, "PUT", new DownloadHandlerBuffer(), new UploadHandlerRaw(Encoding.UTF8.GetBytes(bodyData)));
		}

		// Token: 0x06002782 RID: 10114 RVA: 0x00038794 File Offset: 0x00036994
		public static UnityWebRequest Post(string uri, string postData)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			byte[] data = null;
			if (!string.IsNullOrEmpty(postData))
			{
				string s = WWWTranscoder.URLEncode(postData, Encoding.UTF8);
				data = Encoding.UTF8.GetBytes(s);
			}
			unityWebRequest.uploadHandler = new UploadHandlerRaw(data);
			unityWebRequest.uploadHandler.contentType = "application/x-www-form-urlencoded";
			unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
			return unityWebRequest;
		}

		// Token: 0x06002783 RID: 10115 RVA: 0x00038804 File Offset: 0x00036A04
		public static UnityWebRequest Post(string uri, WWWForm formData)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			byte[] array = null;
			if (formData != null)
			{
				array = formData.data;
				if (array.Length == 0)
				{
					array = null;
				}
			}
			unityWebRequest.uploadHandler = new UploadHandlerRaw(array);
			unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
			if (formData != null)
			{
				Dictionary<string, string> headers = formData.headers;
				foreach (KeyValuePair<string, string> keyValuePair in headers)
				{
					unityWebRequest.SetRequestHeader(keyValuePair.Key, keyValuePair.Value);
				}
			}
			return unityWebRequest;
		}

		// Token: 0x06002784 RID: 10116 RVA: 0x000388C4 File Offset: 0x00036AC4
		public static UnityWebRequest Post(string uri, List<IMultipartFormSection> multipartFormSections)
		{
			byte[] boundary = UnityWebRequest.GenerateBoundary();
			return UnityWebRequest.Post(uri, multipartFormSections, boundary);
		}

		// Token: 0x06002785 RID: 10117 RVA: 0x000388E8 File Offset: 0x00036AE8
		public static UnityWebRequest Post(string uri, List<IMultipartFormSection> multipartFormSections, byte[] boundary)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			byte[] data = null;
			if (multipartFormSections != null && multipartFormSections.Count != 0)
			{
				data = UnityWebRequest.SerializeFormSections(multipartFormSections, boundary);
			}
			unityWebRequest.uploadHandler = new UploadHandlerRaw(data)
			{
				contentType = "multipart/form-data; boundary=" + Encoding.UTF8.GetString(boundary, 0, boundary.Length)
			};
			unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
			return unityWebRequest;
		}

		// Token: 0x06002786 RID: 10118 RVA: 0x00038960 File Offset: 0x00036B60
		public static UnityWebRequest Post(string uri, Dictionary<string, string> formFields)
		{
			UnityWebRequest unityWebRequest = new UnityWebRequest(uri, "POST");
			byte[] data = null;
			if (formFields != null && formFields.Count != 0)
			{
				data = UnityWebRequest.SerializeSimpleForm(formFields);
			}
			unityWebRequest.uploadHandler = new UploadHandlerRaw(data)
			{
				contentType = "application/x-www-form-urlencoded"
			};
			unityWebRequest.downloadHandler = new DownloadHandlerBuffer();
			return unityWebRequest;
		}

		// Token: 0x06002787 RID: 10119 RVA: 0x000389C0 File Offset: 0x00036BC0
		public static byte[] SerializeFormSections(List<IMultipartFormSection> multipartFormSections, byte[] boundary)
		{
			byte[] bytes = Encoding.UTF8.GetBytes("\r\n");
			int num = 0;
			foreach (IMultipartFormSection multipartFormSection in multipartFormSections)
			{
				num += 64 + multipartFormSection.sectionData.Length;
			}
			List<byte> list = new List<byte>(num);
			foreach (IMultipartFormSection multipartFormSection2 in multipartFormSections)
			{
				string str = "form-data";
				string sectionName = multipartFormSection2.sectionName;
				string fileName = multipartFormSection2.fileName;
				if (!string.IsNullOrEmpty(fileName))
				{
					str = "file";
				}
				string text = "Content-Disposition: " + str;
				if (!string.IsNullOrEmpty(sectionName))
				{
					text = text + "; name=\"" + sectionName + "\"";
				}
				if (!string.IsNullOrEmpty(fileName))
				{
					text = text + "; filename=\"" + fileName + "\"";
				}
				text += "\r\n";
				string contentType = multipartFormSection2.contentType;
				if (!string.IsNullOrEmpty(contentType))
				{
					text = text + "Content-Type: " + contentType + "\r\n";
				}
				list.AddRange(boundary);
				list.AddRange(bytes);
				list.AddRange(Encoding.UTF8.GetBytes(text));
				list.AddRange(bytes);
				list.AddRange(multipartFormSection2.sectionData);
			}
			list.TrimExcess();
			return list.ToArray();
		}

		// Token: 0x06002788 RID: 10120 RVA: 0x00038BA4 File Offset: 0x00036DA4
		public static byte[] GenerateBoundary()
		{
			byte[] array = new byte[40];
			for (int i = 0; i < 40; i++)
			{
				int num = Random.Range(48, 110);
				if (num > 57)
				{
					num += 7;
				}
				if (num > 90)
				{
					num += 6;
				}
				array[i] = (byte)num;
			}
			return array;
		}

		// Token: 0x06002789 RID: 10121 RVA: 0x00038C00 File Offset: 0x00036E00
		public static byte[] SerializeSimpleForm(Dictionary<string, string> formFields)
		{
			string text = "";
			foreach (KeyValuePair<string, string> keyValuePair in formFields)
			{
				if (text.Length > 0)
				{
					text += "&";
				}
				text = text + Uri.EscapeDataString(keyValuePair.Key) + "=" + Uri.EscapeDataString(keyValuePair.Value);
			}
			return Encoding.UTF8.GetBytes(text);
		}

		// Token: 0x1700096E RID: 2414
		// (get) Token: 0x0600278A RID: 10122 RVA: 0x00038CAC File Offset: 0x00036EAC
		// (set) Token: 0x0600278B RID: 10123 RVA: 0x00038CC8 File Offset: 0x00036EC8
		public bool disposeDownloadHandlerOnDispose { get; set; }

		// Token: 0x1700096F RID: 2415
		// (get) Token: 0x0600278C RID: 10124 RVA: 0x00038CD4 File Offset: 0x00036ED4
		// (set) Token: 0x0600278D RID: 10125 RVA: 0x00038CF0 File Offset: 0x00036EF0
		public bool disposeUploadHandlerOnDispose { get; set; }

		// Token: 0x0600278E RID: 10126
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalCreate();

		// Token: 0x0600278F RID: 10127
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalDestroy();

		// Token: 0x06002790 RID: 10128 RVA: 0x00038CFC File Offset: 0x00036EFC
		private void InternalSetDefaults()
		{
			this.disposeDownloadHandlerOnDispose = true;
			this.disposeUploadHandlerOnDispose = true;
		}

		// Token: 0x06002791 RID: 10129 RVA: 0x00038D10 File Offset: 0x00036F10
		~UnityWebRequest()
		{
			this.InternalDestroy();
		}

		// Token: 0x06002792 RID: 10130 RVA: 0x00038D40 File Offset: 0x00036F40
		public void Dispose()
		{
			if (this.disposeDownloadHandlerOnDispose)
			{
				DownloadHandler downloadHandler = this.downloadHandler;
				if (downloadHandler != null)
				{
					downloadHandler.Dispose();
				}
			}
			if (this.disposeUploadHandlerOnDispose)
			{
				UploadHandler uploadHandler = this.uploadHandler;
				if (uploadHandler != null)
				{
					uploadHandler.Dispose();
				}
			}
			this.InternalDestroy();
			GC.SuppressFinalize(this);
		}

		// Token: 0x06002793 RID: 10131
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern AsyncOperation InternalBegin();

		// Token: 0x06002794 RID: 10132
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalAbort();

		// Token: 0x06002795 RID: 10133 RVA: 0x00038DA0 File Offset: 0x00036FA0
		public AsyncOperation Send()
		{
			return this.InternalBegin();
		}

		// Token: 0x06002796 RID: 10134 RVA: 0x00038DBC File Offset: 0x00036FBC
		public void Abort()
		{
			this.InternalAbort();
		}

		// Token: 0x06002797 RID: 10135
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalSetMethod(UnityWebRequest.UnityWebRequestMethod methodType);

		// Token: 0x06002798 RID: 10136
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalSetCustomMethod(string customMethodName);

		// Token: 0x06002799 RID: 10137
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int InternalGetMethod();

		// Token: 0x0600279A RID: 10138
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string InternalGetCustomMethod();

		// Token: 0x17000970 RID: 2416
		// (get) Token: 0x0600279B RID: 10139 RVA: 0x00038DC8 File Offset: 0x00036FC8
		// (set) Token: 0x0600279C RID: 10140 RVA: 0x00038E34 File Offset: 0x00037034
		public string method
		{
			get
			{
				string result;
				switch (this.InternalGetMethod())
				{
				case 0:
					result = "GET";
					break;
				case 1:
					result = "POST";
					break;
				case 2:
					result = "PUT";
					break;
				case 3:
					result = "HEAD";
					break;
				default:
					result = this.InternalGetCustomMethod();
					break;
				}
				return result;
			}
			set
			{
				if (string.IsNullOrEmpty(value))
				{
					throw new ArgumentException("Cannot set a UnityWebRequest's method to an empty or null string");
				}
				string text = value.ToUpper();
				if (text != null)
				{
					if (text == "GET")
					{
						this.InternalSetMethod(UnityWebRequest.UnityWebRequestMethod.Get);
						return;
					}
					if (text == "POST")
					{
						this.InternalSetMethod(UnityWebRequest.UnityWebRequestMethod.Post);
						return;
					}
					if (text == "PUT")
					{
						this.InternalSetMethod(UnityWebRequest.UnityWebRequestMethod.Put);
						return;
					}
					if (text == "HEAD")
					{
						this.InternalSetMethod(UnityWebRequest.UnityWebRequestMethod.Head);
						return;
					}
				}
				this.InternalSetCustomMethod(value.ToUpper());
			}
		}

		// Token: 0x0600279D RID: 10141
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int InternalGetError();

		// Token: 0x17000971 RID: 2417
		// (get) Token: 0x0600279E RID: 10142
		public extern string error { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000972 RID: 2418
		// (get) Token: 0x0600279F RID: 10143
		// (set) Token: 0x060027A0 RID: 10144
		public extern bool useHttpContinue { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000973 RID: 2419
		// (get) Token: 0x060027A1 RID: 10145 RVA: 0x00038EEC File Offset: 0x000370EC
		// (set) Token: 0x060027A2 RID: 10146 RVA: 0x00038F08 File Offset: 0x00037108
		public string url
		{
			get
			{
				return this.InternalGetUrl();
			}
			set
			{
				string localUrl = "http://localhost/";
				this.InternalSetUrl(WebRequestUtils.MakeInitialUrl(value, localUrl));
			}
		}

		// Token: 0x060027A3 RID: 10147
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string InternalGetUrl();

		// Token: 0x060027A4 RID: 10148
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InternalSetUrl(string url);

		// Token: 0x17000974 RID: 2420
		// (get) Token: 0x060027A5 RID: 10149
		public extern long responseCode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000975 RID: 2421
		// (get) Token: 0x060027A6 RID: 10150
		public extern float uploadProgress { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000976 RID: 2422
		// (get) Token: 0x060027A7 RID: 10151
		public extern bool isModifiable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000977 RID: 2423
		// (get) Token: 0x060027A8 RID: 10152
		public extern bool isDone { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000978 RID: 2424
		// (get) Token: 0x060027A9 RID: 10153
		public extern bool isError { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000979 RID: 2425
		// (get) Token: 0x060027AA RID: 10154
		public extern float downloadProgress { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700097A RID: 2426
		// (get) Token: 0x060027AB RID: 10155
		public extern ulong uploadedBytes { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700097B RID: 2427
		// (get) Token: 0x060027AC RID: 10156
		public extern ulong downloadedBytes { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700097C RID: 2428
		// (get) Token: 0x060027AD RID: 10157
		// (set) Token: 0x060027AE RID: 10158
		public extern int redirectLimit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700097D RID: 2429
		// (get) Token: 0x060027AF RID: 10159
		// (set) Token: 0x060027B0 RID: 10160
		public extern bool chunkedTransfer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060027B1 RID: 10161
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetRequestHeader(string name);

		// Token: 0x060027B2 RID: 10162
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InternalSetRequestHeader(string name, string value);

		// Token: 0x060027B3 RID: 10163 RVA: 0x00038F2C File Offset: 0x0003712C
		public void SetRequestHeader(string name, string value)
		{
			if (string.IsNullOrEmpty(name))
			{
				throw new ArgumentException("Cannot set a Request Header with a null or empty name");
			}
			if (value == null)
			{
				throw new ArgumentException("Cannot set a Request header with a null");
			}
			if (!UnityWebRequest.IsHeaderNameLegal(name))
			{
				throw new ArgumentException("Cannot set Request Header " + name + " - name contains illegal characters or is not user-overridable");
			}
			if (!UnityWebRequest.IsHeaderValueLegal(value))
			{
				throw new ArgumentException("Cannot set Request Header - value contains illegal characters");
			}
			this.InternalSetRequestHeader(name, value);
		}

		// Token: 0x060027B4 RID: 10164
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetResponseHeader(string name);

		// Token: 0x060027B5 RID: 10165
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string[] InternalGetResponseHeaderKeys();

		// Token: 0x060027B6 RID: 10166 RVA: 0x00038FA4 File Offset: 0x000371A4
		public Dictionary<string, string> GetResponseHeaders()
		{
			string[] array = this.InternalGetResponseHeaderKeys();
			Dictionary<string, string> result;
			if (array == null)
			{
				result = null;
			}
			else
			{
				Dictionary<string, string> dictionary = new Dictionary<string, string>(array.Length, StringComparer.OrdinalIgnoreCase);
				for (int i = 0; i < array.Length; i++)
				{
					string responseHeader = this.GetResponseHeader(array[i]);
					dictionary.Add(array[i], responseHeader);
				}
				result = dictionary;
			}
			return result;
		}

		// Token: 0x1700097E RID: 2430
		// (get) Token: 0x060027B7 RID: 10167
		// (set) Token: 0x060027B8 RID: 10168
		public extern UploadHandler uploadHandler { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700097F RID: 2431
		// (get) Token: 0x060027B9 RID: 10169
		// (set) Token: 0x060027BA RID: 10170
		public extern DownloadHandler downloadHandler { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000980 RID: 2432
		// (get) Token: 0x060027BB RID: 10171
		// (set) Token: 0x060027BC RID: 10172
		public extern int timeout { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060027BD RID: 10173 RVA: 0x0003900C File Offset: 0x0003720C
		private static bool ContainsForbiddenCharacters(string s, int firstAllowedCharCode)
		{
			foreach (char c in s)
			{
				if ((int)c < firstAllowedCharCode || c == '\u007f')
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060027BE RID: 10174 RVA: 0x0003905C File Offset: 0x0003725C
		private static bool IsHeaderNameLegal(string headerName)
		{
			bool result;
			if (string.IsNullOrEmpty(headerName))
			{
				result = false;
			}
			else
			{
				headerName = headerName.ToLower();
				if (UnityWebRequest.ContainsForbiddenCharacters(headerName, 33))
				{
					result = false;
				}
				else if (headerName.StartsWith("sec-") || headerName.StartsWith("proxy-"))
				{
					result = false;
				}
				else
				{
					foreach (string b in UnityWebRequest.forbiddenHeaderKeys)
					{
						if (string.Equals(headerName, b))
						{
							return false;
						}
					}
					result = true;
				}
			}
			return result;
		}

		// Token: 0x060027BF RID: 10175 RVA: 0x00039100 File Offset: 0x00037300
		private static bool IsHeaderValueLegal(string headerValue)
		{
			return !UnityWebRequest.ContainsForbiddenCharacters(headerValue, 32);
		}

		// Token: 0x060027C0 RID: 10176 RVA: 0x0003912C File Offset: 0x0003732C
		private static string GetErrorDescription(UnityWebRequest.UnityWebRequestError errorCode)
		{
			switch (errorCode)
			{
			case UnityWebRequest.UnityWebRequestError.OK:
				return "No Error";
			case UnityWebRequest.UnityWebRequestError.SDKError:
				return "Internal Error With Transport Layer";
			case UnityWebRequest.UnityWebRequestError.UnsupportedProtocol:
				return "Specified Transport Protocol is Unsupported";
			case UnityWebRequest.UnityWebRequestError.MalformattedUrl:
				return "URL is Malformatted";
			case UnityWebRequest.UnityWebRequestError.CannotResolveProxy:
				return "Unable to resolve specified proxy server";
			case UnityWebRequest.UnityWebRequestError.CannotResolveHost:
				return "Unable to resolve host specified in URL";
			case UnityWebRequest.UnityWebRequestError.CannotConnectToHost:
				return "Unable to connect to host specified in URL";
			case UnityWebRequest.UnityWebRequestError.AccessDenied:
				return "Remote server denied access to the specified URL";
			case UnityWebRequest.UnityWebRequestError.GenericHTTPError:
				return "Unknown/Generic HTTP Error - Check HTTP Error code";
			case UnityWebRequest.UnityWebRequestError.WriteError:
				return "Error when transmitting request to remote server - transmission terminated prematurely";
			case UnityWebRequest.UnityWebRequestError.ReadError:
				return "Error when reading response from remote server - transmission terminated prematurely";
			case UnityWebRequest.UnityWebRequestError.OutOfMemory:
				return "Out of Memory";
			case UnityWebRequest.UnityWebRequestError.Timeout:
				return "Timeout occurred while waiting for response from remote server";
			case UnityWebRequest.UnityWebRequestError.HTTPPostError:
				return "Error while transmitting HTTP POST body data";
			case UnityWebRequest.UnityWebRequestError.SSLCannotConnect:
				return "Unable to connect to SSL server at remote host";
			case UnityWebRequest.UnityWebRequestError.Aborted:
				return "Request was manually aborted by local code";
			case UnityWebRequest.UnityWebRequestError.TooManyRedirects:
				return "Redirect limit exceeded";
			case UnityWebRequest.UnityWebRequestError.ReceivedNoData:
				return "Received an empty response from remote host";
			case UnityWebRequest.UnityWebRequestError.SSLNotSupported:
				return "SSL connections are not supported on the local machine";
			case UnityWebRequest.UnityWebRequestError.FailedToSendData:
				return "Failed to transmit body data";
			case UnityWebRequest.UnityWebRequestError.FailedToReceiveData:
				return "Failed to receive response body data";
			case UnityWebRequest.UnityWebRequestError.SSLCertificateError:
				return "Failure to authenticate SSL certificate of remote host";
			case UnityWebRequest.UnityWebRequestError.SSLCipherNotAvailable:
				return "SSL cipher received from remote host is not supported on the local machine";
			case UnityWebRequest.UnityWebRequestError.SSLCACertError:
				return "Failure to authenticate Certificate Authority of the SSL certificate received from the remote host";
			case UnityWebRequest.UnityWebRequestError.UnrecognizedContentEncoding:
				return "Remote host returned data with an unrecognized/unparseable content encoding";
			case UnityWebRequest.UnityWebRequestError.LoginFailed:
				return "HTTP authentication failed";
			case UnityWebRequest.UnityWebRequestError.SSLShutdownFailed:
				return "Failure while shutting down SSL connection";
			}
			return "Unknown error";
		}

		// Token: 0x040008E1 RID: 2273
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x040008E2 RID: 2274
		public const string kHttpVerbGET = "GET";

		// Token: 0x040008E3 RID: 2275
		public const string kHttpVerbHEAD = "HEAD";

		// Token: 0x040008E4 RID: 2276
		public const string kHttpVerbPOST = "POST";

		// Token: 0x040008E5 RID: 2277
		public const string kHttpVerbPUT = "PUT";

		// Token: 0x040008E6 RID: 2278
		public const string kHttpVerbCREATE = "CREATE";

		// Token: 0x040008E7 RID: 2279
		public const string kHttpVerbDELETE = "DELETE";

		// Token: 0x040008EA RID: 2282
		private static readonly string[] forbiddenHeaderKeys = new string[]
		{
			"accept-charset",
			"access-control-request-headers",
			"access-control-request-method",
			"connection",
			"content-length",
			"date",
			"dnt",
			"expect",
			"host",
			"keep-alive",
			"origin",
			"referer",
			"te",
			"trailer",
			"transfer-encoding",
			"upgrade",
			"user-agent",
			"via",
			"x-unity-version"
		};

		// Token: 0x02000247 RID: 583
		internal enum UnityWebRequestMethod
		{
			// Token: 0x040008EC RID: 2284
			Get,
			// Token: 0x040008ED RID: 2285
			Post,
			// Token: 0x040008EE RID: 2286
			Put,
			// Token: 0x040008EF RID: 2287
			Head,
			// Token: 0x040008F0 RID: 2288
			Custom
		}

		// Token: 0x02000248 RID: 584
		internal enum UnityWebRequestError
		{
			// Token: 0x040008F2 RID: 2290
			OK,
			// Token: 0x040008F3 RID: 2291
			Unknown,
			// Token: 0x040008F4 RID: 2292
			SDKError,
			// Token: 0x040008F5 RID: 2293
			UnsupportedProtocol,
			// Token: 0x040008F6 RID: 2294
			MalformattedUrl,
			// Token: 0x040008F7 RID: 2295
			CannotResolveProxy,
			// Token: 0x040008F8 RID: 2296
			CannotResolveHost,
			// Token: 0x040008F9 RID: 2297
			CannotConnectToHost,
			// Token: 0x040008FA RID: 2298
			AccessDenied,
			// Token: 0x040008FB RID: 2299
			GenericHTTPError,
			// Token: 0x040008FC RID: 2300
			WriteError,
			// Token: 0x040008FD RID: 2301
			ReadError,
			// Token: 0x040008FE RID: 2302
			OutOfMemory,
			// Token: 0x040008FF RID: 2303
			Timeout,
			// Token: 0x04000900 RID: 2304
			HTTPPostError,
			// Token: 0x04000901 RID: 2305
			SSLCannotConnect,
			// Token: 0x04000902 RID: 2306
			Aborted,
			// Token: 0x04000903 RID: 2307
			TooManyRedirects,
			// Token: 0x04000904 RID: 2308
			ReceivedNoData,
			// Token: 0x04000905 RID: 2309
			SSLNotSupported,
			// Token: 0x04000906 RID: 2310
			FailedToSendData,
			// Token: 0x04000907 RID: 2311
			FailedToReceiveData,
			// Token: 0x04000908 RID: 2312
			SSLCertificateError,
			// Token: 0x04000909 RID: 2313
			SSLCipherNotAvailable,
			// Token: 0x0400090A RID: 2314
			SSLCACertError,
			// Token: 0x0400090B RID: 2315
			UnrecognizedContentEncoding,
			// Token: 0x0400090C RID: 2316
			LoginFailed,
			// Token: 0x0400090D RID: 2317
			SSLShutdownFailed,
			// Token: 0x0400090E RID: 2318
			NoInternetConnection
		}
	}
}
