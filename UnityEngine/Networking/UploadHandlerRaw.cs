﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine.Networking
{
	// Token: 0x0200024E RID: 590
	[StructLayout(LayoutKind.Sequential)]
	public sealed class UploadHandlerRaw : UploadHandler
	{
		// Token: 0x060027EC RID: 10220 RVA: 0x00039954 File Offset: 0x00037B54
		public UploadHandlerRaw(byte[] data)
		{
			base.InternalCreateRaw(data);
		}

		// Token: 0x060027ED RID: 10221
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string InternalGetContentType();

		// Token: 0x060027EE RID: 10222
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InternalSetContentType(string newContentType);

		// Token: 0x060027EF RID: 10223
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern byte[] InternalGetData();

		// Token: 0x060027F0 RID: 10224
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float InternalGetProgress();

		// Token: 0x060027F1 RID: 10225 RVA: 0x00039964 File Offset: 0x00037B64
		internal override string GetContentType()
		{
			return this.InternalGetContentType();
		}

		// Token: 0x060027F2 RID: 10226 RVA: 0x00039980 File Offset: 0x00037B80
		internal override void SetContentType(string newContentType)
		{
			this.InternalSetContentType(newContentType);
		}

		// Token: 0x060027F3 RID: 10227 RVA: 0x0003998C File Offset: 0x00037B8C
		internal override byte[] GetData()
		{
			return this.InternalGetData();
		}

		// Token: 0x060027F4 RID: 10228 RVA: 0x000399A8 File Offset: 0x00037BA8
		internal override float GetProgress()
		{
			return this.InternalGetProgress();
		}
	}
}
