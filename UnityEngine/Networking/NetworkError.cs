﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x0200027B RID: 635
	public enum NetworkError
	{
		// Token: 0x0400099D RID: 2461
		Ok,
		// Token: 0x0400099E RID: 2462
		WrongHost,
		// Token: 0x0400099F RID: 2463
		WrongConnection,
		// Token: 0x040009A0 RID: 2464
		WrongChannel,
		// Token: 0x040009A1 RID: 2465
		NoResources,
		// Token: 0x040009A2 RID: 2466
		BadMessage,
		// Token: 0x040009A3 RID: 2467
		Timeout,
		// Token: 0x040009A4 RID: 2468
		MessageToLong,
		// Token: 0x040009A5 RID: 2469
		WrongOperation,
		// Token: 0x040009A6 RID: 2470
		VersionMismatch,
		// Token: 0x040009A7 RID: 2471
		CRCMismatch,
		// Token: 0x040009A8 RID: 2472
		DNSFailure
	}
}
