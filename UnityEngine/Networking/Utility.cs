﻿using System;
using System.Collections.Generic;
using UnityEngine.Networking.Types;

namespace UnityEngine.Networking
{
	// Token: 0x0200026F RID: 623
	public class Utility
	{
		// Token: 0x060028F1 RID: 10481 RVA: 0x0003BA98 File Offset: 0x00039C98
		private Utility()
		{
		}

		// Token: 0x170009D6 RID: 2518
		// (get) Token: 0x060028F2 RID: 10482 RVA: 0x0003BAA4 File Offset: 0x00039CA4
		// (set) Token: 0x060028F3 RID: 10483 RVA: 0x0003BABC File Offset: 0x00039CBC
		[Obsolete("This property is unused and should not be referenced in code.", true)]
		public static bool useRandomSourceID
		{
			get
			{
				return false;
			}
			set
			{
			}
		}

		// Token: 0x060028F4 RID: 10484 RVA: 0x0003BAC0 File Offset: 0x00039CC0
		public static SourceID GetSourceID()
		{
			return (SourceID)((long)SystemInfo.deviceUniqueIdentifier.GetHashCode());
		}

		// Token: 0x060028F5 RID: 10485 RVA: 0x0003BAE0 File Offset: 0x00039CE0
		[Obsolete("This function is unused and should not be referenced in code. Please sign in and setup your project in the editor instead.", true)]
		public static void SetAppID(AppID newAppID)
		{
		}

		// Token: 0x060028F6 RID: 10486 RVA: 0x0003BAE4 File Offset: 0x00039CE4
		[Obsolete("This function is unused and should not be referenced in code. Please sign in and setup your project in the editor instead.", true)]
		public static AppID GetAppID()
		{
			return AppID.Invalid;
		}

		// Token: 0x060028F7 RID: 10487 RVA: 0x0003BAFC File Offset: 0x00039CFC
		public static void SetAccessTokenForNetwork(NetworkID netId, NetworkAccessToken accessToken)
		{
			if (Utility.s_dictTokens.ContainsKey(netId))
			{
				Utility.s_dictTokens.Remove(netId);
			}
			Utility.s_dictTokens.Add(netId, accessToken);
		}

		// Token: 0x060028F8 RID: 10488 RVA: 0x0003BB28 File Offset: 0x00039D28
		public static NetworkAccessToken GetAccessTokenForNetwork(NetworkID netId)
		{
			NetworkAccessToken result;
			if (!Utility.s_dictTokens.TryGetValue(netId, out result))
			{
				result = new NetworkAccessToken();
			}
			return result;
		}

		// Token: 0x0400096F RID: 2415
		private static Dictionary<NetworkID, NetworkAccessToken> s_dictTokens = new Dictionary<NetworkID, NetworkAccessToken>();
	}
}
