﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine.Networking
{
	// Token: 0x02000254 RID: 596
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DownloadHandlerAudioClip : DownloadHandler
	{
		// Token: 0x06002822 RID: 10274 RVA: 0x00039DA0 File Offset: 0x00037FA0
		public DownloadHandlerAudioClip(string url, AudioType audioType)
		{
			base.InternalCreateAudioClip(url, audioType);
		}

		// Token: 0x06002823 RID: 10275 RVA: 0x00039DB4 File Offset: 0x00037FB4
		protected override byte[] GetData()
		{
			return this.InternalGetData();
		}

		// Token: 0x06002824 RID: 10276 RVA: 0x00039DD0 File Offset: 0x00037FD0
		protected override string GetText()
		{
			throw new NotSupportedException("String access is not supported for audio clips");
		}

		// Token: 0x06002825 RID: 10277
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern byte[] InternalGetData();

		// Token: 0x17000995 RID: 2453
		// (get) Token: 0x06002826 RID: 10278
		public extern AudioClip audioClip { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06002827 RID: 10279 RVA: 0x00039DE0 File Offset: 0x00037FE0
		public static AudioClip GetContent(UnityWebRequest www)
		{
			return DownloadHandler.GetCheckedDownloader<DownloadHandlerAudioClip>(www).audioClip;
		}
	}
}
