﻿using System;
using System.Text;

namespace UnityEngine.Networking
{
	// Token: 0x0200024A RID: 586
	public class MultipartFormDataSection : IMultipartFormSection
	{
		// Token: 0x060027C6 RID: 10182 RVA: 0x000393A8 File Offset: 0x000375A8
		public MultipartFormDataSection(string name, byte[] data, string contentType)
		{
			if (data == null || data.Length < 1)
			{
				throw new ArgumentException("Cannot create a multipart form data section without body data");
			}
			this.name = name;
			this.data = data;
			this.content = contentType;
		}

		// Token: 0x060027C7 RID: 10183 RVA: 0x000393E4 File Offset: 0x000375E4
		public MultipartFormDataSection(string name, byte[] data) : this(name, data, null)
		{
		}

		// Token: 0x060027C8 RID: 10184 RVA: 0x000393F0 File Offset: 0x000375F0
		public MultipartFormDataSection(byte[] data) : this(null, data)
		{
		}

		// Token: 0x060027C9 RID: 10185 RVA: 0x000393FC File Offset: 0x000375FC
		public MultipartFormDataSection(string name, string data, Encoding encoding, string contentType)
		{
			if (data == null || data.Length < 1)
			{
				throw new ArgumentException("Cannot create a multipart form data section without body data");
			}
			byte[] bytes = encoding.GetBytes(data);
			this.name = name;
			this.data = bytes;
			if (contentType != null && !contentType.Contains("encoding="))
			{
				contentType = contentType.Trim() + "; encoding=" + encoding.WebName;
			}
			this.content = contentType;
		}

		// Token: 0x060027CA RID: 10186 RVA: 0x00039480 File Offset: 0x00037680
		public MultipartFormDataSection(string name, string data, string contentType) : this(name, data, Encoding.UTF8, contentType)
		{
		}

		// Token: 0x060027CB RID: 10187 RVA: 0x00039494 File Offset: 0x00037694
		public MultipartFormDataSection(string name, string data) : this(name, data, "text/plain")
		{
		}

		// Token: 0x060027CC RID: 10188 RVA: 0x000394A4 File Offset: 0x000376A4
		public MultipartFormDataSection(string data) : this(null, data)
		{
		}

		// Token: 0x17000985 RID: 2437
		// (get) Token: 0x060027CD RID: 10189 RVA: 0x000394B0 File Offset: 0x000376B0
		public string sectionName
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000986 RID: 2438
		// (get) Token: 0x060027CE RID: 10190 RVA: 0x000394CC File Offset: 0x000376CC
		public byte[] sectionData
		{
			get
			{
				return this.data;
			}
		}

		// Token: 0x17000987 RID: 2439
		// (get) Token: 0x060027CF RID: 10191 RVA: 0x000394E8 File Offset: 0x000376E8
		public string fileName
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000988 RID: 2440
		// (get) Token: 0x060027D0 RID: 10192 RVA: 0x00039500 File Offset: 0x00037700
		public string contentType
		{
			get
			{
				return this.content;
			}
		}

		// Token: 0x0400090F RID: 2319
		private string name;

		// Token: 0x04000910 RID: 2320
		private byte[] data;

		// Token: 0x04000911 RID: 2321
		private string content;
	}
}
