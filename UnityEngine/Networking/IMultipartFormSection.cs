﻿using System;

namespace UnityEngine.Networking
{
	// Token: 0x02000249 RID: 585
	public interface IMultipartFormSection
	{
		// Token: 0x17000981 RID: 2433
		// (get) Token: 0x060027C2 RID: 10178
		string sectionName { get; }

		// Token: 0x17000982 RID: 2434
		// (get) Token: 0x060027C3 RID: 10179
		byte[] sectionData { get; }

		// Token: 0x17000983 RID: 2435
		// (get) Token: 0x060027C4 RID: 10180
		string fileName { get; }

		// Token: 0x17000984 RID: 2436
		// (get) Token: 0x060027C5 RID: 10181
		string contentType { get; }
	}
}
