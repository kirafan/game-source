﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x0200026B RID: 619
	[DefaultValue(18446744073709551615UL)]
	public enum NetworkID : ulong
	{
		// Token: 0x04000968 RID: 2408
		Invalid = 18446744073709551615UL
	}
}
