﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x0200026C RID: 620
	[DefaultValue(NodeID.Invalid)]
	public enum NodeID : ushort
	{
		// Token: 0x0400096A RID: 2410
		Invalid
	}
}
