﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x02000268 RID: 616
	[DefaultValue(NetworkAccessLevel.Invalid)]
	public enum NetworkAccessLevel : ulong
	{
		// Token: 0x0400095F RID: 2399
		Invalid,
		// Token: 0x04000960 RID: 2400
		User,
		// Token: 0x04000961 RID: 2401
		Owner,
		// Token: 0x04000962 RID: 2402
		Admin = 4UL
	}
}
