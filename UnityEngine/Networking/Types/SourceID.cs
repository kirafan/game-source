﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x0200026A RID: 618
	[DefaultValue(18446744073709551615UL)]
	public enum SourceID : ulong
	{
		// Token: 0x04000966 RID: 2406
		Invalid = 18446744073709551615UL
	}
}
