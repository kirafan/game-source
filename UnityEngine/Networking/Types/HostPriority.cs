﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x0200026D RID: 621
	[DefaultValue(HostPriority.Invalid)]
	public enum HostPriority
	{
		// Token: 0x0400096C RID: 2412
		Invalid = 2147483647
	}
}
