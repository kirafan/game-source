﻿using System;
using System.ComponentModel;

namespace UnityEngine.Networking.Types
{
	// Token: 0x02000269 RID: 617
	[DefaultValue(18446744073709551615UL)]
	public enum AppID : ulong
	{
		// Token: 0x04000964 RID: 2404
		Invalid = 18446744073709551615UL
	}
}
