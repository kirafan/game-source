﻿using System;

namespace UnityEngine.Networking.Types
{
	// Token: 0x0200026E RID: 622
	public class NetworkAccessToken
	{
		// Token: 0x060028EC RID: 10476 RVA: 0x0003B994 File Offset: 0x00039B94
		public NetworkAccessToken()
		{
			this.array = new byte[64];
		}

		// Token: 0x060028ED RID: 10477 RVA: 0x0003B9AC File Offset: 0x00039BAC
		public NetworkAccessToken(byte[] array)
		{
			this.array = array;
		}

		// Token: 0x060028EE RID: 10478 RVA: 0x0003B9BC File Offset: 0x00039BBC
		public NetworkAccessToken(string strArray)
		{
			try
			{
				this.array = Convert.FromBase64String(strArray);
			}
			catch (Exception)
			{
				this.array = new byte[64];
			}
		}

		// Token: 0x060028EF RID: 10479 RVA: 0x0003BA08 File Offset: 0x00039C08
		public string GetByteString()
		{
			return Convert.ToBase64String(this.array);
		}

		// Token: 0x060028F0 RID: 10480 RVA: 0x0003BA28 File Offset: 0x00039C28
		public bool IsValid()
		{
			bool result;
			if (this.array == null || this.array.Length != 64)
			{
				result = false;
			}
			else
			{
				bool flag = false;
				foreach (byte b in this.array)
				{
					if (b != 0)
					{
						flag = true;
						break;
					}
				}
				result = flag;
			}
			return result;
		}

		// Token: 0x0400096D RID: 2413
		private const int NETWORK_ACCESS_TOKEN_SIZE = 64;

		// Token: 0x0400096E RID: 2414
		public byte[] array;
	}
}
