﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Networking
{
	// Token: 0x02000281 RID: 641
	public sealed class ConnectionSimulatorConfig : IDisposable
	{
		// Token: 0x060029DE RID: 10718
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern ConnectionSimulatorConfig(int outMinDelay, int outAvgDelay, int inMinDelay, int inAvgDelay, float packetLossPercentage);

		// Token: 0x060029DF RID: 10719
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Dispose();

		// Token: 0x060029E0 RID: 10720 RVA: 0x0003E050 File Offset: 0x0003C250
		~ConnectionSimulatorConfig()
		{
			this.Dispose();
		}

		// Token: 0x040009CD RID: 2509
		internal IntPtr m_Ptr;
	}
}
