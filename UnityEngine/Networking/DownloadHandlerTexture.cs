﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine.Networking
{
	// Token: 0x02000252 RID: 594
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DownloadHandlerTexture : DownloadHandler
	{
		// Token: 0x06002813 RID: 10259 RVA: 0x00039C00 File Offset: 0x00037E00
		public DownloadHandlerTexture()
		{
			base.InternalCreateTexture(true);
		}

		// Token: 0x06002814 RID: 10260 RVA: 0x00039C10 File Offset: 0x00037E10
		public DownloadHandlerTexture(bool readable)
		{
			base.InternalCreateTexture(readable);
			this.mNonReadable = !readable;
		}

		// Token: 0x06002815 RID: 10261 RVA: 0x00039C2C File Offset: 0x00037E2C
		protected override byte[] GetData()
		{
			return this.InternalGetData();
		}

		// Token: 0x17000993 RID: 2451
		// (get) Token: 0x06002816 RID: 10262 RVA: 0x00039C48 File Offset: 0x00037E48
		public Texture2D texture
		{
			get
			{
				return this.InternalGetTexture();
			}
		}

		// Token: 0x06002817 RID: 10263 RVA: 0x00039C64 File Offset: 0x00037E64
		private Texture2D InternalGetTexture()
		{
			if (this.mHasTexture)
			{
				if (this.mTexture == null)
				{
					this.mTexture = new Texture2D(2, 2);
					this.mTexture.LoadImage(this.GetData(), this.mNonReadable);
				}
			}
			else if (this.mTexture == null)
			{
				this.mTexture = this.InternalGetTextureNative();
				this.mHasTexture = true;
			}
			return this.mTexture;
		}

		// Token: 0x06002818 RID: 10264
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Texture2D InternalGetTextureNative();

		// Token: 0x06002819 RID: 10265
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern byte[] InternalGetData();

		// Token: 0x0600281A RID: 10266 RVA: 0x00039CF0 File Offset: 0x00037EF0
		public static Texture2D GetContent(UnityWebRequest www)
		{
			return DownloadHandler.GetCheckedDownloader<DownloadHandlerTexture>(www).texture;
		}

		// Token: 0x04000919 RID: 2329
		private Texture2D mTexture;

		// Token: 0x0400091A RID: 2330
		private bool mHasTexture;

		// Token: 0x0400091B RID: 2331
		private bool mNonReadable;
	}
}
