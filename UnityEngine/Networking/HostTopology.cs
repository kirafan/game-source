﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Networking
{
	// Token: 0x0200027F RID: 639
	[Serializable]
	public class HostTopology
	{
		// Token: 0x060029C5 RID: 10693 RVA: 0x0003DCB0 File Offset: 0x0003BEB0
		public HostTopology(ConnectionConfig defaultConfig, int maxDefaultConnections)
		{
			if (defaultConfig == null)
			{
				throw new NullReferenceException("config is not defined");
			}
			if (maxDefaultConnections <= 0)
			{
				throw new ArgumentOutOfRangeException("maxDefaultConnections", "Number of connections should be > 0");
			}
			if (maxDefaultConnections >= 65535)
			{
				throw new ArgumentOutOfRangeException("maxDefaultConnections", "Number of connections should be < 65535");
			}
			ConnectionConfig.Validate(defaultConfig);
			this.m_DefConfig = new ConnectionConfig(defaultConfig);
			this.m_MaxDefConnections = maxDefaultConnections;
		}

		// Token: 0x060029C6 RID: 10694 RVA: 0x0003DD5C File Offset: 0x0003BF5C
		private HostTopology()
		{
		}

		// Token: 0x17000A05 RID: 2565
		// (get) Token: 0x060029C7 RID: 10695 RVA: 0x0003DDAC File Offset: 0x0003BFAC
		public ConnectionConfig DefaultConfig
		{
			get
			{
				return this.m_DefConfig;
			}
		}

		// Token: 0x17000A06 RID: 2566
		// (get) Token: 0x060029C8 RID: 10696 RVA: 0x0003DDC8 File Offset: 0x0003BFC8
		public int MaxDefaultConnections
		{
			get
			{
				return this.m_MaxDefConnections;
			}
		}

		// Token: 0x17000A07 RID: 2567
		// (get) Token: 0x060029C9 RID: 10697 RVA: 0x0003DDE4 File Offset: 0x0003BFE4
		public int SpecialConnectionConfigsCount
		{
			get
			{
				return this.m_SpecialConnections.Count;
			}
		}

		// Token: 0x17000A08 RID: 2568
		// (get) Token: 0x060029CA RID: 10698 RVA: 0x0003DE04 File Offset: 0x0003C004
		public List<ConnectionConfig> SpecialConnectionConfigs
		{
			get
			{
				return this.m_SpecialConnections;
			}
		}

		// Token: 0x060029CB RID: 10699 RVA: 0x0003DE20 File Offset: 0x0003C020
		public ConnectionConfig GetSpecialConnectionConfig(int i)
		{
			if (i > this.m_SpecialConnections.Count || i == 0)
			{
				throw new ArgumentException("special configuration index is out of valid range");
			}
			return this.m_SpecialConnections[i - 1];
		}

		// Token: 0x17000A09 RID: 2569
		// (get) Token: 0x060029CC RID: 10700 RVA: 0x0003DE68 File Offset: 0x0003C068
		// (set) Token: 0x060029CD RID: 10701 RVA: 0x0003DE84 File Offset: 0x0003C084
		public ushort ReceivedMessagePoolSize
		{
			get
			{
				return this.m_ReceivedMessagePoolSize;
			}
			set
			{
				this.m_ReceivedMessagePoolSize = value;
			}
		}

		// Token: 0x17000A0A RID: 2570
		// (get) Token: 0x060029CE RID: 10702 RVA: 0x0003DE90 File Offset: 0x0003C090
		// (set) Token: 0x060029CF RID: 10703 RVA: 0x0003DEAC File Offset: 0x0003C0AC
		public ushort SentMessagePoolSize
		{
			get
			{
				return this.m_SentMessagePoolSize;
			}
			set
			{
				this.m_SentMessagePoolSize = value;
			}
		}

		// Token: 0x17000A0B RID: 2571
		// (get) Token: 0x060029D0 RID: 10704 RVA: 0x0003DEB8 File Offset: 0x0003C0B8
		// (set) Token: 0x060029D1 RID: 10705 RVA: 0x0003DED4 File Offset: 0x0003C0D4
		public float MessagePoolSizeGrowthFactor
		{
			get
			{
				return this.m_MessagePoolSizeGrowthFactor;
			}
			set
			{
				if ((double)value <= 0.5 || (double)value > 1.0)
				{
					throw new ArgumentException("pool growth factor should be varied between 0.5 and 1.0");
				}
				this.m_MessagePoolSizeGrowthFactor = value;
			}
		}

		// Token: 0x060029D2 RID: 10706 RVA: 0x0003DF0C File Offset: 0x0003C10C
		public int AddSpecialConnectionConfig(ConnectionConfig config)
		{
			this.m_SpecialConnections.Add(new ConnectionConfig(config));
			return this.m_SpecialConnections.Count;
		}

		// Token: 0x040009C2 RID: 2498
		[SerializeField]
		private ConnectionConfig m_DefConfig = null;

		// Token: 0x040009C3 RID: 2499
		[SerializeField]
		private int m_MaxDefConnections = 0;

		// Token: 0x040009C4 RID: 2500
		[SerializeField]
		private List<ConnectionConfig> m_SpecialConnections = new List<ConnectionConfig>();

		// Token: 0x040009C5 RID: 2501
		[SerializeField]
		private ushort m_ReceivedMessagePoolSize = 128;

		// Token: 0x040009C6 RID: 2502
		[SerializeField]
		private ushort m_SentMessagePoolSize = 128;

		// Token: 0x040009C7 RID: 2503
		[SerializeField]
		private float m_MessagePoolSizeGrowthFactor = 0.75f;
	}
}
