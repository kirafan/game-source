﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Networking
{
	// Token: 0x02000282 RID: 642
	internal sealed class ConnectionConfigInternal : IDisposable
	{
		// Token: 0x060029E1 RID: 10721 RVA: 0x0003E080 File Offset: 0x0003C280
		private ConnectionConfigInternal()
		{
		}

		// Token: 0x060029E2 RID: 10722 RVA: 0x0003E08C File Offset: 0x0003C28C
		public ConnectionConfigInternal(ConnectionConfig config)
		{
			if (config == null)
			{
				throw new NullReferenceException("config is not defined");
			}
			this.InitWrapper();
			this.InitPacketSize(config.PacketSize);
			this.InitFragmentSize(config.FragmentSize);
			this.InitResendTimeout(config.ResendTimeout);
			this.InitDisconnectTimeout(config.DisconnectTimeout);
			this.InitConnectTimeout(config.ConnectTimeout);
			this.InitMinUpdateTimeout(config.MinUpdateTimeout);
			this.InitPingTimeout(config.PingTimeout);
			this.InitReducedPingTimeout(config.ReducedPingTimeout);
			this.InitAllCostTimeout(config.AllCostTimeout);
			this.InitNetworkDropThreshold(config.NetworkDropThreshold);
			this.InitOverflowDropThreshold(config.OverflowDropThreshold);
			this.InitMaxConnectionAttempt(config.MaxConnectionAttempt);
			this.InitAckDelay(config.AckDelay);
			this.InitMaxCombinedReliableMessageSize(config.MaxCombinedReliableMessageSize);
			this.InitMaxCombinedReliableMessageCount(config.MaxCombinedReliableMessageCount);
			this.InitMaxSentMessageQueueSize(config.MaxSentMessageQueueSize);
			this.InitIsAcksLong(config.IsAcksLong);
			this.InitUsePlatformSpecificProtocols(config.UsePlatformSpecificProtocols);
			this.InitWebSocketReceiveBufferMaxSize(config.WebSocketReceiveBufferMaxSize);
			byte b = 0;
			while ((int)b < config.ChannelCount)
			{
				this.AddChannel(config.GetChannel(b));
				b += 1;
			}
		}

		// Token: 0x060029E3 RID: 10723
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitWrapper();

		// Token: 0x060029E4 RID: 10724
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern byte AddChannel(QosType value);

		// Token: 0x060029E5 RID: 10725
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern QosType GetChannel(int i);

		// Token: 0x17000A11 RID: 2577
		// (get) Token: 0x060029E6 RID: 10726
		public extern int ChannelSize { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060029E7 RID: 10727
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitPacketSize(ushort value);

		// Token: 0x060029E8 RID: 10728
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitFragmentSize(ushort value);

		// Token: 0x060029E9 RID: 10729
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitResendTimeout(uint value);

		// Token: 0x060029EA RID: 10730
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitDisconnectTimeout(uint value);

		// Token: 0x060029EB RID: 10731
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitConnectTimeout(uint value);

		// Token: 0x060029EC RID: 10732
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitMinUpdateTimeout(uint value);

		// Token: 0x060029ED RID: 10733
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitPingTimeout(uint value);

		// Token: 0x060029EE RID: 10734
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitReducedPingTimeout(uint value);

		// Token: 0x060029EF RID: 10735
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitAllCostTimeout(uint value);

		// Token: 0x060029F0 RID: 10736
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitNetworkDropThreshold(byte value);

		// Token: 0x060029F1 RID: 10737
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitOverflowDropThreshold(byte value);

		// Token: 0x060029F2 RID: 10738
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitMaxConnectionAttempt(byte value);

		// Token: 0x060029F3 RID: 10739
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitAckDelay(uint value);

		// Token: 0x060029F4 RID: 10740
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitMaxCombinedReliableMessageSize(ushort value);

		// Token: 0x060029F5 RID: 10741
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitMaxCombinedReliableMessageCount(ushort value);

		// Token: 0x060029F6 RID: 10742
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitMaxSentMessageQueueSize(ushort value);

		// Token: 0x060029F7 RID: 10743
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitIsAcksLong(bool value);

		// Token: 0x060029F8 RID: 10744
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitUsePlatformSpecificProtocols(bool value);

		// Token: 0x060029F9 RID: 10745
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitWebSocketReceiveBufferMaxSize(ushort value);

		// Token: 0x060029FA RID: 10746
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Dispose();

		// Token: 0x060029FB RID: 10747 RVA: 0x0003E1C4 File Offset: 0x0003C3C4
		~ConnectionConfigInternal()
		{
			this.Dispose();
		}

		// Token: 0x040009CE RID: 2510
		internal IntPtr m_Ptr;
	}
}
