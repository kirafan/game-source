﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000061 RID: 97
	[UsedByNativeCode]
	public sealed class ImageEffectOpaque : Attribute
	{
	}
}
