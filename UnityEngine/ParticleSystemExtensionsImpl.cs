﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200012F RID: 303
	internal sealed class ParticleSystemExtensionsImpl
	{
		// Token: 0x060015E7 RID: 5607
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetSafeCollisionEventSize(ParticleSystem ps);

		// Token: 0x060015E8 RID: 5608
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetCollisionEventsDeprecated(ParticleSystem ps, GameObject go, ParticleCollisionEvent[] collisionEvents);

		// Token: 0x060015E9 RID: 5609
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetSafeTriggerParticlesSize(ParticleSystem ps, int type);

		// Token: 0x060015EA RID: 5610
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetCollisionEvents(ParticleSystem ps, GameObject go, object collisionEvents);

		// Token: 0x060015EB RID: 5611
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetTriggerParticles(ParticleSystem ps, int type, object particles);

		// Token: 0x060015EC RID: 5612
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetTriggerParticles(ParticleSystem ps, int type, object particles, int offset, int count);
	}
}
