﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001CF RID: 463
	[RequiredByNativeCode]
	public struct AnimatorTransitionInfo
	{
		// Token: 0x06001ECE RID: 7886 RVA: 0x0002341C File Offset: 0x0002161C
		public bool IsName(string name)
		{
			return Animator.StringToHash(name) == this.m_Name || Animator.StringToHash(name) == this.m_FullPath;
		}

		// Token: 0x06001ECF RID: 7887 RVA: 0x00023454 File Offset: 0x00021654
		public bool IsUserName(string name)
		{
			return Animator.StringToHash(name) == this.m_UserName;
		}

		// Token: 0x170007B7 RID: 1975
		// (get) Token: 0x06001ED0 RID: 7888 RVA: 0x00023478 File Offset: 0x00021678
		public int fullPathHash
		{
			get
			{
				return this.m_FullPath;
			}
		}

		// Token: 0x170007B8 RID: 1976
		// (get) Token: 0x06001ED1 RID: 7889 RVA: 0x00023494 File Offset: 0x00021694
		public int nameHash
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x170007B9 RID: 1977
		// (get) Token: 0x06001ED2 RID: 7890 RVA: 0x000234B0 File Offset: 0x000216B0
		public int userNameHash
		{
			get
			{
				return this.m_UserName;
			}
		}

		// Token: 0x170007BA RID: 1978
		// (get) Token: 0x06001ED3 RID: 7891 RVA: 0x000234CC File Offset: 0x000216CC
		public float normalizedTime
		{
			get
			{
				return this.m_NormalizedTime;
			}
		}

		// Token: 0x170007BB RID: 1979
		// (get) Token: 0x06001ED4 RID: 7892 RVA: 0x000234E8 File Offset: 0x000216E8
		public bool anyState
		{
			get
			{
				return this.m_AnyState;
			}
		}

		// Token: 0x170007BC RID: 1980
		// (get) Token: 0x06001ED5 RID: 7893 RVA: 0x00023504 File Offset: 0x00021704
		internal bool entry
		{
			get
			{
				return (this.m_TransitionType & 2) != 0;
			}
		}

		// Token: 0x170007BD RID: 1981
		// (get) Token: 0x06001ED6 RID: 7894 RVA: 0x00023528 File Offset: 0x00021728
		internal bool exit
		{
			get
			{
				return (this.m_TransitionType & 4) != 0;
			}
		}

		// Token: 0x040004FF RID: 1279
		private int m_FullPath;

		// Token: 0x04000500 RID: 1280
		private int m_UserName;

		// Token: 0x04000501 RID: 1281
		private int m_Name;

		// Token: 0x04000502 RID: 1282
		private float m_NormalizedTime;

		// Token: 0x04000503 RID: 1283
		private bool m_AnyState;

		// Token: 0x04000504 RID: 1284
		private int m_TransitionType;
	}
}
