﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000CD RID: 205
	public sealed class Cubemap : Texture
	{
		// Token: 0x06000E10 RID: 3600 RVA: 0x000135BC File Offset: 0x000117BC
		public Cubemap(int size, TextureFormat format, bool mipmap)
		{
			Cubemap.Internal_Create(this, size, format, mipmap);
		}

		// Token: 0x06000E11 RID: 3601 RVA: 0x000135D0 File Offset: 0x000117D0
		public void SetPixel(CubemapFace face, int x, int y, Color color)
		{
			Cubemap.INTERNAL_CALL_SetPixel(this, face, x, y, ref color);
		}

		// Token: 0x06000E12 RID: 3602
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetPixel(Cubemap self, CubemapFace face, int x, int y, ref Color color);

		// Token: 0x06000E13 RID: 3603 RVA: 0x000135E0 File Offset: 0x000117E0
		public Color GetPixel(CubemapFace face, int x, int y)
		{
			Color result;
			Cubemap.INTERNAL_CALL_GetPixel(this, face, x, y, out result);
			return result;
		}

		// Token: 0x06000E14 RID: 3604
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPixel(Cubemap self, CubemapFace face, int x, int y, out Color value);

		// Token: 0x06000E15 RID: 3605
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(CubemapFace face, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E16 RID: 3606 RVA: 0x00013604 File Offset: 0x00011804
		[ExcludeFromDocs]
		public Color[] GetPixels(CubemapFace face)
		{
			int miplevel = 0;
			return this.GetPixels(face, miplevel);
		}

		// Token: 0x06000E17 RID: 3607
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels(Color[] colors, CubemapFace face, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E18 RID: 3608 RVA: 0x00013624 File Offset: 0x00011824
		[ExcludeFromDocs]
		public void SetPixels(Color[] colors, CubemapFace face)
		{
			int miplevel = 0;
			this.SetPixels(colors, face, miplevel);
		}

		// Token: 0x17000309 RID: 777
		// (get) Token: 0x06000E19 RID: 3609
		public extern int mipmapCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000E1A RID: 3610
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Apply([DefaultValue("true")] bool updateMipmaps, [DefaultValue("false")] bool makeNoLongerReadable);

		// Token: 0x06000E1B RID: 3611 RVA: 0x00013640 File Offset: 0x00011840
		[ExcludeFromDocs]
		public void Apply(bool updateMipmaps)
		{
			bool makeNoLongerReadable = false;
			this.Apply(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06000E1C RID: 3612 RVA: 0x00013658 File Offset: 0x00011858
		[ExcludeFromDocs]
		public void Apply()
		{
			bool makeNoLongerReadable = false;
			bool updateMipmaps = true;
			this.Apply(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x06000E1D RID: 3613
		public extern TextureFormat format { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000E1E RID: 3614
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] Cubemap mono, int size, TextureFormat format, bool mipmap);

		// Token: 0x06000E1F RID: 3615
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SmoothEdges([DefaultValue("1")] int smoothRegionWidthInPixels);

		// Token: 0x06000E20 RID: 3616 RVA: 0x00013674 File Offset: 0x00011874
		[ExcludeFromDocs]
		public void SmoothEdges()
		{
			int smoothRegionWidthInPixels = 1;
			this.SmoothEdges(smoothRegionWidthInPixels);
		}
	}
}
