﻿using System;

namespace UnityEngine
{
	// Token: 0x020000F9 RID: 249
	public enum ParticleSystemCollisionQuality
	{
		// Token: 0x04000292 RID: 658
		High,
		// Token: 0x04000293 RID: 659
		Medium,
		// Token: 0x04000294 RID: 660
		Low
	}
}
