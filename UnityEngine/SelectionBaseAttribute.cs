﻿using System;

namespace UnityEngine
{
	// Token: 0x02000369 RID: 873
	[AttributeUsage(AttributeTargets.Class, Inherited = true, AllowMultiple = false)]
	public class SelectionBaseAttribute : Attribute
	{
	}
}
