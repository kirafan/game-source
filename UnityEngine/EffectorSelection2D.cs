﻿using System;

namespace UnityEngine
{
	// Token: 0x0200017D RID: 381
	public enum EffectorSelection2D
	{
		// Token: 0x04000403 RID: 1027
		Rigidbody,
		// Token: 0x04000404 RID: 1028
		Collider
	}
}
