﻿using System;

namespace UnityEngine
{
	// Token: 0x0200014A RID: 330
	public enum ConfigurableJointMotion
	{
		// Token: 0x040003B0 RID: 944
		Locked,
		// Token: 0x040003B1 RID: 945
		Limited,
		// Token: 0x040003B2 RID: 946
		Free
	}
}
