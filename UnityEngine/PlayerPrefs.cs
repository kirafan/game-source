﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000A3 RID: 163
	public sealed class PlayerPrefs
	{
		// Token: 0x06000B30 RID: 2864
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool TrySetInt(string key, int value);

		// Token: 0x06000B31 RID: 2865
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool TrySetFloat(string key, float value);

		// Token: 0x06000B32 RID: 2866
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool TrySetSetString(string key, string value);

		// Token: 0x06000B33 RID: 2867 RVA: 0x0000FB8C File Offset: 0x0000DD8C
		public static void SetInt(string key, int value)
		{
			if (!PlayerPrefs.TrySetInt(key, value))
			{
				throw new PlayerPrefsException("Could not store preference value");
			}
		}

		// Token: 0x06000B34 RID: 2868
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetInt(string key, [DefaultValue("0")] int defaultValue);

		// Token: 0x06000B35 RID: 2869 RVA: 0x0000FBA8 File Offset: 0x0000DDA8
		[ExcludeFromDocs]
		public static int GetInt(string key)
		{
			int defaultValue = 0;
			return PlayerPrefs.GetInt(key, defaultValue);
		}

		// Token: 0x06000B36 RID: 2870 RVA: 0x0000FBC8 File Offset: 0x0000DDC8
		public static void SetFloat(string key, float value)
		{
			if (!PlayerPrefs.TrySetFloat(key, value))
			{
				throw new PlayerPrefsException("Could not store preference value");
			}
		}

		// Token: 0x06000B37 RID: 2871
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetFloat(string key, [DefaultValue("0.0F")] float defaultValue);

		// Token: 0x06000B38 RID: 2872 RVA: 0x0000FBE4 File Offset: 0x0000DDE4
		[ExcludeFromDocs]
		public static float GetFloat(string key)
		{
			float defaultValue = 0f;
			return PlayerPrefs.GetFloat(key, defaultValue);
		}

		// Token: 0x06000B39 RID: 2873 RVA: 0x0000FC08 File Offset: 0x0000DE08
		public static void SetString(string key, string value)
		{
			if (!PlayerPrefs.TrySetSetString(key, value))
			{
				throw new PlayerPrefsException("Could not store preference value");
			}
		}

		// Token: 0x06000B3A RID: 2874
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetString(string key, [DefaultValue("\"\"")] string defaultValue);

		// Token: 0x06000B3B RID: 2875 RVA: 0x0000FC24 File Offset: 0x0000DE24
		[ExcludeFromDocs]
		public static string GetString(string key)
		{
			string defaultValue = "";
			return PlayerPrefs.GetString(key, defaultValue);
		}

		// Token: 0x06000B3C RID: 2876
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool HasKey(string key);

		// Token: 0x06000B3D RID: 2877
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DeleteKey(string key);

		// Token: 0x06000B3E RID: 2878
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DeleteAll();

		// Token: 0x06000B3F RID: 2879
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Save();
	}
}
