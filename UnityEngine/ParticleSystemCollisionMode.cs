﻿using System;

namespace UnityEngine
{
	// Token: 0x02000102 RID: 258
	public enum ParticleSystemCollisionMode
	{
		// Token: 0x040002C5 RID: 709
		Collision3D,
		// Token: 0x040002C6 RID: 710
		Collision2D
	}
}
