﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200001C RID: 28
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class Coroutine : YieldInstruction
	{
		// Token: 0x060001F0 RID: 496 RVA: 0x00004F68 File Offset: 0x00003168
		private Coroutine()
		{
		}

		// Token: 0x060001F1 RID: 497
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ReleaseCoroutine();

		// Token: 0x060001F2 RID: 498 RVA: 0x00004F74 File Offset: 0x00003174
		~Coroutine()
		{
			this.ReleaseCoroutine();
		}

		// Token: 0x0400002B RID: 43
		internal IntPtr m_Ptr;
	}
}
