﻿using System;

namespace UnityEngine
{
	// Token: 0x02000066 RID: 102
	internal struct TouchScreenKeyboard_InternalConstructorHelperArguments
	{
		// Token: 0x04000096 RID: 150
		public uint keyboardType;

		// Token: 0x04000097 RID: 151
		public uint autocorrection;

		// Token: 0x04000098 RID: 152
		public uint multiline;

		// Token: 0x04000099 RID: 153
		public uint secure;

		// Token: 0x0400009A RID: 154
		public uint alert;
	}
}
