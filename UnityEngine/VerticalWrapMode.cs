﻿using System;

namespace UnityEngine
{
	// Token: 0x02000201 RID: 513
	public enum VerticalWrapMode
	{
		// Token: 0x04000623 RID: 1571
		Truncate,
		// Token: 0x04000624 RID: 1572
		Overflow
	}
}
