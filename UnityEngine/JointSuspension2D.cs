﻿using System;

namespace UnityEngine
{
	// Token: 0x0200016E RID: 366
	public struct JointSuspension2D
	{
		// Token: 0x1700062E RID: 1582
		// (get) Token: 0x06001AAC RID: 6828 RVA: 0x00021524 File Offset: 0x0001F724
		// (set) Token: 0x06001AAD RID: 6829 RVA: 0x00021540 File Offset: 0x0001F740
		public float dampingRatio
		{
			get
			{
				return this.m_DampingRatio;
			}
			set
			{
				this.m_DampingRatio = value;
			}
		}

		// Token: 0x1700062F RID: 1583
		// (get) Token: 0x06001AAE RID: 6830 RVA: 0x0002154C File Offset: 0x0001F74C
		// (set) Token: 0x06001AAF RID: 6831 RVA: 0x00021568 File Offset: 0x0001F768
		public float frequency
		{
			get
			{
				return this.m_Frequency;
			}
			set
			{
				this.m_Frequency = value;
			}
		}

		// Token: 0x17000630 RID: 1584
		// (get) Token: 0x06001AB0 RID: 6832 RVA: 0x00021574 File Offset: 0x0001F774
		// (set) Token: 0x06001AB1 RID: 6833 RVA: 0x00021590 File Offset: 0x0001F790
		public float angle
		{
			get
			{
				return this.m_Angle;
			}
			set
			{
				this.m_Angle = value;
			}
		}

		// Token: 0x040003FF RID: 1023
		private float m_DampingRatio;

		// Token: 0x04000400 RID: 1024
		private float m_Frequency;

		// Token: 0x04000401 RID: 1025
		private float m_Angle;
	}
}
