﻿using System;

namespace UnityEngine
{
	// Token: 0x02000391 RID: 913
	internal sealed class UnityString
	{
		// Token: 0x06002F13 RID: 12051 RVA: 0x0004C2E0 File Offset: 0x0004A4E0
		public static string Format(string fmt, params object[] args)
		{
			return string.Format(fmt, args);
		}
	}
}
