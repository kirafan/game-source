﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200003E RID: 62
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class Gradient
	{
		// Token: 0x06000466 RID: 1126 RVA: 0x000073B4 File Offset: 0x000055B4
		[RequiredByNativeCode]
		public Gradient()
		{
			this.Init();
		}

		// Token: 0x06000467 RID: 1127
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Init();

		// Token: 0x06000468 RID: 1128
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Cleanup();

		// Token: 0x06000469 RID: 1129 RVA: 0x000073C4 File Offset: 0x000055C4
		~Gradient()
		{
			this.Cleanup();
		}

		// Token: 0x0600046A RID: 1130 RVA: 0x000073F4 File Offset: 0x000055F4
		public Color Evaluate(float time)
		{
			Color result;
			Gradient.INTERNAL_CALL_Evaluate(this, time, out result);
			return result;
		}

		// Token: 0x0600046B RID: 1131
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Evaluate(Gradient self, float time, out Color value);

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x0600046C RID: 1132
		// (set) Token: 0x0600046D RID: 1133
		public extern GradientColorKey[] colorKeys { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x0600046E RID: 1134
		// (set) Token: 0x0600046F RID: 1135
		public extern GradientAlphaKey[] alphaKeys { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x06000470 RID: 1136
		// (set) Token: 0x06000471 RID: 1137
		public extern GradientMode mode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000472 RID: 1138
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetKeys(GradientColorKey[] colorKeys, GradientAlphaKey[] alphaKeys);

		// Token: 0x04000060 RID: 96
		internal IntPtr m_Ptr;
	}
}
