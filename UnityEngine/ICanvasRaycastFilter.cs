﻿using System;

namespace UnityEngine
{
	// Token: 0x0200020E RID: 526
	public interface ICanvasRaycastFilter
	{
		// Token: 0x06002367 RID: 9063
		bool IsRaycastLocationValid(Vector2 sp, Camera eventCamera);
	}
}
