﻿using System;

namespace UnityEngine
{
	// Token: 0x02000358 RID: 856
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = true)]
	public class ContextMenuItemAttribute : PropertyAttribute
	{
		// Token: 0x06002DD9 RID: 11737 RVA: 0x00048E20 File Offset: 0x00047020
		public ContextMenuItemAttribute(string name, string function)
		{
			this.name = name;
			this.function = function;
		}

		// Token: 0x04000D26 RID: 3366
		public readonly string name;

		// Token: 0x04000D27 RID: 3367
		public readonly string function;
	}
}
