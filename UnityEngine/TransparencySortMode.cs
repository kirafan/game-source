﻿using System;

namespace UnityEngine
{
	// Token: 0x020002E4 RID: 740
	public enum TransparencySortMode
	{
		// Token: 0x04000AE3 RID: 2787
		Default,
		// Token: 0x04000AE4 RID: 2788
		Perspective,
		// Token: 0x04000AE5 RID: 2789
		Orthographic
	}
}
