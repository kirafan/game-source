﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000364 RID: 868
	[UsedByNativeCode]
	public struct Rect
	{
		// Token: 0x06002DF8 RID: 11768 RVA: 0x00049238 File Offset: 0x00047438
		public Rect(float x, float y, float width, float height)
		{
			this.m_XMin = x;
			this.m_YMin = y;
			this.m_Width = width;
			this.m_Height = height;
		}

		// Token: 0x06002DF9 RID: 11769 RVA: 0x00049258 File Offset: 0x00047458
		public Rect(Vector2 position, Vector2 size)
		{
			this.m_XMin = position.x;
			this.m_YMin = position.y;
			this.m_Width = size.x;
			this.m_Height = size.y;
		}

		// Token: 0x06002DFA RID: 11770 RVA: 0x00049290 File Offset: 0x00047490
		public Rect(Rect source)
		{
			this.m_XMin = source.m_XMin;
			this.m_YMin = source.m_YMin;
			this.m_Width = source.m_Width;
			this.m_Height = source.m_Height;
		}

		// Token: 0x17000AE4 RID: 2788
		// (get) Token: 0x06002DFB RID: 11771 RVA: 0x000492C8 File Offset: 0x000474C8
		public static Rect zero
		{
			get
			{
				return new Rect(0f, 0f, 0f, 0f);
			}
		}

		// Token: 0x06002DFC RID: 11772 RVA: 0x000492F8 File Offset: 0x000474F8
		public static Rect MinMaxRect(float xmin, float ymin, float xmax, float ymax)
		{
			return new Rect(xmin, ymin, xmax - xmin, ymax - ymin);
		}

		// Token: 0x06002DFD RID: 11773 RVA: 0x0004931C File Offset: 0x0004751C
		public void Set(float x, float y, float width, float height)
		{
			this.m_XMin = x;
			this.m_YMin = y;
			this.m_Width = width;
			this.m_Height = height;
		}

		// Token: 0x17000AE5 RID: 2789
		// (get) Token: 0x06002DFE RID: 11774 RVA: 0x0004933C File Offset: 0x0004753C
		// (set) Token: 0x06002DFF RID: 11775 RVA: 0x00049358 File Offset: 0x00047558
		public float x
		{
			get
			{
				return this.m_XMin;
			}
			set
			{
				this.m_XMin = value;
			}
		}

		// Token: 0x17000AE6 RID: 2790
		// (get) Token: 0x06002E00 RID: 11776 RVA: 0x00049364 File Offset: 0x00047564
		// (set) Token: 0x06002E01 RID: 11777 RVA: 0x00049380 File Offset: 0x00047580
		public float y
		{
			get
			{
				return this.m_YMin;
			}
			set
			{
				this.m_YMin = value;
			}
		}

		// Token: 0x17000AE7 RID: 2791
		// (get) Token: 0x06002E02 RID: 11778 RVA: 0x0004938C File Offset: 0x0004758C
		// (set) Token: 0x06002E03 RID: 11779 RVA: 0x000493B4 File Offset: 0x000475B4
		public Vector2 position
		{
			get
			{
				return new Vector2(this.m_XMin, this.m_YMin);
			}
			set
			{
				this.m_XMin = value.x;
				this.m_YMin = value.y;
			}
		}

		// Token: 0x17000AE8 RID: 2792
		// (get) Token: 0x06002E04 RID: 11780 RVA: 0x000493D4 File Offset: 0x000475D4
		// (set) Token: 0x06002E05 RID: 11781 RVA: 0x00049414 File Offset: 0x00047614
		public Vector2 center
		{
			get
			{
				return new Vector2(this.x + this.m_Width / 2f, this.y + this.m_Height / 2f);
			}
			set
			{
				this.m_XMin = value.x - this.m_Width / 2f;
				this.m_YMin = value.y - this.m_Height / 2f;
			}
		}

		// Token: 0x17000AE9 RID: 2793
		// (get) Token: 0x06002E06 RID: 11782 RVA: 0x0004944C File Offset: 0x0004764C
		// (set) Token: 0x06002E07 RID: 11783 RVA: 0x00049474 File Offset: 0x00047674
		public Vector2 min
		{
			get
			{
				return new Vector2(this.xMin, this.yMin);
			}
			set
			{
				this.xMin = value.x;
				this.yMin = value.y;
			}
		}

		// Token: 0x17000AEA RID: 2794
		// (get) Token: 0x06002E08 RID: 11784 RVA: 0x00049494 File Offset: 0x00047694
		// (set) Token: 0x06002E09 RID: 11785 RVA: 0x000494BC File Offset: 0x000476BC
		public Vector2 max
		{
			get
			{
				return new Vector2(this.xMax, this.yMax);
			}
			set
			{
				this.xMax = value.x;
				this.yMax = value.y;
			}
		}

		// Token: 0x17000AEB RID: 2795
		// (get) Token: 0x06002E0A RID: 11786 RVA: 0x000494DC File Offset: 0x000476DC
		// (set) Token: 0x06002E0B RID: 11787 RVA: 0x000494F8 File Offset: 0x000476F8
		public float width
		{
			get
			{
				return this.m_Width;
			}
			set
			{
				this.m_Width = value;
			}
		}

		// Token: 0x17000AEC RID: 2796
		// (get) Token: 0x06002E0C RID: 11788 RVA: 0x00049504 File Offset: 0x00047704
		// (set) Token: 0x06002E0D RID: 11789 RVA: 0x00049520 File Offset: 0x00047720
		public float height
		{
			get
			{
				return this.m_Height;
			}
			set
			{
				this.m_Height = value;
			}
		}

		// Token: 0x17000AED RID: 2797
		// (get) Token: 0x06002E0E RID: 11790 RVA: 0x0004952C File Offset: 0x0004772C
		// (set) Token: 0x06002E0F RID: 11791 RVA: 0x00049554 File Offset: 0x00047754
		public Vector2 size
		{
			get
			{
				return new Vector2(this.m_Width, this.m_Height);
			}
			set
			{
				this.m_Width = value.x;
				this.m_Height = value.y;
			}
		}

		// Token: 0x17000AEE RID: 2798
		// (get) Token: 0x06002E10 RID: 11792 RVA: 0x00049574 File Offset: 0x00047774
		// (set) Token: 0x06002E11 RID: 11793 RVA: 0x00049590 File Offset: 0x00047790
		public float xMin
		{
			get
			{
				return this.m_XMin;
			}
			set
			{
				float xMax = this.xMax;
				this.m_XMin = value;
				this.m_Width = xMax - this.m_XMin;
			}
		}

		// Token: 0x17000AEF RID: 2799
		// (get) Token: 0x06002E12 RID: 11794 RVA: 0x000495BC File Offset: 0x000477BC
		// (set) Token: 0x06002E13 RID: 11795 RVA: 0x000495D8 File Offset: 0x000477D8
		public float yMin
		{
			get
			{
				return this.m_YMin;
			}
			set
			{
				float yMax = this.yMax;
				this.m_YMin = value;
				this.m_Height = yMax - this.m_YMin;
			}
		}

		// Token: 0x17000AF0 RID: 2800
		// (get) Token: 0x06002E14 RID: 11796 RVA: 0x00049604 File Offset: 0x00047804
		// (set) Token: 0x06002E15 RID: 11797 RVA: 0x00049628 File Offset: 0x00047828
		public float xMax
		{
			get
			{
				return this.m_Width + this.m_XMin;
			}
			set
			{
				this.m_Width = value - this.m_XMin;
			}
		}

		// Token: 0x17000AF1 RID: 2801
		// (get) Token: 0x06002E16 RID: 11798 RVA: 0x0004963C File Offset: 0x0004783C
		// (set) Token: 0x06002E17 RID: 11799 RVA: 0x00049660 File Offset: 0x00047860
		public float yMax
		{
			get
			{
				return this.m_Height + this.m_YMin;
			}
			set
			{
				this.m_Height = value - this.m_YMin;
			}
		}

		// Token: 0x06002E18 RID: 11800 RVA: 0x00049674 File Offset: 0x00047874
		public bool Contains(Vector2 point)
		{
			return point.x >= this.xMin && point.x < this.xMax && point.y >= this.yMin && point.y < this.yMax;
		}

		// Token: 0x06002E19 RID: 11801 RVA: 0x000496D4 File Offset: 0x000478D4
		public bool Contains(Vector3 point)
		{
			return point.x >= this.xMin && point.x < this.xMax && point.y >= this.yMin && point.y < this.yMax;
		}

		// Token: 0x06002E1A RID: 11802 RVA: 0x00049734 File Offset: 0x00047934
		public bool Contains(Vector3 point, bool allowInverse)
		{
			bool result;
			if (!allowInverse)
			{
				result = this.Contains(point);
			}
			else
			{
				bool flag = false;
				if ((this.width < 0f && point.x <= this.xMin && point.x > this.xMax) || (this.width >= 0f && point.x >= this.xMin && point.x < this.xMax))
				{
					flag = true;
				}
				result = (flag && ((this.height < 0f && point.y <= this.yMin && point.y > this.yMax) || (this.height >= 0f && point.y >= this.yMin && point.y < this.yMax)));
			}
			return result;
		}

		// Token: 0x06002E1B RID: 11803 RVA: 0x00049840 File Offset: 0x00047A40
		private static Rect OrderMinMax(Rect rect)
		{
			if (rect.xMin > rect.xMax)
			{
				float xMin = rect.xMin;
				rect.xMin = rect.xMax;
				rect.xMax = xMin;
			}
			if (rect.yMin > rect.yMax)
			{
				float yMin = rect.yMin;
				rect.yMin = rect.yMax;
				rect.yMax = yMin;
			}
			return rect;
		}

		// Token: 0x06002E1C RID: 11804 RVA: 0x000498BC File Offset: 0x00047ABC
		public bool Overlaps(Rect other)
		{
			return other.xMax > this.xMin && other.xMin < this.xMax && other.yMax > this.yMin && other.yMin < this.yMax;
		}

		// Token: 0x06002E1D RID: 11805 RVA: 0x0004991C File Offset: 0x00047B1C
		public bool Overlaps(Rect other, bool allowInverse)
		{
			Rect rect = this;
			if (allowInverse)
			{
				rect = Rect.OrderMinMax(rect);
				other = Rect.OrderMinMax(other);
			}
			return rect.Overlaps(other);
		}

		// Token: 0x06002E1E RID: 11806 RVA: 0x00049958 File Offset: 0x00047B58
		public static Vector2 NormalizedToPoint(Rect rectangle, Vector2 normalizedRectCoordinates)
		{
			return new Vector2(Mathf.Lerp(rectangle.x, rectangle.xMax, normalizedRectCoordinates.x), Mathf.Lerp(rectangle.y, rectangle.yMax, normalizedRectCoordinates.y));
		}

		// Token: 0x06002E1F RID: 11807 RVA: 0x000499A8 File Offset: 0x00047BA8
		public static Vector2 PointToNormalized(Rect rectangle, Vector2 point)
		{
			return new Vector2(Mathf.InverseLerp(rectangle.x, rectangle.xMax, point.x), Mathf.InverseLerp(rectangle.y, rectangle.yMax, point.y));
		}

		// Token: 0x06002E20 RID: 11808 RVA: 0x000499F8 File Offset: 0x00047BF8
		public static bool operator !=(Rect lhs, Rect rhs)
		{
			return lhs.x != rhs.x || lhs.y != rhs.y || lhs.width != rhs.width || lhs.height != rhs.height;
		}

		// Token: 0x06002E21 RID: 11809 RVA: 0x00049A5C File Offset: 0x00047C5C
		public static bool operator ==(Rect lhs, Rect rhs)
		{
			return lhs.x == rhs.x && lhs.y == rhs.y && lhs.width == rhs.width && lhs.height == rhs.height;
		}

		// Token: 0x06002E22 RID: 11810 RVA: 0x00049AC0 File Offset: 0x00047CC0
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.width.GetHashCode() << 2 ^ this.y.GetHashCode() >> 2 ^ this.height.GetHashCode() >> 1;
		}

		// Token: 0x06002E23 RID: 11811 RVA: 0x00049B30 File Offset: 0x00047D30
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Rect))
			{
				result = false;
			}
			else
			{
				Rect rect = (Rect)other;
				result = (this.x.Equals(rect.x) && this.y.Equals(rect.y) && this.width.Equals(rect.width) && this.height.Equals(rect.height));
			}
			return result;
		}

		// Token: 0x06002E24 RID: 11812 RVA: 0x00049BC8 File Offset: 0x00047DC8
		public override string ToString()
		{
			return UnityString.Format("(x:{0:F2}, y:{1:F2}, width:{2:F2}, height:{3:F2})", new object[]
			{
				this.x,
				this.y,
				this.width,
				this.height
			});
		}

		// Token: 0x06002E25 RID: 11813 RVA: 0x00049C28 File Offset: 0x00047E28
		public string ToString(string format)
		{
			return UnityString.Format("(x:{0}, y:{1}, width:{2}, height:{3})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format),
				this.width.ToString(format),
				this.height.ToString(format)
			});
		}

		// Token: 0x17000AF2 RID: 2802
		// (get) Token: 0x06002E26 RID: 11814 RVA: 0x00049C98 File Offset: 0x00047E98
		[Obsolete("use xMin")]
		public float left
		{
			get
			{
				return this.m_XMin;
			}
		}

		// Token: 0x17000AF3 RID: 2803
		// (get) Token: 0x06002E27 RID: 11815 RVA: 0x00049CB4 File Offset: 0x00047EB4
		[Obsolete("use xMax")]
		public float right
		{
			get
			{
				return this.m_XMin + this.m_Width;
			}
		}

		// Token: 0x17000AF4 RID: 2804
		// (get) Token: 0x06002E28 RID: 11816 RVA: 0x00049CD8 File Offset: 0x00047ED8
		[Obsolete("use yMin")]
		public float top
		{
			get
			{
				return this.m_YMin;
			}
		}

		// Token: 0x17000AF5 RID: 2805
		// (get) Token: 0x06002E29 RID: 11817 RVA: 0x00049CF4 File Offset: 0x00047EF4
		[Obsolete("use yMax")]
		public float bottom
		{
			get
			{
				return this.m_YMin + this.m_Height;
			}
		}

		// Token: 0x04000D3C RID: 3388
		private float m_XMin;

		// Token: 0x04000D3D RID: 3389
		private float m_YMin;

		// Token: 0x04000D3E RID: 3390
		private float m_Width;

		// Token: 0x04000D3F RID: 3391
		private float m_Height;
	}
}
