﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200007B RID: 123
	public sealed class LightProbeProxyVolume : Behaviour
	{
		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000840 RID: 2112 RVA: 0x0000A1DC File Offset: 0x000083DC
		public Bounds boundsGlobal
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_boundsGlobal(out result);
				return result;
			}
		}

		// Token: 0x06000841 RID: 2113
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_boundsGlobal(out Bounds value);

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06000842 RID: 2114 RVA: 0x0000A1FC File Offset: 0x000083FC
		// (set) Token: 0x06000843 RID: 2115 RVA: 0x0000A21C File Offset: 0x0000841C
		public Vector3 sizeCustom
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_sizeCustom(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_sizeCustom(ref value);
			}
		}

		// Token: 0x06000844 RID: 2116
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_sizeCustom(out Vector3 value);

		// Token: 0x06000845 RID: 2117
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_sizeCustom(ref Vector3 value);

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000846 RID: 2118 RVA: 0x0000A228 File Offset: 0x00008428
		// (set) Token: 0x06000847 RID: 2119 RVA: 0x0000A248 File Offset: 0x00008448
		public Vector3 originCustom
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_originCustom(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_originCustom(ref value);
			}
		}

		// Token: 0x06000848 RID: 2120
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_originCustom(out Vector3 value);

		// Token: 0x06000849 RID: 2121
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_originCustom(ref Vector3 value);

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x0600084A RID: 2122
		// (set) Token: 0x0600084B RID: 2123
		public extern LightProbeProxyVolume.BoundingBoxMode boundingBoxMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x0600084C RID: 2124
		// (set) Token: 0x0600084D RID: 2125
		public extern LightProbeProxyVolume.ResolutionMode resolutionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x0600084E RID: 2126
		// (set) Token: 0x0600084F RID: 2127
		public extern LightProbeProxyVolume.ProbePositionMode probePositionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06000850 RID: 2128
		// (set) Token: 0x06000851 RID: 2129
		public extern LightProbeProxyVolume.RefreshMode refreshMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06000852 RID: 2130
		// (set) Token: 0x06000853 RID: 2131
		public extern float probeDensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x06000854 RID: 2132
		// (set) Token: 0x06000855 RID: 2133
		public extern int gridResolutionX { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700020C RID: 524
		// (get) Token: 0x06000856 RID: 2134
		// (set) Token: 0x06000857 RID: 2135
		public extern int gridResolutionY { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x06000858 RID: 2136
		// (set) Token: 0x06000859 RID: 2137
		public extern int gridResolutionZ { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600085A RID: 2138
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Update();

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x0600085B RID: 2139
		public static extern bool isFeatureSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0200007C RID: 124
		public enum ResolutionMode
		{
			// Token: 0x040000E5 RID: 229
			Automatic,
			// Token: 0x040000E6 RID: 230
			Custom
		}

		// Token: 0x0200007D RID: 125
		public enum BoundingBoxMode
		{
			// Token: 0x040000E8 RID: 232
			AutomaticLocal,
			// Token: 0x040000E9 RID: 233
			AutomaticWorld,
			// Token: 0x040000EA RID: 234
			Custom
		}

		// Token: 0x0200007E RID: 126
		public enum ProbePositionMode
		{
			// Token: 0x040000EC RID: 236
			CellCorner,
			// Token: 0x040000ED RID: 237
			CellCenter
		}

		// Token: 0x0200007F RID: 127
		public enum RefreshMode
		{
			// Token: 0x040000EF RID: 239
			Automatic,
			// Token: 0x040000F0 RID: 240
			EveryFrame,
			// Token: 0x040000F1 RID: 241
			ViaScripting
		}
	}
}
