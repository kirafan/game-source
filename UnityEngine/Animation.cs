﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020001C2 RID: 450
	public sealed class Animation : Behaviour, IEnumerable
	{
		// Token: 0x17000796 RID: 1942
		// (get) Token: 0x06001E63 RID: 7779
		// (set) Token: 0x06001E64 RID: 7780
		public extern AnimationClip clip { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000797 RID: 1943
		// (get) Token: 0x06001E65 RID: 7781
		// (set) Token: 0x06001E66 RID: 7782
		public extern bool playAutomatically { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000798 RID: 1944
		// (get) Token: 0x06001E67 RID: 7783
		// (set) Token: 0x06001E68 RID: 7784
		public extern WrapMode wrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001E69 RID: 7785 RVA: 0x00022ECC File Offset: 0x000210CC
		public void Stop()
		{
			Animation.INTERNAL_CALL_Stop(this);
		}

		// Token: 0x06001E6A RID: 7786
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Stop(Animation self);

		// Token: 0x06001E6B RID: 7787 RVA: 0x00022ED8 File Offset: 0x000210D8
		public void Stop(string name)
		{
			this.Internal_StopByName(name);
		}

		// Token: 0x06001E6C RID: 7788
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_StopByName(string name);

		// Token: 0x06001E6D RID: 7789 RVA: 0x00022EE4 File Offset: 0x000210E4
		public void Rewind(string name)
		{
			this.Internal_RewindByName(name);
		}

		// Token: 0x06001E6E RID: 7790
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_RewindByName(string name);

		// Token: 0x06001E6F RID: 7791 RVA: 0x00022EF0 File Offset: 0x000210F0
		public void Rewind()
		{
			Animation.INTERNAL_CALL_Rewind(this);
		}

		// Token: 0x06001E70 RID: 7792
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Rewind(Animation self);

		// Token: 0x06001E71 RID: 7793 RVA: 0x00022EFC File Offset: 0x000210FC
		public void Sample()
		{
			Animation.INTERNAL_CALL_Sample(this);
		}

		// Token: 0x06001E72 RID: 7794
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Sample(Animation self);

		// Token: 0x17000799 RID: 1945
		// (get) Token: 0x06001E73 RID: 7795
		public extern bool isPlaying { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001E74 RID: 7796
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsPlaying(string name);

		// Token: 0x1700079A RID: 1946
		public AnimationState this[string name]
		{
			get
			{
				return this.GetState(name);
			}
		}

		// Token: 0x06001E76 RID: 7798 RVA: 0x00022F24 File Offset: 0x00021124
		[ExcludeFromDocs]
		public bool Play()
		{
			PlayMode mode = PlayMode.StopSameLayer;
			return this.Play(mode);
		}

		// Token: 0x06001E77 RID: 7799 RVA: 0x00022F44 File Offset: 0x00021144
		public bool Play([DefaultValue("PlayMode.StopSameLayer")] PlayMode mode)
		{
			return this.PlayDefaultAnimation(mode);
		}

		// Token: 0x06001E78 RID: 7800
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool Play(string animation, [DefaultValue("PlayMode.StopSameLayer")] PlayMode mode);

		// Token: 0x06001E79 RID: 7801 RVA: 0x00022F60 File Offset: 0x00021160
		[ExcludeFromDocs]
		public bool Play(string animation)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			return this.Play(animation, mode);
		}

		// Token: 0x06001E7A RID: 7802
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CrossFade(string animation, [DefaultValue("0.3F")] float fadeLength, [DefaultValue("PlayMode.StopSameLayer")] PlayMode mode);

		// Token: 0x06001E7B RID: 7803 RVA: 0x00022F80 File Offset: 0x00021180
		[ExcludeFromDocs]
		public void CrossFade(string animation, float fadeLength)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			this.CrossFade(animation, fadeLength, mode);
		}

		// Token: 0x06001E7C RID: 7804 RVA: 0x00022F9C File Offset: 0x0002119C
		[ExcludeFromDocs]
		public void CrossFade(string animation)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			float fadeLength = 0.3f;
			this.CrossFade(animation, fadeLength, mode);
		}

		// Token: 0x06001E7D RID: 7805
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Blend(string animation, [DefaultValue("1.0F")] float targetWeight, [DefaultValue("0.3F")] float fadeLength);

		// Token: 0x06001E7E RID: 7806 RVA: 0x00022FBC File Offset: 0x000211BC
		[ExcludeFromDocs]
		public void Blend(string animation, float targetWeight)
		{
			float fadeLength = 0.3f;
			this.Blend(animation, targetWeight, fadeLength);
		}

		// Token: 0x06001E7F RID: 7807 RVA: 0x00022FDC File Offset: 0x000211DC
		[ExcludeFromDocs]
		public void Blend(string animation)
		{
			float fadeLength = 0.3f;
			float targetWeight = 1f;
			this.Blend(animation, targetWeight, fadeLength);
		}

		// Token: 0x06001E80 RID: 7808
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimationState CrossFadeQueued(string animation, [DefaultValue("0.3F")] float fadeLength, [DefaultValue("QueueMode.CompleteOthers")] QueueMode queue, [DefaultValue("PlayMode.StopSameLayer")] PlayMode mode);

		// Token: 0x06001E81 RID: 7809 RVA: 0x00023000 File Offset: 0x00021200
		[ExcludeFromDocs]
		public AnimationState CrossFadeQueued(string animation, float fadeLength, QueueMode queue)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			return this.CrossFadeQueued(animation, fadeLength, queue, mode);
		}

		// Token: 0x06001E82 RID: 7810 RVA: 0x00023024 File Offset: 0x00021224
		[ExcludeFromDocs]
		public AnimationState CrossFadeQueued(string animation, float fadeLength)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			QueueMode queue = QueueMode.CompleteOthers;
			return this.CrossFadeQueued(animation, fadeLength, queue, mode);
		}

		// Token: 0x06001E83 RID: 7811 RVA: 0x00023048 File Offset: 0x00021248
		[ExcludeFromDocs]
		public AnimationState CrossFadeQueued(string animation)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			QueueMode queue = QueueMode.CompleteOthers;
			float fadeLength = 0.3f;
			return this.CrossFadeQueued(animation, fadeLength, queue, mode);
		}

		// Token: 0x06001E84 RID: 7812
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimationState PlayQueued(string animation, [DefaultValue("QueueMode.CompleteOthers")] QueueMode queue, [DefaultValue("PlayMode.StopSameLayer")] PlayMode mode);

		// Token: 0x06001E85 RID: 7813 RVA: 0x00023074 File Offset: 0x00021274
		[ExcludeFromDocs]
		public AnimationState PlayQueued(string animation, QueueMode queue)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			return this.PlayQueued(animation, queue, mode);
		}

		// Token: 0x06001E86 RID: 7814 RVA: 0x00023094 File Offset: 0x00021294
		[ExcludeFromDocs]
		public AnimationState PlayQueued(string animation)
		{
			PlayMode mode = PlayMode.StopSameLayer;
			QueueMode queue = QueueMode.CompleteOthers;
			return this.PlayQueued(animation, queue, mode);
		}

		// Token: 0x06001E87 RID: 7815 RVA: 0x000230B8 File Offset: 0x000212B8
		public void AddClip(AnimationClip clip, string newName)
		{
			this.AddClip(clip, newName, int.MinValue, int.MaxValue);
		}

		// Token: 0x06001E88 RID: 7816
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddClip(AnimationClip clip, string newName, int firstFrame, int lastFrame, [DefaultValue("false")] bool addLoopFrame);

		// Token: 0x06001E89 RID: 7817 RVA: 0x000230D0 File Offset: 0x000212D0
		[ExcludeFromDocs]
		public void AddClip(AnimationClip clip, string newName, int firstFrame, int lastFrame)
		{
			bool addLoopFrame = false;
			this.AddClip(clip, newName, firstFrame, lastFrame, addLoopFrame);
		}

		// Token: 0x06001E8A RID: 7818
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveClip(AnimationClip clip);

		// Token: 0x06001E8B RID: 7819 RVA: 0x000230EC File Offset: 0x000212EC
		public void RemoveClip(string clipName)
		{
			this.RemoveClip2(clipName);
		}

		// Token: 0x06001E8C RID: 7820
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetClipCount();

		// Token: 0x06001E8D RID: 7821
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void RemoveClip2(string clipName);

		// Token: 0x06001E8E RID: 7822
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool PlayDefaultAnimation(PlayMode mode);

		// Token: 0x06001E8F RID: 7823 RVA: 0x000230F8 File Offset: 0x000212F8
		[Obsolete("use PlayMode instead of AnimationPlayMode.")]
		public bool Play(AnimationPlayMode mode)
		{
			return this.PlayDefaultAnimation((PlayMode)mode);
		}

		// Token: 0x06001E90 RID: 7824 RVA: 0x00023114 File Offset: 0x00021314
		[Obsolete("use PlayMode instead of AnimationPlayMode.")]
		public bool Play(string animation, AnimationPlayMode mode)
		{
			return this.Play(animation, (PlayMode)mode);
		}

		// Token: 0x06001E91 RID: 7825 RVA: 0x00023134 File Offset: 0x00021334
		public void SyncLayer(int layer)
		{
			Animation.INTERNAL_CALL_SyncLayer(this, layer);
		}

		// Token: 0x06001E92 RID: 7826
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SyncLayer(Animation self, int layer);

		// Token: 0x06001E93 RID: 7827 RVA: 0x00023140 File Offset: 0x00021340
		public IEnumerator GetEnumerator()
		{
			return new Animation.Enumerator(this);
		}

		// Token: 0x06001E94 RID: 7828
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern AnimationState GetState(string name);

		// Token: 0x06001E95 RID: 7829
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern AnimationState GetStateAtIndex(int index);

		// Token: 0x06001E96 RID: 7830
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetStateCount();

		// Token: 0x06001E97 RID: 7831 RVA: 0x0002315C File Offset: 0x0002135C
		public AnimationClip GetClip(string name)
		{
			AnimationState state = this.GetState(name);
			AnimationClip result;
			if (state)
			{
				result = state.clip;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x1700079B RID: 1947
		// (get) Token: 0x06001E98 RID: 7832
		// (set) Token: 0x06001E99 RID: 7833
		public extern bool animatePhysics { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700079C RID: 1948
		// (get) Token: 0x06001E9A RID: 7834
		// (set) Token: 0x06001E9B RID: 7835
		[Obsolete("Use cullingType instead")]
		public extern bool animateOnlyIfVisible { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700079D RID: 1949
		// (get) Token: 0x06001E9C RID: 7836
		// (set) Token: 0x06001E9D RID: 7837
		public extern AnimationCullingType cullingType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700079E RID: 1950
		// (get) Token: 0x06001E9E RID: 7838 RVA: 0x00023194 File Offset: 0x00021394
		// (set) Token: 0x06001E9F RID: 7839 RVA: 0x000231B4 File Offset: 0x000213B4
		public Bounds localBounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_localBounds(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localBounds(ref value);
			}
		}

		// Token: 0x06001EA0 RID: 7840
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localBounds(out Bounds value);

		// Token: 0x06001EA1 RID: 7841
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localBounds(ref Bounds value);

		// Token: 0x020001C3 RID: 451
		private sealed class Enumerator : IEnumerator
		{
			// Token: 0x06001EA2 RID: 7842 RVA: 0x000231C0 File Offset: 0x000213C0
			internal Enumerator(Animation outer)
			{
				this.m_Outer = outer;
			}

			// Token: 0x1700079F RID: 1951
			// (get) Token: 0x06001EA3 RID: 7843 RVA: 0x000231D8 File Offset: 0x000213D8
			public object Current
			{
				get
				{
					return this.m_Outer.GetStateAtIndex(this.m_CurrentIndex);
				}
			}

			// Token: 0x06001EA4 RID: 7844 RVA: 0x00023200 File Offset: 0x00021400
			public bool MoveNext()
			{
				int stateCount = this.m_Outer.GetStateCount();
				this.m_CurrentIndex++;
				return this.m_CurrentIndex < stateCount;
			}

			// Token: 0x06001EA5 RID: 7845 RVA: 0x00023238 File Offset: 0x00021438
			public void Reset()
			{
				this.m_CurrentIndex = -1;
			}

			// Token: 0x040004CC RID: 1228
			private Animation m_Outer;

			// Token: 0x040004CD RID: 1229
			private int m_CurrentIndex = -1;
		}
	}
}
