﻿using System;

namespace UnityEngine
{
	// Token: 0x020001D0 RID: 464
	public struct MatchTargetWeightMask
	{
		// Token: 0x06001ED7 RID: 7895 RVA: 0x0002354C File Offset: 0x0002174C
		public MatchTargetWeightMask(Vector3 positionXYZWeight, float rotationWeight)
		{
			this.m_PositionXYZWeight = positionXYZWeight;
			this.m_RotationWeight = rotationWeight;
		}

		// Token: 0x170007BE RID: 1982
		// (get) Token: 0x06001ED8 RID: 7896 RVA: 0x00023560 File Offset: 0x00021760
		// (set) Token: 0x06001ED9 RID: 7897 RVA: 0x0002357C File Offset: 0x0002177C
		public Vector3 positionXYZWeight
		{
			get
			{
				return this.m_PositionXYZWeight;
			}
			set
			{
				this.m_PositionXYZWeight = value;
			}
		}

		// Token: 0x170007BF RID: 1983
		// (get) Token: 0x06001EDA RID: 7898 RVA: 0x00023588 File Offset: 0x00021788
		// (set) Token: 0x06001EDB RID: 7899 RVA: 0x000235A4 File Offset: 0x000217A4
		public float rotationWeight
		{
			get
			{
				return this.m_RotationWeight;
			}
			set
			{
				this.m_RotationWeight = value;
			}
		}

		// Token: 0x04000505 RID: 1285
		private Vector3 m_PositionXYZWeight;

		// Token: 0x04000506 RID: 1286
		private float m_RotationWeight;
	}
}
