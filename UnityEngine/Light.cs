﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x02000079 RID: 121
	public sealed class Light : Behaviour
	{
		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x060007FE RID: 2046
		// (set) Token: 0x060007FF RID: 2047
		public extern LightType type { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06000800 RID: 2048 RVA: 0x0000A144 File Offset: 0x00008344
		// (set) Token: 0x06000801 RID: 2049 RVA: 0x0000A164 File Offset: 0x00008364
		public Color color
		{
			get
			{
				Color result;
				this.INTERNAL_get_color(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_color(ref value);
			}
		}

		// Token: 0x06000802 RID: 2050
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_color(out Color value);

		// Token: 0x06000803 RID: 2051
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_color(ref Color value);

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06000804 RID: 2052
		// (set) Token: 0x06000805 RID: 2053
		public extern float intensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x06000806 RID: 2054
		// (set) Token: 0x06000807 RID: 2055
		public extern float bounceIntensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x06000808 RID: 2056
		// (set) Token: 0x06000809 RID: 2057
		public extern LightShadows shadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EB RID: 491
		// (get) Token: 0x0600080A RID: 2058
		// (set) Token: 0x0600080B RID: 2059
		public extern float shadowStrength { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EC RID: 492
		// (get) Token: 0x0600080C RID: 2060
		// (set) Token: 0x0600080D RID: 2061
		public extern LightShadowResolution shadowResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001ED RID: 493
		// (get) Token: 0x0600080E RID: 2062
		// (set) Token: 0x0600080F RID: 2063
		public extern int shadowCustomResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EE RID: 494
		// (get) Token: 0x06000810 RID: 2064
		// (set) Token: 0x06000811 RID: 2065
		public extern float shadowBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001EF RID: 495
		// (get) Token: 0x06000812 RID: 2066
		// (set) Token: 0x06000813 RID: 2067
		public extern float shadowNormalBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x06000814 RID: 2068
		// (set) Token: 0x06000815 RID: 2069
		public extern float shadowNearPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x06000816 RID: 2070
		// (set) Token: 0x06000817 RID: 2071
		[Obsolete("Shadow softness is removed in Unity 5.0+")]
		public extern float shadowSoftness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x06000818 RID: 2072
		// (set) Token: 0x06000819 RID: 2073
		[Obsolete("Shadow softness is removed in Unity 5.0+")]
		public extern float shadowSoftnessFade { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x0600081A RID: 2074
		// (set) Token: 0x0600081B RID: 2075
		public extern float range { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x0600081C RID: 2076
		// (set) Token: 0x0600081D RID: 2077
		public extern float spotAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x0600081E RID: 2078
		// (set) Token: 0x0600081F RID: 2079
		public extern float cookieSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x06000820 RID: 2080
		// (set) Token: 0x06000821 RID: 2081
		public extern Texture cookie { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x06000822 RID: 2082
		// (set) Token: 0x06000823 RID: 2083
		public extern Flare flare { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x06000824 RID: 2084
		// (set) Token: 0x06000825 RID: 2085
		public extern LightRenderMode renderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x06000826 RID: 2086
		// (set) Token: 0x06000827 RID: 2087
		[Obsolete("Use Light.bakedIndex or Light.isBaked instead.")]
		public extern bool alreadyLightmapped { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x06000828 RID: 2088
		// (set) Token: 0x06000829 RID: 2089
		public extern int bakedIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x0600082A RID: 2090
		public extern bool isBaked { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001FC RID: 508
		// (get) Token: 0x0600082B RID: 2091
		// (set) Token: 0x0600082C RID: 2092
		public extern int cullingMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600082D RID: 2093
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddCommandBuffer(LightEvent evt, CommandBuffer buffer);

		// Token: 0x0600082E RID: 2094
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveCommandBuffer(LightEvent evt, CommandBuffer buffer);

		// Token: 0x0600082F RID: 2095
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveCommandBuffers(LightEvent evt);

		// Token: 0x06000830 RID: 2096
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveAllCommandBuffers();

		// Token: 0x06000831 RID: 2097
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern CommandBuffer[] GetCommandBuffers(LightEvent evt);

		// Token: 0x170001FD RID: 509
		// (get) Token: 0x06000832 RID: 2098
		public extern int commandBufferCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x06000833 RID: 2099
		// (set) Token: 0x06000834 RID: 2100
		public static extern int pixelLightCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000835 RID: 2101
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Light[] GetLights(LightType type, int layer);

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x06000836 RID: 2102 RVA: 0x0000A170 File Offset: 0x00008370
		// (set) Token: 0x06000837 RID: 2103 RVA: 0x0000A18C File Offset: 0x0000838C
		[Obsolete("light.shadowConstantBias was removed, use light.shadowBias", true)]
		public float shadowConstantBias
		{
			get
			{
				return 0f;
			}
			set
			{
			}
		}

		// Token: 0x17000200 RID: 512
		// (get) Token: 0x06000838 RID: 2104 RVA: 0x0000A190 File Offset: 0x00008390
		// (set) Token: 0x06000839 RID: 2105 RVA: 0x0000A1AC File Offset: 0x000083AC
		[Obsolete("light.shadowObjectSizeBias was removed, use light.shadowBias", true)]
		public float shadowObjectSizeBias
		{
			get
			{
				return 0f;
			}
			set
			{
			}
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x0600083A RID: 2106 RVA: 0x0000A1B0 File Offset: 0x000083B0
		// (set) Token: 0x0600083B RID: 2107 RVA: 0x0000A1C8 File Offset: 0x000083C8
		[Obsolete("light.attenuate was removed; all lights always attenuate now", true)]
		public bool attenuate
		{
			get
			{
				return true;
			}
			set
			{
			}
		}
	}
}
