﻿using System;

namespace UnityEngine
{
	// Token: 0x02000165 RID: 357
	public enum CapsuleDirection2D
	{
		// Token: 0x040003E9 RID: 1001
		Vertical,
		// Token: 0x040003EA RID: 1002
		Horizontal
	}
}
