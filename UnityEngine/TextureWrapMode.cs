﻿using System;

namespace UnityEngine
{
	// Token: 0x020002FB RID: 763
	public enum TextureWrapMode
	{
		// Token: 0x04000B56 RID: 2902
		Repeat,
		// Token: 0x04000B57 RID: 2903
		Clamp
	}
}
