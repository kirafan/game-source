﻿using System;

namespace UnityEngine
{
	// Token: 0x02000032 RID: 50
	public enum CursorLockMode
	{
		// Token: 0x04000051 RID: 81
		None,
		// Token: 0x04000052 RID: 82
		Locked,
		// Token: 0x04000053 RID: 83
		Confined
	}
}
