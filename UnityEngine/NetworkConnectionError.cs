﻿using System;

namespace UnityEngine
{
	// Token: 0x02000091 RID: 145
	public enum NetworkConnectionError
	{
		// Token: 0x0400013E RID: 318
		NoError,
		// Token: 0x0400013F RID: 319
		RSAPublicKeyMismatch = 21,
		// Token: 0x04000140 RID: 320
		InvalidPassword = 23,
		// Token: 0x04000141 RID: 321
		ConnectionFailed = 15,
		// Token: 0x04000142 RID: 322
		TooManyConnectedPlayers = 18,
		// Token: 0x04000143 RID: 323
		ConnectionBanned = 22,
		// Token: 0x04000144 RID: 324
		AlreadyConnectedToServer = 16,
		// Token: 0x04000145 RID: 325
		AlreadyConnectedToAnotherServer = -1,
		// Token: 0x04000146 RID: 326
		CreateSocketOrThreadFailure = -2,
		// Token: 0x04000147 RID: 327
		IncorrectParameters = -3,
		// Token: 0x04000148 RID: 328
		EmptyConnectTarget = -4,
		// Token: 0x04000149 RID: 329
		InternalDirectConnectFailed = -5,
		// Token: 0x0400014A RID: 330
		NATTargetNotConnected = 69,
		// Token: 0x0400014B RID: 331
		NATTargetConnectionLost = 71,
		// Token: 0x0400014C RID: 332
		NATPunchthroughFailed = 73
	}
}
