﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035A RID: 858
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = true)]
	public class SpaceAttribute : PropertyAttribute
	{
		// Token: 0x06002DDB RID: 11739 RVA: 0x00048E48 File Offset: 0x00047048
		public SpaceAttribute()
		{
			this.height = 8f;
		}

		// Token: 0x06002DDC RID: 11740 RVA: 0x00048E5C File Offset: 0x0004705C
		public SpaceAttribute(float height)
		{
			this.height = height;
		}

		// Token: 0x04000D29 RID: 3369
		public readonly float height;
	}
}
