﻿using System;

namespace UnityEngine
{
	// Token: 0x020001FE RID: 510
	public enum TextAlignment
	{
		// Token: 0x04000612 RID: 1554
		Left,
		// Token: 0x04000613 RID: 1555
		Center,
		// Token: 0x04000614 RID: 1556
		Right
	}
}
