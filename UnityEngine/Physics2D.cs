﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000158 RID: 344
	public class Physics2D
	{
		// Token: 0x170005D8 RID: 1496
		// (get) Token: 0x060018EE RID: 6382
		// (set) Token: 0x060018EF RID: 6383
		[ThreadAndSerializationSafe]
		public static extern int velocityIterations { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005D9 RID: 1497
		// (get) Token: 0x060018F0 RID: 6384
		// (set) Token: 0x060018F1 RID: 6385
		public static extern int positionIterations { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005DA RID: 1498
		// (get) Token: 0x060018F2 RID: 6386 RVA: 0x0001F0B0 File Offset: 0x0001D2B0
		// (set) Token: 0x060018F3 RID: 6387 RVA: 0x0001F0D0 File Offset: 0x0001D2D0
		public static Vector2 gravity
		{
			get
			{
				Vector2 result;
				Physics2D.INTERNAL_get_gravity(out result);
				return result;
			}
			set
			{
				Physics2D.INTERNAL_set_gravity(ref value);
			}
		}

		// Token: 0x060018F4 RID: 6388
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_gravity(out Vector2 value);

		// Token: 0x060018F5 RID: 6389
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_gravity(ref Vector2 value);

		// Token: 0x170005DB RID: 1499
		// (get) Token: 0x060018F6 RID: 6390
		// (set) Token: 0x060018F7 RID: 6391
		public static extern bool queriesHitTriggers { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005DC RID: 1500
		// (get) Token: 0x060018F8 RID: 6392
		// (set) Token: 0x060018F9 RID: 6393
		public static extern bool queriesStartInColliders { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005DD RID: 1501
		// (get) Token: 0x060018FA RID: 6394
		// (set) Token: 0x060018FB RID: 6395
		public static extern bool changeStopsCallbacks { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005DE RID: 1502
		// (get) Token: 0x060018FC RID: 6396
		// (set) Token: 0x060018FD RID: 6397
		public static extern float velocityThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005DF RID: 1503
		// (get) Token: 0x060018FE RID: 6398
		// (set) Token: 0x060018FF RID: 6399
		public static extern float maxLinearCorrection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E0 RID: 1504
		// (get) Token: 0x06001900 RID: 6400
		// (set) Token: 0x06001901 RID: 6401
		public static extern float maxAngularCorrection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E1 RID: 1505
		// (get) Token: 0x06001902 RID: 6402
		// (set) Token: 0x06001903 RID: 6403
		public static extern float maxTranslationSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E2 RID: 1506
		// (get) Token: 0x06001904 RID: 6404
		// (set) Token: 0x06001905 RID: 6405
		public static extern float maxRotationSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E3 RID: 1507
		// (get) Token: 0x06001906 RID: 6406
		// (set) Token: 0x06001907 RID: 6407
		public static extern float minPenetrationForPenalty { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E4 RID: 1508
		// (get) Token: 0x06001908 RID: 6408
		// (set) Token: 0x06001909 RID: 6409
		public static extern float baumgarteScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E5 RID: 1509
		// (get) Token: 0x0600190A RID: 6410
		// (set) Token: 0x0600190B RID: 6411
		public static extern float baumgarteTOIScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E6 RID: 1510
		// (get) Token: 0x0600190C RID: 6412
		// (set) Token: 0x0600190D RID: 6413
		public static extern float timeToSleep { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E7 RID: 1511
		// (get) Token: 0x0600190E RID: 6414
		// (set) Token: 0x0600190F RID: 6415
		public static extern float linearSleepTolerance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E8 RID: 1512
		// (get) Token: 0x06001910 RID: 6416
		// (set) Token: 0x06001911 RID: 6417
		public static extern float angularSleepTolerance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005E9 RID: 1513
		// (get) Token: 0x06001912 RID: 6418
		// (set) Token: 0x06001913 RID: 6419
		public static extern bool alwaysShowColliders { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005EA RID: 1514
		// (get) Token: 0x06001914 RID: 6420
		// (set) Token: 0x06001915 RID: 6421
		public static extern bool showColliderSleep { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005EB RID: 1515
		// (get) Token: 0x06001916 RID: 6422
		// (set) Token: 0x06001917 RID: 6423
		public static extern bool showColliderContacts { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005EC RID: 1516
		// (get) Token: 0x06001918 RID: 6424
		// (set) Token: 0x06001919 RID: 6425
		public static extern bool showColliderAABB { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005ED RID: 1517
		// (get) Token: 0x0600191A RID: 6426
		// (set) Token: 0x0600191B RID: 6427
		public static extern float contactArrowScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005EE RID: 1518
		// (get) Token: 0x0600191C RID: 6428 RVA: 0x0001F0DC File Offset: 0x0001D2DC
		// (set) Token: 0x0600191D RID: 6429 RVA: 0x0001F0FC File Offset: 0x0001D2FC
		public static Color colliderAwakeColor
		{
			get
			{
				Color result;
				Physics2D.INTERNAL_get_colliderAwakeColor(out result);
				return result;
			}
			set
			{
				Physics2D.INTERNAL_set_colliderAwakeColor(ref value);
			}
		}

		// Token: 0x0600191E RID: 6430
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_colliderAwakeColor(out Color value);

		// Token: 0x0600191F RID: 6431
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_colliderAwakeColor(ref Color value);

		// Token: 0x170005EF RID: 1519
		// (get) Token: 0x06001920 RID: 6432 RVA: 0x0001F108 File Offset: 0x0001D308
		// (set) Token: 0x06001921 RID: 6433 RVA: 0x0001F128 File Offset: 0x0001D328
		public static Color colliderAsleepColor
		{
			get
			{
				Color result;
				Physics2D.INTERNAL_get_colliderAsleepColor(out result);
				return result;
			}
			set
			{
				Physics2D.INTERNAL_set_colliderAsleepColor(ref value);
			}
		}

		// Token: 0x06001922 RID: 6434
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_colliderAsleepColor(out Color value);

		// Token: 0x06001923 RID: 6435
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_colliderAsleepColor(ref Color value);

		// Token: 0x170005F0 RID: 1520
		// (get) Token: 0x06001924 RID: 6436 RVA: 0x0001F134 File Offset: 0x0001D334
		// (set) Token: 0x06001925 RID: 6437 RVA: 0x0001F154 File Offset: 0x0001D354
		public static Color colliderContactColor
		{
			get
			{
				Color result;
				Physics2D.INTERNAL_get_colliderContactColor(out result);
				return result;
			}
			set
			{
				Physics2D.INTERNAL_set_colliderContactColor(ref value);
			}
		}

		// Token: 0x06001926 RID: 6438
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_colliderContactColor(out Color value);

		// Token: 0x06001927 RID: 6439
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_colliderContactColor(ref Color value);

		// Token: 0x170005F1 RID: 1521
		// (get) Token: 0x06001928 RID: 6440 RVA: 0x0001F160 File Offset: 0x0001D360
		// (set) Token: 0x06001929 RID: 6441 RVA: 0x0001F180 File Offset: 0x0001D380
		public static Color colliderAABBColor
		{
			get
			{
				Color result;
				Physics2D.INTERNAL_get_colliderAABBColor(out result);
				return result;
			}
			set
			{
				Physics2D.INTERNAL_set_colliderAABBColor(ref value);
			}
		}

		// Token: 0x0600192A RID: 6442
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_colliderAABBColor(out Color value);

		// Token: 0x0600192B RID: 6443
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_colliderAABBColor(ref Color value);

		// Token: 0x0600192C RID: 6444
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void IgnoreCollision(Collider2D collider1, Collider2D collider2, [DefaultValue("true")] bool ignore);

		// Token: 0x0600192D RID: 6445 RVA: 0x0001F18C File Offset: 0x0001D38C
		[ExcludeFromDocs]
		public static void IgnoreCollision(Collider2D collider1, Collider2D collider2)
		{
			bool ignore = true;
			Physics2D.IgnoreCollision(collider1, collider2, ignore);
		}

		// Token: 0x0600192E RID: 6446
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetIgnoreCollision(Collider2D collider1, Collider2D collider2);

		// Token: 0x0600192F RID: 6447
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void IgnoreLayerCollision(int layer1, int layer2, [DefaultValue("true")] bool ignore);

		// Token: 0x06001930 RID: 6448 RVA: 0x0001F1A4 File Offset: 0x0001D3A4
		[ExcludeFromDocs]
		public static void IgnoreLayerCollision(int layer1, int layer2)
		{
			bool ignore = true;
			Physics2D.IgnoreLayerCollision(layer1, layer2, ignore);
		}

		// Token: 0x06001931 RID: 6449
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetIgnoreLayerCollision(int layer1, int layer2);

		// Token: 0x06001932 RID: 6450
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetLayerCollisionMask(int layer, int layerMask);

		// Token: 0x06001933 RID: 6451
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetLayerCollisionMask(int layer);

		// Token: 0x06001934 RID: 6452
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsTouching(Collider2D collider1, Collider2D collider2);

		// Token: 0x06001935 RID: 6453
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsTouchingLayers(Collider2D collider, [DefaultValue("AllLayers")] int layerMask);

		// Token: 0x06001936 RID: 6454 RVA: 0x0001F1BC File Offset: 0x0001D3BC
		[ExcludeFromDocs]
		public static bool IsTouchingLayers(Collider2D collider)
		{
			int layerMask = -1;
			return Physics2D.IsTouchingLayers(collider, layerMask);
		}

		// Token: 0x06001937 RID: 6455 RVA: 0x0001F1DC File Offset: 0x0001D3DC
		internal static void SetEditorDragMovement(bool dragging, GameObject[] objs)
		{
			foreach (Rigidbody2D rigidbody2D in Physics2D.m_LastDisabledRigidbody2D)
			{
				if (rigidbody2D != null)
				{
					rigidbody2D.SetDragBehaviour(false);
				}
			}
			Physics2D.m_LastDisabledRigidbody2D.Clear();
			if (dragging)
			{
				foreach (GameObject gameObject in objs)
				{
					Rigidbody2D[] componentsInChildren = gameObject.GetComponentsInChildren<Rigidbody2D>(false);
					foreach (Rigidbody2D rigidbody2D2 in componentsInChildren)
					{
						Physics2D.m_LastDisabledRigidbody2D.Add(rigidbody2D2);
						rigidbody2D2.SetDragBehaviour(true);
					}
				}
			}
		}

		// Token: 0x06001938 RID: 6456 RVA: 0x0001F2C4 File Offset: 0x0001D4C4
		private static void Internal_Linecast(Vector2 start, Vector2 end, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit)
		{
			Physics2D.INTERNAL_CALL_Internal_Linecast(ref start, ref end, layerMask, minDepth, maxDepth, out raycastHit);
		}

		// Token: 0x06001939 RID: 6457
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_Linecast(ref Vector2 start, ref Vector2 end, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit);

		// Token: 0x0600193A RID: 6458 RVA: 0x0001F2D8 File Offset: 0x0001D4D8
		[ExcludeFromDocs]
		public static RaycastHit2D Linecast(Vector2 start, Vector2 end, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.Linecast(start, end, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x0600193B RID: 6459 RVA: 0x0001F300 File Offset: 0x0001D500
		[ExcludeFromDocs]
		public static RaycastHit2D Linecast(Vector2 start, Vector2 end, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.Linecast(start, end, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600193C RID: 6460 RVA: 0x0001F32C File Offset: 0x0001D52C
		[ExcludeFromDocs]
		public static RaycastHit2D Linecast(Vector2 start, Vector2 end)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.Linecast(start, end, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600193D RID: 6461 RVA: 0x0001F35C File Offset: 0x0001D55C
		public static RaycastHit2D Linecast(Vector2 start, Vector2 end, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			RaycastHit2D result;
			Physics2D.Internal_Linecast(start, end, layerMask, minDepth, maxDepth, out result);
			return result;
		}

		// Token: 0x0600193E RID: 6462 RVA: 0x0001F380 File Offset: 0x0001D580
		public static RaycastHit2D[] LinecastAll(Vector2 start, Vector2 end, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_LinecastAll(ref start, ref end, layerMask, minDepth, maxDepth);
		}

		// Token: 0x0600193F RID: 6463 RVA: 0x0001F3A4 File Offset: 0x0001D5A4
		[ExcludeFromDocs]
		public static RaycastHit2D[] LinecastAll(Vector2 start, Vector2 end, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_LinecastAll(ref start, ref end, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001940 RID: 6464 RVA: 0x0001F3CC File Offset: 0x0001D5CC
		[ExcludeFromDocs]
		public static RaycastHit2D[] LinecastAll(Vector2 start, Vector2 end, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_LinecastAll(ref start, ref end, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001941 RID: 6465 RVA: 0x0001F3FC File Offset: 0x0001D5FC
		[ExcludeFromDocs]
		public static RaycastHit2D[] LinecastAll(Vector2 start, Vector2 end)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_LinecastAll(ref start, ref end, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001942 RID: 6466
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RaycastHit2D[] INTERNAL_CALL_LinecastAll(ref Vector2 start, ref Vector2 end, int layerMask, float minDepth, float maxDepth);

		// Token: 0x06001943 RID: 6467 RVA: 0x0001F42C File Offset: 0x0001D62C
		public static int LinecastNonAlloc(Vector2 start, Vector2 end, RaycastHit2D[] results, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_LinecastNonAlloc(ref start, ref end, results, layerMask, minDepth, maxDepth);
		}

		// Token: 0x06001944 RID: 6468 RVA: 0x0001F450 File Offset: 0x0001D650
		[ExcludeFromDocs]
		public static int LinecastNonAlloc(Vector2 start, Vector2 end, RaycastHit2D[] results, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_LinecastNonAlloc(ref start, ref end, results, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001945 RID: 6469 RVA: 0x0001F47C File Offset: 0x0001D67C
		[ExcludeFromDocs]
		public static int LinecastNonAlloc(Vector2 start, Vector2 end, RaycastHit2D[] results, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_LinecastNonAlloc(ref start, ref end, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001946 RID: 6470 RVA: 0x0001F4AC File Offset: 0x0001D6AC
		[ExcludeFromDocs]
		public static int LinecastNonAlloc(Vector2 start, Vector2 end, RaycastHit2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_LinecastNonAlloc(ref start, ref end, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001947 RID: 6471
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_LinecastNonAlloc(ref Vector2 start, ref Vector2 end, RaycastHit2D[] results, int layerMask, float minDepth, float maxDepth);

		// Token: 0x06001948 RID: 6472 RVA: 0x0001F4E0 File Offset: 0x0001D6E0
		private static void Internal_Raycast(Vector2 origin, Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit)
		{
			Physics2D.INTERNAL_CALL_Internal_Raycast(ref origin, ref direction, distance, layerMask, minDepth, maxDepth, out raycastHit);
		}

		// Token: 0x06001949 RID: 6473
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_Raycast(ref Vector2 origin, ref Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit);

		// Token: 0x0600194A RID: 6474 RVA: 0x0001F4F4 File Offset: 0x0001D6F4
		[RequiredByNativeCode]
		[ExcludeFromDocs]
		public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.Raycast(origin, direction, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x0600194B RID: 6475 RVA: 0x0001F51C File Offset: 0x0001D71C
		[ExcludeFromDocs]
		public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.Raycast(origin, direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600194C RID: 6476 RVA: 0x0001F548 File Offset: 0x0001D748
		[ExcludeFromDocs]
		public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.Raycast(origin, direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600194D RID: 6477 RVA: 0x0001F578 File Offset: 0x0001D778
		[ExcludeFromDocs]
		public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.Raycast(origin, direction, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600194E RID: 6478 RVA: 0x0001F5B0 File Offset: 0x0001D7B0
		public static RaycastHit2D Raycast(Vector2 origin, Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			RaycastHit2D result;
			Physics2D.Internal_Raycast(origin, direction, distance, layerMask, minDepth, maxDepth, out result);
			return result;
		}

		// Token: 0x0600194F RID: 6479 RVA: 0x0001F5D8 File Offset: 0x0001D7D8
		public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_RaycastAll(ref origin, ref direction, distance, layerMask, minDepth, maxDepth);
		}

		// Token: 0x06001950 RID: 6480 RVA: 0x0001F5FC File Offset: 0x0001D7FC
		[ExcludeFromDocs]
		public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_RaycastAll(ref origin, ref direction, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001951 RID: 6481 RVA: 0x0001F628 File Offset: 0x0001D828
		[ExcludeFromDocs]
		public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_RaycastAll(ref origin, ref direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001952 RID: 6482 RVA: 0x0001F658 File Offset: 0x0001D858
		[ExcludeFromDocs]
		public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_RaycastAll(ref origin, ref direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001953 RID: 6483 RVA: 0x0001F68C File Offset: 0x0001D88C
		[ExcludeFromDocs]
		public static RaycastHit2D[] RaycastAll(Vector2 origin, Vector2 direction)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_RaycastAll(ref origin, ref direction, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001954 RID: 6484
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RaycastHit2D[] INTERNAL_CALL_RaycastAll(ref Vector2 origin, ref Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth);

		// Token: 0x06001955 RID: 6485 RVA: 0x0001F6C8 File Offset: 0x0001D8C8
		public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_RaycastNonAlloc(ref origin, ref direction, results, distance, layerMask, minDepth, maxDepth);
		}

		// Token: 0x06001956 RID: 6486 RVA: 0x0001F6F0 File Offset: 0x0001D8F0
		[ExcludeFromDocs]
		public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_RaycastNonAlloc(ref origin, ref direction, results, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001957 RID: 6487 RVA: 0x0001F71C File Offset: 0x0001D91C
		[ExcludeFromDocs]
		public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_RaycastNonAlloc(ref origin, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001958 RID: 6488 RVA: 0x0001F74C File Offset: 0x0001D94C
		[ExcludeFromDocs]
		public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_RaycastNonAlloc(ref origin, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001959 RID: 6489 RVA: 0x0001F780 File Offset: 0x0001D980
		[ExcludeFromDocs]
		public static int RaycastNonAlloc(Vector2 origin, Vector2 direction, RaycastHit2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_RaycastNonAlloc(ref origin, ref direction, results, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600195A RID: 6490
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_RaycastNonAlloc(ref Vector2 origin, ref Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth, float maxDepth);

		// Token: 0x0600195B RID: 6491 RVA: 0x0001F7BC File Offset: 0x0001D9BC
		private static void Internal_CircleCast(Vector2 origin, float radius, Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit)
		{
			Physics2D.INTERNAL_CALL_Internal_CircleCast(ref origin, radius, ref direction, distance, layerMask, minDepth, maxDepth, out raycastHit);
		}

		// Token: 0x0600195C RID: 6492
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_CircleCast(ref Vector2 origin, float radius, ref Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit);

		// Token: 0x0600195D RID: 6493 RVA: 0x0001F7D4 File Offset: 0x0001D9D4
		[ExcludeFromDocs]
		public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.CircleCast(origin, radius, direction, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x0600195E RID: 6494 RVA: 0x0001F800 File Offset: 0x0001DA00
		[ExcludeFromDocs]
		public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.CircleCast(origin, radius, direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600195F RID: 6495 RVA: 0x0001F830 File Offset: 0x0001DA30
		[ExcludeFromDocs]
		public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.CircleCast(origin, radius, direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001960 RID: 6496 RVA: 0x0001F860 File Offset: 0x0001DA60
		[ExcludeFromDocs]
		public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.CircleCast(origin, radius, direction, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001961 RID: 6497 RVA: 0x0001F898 File Offset: 0x0001DA98
		public static RaycastHit2D CircleCast(Vector2 origin, float radius, Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			RaycastHit2D result;
			Physics2D.Internal_CircleCast(origin, radius, direction, distance, layerMask, minDepth, maxDepth, out result);
			return result;
		}

		// Token: 0x06001962 RID: 6498 RVA: 0x0001F8C0 File Offset: 0x0001DAC0
		public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_CircleCastAll(ref origin, radius, ref direction, distance, layerMask, minDepth, maxDepth);
		}

		// Token: 0x06001963 RID: 6499 RVA: 0x0001F8E8 File Offset: 0x0001DAE8
		[ExcludeFromDocs]
		public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_CircleCastAll(ref origin, radius, ref direction, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001964 RID: 6500 RVA: 0x0001F914 File Offset: 0x0001DB14
		[ExcludeFromDocs]
		public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_CircleCastAll(ref origin, radius, ref direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001965 RID: 6501 RVA: 0x0001F944 File Offset: 0x0001DB44
		[ExcludeFromDocs]
		public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_CircleCastAll(ref origin, radius, ref direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001966 RID: 6502 RVA: 0x0001F978 File Offset: 0x0001DB78
		[ExcludeFromDocs]
		public static RaycastHit2D[] CircleCastAll(Vector2 origin, float radius, Vector2 direction)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_CircleCastAll(ref origin, radius, ref direction, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001967 RID: 6503
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RaycastHit2D[] INTERNAL_CALL_CircleCastAll(ref Vector2 origin, float radius, ref Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth);

		// Token: 0x06001968 RID: 6504 RVA: 0x0001F9B4 File Offset: 0x0001DBB4
		public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_CircleCastNonAlloc(ref origin, radius, ref direction, results, distance, layerMask, minDepth, maxDepth);
		}

		// Token: 0x06001969 RID: 6505 RVA: 0x0001F9DC File Offset: 0x0001DBDC
		[ExcludeFromDocs]
		public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_CircleCastNonAlloc(ref origin, radius, ref direction, results, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x0600196A RID: 6506 RVA: 0x0001FA0C File Offset: 0x0001DC0C
		[ExcludeFromDocs]
		public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction, RaycastHit2D[] results, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_CircleCastNonAlloc(ref origin, radius, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600196B RID: 6507 RVA: 0x0001FA40 File Offset: 0x0001DC40
		[ExcludeFromDocs]
		public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction, RaycastHit2D[] results, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_CircleCastNonAlloc(ref origin, radius, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600196C RID: 6508 RVA: 0x0001FA74 File Offset: 0x0001DC74
		[ExcludeFromDocs]
		public static int CircleCastNonAlloc(Vector2 origin, float radius, Vector2 direction, RaycastHit2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_CircleCastNonAlloc(ref origin, radius, ref direction, results, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600196D RID: 6509
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_CircleCastNonAlloc(ref Vector2 origin, float radius, ref Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth, float maxDepth);

		// Token: 0x0600196E RID: 6510 RVA: 0x0001FAB0 File Offset: 0x0001DCB0
		private static void Internal_BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit)
		{
			Physics2D.INTERNAL_CALL_Internal_BoxCast(ref origin, ref size, angle, ref direction, distance, layerMask, minDepth, maxDepth, out raycastHit);
		}

		// Token: 0x0600196F RID: 6511
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_BoxCast(ref Vector2 origin, ref Vector2 size, float angle, ref Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit);

		// Token: 0x06001970 RID: 6512 RVA: 0x0001FAD4 File Offset: 0x0001DCD4
		[ExcludeFromDocs]
		public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 direction, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.BoxCast(origin, size, angle, direction, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001971 RID: 6513 RVA: 0x0001FB00 File Offset: 0x0001DD00
		[ExcludeFromDocs]
		public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 direction, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.BoxCast(origin, size, angle, direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001972 RID: 6514 RVA: 0x0001FB30 File Offset: 0x0001DD30
		[ExcludeFromDocs]
		public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 direction, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.BoxCast(origin, size, angle, direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001973 RID: 6515 RVA: 0x0001FB64 File Offset: 0x0001DD64
		[ExcludeFromDocs]
		public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 direction)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.BoxCast(origin, size, angle, direction, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001974 RID: 6516 RVA: 0x0001FBA0 File Offset: 0x0001DDA0
		public static RaycastHit2D BoxCast(Vector2 origin, Vector2 size, float angle, Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			RaycastHit2D result;
			Physics2D.Internal_BoxCast(origin, size, angle, direction, distance, layerMask, minDepth, maxDepth, out result);
			return result;
		}

		// Token: 0x06001975 RID: 6517 RVA: 0x0001FBCC File Offset: 0x0001DDCC
		public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle, Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_BoxCastAll(ref origin, ref size, angle, ref direction, distance, layerMask, minDepth, maxDepth);
		}

		// Token: 0x06001976 RID: 6518 RVA: 0x0001FBF8 File Offset: 0x0001DDF8
		[ExcludeFromDocs]
		public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle, Vector2 direction, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_BoxCastAll(ref origin, ref size, angle, ref direction, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001977 RID: 6519 RVA: 0x0001FC28 File Offset: 0x0001DE28
		[ExcludeFromDocs]
		public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle, Vector2 direction, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_BoxCastAll(ref origin, ref size, angle, ref direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001978 RID: 6520 RVA: 0x0001FC5C File Offset: 0x0001DE5C
		[ExcludeFromDocs]
		public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle, Vector2 direction, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_BoxCastAll(ref origin, ref size, angle, ref direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001979 RID: 6521 RVA: 0x0001FC94 File Offset: 0x0001DE94
		[ExcludeFromDocs]
		public static RaycastHit2D[] BoxCastAll(Vector2 origin, Vector2 size, float angle, Vector2 direction)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_BoxCastAll(ref origin, ref size, angle, ref direction, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600197A RID: 6522
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RaycastHit2D[] INTERNAL_CALL_BoxCastAll(ref Vector2 origin, ref Vector2 size, float angle, ref Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth);

		// Token: 0x0600197B RID: 6523 RVA: 0x0001FCD0 File Offset: 0x0001DED0
		public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle, Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_BoxCastNonAlloc(ref origin, ref size, angle, ref direction, results, distance, layerMask, minDepth, maxDepth);
		}

		// Token: 0x0600197C RID: 6524 RVA: 0x0001FCFC File Offset: 0x0001DEFC
		[ExcludeFromDocs]
		public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle, Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_BoxCastNonAlloc(ref origin, ref size, angle, ref direction, results, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x0600197D RID: 6525 RVA: 0x0001FD2C File Offset: 0x0001DF2C
		[ExcludeFromDocs]
		public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle, Vector2 direction, RaycastHit2D[] results, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_BoxCastNonAlloc(ref origin, ref size, angle, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600197E RID: 6526 RVA: 0x0001FD64 File Offset: 0x0001DF64
		[ExcludeFromDocs]
		public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle, Vector2 direction, RaycastHit2D[] results, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_BoxCastNonAlloc(ref origin, ref size, angle, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600197F RID: 6527 RVA: 0x0001FD9C File Offset: 0x0001DF9C
		[ExcludeFromDocs]
		public static int BoxCastNonAlloc(Vector2 origin, Vector2 size, float angle, Vector2 direction, RaycastHit2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_BoxCastNonAlloc(ref origin, ref size, angle, ref direction, results, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001980 RID: 6528
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_BoxCastNonAlloc(ref Vector2 origin, ref Vector2 size, float angle, ref Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth, float maxDepth);

		// Token: 0x06001981 RID: 6529 RVA: 0x0001FDDC File Offset: 0x0001DFDC
		private static void Internal_CapsuleCast(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit)
		{
			Physics2D.INTERNAL_CALL_Internal_CapsuleCast(ref origin, ref size, capsuleDirection, angle, ref direction, distance, layerMask, minDepth, maxDepth, out raycastHit);
		}

		// Token: 0x06001982 RID: 6530
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_CapsuleCast(ref Vector2 origin, ref Vector2 size, CapsuleDirection2D capsuleDirection, float angle, ref Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth, out RaycastHit2D raycastHit);

		// Token: 0x06001983 RID: 6531 RVA: 0x0001FE04 File Offset: 0x0001E004
		[ExcludeFromDocs]
		public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.CapsuleCast(origin, size, capsuleDirection, angle, direction, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001984 RID: 6532 RVA: 0x0001FE34 File Offset: 0x0001E034
		[ExcludeFromDocs]
		public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.CapsuleCast(origin, size, capsuleDirection, angle, direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001985 RID: 6533 RVA: 0x0001FE68 File Offset: 0x0001E068
		[ExcludeFromDocs]
		public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.CapsuleCast(origin, size, capsuleDirection, angle, direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001986 RID: 6534 RVA: 0x0001FE9C File Offset: 0x0001E09C
		[ExcludeFromDocs]
		public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.CapsuleCast(origin, size, capsuleDirection, angle, direction, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001987 RID: 6535 RVA: 0x0001FED8 File Offset: 0x0001E0D8
		public static RaycastHit2D CapsuleCast(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			RaycastHit2D result;
			Physics2D.Internal_CapsuleCast(origin, size, capsuleDirection, angle, direction, distance, layerMask, minDepth, maxDepth, out result);
			return result;
		}

		// Token: 0x06001988 RID: 6536 RVA: 0x0001FF04 File Offset: 0x0001E104
		public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_CapsuleCastAll(ref origin, ref size, capsuleDirection, angle, ref direction, distance, layerMask, minDepth, maxDepth);
		}

		// Token: 0x06001989 RID: 6537 RVA: 0x0001FF30 File Offset: 0x0001E130
		[ExcludeFromDocs]
		public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_CapsuleCastAll(ref origin, ref size, capsuleDirection, angle, ref direction, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x0600198A RID: 6538 RVA: 0x0001FF60 File Offset: 0x0001E160
		[ExcludeFromDocs]
		public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_CapsuleCastAll(ref origin, ref size, capsuleDirection, angle, ref direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600198B RID: 6539 RVA: 0x0001FF94 File Offset: 0x0001E194
		[ExcludeFromDocs]
		public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_CapsuleCastAll(ref origin, ref size, capsuleDirection, angle, ref direction, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600198C RID: 6540 RVA: 0x0001FFCC File Offset: 0x0001E1CC
		[ExcludeFromDocs]
		public static RaycastHit2D[] CapsuleCastAll(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_CapsuleCastAll(ref origin, ref size, capsuleDirection, angle, ref direction, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x0600198D RID: 6541
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RaycastHit2D[] INTERNAL_CALL_CapsuleCastAll(ref Vector2 origin, ref Vector2 size, CapsuleDirection2D capsuleDirection, float angle, ref Vector2 direction, float distance, int layerMask, float minDepth, float maxDepth);

		// Token: 0x0600198E RID: 6542 RVA: 0x0002000C File Offset: 0x0001E20C
		public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_CapsuleCastNonAlloc(ref origin, ref size, capsuleDirection, angle, ref direction, results, distance, layerMask, minDepth, maxDepth);
		}

		// Token: 0x0600198F RID: 6543 RVA: 0x00020038 File Offset: 0x0001E238
		[ExcludeFromDocs]
		public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_CapsuleCastNonAlloc(ref origin, ref size, capsuleDirection, angle, ref direction, results, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001990 RID: 6544 RVA: 0x0002006C File Offset: 0x0001E26C
		[ExcludeFromDocs]
		public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, RaycastHit2D[] results, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_CapsuleCastNonAlloc(ref origin, ref size, capsuleDirection, angle, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001991 RID: 6545 RVA: 0x000200A4 File Offset: 0x0001E2A4
		[ExcludeFromDocs]
		public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, RaycastHit2D[] results, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_CapsuleCastNonAlloc(ref origin, ref size, capsuleDirection, angle, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001992 RID: 6546 RVA: 0x000200DC File Offset: 0x0001E2DC
		[ExcludeFromDocs]
		public static int CapsuleCastNonAlloc(Vector2 origin, Vector2 size, CapsuleDirection2D capsuleDirection, float angle, Vector2 direction, RaycastHit2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			float positiveInfinity2 = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_CapsuleCastNonAlloc(ref origin, ref size, capsuleDirection, angle, ref direction, results, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001993 RID: 6547
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_CapsuleCastNonAlloc(ref Vector2 origin, ref Vector2 size, CapsuleDirection2D capsuleDirection, float angle, ref Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth, float maxDepth);

		// Token: 0x06001994 RID: 6548 RVA: 0x0002011C File Offset: 0x0001E31C
		private static void Internal_GetRayIntersection(Ray ray, float distance, int layerMask, out RaycastHit2D raycastHit)
		{
			Physics2D.INTERNAL_CALL_Internal_GetRayIntersection(ref ray, distance, layerMask, out raycastHit);
		}

		// Token: 0x06001995 RID: 6549
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_GetRayIntersection(ref Ray ray, float distance, int layerMask, out RaycastHit2D raycastHit);

		// Token: 0x06001996 RID: 6550 RVA: 0x0002012C File Offset: 0x0001E32C
		[ExcludeFromDocs]
		public static RaycastHit2D GetRayIntersection(Ray ray, float distance)
		{
			int layerMask = -5;
			return Physics2D.GetRayIntersection(ray, distance, layerMask);
		}

		// Token: 0x06001997 RID: 6551 RVA: 0x0002014C File Offset: 0x0001E34C
		[ExcludeFromDocs]
		public static RaycastHit2D GetRayIntersection(Ray ray)
		{
			int layerMask = -5;
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.GetRayIntersection(ray, positiveInfinity, layerMask);
		}

		// Token: 0x06001998 RID: 6552 RVA: 0x00020174 File Offset: 0x0001E374
		public static RaycastHit2D GetRayIntersection(Ray ray, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask)
		{
			RaycastHit2D result;
			Physics2D.Internal_GetRayIntersection(ray, distance, layerMask, out result);
			return result;
		}

		// Token: 0x06001999 RID: 6553 RVA: 0x00020194 File Offset: 0x0001E394
		[RequiredByNativeCode]
		public static RaycastHit2D[] GetRayIntersectionAll(Ray ray, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask)
		{
			return Physics2D.INTERNAL_CALL_GetRayIntersectionAll(ref ray, distance, layerMask);
		}

		// Token: 0x0600199A RID: 6554 RVA: 0x000201B4 File Offset: 0x0001E3B4
		[ExcludeFromDocs]
		public static RaycastHit2D[] GetRayIntersectionAll(Ray ray, float distance)
		{
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_GetRayIntersectionAll(ref ray, distance, layerMask);
		}

		// Token: 0x0600199B RID: 6555 RVA: 0x000201D8 File Offset: 0x0001E3D8
		[ExcludeFromDocs]
		public static RaycastHit2D[] GetRayIntersectionAll(Ray ray)
		{
			int layerMask = -5;
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_GetRayIntersectionAll(ref ray, positiveInfinity, layerMask);
		}

		// Token: 0x0600199C RID: 6556
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RaycastHit2D[] INTERNAL_CALL_GetRayIntersectionAll(ref Ray ray, float distance, int layerMask);

		// Token: 0x0600199D RID: 6557 RVA: 0x00020200 File Offset: 0x0001E400
		public static int GetRayIntersectionNonAlloc(Ray ray, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("DefaultRaycastLayers")] int layerMask)
		{
			return Physics2D.INTERNAL_CALL_GetRayIntersectionNonAlloc(ref ray, results, distance, layerMask);
		}

		// Token: 0x0600199E RID: 6558 RVA: 0x00020220 File Offset: 0x0001E420
		[ExcludeFromDocs]
		public static int GetRayIntersectionNonAlloc(Ray ray, RaycastHit2D[] results, float distance)
		{
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_GetRayIntersectionNonAlloc(ref ray, results, distance, layerMask);
		}

		// Token: 0x0600199F RID: 6559 RVA: 0x00020244 File Offset: 0x0001E444
		[ExcludeFromDocs]
		public static int GetRayIntersectionNonAlloc(Ray ray, RaycastHit2D[] results)
		{
			int layerMask = -5;
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_GetRayIntersectionNonAlloc(ref ray, results, positiveInfinity, layerMask);
		}

		// Token: 0x060019A0 RID: 6560
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_GetRayIntersectionNonAlloc(ref Ray ray, RaycastHit2D[] results, float distance, int layerMask);

		// Token: 0x060019A1 RID: 6561 RVA: 0x0002026C File Offset: 0x0001E46C
		public static Collider2D OverlapPoint(Vector2 point, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapPoint(ref point, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019A2 RID: 6562 RVA: 0x0002028C File Offset: 0x0001E48C
		[ExcludeFromDocs]
		public static Collider2D OverlapPoint(Vector2 point, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapPoint(ref point, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019A3 RID: 6563 RVA: 0x000202B4 File Offset: 0x0001E4B4
		[ExcludeFromDocs]
		public static Collider2D OverlapPoint(Vector2 point, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapPoint(ref point, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019A4 RID: 6564 RVA: 0x000202E0 File Offset: 0x0001E4E0
		[ExcludeFromDocs]
		public static Collider2D OverlapPoint(Vector2 point)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapPoint(ref point, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019A5 RID: 6565
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D INTERNAL_CALL_OverlapPoint(ref Vector2 point, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019A6 RID: 6566 RVA: 0x00020310 File Offset: 0x0001E510
		public static Collider2D[] OverlapPointAll(Vector2 point, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapPointAll(ref point, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019A7 RID: 6567 RVA: 0x00020330 File Offset: 0x0001E530
		[ExcludeFromDocs]
		public static Collider2D[] OverlapPointAll(Vector2 point, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapPointAll(ref point, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019A8 RID: 6568 RVA: 0x00020358 File Offset: 0x0001E558
		[ExcludeFromDocs]
		public static Collider2D[] OverlapPointAll(Vector2 point, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapPointAll(ref point, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019A9 RID: 6569 RVA: 0x00020384 File Offset: 0x0001E584
		[ExcludeFromDocs]
		public static Collider2D[] OverlapPointAll(Vector2 point)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapPointAll(ref point, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019AA RID: 6570
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D[] INTERNAL_CALL_OverlapPointAll(ref Vector2 point, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019AB RID: 6571 RVA: 0x000203B4 File Offset: 0x0001E5B4
		public static int OverlapPointNonAlloc(Vector2 point, Collider2D[] results, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapPointNonAlloc(ref point, results, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019AC RID: 6572 RVA: 0x000203D8 File Offset: 0x0001E5D8
		[ExcludeFromDocs]
		public static int OverlapPointNonAlloc(Vector2 point, Collider2D[] results, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapPointNonAlloc(ref point, results, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019AD RID: 6573 RVA: 0x00020400 File Offset: 0x0001E600
		[ExcludeFromDocs]
		public static int OverlapPointNonAlloc(Vector2 point, Collider2D[] results, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapPointNonAlloc(ref point, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019AE RID: 6574 RVA: 0x0002042C File Offset: 0x0001E62C
		[ExcludeFromDocs]
		public static int OverlapPointNonAlloc(Vector2 point, Collider2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapPointNonAlloc(ref point, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019AF RID: 6575
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_OverlapPointNonAlloc(ref Vector2 point, Collider2D[] results, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019B0 RID: 6576 RVA: 0x0002045C File Offset: 0x0001E65C
		public static Collider2D OverlapCircle(Vector2 point, float radius, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapCircle(ref point, radius, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019B1 RID: 6577 RVA: 0x00020480 File Offset: 0x0001E680
		[ExcludeFromDocs]
		public static Collider2D OverlapCircle(Vector2 point, float radius, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCircle(ref point, radius, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019B2 RID: 6578 RVA: 0x000204A8 File Offset: 0x0001E6A8
		[ExcludeFromDocs]
		public static Collider2D OverlapCircle(Vector2 point, float radius, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCircle(ref point, radius, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019B3 RID: 6579 RVA: 0x000204D4 File Offset: 0x0001E6D4
		[ExcludeFromDocs]
		public static Collider2D OverlapCircle(Vector2 point, float radius)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapCircle(ref point, radius, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019B4 RID: 6580
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D INTERNAL_CALL_OverlapCircle(ref Vector2 point, float radius, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019B5 RID: 6581 RVA: 0x00020504 File Offset: 0x0001E704
		public static Collider2D[] OverlapCircleAll(Vector2 point, float radius, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapCircleAll(ref point, radius, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019B6 RID: 6582 RVA: 0x00020528 File Offset: 0x0001E728
		[ExcludeFromDocs]
		public static Collider2D[] OverlapCircleAll(Vector2 point, float radius, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCircleAll(ref point, radius, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019B7 RID: 6583 RVA: 0x00020550 File Offset: 0x0001E750
		[ExcludeFromDocs]
		public static Collider2D[] OverlapCircleAll(Vector2 point, float radius, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCircleAll(ref point, radius, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019B8 RID: 6584 RVA: 0x0002057C File Offset: 0x0001E77C
		[ExcludeFromDocs]
		public static Collider2D[] OverlapCircleAll(Vector2 point, float radius)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapCircleAll(ref point, radius, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019B9 RID: 6585
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D[] INTERNAL_CALL_OverlapCircleAll(ref Vector2 point, float radius, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019BA RID: 6586 RVA: 0x000205AC File Offset: 0x0001E7AC
		public static int OverlapCircleNonAlloc(Vector2 point, float radius, Collider2D[] results, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapCircleNonAlloc(ref point, radius, results, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019BB RID: 6587 RVA: 0x000205D0 File Offset: 0x0001E7D0
		[ExcludeFromDocs]
		public static int OverlapCircleNonAlloc(Vector2 point, float radius, Collider2D[] results, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCircleNonAlloc(ref point, radius, results, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019BC RID: 6588 RVA: 0x000205F8 File Offset: 0x0001E7F8
		[ExcludeFromDocs]
		public static int OverlapCircleNonAlloc(Vector2 point, float radius, Collider2D[] results, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCircleNonAlloc(ref point, radius, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019BD RID: 6589 RVA: 0x00020628 File Offset: 0x0001E828
		[ExcludeFromDocs]
		public static int OverlapCircleNonAlloc(Vector2 point, float radius, Collider2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapCircleNonAlloc(ref point, radius, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019BE RID: 6590
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_OverlapCircleNonAlloc(ref Vector2 point, float radius, Collider2D[] results, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019BF RID: 6591 RVA: 0x00020658 File Offset: 0x0001E858
		public static Collider2D OverlapBox(Vector2 point, Vector2 size, float angle, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapBox(ref point, ref size, angle, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019C0 RID: 6592 RVA: 0x0002067C File Offset: 0x0001E87C
		[ExcludeFromDocs]
		public static Collider2D OverlapBox(Vector2 point, Vector2 size, float angle, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapBox(ref point, ref size, angle, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019C1 RID: 6593 RVA: 0x000206A8 File Offset: 0x0001E8A8
		[ExcludeFromDocs]
		public static Collider2D OverlapBox(Vector2 point, Vector2 size, float angle, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapBox(ref point, ref size, angle, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019C2 RID: 6594 RVA: 0x000206D8 File Offset: 0x0001E8D8
		[ExcludeFromDocs]
		public static Collider2D OverlapBox(Vector2 point, Vector2 size, float angle)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapBox(ref point, ref size, angle, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019C3 RID: 6595
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D INTERNAL_CALL_OverlapBox(ref Vector2 point, ref Vector2 size, float angle, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019C4 RID: 6596 RVA: 0x0002070C File Offset: 0x0001E90C
		public static Collider2D[] OverlapBoxAll(Vector2 point, Vector2 size, float angle, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapBoxAll(ref point, ref size, angle, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019C5 RID: 6597 RVA: 0x00020730 File Offset: 0x0001E930
		[ExcludeFromDocs]
		public static Collider2D[] OverlapBoxAll(Vector2 point, Vector2 size, float angle, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapBoxAll(ref point, ref size, angle, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019C6 RID: 6598 RVA: 0x0002075C File Offset: 0x0001E95C
		[ExcludeFromDocs]
		public static Collider2D[] OverlapBoxAll(Vector2 point, Vector2 size, float angle, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapBoxAll(ref point, ref size, angle, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019C7 RID: 6599 RVA: 0x0002078C File Offset: 0x0001E98C
		[ExcludeFromDocs]
		public static Collider2D[] OverlapBoxAll(Vector2 point, Vector2 size, float angle)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapBoxAll(ref point, ref size, angle, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019C8 RID: 6600
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D[] INTERNAL_CALL_OverlapBoxAll(ref Vector2 point, ref Vector2 size, float angle, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019C9 RID: 6601 RVA: 0x000207C0 File Offset: 0x0001E9C0
		public static int OverlapBoxNonAlloc(Vector2 point, Vector2 size, float angle, Collider2D[] results, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapBoxNonAlloc(ref point, ref size, angle, results, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019CA RID: 6602 RVA: 0x000207E8 File Offset: 0x0001E9E8
		[ExcludeFromDocs]
		public static int OverlapBoxNonAlloc(Vector2 point, Vector2 size, float angle, Collider2D[] results, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapBoxNonAlloc(ref point, ref size, angle, results, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019CB RID: 6603 RVA: 0x00020814 File Offset: 0x0001EA14
		[ExcludeFromDocs]
		public static int OverlapBoxNonAlloc(Vector2 point, Vector2 size, float angle, Collider2D[] results, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapBoxNonAlloc(ref point, ref size, angle, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019CC RID: 6604 RVA: 0x00020844 File Offset: 0x0001EA44
		[ExcludeFromDocs]
		public static int OverlapBoxNonAlloc(Vector2 point, Vector2 size, float angle, Collider2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapBoxNonAlloc(ref point, ref size, angle, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019CD RID: 6605
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_OverlapBoxNonAlloc(ref Vector2 point, ref Vector2 size, float angle, Collider2D[] results, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019CE RID: 6606 RVA: 0x00020878 File Offset: 0x0001EA78
		public static Collider2D OverlapArea(Vector2 pointA, Vector2 pointB, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapArea(ref pointA, ref pointB, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019CF RID: 6607 RVA: 0x0002089C File Offset: 0x0001EA9C
		[ExcludeFromDocs]
		public static Collider2D OverlapArea(Vector2 pointA, Vector2 pointB, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapArea(ref pointA, ref pointB, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019D0 RID: 6608 RVA: 0x000208C4 File Offset: 0x0001EAC4
		[ExcludeFromDocs]
		public static Collider2D OverlapArea(Vector2 pointA, Vector2 pointB, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapArea(ref pointA, ref pointB, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019D1 RID: 6609 RVA: 0x000208F4 File Offset: 0x0001EAF4
		[ExcludeFromDocs]
		public static Collider2D OverlapArea(Vector2 pointA, Vector2 pointB)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapArea(ref pointA, ref pointB, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019D2 RID: 6610
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D INTERNAL_CALL_OverlapArea(ref Vector2 pointA, ref Vector2 pointB, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019D3 RID: 6611 RVA: 0x00020924 File Offset: 0x0001EB24
		public static Collider2D[] OverlapAreaAll(Vector2 pointA, Vector2 pointB, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapAreaAll(ref pointA, ref pointB, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019D4 RID: 6612 RVA: 0x00020948 File Offset: 0x0001EB48
		[ExcludeFromDocs]
		public static Collider2D[] OverlapAreaAll(Vector2 pointA, Vector2 pointB, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapAreaAll(ref pointA, ref pointB, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019D5 RID: 6613 RVA: 0x00020970 File Offset: 0x0001EB70
		[ExcludeFromDocs]
		public static Collider2D[] OverlapAreaAll(Vector2 pointA, Vector2 pointB, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapAreaAll(ref pointA, ref pointB, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019D6 RID: 6614 RVA: 0x000209A0 File Offset: 0x0001EBA0
		[ExcludeFromDocs]
		public static Collider2D[] OverlapAreaAll(Vector2 pointA, Vector2 pointB)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapAreaAll(ref pointA, ref pointB, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019D7 RID: 6615
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D[] INTERNAL_CALL_OverlapAreaAll(ref Vector2 pointA, ref Vector2 pointB, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019D8 RID: 6616 RVA: 0x000209D0 File Offset: 0x0001EBD0
		public static int OverlapAreaNonAlloc(Vector2 pointA, Vector2 pointB, Collider2D[] results, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapAreaNonAlloc(ref pointA, ref pointB, results, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019D9 RID: 6617 RVA: 0x000209F4 File Offset: 0x0001EBF4
		[ExcludeFromDocs]
		public static int OverlapAreaNonAlloc(Vector2 pointA, Vector2 pointB, Collider2D[] results, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapAreaNonAlloc(ref pointA, ref pointB, results, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019DA RID: 6618 RVA: 0x00020A20 File Offset: 0x0001EC20
		[ExcludeFromDocs]
		public static int OverlapAreaNonAlloc(Vector2 pointA, Vector2 pointB, Collider2D[] results, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapAreaNonAlloc(ref pointA, ref pointB, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019DB RID: 6619 RVA: 0x00020A50 File Offset: 0x0001EC50
		[ExcludeFromDocs]
		public static int OverlapAreaNonAlloc(Vector2 pointA, Vector2 pointB, Collider2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapAreaNonAlloc(ref pointA, ref pointB, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019DC RID: 6620
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_OverlapAreaNonAlloc(ref Vector2 pointA, ref Vector2 pointB, Collider2D[] results, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019DD RID: 6621 RVA: 0x00020A84 File Offset: 0x0001EC84
		public static Collider2D OverlapCapsule(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapCapsule(ref point, ref size, direction, angle, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019DE RID: 6622 RVA: 0x00020AAC File Offset: 0x0001ECAC
		[ExcludeFromDocs]
		public static Collider2D OverlapCapsule(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCapsule(ref point, ref size, direction, angle, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019DF RID: 6623 RVA: 0x00020AD8 File Offset: 0x0001ECD8
		[ExcludeFromDocs]
		public static Collider2D OverlapCapsule(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCapsule(ref point, ref size, direction, angle, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019E0 RID: 6624 RVA: 0x00020B08 File Offset: 0x0001ED08
		[ExcludeFromDocs]
		public static Collider2D OverlapCapsule(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapCapsule(ref point, ref size, direction, angle, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019E1 RID: 6625
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D INTERNAL_CALL_OverlapCapsule(ref Vector2 point, ref Vector2 size, CapsuleDirection2D direction, float angle, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019E2 RID: 6626 RVA: 0x00020B3C File Offset: 0x0001ED3C
		public static Collider2D[] OverlapCapsuleAll(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapCapsuleAll(ref point, ref size, direction, angle, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019E3 RID: 6627 RVA: 0x00020B64 File Offset: 0x0001ED64
		[ExcludeFromDocs]
		public static Collider2D[] OverlapCapsuleAll(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCapsuleAll(ref point, ref size, direction, angle, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019E4 RID: 6628 RVA: 0x00020B90 File Offset: 0x0001ED90
		[ExcludeFromDocs]
		public static Collider2D[] OverlapCapsuleAll(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCapsuleAll(ref point, ref size, direction, angle, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019E5 RID: 6629 RVA: 0x00020BC0 File Offset: 0x0001EDC0
		[ExcludeFromDocs]
		public static Collider2D[] OverlapCapsuleAll(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapCapsuleAll(ref point, ref size, direction, angle, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019E6 RID: 6630
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider2D[] INTERNAL_CALL_OverlapCapsuleAll(ref Vector2 point, ref Vector2 size, CapsuleDirection2D direction, float angle, int layerMask, float minDepth, float maxDepth);

		// Token: 0x060019E7 RID: 6631 RVA: 0x00020BF4 File Offset: 0x0001EDF4
		public static int OverlapCapsuleNonAlloc(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, Collider2D[] results, [DefaultValue("DefaultRaycastLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Physics2D.INTERNAL_CALL_OverlapCapsuleNonAlloc(ref point, ref size, direction, angle, results, layerMask, minDepth, maxDepth);
		}

		// Token: 0x060019E8 RID: 6632 RVA: 0x00020C1C File Offset: 0x0001EE1C
		[ExcludeFromDocs]
		public static int OverlapCapsuleNonAlloc(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, Collider2D[] results, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCapsuleNonAlloc(ref point, ref size, direction, angle, results, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x060019E9 RID: 6633 RVA: 0x00020C4C File Offset: 0x0001EE4C
		[ExcludeFromDocs]
		public static int OverlapCapsuleNonAlloc(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, Collider2D[] results, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Physics2D.INTERNAL_CALL_OverlapCapsuleNonAlloc(ref point, ref size, direction, angle, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019EA RID: 6634 RVA: 0x00020C80 File Offset: 0x0001EE80
		[ExcludeFromDocs]
		public static int OverlapCapsuleNonAlloc(Vector2 point, Vector2 size, CapsuleDirection2D direction, float angle, Collider2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -5;
			return Physics2D.INTERNAL_CALL_OverlapCapsuleNonAlloc(ref point, ref size, direction, angle, results, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x060019EB RID: 6635
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_OverlapCapsuleNonAlloc(ref Vector2 point, ref Vector2 size, CapsuleDirection2D direction, float angle, Collider2D[] results, int layerMask, float minDepth, float maxDepth);

		// Token: 0x040003C6 RID: 966
		public const int IgnoreRaycastLayer = 4;

		// Token: 0x040003C7 RID: 967
		public const int DefaultRaycastLayers = -5;

		// Token: 0x040003C8 RID: 968
		public const int AllLayers = -1;

		// Token: 0x040003C9 RID: 969
		private static List<Rigidbody2D> m_LastDisabledRigidbody2D = new List<Rigidbody2D>();
	}
}
