﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001F2 RID: 498
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class SplatPrototype
	{
		// Token: 0x1700084C RID: 2124
		// (get) Token: 0x060021B9 RID: 8633 RVA: 0x000272A0 File Offset: 0x000254A0
		// (set) Token: 0x060021BA RID: 8634 RVA: 0x000272BC File Offset: 0x000254BC
		public Texture2D texture
		{
			get
			{
				return this.m_Texture;
			}
			set
			{
				this.m_Texture = value;
			}
		}

		// Token: 0x1700084D RID: 2125
		// (get) Token: 0x060021BB RID: 8635 RVA: 0x000272C8 File Offset: 0x000254C8
		// (set) Token: 0x060021BC RID: 8636 RVA: 0x000272E4 File Offset: 0x000254E4
		public Texture2D normalMap
		{
			get
			{
				return this.m_NormalMap;
			}
			set
			{
				this.m_NormalMap = value;
			}
		}

		// Token: 0x1700084E RID: 2126
		// (get) Token: 0x060021BD RID: 8637 RVA: 0x000272F0 File Offset: 0x000254F0
		// (set) Token: 0x060021BE RID: 8638 RVA: 0x0002730C File Offset: 0x0002550C
		public Vector2 tileSize
		{
			get
			{
				return this.m_TileSize;
			}
			set
			{
				this.m_TileSize = value;
			}
		}

		// Token: 0x1700084F RID: 2127
		// (get) Token: 0x060021BF RID: 8639 RVA: 0x00027318 File Offset: 0x00025518
		// (set) Token: 0x060021C0 RID: 8640 RVA: 0x00027334 File Offset: 0x00025534
		public Vector2 tileOffset
		{
			get
			{
				return this.m_TileOffset;
			}
			set
			{
				this.m_TileOffset = value;
			}
		}

		// Token: 0x17000850 RID: 2128
		// (get) Token: 0x060021C1 RID: 8641 RVA: 0x00027340 File Offset: 0x00025540
		// (set) Token: 0x060021C2 RID: 8642 RVA: 0x0002737C File Offset: 0x0002557C
		public Color specular
		{
			get
			{
				return new Color(this.m_SpecularMetallic.x, this.m_SpecularMetallic.y, this.m_SpecularMetallic.z);
			}
			set
			{
				this.m_SpecularMetallic.x = value.r;
				this.m_SpecularMetallic.y = value.g;
				this.m_SpecularMetallic.z = value.b;
			}
		}

		// Token: 0x17000851 RID: 2129
		// (get) Token: 0x060021C3 RID: 8643 RVA: 0x000273B8 File Offset: 0x000255B8
		// (set) Token: 0x060021C4 RID: 8644 RVA: 0x000273D8 File Offset: 0x000255D8
		public float metallic
		{
			get
			{
				return this.m_SpecularMetallic.w;
			}
			set
			{
				this.m_SpecularMetallic.w = value;
			}
		}

		// Token: 0x17000852 RID: 2130
		// (get) Token: 0x060021C5 RID: 8645 RVA: 0x000273E8 File Offset: 0x000255E8
		// (set) Token: 0x060021C6 RID: 8646 RVA: 0x00027404 File Offset: 0x00025604
		public float smoothness
		{
			get
			{
				return this.m_Smoothness;
			}
			set
			{
				this.m_Smoothness = value;
			}
		}

		// Token: 0x040005C8 RID: 1480
		internal Texture2D m_Texture;

		// Token: 0x040005C9 RID: 1481
		internal Texture2D m_NormalMap;

		// Token: 0x040005CA RID: 1482
		internal Vector2 m_TileSize = new Vector2(15f, 15f);

		// Token: 0x040005CB RID: 1483
		internal Vector2 m_TileOffset = new Vector2(0f, 0f);

		// Token: 0x040005CC RID: 1484
		internal Vector4 m_SpecularMetallic = new Vector4(0f, 0f, 0f, 0f);

		// Token: 0x040005CD RID: 1485
		internal float m_Smoothness = 0f;
	}
}
