﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Connect
{
	// Token: 0x0200028B RID: 651
	internal class UnityConnectSettings
	{
		// Token: 0x17000A13 RID: 2579
		// (get) Token: 0x06002A4F RID: 10831
		// (set) Token: 0x06002A50 RID: 10832
		public static extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A14 RID: 2580
		// (get) Token: 0x06002A51 RID: 10833
		// (set) Token: 0x06002A52 RID: 10834
		public static extern bool testMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A15 RID: 2581
		// (get) Token: 0x06002A53 RID: 10835
		// (set) Token: 0x06002A54 RID: 10836
		public static extern string testEventUrl { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A16 RID: 2582
		// (get) Token: 0x06002A55 RID: 10837
		// (set) Token: 0x06002A56 RID: 10838
		public static extern string testConfigUrl { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
