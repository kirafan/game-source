﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Connect
{
	// Token: 0x0200028E RID: 654
	internal class UnityAdsSettings
	{
		// Token: 0x17000A17 RID: 2583
		// (get) Token: 0x06002A69 RID: 10857
		// (set) Token: 0x06002A6A RID: 10858
		[ThreadAndSerializationSafe]
		public static extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002A6B RID: 10859
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsPlatformEnabled(RuntimePlatform platform);

		// Token: 0x06002A6C RID: 10860
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetPlatformEnabled(RuntimePlatform platform, bool value);

		// Token: 0x17000A18 RID: 2584
		// (get) Token: 0x06002A6D RID: 10861
		// (set) Token: 0x06002A6E RID: 10862
		public static extern bool initializeOnStartup { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A19 RID: 2585
		// (get) Token: 0x06002A6F RID: 10863
		// (set) Token: 0x06002A70 RID: 10864
		public static extern bool testMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002A71 RID: 10865
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetGameId(RuntimePlatform platform);

		// Token: 0x06002A72 RID: 10866
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetGameId(RuntimePlatform platform, string gameId);
	}
}
