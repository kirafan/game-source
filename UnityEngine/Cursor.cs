﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000033 RID: 51
	public sealed class Cursor
	{
		// Token: 0x06000391 RID: 913 RVA: 0x000064D8 File Offset: 0x000046D8
		private static void SetCursor(Texture2D texture, CursorMode cursorMode)
		{
			Cursor.SetCursor(texture, Vector2.zero, cursorMode);
		}

		// Token: 0x06000392 RID: 914 RVA: 0x000064E8 File Offset: 0x000046E8
		public static void SetCursor(Texture2D texture, Vector2 hotspot, CursorMode cursorMode)
		{
			Cursor.INTERNAL_CALL_SetCursor(texture, ref hotspot, cursorMode);
		}

		// Token: 0x06000393 RID: 915
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetCursor(Texture2D texture, ref Vector2 hotspot, CursorMode cursorMode);

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000394 RID: 916
		// (set) Token: 0x06000395 RID: 917
		public static extern bool visible { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000396 RID: 918
		// (set) Token: 0x06000397 RID: 919
		public static extern CursorLockMode lockState { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
