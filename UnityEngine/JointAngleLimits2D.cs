﻿using System;

namespace UnityEngine
{
	// Token: 0x0200016B RID: 363
	public struct JointAngleLimits2D
	{
		// Token: 0x17000628 RID: 1576
		// (get) Token: 0x06001AA0 RID: 6816 RVA: 0x00021434 File Offset: 0x0001F634
		// (set) Token: 0x06001AA1 RID: 6817 RVA: 0x00021450 File Offset: 0x0001F650
		public float min
		{
			get
			{
				return this.m_LowerAngle;
			}
			set
			{
				this.m_LowerAngle = value;
			}
		}

		// Token: 0x17000629 RID: 1577
		// (get) Token: 0x06001AA2 RID: 6818 RVA: 0x0002145C File Offset: 0x0001F65C
		// (set) Token: 0x06001AA3 RID: 6819 RVA: 0x00021478 File Offset: 0x0001F678
		public float max
		{
			get
			{
				return this.m_UpperAngle;
			}
			set
			{
				this.m_UpperAngle = value;
			}
		}

		// Token: 0x040003F9 RID: 1017
		private float m_LowerAngle;

		// Token: 0x040003FA RID: 1018
		private float m_UpperAngle;
	}
}
