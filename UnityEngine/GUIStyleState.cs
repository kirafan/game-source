﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace UnityEngine
{
	// Token: 0x02000234 RID: 564
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class GUIStyleState
	{
		// Token: 0x0600260F RID: 9743 RVA: 0x00033A18 File Offset: 0x00031C18
		public GUIStyleState()
		{
			this.Init();
		}

		// Token: 0x06002610 RID: 9744 RVA: 0x00033A28 File Offset: 0x00031C28
		private GUIStyleState(GUIStyle sourceStyle, IntPtr source)
		{
			this.m_SourceStyle = sourceStyle;
			this.m_Ptr = source;
		}

		// Token: 0x06002611 RID: 9745 RVA: 0x00033A40 File Offset: 0x00031C40
		internal static GUIStyleState ProduceGUIStyleStateFromDeserialization(GUIStyle sourceStyle, IntPtr source)
		{
			GUIStyleState guistyleState = new GUIStyleState(sourceStyle, source);
			guistyleState.m_Background = guistyleState.GetBackgroundInternalFromDeserialization();
			return guistyleState;
		}

		// Token: 0x06002612 RID: 9746 RVA: 0x00033A6C File Offset: 0x00031C6C
		internal static GUIStyleState GetGUIStyleState(GUIStyle sourceStyle, IntPtr source)
		{
			GUIStyleState guistyleState = new GUIStyleState(sourceStyle, source);
			guistyleState.m_Background = guistyleState.GetBackgroundInternal();
			return guistyleState;
		}

		// Token: 0x06002613 RID: 9747 RVA: 0x00033A98 File Offset: 0x00031C98
		~GUIStyleState()
		{
			if (this.m_SourceStyle == null)
			{
				this.Cleanup();
			}
		}

		// Token: 0x1700093B RID: 2363
		// (get) Token: 0x06002614 RID: 9748 RVA: 0x00033AD4 File Offset: 0x00031CD4
		// (set) Token: 0x06002615 RID: 9749 RVA: 0x00033AF0 File Offset: 0x00031CF0
		public Texture2D background
		{
			get
			{
				return this.GetBackgroundInternal();
			}
			set
			{
				this.SetBackgroundInternal(value);
				this.m_Background = value;
			}
		}

		// Token: 0x06002616 RID: 9750
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Init();

		// Token: 0x06002617 RID: 9751
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Cleanup();

		// Token: 0x06002618 RID: 9752
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetBackgroundInternal(Texture2D value);

		// Token: 0x06002619 RID: 9753
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Texture2D GetBackgroundInternalFromDeserialization();

		// Token: 0x0600261A RID: 9754
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Texture2D GetBackgroundInternal();

		// Token: 0x1700093C RID: 2364
		// (get) Token: 0x0600261B RID: 9755 RVA: 0x00033B04 File Offset: 0x00031D04
		// (set) Token: 0x0600261C RID: 9756 RVA: 0x00033B24 File Offset: 0x00031D24
		public Color textColor
		{
			get
			{
				Color result;
				this.INTERNAL_get_textColor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_textColor(ref value);
			}
		}

		// Token: 0x0600261D RID: 9757
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_textColor(out Color value);

		// Token: 0x0600261E RID: 9758
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_textColor(ref Color value);

		// Token: 0x0400084B RID: 2123
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x0400084C RID: 2124
		private readonly GUIStyle m_SourceStyle;

		// Token: 0x0400084D RID: 2125
		[NonSerialized]
		private Texture2D m_Background;
	}
}
