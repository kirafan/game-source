﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F9 RID: 761
	public enum ScreenOrientation
	{
		// Token: 0x04000B4A RID: 2890
		Unknown,
		// Token: 0x04000B4B RID: 2891
		Portrait,
		// Token: 0x04000B4C RID: 2892
		PortraitUpsideDown,
		// Token: 0x04000B4D RID: 2893
		LandscapeLeft,
		// Token: 0x04000B4E RID: 2894
		LandscapeRight,
		// Token: 0x04000B4F RID: 2895
		AutoRotation,
		// Token: 0x04000B50 RID: 2896
		Landscape = 3
	}
}
