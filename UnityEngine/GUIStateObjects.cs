﻿using System;
using System.Collections.Generic;
using System.Security;

namespace UnityEngine
{
	// Token: 0x02000326 RID: 806
	internal class GUIStateObjects
	{
		// Token: 0x06002CB6 RID: 11446 RVA: 0x00046910 File Offset: 0x00044B10
		[SecuritySafeCritical]
		internal static object GetStateObject(Type t, int controlID)
		{
			object obj;
			if (!GUIStateObjects.s_StateCache.TryGetValue(controlID, out obj) || obj.GetType() != t)
			{
				obj = Activator.CreateInstance(t);
				GUIStateObjects.s_StateCache[controlID] = obj;
			}
			return obj;
		}

		// Token: 0x06002CB7 RID: 11447 RVA: 0x0004695C File Offset: 0x00044B5C
		internal static object QueryStateObject(Type t, int controlID)
		{
			object obj = GUIStateObjects.s_StateCache[controlID];
			object result;
			if (t.IsInstanceOfType(obj))
			{
				result = obj;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06002CB8 RID: 11448 RVA: 0x00046994 File Offset: 0x00044B94
		internal static void Tests_ClearObjects()
		{
			GUIStateObjects.s_StateCache.Clear();
		}

		// Token: 0x04000CC1 RID: 3265
		private static Dictionary<int, object> s_StateCache = new Dictionary<int, object>();
	}
}
