﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000366 RID: 870
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	public class RuntimeInitializeOnLoadMethodAttribute : PreserveAttribute
	{
		// Token: 0x06002E2A RID: 11818 RVA: 0x00049D18 File Offset: 0x00047F18
		public RuntimeInitializeOnLoadMethodAttribute()
		{
			this.loadType = RuntimeInitializeLoadType.AfterSceneLoad;
		}

		// Token: 0x06002E2B RID: 11819 RVA: 0x00049D28 File Offset: 0x00047F28
		public RuntimeInitializeOnLoadMethodAttribute(RuntimeInitializeLoadType loadType)
		{
			this.loadType = loadType;
		}

		// Token: 0x17000AF6 RID: 2806
		// (get) Token: 0x06002E2C RID: 11820 RVA: 0x00049D38 File Offset: 0x00047F38
		// (set) Token: 0x06002E2D RID: 11821 RVA: 0x00049D54 File Offset: 0x00047F54
		public RuntimeInitializeLoadType loadType { get; private set; }
	}
}
