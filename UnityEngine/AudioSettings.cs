﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000199 RID: 409
	public sealed class AudioSettings
	{
		// Token: 0x17000708 RID: 1800
		// (get) Token: 0x06001CD9 RID: 7385
		public static extern AudioSpeakerMode driverCapabilities { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000709 RID: 1801
		// (get) Token: 0x06001CDA RID: 7386
		// (set) Token: 0x06001CDB RID: 7387
		public static extern AudioSpeakerMode speakerMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700070A RID: 1802
		// (get) Token: 0x06001CDC RID: 7388
		internal static extern int profilerCaptureFlags { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700070B RID: 1803
		// (get) Token: 0x06001CDD RID: 7389
		[ThreadAndSerializationSafe]
		public static extern double dspTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700070C RID: 1804
		// (get) Token: 0x06001CDE RID: 7390
		// (set) Token: 0x06001CDF RID: 7391
		public static extern int outputSampleRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001CE0 RID: 7392
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void GetDSPBufferSize(out int bufferLength, out int numBuffers);

		// Token: 0x06001CE1 RID: 7393
		[Obsolete("AudioSettings.SetDSPBufferSize is deprecated and has been replaced by audio project settings and the AudioSettings.GetConfiguration/AudioSettings.Reset API.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetDSPBufferSize(int bufferLength, int numBuffers);

		// Token: 0x06001CE2 RID: 7394 RVA: 0x000222BC File Offset: 0x000204BC
		public static AudioConfiguration GetConfiguration()
		{
			AudioConfiguration result;
			AudioSettings.INTERNAL_CALL_GetConfiguration(out result);
			return result;
		}

		// Token: 0x06001CE3 RID: 7395
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetConfiguration(out AudioConfiguration value);

		// Token: 0x06001CE4 RID: 7396 RVA: 0x000222DC File Offset: 0x000204DC
		public static bool Reset(AudioConfiguration config)
		{
			return AudioSettings.INTERNAL_CALL_Reset(ref config);
		}

		// Token: 0x06001CE5 RID: 7397
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Reset(ref AudioConfiguration config);

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x06001CE6 RID: 7398 RVA: 0x000222F8 File Offset: 0x000204F8
		// (remove) Token: 0x06001CE7 RID: 7399 RVA: 0x0002232C File Offset: 0x0002052C
		public static event AudioSettings.AudioConfigurationChangeHandler OnAudioConfigurationChanged;

		// Token: 0x06001CE8 RID: 7400 RVA: 0x00022360 File Offset: 0x00020560
		[RequiredByNativeCode]
		internal static void InvokeOnAudioConfigurationChanged(bool deviceWasChanged)
		{
			if (AudioSettings.OnAudioConfigurationChanged != null)
			{
				AudioSettings.OnAudioConfigurationChanged(deviceWasChanged);
			}
		}

		// Token: 0x1700070D RID: 1805
		// (get) Token: 0x06001CE9 RID: 7401
		internal static extern bool unityAudioDisabled { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0200019A RID: 410
		// (Invoke) Token: 0x06001CEB RID: 7403
		public delegate void AudioConfigurationChangeHandler(bool deviceWasChanged);
	}
}
