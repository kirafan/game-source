﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200004D RID: 77
	public sealed class TrailRenderer : Renderer
	{
		// Token: 0x17000152 RID: 338
		// (get) Token: 0x06000591 RID: 1425
		// (set) Token: 0x06000592 RID: 1426
		public extern float time { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000153 RID: 339
		// (get) Token: 0x06000593 RID: 1427
		// (set) Token: 0x06000594 RID: 1428
		public extern float startWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x06000595 RID: 1429
		// (set) Token: 0x06000596 RID: 1430
		public extern float endWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x06000597 RID: 1431
		// (set) Token: 0x06000598 RID: 1432
		public extern AnimationCurve widthCurve { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x06000599 RID: 1433
		// (set) Token: 0x0600059A RID: 1434
		public extern float widthMultiplier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x0600059B RID: 1435 RVA: 0x00007C00 File Offset: 0x00005E00
		// (set) Token: 0x0600059C RID: 1436 RVA: 0x00007C20 File Offset: 0x00005E20
		public Color startColor
		{
			get
			{
				Color result;
				this.INTERNAL_get_startColor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_startColor(ref value);
			}
		}

		// Token: 0x0600059D RID: 1437
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_startColor(out Color value);

		// Token: 0x0600059E RID: 1438
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_startColor(ref Color value);

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x0600059F RID: 1439 RVA: 0x00007C2C File Offset: 0x00005E2C
		// (set) Token: 0x060005A0 RID: 1440 RVA: 0x00007C4C File Offset: 0x00005E4C
		public Color endColor
		{
			get
			{
				Color result;
				this.INTERNAL_get_endColor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_endColor(ref value);
			}
		}

		// Token: 0x060005A1 RID: 1441
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_endColor(out Color value);

		// Token: 0x060005A2 RID: 1442
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_endColor(ref Color value);

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060005A3 RID: 1443
		// (set) Token: 0x060005A4 RID: 1444
		public extern Gradient colorGradient { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x060005A5 RID: 1445
		// (set) Token: 0x060005A6 RID: 1446
		public extern bool autodestruct { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x060005A7 RID: 1447
		// (set) Token: 0x060005A8 RID: 1448
		public extern int numCornerVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060005A9 RID: 1449
		// (set) Token: 0x060005AA RID: 1450
		public extern int numCapVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060005AB RID: 1451
		// (set) Token: 0x060005AC RID: 1452
		public extern float minVertexDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700015E RID: 350
		// (get) Token: 0x060005AD RID: 1453
		// (set) Token: 0x060005AE RID: 1454
		public extern LineTextureMode textureMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060005AF RID: 1455
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Clear();
	}
}
