﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000161 RID: 353
	public class Collider2D : Behaviour
	{
		// Token: 0x17000609 RID: 1545
		// (get) Token: 0x06001A51 RID: 6737
		// (set) Token: 0x06001A52 RID: 6738
		public extern float density { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700060A RID: 1546
		// (get) Token: 0x06001A53 RID: 6739
		// (set) Token: 0x06001A54 RID: 6740
		public extern bool isTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700060B RID: 1547
		// (get) Token: 0x06001A55 RID: 6741
		// (set) Token: 0x06001A56 RID: 6742
		public extern bool usedByEffector { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700060C RID: 1548
		// (get) Token: 0x06001A57 RID: 6743 RVA: 0x00020FE8 File Offset: 0x0001F1E8
		// (set) Token: 0x06001A58 RID: 6744 RVA: 0x00021008 File Offset: 0x0001F208
		public Vector2 offset
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_offset(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_offset(ref value);
			}
		}

		// Token: 0x06001A59 RID: 6745
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_offset(out Vector2 value);

		// Token: 0x06001A5A RID: 6746
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_offset(ref Vector2 value);

		// Token: 0x1700060D RID: 1549
		// (get) Token: 0x06001A5B RID: 6747
		public extern Rigidbody2D attachedRigidbody { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700060E RID: 1550
		// (get) Token: 0x06001A5C RID: 6748
		public extern int shapeCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700060F RID: 1551
		// (get) Token: 0x06001A5D RID: 6749 RVA: 0x00021014 File Offset: 0x0001F214
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_bounds(out result);
				return result;
			}
		}

		// Token: 0x06001A5E RID: 6750
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_bounds(out Bounds value);

		// Token: 0x17000610 RID: 1552
		// (get) Token: 0x06001A5F RID: 6751
		internal extern ColliderErrorState2D errorState { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000611 RID: 1553
		// (get) Token: 0x06001A60 RID: 6752
		// (set) Token: 0x06001A61 RID: 6753
		public extern PhysicsMaterial2D sharedMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000612 RID: 1554
		// (get) Token: 0x06001A62 RID: 6754
		public extern float friction { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000613 RID: 1555
		// (get) Token: 0x06001A63 RID: 6755
		public extern float bounciness { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001A64 RID: 6756
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouching(Collider2D collider);

		// Token: 0x06001A65 RID: 6757
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouchingLayers([DefaultValue("Physics2D.AllLayers")] int layerMask);

		// Token: 0x06001A66 RID: 6758 RVA: 0x00021034 File Offset: 0x0001F234
		[ExcludeFromDocs]
		public bool IsTouchingLayers()
		{
			int layerMask = -1;
			return this.IsTouchingLayers(layerMask);
		}

		// Token: 0x06001A67 RID: 6759 RVA: 0x00021054 File Offset: 0x0001F254
		public bool OverlapPoint(Vector2 point)
		{
			return Collider2D.INTERNAL_CALL_OverlapPoint(this, ref point);
		}

		// Token: 0x06001A68 RID: 6760
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_OverlapPoint(Collider2D self, ref Vector2 point);

		// Token: 0x06001A69 RID: 6761 RVA: 0x00021074 File Offset: 0x0001F274
		public int Raycast(Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("Physics2D.AllLayers")] int layerMask, [DefaultValue("-Mathf.Infinity")] float minDepth, [DefaultValue("Mathf.Infinity")] float maxDepth)
		{
			return Collider2D.INTERNAL_CALL_Raycast(this, ref direction, results, distance, layerMask, minDepth, maxDepth);
		}

		// Token: 0x06001A6A RID: 6762 RVA: 0x0002109C File Offset: 0x0001F29C
		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Collider2D.INTERNAL_CALL_Raycast(this, ref direction, results, distance, layerMask, minDepth, positiveInfinity);
		}

		// Token: 0x06001A6B RID: 6763 RVA: 0x000210C8 File Offset: 0x0001F2C8
		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results, float distance, int layerMask)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			return Collider2D.INTERNAL_CALL_Raycast(this, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001A6C RID: 6764 RVA: 0x000210F8 File Offset: 0x0001F2F8
		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results, float distance)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -1;
			return Collider2D.INTERNAL_CALL_Raycast(this, ref direction, results, distance, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001A6D RID: 6765 RVA: 0x00021128 File Offset: 0x0001F328
		[ExcludeFromDocs]
		public int Raycast(Vector2 direction, RaycastHit2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			float negativeInfinity = float.NegativeInfinity;
			int layerMask = -1;
			float positiveInfinity2 = float.PositiveInfinity;
			return Collider2D.INTERNAL_CALL_Raycast(this, ref direction, results, positiveInfinity2, layerMask, negativeInfinity, positiveInfinity);
		}

		// Token: 0x06001A6E RID: 6766
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_Raycast(Collider2D self, ref Vector2 direction, RaycastHit2D[] results, float distance, int layerMask, float minDepth, float maxDepth);

		// Token: 0x06001A6F RID: 6767 RVA: 0x00021160 File Offset: 0x0001F360
		public int Cast(Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance, [DefaultValue("true")] bool ignoreSiblingColliders)
		{
			return Collider2D.INTERNAL_CALL_Cast(this, ref direction, results, distance, ignoreSiblingColliders);
		}

		// Token: 0x06001A70 RID: 6768 RVA: 0x00021184 File Offset: 0x0001F384
		[ExcludeFromDocs]
		public int Cast(Vector2 direction, RaycastHit2D[] results, float distance)
		{
			bool ignoreSiblingColliders = true;
			return Collider2D.INTERNAL_CALL_Cast(this, ref direction, results, distance, ignoreSiblingColliders);
		}

		// Token: 0x06001A71 RID: 6769 RVA: 0x000211A8 File Offset: 0x0001F3A8
		[ExcludeFromDocs]
		public int Cast(Vector2 direction, RaycastHit2D[] results)
		{
			bool ignoreSiblingColliders = true;
			float positiveInfinity = float.PositiveInfinity;
			return Collider2D.INTERNAL_CALL_Cast(this, ref direction, results, positiveInfinity, ignoreSiblingColliders);
		}

		// Token: 0x06001A72 RID: 6770
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_Cast(Collider2D self, ref Vector2 direction, RaycastHit2D[] results, float distance, bool ignoreSiblingColliders);
	}
}
