﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200014C RID: 332
	public sealed class ConfigurableJoint : Joint
	{
		// Token: 0x17000577 RID: 1399
		// (get) Token: 0x060017EA RID: 6122 RVA: 0x0001E4F4 File Offset: 0x0001C6F4
		// (set) Token: 0x060017EB RID: 6123 RVA: 0x0001E514 File Offset: 0x0001C714
		public Vector3 secondaryAxis
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_secondaryAxis(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_secondaryAxis(ref value);
			}
		}

		// Token: 0x060017EC RID: 6124
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_secondaryAxis(out Vector3 value);

		// Token: 0x060017ED RID: 6125
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_secondaryAxis(ref Vector3 value);

		// Token: 0x17000578 RID: 1400
		// (get) Token: 0x060017EE RID: 6126
		// (set) Token: 0x060017EF RID: 6127
		public extern ConfigurableJointMotion xMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000579 RID: 1401
		// (get) Token: 0x060017F0 RID: 6128
		// (set) Token: 0x060017F1 RID: 6129
		public extern ConfigurableJointMotion yMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700057A RID: 1402
		// (get) Token: 0x060017F2 RID: 6130
		// (set) Token: 0x060017F3 RID: 6131
		public extern ConfigurableJointMotion zMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700057B RID: 1403
		// (get) Token: 0x060017F4 RID: 6132
		// (set) Token: 0x060017F5 RID: 6133
		public extern ConfigurableJointMotion angularXMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700057C RID: 1404
		// (get) Token: 0x060017F6 RID: 6134
		// (set) Token: 0x060017F7 RID: 6135
		public extern ConfigurableJointMotion angularYMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700057D RID: 1405
		// (get) Token: 0x060017F8 RID: 6136
		// (set) Token: 0x060017F9 RID: 6137
		public extern ConfigurableJointMotion angularZMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700057E RID: 1406
		// (get) Token: 0x060017FA RID: 6138 RVA: 0x0001E520 File Offset: 0x0001C720
		// (set) Token: 0x060017FB RID: 6139 RVA: 0x0001E540 File Offset: 0x0001C740
		public SoftJointLimitSpring linearLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.INTERNAL_get_linearLimitSpring(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_linearLimitSpring(ref value);
			}
		}

		// Token: 0x060017FC RID: 6140
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_linearLimitSpring(out SoftJointLimitSpring value);

		// Token: 0x060017FD RID: 6141
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_linearLimitSpring(ref SoftJointLimitSpring value);

		// Token: 0x1700057F RID: 1407
		// (get) Token: 0x060017FE RID: 6142 RVA: 0x0001E54C File Offset: 0x0001C74C
		// (set) Token: 0x060017FF RID: 6143 RVA: 0x0001E56C File Offset: 0x0001C76C
		public SoftJointLimitSpring angularXLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.INTERNAL_get_angularXLimitSpring(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_angularXLimitSpring(ref value);
			}
		}

		// Token: 0x06001800 RID: 6144
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_angularXLimitSpring(out SoftJointLimitSpring value);

		// Token: 0x06001801 RID: 6145
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_angularXLimitSpring(ref SoftJointLimitSpring value);

		// Token: 0x17000580 RID: 1408
		// (get) Token: 0x06001802 RID: 6146 RVA: 0x0001E578 File Offset: 0x0001C778
		// (set) Token: 0x06001803 RID: 6147 RVA: 0x0001E598 File Offset: 0x0001C798
		public SoftJointLimitSpring angularYZLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.INTERNAL_get_angularYZLimitSpring(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_angularYZLimitSpring(ref value);
			}
		}

		// Token: 0x06001804 RID: 6148
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_angularYZLimitSpring(out SoftJointLimitSpring value);

		// Token: 0x06001805 RID: 6149
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_angularYZLimitSpring(ref SoftJointLimitSpring value);

		// Token: 0x17000581 RID: 1409
		// (get) Token: 0x06001806 RID: 6150 RVA: 0x0001E5A4 File Offset: 0x0001C7A4
		// (set) Token: 0x06001807 RID: 6151 RVA: 0x0001E5C4 File Offset: 0x0001C7C4
		public SoftJointLimit linearLimit
		{
			get
			{
				SoftJointLimit result;
				this.INTERNAL_get_linearLimit(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_linearLimit(ref value);
			}
		}

		// Token: 0x06001808 RID: 6152
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_linearLimit(out SoftJointLimit value);

		// Token: 0x06001809 RID: 6153
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_linearLimit(ref SoftJointLimit value);

		// Token: 0x17000582 RID: 1410
		// (get) Token: 0x0600180A RID: 6154 RVA: 0x0001E5D0 File Offset: 0x0001C7D0
		// (set) Token: 0x0600180B RID: 6155 RVA: 0x0001E5F0 File Offset: 0x0001C7F0
		public SoftJointLimit lowAngularXLimit
		{
			get
			{
				SoftJointLimit result;
				this.INTERNAL_get_lowAngularXLimit(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_lowAngularXLimit(ref value);
			}
		}

		// Token: 0x0600180C RID: 6156
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_lowAngularXLimit(out SoftJointLimit value);

		// Token: 0x0600180D RID: 6157
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_lowAngularXLimit(ref SoftJointLimit value);

		// Token: 0x17000583 RID: 1411
		// (get) Token: 0x0600180E RID: 6158 RVA: 0x0001E5FC File Offset: 0x0001C7FC
		// (set) Token: 0x0600180F RID: 6159 RVA: 0x0001E61C File Offset: 0x0001C81C
		public SoftJointLimit highAngularXLimit
		{
			get
			{
				SoftJointLimit result;
				this.INTERNAL_get_highAngularXLimit(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_highAngularXLimit(ref value);
			}
		}

		// Token: 0x06001810 RID: 6160
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_highAngularXLimit(out SoftJointLimit value);

		// Token: 0x06001811 RID: 6161
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_highAngularXLimit(ref SoftJointLimit value);

		// Token: 0x17000584 RID: 1412
		// (get) Token: 0x06001812 RID: 6162 RVA: 0x0001E628 File Offset: 0x0001C828
		// (set) Token: 0x06001813 RID: 6163 RVA: 0x0001E648 File Offset: 0x0001C848
		public SoftJointLimit angularYLimit
		{
			get
			{
				SoftJointLimit result;
				this.INTERNAL_get_angularYLimit(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_angularYLimit(ref value);
			}
		}

		// Token: 0x06001814 RID: 6164
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_angularYLimit(out SoftJointLimit value);

		// Token: 0x06001815 RID: 6165
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_angularYLimit(ref SoftJointLimit value);

		// Token: 0x17000585 RID: 1413
		// (get) Token: 0x06001816 RID: 6166 RVA: 0x0001E654 File Offset: 0x0001C854
		// (set) Token: 0x06001817 RID: 6167 RVA: 0x0001E674 File Offset: 0x0001C874
		public SoftJointLimit angularZLimit
		{
			get
			{
				SoftJointLimit result;
				this.INTERNAL_get_angularZLimit(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_angularZLimit(ref value);
			}
		}

		// Token: 0x06001818 RID: 6168
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_angularZLimit(out SoftJointLimit value);

		// Token: 0x06001819 RID: 6169
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_angularZLimit(ref SoftJointLimit value);

		// Token: 0x17000586 RID: 1414
		// (get) Token: 0x0600181A RID: 6170 RVA: 0x0001E680 File Offset: 0x0001C880
		// (set) Token: 0x0600181B RID: 6171 RVA: 0x0001E6A0 File Offset: 0x0001C8A0
		public Vector3 targetPosition
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_targetPosition(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_targetPosition(ref value);
			}
		}

		// Token: 0x0600181C RID: 6172
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_targetPosition(out Vector3 value);

		// Token: 0x0600181D RID: 6173
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_targetPosition(ref Vector3 value);

		// Token: 0x17000587 RID: 1415
		// (get) Token: 0x0600181E RID: 6174 RVA: 0x0001E6AC File Offset: 0x0001C8AC
		// (set) Token: 0x0600181F RID: 6175 RVA: 0x0001E6CC File Offset: 0x0001C8CC
		public Vector3 targetVelocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_targetVelocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_targetVelocity(ref value);
			}
		}

		// Token: 0x06001820 RID: 6176
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_targetVelocity(out Vector3 value);

		// Token: 0x06001821 RID: 6177
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_targetVelocity(ref Vector3 value);

		// Token: 0x17000588 RID: 1416
		// (get) Token: 0x06001822 RID: 6178 RVA: 0x0001E6D8 File Offset: 0x0001C8D8
		// (set) Token: 0x06001823 RID: 6179 RVA: 0x0001E6F8 File Offset: 0x0001C8F8
		public JointDrive xDrive
		{
			get
			{
				JointDrive result;
				this.INTERNAL_get_xDrive(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_xDrive(ref value);
			}
		}

		// Token: 0x06001824 RID: 6180
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_xDrive(out JointDrive value);

		// Token: 0x06001825 RID: 6181
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_xDrive(ref JointDrive value);

		// Token: 0x17000589 RID: 1417
		// (get) Token: 0x06001826 RID: 6182 RVA: 0x0001E704 File Offset: 0x0001C904
		// (set) Token: 0x06001827 RID: 6183 RVA: 0x0001E724 File Offset: 0x0001C924
		public JointDrive yDrive
		{
			get
			{
				JointDrive result;
				this.INTERNAL_get_yDrive(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_yDrive(ref value);
			}
		}

		// Token: 0x06001828 RID: 6184
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_yDrive(out JointDrive value);

		// Token: 0x06001829 RID: 6185
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_yDrive(ref JointDrive value);

		// Token: 0x1700058A RID: 1418
		// (get) Token: 0x0600182A RID: 6186 RVA: 0x0001E730 File Offset: 0x0001C930
		// (set) Token: 0x0600182B RID: 6187 RVA: 0x0001E750 File Offset: 0x0001C950
		public JointDrive zDrive
		{
			get
			{
				JointDrive result;
				this.INTERNAL_get_zDrive(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_zDrive(ref value);
			}
		}

		// Token: 0x0600182C RID: 6188
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_zDrive(out JointDrive value);

		// Token: 0x0600182D RID: 6189
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_zDrive(ref JointDrive value);

		// Token: 0x1700058B RID: 1419
		// (get) Token: 0x0600182E RID: 6190 RVA: 0x0001E75C File Offset: 0x0001C95C
		// (set) Token: 0x0600182F RID: 6191 RVA: 0x0001E77C File Offset: 0x0001C97C
		public Quaternion targetRotation
		{
			get
			{
				Quaternion result;
				this.INTERNAL_get_targetRotation(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_targetRotation(ref value);
			}
		}

		// Token: 0x06001830 RID: 6192
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_targetRotation(out Quaternion value);

		// Token: 0x06001831 RID: 6193
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_targetRotation(ref Quaternion value);

		// Token: 0x1700058C RID: 1420
		// (get) Token: 0x06001832 RID: 6194 RVA: 0x0001E788 File Offset: 0x0001C988
		// (set) Token: 0x06001833 RID: 6195 RVA: 0x0001E7A8 File Offset: 0x0001C9A8
		public Vector3 targetAngularVelocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_targetAngularVelocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_targetAngularVelocity(ref value);
			}
		}

		// Token: 0x06001834 RID: 6196
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_targetAngularVelocity(out Vector3 value);

		// Token: 0x06001835 RID: 6197
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_targetAngularVelocity(ref Vector3 value);

		// Token: 0x1700058D RID: 1421
		// (get) Token: 0x06001836 RID: 6198
		// (set) Token: 0x06001837 RID: 6199
		public extern RotationDriveMode rotationDriveMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700058E RID: 1422
		// (get) Token: 0x06001838 RID: 6200 RVA: 0x0001E7B4 File Offset: 0x0001C9B4
		// (set) Token: 0x06001839 RID: 6201 RVA: 0x0001E7D4 File Offset: 0x0001C9D4
		public JointDrive angularXDrive
		{
			get
			{
				JointDrive result;
				this.INTERNAL_get_angularXDrive(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_angularXDrive(ref value);
			}
		}

		// Token: 0x0600183A RID: 6202
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_angularXDrive(out JointDrive value);

		// Token: 0x0600183B RID: 6203
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_angularXDrive(ref JointDrive value);

		// Token: 0x1700058F RID: 1423
		// (get) Token: 0x0600183C RID: 6204 RVA: 0x0001E7E0 File Offset: 0x0001C9E0
		// (set) Token: 0x0600183D RID: 6205 RVA: 0x0001E800 File Offset: 0x0001CA00
		public JointDrive angularYZDrive
		{
			get
			{
				JointDrive result;
				this.INTERNAL_get_angularYZDrive(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_angularYZDrive(ref value);
			}
		}

		// Token: 0x0600183E RID: 6206
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_angularYZDrive(out JointDrive value);

		// Token: 0x0600183F RID: 6207
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_angularYZDrive(ref JointDrive value);

		// Token: 0x17000590 RID: 1424
		// (get) Token: 0x06001840 RID: 6208 RVA: 0x0001E80C File Offset: 0x0001CA0C
		// (set) Token: 0x06001841 RID: 6209 RVA: 0x0001E82C File Offset: 0x0001CA2C
		public JointDrive slerpDrive
		{
			get
			{
				JointDrive result;
				this.INTERNAL_get_slerpDrive(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_slerpDrive(ref value);
			}
		}

		// Token: 0x06001842 RID: 6210
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_slerpDrive(out JointDrive value);

		// Token: 0x06001843 RID: 6211
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_slerpDrive(ref JointDrive value);

		// Token: 0x17000591 RID: 1425
		// (get) Token: 0x06001844 RID: 6212
		// (set) Token: 0x06001845 RID: 6213
		public extern JointProjectionMode projectionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000592 RID: 1426
		// (get) Token: 0x06001846 RID: 6214
		// (set) Token: 0x06001847 RID: 6215
		public extern float projectionDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000593 RID: 1427
		// (get) Token: 0x06001848 RID: 6216
		// (set) Token: 0x06001849 RID: 6217
		public extern float projectionAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000594 RID: 1428
		// (get) Token: 0x0600184A RID: 6218
		// (set) Token: 0x0600184B RID: 6219
		public extern bool configuredInWorldSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000595 RID: 1429
		// (get) Token: 0x0600184C RID: 6220
		// (set) Token: 0x0600184D RID: 6221
		public extern bool swapBodies { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
