﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F5 RID: 757
	public enum BlendWeights
	{
		// Token: 0x04000B37 RID: 2871
		OneBone = 1,
		// Token: 0x04000B38 RID: 2872
		TwoBones,
		// Token: 0x04000B39 RID: 2873
		FourBones = 4
	}
}
