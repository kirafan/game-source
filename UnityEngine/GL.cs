﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x0200005C RID: 92
	public sealed class GL
	{
		// Token: 0x060006CD RID: 1741
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Vertex3(float x, float y, float z);

		// Token: 0x060006CE RID: 1742 RVA: 0x00009190 File Offset: 0x00007390
		public static void Vertex(Vector3 v)
		{
			GL.INTERNAL_CALL_Vertex(ref v);
		}

		// Token: 0x060006CF RID: 1743
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Vertex(ref Vector3 v);

		// Token: 0x060006D0 RID: 1744 RVA: 0x0000919C File Offset: 0x0000739C
		public static void Color(Color c)
		{
			GL.INTERNAL_CALL_Color(ref c);
		}

		// Token: 0x060006D1 RID: 1745
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Color(ref Color c);

		// Token: 0x060006D2 RID: 1746 RVA: 0x000091A8 File Offset: 0x000073A8
		public static void TexCoord(Vector3 v)
		{
			GL.INTERNAL_CALL_TexCoord(ref v);
		}

		// Token: 0x060006D3 RID: 1747
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_TexCoord(ref Vector3 v);

		// Token: 0x060006D4 RID: 1748
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void TexCoord2(float x, float y);

		// Token: 0x060006D5 RID: 1749
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void TexCoord3(float x, float y, float z);

		// Token: 0x060006D6 RID: 1750
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void MultiTexCoord2(int unit, float x, float y);

		// Token: 0x060006D7 RID: 1751
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void MultiTexCoord3(int unit, float x, float y, float z);

		// Token: 0x060006D8 RID: 1752 RVA: 0x000091B4 File Offset: 0x000073B4
		public static void MultiTexCoord(int unit, Vector3 v)
		{
			GL.INTERNAL_CALL_MultiTexCoord(unit, ref v);
		}

		// Token: 0x060006D9 RID: 1753
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_MultiTexCoord(int unit, ref Vector3 v);

		// Token: 0x060006DA RID: 1754
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void BeginInternal(int mode);

		// Token: 0x060006DB RID: 1755 RVA: 0x000091C0 File Offset: 0x000073C0
		public static void Begin(int mode)
		{
			GL.BeginInternal(mode);
		}

		// Token: 0x060006DC RID: 1756
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void End();

		// Token: 0x060006DD RID: 1757
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void LoadOrtho();

		// Token: 0x060006DE RID: 1758
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void LoadPixelMatrix();

		// Token: 0x060006DF RID: 1759
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void LoadPixelMatrixArgs(float left, float right, float bottom, float top);

		// Token: 0x060006E0 RID: 1760 RVA: 0x000091CC File Offset: 0x000073CC
		public static void LoadPixelMatrix(float left, float right, float bottom, float top)
		{
			GL.LoadPixelMatrixArgs(left, right, bottom, top);
		}

		// Token: 0x060006E1 RID: 1761 RVA: 0x000091D8 File Offset: 0x000073D8
		public static void Viewport(Rect pixelRect)
		{
			GL.INTERNAL_CALL_Viewport(ref pixelRect);
		}

		// Token: 0x060006E2 RID: 1762
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Viewport(ref Rect pixelRect);

		// Token: 0x060006E3 RID: 1763 RVA: 0x000091E4 File Offset: 0x000073E4
		public static void LoadProjectionMatrix(Matrix4x4 mat)
		{
			GL.INTERNAL_CALL_LoadProjectionMatrix(ref mat);
		}

		// Token: 0x060006E4 RID: 1764
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_LoadProjectionMatrix(ref Matrix4x4 mat);

		// Token: 0x060006E5 RID: 1765
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void LoadIdentity();

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x060006E6 RID: 1766 RVA: 0x000091F0 File Offset: 0x000073F0
		// (set) Token: 0x060006E7 RID: 1767 RVA: 0x00009210 File Offset: 0x00007410
		public static Matrix4x4 modelview
		{
			get
			{
				Matrix4x4 result;
				GL.INTERNAL_get_modelview(out result);
				return result;
			}
			set
			{
				GL.INTERNAL_set_modelview(ref value);
			}
		}

		// Token: 0x060006E8 RID: 1768
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_modelview(out Matrix4x4 value);

		// Token: 0x060006E9 RID: 1769
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_modelview(ref Matrix4x4 value);

		// Token: 0x060006EA RID: 1770 RVA: 0x0000921C File Offset: 0x0000741C
		public static void MultMatrix(Matrix4x4 mat)
		{
			GL.INTERNAL_CALL_MultMatrix(ref mat);
		}

		// Token: 0x060006EB RID: 1771
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_MultMatrix(ref Matrix4x4 mat);

		// Token: 0x060006EC RID: 1772
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void PushMatrix();

		// Token: 0x060006ED RID: 1773
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void PopMatrix();

		// Token: 0x060006EE RID: 1774 RVA: 0x00009228 File Offset: 0x00007428
		public static Matrix4x4 GetGPUProjectionMatrix(Matrix4x4 proj, bool renderIntoTexture)
		{
			Matrix4x4 result;
			GL.INTERNAL_CALL_GetGPUProjectionMatrix(ref proj, renderIntoTexture, out result);
			return result;
		}

		// Token: 0x060006EF RID: 1775
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetGPUProjectionMatrix(ref Matrix4x4 proj, bool renderIntoTexture, out Matrix4x4 value);

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x060006F0 RID: 1776
		// (set) Token: 0x060006F1 RID: 1777
		public static extern bool wireframe { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x060006F2 RID: 1778
		// (set) Token: 0x060006F3 RID: 1779
		public static extern bool sRGBWrite { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x060006F4 RID: 1780
		// (set) Token: 0x060006F5 RID: 1781
		public static extern bool invertCulling { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060006F6 RID: 1782
		[Obsolete("Use invertCulling property")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetRevertBackfacing(bool revertBackFaces);

		// Token: 0x060006F7 RID: 1783 RVA: 0x00009248 File Offset: 0x00007448
		[ExcludeFromDocs]
		public static void Clear(bool clearDepth, bool clearColor, Color backgroundColor)
		{
			float depth = 1f;
			GL.Clear(clearDepth, clearColor, backgroundColor, depth);
		}

		// Token: 0x060006F8 RID: 1784 RVA: 0x00009268 File Offset: 0x00007468
		public static void Clear(bool clearDepth, bool clearColor, Color backgroundColor, [DefaultValue("1.0f")] float depth)
		{
			GL.Internal_Clear(clearDepth, clearColor, backgroundColor, depth);
		}

		// Token: 0x060006F9 RID: 1785 RVA: 0x00009274 File Offset: 0x00007474
		private static void Internal_Clear(bool clearDepth, bool clearColor, Color backgroundColor, float depth)
		{
			GL.INTERNAL_CALL_Internal_Clear(clearDepth, clearColor, ref backgroundColor, depth);
		}

		// Token: 0x060006FA RID: 1786
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_Clear(bool clearDepth, bool clearColor, ref Color backgroundColor, float depth);

		// Token: 0x060006FB RID: 1787
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ClearWithSkybox(bool clearDepth, Camera camera);

		// Token: 0x060006FC RID: 1788
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Flush();

		// Token: 0x060006FD RID: 1789
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void InvalidateState();

		// Token: 0x060006FE RID: 1790
		[Obsolete("IssuePluginEvent(eventID) is deprecated. Use IssuePluginEvent(callback, eventID) instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void IssuePluginEvent(int eventID);

		// Token: 0x060006FF RID: 1791 RVA: 0x00009284 File Offset: 0x00007484
		public static void IssuePluginEvent(IntPtr callback, int eventID)
		{
			if (callback == IntPtr.Zero)
			{
				throw new ArgumentException("Null callback specified.");
			}
			GL.IssuePluginEventInternal(callback, eventID);
		}

		// Token: 0x06000700 RID: 1792
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void IssuePluginEventInternal(IntPtr callback, int eventID);

		// Token: 0x06000701 RID: 1793
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void RenderTargetBarrier();

		// Token: 0x04000090 RID: 144
		public const int TRIANGLES = 4;

		// Token: 0x04000091 RID: 145
		public const int TRIANGLE_STRIP = 5;

		// Token: 0x04000092 RID: 146
		public const int QUADS = 7;

		// Token: 0x04000093 RID: 147
		public const int LINES = 1;
	}
}
