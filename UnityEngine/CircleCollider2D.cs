﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000162 RID: 354
	public sealed class CircleCollider2D : Collider2D
	{
		// Token: 0x17000614 RID: 1556
		// (get) Token: 0x06001A74 RID: 6772
		// (set) Token: 0x06001A75 RID: 6773
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
