﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200014F RID: 335
	public class Collider : Component
	{
		// Token: 0x1700059A RID: 1434
		// (get) Token: 0x06001860 RID: 6240
		// (set) Token: 0x06001861 RID: 6241
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700059B RID: 1435
		// (get) Token: 0x06001862 RID: 6242
		public extern Rigidbody attachedRigidbody { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700059C RID: 1436
		// (get) Token: 0x06001863 RID: 6243
		// (set) Token: 0x06001864 RID: 6244
		public extern bool isTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700059D RID: 1437
		// (get) Token: 0x06001865 RID: 6245
		// (set) Token: 0x06001866 RID: 6246
		public extern float contactOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700059E RID: 1438
		// (get) Token: 0x06001867 RID: 6247
		// (set) Token: 0x06001868 RID: 6248
		public extern PhysicMaterial material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001869 RID: 6249 RVA: 0x0001E8F8 File Offset: 0x0001CAF8
		public Vector3 ClosestPointOnBounds(Vector3 position)
		{
			Vector3 result;
			Collider.INTERNAL_CALL_ClosestPointOnBounds(this, ref position, out result);
			return result;
		}

		// Token: 0x0600186A RID: 6250
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ClosestPointOnBounds(Collider self, ref Vector3 position, out Vector3 value);

		// Token: 0x1700059F RID: 1439
		// (get) Token: 0x0600186B RID: 6251
		// (set) Token: 0x0600186C RID: 6252
		public extern PhysicMaterial sharedMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005A0 RID: 1440
		// (get) Token: 0x0600186D RID: 6253 RVA: 0x0001E918 File Offset: 0x0001CB18
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_bounds(out result);
				return result;
			}
		}

		// Token: 0x0600186E RID: 6254
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_bounds(out Bounds value);

		// Token: 0x0600186F RID: 6255 RVA: 0x0001E938 File Offset: 0x0001CB38
		private static bool Internal_Raycast(Collider col, Ray ray, out RaycastHit hitInfo, float maxDistance)
		{
			return Collider.INTERNAL_CALL_Internal_Raycast(col, ref ray, out hitInfo, maxDistance);
		}

		// Token: 0x06001870 RID: 6256
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Internal_Raycast(Collider col, ref Ray ray, out RaycastHit hitInfo, float maxDistance);

		// Token: 0x06001871 RID: 6257 RVA: 0x0001E958 File Offset: 0x0001CB58
		public bool Raycast(Ray ray, out RaycastHit hitInfo, float maxDistance)
		{
			return Collider.Internal_Raycast(this, ray, out hitInfo, maxDistance);
		}
	}
}
