﻿using System;

namespace UnityEngine
{
	// Token: 0x0200006F RID: 111
	public struct Touch
	{
		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x06000764 RID: 1892 RVA: 0x0000988C File Offset: 0x00007A8C
		// (set) Token: 0x06000765 RID: 1893 RVA: 0x000098A8 File Offset: 0x00007AA8
		public int fingerId
		{
			get
			{
				return this.m_FingerId;
			}
			set
			{
				this.m_FingerId = value;
			}
		}

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x06000766 RID: 1894 RVA: 0x000098B4 File Offset: 0x00007AB4
		// (set) Token: 0x06000767 RID: 1895 RVA: 0x000098D0 File Offset: 0x00007AD0
		public Vector2 position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x06000768 RID: 1896 RVA: 0x000098DC File Offset: 0x00007ADC
		// (set) Token: 0x06000769 RID: 1897 RVA: 0x000098F8 File Offset: 0x00007AF8
		public Vector2 rawPosition
		{
			get
			{
				return this.m_RawPosition;
			}
			set
			{
				this.m_RawPosition = value;
			}
		}

		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x0600076A RID: 1898 RVA: 0x00009904 File Offset: 0x00007B04
		// (set) Token: 0x0600076B RID: 1899 RVA: 0x00009920 File Offset: 0x00007B20
		public Vector2 deltaPosition
		{
			get
			{
				return this.m_PositionDelta;
			}
			set
			{
				this.m_PositionDelta = value;
			}
		}

		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x0600076C RID: 1900 RVA: 0x0000992C File Offset: 0x00007B2C
		// (set) Token: 0x0600076D RID: 1901 RVA: 0x00009948 File Offset: 0x00007B48
		public float deltaTime
		{
			get
			{
				return this.m_TimeDelta;
			}
			set
			{
				this.m_TimeDelta = value;
			}
		}

		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x0600076E RID: 1902 RVA: 0x00009954 File Offset: 0x00007B54
		// (set) Token: 0x0600076F RID: 1903 RVA: 0x00009970 File Offset: 0x00007B70
		public int tapCount
		{
			get
			{
				return this.m_TapCount;
			}
			set
			{
				this.m_TapCount = value;
			}
		}

		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x06000770 RID: 1904 RVA: 0x0000997C File Offset: 0x00007B7C
		// (set) Token: 0x06000771 RID: 1905 RVA: 0x00009998 File Offset: 0x00007B98
		public TouchPhase phase
		{
			get
			{
				return this.m_Phase;
			}
			set
			{
				this.m_Phase = value;
			}
		}

		// Token: 0x170001AA RID: 426
		// (get) Token: 0x06000772 RID: 1906 RVA: 0x000099A4 File Offset: 0x00007BA4
		// (set) Token: 0x06000773 RID: 1907 RVA: 0x000099C0 File Offset: 0x00007BC0
		public float pressure
		{
			get
			{
				return this.m_Pressure;
			}
			set
			{
				this.m_Pressure = value;
			}
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x06000774 RID: 1908 RVA: 0x000099CC File Offset: 0x00007BCC
		// (set) Token: 0x06000775 RID: 1909 RVA: 0x000099E8 File Offset: 0x00007BE8
		public float maximumPossiblePressure
		{
			get
			{
				return this.m_maximumPossiblePressure;
			}
			set
			{
				this.m_maximumPossiblePressure = value;
			}
		}

		// Token: 0x170001AC RID: 428
		// (get) Token: 0x06000776 RID: 1910 RVA: 0x000099F4 File Offset: 0x00007BF4
		// (set) Token: 0x06000777 RID: 1911 RVA: 0x00009A10 File Offset: 0x00007C10
		public TouchType type
		{
			get
			{
				return this.m_Type;
			}
			set
			{
				this.m_Type = value;
			}
		}

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x06000778 RID: 1912 RVA: 0x00009A1C File Offset: 0x00007C1C
		// (set) Token: 0x06000779 RID: 1913 RVA: 0x00009A38 File Offset: 0x00007C38
		public float altitudeAngle
		{
			get
			{
				return this.m_AltitudeAngle;
			}
			set
			{
				this.m_AltitudeAngle = value;
			}
		}

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x0600077A RID: 1914 RVA: 0x00009A44 File Offset: 0x00007C44
		// (set) Token: 0x0600077B RID: 1915 RVA: 0x00009A60 File Offset: 0x00007C60
		public float azimuthAngle
		{
			get
			{
				return this.m_AzimuthAngle;
			}
			set
			{
				this.m_AzimuthAngle = value;
			}
		}

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x0600077C RID: 1916 RVA: 0x00009A6C File Offset: 0x00007C6C
		// (set) Token: 0x0600077D RID: 1917 RVA: 0x00009A88 File Offset: 0x00007C88
		public float radius
		{
			get
			{
				return this.m_Radius;
			}
			set
			{
				this.m_Radius = value;
			}
		}

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x0600077E RID: 1918 RVA: 0x00009A94 File Offset: 0x00007C94
		// (set) Token: 0x0600077F RID: 1919 RVA: 0x00009AB0 File Offset: 0x00007CB0
		public float radiusVariance
		{
			get
			{
				return this.m_RadiusVariance;
			}
			set
			{
				this.m_RadiusVariance = value;
			}
		}

		// Token: 0x040000BA RID: 186
		private int m_FingerId;

		// Token: 0x040000BB RID: 187
		private Vector2 m_Position;

		// Token: 0x040000BC RID: 188
		private Vector2 m_RawPosition;

		// Token: 0x040000BD RID: 189
		private Vector2 m_PositionDelta;

		// Token: 0x040000BE RID: 190
		private float m_TimeDelta;

		// Token: 0x040000BF RID: 191
		private int m_TapCount;

		// Token: 0x040000C0 RID: 192
		private TouchPhase m_Phase;

		// Token: 0x040000C1 RID: 193
		private TouchType m_Type;

		// Token: 0x040000C2 RID: 194
		private float m_Pressure;

		// Token: 0x040000C3 RID: 195
		private float m_maximumPossiblePressure;

		// Token: 0x040000C4 RID: 196
		private float m_Radius;

		// Token: 0x040000C5 RID: 197
		private float m_RadiusVariance;

		// Token: 0x040000C6 RID: 198
		private float m_AltitudeAngle;

		// Token: 0x040000C7 RID: 199
		private float m_AzimuthAngle;
	}
}
