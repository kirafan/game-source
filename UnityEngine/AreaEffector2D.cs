﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000180 RID: 384
	public sealed class AreaEffector2D : Effector2D
	{
		// Token: 0x17000674 RID: 1652
		// (get) Token: 0x06001B5E RID: 7006
		// (set) Token: 0x06001B5F RID: 7007
		public extern float forceAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000675 RID: 1653
		// (get) Token: 0x06001B60 RID: 7008
		// (set) Token: 0x06001B61 RID: 7009
		public extern bool useGlobalAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000676 RID: 1654
		// (get) Token: 0x06001B62 RID: 7010
		// (set) Token: 0x06001B63 RID: 7011
		public extern float forceMagnitude { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000677 RID: 1655
		// (get) Token: 0x06001B64 RID: 7012
		// (set) Token: 0x06001B65 RID: 7013
		public extern float forceVariation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000678 RID: 1656
		// (get) Token: 0x06001B66 RID: 7014
		// (set) Token: 0x06001B67 RID: 7015
		public extern float drag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000679 RID: 1657
		// (get) Token: 0x06001B68 RID: 7016
		// (set) Token: 0x06001B69 RID: 7017
		public extern float angularDrag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700067A RID: 1658
		// (get) Token: 0x06001B6A RID: 7018
		// (set) Token: 0x06001B6B RID: 7019
		public extern EffectorSelection2D forceTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
