﻿using System;

namespace UnityEngine
{
	// Token: 0x02000106 RID: 262
	public enum ParticleSystemScalingMode
	{
		// Token: 0x040002D3 RID: 723
		Hierarchy,
		// Token: 0x040002D4 RID: 724
		Local,
		// Token: 0x040002D5 RID: 725
		Shape
	}
}
