﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020002DE RID: 734
	internal class EnumInfo
	{
		// Token: 0x06002C90 RID: 11408 RVA: 0x0004644C File Offset: 0x0004464C
		[UsedByNativeCode]
		internal static EnumInfo CreateEnumInfoFromNativeEnum(string[] names, int[] values, string[] annotations, bool isFlags)
		{
			return new EnumInfo
			{
				names = names,
				values = values,
				annotations = annotations,
				isFlags = isFlags
			};
		}

		// Token: 0x04000ACA RID: 2762
		public string[] names;

		// Token: 0x04000ACB RID: 2763
		public int[] values;

		// Token: 0x04000ACC RID: 2764
		public string[] annotations;

		// Token: 0x04000ACD RID: 2765
		public bool isFlags;
	}
}
