﻿using System;

namespace UnityEngine
{
	// Token: 0x020000F7 RID: 247
	public enum ParticleSystemRenderMode
	{
		// Token: 0x04000286 RID: 646
		Billboard,
		// Token: 0x04000287 RID: 647
		Stretch,
		// Token: 0x04000288 RID: 648
		HorizontalBillboard,
		// Token: 0x04000289 RID: 649
		VerticalBillboard,
		// Token: 0x0400028A RID: 650
		Mesh,
		// Token: 0x0400028B RID: 651
		None
	}
}
