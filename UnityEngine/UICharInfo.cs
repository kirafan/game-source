﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000207 RID: 519
	[UsedByNativeCode]
	public struct UICharInfo
	{
		// Token: 0x0400062E RID: 1582
		public Vector2 cursorPos;

		// Token: 0x0400062F RID: 1583
		public float charWidth;
	}
}
