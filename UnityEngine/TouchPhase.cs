﻿using System;

namespace UnityEngine
{
	// Token: 0x0200006C RID: 108
	public enum TouchPhase
	{
		// Token: 0x040000AD RID: 173
		Began,
		// Token: 0x040000AE RID: 174
		Moved,
		// Token: 0x040000AF RID: 175
		Stationary,
		// Token: 0x040000B0 RID: 176
		Ended,
		// Token: 0x040000B1 RID: 177
		Canceled
	}
}
