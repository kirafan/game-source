﻿using System;

namespace UnityEngine
{
	// Token: 0x02000101 RID: 257
	public enum ParticleSystemCollisionType
	{
		// Token: 0x040002C2 RID: 706
		Planes,
		// Token: 0x040002C3 RID: 707
		World
	}
}
