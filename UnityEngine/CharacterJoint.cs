﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000149 RID: 329
	public sealed class CharacterJoint : Joint
	{
		// Token: 0x1700056D RID: 1389
		// (get) Token: 0x060017C7 RID: 6087 RVA: 0x0001E3B8 File Offset: 0x0001C5B8
		// (set) Token: 0x060017C8 RID: 6088 RVA: 0x0001E3D8 File Offset: 0x0001C5D8
		public Vector3 swingAxis
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_swingAxis(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_swingAxis(ref value);
			}
		}

		// Token: 0x060017C9 RID: 6089
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_swingAxis(out Vector3 value);

		// Token: 0x060017CA RID: 6090
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_swingAxis(ref Vector3 value);

		// Token: 0x1700056E RID: 1390
		// (get) Token: 0x060017CB RID: 6091 RVA: 0x0001E3E4 File Offset: 0x0001C5E4
		// (set) Token: 0x060017CC RID: 6092 RVA: 0x0001E404 File Offset: 0x0001C604
		public SoftJointLimitSpring twistLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.INTERNAL_get_twistLimitSpring(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_twistLimitSpring(ref value);
			}
		}

		// Token: 0x060017CD RID: 6093
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_twistLimitSpring(out SoftJointLimitSpring value);

		// Token: 0x060017CE RID: 6094
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_twistLimitSpring(ref SoftJointLimitSpring value);

		// Token: 0x1700056F RID: 1391
		// (get) Token: 0x060017CF RID: 6095 RVA: 0x0001E410 File Offset: 0x0001C610
		// (set) Token: 0x060017D0 RID: 6096 RVA: 0x0001E430 File Offset: 0x0001C630
		public SoftJointLimitSpring swingLimitSpring
		{
			get
			{
				SoftJointLimitSpring result;
				this.INTERNAL_get_swingLimitSpring(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_swingLimitSpring(ref value);
			}
		}

		// Token: 0x060017D1 RID: 6097
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_swingLimitSpring(out SoftJointLimitSpring value);

		// Token: 0x060017D2 RID: 6098
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_swingLimitSpring(ref SoftJointLimitSpring value);

		// Token: 0x17000570 RID: 1392
		// (get) Token: 0x060017D3 RID: 6099 RVA: 0x0001E43C File Offset: 0x0001C63C
		// (set) Token: 0x060017D4 RID: 6100 RVA: 0x0001E45C File Offset: 0x0001C65C
		public SoftJointLimit lowTwistLimit
		{
			get
			{
				SoftJointLimit result;
				this.INTERNAL_get_lowTwistLimit(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_lowTwistLimit(ref value);
			}
		}

		// Token: 0x060017D5 RID: 6101
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_lowTwistLimit(out SoftJointLimit value);

		// Token: 0x060017D6 RID: 6102
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_lowTwistLimit(ref SoftJointLimit value);

		// Token: 0x17000571 RID: 1393
		// (get) Token: 0x060017D7 RID: 6103 RVA: 0x0001E468 File Offset: 0x0001C668
		// (set) Token: 0x060017D8 RID: 6104 RVA: 0x0001E488 File Offset: 0x0001C688
		public SoftJointLimit highTwistLimit
		{
			get
			{
				SoftJointLimit result;
				this.INTERNAL_get_highTwistLimit(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_highTwistLimit(ref value);
			}
		}

		// Token: 0x060017D9 RID: 6105
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_highTwistLimit(out SoftJointLimit value);

		// Token: 0x060017DA RID: 6106
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_highTwistLimit(ref SoftJointLimit value);

		// Token: 0x17000572 RID: 1394
		// (get) Token: 0x060017DB RID: 6107 RVA: 0x0001E494 File Offset: 0x0001C694
		// (set) Token: 0x060017DC RID: 6108 RVA: 0x0001E4B4 File Offset: 0x0001C6B4
		public SoftJointLimit swing1Limit
		{
			get
			{
				SoftJointLimit result;
				this.INTERNAL_get_swing1Limit(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_swing1Limit(ref value);
			}
		}

		// Token: 0x060017DD RID: 6109
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_swing1Limit(out SoftJointLimit value);

		// Token: 0x060017DE RID: 6110
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_swing1Limit(ref SoftJointLimit value);

		// Token: 0x17000573 RID: 1395
		// (get) Token: 0x060017DF RID: 6111 RVA: 0x0001E4C0 File Offset: 0x0001C6C0
		// (set) Token: 0x060017E0 RID: 6112 RVA: 0x0001E4E0 File Offset: 0x0001C6E0
		public SoftJointLimit swing2Limit
		{
			get
			{
				SoftJointLimit result;
				this.INTERNAL_get_swing2Limit(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_swing2Limit(ref value);
			}
		}

		// Token: 0x060017E1 RID: 6113
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_swing2Limit(out SoftJointLimit value);

		// Token: 0x060017E2 RID: 6114
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_swing2Limit(ref SoftJointLimit value);

		// Token: 0x17000574 RID: 1396
		// (get) Token: 0x060017E3 RID: 6115
		// (set) Token: 0x060017E4 RID: 6116
		public extern bool enableProjection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000575 RID: 1397
		// (get) Token: 0x060017E5 RID: 6117
		// (set) Token: 0x060017E6 RID: 6118
		public extern float projectionDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000576 RID: 1398
		// (get) Token: 0x060017E7 RID: 6119
		// (set) Token: 0x060017E8 RID: 6120
		public extern float projectionAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x040003AC RID: 940
		[Obsolete("TargetRotation not in use for Unity 5 and assumed disabled.", true)]
		public Quaternion targetRotation;

		// Token: 0x040003AD RID: 941
		[Obsolete("TargetAngularVelocity not in use for Unity 5 and assumed disabled.", true)]
		public Vector3 targetAngularVelocity;

		// Token: 0x040003AE RID: 942
		[Obsolete("RotationDrive not in use for Unity 5 and assumed disabled.", true)]
		public JointDrive rotationDrive;
	}
}
