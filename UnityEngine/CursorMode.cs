﻿using System;

namespace UnityEngine
{
	// Token: 0x02000031 RID: 49
	public enum CursorMode
	{
		// Token: 0x0400004E RID: 78
		Auto,
		// Token: 0x0400004F RID: 79
		ForceSoftware
	}
}
