﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000072 RID: 114
	public sealed class Gyroscope
	{
		// Token: 0x06000782 RID: 1922 RVA: 0x00009B04 File Offset: 0x00007D04
		internal Gyroscope(int index)
		{
			this.m_GyroIndex = index;
		}

		// Token: 0x06000783 RID: 1923 RVA: 0x00009B14 File Offset: 0x00007D14
		private static Vector3 rotationRate_Internal(int idx)
		{
			Vector3 result;
			Gyroscope.INTERNAL_CALL_rotationRate_Internal(idx, out result);
			return result;
		}

		// Token: 0x06000784 RID: 1924
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_rotationRate_Internal(int idx, out Vector3 value);

		// Token: 0x06000785 RID: 1925 RVA: 0x00009B34 File Offset: 0x00007D34
		private static Vector3 rotationRateUnbiased_Internal(int idx)
		{
			Vector3 result;
			Gyroscope.INTERNAL_CALL_rotationRateUnbiased_Internal(idx, out result);
			return result;
		}

		// Token: 0x06000786 RID: 1926
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_rotationRateUnbiased_Internal(int idx, out Vector3 value);

		// Token: 0x06000787 RID: 1927 RVA: 0x00009B54 File Offset: 0x00007D54
		private static Vector3 gravity_Internal(int idx)
		{
			Vector3 result;
			Gyroscope.INTERNAL_CALL_gravity_Internal(idx, out result);
			return result;
		}

		// Token: 0x06000788 RID: 1928
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_gravity_Internal(int idx, out Vector3 value);

		// Token: 0x06000789 RID: 1929 RVA: 0x00009B74 File Offset: 0x00007D74
		private static Vector3 userAcceleration_Internal(int idx)
		{
			Vector3 result;
			Gyroscope.INTERNAL_CALL_userAcceleration_Internal(idx, out result);
			return result;
		}

		// Token: 0x0600078A RID: 1930
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_userAcceleration_Internal(int idx, out Vector3 value);

		// Token: 0x0600078B RID: 1931 RVA: 0x00009B94 File Offset: 0x00007D94
		private static Quaternion attitude_Internal(int idx)
		{
			Quaternion result;
			Gyroscope.INTERNAL_CALL_attitude_Internal(idx, out result);
			return result;
		}

		// Token: 0x0600078C RID: 1932
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_attitude_Internal(int idx, out Quaternion value);

		// Token: 0x0600078D RID: 1933
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool getEnabled_Internal(int idx);

		// Token: 0x0600078E RID: 1934
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void setEnabled_Internal(int idx, bool enabled);

		// Token: 0x0600078F RID: 1935
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float getUpdateInterval_Internal(int idx);

		// Token: 0x06000790 RID: 1936
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void setUpdateInterval_Internal(int idx, float interval);

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x06000791 RID: 1937 RVA: 0x00009BB4 File Offset: 0x00007DB4
		public Vector3 rotationRate
		{
			get
			{
				return Gyroscope.rotationRate_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06000792 RID: 1938 RVA: 0x00009BD4 File Offset: 0x00007DD4
		public Vector3 rotationRateUnbiased
		{
			get
			{
				return Gyroscope.rotationRateUnbiased_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06000793 RID: 1939 RVA: 0x00009BF4 File Offset: 0x00007DF4
		public Vector3 gravity
		{
			get
			{
				return Gyroscope.gravity_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06000794 RID: 1940 RVA: 0x00009C14 File Offset: 0x00007E14
		public Vector3 userAcceleration
		{
			get
			{
				return Gyroscope.userAcceleration_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000795 RID: 1941 RVA: 0x00009C34 File Offset: 0x00007E34
		public Quaternion attitude
		{
			get
			{
				return Gyroscope.attitude_Internal(this.m_GyroIndex);
			}
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x06000796 RID: 1942 RVA: 0x00009C54 File Offset: 0x00007E54
		// (set) Token: 0x06000797 RID: 1943 RVA: 0x00009C74 File Offset: 0x00007E74
		public bool enabled
		{
			get
			{
				return Gyroscope.getEnabled_Internal(this.m_GyroIndex);
			}
			set
			{
				Gyroscope.setEnabled_Internal(this.m_GyroIndex, value);
			}
		}

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x06000798 RID: 1944 RVA: 0x00009C84 File Offset: 0x00007E84
		// (set) Token: 0x06000799 RID: 1945 RVA: 0x00009CA4 File Offset: 0x00007EA4
		public float updateInterval
		{
			get
			{
				return Gyroscope.getUpdateInterval_Internal(this.m_GyroIndex);
			}
			set
			{
				Gyroscope.setUpdateInterval_Internal(this.m_GyroIndex, value);
			}
		}

		// Token: 0x040000D4 RID: 212
		private int m_GyroIndex;
	}
}
