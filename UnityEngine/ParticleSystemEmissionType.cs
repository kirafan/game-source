﻿using System;

namespace UnityEngine
{
	// Token: 0x020000FB RID: 251
	[Obsolete("ParticleSystemEmissionType no longer does anything. Time and Distance based emission are now both always active.")]
	public enum ParticleSystemEmissionType
	{
		// Token: 0x0400029B RID: 667
		Time,
		// Token: 0x0400029C RID: 668
		Distance
	}
}
