﻿using System;

namespace UnityEngine
{
	// Token: 0x020002EE RID: 750
	public enum ShadowProjection
	{
		// Token: 0x04000B15 RID: 2837
		CloseFit,
		// Token: 0x04000B16 RID: 2838
		StableFit
	}
}
