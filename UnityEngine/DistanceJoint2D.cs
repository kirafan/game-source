﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000172 RID: 370
	public sealed class DistanceJoint2D : AnchoredJoint2D
	{
		// Token: 0x1700063E RID: 1598
		// (get) Token: 0x06001AD6 RID: 6870
		// (set) Token: 0x06001AD7 RID: 6871
		public extern bool autoConfigureDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700063F RID: 1599
		// (get) Token: 0x06001AD8 RID: 6872
		// (set) Token: 0x06001AD9 RID: 6873
		public extern float distance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000640 RID: 1600
		// (get) Token: 0x06001ADA RID: 6874
		// (set) Token: 0x06001ADB RID: 6875
		public extern bool maxDistanceOnly { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
