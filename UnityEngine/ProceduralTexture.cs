﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000C9 RID: 201
	public sealed class ProceduralTexture : Texture
	{
		// Token: 0x06000DB1 RID: 3505
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern ProceduralOutputType GetProceduralOutputType();

		// Token: 0x06000DB2 RID: 3506
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern ProceduralMaterial GetProceduralMaterial();

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x06000DB3 RID: 3507
		public extern bool hasAlpha { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000DB4 RID: 3508
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool HasBeenGenerated();

		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06000DB5 RID: 3509
		public extern TextureFormat format { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000DB6 RID: 3510
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color32[] GetPixels32(int x, int y, int blockWidth, int blockHeight);
	}
}
