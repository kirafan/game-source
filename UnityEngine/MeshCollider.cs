﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000152 RID: 338
	public sealed class MeshCollider : Collider
	{
		// Token: 0x170005A6 RID: 1446
		// (get) Token: 0x06001885 RID: 6277
		// (set) Token: 0x06001886 RID: 6278
		public extern Mesh sharedMesh { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005A7 RID: 1447
		// (get) Token: 0x06001887 RID: 6279
		// (set) Token: 0x06001888 RID: 6280
		public extern bool convex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005A8 RID: 1448
		// (get) Token: 0x06001889 RID: 6281
		// (set) Token: 0x0600188A RID: 6282
		public extern bool inflateMesh { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005A9 RID: 1449
		// (get) Token: 0x0600188B RID: 6283
		// (set) Token: 0x0600188C RID: 6284
		public extern float skinWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005AA RID: 1450
		// (get) Token: 0x0600188D RID: 6285 RVA: 0x0001EA50 File Offset: 0x0001CC50
		// (set) Token: 0x0600188E RID: 6286 RVA: 0x0001EA68 File Offset: 0x0001CC68
		[Obsolete("Configuring smooth sphere collisions is no longer needed. PhysX3 has a better behaviour in place.")]
		public bool smoothSphereCollisions
		{
			get
			{
				return true;
			}
			set
			{
			}
		}
	}
}
