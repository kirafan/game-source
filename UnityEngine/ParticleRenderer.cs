﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020002AD RID: 685
	[Obsolete("This component is part of the legacy particle system, which is deprecated and will be removed in a future release. Use the ParticleSystem component instead.", false)]
	public sealed class ParticleRenderer : Renderer
	{
		// Token: 0x17000A5A RID: 2650
		// (get) Token: 0x06002B80 RID: 11136
		// (set) Token: 0x06002B81 RID: 11137
		public extern ParticleRenderMode particleRenderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A5B RID: 2651
		// (get) Token: 0x06002B82 RID: 11138
		// (set) Token: 0x06002B83 RID: 11139
		public extern float lengthScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A5C RID: 2652
		// (get) Token: 0x06002B84 RID: 11140
		// (set) Token: 0x06002B85 RID: 11141
		public extern float velocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A5D RID: 2653
		// (get) Token: 0x06002B86 RID: 11142
		// (set) Token: 0x06002B87 RID: 11143
		public extern float cameraVelocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A5E RID: 2654
		// (get) Token: 0x06002B88 RID: 11144
		// (set) Token: 0x06002B89 RID: 11145
		public extern float maxParticleSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A5F RID: 2655
		// (get) Token: 0x06002B8A RID: 11146
		// (set) Token: 0x06002B8B RID: 11147
		public extern int uvAnimationXTile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A60 RID: 2656
		// (get) Token: 0x06002B8C RID: 11148
		// (set) Token: 0x06002B8D RID: 11149
		public extern int uvAnimationYTile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A61 RID: 2657
		// (get) Token: 0x06002B8E RID: 11150
		// (set) Token: 0x06002B8F RID: 11151
		public extern float uvAnimationCycles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A62 RID: 2658
		// (get) Token: 0x06002B90 RID: 11152 RVA: 0x00041BBC File Offset: 0x0003FDBC
		// (set) Token: 0x06002B91 RID: 11153 RVA: 0x00041BD8 File Offset: 0x0003FDD8
		[Obsolete("animatedTextureCount has been replaced by uvAnimationXTile and uvAnimationYTile.")]
		public int animatedTextureCount
		{
			get
			{
				return this.uvAnimationXTile;
			}
			set
			{
				this.uvAnimationXTile = value;
			}
		}

		// Token: 0x17000A63 RID: 2659
		// (get) Token: 0x06002B92 RID: 11154 RVA: 0x00041BE4 File Offset: 0x0003FDE4
		// (set) Token: 0x06002B93 RID: 11155 RVA: 0x00041C00 File Offset: 0x0003FE00
		public float maxPartileSize
		{
			get
			{
				return this.maxParticleSize;
			}
			set
			{
				this.maxParticleSize = value;
			}
		}

		// Token: 0x17000A64 RID: 2660
		// (get) Token: 0x06002B94 RID: 11156
		// (set) Token: 0x06002B95 RID: 11157
		public extern Rect[] uvTiles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A65 RID: 2661
		// (get) Token: 0x06002B96 RID: 11158 RVA: 0x00041C0C File Offset: 0x0003FE0C
		// (set) Token: 0x06002B97 RID: 11159 RVA: 0x00041C24 File Offset: 0x0003FE24
		[Obsolete("This function has been removed.", true)]
		public AnimationCurve widthCurve
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x17000A66 RID: 2662
		// (get) Token: 0x06002B98 RID: 11160 RVA: 0x00041C28 File Offset: 0x0003FE28
		// (set) Token: 0x06002B99 RID: 11161 RVA: 0x00041C40 File Offset: 0x0003FE40
		[Obsolete("This function has been removed.", true)]
		public AnimationCurve heightCurve
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x17000A67 RID: 2663
		// (get) Token: 0x06002B9A RID: 11162 RVA: 0x00041C44 File Offset: 0x0003FE44
		// (set) Token: 0x06002B9B RID: 11163 RVA: 0x00041C5C File Offset: 0x0003FE5C
		[Obsolete("This function has been removed.", true)]
		public AnimationCurve rotationCurve
		{
			get
			{
				return null;
			}
			set
			{
			}
		}
	}
}
