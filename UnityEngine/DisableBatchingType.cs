﻿using System;

namespace UnityEngine
{
	// Token: 0x020000B3 RID: 179
	internal enum DisableBatchingType
	{
		// Token: 0x040001A1 RID: 417
		False,
		// Token: 0x040001A2 RID: 418
		True,
		// Token: 0x040001A3 RID: 419
		WhenLODFading
	}
}
