﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200023C RID: 572
	internal sealed class GUIClip
	{
		// Token: 0x060026DA RID: 9946 RVA: 0x00034D14 File Offset: 0x00032F14
		public static Vector2 Unclip(Vector2 pos)
		{
			GUIClip.Unclip_Vector2(ref pos);
			return pos;
		}

		// Token: 0x060026DB RID: 9947 RVA: 0x00034D34 File Offset: 0x00032F34
		public static Rect Unclip(Rect rect)
		{
			GUIClip.Unclip_Rect(ref rect);
			return rect;
		}

		// Token: 0x060026DC RID: 9948 RVA: 0x00034D54 File Offset: 0x00032F54
		public static Vector2 Clip(Vector2 absolutePos)
		{
			GUIClip.Clip_Vector2(ref absolutePos);
			return absolutePos;
		}

		// Token: 0x060026DD RID: 9949 RVA: 0x00034D74 File Offset: 0x00032F74
		public static Rect Clip(Rect absoluteRect)
		{
			GUIClip.Internal_Clip_Rect(ref absoluteRect);
			return absoluteRect;
		}

		// Token: 0x060026DE RID: 9950 RVA: 0x00034D94 File Offset: 0x00032F94
		public static Vector2 GetAbsoluteMousePosition()
		{
			Vector2 result;
			GUIClip.Internal_GetAbsoluteMousePosition(out result);
			return result;
		}

		// Token: 0x060026DF RID: 9951 RVA: 0x00034DB4 File Offset: 0x00032FB4
		internal static void Push(Rect screenRect, Vector2 scrollOffset, Vector2 renderOffset, bool resetOffset)
		{
			GUIClip.INTERNAL_CALL_Push(ref screenRect, ref scrollOffset, ref renderOffset, resetOffset);
		}

		// Token: 0x060026E0 RID: 9952
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Push(ref Rect screenRect, ref Vector2 scrollOffset, ref Vector2 renderOffset, bool resetOffset);

		// Token: 0x060026E1 RID: 9953
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Pop();

		// Token: 0x060026E2 RID: 9954 RVA: 0x00034DC4 File Offset: 0x00032FC4
		internal static Rect GetTopRect()
		{
			Rect result;
			GUIClip.INTERNAL_CALL_GetTopRect(out result);
			return result;
		}

		// Token: 0x060026E3 RID: 9955
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetTopRect(out Rect value);

		// Token: 0x17000964 RID: 2404
		// (get) Token: 0x060026E4 RID: 9956
		public static extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060026E5 RID: 9957 RVA: 0x00034DE4 File Offset: 0x00032FE4
		private static void Unclip_Vector2(ref Vector2 pos)
		{
			GUIClip.INTERNAL_CALL_Unclip_Vector2(ref pos);
		}

		// Token: 0x060026E6 RID: 9958
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Unclip_Vector2(ref Vector2 pos);

		// Token: 0x17000965 RID: 2405
		// (get) Token: 0x060026E7 RID: 9959 RVA: 0x00034DF0 File Offset: 0x00032FF0
		public static Rect topmostRect
		{
			get
			{
				Rect result;
				GUIClip.INTERNAL_get_topmostRect(out result);
				return result;
			}
		}

		// Token: 0x060026E8 RID: 9960
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_topmostRect(out Rect value);

		// Token: 0x060026E9 RID: 9961 RVA: 0x00034E10 File Offset: 0x00033010
		private static void Unclip_Rect(ref Rect rect)
		{
			GUIClip.INTERNAL_CALL_Unclip_Rect(ref rect);
		}

		// Token: 0x060026EA RID: 9962
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Unclip_Rect(ref Rect rect);

		// Token: 0x060026EB RID: 9963 RVA: 0x00034E1C File Offset: 0x0003301C
		private static void Clip_Vector2(ref Vector2 absolutePos)
		{
			GUIClip.INTERNAL_CALL_Clip_Vector2(ref absolutePos);
		}

		// Token: 0x060026EC RID: 9964
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Clip_Vector2(ref Vector2 absolutePos);

		// Token: 0x060026ED RID: 9965 RVA: 0x00034E28 File Offset: 0x00033028
		private static void Internal_Clip_Rect(ref Rect absoluteRect)
		{
			GUIClip.INTERNAL_CALL_Internal_Clip_Rect(ref absoluteRect);
		}

		// Token: 0x060026EE RID: 9966
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_Clip_Rect(ref Rect absoluteRect);

		// Token: 0x060026EF RID: 9967
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Reapply();

		// Token: 0x060026F0 RID: 9968 RVA: 0x00034E34 File Offset: 0x00033034
		internal static Matrix4x4 GetMatrix()
		{
			Matrix4x4 result;
			GUIClip.INTERNAL_CALL_GetMatrix(out result);
			return result;
		}

		// Token: 0x060026F1 RID: 9969
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetMatrix(out Matrix4x4 value);

		// Token: 0x060026F2 RID: 9970 RVA: 0x00034E54 File Offset: 0x00033054
		internal static void SetMatrix(Matrix4x4 m)
		{
			GUIClip.INTERNAL_CALL_SetMatrix(ref m);
		}

		// Token: 0x060026F3 RID: 9971
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetMatrix(ref Matrix4x4 m);

		// Token: 0x17000966 RID: 2406
		// (get) Token: 0x060026F4 RID: 9972 RVA: 0x00034E60 File Offset: 0x00033060
		public static Rect visibleRect
		{
			get
			{
				Rect result;
				GUIClip.INTERNAL_get_visibleRect(out result);
				return result;
			}
		}

		// Token: 0x060026F5 RID: 9973
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_visibleRect(out Rect value);

		// Token: 0x060026F6 RID: 9974
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_GetAbsoluteMousePosition(out Vector2 output);
	}
}
