﻿using System;

namespace UnityEngine
{
	// Token: 0x02000135 RID: 309
	public struct WheelFrictionCurve
	{
		// Token: 0x170004F7 RID: 1271
		// (get) Token: 0x060015F4 RID: 5620 RVA: 0x0001BEE8 File Offset: 0x0001A0E8
		// (set) Token: 0x060015F5 RID: 5621 RVA: 0x0001BF04 File Offset: 0x0001A104
		public float extremumSlip
		{
			get
			{
				return this.m_ExtremumSlip;
			}
			set
			{
				this.m_ExtremumSlip = value;
			}
		}

		// Token: 0x170004F8 RID: 1272
		// (get) Token: 0x060015F6 RID: 5622 RVA: 0x0001BF10 File Offset: 0x0001A110
		// (set) Token: 0x060015F7 RID: 5623 RVA: 0x0001BF2C File Offset: 0x0001A12C
		public float extremumValue
		{
			get
			{
				return this.m_ExtremumValue;
			}
			set
			{
				this.m_ExtremumValue = value;
			}
		}

		// Token: 0x170004F9 RID: 1273
		// (get) Token: 0x060015F8 RID: 5624 RVA: 0x0001BF38 File Offset: 0x0001A138
		// (set) Token: 0x060015F9 RID: 5625 RVA: 0x0001BF54 File Offset: 0x0001A154
		public float asymptoteSlip
		{
			get
			{
				return this.m_AsymptoteSlip;
			}
			set
			{
				this.m_AsymptoteSlip = value;
			}
		}

		// Token: 0x170004FA RID: 1274
		// (get) Token: 0x060015FA RID: 5626 RVA: 0x0001BF60 File Offset: 0x0001A160
		// (set) Token: 0x060015FB RID: 5627 RVA: 0x0001BF7C File Offset: 0x0001A17C
		public float asymptoteValue
		{
			get
			{
				return this.m_AsymptoteValue;
			}
			set
			{
				this.m_AsymptoteValue = value;
			}
		}

		// Token: 0x170004FB RID: 1275
		// (get) Token: 0x060015FC RID: 5628 RVA: 0x0001BF88 File Offset: 0x0001A188
		// (set) Token: 0x060015FD RID: 5629 RVA: 0x0001BFA4 File Offset: 0x0001A1A4
		public float stiffness
		{
			get
			{
				return this.m_Stiffness;
			}
			set
			{
				this.m_Stiffness = value;
			}
		}

		// Token: 0x04000366 RID: 870
		private float m_ExtremumSlip;

		// Token: 0x04000367 RID: 871
		private float m_ExtremumValue;

		// Token: 0x04000368 RID: 872
		private float m_AsymptoteSlip;

		// Token: 0x04000369 RID: 873
		private float m_AsymptoteValue;

		// Token: 0x0400036A RID: 874
		private float m_Stiffness;
	}
}
