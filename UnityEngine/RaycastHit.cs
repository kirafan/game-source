﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000154 RID: 340
	[UsedByNativeCode]
	public struct RaycastHit
	{
		// Token: 0x0600189A RID: 6298 RVA: 0x0001EAA0 File Offset: 0x0001CCA0
		private static void CalculateRaycastTexCoord(out Vector2 output, Collider col, Vector2 uv, Vector3 point, int face, int index)
		{
			RaycastHit.INTERNAL_CALL_CalculateRaycastTexCoord(out output, col, ref uv, ref point, face, index);
		}

		// Token: 0x0600189B RID: 6299
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_CalculateRaycastTexCoord(out Vector2 output, Collider col, ref Vector2 uv, ref Vector3 point, int face, int index);

		// Token: 0x170005AF RID: 1455
		// (get) Token: 0x0600189C RID: 6300 RVA: 0x0001EAB4 File Offset: 0x0001CCB4
		// (set) Token: 0x0600189D RID: 6301 RVA: 0x0001EAD0 File Offset: 0x0001CCD0
		public Vector3 point
		{
			get
			{
				return this.m_Point;
			}
			set
			{
				this.m_Point = value;
			}
		}

		// Token: 0x170005B0 RID: 1456
		// (get) Token: 0x0600189E RID: 6302 RVA: 0x0001EADC File Offset: 0x0001CCDC
		// (set) Token: 0x0600189F RID: 6303 RVA: 0x0001EAF8 File Offset: 0x0001CCF8
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
			set
			{
				this.m_Normal = value;
			}
		}

		// Token: 0x170005B1 RID: 1457
		// (get) Token: 0x060018A0 RID: 6304 RVA: 0x0001EB04 File Offset: 0x0001CD04
		// (set) Token: 0x060018A1 RID: 6305 RVA: 0x0001EB54 File Offset: 0x0001CD54
		public Vector3 barycentricCoordinate
		{
			get
			{
				return new Vector3(1f - (this.m_UV.y + this.m_UV.x), this.m_UV.x, this.m_UV.y);
			}
			set
			{
				this.m_UV = value;
			}
		}

		// Token: 0x170005B2 RID: 1458
		// (get) Token: 0x060018A2 RID: 6306 RVA: 0x0001EB64 File Offset: 0x0001CD64
		// (set) Token: 0x060018A3 RID: 6307 RVA: 0x0001EB80 File Offset: 0x0001CD80
		public float distance
		{
			get
			{
				return this.m_Distance;
			}
			set
			{
				this.m_Distance = value;
			}
		}

		// Token: 0x170005B3 RID: 1459
		// (get) Token: 0x060018A4 RID: 6308 RVA: 0x0001EB8C File Offset: 0x0001CD8C
		public int triangleIndex
		{
			get
			{
				return this.m_FaceID;
			}
		}

		// Token: 0x170005B4 RID: 1460
		// (get) Token: 0x060018A5 RID: 6309 RVA: 0x0001EBA8 File Offset: 0x0001CDA8
		public Vector2 textureCoord
		{
			get
			{
				Vector2 result;
				RaycastHit.CalculateRaycastTexCoord(out result, this.collider, this.m_UV, this.m_Point, this.m_FaceID, 0);
				return result;
			}
		}

		// Token: 0x170005B5 RID: 1461
		// (get) Token: 0x060018A6 RID: 6310 RVA: 0x0001EBE0 File Offset: 0x0001CDE0
		public Vector2 textureCoord2
		{
			get
			{
				Vector2 result;
				RaycastHit.CalculateRaycastTexCoord(out result, this.collider, this.m_UV, this.m_Point, this.m_FaceID, 1);
				return result;
			}
		}

		// Token: 0x170005B6 RID: 1462
		// (get) Token: 0x060018A7 RID: 6311 RVA: 0x0001EC18 File Offset: 0x0001CE18
		[Obsolete("Use textureCoord2 instead")]
		public Vector2 textureCoord1
		{
			get
			{
				Vector2 result;
				RaycastHit.CalculateRaycastTexCoord(out result, this.collider, this.m_UV, this.m_Point, this.m_FaceID, 1);
				return result;
			}
		}

		// Token: 0x170005B7 RID: 1463
		// (get) Token: 0x060018A8 RID: 6312 RVA: 0x0001EC50 File Offset: 0x0001CE50
		public Vector2 lightmapCoord
		{
			get
			{
				Vector2 result;
				RaycastHit.CalculateRaycastTexCoord(out result, this.collider, this.m_UV, this.m_Point, this.m_FaceID, 1);
				if (this.collider.GetComponent<Renderer>() != null)
				{
					Vector4 lightmapScaleOffset = this.collider.GetComponent<Renderer>().lightmapScaleOffset;
					result.x = result.x * lightmapScaleOffset.x + lightmapScaleOffset.z;
					result.y = result.y * lightmapScaleOffset.y + lightmapScaleOffset.w;
				}
				return result;
			}
		}

		// Token: 0x170005B8 RID: 1464
		// (get) Token: 0x060018A9 RID: 6313 RVA: 0x0001ECEC File Offset: 0x0001CEEC
		public Collider collider
		{
			get
			{
				return this.m_Collider;
			}
		}

		// Token: 0x170005B9 RID: 1465
		// (get) Token: 0x060018AA RID: 6314 RVA: 0x0001ED08 File Offset: 0x0001CF08
		public Rigidbody rigidbody
		{
			get
			{
				return (!(this.collider != null)) ? null : this.collider.attachedRigidbody;
			}
		}

		// Token: 0x170005BA RID: 1466
		// (get) Token: 0x060018AB RID: 6315 RVA: 0x0001ED40 File Offset: 0x0001CF40
		public Transform transform
		{
			get
			{
				Rigidbody rigidbody = this.rigidbody;
				Transform result;
				if (rigidbody != null)
				{
					result = rigidbody.transform;
				}
				else if (this.collider != null)
				{
					result = this.collider.transform;
				}
				else
				{
					result = null;
				}
				return result;
			}
		}

		// Token: 0x040003BA RID: 954
		private Vector3 m_Point;

		// Token: 0x040003BB RID: 955
		private Vector3 m_Normal;

		// Token: 0x040003BC RID: 956
		private int m_FaceID;

		// Token: 0x040003BD RID: 957
		private float m_Distance;

		// Token: 0x040003BE RID: 958
		private Vector2 m_UV;

		// Token: 0x040003BF RID: 959
		private Collider m_Collider;
	}
}
