﻿using System;

namespace UnityEngine
{
	// Token: 0x02000226 RID: 550
	public sealed class GUILayoutOption
	{
		// Token: 0x06002570 RID: 9584 RVA: 0x0002F780 File Offset: 0x0002D980
		internal GUILayoutOption(GUILayoutOption.Type type, object value)
		{
			this.type = type;
			this.value = value;
		}

		// Token: 0x040007DD RID: 2013
		internal GUILayoutOption.Type type;

		// Token: 0x040007DE RID: 2014
		internal object value;

		// Token: 0x02000227 RID: 551
		internal enum Type
		{
			// Token: 0x040007E0 RID: 2016
			fixedWidth,
			// Token: 0x040007E1 RID: 2017
			fixedHeight,
			// Token: 0x040007E2 RID: 2018
			minWidth,
			// Token: 0x040007E3 RID: 2019
			maxWidth,
			// Token: 0x040007E4 RID: 2020
			minHeight,
			// Token: 0x040007E5 RID: 2021
			maxHeight,
			// Token: 0x040007E6 RID: 2022
			stretchWidth,
			// Token: 0x040007E7 RID: 2023
			stretchHeight,
			// Token: 0x040007E8 RID: 2024
			alignStart,
			// Token: 0x040007E9 RID: 2025
			alignMiddle,
			// Token: 0x040007EA RID: 2026
			alignEnd,
			// Token: 0x040007EB RID: 2027
			alignJustify,
			// Token: 0x040007EC RID: 2028
			equalSize,
			// Token: 0x040007ED RID: 2029
			spacing
		}
	}
}
