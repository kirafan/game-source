﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200017F RID: 383
	public class Effector2D : Behaviour
	{
		// Token: 0x1700066F RID: 1647
		// (get) Token: 0x06001B56 RID: 6998
		// (set) Token: 0x06001B57 RID: 6999
		public extern bool useColliderMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000670 RID: 1648
		// (get) Token: 0x06001B58 RID: 7000
		// (set) Token: 0x06001B59 RID: 7001
		public extern int colliderMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000671 RID: 1649
		// (get) Token: 0x06001B5A RID: 7002
		internal extern bool requiresCollider { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000672 RID: 1650
		// (get) Token: 0x06001B5B RID: 7003
		internal extern bool designedForTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000673 RID: 1651
		// (get) Token: 0x06001B5C RID: 7004
		internal extern bool designedForNonTrigger { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
