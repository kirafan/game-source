﻿using System;

namespace UnityEngine
{
	// Token: 0x020002B5 RID: 693
	public enum StackTraceLogType
	{
		// Token: 0x04000A35 RID: 2613
		None,
		// Token: 0x04000A36 RID: 2614
		ScriptOnly,
		// Token: 0x04000A37 RID: 2615
		Full
	}
}
