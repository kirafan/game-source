﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000171 RID: 369
	public sealed class SpringJoint2D : AnchoredJoint2D
	{
		// Token: 0x1700063A RID: 1594
		// (get) Token: 0x06001ACD RID: 6861
		// (set) Token: 0x06001ACE RID: 6862
		public extern bool autoConfigureDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700063B RID: 1595
		// (get) Token: 0x06001ACF RID: 6863
		// (set) Token: 0x06001AD0 RID: 6864
		public extern float distance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700063C RID: 1596
		// (get) Token: 0x06001AD1 RID: 6865
		// (set) Token: 0x06001AD2 RID: 6866
		public extern float dampingRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700063D RID: 1597
		// (get) Token: 0x06001AD3 RID: 6867
		// (set) Token: 0x06001AD4 RID: 6868
		public extern float frequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
