﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000C0 RID: 192
	public sealed class SpriteRenderer : Renderer
	{
		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x06000D6A RID: 3434 RVA: 0x00012FC8 File Offset: 0x000111C8
		// (set) Token: 0x06000D6B RID: 3435 RVA: 0x00012FE4 File Offset: 0x000111E4
		public Sprite sprite
		{
			get
			{
				return this.GetSprite_INTERNAL();
			}
			set
			{
				this.SetSprite_INTERNAL(value);
			}
		}

		// Token: 0x06000D6C RID: 3436
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Sprite GetSprite_INTERNAL();

		// Token: 0x06000D6D RID: 3437
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetSprite_INTERNAL(Sprite sprite);

		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06000D6E RID: 3438 RVA: 0x00012FF0 File Offset: 0x000111F0
		// (set) Token: 0x06000D6F RID: 3439 RVA: 0x00013010 File Offset: 0x00011210
		public Color color
		{
			get
			{
				Color result;
				this.INTERNAL_get_color(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_color(ref value);
			}
		}

		// Token: 0x06000D70 RID: 3440
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_color(out Color value);

		// Token: 0x06000D71 RID: 3441
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_color(ref Color value);

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06000D72 RID: 3442
		// (set) Token: 0x06000D73 RID: 3443
		public extern bool flipX { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x06000D74 RID: 3444
		// (set) Token: 0x06000D75 RID: 3445
		public extern bool flipY { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000D76 RID: 3446 RVA: 0x0001301C File Offset: 0x0001121C
		internal Bounds GetSpriteBounds()
		{
			Bounds result;
			SpriteRenderer.INTERNAL_CALL_GetSpriteBounds(this, out result);
			return result;
		}

		// Token: 0x06000D77 RID: 3447
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetSpriteBounds(SpriteRenderer self, out Bounds value);
	}
}
