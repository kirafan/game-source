﻿using System;

namespace UnityEngine
{
	// Token: 0x020002D5 RID: 725
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
	internal class CppInvokeAttribute : Attribute
	{
	}
}
