﻿using System;

namespace UnityEngine
{
	// Token: 0x0200015C RID: 348
	public enum ForceMode2D
	{
		// Token: 0x040003D7 RID: 983
		Force,
		// Token: 0x040003D8 RID: 984
		Impulse
	}
}
