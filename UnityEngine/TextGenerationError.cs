﻿using System;

namespace UnityEngine
{
	// Token: 0x020001FB RID: 507
	[Flags]
	internal enum TextGenerationError
	{
		// Token: 0x040005F0 RID: 1520
		None = 0,
		// Token: 0x040005F1 RID: 1521
		CustomSizeOnNonDynamicFont = 1,
		// Token: 0x040005F2 RID: 1522
		CustomStyleOnNonDynamicFont = 2,
		// Token: 0x040005F3 RID: 1523
		NoFont = 4
	}
}
