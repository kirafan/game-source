﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200003C RID: 60
	[UsedByNativeCode]
	public struct GradientAlphaKey
	{
		// Token: 0x06000465 RID: 1125 RVA: 0x000073A0 File Offset: 0x000055A0
		public GradientAlphaKey(float alpha, float time)
		{
			this.alpha = alpha;
			this.time = time;
		}

		// Token: 0x0400005B RID: 91
		public float alpha;

		// Token: 0x0400005C RID: 92
		public float time;
	}
}
