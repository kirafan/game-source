﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035B RID: 859
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = true)]
	public class HeaderAttribute : PropertyAttribute
	{
		// Token: 0x06002DDD RID: 11741 RVA: 0x00048E6C File Offset: 0x0004706C
		public HeaderAttribute(string header)
		{
			this.header = header;
		}

		// Token: 0x04000D2A RID: 3370
		public readonly string header;
	}
}
