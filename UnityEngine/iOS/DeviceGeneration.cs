﻿using System;

namespace UnityEngine.iOS
{
	// Token: 0x020000EB RID: 235
	public enum DeviceGeneration
	{
		// Token: 0x0400024B RID: 587
		Unknown,
		// Token: 0x0400024C RID: 588
		iPhone,
		// Token: 0x0400024D RID: 589
		iPhone3G,
		// Token: 0x0400024E RID: 590
		iPhone3GS,
		// Token: 0x0400024F RID: 591
		iPodTouch1Gen,
		// Token: 0x04000250 RID: 592
		iPodTouch2Gen,
		// Token: 0x04000251 RID: 593
		iPodTouch3Gen,
		// Token: 0x04000252 RID: 594
		iPad1Gen,
		// Token: 0x04000253 RID: 595
		iPhone4,
		// Token: 0x04000254 RID: 596
		iPodTouch4Gen,
		// Token: 0x04000255 RID: 597
		iPad2Gen,
		// Token: 0x04000256 RID: 598
		iPhone4S,
		// Token: 0x04000257 RID: 599
		iPad3Gen,
		// Token: 0x04000258 RID: 600
		iPhone5,
		// Token: 0x04000259 RID: 601
		iPodTouch5Gen,
		// Token: 0x0400025A RID: 602
		iPadMini1Gen,
		// Token: 0x0400025B RID: 603
		iPad4Gen,
		// Token: 0x0400025C RID: 604
		iPhone5C,
		// Token: 0x0400025D RID: 605
		iPhone5S,
		// Token: 0x0400025E RID: 606
		iPadAir1,
		// Token: 0x0400025F RID: 607
		iPadMini2Gen,
		// Token: 0x04000260 RID: 608
		iPhone6,
		// Token: 0x04000261 RID: 609
		iPhone6Plus,
		// Token: 0x04000262 RID: 610
		iPadMini3Gen,
		// Token: 0x04000263 RID: 611
		iPadAir2,
		// Token: 0x04000264 RID: 612
		iPhone6S,
		// Token: 0x04000265 RID: 613
		iPhone6SPlus,
		// Token: 0x04000266 RID: 614
		iPadPro1Gen,
		// Token: 0x04000267 RID: 615
		iPadMini4Gen,
		// Token: 0x04000268 RID: 616
		iPhoneSE1Gen,
		// Token: 0x04000269 RID: 617
		iPadPro10Inch1Gen,
		// Token: 0x0400026A RID: 618
		iPhone7,
		// Token: 0x0400026B RID: 619
		iPhone7Plus,
		// Token: 0x0400026C RID: 620
		iPodTouch6Gen,
		// Token: 0x0400026D RID: 621
		iPad5Gen,
		// Token: 0x0400026E RID: 622
		iPhoneUnknown = 10001,
		// Token: 0x0400026F RID: 623
		iPadUnknown,
		// Token: 0x04000270 RID: 624
		iPodTouchUnknown
	}
}
