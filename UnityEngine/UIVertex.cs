﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000209 RID: 521
	[UsedByNativeCode]
	public struct UIVertex
	{
		// Token: 0x04000633 RID: 1587
		public Vector3 position;

		// Token: 0x04000634 RID: 1588
		public Vector3 normal;

		// Token: 0x04000635 RID: 1589
		public Color32 color;

		// Token: 0x04000636 RID: 1590
		public Vector2 uv0;

		// Token: 0x04000637 RID: 1591
		public Vector2 uv1;

		// Token: 0x04000638 RID: 1592
		public Vector4 tangent;

		// Token: 0x04000639 RID: 1593
		private static readonly Color32 s_DefaultColor = new Color32(byte.MaxValue, byte.MaxValue, byte.MaxValue, byte.MaxValue);

		// Token: 0x0400063A RID: 1594
		private static readonly Vector4 s_DefaultTangent = new Vector4(1f, 0f, 0f, -1f);

		// Token: 0x0400063B RID: 1595
		public static UIVertex simpleVert = new UIVertex
		{
			position = Vector3.zero,
			normal = Vector3.back,
			tangent = UIVertex.s_DefaultTangent,
			color = UIVertex.s_DefaultColor,
			uv0 = Vector2.zero,
			uv1 = Vector2.zero
		};
	}
}
