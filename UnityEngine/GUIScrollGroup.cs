﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000229 RID: 553
	internal sealed class GUIScrollGroup : GUILayoutGroup
	{
		// Token: 0x06002580 RID: 9600 RVA: 0x00030F30 File Offset: 0x0002F130
		[RequiredByNativeCode]
		public GUIScrollGroup()
		{
		}

		// Token: 0x06002581 RID: 9601 RVA: 0x00030F48 File Offset: 0x0002F148
		public override void CalcWidth()
		{
			float minWidth = this.minWidth;
			float maxWidth = this.maxWidth;
			if (this.allowHorizontalScroll)
			{
				this.minWidth = 0f;
				this.maxWidth = 0f;
			}
			base.CalcWidth();
			this.calcMinWidth = this.minWidth;
			this.calcMaxWidth = this.maxWidth;
			if (this.allowHorizontalScroll)
			{
				if (this.minWidth > 32f)
				{
					this.minWidth = 32f;
				}
				if (minWidth != 0f)
				{
					this.minWidth = minWidth;
				}
				if (maxWidth != 0f)
				{
					this.maxWidth = maxWidth;
					this.stretchWidth = 0;
				}
			}
		}

		// Token: 0x06002582 RID: 9602 RVA: 0x00030FFC File Offset: 0x0002F1FC
		public override void SetHorizontal(float x, float width)
		{
			float num = (!this.needsVerticalScrollbar) ? width : (width - this.verticalScrollbar.fixedWidth - (float)this.verticalScrollbar.margin.left);
			if (this.allowHorizontalScroll && num < this.calcMinWidth)
			{
				this.needsHorizontalScrollbar = true;
				this.minWidth = this.calcMinWidth;
				this.maxWidth = this.calcMaxWidth;
				base.SetHorizontal(x, this.calcMinWidth);
				this.rect.width = width;
				this.clientWidth = this.calcMinWidth;
			}
			else
			{
				this.needsHorizontalScrollbar = false;
				if (this.allowHorizontalScroll)
				{
					this.minWidth = this.calcMinWidth;
					this.maxWidth = this.calcMaxWidth;
				}
				base.SetHorizontal(x, num);
				this.rect.width = width;
				this.clientWidth = num;
			}
		}

		// Token: 0x06002583 RID: 9603 RVA: 0x000310E8 File Offset: 0x0002F2E8
		public override void CalcHeight()
		{
			float minHeight = this.minHeight;
			float maxHeight = this.maxHeight;
			if (this.allowVerticalScroll)
			{
				this.minHeight = 0f;
				this.maxHeight = 0f;
			}
			base.CalcHeight();
			this.calcMinHeight = this.minHeight;
			this.calcMaxHeight = this.maxHeight;
			if (this.needsHorizontalScrollbar)
			{
				float num = this.horizontalScrollbar.fixedHeight + (float)this.horizontalScrollbar.margin.top;
				this.minHeight += num;
				this.maxHeight += num;
			}
			if (this.allowVerticalScroll)
			{
				if (this.minHeight > 32f)
				{
					this.minHeight = 32f;
				}
				if (minHeight != 0f)
				{
					this.minHeight = minHeight;
				}
				if (maxHeight != 0f)
				{
					this.maxHeight = maxHeight;
					this.stretchHeight = 0;
				}
			}
		}

		// Token: 0x06002584 RID: 9604 RVA: 0x000311E4 File Offset: 0x0002F3E4
		public override void SetVertical(float y, float height)
		{
			float num = height;
			if (this.needsHorizontalScrollbar)
			{
				num -= this.horizontalScrollbar.fixedHeight + (float)this.horizontalScrollbar.margin.top;
			}
			if (this.allowVerticalScroll && num < this.calcMinHeight)
			{
				if (!this.needsHorizontalScrollbar && !this.needsVerticalScrollbar)
				{
					this.clientWidth = this.rect.width - this.verticalScrollbar.fixedWidth - (float)this.verticalScrollbar.margin.left;
					if (this.clientWidth < this.calcMinWidth)
					{
						this.clientWidth = this.calcMinWidth;
					}
					float width = this.rect.width;
					this.SetHorizontal(this.rect.x, this.clientWidth);
					this.CalcHeight();
					this.rect.width = width;
				}
				float minHeight = this.minHeight;
				float maxHeight = this.maxHeight;
				this.minHeight = this.calcMinHeight;
				this.maxHeight = this.calcMaxHeight;
				base.SetVertical(y, this.calcMinHeight);
				this.minHeight = minHeight;
				this.maxHeight = maxHeight;
				this.rect.height = height;
				this.clientHeight = this.calcMinHeight;
			}
			else
			{
				if (this.allowVerticalScroll)
				{
					this.minHeight = this.calcMinHeight;
					this.maxHeight = this.calcMaxHeight;
				}
				base.SetVertical(y, num);
				this.rect.height = height;
				this.clientHeight = num;
			}
		}

		// Token: 0x040007FF RID: 2047
		public float calcMinWidth;

		// Token: 0x04000800 RID: 2048
		public float calcMaxWidth;

		// Token: 0x04000801 RID: 2049
		public float calcMinHeight;

		// Token: 0x04000802 RID: 2050
		public float calcMaxHeight;

		// Token: 0x04000803 RID: 2051
		public float clientWidth;

		// Token: 0x04000804 RID: 2052
		public float clientHeight;

		// Token: 0x04000805 RID: 2053
		public bool allowHorizontalScroll = true;

		// Token: 0x04000806 RID: 2054
		public bool allowVerticalScroll = true;

		// Token: 0x04000807 RID: 2055
		public bool needsHorizontalScrollbar;

		// Token: 0x04000808 RID: 2056
		public bool needsVerticalScrollbar;

		// Token: 0x04000809 RID: 2057
		public GUIStyle horizontalScrollbar;

		// Token: 0x0400080A RID: 2058
		public GUIStyle verticalScrollbar;
	}
}
