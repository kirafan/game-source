﻿using System;

namespace UnityEngine
{
	// Token: 0x020001F5 RID: 501
	[Flags]
	public enum TerrainChangedFlags
	{
		// Token: 0x040005DF RID: 1503
		Heightmap = 1,
		// Token: 0x040005E0 RID: 1504
		TreeInstances = 2,
		// Token: 0x040005E1 RID: 1505
		DelayedHeightmapUpdate = 4,
		// Token: 0x040005E2 RID: 1506
		FlushEverythingImmediately = 8,
		// Token: 0x040005E3 RID: 1507
		RemoveDirtyDetailsImmediately = 16,
		// Token: 0x040005E4 RID: 1508
		WillBeDestroyed = 256
	}
}
