﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000202 RID: 514
	public sealed class GUIText : GUIElement
	{
		// Token: 0x17000898 RID: 2200
		// (get) Token: 0x060022A9 RID: 8873
		// (set) Token: 0x060022AA RID: 8874
		public extern string text { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000899 RID: 2201
		// (get) Token: 0x060022AB RID: 8875
		// (set) Token: 0x060022AC RID: 8876
		public extern Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060022AD RID: 8877
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetPixelOffset(out Vector2 output);

		// Token: 0x060022AE RID: 8878 RVA: 0x00028430 File Offset: 0x00026630
		private void Internal_SetPixelOffset(Vector2 p)
		{
			GUIText.INTERNAL_CALL_Internal_SetPixelOffset(this, ref p);
		}

		// Token: 0x060022AF RID: 8879
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_SetPixelOffset(GUIText self, ref Vector2 p);

		// Token: 0x1700089A RID: 2202
		// (get) Token: 0x060022B0 RID: 8880 RVA: 0x0002843C File Offset: 0x0002663C
		// (set) Token: 0x060022B1 RID: 8881 RVA: 0x0002845C File Offset: 0x0002665C
		public Vector2 pixelOffset
		{
			get
			{
				Vector2 result;
				this.Internal_GetPixelOffset(out result);
				return result;
			}
			set
			{
				this.Internal_SetPixelOffset(value);
			}
		}

		// Token: 0x1700089B RID: 2203
		// (get) Token: 0x060022B2 RID: 8882
		// (set) Token: 0x060022B3 RID: 8883
		public extern Font font { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700089C RID: 2204
		// (get) Token: 0x060022B4 RID: 8884
		// (set) Token: 0x060022B5 RID: 8885
		public extern TextAlignment alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700089D RID: 2205
		// (get) Token: 0x060022B6 RID: 8886
		// (set) Token: 0x060022B7 RID: 8887
		public extern TextAnchor anchor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700089E RID: 2206
		// (get) Token: 0x060022B8 RID: 8888
		// (set) Token: 0x060022B9 RID: 8889
		public extern float lineSpacing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700089F RID: 2207
		// (get) Token: 0x060022BA RID: 8890
		// (set) Token: 0x060022BB RID: 8891
		public extern float tabSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008A0 RID: 2208
		// (get) Token: 0x060022BC RID: 8892
		// (set) Token: 0x060022BD RID: 8893
		public extern int fontSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008A1 RID: 2209
		// (get) Token: 0x060022BE RID: 8894
		// (set) Token: 0x060022BF RID: 8895
		public extern FontStyle fontStyle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008A2 RID: 2210
		// (get) Token: 0x060022C0 RID: 8896
		// (set) Token: 0x060022C1 RID: 8897
		public extern bool richText { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008A3 RID: 2211
		// (get) Token: 0x060022C2 RID: 8898 RVA: 0x00028468 File Offset: 0x00026668
		// (set) Token: 0x060022C3 RID: 8899 RVA: 0x00028488 File Offset: 0x00026688
		public Color color
		{
			get
			{
				Color result;
				this.INTERNAL_get_color(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_color(ref value);
			}
		}

		// Token: 0x060022C4 RID: 8900
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_color(out Color value);

		// Token: 0x060022C5 RID: 8901
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_color(ref Color value);
	}
}
