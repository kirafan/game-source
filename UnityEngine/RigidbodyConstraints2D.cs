﻿using System;

namespace UnityEngine
{
	// Token: 0x0200015E RID: 350
	[Flags]
	public enum RigidbodyConstraints2D
	{
		// Token: 0x040003DE RID: 990
		None = 0,
		// Token: 0x040003DF RID: 991
		FreezePositionX = 1,
		// Token: 0x040003E0 RID: 992
		FreezePositionY = 2,
		// Token: 0x040003E1 RID: 993
		FreezeRotation = 4,
		// Token: 0x040003E2 RID: 994
		FreezePosition = 3,
		// Token: 0x040003E3 RID: 995
		FreezeAll = 7
	}
}
