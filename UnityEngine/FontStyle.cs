﻿using System;

namespace UnityEngine
{
	// Token: 0x02000235 RID: 565
	public enum FontStyle
	{
		// Token: 0x0400084F RID: 2127
		Normal,
		// Token: 0x04000850 RID: 2128
		Bold,
		// Token: 0x04000851 RID: 2129
		Italic,
		// Token: 0x04000852 RID: 2130
		BoldAndItalic
	}
}
