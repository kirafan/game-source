﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Serialization
{
	// Token: 0x020003B1 RID: 945
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Field, AllowMultiple = true, Inherited = false)]
	public class FormerlySerializedAsAttribute : Attribute
	{
		// Token: 0x06003034 RID: 12340 RVA: 0x0004F1C8 File Offset: 0x0004D3C8
		public FormerlySerializedAsAttribute(string oldName)
		{
			this.m_oldName = oldName;
		}

		// Token: 0x17000B3F RID: 2879
		// (get) Token: 0x06003035 RID: 12341 RVA: 0x0004F1D8 File Offset: 0x0004D3D8
		public string oldName
		{
			get
			{
				return this.m_oldName;
			}
		}

		// Token: 0x04000DC4 RID: 3524
		private string m_oldName;
	}
}
