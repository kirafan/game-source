﻿using System;
using System.Collections;
using System.Runtime.Serialization;

namespace UnityEngine.Serialization
{
	// Token: 0x020003B3 RID: 947
	internal class ListSerializationSurrogate : ISerializationSurrogate
	{
		// Token: 0x0600303B RID: 12347 RVA: 0x0004F29C File Offset: 0x0004D49C
		public void GetObjectData(object obj, SerializationInfo info, StreamingContext context)
		{
			IList list = (IList)obj;
			info.AddValue("_size", list.Count);
			info.AddValue("_items", ListSerializationSurrogate.ArrayFromGenericList(list));
			info.AddValue("_version", 0);
		}

		// Token: 0x0600303C RID: 12348 RVA: 0x0004F2E0 File Offset: 0x0004D4E0
		public object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector)
		{
			IList list = (IList)Activator.CreateInstance(obj.GetType());
			int @int = info.GetInt32("_size");
			object result;
			if (@int == 0)
			{
				result = list;
			}
			else
			{
				IEnumerator enumerator = ((IEnumerable)info.GetValue("_items", typeof(IEnumerable))).GetEnumerator();
				for (int i = 0; i < @int; i++)
				{
					if (!enumerator.MoveNext())
					{
						throw new InvalidOperationException();
					}
					list.Add(enumerator.Current);
				}
				result = list;
			}
			return result;
		}

		// Token: 0x0600303D RID: 12349 RVA: 0x0004F378 File Offset: 0x0004D578
		private static Array ArrayFromGenericList(IList list)
		{
			Array array = Array.CreateInstance(list.GetType().GetGenericArguments()[0], list.Count);
			list.CopyTo(array, 0);
			return array;
		}

		// Token: 0x04000DC5 RID: 3525
		public static readonly ISerializationSurrogate Default = new ListSerializationSurrogate();
	}
}
