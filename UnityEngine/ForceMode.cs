﻿using System;

namespace UnityEngine
{
	// Token: 0x02000132 RID: 306
	public enum ForceMode
	{
		// Token: 0x04000359 RID: 857
		Force,
		// Token: 0x0400035A RID: 858
		Acceleration = 5,
		// Token: 0x0400035B RID: 859
		Impulse = 1,
		// Token: 0x0400035C RID: 860
		VelocityChange
	}
}
