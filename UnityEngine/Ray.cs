﻿using System;

namespace UnityEngine
{
	// Token: 0x02000362 RID: 866
	public struct Ray
	{
		// Token: 0x06002DE8 RID: 11752 RVA: 0x00048FF8 File Offset: 0x000471F8
		public Ray(Vector3 origin, Vector3 direction)
		{
			this.m_Origin = origin;
			this.m_Direction = direction.normalized;
		}

		// Token: 0x17000AE0 RID: 2784
		// (get) Token: 0x06002DE9 RID: 11753 RVA: 0x00049010 File Offset: 0x00047210
		// (set) Token: 0x06002DEA RID: 11754 RVA: 0x0004902C File Offset: 0x0004722C
		public Vector3 origin
		{
			get
			{
				return this.m_Origin;
			}
			set
			{
				this.m_Origin = value;
			}
		}

		// Token: 0x17000AE1 RID: 2785
		// (get) Token: 0x06002DEB RID: 11755 RVA: 0x00049038 File Offset: 0x00047238
		// (set) Token: 0x06002DEC RID: 11756 RVA: 0x00049054 File Offset: 0x00047254
		public Vector3 direction
		{
			get
			{
				return this.m_Direction;
			}
			set
			{
				this.m_Direction = value.normalized;
			}
		}

		// Token: 0x06002DED RID: 11757 RVA: 0x00049064 File Offset: 0x00047264
		public Vector3 GetPoint(float distance)
		{
			return this.m_Origin + this.m_Direction * distance;
		}

		// Token: 0x06002DEE RID: 11758 RVA: 0x00049090 File Offset: 0x00047290
		public override string ToString()
		{
			return UnityString.Format("Origin: {0}, Dir: {1}", new object[]
			{
				this.m_Origin,
				this.m_Direction
			});
		}

		// Token: 0x06002DEF RID: 11759 RVA: 0x000490D4 File Offset: 0x000472D4
		public string ToString(string format)
		{
			return UnityString.Format("Origin: {0}, Dir: {1}", new object[]
			{
				this.m_Origin.ToString(format),
				this.m_Direction.ToString(format)
			});
		}

		// Token: 0x04000D38 RID: 3384
		private Vector3 m_Origin;

		// Token: 0x04000D39 RID: 3385
		private Vector3 m_Direction;
	}
}
