﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x020000B6 RID: 182
	public sealed class ShaderVariantCollection : Object
	{
		// Token: 0x06000D1D RID: 3357 RVA: 0x00011D74 File Offset: 0x0000FF74
		public ShaderVariantCollection()
		{
			ShaderVariantCollection.Internal_Create(this);
		}

		// Token: 0x06000D1E RID: 3358
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] ShaderVariantCollection mono);

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06000D1F RID: 3359
		public extern int shaderCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06000D20 RID: 3360
		public extern int variantCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000D21 RID: 3361 RVA: 0x00011D84 File Offset: 0x0000FF84
		public bool Add(ShaderVariantCollection.ShaderVariant variant)
		{
			return this.AddInternal(variant.shader, variant.passType, variant.keywords);
		}

		// Token: 0x06000D22 RID: 3362
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool AddInternal(Shader shader, PassType passType, string[] keywords);

		// Token: 0x06000D23 RID: 3363 RVA: 0x00011DB4 File Offset: 0x0000FFB4
		public bool Remove(ShaderVariantCollection.ShaderVariant variant)
		{
			return this.RemoveInternal(variant.shader, variant.passType, variant.keywords);
		}

		// Token: 0x06000D24 RID: 3364
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool RemoveInternal(Shader shader, PassType passType, string[] keywords);

		// Token: 0x06000D25 RID: 3365 RVA: 0x00011DE4 File Offset: 0x0000FFE4
		public bool Contains(ShaderVariantCollection.ShaderVariant variant)
		{
			return this.ContainsInternal(variant.shader, variant.passType, variant.keywords);
		}

		// Token: 0x06000D26 RID: 3366
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool ContainsInternal(Shader shader, PassType passType, string[] keywords);

		// Token: 0x06000D27 RID: 3367
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Clear();

		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x06000D28 RID: 3368
		public extern bool isWarmedUp { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000D29 RID: 3369
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void WarmUp();

		// Token: 0x020000B7 RID: 183
		public struct ShaderVariant
		{
			// Token: 0x06000D2A RID: 3370 RVA: 0x00011E14 File Offset: 0x00010014
			public ShaderVariant(Shader shader, PassType passType, params string[] keywords)
			{
				this.shader = shader;
				this.passType = passType;
				this.keywords = keywords;
				ShaderVariantCollection.ShaderVariant.Internal_CheckVariant(shader, passType, keywords);
			}

			// Token: 0x06000D2B RID: 3371
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void Internal_CheckVariant(Shader shader, PassType passType, string[] keywords);

			// Token: 0x040001A4 RID: 420
			public Shader shader;

			// Token: 0x040001A5 RID: 421
			public PassType passType;

			// Token: 0x040001A6 RID: 422
			public string[] keywords;
		}
	}
}
