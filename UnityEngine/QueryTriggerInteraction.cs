﻿using System;

namespace UnityEngine
{
	// Token: 0x02000141 RID: 321
	public enum QueryTriggerInteraction
	{
		// Token: 0x0400039E RID: 926
		UseGlobal,
		// Token: 0x0400039F RID: 927
		Ignore,
		// Token: 0x040003A0 RID: 928
		Collide
	}
}
