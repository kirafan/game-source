﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000059 RID: 89
	public sealed class GeometryUtility
	{
		// Token: 0x060006AB RID: 1707 RVA: 0x00009074 File Offset: 0x00007274
		public static Plane[] CalculateFrustumPlanes(Camera camera)
		{
			return GeometryUtility.CalculateFrustumPlanes(camera.projectionMatrix * camera.worldToCameraMatrix);
		}

		// Token: 0x060006AC RID: 1708 RVA: 0x000090A0 File Offset: 0x000072A0
		public static Plane[] CalculateFrustumPlanes(Matrix4x4 worldToProjectionMatrix)
		{
			Plane[] array = new Plane[6];
			GeometryUtility.Internal_ExtractPlanes(array, worldToProjectionMatrix);
			return array;
		}

		// Token: 0x060006AD RID: 1709 RVA: 0x000090C4 File Offset: 0x000072C4
		private static void Internal_ExtractPlanes(Plane[] planes, Matrix4x4 worldToProjectionMatrix)
		{
			GeometryUtility.INTERNAL_CALL_Internal_ExtractPlanes(planes, ref worldToProjectionMatrix);
		}

		// Token: 0x060006AE RID: 1710
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_ExtractPlanes(Plane[] planes, ref Matrix4x4 worldToProjectionMatrix);

		// Token: 0x060006AF RID: 1711 RVA: 0x000090D0 File Offset: 0x000072D0
		public static bool TestPlanesAABB(Plane[] planes, Bounds bounds)
		{
			return GeometryUtility.INTERNAL_CALL_TestPlanesAABB(planes, ref bounds);
		}

		// Token: 0x060006B0 RID: 1712
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_TestPlanesAABB(Plane[] planes, ref Bounds bounds);

		// Token: 0x060006B1 RID: 1713 RVA: 0x000090F0 File Offset: 0x000072F0
		public static Bounds CalculateBounds(Vector3[] positions, Matrix4x4 transform)
		{
			Bounds result;
			GeometryUtility.INTERNAL_CALL_CalculateBounds(positions, ref transform, out result);
			return result;
		}

		// Token: 0x060006B2 RID: 1714
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_CalculateBounds(Vector3[] positions, ref Matrix4x4 transform, out Bounds value);
	}
}
