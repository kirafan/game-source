﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001CB RID: 459
	[UsedByNativeCode]
	public struct AnimatorClipInfo
	{
		// Token: 0x170007AC RID: 1964
		// (get) Token: 0x06001EC0 RID: 7872 RVA: 0x00023264 File Offset: 0x00021464
		public AnimationClip clip
		{
			get
			{
				return (this.m_ClipInstanceID == 0) ? null : AnimatorClipInfo.ClipInstanceToScriptingObject(this.m_ClipInstanceID);
			}
		}

		// Token: 0x170007AD RID: 1965
		// (get) Token: 0x06001EC1 RID: 7873 RVA: 0x00023298 File Offset: 0x00021498
		public float weight
		{
			get
			{
				return this.m_Weight;
			}
		}

		// Token: 0x06001EC2 RID: 7874
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimationClip ClipInstanceToScriptingObject(int instanceID);

		// Token: 0x040004EC RID: 1260
		private int m_ClipInstanceID;

		// Token: 0x040004ED RID: 1261
		private float m_Weight;
	}
}
