﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000208 RID: 520
	[UsedByNativeCode]
	public struct UILineInfo
	{
		// Token: 0x04000630 RID: 1584
		public int startCharIdx;

		// Token: 0x04000631 RID: 1585
		public int height;

		// Token: 0x04000632 RID: 1586
		public float topY;
	}
}
