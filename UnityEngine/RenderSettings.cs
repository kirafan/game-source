﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x02000041 RID: 65
	public sealed class RenderSettings : Object
	{
		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x06000480 RID: 1152
		// (set) Token: 0x06000481 RID: 1153
		public static extern bool fog { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x06000482 RID: 1154
		// (set) Token: 0x06000483 RID: 1155
		public static extern FogMode fogMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x06000484 RID: 1156 RVA: 0x00007484 File Offset: 0x00005684
		// (set) Token: 0x06000485 RID: 1157 RVA: 0x000074A4 File Offset: 0x000056A4
		public static Color fogColor
		{
			get
			{
				Color result;
				RenderSettings.INTERNAL_get_fogColor(out result);
				return result;
			}
			set
			{
				RenderSettings.INTERNAL_set_fogColor(ref value);
			}
		}

		// Token: 0x06000486 RID: 1158
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_fogColor(out Color value);

		// Token: 0x06000487 RID: 1159
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_fogColor(ref Color value);

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x06000488 RID: 1160
		// (set) Token: 0x06000489 RID: 1161
		public static extern float fogDensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x0600048A RID: 1162
		// (set) Token: 0x0600048B RID: 1163
		public static extern float fogStartDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x0600048C RID: 1164
		// (set) Token: 0x0600048D RID: 1165
		public static extern float fogEndDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x0600048E RID: 1166
		// (set) Token: 0x0600048F RID: 1167
		public static extern AmbientMode ambientMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x06000490 RID: 1168 RVA: 0x000074B0 File Offset: 0x000056B0
		// (set) Token: 0x06000491 RID: 1169 RVA: 0x000074D0 File Offset: 0x000056D0
		public static Color ambientSkyColor
		{
			get
			{
				Color result;
				RenderSettings.INTERNAL_get_ambientSkyColor(out result);
				return result;
			}
			set
			{
				RenderSettings.INTERNAL_set_ambientSkyColor(ref value);
			}
		}

		// Token: 0x06000492 RID: 1170
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_ambientSkyColor(out Color value);

		// Token: 0x06000493 RID: 1171
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_ambientSkyColor(ref Color value);

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x06000494 RID: 1172 RVA: 0x000074DC File Offset: 0x000056DC
		// (set) Token: 0x06000495 RID: 1173 RVA: 0x000074FC File Offset: 0x000056FC
		public static Color ambientEquatorColor
		{
			get
			{
				Color result;
				RenderSettings.INTERNAL_get_ambientEquatorColor(out result);
				return result;
			}
			set
			{
				RenderSettings.INTERNAL_set_ambientEquatorColor(ref value);
			}
		}

		// Token: 0x06000496 RID: 1174
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_ambientEquatorColor(out Color value);

		// Token: 0x06000497 RID: 1175
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_ambientEquatorColor(ref Color value);

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x06000498 RID: 1176 RVA: 0x00007508 File Offset: 0x00005708
		// (set) Token: 0x06000499 RID: 1177 RVA: 0x00007528 File Offset: 0x00005728
		public static Color ambientGroundColor
		{
			get
			{
				Color result;
				RenderSettings.INTERNAL_get_ambientGroundColor(out result);
				return result;
			}
			set
			{
				RenderSettings.INTERNAL_set_ambientGroundColor(ref value);
			}
		}

		// Token: 0x0600049A RID: 1178
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_ambientGroundColor(out Color value);

		// Token: 0x0600049B RID: 1179
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_ambientGroundColor(ref Color value);

		// Token: 0x170000EB RID: 235
		// (get) Token: 0x0600049C RID: 1180 RVA: 0x00007534 File Offset: 0x00005734
		// (set) Token: 0x0600049D RID: 1181 RVA: 0x00007554 File Offset: 0x00005754
		public static Color ambientLight
		{
			get
			{
				Color result;
				RenderSettings.INTERNAL_get_ambientLight(out result);
				return result;
			}
			set
			{
				RenderSettings.INTERNAL_set_ambientLight(ref value);
			}
		}

		// Token: 0x0600049E RID: 1182
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_ambientLight(out Color value);

		// Token: 0x0600049F RID: 1183
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_ambientLight(ref Color value);

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x060004A0 RID: 1184
		// (set) Token: 0x060004A1 RID: 1185
		public static extern float ambientIntensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x060004A2 RID: 1186 RVA: 0x00007560 File Offset: 0x00005760
		// (set) Token: 0x060004A3 RID: 1187 RVA: 0x00007580 File Offset: 0x00005780
		public static SphericalHarmonicsL2 ambientProbe
		{
			get
			{
				SphericalHarmonicsL2 result;
				RenderSettings.INTERNAL_get_ambientProbe(out result);
				return result;
			}
			set
			{
				RenderSettings.INTERNAL_set_ambientProbe(ref value);
			}
		}

		// Token: 0x060004A4 RID: 1188
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_ambientProbe(out SphericalHarmonicsL2 value);

		// Token: 0x060004A5 RID: 1189
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_ambientProbe(ref SphericalHarmonicsL2 value);

		// Token: 0x170000EE RID: 238
		// (get) Token: 0x060004A6 RID: 1190
		// (set) Token: 0x060004A7 RID: 1191
		public static extern float reflectionIntensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x060004A8 RID: 1192
		// (set) Token: 0x060004A9 RID: 1193
		public static extern int reflectionBounces { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x060004AA RID: 1194
		// (set) Token: 0x060004AB RID: 1195
		public static extern float haloStrength { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x060004AC RID: 1196
		// (set) Token: 0x060004AD RID: 1197
		public static extern float flareStrength { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x060004AE RID: 1198
		// (set) Token: 0x060004AF RID: 1199
		public static extern float flareFadeSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x060004B0 RID: 1200
		// (set) Token: 0x060004B1 RID: 1201
		public static extern Material skybox { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x060004B2 RID: 1202
		// (set) Token: 0x060004B3 RID: 1203
		public static extern Light sun { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x060004B4 RID: 1204
		// (set) Token: 0x060004B5 RID: 1205
		public static extern DefaultReflectionMode defaultReflectionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x060004B6 RID: 1206
		// (set) Token: 0x060004B7 RID: 1207
		public static extern int defaultReflectionResolution { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x060004B8 RID: 1208
		// (set) Token: 0x060004B9 RID: 1209
		public static extern Cubemap customReflection { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060004BA RID: 1210
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Reset();

		// Token: 0x060004BB RID: 1211
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Object GetRenderSettings();
	}
}
