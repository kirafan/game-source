﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Diagnostics
{
	// Token: 0x020000A1 RID: 161
	public static class PlayerConnection
	{
		// Token: 0x17000293 RID: 659
		// (get) Token: 0x06000B2C RID: 2860
		public static extern bool connected { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000B2D RID: 2861
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SendFile(string remoteFilePath, byte[] data);
	}
}
