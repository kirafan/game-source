﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x02000049 RID: 73
	public class Renderer : Component
	{
		// Token: 0x1700012C RID: 300
		// (get) Token: 0x0600053C RID: 1340
		// (set) Token: 0x0600053D RID: 1341
		internal extern Transform staticBatchRootTransform { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700012D RID: 301
		// (get) Token: 0x0600053E RID: 1342
		internal extern int staticBatchIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600053F RID: 1343
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetStaticBatchInfo(int firstSubMesh, int subMeshCount);

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x06000540 RID: 1344
		public extern bool isPartOfStaticBatch { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700012F RID: 303
		// (get) Token: 0x06000541 RID: 1345 RVA: 0x00007AEC File Offset: 0x00005CEC
		public Matrix4x4 worldToLocalMatrix
		{
			get
			{
				Matrix4x4 result;
				this.INTERNAL_get_worldToLocalMatrix(out result);
				return result;
			}
		}

		// Token: 0x06000542 RID: 1346
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_worldToLocalMatrix(out Matrix4x4 value);

		// Token: 0x17000130 RID: 304
		// (get) Token: 0x06000543 RID: 1347 RVA: 0x00007B0C File Offset: 0x00005D0C
		public Matrix4x4 localToWorldMatrix
		{
			get
			{
				Matrix4x4 result;
				this.INTERNAL_get_localToWorldMatrix(out result);
				return result;
			}
		}

		// Token: 0x06000544 RID: 1348
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localToWorldMatrix(out Matrix4x4 value);

		// Token: 0x17000131 RID: 305
		// (get) Token: 0x06000545 RID: 1349
		// (set) Token: 0x06000546 RID: 1350
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x06000547 RID: 1351
		// (set) Token: 0x06000548 RID: 1352
		public extern ShadowCastingMode shadowCastingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x06000549 RID: 1353
		// (set) Token: 0x0600054A RID: 1354
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Property castShadows has been deprecated. Use shadowCastingMode instead.")]
		public extern bool castShadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x0600054B RID: 1355
		// (set) Token: 0x0600054C RID: 1356
		public extern bool receiveShadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000135 RID: 309
		// (get) Token: 0x0600054D RID: 1357
		// (set) Token: 0x0600054E RID: 1358
		public extern Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x0600054F RID: 1359
		// (set) Token: 0x06000550 RID: 1360
		public extern Material sharedMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x06000551 RID: 1361
		// (set) Token: 0x06000552 RID: 1362
		public extern Material[] materials { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06000553 RID: 1363
		// (set) Token: 0x06000554 RID: 1364
		public extern Material[] sharedMaterials { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x06000555 RID: 1365 RVA: 0x00007B2C File Offset: 0x00005D2C
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_bounds(out result);
				return result;
			}
		}

		// Token: 0x06000556 RID: 1366
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_bounds(out Bounds value);

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x06000557 RID: 1367
		// (set) Token: 0x06000558 RID: 1368
		public extern int lightmapIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000559 RID: 1369
		// (set) Token: 0x0600055A RID: 1370
		public extern int realtimeLightmapIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x0600055B RID: 1371 RVA: 0x00007B4C File Offset: 0x00005D4C
		// (set) Token: 0x0600055C RID: 1372 RVA: 0x00007B6C File Offset: 0x00005D6C
		public Vector4 lightmapScaleOffset
		{
			get
			{
				Vector4 result;
				this.INTERNAL_get_lightmapScaleOffset(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_lightmapScaleOffset(ref value);
			}
		}

		// Token: 0x0600055D RID: 1373
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_lightmapScaleOffset(out Vector4 value);

		// Token: 0x0600055E RID: 1374
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_lightmapScaleOffset(ref Vector4 value);

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x0600055F RID: 1375
		// (set) Token: 0x06000560 RID: 1376
		public extern MotionVectorGenerationMode motionVectorGenerationMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000561 RID: 1377 RVA: 0x00007B78 File Offset: 0x00005D78
		// (set) Token: 0x06000562 RID: 1378 RVA: 0x00007B98 File Offset: 0x00005D98
		[Obsolete("motionVectors property is deprecated. Use motionVectorGenerationMode instead.")]
		private bool motionVectors
		{
			get
			{
				return this.motionVectorGenerationMode == MotionVectorGenerationMode.Object;
			}
			set
			{
				this.motionVectorGenerationMode = ((!value) ? MotionVectorGenerationMode.Camera : MotionVectorGenerationMode.Object);
			}
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x06000563 RID: 1379 RVA: 0x00007BB0 File Offset: 0x00005DB0
		// (set) Token: 0x06000564 RID: 1380 RVA: 0x00007BD0 File Offset: 0x00005DD0
		public Vector4 realtimeLightmapScaleOffset
		{
			get
			{
				Vector4 result;
				this.INTERNAL_get_realtimeLightmapScaleOffset(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_realtimeLightmapScaleOffset(ref value);
			}
		}

		// Token: 0x06000565 RID: 1381
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_realtimeLightmapScaleOffset(out Vector4 value);

		// Token: 0x06000566 RID: 1382
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_realtimeLightmapScaleOffset(ref Vector4 value);

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x06000567 RID: 1383
		public extern bool isVisible { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000141 RID: 321
		// (get) Token: 0x06000568 RID: 1384
		// (set) Token: 0x06000569 RID: 1385
		[Obsolete("useLightProbes property is deprecated. Use lightProbeUsage instead.")]
		public extern bool useLightProbes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x0600056A RID: 1386
		// (set) Token: 0x0600056B RID: 1387
		public extern LightProbeUsage lightProbeUsage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x0600056C RID: 1388
		// (set) Token: 0x0600056D RID: 1389
		public extern GameObject lightProbeProxyVolumeOverride { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x0600056E RID: 1390
		// (set) Token: 0x0600056F RID: 1391
		public extern Transform probeAnchor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000145 RID: 325
		// (get) Token: 0x06000570 RID: 1392
		// (set) Token: 0x06000571 RID: 1393
		public extern ReflectionProbeUsage reflectionProbeUsage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000572 RID: 1394
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPropertyBlock(MaterialPropertyBlock properties);

		// Token: 0x06000573 RID: 1395
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GetPropertyBlock(MaterialPropertyBlock dest);

		// Token: 0x17000146 RID: 326
		// (get) Token: 0x06000574 RID: 1396
		// (set) Token: 0x06000575 RID: 1397
		public extern string sortingLayerName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000147 RID: 327
		// (get) Token: 0x06000576 RID: 1398
		// (set) Token: 0x06000577 RID: 1399
		public extern int sortingLayerID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000148 RID: 328
		// (get) Token: 0x06000578 RID: 1400
		// (set) Token: 0x06000579 RID: 1401
		public extern int sortingOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600057A RID: 1402
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetClosestReflectionProbesInternal(object result);

		// Token: 0x0600057B RID: 1403 RVA: 0x00007BDC File Offset: 0x00005DDC
		public void GetClosestReflectionProbes(List<ReflectionProbeBlendInfo> result)
		{
			this.GetClosestReflectionProbesInternal(result);
		}
	}
}
