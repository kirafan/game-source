﻿using System;

namespace UnityEngine
{
	// Token: 0x02000236 RID: 566
	public enum ImagePosition
	{
		// Token: 0x04000854 RID: 2132
		ImageLeft,
		// Token: 0x04000855 RID: 2133
		ImageAbove,
		// Token: 0x04000856 RID: 2134
		ImageOnly,
		// Token: 0x04000857 RID: 2135
		TextOnly
	}
}
