﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000011 RID: 17
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class AssetBundleRequest : AsyncOperation
	{
		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000172 RID: 370
		public extern Object asset { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000173 RID: 371
		public extern Object[] allAssets { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
