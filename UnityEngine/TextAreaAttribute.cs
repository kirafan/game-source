﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035E RID: 862
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class TextAreaAttribute : PropertyAttribute
	{
		// Token: 0x06002DE1 RID: 11745 RVA: 0x00048EB4 File Offset: 0x000470B4
		public TextAreaAttribute()
		{
			this.minLines = 3;
			this.maxLines = 3;
		}

		// Token: 0x06002DE2 RID: 11746 RVA: 0x00048ECC File Offset: 0x000470CC
		public TextAreaAttribute(int minLines, int maxLines)
		{
			this.minLines = minLines;
			this.maxLines = maxLines;
		}

		// Token: 0x04000D2E RID: 3374
		public readonly int minLines;

		// Token: 0x04000D2F RID: 3375
		public readonly int maxLines;
	}
}
