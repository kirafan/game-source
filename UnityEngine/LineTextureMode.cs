﻿using System;

namespace UnityEngine
{
	// Token: 0x0200004C RID: 76
	public enum LineTextureMode
	{
		// Token: 0x0400006D RID: 109
		Stretch,
		// Token: 0x0400006E RID: 110
		Tile,
		// Token: 0x0400006F RID: 111
		DistributePerSegment,
		// Token: 0x04000070 RID: 112
		RepeatPerSegment
	}
}
