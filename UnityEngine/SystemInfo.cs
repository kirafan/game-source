﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x02000015 RID: 21
	public sealed class SystemInfo
	{
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060001B4 RID: 436
		public static extern string operatingSystem { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060001B5 RID: 437
		public static extern OperatingSystemFamily operatingSystemFamily { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060001B6 RID: 438
		public static extern string processorType { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060001B7 RID: 439
		public static extern int processorFrequency { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060001B8 RID: 440
		public static extern int processorCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060001B9 RID: 441
		public static extern int systemMemorySize { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060001BA RID: 442
		public static extern int graphicsMemorySize { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060001BB RID: 443
		public static extern string graphicsDeviceName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060001BC RID: 444
		public static extern string graphicsDeviceVendor { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060001BD RID: 445
		public static extern int graphicsDeviceID { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060001BE RID: 446
		public static extern int graphicsDeviceVendorID { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060001BF RID: 447
		public static extern GraphicsDeviceType graphicsDeviceType { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060001C0 RID: 448
		internal static extern bool usesOpenGLTextureCoords { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060001C1 RID: 449
		public static extern string graphicsDeviceVersion { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060001C2 RID: 450
		public static extern int graphicsShaderLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060001C3 RID: 451 RVA: 0x00004E74 File Offset: 0x00003074
		[Obsolete("graphicsPixelFillrate is no longer supported in Unity 5.0+.")]
		public static int graphicsPixelFillrate
		{
			get
			{
				return -1;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060001C4 RID: 452 RVA: 0x00004E8C File Offset: 0x0000308C
		[Obsolete("Vertex program support is required in Unity 5.0+")]
		public static bool supportsVertexPrograms
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060001C5 RID: 453
		public static extern bool graphicsMultiThreaded { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060001C6 RID: 454
		public static extern bool supportsShadows { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060001C7 RID: 455
		public static extern bool supportsRawShadowDepthSampling { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060001C8 RID: 456
		[Obsolete("supportsRenderTextures always returns true, no need to call it")]
		public static extern bool supportsRenderTextures { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060001C9 RID: 457
		public static extern bool supportsMotionVectors { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060001CA RID: 458
		public static extern bool supportsRenderToCubemap { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060001CB RID: 459
		public static extern bool supportsImageEffects { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060001CC RID: 460
		public static extern bool supports3DTextures { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060001CD RID: 461
		public static extern bool supports2DArrayTextures { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060001CE RID: 462
		public static extern bool supportsCubemapArrayTextures { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060001CF RID: 463
		public static extern CopyTextureSupport copyTextureSupport { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060001D0 RID: 464
		public static extern bool supportsComputeShaders { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060001D1 RID: 465
		public static extern bool supportsInstancing { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060001D2 RID: 466
		public static extern bool supportsSparseTextures { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060001D3 RID: 467
		public static extern int supportedRenderTargetCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060001D4 RID: 468
		public static extern bool usesReversedZBuffer { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060001D5 RID: 469
		[Obsolete("supportsStencil always returns true, no need to call it")]
		public static extern int supportsStencil { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060001D6 RID: 470
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SupportsRenderTextureFormat(RenderTextureFormat format);

		// Token: 0x060001D7 RID: 471
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SupportsTextureFormat(TextureFormat format);

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060001D8 RID: 472
		public static extern NPOTSupport npotSupport { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060001D9 RID: 473
		public static extern string deviceUniqueIdentifier { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060001DA RID: 474
		public static extern string deviceName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060001DB RID: 475
		public static extern string deviceModel { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060001DC RID: 476
		public static extern bool supportsAccelerometer { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060001DD RID: 477
		public static extern bool supportsGyroscope { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060001DE RID: 478
		public static extern bool supportsLocationService { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060001DF RID: 479
		public static extern bool supportsVibration { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060001E0 RID: 480
		public static extern bool supportsAudio { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060001E1 RID: 481
		public static extern DeviceType deviceType { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060001E2 RID: 482
		public static extern int maxTextureSize { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060001E3 RID: 483
		internal static extern int maxRenderTextureSize { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x04000027 RID: 39
		public const string unsupportedIdentifier = "n/a";
	}
}
