﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000211 RID: 529
	public sealed class TerrainCollider : Collider
	{
		// Token: 0x170008E8 RID: 2280
		// (get) Token: 0x0600239D RID: 9117
		// (set) Token: 0x0600239E RID: 9118
		public extern TerrainData terrainData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
