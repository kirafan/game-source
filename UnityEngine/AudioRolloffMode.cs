﻿using System;

namespace UnityEngine
{
	// Token: 0x020001A4 RID: 420
	public enum AudioRolloffMode
	{
		// Token: 0x04000476 RID: 1142
		Logarithmic,
		// Token: 0x04000477 RID: 1143
		Linear,
		// Token: 0x04000478 RID: 1144
		Custom
	}
}
