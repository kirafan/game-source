﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200003F RID: 63
	public sealed class OcclusionArea : Component
	{
		// Token: 0x170000DE RID: 222
		// (get) Token: 0x06000474 RID: 1140 RVA: 0x0000741C File Offset: 0x0000561C
		// (set) Token: 0x06000475 RID: 1141 RVA: 0x0000743C File Offset: 0x0000563C
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_center(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_center(ref value);
			}
		}

		// Token: 0x06000476 RID: 1142
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_center(out Vector3 value);

		// Token: 0x06000477 RID: 1143
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_center(ref Vector3 value);

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x06000478 RID: 1144 RVA: 0x00007448 File Offset: 0x00005648
		// (set) Token: 0x06000479 RID: 1145 RVA: 0x00007468 File Offset: 0x00005668
		public Vector3 size
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_size(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_size(ref value);
			}
		}

		// Token: 0x0600047A RID: 1146
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_size(out Vector3 value);

		// Token: 0x0600047B RID: 1147
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_size(ref Vector3 value);
	}
}
