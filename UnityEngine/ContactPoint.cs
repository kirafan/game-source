﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000143 RID: 323
	[UsedByNativeCode]
	public struct ContactPoint
	{
		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x06001701 RID: 5889 RVA: 0x0001DC6C File Offset: 0x0001BE6C
		public Vector3 point
		{
			get
			{
				return this.m_Point;
			}
		}

		// Token: 0x17000535 RID: 1333
		// (get) Token: 0x06001702 RID: 5890 RVA: 0x0001DC88 File Offset: 0x0001BE88
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
		}

		// Token: 0x17000536 RID: 1334
		// (get) Token: 0x06001703 RID: 5891 RVA: 0x0001DCA4 File Offset: 0x0001BEA4
		public Collider thisCollider
		{
			get
			{
				return ContactPoint.ColliderFromInstanceId(this.m_ThisColliderInstanceID);
			}
		}

		// Token: 0x17000537 RID: 1335
		// (get) Token: 0x06001704 RID: 5892 RVA: 0x0001DCC4 File Offset: 0x0001BEC4
		public Collider otherCollider
		{
			get
			{
				return ContactPoint.ColliderFromInstanceId(this.m_OtherColliderInstanceID);
			}
		}

		// Token: 0x17000538 RID: 1336
		// (get) Token: 0x06001705 RID: 5893 RVA: 0x0001DCE4 File Offset: 0x0001BEE4
		public float separation
		{
			get
			{
				return this.m_Separation;
			}
		}

		// Token: 0x06001706 RID: 5894
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Collider ColliderFromInstanceId(int instanceID);

		// Token: 0x040003A7 RID: 935
		internal Vector3 m_Point;

		// Token: 0x040003A8 RID: 936
		internal Vector3 m_Normal;

		// Token: 0x040003A9 RID: 937
		internal int m_ThisColliderInstanceID;

		// Token: 0x040003AA RID: 938
		internal int m_OtherColliderInstanceID;

		// Token: 0x040003AB RID: 939
		internal float m_Separation;
	}
}
