﻿using System;

namespace UnityEngine
{
	// Token: 0x020002FC RID: 764
	public enum NPOTSupport
	{
		// Token: 0x04000B59 RID: 2905
		None,
		// Token: 0x04000B5A RID: 2906
		Restricted,
		// Token: 0x04000B5B RID: 2907
		Full
	}
}
