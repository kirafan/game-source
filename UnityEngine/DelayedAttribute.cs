﻿using System;

namespace UnityEngine
{
	// Token: 0x02000360 RID: 864
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class DelayedAttribute : PropertyAttribute
	{
	}
}
