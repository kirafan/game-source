﻿using System;

namespace UnityEngine
{
	// Token: 0x02000071 RID: 113
	public struct AccelerationEvent
	{
		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x06000780 RID: 1920 RVA: 0x00009ABC File Offset: 0x00007CBC
		public Vector3 acceleration
		{
			get
			{
				return new Vector3(this.x, this.y, this.z);
			}
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x06000781 RID: 1921 RVA: 0x00009AE8 File Offset: 0x00007CE8
		public float deltaTime
		{
			get
			{
				return this.m_TimeDelta;
			}
		}

		// Token: 0x040000D0 RID: 208
		private float x;

		// Token: 0x040000D1 RID: 209
		private float y;

		// Token: 0x040000D2 RID: 210
		private float z;

		// Token: 0x040000D3 RID: 211
		private float m_TimeDelta;
	}
}
