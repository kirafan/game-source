﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000064 RID: 100
	public sealed class GUITexture : GUIElement
	{
		// Token: 0x17000193 RID: 403
		// (get) Token: 0x0600072A RID: 1834 RVA: 0x000094B0 File Offset: 0x000076B0
		// (set) Token: 0x0600072B RID: 1835 RVA: 0x000094D0 File Offset: 0x000076D0
		public Color color
		{
			get
			{
				Color result;
				this.INTERNAL_get_color(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_color(ref value);
			}
		}

		// Token: 0x0600072C RID: 1836
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_color(out Color value);

		// Token: 0x0600072D RID: 1837
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_color(ref Color value);

		// Token: 0x17000194 RID: 404
		// (get) Token: 0x0600072E RID: 1838
		// (set) Token: 0x0600072F RID: 1839
		public extern Texture texture { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000195 RID: 405
		// (get) Token: 0x06000730 RID: 1840 RVA: 0x000094DC File Offset: 0x000076DC
		// (set) Token: 0x06000731 RID: 1841 RVA: 0x000094FC File Offset: 0x000076FC
		public Rect pixelInset
		{
			get
			{
				Rect result;
				this.INTERNAL_get_pixelInset(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_pixelInset(ref value);
			}
		}

		// Token: 0x06000732 RID: 1842
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_pixelInset(out Rect value);

		// Token: 0x06000733 RID: 1843
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_pixelInset(ref Rect value);

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x06000734 RID: 1844
		// (set) Token: 0x06000735 RID: 1845
		public extern RectOffset border { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
