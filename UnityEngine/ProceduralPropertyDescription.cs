﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020000C7 RID: 199
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class ProceduralPropertyDescription
	{
		// Token: 0x04000200 RID: 512
		public string name;

		// Token: 0x04000201 RID: 513
		public string label;

		// Token: 0x04000202 RID: 514
		public string group;

		// Token: 0x04000203 RID: 515
		public ProceduralPropertyType type;

		// Token: 0x04000204 RID: 516
		public bool hasRange;

		// Token: 0x04000205 RID: 517
		public float minimum;

		// Token: 0x04000206 RID: 518
		public float maximum;

		// Token: 0x04000207 RID: 519
		public float step;

		// Token: 0x04000208 RID: 520
		public string[] enumOptions;

		// Token: 0x04000209 RID: 521
		public string[] componentLabels;
	}
}
