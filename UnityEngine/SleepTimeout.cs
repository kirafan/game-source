﻿using System;

namespace UnityEngine
{
	// Token: 0x0200005B RID: 91
	public sealed class SleepTimeout
	{
		// Token: 0x0400008E RID: 142
		public const int NeverSleep = -1;

		// Token: 0x0400008F RID: 143
		public const int SystemSetting = -2;
	}
}
