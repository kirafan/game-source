﻿using System;

namespace UnityEngine
{
	// Token: 0x020001DA RID: 474
	internal enum BodyDoF
	{
		// Token: 0x04000524 RID: 1316
		SpineFrontBack,
		// Token: 0x04000525 RID: 1317
		SpineLeftRight,
		// Token: 0x04000526 RID: 1318
		SpineRollLeftRight,
		// Token: 0x04000527 RID: 1319
		ChestFrontBack,
		// Token: 0x04000528 RID: 1320
		ChestLeftRight,
		// Token: 0x04000529 RID: 1321
		ChestRollLeftRight,
		// Token: 0x0400052A RID: 1322
		LastBodyDoF
	}
}
