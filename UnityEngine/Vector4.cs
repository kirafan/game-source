﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000394 RID: 916
	[UsedByNativeCode]
	public struct Vector4
	{
		// Token: 0x06002F43 RID: 12099 RVA: 0x0004D374 File Offset: 0x0004B574
		public Vector4(float x, float y, float z, float w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		// Token: 0x06002F44 RID: 12100 RVA: 0x0004D394 File Offset: 0x0004B594
		public Vector4(float x, float y, float z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 0f;
		}

		// Token: 0x06002F45 RID: 12101 RVA: 0x0004D3B8 File Offset: 0x0004B5B8
		public Vector4(float x, float y)
		{
			this.x = x;
			this.y = y;
			this.z = 0f;
			this.w = 0f;
		}

		// Token: 0x17000B10 RID: 2832
		public float this[int index]
		{
			get
			{
				float result;
				switch (index)
				{
				case 0:
					result = this.x;
					break;
				case 1:
					result = this.y;
					break;
				case 2:
					result = this.z;
					break;
				case 3:
					result = this.w;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector4 index!");
				}
				return result;
			}
			set
			{
				switch (index)
				{
				case 0:
					this.x = value;
					break;
				case 1:
					this.y = value;
					break;
				case 2:
					this.z = value;
					break;
				case 3:
					this.w = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector4 index!");
				}
			}
		}

		// Token: 0x06002F48 RID: 12104 RVA: 0x0004D4AC File Offset: 0x0004B6AC
		public void Set(float new_x, float new_y, float new_z, float new_w)
		{
			this.x = new_x;
			this.y = new_y;
			this.z = new_z;
			this.w = new_w;
		}

		// Token: 0x06002F49 RID: 12105 RVA: 0x0004D4CC File Offset: 0x0004B6CC
		public static Vector4 Lerp(Vector4 a, Vector4 b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Vector4(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t, a.w + (b.w - a.w) * t);
		}

		// Token: 0x06002F4A RID: 12106 RVA: 0x0004D554 File Offset: 0x0004B754
		public static Vector4 LerpUnclamped(Vector4 a, Vector4 b, float t)
		{
			return new Vector4(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t, a.w + (b.w - a.w) * t);
		}

		// Token: 0x06002F4B RID: 12107 RVA: 0x0004D5D4 File Offset: 0x0004B7D4
		public static Vector4 MoveTowards(Vector4 current, Vector4 target, float maxDistanceDelta)
		{
			Vector4 a = target - current;
			float magnitude = a.magnitude;
			Vector4 result;
			if (magnitude <= maxDistanceDelta || magnitude == 0f)
			{
				result = target;
			}
			else
			{
				result = current + a / magnitude * maxDistanceDelta;
			}
			return result;
		}

		// Token: 0x06002F4C RID: 12108 RVA: 0x0004D628 File Offset: 0x0004B828
		public static Vector4 Scale(Vector4 a, Vector4 b)
		{
			return new Vector4(a.x * b.x, a.y * b.y, a.z * b.z, a.w * b.w);
		}

		// Token: 0x06002F4D RID: 12109 RVA: 0x0004D680 File Offset: 0x0004B880
		public void Scale(Vector4 scale)
		{
			this.x *= scale.x;
			this.y *= scale.y;
			this.z *= scale.z;
			this.w *= scale.w;
		}

		// Token: 0x06002F4E RID: 12110 RVA: 0x0004D6E0 File Offset: 0x0004B8E0
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode() << 2 ^ this.z.GetHashCode() >> 2 ^ this.w.GetHashCode() >> 1;
		}

		// Token: 0x06002F4F RID: 12111 RVA: 0x0004D744 File Offset: 0x0004B944
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Vector4))
			{
				result = false;
			}
			else
			{
				Vector4 vector = (Vector4)other;
				result = (this.x.Equals(vector.x) && this.y.Equals(vector.y) && this.z.Equals(vector.z) && this.w.Equals(vector.w));
			}
			return result;
		}

		// Token: 0x06002F50 RID: 12112 RVA: 0x0004D7CC File Offset: 0x0004B9CC
		public static Vector4 Normalize(Vector4 a)
		{
			float num = Vector4.Magnitude(a);
			Vector4 result;
			if (num > 1E-05f)
			{
				result = a / num;
			}
			else
			{
				result = Vector4.zero;
			}
			return result;
		}

		// Token: 0x06002F51 RID: 12113 RVA: 0x0004D808 File Offset: 0x0004BA08
		public void Normalize()
		{
			float num = Vector4.Magnitude(this);
			if (num > 1E-05f)
			{
				this /= num;
			}
			else
			{
				this = Vector4.zero;
			}
		}

		// Token: 0x17000B11 RID: 2833
		// (get) Token: 0x06002F52 RID: 12114 RVA: 0x0004D850 File Offset: 0x0004BA50
		public Vector4 normalized
		{
			get
			{
				return Vector4.Normalize(this);
			}
		}

		// Token: 0x06002F53 RID: 12115 RVA: 0x0004D870 File Offset: 0x0004BA70
		public static float Dot(Vector4 a, Vector4 b)
		{
			return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
		}

		// Token: 0x06002F54 RID: 12116 RVA: 0x0004D8C4 File Offset: 0x0004BAC4
		public static Vector4 Project(Vector4 a, Vector4 b)
		{
			return b * Vector4.Dot(a, b) / Vector4.Dot(b, b);
		}

		// Token: 0x06002F55 RID: 12117 RVA: 0x0004D8F4 File Offset: 0x0004BAF4
		public static float Distance(Vector4 a, Vector4 b)
		{
			return Vector4.Magnitude(a - b);
		}

		// Token: 0x06002F56 RID: 12118 RVA: 0x0004D918 File Offset: 0x0004BB18
		public static float Magnitude(Vector4 a)
		{
			return Mathf.Sqrt(Vector4.Dot(a, a));
		}

		// Token: 0x17000B12 RID: 2834
		// (get) Token: 0x06002F57 RID: 12119 RVA: 0x0004D93C File Offset: 0x0004BB3C
		public float magnitude
		{
			get
			{
				return Mathf.Sqrt(Vector4.Dot(this, this));
			}
		}

		// Token: 0x17000B13 RID: 2835
		// (get) Token: 0x06002F58 RID: 12120 RVA: 0x0004D968 File Offset: 0x0004BB68
		public float sqrMagnitude
		{
			get
			{
				return Vector4.Dot(this, this);
			}
		}

		// Token: 0x06002F59 RID: 12121 RVA: 0x0004D990 File Offset: 0x0004BB90
		public static Vector4 Min(Vector4 lhs, Vector4 rhs)
		{
			return new Vector4(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y), Mathf.Min(lhs.z, rhs.z), Mathf.Min(lhs.w, rhs.w));
		}

		// Token: 0x06002F5A RID: 12122 RVA: 0x0004D9F8 File Offset: 0x0004BBF8
		public static Vector4 Max(Vector4 lhs, Vector4 rhs)
		{
			return new Vector4(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y), Mathf.Max(lhs.z, rhs.z), Mathf.Max(lhs.w, rhs.w));
		}

		// Token: 0x17000B14 RID: 2836
		// (get) Token: 0x06002F5B RID: 12123 RVA: 0x0004DA60 File Offset: 0x0004BC60
		public static Vector4 zero
		{
			get
			{
				return new Vector4(0f, 0f, 0f, 0f);
			}
		}

		// Token: 0x17000B15 RID: 2837
		// (get) Token: 0x06002F5C RID: 12124 RVA: 0x0004DA90 File Offset: 0x0004BC90
		public static Vector4 one
		{
			get
			{
				return new Vector4(1f, 1f, 1f, 1f);
			}
		}

		// Token: 0x06002F5D RID: 12125 RVA: 0x0004DAC0 File Offset: 0x0004BCC0
		public static Vector4 operator +(Vector4 a, Vector4 b)
		{
			return new Vector4(a.x + b.x, a.y + b.y, a.z + b.z, a.w + b.w);
		}

		// Token: 0x06002F5E RID: 12126 RVA: 0x0004DB18 File Offset: 0x0004BD18
		public static Vector4 operator -(Vector4 a, Vector4 b)
		{
			return new Vector4(a.x - b.x, a.y - b.y, a.z - b.z, a.w - b.w);
		}

		// Token: 0x06002F5F RID: 12127 RVA: 0x0004DB70 File Offset: 0x0004BD70
		public static Vector4 operator -(Vector4 a)
		{
			return new Vector4(-a.x, -a.y, -a.z, -a.w);
		}

		// Token: 0x06002F60 RID: 12128 RVA: 0x0004DBAC File Offset: 0x0004BDAC
		public static Vector4 operator *(Vector4 a, float d)
		{
			return new Vector4(a.x * d, a.y * d, a.z * d, a.w * d);
		}

		// Token: 0x06002F61 RID: 12129 RVA: 0x0004DBEC File Offset: 0x0004BDEC
		public static Vector4 operator *(float d, Vector4 a)
		{
			return new Vector4(a.x * d, a.y * d, a.z * d, a.w * d);
		}

		// Token: 0x06002F62 RID: 12130 RVA: 0x0004DC2C File Offset: 0x0004BE2C
		public static Vector4 operator /(Vector4 a, float d)
		{
			return new Vector4(a.x / d, a.y / d, a.z / d, a.w / d);
		}

		// Token: 0x06002F63 RID: 12131 RVA: 0x0004DC6C File Offset: 0x0004BE6C
		public static bool operator ==(Vector4 lhs, Vector4 rhs)
		{
			return Vector4.SqrMagnitude(lhs - rhs) < 9.9999994E-11f;
		}

		// Token: 0x06002F64 RID: 12132 RVA: 0x0004DC94 File Offset: 0x0004BE94
		public static bool operator !=(Vector4 lhs, Vector4 rhs)
		{
			return Vector4.SqrMagnitude(lhs - rhs) >= 9.9999994E-11f;
		}

		// Token: 0x06002F65 RID: 12133 RVA: 0x0004DCC0 File Offset: 0x0004BEC0
		public static implicit operator Vector4(Vector3 v)
		{
			return new Vector4(v.x, v.y, v.z, 0f);
		}

		// Token: 0x06002F66 RID: 12134 RVA: 0x0004DCF4 File Offset: 0x0004BEF4
		public static implicit operator Vector3(Vector4 v)
		{
			return new Vector3(v.x, v.y, v.z);
		}

		// Token: 0x06002F67 RID: 12135 RVA: 0x0004DD24 File Offset: 0x0004BF24
		public static implicit operator Vector4(Vector2 v)
		{
			return new Vector4(v.x, v.y, 0f, 0f);
		}

		// Token: 0x06002F68 RID: 12136 RVA: 0x0004DD58 File Offset: 0x0004BF58
		public static implicit operator Vector2(Vector4 v)
		{
			return new Vector2(v.x, v.y);
		}

		// Token: 0x06002F69 RID: 12137 RVA: 0x0004DD80 File Offset: 0x0004BF80
		public override string ToString()
		{
			return UnityString.Format("({0:F1}, {1:F1}, {2:F1}, {3:F1})", new object[]
			{
				this.x,
				this.y,
				this.z,
				this.w
			});
		}

		// Token: 0x06002F6A RID: 12138 RVA: 0x0004DDE0 File Offset: 0x0004BFE0
		public string ToString(string format)
		{
			return UnityString.Format("({0}, {1}, {2}, {3})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format),
				this.z.ToString(format),
				this.w.ToString(format)
			});
		}

		// Token: 0x06002F6B RID: 12139 RVA: 0x0004DE44 File Offset: 0x0004C044
		public static float SqrMagnitude(Vector4 a)
		{
			return Vector4.Dot(a, a);
		}

		// Token: 0x06002F6C RID: 12140 RVA: 0x0004DE60 File Offset: 0x0004C060
		public float SqrMagnitude()
		{
			return Vector4.Dot(this, this);
		}

		// Token: 0x04000D8E RID: 3470
		public const float kEpsilon = 1E-05f;

		// Token: 0x04000D8F RID: 3471
		public float x;

		// Token: 0x04000D90 RID: 3472
		public float y;

		// Token: 0x04000D91 RID: 3473
		public float z;

		// Token: 0x04000D92 RID: 3474
		public float w;
	}
}
