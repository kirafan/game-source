﻿using System;

namespace UnityEngine
{
	// Token: 0x020002C7 RID: 711
	public enum RuntimePlatform
	{
		// Token: 0x04000A54 RID: 2644
		OSXEditor,
		// Token: 0x04000A55 RID: 2645
		OSXPlayer,
		// Token: 0x04000A56 RID: 2646
		WindowsPlayer,
		// Token: 0x04000A57 RID: 2647
		[Obsolete("WebPlayer export is no longer supported in Unity 5.4+.")]
		OSXWebPlayer,
		// Token: 0x04000A58 RID: 2648
		OSXDashboardPlayer,
		// Token: 0x04000A59 RID: 2649
		[Obsolete("WebPlayer export is no longer supported in Unity 5.4+.")]
		WindowsWebPlayer,
		// Token: 0x04000A5A RID: 2650
		WindowsEditor = 7,
		// Token: 0x04000A5B RID: 2651
		IPhonePlayer,
		// Token: 0x04000A5C RID: 2652
		[Obsolete("Xbox360 export is no longer supported in Unity 5.5+.")]
		XBOX360 = 10,
		// Token: 0x04000A5D RID: 2653
		[Obsolete("PS3 export is no longer supported in Unity >=5.5.")]
		PS3 = 9,
		// Token: 0x04000A5E RID: 2654
		Android = 11,
		// Token: 0x04000A5F RID: 2655
		[Obsolete("NaCl export is no longer supported in Unity 5.0+.")]
		NaCl,
		// Token: 0x04000A60 RID: 2656
		[Obsolete("FlashPlayer export is no longer supported in Unity 5.0+.")]
		FlashPlayer = 15,
		// Token: 0x04000A61 RID: 2657
		LinuxPlayer = 13,
		// Token: 0x04000A62 RID: 2658
		LinuxEditor = 16,
		// Token: 0x04000A63 RID: 2659
		WebGLPlayer,
		// Token: 0x04000A64 RID: 2660
		[Obsolete("Use WSAPlayerX86 instead")]
		MetroPlayerX86,
		// Token: 0x04000A65 RID: 2661
		WSAPlayerX86 = 18,
		// Token: 0x04000A66 RID: 2662
		[Obsolete("Use WSAPlayerX64 instead")]
		MetroPlayerX64,
		// Token: 0x04000A67 RID: 2663
		WSAPlayerX64 = 19,
		// Token: 0x04000A68 RID: 2664
		[Obsolete("Use WSAPlayerARM instead")]
		MetroPlayerARM,
		// Token: 0x04000A69 RID: 2665
		WSAPlayerARM = 20,
		// Token: 0x04000A6A RID: 2666
		[Obsolete("Windows Phone 8 was removed in 5.3")]
		WP8Player,
		// Token: 0x04000A6B RID: 2667
		[Obsolete("BlackBerryPlayer export is no longer supported in Unity 5.4+.")]
		BlackBerryPlayer,
		// Token: 0x04000A6C RID: 2668
		TizenPlayer,
		// Token: 0x04000A6D RID: 2669
		PSP2,
		// Token: 0x04000A6E RID: 2670
		PS4,
		// Token: 0x04000A6F RID: 2671
		PSM,
		// Token: 0x04000A70 RID: 2672
		XboxOne,
		// Token: 0x04000A71 RID: 2673
		SamsungTVPlayer,
		// Token: 0x04000A72 RID: 2674
		WiiU = 30,
		// Token: 0x04000A73 RID: 2675
		tvOS
	}
}
