﻿using System;

namespace UnityEngine
{
	// Token: 0x020001C8 RID: 456
	public enum AnimatorControllerParameterType
	{
		// Token: 0x040004E0 RID: 1248
		Float = 1,
		// Token: 0x040004E1 RID: 1249
		Int = 3,
		// Token: 0x040004E2 RID: 1250
		Bool,
		// Token: 0x040004E3 RID: 1251
		Trigger = 9
	}
}
