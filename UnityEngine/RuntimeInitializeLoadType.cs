﻿using System;

namespace UnityEngine
{
	// Token: 0x02000365 RID: 869
	public enum RuntimeInitializeLoadType
	{
		// Token: 0x04000D41 RID: 3393
		AfterSceneLoad,
		// Token: 0x04000D42 RID: 3394
		BeforeSceneLoad
	}
}
