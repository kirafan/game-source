﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200005F RID: 95
	[UsedByNativeCode]
	public sealed class ImageEffectTransformsToLDR : Attribute
	{
	}
}
