﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200023D RID: 573
	[RequiredByNativeCode]
	internal class SliderState
	{
		// Token: 0x04000871 RID: 2161
		public float dragStartPos;

		// Token: 0x04000872 RID: 2162
		public float dragStartValue;

		// Token: 0x04000873 RID: 2163
		public bool isDragging;
	}
}
