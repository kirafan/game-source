﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035D RID: 861
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class MultilineAttribute : PropertyAttribute
	{
		// Token: 0x06002DDF RID: 11743 RVA: 0x00048E94 File Offset: 0x00047094
		public MultilineAttribute()
		{
			this.lines = 3;
		}

		// Token: 0x06002DE0 RID: 11744 RVA: 0x00048EA4 File Offset: 0x000470A4
		public MultilineAttribute(int lines)
		{
			this.lines = lines;
		}

		// Token: 0x04000D2D RID: 3373
		public readonly int lines;
	}
}
