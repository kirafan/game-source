﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000CC RID: 204
	public sealed class Texture2D : Texture
	{
		// Token: 0x06000DD7 RID: 3543 RVA: 0x00013228 File Offset: 0x00011428
		public Texture2D(int width, int height)
		{
			Texture2D.Internal_Create(this, width, height, TextureFormat.RGBA32, true, false, IntPtr.Zero);
		}

		// Token: 0x06000DD8 RID: 3544 RVA: 0x00013244 File Offset: 0x00011444
		public Texture2D(int width, int height, TextureFormat format, bool mipmap)
		{
			Texture2D.Internal_Create(this, width, height, format, mipmap, false, IntPtr.Zero);
		}

		// Token: 0x06000DD9 RID: 3545 RVA: 0x00013260 File Offset: 0x00011460
		public Texture2D(int width, int height, TextureFormat format, bool mipmap, bool linear)
		{
			Texture2D.Internal_Create(this, width, height, format, mipmap, linear, IntPtr.Zero);
		}

		// Token: 0x06000DDA RID: 3546 RVA: 0x0001327C File Offset: 0x0001147C
		internal Texture2D(int width, int height, TextureFormat format, bool mipmap, bool linear, IntPtr nativeTex)
		{
			Texture2D.Internal_Create(this, width, height, format, mipmap, linear, nativeTex);
		}

		// Token: 0x17000305 RID: 773
		// (get) Token: 0x06000DDB RID: 3547
		public extern int mipmapCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000DDC RID: 3548
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] Texture2D mono, int width, int height, TextureFormat format, bool mipmap, bool linear, IntPtr nativeTex);

		// Token: 0x06000DDD RID: 3549 RVA: 0x00013294 File Offset: 0x00011494
		public static Texture2D CreateExternalTexture(int width, int height, TextureFormat format, bool mipmap, bool linear, IntPtr nativeTex)
		{
			return new Texture2D(width, height, format, mipmap, linear, nativeTex);
		}

		// Token: 0x06000DDE RID: 3550
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UpdateExternalTexture(IntPtr nativeTex);

		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06000DDF RID: 3551
		public extern TextureFormat format { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06000DE0 RID: 3552
		public static extern Texture2D whiteTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06000DE1 RID: 3553
		public static extern Texture2D blackTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000DE2 RID: 3554 RVA: 0x000132B8 File Offset: 0x000114B8
		public void SetPixel(int x, int y, Color color)
		{
			Texture2D.INTERNAL_CALL_SetPixel(this, x, y, ref color);
		}

		// Token: 0x06000DE3 RID: 3555
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetPixel(Texture2D self, int x, int y, ref Color color);

		// Token: 0x06000DE4 RID: 3556 RVA: 0x000132C8 File Offset: 0x000114C8
		public Color GetPixel(int x, int y)
		{
			Color result;
			Texture2D.INTERNAL_CALL_GetPixel(this, x, y, out result);
			return result;
		}

		// Token: 0x06000DE5 RID: 3557
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPixel(Texture2D self, int x, int y, out Color value);

		// Token: 0x06000DE6 RID: 3558 RVA: 0x000132E8 File Offset: 0x000114E8
		public Color GetPixelBilinear(float u, float v)
		{
			Color result;
			Texture2D.INTERNAL_CALL_GetPixelBilinear(this, u, v, out result);
			return result;
		}

		// Token: 0x06000DE7 RID: 3559
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPixelBilinear(Texture2D self, float u, float v, out Color value);

		// Token: 0x06000DE8 RID: 3560 RVA: 0x00013308 File Offset: 0x00011508
		[ExcludeFromDocs]
		public void SetPixels(Color[] colors)
		{
			int miplevel = 0;
			this.SetPixels(colors, miplevel);
		}

		// Token: 0x06000DE9 RID: 3561 RVA: 0x00013320 File Offset: 0x00011520
		public void SetPixels(Color[] colors, [DefaultValue("0")] int miplevel)
		{
			int num = this.width >> miplevel;
			if (num < 1)
			{
				num = 1;
			}
			int num2 = this.height >> miplevel;
			if (num2 < 1)
			{
				num2 = 1;
			}
			this.SetPixels(0, 0, num, num2, colors, miplevel);
		}

		// Token: 0x06000DEA RID: 3562
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels(int x, int y, int blockWidth, int blockHeight, Color[] colors, [DefaultValue("0")] int miplevel);

		// Token: 0x06000DEB RID: 3563 RVA: 0x00013364 File Offset: 0x00011564
		[ExcludeFromDocs]
		public void SetPixels(int x, int y, int blockWidth, int blockHeight, Color[] colors)
		{
			int miplevel = 0;
			this.SetPixels(x, y, blockWidth, blockHeight, colors, miplevel);
		}

		// Token: 0x06000DEC RID: 3564
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetAllPixels32(Color32[] colors, int miplevel);

		// Token: 0x06000DED RID: 3565
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetBlockOfPixels32(int x, int y, int blockWidth, int blockHeight, Color32[] colors, int miplevel);

		// Token: 0x06000DEE RID: 3566 RVA: 0x00013384 File Offset: 0x00011584
		[ExcludeFromDocs]
		public void SetPixels32(Color32[] colors)
		{
			int miplevel = 0;
			this.SetPixels32(colors, miplevel);
		}

		// Token: 0x06000DEF RID: 3567 RVA: 0x0001339C File Offset: 0x0001159C
		public void SetPixels32(Color32[] colors, [DefaultValue("0")] int miplevel)
		{
			this.SetAllPixels32(colors, miplevel);
		}

		// Token: 0x06000DF0 RID: 3568 RVA: 0x000133A8 File Offset: 0x000115A8
		[ExcludeFromDocs]
		public void SetPixels32(int x, int y, int blockWidth, int blockHeight, Color32[] colors)
		{
			int miplevel = 0;
			this.SetPixels32(x, y, blockWidth, blockHeight, colors, miplevel);
		}

		// Token: 0x06000DF1 RID: 3569 RVA: 0x000133C8 File Offset: 0x000115C8
		public void SetPixels32(int x, int y, int blockWidth, int blockHeight, Color32[] colors, [DefaultValue("0")] int miplevel)
		{
			this.SetBlockOfPixels32(x, y, blockWidth, blockHeight, colors, miplevel);
		}

		// Token: 0x06000DF2 RID: 3570
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool LoadImage(byte[] data, [DefaultValue("false")] bool markNonReadable);

		// Token: 0x06000DF3 RID: 3571 RVA: 0x000133DC File Offset: 0x000115DC
		[ExcludeFromDocs]
		public bool LoadImage(byte[] data)
		{
			bool markNonReadable = false;
			return this.LoadImage(data, markNonReadable);
		}

		// Token: 0x06000DF4 RID: 3572
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void LoadRawTextureData_ImplArray(byte[] data);

		// Token: 0x06000DF5 RID: 3573
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void LoadRawTextureData_ImplPointer(IntPtr data, int size);

		// Token: 0x06000DF6 RID: 3574 RVA: 0x000133FC File Offset: 0x000115FC
		public void LoadRawTextureData(byte[] data)
		{
			this.LoadRawTextureData_ImplArray(data);
		}

		// Token: 0x06000DF7 RID: 3575 RVA: 0x00013408 File Offset: 0x00011608
		public void LoadRawTextureData(IntPtr data, int size)
		{
			this.LoadRawTextureData_ImplPointer(data, size);
		}

		// Token: 0x06000DF8 RID: 3576
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern byte[] GetRawTextureData();

		// Token: 0x06000DF9 RID: 3577 RVA: 0x00013414 File Offset: 0x00011614
		[ExcludeFromDocs]
		public Color[] GetPixels()
		{
			int miplevel = 0;
			return this.GetPixels(miplevel);
		}

		// Token: 0x06000DFA RID: 3578 RVA: 0x00013434 File Offset: 0x00011634
		public Color[] GetPixels([DefaultValue("0")] int miplevel)
		{
			int num = this.width >> miplevel;
			if (num < 1)
			{
				num = 1;
			}
			int num2 = this.height >> miplevel;
			if (num2 < 1)
			{
				num2 = 1;
			}
			return this.GetPixels(0, 0, num, num2, miplevel);
		}

		// Token: 0x06000DFB RID: 3579
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(int x, int y, int blockWidth, int blockHeight, [DefaultValue("0")] int miplevel);

		// Token: 0x06000DFC RID: 3580 RVA: 0x00013480 File Offset: 0x00011680
		[ExcludeFromDocs]
		public Color[] GetPixels(int x, int y, int blockWidth, int blockHeight)
		{
			int miplevel = 0;
			return this.GetPixels(x, y, blockWidth, blockHeight, miplevel);
		}

		// Token: 0x06000DFD RID: 3581
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color32[] GetPixels32([DefaultValue("0")] int miplevel);

		// Token: 0x06000DFE RID: 3582 RVA: 0x000134A4 File Offset: 0x000116A4
		[ExcludeFromDocs]
		public Color32[] GetPixels32()
		{
			int miplevel = 0;
			return this.GetPixels32(miplevel);
		}

		// Token: 0x06000DFF RID: 3583
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Apply([DefaultValue("true")] bool updateMipmaps, [DefaultValue("false")] bool makeNoLongerReadable);

		// Token: 0x06000E00 RID: 3584 RVA: 0x000134C4 File Offset: 0x000116C4
		[ExcludeFromDocs]
		public void Apply(bool updateMipmaps)
		{
			bool makeNoLongerReadable = false;
			this.Apply(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06000E01 RID: 3585 RVA: 0x000134DC File Offset: 0x000116DC
		[ExcludeFromDocs]
		public void Apply()
		{
			bool makeNoLongerReadable = false;
			bool updateMipmaps = true;
			this.Apply(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06000E02 RID: 3586
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool Resize(int width, int height, TextureFormat format, bool hasMipMap);

		// Token: 0x06000E03 RID: 3587 RVA: 0x000134F8 File Offset: 0x000116F8
		public bool Resize(int width, int height)
		{
			return this.Internal_ResizeWH(width, height);
		}

		// Token: 0x06000E04 RID: 3588
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool Internal_ResizeWH(int width, int height);

		// Token: 0x06000E05 RID: 3589 RVA: 0x00013518 File Offset: 0x00011718
		public void Compress(bool highQuality)
		{
			Texture2D.INTERNAL_CALL_Compress(this, highQuality);
		}

		// Token: 0x06000E06 RID: 3590
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Compress(Texture2D self, bool highQuality);

		// Token: 0x06000E07 RID: 3591
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Rect[] PackTextures(Texture2D[] textures, int padding, [DefaultValue("2048")] int maximumAtlasSize, [DefaultValue("false")] bool makeNoLongerReadable);

		// Token: 0x06000E08 RID: 3592 RVA: 0x00013524 File Offset: 0x00011724
		[ExcludeFromDocs]
		public Rect[] PackTextures(Texture2D[] textures, int padding, int maximumAtlasSize)
		{
			bool makeNoLongerReadable = false;
			return this.PackTextures(textures, padding, maximumAtlasSize, makeNoLongerReadable);
		}

		// Token: 0x06000E09 RID: 3593 RVA: 0x00013548 File Offset: 0x00011748
		[ExcludeFromDocs]
		public Rect[] PackTextures(Texture2D[] textures, int padding)
		{
			bool makeNoLongerReadable = false;
			int maximumAtlasSize = 2048;
			return this.PackTextures(textures, padding, maximumAtlasSize, makeNoLongerReadable);
		}

		// Token: 0x06000E0A RID: 3594 RVA: 0x00013570 File Offset: 0x00011770
		public void ReadPixels(Rect source, int destX, int destY, [DefaultValue("true")] bool recalculateMipMaps)
		{
			Texture2D.INTERNAL_CALL_ReadPixels(this, ref source, destX, destY, recalculateMipMaps);
		}

		// Token: 0x06000E0B RID: 3595 RVA: 0x00013580 File Offset: 0x00011780
		[ExcludeFromDocs]
		public void ReadPixels(Rect source, int destX, int destY)
		{
			bool recalculateMipMaps = true;
			Texture2D.INTERNAL_CALL_ReadPixels(this, ref source, destX, destY, recalculateMipMaps);
		}

		// Token: 0x06000E0C RID: 3596
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ReadPixels(Texture2D self, ref Rect source, int destX, int destY, bool recalculateMipMaps);

		// Token: 0x06000E0D RID: 3597
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern byte[] EncodeToPNG();

		// Token: 0x06000E0E RID: 3598
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern byte[] EncodeToJPG(int quality);

		// Token: 0x06000E0F RID: 3599 RVA: 0x0001359C File Offset: 0x0001179C
		public byte[] EncodeToJPG()
		{
			return this.EncodeToJPG(75);
		}
	}
}
