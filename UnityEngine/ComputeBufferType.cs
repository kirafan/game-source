﻿using System;

namespace UnityEngine
{
	// Token: 0x020002E7 RID: 743
	[Flags]
	public enum ComputeBufferType
	{
		// Token: 0x04000AF1 RID: 2801
		Default = 0,
		// Token: 0x04000AF2 RID: 2802
		Raw = 1,
		// Token: 0x04000AF3 RID: 2803
		Append = 2,
		// Token: 0x04000AF4 RID: 2804
		Counter = 4,
		// Token: 0x04000AF5 RID: 2805
		[Obsolete("Enum member DrawIndirect has been deprecated. Use IndirectArguments instead (UnityUpgradable) -> IndirectArguments", false)]
		DrawIndirect = 256,
		// Token: 0x04000AF6 RID: 2806
		IndirectArguments = 256,
		// Token: 0x04000AF7 RID: 2807
		GPUMemory = 512
	}
}
