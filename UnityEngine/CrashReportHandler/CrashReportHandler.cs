﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.CrashReportHandler
{
	// Token: 0x0200028A RID: 650
	public static class CrashReportHandler
	{
		// Token: 0x17000A12 RID: 2578
		// (get) Token: 0x06002A4C RID: 10828
		// (set) Token: 0x06002A4D RID: 10829
		[ThreadAndSerializationSafe]
		public static extern bool enableCaptureExceptions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
