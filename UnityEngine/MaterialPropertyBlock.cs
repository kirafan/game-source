﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200004F RID: 79
	public sealed class MaterialPropertyBlock
	{
		// Token: 0x060005D6 RID: 1494 RVA: 0x00007D18 File Offset: 0x00005F18
		public MaterialPropertyBlock()
		{
			this.InitBlock();
		}

		// Token: 0x060005D7 RID: 1495
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void InitBlock();

		// Token: 0x060005D8 RID: 1496
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void DestroyBlock();

		// Token: 0x060005D9 RID: 1497 RVA: 0x00007D28 File Offset: 0x00005F28
		~MaterialPropertyBlock()
		{
			this.DestroyBlock();
		}

		// Token: 0x1700016B RID: 363
		// (get) Token: 0x060005DA RID: 1498
		public extern bool isEmpty { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060005DB RID: 1499 RVA: 0x00007D58 File Offset: 0x00005F58
		public void SetFloat(string name, float value)
		{
			this.SetFloat(Shader.PropertyToID(name), value);
		}

		// Token: 0x060005DC RID: 1500
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetFloat(int nameID, float value);

		// Token: 0x060005DD RID: 1501 RVA: 0x00007D68 File Offset: 0x00005F68
		public void SetVector(string name, Vector4 value)
		{
			this.SetVector(Shader.PropertyToID(name), value);
		}

		// Token: 0x060005DE RID: 1502 RVA: 0x00007D78 File Offset: 0x00005F78
		public void SetVector(int nameID, Vector4 value)
		{
			MaterialPropertyBlock.INTERNAL_CALL_SetVector(this, nameID, ref value);
		}

		// Token: 0x060005DF RID: 1503
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetVector(MaterialPropertyBlock self, int nameID, ref Vector4 value);

		// Token: 0x060005E0 RID: 1504 RVA: 0x00007D84 File Offset: 0x00005F84
		public void SetColor(string name, Color value)
		{
			this.SetColor(Shader.PropertyToID(name), value);
		}

		// Token: 0x060005E1 RID: 1505 RVA: 0x00007D94 File Offset: 0x00005F94
		public void SetColor(int nameID, Color value)
		{
			MaterialPropertyBlock.INTERNAL_CALL_SetColor(this, nameID, ref value);
		}

		// Token: 0x060005E2 RID: 1506
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetColor(MaterialPropertyBlock self, int nameID, ref Color value);

		// Token: 0x060005E3 RID: 1507 RVA: 0x00007DA0 File Offset: 0x00005FA0
		public void SetMatrix(string name, Matrix4x4 value)
		{
			this.SetMatrix(Shader.PropertyToID(name), value);
		}

		// Token: 0x060005E4 RID: 1508 RVA: 0x00007DB0 File Offset: 0x00005FB0
		public void SetMatrix(int nameID, Matrix4x4 value)
		{
			MaterialPropertyBlock.INTERNAL_CALL_SetMatrix(this, nameID, ref value);
		}

		// Token: 0x060005E5 RID: 1509
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetMatrix(MaterialPropertyBlock self, int nameID, ref Matrix4x4 value);

		// Token: 0x060005E6 RID: 1510 RVA: 0x00007DBC File Offset: 0x00005FBC
		public void SetTexture(string name, Texture value)
		{
			this.SetTexture(Shader.PropertyToID(name), value);
		}

		// Token: 0x060005E7 RID: 1511
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTexture(int nameID, Texture value);

		// Token: 0x060005E8 RID: 1512 RVA: 0x00007DCC File Offset: 0x00005FCC
		public void SetBuffer(string name, ComputeBuffer value)
		{
			this.SetBuffer(Shader.PropertyToID(name), value);
		}

		// Token: 0x060005E9 RID: 1513
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetBuffer(int nameID, ComputeBuffer value);

		// Token: 0x060005EA RID: 1514 RVA: 0x00007DDC File Offset: 0x00005FDC
		public float GetFloat(string name)
		{
			return this.GetFloat(Shader.PropertyToID(name));
		}

		// Token: 0x060005EB RID: 1515
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetFloat(int nameID);

		// Token: 0x060005EC RID: 1516 RVA: 0x00007E00 File Offset: 0x00006000
		public Vector4 GetVector(string name)
		{
			return this.GetVector(Shader.PropertyToID(name));
		}

		// Token: 0x060005ED RID: 1517 RVA: 0x00007E24 File Offset: 0x00006024
		public Vector4 GetVector(int nameID)
		{
			Vector4 result;
			MaterialPropertyBlock.INTERNAL_CALL_GetVector(this, nameID, out result);
			return result;
		}

		// Token: 0x060005EE RID: 1518
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetVector(MaterialPropertyBlock self, int nameID, out Vector4 value);

		// Token: 0x060005EF RID: 1519 RVA: 0x00007E44 File Offset: 0x00006044
		public Matrix4x4 GetMatrix(string name)
		{
			return this.GetMatrix(Shader.PropertyToID(name));
		}

		// Token: 0x060005F0 RID: 1520 RVA: 0x00007E68 File Offset: 0x00006068
		public Matrix4x4 GetMatrix(int nameID)
		{
			Matrix4x4 result;
			MaterialPropertyBlock.INTERNAL_CALL_GetMatrix(this, nameID, out result);
			return result;
		}

		// Token: 0x060005F1 RID: 1521
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetMatrix(MaterialPropertyBlock self, int nameID, out Matrix4x4 value);

		// Token: 0x060005F2 RID: 1522 RVA: 0x00007E88 File Offset: 0x00006088
		public void GetFloatArray(string name, List<float> values)
		{
			this.GetFloatArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x060005F3 RID: 1523 RVA: 0x00007E98 File Offset: 0x00006098
		public void GetFloatArray(int nameID, List<float> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			this.GetFloatArrayImplList(nameID, values);
		}

		// Token: 0x060005F4 RID: 1524
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetFloatArrayImplList(int nameID, object list);

		// Token: 0x060005F5 RID: 1525 RVA: 0x00007EB4 File Offset: 0x000060B4
		public float[] GetFloatArray(string name)
		{
			return this.GetFloatArray(Shader.PropertyToID(name));
		}

		// Token: 0x060005F6 RID: 1526
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float[] GetFloatArray(int nameID);

		// Token: 0x060005F7 RID: 1527 RVA: 0x00007ED8 File Offset: 0x000060D8
		public void GetVectorArray(string name, List<Vector4> values)
		{
			this.GetVectorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x060005F8 RID: 1528 RVA: 0x00007EE8 File Offset: 0x000060E8
		public void GetVectorArray(int nameID, List<Vector4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			this.GetVectorArrayImplList(nameID, values);
		}

		// Token: 0x060005F9 RID: 1529
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVectorArrayImplList(int nameID, object list);

		// Token: 0x060005FA RID: 1530 RVA: 0x00007F04 File Offset: 0x00006104
		public Vector4[] GetVectorArray(string name)
		{
			return this.GetVectorArray(Shader.PropertyToID(name));
		}

		// Token: 0x060005FB RID: 1531
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Vector4[] GetVectorArray(int nameID);

		// Token: 0x060005FC RID: 1532 RVA: 0x00007F28 File Offset: 0x00006128
		public void GetMatrixArray(string name, List<Matrix4x4> values)
		{
			this.GetMatrixArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x060005FD RID: 1533 RVA: 0x00007F38 File Offset: 0x00006138
		public void GetMatrixArray(int nameID, List<Matrix4x4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			this.GetMatrixArrayImplList(nameID, values);
		}

		// Token: 0x060005FE RID: 1534
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetMatrixArrayImplList(int nameID, object list);

		// Token: 0x060005FF RID: 1535 RVA: 0x00007F54 File Offset: 0x00006154
		public Matrix4x4[] GetMatrixArray(string name)
		{
			return this.GetMatrixArray(Shader.PropertyToID(name));
		}

		// Token: 0x06000600 RID: 1536
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Matrix4x4[] GetMatrixArray(int nameID);

		// Token: 0x06000601 RID: 1537 RVA: 0x00007F78 File Offset: 0x00006178
		public Texture GetTexture(string name)
		{
			return this.GetTexture(Shader.PropertyToID(name));
		}

		// Token: 0x06000602 RID: 1538
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Texture GetTexture(int nameID);

		// Token: 0x06000603 RID: 1539 RVA: 0x00007F9C File Offset: 0x0000619C
		public void SetFloatArray(string name, List<float> values)
		{
			this.SetFloatArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000604 RID: 1540 RVA: 0x00007FAC File Offset: 0x000061AC
		public void SetFloatArray(int nameID, List<float> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetFloatArrayImplList(nameID, values);
		}

		// Token: 0x06000605 RID: 1541 RVA: 0x00007FE0 File Offset: 0x000061E0
		public void SetFloatArray(string name, float[] values)
		{
			this.SetFloatArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000606 RID: 1542 RVA: 0x00007FF0 File Offset: 0x000061F0
		public void SetFloatArray(int nameID, float[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetFloatArrayImpl(nameID, values);
		}

		// Token: 0x06000607 RID: 1543
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatArrayImpl(int nameID, float[] values);

		// Token: 0x06000608 RID: 1544
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatArrayImplList(int nameID, object list);

		// Token: 0x06000609 RID: 1545 RVA: 0x00008020 File Offset: 0x00006220
		public void SetVectorArray(string name, List<Vector4> values)
		{
			this.SetVectorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x0600060A RID: 1546 RVA: 0x00008030 File Offset: 0x00006230
		public void SetVectorArray(int nameID, List<Vector4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetVectorArrayImplList(nameID, values);
		}

		// Token: 0x0600060B RID: 1547 RVA: 0x00008064 File Offset: 0x00006264
		public void SetVectorArray(string name, Vector4[] values)
		{
			this.SetVectorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x0600060C RID: 1548 RVA: 0x00008074 File Offset: 0x00006274
		public void SetVectorArray(int nameID, Vector4[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetVectorArrayImpl(nameID, values);
		}

		// Token: 0x0600060D RID: 1549
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVectorArrayImpl(int nameID, Vector4[] values);

		// Token: 0x0600060E RID: 1550
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVectorArrayImplList(int nameID, object list);

		// Token: 0x0600060F RID: 1551 RVA: 0x000080A4 File Offset: 0x000062A4
		public void SetMatrixArray(string name, List<Matrix4x4> values)
		{
			this.SetMatrixArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000610 RID: 1552 RVA: 0x000080B4 File Offset: 0x000062B4
		public void SetMatrixArray(int nameID, List<Matrix4x4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetMatrixArrayImplList(nameID, values);
		}

		// Token: 0x06000611 RID: 1553 RVA: 0x000080E8 File Offset: 0x000062E8
		public void SetMatrixArray(string name, Matrix4x4[] values)
		{
			this.SetMatrixArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000612 RID: 1554 RVA: 0x000080F8 File Offset: 0x000062F8
		public void SetMatrixArray(int nameID, Matrix4x4[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetMatrixArrayImpl(nameID, values);
		}

		// Token: 0x06000613 RID: 1555
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetMatrixArrayImpl(int nameID, Matrix4x4[] values);

		// Token: 0x06000614 RID: 1556
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetMatrixArrayImplList(int nameID, object list);

		// Token: 0x06000615 RID: 1557
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Clear();

		// Token: 0x04000071 RID: 113
		internal IntPtr m_Ptr;
	}
}
