﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C3 RID: 195
	public enum ProceduralCacheSize
	{
		// Token: 0x040001DD RID: 477
		Tiny,
		// Token: 0x040001DE RID: 478
		Medium,
		// Token: 0x040001DF RID: 479
		Heavy,
		// Token: 0x040001E0 RID: 480
		NoLimit,
		// Token: 0x040001E1 RID: 481
		None
	}
}
