﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001E3 RID: 483
	public sealed class Avatar : Object
	{
		// Token: 0x0600200B RID: 8203 RVA: 0x000245E4 File Offset: 0x000227E4
		private Avatar()
		{
		}

		// Token: 0x17000804 RID: 2052
		// (get) Token: 0x0600200C RID: 8204
		public extern bool isValid { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000805 RID: 2053
		// (get) Token: 0x0600200D RID: 8205
		public extern bool isHuman { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600200E RID: 8206
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetMuscleMinMax(int muscleId, float min, float max);

		// Token: 0x0600200F RID: 8207
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetParameter(int parameterId, float value);

		// Token: 0x06002010 RID: 8208
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern float GetAxisLength(int humanId);

		// Token: 0x06002011 RID: 8209 RVA: 0x000245F0 File Offset: 0x000227F0
		internal Quaternion GetPreRotation(int humanId)
		{
			Quaternion result;
			Avatar.INTERNAL_CALL_GetPreRotation(this, humanId, out result);
			return result;
		}

		// Token: 0x06002012 RID: 8210
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPreRotation(Avatar self, int humanId, out Quaternion value);

		// Token: 0x06002013 RID: 8211 RVA: 0x00024610 File Offset: 0x00022810
		internal Quaternion GetPostRotation(int humanId)
		{
			Quaternion result;
			Avatar.INTERNAL_CALL_GetPostRotation(this, humanId, out result);
			return result;
		}

		// Token: 0x06002014 RID: 8212
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPostRotation(Avatar self, int humanId, out Quaternion value);

		// Token: 0x06002015 RID: 8213 RVA: 0x00024630 File Offset: 0x00022830
		internal Quaternion GetZYPostQ(int humanId, Quaternion parentQ, Quaternion q)
		{
			Quaternion result;
			Avatar.INTERNAL_CALL_GetZYPostQ(this, humanId, ref parentQ, ref q, out result);
			return result;
		}

		// Token: 0x06002016 RID: 8214
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetZYPostQ(Avatar self, int humanId, ref Quaternion parentQ, ref Quaternion q, out Quaternion value);

		// Token: 0x06002017 RID: 8215 RVA: 0x00024654 File Offset: 0x00022854
		internal Quaternion GetZYRoll(int humanId, Vector3 uvw)
		{
			Quaternion result;
			Avatar.INTERNAL_CALL_GetZYRoll(this, humanId, ref uvw, out result);
			return result;
		}

		// Token: 0x06002018 RID: 8216
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetZYRoll(Avatar self, int humanId, ref Vector3 uvw, out Quaternion value);

		// Token: 0x06002019 RID: 8217 RVA: 0x00024678 File Offset: 0x00022878
		internal Vector3 GetLimitSign(int humanId)
		{
			Vector3 result;
			Avatar.INTERNAL_CALL_GetLimitSign(this, humanId, out result);
			return result;
		}

		// Token: 0x0600201A RID: 8218
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetLimitSign(Avatar self, int humanId, out Vector3 value);
	}
}
