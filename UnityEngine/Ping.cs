﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000A0 RID: 160
	public sealed class Ping
	{
		// Token: 0x06000B26 RID: 2854
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Ping(string address);

		// Token: 0x06000B27 RID: 2855
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void DestroyPing();

		// Token: 0x06000B28 RID: 2856 RVA: 0x0000FB48 File Offset: 0x0000DD48
		~Ping()
		{
			this.DestroyPing();
		}

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x06000B29 RID: 2857
		public extern bool isDone { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000291 RID: 657
		// (get) Token: 0x06000B2A RID: 2858
		public extern int time { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000292 RID: 658
		// (get) Token: 0x06000B2B RID: 2859
		public extern string ip { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x04000175 RID: 373
		internal IntPtr m_Ptr;
	}
}
