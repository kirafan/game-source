﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200034E RID: 846
	public interface IUserProfile
	{
		// Token: 0x17000ABC RID: 2748
		// (get) Token: 0x06002D9B RID: 11675
		string userName { get; }

		// Token: 0x17000ABD RID: 2749
		// (get) Token: 0x06002D9C RID: 11676
		string id { get; }

		// Token: 0x17000ABE RID: 2750
		// (get) Token: 0x06002D9D RID: 11677
		bool isFriend { get; }

		// Token: 0x17000ABF RID: 2751
		// (get) Token: 0x06002D9E RID: 11678
		UserState state { get; }

		// Token: 0x17000AC0 RID: 2752
		// (get) Token: 0x06002D9F RID: 11679
		Texture2D image { get; }
	}
}
