﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000350 RID: 848
	public interface IAchievementDescription
	{
		// Token: 0x17000AC6 RID: 2758
		// (get) Token: 0x06002DA8 RID: 11688
		// (set) Token: 0x06002DA9 RID: 11689
		string id { get; set; }

		// Token: 0x17000AC7 RID: 2759
		// (get) Token: 0x06002DAA RID: 11690
		string title { get; }

		// Token: 0x17000AC8 RID: 2760
		// (get) Token: 0x06002DAB RID: 11691
		Texture2D image { get; }

		// Token: 0x17000AC9 RID: 2761
		// (get) Token: 0x06002DAC RID: 11692
		string achievedDescription { get; }

		// Token: 0x17000ACA RID: 2762
		// (get) Token: 0x06002DAD RID: 11693
		string unachievedDescription { get; }

		// Token: 0x17000ACB RID: 2763
		// (get) Token: 0x06002DAE RID: 11694
		bool hidden { get; }

		// Token: 0x17000ACC RID: 2764
		// (get) Token: 0x06002DAF RID: 11695
		int points { get; }
	}
}
