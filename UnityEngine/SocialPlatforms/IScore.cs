﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000351 RID: 849
	public interface IScore
	{
		// Token: 0x06002DB0 RID: 11696
		void ReportScore(Action<bool> callback);

		// Token: 0x17000ACD RID: 2765
		// (get) Token: 0x06002DB1 RID: 11697
		// (set) Token: 0x06002DB2 RID: 11698
		string leaderboardID { get; set; }

		// Token: 0x17000ACE RID: 2766
		// (get) Token: 0x06002DB3 RID: 11699
		// (set) Token: 0x06002DB4 RID: 11700
		long value { get; set; }

		// Token: 0x17000ACF RID: 2767
		// (get) Token: 0x06002DB5 RID: 11701
		DateTime date { get; }

		// Token: 0x17000AD0 RID: 2768
		// (get) Token: 0x06002DB6 RID: 11702
		string formattedValue { get; }

		// Token: 0x17000AD1 RID: 2769
		// (get) Token: 0x06002DB7 RID: 11703
		string userID { get; }

		// Token: 0x17000AD2 RID: 2770
		// (get) Token: 0x06002DB8 RID: 11704
		int rank { get; }
	}
}
