﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200034A RID: 842
	internal static class ActivePlatform
	{
		// Token: 0x17000AB7 RID: 2743
		// (get) Token: 0x06002D82 RID: 11650 RVA: 0x00048B4C File Offset: 0x00046D4C
		// (set) Token: 0x06002D83 RID: 11651 RVA: 0x00048B7C File Offset: 0x00046D7C
		internal static ISocialPlatform Instance
		{
			get
			{
				if (ActivePlatform._active == null)
				{
					ActivePlatform._active = ActivePlatform.SelectSocialPlatform();
				}
				return ActivePlatform._active;
			}
			set
			{
				ActivePlatform._active = value;
			}
		}

		// Token: 0x06002D84 RID: 11652 RVA: 0x00048B88 File Offset: 0x00046D88
		private static ISocialPlatform SelectSocialPlatform()
		{
			return new Local();
		}

		// Token: 0x04000D13 RID: 3347
		private static ISocialPlatform _active;
	}
}
