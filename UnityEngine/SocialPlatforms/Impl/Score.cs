﻿using System;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x0200033F RID: 831
	public class Score : IScore
	{
		// Token: 0x06002D21 RID: 11553 RVA: 0x00047308 File Offset: 0x00045508
		public Score() : this("unkown", -1L)
		{
		}

		// Token: 0x06002D22 RID: 11554 RVA: 0x00047318 File Offset: 0x00045518
		public Score(string leaderboardID, long value) : this(leaderboardID, value, "0", DateTime.Now, "", -1)
		{
		}

		// Token: 0x06002D23 RID: 11555 RVA: 0x00047334 File Offset: 0x00045534
		public Score(string leaderboardID, long value, string userID, DateTime date, string formattedValue, int rank)
		{
			this.leaderboardID = leaderboardID;
			this.value = value;
			this.m_UserID = userID;
			this.m_Date = date;
			this.m_FormattedValue = formattedValue;
			this.m_Rank = rank;
		}

		// Token: 0x06002D24 RID: 11556 RVA: 0x0004736C File Offset: 0x0004556C
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"Rank: '",
				this.m_Rank,
				"' Value: '",
				this.value,
				"' Category: '",
				this.leaderboardID,
				"' PlayerID: '",
				this.m_UserID,
				"' Date: '",
				this.m_Date
			});
		}

		// Token: 0x06002D25 RID: 11557 RVA: 0x000473F4 File Offset: 0x000455F4
		public void ReportScore(Action<bool> callback)
		{
			ActivePlatform.Instance.ReportScore(this.value, this.leaderboardID, callback);
		}

		// Token: 0x06002D26 RID: 11558 RVA: 0x00047410 File Offset: 0x00045610
		public void SetDate(DateTime date)
		{
			this.m_Date = date;
		}

		// Token: 0x06002D27 RID: 11559 RVA: 0x0004741C File Offset: 0x0004561C
		public void SetFormattedValue(string value)
		{
			this.m_FormattedValue = value;
		}

		// Token: 0x06002D28 RID: 11560 RVA: 0x00047428 File Offset: 0x00045628
		public void SetUserID(string userID)
		{
			this.m_UserID = userID;
		}

		// Token: 0x06002D29 RID: 11561 RVA: 0x00047434 File Offset: 0x00045634
		public void SetRank(int rank)
		{
			this.m_Rank = rank;
		}

		// Token: 0x17000AA5 RID: 2725
		// (get) Token: 0x06002D2A RID: 11562 RVA: 0x00047440 File Offset: 0x00045640
		// (set) Token: 0x06002D2B RID: 11563 RVA: 0x0004745C File Offset: 0x0004565C
		public string leaderboardID { get; set; }

		// Token: 0x17000AA6 RID: 2726
		// (get) Token: 0x06002D2C RID: 11564 RVA: 0x00047468 File Offset: 0x00045668
		// (set) Token: 0x06002D2D RID: 11565 RVA: 0x00047484 File Offset: 0x00045684
		public long value { get; set; }

		// Token: 0x17000AA7 RID: 2727
		// (get) Token: 0x06002D2E RID: 11566 RVA: 0x00047490 File Offset: 0x00045690
		public DateTime date
		{
			get
			{
				return this.m_Date;
			}
		}

		// Token: 0x17000AA8 RID: 2728
		// (get) Token: 0x06002D2F RID: 11567 RVA: 0x000474AC File Offset: 0x000456AC
		public string formattedValue
		{
			get
			{
				return this.m_FormattedValue;
			}
		}

		// Token: 0x17000AA9 RID: 2729
		// (get) Token: 0x06002D30 RID: 11568 RVA: 0x000474C8 File Offset: 0x000456C8
		public string userID
		{
			get
			{
				return this.m_UserID;
			}
		}

		// Token: 0x17000AAA RID: 2730
		// (get) Token: 0x06002D31 RID: 11569 RVA: 0x000474E4 File Offset: 0x000456E4
		public int rank
		{
			get
			{
				return this.m_Rank;
			}
		}

		// Token: 0x04000CED RID: 3309
		private DateTime m_Date;

		// Token: 0x04000CEE RID: 3310
		private string m_FormattedValue;

		// Token: 0x04000CEF RID: 3311
		private string m_UserID;

		// Token: 0x04000CF0 RID: 3312
		private int m_Rank;
	}
}
