﻿using System;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x0200033E RID: 830
	public class AchievementDescription : IAchievementDescription
	{
		// Token: 0x06002D16 RID: 11542 RVA: 0x00047160 File Offset: 0x00045360
		public AchievementDescription(string id, string title, Texture2D image, string achievedDescription, string unachievedDescription, bool hidden, int points)
		{
			this.id = id;
			this.m_Title = title;
			this.m_Image = image;
			this.m_AchievedDescription = achievedDescription;
			this.m_UnachievedDescription = unachievedDescription;
			this.m_Hidden = hidden;
			this.m_Points = points;
		}

		// Token: 0x06002D17 RID: 11543 RVA: 0x000471A0 File Offset: 0x000453A0
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.id,
				" - ",
				this.title,
				" - ",
				this.achievedDescription,
				" - ",
				this.unachievedDescription,
				" - ",
				this.points,
				" - ",
				this.hidden
			});
		}

		// Token: 0x06002D18 RID: 11544 RVA: 0x0004722C File Offset: 0x0004542C
		public void SetImage(Texture2D image)
		{
			this.m_Image = image;
		}

		// Token: 0x17000A9E RID: 2718
		// (get) Token: 0x06002D19 RID: 11545 RVA: 0x00047238 File Offset: 0x00045438
		// (set) Token: 0x06002D1A RID: 11546 RVA: 0x00047254 File Offset: 0x00045454
		public string id { get; set; }

		// Token: 0x17000A9F RID: 2719
		// (get) Token: 0x06002D1B RID: 11547 RVA: 0x00047260 File Offset: 0x00045460
		public string title
		{
			get
			{
				return this.m_Title;
			}
		}

		// Token: 0x17000AA0 RID: 2720
		// (get) Token: 0x06002D1C RID: 11548 RVA: 0x0004727C File Offset: 0x0004547C
		public Texture2D image
		{
			get
			{
				return this.m_Image;
			}
		}

		// Token: 0x17000AA1 RID: 2721
		// (get) Token: 0x06002D1D RID: 11549 RVA: 0x00047298 File Offset: 0x00045498
		public string achievedDescription
		{
			get
			{
				return this.m_AchievedDescription;
			}
		}

		// Token: 0x17000AA2 RID: 2722
		// (get) Token: 0x06002D1E RID: 11550 RVA: 0x000472B4 File Offset: 0x000454B4
		public string unachievedDescription
		{
			get
			{
				return this.m_UnachievedDescription;
			}
		}

		// Token: 0x17000AA3 RID: 2723
		// (get) Token: 0x06002D1F RID: 11551 RVA: 0x000472D0 File Offset: 0x000454D0
		public bool hidden
		{
			get
			{
				return this.m_Hidden;
			}
		}

		// Token: 0x17000AA4 RID: 2724
		// (get) Token: 0x06002D20 RID: 11552 RVA: 0x000472EC File Offset: 0x000454EC
		public int points
		{
			get
			{
				return this.m_Points;
			}
		}

		// Token: 0x04000CE6 RID: 3302
		private string m_Title;

		// Token: 0x04000CE7 RID: 3303
		private Texture2D m_Image;

		// Token: 0x04000CE8 RID: 3304
		private string m_AchievedDescription;

		// Token: 0x04000CE9 RID: 3305
		private string m_UnachievedDescription;

		// Token: 0x04000CEA RID: 3306
		private bool m_Hidden;

		// Token: 0x04000CEB RID: 3307
		private int m_Points;
	}
}
