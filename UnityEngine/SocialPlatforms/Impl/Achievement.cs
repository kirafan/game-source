﻿using System;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x0200033D RID: 829
	public class Achievement : IAchievement
	{
		// Token: 0x06002D07 RID: 11527 RVA: 0x00046F80 File Offset: 0x00045180
		public Achievement(string id, double percentCompleted, bool completed, bool hidden, DateTime lastReportedDate)
		{
			this.id = id;
			this.percentCompleted = percentCompleted;
			this.m_Completed = completed;
			this.m_Hidden = hidden;
			this.m_LastReportedDate = lastReportedDate;
		}

		// Token: 0x06002D08 RID: 11528 RVA: 0x00046FB0 File Offset: 0x000451B0
		public Achievement(string id, double percent)
		{
			this.id = id;
			this.percentCompleted = percent;
			this.m_Hidden = false;
			this.m_Completed = false;
			this.m_LastReportedDate = DateTime.MinValue;
		}

		// Token: 0x06002D09 RID: 11529 RVA: 0x00046FE0 File Offset: 0x000451E0
		public Achievement() : this("unknown", 0.0)
		{
		}

		// Token: 0x06002D0A RID: 11530 RVA: 0x00046FF8 File Offset: 0x000451F8
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.id,
				" - ",
				this.percentCompleted,
				" - ",
				this.completed,
				" - ",
				this.hidden,
				" - ",
				this.lastReportedDate
			});
		}

		// Token: 0x06002D0B RID: 11531 RVA: 0x0004707C File Offset: 0x0004527C
		public void ReportProgress(Action<bool> callback)
		{
			ActivePlatform.Instance.ReportProgress(this.id, this.percentCompleted, callback);
		}

		// Token: 0x06002D0C RID: 11532 RVA: 0x00047098 File Offset: 0x00045298
		public void SetCompleted(bool value)
		{
			this.m_Completed = value;
		}

		// Token: 0x06002D0D RID: 11533 RVA: 0x000470A4 File Offset: 0x000452A4
		public void SetHidden(bool value)
		{
			this.m_Hidden = value;
		}

		// Token: 0x06002D0E RID: 11534 RVA: 0x000470B0 File Offset: 0x000452B0
		public void SetLastReportedDate(DateTime date)
		{
			this.m_LastReportedDate = date;
		}

		// Token: 0x17000A99 RID: 2713
		// (get) Token: 0x06002D0F RID: 11535 RVA: 0x000470BC File Offset: 0x000452BC
		// (set) Token: 0x06002D10 RID: 11536 RVA: 0x000470D8 File Offset: 0x000452D8
		public string id { get; set; }

		// Token: 0x17000A9A RID: 2714
		// (get) Token: 0x06002D11 RID: 11537 RVA: 0x000470E4 File Offset: 0x000452E4
		// (set) Token: 0x06002D12 RID: 11538 RVA: 0x00047100 File Offset: 0x00045300
		public double percentCompleted { get; set; }

		// Token: 0x17000A9B RID: 2715
		// (get) Token: 0x06002D13 RID: 11539 RVA: 0x0004710C File Offset: 0x0004530C
		public bool completed
		{
			get
			{
				return this.m_Completed;
			}
		}

		// Token: 0x17000A9C RID: 2716
		// (get) Token: 0x06002D14 RID: 11540 RVA: 0x00047128 File Offset: 0x00045328
		public bool hidden
		{
			get
			{
				return this.m_Hidden;
			}
		}

		// Token: 0x17000A9D RID: 2717
		// (get) Token: 0x06002D15 RID: 11541 RVA: 0x00047144 File Offset: 0x00045344
		public DateTime lastReportedDate
		{
			get
			{
				return this.m_LastReportedDate;
			}
		}

		// Token: 0x04000CE1 RID: 3297
		private bool m_Completed;

		// Token: 0x04000CE2 RID: 3298
		private bool m_Hidden;

		// Token: 0x04000CE3 RID: 3299
		private DateTime m_LastReportedDate;
	}
}
