﻿using System;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x0200033B RID: 827
	public class LocalUser : UserProfile, ILocalUser, IUserProfile
	{
		// Token: 0x06002CEF RID: 11503 RVA: 0x00046D04 File Offset: 0x00044F04
		public LocalUser()
		{
			this.m_Friends = new UserProfile[0];
			this.m_Authenticated = false;
			this.m_Underage = false;
		}

		// Token: 0x06002CF0 RID: 11504 RVA: 0x00046D28 File Offset: 0x00044F28
		public void Authenticate(Action<bool> callback)
		{
			ActivePlatform.Instance.Authenticate(this, callback);
		}

		// Token: 0x06002CF1 RID: 11505 RVA: 0x00046D38 File Offset: 0x00044F38
		public void Authenticate(Action<bool, string> callback)
		{
			ActivePlatform.Instance.Authenticate(this, callback);
		}

		// Token: 0x06002CF2 RID: 11506 RVA: 0x00046D48 File Offset: 0x00044F48
		public void LoadFriends(Action<bool> callback)
		{
			ActivePlatform.Instance.LoadFriends(this, callback);
		}

		// Token: 0x06002CF3 RID: 11507 RVA: 0x00046D58 File Offset: 0x00044F58
		public void SetFriends(IUserProfile[] friends)
		{
			this.m_Friends = friends;
		}

		// Token: 0x06002CF4 RID: 11508 RVA: 0x00046D64 File Offset: 0x00044F64
		public void SetAuthenticated(bool value)
		{
			this.m_Authenticated = value;
		}

		// Token: 0x06002CF5 RID: 11509 RVA: 0x00046D70 File Offset: 0x00044F70
		public void SetUnderage(bool value)
		{
			this.m_Underage = value;
		}

		// Token: 0x17000A91 RID: 2705
		// (get) Token: 0x06002CF6 RID: 11510 RVA: 0x00046D7C File Offset: 0x00044F7C
		public IUserProfile[] friends
		{
			get
			{
				return this.m_Friends;
			}
		}

		// Token: 0x17000A92 RID: 2706
		// (get) Token: 0x06002CF7 RID: 11511 RVA: 0x00046D98 File Offset: 0x00044F98
		public bool authenticated
		{
			get
			{
				return this.m_Authenticated;
			}
		}

		// Token: 0x17000A93 RID: 2707
		// (get) Token: 0x06002CF8 RID: 11512 RVA: 0x00046DB4 File Offset: 0x00044FB4
		public bool underage
		{
			get
			{
				return this.m_Underage;
			}
		}

		// Token: 0x04000CD9 RID: 3289
		private IUserProfile[] m_Friends;

		// Token: 0x04000CDA RID: 3290
		private bool m_Authenticated;

		// Token: 0x04000CDB RID: 3291
		private bool m_Underage;
	}
}
