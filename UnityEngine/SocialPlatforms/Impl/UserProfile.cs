﻿using System;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x0200033C RID: 828
	public class UserProfile : IUserProfile
	{
		// Token: 0x06002CF9 RID: 11513 RVA: 0x00046DD0 File Offset: 0x00044FD0
		public UserProfile()
		{
			this.m_UserName = "Uninitialized";
			this.m_ID = "0";
			this.m_IsFriend = false;
			this.m_State = UserState.Offline;
			this.m_Image = new Texture2D(32, 32);
		}

		// Token: 0x06002CFA RID: 11514 RVA: 0x00046E0C File Offset: 0x0004500C
		public UserProfile(string name, string id, bool friend) : this(name, id, friend, UserState.Offline, new Texture2D(0, 0))
		{
		}

		// Token: 0x06002CFB RID: 11515 RVA: 0x00046E20 File Offset: 0x00045020
		public UserProfile(string name, string id, bool friend, UserState state, Texture2D image)
		{
			this.m_UserName = name;
			this.m_ID = id;
			this.m_IsFriend = friend;
			this.m_State = state;
			this.m_Image = image;
		}

		// Token: 0x06002CFC RID: 11516 RVA: 0x00046E50 File Offset: 0x00045050
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				this.id,
				" - ",
				this.userName,
				" - ",
				this.isFriend,
				" - ",
				this.state
			});
		}

		// Token: 0x06002CFD RID: 11517 RVA: 0x00046EB8 File Offset: 0x000450B8
		public void SetUserName(string name)
		{
			this.m_UserName = name;
		}

		// Token: 0x06002CFE RID: 11518 RVA: 0x00046EC4 File Offset: 0x000450C4
		public void SetUserID(string id)
		{
			this.m_ID = id;
		}

		// Token: 0x06002CFF RID: 11519 RVA: 0x00046ED0 File Offset: 0x000450D0
		public void SetImage(Texture2D image)
		{
			this.m_Image = image;
		}

		// Token: 0x06002D00 RID: 11520 RVA: 0x00046EDC File Offset: 0x000450DC
		public void SetIsFriend(bool value)
		{
			this.m_IsFriend = value;
		}

		// Token: 0x06002D01 RID: 11521 RVA: 0x00046EE8 File Offset: 0x000450E8
		public void SetState(UserState state)
		{
			this.m_State = state;
		}

		// Token: 0x17000A94 RID: 2708
		// (get) Token: 0x06002D02 RID: 11522 RVA: 0x00046EF4 File Offset: 0x000450F4
		public string userName
		{
			get
			{
				return this.m_UserName;
			}
		}

		// Token: 0x17000A95 RID: 2709
		// (get) Token: 0x06002D03 RID: 11523 RVA: 0x00046F10 File Offset: 0x00045110
		public string id
		{
			get
			{
				return this.m_ID;
			}
		}

		// Token: 0x17000A96 RID: 2710
		// (get) Token: 0x06002D04 RID: 11524 RVA: 0x00046F2C File Offset: 0x0004512C
		public bool isFriend
		{
			get
			{
				return this.m_IsFriend;
			}
		}

		// Token: 0x17000A97 RID: 2711
		// (get) Token: 0x06002D05 RID: 11525 RVA: 0x00046F48 File Offset: 0x00045148
		public UserState state
		{
			get
			{
				return this.m_State;
			}
		}

		// Token: 0x17000A98 RID: 2712
		// (get) Token: 0x06002D06 RID: 11526 RVA: 0x00046F64 File Offset: 0x00045164
		public Texture2D image
		{
			get
			{
				return this.m_Image;
			}
		}

		// Token: 0x04000CDC RID: 3292
		protected string m_UserName;

		// Token: 0x04000CDD RID: 3293
		protected string m_ID;

		// Token: 0x04000CDE RID: 3294
		protected bool m_IsFriend;

		// Token: 0x04000CDF RID: 3295
		protected UserState m_State;

		// Token: 0x04000CE0 RID: 3296
		protected Texture2D m_Image;
	}
}
