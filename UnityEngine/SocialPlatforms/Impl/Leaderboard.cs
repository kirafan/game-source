﻿using System;

namespace UnityEngine.SocialPlatforms.Impl
{
	// Token: 0x02000340 RID: 832
	public class Leaderboard : ILeaderboard
	{
		// Token: 0x06002D32 RID: 11570 RVA: 0x00047500 File Offset: 0x00045700
		public Leaderboard()
		{
			this.id = "Invalid";
			this.range = new Range(1, 10);
			this.userScope = UserScope.Global;
			this.timeScope = TimeScope.AllTime;
			this.m_Loading = false;
			this.m_LocalUserScore = new Score("Invalid", 0L);
			this.m_MaxRange = 0U;
			this.m_Scores = new Score[0];
			this.m_Title = "Invalid";
			this.m_UserIDs = new string[0];
		}

		// Token: 0x06002D33 RID: 11571 RVA: 0x00047580 File Offset: 0x00045780
		public void SetUserFilter(string[] userIDs)
		{
			this.m_UserIDs = userIDs;
		}

		// Token: 0x06002D34 RID: 11572 RVA: 0x0004758C File Offset: 0x0004578C
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"ID: '",
				this.id,
				"' Title: '",
				this.m_Title,
				"' Loading: '",
				this.m_Loading,
				"' Range: [",
				this.range.from,
				",",
				this.range.count,
				"] MaxRange: '",
				this.m_MaxRange,
				"' Scores: '",
				this.m_Scores.Length,
				"' UserScope: '",
				this.userScope,
				"' TimeScope: '",
				this.timeScope,
				"' UserFilter: '",
				this.m_UserIDs.Length
			});
		}

		// Token: 0x06002D35 RID: 11573 RVA: 0x000476A0 File Offset: 0x000458A0
		public void LoadScores(Action<bool> callback)
		{
			ActivePlatform.Instance.LoadScores(this, callback);
		}

		// Token: 0x17000AAB RID: 2731
		// (get) Token: 0x06002D36 RID: 11574 RVA: 0x000476B0 File Offset: 0x000458B0
		public bool loading
		{
			get
			{
				return ActivePlatform.Instance.GetLoading(this);
			}
		}

		// Token: 0x06002D37 RID: 11575 RVA: 0x000476D0 File Offset: 0x000458D0
		public void SetLocalUserScore(IScore score)
		{
			this.m_LocalUserScore = score;
		}

		// Token: 0x06002D38 RID: 11576 RVA: 0x000476DC File Offset: 0x000458DC
		public void SetMaxRange(uint maxRange)
		{
			this.m_MaxRange = maxRange;
		}

		// Token: 0x06002D39 RID: 11577 RVA: 0x000476E8 File Offset: 0x000458E8
		public void SetScores(IScore[] scores)
		{
			this.m_Scores = scores;
		}

		// Token: 0x06002D3A RID: 11578 RVA: 0x000476F4 File Offset: 0x000458F4
		public void SetTitle(string title)
		{
			this.m_Title = title;
		}

		// Token: 0x06002D3B RID: 11579 RVA: 0x00047700 File Offset: 0x00045900
		public string[] GetUserFilter()
		{
			return this.m_UserIDs;
		}

		// Token: 0x17000AAC RID: 2732
		// (get) Token: 0x06002D3C RID: 11580 RVA: 0x0004771C File Offset: 0x0004591C
		// (set) Token: 0x06002D3D RID: 11581 RVA: 0x00047738 File Offset: 0x00045938
		public string id { get; set; }

		// Token: 0x17000AAD RID: 2733
		// (get) Token: 0x06002D3E RID: 11582 RVA: 0x00047744 File Offset: 0x00045944
		// (set) Token: 0x06002D3F RID: 11583 RVA: 0x00047760 File Offset: 0x00045960
		public UserScope userScope { get; set; }

		// Token: 0x17000AAE RID: 2734
		// (get) Token: 0x06002D40 RID: 11584 RVA: 0x0004776C File Offset: 0x0004596C
		// (set) Token: 0x06002D41 RID: 11585 RVA: 0x00047788 File Offset: 0x00045988
		public Range range { get; set; }

		// Token: 0x17000AAF RID: 2735
		// (get) Token: 0x06002D42 RID: 11586 RVA: 0x00047794 File Offset: 0x00045994
		// (set) Token: 0x06002D43 RID: 11587 RVA: 0x000477B0 File Offset: 0x000459B0
		public TimeScope timeScope { get; set; }

		// Token: 0x17000AB0 RID: 2736
		// (get) Token: 0x06002D44 RID: 11588 RVA: 0x000477BC File Offset: 0x000459BC
		public IScore localUserScore
		{
			get
			{
				return this.m_LocalUserScore;
			}
		}

		// Token: 0x17000AB1 RID: 2737
		// (get) Token: 0x06002D45 RID: 11589 RVA: 0x000477D8 File Offset: 0x000459D8
		public uint maxRange
		{
			get
			{
				return this.m_MaxRange;
			}
		}

		// Token: 0x17000AB2 RID: 2738
		// (get) Token: 0x06002D46 RID: 11590 RVA: 0x000477F4 File Offset: 0x000459F4
		public IScore[] scores
		{
			get
			{
				return this.m_Scores;
			}
		}

		// Token: 0x17000AB3 RID: 2739
		// (get) Token: 0x06002D47 RID: 11591 RVA: 0x00047810 File Offset: 0x00045A10
		public string title
		{
			get
			{
				return this.m_Title;
			}
		}

		// Token: 0x04000CF3 RID: 3315
		private bool m_Loading;

		// Token: 0x04000CF4 RID: 3316
		private IScore m_LocalUserScore;

		// Token: 0x04000CF5 RID: 3317
		private uint m_MaxRange;

		// Token: 0x04000CF6 RID: 3318
		private IScore[] m_Scores;

		// Token: 0x04000CF7 RID: 3319
		private string m_Title;

		// Token: 0x04000CF8 RID: 3320
		private string[] m_UserIDs;
	}
}
