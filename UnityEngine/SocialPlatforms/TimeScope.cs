﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000353 RID: 851
	public enum TimeScope
	{
		// Token: 0x04000D1E RID: 3358
		Today,
		// Token: 0x04000D1F RID: 3359
		Week,
		// Token: 0x04000D20 RID: 3360
		AllTime
	}
}
