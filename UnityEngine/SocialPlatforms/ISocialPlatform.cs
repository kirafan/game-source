﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200034B RID: 843
	public interface ISocialPlatform
	{
		// Token: 0x17000AB8 RID: 2744
		// (get) Token: 0x06002D85 RID: 11653
		ILocalUser localUser { get; }

		// Token: 0x06002D86 RID: 11654
		void LoadUsers(string[] userIDs, Action<IUserProfile[]> callback);

		// Token: 0x06002D87 RID: 11655
		void ReportProgress(string achievementID, double progress, Action<bool> callback);

		// Token: 0x06002D88 RID: 11656
		void LoadAchievementDescriptions(Action<IAchievementDescription[]> callback);

		// Token: 0x06002D89 RID: 11657
		void LoadAchievements(Action<IAchievement[]> callback);

		// Token: 0x06002D8A RID: 11658
		IAchievement CreateAchievement();

		// Token: 0x06002D8B RID: 11659
		void ReportScore(long score, string board, Action<bool> callback);

		// Token: 0x06002D8C RID: 11660
		void LoadScores(string leaderboardID, Action<IScore[]> callback);

		// Token: 0x06002D8D RID: 11661
		ILeaderboard CreateLeaderboard();

		// Token: 0x06002D8E RID: 11662
		void ShowAchievementsUI();

		// Token: 0x06002D8F RID: 11663
		void ShowLeaderboardUI();

		// Token: 0x06002D90 RID: 11664
		void Authenticate(ILocalUser user, Action<bool> callback);

		// Token: 0x06002D91 RID: 11665
		void Authenticate(ILocalUser user, Action<bool, string> callback);

		// Token: 0x06002D92 RID: 11666
		void LoadFriends(ILocalUser user, Action<bool> callback);

		// Token: 0x06002D93 RID: 11667
		void LoadScores(ILeaderboard board, Action<bool> callback);

		// Token: 0x06002D94 RID: 11668
		bool GetLoading(ILeaderboard board);
	}
}
