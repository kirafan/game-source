﻿using System;
using System.Collections.Generic;
using UnityEngine.SocialPlatforms.Impl;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000341 RID: 833
	public class Local : ISocialPlatform
	{
		// Token: 0x17000AB4 RID: 2740
		// (get) Token: 0x06002D49 RID: 11593 RVA: 0x0004786C File Offset: 0x00045A6C
		public ILocalUser localUser
		{
			get
			{
				if (Local.m_LocalUser == null)
				{
					Local.m_LocalUser = new LocalUser();
				}
				return Local.m_LocalUser;
			}
		}

		// Token: 0x06002D4A RID: 11594 RVA: 0x0004789C File Offset: 0x00045A9C
		void ISocialPlatform.Authenticate(ILocalUser user, Action<bool> callback)
		{
			LocalUser localUser = (LocalUser)user;
			this.m_DefaultTexture = this.CreateDummyTexture(32, 32);
			this.PopulateStaticData();
			localUser.SetAuthenticated(true);
			localUser.SetUnderage(false);
			localUser.SetUserID("1000");
			localUser.SetUserName("Lerpz");
			localUser.SetImage(this.m_DefaultTexture);
			if (callback != null)
			{
				callback(true);
			}
		}

		// Token: 0x06002D4B RID: 11595 RVA: 0x00047904 File Offset: 0x00045B04
		void ISocialPlatform.Authenticate(ILocalUser user, Action<bool, string> callback)
		{
			((ISocialPlatform)this).Authenticate(user, delegate(bool success)
			{
				callback(success, null);
			});
		}

		// Token: 0x06002D4C RID: 11596 RVA: 0x00047934 File Offset: 0x00045B34
		void ISocialPlatform.LoadFriends(ILocalUser user, Action<bool> callback)
		{
			if (this.VerifyUser())
			{
				((LocalUser)user).SetFriends(this.m_Friends.ToArray());
				if (callback != null)
				{
					callback(true);
				}
			}
		}

		// Token: 0x06002D4D RID: 11597 RVA: 0x0004796C File Offset: 0x00045B6C
		public void LoadUsers(string[] userIDs, Action<IUserProfile[]> callback)
		{
			List<UserProfile> list = new List<UserProfile>();
			if (this.VerifyUser())
			{
				foreach (string b in userIDs)
				{
					foreach (UserProfile userProfile in this.m_Users)
					{
						if (userProfile.id == b)
						{
							list.Add(userProfile);
						}
					}
					foreach (UserProfile userProfile2 in this.m_Friends)
					{
						if (userProfile2.id == b)
						{
							list.Add(userProfile2);
						}
					}
				}
				callback(list.ToArray());
			}
		}

		// Token: 0x06002D4E RID: 11598 RVA: 0x00047A80 File Offset: 0x00045C80
		public void ReportProgress(string id, double progress, Action<bool> callback)
		{
			if (this.VerifyUser())
			{
				foreach (Achievement achievement in this.m_Achievements)
				{
					if (achievement.id == id && achievement.percentCompleted <= progress)
					{
						if (progress >= 100.0)
						{
							achievement.SetCompleted(true);
						}
						achievement.SetHidden(false);
						achievement.SetLastReportedDate(DateTime.Now);
						achievement.percentCompleted = progress;
						if (callback != null)
						{
							callback(true);
						}
						return;
					}
				}
				foreach (AchievementDescription achievementDescription in this.m_AchievementDescriptions)
				{
					if (achievementDescription.id == id)
					{
						bool completed = progress >= 100.0;
						Achievement item = new Achievement(id, progress, completed, false, DateTime.Now);
						this.m_Achievements.Add(item);
						if (callback != null)
						{
							callback(true);
						}
						return;
					}
				}
				Debug.LogError("Achievement ID not found");
				if (callback != null)
				{
					callback(false);
				}
			}
		}

		// Token: 0x06002D4F RID: 11599 RVA: 0x00047C00 File Offset: 0x00045E00
		public void LoadAchievementDescriptions(Action<IAchievementDescription[]> callback)
		{
			if (this.VerifyUser())
			{
				if (callback != null)
				{
					callback(this.m_AchievementDescriptions.ToArray());
				}
			}
		}

		// Token: 0x06002D50 RID: 11600 RVA: 0x00047C2C File Offset: 0x00045E2C
		public void LoadAchievements(Action<IAchievement[]> callback)
		{
			if (this.VerifyUser())
			{
				if (callback != null)
				{
					callback(this.m_Achievements.ToArray());
				}
			}
		}

		// Token: 0x06002D51 RID: 11601 RVA: 0x00047C58 File Offset: 0x00045E58
		public void ReportScore(long score, string board, Action<bool> callback)
		{
			if (this.VerifyUser())
			{
				foreach (Leaderboard leaderboard in this.m_Leaderboards)
				{
					if (leaderboard.id == board)
					{
						leaderboard.SetScores(new List<Score>((Score[])leaderboard.scores)
						{
							new Score(board, score, this.localUser.id, DateTime.Now, score + " points", 0)
						}.ToArray());
						if (callback != null)
						{
							callback(true);
						}
						return;
					}
				}
				Debug.LogError("Leaderboard not found");
				if (callback != null)
				{
					callback(false);
				}
			}
		}

		// Token: 0x06002D52 RID: 11602 RVA: 0x00047D48 File Offset: 0x00045F48
		public void LoadScores(string leaderboardID, Action<IScore[]> callback)
		{
			if (this.VerifyUser())
			{
				foreach (Leaderboard leaderboard in this.m_Leaderboards)
				{
					if (leaderboard.id == leaderboardID)
					{
						this.SortScores(leaderboard);
						if (callback != null)
						{
							callback(leaderboard.scores);
						}
						return;
					}
				}
				Debug.LogError("Leaderboard not found");
				if (callback != null)
				{
					callback(new Score[0]);
				}
			}
		}

		// Token: 0x06002D53 RID: 11603 RVA: 0x00047E00 File Offset: 0x00046000
		void ISocialPlatform.LoadScores(ILeaderboard board, Action<bool> callback)
		{
			if (this.VerifyUser())
			{
				Leaderboard leaderboard = (Leaderboard)board;
				foreach (Leaderboard leaderboard2 in this.m_Leaderboards)
				{
					if (leaderboard2.id == leaderboard.id)
					{
						leaderboard.SetTitle(leaderboard2.title);
						leaderboard.SetScores(leaderboard2.scores);
						leaderboard.SetMaxRange((uint)leaderboard2.scores.Length);
					}
				}
				this.SortScores(leaderboard);
				this.SetLocalPlayerScore(leaderboard);
				if (callback != null)
				{
					callback(true);
				}
			}
		}

		// Token: 0x06002D54 RID: 11604 RVA: 0x00047ECC File Offset: 0x000460CC
		bool ISocialPlatform.GetLoading(ILeaderboard board)
		{
			return this.VerifyUser() && ((Leaderboard)board).loading;
		}

		// Token: 0x06002D55 RID: 11605 RVA: 0x00047F00 File Offset: 0x00046100
		private void SortScores(Leaderboard board)
		{
			List<Score> list = new List<Score>((Score[])board.scores);
			list.Sort((Score s1, Score s2) => s2.value.CompareTo(s1.value));
			for (int i = 0; i < list.Count; i++)
			{
				list[i].SetRank(i + 1);
			}
		}

		// Token: 0x06002D56 RID: 11606 RVA: 0x00047F68 File Offset: 0x00046168
		private void SetLocalPlayerScore(Leaderboard board)
		{
			foreach (Score score in board.scores)
			{
				if (score.userID == this.localUser.id)
				{
					board.SetLocalUserScore(score);
					break;
				}
			}
		}

		// Token: 0x06002D57 RID: 11607 RVA: 0x00047FC8 File Offset: 0x000461C8
		public void ShowAchievementsUI()
		{
			Debug.Log("ShowAchievementsUI not implemented");
		}

		// Token: 0x06002D58 RID: 11608 RVA: 0x00047FD8 File Offset: 0x000461D8
		public void ShowLeaderboardUI()
		{
			Debug.Log("ShowLeaderboardUI not implemented");
		}

		// Token: 0x06002D59 RID: 11609 RVA: 0x00047FE8 File Offset: 0x000461E8
		public ILeaderboard CreateLeaderboard()
		{
			return new Leaderboard();
		}

		// Token: 0x06002D5A RID: 11610 RVA: 0x00048004 File Offset: 0x00046204
		public IAchievement CreateAchievement()
		{
			return new Achievement();
		}

		// Token: 0x06002D5B RID: 11611 RVA: 0x00048020 File Offset: 0x00046220
		private bool VerifyUser()
		{
			bool result;
			if (!this.localUser.authenticated)
			{
				Debug.LogError("Must authenticate first");
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06002D5C RID: 11612 RVA: 0x00048058 File Offset: 0x00046258
		private void PopulateStaticData()
		{
			this.m_Friends.Add(new UserProfile("Fred", "1001", true, UserState.Online, this.m_DefaultTexture));
			this.m_Friends.Add(new UserProfile("Julia", "1002", true, UserState.Online, this.m_DefaultTexture));
			this.m_Friends.Add(new UserProfile("Jeff", "1003", true, UserState.Online, this.m_DefaultTexture));
			this.m_Users.Add(new UserProfile("Sam", "1004", false, UserState.Offline, this.m_DefaultTexture));
			this.m_Users.Add(new UserProfile("Max", "1005", false, UserState.Offline, this.m_DefaultTexture));
			this.m_AchievementDescriptions.Add(new AchievementDescription("Achievement01", "First achievement", this.m_DefaultTexture, "Get first achievement", "Received first achievement", false, 10));
			this.m_AchievementDescriptions.Add(new AchievementDescription("Achievement02", "Second achievement", this.m_DefaultTexture, "Get second achievement", "Received second achievement", false, 20));
			this.m_AchievementDescriptions.Add(new AchievementDescription("Achievement03", "Third achievement", this.m_DefaultTexture, "Get third achievement", "Received third achievement", false, 15));
			Leaderboard leaderboard = new Leaderboard();
			leaderboard.SetTitle("High Scores");
			leaderboard.id = "Leaderboard01";
			leaderboard.SetScores(new List<Score>
			{
				new Score("Leaderboard01", 300L, "1001", DateTime.Now.AddDays(-1.0), "300 points", 1),
				new Score("Leaderboard01", 255L, "1002", DateTime.Now.AddDays(-1.0), "255 points", 2),
				new Score("Leaderboard01", 55L, "1003", DateTime.Now.AddDays(-1.0), "55 points", 3),
				new Score("Leaderboard01", 10L, "1004", DateTime.Now.AddDays(-1.0), "10 points", 4)
			}.ToArray());
			this.m_Leaderboards.Add(leaderboard);
		}

		// Token: 0x06002D5D RID: 11613 RVA: 0x000482AC File Offset: 0x000464AC
		private Texture2D CreateDummyTexture(int width, int height)
		{
			Texture2D texture2D = new Texture2D(width, height);
			for (int i = 0; i < height; i++)
			{
				for (int j = 0; j < width; j++)
				{
					Color color = ((j & i) <= 0) ? Color.gray : Color.white;
					texture2D.SetPixel(j, i, color);
				}
			}
			texture2D.Apply();
			return texture2D;
		}

		// Token: 0x04000CFD RID: 3325
		private static LocalUser m_LocalUser = null;

		// Token: 0x04000CFE RID: 3326
		private List<UserProfile> m_Friends = new List<UserProfile>();

		// Token: 0x04000CFF RID: 3327
		private List<UserProfile> m_Users = new List<UserProfile>();

		// Token: 0x04000D00 RID: 3328
		private List<AchievementDescription> m_AchievementDescriptions = new List<AchievementDescription>();

		// Token: 0x04000D01 RID: 3329
		private List<Achievement> m_Achievements = new List<Achievement>();

		// Token: 0x04000D02 RID: 3330
		private List<Leaderboard> m_Leaderboards = new List<Leaderboard>();

		// Token: 0x04000D03 RID: 3331
		private Texture2D m_DefaultTexture;
	}
}
