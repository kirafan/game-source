﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200034D RID: 845
	public enum UserState
	{
		// Token: 0x04000D15 RID: 3349
		Online,
		// Token: 0x04000D16 RID: 3350
		OnlineAndAway,
		// Token: 0x04000D17 RID: 3351
		OnlineAndBusy,
		// Token: 0x04000D18 RID: 3352
		Offline,
		// Token: 0x04000D19 RID: 3353
		Playing
	}
}
