﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000354 RID: 852
	public struct Range
	{
		// Token: 0x06002DB9 RID: 11705 RVA: 0x00048BA4 File Offset: 0x00046DA4
		public Range(int fromValue, int valueCount)
		{
			this.from = fromValue;
			this.count = valueCount;
		}

		// Token: 0x04000D21 RID: 3361
		public int from;

		// Token: 0x04000D22 RID: 3362
		public int count;
	}
}
