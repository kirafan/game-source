﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000355 RID: 853
	public interface ILeaderboard
	{
		// Token: 0x06002DBA RID: 11706
		void SetUserFilter(string[] userIDs);

		// Token: 0x06002DBB RID: 11707
		void LoadScores(Action<bool> callback);

		// Token: 0x17000AD3 RID: 2771
		// (get) Token: 0x06002DBC RID: 11708
		bool loading { get; }

		// Token: 0x17000AD4 RID: 2772
		// (get) Token: 0x06002DBD RID: 11709
		// (set) Token: 0x06002DBE RID: 11710
		string id { get; set; }

		// Token: 0x17000AD5 RID: 2773
		// (get) Token: 0x06002DBF RID: 11711
		// (set) Token: 0x06002DC0 RID: 11712
		UserScope userScope { get; set; }

		// Token: 0x17000AD6 RID: 2774
		// (get) Token: 0x06002DC1 RID: 11713
		// (set) Token: 0x06002DC2 RID: 11714
		Range range { get; set; }

		// Token: 0x17000AD7 RID: 2775
		// (get) Token: 0x06002DC3 RID: 11715
		// (set) Token: 0x06002DC4 RID: 11716
		TimeScope timeScope { get; set; }

		// Token: 0x17000AD8 RID: 2776
		// (get) Token: 0x06002DC5 RID: 11717
		IScore localUserScore { get; }

		// Token: 0x17000AD9 RID: 2777
		// (get) Token: 0x06002DC6 RID: 11718
		uint maxRange { get; }

		// Token: 0x17000ADA RID: 2778
		// (get) Token: 0x06002DC7 RID: 11719
		IScore[] scores { get; }

		// Token: 0x17000ADB RID: 2779
		// (get) Token: 0x06002DC8 RID: 11720
		string title { get; }
	}
}
