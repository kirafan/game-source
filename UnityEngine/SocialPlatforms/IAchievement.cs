﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200034F RID: 847
	public interface IAchievement
	{
		// Token: 0x06002DA0 RID: 11680
		void ReportProgress(Action<bool> callback);

		// Token: 0x17000AC1 RID: 2753
		// (get) Token: 0x06002DA1 RID: 11681
		// (set) Token: 0x06002DA2 RID: 11682
		string id { get; set; }

		// Token: 0x17000AC2 RID: 2754
		// (get) Token: 0x06002DA3 RID: 11683
		// (set) Token: 0x06002DA4 RID: 11684
		double percentCompleted { get; set; }

		// Token: 0x17000AC3 RID: 2755
		// (get) Token: 0x06002DA5 RID: 11685
		bool completed { get; }

		// Token: 0x17000AC4 RID: 2756
		// (get) Token: 0x06002DA6 RID: 11686
		bool hidden { get; }

		// Token: 0x17000AC5 RID: 2757
		// (get) Token: 0x06002DA7 RID: 11687
		DateTime lastReportedDate { get; }
	}
}
