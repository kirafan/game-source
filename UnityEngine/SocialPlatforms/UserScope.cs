﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x02000352 RID: 850
	public enum UserScope
	{
		// Token: 0x04000D1B RID: 3355
		Global,
		// Token: 0x04000D1C RID: 3356
		FriendsOnly
	}
}
