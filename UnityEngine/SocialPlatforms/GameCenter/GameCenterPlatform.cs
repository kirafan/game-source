﻿using System;

namespace UnityEngine.SocialPlatforms.GameCenter
{
	// Token: 0x020002DF RID: 735
	public class GameCenterPlatform : Local
	{
		// Token: 0x06002C92 RID: 11410 RVA: 0x0004648C File Offset: 0x0004468C
		public static void ResetAllAchievements(Action<bool> callback)
		{
			Debug.Log("ResetAllAchievements - no effect in editor");
			if (callback != null)
			{
				callback(true);
			}
		}

		// Token: 0x06002C93 RID: 11411 RVA: 0x000464A8 File Offset: 0x000446A8
		public static void ShowDefaultAchievementCompletionBanner(bool value)
		{
			Debug.Log("ShowDefaultAchievementCompletionBanner - no effect in editor");
		}

		// Token: 0x06002C94 RID: 11412 RVA: 0x000464B8 File Offset: 0x000446B8
		public static void ShowLeaderboardUI(string leaderboardID, TimeScope timeScope)
		{
			Debug.Log("ShowLeaderboardUI - no effect in editor");
		}
	}
}
