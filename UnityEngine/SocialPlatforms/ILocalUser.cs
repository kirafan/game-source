﻿using System;

namespace UnityEngine.SocialPlatforms
{
	// Token: 0x0200034C RID: 844
	public interface ILocalUser : IUserProfile
	{
		// Token: 0x06002D95 RID: 11669
		void Authenticate(Action<bool> callback);

		// Token: 0x06002D96 RID: 11670
		void Authenticate(Action<bool, string> callback);

		// Token: 0x06002D97 RID: 11671
		void LoadFriends(Action<bool> callback);

		// Token: 0x17000AB9 RID: 2745
		// (get) Token: 0x06002D98 RID: 11672
		IUserProfile[] friends { get; }

		// Token: 0x17000ABA RID: 2746
		// (get) Token: 0x06002D99 RID: 11673
		bool authenticated { get; }

		// Token: 0x17000ABB RID: 2747
		// (get) Token: 0x06002D9A RID: 11674
		bool underage { get; }
	}
}
