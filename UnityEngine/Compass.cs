﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000076 RID: 118
	public sealed class Compass
	{
		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x060007A9 RID: 1961
		public extern float magneticHeading { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x060007AA RID: 1962
		public extern float trueHeading { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x060007AB RID: 1963
		public extern float headingAccuracy { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x060007AC RID: 1964 RVA: 0x00009DAC File Offset: 0x00007FAC
		public Vector3 rawVector
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_rawVector(out result);
				return result;
			}
		}

		// Token: 0x060007AD RID: 1965
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rawVector(out Vector3 value);

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x060007AE RID: 1966
		public extern double timestamp { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x060007AF RID: 1967
		// (set) Token: 0x060007B0 RID: 1968
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
