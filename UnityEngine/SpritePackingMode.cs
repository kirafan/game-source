﻿using System;

namespace UnityEngine
{
	// Token: 0x020000BC RID: 188
	public enum SpritePackingMode
	{
		// Token: 0x040001CF RID: 463
		Tight,
		// Token: 0x040001D0 RID: 464
		Rectangle
	}
}
