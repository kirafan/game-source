﻿using System;

namespace UnityEngine
{
	// Token: 0x020001F0 RID: 496
	public enum DetailRenderMode
	{
		// Token: 0x040005B9 RID: 1465
		GrassBillboard,
		// Token: 0x040005BA RID: 1466
		VertexLit,
		// Token: 0x040005BB RID: 1467
		Grass
	}
}
