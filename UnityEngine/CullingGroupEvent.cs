﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002E RID: 46
	public struct CullingGroupEvent
	{
		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x0600036B RID: 875 RVA: 0x00006268 File Offset: 0x00004468
		public int index
		{
			get
			{
				return this.m_Index;
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x0600036C RID: 876 RVA: 0x00006284 File Offset: 0x00004484
		public bool isVisible
		{
			get
			{
				return (this.m_ThisState & 128) != 0;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x0600036D RID: 877 RVA: 0x000062AC File Offset: 0x000044AC
		public bool wasVisible
		{
			get
			{
				return (this.m_PrevState & 128) != 0;
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x0600036E RID: 878 RVA: 0x000062D4 File Offset: 0x000044D4
		public bool hasBecomeVisible
		{
			get
			{
				return this.isVisible && !this.wasVisible;
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x0600036F RID: 879 RVA: 0x00006300 File Offset: 0x00004500
		public bool hasBecomeInvisible
		{
			get
			{
				return !this.isVisible && this.wasVisible;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x06000370 RID: 880 RVA: 0x0000632C File Offset: 0x0000452C
		public int currentDistance
		{
			get
			{
				return (int)(this.m_ThisState & 127);
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000371 RID: 881 RVA: 0x0000634C File Offset: 0x0000454C
		public int previousDistance
		{
			get
			{
				return (int)(this.m_PrevState & 127);
			}
		}

		// Token: 0x04000046 RID: 70
		private int m_Index;

		// Token: 0x04000047 RID: 71
		private byte m_PrevState;

		// Token: 0x04000048 RID: 72
		private byte m_ThisState;

		// Token: 0x04000049 RID: 73
		private const byte kIsVisibleMask = 128;

		// Token: 0x0400004A RID: 74
		private const byte kDistanceMask = 127;
	}
}
