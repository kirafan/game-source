﻿using System;

namespace UnityEngine
{
	// Token: 0x020002DA RID: 730
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false)]
	internal class WritableAttribute : Attribute
	{
	}
}
