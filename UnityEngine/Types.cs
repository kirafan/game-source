﻿using System;

namespace UnityEngine
{
	// Token: 0x02000368 RID: 872
	public static class Types
	{
		// Token: 0x06002E2E RID: 11822 RVA: 0x00049D60 File Offset: 0x00047F60
		[Obsolete("This was an internal method which is no longer used", true)]
		public static Type GetType(string typeName, string assemblyName)
		{
			return null;
		}
	}
}
