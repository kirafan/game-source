﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200007A RID: 122
	public sealed class LightProbeGroup : Behaviour
	{
		// Token: 0x17000202 RID: 514
		// (get) Token: 0x0600083D RID: 2109
		// (set) Token: 0x0600083E RID: 2110
		public extern Vector3[] probePositions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
