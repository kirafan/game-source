﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000166 RID: 358
	public sealed class CapsuleCollider2D : Collider2D
	{
		// Token: 0x17000619 RID: 1561
		// (get) Token: 0x06001A82 RID: 6786 RVA: 0x0002121C File Offset: 0x0001F41C
		// (set) Token: 0x06001A83 RID: 6787 RVA: 0x0002123C File Offset: 0x0001F43C
		public Vector2 size
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_size(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_size(ref value);
			}
		}

		// Token: 0x06001A84 RID: 6788
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_size(out Vector2 value);

		// Token: 0x06001A85 RID: 6789
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_size(ref Vector2 value);

		// Token: 0x1700061A RID: 1562
		// (get) Token: 0x06001A86 RID: 6790
		// (set) Token: 0x06001A87 RID: 6791
		public extern CapsuleDirection2D direction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
