﻿using System;

namespace UnityEngine
{
	// Token: 0x020001C0 RID: 448
	public enum AnimationPlayMode
	{
		// Token: 0x040004C4 RID: 1220
		Stop,
		// Token: 0x040004C5 RID: 1221
		Queue,
		// Token: 0x040004C6 RID: 1222
		Mix
	}
}
