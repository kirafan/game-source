﻿using System;

namespace UnityEngine
{
	// Token: 0x020002AC RID: 684
	[Obsolete("This is part of the legacy particle system, which is deprecated and will be removed in a future release. Use the ParticleSystem component instead.", false)]
	public enum ParticleRenderMode
	{
		// Token: 0x04000A26 RID: 2598
		Billboard,
		// Token: 0x04000A27 RID: 2599
		Stretch = 3,
		// Token: 0x04000A28 RID: 2600
		SortedBillboard = 2,
		// Token: 0x04000A29 RID: 2601
		HorizontalBillboard = 4,
		// Token: 0x04000A2A RID: 2602
		VerticalBillboard
	}
}
