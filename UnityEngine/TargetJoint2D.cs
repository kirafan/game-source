﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000177 RID: 375
	public sealed class TargetJoint2D : Joint2D
	{
		// Token: 0x1700065C RID: 1628
		// (get) Token: 0x06001B1D RID: 6941 RVA: 0x000217EC File Offset: 0x0001F9EC
		// (set) Token: 0x06001B1E RID: 6942 RVA: 0x0002180C File Offset: 0x0001FA0C
		public Vector2 anchor
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_anchor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_anchor(ref value);
			}
		}

		// Token: 0x06001B1F RID: 6943
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_anchor(out Vector2 value);

		// Token: 0x06001B20 RID: 6944
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_anchor(ref Vector2 value);

		// Token: 0x1700065D RID: 1629
		// (get) Token: 0x06001B21 RID: 6945 RVA: 0x00021818 File Offset: 0x0001FA18
		// (set) Token: 0x06001B22 RID: 6946 RVA: 0x00021838 File Offset: 0x0001FA38
		public Vector2 target
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_target(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_target(ref value);
			}
		}

		// Token: 0x06001B23 RID: 6947
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_target(out Vector2 value);

		// Token: 0x06001B24 RID: 6948
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_target(ref Vector2 value);

		// Token: 0x1700065E RID: 1630
		// (get) Token: 0x06001B25 RID: 6949
		// (set) Token: 0x06001B26 RID: 6950
		public extern bool autoConfigureTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700065F RID: 1631
		// (get) Token: 0x06001B27 RID: 6951
		// (set) Token: 0x06001B28 RID: 6952
		public extern float maxForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000660 RID: 1632
		// (get) Token: 0x06001B29 RID: 6953
		// (set) Token: 0x06001B2A RID: 6954
		public extern float dampingRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000661 RID: 1633
		// (get) Token: 0x06001B2B RID: 6955
		// (set) Token: 0x06001B2C RID: 6956
		public extern float frequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
