﻿using System;

namespace UnityEngine
{
	// Token: 0x02000198 RID: 408
	public struct AudioConfiguration
	{
		// Token: 0x04000445 RID: 1093
		public AudioSpeakerMode speakerMode;

		// Token: 0x04000446 RID: 1094
		public int dspBufferSize;

		// Token: 0x04000447 RID: 1095
		public int sampleRate;

		// Token: 0x04000448 RID: 1096
		public int numRealVoices;

		// Token: 0x04000449 RID: 1097
		public int numVirtualVoices;
	}
}
