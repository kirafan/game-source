﻿using System;

namespace UnityEngine
{
	// Token: 0x0200013B RID: 315
	public struct JointSpring
	{
		// Token: 0x0400037A RID: 890
		public float spring;

		// Token: 0x0400037B RID: 891
		public float damper;

		// Token: 0x0400037C RID: 892
		public float targetPosition;
	}
}
