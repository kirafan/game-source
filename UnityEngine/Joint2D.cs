﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200016F RID: 367
	public class Joint2D : Behaviour
	{
		// Token: 0x17000631 RID: 1585
		// (get) Token: 0x06001AB3 RID: 6835
		// (set) Token: 0x06001AB4 RID: 6836
		public extern Rigidbody2D connectedBody { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000632 RID: 1586
		// (get) Token: 0x06001AB5 RID: 6837
		// (set) Token: 0x06001AB6 RID: 6838
		public extern bool enableCollision { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000633 RID: 1587
		// (get) Token: 0x06001AB7 RID: 6839
		// (set) Token: 0x06001AB8 RID: 6840
		public extern float breakForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000634 RID: 1588
		// (get) Token: 0x06001AB9 RID: 6841
		// (set) Token: 0x06001ABA RID: 6842
		public extern float breakTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000635 RID: 1589
		// (get) Token: 0x06001ABB RID: 6843 RVA: 0x000215A4 File Offset: 0x0001F7A4
		public Vector2 reactionForce
		{
			get
			{
				return this.GetReactionForce(Time.fixedDeltaTime);
			}
		}

		// Token: 0x17000636 RID: 1590
		// (get) Token: 0x06001ABC RID: 6844 RVA: 0x000215C4 File Offset: 0x0001F7C4
		public float reactionTorque
		{
			get
			{
				return this.GetReactionTorque(Time.fixedDeltaTime);
			}
		}

		// Token: 0x06001ABD RID: 6845 RVA: 0x000215E4 File Offset: 0x0001F7E4
		public Vector2 GetReactionForce(float timeStep)
		{
			Vector2 result;
			Joint2D.Joint2D_CUSTOM_INTERNAL_GetReactionForce(this, timeStep, out result);
			return result;
		}

		// Token: 0x06001ABE RID: 6846
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Joint2D_CUSTOM_INTERNAL_GetReactionForce(Joint2D joint, float timeStep, out Vector2 value);

		// Token: 0x06001ABF RID: 6847 RVA: 0x00021604 File Offset: 0x0001F804
		public float GetReactionTorque(float timeStep)
		{
			return Joint2D.INTERNAL_CALL_GetReactionTorque(this, timeStep);
		}

		// Token: 0x06001AC0 RID: 6848
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float INTERNAL_CALL_GetReactionTorque(Joint2D self, float timeStep);
	}
}
