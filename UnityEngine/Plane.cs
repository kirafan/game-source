﻿using System;

namespace UnityEngine
{
	// Token: 0x02000356 RID: 854
	public struct Plane
	{
		// Token: 0x06002DC9 RID: 11721 RVA: 0x00048BB8 File Offset: 0x00046DB8
		public Plane(Vector3 inNormal, Vector3 inPoint)
		{
			this.m_Normal = Vector3.Normalize(inNormal);
			this.m_Distance = -Vector3.Dot(inNormal, inPoint);
		}

		// Token: 0x06002DCA RID: 11722 RVA: 0x00048BD8 File Offset: 0x00046DD8
		public Plane(Vector3 inNormal, float d)
		{
			this.m_Normal = Vector3.Normalize(inNormal);
			this.m_Distance = d;
		}

		// Token: 0x06002DCB RID: 11723 RVA: 0x00048BF0 File Offset: 0x00046DF0
		public Plane(Vector3 a, Vector3 b, Vector3 c)
		{
			this.m_Normal = Vector3.Normalize(Vector3.Cross(b - a, c - a));
			this.m_Distance = -Vector3.Dot(this.m_Normal, a);
		}

		// Token: 0x17000ADC RID: 2780
		// (get) Token: 0x06002DCC RID: 11724 RVA: 0x00048C24 File Offset: 0x00046E24
		// (set) Token: 0x06002DCD RID: 11725 RVA: 0x00048C40 File Offset: 0x00046E40
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
			set
			{
				this.m_Normal = value;
			}
		}

		// Token: 0x17000ADD RID: 2781
		// (get) Token: 0x06002DCE RID: 11726 RVA: 0x00048C4C File Offset: 0x00046E4C
		// (set) Token: 0x06002DCF RID: 11727 RVA: 0x00048C68 File Offset: 0x00046E68
		public float distance
		{
			get
			{
				return this.m_Distance;
			}
			set
			{
				this.m_Distance = value;
			}
		}

		// Token: 0x06002DD0 RID: 11728 RVA: 0x00048C74 File Offset: 0x00046E74
		public void SetNormalAndPosition(Vector3 inNormal, Vector3 inPoint)
		{
			this.normal = Vector3.Normalize(inNormal);
			this.distance = -Vector3.Dot(inNormal, inPoint);
		}

		// Token: 0x06002DD1 RID: 11729 RVA: 0x00048C94 File Offset: 0x00046E94
		public void Set3Points(Vector3 a, Vector3 b, Vector3 c)
		{
			this.normal = Vector3.Normalize(Vector3.Cross(b - a, c - a));
			this.distance = -Vector3.Dot(this.normal, a);
		}

		// Token: 0x06002DD2 RID: 11730 RVA: 0x00048CC8 File Offset: 0x00046EC8
		public float GetDistanceToPoint(Vector3 inPt)
		{
			return Vector3.Dot(this.normal, inPt) + this.distance;
		}

		// Token: 0x06002DD3 RID: 11731 RVA: 0x00048CF0 File Offset: 0x00046EF0
		public bool GetSide(Vector3 inPt)
		{
			return Vector3.Dot(this.normal, inPt) + this.distance > 0f;
		}

		// Token: 0x06002DD4 RID: 11732 RVA: 0x00048D20 File Offset: 0x00046F20
		public bool SameSide(Vector3 inPt0, Vector3 inPt1)
		{
			float distanceToPoint = this.GetDistanceToPoint(inPt0);
			float distanceToPoint2 = this.GetDistanceToPoint(inPt1);
			return (distanceToPoint > 0f && distanceToPoint2 > 0f) || (distanceToPoint <= 0f && distanceToPoint2 <= 0f);
		}

		// Token: 0x06002DD5 RID: 11733 RVA: 0x00048D80 File Offset: 0x00046F80
		public bool Raycast(Ray ray, out float enter)
		{
			float num = Vector3.Dot(ray.direction, this.normal);
			float num2 = -Vector3.Dot(ray.origin, this.normal) - this.distance;
			bool result;
			if (Mathf.Approximately(num, 0f))
			{
				enter = 0f;
				result = false;
			}
			else
			{
				enter = num2 / num;
				result = (enter > 0f);
			}
			return result;
		}

		// Token: 0x04000D23 RID: 3363
		private Vector3 m_Normal;

		// Token: 0x04000D24 RID: 3364
		private float m_Distance;
	}
}
