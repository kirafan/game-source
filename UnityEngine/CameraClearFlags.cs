﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F1 RID: 753
	public enum CameraClearFlags
	{
		// Token: 0x04000B21 RID: 2849
		Skybox = 1,
		// Token: 0x04000B22 RID: 2850
		Color,
		// Token: 0x04000B23 RID: 2851
		SolidColor = 2,
		// Token: 0x04000B24 RID: 2852
		Depth,
		// Token: 0x04000B25 RID: 2853
		Nothing
	}
}
