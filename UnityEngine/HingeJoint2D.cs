﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000174 RID: 372
	public sealed class HingeJoint2D : AnchoredJoint2D
	{
		// Token: 0x17000643 RID: 1603
		// (get) Token: 0x06001AE2 RID: 6882
		// (set) Token: 0x06001AE3 RID: 6883
		public extern bool useMotor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000644 RID: 1604
		// (get) Token: 0x06001AE4 RID: 6884
		// (set) Token: 0x06001AE5 RID: 6885
		public extern bool useLimits { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000645 RID: 1605
		// (get) Token: 0x06001AE6 RID: 6886 RVA: 0x000216A0 File Offset: 0x0001F8A0
		// (set) Token: 0x06001AE7 RID: 6887 RVA: 0x000216C0 File Offset: 0x0001F8C0
		public JointMotor2D motor
		{
			get
			{
				JointMotor2D result;
				this.INTERNAL_get_motor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_motor(ref value);
			}
		}

		// Token: 0x06001AE8 RID: 6888
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_motor(out JointMotor2D value);

		// Token: 0x06001AE9 RID: 6889
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_motor(ref JointMotor2D value);

		// Token: 0x17000646 RID: 1606
		// (get) Token: 0x06001AEA RID: 6890 RVA: 0x000216CC File Offset: 0x0001F8CC
		// (set) Token: 0x06001AEB RID: 6891 RVA: 0x000216EC File Offset: 0x0001F8EC
		public JointAngleLimits2D limits
		{
			get
			{
				JointAngleLimits2D result;
				this.INTERNAL_get_limits(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_limits(ref value);
			}
		}

		// Token: 0x06001AEC RID: 6892
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_limits(out JointAngleLimits2D value);

		// Token: 0x06001AED RID: 6893
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_limits(ref JointAngleLimits2D value);

		// Token: 0x17000647 RID: 1607
		// (get) Token: 0x06001AEE RID: 6894
		public extern JointLimitState2D limitState { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000648 RID: 1608
		// (get) Token: 0x06001AEF RID: 6895
		public extern float referenceAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000649 RID: 1609
		// (get) Token: 0x06001AF0 RID: 6896
		public extern float jointAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700064A RID: 1610
		// (get) Token: 0x06001AF1 RID: 6897
		public extern float jointSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001AF2 RID: 6898 RVA: 0x000216F8 File Offset: 0x0001F8F8
		public float GetMotorTorque(float timeStep)
		{
			return HingeJoint2D.INTERNAL_CALL_GetMotorTorque(this, timeStep);
		}

		// Token: 0x06001AF3 RID: 6899
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float INTERNAL_CALL_GetMotorTorque(HingeJoint2D self, float timeStep);
	}
}
