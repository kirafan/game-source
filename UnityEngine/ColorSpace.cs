﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F8 RID: 760
	public enum ColorSpace
	{
		// Token: 0x04000B46 RID: 2886
		Uninitialized = -1,
		// Token: 0x04000B47 RID: 2887
		Gamma,
		// Token: 0x04000B48 RID: 2888
		Linear
	}
}
