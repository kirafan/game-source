﻿using System;

namespace UnityEngine.Assertions
{
	// Token: 0x02000397 RID: 919
	public class AssertionException : Exception
	{
		// Token: 0x06002F8C RID: 12172 RVA: 0x0004E280 File Offset: 0x0004C480
		public AssertionException(string message, string userMessage) : base(message)
		{
			this.m_UserMessage = userMessage;
		}

		// Token: 0x17000B17 RID: 2839
		// (get) Token: 0x06002F8D RID: 12173 RVA: 0x0004E294 File Offset: 0x0004C494
		public override string Message
		{
			get
			{
				string text = base.Message;
				if (this.m_UserMessage != null)
				{
					text = text + '\n' + this.m_UserMessage;
				}
				return text;
			}
		}

		// Token: 0x04000D96 RID: 3478
		private string m_UserMessage;
	}
}
