﻿using System;

namespace UnityEngine.Assertions
{
	// Token: 0x02000398 RID: 920
	internal class AssertionMessageUtil
	{
		// Token: 0x06002F8F RID: 12175 RVA: 0x0004E2D8 File Offset: 0x0004C4D8
		public static string GetMessage(string failureMessage)
		{
			return UnityString.Format("{0} {1}", new object[]
			{
				"Assertion failed.",
				failureMessage
			});
		}

		// Token: 0x06002F90 RID: 12176 RVA: 0x0004E30C File Offset: 0x0004C50C
		public static string GetMessage(string failureMessage, string expected)
		{
			return AssertionMessageUtil.GetMessage(UnityString.Format("{0}{1}{2} {3}", new object[]
			{
				failureMessage,
				Environment.NewLine,
				"Expected:",
				expected
			}));
		}

		// Token: 0x06002F91 RID: 12177 RVA: 0x0004E350 File Offset: 0x0004C550
		public static string GetEqualityMessage(object actual, object expected, bool expectEqual)
		{
			return AssertionMessageUtil.GetMessage(UnityString.Format("Values are {0}equal.", new object[]
			{
				(!expectEqual) ? "" : "not "
			}), UnityString.Format("{0} {2} {1}", new object[]
			{
				actual,
				expected,
				(!expectEqual) ? "!=" : "=="
			}));
		}

		// Token: 0x06002F92 RID: 12178 RVA: 0x0004E3C4 File Offset: 0x0004C5C4
		public static string NullFailureMessage(object value, bool expectNull)
		{
			return AssertionMessageUtil.GetMessage(UnityString.Format("Value was {0}Null", new object[]
			{
				(!expectNull) ? "" : "not "
			}), UnityString.Format("Value was {0}Null", new object[]
			{
				(!expectNull) ? "not " : ""
			}));
		}

		// Token: 0x06002F93 RID: 12179 RVA: 0x0004E430 File Offset: 0x0004C630
		public static string BooleanFailureMessage(bool expected)
		{
			return AssertionMessageUtil.GetMessage("Value was " + !expected, expected.ToString());
		}

		// Token: 0x04000D97 RID: 3479
		private const string k_Expected = "Expected:";

		// Token: 0x04000D98 RID: 3480
		private const string k_AssertionFailed = "Assertion failed.";
	}
}
