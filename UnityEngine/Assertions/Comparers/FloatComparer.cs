﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Assertions.Comparers
{
	// Token: 0x02000399 RID: 921
	public class FloatComparer : IEqualityComparer<float>
	{
		// Token: 0x06002F94 RID: 12180 RVA: 0x0004E46C File Offset: 0x0004C66C
		public FloatComparer() : this(1E-05f, false)
		{
		}

		// Token: 0x06002F95 RID: 12181 RVA: 0x0004E47C File Offset: 0x0004C67C
		public FloatComparer(bool relative) : this(1E-05f, relative)
		{
		}

		// Token: 0x06002F96 RID: 12182 RVA: 0x0004E48C File Offset: 0x0004C68C
		public FloatComparer(float error) : this(error, false)
		{
		}

		// Token: 0x06002F97 RID: 12183 RVA: 0x0004E498 File Offset: 0x0004C698
		public FloatComparer(float error, bool relative)
		{
			this.m_Error = error;
			this.m_Relative = relative;
		}

		// Token: 0x06002F98 RID: 12184 RVA: 0x0004E4B0 File Offset: 0x0004C6B0
		public bool Equals(float a, float b)
		{
			return (!this.m_Relative) ? FloatComparer.AreEqual(a, b, this.m_Error) : FloatComparer.AreEqualRelative(a, b, this.m_Error);
		}

		// Token: 0x06002F99 RID: 12185 RVA: 0x0004E4F0 File Offset: 0x0004C6F0
		public int GetHashCode(float obj)
		{
			return base.GetHashCode();
		}

		// Token: 0x06002F9A RID: 12186 RVA: 0x0004E50C File Offset: 0x0004C70C
		public static bool AreEqual(float expected, float actual, float error)
		{
			return Math.Abs(actual - expected) <= error;
		}

		// Token: 0x06002F9B RID: 12187 RVA: 0x0004E530 File Offset: 0x0004C730
		public static bool AreEqualRelative(float expected, float actual, float error)
		{
			bool result;
			if (expected == actual)
			{
				result = true;
			}
			else
			{
				float num = Math.Abs(expected);
				float num2 = Math.Abs(actual);
				float num3 = Math.Abs((actual - expected) / ((num <= num2) ? num2 : num));
				result = (num3 <= error);
			}
			return result;
		}

		// Token: 0x04000D99 RID: 3481
		private readonly float m_Error;

		// Token: 0x04000D9A RID: 3482
		private readonly bool m_Relative;

		// Token: 0x04000D9B RID: 3483
		public static readonly FloatComparer s_ComparerWithDefaultTolerance = new FloatComparer(1E-05f);

		// Token: 0x04000D9C RID: 3484
		public const float kEpsilon = 1E-05f;
	}
}
