﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using UnityEngine.Assertions.Comparers;

namespace UnityEngine.Assertions
{
	// Token: 0x02000396 RID: 918
	[DebuggerStepThrough]
	public static class Assert
	{
		// Token: 0x06002F6F RID: 12143 RVA: 0x0004DEC4 File Offset: 0x0004C0C4
		private static void Fail(string message, string userMessage)
		{
			if (Debugger.IsAttached)
			{
				throw new AssertionException(message, userMessage);
			}
			if (Assert.raiseExceptions)
			{
				throw new AssertionException(message, userMessage);
			}
			if (message == null)
			{
				message = "Assertion has failed\n";
			}
			if (userMessage != null)
			{
				message = userMessage + '\n' + message;
			}
			Debug.LogAssertion(message);
		}

		// Token: 0x06002F70 RID: 12144 RVA: 0x0004DF20 File Offset: 0x0004C120
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Assert.Equals should not be used for Assertions", true)]
		public new static bool Equals(object obj1, object obj2)
		{
			throw new InvalidOperationException("Assert.Equals should not be used for Assertions");
		}

		// Token: 0x06002F71 RID: 12145 RVA: 0x0004DF30 File Offset: 0x0004C130
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Assert.ReferenceEquals should not be used for Assertions", true)]
		public new static bool ReferenceEquals(object obj1, object obj2)
		{
			throw new InvalidOperationException("Assert.ReferenceEquals should not be used for Assertions");
		}

		// Token: 0x06002F72 RID: 12146 RVA: 0x0004DF40 File Offset: 0x0004C140
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsTrue(bool condition)
		{
			Assert.IsTrue(condition, null);
		}

		// Token: 0x06002F73 RID: 12147 RVA: 0x0004DF4C File Offset: 0x0004C14C
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsTrue(bool condition, string message)
		{
			if (!condition)
			{
				Assert.Fail(AssertionMessageUtil.BooleanFailureMessage(true), message);
			}
		}

		// Token: 0x06002F74 RID: 12148 RVA: 0x0004DF64 File Offset: 0x0004C164
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsFalse(bool condition)
		{
			Assert.IsFalse(condition, null);
		}

		// Token: 0x06002F75 RID: 12149 RVA: 0x0004DF70 File Offset: 0x0004C170
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsFalse(bool condition, string message)
		{
			if (condition)
			{
				Assert.Fail(AssertionMessageUtil.BooleanFailureMessage(false), message);
			}
		}

		// Token: 0x06002F76 RID: 12150 RVA: 0x0004DF88 File Offset: 0x0004C188
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreApproximatelyEqual(float expected, float actual)
		{
			Assert.AreEqual<float>(expected, actual, null, FloatComparer.s_ComparerWithDefaultTolerance);
		}

		// Token: 0x06002F77 RID: 12151 RVA: 0x0004DF98 File Offset: 0x0004C198
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreApproximatelyEqual(float expected, float actual, string message)
		{
			Assert.AreEqual<float>(expected, actual, message, FloatComparer.s_ComparerWithDefaultTolerance);
		}

		// Token: 0x06002F78 RID: 12152 RVA: 0x0004DFA8 File Offset: 0x0004C1A8
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreApproximatelyEqual(float expected, float actual, float tolerance)
		{
			Assert.AreApproximatelyEqual(expected, actual, tolerance, null);
		}

		// Token: 0x06002F79 RID: 12153 RVA: 0x0004DFB4 File Offset: 0x0004C1B4
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreApproximatelyEqual(float expected, float actual, float tolerance, string message)
		{
			Assert.AreEqual<float>(expected, actual, message, new FloatComparer(tolerance));
		}

		// Token: 0x06002F7A RID: 12154 RVA: 0x0004DFC8 File Offset: 0x0004C1C8
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotApproximatelyEqual(float expected, float actual)
		{
			Assert.AreNotEqual<float>(expected, actual, null, FloatComparer.s_ComparerWithDefaultTolerance);
		}

		// Token: 0x06002F7B RID: 12155 RVA: 0x0004DFD8 File Offset: 0x0004C1D8
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotApproximatelyEqual(float expected, float actual, string message)
		{
			Assert.AreNotEqual<float>(expected, actual, message, FloatComparer.s_ComparerWithDefaultTolerance);
		}

		// Token: 0x06002F7C RID: 12156 RVA: 0x0004DFE8 File Offset: 0x0004C1E8
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotApproximatelyEqual(float expected, float actual, float tolerance)
		{
			Assert.AreNotApproximatelyEqual(expected, actual, tolerance, null);
		}

		// Token: 0x06002F7D RID: 12157 RVA: 0x0004DFF4 File Offset: 0x0004C1F4
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotApproximatelyEqual(float expected, float actual, float tolerance, string message)
		{
			Assert.AreNotEqual<float>(expected, actual, message, new FloatComparer(tolerance));
		}

		// Token: 0x06002F7E RID: 12158 RVA: 0x0004E008 File Offset: 0x0004C208
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual<T>(T expected, T actual)
		{
			Assert.AreEqual<T>(expected, actual, null);
		}

		// Token: 0x06002F7F RID: 12159 RVA: 0x0004E014 File Offset: 0x0004C214
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual<T>(T expected, T actual, string message)
		{
			Assert.AreEqual<T>(expected, actual, message, EqualityComparer<T>.Default);
		}

		// Token: 0x06002F80 RID: 12160 RVA: 0x0004E024 File Offset: 0x0004C224
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual<T>(T expected, T actual, string message, IEqualityComparer<T> comparer)
		{
			if (typeof(Object).IsAssignableFrom(typeof(T)))
			{
				Assert.AreEqual(expected as Object, actual as Object, message);
			}
			else if (!comparer.Equals(actual, expected))
			{
				Assert.Fail(AssertionMessageUtil.GetEqualityMessage(actual, expected, true), message);
			}
		}

		// Token: 0x06002F81 RID: 12161 RVA: 0x0004E098 File Offset: 0x0004C298
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreEqual(Object expected, Object actual, string message)
		{
			if (actual != expected)
			{
				Assert.Fail(AssertionMessageUtil.GetEqualityMessage(actual, expected, true), message);
			}
		}

		// Token: 0x06002F82 RID: 12162 RVA: 0x0004E0B8 File Offset: 0x0004C2B8
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual<T>(T expected, T actual)
		{
			Assert.AreNotEqual<T>(expected, actual, null);
		}

		// Token: 0x06002F83 RID: 12163 RVA: 0x0004E0C4 File Offset: 0x0004C2C4
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual<T>(T expected, T actual, string message)
		{
			Assert.AreNotEqual<T>(expected, actual, message, EqualityComparer<T>.Default);
		}

		// Token: 0x06002F84 RID: 12164 RVA: 0x0004E0D4 File Offset: 0x0004C2D4
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual<T>(T expected, T actual, string message, IEqualityComparer<T> comparer)
		{
			if (typeof(Object).IsAssignableFrom(typeof(T)))
			{
				Assert.AreNotEqual(expected as Object, actual as Object, message);
			}
			else if (comparer.Equals(actual, expected))
			{
				Assert.Fail(AssertionMessageUtil.GetEqualityMessage(actual, expected, false), message);
			}
		}

		// Token: 0x06002F85 RID: 12165 RVA: 0x0004E148 File Offset: 0x0004C348
		[Conditional("UNITY_ASSERTIONS")]
		public static void AreNotEqual(Object expected, Object actual, string message)
		{
			if (actual == expected)
			{
				Assert.Fail(AssertionMessageUtil.GetEqualityMessage(actual, expected, false), message);
			}
		}

		// Token: 0x06002F86 RID: 12166 RVA: 0x0004E168 File Offset: 0x0004C368
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNull<T>(T value) where T : class
		{
			Assert.IsNull<T>(value, null);
		}

		// Token: 0x06002F87 RID: 12167 RVA: 0x0004E174 File Offset: 0x0004C374
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNull<T>(T value, string message) where T : class
		{
			if (typeof(Object).IsAssignableFrom(typeof(T)))
			{
				Assert.IsNull(value as Object, message);
			}
			else if (value != null)
			{
				Assert.Fail(AssertionMessageUtil.NullFailureMessage(value, true), message);
			}
		}

		// Token: 0x06002F88 RID: 12168 RVA: 0x0004E1D8 File Offset: 0x0004C3D8
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNull(Object value, string message)
		{
			if (value != null)
			{
				Assert.Fail(AssertionMessageUtil.NullFailureMessage(value, true), message);
			}
		}

		// Token: 0x06002F89 RID: 12169 RVA: 0x0004E1F4 File Offset: 0x0004C3F4
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNotNull<T>(T value) where T : class
		{
			Assert.IsNotNull<T>(value, null);
		}

		// Token: 0x06002F8A RID: 12170 RVA: 0x0004E200 File Offset: 0x0004C400
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNotNull<T>(T value, string message) where T : class
		{
			if (typeof(Object).IsAssignableFrom(typeof(T)))
			{
				Assert.IsNotNull(value as Object, message);
			}
			else if (value == null)
			{
				Assert.Fail(AssertionMessageUtil.NullFailureMessage(value, false), message);
			}
		}

		// Token: 0x06002F8B RID: 12171 RVA: 0x0004E264 File Offset: 0x0004C464
		[Conditional("UNITY_ASSERTIONS")]
		public static void IsNotNull(Object value, string message)
		{
			if (value == null)
			{
				Assert.Fail(AssertionMessageUtil.NullFailureMessage(value, false), message);
			}
		}

		// Token: 0x04000D94 RID: 3476
		internal const string UNITY_ASSERTIONS = "UNITY_ASSERTIONS";

		// Token: 0x04000D95 RID: 3477
		public static bool raiseExceptions;
	}
}
