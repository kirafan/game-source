﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000E7 RID: 231
	public sealed class DynamicGI
	{
		// Token: 0x1700036D RID: 877
		// (get) Token: 0x06001036 RID: 4150
		// (set) Token: 0x06001037 RID: 4151
		public static extern float indirectScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700036E RID: 878
		// (get) Token: 0x06001038 RID: 4152
		// (set) Token: 0x06001039 RID: 4153
		public static extern float updateThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600103A RID: 4154 RVA: 0x00016948 File Offset: 0x00014B48
		public static void SetEmissive(Renderer renderer, Color color)
		{
			DynamicGI.INTERNAL_CALL_SetEmissive(renderer, ref color);
		}

		// Token: 0x0600103B RID: 4155
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetEmissive(Renderer renderer, ref Color color);

		// Token: 0x0600103C RID: 4156 RVA: 0x00016954 File Offset: 0x00014B54
		public static void UpdateMaterials(Renderer renderer)
		{
			DynamicGI.UpdateMaterialsForRenderer(renderer);
		}

		// Token: 0x0600103D RID: 4157
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void UpdateMaterialsForRenderer(Renderer renderer);

		// Token: 0x0600103E RID: 4158 RVA: 0x00016960 File Offset: 0x00014B60
		public static void UpdateMaterials(Terrain terrain)
		{
			if (terrain == null)
			{
				throw new ArgumentNullException("terrain");
			}
			if (terrain.terrainData == null)
			{
				throw new ArgumentException("Invalid terrainData.");
			}
			DynamicGI.UpdateMaterialsForTerrain(terrain, new Rect(0f, 0f, 1f, 1f));
		}

		// Token: 0x0600103F RID: 4159 RVA: 0x000169C0 File Offset: 0x00014BC0
		public static void UpdateMaterials(Terrain terrain, int x, int y, int width, int height)
		{
			if (terrain == null)
			{
				throw new ArgumentNullException("terrain");
			}
			if (terrain.terrainData == null)
			{
				throw new ArgumentException("Invalid terrainData.");
			}
			float num = (float)terrain.terrainData.alphamapWidth;
			float num2 = (float)terrain.terrainData.alphamapHeight;
			DynamicGI.UpdateMaterialsForTerrain(terrain, new Rect((float)x / num, (float)y / num2, (float)width / num, (float)height / num2));
		}

		// Token: 0x06001040 RID: 4160 RVA: 0x00016A38 File Offset: 0x00014C38
		internal static void UpdateMaterialsForTerrain(Terrain terrain, Rect uvBounds)
		{
			DynamicGI.INTERNAL_CALL_UpdateMaterialsForTerrain(terrain, ref uvBounds);
		}

		// Token: 0x06001041 RID: 4161
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UpdateMaterialsForTerrain(Terrain terrain, ref Rect uvBounds);

		// Token: 0x06001042 RID: 4162
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void UpdateEnvironment();

		// Token: 0x1700036F RID: 879
		// (get) Token: 0x06001043 RID: 4163
		// (set) Token: 0x06001044 RID: 4164
		public static extern bool synchronousMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
