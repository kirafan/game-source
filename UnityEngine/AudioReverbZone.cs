﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001A8 RID: 424
	public sealed class AudioReverbZone : Behaviour
	{
		// Token: 0x17000739 RID: 1849
		// (get) Token: 0x06001D76 RID: 7542
		// (set) Token: 0x06001D77 RID: 7543
		public extern float minDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700073A RID: 1850
		// (get) Token: 0x06001D78 RID: 7544
		// (set) Token: 0x06001D79 RID: 7545
		public extern float maxDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700073B RID: 1851
		// (get) Token: 0x06001D7A RID: 7546
		// (set) Token: 0x06001D7B RID: 7547
		public extern AudioReverbPreset reverbPreset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700073C RID: 1852
		// (get) Token: 0x06001D7C RID: 7548
		// (set) Token: 0x06001D7D RID: 7549
		public extern int room { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700073D RID: 1853
		// (get) Token: 0x06001D7E RID: 7550
		// (set) Token: 0x06001D7F RID: 7551
		public extern int roomHF { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700073E RID: 1854
		// (get) Token: 0x06001D80 RID: 7552
		// (set) Token: 0x06001D81 RID: 7553
		public extern int roomLF { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700073F RID: 1855
		// (get) Token: 0x06001D82 RID: 7554
		// (set) Token: 0x06001D83 RID: 7555
		public extern float decayTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000740 RID: 1856
		// (get) Token: 0x06001D84 RID: 7556
		// (set) Token: 0x06001D85 RID: 7557
		public extern float decayHFRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000741 RID: 1857
		// (get) Token: 0x06001D86 RID: 7558
		// (set) Token: 0x06001D87 RID: 7559
		public extern int reflections { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000742 RID: 1858
		// (get) Token: 0x06001D88 RID: 7560
		// (set) Token: 0x06001D89 RID: 7561
		public extern float reflectionsDelay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000743 RID: 1859
		// (get) Token: 0x06001D8A RID: 7562
		// (set) Token: 0x06001D8B RID: 7563
		public extern int reverb { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000744 RID: 1860
		// (get) Token: 0x06001D8C RID: 7564
		// (set) Token: 0x06001D8D RID: 7565
		public extern float reverbDelay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000745 RID: 1861
		// (get) Token: 0x06001D8E RID: 7566
		// (set) Token: 0x06001D8F RID: 7567
		public extern float HFReference { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000746 RID: 1862
		// (get) Token: 0x06001D90 RID: 7568
		// (set) Token: 0x06001D91 RID: 7569
		public extern float LFReference { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000747 RID: 1863
		// (get) Token: 0x06001D92 RID: 7570
		// (set) Token: 0x06001D93 RID: 7571
		public extern float roomRolloffFactor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000748 RID: 1864
		// (get) Token: 0x06001D94 RID: 7572
		// (set) Token: 0x06001D95 RID: 7573
		public extern float diffusion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000749 RID: 1865
		// (get) Token: 0x06001D96 RID: 7574
		// (set) Token: 0x06001D97 RID: 7575
		public extern float density { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
