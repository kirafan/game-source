﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001C4 RID: 452
	[UsedByNativeCode]
	public sealed class AnimationState : TrackedReference
	{
		// Token: 0x170007A0 RID: 1952
		// (get) Token: 0x06001EA7 RID: 7847
		// (set) Token: 0x06001EA8 RID: 7848
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007A1 RID: 1953
		// (get) Token: 0x06001EA9 RID: 7849
		// (set) Token: 0x06001EAA RID: 7850
		public extern float weight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007A2 RID: 1954
		// (get) Token: 0x06001EAB RID: 7851
		// (set) Token: 0x06001EAC RID: 7852
		public extern WrapMode wrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007A3 RID: 1955
		// (get) Token: 0x06001EAD RID: 7853
		// (set) Token: 0x06001EAE RID: 7854
		public extern float time { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007A4 RID: 1956
		// (get) Token: 0x06001EAF RID: 7855
		// (set) Token: 0x06001EB0 RID: 7856
		public extern float normalizedTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007A5 RID: 1957
		// (get) Token: 0x06001EB1 RID: 7857
		// (set) Token: 0x06001EB2 RID: 7858
		public extern float speed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007A6 RID: 1958
		// (get) Token: 0x06001EB3 RID: 7859
		// (set) Token: 0x06001EB4 RID: 7860
		public extern float normalizedSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007A7 RID: 1959
		// (get) Token: 0x06001EB5 RID: 7861
		public extern float length { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007A8 RID: 1960
		// (get) Token: 0x06001EB6 RID: 7862
		// (set) Token: 0x06001EB7 RID: 7863
		public extern int layer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007A9 RID: 1961
		// (get) Token: 0x06001EB8 RID: 7864
		public extern AnimationClip clip { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001EB9 RID: 7865
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddMixingTransform(Transform mix, [DefaultValue("true")] bool recursive);

		// Token: 0x06001EBA RID: 7866 RVA: 0x0002324C File Offset: 0x0002144C
		[ExcludeFromDocs]
		public void AddMixingTransform(Transform mix)
		{
			bool recursive = true;
			this.AddMixingTransform(mix, recursive);
		}

		// Token: 0x06001EBB RID: 7867
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RemoveMixingTransform(Transform mix);

		// Token: 0x170007AA RID: 1962
		// (get) Token: 0x06001EBC RID: 7868
		// (set) Token: 0x06001EBD RID: 7869
		public extern string name { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007AB RID: 1963
		// (get) Token: 0x06001EBE RID: 7870
		// (set) Token: 0x06001EBF RID: 7871
		public extern AnimationBlendMode blendMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
