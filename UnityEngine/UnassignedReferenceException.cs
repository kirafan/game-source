﻿using System;
using System.Runtime.Serialization;

namespace UnityEngine
{
	// Token: 0x02000371 RID: 881
	[Serializable]
	public class UnassignedReferenceException : SystemException
	{
		// Token: 0x06002E46 RID: 11846 RVA: 0x0004A638 File Offset: 0x00048838
		public UnassignedReferenceException() : base("A Unity Runtime error occurred!")
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E47 RID: 11847 RVA: 0x0004A654 File Offset: 0x00048854
		public UnassignedReferenceException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E48 RID: 11848 RVA: 0x0004A66C File Offset: 0x0004886C
		public UnassignedReferenceException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E49 RID: 11849 RVA: 0x0004A684 File Offset: 0x00048884
		protected UnassignedReferenceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000D4B RID: 3403
		private const int Result = -2147467261;

		// Token: 0x04000D4C RID: 3404
		private string unityStackTrace;
	}
}
