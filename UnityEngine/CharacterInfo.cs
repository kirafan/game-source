﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000204 RID: 516
	[UsedByNativeCode]
	public struct CharacterInfo
	{
		// Token: 0x170008B0 RID: 2224
		// (get) Token: 0x060022E1 RID: 8929 RVA: 0x000284C8 File Offset: 0x000266C8
		// (set) Token: 0x060022E2 RID: 8930 RVA: 0x000284E4 File Offset: 0x000266E4
		public int advance
		{
			get
			{
				return (int)this.width;
			}
			set
			{
				this.width = (float)value;
			}
		}

		// Token: 0x170008B1 RID: 2225
		// (get) Token: 0x060022E3 RID: 8931 RVA: 0x000284F0 File Offset: 0x000266F0
		// (set) Token: 0x060022E4 RID: 8932 RVA: 0x00028514 File Offset: 0x00026714
		public int glyphWidth
		{
			get
			{
				return (int)this.vert.width;
			}
			set
			{
				this.vert.width = (float)value;
			}
		}

		// Token: 0x170008B2 RID: 2226
		// (get) Token: 0x060022E5 RID: 8933 RVA: 0x00028524 File Offset: 0x00026724
		// (set) Token: 0x060022E6 RID: 8934 RVA: 0x00028548 File Offset: 0x00026748
		public int glyphHeight
		{
			get
			{
				return (int)(-(int)this.vert.height);
			}
			set
			{
				float height = this.vert.height;
				this.vert.height = (float)(-(float)value);
				this.vert.y = this.vert.y + (height - this.vert.height);
			}
		}

		// Token: 0x170008B3 RID: 2227
		// (get) Token: 0x060022E7 RID: 8935 RVA: 0x00028590 File Offset: 0x00026790
		// (set) Token: 0x060022E8 RID: 8936 RVA: 0x000285B4 File Offset: 0x000267B4
		public int bearing
		{
			get
			{
				return (int)this.vert.x;
			}
			set
			{
				this.vert.x = (float)value;
			}
		}

		// Token: 0x170008B4 RID: 2228
		// (get) Token: 0x060022E9 RID: 8937 RVA: 0x000285C4 File Offset: 0x000267C4
		// (set) Token: 0x060022EA RID: 8938 RVA: 0x000285F4 File Offset: 0x000267F4
		public int minY
		{
			get
			{
				return (int)(this.vert.y + this.vert.height);
			}
			set
			{
				this.vert.height = (float)value - this.vert.y;
			}
		}

		// Token: 0x170008B5 RID: 2229
		// (get) Token: 0x060022EB RID: 8939 RVA: 0x00028610 File Offset: 0x00026810
		// (set) Token: 0x060022EC RID: 8940 RVA: 0x00028634 File Offset: 0x00026834
		public int maxY
		{
			get
			{
				return (int)this.vert.y;
			}
			set
			{
				float y = this.vert.y;
				this.vert.y = (float)value;
				this.vert.height = this.vert.height + (y - this.vert.y);
			}
		}

		// Token: 0x170008B6 RID: 2230
		// (get) Token: 0x060022ED RID: 8941 RVA: 0x0002867C File Offset: 0x0002687C
		// (set) Token: 0x060022EE RID: 8942 RVA: 0x000286A0 File Offset: 0x000268A0
		public int minX
		{
			get
			{
				return (int)this.vert.x;
			}
			set
			{
				float x = this.vert.x;
				this.vert.x = (float)value;
				this.vert.width = this.vert.width + (x - this.vert.x);
			}
		}

		// Token: 0x170008B7 RID: 2231
		// (get) Token: 0x060022EF RID: 8943 RVA: 0x000286E8 File Offset: 0x000268E8
		// (set) Token: 0x060022F0 RID: 8944 RVA: 0x00028718 File Offset: 0x00026918
		public int maxX
		{
			get
			{
				return (int)(this.vert.x + this.vert.width);
			}
			set
			{
				this.vert.width = (float)value - this.vert.x;
			}
		}

		// Token: 0x170008B8 RID: 2232
		// (get) Token: 0x060022F1 RID: 8945 RVA: 0x00028734 File Offset: 0x00026934
		// (set) Token: 0x060022F2 RID: 8946 RVA: 0x00028764 File Offset: 0x00026964
		internal Vector2 uvBottomLeftUnFlipped
		{
			get
			{
				return new Vector2(this.uv.x, this.uv.y);
			}
			set
			{
				Vector2 uvTopRightUnFlipped = this.uvTopRightUnFlipped;
				this.uv.x = value.x;
				this.uv.y = value.y;
				this.uv.width = uvTopRightUnFlipped.x - this.uv.x;
				this.uv.height = uvTopRightUnFlipped.y - this.uv.y;
			}
		}

		// Token: 0x170008B9 RID: 2233
		// (get) Token: 0x060022F3 RID: 8947 RVA: 0x000287DC File Offset: 0x000269DC
		// (set) Token: 0x060022F4 RID: 8948 RVA: 0x00028818 File Offset: 0x00026A18
		internal Vector2 uvBottomRightUnFlipped
		{
			get
			{
				return new Vector2(this.uv.x + this.uv.width, this.uv.y);
			}
			set
			{
				Vector2 uvTopRightUnFlipped = this.uvTopRightUnFlipped;
				this.uv.width = value.x - this.uv.x;
				this.uv.y = value.y;
				this.uv.height = uvTopRightUnFlipped.y - this.uv.y;
			}
		}

		// Token: 0x170008BA RID: 2234
		// (get) Token: 0x060022F5 RID: 8949 RVA: 0x0002887C File Offset: 0x00026A7C
		// (set) Token: 0x060022F6 RID: 8950 RVA: 0x000288C4 File Offset: 0x00026AC4
		internal Vector2 uvTopRightUnFlipped
		{
			get
			{
				return new Vector2(this.uv.x + this.uv.width, this.uv.y + this.uv.height);
			}
			set
			{
				this.uv.width = value.x - this.uv.x;
				this.uv.height = value.y - this.uv.y;
			}
		}

		// Token: 0x170008BB RID: 2235
		// (get) Token: 0x060022F7 RID: 8951 RVA: 0x00028904 File Offset: 0x00026B04
		// (set) Token: 0x060022F8 RID: 8952 RVA: 0x00028940 File Offset: 0x00026B40
		internal Vector2 uvTopLeftUnFlipped
		{
			get
			{
				return new Vector2(this.uv.x, this.uv.y + this.uv.height);
			}
			set
			{
				Vector2 uvTopRightUnFlipped = this.uvTopRightUnFlipped;
				this.uv.x = value.x;
				this.uv.height = value.y - this.uv.y;
				this.uv.width = uvTopRightUnFlipped.x - this.uv.x;
			}
		}

		// Token: 0x170008BC RID: 2236
		// (get) Token: 0x060022F9 RID: 8953 RVA: 0x000289A4 File Offset: 0x00026BA4
		// (set) Token: 0x060022FA RID: 8954 RVA: 0x000289C0 File Offset: 0x00026BC0
		public Vector2 uvBottomLeft
		{
			get
			{
				return this.uvBottomLeftUnFlipped;
			}
			set
			{
				this.uvBottomLeftUnFlipped = value;
			}
		}

		// Token: 0x170008BD RID: 2237
		// (get) Token: 0x060022FB RID: 8955 RVA: 0x000289CC File Offset: 0x00026BCC
		// (set) Token: 0x060022FC RID: 8956 RVA: 0x00028A00 File Offset: 0x00026C00
		public Vector2 uvBottomRight
		{
			get
			{
				return (!this.flipped) ? this.uvBottomRightUnFlipped : this.uvTopLeftUnFlipped;
			}
			set
			{
				if (this.flipped)
				{
					this.uvTopLeftUnFlipped = value;
				}
				else
				{
					this.uvBottomRightUnFlipped = value;
				}
			}
		}

		// Token: 0x170008BE RID: 2238
		// (get) Token: 0x060022FD RID: 8957 RVA: 0x00028A24 File Offset: 0x00026C24
		// (set) Token: 0x060022FE RID: 8958 RVA: 0x00028A40 File Offset: 0x00026C40
		public Vector2 uvTopRight
		{
			get
			{
				return this.uvTopRightUnFlipped;
			}
			set
			{
				this.uvTopRightUnFlipped = value;
			}
		}

		// Token: 0x170008BF RID: 2239
		// (get) Token: 0x060022FF RID: 8959 RVA: 0x00028A4C File Offset: 0x00026C4C
		// (set) Token: 0x06002300 RID: 8960 RVA: 0x00028A80 File Offset: 0x00026C80
		public Vector2 uvTopLeft
		{
			get
			{
				return (!this.flipped) ? this.uvTopLeftUnFlipped : this.uvBottomRightUnFlipped;
			}
			set
			{
				if (this.flipped)
				{
					this.uvBottomRightUnFlipped = value;
				}
				else
				{
					this.uvTopLeftUnFlipped = value;
				}
			}
		}

		// Token: 0x04000625 RID: 1573
		public int index;

		// Token: 0x04000626 RID: 1574
		[Obsolete("CharacterInfo.uv is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead.")]
		public Rect uv;

		// Token: 0x04000627 RID: 1575
		[Obsolete("CharacterInfo.vert is deprecated. Use minX, maxX, minY, maxY instead.")]
		public Rect vert;

		// Token: 0x04000628 RID: 1576
		[Obsolete("CharacterInfo.width is deprecated. Use advance instead.")]
		public float width;

		// Token: 0x04000629 RID: 1577
		public int size;

		// Token: 0x0400062A RID: 1578
		public FontStyle style;

		// Token: 0x0400062B RID: 1579
		[Obsolete("CharacterInfo.flipped is deprecated. Use uvBottomLeft, uvBottomRight, uvTopRight or uvTopLeft instead, which will be correct regardless of orientation.")]
		public bool flipped;
	}
}
