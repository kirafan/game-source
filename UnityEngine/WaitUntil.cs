﻿using System;

namespace UnityEngine
{
	// Token: 0x0200001B RID: 27
	public sealed class WaitUntil : CustomYieldInstruction
	{
		// Token: 0x060001EE RID: 494 RVA: 0x00004F34 File Offset: 0x00003134
		public WaitUntil(Func<bool> predicate)
		{
			this.m_Predicate = predicate;
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060001EF RID: 495 RVA: 0x00004F44 File Offset: 0x00003144
		public override bool keepWaiting
		{
			get
			{
				return !this.m_Predicate();
			}
		}

		// Token: 0x0400002A RID: 42
		private Func<bool> m_Predicate;
	}
}
