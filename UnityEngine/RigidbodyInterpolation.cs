﻿using System;

namespace UnityEngine
{
	// Token: 0x02000139 RID: 313
	public enum RigidbodyInterpolation
	{
		// Token: 0x04000374 RID: 884
		None,
		// Token: 0x04000375 RID: 885
		Interpolate,
		// Token: 0x04000376 RID: 886
		Extrapolate
	}
}
