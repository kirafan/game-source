﻿using System;

namespace UnityEngine
{
	// Token: 0x0200022D RID: 557
	internal sealed class GUIWordWrapSizer : GUILayoutEntry
	{
		// Token: 0x06002597 RID: 9623 RVA: 0x00031E08 File Offset: 0x00030008
		public GUIWordWrapSizer(GUIStyle style, GUIContent content, GUILayoutOption[] options) : base(0f, 0f, 0f, 0f, style)
		{
			this.m_Content = new GUIContent(content);
			this.ApplyOptions(options);
			this.m_ForcedMinHeight = this.minHeight;
			this.m_ForcedMaxHeight = this.maxHeight;
		}

		// Token: 0x06002598 RID: 9624 RVA: 0x00031E5C File Offset: 0x0003005C
		public override void CalcWidth()
		{
			if (this.minWidth == 0f || this.maxWidth == 0f)
			{
				float minWidth;
				float maxWidth;
				base.style.CalcMinMaxWidth(this.m_Content, out minWidth, out maxWidth);
				if (this.minWidth == 0f)
				{
					this.minWidth = minWidth;
				}
				if (this.maxWidth == 0f)
				{
					this.maxWidth = maxWidth;
				}
			}
		}

		// Token: 0x06002599 RID: 9625 RVA: 0x00031ED0 File Offset: 0x000300D0
		public override void CalcHeight()
		{
			if (this.m_ForcedMinHeight == 0f || this.m_ForcedMaxHeight == 0f)
			{
				float num = base.style.CalcHeight(this.m_Content, this.rect.width);
				if (this.m_ForcedMinHeight == 0f)
				{
					this.minHeight = num;
				}
				else
				{
					this.minHeight = this.m_ForcedMinHeight;
				}
				if (this.m_ForcedMaxHeight == 0f)
				{
					this.maxHeight = num;
				}
				else
				{
					this.maxHeight = this.m_ForcedMaxHeight;
				}
			}
		}

		// Token: 0x0400081C RID: 2076
		private readonly GUIContent m_Content;

		// Token: 0x0400081D RID: 2077
		private readonly float m_ForcedMinHeight;

		// Token: 0x0400081E RID: 2078
		private readonly float m_ForcedMaxHeight;
	}
}
