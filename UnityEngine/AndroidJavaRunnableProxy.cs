﻿using System;

namespace UnityEngine
{
	// Token: 0x020002B0 RID: 688
	internal class AndroidJavaRunnableProxy : AndroidJavaProxy
	{
		// Token: 0x06002BA2 RID: 11170 RVA: 0x00041C9C File Offset: 0x0003FE9C
		public AndroidJavaRunnableProxy(AndroidJavaRunnable runnable) : base("java/lang/Runnable")
		{
			this.mRunnable = runnable;
		}

		// Token: 0x06002BA3 RID: 11171 RVA: 0x00041CB4 File Offset: 0x0003FEB4
		public void run()
		{
			this.mRunnable();
		}

		// Token: 0x04000A2C RID: 2604
		private AndroidJavaRunnable mRunnable;
	}
}
