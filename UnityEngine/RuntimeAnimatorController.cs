﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001D9 RID: 473
	public class RuntimeAnimatorController : Object
	{
		// Token: 0x17000803 RID: 2051
		// (get) Token: 0x0600200A RID: 8202
		public extern AnimationClip[] animationClips { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
