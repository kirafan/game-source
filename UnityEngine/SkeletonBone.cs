﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001D4 RID: 468
	[RequiredByNativeCode]
	public struct SkeletonBone
	{
		// Token: 0x170007F3 RID: 2035
		// (get) Token: 0x06001FE4 RID: 8164 RVA: 0x000242FC File Offset: 0x000224FC
		// (set) Token: 0x06001FE5 RID: 8165 RVA: 0x00024314 File Offset: 0x00022514
		[Obsolete("transformModified is no longer used and has been deprecated.", true)]
		public int transformModified
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x0400050C RID: 1292
		public string name;

		// Token: 0x0400050D RID: 1293
		internal string parentName;

		// Token: 0x0400050E RID: 1294
		public Vector3 position;

		// Token: 0x0400050F RID: 1295
		public Quaternion rotation;

		// Token: 0x04000510 RID: 1296
		public Vector3 scale;
	}
}
