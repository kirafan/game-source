﻿using System;

namespace UnityEngine
{
	// Token: 0x02000055 RID: 85
	public enum LightmapsModeLegacy
	{
		// Token: 0x04000087 RID: 135
		Single,
		// Token: 0x04000088 RID: 136
		Dual,
		// Token: 0x04000089 RID: 137
		Directional
	}
}
