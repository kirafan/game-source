﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000179 RID: 377
	public sealed class WheelJoint2D : AnchoredJoint2D
	{
		// Token: 0x17000665 RID: 1637
		// (get) Token: 0x06001B34 RID: 6964 RVA: 0x00021854 File Offset: 0x0001FA54
		// (set) Token: 0x06001B35 RID: 6965 RVA: 0x00021874 File Offset: 0x0001FA74
		public JointSuspension2D suspension
		{
			get
			{
				JointSuspension2D result;
				this.INTERNAL_get_suspension(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_suspension(ref value);
			}
		}

		// Token: 0x06001B36 RID: 6966
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_suspension(out JointSuspension2D value);

		// Token: 0x06001B37 RID: 6967
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_suspension(ref JointSuspension2D value);

		// Token: 0x17000666 RID: 1638
		// (get) Token: 0x06001B38 RID: 6968
		// (set) Token: 0x06001B39 RID: 6969
		public extern bool useMotor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000667 RID: 1639
		// (get) Token: 0x06001B3A RID: 6970 RVA: 0x00021880 File Offset: 0x0001FA80
		// (set) Token: 0x06001B3B RID: 6971 RVA: 0x000218A0 File Offset: 0x0001FAA0
		public JointMotor2D motor
		{
			get
			{
				JointMotor2D result;
				this.INTERNAL_get_motor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_motor(ref value);
			}
		}

		// Token: 0x06001B3C RID: 6972
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_motor(out JointMotor2D value);

		// Token: 0x06001B3D RID: 6973
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_motor(ref JointMotor2D value);

		// Token: 0x17000668 RID: 1640
		// (get) Token: 0x06001B3E RID: 6974
		public extern float jointTranslation { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000669 RID: 1641
		// (get) Token: 0x06001B3F RID: 6975
		public extern float jointSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001B40 RID: 6976 RVA: 0x000218AC File Offset: 0x0001FAAC
		public float GetMotorTorque(float timeStep)
		{
			return WheelJoint2D.INTERNAL_CALL_GetMotorTorque(this, timeStep);
		}

		// Token: 0x06001B41 RID: 6977
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float INTERNAL_CALL_GetMotorTorque(WheelJoint2D self, float timeStep);
	}
}
