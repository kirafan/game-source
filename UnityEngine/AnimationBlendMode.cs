﻿using System;

namespace UnityEngine
{
	// Token: 0x020001BF RID: 447
	public enum AnimationBlendMode
	{
		// Token: 0x040004C1 RID: 1217
		Blend,
		// Token: 0x040004C2 RID: 1218
		Additive
	}
}
