﻿using System;

namespace UnityEngine
{
	// Token: 0x0200021F RID: 543
	public enum FocusType
	{
		// Token: 0x040007D4 RID: 2004
		[Obsolete("FocusType.Native now behaves the same as FocusType.Passive in all OS cases. (UnityUpgradable) -> Passive", false)]
		Native,
		// Token: 0x040007D5 RID: 2005
		Keyboard,
		// Token: 0x040007D6 RID: 2006
		Passive
	}
}
