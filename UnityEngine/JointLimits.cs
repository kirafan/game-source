﻿using System;

namespace UnityEngine
{
	// Token: 0x0200013C RID: 316
	public struct JointLimits
	{
		// Token: 0x1700050B RID: 1291
		// (get) Token: 0x0600161C RID: 5660 RVA: 0x0001C1FC File Offset: 0x0001A3FC
		// (set) Token: 0x0600161D RID: 5661 RVA: 0x0001C218 File Offset: 0x0001A418
		public float min
		{
			get
			{
				return this.m_Min;
			}
			set
			{
				this.m_Min = value;
			}
		}

		// Token: 0x1700050C RID: 1292
		// (get) Token: 0x0600161E RID: 5662 RVA: 0x0001C224 File Offset: 0x0001A424
		// (set) Token: 0x0600161F RID: 5663 RVA: 0x0001C240 File Offset: 0x0001A440
		public float max
		{
			get
			{
				return this.m_Max;
			}
			set
			{
				this.m_Max = value;
			}
		}

		// Token: 0x1700050D RID: 1293
		// (get) Token: 0x06001620 RID: 5664 RVA: 0x0001C24C File Offset: 0x0001A44C
		// (set) Token: 0x06001621 RID: 5665 RVA: 0x0001C268 File Offset: 0x0001A468
		public float bounciness
		{
			get
			{
				return this.m_Bounciness;
			}
			set
			{
				this.m_Bounciness = value;
			}
		}

		// Token: 0x1700050E RID: 1294
		// (get) Token: 0x06001622 RID: 5666 RVA: 0x0001C274 File Offset: 0x0001A474
		// (set) Token: 0x06001623 RID: 5667 RVA: 0x0001C290 File Offset: 0x0001A490
		public float bounceMinVelocity
		{
			get
			{
				return this.m_BounceMinVelocity;
			}
			set
			{
				this.m_BounceMinVelocity = value;
			}
		}

		// Token: 0x1700050F RID: 1295
		// (get) Token: 0x06001624 RID: 5668 RVA: 0x0001C29C File Offset: 0x0001A49C
		// (set) Token: 0x06001625 RID: 5669 RVA: 0x0001C2B8 File Offset: 0x0001A4B8
		public float contactDistance
		{
			get
			{
				return this.m_ContactDistance;
			}
			set
			{
				this.m_ContactDistance = value;
			}
		}

		// Token: 0x0400037D RID: 893
		private float m_Min;

		// Token: 0x0400037E RID: 894
		private float m_Max;

		// Token: 0x0400037F RID: 895
		private float m_Bounciness;

		// Token: 0x04000380 RID: 896
		private float m_BounceMinVelocity;

		// Token: 0x04000381 RID: 897
		private float m_ContactDistance;

		// Token: 0x04000382 RID: 898
		[Obsolete("minBounce and maxBounce are replaced by a single JointLimits.bounciness for both limit ends.", true)]
		public float minBounce;

		// Token: 0x04000383 RID: 899
		[Obsolete("minBounce and maxBounce are replaced by a single JointLimits.bounciness for both limit ends.", true)]
		public float maxBounce;
	}
}
