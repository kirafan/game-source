﻿using System;

namespace UnityEngine
{
	// Token: 0x020002DC RID: 732
	[Flags]
	internal enum DiagnosticSwitchFlags
	{
		// Token: 0x04000AC0 RID: 2752
		None = 0,
		// Token: 0x04000AC1 RID: 2753
		CanChangeAfterEngineStart = 1
	}
}
