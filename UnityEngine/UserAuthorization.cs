﻿using System;

namespace UnityEngine
{
	// Token: 0x0200000D RID: 13
	public enum UserAuthorization
	{
		// Token: 0x04000018 RID: 24
		WebCam = 1,
		// Token: 0x04000019 RID: 25
		Microphone
	}
}
