﻿using System;

namespace UnityEngine
{
	// Token: 0x020001CC RID: 460
	public enum AnimatorCullingMode
	{
		// Token: 0x040004EF RID: 1263
		AlwaysAnimate,
		// Token: 0x040004F0 RID: 1264
		CullUpdateTransforms,
		// Token: 0x040004F1 RID: 1265
		CullCompletely
	}
}
