﻿using System;

namespace UnityEngine
{
	// Token: 0x02000300 RID: 768
	public enum RenderTextureReadWrite
	{
		// Token: 0x04000BB0 RID: 2992
		Default,
		// Token: 0x04000BB1 RID: 2993
		Linear,
		// Token: 0x04000BB2 RID: 2994
		sRGB
	}
}
