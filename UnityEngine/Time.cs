﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000D3 RID: 211
	public sealed class Time
	{
		// Token: 0x17000326 RID: 806
		// (get) Token: 0x06000EA1 RID: 3745
		public static extern float time { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000327 RID: 807
		// (get) Token: 0x06000EA2 RID: 3746
		public static extern float timeSinceLevelLoad { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000328 RID: 808
		// (get) Token: 0x06000EA3 RID: 3747
		public static extern float deltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000329 RID: 809
		// (get) Token: 0x06000EA4 RID: 3748
		public static extern float fixedTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700032A RID: 810
		// (get) Token: 0x06000EA5 RID: 3749
		public static extern float unscaledTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700032B RID: 811
		// (get) Token: 0x06000EA6 RID: 3750
		public static extern float unscaledDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700032C RID: 812
		// (get) Token: 0x06000EA7 RID: 3751
		// (set) Token: 0x06000EA8 RID: 3752
		public static extern float fixedDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06000EA9 RID: 3753
		// (set) Token: 0x06000EAA RID: 3754
		public static extern float maximumDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700032E RID: 814
		// (get) Token: 0x06000EAB RID: 3755
		public static extern float smoothDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700032F RID: 815
		// (get) Token: 0x06000EAC RID: 3756
		// (set) Token: 0x06000EAD RID: 3757
		public static extern float maximumParticleDeltaTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000330 RID: 816
		// (get) Token: 0x06000EAE RID: 3758
		// (set) Token: 0x06000EAF RID: 3759
		public static extern float timeScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000331 RID: 817
		// (get) Token: 0x06000EB0 RID: 3760
		public static extern int frameCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000332 RID: 818
		// (get) Token: 0x06000EB1 RID: 3761
		public static extern int renderedFrameCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000333 RID: 819
		// (get) Token: 0x06000EB2 RID: 3762
		public static extern float realtimeSinceStartup { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000334 RID: 820
		// (get) Token: 0x06000EB3 RID: 3763
		// (set) Token: 0x06000EB4 RID: 3764
		public static extern int captureFramerate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
