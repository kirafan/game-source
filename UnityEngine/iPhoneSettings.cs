﻿using System;

namespace UnityEngine
{
	// Token: 0x020000EC RID: 236
	public sealed class iPhoneSettings
	{
		// Token: 0x17000373 RID: 883
		// (get) Token: 0x06001051 RID: 4177 RVA: 0x00016A54 File Offset: 0x00014C54
		[Obsolete("verticalOrientation property is deprecated. Please use Screen.orientation == ScreenOrientation.Portrait instead.")]
		public static bool verticalOrientation
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000374 RID: 884
		// (get) Token: 0x06001052 RID: 4178 RVA: 0x00016A6C File Offset: 0x00014C6C
		[Obsolete("screenCanDarken property is deprecated. Please use (Screen.sleepTimeout != SleepTimeout.NeverSleep) instead.")]
		public static bool screenCanDarken
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000375 RID: 885
		// (get) Token: 0x06001053 RID: 4179 RVA: 0x00016A84 File Offset: 0x00014C84
		[Obsolete("locationServiceEnabledByUser property is deprecated. Please use Input.location.isEnabledByUser instead.")]
		public static bool locationServiceEnabledByUser
		{
			get
			{
				return Input.location.isEnabledByUser;
			}
		}

		// Token: 0x06001054 RID: 4180 RVA: 0x00016AA4 File Offset: 0x00014CA4
		[Obsolete("StartLocationServiceUpdates method is deprecated. Please use Input.location.Start instead.")]
		public static void StartLocationServiceUpdates(float desiredAccuracyInMeters, float updateDistanceInMeters)
		{
			Input.location.Start(desiredAccuracyInMeters, updateDistanceInMeters);
		}

		// Token: 0x06001055 RID: 4181 RVA: 0x00016AB4 File Offset: 0x00014CB4
		[Obsolete("StartLocationServiceUpdates method is deprecated. Please use Input.location.Start instead.")]
		public static void StartLocationServiceUpdates(float desiredAccuracyInMeters)
		{
			Input.location.Start(desiredAccuracyInMeters);
		}

		// Token: 0x06001056 RID: 4182 RVA: 0x00016AC4 File Offset: 0x00014CC4
		[Obsolete("StartLocationServiceUpdates method is deprecated. Please use Input.location.Start instead.")]
		public static void StartLocationServiceUpdates()
		{
			Input.location.Start();
		}

		// Token: 0x06001057 RID: 4183 RVA: 0x00016AD4 File Offset: 0x00014CD4
		[Obsolete("StopLocationServiceUpdates method is deprecated. Please use Input.location.Stop instead.")]
		public static void StopLocationServiceUpdates()
		{
			Input.location.Stop();
		}
	}
}
