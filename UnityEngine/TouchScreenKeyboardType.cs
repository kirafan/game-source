﻿using System;

namespace UnityEngine
{
	// Token: 0x02000376 RID: 886
	public enum TouchScreenKeyboardType
	{
		// Token: 0x04000D51 RID: 3409
		Default,
		// Token: 0x04000D52 RID: 3410
		ASCIICapable,
		// Token: 0x04000D53 RID: 3411
		NumbersAndPunctuation,
		// Token: 0x04000D54 RID: 3412
		URL,
		// Token: 0x04000D55 RID: 3413
		NumberPad,
		// Token: 0x04000D56 RID: 3414
		PhonePad,
		// Token: 0x04000D57 RID: 3415
		NamePhonePad,
		// Token: 0x04000D58 RID: 3416
		EmailAddress,
		// Token: 0x04000D59 RID: 3417
		NintendoNetworkAccount
	}
}
