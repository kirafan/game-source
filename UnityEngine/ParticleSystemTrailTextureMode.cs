﻿using System;

namespace UnityEngine
{
	// Token: 0x0200010E RID: 270
	public enum ParticleSystemTrailTextureMode
	{
		// Token: 0x04000301 RID: 769
		Stretch,
		// Token: 0x04000302 RID: 770
		Tile,
		// Token: 0x04000303 RID: 771
		DistributePerSegment,
		// Token: 0x04000304 RID: 772
		RepeatPerSegment
	}
}
