﻿using System;

namespace UnityEngine
{
	// Token: 0x02000136 RID: 310
	public struct SoftJointLimit
	{
		// Token: 0x170004FC RID: 1276
		// (get) Token: 0x060015FE RID: 5630 RVA: 0x0001BFB0 File Offset: 0x0001A1B0
		// (set) Token: 0x060015FF RID: 5631 RVA: 0x0001BFCC File Offset: 0x0001A1CC
		public float limit
		{
			get
			{
				return this.m_Limit;
			}
			set
			{
				this.m_Limit = value;
			}
		}

		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x06001600 RID: 5632 RVA: 0x0001BFD8 File Offset: 0x0001A1D8
		// (set) Token: 0x06001601 RID: 5633 RVA: 0x0001BFF4 File Offset: 0x0001A1F4
		[Obsolete("Spring has been moved to SoftJointLimitSpring class in Unity 5", true)]
		public float spring
		{
			get
			{
				return 0f;
			}
			set
			{
			}
		}

		// Token: 0x170004FE RID: 1278
		// (get) Token: 0x06001602 RID: 5634 RVA: 0x0001BFF8 File Offset: 0x0001A1F8
		// (set) Token: 0x06001603 RID: 5635 RVA: 0x0001C014 File Offset: 0x0001A214
		[Obsolete("Damper has been moved to SoftJointLimitSpring class in Unity 5", true)]
		public float damper
		{
			get
			{
				return 0f;
			}
			set
			{
			}
		}

		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x06001604 RID: 5636 RVA: 0x0001C018 File Offset: 0x0001A218
		// (set) Token: 0x06001605 RID: 5637 RVA: 0x0001C034 File Offset: 0x0001A234
		public float bounciness
		{
			get
			{
				return this.m_Bounciness;
			}
			set
			{
				this.m_Bounciness = value;
			}
		}

		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x06001606 RID: 5638 RVA: 0x0001C040 File Offset: 0x0001A240
		// (set) Token: 0x06001607 RID: 5639 RVA: 0x0001C05C File Offset: 0x0001A25C
		public float contactDistance
		{
			get
			{
				return this.m_ContactDistance;
			}
			set
			{
				this.m_ContactDistance = value;
			}
		}

		// Token: 0x17000501 RID: 1281
		// (get) Token: 0x06001608 RID: 5640 RVA: 0x0001C068 File Offset: 0x0001A268
		// (set) Token: 0x06001609 RID: 5641 RVA: 0x0001C084 File Offset: 0x0001A284
		[Obsolete("Use SoftJointLimit.bounciness instead", true)]
		public float bouncyness
		{
			get
			{
				return this.m_Bounciness;
			}
			set
			{
				this.m_Bounciness = value;
			}
		}

		// Token: 0x0400036B RID: 875
		private float m_Limit;

		// Token: 0x0400036C RID: 876
		private float m_Bounciness;

		// Token: 0x0400036D RID: 877
		private float m_ContactDistance;
	}
}
