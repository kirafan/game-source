﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E1 RID: 481
	internal enum HumanBodyPart
	{
		// Token: 0x0400059F RID: 1439
		BodyStart,
		// Token: 0x040005A0 RID: 1440
		LeftFingerStart = 24,
		// Token: 0x040005A1 RID: 1441
		RightFingerStart = 39,
		// Token: 0x040005A2 RID: 1442
		LastBone = 54
	}
}
