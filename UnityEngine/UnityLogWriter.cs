﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace UnityEngine
{
	// Token: 0x020000DD RID: 221
	internal sealed class UnityLogWriter : TextWriter
	{
		// Token: 0x06000FB7 RID: 4023
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WriteStringToUnityLog(string s);

		// Token: 0x06000FB8 RID: 4024 RVA: 0x00015E48 File Offset: 0x00014048
		public static void Init()
		{
			Console.SetOut(new UnityLogWriter());
		}

		// Token: 0x17000362 RID: 866
		// (get) Token: 0x06000FB9 RID: 4025 RVA: 0x00015E58 File Offset: 0x00014058
		public override Encoding Encoding
		{
			get
			{
				return Encoding.UTF8;
			}
		}

		// Token: 0x06000FBA RID: 4026 RVA: 0x00015E74 File Offset: 0x00014074
		public override void Write(char value)
		{
			UnityLogWriter.WriteStringToUnityLog(value.ToString());
		}

		// Token: 0x06000FBB RID: 4027 RVA: 0x00015E8C File Offset: 0x0001408C
		public override void Write(string s)
		{
			UnityLogWriter.WriteStringToUnityLog(s);
		}
	}
}
