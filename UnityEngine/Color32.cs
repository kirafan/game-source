﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020002D0 RID: 720
	[UsedByNativeCode]
	[IL2CPPStructAlignment(Align = 4)]
	public struct Color32
	{
		// Token: 0x06002C77 RID: 11383 RVA: 0x00045FEC File Offset: 0x000441EC
		public Color32(byte r, byte g, byte b, byte a)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}

		// Token: 0x06002C78 RID: 11384 RVA: 0x0004600C File Offset: 0x0004420C
		public static implicit operator Color32(Color c)
		{
			return new Color32((byte)(Mathf.Clamp01(c.r) * 255f), (byte)(Mathf.Clamp01(c.g) * 255f), (byte)(Mathf.Clamp01(c.b) * 255f), (byte)(Mathf.Clamp01(c.a) * 255f));
		}

		// Token: 0x06002C79 RID: 11385 RVA: 0x00046074 File Offset: 0x00044274
		public static implicit operator Color(Color32 c)
		{
			return new Color((float)c.r / 255f, (float)c.g / 255f, (float)c.b / 255f, (float)c.a / 255f);
		}

		// Token: 0x06002C7A RID: 11386 RVA: 0x000460C8 File Offset: 0x000442C8
		public static Color32 Lerp(Color32 a, Color32 b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Color32((byte)((float)a.r + (float)(b.r - a.r) * t), (byte)((float)a.g + (float)(b.g - a.g) * t), (byte)((float)a.b + (float)(b.b - a.b) * t), (byte)((float)a.a + (float)(b.a - a.a) * t));
		}

		// Token: 0x06002C7B RID: 11387 RVA: 0x0004615C File Offset: 0x0004435C
		public static Color32 LerpUnclamped(Color32 a, Color32 b, float t)
		{
			return new Color32((byte)((float)a.r + (float)(b.r - a.r) * t), (byte)((float)a.g + (float)(b.g - a.g) * t), (byte)((float)a.b + (float)(b.b - a.b) * t), (byte)((float)a.a + (float)(b.a - a.a) * t));
		}

		// Token: 0x06002C7C RID: 11388 RVA: 0x000461E8 File Offset: 0x000443E8
		public override string ToString()
		{
			return UnityString.Format("RGBA({0}, {1}, {2}, {3})", new object[]
			{
				this.r,
				this.g,
				this.b,
				this.a
			});
		}

		// Token: 0x06002C7D RID: 11389 RVA: 0x00046248 File Offset: 0x00044448
		public string ToString(string format)
		{
			return UnityString.Format("RGBA({0}, {1}, {2}, {3})", new object[]
			{
				this.r.ToString(format),
				this.g.ToString(format),
				this.b.ToString(format),
				this.a.ToString(format)
			});
		}

		// Token: 0x04000ABB RID: 2747
		public byte r;

		// Token: 0x04000ABC RID: 2748
		public byte g;

		// Token: 0x04000ABD RID: 2749
		public byte b;

		// Token: 0x04000ABE RID: 2750
		public byte a;
	}
}
