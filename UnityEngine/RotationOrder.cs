﻿using System;

namespace UnityEngine
{
	// Token: 0x020000D4 RID: 212
	internal enum RotationOrder
	{
		// Token: 0x0400020B RID: 523
		OrderXYZ,
		// Token: 0x0400020C RID: 524
		OrderXZY,
		// Token: 0x0400020D RID: 525
		OrderYZX,
		// Token: 0x0400020E RID: 526
		OrderYXZ,
		// Token: 0x0400020F RID: 527
		OrderZXY,
		// Token: 0x04000210 RID: 528
		OrderZYX
	}
}
