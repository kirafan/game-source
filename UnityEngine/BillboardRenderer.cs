﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000020 RID: 32
	public sealed class BillboardRenderer : Renderer
	{
		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000220 RID: 544
		// (set) Token: 0x06000221 RID: 545
		public extern BillboardAsset billboard { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
