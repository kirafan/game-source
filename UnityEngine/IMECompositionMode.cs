﻿using System;

namespace UnityEngine
{
	// Token: 0x0200006D RID: 109
	public enum IMECompositionMode
	{
		// Token: 0x040000B3 RID: 179
		Auto,
		// Token: 0x040000B4 RID: 180
		On,
		// Token: 0x040000B5 RID: 181
		Off
	}
}
