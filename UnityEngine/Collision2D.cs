﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000169 RID: 361
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class Collision2D
	{
		// Token: 0x17000621 RID: 1569
		// (get) Token: 0x06001A99 RID: 6809 RVA: 0x00021320 File Offset: 0x0001F520
		public bool enabled
		{
			get
			{
				return this.m_Enabled;
			}
		}

		// Token: 0x17000622 RID: 1570
		// (get) Token: 0x06001A9A RID: 6810 RVA: 0x0002133C File Offset: 0x0001F53C
		public Rigidbody2D rigidbody
		{
			get
			{
				return this.m_Rigidbody;
			}
		}

		// Token: 0x17000623 RID: 1571
		// (get) Token: 0x06001A9B RID: 6811 RVA: 0x00021358 File Offset: 0x0001F558
		public Collider2D collider
		{
			get
			{
				return this.m_Collider;
			}
		}

		// Token: 0x17000624 RID: 1572
		// (get) Token: 0x06001A9C RID: 6812 RVA: 0x00021374 File Offset: 0x0001F574
		public Transform transform
		{
			get
			{
				return (!(this.rigidbody != null)) ? this.collider.transform : this.rigidbody.transform;
			}
		}

		// Token: 0x17000625 RID: 1573
		// (get) Token: 0x06001A9D RID: 6813 RVA: 0x000213B8 File Offset: 0x0001F5B8
		public GameObject gameObject
		{
			get
			{
				return (!(this.m_Rigidbody != null)) ? this.m_Collider.gameObject : this.m_Rigidbody.gameObject;
			}
		}

		// Token: 0x17000626 RID: 1574
		// (get) Token: 0x06001A9E RID: 6814 RVA: 0x000213FC File Offset: 0x0001F5FC
		public ContactPoint2D[] contacts
		{
			get
			{
				return this.m_Contacts;
			}
		}

		// Token: 0x17000627 RID: 1575
		// (get) Token: 0x06001A9F RID: 6815 RVA: 0x00021418 File Offset: 0x0001F618
		public Vector2 relativeVelocity
		{
			get
			{
				return this.m_RelativeVelocity;
			}
		}

		// Token: 0x040003EF RID: 1007
		internal Rigidbody2D m_Rigidbody;

		// Token: 0x040003F0 RID: 1008
		internal Collider2D m_Collider;

		// Token: 0x040003F1 RID: 1009
		internal ContactPoint2D[] m_Contacts;

		// Token: 0x040003F2 RID: 1010
		internal Vector2 m_RelativeVelocity;

		// Token: 0x040003F3 RID: 1011
		internal bool m_Enabled;
	}
}
