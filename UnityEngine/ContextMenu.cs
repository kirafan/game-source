﻿using System;

namespace UnityEngine
{
	// Token: 0x020002BE RID: 702
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public sealed class ContextMenu : Attribute
	{
		// Token: 0x06002C40 RID: 11328 RVA: 0x00045148 File Offset: 0x00043348
		public ContextMenu(string itemName) : this(itemName, false)
		{
		}

		// Token: 0x06002C41 RID: 11329 RVA: 0x00045154 File Offset: 0x00043354
		public ContextMenu(string itemName, bool isValidateFunction) : this(itemName, isValidateFunction, 1000000)
		{
		}

		// Token: 0x06002C42 RID: 11330 RVA: 0x00045164 File Offset: 0x00043364
		public ContextMenu(string itemName, bool isValidateFunction, int priority)
		{
			this.menuItem = itemName;
			this.validate = isValidateFunction;
			this.priority = priority;
		}

		// Token: 0x04000A40 RID: 2624
		public readonly string menuItem;

		// Token: 0x04000A41 RID: 2625
		public readonly bool validate;

		// Token: 0x04000A42 RID: 2626
		public readonly int priority;
	}
}
