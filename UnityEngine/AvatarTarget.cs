﻿using System;

namespace UnityEngine
{
	// Token: 0x020001C5 RID: 453
	public enum AvatarTarget
	{
		// Token: 0x040004CF RID: 1231
		Root,
		// Token: 0x040004D0 RID: 1232
		Body,
		// Token: 0x040004D1 RID: 1233
		LeftFoot,
		// Token: 0x040004D2 RID: 1234
		RightFoot,
		// Token: 0x040004D3 RID: 1235
		LeftHand,
		// Token: 0x040004D4 RID: 1236
		RightHand
	}
}
