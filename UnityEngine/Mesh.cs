﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x0200008A RID: 138
	public sealed class Mesh : Object
	{
		// Token: 0x060009A6 RID: 2470 RVA: 0x0000E45C File Offset: 0x0000C65C
		public Mesh()
		{
			Mesh.Internal_Create(this);
		}

		// Token: 0x060009A7 RID: 2471
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] Mesh mono);

		// Token: 0x060009A8 RID: 2472
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Clear([UnityEngine.Internal.DefaultValue("true")] bool keepVertexLayout);

		// Token: 0x060009A9 RID: 2473 RVA: 0x0000E46C File Offset: 0x0000C66C
		[ExcludeFromDocs]
		public void Clear()
		{
			bool keepVertexLayout = true;
			this.Clear(keepVertexLayout);
		}

		// Token: 0x1700023D RID: 573
		// (get) Token: 0x060009AA RID: 2474
		public extern bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700023E RID: 574
		// (get) Token: 0x060009AB RID: 2475
		internal extern bool canAccess { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060009AC RID: 2476
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void PrintErrorCantAccessMesh(Mesh.InternalShaderChannel channel);

		// Token: 0x060009AD RID: 2477
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void PrintErrorCantAccessMeshForIndices();

		// Token: 0x060009AE RID: 2478
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void PrintErrorBadSubmeshIndexTriangles();

		// Token: 0x060009AF RID: 2479
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void PrintErrorBadSubmeshIndexIndices();

		// Token: 0x060009B0 RID: 2480
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetArrayForChannelImpl(Mesh.InternalShaderChannel channel, Mesh.InternalVertexChannelType format, int dim, Array values, int arraySize);

		// Token: 0x060009B1 RID: 2481
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Array GetAllocArrayFromChannelImpl(Mesh.InternalShaderChannel channel, Mesh.InternalVertexChannelType format, int dim);

		// Token: 0x060009B2 RID: 2482
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetArrayFromChannelImpl(Mesh.InternalShaderChannel channel, Mesh.InternalVertexChannelType format, int dim, Array values);

		// Token: 0x060009B3 RID: 2483
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool HasChannel(Mesh.InternalShaderChannel channel);

		// Token: 0x060009B4 RID: 2484
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ResizeList(object list, int size);

		// Token: 0x060009B5 RID: 2485
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Array ExtractArrayFromList(object list);

		// Token: 0x060009B6 RID: 2486
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int[] GetTrianglesImpl(int submesh);

		// Token: 0x060009B7 RID: 2487
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetTrianglesNonAllocImpl(object values, int submesh);

		// Token: 0x060009B8 RID: 2488
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int[] GetIndicesImpl(int submesh);

		// Token: 0x060009B9 RID: 2489
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetIndicesNonAllocImpl(object values, int submesh);

		// Token: 0x060009BA RID: 2490
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTrianglesImpl(int submesh, Array triangles, int arraySize, [UnityEngine.Internal.DefaultValue("true")] bool calculateBounds);

		// Token: 0x060009BB RID: 2491 RVA: 0x0000E484 File Offset: 0x0000C684
		[ExcludeFromDocs]
		private void SetTrianglesImpl(int submesh, Array triangles, int arraySize)
		{
			bool calculateBounds = true;
			this.SetTrianglesImpl(submesh, triangles, arraySize, calculateBounds);
		}

		// Token: 0x060009BC RID: 2492
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetIndicesImpl(int submesh, MeshTopology topology, Array indices, int arraySize, [UnityEngine.Internal.DefaultValue("true")] bool calculateBounds);

		// Token: 0x060009BD RID: 2493 RVA: 0x0000E4A0 File Offset: 0x0000C6A0
		[ExcludeFromDocs]
		private void SetIndicesImpl(int submesh, MeshTopology topology, Array indices, int arraySize)
		{
			bool calculateBounds = true;
			this.SetIndicesImpl(submesh, topology, indices, arraySize, calculateBounds);
		}

		// Token: 0x060009BE RID: 2494 RVA: 0x0000E4BC File Offset: 0x0000C6BC
		[ExcludeFromDocs]
		public void SetTriangles(int[] triangles, int submesh)
		{
			bool calculateBounds = true;
			this.SetTriangles(triangles, submesh, calculateBounds);
		}

		// Token: 0x060009BF RID: 2495 RVA: 0x0000E4D8 File Offset: 0x0000C6D8
		public void SetTriangles(int[] triangles, int submesh, [UnityEngine.Internal.DefaultValue("true")] bool calculateBounds)
		{
			if (this.CheckCanAccessSubmeshTriangles(submesh))
			{
				this.SetTrianglesImpl(submesh, triangles, this.SafeLength(triangles), calculateBounds);
			}
		}

		// Token: 0x060009C0 RID: 2496 RVA: 0x0000E4F8 File Offset: 0x0000C6F8
		[ExcludeFromDocs]
		public void SetTriangles(List<int> triangles, int submesh)
		{
			bool calculateBounds = true;
			this.SetTriangles(triangles, submesh, calculateBounds);
		}

		// Token: 0x060009C1 RID: 2497 RVA: 0x0000E514 File Offset: 0x0000C714
		public void SetTriangles(List<int> triangles, int submesh, [UnityEngine.Internal.DefaultValue("true")] bool calculateBounds)
		{
			if (this.CheckCanAccessSubmeshTriangles(submesh))
			{
				this.SetTrianglesImpl(submesh, Mesh.ExtractArrayFromList(triangles), this.SafeLength<int>(triangles), calculateBounds);
			}
		}

		// Token: 0x060009C2 RID: 2498 RVA: 0x0000E538 File Offset: 0x0000C738
		[ExcludeFromDocs]
		public void SetIndices(int[] indices, MeshTopology topology, int submesh)
		{
			bool calculateBounds = true;
			this.SetIndices(indices, topology, submesh, calculateBounds);
		}

		// Token: 0x060009C3 RID: 2499 RVA: 0x0000E554 File Offset: 0x0000C754
		public void SetIndices(int[] indices, MeshTopology topology, int submesh, [UnityEngine.Internal.DefaultValue("true")] bool calculateBounds)
		{
			if (this.CheckCanAccessSubmeshIndices(submesh))
			{
				this.SetIndicesImpl(submesh, topology, indices, this.SafeLength(indices), calculateBounds);
			}
		}

		// Token: 0x1700023F RID: 575
		// (get) Token: 0x060009C4 RID: 2500
		public extern int blendShapeCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060009C5 RID: 2501
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ClearBlendShapes();

		// Token: 0x060009C6 RID: 2502
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetBlendShapeName(int shapeIndex);

		// Token: 0x060009C7 RID: 2503
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetBlendShapeFrameCount(int shapeIndex);

		// Token: 0x060009C8 RID: 2504
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetBlendShapeFrameWeight(int shapeIndex, int frameIndex);

		// Token: 0x060009C9 RID: 2505
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GetBlendShapeFrameVertices(int shapeIndex, int frameIndex, Vector3[] deltaVertices, Vector3[] deltaNormals, Vector3[] deltaTangents);

		// Token: 0x060009CA RID: 2506
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddBlendShapeFrame(string shapeName, float frameWeight, Vector3[] deltaVertices, Vector3[] deltaNormals, Vector3[] deltaTangents);

		// Token: 0x17000240 RID: 576
		// (get) Token: 0x060009CB RID: 2507
		public extern int vertexBufferCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060009CC RID: 2508 RVA: 0x0000E578 File Offset: 0x0000C778
		public IntPtr GetNativeVertexBufferPtr(int bufferIndex)
		{
			IntPtr result;
			Mesh.INTERNAL_CALL_GetNativeVertexBufferPtr(this, bufferIndex, out result);
			return result;
		}

		// Token: 0x060009CD RID: 2509
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetNativeVertexBufferPtr(Mesh self, int bufferIndex, out IntPtr value);

		// Token: 0x060009CE RID: 2510 RVA: 0x0000E598 File Offset: 0x0000C798
		public IntPtr GetNativeIndexBufferPtr()
		{
			IntPtr result;
			Mesh.INTERNAL_CALL_GetNativeIndexBufferPtr(this, out result);
			return result;
		}

		// Token: 0x060009CF RID: 2511
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetNativeIndexBufferPtr(Mesh self, out IntPtr value);

		// Token: 0x17000241 RID: 577
		// (get) Token: 0x060009D0 RID: 2512 RVA: 0x0000E5B8 File Offset: 0x0000C7B8
		// (set) Token: 0x060009D1 RID: 2513 RVA: 0x0000E5D8 File Offset: 0x0000C7D8
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_bounds(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_bounds(ref value);
			}
		}

		// Token: 0x060009D2 RID: 2514
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_bounds(out Bounds value);

		// Token: 0x060009D3 RID: 2515
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_bounds(ref Bounds value);

		// Token: 0x060009D4 RID: 2516
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RecalculateBounds();

		// Token: 0x060009D5 RID: 2517
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RecalculateNormals();

		// Token: 0x060009D6 RID: 2518
		[Obsolete("This method is no longer supported (UnityUpgradable)", true)]
		[EditorBrowsable(EditorBrowsableState.Never)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Optimize();

		// Token: 0x060009D7 RID: 2519
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern MeshTopology GetTopology(int submesh);

		// Token: 0x17000242 RID: 578
		// (get) Token: 0x060009D8 RID: 2520
		public extern int vertexCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000243 RID: 579
		// (get) Token: 0x060009D9 RID: 2521
		// (set) Token: 0x060009DA RID: 2522
		public extern int subMeshCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060009DB RID: 2523
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetIndexStart(int submesh);

		// Token: 0x060009DC RID: 2524
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern uint GetIndexCount(int submesh);

		// Token: 0x060009DD RID: 2525
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CombineMeshes(CombineInstance[] combine, [UnityEngine.Internal.DefaultValue("true")] bool mergeSubMeshes, [UnityEngine.Internal.DefaultValue("true")] bool useMatrices);

		// Token: 0x060009DE RID: 2526 RVA: 0x0000E5E4 File Offset: 0x0000C7E4
		[ExcludeFromDocs]
		public void CombineMeshes(CombineInstance[] combine, bool mergeSubMeshes)
		{
			bool useMatrices = true;
			this.CombineMeshes(combine, mergeSubMeshes, useMatrices);
		}

		// Token: 0x060009DF RID: 2527 RVA: 0x0000E600 File Offset: 0x0000C800
		[ExcludeFromDocs]
		public void CombineMeshes(CombineInstance[] combine)
		{
			bool useMatrices = true;
			bool mergeSubMeshes = true;
			this.CombineMeshes(combine, mergeSubMeshes, useMatrices);
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x060009E0 RID: 2528
		// (set) Token: 0x060009E1 RID: 2529
		public extern BoneWeight[] boneWeights { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060009E2 RID: 2530
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetBoneWeightsNonAllocImpl(object values);

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x060009E3 RID: 2531
		// (set) Token: 0x060009E4 RID: 2532
		public extern Matrix4x4[] bindposes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060009E5 RID: 2533
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetBindposeCount();

		// Token: 0x060009E6 RID: 2534
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetBindposesNonAllocImpl(object values);

		// Token: 0x060009E7 RID: 2535
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void MarkDynamic();

		// Token: 0x060009E8 RID: 2536
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UploadMeshData(bool markNoLogerReadable);

		// Token: 0x060009E9 RID: 2537
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetBlendShapeIndex(string blendShapeName);

		// Token: 0x060009EA RID: 2538 RVA: 0x0000E61C File Offset: 0x0000C81C
		internal Mesh.InternalShaderChannel GetUVChannel(int uvIndex)
		{
			if (uvIndex < 0 || uvIndex > 3)
			{
				throw new ArgumentException("GetUVChannel called for bad uvIndex", "uvIndex");
			}
			return Mesh.InternalShaderChannel.TexCoord0 + uvIndex;
		}

		// Token: 0x060009EB RID: 2539 RVA: 0x0000E654 File Offset: 0x0000C854
		internal static int DefaultDimensionForChannel(Mesh.InternalShaderChannel channel)
		{
			int result;
			if (channel == Mesh.InternalShaderChannel.Vertex || channel == Mesh.InternalShaderChannel.Normal)
			{
				result = 3;
			}
			else if (channel >= Mesh.InternalShaderChannel.TexCoord0 && channel <= Mesh.InternalShaderChannel.TexCoord3)
			{
				result = 2;
			}
			else
			{
				if (channel != Mesh.InternalShaderChannel.Tangent && channel != Mesh.InternalShaderChannel.Color)
				{
					throw new ArgumentException("DefaultDimensionForChannel called for bad channel", "channel");
				}
				result = 4;
			}
			return result;
		}

		// Token: 0x060009EC RID: 2540 RVA: 0x0000E6B4 File Offset: 0x0000C8B4
		private T[] GetAllocArrayFromChannel<T>(Mesh.InternalShaderChannel channel, Mesh.InternalVertexChannelType format, int dim)
		{
			if (this.canAccess)
			{
				if (this.HasChannel(channel))
				{
					return (T[])this.GetAllocArrayFromChannelImpl(channel, format, dim);
				}
			}
			else
			{
				this.PrintErrorCantAccessMesh(channel);
			}
			return new T[0];
		}

		// Token: 0x060009ED RID: 2541 RVA: 0x0000E70C File Offset: 0x0000C90C
		private T[] GetAllocArrayFromChannel<T>(Mesh.InternalShaderChannel channel)
		{
			return this.GetAllocArrayFromChannel<T>(channel, Mesh.InternalVertexChannelType.Float, Mesh.DefaultDimensionForChannel(channel));
		}

		// Token: 0x060009EE RID: 2542 RVA: 0x0000E730 File Offset: 0x0000C930
		private int SafeLength(Array values)
		{
			return (values == null) ? 0 : values.Length;
		}

		// Token: 0x060009EF RID: 2543 RVA: 0x0000E758 File Offset: 0x0000C958
		private int SafeLength<T>(List<T> values)
		{
			return (values == null) ? 0 : values.Count;
		}

		// Token: 0x060009F0 RID: 2544 RVA: 0x0000E780 File Offset: 0x0000C980
		private void SetSizedArrayForChannel(Mesh.InternalShaderChannel channel, Mesh.InternalVertexChannelType format, int dim, Array values, int valuesCount)
		{
			if (this.canAccess)
			{
				this.SetArrayForChannelImpl(channel, format, dim, values, valuesCount);
			}
			else
			{
				this.PrintErrorCantAccessMesh(channel);
			}
		}

		// Token: 0x060009F1 RID: 2545 RVA: 0x0000E7A8 File Offset: 0x0000C9A8
		private void SetArrayForChannel<T>(Mesh.InternalShaderChannel channel, Mesh.InternalVertexChannelType format, int dim, T[] values)
		{
			this.SetSizedArrayForChannel(channel, format, dim, values, this.SafeLength(values));
		}

		// Token: 0x060009F2 RID: 2546 RVA: 0x0000E7C0 File Offset: 0x0000C9C0
		private void SetArrayForChannel<T>(Mesh.InternalShaderChannel channel, T[] values)
		{
			this.SetSizedArrayForChannel(channel, Mesh.InternalVertexChannelType.Float, Mesh.DefaultDimensionForChannel(channel), values, this.SafeLength(values));
		}

		// Token: 0x060009F3 RID: 2547 RVA: 0x0000E7DC File Offset: 0x0000C9DC
		private void SetListForChannel<T>(Mesh.InternalShaderChannel channel, Mesh.InternalVertexChannelType format, int dim, List<T> values)
		{
			this.SetSizedArrayForChannel(channel, format, dim, Mesh.ExtractArrayFromList(values), this.SafeLength<T>(values));
		}

		// Token: 0x060009F4 RID: 2548 RVA: 0x0000E7F8 File Offset: 0x0000C9F8
		private void SetListForChannel<T>(Mesh.InternalShaderChannel channel, List<T> values)
		{
			this.SetSizedArrayForChannel(channel, Mesh.InternalVertexChannelType.Float, Mesh.DefaultDimensionForChannel(channel), Mesh.ExtractArrayFromList(values), this.SafeLength<T>(values));
		}

		// Token: 0x060009F5 RID: 2549 RVA: 0x0000E818 File Offset: 0x0000CA18
		private void GetListForChannel<T>(List<T> buffer, int capacity, Mesh.InternalShaderChannel channel, int dim)
		{
			this.GetListForChannel<T>(buffer, capacity, channel, dim, Mesh.InternalVertexChannelType.Float);
		}

		// Token: 0x060009F6 RID: 2550 RVA: 0x0000E828 File Offset: 0x0000CA28
		private void GetListForChannel<T>(List<T> buffer, int capacity, Mesh.InternalShaderChannel channel, int dim, Mesh.InternalVertexChannelType channelType)
		{
			buffer.Clear();
			if (!this.canAccess)
			{
				this.PrintErrorCantAccessMesh(channel);
			}
			else if (this.HasChannel(channel))
			{
				this.PrepareUserBuffer<T>(buffer, capacity);
				this.GetArrayFromChannelImpl(channel, channelType, dim, Mesh.ExtractArrayFromList(buffer));
			}
		}

		// Token: 0x060009F7 RID: 2551 RVA: 0x0000E880 File Offset: 0x0000CA80
		private void PrepareUserBuffer<T>(List<T> buffer, int capacity)
		{
			buffer.Clear();
			if (buffer.Capacity < capacity)
			{
				buffer.Capacity = capacity;
			}
			Mesh.ResizeList(buffer, capacity);
		}

		// Token: 0x17000246 RID: 582
		// (get) Token: 0x060009F8 RID: 2552 RVA: 0x0000E8A4 File Offset: 0x0000CAA4
		// (set) Token: 0x060009F9 RID: 2553 RVA: 0x0000E8C0 File Offset: 0x0000CAC0
		public Vector3[] vertices
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector3>(Mesh.InternalShaderChannel.Vertex);
			}
			set
			{
				this.SetArrayForChannel<Vector3>(Mesh.InternalShaderChannel.Vertex, value);
			}
		}

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x060009FA RID: 2554 RVA: 0x0000E8CC File Offset: 0x0000CACC
		// (set) Token: 0x060009FB RID: 2555 RVA: 0x0000E8E8 File Offset: 0x0000CAE8
		public Vector3[] normals
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector3>(Mesh.InternalShaderChannel.Normal);
			}
			set
			{
				this.SetArrayForChannel<Vector3>(Mesh.InternalShaderChannel.Normal, value);
			}
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x060009FC RID: 2556 RVA: 0x0000E8F4 File Offset: 0x0000CAF4
		// (set) Token: 0x060009FD RID: 2557 RVA: 0x0000E910 File Offset: 0x0000CB10
		public Vector4[] tangents
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector4>(Mesh.InternalShaderChannel.Tangent);
			}
			set
			{
				this.SetArrayForChannel<Vector4>(Mesh.InternalShaderChannel.Tangent, value);
			}
		}

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x060009FE RID: 2558 RVA: 0x0000E91C File Offset: 0x0000CB1C
		// (set) Token: 0x060009FF RID: 2559 RVA: 0x0000E938 File Offset: 0x0000CB38
		public Vector2[] uv
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(Mesh.InternalShaderChannel.TexCoord0);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(Mesh.InternalShaderChannel.TexCoord0, value);
			}
		}

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x06000A00 RID: 2560 RVA: 0x0000E944 File Offset: 0x0000CB44
		// (set) Token: 0x06000A01 RID: 2561 RVA: 0x0000E960 File Offset: 0x0000CB60
		public Vector2[] uv2
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(Mesh.InternalShaderChannel.TexCoord1);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(Mesh.InternalShaderChannel.TexCoord1, value);
			}
		}

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x06000A02 RID: 2562 RVA: 0x0000E96C File Offset: 0x0000CB6C
		// (set) Token: 0x06000A03 RID: 2563 RVA: 0x0000E988 File Offset: 0x0000CB88
		public Vector2[] uv3
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(Mesh.InternalShaderChannel.TexCoord2);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(Mesh.InternalShaderChannel.TexCoord2, value);
			}
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x06000A04 RID: 2564 RVA: 0x0000E994 File Offset: 0x0000CB94
		// (set) Token: 0x06000A05 RID: 2565 RVA: 0x0000E9B0 File Offset: 0x0000CBB0
		public Vector2[] uv4
		{
			get
			{
				return this.GetAllocArrayFromChannel<Vector2>(Mesh.InternalShaderChannel.TexCoord3);
			}
			set
			{
				this.SetArrayForChannel<Vector2>(Mesh.InternalShaderChannel.TexCoord3, value);
			}
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x06000A06 RID: 2566 RVA: 0x0000E9BC File Offset: 0x0000CBBC
		// (set) Token: 0x06000A07 RID: 2567 RVA: 0x0000E9D8 File Offset: 0x0000CBD8
		public Color[] colors
		{
			get
			{
				return this.GetAllocArrayFromChannel<Color>(Mesh.InternalShaderChannel.Color);
			}
			set
			{
				this.SetArrayForChannel<Color>(Mesh.InternalShaderChannel.Color, value);
			}
		}

		// Token: 0x1700024E RID: 590
		// (get) Token: 0x06000A08 RID: 2568 RVA: 0x0000E9E4 File Offset: 0x0000CBE4
		// (set) Token: 0x06000A09 RID: 2569 RVA: 0x0000EA04 File Offset: 0x0000CC04
		public Color32[] colors32
		{
			get
			{
				return this.GetAllocArrayFromChannel<Color32>(Mesh.InternalShaderChannel.Color, Mesh.InternalVertexChannelType.Color, 1);
			}
			set
			{
				this.SetArrayForChannel<Color32>(Mesh.InternalShaderChannel.Color, Mesh.InternalVertexChannelType.Color, 1, value);
			}
		}

		// Token: 0x06000A0A RID: 2570 RVA: 0x0000EA14 File Offset: 0x0000CC14
		public void GetVertices(List<Vector3> vertices)
		{
			if (vertices == null)
			{
				throw new ArgumentNullException("The result vertices list cannot be null.", "vertices");
			}
			this.GetListForChannel<Vector3>(vertices, this.vertexCount, Mesh.InternalShaderChannel.Vertex, Mesh.DefaultDimensionForChannel(Mesh.InternalShaderChannel.Vertex));
		}

		// Token: 0x06000A0B RID: 2571 RVA: 0x0000EA44 File Offset: 0x0000CC44
		public void SetVertices(List<Vector3> inVertices)
		{
			this.SetListForChannel<Vector3>(Mesh.InternalShaderChannel.Vertex, inVertices);
		}

		// Token: 0x06000A0C RID: 2572 RVA: 0x0000EA50 File Offset: 0x0000CC50
		public void GetNormals(List<Vector3> normals)
		{
			if (normals == null)
			{
				throw new ArgumentNullException("The result normals list cannot be null.", "normals");
			}
			this.GetListForChannel<Vector3>(normals, this.vertexCount, Mesh.InternalShaderChannel.Normal, Mesh.DefaultDimensionForChannel(Mesh.InternalShaderChannel.Normal));
		}

		// Token: 0x06000A0D RID: 2573 RVA: 0x0000EA80 File Offset: 0x0000CC80
		public void SetNormals(List<Vector3> inNormals)
		{
			this.SetListForChannel<Vector3>(Mesh.InternalShaderChannel.Normal, inNormals);
		}

		// Token: 0x06000A0E RID: 2574 RVA: 0x0000EA8C File Offset: 0x0000CC8C
		public void GetTangents(List<Vector4> tangents)
		{
			if (tangents == null)
			{
				throw new ArgumentNullException("The result tangents list cannot be null.", "tangents");
			}
			this.GetListForChannel<Vector4>(tangents, this.vertexCount, Mesh.InternalShaderChannel.Tangent, Mesh.DefaultDimensionForChannel(Mesh.InternalShaderChannel.Tangent));
		}

		// Token: 0x06000A0F RID: 2575 RVA: 0x0000EABC File Offset: 0x0000CCBC
		public void SetTangents(List<Vector4> inTangents)
		{
			this.SetListForChannel<Vector4>(Mesh.InternalShaderChannel.Tangent, inTangents);
		}

		// Token: 0x06000A10 RID: 2576 RVA: 0x0000EAC8 File Offset: 0x0000CCC8
		public void GetColors(List<Color> colors)
		{
			if (colors == null)
			{
				throw new ArgumentNullException("The result colors list cannot be null.", "colors");
			}
			this.GetListForChannel<Color>(colors, this.vertexCount, Mesh.InternalShaderChannel.Color, Mesh.DefaultDimensionForChannel(Mesh.InternalShaderChannel.Color));
		}

		// Token: 0x06000A11 RID: 2577 RVA: 0x0000EAF8 File Offset: 0x0000CCF8
		public void SetColors(List<Color> inColors)
		{
			this.SetListForChannel<Color>(Mesh.InternalShaderChannel.Color, inColors);
		}

		// Token: 0x06000A12 RID: 2578 RVA: 0x0000EB04 File Offset: 0x0000CD04
		public void GetColors(List<Color32> colors)
		{
			if (colors == null)
			{
				throw new ArgumentNullException("The result colors list cannot be null.", "colors");
			}
			this.GetListForChannel<Color32>(colors, this.vertexCount, Mesh.InternalShaderChannel.Color, 1, Mesh.InternalVertexChannelType.Color);
		}

		// Token: 0x06000A13 RID: 2579 RVA: 0x0000EB30 File Offset: 0x0000CD30
		public void SetColors(List<Color32> inColors)
		{
			this.SetListForChannel<Color32>(Mesh.InternalShaderChannel.Color, Mesh.InternalVertexChannelType.Color, 1, inColors);
		}

		// Token: 0x06000A14 RID: 2580 RVA: 0x0000EB40 File Offset: 0x0000CD40
		private void SetUvsImpl<T>(int uvIndex, int dim, List<T> uvs)
		{
			if (uvIndex < 0 || uvIndex > 3)
			{
				Debug.LogError("The uv index is invalid (must be in [0..3]");
			}
			else
			{
				this.SetListForChannel<T>(this.GetUVChannel(uvIndex), Mesh.InternalVertexChannelType.Float, dim, uvs);
			}
		}

		// Token: 0x06000A15 RID: 2581 RVA: 0x0000EB74 File Offset: 0x0000CD74
		public void SetUVs(int channel, List<Vector2> uvs)
		{
			this.SetUvsImpl<Vector2>(channel, 2, uvs);
		}

		// Token: 0x06000A16 RID: 2582 RVA: 0x0000EB80 File Offset: 0x0000CD80
		public void SetUVs(int channel, List<Vector3> uvs)
		{
			this.SetUvsImpl<Vector3>(channel, 3, uvs);
		}

		// Token: 0x06000A17 RID: 2583 RVA: 0x0000EB8C File Offset: 0x0000CD8C
		public void SetUVs(int channel, List<Vector4> uvs)
		{
			this.SetUvsImpl<Vector4>(channel, 4, uvs);
		}

		// Token: 0x06000A18 RID: 2584 RVA: 0x0000EB98 File Offset: 0x0000CD98
		private void GetUVsImpl<T>(int uvIndex, List<T> uvs, int dim)
		{
			if (uvs == null)
			{
				throw new ArgumentNullException("The result uvs list cannot be null.", "uvs");
			}
			if (uvIndex < 0 || uvIndex > 3)
			{
				throw new IndexOutOfRangeException("Specified uv index is out of range. Must be in the range [0, 3].");
			}
			this.GetListForChannel<T>(uvs, this.vertexCount, this.GetUVChannel(uvIndex), dim);
		}

		// Token: 0x06000A19 RID: 2585 RVA: 0x0000EBEC File Offset: 0x0000CDEC
		public void GetUVs(int channel, List<Vector2> uvs)
		{
			this.GetUVsImpl<Vector2>(channel, uvs, 2);
		}

		// Token: 0x06000A1A RID: 2586 RVA: 0x0000EBF8 File Offset: 0x0000CDF8
		public void GetUVs(int channel, List<Vector3> uvs)
		{
			this.GetUVsImpl<Vector3>(channel, uvs, 3);
		}

		// Token: 0x06000A1B RID: 2587 RVA: 0x0000EC04 File Offset: 0x0000CE04
		public void GetUVs(int channel, List<Vector4> uvs)
		{
			this.GetUVsImpl<Vector4>(channel, uvs, 4);
		}

		// Token: 0x06000A1C RID: 2588 RVA: 0x0000EC10 File Offset: 0x0000CE10
		private bool CheckCanAccessSubmesh(int submesh, bool errorAboutTriangles)
		{
			bool result;
			if (!this.canAccess)
			{
				this.PrintErrorCantAccessMeshForIndices();
				result = false;
			}
			else if (submesh < 0 || submesh >= this.subMeshCount)
			{
				if (errorAboutTriangles)
				{
					this.PrintErrorBadSubmeshIndexTriangles();
				}
				else
				{
					this.PrintErrorBadSubmeshIndexIndices();
				}
				result = false;
			}
			else
			{
				result = true;
			}
			return result;
		}

		// Token: 0x06000A1D RID: 2589 RVA: 0x0000EC74 File Offset: 0x0000CE74
		private bool CheckCanAccessSubmeshTriangles(int submesh)
		{
			return this.CheckCanAccessSubmesh(submesh, true);
		}

		// Token: 0x06000A1E RID: 2590 RVA: 0x0000EC94 File Offset: 0x0000CE94
		private bool CheckCanAccessSubmeshIndices(int submesh)
		{
			return this.CheckCanAccessSubmesh(submesh, false);
		}

		// Token: 0x1700024F RID: 591
		// (get) Token: 0x06000A1F RID: 2591 RVA: 0x0000ECB4 File Offset: 0x0000CEB4
		// (set) Token: 0x06000A20 RID: 2592 RVA: 0x0000ECF0 File Offset: 0x0000CEF0
		public int[] triangles
		{
			get
			{
				int[] result;
				if (this.canAccess)
				{
					result = this.GetTrianglesImpl(-1);
				}
				else
				{
					this.PrintErrorCantAccessMeshForIndices();
					result = new int[0];
				}
				return result;
			}
			set
			{
				if (this.canAccess)
				{
					this.SetTrianglesImpl(-1, value, this.SafeLength(value));
				}
				else
				{
					this.PrintErrorCantAccessMeshForIndices();
				}
			}
		}

		// Token: 0x06000A21 RID: 2593 RVA: 0x0000ED18 File Offset: 0x0000CF18
		public int[] GetTriangles(int submesh)
		{
			return (!this.CheckCanAccessSubmeshTriangles(submesh)) ? new int[0] : this.GetTrianglesImpl(submesh);
		}

		// Token: 0x06000A22 RID: 2594 RVA: 0x0000ED4C File Offset: 0x0000CF4C
		public void GetTriangles(List<int> triangles, int submesh)
		{
			if (triangles == null)
			{
				throw new ArgumentNullException("The result triangles list cannot be null.", "triangles");
			}
			if (submesh < 0 || submesh >= this.subMeshCount)
			{
				throw new IndexOutOfRangeException("Specified sub mesh is out of range. Must be greater or equal to 0 and less than subMeshCount.");
			}
			this.PrepareUserBuffer<int>(triangles, (int)this.GetIndexCount(submesh));
			this.GetTrianglesNonAllocImpl(triangles, submesh);
		}

		// Token: 0x06000A23 RID: 2595 RVA: 0x0000EDA4 File Offset: 0x0000CFA4
		public int[] GetIndices(int submesh)
		{
			return (!this.CheckCanAccessSubmeshIndices(submesh)) ? new int[0] : this.GetIndicesImpl(submesh);
		}

		// Token: 0x06000A24 RID: 2596 RVA: 0x0000EDD8 File Offset: 0x0000CFD8
		public void GetIndices(List<int> indices, int submesh)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("The result indices list cannot be null.", "indices");
			}
			if (submesh < 0 || submesh >= this.subMeshCount)
			{
				throw new IndexOutOfRangeException("Specified sub mesh is out of range. Must be greater or equal to 0 and less than subMeshCount.");
			}
			this.PrepareUserBuffer<int>(indices, (int)this.GetIndexCount(submesh));
			indices.Clear();
			this.GetIndicesNonAllocImpl(indices, submesh);
		}

		// Token: 0x06000A25 RID: 2597 RVA: 0x0000EE38 File Offset: 0x0000D038
		public void GetBindposes(List<Matrix4x4> bindposes)
		{
			if (bindposes == null)
			{
				throw new ArgumentNullException("The result bindposes list cannot be null.", "bindposes");
			}
			this.PrepareUserBuffer<Matrix4x4>(bindposes, this.GetBindposeCount());
			this.GetBindposesNonAllocImpl(bindposes);
		}

		// Token: 0x06000A26 RID: 2598 RVA: 0x0000EE68 File Offset: 0x0000D068
		public void GetBoneWeights(List<BoneWeight> boneWeights)
		{
			if (boneWeights == null)
			{
				throw new ArgumentNullException("The result boneWeights list cannot be null.", "boneWeights");
			}
			this.PrepareUserBuffer<BoneWeight>(boneWeights, this.vertexCount);
			this.GetBoneWeightsNonAllocImpl(boneWeights);
		}

		// Token: 0x0200008B RID: 139
		internal enum InternalShaderChannel
		{
			// Token: 0x04000120 RID: 288
			Vertex,
			// Token: 0x04000121 RID: 289
			Normal,
			// Token: 0x04000122 RID: 290
			Color,
			// Token: 0x04000123 RID: 291
			TexCoord0,
			// Token: 0x04000124 RID: 292
			TexCoord1,
			// Token: 0x04000125 RID: 293
			TexCoord2,
			// Token: 0x04000126 RID: 294
			TexCoord3,
			// Token: 0x04000127 RID: 295
			Tangent
		}

		// Token: 0x0200008C RID: 140
		internal enum InternalVertexChannelType
		{
			// Token: 0x04000129 RID: 297
			Float,
			// Token: 0x0400012A RID: 298
			Color = 2
		}
	}
}
