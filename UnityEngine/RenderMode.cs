﻿using System;

namespace UnityEngine
{
	// Token: 0x0200020B RID: 523
	public enum RenderMode
	{
		// Token: 0x0400063E RID: 1598
		ScreenSpaceOverlay,
		// Token: 0x0400063F RID: 1599
		ScreenSpaceCamera,
		// Token: 0x04000640 RID: 1600
		WorldSpace
	}
}
