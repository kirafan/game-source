﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000029 RID: 41
	public sealed class ComputeShader : Object
	{
		// Token: 0x06000330 RID: 816
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int FindKernel(string name);

		// Token: 0x06000331 RID: 817
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasKernel(string name);

		// Token: 0x06000332 RID: 818
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void GetKernelThreadGroupSizes(int kernelIndex, out uint x, out uint y, out uint z);

		// Token: 0x06000333 RID: 819 RVA: 0x00005CE4 File Offset: 0x00003EE4
		public void SetFloat(string name, float val)
		{
			this.SetFloat(Shader.PropertyToID(name), val);
		}

		// Token: 0x06000334 RID: 820
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetFloat(int nameID, float val);

		// Token: 0x06000335 RID: 821 RVA: 0x00005CF4 File Offset: 0x00003EF4
		public void SetInt(string name, int val)
		{
			this.SetInt(Shader.PropertyToID(name), val);
		}

		// Token: 0x06000336 RID: 822
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetInt(int nameID, int val);

		// Token: 0x06000337 RID: 823 RVA: 0x00005D04 File Offset: 0x00003F04
		public void SetBool(string name, bool val)
		{
			this.SetInt(name, (!val) ? 0 : 1);
		}

		// Token: 0x06000338 RID: 824 RVA: 0x00005D1C File Offset: 0x00003F1C
		public void SetBool(int nameID, bool val)
		{
			this.SetInt(nameID, (!val) ? 0 : 1);
		}

		// Token: 0x06000339 RID: 825 RVA: 0x00005D34 File Offset: 0x00003F34
		public void SetVector(string name, Vector4 val)
		{
			this.SetVector(Shader.PropertyToID(name), val);
		}

		// Token: 0x0600033A RID: 826 RVA: 0x00005D44 File Offset: 0x00003F44
		public void SetVector(int nameID, Vector4 val)
		{
			ComputeShader.INTERNAL_CALL_SetVector(this, nameID, ref val);
		}

		// Token: 0x0600033B RID: 827
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetVector(ComputeShader self, int nameID, ref Vector4 val);

		// Token: 0x0600033C RID: 828 RVA: 0x00005D50 File Offset: 0x00003F50
		public void SetFloats(string name, params float[] values)
		{
			this.Internal_SetFloats(Shader.PropertyToID(name), values);
		}

		// Token: 0x0600033D RID: 829 RVA: 0x00005D60 File Offset: 0x00003F60
		public void SetFloats(int nameID, params float[] values)
		{
			this.Internal_SetFloats(nameID, values);
		}

		// Token: 0x0600033E RID: 830
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetFloats(int nameID, float[] values);

		// Token: 0x0600033F RID: 831 RVA: 0x00005D6C File Offset: 0x00003F6C
		public void SetInts(string name, params int[] values)
		{
			this.Internal_SetInts(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000340 RID: 832 RVA: 0x00005D7C File Offset: 0x00003F7C
		public void SetInts(int nameID, params int[] values)
		{
			this.Internal_SetInts(nameID, values);
		}

		// Token: 0x06000341 RID: 833
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetInts(int nameID, int[] values);

		// Token: 0x06000342 RID: 834 RVA: 0x00005D88 File Offset: 0x00003F88
		public void SetTexture(int kernelIndex, string name, Texture texture)
		{
			this.SetTexture(kernelIndex, Shader.PropertyToID(name), texture);
		}

		// Token: 0x06000343 RID: 835
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTexture(int kernelIndex, int nameID, Texture texture);

		// Token: 0x06000344 RID: 836 RVA: 0x00005D9C File Offset: 0x00003F9C
		public void SetTextureFromGlobal(int kernelIndex, string name, string globalTextureName)
		{
			this.SetTextureFromGlobal(kernelIndex, Shader.PropertyToID(name), Shader.PropertyToID(globalTextureName));
		}

		// Token: 0x06000345 RID: 837
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTextureFromGlobal(int kernelIndex, int nameID, int globalTextureNameID);

		// Token: 0x06000346 RID: 838 RVA: 0x00005DB4 File Offset: 0x00003FB4
		public void SetBuffer(int kernelIndex, string name, ComputeBuffer buffer)
		{
			this.SetBuffer(kernelIndex, Shader.PropertyToID(name), buffer);
		}

		// Token: 0x06000347 RID: 839
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetBuffer(int kernelIndex, int nameID, ComputeBuffer buffer);

		// Token: 0x06000348 RID: 840
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Dispatch(int kernelIndex, int threadGroupsX, int threadGroupsY, int threadGroupsZ);

		// Token: 0x06000349 RID: 841 RVA: 0x00005DC8 File Offset: 0x00003FC8
		[ExcludeFromDocs]
		public void DispatchIndirect(int kernelIndex, ComputeBuffer argsBuffer)
		{
			uint argsOffset = 0U;
			this.DispatchIndirect(kernelIndex, argsBuffer, argsOffset);
		}

		// Token: 0x0600034A RID: 842 RVA: 0x00005DE4 File Offset: 0x00003FE4
		public void DispatchIndirect(int kernelIndex, ComputeBuffer argsBuffer, [DefaultValue("0")] uint argsOffset)
		{
			if (argsBuffer == null)
			{
				throw new ArgumentNullException("argsBuffer");
			}
			if (argsBuffer.m_Ptr == IntPtr.Zero)
			{
				throw new ObjectDisposedException("argsBuffer");
			}
			this.Internal_DispatchIndirect(kernelIndex, argsBuffer, argsOffset);
		}

		// Token: 0x0600034B RID: 843
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_DispatchIndirect(int kernelIndex, ComputeBuffer argsBuffer, uint argsOffset);
	}
}
