﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000183 RID: 387
	public sealed class PlatformEffector2D : Effector2D
	{
		// Token: 0x1700068A RID: 1674
		// (get) Token: 0x06001B8D RID: 7053
		// (set) Token: 0x06001B8E RID: 7054
		public extern bool useOneWay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700068B RID: 1675
		// (get) Token: 0x06001B8F RID: 7055
		// (set) Token: 0x06001B90 RID: 7056
		public extern bool useOneWayGrouping { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700068C RID: 1676
		// (get) Token: 0x06001B91 RID: 7057
		// (set) Token: 0x06001B92 RID: 7058
		public extern bool useSideFriction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700068D RID: 1677
		// (get) Token: 0x06001B93 RID: 7059
		// (set) Token: 0x06001B94 RID: 7060
		public extern bool useSideBounce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700068E RID: 1678
		// (get) Token: 0x06001B95 RID: 7061
		// (set) Token: 0x06001B96 RID: 7062
		public extern float surfaceArc { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700068F RID: 1679
		// (get) Token: 0x06001B97 RID: 7063
		// (set) Token: 0x06001B98 RID: 7064
		public extern float sideArc { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000690 RID: 1680
		// (get) Token: 0x06001B99 RID: 7065
		// (set) Token: 0x06001B9A RID: 7066
		public extern float rotationalOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
