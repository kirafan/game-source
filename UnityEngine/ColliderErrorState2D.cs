﻿using System;

namespace UnityEngine
{
	// Token: 0x0200015D RID: 349
	internal enum ColliderErrorState2D
	{
		// Token: 0x040003DA RID: 986
		None,
		// Token: 0x040003DB RID: 987
		NoShapes,
		// Token: 0x040003DC RID: 988
		RemovedShapes
	}
}
