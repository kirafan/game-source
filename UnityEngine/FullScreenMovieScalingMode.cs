﻿using System;

namespace UnityEngine
{
	// Token: 0x02000068 RID: 104
	public enum FullScreenMovieScalingMode
	{
		// Token: 0x040000A1 RID: 161
		None,
		// Token: 0x040000A2 RID: 162
		AspectFit,
		// Token: 0x040000A3 RID: 163
		AspectFill,
		// Token: 0x040000A4 RID: 164
		Fill
	}
}
