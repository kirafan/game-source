﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x0200039F RID: 927
	[AttributeUsage(AttributeTargets.Parameter)]
	internal class NativeParameterAttribute : Attribute
	{
		// Token: 0x17000B20 RID: 2848
		// (get) Token: 0x06002FC6 RID: 12230 RVA: 0x0004E7EC File Offset: 0x0004C9EC
		// (set) Token: 0x06002FC7 RID: 12231 RVA: 0x0004E808 File Offset: 0x0004CA08
		public bool CanBeNull { get; set; }
	}
}
