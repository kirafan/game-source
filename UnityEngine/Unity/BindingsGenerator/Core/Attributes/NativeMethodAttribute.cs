﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x0200039E RID: 926
	[AttributeUsage(AttributeTargets.Method)]
	internal class NativeMethodAttribute : Attribute
	{
		// Token: 0x17000B1D RID: 2845
		// (get) Token: 0x06002FBF RID: 12223 RVA: 0x0004E76C File Offset: 0x0004C96C
		// (set) Token: 0x06002FC0 RID: 12224 RVA: 0x0004E788 File Offset: 0x0004C988
		public string Name { get; set; }

		// Token: 0x17000B1E RID: 2846
		// (get) Token: 0x06002FC1 RID: 12225 RVA: 0x0004E794 File Offset: 0x0004C994
		// (set) Token: 0x06002FC2 RID: 12226 RVA: 0x0004E7B0 File Offset: 0x0004C9B0
		public bool IsThreadSafe { get; set; }

		// Token: 0x17000B1F RID: 2847
		// (get) Token: 0x06002FC3 RID: 12227 RVA: 0x0004E7BC File Offset: 0x0004C9BC
		// (set) Token: 0x06002FC4 RID: 12228 RVA: 0x0004E7D8 File Offset: 0x0004C9D8
		public bool IsFreeFunction { get; set; }
	}
}
