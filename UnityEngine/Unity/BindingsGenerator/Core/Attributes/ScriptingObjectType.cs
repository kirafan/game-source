﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x020003A4 RID: 932
	internal enum ScriptingObjectType
	{
		// Token: 0x04000DB1 RID: 3505
		UnityEngineObject,
		// Token: 0x04000DB2 RID: 3506
		IntPtrObject
	}
}
