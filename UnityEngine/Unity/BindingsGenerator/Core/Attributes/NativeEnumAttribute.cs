﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x0200039B RID: 923
	[AttributeUsage(AttributeTargets.Enum)]
	internal class NativeEnumAttribute : Attribute
	{
		// Token: 0x17000B18 RID: 2840
		// (get) Token: 0x06002FB2 RID: 12210 RVA: 0x0004E68C File Offset: 0x0004C88C
		// (set) Token: 0x06002FB3 RID: 12211 RVA: 0x0004E6A8 File Offset: 0x0004C8A8
		public string Name { get; set; }

		// Token: 0x17000B19 RID: 2841
		// (get) Token: 0x06002FB4 RID: 12212 RVA: 0x0004E6B4 File Offset: 0x0004C8B4
		// (set) Token: 0x06002FB5 RID: 12213 RVA: 0x0004E6D0 File Offset: 0x0004C8D0
		public string Header { get; set; }

		// Token: 0x17000B1A RID: 2842
		// (get) Token: 0x06002FB6 RID: 12214 RVA: 0x0004E6DC File Offset: 0x0004C8DC
		// (set) Token: 0x06002FB7 RID: 12215 RVA: 0x0004E6F8 File Offset: 0x0004C8F8
		public bool GenerateNativeType { get; set; }
	}
}
