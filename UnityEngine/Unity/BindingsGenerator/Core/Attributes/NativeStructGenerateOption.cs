﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x020003A2 RID: 930
	internal enum NativeStructGenerateOption
	{
		// Token: 0x04000DAA RID: 3498
		Default,
		// Token: 0x04000DAB RID: 3499
		UseCustomStruct,
		// Token: 0x04000DAC RID: 3500
		ForceGenerate
	}
}
