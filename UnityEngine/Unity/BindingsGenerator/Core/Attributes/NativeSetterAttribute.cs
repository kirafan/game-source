﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x020003A1 RID: 929
	[AttributeUsage(AttributeTargets.Method)]
	internal class NativeSetterAttribute : Attribute
	{
		// Token: 0x17000B23 RID: 2851
		// (get) Token: 0x06002FCE RID: 12238 RVA: 0x0004E874 File Offset: 0x0004CA74
		// (set) Token: 0x06002FCF RID: 12239 RVA: 0x0004E890 File Offset: 0x0004CA90
		public string Name { get; set; }
	}
}
