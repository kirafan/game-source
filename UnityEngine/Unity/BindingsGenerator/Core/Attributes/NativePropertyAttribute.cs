﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x020003A0 RID: 928
	[AttributeUsage(AttributeTargets.Property)]
	internal class NativePropertyAttribute : Attribute
	{
		// Token: 0x17000B21 RID: 2849
		// (get) Token: 0x06002FC9 RID: 12233 RVA: 0x0004E81C File Offset: 0x0004CA1C
		// (set) Token: 0x06002FCA RID: 12234 RVA: 0x0004E838 File Offset: 0x0004CA38
		public string Name { get; set; }

		// Token: 0x17000B22 RID: 2850
		// (get) Token: 0x06002FCB RID: 12235 RVA: 0x0004E844 File Offset: 0x0004CA44
		// (set) Token: 0x06002FCC RID: 12236 RVA: 0x0004E860 File Offset: 0x0004CA60
		public bool IsThreadSafe { get; set; }
	}
}
