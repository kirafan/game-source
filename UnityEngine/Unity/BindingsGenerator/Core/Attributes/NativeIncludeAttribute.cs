﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x0200039D RID: 925
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = true)]
	internal class NativeIncludeAttribute : Attribute
	{
		// Token: 0x17000B1C RID: 2844
		// (get) Token: 0x06002FBC RID: 12220 RVA: 0x0004E73C File Offset: 0x0004C93C
		// (set) Token: 0x06002FBD RID: 12221 RVA: 0x0004E758 File Offset: 0x0004C958
		public string Header { get; set; }
	}
}
