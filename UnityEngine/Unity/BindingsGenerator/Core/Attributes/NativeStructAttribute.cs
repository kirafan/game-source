﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x020003A3 RID: 931
	[AttributeUsage(AttributeTargets.Struct)]
	internal class NativeStructAttribute : Attribute
	{
		// Token: 0x17000B24 RID: 2852
		// (get) Token: 0x06002FD1 RID: 12241 RVA: 0x0004E8A4 File Offset: 0x0004CAA4
		// (set) Token: 0x06002FD2 RID: 12242 RVA: 0x0004E8C0 File Offset: 0x0004CAC0
		public string Name { get; set; }

		// Token: 0x17000B25 RID: 2853
		// (get) Token: 0x06002FD3 RID: 12243 RVA: 0x0004E8CC File Offset: 0x0004CACC
		// (set) Token: 0x06002FD4 RID: 12244 RVA: 0x0004E8E8 File Offset: 0x0004CAE8
		public string Header { get; set; }

		// Token: 0x17000B26 RID: 2854
		// (get) Token: 0x06002FD5 RID: 12245 RVA: 0x0004E8F4 File Offset: 0x0004CAF4
		// (set) Token: 0x06002FD6 RID: 12246 RVA: 0x0004E910 File Offset: 0x0004CB10
		public NativeStructGenerateOption GenerateMarshallingType { get; set; }
	}
}
