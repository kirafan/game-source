﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x0200039C RID: 924
	[AttributeUsage(AttributeTargets.Method)]
	internal class NativeGetterAttribute : Attribute
	{
		// Token: 0x17000B1B RID: 2843
		// (get) Token: 0x06002FB9 RID: 12217 RVA: 0x0004E70C File Offset: 0x0004C90C
		// (set) Token: 0x06002FBA RID: 12218 RVA: 0x0004E728 File Offset: 0x0004C928
		public string Name { get; set; }
	}
}
