﻿using System;

namespace Unity.BindingsGenerator.Core.Attributes
{
	// Token: 0x020003A5 RID: 933
	[AttributeUsage(AttributeTargets.Class)]
	internal class NativeTypeAttribute : Attribute
	{
		// Token: 0x06002FD7 RID: 12247 RVA: 0x0004E91C File Offset: 0x0004CB1C
		public NativeTypeAttribute()
		{
			this.ObjectType = ScriptingObjectType.UnityEngineObject;
		}

		// Token: 0x17000B27 RID: 2855
		// (get) Token: 0x06002FD8 RID: 12248 RVA: 0x0004E92C File Offset: 0x0004CB2C
		// (set) Token: 0x06002FD9 RID: 12249 RVA: 0x0004E948 File Offset: 0x0004CB48
		public string Name { get; set; }

		// Token: 0x17000B28 RID: 2856
		// (get) Token: 0x06002FDA RID: 12250 RVA: 0x0004E954 File Offset: 0x0004CB54
		// (set) Token: 0x06002FDB RID: 12251 RVA: 0x0004E970 File Offset: 0x0004CB70
		public string Header { get; set; }

		// Token: 0x17000B29 RID: 2857
		// (get) Token: 0x06002FDC RID: 12252 RVA: 0x0004E97C File Offset: 0x0004CB7C
		// (set) Token: 0x06002FDD RID: 12253 RVA: 0x0004E998 File Offset: 0x0004CB98
		public ScriptingObjectType ObjectType { get; set; }
	}
}
