﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000336 RID: 822
	[AttributeUsage(AttributeTargets.Method, Inherited = true)]
	public sealed class PureAttribute : Attribute
	{
	}
}
