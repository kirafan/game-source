﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200033A RID: 826
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class LinqTunnelAttribute : Attribute
	{
	}
}
