﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200032B RID: 811
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public sealed class NotifyPropertyChangedInvocatorAttribute : Attribute
	{
		// Token: 0x06002CC0 RID: 11456 RVA: 0x00046A00 File Offset: 0x00044C00
		public NotifyPropertyChangedInvocatorAttribute()
		{
		}

		// Token: 0x06002CC1 RID: 11457 RVA: 0x00046A0C File Offset: 0x00044C0C
		public NotifyPropertyChangedInvocatorAttribute(string parameterName)
		{
			this.ParameterName = parameterName;
		}

		// Token: 0x17000A86 RID: 2694
		// (get) Token: 0x06002CC2 RID: 11458 RVA: 0x00046A1C File Offset: 0x00044C1C
		// (set) Token: 0x06002CC3 RID: 11459 RVA: 0x00046A38 File Offset: 0x00044C38
		public string ParameterName { get; private set; }
	}
}
