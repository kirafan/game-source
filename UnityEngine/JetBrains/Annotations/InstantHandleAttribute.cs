﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000335 RID: 821
	[AttributeUsage(AttributeTargets.Parameter, Inherited = true)]
	public sealed class InstantHandleAttribute : Attribute
	{
	}
}
