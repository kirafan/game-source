﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000337 RID: 823
	[AttributeUsage(AttributeTargets.Parameter)]
	public class PathReferenceAttribute : Attribute
	{
		// Token: 0x06002CE8 RID: 11496 RVA: 0x00046CA8 File Offset: 0x00044EA8
		public PathReferenceAttribute()
		{
		}

		// Token: 0x06002CE9 RID: 11497 RVA: 0x00046CB4 File Offset: 0x00044EB4
		public PathReferenceAttribute([PathReference] string basePath)
		{
			this.BasePath = basePath;
		}

		// Token: 0x17000A90 RID: 2704
		// (get) Token: 0x06002CEA RID: 11498 RVA: 0x00046CC4 File Offset: 0x00044EC4
		// (set) Token: 0x06002CEB RID: 11499 RVA: 0x00046CE0 File Offset: 0x00044EE0
		[NotNull]
		public string BasePath { get; private set; }
	}
}
