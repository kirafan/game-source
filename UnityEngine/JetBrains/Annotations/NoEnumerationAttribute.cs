﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000338 RID: 824
	[AttributeUsage(AttributeTargets.Parameter)]
	public sealed class NoEnumerationAttribute : Attribute
	{
	}
}
