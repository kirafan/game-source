﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200032C RID: 812
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true, Inherited = true)]
	public sealed class ContractAnnotationAttribute : Attribute
	{
		// Token: 0x06002CC4 RID: 11460 RVA: 0x00046A44 File Offset: 0x00044C44
		public ContractAnnotationAttribute([NotNull] string contract) : this(contract, false)
		{
		}

		// Token: 0x06002CC5 RID: 11461 RVA: 0x00046A50 File Offset: 0x00044C50
		public ContractAnnotationAttribute([NotNull] string contract, bool forceFullStates)
		{
			this.Contract = contract;
			this.ForceFullStates = forceFullStates;
		}

		// Token: 0x17000A87 RID: 2695
		// (get) Token: 0x06002CC6 RID: 11462 RVA: 0x00046A68 File Offset: 0x00044C68
		// (set) Token: 0x06002CC7 RID: 11463 RVA: 0x00046A84 File Offset: 0x00044C84
		public string Contract { get; private set; }

		// Token: 0x17000A88 RID: 2696
		// (get) Token: 0x06002CC8 RID: 11464 RVA: 0x00046A90 File Offset: 0x00044C90
		// (set) Token: 0x06002CC9 RID: 11465 RVA: 0x00046AAC File Offset: 0x00044CAC
		public bool ForceFullStates { get; private set; }
	}
}
