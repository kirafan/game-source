﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000331 RID: 817
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	public sealed class MeansImplicitUseAttribute : Attribute
	{
		// Token: 0x06002CDA RID: 11482 RVA: 0x00046BC8 File Offset: 0x00044DC8
		public MeansImplicitUseAttribute() : this(ImplicitUseKindFlags.Default, ImplicitUseTargetFlags.Default)
		{
		}

		// Token: 0x06002CDB RID: 11483 RVA: 0x00046BD4 File Offset: 0x00044DD4
		public MeansImplicitUseAttribute(ImplicitUseKindFlags useKindFlags) : this(useKindFlags, ImplicitUseTargetFlags.Default)
		{
		}

		// Token: 0x06002CDC RID: 11484 RVA: 0x00046BE0 File Offset: 0x00044DE0
		public MeansImplicitUseAttribute(ImplicitUseTargetFlags targetFlags) : this(ImplicitUseKindFlags.Default, targetFlags)
		{
		}

		// Token: 0x06002CDD RID: 11485 RVA: 0x00046BEC File Offset: 0x00044DEC
		public MeansImplicitUseAttribute(ImplicitUseKindFlags useKindFlags, ImplicitUseTargetFlags targetFlags)
		{
			this.UseKindFlags = useKindFlags;
			this.TargetFlags = targetFlags;
		}

		// Token: 0x17000A8D RID: 2701
		// (get) Token: 0x06002CDE RID: 11486 RVA: 0x00046C04 File Offset: 0x00044E04
		// (set) Token: 0x06002CDF RID: 11487 RVA: 0x00046C20 File Offset: 0x00044E20
		[UsedImplicitly]
		public ImplicitUseKindFlags UseKindFlags { get; private set; }

		// Token: 0x17000A8E RID: 2702
		// (get) Token: 0x06002CE0 RID: 11488 RVA: 0x00046C2C File Offset: 0x00044E2C
		// (set) Token: 0x06002CE1 RID: 11489 RVA: 0x00046C48 File Offset: 0x00044E48
		[UsedImplicitly]
		public ImplicitUseTargetFlags TargetFlags { get; private set; }
	}
}
