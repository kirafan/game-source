﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000332 RID: 818
	[Flags]
	public enum ImplicitUseKindFlags
	{
		// Token: 0x04000CCD RID: 3277
		Default = 7,
		// Token: 0x04000CCE RID: 3278
		Access = 1,
		// Token: 0x04000CCF RID: 3279
		Assign = 2,
		// Token: 0x04000CD0 RID: 3280
		InstantiatedWithFixedConstructorSignature = 4,
		// Token: 0x04000CD1 RID: 3281
		InstantiatedNoFixedConstructorSignature = 8
	}
}
