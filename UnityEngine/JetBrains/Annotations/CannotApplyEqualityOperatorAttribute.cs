﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200032E RID: 814
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface, AllowMultiple = false, Inherited = true)]
	public sealed class CannotApplyEqualityOperatorAttribute : Attribute
	{
	}
}
