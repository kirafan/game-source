﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000339 RID: 825
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public sealed class AssertionMethodAttribute : Attribute
	{
	}
}
