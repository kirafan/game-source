﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000330 RID: 816
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
	public sealed class UsedImplicitlyAttribute : Attribute
	{
		// Token: 0x06002CD2 RID: 11474 RVA: 0x00046B3C File Offset: 0x00044D3C
		public UsedImplicitlyAttribute() : this(ImplicitUseKindFlags.Default, ImplicitUseTargetFlags.Default)
		{
		}

		// Token: 0x06002CD3 RID: 11475 RVA: 0x00046B48 File Offset: 0x00044D48
		public UsedImplicitlyAttribute(ImplicitUseKindFlags useKindFlags) : this(useKindFlags, ImplicitUseTargetFlags.Default)
		{
		}

		// Token: 0x06002CD4 RID: 11476 RVA: 0x00046B54 File Offset: 0x00044D54
		public UsedImplicitlyAttribute(ImplicitUseTargetFlags targetFlags) : this(ImplicitUseKindFlags.Default, targetFlags)
		{
		}

		// Token: 0x06002CD5 RID: 11477 RVA: 0x00046B60 File Offset: 0x00044D60
		public UsedImplicitlyAttribute(ImplicitUseKindFlags useKindFlags, ImplicitUseTargetFlags targetFlags)
		{
			this.UseKindFlags = useKindFlags;
			this.TargetFlags = targetFlags;
		}

		// Token: 0x17000A8B RID: 2699
		// (get) Token: 0x06002CD6 RID: 11478 RVA: 0x00046B78 File Offset: 0x00044D78
		// (set) Token: 0x06002CD7 RID: 11479 RVA: 0x00046B94 File Offset: 0x00044D94
		public ImplicitUseKindFlags UseKindFlags { get; private set; }

		// Token: 0x17000A8C RID: 2700
		// (get) Token: 0x06002CD8 RID: 11480 RVA: 0x00046BA0 File Offset: 0x00044DA0
		// (set) Token: 0x06002CD9 RID: 11481 RVA: 0x00046BBC File Offset: 0x00044DBC
		public ImplicitUseTargetFlags TargetFlags { get; private set; }
	}
}
