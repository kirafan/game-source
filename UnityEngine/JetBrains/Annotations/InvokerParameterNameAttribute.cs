﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200032A RID: 810
	[AttributeUsage(AttributeTargets.Parameter, AllowMultiple = false, Inherited = true)]
	public sealed class InvokerParameterNameAttribute : Attribute
	{
	}
}
