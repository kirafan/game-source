﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000334 RID: 820
	[MeansImplicitUse]
	public sealed class PublicAPIAttribute : Attribute
	{
		// Token: 0x06002CE2 RID: 11490 RVA: 0x00046C54 File Offset: 0x00044E54
		public PublicAPIAttribute()
		{
		}

		// Token: 0x06002CE3 RID: 11491 RVA: 0x00046C60 File Offset: 0x00044E60
		public PublicAPIAttribute([NotNull] string comment)
		{
			this.Comment = comment;
		}

		// Token: 0x17000A8F RID: 2703
		// (get) Token: 0x06002CE4 RID: 11492 RVA: 0x00046C70 File Offset: 0x00044E70
		// (set) Token: 0x06002CE5 RID: 11493 RVA: 0x00046C8C File Offset: 0x00044E8C
		[NotNull]
		public string Comment { get; private set; }
	}
}
