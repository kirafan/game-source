﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200032D RID: 813
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = true)]
	public sealed class LocalizationRequiredAttribute : Attribute
	{
		// Token: 0x06002CCA RID: 11466 RVA: 0x00046AB8 File Offset: 0x00044CB8
		public LocalizationRequiredAttribute() : this(true)
		{
		}

		// Token: 0x06002CCB RID: 11467 RVA: 0x00046AC4 File Offset: 0x00044CC4
		public LocalizationRequiredAttribute(bool required)
		{
			this.Required = required;
		}

		// Token: 0x17000A89 RID: 2697
		// (get) Token: 0x06002CCC RID: 11468 RVA: 0x00046AD4 File Offset: 0x00044CD4
		// (set) Token: 0x06002CCD RID: 11469 RVA: 0x00046AF0 File Offset: 0x00044CF0
		public bool Required { get; private set; }
	}
}
