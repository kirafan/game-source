﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000333 RID: 819
	[Flags]
	public enum ImplicitUseTargetFlags
	{
		// Token: 0x04000CD3 RID: 3283
		Default = 1,
		// Token: 0x04000CD4 RID: 3284
		Itself = 1,
		// Token: 0x04000CD5 RID: 3285
		Members = 2,
		// Token: 0x04000CD6 RID: 3286
		WithMembers = 3
	}
}
