﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x0200032F RID: 815
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	[BaseTypeRequired(typeof(Attribute))]
	public sealed class BaseTypeRequiredAttribute : Attribute
	{
		// Token: 0x06002CCF RID: 11471 RVA: 0x00046B04 File Offset: 0x00044D04
		public BaseTypeRequiredAttribute([NotNull] Type baseType)
		{
			this.BaseType = baseType;
		}

		// Token: 0x17000A8A RID: 2698
		// (get) Token: 0x06002CD0 RID: 11472 RVA: 0x00046B14 File Offset: 0x00044D14
		// (set) Token: 0x06002CD1 RID: 11473 RVA: 0x00046B30 File Offset: 0x00044D30
		[NotNull]
		public Type BaseType { get; private set; }
	}
}
