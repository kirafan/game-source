﻿using System;

namespace JetBrains.Annotations
{
	// Token: 0x02000329 RID: 809
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
	public sealed class StringFormatMethodAttribute : Attribute
	{
		// Token: 0x06002CBC RID: 11452 RVA: 0x000469C0 File Offset: 0x00044BC0
		public StringFormatMethodAttribute(string formatParameterName)
		{
			this.FormatParameterName = formatParameterName;
		}

		// Token: 0x17000A85 RID: 2693
		// (get) Token: 0x06002CBD RID: 11453 RVA: 0x000469D0 File Offset: 0x00044BD0
		// (set) Token: 0x06002CBE RID: 11454 RVA: 0x000469EC File Offset: 0x00044BEC
		public string FormatParameterName { get; private set; }
	}
}
