﻿using System;

namespace UnityEngine
{
	// Token: 0x02000378 RID: 888
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
	public class UnityAPICompatibilityVersionAttribute : Attribute
	{
		// Token: 0x06002E69 RID: 11881 RVA: 0x0004A8D8 File Offset: 0x00048AD8
		public UnityAPICompatibilityVersionAttribute(string version)
		{
			this._version = version;
		}

		// Token: 0x17000AF8 RID: 2808
		// (get) Token: 0x06002E6A RID: 11882 RVA: 0x0004A8E8 File Offset: 0x00048AE8
		public string version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x04000D5B RID: 3419
		private string _version;
	}
}
