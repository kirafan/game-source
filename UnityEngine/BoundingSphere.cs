﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002C RID: 44
	public struct BoundingSphere
	{
		// Token: 0x06000369 RID: 873 RVA: 0x00006224 File Offset: 0x00004424
		public BoundingSphere(Vector3 pos, float rad)
		{
			this.position = pos;
			this.radius = rad;
		}

		// Token: 0x0600036A RID: 874 RVA: 0x00006238 File Offset: 0x00004438
		public BoundingSphere(Vector4 packedSphere)
		{
			this.position = new Vector3(packedSphere.x, packedSphere.y, packedSphere.z);
			this.radius = packedSphere.w;
		}

		// Token: 0x04000040 RID: 64
		public Vector3 position;

		// Token: 0x04000041 RID: 65
		public float radius;
	}
}
