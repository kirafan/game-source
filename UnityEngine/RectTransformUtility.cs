﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200020A RID: 522
	public sealed class RectTransformUtility
	{
		// Token: 0x06002327 RID: 8999 RVA: 0x00028D98 File Offset: 0x00026F98
		private RectTransformUtility()
		{
		}

		// Token: 0x06002328 RID: 9000 RVA: 0x00028DA4 File Offset: 0x00026FA4
		public static bool RectangleContainsScreenPoint(RectTransform rect, Vector2 screenPoint)
		{
			return RectTransformUtility.RectangleContainsScreenPoint(rect, screenPoint, null);
		}

		// Token: 0x06002329 RID: 9001 RVA: 0x00028DC4 File Offset: 0x00026FC4
		public static bool ScreenPointToWorldPointInRectangle(RectTransform rect, Vector2 screenPoint, Camera cam, out Vector3 worldPoint)
		{
			worldPoint = Vector2.zero;
			Ray ray = RectTransformUtility.ScreenPointToRay(cam, screenPoint);
			Plane plane = new Plane(rect.rotation * Vector3.back, rect.position);
			float distance;
			bool result;
			if (!plane.Raycast(ray, out distance))
			{
				result = false;
			}
			else
			{
				worldPoint = ray.GetPoint(distance);
				result = true;
			}
			return result;
		}

		// Token: 0x0600232A RID: 9002 RVA: 0x00028E34 File Offset: 0x00027034
		public static bool ScreenPointToLocalPointInRectangle(RectTransform rect, Vector2 screenPoint, Camera cam, out Vector2 localPoint)
		{
			localPoint = Vector2.zero;
			Vector3 position;
			bool result;
			if (RectTransformUtility.ScreenPointToWorldPointInRectangle(rect, screenPoint, cam, out position))
			{
				localPoint = rect.InverseTransformPoint(position);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x0600232B RID: 9003 RVA: 0x00028E80 File Offset: 0x00027080
		public static Ray ScreenPointToRay(Camera cam, Vector2 screenPos)
		{
			Ray result;
			if (cam != null)
			{
				result = cam.ScreenPointToRay(screenPos);
			}
			else
			{
				Vector3 origin = screenPos;
				origin.z -= 100f;
				result = new Ray(origin, Vector3.forward);
			}
			return result;
		}

		// Token: 0x0600232C RID: 9004 RVA: 0x00028ED8 File Offset: 0x000270D8
		public static Vector2 WorldToScreenPoint(Camera cam, Vector3 worldPoint)
		{
			Vector2 result;
			if (cam == null)
			{
				result = new Vector2(worldPoint.x, worldPoint.y);
			}
			else
			{
				result = cam.WorldToScreenPoint(worldPoint);
			}
			return result;
		}

		// Token: 0x0600232D RID: 9005 RVA: 0x00028F20 File Offset: 0x00027120
		public static Bounds CalculateRelativeRectTransformBounds(Transform root, Transform child)
		{
			RectTransform[] componentsInChildren = child.GetComponentsInChildren<RectTransform>(false);
			Bounds result;
			if (componentsInChildren.Length > 0)
			{
				Vector3 vector = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);
				Vector3 vector2 = new Vector3(float.MinValue, float.MinValue, float.MinValue);
				Matrix4x4 worldToLocalMatrix = root.worldToLocalMatrix;
				int i = 0;
				int num = componentsInChildren.Length;
				while (i < num)
				{
					componentsInChildren[i].GetWorldCorners(RectTransformUtility.s_Corners);
					for (int j = 0; j < 4; j++)
					{
						Vector3 lhs = worldToLocalMatrix.MultiplyPoint3x4(RectTransformUtility.s_Corners[j]);
						vector = Vector3.Min(lhs, vector);
						vector2 = Vector3.Max(lhs, vector2);
					}
					i++;
				}
				Bounds bounds = new Bounds(vector, Vector3.zero);
				bounds.Encapsulate(vector2);
				result = bounds;
			}
			else
			{
				result = new Bounds(Vector3.zero, Vector3.zero);
			}
			return result;
		}

		// Token: 0x0600232E RID: 9006 RVA: 0x0002901C File Offset: 0x0002721C
		public static Bounds CalculateRelativeRectTransformBounds(Transform trans)
		{
			return RectTransformUtility.CalculateRelativeRectTransformBounds(trans, trans);
		}

		// Token: 0x0600232F RID: 9007 RVA: 0x00029038 File Offset: 0x00027238
		public static void FlipLayoutOnAxis(RectTransform rect, int axis, bool keepPositioning, bool recursive)
		{
			if (!(rect == null))
			{
				if (recursive)
				{
					for (int i = 0; i < rect.childCount; i++)
					{
						RectTransform rectTransform = rect.GetChild(i) as RectTransform;
						if (rectTransform != null)
						{
							RectTransformUtility.FlipLayoutOnAxis(rectTransform, axis, false, true);
						}
					}
				}
				Vector2 pivot = rect.pivot;
				pivot[axis] = 1f - pivot[axis];
				rect.pivot = pivot;
				if (!keepPositioning)
				{
					Vector2 anchoredPosition = rect.anchoredPosition;
					anchoredPosition[axis] = -anchoredPosition[axis];
					rect.anchoredPosition = anchoredPosition;
					Vector2 anchorMin = rect.anchorMin;
					Vector2 anchorMax = rect.anchorMax;
					float num = anchorMin[axis];
					anchorMin[axis] = 1f - anchorMax[axis];
					anchorMax[axis] = 1f - num;
					rect.anchorMin = anchorMin;
					rect.anchorMax = anchorMax;
				}
			}
		}

		// Token: 0x06002330 RID: 9008 RVA: 0x00029138 File Offset: 0x00027338
		public static void FlipLayoutAxes(RectTransform rect, bool keepPositioning, bool recursive)
		{
			if (!(rect == null))
			{
				if (recursive)
				{
					for (int i = 0; i < rect.childCount; i++)
					{
						RectTransform rectTransform = rect.GetChild(i) as RectTransform;
						if (rectTransform != null)
						{
							RectTransformUtility.FlipLayoutAxes(rectTransform, false, true);
						}
					}
				}
				rect.pivot = RectTransformUtility.GetTransposed(rect.pivot);
				rect.sizeDelta = RectTransformUtility.GetTransposed(rect.sizeDelta);
				if (!keepPositioning)
				{
					rect.anchoredPosition = RectTransformUtility.GetTransposed(rect.anchoredPosition);
					rect.anchorMin = RectTransformUtility.GetTransposed(rect.anchorMin);
					rect.anchorMax = RectTransformUtility.GetTransposed(rect.anchorMax);
				}
			}
		}

		// Token: 0x06002331 RID: 9009 RVA: 0x000291FC File Offset: 0x000273FC
		private static Vector2 GetTransposed(Vector2 input)
		{
			return new Vector2(input.y, input.x);
		}

		// Token: 0x06002332 RID: 9010 RVA: 0x00029224 File Offset: 0x00027424
		public static bool RectangleContainsScreenPoint(RectTransform rect, Vector2 screenPoint, Camera cam)
		{
			return RectTransformUtility.INTERNAL_CALL_RectangleContainsScreenPoint(rect, ref screenPoint, cam);
		}

		// Token: 0x06002333 RID: 9011
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_RectangleContainsScreenPoint(RectTransform rect, ref Vector2 screenPoint, Camera cam);

		// Token: 0x06002334 RID: 9012 RVA: 0x00029244 File Offset: 0x00027444
		public static Vector2 PixelAdjustPoint(Vector2 point, Transform elementTransform, Canvas canvas)
		{
			Vector2 result;
			RectTransformUtility.INTERNAL_CALL_PixelAdjustPoint(ref point, elementTransform, canvas, out result);
			return result;
		}

		// Token: 0x06002335 RID: 9013
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_PixelAdjustPoint(ref Vector2 point, Transform elementTransform, Canvas canvas, out Vector2 value);

		// Token: 0x06002336 RID: 9014 RVA: 0x00029268 File Offset: 0x00027468
		public static Rect PixelAdjustRect(RectTransform rectTransform, Canvas canvas)
		{
			Rect result;
			RectTransformUtility.INTERNAL_CALL_PixelAdjustRect(rectTransform, canvas, out result);
			return result;
		}

		// Token: 0x06002337 RID: 9015
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_PixelAdjustRect(RectTransform rectTransform, Canvas canvas, out Rect value);

		// Token: 0x0400063C RID: 1596
		private static Vector3[] s_Corners = new Vector3[4];
	}
}
