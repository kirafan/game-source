﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200004E RID: 78
	public sealed class LineRenderer : Renderer
	{
		// Token: 0x060005B1 RID: 1457 RVA: 0x00007C60 File Offset: 0x00005E60
		[Obsolete("SetWidth has been deprecated. Please use the startWidth, endWidth, or widthCurve properties instead.")]
		public void SetWidth(float start, float end)
		{
			this.startWidth = start;
			this.endWidth = end;
		}

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060005B2 RID: 1458
		// (set) Token: 0x060005B3 RID: 1459
		public extern float startWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x060005B4 RID: 1460
		// (set) Token: 0x060005B5 RID: 1461
		public extern float endWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x060005B6 RID: 1462
		// (set) Token: 0x060005B7 RID: 1463
		public extern AnimationCurve widthCurve { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x060005B8 RID: 1464
		// (set) Token: 0x060005B9 RID: 1465
		public extern float widthMultiplier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060005BA RID: 1466 RVA: 0x00007C74 File Offset: 0x00005E74
		[Obsolete("SetColors has been deprecated. Please use the startColor, endColor, or colorGradient properties instead.")]
		public void SetColors(Color start, Color end)
		{
			this.startColor = start;
			this.endColor = end;
		}

		// Token: 0x17000163 RID: 355
		// (get) Token: 0x060005BB RID: 1467 RVA: 0x00007C88 File Offset: 0x00005E88
		// (set) Token: 0x060005BC RID: 1468 RVA: 0x00007CA8 File Offset: 0x00005EA8
		public Color startColor
		{
			get
			{
				Color result;
				this.INTERNAL_get_startColor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_startColor(ref value);
			}
		}

		// Token: 0x060005BD RID: 1469
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_startColor(out Color value);

		// Token: 0x060005BE RID: 1470
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_startColor(ref Color value);

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x060005BF RID: 1471 RVA: 0x00007CB4 File Offset: 0x00005EB4
		// (set) Token: 0x060005C0 RID: 1472 RVA: 0x00007CD4 File Offset: 0x00005ED4
		public Color endColor
		{
			get
			{
				Color result;
				this.INTERNAL_get_endColor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_endColor(ref value);
			}
		}

		// Token: 0x060005C1 RID: 1473
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_endColor(out Color value);

		// Token: 0x060005C2 RID: 1474
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_endColor(ref Color value);

		// Token: 0x17000165 RID: 357
		// (get) Token: 0x060005C3 RID: 1475
		// (set) Token: 0x060005C4 RID: 1476
		public extern Gradient colorGradient { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060005C5 RID: 1477 RVA: 0x00007CE0 File Offset: 0x00005EE0
		[Obsolete("SetVertexCount has been deprecated. Please use the numPositions property instead.")]
		public void SetVertexCount(int count)
		{
			this.numPositions = count;
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x060005C6 RID: 1478
		// (set) Token: 0x060005C7 RID: 1479
		public extern int numPositions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060005C8 RID: 1480 RVA: 0x00007CEC File Offset: 0x00005EEC
		public void SetPosition(int index, Vector3 position)
		{
			LineRenderer.INTERNAL_CALL_SetPosition(this, index, ref position);
		}

		// Token: 0x060005C9 RID: 1481
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetPosition(LineRenderer self, int index, ref Vector3 position);

		// Token: 0x060005CA RID: 1482 RVA: 0x00007CF8 File Offset: 0x00005EF8
		public Vector3 GetPosition(int index)
		{
			Vector3 result;
			LineRenderer.INTERNAL_CALL_GetPosition(this, index, out result);
			return result;
		}

		// Token: 0x060005CB RID: 1483
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPosition(LineRenderer self, int index, out Vector3 value);

		// Token: 0x060005CC RID: 1484
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPositions(Vector3[] positions);

		// Token: 0x060005CD RID: 1485
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetPositions(Vector3[] positions);

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x060005CE RID: 1486
		// (set) Token: 0x060005CF RID: 1487
		public extern bool useWorldSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x060005D0 RID: 1488
		// (set) Token: 0x060005D1 RID: 1489
		public extern int numCornerVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000169 RID: 361
		// (get) Token: 0x060005D2 RID: 1490
		// (set) Token: 0x060005D3 RID: 1491
		public extern int numCapVertices { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x060005D4 RID: 1492
		// (set) Token: 0x060005D5 RID: 1493
		public extern LineTextureMode textureMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
