﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200009C RID: 156
	[RequiredByNativeCode]
	[Obsolete("NetworkView RPC functions are deprecated. Refer to the new Multiplayer Networking system.")]
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = true)]
	public sealed class RPC : Attribute
	{
	}
}
