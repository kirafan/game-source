﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200001E RID: 30
	public class Behaviour : Component
	{
		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060001FC RID: 508
		// (set) Token: 0x060001FD RID: 509
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060001FE RID: 510
		public extern bool isActiveAndEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
