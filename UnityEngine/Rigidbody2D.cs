﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000160 RID: 352
	public sealed class Rigidbody2D : Component
	{
		// Token: 0x170005F2 RID: 1522
		// (get) Token: 0x060019EE RID: 6638 RVA: 0x00020CC8 File Offset: 0x0001EEC8
		// (set) Token: 0x060019EF RID: 6639 RVA: 0x00020CE8 File Offset: 0x0001EEE8
		public Vector2 position
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_position(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_position(ref value);
			}
		}

		// Token: 0x060019F0 RID: 6640
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_position(out Vector2 value);

		// Token: 0x060019F1 RID: 6641
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_position(ref Vector2 value);

		// Token: 0x170005F3 RID: 1523
		// (get) Token: 0x060019F2 RID: 6642
		// (set) Token: 0x060019F3 RID: 6643
		public extern float rotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060019F4 RID: 6644 RVA: 0x00020CF4 File Offset: 0x0001EEF4
		public void MovePosition(Vector2 position)
		{
			Rigidbody2D.INTERNAL_CALL_MovePosition(this, ref position);
		}

		// Token: 0x060019F5 RID: 6645
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_MovePosition(Rigidbody2D self, ref Vector2 position);

		// Token: 0x060019F6 RID: 6646 RVA: 0x00020D00 File Offset: 0x0001EF00
		public void MoveRotation(float angle)
		{
			Rigidbody2D.INTERNAL_CALL_MoveRotation(this, angle);
		}

		// Token: 0x060019F7 RID: 6647
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_MoveRotation(Rigidbody2D self, float angle);

		// Token: 0x170005F4 RID: 1524
		// (get) Token: 0x060019F8 RID: 6648 RVA: 0x00020D0C File Offset: 0x0001EF0C
		// (set) Token: 0x060019F9 RID: 6649 RVA: 0x00020D2C File Offset: 0x0001EF2C
		public Vector2 velocity
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_velocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_velocity(ref value);
			}
		}

		// Token: 0x060019FA RID: 6650
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_velocity(out Vector2 value);

		// Token: 0x060019FB RID: 6651
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_velocity(ref Vector2 value);

		// Token: 0x170005F5 RID: 1525
		// (get) Token: 0x060019FC RID: 6652
		// (set) Token: 0x060019FD RID: 6653
		public extern float angularVelocity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005F6 RID: 1526
		// (get) Token: 0x060019FE RID: 6654
		// (set) Token: 0x060019FF RID: 6655
		public extern bool useAutoMass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005F7 RID: 1527
		// (get) Token: 0x06001A00 RID: 6656
		// (set) Token: 0x06001A01 RID: 6657
		public extern float mass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005F8 RID: 1528
		// (get) Token: 0x06001A02 RID: 6658
		// (set) Token: 0x06001A03 RID: 6659
		public extern PhysicsMaterial2D sharedMaterial { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005F9 RID: 1529
		// (get) Token: 0x06001A04 RID: 6660 RVA: 0x00020D38 File Offset: 0x0001EF38
		// (set) Token: 0x06001A05 RID: 6661 RVA: 0x00020D58 File Offset: 0x0001EF58
		public Vector2 centerOfMass
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_centerOfMass(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_centerOfMass(ref value);
			}
		}

		// Token: 0x06001A06 RID: 6662
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_centerOfMass(out Vector2 value);

		// Token: 0x06001A07 RID: 6663
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_centerOfMass(ref Vector2 value);

		// Token: 0x170005FA RID: 1530
		// (get) Token: 0x06001A08 RID: 6664 RVA: 0x00020D64 File Offset: 0x0001EF64
		public Vector2 worldCenterOfMass
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_worldCenterOfMass(out result);
				return result;
			}
		}

		// Token: 0x06001A09 RID: 6665
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_worldCenterOfMass(out Vector2 value);

		// Token: 0x170005FB RID: 1531
		// (get) Token: 0x06001A0A RID: 6666
		// (set) Token: 0x06001A0B RID: 6667
		public extern float inertia { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005FC RID: 1532
		// (get) Token: 0x06001A0C RID: 6668
		// (set) Token: 0x06001A0D RID: 6669
		public extern float drag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005FD RID: 1533
		// (get) Token: 0x06001A0E RID: 6670
		// (set) Token: 0x06001A0F RID: 6671
		public extern float angularDrag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005FE RID: 1534
		// (get) Token: 0x06001A10 RID: 6672
		// (set) Token: 0x06001A11 RID: 6673
		public extern float gravityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005FF RID: 1535
		// (get) Token: 0x06001A12 RID: 6674
		// (set) Token: 0x06001A13 RID: 6675
		public extern RigidbodyType2D bodyType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001A14 RID: 6676
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetDragBehaviour(bool dragged);

		// Token: 0x17000600 RID: 1536
		// (get) Token: 0x06001A15 RID: 6677
		// (set) Token: 0x06001A16 RID: 6678
		public extern bool useFullKinematicContacts { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000601 RID: 1537
		// (get) Token: 0x06001A17 RID: 6679 RVA: 0x00020D84 File Offset: 0x0001EF84
		// (set) Token: 0x06001A18 RID: 6680 RVA: 0x00020DA4 File Offset: 0x0001EFA4
		public bool isKinematic
		{
			get
			{
				return this.bodyType == RigidbodyType2D.Kinematic;
			}
			set
			{
				this.bodyType = ((!value) ? RigidbodyType2D.Dynamic : RigidbodyType2D.Kinematic);
			}
		}

		// Token: 0x17000602 RID: 1538
		// (get) Token: 0x06001A19 RID: 6681
		// (set) Token: 0x06001A1A RID: 6682
		[Obsolete("The fixedAngle is no longer supported. Use constraints instead.")]
		public extern bool fixedAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000603 RID: 1539
		// (get) Token: 0x06001A1B RID: 6683
		// (set) Token: 0x06001A1C RID: 6684
		public extern bool freezeRotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000604 RID: 1540
		// (get) Token: 0x06001A1D RID: 6685
		// (set) Token: 0x06001A1E RID: 6686
		public extern RigidbodyConstraints2D constraints { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001A1F RID: 6687
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsSleeping();

		// Token: 0x06001A20 RID: 6688
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsAwake();

		// Token: 0x06001A21 RID: 6689
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Sleep();

		// Token: 0x06001A22 RID: 6690
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void WakeUp();

		// Token: 0x17000605 RID: 1541
		// (get) Token: 0x06001A23 RID: 6691
		// (set) Token: 0x06001A24 RID: 6692
		public extern bool simulated { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000606 RID: 1542
		// (get) Token: 0x06001A25 RID: 6693
		// (set) Token: 0x06001A26 RID: 6694
		public extern RigidbodyInterpolation2D interpolation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000607 RID: 1543
		// (get) Token: 0x06001A27 RID: 6695
		// (set) Token: 0x06001A28 RID: 6696
		public extern RigidbodySleepMode2D sleepMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000608 RID: 1544
		// (get) Token: 0x06001A29 RID: 6697
		// (set) Token: 0x06001A2A RID: 6698
		public extern CollisionDetectionMode2D collisionDetectionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001A2B RID: 6699
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouching(Collider2D collider);

		// Token: 0x06001A2C RID: 6700
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsTouchingLayers([DefaultValue("Physics2D.AllLayers")] int layerMask);

		// Token: 0x06001A2D RID: 6701 RVA: 0x00020DBC File Offset: 0x0001EFBC
		[ExcludeFromDocs]
		public bool IsTouchingLayers()
		{
			int layerMask = -1;
			return this.IsTouchingLayers(layerMask);
		}

		// Token: 0x06001A2E RID: 6702 RVA: 0x00020DDC File Offset: 0x0001EFDC
		public bool OverlapPoint(Vector2 point)
		{
			return Rigidbody2D.INTERNAL_CALL_OverlapPoint(this, ref point);
		}

		// Token: 0x06001A2F RID: 6703
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_OverlapPoint(Rigidbody2D self, ref Vector2 point);

		// Token: 0x06001A30 RID: 6704 RVA: 0x00020DFC File Offset: 0x0001EFFC
		public int Cast(Vector2 direction, RaycastHit2D[] results, [DefaultValue("Mathf.Infinity")] float distance)
		{
			return Rigidbody2D.INTERNAL_CALL_Cast(this, ref direction, results, distance);
		}

		// Token: 0x06001A31 RID: 6705 RVA: 0x00020E1C File Offset: 0x0001F01C
		[ExcludeFromDocs]
		public int Cast(Vector2 direction, RaycastHit2D[] results)
		{
			float positiveInfinity = float.PositiveInfinity;
			return Rigidbody2D.INTERNAL_CALL_Cast(this, ref direction, results, positiveInfinity);
		}

		// Token: 0x06001A32 RID: 6706
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_Cast(Rigidbody2D self, ref Vector2 direction, RaycastHit2D[] results, float distance);

		// Token: 0x06001A33 RID: 6707 RVA: 0x00020E44 File Offset: 0x0001F044
		public void AddForce(Vector2 force, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode)
		{
			Rigidbody2D.INTERNAL_CALL_AddForce(this, ref force, mode);
		}

		// Token: 0x06001A34 RID: 6708 RVA: 0x00020E50 File Offset: 0x0001F050
		[ExcludeFromDocs]
		public void AddForce(Vector2 force)
		{
			ForceMode2D mode = ForceMode2D.Force;
			Rigidbody2D.INTERNAL_CALL_AddForce(this, ref force, mode);
		}

		// Token: 0x06001A35 RID: 6709
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddForce(Rigidbody2D self, ref Vector2 force, ForceMode2D mode);

		// Token: 0x06001A36 RID: 6710 RVA: 0x00020E6C File Offset: 0x0001F06C
		public void AddRelativeForce(Vector2 relativeForce, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode)
		{
			Rigidbody2D.INTERNAL_CALL_AddRelativeForce(this, ref relativeForce, mode);
		}

		// Token: 0x06001A37 RID: 6711 RVA: 0x00020E78 File Offset: 0x0001F078
		[ExcludeFromDocs]
		public void AddRelativeForce(Vector2 relativeForce)
		{
			ForceMode2D mode = ForceMode2D.Force;
			Rigidbody2D.INTERNAL_CALL_AddRelativeForce(this, ref relativeForce, mode);
		}

		// Token: 0x06001A38 RID: 6712
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddRelativeForce(Rigidbody2D self, ref Vector2 relativeForce, ForceMode2D mode);

		// Token: 0x06001A39 RID: 6713 RVA: 0x00020E94 File Offset: 0x0001F094
		public void AddForceAtPosition(Vector2 force, Vector2 position, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode)
		{
			Rigidbody2D.INTERNAL_CALL_AddForceAtPosition(this, ref force, ref position, mode);
		}

		// Token: 0x06001A3A RID: 6714 RVA: 0x00020EA4 File Offset: 0x0001F0A4
		[ExcludeFromDocs]
		public void AddForceAtPosition(Vector2 force, Vector2 position)
		{
			ForceMode2D mode = ForceMode2D.Force;
			Rigidbody2D.INTERNAL_CALL_AddForceAtPosition(this, ref force, ref position, mode);
		}

		// Token: 0x06001A3B RID: 6715
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddForceAtPosition(Rigidbody2D self, ref Vector2 force, ref Vector2 position, ForceMode2D mode);

		// Token: 0x06001A3C RID: 6716
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void AddTorque(float torque, [DefaultValue("ForceMode2D.Force")] ForceMode2D mode);

		// Token: 0x06001A3D RID: 6717 RVA: 0x00020EC0 File Offset: 0x0001F0C0
		[ExcludeFromDocs]
		public void AddTorque(float torque)
		{
			ForceMode2D mode = ForceMode2D.Force;
			this.AddTorque(torque, mode);
		}

		// Token: 0x06001A3E RID: 6718 RVA: 0x00020ED8 File Offset: 0x0001F0D8
		public Vector2 GetPoint(Vector2 point)
		{
			Vector2 result;
			Rigidbody2D.Rigidbody2D_CUSTOM_INTERNAL_GetPoint(this, point, out result);
			return result;
		}

		// Token: 0x06001A3F RID: 6719 RVA: 0x00020EF8 File Offset: 0x0001F0F8
		private static void Rigidbody2D_CUSTOM_INTERNAL_GetPoint(Rigidbody2D rigidbody, Vector2 point, out Vector2 value)
		{
			Rigidbody2D.INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint(rigidbody, ref point, out value);
		}

		// Token: 0x06001A40 RID: 6720
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPoint(Rigidbody2D rigidbody, ref Vector2 point, out Vector2 value);

		// Token: 0x06001A41 RID: 6721 RVA: 0x00020F04 File Offset: 0x0001F104
		public Vector2 GetRelativePoint(Vector2 relativePoint)
		{
			Vector2 result;
			Rigidbody2D.Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(this, relativePoint, out result);
			return result;
		}

		// Token: 0x06001A42 RID: 6722 RVA: 0x00020F24 File Offset: 0x0001F124
		private static void Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(Rigidbody2D rigidbody, Vector2 relativePoint, out Vector2 value)
		{
			Rigidbody2D.INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(rigidbody, ref relativePoint, out value);
		}

		// Token: 0x06001A43 RID: 6723
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePoint(Rigidbody2D rigidbody, ref Vector2 relativePoint, out Vector2 value);

		// Token: 0x06001A44 RID: 6724 RVA: 0x00020F30 File Offset: 0x0001F130
		public Vector2 GetVector(Vector2 vector)
		{
			Vector2 result;
			Rigidbody2D.Rigidbody2D_CUSTOM_INTERNAL_GetVector(this, vector, out result);
			return result;
		}

		// Token: 0x06001A45 RID: 6725 RVA: 0x00020F50 File Offset: 0x0001F150
		private static void Rigidbody2D_CUSTOM_INTERNAL_GetVector(Rigidbody2D rigidbody, Vector2 vector, out Vector2 value)
		{
			Rigidbody2D.INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector(rigidbody, ref vector, out value);
		}

		// Token: 0x06001A46 RID: 6726
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetVector(Rigidbody2D rigidbody, ref Vector2 vector, out Vector2 value);

		// Token: 0x06001A47 RID: 6727 RVA: 0x00020F5C File Offset: 0x0001F15C
		public Vector2 GetRelativeVector(Vector2 relativeVector)
		{
			Vector2 result;
			Rigidbody2D.Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(this, relativeVector, out result);
			return result;
		}

		// Token: 0x06001A48 RID: 6728 RVA: 0x00020F7C File Offset: 0x0001F17C
		private static void Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(Rigidbody2D rigidbody, Vector2 relativeVector, out Vector2 value)
		{
			Rigidbody2D.INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(rigidbody, ref relativeVector, out value);
		}

		// Token: 0x06001A49 RID: 6729
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativeVector(Rigidbody2D rigidbody, ref Vector2 relativeVector, out Vector2 value);

		// Token: 0x06001A4A RID: 6730 RVA: 0x00020F88 File Offset: 0x0001F188
		public Vector2 GetPointVelocity(Vector2 point)
		{
			Vector2 result;
			Rigidbody2D.Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(this, point, out result);
			return result;
		}

		// Token: 0x06001A4B RID: 6731 RVA: 0x00020FA8 File Offset: 0x0001F1A8
		private static void Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(Rigidbody2D rigidbody, Vector2 point, out Vector2 value)
		{
			Rigidbody2D.INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(rigidbody, ref point, out value);
		}

		// Token: 0x06001A4C RID: 6732
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetPointVelocity(Rigidbody2D rigidbody, ref Vector2 point, out Vector2 value);

		// Token: 0x06001A4D RID: 6733 RVA: 0x00020FB4 File Offset: 0x0001F1B4
		public Vector2 GetRelativePointVelocity(Vector2 relativePoint)
		{
			Vector2 result;
			Rigidbody2D.Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(this, relativePoint, out result);
			return result;
		}

		// Token: 0x06001A4E RID: 6734 RVA: 0x00020FD4 File Offset: 0x0001F1D4
		private static void Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(Rigidbody2D rigidbody, Vector2 relativePoint, out Vector2 value)
		{
			Rigidbody2D.INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(rigidbody, ref relativePoint, out value);
		}

		// Token: 0x06001A4F RID: 6735
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Rigidbody2D_CUSTOM_INTERNAL_GetRelativePointVelocity(Rigidbody2D rigidbody, ref Vector2 relativePoint, out Vector2 value);
	}
}
