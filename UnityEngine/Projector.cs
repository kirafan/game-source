﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200004A RID: 74
	public sealed class Projector : Behaviour
	{
		// Token: 0x17000149 RID: 329
		// (get) Token: 0x0600057D RID: 1405
		// (set) Token: 0x0600057E RID: 1406
		public extern float nearClipPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014A RID: 330
		// (get) Token: 0x0600057F RID: 1407
		// (set) Token: 0x06000580 RID: 1408
		public extern float farClipPlane { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x06000581 RID: 1409
		// (set) Token: 0x06000582 RID: 1410
		public extern float fieldOfView { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014C RID: 332
		// (get) Token: 0x06000583 RID: 1411
		// (set) Token: 0x06000584 RID: 1412
		public extern float aspectRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x06000585 RID: 1413
		// (set) Token: 0x06000586 RID: 1414
		public extern bool orthographic { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x06000587 RID: 1415
		// (set) Token: 0x06000588 RID: 1416
		public extern float orthographicSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x06000589 RID: 1417
		// (set) Token: 0x0600058A RID: 1418
		public extern int ignoreLayers { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000150 RID: 336
		// (get) Token: 0x0600058B RID: 1419
		// (set) Token: 0x0600058C RID: 1420
		public extern Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
