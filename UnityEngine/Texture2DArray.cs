﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000CF RID: 207
	public sealed class Texture2DArray : Texture
	{
		// Token: 0x06000E30 RID: 3632 RVA: 0x00013748 File Offset: 0x00011948
		public Texture2DArray(int width, int height, int depth, TextureFormat format, bool mipmap)
		{
			Texture2DArray.Internal_Create(this, width, height, depth, format, mipmap, false);
		}

		// Token: 0x06000E31 RID: 3633 RVA: 0x00013760 File Offset: 0x00011960
		public Texture2DArray(int width, int height, int depth, TextureFormat format, bool mipmap, bool linear)
		{
			Texture2DArray.Internal_Create(this, width, height, depth, format, mipmap, linear);
		}

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x06000E32 RID: 3634
		public extern int depth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700030E RID: 782
		// (get) Token: 0x06000E33 RID: 3635
		public extern TextureFormat format { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000E34 RID: 3636
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Apply([DefaultValue("true")] bool updateMipmaps, [DefaultValue("false")] bool makeNoLongerReadable);

		// Token: 0x06000E35 RID: 3637 RVA: 0x00013778 File Offset: 0x00011978
		[ExcludeFromDocs]
		public void Apply(bool updateMipmaps)
		{
			bool makeNoLongerReadable = false;
			this.Apply(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06000E36 RID: 3638 RVA: 0x00013790 File Offset: 0x00011990
		[ExcludeFromDocs]
		public void Apply()
		{
			bool makeNoLongerReadable = false;
			bool updateMipmaps = true;
			this.Apply(updateMipmaps, makeNoLongerReadable);
		}

		// Token: 0x06000E37 RID: 3639
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] Texture2DArray mono, int width, int height, int depth, TextureFormat format, bool mipmap, bool linear);

		// Token: 0x06000E38 RID: 3640
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels(Color[] colors, int arrayElement, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E39 RID: 3641 RVA: 0x000137AC File Offset: 0x000119AC
		[ExcludeFromDocs]
		public void SetPixels(Color[] colors, int arrayElement)
		{
			int miplevel = 0;
			this.SetPixels(colors, arrayElement, miplevel);
		}

		// Token: 0x06000E3A RID: 3642
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPixels32(Color32[] colors, int arrayElement, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E3B RID: 3643 RVA: 0x000137C8 File Offset: 0x000119C8
		[ExcludeFromDocs]
		public void SetPixels32(Color32[] colors, int arrayElement)
		{
			int miplevel = 0;
			this.SetPixels32(colors, arrayElement, miplevel);
		}

		// Token: 0x06000E3C RID: 3644
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color[] GetPixels(int arrayElement, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E3D RID: 3645 RVA: 0x000137E4 File Offset: 0x000119E4
		[ExcludeFromDocs]
		public Color[] GetPixels(int arrayElement)
		{
			int miplevel = 0;
			return this.GetPixels(arrayElement, miplevel);
		}

		// Token: 0x06000E3E RID: 3646
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Color32[] GetPixels32(int arrayElement, [DefaultValue("0")] int miplevel);

		// Token: 0x06000E3F RID: 3647 RVA: 0x00013804 File Offset: 0x00011A04
		[ExcludeFromDocs]
		public Color32[] GetPixels32(int arrayElement)
		{
			int miplevel = 0;
			return this.GetPixels32(arrayElement, miplevel);
		}
	}
}
