﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020002C0 RID: 704
	[UsedByNativeCode]
	public sealed class HideInInspector : Attribute
	{
	}
}
