﻿using System;

namespace UnityEngine
{
	// Token: 0x020001A7 RID: 423
	public enum AudioReverbPreset
	{
		// Token: 0x0400047F RID: 1151
		Off,
		// Token: 0x04000480 RID: 1152
		Generic,
		// Token: 0x04000481 RID: 1153
		PaddedCell,
		// Token: 0x04000482 RID: 1154
		Room,
		// Token: 0x04000483 RID: 1155
		Bathroom,
		// Token: 0x04000484 RID: 1156
		Livingroom,
		// Token: 0x04000485 RID: 1157
		Stoneroom,
		// Token: 0x04000486 RID: 1158
		Auditorium,
		// Token: 0x04000487 RID: 1159
		Concerthall,
		// Token: 0x04000488 RID: 1160
		Cave,
		// Token: 0x04000489 RID: 1161
		Arena,
		// Token: 0x0400048A RID: 1162
		Hangar,
		// Token: 0x0400048B RID: 1163
		CarpetedHallway,
		// Token: 0x0400048C RID: 1164
		Hallway,
		// Token: 0x0400048D RID: 1165
		StoneCorridor,
		// Token: 0x0400048E RID: 1166
		Alley,
		// Token: 0x0400048F RID: 1167
		Forest,
		// Token: 0x04000490 RID: 1168
		City,
		// Token: 0x04000491 RID: 1169
		Mountains,
		// Token: 0x04000492 RID: 1170
		Quarry,
		// Token: 0x04000493 RID: 1171
		Plain,
		// Token: 0x04000494 RID: 1172
		ParkingLot,
		// Token: 0x04000495 RID: 1173
		SewerPipe,
		// Token: 0x04000496 RID: 1174
		Underwater,
		// Token: 0x04000497 RID: 1175
		Drugged,
		// Token: 0x04000498 RID: 1176
		Dizzy,
		// Token: 0x04000499 RID: 1177
		Psychotic,
		// Token: 0x0400049A RID: 1178
		User
	}
}
