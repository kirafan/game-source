﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000046 RID: 70
	public class SkinnedMeshRenderer : Renderer
	{
		// Token: 0x17000121 RID: 289
		// (get) Token: 0x0600051C RID: 1308
		// (set) Token: 0x0600051D RID: 1309
		public extern Transform[] bones { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x0600051E RID: 1310
		// (set) Token: 0x0600051F RID: 1311
		public extern Transform rootBone { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x06000520 RID: 1312
		// (set) Token: 0x06000521 RID: 1313
		public extern SkinQuality quality { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x06000522 RID: 1314
		// (set) Token: 0x06000523 RID: 1315
		public extern Mesh sharedMesh { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x06000524 RID: 1316
		// (set) Token: 0x06000525 RID: 1317
		public extern bool updateWhenOffscreen { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x06000526 RID: 1318
		// (set) Token: 0x06000527 RID: 1319
		public extern bool skinnedMotionVectors { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x06000528 RID: 1320 RVA: 0x00007A7C File Offset: 0x00005C7C
		// (set) Token: 0x06000529 RID: 1321 RVA: 0x00007A9C File Offset: 0x00005C9C
		public Bounds localBounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_localBounds(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localBounds(ref value);
			}
		}

		// Token: 0x0600052A RID: 1322
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localBounds(out Bounds value);

		// Token: 0x0600052B RID: 1323
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localBounds(ref Bounds value);

		// Token: 0x0600052C RID: 1324
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void BakeMesh(Mesh mesh);

		// Token: 0x0600052D RID: 1325
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetBlendShapeWeight(int index);

		// Token: 0x0600052E RID: 1326
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetBlendShapeWeight(int index, float value);
	}
}
