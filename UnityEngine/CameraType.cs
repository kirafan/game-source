﻿using System;

namespace UnityEngine
{
	// Token: 0x020002E6 RID: 742
	[Flags]
	public enum CameraType
	{
		// Token: 0x04000AEC RID: 2796
		Game = 1,
		// Token: 0x04000AED RID: 2797
		SceneView = 2,
		// Token: 0x04000AEE RID: 2798
		Preview = 4,
		// Token: 0x04000AEF RID: 2799
		VR = 8
	}
}
