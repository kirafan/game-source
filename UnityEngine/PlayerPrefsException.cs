﻿using System;

namespace UnityEngine
{
	// Token: 0x020000A2 RID: 162
	public sealed class PlayerPrefsException : Exception
	{
		// Token: 0x06000B2E RID: 2862 RVA: 0x0000FB78 File Offset: 0x0000DD78
		public PlayerPrefsException(string error) : base(error)
		{
		}
	}
}
