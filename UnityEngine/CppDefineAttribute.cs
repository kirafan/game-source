﻿using System;

namespace UnityEngine
{
	// Token: 0x020002D3 RID: 723
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
	internal class CppDefineAttribute : Attribute
	{
		// Token: 0x06002C83 RID: 11395 RVA: 0x00046370 File Offset: 0x00044570
		public CppDefineAttribute(string symbol, string value)
		{
		}
	}
}
