﻿using System;

namespace UnityEngine
{
	// Token: 0x02000051 RID: 81
	internal struct InternalDrawTextureArguments
	{
		// Token: 0x04000072 RID: 114
		public Rect screenRect;

		// Token: 0x04000073 RID: 115
		public Texture texture;

		// Token: 0x04000074 RID: 116
		public Rect sourceRect;

		// Token: 0x04000075 RID: 117
		public int leftBorder;

		// Token: 0x04000076 RID: 118
		public int rightBorder;

		// Token: 0x04000077 RID: 119
		public int topBorder;

		// Token: 0x04000078 RID: 120
		public int bottomBorder;

		// Token: 0x04000079 RID: 121
		public Color32 color;

		// Token: 0x0400007A RID: 122
		public Material mat;

		// Token: 0x0400007B RID: 123
		public int pass;
	}
}
