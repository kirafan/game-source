﻿using System;

namespace UnityEngine
{
	// Token: 0x02000074 RID: 116
	public enum LocationServiceStatus
	{
		// Token: 0x040000DC RID: 220
		Stopped,
		// Token: 0x040000DD RID: 221
		Initializing,
		// Token: 0x040000DE RID: 222
		Running,
		// Token: 0x040000DF RID: 223
		Failed
	}
}
