﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001AD RID: 429
	public sealed class AudioChorusFilter : Behaviour
	{
		// Token: 0x17000754 RID: 1876
		// (get) Token: 0x06001DB1 RID: 7601
		// (set) Token: 0x06001DB2 RID: 7602
		public extern float dryMix { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000755 RID: 1877
		// (get) Token: 0x06001DB3 RID: 7603
		// (set) Token: 0x06001DB4 RID: 7604
		public extern float wetMix1 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000756 RID: 1878
		// (get) Token: 0x06001DB5 RID: 7605
		// (set) Token: 0x06001DB6 RID: 7606
		public extern float wetMix2 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000757 RID: 1879
		// (get) Token: 0x06001DB7 RID: 7607
		// (set) Token: 0x06001DB8 RID: 7608
		public extern float wetMix3 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000758 RID: 1880
		// (get) Token: 0x06001DB9 RID: 7609
		// (set) Token: 0x06001DBA RID: 7610
		public extern float delay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000759 RID: 1881
		// (get) Token: 0x06001DBB RID: 7611
		// (set) Token: 0x06001DBC RID: 7612
		public extern float rate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700075A RID: 1882
		// (get) Token: 0x06001DBD RID: 7613
		// (set) Token: 0x06001DBE RID: 7614
		public extern float depth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700075B RID: 1883
		// (get) Token: 0x06001DBF RID: 7615
		// (set) Token: 0x06001DC0 RID: 7616
		[Obsolete("feedback is deprecated, this property does nothing.")]
		public extern float feedback { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
