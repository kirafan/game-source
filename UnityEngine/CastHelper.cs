﻿using System;

namespace UnityEngine
{
	// Token: 0x020002CD RID: 717
	internal struct CastHelper<T>
	{
		// Token: 0x04000AB5 RID: 2741
		public T t;

		// Token: 0x04000AB6 RID: 2742
		public IntPtr onePointerFurtherThanT;
	}
}
