﻿using System;

namespace UnityEngine
{
	// Token: 0x02000359 RID: 857
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public class TooltipAttribute : PropertyAttribute
	{
		// Token: 0x06002DDA RID: 11738 RVA: 0x00048E38 File Offset: 0x00047038
		public TooltipAttribute(string tooltip)
		{
			this.tooltip = tooltip;
		}

		// Token: 0x04000D28 RID: 3368
		public readonly string tooltip;
	}
}
