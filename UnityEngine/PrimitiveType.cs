﻿using System;

namespace UnityEngine
{
	// Token: 0x020002C5 RID: 709
	public enum PrimitiveType
	{
		// Token: 0x04000A4A RID: 2634
		Sphere,
		// Token: 0x04000A4B RID: 2635
		Capsule,
		// Token: 0x04000A4C RID: 2636
		Cylinder,
		// Token: 0x04000A4D RID: 2637
		Cube,
		// Token: 0x04000A4E RID: 2638
		Plane,
		// Token: 0x04000A4F RID: 2639
		Quad
	}
}
