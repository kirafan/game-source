﻿using System;

namespace UnityEngine
{
	// Token: 0x020001DD RID: 477
	internal enum ArmDoF
	{
		// Token: 0x04000544 RID: 1348
		ShoulderDownUp,
		// Token: 0x04000545 RID: 1349
		ShoulderFrontBack,
		// Token: 0x04000546 RID: 1350
		ArmDownUp,
		// Token: 0x04000547 RID: 1351
		ArmFrontBack,
		// Token: 0x04000548 RID: 1352
		ArmRollInOut,
		// Token: 0x04000549 RID: 1353
		ForeArmCloseOpen,
		// Token: 0x0400054A RID: 1354
		ForeArmRollInOut,
		// Token: 0x0400054B RID: 1355
		HandDownUp,
		// Token: 0x0400054C RID: 1356
		HandInOut,
		// Token: 0x0400054D RID: 1357
		LastArmDoF
	}
}
