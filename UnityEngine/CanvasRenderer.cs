﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000210 RID: 528
	public sealed class CanvasRenderer : Component
	{
		// Token: 0x06002373 RID: 9075 RVA: 0x00029374 File Offset: 0x00027574
		public void SetColor(Color color)
		{
			CanvasRenderer.INTERNAL_CALL_SetColor(this, ref color);
		}

		// Token: 0x06002374 RID: 9076
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetColor(CanvasRenderer self, ref Color color);

		// Token: 0x06002375 RID: 9077 RVA: 0x00029380 File Offset: 0x00027580
		public Color GetColor()
		{
			Color result;
			CanvasRenderer.INTERNAL_CALL_GetColor(this, out result);
			return result;
		}

		// Token: 0x06002376 RID: 9078
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetColor(CanvasRenderer self, out Color value);

		// Token: 0x06002377 RID: 9079
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetAlpha();

		// Token: 0x06002378 RID: 9080
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetAlpha(float alpha);

		// Token: 0x170008DF RID: 2271
		// (get) Token: 0x06002379 RID: 9081
		// (set) Token: 0x0600237A RID: 9082
		[Obsolete("isMask is no longer supported. See EnableClipping for vertex clipping configuration")]
		public extern bool isMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600237B RID: 9083 RVA: 0x000293A0 File Offset: 0x000275A0
		[Obsolete("UI System now uses meshes. Generate a mesh and use 'SetMesh' instead")]
		public void SetVertices(List<UIVertex> vertices)
		{
			this.SetVertices(vertices.ToArray(), vertices.Count);
		}

		// Token: 0x0600237C RID: 9084 RVA: 0x000293B8 File Offset: 0x000275B8
		[Obsolete("UI System now uses meshes. Generate a mesh and use 'SetMesh' instead")]
		public void SetVertices(UIVertex[] vertices, int size)
		{
			Mesh mesh = new Mesh();
			List<Vector3> list = new List<Vector3>();
			List<Color32> list2 = new List<Color32>();
			List<Vector2> list3 = new List<Vector2>();
			List<Vector2> list4 = new List<Vector2>();
			List<Vector3> list5 = new List<Vector3>();
			List<Vector4> list6 = new List<Vector4>();
			List<int> list7 = new List<int>();
			for (int i = 0; i < size; i += 4)
			{
				for (int j = 0; j < 4; j++)
				{
					list.Add(vertices[i + j].position);
					list2.Add(vertices[i + j].color);
					list3.Add(vertices[i + j].uv0);
					list4.Add(vertices[i + j].uv1);
					list5.Add(vertices[i + j].normal);
					list6.Add(vertices[i + j].tangent);
				}
				list7.Add(i);
				list7.Add(i + 1);
				list7.Add(i + 2);
				list7.Add(i + 2);
				list7.Add(i + 3);
				list7.Add(i);
			}
			mesh.SetVertices(list);
			mesh.SetColors(list2);
			mesh.SetNormals(list5);
			mesh.SetTangents(list6);
			mesh.SetUVs(0, list3);
			mesh.SetUVs(1, list4);
			mesh.SetIndices(list7.ToArray(), MeshTopology.Triangles, 0);
			this.SetMesh(mesh);
			Object.DestroyImmediate(mesh);
		}

		// Token: 0x0600237D RID: 9085 RVA: 0x0002953C File Offset: 0x0002773C
		public void EnableRectClipping(Rect rect)
		{
			CanvasRenderer.INTERNAL_CALL_EnableRectClipping(this, ref rect);
		}

		// Token: 0x0600237E RID: 9086
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_EnableRectClipping(CanvasRenderer self, ref Rect rect);

		// Token: 0x0600237F RID: 9087
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void DisableRectClipping();

		// Token: 0x170008E0 RID: 2272
		// (get) Token: 0x06002380 RID: 9088
		public extern bool hasRectClipping { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008E1 RID: 2273
		// (get) Token: 0x06002381 RID: 9089
		// (set) Token: 0x06002382 RID: 9090
		public extern bool hasPopInstruction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008E2 RID: 2274
		// (get) Token: 0x06002383 RID: 9091
		// (set) Token: 0x06002384 RID: 9092
		public extern int materialCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002385 RID: 9093
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetMaterial(Material material, int index);

		// Token: 0x06002386 RID: 9094 RVA: 0x00029548 File Offset: 0x00027748
		public void SetMaterial(Material material, Texture texture)
		{
			this.materialCount = Math.Max(1, this.materialCount);
			this.SetMaterial(material, 0);
			this.SetTexture(texture);
		}

		// Token: 0x06002387 RID: 9095 RVA: 0x0002956C File Offset: 0x0002776C
		public Material GetMaterial()
		{
			return this.GetMaterial(0);
		}

		// Token: 0x06002388 RID: 9096
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Material GetMaterial(int index);

		// Token: 0x170008E3 RID: 2275
		// (get) Token: 0x06002389 RID: 9097
		// (set) Token: 0x0600238A RID: 9098
		public extern int popMaterialCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600238B RID: 9099
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetPopMaterial(Material material, int index);

		// Token: 0x0600238C RID: 9100
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Material GetPopMaterial(int index);

		// Token: 0x0600238D RID: 9101
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTexture(Texture texture);

		// Token: 0x0600238E RID: 9102
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetAlphaTexture(Texture texture);

		// Token: 0x0600238F RID: 9103
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetMesh(Mesh mesh);

		// Token: 0x06002390 RID: 9104
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Clear();

		// Token: 0x06002391 RID: 9105 RVA: 0x00029588 File Offset: 0x00027788
		public static void SplitUIVertexStreams(List<UIVertex> verts, List<Vector3> positions, List<Color32> colors, List<Vector2> uv0S, List<Vector2> uv1S, List<Vector3> normals, List<Vector4> tangents, List<int> indicies)
		{
			CanvasRenderer.SplitUIVertexStreamsInternal(verts, positions, colors, uv0S, uv1S, normals, tangents);
			CanvasRenderer.SplitIndiciesStreamsInternal(verts, indicies);
		}

		// Token: 0x06002392 RID: 9106
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SplitUIVertexStreamsInternal(object verts, object positions, object colors, object uv0S, object uv1S, object normals, object tangents);

		// Token: 0x06002393 RID: 9107
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SplitIndiciesStreamsInternal(object verts, object indicies);

		// Token: 0x06002394 RID: 9108 RVA: 0x000295A4 File Offset: 0x000277A4
		public static void CreateUIVertexStream(List<UIVertex> verts, List<Vector3> positions, List<Color32> colors, List<Vector2> uv0S, List<Vector2> uv1S, List<Vector3> normals, List<Vector4> tangents, List<int> indicies)
		{
			CanvasRenderer.CreateUIVertexStreamInternal(verts, positions, colors, uv0S, uv1S, normals, tangents, indicies);
		}

		// Token: 0x06002395 RID: 9109
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CreateUIVertexStreamInternal(object verts, object positions, object colors, object uv0S, object uv1S, object normals, object tangents, object indicies);

		// Token: 0x06002396 RID: 9110 RVA: 0x000295B8 File Offset: 0x000277B8
		public static void AddUIVertexStream(List<UIVertex> verts, List<Vector3> positions, List<Color32> colors, List<Vector2> uv0S, List<Vector2> uv1S, List<Vector3> normals, List<Vector4> tangents)
		{
			CanvasRenderer.SplitUIVertexStreamsInternal(verts, positions, colors, uv0S, uv1S, normals, tangents);
		}

		// Token: 0x170008E4 RID: 2276
		// (get) Token: 0x06002397 RID: 9111
		public extern int relativeDepth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008E5 RID: 2277
		// (get) Token: 0x06002398 RID: 9112
		// (set) Token: 0x06002399 RID: 9113
		public extern bool cull { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008E6 RID: 2278
		// (get) Token: 0x0600239A RID: 9114
		public extern int absoluteDepth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008E7 RID: 2279
		// (get) Token: 0x0600239B RID: 9115
		public extern bool hasMoved { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
