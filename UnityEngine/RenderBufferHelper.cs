﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000050 RID: 80
	internal struct RenderBufferHelper
	{
		// Token: 0x06000616 RID: 1558
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetLoadAction(out RenderBuffer b);

		// Token: 0x06000617 RID: 1559
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetLoadAction(out RenderBuffer b, int a);

		// Token: 0x06000618 RID: 1560
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetStoreAction(out RenderBuffer b);

		// Token: 0x06000619 RID: 1561
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetStoreAction(out RenderBuffer b, int a);

		// Token: 0x0600061A RID: 1562 RVA: 0x00008128 File Offset: 0x00006328
		internal static IntPtr GetNativeRenderBufferPtr(IntPtr rb)
		{
			IntPtr result;
			RenderBufferHelper.INTERNAL_CALL_GetNativeRenderBufferPtr(rb, out result);
			return result;
		}

		// Token: 0x0600061B RID: 1563
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetNativeRenderBufferPtr(IntPtr rb, out IntPtr value);
	}
}
