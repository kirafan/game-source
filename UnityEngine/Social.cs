﻿using System;
using UnityEngine.SocialPlatforms;

namespace UnityEngine
{
	// Token: 0x02000349 RID: 841
	public static class Social
	{
		// Token: 0x17000AB5 RID: 2741
		// (get) Token: 0x06002D75 RID: 11637 RVA: 0x00048A44 File Offset: 0x00046C44
		// (set) Token: 0x06002D76 RID: 11638 RVA: 0x00048A60 File Offset: 0x00046C60
		public static ISocialPlatform Active
		{
			get
			{
				return ActivePlatform.Instance;
			}
			set
			{
				ActivePlatform.Instance = value;
			}
		}

		// Token: 0x17000AB6 RID: 2742
		// (get) Token: 0x06002D77 RID: 11639 RVA: 0x00048A6C File Offset: 0x00046C6C
		public static ILocalUser localUser
		{
			get
			{
				return Social.Active.localUser;
			}
		}

		// Token: 0x06002D78 RID: 11640 RVA: 0x00048A8C File Offset: 0x00046C8C
		public static void LoadUsers(string[] userIDs, Action<IUserProfile[]> callback)
		{
			Social.Active.LoadUsers(userIDs, callback);
		}

		// Token: 0x06002D79 RID: 11641 RVA: 0x00048A9C File Offset: 0x00046C9C
		public static void ReportProgress(string achievementID, double progress, Action<bool> callback)
		{
			Social.Active.ReportProgress(achievementID, progress, callback);
		}

		// Token: 0x06002D7A RID: 11642 RVA: 0x00048AAC File Offset: 0x00046CAC
		public static void LoadAchievementDescriptions(Action<IAchievementDescription[]> callback)
		{
			Social.Active.LoadAchievementDescriptions(callback);
		}

		// Token: 0x06002D7B RID: 11643 RVA: 0x00048ABC File Offset: 0x00046CBC
		public static void LoadAchievements(Action<IAchievement[]> callback)
		{
			Social.Active.LoadAchievements(callback);
		}

		// Token: 0x06002D7C RID: 11644 RVA: 0x00048ACC File Offset: 0x00046CCC
		public static void ReportScore(long score, string board, Action<bool> callback)
		{
			Social.Active.ReportScore(score, board, callback);
		}

		// Token: 0x06002D7D RID: 11645 RVA: 0x00048ADC File Offset: 0x00046CDC
		public static void LoadScores(string leaderboardID, Action<IScore[]> callback)
		{
			Social.Active.LoadScores(leaderboardID, callback);
		}

		// Token: 0x06002D7E RID: 11646 RVA: 0x00048AEC File Offset: 0x00046CEC
		public static ILeaderboard CreateLeaderboard()
		{
			return Social.Active.CreateLeaderboard();
		}

		// Token: 0x06002D7F RID: 11647 RVA: 0x00048B0C File Offset: 0x00046D0C
		public static IAchievement CreateAchievement()
		{
			return Social.Active.CreateAchievement();
		}

		// Token: 0x06002D80 RID: 11648 RVA: 0x00048B2C File Offset: 0x00046D2C
		public static void ShowAchievementsUI()
		{
			Social.Active.ShowAchievementsUI();
		}

		// Token: 0x06002D81 RID: 11649 RVA: 0x00048B3C File Offset: 0x00046D3C
		public static void ShowLeaderboardUI()
		{
			Social.Active.ShowLeaderboardUI();
		}
	}
}
