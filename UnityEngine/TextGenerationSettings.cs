﻿using System;

namespace UnityEngine
{
	// Token: 0x020001FC RID: 508
	public struct TextGenerationSettings
	{
		// Token: 0x06002281 RID: 8833 RVA: 0x00027C24 File Offset: 0x00025E24
		private bool CompareColors(Color left, Color right)
		{
			return Mathf.Approximately(left.r, right.r) && Mathf.Approximately(left.g, right.g) && Mathf.Approximately(left.b, right.b) && Mathf.Approximately(left.a, right.a);
		}

		// Token: 0x06002282 RID: 8834 RVA: 0x00027C98 File Offset: 0x00025E98
		private bool CompareVector2(Vector2 left, Vector2 right)
		{
			return Mathf.Approximately(left.x, right.x) && Mathf.Approximately(left.y, right.y);
		}

		// Token: 0x06002283 RID: 8835 RVA: 0x00027CDC File Offset: 0x00025EDC
		public bool Equals(TextGenerationSettings other)
		{
			return this.CompareColors(this.color, other.color) && this.fontSize == other.fontSize && Mathf.Approximately(this.scaleFactor, other.scaleFactor) && this.resizeTextMinSize == other.resizeTextMinSize && this.resizeTextMaxSize == other.resizeTextMaxSize && Mathf.Approximately(this.lineSpacing, other.lineSpacing) && this.fontStyle == other.fontStyle && this.richText == other.richText && this.textAnchor == other.textAnchor && this.alignByGeometry == other.alignByGeometry && this.resizeTextForBestFit == other.resizeTextForBestFit && this.resizeTextMinSize == other.resizeTextMinSize && this.resizeTextMaxSize == other.resizeTextMaxSize && this.resizeTextForBestFit == other.resizeTextForBestFit && this.updateBounds == other.updateBounds && this.horizontalOverflow == other.horizontalOverflow && this.verticalOverflow == other.verticalOverflow && this.CompareVector2(this.generationExtents, other.generationExtents) && this.CompareVector2(this.pivot, other.pivot) && this.font == other.font;
		}

		// Token: 0x040005F4 RID: 1524
		public Font font;

		// Token: 0x040005F5 RID: 1525
		public Color color;

		// Token: 0x040005F6 RID: 1526
		public int fontSize;

		// Token: 0x040005F7 RID: 1527
		public float lineSpacing;

		// Token: 0x040005F8 RID: 1528
		public bool richText;

		// Token: 0x040005F9 RID: 1529
		public float scaleFactor;

		// Token: 0x040005FA RID: 1530
		public FontStyle fontStyle;

		// Token: 0x040005FB RID: 1531
		public TextAnchor textAnchor;

		// Token: 0x040005FC RID: 1532
		public bool alignByGeometry;

		// Token: 0x040005FD RID: 1533
		public bool resizeTextForBestFit;

		// Token: 0x040005FE RID: 1534
		public int resizeTextMinSize;

		// Token: 0x040005FF RID: 1535
		public int resizeTextMaxSize;

		// Token: 0x04000600 RID: 1536
		public bool updateBounds;

		// Token: 0x04000601 RID: 1537
		public VerticalWrapMode verticalOverflow;

		// Token: 0x04000602 RID: 1538
		public HorizontalWrapMode horizontalOverflow;

		// Token: 0x04000603 RID: 1539
		public Vector2 generationExtents;

		// Token: 0x04000604 RID: 1540
		public Vector2 pivot;

		// Token: 0x04000605 RID: 1541
		public bool generateOutOfBounds;
	}
}
