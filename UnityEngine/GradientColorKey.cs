﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200003B RID: 59
	[UsedByNativeCode]
	public struct GradientColorKey
	{
		// Token: 0x06000464 RID: 1124 RVA: 0x0000738C File Offset: 0x0000558C
		public GradientColorKey(Color col, float time)
		{
			this.color = col;
			this.time = time;
		}

		// Token: 0x04000059 RID: 89
		public Color color;

		// Token: 0x0400005A RID: 90
		public float time;
	}
}
