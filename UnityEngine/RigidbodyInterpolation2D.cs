﻿using System;

namespace UnityEngine
{
	// Token: 0x02000159 RID: 345
	public enum RigidbodyInterpolation2D
	{
		// Token: 0x040003CB RID: 971
		None,
		// Token: 0x040003CC RID: 972
		Interpolate,
		// Token: 0x040003CD RID: 973
		Extrapolate
	}
}
