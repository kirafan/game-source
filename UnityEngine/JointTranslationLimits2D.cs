﻿using System;

namespace UnityEngine
{
	// Token: 0x0200016C RID: 364
	public struct JointTranslationLimits2D
	{
		// Token: 0x1700062A RID: 1578
		// (get) Token: 0x06001AA4 RID: 6820 RVA: 0x00021484 File Offset: 0x0001F684
		// (set) Token: 0x06001AA5 RID: 6821 RVA: 0x000214A0 File Offset: 0x0001F6A0
		public float min
		{
			get
			{
				return this.m_LowerTranslation;
			}
			set
			{
				this.m_LowerTranslation = value;
			}
		}

		// Token: 0x1700062B RID: 1579
		// (get) Token: 0x06001AA6 RID: 6822 RVA: 0x000214AC File Offset: 0x0001F6AC
		// (set) Token: 0x06001AA7 RID: 6823 RVA: 0x000214C8 File Offset: 0x0001F6C8
		public float max
		{
			get
			{
				return this.m_UpperTranslation;
			}
			set
			{
				this.m_UpperTranslation = value;
			}
		}

		// Token: 0x040003FB RID: 1019
		private float m_LowerTranslation;

		// Token: 0x040003FC RID: 1020
		private float m_UpperTranslation;
	}
}
