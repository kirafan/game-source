﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x02000057 RID: 87
	public sealed class LightProbes : Object
	{
		// Token: 0x06000694 RID: 1684 RVA: 0x00009014 File Offset: 0x00007214
		public static void GetInterpolatedProbe(Vector3 position, Renderer renderer, out SphericalHarmonicsL2 probe)
		{
			LightProbes.INTERNAL_CALL_GetInterpolatedProbe(ref position, renderer, out probe);
		}

		// Token: 0x06000695 RID: 1685
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetInterpolatedProbe(ref Vector3 position, Renderer renderer, out SphericalHarmonicsL2 probe);

		// Token: 0x17000171 RID: 369
		// (get) Token: 0x06000696 RID: 1686
		public extern Vector3[] positions { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000172 RID: 370
		// (get) Token: 0x06000697 RID: 1687
		// (set) Token: 0x06000698 RID: 1688
		public extern SphericalHarmonicsL2[] bakedProbes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000173 RID: 371
		// (get) Token: 0x06000699 RID: 1689
		public extern int count { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000174 RID: 372
		// (get) Token: 0x0600069A RID: 1690
		public extern int cellCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600069B RID: 1691 RVA: 0x00009020 File Offset: 0x00007220
		[Obsolete("GetInterpolatedLightProbe has been deprecated. Please use the static GetInterpolatedProbe instead.", true)]
		public void GetInterpolatedLightProbe(Vector3 position, Renderer renderer, float[] coefficients)
		{
		}

		// Token: 0x17000175 RID: 373
		// (get) Token: 0x0600069C RID: 1692 RVA: 0x00009024 File Offset: 0x00007224
		// (set) Token: 0x0600069D RID: 1693 RVA: 0x00009040 File Offset: 0x00007240
		[Obsolete("coefficients property has been deprecated. Please use bakedProbes instead.", true)]
		public float[] coefficients
		{
			get
			{
				return new float[0];
			}
			set
			{
			}
		}
	}
}
