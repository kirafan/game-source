﻿using System;

namespace UnityEngine
{
	// Token: 0x02000367 RID: 871
	internal static class ScrollWaitDefinitions
	{
		// Token: 0x04000D44 RID: 3396
		public const int firstWait = 250;

		// Token: 0x04000D45 RID: 3397
		public const int regularWait = 30;
	}
}
