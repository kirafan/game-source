﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000088 RID: 136
	[RequiredByNativeCode]
	public struct Keyframe
	{
		// Token: 0x0600097E RID: 2430 RVA: 0x0000E190 File Offset: 0x0000C390
		public Keyframe(float time, float value)
		{
			this.m_Time = time;
			this.m_Value = value;
			this.m_InTangent = 0f;
			this.m_OutTangent = 0f;
		}

		// Token: 0x0600097F RID: 2431 RVA: 0x0000E1B8 File Offset: 0x0000C3B8
		public Keyframe(float time, float value, float inTangent, float outTangent)
		{
			this.m_Time = time;
			this.m_Value = value;
			this.m_InTangent = inTangent;
			this.m_OutTangent = outTangent;
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x06000980 RID: 2432 RVA: 0x0000E1D8 File Offset: 0x0000C3D8
		// (set) Token: 0x06000981 RID: 2433 RVA: 0x0000E1F4 File Offset: 0x0000C3F4
		public float time
		{
			get
			{
				return this.m_Time;
			}
			set
			{
				this.m_Time = value;
			}
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06000982 RID: 2434 RVA: 0x0000E200 File Offset: 0x0000C400
		// (set) Token: 0x06000983 RID: 2435 RVA: 0x0000E21C File Offset: 0x0000C41C
		public float value
		{
			get
			{
				return this.m_Value;
			}
			set
			{
				this.m_Value = value;
			}
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06000984 RID: 2436 RVA: 0x0000E228 File Offset: 0x0000C428
		// (set) Token: 0x06000985 RID: 2437 RVA: 0x0000E244 File Offset: 0x0000C444
		public float inTangent
		{
			get
			{
				return this.m_InTangent;
			}
			set
			{
				this.m_InTangent = value;
			}
		}

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06000986 RID: 2438 RVA: 0x0000E250 File Offset: 0x0000C450
		// (set) Token: 0x06000987 RID: 2439 RVA: 0x0000E26C File Offset: 0x0000C46C
		public float outTangent
		{
			get
			{
				return this.m_OutTangent;
			}
			set
			{
				this.m_OutTangent = value;
			}
		}

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06000988 RID: 2440 RVA: 0x0000E278 File Offset: 0x0000C478
		// (set) Token: 0x06000989 RID: 2441 RVA: 0x0000E290 File Offset: 0x0000C490
		public int tangentMode
		{
			get
			{
				return 0;
			}
			set
			{
			}
		}

		// Token: 0x0400011A RID: 282
		private float m_Time;

		// Token: 0x0400011B RID: 283
		private float m_Value;

		// Token: 0x0400011C RID: 284
		private float m_InTangent;

		// Token: 0x0400011D RID: 285
		private float m_OutTangent;
	}
}
