﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F3 RID: 755
	public enum TexGenMode
	{
		// Token: 0x04000B2C RID: 2860
		None,
		// Token: 0x04000B2D RID: 2861
		SphereMap,
		// Token: 0x04000B2E RID: 2862
		Object,
		// Token: 0x04000B2F RID: 2863
		EyeLinear,
		// Token: 0x04000B30 RID: 2864
		CubeReflect,
		// Token: 0x04000B31 RID: 2865
		CubeNormal
	}
}
