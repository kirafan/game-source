﻿using System;

namespace UnityEngine
{
	// Token: 0x02000073 RID: 115
	public struct LocationInfo
	{
		// Token: 0x170001BA RID: 442
		// (get) Token: 0x0600079A RID: 1946 RVA: 0x00009CB4 File Offset: 0x00007EB4
		public float latitude
		{
			get
			{
				return this.m_Latitude;
			}
		}

		// Token: 0x170001BB RID: 443
		// (get) Token: 0x0600079B RID: 1947 RVA: 0x00009CD0 File Offset: 0x00007ED0
		public float longitude
		{
			get
			{
				return this.m_Longitude;
			}
		}

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x0600079C RID: 1948 RVA: 0x00009CEC File Offset: 0x00007EEC
		public float altitude
		{
			get
			{
				return this.m_Altitude;
			}
		}

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x0600079D RID: 1949 RVA: 0x00009D08 File Offset: 0x00007F08
		public float horizontalAccuracy
		{
			get
			{
				return this.m_HorizontalAccuracy;
			}
		}

		// Token: 0x170001BE RID: 446
		// (get) Token: 0x0600079E RID: 1950 RVA: 0x00009D24 File Offset: 0x00007F24
		public float verticalAccuracy
		{
			get
			{
				return this.m_VerticalAccuracy;
			}
		}

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x0600079F RID: 1951 RVA: 0x00009D40 File Offset: 0x00007F40
		public double timestamp
		{
			get
			{
				return this.m_Timestamp;
			}
		}

		// Token: 0x040000D5 RID: 213
		private double m_Timestamp;

		// Token: 0x040000D6 RID: 214
		private float m_Latitude;

		// Token: 0x040000D7 RID: 215
		private float m_Longitude;

		// Token: 0x040000D8 RID: 216
		private float m_Altitude;

		// Token: 0x040000D9 RID: 217
		private float m_HorizontalAccuracy;

		// Token: 0x040000DA RID: 218
		private float m_VerticalAccuracy;
	}
}
