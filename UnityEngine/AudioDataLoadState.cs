﻿using System;

namespace UnityEngine
{
	// Token: 0x02000197 RID: 407
	public enum AudioDataLoadState
	{
		// Token: 0x04000441 RID: 1089
		Unloaded,
		// Token: 0x04000442 RID: 1090
		Loading,
		// Token: 0x04000443 RID: 1091
		Loaded,
		// Token: 0x04000444 RID: 1092
		Failed
	}
}
