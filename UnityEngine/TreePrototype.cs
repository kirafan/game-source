﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001EF RID: 495
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class TreePrototype
	{
		// Token: 0x1700083E RID: 2110
		// (get) Token: 0x0600219B RID: 8603 RVA: 0x00026F44 File Offset: 0x00025144
		// (set) Token: 0x0600219C RID: 8604 RVA: 0x00026F60 File Offset: 0x00025160
		public GameObject prefab
		{
			get
			{
				return this.m_Prefab;
			}
			set
			{
				this.m_Prefab = value;
			}
		}

		// Token: 0x1700083F RID: 2111
		// (get) Token: 0x0600219D RID: 8605 RVA: 0x00026F6C File Offset: 0x0002516C
		// (set) Token: 0x0600219E RID: 8606 RVA: 0x00026F88 File Offset: 0x00025188
		public float bendFactor
		{
			get
			{
				return this.m_BendFactor;
			}
			set
			{
				this.m_BendFactor = value;
			}
		}

		// Token: 0x040005B6 RID: 1462
		internal GameObject m_Prefab;

		// Token: 0x040005B7 RID: 1463
		internal float m_BendFactor;
	}
}
