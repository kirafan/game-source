﻿using System;

namespace UnityEngine
{
	// Token: 0x020002EA RID: 746
	public enum LightShadows
	{
		// Token: 0x04000B02 RID: 2818
		None,
		// Token: 0x04000B03 RID: 2819
		Hard,
		// Token: 0x04000B04 RID: 2820
		Soft
	}
}
