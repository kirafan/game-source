﻿using System;

namespace UnityEngine
{
	// Token: 0x020002D6 RID: 726
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	internal class CppPropertyBodyAttribute : Attribute
	{
		// Token: 0x06002C86 RID: 11398 RVA: 0x00046390 File Offset: 0x00044590
		public CppPropertyBodyAttribute(string getterBody, string setterBody)
		{
		}

		// Token: 0x06002C87 RID: 11399 RVA: 0x0004639C File Offset: 0x0004459C
		public CppPropertyBodyAttribute(string getterBody)
		{
		}
	}
}
