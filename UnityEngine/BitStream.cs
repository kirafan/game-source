﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200009B RID: 155
	[RequiredByNativeCode]
	public sealed class BitStream
	{
		// Token: 0x06000AE3 RID: 2787
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Serializeb(ref int value);

		// Token: 0x06000AE4 RID: 2788
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Serializec(ref char value);

		// Token: 0x06000AE5 RID: 2789
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Serializes(ref short value);

		// Token: 0x06000AE6 RID: 2790
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Serializei(ref int value);

		// Token: 0x06000AE7 RID: 2791
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Serializef(ref float value, float maximumDelta);

		// Token: 0x06000AE8 RID: 2792 RVA: 0x0000F7B4 File Offset: 0x0000D9B4
		private void Serializeq(ref Quaternion value, float maximumDelta)
		{
			BitStream.INTERNAL_CALL_Serializeq(this, ref value, maximumDelta);
		}

		// Token: 0x06000AE9 RID: 2793
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Serializeq(BitStream self, ref Quaternion value, float maximumDelta);

		// Token: 0x06000AEA RID: 2794 RVA: 0x0000F7C0 File Offset: 0x0000D9C0
		private void Serializev(ref Vector3 value, float maximumDelta)
		{
			BitStream.INTERNAL_CALL_Serializev(this, ref value, maximumDelta);
		}

		// Token: 0x06000AEB RID: 2795
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Serializev(BitStream self, ref Vector3 value, float maximumDelta);

		// Token: 0x06000AEC RID: 2796 RVA: 0x0000F7CC File Offset: 0x0000D9CC
		private void Serializen(ref NetworkViewID viewID)
		{
			BitStream.INTERNAL_CALL_Serializen(this, ref viewID);
		}

		// Token: 0x06000AED RID: 2797
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Serializen(BitStream self, ref NetworkViewID viewID);

		// Token: 0x06000AEE RID: 2798 RVA: 0x0000F7D8 File Offset: 0x0000D9D8
		public void Serialize(ref bool value)
		{
			int num = (!value) ? 0 : 1;
			this.Serializeb(ref num);
			value = (num != 0);
		}

		// Token: 0x06000AEF RID: 2799 RVA: 0x0000F80C File Offset: 0x0000DA0C
		public void Serialize(ref char value)
		{
			this.Serializec(ref value);
		}

		// Token: 0x06000AF0 RID: 2800 RVA: 0x0000F818 File Offset: 0x0000DA18
		public void Serialize(ref short value)
		{
			this.Serializes(ref value);
		}

		// Token: 0x06000AF1 RID: 2801 RVA: 0x0000F824 File Offset: 0x0000DA24
		public void Serialize(ref int value)
		{
			this.Serializei(ref value);
		}

		// Token: 0x06000AF2 RID: 2802 RVA: 0x0000F830 File Offset: 0x0000DA30
		[ExcludeFromDocs]
		public void Serialize(ref float value)
		{
			float maxDelta = 1E-05f;
			this.Serialize(ref value, maxDelta);
		}

		// Token: 0x06000AF3 RID: 2803 RVA: 0x0000F84C File Offset: 0x0000DA4C
		public void Serialize(ref float value, [DefaultValue("0.00001F")] float maxDelta)
		{
			this.Serializef(ref value, maxDelta);
		}

		// Token: 0x06000AF4 RID: 2804 RVA: 0x0000F858 File Offset: 0x0000DA58
		[ExcludeFromDocs]
		public void Serialize(ref Quaternion value)
		{
			float maxDelta = 1E-05f;
			this.Serialize(ref value, maxDelta);
		}

		// Token: 0x06000AF5 RID: 2805 RVA: 0x0000F874 File Offset: 0x0000DA74
		public void Serialize(ref Quaternion value, [DefaultValue("0.00001F")] float maxDelta)
		{
			this.Serializeq(ref value, maxDelta);
		}

		// Token: 0x06000AF6 RID: 2806 RVA: 0x0000F880 File Offset: 0x0000DA80
		[ExcludeFromDocs]
		public void Serialize(ref Vector3 value)
		{
			float maxDelta = 1E-05f;
			this.Serialize(ref value, maxDelta);
		}

		// Token: 0x06000AF7 RID: 2807 RVA: 0x0000F89C File Offset: 0x0000DA9C
		public void Serialize(ref Vector3 value, [DefaultValue("0.00001F")] float maxDelta)
		{
			this.Serializev(ref value, maxDelta);
		}

		// Token: 0x06000AF8 RID: 2808 RVA: 0x0000F8A8 File Offset: 0x0000DAA8
		public void Serialize(ref NetworkPlayer value)
		{
			int index = value.index;
			this.Serializei(ref index);
			value.index = index;
		}

		// Token: 0x06000AF9 RID: 2809 RVA: 0x0000F8CC File Offset: 0x0000DACC
		public void Serialize(ref NetworkViewID viewID)
		{
			this.Serializen(ref viewID);
		}

		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06000AFA RID: 2810
		public extern bool isReading { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700027E RID: 638
		// (get) Token: 0x06000AFB RID: 2811
		public extern bool isWriting { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000AFC RID: 2812
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Serialize(ref string value);

		// Token: 0x04000167 RID: 359
		internal IntPtr m_Ptr;
	}
}
