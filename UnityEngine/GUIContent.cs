﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine
{
	// Token: 0x0200021D RID: 541
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public class GUIContent
	{
		// Token: 0x060024C1 RID: 9409 RVA: 0x0002DCA8 File Offset: 0x0002BEA8
		public GUIContent()
		{
		}

		// Token: 0x060024C2 RID: 9410 RVA: 0x0002DCC8 File Offset: 0x0002BEC8
		public GUIContent(string text) : this(text, null, string.Empty)
		{
		}

		// Token: 0x060024C3 RID: 9411 RVA: 0x0002DCD8 File Offset: 0x0002BED8
		public GUIContent(Texture image) : this(string.Empty, image, string.Empty)
		{
		}

		// Token: 0x060024C4 RID: 9412 RVA: 0x0002DCEC File Offset: 0x0002BEEC
		public GUIContent(string text, Texture image) : this(text, image, string.Empty)
		{
		}

		// Token: 0x060024C5 RID: 9413 RVA: 0x0002DCFC File Offset: 0x0002BEFC
		public GUIContent(string text, string tooltip) : this(text, null, tooltip)
		{
		}

		// Token: 0x060024C6 RID: 9414 RVA: 0x0002DD08 File Offset: 0x0002BF08
		public GUIContent(Texture image, string tooltip) : this(string.Empty, image, tooltip)
		{
		}

		// Token: 0x060024C7 RID: 9415 RVA: 0x0002DD18 File Offset: 0x0002BF18
		public GUIContent(string text, Texture image, string tooltip)
		{
			this.text = text;
			this.image = image;
			this.tooltip = tooltip;
		}

		// Token: 0x060024C8 RID: 9416 RVA: 0x0002DD4C File Offset: 0x0002BF4C
		public GUIContent(GUIContent src)
		{
			this.text = src.m_Text;
			this.image = src.m_Image;
			this.tooltip = src.m_Tooltip;
		}

		// Token: 0x17000912 RID: 2322
		// (get) Token: 0x060024C9 RID: 9417 RVA: 0x0002DD9C File Offset: 0x0002BF9C
		// (set) Token: 0x060024CA RID: 9418 RVA: 0x0002DDB8 File Offset: 0x0002BFB8
		public string text
		{
			get
			{
				return this.m_Text;
			}
			set
			{
				this.m_Text = value;
			}
		}

		// Token: 0x17000913 RID: 2323
		// (get) Token: 0x060024CB RID: 9419 RVA: 0x0002DDC4 File Offset: 0x0002BFC4
		// (set) Token: 0x060024CC RID: 9420 RVA: 0x0002DDE0 File Offset: 0x0002BFE0
		public Texture image
		{
			get
			{
				return this.m_Image;
			}
			set
			{
				this.m_Image = value;
			}
		}

		// Token: 0x17000914 RID: 2324
		// (get) Token: 0x060024CD RID: 9421 RVA: 0x0002DDEC File Offset: 0x0002BFEC
		// (set) Token: 0x060024CE RID: 9422 RVA: 0x0002DE08 File Offset: 0x0002C008
		public string tooltip
		{
			get
			{
				return this.m_Tooltip;
			}
			set
			{
				this.m_Tooltip = value;
			}
		}

		// Token: 0x17000915 RID: 2325
		// (get) Token: 0x060024CF RID: 9423 RVA: 0x0002DE14 File Offset: 0x0002C014
		internal int hash
		{
			get
			{
				int result = 0;
				if (!string.IsNullOrEmpty(this.m_Text))
				{
					result = this.m_Text.GetHashCode() * 37;
				}
				return result;
			}
		}

		// Token: 0x060024D0 RID: 9424 RVA: 0x0002DE4C File Offset: 0x0002C04C
		internal static GUIContent Temp(string t)
		{
			GUIContent.s_Text.m_Text = t;
			GUIContent.s_Text.m_Tooltip = string.Empty;
			return GUIContent.s_Text;
		}

		// Token: 0x060024D1 RID: 9425 RVA: 0x0002DE80 File Offset: 0x0002C080
		internal static GUIContent Temp(string t, string tooltip)
		{
			GUIContent.s_Text.m_Text = t;
			GUIContent.s_Text.m_Tooltip = tooltip;
			return GUIContent.s_Text;
		}

		// Token: 0x060024D2 RID: 9426 RVA: 0x0002DEB0 File Offset: 0x0002C0B0
		internal static GUIContent Temp(Texture i)
		{
			GUIContent.s_Image.m_Image = i;
			GUIContent.s_Image.m_Tooltip = string.Empty;
			return GUIContent.s_Image;
		}

		// Token: 0x060024D3 RID: 9427 RVA: 0x0002DEE4 File Offset: 0x0002C0E4
		internal static GUIContent Temp(Texture i, string tooltip)
		{
			GUIContent.s_Image.m_Image = i;
			GUIContent.s_Image.m_Tooltip = tooltip;
			return GUIContent.s_Image;
		}

		// Token: 0x060024D4 RID: 9428 RVA: 0x0002DF14 File Offset: 0x0002C114
		internal static GUIContent Temp(string t, Texture i)
		{
			GUIContent.s_TextImage.m_Text = t;
			GUIContent.s_TextImage.m_Image = i;
			return GUIContent.s_TextImage;
		}

		// Token: 0x060024D5 RID: 9429 RVA: 0x0002DF44 File Offset: 0x0002C144
		internal static void ClearStaticCache()
		{
			GUIContent.s_Text.m_Text = null;
			GUIContent.s_Text.m_Tooltip = string.Empty;
			GUIContent.s_Image.m_Image = null;
			GUIContent.s_Image.m_Tooltip = string.Empty;
			GUIContent.s_TextImage.m_Text = null;
			GUIContent.s_TextImage.m_Image = null;
		}

		// Token: 0x060024D6 RID: 9430 RVA: 0x0002DF9C File Offset: 0x0002C19C
		internal static GUIContent[] Temp(string[] texts)
		{
			GUIContent[] array = new GUIContent[texts.Length];
			for (int i = 0; i < texts.Length; i++)
			{
				array[i] = new GUIContent(texts[i]);
			}
			return array;
		}

		// Token: 0x060024D7 RID: 9431 RVA: 0x0002DFDC File Offset: 0x0002C1DC
		internal static GUIContent[] Temp(Texture[] images)
		{
			GUIContent[] array = new GUIContent[images.Length];
			for (int i = 0; i < images.Length; i++)
			{
				array[i] = new GUIContent(images[i]);
			}
			return array;
		}

		// Token: 0x040007C8 RID: 1992
		[SerializeField]
		private string m_Text = string.Empty;

		// Token: 0x040007C9 RID: 1993
		[SerializeField]
		private Texture m_Image;

		// Token: 0x040007CA RID: 1994
		[SerializeField]
		private string m_Tooltip = string.Empty;

		// Token: 0x040007CB RID: 1995
		private static readonly GUIContent s_Text = new GUIContent();

		// Token: 0x040007CC RID: 1996
		private static readonly GUIContent s_Image = new GUIContent();

		// Token: 0x040007CD RID: 1997
		private static readonly GUIContent s_TextImage = new GUIContent();

		// Token: 0x040007CE RID: 1998
		public static GUIContent none = new GUIContent("");
	}
}
