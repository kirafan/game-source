﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200036D RID: 877
	[RequiredByNativeCode]
	public interface ISerializationCallbackReceiver
	{
		// Token: 0x06002E33 RID: 11827
		void OnBeforeSerialize();

		// Token: 0x06002E34 RID: 11828
		void OnAfterDeserialize();
	}
}
