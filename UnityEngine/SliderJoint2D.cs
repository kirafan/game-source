﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000176 RID: 374
	public sealed class SliderJoint2D : AnchoredJoint2D
	{
		// Token: 0x17000652 RID: 1618
		// (get) Token: 0x06001B06 RID: 6918
		// (set) Token: 0x06001B07 RID: 6919
		public extern bool autoConfigureAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000653 RID: 1619
		// (get) Token: 0x06001B08 RID: 6920
		// (set) Token: 0x06001B09 RID: 6921
		public extern float angle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000654 RID: 1620
		// (get) Token: 0x06001B0A RID: 6922
		// (set) Token: 0x06001B0B RID: 6923
		public extern bool useMotor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000655 RID: 1621
		// (get) Token: 0x06001B0C RID: 6924
		// (set) Token: 0x06001B0D RID: 6925
		public extern bool useLimits { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000656 RID: 1622
		// (get) Token: 0x06001B0E RID: 6926 RVA: 0x00021770 File Offset: 0x0001F970
		// (set) Token: 0x06001B0F RID: 6927 RVA: 0x00021790 File Offset: 0x0001F990
		public JointMotor2D motor
		{
			get
			{
				JointMotor2D result;
				this.INTERNAL_get_motor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_motor(ref value);
			}
		}

		// Token: 0x06001B10 RID: 6928
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_motor(out JointMotor2D value);

		// Token: 0x06001B11 RID: 6929
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_motor(ref JointMotor2D value);

		// Token: 0x17000657 RID: 1623
		// (get) Token: 0x06001B12 RID: 6930 RVA: 0x0002179C File Offset: 0x0001F99C
		// (set) Token: 0x06001B13 RID: 6931 RVA: 0x000217BC File Offset: 0x0001F9BC
		public JointTranslationLimits2D limits
		{
			get
			{
				JointTranslationLimits2D result;
				this.INTERNAL_get_limits(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_limits(ref value);
			}
		}

		// Token: 0x06001B14 RID: 6932
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_limits(out JointTranslationLimits2D value);

		// Token: 0x06001B15 RID: 6933
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_limits(ref JointTranslationLimits2D value);

		// Token: 0x17000658 RID: 1624
		// (get) Token: 0x06001B16 RID: 6934
		public extern JointLimitState2D limitState { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000659 RID: 1625
		// (get) Token: 0x06001B17 RID: 6935
		public extern float referenceAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700065A RID: 1626
		// (get) Token: 0x06001B18 RID: 6936
		public extern float jointTranslation { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700065B RID: 1627
		// (get) Token: 0x06001B19 RID: 6937
		public extern float jointSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001B1A RID: 6938 RVA: 0x000217C8 File Offset: 0x0001F9C8
		public float GetMotorForce(float timeStep)
		{
			return SliderJoint2D.INTERNAL_CALL_GetMotorForce(this, timeStep);
		}

		// Token: 0x06001B1B RID: 6939
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float INTERNAL_CALL_GetMotorForce(SliderJoint2D self, float timeStep);
	}
}
