﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200017C RID: 380
	public sealed class ConstantForce2D : PhysicsUpdateBehaviour2D
	{
		// Token: 0x1700066C RID: 1644
		// (get) Token: 0x06001B4B RID: 6987 RVA: 0x000218F8 File Offset: 0x0001FAF8
		// (set) Token: 0x06001B4C RID: 6988 RVA: 0x00021918 File Offset: 0x0001FB18
		public Vector2 force
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_force(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_force(ref value);
			}
		}

		// Token: 0x06001B4D RID: 6989
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_force(out Vector2 value);

		// Token: 0x06001B4E RID: 6990
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_force(ref Vector2 value);

		// Token: 0x1700066D RID: 1645
		// (get) Token: 0x06001B4F RID: 6991 RVA: 0x00021924 File Offset: 0x0001FB24
		// (set) Token: 0x06001B50 RID: 6992 RVA: 0x00021944 File Offset: 0x0001FB44
		public Vector2 relativeForce
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_relativeForce(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_relativeForce(ref value);
			}
		}

		// Token: 0x06001B51 RID: 6993
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_relativeForce(out Vector2 value);

		// Token: 0x06001B52 RID: 6994
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_relativeForce(ref Vector2 value);

		// Token: 0x1700066E RID: 1646
		// (get) Token: 0x06001B53 RID: 6995
		// (set) Token: 0x06001B54 RID: 6996
		public extern float torque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
