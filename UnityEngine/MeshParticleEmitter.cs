﻿using System;

namespace UnityEngine
{
	// Token: 0x020002AA RID: 682
	[Obsolete("This component is part of the legacy particle system, which is deprecated and will be removed in a future release. Use the ParticleSystem component instead.", false)]
	public sealed class MeshParticleEmitter : ParticleEmitter
	{
		// Token: 0x06002B63 RID: 11107 RVA: 0x00041AF0 File Offset: 0x0003FCF0
		internal MeshParticleEmitter()
		{
		}
	}
}
