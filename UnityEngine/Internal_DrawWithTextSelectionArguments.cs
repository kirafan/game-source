﻿using System;

namespace UnityEngine
{
	// Token: 0x02000245 RID: 581
	internal struct Internal_DrawWithTextSelectionArguments
	{
		// Token: 0x040008D6 RID: 2262
		public IntPtr target;

		// Token: 0x040008D7 RID: 2263
		public Rect position;

		// Token: 0x040008D8 RID: 2264
		public int firstPos;

		// Token: 0x040008D9 RID: 2265
		public int lastPos;

		// Token: 0x040008DA RID: 2266
		public Color cursorColor;

		// Token: 0x040008DB RID: 2267
		public Color selectionColor;

		// Token: 0x040008DC RID: 2268
		public int isHover;

		// Token: 0x040008DD RID: 2269
		public int isActive;

		// Token: 0x040008DE RID: 2270
		public int on;

		// Token: 0x040008DF RID: 2271
		public int hasKeyboardFocus;

		// Token: 0x040008E0 RID: 2272
		public int drawSelectionAsComposition;
	}
}
