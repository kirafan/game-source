﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000075 RID: 117
	public sealed class LocationService
	{
		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x060007A1 RID: 1953
		public extern bool isEnabledByUser { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x060007A2 RID: 1954
		public extern LocationServiceStatus status { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x060007A3 RID: 1955
		public extern LocationInfo lastData { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060007A4 RID: 1956
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Start([DefaultValue("10f")] float desiredAccuracyInMeters, [DefaultValue("10f")] float updateDistanceInMeters);

		// Token: 0x060007A5 RID: 1957 RVA: 0x00009D64 File Offset: 0x00007F64
		[ExcludeFromDocs]
		public void Start(float desiredAccuracyInMeters)
		{
			float updateDistanceInMeters = 10f;
			this.Start(desiredAccuracyInMeters, updateDistanceInMeters);
		}

		// Token: 0x060007A6 RID: 1958 RVA: 0x00009D80 File Offset: 0x00007F80
		[ExcludeFromDocs]
		public void Start()
		{
			float updateDistanceInMeters = 10f;
			float desiredAccuracyInMeters = 10f;
			this.Start(desiredAccuracyInMeters, updateDistanceInMeters);
		}

		// Token: 0x060007A7 RID: 1959
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Stop();
	}
}
