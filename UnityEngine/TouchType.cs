﻿using System;

namespace UnityEngine
{
	// Token: 0x0200006E RID: 110
	public enum TouchType
	{
		// Token: 0x040000B7 RID: 183
		Direct,
		// Token: 0x040000B8 RID: 184
		Indirect,
		// Token: 0x040000B9 RID: 185
		Stylus
	}
}
