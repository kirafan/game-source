﻿using System;

namespace UnityEngine
{
	// Token: 0x020000FA RID: 250
	public enum ParticleSystemRenderSpace
	{
		// Token: 0x04000296 RID: 662
		View,
		// Token: 0x04000297 RID: 663
		World,
		// Token: 0x04000298 RID: 664
		Local,
		// Token: 0x04000299 RID: 665
		Facing
	}
}
