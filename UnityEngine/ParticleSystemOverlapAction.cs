﻿using System;

namespace UnityEngine
{
	// Token: 0x02000103 RID: 259
	public enum ParticleSystemOverlapAction
	{
		// Token: 0x040002C8 RID: 712
		Ignore,
		// Token: 0x040002C9 RID: 713
		Kill,
		// Token: 0x040002CA RID: 714
		Callback
	}
}
