﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F0 RID: 752
	public enum ShadowResolution
	{
		// Token: 0x04000B1C RID: 2844
		Low,
		// Token: 0x04000B1D RID: 2845
		Medium,
		// Token: 0x04000B1E RID: 2846
		High,
		// Token: 0x04000B1F RID: 2847
		VeryHigh
	}
}
