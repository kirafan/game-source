﻿using System;
using System.Runtime.InteropServices;

namespace UnityEngine
{
	// Token: 0x020001B7 RID: 439
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class AnimationClipPair
	{
		// Token: 0x040004A2 RID: 1186
		public AnimationClip originalClip;

		// Token: 0x040004A3 RID: 1187
		public AnimationClip overrideClip;
	}
}
