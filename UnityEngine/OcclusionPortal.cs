﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000040 RID: 64
	public sealed class OcclusionPortal : Component
	{
		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x0600047D RID: 1149
		// (set) Token: 0x0600047E RID: 1150
		public extern bool open { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
