﻿using System;

namespace UnityEngine
{
	// Token: 0x020002C3 RID: 707
	[AttributeUsage(AttributeTargets.Struct)]
	internal class IL2CPPStructAlignmentAttribute : Attribute
	{
		// Token: 0x06002C4B RID: 11339 RVA: 0x00045204 File Offset: 0x00043404
		public IL2CPPStructAlignmentAttribute()
		{
			this.Align = 1;
		}

		// Token: 0x04000A45 RID: 2629
		public int Align;
	}
}
