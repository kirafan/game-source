﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000170 RID: 368
	public class AnchoredJoint2D : Joint2D
	{
		// Token: 0x17000637 RID: 1591
		// (get) Token: 0x06001AC2 RID: 6850 RVA: 0x00021628 File Offset: 0x0001F828
		// (set) Token: 0x06001AC3 RID: 6851 RVA: 0x00021648 File Offset: 0x0001F848
		public Vector2 anchor
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_anchor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_anchor(ref value);
			}
		}

		// Token: 0x06001AC4 RID: 6852
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_anchor(out Vector2 value);

		// Token: 0x06001AC5 RID: 6853
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_anchor(ref Vector2 value);

		// Token: 0x17000638 RID: 1592
		// (get) Token: 0x06001AC6 RID: 6854 RVA: 0x00021654 File Offset: 0x0001F854
		// (set) Token: 0x06001AC7 RID: 6855 RVA: 0x00021674 File Offset: 0x0001F874
		public Vector2 connectedAnchor
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_connectedAnchor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_connectedAnchor(ref value);
			}
		}

		// Token: 0x06001AC8 RID: 6856
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_connectedAnchor(out Vector2 value);

		// Token: 0x06001AC9 RID: 6857
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_connectedAnchor(ref Vector2 value);

		// Token: 0x17000639 RID: 1593
		// (get) Token: 0x06001ACA RID: 6858
		// (set) Token: 0x06001ACB RID: 6859
		public extern bool autoConfigureConnectedAnchor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
