﻿using System;

namespace UnityEngine
{
	// Token: 0x0200010D RID: 269
	[Flags]
	public enum ParticleSystemSubEmitterProperties
	{
		// Token: 0x040002FB RID: 763
		InheritNothing = 0,
		// Token: 0x040002FC RID: 764
		InheritEverything = 7,
		// Token: 0x040002FD RID: 765
		InheritColor = 1,
		// Token: 0x040002FE RID: 766
		InheritSize = 2,
		// Token: 0x040002FF RID: 767
		InheritRotation = 4
	}
}
