﻿using System;

namespace UnityEngine
{
	// Token: 0x02000004 RID: 4
	public class AndroidJavaClass : AndroidJavaObject
	{
		// Token: 0x0600002B RID: 43 RVA: 0x00003978 File Offset: 0x00001B78
		public AndroidJavaClass(string className)
		{
			this._AndroidJavaClass(className);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x00003988 File Offset: 0x00001B88
		internal AndroidJavaClass(IntPtr jclass)
		{
			if (jclass == IntPtr.Zero)
			{
				throw new Exception("JNI: Init'd AndroidJavaClass with null ptr!");
			}
			this.m_jclass = AndroidJNI.NewGlobalRef(jclass);
			this.m_jobject = IntPtr.Zero;
		}

		// Token: 0x0600002D RID: 45 RVA: 0x000039C4 File Offset: 0x00001BC4
		private void _AndroidJavaClass(string className)
		{
			base.DebugPrint("Creating AndroidJavaClass from " + className);
			using (AndroidJavaObject androidJavaObject = AndroidJavaObject.FindClass(className))
			{
				this.m_jclass = AndroidJNI.NewGlobalRef(androidJavaObject.GetRawObject());
				this.m_jobject = IntPtr.Zero;
			}
		}
	}
}
