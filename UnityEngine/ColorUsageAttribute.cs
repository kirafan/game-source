﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035F RID: 863
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class ColorUsageAttribute : PropertyAttribute
	{
		// Token: 0x06002DE3 RID: 11747 RVA: 0x00048EE4 File Offset: 0x000470E4
		public ColorUsageAttribute(bool showAlpha)
		{
			this.showAlpha = showAlpha;
		}

		// Token: 0x06002DE4 RID: 11748 RVA: 0x00048F3C File Offset: 0x0004713C
		public ColorUsageAttribute(bool showAlpha, bool hdr, float minBrightness, float maxBrightness, float minExposureValue, float maxExposureValue)
		{
			this.showAlpha = showAlpha;
			this.hdr = hdr;
			this.minBrightness = minBrightness;
			this.maxBrightness = maxBrightness;
			this.minExposureValue = minExposureValue;
			this.maxExposureValue = maxExposureValue;
		}

		// Token: 0x04000D30 RID: 3376
		public readonly bool showAlpha = true;

		// Token: 0x04000D31 RID: 3377
		public readonly bool hdr = false;

		// Token: 0x04000D32 RID: 3378
		public readonly float minBrightness = 0f;

		// Token: 0x04000D33 RID: 3379
		public readonly float maxBrightness = 8f;

		// Token: 0x04000D34 RID: 3380
		public readonly float minExposureValue = 0.125f;

		// Token: 0x04000D35 RID: 3381
		public readonly float maxExposureValue = 3f;
	}
}
