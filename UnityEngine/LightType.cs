﻿using System;

namespace UnityEngine
{
	// Token: 0x020002E8 RID: 744
	public enum LightType
	{
		// Token: 0x04000AF9 RID: 2809
		Spot,
		// Token: 0x04000AFA RID: 2810
		Directional,
		// Token: 0x04000AFB RID: 2811
		Point,
		// Token: 0x04000AFC RID: 2812
		Area
	}
}
