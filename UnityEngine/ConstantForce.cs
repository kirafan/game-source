﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200014D RID: 333
	public sealed class ConstantForce : Behaviour
	{
		// Token: 0x17000596 RID: 1430
		// (get) Token: 0x0600184F RID: 6223 RVA: 0x0001E840 File Offset: 0x0001CA40
		// (set) Token: 0x06001850 RID: 6224 RVA: 0x0001E860 File Offset: 0x0001CA60
		public Vector3 force
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_force(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_force(ref value);
			}
		}

		// Token: 0x06001851 RID: 6225
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_force(out Vector3 value);

		// Token: 0x06001852 RID: 6226
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_force(ref Vector3 value);

		// Token: 0x17000597 RID: 1431
		// (get) Token: 0x06001853 RID: 6227 RVA: 0x0001E86C File Offset: 0x0001CA6C
		// (set) Token: 0x06001854 RID: 6228 RVA: 0x0001E88C File Offset: 0x0001CA8C
		public Vector3 relativeForce
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_relativeForce(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_relativeForce(ref value);
			}
		}

		// Token: 0x06001855 RID: 6229
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_relativeForce(out Vector3 value);

		// Token: 0x06001856 RID: 6230
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_relativeForce(ref Vector3 value);

		// Token: 0x17000598 RID: 1432
		// (get) Token: 0x06001857 RID: 6231 RVA: 0x0001E898 File Offset: 0x0001CA98
		// (set) Token: 0x06001858 RID: 6232 RVA: 0x0001E8B8 File Offset: 0x0001CAB8
		public Vector3 torque
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_torque(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_torque(ref value);
			}
		}

		// Token: 0x06001859 RID: 6233
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_torque(out Vector3 value);

		// Token: 0x0600185A RID: 6234
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_torque(ref Vector3 value);

		// Token: 0x17000599 RID: 1433
		// (get) Token: 0x0600185B RID: 6235 RVA: 0x0001E8C4 File Offset: 0x0001CAC4
		// (set) Token: 0x0600185C RID: 6236 RVA: 0x0001E8E4 File Offset: 0x0001CAE4
		public Vector3 relativeTorque
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_relativeTorque(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_relativeTorque(ref value);
			}
		}

		// Token: 0x0600185D RID: 6237
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_relativeTorque(out Vector3 value);

		// Token: 0x0600185E RID: 6238
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_relativeTorque(ref Vector3 value);
	}
}
