﻿using System;

namespace UnityEngine
{
	// Token: 0x020001BA RID: 442
	internal enum AnimationEventSource
	{
		// Token: 0x040004AC RID: 1196
		NoSource,
		// Token: 0x040004AD RID: 1197
		Legacy,
		// Token: 0x040004AE RID: 1198
		Animator
	}
}
