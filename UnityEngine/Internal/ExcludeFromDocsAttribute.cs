﻿using System;

namespace UnityEngine.Internal
{
	// Token: 0x020003A9 RID: 937
	[AttributeUsage(AttributeTargets.Method)]
	[Serializable]
	public class ExcludeFromDocsAttribute : Attribute
	{
	}
}
