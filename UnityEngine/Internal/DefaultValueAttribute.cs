﻿using System;

namespace UnityEngine.Internal
{
	// Token: 0x020003A8 RID: 936
	[AttributeUsage(AttributeTargets.Parameter | AttributeTargets.GenericParameter)]
	[Serializable]
	public class DefaultValueAttribute : Attribute
	{
		// Token: 0x06002FF0 RID: 12272 RVA: 0x0004EC34 File Offset: 0x0004CE34
		public DefaultValueAttribute(string value)
		{
			this.DefaultValue = value;
		}

		// Token: 0x17000B34 RID: 2868
		// (get) Token: 0x06002FF1 RID: 12273 RVA: 0x0004EC44 File Offset: 0x0004CE44
		public object Value
		{
			get
			{
				return this.DefaultValue;
			}
		}

		// Token: 0x06002FF2 RID: 12274 RVA: 0x0004EC60 File Offset: 0x0004CE60
		public override bool Equals(object obj)
		{
			DefaultValueAttribute defaultValueAttribute = obj as DefaultValueAttribute;
			bool result;
			if (defaultValueAttribute == null)
			{
				result = false;
			}
			else if (this.DefaultValue == null)
			{
				result = (defaultValueAttribute.Value == null);
			}
			else
			{
				result = this.DefaultValue.Equals(defaultValueAttribute.Value);
			}
			return result;
		}

		// Token: 0x06002FF3 RID: 12275 RVA: 0x0004ECB4 File Offset: 0x0004CEB4
		public override int GetHashCode()
		{
			int hashCode;
			if (this.DefaultValue == null)
			{
				hashCode = base.GetHashCode();
			}
			else
			{
				hashCode = this.DefaultValue.GetHashCode();
			}
			return hashCode;
		}

		// Token: 0x04000DBA RID: 3514
		private object DefaultValue;
	}
}
