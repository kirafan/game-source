﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x020000CB RID: 203
	public class Texture : Object
	{
		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06000DBC RID: 3516
		// (set) Token: 0x06000DBD RID: 3517
		public static extern int masterTextureLimit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06000DBE RID: 3518
		// (set) Token: 0x06000DBF RID: 3519
		public static extern AnisotropicFiltering anisotropicFiltering { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000DC0 RID: 3520
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetGlobalAnisotropicFilteringLimits(int forcedMin, int globalMax);

		// Token: 0x06000DC1 RID: 3521
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetWidth(Texture t);

		// Token: 0x06000DC2 RID: 3522
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetHeight(Texture t);

		// Token: 0x06000DC3 RID: 3523
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern TextureDimension Internal_GetDimension(Texture t);

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06000DC4 RID: 3524 RVA: 0x00013164 File Offset: 0x00011364
		// (set) Token: 0x06000DC5 RID: 3525 RVA: 0x00013180 File Offset: 0x00011380
		public virtual int width
		{
			get
			{
				return Texture.Internal_GetWidth(this);
			}
			set
			{
				throw new Exception("not implemented");
			}
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06000DC6 RID: 3526 RVA: 0x00013190 File Offset: 0x00011390
		// (set) Token: 0x06000DC7 RID: 3527 RVA: 0x000131AC File Offset: 0x000113AC
		public virtual int height
		{
			get
			{
				return Texture.Internal_GetHeight(this);
			}
			set
			{
				throw new Exception("not implemented");
			}
		}

		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06000DC8 RID: 3528 RVA: 0x000131BC File Offset: 0x000113BC
		// (set) Token: 0x06000DC9 RID: 3529 RVA: 0x000131D8 File Offset: 0x000113D8
		public virtual TextureDimension dimension
		{
			get
			{
				return Texture.Internal_GetDimension(this);
			}
			set
			{
				throw new Exception("not implemented");
			}
		}

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06000DCA RID: 3530
		// (set) Token: 0x06000DCB RID: 3531
		public extern FilterMode filterMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x06000DCC RID: 3532
		// (set) Token: 0x06000DCD RID: 3533
		public extern int anisoLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x06000DCE RID: 3534
		// (set) Token: 0x06000DCF RID: 3535
		public extern TextureWrapMode wrapMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000303 RID: 771
		// (get) Token: 0x06000DD0 RID: 3536
		// (set) Token: 0x06000DD1 RID: 3537
		public extern float mipMapBias { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000304 RID: 772
		// (get) Token: 0x06000DD2 RID: 3538 RVA: 0x000131E8 File Offset: 0x000113E8
		public Vector2 texelSize
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_texelSize(out result);
				return result;
			}
		}

		// Token: 0x06000DD3 RID: 3539
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_texelSize(out Vector2 value);

		// Token: 0x06000DD4 RID: 3540 RVA: 0x00013208 File Offset: 0x00011408
		public IntPtr GetNativeTexturePtr()
		{
			IntPtr result;
			Texture.INTERNAL_CALL_GetNativeTexturePtr(this, out result);
			return result;
		}

		// Token: 0x06000DD5 RID: 3541
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetNativeTexturePtr(Texture self, out IntPtr value);

		// Token: 0x06000DD6 RID: 3542
		[Obsolete("Use GetNativeTexturePtr instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetNativeTextureID();
	}
}
