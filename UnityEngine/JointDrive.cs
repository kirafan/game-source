﻿using System;

namespace UnityEngine
{
	// Token: 0x02000138 RID: 312
	public struct JointDrive
	{
		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x0600160E RID: 5646 RVA: 0x0001C0E0 File Offset: 0x0001A2E0
		// (set) Token: 0x0600160F RID: 5647 RVA: 0x0001C0F8 File Offset: 0x0001A2F8
		[Obsolete("JointDriveMode is obsolete")]
		public JointDriveMode mode
		{
			get
			{
				return JointDriveMode.None;
			}
			set
			{
			}
		}

		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x06001610 RID: 5648 RVA: 0x0001C0FC File Offset: 0x0001A2FC
		// (set) Token: 0x06001611 RID: 5649 RVA: 0x0001C118 File Offset: 0x0001A318
		public float positionSpring
		{
			get
			{
				return this.m_PositionSpring;
			}
			set
			{
				this.m_PositionSpring = value;
			}
		}

		// Token: 0x17000506 RID: 1286
		// (get) Token: 0x06001612 RID: 5650 RVA: 0x0001C124 File Offset: 0x0001A324
		// (set) Token: 0x06001613 RID: 5651 RVA: 0x0001C140 File Offset: 0x0001A340
		public float positionDamper
		{
			get
			{
				return this.m_PositionDamper;
			}
			set
			{
				this.m_PositionDamper = value;
			}
		}

		// Token: 0x17000507 RID: 1287
		// (get) Token: 0x06001614 RID: 5652 RVA: 0x0001C14C File Offset: 0x0001A34C
		// (set) Token: 0x06001615 RID: 5653 RVA: 0x0001C168 File Offset: 0x0001A368
		public float maximumForce
		{
			get
			{
				return this.m_MaximumForce;
			}
			set
			{
				this.m_MaximumForce = value;
			}
		}

		// Token: 0x04000370 RID: 880
		private float m_PositionSpring;

		// Token: 0x04000371 RID: 881
		private float m_PositionDamper;

		// Token: 0x04000372 RID: 882
		private float m_MaximumForce;
	}
}
