﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000014 RID: 20
	[RequiredByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public class AsyncOperation : YieldInstruction
	{
		// Token: 0x060001AB RID: 427
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InternalDestroy();

		// Token: 0x060001AC RID: 428 RVA: 0x00004E3C File Offset: 0x0000303C
		~AsyncOperation()
		{
			this.InternalDestroy();
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060001AD RID: 429
		public extern bool isDone { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060001AE RID: 430
		public extern float progress { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060001AF RID: 431
		// (set) Token: 0x060001B0 RID: 432
		public extern int priority { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060001B1 RID: 433
		// (set) Token: 0x060001B2 RID: 434
		public extern bool allowSceneActivation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x04000026 RID: 38
		internal IntPtr m_Ptr;
	}
}
