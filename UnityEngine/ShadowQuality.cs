﻿using System;

namespace UnityEngine
{
	// Token: 0x020002EF RID: 751
	public enum ShadowQuality
	{
		// Token: 0x04000B18 RID: 2840
		Disable,
		// Token: 0x04000B19 RID: 2841
		HardOnly,
		// Token: 0x04000B1A RID: 2842
		All
	}
}
