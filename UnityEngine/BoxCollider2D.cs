﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000163 RID: 355
	public sealed class BoxCollider2D : Collider2D
	{
		// Token: 0x17000615 RID: 1557
		// (get) Token: 0x06001A77 RID: 6775 RVA: 0x000211E0 File Offset: 0x0001F3E0
		// (set) Token: 0x06001A78 RID: 6776 RVA: 0x00021200 File Offset: 0x0001F400
		public Vector2 size
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_size(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_size(ref value);
			}
		}

		// Token: 0x06001A79 RID: 6777
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_size(out Vector2 value);

		// Token: 0x06001A7A RID: 6778
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_size(ref Vector2 value);
	}
}
