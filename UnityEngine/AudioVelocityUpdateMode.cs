﻿using System;

namespace UnityEngine
{
	// Token: 0x020001A1 RID: 417
	public enum AudioVelocityUpdateMode
	{
		// Token: 0x0400046B RID: 1131
		Auto,
		// Token: 0x0400046C RID: 1132
		Fixed,
		// Token: 0x0400046D RID: 1133
		Dynamic
	}
}
