﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001AC RID: 428
	public sealed class AudioEchoFilter : Behaviour
	{
		// Token: 0x17000750 RID: 1872
		// (get) Token: 0x06001DA8 RID: 7592
		// (set) Token: 0x06001DA9 RID: 7593
		public extern float delay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000751 RID: 1873
		// (get) Token: 0x06001DAA RID: 7594
		// (set) Token: 0x06001DAB RID: 7595
		public extern float decayRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000752 RID: 1874
		// (get) Token: 0x06001DAC RID: 7596
		// (set) Token: 0x06001DAD RID: 7597
		public extern float dryMix { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000753 RID: 1875
		// (get) Token: 0x06001DAE RID: 7598
		// (set) Token: 0x06001DAF RID: 7599
		public extern float wetMix { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
