﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200008E RID: 142
	public class Motion : Object
	{
		// Token: 0x17000251 RID: 593
		// (get) Token: 0x06000A3F RID: 2623
		public extern float averageDuration { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x06000A40 RID: 2624
		public extern float averageAngularSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x06000A41 RID: 2625 RVA: 0x0000EF44 File Offset: 0x0000D144
		public Vector3 averageSpeed
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_averageSpeed(out result);
				return result;
			}
		}

		// Token: 0x06000A42 RID: 2626
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_averageSpeed(out Vector3 value);

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x06000A43 RID: 2627
		public extern float apparentSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x06000A44 RID: 2628
		public extern bool isLooping { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x06000A45 RID: 2629
		public extern bool legacy { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x06000A46 RID: 2630
		public extern bool isHumanMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000A47 RID: 2631
		[Obsolete("ValidateIfRetargetable is not supported anymore. Use isHumanMotion instead.", true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool ValidateIfRetargetable(bool val);

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x06000A48 RID: 2632
		[Obsolete("isAnimatorMotion is not supported anymore. Use !legacy instead.", true)]
		public extern bool isAnimatorMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
