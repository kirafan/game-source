﻿using System;

namespace UnityEngine
{
	// Token: 0x02000133 RID: 307
	[Flags]
	[Obsolete("JointDriveMode is no longer supported")]
	public enum JointDriveMode
	{
		// Token: 0x0400035E RID: 862
		[Obsolete("JointDriveMode.None is no longer supported")]
		None = 0,
		// Token: 0x0400035F RID: 863
		[Obsolete("JointDriveMode.Position is no longer supported")]
		Position = 1,
		// Token: 0x04000360 RID: 864
		[Obsolete("JointDriveMode.Velocity is no longer supported")]
		Velocity = 2,
		// Token: 0x04000361 RID: 865
		[Obsolete("JointDriveMode.PositionAndvelocity is no longer supported")]
		PositionAndVelocity = 3
	}
}
