﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x0200006A RID: 106
	public sealed class Handheld
	{
		// Token: 0x0600073A RID: 1850 RVA: 0x00009538 File Offset: 0x00007738
		public static bool PlayFullScreenMovie(string path, [DefaultValue("Color.black")] Color bgColor, [DefaultValue("FullScreenMovieControlMode.Full")] FullScreenMovieControlMode controlMode, [DefaultValue("FullScreenMovieScalingMode.AspectFit")] FullScreenMovieScalingMode scalingMode)
		{
			return Handheld.INTERNAL_CALL_PlayFullScreenMovie(path, ref bgColor, controlMode, scalingMode);
		}

		// Token: 0x0600073B RID: 1851 RVA: 0x00009558 File Offset: 0x00007758
		[ExcludeFromDocs]
		public static bool PlayFullScreenMovie(string path, Color bgColor, FullScreenMovieControlMode controlMode)
		{
			FullScreenMovieScalingMode scalingMode = FullScreenMovieScalingMode.AspectFit;
			return Handheld.INTERNAL_CALL_PlayFullScreenMovie(path, ref bgColor, controlMode, scalingMode);
		}

		// Token: 0x0600073C RID: 1852 RVA: 0x0000957C File Offset: 0x0000777C
		[ExcludeFromDocs]
		public static bool PlayFullScreenMovie(string path, Color bgColor)
		{
			FullScreenMovieScalingMode scalingMode = FullScreenMovieScalingMode.AspectFit;
			FullScreenMovieControlMode controlMode = FullScreenMovieControlMode.Full;
			return Handheld.INTERNAL_CALL_PlayFullScreenMovie(path, ref bgColor, controlMode, scalingMode);
		}

		// Token: 0x0600073D RID: 1853 RVA: 0x000095A0 File Offset: 0x000077A0
		[ExcludeFromDocs]
		public static bool PlayFullScreenMovie(string path)
		{
			FullScreenMovieScalingMode scalingMode = FullScreenMovieScalingMode.AspectFit;
			FullScreenMovieControlMode controlMode = FullScreenMovieControlMode.Full;
			Color black = Color.black;
			return Handheld.INTERNAL_CALL_PlayFullScreenMovie(path, ref black, controlMode, scalingMode);
		}

		// Token: 0x0600073E RID: 1854
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_PlayFullScreenMovie(string path, ref Color bgColor, FullScreenMovieControlMode controlMode, FullScreenMovieScalingMode scalingMode);

		// Token: 0x0600073F RID: 1855
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Vibrate();

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x06000740 RID: 1856
		// (set) Token: 0x06000741 RID: 1857
		[Obsolete("Property Handheld.use32BitDisplayBuffer has been deprecated. Modifying it has no effect, use PlayerSettings instead.")]
		public static extern bool use32BitDisplayBuffer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000742 RID: 1858
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetActivityIndicatorStyleImpl(int style);

		// Token: 0x06000743 RID: 1859 RVA: 0x000095CC File Offset: 0x000077CC
		public static void SetActivityIndicatorStyle(AndroidActivityIndicatorStyle style)
		{
			Handheld.SetActivityIndicatorStyleImpl((int)style);
		}

		// Token: 0x06000744 RID: 1860
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetActivityIndicatorStyle();

		// Token: 0x06000745 RID: 1861
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void StartActivityIndicator();

		// Token: 0x06000746 RID: 1862
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void StopActivityIndicator();

		// Token: 0x06000747 RID: 1863
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ClearShaderCache();
	}
}
