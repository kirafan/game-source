﻿using System;

namespace UnityEngine
{
	// Token: 0x020001DB RID: 475
	internal enum HeadDoF
	{
		// Token: 0x0400052C RID: 1324
		NeckFrontBack,
		// Token: 0x0400052D RID: 1325
		NeckLeftRight,
		// Token: 0x0400052E RID: 1326
		NeckRollLeftRight,
		// Token: 0x0400052F RID: 1327
		HeadFrontBack,
		// Token: 0x04000530 RID: 1328
		HeadLeftRight,
		// Token: 0x04000531 RID: 1329
		HeadRollLeftRight,
		// Token: 0x04000532 RID: 1330
		LeftEyeDownUp,
		// Token: 0x04000533 RID: 1331
		LeftEyeInOut,
		// Token: 0x04000534 RID: 1332
		RightEyeDownUp,
		// Token: 0x04000535 RID: 1333
		RightEyeInOut,
		// Token: 0x04000536 RID: 1334
		JawDownUp,
		// Token: 0x04000537 RID: 1335
		JawLeftRight,
		// Token: 0x04000538 RID: 1336
		LastHeadDoF
	}
}
