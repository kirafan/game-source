﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000173 RID: 371
	public sealed class FrictionJoint2D : AnchoredJoint2D
	{
		// Token: 0x17000641 RID: 1601
		// (get) Token: 0x06001ADD RID: 6877
		// (set) Token: 0x06001ADE RID: 6878
		public extern float maxForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000642 RID: 1602
		// (get) Token: 0x06001ADF RID: 6879
		// (set) Token: 0x06001AE0 RID: 6880
		public extern float maxTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
