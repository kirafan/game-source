﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001CE RID: 462
	[RequiredByNativeCode]
	public struct AnimatorStateInfo
	{
		// Token: 0x06001EC3 RID: 7875 RVA: 0x000232B4 File Offset: 0x000214B4
		public bool IsName(string name)
		{
			int num = Animator.StringToHash(name);
			return num == this.m_FullPath || num == this.m_Name || num == this.m_Path;
		}

		// Token: 0x170007AE RID: 1966
		// (get) Token: 0x06001EC4 RID: 7876 RVA: 0x000232F4 File Offset: 0x000214F4
		public int fullPathHash
		{
			get
			{
				return this.m_FullPath;
			}
		}

		// Token: 0x170007AF RID: 1967
		// (get) Token: 0x06001EC5 RID: 7877 RVA: 0x00023310 File Offset: 0x00021510
		[Obsolete("Use AnimatorStateInfo.fullPathHash instead.")]
		public int nameHash
		{
			get
			{
				return this.m_Path;
			}
		}

		// Token: 0x170007B0 RID: 1968
		// (get) Token: 0x06001EC6 RID: 7878 RVA: 0x0002332C File Offset: 0x0002152C
		public int shortNameHash
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x170007B1 RID: 1969
		// (get) Token: 0x06001EC7 RID: 7879 RVA: 0x00023348 File Offset: 0x00021548
		public float normalizedTime
		{
			get
			{
				return this.m_NormalizedTime;
			}
		}

		// Token: 0x170007B2 RID: 1970
		// (get) Token: 0x06001EC8 RID: 7880 RVA: 0x00023364 File Offset: 0x00021564
		public float length
		{
			get
			{
				return this.m_Length;
			}
		}

		// Token: 0x170007B3 RID: 1971
		// (get) Token: 0x06001EC9 RID: 7881 RVA: 0x00023380 File Offset: 0x00021580
		public float speed
		{
			get
			{
				return this.m_Speed;
			}
		}

		// Token: 0x170007B4 RID: 1972
		// (get) Token: 0x06001ECA RID: 7882 RVA: 0x0002339C File Offset: 0x0002159C
		public float speedMultiplier
		{
			get
			{
				return this.m_SpeedMultiplier;
			}
		}

		// Token: 0x170007B5 RID: 1973
		// (get) Token: 0x06001ECB RID: 7883 RVA: 0x000233B8 File Offset: 0x000215B8
		public int tagHash
		{
			get
			{
				return this.m_Tag;
			}
		}

		// Token: 0x06001ECC RID: 7884 RVA: 0x000233D4 File Offset: 0x000215D4
		public bool IsTag(string tag)
		{
			return Animator.StringToHash(tag) == this.m_Tag;
		}

		// Token: 0x170007B6 RID: 1974
		// (get) Token: 0x06001ECD RID: 7885 RVA: 0x000233F8 File Offset: 0x000215F8
		public bool loop
		{
			get
			{
				return this.m_Loop != 0;
			}
		}

		// Token: 0x040004F6 RID: 1270
		private int m_Name;

		// Token: 0x040004F7 RID: 1271
		private int m_Path;

		// Token: 0x040004F8 RID: 1272
		private int m_FullPath;

		// Token: 0x040004F9 RID: 1273
		private float m_NormalizedTime;

		// Token: 0x040004FA RID: 1274
		private float m_Length;

		// Token: 0x040004FB RID: 1275
		private float m_Speed;

		// Token: 0x040004FC RID: 1276
		private float m_SpeedMultiplier;

		// Token: 0x040004FD RID: 1277
		private int m_Tag;

		// Token: 0x040004FE RID: 1278
		private int m_Loop;
	}
}
