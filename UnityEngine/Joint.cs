﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000145 RID: 325
	public class Joint : Component
	{
		// Token: 0x17000555 RID: 1365
		// (get) Token: 0x06001789 RID: 6025
		// (set) Token: 0x0600178A RID: 6026
		public extern Rigidbody connectedBody { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000556 RID: 1366
		// (get) Token: 0x0600178B RID: 6027 RVA: 0x0001E250 File Offset: 0x0001C450
		// (set) Token: 0x0600178C RID: 6028 RVA: 0x0001E270 File Offset: 0x0001C470
		public Vector3 axis
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_axis(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_axis(ref value);
			}
		}

		// Token: 0x0600178D RID: 6029
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_axis(out Vector3 value);

		// Token: 0x0600178E RID: 6030
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_axis(ref Vector3 value);

		// Token: 0x17000557 RID: 1367
		// (get) Token: 0x0600178F RID: 6031 RVA: 0x0001E27C File Offset: 0x0001C47C
		// (set) Token: 0x06001790 RID: 6032 RVA: 0x0001E29C File Offset: 0x0001C49C
		public Vector3 anchor
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_anchor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_anchor(ref value);
			}
		}

		// Token: 0x06001791 RID: 6033
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_anchor(out Vector3 value);

		// Token: 0x06001792 RID: 6034
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_anchor(ref Vector3 value);

		// Token: 0x17000558 RID: 1368
		// (get) Token: 0x06001793 RID: 6035 RVA: 0x0001E2A8 File Offset: 0x0001C4A8
		// (set) Token: 0x06001794 RID: 6036 RVA: 0x0001E2C8 File Offset: 0x0001C4C8
		public Vector3 connectedAnchor
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_connectedAnchor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_connectedAnchor(ref value);
			}
		}

		// Token: 0x06001795 RID: 6037
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_connectedAnchor(out Vector3 value);

		// Token: 0x06001796 RID: 6038
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_connectedAnchor(ref Vector3 value);

		// Token: 0x17000559 RID: 1369
		// (get) Token: 0x06001797 RID: 6039
		// (set) Token: 0x06001798 RID: 6040
		public extern bool autoConfigureConnectedAnchor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700055A RID: 1370
		// (get) Token: 0x06001799 RID: 6041
		// (set) Token: 0x0600179A RID: 6042
		public extern float breakForce { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700055B RID: 1371
		// (get) Token: 0x0600179B RID: 6043
		// (set) Token: 0x0600179C RID: 6044
		public extern float breakTorque { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700055C RID: 1372
		// (get) Token: 0x0600179D RID: 6045
		// (set) Token: 0x0600179E RID: 6046
		public extern bool enableCollision { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700055D RID: 1373
		// (get) Token: 0x0600179F RID: 6047
		// (set) Token: 0x060017A0 RID: 6048
		public extern bool enablePreprocessing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700055E RID: 1374
		// (get) Token: 0x060017A1 RID: 6049 RVA: 0x0001E2D4 File Offset: 0x0001C4D4
		public Vector3 currentForce
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_currentForce(out result);
				return result;
			}
		}

		// Token: 0x060017A2 RID: 6050
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_currentForce(out Vector3 value);

		// Token: 0x1700055F RID: 1375
		// (get) Token: 0x060017A3 RID: 6051 RVA: 0x0001E2F4 File Offset: 0x0001C4F4
		public Vector3 currentTorque
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_currentTorque(out result);
				return result;
			}
		}

		// Token: 0x060017A4 RID: 6052
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_currentTorque(out Vector3 value);
	}
}
