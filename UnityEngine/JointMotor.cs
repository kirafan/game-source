﻿using System;

namespace UnityEngine
{
	// Token: 0x0200013A RID: 314
	public struct JointMotor
	{
		// Token: 0x17000508 RID: 1288
		// (get) Token: 0x06001616 RID: 5654 RVA: 0x0001C174 File Offset: 0x0001A374
		// (set) Token: 0x06001617 RID: 5655 RVA: 0x0001C190 File Offset: 0x0001A390
		public float targetVelocity
		{
			get
			{
				return this.m_TargetVelocity;
			}
			set
			{
				this.m_TargetVelocity = value;
			}
		}

		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x06001618 RID: 5656 RVA: 0x0001C19C File Offset: 0x0001A39C
		// (set) Token: 0x06001619 RID: 5657 RVA: 0x0001C1B8 File Offset: 0x0001A3B8
		public float force
		{
			get
			{
				return this.m_Force;
			}
			set
			{
				this.m_Force = value;
			}
		}

		// Token: 0x1700050A RID: 1290
		// (get) Token: 0x0600161A RID: 5658 RVA: 0x0001C1C4 File Offset: 0x0001A3C4
		// (set) Token: 0x0600161B RID: 5659 RVA: 0x0001C1E4 File Offset: 0x0001A3E4
		public bool freeSpin
		{
			get
			{
				return this.m_FreeSpin == 1;
			}
			set
			{
				this.m_FreeSpin = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x04000377 RID: 887
		private float m_TargetVelocity;

		// Token: 0x04000378 RID: 888
		private float m_Force;

		// Token: 0x04000379 RID: 889
		private int m_FreeSpin;
	}
}
