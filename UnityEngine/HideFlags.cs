﻿using System;

namespace UnityEngine
{
	// Token: 0x020000D7 RID: 215
	[Flags]
	public enum HideFlags
	{
		// Token: 0x04000214 RID: 532
		None = 0,
		// Token: 0x04000215 RID: 533
		HideInHierarchy = 1,
		// Token: 0x04000216 RID: 534
		HideInInspector = 2,
		// Token: 0x04000217 RID: 535
		DontSaveInEditor = 4,
		// Token: 0x04000218 RID: 536
		NotEditable = 8,
		// Token: 0x04000219 RID: 537
		DontSaveInBuild = 16,
		// Token: 0x0400021A RID: 538
		DontUnloadUnusedAsset = 32,
		// Token: 0x0400021B RID: 539
		DontSave = 52,
		// Token: 0x0400021C RID: 540
		HideAndDontSave = 61
	}
}
