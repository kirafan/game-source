﻿using System;

namespace UnityEngine
{
	// Token: 0x0200014E RID: 334
	public enum CollisionDetectionMode
	{
		// Token: 0x040003B7 RID: 951
		Discrete,
		// Token: 0x040003B8 RID: 952
		Continuous,
		// Token: 0x040003B9 RID: 953
		ContinuousDynamic
	}
}
