﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000078 RID: 120
	[UsedByNativeCode]
	public struct LayerMask
	{
		// Token: 0x060007F6 RID: 2038 RVA: 0x0000A074 File Offset: 0x00008274
		public static implicit operator int(LayerMask mask)
		{
			return mask.m_Mask;
		}

		// Token: 0x060007F7 RID: 2039 RVA: 0x0000A090 File Offset: 0x00008290
		public static implicit operator LayerMask(int intVal)
		{
			LayerMask result;
			result.m_Mask = intVal;
			return result;
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x060007F8 RID: 2040 RVA: 0x0000A0B0 File Offset: 0x000082B0
		// (set) Token: 0x060007F9 RID: 2041 RVA: 0x0000A0CC File Offset: 0x000082CC
		public int value
		{
			get
			{
				return this.m_Mask;
			}
			set
			{
				this.m_Mask = value;
			}
		}

		// Token: 0x060007FA RID: 2042
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string LayerToName(int layer);

		// Token: 0x060007FB RID: 2043
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int NameToLayer(string layerName);

		// Token: 0x060007FC RID: 2044 RVA: 0x0000A0D8 File Offset: 0x000082D8
		public static int GetMask(params string[] layerNames)
		{
			if (layerNames == null)
			{
				throw new ArgumentNullException("layerNames");
			}
			int num = 0;
			foreach (string layerName in layerNames)
			{
				int num2 = LayerMask.NameToLayer(layerName);
				if (num2 != -1)
				{
					num |= 1 << num2;
				}
			}
			return num;
		}

		// Token: 0x040000E3 RID: 227
		private int m_Mask;
	}
}
