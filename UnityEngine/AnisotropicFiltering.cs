﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F4 RID: 756
	public enum AnisotropicFiltering
	{
		// Token: 0x04000B33 RID: 2867
		Disable,
		// Token: 0x04000B34 RID: 2868
		Enable,
		// Token: 0x04000B35 RID: 2869
		ForceEnable
	}
}
