﻿using System;

namespace UnityEngine
{
	// Token: 0x020001FF RID: 511
	public enum TextAnchor
	{
		// Token: 0x04000616 RID: 1558
		UpperLeft,
		// Token: 0x04000617 RID: 1559
		UpperCenter,
		// Token: 0x04000618 RID: 1560
		UpperRight,
		// Token: 0x04000619 RID: 1561
		MiddleLeft,
		// Token: 0x0400061A RID: 1562
		MiddleCenter,
		// Token: 0x0400061B RID: 1563
		MiddleRight,
		// Token: 0x0400061C RID: 1564
		LowerLeft,
		// Token: 0x0400061D RID: 1565
		LowerCenter,
		// Token: 0x0400061E RID: 1566
		LowerRight
	}
}
