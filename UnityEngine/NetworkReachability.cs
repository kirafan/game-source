﻿using System;

namespace UnityEngine
{
	// Token: 0x02000009 RID: 9
	public enum NetworkReachability
	{
		// Token: 0x04000010 RID: 16
		NotReachable,
		// Token: 0x04000011 RID: 17
		ReachableViaCarrierDataNetwork,
		// Token: 0x04000012 RID: 18
		ReachableViaLocalAreaNetwork
	}
}
