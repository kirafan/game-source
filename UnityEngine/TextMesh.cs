﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000203 RID: 515
	public sealed class TextMesh : Component
	{
		// Token: 0x170008A4 RID: 2212
		// (get) Token: 0x060022C7 RID: 8903
		// (set) Token: 0x060022C8 RID: 8904
		public extern string text { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008A5 RID: 2213
		// (get) Token: 0x060022C9 RID: 8905
		// (set) Token: 0x060022CA RID: 8906
		public extern Font font { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008A6 RID: 2214
		// (get) Token: 0x060022CB RID: 8907
		// (set) Token: 0x060022CC RID: 8908
		public extern int fontSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008A7 RID: 2215
		// (get) Token: 0x060022CD RID: 8909
		// (set) Token: 0x060022CE RID: 8910
		public extern FontStyle fontStyle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008A8 RID: 2216
		// (get) Token: 0x060022CF RID: 8911
		// (set) Token: 0x060022D0 RID: 8912
		public extern float offsetZ { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008A9 RID: 2217
		// (get) Token: 0x060022D1 RID: 8913
		// (set) Token: 0x060022D2 RID: 8914
		public extern TextAlignment alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008AA RID: 2218
		// (get) Token: 0x060022D3 RID: 8915
		// (set) Token: 0x060022D4 RID: 8916
		public extern TextAnchor anchor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008AB RID: 2219
		// (get) Token: 0x060022D5 RID: 8917
		// (set) Token: 0x060022D6 RID: 8918
		public extern float characterSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008AC RID: 2220
		// (get) Token: 0x060022D7 RID: 8919
		// (set) Token: 0x060022D8 RID: 8920
		public extern float lineSpacing { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008AD RID: 2221
		// (get) Token: 0x060022D9 RID: 8921
		// (set) Token: 0x060022DA RID: 8922
		public extern float tabSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008AE RID: 2222
		// (get) Token: 0x060022DB RID: 8923
		// (set) Token: 0x060022DC RID: 8924
		public extern bool richText { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008AF RID: 2223
		// (get) Token: 0x060022DD RID: 8925 RVA: 0x0002849C File Offset: 0x0002669C
		// (set) Token: 0x060022DE RID: 8926 RVA: 0x000284BC File Offset: 0x000266BC
		public Color color
		{
			get
			{
				Color result;
				this.INTERNAL_get_color(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_color(ref value);
			}
		}

		// Token: 0x060022DF RID: 8927
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_color(out Color value);

		// Token: 0x060022E0 RID: 8928
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_color(ref Color value);
	}
}
