﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000035 RID: 53
	public sealed class Debug
	{
		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x0600039E RID: 926 RVA: 0x00006524 File Offset: 0x00004724
		public static ILogger logger
		{
			get
			{
				return Debug.s_Logger;
			}
		}

		// Token: 0x0600039F RID: 927 RVA: 0x00006540 File Offset: 0x00004740
		public static void DrawLine(Vector3 start, Vector3 end, [DefaultValue("Color.white")] Color color, [DefaultValue("0.0f")] float duration, [DefaultValue("true")] bool depthTest)
		{
			Debug.INTERNAL_CALL_DrawLine(ref start, ref end, ref color, duration, depthTest);
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x00006554 File Offset: 0x00004754
		[ExcludeFromDocs]
		public static void DrawLine(Vector3 start, Vector3 end, Color color, float duration)
		{
			bool depthTest = true;
			Debug.INTERNAL_CALL_DrawLine(ref start, ref end, ref color, duration, depthTest);
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x00006574 File Offset: 0x00004774
		[ExcludeFromDocs]
		public static void DrawLine(Vector3 start, Vector3 end, Color color)
		{
			bool depthTest = true;
			float duration = 0f;
			Debug.INTERNAL_CALL_DrawLine(ref start, ref end, ref color, duration, depthTest);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00006598 File Offset: 0x00004798
		[ExcludeFromDocs]
		public static void DrawLine(Vector3 start, Vector3 end)
		{
			bool depthTest = true;
			float duration = 0f;
			Color white = Color.white;
			Debug.INTERNAL_CALL_DrawLine(ref start, ref end, ref white, duration, depthTest);
		}

		// Token: 0x060003A3 RID: 931
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_DrawLine(ref Vector3 start, ref Vector3 end, ref Color color, float duration, bool depthTest);

		// Token: 0x060003A4 RID: 932 RVA: 0x000065C4 File Offset: 0x000047C4
		[ExcludeFromDocs]
		public static void DrawRay(Vector3 start, Vector3 dir, Color color, float duration)
		{
			bool depthTest = true;
			Debug.DrawRay(start, dir, color, duration, depthTest);
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x000065E0 File Offset: 0x000047E0
		[ExcludeFromDocs]
		public static void DrawRay(Vector3 start, Vector3 dir, Color color)
		{
			bool depthTest = true;
			float duration = 0f;
			Debug.DrawRay(start, dir, color, duration, depthTest);
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x00006600 File Offset: 0x00004800
		[ExcludeFromDocs]
		public static void DrawRay(Vector3 start, Vector3 dir)
		{
			bool depthTest = true;
			float duration = 0f;
			Color white = Color.white;
			Debug.DrawRay(start, dir, white, duration, depthTest);
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x00006628 File Offset: 0x00004828
		public static void DrawRay(Vector3 start, Vector3 dir, [DefaultValue("Color.white")] Color color, [DefaultValue("0.0f")] float duration, [DefaultValue("true")] bool depthTest)
		{
			Debug.DrawLine(start, start + dir, color, duration, depthTest);
		}

		// Token: 0x060003A8 RID: 936
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Break();

		// Token: 0x060003A9 RID: 937
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DebugBreak();

		// Token: 0x060003AA RID: 938 RVA: 0x0000663C File Offset: 0x0000483C
		public static void Log(object message)
		{
			Debug.logger.Log(LogType.Log, message);
		}

		// Token: 0x060003AB RID: 939 RVA: 0x0000664C File Offset: 0x0000484C
		public static void Log(object message, Object context)
		{
			Debug.logger.Log(LogType.Log, message, context);
		}

		// Token: 0x060003AC RID: 940 RVA: 0x0000665C File Offset: 0x0000485C
		public static void LogFormat(string format, params object[] args)
		{
			Debug.logger.LogFormat(LogType.Log, format, args);
		}

		// Token: 0x060003AD RID: 941 RVA: 0x0000666C File Offset: 0x0000486C
		public static void LogFormat(Object context, string format, params object[] args)
		{
			Debug.logger.LogFormat(LogType.Log, context, format, args);
		}

		// Token: 0x060003AE RID: 942 RVA: 0x00006680 File Offset: 0x00004880
		public static void LogError(object message)
		{
			Debug.logger.Log(LogType.Error, message);
		}

		// Token: 0x060003AF RID: 943 RVA: 0x00006690 File Offset: 0x00004890
		public static void LogError(object message, Object context)
		{
			Debug.logger.Log(LogType.Error, message, context);
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x000066A0 File Offset: 0x000048A0
		public static void LogErrorFormat(string format, params object[] args)
		{
			Debug.logger.LogFormat(LogType.Error, format, args);
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x000066B0 File Offset: 0x000048B0
		public static void LogErrorFormat(Object context, string format, params object[] args)
		{
			Debug.logger.LogFormat(LogType.Error, context, format, args);
		}

		// Token: 0x060003B2 RID: 946
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ClearDeveloperConsole();

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x060003B3 RID: 947
		// (set) Token: 0x060003B4 RID: 948
		public static extern bool developerConsoleVisible { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060003B5 RID: 949 RVA: 0x000066C4 File Offset: 0x000048C4
		public static void LogException(Exception exception)
		{
			Debug.logger.LogException(exception, null);
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x000066D4 File Offset: 0x000048D4
		public static void LogException(Exception exception, Object context)
		{
			Debug.logger.LogException(exception, context);
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x000066E4 File Offset: 0x000048E4
		public static void LogWarning(object message)
		{
			Debug.logger.Log(LogType.Warning, message);
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x000066F4 File Offset: 0x000048F4
		public static void LogWarning(object message, Object context)
		{
			Debug.logger.Log(LogType.Warning, message, context);
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x00006704 File Offset: 0x00004904
		public static void LogWarningFormat(string format, params object[] args)
		{
			Debug.logger.LogFormat(LogType.Warning, format, args);
		}

		// Token: 0x060003BA RID: 954 RVA: 0x00006714 File Offset: 0x00004914
		public static void LogWarningFormat(Object context, string format, params object[] args)
		{
			Debug.logger.LogFormat(LogType.Warning, context, format, args);
		}

		// Token: 0x060003BB RID: 955 RVA: 0x00006728 File Offset: 0x00004928
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition)
		{
			if (!condition)
			{
				Debug.logger.Log(LogType.Assert, "Assertion failed");
			}
		}

		// Token: 0x060003BC RID: 956 RVA: 0x00006744 File Offset: 0x00004944
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, Object context)
		{
			if (!condition)
			{
				Debug.logger.Log(LogType.Assert, "Assertion failed", context);
			}
		}

		// Token: 0x060003BD RID: 957 RVA: 0x00006760 File Offset: 0x00004960
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, object message)
		{
			if (!condition)
			{
				Debug.logger.Log(LogType.Assert, message);
			}
		}

		// Token: 0x060003BE RID: 958 RVA: 0x00006778 File Offset: 0x00004978
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, string message)
		{
			if (!condition)
			{
				Debug.logger.Log(LogType.Assert, message);
			}
		}

		// Token: 0x060003BF RID: 959 RVA: 0x00006790 File Offset: 0x00004990
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, object message, Object context)
		{
			if (!condition)
			{
				Debug.logger.Log(LogType.Assert, message, context);
			}
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x000067A8 File Offset: 0x000049A8
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, string message, Object context)
		{
			if (!condition)
			{
				Debug.logger.Log(LogType.Assert, message, context);
			}
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x000067C0 File Offset: 0x000049C0
		[Conditional("UNITY_ASSERTIONS")]
		public static void AssertFormat(bool condition, string format, params object[] args)
		{
			if (!condition)
			{
				Debug.logger.LogFormat(LogType.Assert, format, args);
			}
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x000067D8 File Offset: 0x000049D8
		[Conditional("UNITY_ASSERTIONS")]
		public static void AssertFormat(bool condition, Object context, string format, params object[] args)
		{
			if (!condition)
			{
				Debug.logger.LogFormat(LogType.Assert, context, format, args);
			}
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x000067F0 File Offset: 0x000049F0
		[Conditional("UNITY_ASSERTIONS")]
		public static void LogAssertion(object message)
		{
			Debug.logger.Log(LogType.Assert, message);
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x00006800 File Offset: 0x00004A00
		[Conditional("UNITY_ASSERTIONS")]
		public static void LogAssertion(object message, Object context)
		{
			Debug.logger.Log(LogType.Assert, message, context);
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x00006810 File Offset: 0x00004A10
		[Conditional("UNITY_ASSERTIONS")]
		public static void LogAssertionFormat(string format, params object[] args)
		{
			Debug.logger.LogFormat(LogType.Assert, format, args);
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x00006820 File Offset: 0x00004A20
		[Conditional("UNITY_ASSERTIONS")]
		public static void LogAssertionFormat(Object context, string format, params object[] args)
		{
			Debug.logger.LogFormat(LogType.Assert, context, format, args);
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x060003C7 RID: 967
		public static extern bool isDebugBuild { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060003C8 RID: 968
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void OpenConsoleFile();

		// Token: 0x060003C9 RID: 969
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void GetDiagnosticSwitches(List<DiagnosticSwitch> results);

		// Token: 0x060003CA RID: 970
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetDiagnosticSwitch(string name, object value, bool setPersistent);

		// Token: 0x060003CB RID: 971 RVA: 0x00006834 File Offset: 0x00004A34
		[Obsolete("Assert(bool, string, params object[]) is obsolete. Use AssertFormat(bool, string, params object[]) (UnityUpgradable) -> AssertFormat(*)", true)]
		[Conditional("UNITY_ASSERTIONS")]
		public static void Assert(bool condition, string format, params object[] args)
		{
			if (!condition)
			{
				Debug.logger.LogFormat(LogType.Assert, format, args);
			}
		}

		// Token: 0x04000054 RID: 84
		internal static ILogger s_Logger = new Logger(new DebugLogHandler());
	}
}
