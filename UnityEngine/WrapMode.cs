﻿using System;

namespace UnityEngine
{
	// Token: 0x020001B9 RID: 441
	public enum WrapMode
	{
		// Token: 0x040004A5 RID: 1189
		Once = 1,
		// Token: 0x040004A6 RID: 1190
		Loop,
		// Token: 0x040004A7 RID: 1191
		PingPong = 4,
		// Token: 0x040004A8 RID: 1192
		Default = 0,
		// Token: 0x040004A9 RID: 1193
		ClampForever = 8,
		// Token: 0x040004AA RID: 1194
		Clamp = 1
	}
}
