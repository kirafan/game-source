﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C2 RID: 194
	public enum ProceduralProcessorUsage
	{
		// Token: 0x040001D8 RID: 472
		Unsupported,
		// Token: 0x040001D9 RID: 473
		One,
		// Token: 0x040001DA RID: 474
		Half,
		// Token: 0x040001DB RID: 475
		All
	}
}
