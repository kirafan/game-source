﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000098 RID: 152
	[RequiredByNativeCode]
	public struct NetworkViewID
	{
		// Token: 0x1700025F RID: 607
		// (get) Token: 0x06000A5E RID: 2654 RVA: 0x0000F164 File Offset: 0x0000D364
		public static NetworkViewID unassigned
		{
			get
			{
				NetworkViewID result;
				NetworkViewID.INTERNAL_get_unassigned(out result);
				return result;
			}
		}

		// Token: 0x06000A5F RID: 2655
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_unassigned(out NetworkViewID value);

		// Token: 0x06000A60 RID: 2656 RVA: 0x0000F184 File Offset: 0x0000D384
		internal static bool Internal_IsMine(NetworkViewID value)
		{
			return NetworkViewID.INTERNAL_CALL_Internal_IsMine(ref value);
		}

		// Token: 0x06000A61 RID: 2657
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Internal_IsMine(ref NetworkViewID value);

		// Token: 0x06000A62 RID: 2658 RVA: 0x0000F1A0 File Offset: 0x0000D3A0
		internal static void Internal_GetOwner(NetworkViewID value, out NetworkPlayer player)
		{
			NetworkViewID.INTERNAL_CALL_Internal_GetOwner(ref value, out player);
		}

		// Token: 0x06000A63 RID: 2659
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_GetOwner(ref NetworkViewID value, out NetworkPlayer player);

		// Token: 0x06000A64 RID: 2660 RVA: 0x0000F1AC File Offset: 0x0000D3AC
		internal static string Internal_GetString(NetworkViewID value)
		{
			return NetworkViewID.INTERNAL_CALL_Internal_GetString(ref value);
		}

		// Token: 0x06000A65 RID: 2661
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string INTERNAL_CALL_Internal_GetString(ref NetworkViewID value);

		// Token: 0x06000A66 RID: 2662 RVA: 0x0000F1C8 File Offset: 0x0000D3C8
		internal static bool Internal_Compare(NetworkViewID lhs, NetworkViewID rhs)
		{
			return NetworkViewID.INTERNAL_CALL_Internal_Compare(ref lhs, ref rhs);
		}

		// Token: 0x06000A67 RID: 2663
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Internal_Compare(ref NetworkViewID lhs, ref NetworkViewID rhs);

		// Token: 0x06000A68 RID: 2664 RVA: 0x0000F1E8 File Offset: 0x0000D3E8
		public static bool operator ==(NetworkViewID lhs, NetworkViewID rhs)
		{
			return NetworkViewID.Internal_Compare(lhs, rhs);
		}

		// Token: 0x06000A69 RID: 2665 RVA: 0x0000F204 File Offset: 0x0000D404
		public static bool operator !=(NetworkViewID lhs, NetworkViewID rhs)
		{
			return !NetworkViewID.Internal_Compare(lhs, rhs);
		}

		// Token: 0x06000A6A RID: 2666 RVA: 0x0000F224 File Offset: 0x0000D424
		public override int GetHashCode()
		{
			return this.a ^ this.b ^ this.c;
		}

		// Token: 0x06000A6B RID: 2667 RVA: 0x0000F250 File Offset: 0x0000D450
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is NetworkViewID))
			{
				result = false;
			}
			else
			{
				NetworkViewID rhs = (NetworkViewID)other;
				result = NetworkViewID.Internal_Compare(this, rhs);
			}
			return result;
		}

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x06000A6C RID: 2668 RVA: 0x0000F28C File Offset: 0x0000D48C
		public bool isMine
		{
			get
			{
				return NetworkViewID.Internal_IsMine(this);
			}
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x06000A6D RID: 2669 RVA: 0x0000F2AC File Offset: 0x0000D4AC
		public NetworkPlayer owner
		{
			get
			{
				NetworkPlayer result;
				NetworkViewID.Internal_GetOwner(this, out result);
				return result;
			}
		}

		// Token: 0x06000A6E RID: 2670 RVA: 0x0000F2D0 File Offset: 0x0000D4D0
		public override string ToString()
		{
			return NetworkViewID.Internal_GetString(this);
		}

		// Token: 0x04000164 RID: 356
		private int a;

		// Token: 0x04000165 RID: 357
		private int b;

		// Token: 0x04000166 RID: 358
		private int c;
	}
}
