﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001AF RID: 431
	public sealed class Microphone
	{
		// Token: 0x06001DE3 RID: 7651
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern AudioClip Start(string deviceName, bool loop, int lengthSec, int frequency);

		// Token: 0x06001DE4 RID: 7652
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void End(string deviceName);

		// Token: 0x1700076C RID: 1900
		// (get) Token: 0x06001DE5 RID: 7653
		public static extern string[] devices { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001DE6 RID: 7654
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsRecording(string deviceName);

		// Token: 0x06001DE7 RID: 7655
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetPosition(string deviceName);

		// Token: 0x06001DE8 RID: 7656
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void GetDeviceCaps(string deviceName, out int minFreq, out int maxFreq);
	}
}
