﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200020F RID: 527
	public sealed class CanvasGroup : Component, ICanvasRaycastFilter
	{
		// Token: 0x170008DB RID: 2267
		// (get) Token: 0x06002369 RID: 9065
		// (set) Token: 0x0600236A RID: 9066
		public extern float alpha { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008DC RID: 2268
		// (get) Token: 0x0600236B RID: 9067
		// (set) Token: 0x0600236C RID: 9068
		public extern bool interactable { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008DD RID: 2269
		// (get) Token: 0x0600236D RID: 9069
		// (set) Token: 0x0600236E RID: 9070
		public extern bool blocksRaycasts { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008DE RID: 2270
		// (get) Token: 0x0600236F RID: 9071
		// (set) Token: 0x06002370 RID: 9072
		public extern bool ignoreParentGroups { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002371 RID: 9073 RVA: 0x00029350 File Offset: 0x00027550
		public bool IsRaycastLocationValid(Vector2 sp, Camera eventCamera)
		{
			return this.blocksRaycasts;
		}
	}
}
