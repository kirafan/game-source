﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020002E0 RID: 736
	[UsedByNativeCode]
	public struct Resolution
	{
		// Token: 0x17000A80 RID: 2688
		// (get) Token: 0x06002C95 RID: 11413 RVA: 0x000464C8 File Offset: 0x000446C8
		// (set) Token: 0x06002C96 RID: 11414 RVA: 0x000464E4 File Offset: 0x000446E4
		public int width
		{
			get
			{
				return this.m_Width;
			}
			set
			{
				this.m_Width = value;
			}
		}

		// Token: 0x17000A81 RID: 2689
		// (get) Token: 0x06002C97 RID: 11415 RVA: 0x000464F0 File Offset: 0x000446F0
		// (set) Token: 0x06002C98 RID: 11416 RVA: 0x0004650C File Offset: 0x0004470C
		public int height
		{
			get
			{
				return this.m_Height;
			}
			set
			{
				this.m_Height = value;
			}
		}

		// Token: 0x17000A82 RID: 2690
		// (get) Token: 0x06002C99 RID: 11417 RVA: 0x00046518 File Offset: 0x00044718
		// (set) Token: 0x06002C9A RID: 11418 RVA: 0x00046534 File Offset: 0x00044734
		public int refreshRate
		{
			get
			{
				return this.m_RefreshRate;
			}
			set
			{
				this.m_RefreshRate = value;
			}
		}

		// Token: 0x06002C9B RID: 11419 RVA: 0x00046540 File Offset: 0x00044740
		public override string ToString()
		{
			return UnityString.Format("{0} x {1} @ {2}Hz", new object[]
			{
				this.m_Width,
				this.m_Height,
				this.m_RefreshRate
			});
		}

		// Token: 0x04000ACE RID: 2766
		private int m_Width;

		// Token: 0x04000ACF RID: 2767
		private int m_Height;

		// Token: 0x04000AD0 RID: 2768
		private int m_RefreshRate;
	}
}
