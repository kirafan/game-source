﻿using System;

namespace UnityEngine
{
	// Token: 0x020002AF RID: 687
	public sealed class AndroidJavaException : Exception
	{
		// Token: 0x06002BA0 RID: 11168 RVA: 0x00041C60 File Offset: 0x0003FE60
		internal AndroidJavaException(string message, string javaStackTrace) : base(message)
		{
			this.mJavaStackTrace = javaStackTrace;
		}

		// Token: 0x17000A68 RID: 2664
		// (get) Token: 0x06002BA1 RID: 11169 RVA: 0x00041C74 File Offset: 0x0003FE74
		public override string StackTrace
		{
			get
			{
				return this.mJavaStackTrace + base.StackTrace;
			}
		}

		// Token: 0x04000A2B RID: 2603
		private string mJavaStackTrace;
	}
}
