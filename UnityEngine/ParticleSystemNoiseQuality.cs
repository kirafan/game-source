﻿using System;

namespace UnityEngine
{
	// Token: 0x0200010B RID: 267
	public enum ParticleSystemNoiseQuality
	{
		// Token: 0x040002F3 RID: 755
		Low,
		// Token: 0x040002F4 RID: 756
		Medium,
		// Token: 0x040002F5 RID: 757
		High
	}
}
