﻿using System;

namespace UnityEngine
{
	// Token: 0x020001CA RID: 458
	public enum AnimatorRecorderMode
	{
		// Token: 0x040004E9 RID: 1257
		Offline,
		// Token: 0x040004EA RID: 1258
		Playback,
		// Token: 0x040004EB RID: 1259
		Record
	}
}
