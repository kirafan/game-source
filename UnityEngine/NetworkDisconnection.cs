﻿using System;

namespace UnityEngine
{
	// Token: 0x02000092 RID: 146
	public enum NetworkDisconnection
	{
		// Token: 0x0400014E RID: 334
		LostConnection = 20,
		// Token: 0x0400014F RID: 335
		Disconnected = 19
	}
}
