﻿using System;

namespace UnityEngine
{
	// Token: 0x0200015A RID: 346
	public enum RigidbodySleepMode2D
	{
		// Token: 0x040003CF RID: 975
		NeverSleep,
		// Token: 0x040003D0 RID: 976
		StartAwake,
		// Token: 0x040003D1 RID: 977
		StartAsleep
	}
}
