﻿using System;

namespace UnityEngine
{
	// Token: 0x020002FA RID: 762
	public enum FilterMode
	{
		// Token: 0x04000B52 RID: 2898
		Point,
		// Token: 0x04000B53 RID: 2899
		Bilinear,
		// Token: 0x04000B54 RID: 2900
		Trilinear
	}
}
