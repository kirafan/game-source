﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000192 RID: 402
	[MovedFrom("UnityEngine")]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class NavMeshPath
	{
		// Token: 0x06001CB4 RID: 7348
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern NavMeshPath();

		// Token: 0x06001CB5 RID: 7349
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void DestroyNavMeshPath();

		// Token: 0x06001CB6 RID: 7350 RVA: 0x00022160 File Offset: 0x00020360
		~NavMeshPath()
		{
			this.DestroyNavMeshPath();
			this.m_Ptr = IntPtr.Zero;
		}

		// Token: 0x06001CB7 RID: 7351
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetCornersNonAlloc(Vector3[] results);

		// Token: 0x06001CB8 RID: 7352
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Vector3[] CalculateCornersInternal();

		// Token: 0x06001CB9 RID: 7353
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ClearCornersInternal();

		// Token: 0x06001CBA RID: 7354 RVA: 0x0002219C File Offset: 0x0002039C
		public void ClearCorners()
		{
			this.ClearCornersInternal();
			this.m_corners = null;
		}

		// Token: 0x06001CBB RID: 7355 RVA: 0x000221AC File Offset: 0x000203AC
		private void CalculateCorners()
		{
			if (this.m_corners == null)
			{
				this.m_corners = this.CalculateCornersInternal();
			}
		}

		// Token: 0x170006F7 RID: 1783
		// (get) Token: 0x06001CBC RID: 7356 RVA: 0x000221C8 File Offset: 0x000203C8
		public Vector3[] corners
		{
			get
			{
				this.CalculateCorners();
				return this.m_corners;
			}
		}

		// Token: 0x170006F8 RID: 1784
		// (get) Token: 0x06001CBD RID: 7357
		public extern NavMeshPathStatus status { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0400042B RID: 1067
		internal IntPtr m_Ptr;

		// Token: 0x0400042C RID: 1068
		internal Vector3[] m_corners;
	}
}
