﻿using System;
using UnityEngine.Scripting;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x0200018B RID: 395
	[UsedByNativeCode]
	[MovedFrom("UnityEngine")]
	public struct NavMeshTriangulation
	{
		// Token: 0x170006CA RID: 1738
		// (get) Token: 0x06001C20 RID: 7200 RVA: 0x00021D70 File Offset: 0x0001FF70
		[Obsolete("Use areas instead.")]
		public int[] layers
		{
			get
			{
				return this.areas;
			}
		}

		// Token: 0x0400041A RID: 1050
		public Vector3[] vertices;

		// Token: 0x0400041B RID: 1051
		public int[] indices;

		// Token: 0x0400041C RID: 1052
		public int[] areas;
	}
}
