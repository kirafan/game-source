﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x0200018C RID: 396
	[MovedFrom("UnityEngine")]
	public sealed class NavMesh
	{
		// Token: 0x06001C22 RID: 7202 RVA: 0x00021D94 File Offset: 0x0001FF94
		public static bool Raycast(Vector3 sourcePosition, Vector3 targetPosition, out NavMeshHit hit, int areaMask)
		{
			return NavMesh.INTERNAL_CALL_Raycast(ref sourcePosition, ref targetPosition, out hit, areaMask);
		}

		// Token: 0x06001C23 RID: 7203
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Raycast(ref Vector3 sourcePosition, ref Vector3 targetPosition, out NavMeshHit hit, int areaMask);

		// Token: 0x06001C24 RID: 7204 RVA: 0x00021DB4 File Offset: 0x0001FFB4
		public static bool CalculatePath(Vector3 sourcePosition, Vector3 targetPosition, int areaMask, NavMeshPath path)
		{
			path.ClearCorners();
			return NavMesh.CalculatePathInternal(sourcePosition, targetPosition, areaMask, path);
		}

		// Token: 0x06001C25 RID: 7205 RVA: 0x00021DD8 File Offset: 0x0001FFD8
		internal static bool CalculatePathInternal(Vector3 sourcePosition, Vector3 targetPosition, int areaMask, NavMeshPath path)
		{
			return NavMesh.INTERNAL_CALL_CalculatePathInternal(ref sourcePosition, ref targetPosition, areaMask, path);
		}

		// Token: 0x06001C26 RID: 7206
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_CalculatePathInternal(ref Vector3 sourcePosition, ref Vector3 targetPosition, int areaMask, NavMeshPath path);

		// Token: 0x06001C27 RID: 7207 RVA: 0x00021DF8 File Offset: 0x0001FFF8
		public static bool FindClosestEdge(Vector3 sourcePosition, out NavMeshHit hit, int areaMask)
		{
			return NavMesh.INTERNAL_CALL_FindClosestEdge(ref sourcePosition, out hit, areaMask);
		}

		// Token: 0x06001C28 RID: 7208
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_FindClosestEdge(ref Vector3 sourcePosition, out NavMeshHit hit, int areaMask);

		// Token: 0x06001C29 RID: 7209 RVA: 0x00021E18 File Offset: 0x00020018
		public static bool SamplePosition(Vector3 sourcePosition, out NavMeshHit hit, float maxDistance, int areaMask)
		{
			return NavMesh.INTERNAL_CALL_SamplePosition(ref sourcePosition, out hit, maxDistance, areaMask);
		}

		// Token: 0x06001C2A RID: 7210
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_SamplePosition(ref Vector3 sourcePosition, out NavMeshHit hit, float maxDistance, int areaMask);

		// Token: 0x06001C2B RID: 7211
		[Obsolete("Use SetAreaCost instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetLayerCost(int layer, float cost);

		// Token: 0x06001C2C RID: 7212
		[Obsolete("Use GetAreaCost instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetLayerCost(int layer);

		// Token: 0x06001C2D RID: 7213
		[Obsolete("Use GetAreaFromName instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetNavMeshLayerFromName(string layerName);

		// Token: 0x06001C2E RID: 7214
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetAreaCost(int areaIndex, float cost);

		// Token: 0x06001C2F RID: 7215
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetAreaCost(int areaIndex);

		// Token: 0x06001C30 RID: 7216
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetAreaFromName(string areaName);

		// Token: 0x06001C31 RID: 7217 RVA: 0x00021E38 File Offset: 0x00020038
		public static NavMeshTriangulation CalculateTriangulation()
		{
			return (NavMeshTriangulation)NavMesh.TriangulateInternal();
		}

		// Token: 0x06001C32 RID: 7218
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern object TriangulateInternal();

		// Token: 0x06001C33 RID: 7219
		[Obsolete("use NavMesh.CalculateTriangulation() instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Triangulate(out Vector3[] vertices, out int[] indices);

		// Token: 0x06001C34 RID: 7220
		[Obsolete("AddOffMeshLinks has no effect and is deprecated.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void AddOffMeshLinks();

		// Token: 0x06001C35 RID: 7221
		[Obsolete("RestoreNavMesh has no effect and is deprecated.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void RestoreNavMesh();

		// Token: 0x170006CB RID: 1739
		// (get) Token: 0x06001C36 RID: 7222 RVA: 0x00021E5C File Offset: 0x0002005C
		// (set) Token: 0x06001C37 RID: 7223 RVA: 0x00021E78 File Offset: 0x00020078
		public static float avoidancePredictionTime
		{
			get
			{
				return NavMesh.GetAvoidancePredictionTime();
			}
			set
			{
				NavMesh.SetAvoidancePredictionTime(value);
			}
		}

		// Token: 0x06001C38 RID: 7224
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetAvoidancePredictionTime(float t);

		// Token: 0x06001C39 RID: 7225
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern float GetAvoidancePredictionTime();

		// Token: 0x170006CC RID: 1740
		// (get) Token: 0x06001C3A RID: 7226 RVA: 0x00021E84 File Offset: 0x00020084
		// (set) Token: 0x06001C3B RID: 7227 RVA: 0x00021EA0 File Offset: 0x000200A0
		public static int pathfindingIterationsPerFrame
		{
			get
			{
				return NavMesh.GetPathfindingIterationsPerFrame();
			}
			set
			{
				NavMesh.SetPathfindingIterationsPerFrame(value);
			}
		}

		// Token: 0x06001C3C RID: 7228
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetPathfindingIterationsPerFrame(int iter);

		// Token: 0x06001C3D RID: 7229
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetPathfindingIterationsPerFrame();

		// Token: 0x0400041D RID: 1053
		public const int AllAreas = -1;
	}
}
