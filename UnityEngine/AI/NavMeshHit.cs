﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x0200018A RID: 394
	[MovedFrom("UnityEngine")]
	public struct NavMeshHit
	{
		// Token: 0x170006C5 RID: 1733
		// (get) Token: 0x06001C16 RID: 7190 RVA: 0x00021C94 File Offset: 0x0001FE94
		// (set) Token: 0x06001C17 RID: 7191 RVA: 0x00021CB0 File Offset: 0x0001FEB0
		public Vector3 position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x170006C6 RID: 1734
		// (get) Token: 0x06001C18 RID: 7192 RVA: 0x00021CBC File Offset: 0x0001FEBC
		// (set) Token: 0x06001C19 RID: 7193 RVA: 0x00021CD8 File Offset: 0x0001FED8
		public Vector3 normal
		{
			get
			{
				return this.m_Normal;
			}
			set
			{
				this.m_Normal = value;
			}
		}

		// Token: 0x170006C7 RID: 1735
		// (get) Token: 0x06001C1A RID: 7194 RVA: 0x00021CE4 File Offset: 0x0001FEE4
		// (set) Token: 0x06001C1B RID: 7195 RVA: 0x00021D00 File Offset: 0x0001FF00
		public float distance
		{
			get
			{
				return this.m_Distance;
			}
			set
			{
				this.m_Distance = value;
			}
		}

		// Token: 0x170006C8 RID: 1736
		// (get) Token: 0x06001C1C RID: 7196 RVA: 0x00021D0C File Offset: 0x0001FF0C
		// (set) Token: 0x06001C1D RID: 7197 RVA: 0x00021D28 File Offset: 0x0001FF28
		public int mask
		{
			get
			{
				return this.m_Mask;
			}
			set
			{
				this.m_Mask = value;
			}
		}

		// Token: 0x170006C9 RID: 1737
		// (get) Token: 0x06001C1E RID: 7198 RVA: 0x00021D34 File Offset: 0x0001FF34
		// (set) Token: 0x06001C1F RID: 7199 RVA: 0x00021D58 File Offset: 0x0001FF58
		public bool hit
		{
			get
			{
				return this.m_Hit != 0;
			}
			set
			{
				this.m_Hit = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x04000415 RID: 1045
		private Vector3 m_Position;

		// Token: 0x04000416 RID: 1046
		private Vector3 m_Normal;

		// Token: 0x04000417 RID: 1047
		private float m_Distance;

		// Token: 0x04000418 RID: 1048
		private int m_Mask;

		// Token: 0x04000419 RID: 1049
		private int m_Hit;
	}
}
