﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x0200018F RID: 399
	[MovedFrom("UnityEngine")]
	public enum NavMeshObstacleShape
	{
		// Token: 0x04000425 RID: 1061
		Capsule,
		// Token: 0x04000426 RID: 1062
		Box
	}
}
