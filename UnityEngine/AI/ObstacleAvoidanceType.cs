﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x0200018D RID: 397
	[MovedFrom("UnityEngine")]
	public enum ObstacleAvoidanceType
	{
		// Token: 0x0400041F RID: 1055
		NoObstacleAvoidance,
		// Token: 0x04000420 RID: 1056
		LowQualityObstacleAvoidance,
		// Token: 0x04000421 RID: 1057
		MedQualityObstacleAvoidance,
		// Token: 0x04000422 RID: 1058
		GoodQualityObstacleAvoidance,
		// Token: 0x04000423 RID: 1059
		HighQualityObstacleAvoidance
	}
}
