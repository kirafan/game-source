﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000190 RID: 400
	[MovedFrom("UnityEngine")]
	public sealed class NavMeshObstacle : Behaviour
	{
		// Token: 0x170006ED RID: 1773
		// (get) Token: 0x06001C99 RID: 7321
		// (set) Token: 0x06001C9A RID: 7322
		public extern float height { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006EE RID: 1774
		// (get) Token: 0x06001C9B RID: 7323
		// (set) Token: 0x06001C9C RID: 7324
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006EF RID: 1775
		// (get) Token: 0x06001C9D RID: 7325 RVA: 0x000220DC File Offset: 0x000202DC
		// (set) Token: 0x06001C9E RID: 7326 RVA: 0x000220FC File Offset: 0x000202FC
		public Vector3 velocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_velocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_velocity(ref value);
			}
		}

		// Token: 0x06001C9F RID: 7327
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_velocity(out Vector3 value);

		// Token: 0x06001CA0 RID: 7328
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_velocity(ref Vector3 value);

		// Token: 0x170006F0 RID: 1776
		// (get) Token: 0x06001CA1 RID: 7329
		// (set) Token: 0x06001CA2 RID: 7330
		public extern bool carving { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006F1 RID: 1777
		// (get) Token: 0x06001CA3 RID: 7331
		// (set) Token: 0x06001CA4 RID: 7332
		public extern bool carveOnlyStationary { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006F2 RID: 1778
		// (get) Token: 0x06001CA5 RID: 7333
		// (set) Token: 0x06001CA6 RID: 7334
		public extern float carvingMoveThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006F3 RID: 1779
		// (get) Token: 0x06001CA7 RID: 7335
		// (set) Token: 0x06001CA8 RID: 7336
		public extern float carvingTimeToStationary { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006F4 RID: 1780
		// (get) Token: 0x06001CA9 RID: 7337
		// (set) Token: 0x06001CAA RID: 7338
		public extern NavMeshObstacleShape shape { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006F5 RID: 1781
		// (get) Token: 0x06001CAB RID: 7339 RVA: 0x00022108 File Offset: 0x00020308
		// (set) Token: 0x06001CAC RID: 7340 RVA: 0x00022128 File Offset: 0x00020328
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_center(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_center(ref value);
			}
		}

		// Token: 0x06001CAD RID: 7341
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_center(out Vector3 value);

		// Token: 0x06001CAE RID: 7342
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_center(ref Vector3 value);

		// Token: 0x170006F6 RID: 1782
		// (get) Token: 0x06001CAF RID: 7343 RVA: 0x00022134 File Offset: 0x00020334
		// (set) Token: 0x06001CB0 RID: 7344 RVA: 0x00022154 File Offset: 0x00020354
		public Vector3 size
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_size(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_size(ref value);
			}
		}

		// Token: 0x06001CB1 RID: 7345
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_size(out Vector3 value);

		// Token: 0x06001CB2 RID: 7346
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_size(ref Vector3 value);

		// Token: 0x06001CB3 RID: 7347
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void FitExtents();
	}
}
