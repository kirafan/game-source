﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000191 RID: 401
	[MovedFrom("UnityEngine")]
	public enum NavMeshPathStatus
	{
		// Token: 0x04000428 RID: 1064
		PathComplete,
		// Token: 0x04000429 RID: 1065
		PathPartial,
		// Token: 0x0400042A RID: 1066
		PathInvalid
	}
}
