﻿using System;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000193 RID: 403
	[MovedFrom("UnityEngine")]
	public enum OffMeshLinkType
	{
		// Token: 0x0400042E RID: 1070
		LinkTypeManual,
		// Token: 0x0400042F RID: 1071
		LinkTypeDropDown,
		// Token: 0x04000430 RID: 1072
		LinkTypeJumpAcross
	}
}
