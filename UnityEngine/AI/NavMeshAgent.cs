﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x0200018E RID: 398
	[MovedFrom("UnityEngine")]
	public sealed class NavMeshAgent : Behaviour
	{
		// Token: 0x06001C3F RID: 7231 RVA: 0x00021EB4 File Offset: 0x000200B4
		public bool SetDestination(Vector3 target)
		{
			return NavMeshAgent.INTERNAL_CALL_SetDestination(this, ref target);
		}

		// Token: 0x06001C40 RID: 7232
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_SetDestination(NavMeshAgent self, ref Vector3 target);

		// Token: 0x170006CD RID: 1741
		// (get) Token: 0x06001C41 RID: 7233 RVA: 0x00021ED4 File Offset: 0x000200D4
		// (set) Token: 0x06001C42 RID: 7234 RVA: 0x00021EF4 File Offset: 0x000200F4
		public Vector3 destination
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_destination(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_destination(ref value);
			}
		}

		// Token: 0x06001C43 RID: 7235
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_destination(out Vector3 value);

		// Token: 0x06001C44 RID: 7236
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_destination(ref Vector3 value);

		// Token: 0x170006CE RID: 1742
		// (get) Token: 0x06001C45 RID: 7237
		// (set) Token: 0x06001C46 RID: 7238
		public extern float stoppingDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006CF RID: 1743
		// (get) Token: 0x06001C47 RID: 7239 RVA: 0x00021F00 File Offset: 0x00020100
		// (set) Token: 0x06001C48 RID: 7240 RVA: 0x00021F20 File Offset: 0x00020120
		public Vector3 velocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_velocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_velocity(ref value);
			}
		}

		// Token: 0x06001C49 RID: 7241
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_velocity(out Vector3 value);

		// Token: 0x06001C4A RID: 7242
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_velocity(ref Vector3 value);

		// Token: 0x170006D0 RID: 1744
		// (get) Token: 0x06001C4B RID: 7243 RVA: 0x00021F2C File Offset: 0x0002012C
		// (set) Token: 0x06001C4C RID: 7244 RVA: 0x00021F4C File Offset: 0x0002014C
		public Vector3 nextPosition
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_nextPosition(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_nextPosition(ref value);
			}
		}

		// Token: 0x06001C4D RID: 7245
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_nextPosition(out Vector3 value);

		// Token: 0x06001C4E RID: 7246
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_nextPosition(ref Vector3 value);

		// Token: 0x170006D1 RID: 1745
		// (get) Token: 0x06001C4F RID: 7247 RVA: 0x00021F58 File Offset: 0x00020158
		public Vector3 steeringTarget
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_steeringTarget(out result);
				return result;
			}
		}

		// Token: 0x06001C50 RID: 7248
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_steeringTarget(out Vector3 value);

		// Token: 0x170006D2 RID: 1746
		// (get) Token: 0x06001C51 RID: 7249 RVA: 0x00021F78 File Offset: 0x00020178
		public Vector3 desiredVelocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_desiredVelocity(out result);
				return result;
			}
		}

		// Token: 0x06001C52 RID: 7250
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_desiredVelocity(out Vector3 value);

		// Token: 0x170006D3 RID: 1747
		// (get) Token: 0x06001C53 RID: 7251
		public extern float remainingDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170006D4 RID: 1748
		// (get) Token: 0x06001C54 RID: 7252
		// (set) Token: 0x06001C55 RID: 7253
		public extern float baseOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006D5 RID: 1749
		// (get) Token: 0x06001C56 RID: 7254
		public extern bool isOnOffMeshLink { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001C57 RID: 7255
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ActivateCurrentOffMeshLink(bool activated);

		// Token: 0x170006D6 RID: 1750
		// (get) Token: 0x06001C58 RID: 7256 RVA: 0x00021F98 File Offset: 0x00020198
		public OffMeshLinkData currentOffMeshLinkData
		{
			get
			{
				return this.GetCurrentOffMeshLinkDataInternal();
			}
		}

		// Token: 0x06001C59 RID: 7257
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern OffMeshLinkData GetCurrentOffMeshLinkDataInternal();

		// Token: 0x170006D7 RID: 1751
		// (get) Token: 0x06001C5A RID: 7258 RVA: 0x00021FB4 File Offset: 0x000201B4
		public OffMeshLinkData nextOffMeshLinkData
		{
			get
			{
				return this.GetNextOffMeshLinkDataInternal();
			}
		}

		// Token: 0x06001C5B RID: 7259
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern OffMeshLinkData GetNextOffMeshLinkDataInternal();

		// Token: 0x06001C5C RID: 7260
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CompleteOffMeshLink();

		// Token: 0x170006D8 RID: 1752
		// (get) Token: 0x06001C5D RID: 7261
		// (set) Token: 0x06001C5E RID: 7262
		public extern bool autoTraverseOffMeshLink { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006D9 RID: 1753
		// (get) Token: 0x06001C5F RID: 7263
		// (set) Token: 0x06001C60 RID: 7264
		public extern bool autoBraking { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006DA RID: 1754
		// (get) Token: 0x06001C61 RID: 7265
		// (set) Token: 0x06001C62 RID: 7266
		public extern bool autoRepath { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006DB RID: 1755
		// (get) Token: 0x06001C63 RID: 7267
		public extern bool hasPath { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170006DC RID: 1756
		// (get) Token: 0x06001C64 RID: 7268
		public extern bool pathPending { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170006DD RID: 1757
		// (get) Token: 0x06001C65 RID: 7269
		public extern bool isPathStale { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170006DE RID: 1758
		// (get) Token: 0x06001C66 RID: 7270
		public extern NavMeshPathStatus pathStatus { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170006DF RID: 1759
		// (get) Token: 0x06001C67 RID: 7271 RVA: 0x00021FD0 File Offset: 0x000201D0
		public Vector3 pathEndPosition
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_pathEndPosition(out result);
				return result;
			}
		}

		// Token: 0x06001C68 RID: 7272
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_pathEndPosition(out Vector3 value);

		// Token: 0x06001C69 RID: 7273 RVA: 0x00021FF0 File Offset: 0x000201F0
		public bool Warp(Vector3 newPosition)
		{
			return NavMeshAgent.INTERNAL_CALL_Warp(this, ref newPosition);
		}

		// Token: 0x06001C6A RID: 7274
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Warp(NavMeshAgent self, ref Vector3 newPosition);

		// Token: 0x06001C6B RID: 7275 RVA: 0x00022010 File Offset: 0x00020210
		public void Move(Vector3 offset)
		{
			NavMeshAgent.INTERNAL_CALL_Move(this, ref offset);
		}

		// Token: 0x06001C6C RID: 7276
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Move(NavMeshAgent self, ref Vector3 offset);

		// Token: 0x06001C6D RID: 7277 RVA: 0x0002201C File Offset: 0x0002021C
		public void Stop()
		{
			this.StopInternal();
		}

		// Token: 0x06001C6E RID: 7278
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void StopInternal();

		// Token: 0x06001C6F RID: 7279 RVA: 0x00022028 File Offset: 0x00020228
		[Obsolete("Use Stop() instead")]
		public void Stop(bool stopUpdates)
		{
			this.StopInternal();
		}

		// Token: 0x06001C70 RID: 7280
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Resume();

		// Token: 0x06001C71 RID: 7281
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ResetPath();

		// Token: 0x06001C72 RID: 7282
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SetPath(NavMeshPath path);

		// Token: 0x170006E0 RID: 1760
		// (get) Token: 0x06001C73 RID: 7283 RVA: 0x00022034 File Offset: 0x00020234
		// (set) Token: 0x06001C74 RID: 7284 RVA: 0x00022058 File Offset: 0x00020258
		public NavMeshPath path
		{
			get
			{
				NavMeshPath navMeshPath = new NavMeshPath();
				this.CopyPathTo(navMeshPath);
				return navMeshPath;
			}
			set
			{
				if (value == null)
				{
					throw new NullReferenceException();
				}
				this.SetPath(value);
			}
		}

		// Token: 0x06001C75 RID: 7285
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void CopyPathTo(NavMeshPath path);

		// Token: 0x06001C76 RID: 7286
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool FindClosestEdge(out NavMeshHit hit);

		// Token: 0x06001C77 RID: 7287 RVA: 0x00022070 File Offset: 0x00020270
		public bool Raycast(Vector3 targetPosition, out NavMeshHit hit)
		{
			return NavMeshAgent.INTERNAL_CALL_Raycast(this, ref targetPosition, out hit);
		}

		// Token: 0x06001C78 RID: 7288
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_Raycast(NavMeshAgent self, ref Vector3 targetPosition, out NavMeshHit hit);

		// Token: 0x06001C79 RID: 7289 RVA: 0x00022090 File Offset: 0x00020290
		public bool CalculatePath(Vector3 targetPosition, NavMeshPath path)
		{
			path.ClearCorners();
			return this.CalculatePathInternal(targetPosition, path);
		}

		// Token: 0x06001C7A RID: 7290 RVA: 0x000220B4 File Offset: 0x000202B4
		private bool CalculatePathInternal(Vector3 targetPosition, NavMeshPath path)
		{
			return NavMeshAgent.INTERNAL_CALL_CalculatePathInternal(this, ref targetPosition, path);
		}

		// Token: 0x06001C7B RID: 7291
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_CalculatePathInternal(NavMeshAgent self, ref Vector3 targetPosition, NavMeshPath path);

		// Token: 0x06001C7C RID: 7292
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SamplePathPosition(int areaMask, float maxDistance, out NavMeshHit hit);

		// Token: 0x06001C7D RID: 7293
		[Obsolete("Use SetAreaCost instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetLayerCost(int layer, float cost);

		// Token: 0x06001C7E RID: 7294
		[Obsolete("Use GetAreaCost instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetLayerCost(int layer);

		// Token: 0x06001C7F RID: 7295
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetAreaCost(int areaIndex, float areaCost);

		// Token: 0x06001C80 RID: 7296
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetAreaCost(int areaIndex);

		// Token: 0x170006E1 RID: 1761
		// (get) Token: 0x06001C81 RID: 7297
		// (set) Token: 0x06001C82 RID: 7298
		[Obsolete("Use areaMask instead.")]
		public extern int walkableMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006E2 RID: 1762
		// (get) Token: 0x06001C83 RID: 7299
		// (set) Token: 0x06001C84 RID: 7300
		public extern int areaMask { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006E3 RID: 1763
		// (get) Token: 0x06001C85 RID: 7301
		// (set) Token: 0x06001C86 RID: 7302
		public extern float speed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006E4 RID: 1764
		// (get) Token: 0x06001C87 RID: 7303
		// (set) Token: 0x06001C88 RID: 7304
		public extern float angularSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006E5 RID: 1765
		// (get) Token: 0x06001C89 RID: 7305
		// (set) Token: 0x06001C8A RID: 7306
		public extern float acceleration { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006E6 RID: 1766
		// (get) Token: 0x06001C8B RID: 7307
		// (set) Token: 0x06001C8C RID: 7308
		public extern bool updatePosition { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006E7 RID: 1767
		// (get) Token: 0x06001C8D RID: 7309
		// (set) Token: 0x06001C8E RID: 7310
		public extern bool updateRotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006E8 RID: 1768
		// (get) Token: 0x06001C8F RID: 7311
		// (set) Token: 0x06001C90 RID: 7312
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006E9 RID: 1769
		// (get) Token: 0x06001C91 RID: 7313
		// (set) Token: 0x06001C92 RID: 7314
		public extern float height { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006EA RID: 1770
		// (get) Token: 0x06001C93 RID: 7315
		// (set) Token: 0x06001C94 RID: 7316
		public extern ObstacleAvoidanceType obstacleAvoidanceType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006EB RID: 1771
		// (get) Token: 0x06001C95 RID: 7317
		// (set) Token: 0x06001C96 RID: 7318
		public extern int avoidancePriority { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170006EC RID: 1772
		// (get) Token: 0x06001C97 RID: 7319
		public extern bool isOnNavMesh { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
