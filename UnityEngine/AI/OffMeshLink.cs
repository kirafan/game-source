﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000195 RID: 405
	[MovedFrom("UnityEngine")]
	public sealed class OffMeshLink : Component
	{
		// Token: 0x170006FF RID: 1791
		// (get) Token: 0x06001CC6 RID: 7366
		// (set) Token: 0x06001CC7 RID: 7367
		public extern bool activated { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000700 RID: 1792
		// (get) Token: 0x06001CC8 RID: 7368
		public extern bool occupied { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000701 RID: 1793
		// (get) Token: 0x06001CC9 RID: 7369
		// (set) Token: 0x06001CCA RID: 7370
		public extern float costOverride { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000702 RID: 1794
		// (get) Token: 0x06001CCB RID: 7371
		// (set) Token: 0x06001CCC RID: 7372
		public extern bool biDirectional { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001CCD RID: 7373
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UpdatePositions();

		// Token: 0x17000703 RID: 1795
		// (get) Token: 0x06001CCE RID: 7374
		// (set) Token: 0x06001CCF RID: 7375
		[Obsolete("Use area instead.")]
		public extern int navMeshLayer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000704 RID: 1796
		// (get) Token: 0x06001CD0 RID: 7376
		// (set) Token: 0x06001CD1 RID: 7377
		public extern int area { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000705 RID: 1797
		// (get) Token: 0x06001CD2 RID: 7378
		// (set) Token: 0x06001CD3 RID: 7379
		public extern bool autoUpdatePositions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000706 RID: 1798
		// (get) Token: 0x06001CD4 RID: 7380
		// (set) Token: 0x06001CD5 RID: 7381
		public extern Transform startTransform { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000707 RID: 1799
		// (get) Token: 0x06001CD6 RID: 7382
		// (set) Token: 0x06001CD7 RID: 7383
		public extern Transform endTransform { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
