﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.AI
{
	// Token: 0x02000194 RID: 404
	[MovedFrom("UnityEngine")]
	public struct OffMeshLinkData
	{
		// Token: 0x170006F9 RID: 1785
		// (get) Token: 0x06001CBE RID: 7358 RVA: 0x000221EC File Offset: 0x000203EC
		public bool valid
		{
			get
			{
				return this.m_Valid != 0;
			}
		}

		// Token: 0x170006FA RID: 1786
		// (get) Token: 0x06001CBF RID: 7359 RVA: 0x00022210 File Offset: 0x00020410
		public bool activated
		{
			get
			{
				return this.m_Activated != 0;
			}
		}

		// Token: 0x170006FB RID: 1787
		// (get) Token: 0x06001CC0 RID: 7360 RVA: 0x00022234 File Offset: 0x00020434
		public OffMeshLinkType linkType
		{
			get
			{
				return this.m_LinkType;
			}
		}

		// Token: 0x170006FC RID: 1788
		// (get) Token: 0x06001CC1 RID: 7361 RVA: 0x00022250 File Offset: 0x00020450
		public Vector3 startPos
		{
			get
			{
				return this.m_StartPos;
			}
		}

		// Token: 0x170006FD RID: 1789
		// (get) Token: 0x06001CC2 RID: 7362 RVA: 0x0002226C File Offset: 0x0002046C
		public Vector3 endPos
		{
			get
			{
				return this.m_EndPos;
			}
		}

		// Token: 0x170006FE RID: 1790
		// (get) Token: 0x06001CC3 RID: 7363 RVA: 0x00022288 File Offset: 0x00020488
		public OffMeshLink offMeshLink
		{
			get
			{
				return this.GetOffMeshLinkInternal(this.m_InstanceID);
			}
		}

		// Token: 0x06001CC4 RID: 7364
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern OffMeshLink GetOffMeshLinkInternal(int instanceID);

		// Token: 0x04000431 RID: 1073
		private int m_Valid;

		// Token: 0x04000432 RID: 1074
		private int m_Activated;

		// Token: 0x04000433 RID: 1075
		private int m_InstanceID;

		// Token: 0x04000434 RID: 1076
		private OffMeshLinkType m_LinkType;

		// Token: 0x04000435 RID: 1077
		private Vector3 m_StartPos;

		// Token: 0x04000436 RID: 1078
		private Vector3 m_EndPos;
	}
}
