﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020002AB RID: 683
	[Obsolete("This component is part of the legacy particle system, which is deprecated and will be removed in a future release. Use the ParticleSystem component instead.", false)]
	public sealed class ParticleAnimator : Component
	{
		// Token: 0x17000A51 RID: 2641
		// (get) Token: 0x06002B65 RID: 11109
		// (set) Token: 0x06002B66 RID: 11110
		public extern bool doesAnimateColor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A52 RID: 2642
		// (get) Token: 0x06002B67 RID: 11111 RVA: 0x00041B04 File Offset: 0x0003FD04
		// (set) Token: 0x06002B68 RID: 11112 RVA: 0x00041B24 File Offset: 0x0003FD24
		public Vector3 worldRotationAxis
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_worldRotationAxis(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_worldRotationAxis(ref value);
			}
		}

		// Token: 0x06002B69 RID: 11113
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_worldRotationAxis(out Vector3 value);

		// Token: 0x06002B6A RID: 11114
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_worldRotationAxis(ref Vector3 value);

		// Token: 0x17000A53 RID: 2643
		// (get) Token: 0x06002B6B RID: 11115 RVA: 0x00041B30 File Offset: 0x0003FD30
		// (set) Token: 0x06002B6C RID: 11116 RVA: 0x00041B50 File Offset: 0x0003FD50
		public Vector3 localRotationAxis
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_localRotationAxis(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localRotationAxis(ref value);
			}
		}

		// Token: 0x06002B6D RID: 11117
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localRotationAxis(out Vector3 value);

		// Token: 0x06002B6E RID: 11118
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localRotationAxis(ref Vector3 value);

		// Token: 0x17000A54 RID: 2644
		// (get) Token: 0x06002B6F RID: 11119
		// (set) Token: 0x06002B70 RID: 11120
		public extern float sizeGrow { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A55 RID: 2645
		// (get) Token: 0x06002B71 RID: 11121 RVA: 0x00041B5C File Offset: 0x0003FD5C
		// (set) Token: 0x06002B72 RID: 11122 RVA: 0x00041B7C File Offset: 0x0003FD7C
		public Vector3 rndForce
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_rndForce(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_rndForce(ref value);
			}
		}

		// Token: 0x06002B73 RID: 11123
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rndForce(out Vector3 value);

		// Token: 0x06002B74 RID: 11124
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_rndForce(ref Vector3 value);

		// Token: 0x17000A56 RID: 2646
		// (get) Token: 0x06002B75 RID: 11125 RVA: 0x00041B88 File Offset: 0x0003FD88
		// (set) Token: 0x06002B76 RID: 11126 RVA: 0x00041BA8 File Offset: 0x0003FDA8
		public Vector3 force
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_force(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_force(ref value);
			}
		}

		// Token: 0x06002B77 RID: 11127
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_force(out Vector3 value);

		// Token: 0x06002B78 RID: 11128
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_force(ref Vector3 value);

		// Token: 0x17000A57 RID: 2647
		// (get) Token: 0x06002B79 RID: 11129
		// (set) Token: 0x06002B7A RID: 11130
		public extern float damping { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A58 RID: 2648
		// (get) Token: 0x06002B7B RID: 11131
		// (set) Token: 0x06002B7C RID: 11132
		public extern bool autodestruct { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A59 RID: 2649
		// (get) Token: 0x06002B7D RID: 11133
		// (set) Token: 0x06002B7E RID: 11134
		public extern Color[] colorAnimation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
