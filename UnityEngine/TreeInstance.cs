﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001F3 RID: 499
	[UsedByNativeCode]
	public struct TreeInstance
	{
		// Token: 0x040005CE RID: 1486
		public Vector3 position;

		// Token: 0x040005CF RID: 1487
		public float widthScale;

		// Token: 0x040005D0 RID: 1488
		public float heightScale;

		// Token: 0x040005D1 RID: 1489
		public float rotation;

		// Token: 0x040005D2 RID: 1490
		public Color32 color;

		// Token: 0x040005D3 RID: 1491
		public Color32 lightmapColor;

		// Token: 0x040005D4 RID: 1492
		public int prototypeIndex;

		// Token: 0x040005D5 RID: 1493
		internal float temporaryDistance;
	}
}
