﻿using System;

namespace UnityEngine
{
	// Token: 0x020001A3 RID: 419
	public enum FFTWindow
	{
		// Token: 0x0400046F RID: 1135
		Rectangular,
		// Token: 0x04000470 RID: 1136
		Triangle,
		// Token: 0x04000471 RID: 1137
		Hamming,
		// Token: 0x04000472 RID: 1138
		Hanning,
		// Token: 0x04000473 RID: 1139
		Blackman,
		// Token: 0x04000474 RID: 1140
		BlackmanHarris
	}
}
