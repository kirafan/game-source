﻿using System;

namespace UnityEngine
{
	// Token: 0x02000238 RID: 568
	public enum TextClipping
	{
		// Token: 0x04000869 RID: 2153
		Overflow,
		// Token: 0x0400086A RID: 2154
		Clip
	}
}
