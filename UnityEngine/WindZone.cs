﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000F3 RID: 243
	public sealed class WindZone : Component
	{
		// Token: 0x17000380 RID: 896
		// (get) Token: 0x060010A8 RID: 4264
		// (set) Token: 0x060010A9 RID: 4265
		public extern WindZoneMode mode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000381 RID: 897
		// (get) Token: 0x060010AA RID: 4266
		// (set) Token: 0x060010AB RID: 4267
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000382 RID: 898
		// (get) Token: 0x060010AC RID: 4268
		// (set) Token: 0x060010AD RID: 4269
		public extern float windMain { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000383 RID: 899
		// (get) Token: 0x060010AE RID: 4270
		// (set) Token: 0x060010AF RID: 4271
		public extern float windTurbulence { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000384 RID: 900
		// (get) Token: 0x060010B0 RID: 4272
		// (set) Token: 0x060010B1 RID: 4273
		public extern float windPulseMagnitude { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x060010B2 RID: 4274
		// (set) Token: 0x060010B3 RID: 4275
		public extern float windPulseFrequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
