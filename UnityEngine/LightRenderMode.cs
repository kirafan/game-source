﻿using System;

namespace UnityEngine
{
	// Token: 0x020002E9 RID: 745
	public enum LightRenderMode
	{
		// Token: 0x04000AFE RID: 2814
		Auto,
		// Token: 0x04000AFF RID: 2815
		ForcePixel,
		// Token: 0x04000B00 RID: 2816
		ForceVertex
	}
}
