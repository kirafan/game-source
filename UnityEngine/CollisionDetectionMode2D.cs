﻿using System;

namespace UnityEngine
{
	// Token: 0x0200015B RID: 347
	public enum CollisionDetectionMode2D
	{
		// Token: 0x040003D3 RID: 979
		[Obsolete("Enum member CollisionDetectionMode2D.None has been deprecated. Use CollisionDetectionMode2D.Discrete instead (UnityUpgradable) -> Discrete", true)]
		None,
		// Token: 0x040003D4 RID: 980
		Discrete = 0,
		// Token: 0x040003D5 RID: 981
		Continuous
	}
}
