﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200020C RID: 524
	public sealed class Canvas : Behaviour
	{
		// Token: 0x170008C8 RID: 2248
		// (get) Token: 0x0600233A RID: 9018
		// (set) Token: 0x0600233B RID: 9019
		public extern RenderMode renderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008C9 RID: 2249
		// (get) Token: 0x0600233C RID: 9020
		public extern bool isRootCanvas { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008CA RID: 2250
		// (get) Token: 0x0600233D RID: 9021
		// (set) Token: 0x0600233E RID: 9022
		public extern Camera worldCamera { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008CB RID: 2251
		// (get) Token: 0x0600233F RID: 9023 RVA: 0x000292A0 File Offset: 0x000274A0
		public Rect pixelRect
		{
			get
			{
				Rect result;
				this.INTERNAL_get_pixelRect(out result);
				return result;
			}
		}

		// Token: 0x06002340 RID: 9024
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_pixelRect(out Rect value);

		// Token: 0x170008CC RID: 2252
		// (get) Token: 0x06002341 RID: 9025
		// (set) Token: 0x06002342 RID: 9026
		public extern float scaleFactor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008CD RID: 2253
		// (get) Token: 0x06002343 RID: 9027
		// (set) Token: 0x06002344 RID: 9028
		public extern float referencePixelsPerUnit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008CE RID: 2254
		// (get) Token: 0x06002345 RID: 9029
		// (set) Token: 0x06002346 RID: 9030
		public extern bool overridePixelPerfect { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008CF RID: 2255
		// (get) Token: 0x06002347 RID: 9031
		// (set) Token: 0x06002348 RID: 9032
		public extern bool pixelPerfect { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008D0 RID: 2256
		// (get) Token: 0x06002349 RID: 9033
		// (set) Token: 0x0600234A RID: 9034
		public extern float planeDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008D1 RID: 2257
		// (get) Token: 0x0600234B RID: 9035
		public extern int renderOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008D2 RID: 2258
		// (get) Token: 0x0600234C RID: 9036
		// (set) Token: 0x0600234D RID: 9037
		public extern bool overrideSorting { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008D3 RID: 2259
		// (get) Token: 0x0600234E RID: 9038
		// (set) Token: 0x0600234F RID: 9039
		public extern int sortingOrder { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008D4 RID: 2260
		// (get) Token: 0x06002350 RID: 9040
		// (set) Token: 0x06002351 RID: 9041
		public extern int targetDisplay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008D5 RID: 2261
		// (get) Token: 0x06002352 RID: 9042
		// (set) Token: 0x06002353 RID: 9043
		[Obsolete("Setting normalizedSize via a int is not supported. Please use normalizedSortingGridSize")]
		public extern int sortingGridNormalizedSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008D6 RID: 2262
		// (get) Token: 0x06002354 RID: 9044
		// (set) Token: 0x06002355 RID: 9045
		public extern float normalizedSortingGridSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008D7 RID: 2263
		// (get) Token: 0x06002356 RID: 9046
		// (set) Token: 0x06002357 RID: 9047
		public extern int sortingLayerID { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008D8 RID: 2264
		// (get) Token: 0x06002358 RID: 9048
		public extern int cachedSortingLayerValue { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008D9 RID: 2265
		// (get) Token: 0x06002359 RID: 9049
		// (set) Token: 0x0600235A RID: 9050
		public extern string sortingLayerName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008DA RID: 2266
		// (get) Token: 0x0600235B RID: 9051
		public extern Canvas rootCanvas { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600235C RID: 9052
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Material GetDefaultCanvasMaterial();

		// Token: 0x0600235D RID: 9053
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Material GetETC1SupportedCanvasMaterial();

		// Token: 0x0600235E RID: 9054
		[Obsolete("Shared default material now used for text and general UI elements, call Canvas.GetDefaultCanvasMaterial()")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Material GetDefaultCanvasTextMaterial();

		// Token: 0x1400000D RID: 13
		// (add) Token: 0x0600235F RID: 9055 RVA: 0x000292C0 File Offset: 0x000274C0
		// (remove) Token: 0x06002360 RID: 9056 RVA: 0x000292F4 File Offset: 0x000274F4
		public static event Canvas.WillRenderCanvases willRenderCanvases;

		// Token: 0x06002361 RID: 9057 RVA: 0x00029328 File Offset: 0x00027528
		[RequiredByNativeCode]
		private static void SendWillRenderCanvases()
		{
			if (Canvas.willRenderCanvases != null)
			{
				Canvas.willRenderCanvases();
			}
		}

		// Token: 0x06002362 RID: 9058 RVA: 0x00029340 File Offset: 0x00027540
		public static void ForceUpdateCanvases()
		{
			Canvas.SendWillRenderCanvases();
		}

		// Token: 0x0200020D RID: 525
		// (Invoke) Token: 0x06002364 RID: 9060
		public delegate void WillRenderCanvases();
	}
}
