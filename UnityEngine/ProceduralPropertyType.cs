﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C5 RID: 197
	public enum ProceduralPropertyType
	{
		// Token: 0x040001EA RID: 490
		Boolean,
		// Token: 0x040001EB RID: 491
		Float,
		// Token: 0x040001EC RID: 492
		Vector2,
		// Token: 0x040001ED RID: 493
		Vector3,
		// Token: 0x040001EE RID: 494
		Vector4,
		// Token: 0x040001EF RID: 495
		Color3,
		// Token: 0x040001F0 RID: 496
		Color4,
		// Token: 0x040001F1 RID: 497
		Enum,
		// Token: 0x040001F2 RID: 498
		Texture
	}
}
