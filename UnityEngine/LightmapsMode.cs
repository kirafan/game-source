﻿using System;
using System.ComponentModel;

namespace UnityEngine
{
	// Token: 0x02000301 RID: 769
	public enum LightmapsMode
	{
		// Token: 0x04000BB4 RID: 2996
		NonDirectional,
		// Token: 0x04000BB5 RID: 2997
		CombinedDirectional,
		// Token: 0x04000BB6 RID: 2998
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Enum member LightmapsMode.SeparateDirectional has been deprecated and will be removed in a future version. Use CombinedDirectional instead (UnityUpgradable) -> CombinedDirectional", false)]
		SeparateDirectional
	}
}
