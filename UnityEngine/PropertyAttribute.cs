﻿using System;

namespace UnityEngine
{
	// Token: 0x02000357 RID: 855
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public abstract class PropertyAttribute : Attribute
	{
		// Token: 0x17000ADE RID: 2782
		// (get) Token: 0x06002DD7 RID: 11735 RVA: 0x00048DF8 File Offset: 0x00046FF8
		// (set) Token: 0x06002DD8 RID: 11736 RVA: 0x00048E14 File Offset: 0x00047014
		public int order { get; set; }
	}
}
