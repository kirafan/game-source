﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001AB RID: 427
	public sealed class AudioDistortionFilter : Behaviour
	{
		// Token: 0x1700074F RID: 1871
		// (get) Token: 0x06001DA5 RID: 7589
		// (set) Token: 0x06001DA6 RID: 7590
		public extern float distortionLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
