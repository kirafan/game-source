﻿using System;
using UnityEngine.Rendering;

namespace UnityEngine
{
	// Token: 0x020002E2 RID: 738
	public struct RenderTargetSetup
	{
		// Token: 0x06002CA3 RID: 11427 RVA: 0x00046618 File Offset: 0x00044818
		public RenderTargetSetup(RenderBuffer[] color, RenderBuffer depth, int mip, CubemapFace face, RenderBufferLoadAction[] colorLoad, RenderBufferStoreAction[] colorStore, RenderBufferLoadAction depthLoad, RenderBufferStoreAction depthStore)
		{
			this.color = color;
			this.depth = depth;
			this.mipLevel = mip;
			this.cubemapFace = face;
			this.depthSlice = 0;
			this.colorLoad = colorLoad;
			this.colorStore = colorStore;
			this.depthLoad = depthLoad;
			this.depthStore = depthStore;
		}

		// Token: 0x06002CA4 RID: 11428 RVA: 0x0004666C File Offset: 0x0004486C
		public RenderTargetSetup(RenderBuffer color, RenderBuffer depth)
		{
			this = new RenderTargetSetup(new RenderBuffer[]
			{
				color
			}, depth);
		}

		// Token: 0x06002CA5 RID: 11429 RVA: 0x0004668C File Offset: 0x0004488C
		public RenderTargetSetup(RenderBuffer color, RenderBuffer depth, int mipLevel)
		{
			this = new RenderTargetSetup(new RenderBuffer[]
			{
				color
			}, depth, mipLevel);
		}

		// Token: 0x06002CA6 RID: 11430 RVA: 0x000466AC File Offset: 0x000448AC
		public RenderTargetSetup(RenderBuffer color, RenderBuffer depth, int mipLevel, CubemapFace face)
		{
			this = new RenderTargetSetup(new RenderBuffer[]
			{
				color
			}, depth, mipLevel, face);
		}

		// Token: 0x06002CA7 RID: 11431 RVA: 0x000466CC File Offset: 0x000448CC
		public RenderTargetSetup(RenderBuffer color, RenderBuffer depth, int mipLevel, CubemapFace face, int depthSlice)
		{
			this = new RenderTargetSetup(new RenderBuffer[]
			{
				color
			}, depth, mipLevel, face);
			this.depthSlice = depthSlice;
		}

		// Token: 0x06002CA8 RID: 11432 RVA: 0x000466F4 File Offset: 0x000448F4
		public RenderTargetSetup(RenderBuffer[] color, RenderBuffer depth)
		{
			this = new RenderTargetSetup(color, depth, 0, CubemapFace.Unknown);
		}

		// Token: 0x06002CA9 RID: 11433 RVA: 0x00046704 File Offset: 0x00044904
		public RenderTargetSetup(RenderBuffer[] color, RenderBuffer depth, int mipLevel)
		{
			this = new RenderTargetSetup(color, depth, mipLevel, CubemapFace.Unknown);
		}

		// Token: 0x06002CAA RID: 11434 RVA: 0x00046714 File Offset: 0x00044914
		public RenderTargetSetup(RenderBuffer[] color, RenderBuffer depth, int mip, CubemapFace face)
		{
			this = new RenderTargetSetup(color, depth, mip, face, RenderTargetSetup.LoadActions(color), RenderTargetSetup.StoreActions(color), depth.loadAction, depth.storeAction);
		}

		// Token: 0x06002CAB RID: 11435 RVA: 0x00046748 File Offset: 0x00044948
		internal static RenderBufferLoadAction[] LoadActions(RenderBuffer[] buf)
		{
			RenderBufferLoadAction[] array = new RenderBufferLoadAction[buf.Length];
			for (int i = 0; i < buf.Length; i++)
			{
				array[i] = buf[i].loadAction;
				buf[i].loadAction = RenderBufferLoadAction.Load;
			}
			return array;
		}

		// Token: 0x06002CAC RID: 11436 RVA: 0x0004679C File Offset: 0x0004499C
		internal static RenderBufferStoreAction[] StoreActions(RenderBuffer[] buf)
		{
			RenderBufferStoreAction[] array = new RenderBufferStoreAction[buf.Length];
			for (int i = 0; i < buf.Length; i++)
			{
				array[i] = buf[i].storeAction;
				buf[i].storeAction = RenderBufferStoreAction.Store;
			}
			return array;
		}

		// Token: 0x04000AD3 RID: 2771
		public RenderBuffer[] color;

		// Token: 0x04000AD4 RID: 2772
		public RenderBuffer depth;

		// Token: 0x04000AD5 RID: 2773
		public int mipLevel;

		// Token: 0x04000AD6 RID: 2774
		public CubemapFace cubemapFace;

		// Token: 0x04000AD7 RID: 2775
		public int depthSlice;

		// Token: 0x04000AD8 RID: 2776
		public RenderBufferLoadAction[] colorLoad;

		// Token: 0x04000AD9 RID: 2777
		public RenderBufferStoreAction[] colorStore;

		// Token: 0x04000ADA RID: 2778
		public RenderBufferLoadAction depthLoad;

		// Token: 0x04000ADB RID: 2779
		public RenderBufferStoreAction depthStore;
	}
}
