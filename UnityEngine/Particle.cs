﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020002A6 RID: 678
	[UsedByNativeCode]
	[Obsolete("This is part of the legacy particle system, which is deprecated and will be removed in a future release. Use the ParticleSystem component instead.", false)]
	public struct Particle
	{
		// Token: 0x17000A37 RID: 2615
		// (get) Token: 0x06002B1F RID: 11039 RVA: 0x00041824 File Offset: 0x0003FA24
		// (set) Token: 0x06002B20 RID: 11040 RVA: 0x00041840 File Offset: 0x0003FA40
		public Vector3 position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				this.m_Position = value;
			}
		}

		// Token: 0x17000A38 RID: 2616
		// (get) Token: 0x06002B21 RID: 11041 RVA: 0x0004184C File Offset: 0x0003FA4C
		// (set) Token: 0x06002B22 RID: 11042 RVA: 0x00041868 File Offset: 0x0003FA68
		public Vector3 velocity
		{
			get
			{
				return this.m_Velocity;
			}
			set
			{
				this.m_Velocity = value;
			}
		}

		// Token: 0x17000A39 RID: 2617
		// (get) Token: 0x06002B23 RID: 11043 RVA: 0x00041874 File Offset: 0x0003FA74
		// (set) Token: 0x06002B24 RID: 11044 RVA: 0x00041890 File Offset: 0x0003FA90
		public float energy
		{
			get
			{
				return this.m_Energy;
			}
			set
			{
				this.m_Energy = value;
			}
		}

		// Token: 0x17000A3A RID: 2618
		// (get) Token: 0x06002B25 RID: 11045 RVA: 0x0004189C File Offset: 0x0003FA9C
		// (set) Token: 0x06002B26 RID: 11046 RVA: 0x000418B8 File Offset: 0x0003FAB8
		public float startEnergy
		{
			get
			{
				return this.m_StartEnergy;
			}
			set
			{
				this.m_StartEnergy = value;
			}
		}

		// Token: 0x17000A3B RID: 2619
		// (get) Token: 0x06002B27 RID: 11047 RVA: 0x000418C4 File Offset: 0x0003FAC4
		// (set) Token: 0x06002B28 RID: 11048 RVA: 0x000418E0 File Offset: 0x0003FAE0
		public float size
		{
			get
			{
				return this.m_Size;
			}
			set
			{
				this.m_Size = value;
			}
		}

		// Token: 0x17000A3C RID: 2620
		// (get) Token: 0x06002B29 RID: 11049 RVA: 0x000418EC File Offset: 0x0003FAEC
		// (set) Token: 0x06002B2A RID: 11050 RVA: 0x00041908 File Offset: 0x0003FB08
		public float rotation
		{
			get
			{
				return this.m_Rotation;
			}
			set
			{
				this.m_Rotation = value;
			}
		}

		// Token: 0x17000A3D RID: 2621
		// (get) Token: 0x06002B2B RID: 11051 RVA: 0x00041914 File Offset: 0x0003FB14
		// (set) Token: 0x06002B2C RID: 11052 RVA: 0x00041930 File Offset: 0x0003FB30
		public float angularVelocity
		{
			get
			{
				return this.m_AngularVelocity;
			}
			set
			{
				this.m_AngularVelocity = value;
			}
		}

		// Token: 0x17000A3E RID: 2622
		// (get) Token: 0x06002B2D RID: 11053 RVA: 0x0004193C File Offset: 0x0003FB3C
		// (set) Token: 0x06002B2E RID: 11054 RVA: 0x00041958 File Offset: 0x0003FB58
		public Color color
		{
			get
			{
				return this.m_Color;
			}
			set
			{
				this.m_Color = value;
			}
		}

		// Token: 0x04000A16 RID: 2582
		private Vector3 m_Position;

		// Token: 0x04000A17 RID: 2583
		private Vector3 m_Velocity;

		// Token: 0x04000A18 RID: 2584
		private float m_Size;

		// Token: 0x04000A19 RID: 2585
		private float m_Rotation;

		// Token: 0x04000A1A RID: 2586
		private float m_AngularVelocity;

		// Token: 0x04000A1B RID: 2587
		private float m_Energy;

		// Token: 0x04000A1C RID: 2588
		private float m_StartEnergy;

		// Token: 0x04000A1D RID: 2589
		private Color m_Color;
	}
}
