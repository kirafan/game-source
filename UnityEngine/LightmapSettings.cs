﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000058 RID: 88
	public sealed class LightmapSettings : Object
	{
		// Token: 0x17000176 RID: 374
		// (get) Token: 0x0600069F RID: 1695
		// (set) Token: 0x060006A0 RID: 1696
		public static extern LightmapData[] lightmaps { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000177 RID: 375
		// (get) Token: 0x060006A1 RID: 1697
		// (set) Token: 0x060006A2 RID: 1698
		[Obsolete("Use lightmapsMode property")]
		public static extern LightmapsModeLegacy lightmapsModeLegacy { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000178 RID: 376
		// (get) Token: 0x060006A3 RID: 1699
		// (set) Token: 0x060006A4 RID: 1700
		public static extern LightmapsMode lightmapsMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000179 RID: 377
		// (get) Token: 0x060006A5 RID: 1701 RVA: 0x0000904C File Offset: 0x0000724C
		// (set) Token: 0x060006A6 RID: 1702 RVA: 0x00009068 File Offset: 0x00007268
		[Obsolete("bakedColorSpace is no longer valid. Use QualitySettings.desiredColorSpace.", false)]
		public static ColorSpace bakedColorSpace
		{
			get
			{
				return QualitySettings.desiredColorSpace;
			}
			set
			{
			}
		}

		// Token: 0x1700017A RID: 378
		// (get) Token: 0x060006A7 RID: 1703
		// (set) Token: 0x060006A8 RID: 1704
		public static extern LightProbes lightProbes { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060006A9 RID: 1705
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Reset();
	}
}
