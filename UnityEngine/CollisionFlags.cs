﻿using System;

namespace UnityEngine
{
	// Token: 0x02000140 RID: 320
	public enum CollisionFlags
	{
		// Token: 0x04000396 RID: 918
		None,
		// Token: 0x04000397 RID: 919
		Sides,
		// Token: 0x04000398 RID: 920
		Above,
		// Token: 0x04000399 RID: 921
		Below = 4,
		// Token: 0x0400039A RID: 922
		CollidedSides = 1,
		// Token: 0x0400039B RID: 923
		CollidedAbove,
		// Token: 0x0400039C RID: 924
		CollidedBelow = 4
	}
}
