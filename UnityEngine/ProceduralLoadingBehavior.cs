﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C4 RID: 196
	public enum ProceduralLoadingBehavior
	{
		// Token: 0x040001E3 RID: 483
		DoNothing,
		// Token: 0x040001E4 RID: 484
		Generate,
		// Token: 0x040001E5 RID: 485
		BakeAndKeep,
		// Token: 0x040001E6 RID: 486
		BakeAndDiscard,
		// Token: 0x040001E7 RID: 487
		Cache,
		// Token: 0x040001E8 RID: 488
		DoNothingAndCache
	}
}
