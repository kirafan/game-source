﻿using System;

namespace UnityEngine
{
	// Token: 0x02000214 RID: 532
	public enum EventType
	{
		// Token: 0x04000789 RID: 1929
		MouseDown,
		// Token: 0x0400078A RID: 1930
		MouseUp,
		// Token: 0x0400078B RID: 1931
		MouseMove,
		// Token: 0x0400078C RID: 1932
		MouseDrag,
		// Token: 0x0400078D RID: 1933
		KeyDown,
		// Token: 0x0400078E RID: 1934
		KeyUp,
		// Token: 0x0400078F RID: 1935
		ScrollWheel,
		// Token: 0x04000790 RID: 1936
		Repaint,
		// Token: 0x04000791 RID: 1937
		Layout,
		// Token: 0x04000792 RID: 1938
		DragUpdated,
		// Token: 0x04000793 RID: 1939
		DragPerform,
		// Token: 0x04000794 RID: 1940
		DragExited = 15,
		// Token: 0x04000795 RID: 1941
		Ignore = 11,
		// Token: 0x04000796 RID: 1942
		Used,
		// Token: 0x04000797 RID: 1943
		ValidateCommand,
		// Token: 0x04000798 RID: 1944
		ExecuteCommand,
		// Token: 0x04000799 RID: 1945
		ContextClick = 16,
		// Token: 0x0400079A RID: 1946
		mouseDown = 0,
		// Token: 0x0400079B RID: 1947
		mouseUp,
		// Token: 0x0400079C RID: 1948
		mouseMove,
		// Token: 0x0400079D RID: 1949
		mouseDrag,
		// Token: 0x0400079E RID: 1950
		keyDown,
		// Token: 0x0400079F RID: 1951
		keyUp,
		// Token: 0x040007A0 RID: 1952
		scrollWheel,
		// Token: 0x040007A1 RID: 1953
		repaint,
		// Token: 0x040007A2 RID: 1954
		layout,
		// Token: 0x040007A3 RID: 1955
		dragUpdated,
		// Token: 0x040007A4 RID: 1956
		dragPerform,
		// Token: 0x040007A5 RID: 1957
		ignore,
		// Token: 0x040007A6 RID: 1958
		used
	}
}
