﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000212 RID: 530
	[StructLayout(LayoutKind.Sequential)]
	public sealed class Event
	{
		// Token: 0x0600239F RID: 9119 RVA: 0x000295D4 File Offset: 0x000277D4
		public Event()
		{
			this.Init(0);
		}

		// Token: 0x060023A0 RID: 9120 RVA: 0x000295E4 File Offset: 0x000277E4
		public Event(int displayIndex)
		{
			this.Init(displayIndex);
		}

		// Token: 0x060023A1 RID: 9121 RVA: 0x000295F4 File Offset: 0x000277F4
		public Event(Event other)
		{
			if (other == null)
			{
				throw new ArgumentException("Event to copy from is null.");
			}
			this.InitCopy(other);
		}

		// Token: 0x060023A2 RID: 9122 RVA: 0x00029618 File Offset: 0x00027818
		private Event(IntPtr ptr)
		{
			this.InitPtr(ptr);
		}

		// Token: 0x060023A3 RID: 9123 RVA: 0x00029628 File Offset: 0x00027828
		~Event()
		{
			this.Cleanup();
		}

		// Token: 0x060023A4 RID: 9124 RVA: 0x00029658 File Offset: 0x00027858
		internal static void CleanupRoots()
		{
			Event.s_Current = null;
			Event.s_MasterEvent = null;
		}

		// Token: 0x170008E9 RID: 2281
		// (get) Token: 0x060023A5 RID: 9125 RVA: 0x00029668 File Offset: 0x00027868
		// (set) Token: 0x060023A6 RID: 9126 RVA: 0x00029688 File Offset: 0x00027888
		public Vector2 mousePosition
		{
			get
			{
				Vector2 result;
				this.Internal_GetMousePosition(out result);
				return result;
			}
			set
			{
				this.Internal_SetMousePosition(value);
			}
		}

		// Token: 0x170008EA RID: 2282
		// (get) Token: 0x060023A7 RID: 9127 RVA: 0x00029694 File Offset: 0x00027894
		// (set) Token: 0x060023A8 RID: 9128 RVA: 0x000296B4 File Offset: 0x000278B4
		public Vector2 delta
		{
			get
			{
				Vector2 result;
				this.Internal_GetMouseDelta(out result);
				return result;
			}
			set
			{
				this.Internal_SetMouseDelta(value);
			}
		}

		// Token: 0x170008EB RID: 2283
		// (get) Token: 0x060023A9 RID: 9129 RVA: 0x000296C0 File Offset: 0x000278C0
		// (set) Token: 0x060023AA RID: 9130 RVA: 0x000296E4 File Offset: 0x000278E4
		[Obsolete("Use HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);", true)]
		public Ray mouseRay
		{
			get
			{
				return new Ray(Vector3.up, Vector3.up);
			}
			set
			{
			}
		}

		// Token: 0x170008EC RID: 2284
		// (get) Token: 0x060023AB RID: 9131 RVA: 0x000296E8 File Offset: 0x000278E8
		// (set) Token: 0x060023AC RID: 9132 RVA: 0x0002970C File Offset: 0x0002790C
		public bool shift
		{
			get
			{
				return (this.modifiers & EventModifiers.Shift) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Shift;
				}
				else
				{
					this.modifiers |= EventModifiers.Shift;
				}
			}
		}

		// Token: 0x170008ED RID: 2285
		// (get) Token: 0x060023AD RID: 9133 RVA: 0x00029738 File Offset: 0x00027938
		// (set) Token: 0x060023AE RID: 9134 RVA: 0x0002975C File Offset: 0x0002795C
		public bool control
		{
			get
			{
				return (this.modifiers & EventModifiers.Control) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Control;
				}
				else
				{
					this.modifiers |= EventModifiers.Control;
				}
			}
		}

		// Token: 0x170008EE RID: 2286
		// (get) Token: 0x060023AF RID: 9135 RVA: 0x00029788 File Offset: 0x00027988
		// (set) Token: 0x060023B0 RID: 9136 RVA: 0x000297AC File Offset: 0x000279AC
		public bool alt
		{
			get
			{
				return (this.modifiers & EventModifiers.Alt) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Alt;
				}
				else
				{
					this.modifiers |= EventModifiers.Alt;
				}
			}
		}

		// Token: 0x170008EF RID: 2287
		// (get) Token: 0x060023B1 RID: 9137 RVA: 0x000297D8 File Offset: 0x000279D8
		// (set) Token: 0x060023B2 RID: 9138 RVA: 0x000297FC File Offset: 0x000279FC
		public bool command
		{
			get
			{
				return (this.modifiers & EventModifiers.Command) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Command;
				}
				else
				{
					this.modifiers |= EventModifiers.Command;
				}
			}
		}

		// Token: 0x170008F0 RID: 2288
		// (get) Token: 0x060023B3 RID: 9139 RVA: 0x00029828 File Offset: 0x00027A28
		// (set) Token: 0x060023B4 RID: 9140 RVA: 0x0002984C File Offset: 0x00027A4C
		public bool capsLock
		{
			get
			{
				return (this.modifiers & EventModifiers.CapsLock) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.CapsLock;
				}
				else
				{
					this.modifiers |= EventModifiers.CapsLock;
				}
			}
		}

		// Token: 0x170008F1 RID: 2289
		// (get) Token: 0x060023B5 RID: 9141 RVA: 0x00029878 File Offset: 0x00027A78
		// (set) Token: 0x060023B6 RID: 9142 RVA: 0x0002989C File Offset: 0x00027A9C
		public bool numeric
		{
			get
			{
				return (this.modifiers & EventModifiers.Numeric) != EventModifiers.None;
			}
			set
			{
				if (!value)
				{
					this.modifiers &= ~EventModifiers.Shift;
				}
				else
				{
					this.modifiers |= EventModifiers.Shift;
				}
			}
		}

		// Token: 0x170008F2 RID: 2290
		// (get) Token: 0x060023B7 RID: 9143 RVA: 0x000298C8 File Offset: 0x00027AC8
		public bool functionKey
		{
			get
			{
				return (this.modifiers & EventModifiers.FunctionKey) != EventModifiers.None;
			}
		}

		// Token: 0x170008F3 RID: 2291
		// (get) Token: 0x060023B8 RID: 9144 RVA: 0x000298EC File Offset: 0x00027AEC
		// (set) Token: 0x060023B9 RID: 9145 RVA: 0x00029908 File Offset: 0x00027B08
		public static Event current
		{
			get
			{
				return Event.s_Current;
			}
			set
			{
				if (value != null)
				{
					Event.s_Current = value;
				}
				else
				{
					Event.s_Current = Event.s_MasterEvent;
				}
				Event.Internal_SetNativeEvent(Event.s_Current.m_Ptr);
			}
		}

		// Token: 0x060023BA RID: 9146 RVA: 0x00029938 File Offset: 0x00027B38
		[RequiredByNativeCode]
		private static void Internal_MakeMasterEventCurrent(int displayIndex)
		{
			if (Event.s_MasterEvent == null)
			{
				Event.s_MasterEvent = new Event(displayIndex);
			}
			Event.s_MasterEvent.displayIndex = displayIndex;
			Event.s_Current = Event.s_MasterEvent;
			Event.Internal_SetNativeEvent(Event.s_MasterEvent.m_Ptr);
		}

		// Token: 0x170008F4 RID: 2292
		// (get) Token: 0x060023BB RID: 9147 RVA: 0x00029974 File Offset: 0x00027B74
		public bool isKey
		{
			get
			{
				EventType type = this.type;
				return type == EventType.KeyDown || type == EventType.KeyUp;
			}
		}

		// Token: 0x170008F5 RID: 2293
		// (get) Token: 0x060023BC RID: 9148 RVA: 0x000299A0 File Offset: 0x00027BA0
		public bool isMouse
		{
			get
			{
				EventType type = this.type;
				return type == EventType.MouseMove || type == EventType.MouseDown || type == EventType.MouseUp || type == EventType.MouseDrag;
			}
		}

		// Token: 0x060023BD RID: 9149 RVA: 0x000299D8 File Offset: 0x00027BD8
		public static Event KeyboardEvent(string key)
		{
			Event @event = new Event(0);
			@event.type = EventType.KeyDown;
			Event result;
			if (string.IsNullOrEmpty(key))
			{
				result = @event;
			}
			else
			{
				int num = 0;
				bool flag;
				do
				{
					flag = true;
					if (num >= key.Length)
					{
						break;
					}
					char c = key[num];
					switch (c)
					{
					case '#':
						@event.modifiers |= EventModifiers.Shift;
						num++;
						break;
					default:
						if (c != '^')
						{
							flag = false;
						}
						else
						{
							@event.modifiers |= EventModifiers.Control;
							num++;
						}
						break;
					case '%':
						@event.modifiers |= EventModifiers.Command;
						num++;
						break;
					case '&':
						@event.modifiers |= EventModifiers.Alt;
						num++;
						break;
					}
				}
				while (flag);
				string text = key.Substring(num, key.Length - num).ToLower();
				switch (text)
				{
				case "[0]":
					@event.character = '0';
					@event.keyCode = KeyCode.Keypad0;
					goto IL_A83;
				case "[1]":
					@event.character = '1';
					@event.keyCode = KeyCode.Keypad1;
					goto IL_A83;
				case "[2]":
					@event.character = '2';
					@event.keyCode = KeyCode.Keypad2;
					goto IL_A83;
				case "[3]":
					@event.character = '3';
					@event.keyCode = KeyCode.Keypad3;
					goto IL_A83;
				case "[4]":
					@event.character = '4';
					@event.keyCode = KeyCode.Keypad4;
					goto IL_A83;
				case "[5]":
					@event.character = '5';
					@event.keyCode = KeyCode.Keypad5;
					goto IL_A83;
				case "[6]":
					@event.character = '6';
					@event.keyCode = KeyCode.Keypad6;
					goto IL_A83;
				case "[7]":
					@event.character = '7';
					@event.keyCode = KeyCode.Keypad7;
					goto IL_A83;
				case "[8]":
					@event.character = '8';
					@event.keyCode = KeyCode.Keypad8;
					goto IL_A83;
				case "[9]":
					@event.character = '9';
					@event.keyCode = KeyCode.Keypad9;
					goto IL_A83;
				case "[.]":
					@event.character = '.';
					@event.keyCode = KeyCode.KeypadPeriod;
					goto IL_A83;
				case "[/]":
					@event.character = '/';
					@event.keyCode = KeyCode.KeypadDivide;
					goto IL_A83;
				case "[-]":
					@event.character = '-';
					@event.keyCode = KeyCode.KeypadMinus;
					goto IL_A83;
				case "[+]":
					@event.character = '+';
					@event.keyCode = KeyCode.KeypadPlus;
					goto IL_A83;
				case "[=]":
					@event.character = '=';
					@event.keyCode = KeyCode.KeypadEquals;
					goto IL_A83;
				case "[equals]":
					@event.character = '=';
					@event.keyCode = KeyCode.KeypadEquals;
					goto IL_A83;
				case "[enter]":
					@event.character = '\n';
					@event.keyCode = KeyCode.KeypadEnter;
					goto IL_A83;
				case "up":
					@event.keyCode = KeyCode.UpArrow;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "down":
					@event.keyCode = KeyCode.DownArrow;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "left":
					@event.keyCode = KeyCode.LeftArrow;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "right":
					@event.keyCode = KeyCode.RightArrow;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "insert":
					@event.keyCode = KeyCode.Insert;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "home":
					@event.keyCode = KeyCode.Home;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "end":
					@event.keyCode = KeyCode.End;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "pgup":
					@event.keyCode = KeyCode.PageDown;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "page up":
					@event.keyCode = KeyCode.PageUp;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "pgdown":
					@event.keyCode = KeyCode.PageUp;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "page down":
					@event.keyCode = KeyCode.PageDown;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "backspace":
					@event.keyCode = KeyCode.Backspace;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "delete":
					@event.keyCode = KeyCode.Delete;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "tab":
					@event.keyCode = KeyCode.Tab;
					goto IL_A83;
				case "f1":
					@event.keyCode = KeyCode.F1;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f2":
					@event.keyCode = KeyCode.F2;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f3":
					@event.keyCode = KeyCode.F3;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f4":
					@event.keyCode = KeyCode.F4;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f5":
					@event.keyCode = KeyCode.F5;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f6":
					@event.keyCode = KeyCode.F6;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f7":
					@event.keyCode = KeyCode.F7;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f8":
					@event.keyCode = KeyCode.F8;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f9":
					@event.keyCode = KeyCode.F9;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f10":
					@event.keyCode = KeyCode.F10;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f11":
					@event.keyCode = KeyCode.F11;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f12":
					@event.keyCode = KeyCode.F12;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f13":
					@event.keyCode = KeyCode.F13;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f14":
					@event.keyCode = KeyCode.F14;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "f15":
					@event.keyCode = KeyCode.F15;
					@event.modifiers |= EventModifiers.FunctionKey;
					goto IL_A83;
				case "[esc]":
					@event.keyCode = KeyCode.Escape;
					goto IL_A83;
				case "return":
					@event.character = '\n';
					@event.keyCode = KeyCode.Return;
					@event.modifiers &= ~EventModifiers.FunctionKey;
					goto IL_A83;
				case "space":
					@event.keyCode = KeyCode.Space;
					@event.character = ' ';
					@event.modifiers &= ~EventModifiers.FunctionKey;
					goto IL_A83;
				}
				if (text.Length != 1)
				{
					try
					{
						@event.keyCode = (KeyCode)Enum.Parse(typeof(KeyCode), text, true);
					}
					catch (ArgumentException)
					{
						Debug.LogError(UnityString.Format("Unable to find key name that matches '{0}'", new object[]
						{
							text
						}));
					}
				}
				else
				{
					@event.character = text.ToLower()[0];
					@event.keyCode = (KeyCode)@event.character;
					if (@event.modifiers != EventModifiers.None)
					{
						@event.character = '\0';
					}
				}
				IL_A83:
				result = @event;
			}
			return result;
		}

		// Token: 0x060023BE RID: 9150 RVA: 0x0002A480 File Offset: 0x00028680
		public override int GetHashCode()
		{
			int num = 1;
			if (this.isKey)
			{
				num = (int)((ushort)this.keyCode);
			}
			if (this.isMouse)
			{
				num = this.mousePosition.GetHashCode();
			}
			return num * 37 | (int)this.modifiers;
		}

		// Token: 0x060023BF RID: 9151 RVA: 0x0002A4D8 File Offset: 0x000286D8
		public override bool Equals(object obj)
		{
			bool result;
			if (obj == null)
			{
				result = false;
			}
			else if (object.ReferenceEquals(this, obj))
			{
				result = true;
			}
			else if (obj.GetType() != base.GetType())
			{
				result = false;
			}
			else
			{
				Event @event = (Event)obj;
				if (this.type != @event.type || (this.modifiers & ~EventModifiers.CapsLock) != (@event.modifiers & ~EventModifiers.CapsLock))
				{
					result = false;
				}
				else if (this.isKey)
				{
					result = (this.keyCode == @event.keyCode);
				}
				else
				{
					result = (this.isMouse && this.mousePosition == @event.mousePosition);
				}
			}
			return result;
		}

		// Token: 0x060023C0 RID: 9152 RVA: 0x0002A5A0 File Offset: 0x000287A0
		public override string ToString()
		{
			string result;
			if (this.isKey)
			{
				if (this.character == '\0')
				{
					result = UnityString.Format("Event:{0}   Character:\\0   Modifiers:{1}   KeyCode:{2}", new object[]
					{
						this.type,
						this.modifiers,
						this.keyCode
					});
				}
				else
				{
					result = string.Concat(new object[]
					{
						"Event:",
						this.type,
						"   Character:",
						(int)this.character,
						"   Modifiers:",
						this.modifiers,
						"   KeyCode:",
						this.keyCode
					});
				}
			}
			else if (this.isMouse)
			{
				result = UnityString.Format("Event: {0}   Position: {1} Modifiers: {2}", new object[]
				{
					this.type,
					this.mousePosition,
					this.modifiers
				});
			}
			else if (this.type == EventType.ExecuteCommand || this.type == EventType.ValidateCommand)
			{
				result = UnityString.Format("Event: {0}  \"{1}\"", new object[]
				{
					this.type,
					this.commandName
				});
			}
			else
			{
				result = "" + this.type;
			}
			return result;
		}

		// Token: 0x060023C1 RID: 9153 RVA: 0x0002A71C File Offset: 0x0002891C
		public void Use()
		{
			if (this.type == EventType.Repaint || this.type == EventType.Layout)
			{
				Debug.LogWarning(UnityString.Format("Event.Use() should not be called for events of type {0}", new object[]
				{
					this.type
				}));
			}
			this.Internal_Use();
		}

		// Token: 0x060023C2 RID: 9154
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Init(int displayIndex);

		// Token: 0x060023C3 RID: 9155
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Cleanup();

		// Token: 0x060023C4 RID: 9156
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InitCopy(Event other);

		// Token: 0x060023C5 RID: 9157
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InitPtr(IntPtr ptr);

		// Token: 0x170008F6 RID: 2294
		// (get) Token: 0x060023C6 RID: 9158
		public extern EventType rawType { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008F7 RID: 2295
		// (get) Token: 0x060023C7 RID: 9159
		// (set) Token: 0x060023C8 RID: 9160
		public extern EventType type { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060023C9 RID: 9161
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern EventType GetTypeForControl(int controlID);

		// Token: 0x060023CA RID: 9162 RVA: 0x0002A770 File Offset: 0x00028970
		private void Internal_SetMousePosition(Vector2 value)
		{
			Event.INTERNAL_CALL_Internal_SetMousePosition(this, ref value);
		}

		// Token: 0x060023CB RID: 9163
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_SetMousePosition(Event self, ref Vector2 value);

		// Token: 0x060023CC RID: 9164
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetMousePosition(out Vector2 value);

		// Token: 0x060023CD RID: 9165 RVA: 0x0002A77C File Offset: 0x0002897C
		private void Internal_SetMouseDelta(Vector2 value)
		{
			Event.INTERNAL_CALL_Internal_SetMouseDelta(this, ref value);
		}

		// Token: 0x060023CE RID: 9166
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_SetMouseDelta(Event self, ref Vector2 value);

		// Token: 0x060023CF RID: 9167
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetMouseDelta(out Vector2 value);

		// Token: 0x170008F8 RID: 2296
		// (get) Token: 0x060023D0 RID: 9168
		// (set) Token: 0x060023D1 RID: 9169
		public extern int button { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008F9 RID: 2297
		// (get) Token: 0x060023D2 RID: 9170
		// (set) Token: 0x060023D3 RID: 9171
		public extern EventModifiers modifiers { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008FA RID: 2298
		// (get) Token: 0x060023D4 RID: 9172
		// (set) Token: 0x060023D5 RID: 9173
		public extern float pressure { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008FB RID: 2299
		// (get) Token: 0x060023D6 RID: 9174
		// (set) Token: 0x060023D7 RID: 9175
		public extern int clickCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008FC RID: 2300
		// (get) Token: 0x060023D8 RID: 9176
		// (set) Token: 0x060023D9 RID: 9177
		public extern char character { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008FD RID: 2301
		// (get) Token: 0x060023DA RID: 9178
		// (set) Token: 0x060023DB RID: 9179
		public extern string commandName { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008FE RID: 2302
		// (get) Token: 0x060023DC RID: 9180
		// (set) Token: 0x060023DD RID: 9181
		public extern KeyCode keyCode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060023DE RID: 9182
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_SetNativeEvent(IntPtr ptr);

		// Token: 0x170008FF RID: 2303
		// (get) Token: 0x060023DF RID: 9183
		// (set) Token: 0x060023E0 RID: 9184
		public extern int displayIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060023E1 RID: 9185
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_Use();

		// Token: 0x060023E2 RID: 9186
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool PopEvent(Event outEvent);

		// Token: 0x060023E3 RID: 9187
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetEventCount();

		// Token: 0x04000642 RID: 1602
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x04000643 RID: 1603
		private static Event s_Current;

		// Token: 0x04000644 RID: 1604
		private static Event s_MasterEvent;
	}
}
