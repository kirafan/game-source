﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200005D RID: 93
	public sealed class MeshRenderer : Renderer
	{
		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000703 RID: 1795
		// (set) Token: 0x06000704 RID: 1796
		public extern Mesh additionalVertexStreams { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
