﻿using System;

namespace UnityEngine
{
	// Token: 0x020001E0 RID: 480
	public enum HumanBodyBones
	{
		// Token: 0x04000567 RID: 1383
		Hips,
		// Token: 0x04000568 RID: 1384
		LeftUpperLeg,
		// Token: 0x04000569 RID: 1385
		RightUpperLeg,
		// Token: 0x0400056A RID: 1386
		LeftLowerLeg,
		// Token: 0x0400056B RID: 1387
		RightLowerLeg,
		// Token: 0x0400056C RID: 1388
		LeftFoot,
		// Token: 0x0400056D RID: 1389
		RightFoot,
		// Token: 0x0400056E RID: 1390
		Spine,
		// Token: 0x0400056F RID: 1391
		Chest,
		// Token: 0x04000570 RID: 1392
		Neck,
		// Token: 0x04000571 RID: 1393
		Head,
		// Token: 0x04000572 RID: 1394
		LeftShoulder,
		// Token: 0x04000573 RID: 1395
		RightShoulder,
		// Token: 0x04000574 RID: 1396
		LeftUpperArm,
		// Token: 0x04000575 RID: 1397
		RightUpperArm,
		// Token: 0x04000576 RID: 1398
		LeftLowerArm,
		// Token: 0x04000577 RID: 1399
		RightLowerArm,
		// Token: 0x04000578 RID: 1400
		LeftHand,
		// Token: 0x04000579 RID: 1401
		RightHand,
		// Token: 0x0400057A RID: 1402
		LeftToes,
		// Token: 0x0400057B RID: 1403
		RightToes,
		// Token: 0x0400057C RID: 1404
		LeftEye,
		// Token: 0x0400057D RID: 1405
		RightEye,
		// Token: 0x0400057E RID: 1406
		Jaw,
		// Token: 0x0400057F RID: 1407
		LeftThumbProximal,
		// Token: 0x04000580 RID: 1408
		LeftThumbIntermediate,
		// Token: 0x04000581 RID: 1409
		LeftThumbDistal,
		// Token: 0x04000582 RID: 1410
		LeftIndexProximal,
		// Token: 0x04000583 RID: 1411
		LeftIndexIntermediate,
		// Token: 0x04000584 RID: 1412
		LeftIndexDistal,
		// Token: 0x04000585 RID: 1413
		LeftMiddleProximal,
		// Token: 0x04000586 RID: 1414
		LeftMiddleIntermediate,
		// Token: 0x04000587 RID: 1415
		LeftMiddleDistal,
		// Token: 0x04000588 RID: 1416
		LeftRingProximal,
		// Token: 0x04000589 RID: 1417
		LeftRingIntermediate,
		// Token: 0x0400058A RID: 1418
		LeftRingDistal,
		// Token: 0x0400058B RID: 1419
		LeftLittleProximal,
		// Token: 0x0400058C RID: 1420
		LeftLittleIntermediate,
		// Token: 0x0400058D RID: 1421
		LeftLittleDistal,
		// Token: 0x0400058E RID: 1422
		RightThumbProximal,
		// Token: 0x0400058F RID: 1423
		RightThumbIntermediate,
		// Token: 0x04000590 RID: 1424
		RightThumbDistal,
		// Token: 0x04000591 RID: 1425
		RightIndexProximal,
		// Token: 0x04000592 RID: 1426
		RightIndexIntermediate,
		// Token: 0x04000593 RID: 1427
		RightIndexDistal,
		// Token: 0x04000594 RID: 1428
		RightMiddleProximal,
		// Token: 0x04000595 RID: 1429
		RightMiddleIntermediate,
		// Token: 0x04000596 RID: 1430
		RightMiddleDistal,
		// Token: 0x04000597 RID: 1431
		RightRingProximal,
		// Token: 0x04000598 RID: 1432
		RightRingIntermediate,
		// Token: 0x04000599 RID: 1433
		RightRingDistal,
		// Token: 0x0400059A RID: 1434
		RightLittleProximal,
		// Token: 0x0400059B RID: 1435
		RightLittleIntermediate,
		// Token: 0x0400059C RID: 1436
		RightLittleDistal,
		// Token: 0x0400059D RID: 1437
		LastBone
	}
}
