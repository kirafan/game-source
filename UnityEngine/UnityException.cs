﻿using System;
using System.Runtime.Serialization;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200036F RID: 879
	[RequiredByNativeCode]
	[Serializable]
	public class UnityException : SystemException
	{
		// Token: 0x06002E3E RID: 11838 RVA: 0x0004A588 File Offset: 0x00048788
		public UnityException() : base("A Unity Runtime error occurred!")
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E3F RID: 11839 RVA: 0x0004A5A4 File Offset: 0x000487A4
		public UnityException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E40 RID: 11840 RVA: 0x0004A5BC File Offset: 0x000487BC
		public UnityException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E41 RID: 11841 RVA: 0x0004A5D4 File Offset: 0x000487D4
		protected UnityException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000D47 RID: 3399
		private const int Result = -2147467261;

		// Token: 0x04000D48 RID: 3400
		private string unityStackTrace;
	}
}
