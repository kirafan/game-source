﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200036A RID: 874
	[Obsolete("Use SerializeField on the private variables that you want to be serialized instead")]
	[RequiredByNativeCode]
	public sealed class SerializePrivateVariables : Attribute
	{
	}
}
