﻿using System;

namespace UnityEngine
{
	// Token: 0x020002A9 RID: 681
	[Obsolete("This component is part of the legacy particle system, which is deprecated and will be removed in a future release. Use the ParticleSystem component instead.", false)]
	public sealed class EllipsoidParticleEmitter : ParticleEmitter
	{
		// Token: 0x06002B62 RID: 11106 RVA: 0x00041AE4 File Offset: 0x0003FCE4
		internal EllipsoidParticleEmitter()
		{
		}
	}
}
