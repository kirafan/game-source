﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000048 RID: 72
	public sealed class LensFlare : Behaviour
	{
		// Token: 0x17000128 RID: 296
		// (get) Token: 0x06000531 RID: 1329
		// (set) Token: 0x06000532 RID: 1330
		public extern Flare flare { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x06000533 RID: 1331
		// (set) Token: 0x06000534 RID: 1332
		public extern float brightness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x06000535 RID: 1333
		// (set) Token: 0x06000536 RID: 1334
		public extern float fadeSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700012B RID: 299
		// (get) Token: 0x06000537 RID: 1335 RVA: 0x00007AB8 File Offset: 0x00005CB8
		// (set) Token: 0x06000538 RID: 1336 RVA: 0x00007AD8 File Offset: 0x00005CD8
		public Color color
		{
			get
			{
				Color result;
				this.INTERNAL_get_color(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_color(ref value);
			}
		}

		// Token: 0x06000539 RID: 1337
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_color(out Color value);

		// Token: 0x0600053A RID: 1338
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_color(ref Color value);
	}
}
