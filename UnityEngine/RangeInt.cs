﻿using System;

namespace UnityEngine
{
	// Token: 0x02000361 RID: 865
	public struct RangeInt
	{
		// Token: 0x06002DE6 RID: 11750 RVA: 0x00048FC0 File Offset: 0x000471C0
		public RangeInt(int start, int length)
		{
			this.start = start;
			this.length = length;
		}

		// Token: 0x17000ADF RID: 2783
		// (get) Token: 0x06002DE7 RID: 11751 RVA: 0x00048FD4 File Offset: 0x000471D4
		public int end
		{
			get
			{
				return this.start + this.length;
			}
		}

		// Token: 0x04000D36 RID: 3382
		public int start;

		// Token: 0x04000D37 RID: 3383
		public int length;
	}
}
