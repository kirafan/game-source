﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001A9 RID: 425
	public sealed class AudioLowPassFilter : Behaviour
	{
		// Token: 0x1700074A RID: 1866
		// (get) Token: 0x06001D99 RID: 7577
		// (set) Token: 0x06001D9A RID: 7578
		public extern float cutoffFrequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700074B RID: 1867
		// (get) Token: 0x06001D9B RID: 7579
		// (set) Token: 0x06001D9C RID: 7580
		public extern AnimationCurve customCutoffCurve { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700074C RID: 1868
		// (get) Token: 0x06001D9D RID: 7581
		// (set) Token: 0x06001D9E RID: 7582
		public extern float lowpassResonanceQ { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
