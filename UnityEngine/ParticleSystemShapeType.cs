﻿using System;

namespace UnityEngine
{
	// Token: 0x020000FE RID: 254
	public enum ParticleSystemShapeType
	{
		// Token: 0x040002A9 RID: 681
		Sphere,
		// Token: 0x040002AA RID: 682
		SphereShell,
		// Token: 0x040002AB RID: 683
		Hemisphere,
		// Token: 0x040002AC RID: 684
		HemisphereShell,
		// Token: 0x040002AD RID: 685
		Cone,
		// Token: 0x040002AE RID: 686
		Box,
		// Token: 0x040002AF RID: 687
		Mesh,
		// Token: 0x040002B0 RID: 688
		ConeShell,
		// Token: 0x040002B1 RID: 689
		ConeVolume,
		// Token: 0x040002B2 RID: 690
		ConeVolumeShell,
		// Token: 0x040002B3 RID: 691
		Circle,
		// Token: 0x040002B4 RID: 692
		CircleEdge,
		// Token: 0x040002B5 RID: 693
		SingleSidedEdge,
		// Token: 0x040002B6 RID: 694
		MeshRenderer,
		// Token: 0x040002B7 RID: 695
		SkinnedMeshRenderer,
		// Token: 0x040002B8 RID: 696
		BoxShell,
		// Token: 0x040002B9 RID: 697
		BoxEdge
	}
}
