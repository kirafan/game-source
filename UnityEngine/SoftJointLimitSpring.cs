﻿using System;

namespace UnityEngine
{
	// Token: 0x02000137 RID: 311
	public struct SoftJointLimitSpring
	{
		// Token: 0x17000502 RID: 1282
		// (get) Token: 0x0600160A RID: 5642 RVA: 0x0001C090 File Offset: 0x0001A290
		// (set) Token: 0x0600160B RID: 5643 RVA: 0x0001C0AC File Offset: 0x0001A2AC
		public float spring
		{
			get
			{
				return this.m_Spring;
			}
			set
			{
				this.m_Spring = value;
			}
		}

		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x0600160C RID: 5644 RVA: 0x0001C0B8 File Offset: 0x0001A2B8
		// (set) Token: 0x0600160D RID: 5645 RVA: 0x0001C0D4 File Offset: 0x0001A2D4
		public float damper
		{
			get
			{
				return this.m_Damper;
			}
			set
			{
				this.m_Damper = value;
			}
		}

		// Token: 0x0400036E RID: 878
		private float m_Spring;

		// Token: 0x0400036F RID: 879
		private float m_Damper;
	}
}
