﻿using System;

namespace UnityEngine
{
	// Token: 0x02000100 RID: 256
	public enum ParticleSystemAnimationType
	{
		// Token: 0x040002BF RID: 703
		WholeSheet,
		// Token: 0x040002C0 RID: 704
		SingleRow
	}
}
