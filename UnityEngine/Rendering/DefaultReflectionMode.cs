﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200030E RID: 782
	public enum DefaultReflectionMode
	{
		// Token: 0x04000C20 RID: 3104
		Skybox,
		// Token: 0x04000C21 RID: 3105
		Custom
	}
}
