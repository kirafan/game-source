﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200031A RID: 794
	public enum ReflectionProbeType
	{
		// Token: 0x04000C8A RID: 3210
		Cube,
		// Token: 0x04000C8B RID: 3211
		Card
	}
}
