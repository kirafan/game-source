﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000304 RID: 772
	public enum RenderQueue
	{
		// Token: 0x04000BC1 RID: 3009
		Background = 1000,
		// Token: 0x04000BC2 RID: 3010
		Geometry = 2000,
		// Token: 0x04000BC3 RID: 3011
		AlphaTest = 2450,
		// Token: 0x04000BC4 RID: 3012
		Transparent = 3000,
		// Token: 0x04000BC5 RID: 3013
		Overlay = 4000
	}
}
