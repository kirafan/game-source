﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200030C RID: 780
	public enum StencilOp
	{
		// Token: 0x04000C12 RID: 3090
		Keep,
		// Token: 0x04000C13 RID: 3091
		Zero,
		// Token: 0x04000C14 RID: 3092
		Replace,
		// Token: 0x04000C15 RID: 3093
		IncrementSaturate,
		// Token: 0x04000C16 RID: 3094
		DecrementSaturate,
		// Token: 0x04000C17 RID: 3095
		Invert,
		// Token: 0x04000C18 RID: 3096
		IncrementWrap,
		// Token: 0x04000C19 RID: 3097
		DecrementWrap
	}
}
