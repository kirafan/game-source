﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000315 RID: 789
	public enum LightShadowResolution
	{
		// Token: 0x04000C66 RID: 3174
		FromQualitySettings = -1,
		// Token: 0x04000C67 RID: 3175
		Low,
		// Token: 0x04000C68 RID: 3176
		Medium,
		// Token: 0x04000C69 RID: 3177
		High,
		// Token: 0x04000C6A RID: 3178
		VeryHigh
	}
}
