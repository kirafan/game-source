﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000318 RID: 792
	public struct RenderTargetIdentifier
	{
		// Token: 0x06002CAD RID: 11437 RVA: 0x000467F0 File Offset: 0x000449F0
		public RenderTargetIdentifier(BuiltinRenderTextureType type)
		{
			this.m_Type = type;
			this.m_NameID = -1;
			this.m_InstanceID = 0;
		}

		// Token: 0x06002CAE RID: 11438 RVA: 0x00046808 File Offset: 0x00044A08
		public RenderTargetIdentifier(string name)
		{
			this.m_Type = BuiltinRenderTextureType.None;
			this.m_NameID = Shader.PropertyToID(name);
			this.m_InstanceID = 0;
		}

		// Token: 0x06002CAF RID: 11439 RVA: 0x00046828 File Offset: 0x00044A28
		public RenderTargetIdentifier(int nameID)
		{
			this.m_Type = BuiltinRenderTextureType.None;
			this.m_NameID = nameID;
			this.m_InstanceID = 0;
		}

		// Token: 0x06002CB0 RID: 11440 RVA: 0x00046840 File Offset: 0x00044A40
		public RenderTargetIdentifier(Texture tex)
		{
			this.m_Type = ((!(tex == null) && !(tex is RenderTexture)) ? BuiltinRenderTextureType.BindableTexture : BuiltinRenderTextureType.None);
			this.m_NameID = -1;
			this.m_InstanceID = ((!tex) ? 0 : tex.GetInstanceID());
		}

		// Token: 0x06002CB1 RID: 11441 RVA: 0x00046898 File Offset: 0x00044A98
		public static implicit operator RenderTargetIdentifier(BuiltinRenderTextureType type)
		{
			return new RenderTargetIdentifier(type);
		}

		// Token: 0x06002CB2 RID: 11442 RVA: 0x000468B4 File Offset: 0x00044AB4
		public static implicit operator RenderTargetIdentifier(string name)
		{
			return new RenderTargetIdentifier(name);
		}

		// Token: 0x06002CB3 RID: 11443 RVA: 0x000468D0 File Offset: 0x00044AD0
		public static implicit operator RenderTargetIdentifier(int nameID)
		{
			return new RenderTargetIdentifier(nameID);
		}

		// Token: 0x06002CB4 RID: 11444 RVA: 0x000468EC File Offset: 0x00044AEC
		public static implicit operator RenderTargetIdentifier(Texture tex)
		{
			return new RenderTargetIdentifier(tex);
		}

		// Token: 0x04000C81 RID: 3201
		private BuiltinRenderTextureType m_Type;

		// Token: 0x04000C82 RID: 3202
		private int m_NameID;

		// Token: 0x04000C83 RID: 3203
		private int m_InstanceID;
	}
}
