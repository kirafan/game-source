﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000323 RID: 803
	public enum BuiltinShaderMode
	{
		// Token: 0x04000CAE RID: 3246
		Disabled,
		// Token: 0x04000CAF RID: 3247
		UseBuiltin,
		// Token: 0x04000CB0 RID: 3248
		UseCustom
	}
}
