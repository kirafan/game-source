﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200030A RID: 778
	public enum CullMode
	{
		// Token: 0x04000C08 RID: 3080
		Off,
		// Token: 0x04000C09 RID: 3081
		Front,
		// Token: 0x04000C0A RID: 3082
		Back
	}
}
