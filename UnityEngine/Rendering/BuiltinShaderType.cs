﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000322 RID: 802
	public enum BuiltinShaderType
	{
		// Token: 0x04000CA5 RID: 3237
		DeferredShading,
		// Token: 0x04000CA6 RID: 3238
		DeferredReflections,
		// Token: 0x04000CA7 RID: 3239
		LegacyDeferredLighting,
		// Token: 0x04000CA8 RID: 3240
		ScreenSpaceShadows,
		// Token: 0x04000CA9 RID: 3241
		DepthNormals,
		// Token: 0x04000CAA RID: 3242
		MotionVectors,
		// Token: 0x04000CAB RID: 3243
		LightHalo,
		// Token: 0x04000CAC RID: 3244
		LensFlare
	}
}
