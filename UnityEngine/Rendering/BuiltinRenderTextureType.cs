﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000312 RID: 786
	public enum BuiltinRenderTextureType
	{
		// Token: 0x04000C44 RID: 3140
		BindableTexture = -1,
		// Token: 0x04000C45 RID: 3141
		None,
		// Token: 0x04000C46 RID: 3142
		CurrentActive,
		// Token: 0x04000C47 RID: 3143
		CameraTarget,
		// Token: 0x04000C48 RID: 3144
		Depth,
		// Token: 0x04000C49 RID: 3145
		DepthNormals,
		// Token: 0x04000C4A RID: 3146
		ResolvedDepth,
		// Token: 0x04000C4B RID: 3147
		PrepassNormalsSpec = 7,
		// Token: 0x04000C4C RID: 3148
		PrepassLight,
		// Token: 0x04000C4D RID: 3149
		PrepassLightSpec,
		// Token: 0x04000C4E RID: 3150
		GBuffer0,
		// Token: 0x04000C4F RID: 3151
		GBuffer1,
		// Token: 0x04000C50 RID: 3152
		GBuffer2,
		// Token: 0x04000C51 RID: 3153
		GBuffer3,
		// Token: 0x04000C52 RID: 3154
		Reflections
	}
}
