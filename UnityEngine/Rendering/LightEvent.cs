﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000311 RID: 785
	public enum LightEvent
	{
		// Token: 0x04000C3F RID: 3135
		BeforeShadowMap,
		// Token: 0x04000C40 RID: 3136
		AfterShadowMap,
		// Token: 0x04000C41 RID: 3137
		BeforeScreenspaceMask,
		// Token: 0x04000C42 RID: 3138
		AfterScreenspaceMask
	}
}
