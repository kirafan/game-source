﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000321 RID: 801
	public enum LightProbeUsage
	{
		// Token: 0x04000CA1 RID: 3233
		Off,
		// Token: 0x04000CA2 RID: 3234
		BlendProbes,
		// Token: 0x04000CA3 RID: 3235
		UseProxyVolume
	}
}
