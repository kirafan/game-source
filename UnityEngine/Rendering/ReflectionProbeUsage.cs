﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000319 RID: 793
	public enum ReflectionProbeUsage
	{
		// Token: 0x04000C85 RID: 3205
		Off,
		// Token: 0x04000C86 RID: 3206
		BlendProbes,
		// Token: 0x04000C87 RID: 3207
		BlendProbesAndSkybox,
		// Token: 0x04000C88 RID: 3208
		Simple
	}
}
