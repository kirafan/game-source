﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000314 RID: 788
	public enum ShadowCastingMode
	{
		// Token: 0x04000C61 RID: 3169
		Off,
		// Token: 0x04000C62 RID: 3170
		On,
		// Token: 0x04000C63 RID: 3171
		TwoSided,
		// Token: 0x04000C64 RID: 3172
		ShadowsOnly
	}
}
