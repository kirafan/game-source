﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000308 RID: 776
	public enum BlendOp
	{
		// Token: 0x04000BD9 RID: 3033
		Add,
		// Token: 0x04000BDA RID: 3034
		Subtract,
		// Token: 0x04000BDB RID: 3035
		ReverseSubtract,
		// Token: 0x04000BDC RID: 3036
		Min,
		// Token: 0x04000BDD RID: 3037
		Max,
		// Token: 0x04000BDE RID: 3038
		LogicalClear,
		// Token: 0x04000BDF RID: 3039
		LogicalSet,
		// Token: 0x04000BE0 RID: 3040
		LogicalCopy,
		// Token: 0x04000BE1 RID: 3041
		LogicalCopyInverted,
		// Token: 0x04000BE2 RID: 3042
		LogicalNoop,
		// Token: 0x04000BE3 RID: 3043
		LogicalInvert,
		// Token: 0x04000BE4 RID: 3044
		LogicalAnd,
		// Token: 0x04000BE5 RID: 3045
		LogicalNand,
		// Token: 0x04000BE6 RID: 3046
		LogicalOr,
		// Token: 0x04000BE7 RID: 3047
		LogicalNor,
		// Token: 0x04000BE8 RID: 3048
		LogicalXor,
		// Token: 0x04000BE9 RID: 3049
		LogicalEquivalence,
		// Token: 0x04000BEA RID: 3050
		LogicalAndReverse,
		// Token: 0x04000BEB RID: 3051
		LogicalAndInverted,
		// Token: 0x04000BEC RID: 3052
		LogicalOrReverse,
		// Token: 0x04000BED RID: 3053
		LogicalOrInverted,
		// Token: 0x04000BEE RID: 3054
		Multiply,
		// Token: 0x04000BEF RID: 3055
		Screen,
		// Token: 0x04000BF0 RID: 3056
		Overlay,
		// Token: 0x04000BF1 RID: 3057
		Darken,
		// Token: 0x04000BF2 RID: 3058
		Lighten,
		// Token: 0x04000BF3 RID: 3059
		ColorDodge,
		// Token: 0x04000BF4 RID: 3060
		ColorBurn,
		// Token: 0x04000BF5 RID: 3061
		HardLight,
		// Token: 0x04000BF6 RID: 3062
		SoftLight,
		// Token: 0x04000BF7 RID: 3063
		Difference,
		// Token: 0x04000BF8 RID: 3064
		Exclusion,
		// Token: 0x04000BF9 RID: 3065
		HSLHue,
		// Token: 0x04000BFA RID: 3066
		HSLSaturation,
		// Token: 0x04000BFB RID: 3067
		HSLColor,
		// Token: 0x04000BFC RID: 3068
		HSLLuminosity
	}
}
