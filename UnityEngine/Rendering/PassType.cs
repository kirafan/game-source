﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000313 RID: 787
	public enum PassType
	{
		// Token: 0x04000C54 RID: 3156
		Normal,
		// Token: 0x04000C55 RID: 3157
		Vertex,
		// Token: 0x04000C56 RID: 3158
		VertexLM,
		// Token: 0x04000C57 RID: 3159
		VertexLMRGBM,
		// Token: 0x04000C58 RID: 3160
		ForwardBase,
		// Token: 0x04000C59 RID: 3161
		ForwardAdd,
		// Token: 0x04000C5A RID: 3162
		LightPrePassBase,
		// Token: 0x04000C5B RID: 3163
		LightPrePassFinal,
		// Token: 0x04000C5C RID: 3164
		ShadowCaster,
		// Token: 0x04000C5D RID: 3165
		Deferred = 10,
		// Token: 0x04000C5E RID: 3166
		Meta,
		// Token: 0x04000C5F RID: 3167
		MotionVectors
	}
}
