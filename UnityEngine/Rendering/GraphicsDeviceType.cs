﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x02000316 RID: 790
	[UsedByNativeCode]
	public enum GraphicsDeviceType
	{
		// Token: 0x04000C6C RID: 3180
		[Obsolete("OpenGL2 is no longer supported in Unity 5.5+")]
		OpenGL2,
		// Token: 0x04000C6D RID: 3181
		Direct3D9,
		// Token: 0x04000C6E RID: 3182
		Direct3D11,
		// Token: 0x04000C6F RID: 3183
		[Obsolete("PS3 is no longer supported in Unity 5.5+")]
		PlayStation3,
		// Token: 0x04000C70 RID: 3184
		Null,
		// Token: 0x04000C71 RID: 3185
		[Obsolete("Xbox360 is no longer supported in Unity 5.5+")]
		Xbox360 = 6,
		// Token: 0x04000C72 RID: 3186
		OpenGLES2 = 8,
		// Token: 0x04000C73 RID: 3187
		OpenGLES3 = 11,
		// Token: 0x04000C74 RID: 3188
		PlayStationVita,
		// Token: 0x04000C75 RID: 3189
		PlayStation4,
		// Token: 0x04000C76 RID: 3190
		XboxOne,
		// Token: 0x04000C77 RID: 3191
		PlayStationMobile,
		// Token: 0x04000C78 RID: 3192
		Metal,
		// Token: 0x04000C79 RID: 3193
		OpenGLCore,
		// Token: 0x04000C7A RID: 3194
		Direct3D12,
		// Token: 0x04000C7B RID: 3195
		N3DS,
		// Token: 0x04000C7C RID: 3196
		Vulkan = 21
	}
}
