﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000307 RID: 775
	public enum BlendMode
	{
		// Token: 0x04000BCD RID: 3021
		Zero,
		// Token: 0x04000BCE RID: 3022
		One,
		// Token: 0x04000BCF RID: 3023
		DstColor,
		// Token: 0x04000BD0 RID: 3024
		SrcColor,
		// Token: 0x04000BD1 RID: 3025
		OneMinusDstColor,
		// Token: 0x04000BD2 RID: 3026
		SrcAlpha,
		// Token: 0x04000BD3 RID: 3027
		OneMinusSrcColor,
		// Token: 0x04000BD4 RID: 3028
		DstAlpha,
		// Token: 0x04000BD5 RID: 3029
		OneMinusDstAlpha,
		// Token: 0x04000BD6 RID: 3030
		SrcAlphaSaturate,
		// Token: 0x04000BD7 RID: 3031
		OneMinusSrcAlpha
	}
}
