﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000306 RID: 774
	public enum RenderBufferStoreAction
	{
		// Token: 0x04000BCA RID: 3018
		Store,
		// Token: 0x04000BCB RID: 3019
		DontCare
	}
}
