﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x020000F6 RID: 246
	[Flags]
	public enum UVChannelFlags
	{
		// Token: 0x04000281 RID: 641
		UV0 = 1,
		// Token: 0x04000282 RID: 642
		UV1 = 2,
		// Token: 0x04000283 RID: 643
		UV2 = 4,
		// Token: 0x04000284 RID: 644
		UV3 = 8
	}
}
