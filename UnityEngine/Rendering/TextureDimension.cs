﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000324 RID: 804
	public enum TextureDimension
	{
		// Token: 0x04000CB2 RID: 3250
		Unknown = -1,
		// Token: 0x04000CB3 RID: 3251
		None,
		// Token: 0x04000CB4 RID: 3252
		Any,
		// Token: 0x04000CB5 RID: 3253
		Tex2D,
		// Token: 0x04000CB6 RID: 3254
		Tex3D,
		// Token: 0x04000CB7 RID: 3255
		Cube,
		// Token: 0x04000CB8 RID: 3256
		Tex2DArray,
		// Token: 0x04000CB9 RID: 3257
		CubeArray
	}
}
