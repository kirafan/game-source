﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000310 RID: 784
	public enum CameraEvent
	{
		// Token: 0x04000C27 RID: 3111
		BeforeDepthTexture,
		// Token: 0x04000C28 RID: 3112
		AfterDepthTexture,
		// Token: 0x04000C29 RID: 3113
		BeforeDepthNormalsTexture,
		// Token: 0x04000C2A RID: 3114
		AfterDepthNormalsTexture,
		// Token: 0x04000C2B RID: 3115
		BeforeGBuffer,
		// Token: 0x04000C2C RID: 3116
		AfterGBuffer,
		// Token: 0x04000C2D RID: 3117
		BeforeLighting,
		// Token: 0x04000C2E RID: 3118
		AfterLighting,
		// Token: 0x04000C2F RID: 3119
		BeforeFinalPass,
		// Token: 0x04000C30 RID: 3120
		AfterFinalPass,
		// Token: 0x04000C31 RID: 3121
		BeforeForwardOpaque,
		// Token: 0x04000C32 RID: 3122
		AfterForwardOpaque,
		// Token: 0x04000C33 RID: 3123
		BeforeImageEffectsOpaque,
		// Token: 0x04000C34 RID: 3124
		AfterImageEffectsOpaque,
		// Token: 0x04000C35 RID: 3125
		BeforeSkybox,
		// Token: 0x04000C36 RID: 3126
		AfterSkybox,
		// Token: 0x04000C37 RID: 3127
		BeforeForwardAlpha,
		// Token: 0x04000C38 RID: 3128
		AfterForwardAlpha,
		// Token: 0x04000C39 RID: 3129
		BeforeImageEffects,
		// Token: 0x04000C3A RID: 3130
		AfterImageEffects,
		// Token: 0x04000C3B RID: 3131
		AfterEverything,
		// Token: 0x04000C3C RID: 3132
		BeforeReflections,
		// Token: 0x04000C3D RID: 3133
		AfterReflections
	}
}
