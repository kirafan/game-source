﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Rendering
{
	// Token: 0x020000AD RID: 173
	public sealed class GraphicsSettings : Object
	{
		// Token: 0x06000BC7 RID: 3015
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetShaderMode(BuiltinShaderType type, BuiltinShaderMode mode);

		// Token: 0x06000BC8 RID: 3016
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern BuiltinShaderMode GetShaderMode(BuiltinShaderType type);

		// Token: 0x06000BC9 RID: 3017
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetCustomShader(BuiltinShaderType type, Shader shader);

		// Token: 0x06000BCA RID: 3018
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Shader GetCustomShader(BuiltinShaderType type);

		// Token: 0x06000BCB RID: 3019
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Object GetGraphicsSettings();
	}
}
