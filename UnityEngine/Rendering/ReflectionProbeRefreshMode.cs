﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200031E RID: 798
	public enum ReflectionProbeRefreshMode
	{
		// Token: 0x04000C96 RID: 3222
		OnAwake,
		// Token: 0x04000C97 RID: 3223
		EveryFrame,
		// Token: 0x04000C98 RID: 3224
		ViaScripting
	}
}
