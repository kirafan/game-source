﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Rendering
{
	// Token: 0x020000BA RID: 186
	public sealed class SplashScreen
	{
		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x06000D49 RID: 3401
		public static extern bool isFinished { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000D4A RID: 3402
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Begin();

		// Token: 0x06000D4B RID: 3403
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Draw();
	}
}
