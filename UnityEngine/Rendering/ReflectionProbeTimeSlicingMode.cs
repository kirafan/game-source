﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200031F RID: 799
	public enum ReflectionProbeTimeSlicingMode
	{
		// Token: 0x04000C9A RID: 3226
		AllFacesAtOnce,
		// Token: 0x04000C9B RID: 3227
		IndividualFaces,
		// Token: 0x04000C9C RID: 3228
		NoTimeSlicing
	}
}
