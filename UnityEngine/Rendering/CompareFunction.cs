﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000309 RID: 777
	public enum CompareFunction
	{
		// Token: 0x04000BFE RID: 3070
		Disabled,
		// Token: 0x04000BFF RID: 3071
		Never,
		// Token: 0x04000C00 RID: 3072
		Less,
		// Token: 0x04000C01 RID: 3073
		Equal,
		// Token: 0x04000C02 RID: 3074
		LessEqual,
		// Token: 0x04000C03 RID: 3075
		Greater,
		// Token: 0x04000C04 RID: 3076
		NotEqual,
		// Token: 0x04000C05 RID: 3077
		GreaterEqual,
		// Token: 0x04000C06 RID: 3078
		Always
	}
}
