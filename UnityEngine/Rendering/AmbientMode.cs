﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200030D RID: 781
	public enum AmbientMode
	{
		// Token: 0x04000C1B RID: 3099
		Skybox,
		// Token: 0x04000C1C RID: 3100
		Trilight,
		// Token: 0x04000C1D RID: 3101
		Flat = 3,
		// Token: 0x04000C1E RID: 3102
		Custom
	}
}
