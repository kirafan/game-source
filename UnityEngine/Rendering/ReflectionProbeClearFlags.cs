﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200031B RID: 795
	public enum ReflectionProbeClearFlags
	{
		// Token: 0x04000C8D RID: 3213
		Skybox = 1,
		// Token: 0x04000C8E RID: 3214
		SolidColor
	}
}
