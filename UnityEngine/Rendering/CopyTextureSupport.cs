﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000325 RID: 805
	[Flags]
	public enum CopyTextureSupport
	{
		// Token: 0x04000CBB RID: 3259
		None = 0,
		// Token: 0x04000CBC RID: 3260
		Basic = 1,
		// Token: 0x04000CBD RID: 3261
		Copy3D = 2,
		// Token: 0x04000CBE RID: 3262
		DifferentTypes = 4,
		// Token: 0x04000CBF RID: 3263
		TextureToRT = 8,
		// Token: 0x04000CC0 RID: 3264
		RTToTexture = 16
	}
}
