﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000320 RID: 800
	public enum ShadowSamplingMode
	{
		// Token: 0x04000C9E RID: 3230
		CompareDepths,
		// Token: 0x04000C9F RID: 3231
		RawDepth
	}
}
