﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine.Rendering
{
	// Token: 0x0200031D RID: 797
	[UsedByNativeCode]
	public struct ReflectionProbeBlendInfo
	{
		// Token: 0x04000C93 RID: 3219
		public ReflectionProbe probe;

		// Token: 0x04000C94 RID: 3220
		public float weight;
	}
}
