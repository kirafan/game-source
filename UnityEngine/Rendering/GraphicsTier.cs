﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000317 RID: 791
	public enum GraphicsTier
	{
		// Token: 0x04000C7E RID: 3198
		Tier1,
		// Token: 0x04000C7F RID: 3199
		Tier2,
		// Token: 0x04000C80 RID: 3200
		Tier3
	}
}
