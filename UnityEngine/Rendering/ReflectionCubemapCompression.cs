﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200030F RID: 783
	public enum ReflectionCubemapCompression
	{
		// Token: 0x04000C23 RID: 3107
		Uncompressed,
		// Token: 0x04000C24 RID: 3108
		Compressed,
		// Token: 0x04000C25 RID: 3109
		Auto
	}
}
