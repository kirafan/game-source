﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000303 RID: 771
	public enum OpaqueSortMode
	{
		// Token: 0x04000BBD RID: 3005
		Default,
		// Token: 0x04000BBE RID: 3006
		FrontToBack,
		// Token: 0x04000BBF RID: 3007
		NoDistanceSort
	}
}
