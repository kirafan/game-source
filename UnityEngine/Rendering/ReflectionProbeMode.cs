﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200031C RID: 796
	public enum ReflectionProbeMode
	{
		// Token: 0x04000C90 RID: 3216
		Baked,
		// Token: 0x04000C91 RID: 3217
		Realtime,
		// Token: 0x04000C92 RID: 3218
		Custom
	}
}
