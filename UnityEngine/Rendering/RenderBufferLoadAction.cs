﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x02000305 RID: 773
	public enum RenderBufferLoadAction
	{
		// Token: 0x04000BC7 RID: 3015
		Load,
		// Token: 0x04000BC8 RID: 3016
		DontCare = 2
	}
}
