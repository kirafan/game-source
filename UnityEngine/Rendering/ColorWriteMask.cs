﻿using System;

namespace UnityEngine.Rendering
{
	// Token: 0x0200030B RID: 779
	[Flags]
	public enum ColorWriteMask
	{
		// Token: 0x04000C0C RID: 3084
		Alpha = 1,
		// Token: 0x04000C0D RID: 3085
		Blue = 2,
		// Token: 0x04000C0E RID: 3086
		Green = 4,
		// Token: 0x04000C0F RID: 3087
		Red = 8,
		// Token: 0x04000C10 RID: 3088
		All = 15
	}
}
