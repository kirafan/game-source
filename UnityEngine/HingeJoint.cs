﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000146 RID: 326
	public sealed class HingeJoint : Joint
	{
		// Token: 0x17000560 RID: 1376
		// (get) Token: 0x060017A6 RID: 6054 RVA: 0x0001E31C File Offset: 0x0001C51C
		// (set) Token: 0x060017A7 RID: 6055 RVA: 0x0001E33C File Offset: 0x0001C53C
		public JointMotor motor
		{
			get
			{
				JointMotor result;
				this.INTERNAL_get_motor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_motor(ref value);
			}
		}

		// Token: 0x060017A8 RID: 6056
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_motor(out JointMotor value);

		// Token: 0x060017A9 RID: 6057
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_motor(ref JointMotor value);

		// Token: 0x17000561 RID: 1377
		// (get) Token: 0x060017AA RID: 6058 RVA: 0x0001E348 File Offset: 0x0001C548
		// (set) Token: 0x060017AB RID: 6059 RVA: 0x0001E368 File Offset: 0x0001C568
		public JointLimits limits
		{
			get
			{
				JointLimits result;
				this.INTERNAL_get_limits(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_limits(ref value);
			}
		}

		// Token: 0x060017AC RID: 6060
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_limits(out JointLimits value);

		// Token: 0x060017AD RID: 6061
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_limits(ref JointLimits value);

		// Token: 0x17000562 RID: 1378
		// (get) Token: 0x060017AE RID: 6062 RVA: 0x0001E374 File Offset: 0x0001C574
		// (set) Token: 0x060017AF RID: 6063 RVA: 0x0001E394 File Offset: 0x0001C594
		public JointSpring spring
		{
			get
			{
				JointSpring result;
				this.INTERNAL_get_spring(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_spring(ref value);
			}
		}

		// Token: 0x060017B0 RID: 6064
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_spring(out JointSpring value);

		// Token: 0x060017B1 RID: 6065
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_spring(ref JointSpring value);

		// Token: 0x17000563 RID: 1379
		// (get) Token: 0x060017B2 RID: 6066
		// (set) Token: 0x060017B3 RID: 6067
		public extern bool useMotor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000564 RID: 1380
		// (get) Token: 0x060017B4 RID: 6068
		// (set) Token: 0x060017B5 RID: 6069
		public extern bool useLimits { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000565 RID: 1381
		// (get) Token: 0x060017B6 RID: 6070
		// (set) Token: 0x060017B7 RID: 6071
		public extern bool useSpring { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000566 RID: 1382
		// (get) Token: 0x060017B8 RID: 6072
		public extern float velocity { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000567 RID: 1383
		// (get) Token: 0x060017B9 RID: 6073
		public extern float angle { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
