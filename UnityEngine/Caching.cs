﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000022 RID: 34
	public sealed class Caching
	{
		// Token: 0x06000223 RID: 547 RVA: 0x000050D8 File Offset: 0x000032D8
		public static bool Authorize(string name, string domain, long size, string signature)
		{
			return Caching.Authorize(name, domain, size, -1, signature);
		}

		// Token: 0x06000224 RID: 548
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool Authorize(string name, string domain, long size, int expiration, string signature);

		// Token: 0x06000225 RID: 549 RVA: 0x000050F8 File Offset: 0x000032F8
		[Obsolete("Size is now specified as a long")]
		public static bool Authorize(string name, string domain, int size, int expiration, string signature)
		{
			return Caching.Authorize(name, domain, (long)size, expiration, signature);
		}

		// Token: 0x06000226 RID: 550 RVA: 0x0000511C File Offset: 0x0000331C
		[Obsolete("Size is now specified as a long")]
		public static bool Authorize(string name, string domain, int size, string signature)
		{
			return Caching.Authorize(name, domain, (long)size, signature);
		}

		// Token: 0x06000227 RID: 551
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool CleanCache();

		// Token: 0x06000228 RID: 552
		[Obsolete("this API is not for public use.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool CleanNamedCache(string name);

		// Token: 0x06000229 RID: 553
		[Obsolete("This function is obsolete and has no effect.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool DeleteFromCache(string url);

		// Token: 0x0600022A RID: 554
		[Obsolete("This function is obsolete and will always return -1. Use IsVersionCached instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetVersionFromCache(string url);

		// Token: 0x0600022B RID: 555 RVA: 0x0000513C File Offset: 0x0000333C
		public static bool IsVersionCached(string url, int version)
		{
			Hash128 hash = new Hash128(0U, 0U, 0U, (uint)version);
			return Caching.IsVersionCached(url, hash);
		}

		// Token: 0x0600022C RID: 556 RVA: 0x00005164 File Offset: 0x00003364
		public static bool IsVersionCached(string url, Hash128 hash)
		{
			return Caching.INTERNAL_CALL_IsVersionCached(url, ref hash);
		}

		// Token: 0x0600022D RID: 557
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_IsVersionCached(string url, ref Hash128 hash);

		// Token: 0x0600022E RID: 558 RVA: 0x00005184 File Offset: 0x00003384
		public static bool MarkAsUsed(string url, int version)
		{
			Hash128 hash = new Hash128(0U, 0U, 0U, (uint)version);
			return Caching.MarkAsUsed(url, hash);
		}

		// Token: 0x0600022F RID: 559 RVA: 0x000051AC File Offset: 0x000033AC
		public static bool MarkAsUsed(string url, Hash128 hash)
		{
			return Caching.INTERNAL_CALL_MarkAsUsed(url, ref hash);
		}

		// Token: 0x06000230 RID: 560
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_MarkAsUsed(string url, ref Hash128 hash);

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x06000231 RID: 561
		[Obsolete("this API is not for public use.")]
		public static extern CacheIndex[] index { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000232 RID: 562
		public static extern long spaceFree { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000233 RID: 563
		// (set) Token: 0x06000234 RID: 564
		public static extern long maximumAvailableDiskSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000235 RID: 565
		public static extern long spaceOccupied { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000236 RID: 566
		[Obsolete("Please use Caching.spaceFree instead")]
		public static extern int spaceAvailable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000237 RID: 567
		[Obsolete("Please use Caching.spaceOccupied instead")]
		public static extern int spaceUsed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000238 RID: 568
		// (set) Token: 0x06000239 RID: 569
		public static extern int expirationDelay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600023A RID: 570
		// (set) Token: 0x0600023B RID: 571
		public static extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600023C RID: 572
		// (set) Token: 0x0600023D RID: 573
		public static extern bool compressionEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x0600023E RID: 574
		public static extern bool ready { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
