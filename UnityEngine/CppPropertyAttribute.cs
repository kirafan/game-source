﻿using System;

namespace UnityEngine
{
	// Token: 0x020002D7 RID: 727
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	internal class CppPropertyAttribute : Attribute
	{
		// Token: 0x06002C88 RID: 11400 RVA: 0x000463A8 File Offset: 0x000445A8
		public CppPropertyAttribute(string getter, string setter)
		{
		}

		// Token: 0x06002C89 RID: 11401 RVA: 0x000463B4 File Offset: 0x000445B4
		public CppPropertyAttribute(string getter)
		{
		}
	}
}
