﻿using System;

namespace UnityEngine
{
	// Token: 0x020002ED RID: 749
	[Obsolete("See QualitySettings.names, QualitySettings.SetQualityLevel, and QualitySettings.GetQualityLevel")]
	public enum QualityLevel
	{
		// Token: 0x04000B0E RID: 2830
		Fastest,
		// Token: 0x04000B0F RID: 2831
		Fast,
		// Token: 0x04000B10 RID: 2832
		Simple,
		// Token: 0x04000B11 RID: 2833
		Good,
		// Token: 0x04000B12 RID: 2834
		Beautiful,
		// Token: 0x04000B13 RID: 2835
		Fantastic
	}
}
