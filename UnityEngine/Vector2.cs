﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000393 RID: 915
	[UsedByNativeCode]
	public struct Vector2
	{
		// Token: 0x06002F18 RID: 12056 RVA: 0x0004C9E0 File Offset: 0x0004ABE0
		public Vector2(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		// Token: 0x17000B06 RID: 2822
		public float this[int index]
		{
			get
			{
				float result;
				if (index != 0)
				{
					if (index != 1)
					{
						throw new IndexOutOfRangeException("Invalid Vector2 index!");
					}
					result = this.y;
				}
				else
				{
					result = this.x;
				}
				return result;
			}
			set
			{
				if (index != 0)
				{
					if (index != 1)
					{
						throw new IndexOutOfRangeException("Invalid Vector2 index!");
					}
					this.y = value;
				}
				else
				{
					this.x = value;
				}
			}
		}

		// Token: 0x06002F1B RID: 12059 RVA: 0x0004CA70 File Offset: 0x0004AC70
		public void Set(float newX, float newY)
		{
			this.x = newX;
			this.y = newY;
		}

		// Token: 0x06002F1C RID: 12060 RVA: 0x0004CA84 File Offset: 0x0004AC84
		public static Vector2 Lerp(Vector2 a, Vector2 b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Vector2(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t);
		}

		// Token: 0x06002F1D RID: 12061 RVA: 0x0004CAD8 File Offset: 0x0004ACD8
		public static Vector2 LerpUnclamped(Vector2 a, Vector2 b, float t)
		{
			return new Vector2(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t);
		}

		// Token: 0x06002F1E RID: 12062 RVA: 0x0004CB24 File Offset: 0x0004AD24
		public static Vector2 MoveTowards(Vector2 current, Vector2 target, float maxDistanceDelta)
		{
			Vector2 a = target - current;
			float magnitude = a.magnitude;
			Vector2 result;
			if (magnitude <= maxDistanceDelta || magnitude == 0f)
			{
				result = target;
			}
			else
			{
				result = current + a / magnitude * maxDistanceDelta;
			}
			return result;
		}

		// Token: 0x06002F1F RID: 12063 RVA: 0x0004CB78 File Offset: 0x0004AD78
		public static Vector2 Scale(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x * b.x, a.y * b.y);
		}

		// Token: 0x06002F20 RID: 12064 RVA: 0x0004CBB0 File Offset: 0x0004ADB0
		public void Scale(Vector2 scale)
		{
			this.x *= scale.x;
			this.y *= scale.y;
		}

		// Token: 0x06002F21 RID: 12065 RVA: 0x0004CBDC File Offset: 0x0004ADDC
		public void Normalize()
		{
			float magnitude = this.magnitude;
			if (magnitude > 1E-05f)
			{
				this /= magnitude;
			}
			else
			{
				this = Vector2.zero;
			}
		}

		// Token: 0x17000B07 RID: 2823
		// (get) Token: 0x06002F22 RID: 12066 RVA: 0x0004CC20 File Offset: 0x0004AE20
		public Vector2 normalized
		{
			get
			{
				Vector2 result = new Vector2(this.x, this.y);
				result.Normalize();
				return result;
			}
		}

		// Token: 0x06002F23 RID: 12067 RVA: 0x0004CC50 File Offset: 0x0004AE50
		public override string ToString()
		{
			return UnityString.Format("({0:F1}, {1:F1})", new object[]
			{
				this.x,
				this.y
			});
		}

		// Token: 0x06002F24 RID: 12068 RVA: 0x0004CC94 File Offset: 0x0004AE94
		public string ToString(string format)
		{
			return UnityString.Format("({0}, {1})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format)
			});
		}

		// Token: 0x06002F25 RID: 12069 RVA: 0x0004CCD8 File Offset: 0x0004AED8
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode() << 2;
		}

		// Token: 0x06002F26 RID: 12070 RVA: 0x0004CD14 File Offset: 0x0004AF14
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Vector2))
			{
				result = false;
			}
			else
			{
				Vector2 vector = (Vector2)other;
				result = (this.x.Equals(vector.x) && this.y.Equals(vector.y));
			}
			return result;
		}

		// Token: 0x06002F27 RID: 12071 RVA: 0x0004CD70 File Offset: 0x0004AF70
		public static Vector2 Reflect(Vector2 inDirection, Vector2 inNormal)
		{
			return -2f * Vector2.Dot(inNormal, inDirection) * inNormal + inDirection;
		}

		// Token: 0x06002F28 RID: 12072 RVA: 0x0004CDA0 File Offset: 0x0004AFA0
		public static float Dot(Vector2 lhs, Vector2 rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y;
		}

		// Token: 0x17000B08 RID: 2824
		// (get) Token: 0x06002F29 RID: 12073 RVA: 0x0004CDD4 File Offset: 0x0004AFD4
		public float magnitude
		{
			get
			{
				return Mathf.Sqrt(this.x * this.x + this.y * this.y);
			}
		}

		// Token: 0x17000B09 RID: 2825
		// (get) Token: 0x06002F2A RID: 12074 RVA: 0x0004CE0C File Offset: 0x0004B00C
		public float sqrMagnitude
		{
			get
			{
				return this.x * this.x + this.y * this.y;
			}
		}

		// Token: 0x06002F2B RID: 12075 RVA: 0x0004CE3C File Offset: 0x0004B03C
		public static float Angle(Vector2 from, Vector2 to)
		{
			return Mathf.Acos(Mathf.Clamp(Vector2.Dot(from.normalized, to.normalized), -1f, 1f)) * 57.29578f;
		}

		// Token: 0x06002F2C RID: 12076 RVA: 0x0004CE80 File Offset: 0x0004B080
		public static float Distance(Vector2 a, Vector2 b)
		{
			return (a - b).magnitude;
		}

		// Token: 0x06002F2D RID: 12077 RVA: 0x0004CEA4 File Offset: 0x0004B0A4
		public static Vector2 ClampMagnitude(Vector2 vector, float maxLength)
		{
			Vector2 result;
			if (vector.sqrMagnitude > maxLength * maxLength)
			{
				result = vector.normalized * maxLength;
			}
			else
			{
				result = vector;
			}
			return result;
		}

		// Token: 0x06002F2E RID: 12078 RVA: 0x0004CEDC File Offset: 0x0004B0DC
		public static float SqrMagnitude(Vector2 a)
		{
			return a.x * a.x + a.y * a.y;
		}

		// Token: 0x06002F2F RID: 12079 RVA: 0x0004CF10 File Offset: 0x0004B110
		public float SqrMagnitude()
		{
			return this.x * this.x + this.y * this.y;
		}

		// Token: 0x06002F30 RID: 12080 RVA: 0x0004CF40 File Offset: 0x0004B140
		public static Vector2 Min(Vector2 lhs, Vector2 rhs)
		{
			return new Vector2(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y));
		}

		// Token: 0x06002F31 RID: 12081 RVA: 0x0004CF80 File Offset: 0x0004B180
		public static Vector2 Max(Vector2 lhs, Vector2 rhs)
		{
			return new Vector2(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y));
		}

		// Token: 0x06002F32 RID: 12082 RVA: 0x0004CFC0 File Offset: 0x0004B1C0
		public static Vector2 SmoothDamp(Vector2 current, Vector2 target, ref Vector2 currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
		{
			smoothTime = Mathf.Max(0.0001f, smoothTime);
			float num = 2f / smoothTime;
			float num2 = num * deltaTime;
			float d = 1f / (1f + num2 + 0.48f * num2 * num2 + 0.235f * num2 * num2 * num2);
			Vector2 vector = current - target;
			Vector2 vector2 = target;
			float maxLength = maxSpeed * smoothTime;
			vector = Vector2.ClampMagnitude(vector, maxLength);
			target = current - vector;
			Vector2 vector3 = (currentVelocity + num * vector) * deltaTime;
			currentVelocity = (currentVelocity - num * vector3) * d;
			Vector2 vector4 = target + (vector + vector3) * d;
			if (Vector2.Dot(vector2 - current, vector4 - vector2) > 0f)
			{
				vector4 = vector2;
				currentVelocity = (vector4 - vector2) / deltaTime;
			}
			return vector4;
		}

		// Token: 0x06002F33 RID: 12083 RVA: 0x0004D0C8 File Offset: 0x0004B2C8
		public static Vector2 operator +(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x + b.x, a.y + b.y);
		}

		// Token: 0x06002F34 RID: 12084 RVA: 0x0004D100 File Offset: 0x0004B300
		public static Vector2 operator -(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x - b.x, a.y - b.y);
		}

		// Token: 0x06002F35 RID: 12085 RVA: 0x0004D138 File Offset: 0x0004B338
		public static Vector2 operator -(Vector2 a)
		{
			return new Vector2(-a.x, -a.y);
		}

		// Token: 0x06002F36 RID: 12086 RVA: 0x0004D164 File Offset: 0x0004B364
		public static Vector2 operator *(Vector2 a, float d)
		{
			return new Vector2(a.x * d, a.y * d);
		}

		// Token: 0x06002F37 RID: 12087 RVA: 0x0004D190 File Offset: 0x0004B390
		public static Vector2 operator *(float d, Vector2 a)
		{
			return new Vector2(a.x * d, a.y * d);
		}

		// Token: 0x06002F38 RID: 12088 RVA: 0x0004D1BC File Offset: 0x0004B3BC
		public static Vector2 operator /(Vector2 a, float d)
		{
			return new Vector2(a.x / d, a.y / d);
		}

		// Token: 0x06002F39 RID: 12089 RVA: 0x0004D1E8 File Offset: 0x0004B3E8
		public static bool operator ==(Vector2 lhs, Vector2 rhs)
		{
			return (lhs - rhs).sqrMagnitude < 9.9999994E-11f;
		}

		// Token: 0x06002F3A RID: 12090 RVA: 0x0004D214 File Offset: 0x0004B414
		public static bool operator !=(Vector2 lhs, Vector2 rhs)
		{
			return (lhs - rhs).sqrMagnitude >= 9.9999994E-11f;
		}

		// Token: 0x06002F3B RID: 12091 RVA: 0x0004D244 File Offset: 0x0004B444
		public static implicit operator Vector2(Vector3 v)
		{
			return new Vector2(v.x, v.y);
		}

		// Token: 0x06002F3C RID: 12092 RVA: 0x0004D26C File Offset: 0x0004B46C
		public static implicit operator Vector3(Vector2 v)
		{
			return new Vector3(v.x, v.y, 0f);
		}

		// Token: 0x17000B0A RID: 2826
		// (get) Token: 0x06002F3D RID: 12093 RVA: 0x0004D29C File Offset: 0x0004B49C
		public static Vector2 zero
		{
			get
			{
				return new Vector2(0f, 0f);
			}
		}

		// Token: 0x17000B0B RID: 2827
		// (get) Token: 0x06002F3E RID: 12094 RVA: 0x0004D2C0 File Offset: 0x0004B4C0
		public static Vector2 one
		{
			get
			{
				return new Vector2(1f, 1f);
			}
		}

		// Token: 0x17000B0C RID: 2828
		// (get) Token: 0x06002F3F RID: 12095 RVA: 0x0004D2E4 File Offset: 0x0004B4E4
		public static Vector2 up
		{
			get
			{
				return new Vector2(0f, 1f);
			}
		}

		// Token: 0x17000B0D RID: 2829
		// (get) Token: 0x06002F40 RID: 12096 RVA: 0x0004D308 File Offset: 0x0004B508
		public static Vector2 down
		{
			get
			{
				return new Vector2(0f, -1f);
			}
		}

		// Token: 0x17000B0E RID: 2830
		// (get) Token: 0x06002F41 RID: 12097 RVA: 0x0004D32C File Offset: 0x0004B52C
		public static Vector2 left
		{
			get
			{
				return new Vector2(-1f, 0f);
			}
		}

		// Token: 0x17000B0F RID: 2831
		// (get) Token: 0x06002F42 RID: 12098 RVA: 0x0004D350 File Offset: 0x0004B550
		public static Vector2 right
		{
			get
			{
				return new Vector2(1f, 0f);
			}
		}

		// Token: 0x04000D8B RID: 3467
		public float x;

		// Token: 0x04000D8C RID: 3468
		public float y;

		// Token: 0x04000D8D RID: 3469
		public const float kEpsilon = 1E-05f;
	}
}
