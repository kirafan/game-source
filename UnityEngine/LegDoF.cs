﻿using System;

namespace UnityEngine
{
	// Token: 0x020001DC RID: 476
	internal enum LegDoF
	{
		// Token: 0x0400053A RID: 1338
		UpperLegFrontBack,
		// Token: 0x0400053B RID: 1339
		UpperLegInOut,
		// Token: 0x0400053C RID: 1340
		UpperLegRollInOut,
		// Token: 0x0400053D RID: 1341
		LegCloseOpen,
		// Token: 0x0400053E RID: 1342
		LegRollInOut,
		// Token: 0x0400053F RID: 1343
		FootCloseOpen,
		// Token: 0x04000540 RID: 1344
		FootInOut,
		// Token: 0x04000541 RID: 1345
		ToesUpDown,
		// Token: 0x04000542 RID: 1346
		LastLegDoF
	}
}
