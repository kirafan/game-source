﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000034 RID: 52
	internal sealed class DebugLogHandler : ILogHandler
	{
		// Token: 0x06000399 RID: 921
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_Log(LogType level, string msg, [Writable] Object obj);

		// Token: 0x0600039A RID: 922
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_LogException(Exception exception, [Writable] Object obj);

		// Token: 0x0600039B RID: 923 RVA: 0x000064FC File Offset: 0x000046FC
		public void LogFormat(LogType logType, Object context, string format, params object[] args)
		{
			DebugLogHandler.Internal_Log(logType, string.Format(format, args), context);
		}

		// Token: 0x0600039C RID: 924 RVA: 0x00006510 File Offset: 0x00004710
		public void LogException(Exception exception, Object context)
		{
			DebugLogHandler.Internal_LogException(exception, context);
		}
	}
}
