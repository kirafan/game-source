﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020001B8 RID: 440
	public sealed class AnimatorOverrideController : RuntimeAnimatorController
	{
		// Token: 0x06001E1D RID: 7709 RVA: 0x000229F4 File Offset: 0x00020BF4
		public AnimatorOverrideController()
		{
			AnimatorOverrideController.Internal_CreateAnimationSet(this);
		}

		// Token: 0x06001E1E RID: 7710
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateAnimationSet([Writable] AnimatorOverrideController self);

		// Token: 0x1700077C RID: 1916
		// (get) Token: 0x06001E1F RID: 7711
		// (set) Token: 0x06001E20 RID: 7712
		public extern RuntimeAnimatorController runtimeAnimatorController { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700077D RID: 1917
		public AnimationClip this[string name]
		{
			get
			{
				return this.Internal_GetClipByName(name, true);
			}
			set
			{
				this.Internal_SetClipByName(name, value);
			}
		}

		// Token: 0x06001E23 RID: 7715
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationClip Internal_GetClipByName(string name, bool returnEffectiveClip);

		// Token: 0x06001E24 RID: 7716
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetClipByName(string name, AnimationClip clip);

		// Token: 0x1700077E RID: 1918
		public AnimationClip this[AnimationClip clip]
		{
			get
			{
				return this.Internal_GetClip(clip, true);
			}
			set
			{
				this.Internal_SetClip(clip, value);
			}
		}

		// Token: 0x06001E27 RID: 7719
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationClip Internal_GetClip(AnimationClip originalClip, bool returnEffectiveClip);

		// Token: 0x06001E28 RID: 7720
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetClip(AnimationClip originalClip, AnimationClip overrideClip, [DefaultValue("true")] bool notify);

		// Token: 0x06001E29 RID: 7721 RVA: 0x00022A5C File Offset: 0x00020C5C
		[ExcludeFromDocs]
		private void Internal_SetClip(AnimationClip originalClip, AnimationClip overrideClip)
		{
			bool notify = true;
			this.Internal_SetClip(originalClip, overrideClip, notify);
		}

		// Token: 0x06001E2A RID: 7722
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_SetDirty();

		// Token: 0x1700077F RID: 1919
		// (get) Token: 0x06001E2B RID: 7723 RVA: 0x00022A78 File Offset: 0x00020C78
		// (set) Token: 0x06001E2C RID: 7724 RVA: 0x00022B34 File Offset: 0x00020D34
		public AnimationClipPair[] clips
		{
			get
			{
				AnimationClip[] array = this.GetOriginalClips();
				Dictionary<AnimationClip, bool> dictionary = new Dictionary<AnimationClip, bool>(array.Length);
				foreach (AnimationClip key in array)
				{
					dictionary[key] = true;
				}
				array = new AnimationClip[dictionary.Count];
				dictionary.Keys.CopyTo(array, 0);
				AnimationClipPair[] array3 = new AnimationClipPair[array.Length];
				for (int j = 0; j < array.Length; j++)
				{
					array3[j] = new AnimationClipPair();
					array3[j].originalClip = array[j];
					array3[j].overrideClip = this.Internal_GetClip(array[j], false);
				}
				return array3;
			}
			set
			{
				for (int i = 0; i < value.Length; i++)
				{
					this.Internal_SetClip(value[i].originalClip, value[i].overrideClip, false);
				}
				this.Internal_SetDirty();
			}
		}

		// Token: 0x06001E2D RID: 7725
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationClip[] GetOriginalClips();

		// Token: 0x06001E2E RID: 7726
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AnimationClip[] GetOverrideClips();
	}
}
