﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Profiling
{
	// Token: 0x020000F5 RID: 245
	[UsedByNativeCode]
	internal sealed class Sampler
	{
		// Token: 0x060010CF RID: 4303 RVA: 0x00017258 File Offset: 0x00015458
		internal Sampler()
		{
		}

		// Token: 0x1700038C RID: 908
		// (get) Token: 0x060010D0 RID: 4304
		public extern string name { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700038D RID: 909
		// (get) Token: 0x060010D1 RID: 4305
		// (set) Token: 0x060010D2 RID: 4306
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0400027F RID: 639
		internal IntPtr m_Ptr;
	}
}
