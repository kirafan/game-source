﻿using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting.APIUpdating;

namespace UnityEngine.Profiling
{
	// Token: 0x020000F4 RID: 244
	[MovedFrom("UnityEngine")]
	public sealed class Profiler
	{
		// Token: 0x17000386 RID: 902
		// (get) Token: 0x060010B5 RID: 4277
		public static extern bool supported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000387 RID: 903
		// (get) Token: 0x060010B6 RID: 4278
		// (set) Token: 0x060010B7 RID: 4279
		public static extern string logFile { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000388 RID: 904
		// (get) Token: 0x060010B8 RID: 4280
		// (set) Token: 0x060010B9 RID: 4281
		public static extern bool enableBinaryLog { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000389 RID: 905
		// (get) Token: 0x060010BA RID: 4282
		// (set) Token: 0x060010BB RID: 4283
		public static extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x060010BC RID: 4284
		[Conditional("UNITY_EDITOR")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void AddFramesFromFile(string file);

		// Token: 0x060010BD RID: 4285 RVA: 0x0001724C File Offset: 0x0001544C
		[Conditional("ENABLE_PROFILER")]
		public static void BeginSample(string name)
		{
			Profiler.BeginSampleOnly(name);
		}

		// Token: 0x060010BE RID: 4286
		[Conditional("ENABLE_PROFILER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void BeginSample(string name, Object targetObject);

		// Token: 0x060010BF RID: 4287
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void BeginSampleOnly(string name);

		// Token: 0x060010C0 RID: 4288
		[Conditional("ENABLE_PROFILER")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void EndSample();

		// Token: 0x1700038A RID: 906
		// (get) Token: 0x060010C1 RID: 4289
		// (set) Token: 0x060010C2 RID: 4290
		[Obsolete("maxNumberOfSamplesPerFrame is no longer needed, as the profiler buffers auto expand as needed")]
		public static extern int maxNumberOfSamplesPerFrame { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700038B RID: 907
		// (get) Token: 0x060010C3 RID: 4291
		public static extern uint usedHeapSize { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060010C4 RID: 4292
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetRuntimeMemorySize(Object o);

		// Token: 0x060010C5 RID: 4293
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern uint GetMonoHeapSize();

		// Token: 0x060010C6 RID: 4294
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern uint GetMonoUsedSize();

		// Token: 0x060010C7 RID: 4295
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SetTempAllocatorRequestedSize(uint size);

		// Token: 0x060010C8 RID: 4296
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern uint GetTempAllocatorSize();

		// Token: 0x060010C9 RID: 4297
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern uint GetTotalAllocatedMemory();

		// Token: 0x060010CA RID: 4298
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern uint GetTotalUnusedReservedMemory();

		// Token: 0x060010CB RID: 4299
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern uint GetTotalReservedMemory();

		// Token: 0x060010CC RID: 4300
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Sampler GetSampler(string name);

		// Token: 0x060010CD RID: 4301
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string[] GetSamplerNames();

		// Token: 0x060010CE RID: 4302
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetSamplersEnabled(bool enabled);
	}
}
