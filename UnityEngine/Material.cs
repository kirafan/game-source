﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000B5 RID: 181
	public class Material : Object
	{
		// Token: 0x06000CA2 RID: 3234 RVA: 0x0001156C File Offset: 0x0000F76C
		[Obsolete("Creating materials from shader source string is no longer supported. Use Shader assets instead.")]
		public Material(string contents)
		{
			Material.Internal_CreateWithString(this, contents);
		}

		// Token: 0x06000CA3 RID: 3235 RVA: 0x0001157C File Offset: 0x0000F77C
		public Material(Shader shader)
		{
			Material.Internal_CreateWithShader(this, shader);
		}

		// Token: 0x06000CA4 RID: 3236 RVA: 0x0001158C File Offset: 0x0000F78C
		public Material(Material source)
		{
			Material.Internal_CreateWithMaterial(this, source);
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06000CA5 RID: 3237
		// (set) Token: 0x06000CA6 RID: 3238
		public extern Shader shader { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06000CA7 RID: 3239 RVA: 0x0001159C File Offset: 0x0000F79C
		// (set) Token: 0x06000CA8 RID: 3240 RVA: 0x000115BC File Offset: 0x0000F7BC
		public Color color
		{
			get
			{
				return this.GetColor("_Color");
			}
			set
			{
				this.SetColor("_Color", value);
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06000CA9 RID: 3241 RVA: 0x000115CC File Offset: 0x0000F7CC
		// (set) Token: 0x06000CAA RID: 3242 RVA: 0x000115EC File Offset: 0x0000F7EC
		public Texture mainTexture
		{
			get
			{
				return this.GetTexture("_MainTex");
			}
			set
			{
				this.SetTexture("_MainTex", value);
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06000CAB RID: 3243 RVA: 0x000115FC File Offset: 0x0000F7FC
		// (set) Token: 0x06000CAC RID: 3244 RVA: 0x0001161C File Offset: 0x0000F81C
		public Vector2 mainTextureOffset
		{
			get
			{
				return this.GetTextureOffset("_MainTex");
			}
			set
			{
				this.SetTextureOffset("_MainTex", value);
			}
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x06000CAD RID: 3245 RVA: 0x0001162C File Offset: 0x0000F82C
		// (set) Token: 0x06000CAE RID: 3246 RVA: 0x0001164C File Offset: 0x0000F84C
		public Vector2 mainTextureScale
		{
			get
			{
				return this.GetTextureScale("_MainTex");
			}
			set
			{
				this.SetTextureScale("_MainTex", value);
			}
		}

		// Token: 0x06000CAF RID: 3247 RVA: 0x0001165C File Offset: 0x0000F85C
		public void SetColor(string propertyName, Color color)
		{
			this.SetColor(Shader.PropertyToID(propertyName), color);
		}

		// Token: 0x06000CB0 RID: 3248 RVA: 0x0001166C File Offset: 0x0000F86C
		public void SetColor(int nameID, Color color)
		{
			Material.INTERNAL_CALL_SetColor(this, nameID, ref color);
		}

		// Token: 0x06000CB1 RID: 3249
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetColor(Material self, int nameID, ref Color color);

		// Token: 0x06000CB2 RID: 3250 RVA: 0x00011678 File Offset: 0x0000F878
		public Color GetColor(string propertyName)
		{
			return this.GetColor(Shader.PropertyToID(propertyName));
		}

		// Token: 0x06000CB3 RID: 3251 RVA: 0x0001169C File Offset: 0x0000F89C
		public Color GetColor(int nameID)
		{
			Color result;
			Material.INTERNAL_CALL_GetColor(this, nameID, out result);
			return result;
		}

		// Token: 0x06000CB4 RID: 3252
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetColor(Material self, int nameID, out Color value);

		// Token: 0x06000CB5 RID: 3253 RVA: 0x000116BC File Offset: 0x0000F8BC
		public void SetVector(string propertyName, Vector4 vector)
		{
			this.SetColor(propertyName, new Color(vector.x, vector.y, vector.z, vector.w));
		}

		// Token: 0x06000CB6 RID: 3254 RVA: 0x000116E8 File Offset: 0x0000F8E8
		public void SetVector(int nameID, Vector4 vector)
		{
			this.SetColor(nameID, new Color(vector.x, vector.y, vector.z, vector.w));
		}

		// Token: 0x06000CB7 RID: 3255 RVA: 0x00011714 File Offset: 0x0000F914
		public Vector4 GetVector(string propertyName)
		{
			Color color = this.GetColor(propertyName);
			return new Vector4(color.r, color.g, color.b, color.a);
		}

		// Token: 0x06000CB8 RID: 3256 RVA: 0x00011754 File Offset: 0x0000F954
		public Vector4 GetVector(int nameID)
		{
			Color color = this.GetColor(nameID);
			return new Vector4(color.r, color.g, color.b, color.a);
		}

		// Token: 0x06000CB9 RID: 3257 RVA: 0x00011794 File Offset: 0x0000F994
		public void SetTexture(string propertyName, Texture texture)
		{
			this.SetTexture(Shader.PropertyToID(propertyName), texture);
		}

		// Token: 0x06000CBA RID: 3258
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTexture(int nameID, Texture texture);

		// Token: 0x06000CBB RID: 3259 RVA: 0x000117A4 File Offset: 0x0000F9A4
		public Texture GetTexture(string propertyName)
		{
			return this.GetTexture(Shader.PropertyToID(propertyName));
		}

		// Token: 0x06000CBC RID: 3260
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Texture GetTexture(int nameID);

		// Token: 0x06000CBD RID: 3261
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_GetTextureScaleAndOffset(Material mat, string name, out Vector4 output);

		// Token: 0x06000CBE RID: 3262 RVA: 0x000117C8 File Offset: 0x0000F9C8
		public void SetTextureOffset(string propertyName, Vector2 offset)
		{
			Material.INTERNAL_CALL_SetTextureOffset(this, propertyName, ref offset);
		}

		// Token: 0x06000CBF RID: 3263
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetTextureOffset(Material self, string propertyName, ref Vector2 offset);

		// Token: 0x06000CC0 RID: 3264 RVA: 0x000117D4 File Offset: 0x0000F9D4
		public Vector2 GetTextureOffset(string propertyName)
		{
			Vector4 vector;
			Material.Internal_GetTextureScaleAndOffset(this, propertyName, out vector);
			return new Vector2(vector.z, vector.w);
		}

		// Token: 0x06000CC1 RID: 3265 RVA: 0x00011808 File Offset: 0x0000FA08
		public void SetTextureScale(string propertyName, Vector2 scale)
		{
			Material.INTERNAL_CALL_SetTextureScale(this, propertyName, ref scale);
		}

		// Token: 0x06000CC2 RID: 3266
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetTextureScale(Material self, string propertyName, ref Vector2 scale);

		// Token: 0x06000CC3 RID: 3267 RVA: 0x00011814 File Offset: 0x0000FA14
		public Vector2 GetTextureScale(string propertyName)
		{
			Vector4 vector;
			Material.Internal_GetTextureScaleAndOffset(this, propertyName, out vector);
			return new Vector2(vector.x, vector.y);
		}

		// Token: 0x06000CC4 RID: 3268 RVA: 0x00011848 File Offset: 0x0000FA48
		public void SetMatrix(string propertyName, Matrix4x4 matrix)
		{
			this.SetMatrix(Shader.PropertyToID(propertyName), matrix);
		}

		// Token: 0x06000CC5 RID: 3269 RVA: 0x00011858 File Offset: 0x0000FA58
		public void SetMatrix(int nameID, Matrix4x4 matrix)
		{
			Material.INTERNAL_CALL_SetMatrix(this, nameID, ref matrix);
		}

		// Token: 0x06000CC6 RID: 3270
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetMatrix(Material self, int nameID, ref Matrix4x4 matrix);

		// Token: 0x06000CC7 RID: 3271 RVA: 0x00011864 File Offset: 0x0000FA64
		public Matrix4x4 GetMatrix(string propertyName)
		{
			return this.GetMatrix(Shader.PropertyToID(propertyName));
		}

		// Token: 0x06000CC8 RID: 3272 RVA: 0x00011888 File Offset: 0x0000FA88
		public Matrix4x4 GetMatrix(int nameID)
		{
			Matrix4x4 result;
			Material.INTERNAL_CALL_GetMatrix(this, nameID, out result);
			return result;
		}

		// Token: 0x06000CC9 RID: 3273
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetMatrix(Material self, int nameID, out Matrix4x4 value);

		// Token: 0x06000CCA RID: 3274 RVA: 0x000118A8 File Offset: 0x0000FAA8
		public void SetFloat(string propertyName, float value)
		{
			this.SetFloat(Shader.PropertyToID(propertyName), value);
		}

		// Token: 0x06000CCB RID: 3275
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetFloat(int nameID, float value);

		// Token: 0x06000CCC RID: 3276 RVA: 0x000118B8 File Offset: 0x0000FAB8
		public float GetFloat(string propertyName)
		{
			return this.GetFloat(Shader.PropertyToID(propertyName));
		}

		// Token: 0x06000CCD RID: 3277
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetFloat(int nameID);

		// Token: 0x06000CCE RID: 3278 RVA: 0x000118DC File Offset: 0x0000FADC
		public void SetFloatArray(string name, List<float> values)
		{
			this.SetFloatArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CCF RID: 3279 RVA: 0x000118EC File Offset: 0x0000FAEC
		public void SetFloatArray(int nameID, List<float> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetFloatArrayImplList(nameID, values);
		}

		// Token: 0x06000CD0 RID: 3280
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatArrayImplList(int nameID, object values);

		// Token: 0x06000CD1 RID: 3281 RVA: 0x00011920 File Offset: 0x0000FB20
		public void SetFloatArray(string name, float[] values)
		{
			this.SetFloatArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CD2 RID: 3282 RVA: 0x00011930 File Offset: 0x0000FB30
		public void SetFloatArray(int nameID, float[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetFloatArrayImpl(nameID, values);
		}

		// Token: 0x06000CD3 RID: 3283
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatArrayImpl(int nameID, float[] values);

		// Token: 0x06000CD4 RID: 3284 RVA: 0x00011960 File Offset: 0x0000FB60
		public void SetVectorArray(string name, List<Vector4> values)
		{
			this.SetVectorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CD5 RID: 3285 RVA: 0x00011970 File Offset: 0x0000FB70
		public void SetVectorArray(int nameID, List<Vector4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetVectorArrayImplList(nameID, values);
		}

		// Token: 0x06000CD6 RID: 3286
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVectorArrayImplList(int nameID, object values);

		// Token: 0x06000CD7 RID: 3287 RVA: 0x000119A4 File Offset: 0x0000FBA4
		public void SetVectorArray(string name, Vector4[] values)
		{
			this.SetVectorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CD8 RID: 3288 RVA: 0x000119B4 File Offset: 0x0000FBB4
		public void SetVectorArray(int nameID, Vector4[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetVectorArrayImpl(nameID, values);
		}

		// Token: 0x06000CD9 RID: 3289
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetVectorArrayImpl(int nameID, Vector4[] values);

		// Token: 0x06000CDA RID: 3290 RVA: 0x000119E4 File Offset: 0x0000FBE4
		public void SetColorArray(string name, List<Color> values)
		{
			this.SetColorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CDB RID: 3291 RVA: 0x000119F4 File Offset: 0x0000FBF4
		public void SetColorArray(int nameID, List<Color> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetColorArrayImplList(nameID, values);
		}

		// Token: 0x06000CDC RID: 3292
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetColorArrayImplList(int nameID, object values);

		// Token: 0x06000CDD RID: 3293 RVA: 0x00011A28 File Offset: 0x0000FC28
		public void SetColorArray(string name, Color[] values)
		{
			this.SetColorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CDE RID: 3294 RVA: 0x00011A38 File Offset: 0x0000FC38
		public void SetColorArray(int nameID, Color[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetColorArrayImpl(nameID, values);
		}

		// Token: 0x06000CDF RID: 3295
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetColorArrayImpl(int nameID, Color[] values);

		// Token: 0x06000CE0 RID: 3296 RVA: 0x00011A68 File Offset: 0x0000FC68
		public void SetMatrixArray(string name, List<Matrix4x4> values)
		{
			this.SetMatrixArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CE1 RID: 3297 RVA: 0x00011A78 File Offset: 0x0000FC78
		public void SetMatrixArray(int nameID, List<Matrix4x4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Count == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetMatrixArrayImplList(nameID, values);
		}

		// Token: 0x06000CE2 RID: 3298
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetMatrixArrayImplList(int nameID, object values);

		// Token: 0x06000CE3 RID: 3299 RVA: 0x00011AAC File Offset: 0x0000FCAC
		public void SetMatrixArray(string name, Matrix4x4[] values)
		{
			this.SetMatrixArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CE4 RID: 3300 RVA: 0x00011ABC File Offset: 0x0000FCBC
		public void SetMatrixArray(int nameID, Matrix4x4[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			if (values.Length == 0)
			{
				throw new ArgumentException("Zero-sized array is not allowed.");
			}
			this.SetMatrixArrayImpl(nameID, values);
		}

		// Token: 0x06000CE5 RID: 3301
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetMatrixArrayImpl(int nameID, Matrix4x4[] values);

		// Token: 0x06000CE6 RID: 3302 RVA: 0x00011AEC File Offset: 0x0000FCEC
		public void GetFloatArray(string name, List<float> values)
		{
			this.GetFloatArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CE7 RID: 3303 RVA: 0x00011AFC File Offset: 0x0000FCFC
		public void GetFloatArray(int nameID, List<float> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			this.GetFloatArrayImplList(nameID, values);
		}

		// Token: 0x06000CE8 RID: 3304
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetFloatArrayImplList(int nameID, object list);

		// Token: 0x06000CE9 RID: 3305 RVA: 0x00011B18 File Offset: 0x0000FD18
		public float[] GetFloatArray(string name)
		{
			return this.GetFloatArray(Shader.PropertyToID(name));
		}

		// Token: 0x06000CEA RID: 3306 RVA: 0x00011B3C File Offset: 0x0000FD3C
		public float[] GetFloatArray(int nameID)
		{
			return this.GetFloatArrayImpl(nameID);
		}

		// Token: 0x06000CEB RID: 3307
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float[] GetFloatArrayImpl(int nameID);

		// Token: 0x06000CEC RID: 3308 RVA: 0x00011B58 File Offset: 0x0000FD58
		public void GetVectorArray(string name, List<Vector4> values)
		{
			this.GetVectorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CED RID: 3309 RVA: 0x00011B68 File Offset: 0x0000FD68
		public void GetVectorArray(int nameID, List<Vector4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			this.GetVectorArrayImplList(nameID, values);
		}

		// Token: 0x06000CEE RID: 3310
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetVectorArrayImplList(int nameID, object list);

		// Token: 0x06000CEF RID: 3311 RVA: 0x00011B84 File Offset: 0x0000FD84
		public Vector4[] GetVectorArray(string name)
		{
			return this.GetVectorArray(Shader.PropertyToID(name));
		}

		// Token: 0x06000CF0 RID: 3312 RVA: 0x00011BA8 File Offset: 0x0000FDA8
		public Vector4[] GetVectorArray(int nameID)
		{
			return this.GetVectorArrayImpl(nameID);
		}

		// Token: 0x06000CF1 RID: 3313
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Vector4[] GetVectorArrayImpl(int nameID);

		// Token: 0x06000CF2 RID: 3314 RVA: 0x00011BC4 File Offset: 0x0000FDC4
		public void GetColorArray(string name, List<Color> values)
		{
			this.GetColorArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CF3 RID: 3315 RVA: 0x00011BD4 File Offset: 0x0000FDD4
		public void GetColorArray(int nameID, List<Color> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			this.GetColorArrayImplList(nameID, values);
		}

		// Token: 0x06000CF4 RID: 3316
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetColorArrayImplList(int nameID, object list);

		// Token: 0x06000CF5 RID: 3317 RVA: 0x00011BF0 File Offset: 0x0000FDF0
		public Color[] GetColorArray(string name)
		{
			return this.GetColorArray(Shader.PropertyToID(name));
		}

		// Token: 0x06000CF6 RID: 3318 RVA: 0x00011C14 File Offset: 0x0000FE14
		public Color[] GetColorArray(int nameID)
		{
			return this.GetColorArrayImpl(nameID);
		}

		// Token: 0x06000CF7 RID: 3319
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Color[] GetColorArrayImpl(int nameID);

		// Token: 0x06000CF8 RID: 3320 RVA: 0x00011C30 File Offset: 0x0000FE30
		public void GetMatrixArray(string name, List<Matrix4x4> values)
		{
			this.GetMatrixArray(Shader.PropertyToID(name), values);
		}

		// Token: 0x06000CF9 RID: 3321 RVA: 0x00011C40 File Offset: 0x0000FE40
		public void GetMatrixArray(int nameID, List<Matrix4x4> values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			this.GetMatrixArrayImplList(nameID, values);
		}

		// Token: 0x06000CFA RID: 3322
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetMatrixArrayImplList(int nameID, object list);

		// Token: 0x06000CFB RID: 3323 RVA: 0x00011C5C File Offset: 0x0000FE5C
		public Matrix4x4[] GetMatrixArray(string name)
		{
			return this.GetMatrixArray(Shader.PropertyToID(name));
		}

		// Token: 0x06000CFC RID: 3324 RVA: 0x00011C80 File Offset: 0x0000FE80
		public Matrix4x4[] GetMatrixArray(int nameID)
		{
			return this.GetMatrixArrayImpl(nameID);
		}

		// Token: 0x06000CFD RID: 3325
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Matrix4x4[] GetMatrixArrayImpl(int nameID);

		// Token: 0x06000CFE RID: 3326 RVA: 0x00011C9C File Offset: 0x0000FE9C
		public void SetInt(string propertyName, int value)
		{
			this.SetFloat(propertyName, (float)value);
		}

		// Token: 0x06000CFF RID: 3327 RVA: 0x00011CA8 File Offset: 0x0000FEA8
		public void SetInt(int nameID, int value)
		{
			this.SetFloat(nameID, (float)value);
		}

		// Token: 0x06000D00 RID: 3328 RVA: 0x00011CB4 File Offset: 0x0000FEB4
		public int GetInt(string propertyName)
		{
			return (int)this.GetFloat(propertyName);
		}

		// Token: 0x06000D01 RID: 3329 RVA: 0x00011CD4 File Offset: 0x0000FED4
		public int GetInt(int nameID)
		{
			return (int)this.GetFloat(nameID);
		}

		// Token: 0x06000D02 RID: 3330 RVA: 0x00011CF4 File Offset: 0x0000FEF4
		public void SetBuffer(string name, ComputeBuffer value)
		{
			this.SetBuffer(Shader.PropertyToID(name), value);
		}

		// Token: 0x06000D03 RID: 3331 RVA: 0x00011D04 File Offset: 0x0000FF04
		public void SetBuffer(int nameID, ComputeBuffer value)
		{
			this.SetBufferImpl(nameID, value);
		}

		// Token: 0x06000D04 RID: 3332
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetBufferImpl(int nameID, ComputeBuffer value);

		// Token: 0x06000D05 RID: 3333 RVA: 0x00011D10 File Offset: 0x0000FF10
		public bool HasProperty(string propertyName)
		{
			return this.HasProperty(Shader.PropertyToID(propertyName));
		}

		// Token: 0x06000D06 RID: 3334
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasProperty(int nameID);

		// Token: 0x06000D07 RID: 3335
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetTag(string tag, bool searchFallbacks, [DefaultValue("\"\"")] string defaultValue);

		// Token: 0x06000D08 RID: 3336 RVA: 0x00011D34 File Offset: 0x0000FF34
		[ExcludeFromDocs]
		public string GetTag(string tag, bool searchFallbacks)
		{
			string defaultValue = "";
			return this.GetTag(tag, searchFallbacks, defaultValue);
		}

		// Token: 0x06000D09 RID: 3337
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetOverrideTag(string tag, string val);

		// Token: 0x06000D0A RID: 3338
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Lerp(Material start, Material end, float t);

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x06000D0B RID: 3339
		public extern int passCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000D0C RID: 3340
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SetPass(int pass);

		// Token: 0x06000D0D RID: 3341
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetPassName(int pass);

		// Token: 0x06000D0E RID: 3342
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int FindPass(string passName);

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x06000D0F RID: 3343
		// (set) Token: 0x06000D10 RID: 3344
		public extern int renderQueue { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000D11 RID: 3345 RVA: 0x00011D58 File Offset: 0x0000FF58
		[Obsolete("Creating materials from shader source string will be removed in the future. Use Shader assets instead.")]
		public static Material Create(string scriptContents)
		{
			return new Material(scriptContents);
		}

		// Token: 0x06000D12 RID: 3346
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateWithString([Writable] Material mono, string contents);

		// Token: 0x06000D13 RID: 3347
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateWithShader([Writable] Material mono, Shader shader);

		// Token: 0x06000D14 RID: 3348
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateWithMaterial([Writable] Material mono, Material source);

		// Token: 0x06000D15 RID: 3349
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CopyPropertiesFromMaterial(Material mat);

		// Token: 0x06000D16 RID: 3350
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void EnableKeyword(string keyword);

		// Token: 0x06000D17 RID: 3351
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void DisableKeyword(string keyword);

		// Token: 0x06000D18 RID: 3352
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsKeywordEnabled(string keyword);

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06000D19 RID: 3353
		// (set) Token: 0x06000D1A RID: 3354
		public extern string[] shaderKeywords { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06000D1B RID: 3355
		// (set) Token: 0x06000D1C RID: 3356
		public extern MaterialGlobalIlluminationFlags globalIlluminationFlags { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
