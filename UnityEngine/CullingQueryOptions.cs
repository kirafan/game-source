﻿using System;

namespace UnityEngine
{
	// Token: 0x0200002D RID: 45
	internal enum CullingQueryOptions
	{
		// Token: 0x04000043 RID: 67
		Normal,
		// Token: 0x04000044 RID: 68
		IgnoreVisibility,
		// Token: 0x04000045 RID: 69
		IgnoreDistance
	}
}
