﻿using System;

namespace UnityEngine
{
	// Token: 0x0200016A RID: 362
	public enum JointLimitState2D
	{
		// Token: 0x040003F5 RID: 1013
		Inactive,
		// Token: 0x040003F6 RID: 1014
		LowerLimit,
		// Token: 0x040003F7 RID: 1015
		UpperLimit,
		// Token: 0x040003F8 RID: 1016
		EqualLimits
	}
}
