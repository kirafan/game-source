﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Experimental.Director;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001D1 RID: 465
	[UsedByNativeCode]
	public sealed class Animator : DirectorPlayer
	{
		// Token: 0x170007C0 RID: 1984
		// (get) Token: 0x06001EDD RID: 7901
		public extern bool isOptimizable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007C1 RID: 1985
		// (get) Token: 0x06001EDE RID: 7902
		public extern bool isHuman { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007C2 RID: 1986
		// (get) Token: 0x06001EDF RID: 7903
		public extern bool hasRootMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007C3 RID: 1987
		// (get) Token: 0x06001EE0 RID: 7904
		internal extern bool isRootPositionOrRotationControlledByCurves { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007C4 RID: 1988
		// (get) Token: 0x06001EE1 RID: 7905
		public extern float humanScale { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007C5 RID: 1989
		// (get) Token: 0x06001EE2 RID: 7906
		public extern bool isInitialized { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001EE3 RID: 7907 RVA: 0x000235B8 File Offset: 0x000217B8
		public float GetFloat(string name)
		{
			return this.GetFloatString(name);
		}

		// Token: 0x06001EE4 RID: 7908 RVA: 0x000235D4 File Offset: 0x000217D4
		public float GetFloat(int id)
		{
			return this.GetFloatID(id);
		}

		// Token: 0x06001EE5 RID: 7909 RVA: 0x000235F0 File Offset: 0x000217F0
		public void SetFloat(string name, float value)
		{
			this.SetFloatString(name, value);
		}

		// Token: 0x06001EE6 RID: 7910 RVA: 0x000235FC File Offset: 0x000217FC
		public void SetFloat(string name, float value, float dampTime, float deltaTime)
		{
			this.SetFloatStringDamp(name, value, dampTime, deltaTime);
		}

		// Token: 0x06001EE7 RID: 7911 RVA: 0x0002360C File Offset: 0x0002180C
		public void SetFloat(int id, float value)
		{
			this.SetFloatID(id, value);
		}

		// Token: 0x06001EE8 RID: 7912 RVA: 0x00023618 File Offset: 0x00021818
		public void SetFloat(int id, float value, float dampTime, float deltaTime)
		{
			this.SetFloatIDDamp(id, value, dampTime, deltaTime);
		}

		// Token: 0x06001EE9 RID: 7913 RVA: 0x00023628 File Offset: 0x00021828
		public bool GetBool(string name)
		{
			return this.GetBoolString(name);
		}

		// Token: 0x06001EEA RID: 7914 RVA: 0x00023644 File Offset: 0x00021844
		public bool GetBool(int id)
		{
			return this.GetBoolID(id);
		}

		// Token: 0x06001EEB RID: 7915 RVA: 0x00023660 File Offset: 0x00021860
		public void SetBool(string name, bool value)
		{
			this.SetBoolString(name, value);
		}

		// Token: 0x06001EEC RID: 7916 RVA: 0x0002366C File Offset: 0x0002186C
		public void SetBool(int id, bool value)
		{
			this.SetBoolID(id, value);
		}

		// Token: 0x06001EED RID: 7917 RVA: 0x00023678 File Offset: 0x00021878
		public int GetInteger(string name)
		{
			return this.GetIntegerString(name);
		}

		// Token: 0x06001EEE RID: 7918 RVA: 0x00023694 File Offset: 0x00021894
		public int GetInteger(int id)
		{
			return this.GetIntegerID(id);
		}

		// Token: 0x06001EEF RID: 7919 RVA: 0x000236B0 File Offset: 0x000218B0
		public void SetInteger(string name, int value)
		{
			this.SetIntegerString(name, value);
		}

		// Token: 0x06001EF0 RID: 7920 RVA: 0x000236BC File Offset: 0x000218BC
		public void SetInteger(int id, int value)
		{
			this.SetIntegerID(id, value);
		}

		// Token: 0x06001EF1 RID: 7921 RVA: 0x000236C8 File Offset: 0x000218C8
		public void SetTrigger(string name)
		{
			this.SetTriggerString(name);
		}

		// Token: 0x06001EF2 RID: 7922 RVA: 0x000236D4 File Offset: 0x000218D4
		public void SetTrigger(int id)
		{
			this.SetTriggerID(id);
		}

		// Token: 0x06001EF3 RID: 7923 RVA: 0x000236E0 File Offset: 0x000218E0
		public void ResetTrigger(string name)
		{
			this.ResetTriggerString(name);
		}

		// Token: 0x06001EF4 RID: 7924 RVA: 0x000236EC File Offset: 0x000218EC
		public void ResetTrigger(int id)
		{
			this.ResetTriggerID(id);
		}

		// Token: 0x06001EF5 RID: 7925 RVA: 0x000236F8 File Offset: 0x000218F8
		public bool IsParameterControlledByCurve(string name)
		{
			return this.IsParameterControlledByCurveString(name);
		}

		// Token: 0x06001EF6 RID: 7926 RVA: 0x00023714 File Offset: 0x00021914
		public bool IsParameterControlledByCurve(int id)
		{
			return this.IsParameterControlledByCurveID(id);
		}

		// Token: 0x170007C6 RID: 1990
		// (get) Token: 0x06001EF7 RID: 7927 RVA: 0x00023730 File Offset: 0x00021930
		public Vector3 deltaPosition
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_deltaPosition(out result);
				return result;
			}
		}

		// Token: 0x06001EF8 RID: 7928
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_deltaPosition(out Vector3 value);

		// Token: 0x170007C7 RID: 1991
		// (get) Token: 0x06001EF9 RID: 7929 RVA: 0x00023750 File Offset: 0x00021950
		public Quaternion deltaRotation
		{
			get
			{
				Quaternion result;
				this.INTERNAL_get_deltaRotation(out result);
				return result;
			}
		}

		// Token: 0x06001EFA RID: 7930
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_deltaRotation(out Quaternion value);

		// Token: 0x170007C8 RID: 1992
		// (get) Token: 0x06001EFB RID: 7931 RVA: 0x00023770 File Offset: 0x00021970
		public Vector3 velocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_velocity(out result);
				return result;
			}
		}

		// Token: 0x06001EFC RID: 7932
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_velocity(out Vector3 value);

		// Token: 0x170007C9 RID: 1993
		// (get) Token: 0x06001EFD RID: 7933 RVA: 0x00023790 File Offset: 0x00021990
		public Vector3 angularVelocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_angularVelocity(out result);
				return result;
			}
		}

		// Token: 0x06001EFE RID: 7934
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_angularVelocity(out Vector3 value);

		// Token: 0x170007CA RID: 1994
		// (get) Token: 0x06001EFF RID: 7935 RVA: 0x000237B0 File Offset: 0x000219B0
		// (set) Token: 0x06001F00 RID: 7936 RVA: 0x000237D0 File Offset: 0x000219D0
		public Vector3 rootPosition
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_rootPosition(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_rootPosition(ref value);
			}
		}

		// Token: 0x06001F01 RID: 7937
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rootPosition(out Vector3 value);

		// Token: 0x06001F02 RID: 7938
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_rootPosition(ref Vector3 value);

		// Token: 0x170007CB RID: 1995
		// (get) Token: 0x06001F03 RID: 7939 RVA: 0x000237DC File Offset: 0x000219DC
		// (set) Token: 0x06001F04 RID: 7940 RVA: 0x000237FC File Offset: 0x000219FC
		public Quaternion rootRotation
		{
			get
			{
				Quaternion result;
				this.INTERNAL_get_rootRotation(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_rootRotation(ref value);
			}
		}

		// Token: 0x06001F05 RID: 7941
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rootRotation(out Quaternion value);

		// Token: 0x06001F06 RID: 7942
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_rootRotation(ref Quaternion value);

		// Token: 0x170007CC RID: 1996
		// (get) Token: 0x06001F07 RID: 7943
		// (set) Token: 0x06001F08 RID: 7944
		public extern bool applyRootMotion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007CD RID: 1997
		// (get) Token: 0x06001F09 RID: 7945
		// (set) Token: 0x06001F0A RID: 7946
		public extern bool linearVelocityBlending { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007CE RID: 1998
		// (get) Token: 0x06001F0B RID: 7947 RVA: 0x00023808 File Offset: 0x00021A08
		// (set) Token: 0x06001F0C RID: 7948 RVA: 0x00023828 File Offset: 0x00021A28
		[Obsolete("Use Animator.updateMode instead")]
		public bool animatePhysics
		{
			get
			{
				return this.updateMode == AnimatorUpdateMode.AnimatePhysics;
			}
			set
			{
				this.updateMode = ((!value) ? AnimatorUpdateMode.Normal : AnimatorUpdateMode.AnimatePhysics);
			}
		}

		// Token: 0x170007CF RID: 1999
		// (get) Token: 0x06001F0D RID: 7949
		// (set) Token: 0x06001F0E RID: 7950
		public extern AnimatorUpdateMode updateMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007D0 RID: 2000
		// (get) Token: 0x06001F0F RID: 7951
		public extern bool hasTransformHierarchy { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007D1 RID: 2001
		// (get) Token: 0x06001F10 RID: 7952
		// (set) Token: 0x06001F11 RID: 7953
		internal extern bool allowConstantClipSamplingOptimization { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007D2 RID: 2002
		// (get) Token: 0x06001F12 RID: 7954
		public extern float gravityWeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007D3 RID: 2003
		// (get) Token: 0x06001F13 RID: 7955 RVA: 0x00023840 File Offset: 0x00021A40
		// (set) Token: 0x06001F14 RID: 7956 RVA: 0x00023864 File Offset: 0x00021A64
		public Vector3 bodyPosition
		{
			get
			{
				this.CheckIfInIKPass();
				return this.GetBodyPositionInternal();
			}
			set
			{
				this.CheckIfInIKPass();
				this.SetBodyPositionInternal(value);
			}
		}

		// Token: 0x06001F15 RID: 7957 RVA: 0x00023874 File Offset: 0x00021A74
		internal Vector3 GetBodyPositionInternal()
		{
			Vector3 result;
			Animator.INTERNAL_CALL_GetBodyPositionInternal(this, out result);
			return result;
		}

		// Token: 0x06001F16 RID: 7958
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetBodyPositionInternal(Animator self, out Vector3 value);

		// Token: 0x06001F17 RID: 7959 RVA: 0x00023894 File Offset: 0x00021A94
		internal void SetBodyPositionInternal(Vector3 bodyPosition)
		{
			Animator.INTERNAL_CALL_SetBodyPositionInternal(this, ref bodyPosition);
		}

		// Token: 0x06001F18 RID: 7960
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetBodyPositionInternal(Animator self, ref Vector3 bodyPosition);

		// Token: 0x170007D4 RID: 2004
		// (get) Token: 0x06001F19 RID: 7961 RVA: 0x000238A0 File Offset: 0x00021AA0
		// (set) Token: 0x06001F1A RID: 7962 RVA: 0x000238C4 File Offset: 0x00021AC4
		public Quaternion bodyRotation
		{
			get
			{
				this.CheckIfInIKPass();
				return this.GetBodyRotationInternal();
			}
			set
			{
				this.CheckIfInIKPass();
				this.SetBodyRotationInternal(value);
			}
		}

		// Token: 0x06001F1B RID: 7963 RVA: 0x000238D4 File Offset: 0x00021AD4
		internal Quaternion GetBodyRotationInternal()
		{
			Quaternion result;
			Animator.INTERNAL_CALL_GetBodyRotationInternal(this, out result);
			return result;
		}

		// Token: 0x06001F1C RID: 7964
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetBodyRotationInternal(Animator self, out Quaternion value);

		// Token: 0x06001F1D RID: 7965 RVA: 0x000238F4 File Offset: 0x00021AF4
		internal void SetBodyRotationInternal(Quaternion bodyRotation)
		{
			Animator.INTERNAL_CALL_SetBodyRotationInternal(this, ref bodyRotation);
		}

		// Token: 0x06001F1E RID: 7966
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetBodyRotationInternal(Animator self, ref Quaternion bodyRotation);

		// Token: 0x06001F1F RID: 7967 RVA: 0x00023900 File Offset: 0x00021B00
		public Vector3 GetIKPosition(AvatarIKGoal goal)
		{
			this.CheckIfInIKPass();
			return this.GetIKPositionInternal(goal);
		}

		// Token: 0x06001F20 RID: 7968 RVA: 0x00023924 File Offset: 0x00021B24
		internal Vector3 GetIKPositionInternal(AvatarIKGoal goal)
		{
			Vector3 result;
			Animator.INTERNAL_CALL_GetIKPositionInternal(this, goal, out result);
			return result;
		}

		// Token: 0x06001F21 RID: 7969
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetIKPositionInternal(Animator self, AvatarIKGoal goal, out Vector3 value);

		// Token: 0x06001F22 RID: 7970 RVA: 0x00023944 File Offset: 0x00021B44
		public void SetIKPosition(AvatarIKGoal goal, Vector3 goalPosition)
		{
			this.CheckIfInIKPass();
			this.SetIKPositionInternal(goal, goalPosition);
		}

		// Token: 0x06001F23 RID: 7971 RVA: 0x00023958 File Offset: 0x00021B58
		internal void SetIKPositionInternal(AvatarIKGoal goal, Vector3 goalPosition)
		{
			Animator.INTERNAL_CALL_SetIKPositionInternal(this, goal, ref goalPosition);
		}

		// Token: 0x06001F24 RID: 7972
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetIKPositionInternal(Animator self, AvatarIKGoal goal, ref Vector3 goalPosition);

		// Token: 0x06001F25 RID: 7973 RVA: 0x00023964 File Offset: 0x00021B64
		public Quaternion GetIKRotation(AvatarIKGoal goal)
		{
			this.CheckIfInIKPass();
			return this.GetIKRotationInternal(goal);
		}

		// Token: 0x06001F26 RID: 7974 RVA: 0x00023988 File Offset: 0x00021B88
		internal Quaternion GetIKRotationInternal(AvatarIKGoal goal)
		{
			Quaternion result;
			Animator.INTERNAL_CALL_GetIKRotationInternal(this, goal, out result);
			return result;
		}

		// Token: 0x06001F27 RID: 7975
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetIKRotationInternal(Animator self, AvatarIKGoal goal, out Quaternion value);

		// Token: 0x06001F28 RID: 7976 RVA: 0x000239A8 File Offset: 0x00021BA8
		public void SetIKRotation(AvatarIKGoal goal, Quaternion goalRotation)
		{
			this.CheckIfInIKPass();
			this.SetIKRotationInternal(goal, goalRotation);
		}

		// Token: 0x06001F29 RID: 7977 RVA: 0x000239BC File Offset: 0x00021BBC
		internal void SetIKRotationInternal(AvatarIKGoal goal, Quaternion goalRotation)
		{
			Animator.INTERNAL_CALL_SetIKRotationInternal(this, goal, ref goalRotation);
		}

		// Token: 0x06001F2A RID: 7978
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetIKRotationInternal(Animator self, AvatarIKGoal goal, ref Quaternion goalRotation);

		// Token: 0x06001F2B RID: 7979 RVA: 0x000239C8 File Offset: 0x00021BC8
		public float GetIKPositionWeight(AvatarIKGoal goal)
		{
			this.CheckIfInIKPass();
			return this.GetIKPositionWeightInternal(goal);
		}

		// Token: 0x06001F2C RID: 7980
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern float GetIKPositionWeightInternal(AvatarIKGoal goal);

		// Token: 0x06001F2D RID: 7981 RVA: 0x000239EC File Offset: 0x00021BEC
		public void SetIKPositionWeight(AvatarIKGoal goal, float value)
		{
			this.CheckIfInIKPass();
			this.SetIKPositionWeightInternal(goal, value);
		}

		// Token: 0x06001F2E RID: 7982
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetIKPositionWeightInternal(AvatarIKGoal goal, float value);

		// Token: 0x06001F2F RID: 7983 RVA: 0x00023A00 File Offset: 0x00021C00
		public float GetIKRotationWeight(AvatarIKGoal goal)
		{
			this.CheckIfInIKPass();
			return this.GetIKRotationWeightInternal(goal);
		}

		// Token: 0x06001F30 RID: 7984
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern float GetIKRotationWeightInternal(AvatarIKGoal goal);

		// Token: 0x06001F31 RID: 7985 RVA: 0x00023A24 File Offset: 0x00021C24
		public void SetIKRotationWeight(AvatarIKGoal goal, float value)
		{
			this.CheckIfInIKPass();
			this.SetIKRotationWeightInternal(goal, value);
		}

		// Token: 0x06001F32 RID: 7986
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetIKRotationWeightInternal(AvatarIKGoal goal, float value);

		// Token: 0x06001F33 RID: 7987 RVA: 0x00023A38 File Offset: 0x00021C38
		public Vector3 GetIKHintPosition(AvatarIKHint hint)
		{
			this.CheckIfInIKPass();
			return this.GetIKHintPositionInternal(hint);
		}

		// Token: 0x06001F34 RID: 7988 RVA: 0x00023A5C File Offset: 0x00021C5C
		internal Vector3 GetIKHintPositionInternal(AvatarIKHint hint)
		{
			Vector3 result;
			Animator.INTERNAL_CALL_GetIKHintPositionInternal(this, hint, out result);
			return result;
		}

		// Token: 0x06001F35 RID: 7989
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetIKHintPositionInternal(Animator self, AvatarIKHint hint, out Vector3 value);

		// Token: 0x06001F36 RID: 7990 RVA: 0x00023A7C File Offset: 0x00021C7C
		public void SetIKHintPosition(AvatarIKHint hint, Vector3 hintPosition)
		{
			this.CheckIfInIKPass();
			this.SetIKHintPositionInternal(hint, hintPosition);
		}

		// Token: 0x06001F37 RID: 7991 RVA: 0x00023A90 File Offset: 0x00021C90
		internal void SetIKHintPositionInternal(AvatarIKHint hint, Vector3 hintPosition)
		{
			Animator.INTERNAL_CALL_SetIKHintPositionInternal(this, hint, ref hintPosition);
		}

		// Token: 0x06001F38 RID: 7992
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetIKHintPositionInternal(Animator self, AvatarIKHint hint, ref Vector3 hintPosition);

		// Token: 0x06001F39 RID: 7993 RVA: 0x00023A9C File Offset: 0x00021C9C
		public float GetIKHintPositionWeight(AvatarIKHint hint)
		{
			this.CheckIfInIKPass();
			return this.GetHintWeightPositionInternal(hint);
		}

		// Token: 0x06001F3A RID: 7994
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern float GetHintWeightPositionInternal(AvatarIKHint hint);

		// Token: 0x06001F3B RID: 7995 RVA: 0x00023AC0 File Offset: 0x00021CC0
		public void SetIKHintPositionWeight(AvatarIKHint hint, float value)
		{
			this.CheckIfInIKPass();
			this.SetIKHintPositionWeightInternal(hint, value);
		}

		// Token: 0x06001F3C RID: 7996
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetIKHintPositionWeightInternal(AvatarIKHint hint, float value);

		// Token: 0x06001F3D RID: 7997 RVA: 0x00023AD4 File Offset: 0x00021CD4
		public void SetLookAtPosition(Vector3 lookAtPosition)
		{
			this.CheckIfInIKPass();
			this.SetLookAtPositionInternal(lookAtPosition);
		}

		// Token: 0x06001F3E RID: 7998 RVA: 0x00023AE4 File Offset: 0x00021CE4
		internal void SetLookAtPositionInternal(Vector3 lookAtPosition)
		{
			Animator.INTERNAL_CALL_SetLookAtPositionInternal(this, ref lookAtPosition);
		}

		// Token: 0x06001F3F RID: 7999
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetLookAtPositionInternal(Animator self, ref Vector3 lookAtPosition);

		// Token: 0x06001F40 RID: 8000 RVA: 0x00023AF0 File Offset: 0x00021CF0
		[ExcludeFromDocs]
		public void SetLookAtWeight(float weight, float bodyWeight, float headWeight, float eyesWeight)
		{
			float clampWeight = 0.5f;
			this.SetLookAtWeight(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x06001F41 RID: 8001 RVA: 0x00023B10 File Offset: 0x00021D10
		[ExcludeFromDocs]
		public void SetLookAtWeight(float weight, float bodyWeight, float headWeight)
		{
			float clampWeight = 0.5f;
			float eyesWeight = 0f;
			this.SetLookAtWeight(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x06001F42 RID: 8002 RVA: 0x00023B38 File Offset: 0x00021D38
		[ExcludeFromDocs]
		public void SetLookAtWeight(float weight, float bodyWeight)
		{
			float clampWeight = 0.5f;
			float eyesWeight = 0f;
			float headWeight = 1f;
			this.SetLookAtWeight(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x06001F43 RID: 8003 RVA: 0x00023B64 File Offset: 0x00021D64
		[ExcludeFromDocs]
		public void SetLookAtWeight(float weight)
		{
			float clampWeight = 0.5f;
			float eyesWeight = 0f;
			float headWeight = 1f;
			float bodyWeight = 0f;
			this.SetLookAtWeight(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x06001F44 RID: 8004 RVA: 0x00023B98 File Offset: 0x00021D98
		public void SetLookAtWeight(float weight, [DefaultValue("0.00f")] float bodyWeight, [DefaultValue("1.00f")] float headWeight, [DefaultValue("0.00f")] float eyesWeight, [DefaultValue("0.50f")] float clampWeight)
		{
			this.CheckIfInIKPass();
			this.SetLookAtWeightInternal(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x06001F45 RID: 8005
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetLookAtWeightInternal(float weight, [DefaultValue("0.00f")] float bodyWeight, [DefaultValue("1.00f")] float headWeight, [DefaultValue("0.00f")] float eyesWeight, [DefaultValue("0.50f")] float clampWeight);

		// Token: 0x06001F46 RID: 8006 RVA: 0x00023BB0 File Offset: 0x00021DB0
		[ExcludeFromDocs]
		internal void SetLookAtWeightInternal(float weight, float bodyWeight, float headWeight, float eyesWeight)
		{
			float clampWeight = 0.5f;
			this.SetLookAtWeightInternal(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x06001F47 RID: 8007 RVA: 0x00023BD0 File Offset: 0x00021DD0
		[ExcludeFromDocs]
		internal void SetLookAtWeightInternal(float weight, float bodyWeight, float headWeight)
		{
			float clampWeight = 0.5f;
			float eyesWeight = 0f;
			this.SetLookAtWeightInternal(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x06001F48 RID: 8008 RVA: 0x00023BF8 File Offset: 0x00021DF8
		[ExcludeFromDocs]
		internal void SetLookAtWeightInternal(float weight, float bodyWeight)
		{
			float clampWeight = 0.5f;
			float eyesWeight = 0f;
			float headWeight = 1f;
			this.SetLookAtWeightInternal(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x06001F49 RID: 8009 RVA: 0x00023C24 File Offset: 0x00021E24
		[ExcludeFromDocs]
		internal void SetLookAtWeightInternal(float weight)
		{
			float clampWeight = 0.5f;
			float eyesWeight = 0f;
			float headWeight = 1f;
			float bodyWeight = 0f;
			this.SetLookAtWeightInternal(weight, bodyWeight, headWeight, eyesWeight, clampWeight);
		}

		// Token: 0x06001F4A RID: 8010 RVA: 0x00023C58 File Offset: 0x00021E58
		public void SetBoneLocalRotation(HumanBodyBones humanBoneId, Quaternion rotation)
		{
			this.CheckIfInIKPass();
			this.SetBoneLocalRotationInternal(humanBoneId, rotation);
		}

		// Token: 0x06001F4B RID: 8011 RVA: 0x00023C6C File Offset: 0x00021E6C
		internal void SetBoneLocalRotationInternal(HumanBodyBones humanBoneId, Quaternion rotation)
		{
			Animator.INTERNAL_CALL_SetBoneLocalRotationInternal(this, humanBoneId, ref rotation);
		}

		// Token: 0x06001F4C RID: 8012
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetBoneLocalRotationInternal(Animator self, HumanBodyBones humanBoneId, ref Quaternion rotation);

		// Token: 0x06001F4D RID: 8013
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern ScriptableObject GetBehaviour(Type type);

		// Token: 0x06001F4E RID: 8014 RVA: 0x00023C78 File Offset: 0x00021E78
		public T GetBehaviour<T>() where T : StateMachineBehaviour
		{
			return this.GetBehaviour(typeof(T)) as T;
		}

		// Token: 0x06001F4F RID: 8015
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern ScriptableObject[] GetBehaviours(Type type);

		// Token: 0x06001F50 RID: 8016 RVA: 0x00023CA8 File Offset: 0x00021EA8
		internal static T[] ConvertStateMachineBehaviour<T>(ScriptableObject[] rawObjects) where T : StateMachineBehaviour
		{
			T[] result;
			if (rawObjects == null)
			{
				result = null;
			}
			else
			{
				T[] array = new T[rawObjects.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = (T)((object)rawObjects[i]);
				}
				result = array;
			}
			return result;
		}

		// Token: 0x06001F51 RID: 8017 RVA: 0x00023CF8 File Offset: 0x00021EF8
		public T[] GetBehaviours<T>() where T : StateMachineBehaviour
		{
			return Animator.ConvertStateMachineBehaviour<T>(this.GetBehaviours(typeof(T)));
		}

		// Token: 0x170007D5 RID: 2005
		// (get) Token: 0x06001F52 RID: 8018
		// (set) Token: 0x06001F53 RID: 8019
		public extern bool stabilizeFeet { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007D6 RID: 2006
		// (get) Token: 0x06001F54 RID: 8020
		public extern int layerCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001F55 RID: 8021
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern string GetLayerName(int layerIndex);

		// Token: 0x06001F56 RID: 8022
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetLayerIndex(string layerName);

		// Token: 0x06001F57 RID: 8023
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetLayerWeight(int layerIndex);

		// Token: 0x06001F58 RID: 8024
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetLayerWeight(int layerIndex, float weight);

		// Token: 0x06001F59 RID: 8025
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimatorStateInfo GetCurrentAnimatorStateInfo(int layerIndex);

		// Token: 0x06001F5A RID: 8026
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimatorStateInfo GetNextAnimatorStateInfo(int layerIndex);

		// Token: 0x06001F5B RID: 8027
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimatorTransitionInfo GetAnimatorTransitionInfo(int layerIndex);

		// Token: 0x06001F5C RID: 8028
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetCurrentAnimatorClipInfoCount(int layerIndex);

		// Token: 0x06001F5D RID: 8029
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimatorClipInfo[] GetCurrentAnimatorClipInfo(int layerIndex);

		// Token: 0x06001F5E RID: 8030 RVA: 0x00023D24 File Offset: 0x00021F24
		public void GetCurrentAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips)
		{
			if (clips == null)
			{
				throw new ArgumentNullException("clips");
			}
			this.GetAnimatorClipInfoInternal(layerIndex, true, clips);
		}

		// Token: 0x06001F5F RID: 8031
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetAnimatorClipInfoInternal(int layerIndex, bool isCurrent, object clips);

		// Token: 0x06001F60 RID: 8032
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetNextAnimatorClipInfoCount(int layerIndex);

		// Token: 0x06001F61 RID: 8033
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimatorClipInfo[] GetNextAnimatorClipInfo(int layerIndex);

		// Token: 0x06001F62 RID: 8034 RVA: 0x00023D44 File Offset: 0x00021F44
		public void GetNextAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips)
		{
			if (clips == null)
			{
				throw new ArgumentNullException("clips");
			}
			this.GetAnimatorClipInfoInternal(layerIndex, false, clips);
		}

		// Token: 0x06001F63 RID: 8035
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsInTransition(int layerIndex);

		// Token: 0x170007D7 RID: 2007
		// (get) Token: 0x06001F64 RID: 8036
		public extern AnimatorControllerParameter[] parameters { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007D8 RID: 2008
		// (get) Token: 0x06001F65 RID: 8037
		public extern int parameterCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001F66 RID: 8038 RVA: 0x00023D64 File Offset: 0x00021F64
		public AnimatorControllerParameter GetParameter(int index)
		{
			AnimatorControllerParameter[] parameters = this.parameters;
			if (index < 0 && index >= this.parameters.Length)
			{
				throw new IndexOutOfRangeException("Index must be between 0 and " + this.parameters.Length);
			}
			return parameters[index];
		}

		// Token: 0x170007D9 RID: 2009
		// (get) Token: 0x06001F67 RID: 8039
		// (set) Token: 0x06001F68 RID: 8040
		public extern float feetPivotActive { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007DA RID: 2010
		// (get) Token: 0x06001F69 RID: 8041
		public extern float pivotWeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007DB RID: 2011
		// (get) Token: 0x06001F6A RID: 8042 RVA: 0x00023DB8 File Offset: 0x00021FB8
		public Vector3 pivotPosition
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_pivotPosition(out result);
				return result;
			}
		}

		// Token: 0x06001F6B RID: 8043
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_pivotPosition(out Vector3 value);

		// Token: 0x06001F6C RID: 8044 RVA: 0x00023DD8 File Offset: 0x00021FD8
		public void MatchTarget(Vector3 matchPosition, Quaternion matchRotation, AvatarTarget targetBodyPart, MatchTargetWeightMask weightMask, float startNormalizedTime, [DefaultValue("1")] float targetNormalizedTime)
		{
			Animator.INTERNAL_CALL_MatchTarget(this, ref matchPosition, ref matchRotation, targetBodyPart, ref weightMask, startNormalizedTime, targetNormalizedTime);
		}

		// Token: 0x06001F6D RID: 8045 RVA: 0x00023DEC File Offset: 0x00021FEC
		[ExcludeFromDocs]
		public void MatchTarget(Vector3 matchPosition, Quaternion matchRotation, AvatarTarget targetBodyPart, MatchTargetWeightMask weightMask, float startNormalizedTime)
		{
			float targetNormalizedTime = 1f;
			Animator.INTERNAL_CALL_MatchTarget(this, ref matchPosition, ref matchRotation, targetBodyPart, ref weightMask, startNormalizedTime, targetNormalizedTime);
		}

		// Token: 0x06001F6E RID: 8046
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_MatchTarget(Animator self, ref Vector3 matchPosition, ref Quaternion matchRotation, AvatarTarget targetBodyPart, ref MatchTargetWeightMask weightMask, float startNormalizedTime, float targetNormalizedTime);

		// Token: 0x06001F6F RID: 8047
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InterruptMatchTarget([DefaultValue("true")] bool completeMatch);

		// Token: 0x06001F70 RID: 8048 RVA: 0x00023E10 File Offset: 0x00022010
		[ExcludeFromDocs]
		public void InterruptMatchTarget()
		{
			bool completeMatch = true;
			this.InterruptMatchTarget(completeMatch);
		}

		// Token: 0x170007DC RID: 2012
		// (get) Token: 0x06001F71 RID: 8049
		public extern bool isMatchingTarget { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007DD RID: 2013
		// (get) Token: 0x06001F72 RID: 8050
		// (set) Token: 0x06001F73 RID: 8051
		public extern float speed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001F74 RID: 8052 RVA: 0x00023E28 File Offset: 0x00022028
		[Obsolete("ForceStateNormalizedTime is deprecated. Please use Play or CrossFade instead.")]
		public void ForceStateNormalizedTime(float normalizedTime)
		{
			this.Play(0, 0, normalizedTime);
		}

		// Token: 0x06001F75 RID: 8053 RVA: 0x00023E34 File Offset: 0x00022034
		[ExcludeFromDocs]
		public void CrossFadeInFixedTime(string stateName, float transitionDuration, int layer)
		{
			float fixedTime = 0f;
			this.CrossFadeInFixedTime(stateName, transitionDuration, layer, fixedTime);
		}

		// Token: 0x06001F76 RID: 8054 RVA: 0x00023E54 File Offset: 0x00022054
		[ExcludeFromDocs]
		public void CrossFadeInFixedTime(string stateName, float transitionDuration)
		{
			float fixedTime = 0f;
			int layer = -1;
			this.CrossFadeInFixedTime(stateName, transitionDuration, layer, fixedTime);
		}

		// Token: 0x06001F77 RID: 8055 RVA: 0x00023E74 File Offset: 0x00022074
		public void CrossFadeInFixedTime(string stateName, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("0.0f")] float fixedTime)
		{
			this.CrossFadeInFixedTime(Animator.StringToHash(stateName), transitionDuration, layer, fixedTime);
		}

		// Token: 0x06001F78 RID: 8056
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CrossFadeInFixedTime(int stateNameHash, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("0.0f")] float fixedTime);

		// Token: 0x06001F79 RID: 8057 RVA: 0x00023E88 File Offset: 0x00022088
		[ExcludeFromDocs]
		public void CrossFadeInFixedTime(int stateNameHash, float transitionDuration, int layer)
		{
			float fixedTime = 0f;
			this.CrossFadeInFixedTime(stateNameHash, transitionDuration, layer, fixedTime);
		}

		// Token: 0x06001F7A RID: 8058 RVA: 0x00023EA8 File Offset: 0x000220A8
		[ExcludeFromDocs]
		public void CrossFadeInFixedTime(int stateNameHash, float transitionDuration)
		{
			float fixedTime = 0f;
			int layer = -1;
			this.CrossFadeInFixedTime(stateNameHash, transitionDuration, layer, fixedTime);
		}

		// Token: 0x06001F7B RID: 8059 RVA: 0x00023EC8 File Offset: 0x000220C8
		[ExcludeFromDocs]
		public void CrossFade(string stateName, float transitionDuration, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.CrossFade(stateName, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x06001F7C RID: 8060 RVA: 0x00023EE8 File Offset: 0x000220E8
		[ExcludeFromDocs]
		public void CrossFade(string stateName, float transitionDuration)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.CrossFade(stateName, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x06001F7D RID: 8061 RVA: 0x00023F08 File Offset: 0x00022108
		public void CrossFade(string stateName, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			this.CrossFade(Animator.StringToHash(stateName), transitionDuration, layer, normalizedTime);
		}

		// Token: 0x06001F7E RID: 8062
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CrossFade(int stateNameHash, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime);

		// Token: 0x06001F7F RID: 8063 RVA: 0x00023F1C File Offset: 0x0002211C
		[ExcludeFromDocs]
		public void CrossFade(int stateNameHash, float transitionDuration, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.CrossFade(stateNameHash, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x06001F80 RID: 8064 RVA: 0x00023F3C File Offset: 0x0002213C
		[ExcludeFromDocs]
		public void CrossFade(int stateNameHash, float transitionDuration)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.CrossFade(stateNameHash, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x06001F81 RID: 8065 RVA: 0x00023F5C File Offset: 0x0002215C
		[ExcludeFromDocs]
		public void PlayInFixedTime(string stateName, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.PlayInFixedTime(stateName, layer, negativeInfinity);
		}

		// Token: 0x06001F82 RID: 8066 RVA: 0x00023F7C File Offset: 0x0002217C
		[ExcludeFromDocs]
		public void PlayInFixedTime(string stateName)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.PlayInFixedTime(stateName, layer, negativeInfinity);
		}

		// Token: 0x06001F83 RID: 8067 RVA: 0x00023F9C File Offset: 0x0002219C
		public void PlayInFixedTime(string stateName, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float fixedTime)
		{
			this.PlayInFixedTime(Animator.StringToHash(stateName), layer, fixedTime);
		}

		// Token: 0x06001F84 RID: 8068
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void PlayInFixedTime(int stateNameHash, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float fixedTime);

		// Token: 0x06001F85 RID: 8069 RVA: 0x00023FB0 File Offset: 0x000221B0
		[ExcludeFromDocs]
		public void PlayInFixedTime(int stateNameHash, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.PlayInFixedTime(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x06001F86 RID: 8070 RVA: 0x00023FD0 File Offset: 0x000221D0
		[ExcludeFromDocs]
		public void PlayInFixedTime(int stateNameHash)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.PlayInFixedTime(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x06001F87 RID: 8071 RVA: 0x00023FF0 File Offset: 0x000221F0
		[ExcludeFromDocs]
		public void Play(string stateName, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.Play(stateName, layer, negativeInfinity);
		}

		// Token: 0x06001F88 RID: 8072 RVA: 0x00024010 File Offset: 0x00022210
		[ExcludeFromDocs]
		public void Play(string stateName)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.Play(stateName, layer, negativeInfinity);
		}

		// Token: 0x06001F89 RID: 8073 RVA: 0x00024030 File Offset: 0x00022230
		public void Play(string stateName, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			this.Play(Animator.StringToHash(stateName), layer, normalizedTime);
		}

		// Token: 0x06001F8A RID: 8074
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Play(int stateNameHash, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime);

		// Token: 0x06001F8B RID: 8075 RVA: 0x00024044 File Offset: 0x00022244
		[ExcludeFromDocs]
		public void Play(int stateNameHash, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.Play(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x06001F8C RID: 8076 RVA: 0x00024064 File Offset: 0x00022264
		[ExcludeFromDocs]
		public void Play(int stateNameHash)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.Play(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x06001F8D RID: 8077
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTarget(AvatarTarget targetIndex, float targetNormalizedTime);

		// Token: 0x170007DE RID: 2014
		// (get) Token: 0x06001F8E RID: 8078 RVA: 0x00024084 File Offset: 0x00022284
		public Vector3 targetPosition
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_targetPosition(out result);
				return result;
			}
		}

		// Token: 0x06001F8F RID: 8079
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_targetPosition(out Vector3 value);

		// Token: 0x170007DF RID: 2015
		// (get) Token: 0x06001F90 RID: 8080 RVA: 0x000240A4 File Offset: 0x000222A4
		public Quaternion targetRotation
		{
			get
			{
				Quaternion result;
				this.INTERNAL_get_targetRotation(out result);
				return result;
			}
		}

		// Token: 0x06001F91 RID: 8081
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_targetRotation(out Quaternion value);

		// Token: 0x06001F92 RID: 8082
		[Obsolete("use mask and layers to control subset of transfroms in a skeleton", true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsControlled(Transform transform);

		// Token: 0x06001F93 RID: 8083
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool IsBoneTransform(Transform transform);

		// Token: 0x170007E0 RID: 2016
		// (get) Token: 0x06001F94 RID: 8084
		internal extern Transform avatarRoot { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001F95 RID: 8085
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Transform GetBoneTransform(HumanBodyBones humanBoneId);

		// Token: 0x170007E1 RID: 2017
		// (get) Token: 0x06001F96 RID: 8086
		// (set) Token: 0x06001F97 RID: 8087
		public extern AnimatorCullingMode cullingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001F98 RID: 8088
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StartPlayback();

		// Token: 0x06001F99 RID: 8089
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StopPlayback();

		// Token: 0x170007E2 RID: 2018
		// (get) Token: 0x06001F9A RID: 8090
		// (set) Token: 0x06001F9B RID: 8091
		public extern float playbackTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001F9C RID: 8092
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StartRecording(int frameCount);

		// Token: 0x06001F9D RID: 8093
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StopRecording();

		// Token: 0x170007E3 RID: 2019
		// (get) Token: 0x06001F9E RID: 8094
		// (set) Token: 0x06001F9F RID: 8095
		public extern float recorderStartTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007E4 RID: 2020
		// (get) Token: 0x06001FA0 RID: 8096
		// (set) Token: 0x06001FA1 RID: 8097
		public extern float recorderStopTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007E5 RID: 2021
		// (get) Token: 0x06001FA2 RID: 8098
		public extern AnimatorRecorderMode recorderMode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007E6 RID: 2022
		// (get) Token: 0x06001FA3 RID: 8099
		// (set) Token: 0x06001FA4 RID: 8100
		public extern RuntimeAnimatorController runtimeAnimatorController { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001FA5 RID: 8101
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasState(int layerIndex, int stateID);

		// Token: 0x06001FA6 RID: 8102
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int StringToHash(string name);

		// Token: 0x170007E7 RID: 2023
		// (get) Token: 0x06001FA7 RID: 8103
		// (set) Token: 0x06001FA8 RID: 8104
		public extern Avatar avatar { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001FA9 RID: 8105
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string GetStats();

		// Token: 0x06001FAA RID: 8106 RVA: 0x000240C4 File Offset: 0x000222C4
		private void CheckIfInIKPass()
		{
			if (this.logWarnings && !this.CheckIfInIKPassInternal())
			{
				Debug.LogWarning("Setting and getting Body Position/Rotation, IK Goals, Lookat and BoneLocalRotation should only be done in OnAnimatorIK or OnStateIK");
			}
		}

		// Token: 0x06001FAB RID: 8107
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool CheckIfInIKPassInternal();

		// Token: 0x06001FAC RID: 8108
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatString(string name, float value);

		// Token: 0x06001FAD RID: 8109
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatID(int id, float value);

		// Token: 0x06001FAE RID: 8110
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetFloatString(string name);

		// Token: 0x06001FAF RID: 8111
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern float GetFloatID(int id);

		// Token: 0x06001FB0 RID: 8112
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetBoolString(string name, bool value);

		// Token: 0x06001FB1 RID: 8113
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetBoolID(int id, bool value);

		// Token: 0x06001FB2 RID: 8114
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool GetBoolString(string name);

		// Token: 0x06001FB3 RID: 8115
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool GetBoolID(int id);

		// Token: 0x06001FB4 RID: 8116
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetIntegerString(string name, int value);

		// Token: 0x06001FB5 RID: 8117
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetIntegerID(int id, int value);

		// Token: 0x06001FB6 RID: 8118
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetIntegerString(string name);

		// Token: 0x06001FB7 RID: 8119
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetIntegerID(int id);

		// Token: 0x06001FB8 RID: 8120
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTriggerString(string name);

		// Token: 0x06001FB9 RID: 8121
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetTriggerID(int id);

		// Token: 0x06001FBA RID: 8122
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ResetTriggerString(string name);

		// Token: 0x06001FBB RID: 8123
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ResetTriggerID(int id);

		// Token: 0x06001FBC RID: 8124
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsParameterControlledByCurveString(string name);

		// Token: 0x06001FBD RID: 8125
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool IsParameterControlledByCurveID(int id);

		// Token: 0x06001FBE RID: 8126
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatStringDamp(string name, float value, float dampTime, float deltaTime);

		// Token: 0x06001FBF RID: 8127
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFloatIDDamp(int id, float value, float dampTime, float deltaTime);

		// Token: 0x170007E8 RID: 2024
		// (get) Token: 0x06001FC0 RID: 8128
		// (set) Token: 0x06001FC1 RID: 8129
		public extern bool layersAffectMassCenter { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007E9 RID: 2025
		// (get) Token: 0x06001FC2 RID: 8130
		public extern float leftFeetBottomHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170007EA RID: 2026
		// (get) Token: 0x06001FC3 RID: 8131
		public extern float rightFeetBottomHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001FC4 RID: 8132
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Update(float deltaTime);

		// Token: 0x06001FC5 RID: 8133
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Rebind();

		// Token: 0x06001FC6 RID: 8134
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ApplyBuiltinRootMotion();

		// Token: 0x06001FC7 RID: 8135
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string ResolveHash(int hash);

		// Token: 0x170007EB RID: 2027
		// (get) Token: 0x06001FC8 RID: 8136
		// (set) Token: 0x06001FC9 RID: 8137
		public extern bool logWarnings { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170007EC RID: 2028
		// (get) Token: 0x06001FCA RID: 8138
		// (set) Token: 0x06001FCB RID: 8139
		public extern bool fireEvents { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001FCC RID: 8140 RVA: 0x000240E8 File Offset: 0x000222E8
		[Obsolete("GetVector is deprecated.")]
		public Vector3 GetVector(string name)
		{
			return Vector3.zero;
		}

		// Token: 0x06001FCD RID: 8141 RVA: 0x00024104 File Offset: 0x00022304
		[Obsolete("GetVector is deprecated.")]
		public Vector3 GetVector(int id)
		{
			return Vector3.zero;
		}

		// Token: 0x06001FCE RID: 8142 RVA: 0x00024120 File Offset: 0x00022320
		[Obsolete("SetVector is deprecated.")]
		public void SetVector(string name, Vector3 value)
		{
		}

		// Token: 0x06001FCF RID: 8143 RVA: 0x00024124 File Offset: 0x00022324
		[Obsolete("SetVector is deprecated.")]
		public void SetVector(int id, Vector3 value)
		{
		}

		// Token: 0x06001FD0 RID: 8144 RVA: 0x00024128 File Offset: 0x00022328
		[Obsolete("GetQuaternion is deprecated.")]
		public Quaternion GetQuaternion(string name)
		{
			return Quaternion.identity;
		}

		// Token: 0x06001FD1 RID: 8145 RVA: 0x00024144 File Offset: 0x00022344
		[Obsolete("GetQuaternion is deprecated.")]
		public Quaternion GetQuaternion(int id)
		{
			return Quaternion.identity;
		}

		// Token: 0x06001FD2 RID: 8146 RVA: 0x00024160 File Offset: 0x00022360
		[Obsolete("SetQuaternion is deprecated.")]
		public void SetQuaternion(string name, Quaternion value)
		{
		}

		// Token: 0x06001FD3 RID: 8147 RVA: 0x00024164 File Offset: 0x00022364
		[Obsolete("SetQuaternion is deprecated.")]
		public void SetQuaternion(int id, Quaternion value)
		{
		}
	}
}
