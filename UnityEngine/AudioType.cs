﻿using System;

namespace UnityEngine
{
	// Token: 0x0200019B RID: 411
	public enum AudioType
	{
		// Token: 0x0400044C RID: 1100
		UNKNOWN,
		// Token: 0x0400044D RID: 1101
		ACC,
		// Token: 0x0400044E RID: 1102
		AIFF,
		// Token: 0x0400044F RID: 1103
		IT = 10,
		// Token: 0x04000450 RID: 1104
		MOD = 12,
		// Token: 0x04000451 RID: 1105
		MPEG,
		// Token: 0x04000452 RID: 1106
		OGGVORBIS,
		// Token: 0x04000453 RID: 1107
		S3M = 17,
		// Token: 0x04000454 RID: 1108
		WAV = 20,
		// Token: 0x04000455 RID: 1109
		XM,
		// Token: 0x04000456 RID: 1110
		XMA,
		// Token: 0x04000457 RID: 1111
		VAG,
		// Token: 0x04000458 RID: 1112
		AUDIOQUEUE
	}
}
