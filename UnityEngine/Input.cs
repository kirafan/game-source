﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000077 RID: 119
	public sealed class Input
	{
		// Token: 0x060007B2 RID: 1970
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int mainGyroIndex_Internal();

		// Token: 0x060007B3 RID: 1971
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyInt(int key);

		// Token: 0x060007B4 RID: 1972
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyString(string name);

		// Token: 0x060007B5 RID: 1973
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyUpInt(int key);

		// Token: 0x060007B6 RID: 1974
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyUpString(string name);

		// Token: 0x060007B7 RID: 1975
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyDownInt(int key);

		// Token: 0x060007B8 RID: 1976
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetKeyDownString(string name);

		// Token: 0x060007B9 RID: 1977
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetAxis(string axisName);

		// Token: 0x060007BA RID: 1978
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GetAxisRaw(string axisName);

		// Token: 0x060007BB RID: 1979
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetButton(string buttonName);

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x060007BC RID: 1980
		// (set) Token: 0x060007BD RID: 1981
		public static extern bool compensateSensors { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x060007BE RID: 1982
		[Obsolete("isGyroAvailable property is deprecated. Please use SystemInfo.supportsGyroscope instead.")]
		public static extern bool isGyroAvailable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x060007BF RID: 1983 RVA: 0x00009DD4 File Offset: 0x00007FD4
		public static Gyroscope gyro
		{
			get
			{
				if (Input.m_MainGyro == null)
				{
					Input.m_MainGyro = new Gyroscope(Input.mainGyroIndex_Internal());
				}
				return Input.m_MainGyro;
			}
		}

		// Token: 0x060007C0 RID: 1984
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetButtonDown(string buttonName);

		// Token: 0x060007C1 RID: 1985
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetButtonUp(string buttonName);

		// Token: 0x060007C2 RID: 1986 RVA: 0x00009E08 File Offset: 0x00008008
		public static bool GetKey(string name)
		{
			return Input.GetKeyString(name);
		}

		// Token: 0x060007C3 RID: 1987 RVA: 0x00009E24 File Offset: 0x00008024
		public static bool GetKey(KeyCode key)
		{
			return Input.GetKeyInt((int)key);
		}

		// Token: 0x060007C4 RID: 1988 RVA: 0x00009E40 File Offset: 0x00008040
		public static bool GetKeyDown(string name)
		{
			return Input.GetKeyDownString(name);
		}

		// Token: 0x060007C5 RID: 1989 RVA: 0x00009E5C File Offset: 0x0000805C
		public static bool GetKeyDown(KeyCode key)
		{
			return Input.GetKeyDownInt((int)key);
		}

		// Token: 0x060007C6 RID: 1990 RVA: 0x00009E78 File Offset: 0x00008078
		public static bool GetKeyUp(string name)
		{
			return Input.GetKeyUpString(name);
		}

		// Token: 0x060007C7 RID: 1991 RVA: 0x00009E94 File Offset: 0x00008094
		public static bool GetKeyUp(KeyCode key)
		{
			return Input.GetKeyUpInt((int)key);
		}

		// Token: 0x060007C8 RID: 1992
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string[] GetJoystickNames();

		// Token: 0x060007C9 RID: 1993
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetMouseButton(int button);

		// Token: 0x060007CA RID: 1994
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetMouseButtonDown(int button);

		// Token: 0x060007CB RID: 1995
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool GetMouseButtonUp(int button);

		// Token: 0x060007CC RID: 1996
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ResetInputAxes();

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x060007CD RID: 1997 RVA: 0x00009EB0 File Offset: 0x000080B0
		public static Vector3 mousePosition
		{
			get
			{
				Vector3 result;
				Input.INTERNAL_get_mousePosition(out result);
				return result;
			}
		}

		// Token: 0x060007CE RID: 1998
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_mousePosition(out Vector3 value);

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x060007CF RID: 1999 RVA: 0x00009ED0 File Offset: 0x000080D0
		public static Vector2 mouseScrollDelta
		{
			get
			{
				Vector2 result;
				Input.INTERNAL_get_mouseScrollDelta(out result);
				return result;
			}
		}

		// Token: 0x060007D0 RID: 2000
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_mouseScrollDelta(out Vector2 value);

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x060007D1 RID: 2001
		public static extern bool mousePresent { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x060007D2 RID: 2002
		// (set) Token: 0x060007D3 RID: 2003
		public static extern bool simulateMouseWithTouches { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x060007D4 RID: 2004
		public static extern bool anyKey { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x060007D5 RID: 2005
		public static extern bool anyKeyDown { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x060007D6 RID: 2006
		public static extern string inputString { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x060007D7 RID: 2007 RVA: 0x00009EF0 File Offset: 0x000080F0
		public static Vector3 acceleration
		{
			get
			{
				Vector3 result;
				Input.INTERNAL_get_acceleration(out result);
				return result;
			}
		}

		// Token: 0x060007D8 RID: 2008
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_acceleration(out Vector3 value);

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x060007D9 RID: 2009 RVA: 0x00009F10 File Offset: 0x00008110
		public static AccelerationEvent[] accelerationEvents
		{
			get
			{
				int accelerationEventCount = Input.accelerationEventCount;
				AccelerationEvent[] array = new AccelerationEvent[accelerationEventCount];
				for (int i = 0; i < accelerationEventCount; i++)
				{
					array[i] = Input.GetAccelerationEvent(i);
				}
				return array;
			}
		}

		// Token: 0x060007DA RID: 2010 RVA: 0x00009F58 File Offset: 0x00008158
		public static AccelerationEvent GetAccelerationEvent(int index)
		{
			AccelerationEvent result;
			Input.INTERNAL_CALL_GetAccelerationEvent(index, out result);
			return result;
		}

		// Token: 0x060007DB RID: 2011
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetAccelerationEvent(int index, out AccelerationEvent value);

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x060007DC RID: 2012
		public static extern int accelerationEventCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x060007DD RID: 2013 RVA: 0x00009F78 File Offset: 0x00008178
		public static Touch[] touches
		{
			get
			{
				int touchCount = Input.touchCount;
				Touch[] array = new Touch[touchCount];
				for (int i = 0; i < touchCount; i++)
				{
					array[i] = Input.GetTouch(i);
				}
				return array;
			}
		}

		// Token: 0x060007DE RID: 2014 RVA: 0x00009FC0 File Offset: 0x000081C0
		public static Touch GetTouch(int index)
		{
			Touch result;
			Input.INTERNAL_CALL_GetTouch(index, out result);
			return result;
		}

		// Token: 0x060007DF RID: 2015
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetTouch(int index, out Touch value);

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x060007E0 RID: 2016
		public static extern int touchCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x060007E1 RID: 2017
		// (set) Token: 0x060007E2 RID: 2018
		[Obsolete("eatKeyPressOnTextFieldFocus property is deprecated, and only provided to support legacy behavior.")]
		public static extern bool eatKeyPressOnTextFieldFocus { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x060007E3 RID: 2019
		public static extern bool touchPressureSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x060007E4 RID: 2020
		public static extern bool stylusTouchSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001DB RID: 475
		// (get) Token: 0x060007E5 RID: 2021
		public static extern bool touchSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001DC RID: 476
		// (get) Token: 0x060007E6 RID: 2022
		// (set) Token: 0x060007E7 RID: 2023
		public static extern bool multiTouchEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001DD RID: 477
		// (get) Token: 0x060007E8 RID: 2024 RVA: 0x00009FE0 File Offset: 0x000081E0
		public static LocationService location
		{
			get
			{
				if (Input.locationServiceInstance == null)
				{
					Input.locationServiceInstance = new LocationService();
				}
				return Input.locationServiceInstance;
			}
		}

		// Token: 0x170001DE RID: 478
		// (get) Token: 0x060007E9 RID: 2025 RVA: 0x0000A010 File Offset: 0x00008210
		public static Compass compass
		{
			get
			{
				if (Input.compassInstance == null)
				{
					Input.compassInstance = new Compass();
				}
				return Input.compassInstance;
			}
		}

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x060007EA RID: 2026
		public static extern DeviceOrientation deviceOrientation { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x060007EB RID: 2027
		// (set) Token: 0x060007EC RID: 2028
		public static extern IMECompositionMode imeCompositionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x060007ED RID: 2029
		public static extern string compositionString { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x060007EE RID: 2030
		public static extern bool imeIsSelected { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x060007EF RID: 2031 RVA: 0x0000A040 File Offset: 0x00008240
		// (set) Token: 0x060007F0 RID: 2032 RVA: 0x0000A060 File Offset: 0x00008260
		public static Vector2 compositionCursorPos
		{
			get
			{
				Vector2 result;
				Input.INTERNAL_get_compositionCursorPos(out result);
				return result;
			}
			set
			{
				Input.INTERNAL_set_compositionCursorPos(ref value);
			}
		}

		// Token: 0x060007F1 RID: 2033
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_get_compositionCursorPos(out Vector2 value);

		// Token: 0x060007F2 RID: 2034
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_set_compositionCursorPos(ref Vector2 value);

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x060007F3 RID: 2035
		// (set) Token: 0x060007F4 RID: 2036
		public static extern bool backButtonLeavesApp { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x040000E0 RID: 224
		private static Gyroscope m_MainGyro = null;

		// Token: 0x040000E1 RID: 225
		private static LocationService locationServiceInstance;

		// Token: 0x040000E2 RID: 226
		private static Compass compassInstance;
	}
}
