﻿using System;

namespace UnityEngine
{
	// Token: 0x020001BD RID: 445
	public enum PlayMode
	{
		// Token: 0x040004BB RID: 1211
		StopSameLayer,
		// Token: 0x040004BC RID: 1212
		StopAll = 4
	}
}
