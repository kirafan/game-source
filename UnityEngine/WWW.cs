﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020000DA RID: 218
	[UsedByNativeCode]
	public sealed class WWW : IDisposable
	{
		// Token: 0x06000F60 RID: 3936 RVA: 0x00014AB8 File Offset: 0x00012CB8
		public WWW(string url)
		{
			this.InitWWW(url, null, null);
		}

		// Token: 0x06000F61 RID: 3937 RVA: 0x00014ACC File Offset: 0x00012CCC
		public WWW(string url, WWWForm form)
		{
			string[] iHeaders = WWW.FlattenedHeadersFrom(form.headers);
			this.InitWWW(url, form.data, iHeaders);
		}

		// Token: 0x06000F62 RID: 3938 RVA: 0x00014AFC File Offset: 0x00012CFC
		public WWW(string url, byte[] postData)
		{
			this.InitWWW(url, postData, null);
		}

		// Token: 0x06000F63 RID: 3939 RVA: 0x00014B10 File Offset: 0x00012D10
		[Obsolete("This overload is deprecated. Use UnityEngine.WWW.WWW(string, byte[], System.Collections.Generic.Dictionary<string, string>) instead.", true)]
		public WWW(string url, byte[] postData, Hashtable headers)
		{
			Debug.LogError("This overload is deprecated. Use UnityEngine.WWW.WWW(string, byte[], System.Collections.Generic.Dictionary<string, string>) instead");
		}

		// Token: 0x06000F64 RID: 3940 RVA: 0x00014B24 File Offset: 0x00012D24
		public WWW(string url, byte[] postData, Dictionary<string, string> headers)
		{
			string[] iHeaders = WWW.FlattenedHeadersFrom(headers);
			this.InitWWW(url, postData, iHeaders);
		}

		// Token: 0x06000F65 RID: 3941 RVA: 0x00014B48 File Offset: 0x00012D48
		internal WWW(string url, Hash128 hash, uint crc)
		{
			WWW.INTERNAL_CALL_WWW(this, url, ref hash, crc);
		}

		// Token: 0x06000F66 RID: 3942 RVA: 0x00014B5C File Offset: 0x00012D5C
		public void Dispose()
		{
			this.DestroyWWW(true);
		}

		// Token: 0x06000F67 RID: 3943 RVA: 0x00014B68 File Offset: 0x00012D68
		~WWW()
		{
			this.DestroyWWW(false);
		}

		// Token: 0x06000F68 RID: 3944
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void DestroyWWW(bool cancel);

		// Token: 0x06000F69 RID: 3945
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InitWWW(string url, byte[] postData, string[] iHeaders);

		// Token: 0x06000F6A RID: 3946
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool enforceWebSecurityRestrictions();

		// Token: 0x06000F6B RID: 3947 RVA: 0x00014B9C File Offset: 0x00012D9C
		[ExcludeFromDocs]
		public static string EscapeURL(string s)
		{
			Encoding utf = Encoding.UTF8;
			return WWW.EscapeURL(s, utf);
		}

		// Token: 0x06000F6C RID: 3948 RVA: 0x00014BC0 File Offset: 0x00012DC0
		public static string EscapeURL(string s, [DefaultValue("System.Text.Encoding.UTF8")] Encoding e)
		{
			string result;
			if (s == null)
			{
				result = null;
			}
			else if (s == "")
			{
				result = "";
			}
			else if (e == null)
			{
				result = null;
			}
			else
			{
				result = WWWTranscoder.URLEncode(s, e);
			}
			return result;
		}

		// Token: 0x06000F6D RID: 3949 RVA: 0x00014C14 File Offset: 0x00012E14
		[ExcludeFromDocs]
		public static string UnEscapeURL(string s)
		{
			Encoding utf = Encoding.UTF8;
			return WWW.UnEscapeURL(s, utf);
		}

		// Token: 0x06000F6E RID: 3950 RVA: 0x00014C38 File Offset: 0x00012E38
		public static string UnEscapeURL(string s, [DefaultValue("System.Text.Encoding.UTF8")] Encoding e)
		{
			string result;
			if (s == null)
			{
				result = null;
			}
			else if (s.IndexOf('%') == -1 && s.IndexOf('+') == -1)
			{
				result = s;
			}
			else
			{
				result = WWWTranscoder.URLDecode(s, e);
			}
			return result;
		}

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x06000F6F RID: 3951 RVA: 0x00014C84 File Offset: 0x00012E84
		public Dictionary<string, string> responseHeaders
		{
			get
			{
				if (!this.isDone)
				{
					throw new UnityException("WWW is not finished downloading yet");
				}
				return WWW.ParseHTTPHeaderString(this.responseHeadersString);
			}
		}

		// Token: 0x1700034E RID: 846
		// (get) Token: 0x06000F70 RID: 3952
		private extern string responseHeadersString { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700034F RID: 847
		// (get) Token: 0x06000F71 RID: 3953 RVA: 0x00014CBC File Offset: 0x00012EBC
		public string text
		{
			get
			{
				if (!this.isDone)
				{
					throw new UnityException("WWW is not ready downloading yet");
				}
				byte[] bytes = this.bytes;
				return this.GetTextEncoder().GetString(bytes, 0, bytes.Length);
			}
		}

		// Token: 0x17000350 RID: 848
		// (get) Token: 0x06000F72 RID: 3954 RVA: 0x00014D00 File Offset: 0x00012F00
		internal static Encoding DefaultEncoding
		{
			get
			{
				return Encoding.ASCII;
			}
		}

		// Token: 0x06000F73 RID: 3955 RVA: 0x00014D1C File Offset: 0x00012F1C
		private Encoding GetTextEncoder()
		{
			string text = null;
			if (this.responseHeaders.TryGetValue("CONTENT-TYPE", out text))
			{
				int num = text.IndexOf("charset", StringComparison.OrdinalIgnoreCase);
				if (num > -1)
				{
					int num2 = text.IndexOf('=', num);
					if (num2 > -1)
					{
						string text2 = text.Substring(num2 + 1).Trim().Trim(new char[]
						{
							'\'',
							'"'
						}).Trim();
						int num3 = text2.IndexOf(';');
						if (num3 > -1)
						{
							text2 = text2.Substring(0, num3);
						}
						try
						{
							return Encoding.GetEncoding(text2);
						}
						catch (Exception)
						{
							Debug.Log("Unsupported encoding: '" + text2 + "'");
						}
					}
				}
			}
			return Encoding.UTF8;
		}

		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06000F74 RID: 3956 RVA: 0x00014DFC File Offset: 0x00012FFC
		[Obsolete("Please use WWW.text instead")]
		public string data
		{
			get
			{
				return this.text;
			}
		}

		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06000F75 RID: 3957
		public extern byte[] bytes { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000353 RID: 851
		// (get) Token: 0x06000F76 RID: 3958
		public extern int size { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000354 RID: 852
		// (get) Token: 0x06000F77 RID: 3959
		public extern string error { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000F78 RID: 3960
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Texture2D GetTexture(bool markNonReadable);

		// Token: 0x17000355 RID: 853
		// (get) Token: 0x06000F79 RID: 3961 RVA: 0x00014E18 File Offset: 0x00013018
		public Texture2D texture
		{
			get
			{
				return this.GetTexture(false);
			}
		}

		// Token: 0x17000356 RID: 854
		// (get) Token: 0x06000F7A RID: 3962 RVA: 0x00014E34 File Offset: 0x00013034
		public Texture2D textureNonReadable
		{
			get
			{
				return this.GetTexture(true);
			}
		}

		// Token: 0x17000357 RID: 855
		// (get) Token: 0x06000F7B RID: 3963 RVA: 0x00014E50 File Offset: 0x00013050
		public AudioClip audioClip
		{
			get
			{
				return this.GetAudioClip(true);
			}
		}

		// Token: 0x06000F7C RID: 3964 RVA: 0x00014E6C File Offset: 0x0001306C
		public AudioClip GetAudioClip(bool threeD)
		{
			return this.GetAudioClip(threeD, false);
		}

		// Token: 0x06000F7D RID: 3965 RVA: 0x00014E8C File Offset: 0x0001308C
		public AudioClip GetAudioClip(bool threeD, bool stream)
		{
			return this.GetAudioClip(threeD, stream, AudioType.UNKNOWN);
		}

		// Token: 0x06000F7E RID: 3966 RVA: 0x00014EAC File Offset: 0x000130AC
		public AudioClip GetAudioClip(bool threeD, bool stream, AudioType audioType)
		{
			return this.GetAudioClipInternal(threeD, stream, false, audioType);
		}

		// Token: 0x06000F7F RID: 3967 RVA: 0x00014ECC File Offset: 0x000130CC
		public AudioClip GetAudioClipCompressed()
		{
			return this.GetAudioClipCompressed(true);
		}

		// Token: 0x06000F80 RID: 3968 RVA: 0x00014EE8 File Offset: 0x000130E8
		public AudioClip GetAudioClipCompressed(bool threeD)
		{
			return this.GetAudioClipCompressed(threeD, AudioType.UNKNOWN);
		}

		// Token: 0x06000F81 RID: 3969 RVA: 0x00014F08 File Offset: 0x00013108
		public AudioClip GetAudioClipCompressed(bool threeD, AudioType audioType)
		{
			return this.GetAudioClipInternal(threeD, false, true, audioType);
		}

		// Token: 0x06000F82 RID: 3970
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AudioClip GetAudioClipInternal(bool threeD, bool stream, bool compressed, AudioType audioType);

		// Token: 0x06000F83 RID: 3971
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void LoadImageIntoTexture(Texture2D tex);

		// Token: 0x17000358 RID: 856
		// (get) Token: 0x06000F84 RID: 3972
		public extern bool isDone { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000F85 RID: 3973
		[Obsolete("All blocking WWW functions have been deprecated, please use one of the asynchronous functions instead.", true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string GetURL(string url);

		// Token: 0x06000F86 RID: 3974 RVA: 0x00014F28 File Offset: 0x00013128
		[Obsolete("All blocking WWW functions have been deprecated, please use one of the asynchronous functions instead.", true)]
		public static Texture2D GetTextureFromURL(string url)
		{
			return new WWW(url).texture;
		}

		// Token: 0x17000359 RID: 857
		// (get) Token: 0x06000F87 RID: 3975
		public extern float progress { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700035A RID: 858
		// (get) Token: 0x06000F88 RID: 3976
		public extern float uploadProgress { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x06000F89 RID: 3977
		public extern int bytesDownloaded { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06000F8A RID: 3978 RVA: 0x00014F48 File Offset: 0x00013148
		[Obsolete("Property WWW.oggVorbis has been deprecated. Use WWW.audioClip instead (UnityUpgradable).", true)]
		public AudioClip oggVorbis
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06000F8B RID: 3979 RVA: 0x00014F60 File Offset: 0x00013160
		[Obsolete("LoadUnityWeb is no longer supported. Please use javascript to reload the web player on a different url instead", true)]
		public void LoadUnityWeb()
		{
		}

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06000F8C RID: 3980
		public extern string url { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06000F8D RID: 3981
		public extern AssetBundle assetBundle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06000F8E RID: 3982
		// (set) Token: 0x06000F8F RID: 3983
		public extern ThreadPriority threadPriority { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000F90 RID: 3984
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_WWW(WWW self, string url, ref Hash128 hash, uint crc);

		// Token: 0x06000F91 RID: 3985 RVA: 0x00014F64 File Offset: 0x00013164
		[ExcludeFromDocs]
		public static WWW LoadFromCacheOrDownload(string url, int version)
		{
			uint crc = 0U;
			return WWW.LoadFromCacheOrDownload(url, version, crc);
		}

		// Token: 0x06000F92 RID: 3986 RVA: 0x00014F84 File Offset: 0x00013184
		public static WWW LoadFromCacheOrDownload(string url, int version, [DefaultValue("0")] uint crc)
		{
			Hash128 hash = new Hash128(0U, 0U, 0U, (uint)version);
			return WWW.LoadFromCacheOrDownload(url, hash, crc);
		}

		// Token: 0x06000F93 RID: 3987 RVA: 0x00014FAC File Offset: 0x000131AC
		[ExcludeFromDocs]
		public static WWW LoadFromCacheOrDownload(string url, Hash128 hash)
		{
			uint crc = 0U;
			return WWW.LoadFromCacheOrDownload(url, hash, crc);
		}

		// Token: 0x06000F94 RID: 3988 RVA: 0x00014FCC File Offset: 0x000131CC
		public static WWW LoadFromCacheOrDownload(string url, Hash128 hash, [DefaultValue("0")] uint crc)
		{
			return new WWW(url, hash, crc);
		}

		// Token: 0x06000F95 RID: 3989 RVA: 0x00014FEC File Offset: 0x000131EC
		private static string[] FlattenedHeadersFrom(Dictionary<string, string> headers)
		{
			string[] result;
			if (headers == null)
			{
				result = null;
			}
			else
			{
				string[] array = new string[headers.Count * 2];
				int num = 0;
				foreach (KeyValuePair<string, string> keyValuePair in headers)
				{
					array[num++] = keyValuePair.Key.ToString();
					array[num++] = keyValuePair.Value.ToString();
				}
				result = array;
			}
			return result;
		}

		// Token: 0x06000F96 RID: 3990 RVA: 0x0001508C File Offset: 0x0001328C
		internal static Dictionary<string, string> ParseHTTPHeaderString(string input)
		{
			if (input == null)
			{
				throw new ArgumentException("input was null to ParseHTTPHeaderString");
			}
			Dictionary<string, string> dictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
			StringReader stringReader = new StringReader(input);
			int num = 0;
			for (;;)
			{
				string text = stringReader.ReadLine();
				if (text == null)
				{
					break;
				}
				if (num++ == 0 && text.StartsWith("HTTP"))
				{
					dictionary["STATUS"] = text;
				}
				else
				{
					int num2 = text.IndexOf(": ");
					if (num2 != -1)
					{
						string key = text.Substring(0, num2).ToUpper();
						string text2 = text.Substring(num2 + 2);
						string str;
						if (dictionary.TryGetValue(key, out str))
						{
							text2 = str + "," + text2;
						}
						dictionary[key] = text2;
					}
				}
			}
			return dictionary;
		}

		// Token: 0x04000223 RID: 547
		[RequiredByNativeCode]
		internal IntPtr m_Ptr;
	}
}
