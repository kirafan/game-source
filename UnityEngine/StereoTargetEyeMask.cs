﻿using System;

namespace UnityEngine
{
	// Token: 0x020002E5 RID: 741
	public enum StereoTargetEyeMask
	{
		// Token: 0x04000AE7 RID: 2791
		None,
		// Token: 0x04000AE8 RID: 2792
		Left,
		// Token: 0x04000AE9 RID: 2793
		Right,
		// Token: 0x04000AEA RID: 2794
		Both
	}
}
