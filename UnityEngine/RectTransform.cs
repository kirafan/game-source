﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020000A8 RID: 168
	public sealed class RectTransform : Transform
	{
		// Token: 0x1700029C RID: 668
		// (get) Token: 0x06000B61 RID: 2913 RVA: 0x0000FEC0 File Offset: 0x0000E0C0
		public Rect rect
		{
			get
			{
				Rect result;
				this.INTERNAL_get_rect(out result);
				return result;
			}
		}

		// Token: 0x06000B62 RID: 2914
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rect(out Rect value);

		// Token: 0x1700029D RID: 669
		// (get) Token: 0x06000B63 RID: 2915 RVA: 0x0000FEE0 File Offset: 0x0000E0E0
		// (set) Token: 0x06000B64 RID: 2916 RVA: 0x0000FF00 File Offset: 0x0000E100
		public Vector2 anchorMin
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_anchorMin(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_anchorMin(ref value);
			}
		}

		// Token: 0x06000B65 RID: 2917
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_anchorMin(out Vector2 value);

		// Token: 0x06000B66 RID: 2918
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_anchorMin(ref Vector2 value);

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x06000B67 RID: 2919 RVA: 0x0000FF0C File Offset: 0x0000E10C
		// (set) Token: 0x06000B68 RID: 2920 RVA: 0x0000FF2C File Offset: 0x0000E12C
		public Vector2 anchorMax
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_anchorMax(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_anchorMax(ref value);
			}
		}

		// Token: 0x06000B69 RID: 2921
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_anchorMax(out Vector2 value);

		// Token: 0x06000B6A RID: 2922
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_anchorMax(ref Vector2 value);

		// Token: 0x1700029F RID: 671
		// (get) Token: 0x06000B6B RID: 2923 RVA: 0x0000FF38 File Offset: 0x0000E138
		// (set) Token: 0x06000B6C RID: 2924 RVA: 0x0000FF78 File Offset: 0x0000E178
		public Vector3 anchoredPosition3D
		{
			get
			{
				Vector2 anchoredPosition = this.anchoredPosition;
				return new Vector3(anchoredPosition.x, anchoredPosition.y, base.localPosition.z);
			}
			set
			{
				this.anchoredPosition = new Vector2(value.x, value.y);
				Vector3 localPosition = base.localPosition;
				localPosition.z = value.z;
				base.localPosition = localPosition;
			}
		}

		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x06000B6D RID: 2925 RVA: 0x0000FFBC File Offset: 0x0000E1BC
		// (set) Token: 0x06000B6E RID: 2926 RVA: 0x0000FFDC File Offset: 0x0000E1DC
		public Vector2 anchoredPosition
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_anchoredPosition(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_anchoredPosition(ref value);
			}
		}

		// Token: 0x06000B6F RID: 2927
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_anchoredPosition(out Vector2 value);

		// Token: 0x06000B70 RID: 2928
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_anchoredPosition(ref Vector2 value);

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000B71 RID: 2929 RVA: 0x0000FFE8 File Offset: 0x0000E1E8
		// (set) Token: 0x06000B72 RID: 2930 RVA: 0x00010008 File Offset: 0x0000E208
		public Vector2 sizeDelta
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_sizeDelta(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_sizeDelta(ref value);
			}
		}

		// Token: 0x06000B73 RID: 2931
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_sizeDelta(out Vector2 value);

		// Token: 0x06000B74 RID: 2932
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_sizeDelta(ref Vector2 value);

		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000B75 RID: 2933 RVA: 0x00010014 File Offset: 0x0000E214
		// (set) Token: 0x06000B76 RID: 2934 RVA: 0x00010034 File Offset: 0x0000E234
		public Vector2 pivot
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_pivot(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_pivot(ref value);
			}
		}

		// Token: 0x06000B77 RID: 2935
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_pivot(out Vector2 value);

		// Token: 0x06000B78 RID: 2936
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_pivot(ref Vector2 value);

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x06000B79 RID: 2937
		// (set) Token: 0x06000B7A RID: 2938
		internal extern Object drivenByObject { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06000B7B RID: 2939
		// (set) Token: 0x06000B7C RID: 2940
		internal extern DrivenTransformProperties drivenProperties { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x06000B7D RID: 2941 RVA: 0x00010040 File Offset: 0x0000E240
		// (remove) Token: 0x06000B7E RID: 2942 RVA: 0x00010074 File Offset: 0x0000E274
		public static event RectTransform.ReapplyDrivenProperties reapplyDrivenProperties;

		// Token: 0x06000B7F RID: 2943 RVA: 0x000100A8 File Offset: 0x0000E2A8
		[RequiredByNativeCode]
		internal static void SendReapplyDrivenProperties(RectTransform driven)
		{
			if (RectTransform.reapplyDrivenProperties != null)
			{
				RectTransform.reapplyDrivenProperties(driven);
			}
		}

		// Token: 0x06000B80 RID: 2944 RVA: 0x000100C0 File Offset: 0x0000E2C0
		public void GetLocalCorners(Vector3[] fourCornersArray)
		{
			if (fourCornersArray == null || fourCornersArray.Length < 4)
			{
				Debug.LogError("Calling GetLocalCorners with an array that is null or has less than 4 elements.");
			}
			else
			{
				Rect rect = this.rect;
				float x = rect.x;
				float y = rect.y;
				float xMax = rect.xMax;
				float yMax = rect.yMax;
				fourCornersArray[0] = new Vector3(x, y, 0f);
				fourCornersArray[1] = new Vector3(x, yMax, 0f);
				fourCornersArray[2] = new Vector3(xMax, yMax, 0f);
				fourCornersArray[3] = new Vector3(xMax, y, 0f);
			}
		}

		// Token: 0x06000B81 RID: 2945 RVA: 0x00010178 File Offset: 0x0000E378
		public void GetWorldCorners(Vector3[] fourCornersArray)
		{
			if (fourCornersArray == null || fourCornersArray.Length < 4)
			{
				Debug.LogError("Calling GetWorldCorners with an array that is null or has less than 4 elements.");
			}
			else
			{
				this.GetLocalCorners(fourCornersArray);
				Transform transform = base.transform;
				for (int i = 0; i < 4; i++)
				{
					fourCornersArray[i] = transform.TransformPoint(fourCornersArray[i]);
				}
			}
		}

		// Token: 0x06000B82 RID: 2946 RVA: 0x000101E4 File Offset: 0x0000E3E4
		internal Rect GetRectInParentSpace()
		{
			Rect rect = this.rect;
			Vector2 a = this.offsetMin + Vector2.Scale(this.pivot, rect.size);
			Transform parent = base.transform.parent;
			if (parent)
			{
				RectTransform component = parent.GetComponent<RectTransform>();
				if (component)
				{
					a += Vector2.Scale(this.anchorMin, component.rect.size);
				}
			}
			rect.x += a.x;
			rect.y += a.y;
			return rect;
		}

		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x06000B83 RID: 2947 RVA: 0x00010298 File Offset: 0x0000E498
		// (set) Token: 0x06000B84 RID: 2948 RVA: 0x000102CC File Offset: 0x0000E4CC
		public Vector2 offsetMin
		{
			get
			{
				return this.anchoredPosition - Vector2.Scale(this.sizeDelta, this.pivot);
			}
			set
			{
				Vector2 vector = value - (this.anchoredPosition - Vector2.Scale(this.sizeDelta, this.pivot));
				this.sizeDelta -= vector;
				this.anchoredPosition += Vector2.Scale(vector, Vector2.one - this.pivot);
			}
		}

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000B85 RID: 2949 RVA: 0x00010338 File Offset: 0x0000E538
		// (set) Token: 0x06000B86 RID: 2950 RVA: 0x00010374 File Offset: 0x0000E574
		public Vector2 offsetMax
		{
			get
			{
				return this.anchoredPosition + Vector2.Scale(this.sizeDelta, Vector2.one - this.pivot);
			}
			set
			{
				Vector2 vector = value - (this.anchoredPosition + Vector2.Scale(this.sizeDelta, Vector2.one - this.pivot));
				this.sizeDelta += vector;
				this.anchoredPosition += Vector2.Scale(vector, this.pivot);
			}
		}

		// Token: 0x06000B87 RID: 2951 RVA: 0x000103E0 File Offset: 0x0000E5E0
		public void SetInsetAndSizeFromParentEdge(RectTransform.Edge edge, float inset, float size)
		{
			int index = (edge != RectTransform.Edge.Top && edge != RectTransform.Edge.Bottom) ? 0 : 1;
			bool flag = edge == RectTransform.Edge.Top || edge == RectTransform.Edge.Right;
			float value = (float)((!flag) ? 0 : 1);
			Vector2 vector = this.anchorMin;
			vector[index] = value;
			this.anchorMin = vector;
			vector = this.anchorMax;
			vector[index] = value;
			this.anchorMax = vector;
			Vector2 sizeDelta = this.sizeDelta;
			sizeDelta[index] = size;
			this.sizeDelta = sizeDelta;
			Vector2 anchoredPosition = this.anchoredPosition;
			anchoredPosition[index] = ((!flag) ? (inset + size * this.pivot[index]) : (-inset - size * (1f - this.pivot[index])));
			this.anchoredPosition = anchoredPosition;
		}

		// Token: 0x06000B88 RID: 2952 RVA: 0x000104BC File Offset: 0x0000E6BC
		public void SetSizeWithCurrentAnchors(RectTransform.Axis axis, float size)
		{
			Vector2 sizeDelta = this.sizeDelta;
			sizeDelta[(int)axis] = size - this.GetParentSize()[(int)axis] * (this.anchorMax[(int)axis] - this.anchorMin[(int)axis]);
			this.sizeDelta = sizeDelta;
		}

		// Token: 0x06000B89 RID: 2953 RVA: 0x00010514 File Offset: 0x0000E714
		private Vector2 GetParentSize()
		{
			RectTransform rectTransform = base.parent as RectTransform;
			Vector2 result;
			if (!rectTransform)
			{
				result = Vector2.zero;
			}
			else
			{
				result = rectTransform.rect.size;
			}
			return result;
		}

		// Token: 0x020000A9 RID: 169
		// (Invoke) Token: 0x06000B8B RID: 2955
		public delegate void ReapplyDrivenProperties(RectTransform driven);

		// Token: 0x020000AA RID: 170
		public enum Edge
		{
			// Token: 0x04000196 RID: 406
			Left,
			// Token: 0x04000197 RID: 407
			Right,
			// Token: 0x04000198 RID: 408
			Top,
			// Token: 0x04000199 RID: 409
			Bottom
		}

		// Token: 0x020000AB RID: 171
		public enum Axis
		{
			// Token: 0x0400019B RID: 411
			Horizontal,
			// Token: 0x0400019C RID: 412
			Vertical
		}
	}
}
