﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001D2 RID: 466
	[UsedByNativeCode]
	public sealed class AnimatorControllerParameter
	{
		// Token: 0x170007ED RID: 2029
		// (get) Token: 0x06001FD5 RID: 8149 RVA: 0x0002417C File Offset: 0x0002237C
		public string name
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x170007EE RID: 2030
		// (get) Token: 0x06001FD6 RID: 8150 RVA: 0x00024198 File Offset: 0x00022398
		public int nameHash
		{
			get
			{
				return Animator.StringToHash(this.m_Name);
			}
		}

		// Token: 0x170007EF RID: 2031
		// (get) Token: 0x06001FD7 RID: 8151 RVA: 0x000241B8 File Offset: 0x000223B8
		// (set) Token: 0x06001FD8 RID: 8152 RVA: 0x000241D4 File Offset: 0x000223D4
		public AnimatorControllerParameterType type
		{
			get
			{
				return this.m_Type;
			}
			set
			{
				this.m_Type = value;
			}
		}

		// Token: 0x170007F0 RID: 2032
		// (get) Token: 0x06001FD9 RID: 8153 RVA: 0x000241E0 File Offset: 0x000223E0
		// (set) Token: 0x06001FDA RID: 8154 RVA: 0x000241FC File Offset: 0x000223FC
		public float defaultFloat
		{
			get
			{
				return this.m_DefaultFloat;
			}
			set
			{
				this.m_DefaultFloat = value;
			}
		}

		// Token: 0x170007F1 RID: 2033
		// (get) Token: 0x06001FDB RID: 8155 RVA: 0x00024208 File Offset: 0x00022408
		// (set) Token: 0x06001FDC RID: 8156 RVA: 0x00024224 File Offset: 0x00022424
		public int defaultInt
		{
			get
			{
				return this.m_DefaultInt;
			}
			set
			{
				this.m_DefaultInt = value;
			}
		}

		// Token: 0x170007F2 RID: 2034
		// (get) Token: 0x06001FDD RID: 8157 RVA: 0x00024230 File Offset: 0x00022430
		// (set) Token: 0x06001FDE RID: 8158 RVA: 0x0002424C File Offset: 0x0002244C
		public bool defaultBool
		{
			get
			{
				return this.m_DefaultBool;
			}
			set
			{
				this.m_DefaultBool = value;
			}
		}

		// Token: 0x06001FDF RID: 8159 RVA: 0x00024258 File Offset: 0x00022458
		public override bool Equals(object o)
		{
			AnimatorControllerParameter animatorControllerParameter = o as AnimatorControllerParameter;
			return animatorControllerParameter != null && this.m_Name == animatorControllerParameter.m_Name && this.m_Type == animatorControllerParameter.m_Type && this.m_DefaultFloat == animatorControllerParameter.m_DefaultFloat && this.m_DefaultInt == animatorControllerParameter.m_DefaultInt && this.m_DefaultBool == animatorControllerParameter.m_DefaultBool;
		}

		// Token: 0x06001FE0 RID: 8160 RVA: 0x000242D4 File Offset: 0x000224D4
		public override int GetHashCode()
		{
			return this.name.GetHashCode();
		}

		// Token: 0x04000507 RID: 1287
		internal string m_Name = "";

		// Token: 0x04000508 RID: 1288
		internal AnimatorControllerParameterType m_Type;

		// Token: 0x04000509 RID: 1289
		internal float m_DefaultFloat;

		// Token: 0x0400050A RID: 1290
		internal int m_DefaultInt;

		// Token: 0x0400050B RID: 1291
		internal bool m_DefaultBool;
	}
}
