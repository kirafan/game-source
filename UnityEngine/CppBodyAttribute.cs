﻿using System;

namespace UnityEngine
{
	// Token: 0x020002D4 RID: 724
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = false)]
	internal class CppBodyAttribute : Attribute
	{
		// Token: 0x06002C84 RID: 11396 RVA: 0x0004637C File Offset: 0x0004457C
		public CppBodyAttribute(string body)
		{
		}
	}
}
