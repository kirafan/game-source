﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000002 RID: 2
	public sealed class AndroidInput
	{
		// Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		private AndroidInput()
		{
		}

		// Token: 0x06000002 RID: 2 RVA: 0x0000205C File Offset: 0x0000025C
		public static Touch GetSecondaryTouch(int index)
		{
			Touch result;
			AndroidInput.INTERNAL_CALL_GetSecondaryTouch(index, out result);
			return result;
		}

		// Token: 0x06000003 RID: 3
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetSecondaryTouch(int index, out Touch value);

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000004 RID: 4
		public static extern int touchCountSecondary { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000005 RID: 5
		public static extern bool secondaryTouchEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000006 RID: 6
		public static extern int secondaryTouchWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000007 RID: 7
		public static extern int secondaryTouchHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
