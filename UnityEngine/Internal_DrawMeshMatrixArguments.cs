﻿using System;

namespace UnityEngine
{
	// Token: 0x02000052 RID: 82
	internal struct Internal_DrawMeshMatrixArguments
	{
		// Token: 0x0400007C RID: 124
		public int layer;

		// Token: 0x0400007D RID: 125
		public int submeshIndex;

		// Token: 0x0400007E RID: 126
		public Matrix4x4 matrix;

		// Token: 0x0400007F RID: 127
		public int castShadows;

		// Token: 0x04000080 RID: 128
		public int receiveShadows;

		// Token: 0x04000081 RID: 129
		public int reflectionProbeAnchorInstanceID;

		// Token: 0x04000082 RID: 130
		public bool useLightProbes;
	}
}
