﻿using System;

namespace UnityEngine
{
	// Token: 0x0200035C RID: 860
	[AttributeUsage(AttributeTargets.Field, Inherited = true, AllowMultiple = false)]
	public sealed class RangeAttribute : PropertyAttribute
	{
		// Token: 0x06002DDE RID: 11742 RVA: 0x00048E7C File Offset: 0x0004707C
		public RangeAttribute(float min, float max)
		{
			this.min = min;
			this.max = max;
		}

		// Token: 0x04000D2B RID: 3371
		public readonly float min;

		// Token: 0x04000D2C RID: 3372
		public readonly float max;
	}
}
