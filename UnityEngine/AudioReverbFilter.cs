﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001AE RID: 430
	public sealed class AudioReverbFilter : Behaviour
	{
		// Token: 0x1700075C RID: 1884
		// (get) Token: 0x06001DC2 RID: 7618
		// (set) Token: 0x06001DC3 RID: 7619
		public extern AudioReverbPreset reverbPreset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700075D RID: 1885
		// (get) Token: 0x06001DC4 RID: 7620
		// (set) Token: 0x06001DC5 RID: 7621
		public extern float dryLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700075E RID: 1886
		// (get) Token: 0x06001DC6 RID: 7622
		// (set) Token: 0x06001DC7 RID: 7623
		public extern float room { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700075F RID: 1887
		// (get) Token: 0x06001DC8 RID: 7624
		// (set) Token: 0x06001DC9 RID: 7625
		public extern float roomHF { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000760 RID: 1888
		// (get) Token: 0x06001DCA RID: 7626
		// (set) Token: 0x06001DCB RID: 7627
		public extern float roomRolloff { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000761 RID: 1889
		// (get) Token: 0x06001DCC RID: 7628
		// (set) Token: 0x06001DCD RID: 7629
		public extern float decayTime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000762 RID: 1890
		// (get) Token: 0x06001DCE RID: 7630
		// (set) Token: 0x06001DCF RID: 7631
		public extern float decayHFRatio { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000763 RID: 1891
		// (get) Token: 0x06001DD0 RID: 7632
		// (set) Token: 0x06001DD1 RID: 7633
		public extern float reflectionsLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000764 RID: 1892
		// (get) Token: 0x06001DD2 RID: 7634
		// (set) Token: 0x06001DD3 RID: 7635
		public extern float reflectionsDelay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000765 RID: 1893
		// (get) Token: 0x06001DD4 RID: 7636
		// (set) Token: 0x06001DD5 RID: 7637
		public extern float reverbLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000766 RID: 1894
		// (get) Token: 0x06001DD6 RID: 7638
		// (set) Token: 0x06001DD7 RID: 7639
		public extern float reverbDelay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000767 RID: 1895
		// (get) Token: 0x06001DD8 RID: 7640
		// (set) Token: 0x06001DD9 RID: 7641
		public extern float diffusion { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000768 RID: 1896
		// (get) Token: 0x06001DDA RID: 7642
		// (set) Token: 0x06001DDB RID: 7643
		public extern float density { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000769 RID: 1897
		// (get) Token: 0x06001DDC RID: 7644
		// (set) Token: 0x06001DDD RID: 7645
		public extern float hfReference { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700076A RID: 1898
		// (get) Token: 0x06001DDE RID: 7646
		// (set) Token: 0x06001DDF RID: 7647
		public extern float roomLF { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700076B RID: 1899
		// (get) Token: 0x06001DE0 RID: 7648
		// (set) Token: 0x06001DE1 RID: 7649
		public extern float lfReference { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
