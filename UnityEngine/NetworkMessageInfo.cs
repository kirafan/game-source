﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200009F RID: 159
	[RequiredByNativeCode]
	public struct NetworkMessageInfo
	{
		// Token: 0x1700028D RID: 653
		// (get) Token: 0x06000B22 RID: 2850 RVA: 0x0000FAC4 File Offset: 0x0000DCC4
		public double timestamp
		{
			get
			{
				return this.m_TimeStamp;
			}
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x06000B23 RID: 2851 RVA: 0x0000FAE0 File Offset: 0x0000DCE0
		public NetworkPlayer sender
		{
			get
			{
				return this.m_Sender;
			}
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06000B24 RID: 2852 RVA: 0x0000FAFC File Offset: 0x0000DCFC
		public NetworkView networkView
		{
			get
			{
				NetworkView result;
				if (this.m_ViewID == NetworkViewID.unassigned)
				{
					Debug.LogError("No NetworkView is assigned to this NetworkMessageInfo object. Note that this is expected in OnNetworkInstantiate().");
					result = this.NullNetworkView();
				}
				else
				{
					result = NetworkView.Find(this.m_ViewID);
				}
				return result;
			}
		}

		// Token: 0x06000B25 RID: 2853
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern NetworkView NullNetworkView();

		// Token: 0x04000172 RID: 370
		private double m_TimeStamp;

		// Token: 0x04000173 RID: 371
		private NetworkPlayer m_Sender;

		// Token: 0x04000174 RID: 372
		private NetworkViewID m_ViewID;
	}
}
