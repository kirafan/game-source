﻿using System;

namespace UnityEngine
{
	// Token: 0x020002A7 RID: 679
	internal struct InternalEmitParticleArguments
	{
		// Token: 0x04000A1E RID: 2590
		public Vector3 pos;

		// Token: 0x04000A1F RID: 2591
		public Vector3 velocity;

		// Token: 0x04000A20 RID: 2592
		public float size;

		// Token: 0x04000A21 RID: 2593
		public float energy;

		// Token: 0x04000A22 RID: 2594
		public Color color;

		// Token: 0x04000A23 RID: 2595
		public float rotation;

		// Token: 0x04000A24 RID: 2596
		public float angularVelocity;
	}
}
