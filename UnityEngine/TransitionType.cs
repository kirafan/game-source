﻿using System;

namespace UnityEngine
{
	// Token: 0x020001C9 RID: 457
	internal enum TransitionType
	{
		// Token: 0x040004E5 RID: 1253
		Normal = 1,
		// Token: 0x040004E6 RID: 1254
		Entry,
		// Token: 0x040004E7 RID: 1255
		Exit = 4
	}
}
