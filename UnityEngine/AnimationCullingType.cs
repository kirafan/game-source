﻿using System;

namespace UnityEngine
{
	// Token: 0x020001C1 RID: 449
	public enum AnimationCullingType
	{
		// Token: 0x040004C8 RID: 1224
		AlwaysAnimate,
		// Token: 0x040004C9 RID: 1225
		BasedOnRenderers,
		// Token: 0x040004CA RID: 1226
		[Obsolete("Enum member AnimatorCullingMode.BasedOnClipBounds has been deprecated. Use AnimationCullingType.AlwaysAnimate or AnimationCullingType.BasedOnRenderers instead")]
		BasedOnClipBounds,
		// Token: 0x040004CB RID: 1227
		[Obsolete("Enum member AnimatorCullingMode.BasedOnUserBounds has been deprecated. Use AnimationCullingType.AlwaysAnimate or AnimationCullingType.BasedOnRenderers instead")]
		BasedOnUserBounds
	}
}
