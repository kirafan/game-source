﻿using System;

namespace UnityEngine
{
	// Token: 0x020002C6 RID: 710
	public enum Space
	{
		// Token: 0x04000A51 RID: 2641
		World,
		// Token: 0x04000A52 RID: 2642
		Self
	}
}
