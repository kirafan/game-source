﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000099 RID: 153
	public sealed class NetworkView : Behaviour
	{
		// Token: 0x06000A70 RID: 2672
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_RPC(NetworkView view, string name, RPCMode mode, object[] args);

		// Token: 0x06000A71 RID: 2673 RVA: 0x0000F2F8 File Offset: 0x0000D4F8
		private static void Internal_RPC_Target(NetworkView view, string name, NetworkPlayer target, object[] args)
		{
			NetworkView.INTERNAL_CALL_Internal_RPC_Target(view, name, ref target, args);
		}

		// Token: 0x06000A72 RID: 2674
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_RPC_Target(NetworkView view, string name, ref NetworkPlayer target, object[] args);

		// Token: 0x06000A73 RID: 2675 RVA: 0x0000F308 File Offset: 0x0000D508
		[Obsolete("NetworkView RPC functions are deprecated. Refer to the new Multiplayer Networking system.")]
		public void RPC(string name, RPCMode mode, params object[] args)
		{
			NetworkView.Internal_RPC(this, name, mode, args);
		}

		// Token: 0x06000A74 RID: 2676 RVA: 0x0000F314 File Offset: 0x0000D514
		[Obsolete("NetworkView RPC functions are deprecated. Refer to the new Multiplayer Networking system.")]
		public void RPC(string name, NetworkPlayer target, params object[] args)
		{
			NetworkView.Internal_RPC_Target(this, name, target, args);
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x06000A75 RID: 2677
		// (set) Token: 0x06000A76 RID: 2678
		public extern Component observed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x06000A77 RID: 2679
		// (set) Token: 0x06000A78 RID: 2680
		public extern NetworkStateSynchronization stateSynchronization { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000A79 RID: 2681
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetViewID(out NetworkViewID viewID);

		// Token: 0x06000A7A RID: 2682 RVA: 0x0000F320 File Offset: 0x0000D520
		private void Internal_SetViewID(NetworkViewID viewID)
		{
			NetworkView.INTERNAL_CALL_Internal_SetViewID(this, ref viewID);
		}

		// Token: 0x06000A7B RID: 2683
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_SetViewID(NetworkView self, ref NetworkViewID viewID);

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x06000A7C RID: 2684 RVA: 0x0000F32C File Offset: 0x0000D52C
		// (set) Token: 0x06000A7D RID: 2685 RVA: 0x0000F34C File Offset: 0x0000D54C
		public NetworkViewID viewID
		{
			get
			{
				NetworkViewID result;
				this.Internal_GetViewID(out result);
				return result;
			}
			set
			{
				this.Internal_SetViewID(value);
			}
		}

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x06000A7E RID: 2686
		// (set) Token: 0x06000A7F RID: 2687
		public extern int group { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x06000A80 RID: 2688 RVA: 0x0000F358 File Offset: 0x0000D558
		public bool isMine
		{
			get
			{
				return this.viewID.isMine;
			}
		}

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x06000A81 RID: 2689 RVA: 0x0000F37C File Offset: 0x0000D57C
		public NetworkPlayer owner
		{
			get
			{
				return this.viewID.owner;
			}
		}

		// Token: 0x06000A82 RID: 2690 RVA: 0x0000F3A0 File Offset: 0x0000D5A0
		public bool SetScope(NetworkPlayer player, bool relevancy)
		{
			return NetworkView.INTERNAL_CALL_SetScope(this, ref player, relevancy);
		}

		// Token: 0x06000A83 RID: 2691
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_SetScope(NetworkView self, ref NetworkPlayer player, bool relevancy);

		// Token: 0x06000A84 RID: 2692 RVA: 0x0000F3C0 File Offset: 0x0000D5C0
		public static NetworkView Find(NetworkViewID viewID)
		{
			return NetworkView.INTERNAL_CALL_Find(ref viewID);
		}

		// Token: 0x06000A85 RID: 2693
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern NetworkView INTERNAL_CALL_Find(ref NetworkViewID viewID);
	}
}
