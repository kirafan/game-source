﻿using System;

namespace UnityEngine
{
	// Token: 0x0200016D RID: 365
	public struct JointMotor2D
	{
		// Token: 0x1700062C RID: 1580
		// (get) Token: 0x06001AA8 RID: 6824 RVA: 0x000214D4 File Offset: 0x0001F6D4
		// (set) Token: 0x06001AA9 RID: 6825 RVA: 0x000214F0 File Offset: 0x0001F6F0
		public float motorSpeed
		{
			get
			{
				return this.m_MotorSpeed;
			}
			set
			{
				this.m_MotorSpeed = value;
			}
		}

		// Token: 0x1700062D RID: 1581
		// (get) Token: 0x06001AAA RID: 6826 RVA: 0x000214FC File Offset: 0x0001F6FC
		// (set) Token: 0x06001AAB RID: 6827 RVA: 0x00021518 File Offset: 0x0001F718
		public float maxMotorTorque
		{
			get
			{
				return this.m_MaximumMotorTorque;
			}
			set
			{
				this.m_MaximumMotorTorque = value;
			}
		}

		// Token: 0x040003FD RID: 1021
		private float m_MotorSpeed;

		// Token: 0x040003FE RID: 1022
		private float m_MaximumMotorTorque;
	}
}
