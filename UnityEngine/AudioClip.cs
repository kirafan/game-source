﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200019E RID: 414
	public sealed class AudioClip : Object
	{
		// Token: 0x1700070E RID: 1806
		// (get) Token: 0x06001CEF RID: 7407
		public extern float length { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700070F RID: 1807
		// (get) Token: 0x06001CF0 RID: 7408
		public extern int samples { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000710 RID: 1808
		// (get) Token: 0x06001CF1 RID: 7409
		public extern int channels { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000711 RID: 1809
		// (get) Token: 0x06001CF2 RID: 7410
		public extern int frequency { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000712 RID: 1810
		// (get) Token: 0x06001CF3 RID: 7411
		[Obsolete("Use AudioClip.loadState instead to get more detailed information about the loading process.")]
		public extern bool isReadyToPlay { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000713 RID: 1811
		// (get) Token: 0x06001CF4 RID: 7412
		public extern AudioClipLoadType loadType { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001CF5 RID: 7413
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool LoadAudioData();

		// Token: 0x06001CF6 RID: 7414
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool UnloadAudioData();

		// Token: 0x17000714 RID: 1812
		// (get) Token: 0x06001CF7 RID: 7415
		public extern bool preloadAudioData { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000715 RID: 1813
		// (get) Token: 0x06001CF8 RID: 7416
		public extern AudioDataLoadState loadState { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000716 RID: 1814
		// (get) Token: 0x06001CF9 RID: 7417
		public extern bool loadInBackground { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001CFA RID: 7418
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetData(float[] data, int offsetSamples);

		// Token: 0x06001CFB RID: 7419
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SetData(float[] data, int offsetSamples);

		// Token: 0x06001CFC RID: 7420 RVA: 0x00022390 File Offset: 0x00020590
		[Obsolete("The _3D argument of AudioClip is deprecated. Use the spatialBlend property of AudioSource instead to morph between 2D and 3D playback.")]
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool _3D, bool stream)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream);
		}

		// Token: 0x06001CFD RID: 7421 RVA: 0x000223B0 File Offset: 0x000205B0
		[Obsolete("The _3D argument of AudioClip is deprecated. Use the spatialBlend property of AudioSource instead to morph between 2D and 3D playback.")]
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool _3D, bool stream, AudioClip.PCMReaderCallback pcmreadercallback)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream, pcmreadercallback, null);
		}

		// Token: 0x06001CFE RID: 7422 RVA: 0x000223D4 File Offset: 0x000205D4
		[Obsolete("The _3D argument of AudioClip is deprecated. Use the spatialBlend property of AudioSource instead to morph between 2D and 3D playback.")]
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool _3D, bool stream, AudioClip.PCMReaderCallback pcmreadercallback, AudioClip.PCMSetPositionCallback pcmsetpositioncallback)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream, pcmreadercallback, pcmsetpositioncallback);
		}

		// Token: 0x06001CFF RID: 7423 RVA: 0x000223F8 File Offset: 0x000205F8
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool stream)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream, null, null);
		}

		// Token: 0x06001D00 RID: 7424 RVA: 0x0002241C File Offset: 0x0002061C
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool stream, AudioClip.PCMReaderCallback pcmreadercallback)
		{
			return AudioClip.Create(name, lengthSamples, channels, frequency, stream, pcmreadercallback, null);
		}

		// Token: 0x06001D01 RID: 7425 RVA: 0x00022444 File Offset: 0x00020644
		public static AudioClip Create(string name, int lengthSamples, int channels, int frequency, bool stream, AudioClip.PCMReaderCallback pcmreadercallback, AudioClip.PCMSetPositionCallback pcmsetpositioncallback)
		{
			if (name == null)
			{
				throw new NullReferenceException();
			}
			if (lengthSamples <= 0)
			{
				throw new ArgumentException("Length of created clip must be larger than 0");
			}
			if (channels <= 0)
			{
				throw new ArgumentException("Number of channels in created clip must be greater than 0");
			}
			if (frequency <= 0)
			{
				throw new ArgumentException("Frequency in created clip must be greater than 0");
			}
			AudioClip audioClip = AudioClip.Construct_Internal();
			if (pcmreadercallback != null)
			{
				audioClip.m_PCMReaderCallback += pcmreadercallback;
			}
			if (pcmsetpositioncallback != null)
			{
				audioClip.m_PCMSetPositionCallback += pcmsetpositioncallback;
			}
			audioClip.Init_Internal(name, lengthSamples, channels, frequency, stream);
			return audioClip;
		}

		// Token: 0x14000009 RID: 9
		// (add) Token: 0x06001D02 RID: 7426 RVA: 0x000224CC File Offset: 0x000206CC
		// (remove) Token: 0x06001D03 RID: 7427 RVA: 0x00022504 File Offset: 0x00020704
		private event AudioClip.PCMReaderCallback m_PCMReaderCallback = null;

		// Token: 0x1400000A RID: 10
		// (add) Token: 0x06001D04 RID: 7428 RVA: 0x0002253C File Offset: 0x0002073C
		// (remove) Token: 0x06001D05 RID: 7429 RVA: 0x00022574 File Offset: 0x00020774
		private event AudioClip.PCMSetPositionCallback m_PCMSetPositionCallback = null;

		// Token: 0x06001D06 RID: 7430 RVA: 0x000225AC File Offset: 0x000207AC
		[RequiredByNativeCode]
		private void InvokePCMReaderCallback_Internal(float[] data)
		{
			if (this.m_PCMReaderCallback != null)
			{
				this.m_PCMReaderCallback(data);
			}
		}

		// Token: 0x06001D07 RID: 7431 RVA: 0x000225C8 File Offset: 0x000207C8
		[RequiredByNativeCode]
		private void InvokePCMSetPositionCallback_Internal(int position)
		{
			if (this.m_PCMSetPositionCallback != null)
			{
				this.m_PCMSetPositionCallback(position);
			}
		}

		// Token: 0x06001D08 RID: 7432
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AudioClip Construct_Internal();

		// Token: 0x06001D09 RID: 7433
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Init_Internal(string name, int lengthSamples, int channels, int frequency, bool stream);

		// Token: 0x0200019F RID: 415
		// (Invoke) Token: 0x06001D0B RID: 7435
		public delegate void PCMReaderCallback(float[] data);

		// Token: 0x020001A0 RID: 416
		// (Invoke) Token: 0x06001D0F RID: 7439
		public delegate void PCMSetPositionCallback(int position);
	}
}
