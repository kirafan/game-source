﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x02000144 RID: 324
	public sealed class Rigidbody : Component
	{
		// Token: 0x17000539 RID: 1337
		// (get) Token: 0x06001708 RID: 5896 RVA: 0x0001DD08 File Offset: 0x0001BF08
		// (set) Token: 0x06001709 RID: 5897 RVA: 0x0001DD28 File Offset: 0x0001BF28
		public Vector3 velocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_velocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_velocity(ref value);
			}
		}

		// Token: 0x0600170A RID: 5898
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_velocity(out Vector3 value);

		// Token: 0x0600170B RID: 5899
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_velocity(ref Vector3 value);

		// Token: 0x1700053A RID: 1338
		// (get) Token: 0x0600170C RID: 5900 RVA: 0x0001DD34 File Offset: 0x0001BF34
		// (set) Token: 0x0600170D RID: 5901 RVA: 0x0001DD54 File Offset: 0x0001BF54
		public Vector3 angularVelocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_angularVelocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_angularVelocity(ref value);
			}
		}

		// Token: 0x0600170E RID: 5902
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_angularVelocity(out Vector3 value);

		// Token: 0x0600170F RID: 5903
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_angularVelocity(ref Vector3 value);

		// Token: 0x1700053B RID: 1339
		// (get) Token: 0x06001710 RID: 5904
		// (set) Token: 0x06001711 RID: 5905
		public extern float drag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700053C RID: 1340
		// (get) Token: 0x06001712 RID: 5906
		// (set) Token: 0x06001713 RID: 5907
		public extern float angularDrag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700053D RID: 1341
		// (get) Token: 0x06001714 RID: 5908
		// (set) Token: 0x06001715 RID: 5909
		public extern float mass { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001716 RID: 5910 RVA: 0x0001DD60 File Offset: 0x0001BF60
		public void SetDensity(float density)
		{
			Rigidbody.INTERNAL_CALL_SetDensity(this, density);
		}

		// Token: 0x06001717 RID: 5911
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetDensity(Rigidbody self, float density);

		// Token: 0x1700053E RID: 1342
		// (get) Token: 0x06001718 RID: 5912
		// (set) Token: 0x06001719 RID: 5913
		public extern bool useGravity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700053F RID: 1343
		// (get) Token: 0x0600171A RID: 5914
		// (set) Token: 0x0600171B RID: 5915
		public extern float maxDepenetrationVelocity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000540 RID: 1344
		// (get) Token: 0x0600171C RID: 5916
		// (set) Token: 0x0600171D RID: 5917
		public extern bool isKinematic { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000541 RID: 1345
		// (get) Token: 0x0600171E RID: 5918
		// (set) Token: 0x0600171F RID: 5919
		public extern bool freezeRotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000542 RID: 1346
		// (get) Token: 0x06001720 RID: 5920
		// (set) Token: 0x06001721 RID: 5921
		public extern RigidbodyConstraints constraints { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000543 RID: 1347
		// (get) Token: 0x06001722 RID: 5922
		// (set) Token: 0x06001723 RID: 5923
		public extern CollisionDetectionMode collisionDetectionMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001724 RID: 5924 RVA: 0x0001DD6C File Offset: 0x0001BF6C
		public void AddForce(Vector3 force, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			Rigidbody.INTERNAL_CALL_AddForce(this, ref force, mode);
		}

		// Token: 0x06001725 RID: 5925 RVA: 0x0001DD78 File Offset: 0x0001BF78
		[ExcludeFromDocs]
		public void AddForce(Vector3 force)
		{
			ForceMode mode = ForceMode.Force;
			Rigidbody.INTERNAL_CALL_AddForce(this, ref force, mode);
		}

		// Token: 0x06001726 RID: 5926
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddForce(Rigidbody self, ref Vector3 force, ForceMode mode);

		// Token: 0x06001727 RID: 5927 RVA: 0x0001DD94 File Offset: 0x0001BF94
		[ExcludeFromDocs]
		public void AddForce(float x, float y, float z)
		{
			ForceMode mode = ForceMode.Force;
			this.AddForce(x, y, z, mode);
		}

		// Token: 0x06001728 RID: 5928 RVA: 0x0001DDB0 File Offset: 0x0001BFB0
		public void AddForce(float x, float y, float z, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			this.AddForce(new Vector3(x, y, z), mode);
		}

		// Token: 0x06001729 RID: 5929 RVA: 0x0001DDC4 File Offset: 0x0001BFC4
		public void AddRelativeForce(Vector3 force, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			Rigidbody.INTERNAL_CALL_AddRelativeForce(this, ref force, mode);
		}

		// Token: 0x0600172A RID: 5930 RVA: 0x0001DDD0 File Offset: 0x0001BFD0
		[ExcludeFromDocs]
		public void AddRelativeForce(Vector3 force)
		{
			ForceMode mode = ForceMode.Force;
			Rigidbody.INTERNAL_CALL_AddRelativeForce(this, ref force, mode);
		}

		// Token: 0x0600172B RID: 5931
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddRelativeForce(Rigidbody self, ref Vector3 force, ForceMode mode);

		// Token: 0x0600172C RID: 5932 RVA: 0x0001DDEC File Offset: 0x0001BFEC
		[ExcludeFromDocs]
		public void AddRelativeForce(float x, float y, float z)
		{
			ForceMode mode = ForceMode.Force;
			this.AddRelativeForce(x, y, z, mode);
		}

		// Token: 0x0600172D RID: 5933 RVA: 0x0001DE08 File Offset: 0x0001C008
		public void AddRelativeForce(float x, float y, float z, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			this.AddRelativeForce(new Vector3(x, y, z), mode);
		}

		// Token: 0x0600172E RID: 5934 RVA: 0x0001DE1C File Offset: 0x0001C01C
		public void AddTorque(Vector3 torque, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			Rigidbody.INTERNAL_CALL_AddTorque(this, ref torque, mode);
		}

		// Token: 0x0600172F RID: 5935 RVA: 0x0001DE28 File Offset: 0x0001C028
		[ExcludeFromDocs]
		public void AddTorque(Vector3 torque)
		{
			ForceMode mode = ForceMode.Force;
			Rigidbody.INTERNAL_CALL_AddTorque(this, ref torque, mode);
		}

		// Token: 0x06001730 RID: 5936
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddTorque(Rigidbody self, ref Vector3 torque, ForceMode mode);

		// Token: 0x06001731 RID: 5937 RVA: 0x0001DE44 File Offset: 0x0001C044
		[ExcludeFromDocs]
		public void AddTorque(float x, float y, float z)
		{
			ForceMode mode = ForceMode.Force;
			this.AddTorque(x, y, z, mode);
		}

		// Token: 0x06001732 RID: 5938 RVA: 0x0001DE60 File Offset: 0x0001C060
		public void AddTorque(float x, float y, float z, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			this.AddTorque(new Vector3(x, y, z), mode);
		}

		// Token: 0x06001733 RID: 5939 RVA: 0x0001DE74 File Offset: 0x0001C074
		public void AddRelativeTorque(Vector3 torque, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			Rigidbody.INTERNAL_CALL_AddRelativeTorque(this, ref torque, mode);
		}

		// Token: 0x06001734 RID: 5940 RVA: 0x0001DE80 File Offset: 0x0001C080
		[ExcludeFromDocs]
		public void AddRelativeTorque(Vector3 torque)
		{
			ForceMode mode = ForceMode.Force;
			Rigidbody.INTERNAL_CALL_AddRelativeTorque(this, ref torque, mode);
		}

		// Token: 0x06001735 RID: 5941
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddRelativeTorque(Rigidbody self, ref Vector3 torque, ForceMode mode);

		// Token: 0x06001736 RID: 5942 RVA: 0x0001DE9C File Offset: 0x0001C09C
		[ExcludeFromDocs]
		public void AddRelativeTorque(float x, float y, float z)
		{
			ForceMode mode = ForceMode.Force;
			this.AddRelativeTorque(x, y, z, mode);
		}

		// Token: 0x06001737 RID: 5943 RVA: 0x0001DEB8 File Offset: 0x0001C0B8
		public void AddRelativeTorque(float x, float y, float z, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			this.AddRelativeTorque(new Vector3(x, y, z), mode);
		}

		// Token: 0x06001738 RID: 5944 RVA: 0x0001DECC File Offset: 0x0001C0CC
		public void AddForceAtPosition(Vector3 force, Vector3 position, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			Rigidbody.INTERNAL_CALL_AddForceAtPosition(this, ref force, ref position, mode);
		}

		// Token: 0x06001739 RID: 5945 RVA: 0x0001DEDC File Offset: 0x0001C0DC
		[ExcludeFromDocs]
		public void AddForceAtPosition(Vector3 force, Vector3 position)
		{
			ForceMode mode = ForceMode.Force;
			Rigidbody.INTERNAL_CALL_AddForceAtPosition(this, ref force, ref position, mode);
		}

		// Token: 0x0600173A RID: 5946
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddForceAtPosition(Rigidbody self, ref Vector3 force, ref Vector3 position, ForceMode mode);

		// Token: 0x0600173B RID: 5947 RVA: 0x0001DEF8 File Offset: 0x0001C0F8
		public void AddExplosionForce(float explosionForce, Vector3 explosionPosition, float explosionRadius, [DefaultValue("0.0F")] float upwardsModifier, [DefaultValue("ForceMode.Force")] ForceMode mode)
		{
			Rigidbody.INTERNAL_CALL_AddExplosionForce(this, explosionForce, ref explosionPosition, explosionRadius, upwardsModifier, mode);
		}

		// Token: 0x0600173C RID: 5948 RVA: 0x0001DF0C File Offset: 0x0001C10C
		[ExcludeFromDocs]
		public void AddExplosionForce(float explosionForce, Vector3 explosionPosition, float explosionRadius, float upwardsModifier)
		{
			ForceMode mode = ForceMode.Force;
			Rigidbody.INTERNAL_CALL_AddExplosionForce(this, explosionForce, ref explosionPosition, explosionRadius, upwardsModifier, mode);
		}

		// Token: 0x0600173D RID: 5949 RVA: 0x0001DF2C File Offset: 0x0001C12C
		[ExcludeFromDocs]
		public void AddExplosionForce(float explosionForce, Vector3 explosionPosition, float explosionRadius)
		{
			ForceMode mode = ForceMode.Force;
			float upwardsModifier = 0f;
			Rigidbody.INTERNAL_CALL_AddExplosionForce(this, explosionForce, ref explosionPosition, explosionRadius, upwardsModifier, mode);
		}

		// Token: 0x0600173E RID: 5950
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddExplosionForce(Rigidbody self, float explosionForce, ref Vector3 explosionPosition, float explosionRadius, float upwardsModifier, ForceMode mode);

		// Token: 0x0600173F RID: 5951 RVA: 0x0001DF50 File Offset: 0x0001C150
		public Vector3 ClosestPointOnBounds(Vector3 position)
		{
			Vector3 result;
			Rigidbody.INTERNAL_CALL_ClosestPointOnBounds(this, ref position, out result);
			return result;
		}

		// Token: 0x06001740 RID: 5952
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ClosestPointOnBounds(Rigidbody self, ref Vector3 position, out Vector3 value);

		// Token: 0x06001741 RID: 5953 RVA: 0x0001DF70 File Offset: 0x0001C170
		public Vector3 GetRelativePointVelocity(Vector3 relativePoint)
		{
			Vector3 result;
			Rigidbody.INTERNAL_CALL_GetRelativePointVelocity(this, ref relativePoint, out result);
			return result;
		}

		// Token: 0x06001742 RID: 5954
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetRelativePointVelocity(Rigidbody self, ref Vector3 relativePoint, out Vector3 value);

		// Token: 0x06001743 RID: 5955 RVA: 0x0001DF90 File Offset: 0x0001C190
		public Vector3 GetPointVelocity(Vector3 worldPoint)
		{
			Vector3 result;
			Rigidbody.INTERNAL_CALL_GetPointVelocity(this, ref worldPoint, out result);
			return result;
		}

		// Token: 0x06001744 RID: 5956
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPointVelocity(Rigidbody self, ref Vector3 worldPoint, out Vector3 value);

		// Token: 0x17000544 RID: 1348
		// (get) Token: 0x06001745 RID: 5957 RVA: 0x0001DFB0 File Offset: 0x0001C1B0
		// (set) Token: 0x06001746 RID: 5958 RVA: 0x0001DFD0 File Offset: 0x0001C1D0
		public Vector3 centerOfMass
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_centerOfMass(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_centerOfMass(ref value);
			}
		}

		// Token: 0x06001747 RID: 5959
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_centerOfMass(out Vector3 value);

		// Token: 0x06001748 RID: 5960
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_centerOfMass(ref Vector3 value);

		// Token: 0x17000545 RID: 1349
		// (get) Token: 0x06001749 RID: 5961 RVA: 0x0001DFDC File Offset: 0x0001C1DC
		public Vector3 worldCenterOfMass
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_worldCenterOfMass(out result);
				return result;
			}
		}

		// Token: 0x0600174A RID: 5962
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_worldCenterOfMass(out Vector3 value);

		// Token: 0x17000546 RID: 1350
		// (get) Token: 0x0600174B RID: 5963 RVA: 0x0001DFFC File Offset: 0x0001C1FC
		// (set) Token: 0x0600174C RID: 5964 RVA: 0x0001E01C File Offset: 0x0001C21C
		public Quaternion inertiaTensorRotation
		{
			get
			{
				Quaternion result;
				this.INTERNAL_get_inertiaTensorRotation(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_inertiaTensorRotation(ref value);
			}
		}

		// Token: 0x0600174D RID: 5965
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_inertiaTensorRotation(out Quaternion value);

		// Token: 0x0600174E RID: 5966
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_inertiaTensorRotation(ref Quaternion value);

		// Token: 0x17000547 RID: 1351
		// (get) Token: 0x0600174F RID: 5967 RVA: 0x0001E028 File Offset: 0x0001C228
		// (set) Token: 0x06001750 RID: 5968 RVA: 0x0001E048 File Offset: 0x0001C248
		public Vector3 inertiaTensor
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_inertiaTensor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_inertiaTensor(ref value);
			}
		}

		// Token: 0x06001751 RID: 5969
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_inertiaTensor(out Vector3 value);

		// Token: 0x06001752 RID: 5970
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_inertiaTensor(ref Vector3 value);

		// Token: 0x17000548 RID: 1352
		// (get) Token: 0x06001753 RID: 5971
		// (set) Token: 0x06001754 RID: 5972
		public extern bool detectCollisions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000549 RID: 1353
		// (get) Token: 0x06001755 RID: 5973
		// (set) Token: 0x06001756 RID: 5974
		[Obsolete("Cone friction is no longer supported.")]
		public extern bool useConeFriction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700054A RID: 1354
		// (get) Token: 0x06001757 RID: 5975 RVA: 0x0001E054 File Offset: 0x0001C254
		// (set) Token: 0x06001758 RID: 5976 RVA: 0x0001E074 File Offset: 0x0001C274
		public Vector3 position
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_position(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_position(ref value);
			}
		}

		// Token: 0x06001759 RID: 5977
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_position(out Vector3 value);

		// Token: 0x0600175A RID: 5978
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_position(ref Vector3 value);

		// Token: 0x1700054B RID: 1355
		// (get) Token: 0x0600175B RID: 5979 RVA: 0x0001E080 File Offset: 0x0001C280
		// (set) Token: 0x0600175C RID: 5980 RVA: 0x0001E0A0 File Offset: 0x0001C2A0
		public Quaternion rotation
		{
			get
			{
				Quaternion result;
				this.INTERNAL_get_rotation(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_rotation(ref value);
			}
		}

		// Token: 0x0600175D RID: 5981
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rotation(out Quaternion value);

		// Token: 0x0600175E RID: 5982
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_rotation(ref Quaternion value);

		// Token: 0x0600175F RID: 5983 RVA: 0x0001E0AC File Offset: 0x0001C2AC
		public void MovePosition(Vector3 position)
		{
			Rigidbody.INTERNAL_CALL_MovePosition(this, ref position);
		}

		// Token: 0x06001760 RID: 5984
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_MovePosition(Rigidbody self, ref Vector3 position);

		// Token: 0x06001761 RID: 5985 RVA: 0x0001E0B8 File Offset: 0x0001C2B8
		public void MoveRotation(Quaternion rot)
		{
			Rigidbody.INTERNAL_CALL_MoveRotation(this, ref rot);
		}

		// Token: 0x06001762 RID: 5986
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_MoveRotation(Rigidbody self, ref Quaternion rot);

		// Token: 0x1700054C RID: 1356
		// (get) Token: 0x06001763 RID: 5987
		// (set) Token: 0x06001764 RID: 5988
		public extern RigidbodyInterpolation interpolation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001765 RID: 5989 RVA: 0x0001E0C4 File Offset: 0x0001C2C4
		public void Sleep()
		{
			Rigidbody.INTERNAL_CALL_Sleep(this);
		}

		// Token: 0x06001766 RID: 5990
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Sleep(Rigidbody self);

		// Token: 0x06001767 RID: 5991 RVA: 0x0001E0D0 File Offset: 0x0001C2D0
		public bool IsSleeping()
		{
			return Rigidbody.INTERNAL_CALL_IsSleeping(this);
		}

		// Token: 0x06001768 RID: 5992
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_IsSleeping(Rigidbody self);

		// Token: 0x06001769 RID: 5993 RVA: 0x0001E0EC File Offset: 0x0001C2EC
		public void WakeUp()
		{
			Rigidbody.INTERNAL_CALL_WakeUp(this);
		}

		// Token: 0x0600176A RID: 5994
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_WakeUp(Rigidbody self);

		// Token: 0x0600176B RID: 5995 RVA: 0x0001E0F8 File Offset: 0x0001C2F8
		public void ResetCenterOfMass()
		{
			Rigidbody.INTERNAL_CALL_ResetCenterOfMass(this);
		}

		// Token: 0x0600176C RID: 5996
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ResetCenterOfMass(Rigidbody self);

		// Token: 0x0600176D RID: 5997 RVA: 0x0001E104 File Offset: 0x0001C304
		public void ResetInertiaTensor()
		{
			Rigidbody.INTERNAL_CALL_ResetInertiaTensor(this);
		}

		// Token: 0x0600176E RID: 5998
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ResetInertiaTensor(Rigidbody self);

		// Token: 0x1700054D RID: 1357
		// (get) Token: 0x0600176F RID: 5999
		// (set) Token: 0x06001770 RID: 6000
		public extern int solverIterations { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700054E RID: 1358
		// (get) Token: 0x06001771 RID: 6001 RVA: 0x0001E110 File Offset: 0x0001C310
		// (set) Token: 0x06001772 RID: 6002 RVA: 0x0001E12C File Offset: 0x0001C32C
		[Obsolete("Please use Rigidbody.solverIterations instead. (UnityUpgradable) -> solverIterations")]
		public int solverIterationCount
		{
			get
			{
				return this.solverIterations;
			}
			set
			{
				this.solverIterations = value;
			}
		}

		// Token: 0x1700054F RID: 1359
		// (get) Token: 0x06001773 RID: 6003
		// (set) Token: 0x06001774 RID: 6004
		public extern int solverVelocityIterations { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000550 RID: 1360
		// (get) Token: 0x06001775 RID: 6005 RVA: 0x0001E138 File Offset: 0x0001C338
		// (set) Token: 0x06001776 RID: 6006 RVA: 0x0001E154 File Offset: 0x0001C354
		[Obsolete("Please use Rigidbody.solverVelocityIterations instead. (UnityUpgradable) -> solverVelocityIterations")]
		public int solverVelocityIterationCount
		{
			get
			{
				return this.solverVelocityIterations;
			}
			set
			{
				this.solverVelocityIterations = value;
			}
		}

		// Token: 0x17000551 RID: 1361
		// (get) Token: 0x06001777 RID: 6007
		// (set) Token: 0x06001778 RID: 6008
		[Obsolete("The sleepVelocity is no longer supported. Use sleepThreshold. Note that sleepThreshold is energy but not velocity.")]
		public extern float sleepVelocity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000552 RID: 1362
		// (get) Token: 0x06001779 RID: 6009
		// (set) Token: 0x0600177A RID: 6010
		[Obsolete("The sleepAngularVelocity is no longer supported. Set Use sleepThreshold to specify energy.")]
		public extern float sleepAngularVelocity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000553 RID: 1363
		// (get) Token: 0x0600177B RID: 6011
		// (set) Token: 0x0600177C RID: 6012
		public extern float sleepThreshold { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000554 RID: 1364
		// (get) Token: 0x0600177D RID: 6013
		// (set) Token: 0x0600177E RID: 6014
		public extern float maxAngularVelocity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600177F RID: 6015 RVA: 0x0001E160 File Offset: 0x0001C360
		public bool SweepTest(Vector3 direction, out RaycastHit hitInfo, [DefaultValue("Mathf.Infinity")] float maxDistance, [DefaultValue("QueryTriggerInteraction.UseGlobal")] QueryTriggerInteraction queryTriggerInteraction)
		{
			return Rigidbody.INTERNAL_CALL_SweepTest(this, ref direction, out hitInfo, maxDistance, queryTriggerInteraction);
		}

		// Token: 0x06001780 RID: 6016 RVA: 0x0001E184 File Offset: 0x0001C384
		[ExcludeFromDocs]
		public bool SweepTest(Vector3 direction, out RaycastHit hitInfo, float maxDistance)
		{
			QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal;
			return Rigidbody.INTERNAL_CALL_SweepTest(this, ref direction, out hitInfo, maxDistance, queryTriggerInteraction);
		}

		// Token: 0x06001781 RID: 6017 RVA: 0x0001E1A8 File Offset: 0x0001C3A8
		[ExcludeFromDocs]
		public bool SweepTest(Vector3 direction, out RaycastHit hitInfo)
		{
			QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal;
			float positiveInfinity = float.PositiveInfinity;
			return Rigidbody.INTERNAL_CALL_SweepTest(this, ref direction, out hitInfo, positiveInfinity, queryTriggerInteraction);
		}

		// Token: 0x06001782 RID: 6018
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_SweepTest(Rigidbody self, ref Vector3 direction, out RaycastHit hitInfo, float maxDistance, QueryTriggerInteraction queryTriggerInteraction);

		// Token: 0x06001783 RID: 6019 RVA: 0x0001E1D0 File Offset: 0x0001C3D0
		public RaycastHit[] SweepTestAll(Vector3 direction, [DefaultValue("Mathf.Infinity")] float maxDistance, [DefaultValue("QueryTriggerInteraction.UseGlobal")] QueryTriggerInteraction queryTriggerInteraction)
		{
			return Rigidbody.INTERNAL_CALL_SweepTestAll(this, ref direction, maxDistance, queryTriggerInteraction);
		}

		// Token: 0x06001784 RID: 6020 RVA: 0x0001E1F0 File Offset: 0x0001C3F0
		[ExcludeFromDocs]
		public RaycastHit[] SweepTestAll(Vector3 direction, float maxDistance)
		{
			QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal;
			return Rigidbody.INTERNAL_CALL_SweepTestAll(this, ref direction, maxDistance, queryTriggerInteraction);
		}

		// Token: 0x06001785 RID: 6021 RVA: 0x0001E214 File Offset: 0x0001C414
		[ExcludeFromDocs]
		public RaycastHit[] SweepTestAll(Vector3 direction)
		{
			QueryTriggerInteraction queryTriggerInteraction = QueryTriggerInteraction.UseGlobal;
			float positiveInfinity = float.PositiveInfinity;
			return Rigidbody.INTERNAL_CALL_SweepTestAll(this, ref direction, positiveInfinity, queryTriggerInteraction);
		}

		// Token: 0x06001786 RID: 6022
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RaycastHit[] INTERNAL_CALL_SweepTestAll(Rigidbody self, ref Vector3 direction, float maxDistance, QueryTriggerInteraction queryTriggerInteraction);

		// Token: 0x06001787 RID: 6023 RVA: 0x0001E23C File Offset: 0x0001C43C
		[Obsolete("use Rigidbody.maxAngularVelocity instead.")]
		public void SetMaxAngularVelocity(float a)
		{
			this.maxAngularVelocity = a;
		}
	}
}
