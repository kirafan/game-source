﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000187 RID: 391
	[UsedByNativeCode]
	public struct ClothSkinningCoefficient
	{
		// Token: 0x04000411 RID: 1041
		public float maxDistance;

		// Token: 0x04000412 RID: 1042
		public float collisionSphereDistance;
	}
}
