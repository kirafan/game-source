﻿using System;

namespace UnityEngine
{
	// Token: 0x0200010C RID: 268
	public enum ParticleSystemSubEmitterType
	{
		// Token: 0x040002F7 RID: 759
		Birth,
		// Token: 0x040002F8 RID: 760
		Collision,
		// Token: 0x040002F9 RID: 761
		Death
	}
}
