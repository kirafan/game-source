﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngineInternal;

namespace UnityEngine
{
	// Token: 0x0200009A RID: 154
	public sealed class Network
	{
		// Token: 0x06000A87 RID: 2695
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern NetworkConnectionError InitializeServer(int connections, int listenPort, bool useNat);

		// Token: 0x06000A88 RID: 2696
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern NetworkConnectionError Internal_InitializeServerDeprecated(int connections, int listenPort);

		// Token: 0x06000A89 RID: 2697 RVA: 0x0000F3E4 File Offset: 0x0000D5E4
		[Obsolete("Use the IntializeServer(connections, listenPort, useNat) function instead")]
		public static NetworkConnectionError InitializeServer(int connections, int listenPort)
		{
			return Network.Internal_InitializeServerDeprecated(connections, listenPort);
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x06000A8A RID: 2698
		// (set) Token: 0x06000A8B RID: 2699
		public static extern string incomingPassword { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x06000A8C RID: 2700
		// (set) Token: 0x06000A8D RID: 2701
		public static extern NetworkLogLevel logLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000A8E RID: 2702
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void InitializeSecurity();

		// Token: 0x06000A8F RID: 2703
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern NetworkConnectionError Internal_ConnectToSingleIP(string IP, int remotePort, int localPort, [DefaultValue("\"\"")] string password);

		// Token: 0x06000A90 RID: 2704 RVA: 0x0000F400 File Offset: 0x0000D600
		[ExcludeFromDocs]
		private static NetworkConnectionError Internal_ConnectToSingleIP(string IP, int remotePort, int localPort)
		{
			string password = "";
			return Network.Internal_ConnectToSingleIP(IP, remotePort, localPort, password);
		}

		// Token: 0x06000A91 RID: 2705
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern NetworkConnectionError Internal_ConnectToGuid(string guid, string password);

		// Token: 0x06000A92 RID: 2706
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern NetworkConnectionError Internal_ConnectToIPs(string[] IP, int remotePort, int localPort, [DefaultValue("\"\"")] string password);

		// Token: 0x06000A93 RID: 2707 RVA: 0x0000F424 File Offset: 0x0000D624
		[ExcludeFromDocs]
		private static NetworkConnectionError Internal_ConnectToIPs(string[] IP, int remotePort, int localPort)
		{
			string password = "";
			return Network.Internal_ConnectToIPs(IP, remotePort, localPort, password);
		}

		// Token: 0x06000A94 RID: 2708 RVA: 0x0000F448 File Offset: 0x0000D648
		[ExcludeFromDocs]
		public static NetworkConnectionError Connect(string IP, int remotePort)
		{
			string password = "";
			return Network.Connect(IP, remotePort, password);
		}

		// Token: 0x06000A95 RID: 2709 RVA: 0x0000F46C File Offset: 0x0000D66C
		public static NetworkConnectionError Connect(string IP, int remotePort, [DefaultValue("\"\"")] string password)
		{
			return Network.Internal_ConnectToSingleIP(IP, remotePort, 0, password);
		}

		// Token: 0x06000A96 RID: 2710 RVA: 0x0000F48C File Offset: 0x0000D68C
		[ExcludeFromDocs]
		public static NetworkConnectionError Connect(string[] IPs, int remotePort)
		{
			string password = "";
			return Network.Connect(IPs, remotePort, password);
		}

		// Token: 0x06000A97 RID: 2711 RVA: 0x0000F4B0 File Offset: 0x0000D6B0
		public static NetworkConnectionError Connect(string[] IPs, int remotePort, [DefaultValue("\"\"")] string password)
		{
			return Network.Internal_ConnectToIPs(IPs, remotePort, 0, password);
		}

		// Token: 0x06000A98 RID: 2712 RVA: 0x0000F4D0 File Offset: 0x0000D6D0
		[ExcludeFromDocs]
		public static NetworkConnectionError Connect(string GUID)
		{
			string password = "";
			return Network.Connect(GUID, password);
		}

		// Token: 0x06000A99 RID: 2713 RVA: 0x0000F4F4 File Offset: 0x0000D6F4
		public static NetworkConnectionError Connect(string GUID, [DefaultValue("\"\"")] string password)
		{
			return Network.Internal_ConnectToGuid(GUID, password);
		}

		// Token: 0x06000A9A RID: 2714 RVA: 0x0000F510 File Offset: 0x0000D710
		[ExcludeFromDocs]
		public static NetworkConnectionError Connect(HostData hostData)
		{
			string password = "";
			return Network.Connect(hostData, password);
		}

		// Token: 0x06000A9B RID: 2715 RVA: 0x0000F534 File Offset: 0x0000D734
		public static NetworkConnectionError Connect(HostData hostData, [DefaultValue("\"\"")] string password)
		{
			if (hostData == null)
			{
				throw new NullReferenceException();
			}
			NetworkConnectionError result;
			if (hostData.guid.Length > 0 && hostData.useNat)
			{
				result = Network.Connect(hostData.guid, password);
			}
			else
			{
				result = Network.Connect(hostData.ip, hostData.port, password);
			}
			return result;
		}

		// Token: 0x06000A9C RID: 2716
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Disconnect([DefaultValue("200")] int timeout);

		// Token: 0x06000A9D RID: 2717 RVA: 0x0000F598 File Offset: 0x0000D798
		[ExcludeFromDocs]
		public static void Disconnect()
		{
			int timeout = 200;
			Network.Disconnect(timeout);
		}

		// Token: 0x06000A9E RID: 2718 RVA: 0x0000F5B4 File Offset: 0x0000D7B4
		public static void CloseConnection(NetworkPlayer target, bool sendDisconnectionNotification)
		{
			Network.INTERNAL_CALL_CloseConnection(ref target, sendDisconnectionNotification);
		}

		// Token: 0x06000A9F RID: 2719
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_CloseConnection(ref NetworkPlayer target, bool sendDisconnectionNotification);

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x06000AA0 RID: 2720
		public static extern NetworkPlayer[] connections { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000AA1 RID: 2721
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetPlayer();

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x06000AA2 RID: 2722 RVA: 0x0000F5C0 File Offset: 0x0000D7C0
		public static NetworkPlayer player
		{
			get
			{
				NetworkPlayer result;
				result.index = Network.Internal_GetPlayer();
				return result;
			}
		}

		// Token: 0x06000AA3 RID: 2723
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_AllocateViewID(out NetworkViewID viewID);

		// Token: 0x06000AA4 RID: 2724 RVA: 0x0000F5E4 File Offset: 0x0000D7E4
		public static NetworkViewID AllocateViewID()
		{
			NetworkViewID result;
			Network.Internal_AllocateViewID(out result);
			return result;
		}

		// Token: 0x06000AA5 RID: 2725 RVA: 0x0000F604 File Offset: 0x0000D804
		[TypeInferenceRule(TypeInferenceRules.TypeOfFirstArgument)]
		public static Object Instantiate(Object prefab, Vector3 position, Quaternion rotation, int group)
		{
			return Network.INTERNAL_CALL_Instantiate(prefab, ref position, ref rotation, group);
		}

		// Token: 0x06000AA6 RID: 2726
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Object INTERNAL_CALL_Instantiate(Object prefab, ref Vector3 position, ref Quaternion rotation, int group);

		// Token: 0x06000AA7 RID: 2727 RVA: 0x0000F624 File Offset: 0x0000D824
		public static void Destroy(NetworkViewID viewID)
		{
			Network.INTERNAL_CALL_Destroy(ref viewID);
		}

		// Token: 0x06000AA8 RID: 2728
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Destroy(ref NetworkViewID viewID);

		// Token: 0x06000AA9 RID: 2729 RVA: 0x0000F630 File Offset: 0x0000D830
		public static void Destroy(GameObject gameObject)
		{
			if (gameObject != null)
			{
				NetworkView component = gameObject.GetComponent<NetworkView>();
				if (component != null)
				{
					Network.Destroy(component.viewID);
				}
				else
				{
					Debug.LogError("Couldn't destroy game object because no network view is attached to it.", gameObject);
				}
			}
		}

		// Token: 0x06000AAA RID: 2730 RVA: 0x0000F67C File Offset: 0x0000D87C
		public static void DestroyPlayerObjects(NetworkPlayer playerID)
		{
			Network.INTERNAL_CALL_DestroyPlayerObjects(ref playerID);
		}

		// Token: 0x06000AAB RID: 2731
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_DestroyPlayerObjects(ref NetworkPlayer playerID);

		// Token: 0x06000AAC RID: 2732 RVA: 0x0000F688 File Offset: 0x0000D888
		private static void Internal_RemoveRPCs(NetworkPlayer playerID, NetworkViewID viewID, uint channelMask)
		{
			Network.INTERNAL_CALL_Internal_RemoveRPCs(ref playerID, ref viewID, channelMask);
		}

		// Token: 0x06000AAD RID: 2733
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_RemoveRPCs(ref NetworkPlayer playerID, ref NetworkViewID viewID, uint channelMask);

		// Token: 0x06000AAE RID: 2734 RVA: 0x0000F698 File Offset: 0x0000D898
		public static void RemoveRPCs(NetworkPlayer playerID)
		{
			Network.Internal_RemoveRPCs(playerID, NetworkViewID.unassigned, uint.MaxValue);
		}

		// Token: 0x06000AAF RID: 2735 RVA: 0x0000F6A8 File Offset: 0x0000D8A8
		public static void RemoveRPCs(NetworkPlayer playerID, int group)
		{
			Network.Internal_RemoveRPCs(playerID, NetworkViewID.unassigned, 1U << group);
		}

		// Token: 0x06000AB0 RID: 2736 RVA: 0x0000F6BC File Offset: 0x0000D8BC
		public static void RemoveRPCs(NetworkViewID viewID)
		{
			Network.Internal_RemoveRPCs(NetworkPlayer.unassigned, viewID, uint.MaxValue);
		}

		// Token: 0x06000AB1 RID: 2737 RVA: 0x0000F6CC File Offset: 0x0000D8CC
		public static void RemoveRPCsInGroup(int group)
		{
			Network.Internal_RemoveRPCs(NetworkPlayer.unassigned, NetworkViewID.unassigned, 1U << group);
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x06000AB2 RID: 2738
		public static extern bool isClient { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700026D RID: 621
		// (get) Token: 0x06000AB3 RID: 2739
		public static extern bool isServer { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700026E RID: 622
		// (get) Token: 0x06000AB4 RID: 2740
		public static extern NetworkPeerType peerType { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000AB5 RID: 2741
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SetLevelPrefix(int prefix);

		// Token: 0x06000AB6 RID: 2742 RVA: 0x0000F6E4 File Offset: 0x0000D8E4
		public static int GetLastPing(NetworkPlayer player)
		{
			return Network.INTERNAL_CALL_GetLastPing(ref player);
		}

		// Token: 0x06000AB7 RID: 2743
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_GetLastPing(ref NetworkPlayer player);

		// Token: 0x06000AB8 RID: 2744 RVA: 0x0000F700 File Offset: 0x0000D900
		public static int GetAveragePing(NetworkPlayer player)
		{
			return Network.INTERNAL_CALL_GetAveragePing(ref player);
		}

		// Token: 0x06000AB9 RID: 2745
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_GetAveragePing(ref NetworkPlayer player);

		// Token: 0x1700026F RID: 623
		// (get) Token: 0x06000ABA RID: 2746
		// (set) Token: 0x06000ABB RID: 2747
		public static extern float sendRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000270 RID: 624
		// (get) Token: 0x06000ABC RID: 2748
		// (set) Token: 0x06000ABD RID: 2749
		public static extern bool isMessageQueueRunning { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000ABE RID: 2750 RVA: 0x0000F71C File Offset: 0x0000D91C
		public static void SetReceivingEnabled(NetworkPlayer player, int group, bool enabled)
		{
			Network.INTERNAL_CALL_SetReceivingEnabled(ref player, group, enabled);
		}

		// Token: 0x06000ABF RID: 2751
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetReceivingEnabled(ref NetworkPlayer player, int group, bool enabled);

		// Token: 0x06000AC0 RID: 2752
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_SetSendingGlobal(int group, bool enabled);

		// Token: 0x06000AC1 RID: 2753 RVA: 0x0000F728 File Offset: 0x0000D928
		private static void Internal_SetSendingSpecific(NetworkPlayer player, int group, bool enabled)
		{
			Network.INTERNAL_CALL_Internal_SetSendingSpecific(ref player, group, enabled);
		}

		// Token: 0x06000AC2 RID: 2754
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_SetSendingSpecific(ref NetworkPlayer player, int group, bool enabled);

		// Token: 0x06000AC3 RID: 2755 RVA: 0x0000F734 File Offset: 0x0000D934
		public static void SetSendingEnabled(int group, bool enabled)
		{
			Network.Internal_SetSendingGlobal(group, enabled);
		}

		// Token: 0x06000AC4 RID: 2756 RVA: 0x0000F740 File Offset: 0x0000D940
		public static void SetSendingEnabled(NetworkPlayer player, int group, bool enabled)
		{
			Network.Internal_SetSendingSpecific(player, group, enabled);
		}

		// Token: 0x06000AC5 RID: 2757
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_GetTime(out double t);

		// Token: 0x17000271 RID: 625
		// (get) Token: 0x06000AC6 RID: 2758 RVA: 0x0000F74C File Offset: 0x0000D94C
		public static double time
		{
			get
			{
				double result;
				Network.Internal_GetTime(out result);
				return result;
			}
		}

		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06000AC7 RID: 2759
		// (set) Token: 0x06000AC8 RID: 2760
		public static extern int minimumAllocatableViewIDs { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06000AC9 RID: 2761
		// (set) Token: 0x06000ACA RID: 2762
		[Obsolete("No longer needed. This is now explicitly set in the InitializeServer function call. It is implicitly set when calling Connect depending on if an IP/port combination is used (useNat=false) or a GUID is used(useNat=true).")]
		public static extern bool useNat { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06000ACB RID: 2763
		// (set) Token: 0x06000ACC RID: 2764
		public static extern string natFacilitatorIP { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000ACD RID: 2765
		// (set) Token: 0x06000ACE RID: 2766
		public static extern int natFacilitatorPort { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000ACF RID: 2767
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern ConnectionTesterStatus TestConnection([DefaultValue("false")] bool forceTest);

		// Token: 0x06000AD0 RID: 2768 RVA: 0x0000F76C File Offset: 0x0000D96C
		[ExcludeFromDocs]
		public static ConnectionTesterStatus TestConnection()
		{
			bool forceTest = false;
			return Network.TestConnection(forceTest);
		}

		// Token: 0x06000AD1 RID: 2769
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern ConnectionTesterStatus TestConnectionNAT([DefaultValue("false")] bool forceTest);

		// Token: 0x06000AD2 RID: 2770 RVA: 0x0000F78C File Offset: 0x0000D98C
		[ExcludeFromDocs]
		public static ConnectionTesterStatus TestConnectionNAT()
		{
			bool forceTest = false;
			return Network.TestConnectionNAT(forceTest);
		}

		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06000AD3 RID: 2771
		// (set) Token: 0x06000AD4 RID: 2772
		public static extern string connectionTesterIP { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x06000AD5 RID: 2773
		// (set) Token: 0x06000AD6 RID: 2774
		public static extern int connectionTesterPort { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000AD7 RID: 2775
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool HavePublicAddress();

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x06000AD8 RID: 2776
		// (set) Token: 0x06000AD9 RID: 2777
		public static extern int maxConnections { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000279 RID: 633
		// (get) Token: 0x06000ADA RID: 2778
		// (set) Token: 0x06000ADB RID: 2779
		public static extern string proxyIP { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700027A RID: 634
		// (get) Token: 0x06000ADC RID: 2780
		// (set) Token: 0x06000ADD RID: 2781
		public static extern int proxyPort { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06000ADE RID: 2782
		// (set) Token: 0x06000ADF RID: 2783
		public static extern bool useProxy { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06000AE0 RID: 2784
		// (set) Token: 0x06000AE1 RID: 2785
		public static extern string proxyPassword { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
