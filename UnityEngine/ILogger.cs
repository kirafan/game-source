﻿using System;

namespace UnityEngine
{
	// Token: 0x020003AA RID: 938
	public interface ILogger : ILogHandler
	{
		// Token: 0x17000B35 RID: 2869
		// (get) Token: 0x06002FF5 RID: 12277
		// (set) Token: 0x06002FF6 RID: 12278
		ILogHandler logHandler { get; set; }

		// Token: 0x17000B36 RID: 2870
		// (get) Token: 0x06002FF7 RID: 12279
		// (set) Token: 0x06002FF8 RID: 12280
		bool logEnabled { get; set; }

		// Token: 0x17000B37 RID: 2871
		// (get) Token: 0x06002FF9 RID: 12281
		// (set) Token: 0x06002FFA RID: 12282
		LogType filterLogType { get; set; }

		// Token: 0x06002FFB RID: 12283
		bool IsLogTypeAllowed(LogType logType);

		// Token: 0x06002FFC RID: 12284
		void Log(LogType logType, object message);

		// Token: 0x06002FFD RID: 12285
		void Log(LogType logType, object message, Object context);

		// Token: 0x06002FFE RID: 12286
		void Log(LogType logType, string tag, object message);

		// Token: 0x06002FFF RID: 12287
		void Log(LogType logType, string tag, object message, Object context);

		// Token: 0x06003000 RID: 12288
		void Log(object message);

		// Token: 0x06003001 RID: 12289
		void Log(string tag, object message);

		// Token: 0x06003002 RID: 12290
		void Log(string tag, object message, Object context);

		// Token: 0x06003003 RID: 12291
		void LogWarning(string tag, object message);

		// Token: 0x06003004 RID: 12292
		void LogWarning(string tag, object message, Object context);

		// Token: 0x06003005 RID: 12293
		void LogError(string tag, object message);

		// Token: 0x06003006 RID: 12294
		void LogError(string tag, object message, Object context);

		// Token: 0x06003007 RID: 12295
		void LogFormat(LogType logType, string format, params object[] args);

		// Token: 0x06003008 RID: 12296
		void LogException(Exception exception);
	}
}
