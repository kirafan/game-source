﻿using System;

namespace UnityEngine
{
	// Token: 0x020002EC RID: 748
	public enum LightmappingMode
	{
		// Token: 0x04000B0A RID: 2826
		Realtime = 4,
		// Token: 0x04000B0B RID: 2827
		Baked = 2,
		// Token: 0x04000B0C RID: 2828
		Mixed = 1
	}
}
