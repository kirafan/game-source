﻿using System;

namespace UnityEngine
{
	// Token: 0x02000375 RID: 885
	internal class SystemClock
	{
		// Token: 0x17000AF7 RID: 2807
		// (get) Token: 0x06002E5F RID: 11871 RVA: 0x0004A738 File Offset: 0x00048938
		public static DateTime now
		{
			get
			{
				return DateTime.Now;
			}
		}

		// Token: 0x06002E60 RID: 11872 RVA: 0x0004A754 File Offset: 0x00048954
		public static long ToUnixTimeMilliseconds(DateTime date)
		{
			return Convert.ToInt64((date.ToUniversalTime() - SystemClock.s_Epoch).TotalMilliseconds);
		}

		// Token: 0x06002E61 RID: 11873 RVA: 0x0004A788 File Offset: 0x00048988
		public static long ToUnixTimeSeconds(DateTime date)
		{
			return Convert.ToInt64((date.ToUniversalTime() - SystemClock.s_Epoch).TotalSeconds);
		}

		// Token: 0x04000D4F RID: 3407
		private static readonly DateTime s_Epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
	}
}
