﻿using System;

namespace UnityEngine
{
	// Token: 0x020000C6 RID: 198
	public enum ProceduralOutputType
	{
		// Token: 0x040001F4 RID: 500
		Unknown,
		// Token: 0x040001F5 RID: 501
		Diffuse,
		// Token: 0x040001F6 RID: 502
		Normal,
		// Token: 0x040001F7 RID: 503
		Height,
		// Token: 0x040001F8 RID: 504
		Emissive,
		// Token: 0x040001F9 RID: 505
		Specular,
		// Token: 0x040001FA RID: 506
		Opacity,
		// Token: 0x040001FB RID: 507
		Smoothness,
		// Token: 0x040001FC RID: 508
		AmbientOcclusion,
		// Token: 0x040001FD RID: 509
		DetailMask,
		// Token: 0x040001FE RID: 510
		Metallic,
		// Token: 0x040001FF RID: 511
		Roughness
	}
}
