﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200008D RID: 141
	[RequiredByNativeCode]
	public class MonoBehaviour : Behaviour
	{
		// Token: 0x06000A27 RID: 2599
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern MonoBehaviour();

		// Token: 0x06000A28 RID: 2600
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_CancelInvokeAll();

		// Token: 0x06000A29 RID: 2601
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool Internal_IsInvokingAll();

		// Token: 0x06000A2A RID: 2602
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Invoke(string methodName, float time);

		// Token: 0x06000A2B RID: 2603
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void InvokeRepeating(string methodName, float time, float repeatRate);

		// Token: 0x06000A2C RID: 2604 RVA: 0x0000EE98 File Offset: 0x0000D098
		public void CancelInvoke()
		{
			this.Internal_CancelInvokeAll();
		}

		// Token: 0x06000A2D RID: 2605
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CancelInvoke(string methodName);

		// Token: 0x06000A2E RID: 2606
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsInvoking(string methodName);

		// Token: 0x06000A2F RID: 2607 RVA: 0x0000EEA4 File Offset: 0x0000D0A4
		public bool IsInvoking()
		{
			return this.Internal_IsInvokingAll();
		}

		// Token: 0x06000A30 RID: 2608 RVA: 0x0000EEC0 File Offset: 0x0000D0C0
		public Coroutine StartCoroutine(IEnumerator routine)
		{
			return this.StartCoroutine_Auto_Internal(routine);
		}

		// Token: 0x06000A31 RID: 2609 RVA: 0x0000EEDC File Offset: 0x0000D0DC
		[Obsolete("StartCoroutine_Auto has been deprecated. Use StartCoroutine instead (UnityUpgradable) -> StartCoroutine([mscorlib] System.Collections.IEnumerator)", false)]
		public Coroutine StartCoroutine_Auto(IEnumerator routine)
		{
			return this.StartCoroutine(routine);
		}

		// Token: 0x06000A32 RID: 2610
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Coroutine StartCoroutine_Auto_Internal(IEnumerator routine);

		// Token: 0x06000A33 RID: 2611
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Coroutine StartCoroutine(string methodName, [DefaultValue("null")] object value);

		// Token: 0x06000A34 RID: 2612 RVA: 0x0000EEF8 File Offset: 0x0000D0F8
		[ExcludeFromDocs]
		public Coroutine StartCoroutine(string methodName)
		{
			object value = null;
			return this.StartCoroutine(methodName, value);
		}

		// Token: 0x06000A35 RID: 2613
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StopCoroutine(string methodName);

		// Token: 0x06000A36 RID: 2614 RVA: 0x0000EF18 File Offset: 0x0000D118
		public void StopCoroutine(IEnumerator routine)
		{
			this.StopCoroutineViaEnumerator_Auto(routine);
		}

		// Token: 0x06000A37 RID: 2615 RVA: 0x0000EF24 File Offset: 0x0000D124
		public void StopCoroutine(Coroutine routine)
		{
			this.StopCoroutine_Auto(routine);
		}

		// Token: 0x06000A38 RID: 2616
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void StopCoroutineViaEnumerator_Auto(IEnumerator routine);

		// Token: 0x06000A39 RID: 2617
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void StopCoroutine_Auto(Coroutine routine);

		// Token: 0x06000A3A RID: 2618
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void StopAllCoroutines();

		// Token: 0x06000A3B RID: 2619 RVA: 0x0000EF30 File Offset: 0x0000D130
		public static void print(object message)
		{
			Debug.Log(message);
		}

		// Token: 0x17000250 RID: 592
		// (get) Token: 0x06000A3C RID: 2620
		// (set) Token: 0x06000A3D RID: 2621
		public extern bool useGUILayout { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
