﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Audio;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020001A6 RID: 422
	public sealed class AudioSource : Behaviour
	{
		// Token: 0x1700071A RID: 1818
		// (get) Token: 0x06001D20 RID: 7456
		// (set) Token: 0x06001D21 RID: 7457
		public extern float volume { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700071B RID: 1819
		// (get) Token: 0x06001D22 RID: 7458
		// (set) Token: 0x06001D23 RID: 7459
		public extern float pitch { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700071C RID: 1820
		// (get) Token: 0x06001D24 RID: 7460
		// (set) Token: 0x06001D25 RID: 7461
		public extern float time { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700071D RID: 1821
		// (get) Token: 0x06001D26 RID: 7462
		// (set) Token: 0x06001D27 RID: 7463
		[ThreadAndSerializationSafe]
		public extern int timeSamples { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700071E RID: 1822
		// (get) Token: 0x06001D28 RID: 7464
		// (set) Token: 0x06001D29 RID: 7465
		[ThreadAndSerializationSafe]
		public extern AudioClip clip { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700071F RID: 1823
		// (get) Token: 0x06001D2A RID: 7466
		// (set) Token: 0x06001D2B RID: 7467
		public extern AudioMixerGroup outputAudioMixerGroup { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001D2C RID: 7468
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Play([DefaultValue("0")] ulong delay);

		// Token: 0x06001D2D RID: 7469 RVA: 0x00022658 File Offset: 0x00020858
		[ExcludeFromDocs]
		public void Play()
		{
			ulong delay = 0UL;
			this.Play(delay);
		}

		// Token: 0x06001D2E RID: 7470
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void PlayDelayed(float delay);

		// Token: 0x06001D2F RID: 7471
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void PlayScheduled(double time);

		// Token: 0x06001D30 RID: 7472
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetScheduledStartTime(double time);

		// Token: 0x06001D31 RID: 7473
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetScheduledEndTime(double time);

		// Token: 0x06001D32 RID: 7474
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Stop();

		// Token: 0x06001D33 RID: 7475 RVA: 0x00022670 File Offset: 0x00020870
		public void Pause()
		{
			AudioSource.INTERNAL_CALL_Pause(this);
		}

		// Token: 0x06001D34 RID: 7476
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Pause(AudioSource self);

		// Token: 0x06001D35 RID: 7477 RVA: 0x0002267C File Offset: 0x0002087C
		public void UnPause()
		{
			AudioSource.INTERNAL_CALL_UnPause(this);
		}

		// Token: 0x06001D36 RID: 7478
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_UnPause(AudioSource self);

		// Token: 0x17000720 RID: 1824
		// (get) Token: 0x06001D37 RID: 7479
		public extern bool isPlaying { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000721 RID: 1825
		// (get) Token: 0x06001D38 RID: 7480
		public extern bool isVirtual { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001D39 RID: 7481
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void PlayOneShot(AudioClip clip, [DefaultValue("1.0F")] float volumeScale);

		// Token: 0x06001D3A RID: 7482 RVA: 0x00022688 File Offset: 0x00020888
		[ExcludeFromDocs]
		public void PlayOneShot(AudioClip clip)
		{
			float volumeScale = 1f;
			this.PlayOneShot(clip, volumeScale);
		}

		// Token: 0x06001D3B RID: 7483 RVA: 0x000226A4 File Offset: 0x000208A4
		[ExcludeFromDocs]
		public static void PlayClipAtPoint(AudioClip clip, Vector3 position)
		{
			float volume = 1f;
			AudioSource.PlayClipAtPoint(clip, position, volume);
		}

		// Token: 0x06001D3C RID: 7484 RVA: 0x000226C0 File Offset: 0x000208C0
		public static void PlayClipAtPoint(AudioClip clip, Vector3 position, [DefaultValue("1.0F")] float volume)
		{
			GameObject gameObject = new GameObject("One shot audio");
			gameObject.transform.position = position;
			AudioSource audioSource = (AudioSource)gameObject.AddComponent(typeof(AudioSource));
			audioSource.clip = clip;
			audioSource.spatialBlend = 1f;
			audioSource.volume = volume;
			audioSource.Play();
			Object.Destroy(gameObject, clip.length * ((Time.timeScale >= 0.01f) ? Time.timeScale : 0.01f));
		}

		// Token: 0x17000722 RID: 1826
		// (get) Token: 0x06001D3D RID: 7485
		// (set) Token: 0x06001D3E RID: 7486
		public extern bool loop { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000723 RID: 1827
		// (get) Token: 0x06001D3F RID: 7487
		// (set) Token: 0x06001D40 RID: 7488
		public extern bool ignoreListenerVolume { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000724 RID: 1828
		// (get) Token: 0x06001D41 RID: 7489
		// (set) Token: 0x06001D42 RID: 7490
		public extern bool playOnAwake { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000725 RID: 1829
		// (get) Token: 0x06001D43 RID: 7491
		// (set) Token: 0x06001D44 RID: 7492
		public extern bool ignoreListenerPause { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000726 RID: 1830
		// (get) Token: 0x06001D45 RID: 7493
		// (set) Token: 0x06001D46 RID: 7494
		public extern AudioVelocityUpdateMode velocityUpdateMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000727 RID: 1831
		// (get) Token: 0x06001D47 RID: 7495
		// (set) Token: 0x06001D48 RID: 7496
		public extern float panStereo { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000728 RID: 1832
		// (get) Token: 0x06001D49 RID: 7497
		// (set) Token: 0x06001D4A RID: 7498
		public extern float spatialBlend { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000729 RID: 1833
		// (get) Token: 0x06001D4B RID: 7499
		// (set) Token: 0x06001D4C RID: 7500
		public extern bool spatialize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700072A RID: 1834
		// (get) Token: 0x06001D4D RID: 7501
		// (set) Token: 0x06001D4E RID: 7502
		public extern bool spatializePostEffects { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001D4F RID: 7503
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetCustomCurve(AudioSourceCurveType type, AnimationCurve curve);

		// Token: 0x06001D50 RID: 7504
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AnimationCurve GetCustomCurve(AudioSourceCurveType type);

		// Token: 0x1700072B RID: 1835
		// (get) Token: 0x06001D51 RID: 7505
		// (set) Token: 0x06001D52 RID: 7506
		public extern float reverbZoneMix { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700072C RID: 1836
		// (get) Token: 0x06001D53 RID: 7507
		// (set) Token: 0x06001D54 RID: 7508
		public extern bool bypassEffects { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700072D RID: 1837
		// (get) Token: 0x06001D55 RID: 7509
		// (set) Token: 0x06001D56 RID: 7510
		public extern bool bypassListenerEffects { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700072E RID: 1838
		// (get) Token: 0x06001D57 RID: 7511
		// (set) Token: 0x06001D58 RID: 7512
		public extern bool bypassReverbZones { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700072F RID: 1839
		// (get) Token: 0x06001D59 RID: 7513
		// (set) Token: 0x06001D5A RID: 7514
		public extern float dopplerLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000730 RID: 1840
		// (get) Token: 0x06001D5B RID: 7515
		// (set) Token: 0x06001D5C RID: 7516
		public extern float spread { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000731 RID: 1841
		// (get) Token: 0x06001D5D RID: 7517
		// (set) Token: 0x06001D5E RID: 7518
		public extern int priority { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000732 RID: 1842
		// (get) Token: 0x06001D5F RID: 7519
		// (set) Token: 0x06001D60 RID: 7520
		public extern bool mute { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000733 RID: 1843
		// (get) Token: 0x06001D61 RID: 7521
		// (set) Token: 0x06001D62 RID: 7522
		public extern float minDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000734 RID: 1844
		// (get) Token: 0x06001D63 RID: 7523
		// (set) Token: 0x06001D64 RID: 7524
		public extern float maxDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000735 RID: 1845
		// (get) Token: 0x06001D65 RID: 7525
		// (set) Token: 0x06001D66 RID: 7526
		public extern AudioRolloffMode rolloffMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001D67 RID: 7527
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetOutputDataHelper(float[] samples, int channel);

		// Token: 0x06001D68 RID: 7528 RVA: 0x00022748 File Offset: 0x00020948
		[Obsolete("GetOutputData return a float[] is deprecated, use GetOutputData passing a pre allocated array instead.")]
		public float[] GetOutputData(int numSamples, int channel)
		{
			float[] array = new float[numSamples];
			this.GetOutputDataHelper(array, channel);
			return array;
		}

		// Token: 0x06001D69 RID: 7529 RVA: 0x00022770 File Offset: 0x00020970
		public void GetOutputData(float[] samples, int channel)
		{
			this.GetOutputDataHelper(samples, channel);
		}

		// Token: 0x06001D6A RID: 7530
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetSpectrumDataHelper(float[] samples, int channel, FFTWindow window);

		// Token: 0x06001D6B RID: 7531 RVA: 0x0002277C File Offset: 0x0002097C
		[Obsolete("GetSpectrumData returning a float[] is deprecated, use GetSpectrumData passing a pre allocated array instead.")]
		public float[] GetSpectrumData(int numSamples, int channel, FFTWindow window)
		{
			float[] array = new float[numSamples];
			this.GetSpectrumDataHelper(array, channel, window);
			return array;
		}

		// Token: 0x06001D6C RID: 7532 RVA: 0x000227A4 File Offset: 0x000209A4
		public void GetSpectrumData(float[] samples, int channel, FFTWindow window)
		{
			this.GetSpectrumDataHelper(samples, channel, window);
		}

		// Token: 0x17000736 RID: 1846
		// (get) Token: 0x06001D6D RID: 7533
		// (set) Token: 0x06001D6E RID: 7534
		[Obsolete("minVolume is not supported anymore. Use min-, maxDistance and rolloffMode instead.", true)]
		public extern float minVolume { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000737 RID: 1847
		// (get) Token: 0x06001D6F RID: 7535
		// (set) Token: 0x06001D70 RID: 7536
		[Obsolete("maxVolume is not supported anymore. Use min-, maxDistance and rolloffMode instead.", true)]
		public extern float maxVolume { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000738 RID: 1848
		// (get) Token: 0x06001D71 RID: 7537
		// (set) Token: 0x06001D72 RID: 7538
		[Obsolete("rolloffFactor is not supported anymore. Use min-, maxDistance and rolloffMode instead.", true)]
		public extern float rolloffFactor { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06001D73 RID: 7539
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool SetSpatializerFloat(int index, float value);

		// Token: 0x06001D74 RID: 7540
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetSpatializerFloat(int index, out float value);
	}
}
