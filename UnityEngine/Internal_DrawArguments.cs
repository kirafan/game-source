﻿using System;

namespace UnityEngine
{
	// Token: 0x02000244 RID: 580
	internal struct Internal_DrawArguments
	{
		// Token: 0x040008D0 RID: 2256
		public IntPtr target;

		// Token: 0x040008D1 RID: 2257
		public Rect position;

		// Token: 0x040008D2 RID: 2258
		public int isHover;

		// Token: 0x040008D3 RID: 2259
		public int isActive;

		// Token: 0x040008D4 RID: 2260
		public int on;

		// Token: 0x040008D5 RID: 2261
		public int hasKeyboardFocus;
	}
}
