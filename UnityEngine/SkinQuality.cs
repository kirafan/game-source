﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F7 RID: 759
	public enum SkinQuality
	{
		// Token: 0x04000B41 RID: 2881
		Auto,
		// Token: 0x04000B42 RID: 2882
		Bone1,
		// Token: 0x04000B43 RID: 2883
		Bone2,
		// Token: 0x04000B44 RID: 2884
		Bone4 = 4
	}
}
