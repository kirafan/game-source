﻿using System;

namespace UnityEngine
{
	// Token: 0x020002F6 RID: 758
	public enum MeshTopology
	{
		// Token: 0x04000B3B RID: 2875
		Triangles,
		// Token: 0x04000B3C RID: 2876
		Quads = 2,
		// Token: 0x04000B3D RID: 2877
		Lines,
		// Token: 0x04000B3E RID: 2878
		LineStrip,
		// Token: 0x04000B3F RID: 2879
		Points
	}
}
