﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200036B RID: 875
	[RequiredByNativeCode]
	public sealed class SerializeField : Attribute
	{
	}
}
