﻿using System;

namespace UnityEngine
{
	// Token: 0x02000131 RID: 305
	public enum RigidbodyConstraints
	{
		// Token: 0x0400034E RID: 846
		None,
		// Token: 0x0400034F RID: 847
		FreezePositionX = 2,
		// Token: 0x04000350 RID: 848
		FreezePositionY = 4,
		// Token: 0x04000351 RID: 849
		FreezePositionZ = 8,
		// Token: 0x04000352 RID: 850
		FreezeRotationX = 16,
		// Token: 0x04000353 RID: 851
		FreezeRotationY = 32,
		// Token: 0x04000354 RID: 852
		FreezeRotationZ = 64,
		// Token: 0x04000355 RID: 853
		FreezePosition = 14,
		// Token: 0x04000356 RID: 854
		FreezeRotation = 112,
		// Token: 0x04000357 RID: 855
		FreezeAll = 126
	}
}
