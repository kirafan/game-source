﻿using System;

namespace UnityEngine
{
	// Token: 0x02000095 RID: 149
	public enum NetworkPeerType
	{
		// Token: 0x0400015B RID: 347
		Disconnected,
		// Token: 0x0400015C RID: 348
		Server,
		// Token: 0x0400015D RID: 349
		Client,
		// Token: 0x0400015E RID: 350
		Connecting
	}
}
