﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000B8 RID: 184
	public struct SortingLayer
	{
		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x06000D2C RID: 3372 RVA: 0x00011E34 File Offset: 0x00010034
		public int id
		{
			get
			{
				return this.m_Id;
			}
		}

		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x06000D2D RID: 3373 RVA: 0x00011E50 File Offset: 0x00010050
		public string name
		{
			get
			{
				return SortingLayer.IDToName(this.m_Id);
			}
		}

		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x06000D2E RID: 3374 RVA: 0x00011E70 File Offset: 0x00010070
		public int value
		{
			get
			{
				return SortingLayer.GetLayerValueFromID(this.m_Id);
			}
		}

		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x06000D2F RID: 3375 RVA: 0x00011E90 File Offset: 0x00010090
		public static SortingLayer[] layers
		{
			get
			{
				int[] sortingLayerIDsInternal = SortingLayer.GetSortingLayerIDsInternal();
				SortingLayer[] array = new SortingLayer[sortingLayerIDsInternal.Length];
				for (int i = 0; i < sortingLayerIDsInternal.Length; i++)
				{
					array[i].m_Id = sortingLayerIDsInternal[i];
				}
				return array;
			}
		}

		// Token: 0x06000D30 RID: 3376
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int[] GetSortingLayerIDsInternal();

		// Token: 0x06000D31 RID: 3377
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetLayerValueFromID(int id);

		// Token: 0x06000D32 RID: 3378
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetLayerValueFromName(string name);

		// Token: 0x06000D33 RID: 3379
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int NameToID(string name);

		// Token: 0x06000D34 RID: 3380
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string IDToName(int id);

		// Token: 0x06000D35 RID: 3381
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsValid(int id);

		// Token: 0x040001A7 RID: 423
		private int m_Id;
	}
}
