﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001BB RID: 443
	[RequiredByNativeCode]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class AnimationEvent
	{
		// Token: 0x06001E2F RID: 7727 RVA: 0x00022B74 File Offset: 0x00020D74
		public AnimationEvent()
		{
			this.m_Time = 0f;
			this.m_FunctionName = "";
			this.m_StringParameter = "";
			this.m_ObjectReferenceParameter = null;
			this.m_FloatParameter = 0f;
			this.m_IntParameter = 0;
			this.m_MessageOptions = 0;
			this.m_Source = AnimationEventSource.NoSource;
			this.m_StateSender = null;
		}

		// Token: 0x17000780 RID: 1920
		// (get) Token: 0x06001E30 RID: 7728 RVA: 0x00022BD8 File Offset: 0x00020DD8
		// (set) Token: 0x06001E31 RID: 7729 RVA: 0x00022BF4 File Offset: 0x00020DF4
		[Obsolete("Use stringParameter instead")]
		public string data
		{
			get
			{
				return this.m_StringParameter;
			}
			set
			{
				this.m_StringParameter = value;
			}
		}

		// Token: 0x17000781 RID: 1921
		// (get) Token: 0x06001E32 RID: 7730 RVA: 0x00022C00 File Offset: 0x00020E00
		// (set) Token: 0x06001E33 RID: 7731 RVA: 0x00022C1C File Offset: 0x00020E1C
		public string stringParameter
		{
			get
			{
				return this.m_StringParameter;
			}
			set
			{
				this.m_StringParameter = value;
			}
		}

		// Token: 0x17000782 RID: 1922
		// (get) Token: 0x06001E34 RID: 7732 RVA: 0x00022C28 File Offset: 0x00020E28
		// (set) Token: 0x06001E35 RID: 7733 RVA: 0x00022C44 File Offset: 0x00020E44
		public float floatParameter
		{
			get
			{
				return this.m_FloatParameter;
			}
			set
			{
				this.m_FloatParameter = value;
			}
		}

		// Token: 0x17000783 RID: 1923
		// (get) Token: 0x06001E36 RID: 7734 RVA: 0x00022C50 File Offset: 0x00020E50
		// (set) Token: 0x06001E37 RID: 7735 RVA: 0x00022C6C File Offset: 0x00020E6C
		public int intParameter
		{
			get
			{
				return this.m_IntParameter;
			}
			set
			{
				this.m_IntParameter = value;
			}
		}

		// Token: 0x17000784 RID: 1924
		// (get) Token: 0x06001E38 RID: 7736 RVA: 0x00022C78 File Offset: 0x00020E78
		// (set) Token: 0x06001E39 RID: 7737 RVA: 0x00022C94 File Offset: 0x00020E94
		public Object objectReferenceParameter
		{
			get
			{
				return this.m_ObjectReferenceParameter;
			}
			set
			{
				this.m_ObjectReferenceParameter = value;
			}
		}

		// Token: 0x17000785 RID: 1925
		// (get) Token: 0x06001E3A RID: 7738 RVA: 0x00022CA0 File Offset: 0x00020EA0
		// (set) Token: 0x06001E3B RID: 7739 RVA: 0x00022CBC File Offset: 0x00020EBC
		public string functionName
		{
			get
			{
				return this.m_FunctionName;
			}
			set
			{
				this.m_FunctionName = value;
			}
		}

		// Token: 0x17000786 RID: 1926
		// (get) Token: 0x06001E3C RID: 7740 RVA: 0x00022CC8 File Offset: 0x00020EC8
		// (set) Token: 0x06001E3D RID: 7741 RVA: 0x00022CE4 File Offset: 0x00020EE4
		public float time
		{
			get
			{
				return this.m_Time;
			}
			set
			{
				this.m_Time = value;
			}
		}

		// Token: 0x17000787 RID: 1927
		// (get) Token: 0x06001E3E RID: 7742 RVA: 0x00022CF0 File Offset: 0x00020EF0
		// (set) Token: 0x06001E3F RID: 7743 RVA: 0x00022D0C File Offset: 0x00020F0C
		public SendMessageOptions messageOptions
		{
			get
			{
				return (SendMessageOptions)this.m_MessageOptions;
			}
			set
			{
				this.m_MessageOptions = (int)value;
			}
		}

		// Token: 0x17000788 RID: 1928
		// (get) Token: 0x06001E40 RID: 7744 RVA: 0x00022D18 File Offset: 0x00020F18
		public bool isFiredByLegacy
		{
			get
			{
				return this.m_Source == AnimationEventSource.Legacy;
			}
		}

		// Token: 0x17000789 RID: 1929
		// (get) Token: 0x06001E41 RID: 7745 RVA: 0x00022D38 File Offset: 0x00020F38
		public bool isFiredByAnimator
		{
			get
			{
				return this.m_Source == AnimationEventSource.Animator;
			}
		}

		// Token: 0x1700078A RID: 1930
		// (get) Token: 0x06001E42 RID: 7746 RVA: 0x00022D58 File Offset: 0x00020F58
		public AnimationState animationState
		{
			get
			{
				if (!this.isFiredByLegacy)
				{
					Debug.LogError("AnimationEvent was not fired by Animation component, you shouldn't use AnimationEvent.animationState");
				}
				return this.m_StateSender;
			}
		}

		// Token: 0x1700078B RID: 1931
		// (get) Token: 0x06001E43 RID: 7747 RVA: 0x00022D88 File Offset: 0x00020F88
		public AnimatorStateInfo animatorStateInfo
		{
			get
			{
				if (!this.isFiredByAnimator)
				{
					Debug.LogError("AnimationEvent was not fired by Animator component, you shouldn't use AnimationEvent.animatorStateInfo");
				}
				return this.m_AnimatorStateInfo;
			}
		}

		// Token: 0x1700078C RID: 1932
		// (get) Token: 0x06001E44 RID: 7748 RVA: 0x00022DB8 File Offset: 0x00020FB8
		public AnimatorClipInfo animatorClipInfo
		{
			get
			{
				if (!this.isFiredByAnimator)
				{
					Debug.LogError("AnimationEvent was not fired by Animator component, you shouldn't use AnimationEvent.animatorClipInfo");
				}
				return this.m_AnimatorClipInfo;
			}
		}

		// Token: 0x06001E45 RID: 7749 RVA: 0x00022DE8 File Offset: 0x00020FE8
		internal int GetHash()
		{
			int hashCode = this.functionName.GetHashCode();
			return 33 * hashCode + this.time.GetHashCode();
		}

		// Token: 0x040004AF RID: 1199
		internal float m_Time;

		// Token: 0x040004B0 RID: 1200
		internal string m_FunctionName;

		// Token: 0x040004B1 RID: 1201
		internal string m_StringParameter;

		// Token: 0x040004B2 RID: 1202
		internal Object m_ObjectReferenceParameter;

		// Token: 0x040004B3 RID: 1203
		internal float m_FloatParameter;

		// Token: 0x040004B4 RID: 1204
		internal int m_IntParameter;

		// Token: 0x040004B5 RID: 1205
		internal int m_MessageOptions;

		// Token: 0x040004B6 RID: 1206
		internal AnimationEventSource m_Source;

		// Token: 0x040004B7 RID: 1207
		internal AnimationState m_StateSender;

		// Token: 0x040004B8 RID: 1208
		internal AnimatorStateInfo m_AnimatorStateInfo;

		// Token: 0x040004B9 RID: 1209
		internal AnimatorClipInfo m_AnimatorClipInfo;
	}
}
