﻿using System;

namespace UnityEngine
{
	// Token: 0x0200021E RID: 542
	public enum ScaleMode
	{
		// Token: 0x040007D0 RID: 2000
		StretchToFill,
		// Token: 0x040007D1 RID: 2001
		ScaleAndCrop,
		// Token: 0x040007D2 RID: 2002
		ScaleToFit
	}
}
