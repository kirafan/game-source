﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000168 RID: 360
	[UsedByNativeCode]
	public struct ContactPoint2D
	{
		// Token: 0x1700061D RID: 1565
		// (get) Token: 0x06001A94 RID: 6804 RVA: 0x000212A8 File Offset: 0x0001F4A8
		public Vector2 point
		{
			get
			{
				return this.m_Point;
			}
		}

		// Token: 0x1700061E RID: 1566
		// (get) Token: 0x06001A95 RID: 6805 RVA: 0x000212C4 File Offset: 0x0001F4C4
		public Vector2 normal
		{
			get
			{
				return this.m_Normal;
			}
		}

		// Token: 0x1700061F RID: 1567
		// (get) Token: 0x06001A96 RID: 6806 RVA: 0x000212E0 File Offset: 0x0001F4E0
		public Collider2D collider
		{
			get
			{
				return this.m_Collider;
			}
		}

		// Token: 0x17000620 RID: 1568
		// (get) Token: 0x06001A97 RID: 6807 RVA: 0x000212FC File Offset: 0x0001F4FC
		public Collider2D otherCollider
		{
			get
			{
				return this.m_OtherCollider;
			}
		}

		// Token: 0x040003EB RID: 1003
		internal Vector2 m_Point;

		// Token: 0x040003EC RID: 1004
		internal Vector2 m_Normal;

		// Token: 0x040003ED RID: 1005
		internal Collider2D m_Collider;

		// Token: 0x040003EE RID: 1006
		internal Collider2D m_OtherCollider;
	}
}
