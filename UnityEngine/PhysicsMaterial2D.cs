﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200017A RID: 378
	public sealed class PhysicsMaterial2D : Object
	{
		// Token: 0x06001B42 RID: 6978 RVA: 0x000218C8 File Offset: 0x0001FAC8
		public PhysicsMaterial2D()
		{
			PhysicsMaterial2D.Internal_Create(this, null);
		}

		// Token: 0x06001B43 RID: 6979 RVA: 0x000218D8 File Offset: 0x0001FAD8
		public PhysicsMaterial2D(string name)
		{
			PhysicsMaterial2D.Internal_Create(this, name);
		}

		// Token: 0x06001B44 RID: 6980
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] PhysicsMaterial2D mat, string name);

		// Token: 0x1700066A RID: 1642
		// (get) Token: 0x06001B45 RID: 6981
		// (set) Token: 0x06001B46 RID: 6982
		public extern float bounciness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700066B RID: 1643
		// (get) Token: 0x06001B47 RID: 6983
		// (set) Token: 0x06001B48 RID: 6984
		public extern float friction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
