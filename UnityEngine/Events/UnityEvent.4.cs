﻿using System;
using System.Reflection;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200038E RID: 910
	[Serializable]
	public abstract class UnityEvent<T0, T1, T2> : UnityEventBase
	{
		// Token: 0x06002F00 RID: 12032 RVA: 0x0004C0B4 File Offset: 0x0004A2B4
		[RequiredByNativeCode]
		public UnityEvent()
		{
		}

		// Token: 0x06002F01 RID: 12033 RVA: 0x0004C0CC File Offset: 0x0004A2CC
		public void AddListener(UnityAction<T0, T1, T2> call)
		{
			base.AddCall(UnityEvent<T0, T1, T2>.GetDelegate(call));
		}

		// Token: 0x06002F02 RID: 12034 RVA: 0x0004C0DC File Offset: 0x0004A2DC
		public void RemoveListener(UnityAction<T0, T1, T2> call)
		{
			base.RemoveListener(call.Target, call.GetMethodInfo());
		}

		// Token: 0x06002F03 RID: 12035 RVA: 0x0004C0F4 File Offset: 0x0004A2F4
		protected override MethodInfo FindMethod_Impl(string name, object targetObj)
		{
			return UnityEventBase.GetValidMethodInfo(targetObj, name, new Type[]
			{
				typeof(T0),
				typeof(T1),
				typeof(T2)
			});
		}

		// Token: 0x06002F04 RID: 12036 RVA: 0x0004C140 File Offset: 0x0004A340
		internal override BaseInvokableCall GetDelegate(object target, MethodInfo theFunction)
		{
			return new InvokableCall<T0, T1, T2>(target, theFunction);
		}

		// Token: 0x06002F05 RID: 12037 RVA: 0x0004C15C File Offset: 0x0004A35C
		private static BaseInvokableCall GetDelegate(UnityAction<T0, T1, T2> action)
		{
			return new InvokableCall<T0, T1, T2>(action);
		}

		// Token: 0x06002F06 RID: 12038 RVA: 0x0004C178 File Offset: 0x0004A378
		public void Invoke(T0 arg0, T1 arg1, T2 arg2)
		{
			this.m_InvokeArray[0] = arg0;
			this.m_InvokeArray[1] = arg1;
			this.m_InvokeArray[2] = arg2;
			base.Invoke(this.m_InvokeArray);
		}

		// Token: 0x04000D85 RID: 3461
		private readonly object[] m_InvokeArray = new object[3];
	}
}
