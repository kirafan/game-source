﻿using System;
using System.Reflection;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x02000390 RID: 912
	[Serializable]
	public abstract class UnityEvent<T0, T1, T2, T3> : UnityEventBase
	{
		// Token: 0x06002F0B RID: 12043 RVA: 0x0004C1B4 File Offset: 0x0004A3B4
		[RequiredByNativeCode]
		public UnityEvent()
		{
		}

		// Token: 0x06002F0C RID: 12044 RVA: 0x0004C1CC File Offset: 0x0004A3CC
		public void AddListener(UnityAction<T0, T1, T2, T3> call)
		{
			base.AddCall(UnityEvent<T0, T1, T2, T3>.GetDelegate(call));
		}

		// Token: 0x06002F0D RID: 12045 RVA: 0x0004C1DC File Offset: 0x0004A3DC
		public void RemoveListener(UnityAction<T0, T1, T2, T3> call)
		{
			base.RemoveListener(call.Target, call.GetMethodInfo());
		}

		// Token: 0x06002F0E RID: 12046 RVA: 0x0004C1F4 File Offset: 0x0004A3F4
		protected override MethodInfo FindMethod_Impl(string name, object targetObj)
		{
			return UnityEventBase.GetValidMethodInfo(targetObj, name, new Type[]
			{
				typeof(T0),
				typeof(T1),
				typeof(T2),
				typeof(T3)
			});
		}

		// Token: 0x06002F0F RID: 12047 RVA: 0x0004C24C File Offset: 0x0004A44C
		internal override BaseInvokableCall GetDelegate(object target, MethodInfo theFunction)
		{
			return new InvokableCall<T0, T1, T2, T3>(target, theFunction);
		}

		// Token: 0x06002F10 RID: 12048 RVA: 0x0004C268 File Offset: 0x0004A468
		private static BaseInvokableCall GetDelegate(UnityAction<T0, T1, T2, T3> action)
		{
			return new InvokableCall<T0, T1, T2, T3>(action);
		}

		// Token: 0x06002F11 RID: 12049 RVA: 0x0004C284 File Offset: 0x0004A484
		public void Invoke(T0 arg0, T1 arg1, T2 arg2, T3 arg3)
		{
			this.m_InvokeArray[0] = arg0;
			this.m_InvokeArray[1] = arg1;
			this.m_InvokeArray[2] = arg2;
			this.m_InvokeArray[3] = arg3;
			base.Invoke(this.m_InvokeArray);
		}

		// Token: 0x04000D86 RID: 3462
		private readonly object[] m_InvokeArray = new object[4];
	}
}
