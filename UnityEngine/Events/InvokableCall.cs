﻿using System;
using System.Reflection;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200037C RID: 892
	internal class InvokableCall : BaseInvokableCall
	{
		// Token: 0x06002E80 RID: 11904 RVA: 0x0004ABA0 File Offset: 0x00048DA0
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate += (UnityAction)theFunction.CreateDelegate(typeof(UnityAction), target);
		}

		// Token: 0x06002E81 RID: 11905 RVA: 0x0004ABC8 File Offset: 0x00048DC8
		public InvokableCall(UnityAction action)
		{
			this.Delegate += action;
		}

		// Token: 0x1400000F RID: 15
		// (add) Token: 0x06002E82 RID: 11906 RVA: 0x0004ABD8 File Offset: 0x00048DD8
		// (remove) Token: 0x06002E83 RID: 11907 RVA: 0x0004AC10 File Offset: 0x00048E10
		private event UnityAction Delegate;

		// Token: 0x06002E84 RID: 11908 RVA: 0x0004AC48 File Offset: 0x00048E48
		public override void Invoke(object[] args)
		{
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate();
			}
		}

		// Token: 0x06002E85 RID: 11909 RVA: 0x0004AC68 File Offset: 0x00048E68
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}
	}
}
