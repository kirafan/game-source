﻿using System;
using System.Reflection;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x02000380 RID: 896
	internal class InvokableCall<T1, T2, T3, T4> : BaseInvokableCall
	{
		// Token: 0x06002E98 RID: 11928 RVA: 0x0004B070 File Offset: 0x00049270
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate = (UnityAction<T1, T2, T3, T4>)theFunction.CreateDelegate(typeof(UnityAction<T1, T2, T3, T4>), target);
		}

		// Token: 0x06002E99 RID: 11929 RVA: 0x0004B098 File Offset: 0x00049298
		public InvokableCall(UnityAction<T1, T2, T3, T4> action)
		{
			this.Delegate += action;
		}

		// Token: 0x14000013 RID: 19
		// (add) Token: 0x06002E9A RID: 11930 RVA: 0x0004B0A8 File Offset: 0x000492A8
		// (remove) Token: 0x06002E9B RID: 11931 RVA: 0x0004B0E0 File Offset: 0x000492E0
		protected event UnityAction<T1, T2, T3, T4> Delegate;

		// Token: 0x06002E9C RID: 11932 RVA: 0x0004B118 File Offset: 0x00049318
		public override void Invoke(object[] args)
		{
			if (args.Length != 4)
			{
				throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 1");
			}
			BaseInvokableCall.ThrowOnInvalidArg<T1>(args[0]);
			BaseInvokableCall.ThrowOnInvalidArg<T2>(args[1]);
			BaseInvokableCall.ThrowOnInvalidArg<T3>(args[2]);
			BaseInvokableCall.ThrowOnInvalidArg<T4>(args[3]);
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate((T1)((object)args[0]), (T2)((object)args[1]), (T3)((object)args[2]), (T4)((object)args[3]));
			}
		}

		// Token: 0x06002E9D RID: 11933 RVA: 0x0004B198 File Offset: 0x00049398
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}
	}
}
