﻿using System;
using System.Reflection;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200037D RID: 893
	internal class InvokableCall<T1> : BaseInvokableCall
	{
		// Token: 0x06002E86 RID: 11910 RVA: 0x0004ACA4 File Offset: 0x00048EA4
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate += (UnityAction<T1>)theFunction.CreateDelegate(typeof(UnityAction<T1>), target);
		}

		// Token: 0x06002E87 RID: 11911 RVA: 0x0004ACCC File Offset: 0x00048ECC
		public InvokableCall(UnityAction<T1> action)
		{
			this.Delegate += action;
		}

		// Token: 0x14000010 RID: 16
		// (add) Token: 0x06002E88 RID: 11912 RVA: 0x0004ACDC File Offset: 0x00048EDC
		// (remove) Token: 0x06002E89 RID: 11913 RVA: 0x0004AD14 File Offset: 0x00048F14
		protected event UnityAction<T1> Delegate;

		// Token: 0x06002E8A RID: 11914 RVA: 0x0004AD4C File Offset: 0x00048F4C
		public override void Invoke(object[] args)
		{
			if (args.Length != 1)
			{
				throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 1");
			}
			BaseInvokableCall.ThrowOnInvalidArg<T1>(args[0]);
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate((T1)((object)args[0]));
			}
		}

		// Token: 0x06002E8B RID: 11915 RVA: 0x0004AD9C File Offset: 0x00048F9C
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}
	}
}
