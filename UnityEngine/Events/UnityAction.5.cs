﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x0200038F RID: 911
	// (Invoke) Token: 0x06002F08 RID: 12040
	public delegate void UnityAction<T0, T1, T2, T3>(T0 arg0, T1 arg1, T2 arg2, T3 arg3);
}
