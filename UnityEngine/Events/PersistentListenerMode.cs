﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x02000379 RID: 889
	[Serializable]
	public enum PersistentListenerMode
	{
		// Token: 0x04000D5D RID: 3421
		EventDefined,
		// Token: 0x04000D5E RID: 3422
		Void,
		// Token: 0x04000D5F RID: 3423
		Object,
		// Token: 0x04000D60 RID: 3424
		Int,
		// Token: 0x04000D61 RID: 3425
		Float,
		// Token: 0x04000D62 RID: 3426
		String,
		// Token: 0x04000D63 RID: 3427
		Bool
	}
}
