﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x02000389 RID: 905
	// (Invoke) Token: 0x06002EE7 RID: 12007
	public delegate void UnityAction<T0>(T0 arg0);
}
