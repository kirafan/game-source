﻿using System;
using System.Reflection;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200037F RID: 895
	internal class InvokableCall<T1, T2, T3> : BaseInvokableCall
	{
		// Token: 0x06002E92 RID: 11922 RVA: 0x0004AF1C File Offset: 0x0004911C
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate = (UnityAction<T1, T2, T3>)theFunction.CreateDelegate(typeof(UnityAction<T1, T2, T3>), target);
		}

		// Token: 0x06002E93 RID: 11923 RVA: 0x0004AF44 File Offset: 0x00049144
		public InvokableCall(UnityAction<T1, T2, T3> action)
		{
			this.Delegate += action;
		}

		// Token: 0x14000012 RID: 18
		// (add) Token: 0x06002E94 RID: 11924 RVA: 0x0004AF54 File Offset: 0x00049154
		// (remove) Token: 0x06002E95 RID: 11925 RVA: 0x0004AF8C File Offset: 0x0004918C
		protected event UnityAction<T1, T2, T3> Delegate;

		// Token: 0x06002E96 RID: 11926 RVA: 0x0004AFC4 File Offset: 0x000491C4
		public override void Invoke(object[] args)
		{
			if (args.Length != 3)
			{
				throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 1");
			}
			BaseInvokableCall.ThrowOnInvalidArg<T1>(args[0]);
			BaseInvokableCall.ThrowOnInvalidArg<T2>(args[1]);
			BaseInvokableCall.ThrowOnInvalidArg<T3>(args[2]);
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate((T1)((object)args[0]), (T2)((object)args[1]), (T3)((object)args[2]));
			}
		}

		// Token: 0x06002E97 RID: 11927 RVA: 0x0004B034 File Offset: 0x00049234
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}
	}
}
