﻿using System;
using System.Reflection;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200038C RID: 908
	[Serializable]
	public abstract class UnityEvent<T0, T1> : UnityEventBase
	{
		// Token: 0x06002EF5 RID: 12021 RVA: 0x0004BFD4 File Offset: 0x0004A1D4
		[RequiredByNativeCode]
		public UnityEvent()
		{
		}

		// Token: 0x06002EF6 RID: 12022 RVA: 0x0004BFEC File Offset: 0x0004A1EC
		public void AddListener(UnityAction<T0, T1> call)
		{
			base.AddCall(UnityEvent<T0, T1>.GetDelegate(call));
		}

		// Token: 0x06002EF7 RID: 12023 RVA: 0x0004BFFC File Offset: 0x0004A1FC
		public void RemoveListener(UnityAction<T0, T1> call)
		{
			base.RemoveListener(call.Target, call.GetMethodInfo());
		}

		// Token: 0x06002EF8 RID: 12024 RVA: 0x0004C014 File Offset: 0x0004A214
		protected override MethodInfo FindMethod_Impl(string name, object targetObj)
		{
			return UnityEventBase.GetValidMethodInfo(targetObj, name, new Type[]
			{
				typeof(T0),
				typeof(T1)
			});
		}

		// Token: 0x06002EF9 RID: 12025 RVA: 0x0004C050 File Offset: 0x0004A250
		internal override BaseInvokableCall GetDelegate(object target, MethodInfo theFunction)
		{
			return new InvokableCall<T0, T1>(target, theFunction);
		}

		// Token: 0x06002EFA RID: 12026 RVA: 0x0004C06C File Offset: 0x0004A26C
		private static BaseInvokableCall GetDelegate(UnityAction<T0, T1> action)
		{
			return new InvokableCall<T0, T1>(action);
		}

		// Token: 0x06002EFB RID: 12027 RVA: 0x0004C088 File Offset: 0x0004A288
		public void Invoke(T0 arg0, T1 arg1)
		{
			this.m_InvokeArray[0] = arg0;
			this.m_InvokeArray[1] = arg1;
			base.Invoke(this.m_InvokeArray);
		}

		// Token: 0x04000D84 RID: 3460
		private readonly object[] m_InvokeArray = new object[2];
	}
}
