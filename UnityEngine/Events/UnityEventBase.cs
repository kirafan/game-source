﻿using System;
using System.Reflection;
using UnityEngine.Serialization;

namespace UnityEngine.Events
{
	// Token: 0x02000386 RID: 902
	[Serializable]
	public abstract class UnityEventBase : ISerializationCallbackReceiver
	{
		// Token: 0x06002EC7 RID: 11975 RVA: 0x0004BA30 File Offset: 0x00049C30
		protected UnityEventBase()
		{
			this.m_Calls = new InvokableCallList();
			this.m_PersistentCalls = new PersistentCallGroup();
			this.m_TypeName = base.GetType().AssemblyQualifiedName;
		}

		// Token: 0x06002EC8 RID: 11976 RVA: 0x0004BA68 File Offset: 0x00049C68
		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
		}

		// Token: 0x06002EC9 RID: 11977 RVA: 0x0004BA6C File Offset: 0x00049C6C
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			this.DirtyPersistentCalls();
			this.m_TypeName = base.GetType().AssemblyQualifiedName;
		}

		// Token: 0x06002ECA RID: 11978
		protected abstract MethodInfo FindMethod_Impl(string name, object targetObj);

		// Token: 0x06002ECB RID: 11979
		internal abstract BaseInvokableCall GetDelegate(object target, MethodInfo theFunction);

		// Token: 0x06002ECC RID: 11980 RVA: 0x0004BA88 File Offset: 0x00049C88
		internal MethodInfo FindMethod(PersistentCall call)
		{
			Type argumentType = typeof(Object);
			if (!string.IsNullOrEmpty(call.arguments.unityObjectArgumentAssemblyTypeName))
			{
				argumentType = (Type.GetType(call.arguments.unityObjectArgumentAssemblyTypeName, false) ?? typeof(Object));
			}
			return this.FindMethod(call.methodName, call.target, call.mode, argumentType);
		}

		// Token: 0x06002ECD RID: 11981 RVA: 0x0004BAFC File Offset: 0x00049CFC
		internal MethodInfo FindMethod(string name, object listener, PersistentListenerMode mode, Type argumentType)
		{
			MethodInfo result;
			switch (mode)
			{
			case PersistentListenerMode.EventDefined:
				result = this.FindMethod_Impl(name, listener);
				break;
			case PersistentListenerMode.Void:
				result = UnityEventBase.GetValidMethodInfo(listener, name, new Type[0]);
				break;
			case PersistentListenerMode.Object:
				result = UnityEventBase.GetValidMethodInfo(listener, name, new Type[]
				{
					argumentType ?? typeof(Object)
				});
				break;
			case PersistentListenerMode.Int:
				result = UnityEventBase.GetValidMethodInfo(listener, name, new Type[]
				{
					typeof(int)
				});
				break;
			case PersistentListenerMode.Float:
				result = UnityEventBase.GetValidMethodInfo(listener, name, new Type[]
				{
					typeof(float)
				});
				break;
			case PersistentListenerMode.String:
				result = UnityEventBase.GetValidMethodInfo(listener, name, new Type[]
				{
					typeof(string)
				});
				break;
			case PersistentListenerMode.Bool:
				result = UnityEventBase.GetValidMethodInfo(listener, name, new Type[]
				{
					typeof(bool)
				});
				break;
			default:
				result = null;
				break;
			}
			return result;
		}

		// Token: 0x06002ECE RID: 11982 RVA: 0x0004BC04 File Offset: 0x00049E04
		public int GetPersistentEventCount()
		{
			return this.m_PersistentCalls.Count;
		}

		// Token: 0x06002ECF RID: 11983 RVA: 0x0004BC24 File Offset: 0x00049E24
		public Object GetPersistentTarget(int index)
		{
			PersistentCall listener = this.m_PersistentCalls.GetListener(index);
			return (listener == null) ? null : listener.target;
		}

		// Token: 0x06002ED0 RID: 11984 RVA: 0x0004BC58 File Offset: 0x00049E58
		public string GetPersistentMethodName(int index)
		{
			PersistentCall listener = this.m_PersistentCalls.GetListener(index);
			return (listener == null) ? string.Empty : listener.methodName;
		}

		// Token: 0x06002ED1 RID: 11985 RVA: 0x0004BC90 File Offset: 0x00049E90
		private void DirtyPersistentCalls()
		{
			this.m_Calls.ClearPersistent();
			this.m_CallsDirty = true;
		}

		// Token: 0x06002ED2 RID: 11986 RVA: 0x0004BCA8 File Offset: 0x00049EA8
		private void RebuildPersistentCallsIfNeeded()
		{
			if (this.m_CallsDirty)
			{
				this.m_PersistentCalls.Initialize(this.m_Calls, this);
				this.m_CallsDirty = false;
			}
		}

		// Token: 0x06002ED3 RID: 11987 RVA: 0x0004BCD4 File Offset: 0x00049ED4
		public void SetPersistentListenerState(int index, UnityEventCallState state)
		{
			PersistentCall listener = this.m_PersistentCalls.GetListener(index);
			if (listener != null)
			{
				listener.callState = state;
			}
			this.DirtyPersistentCalls();
		}

		// Token: 0x06002ED4 RID: 11988 RVA: 0x0004BD04 File Offset: 0x00049F04
		protected void AddListener(object targetObj, MethodInfo method)
		{
			this.m_Calls.AddListener(this.GetDelegate(targetObj, method));
		}

		// Token: 0x06002ED5 RID: 11989 RVA: 0x0004BD1C File Offset: 0x00049F1C
		internal void AddCall(BaseInvokableCall call)
		{
			this.m_Calls.AddListener(call);
		}

		// Token: 0x06002ED6 RID: 11990 RVA: 0x0004BD2C File Offset: 0x00049F2C
		protected void RemoveListener(object targetObj, MethodInfo method)
		{
			this.m_Calls.RemoveListener(targetObj, method);
		}

		// Token: 0x06002ED7 RID: 11991 RVA: 0x0004BD3C File Offset: 0x00049F3C
		public void RemoveAllListeners()
		{
			this.m_Calls.Clear();
		}

		// Token: 0x06002ED8 RID: 11992 RVA: 0x0004BD4C File Offset: 0x00049F4C
		protected void Invoke(object[] parameters)
		{
			this.RebuildPersistentCallsIfNeeded();
			this.m_Calls.Invoke(parameters);
		}

		// Token: 0x06002ED9 RID: 11993 RVA: 0x0004BD64 File Offset: 0x00049F64
		public override string ToString()
		{
			return base.ToString() + " " + base.GetType().FullName;
		}

		// Token: 0x06002EDA RID: 11994 RVA: 0x0004BD94 File Offset: 0x00049F94
		public static MethodInfo GetValidMethodInfo(object obj, string functionName, Type[] argumentTypes)
		{
			Type type = obj.GetType();
			while (type != typeof(object) && type != null)
			{
				MethodInfo method = type.GetMethod(functionName, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, argumentTypes, null);
				if (method != null)
				{
					ParameterInfo[] parameters = method.GetParameters();
					bool flag = true;
					int num = 0;
					foreach (ParameterInfo parameterInfo in parameters)
					{
						Type type2 = argumentTypes[num];
						Type parameterType = parameterInfo.ParameterType;
						flag = (type2.IsPrimitive == parameterType.IsPrimitive);
						if (!flag)
						{
							break;
						}
						num++;
					}
					if (flag)
					{
						return method;
					}
				}
				type = type.BaseType;
			}
			return null;
		}

		// Token: 0x04000D7E RID: 3454
		private InvokableCallList m_Calls;

		// Token: 0x04000D7F RID: 3455
		[FormerlySerializedAs("m_PersistentListeners")]
		[SerializeField]
		private PersistentCallGroup m_PersistentCalls;

		// Token: 0x04000D80 RID: 3456
		[SerializeField]
		private string m_TypeName;

		// Token: 0x04000D81 RID: 3457
		private bool m_CallsDirty = true;
	}
}
