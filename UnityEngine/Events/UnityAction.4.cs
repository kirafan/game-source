﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x0200038D RID: 909
	// (Invoke) Token: 0x06002EFD RID: 12029
	public delegate void UnityAction<T0, T1, T2>(T0 arg0, T1 arg1, T2 arg2);
}
