﻿using System;
using System.Reflection;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x02000388 RID: 904
	[Serializable]
	public class UnityEvent : UnityEventBase
	{
		// Token: 0x06002EDF RID: 11999 RVA: 0x0004BE60 File Offset: 0x0004A060
		[RequiredByNativeCode]
		public UnityEvent()
		{
		}

		// Token: 0x06002EE0 RID: 12000 RVA: 0x0004BE78 File Offset: 0x0004A078
		public void AddListener(UnityAction call)
		{
			base.AddCall(UnityEvent.GetDelegate(call));
		}

		// Token: 0x06002EE1 RID: 12001 RVA: 0x0004BE88 File Offset: 0x0004A088
		public void RemoveListener(UnityAction call)
		{
			base.RemoveListener(call.Target, call.GetMethodInfo());
		}

		// Token: 0x06002EE2 RID: 12002 RVA: 0x0004BEA0 File Offset: 0x0004A0A0
		protected override MethodInfo FindMethod_Impl(string name, object targetObj)
		{
			return UnityEventBase.GetValidMethodInfo(targetObj, name, new Type[0]);
		}

		// Token: 0x06002EE3 RID: 12003 RVA: 0x0004BEC4 File Offset: 0x0004A0C4
		internal override BaseInvokableCall GetDelegate(object target, MethodInfo theFunction)
		{
			return new InvokableCall(target, theFunction);
		}

		// Token: 0x06002EE4 RID: 12004 RVA: 0x0004BEE0 File Offset: 0x0004A0E0
		private static BaseInvokableCall GetDelegate(UnityAction action)
		{
			return new InvokableCall(action);
		}

		// Token: 0x06002EE5 RID: 12005 RVA: 0x0004BEFC File Offset: 0x0004A0FC
		public void Invoke()
		{
			base.Invoke(this.m_InvokeArray);
		}

		// Token: 0x04000D82 RID: 3458
		private readonly object[] m_InvokeArray = new object[0];
	}
}
