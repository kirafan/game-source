﻿using System;
using System.Reflection;

namespace UnityEngine.Events
{
	// Token: 0x02000381 RID: 897
	internal class CachedInvokableCall<T> : InvokableCall<T>
	{
		// Token: 0x06002E9E RID: 11934 RVA: 0x0004B1D4 File Offset: 0x000493D4
		public CachedInvokableCall(Object target, MethodInfo theFunction, T argument) : base(target, theFunction)
		{
			this.m_Arg1[0] = argument;
		}

		// Token: 0x06002E9F RID: 11935 RVA: 0x0004B1FC File Offset: 0x000493FC
		public override void Invoke(object[] args)
		{
			base.Invoke(this.m_Arg1);
		}

		// Token: 0x04000D6F RID: 3439
		private readonly object[] m_Arg1 = new object[1];
	}
}
