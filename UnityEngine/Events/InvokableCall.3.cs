﻿using System;
using System.Reflection;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200037E RID: 894
	internal class InvokableCall<T1, T2> : BaseInvokableCall
	{
		// Token: 0x06002E8C RID: 11916 RVA: 0x0004ADD8 File Offset: 0x00048FD8
		public InvokableCall(object target, MethodInfo theFunction) : base(target, theFunction)
		{
			this.Delegate = (UnityAction<T1, T2>)theFunction.CreateDelegate(typeof(UnityAction<T1, T2>), target);
		}

		// Token: 0x06002E8D RID: 11917 RVA: 0x0004AE00 File Offset: 0x00049000
		public InvokableCall(UnityAction<T1, T2> action)
		{
			this.Delegate += action;
		}

		// Token: 0x14000011 RID: 17
		// (add) Token: 0x06002E8E RID: 11918 RVA: 0x0004AE10 File Offset: 0x00049010
		// (remove) Token: 0x06002E8F RID: 11919 RVA: 0x0004AE48 File Offset: 0x00049048
		protected event UnityAction<T1, T2> Delegate;

		// Token: 0x06002E90 RID: 11920 RVA: 0x0004AE80 File Offset: 0x00049080
		public override void Invoke(object[] args)
		{
			if (args.Length != 2)
			{
				throw new ArgumentException("Passed argument 'args' is invalid size. Expected size is 1");
			}
			BaseInvokableCall.ThrowOnInvalidArg<T1>(args[0]);
			BaseInvokableCall.ThrowOnInvalidArg<T2>(args[1]);
			if (BaseInvokableCall.AllowInvoke(this.Delegate))
			{
				this.Delegate((T1)((object)args[0]), (T2)((object)args[1]));
			}
		}

		// Token: 0x06002E91 RID: 11921 RVA: 0x0004AEE0 File Offset: 0x000490E0
		public override bool Find(object targetObj, MethodInfo method)
		{
			return this.Delegate.Target == targetObj && this.Delegate.GetMethodInfo().Equals(method);
		}
	}
}
