﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x02000382 RID: 898
	public enum UnityEventCallState
	{
		// Token: 0x04000D71 RID: 3441
		Off,
		// Token: 0x04000D72 RID: 3442
		EditorAndRuntime,
		// Token: 0x04000D73 RID: 3443
		RuntimeOnly
	}
}
