﻿using System;
using System.Reflection;
using UnityEngine.Serialization;

namespace UnityEngine.Events
{
	// Token: 0x02000383 RID: 899
	[Serializable]
	internal class PersistentCall
	{
		// Token: 0x17000AFF RID: 2815
		// (get) Token: 0x06002EA1 RID: 11937 RVA: 0x0004B230 File Offset: 0x00049430
		public Object target
		{
			get
			{
				return this.m_Target;
			}
		}

		// Token: 0x17000B00 RID: 2816
		// (get) Token: 0x06002EA2 RID: 11938 RVA: 0x0004B24C File Offset: 0x0004944C
		public string methodName
		{
			get
			{
				return this.m_MethodName;
			}
		}

		// Token: 0x17000B01 RID: 2817
		// (get) Token: 0x06002EA3 RID: 11939 RVA: 0x0004B268 File Offset: 0x00049468
		// (set) Token: 0x06002EA4 RID: 11940 RVA: 0x0004B284 File Offset: 0x00049484
		public PersistentListenerMode mode
		{
			get
			{
				return this.m_Mode;
			}
			set
			{
				this.m_Mode = value;
			}
		}

		// Token: 0x17000B02 RID: 2818
		// (get) Token: 0x06002EA5 RID: 11941 RVA: 0x0004B290 File Offset: 0x00049490
		public ArgumentCache arguments
		{
			get
			{
				return this.m_Arguments;
			}
		}

		// Token: 0x17000B03 RID: 2819
		// (get) Token: 0x06002EA6 RID: 11942 RVA: 0x0004B2AC File Offset: 0x000494AC
		// (set) Token: 0x06002EA7 RID: 11943 RVA: 0x0004B2C8 File Offset: 0x000494C8
		public UnityEventCallState callState
		{
			get
			{
				return this.m_CallState;
			}
			set
			{
				this.m_CallState = value;
			}
		}

		// Token: 0x06002EA8 RID: 11944 RVA: 0x0004B2D4 File Offset: 0x000494D4
		public bool IsValid()
		{
			return this.target != null && !string.IsNullOrEmpty(this.methodName);
		}

		// Token: 0x06002EA9 RID: 11945 RVA: 0x0004B30C File Offset: 0x0004950C
		public BaseInvokableCall GetRuntimeCall(UnityEventBase theEvent)
		{
			BaseInvokableCall result;
			if (this.m_CallState == UnityEventCallState.Off || theEvent == null)
			{
				result = null;
			}
			else
			{
				MethodInfo methodInfo = theEvent.FindMethod(this);
				if (methodInfo == null)
				{
					result = null;
				}
				else
				{
					switch (this.m_Mode)
					{
					case PersistentListenerMode.EventDefined:
						result = theEvent.GetDelegate(this.target, methodInfo);
						break;
					case PersistentListenerMode.Void:
						result = new InvokableCall(this.target, methodInfo);
						break;
					case PersistentListenerMode.Object:
						result = PersistentCall.GetObjectCall(this.target, methodInfo, this.m_Arguments);
						break;
					case PersistentListenerMode.Int:
						result = new CachedInvokableCall<int>(this.target, methodInfo, this.m_Arguments.intArgument);
						break;
					case PersistentListenerMode.Float:
						result = new CachedInvokableCall<float>(this.target, methodInfo, this.m_Arguments.floatArgument);
						break;
					case PersistentListenerMode.String:
						result = new CachedInvokableCall<string>(this.target, methodInfo, this.m_Arguments.stringArgument);
						break;
					case PersistentListenerMode.Bool:
						result = new CachedInvokableCall<bool>(this.target, methodInfo, this.m_Arguments.boolArgument);
						break;
					default:
						result = null;
						break;
					}
				}
			}
			return result;
		}

		// Token: 0x06002EAA RID: 11946 RVA: 0x0004B430 File Offset: 0x00049630
		private static BaseInvokableCall GetObjectCall(Object target, MethodInfo method, ArgumentCache arguments)
		{
			Type type = typeof(Object);
			if (!string.IsNullOrEmpty(arguments.unityObjectArgumentAssemblyTypeName))
			{
				type = (Type.GetType(arguments.unityObjectArgumentAssemblyTypeName, false) ?? typeof(Object));
			}
			Type typeFromHandle = typeof(CachedInvokableCall<>);
			Type type2 = typeFromHandle.MakeGenericType(new Type[]
			{
				type
			});
			ConstructorInfo constructor = type2.GetConstructor(new Type[]
			{
				typeof(Object),
				typeof(MethodInfo),
				type
			});
			Object @object = arguments.unityObjectArgument;
			if (@object != null && !type.IsAssignableFrom(@object.GetType()))
			{
				@object = null;
			}
			return constructor.Invoke(new object[]
			{
				target,
				method,
				@object
			}) as BaseInvokableCall;
		}

		// Token: 0x06002EAB RID: 11947 RVA: 0x0004B510 File Offset: 0x00049710
		public void RegisterPersistentListener(Object ttarget, string mmethodName)
		{
			this.m_Target = ttarget;
			this.m_MethodName = mmethodName;
		}

		// Token: 0x06002EAC RID: 11948 RVA: 0x0004B524 File Offset: 0x00049724
		public void UnregisterPersistentListener()
		{
			this.m_MethodName = string.Empty;
			this.m_Target = null;
		}

		// Token: 0x04000D74 RID: 3444
		[SerializeField]
		[FormerlySerializedAs("instance")]
		private Object m_Target;

		// Token: 0x04000D75 RID: 3445
		[FormerlySerializedAs("methodName")]
		[SerializeField]
		private string m_MethodName;

		// Token: 0x04000D76 RID: 3446
		[SerializeField]
		[FormerlySerializedAs("mode")]
		private PersistentListenerMode m_Mode = PersistentListenerMode.EventDefined;

		// Token: 0x04000D77 RID: 3447
		[FormerlySerializedAs("arguments")]
		[SerializeField]
		private ArgumentCache m_Arguments = new ArgumentCache();

		// Token: 0x04000D78 RID: 3448
		[FormerlySerializedAs("enabled")]
		[FormerlySerializedAs("m_Enabled")]
		[SerializeField]
		private UnityEventCallState m_CallState = UnityEventCallState.RuntimeOnly;
	}
}
