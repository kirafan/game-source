﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x02000387 RID: 903
	// (Invoke) Token: 0x06002EDC RID: 11996
	public delegate void UnityAction();
}
