﻿using System;
using System.Reflection;

namespace UnityEngine.Events
{
	// Token: 0x0200037B RID: 891
	internal abstract class BaseInvokableCall
	{
		// Token: 0x06002E7A RID: 11898 RVA: 0x0004AADC File Offset: 0x00048CDC
		protected BaseInvokableCall()
		{
		}

		// Token: 0x06002E7B RID: 11899 RVA: 0x0004AAE8 File Offset: 0x00048CE8
		protected BaseInvokableCall(object target, MethodInfo function)
		{
			if (target == null)
			{
				throw new ArgumentNullException("target");
			}
			if (function == null)
			{
				throw new ArgumentNullException("function");
			}
		}

		// Token: 0x06002E7C RID: 11900
		public abstract void Invoke(object[] args);

		// Token: 0x06002E7D RID: 11901 RVA: 0x0004AB14 File Offset: 0x00048D14
		protected static void ThrowOnInvalidArg<T>(object arg)
		{
			if (arg != null && !(arg is T))
			{
				throw new ArgumentException(UnityString.Format("Passed argument 'args[0]' is of the wrong type. Type:{0} Expected:{1}", new object[]
				{
					arg.GetType(),
					typeof(T)
				}));
			}
		}

		// Token: 0x06002E7E RID: 11902 RVA: 0x0004AB54 File Offset: 0x00048D54
		protected static bool AllowInvoke(Delegate @delegate)
		{
			object target = @delegate.Target;
			bool result;
			if (target == null)
			{
				result = true;
			}
			else
			{
				Object @object = target as Object;
				result = (object.ReferenceEquals(@object, null) || @object != null);
			}
			return result;
		}

		// Token: 0x06002E7F RID: 11903
		public abstract bool Find(object targetObj, MethodInfo method);
	}
}
