﻿using System;

namespace UnityEngine.Events
{
	// Token: 0x0200038B RID: 907
	// (Invoke) Token: 0x06002EF2 RID: 12018
	public delegate void UnityAction<T0, T1>(T0 arg0, T1 arg1);
}
