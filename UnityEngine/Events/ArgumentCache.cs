﻿using System;
using UnityEngine.Serialization;

namespace UnityEngine.Events
{
	// Token: 0x0200037A RID: 890
	[Serializable]
	internal class ArgumentCache : ISerializationCallbackReceiver
	{
		// Token: 0x17000AF9 RID: 2809
		// (get) Token: 0x06002E6C RID: 11884 RVA: 0x0004A90C File Offset: 0x00048B0C
		// (set) Token: 0x06002E6D RID: 11885 RVA: 0x0004A928 File Offset: 0x00048B28
		public Object unityObjectArgument
		{
			get
			{
				return this.m_ObjectArgument;
			}
			set
			{
				this.m_ObjectArgument = value;
				this.m_ObjectArgumentAssemblyTypeName = ((!(value != null)) ? string.Empty : value.GetType().AssemblyQualifiedName);
			}
		}

		// Token: 0x17000AFA RID: 2810
		// (get) Token: 0x06002E6E RID: 11886 RVA: 0x0004A95C File Offset: 0x00048B5C
		public string unityObjectArgumentAssemblyTypeName
		{
			get
			{
				return this.m_ObjectArgumentAssemblyTypeName;
			}
		}

		// Token: 0x17000AFB RID: 2811
		// (get) Token: 0x06002E6F RID: 11887 RVA: 0x0004A978 File Offset: 0x00048B78
		// (set) Token: 0x06002E70 RID: 11888 RVA: 0x0004A994 File Offset: 0x00048B94
		public int intArgument
		{
			get
			{
				return this.m_IntArgument;
			}
			set
			{
				this.m_IntArgument = value;
			}
		}

		// Token: 0x17000AFC RID: 2812
		// (get) Token: 0x06002E71 RID: 11889 RVA: 0x0004A9A0 File Offset: 0x00048BA0
		// (set) Token: 0x06002E72 RID: 11890 RVA: 0x0004A9BC File Offset: 0x00048BBC
		public float floatArgument
		{
			get
			{
				return this.m_FloatArgument;
			}
			set
			{
				this.m_FloatArgument = value;
			}
		}

		// Token: 0x17000AFD RID: 2813
		// (get) Token: 0x06002E73 RID: 11891 RVA: 0x0004A9C8 File Offset: 0x00048BC8
		// (set) Token: 0x06002E74 RID: 11892 RVA: 0x0004A9E4 File Offset: 0x00048BE4
		public string stringArgument
		{
			get
			{
				return this.m_StringArgument;
			}
			set
			{
				this.m_StringArgument = value;
			}
		}

		// Token: 0x17000AFE RID: 2814
		// (get) Token: 0x06002E75 RID: 11893 RVA: 0x0004A9F0 File Offset: 0x00048BF0
		// (set) Token: 0x06002E76 RID: 11894 RVA: 0x0004AA0C File Offset: 0x00048C0C
		public bool boolArgument
		{
			get
			{
				return this.m_BoolArgument;
			}
			set
			{
				this.m_BoolArgument = value;
			}
		}

		// Token: 0x06002E77 RID: 11895 RVA: 0x0004AA18 File Offset: 0x00048C18
		private void TidyAssemblyTypeName()
		{
			if (!string.IsNullOrEmpty(this.m_ObjectArgumentAssemblyTypeName))
			{
				int num = int.MaxValue;
				int num2 = this.m_ObjectArgumentAssemblyTypeName.IndexOf(", Version=");
				if (num2 != -1)
				{
					num = Math.Min(num2, num);
				}
				num2 = this.m_ObjectArgumentAssemblyTypeName.IndexOf(", Culture=");
				if (num2 != -1)
				{
					num = Math.Min(num2, num);
				}
				num2 = this.m_ObjectArgumentAssemblyTypeName.IndexOf(", PublicKeyToken=");
				if (num2 != -1)
				{
					num = Math.Min(num2, num);
				}
				if (num != 2147483647)
				{
					this.m_ObjectArgumentAssemblyTypeName = this.m_ObjectArgumentAssemblyTypeName.Substring(0, num);
				}
			}
		}

		// Token: 0x06002E78 RID: 11896 RVA: 0x0004AAC4 File Offset: 0x00048CC4
		public void OnBeforeSerialize()
		{
			this.TidyAssemblyTypeName();
		}

		// Token: 0x06002E79 RID: 11897 RVA: 0x0004AAD0 File Offset: 0x00048CD0
		public void OnAfterDeserialize()
		{
			this.TidyAssemblyTypeName();
		}

		// Token: 0x04000D64 RID: 3428
		[SerializeField]
		[FormerlySerializedAs("objectArgument")]
		private Object m_ObjectArgument;

		// Token: 0x04000D65 RID: 3429
		[FormerlySerializedAs("objectArgumentAssemblyTypeName")]
		[SerializeField]
		private string m_ObjectArgumentAssemblyTypeName;

		// Token: 0x04000D66 RID: 3430
		[SerializeField]
		[FormerlySerializedAs("intArgument")]
		private int m_IntArgument;

		// Token: 0x04000D67 RID: 3431
		[FormerlySerializedAs("floatArgument")]
		[SerializeField]
		private float m_FloatArgument;

		// Token: 0x04000D68 RID: 3432
		[SerializeField]
		[FormerlySerializedAs("stringArgument")]
		private string m_StringArgument;

		// Token: 0x04000D69 RID: 3433
		[SerializeField]
		private bool m_BoolArgument;
	}
}
