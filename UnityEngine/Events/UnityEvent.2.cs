﻿using System;
using System.Reflection;
using UnityEngine.Scripting;
using UnityEngineInternal;

namespace UnityEngine.Events
{
	// Token: 0x0200038A RID: 906
	[Serializable]
	public abstract class UnityEvent<T0> : UnityEventBase
	{
		// Token: 0x06002EEA RID: 12010 RVA: 0x0004BF0C File Offset: 0x0004A10C
		[RequiredByNativeCode]
		public UnityEvent()
		{
		}

		// Token: 0x06002EEB RID: 12011 RVA: 0x0004BF24 File Offset: 0x0004A124
		public void AddListener(UnityAction<T0> call)
		{
			base.AddCall(UnityEvent<T0>.GetDelegate(call));
		}

		// Token: 0x06002EEC RID: 12012 RVA: 0x0004BF34 File Offset: 0x0004A134
		public void RemoveListener(UnityAction<T0> call)
		{
			base.RemoveListener(call.Target, call.GetMethodInfo());
		}

		// Token: 0x06002EED RID: 12013 RVA: 0x0004BF4C File Offset: 0x0004A14C
		protected override MethodInfo FindMethod_Impl(string name, object targetObj)
		{
			return UnityEventBase.GetValidMethodInfo(targetObj, name, new Type[]
			{
				typeof(T0)
			});
		}

		// Token: 0x06002EEE RID: 12014 RVA: 0x0004BF7C File Offset: 0x0004A17C
		internal override BaseInvokableCall GetDelegate(object target, MethodInfo theFunction)
		{
			return new InvokableCall<T0>(target, theFunction);
		}

		// Token: 0x06002EEF RID: 12015 RVA: 0x0004BF98 File Offset: 0x0004A198
		private static BaseInvokableCall GetDelegate(UnityAction<T0> action)
		{
			return new InvokableCall<T0>(action);
		}

		// Token: 0x06002EF0 RID: 12016 RVA: 0x0004BFB4 File Offset: 0x0004A1B4
		public void Invoke(T0 arg0)
		{
			this.m_InvokeArray[0] = arg0;
			base.Invoke(this.m_InvokeArray);
		}

		// Token: 0x04000D83 RID: 3459
		private readonly object[] m_InvokeArray = new object[1];
	}
}
