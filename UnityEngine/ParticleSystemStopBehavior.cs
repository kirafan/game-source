﻿using System;

namespace UnityEngine
{
	// Token: 0x02000105 RID: 261
	public enum ParticleSystemStopBehavior
	{
		// Token: 0x040002D0 RID: 720
		StopEmittingAndClear,
		// Token: 0x040002D1 RID: 721
		StopEmitting
	}
}
