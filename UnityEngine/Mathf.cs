﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngineInternal;

namespace UnityEngine
{
	// Token: 0x02000087 RID: 135
	public struct Mathf
	{
		// Token: 0x0600093F RID: 2367
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int ClosestPowerOfTwo(int value);

		// Token: 0x06000940 RID: 2368
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float GammaToLinearSpace(float value);

		// Token: 0x06000941 RID: 2369
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float LinearToGammaSpace(float value);

		// Token: 0x06000942 RID: 2370
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsPowerOfTwo(int value);

		// Token: 0x06000943 RID: 2371
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int NextPowerOfTwo(int value);

		// Token: 0x06000944 RID: 2372
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float PerlinNoise(float x, float y);

		// Token: 0x06000945 RID: 2373
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern ushort FloatToHalf(float val);

		// Token: 0x06000946 RID: 2374
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float HalfToFloat(ushort val);

		// Token: 0x06000947 RID: 2375 RVA: 0x0000D5EC File Offset: 0x0000B7EC
		public static float Sin(float f)
		{
			return (float)Math.Sin((double)f);
		}

		// Token: 0x06000948 RID: 2376 RVA: 0x0000D60C File Offset: 0x0000B80C
		public static float Cos(float f)
		{
			return (float)Math.Cos((double)f);
		}

		// Token: 0x06000949 RID: 2377 RVA: 0x0000D62C File Offset: 0x0000B82C
		public static float Tan(float f)
		{
			return (float)Math.Tan((double)f);
		}

		// Token: 0x0600094A RID: 2378 RVA: 0x0000D64C File Offset: 0x0000B84C
		public static float Asin(float f)
		{
			return (float)Math.Asin((double)f);
		}

		// Token: 0x0600094B RID: 2379 RVA: 0x0000D66C File Offset: 0x0000B86C
		public static float Acos(float f)
		{
			return (float)Math.Acos((double)f);
		}

		// Token: 0x0600094C RID: 2380 RVA: 0x0000D68C File Offset: 0x0000B88C
		public static float Atan(float f)
		{
			return (float)Math.Atan((double)f);
		}

		// Token: 0x0600094D RID: 2381 RVA: 0x0000D6AC File Offset: 0x0000B8AC
		public static float Atan2(float y, float x)
		{
			return (float)Math.Atan2((double)y, (double)x);
		}

		// Token: 0x0600094E RID: 2382 RVA: 0x0000D6CC File Offset: 0x0000B8CC
		public static float Sqrt(float f)
		{
			return (float)Math.Sqrt((double)f);
		}

		// Token: 0x0600094F RID: 2383 RVA: 0x0000D6EC File Offset: 0x0000B8EC
		public static float Abs(float f)
		{
			return Math.Abs(f);
		}

		// Token: 0x06000950 RID: 2384 RVA: 0x0000D708 File Offset: 0x0000B908
		public static int Abs(int value)
		{
			return Math.Abs(value);
		}

		// Token: 0x06000951 RID: 2385 RVA: 0x0000D724 File Offset: 0x0000B924
		public static float Min(float a, float b)
		{
			return (a >= b) ? b : a;
		}

		// Token: 0x06000952 RID: 2386 RVA: 0x0000D748 File Offset: 0x0000B948
		public static float Min(params float[] values)
		{
			int num = values.Length;
			float result;
			if (num == 0)
			{
				result = 0f;
			}
			else
			{
				float num2 = values[0];
				for (int i = 1; i < num; i++)
				{
					if (values[i] < num2)
					{
						num2 = values[i];
					}
				}
				result = num2;
			}
			return result;
		}

		// Token: 0x06000953 RID: 2387 RVA: 0x0000D798 File Offset: 0x0000B998
		public static int Min(int a, int b)
		{
			return (a >= b) ? b : a;
		}

		// Token: 0x06000954 RID: 2388 RVA: 0x0000D7BC File Offset: 0x0000B9BC
		public static int Min(params int[] values)
		{
			int num = values.Length;
			int result;
			if (num == 0)
			{
				result = 0;
			}
			else
			{
				int num2 = values[0];
				for (int i = 1; i < num; i++)
				{
					if (values[i] < num2)
					{
						num2 = values[i];
					}
				}
				result = num2;
			}
			return result;
		}

		// Token: 0x06000955 RID: 2389 RVA: 0x0000D808 File Offset: 0x0000BA08
		public static float Max(float a, float b)
		{
			return (a <= b) ? b : a;
		}

		// Token: 0x06000956 RID: 2390 RVA: 0x0000D82C File Offset: 0x0000BA2C
		public static float Max(params float[] values)
		{
			int num = values.Length;
			float result;
			if (num == 0)
			{
				result = 0f;
			}
			else
			{
				float num2 = values[0];
				for (int i = 1; i < num; i++)
				{
					if (values[i] > num2)
					{
						num2 = values[i];
					}
				}
				result = num2;
			}
			return result;
		}

		// Token: 0x06000957 RID: 2391 RVA: 0x0000D87C File Offset: 0x0000BA7C
		public static int Max(int a, int b)
		{
			return (a <= b) ? b : a;
		}

		// Token: 0x06000958 RID: 2392 RVA: 0x0000D8A0 File Offset: 0x0000BAA0
		public static int Max(params int[] values)
		{
			int num = values.Length;
			int result;
			if (num == 0)
			{
				result = 0;
			}
			else
			{
				int num2 = values[0];
				for (int i = 1; i < num; i++)
				{
					if (values[i] > num2)
					{
						num2 = values[i];
					}
				}
				result = num2;
			}
			return result;
		}

		// Token: 0x06000959 RID: 2393 RVA: 0x0000D8EC File Offset: 0x0000BAEC
		public static float Pow(float f, float p)
		{
			return (float)Math.Pow((double)f, (double)p);
		}

		// Token: 0x0600095A RID: 2394 RVA: 0x0000D90C File Offset: 0x0000BB0C
		public static float Exp(float power)
		{
			return (float)Math.Exp((double)power);
		}

		// Token: 0x0600095B RID: 2395 RVA: 0x0000D92C File Offset: 0x0000BB2C
		public static float Log(float f, float p)
		{
			return (float)Math.Log((double)f, (double)p);
		}

		// Token: 0x0600095C RID: 2396 RVA: 0x0000D94C File Offset: 0x0000BB4C
		public static float Log(float f)
		{
			return (float)Math.Log((double)f);
		}

		// Token: 0x0600095D RID: 2397 RVA: 0x0000D96C File Offset: 0x0000BB6C
		public static float Log10(float f)
		{
			return (float)Math.Log10((double)f);
		}

		// Token: 0x0600095E RID: 2398 RVA: 0x0000D98C File Offset: 0x0000BB8C
		public static float Ceil(float f)
		{
			return (float)Math.Ceiling((double)f);
		}

		// Token: 0x0600095F RID: 2399 RVA: 0x0000D9AC File Offset: 0x0000BBAC
		public static float Floor(float f)
		{
			return (float)Math.Floor((double)f);
		}

		// Token: 0x06000960 RID: 2400 RVA: 0x0000D9CC File Offset: 0x0000BBCC
		public static float Round(float f)
		{
			return (float)Math.Round((double)f);
		}

		// Token: 0x06000961 RID: 2401 RVA: 0x0000D9EC File Offset: 0x0000BBEC
		public static int CeilToInt(float f)
		{
			return (int)Math.Ceiling((double)f);
		}

		// Token: 0x06000962 RID: 2402 RVA: 0x0000DA0C File Offset: 0x0000BC0C
		public static int FloorToInt(float f)
		{
			return (int)Math.Floor((double)f);
		}

		// Token: 0x06000963 RID: 2403 RVA: 0x0000DA2C File Offset: 0x0000BC2C
		public static int RoundToInt(float f)
		{
			return (int)Math.Round((double)f);
		}

		// Token: 0x06000964 RID: 2404 RVA: 0x0000DA4C File Offset: 0x0000BC4C
		public static float Sign(float f)
		{
			return (f < 0f) ? -1f : 1f;
		}

		// Token: 0x06000965 RID: 2405 RVA: 0x0000DA7C File Offset: 0x0000BC7C
		public static float Clamp(float value, float min, float max)
		{
			if (value < min)
			{
				value = min;
			}
			else if (value > max)
			{
				value = max;
			}
			return value;
		}

		// Token: 0x06000966 RID: 2406 RVA: 0x0000DAAC File Offset: 0x0000BCAC
		public static int Clamp(int value, int min, int max)
		{
			if (value < min)
			{
				value = min;
			}
			else if (value > max)
			{
				value = max;
			}
			return value;
		}

		// Token: 0x06000967 RID: 2407 RVA: 0x0000DADC File Offset: 0x0000BCDC
		public static float Clamp01(float value)
		{
			float result;
			if (value < 0f)
			{
				result = 0f;
			}
			else if (value > 1f)
			{
				result = 1f;
			}
			else
			{
				result = value;
			}
			return result;
		}

		// Token: 0x06000968 RID: 2408 RVA: 0x0000DB20 File Offset: 0x0000BD20
		public static float Lerp(float a, float b, float t)
		{
			return a + (b - a) * Mathf.Clamp01(t);
		}

		// Token: 0x06000969 RID: 2409 RVA: 0x0000DB44 File Offset: 0x0000BD44
		public static float LerpUnclamped(float a, float b, float t)
		{
			return a + (b - a) * t;
		}

		// Token: 0x0600096A RID: 2410 RVA: 0x0000DB60 File Offset: 0x0000BD60
		public static float LerpAngle(float a, float b, float t)
		{
			float num = Mathf.Repeat(b - a, 360f);
			if (num > 180f)
			{
				num -= 360f;
			}
			return a + num * Mathf.Clamp01(t);
		}

		// Token: 0x0600096B RID: 2411 RVA: 0x0000DBA0 File Offset: 0x0000BDA0
		public static float MoveTowards(float current, float target, float maxDelta)
		{
			float result;
			if (Mathf.Abs(target - current) <= maxDelta)
			{
				result = target;
			}
			else
			{
				result = current + Mathf.Sign(target - current) * maxDelta;
			}
			return result;
		}

		// Token: 0x0600096C RID: 2412 RVA: 0x0000DBD8 File Offset: 0x0000BDD8
		public static float MoveTowardsAngle(float current, float target, float maxDelta)
		{
			float num = Mathf.DeltaAngle(current, target);
			float result;
			if (-maxDelta < num && num < maxDelta)
			{
				result = target;
			}
			else
			{
				target = current + num;
				result = Mathf.MoveTowards(current, target, maxDelta);
			}
			return result;
		}

		// Token: 0x0600096D RID: 2413 RVA: 0x0000DC18 File Offset: 0x0000BE18
		public static float SmoothStep(float from, float to, float t)
		{
			t = Mathf.Clamp01(t);
			t = -2f * t * t * t + 3f * t * t;
			return to * t + from * (1f - t);
		}

		// Token: 0x0600096E RID: 2414 RVA: 0x0000DC5C File Offset: 0x0000BE5C
		public static float Gamma(float value, float absmax, float gamma)
		{
			bool flag = false;
			if (value < 0f)
			{
				flag = true;
			}
			float num = Mathf.Abs(value);
			float result;
			if (num > absmax)
			{
				result = ((!flag) ? num : (-num));
			}
			else
			{
				float num2 = Mathf.Pow(num / absmax, gamma) * absmax;
				result = ((!flag) ? num2 : (-num2));
			}
			return result;
		}

		// Token: 0x0600096F RID: 2415 RVA: 0x0000DCBC File Offset: 0x0000BEBC
		public static bool Approximately(float a, float b)
		{
			return Mathf.Abs(b - a) < Mathf.Max(1E-06f * Mathf.Max(Mathf.Abs(a), Mathf.Abs(b)), Mathf.Epsilon * 8f);
		}

		// Token: 0x06000970 RID: 2416 RVA: 0x0000DD04 File Offset: 0x0000BF04
		[ExcludeFromDocs]
		public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime, float maxSpeed)
		{
			float deltaTime = Time.deltaTime;
			return Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		// Token: 0x06000971 RID: 2417 RVA: 0x0000DD2C File Offset: 0x0000BF2C
		[ExcludeFromDocs]
		public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime)
		{
			float deltaTime = Time.deltaTime;
			float positiveInfinity = float.PositiveInfinity;
			return Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, positiveInfinity, deltaTime);
		}

		// Token: 0x06000972 RID: 2418 RVA: 0x0000DD58 File Offset: 0x0000BF58
		public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime, [DefaultValue("Mathf.Infinity")] float maxSpeed, [DefaultValue("Time.deltaTime")] float deltaTime)
		{
			smoothTime = Mathf.Max(0.0001f, smoothTime);
			float num = 2f / smoothTime;
			float num2 = num * deltaTime;
			float num3 = 1f / (1f + num2 + 0.48f * num2 * num2 + 0.235f * num2 * num2 * num2);
			float num4 = current - target;
			float num5 = target;
			float num6 = maxSpeed * smoothTime;
			num4 = Mathf.Clamp(num4, -num6, num6);
			target = current - num4;
			float num7 = (currentVelocity + num * num4) * deltaTime;
			currentVelocity = (currentVelocity - num * num7) * num3;
			float num8 = target + (num4 + num7) * num3;
			if (num5 - current > 0f == num8 > num5)
			{
				num8 = num5;
				currentVelocity = (num8 - num5) / deltaTime;
			}
			return num8;
		}

		// Token: 0x06000973 RID: 2419 RVA: 0x0000DE14 File Offset: 0x0000C014
		[ExcludeFromDocs]
		public static float SmoothDampAngle(float current, float target, ref float currentVelocity, float smoothTime, float maxSpeed)
		{
			float deltaTime = Time.deltaTime;
			return Mathf.SmoothDampAngle(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		// Token: 0x06000974 RID: 2420 RVA: 0x0000DE3C File Offset: 0x0000C03C
		[ExcludeFromDocs]
		public static float SmoothDampAngle(float current, float target, ref float currentVelocity, float smoothTime)
		{
			float deltaTime = Time.deltaTime;
			float positiveInfinity = float.PositiveInfinity;
			return Mathf.SmoothDampAngle(current, target, ref currentVelocity, smoothTime, positiveInfinity, deltaTime);
		}

		// Token: 0x06000975 RID: 2421 RVA: 0x0000DE68 File Offset: 0x0000C068
		public static float SmoothDampAngle(float current, float target, ref float currentVelocity, float smoothTime, [DefaultValue("Mathf.Infinity")] float maxSpeed, [DefaultValue("Time.deltaTime")] float deltaTime)
		{
			target = current + Mathf.DeltaAngle(current, target);
			return Mathf.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		// Token: 0x06000976 RID: 2422 RVA: 0x0000DE98 File Offset: 0x0000C098
		public static float Repeat(float t, float length)
		{
			return t - Mathf.Floor(t / length) * length;
		}

		// Token: 0x06000977 RID: 2423 RVA: 0x0000DEBC File Offset: 0x0000C0BC
		public static float PingPong(float t, float length)
		{
			t = Mathf.Repeat(t, length * 2f);
			return length - Mathf.Abs(t - length);
		}

		// Token: 0x06000978 RID: 2424 RVA: 0x0000DEEC File Offset: 0x0000C0EC
		public static float InverseLerp(float a, float b, float value)
		{
			float result;
			if (a != b)
			{
				result = Mathf.Clamp01((value - a) / (b - a));
			}
			else
			{
				result = 0f;
			}
			return result;
		}

		// Token: 0x06000979 RID: 2425 RVA: 0x0000DF20 File Offset: 0x0000C120
		public static float DeltaAngle(float current, float target)
		{
			float num = Mathf.Repeat(target - current, 360f);
			if (num > 180f)
			{
				num -= 360f;
			}
			return num;
		}

		// Token: 0x0600097A RID: 2426 RVA: 0x0000DF58 File Offset: 0x0000C158
		internal static bool LineIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, ref Vector2 result)
		{
			float num = p2.x - p1.x;
			float num2 = p2.y - p1.y;
			float num3 = p4.x - p3.x;
			float num4 = p4.y - p3.y;
			float num5 = num * num4 - num2 * num3;
			bool result2;
			if (num5 == 0f)
			{
				result2 = false;
			}
			else
			{
				float num6 = p3.x - p1.x;
				float num7 = p3.y - p1.y;
				float num8 = (num6 * num4 - num7 * num3) / num5;
				result = new Vector2(p1.x + num8 * num, p1.y + num8 * num2);
				result2 = true;
			}
			return result2;
		}

		// Token: 0x0600097B RID: 2427 RVA: 0x0000E020 File Offset: 0x0000C220
		internal static bool LineSegmentIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector2 p4, ref Vector2 result)
		{
			float num = p2.x - p1.x;
			float num2 = p2.y - p1.y;
			float num3 = p4.x - p3.x;
			float num4 = p4.y - p3.y;
			float num5 = num * num4 - num2 * num3;
			bool result2;
			if (num5 == 0f)
			{
				result2 = false;
			}
			else
			{
				float num6 = p3.x - p1.x;
				float num7 = p3.y - p1.y;
				float num8 = (num6 * num4 - num7 * num3) / num5;
				if (num8 < 0f || num8 > 1f)
				{
					result2 = false;
				}
				else
				{
					float num9 = (num6 * num2 - num7 * num) / num5;
					if (num9 < 0f || num9 > 1f)
					{
						result2 = false;
					}
					else
					{
						result = new Vector2(p1.x + num8 * num, p1.y + num8 * num2);
						result2 = true;
					}
				}
			}
			return result2;
		}

		// Token: 0x0600097C RID: 2428 RVA: 0x0000E138 File Offset: 0x0000C338
		internal static long RandomToLong(Random r)
		{
			byte[] array = new byte[8];
			r.NextBytes(array);
			return (long)(BitConverter.ToUInt64(array, 0) & 9223372036854775807UL);
		}

		// Token: 0x04000114 RID: 276
		public const float PI = 3.1415927f;

		// Token: 0x04000115 RID: 277
		public const float Infinity = float.PositiveInfinity;

		// Token: 0x04000116 RID: 278
		public const float NegativeInfinity = float.NegativeInfinity;

		// Token: 0x04000117 RID: 279
		public const float Deg2Rad = 0.017453292f;

		// Token: 0x04000118 RID: 280
		public const float Rad2Deg = 57.29578f;

		// Token: 0x04000119 RID: 281
		public static readonly float Epsilon = (!MathfInternal.IsFlushToZeroEnabled) ? MathfInternal.FloatMinDenormal : MathfInternal.FloatMinNormal;
	}
}
