﻿using System;

namespace UnityEngine
{
	// Token: 0x02000056 RID: 86
	public enum MotionVectorGenerationMode
	{
		// Token: 0x0400008B RID: 139
		Camera,
		// Token: 0x0400008C RID: 140
		Object,
		// Token: 0x0400008D RID: 141
		ForceNoMotion
	}
}
