﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000D1 RID: 209
	public sealed class SparseTexture : Texture
	{
		// Token: 0x06000E50 RID: 3664 RVA: 0x00013900 File Offset: 0x00011B00
		public SparseTexture(int width, int height, TextureFormat format, int mipCount)
		{
			SparseTexture.Internal_Create(this, width, height, format, mipCount, false);
		}

		// Token: 0x06000E51 RID: 3665 RVA: 0x00013918 File Offset: 0x00011B18
		public SparseTexture(int width, int height, TextureFormat format, int mipCount, bool linear)
		{
			SparseTexture.Internal_Create(this, width, height, format, mipCount, linear);
		}

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x06000E52 RID: 3666
		public extern int tileWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000312 RID: 786
		// (get) Token: 0x06000E53 RID: 3667
		public extern int tileHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000313 RID: 787
		// (get) Token: 0x06000E54 RID: 3668
		public extern bool isCreated { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000E55 RID: 3669
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] SparseTexture mono, int width, int height, TextureFormat format, int mipCount, bool linear);

		// Token: 0x06000E56 RID: 3670
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UpdateTile(int tileX, int tileY, int miplevel, Color32[] data);

		// Token: 0x06000E57 RID: 3671
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UpdateTileRaw(int tileX, int tileY, int miplevel, byte[] data);

		// Token: 0x06000E58 RID: 3672
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void UnloadTile(int tileX, int tileY, int miplevel);
	}
}
