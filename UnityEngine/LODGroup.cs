﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000082 RID: 130
	public sealed class LODGroup : Component
	{
		// Token: 0x1700020F RID: 527
		// (get) Token: 0x0600085E RID: 2142 RVA: 0x0000A278 File Offset: 0x00008478
		// (set) Token: 0x0600085F RID: 2143 RVA: 0x0000A298 File Offset: 0x00008498
		public Vector3 localReferencePoint
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_localReferencePoint(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localReferencePoint(ref value);
			}
		}

		// Token: 0x06000860 RID: 2144
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localReferencePoint(out Vector3 value);

		// Token: 0x06000861 RID: 2145
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localReferencePoint(ref Vector3 value);

		// Token: 0x17000210 RID: 528
		// (get) Token: 0x06000862 RID: 2146
		// (set) Token: 0x06000863 RID: 2147
		public extern float size { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x06000864 RID: 2148
		public extern int lodCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000865 RID: 2149
		// (set) Token: 0x06000866 RID: 2150
		public extern LODFadeMode fadeMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06000867 RID: 2151
		// (set) Token: 0x06000868 RID: 2152
		public extern bool animateCrossFading { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06000869 RID: 2153
		// (set) Token: 0x0600086A RID: 2154
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600086B RID: 2155
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RecalculateBounds();

		// Token: 0x0600086C RID: 2156
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern LOD[] GetLODs();

		// Token: 0x0600086D RID: 2157 RVA: 0x0000A2A4 File Offset: 0x000084A4
		[Obsolete("Use SetLODs instead.")]
		public void SetLODS(LOD[] lods)
		{
			this.SetLODs(lods);
		}

		// Token: 0x0600086E RID: 2158
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetLODs(LOD[] lods);

		// Token: 0x0600086F RID: 2159
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ForceLOD(int index);

		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000870 RID: 2160
		// (set) Token: 0x06000871 RID: 2161
		public static extern float crossFadeAnimationDuration { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
