﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000373 RID: 883
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false)]
	public sealed class SharedBetweenAnimatorsAttribute : Attribute
	{
	}
}
