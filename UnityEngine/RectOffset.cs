﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000062 RID: 98
	[UsedByNativeCode]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class RectOffset
	{
		// Token: 0x0600070D RID: 1805 RVA: 0x000092EC File Offset: 0x000074EC
		public RectOffset()
		{
			this.Init();
		}

		// Token: 0x0600070E RID: 1806 RVA: 0x000092FC File Offset: 0x000074FC
		internal RectOffset(GUIStyle sourceStyle, IntPtr source)
		{
			this.m_SourceStyle = sourceStyle;
			this.m_Ptr = source;
		}

		// Token: 0x0600070F RID: 1807 RVA: 0x00009314 File Offset: 0x00007514
		public RectOffset(int left, int right, int top, int bottom)
		{
			this.Init();
			this.left = left;
			this.right = right;
			this.top = top;
			this.bottom = bottom;
		}

		// Token: 0x06000710 RID: 1808
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Init();

		// Token: 0x06000711 RID: 1809
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Cleanup();

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x06000712 RID: 1810
		// (set) Token: 0x06000713 RID: 1811
		public extern int left { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000714 RID: 1812
		// (set) Token: 0x06000715 RID: 1813
		public extern int right { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x06000716 RID: 1814
		// (set) Token: 0x06000717 RID: 1815
		public extern int top { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x06000718 RID: 1816
		// (set) Token: 0x06000719 RID: 1817
		public extern int bottom { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x0600071A RID: 1818
		public extern int horizontal { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x0600071B RID: 1819
		public extern int vertical { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600071C RID: 1820 RVA: 0x00009340 File Offset: 0x00007540
		public Rect Add(Rect rect)
		{
			Rect result;
			RectOffset.INTERNAL_CALL_Add(this, ref rect, out result);
			return result;
		}

		// Token: 0x0600071D RID: 1821
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Add(RectOffset self, ref Rect rect, out Rect value);

		// Token: 0x0600071E RID: 1822 RVA: 0x00009360 File Offset: 0x00007560
		public Rect Remove(Rect rect)
		{
			Rect result;
			RectOffset.INTERNAL_CALL_Remove(this, ref rect, out result);
			return result;
		}

		// Token: 0x0600071F RID: 1823
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Remove(RectOffset self, ref Rect rect, out Rect value);

		// Token: 0x06000720 RID: 1824 RVA: 0x00009380 File Offset: 0x00007580
		~RectOffset()
		{
			if (this.m_SourceStyle == null)
			{
				this.Cleanup();
			}
		}

		// Token: 0x06000721 RID: 1825 RVA: 0x000093BC File Offset: 0x000075BC
		public override string ToString()
		{
			return UnityString.Format("RectOffset (l:{0} r:{1} t:{2} b:{3})", new object[]
			{
				this.left,
				this.right,
				this.top,
				this.bottom
			});
		}

		// Token: 0x04000094 RID: 148
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x04000095 RID: 149
		private readonly GUIStyle m_SourceStyle;
	}
}
