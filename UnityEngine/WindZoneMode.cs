﻿using System;

namespace UnityEngine
{
	// Token: 0x020000F2 RID: 242
	public enum WindZoneMode
	{
		// Token: 0x0400027D RID: 637
		Directional,
		// Token: 0x0400027E RID: 638
		Spherical
	}
}
