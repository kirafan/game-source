﻿using System;
using System.Collections;

namespace UnityEngine
{
	// Token: 0x02000019 RID: 25
	public abstract class CustomYieldInstruction : IEnumerator
	{
		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060001E8 RID: 488
		public abstract bool keepWaiting { get; }

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x00004ECC File Offset: 0x000030CC
		public object Current
		{
			get
			{
				return null;
			}
		}

		// Token: 0x060001EA RID: 490 RVA: 0x00004EE4 File Offset: 0x000030E4
		public bool MoveNext()
		{
			return this.keepWaiting;
		}

		// Token: 0x060001EB RID: 491 RVA: 0x00004F00 File Offset: 0x00003100
		public void Reset()
		{
		}
	}
}
