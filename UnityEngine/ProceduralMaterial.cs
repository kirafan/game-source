﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020000C8 RID: 200
	public sealed class ProceduralMaterial : Material
	{
		// Token: 0x06000D82 RID: 3458 RVA: 0x000130CC File Offset: 0x000112CC
		internal ProceduralMaterial() : base(null)
		{
		}

		// Token: 0x06000D83 RID: 3459
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern ProceduralPropertyDescription[] GetProceduralPropertyDescriptions();

		// Token: 0x06000D84 RID: 3460
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasProceduralProperty(string inputName);

		// Token: 0x06000D85 RID: 3461
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetProceduralBoolean(string inputName);

		// Token: 0x06000D86 RID: 3462
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsProceduralPropertyVisible(string inputName);

		// Token: 0x06000D87 RID: 3463
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetProceduralBoolean(string inputName, bool value);

		// Token: 0x06000D88 RID: 3464
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern float GetProceduralFloat(string inputName);

		// Token: 0x06000D89 RID: 3465
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetProceduralFloat(string inputName, float value);

		// Token: 0x06000D8A RID: 3466 RVA: 0x000130D8 File Offset: 0x000112D8
		public Vector4 GetProceduralVector(string inputName)
		{
			Vector4 result;
			ProceduralMaterial.INTERNAL_CALL_GetProceduralVector(this, inputName, out result);
			return result;
		}

		// Token: 0x06000D8B RID: 3467
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetProceduralVector(ProceduralMaterial self, string inputName, out Vector4 value);

		// Token: 0x06000D8C RID: 3468 RVA: 0x000130F8 File Offset: 0x000112F8
		public void SetProceduralVector(string inputName, Vector4 value)
		{
			ProceduralMaterial.INTERNAL_CALL_SetProceduralVector(this, inputName, ref value);
		}

		// Token: 0x06000D8D RID: 3469
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetProceduralVector(ProceduralMaterial self, string inputName, ref Vector4 value);

		// Token: 0x06000D8E RID: 3470 RVA: 0x00013104 File Offset: 0x00011304
		public Color GetProceduralColor(string inputName)
		{
			Color result;
			ProceduralMaterial.INTERNAL_CALL_GetProceduralColor(this, inputName, out result);
			return result;
		}

		// Token: 0x06000D8F RID: 3471
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetProceduralColor(ProceduralMaterial self, string inputName, out Color value);

		// Token: 0x06000D90 RID: 3472 RVA: 0x00013124 File Offset: 0x00011324
		public void SetProceduralColor(string inputName, Color value)
		{
			ProceduralMaterial.INTERNAL_CALL_SetProceduralColor(this, inputName, ref value);
		}

		// Token: 0x06000D91 RID: 3473
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetProceduralColor(ProceduralMaterial self, string inputName, ref Color value);

		// Token: 0x06000D92 RID: 3474
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetProceduralEnum(string inputName);

		// Token: 0x06000D93 RID: 3475
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetProceduralEnum(string inputName, int value);

		// Token: 0x06000D94 RID: 3476
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Texture2D GetProceduralTexture(string inputName);

		// Token: 0x06000D95 RID: 3477
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetProceduralTexture(string inputName, Texture2D value);

		// Token: 0x06000D96 RID: 3478
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsProceduralPropertyCached(string inputName);

		// Token: 0x06000D97 RID: 3479
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void CacheProceduralProperty(string inputName, bool value);

		// Token: 0x06000D98 RID: 3480
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ClearCache();

		// Token: 0x170002EC RID: 748
		// (get) Token: 0x06000D99 RID: 3481
		// (set) Token: 0x06000D9A RID: 3482
		public extern ProceduralCacheSize cacheSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06000D9B RID: 3483
		// (set) Token: 0x06000D9C RID: 3484
		public extern int animationUpdateRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000D9D RID: 3485
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RebuildTextures();

		// Token: 0x06000D9E RID: 3486
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RebuildTexturesImmediately();

		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06000D9F RID: 3487
		public extern bool isProcessing { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000DA0 RID: 3488
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void StopRebuilds();

		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06000DA1 RID: 3489
		public extern bool isCachedDataAvailable { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06000DA2 RID: 3490
		// (set) Token: 0x06000DA3 RID: 3491
		public extern bool isLoadTimeGenerated { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06000DA4 RID: 3492
		public extern ProceduralLoadingBehavior loadingBehavior { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002F2 RID: 754
		// (get) Token: 0x06000DA5 RID: 3493
		public static extern bool isSupported { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x06000DA6 RID: 3494
		// (set) Token: 0x06000DA7 RID: 3495
		public static extern ProceduralProcessorUsage substanceProcessorUsage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06000DA8 RID: 3496
		// (set) Token: 0x06000DA9 RID: 3497
		public extern string preset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000DAA RID: 3498
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Texture[] GetGeneratedTextures();

		// Token: 0x06000DAB RID: 3499
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern ProceduralTexture GetGeneratedTexture(string textureName);

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x06000DAC RID: 3500
		// (set) Token: 0x06000DAD RID: 3501
		public extern bool isReadable { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000DAE RID: 3502
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void FreezeAndReleaseSourceData();

		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06000DAF RID: 3503
		public extern bool isFrozen { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
