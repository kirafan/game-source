﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x02000343 RID: 835
	public struct MathfInternal
	{
		// Token: 0x04000D06 RID: 3334
		public static volatile float FloatMinNormal = 1.1754944E-38f;

		// Token: 0x04000D07 RID: 3335
		public static volatile float FloatMinDenormal = float.Epsilon;

		// Token: 0x04000D08 RID: 3336
		public static bool IsFlushToZeroEnabled = MathfInternal.FloatMinDenormal == 0f;
	}
}
