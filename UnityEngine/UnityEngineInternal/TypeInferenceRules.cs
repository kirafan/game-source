﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x020003BC RID: 956
	public enum TypeInferenceRules
	{
		// Token: 0x04000DD8 RID: 3544
		TypeReferencedByFirstArgument,
		// Token: 0x04000DD9 RID: 3545
		TypeReferencedBySecondArgument,
		// Token: 0x04000DDA RID: 3546
		ArrayOfTypeReferencedByFirstArgument,
		// Token: 0x04000DDB RID: 3547
		TypeOfFirstArgument
	}
}
