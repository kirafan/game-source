﻿using System;
using UnityEngine;

namespace UnityEngineInternal
{
	// Token: 0x020003BB RID: 955
	public sealed class APIUpdaterRuntimeServices
	{
		// Token: 0x0600304F RID: 12367 RVA: 0x0004FC5C File Offset: 0x0004DE5C
		[Obsolete("Method is not meant to be used at runtime. Please, replace this call with GameObject.AddComponent<T>()/GameObject.AddComponent(Type).", true)]
		public static Component AddComponent(GameObject go, string sourceInfo, string name)
		{
			throw new Exception();
		}
	}
}
