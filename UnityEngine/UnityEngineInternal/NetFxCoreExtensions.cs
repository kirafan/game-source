﻿using System;
using System.Reflection;

namespace UnityEngineInternal
{
	// Token: 0x020003BF RID: 959
	internal static class NetFxCoreExtensions
	{
		// Token: 0x06003054 RID: 12372 RVA: 0x0004FCB0 File Offset: 0x0004DEB0
		public static Delegate CreateDelegate(this MethodInfo self, Type delegateType, object target)
		{
			return Delegate.CreateDelegate(delegateType, target, self);
		}

		// Token: 0x06003055 RID: 12373 RVA: 0x0004FCD0 File Offset: 0x0004DED0
		public static MethodInfo GetMethodInfo(this Delegate self)
		{
			return self.Method;
		}
	}
}
