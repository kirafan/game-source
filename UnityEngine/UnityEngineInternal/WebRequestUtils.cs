﻿using System;
using System.Text.RegularExpressions;
using UnityEngine.Scripting;

namespace UnityEngineInternal
{
	// Token: 0x0200024C RID: 588
	internal static class WebRequestUtils
	{
		// Token: 0x060027DC RID: 10204 RVA: 0x000396E4 File Offset: 0x000378E4
		[RequiredByNativeCode]
		internal static string RedirectTo(string baseUri, string redirectUri)
		{
			Uri uri;
			if (redirectUri[0] == '/')
			{
				uri = new Uri(redirectUri, UriKind.Relative);
			}
			else
			{
				uri = new Uri(redirectUri, UriKind.RelativeOrAbsolute);
			}
			string result;
			if (uri.IsAbsoluteUri)
			{
				result = redirectUri;
			}
			else
			{
				Uri baseUri2 = new Uri(baseUri, UriKind.Absolute);
				Uri uri2 = new Uri(baseUri2, uri);
				result = uri2.AbsoluteUri;
			}
			return result;
		}

		// Token: 0x060027DD RID: 10205 RVA: 0x00039744 File Offset: 0x00037944
		internal static string MakeInitialUrl(string targetUrl, string localUrl)
		{
			Uri uri = new Uri(localUrl);
			if (targetUrl.StartsWith("//"))
			{
				targetUrl = uri.Scheme + ":" + targetUrl;
			}
			if (targetUrl.StartsWith("/"))
			{
				targetUrl = uri.Scheme + "://" + uri.Host + targetUrl;
			}
			if (WebRequestUtils.domainRegex.IsMatch(targetUrl))
			{
				targetUrl = uri.Scheme + "://" + targetUrl;
			}
			Uri uri2 = null;
			try
			{
				uri2 = new Uri(targetUrl);
			}
			catch (FormatException ex)
			{
				try
				{
					uri2 = new Uri(uri, targetUrl);
				}
				catch (FormatException)
				{
					throw ex;
				}
			}
			return (!targetUrl.Contains("%")) ? uri2.AbsoluteUri : uri2.OriginalString;
		}

		// Token: 0x04000916 RID: 2326
		private static Regex domainRegex = new Regex("^\\s*\\w+(?:\\.\\w+)+\\s*$");
	}
}
