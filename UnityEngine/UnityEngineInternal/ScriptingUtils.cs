﻿using System;
using System.Reflection;

namespace UnityEngineInternal
{
	// Token: 0x02000346 RID: 838
	public class ScriptingUtils
	{
		// Token: 0x06002D6C RID: 11628 RVA: 0x000483A0 File Offset: 0x000465A0
		public static Delegate CreateDelegate(Type type, MethodInfo methodInfo)
		{
			return Delegate.CreateDelegate(type, methodInfo);
		}
	}
}
