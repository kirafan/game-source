﻿using System;
using System.Reflection;

namespace UnityEngineInternal
{
	// Token: 0x02000345 RID: 837
	// (Invoke) Token: 0x06002D68 RID: 11624
	public delegate MethodInfo GetMethodDelegate(Type classType, string methodName, bool searchBaseTypes, bool instanceMethod, Type[] methodParamTypes);
}
