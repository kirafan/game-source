﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x020003BD RID: 957
	[AttributeUsage(AttributeTargets.Method)]
	[Serializable]
	public class TypeInferenceRuleAttribute : Attribute
	{
		// Token: 0x06003050 RID: 12368 RVA: 0x0004FC64 File Offset: 0x0004DE64
		public TypeInferenceRuleAttribute(TypeInferenceRules rule) : this(rule.ToString())
		{
		}

		// Token: 0x06003051 RID: 12369 RVA: 0x0004FC7C File Offset: 0x0004DE7C
		public TypeInferenceRuleAttribute(string rule)
		{
			this._rule = rule;
		}

		// Token: 0x06003052 RID: 12370 RVA: 0x0004FC8C File Offset: 0x0004DE8C
		public override string ToString()
		{
			return this._rule;
		}

		// Token: 0x04000DDC RID: 3548
		private readonly string _rule;
	}
}
