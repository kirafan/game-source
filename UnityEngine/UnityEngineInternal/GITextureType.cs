﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x020000E9 RID: 233
	public enum GITextureType
	{
		// Token: 0x04000242 RID: 578
		Charting,
		// Token: 0x04000243 RID: 579
		Albedo,
		// Token: 0x04000244 RID: 580
		Emissive,
		// Token: 0x04000245 RID: 581
		Irradiance,
		// Token: 0x04000246 RID: 582
		Directionality,
		// Token: 0x04000247 RID: 583
		Baked,
		// Token: 0x04000248 RID: 584
		BakedDirectional,
		// Token: 0x04000249 RID: 585
		InputWorkspace
	}
}
