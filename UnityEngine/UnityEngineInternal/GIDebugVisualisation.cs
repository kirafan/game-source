﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngineInternal
{
	// Token: 0x020000EA RID: 234
	public sealed class GIDebugVisualisation
	{
		// Token: 0x06001046 RID: 4166
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ResetRuntimeInputTextures();

		// Token: 0x06001047 RID: 4167
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void PlayCycleMode();

		// Token: 0x06001048 RID: 4168
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void PauseCycleMode();

		// Token: 0x06001049 RID: 4169
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void StopCycleMode();

		// Token: 0x17000370 RID: 880
		// (get) Token: 0x0600104A RID: 4170
		public static extern bool cycleMode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x0600104B RID: 4171
		public static extern bool pauseCycleMode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000372 RID: 882
		// (get) Token: 0x0600104C RID: 4172
		// (set) Token: 0x0600104D RID: 4173
		public static extern GITextureType texType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600104E RID: 4174
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void CycleSkipInstances(int skip);

		// Token: 0x0600104F RID: 4175
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void CycleSkipSystems(int skip);
	}
}
