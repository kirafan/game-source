﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x02000344 RID: 836
	// (Invoke) Token: 0x06002D64 RID: 11620
	public delegate void FastCallExceptionHandler(Exception ex);
}
