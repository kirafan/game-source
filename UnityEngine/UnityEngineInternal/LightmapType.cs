﻿using System;

namespace UnityEngineInternal
{
	// Token: 0x020000E8 RID: 232
	public enum LightmapType
	{
		// Token: 0x0400023E RID: 574
		NoLightmap = -1,
		// Token: 0x0400023F RID: 575
		StaticLightmap,
		// Token: 0x04000240 RID: 576
		DynamicLightmap
	}
}
