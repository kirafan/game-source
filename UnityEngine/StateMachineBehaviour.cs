﻿using System;
using UnityEngine.Experimental.Director;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000374 RID: 884
	[RequiredByNativeCode]
	public abstract class StateMachineBehaviour : ScriptableObject
	{
		// Token: 0x06002E50 RID: 11856 RVA: 0x0004A6F8 File Offset: 0x000488F8
		public virtual void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x06002E51 RID: 11857 RVA: 0x0004A6FC File Offset: 0x000488FC
		public virtual void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x06002E52 RID: 11858 RVA: 0x0004A700 File Offset: 0x00048900
		public virtual void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x06002E53 RID: 11859 RVA: 0x0004A704 File Offset: 0x00048904
		public virtual void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x06002E54 RID: 11860 RVA: 0x0004A708 File Offset: 0x00048908
		public virtual void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
		{
		}

		// Token: 0x06002E55 RID: 11861 RVA: 0x0004A70C File Offset: 0x0004890C
		public virtual void OnStateMachineEnter(Animator animator, int stateMachinePathHash)
		{
		}

		// Token: 0x06002E56 RID: 11862 RVA: 0x0004A710 File Offset: 0x00048910
		public virtual void OnStateMachineExit(Animator animator, int stateMachinePathHash)
		{
		}

		// Token: 0x06002E57 RID: 11863 RVA: 0x0004A714 File Offset: 0x00048914
		public virtual void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06002E58 RID: 11864 RVA: 0x0004A718 File Offset: 0x00048918
		public virtual void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06002E59 RID: 11865 RVA: 0x0004A71C File Offset: 0x0004891C
		public virtual void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06002E5A RID: 11866 RVA: 0x0004A720 File Offset: 0x00048920
		public virtual void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06002E5B RID: 11867 RVA: 0x0004A724 File Offset: 0x00048924
		public virtual void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06002E5C RID: 11868 RVA: 0x0004A728 File Offset: 0x00048928
		public virtual void OnStateMachineEnter(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller)
		{
		}

		// Token: 0x06002E5D RID: 11869 RVA: 0x0004A72C File Offset: 0x0004892C
		public virtual void OnStateMachineExit(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller)
		{
		}
	}
}
