﻿using System;

namespace UnityEngine
{
	// Token: 0x020001A5 RID: 421
	public enum AudioSourceCurveType
	{
		// Token: 0x0400047A RID: 1146
		CustomRolloff,
		// Token: 0x0400047B RID: 1147
		SpatialBlend,
		// Token: 0x0400047C RID: 1148
		ReverbZoneMix,
		// Token: 0x0400047D RID: 1149
		Spread
	}
}
