﻿using System;
using System.IO;
using System.Text;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000DC RID: 220
	internal sealed class WWWTranscoder
	{
		// Token: 0x06000FA1 RID: 4001 RVA: 0x00015874 File Offset: 0x00013A74
		private static byte Hex2Byte(byte[] b, int offset)
		{
			byte b2 = 0;
			for (int i = offset; i < offset + 2; i++)
			{
				b2 *= 16;
				int num = (int)b[i];
				if (num >= 48 && num <= 57)
				{
					num -= 48;
				}
				else if (num >= 65 && num <= 75)
				{
					num -= 55;
				}
				else if (num >= 97 && num <= 102)
				{
					num -= 87;
				}
				if (num > 15)
				{
					return 63;
				}
				b2 += (byte)num;
			}
			return b2;
		}

		// Token: 0x06000FA2 RID: 4002 RVA: 0x0001590C File Offset: 0x00013B0C
		private static byte[] Byte2Hex(byte b, byte[] hexChars)
		{
			return new byte[]
			{
				hexChars[b >> 4],
				hexChars[(int)(b & 15)]
			};
		}

		// Token: 0x06000FA3 RID: 4003 RVA: 0x0001593C File Offset: 0x00013B3C
		[ExcludeFromDocs]
		public static string URLEncode(string toEncode)
		{
			Encoding utf = Encoding.UTF8;
			return WWWTranscoder.URLEncode(toEncode, utf);
		}

		// Token: 0x06000FA4 RID: 4004 RVA: 0x00015960 File Offset: 0x00013B60
		public static string URLEncode(string toEncode, [DefaultValue("Encoding.UTF8")] Encoding e)
		{
			byte[] array = WWWTranscoder.Encode(e.GetBytes(toEncode), WWWTranscoder.urlEscapeChar, WWWTranscoder.urlSpace, WWWTranscoder.urlForbidden, false);
			return WWW.DefaultEncoding.GetString(array, 0, array.Length);
		}

		// Token: 0x06000FA5 RID: 4005 RVA: 0x000159A4 File Offset: 0x00013BA4
		public static byte[] URLEncode(byte[] toEncode)
		{
			return WWWTranscoder.Encode(toEncode, WWWTranscoder.urlEscapeChar, WWWTranscoder.urlSpace, WWWTranscoder.urlForbidden, false);
		}

		// Token: 0x06000FA6 RID: 4006 RVA: 0x000159D0 File Offset: 0x00013BD0
		[ExcludeFromDocs]
		public static string QPEncode(string toEncode)
		{
			Encoding utf = Encoding.UTF8;
			return WWWTranscoder.QPEncode(toEncode, utf);
		}

		// Token: 0x06000FA7 RID: 4007 RVA: 0x000159F4 File Offset: 0x00013BF4
		public static string QPEncode(string toEncode, [DefaultValue("Encoding.UTF8")] Encoding e)
		{
			byte[] array = WWWTranscoder.Encode(e.GetBytes(toEncode), WWWTranscoder.qpEscapeChar, WWWTranscoder.qpSpace, WWWTranscoder.qpForbidden, true);
			return WWW.DefaultEncoding.GetString(array, 0, array.Length);
		}

		// Token: 0x06000FA8 RID: 4008 RVA: 0x00015A38 File Offset: 0x00013C38
		public static byte[] QPEncode(byte[] toEncode)
		{
			return WWWTranscoder.Encode(toEncode, WWWTranscoder.qpEscapeChar, WWWTranscoder.qpSpace, WWWTranscoder.qpForbidden, true);
		}

		// Token: 0x06000FA9 RID: 4009 RVA: 0x00015A64 File Offset: 0x00013C64
		public static byte[] Encode(byte[] input, byte escapeChar, byte space, byte[] forbidden, bool uppercase)
		{
			byte[] result;
			using (MemoryStream memoryStream = new MemoryStream(input.Length * 2))
			{
				for (int i = 0; i < input.Length; i++)
				{
					if (input[i] == 32)
					{
						memoryStream.WriteByte(space);
					}
					else if (input[i] < 32 || input[i] > 126 || WWWTranscoder.ByteArrayContains(forbidden, input[i]))
					{
						memoryStream.WriteByte(escapeChar);
						memoryStream.Write(WWWTranscoder.Byte2Hex(input[i], (!uppercase) ? WWWTranscoder.lcHexChars : WWWTranscoder.ucHexChars), 0, 2);
					}
					else
					{
						memoryStream.WriteByte(input[i]);
					}
				}
				result = memoryStream.ToArray();
			}
			return result;
		}

		// Token: 0x06000FAA RID: 4010 RVA: 0x00015B38 File Offset: 0x00013D38
		private static bool ByteArrayContains(byte[] array, byte b)
		{
			int num = array.Length;
			for (int i = 0; i < num; i++)
			{
				if (array[i] == b)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000FAB RID: 4011 RVA: 0x00015B78 File Offset: 0x00013D78
		[ExcludeFromDocs]
		public static string URLDecode(string toEncode)
		{
			Encoding utf = Encoding.UTF8;
			return WWWTranscoder.URLDecode(toEncode, utf);
		}

		// Token: 0x06000FAC RID: 4012 RVA: 0x00015B9C File Offset: 0x00013D9C
		public static string URLDecode(string toEncode, [DefaultValue("Encoding.UTF8")] Encoding e)
		{
			byte[] array = WWWTranscoder.Decode(WWW.DefaultEncoding.GetBytes(toEncode), WWWTranscoder.urlEscapeChar, WWWTranscoder.urlSpace);
			return e.GetString(array, 0, array.Length);
		}

		// Token: 0x06000FAD RID: 4013 RVA: 0x00015BD8 File Offset: 0x00013DD8
		public static byte[] URLDecode(byte[] toEncode)
		{
			return WWWTranscoder.Decode(toEncode, WWWTranscoder.urlEscapeChar, WWWTranscoder.urlSpace);
		}

		// Token: 0x06000FAE RID: 4014 RVA: 0x00015C00 File Offset: 0x00013E00
		[ExcludeFromDocs]
		public static string QPDecode(string toEncode)
		{
			Encoding utf = Encoding.UTF8;
			return WWWTranscoder.QPDecode(toEncode, utf);
		}

		// Token: 0x06000FAF RID: 4015 RVA: 0x00015C24 File Offset: 0x00013E24
		public static string QPDecode(string toEncode, [DefaultValue("Encoding.UTF8")] Encoding e)
		{
			byte[] array = WWWTranscoder.Decode(WWW.DefaultEncoding.GetBytes(toEncode), WWWTranscoder.qpEscapeChar, WWWTranscoder.qpSpace);
			return e.GetString(array, 0, array.Length);
		}

		// Token: 0x06000FB0 RID: 4016 RVA: 0x00015C60 File Offset: 0x00013E60
		public static byte[] QPDecode(byte[] toEncode)
		{
			return WWWTranscoder.Decode(toEncode, WWWTranscoder.qpEscapeChar, WWWTranscoder.qpSpace);
		}

		// Token: 0x06000FB1 RID: 4017 RVA: 0x00015C88 File Offset: 0x00013E88
		public static byte[] Decode(byte[] input, byte escapeChar, byte space)
		{
			byte[] result;
			using (MemoryStream memoryStream = new MemoryStream(input.Length))
			{
				for (int i = 0; i < input.Length; i++)
				{
					if (input[i] == space)
					{
						memoryStream.WriteByte(32);
					}
					else if (input[i] == escapeChar && i + 2 < input.Length)
					{
						i++;
						memoryStream.WriteByte(WWWTranscoder.Hex2Byte(input, i++));
					}
					else
					{
						memoryStream.WriteByte(input[i]);
					}
				}
				result = memoryStream.ToArray();
			}
			return result;
		}

		// Token: 0x06000FB2 RID: 4018 RVA: 0x00015D34 File Offset: 0x00013F34
		[ExcludeFromDocs]
		public static bool SevenBitClean(string s)
		{
			Encoding utf = Encoding.UTF8;
			return WWWTranscoder.SevenBitClean(s, utf);
		}

		// Token: 0x06000FB3 RID: 4019 RVA: 0x00015D58 File Offset: 0x00013F58
		public static bool SevenBitClean(string s, [DefaultValue("Encoding.UTF8")] Encoding e)
		{
			return WWWTranscoder.SevenBitClean(e.GetBytes(s));
		}

		// Token: 0x06000FB4 RID: 4020 RVA: 0x00015D7C File Offset: 0x00013F7C
		public static bool SevenBitClean(byte[] input)
		{
			for (int i = 0; i < input.Length; i++)
			{
				if (input[i] < 32 || input[i] > 126)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0400022A RID: 554
		private static byte[] ucHexChars = WWW.DefaultEncoding.GetBytes("0123456789ABCDEF");

		// Token: 0x0400022B RID: 555
		private static byte[] lcHexChars = WWW.DefaultEncoding.GetBytes("0123456789abcdef");

		// Token: 0x0400022C RID: 556
		private static byte urlEscapeChar = 37;

		// Token: 0x0400022D RID: 557
		private static byte urlSpace = 43;

		// Token: 0x0400022E RID: 558
		private static byte[] urlForbidden = WWW.DefaultEncoding.GetBytes("@&;:<>=?\"'/\\!#%+$,{}|^[]`");

		// Token: 0x0400022F RID: 559
		private static byte qpEscapeChar = 61;

		// Token: 0x04000230 RID: 560
		private static byte qpSpace = 95;

		// Token: 0x04000231 RID: 561
		private static byte[] qpForbidden = WWW.DefaultEncoding.GetBytes("&;=?\"'%+_");
	}
}
