﻿using System;

namespace UnityEngine
{
	// Token: 0x0200023E RID: 574
	internal struct SliderHandler
	{
		// Token: 0x060026F8 RID: 9976 RVA: 0x00034E88 File Offset: 0x00033088
		public SliderHandler(Rect position, float currentValue, float size, float start, float end, GUIStyle slider, GUIStyle thumb, bool horiz, int id)
		{
			this.position = position;
			this.currentValue = currentValue;
			this.size = size;
			this.start = start;
			this.end = end;
			this.slider = slider;
			this.thumb = thumb;
			this.horiz = horiz;
			this.id = id;
		}

		// Token: 0x060026F9 RID: 9977 RVA: 0x00034EDC File Offset: 0x000330DC
		public float Handle()
		{
			float result;
			if (this.slider == null || this.thumb == null)
			{
				result = this.currentValue;
			}
			else
			{
				switch (this.CurrentEventType())
				{
				case EventType.MouseDown:
					return this.OnMouseDown();
				case EventType.MouseUp:
					return this.OnMouseUp();
				case EventType.MouseDrag:
					return this.OnMouseDrag();
				case EventType.Repaint:
					return this.OnRepaint();
				}
				result = this.currentValue;
			}
			return result;
		}

		// Token: 0x060026FA RID: 9978 RVA: 0x00034F7C File Offset: 0x0003317C
		private float OnMouseDown()
		{
			float result;
			if (!this.position.Contains(this.CurrentEvent().mousePosition) || this.IsEmptySlider())
			{
				result = this.currentValue;
			}
			else
			{
				GUI.scrollTroughSide = 0;
				GUIUtility.hotControl = this.id;
				this.CurrentEvent().Use();
				if (this.ThumbSelectionRect().Contains(this.CurrentEvent().mousePosition))
				{
					this.StartDraggingWithValue(this.ClampedCurrentValue());
					result = this.currentValue;
				}
				else
				{
					GUI.changed = true;
					if (this.SupportsPageMovements())
					{
						this.SliderState().isDragging = false;
						GUI.nextScrollStepTime = SystemClock.now.AddMilliseconds(250.0);
						GUI.scrollTroughSide = this.CurrentScrollTroughSide();
						result = this.PageMovementValue();
					}
					else
					{
						float num = this.ValueForCurrentMousePosition();
						this.StartDraggingWithValue(num);
						result = this.Clamp(num);
					}
				}
			}
			return result;
		}

		// Token: 0x060026FB RID: 9979 RVA: 0x00035084 File Offset: 0x00033284
		private float OnMouseDrag()
		{
			float result;
			if (GUIUtility.hotControl != this.id)
			{
				result = this.currentValue;
			}
			else
			{
				SliderState sliderState = this.SliderState();
				if (!sliderState.isDragging)
				{
					result = this.currentValue;
				}
				else
				{
					GUI.changed = true;
					this.CurrentEvent().Use();
					float num = this.MousePosition() - sliderState.dragStartPos;
					float value = sliderState.dragStartValue + num / this.ValuesPerPixel();
					result = this.Clamp(value);
				}
			}
			return result;
		}

		// Token: 0x060026FC RID: 9980 RVA: 0x0003510C File Offset: 0x0003330C
		private float OnMouseUp()
		{
			if (GUIUtility.hotControl == this.id)
			{
				this.CurrentEvent().Use();
				GUIUtility.hotControl = 0;
			}
			return this.currentValue;
		}

		// Token: 0x060026FD RID: 9981 RVA: 0x0003514C File Offset: 0x0003334C
		private float OnRepaint()
		{
			this.slider.Draw(this.position, GUIContent.none, this.id);
			if (!this.IsEmptySlider())
			{
				this.thumb.Draw(this.ThumbRect(), GUIContent.none, this.id);
			}
			float result;
			if (GUIUtility.hotControl != this.id || !this.position.Contains(this.CurrentEvent().mousePosition) || this.IsEmptySlider())
			{
				result = this.currentValue;
			}
			else if (this.ThumbRect().Contains(this.CurrentEvent().mousePosition))
			{
				if (GUI.scrollTroughSide != 0)
				{
					GUIUtility.hotControl = 0;
				}
				result = this.currentValue;
			}
			else
			{
				GUI.InternalRepaintEditorWindow();
				if (SystemClock.now < GUI.nextScrollStepTime)
				{
					result = this.currentValue;
				}
				else if (this.CurrentScrollTroughSide() != GUI.scrollTroughSide)
				{
					result = this.currentValue;
				}
				else
				{
					GUI.nextScrollStepTime = SystemClock.now.AddMilliseconds(30.0);
					if (this.SupportsPageMovements())
					{
						this.SliderState().isDragging = false;
						GUI.changed = true;
						result = this.PageMovementValue();
					}
					else
					{
						result = this.ClampedCurrentValue();
					}
				}
			}
			return result;
		}

		// Token: 0x060026FE RID: 9982 RVA: 0x000352B4 File Offset: 0x000334B4
		private EventType CurrentEventType()
		{
			return this.CurrentEvent().GetTypeForControl(this.id);
		}

		// Token: 0x060026FF RID: 9983 RVA: 0x000352DC File Offset: 0x000334DC
		private int CurrentScrollTroughSide()
		{
			float num = (!this.horiz) ? this.CurrentEvent().mousePosition.y : this.CurrentEvent().mousePosition.x;
			float num2 = (!this.horiz) ? this.ThumbRect().y : this.ThumbRect().x;
			return (num <= num2) ? -1 : 1;
		}

		// Token: 0x06002700 RID: 9984 RVA: 0x00035368 File Offset: 0x00033568
		private bool IsEmptySlider()
		{
			return this.start == this.end;
		}

		// Token: 0x06002701 RID: 9985 RVA: 0x0003538C File Offset: 0x0003358C
		private bool SupportsPageMovements()
		{
			return this.size != 0f && GUI.usePageScrollbars;
		}

		// Token: 0x06002702 RID: 9986 RVA: 0x000353BC File Offset: 0x000335BC
		private float PageMovementValue()
		{
			float num = this.currentValue;
			int num2 = (this.start <= this.end) ? 1 : -1;
			if (this.MousePosition() > this.PageUpMovementBound())
			{
				num += this.size * (float)num2 * 0.9f;
			}
			else
			{
				num -= this.size * (float)num2 * 0.9f;
			}
			return this.Clamp(num);
		}

		// Token: 0x06002703 RID: 9987 RVA: 0x00035434 File Offset: 0x00033634
		private float PageUpMovementBound()
		{
			float result;
			if (this.horiz)
			{
				result = this.ThumbRect().xMax - this.position.x;
			}
			else
			{
				result = this.ThumbRect().yMax - this.position.y;
			}
			return result;
		}

		// Token: 0x06002704 RID: 9988 RVA: 0x00035498 File Offset: 0x00033698
		private Event CurrentEvent()
		{
			return Event.current;
		}

		// Token: 0x06002705 RID: 9989 RVA: 0x000354B4 File Offset: 0x000336B4
		private float ValueForCurrentMousePosition()
		{
			float result;
			if (this.horiz)
			{
				result = (this.MousePosition() - this.ThumbRect().width * 0.5f) / this.ValuesPerPixel() + this.start - this.size * 0.5f;
			}
			else
			{
				result = (this.MousePosition() - this.ThumbRect().height * 0.5f) / this.ValuesPerPixel() + this.start - this.size * 0.5f;
			}
			return result;
		}

		// Token: 0x06002706 RID: 9990 RVA: 0x00035548 File Offset: 0x00033748
		private float Clamp(float value)
		{
			return Mathf.Clamp(value, this.MinValue(), this.MaxValue());
		}

		// Token: 0x06002707 RID: 9991 RVA: 0x00035570 File Offset: 0x00033770
		private Rect ThumbSelectionRect()
		{
			return this.ThumbRect();
		}

		// Token: 0x06002708 RID: 9992 RVA: 0x00035590 File Offset: 0x00033790
		private void StartDraggingWithValue(float dragStartValue)
		{
			SliderState sliderState = this.SliderState();
			sliderState.dragStartPos = this.MousePosition();
			sliderState.dragStartValue = dragStartValue;
			sliderState.isDragging = true;
		}

		// Token: 0x06002709 RID: 9993 RVA: 0x000355C0 File Offset: 0x000337C0
		private SliderState SliderState()
		{
			return (SliderState)GUIUtility.GetStateObject(typeof(SliderState), this.id);
		}

		// Token: 0x0600270A RID: 9994 RVA: 0x000355F0 File Offset: 0x000337F0
		private Rect ThumbRect()
		{
			return (!this.horiz) ? this.VerticalThumbRect() : this.HorizontalThumbRect();
		}

		// Token: 0x0600270B RID: 9995 RVA: 0x00035624 File Offset: 0x00033824
		private Rect VerticalThumbRect()
		{
			float num = this.ValuesPerPixel();
			Rect result;
			if (this.start < this.end)
			{
				result = new Rect(this.position.x + (float)this.slider.padding.left, (this.ClampedCurrentValue() - this.start) * num + this.position.y + (float)this.slider.padding.top, this.position.width - (float)this.slider.padding.horizontal, this.size * num + this.ThumbSize());
			}
			else
			{
				result = new Rect(this.position.x + (float)this.slider.padding.left, (this.ClampedCurrentValue() + this.size - this.start) * num + this.position.y + (float)this.slider.padding.top, this.position.width - (float)this.slider.padding.horizontal, this.size * -num + this.ThumbSize());
			}
			return result;
		}

		// Token: 0x0600270C RID: 9996 RVA: 0x00035770 File Offset: 0x00033970
		private Rect HorizontalThumbRect()
		{
			float num = this.ValuesPerPixel();
			Rect result;
			if (this.start < this.end)
			{
				result = new Rect((this.ClampedCurrentValue() - this.start) * num + this.position.x + (float)this.slider.padding.left, this.position.y + (float)this.slider.padding.top, this.size * num + this.ThumbSize(), this.position.height - (float)this.slider.padding.vertical);
			}
			else
			{
				result = new Rect((this.ClampedCurrentValue() + this.size - this.start) * num + this.position.x + (float)this.slider.padding.left, this.position.y, this.size * -num + this.ThumbSize(), this.position.height);
			}
			return result;
		}

		// Token: 0x0600270D RID: 9997 RVA: 0x00035898 File Offset: 0x00033A98
		private float ClampedCurrentValue()
		{
			return this.Clamp(this.currentValue);
		}

		// Token: 0x0600270E RID: 9998 RVA: 0x000358BC File Offset: 0x00033ABC
		private float MousePosition()
		{
			float result;
			if (this.horiz)
			{
				result = this.CurrentEvent().mousePosition.x - this.position.x;
			}
			else
			{
				result = this.CurrentEvent().mousePosition.y - this.position.y;
			}
			return result;
		}

		// Token: 0x0600270F RID: 9999 RVA: 0x00035928 File Offset: 0x00033B28
		private float ValuesPerPixel()
		{
			float result;
			if (this.horiz)
			{
				result = (this.position.width - (float)this.slider.padding.horizontal - this.ThumbSize()) / (this.end - this.start);
			}
			else
			{
				result = (this.position.height - (float)this.slider.padding.vertical - this.ThumbSize()) / (this.end - this.start);
			}
			return result;
		}

		// Token: 0x06002710 RID: 10000 RVA: 0x000359B8 File Offset: 0x00033BB8
		private float ThumbSize()
		{
			float result;
			if (this.horiz)
			{
				result = ((this.thumb.fixedWidth == 0f) ? ((float)this.thumb.padding.horizontal) : this.thumb.fixedWidth);
			}
			else
			{
				result = ((this.thumb.fixedHeight == 0f) ? ((float)this.thumb.padding.vertical) : this.thumb.fixedHeight);
			}
			return result;
		}

		// Token: 0x06002711 RID: 10001 RVA: 0x00035A4C File Offset: 0x00033C4C
		private float MaxValue()
		{
			return Mathf.Max(this.start, this.end) - this.size;
		}

		// Token: 0x06002712 RID: 10002 RVA: 0x00035A7C File Offset: 0x00033C7C
		private float MinValue()
		{
			return Mathf.Min(this.start, this.end);
		}

		// Token: 0x04000874 RID: 2164
		private readonly Rect position;

		// Token: 0x04000875 RID: 2165
		private readonly float currentValue;

		// Token: 0x04000876 RID: 2166
		private readonly float size;

		// Token: 0x04000877 RID: 2167
		private readonly float start;

		// Token: 0x04000878 RID: 2168
		private readonly float end;

		// Token: 0x04000879 RID: 2169
		private readonly GUIStyle slider;

		// Token: 0x0400087A RID: 2170
		private readonly GUIStyle thumb;

		// Token: 0x0400087B RID: 2171
		private readonly bool horiz;

		// Token: 0x0400087C RID: 2172
		private readonly int id;
	}
}
