﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020000E5 RID: 229
	internal sealed class Playables
	{
		// Token: 0x06001016 RID: 4118
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern object CastToInternal(Type castType, IntPtr handle, int version);

		// Token: 0x06001017 RID: 4119
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Type GetTypeOfInternal(IntPtr handle, int version);

		// Token: 0x06001018 RID: 4120 RVA: 0x00016728 File Offset: 0x00014928
		internal static void InternalDestroy(ref Playable playable)
		{
			Playables.INTERNAL_CALL_InternalDestroy(ref playable);
		}

		// Token: 0x06001019 RID: 4121
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_InternalDestroy(ref Playable playable);

		// Token: 0x0600101A RID: 4122 RVA: 0x00016734 File Offset: 0x00014934
		internal static bool ConnectInternal(ref Playable source, ref Playable target, int sourceOutputPort, int targetInputPort)
		{
			return Playables.INTERNAL_CALL_ConnectInternal(ref source, ref target, sourceOutputPort, targetInputPort);
		}

		// Token: 0x0600101B RID: 4123
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_ConnectInternal(ref Playable source, ref Playable target, int sourceOutputPort, int targetInputPort);

		// Token: 0x0600101C RID: 4124 RVA: 0x00016754 File Offset: 0x00014954
		internal static void DisconnectInternal(ref Playable target, int inputPort)
		{
			Playables.INTERNAL_CALL_DisconnectInternal(ref target, inputPort);
		}

		// Token: 0x0600101D RID: 4125
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_DisconnectInternal(ref Playable target, int inputPort);

		// Token: 0x0600101E RID: 4126 RVA: 0x00016760 File Offset: 0x00014960
		internal static void SetPlayableDeleteOnDisconnect(ref Playable target, bool value)
		{
			Playables.INTERNAL_CALL_SetPlayableDeleteOnDisconnect(ref target, value);
		}

		// Token: 0x0600101F RID: 4127
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetPlayableDeleteOnDisconnect(ref Playable target, bool value);

		// Token: 0x06001020 RID: 4128
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void BeginIgnoreAllocationTracker();

		// Token: 0x06001021 RID: 4129
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void EndIgnoreAllocationTracker();

		// Token: 0x06001022 RID: 4130 RVA: 0x0001676C File Offset: 0x0001496C
		internal static bool CheckInputBounds(Playable playable, int inputIndex)
		{
			return playable.CheckInputBounds(inputIndex);
		}

		// Token: 0x06001023 RID: 4131 RVA: 0x0001678C File Offset: 0x0001498C
		internal static bool IsValid(Playable playable)
		{
			return playable.IsValid();
		}

		// Token: 0x06001024 RID: 4132 RVA: 0x000167A8 File Offset: 0x000149A8
		internal static int GetInputCountValidated(Playable playable, Type typeofPlayable)
		{
			return playable.inputCount;
		}

		// Token: 0x06001025 RID: 4133 RVA: 0x000167C4 File Offset: 0x000149C4
		internal static int GetOutputCountValidated(Playable playable, Type typeofPlayable)
		{
			return playable.outputCount;
		}

		// Token: 0x06001026 RID: 4134 RVA: 0x000167E0 File Offset: 0x000149E0
		internal static PlayState GetPlayStateValidated(Playable playable, Type typeofPlayable)
		{
			return playable.state;
		}

		// Token: 0x06001027 RID: 4135 RVA: 0x000167FC File Offset: 0x000149FC
		internal static void SetPlayStateValidated(Playable playable, PlayState playState, Type typeofPlayable)
		{
			playable.state = playState;
		}

		// Token: 0x06001028 RID: 4136 RVA: 0x00016808 File Offset: 0x00014A08
		internal static double GetTimeValidated(Playable playable, Type typeofPlayable)
		{
			return playable.time;
		}

		// Token: 0x06001029 RID: 4137 RVA: 0x00016824 File Offset: 0x00014A24
		internal static void SetTimeValidated(Playable playable, double time, Type typeofPlayable)
		{
			playable.time = time;
		}

		// Token: 0x0600102A RID: 4138 RVA: 0x00016830 File Offset: 0x00014A30
		internal static double GetDurationValidated(Playable playable, Type typeofPlayable)
		{
			return playable.duration;
		}

		// Token: 0x0600102B RID: 4139 RVA: 0x0001684C File Offset: 0x00014A4C
		internal static void SetDurationValidated(Playable playable, double duration, Type typeofPlayable)
		{
			playable.duration = duration;
		}

		// Token: 0x0600102C RID: 4140 RVA: 0x00016858 File Offset: 0x00014A58
		internal static Playable GetInputValidated(Playable playable, int inputPort, Type typeofPlayable)
		{
			return playable.GetInput(inputPort);
		}

		// Token: 0x0600102D RID: 4141 RVA: 0x00016878 File Offset: 0x00014A78
		internal static Playable GetOutputValidated(Playable playable, int outputPort, Type typeofPlayable)
		{
			return playable.GetOutput(outputPort);
		}

		// Token: 0x0600102E RID: 4142 RVA: 0x00016898 File Offset: 0x00014A98
		internal static void SetInputWeightValidated(Playable playable, int inputIndex, float weight, Type typeofPlayable)
		{
			playable.SetInputWeight(inputIndex, weight);
		}

		// Token: 0x0600102F RID: 4143 RVA: 0x000168A8 File Offset: 0x00014AA8
		internal static void SetInputWeightValidated(Playable playable, Playable input, float weight, Type typeofPlayable)
		{
			playable.SetInputWeight(input, weight);
		}

		// Token: 0x06001030 RID: 4144 RVA: 0x000168B4 File Offset: 0x00014AB4
		internal static float GetInputWeightValidated(Playable playable, int index, Type typeofPlayable)
		{
			return playable.GetInputWeight(index);
		}

		// Token: 0x06001031 RID: 4145 RVA: 0x000168D4 File Offset: 0x00014AD4
		internal static bool Equals(Playable isAPlayable, object mightBeAnythingOrNull)
		{
			return mightBeAnythingOrNull != null && isAPlayable.Equals(mightBeAnythingOrNull);
		}

		// Token: 0x06001032 RID: 4146 RVA: 0x00016900 File Offset: 0x00014B00
		internal static bool Equals(Playable lhs, Playable rhs)
		{
			return Playables.CompareVersion(lhs, rhs);
		}

		// Token: 0x06001033 RID: 4147 RVA: 0x0001691C File Offset: 0x00014B1C
		internal static bool CompareVersion(Playable lhs, Playable rhs)
		{
			return Playable.CompareVersion(lhs, rhs);
		}
	}
}
