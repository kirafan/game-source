﻿using System;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020003A7 RID: 935
	public struct FrameData
	{
		// Token: 0x17000B2B RID: 2859
		// (get) Token: 0x06002FE7 RID: 12263 RVA: 0x0004EB28 File Offset: 0x0004CD28
		public int updateId
		{
			get
			{
				return this.m_UpdateId;
			}
		}

		// Token: 0x17000B2C RID: 2860
		// (get) Token: 0x06002FE8 RID: 12264 RVA: 0x0004EB44 File Offset: 0x0004CD44
		public float time
		{
			get
			{
				return (float)this.m_Time;
			}
		}

		// Token: 0x17000B2D RID: 2861
		// (get) Token: 0x06002FE9 RID: 12265 RVA: 0x0004EB60 File Offset: 0x0004CD60
		public float lastTime
		{
			get
			{
				return (float)this.m_LastTime;
			}
		}

		// Token: 0x17000B2E RID: 2862
		// (get) Token: 0x06002FEA RID: 12266 RVA: 0x0004EB7C File Offset: 0x0004CD7C
		public float deltaTime
		{
			get
			{
				return (float)this.m_Time - (float)this.m_LastTime;
			}
		}

		// Token: 0x17000B2F RID: 2863
		// (get) Token: 0x06002FEB RID: 12267 RVA: 0x0004EBA0 File Offset: 0x0004CDA0
		public float timeScale
		{
			get
			{
				return (float)this.m_TimeScale;
			}
		}

		// Token: 0x17000B30 RID: 2864
		// (get) Token: 0x06002FEC RID: 12268 RVA: 0x0004EBBC File Offset: 0x0004CDBC
		public double dTime
		{
			get
			{
				return this.m_Time;
			}
		}

		// Token: 0x17000B31 RID: 2865
		// (get) Token: 0x06002FED RID: 12269 RVA: 0x0004EBD8 File Offset: 0x0004CDD8
		public double dLastTime
		{
			get
			{
				return this.m_LastTime;
			}
		}

		// Token: 0x17000B32 RID: 2866
		// (get) Token: 0x06002FEE RID: 12270 RVA: 0x0004EBF4 File Offset: 0x0004CDF4
		public double dDeltaTime
		{
			get
			{
				return this.m_Time - this.m_LastTime;
			}
		}

		// Token: 0x17000B33 RID: 2867
		// (get) Token: 0x06002FEF RID: 12271 RVA: 0x0004EC18 File Offset: 0x0004CE18
		public double dtimeScale
		{
			get
			{
				return this.m_TimeScale;
			}
		}

		// Token: 0x04000DB6 RID: 3510
		internal int m_UpdateId;

		// Token: 0x04000DB7 RID: 3511
		internal double m_Time;

		// Token: 0x04000DB8 RID: 3512
		internal double m_LastTime;

		// Token: 0x04000DB9 RID: 3513
		internal double m_TimeScale;
	}
}
