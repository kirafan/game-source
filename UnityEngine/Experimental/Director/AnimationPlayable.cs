﻿using System;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020001EC RID: 492
	[UsedByNativeCode]
	public struct AnimationPlayable
	{
		// Token: 0x17000829 RID: 2089
		// (get) Token: 0x0600213C RID: 8508 RVA: 0x00025FE8 File Offset: 0x000241E8
		internal Playable node
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x0600213D RID: 8509 RVA: 0x00026004 File Offset: 0x00024204
		public void Destroy()
		{
			this.node.Destroy();
		}

		// Token: 0x0600213E RID: 8510 RVA: 0x00026020 File Offset: 0x00024220
		public int AddInput(Playable input)
		{
			if (!Playable.Connect(input, this, -1, -1))
			{
				throw new InvalidOperationException("AddInput Failed. Either the connected playable is incompatible or this AnimationPlayable type doesn't support adding inputs");
			}
			return this.inputCount - 1;
		}

		// Token: 0x0600213F RID: 8511 RVA: 0x00026064 File Offset: 0x00024264
		public bool SetInput(Playable source, int index)
		{
			bool result;
			if (!this.node.CheckInputBounds(index))
			{
				result = false;
			}
			else
			{
				if (this.GetInput(index).IsValid())
				{
					Playable.Disconnect(this, index);
				}
				result = Playable.Connect(source, this, -1, index);
			}
			return result;
		}

		// Token: 0x06002140 RID: 8512 RVA: 0x000260D0 File Offset: 0x000242D0
		public bool SetInputs(IEnumerable<Playable> sources)
		{
			for (int i = 0; i < this.inputCount; i++)
			{
				Playable.Disconnect(this, i);
			}
			bool flag = false;
			int num = 0;
			foreach (Playable source in sources)
			{
				if (num < this.inputCount)
				{
					flag |= Playable.Connect(source, this, -1, num);
				}
				else
				{
					flag |= Playable.Connect(source, this, -1, -1);
				}
				this.node.SetInputWeight(num, 1f);
				num++;
			}
			for (int j = num; j < this.inputCount; j++)
			{
				this.node.SetInputWeight(j, 0f);
			}
			return flag;
		}

		// Token: 0x06002141 RID: 8513 RVA: 0x000261F0 File Offset: 0x000243F0
		public bool RemoveInput(int index)
		{
			bool result;
			if (!Playables.CheckInputBounds(this, index))
			{
				result = false;
			}
			else
			{
				Playable.Disconnect(this, index);
				result = true;
			}
			return result;
		}

		// Token: 0x06002142 RID: 8514 RVA: 0x00026234 File Offset: 0x00024434
		public bool RemoveInput(Playable playable)
		{
			for (int i = 0; i < this.inputCount; i++)
			{
				if (this.GetInput(i) == playable)
				{
					Playable.Disconnect(this, i);
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002143 RID: 8515 RVA: 0x00026290 File Offset: 0x00024490
		public bool RemoveAllInputs()
		{
			int inputCount = this.node.inputCount;
			for (int i = 0; i < inputCount; i++)
			{
				this.RemoveInput(i);
			}
			return true;
		}

		// Token: 0x1700082A RID: 2090
		// (get) Token: 0x06002144 RID: 8516 RVA: 0x000262D4 File Offset: 0x000244D4
		public static AnimationPlayable Null
		{
			get
			{
				AnimationPlayable result = default(AnimationPlayable);
				result.handle.m_Version = 10;
				return result;
			}
		}

		// Token: 0x06002145 RID: 8517 RVA: 0x00026300 File Offset: 0x00024500
		public static bool operator ==(AnimationPlayable x, Playable y)
		{
			return Playables.Equals(x, y);
		}

		// Token: 0x06002146 RID: 8518 RVA: 0x00026324 File Offset: 0x00024524
		public static bool operator !=(AnimationPlayable x, Playable y)
		{
			return !Playables.Equals(x, y);
		}

		// Token: 0x06002147 RID: 8519 RVA: 0x00026348 File Offset: 0x00024548
		public override bool Equals(object p)
		{
			return Playables.Equals(this, p);
		}

		// Token: 0x06002148 RID: 8520 RVA: 0x00026370 File Offset: 0x00024570
		public override int GetHashCode()
		{
			return this.node.GetHashCode();
		}

		// Token: 0x06002149 RID: 8521 RVA: 0x0002639C File Offset: 0x0002459C
		public static implicit operator Playable(AnimationPlayable b)
		{
			return b.node;
		}

		// Token: 0x0600214A RID: 8522 RVA: 0x000263B8 File Offset: 0x000245B8
		public bool IsValid()
		{
			return Playables.IsValid(this);
		}

		// Token: 0x0600214B RID: 8523 RVA: 0x000263E0 File Offset: 0x000245E0
		public T CastTo<T>() where T : struct
		{
			return this.handle.CastTo<T>();
		}

		// Token: 0x1700082B RID: 2091
		// (get) Token: 0x0600214C RID: 8524 RVA: 0x00026400 File Offset: 0x00024600
		public int inputCount
		{
			get
			{
				return Playables.GetInputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x0600214D RID: 8525 RVA: 0x00026438 File Offset: 0x00024638
		public Playable GetInput(int inputPort)
		{
			return Playables.GetInputValidated(this, inputPort, base.GetType());
		}

		// Token: 0x1700082C RID: 2092
		// (get) Token: 0x0600214E RID: 8526 RVA: 0x00026470 File Offset: 0x00024670
		public int outputCount
		{
			get
			{
				return Playables.GetOutputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x0600214F RID: 8527 RVA: 0x000264A8 File Offset: 0x000246A8
		public Playable GetOutput(int outputPort)
		{
			return Playables.GetOutputValidated(this, outputPort, base.GetType());
		}

		// Token: 0x06002150 RID: 8528 RVA: 0x000264E0 File Offset: 0x000246E0
		public float GetInputWeight(int index)
		{
			return Playables.GetInputWeightValidated(this, index, base.GetType());
		}

		// Token: 0x06002151 RID: 8529 RVA: 0x00026518 File Offset: 0x00024718
		public void SetInputWeight(int inputIndex, float weight)
		{
			Playables.SetInputWeightValidated(this, inputIndex, weight, base.GetType());
		}

		// Token: 0x1700082D RID: 2093
		// (get) Token: 0x06002152 RID: 8530 RVA: 0x00026540 File Offset: 0x00024740
		// (set) Token: 0x06002153 RID: 8531 RVA: 0x00026578 File Offset: 0x00024778
		public PlayState state
		{
			get
			{
				return Playables.GetPlayStateValidated(this, base.GetType());
			}
			set
			{
				Playables.SetPlayStateValidated(this, value, base.GetType());
			}
		}

		// Token: 0x1700082E RID: 2094
		// (get) Token: 0x06002154 RID: 8532 RVA: 0x0002659C File Offset: 0x0002479C
		// (set) Token: 0x06002155 RID: 8533 RVA: 0x000265D4 File Offset: 0x000247D4
		public double time
		{
			get
			{
				return Playables.GetTimeValidated(this, base.GetType());
			}
			set
			{
				Playables.SetTimeValidated(this, value, base.GetType());
			}
		}

		// Token: 0x1700082F RID: 2095
		// (get) Token: 0x06002156 RID: 8534 RVA: 0x000265F8 File Offset: 0x000247F8
		// (set) Token: 0x06002157 RID: 8535 RVA: 0x00026630 File Offset: 0x00024830
		public double duration
		{
			get
			{
				return Playables.GetDurationValidated(this, base.GetType());
			}
			set
			{
				Playables.SetDurationValidated(this, value, base.GetType());
			}
		}

		// Token: 0x040005B3 RID: 1459
		internal Playable handle;
	}
}
