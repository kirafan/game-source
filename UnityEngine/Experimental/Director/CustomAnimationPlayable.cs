﻿using System;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020001ED RID: 493
	[RequiredByNativeCode]
	public class CustomAnimationPlayable : ScriptPlayable
	{
		// Token: 0x06002158 RID: 8536 RVA: 0x00026654 File Offset: 0x00024854
		public CustomAnimationPlayable()
		{
			if (!this.handle.IsValid())
			{
				string text = base.GetType().ToString();
				throw new InvalidOperationException(string.Concat(new string[]
				{
					text,
					" must be instantiated using the Playable.Create<",
					text,
					"> method instead of new ",
					text,
					"."
				}));
			}
		}

		// Token: 0x17000830 RID: 2096
		// (get) Token: 0x06002159 RID: 8537 RVA: 0x000266BC File Offset: 0x000248BC
		internal Playable node
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x0600215A RID: 8538 RVA: 0x000266DC File Offset: 0x000248DC
		internal void SetHandle(int version, IntPtr playableHandle)
		{
			this.handle.handle.m_Handle = playableHandle;
			this.handle.handle.m_Version = version;
		}

		// Token: 0x0600215B RID: 8539 RVA: 0x00026704 File Offset: 0x00024904
		public void Destroy()
		{
			this.node.Destroy();
		}

		// Token: 0x0600215C RID: 8540 RVA: 0x00026720 File Offset: 0x00024920
		public static implicit operator Playable(CustomAnimationPlayable s)
		{
			return new Playable
			{
				m_Handle = s.node.m_Handle,
				m_Version = s.node.m_Version
			};
		}

		// Token: 0x0600215D RID: 8541 RVA: 0x00026768 File Offset: 0x00024968
		public static implicit operator AnimationPlayable(CustomAnimationPlayable s)
		{
			return s.handle;
		}

		// Token: 0x0600215E RID: 8542 RVA: 0x00026784 File Offset: 0x00024984
		public virtual void PrepareFrame(FrameData info)
		{
		}

		// Token: 0x0600215F RID: 8543 RVA: 0x00026788 File Offset: 0x00024988
		public virtual void OnSetTime(float localTime)
		{
		}

		// Token: 0x06002160 RID: 8544 RVA: 0x0002678C File Offset: 0x0002498C
		public virtual void OnSetPlayState(PlayState newState)
		{
		}

		// Token: 0x06002161 RID: 8545 RVA: 0x00026790 File Offset: 0x00024990
		public T CastTo<T>() where T : struct
		{
			return this.handle.CastTo<T>();
		}

		// Token: 0x17000831 RID: 2097
		// (get) Token: 0x06002162 RID: 8546 RVA: 0x000267B0 File Offset: 0x000249B0
		public int inputCount
		{
			get
			{
				return Playables.GetInputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x06002163 RID: 8547 RVA: 0x000267D8 File Offset: 0x000249D8
		public Playable GetInput(int inputPort)
		{
			return Playables.GetInputValidated(this, inputPort, base.GetType());
		}

		// Token: 0x17000832 RID: 2098
		// (get) Token: 0x06002164 RID: 8548 RVA: 0x00026800 File Offset: 0x00024A00
		public int outputCount
		{
			get
			{
				return Playables.GetOutputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x06002165 RID: 8549 RVA: 0x00026828 File Offset: 0x00024A28
		public Playable GetOutput(int outputPort)
		{
			return Playables.GetOutputValidated(this, outputPort, base.GetType());
		}

		// Token: 0x06002166 RID: 8550 RVA: 0x00026850 File Offset: 0x00024A50
		public float GetInputWeight(int index)
		{
			return Playables.GetInputWeightValidated(this, index, base.GetType());
		}

		// Token: 0x06002167 RID: 8551 RVA: 0x00026878 File Offset: 0x00024A78
		public void SetInputWeight(int inputIndex, float weight)
		{
			Playables.SetInputWeightValidated(this, inputIndex, weight, base.GetType());
		}

		// Token: 0x17000833 RID: 2099
		// (get) Token: 0x06002168 RID: 8552 RVA: 0x00026890 File Offset: 0x00024A90
		// (set) Token: 0x06002169 RID: 8553 RVA: 0x000268B8 File Offset: 0x00024AB8
		public PlayState state
		{
			get
			{
				return Playables.GetPlayStateValidated(this, base.GetType());
			}
			set
			{
				Playables.SetPlayStateValidated(this, value, base.GetType());
			}
		}

		// Token: 0x17000834 RID: 2100
		// (get) Token: 0x0600216A RID: 8554 RVA: 0x000268D0 File Offset: 0x00024AD0
		// (set) Token: 0x0600216B RID: 8555 RVA: 0x000268F8 File Offset: 0x00024AF8
		public double time
		{
			get
			{
				return Playables.GetTimeValidated(this, base.GetType());
			}
			set
			{
				Playables.SetTimeValidated(this, value, base.GetType());
			}
		}

		// Token: 0x17000835 RID: 2101
		// (get) Token: 0x0600216C RID: 8556 RVA: 0x00026910 File Offset: 0x00024B10
		// (set) Token: 0x0600216D RID: 8557 RVA: 0x00026938 File Offset: 0x00024B38
		public double duration
		{
			get
			{
				return Playables.GetDurationValidated(this, base.GetType());
			}
			set
			{
				Playables.SetDurationValidated(this, value, base.GetType());
			}
		}

		// Token: 0x0600216E RID: 8558 RVA: 0x00026950 File Offset: 0x00024B50
		public int AddInput(Playable input)
		{
			return AnimationPlayableUtilities.AddInputValidated(this, input, base.GetType());
		}

		// Token: 0x0600216F RID: 8559 RVA: 0x00026978 File Offset: 0x00024B78
		public bool SetInput(Playable source, int index)
		{
			return AnimationPlayableUtilities.SetInputValidated(this, source, index, base.GetType());
		}

		// Token: 0x06002170 RID: 8560 RVA: 0x000269A0 File Offset: 0x00024BA0
		public bool SetInputs(IEnumerable<Playable> sources)
		{
			return AnimationPlayableUtilities.SetInputsValidated(this, sources, base.GetType());
		}

		// Token: 0x06002171 RID: 8561 RVA: 0x000269C8 File Offset: 0x00024BC8
		public bool RemoveInput(int index)
		{
			return AnimationPlayableUtilities.RemoveInputValidated(this, index, base.GetType());
		}

		// Token: 0x06002172 RID: 8562 RVA: 0x000269F0 File Offset: 0x00024BF0
		public bool RemoveAllInputs()
		{
			return AnimationPlayableUtilities.RemoveAllInputsValidated(this, base.GetType());
		}

		// Token: 0x040005B4 RID: 1460
		internal AnimationPlayable handle;
	}
}
