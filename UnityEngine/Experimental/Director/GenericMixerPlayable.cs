﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020000E4 RID: 228
	[RequiredByNativeCode]
	public struct GenericMixerPlayable
	{
		// Token: 0x1700036C RID: 876
		// (get) Token: 0x0600100F RID: 4111 RVA: 0x00016668 File Offset: 0x00014868
		internal Playable node
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x06001010 RID: 4112 RVA: 0x00016684 File Offset: 0x00014884
		public static GenericMixerPlayable Create()
		{
			GenericMixerPlayable result = default(GenericMixerPlayable);
			GenericMixerPlayable.InternalCreate(ref result);
			return result;
		}

		// Token: 0x06001011 RID: 4113
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalCreate(ref GenericMixerPlayable playable);

		// Token: 0x06001012 RID: 4114 RVA: 0x000166AC File Offset: 0x000148AC
		public void Destroy()
		{
			this.handle.Destroy();
		}

		// Token: 0x06001013 RID: 4115 RVA: 0x000166BC File Offset: 0x000148BC
		public T CastTo<T>() where T : struct
		{
			return this.handle.CastTo<T>();
		}

		// Token: 0x06001014 RID: 4116 RVA: 0x000166DC File Offset: 0x000148DC
		public static implicit operator Playable(GenericMixerPlayable s)
		{
			return new Playable
			{
				m_Handle = s.handle.m_Handle,
				m_Version = s.handle.m_Version
			};
		}

		// Token: 0x0400023C RID: 572
		internal Playable handle;
	}
}
