﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020001EE RID: 494
	[UsedByNativeCode]
	internal struct AnimationOffsetPlayable
	{
		// Token: 0x17000836 RID: 2102
		// (get) Token: 0x06002173 RID: 8563 RVA: 0x00026A18 File Offset: 0x00024C18
		internal Playable node
		{
			get
			{
				return this.handle.node;
			}
		}

		// Token: 0x06002174 RID: 8564 RVA: 0x00026A38 File Offset: 0x00024C38
		public static AnimationOffsetPlayable Create()
		{
			AnimationOffsetPlayable result = default(AnimationOffsetPlayable);
			AnimationOffsetPlayable.InternalCreate(ref result);
			return result;
		}

		// Token: 0x06002175 RID: 8565
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalCreate(ref AnimationOffsetPlayable that);

		// Token: 0x06002176 RID: 8566 RVA: 0x00026A60 File Offset: 0x00024C60
		public void Destroy()
		{
			this.node.Destroy();
		}

		// Token: 0x17000837 RID: 2103
		// (get) Token: 0x06002177 RID: 8567 RVA: 0x00026A7C File Offset: 0x00024C7C
		public int inputCount
		{
			get
			{
				return Playables.GetInputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x06002178 RID: 8568 RVA: 0x00026AB4 File Offset: 0x00024CB4
		public Playable GetInput(int inputPort)
		{
			return Playables.GetInputValidated(this, inputPort, base.GetType());
		}

		// Token: 0x17000838 RID: 2104
		// (get) Token: 0x06002179 RID: 8569 RVA: 0x00026AEC File Offset: 0x00024CEC
		public int outputCount
		{
			get
			{
				return Playables.GetOutputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x0600217A RID: 8570 RVA: 0x00026B24 File Offset: 0x00024D24
		public Playable GetOutput(int outputPort)
		{
			return Playables.GetOutputValidated(this, outputPort, base.GetType());
		}

		// Token: 0x0600217B RID: 8571 RVA: 0x00026B5C File Offset: 0x00024D5C
		public float GetInputWeight(int index)
		{
			return Playables.GetInputWeightValidated(this, index, base.GetType());
		}

		// Token: 0x0600217C RID: 8572 RVA: 0x00026B94 File Offset: 0x00024D94
		public void SetInputWeight(int inputIndex, float weight)
		{
			Playables.SetInputWeightValidated(this, inputIndex, weight, base.GetType());
		}

		// Token: 0x17000839 RID: 2105
		// (get) Token: 0x0600217D RID: 8573 RVA: 0x00026BBC File Offset: 0x00024DBC
		// (set) Token: 0x0600217E RID: 8574 RVA: 0x00026BF4 File Offset: 0x00024DF4
		public PlayState state
		{
			get
			{
				return Playables.GetPlayStateValidated(this, base.GetType());
			}
			set
			{
				Playables.SetPlayStateValidated(this, value, base.GetType());
			}
		}

		// Token: 0x1700083A RID: 2106
		// (get) Token: 0x0600217F RID: 8575 RVA: 0x00026C18 File Offset: 0x00024E18
		// (set) Token: 0x06002180 RID: 8576 RVA: 0x00026C50 File Offset: 0x00024E50
		public double time
		{
			get
			{
				return Playables.GetTimeValidated(this, base.GetType());
			}
			set
			{
				Playables.SetTimeValidated(this, value, base.GetType());
			}
		}

		// Token: 0x1700083B RID: 2107
		// (get) Token: 0x06002181 RID: 8577 RVA: 0x00026C74 File Offset: 0x00024E74
		// (set) Token: 0x06002182 RID: 8578 RVA: 0x00026CAC File Offset: 0x00024EAC
		public double duration
		{
			get
			{
				return Playables.GetDurationValidated(this, base.GetType());
			}
			set
			{
				Playables.SetDurationValidated(this, value, base.GetType());
			}
		}

		// Token: 0x06002183 RID: 8579 RVA: 0x00026CD0 File Offset: 0x00024ED0
		public static bool operator ==(AnimationOffsetPlayable x, Playable y)
		{
			return Playables.Equals(x, y);
		}

		// Token: 0x06002184 RID: 8580 RVA: 0x00026CF4 File Offset: 0x00024EF4
		public static bool operator !=(AnimationOffsetPlayable x, Playable y)
		{
			return !Playables.Equals(x, y);
		}

		// Token: 0x06002185 RID: 8581 RVA: 0x00026D18 File Offset: 0x00024F18
		public override bool Equals(object p)
		{
			return Playables.Equals(this, p);
		}

		// Token: 0x06002186 RID: 8582 RVA: 0x00026D40 File Offset: 0x00024F40
		public override int GetHashCode()
		{
			return this.node.GetHashCode();
		}

		// Token: 0x06002187 RID: 8583 RVA: 0x00026D6C File Offset: 0x00024F6C
		public static implicit operator Playable(AnimationOffsetPlayable b)
		{
			return b.node;
		}

		// Token: 0x06002188 RID: 8584 RVA: 0x00026D88 File Offset: 0x00024F88
		public static implicit operator AnimationPlayable(AnimationOffsetPlayable b)
		{
			return b.handle;
		}

		// Token: 0x06002189 RID: 8585 RVA: 0x00026DA4 File Offset: 0x00024FA4
		public bool IsValid()
		{
			return Playables.IsValid(this);
		}

		// Token: 0x0600218A RID: 8586 RVA: 0x00026DCC File Offset: 0x00024FCC
		public T CastTo<T>() where T : struct
		{
			return this.handle.CastTo<T>();
		}

		// Token: 0x0600218B RID: 8587 RVA: 0x00026DEC File Offset: 0x00024FEC
		public int AddInput(Playable input)
		{
			return AnimationPlayableUtilities.AddInputValidated(this, input, base.GetType());
		}

		// Token: 0x0600218C RID: 8588 RVA: 0x00026E24 File Offset: 0x00025024
		public bool RemoveInput(int index)
		{
			return AnimationPlayableUtilities.RemoveInputValidated(this, index, base.GetType());
		}

		// Token: 0x0600218D RID: 8589 RVA: 0x00026E5C File Offset: 0x0002505C
		public bool RemoveAllInputs()
		{
			return AnimationPlayableUtilities.RemoveAllInputsValidated(this, base.GetType());
		}

		// Token: 0x0600218E RID: 8590 RVA: 0x00026E94 File Offset: 0x00025094
		private static Vector3 GetPosition(ref AnimationOffsetPlayable that)
		{
			Vector3 result;
			AnimationOffsetPlayable.INTERNAL_CALL_GetPosition(ref that, out result);
			return result;
		}

		// Token: 0x0600218F RID: 8591
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPosition(ref AnimationOffsetPlayable that, out Vector3 value);

		// Token: 0x06002190 RID: 8592 RVA: 0x00026EB4 File Offset: 0x000250B4
		private static void SetPosition(ref AnimationOffsetPlayable that, Vector3 value)
		{
			AnimationOffsetPlayable.INTERNAL_CALL_SetPosition(ref that, ref value);
		}

		// Token: 0x06002191 RID: 8593
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetPosition(ref AnimationOffsetPlayable that, ref Vector3 value);

		// Token: 0x06002192 RID: 8594 RVA: 0x00026EC0 File Offset: 0x000250C0
		private static Quaternion GetRotation(ref AnimationOffsetPlayable that)
		{
			Quaternion result;
			AnimationOffsetPlayable.INTERNAL_CALL_GetRotation(ref that, out result);
			return result;
		}

		// Token: 0x06002193 RID: 8595
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetRotation(ref AnimationOffsetPlayable that, out Quaternion value);

		// Token: 0x06002194 RID: 8596 RVA: 0x00026EE0 File Offset: 0x000250E0
		private static void SetRotation(ref AnimationOffsetPlayable that, Quaternion value)
		{
			AnimationOffsetPlayable.INTERNAL_CALL_SetRotation(ref that, ref value);
		}

		// Token: 0x06002195 RID: 8597
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetRotation(ref AnimationOffsetPlayable that, ref Quaternion value);

		// Token: 0x1700083C RID: 2108
		// (get) Token: 0x06002196 RID: 8598 RVA: 0x00026EEC File Offset: 0x000250EC
		// (set) Token: 0x06002197 RID: 8599 RVA: 0x00026F08 File Offset: 0x00025108
		public Vector3 position
		{
			get
			{
				return AnimationOffsetPlayable.GetPosition(ref this);
			}
			set
			{
				AnimationOffsetPlayable.SetPosition(ref this, value);
			}
		}

		// Token: 0x1700083D RID: 2109
		// (get) Token: 0x06002198 RID: 8600 RVA: 0x00026F14 File Offset: 0x00025114
		// (set) Token: 0x06002199 RID: 8601 RVA: 0x00026F30 File Offset: 0x00025130
		public Quaternion rotation
		{
			get
			{
				return AnimationOffsetPlayable.GetRotation(ref this);
			}
			set
			{
				AnimationOffsetPlayable.SetRotation(ref this, value);
			}
		}

		// Token: 0x040005B5 RID: 1461
		internal AnimationPlayable handle;
	}
}
