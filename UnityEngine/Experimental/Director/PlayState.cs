﻿using System;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020000E2 RID: 226
	public enum PlayState
	{
		// Token: 0x04000238 RID: 568
		Paused,
		// Token: 0x04000239 RID: 569
		Playing
	}
}
