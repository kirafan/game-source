﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020000E1 RID: 225
	public class DirectorPlayer : Behaviour
	{
		// Token: 0x06000FC2 RID: 4034 RVA: 0x00015FC8 File Offset: 0x000141C8
		public void Play(Playable pStruct)
		{
			this.PlayStructInternal(pStruct);
		}

		// Token: 0x06000FC3 RID: 4035 RVA: 0x00015FD4 File Offset: 0x000141D4
		private void PlayStructInternal(Playable pStruct)
		{
			DirectorPlayer.INTERNAL_CALL_PlayStructInternal(this, ref pStruct);
		}

		// Token: 0x06000FC4 RID: 4036
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_PlayStructInternal(DirectorPlayer self, ref Playable pStruct);

		// Token: 0x06000FC5 RID: 4037
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Stop();

		// Token: 0x06000FC6 RID: 4038
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTime(double time);

		// Token: 0x06000FC7 RID: 4039
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern double GetTime();

		// Token: 0x06000FC8 RID: 4040
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetTimeUpdateMode(DirectorUpdateMode mode);

		// Token: 0x06000FC9 RID: 4041
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern DirectorUpdateMode GetTimeUpdateMode();
	}
}
