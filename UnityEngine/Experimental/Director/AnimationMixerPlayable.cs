﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020001E9 RID: 489
	[UsedByNativeCode]
	public struct AnimationMixerPlayable
	{
		// Token: 0x17000814 RID: 2068
		// (get) Token: 0x060020DF RID: 8415 RVA: 0x00025254 File Offset: 0x00023454
		internal Playable node
		{
			get
			{
				return this.handle.node;
			}
		}

		// Token: 0x060020E0 RID: 8416 RVA: 0x00025274 File Offset: 0x00023474
		public static AnimationMixerPlayable Create()
		{
			AnimationMixerPlayable result = default(AnimationMixerPlayable);
			AnimationMixerPlayable.InternalCreate(ref result);
			return result;
		}

		// Token: 0x060020E1 RID: 8417
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalCreate(ref AnimationMixerPlayable that);

		// Token: 0x060020E2 RID: 8418 RVA: 0x0002529C File Offset: 0x0002349C
		public void Destroy()
		{
			this.node.Destroy();
		}

		// Token: 0x17000815 RID: 2069
		// (get) Token: 0x060020E3 RID: 8419 RVA: 0x000252B8 File Offset: 0x000234B8
		public int inputCount
		{
			get
			{
				return Playables.GetInputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x060020E4 RID: 8420 RVA: 0x000252F0 File Offset: 0x000234F0
		public Playable GetInput(int inputPort)
		{
			return Playables.GetInputValidated(this, inputPort, base.GetType());
		}

		// Token: 0x17000816 RID: 2070
		// (get) Token: 0x060020E5 RID: 8421 RVA: 0x00025328 File Offset: 0x00023528
		public int outputCount
		{
			get
			{
				return Playables.GetOutputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x060020E6 RID: 8422 RVA: 0x00025360 File Offset: 0x00023560
		public Playable GetOutput(int outputPort)
		{
			return Playables.GetOutputValidated(this, outputPort, base.GetType());
		}

		// Token: 0x060020E7 RID: 8423 RVA: 0x00025398 File Offset: 0x00023598
		public bool SetInputs(AnimationClip[] clips)
		{
			return AnimationPlayableUtilities.SetInputs(this, clips);
		}

		// Token: 0x060020E8 RID: 8424 RVA: 0x000253BC File Offset: 0x000235BC
		public float GetInputWeight(int index)
		{
			return Playables.GetInputWeightValidated(this, index, base.GetType());
		}

		// Token: 0x060020E9 RID: 8425 RVA: 0x000253F4 File Offset: 0x000235F4
		public void SetInputWeight(int inputIndex, float weight)
		{
			Playables.SetInputWeightValidated(this, inputIndex, weight, base.GetType());
		}

		// Token: 0x17000817 RID: 2071
		// (get) Token: 0x060020EA RID: 8426 RVA: 0x0002541C File Offset: 0x0002361C
		// (set) Token: 0x060020EB RID: 8427 RVA: 0x00025454 File Offset: 0x00023654
		public PlayState state
		{
			get
			{
				return Playables.GetPlayStateValidated(this, base.GetType());
			}
			set
			{
				Playables.SetPlayStateValidated(this, value, base.GetType());
			}
		}

		// Token: 0x17000818 RID: 2072
		// (get) Token: 0x060020EC RID: 8428 RVA: 0x00025478 File Offset: 0x00023678
		// (set) Token: 0x060020ED RID: 8429 RVA: 0x000254B0 File Offset: 0x000236B0
		public double time
		{
			get
			{
				return Playables.GetTimeValidated(this, base.GetType());
			}
			set
			{
				Playables.SetTimeValidated(this, value, base.GetType());
			}
		}

		// Token: 0x17000819 RID: 2073
		// (get) Token: 0x060020EE RID: 8430 RVA: 0x000254D4 File Offset: 0x000236D4
		// (set) Token: 0x060020EF RID: 8431 RVA: 0x0002550C File Offset: 0x0002370C
		public double duration
		{
			get
			{
				return Playables.GetDurationValidated(this, base.GetType());
			}
			set
			{
				Playables.SetDurationValidated(this, value, base.GetType());
			}
		}

		// Token: 0x060020F0 RID: 8432 RVA: 0x00025530 File Offset: 0x00023730
		public static bool operator ==(AnimationMixerPlayable x, Playable y)
		{
			return Playables.Equals(x, y);
		}

		// Token: 0x060020F1 RID: 8433 RVA: 0x00025554 File Offset: 0x00023754
		public static bool operator !=(AnimationMixerPlayable x, Playable y)
		{
			return !Playables.Equals(x, y);
		}

		// Token: 0x060020F2 RID: 8434 RVA: 0x00025578 File Offset: 0x00023778
		public override bool Equals(object p)
		{
			return Playables.Equals(this, p);
		}

		// Token: 0x060020F3 RID: 8435 RVA: 0x000255A0 File Offset: 0x000237A0
		public override int GetHashCode()
		{
			return this.node.GetHashCode();
		}

		// Token: 0x060020F4 RID: 8436 RVA: 0x000255CC File Offset: 0x000237CC
		public static implicit operator Playable(AnimationMixerPlayable b)
		{
			return b.node;
		}

		// Token: 0x060020F5 RID: 8437 RVA: 0x000255E8 File Offset: 0x000237E8
		public static implicit operator AnimationPlayable(AnimationMixerPlayable b)
		{
			return b.handle;
		}

		// Token: 0x060020F6 RID: 8438 RVA: 0x00025604 File Offset: 0x00023804
		public bool IsValid()
		{
			return Playables.IsValid(this);
		}

		// Token: 0x060020F7 RID: 8439 RVA: 0x0002562C File Offset: 0x0002382C
		public T CastTo<T>() where T : struct
		{
			return this.handle.CastTo<T>();
		}

		// Token: 0x060020F8 RID: 8440 RVA: 0x0002564C File Offset: 0x0002384C
		public int AddInput(Playable input)
		{
			return AnimationPlayableUtilities.AddInputValidated(this, input, base.GetType());
		}

		// Token: 0x060020F9 RID: 8441 RVA: 0x00025684 File Offset: 0x00023884
		public bool SetInput(Playable source, int index)
		{
			return AnimationPlayableUtilities.SetInputValidated(this, source, index, base.GetType());
		}

		// Token: 0x060020FA RID: 8442 RVA: 0x000256BC File Offset: 0x000238BC
		public bool SetInputs(IEnumerable<Playable> sources)
		{
			return AnimationPlayableUtilities.SetInputsValidated(this, sources, base.GetType());
		}

		// Token: 0x060020FB RID: 8443 RVA: 0x000256F4 File Offset: 0x000238F4
		public bool RemoveInput(int index)
		{
			return AnimationPlayableUtilities.RemoveInputValidated(this, index, base.GetType());
		}

		// Token: 0x060020FC RID: 8444 RVA: 0x0002572C File Offset: 0x0002392C
		public bool RemoveAllInputs()
		{
			return AnimationPlayableUtilities.RemoveAllInputsValidated(this, base.GetType());
		}

		// Token: 0x040005B0 RID: 1456
		internal AnimationPlayable handle;
	}
}
