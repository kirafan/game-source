﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020001E8 RID: 488
	[UsedByNativeCode]
	public struct AnimatorControllerPlayable
	{
		// Token: 0x1700080D RID: 2061
		// (get) Token: 0x0600205E RID: 8286 RVA: 0x00024888 File Offset: 0x00022A88
		internal Playable node
		{
			get
			{
				return this.handle.node;
			}
		}

		// Token: 0x0600205F RID: 8287 RVA: 0x000248A8 File Offset: 0x00022AA8
		public static AnimatorControllerPlayable Create(RuntimeAnimatorController controller)
		{
			AnimatorControllerPlayable result = default(AnimatorControllerPlayable);
			AnimatorControllerPlayable.InternalCreate(controller, ref result);
			return result;
		}

		// Token: 0x06002060 RID: 8288
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalCreate(RuntimeAnimatorController controller, ref AnimatorControllerPlayable that);

		// Token: 0x06002061 RID: 8289 RVA: 0x000248D0 File Offset: 0x00022AD0
		public void Destroy()
		{
			this.node.Destroy();
		}

		// Token: 0x06002062 RID: 8290 RVA: 0x000248EC File Offset: 0x00022AEC
		public static implicit operator Playable(AnimatorControllerPlayable s)
		{
			return s.node;
		}

		// Token: 0x06002063 RID: 8291 RVA: 0x00024908 File Offset: 0x00022B08
		public static implicit operator AnimationPlayable(AnimatorControllerPlayable s)
		{
			return s.handle;
		}

		// Token: 0x1700080E RID: 2062
		// (get) Token: 0x06002064 RID: 8292 RVA: 0x00024924 File Offset: 0x00022B24
		public RuntimeAnimatorController animatorController
		{
			get
			{
				return AnimatorControllerPlayable.GetAnimatorControllerInternal(ref this);
			}
		}

		// Token: 0x06002065 RID: 8293
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern RuntimeAnimatorController GetAnimatorControllerInternal(ref AnimatorControllerPlayable that);

		// Token: 0x06002066 RID: 8294 RVA: 0x00024940 File Offset: 0x00022B40
		public float GetFloat(string name)
		{
			return AnimatorControllerPlayable.GetFloatString(ref this, name);
		}

		// Token: 0x06002067 RID: 8295 RVA: 0x0002495C File Offset: 0x00022B5C
		public float GetFloat(int id)
		{
			return AnimatorControllerPlayable.GetFloatID(ref this, id);
		}

		// Token: 0x06002068 RID: 8296 RVA: 0x00024978 File Offset: 0x00022B78
		public void SetFloat(string name, float value)
		{
			AnimatorControllerPlayable.SetFloatString(ref this, name, value);
		}

		// Token: 0x06002069 RID: 8297 RVA: 0x00024984 File Offset: 0x00022B84
		public void SetFloat(int id, float value)
		{
			AnimatorControllerPlayable.SetFloatID(ref this, id, value);
		}

		// Token: 0x0600206A RID: 8298 RVA: 0x00024990 File Offset: 0x00022B90
		public bool GetBool(string name)
		{
			return AnimatorControllerPlayable.GetBoolString(ref this, name);
		}

		// Token: 0x0600206B RID: 8299 RVA: 0x000249AC File Offset: 0x00022BAC
		public bool GetBool(int id)
		{
			return AnimatorControllerPlayable.GetBoolID(ref this, id);
		}

		// Token: 0x0600206C RID: 8300 RVA: 0x000249C8 File Offset: 0x00022BC8
		public void SetBool(string name, bool value)
		{
			AnimatorControllerPlayable.SetBoolString(ref this, name, value);
		}

		// Token: 0x0600206D RID: 8301 RVA: 0x000249D4 File Offset: 0x00022BD4
		public void SetBool(int id, bool value)
		{
			AnimatorControllerPlayable.SetBoolID(ref this, id, value);
		}

		// Token: 0x0600206E RID: 8302 RVA: 0x000249E0 File Offset: 0x00022BE0
		public int GetInteger(string name)
		{
			return AnimatorControllerPlayable.GetIntegerString(ref this, name);
		}

		// Token: 0x0600206F RID: 8303 RVA: 0x000249FC File Offset: 0x00022BFC
		public int GetInteger(int id)
		{
			return AnimatorControllerPlayable.GetIntegerID(ref this, id);
		}

		// Token: 0x06002070 RID: 8304 RVA: 0x00024A18 File Offset: 0x00022C18
		public void SetInteger(string name, int value)
		{
			AnimatorControllerPlayable.SetIntegerString(ref this, name, value);
		}

		// Token: 0x06002071 RID: 8305 RVA: 0x00024A24 File Offset: 0x00022C24
		public void SetInteger(int id, int value)
		{
			AnimatorControllerPlayable.SetIntegerID(ref this, id, value);
		}

		// Token: 0x06002072 RID: 8306 RVA: 0x00024A30 File Offset: 0x00022C30
		public void SetTrigger(string name)
		{
			AnimatorControllerPlayable.SetTriggerString(ref this, name);
		}

		// Token: 0x06002073 RID: 8307 RVA: 0x00024A3C File Offset: 0x00022C3C
		public void SetTrigger(int id)
		{
			AnimatorControllerPlayable.SetTriggerID(ref this, id);
		}

		// Token: 0x06002074 RID: 8308 RVA: 0x00024A48 File Offset: 0x00022C48
		public void ResetTrigger(string name)
		{
			AnimatorControllerPlayable.ResetTriggerString(ref this, name);
		}

		// Token: 0x06002075 RID: 8309 RVA: 0x00024A54 File Offset: 0x00022C54
		public void ResetTrigger(int id)
		{
			AnimatorControllerPlayable.ResetTriggerID(ref this, id);
		}

		// Token: 0x06002076 RID: 8310 RVA: 0x00024A60 File Offset: 0x00022C60
		public bool IsParameterControlledByCurve(string name)
		{
			return AnimatorControllerPlayable.IsParameterControlledByCurveString(ref this, name);
		}

		// Token: 0x06002077 RID: 8311 RVA: 0x00024A7C File Offset: 0x00022C7C
		public bool IsParameterControlledByCurve(int id)
		{
			return AnimatorControllerPlayable.IsParameterControlledByCurveID(ref this, id);
		}

		// Token: 0x1700080F RID: 2063
		// (get) Token: 0x06002078 RID: 8312 RVA: 0x00024A98 File Offset: 0x00022C98
		public int layerCount
		{
			get
			{
				return AnimatorControllerPlayable.GetLayerCountInternal(ref this);
			}
		}

		// Token: 0x06002079 RID: 8313
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetLayerCountInternal(ref AnimatorControllerPlayable that);

		// Token: 0x0600207A RID: 8314
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetLayerNameInternal(ref AnimatorControllerPlayable that, int layerIndex);

		// Token: 0x0600207B RID: 8315 RVA: 0x00024AB4 File Offset: 0x00022CB4
		public string GetLayerName(int layerIndex)
		{
			return AnimatorControllerPlayable.GetLayerNameInternal(ref this, layerIndex);
		}

		// Token: 0x0600207C RID: 8316
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetLayerIndexInternal(ref AnimatorControllerPlayable that, string layerName);

		// Token: 0x0600207D RID: 8317 RVA: 0x00024AD0 File Offset: 0x00022CD0
		public int GetLayerIndex(string layerName)
		{
			return AnimatorControllerPlayable.GetLayerIndexInternal(ref this, layerName);
		}

		// Token: 0x0600207E RID: 8318
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetLayerWeightInternal(ref AnimatorControllerPlayable that, int layerIndex);

		// Token: 0x0600207F RID: 8319 RVA: 0x00024AEC File Offset: 0x00022CEC
		public float GetLayerWeight(int layerIndex)
		{
			return AnimatorControllerPlayable.GetLayerWeightInternal(ref this, layerIndex);
		}

		// Token: 0x06002080 RID: 8320
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetLayerWeightInternal(ref AnimatorControllerPlayable that, int layerIndex, float weight);

		// Token: 0x06002081 RID: 8321 RVA: 0x00024B08 File Offset: 0x00022D08
		public void SetLayerWeight(int layerIndex, float weight)
		{
			AnimatorControllerPlayable.SetLayerWeightInternal(ref this, layerIndex, weight);
		}

		// Token: 0x06002082 RID: 8322
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimatorStateInfo GetCurrentAnimatorStateInfoInternal(ref AnimatorControllerPlayable that, int layerIndex);

		// Token: 0x06002083 RID: 8323 RVA: 0x00024B14 File Offset: 0x00022D14
		public AnimatorStateInfo GetCurrentAnimatorStateInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetCurrentAnimatorStateInfoInternal(ref this, layerIndex);
		}

		// Token: 0x06002084 RID: 8324
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimatorStateInfo GetNextAnimatorStateInfoInternal(ref AnimatorControllerPlayable that, int layerIndex);

		// Token: 0x06002085 RID: 8325 RVA: 0x00024B30 File Offset: 0x00022D30
		public AnimatorStateInfo GetNextAnimatorStateInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetNextAnimatorStateInfoInternal(ref this, layerIndex);
		}

		// Token: 0x06002086 RID: 8326
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimatorTransitionInfo GetAnimatorTransitionInfoInternal(ref AnimatorControllerPlayable that, int layerIndex);

		// Token: 0x06002087 RID: 8327 RVA: 0x00024B4C File Offset: 0x00022D4C
		public AnimatorTransitionInfo GetAnimatorTransitionInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetAnimatorTransitionInfoInternal(ref this, layerIndex);
		}

		// Token: 0x06002088 RID: 8328
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimatorClipInfo[] GetCurrentAnimatorClipInfoInternal(ref AnimatorControllerPlayable that, int layerIndex);

		// Token: 0x06002089 RID: 8329 RVA: 0x00024B68 File Offset: 0x00022D68
		public AnimatorClipInfo[] GetCurrentAnimatorClipInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetCurrentAnimatorClipInfoInternal(ref this, layerIndex);
		}

		// Token: 0x0600208A RID: 8330 RVA: 0x00024B84 File Offset: 0x00022D84
		public void GetCurrentAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips)
		{
			if (clips == null)
			{
				throw new ArgumentNullException("clips");
			}
			this.GetAnimatorClipInfoInternal(ref this, layerIndex, true, clips);
		}

		// Token: 0x0600208B RID: 8331 RVA: 0x00024BA4 File Offset: 0x00022DA4
		public void GetNextAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips)
		{
			if (clips == null)
			{
				throw new ArgumentNullException("clips");
			}
			this.GetAnimatorClipInfoInternal(ref this, layerIndex, false, clips);
		}

		// Token: 0x0600208C RID: 8332
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetAnimatorClipInfoInternal(ref AnimatorControllerPlayable that, int layerIndex, bool isCurrent, object clips);

		// Token: 0x0600208D RID: 8333
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetAnimatorClipInfoCountInternal(ref AnimatorControllerPlayable that, int layerIndex, bool current);

		// Token: 0x0600208E RID: 8334 RVA: 0x00024BC4 File Offset: 0x00022DC4
		public int GetCurrentAnimatorClipInfoCount(int layerIndex)
		{
			return this.GetAnimatorClipInfoCountInternal(ref this, layerIndex, true);
		}

		// Token: 0x0600208F RID: 8335 RVA: 0x00024BE4 File Offset: 0x00022DE4
		public int GetNextAnimatorClipInfoCount(int layerIndex)
		{
			return this.GetAnimatorClipInfoCountInternal(ref this, layerIndex, false);
		}

		// Token: 0x06002090 RID: 8336
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimatorClipInfo[] GetNextAnimatorClipInfoInternal(ref AnimatorControllerPlayable that, int layerIndex);

		// Token: 0x06002091 RID: 8337 RVA: 0x00024C04 File Offset: 0x00022E04
		public AnimatorClipInfo[] GetNextAnimatorClipInfo(int layerIndex)
		{
			return AnimatorControllerPlayable.GetNextAnimatorClipInfoInternal(ref this, layerIndex);
		}

		// Token: 0x06002092 RID: 8338 RVA: 0x00024C20 File Offset: 0x00022E20
		internal string ResolveHash(int hash)
		{
			return AnimatorControllerPlayable.ResolveHashInternal(ref this, hash);
		}

		// Token: 0x06002093 RID: 8339
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string ResolveHashInternal(ref AnimatorControllerPlayable that, int hash);

		// Token: 0x06002094 RID: 8340
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsInTransitionInternal(ref AnimatorControllerPlayable that, int layerIndex);

		// Token: 0x06002095 RID: 8341 RVA: 0x00024C3C File Offset: 0x00022E3C
		public bool IsInTransition(int layerIndex)
		{
			return AnimatorControllerPlayable.IsInTransitionInternal(ref this, layerIndex);
		}

		// Token: 0x06002096 RID: 8342
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetParameterCountInternal(ref AnimatorControllerPlayable that);

		// Token: 0x17000810 RID: 2064
		// (get) Token: 0x06002097 RID: 8343 RVA: 0x00024C58 File Offset: 0x00022E58
		public int parameterCount
		{
			get
			{
				return AnimatorControllerPlayable.GetParameterCountInternal(ref this);
			}
		}

		// Token: 0x06002098 RID: 8344
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimatorControllerParameter[] GetParametersArrayInternal(ref AnimatorControllerPlayable that);

		// Token: 0x06002099 RID: 8345 RVA: 0x00024C74 File Offset: 0x00022E74
		public AnimatorControllerParameter GetParameter(int index)
		{
			AnimatorControllerParameter[] parametersArrayInternal = AnimatorControllerPlayable.GetParametersArrayInternal(ref this);
			if (index < 0 && index >= parametersArrayInternal.Length)
			{
				throw new IndexOutOfRangeException("index");
			}
			return parametersArrayInternal[index];
		}

		// Token: 0x0600209A RID: 8346
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int StringToHash(string name);

		// Token: 0x0600209B RID: 8347 RVA: 0x00024CB0 File Offset: 0x00022EB0
		[ExcludeFromDocs]
		public void CrossFadeInFixedTime(string stateName, float transitionDuration, int layer)
		{
			float fixedTime = 0f;
			this.CrossFadeInFixedTime(stateName, transitionDuration, layer, fixedTime);
		}

		// Token: 0x0600209C RID: 8348 RVA: 0x00024CD0 File Offset: 0x00022ED0
		[ExcludeFromDocs]
		public void CrossFadeInFixedTime(string stateName, float transitionDuration)
		{
			float fixedTime = 0f;
			int layer = -1;
			this.CrossFadeInFixedTime(stateName, transitionDuration, layer, fixedTime);
		}

		// Token: 0x0600209D RID: 8349 RVA: 0x00024CF0 File Offset: 0x00022EF0
		public void CrossFadeInFixedTime(string stateName, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("0.0f")] float fixedTime)
		{
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref this, AnimatorControllerPlayable.StringToHash(stateName), transitionDuration, layer, fixedTime);
		}

		// Token: 0x0600209E RID: 8350 RVA: 0x00024D04 File Offset: 0x00022F04
		[ExcludeFromDocs]
		public void CrossFadeInFixedTime(int stateNameHash, float transitionDuration, int layer)
		{
			float fixedTime = 0f;
			this.CrossFadeInFixedTime(stateNameHash, transitionDuration, layer, fixedTime);
		}

		// Token: 0x0600209F RID: 8351 RVA: 0x00024D24 File Offset: 0x00022F24
		[ExcludeFromDocs]
		public void CrossFadeInFixedTime(int stateNameHash, float transitionDuration)
		{
			float fixedTime = 0f;
			int layer = -1;
			this.CrossFadeInFixedTime(stateNameHash, transitionDuration, layer, fixedTime);
		}

		// Token: 0x060020A0 RID: 8352 RVA: 0x00024D44 File Offset: 0x00022F44
		public void CrossFadeInFixedTime(int stateNameHash, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("0.0f")] float fixedTime)
		{
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref this, stateNameHash, transitionDuration, layer, fixedTime);
		}

		// Token: 0x060020A1 RID: 8353
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CrossFadeInFixedTimeInternal(ref AnimatorControllerPlayable that, int stateNameHash, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("0.0f")] float fixedTime);

		// Token: 0x060020A2 RID: 8354 RVA: 0x00024D54 File Offset: 0x00022F54
		[ExcludeFromDocs]
		private static void CrossFadeInFixedTimeInternal(ref AnimatorControllerPlayable that, int stateNameHash, float transitionDuration, int layer)
		{
			float fixedTime = 0f;
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref that, stateNameHash, transitionDuration, layer, fixedTime);
		}

		// Token: 0x060020A3 RID: 8355 RVA: 0x00024D74 File Offset: 0x00022F74
		[ExcludeFromDocs]
		private static void CrossFadeInFixedTimeInternal(ref AnimatorControllerPlayable that, int stateNameHash, float transitionDuration)
		{
			float fixedTime = 0f;
			int layer = -1;
			AnimatorControllerPlayable.CrossFadeInFixedTimeInternal(ref that, stateNameHash, transitionDuration, layer, fixedTime);
		}

		// Token: 0x060020A4 RID: 8356 RVA: 0x00024D94 File Offset: 0x00022F94
		[ExcludeFromDocs]
		public void CrossFade(string stateName, float transitionDuration, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.CrossFade(stateName, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x060020A5 RID: 8357 RVA: 0x00024DB4 File Offset: 0x00022FB4
		[ExcludeFromDocs]
		public void CrossFade(string stateName, float transitionDuration)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.CrossFade(stateName, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x060020A6 RID: 8358 RVA: 0x00024DD4 File Offset: 0x00022FD4
		public void CrossFade(string stateName, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			AnimatorControllerPlayable.CrossFadeInternal(ref this, AnimatorControllerPlayable.StringToHash(stateName), transitionDuration, layer, normalizedTime);
		}

		// Token: 0x060020A7 RID: 8359 RVA: 0x00024DE8 File Offset: 0x00022FE8
		[ExcludeFromDocs]
		public void CrossFade(int stateNameHash, float transitionDuration, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.CrossFade(stateNameHash, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x060020A8 RID: 8360 RVA: 0x00024E08 File Offset: 0x00023008
		[ExcludeFromDocs]
		public void CrossFade(int stateNameHash, float transitionDuration)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.CrossFade(stateNameHash, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x060020A9 RID: 8361 RVA: 0x00024E28 File Offset: 0x00023028
		public void CrossFade(int stateNameHash, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			AnimatorControllerPlayable.CrossFadeInternal(ref this, stateNameHash, transitionDuration, layer, normalizedTime);
		}

		// Token: 0x060020AA RID: 8362
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void CrossFadeInternal(ref AnimatorControllerPlayable that, int stateNameHash, float transitionDuration, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime);

		// Token: 0x060020AB RID: 8363 RVA: 0x00024E38 File Offset: 0x00023038
		[ExcludeFromDocs]
		private static void CrossFadeInternal(ref AnimatorControllerPlayable that, int stateNameHash, float transitionDuration, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			AnimatorControllerPlayable.CrossFadeInternal(ref that, stateNameHash, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x060020AC RID: 8364 RVA: 0x00024E58 File Offset: 0x00023058
		[ExcludeFromDocs]
		private static void CrossFadeInternal(ref AnimatorControllerPlayable that, int stateNameHash, float transitionDuration)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			AnimatorControllerPlayable.CrossFadeInternal(ref that, stateNameHash, transitionDuration, layer, negativeInfinity);
		}

		// Token: 0x060020AD RID: 8365 RVA: 0x00024E78 File Offset: 0x00023078
		[ExcludeFromDocs]
		public void PlayInFixedTime(string stateName, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.PlayInFixedTime(stateName, layer, negativeInfinity);
		}

		// Token: 0x060020AE RID: 8366 RVA: 0x00024E98 File Offset: 0x00023098
		[ExcludeFromDocs]
		public void PlayInFixedTime(string stateName)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.PlayInFixedTime(stateName, layer, negativeInfinity);
		}

		// Token: 0x060020AF RID: 8367 RVA: 0x00024EB8 File Offset: 0x000230B8
		public void PlayInFixedTime(string stateName, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float fixedTime)
		{
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref this, AnimatorControllerPlayable.StringToHash(stateName), layer, fixedTime);
		}

		// Token: 0x060020B0 RID: 8368 RVA: 0x00024ECC File Offset: 0x000230CC
		[ExcludeFromDocs]
		public void PlayInFixedTime(int stateNameHash, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.PlayInFixedTime(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x060020B1 RID: 8369 RVA: 0x00024EEC File Offset: 0x000230EC
		[ExcludeFromDocs]
		public void PlayInFixedTime(int stateNameHash)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.PlayInFixedTime(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x060020B2 RID: 8370 RVA: 0x00024F0C File Offset: 0x0002310C
		public void PlayInFixedTime(int stateNameHash, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float fixedTime)
		{
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref this, stateNameHash, layer, fixedTime);
		}

		// Token: 0x060020B3 RID: 8371
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void PlayInFixedTimeInternal(ref AnimatorControllerPlayable that, int stateNameHash, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float fixedTime);

		// Token: 0x060020B4 RID: 8372 RVA: 0x00024F18 File Offset: 0x00023118
		[ExcludeFromDocs]
		private static void PlayInFixedTimeInternal(ref AnimatorControllerPlayable that, int stateNameHash, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref that, stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x060020B5 RID: 8373 RVA: 0x00024F38 File Offset: 0x00023138
		[ExcludeFromDocs]
		private static void PlayInFixedTimeInternal(ref AnimatorControllerPlayable that, int stateNameHash)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			AnimatorControllerPlayable.PlayInFixedTimeInternal(ref that, stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x060020B6 RID: 8374 RVA: 0x00024F58 File Offset: 0x00023158
		[ExcludeFromDocs]
		public void Play(string stateName, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.Play(stateName, layer, negativeInfinity);
		}

		// Token: 0x060020B7 RID: 8375 RVA: 0x00024F78 File Offset: 0x00023178
		[ExcludeFromDocs]
		public void Play(string stateName)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.Play(stateName, layer, negativeInfinity);
		}

		// Token: 0x060020B8 RID: 8376 RVA: 0x00024F98 File Offset: 0x00023198
		public void Play(string stateName, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			this.PlayInternal(ref this, AnimatorControllerPlayable.StringToHash(stateName), layer, normalizedTime);
		}

		// Token: 0x060020B9 RID: 8377 RVA: 0x00024FAC File Offset: 0x000231AC
		[ExcludeFromDocs]
		public void Play(int stateNameHash, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.Play(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x060020BA RID: 8378 RVA: 0x00024FCC File Offset: 0x000231CC
		[ExcludeFromDocs]
		public void Play(int stateNameHash)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.Play(stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x060020BB RID: 8379 RVA: 0x00024FEC File Offset: 0x000231EC
		public void Play(int stateNameHash, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime)
		{
			this.PlayInternal(ref this, stateNameHash, layer, normalizedTime);
		}

		// Token: 0x060020BC RID: 8380
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void PlayInternal(ref AnimatorControllerPlayable that, int stateNameHash, [DefaultValue("-1")] int layer, [DefaultValue("float.NegativeInfinity")] float normalizedTime);

		// Token: 0x060020BD RID: 8381 RVA: 0x00024FFC File Offset: 0x000231FC
		[ExcludeFromDocs]
		private void PlayInternal(ref AnimatorControllerPlayable that, int stateNameHash, int layer)
		{
			float negativeInfinity = float.NegativeInfinity;
			this.PlayInternal(ref that, stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x060020BE RID: 8382 RVA: 0x0002501C File Offset: 0x0002321C
		[ExcludeFromDocs]
		private void PlayInternal(ref AnimatorControllerPlayable that, int stateNameHash)
		{
			float negativeInfinity = float.NegativeInfinity;
			int layer = -1;
			this.PlayInternal(ref that, stateNameHash, layer, negativeInfinity);
		}

		// Token: 0x060020BF RID: 8383 RVA: 0x0002503C File Offset: 0x0002323C
		public bool HasState(int layerIndex, int stateID)
		{
			return this.HasStateInternal(ref this, layerIndex, stateID);
		}

		// Token: 0x060020C0 RID: 8384
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool HasStateInternal(ref AnimatorControllerPlayable that, int layerIndex, int stateID);

		// Token: 0x060020C1 RID: 8385
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetFloatString(ref AnimatorControllerPlayable that, string name, float value);

		// Token: 0x060020C2 RID: 8386
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetFloatID(ref AnimatorControllerPlayable that, int id, float value);

		// Token: 0x060020C3 RID: 8387
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetFloatString(ref AnimatorControllerPlayable that, string name);

		// Token: 0x060020C4 RID: 8388
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetFloatID(ref AnimatorControllerPlayable that, int id);

		// Token: 0x060020C5 RID: 8389
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetBoolString(ref AnimatorControllerPlayable that, string name, bool value);

		// Token: 0x060020C6 RID: 8390
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetBoolID(ref AnimatorControllerPlayable that, int id, bool value);

		// Token: 0x060020C7 RID: 8391
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetBoolString(ref AnimatorControllerPlayable that, string name);

		// Token: 0x060020C8 RID: 8392
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetBoolID(ref AnimatorControllerPlayable that, int id);

		// Token: 0x060020C9 RID: 8393
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetIntegerString(ref AnimatorControllerPlayable that, string name, int value);

		// Token: 0x060020CA RID: 8394
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetIntegerID(ref AnimatorControllerPlayable that, int id, int value);

		// Token: 0x060020CB RID: 8395
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetIntegerString(ref AnimatorControllerPlayable that, string name);

		// Token: 0x060020CC RID: 8396
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetIntegerID(ref AnimatorControllerPlayable that, int id);

		// Token: 0x060020CD RID: 8397
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetTriggerString(ref AnimatorControllerPlayable that, string name);

		// Token: 0x060020CE RID: 8398
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetTriggerID(ref AnimatorControllerPlayable that, int id);

		// Token: 0x060020CF RID: 8399
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ResetTriggerString(ref AnimatorControllerPlayable that, string name);

		// Token: 0x060020D0 RID: 8400
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ResetTriggerID(ref AnimatorControllerPlayable that, int id);

		// Token: 0x060020D1 RID: 8401
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsParameterControlledByCurveString(ref AnimatorControllerPlayable that, string name);

		// Token: 0x060020D2 RID: 8402
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsParameterControlledByCurveID(ref AnimatorControllerPlayable that, int id);

		// Token: 0x17000811 RID: 2065
		// (get) Token: 0x060020D3 RID: 8403 RVA: 0x0002505C File Offset: 0x0002325C
		// (set) Token: 0x060020D4 RID: 8404 RVA: 0x00025094 File Offset: 0x00023294
		public PlayState state
		{
			get
			{
				return Playables.GetPlayStateValidated(this, base.GetType());
			}
			set
			{
				Playables.SetPlayStateValidated(this, value, base.GetType());
			}
		}

		// Token: 0x17000812 RID: 2066
		// (get) Token: 0x060020D5 RID: 8405 RVA: 0x000250B8 File Offset: 0x000232B8
		// (set) Token: 0x060020D6 RID: 8406 RVA: 0x000250F0 File Offset: 0x000232F0
		public double time
		{
			get
			{
				return Playables.GetTimeValidated(this, base.GetType());
			}
			set
			{
				Playables.SetTimeValidated(this, value, base.GetType());
			}
		}

		// Token: 0x17000813 RID: 2067
		// (get) Token: 0x060020D7 RID: 8407 RVA: 0x00025114 File Offset: 0x00023314
		// (set) Token: 0x060020D8 RID: 8408 RVA: 0x0002514C File Offset: 0x0002334C
		public double duration
		{
			get
			{
				return Playables.GetDurationValidated(this, base.GetType());
			}
			set
			{
				Playables.SetDurationValidated(this, value, base.GetType());
			}
		}

		// Token: 0x060020D9 RID: 8409 RVA: 0x00025170 File Offset: 0x00023370
		public bool IsValid()
		{
			return Playables.IsValid(this);
		}

		// Token: 0x060020DA RID: 8410 RVA: 0x00025198 File Offset: 0x00023398
		public T CastTo<T>() where T : struct
		{
			return this.handle.CastTo<T>();
		}

		// Token: 0x060020DB RID: 8411 RVA: 0x000251B8 File Offset: 0x000233B8
		public static bool operator ==(AnimatorControllerPlayable x, Playable y)
		{
			return Playables.Equals(x, y);
		}

		// Token: 0x060020DC RID: 8412 RVA: 0x000251DC File Offset: 0x000233DC
		public static bool operator !=(AnimatorControllerPlayable x, Playable y)
		{
			return !Playables.Equals(x, y);
		}

		// Token: 0x060020DD RID: 8413 RVA: 0x00025200 File Offset: 0x00023400
		public override bool Equals(object p)
		{
			return Playables.Equals(this, p);
		}

		// Token: 0x060020DE RID: 8414 RVA: 0x00025228 File Offset: 0x00023428
		public override int GetHashCode()
		{
			return this.node.GetHashCode();
		}

		// Token: 0x040005AF RID: 1455
		internal AnimationPlayable handle;
	}
}
