﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020001EA RID: 490
	[UsedByNativeCode]
	internal struct AnimationLayerMixerPlayable
	{
		// Token: 0x1700081A RID: 2074
		// (get) Token: 0x060020FD RID: 8445 RVA: 0x00025764 File Offset: 0x00023964
		internal Playable node
		{
			get
			{
				return this.handle.node;
			}
		}

		// Token: 0x060020FE RID: 8446 RVA: 0x00025784 File Offset: 0x00023984
		public static AnimationLayerMixerPlayable Create()
		{
			AnimationLayerMixerPlayable result = default(AnimationLayerMixerPlayable);
			AnimationLayerMixerPlayable.InternalCreate(ref result);
			return result;
		}

		// Token: 0x060020FF RID: 8447
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalCreate(ref AnimationLayerMixerPlayable that);

		// Token: 0x06002100 RID: 8448 RVA: 0x000257AC File Offset: 0x000239AC
		public void Destroy()
		{
			this.node.Destroy();
		}

		// Token: 0x06002101 RID: 8449 RVA: 0x000257C8 File Offset: 0x000239C8
		public static bool operator ==(AnimationLayerMixerPlayable x, Playable y)
		{
			return Playables.Equals(x, y);
		}

		// Token: 0x06002102 RID: 8450 RVA: 0x000257EC File Offset: 0x000239EC
		public static bool operator !=(AnimationLayerMixerPlayable x, Playable y)
		{
			return !Playables.Equals(x, y);
		}

		// Token: 0x06002103 RID: 8451 RVA: 0x00025810 File Offset: 0x00023A10
		public override bool Equals(object p)
		{
			return Playables.Equals(this, p);
		}

		// Token: 0x06002104 RID: 8452 RVA: 0x00025838 File Offset: 0x00023A38
		public override int GetHashCode()
		{
			return this.node.GetHashCode();
		}

		// Token: 0x06002105 RID: 8453 RVA: 0x00025864 File Offset: 0x00023A64
		public static implicit operator Playable(AnimationLayerMixerPlayable b)
		{
			return b.node;
		}

		// Token: 0x06002106 RID: 8454 RVA: 0x00025880 File Offset: 0x00023A80
		public static implicit operator AnimationPlayable(AnimationLayerMixerPlayable b)
		{
			return b.handle;
		}

		// Token: 0x06002107 RID: 8455 RVA: 0x0002589C File Offset: 0x00023A9C
		public bool IsValid()
		{
			return Playables.IsValid(this);
		}

		// Token: 0x1700081B RID: 2075
		// (get) Token: 0x06002108 RID: 8456 RVA: 0x000258C4 File Offset: 0x00023AC4
		public int inputCount
		{
			get
			{
				return Playables.GetInputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x06002109 RID: 8457 RVA: 0x000258FC File Offset: 0x00023AFC
		public Playable GetInput(int inputPort)
		{
			return Playables.GetInputValidated(this, inputPort, base.GetType());
		}

		// Token: 0x1700081C RID: 2076
		// (get) Token: 0x0600210A RID: 8458 RVA: 0x00025934 File Offset: 0x00023B34
		public int outputCount
		{
			get
			{
				return Playables.GetOutputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x0600210B RID: 8459 RVA: 0x0002596C File Offset: 0x00023B6C
		public Playable GetOutput(int outputPort)
		{
			return Playables.GetOutputValidated(this, outputPort, base.GetType());
		}

		// Token: 0x0600210C RID: 8460 RVA: 0x000259A4 File Offset: 0x00023BA4
		public float GetInputWeight(int index)
		{
			return Playables.GetInputWeightValidated(this, index, base.GetType());
		}

		// Token: 0x0600210D RID: 8461 RVA: 0x000259DC File Offset: 0x00023BDC
		public void SetInputWeight(int inputIndex, float weight)
		{
			Playables.SetInputWeightValidated(this, inputIndex, weight, base.GetType());
		}

		// Token: 0x1700081D RID: 2077
		// (get) Token: 0x0600210E RID: 8462 RVA: 0x00025A04 File Offset: 0x00023C04
		// (set) Token: 0x0600210F RID: 8463 RVA: 0x00025A3C File Offset: 0x00023C3C
		public PlayState state
		{
			get
			{
				return Playables.GetPlayStateValidated(this, base.GetType());
			}
			set
			{
				Playables.SetPlayStateValidated(this, value, base.GetType());
			}
		}

		// Token: 0x1700081E RID: 2078
		// (get) Token: 0x06002110 RID: 8464 RVA: 0x00025A60 File Offset: 0x00023C60
		// (set) Token: 0x06002111 RID: 8465 RVA: 0x00025A98 File Offset: 0x00023C98
		public double time
		{
			get
			{
				return Playables.GetTimeValidated(this, base.GetType());
			}
			set
			{
				Playables.SetTimeValidated(this, value, base.GetType());
			}
		}

		// Token: 0x1700081F RID: 2079
		// (get) Token: 0x06002112 RID: 8466 RVA: 0x00025ABC File Offset: 0x00023CBC
		// (set) Token: 0x06002113 RID: 8467 RVA: 0x00025AF4 File Offset: 0x00023CF4
		public double duration
		{
			get
			{
				return Playables.GetDurationValidated(this, base.GetType());
			}
			set
			{
				Playables.SetDurationValidated(this, value, base.GetType());
			}
		}

		// Token: 0x06002114 RID: 8468 RVA: 0x00025B18 File Offset: 0x00023D18
		public T CastTo<T>() where T : struct
		{
			return this.handle.CastTo<T>();
		}

		// Token: 0x06002115 RID: 8469 RVA: 0x00025B38 File Offset: 0x00023D38
		public int AddInput(Playable input)
		{
			return AnimationPlayableUtilities.AddInputValidated(this, input, base.GetType());
		}

		// Token: 0x06002116 RID: 8470 RVA: 0x00025B70 File Offset: 0x00023D70
		public bool SetInput(Playable source, int index)
		{
			return AnimationPlayableUtilities.SetInputValidated(this, source, index, base.GetType());
		}

		// Token: 0x06002117 RID: 8471 RVA: 0x00025BA8 File Offset: 0x00023DA8
		public bool SetInputs(IEnumerable<Playable> sources)
		{
			return AnimationPlayableUtilities.SetInputsValidated(this, sources, base.GetType());
		}

		// Token: 0x06002118 RID: 8472 RVA: 0x00025BE0 File Offset: 0x00023DE0
		public bool RemoveInput(int index)
		{
			return AnimationPlayableUtilities.RemoveInputValidated(this, index, base.GetType());
		}

		// Token: 0x06002119 RID: 8473 RVA: 0x00025C18 File Offset: 0x00023E18
		public bool RemoveAllInputs()
		{
			return AnimationPlayableUtilities.RemoveAllInputsValidated(this, base.GetType());
		}

		// Token: 0x040005B1 RID: 1457
		internal AnimationPlayable handle;
	}
}
