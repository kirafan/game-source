﻿using System;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020000E0 RID: 224
	public enum DirectorUpdateMode
	{
		// Token: 0x04000233 RID: 563
		DSPClock,
		// Token: 0x04000234 RID: 564
		GameTime,
		// Token: 0x04000235 RID: 565
		UnscaledGameTime,
		// Token: 0x04000236 RID: 566
		Manual
	}
}
