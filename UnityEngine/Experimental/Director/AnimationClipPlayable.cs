﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020001EB RID: 491
	[UsedByNativeCode]
	public struct AnimationClipPlayable
	{
		// Token: 0x17000820 RID: 2080
		// (get) Token: 0x0600211A RID: 8474 RVA: 0x00025C50 File Offset: 0x00023E50
		internal Playable node
		{
			get
			{
				return this.handle.node;
			}
		}

		// Token: 0x0600211B RID: 8475 RVA: 0x00025C70 File Offset: 0x00023E70
		public static AnimationClipPlayable Create(AnimationClip clip)
		{
			AnimationClipPlayable result = default(AnimationClipPlayable);
			AnimationClipPlayable.InternalCreate(clip, ref result);
			return result;
		}

		// Token: 0x0600211C RID: 8476
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalCreate(AnimationClip clip, ref AnimationClipPlayable that);

		// Token: 0x0600211D RID: 8477 RVA: 0x00025C98 File Offset: 0x00023E98
		public void Destroy()
		{
			this.node.Destroy();
		}

		// Token: 0x0600211E RID: 8478 RVA: 0x00025CB4 File Offset: 0x00023EB4
		public static bool operator ==(AnimationClipPlayable x, Playable y)
		{
			return Playables.Equals(x, y);
		}

		// Token: 0x0600211F RID: 8479 RVA: 0x00025CD8 File Offset: 0x00023ED8
		public static bool operator !=(AnimationClipPlayable x, Playable y)
		{
			return !Playables.Equals(x, y);
		}

		// Token: 0x06002120 RID: 8480 RVA: 0x00025CFC File Offset: 0x00023EFC
		public override bool Equals(object p)
		{
			return Playables.Equals(this, p);
		}

		// Token: 0x06002121 RID: 8481 RVA: 0x00025D24 File Offset: 0x00023F24
		public override int GetHashCode()
		{
			return this.node.GetHashCode();
		}

		// Token: 0x06002122 RID: 8482 RVA: 0x00025D50 File Offset: 0x00023F50
		public static implicit operator Playable(AnimationClipPlayable b)
		{
			return b.node;
		}

		// Token: 0x06002123 RID: 8483 RVA: 0x00025D6C File Offset: 0x00023F6C
		public static implicit operator AnimationPlayable(AnimationClipPlayable b)
		{
			return b.handle;
		}

		// Token: 0x06002124 RID: 8484 RVA: 0x00025D88 File Offset: 0x00023F88
		public bool IsValid()
		{
			return Playables.IsValid(this);
		}

		// Token: 0x17000821 RID: 2081
		// (get) Token: 0x06002125 RID: 8485 RVA: 0x00025DB0 File Offset: 0x00023FB0
		// (set) Token: 0x06002126 RID: 8486 RVA: 0x00025DE8 File Offset: 0x00023FE8
		public PlayState state
		{
			get
			{
				return Playables.GetPlayStateValidated(this, base.GetType());
			}
			set
			{
				Playables.SetPlayStateValidated(this, value, base.GetType());
			}
		}

		// Token: 0x17000822 RID: 2082
		// (get) Token: 0x06002127 RID: 8487 RVA: 0x00025E0C File Offset: 0x0002400C
		// (set) Token: 0x06002128 RID: 8488 RVA: 0x00025E44 File Offset: 0x00024044
		public double time
		{
			get
			{
				return Playables.GetTimeValidated(this, base.GetType());
			}
			set
			{
				Playables.SetTimeValidated(this, value, base.GetType());
			}
		}

		// Token: 0x17000823 RID: 2083
		// (get) Token: 0x06002129 RID: 8489 RVA: 0x00025E68 File Offset: 0x00024068
		// (set) Token: 0x0600212A RID: 8490 RVA: 0x00025EA0 File Offset: 0x000240A0
		public double duration
		{
			get
			{
				return Playables.GetDurationValidated(this, base.GetType());
			}
			set
			{
				Playables.SetDurationValidated(this, value, base.GetType());
			}
		}

		// Token: 0x17000824 RID: 2084
		// (get) Token: 0x0600212B RID: 8491 RVA: 0x00025EC4 File Offset: 0x000240C4
		public int outputCount
		{
			get
			{
				return Playables.GetOutputCountValidated(this, base.GetType());
			}
		}

		// Token: 0x0600212C RID: 8492 RVA: 0x00025EFC File Offset: 0x000240FC
		public Playable GetOutput(int outputPort)
		{
			return Playables.GetOutputValidated(this, outputPort, base.GetType());
		}

		// Token: 0x0600212D RID: 8493 RVA: 0x00025F34 File Offset: 0x00024134
		public T CastTo<T>() where T : struct
		{
			return this.handle.CastTo<T>();
		}

		// Token: 0x0600212E RID: 8494
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AnimationClip GetAnimationClip(ref AnimationClipPlayable that);

		// Token: 0x17000825 RID: 2085
		// (get) Token: 0x0600212F RID: 8495 RVA: 0x00025F54 File Offset: 0x00024154
		public AnimationClip clip
		{
			get
			{
				return AnimationClipPlayable.GetAnimationClip(ref this);
			}
		}

		// Token: 0x06002130 RID: 8496
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float GetSpeed(ref AnimationClipPlayable that);

		// Token: 0x06002131 RID: 8497
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetSpeed(ref AnimationClipPlayable that, float value);

		// Token: 0x17000826 RID: 2086
		// (get) Token: 0x06002132 RID: 8498 RVA: 0x00025F70 File Offset: 0x00024170
		// (set) Token: 0x06002133 RID: 8499 RVA: 0x00025F8C File Offset: 0x0002418C
		public float speed
		{
			get
			{
				return AnimationClipPlayable.GetSpeed(ref this);
			}
			set
			{
				AnimationClipPlayable.SetSpeed(ref this, value);
			}
		}

		// Token: 0x06002134 RID: 8500
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetApplyFootIK(ref AnimationClipPlayable that);

		// Token: 0x06002135 RID: 8501
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetApplyFootIK(ref AnimationClipPlayable that, bool value);

		// Token: 0x17000827 RID: 2087
		// (get) Token: 0x06002136 RID: 8502 RVA: 0x00025F98 File Offset: 0x00024198
		// (set) Token: 0x06002137 RID: 8503 RVA: 0x00025FB4 File Offset: 0x000241B4
		public bool applyFootIK
		{
			get
			{
				return AnimationClipPlayable.GetApplyFootIK(ref this);
			}
			set
			{
				AnimationClipPlayable.SetApplyFootIK(ref this, value);
			}
		}

		// Token: 0x06002138 RID: 8504
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetRemoveStartOffset(ref AnimationClipPlayable that);

		// Token: 0x06002139 RID: 8505
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetRemoveStartOffset(ref AnimationClipPlayable that, bool value);

		// Token: 0x17000828 RID: 2088
		// (get) Token: 0x0600213A RID: 8506 RVA: 0x00025FC0 File Offset: 0x000241C0
		// (set) Token: 0x0600213B RID: 8507 RVA: 0x00025FDC File Offset: 0x000241DC
		internal bool removeStartOffset
		{
			get
			{
				return AnimationClipPlayable.GetRemoveStartOffset(ref this);
			}
			set
			{
				AnimationClipPlayable.SetRemoveStartOffset(ref this, value);
			}
		}

		// Token: 0x040005B2 RID: 1458
		internal AnimationPlayable handle;
	}
}
