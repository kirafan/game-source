﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020000E3 RID: 227
	[UsedByNativeCode]
	public struct Playable
	{
		// Token: 0x06000FCA RID: 4042 RVA: 0x00015FE0 File Offset: 0x000141E0
		public void Destroy()
		{
			Playables.InternalDestroy(ref this);
		}

		// Token: 0x06000FCB RID: 4043 RVA: 0x00015FEC File Offset: 0x000141EC
		public bool IsValid()
		{
			return Playable.IsValidInternal(ref this);
		}

		// Token: 0x06000FCC RID: 4044 RVA: 0x00016008 File Offset: 0x00014208
		private static bool IsValidInternal(ref Playable playable)
		{
			return Playable.INTERNAL_CALL_IsValidInternal(ref playable);
		}

		// Token: 0x06000FCD RID: 4045
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_IsValidInternal(ref Playable playable);

		// Token: 0x06000FCE RID: 4046 RVA: 0x00016024 File Offset: 0x00014224
		public T CastTo<T>() where T : struct
		{
			return (T)((object)Playables.CastToInternal(typeof(T), this.m_Handle, this.m_Version));
		}

		// Token: 0x06000FCF RID: 4047 RVA: 0x0001605C File Offset: 0x0001425C
		public static Type GetTypeOf(Playable playable)
		{
			return Playables.GetTypeOfInternal(playable.m_Handle, playable.m_Version);
		}

		// Token: 0x17000363 RID: 867
		// (get) Token: 0x06000FD0 RID: 4048 RVA: 0x00016084 File Offset: 0x00014284
		public static Playable Null
		{
			get
			{
				return new Playable
				{
					m_Version = 10
				};
			}
		}

		// Token: 0x06000FD1 RID: 4049 RVA: 0x000160AC File Offset: 0x000142AC
		public static bool Connect(Playable source, Playable target)
		{
			return Playable.Connect(source, target, -1, -1);
		}

		// Token: 0x06000FD2 RID: 4050 RVA: 0x000160CC File Offset: 0x000142CC
		public static bool Connect(Playable source, Playable target, int sourceOutputPort, int targetInputPort)
		{
			return Playables.ConnectInternal(ref source, ref target, sourceOutputPort, targetInputPort);
		}

		// Token: 0x06000FD3 RID: 4051 RVA: 0x000160EC File Offset: 0x000142EC
		public static void Disconnect(Playable target, int inputPort)
		{
			if (target.CheckInputBounds(inputPort))
			{
				Playables.DisconnectInternal(ref target, inputPort);
			}
		}

		// Token: 0x06000FD4 RID: 4052 RVA: 0x0001610C File Offset: 0x0001430C
		public static T Create<T>() where T : CustomAnimationPlayable, new()
		{
			return Playable.InternalCreate(typeof(T)) as T;
		}

		// Token: 0x06000FD5 RID: 4053
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern object InternalCreate(Type type);

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x06000FD6 RID: 4054 RVA: 0x0001613C File Offset: 0x0001433C
		public int inputCount
		{
			get
			{
				return Playable.GetInputCountInternal(ref this);
			}
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x06000FD7 RID: 4055 RVA: 0x00016158 File Offset: 0x00014358
		public int outputCount
		{
			get
			{
				return Playable.GetOutputCountInternal(ref this);
			}
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x06000FD8 RID: 4056 RVA: 0x00016174 File Offset: 0x00014374
		// (set) Token: 0x06000FD9 RID: 4057 RVA: 0x00016190 File Offset: 0x00014390
		public PlayState state
		{
			get
			{
				return Playable.GetPlayStateInternal(ref this);
			}
			set
			{
				Playable.SetPlayStateInternal(ref this, value);
			}
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x06000FDA RID: 4058 RVA: 0x0001619C File Offset: 0x0001439C
		// (set) Token: 0x06000FDB RID: 4059 RVA: 0x000161B8 File Offset: 0x000143B8
		public double time
		{
			get
			{
				return Playable.GetTimeInternal(ref this);
			}
			set
			{
				Playable.SetTimeInternal(ref this, value);
			}
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06000FDC RID: 4060 RVA: 0x000161C4 File Offset: 0x000143C4
		internal bool canChangeInputs
		{
			get
			{
				return Playable.CanChangeInputsInternal(ref this);
			}
		}

		// Token: 0x17000369 RID: 873
		// (get) Token: 0x06000FDD RID: 4061 RVA: 0x000161E0 File Offset: 0x000143E0
		internal bool canSetWeights
		{
			get
			{
				return Playable.CanSetWeightsInternal(ref this);
			}
		}

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x06000FDE RID: 4062 RVA: 0x000161FC File Offset: 0x000143FC
		internal bool canDestroy
		{
			get
			{
				return Playable.CanDestroyInternal(ref this);
			}
		}

		// Token: 0x06000FDF RID: 4063 RVA: 0x00016218 File Offset: 0x00014418
		private static bool CanChangeInputsInternal(ref Playable playable)
		{
			return Playable.INTERNAL_CALL_CanChangeInputsInternal(ref playable);
		}

		// Token: 0x06000FE0 RID: 4064
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_CanChangeInputsInternal(ref Playable playable);

		// Token: 0x06000FE1 RID: 4065 RVA: 0x00016234 File Offset: 0x00014434
		private static bool CanSetWeightsInternal(ref Playable playable)
		{
			return Playable.INTERNAL_CALL_CanSetWeightsInternal(ref playable);
		}

		// Token: 0x06000FE2 RID: 4066
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_CanSetWeightsInternal(ref Playable playable);

		// Token: 0x06000FE3 RID: 4067 RVA: 0x00016250 File Offset: 0x00014450
		private static bool CanDestroyInternal(ref Playable playable)
		{
			return Playable.INTERNAL_CALL_CanDestroyInternal(ref playable);
		}

		// Token: 0x06000FE4 RID: 4068
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_CanDestroyInternal(ref Playable playable);

		// Token: 0x06000FE5 RID: 4069 RVA: 0x0001626C File Offset: 0x0001446C
		private static PlayState GetPlayStateInternal(ref Playable playable)
		{
			return Playable.INTERNAL_CALL_GetPlayStateInternal(ref playable);
		}

		// Token: 0x06000FE6 RID: 4070
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern PlayState INTERNAL_CALL_GetPlayStateInternal(ref Playable playable);

		// Token: 0x06000FE7 RID: 4071 RVA: 0x00016288 File Offset: 0x00014488
		private static void SetPlayStateInternal(ref Playable playable, PlayState playState)
		{
			Playable.INTERNAL_CALL_SetPlayStateInternal(ref playable, playState);
		}

		// Token: 0x06000FE8 RID: 4072
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetPlayStateInternal(ref Playable playable, PlayState playState);

		// Token: 0x06000FE9 RID: 4073 RVA: 0x00016294 File Offset: 0x00014494
		private static double GetTimeInternal(ref Playable playable)
		{
			return Playable.INTERNAL_CALL_GetTimeInternal(ref playable);
		}

		// Token: 0x06000FEA RID: 4074
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern double INTERNAL_CALL_GetTimeInternal(ref Playable playable);

		// Token: 0x06000FEB RID: 4075 RVA: 0x000162B0 File Offset: 0x000144B0
		private static void SetTimeInternal(ref Playable playable, double time)
		{
			Playable.INTERNAL_CALL_SetTimeInternal(ref playable, time);
		}

		// Token: 0x06000FEC RID: 4076
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetTimeInternal(ref Playable playable, double time);

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06000FED RID: 4077 RVA: 0x000162BC File Offset: 0x000144BC
		// (set) Token: 0x06000FEE RID: 4078 RVA: 0x000162D8 File Offset: 0x000144D8
		public double duration
		{
			get
			{
				return Playable.GetDurationInternal(ref this);
			}
			set
			{
				Playable.SetDurationInternal(ref this, value);
			}
		}

		// Token: 0x06000FEF RID: 4079 RVA: 0x000162E4 File Offset: 0x000144E4
		private static double GetDurationInternal(ref Playable playable)
		{
			return Playable.INTERNAL_CALL_GetDurationInternal(ref playable);
		}

		// Token: 0x06000FF0 RID: 4080
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern double INTERNAL_CALL_GetDurationInternal(ref Playable playable);

		// Token: 0x06000FF1 RID: 4081 RVA: 0x00016300 File Offset: 0x00014500
		private static void SetDurationInternal(ref Playable playable, double duration)
		{
			Playable.INTERNAL_CALL_SetDurationInternal(ref playable, duration);
		}

		// Token: 0x06000FF2 RID: 4082
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetDurationInternal(ref Playable playable, double duration);

		// Token: 0x06000FF3 RID: 4083 RVA: 0x0001630C File Offset: 0x0001450C
		private static int GetInputCountInternal(ref Playable playable)
		{
			return Playable.INTERNAL_CALL_GetInputCountInternal(ref playable);
		}

		// Token: 0x06000FF4 RID: 4084
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_GetInputCountInternal(ref Playable playable);

		// Token: 0x06000FF5 RID: 4085 RVA: 0x00016328 File Offset: 0x00014528
		private static int GetOutputCountInternal(ref Playable playable)
		{
			return Playable.INTERNAL_CALL_GetOutputCountInternal(ref playable);
		}

		// Token: 0x06000FF6 RID: 4086
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_GetOutputCountInternal(ref Playable playable);

		// Token: 0x06000FF7 RID: 4087 RVA: 0x00016344 File Offset: 0x00014544
		public Playable[] GetInputs()
		{
			List<Playable> list = new List<Playable>();
			int inputCount = this.inputCount;
			for (int i = 0; i < inputCount; i++)
			{
				list.Add(this.GetInput(i));
			}
			return list.ToArray();
		}

		// Token: 0x06000FF8 RID: 4088 RVA: 0x00016390 File Offset: 0x00014590
		public Playable GetInput(int inputPort)
		{
			return Playable.GetInputInternal(ref this, inputPort);
		}

		// Token: 0x06000FF9 RID: 4089 RVA: 0x000163AC File Offset: 0x000145AC
		private static Playable GetInputInternal(ref Playable playable, int index)
		{
			Playable result;
			Playable.INTERNAL_CALL_GetInputInternal(ref playable, index, out result);
			return result;
		}

		// Token: 0x06000FFA RID: 4090
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetInputInternal(ref Playable playable, int index, out Playable value);

		// Token: 0x06000FFB RID: 4091 RVA: 0x000163CC File Offset: 0x000145CC
		public Playable[] GetOutputs()
		{
			List<Playable> list = new List<Playable>();
			int outputCount = this.outputCount;
			for (int i = 0; i < outputCount; i++)
			{
				list.Add(this.GetOutput(i));
			}
			return list.ToArray();
		}

		// Token: 0x06000FFC RID: 4092 RVA: 0x00016418 File Offset: 0x00014618
		public Playable GetOutput(int outputPort)
		{
			return Playable.GetOutputInternal(ref this, outputPort);
		}

		// Token: 0x06000FFD RID: 4093 RVA: 0x00016434 File Offset: 0x00014634
		private static Playable GetOutputInternal(ref Playable playable, int index)
		{
			Playable result;
			Playable.INTERNAL_CALL_GetOutputInternal(ref playable, index, out result);
			return result;
		}

		// Token: 0x06000FFE RID: 4094
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetOutputInternal(ref Playable playable, int index, out Playable value);

		// Token: 0x06000FFF RID: 4095 RVA: 0x00016454 File Offset: 0x00014654
		private static void SetInputWeightFromIndexInternal(ref Playable playable, int index, float weight)
		{
			Playable.INTERNAL_CALL_SetInputWeightFromIndexInternal(ref playable, index, weight);
		}

		// Token: 0x06001000 RID: 4096
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetInputWeightFromIndexInternal(ref Playable playable, int index, float weight);

		// Token: 0x06001001 RID: 4097 RVA: 0x00016460 File Offset: 0x00014660
		private static void SetInputWeightInternal(ref Playable playable, Playable input, float weight)
		{
			Playable.INTERNAL_CALL_SetInputWeightInternal(ref playable, ref input, weight);
		}

		// Token: 0x06001002 RID: 4098
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetInputWeightInternal(ref Playable playable, ref Playable input, float weight);

		// Token: 0x06001003 RID: 4099 RVA: 0x0001646C File Offset: 0x0001466C
		public void SetInputWeight(Playable input, float weight)
		{
			Playable.SetInputWeightInternal(ref this, input, weight);
		}

		// Token: 0x06001004 RID: 4100 RVA: 0x00016478 File Offset: 0x00014678
		public bool SetInputWeight(int inputIndex, float weight)
		{
			bool result;
			if (this.CheckInputBounds(inputIndex))
			{
				Playable.SetInputWeightFromIndexInternal(ref this, inputIndex, weight);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06001005 RID: 4101 RVA: 0x000164AC File Offset: 0x000146AC
		public float GetInputWeight(int index)
		{
			return Playable.GetInputWeightInternal(ref this, index);
		}

		// Token: 0x06001006 RID: 4102 RVA: 0x000164C8 File Offset: 0x000146C8
		private static float GetInputWeightInternal(ref Playable playable, int index)
		{
			return Playable.INTERNAL_CALL_GetInputWeightInternal(ref playable, index);
		}

		// Token: 0x06001007 RID: 4103
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float INTERNAL_CALL_GetInputWeightInternal(ref Playable playable, int index);

		// Token: 0x06001008 RID: 4104 RVA: 0x000164E4 File Offset: 0x000146E4
		public static bool operator ==(Playable x, Playable y)
		{
			return Playable.CompareVersion(x, y);
		}

		// Token: 0x06001009 RID: 4105 RVA: 0x00016500 File Offset: 0x00014700
		public static bool operator !=(Playable x, Playable y)
		{
			return !Playable.CompareVersion(x, y);
		}

		// Token: 0x0600100A RID: 4106 RVA: 0x00016520 File Offset: 0x00014720
		public override bool Equals(object p)
		{
			return p != null && p.GetHashCode() == this.GetHashCode();
		}

		// Token: 0x0600100B RID: 4107 RVA: 0x00016554 File Offset: 0x00014754
		public override int GetHashCode()
		{
			return (int)this.m_Handle ^ this.m_Version;
		}

		// Token: 0x0600100C RID: 4108 RVA: 0x0001657C File Offset: 0x0001477C
		internal static bool CompareVersion(Playable lhs, Playable rhs)
		{
			return lhs.m_Handle == rhs.m_Handle && lhs.m_Version == rhs.m_Version;
		}

		// Token: 0x0600100D RID: 4109 RVA: 0x000165BC File Offset: 0x000147BC
		internal bool CheckInputBounds(int inputIndex)
		{
			return this.CheckInputBounds(inputIndex, false);
		}

		// Token: 0x0600100E RID: 4110 RVA: 0x000165DC File Offset: 0x000147DC
		internal bool CheckInputBounds(int inputIndex, bool acceptAny)
		{
			bool result;
			if (inputIndex == -1 && acceptAny)
			{
				result = true;
			}
			else
			{
				if (inputIndex < 0)
				{
					throw new IndexOutOfRangeException("Index must be greater than 0");
				}
				Playable[] inputs = this.GetInputs();
				if (inputs.Length <= inputIndex)
				{
					throw new IndexOutOfRangeException(string.Concat(new object[]
					{
						"inputIndex ",
						inputIndex,
						" is greater than the number of available inputs (",
						inputs.Length,
						")."
					}));
				}
				result = true;
			}
			return result;
		}

		// Token: 0x0400023A RID: 570
		internal IntPtr m_Handle;

		// Token: 0x0400023B RID: 571
		internal int m_Version;
	}
}
