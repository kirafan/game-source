﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020001E7 RID: 487
	public interface IAnimatorControllerPlayable
	{
		// Token: 0x06002032 RID: 8242
		float GetFloat(string name);

		// Token: 0x06002033 RID: 8243
		float GetFloat(int id);

		// Token: 0x06002034 RID: 8244
		void SetFloat(string name, float value);

		// Token: 0x06002035 RID: 8245
		void SetFloat(int id, float value);

		// Token: 0x06002036 RID: 8246
		bool GetBool(string name);

		// Token: 0x06002037 RID: 8247
		bool GetBool(int id);

		// Token: 0x06002038 RID: 8248
		void SetBool(string name, bool value);

		// Token: 0x06002039 RID: 8249
		void SetBool(int id, bool value);

		// Token: 0x0600203A RID: 8250
		int GetInteger(string name);

		// Token: 0x0600203B RID: 8251
		int GetInteger(int id);

		// Token: 0x0600203C RID: 8252
		void SetInteger(string name, int value);

		// Token: 0x0600203D RID: 8253
		void SetInteger(int id, int value);

		// Token: 0x0600203E RID: 8254
		void SetTrigger(string name);

		// Token: 0x0600203F RID: 8255
		void SetTrigger(int id);

		// Token: 0x06002040 RID: 8256
		void ResetTrigger(string name);

		// Token: 0x06002041 RID: 8257
		void ResetTrigger(int id);

		// Token: 0x06002042 RID: 8258
		bool IsParameterControlledByCurve(string name);

		// Token: 0x06002043 RID: 8259
		bool IsParameterControlledByCurve(int id);

		// Token: 0x1700080B RID: 2059
		// (get) Token: 0x06002044 RID: 8260
		int layerCount { get; }

		// Token: 0x06002045 RID: 8261
		string GetLayerName(int layerIndex);

		// Token: 0x06002046 RID: 8262
		int GetLayerIndex(string layerName);

		// Token: 0x06002047 RID: 8263
		float GetLayerWeight(int layerIndex);

		// Token: 0x06002048 RID: 8264
		void SetLayerWeight(int layerIndex, float weight);

		// Token: 0x06002049 RID: 8265
		AnimatorStateInfo GetCurrentAnimatorStateInfo(int layerIndex);

		// Token: 0x0600204A RID: 8266
		AnimatorStateInfo GetNextAnimatorStateInfo(int layerIndex);

		// Token: 0x0600204B RID: 8267
		AnimatorTransitionInfo GetAnimatorTransitionInfo(int layerIndex);

		// Token: 0x0600204C RID: 8268
		AnimatorClipInfo[] GetCurrentAnimatorClipInfo(int layerIndex);

		// Token: 0x0600204D RID: 8269
		AnimatorClipInfo[] GetNextAnimatorClipInfo(int layerIndex);

		// Token: 0x0600204E RID: 8270
		int GetCurrentAnimatorClipInfoCount(int layerIndex);

		// Token: 0x0600204F RID: 8271
		void GetCurrentAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips);

		// Token: 0x06002050 RID: 8272
		int GetNextAnimatorClipInfoCount(int layerIndex);

		// Token: 0x06002051 RID: 8273
		void GetNextAnimatorClipInfo(int layerIndex, List<AnimatorClipInfo> clips);

		// Token: 0x06002052 RID: 8274
		bool IsInTransition(int layerIndex);

		// Token: 0x1700080C RID: 2060
		// (get) Token: 0x06002053 RID: 8275
		int parameterCount { get; }

		// Token: 0x06002054 RID: 8276
		AnimatorControllerParameter GetParameter(int index);

		// Token: 0x06002055 RID: 8277
		void CrossFadeInFixedTime(string stateName, float transitionDuration, int layer, float fixedTime);

		// Token: 0x06002056 RID: 8278
		void CrossFadeInFixedTime(int stateNameHash, float transitionDuration, int layer, float fixedTime);

		// Token: 0x06002057 RID: 8279
		void CrossFade(string stateName, float transitionDuration, int layer, float normalizedTime);

		// Token: 0x06002058 RID: 8280
		void CrossFade(int stateNameHash, float transitionDuration, int layer, float normalizedTime);

		// Token: 0x06002059 RID: 8281
		void PlayInFixedTime(string stateName, int layer, float fixedTime);

		// Token: 0x0600205A RID: 8282
		void PlayInFixedTime(int stateNameHash, int layer, float fixedTime);

		// Token: 0x0600205B RID: 8283
		void Play(string stateName, int layer, float normalizedTime);

		// Token: 0x0600205C RID: 8284
		void Play(int stateNameHash, int layer, float normalizedTime);

		// Token: 0x0600205D RID: 8285
		bool HasState(int layerIndex, int stateID);
	}
}
