﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Experimental.Director
{
	// Token: 0x020003A6 RID: 934
	internal class AnimationPlayableUtilities
	{
		// Token: 0x17000B2A RID: 2858
		// (get) Token: 0x06002FDF RID: 12255 RVA: 0x0004E9AC File Offset: 0x0004CBAC
		public static AnimationPlayable Null
		{
			get
			{
				AnimationPlayable result = default(AnimationPlayable);
				result.handle.m_Version = 10;
				return result;
			}
		}

		// Token: 0x06002FE0 RID: 12256 RVA: 0x0004E9D8 File Offset: 0x0004CBD8
		internal static int AddInputValidated(AnimationPlayable target, Playable input, Type typeofTarget)
		{
			return target.AddInput(input);
		}

		// Token: 0x06002FE1 RID: 12257 RVA: 0x0004E9F8 File Offset: 0x0004CBF8
		internal static bool SetInputValidated(AnimationPlayable target, Playable source, int index, Type typeofTarget)
		{
			return target.SetInput(source, index);
		}

		// Token: 0x06002FE2 RID: 12258 RVA: 0x0004EA18 File Offset: 0x0004CC18
		internal static bool SetInputsValidated(AnimationPlayable target, IEnumerable<Playable> sources, Type typeofTarget)
		{
			return target.SetInputs(sources);
		}

		// Token: 0x06002FE3 RID: 12259 RVA: 0x0004EA38 File Offset: 0x0004CC38
		internal static bool RemoveInputValidated(AnimationPlayable target, int index, Type typeofTarget)
		{
			return target.RemoveInput(index);
		}

		// Token: 0x06002FE4 RID: 12260 RVA: 0x0004EA58 File Offset: 0x0004CC58
		internal static bool RemoveInputValidated(AnimationPlayable target, Playable playable, Type typeofTarget)
		{
			return target.RemoveInput(playable);
		}

		// Token: 0x06002FE5 RID: 12261 RVA: 0x0004EA78 File Offset: 0x0004CC78
		internal static bool RemoveAllInputsValidated(AnimationPlayable target, Type typeofTarget)
		{
			return target.RemoveAllInputs();
		}

		// Token: 0x06002FE6 RID: 12262 RVA: 0x0004EA94 File Offset: 0x0004CC94
		internal static bool SetInputs(AnimationMixerPlayable playable, AnimationClip[] clips)
		{
			if (clips == null)
			{
				throw new NullReferenceException("Parameter clips was null. You need to pass in a valid array of clips.");
			}
			Playables.BeginIgnoreAllocationTracker();
			Playable[] array = new Playable[clips.Length];
			for (int i = 0; i < clips.Length; i++)
			{
				array[i] = AnimationClipPlayable.Create(clips[i]);
				Playable playable2 = array[i];
				Playables.SetPlayableDeleteOnDisconnect(ref playable2, true);
			}
			Playables.EndIgnoreAllocationTracker();
			return AnimationPlayableUtilities.SetInputsValidated(playable, array, typeof(AnimationMixerPlayable));
		}
	}
}
