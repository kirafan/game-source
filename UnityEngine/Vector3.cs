﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000083 RID: 131
	[UsedByNativeCode]
	public struct Vector3
	{
		// Token: 0x06000872 RID: 2162 RVA: 0x0000A2B0 File Offset: 0x000084B0
		public Vector3(float x, float y, float z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		// Token: 0x06000873 RID: 2163 RVA: 0x0000A2C8 File Offset: 0x000084C8
		public Vector3(float x, float y)
		{
			this.x = x;
			this.y = y;
			this.z = 0f;
		}

		// Token: 0x06000874 RID: 2164 RVA: 0x0000A2E4 File Offset: 0x000084E4
		[ThreadAndSerializationSafe]
		public static Vector3 Slerp(Vector3 a, Vector3 b, float t)
		{
			Vector3 result;
			Vector3.INTERNAL_CALL_Slerp(ref a, ref b, t, out result);
			return result;
		}

		// Token: 0x06000875 RID: 2165
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Slerp(ref Vector3 a, ref Vector3 b, float t, out Vector3 value);

		// Token: 0x06000876 RID: 2166 RVA: 0x0000A308 File Offset: 0x00008508
		public static Vector3 SlerpUnclamped(Vector3 a, Vector3 b, float t)
		{
			Vector3 result;
			Vector3.INTERNAL_CALL_SlerpUnclamped(ref a, ref b, t, out result);
			return result;
		}

		// Token: 0x06000877 RID: 2167
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SlerpUnclamped(ref Vector3 a, ref Vector3 b, float t, out Vector3 value);

		// Token: 0x06000878 RID: 2168 RVA: 0x0000A32C File Offset: 0x0000852C
		private static void Internal_OrthoNormalize2(ref Vector3 a, ref Vector3 b)
		{
			Vector3.INTERNAL_CALL_Internal_OrthoNormalize2(ref a, ref b);
		}

		// Token: 0x06000879 RID: 2169
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_OrthoNormalize2(ref Vector3 a, ref Vector3 b);

		// Token: 0x0600087A RID: 2170 RVA: 0x0000A338 File Offset: 0x00008538
		private static void Internal_OrthoNormalize3(ref Vector3 a, ref Vector3 b, ref Vector3 c)
		{
			Vector3.INTERNAL_CALL_Internal_OrthoNormalize3(ref a, ref b, ref c);
		}

		// Token: 0x0600087B RID: 2171
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_OrthoNormalize3(ref Vector3 a, ref Vector3 b, ref Vector3 c);

		// Token: 0x0600087C RID: 2172 RVA: 0x0000A344 File Offset: 0x00008544
		public static void OrthoNormalize(ref Vector3 normal, ref Vector3 tangent)
		{
			Vector3.Internal_OrthoNormalize2(ref normal, ref tangent);
		}

		// Token: 0x0600087D RID: 2173 RVA: 0x0000A350 File Offset: 0x00008550
		public static void OrthoNormalize(ref Vector3 normal, ref Vector3 tangent, ref Vector3 binormal)
		{
			Vector3.Internal_OrthoNormalize3(ref normal, ref tangent, ref binormal);
		}

		// Token: 0x0600087E RID: 2174 RVA: 0x0000A35C File Offset: 0x0000855C
		public static Vector3 RotateTowards(Vector3 current, Vector3 target, float maxRadiansDelta, float maxMagnitudeDelta)
		{
			Vector3 result;
			Vector3.INTERNAL_CALL_RotateTowards(ref current, ref target, maxRadiansDelta, maxMagnitudeDelta, out result);
			return result;
		}

		// Token: 0x0600087F RID: 2175
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_RotateTowards(ref Vector3 current, ref Vector3 target, float maxRadiansDelta, float maxMagnitudeDelta, out Vector3 value);

		// Token: 0x06000880 RID: 2176 RVA: 0x0000A380 File Offset: 0x00008580
		[Obsolete("Use Vector3.ProjectOnPlane instead.")]
		public static Vector3 Exclude(Vector3 excludeThis, Vector3 fromThat)
		{
			return fromThat - Vector3.Project(fromThat, excludeThis);
		}

		// Token: 0x06000881 RID: 2177 RVA: 0x0000A3A4 File Offset: 0x000085A4
		public static Vector3 Lerp(Vector3 a, Vector3 b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Vector3(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t);
		}

		// Token: 0x06000882 RID: 2178 RVA: 0x0000A414 File Offset: 0x00008614
		public static Vector3 LerpUnclamped(Vector3 a, Vector3 b, float t)
		{
			return new Vector3(a.x + (b.x - a.x) * t, a.y + (b.y - a.y) * t, a.z + (b.z - a.z) * t);
		}

		// Token: 0x06000883 RID: 2179 RVA: 0x0000A47C File Offset: 0x0000867C
		public static Vector3 MoveTowards(Vector3 current, Vector3 target, float maxDistanceDelta)
		{
			Vector3 a = target - current;
			float magnitude = a.magnitude;
			Vector3 result;
			if (magnitude <= maxDistanceDelta || magnitude == 0f)
			{
				result = target;
			}
			else
			{
				result = current + a / magnitude * maxDistanceDelta;
			}
			return result;
		}

		// Token: 0x06000884 RID: 2180 RVA: 0x0000A4D0 File Offset: 0x000086D0
		[ExcludeFromDocs]
		public static Vector3 SmoothDamp(Vector3 current, Vector3 target, ref Vector3 currentVelocity, float smoothTime, float maxSpeed)
		{
			float deltaTime = Time.deltaTime;
			return Vector3.SmoothDamp(current, target, ref currentVelocity, smoothTime, maxSpeed, deltaTime);
		}

		// Token: 0x06000885 RID: 2181 RVA: 0x0000A4F8 File Offset: 0x000086F8
		[ExcludeFromDocs]
		public static Vector3 SmoothDamp(Vector3 current, Vector3 target, ref Vector3 currentVelocity, float smoothTime)
		{
			float deltaTime = Time.deltaTime;
			float positiveInfinity = float.PositiveInfinity;
			return Vector3.SmoothDamp(current, target, ref currentVelocity, smoothTime, positiveInfinity, deltaTime);
		}

		// Token: 0x06000886 RID: 2182 RVA: 0x0000A524 File Offset: 0x00008724
		public static Vector3 SmoothDamp(Vector3 current, Vector3 target, ref Vector3 currentVelocity, float smoothTime, [DefaultValue("Mathf.Infinity")] float maxSpeed, [DefaultValue("Time.deltaTime")] float deltaTime)
		{
			smoothTime = Mathf.Max(0.0001f, smoothTime);
			float num = 2f / smoothTime;
			float num2 = num * deltaTime;
			float d = 1f / (1f + num2 + 0.48f * num2 * num2 + 0.235f * num2 * num2 * num2);
			Vector3 vector = current - target;
			Vector3 vector2 = target;
			float maxLength = maxSpeed * smoothTime;
			vector = Vector3.ClampMagnitude(vector, maxLength);
			target = current - vector;
			Vector3 vector3 = (currentVelocity + num * vector) * deltaTime;
			currentVelocity = (currentVelocity - num * vector3) * d;
			Vector3 vector4 = target + (vector + vector3) * d;
			if (Vector3.Dot(vector2 - current, vector4 - vector2) > 0f)
			{
				vector4 = vector2;
				currentVelocity = (vector4 - vector2) / deltaTime;
			}
			return vector4;
		}

		// Token: 0x17000216 RID: 534
		public float this[int index]
		{
			get
			{
				float result;
				switch (index)
				{
				case 0:
					result = this.x;
					break;
				case 1:
					result = this.y;
					break;
				case 2:
					result = this.z;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector3 index!");
				}
				return result;
			}
			set
			{
				switch (index)
				{
				case 0:
					this.x = value;
					break;
				case 1:
					this.y = value;
					break;
				case 2:
					this.z = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector3 index!");
				}
			}
		}

		// Token: 0x06000889 RID: 2185 RVA: 0x0000A6D8 File Offset: 0x000088D8
		public void Set(float new_x, float new_y, float new_z)
		{
			this.x = new_x;
			this.y = new_y;
			this.z = new_z;
		}

		// Token: 0x0600088A RID: 2186 RVA: 0x0000A6F0 File Offset: 0x000088F0
		public static Vector3 Scale(Vector3 a, Vector3 b)
		{
			return new Vector3(a.x * b.x, a.y * b.y, a.z * b.z);
		}

		// Token: 0x0600088B RID: 2187 RVA: 0x0000A738 File Offset: 0x00008938
		public void Scale(Vector3 scale)
		{
			this.x *= scale.x;
			this.y *= scale.y;
			this.z *= scale.z;
		}

		// Token: 0x0600088C RID: 2188 RVA: 0x0000A778 File Offset: 0x00008978
		public static Vector3 Cross(Vector3 lhs, Vector3 rhs)
		{
			return new Vector3(lhs.y * rhs.z - lhs.z * rhs.y, lhs.z * rhs.x - lhs.x * rhs.z, lhs.x * rhs.y - lhs.y * rhs.x);
		}

		// Token: 0x0600088D RID: 2189 RVA: 0x0000A7F0 File Offset: 0x000089F0
		public override int GetHashCode()
		{
			return this.x.GetHashCode() ^ this.y.GetHashCode() << 2 ^ this.z.GetHashCode() >> 2;
		}

		// Token: 0x0600088E RID: 2190 RVA: 0x0000A840 File Offset: 0x00008A40
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Vector3))
			{
				result = false;
			}
			else
			{
				Vector3 vector = (Vector3)other;
				result = (this.x.Equals(vector.x) && this.y.Equals(vector.y) && this.z.Equals(vector.z));
			}
			return result;
		}

		// Token: 0x0600088F RID: 2191 RVA: 0x0000A8B4 File Offset: 0x00008AB4
		public static Vector3 Reflect(Vector3 inDirection, Vector3 inNormal)
		{
			return -2f * Vector3.Dot(inNormal, inDirection) * inNormal + inDirection;
		}

		// Token: 0x06000890 RID: 2192 RVA: 0x0000A8E4 File Offset: 0x00008AE4
		public static Vector3 Normalize(Vector3 value)
		{
			float num = Vector3.Magnitude(value);
			Vector3 result;
			if (num > 1E-05f)
			{
				result = value / num;
			}
			else
			{
				result = Vector3.zero;
			}
			return result;
		}

		// Token: 0x06000891 RID: 2193 RVA: 0x0000A920 File Offset: 0x00008B20
		public void Normalize()
		{
			float num = Vector3.Magnitude(this);
			if (num > 1E-05f)
			{
				this /= num;
			}
			else
			{
				this = Vector3.zero;
			}
		}

		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06000892 RID: 2194 RVA: 0x0000A968 File Offset: 0x00008B68
		public Vector3 normalized
		{
			get
			{
				return Vector3.Normalize(this);
			}
		}

		// Token: 0x06000893 RID: 2195 RVA: 0x0000A988 File Offset: 0x00008B88
		public static float Dot(Vector3 lhs, Vector3 rhs)
		{
			return lhs.x * rhs.x + lhs.y * rhs.y + lhs.z * rhs.z;
		}

		// Token: 0x06000894 RID: 2196 RVA: 0x0000A9CC File Offset: 0x00008BCC
		public static Vector3 Project(Vector3 vector, Vector3 onNormal)
		{
			float num = Vector3.Dot(onNormal, onNormal);
			Vector3 result;
			if (num < Mathf.Epsilon)
			{
				result = Vector3.zero;
			}
			else
			{
				result = onNormal * Vector3.Dot(vector, onNormal) / num;
			}
			return result;
		}

		// Token: 0x06000895 RID: 2197 RVA: 0x0000AA14 File Offset: 0x00008C14
		public static Vector3 ProjectOnPlane(Vector3 vector, Vector3 planeNormal)
		{
			return vector - Vector3.Project(vector, planeNormal);
		}

		// Token: 0x06000896 RID: 2198 RVA: 0x0000AA38 File Offset: 0x00008C38
		public static float Angle(Vector3 from, Vector3 to)
		{
			return Mathf.Acos(Mathf.Clamp(Vector3.Dot(from.normalized, to.normalized), -1f, 1f)) * 57.29578f;
		}

		// Token: 0x06000897 RID: 2199 RVA: 0x0000AA7C File Offset: 0x00008C7C
		public static float Distance(Vector3 a, Vector3 b)
		{
			Vector3 vector = new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
			return Mathf.Sqrt(vector.x * vector.x + vector.y * vector.y + vector.z * vector.z);
		}

		// Token: 0x06000898 RID: 2200 RVA: 0x0000AAFC File Offset: 0x00008CFC
		public static Vector3 ClampMagnitude(Vector3 vector, float maxLength)
		{
			Vector3 result;
			if (vector.sqrMagnitude > maxLength * maxLength)
			{
				result = vector.normalized * maxLength;
			}
			else
			{
				result = vector;
			}
			return result;
		}

		// Token: 0x06000899 RID: 2201 RVA: 0x0000AB34 File Offset: 0x00008D34
		public static float Magnitude(Vector3 a)
		{
			return Mathf.Sqrt(a.x * a.x + a.y * a.y + a.z * a.z);
		}

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x0600089A RID: 2202 RVA: 0x0000AB80 File Offset: 0x00008D80
		public float magnitude
		{
			get
			{
				return Mathf.Sqrt(this.x * this.x + this.y * this.y + this.z * this.z);
			}
		}

		// Token: 0x0600089B RID: 2203 RVA: 0x0000ABC4 File Offset: 0x00008DC4
		public static float SqrMagnitude(Vector3 a)
		{
			return a.x * a.x + a.y * a.y + a.z * a.z;
		}

		// Token: 0x17000219 RID: 537
		// (get) Token: 0x0600089C RID: 2204 RVA: 0x0000AC08 File Offset: 0x00008E08
		public float sqrMagnitude
		{
			get
			{
				return this.x * this.x + this.y * this.y + this.z * this.z;
			}
		}

		// Token: 0x0600089D RID: 2205 RVA: 0x0000AC48 File Offset: 0x00008E48
		public static Vector3 Min(Vector3 lhs, Vector3 rhs)
		{
			return new Vector3(Mathf.Min(lhs.x, rhs.x), Mathf.Min(lhs.y, rhs.y), Mathf.Min(lhs.z, rhs.z));
		}

		// Token: 0x0600089E RID: 2206 RVA: 0x0000AC9C File Offset: 0x00008E9C
		public static Vector3 Max(Vector3 lhs, Vector3 rhs)
		{
			return new Vector3(Mathf.Max(lhs.x, rhs.x), Mathf.Max(lhs.y, rhs.y), Mathf.Max(lhs.z, rhs.z));
		}

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x0600089F RID: 2207 RVA: 0x0000ACF0 File Offset: 0x00008EF0
		public static Vector3 zero
		{
			get
			{
				return new Vector3(0f, 0f, 0f);
			}
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x060008A0 RID: 2208 RVA: 0x0000AD1C File Offset: 0x00008F1C
		public static Vector3 one
		{
			get
			{
				return new Vector3(1f, 1f, 1f);
			}
		}

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x060008A1 RID: 2209 RVA: 0x0000AD48 File Offset: 0x00008F48
		public static Vector3 forward
		{
			get
			{
				return new Vector3(0f, 0f, 1f);
			}
		}

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x060008A2 RID: 2210 RVA: 0x0000AD74 File Offset: 0x00008F74
		public static Vector3 back
		{
			get
			{
				return new Vector3(0f, 0f, -1f);
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x060008A3 RID: 2211 RVA: 0x0000ADA0 File Offset: 0x00008FA0
		public static Vector3 up
		{
			get
			{
				return new Vector3(0f, 1f, 0f);
			}
		}

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x060008A4 RID: 2212 RVA: 0x0000ADCC File Offset: 0x00008FCC
		public static Vector3 down
		{
			get
			{
				return new Vector3(0f, -1f, 0f);
			}
		}

		// Token: 0x17000220 RID: 544
		// (get) Token: 0x060008A5 RID: 2213 RVA: 0x0000ADF8 File Offset: 0x00008FF8
		public static Vector3 left
		{
			get
			{
				return new Vector3(-1f, 0f, 0f);
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x060008A6 RID: 2214 RVA: 0x0000AE24 File Offset: 0x00009024
		public static Vector3 right
		{
			get
			{
				return new Vector3(1f, 0f, 0f);
			}
		}

		// Token: 0x060008A7 RID: 2215 RVA: 0x0000AE50 File Offset: 0x00009050
		public static Vector3 operator +(Vector3 a, Vector3 b)
		{
			return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
		}

		// Token: 0x060008A8 RID: 2216 RVA: 0x0000AE98 File Offset: 0x00009098
		public static Vector3 operator -(Vector3 a, Vector3 b)
		{
			return new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
		}

		// Token: 0x060008A9 RID: 2217 RVA: 0x0000AEE0 File Offset: 0x000090E0
		public static Vector3 operator -(Vector3 a)
		{
			return new Vector3(-a.x, -a.y, -a.z);
		}

		// Token: 0x060008AA RID: 2218 RVA: 0x0000AF14 File Offset: 0x00009114
		public static Vector3 operator *(Vector3 a, float d)
		{
			return new Vector3(a.x * d, a.y * d, a.z * d);
		}

		// Token: 0x060008AB RID: 2219 RVA: 0x0000AF4C File Offset: 0x0000914C
		public static Vector3 operator *(float d, Vector3 a)
		{
			return new Vector3(a.x * d, a.y * d, a.z * d);
		}

		// Token: 0x060008AC RID: 2220 RVA: 0x0000AF84 File Offset: 0x00009184
		public static Vector3 operator /(Vector3 a, float d)
		{
			return new Vector3(a.x / d, a.y / d, a.z / d);
		}

		// Token: 0x060008AD RID: 2221 RVA: 0x0000AFBC File Offset: 0x000091BC
		public static bool operator ==(Vector3 lhs, Vector3 rhs)
		{
			return Vector3.SqrMagnitude(lhs - rhs) < 9.9999994E-11f;
		}

		// Token: 0x060008AE RID: 2222 RVA: 0x0000AFE4 File Offset: 0x000091E4
		public static bool operator !=(Vector3 lhs, Vector3 rhs)
		{
			return Vector3.SqrMagnitude(lhs - rhs) >= 9.9999994E-11f;
		}

		// Token: 0x060008AF RID: 2223 RVA: 0x0000B010 File Offset: 0x00009210
		public override string ToString()
		{
			return UnityString.Format("({0:F1}, {1:F1}, {2:F1})", new object[]
			{
				this.x,
				this.y,
				this.z
			});
		}

		// Token: 0x060008B0 RID: 2224 RVA: 0x0000B060 File Offset: 0x00009260
		public string ToString(string format)
		{
			return UnityString.Format("({0}, {1}, {2})", new object[]
			{
				this.x.ToString(format),
				this.y.ToString(format),
				this.z.ToString(format)
			});
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x060008B1 RID: 2225 RVA: 0x0000B0B4 File Offset: 0x000092B4
		[Obsolete("Use Vector3.forward instead.")]
		public static Vector3 fwd
		{
			get
			{
				return new Vector3(0f, 0f, 1f);
			}
		}

		// Token: 0x060008B2 RID: 2226 RVA: 0x0000B0E0 File Offset: 0x000092E0
		[Obsolete("Use Vector3.Angle instead. AngleBetween uses radians instead of degrees and was deprecated for this reason")]
		public static float AngleBetween(Vector3 from, Vector3 to)
		{
			return Mathf.Acos(Mathf.Clamp(Vector3.Dot(from.normalized, to.normalized), -1f, 1f));
		}

		// Token: 0x040000F9 RID: 249
		public const float kEpsilon = 1E-05f;

		// Token: 0x040000FA RID: 250
		public float x;

		// Token: 0x040000FB RID: 251
		public float y;

		// Token: 0x040000FC RID: 252
		public float z;
	}
}
