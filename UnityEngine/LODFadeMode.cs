﻿using System;

namespace UnityEngine
{
	// Token: 0x02000080 RID: 128
	public enum LODFadeMode
	{
		// Token: 0x040000F3 RID: 243
		None,
		// Token: 0x040000F4 RID: 244
		CrossFade,
		// Token: 0x040000F5 RID: 245
		SpeedTree
	}
}
