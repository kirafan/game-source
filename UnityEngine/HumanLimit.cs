﻿using System;

namespace UnityEngine
{
	// Token: 0x020001D5 RID: 469
	public struct HumanLimit
	{
		// Token: 0x170007F4 RID: 2036
		// (get) Token: 0x06001FE6 RID: 8166 RVA: 0x00024318 File Offset: 0x00022518
		// (set) Token: 0x06001FE7 RID: 8167 RVA: 0x0002433C File Offset: 0x0002253C
		public bool useDefaultValues
		{
			get
			{
				return this.m_UseDefaultValues != 0;
			}
			set
			{
				this.m_UseDefaultValues = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x170007F5 RID: 2037
		// (get) Token: 0x06001FE8 RID: 8168 RVA: 0x00024354 File Offset: 0x00022554
		// (set) Token: 0x06001FE9 RID: 8169 RVA: 0x00024370 File Offset: 0x00022570
		public Vector3 min
		{
			get
			{
				return this.m_Min;
			}
			set
			{
				this.m_Min = value;
			}
		}

		// Token: 0x170007F6 RID: 2038
		// (get) Token: 0x06001FEA RID: 8170 RVA: 0x0002437C File Offset: 0x0002257C
		// (set) Token: 0x06001FEB RID: 8171 RVA: 0x00024398 File Offset: 0x00022598
		public Vector3 max
		{
			get
			{
				return this.m_Max;
			}
			set
			{
				this.m_Max = value;
			}
		}

		// Token: 0x170007F7 RID: 2039
		// (get) Token: 0x06001FEC RID: 8172 RVA: 0x000243A4 File Offset: 0x000225A4
		// (set) Token: 0x06001FED RID: 8173 RVA: 0x000243C0 File Offset: 0x000225C0
		public Vector3 center
		{
			get
			{
				return this.m_Center;
			}
			set
			{
				this.m_Center = value;
			}
		}

		// Token: 0x170007F8 RID: 2040
		// (get) Token: 0x06001FEE RID: 8174 RVA: 0x000243CC File Offset: 0x000225CC
		// (set) Token: 0x06001FEF RID: 8175 RVA: 0x000243E8 File Offset: 0x000225E8
		public float axisLength
		{
			get
			{
				return this.m_AxisLength;
			}
			set
			{
				this.m_AxisLength = value;
			}
		}

		// Token: 0x04000511 RID: 1297
		private Vector3 m_Min;

		// Token: 0x04000512 RID: 1298
		private Vector3 m_Max;

		// Token: 0x04000513 RID: 1299
		private Vector3 m_Center;

		// Token: 0x04000514 RID: 1300
		private float m_AxisLength;

		// Token: 0x04000515 RID: 1301
		private int m_UseDefaultValues;
	}
}
