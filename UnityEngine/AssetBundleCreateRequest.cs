﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000010 RID: 16
	[RequiredByNativeCode]
	public sealed class AssetBundleCreateRequest : AsyncOperation
	{
		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600016F RID: 367
		public extern AssetBundle assetBundle { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000170 RID: 368
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void DisableCompatibilityChecks();
	}
}
