﻿using System;

namespace UnityEngine
{
	// Token: 0x020002FE RID: 766
	public enum CubemapFace
	{
		// Token: 0x04000B92 RID: 2962
		Unknown = -1,
		// Token: 0x04000B93 RID: 2963
		PositiveX,
		// Token: 0x04000B94 RID: 2964
		NegativeX,
		// Token: 0x04000B95 RID: 2965
		PositiveY,
		// Token: 0x04000B96 RID: 2966
		NegativeY,
		// Token: 0x04000B97 RID: 2967
		PositiveZ,
		// Token: 0x04000B98 RID: 2968
		NegativeZ
	}
}
