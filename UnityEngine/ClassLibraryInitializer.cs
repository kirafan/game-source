﻿using System;

namespace UnityEngine
{
	// Token: 0x020002CE RID: 718
	internal static class ClassLibraryInitializer
	{
		// Token: 0x06002C4C RID: 11340 RVA: 0x00045214 File Offset: 0x00043414
		private static void Init()
		{
			UnityLogWriter.Init();
		}
	}
}
