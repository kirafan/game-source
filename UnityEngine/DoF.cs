﻿using System;

namespace UnityEngine
{
	// Token: 0x020001DF RID: 479
	internal enum DoF
	{
		// Token: 0x04000555 RID: 1365
		BodyDoFStart,
		// Token: 0x04000556 RID: 1366
		HeadDoFStart = 6,
		// Token: 0x04000557 RID: 1367
		LeftLegDoFStart = 18,
		// Token: 0x04000558 RID: 1368
		RightLegDoFStart = 26,
		// Token: 0x04000559 RID: 1369
		LeftArmDoFStart = 34,
		// Token: 0x0400055A RID: 1370
		RightArmDoFStart = 43,
		// Token: 0x0400055B RID: 1371
		LeftThumbDoFStart = 52,
		// Token: 0x0400055C RID: 1372
		LeftIndexDoFStart = 56,
		// Token: 0x0400055D RID: 1373
		LeftMiddleDoFStart = 60,
		// Token: 0x0400055E RID: 1374
		LeftRingDoFStart = 64,
		// Token: 0x0400055F RID: 1375
		LeftLittleDoFStart = 68,
		// Token: 0x04000560 RID: 1376
		RightThumbDoFStart = 72,
		// Token: 0x04000561 RID: 1377
		RightIndexDoFStart = 76,
		// Token: 0x04000562 RID: 1378
		RightMiddleDoFStart = 80,
		// Token: 0x04000563 RID: 1379
		RightRingDoFStart = 84,
		// Token: 0x04000564 RID: 1380
		RightLittleDoFStart = 88,
		// Token: 0x04000565 RID: 1381
		LastDoF = 92
	}
}
