﻿using System;

namespace UnityEngine
{
	// Token: 0x0200001A RID: 26
	public sealed class WaitWhile : CustomYieldInstruction
	{
		// Token: 0x060001EC RID: 492 RVA: 0x00004F04 File Offset: 0x00003104
		public WaitWhile(Func<bool> predicate)
		{
			this.m_Predicate = predicate;
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060001ED RID: 493 RVA: 0x00004F14 File Offset: 0x00003114
		public override bool keepWaiting
		{
			get
			{
				return this.m_Predicate();
			}
		}

		// Token: 0x04000029 RID: 41
		private Func<bool> m_Predicate;
	}
}
