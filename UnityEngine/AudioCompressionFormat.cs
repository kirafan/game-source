﻿using System;

namespace UnityEngine
{
	// Token: 0x0200019C RID: 412
	public enum AudioCompressionFormat
	{
		// Token: 0x0400045A RID: 1114
		PCM,
		// Token: 0x0400045B RID: 1115
		Vorbis,
		// Token: 0x0400045C RID: 1116
		ADPCM,
		// Token: 0x0400045D RID: 1117
		MP3,
		// Token: 0x0400045E RID: 1118
		VAG,
		// Token: 0x0400045F RID: 1119
		HEVAG,
		// Token: 0x04000460 RID: 1120
		XMA,
		// Token: 0x04000461 RID: 1121
		AAC,
		// Token: 0x04000462 RID: 1122
		GCADPCM,
		// Token: 0x04000463 RID: 1123
		ATRAC9
	}
}
