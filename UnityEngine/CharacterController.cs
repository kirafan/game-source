﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000156 RID: 342
	public sealed class CharacterController : Collider
	{
		// Token: 0x060018C4 RID: 6340 RVA: 0x0001EE28 File Offset: 0x0001D028
		public bool SimpleMove(Vector3 speed)
		{
			return CharacterController.INTERNAL_CALL_SimpleMove(this, ref speed);
		}

		// Token: 0x060018C5 RID: 6341
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool INTERNAL_CALL_SimpleMove(CharacterController self, ref Vector3 speed);

		// Token: 0x060018C6 RID: 6342 RVA: 0x0001EE48 File Offset: 0x0001D048
		public CollisionFlags Move(Vector3 motion)
		{
			return CharacterController.INTERNAL_CALL_Move(this, ref motion);
		}

		// Token: 0x060018C7 RID: 6343
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern CollisionFlags INTERNAL_CALL_Move(CharacterController self, ref Vector3 motion);

		// Token: 0x170005C5 RID: 1477
		// (get) Token: 0x060018C8 RID: 6344
		public extern bool isGrounded { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170005C6 RID: 1478
		// (get) Token: 0x060018C9 RID: 6345 RVA: 0x0001EE68 File Offset: 0x0001D068
		public Vector3 velocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_velocity(out result);
				return result;
			}
		}

		// Token: 0x060018CA RID: 6346
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_velocity(out Vector3 value);

		// Token: 0x170005C7 RID: 1479
		// (get) Token: 0x060018CB RID: 6347
		public extern CollisionFlags collisionFlags { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170005C8 RID: 1480
		// (get) Token: 0x060018CC RID: 6348
		// (set) Token: 0x060018CD RID: 6349
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005C9 RID: 1481
		// (get) Token: 0x060018CE RID: 6350
		// (set) Token: 0x060018CF RID: 6351
		public extern float height { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005CA RID: 1482
		// (get) Token: 0x060018D0 RID: 6352 RVA: 0x0001EE88 File Offset: 0x0001D088
		// (set) Token: 0x060018D1 RID: 6353 RVA: 0x0001EEA8 File Offset: 0x0001D0A8
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_center(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_center(ref value);
			}
		}

		// Token: 0x060018D2 RID: 6354
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_center(out Vector3 value);

		// Token: 0x060018D3 RID: 6355
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_center(ref Vector3 value);

		// Token: 0x170005CB RID: 1483
		// (get) Token: 0x060018D4 RID: 6356
		// (set) Token: 0x060018D5 RID: 6357
		public extern float slopeLimit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005CC RID: 1484
		// (get) Token: 0x060018D6 RID: 6358
		// (set) Token: 0x060018D7 RID: 6359
		public extern float stepOffset { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005CD RID: 1485
		// (get) Token: 0x060018D8 RID: 6360
		// (set) Token: 0x060018D9 RID: 6361
		public extern float skinWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005CE RID: 1486
		// (get) Token: 0x060018DA RID: 6362
		// (set) Token: 0x060018DB RID: 6363
		public extern bool detectCollisions { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005CF RID: 1487
		// (get) Token: 0x060018DC RID: 6364
		// (set) Token: 0x060018DD RID: 6365
		public extern bool enableOverlapRecovery { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
