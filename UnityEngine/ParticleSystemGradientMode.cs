﻿using System;

namespace UnityEngine
{
	// Token: 0x020000FD RID: 253
	public enum ParticleSystemGradientMode
	{
		// Token: 0x040002A3 RID: 675
		Color,
		// Token: 0x040002A4 RID: 676
		Gradient,
		// Token: 0x040002A5 RID: 677
		TwoColors,
		// Token: 0x040002A6 RID: 678
		TwoGradients,
		// Token: 0x040002A7 RID: 679
		RandomColor
	}
}
