﻿using System;

namespace UnityEngine
{
	// Token: 0x020002CB RID: 715
	public enum DeviceType
	{
		// Token: 0x04000AAC RID: 2732
		Unknown,
		// Token: 0x04000AAD RID: 2733
		Handheld,
		// Token: 0x04000AAE RID: 2734
		Console,
		// Token: 0x04000AAF RID: 2735
		Desktop
	}
}
