﻿using System;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200023F RID: 575
	public class TextEditor
	{
		// Token: 0x06002713 RID: 10003 RVA: 0x00035AA4 File Offset: 0x00033CA4
		[RequiredByNativeCode]
		public TextEditor()
		{
		}

		// Token: 0x17000967 RID: 2407
		// (get) Token: 0x06002714 RID: 10004 RVA: 0x00035B34 File Offset: 0x00033D34
		// (set) Token: 0x06002715 RID: 10005 RVA: 0x00035B50 File Offset: 0x00033D50
		[Obsolete("Please use 'text' instead of 'content'", false)]
		public GUIContent content
		{
			get
			{
				return this.m_Content;
			}
			set
			{
				this.m_Content = value;
			}
		}

		// Token: 0x17000968 RID: 2408
		// (get) Token: 0x06002716 RID: 10006 RVA: 0x00035B5C File Offset: 0x00033D5C
		// (set) Token: 0x06002717 RID: 10007 RVA: 0x00035B7C File Offset: 0x00033D7C
		public string text
		{
			get
			{
				return this.m_Content.text;
			}
			set
			{
				this.m_Content.text = value;
				this.ClampTextIndex(ref this.m_CursorIndex);
				this.ClampTextIndex(ref this.m_SelectIndex);
			}
		}

		// Token: 0x17000969 RID: 2409
		// (get) Token: 0x06002718 RID: 10008 RVA: 0x00035BA4 File Offset: 0x00033DA4
		// (set) Token: 0x06002719 RID: 10009 RVA: 0x00035BC0 File Offset: 0x00033DC0
		public Rect position
		{
			get
			{
				return this.m_Position;
			}
			set
			{
				if (!(this.m_Position == value))
				{
					this.m_Position = value;
					this.UpdateScrollOffset();
				}
			}
		}

		// Token: 0x1700096A RID: 2410
		// (get) Token: 0x0600271A RID: 10010 RVA: 0x00035BE8 File Offset: 0x00033DE8
		// (set) Token: 0x0600271B RID: 10011 RVA: 0x00035C04 File Offset: 0x00033E04
		public int cursorIndex
		{
			get
			{
				return this.m_CursorIndex;
			}
			set
			{
				int cursorIndex = this.m_CursorIndex;
				this.m_CursorIndex = value;
				this.ClampTextIndex(ref this.m_CursorIndex);
				if (this.m_CursorIndex != cursorIndex)
				{
					this.m_RevealCursor = true;
				}
			}
		}

		// Token: 0x1700096B RID: 2411
		// (get) Token: 0x0600271C RID: 10012 RVA: 0x00035C40 File Offset: 0x00033E40
		// (set) Token: 0x0600271D RID: 10013 RVA: 0x00035C5C File Offset: 0x00033E5C
		public int selectIndex
		{
			get
			{
				return this.m_SelectIndex;
			}
			set
			{
				this.m_SelectIndex = value;
				this.ClampTextIndex(ref this.m_SelectIndex);
			}
		}

		// Token: 0x0600271E RID: 10014 RVA: 0x00035C74 File Offset: 0x00033E74
		private void ClearCursorPos()
		{
			this.hasHorizontalCursorPos = false;
			this.m_iAltCursorPos = -1;
		}

		// Token: 0x0600271F RID: 10015 RVA: 0x00035C88 File Offset: 0x00033E88
		public void OnFocus()
		{
			if (this.multiline)
			{
				int num = 0;
				this.selectIndex = num;
				this.cursorIndex = num;
			}
			else
			{
				this.SelectAll();
			}
			this.m_HasFocus = true;
		}

		// Token: 0x06002720 RID: 10016 RVA: 0x00035CC4 File Offset: 0x00033EC4
		public void OnLostFocus()
		{
			this.m_HasFocus = false;
			this.scrollOffset = Vector2.zero;
		}

		// Token: 0x06002721 RID: 10017 RVA: 0x00035CDC File Offset: 0x00033EDC
		private void GrabGraphicalCursorPos()
		{
			if (!this.hasHorizontalCursorPos)
			{
				this.graphicalCursorPos = this.style.GetCursorPixelPosition(this.position, this.m_Content, this.cursorIndex);
				this.graphicalSelectCursorPos = this.style.GetCursorPixelPosition(this.position, this.m_Content, this.selectIndex);
				this.hasHorizontalCursorPos = false;
			}
		}

		// Token: 0x06002722 RID: 10018 RVA: 0x00035D44 File Offset: 0x00033F44
		public bool HandleKeyEvent(Event e)
		{
			this.InitKeyActions();
			EventModifiers modifiers = e.modifiers;
			e.modifiers &= ~EventModifiers.CapsLock;
			bool result;
			if (TextEditor.s_Keyactions.ContainsKey(e))
			{
				TextEditor.TextEditOp operation = TextEditor.s_Keyactions[e];
				this.PerformOperation(operation);
				e.modifiers = modifiers;
				result = true;
			}
			else
			{
				e.modifiers = modifiers;
				result = false;
			}
			return result;
		}

		// Token: 0x06002723 RID: 10019 RVA: 0x00035DB0 File Offset: 0x00033FB0
		public bool DeleteLineBack()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else
			{
				int num = this.cursorIndex;
				int num2 = num;
				while (num2-- != 0)
				{
					if (this.text[num2] == '\n')
					{
						num = num2 + 1;
						break;
					}
				}
				if (num2 == -1)
				{
					num = 0;
				}
				if (this.cursorIndex != num)
				{
					this.m_Content.text = this.text.Remove(num, this.cursorIndex - num);
					int num3 = num;
					this.cursorIndex = num3;
					this.selectIndex = num3;
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06002724 RID: 10020 RVA: 0x00035E64 File Offset: 0x00034064
		public bool DeleteWordBack()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else
			{
				int num = this.FindEndOfPreviousWord(this.cursorIndex);
				if (this.cursorIndex != num)
				{
					this.m_Content.text = this.text.Remove(num, this.cursorIndex - num);
					int num2 = num;
					this.cursorIndex = num2;
					this.selectIndex = num2;
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06002725 RID: 10021 RVA: 0x00035EE4 File Offset: 0x000340E4
		public bool DeleteWordForward()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else
			{
				int num = this.FindStartOfNextWord(this.cursorIndex);
				if (this.cursorIndex < this.text.Length)
				{
					this.m_Content.text = this.text.Remove(this.cursorIndex, num - this.cursorIndex);
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06002726 RID: 10022 RVA: 0x00035F64 File Offset: 0x00034164
		public bool Delete()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else if (this.cursorIndex < this.text.Length)
			{
				this.m_Content.text = this.text.Remove(this.cursorIndex, 1);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06002727 RID: 10023 RVA: 0x00035FD0 File Offset: 0x000341D0
		public bool CanPaste()
		{
			return GUIUtility.systemCopyBuffer.Length != 0;
		}

		// Token: 0x06002728 RID: 10024 RVA: 0x00035FF8 File Offset: 0x000341F8
		public bool Backspace()
		{
			bool result;
			if (this.hasSelection)
			{
				this.DeleteSelection();
				result = true;
			}
			else if (this.cursorIndex > 0)
			{
				this.m_Content.text = this.text.Remove(this.cursorIndex - 1, 1);
				int num = this.cursorIndex - 1;
				this.cursorIndex = num;
				this.selectIndex = num;
				this.ClearCursorPos();
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06002729 RID: 10025 RVA: 0x00036078 File Offset: 0x00034278
		public void SelectAll()
		{
			this.cursorIndex = 0;
			this.selectIndex = this.text.Length;
			this.ClearCursorPos();
		}

		// Token: 0x0600272A RID: 10026 RVA: 0x0003609C File Offset: 0x0003429C
		public void SelectNone()
		{
			this.selectIndex = this.cursorIndex;
			this.ClearCursorPos();
		}

		// Token: 0x1700096C RID: 2412
		// (get) Token: 0x0600272B RID: 10027 RVA: 0x000360B4 File Offset: 0x000342B4
		public bool hasSelection
		{
			get
			{
				return this.cursorIndex != this.selectIndex;
			}
		}

		// Token: 0x1700096D RID: 2413
		// (get) Token: 0x0600272C RID: 10028 RVA: 0x000360DC File Offset: 0x000342DC
		public string SelectedText
		{
			get
			{
				string result;
				if (this.cursorIndex == this.selectIndex)
				{
					result = "";
				}
				else if (this.cursorIndex < this.selectIndex)
				{
					result = this.text.Substring(this.cursorIndex, this.selectIndex - this.cursorIndex);
				}
				else
				{
					result = this.text.Substring(this.selectIndex, this.cursorIndex - this.selectIndex);
				}
				return result;
			}
		}

		// Token: 0x0600272D RID: 10029 RVA: 0x00036160 File Offset: 0x00034360
		public bool DeleteSelection()
		{
			bool result;
			if (this.cursorIndex == this.selectIndex)
			{
				result = false;
			}
			else
			{
				if (this.cursorIndex < this.selectIndex)
				{
					this.m_Content.text = this.text.Substring(0, this.cursorIndex) + this.text.Substring(this.selectIndex, this.text.Length - this.selectIndex);
					this.selectIndex = this.cursorIndex;
				}
				else
				{
					this.m_Content.text = this.text.Substring(0, this.selectIndex) + this.text.Substring(this.cursorIndex, this.text.Length - this.cursorIndex);
					this.cursorIndex = this.selectIndex;
				}
				this.ClearCursorPos();
				result = true;
			}
			return result;
		}

		// Token: 0x0600272E RID: 10030 RVA: 0x00036250 File Offset: 0x00034450
		public void ReplaceSelection(string replace)
		{
			this.DeleteSelection();
			this.m_Content.text = this.text.Insert(this.cursorIndex, replace);
			this.selectIndex = (this.cursorIndex += replace.Length);
			this.ClearCursorPos();
		}

		// Token: 0x0600272F RID: 10031 RVA: 0x000362A4 File Offset: 0x000344A4
		public void Insert(char c)
		{
			this.ReplaceSelection(c.ToString());
		}

		// Token: 0x06002730 RID: 10032 RVA: 0x000362BC File Offset: 0x000344BC
		public void MoveSelectionToAltCursor()
		{
			if (this.m_iAltCursorPos != -1)
			{
				int iAltCursorPos = this.m_iAltCursorPos;
				string selectedText = this.SelectedText;
				this.m_Content.text = this.text.Insert(iAltCursorPos, selectedText);
				if (iAltCursorPos < this.cursorIndex)
				{
					this.cursorIndex += selectedText.Length;
					this.selectIndex += selectedText.Length;
				}
				this.DeleteSelection();
				int num = iAltCursorPos;
				this.cursorIndex = num;
				this.selectIndex = num;
				this.ClearCursorPos();
			}
		}

		// Token: 0x06002731 RID: 10033 RVA: 0x00036354 File Offset: 0x00034554
		public void MoveRight()
		{
			this.ClearCursorPos();
			if (this.selectIndex == this.cursorIndex)
			{
				this.cursorIndex++;
				this.DetectFocusChange();
				this.selectIndex = this.cursorIndex;
			}
			else if (this.selectIndex > this.cursorIndex)
			{
				this.cursorIndex = this.selectIndex;
			}
			else
			{
				this.selectIndex = this.cursorIndex;
			}
		}

		// Token: 0x06002732 RID: 10034 RVA: 0x000363D0 File Offset: 0x000345D0
		public void MoveLeft()
		{
			if (this.selectIndex == this.cursorIndex)
			{
				this.cursorIndex--;
				this.selectIndex = this.cursorIndex;
			}
			else if (this.selectIndex > this.cursorIndex)
			{
				this.selectIndex = this.cursorIndex;
			}
			else
			{
				this.cursorIndex = this.selectIndex;
			}
			this.ClearCursorPos();
		}

		// Token: 0x06002733 RID: 10035 RVA: 0x00036448 File Offset: 0x00034648
		public void MoveUp()
		{
			if (this.selectIndex < this.cursorIndex)
			{
				this.selectIndex = this.cursorIndex;
			}
			else
			{
				this.cursorIndex = this.selectIndex;
			}
			this.GrabGraphicalCursorPos();
			this.graphicalCursorPos.y = this.graphicalCursorPos.y - 1f;
			int cursorStringIndex = this.style.GetCursorStringIndex(this.position, this.m_Content, this.graphicalCursorPos);
			this.selectIndex = cursorStringIndex;
			this.cursorIndex = cursorStringIndex;
			if (this.cursorIndex <= 0)
			{
				this.ClearCursorPos();
			}
		}

		// Token: 0x06002734 RID: 10036 RVA: 0x000364E0 File Offset: 0x000346E0
		public void MoveDown()
		{
			if (this.selectIndex > this.cursorIndex)
			{
				this.selectIndex = this.cursorIndex;
			}
			else
			{
				this.cursorIndex = this.selectIndex;
			}
			this.GrabGraphicalCursorPos();
			this.graphicalCursorPos.y = this.graphicalCursorPos.y + (this.style.lineHeight + 5f);
			int cursorStringIndex = this.style.GetCursorStringIndex(this.position, this.m_Content, this.graphicalCursorPos);
			this.selectIndex = cursorStringIndex;
			this.cursorIndex = cursorStringIndex;
			if (this.cursorIndex == this.text.Length)
			{
				this.ClearCursorPos();
			}
		}

		// Token: 0x06002735 RID: 10037 RVA: 0x00036590 File Offset: 0x00034790
		public void MoveLineStart()
		{
			int num = (this.selectIndex >= this.cursorIndex) ? this.cursorIndex : this.selectIndex;
			int num2 = num;
			int num3;
			while (num2-- != 0)
			{
				if (this.text[num2] == '\n')
				{
					num3 = num2 + 1;
					this.cursorIndex = num3;
					this.selectIndex = num3;
					return;
				}
			}
			num3 = 0;
			this.cursorIndex = num3;
			this.selectIndex = num3;
		}

		// Token: 0x06002736 RID: 10038 RVA: 0x00036610 File Offset: 0x00034810
		public void MoveLineEnd()
		{
			int num = (this.selectIndex <= this.cursorIndex) ? this.cursorIndex : this.selectIndex;
			int i = num;
			int length = this.text.Length;
			int num2;
			while (i < length)
			{
				if (this.text[i] == '\n')
				{
					num2 = i;
					this.cursorIndex = num2;
					this.selectIndex = num2;
					return;
				}
				i++;
			}
			num2 = length;
			this.cursorIndex = num2;
			this.selectIndex = num2;
		}

		// Token: 0x06002737 RID: 10039 RVA: 0x0003669C File Offset: 0x0003489C
		public void MoveGraphicalLineStart()
		{
			int graphicalLineStart = this.GetGraphicalLineStart((this.cursorIndex >= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			this.selectIndex = graphicalLineStart;
			this.cursorIndex = graphicalLineStart;
		}

		// Token: 0x06002738 RID: 10040 RVA: 0x000366E4 File Offset: 0x000348E4
		public void MoveGraphicalLineEnd()
		{
			int graphicalLineEnd = this.GetGraphicalLineEnd((this.cursorIndex <= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			this.selectIndex = graphicalLineEnd;
			this.cursorIndex = graphicalLineEnd;
		}

		// Token: 0x06002739 RID: 10041 RVA: 0x0003672C File Offset: 0x0003492C
		public void MoveTextStart()
		{
			int num = 0;
			this.cursorIndex = num;
			this.selectIndex = num;
		}

		// Token: 0x0600273A RID: 10042 RVA: 0x0003674C File Offset: 0x0003494C
		public void MoveTextEnd()
		{
			int length = this.text.Length;
			this.cursorIndex = length;
			this.selectIndex = length;
		}

		// Token: 0x0600273B RID: 10043 RVA: 0x00036774 File Offset: 0x00034974
		private int IndexOfEndOfLine(int startIndex)
		{
			int num = this.text.IndexOf('\n', startIndex);
			return (num == -1) ? this.text.Length : num;
		}

		// Token: 0x0600273C RID: 10044 RVA: 0x000367B0 File Offset: 0x000349B0
		public void MoveParagraphForward()
		{
			this.cursorIndex = ((this.cursorIndex <= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			if (this.cursorIndex < this.text.Length)
			{
				int num = this.IndexOfEndOfLine(this.cursorIndex + 1);
				this.cursorIndex = num;
				this.selectIndex = num;
			}
		}

		// Token: 0x0600273D RID: 10045 RVA: 0x0003681C File Offset: 0x00034A1C
		public void MoveParagraphBackward()
		{
			this.cursorIndex = ((this.cursorIndex >= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			if (this.cursorIndex > 1)
			{
				int num = this.text.LastIndexOf('\n', this.cursorIndex - 2) + 1;
				this.cursorIndex = num;
				this.selectIndex = num;
			}
			else
			{
				int num = 0;
				this.cursorIndex = num;
				this.selectIndex = num;
			}
		}

		// Token: 0x0600273E RID: 10046 RVA: 0x0003689C File Offset: 0x00034A9C
		public void MoveCursorToPosition(Vector2 cursorPosition)
		{
			this.selectIndex = this.style.GetCursorStringIndex(this.position, this.m_Content, cursorPosition + this.scrollOffset);
			if (!Event.current.shift)
			{
				this.cursorIndex = this.selectIndex;
			}
			this.DetectFocusChange();
		}

		// Token: 0x0600273F RID: 10047 RVA: 0x000368F8 File Offset: 0x00034AF8
		public void MoveAltCursorToPosition(Vector2 cursorPosition)
		{
			int cursorStringIndex = this.style.GetCursorStringIndex(this.position, this.m_Content, cursorPosition + this.scrollOffset);
			this.m_iAltCursorPos = Mathf.Min(this.text.Length, cursorStringIndex);
			this.DetectFocusChange();
		}

		// Token: 0x06002740 RID: 10048 RVA: 0x00036948 File Offset: 0x00034B48
		public bool IsOverSelection(Vector2 cursorPosition)
		{
			int cursorStringIndex = this.style.GetCursorStringIndex(this.position, this.m_Content, cursorPosition + this.scrollOffset);
			return cursorStringIndex < Mathf.Max(this.cursorIndex, this.selectIndex) && cursorStringIndex > Mathf.Min(this.cursorIndex, this.selectIndex);
		}

		// Token: 0x06002741 RID: 10049 RVA: 0x000369B0 File Offset: 0x00034BB0
		public void SelectToPosition(Vector2 cursorPosition)
		{
			if (!this.m_MouseDragSelectsWholeWords)
			{
				this.cursorIndex = this.style.GetCursorStringIndex(this.position, this.m_Content, cursorPosition + this.scrollOffset);
			}
			else
			{
				int num = this.style.GetCursorStringIndex(this.position, this.m_Content, cursorPosition + this.scrollOffset);
				if (this.m_DblClickSnap == TextEditor.DblClickSnapping.WORDS)
				{
					if (num < this.m_DblClickInitPos)
					{
						this.cursorIndex = this.FindEndOfClassification(num, -1);
						this.selectIndex = this.FindEndOfClassification(this.m_DblClickInitPos, 1);
					}
					else
					{
						if (num >= this.text.Length)
						{
							num = this.text.Length - 1;
						}
						this.cursorIndex = this.FindEndOfClassification(num, 1);
						this.selectIndex = this.FindEndOfClassification(this.m_DblClickInitPos - 1, -1);
					}
				}
				else if (num < this.m_DblClickInitPos)
				{
					if (num > 0)
					{
						this.cursorIndex = this.text.LastIndexOf('\n', Mathf.Max(0, num - 2)) + 1;
					}
					else
					{
						this.cursorIndex = 0;
					}
					this.selectIndex = this.text.LastIndexOf('\n', this.m_DblClickInitPos);
				}
				else
				{
					if (num < this.text.Length)
					{
						this.cursorIndex = this.IndexOfEndOfLine(num);
					}
					else
					{
						this.cursorIndex = this.text.Length;
					}
					this.selectIndex = this.text.LastIndexOf('\n', Mathf.Max(0, this.m_DblClickInitPos - 2)) + 1;
				}
			}
		}

		// Token: 0x06002742 RID: 10050 RVA: 0x00036B64 File Offset: 0x00034D64
		public void SelectLeft()
		{
			if (this.m_bJustSelected && this.cursorIndex > this.selectIndex)
			{
				int cursorIndex = this.cursorIndex;
				this.cursorIndex = this.selectIndex;
				this.selectIndex = cursorIndex;
			}
			this.m_bJustSelected = false;
			this.cursorIndex--;
		}

		// Token: 0x06002743 RID: 10051 RVA: 0x00036BC0 File Offset: 0x00034DC0
		public void SelectRight()
		{
			if (this.m_bJustSelected && this.cursorIndex < this.selectIndex)
			{
				int cursorIndex = this.cursorIndex;
				this.cursorIndex = this.selectIndex;
				this.selectIndex = cursorIndex;
			}
			this.m_bJustSelected = false;
			this.cursorIndex++;
		}

		// Token: 0x06002744 RID: 10052 RVA: 0x00036C1C File Offset: 0x00034E1C
		public void SelectUp()
		{
			this.GrabGraphicalCursorPos();
			this.graphicalCursorPos.y = this.graphicalCursorPos.y - 1f;
			this.cursorIndex = this.style.GetCursorStringIndex(this.position, this.m_Content, this.graphicalCursorPos);
		}

		// Token: 0x06002745 RID: 10053 RVA: 0x00036C6C File Offset: 0x00034E6C
		public void SelectDown()
		{
			this.GrabGraphicalCursorPos();
			this.graphicalCursorPos.y = this.graphicalCursorPos.y + (this.style.lineHeight + 5f);
			this.cursorIndex = this.style.GetCursorStringIndex(this.position, this.m_Content, this.graphicalCursorPos);
		}

		// Token: 0x06002746 RID: 10054 RVA: 0x00036CC8 File Offset: 0x00034EC8
		public void SelectTextEnd()
		{
			this.cursorIndex = this.text.Length;
		}

		// Token: 0x06002747 RID: 10055 RVA: 0x00036CDC File Offset: 0x00034EDC
		public void SelectTextStart()
		{
			this.cursorIndex = 0;
		}

		// Token: 0x06002748 RID: 10056 RVA: 0x00036CE8 File Offset: 0x00034EE8
		public void MouseDragSelectsWholeWords(bool on)
		{
			this.m_MouseDragSelectsWholeWords = on;
			this.m_DblClickInitPos = this.cursorIndex;
		}

		// Token: 0x06002749 RID: 10057 RVA: 0x00036D00 File Offset: 0x00034F00
		public void DblClickSnap(TextEditor.DblClickSnapping snapping)
		{
			this.m_DblClickSnap = snapping;
		}

		// Token: 0x0600274A RID: 10058 RVA: 0x00036D0C File Offset: 0x00034F0C
		private int GetGraphicalLineStart(int p)
		{
			Vector2 cursorPixelPosition = this.style.GetCursorPixelPosition(this.position, this.m_Content, p);
			cursorPixelPosition.x = 0f;
			return this.style.GetCursorStringIndex(this.position, this.m_Content, cursorPixelPosition);
		}

		// Token: 0x0600274B RID: 10059 RVA: 0x00036D60 File Offset: 0x00034F60
		private int GetGraphicalLineEnd(int p)
		{
			Vector2 cursorPixelPosition = this.style.GetCursorPixelPosition(this.position, this.m_Content, p);
			cursorPixelPosition.x += 5000f;
			return this.style.GetCursorStringIndex(this.position, this.m_Content, cursorPixelPosition);
		}

		// Token: 0x0600274C RID: 10060 RVA: 0x00036DBC File Offset: 0x00034FBC
		private int FindNextSeperator(int startPos)
		{
			int length = this.text.Length;
			while (startPos < length && !TextEditor.isLetterLikeChar(this.text[startPos]))
			{
				startPos++;
			}
			while (startPos < length && TextEditor.isLetterLikeChar(this.text[startPos]))
			{
				startPos++;
			}
			return startPos;
		}

		// Token: 0x0600274D RID: 10061 RVA: 0x00036E2C File Offset: 0x0003502C
		private static bool isLetterLikeChar(char c)
		{
			return char.IsLetterOrDigit(c) || c == '\'';
		}

		// Token: 0x0600274E RID: 10062 RVA: 0x00036E54 File Offset: 0x00035054
		private int FindPrevSeperator(int startPos)
		{
			startPos--;
			while (startPos > 0 && !TextEditor.isLetterLikeChar(this.text[startPos]))
			{
				startPos--;
			}
			while (startPos >= 0 && TextEditor.isLetterLikeChar(this.text[startPos]))
			{
				startPos--;
			}
			return startPos + 1;
		}

		// Token: 0x0600274F RID: 10063 RVA: 0x00036EC0 File Offset: 0x000350C0
		public void MoveWordRight()
		{
			this.cursorIndex = ((this.cursorIndex <= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			int num = this.FindNextSeperator(this.cursorIndex);
			this.selectIndex = num;
			this.cursorIndex = num;
			this.ClearCursorPos();
		}

		// Token: 0x06002750 RID: 10064 RVA: 0x00036F18 File Offset: 0x00035118
		public void MoveToStartOfNextWord()
		{
			this.ClearCursorPos();
			if (this.cursorIndex != this.selectIndex)
			{
				this.MoveRight();
			}
			else
			{
				int num = this.FindStartOfNextWord(this.cursorIndex);
				this.selectIndex = num;
				this.cursorIndex = num;
			}
		}

		// Token: 0x06002751 RID: 10065 RVA: 0x00036F64 File Offset: 0x00035164
		public void MoveToEndOfPreviousWord()
		{
			this.ClearCursorPos();
			if (this.cursorIndex != this.selectIndex)
			{
				this.MoveLeft();
			}
			else
			{
				int num = this.FindEndOfPreviousWord(this.cursorIndex);
				this.selectIndex = num;
				this.cursorIndex = num;
			}
		}

		// Token: 0x06002752 RID: 10066 RVA: 0x00036FB0 File Offset: 0x000351B0
		public void SelectToStartOfNextWord()
		{
			this.ClearCursorPos();
			this.cursorIndex = this.FindStartOfNextWord(this.cursorIndex);
		}

		// Token: 0x06002753 RID: 10067 RVA: 0x00036FCC File Offset: 0x000351CC
		public void SelectToEndOfPreviousWord()
		{
			this.ClearCursorPos();
			this.cursorIndex = this.FindEndOfPreviousWord(this.cursorIndex);
		}

		// Token: 0x06002754 RID: 10068 RVA: 0x00036FE8 File Offset: 0x000351E8
		private TextEditor.CharacterType ClassifyChar(char c)
		{
			TextEditor.CharacterType result;
			if (char.IsWhiteSpace(c))
			{
				result = TextEditor.CharacterType.WhiteSpace;
			}
			else if (char.IsLetterOrDigit(c) || c == '\'')
			{
				result = TextEditor.CharacterType.LetterLike;
			}
			else
			{
				result = TextEditor.CharacterType.Symbol;
			}
			return result;
		}

		// Token: 0x06002755 RID: 10069 RVA: 0x0003702C File Offset: 0x0003522C
		public int FindStartOfNextWord(int p)
		{
			int length = this.text.Length;
			int result;
			if (p == length)
			{
				result = p;
			}
			else
			{
				char c = this.text[p];
				TextEditor.CharacterType characterType = this.ClassifyChar(c);
				if (characterType != TextEditor.CharacterType.WhiteSpace)
				{
					p++;
					while (p < length && this.ClassifyChar(this.text[p]) == characterType)
					{
						p++;
					}
				}
				else if (c == '\t' || c == '\n')
				{
					return p + 1;
				}
				if (p == length)
				{
					result = p;
				}
				else
				{
					c = this.text[p];
					if (c == ' ')
					{
						while (p < length && char.IsWhiteSpace(this.text[p]))
						{
							p++;
						}
					}
					else if (c == '\t' || c == '\n')
					{
						return p;
					}
					result = p;
				}
			}
			return result;
		}

		// Token: 0x06002756 RID: 10070 RVA: 0x00037134 File Offset: 0x00035334
		private int FindEndOfPreviousWord(int p)
		{
			int result;
			if (p == 0)
			{
				result = p;
			}
			else
			{
				p--;
				while (p > 0 && this.text[p] == ' ')
				{
					p--;
				}
				TextEditor.CharacterType characterType = this.ClassifyChar(this.text[p]);
				if (characterType != TextEditor.CharacterType.WhiteSpace)
				{
					while (p > 0 && this.ClassifyChar(this.text[p - 1]) == characterType)
					{
						p--;
					}
				}
				result = p;
			}
			return result;
		}

		// Token: 0x06002757 RID: 10071 RVA: 0x000371C8 File Offset: 0x000353C8
		public void MoveWordLeft()
		{
			this.cursorIndex = ((this.cursorIndex >= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			this.cursorIndex = this.FindPrevSeperator(this.cursorIndex);
			this.selectIndex = this.cursorIndex;
		}

		// Token: 0x06002758 RID: 10072 RVA: 0x0003721C File Offset: 0x0003541C
		public void SelectWordRight()
		{
			this.ClearCursorPos();
			int selectIndex = this.selectIndex;
			if (this.cursorIndex < this.selectIndex)
			{
				this.selectIndex = this.cursorIndex;
				this.MoveWordRight();
				this.selectIndex = selectIndex;
				this.cursorIndex = ((this.cursorIndex >= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			}
			else
			{
				this.selectIndex = this.cursorIndex;
				this.MoveWordRight();
				this.selectIndex = selectIndex;
			}
		}

		// Token: 0x06002759 RID: 10073 RVA: 0x000372A8 File Offset: 0x000354A8
		public void SelectWordLeft()
		{
			this.ClearCursorPos();
			int selectIndex = this.selectIndex;
			if (this.cursorIndex > this.selectIndex)
			{
				this.selectIndex = this.cursorIndex;
				this.MoveWordLeft();
				this.selectIndex = selectIndex;
				this.cursorIndex = ((this.cursorIndex <= this.selectIndex) ? this.selectIndex : this.cursorIndex);
			}
			else
			{
				this.selectIndex = this.cursorIndex;
				this.MoveWordLeft();
				this.selectIndex = selectIndex;
			}
		}

		// Token: 0x0600275A RID: 10074 RVA: 0x00037334 File Offset: 0x00035534
		public void ExpandSelectGraphicalLineStart()
		{
			this.ClearCursorPos();
			if (this.cursorIndex < this.selectIndex)
			{
				this.cursorIndex = this.GetGraphicalLineStart(this.cursorIndex);
			}
			else
			{
				int cursorIndex = this.cursorIndex;
				this.cursorIndex = this.GetGraphicalLineStart(this.selectIndex);
				this.selectIndex = cursorIndex;
			}
		}

		// Token: 0x0600275B RID: 10075 RVA: 0x00037394 File Offset: 0x00035594
		public void ExpandSelectGraphicalLineEnd()
		{
			this.ClearCursorPos();
			if (this.cursorIndex > this.selectIndex)
			{
				this.cursorIndex = this.GetGraphicalLineEnd(this.cursorIndex);
			}
			else
			{
				int cursorIndex = this.cursorIndex;
				this.cursorIndex = this.GetGraphicalLineEnd(this.selectIndex);
				this.selectIndex = cursorIndex;
			}
		}

		// Token: 0x0600275C RID: 10076 RVA: 0x000373F4 File Offset: 0x000355F4
		public void SelectGraphicalLineStart()
		{
			this.ClearCursorPos();
			this.cursorIndex = this.GetGraphicalLineStart(this.cursorIndex);
		}

		// Token: 0x0600275D RID: 10077 RVA: 0x00037410 File Offset: 0x00035610
		public void SelectGraphicalLineEnd()
		{
			this.ClearCursorPos();
			this.cursorIndex = this.GetGraphicalLineEnd(this.cursorIndex);
		}

		// Token: 0x0600275E RID: 10078 RVA: 0x0003742C File Offset: 0x0003562C
		public void SelectParagraphForward()
		{
			this.ClearCursorPos();
			bool flag = this.cursorIndex < this.selectIndex;
			if (this.cursorIndex < this.text.Length)
			{
				this.cursorIndex = this.IndexOfEndOfLine(this.cursorIndex + 1);
				if (flag && this.cursorIndex > this.selectIndex)
				{
					this.cursorIndex = this.selectIndex;
				}
			}
		}

		// Token: 0x0600275F RID: 10079 RVA: 0x000374A0 File Offset: 0x000356A0
		public void SelectParagraphBackward()
		{
			this.ClearCursorPos();
			bool flag = this.cursorIndex > this.selectIndex;
			if (this.cursorIndex > 1)
			{
				this.cursorIndex = this.text.LastIndexOf('\n', this.cursorIndex - 2) + 1;
				if (flag && this.cursorIndex < this.selectIndex)
				{
					this.cursorIndex = this.selectIndex;
				}
			}
			else
			{
				int num = 0;
				this.cursorIndex = num;
				this.selectIndex = num;
			}
		}

		// Token: 0x06002760 RID: 10080 RVA: 0x00037528 File Offset: 0x00035728
		public void SelectCurrentWord()
		{
			this.ClearCursorPos();
			int length = this.text.Length;
			this.selectIndex = this.cursorIndex;
			if (length != 0)
			{
				if (this.cursorIndex >= length)
				{
					this.cursorIndex = length - 1;
				}
				if (this.selectIndex >= length)
				{
					this.selectIndex--;
				}
				if (this.cursorIndex < this.selectIndex)
				{
					this.cursorIndex = this.FindEndOfClassification(this.cursorIndex, -1);
					this.selectIndex = this.FindEndOfClassification(this.selectIndex, 1);
				}
				else
				{
					this.cursorIndex = this.FindEndOfClassification(this.cursorIndex, 1);
					this.selectIndex = this.FindEndOfClassification(this.selectIndex, -1);
				}
				this.m_bJustSelected = true;
			}
		}

		// Token: 0x06002761 RID: 10081 RVA: 0x000375FC File Offset: 0x000357FC
		private int FindEndOfClassification(int p, int dir)
		{
			int length = this.text.Length;
			int result;
			if (p >= length || p < 0)
			{
				result = p;
			}
			else
			{
				TextEditor.CharacterType characterType = this.ClassifyChar(this.text[p]);
				for (;;)
				{
					p += dir;
					if (p < 0)
					{
						break;
					}
					if (p >= length)
					{
						goto Block_3;
					}
					if (this.ClassifyChar(this.text[p]) != characterType)
					{
						goto Block_4;
					}
				}
				return 0;
				Block_3:
				return length;
				Block_4:
				if (dir == 1)
				{
					result = p;
				}
				else
				{
					result = p + 1;
				}
			}
			return result;
		}

		// Token: 0x06002762 RID: 10082 RVA: 0x00037694 File Offset: 0x00035894
		public void SelectCurrentParagraph()
		{
			this.ClearCursorPos();
			int length = this.text.Length;
			if (this.cursorIndex < length)
			{
				this.cursorIndex = this.IndexOfEndOfLine(this.cursorIndex) + 1;
			}
			if (this.selectIndex != 0)
			{
				this.selectIndex = this.text.LastIndexOf('\n', this.selectIndex - 1) + 1;
			}
		}

		// Token: 0x06002763 RID: 10083 RVA: 0x00037700 File Offset: 0x00035900
		public void UpdateScrollOffsetIfNeeded(Event evt)
		{
			if (evt.type != EventType.Repaint && evt.type != EventType.Layout)
			{
				this.UpdateScrollOffset();
			}
		}

		// Token: 0x06002764 RID: 10084 RVA: 0x00037724 File Offset: 0x00035924
		private void UpdateScrollOffset()
		{
			int cursorIndex = this.cursorIndex;
			this.graphicalCursorPos = this.style.GetCursorPixelPosition(new Rect(0f, 0f, this.position.width, this.position.height), this.m_Content, cursorIndex);
			Rect rect = this.style.padding.Remove(this.position);
			Vector2 vector = new Vector2(this.style.CalcSize(this.m_Content).x, this.style.CalcHeight(this.m_Content, this.position.width));
			if (vector.x < this.position.width)
			{
				this.scrollOffset.x = 0f;
			}
			else if (this.m_RevealCursor)
			{
				if (this.graphicalCursorPos.x + 1f > this.scrollOffset.x + rect.width)
				{
					this.scrollOffset.x = this.graphicalCursorPos.x - rect.width;
				}
				if (this.graphicalCursorPos.x < this.scrollOffset.x + (float)this.style.padding.left)
				{
					this.scrollOffset.x = this.graphicalCursorPos.x - (float)this.style.padding.left;
				}
			}
			if (vector.y < rect.height)
			{
				this.scrollOffset.y = 0f;
			}
			else if (this.m_RevealCursor)
			{
				if (this.graphicalCursorPos.y + this.style.lineHeight > this.scrollOffset.y + rect.height + (float)this.style.padding.top)
				{
					this.scrollOffset.y = this.graphicalCursorPos.y - rect.height - (float)this.style.padding.top + this.style.lineHeight;
				}
				if (this.graphicalCursorPos.y < this.scrollOffset.y + (float)this.style.padding.top)
				{
					this.scrollOffset.y = this.graphicalCursorPos.y - (float)this.style.padding.top;
				}
			}
			if (this.scrollOffset.y > 0f && vector.y - this.scrollOffset.y < rect.height)
			{
				this.scrollOffset.y = vector.y - rect.height - (float)this.style.padding.top - (float)this.style.padding.bottom;
			}
			this.scrollOffset.y = ((this.scrollOffset.y >= 0f) ? this.scrollOffset.y : 0f);
			this.m_RevealCursor = false;
		}

		// Token: 0x06002765 RID: 10085 RVA: 0x00037A68 File Offset: 0x00035C68
		public void DrawCursor(string newText)
		{
			string text = this.text;
			int num = this.cursorIndex;
			if (Input.compositionString.Length > 0)
			{
				this.m_Content.text = newText.Substring(0, this.cursorIndex) + Input.compositionString + newText.Substring(this.selectIndex);
				num += Input.compositionString.Length;
			}
			else
			{
				this.m_Content.text = newText;
			}
			this.graphicalCursorPos = this.style.GetCursorPixelPosition(new Rect(0f, 0f, this.position.width, this.position.height), this.m_Content, num);
			Vector2 contentOffset = this.style.contentOffset;
			this.style.contentOffset -= this.scrollOffset;
			this.style.Internal_clipOffset = this.scrollOffset;
			Input.compositionCursorPos = this.graphicalCursorPos + new Vector2(this.position.x, this.position.y + this.style.lineHeight) - this.scrollOffset;
			if (Input.compositionString.Length > 0)
			{
				this.style.DrawWithTextSelection(this.position, this.m_Content, this.controlID, this.cursorIndex, this.cursorIndex + Input.compositionString.Length, true);
			}
			else
			{
				this.style.DrawWithTextSelection(this.position, this.m_Content, this.controlID, this.cursorIndex, this.selectIndex);
			}
			if (this.m_iAltCursorPos != -1)
			{
				this.style.DrawCursor(this.position, this.m_Content, this.controlID, this.m_iAltCursorPos);
			}
			this.style.contentOffset = contentOffset;
			this.style.Internal_clipOffset = Vector2.zero;
			this.m_Content.text = text;
		}

		// Token: 0x06002766 RID: 10086 RVA: 0x00037C74 File Offset: 0x00035E74
		private bool PerformOperation(TextEditor.TextEditOp operation)
		{
			this.m_RevealCursor = true;
			switch (operation)
			{
			case TextEditor.TextEditOp.MoveLeft:
				this.MoveLeft();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveRight:
				this.MoveRight();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveUp:
				this.MoveUp();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveDown:
				this.MoveDown();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveLineStart:
				this.MoveLineStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveLineEnd:
				this.MoveLineEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveTextStart:
				this.MoveTextStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveTextEnd:
				this.MoveTextEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveGraphicalLineStart:
				this.MoveGraphicalLineStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveGraphicalLineEnd:
				this.MoveGraphicalLineEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveWordLeft:
				this.MoveWordLeft();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveWordRight:
				this.MoveWordRight();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveParagraphForward:
				this.MoveParagraphForward();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveParagraphBackward:
				this.MoveParagraphBackward();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveToStartOfNextWord:
				this.MoveToStartOfNextWord();
				goto IL_2BA;
			case TextEditor.TextEditOp.MoveToEndOfPreviousWord:
				this.MoveToEndOfPreviousWord();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectLeft:
				this.SelectLeft();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectRight:
				this.SelectRight();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectUp:
				this.SelectUp();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectDown:
				this.SelectDown();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectTextStart:
				this.SelectTextStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectTextEnd:
				this.SelectTextEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.ExpandSelectGraphicalLineStart:
				this.ExpandSelectGraphicalLineStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.ExpandSelectGraphicalLineEnd:
				this.ExpandSelectGraphicalLineEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectGraphicalLineStart:
				this.SelectGraphicalLineStart();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectGraphicalLineEnd:
				this.SelectGraphicalLineEnd();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectWordLeft:
				this.SelectWordLeft();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectWordRight:
				this.SelectWordRight();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectToEndOfPreviousWord:
				this.SelectToEndOfPreviousWord();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectToStartOfNextWord:
				this.SelectToStartOfNextWord();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectParagraphBackward:
				this.SelectParagraphBackward();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectParagraphForward:
				this.SelectParagraphForward();
				goto IL_2BA;
			case TextEditor.TextEditOp.Delete:
				return this.Delete();
			case TextEditor.TextEditOp.Backspace:
				return this.Backspace();
			case TextEditor.TextEditOp.DeleteWordBack:
				return this.DeleteWordBack();
			case TextEditor.TextEditOp.DeleteWordForward:
				return this.DeleteWordForward();
			case TextEditor.TextEditOp.DeleteLineBack:
				return this.DeleteLineBack();
			case TextEditor.TextEditOp.Cut:
				return this.Cut();
			case TextEditor.TextEditOp.Copy:
				this.Copy();
				goto IL_2BA;
			case TextEditor.TextEditOp.Paste:
				return this.Paste();
			case TextEditor.TextEditOp.SelectAll:
				this.SelectAll();
				goto IL_2BA;
			case TextEditor.TextEditOp.SelectNone:
				this.SelectNone();
				goto IL_2BA;
			}
			Debug.Log("Unimplemented: " + operation);
			IL_2BA:
			return false;
		}

		// Token: 0x06002767 RID: 10087 RVA: 0x00037F44 File Offset: 0x00036144
		public void SaveBackup()
		{
			this.oldText = this.text;
			this.oldPos = this.cursorIndex;
			this.oldSelectPos = this.selectIndex;
		}

		// Token: 0x06002768 RID: 10088 RVA: 0x00037F6C File Offset: 0x0003616C
		public void Undo()
		{
			this.m_Content.text = this.oldText;
			this.cursorIndex = this.oldPos;
			this.selectIndex = this.oldSelectPos;
		}

		// Token: 0x06002769 RID: 10089 RVA: 0x00037F98 File Offset: 0x00036198
		public bool Cut()
		{
			bool result;
			if (this.isPasswordField)
			{
				result = false;
			}
			else
			{
				this.Copy();
				result = this.DeleteSelection();
			}
			return result;
		}

		// Token: 0x0600276A RID: 10090 RVA: 0x00037FCC File Offset: 0x000361CC
		public void Copy()
		{
			if (this.selectIndex != this.cursorIndex)
			{
				if (!this.isPasswordField)
				{
					string systemCopyBuffer;
					if (this.cursorIndex < this.selectIndex)
					{
						systemCopyBuffer = this.text.Substring(this.cursorIndex, this.selectIndex - this.cursorIndex);
					}
					else
					{
						systemCopyBuffer = this.text.Substring(this.selectIndex, this.cursorIndex - this.selectIndex);
					}
					GUIUtility.systemCopyBuffer = systemCopyBuffer;
				}
			}
		}

		// Token: 0x0600276B RID: 10091 RVA: 0x0003805C File Offset: 0x0003625C
		private static string ReplaceNewlinesWithSpaces(string value)
		{
			value = value.Replace("\r\n", " ");
			value = value.Replace('\n', ' ');
			value = value.Replace('\r', ' ');
			return value;
		}

		// Token: 0x0600276C RID: 10092 RVA: 0x0003809C File Offset: 0x0003629C
		public bool Paste()
		{
			string text = GUIUtility.systemCopyBuffer;
			bool result;
			if (text != "")
			{
				if (!this.multiline)
				{
					text = TextEditor.ReplaceNewlinesWithSpaces(text);
				}
				this.ReplaceSelection(text);
				result = true;
			}
			else
			{
				result = false;
			}
			return result;
		}

		// Token: 0x0600276D RID: 10093 RVA: 0x000380EC File Offset: 0x000362EC
		private static void MapKey(string key, TextEditor.TextEditOp action)
		{
			TextEditor.s_Keyactions[Event.KeyboardEvent(key)] = action;
		}

		// Token: 0x0600276E RID: 10094 RVA: 0x00038100 File Offset: 0x00036300
		private void InitKeyActions()
		{
			if (TextEditor.s_Keyactions == null)
			{
				TextEditor.s_Keyactions = new Dictionary<Event, TextEditor.TextEditOp>();
				TextEditor.MapKey("left", TextEditor.TextEditOp.MoveLeft);
				TextEditor.MapKey("right", TextEditor.TextEditOp.MoveRight);
				TextEditor.MapKey("up", TextEditor.TextEditOp.MoveUp);
				TextEditor.MapKey("down", TextEditor.TextEditOp.MoveDown);
				TextEditor.MapKey("#left", TextEditor.TextEditOp.SelectLeft);
				TextEditor.MapKey("#right", TextEditor.TextEditOp.SelectRight);
				TextEditor.MapKey("#up", TextEditor.TextEditOp.SelectUp);
				TextEditor.MapKey("#down", TextEditor.TextEditOp.SelectDown);
				TextEditor.MapKey("delete", TextEditor.TextEditOp.Delete);
				TextEditor.MapKey("backspace", TextEditor.TextEditOp.Backspace);
				TextEditor.MapKey("#backspace", TextEditor.TextEditOp.Backspace);
				if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.MacOSX)
				{
					TextEditor.MapKey("^left", TextEditor.TextEditOp.MoveGraphicalLineStart);
					TextEditor.MapKey("^right", TextEditor.TextEditOp.MoveGraphicalLineEnd);
					TextEditor.MapKey("&left", TextEditor.TextEditOp.MoveWordLeft);
					TextEditor.MapKey("&right", TextEditor.TextEditOp.MoveWordRight);
					TextEditor.MapKey("&up", TextEditor.TextEditOp.MoveParagraphBackward);
					TextEditor.MapKey("&down", TextEditor.TextEditOp.MoveParagraphForward);
					TextEditor.MapKey("%left", TextEditor.TextEditOp.MoveGraphicalLineStart);
					TextEditor.MapKey("%right", TextEditor.TextEditOp.MoveGraphicalLineEnd);
					TextEditor.MapKey("%up", TextEditor.TextEditOp.MoveTextStart);
					TextEditor.MapKey("%down", TextEditor.TextEditOp.MoveTextEnd);
					TextEditor.MapKey("#home", TextEditor.TextEditOp.SelectTextStart);
					TextEditor.MapKey("#end", TextEditor.TextEditOp.SelectTextEnd);
					TextEditor.MapKey("#^left", TextEditor.TextEditOp.ExpandSelectGraphicalLineStart);
					TextEditor.MapKey("#^right", TextEditor.TextEditOp.ExpandSelectGraphicalLineEnd);
					TextEditor.MapKey("#^up", TextEditor.TextEditOp.SelectParagraphBackward);
					TextEditor.MapKey("#^down", TextEditor.TextEditOp.SelectParagraphForward);
					TextEditor.MapKey("#&left", TextEditor.TextEditOp.SelectWordLeft);
					TextEditor.MapKey("#&right", TextEditor.TextEditOp.SelectWordRight);
					TextEditor.MapKey("#&up", TextEditor.TextEditOp.SelectParagraphBackward);
					TextEditor.MapKey("#&down", TextEditor.TextEditOp.SelectParagraphForward);
					TextEditor.MapKey("#%left", TextEditor.TextEditOp.ExpandSelectGraphicalLineStart);
					TextEditor.MapKey("#%right", TextEditor.TextEditOp.ExpandSelectGraphicalLineEnd);
					TextEditor.MapKey("#%up", TextEditor.TextEditOp.SelectTextStart);
					TextEditor.MapKey("#%down", TextEditor.TextEditOp.SelectTextEnd);
					TextEditor.MapKey("%a", TextEditor.TextEditOp.SelectAll);
					TextEditor.MapKey("%x", TextEditor.TextEditOp.Cut);
					TextEditor.MapKey("%c", TextEditor.TextEditOp.Copy);
					TextEditor.MapKey("%v", TextEditor.TextEditOp.Paste);
					TextEditor.MapKey("^d", TextEditor.TextEditOp.Delete);
					TextEditor.MapKey("^h", TextEditor.TextEditOp.Backspace);
					TextEditor.MapKey("^b", TextEditor.TextEditOp.MoveLeft);
					TextEditor.MapKey("^f", TextEditor.TextEditOp.MoveRight);
					TextEditor.MapKey("^a", TextEditor.TextEditOp.MoveLineStart);
					TextEditor.MapKey("^e", TextEditor.TextEditOp.MoveLineEnd);
					TextEditor.MapKey("&delete", TextEditor.TextEditOp.DeleteWordForward);
					TextEditor.MapKey("&backspace", TextEditor.TextEditOp.DeleteWordBack);
					TextEditor.MapKey("%backspace", TextEditor.TextEditOp.DeleteLineBack);
				}
				else
				{
					TextEditor.MapKey("home", TextEditor.TextEditOp.MoveGraphicalLineStart);
					TextEditor.MapKey("end", TextEditor.TextEditOp.MoveGraphicalLineEnd);
					TextEditor.MapKey("%left", TextEditor.TextEditOp.MoveWordLeft);
					TextEditor.MapKey("%right", TextEditor.TextEditOp.MoveWordRight);
					TextEditor.MapKey("%up", TextEditor.TextEditOp.MoveParagraphBackward);
					TextEditor.MapKey("%down", TextEditor.TextEditOp.MoveParagraphForward);
					TextEditor.MapKey("^left", TextEditor.TextEditOp.MoveToEndOfPreviousWord);
					TextEditor.MapKey("^right", TextEditor.TextEditOp.MoveToStartOfNextWord);
					TextEditor.MapKey("^up", TextEditor.TextEditOp.MoveParagraphBackward);
					TextEditor.MapKey("^down", TextEditor.TextEditOp.MoveParagraphForward);
					TextEditor.MapKey("#^left", TextEditor.TextEditOp.SelectToEndOfPreviousWord);
					TextEditor.MapKey("#^right", TextEditor.TextEditOp.SelectToStartOfNextWord);
					TextEditor.MapKey("#^up", TextEditor.TextEditOp.SelectParagraphBackward);
					TextEditor.MapKey("#^down", TextEditor.TextEditOp.SelectParagraphForward);
					TextEditor.MapKey("#home", TextEditor.TextEditOp.SelectGraphicalLineStart);
					TextEditor.MapKey("#end", TextEditor.TextEditOp.SelectGraphicalLineEnd);
					TextEditor.MapKey("^delete", TextEditor.TextEditOp.DeleteWordForward);
					TextEditor.MapKey("^backspace", TextEditor.TextEditOp.DeleteWordBack);
					TextEditor.MapKey("%backspace", TextEditor.TextEditOp.DeleteLineBack);
					TextEditor.MapKey("^a", TextEditor.TextEditOp.SelectAll);
					TextEditor.MapKey("^x", TextEditor.TextEditOp.Cut);
					TextEditor.MapKey("^c", TextEditor.TextEditOp.Copy);
					TextEditor.MapKey("^v", TextEditor.TextEditOp.Paste);
					TextEditor.MapKey("#delete", TextEditor.TextEditOp.Cut);
					TextEditor.MapKey("^insert", TextEditor.TextEditOp.Copy);
					TextEditor.MapKey("#insert", TextEditor.TextEditOp.Paste);
				}
			}
		}

		// Token: 0x0600276F RID: 10095 RVA: 0x000384AC File Offset: 0x000366AC
		public void DetectFocusChange()
		{
			if (this.m_HasFocus && this.controlID != GUIUtility.keyboardControl)
			{
				this.OnLostFocus();
			}
			if (!this.m_HasFocus && this.controlID == GUIUtility.keyboardControl)
			{
				this.OnFocus();
			}
		}

		// Token: 0x06002770 RID: 10096 RVA: 0x000384FC File Offset: 0x000366FC
		private void ClampTextIndex(ref int index)
		{
			index = Mathf.Clamp(index, 0, this.text.Length);
		}

		// Token: 0x0400087D RID: 2173
		public TouchScreenKeyboard keyboardOnScreen = null;

		// Token: 0x0400087E RID: 2174
		public int controlID = 0;

		// Token: 0x0400087F RID: 2175
		public GUIStyle style = GUIStyle.none;

		// Token: 0x04000880 RID: 2176
		public bool multiline = false;

		// Token: 0x04000881 RID: 2177
		public bool hasHorizontalCursorPos = false;

		// Token: 0x04000882 RID: 2178
		public bool isPasswordField = false;

		// Token: 0x04000883 RID: 2179
		internal bool m_HasFocus;

		// Token: 0x04000884 RID: 2180
		public Vector2 scrollOffset = Vector2.zero;

		// Token: 0x04000885 RID: 2181
		private GUIContent m_Content = new GUIContent();

		// Token: 0x04000886 RID: 2182
		private Rect m_Position;

		// Token: 0x04000887 RID: 2183
		private int m_CursorIndex = 0;

		// Token: 0x04000888 RID: 2184
		private int m_SelectIndex = 0;

		// Token: 0x04000889 RID: 2185
		private bool m_RevealCursor = false;

		// Token: 0x0400088A RID: 2186
		public Vector2 graphicalCursorPos;

		// Token: 0x0400088B RID: 2187
		public Vector2 graphicalSelectCursorPos;

		// Token: 0x0400088C RID: 2188
		private bool m_MouseDragSelectsWholeWords = false;

		// Token: 0x0400088D RID: 2189
		private int m_DblClickInitPos = 0;

		// Token: 0x0400088E RID: 2190
		private TextEditor.DblClickSnapping m_DblClickSnap = TextEditor.DblClickSnapping.WORDS;

		// Token: 0x0400088F RID: 2191
		private bool m_bJustSelected = false;

		// Token: 0x04000890 RID: 2192
		private int m_iAltCursorPos = -1;

		// Token: 0x04000891 RID: 2193
		private string oldText;

		// Token: 0x04000892 RID: 2194
		private int oldPos;

		// Token: 0x04000893 RID: 2195
		private int oldSelectPos;

		// Token: 0x04000894 RID: 2196
		private static Dictionary<Event, TextEditor.TextEditOp> s_Keyactions;

		// Token: 0x02000240 RID: 576
		public enum DblClickSnapping : byte
		{
			// Token: 0x04000896 RID: 2198
			WORDS,
			// Token: 0x04000897 RID: 2199
			PARAGRAPHS
		}

		// Token: 0x02000241 RID: 577
		private enum CharacterType
		{
			// Token: 0x04000899 RID: 2201
			LetterLike,
			// Token: 0x0400089A RID: 2202
			Symbol,
			// Token: 0x0400089B RID: 2203
			Symbol2,
			// Token: 0x0400089C RID: 2204
			WhiteSpace
		}

		// Token: 0x02000242 RID: 578
		private enum TextEditOp
		{
			// Token: 0x0400089E RID: 2206
			MoveLeft,
			// Token: 0x0400089F RID: 2207
			MoveRight,
			// Token: 0x040008A0 RID: 2208
			MoveUp,
			// Token: 0x040008A1 RID: 2209
			MoveDown,
			// Token: 0x040008A2 RID: 2210
			MoveLineStart,
			// Token: 0x040008A3 RID: 2211
			MoveLineEnd,
			// Token: 0x040008A4 RID: 2212
			MoveTextStart,
			// Token: 0x040008A5 RID: 2213
			MoveTextEnd,
			// Token: 0x040008A6 RID: 2214
			MovePageUp,
			// Token: 0x040008A7 RID: 2215
			MovePageDown,
			// Token: 0x040008A8 RID: 2216
			MoveGraphicalLineStart,
			// Token: 0x040008A9 RID: 2217
			MoveGraphicalLineEnd,
			// Token: 0x040008AA RID: 2218
			MoveWordLeft,
			// Token: 0x040008AB RID: 2219
			MoveWordRight,
			// Token: 0x040008AC RID: 2220
			MoveParagraphForward,
			// Token: 0x040008AD RID: 2221
			MoveParagraphBackward,
			// Token: 0x040008AE RID: 2222
			MoveToStartOfNextWord,
			// Token: 0x040008AF RID: 2223
			MoveToEndOfPreviousWord,
			// Token: 0x040008B0 RID: 2224
			SelectLeft,
			// Token: 0x040008B1 RID: 2225
			SelectRight,
			// Token: 0x040008B2 RID: 2226
			SelectUp,
			// Token: 0x040008B3 RID: 2227
			SelectDown,
			// Token: 0x040008B4 RID: 2228
			SelectTextStart,
			// Token: 0x040008B5 RID: 2229
			SelectTextEnd,
			// Token: 0x040008B6 RID: 2230
			SelectPageUp,
			// Token: 0x040008B7 RID: 2231
			SelectPageDown,
			// Token: 0x040008B8 RID: 2232
			ExpandSelectGraphicalLineStart,
			// Token: 0x040008B9 RID: 2233
			ExpandSelectGraphicalLineEnd,
			// Token: 0x040008BA RID: 2234
			SelectGraphicalLineStart,
			// Token: 0x040008BB RID: 2235
			SelectGraphicalLineEnd,
			// Token: 0x040008BC RID: 2236
			SelectWordLeft,
			// Token: 0x040008BD RID: 2237
			SelectWordRight,
			// Token: 0x040008BE RID: 2238
			SelectToEndOfPreviousWord,
			// Token: 0x040008BF RID: 2239
			SelectToStartOfNextWord,
			// Token: 0x040008C0 RID: 2240
			SelectParagraphBackward,
			// Token: 0x040008C1 RID: 2241
			SelectParagraphForward,
			// Token: 0x040008C2 RID: 2242
			Delete,
			// Token: 0x040008C3 RID: 2243
			Backspace,
			// Token: 0x040008C4 RID: 2244
			DeleteWordBack,
			// Token: 0x040008C5 RID: 2245
			DeleteWordForward,
			// Token: 0x040008C6 RID: 2246
			DeleteLineBack,
			// Token: 0x040008C7 RID: 2247
			Cut,
			// Token: 0x040008C8 RID: 2248
			Copy,
			// Token: 0x040008C9 RID: 2249
			Paste,
			// Token: 0x040008CA RID: 2250
			SelectAll,
			// Token: 0x040008CB RID: 2251
			SelectNone,
			// Token: 0x040008CC RID: 2252
			ScrollStart,
			// Token: 0x040008CD RID: 2253
			ScrollEnd,
			// Token: 0x040008CE RID: 2254
			ScrollPageUp,
			// Token: 0x040008CF RID: 2255
			ScrollPageDown
		}
	}
}
