﻿using System;

namespace UnityEngine
{
	// Token: 0x020001BE RID: 446
	public enum QueueMode
	{
		// Token: 0x040004BE RID: 1214
		CompleteOthers,
		// Token: 0x040004BF RID: 1215
		PlayNow = 2
	}
}
