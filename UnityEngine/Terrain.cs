﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001F7 RID: 503
	[UsedByNativeCode]
	public sealed class Terrain : Behaviour
	{
		// Token: 0x17000871 RID: 2161
		// (get) Token: 0x0600222F RID: 8751
		// (set) Token: 0x06002230 RID: 8752
		public extern TerrainData terrainData { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000872 RID: 2162
		// (get) Token: 0x06002231 RID: 8753
		// (set) Token: 0x06002232 RID: 8754
		public extern float treeDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000873 RID: 2163
		// (get) Token: 0x06002233 RID: 8755
		// (set) Token: 0x06002234 RID: 8756
		public extern float treeBillboardDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000874 RID: 2164
		// (get) Token: 0x06002235 RID: 8757
		// (set) Token: 0x06002236 RID: 8758
		public extern float treeCrossFadeLength { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000875 RID: 2165
		// (get) Token: 0x06002237 RID: 8759
		// (set) Token: 0x06002238 RID: 8760
		public extern int treeMaximumFullLODCount { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000876 RID: 2166
		// (get) Token: 0x06002239 RID: 8761
		// (set) Token: 0x0600223A RID: 8762
		public extern float detailObjectDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000877 RID: 2167
		// (get) Token: 0x0600223B RID: 8763
		// (set) Token: 0x0600223C RID: 8764
		public extern float detailObjectDensity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000878 RID: 2168
		// (get) Token: 0x0600223D RID: 8765
		// (set) Token: 0x0600223E RID: 8766
		public extern float heightmapPixelError { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000879 RID: 2169
		// (get) Token: 0x0600223F RID: 8767
		// (set) Token: 0x06002240 RID: 8768
		public extern int heightmapMaximumLOD { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700087A RID: 2170
		// (get) Token: 0x06002241 RID: 8769
		// (set) Token: 0x06002242 RID: 8770
		public extern float basemapDistance { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700087B RID: 2171
		// (get) Token: 0x06002243 RID: 8771 RVA: 0x00027AE4 File Offset: 0x00025CE4
		// (set) Token: 0x06002244 RID: 8772 RVA: 0x00027B00 File Offset: 0x00025D00
		[Obsolete("use basemapDistance", true)]
		public float splatmapDistance
		{
			get
			{
				return this.basemapDistance;
			}
			set
			{
				this.basemapDistance = value;
			}
		}

		// Token: 0x1700087C RID: 2172
		// (get) Token: 0x06002245 RID: 8773
		// (set) Token: 0x06002246 RID: 8774
		public extern int lightmapIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700087D RID: 2173
		// (get) Token: 0x06002247 RID: 8775
		// (set) Token: 0x06002248 RID: 8776
		public extern int realtimeLightmapIndex { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700087E RID: 2174
		// (get) Token: 0x06002249 RID: 8777 RVA: 0x00027B0C File Offset: 0x00025D0C
		// (set) Token: 0x0600224A RID: 8778 RVA: 0x00027B2C File Offset: 0x00025D2C
		public Vector4 lightmapScaleOffset
		{
			get
			{
				Vector4 result;
				this.INTERNAL_get_lightmapScaleOffset(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_lightmapScaleOffset(ref value);
			}
		}

		// Token: 0x0600224B RID: 8779
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_lightmapScaleOffset(out Vector4 value);

		// Token: 0x0600224C RID: 8780
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_lightmapScaleOffset(ref Vector4 value);

		// Token: 0x1700087F RID: 2175
		// (get) Token: 0x0600224D RID: 8781 RVA: 0x00027B38 File Offset: 0x00025D38
		// (set) Token: 0x0600224E RID: 8782 RVA: 0x00027B58 File Offset: 0x00025D58
		public Vector4 realtimeLightmapScaleOffset
		{
			get
			{
				Vector4 result;
				this.INTERNAL_get_realtimeLightmapScaleOffset(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_realtimeLightmapScaleOffset(ref value);
			}
		}

		// Token: 0x0600224F RID: 8783
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_realtimeLightmapScaleOffset(out Vector4 value);

		// Token: 0x06002250 RID: 8784
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_realtimeLightmapScaleOffset(ref Vector4 value);

		// Token: 0x17000880 RID: 2176
		// (get) Token: 0x06002251 RID: 8785
		// (set) Token: 0x06002252 RID: 8786
		public extern bool castShadows { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000881 RID: 2177
		// (get) Token: 0x06002253 RID: 8787
		// (set) Token: 0x06002254 RID: 8788
		public extern ReflectionProbeUsage reflectionProbeUsage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002255 RID: 8789
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetClosestReflectionProbesInternal(object result);

		// Token: 0x06002256 RID: 8790 RVA: 0x00027B64 File Offset: 0x00025D64
		public void GetClosestReflectionProbes(List<ReflectionProbeBlendInfo> result)
		{
			this.GetClosestReflectionProbesInternal(result);
		}

		// Token: 0x17000882 RID: 2178
		// (get) Token: 0x06002257 RID: 8791
		// (set) Token: 0x06002258 RID: 8792
		public extern Terrain.MaterialType materialType { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000883 RID: 2179
		// (get) Token: 0x06002259 RID: 8793
		// (set) Token: 0x0600225A RID: 8794
		public extern Material materialTemplate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000884 RID: 2180
		// (get) Token: 0x0600225B RID: 8795 RVA: 0x00027B70 File Offset: 0x00025D70
		// (set) Token: 0x0600225C RID: 8796 RVA: 0x00027B90 File Offset: 0x00025D90
		public Color legacySpecular
		{
			get
			{
				Color result;
				this.INTERNAL_get_legacySpecular(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_legacySpecular(ref value);
			}
		}

		// Token: 0x0600225D RID: 8797
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_legacySpecular(out Color value);

		// Token: 0x0600225E RID: 8798
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_legacySpecular(ref Color value);

		// Token: 0x17000885 RID: 2181
		// (get) Token: 0x0600225F RID: 8799
		// (set) Token: 0x06002260 RID: 8800
		public extern float legacyShininess { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000886 RID: 2182
		// (get) Token: 0x06002261 RID: 8801
		// (set) Token: 0x06002262 RID: 8802
		public extern bool drawHeightmap { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000887 RID: 2183
		// (get) Token: 0x06002263 RID: 8803
		// (set) Token: 0x06002264 RID: 8804
		public extern bool drawTreesAndFoliage { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002265 RID: 8805 RVA: 0x00027B9C File Offset: 0x00025D9C
		public float SampleHeight(Vector3 worldPosition)
		{
			return Terrain.INTERNAL_CALL_SampleHeight(this, ref worldPosition);
		}

		// Token: 0x06002266 RID: 8806
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float INTERNAL_CALL_SampleHeight(Terrain self, ref Vector3 worldPosition);

		// Token: 0x06002267 RID: 8807
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void ApplyDelayedHeightmapModification();

		// Token: 0x06002268 RID: 8808 RVA: 0x00027BBC File Offset: 0x00025DBC
		public void AddTreeInstance(TreeInstance instance)
		{
			Terrain.INTERNAL_CALL_AddTreeInstance(this, ref instance);
		}

		// Token: 0x06002269 RID: 8809
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_AddTreeInstance(Terrain self, ref TreeInstance instance);

		// Token: 0x0600226A RID: 8810
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetNeighbors(Terrain left, Terrain top, Terrain right, Terrain bottom);

		// Token: 0x17000888 RID: 2184
		// (get) Token: 0x0600226B RID: 8811
		// (set) Token: 0x0600226C RID: 8812
		public extern float treeLODBiasMultiplier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000889 RID: 2185
		// (get) Token: 0x0600226D RID: 8813
		// (set) Token: 0x0600226E RID: 8814
		public extern bool collectDetailPatches { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700088A RID: 2186
		// (get) Token: 0x0600226F RID: 8815
		// (set) Token: 0x06002270 RID: 8816
		public extern TerrainRenderFlags editorRenderFlags { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002271 RID: 8817 RVA: 0x00027BC8 File Offset: 0x00025DC8
		public Vector3 GetPosition()
		{
			Vector3 result;
			Terrain.INTERNAL_CALL_GetPosition(this, out result);
			return result;
		}

		// Token: 0x06002272 RID: 8818
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetPosition(Terrain self, out Vector3 value);

		// Token: 0x06002273 RID: 8819
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Flush();

		// Token: 0x06002274 RID: 8820 RVA: 0x00027BE8 File Offset: 0x00025DE8
		internal void RemoveTrees(Vector2 position, float radius, int prototypeIndex)
		{
			Terrain.INTERNAL_CALL_RemoveTrees(this, ref position, radius, prototypeIndex);
		}

		// Token: 0x06002275 RID: 8821
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_RemoveTrees(Terrain self, ref Vector2 position, float radius, int prototypeIndex);

		// Token: 0x06002276 RID: 8822
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetSplatMaterialPropertyBlock(MaterialPropertyBlock properties);

		// Token: 0x06002277 RID: 8823 RVA: 0x00027BF8 File Offset: 0x00025DF8
		public void GetSplatMaterialPropertyBlock(MaterialPropertyBlock dest)
		{
			if (dest == null)
			{
				throw new ArgumentNullException("dest");
			}
			this.Internal_GetSplatMaterialPropertyBlock(dest);
		}

		// Token: 0x06002278 RID: 8824
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_GetSplatMaterialPropertyBlock(MaterialPropertyBlock dest);

		// Token: 0x1700088B RID: 2187
		// (get) Token: 0x06002279 RID: 8825
		public static extern Terrain activeTerrain { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700088C RID: 2188
		// (get) Token: 0x0600227A RID: 8826
		public static extern Terrain[] activeTerrains { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600227B RID: 8827
		[UsedByNativeCode]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern GameObject CreateTerrainGameObject(TerrainData assignTerrain);

		// Token: 0x020001F8 RID: 504
		public enum MaterialType
		{
			// Token: 0x040005EB RID: 1515
			BuiltInStandard,
			// Token: 0x040005EC RID: 1516
			BuiltInLegacyDiffuse,
			// Token: 0x040005ED RID: 1517
			BuiltInLegacySpecular,
			// Token: 0x040005EE RID: 1518
			Custom
		}
	}
}
