﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000045 RID: 69
	[UsedByNativeCode]
	public struct BoneWeight
	{
		// Token: 0x17000119 RID: 281
		// (get) Token: 0x06000507 RID: 1287 RVA: 0x000076A8 File Offset: 0x000058A8
		// (set) Token: 0x06000508 RID: 1288 RVA: 0x000076C4 File Offset: 0x000058C4
		public float weight0
		{
			get
			{
				return this.m_Weight0;
			}
			set
			{
				this.m_Weight0 = value;
			}
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x06000509 RID: 1289 RVA: 0x000076D0 File Offset: 0x000058D0
		// (set) Token: 0x0600050A RID: 1290 RVA: 0x000076EC File Offset: 0x000058EC
		public float weight1
		{
			get
			{
				return this.m_Weight1;
			}
			set
			{
				this.m_Weight1 = value;
			}
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x0600050B RID: 1291 RVA: 0x000076F8 File Offset: 0x000058F8
		// (set) Token: 0x0600050C RID: 1292 RVA: 0x00007714 File Offset: 0x00005914
		public float weight2
		{
			get
			{
				return this.m_Weight2;
			}
			set
			{
				this.m_Weight2 = value;
			}
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x0600050D RID: 1293 RVA: 0x00007720 File Offset: 0x00005920
		// (set) Token: 0x0600050E RID: 1294 RVA: 0x0000773C File Offset: 0x0000593C
		public float weight3
		{
			get
			{
				return this.m_Weight3;
			}
			set
			{
				this.m_Weight3 = value;
			}
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x0600050F RID: 1295 RVA: 0x00007748 File Offset: 0x00005948
		// (set) Token: 0x06000510 RID: 1296 RVA: 0x00007764 File Offset: 0x00005964
		public int boneIndex0
		{
			get
			{
				return this.m_BoneIndex0;
			}
			set
			{
				this.m_BoneIndex0 = value;
			}
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x06000511 RID: 1297 RVA: 0x00007770 File Offset: 0x00005970
		// (set) Token: 0x06000512 RID: 1298 RVA: 0x0000778C File Offset: 0x0000598C
		public int boneIndex1
		{
			get
			{
				return this.m_BoneIndex1;
			}
			set
			{
				this.m_BoneIndex1 = value;
			}
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06000513 RID: 1299 RVA: 0x00007798 File Offset: 0x00005998
		// (set) Token: 0x06000514 RID: 1300 RVA: 0x000077B4 File Offset: 0x000059B4
		public int boneIndex2
		{
			get
			{
				return this.m_BoneIndex2;
			}
			set
			{
				this.m_BoneIndex2 = value;
			}
		}

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06000515 RID: 1301 RVA: 0x000077C0 File Offset: 0x000059C0
		// (set) Token: 0x06000516 RID: 1302 RVA: 0x000077DC File Offset: 0x000059DC
		public int boneIndex3
		{
			get
			{
				return this.m_BoneIndex3;
			}
			set
			{
				this.m_BoneIndex3 = value;
			}
		}

		// Token: 0x06000517 RID: 1303 RVA: 0x000077E8 File Offset: 0x000059E8
		public override int GetHashCode()
		{
			return this.boneIndex0.GetHashCode() ^ this.boneIndex1.GetHashCode() << 2 ^ this.boneIndex2.GetHashCode() >> 2 ^ this.boneIndex3.GetHashCode() >> 1 ^ this.weight0.GetHashCode() << 5 ^ this.weight1.GetHashCode() << 4 ^ this.weight2.GetHashCode() >> 4 ^ this.weight3.GetHashCode() >> 3;
		}

		// Token: 0x06000518 RID: 1304 RVA: 0x000078B8 File Offset: 0x00005AB8
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is BoneWeight))
			{
				result = false;
			}
			else
			{
				BoneWeight boneWeight = (BoneWeight)other;
				bool flag;
				if (this.boneIndex0.Equals(boneWeight.boneIndex0) && this.boneIndex1.Equals(boneWeight.boneIndex1) && this.boneIndex2.Equals(boneWeight.boneIndex2) && this.boneIndex3.Equals(boneWeight.boneIndex3))
				{
					Vector4 vector = new Vector4(this.weight0, this.weight1, this.weight2, this.weight3);
					flag = vector.Equals(new Vector4(boneWeight.weight0, boneWeight.weight1, boneWeight.weight2, boneWeight.weight3));
				}
				else
				{
					flag = false;
				}
				result = flag;
			}
			return result;
		}

		// Token: 0x06000519 RID: 1305 RVA: 0x000079A8 File Offset: 0x00005BA8
		public static bool operator ==(BoneWeight lhs, BoneWeight rhs)
		{
			return lhs.boneIndex0 == rhs.boneIndex0 && lhs.boneIndex1 == rhs.boneIndex1 && lhs.boneIndex2 == rhs.boneIndex2 && lhs.boneIndex3 == rhs.boneIndex3 && new Vector4(lhs.weight0, lhs.weight1, lhs.weight2, lhs.weight3) == new Vector4(rhs.weight0, rhs.weight1, rhs.weight2, rhs.weight3);
		}

		// Token: 0x0600051A RID: 1306 RVA: 0x00007A54 File Offset: 0x00005C54
		public static bool operator !=(BoneWeight lhs, BoneWeight rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x04000064 RID: 100
		private float m_Weight0;

		// Token: 0x04000065 RID: 101
		private float m_Weight1;

		// Token: 0x04000066 RID: 102
		private float m_Weight2;

		// Token: 0x04000067 RID: 103
		private float m_Weight3;

		// Token: 0x04000068 RID: 104
		private int m_BoneIndex0;

		// Token: 0x04000069 RID: 105
		private int m_BoneIndex1;

		// Token: 0x0400006A RID: 106
		private int m_BoneIndex2;

		// Token: 0x0400006B RID: 107
		private int m_BoneIndex3;
	}
}
