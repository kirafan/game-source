﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020002CF RID: 719
	[UsedByNativeCode]
	public struct Color
	{
		// Token: 0x06002C4D RID: 11341 RVA: 0x0004521C File Offset: 0x0004341C
		public Color(float r, float g, float b, float a)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = a;
		}

		// Token: 0x06002C4E RID: 11342 RVA: 0x0004523C File Offset: 0x0004343C
		public Color(float r, float g, float b)
		{
			this.r = r;
			this.g = g;
			this.b = b;
			this.a = 1f;
		}

		// Token: 0x06002C4F RID: 11343 RVA: 0x00045260 File Offset: 0x00043460
		public override string ToString()
		{
			return UnityString.Format("RGBA({0:F3}, {1:F3}, {2:F3}, {3:F3})", new object[]
			{
				this.r,
				this.g,
				this.b,
				this.a
			});
		}

		// Token: 0x06002C50 RID: 11344 RVA: 0x000452C0 File Offset: 0x000434C0
		public string ToString(string format)
		{
			return UnityString.Format("RGBA({0}, {1}, {2}, {3})", new object[]
			{
				this.r.ToString(format),
				this.g.ToString(format),
				this.b.ToString(format),
				this.a.ToString(format)
			});
		}

		// Token: 0x06002C51 RID: 11345 RVA: 0x00045324 File Offset: 0x00043524
		public override int GetHashCode()
		{
			return this.GetHashCode();
		}

		// Token: 0x06002C52 RID: 11346 RVA: 0x00045354 File Offset: 0x00043554
		public override bool Equals(object other)
		{
			bool result;
			if (!(other is Color))
			{
				result = false;
			}
			else
			{
				Color color = (Color)other;
				result = (this.r.Equals(color.r) && this.g.Equals(color.g) && this.b.Equals(color.b) && this.a.Equals(color.a));
			}
			return result;
		}

		// Token: 0x06002C53 RID: 11347 RVA: 0x000453DC File Offset: 0x000435DC
		public static Color operator +(Color a, Color b)
		{
			return new Color(a.r + b.r, a.g + b.g, a.b + b.b, a.a + b.a);
		}

		// Token: 0x06002C54 RID: 11348 RVA: 0x00045434 File Offset: 0x00043634
		public static Color operator -(Color a, Color b)
		{
			return new Color(a.r - b.r, a.g - b.g, a.b - b.b, a.a - b.a);
		}

		// Token: 0x06002C55 RID: 11349 RVA: 0x0004548C File Offset: 0x0004368C
		public static Color operator *(Color a, Color b)
		{
			return new Color(a.r * b.r, a.g * b.g, a.b * b.b, a.a * b.a);
		}

		// Token: 0x06002C56 RID: 11350 RVA: 0x000454E4 File Offset: 0x000436E4
		public static Color operator *(Color a, float b)
		{
			return new Color(a.r * b, a.g * b, a.b * b, a.a * b);
		}

		// Token: 0x06002C57 RID: 11351 RVA: 0x00045524 File Offset: 0x00043724
		public static Color operator *(float b, Color a)
		{
			return new Color(a.r * b, a.g * b, a.b * b, a.a * b);
		}

		// Token: 0x06002C58 RID: 11352 RVA: 0x00045564 File Offset: 0x00043764
		public static Color operator /(Color a, float b)
		{
			return new Color(a.r / b, a.g / b, a.b / b, a.a / b);
		}

		// Token: 0x06002C59 RID: 11353 RVA: 0x000455A4 File Offset: 0x000437A4
		public static bool operator ==(Color lhs, Color rhs)
		{
			return lhs == rhs;
		}

		// Token: 0x06002C5A RID: 11354 RVA: 0x000455CC File Offset: 0x000437CC
		public static bool operator !=(Color lhs, Color rhs)
		{
			return !(lhs == rhs);
		}

		// Token: 0x06002C5B RID: 11355 RVA: 0x000455EC File Offset: 0x000437EC
		public static Color Lerp(Color a, Color b, float t)
		{
			t = Mathf.Clamp01(t);
			return new Color(a.r + (b.r - a.r) * t, a.g + (b.g - a.g) * t, a.b + (b.b - a.b) * t, a.a + (b.a - a.a) * t);
		}

		// Token: 0x06002C5C RID: 11356 RVA: 0x00045674 File Offset: 0x00043874
		public static Color LerpUnclamped(Color a, Color b, float t)
		{
			return new Color(a.r + (b.r - a.r) * t, a.g + (b.g - a.g) * t, a.b + (b.b - a.b) * t, a.a + (b.a - a.a) * t);
		}

		// Token: 0x06002C5D RID: 11357 RVA: 0x000456F4 File Offset: 0x000438F4
		internal Color RGBMultiplied(float multiplier)
		{
			return new Color(this.r * multiplier, this.g * multiplier, this.b * multiplier, this.a);
		}

		// Token: 0x06002C5E RID: 11358 RVA: 0x0004572C File Offset: 0x0004392C
		internal Color AlphaMultiplied(float multiplier)
		{
			return new Color(this.r, this.g, this.b, this.a * multiplier);
		}

		// Token: 0x06002C5F RID: 11359 RVA: 0x00045760 File Offset: 0x00043960
		internal Color RGBMultiplied(Color multiplier)
		{
			return new Color(this.r * multiplier.r, this.g * multiplier.g, this.b * multiplier.b, this.a);
		}

		// Token: 0x17000A70 RID: 2672
		// (get) Token: 0x06002C60 RID: 11360 RVA: 0x000457AC File Offset: 0x000439AC
		public static Color red
		{
			get
			{
				return new Color(1f, 0f, 0f, 1f);
			}
		}

		// Token: 0x17000A71 RID: 2673
		// (get) Token: 0x06002C61 RID: 11361 RVA: 0x000457DC File Offset: 0x000439DC
		public static Color green
		{
			get
			{
				return new Color(0f, 1f, 0f, 1f);
			}
		}

		// Token: 0x17000A72 RID: 2674
		// (get) Token: 0x06002C62 RID: 11362 RVA: 0x0004580C File Offset: 0x00043A0C
		public static Color blue
		{
			get
			{
				return new Color(0f, 0f, 1f, 1f);
			}
		}

		// Token: 0x17000A73 RID: 2675
		// (get) Token: 0x06002C63 RID: 11363 RVA: 0x0004583C File Offset: 0x00043A3C
		public static Color white
		{
			get
			{
				return new Color(1f, 1f, 1f, 1f);
			}
		}

		// Token: 0x17000A74 RID: 2676
		// (get) Token: 0x06002C64 RID: 11364 RVA: 0x0004586C File Offset: 0x00043A6C
		public static Color black
		{
			get
			{
				return new Color(0f, 0f, 0f, 1f);
			}
		}

		// Token: 0x17000A75 RID: 2677
		// (get) Token: 0x06002C65 RID: 11365 RVA: 0x0004589C File Offset: 0x00043A9C
		public static Color yellow
		{
			get
			{
				return new Color(1f, 0.92156863f, 0.015686275f, 1f);
			}
		}

		// Token: 0x17000A76 RID: 2678
		// (get) Token: 0x06002C66 RID: 11366 RVA: 0x000458CC File Offset: 0x00043ACC
		public static Color cyan
		{
			get
			{
				return new Color(0f, 1f, 1f, 1f);
			}
		}

		// Token: 0x17000A77 RID: 2679
		// (get) Token: 0x06002C67 RID: 11367 RVA: 0x000458FC File Offset: 0x00043AFC
		public static Color magenta
		{
			get
			{
				return new Color(1f, 0f, 1f, 1f);
			}
		}

		// Token: 0x17000A78 RID: 2680
		// (get) Token: 0x06002C68 RID: 11368 RVA: 0x0004592C File Offset: 0x00043B2C
		public static Color gray
		{
			get
			{
				return new Color(0.5f, 0.5f, 0.5f, 1f);
			}
		}

		// Token: 0x17000A79 RID: 2681
		// (get) Token: 0x06002C69 RID: 11369 RVA: 0x0004595C File Offset: 0x00043B5C
		public static Color grey
		{
			get
			{
				return new Color(0.5f, 0.5f, 0.5f, 1f);
			}
		}

		// Token: 0x17000A7A RID: 2682
		// (get) Token: 0x06002C6A RID: 11370 RVA: 0x0004598C File Offset: 0x00043B8C
		public static Color clear
		{
			get
			{
				return new Color(0f, 0f, 0f, 0f);
			}
		}

		// Token: 0x17000A7B RID: 2683
		// (get) Token: 0x06002C6B RID: 11371 RVA: 0x000459BC File Offset: 0x00043BBC
		public float grayscale
		{
			get
			{
				return 0.299f * this.r + 0.587f * this.g + 0.114f * this.b;
			}
		}

		// Token: 0x17000A7C RID: 2684
		// (get) Token: 0x06002C6C RID: 11372 RVA: 0x000459F8 File Offset: 0x00043BF8
		public Color linear
		{
			get
			{
				return new Color(Mathf.GammaToLinearSpace(this.r), Mathf.GammaToLinearSpace(this.g), Mathf.GammaToLinearSpace(this.b), this.a);
			}
		}

		// Token: 0x17000A7D RID: 2685
		// (get) Token: 0x06002C6D RID: 11373 RVA: 0x00045A3C File Offset: 0x00043C3C
		public Color gamma
		{
			get
			{
				return new Color(Mathf.LinearToGammaSpace(this.r), Mathf.LinearToGammaSpace(this.g), Mathf.LinearToGammaSpace(this.b), this.a);
			}
		}

		// Token: 0x17000A7E RID: 2686
		// (get) Token: 0x06002C6E RID: 11374 RVA: 0x00045A80 File Offset: 0x00043C80
		public float maxColorComponent
		{
			get
			{
				return Mathf.Max(Mathf.Max(this.r, this.g), this.b);
			}
		}

		// Token: 0x06002C6F RID: 11375 RVA: 0x00045AB4 File Offset: 0x00043CB4
		public static implicit operator Vector4(Color c)
		{
			return new Vector4(c.r, c.g, c.b, c.a);
		}

		// Token: 0x06002C70 RID: 11376 RVA: 0x00045AEC File Offset: 0x00043CEC
		public static implicit operator Color(Vector4 v)
		{
			return new Color(v.x, v.y, v.z, v.w);
		}

		// Token: 0x17000A7F RID: 2687
		public float this[int index]
		{
			get
			{
				float result;
				switch (index)
				{
				case 0:
					result = this.r;
					break;
				case 1:
					result = this.g;
					break;
				case 2:
					result = this.b;
					break;
				case 3:
					result = this.a;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector3 index!");
				}
				return result;
			}
			set
			{
				switch (index)
				{
				case 0:
					this.r = value;
					break;
				case 1:
					this.g = value;
					break;
				case 2:
					this.b = value;
					break;
				case 3:
					this.a = value;
					break;
				default:
					throw new IndexOutOfRangeException("Invalid Vector3 index!");
				}
			}
		}

		// Token: 0x06002C73 RID: 11379 RVA: 0x00045BF0 File Offset: 0x00043DF0
		public static void RGBToHSV(Color rgbColor, out float H, out float S, out float V)
		{
			if (rgbColor.b > rgbColor.g && rgbColor.b > rgbColor.r)
			{
				Color.RGBToHSVHelper(4f, rgbColor.b, rgbColor.r, rgbColor.g, out H, out S, out V);
			}
			else if (rgbColor.g > rgbColor.r)
			{
				Color.RGBToHSVHelper(2f, rgbColor.g, rgbColor.b, rgbColor.r, out H, out S, out V);
			}
			else
			{
				Color.RGBToHSVHelper(0f, rgbColor.r, rgbColor.g, rgbColor.b, out H, out S, out V);
			}
		}

		// Token: 0x06002C74 RID: 11380 RVA: 0x00045CA8 File Offset: 0x00043EA8
		private static void RGBToHSVHelper(float offset, float dominantcolor, float colorone, float colortwo, out float H, out float S, out float V)
		{
			V = dominantcolor;
			if (V != 0f)
			{
				float num;
				if (colorone > colortwo)
				{
					num = colortwo;
				}
				else
				{
					num = colorone;
				}
				float num2 = V - num;
				if (num2 != 0f)
				{
					S = num2 / V;
					H = offset + (colorone - colortwo) / num2;
				}
				else
				{
					S = 0f;
					H = offset + (colorone - colortwo);
				}
				H /= 6f;
				if (H < 0f)
				{
					H += 1f;
				}
			}
			else
			{
				S = 0f;
				H = 0f;
			}
		}

		// Token: 0x06002C75 RID: 11381 RVA: 0x00045D58 File Offset: 0x00043F58
		public static Color HSVToRGB(float H, float S, float V)
		{
			return Color.HSVToRGB(H, S, V, true);
		}

		// Token: 0x06002C76 RID: 11382 RVA: 0x00045D78 File Offset: 0x00043F78
		public static Color HSVToRGB(float H, float S, float V, bool hdr)
		{
			Color white = Color.white;
			if (S == 0f)
			{
				white.r = V;
				white.g = V;
				white.b = V;
			}
			else if (V == 0f)
			{
				white.r = 0f;
				white.g = 0f;
				white.b = 0f;
			}
			else
			{
				white.r = 0f;
				white.g = 0f;
				white.b = 0f;
				float num = H * 6f;
				int num2 = (int)Mathf.Floor(num);
				float num3 = num - (float)num2;
				float num4 = V * (1f - S);
				float num5 = V * (1f - S * num3);
				float num6 = V * (1f - S * (1f - num3));
				switch (num2 + 1)
				{
				case 0:
					white.r = V;
					white.g = num4;
					white.b = num5;
					break;
				case 1:
					white.r = V;
					white.g = num6;
					white.b = num4;
					break;
				case 2:
					white.r = num5;
					white.g = V;
					white.b = num4;
					break;
				case 3:
					white.r = num4;
					white.g = V;
					white.b = num6;
					break;
				case 4:
					white.r = num4;
					white.g = num5;
					white.b = V;
					break;
				case 5:
					white.r = num6;
					white.g = num4;
					white.b = V;
					break;
				case 6:
					white.r = V;
					white.g = num4;
					white.b = num5;
					break;
				case 7:
					white.r = V;
					white.g = num6;
					white.b = num4;
					break;
				}
				if (!hdr)
				{
					white.r = Mathf.Clamp(white.r, 0f, 1f);
					white.g = Mathf.Clamp(white.g, 0f, 1f);
					white.b = Mathf.Clamp(white.b, 0f, 1f);
				}
			}
			return white;
		}

		// Token: 0x04000AB7 RID: 2743
		public float r;

		// Token: 0x04000AB8 RID: 2744
		public float g;

		// Token: 0x04000AB9 RID: 2745
		public float b;

		// Token: 0x04000ABA RID: 2746
		public float a;
	}
}
