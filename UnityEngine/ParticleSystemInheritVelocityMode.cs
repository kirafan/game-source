﻿using System;

namespace UnityEngine
{
	// Token: 0x02000107 RID: 263
	public enum ParticleSystemInheritVelocityMode
	{
		// Token: 0x040002D7 RID: 727
		Initial,
		// Token: 0x040002D8 RID: 728
		Current
	}
}
