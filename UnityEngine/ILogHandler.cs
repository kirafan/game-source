﻿using System;

namespace UnityEngine
{
	// Token: 0x020003AB RID: 939
	public interface ILogHandler
	{
		// Token: 0x06003009 RID: 12297
		void LogFormat(LogType logType, Object context, string format, params object[] args);

		// Token: 0x0600300A RID: 12298
		void LogException(Exception exception, Object context);
	}
}
