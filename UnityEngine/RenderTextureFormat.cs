﻿using System;

namespace UnityEngine
{
	// Token: 0x020002FF RID: 767
	public enum RenderTextureFormat
	{
		// Token: 0x04000B9A RID: 2970
		ARGB32,
		// Token: 0x04000B9B RID: 2971
		Depth,
		// Token: 0x04000B9C RID: 2972
		ARGBHalf,
		// Token: 0x04000B9D RID: 2973
		Shadowmap,
		// Token: 0x04000B9E RID: 2974
		RGB565,
		// Token: 0x04000B9F RID: 2975
		ARGB4444,
		// Token: 0x04000BA0 RID: 2976
		ARGB1555,
		// Token: 0x04000BA1 RID: 2977
		Default,
		// Token: 0x04000BA2 RID: 2978
		ARGB2101010,
		// Token: 0x04000BA3 RID: 2979
		DefaultHDR,
		// Token: 0x04000BA4 RID: 2980
		ARGBFloat = 11,
		// Token: 0x04000BA5 RID: 2981
		RGFloat,
		// Token: 0x04000BA6 RID: 2982
		RGHalf,
		// Token: 0x04000BA7 RID: 2983
		RFloat,
		// Token: 0x04000BA8 RID: 2984
		RHalf,
		// Token: 0x04000BA9 RID: 2985
		R8,
		// Token: 0x04000BAA RID: 2986
		ARGBInt,
		// Token: 0x04000BAB RID: 2987
		RGInt,
		// Token: 0x04000BAC RID: 2988
		RInt,
		// Token: 0x04000BAD RID: 2989
		BGRA32,
		// Token: 0x04000BAE RID: 2990
		RGB111110Float = 22
	}
}
