﻿using System;

namespace UnityEngine.Scripting
{
	// Token: 0x020003AF RID: 943
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Interface, Inherited = false)]
	internal class RequiredByNativeCodeAttribute : Attribute
	{
		// Token: 0x06003029 RID: 12329 RVA: 0x0004F0FC File Offset: 0x0004D2FC
		public RequiredByNativeCodeAttribute()
		{
		}

		// Token: 0x0600302A RID: 12330 RVA: 0x0004F108 File Offset: 0x0004D308
		public RequiredByNativeCodeAttribute(string name)
		{
			this.Name = name;
		}

		// Token: 0x0600302B RID: 12331 RVA: 0x0004F118 File Offset: 0x0004D318
		public RequiredByNativeCodeAttribute(bool optional)
		{
			this.Optional = optional;
		}

		// Token: 0x0600302C RID: 12332 RVA: 0x0004F128 File Offset: 0x0004D328
		public RequiredByNativeCodeAttribute(string name, bool optional)
		{
			this.Name = name;
			this.Optional = optional;
		}

		// Token: 0x17000B3C RID: 2876
		// (get) Token: 0x0600302D RID: 12333 RVA: 0x0004F140 File Offset: 0x0004D340
		// (set) Token: 0x0600302E RID: 12334 RVA: 0x0004F15C File Offset: 0x0004D35C
		public string Name { get; set; }

		// Token: 0x17000B3D RID: 2877
		// (get) Token: 0x0600302F RID: 12335 RVA: 0x0004F168 File Offset: 0x0004D368
		// (set) Token: 0x06003030 RID: 12336 RVA: 0x0004F184 File Offset: 0x0004D384
		public bool Optional { get; set; }
	}
}
