﻿using System;

namespace UnityEngine.Scripting
{
	// Token: 0x020003AD RID: 941
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field, Inherited = false)]
	public class PreserveAttribute : Attribute
	{
	}
}
