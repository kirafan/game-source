﻿using System;

namespace UnityEngine.Scripting
{
	// Token: 0x020003AE RID: 942
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Interface, Inherited = false)]
	internal class UsedByNativeCodeAttribute : Attribute
	{
		// Token: 0x06003025 RID: 12325 RVA: 0x0004F0B8 File Offset: 0x0004D2B8
		public UsedByNativeCodeAttribute()
		{
		}

		// Token: 0x06003026 RID: 12326 RVA: 0x0004F0C4 File Offset: 0x0004D2C4
		public UsedByNativeCodeAttribute(string name)
		{
			this.Name = name;
		}

		// Token: 0x17000B3B RID: 2875
		// (get) Token: 0x06003027 RID: 12327 RVA: 0x0004F0D4 File Offset: 0x0004D2D4
		// (set) Token: 0x06003028 RID: 12328 RVA: 0x0004F0F0 File Offset: 0x0004D2F0
		public string Name { get; set; }
	}
}
