﻿using System;

namespace UnityEngine.Scripting.APIUpdating
{
	// Token: 0x020003B0 RID: 944
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate)]
	public class MovedFromAttribute : Attribute
	{
		// Token: 0x06003031 RID: 12337 RVA: 0x0004F190 File Offset: 0x0004D390
		public MovedFromAttribute(string sourceNamespace)
		{
			this.Namespace = sourceNamespace;
		}

		// Token: 0x17000B3E RID: 2878
		// (get) Token: 0x06003032 RID: 12338 RVA: 0x0004F1A0 File Offset: 0x0004D3A0
		// (set) Token: 0x06003033 RID: 12339 RVA: 0x0004F1BC File Offset: 0x0004D3BC
		public string Namespace { get; private set; }
	}
}
