﻿using System;
using System.Runtime.Serialization;

namespace UnityEngine
{
	// Token: 0x02000370 RID: 880
	[Serializable]
	public class MissingComponentException : SystemException
	{
		// Token: 0x06002E42 RID: 11842 RVA: 0x0004A5E0 File Offset: 0x000487E0
		public MissingComponentException() : base("A Unity Runtime error occurred!")
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E43 RID: 11843 RVA: 0x0004A5FC File Offset: 0x000487FC
		public MissingComponentException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E44 RID: 11844 RVA: 0x0004A614 File Offset: 0x00048814
		public MissingComponentException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06002E45 RID: 11845 RVA: 0x0004A62C File Offset: 0x0004882C
		protected MissingComponentException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000D49 RID: 3401
		private const int Result = -2147467261;

		// Token: 0x04000D4A RID: 3402
		private string unityStackTrace;
	}
}
