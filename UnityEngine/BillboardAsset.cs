﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x0200001F RID: 31
	public sealed class BillboardAsset : Object
	{
		// Token: 0x060001FF RID: 511 RVA: 0x00005010 File Offset: 0x00003210
		public BillboardAsset()
		{
			BillboardAsset.Internal_Create(this);
		}

		// Token: 0x06000200 RID: 512
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Create([Writable] BillboardAsset obj);

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x06000201 RID: 513
		// (set) Token: 0x06000202 RID: 514
		public extern float width { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000203 RID: 515
		// (set) Token: 0x06000204 RID: 516
		public extern float height { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000205 RID: 517
		// (set) Token: 0x06000206 RID: 518
		public extern float bottom { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x06000207 RID: 519
		public extern int imageCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x06000208 RID: 520
		public extern int vertexCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000209 RID: 521
		public extern int indexCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x0600020A RID: 522
		// (set) Token: 0x0600020B RID: 523
		public extern Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600020C RID: 524 RVA: 0x00005020 File Offset: 0x00003220
		public void GetImageTexCoords(List<Vector4> imageTexCoords)
		{
			if (imageTexCoords == null)
			{
				throw new ArgumentNullException("imageTexCoords");
			}
			this.GetImageTexCoordsInternal(imageTexCoords);
		}

		// Token: 0x0600020D RID: 525
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Vector4[] GetImageTexCoords();

		// Token: 0x0600020E RID: 526
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetImageTexCoordsInternal(object list);

		// Token: 0x0600020F RID: 527 RVA: 0x0000503C File Offset: 0x0000323C
		public void SetImageTexCoords(List<Vector4> imageTexCoords)
		{
			if (imageTexCoords == null)
			{
				throw new ArgumentNullException("imageTexCoords");
			}
			this.SetImageTexCoordsInternalList(imageTexCoords);
		}

		// Token: 0x06000210 RID: 528
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetImageTexCoords(Vector4[] imageTexCoords);

		// Token: 0x06000211 RID: 529
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetImageTexCoordsInternalList(object list);

		// Token: 0x06000212 RID: 530 RVA: 0x00005058 File Offset: 0x00003258
		public void GetVertices(List<Vector2> vertices)
		{
			if (vertices == null)
			{
				throw new ArgumentNullException("vertices");
			}
			this.GetVerticesInternal(vertices);
		}

		// Token: 0x06000213 RID: 531
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Vector2[] GetVertices();

		// Token: 0x06000214 RID: 532
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetVerticesInternal(object list);

		// Token: 0x06000215 RID: 533 RVA: 0x00005074 File Offset: 0x00003274
		public void SetVertices(List<Vector2> vertices)
		{
			if (vertices == null)
			{
				throw new ArgumentNullException("vertices");
			}
			this.SetVerticesInternalList(vertices);
		}

		// Token: 0x06000216 RID: 534
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetVertices(Vector2[] vertices);

		// Token: 0x06000217 RID: 535
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetVerticesInternalList(object list);

		// Token: 0x06000218 RID: 536 RVA: 0x00005090 File Offset: 0x00003290
		public void GetIndices(List<ushort> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			this.GetIndicesInternal(indices);
		}

		// Token: 0x06000219 RID: 537
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern ushort[] GetIndices();

		// Token: 0x0600021A RID: 538
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetIndicesInternal(object list);

		// Token: 0x0600021B RID: 539 RVA: 0x000050AC File Offset: 0x000032AC
		public void SetIndices(List<ushort> indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			this.SetIndicesInternalList(indices);
		}

		// Token: 0x0600021C RID: 540
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetIndices(ushort[] indices);

		// Token: 0x0600021D RID: 541
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetIndicesInternalList(object list);

		// Token: 0x0600021E RID: 542
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void MakeMaterialProperties(MaterialPropertyBlock properties, Camera camera);
	}
}
