﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020002A8 RID: 680
	[Obsolete("This component is part of the legacy particle system, which is deprecated and will be removed in a future release. Use the ParticleSystem component instead.", false)]
	public class ParticleEmitter : Component
	{
		// Token: 0x06002B2F RID: 11055 RVA: 0x00041964 File Offset: 0x0003FB64
		internal ParticleEmitter()
		{
		}

		// Token: 0x17000A3F RID: 2623
		// (get) Token: 0x06002B30 RID: 11056
		// (set) Token: 0x06002B31 RID: 11057
		public extern bool emit { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A40 RID: 2624
		// (get) Token: 0x06002B32 RID: 11058
		// (set) Token: 0x06002B33 RID: 11059
		public extern float minSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A41 RID: 2625
		// (get) Token: 0x06002B34 RID: 11060
		// (set) Token: 0x06002B35 RID: 11061
		public extern float maxSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A42 RID: 2626
		// (get) Token: 0x06002B36 RID: 11062
		// (set) Token: 0x06002B37 RID: 11063
		public extern float minEnergy { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A43 RID: 2627
		// (get) Token: 0x06002B38 RID: 11064
		// (set) Token: 0x06002B39 RID: 11065
		public extern float maxEnergy { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A44 RID: 2628
		// (get) Token: 0x06002B3A RID: 11066
		// (set) Token: 0x06002B3B RID: 11067
		public extern float minEmission { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A45 RID: 2629
		// (get) Token: 0x06002B3C RID: 11068
		// (set) Token: 0x06002B3D RID: 11069
		public extern float maxEmission { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A46 RID: 2630
		// (get) Token: 0x06002B3E RID: 11070
		// (set) Token: 0x06002B3F RID: 11071
		public extern float emitterVelocityScale { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A47 RID: 2631
		// (get) Token: 0x06002B40 RID: 11072 RVA: 0x00041970 File Offset: 0x0003FB70
		// (set) Token: 0x06002B41 RID: 11073 RVA: 0x00041990 File Offset: 0x0003FB90
		public Vector3 worldVelocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_worldVelocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_worldVelocity(ref value);
			}
		}

		// Token: 0x06002B42 RID: 11074
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_worldVelocity(out Vector3 value);

		// Token: 0x06002B43 RID: 11075
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_worldVelocity(ref Vector3 value);

		// Token: 0x17000A48 RID: 2632
		// (get) Token: 0x06002B44 RID: 11076 RVA: 0x0004199C File Offset: 0x0003FB9C
		// (set) Token: 0x06002B45 RID: 11077 RVA: 0x000419BC File Offset: 0x0003FBBC
		public Vector3 localVelocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_localVelocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localVelocity(ref value);
			}
		}

		// Token: 0x06002B46 RID: 11078
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localVelocity(out Vector3 value);

		// Token: 0x06002B47 RID: 11079
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localVelocity(ref Vector3 value);

		// Token: 0x17000A49 RID: 2633
		// (get) Token: 0x06002B48 RID: 11080 RVA: 0x000419C8 File Offset: 0x0003FBC8
		// (set) Token: 0x06002B49 RID: 11081 RVA: 0x000419E8 File Offset: 0x0003FBE8
		public Vector3 rndVelocity
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_rndVelocity(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_rndVelocity(ref value);
			}
		}

		// Token: 0x06002B4A RID: 11082
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rndVelocity(out Vector3 value);

		// Token: 0x06002B4B RID: 11083
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_rndVelocity(ref Vector3 value);

		// Token: 0x17000A4A RID: 2634
		// (get) Token: 0x06002B4C RID: 11084
		// (set) Token: 0x06002B4D RID: 11085
		public extern bool useWorldSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A4B RID: 2635
		// (get) Token: 0x06002B4E RID: 11086
		// (set) Token: 0x06002B4F RID: 11087
		public extern bool rndRotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A4C RID: 2636
		// (get) Token: 0x06002B50 RID: 11088
		// (set) Token: 0x06002B51 RID: 11089
		public extern float angularVelocity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A4D RID: 2637
		// (get) Token: 0x06002B52 RID: 11090
		// (set) Token: 0x06002B53 RID: 11091
		public extern float rndAngularVelocity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A4E RID: 2638
		// (get) Token: 0x06002B54 RID: 11092
		// (set) Token: 0x06002B55 RID: 11093
		public extern Particle[] particles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000A4F RID: 2639
		// (get) Token: 0x06002B56 RID: 11094
		public extern int particleCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06002B57 RID: 11095 RVA: 0x000419F4 File Offset: 0x0003FBF4
		public void ClearParticles()
		{
			ParticleEmitter.INTERNAL_CALL_ClearParticles(this);
		}

		// Token: 0x06002B58 RID: 11096
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_ClearParticles(ParticleEmitter self);

		// Token: 0x06002B59 RID: 11097 RVA: 0x00041A00 File Offset: 0x0003FC00
		public void Emit()
		{
			this.Emit2((int)Random.Range(this.minEmission, this.maxEmission));
		}

		// Token: 0x06002B5A RID: 11098 RVA: 0x00041A1C File Offset: 0x0003FC1C
		public void Emit(int count)
		{
			this.Emit2(count);
		}

		// Token: 0x06002B5B RID: 11099 RVA: 0x00041A28 File Offset: 0x0003FC28
		public void Emit(Vector3 pos, Vector3 velocity, float size, float energy, Color color)
		{
			InternalEmitParticleArguments internalEmitParticleArguments = default(InternalEmitParticleArguments);
			internalEmitParticleArguments.pos = pos;
			internalEmitParticleArguments.velocity = velocity;
			internalEmitParticleArguments.size = size;
			internalEmitParticleArguments.energy = energy;
			internalEmitParticleArguments.color = color;
			internalEmitParticleArguments.rotation = 0f;
			internalEmitParticleArguments.angularVelocity = 0f;
			this.Emit3(ref internalEmitParticleArguments);
		}

		// Token: 0x06002B5C RID: 11100 RVA: 0x00041A88 File Offset: 0x0003FC88
		public void Emit(Vector3 pos, Vector3 velocity, float size, float energy, Color color, float rotation, float angularVelocity)
		{
			InternalEmitParticleArguments internalEmitParticleArguments = default(InternalEmitParticleArguments);
			internalEmitParticleArguments.pos = pos;
			internalEmitParticleArguments.velocity = velocity;
			internalEmitParticleArguments.size = size;
			internalEmitParticleArguments.energy = energy;
			internalEmitParticleArguments.color = color;
			internalEmitParticleArguments.rotation = rotation;
			internalEmitParticleArguments.angularVelocity = angularVelocity;
			this.Emit3(ref internalEmitParticleArguments);
		}

		// Token: 0x06002B5D RID: 11101
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Emit2(int count);

		// Token: 0x06002B5E RID: 11102
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Emit3(ref InternalEmitParticleArguments args);

		// Token: 0x06002B5F RID: 11103
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void Simulate(float deltaTime);

		// Token: 0x17000A50 RID: 2640
		// (get) Token: 0x06002B60 RID: 11104
		// (set) Token: 0x06002B61 RID: 11105
		public extern bool enabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
