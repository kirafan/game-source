﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000181 RID: 385
	public sealed class BuoyancyEffector2D : Effector2D
	{
		// Token: 0x1700067B RID: 1659
		// (get) Token: 0x06001B6D RID: 7021
		// (set) Token: 0x06001B6E RID: 7022
		public extern float surfaceLevel { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700067C RID: 1660
		// (get) Token: 0x06001B6F RID: 7023
		// (set) Token: 0x06001B70 RID: 7024
		public extern float density { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700067D RID: 1661
		// (get) Token: 0x06001B71 RID: 7025
		// (set) Token: 0x06001B72 RID: 7026
		public extern float linearDrag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700067E RID: 1662
		// (get) Token: 0x06001B73 RID: 7027
		// (set) Token: 0x06001B74 RID: 7028
		public extern float angularDrag { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700067F RID: 1663
		// (get) Token: 0x06001B75 RID: 7029
		// (set) Token: 0x06001B76 RID: 7030
		public extern float flowAngle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000680 RID: 1664
		// (get) Token: 0x06001B77 RID: 7031
		// (set) Token: 0x06001B78 RID: 7032
		public extern float flowMagnitude { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000681 RID: 1665
		// (get) Token: 0x06001B79 RID: 7033
		// (set) Token: 0x06001B7A RID: 7034
		public extern float flowVariation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
