﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000150 RID: 336
	public sealed class BoxCollider : Collider
	{
		// Token: 0x170005A1 RID: 1441
		// (get) Token: 0x06001873 RID: 6259 RVA: 0x0001E980 File Offset: 0x0001CB80
		// (set) Token: 0x06001874 RID: 6260 RVA: 0x0001E9A0 File Offset: 0x0001CBA0
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_center(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_center(ref value);
			}
		}

		// Token: 0x06001875 RID: 6261
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_center(out Vector3 value);

		// Token: 0x06001876 RID: 6262
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_center(ref Vector3 value);

		// Token: 0x170005A2 RID: 1442
		// (get) Token: 0x06001877 RID: 6263 RVA: 0x0001E9AC File Offset: 0x0001CBAC
		// (set) Token: 0x06001878 RID: 6264 RVA: 0x0001E9CC File Offset: 0x0001CBCC
		public Vector3 size
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_size(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_size(ref value);
			}
		}

		// Token: 0x06001879 RID: 6265
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_size(out Vector3 value);

		// Token: 0x0600187A RID: 6266
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_size(ref Vector3 value);

		// Token: 0x170005A3 RID: 1443
		// (get) Token: 0x0600187B RID: 6267 RVA: 0x0001E9D8 File Offset: 0x0001CBD8
		// (set) Token: 0x0600187C RID: 6268 RVA: 0x0001EA00 File Offset: 0x0001CC00
		[Obsolete("use BoxCollider.size instead.")]
		public Vector3 extents
		{
			get
			{
				return this.size * 0.5f;
			}
			set
			{
				this.size = value * 2f;
			}
		}
	}
}
