﻿using System;

namespace UnityEngine
{
	// Token: 0x020002EB RID: 747
	public enum FogMode
	{
		// Token: 0x04000B06 RID: 2822
		Linear = 1,
		// Token: 0x04000B07 RID: 2823
		Exponential,
		// Token: 0x04000B08 RID: 2824
		ExponentialSquared
	}
}
