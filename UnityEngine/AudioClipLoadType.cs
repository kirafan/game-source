﻿using System;

namespace UnityEngine
{
	// Token: 0x0200019D RID: 413
	public enum AudioClipLoadType
	{
		// Token: 0x04000465 RID: 1125
		DecompressOnLoad,
		// Token: 0x04000466 RID: 1126
		CompressedInMemory,
		// Token: 0x04000467 RID: 1127
		Streaming
	}
}
