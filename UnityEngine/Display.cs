﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000036 RID: 54
	[UsedByNativeCode]
	public sealed class Display
	{
		// Token: 0x060003CD RID: 973 RVA: 0x00006860 File Offset: 0x00004A60
		internal Display()
		{
			this.nativeDisplay = new IntPtr(0);
		}

		// Token: 0x060003CE RID: 974 RVA: 0x00006878 File Offset: 0x00004A78
		internal Display(IntPtr nativeDisplay)
		{
			this.nativeDisplay = nativeDisplay;
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x060003CF RID: 975 RVA: 0x00006888 File Offset: 0x00004A88
		public int renderingWidth
		{
			get
			{
				int result = 0;
				int num = 0;
				Display.GetRenderingExtImpl(this.nativeDisplay, out result, out num);
				return result;
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060003D0 RID: 976 RVA: 0x000068B4 File Offset: 0x00004AB4
		public int renderingHeight
		{
			get
			{
				int num = 0;
				int result = 0;
				Display.GetRenderingExtImpl(this.nativeDisplay, out num, out result);
				return result;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x060003D1 RID: 977 RVA: 0x000068E0 File Offset: 0x00004AE0
		public int systemWidth
		{
			get
			{
				int result = 0;
				int num = 0;
				Display.GetSystemExtImpl(this.nativeDisplay, out result, out num);
				return result;
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x060003D2 RID: 978 RVA: 0x0000690C File Offset: 0x00004B0C
		public int systemHeight
		{
			get
			{
				int num = 0;
				int result = 0;
				Display.GetSystemExtImpl(this.nativeDisplay, out num, out result);
				return result;
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x060003D3 RID: 979 RVA: 0x00006938 File Offset: 0x00004B38
		public RenderBuffer colorBuffer
		{
			get
			{
				RenderBuffer result;
				RenderBuffer renderBuffer;
				Display.GetRenderingBuffersImpl(this.nativeDisplay, out result, out renderBuffer);
				return result;
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x060003D4 RID: 980 RVA: 0x00006960 File Offset: 0x00004B60
		public RenderBuffer depthBuffer
		{
			get
			{
				RenderBuffer renderBuffer;
				RenderBuffer result;
				Display.GetRenderingBuffersImpl(this.nativeDisplay, out renderBuffer, out result);
				return result;
			}
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x00006988 File Offset: 0x00004B88
		public void Activate()
		{
			Display.ActivateDisplayImpl(this.nativeDisplay, 0, 0, 60);
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x0000699C File Offset: 0x00004B9C
		public void Activate(int width, int height, int refreshRate)
		{
			Display.ActivateDisplayImpl(this.nativeDisplay, width, height, refreshRate);
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x000069B0 File Offset: 0x00004BB0
		public void SetParams(int width, int height, int x, int y)
		{
			Display.SetParamsImpl(this.nativeDisplay, width, height, x, y);
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x000069C4 File Offset: 0x00004BC4
		public void SetRenderingResolution(int w, int h)
		{
			Display.SetRenderingResolutionImpl(this.nativeDisplay, w, h);
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x000069D4 File Offset: 0x00004BD4
		[Obsolete("MultiDisplayLicense has been deprecated.", false)]
		public static bool MultiDisplayLicense()
		{
			return true;
		}

		// Token: 0x060003DA RID: 986 RVA: 0x000069EC File Offset: 0x00004BEC
		public static Vector3 RelativeMouseAt(Vector3 inputMouseCoordinates)
		{
			int num = 0;
			int num2 = 0;
			int x = (int)inputMouseCoordinates.x;
			int y = (int)inputMouseCoordinates.y;
			Vector3 result;
			result.z = (float)Display.RelativeMouseAtImpl(x, y, out num, out num2);
			result.x = (float)num;
			result.y = (float)num2;
			return result;
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060003DB RID: 987 RVA: 0x00006A44 File Offset: 0x00004C44
		public static Display main
		{
			get
			{
				return Display._mainDisplay;
			}
		}

		// Token: 0x060003DC RID: 988 RVA: 0x00006A60 File Offset: 0x00004C60
		[RequiredByNativeCode]
		private static void RecreateDisplayList(IntPtr[] nativeDisplay)
		{
			Display.displays = new Display[nativeDisplay.Length];
			for (int i = 0; i < nativeDisplay.Length; i++)
			{
				Display.displays[i] = new Display(nativeDisplay[i]);
			}
			Display._mainDisplay = Display.displays[0];
		}

		// Token: 0x060003DD RID: 989 RVA: 0x00006AAC File Offset: 0x00004CAC
		[RequiredByNativeCode]
		private static void FireDisplaysUpdated()
		{
			if (Display.onDisplaysUpdated != null)
			{
				Display.onDisplaysUpdated();
			}
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x060003DE RID: 990 RVA: 0x00006AC4 File Offset: 0x00004CC4
		// (remove) Token: 0x060003DF RID: 991 RVA: 0x00006AF8 File Offset: 0x00004CF8
		public static event Display.DisplaysUpdatedDelegate onDisplaysUpdated;

		// Token: 0x060003E0 RID: 992
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetSystemExtImpl(IntPtr nativeDisplay, out int w, out int h);

		// Token: 0x060003E1 RID: 993
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRenderingExtImpl(IntPtr nativeDisplay, out int w, out int h);

		// Token: 0x060003E2 RID: 994
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void GetRenderingBuffersImpl(IntPtr nativeDisplay, out RenderBuffer color, out RenderBuffer depth);

		// Token: 0x060003E3 RID: 995
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetRenderingResolutionImpl(IntPtr nativeDisplay, int w, int h);

		// Token: 0x060003E4 RID: 996
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ActivateDisplayImpl(IntPtr nativeDisplay, int width, int height, int refreshRate);

		// Token: 0x060003E5 RID: 997
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetParamsImpl(IntPtr nativeDisplay, int width, int height, int x, int y);

		// Token: 0x060003E6 RID: 998
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int RelativeMouseAtImpl(int x, int y, out int rx, out int ry);

		// Token: 0x060003E7 RID: 999 RVA: 0x00006B2C File Offset: 0x00004D2C
		// Note: this type is marked as 'beforefieldinit'.
		static Display()
		{
			Display.onDisplaysUpdated = null;
		}

		// Token: 0x04000055 RID: 85
		internal IntPtr nativeDisplay;

		// Token: 0x04000056 RID: 86
		public static Display[] displays = new Display[]
		{
			new Display()
		};

		// Token: 0x04000057 RID: 87
		private static Display _mainDisplay = Display.displays[0];

		// Token: 0x02000037 RID: 55
		// (Invoke) Token: 0x060003E9 RID: 1001
		public delegate void DisplaysUpdatedDelegate();
	}
}
