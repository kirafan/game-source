﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000155 RID: 341
	public sealed class PhysicMaterial : Object
	{
		// Token: 0x060018AC RID: 6316 RVA: 0x0001ED98 File Offset: 0x0001CF98
		public PhysicMaterial()
		{
			PhysicMaterial.Internal_CreateDynamicsMaterial(this, null);
		}

		// Token: 0x060018AD RID: 6317 RVA: 0x0001EDA8 File Offset: 0x0001CFA8
		public PhysicMaterial(string name)
		{
			PhysicMaterial.Internal_CreateDynamicsMaterial(this, name);
		}

		// Token: 0x060018AE RID: 6318
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateDynamicsMaterial([Writable] PhysicMaterial mat, string name);

		// Token: 0x170005BB RID: 1467
		// (get) Token: 0x060018AF RID: 6319
		// (set) Token: 0x060018B0 RID: 6320
		public extern float dynamicFriction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005BC RID: 1468
		// (get) Token: 0x060018B1 RID: 6321
		// (set) Token: 0x060018B2 RID: 6322
		public extern float staticFriction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005BD RID: 1469
		// (get) Token: 0x060018B3 RID: 6323
		// (set) Token: 0x060018B4 RID: 6324
		public extern float bounciness { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005BE RID: 1470
		// (get) Token: 0x060018B5 RID: 6325 RVA: 0x0001EDB8 File Offset: 0x0001CFB8
		// (set) Token: 0x060018B6 RID: 6326 RVA: 0x0001EDD4 File Offset: 0x0001CFD4
		[Obsolete("Use PhysicMaterial.bounciness instead", true)]
		public float bouncyness
		{
			get
			{
				return this.bounciness;
			}
			set
			{
				this.bounciness = value;
			}
		}

		// Token: 0x170005BF RID: 1471
		// (get) Token: 0x060018B7 RID: 6327 RVA: 0x0001EDE0 File Offset: 0x0001CFE0
		// (set) Token: 0x060018B8 RID: 6328 RVA: 0x0001EDFC File Offset: 0x0001CFFC
		[Obsolete("Anisotropic friction is no longer supported since Unity 5.0.", true)]
		public Vector3 frictionDirection2
		{
			get
			{
				return Vector3.zero;
			}
			set
			{
			}
		}

		// Token: 0x170005C0 RID: 1472
		// (get) Token: 0x060018B9 RID: 6329
		// (set) Token: 0x060018BA RID: 6330
		[Obsolete("Anisotropic friction is no longer supported since Unity 5.0.", true)]
		public extern float dynamicFriction2 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005C1 RID: 1473
		// (get) Token: 0x060018BB RID: 6331
		// (set) Token: 0x060018BC RID: 6332
		[Obsolete("Anisotropic friction is no longer supported since Unity 5.0.", true)]
		public extern float staticFriction2 { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005C2 RID: 1474
		// (get) Token: 0x060018BD RID: 6333
		// (set) Token: 0x060018BE RID: 6334
		public extern PhysicMaterialCombine frictionCombine { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005C3 RID: 1475
		// (get) Token: 0x060018BF RID: 6335
		// (set) Token: 0x060018C0 RID: 6336
		public extern PhysicMaterialCombine bounceCombine { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005C4 RID: 1476
		// (get) Token: 0x060018C1 RID: 6337 RVA: 0x0001EE00 File Offset: 0x0001D000
		// (set) Token: 0x060018C2 RID: 6338 RVA: 0x0001EE1C File Offset: 0x0001D01C
		[Obsolete("Anisotropic friction is no longer supported since Unity 5.0.", true)]
		public Vector3 frictionDirection
		{
			get
			{
				return Vector3.zero;
			}
			set
			{
			}
		}
	}
}
