﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000054 RID: 84
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class LightmapData
	{
		// Token: 0x1700016F RID: 367
		// (get) Token: 0x0600068F RID: 1679 RVA: 0x00008FBC File Offset: 0x000071BC
		// (set) Token: 0x06000690 RID: 1680 RVA: 0x00008FD8 File Offset: 0x000071D8
		public Texture2D lightmapLight
		{
			get
			{
				return this.m_Light;
			}
			set
			{
				this.m_Light = value;
			}
		}

		// Token: 0x17000170 RID: 368
		// (get) Token: 0x06000691 RID: 1681 RVA: 0x00008FE4 File Offset: 0x000071E4
		// (set) Token: 0x06000692 RID: 1682 RVA: 0x00009000 File Offset: 0x00007200
		public Texture2D lightmapDir
		{
			get
			{
				return this.m_Dir;
			}
			set
			{
				this.m_Dir = value;
			}
		}

		// Token: 0x04000084 RID: 132
		internal Texture2D m_Light;

		// Token: 0x04000085 RID: 133
		internal Texture2D m_Dir;
	}
}
