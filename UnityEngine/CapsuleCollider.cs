﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x02000153 RID: 339
	public sealed class CapsuleCollider : Collider
	{
		// Token: 0x170005AB RID: 1451
		// (get) Token: 0x06001890 RID: 6288 RVA: 0x0001EA74 File Offset: 0x0001CC74
		// (set) Token: 0x06001891 RID: 6289 RVA: 0x0001EA94 File Offset: 0x0001CC94
		public Vector3 center
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_center(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_center(ref value);
			}
		}

		// Token: 0x06001892 RID: 6290
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_center(out Vector3 value);

		// Token: 0x06001893 RID: 6291
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_center(ref Vector3 value);

		// Token: 0x170005AC RID: 1452
		// (get) Token: 0x06001894 RID: 6292
		// (set) Token: 0x06001895 RID: 6293
		public extern float radius { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005AD RID: 1453
		// (get) Token: 0x06001896 RID: 6294
		// (set) Token: 0x06001897 RID: 6295
		public extern float height { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170005AE RID: 1454
		// (get) Token: 0x06001898 RID: 6296
		// (set) Token: 0x06001899 RID: 6297
		public extern int direction { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
