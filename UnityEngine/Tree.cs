﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001F9 RID: 505
	public sealed class Tree : Component
	{
		// Token: 0x1700088D RID: 2189
		// (get) Token: 0x0600227D RID: 8829
		// (set) Token: 0x0600227E RID: 8830
		public extern ScriptableObject data { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700088E RID: 2190
		// (get) Token: 0x0600227F RID: 8831
		public extern bool hasSpeedTreeWind { [MethodImpl(MethodImplOptions.InternalCall)] get; }
	}
}
