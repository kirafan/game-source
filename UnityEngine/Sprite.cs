﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000BF RID: 191
	public sealed class Sprite : Object
	{
		// Token: 0x06000D4D RID: 3405 RVA: 0x00012E14 File Offset: 0x00011014
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot, [DefaultValue("100.0f")] float pixelsPerUnit, [DefaultValue("0")] uint extrude, [DefaultValue("SpriteMeshType.Tight")] SpriteMeshType meshType, [DefaultValue("Vector4.zero")] Vector4 border)
		{
			return Sprite.INTERNAL_CALL_Create(texture, ref rect, ref pivot, pixelsPerUnit, extrude, meshType, ref border);
		}

		// Token: 0x06000D4E RID: 3406 RVA: 0x00012E3C File Offset: 0x0001103C
		[ExcludeFromDocs]
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType)
		{
			Vector4 zero = Vector4.zero;
			return Sprite.INTERNAL_CALL_Create(texture, ref rect, ref pivot, pixelsPerUnit, extrude, meshType, ref zero);
		}

		// Token: 0x06000D4F RID: 3407 RVA: 0x00012E68 File Offset: 0x00011068
		[ExcludeFromDocs]
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot, float pixelsPerUnit, uint extrude)
		{
			Vector4 zero = Vector4.zero;
			SpriteMeshType meshType = SpriteMeshType.Tight;
			return Sprite.INTERNAL_CALL_Create(texture, ref rect, ref pivot, pixelsPerUnit, extrude, meshType, ref zero);
		}

		// Token: 0x06000D50 RID: 3408 RVA: 0x00012E98 File Offset: 0x00011098
		[ExcludeFromDocs]
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot, float pixelsPerUnit)
		{
			Vector4 zero = Vector4.zero;
			SpriteMeshType meshType = SpriteMeshType.Tight;
			uint extrude = 0U;
			return Sprite.INTERNAL_CALL_Create(texture, ref rect, ref pivot, pixelsPerUnit, extrude, meshType, ref zero);
		}

		// Token: 0x06000D51 RID: 3409 RVA: 0x00012EC8 File Offset: 0x000110C8
		[ExcludeFromDocs]
		public static Sprite Create(Texture2D texture, Rect rect, Vector2 pivot)
		{
			Vector4 zero = Vector4.zero;
			SpriteMeshType meshType = SpriteMeshType.Tight;
			uint extrude = 0U;
			float pixelsPerUnit = 100f;
			return Sprite.INTERNAL_CALL_Create(texture, ref rect, ref pivot, pixelsPerUnit, extrude, meshType, ref zero);
		}

		// Token: 0x06000D52 RID: 3410
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Sprite INTERNAL_CALL_Create(Texture2D texture, ref Rect rect, ref Vector2 pivot, float pixelsPerUnit, uint extrude, SpriteMeshType meshType, ref Vector4 border);

		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x06000D53 RID: 3411 RVA: 0x00012F00 File Offset: 0x00011100
		public Bounds bounds
		{
			get
			{
				Bounds result;
				this.INTERNAL_get_bounds(out result);
				return result;
			}
		}

		// Token: 0x06000D54 RID: 3412
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_bounds(out Bounds value);

		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06000D55 RID: 3413 RVA: 0x00012F20 File Offset: 0x00011120
		public Rect rect
		{
			get
			{
				Rect result;
				this.INTERNAL_get_rect(out result);
				return result;
			}
		}

		// Token: 0x06000D56 RID: 3414
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rect(out Rect value);

		// Token: 0x170002DB RID: 731
		// (get) Token: 0x06000D57 RID: 3415
		public extern float pixelsPerUnit { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002DC RID: 732
		// (get) Token: 0x06000D58 RID: 3416
		public extern Texture2D texture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x06000D59 RID: 3417
		public extern Texture2D associatedAlphaSplitTexture { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06000D5A RID: 3418 RVA: 0x00012F40 File Offset: 0x00011140
		public Rect textureRect
		{
			get
			{
				Rect result;
				this.INTERNAL_get_textureRect(out result);
				return result;
			}
		}

		// Token: 0x06000D5B RID: 3419
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_textureRect(out Rect value);

		// Token: 0x170002DF RID: 735
		// (get) Token: 0x06000D5C RID: 3420 RVA: 0x00012F60 File Offset: 0x00011160
		public Vector2 textureRectOffset
		{
			get
			{
				Vector2 result;
				Sprite.Internal_GetTextureRectOffset(this, out result);
				return result;
			}
		}

		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x06000D5D RID: 3421
		public extern bool packed { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x06000D5E RID: 3422
		public extern SpritePackingMode packingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06000D5F RID: 3423
		public extern SpritePackingRotation packingRotation { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000D60 RID: 3424
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_GetTextureRectOffset(Sprite sprite, out Vector2 output);

		// Token: 0x06000D61 RID: 3425
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_GetPivot(Sprite sprite, out Vector2 output);

		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06000D62 RID: 3426 RVA: 0x00012F80 File Offset: 0x00011180
		public Vector2 pivot
		{
			get
			{
				Vector2 result;
				Sprite.Internal_GetPivot(this, out result);
				return result;
			}
		}

		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06000D63 RID: 3427 RVA: 0x00012FA0 File Offset: 0x000111A0
		public Vector4 border
		{
			get
			{
				Vector4 result;
				this.INTERNAL_get_border(out result);
				return result;
			}
		}

		// Token: 0x06000D64 RID: 3428
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_border(out Vector4 value);

		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x06000D65 RID: 3429
		public extern Vector2[] vertices { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x06000D66 RID: 3430
		public extern ushort[] triangles { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x06000D67 RID: 3431
		public extern Vector2[] uv { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000D68 RID: 3432
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void OverrideGeometry(Vector2[] vertices, ushort[] triangles);
	}
}
