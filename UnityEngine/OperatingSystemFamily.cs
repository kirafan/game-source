﻿using System;

namespace UnityEngine
{
	// Token: 0x020002C8 RID: 712
	public enum OperatingSystemFamily
	{
		// Token: 0x04000A75 RID: 2677
		Other,
		// Token: 0x04000A76 RID: 2678
		MacOSX,
		// Token: 0x04000A77 RID: 2679
		Windows,
		// Token: 0x04000A78 RID: 2680
		Linux
	}
}
