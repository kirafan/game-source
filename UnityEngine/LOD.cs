﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000081 RID: 129
	[UsedByNativeCode]
	public struct LOD
	{
		// Token: 0x0600085C RID: 2140 RVA: 0x0000A254 File Offset: 0x00008454
		public LOD(float screenRelativeTransitionHeight, Renderer[] renderers)
		{
			this.screenRelativeTransitionHeight = screenRelativeTransitionHeight;
			this.fadeTransitionWidth = 0f;
			this.renderers = renderers;
		}

		// Token: 0x040000F6 RID: 246
		public float screenRelativeTransitionHeight;

		// Token: 0x040000F7 RID: 247
		public float fadeTransitionWidth;

		// Token: 0x040000F8 RID: 248
		public Renderer[] renderers;
	}
}
