﻿using System;

namespace UnityEngine
{
	// Token: 0x020002CA RID: 714
	public enum LogType
	{
		// Token: 0x04000AA6 RID: 2726
		Error,
		// Token: 0x04000AA7 RID: 2727
		Assert,
		// Token: 0x04000AA8 RID: 2728
		Warning,
		// Token: 0x04000AA9 RID: 2729
		Log,
		// Token: 0x04000AAA RID: 2730
		Exception
	}
}
