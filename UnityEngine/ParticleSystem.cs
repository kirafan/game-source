﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Rendering;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200010F RID: 271
	public sealed class ParticleSystem : Component
	{
		// Token: 0x1700038E RID: 910
		// (get) Token: 0x060010D4 RID: 4308
		// (set) Token: 0x060010D5 RID: 4309
		[Obsolete("startDelay property is deprecated. Use main.startDelay or main.startDelayMultiplier instead.")]
		public extern float startDelay { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700038F RID: 911
		// (get) Token: 0x060010D6 RID: 4310
		public extern bool isPlaying { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000390 RID: 912
		// (get) Token: 0x060010D7 RID: 4311
		public extern bool isEmitting { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000391 RID: 913
		// (get) Token: 0x060010D8 RID: 4312
		public extern bool isStopped { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000392 RID: 914
		// (get) Token: 0x060010D9 RID: 4313
		public extern bool isPaused { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000393 RID: 915
		// (get) Token: 0x060010DA RID: 4314
		// (set) Token: 0x060010DB RID: 4315
		[Obsolete("loop property is deprecated. Use main.loop instead.")]
		public extern bool loop { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000394 RID: 916
		// (get) Token: 0x060010DC RID: 4316
		// (set) Token: 0x060010DD RID: 4317
		[Obsolete("playOnAwake property is deprecated. Use main.playOnAwake instead.")]
		public extern bool playOnAwake { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000395 RID: 917
		// (get) Token: 0x060010DE RID: 4318
		// (set) Token: 0x060010DF RID: 4319
		public extern float time { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000396 RID: 918
		// (get) Token: 0x060010E0 RID: 4320
		[Obsolete("duration property is deprecated. Use main.duration instead.")]
		public extern float duration { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000397 RID: 919
		// (get) Token: 0x060010E1 RID: 4321
		// (set) Token: 0x060010E2 RID: 4322
		[Obsolete("playbackSpeed property is deprecated. Use main.simulationSpeed instead.")]
		public extern float playbackSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000398 RID: 920
		// (get) Token: 0x060010E3 RID: 4323
		public extern int particleCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000399 RID: 921
		// (get) Token: 0x060010E4 RID: 4324 RVA: 0x0001726C File Offset: 0x0001546C
		// (set) Token: 0x060010E5 RID: 4325 RVA: 0x00017290 File Offset: 0x00015490
		[Obsolete("enableEmission property is deprecated. Use emission.enabled instead.")]
		public bool enableEmission
		{
			get
			{
				return this.emission.enabled;
			}
			set
			{
				this.emission.enabled = value;
			}
		}

		// Token: 0x1700039A RID: 922
		// (get) Token: 0x060010E6 RID: 4326 RVA: 0x000172B0 File Offset: 0x000154B0
		// (set) Token: 0x060010E7 RID: 4327 RVA: 0x000172D4 File Offset: 0x000154D4
		[Obsolete("emissionRate property is deprecated. Use emission.rateOverTime, emission.rateOverDistance, emission.rateOverTimeMultiplier or emission.rateOverDistanceMultiplier instead.")]
		public float emissionRate
		{
			get
			{
				return this.emission.rateOverTimeMultiplier;
			}
			set
			{
				this.emission.rateOverTime = value;
			}
		}

		// Token: 0x1700039B RID: 923
		// (get) Token: 0x060010E8 RID: 4328
		// (set) Token: 0x060010E9 RID: 4329
		[Obsolete("startSpeed property is deprecated. Use main.startSpeed or main.startSpeedMultiplier instead.")]
		public extern float startSpeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700039C RID: 924
		// (get) Token: 0x060010EA RID: 4330
		// (set) Token: 0x060010EB RID: 4331
		[Obsolete("startSize property is deprecated. Use main.startSize or main.startSizeMultiplier instead.")]
		public extern float startSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700039D RID: 925
		// (get) Token: 0x060010EC RID: 4332 RVA: 0x000172F8 File Offset: 0x000154F8
		// (set) Token: 0x060010ED RID: 4333 RVA: 0x00017318 File Offset: 0x00015518
		[Obsolete("startColor property is deprecated. Use main.startColor instead.")]
		public Color startColor
		{
			get
			{
				Color result;
				this.INTERNAL_get_startColor(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_startColor(ref value);
			}
		}

		// Token: 0x060010EE RID: 4334
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_startColor(out Color value);

		// Token: 0x060010EF RID: 4335
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_startColor(ref Color value);

		// Token: 0x1700039E RID: 926
		// (get) Token: 0x060010F0 RID: 4336
		// (set) Token: 0x060010F1 RID: 4337
		[Obsolete("startRotation property is deprecated. Use main.startRotation or main.startRotationMultiplier instead.")]
		public extern float startRotation { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700039F RID: 927
		// (get) Token: 0x060010F2 RID: 4338 RVA: 0x00017324 File Offset: 0x00015524
		// (set) Token: 0x060010F3 RID: 4339 RVA: 0x00017344 File Offset: 0x00015544
		[Obsolete("startRotation3D property is deprecated. Use main.startRotationX, main.startRotationY and main.startRotationZ instead. (Or main.startRotationXMultiplier, main.startRotationYMultiplier and main.startRotationZMultiplier).")]
		public Vector3 startRotation3D
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_startRotation3D(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_startRotation3D(ref value);
			}
		}

		// Token: 0x060010F4 RID: 4340
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_startRotation3D(out Vector3 value);

		// Token: 0x060010F5 RID: 4341
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_startRotation3D(ref Vector3 value);

		// Token: 0x170003A0 RID: 928
		// (get) Token: 0x060010F6 RID: 4342
		// (set) Token: 0x060010F7 RID: 4343
		[Obsolete("startLifetime property is deprecated. Use main.startLifetime or main.startLifetimeMultiplier instead.")]
		public extern float startLifetime { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003A1 RID: 929
		// (get) Token: 0x060010F8 RID: 4344
		// (set) Token: 0x060010F9 RID: 4345
		[Obsolete("gravityModifier property is deprecated. Use main.gravityModifier or main.gravityModifierMultiplier instead.")]
		public extern float gravityModifier { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003A2 RID: 930
		// (get) Token: 0x060010FA RID: 4346
		// (set) Token: 0x060010FB RID: 4347
		[Obsolete("maxParticles property is deprecated. Use main.maxParticles instead.")]
		public extern int maxParticles { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003A3 RID: 931
		// (get) Token: 0x060010FC RID: 4348
		// (set) Token: 0x060010FD RID: 4349
		[Obsolete("simulationSpace property is deprecated. Use main.simulationSpace instead.")]
		public extern ParticleSystemSimulationSpace simulationSpace { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003A4 RID: 932
		// (get) Token: 0x060010FE RID: 4350
		// (set) Token: 0x060010FF RID: 4351
		[Obsolete("scalingMode property is deprecated. Use main.scalingMode instead.")]
		public extern ParticleSystemScalingMode scalingMode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003A5 RID: 933
		// (get) Token: 0x06001100 RID: 4352
		// (set) Token: 0x06001101 RID: 4353
		public extern uint randomSeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003A6 RID: 934
		// (get) Token: 0x06001102 RID: 4354
		// (set) Token: 0x06001103 RID: 4355
		public extern bool useAutoRandomSeed { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x06001104 RID: 4356 RVA: 0x00017350 File Offset: 0x00015550
		public ParticleSystem.MainModule main
		{
			get
			{
				return new ParticleSystem.MainModule(this);
			}
		}

		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x06001105 RID: 4357 RVA: 0x0001736C File Offset: 0x0001556C
		public ParticleSystem.EmissionModule emission
		{
			get
			{
				return new ParticleSystem.EmissionModule(this);
			}
		}

		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x06001106 RID: 4358 RVA: 0x00017388 File Offset: 0x00015588
		public ParticleSystem.ShapeModule shape
		{
			get
			{
				return new ParticleSystem.ShapeModule(this);
			}
		}

		// Token: 0x170003AA RID: 938
		// (get) Token: 0x06001107 RID: 4359 RVA: 0x000173A4 File Offset: 0x000155A4
		public ParticleSystem.VelocityOverLifetimeModule velocityOverLifetime
		{
			get
			{
				return new ParticleSystem.VelocityOverLifetimeModule(this);
			}
		}

		// Token: 0x170003AB RID: 939
		// (get) Token: 0x06001108 RID: 4360 RVA: 0x000173C0 File Offset: 0x000155C0
		public ParticleSystem.LimitVelocityOverLifetimeModule limitVelocityOverLifetime
		{
			get
			{
				return new ParticleSystem.LimitVelocityOverLifetimeModule(this);
			}
		}

		// Token: 0x170003AC RID: 940
		// (get) Token: 0x06001109 RID: 4361 RVA: 0x000173DC File Offset: 0x000155DC
		public ParticleSystem.InheritVelocityModule inheritVelocity
		{
			get
			{
				return new ParticleSystem.InheritVelocityModule(this);
			}
		}

		// Token: 0x170003AD RID: 941
		// (get) Token: 0x0600110A RID: 4362 RVA: 0x000173F8 File Offset: 0x000155F8
		public ParticleSystem.ForceOverLifetimeModule forceOverLifetime
		{
			get
			{
				return new ParticleSystem.ForceOverLifetimeModule(this);
			}
		}

		// Token: 0x170003AE RID: 942
		// (get) Token: 0x0600110B RID: 4363 RVA: 0x00017414 File Offset: 0x00015614
		public ParticleSystem.ColorOverLifetimeModule colorOverLifetime
		{
			get
			{
				return new ParticleSystem.ColorOverLifetimeModule(this);
			}
		}

		// Token: 0x170003AF RID: 943
		// (get) Token: 0x0600110C RID: 4364 RVA: 0x00017430 File Offset: 0x00015630
		public ParticleSystem.ColorBySpeedModule colorBySpeed
		{
			get
			{
				return new ParticleSystem.ColorBySpeedModule(this);
			}
		}

		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x0600110D RID: 4365 RVA: 0x0001744C File Offset: 0x0001564C
		public ParticleSystem.SizeOverLifetimeModule sizeOverLifetime
		{
			get
			{
				return new ParticleSystem.SizeOverLifetimeModule(this);
			}
		}

		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x0600110E RID: 4366 RVA: 0x00017468 File Offset: 0x00015668
		public ParticleSystem.SizeBySpeedModule sizeBySpeed
		{
			get
			{
				return new ParticleSystem.SizeBySpeedModule(this);
			}
		}

		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x0600110F RID: 4367 RVA: 0x00017484 File Offset: 0x00015684
		public ParticleSystem.RotationOverLifetimeModule rotationOverLifetime
		{
			get
			{
				return new ParticleSystem.RotationOverLifetimeModule(this);
			}
		}

		// Token: 0x170003B3 RID: 947
		// (get) Token: 0x06001110 RID: 4368 RVA: 0x000174A0 File Offset: 0x000156A0
		public ParticleSystem.RotationBySpeedModule rotationBySpeed
		{
			get
			{
				return new ParticleSystem.RotationBySpeedModule(this);
			}
		}

		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x06001111 RID: 4369 RVA: 0x000174BC File Offset: 0x000156BC
		public ParticleSystem.ExternalForcesModule externalForces
		{
			get
			{
				return new ParticleSystem.ExternalForcesModule(this);
			}
		}

		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x06001112 RID: 4370 RVA: 0x000174D8 File Offset: 0x000156D8
		public ParticleSystem.NoiseModule noise
		{
			get
			{
				return new ParticleSystem.NoiseModule(this);
			}
		}

		// Token: 0x170003B6 RID: 950
		// (get) Token: 0x06001113 RID: 4371 RVA: 0x000174F4 File Offset: 0x000156F4
		public ParticleSystem.CollisionModule collision
		{
			get
			{
				return new ParticleSystem.CollisionModule(this);
			}
		}

		// Token: 0x170003B7 RID: 951
		// (get) Token: 0x06001114 RID: 4372 RVA: 0x00017510 File Offset: 0x00015710
		public ParticleSystem.TriggerModule trigger
		{
			get
			{
				return new ParticleSystem.TriggerModule(this);
			}
		}

		// Token: 0x170003B8 RID: 952
		// (get) Token: 0x06001115 RID: 4373 RVA: 0x0001752C File Offset: 0x0001572C
		public ParticleSystem.SubEmittersModule subEmitters
		{
			get
			{
				return new ParticleSystem.SubEmittersModule(this);
			}
		}

		// Token: 0x170003B9 RID: 953
		// (get) Token: 0x06001116 RID: 4374 RVA: 0x00017548 File Offset: 0x00015748
		public ParticleSystem.TextureSheetAnimationModule textureSheetAnimation
		{
			get
			{
				return new ParticleSystem.TextureSheetAnimationModule(this);
			}
		}

		// Token: 0x170003BA RID: 954
		// (get) Token: 0x06001117 RID: 4375 RVA: 0x00017564 File Offset: 0x00015764
		public ParticleSystem.LightsModule lights
		{
			get
			{
				return new ParticleSystem.LightsModule(this);
			}
		}

		// Token: 0x170003BB RID: 955
		// (get) Token: 0x06001118 RID: 4376 RVA: 0x00017580 File Offset: 0x00015780
		public ParticleSystem.TrailModule trails
		{
			get
			{
				return new ParticleSystem.TrailModule(this);
			}
		}

		// Token: 0x06001119 RID: 4377
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetParticles(ParticleSystem.Particle[] particles, int size);

		// Token: 0x0600111A RID: 4378
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetParticles(ParticleSystem.Particle[] particles);

		// Token: 0x0600111B RID: 4379 RVA: 0x0001759C File Offset: 0x0001579C
		public void SetCustomParticleData(List<Vector4> customData, ParticleSystemCustomData streamIndex)
		{
			this.SetCustomParticleDataInternal(customData, (int)streamIndex);
		}

		// Token: 0x0600111C RID: 4380 RVA: 0x000175A8 File Offset: 0x000157A8
		public int GetCustomParticleData(List<Vector4> customData, ParticleSystemCustomData streamIndex)
		{
			return this.GetCustomParticleDataInternal(customData, (int)streamIndex);
		}

		// Token: 0x0600111D RID: 4381
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetCustomParticleDataInternal(object customData, int streamIndex);

		// Token: 0x0600111E RID: 4382
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern int GetCustomParticleDataInternal(object customData, int streamIndex);

		// Token: 0x0600111F RID: 4383
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_Simulate(ParticleSystem self, float t, bool restart, bool fixedTimeStep);

		// Token: 0x06001120 RID: 4384
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_Play(ParticleSystem self);

		// Token: 0x06001121 RID: 4385
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_Stop(ParticleSystem self, ParticleSystemStopBehavior stopBehavior);

		// Token: 0x06001122 RID: 4386
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_Pause(ParticleSystem self);

		// Token: 0x06001123 RID: 4387
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_Clear(ParticleSystem self);

		// Token: 0x06001124 RID: 4388
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Internal_IsAlive(ParticleSystem self);

		// Token: 0x06001125 RID: 4389 RVA: 0x000175C8 File Offset: 0x000157C8
		[ExcludeFromDocs]
		public void Simulate(float t, bool withChildren, bool restart)
		{
			bool fixedTimeStep = true;
			this.Simulate(t, withChildren, restart, fixedTimeStep);
		}

		// Token: 0x06001126 RID: 4390 RVA: 0x000175E4 File Offset: 0x000157E4
		[ExcludeFromDocs]
		public void Simulate(float t, bool withChildren)
		{
			bool fixedTimeStep = true;
			bool restart = true;
			this.Simulate(t, withChildren, restart, fixedTimeStep);
		}

		// Token: 0x06001127 RID: 4391 RVA: 0x00017600 File Offset: 0x00015800
		[ExcludeFromDocs]
		public void Simulate(float t)
		{
			bool fixedTimeStep = true;
			bool restart = true;
			bool withChildren = true;
			this.Simulate(t, withChildren, restart, fixedTimeStep);
		}

		// Token: 0x06001128 RID: 4392 RVA: 0x00017620 File Offset: 0x00015820
		public void Simulate(float t, [DefaultValue("true")] bool withChildren, [DefaultValue("true")] bool restart, [DefaultValue("true")] bool fixedTimeStep)
		{
			this.IterateParticleSystems(withChildren, (ParticleSystem ps) => ParticleSystem.Internal_Simulate(ps, t, restart, fixedTimeStep));
		}

		// Token: 0x06001129 RID: 4393 RVA: 0x00017660 File Offset: 0x00015860
		[ExcludeFromDocs]
		public void Play()
		{
			bool withChildren = true;
			this.Play(withChildren);
		}

		// Token: 0x0600112A RID: 4394 RVA: 0x00017678 File Offset: 0x00015878
		public void Play([DefaultValue("true")] bool withChildren)
		{
			this.IterateParticleSystems(withChildren, (ParticleSystem ps) => ParticleSystem.Internal_Play(ps));
		}

		// Token: 0x0600112B RID: 4395 RVA: 0x000176A0 File Offset: 0x000158A0
		[ExcludeFromDocs]
		public void Stop(bool withChildren)
		{
			ParticleSystemStopBehavior stopBehavior = ParticleSystemStopBehavior.StopEmitting;
			this.Stop(withChildren, stopBehavior);
		}

		// Token: 0x0600112C RID: 4396 RVA: 0x000176B8 File Offset: 0x000158B8
		[ExcludeFromDocs]
		public void Stop()
		{
			ParticleSystemStopBehavior stopBehavior = ParticleSystemStopBehavior.StopEmitting;
			bool withChildren = true;
			this.Stop(withChildren, stopBehavior);
		}

		// Token: 0x0600112D RID: 4397 RVA: 0x000176D4 File Offset: 0x000158D4
		public void Stop([DefaultValue("true")] bool withChildren, [DefaultValue("ParticleSystemStopBehavior.StopEmitting")] ParticleSystemStopBehavior stopBehavior)
		{
			this.IterateParticleSystems(withChildren, (ParticleSystem ps) => ParticleSystem.Internal_Stop(ps, stopBehavior));
		}

		// Token: 0x0600112E RID: 4398 RVA: 0x00017704 File Offset: 0x00015904
		[ExcludeFromDocs]
		public void Pause()
		{
			bool withChildren = true;
			this.Pause(withChildren);
		}

		// Token: 0x0600112F RID: 4399 RVA: 0x0001771C File Offset: 0x0001591C
		public void Pause([DefaultValue("true")] bool withChildren)
		{
			this.IterateParticleSystems(withChildren, (ParticleSystem ps) => ParticleSystem.Internal_Pause(ps));
		}

		// Token: 0x06001130 RID: 4400 RVA: 0x00017744 File Offset: 0x00015944
		[ExcludeFromDocs]
		public void Clear()
		{
			bool withChildren = true;
			this.Clear(withChildren);
		}

		// Token: 0x06001131 RID: 4401 RVA: 0x0001775C File Offset: 0x0001595C
		public void Clear([DefaultValue("true")] bool withChildren)
		{
			this.IterateParticleSystems(withChildren, (ParticleSystem ps) => ParticleSystem.Internal_Clear(ps));
		}

		// Token: 0x06001132 RID: 4402 RVA: 0x00017784 File Offset: 0x00015984
		[ExcludeFromDocs]
		public bool IsAlive()
		{
			bool withChildren = true;
			return this.IsAlive(withChildren);
		}

		// Token: 0x06001133 RID: 4403 RVA: 0x000177A4 File Offset: 0x000159A4
		public bool IsAlive([DefaultValue("true")] bool withChildren)
		{
			return this.IterateParticleSystems(withChildren, (ParticleSystem ps) => ParticleSystem.Internal_IsAlive(ps));
		}

		// Token: 0x06001134 RID: 4404 RVA: 0x000177E0 File Offset: 0x000159E0
		public void Emit(int count)
		{
			ParticleSystem.INTERNAL_CALL_Emit(this, count);
		}

		// Token: 0x06001135 RID: 4405
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Emit(ParticleSystem self, int count);

		// Token: 0x06001136 RID: 4406 RVA: 0x000177EC File Offset: 0x000159EC
		[Obsolete("Emit with specific parameters is deprecated. Pass a ParticleSystem.EmitParams parameter instead, which allows you to override some/all of the emission properties")]
		public void Emit(Vector3 position, Vector3 velocity, float size, float lifetime, Color32 color)
		{
			ParticleSystem.Particle particle = default(ParticleSystem.Particle);
			particle.position = position;
			particle.velocity = velocity;
			particle.lifetime = lifetime;
			particle.startLifetime = lifetime;
			particle.startSize = size;
			particle.rotation3D = Vector3.zero;
			particle.angularVelocity3D = Vector3.zero;
			particle.startColor = color;
			particle.randomSeed = 5U;
			this.Internal_EmitOld(ref particle);
		}

		// Token: 0x06001137 RID: 4407 RVA: 0x00017860 File Offset: 0x00015A60
		[Obsolete("Emit with a single particle structure is deprecated. Pass a ParticleSystem.EmitParams parameter instead, which allows you to override some/all of the emission properties")]
		public void Emit(ParticleSystem.Particle particle)
		{
			this.Internal_EmitOld(ref particle);
		}

		// Token: 0x06001138 RID: 4408
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_EmitOld(ref ParticleSystem.Particle particle);

		// Token: 0x06001139 RID: 4409 RVA: 0x0001786C File Offset: 0x00015A6C
		public void Emit(ParticleSystem.EmitParams emitParams, int count)
		{
			this.Internal_Emit(ref emitParams, count);
		}

		// Token: 0x0600113A RID: 4410
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Internal_Emit(ref ParticleSystem.EmitParams emitParams, int count);

		// Token: 0x0600113B RID: 4411 RVA: 0x00017878 File Offset: 0x00015A78
		internal bool IterateParticleSystems(bool recurse, ParticleSystem.IteratorDelegate func)
		{
			bool flag = func(this);
			if (recurse)
			{
				flag |= ParticleSystem.IterateParticleSystemsRecursive(base.transform, func);
			}
			return flag;
		}

		// Token: 0x0600113C RID: 4412 RVA: 0x000178AC File Offset: 0x00015AAC
		private static bool IterateParticleSystemsRecursive(Transform transform, ParticleSystem.IteratorDelegate func)
		{
			bool flag = false;
			int childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				Transform child = transform.GetChild(i);
				ParticleSystem component = child.gameObject.GetComponent<ParticleSystem>();
				if (component != null)
				{
					flag = func(component);
					if (flag)
					{
						break;
					}
					ParticleSystem.IterateParticleSystemsRecursive(child, func);
				}
			}
			return flag;
		}

		// Token: 0x02000110 RID: 272
		public struct Burst
		{
			// Token: 0x06001141 RID: 4417 RVA: 0x00017994 File Offset: 0x00015B94
			public Burst(float _time, short _count)
			{
				this.m_Time = _time;
				this.m_MinCount = _count;
				this.m_MaxCount = _count;
			}

			// Token: 0x06001142 RID: 4418 RVA: 0x000179AC File Offset: 0x00015BAC
			public Burst(float _time, short _minCount, short _maxCount)
			{
				this.m_Time = _time;
				this.m_MinCount = _minCount;
				this.m_MaxCount = _maxCount;
			}

			// Token: 0x170003BC RID: 956
			// (get) Token: 0x06001143 RID: 4419 RVA: 0x000179C4 File Offset: 0x00015BC4
			// (set) Token: 0x06001144 RID: 4420 RVA: 0x000179E0 File Offset: 0x00015BE0
			public float time
			{
				get
				{
					return this.m_Time;
				}
				set
				{
					this.m_Time = value;
				}
			}

			// Token: 0x170003BD RID: 957
			// (get) Token: 0x06001145 RID: 4421 RVA: 0x000179EC File Offset: 0x00015BEC
			// (set) Token: 0x06001146 RID: 4422 RVA: 0x00017A08 File Offset: 0x00015C08
			public short minCount
			{
				get
				{
					return this.m_MinCount;
				}
				set
				{
					this.m_MinCount = value;
				}
			}

			// Token: 0x170003BE RID: 958
			// (get) Token: 0x06001147 RID: 4423 RVA: 0x00017A14 File Offset: 0x00015C14
			// (set) Token: 0x06001148 RID: 4424 RVA: 0x00017A30 File Offset: 0x00015C30
			public short maxCount
			{
				get
				{
					return this.m_MaxCount;
				}
				set
				{
					this.m_MaxCount = value;
				}
			}

			// Token: 0x04000309 RID: 777
			private float m_Time;

			// Token: 0x0400030A RID: 778
			private short m_MinCount;

			// Token: 0x0400030B RID: 779
			private short m_MaxCount;
		}

		// Token: 0x02000111 RID: 273
		public struct MinMaxCurve
		{
			// Token: 0x06001149 RID: 4425 RVA: 0x00017A3C File Offset: 0x00015C3C
			public MinMaxCurve(float constant)
			{
				this.m_Mode = ParticleSystemCurveMode.Constant;
				this.m_CurveMultiplier = 0f;
				this.m_CurveMin = null;
				this.m_CurveMax = null;
				this.m_ConstantMin = 0f;
				this.m_ConstantMax = constant;
			}

			// Token: 0x0600114A RID: 4426 RVA: 0x00017A74 File Offset: 0x00015C74
			public MinMaxCurve(float multiplier, AnimationCurve curve)
			{
				this.m_Mode = ParticleSystemCurveMode.Curve;
				this.m_CurveMultiplier = multiplier;
				this.m_CurveMin = null;
				this.m_CurveMax = curve;
				this.m_ConstantMin = 0f;
				this.m_ConstantMax = 0f;
			}

			// Token: 0x0600114B RID: 4427 RVA: 0x00017AAC File Offset: 0x00015CAC
			public MinMaxCurve(float multiplier, AnimationCurve min, AnimationCurve max)
			{
				this.m_Mode = ParticleSystemCurveMode.TwoCurves;
				this.m_CurveMultiplier = multiplier;
				this.m_CurveMin = min;
				this.m_CurveMax = max;
				this.m_ConstantMin = 0f;
				this.m_ConstantMax = 0f;
			}

			// Token: 0x0600114C RID: 4428 RVA: 0x00017AE4 File Offset: 0x00015CE4
			public MinMaxCurve(float min, float max)
			{
				this.m_Mode = ParticleSystemCurveMode.TwoConstants;
				this.m_CurveMultiplier = 0f;
				this.m_CurveMin = null;
				this.m_CurveMax = null;
				this.m_ConstantMin = min;
				this.m_ConstantMax = max;
			}

			// Token: 0x170003BF RID: 959
			// (get) Token: 0x0600114D RID: 4429 RVA: 0x00017B18 File Offset: 0x00015D18
			// (set) Token: 0x0600114E RID: 4430 RVA: 0x00017B34 File Offset: 0x00015D34
			public ParticleSystemCurveMode mode
			{
				get
				{
					return this.m_Mode;
				}
				set
				{
					this.m_Mode = value;
				}
			}

			// Token: 0x170003C0 RID: 960
			// (get) Token: 0x0600114F RID: 4431 RVA: 0x00017B40 File Offset: 0x00015D40
			// (set) Token: 0x06001150 RID: 4432 RVA: 0x00017B5C File Offset: 0x00015D5C
			[Obsolete("Please use MinMaxCurve.curveMultiplier instead. (UnityUpgradable) -> UnityEngine.ParticleSystem/MinMaxCurve.curveMultiplier")]
			public float curveScalar
			{
				get
				{
					return this.m_CurveMultiplier;
				}
				set
				{
					this.m_CurveMultiplier = value;
				}
			}

			// Token: 0x170003C1 RID: 961
			// (get) Token: 0x06001151 RID: 4433 RVA: 0x00017B68 File Offset: 0x00015D68
			// (set) Token: 0x06001152 RID: 4434 RVA: 0x00017B84 File Offset: 0x00015D84
			public float curveMultiplier
			{
				get
				{
					return this.m_CurveMultiplier;
				}
				set
				{
					this.m_CurveMultiplier = value;
				}
			}

			// Token: 0x170003C2 RID: 962
			// (get) Token: 0x06001153 RID: 4435 RVA: 0x00017B90 File Offset: 0x00015D90
			// (set) Token: 0x06001154 RID: 4436 RVA: 0x00017BAC File Offset: 0x00015DAC
			public AnimationCurve curveMax
			{
				get
				{
					return this.m_CurveMax;
				}
				set
				{
					this.m_CurveMax = value;
				}
			}

			// Token: 0x170003C3 RID: 963
			// (get) Token: 0x06001155 RID: 4437 RVA: 0x00017BB8 File Offset: 0x00015DB8
			// (set) Token: 0x06001156 RID: 4438 RVA: 0x00017BD4 File Offset: 0x00015DD4
			public AnimationCurve curveMin
			{
				get
				{
					return this.m_CurveMin;
				}
				set
				{
					this.m_CurveMin = value;
				}
			}

			// Token: 0x170003C4 RID: 964
			// (get) Token: 0x06001157 RID: 4439 RVA: 0x00017BE0 File Offset: 0x00015DE0
			// (set) Token: 0x06001158 RID: 4440 RVA: 0x00017BFC File Offset: 0x00015DFC
			public float constantMax
			{
				get
				{
					return this.m_ConstantMax;
				}
				set
				{
					this.m_ConstantMax = value;
				}
			}

			// Token: 0x170003C5 RID: 965
			// (get) Token: 0x06001159 RID: 4441 RVA: 0x00017C08 File Offset: 0x00015E08
			// (set) Token: 0x0600115A RID: 4442 RVA: 0x00017C24 File Offset: 0x00015E24
			public float constantMin
			{
				get
				{
					return this.m_ConstantMin;
				}
				set
				{
					this.m_ConstantMin = value;
				}
			}

			// Token: 0x170003C6 RID: 966
			// (get) Token: 0x0600115B RID: 4443 RVA: 0x00017C30 File Offset: 0x00015E30
			// (set) Token: 0x0600115C RID: 4444 RVA: 0x00017C4C File Offset: 0x00015E4C
			public float constant
			{
				get
				{
					return this.m_ConstantMax;
				}
				set
				{
					this.m_ConstantMax = value;
				}
			}

			// Token: 0x170003C7 RID: 967
			// (get) Token: 0x0600115D RID: 4445 RVA: 0x00017C58 File Offset: 0x00015E58
			// (set) Token: 0x0600115E RID: 4446 RVA: 0x00017C74 File Offset: 0x00015E74
			public AnimationCurve curve
			{
				get
				{
					return this.m_CurveMax;
				}
				set
				{
					this.m_CurveMax = value;
				}
			}

			// Token: 0x0600115F RID: 4447 RVA: 0x00017C80 File Offset: 0x00015E80
			public float Evaluate(float time)
			{
				return this.Evaluate(time, 1f);
			}

			// Token: 0x06001160 RID: 4448 RVA: 0x00017CA4 File Offset: 0x00015EA4
			public float Evaluate(float time, float lerpFactor)
			{
				time = Mathf.Clamp(time, 0f, 1f);
				lerpFactor = Mathf.Clamp(lerpFactor, 0f, 1f);
				float result;
				if (this.m_Mode == ParticleSystemCurveMode.Constant)
				{
					result = this.m_ConstantMax;
				}
				else if (this.m_Mode == ParticleSystemCurveMode.TwoConstants)
				{
					result = Mathf.Lerp(this.m_ConstantMin, this.m_ConstantMax, lerpFactor);
				}
				else
				{
					float num = this.m_CurveMax.Evaluate(time) * this.m_CurveMultiplier;
					if (this.m_Mode == ParticleSystemCurveMode.TwoCurves)
					{
						result = Mathf.Lerp(this.m_CurveMin.Evaluate(time) * this.m_CurveMultiplier, num, lerpFactor);
					}
					else
					{
						result = num;
					}
				}
				return result;
			}

			// Token: 0x06001161 RID: 4449 RVA: 0x00017D5C File Offset: 0x00015F5C
			public static implicit operator ParticleSystem.MinMaxCurve(float constant)
			{
				return new ParticleSystem.MinMaxCurve(constant);
			}

			// Token: 0x0400030C RID: 780
			private ParticleSystemCurveMode m_Mode;

			// Token: 0x0400030D RID: 781
			private float m_CurveMultiplier;

			// Token: 0x0400030E RID: 782
			private AnimationCurve m_CurveMin;

			// Token: 0x0400030F RID: 783
			private AnimationCurve m_CurveMax;

			// Token: 0x04000310 RID: 784
			private float m_ConstantMin;

			// Token: 0x04000311 RID: 785
			private float m_ConstantMax;
		}

		// Token: 0x02000112 RID: 274
		public struct MinMaxGradient
		{
			// Token: 0x06001162 RID: 4450 RVA: 0x00017D78 File Offset: 0x00015F78
			public MinMaxGradient(Color color)
			{
				this.m_Mode = ParticleSystemGradientMode.Color;
				this.m_GradientMin = null;
				this.m_GradientMax = null;
				this.m_ColorMin = Color.black;
				this.m_ColorMax = color;
			}

			// Token: 0x06001163 RID: 4451 RVA: 0x00017DA4 File Offset: 0x00015FA4
			public MinMaxGradient(Gradient gradient)
			{
				this.m_Mode = ParticleSystemGradientMode.Gradient;
				this.m_GradientMin = null;
				this.m_GradientMax = gradient;
				this.m_ColorMin = Color.black;
				this.m_ColorMax = Color.black;
			}

			// Token: 0x06001164 RID: 4452 RVA: 0x00017DD4 File Offset: 0x00015FD4
			public MinMaxGradient(Color min, Color max)
			{
				this.m_Mode = ParticleSystemGradientMode.TwoColors;
				this.m_GradientMin = null;
				this.m_GradientMax = null;
				this.m_ColorMin = min;
				this.m_ColorMax = max;
			}

			// Token: 0x06001165 RID: 4453 RVA: 0x00017DFC File Offset: 0x00015FFC
			public MinMaxGradient(Gradient min, Gradient max)
			{
				this.m_Mode = ParticleSystemGradientMode.TwoGradients;
				this.m_GradientMin = min;
				this.m_GradientMax = max;
				this.m_ColorMin = Color.black;
				this.m_ColorMax = Color.black;
			}

			// Token: 0x170003C8 RID: 968
			// (get) Token: 0x06001166 RID: 4454 RVA: 0x00017E2C File Offset: 0x0001602C
			// (set) Token: 0x06001167 RID: 4455 RVA: 0x00017E48 File Offset: 0x00016048
			public ParticleSystemGradientMode mode
			{
				get
				{
					return this.m_Mode;
				}
				set
				{
					this.m_Mode = value;
				}
			}

			// Token: 0x170003C9 RID: 969
			// (get) Token: 0x06001168 RID: 4456 RVA: 0x00017E54 File Offset: 0x00016054
			// (set) Token: 0x06001169 RID: 4457 RVA: 0x00017E70 File Offset: 0x00016070
			public Gradient gradientMax
			{
				get
				{
					return this.m_GradientMax;
				}
				set
				{
					this.m_GradientMax = value;
				}
			}

			// Token: 0x170003CA RID: 970
			// (get) Token: 0x0600116A RID: 4458 RVA: 0x00017E7C File Offset: 0x0001607C
			// (set) Token: 0x0600116B RID: 4459 RVA: 0x00017E98 File Offset: 0x00016098
			public Gradient gradientMin
			{
				get
				{
					return this.m_GradientMin;
				}
				set
				{
					this.m_GradientMin = value;
				}
			}

			// Token: 0x170003CB RID: 971
			// (get) Token: 0x0600116C RID: 4460 RVA: 0x00017EA4 File Offset: 0x000160A4
			// (set) Token: 0x0600116D RID: 4461 RVA: 0x00017EC0 File Offset: 0x000160C0
			public Color colorMax
			{
				get
				{
					return this.m_ColorMax;
				}
				set
				{
					this.m_ColorMax = value;
				}
			}

			// Token: 0x170003CC RID: 972
			// (get) Token: 0x0600116E RID: 4462 RVA: 0x00017ECC File Offset: 0x000160CC
			// (set) Token: 0x0600116F RID: 4463 RVA: 0x00017EE8 File Offset: 0x000160E8
			public Color colorMin
			{
				get
				{
					return this.m_ColorMin;
				}
				set
				{
					this.m_ColorMin = value;
				}
			}

			// Token: 0x170003CD RID: 973
			// (get) Token: 0x06001170 RID: 4464 RVA: 0x00017EF4 File Offset: 0x000160F4
			// (set) Token: 0x06001171 RID: 4465 RVA: 0x00017F10 File Offset: 0x00016110
			public Color color
			{
				get
				{
					return this.m_ColorMax;
				}
				set
				{
					this.m_ColorMax = value;
				}
			}

			// Token: 0x170003CE RID: 974
			// (get) Token: 0x06001172 RID: 4466 RVA: 0x00017F1C File Offset: 0x0001611C
			// (set) Token: 0x06001173 RID: 4467 RVA: 0x00017F38 File Offset: 0x00016138
			public Gradient gradient
			{
				get
				{
					return this.m_GradientMax;
				}
				set
				{
					this.m_GradientMax = value;
				}
			}

			// Token: 0x06001174 RID: 4468 RVA: 0x00017F44 File Offset: 0x00016144
			public Color Evaluate(float time)
			{
				return this.Evaluate(time, 1f);
			}

			// Token: 0x06001175 RID: 4469 RVA: 0x00017F68 File Offset: 0x00016168
			public Color Evaluate(float time, float lerpFactor)
			{
				time = Mathf.Clamp(time, 0f, 1f);
				lerpFactor = Mathf.Clamp(lerpFactor, 0f, 1f);
				Color result;
				if (this.m_Mode == ParticleSystemGradientMode.Color)
				{
					result = this.m_ColorMax;
				}
				else if (this.m_Mode == ParticleSystemGradientMode.TwoColors)
				{
					result = Color.Lerp(this.m_ColorMin, this.m_ColorMax, lerpFactor);
				}
				else
				{
					Color color = this.m_GradientMax.Evaluate(time);
					if (this.m_Mode == ParticleSystemGradientMode.TwoGradients)
					{
						result = Color.Lerp(this.m_GradientMin.Evaluate(time), color, lerpFactor);
					}
					else
					{
						result = color;
					}
				}
				return result;
			}

			// Token: 0x06001176 RID: 4470 RVA: 0x00018010 File Offset: 0x00016210
			public static implicit operator ParticleSystem.MinMaxGradient(Color color)
			{
				return new ParticleSystem.MinMaxGradient(color);
			}

			// Token: 0x06001177 RID: 4471 RVA: 0x0001802C File Offset: 0x0001622C
			public static implicit operator ParticleSystem.MinMaxGradient(Gradient gradient)
			{
				return new ParticleSystem.MinMaxGradient(gradient);
			}

			// Token: 0x04000312 RID: 786
			private ParticleSystemGradientMode m_Mode;

			// Token: 0x04000313 RID: 787
			private Gradient m_GradientMin;

			// Token: 0x04000314 RID: 788
			private Gradient m_GradientMax;

			// Token: 0x04000315 RID: 789
			private Color m_ColorMin;

			// Token: 0x04000316 RID: 790
			private Color m_ColorMax;
		}

		// Token: 0x02000113 RID: 275
		public struct MainModule
		{
			// Token: 0x06001178 RID: 4472 RVA: 0x00018048 File Offset: 0x00016248
			internal MainModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x170003CF RID: 975
			// (get) Token: 0x06001179 RID: 4473 RVA: 0x00018054 File Offset: 0x00016254
			// (set) Token: 0x0600117A RID: 4474 RVA: 0x00018074 File Offset: 0x00016274
			public float duration
			{
				get
				{
					return ParticleSystem.MainModule.GetDuration(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetDuration(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003D0 RID: 976
			// (get) Token: 0x0600117B RID: 4475 RVA: 0x00018084 File Offset: 0x00016284
			// (set) Token: 0x0600117C RID: 4476 RVA: 0x000180A4 File Offset: 0x000162A4
			public bool loop
			{
				get
				{
					return ParticleSystem.MainModule.GetLoop(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetLoop(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003D1 RID: 977
			// (get) Token: 0x0600117D RID: 4477 RVA: 0x000180B4 File Offset: 0x000162B4
			// (set) Token: 0x0600117E RID: 4478 RVA: 0x000180D4 File Offset: 0x000162D4
			public bool prewarm
			{
				get
				{
					return ParticleSystem.MainModule.GetPrewarm(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetPrewarm(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003D2 RID: 978
			// (get) Token: 0x06001180 RID: 4480 RVA: 0x000180F4 File Offset: 0x000162F4
			// (set) Token: 0x0600117F RID: 4479 RVA: 0x000180E4 File Offset: 0x000162E4
			public ParticleSystem.MinMaxCurve startDelay
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartDelay(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartDelay(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003D3 RID: 979
			// (get) Token: 0x06001181 RID: 4481 RVA: 0x00018120 File Offset: 0x00016320
			// (set) Token: 0x06001182 RID: 4482 RVA: 0x00018140 File Offset: 0x00016340
			public float startDelayMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartDelayMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartDelayMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003D4 RID: 980
			// (get) Token: 0x06001184 RID: 4484 RVA: 0x00018160 File Offset: 0x00016360
			// (set) Token: 0x06001183 RID: 4483 RVA: 0x00018150 File Offset: 0x00016350
			public ParticleSystem.MinMaxCurve startLifetime
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartLifetime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartLifetime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003D5 RID: 981
			// (get) Token: 0x06001185 RID: 4485 RVA: 0x0001818C File Offset: 0x0001638C
			// (set) Token: 0x06001186 RID: 4486 RVA: 0x000181AC File Offset: 0x000163AC
			public float startLifetimeMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartLifetimeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartLifetimeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003D6 RID: 982
			// (get) Token: 0x06001188 RID: 4488 RVA: 0x000181CC File Offset: 0x000163CC
			// (set) Token: 0x06001187 RID: 4487 RVA: 0x000181BC File Offset: 0x000163BC
			public ParticleSystem.MinMaxCurve startSpeed
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSpeed(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSpeed(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003D7 RID: 983
			// (get) Token: 0x06001189 RID: 4489 RVA: 0x000181F8 File Offset: 0x000163F8
			// (set) Token: 0x0600118A RID: 4490 RVA: 0x00018218 File Offset: 0x00016418
			public float startSpeedMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSpeedMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSpeedMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003D8 RID: 984
			// (get) Token: 0x0600118B RID: 4491 RVA: 0x00018228 File Offset: 0x00016428
			// (set) Token: 0x0600118C RID: 4492 RVA: 0x00018248 File Offset: 0x00016448
			public bool startSize3D
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSize3D(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSize3D(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003D9 RID: 985
			// (get) Token: 0x0600118E RID: 4494 RVA: 0x00018268 File Offset: 0x00016468
			// (set) Token: 0x0600118D RID: 4493 RVA: 0x00018258 File Offset: 0x00016458
			public ParticleSystem.MinMaxCurve startSize
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSizeX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003DA RID: 986
			// (get) Token: 0x0600118F RID: 4495 RVA: 0x00018294 File Offset: 0x00016494
			// (set) Token: 0x06001190 RID: 4496 RVA: 0x000182B4 File Offset: 0x000164B4
			public float startSizeMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSizeXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003DB RID: 987
			// (get) Token: 0x06001192 RID: 4498 RVA: 0x000182D4 File Offset: 0x000164D4
			// (set) Token: 0x06001191 RID: 4497 RVA: 0x000182C4 File Offset: 0x000164C4
			public ParticleSystem.MinMaxCurve startSizeX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSizeX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003DC RID: 988
			// (get) Token: 0x06001193 RID: 4499 RVA: 0x00018300 File Offset: 0x00016500
			// (set) Token: 0x06001194 RID: 4500 RVA: 0x00018320 File Offset: 0x00016520
			public float startSizeXMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSizeXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003DD RID: 989
			// (get) Token: 0x06001196 RID: 4502 RVA: 0x00018340 File Offset: 0x00016540
			// (set) Token: 0x06001195 RID: 4501 RVA: 0x00018330 File Offset: 0x00016530
			public ParticleSystem.MinMaxCurve startSizeY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSizeY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003DE RID: 990
			// (get) Token: 0x06001197 RID: 4503 RVA: 0x0001836C File Offset: 0x0001656C
			// (set) Token: 0x06001198 RID: 4504 RVA: 0x0001838C File Offset: 0x0001658C
			public float startSizeYMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSizeYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003DF RID: 991
			// (get) Token: 0x0600119A RID: 4506 RVA: 0x000183AC File Offset: 0x000165AC
			// (set) Token: 0x06001199 RID: 4505 RVA: 0x0001839C File Offset: 0x0001659C
			public ParticleSystem.MinMaxCurve startSizeZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartSizeZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003E0 RID: 992
			// (get) Token: 0x0600119B RID: 4507 RVA: 0x000183D8 File Offset: 0x000165D8
			// (set) Token: 0x0600119C RID: 4508 RVA: 0x000183F8 File Offset: 0x000165F8
			public float startSizeZMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartSizeZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartSizeZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003E1 RID: 993
			// (get) Token: 0x0600119D RID: 4509 RVA: 0x00018408 File Offset: 0x00016608
			// (set) Token: 0x0600119E RID: 4510 RVA: 0x00018428 File Offset: 0x00016628
			public bool startRotation3D
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotation3D(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotation3D(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003E2 RID: 994
			// (get) Token: 0x060011A0 RID: 4512 RVA: 0x00018448 File Offset: 0x00016648
			// (set) Token: 0x0600119F RID: 4511 RVA: 0x00018438 File Offset: 0x00016638
			public ParticleSystem.MinMaxCurve startRotation
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartRotationZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003E3 RID: 995
			// (get) Token: 0x060011A1 RID: 4513 RVA: 0x00018474 File Offset: 0x00016674
			// (set) Token: 0x060011A2 RID: 4514 RVA: 0x00018494 File Offset: 0x00016694
			public float startRotationMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotationZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003E4 RID: 996
			// (get) Token: 0x060011A4 RID: 4516 RVA: 0x000184B4 File Offset: 0x000166B4
			// (set) Token: 0x060011A3 RID: 4515 RVA: 0x000184A4 File Offset: 0x000166A4
			public ParticleSystem.MinMaxCurve startRotationX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartRotationX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003E5 RID: 997
			// (get) Token: 0x060011A5 RID: 4517 RVA: 0x000184E0 File Offset: 0x000166E0
			// (set) Token: 0x060011A6 RID: 4518 RVA: 0x00018500 File Offset: 0x00016700
			public float startRotationXMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotationXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003E6 RID: 998
			// (get) Token: 0x060011A8 RID: 4520 RVA: 0x00018520 File Offset: 0x00016720
			// (set) Token: 0x060011A7 RID: 4519 RVA: 0x00018510 File Offset: 0x00016710
			public ParticleSystem.MinMaxCurve startRotationY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartRotationY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003E7 RID: 999
			// (get) Token: 0x060011A9 RID: 4521 RVA: 0x0001854C File Offset: 0x0001674C
			// (set) Token: 0x060011AA RID: 4522 RVA: 0x0001856C File Offset: 0x0001676C
			public float startRotationYMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotationYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003E8 RID: 1000
			// (get) Token: 0x060011AC RID: 4524 RVA: 0x0001858C File Offset: 0x0001678C
			// (set) Token: 0x060011AB RID: 4523 RVA: 0x0001857C File Offset: 0x0001677C
			public ParticleSystem.MinMaxCurve startRotationZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetStartRotationZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003E9 RID: 1001
			// (get) Token: 0x060011AD RID: 4525 RVA: 0x000185B8 File Offset: 0x000167B8
			// (set) Token: 0x060011AE RID: 4526 RVA: 0x000185D8 File Offset: 0x000167D8
			public float startRotationZMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetStartRotationZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetStartRotationZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003EA RID: 1002
			// (get) Token: 0x060011AF RID: 4527 RVA: 0x000185E8 File Offset: 0x000167E8
			// (set) Token: 0x060011B0 RID: 4528 RVA: 0x00018608 File Offset: 0x00016808
			public float randomizeRotationDirection
			{
				get
				{
					return ParticleSystem.MainModule.GetRandomizeRotationDirection(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetRandomizeRotationDirection(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003EB RID: 1003
			// (get) Token: 0x060011B2 RID: 4530 RVA: 0x00018628 File Offset: 0x00016828
			// (set) Token: 0x060011B1 RID: 4529 RVA: 0x00018618 File Offset: 0x00016818
			public ParticleSystem.MinMaxGradient startColor
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.MainModule.GetStartColor(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetStartColor(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003EC RID: 1004
			// (get) Token: 0x060011B4 RID: 4532 RVA: 0x00018664 File Offset: 0x00016864
			// (set) Token: 0x060011B3 RID: 4531 RVA: 0x00018654 File Offset: 0x00016854
			public ParticleSystem.MinMaxCurve gravityModifier
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.MainModule.GetGravityModifier(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.MainModule.SetGravityModifier(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003ED RID: 1005
			// (get) Token: 0x060011B5 RID: 4533 RVA: 0x00018690 File Offset: 0x00016890
			// (set) Token: 0x060011B6 RID: 4534 RVA: 0x000186B0 File Offset: 0x000168B0
			public float gravityModifierMultiplier
			{
				get
				{
					return ParticleSystem.MainModule.GetGravityModifierMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetGravityModifierMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003EE RID: 1006
			// (get) Token: 0x060011B7 RID: 4535 RVA: 0x000186C0 File Offset: 0x000168C0
			// (set) Token: 0x060011B8 RID: 4536 RVA: 0x000186E0 File Offset: 0x000168E0
			public ParticleSystemSimulationSpace simulationSpace
			{
				get
				{
					return ParticleSystem.MainModule.GetSimulationSpace(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetSimulationSpace(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003EF RID: 1007
			// (get) Token: 0x060011B9 RID: 4537 RVA: 0x000186F0 File Offset: 0x000168F0
			// (set) Token: 0x060011BA RID: 4538 RVA: 0x00018710 File Offset: 0x00016910
			public Transform customSimulationSpace
			{
				get
				{
					return ParticleSystem.MainModule.GetCustomSimulationSpace(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetCustomSimulationSpace(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003F0 RID: 1008
			// (get) Token: 0x060011BB RID: 4539 RVA: 0x00018720 File Offset: 0x00016920
			// (set) Token: 0x060011BC RID: 4540 RVA: 0x00018740 File Offset: 0x00016940
			public float simulationSpeed
			{
				get
				{
					return ParticleSystem.MainModule.GetSimulationSpeed(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetSimulationSpeed(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003F1 RID: 1009
			// (get) Token: 0x060011BD RID: 4541 RVA: 0x00018750 File Offset: 0x00016950
			// (set) Token: 0x060011BE RID: 4542 RVA: 0x00018770 File Offset: 0x00016970
			public ParticleSystemScalingMode scalingMode
			{
				get
				{
					return ParticleSystem.MainModule.GetScalingMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetScalingMode(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003F2 RID: 1010
			// (get) Token: 0x060011BF RID: 4543 RVA: 0x00018780 File Offset: 0x00016980
			// (set) Token: 0x060011C0 RID: 4544 RVA: 0x000187A0 File Offset: 0x000169A0
			public bool playOnAwake
			{
				get
				{
					return ParticleSystem.MainModule.GetPlayOnAwake(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetPlayOnAwake(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003F3 RID: 1011
			// (get) Token: 0x060011C1 RID: 4545 RVA: 0x000187B0 File Offset: 0x000169B0
			// (set) Token: 0x060011C2 RID: 4546 RVA: 0x000187D0 File Offset: 0x000169D0
			public int maxParticles
			{
				get
				{
					return ParticleSystem.MainModule.GetMaxParticles(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.MainModule.SetMaxParticles(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060011C3 RID: 4547
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060011C4 RID: 4548
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060011C5 RID: 4549
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDuration(ParticleSystem system, float value);

			// Token: 0x060011C6 RID: 4550
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetDuration(ParticleSystem system);

			// Token: 0x060011C7 RID: 4551
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLoop(ParticleSystem system, bool value);

			// Token: 0x060011C8 RID: 4552
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetLoop(ParticleSystem system);

			// Token: 0x060011C9 RID: 4553
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetPrewarm(ParticleSystem system, bool value);

			// Token: 0x060011CA RID: 4554
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetPrewarm(ParticleSystem system);

			// Token: 0x060011CB RID: 4555
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartDelay(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011CC RID: 4556
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartDelay(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011CD RID: 4557
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartDelayMultiplier(ParticleSystem system, float value);

			// Token: 0x060011CE RID: 4558
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartDelayMultiplier(ParticleSystem system);

			// Token: 0x060011CF RID: 4559
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011D0 RID: 4560
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011D1 RID: 4561
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartLifetimeMultiplier(ParticleSystem system, float value);

			// Token: 0x060011D2 RID: 4562
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartLifetimeMultiplier(ParticleSystem system);

			// Token: 0x060011D3 RID: 4563
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011D4 RID: 4564
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011D5 RID: 4565
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSpeedMultiplier(ParticleSystem system, float value);

			// Token: 0x060011D6 RID: 4566
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartSpeedMultiplier(ParticleSystem system);

			// Token: 0x060011D7 RID: 4567
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSize3D(ParticleSystem system, bool value);

			// Token: 0x060011D8 RID: 4568
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetStartSize3D(ParticleSystem system);

			// Token: 0x060011D9 RID: 4569
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011DA RID: 4570
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartSizeX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011DB RID: 4571
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeXMultiplier(ParticleSystem system, float value);

			// Token: 0x060011DC RID: 4572
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartSizeXMultiplier(ParticleSystem system);

			// Token: 0x060011DD RID: 4573
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011DE RID: 4574
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartSizeY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011DF RID: 4575
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeYMultiplier(ParticleSystem system, float value);

			// Token: 0x060011E0 RID: 4576
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartSizeYMultiplier(ParticleSystem system);

			// Token: 0x060011E1 RID: 4577
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011E2 RID: 4578
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartSizeZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011E3 RID: 4579
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartSizeZMultiplier(ParticleSystem system, float value);

			// Token: 0x060011E4 RID: 4580
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartSizeZMultiplier(ParticleSystem system);

			// Token: 0x060011E5 RID: 4581
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotation3D(ParticleSystem system, bool value);

			// Token: 0x060011E6 RID: 4582
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetStartRotation3D(ParticleSystem system);

			// Token: 0x060011E7 RID: 4583
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011E8 RID: 4584
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartRotationX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011E9 RID: 4585
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationXMultiplier(ParticleSystem system, float value);

			// Token: 0x060011EA RID: 4586
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartRotationXMultiplier(ParticleSystem system);

			// Token: 0x060011EB RID: 4587
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011EC RID: 4588
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartRotationY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011ED RID: 4589
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationYMultiplier(ParticleSystem system, float value);

			// Token: 0x060011EE RID: 4590
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartRotationYMultiplier(ParticleSystem system);

			// Token: 0x060011EF RID: 4591
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011F0 RID: 4592
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartRotationZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011F1 RID: 4593
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartRotationZMultiplier(ParticleSystem system, float value);

			// Token: 0x060011F2 RID: 4594
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartRotationZMultiplier(ParticleSystem system);

			// Token: 0x060011F3 RID: 4595
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRandomizeRotationDirection(ParticleSystem system, float value);

			// Token: 0x060011F4 RID: 4596
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRandomizeRotationDirection(ParticleSystem system);

			// Token: 0x060011F5 RID: 4597
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x060011F6 RID: 4598
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x060011F7 RID: 4599
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetGravityModifier(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011F8 RID: 4600
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetGravityModifier(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060011F9 RID: 4601
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetGravityModifierMultiplier(ParticleSystem system, float value);

			// Token: 0x060011FA RID: 4602
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetGravityModifierMultiplier(ParticleSystem system);

			// Token: 0x060011FB RID: 4603
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSimulationSpace(ParticleSystem system, ParticleSystemSimulationSpace value);

			// Token: 0x060011FC RID: 4604
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemSimulationSpace GetSimulationSpace(ParticleSystem system);

			// Token: 0x060011FD RID: 4605
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCustomSimulationSpace(ParticleSystem system, Transform value);

			// Token: 0x060011FE RID: 4606
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Transform GetCustomSimulationSpace(ParticleSystem system);

			// Token: 0x060011FF RID: 4607
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSimulationSpeed(ParticleSystem system, float value);

			// Token: 0x06001200 RID: 4608
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetSimulationSpeed(ParticleSystem system);

			// Token: 0x06001201 RID: 4609
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetScalingMode(ParticleSystem system, ParticleSystemScalingMode value);

			// Token: 0x06001202 RID: 4610
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystemScalingMode GetScalingMode(ParticleSystem system);

			// Token: 0x06001203 RID: 4611
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetPlayOnAwake(ParticleSystem system, bool value);

			// Token: 0x06001204 RID: 4612
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetPlayOnAwake(ParticleSystem system);

			// Token: 0x06001205 RID: 4613
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMaxParticles(ParticleSystem system, int value);

			// Token: 0x06001206 RID: 4614
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxParticles(ParticleSystem system);

			// Token: 0x04000317 RID: 791
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000114 RID: 276
		public struct EmissionModule
		{
			// Token: 0x06001207 RID: 4615 RVA: 0x000187E0 File Offset: 0x000169E0
			internal EmissionModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x170003F4 RID: 1012
			// (get) Token: 0x06001209 RID: 4617 RVA: 0x000187FC File Offset: 0x000169FC
			// (set) Token: 0x06001208 RID: 4616 RVA: 0x000187EC File Offset: 0x000169EC
			public bool enabled
			{
				get
				{
					return ParticleSystem.EmissionModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.EmissionModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003F5 RID: 1013
			// (get) Token: 0x0600120B RID: 4619 RVA: 0x0001882C File Offset: 0x00016A2C
			// (set) Token: 0x0600120A RID: 4618 RVA: 0x0001881C File Offset: 0x00016A1C
			public ParticleSystem.MinMaxCurve rateOverTime
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.EmissionModule.GetRateOverTime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverTime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003F6 RID: 1014
			// (get) Token: 0x0600120C RID: 4620 RVA: 0x00018858 File Offset: 0x00016A58
			// (set) Token: 0x0600120D RID: 4621 RVA: 0x00018878 File Offset: 0x00016A78
			public float rateOverTimeMultiplier
			{
				get
				{
					return ParticleSystem.EmissionModule.GetRateOverTimeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverTimeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003F7 RID: 1015
			// (get) Token: 0x0600120F RID: 4623 RVA: 0x00018898 File Offset: 0x00016A98
			// (set) Token: 0x0600120E RID: 4622 RVA: 0x00018888 File Offset: 0x00016A88
			public ParticleSystem.MinMaxCurve rateOverDistance
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.EmissionModule.GetRateOverDistance(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverDistance(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003F8 RID: 1016
			// (get) Token: 0x06001210 RID: 4624 RVA: 0x000188C4 File Offset: 0x00016AC4
			// (set) Token: 0x06001211 RID: 4625 RVA: 0x000188E4 File Offset: 0x00016AE4
			public float rateOverDistanceMultiplier
			{
				get
				{
					return ParticleSystem.EmissionModule.GetRateOverDistanceMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverDistanceMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06001212 RID: 4626 RVA: 0x000188F4 File Offset: 0x00016AF4
			public void SetBursts(ParticleSystem.Burst[] bursts)
			{
				ParticleSystem.EmissionModule.SetBursts(this.m_ParticleSystem, bursts, bursts.Length);
			}

			// Token: 0x06001213 RID: 4627 RVA: 0x00018908 File Offset: 0x00016B08
			public void SetBursts(ParticleSystem.Burst[] bursts, int size)
			{
				ParticleSystem.EmissionModule.SetBursts(this.m_ParticleSystem, bursts, size);
			}

			// Token: 0x06001214 RID: 4628 RVA: 0x00018918 File Offset: 0x00016B18
			public int GetBursts(ParticleSystem.Burst[] bursts)
			{
				return ParticleSystem.EmissionModule.GetBursts(this.m_ParticleSystem, bursts);
			}

			// Token: 0x170003F9 RID: 1017
			// (get) Token: 0x06001215 RID: 4629 RVA: 0x0001893C File Offset: 0x00016B3C
			public int burstCount
			{
				get
				{
					return ParticleSystem.EmissionModule.GetBurstCount(this.m_ParticleSystem);
				}
			}

			// Token: 0x170003FA RID: 1018
			// (get) Token: 0x06001216 RID: 4630 RVA: 0x0001895C File Offset: 0x00016B5C
			// (set) Token: 0x06001217 RID: 4631 RVA: 0x00018974 File Offset: 0x00016B74
			[Obsolete("ParticleSystemEmissionType no longer does anything. Time and Distance based emission are now both always active.")]
			public ParticleSystemEmissionType type
			{
				get
				{
					return ParticleSystemEmissionType.Time;
				}
				set
				{
				}
			}

			// Token: 0x170003FB RID: 1019
			// (get) Token: 0x06001219 RID: 4633 RVA: 0x00018988 File Offset: 0x00016B88
			// (set) Token: 0x06001218 RID: 4632 RVA: 0x00018978 File Offset: 0x00016B78
			[Obsolete("rate property is deprecated. Use rateOverTime or rateOverDistance instead.")]
			public ParticleSystem.MinMaxCurve rate
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.EmissionModule.GetRateOverTime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverTime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170003FC RID: 1020
			// (get) Token: 0x0600121A RID: 4634 RVA: 0x000189B4 File Offset: 0x00016BB4
			// (set) Token: 0x0600121B RID: 4635 RVA: 0x000189D4 File Offset: 0x00016BD4
			[Obsolete("rateMultiplier property is deprecated. Use rateOverTimeMultiplier or rateOverDistanceMultiplier instead.")]
			public float rateMultiplier
			{
				get
				{
					return ParticleSystem.EmissionModule.GetRateOverTimeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.EmissionModule.SetRateOverTimeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600121C RID: 4636
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600121D RID: 4637
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600121E RID: 4638
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetBurstCount(ParticleSystem system);

			// Token: 0x0600121F RID: 4639
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRateOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001220 RID: 4640
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRateOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001221 RID: 4641
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRateOverTimeMultiplier(ParticleSystem system, float value);

			// Token: 0x06001222 RID: 4642
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRateOverTimeMultiplier(ParticleSystem system);

			// Token: 0x06001223 RID: 4643
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRateOverDistance(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001224 RID: 4644
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRateOverDistance(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001225 RID: 4645
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRateOverDistanceMultiplier(ParticleSystem system, float value);

			// Token: 0x06001226 RID: 4646
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRateOverDistanceMultiplier(ParticleSystem system);

			// Token: 0x06001227 RID: 4647
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetBursts(ParticleSystem system, ParticleSystem.Burst[] bursts, int size);

			// Token: 0x06001228 RID: 4648
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetBursts(ParticleSystem system, ParticleSystem.Burst[] bursts);

			// Token: 0x04000318 RID: 792
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000115 RID: 277
		public struct ShapeModule
		{
			// Token: 0x06001229 RID: 4649 RVA: 0x000189E4 File Offset: 0x00016BE4
			internal ShapeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x170003FD RID: 1021
			// (get) Token: 0x0600122B RID: 4651 RVA: 0x00018A00 File Offset: 0x00016C00
			// (set) Token: 0x0600122A RID: 4650 RVA: 0x000189F0 File Offset: 0x00016BF0
			public bool enabled
			{
				get
				{
					return ParticleSystem.ShapeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170003FE RID: 1022
			// (get) Token: 0x0600122C RID: 4652 RVA: 0x00018A20 File Offset: 0x00016C20
			// (set) Token: 0x0600122D RID: 4653 RVA: 0x00018A40 File Offset: 0x00016C40
			public ParticleSystemShapeType shapeType
			{
				get
				{
					return (ParticleSystemShapeType)ParticleSystem.ShapeModule.GetShapeType(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetShapeType(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x170003FF RID: 1023
			// (get) Token: 0x0600122E RID: 4654 RVA: 0x00018A50 File Offset: 0x00016C50
			// (set) Token: 0x0600122F RID: 4655 RVA: 0x00018A70 File Offset: 0x00016C70
			public float randomDirectionAmount
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRandomDirectionAmount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRandomDirectionAmount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000400 RID: 1024
			// (get) Token: 0x06001230 RID: 4656 RVA: 0x00018A80 File Offset: 0x00016C80
			// (set) Token: 0x06001231 RID: 4657 RVA: 0x00018AA0 File Offset: 0x00016CA0
			public float sphericalDirectionAmount
			{
				get
				{
					return ParticleSystem.ShapeModule.GetSphericalDirectionAmount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetSphericalDirectionAmount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000401 RID: 1025
			// (get) Token: 0x06001232 RID: 4658 RVA: 0x00018AB0 File Offset: 0x00016CB0
			// (set) Token: 0x06001233 RID: 4659 RVA: 0x00018AD0 File Offset: 0x00016CD0
			public bool alignToDirection
			{
				get
				{
					return ParticleSystem.ShapeModule.GetAlignToDirection(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetAlignToDirection(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000402 RID: 1026
			// (get) Token: 0x06001234 RID: 4660 RVA: 0x00018AE0 File Offset: 0x00016CE0
			// (set) Token: 0x06001235 RID: 4661 RVA: 0x00018B00 File Offset: 0x00016D00
			public float radius
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRadius(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetRadius(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000403 RID: 1027
			// (get) Token: 0x06001236 RID: 4662 RVA: 0x00018B10 File Offset: 0x00016D10
			// (set) Token: 0x06001237 RID: 4663 RVA: 0x00018B30 File Offset: 0x00016D30
			public float angle
			{
				get
				{
					return ParticleSystem.ShapeModule.GetAngle(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetAngle(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000404 RID: 1028
			// (get) Token: 0x06001238 RID: 4664 RVA: 0x00018B40 File Offset: 0x00016D40
			// (set) Token: 0x06001239 RID: 4665 RVA: 0x00018B60 File Offset: 0x00016D60
			public float length
			{
				get
				{
					return ParticleSystem.ShapeModule.GetLength(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetLength(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000405 RID: 1029
			// (get) Token: 0x0600123A RID: 4666 RVA: 0x00018B70 File Offset: 0x00016D70
			// (set) Token: 0x0600123B RID: 4667 RVA: 0x00018B90 File Offset: 0x00016D90
			public Vector3 box
			{
				get
				{
					return ParticleSystem.ShapeModule.GetBox(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetBox(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000406 RID: 1030
			// (get) Token: 0x0600123C RID: 4668 RVA: 0x00018BA0 File Offset: 0x00016DA0
			// (set) Token: 0x0600123D RID: 4669 RVA: 0x00018BC0 File Offset: 0x00016DC0
			public ParticleSystemMeshShapeType meshShapeType
			{
				get
				{
					return (ParticleSystemMeshShapeType)ParticleSystem.ShapeModule.GetMeshShapeType(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshShapeType(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x17000407 RID: 1031
			// (get) Token: 0x0600123E RID: 4670 RVA: 0x00018BD0 File Offset: 0x00016DD0
			// (set) Token: 0x0600123F RID: 4671 RVA: 0x00018BF0 File Offset: 0x00016DF0
			public Mesh mesh
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMesh(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMesh(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000408 RID: 1032
			// (get) Token: 0x06001240 RID: 4672 RVA: 0x00018C00 File Offset: 0x00016E00
			// (set) Token: 0x06001241 RID: 4673 RVA: 0x00018C20 File Offset: 0x00016E20
			public MeshRenderer meshRenderer
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMeshRenderer(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshRenderer(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000409 RID: 1033
			// (get) Token: 0x06001242 RID: 4674 RVA: 0x00018C30 File Offset: 0x00016E30
			// (set) Token: 0x06001243 RID: 4675 RVA: 0x00018C50 File Offset: 0x00016E50
			public SkinnedMeshRenderer skinnedMeshRenderer
			{
				get
				{
					return ParticleSystem.ShapeModule.GetSkinnedMeshRenderer(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetSkinnedMeshRenderer(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700040A RID: 1034
			// (get) Token: 0x06001244 RID: 4676 RVA: 0x00018C60 File Offset: 0x00016E60
			// (set) Token: 0x06001245 RID: 4677 RVA: 0x00018C80 File Offset: 0x00016E80
			public bool useMeshMaterialIndex
			{
				get
				{
					return ParticleSystem.ShapeModule.GetUseMeshMaterialIndex(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetUseMeshMaterialIndex(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700040B RID: 1035
			// (get) Token: 0x06001246 RID: 4678 RVA: 0x00018C90 File Offset: 0x00016E90
			// (set) Token: 0x06001247 RID: 4679 RVA: 0x00018CB0 File Offset: 0x00016EB0
			public int meshMaterialIndex
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMeshMaterialIndex(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshMaterialIndex(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700040C RID: 1036
			// (get) Token: 0x06001248 RID: 4680 RVA: 0x00018CC0 File Offset: 0x00016EC0
			// (set) Token: 0x06001249 RID: 4681 RVA: 0x00018CE0 File Offset: 0x00016EE0
			public bool useMeshColors
			{
				get
				{
					return ParticleSystem.ShapeModule.GetUseMeshColors(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetUseMeshColors(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700040D RID: 1037
			// (get) Token: 0x0600124A RID: 4682 RVA: 0x00018CF0 File Offset: 0x00016EF0
			// (set) Token: 0x0600124B RID: 4683 RVA: 0x00018D10 File Offset: 0x00016F10
			public float normalOffset
			{
				get
				{
					return ParticleSystem.ShapeModule.GetNormalOffset(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetNormalOffset(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700040E RID: 1038
			// (get) Token: 0x0600124C RID: 4684 RVA: 0x00018D20 File Offset: 0x00016F20
			// (set) Token: 0x0600124D RID: 4685 RVA: 0x00018D40 File Offset: 0x00016F40
			public float meshScale
			{
				get
				{
					return ParticleSystem.ShapeModule.GetMeshScale(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetMeshScale(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700040F RID: 1039
			// (get) Token: 0x0600124E RID: 4686 RVA: 0x00018D50 File Offset: 0x00016F50
			// (set) Token: 0x0600124F RID: 4687 RVA: 0x00018D70 File Offset: 0x00016F70
			public float arc
			{
				get
				{
					return ParticleSystem.ShapeModule.GetArc(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ShapeModule.SetArc(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000410 RID: 1040
			// (get) Token: 0x06001250 RID: 4688 RVA: 0x00018D80 File Offset: 0x00016F80
			// (set) Token: 0x06001251 RID: 4689 RVA: 0x00018DAC File Offset: 0x00016FAC
			[Obsolete("randomDirection property is deprecated. Use randomDirectionAmount instead.")]
			public bool randomDirection
			{
				get
				{
					return ParticleSystem.ShapeModule.GetRandomDirectionAmount(this.m_ParticleSystem) >= 0.5f;
				}
				set
				{
					ParticleSystem.ShapeModule.SetRandomDirectionAmount(this.m_ParticleSystem, 1f);
				}
			}

			// Token: 0x06001252 RID: 4690
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06001253 RID: 4691
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06001254 RID: 4692
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetShapeType(ParticleSystem system, int value);

			// Token: 0x06001255 RID: 4693
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetShapeType(ParticleSystem system);

			// Token: 0x06001256 RID: 4694
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRandomDirectionAmount(ParticleSystem system, float value);

			// Token: 0x06001257 RID: 4695
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRandomDirectionAmount(ParticleSystem system);

			// Token: 0x06001258 RID: 4696
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSphericalDirectionAmount(ParticleSystem system, float value);

			// Token: 0x06001259 RID: 4697
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetSphericalDirectionAmount(ParticleSystem system);

			// Token: 0x0600125A RID: 4698
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetAlignToDirection(ParticleSystem system, bool value);

			// Token: 0x0600125B RID: 4699
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetAlignToDirection(ParticleSystem system);

			// Token: 0x0600125C RID: 4700
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadius(ParticleSystem system, float value);

			// Token: 0x0600125D RID: 4701
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadius(ParticleSystem system);

			// Token: 0x0600125E RID: 4702
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetAngle(ParticleSystem system, float value);

			// Token: 0x0600125F RID: 4703
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetAngle(ParticleSystem system);

			// Token: 0x06001260 RID: 4704
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLength(ParticleSystem system, float value);

			// Token: 0x06001261 RID: 4705
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetLength(ParticleSystem system);

			// Token: 0x06001262 RID: 4706 RVA: 0x00018DC0 File Offset: 0x00016FC0
			private static void SetBox(ParticleSystem system, Vector3 value)
			{
				ParticleSystem.ShapeModule.INTERNAL_CALL_SetBox(system, ref value);
			}

			// Token: 0x06001263 RID: 4707
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetBox(ParticleSystem system, ref Vector3 value);

			// Token: 0x06001264 RID: 4708 RVA: 0x00018DCC File Offset: 0x00016FCC
			private static Vector3 GetBox(ParticleSystem system)
			{
				Vector3 result;
				ParticleSystem.ShapeModule.INTERNAL_CALL_GetBox(system, out result);
				return result;
			}

			// Token: 0x06001265 RID: 4709
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetBox(ParticleSystem system, out Vector3 value);

			// Token: 0x06001266 RID: 4710
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshShapeType(ParticleSystem system, int value);

			// Token: 0x06001267 RID: 4711
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMeshShapeType(ParticleSystem system);

			// Token: 0x06001268 RID: 4712
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMesh(ParticleSystem system, Mesh value);

			// Token: 0x06001269 RID: 4713
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Mesh GetMesh(ParticleSystem system);

			// Token: 0x0600126A RID: 4714
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshRenderer(ParticleSystem system, MeshRenderer value);

			// Token: 0x0600126B RID: 4715
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern MeshRenderer GetMeshRenderer(ParticleSystem system);

			// Token: 0x0600126C RID: 4716
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSkinnedMeshRenderer(ParticleSystem system, SkinnedMeshRenderer value);

			// Token: 0x0600126D RID: 4717
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern SkinnedMeshRenderer GetSkinnedMeshRenderer(ParticleSystem system);

			// Token: 0x0600126E RID: 4718
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseMeshMaterialIndex(ParticleSystem system, bool value);

			// Token: 0x0600126F RID: 4719
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseMeshMaterialIndex(ParticleSystem system);

			// Token: 0x06001270 RID: 4720
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshMaterialIndex(ParticleSystem system, int value);

			// Token: 0x06001271 RID: 4721
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMeshMaterialIndex(ParticleSystem system);

			// Token: 0x06001272 RID: 4722
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseMeshColors(ParticleSystem system, bool value);

			// Token: 0x06001273 RID: 4723
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseMeshColors(ParticleSystem system);

			// Token: 0x06001274 RID: 4724
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetNormalOffset(ParticleSystem system, float value);

			// Token: 0x06001275 RID: 4725
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetNormalOffset(ParticleSystem system);

			// Token: 0x06001276 RID: 4726
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMeshScale(ParticleSystem system, float value);

			// Token: 0x06001277 RID: 4727
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMeshScale(ParticleSystem system);

			// Token: 0x06001278 RID: 4728
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetArc(ParticleSystem system, float value);

			// Token: 0x06001279 RID: 4729
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetArc(ParticleSystem system);

			// Token: 0x04000319 RID: 793
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000116 RID: 278
		public struct VelocityOverLifetimeModule
		{
			// Token: 0x0600127A RID: 4730 RVA: 0x00018DEC File Offset: 0x00016FEC
			internal VelocityOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000411 RID: 1041
			// (get) Token: 0x0600127C RID: 4732 RVA: 0x00018E08 File Offset: 0x00017008
			// (set) Token: 0x0600127B RID: 4731 RVA: 0x00018DF8 File Offset: 0x00016FF8
			public bool enabled
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000412 RID: 1042
			// (get) Token: 0x0600127E RID: 4734 RVA: 0x00018E38 File Offset: 0x00017038
			// (set) Token: 0x0600127D RID: 4733 RVA: 0x00018E28 File Offset: 0x00017028
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000413 RID: 1043
			// (get) Token: 0x06001280 RID: 4736 RVA: 0x00018E74 File Offset: 0x00017074
			// (set) Token: 0x0600127F RID: 4735 RVA: 0x00018E64 File Offset: 0x00017064
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000414 RID: 1044
			// (get) Token: 0x06001282 RID: 4738 RVA: 0x00018EB0 File Offset: 0x000170B0
			// (set) Token: 0x06001281 RID: 4737 RVA: 0x00018EA0 File Offset: 0x000170A0
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.VelocityOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000415 RID: 1045
			// (get) Token: 0x06001283 RID: 4739 RVA: 0x00018EDC File Offset: 0x000170DC
			// (set) Token: 0x06001284 RID: 4740 RVA: 0x00018EFC File Offset: 0x000170FC
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000416 RID: 1046
			// (get) Token: 0x06001285 RID: 4741 RVA: 0x00018F0C File Offset: 0x0001710C
			// (set) Token: 0x06001286 RID: 4742 RVA: 0x00018F2C File Offset: 0x0001712C
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000417 RID: 1047
			// (get) Token: 0x06001287 RID: 4743 RVA: 0x00018F3C File Offset: 0x0001713C
			// (set) Token: 0x06001288 RID: 4744 RVA: 0x00018F5C File Offset: 0x0001715C
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.VelocityOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000418 RID: 1048
			// (get) Token: 0x06001289 RID: 4745 RVA: 0x00018F6C File Offset: 0x0001716C
			// (set) Token: 0x0600128A RID: 4746 RVA: 0x00018F98 File Offset: 0x00017198
			public ParticleSystemSimulationSpace space
			{
				get
				{
					return (!ParticleSystem.VelocityOverLifetimeModule.GetWorldSpace(this.m_ParticleSystem)) ? ParticleSystemSimulationSpace.Local : ParticleSystemSimulationSpace.World;
				}
				set
				{
					ParticleSystem.VelocityOverLifetimeModule.SetWorldSpace(this.m_ParticleSystem, value == ParticleSystemSimulationSpace.World);
				}
			}

			// Token: 0x0600128B RID: 4747
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600128C RID: 4748
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600128D RID: 4749
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600128E RID: 4750
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600128F RID: 4751
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001290 RID: 4752
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001291 RID: 4753
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001292 RID: 4754
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001293 RID: 4755
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x06001294 RID: 4756
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x06001295 RID: 4757
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x06001296 RID: 4758
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x06001297 RID: 4759
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x06001298 RID: 4760
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x06001299 RID: 4761
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWorldSpace(ParticleSystem system, bool value);

			// Token: 0x0600129A RID: 4762
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetWorldSpace(ParticleSystem system);

			// Token: 0x0400031A RID: 794
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000117 RID: 279
		public struct LimitVelocityOverLifetimeModule
		{
			// Token: 0x0600129B RID: 4763 RVA: 0x00018FAC File Offset: 0x000171AC
			internal LimitVelocityOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000419 RID: 1049
			// (get) Token: 0x0600129D RID: 4765 RVA: 0x00018FC8 File Offset: 0x000171C8
			// (set) Token: 0x0600129C RID: 4764 RVA: 0x00018FB8 File Offset: 0x000171B8
			public bool enabled
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700041A RID: 1050
			// (get) Token: 0x0600129F RID: 4767 RVA: 0x00018FF8 File Offset: 0x000171F8
			// (set) Token: 0x0600129E RID: 4766 RVA: 0x00018FE8 File Offset: 0x000171E8
			public ParticleSystem.MinMaxCurve limitX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LimitVelocityOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700041B RID: 1051
			// (get) Token: 0x060012A0 RID: 4768 RVA: 0x00019024 File Offset: 0x00017224
			// (set) Token: 0x060012A1 RID: 4769 RVA: 0x00019044 File Offset: 0x00017244
			public float limitXMultiplier
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700041C RID: 1052
			// (get) Token: 0x060012A3 RID: 4771 RVA: 0x00019064 File Offset: 0x00017264
			// (set) Token: 0x060012A2 RID: 4770 RVA: 0x00019054 File Offset: 0x00017254
			public ParticleSystem.MinMaxCurve limitY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LimitVelocityOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700041D RID: 1053
			// (get) Token: 0x060012A4 RID: 4772 RVA: 0x00019090 File Offset: 0x00017290
			// (set) Token: 0x060012A5 RID: 4773 RVA: 0x000190B0 File Offset: 0x000172B0
			public float limitYMultiplier
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700041E RID: 1054
			// (get) Token: 0x060012A7 RID: 4775 RVA: 0x000190D0 File Offset: 0x000172D0
			// (set) Token: 0x060012A6 RID: 4774 RVA: 0x000190C0 File Offset: 0x000172C0
			public ParticleSystem.MinMaxCurve limitZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LimitVelocityOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700041F RID: 1055
			// (get) Token: 0x060012A8 RID: 4776 RVA: 0x000190FC File Offset: 0x000172FC
			// (set) Token: 0x060012A9 RID: 4777 RVA: 0x0001911C File Offset: 0x0001731C
			public float limitZMultiplier
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000420 RID: 1056
			// (get) Token: 0x060012AB RID: 4779 RVA: 0x0001913C File Offset: 0x0001733C
			// (set) Token: 0x060012AA RID: 4778 RVA: 0x0001912C File Offset: 0x0001732C
			public ParticleSystem.MinMaxCurve limit
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LimitVelocityOverLifetimeModule.GetMagnitude(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetMagnitude(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000421 RID: 1057
			// (get) Token: 0x060012AC RID: 4780 RVA: 0x00019168 File Offset: 0x00017368
			// (set) Token: 0x060012AD RID: 4781 RVA: 0x00019188 File Offset: 0x00017388
			public float limitMultiplier
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetMagnitudeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetMagnitudeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000422 RID: 1058
			// (get) Token: 0x060012AE RID: 4782 RVA: 0x00019198 File Offset: 0x00017398
			// (set) Token: 0x060012AF RID: 4783 RVA: 0x000191B8 File Offset: 0x000173B8
			public float dampen
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetDampen(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetDampen(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000423 RID: 1059
			// (get) Token: 0x060012B0 RID: 4784 RVA: 0x000191C8 File Offset: 0x000173C8
			// (set) Token: 0x060012B1 RID: 4785 RVA: 0x000191E8 File Offset: 0x000173E8
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.LimitVelocityOverLifetimeModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000424 RID: 1060
			// (get) Token: 0x060012B2 RID: 4786 RVA: 0x000191F8 File Offset: 0x000173F8
			// (set) Token: 0x060012B3 RID: 4787 RVA: 0x00019224 File Offset: 0x00017424
			public ParticleSystemSimulationSpace space
			{
				get
				{
					return (!ParticleSystem.LimitVelocityOverLifetimeModule.GetWorldSpace(this.m_ParticleSystem)) ? ParticleSystemSimulationSpace.Local : ParticleSystemSimulationSpace.World;
				}
				set
				{
					ParticleSystem.LimitVelocityOverLifetimeModule.SetWorldSpace(this.m_ParticleSystem, value == ParticleSystemSimulationSpace.World);
				}
			}

			// Token: 0x060012B4 RID: 4788
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060012B5 RID: 4789
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060012B6 RID: 4790
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012B7 RID: 4791
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012B8 RID: 4792
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x060012B9 RID: 4793
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x060012BA RID: 4794
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012BB RID: 4795
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012BC RID: 4796
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x060012BD RID: 4797
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x060012BE RID: 4798
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012BF RID: 4799
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012C0 RID: 4800
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x060012C1 RID: 4801
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x060012C2 RID: 4802
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMagnitude(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012C3 RID: 4803
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetMagnitude(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012C4 RID: 4804
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMagnitudeMultiplier(ParticleSystem system, float value);

			// Token: 0x060012C5 RID: 4805
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMagnitudeMultiplier(ParticleSystem system);

			// Token: 0x060012C6 RID: 4806
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDampen(ParticleSystem system, float value);

			// Token: 0x060012C7 RID: 4807
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetDampen(ParticleSystem system);

			// Token: 0x060012C8 RID: 4808
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x060012C9 RID: 4809
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x060012CA RID: 4810
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWorldSpace(ParticleSystem system, bool value);

			// Token: 0x060012CB RID: 4811
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetWorldSpace(ParticleSystem system);

			// Token: 0x0400031B RID: 795
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000118 RID: 280
		public struct InheritVelocityModule
		{
			// Token: 0x060012CC RID: 4812 RVA: 0x00019238 File Offset: 0x00017438
			internal InheritVelocityModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000425 RID: 1061
			// (get) Token: 0x060012CE RID: 4814 RVA: 0x00019254 File Offset: 0x00017454
			// (set) Token: 0x060012CD RID: 4813 RVA: 0x00019244 File Offset: 0x00017444
			public bool enabled
			{
				get
				{
					return ParticleSystem.InheritVelocityModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.InheritVelocityModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000426 RID: 1062
			// (get) Token: 0x060012CF RID: 4815 RVA: 0x00019274 File Offset: 0x00017474
			// (set) Token: 0x060012D0 RID: 4816 RVA: 0x00019294 File Offset: 0x00017494
			public ParticleSystemInheritVelocityMode mode
			{
				get
				{
					return (ParticleSystemInheritVelocityMode)ParticleSystem.InheritVelocityModule.GetMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.InheritVelocityModule.SetMode(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x17000427 RID: 1063
			// (get) Token: 0x060012D2 RID: 4818 RVA: 0x000192B4 File Offset: 0x000174B4
			// (set) Token: 0x060012D1 RID: 4817 RVA: 0x000192A4 File Offset: 0x000174A4
			public ParticleSystem.MinMaxCurve curve
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.InheritVelocityModule.GetCurve(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.InheritVelocityModule.SetCurve(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000428 RID: 1064
			// (get) Token: 0x060012D3 RID: 4819 RVA: 0x000192E0 File Offset: 0x000174E0
			// (set) Token: 0x060012D4 RID: 4820 RVA: 0x00019300 File Offset: 0x00017500
			public float curveMultiplier
			{
				get
				{
					return ParticleSystem.InheritVelocityModule.GetCurveMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.InheritVelocityModule.SetCurveMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060012D5 RID: 4821
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060012D6 RID: 4822
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060012D7 RID: 4823
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMode(ParticleSystem system, int value);

			// Token: 0x060012D8 RID: 4824
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMode(ParticleSystem system);

			// Token: 0x060012D9 RID: 4825
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCurve(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012DA RID: 4826
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetCurve(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012DB RID: 4827
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCurveMultiplier(ParticleSystem system, float value);

			// Token: 0x060012DC RID: 4828
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetCurveMultiplier(ParticleSystem system);

			// Token: 0x0400031C RID: 796
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000119 RID: 281
		public struct ForceOverLifetimeModule
		{
			// Token: 0x060012DD RID: 4829 RVA: 0x00019310 File Offset: 0x00017510
			internal ForceOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000429 RID: 1065
			// (get) Token: 0x060012DF RID: 4831 RVA: 0x0001932C File Offset: 0x0001752C
			// (set) Token: 0x060012DE RID: 4830 RVA: 0x0001931C File Offset: 0x0001751C
			public bool enabled
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700042A RID: 1066
			// (get) Token: 0x060012E1 RID: 4833 RVA: 0x0001935C File Offset: 0x0001755C
			// (set) Token: 0x060012E0 RID: 4832 RVA: 0x0001934C File Offset: 0x0001754C
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.ForceOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700042B RID: 1067
			// (get) Token: 0x060012E3 RID: 4835 RVA: 0x00019398 File Offset: 0x00017598
			// (set) Token: 0x060012E2 RID: 4834 RVA: 0x00019388 File Offset: 0x00017588
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.ForceOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700042C RID: 1068
			// (get) Token: 0x060012E5 RID: 4837 RVA: 0x000193D4 File Offset: 0x000175D4
			// (set) Token: 0x060012E4 RID: 4836 RVA: 0x000193C4 File Offset: 0x000175C4
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.ForceOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700042D RID: 1069
			// (get) Token: 0x060012E6 RID: 4838 RVA: 0x00019400 File Offset: 0x00017600
			// (set) Token: 0x060012E7 RID: 4839 RVA: 0x00019420 File Offset: 0x00017620
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700042E RID: 1070
			// (get) Token: 0x060012E8 RID: 4840 RVA: 0x00019430 File Offset: 0x00017630
			// (set) Token: 0x060012E9 RID: 4841 RVA: 0x00019450 File Offset: 0x00017650
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700042F RID: 1071
			// (get) Token: 0x060012EA RID: 4842 RVA: 0x00019460 File Offset: 0x00017660
			// (set) Token: 0x060012EB RID: 4843 RVA: 0x00019480 File Offset: 0x00017680
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000430 RID: 1072
			// (get) Token: 0x060012EC RID: 4844 RVA: 0x00019490 File Offset: 0x00017690
			// (set) Token: 0x060012ED RID: 4845 RVA: 0x000194BC File Offset: 0x000176BC
			public ParticleSystemSimulationSpace space
			{
				get
				{
					return (!ParticleSystem.ForceOverLifetimeModule.GetWorldSpace(this.m_ParticleSystem)) ? ParticleSystemSimulationSpace.Local : ParticleSystemSimulationSpace.World;
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetWorldSpace(this.m_ParticleSystem, value == ParticleSystemSimulationSpace.World);
				}
			}

			// Token: 0x17000431 RID: 1073
			// (get) Token: 0x060012EF RID: 4847 RVA: 0x000194E0 File Offset: 0x000176E0
			// (set) Token: 0x060012EE RID: 4846 RVA: 0x000194D0 File Offset: 0x000176D0
			public bool randomized
			{
				get
				{
					return ParticleSystem.ForceOverLifetimeModule.GetRandomized(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ForceOverLifetimeModule.SetRandomized(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060012F0 RID: 4848
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060012F1 RID: 4849
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060012F2 RID: 4850
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012F3 RID: 4851
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012F4 RID: 4852
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012F5 RID: 4853
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012F6 RID: 4854
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012F7 RID: 4855
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060012F8 RID: 4856
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x060012F9 RID: 4857
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x060012FA RID: 4858
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x060012FB RID: 4859
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x060012FC RID: 4860
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x060012FD RID: 4861
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x060012FE RID: 4862
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWorldSpace(ParticleSystem system, bool value);

			// Token: 0x060012FF RID: 4863
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetWorldSpace(ParticleSystem system);

			// Token: 0x06001300 RID: 4864
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRandomized(ParticleSystem system, bool value);

			// Token: 0x06001301 RID: 4865
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetRandomized(ParticleSystem system);

			// Token: 0x0400031D RID: 797
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200011A RID: 282
		public struct ColorOverLifetimeModule
		{
			// Token: 0x06001302 RID: 4866 RVA: 0x00019500 File Offset: 0x00017700
			internal ColorOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000432 RID: 1074
			// (get) Token: 0x06001304 RID: 4868 RVA: 0x0001951C File Offset: 0x0001771C
			// (set) Token: 0x06001303 RID: 4867 RVA: 0x0001950C File Offset: 0x0001770C
			public bool enabled
			{
				get
				{
					return ParticleSystem.ColorOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ColorOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000433 RID: 1075
			// (get) Token: 0x06001306 RID: 4870 RVA: 0x0001954C File Offset: 0x0001774C
			// (set) Token: 0x06001305 RID: 4869 RVA: 0x0001953C File Offset: 0x0001773C
			public ParticleSystem.MinMaxGradient color
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.ColorOverLifetimeModule.GetColor(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ColorOverLifetimeModule.SetColor(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x06001307 RID: 4871
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06001308 RID: 4872
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06001309 RID: 4873
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0600130A RID: 4874
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0400031E RID: 798
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200011B RID: 283
		public struct ColorBySpeedModule
		{
			// Token: 0x0600130B RID: 4875 RVA: 0x00019578 File Offset: 0x00017778
			internal ColorBySpeedModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000434 RID: 1076
			// (get) Token: 0x0600130D RID: 4877 RVA: 0x00019594 File Offset: 0x00017794
			// (set) Token: 0x0600130C RID: 4876 RVA: 0x00019584 File Offset: 0x00017784
			public bool enabled
			{
				get
				{
					return ParticleSystem.ColorBySpeedModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ColorBySpeedModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000435 RID: 1077
			// (get) Token: 0x0600130F RID: 4879 RVA: 0x000195C4 File Offset: 0x000177C4
			// (set) Token: 0x0600130E RID: 4878 RVA: 0x000195B4 File Offset: 0x000177B4
			public ParticleSystem.MinMaxGradient color
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.ColorBySpeedModule.GetColor(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.ColorBySpeedModule.SetColor(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000436 RID: 1078
			// (get) Token: 0x06001311 RID: 4881 RVA: 0x00019600 File Offset: 0x00017800
			// (set) Token: 0x06001310 RID: 4880 RVA: 0x000195F0 File Offset: 0x000177F0
			public Vector2 range
			{
				get
				{
					return ParticleSystem.ColorBySpeedModule.GetRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ColorBySpeedModule.SetRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06001312 RID: 4882
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06001313 RID: 4883
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06001314 RID: 4884
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x06001315 RID: 4885
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetColor(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x06001316 RID: 4886 RVA: 0x00019620 File Offset: 0x00017820
			private static void SetRange(ParticleSystem system, Vector2 value)
			{
				ParticleSystem.ColorBySpeedModule.INTERNAL_CALL_SetRange(system, ref value);
			}

			// Token: 0x06001317 RID: 4887
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetRange(ParticleSystem system, ref Vector2 value);

			// Token: 0x06001318 RID: 4888 RVA: 0x0001962C File Offset: 0x0001782C
			private static Vector2 GetRange(ParticleSystem system)
			{
				Vector2 result;
				ParticleSystem.ColorBySpeedModule.INTERNAL_CALL_GetRange(system, out result);
				return result;
			}

			// Token: 0x06001319 RID: 4889
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetRange(ParticleSystem system, out Vector2 value);

			// Token: 0x0400031F RID: 799
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200011C RID: 284
		public struct SizeOverLifetimeModule
		{
			// Token: 0x0600131A RID: 4890 RVA: 0x0001964C File Offset: 0x0001784C
			internal SizeOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000437 RID: 1079
			// (get) Token: 0x0600131C RID: 4892 RVA: 0x00019668 File Offset: 0x00017868
			// (set) Token: 0x0600131B RID: 4891 RVA: 0x00019658 File Offset: 0x00017858
			public bool enabled
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000438 RID: 1080
			// (get) Token: 0x0600131E RID: 4894 RVA: 0x00019698 File Offset: 0x00017898
			// (set) Token: 0x0600131D RID: 4893 RVA: 0x00019688 File Offset: 0x00017888
			public ParticleSystem.MinMaxCurve size
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000439 RID: 1081
			// (get) Token: 0x0600131F RID: 4895 RVA: 0x000196C4 File Offset: 0x000178C4
			// (set) Token: 0x06001320 RID: 4896 RVA: 0x000196E4 File Offset: 0x000178E4
			public float sizeMultiplier
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700043A RID: 1082
			// (get) Token: 0x06001322 RID: 4898 RVA: 0x00019704 File Offset: 0x00017904
			// (set) Token: 0x06001321 RID: 4897 RVA: 0x000196F4 File Offset: 0x000178F4
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700043B RID: 1083
			// (get) Token: 0x06001323 RID: 4899 RVA: 0x00019730 File Offset: 0x00017930
			// (set) Token: 0x06001324 RID: 4900 RVA: 0x00019750 File Offset: 0x00017950
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700043C RID: 1084
			// (get) Token: 0x06001326 RID: 4902 RVA: 0x00019770 File Offset: 0x00017970
			// (set) Token: 0x06001325 RID: 4901 RVA: 0x00019760 File Offset: 0x00017960
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700043D RID: 1085
			// (get) Token: 0x06001327 RID: 4903 RVA: 0x0001979C File Offset: 0x0001799C
			// (set) Token: 0x06001328 RID: 4904 RVA: 0x000197BC File Offset: 0x000179BC
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700043E RID: 1086
			// (get) Token: 0x0600132A RID: 4906 RVA: 0x000197DC File Offset: 0x000179DC
			// (set) Token: 0x06001329 RID: 4905 RVA: 0x000197CC File Offset: 0x000179CC
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700043F RID: 1087
			// (get) Token: 0x0600132B RID: 4907 RVA: 0x00019808 File Offset: 0x00017A08
			// (set) Token: 0x0600132C RID: 4908 RVA: 0x00019828 File Offset: 0x00017A28
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000440 RID: 1088
			// (get) Token: 0x0600132D RID: 4909 RVA: 0x00019838 File Offset: 0x00017A38
			// (set) Token: 0x0600132E RID: 4910 RVA: 0x00019858 File Offset: 0x00017A58
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.SizeOverLifetimeModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeOverLifetimeModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600132F RID: 4911
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06001330 RID: 4912
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06001331 RID: 4913
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001332 RID: 4914
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001333 RID: 4915
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001334 RID: 4916
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001335 RID: 4917
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001336 RID: 4918
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001337 RID: 4919
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x06001338 RID: 4920
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x06001339 RID: 4921
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x0600133A RID: 4922
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x0600133B RID: 4923
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x0600133C RID: 4924
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x0600133D RID: 4925
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x0600133E RID: 4926
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x04000320 RID: 800
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200011D RID: 285
		public struct SizeBySpeedModule
		{
			// Token: 0x0600133F RID: 4927 RVA: 0x00019868 File Offset: 0x00017A68
			internal SizeBySpeedModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000441 RID: 1089
			// (get) Token: 0x06001341 RID: 4929 RVA: 0x00019884 File Offset: 0x00017A84
			// (set) Token: 0x06001340 RID: 4928 RVA: 0x00019874 File Offset: 0x00017A74
			public bool enabled
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000442 RID: 1090
			// (get) Token: 0x06001343 RID: 4931 RVA: 0x000198B4 File Offset: 0x00017AB4
			// (set) Token: 0x06001342 RID: 4930 RVA: 0x000198A4 File Offset: 0x00017AA4
			public ParticleSystem.MinMaxCurve size
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeBySpeedModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000443 RID: 1091
			// (get) Token: 0x06001344 RID: 4932 RVA: 0x000198E0 File Offset: 0x00017AE0
			// (set) Token: 0x06001345 RID: 4933 RVA: 0x00019900 File Offset: 0x00017B00
			public float sizeMultiplier
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000444 RID: 1092
			// (get) Token: 0x06001347 RID: 4935 RVA: 0x00019920 File Offset: 0x00017B20
			// (set) Token: 0x06001346 RID: 4934 RVA: 0x00019910 File Offset: 0x00017B10
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeBySpeedModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000445 RID: 1093
			// (get) Token: 0x06001348 RID: 4936 RVA: 0x0001994C File Offset: 0x00017B4C
			// (set) Token: 0x06001349 RID: 4937 RVA: 0x0001996C File Offset: 0x00017B6C
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000446 RID: 1094
			// (get) Token: 0x0600134B RID: 4939 RVA: 0x0001998C File Offset: 0x00017B8C
			// (set) Token: 0x0600134A RID: 4938 RVA: 0x0001997C File Offset: 0x00017B7C
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeBySpeedModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000447 RID: 1095
			// (get) Token: 0x0600134C RID: 4940 RVA: 0x000199B8 File Offset: 0x00017BB8
			// (set) Token: 0x0600134D RID: 4941 RVA: 0x000199D8 File Offset: 0x00017BD8
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000448 RID: 1096
			// (get) Token: 0x0600134F RID: 4943 RVA: 0x000199F8 File Offset: 0x00017BF8
			// (set) Token: 0x0600134E RID: 4942 RVA: 0x000199E8 File Offset: 0x00017BE8
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.SizeBySpeedModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000449 RID: 1097
			// (get) Token: 0x06001350 RID: 4944 RVA: 0x00019A24 File Offset: 0x00017C24
			// (set) Token: 0x06001351 RID: 4945 RVA: 0x00019A44 File Offset: 0x00017C44
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700044A RID: 1098
			// (get) Token: 0x06001352 RID: 4946 RVA: 0x00019A54 File Offset: 0x00017C54
			// (set) Token: 0x06001353 RID: 4947 RVA: 0x00019A74 File Offset: 0x00017C74
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700044B RID: 1099
			// (get) Token: 0x06001355 RID: 4949 RVA: 0x00019A94 File Offset: 0x00017C94
			// (set) Token: 0x06001354 RID: 4948 RVA: 0x00019A84 File Offset: 0x00017C84
			public Vector2 range
			{
				get
				{
					return ParticleSystem.SizeBySpeedModule.GetRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SizeBySpeedModule.SetRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06001356 RID: 4950
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06001357 RID: 4951
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06001358 RID: 4952
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001359 RID: 4953
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600135A RID: 4954
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600135B RID: 4955
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600135C RID: 4956
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600135D RID: 4957
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600135E RID: 4958
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x0600135F RID: 4959
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x06001360 RID: 4960
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x06001361 RID: 4961
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x06001362 RID: 4962
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x06001363 RID: 4963
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x06001364 RID: 4964
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x06001365 RID: 4965
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x06001366 RID: 4966 RVA: 0x00019AB4 File Offset: 0x00017CB4
			private static void SetRange(ParticleSystem system, Vector2 value)
			{
				ParticleSystem.SizeBySpeedModule.INTERNAL_CALL_SetRange(system, ref value);
			}

			// Token: 0x06001367 RID: 4967
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetRange(ParticleSystem system, ref Vector2 value);

			// Token: 0x06001368 RID: 4968 RVA: 0x00019AC0 File Offset: 0x00017CC0
			private static Vector2 GetRange(ParticleSystem system)
			{
				Vector2 result;
				ParticleSystem.SizeBySpeedModule.INTERNAL_CALL_GetRange(system, out result);
				return result;
			}

			// Token: 0x06001369 RID: 4969
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetRange(ParticleSystem system, out Vector2 value);

			// Token: 0x04000321 RID: 801
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200011E RID: 286
		public struct RotationOverLifetimeModule
		{
			// Token: 0x0600136A RID: 4970 RVA: 0x00019AE0 File Offset: 0x00017CE0
			internal RotationOverLifetimeModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700044C RID: 1100
			// (get) Token: 0x0600136C RID: 4972 RVA: 0x00019AFC File Offset: 0x00017CFC
			// (set) Token: 0x0600136B RID: 4971 RVA: 0x00019AEC File Offset: 0x00017CEC
			public bool enabled
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700044D RID: 1101
			// (get) Token: 0x0600136E RID: 4974 RVA: 0x00019B2C File Offset: 0x00017D2C
			// (set) Token: 0x0600136D RID: 4973 RVA: 0x00019B1C File Offset: 0x00017D1C
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationOverLifetimeModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700044E RID: 1102
			// (get) Token: 0x0600136F RID: 4975 RVA: 0x00019B58 File Offset: 0x00017D58
			// (set) Token: 0x06001370 RID: 4976 RVA: 0x00019B78 File Offset: 0x00017D78
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700044F RID: 1103
			// (get) Token: 0x06001372 RID: 4978 RVA: 0x00019B98 File Offset: 0x00017D98
			// (set) Token: 0x06001371 RID: 4977 RVA: 0x00019B88 File Offset: 0x00017D88
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationOverLifetimeModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000450 RID: 1104
			// (get) Token: 0x06001373 RID: 4979 RVA: 0x00019BC4 File Offset: 0x00017DC4
			// (set) Token: 0x06001374 RID: 4980 RVA: 0x00019BE4 File Offset: 0x00017DE4
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000451 RID: 1105
			// (get) Token: 0x06001376 RID: 4982 RVA: 0x00019C04 File Offset: 0x00017E04
			// (set) Token: 0x06001375 RID: 4981 RVA: 0x00019BF4 File Offset: 0x00017DF4
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationOverLifetimeModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000452 RID: 1106
			// (get) Token: 0x06001377 RID: 4983 RVA: 0x00019C30 File Offset: 0x00017E30
			// (set) Token: 0x06001378 RID: 4984 RVA: 0x00019C50 File Offset: 0x00017E50
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000453 RID: 1107
			// (get) Token: 0x06001379 RID: 4985 RVA: 0x00019C60 File Offset: 0x00017E60
			// (set) Token: 0x0600137A RID: 4986 RVA: 0x00019C80 File Offset: 0x00017E80
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.RotationOverLifetimeModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationOverLifetimeModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600137B RID: 4987
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600137C RID: 4988
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600137D RID: 4989
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600137E RID: 4990
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600137F RID: 4991
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001380 RID: 4992
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001381 RID: 4993
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001382 RID: 4994
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001383 RID: 4995
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x06001384 RID: 4996
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x06001385 RID: 4997
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x06001386 RID: 4998
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x06001387 RID: 4999
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x06001388 RID: 5000
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x06001389 RID: 5001
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x0600138A RID: 5002
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x04000322 RID: 802
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x0200011F RID: 287
		public struct RotationBySpeedModule
		{
			// Token: 0x0600138B RID: 5003 RVA: 0x00019C90 File Offset: 0x00017E90
			internal RotationBySpeedModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000454 RID: 1108
			// (get) Token: 0x0600138D RID: 5005 RVA: 0x00019CAC File Offset: 0x00017EAC
			// (set) Token: 0x0600138C RID: 5004 RVA: 0x00019C9C File Offset: 0x00017E9C
			public bool enabled
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000455 RID: 1109
			// (get) Token: 0x0600138F RID: 5007 RVA: 0x00019CDC File Offset: 0x00017EDC
			// (set) Token: 0x0600138E RID: 5006 RVA: 0x00019CCC File Offset: 0x00017ECC
			public ParticleSystem.MinMaxCurve x
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationBySpeedModule.GetX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000456 RID: 1110
			// (get) Token: 0x06001390 RID: 5008 RVA: 0x00019D08 File Offset: 0x00017F08
			// (set) Token: 0x06001391 RID: 5009 RVA: 0x00019D28 File Offset: 0x00017F28
			public float xMultiplier
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000457 RID: 1111
			// (get) Token: 0x06001393 RID: 5011 RVA: 0x00019D48 File Offset: 0x00017F48
			// (set) Token: 0x06001392 RID: 5010 RVA: 0x00019D38 File Offset: 0x00017F38
			public ParticleSystem.MinMaxCurve y
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationBySpeedModule.GetY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000458 RID: 1112
			// (get) Token: 0x06001394 RID: 5012 RVA: 0x00019D74 File Offset: 0x00017F74
			// (set) Token: 0x06001395 RID: 5013 RVA: 0x00019D94 File Offset: 0x00017F94
			public float yMultiplier
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000459 RID: 1113
			// (get) Token: 0x06001397 RID: 5015 RVA: 0x00019DB4 File Offset: 0x00017FB4
			// (set) Token: 0x06001396 RID: 5014 RVA: 0x00019DA4 File Offset: 0x00017FA4
			public ParticleSystem.MinMaxCurve z
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.RotationBySpeedModule.GetZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700045A RID: 1114
			// (get) Token: 0x06001398 RID: 5016 RVA: 0x00019DE0 File Offset: 0x00017FE0
			// (set) Token: 0x06001399 RID: 5017 RVA: 0x00019E00 File Offset: 0x00018000
			public float zMultiplier
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700045B RID: 1115
			// (get) Token: 0x0600139A RID: 5018 RVA: 0x00019E10 File Offset: 0x00018010
			// (set) Token: 0x0600139B RID: 5019 RVA: 0x00019E30 File Offset: 0x00018030
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700045C RID: 1116
			// (get) Token: 0x0600139D RID: 5021 RVA: 0x00019E50 File Offset: 0x00018050
			// (set) Token: 0x0600139C RID: 5020 RVA: 0x00019E40 File Offset: 0x00018040
			public Vector2 range
			{
				get
				{
					return ParticleSystem.RotationBySpeedModule.GetRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.RotationBySpeedModule.SetRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600139E RID: 5022
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600139F RID: 5023
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060013A0 RID: 5024
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013A1 RID: 5025
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013A2 RID: 5026
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013A3 RID: 5027
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013A4 RID: 5028
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013A5 RID: 5029
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013A6 RID: 5030
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetXMultiplier(ParticleSystem system, float value);

			// Token: 0x060013A7 RID: 5031
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetXMultiplier(ParticleSystem system);

			// Token: 0x060013A8 RID: 5032
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetYMultiplier(ParticleSystem system, float value);

			// Token: 0x060013A9 RID: 5033
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetYMultiplier(ParticleSystem system);

			// Token: 0x060013AA RID: 5034
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetZMultiplier(ParticleSystem system, float value);

			// Token: 0x060013AB RID: 5035
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetZMultiplier(ParticleSystem system);

			// Token: 0x060013AC RID: 5036
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x060013AD RID: 5037
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x060013AE RID: 5038 RVA: 0x00019E70 File Offset: 0x00018070
			private static void SetRange(ParticleSystem system, Vector2 value)
			{
				ParticleSystem.RotationBySpeedModule.INTERNAL_CALL_SetRange(system, ref value);
			}

			// Token: 0x060013AF RID: 5039
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_SetRange(ParticleSystem system, ref Vector2 value);

			// Token: 0x060013B0 RID: 5040 RVA: 0x00019E7C File Offset: 0x0001807C
			private static Vector2 GetRange(ParticleSystem system)
			{
				Vector2 result;
				ParticleSystem.RotationBySpeedModule.INTERNAL_CALL_GetRange(system, out result);
				return result;
			}

			// Token: 0x060013B1 RID: 5041
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetRange(ParticleSystem system, out Vector2 value);

			// Token: 0x04000323 RID: 803
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000120 RID: 288
		public struct ExternalForcesModule
		{
			// Token: 0x060013B2 RID: 5042 RVA: 0x00019E9C File Offset: 0x0001809C
			internal ExternalForcesModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700045D RID: 1117
			// (get) Token: 0x060013B4 RID: 5044 RVA: 0x00019EB8 File Offset: 0x000180B8
			// (set) Token: 0x060013B3 RID: 5043 RVA: 0x00019EA8 File Offset: 0x000180A8
			public bool enabled
			{
				get
				{
					return ParticleSystem.ExternalForcesModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ExternalForcesModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700045E RID: 1118
			// (get) Token: 0x060013B5 RID: 5045 RVA: 0x00019ED8 File Offset: 0x000180D8
			// (set) Token: 0x060013B6 RID: 5046 RVA: 0x00019EF8 File Offset: 0x000180F8
			public float multiplier
			{
				get
				{
					return ParticleSystem.ExternalForcesModule.GetMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.ExternalForcesModule.SetMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060013B7 RID: 5047
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060013B8 RID: 5048
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060013B9 RID: 5049
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMultiplier(ParticleSystem system, float value);

			// Token: 0x060013BA RID: 5050
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMultiplier(ParticleSystem system);

			// Token: 0x04000324 RID: 804
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000121 RID: 289
		public struct NoiseModule
		{
			// Token: 0x060013BB RID: 5051 RVA: 0x00019F08 File Offset: 0x00018108
			internal NoiseModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700045F RID: 1119
			// (get) Token: 0x060013BD RID: 5053 RVA: 0x00019F24 File Offset: 0x00018124
			// (set) Token: 0x060013BC RID: 5052 RVA: 0x00019F14 File Offset: 0x00018114
			public bool enabled
			{
				get
				{
					return ParticleSystem.NoiseModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000460 RID: 1120
			// (get) Token: 0x060013BE RID: 5054 RVA: 0x00019F44 File Offset: 0x00018144
			// (set) Token: 0x060013BF RID: 5055 RVA: 0x00019F64 File Offset: 0x00018164
			public bool separateAxes
			{
				get
				{
					return ParticleSystem.NoiseModule.GetSeparateAxes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetSeparateAxes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000461 RID: 1121
			// (get) Token: 0x060013C1 RID: 5057 RVA: 0x00019F84 File Offset: 0x00018184
			// (set) Token: 0x060013C0 RID: 5056 RVA: 0x00019F74 File Offset: 0x00018174
			public ParticleSystem.MinMaxCurve strength
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetStrengthX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000462 RID: 1122
			// (get) Token: 0x060013C2 RID: 5058 RVA: 0x00019FB0 File Offset: 0x000181B0
			// (set) Token: 0x060013C3 RID: 5059 RVA: 0x00019FD0 File Offset: 0x000181D0
			public float strengthMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetStrengthXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000463 RID: 1123
			// (get) Token: 0x060013C5 RID: 5061 RVA: 0x00019FF0 File Offset: 0x000181F0
			// (set) Token: 0x060013C4 RID: 5060 RVA: 0x00019FE0 File Offset: 0x000181E0
			public ParticleSystem.MinMaxCurve strengthX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetStrengthX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000464 RID: 1124
			// (get) Token: 0x060013C6 RID: 5062 RVA: 0x0001A01C File Offset: 0x0001821C
			// (set) Token: 0x060013C7 RID: 5063 RVA: 0x0001A03C File Offset: 0x0001823C
			public float strengthXMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetStrengthXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000465 RID: 1125
			// (get) Token: 0x060013C9 RID: 5065 RVA: 0x0001A05C File Offset: 0x0001825C
			// (set) Token: 0x060013C8 RID: 5064 RVA: 0x0001A04C File Offset: 0x0001824C
			public ParticleSystem.MinMaxCurve strengthY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetStrengthY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000466 RID: 1126
			// (get) Token: 0x060013CA RID: 5066 RVA: 0x0001A088 File Offset: 0x00018288
			// (set) Token: 0x060013CB RID: 5067 RVA: 0x0001A0A8 File Offset: 0x000182A8
			public float strengthYMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetStrengthYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000467 RID: 1127
			// (get) Token: 0x060013CD RID: 5069 RVA: 0x0001A0C8 File Offset: 0x000182C8
			// (set) Token: 0x060013CC RID: 5068 RVA: 0x0001A0B8 File Offset: 0x000182B8
			public ParticleSystem.MinMaxCurve strengthZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetStrengthZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000468 RID: 1128
			// (get) Token: 0x060013CE RID: 5070 RVA: 0x0001A0F4 File Offset: 0x000182F4
			// (set) Token: 0x060013CF RID: 5071 RVA: 0x0001A114 File Offset: 0x00018314
			public float strengthZMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetStrengthZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetStrengthZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000469 RID: 1129
			// (get) Token: 0x060013D0 RID: 5072 RVA: 0x0001A124 File Offset: 0x00018324
			// (set) Token: 0x060013D1 RID: 5073 RVA: 0x0001A144 File Offset: 0x00018344
			public float frequency
			{
				get
				{
					return ParticleSystem.NoiseModule.GetFrequency(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetFrequency(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700046A RID: 1130
			// (get) Token: 0x060013D2 RID: 5074 RVA: 0x0001A154 File Offset: 0x00018354
			// (set) Token: 0x060013D3 RID: 5075 RVA: 0x0001A174 File Offset: 0x00018374
			public bool damping
			{
				get
				{
					return ParticleSystem.NoiseModule.GetDamping(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetDamping(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700046B RID: 1131
			// (get) Token: 0x060013D4 RID: 5076 RVA: 0x0001A184 File Offset: 0x00018384
			// (set) Token: 0x060013D5 RID: 5077 RVA: 0x0001A1A4 File Offset: 0x000183A4
			public int octaveCount
			{
				get
				{
					return ParticleSystem.NoiseModule.GetOctaveCount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetOctaveCount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700046C RID: 1132
			// (get) Token: 0x060013D6 RID: 5078 RVA: 0x0001A1B4 File Offset: 0x000183B4
			// (set) Token: 0x060013D7 RID: 5079 RVA: 0x0001A1D4 File Offset: 0x000183D4
			public float octaveMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetOctaveMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetOctaveMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700046D RID: 1133
			// (get) Token: 0x060013D8 RID: 5080 RVA: 0x0001A1E4 File Offset: 0x000183E4
			// (set) Token: 0x060013D9 RID: 5081 RVA: 0x0001A204 File Offset: 0x00018404
			public float octaveScale
			{
				get
				{
					return ParticleSystem.NoiseModule.GetOctaveScale(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetOctaveScale(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700046E RID: 1134
			// (get) Token: 0x060013DA RID: 5082 RVA: 0x0001A214 File Offset: 0x00018414
			// (set) Token: 0x060013DB RID: 5083 RVA: 0x0001A234 File Offset: 0x00018434
			public ParticleSystemNoiseQuality quality
			{
				get
				{
					return (ParticleSystemNoiseQuality)ParticleSystem.NoiseModule.GetQuality(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetQuality(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x1700046F RID: 1135
			// (get) Token: 0x060013DD RID: 5085 RVA: 0x0001A254 File Offset: 0x00018454
			// (set) Token: 0x060013DC RID: 5084 RVA: 0x0001A244 File Offset: 0x00018444
			public ParticleSystem.MinMaxCurve scrollSpeed
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetScrollSpeed(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetScrollSpeed(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000470 RID: 1136
			// (get) Token: 0x060013DE RID: 5086 RVA: 0x0001A280 File Offset: 0x00018480
			// (set) Token: 0x060013DF RID: 5087 RVA: 0x0001A2A0 File Offset: 0x000184A0
			public float scrollSpeedMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetScrollSpeedMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetScrollSpeedMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000471 RID: 1137
			// (get) Token: 0x060013E0 RID: 5088 RVA: 0x0001A2B0 File Offset: 0x000184B0
			// (set) Token: 0x060013E1 RID: 5089 RVA: 0x0001A2D0 File Offset: 0x000184D0
			public bool remapEnabled
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000472 RID: 1138
			// (get) Token: 0x060013E3 RID: 5091 RVA: 0x0001A2F0 File Offset: 0x000184F0
			// (set) Token: 0x060013E2 RID: 5090 RVA: 0x0001A2E0 File Offset: 0x000184E0
			public ParticleSystem.MinMaxCurve remap
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetRemapX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000473 RID: 1139
			// (get) Token: 0x060013E4 RID: 5092 RVA: 0x0001A31C File Offset: 0x0001851C
			// (set) Token: 0x060013E5 RID: 5093 RVA: 0x0001A33C File Offset: 0x0001853C
			public float remapMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000474 RID: 1140
			// (get) Token: 0x060013E7 RID: 5095 RVA: 0x0001A35C File Offset: 0x0001855C
			// (set) Token: 0x060013E6 RID: 5094 RVA: 0x0001A34C File Offset: 0x0001854C
			public ParticleSystem.MinMaxCurve remapX
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetRemapX(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapX(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000475 RID: 1141
			// (get) Token: 0x060013E8 RID: 5096 RVA: 0x0001A388 File Offset: 0x00018588
			// (set) Token: 0x060013E9 RID: 5097 RVA: 0x0001A3A8 File Offset: 0x000185A8
			public float remapXMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapXMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapXMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000476 RID: 1142
			// (get) Token: 0x060013EB RID: 5099 RVA: 0x0001A3C8 File Offset: 0x000185C8
			// (set) Token: 0x060013EA RID: 5098 RVA: 0x0001A3B8 File Offset: 0x000185B8
			public ParticleSystem.MinMaxCurve remapY
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetRemapY(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapY(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000477 RID: 1143
			// (get) Token: 0x060013EC RID: 5100 RVA: 0x0001A3F4 File Offset: 0x000185F4
			// (set) Token: 0x060013ED RID: 5101 RVA: 0x0001A414 File Offset: 0x00018614
			public float remapYMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapYMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapYMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000478 RID: 1144
			// (get) Token: 0x060013EF RID: 5103 RVA: 0x0001A434 File Offset: 0x00018634
			// (set) Token: 0x060013EE RID: 5102 RVA: 0x0001A424 File Offset: 0x00018624
			public ParticleSystem.MinMaxCurve remapZ
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.NoiseModule.GetRemapZ(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapZ(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000479 RID: 1145
			// (get) Token: 0x060013F0 RID: 5104 RVA: 0x0001A460 File Offset: 0x00018660
			// (set) Token: 0x060013F1 RID: 5105 RVA: 0x0001A480 File Offset: 0x00018680
			public float remapZMultiplier
			{
				get
				{
					return ParticleSystem.NoiseModule.GetRemapZMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.NoiseModule.SetRemapZMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060013F2 RID: 5106
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060013F3 RID: 5107
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060013F4 RID: 5108
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSeparateAxes(ParticleSystem system, bool value);

			// Token: 0x060013F5 RID: 5109
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSeparateAxes(ParticleSystem system);

			// Token: 0x060013F6 RID: 5110
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013F7 RID: 5111
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStrengthX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013F8 RID: 5112
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013F9 RID: 5113
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStrengthY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013FA RID: 5114
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013FB RID: 5115
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStrengthZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060013FC RID: 5116
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthXMultiplier(ParticleSystem system, float value);

			// Token: 0x060013FD RID: 5117
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStrengthXMultiplier(ParticleSystem system);

			// Token: 0x060013FE RID: 5118
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthYMultiplier(ParticleSystem system, float value);

			// Token: 0x060013FF RID: 5119
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStrengthYMultiplier(ParticleSystem system);

			// Token: 0x06001400 RID: 5120
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStrengthZMultiplier(ParticleSystem system, float value);

			// Token: 0x06001401 RID: 5121
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStrengthZMultiplier(ParticleSystem system);

			// Token: 0x06001402 RID: 5122
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFrequency(ParticleSystem system, float value);

			// Token: 0x06001403 RID: 5123
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetFrequency(ParticleSystem system);

			// Token: 0x06001404 RID: 5124
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDamping(ParticleSystem system, bool value);

			// Token: 0x06001405 RID: 5125
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetDamping(ParticleSystem system);

			// Token: 0x06001406 RID: 5126
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOctaveCount(ParticleSystem system, int value);

			// Token: 0x06001407 RID: 5127
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetOctaveCount(ParticleSystem system);

			// Token: 0x06001408 RID: 5128
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOctaveMultiplier(ParticleSystem system, float value);

			// Token: 0x06001409 RID: 5129
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOctaveMultiplier(ParticleSystem system);

			// Token: 0x0600140A RID: 5130
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOctaveScale(ParticleSystem system, float value);

			// Token: 0x0600140B RID: 5131
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetOctaveScale(ParticleSystem system);

			// Token: 0x0600140C RID: 5132
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetQuality(ParticleSystem system, int value);

			// Token: 0x0600140D RID: 5133
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetQuality(ParticleSystem system);

			// Token: 0x0600140E RID: 5134
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetScrollSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600140F RID: 5135
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetScrollSpeed(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001410 RID: 5136
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetScrollSpeedMultiplier(ParticleSystem system, float value);

			// Token: 0x06001411 RID: 5137
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetScrollSpeedMultiplier(ParticleSystem system);

			// Token: 0x06001412 RID: 5138
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapEnabled(ParticleSystem system, bool value);

			// Token: 0x06001413 RID: 5139
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetRemapEnabled(ParticleSystem system);

			// Token: 0x06001414 RID: 5140
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001415 RID: 5141
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRemapX(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001416 RID: 5142
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001417 RID: 5143
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRemapY(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001418 RID: 5144
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001419 RID: 5145
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRemapZ(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600141A RID: 5146
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapXMultiplier(ParticleSystem system, float value);

			// Token: 0x0600141B RID: 5147
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRemapXMultiplier(ParticleSystem system);

			// Token: 0x0600141C RID: 5148
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapYMultiplier(ParticleSystem system, float value);

			// Token: 0x0600141D RID: 5149
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRemapYMultiplier(ParticleSystem system);

			// Token: 0x0600141E RID: 5150
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRemapZMultiplier(ParticleSystem system, float value);

			// Token: 0x0600141F RID: 5151
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRemapZMultiplier(ParticleSystem system);

			// Token: 0x04000325 RID: 805
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000122 RID: 290
		public struct CollisionModule
		{
			// Token: 0x06001420 RID: 5152 RVA: 0x0001A490 File Offset: 0x00018690
			internal CollisionModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700047A RID: 1146
			// (get) Token: 0x06001422 RID: 5154 RVA: 0x0001A4AC File Offset: 0x000186AC
			// (set) Token: 0x06001421 RID: 5153 RVA: 0x0001A49C File Offset: 0x0001869C
			public bool enabled
			{
				get
				{
					return ParticleSystem.CollisionModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700047B RID: 1147
			// (get) Token: 0x06001424 RID: 5156 RVA: 0x0001A4DC File Offset: 0x000186DC
			// (set) Token: 0x06001423 RID: 5155 RVA: 0x0001A4CC File Offset: 0x000186CC
			public ParticleSystemCollisionType type
			{
				get
				{
					return (ParticleSystemCollisionType)ParticleSystem.CollisionModule.GetType(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetType(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x1700047C RID: 1148
			// (get) Token: 0x06001426 RID: 5158 RVA: 0x0001A50C File Offset: 0x0001870C
			// (set) Token: 0x06001425 RID: 5157 RVA: 0x0001A4FC File Offset: 0x000186FC
			public ParticleSystemCollisionMode mode
			{
				get
				{
					return (ParticleSystemCollisionMode)ParticleSystem.CollisionModule.GetMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMode(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x1700047D RID: 1149
			// (get) Token: 0x06001428 RID: 5160 RVA: 0x0001A53C File Offset: 0x0001873C
			// (set) Token: 0x06001427 RID: 5159 RVA: 0x0001A52C File Offset: 0x0001872C
			public ParticleSystem.MinMaxCurve dampen
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.CollisionModule.GetDampen(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.CollisionModule.SetDampen(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x1700047E RID: 1150
			// (get) Token: 0x06001429 RID: 5161 RVA: 0x0001A568 File Offset: 0x00018768
			// (set) Token: 0x0600142A RID: 5162 RVA: 0x0001A588 File Offset: 0x00018788
			public float dampenMultiplier
			{
				get
				{
					return ParticleSystem.CollisionModule.GetDampenMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetDampenMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700047F RID: 1151
			// (get) Token: 0x0600142C RID: 5164 RVA: 0x0001A5A8 File Offset: 0x000187A8
			// (set) Token: 0x0600142B RID: 5163 RVA: 0x0001A598 File Offset: 0x00018798
			public ParticleSystem.MinMaxCurve bounce
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.CollisionModule.GetBounce(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.CollisionModule.SetBounce(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000480 RID: 1152
			// (get) Token: 0x0600142D RID: 5165 RVA: 0x0001A5D4 File Offset: 0x000187D4
			// (set) Token: 0x0600142E RID: 5166 RVA: 0x0001A5F4 File Offset: 0x000187F4
			public float bounceMultiplier
			{
				get
				{
					return ParticleSystem.CollisionModule.GetBounceMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetBounceMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000481 RID: 1153
			// (get) Token: 0x06001430 RID: 5168 RVA: 0x0001A614 File Offset: 0x00018814
			// (set) Token: 0x0600142F RID: 5167 RVA: 0x0001A604 File Offset: 0x00018804
			public ParticleSystem.MinMaxCurve lifetimeLoss
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.CollisionModule.GetLifetimeLoss(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.CollisionModule.SetLifetimeLoss(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x17000482 RID: 1154
			// (get) Token: 0x06001431 RID: 5169 RVA: 0x0001A640 File Offset: 0x00018840
			// (set) Token: 0x06001432 RID: 5170 RVA: 0x0001A660 File Offset: 0x00018860
			public float lifetimeLossMultiplier
			{
				get
				{
					return ParticleSystem.CollisionModule.GetLifetimeLossMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetLifetimeLossMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000483 RID: 1155
			// (get) Token: 0x06001433 RID: 5171 RVA: 0x0001A670 File Offset: 0x00018870
			// (set) Token: 0x06001434 RID: 5172 RVA: 0x0001A690 File Offset: 0x00018890
			public float minKillSpeed
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMinKillSpeed(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMinKillSpeed(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000484 RID: 1156
			// (get) Token: 0x06001435 RID: 5173 RVA: 0x0001A6A0 File Offset: 0x000188A0
			// (set) Token: 0x06001436 RID: 5174 RVA: 0x0001A6C0 File Offset: 0x000188C0
			public float maxKillSpeed
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMaxKillSpeed(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMaxKillSpeed(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000485 RID: 1157
			// (get) Token: 0x06001437 RID: 5175 RVA: 0x0001A6D0 File Offset: 0x000188D0
			// (set) Token: 0x06001438 RID: 5176 RVA: 0x0001A6F8 File Offset: 0x000188F8
			public LayerMask collidesWith
			{
				get
				{
					return ParticleSystem.CollisionModule.GetCollidesWith(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetCollidesWith(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000486 RID: 1158
			// (get) Token: 0x06001439 RID: 5177 RVA: 0x0001A70C File Offset: 0x0001890C
			// (set) Token: 0x0600143A RID: 5178 RVA: 0x0001A72C File Offset: 0x0001892C
			public bool enableDynamicColliders
			{
				get
				{
					return ParticleSystem.CollisionModule.GetEnableDynamicColliders(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetEnableDynamicColliders(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000487 RID: 1159
			// (get) Token: 0x0600143B RID: 5179 RVA: 0x0001A73C File Offset: 0x0001893C
			// (set) Token: 0x0600143C RID: 5180 RVA: 0x0001A75C File Offset: 0x0001895C
			public bool enableInteriorCollisions
			{
				get
				{
					return ParticleSystem.CollisionModule.GetEnableInteriorCollisions(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetEnableInteriorCollisions(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000488 RID: 1160
			// (get) Token: 0x0600143D RID: 5181 RVA: 0x0001A76C File Offset: 0x0001896C
			// (set) Token: 0x0600143E RID: 5182 RVA: 0x0001A78C File Offset: 0x0001898C
			public int maxCollisionShapes
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMaxCollisionShapes(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetMaxCollisionShapes(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000489 RID: 1161
			// (get) Token: 0x06001440 RID: 5184 RVA: 0x0001A7AC File Offset: 0x000189AC
			// (set) Token: 0x0600143F RID: 5183 RVA: 0x0001A79C File Offset: 0x0001899C
			public ParticleSystemCollisionQuality quality
			{
				get
				{
					return (ParticleSystemCollisionQuality)ParticleSystem.CollisionModule.GetQuality(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetQuality(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x1700048A RID: 1162
			// (get) Token: 0x06001441 RID: 5185 RVA: 0x0001A7CC File Offset: 0x000189CC
			// (set) Token: 0x06001442 RID: 5186 RVA: 0x0001A7EC File Offset: 0x000189EC
			public float voxelSize
			{
				get
				{
					return ParticleSystem.CollisionModule.GetVoxelSize(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetVoxelSize(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700048B RID: 1163
			// (get) Token: 0x06001443 RID: 5187 RVA: 0x0001A7FC File Offset: 0x000189FC
			// (set) Token: 0x06001444 RID: 5188 RVA: 0x0001A81C File Offset: 0x00018A1C
			public float radiusScale
			{
				get
				{
					return ParticleSystem.CollisionModule.GetRadiusScale(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetRadiusScale(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700048C RID: 1164
			// (get) Token: 0x06001445 RID: 5189 RVA: 0x0001A82C File Offset: 0x00018A2C
			// (set) Token: 0x06001446 RID: 5190 RVA: 0x0001A84C File Offset: 0x00018A4C
			public bool sendCollisionMessages
			{
				get
				{
					return ParticleSystem.CollisionModule.GetUsesCollisionMessages(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.CollisionModule.SetUsesCollisionMessages(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06001447 RID: 5191 RVA: 0x0001A85C File Offset: 0x00018A5C
			public void SetPlane(int index, Transform transform)
			{
				ParticleSystem.CollisionModule.SetPlane(this.m_ParticleSystem, index, transform);
			}

			// Token: 0x06001448 RID: 5192 RVA: 0x0001A86C File Offset: 0x00018A6C
			public Transform GetPlane(int index)
			{
				return ParticleSystem.CollisionModule.GetPlane(this.m_ParticleSystem, index);
			}

			// Token: 0x1700048D RID: 1165
			// (get) Token: 0x06001449 RID: 5193 RVA: 0x0001A890 File Offset: 0x00018A90
			public int maxPlaneCount
			{
				get
				{
					return ParticleSystem.CollisionModule.GetMaxPlaneCount(this.m_ParticleSystem);
				}
			}

			// Token: 0x0600144A RID: 5194
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600144B RID: 5195
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600144C RID: 5196
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetType(ParticleSystem system, int value);

			// Token: 0x0600144D RID: 5197
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetType(ParticleSystem system);

			// Token: 0x0600144E RID: 5198
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMode(ParticleSystem system, int value);

			// Token: 0x0600144F RID: 5199
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMode(ParticleSystem system);

			// Token: 0x06001450 RID: 5200
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDampen(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001451 RID: 5201
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetDampen(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001452 RID: 5202
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDampenMultiplier(ParticleSystem system, float value);

			// Token: 0x06001453 RID: 5203
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetDampenMultiplier(ParticleSystem system);

			// Token: 0x06001454 RID: 5204
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetBounce(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001455 RID: 5205
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetBounce(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001456 RID: 5206
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetBounceMultiplier(ParticleSystem system, float value);

			// Token: 0x06001457 RID: 5207
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetBounceMultiplier(ParticleSystem system);

			// Token: 0x06001458 RID: 5208
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLifetimeLoss(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001459 RID: 5209
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetLifetimeLoss(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600145A RID: 5210
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLifetimeLossMultiplier(ParticleSystem system, float value);

			// Token: 0x0600145B RID: 5211
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetLifetimeLossMultiplier(ParticleSystem system);

			// Token: 0x0600145C RID: 5212
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMinKillSpeed(ParticleSystem system, float value);

			// Token: 0x0600145D RID: 5213
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMinKillSpeed(ParticleSystem system);

			// Token: 0x0600145E RID: 5214
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMaxKillSpeed(ParticleSystem system, float value);

			// Token: 0x0600145F RID: 5215
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMaxKillSpeed(ParticleSystem system);

			// Token: 0x06001460 RID: 5216
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCollidesWith(ParticleSystem system, int value);

			// Token: 0x06001461 RID: 5217
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetCollidesWith(ParticleSystem system);

			// Token: 0x06001462 RID: 5218
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnableDynamicColliders(ParticleSystem system, bool value);

			// Token: 0x06001463 RID: 5219
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnableDynamicColliders(ParticleSystem system);

			// Token: 0x06001464 RID: 5220
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnableInteriorCollisions(ParticleSystem system, bool value);

			// Token: 0x06001465 RID: 5221
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnableInteriorCollisions(ParticleSystem system);

			// Token: 0x06001466 RID: 5222
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMaxCollisionShapes(ParticleSystem system, int value);

			// Token: 0x06001467 RID: 5223
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxCollisionShapes(ParticleSystem system);

			// Token: 0x06001468 RID: 5224
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetQuality(ParticleSystem system, int value);

			// Token: 0x06001469 RID: 5225
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetQuality(ParticleSystem system);

			// Token: 0x0600146A RID: 5226
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetVoxelSize(ParticleSystem system, float value);

			// Token: 0x0600146B RID: 5227
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetVoxelSize(ParticleSystem system);

			// Token: 0x0600146C RID: 5228
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadiusScale(ParticleSystem system, float value);

			// Token: 0x0600146D RID: 5229
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadiusScale(ParticleSystem system);

			// Token: 0x0600146E RID: 5230
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUsesCollisionMessages(ParticleSystem system, bool value);

			// Token: 0x0600146F RID: 5231
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUsesCollisionMessages(ParticleSystem system);

			// Token: 0x06001470 RID: 5232
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetPlane(ParticleSystem system, int index, Transform transform);

			// Token: 0x06001471 RID: 5233
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Transform GetPlane(ParticleSystem system, int index);

			// Token: 0x06001472 RID: 5234
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxPlaneCount(ParticleSystem system);

			// Token: 0x04000326 RID: 806
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000123 RID: 291
		public struct TriggerModule
		{
			// Token: 0x06001473 RID: 5235 RVA: 0x0001A8B0 File Offset: 0x00018AB0
			internal TriggerModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700048E RID: 1166
			// (get) Token: 0x06001475 RID: 5237 RVA: 0x0001A8CC File Offset: 0x00018ACC
			// (set) Token: 0x06001474 RID: 5236 RVA: 0x0001A8BC File Offset: 0x00018ABC
			public bool enabled
			{
				get
				{
					return ParticleSystem.TriggerModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700048F RID: 1167
			// (get) Token: 0x06001477 RID: 5239 RVA: 0x0001A8FC File Offset: 0x00018AFC
			// (set) Token: 0x06001476 RID: 5238 RVA: 0x0001A8EC File Offset: 0x00018AEC
			public ParticleSystemOverlapAction inside
			{
				get
				{
					return (ParticleSystemOverlapAction)ParticleSystem.TriggerModule.GetInside(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetInside(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x17000490 RID: 1168
			// (get) Token: 0x06001479 RID: 5241 RVA: 0x0001A92C File Offset: 0x00018B2C
			// (set) Token: 0x06001478 RID: 5240 RVA: 0x0001A91C File Offset: 0x00018B1C
			public ParticleSystemOverlapAction outside
			{
				get
				{
					return (ParticleSystemOverlapAction)ParticleSystem.TriggerModule.GetOutside(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetOutside(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x17000491 RID: 1169
			// (get) Token: 0x0600147B RID: 5243 RVA: 0x0001A95C File Offset: 0x00018B5C
			// (set) Token: 0x0600147A RID: 5242 RVA: 0x0001A94C File Offset: 0x00018B4C
			public ParticleSystemOverlapAction enter
			{
				get
				{
					return (ParticleSystemOverlapAction)ParticleSystem.TriggerModule.GetEnter(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetEnter(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x17000492 RID: 1170
			// (get) Token: 0x0600147D RID: 5245 RVA: 0x0001A98C File Offset: 0x00018B8C
			// (set) Token: 0x0600147C RID: 5244 RVA: 0x0001A97C File Offset: 0x00018B7C
			public ParticleSystemOverlapAction exit
			{
				get
				{
					return (ParticleSystemOverlapAction)ParticleSystem.TriggerModule.GetExit(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetExit(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x17000493 RID: 1171
			// (get) Token: 0x0600147E RID: 5246 RVA: 0x0001A9AC File Offset: 0x00018BAC
			// (set) Token: 0x0600147F RID: 5247 RVA: 0x0001A9CC File Offset: 0x00018BCC
			public float radiusScale
			{
				get
				{
					return ParticleSystem.TriggerModule.GetRadiusScale(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TriggerModule.SetRadiusScale(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x06001480 RID: 5248 RVA: 0x0001A9DC File Offset: 0x00018BDC
			public void SetCollider(int index, Component collider)
			{
				ParticleSystem.TriggerModule.SetCollider(this.m_ParticleSystem, index, collider);
			}

			// Token: 0x06001481 RID: 5249 RVA: 0x0001A9EC File Offset: 0x00018BEC
			public Component GetCollider(int index)
			{
				return ParticleSystem.TriggerModule.GetCollider(this.m_ParticleSystem, index);
			}

			// Token: 0x17000494 RID: 1172
			// (get) Token: 0x06001482 RID: 5250 RVA: 0x0001AA10 File Offset: 0x00018C10
			public int maxColliderCount
			{
				get
				{
					return ParticleSystem.TriggerModule.GetMaxColliderCount(this.m_ParticleSystem);
				}
			}

			// Token: 0x06001483 RID: 5251
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06001484 RID: 5252
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06001485 RID: 5253
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetInside(ParticleSystem system, int value);

			// Token: 0x06001486 RID: 5254
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetInside(ParticleSystem system);

			// Token: 0x06001487 RID: 5255
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetOutside(ParticleSystem system, int value);

			// Token: 0x06001488 RID: 5256
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetOutside(ParticleSystem system);

			// Token: 0x06001489 RID: 5257
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnter(ParticleSystem system, int value);

			// Token: 0x0600148A RID: 5258
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetEnter(ParticleSystem system);

			// Token: 0x0600148B RID: 5259
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetExit(ParticleSystem system, int value);

			// Token: 0x0600148C RID: 5260
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetExit(ParticleSystem system);

			// Token: 0x0600148D RID: 5261
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRadiusScale(ParticleSystem system, float value);

			// Token: 0x0600148E RID: 5262
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRadiusScale(ParticleSystem system);

			// Token: 0x0600148F RID: 5263
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCollider(ParticleSystem system, int index, Component collider);

			// Token: 0x06001490 RID: 5264
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Component GetCollider(ParticleSystem system, int index);

			// Token: 0x06001491 RID: 5265
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxColliderCount(ParticleSystem system);

			// Token: 0x04000327 RID: 807
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000124 RID: 292
		public struct SubEmittersModule
		{
			// Token: 0x06001492 RID: 5266 RVA: 0x0001AA30 File Offset: 0x00018C30
			internal SubEmittersModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x17000495 RID: 1173
			// (get) Token: 0x06001494 RID: 5268 RVA: 0x0001AA4C File Offset: 0x00018C4C
			// (set) Token: 0x06001493 RID: 5267 RVA: 0x0001AA3C File Offset: 0x00018C3C
			public bool enabled
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x17000496 RID: 1174
			// (get) Token: 0x06001495 RID: 5269 RVA: 0x0001AA6C File Offset: 0x00018C6C
			public int subEmittersCount
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetSubEmittersCount(this.m_ParticleSystem);
				}
			}

			// Token: 0x06001496 RID: 5270 RVA: 0x0001AA8C File Offset: 0x00018C8C
			public void AddSubEmitter(ParticleSystem subEmitter, ParticleSystemSubEmitterType type, ParticleSystemSubEmitterProperties properties)
			{
				ParticleSystem.SubEmittersModule.AddSubEmitter(this.m_ParticleSystem, subEmitter, (int)type, (int)properties);
			}

			// Token: 0x06001497 RID: 5271 RVA: 0x0001AAA0 File Offset: 0x00018CA0
			public void RemoveSubEmitter(int index)
			{
				ParticleSystem.SubEmittersModule.RemoveSubEmitter(this.m_ParticleSystem, index);
			}

			// Token: 0x06001498 RID: 5272 RVA: 0x0001AAB0 File Offset: 0x00018CB0
			public void SetSubEmitterSystem(int index, ParticleSystem subEmitter)
			{
				ParticleSystem.SubEmittersModule.SetSubEmitterSystem(this.m_ParticleSystem, index, subEmitter);
			}

			// Token: 0x06001499 RID: 5273 RVA: 0x0001AAC0 File Offset: 0x00018CC0
			public void SetSubEmitterType(int index, ParticleSystemSubEmitterType type)
			{
				ParticleSystem.SubEmittersModule.SetSubEmitterType(this.m_ParticleSystem, index, (int)type);
			}

			// Token: 0x0600149A RID: 5274 RVA: 0x0001AAD0 File Offset: 0x00018CD0
			public void SetSubEmitterProperties(int index, ParticleSystemSubEmitterProperties properties)
			{
				ParticleSystem.SubEmittersModule.SetSubEmitterProperties(this.m_ParticleSystem, index, (int)properties);
			}

			// Token: 0x0600149B RID: 5275 RVA: 0x0001AAE0 File Offset: 0x00018CE0
			public ParticleSystem GetSubEmitterSystem(int index)
			{
				return ParticleSystem.SubEmittersModule.GetSubEmitterSystem(this.m_ParticleSystem, index);
			}

			// Token: 0x0600149C RID: 5276 RVA: 0x0001AB04 File Offset: 0x00018D04
			public ParticleSystemSubEmitterType GetSubEmitterType(int index)
			{
				return (ParticleSystemSubEmitterType)ParticleSystem.SubEmittersModule.GetSubEmitterType(this.m_ParticleSystem, index);
			}

			// Token: 0x0600149D RID: 5277 RVA: 0x0001AB28 File Offset: 0x00018D28
			public ParticleSystemSubEmitterProperties GetSubEmitterProperties(int index)
			{
				return (ParticleSystemSubEmitterProperties)ParticleSystem.SubEmittersModule.GetSubEmitterProperties(this.m_ParticleSystem, index);
			}

			// Token: 0x17000497 RID: 1175
			// (get) Token: 0x0600149E RID: 5278 RVA: 0x0001AB4C File Offset: 0x00018D4C
			// (set) Token: 0x0600149F RID: 5279 RVA: 0x0001AB70 File Offset: 0x00018D70
			[Obsolete("birth0 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
			public ParticleSystem birth0
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetBirth(this.m_ParticleSystem, 0);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetBirth(this.m_ParticleSystem, 0, value);
				}
			}

			// Token: 0x17000498 RID: 1176
			// (get) Token: 0x060014A0 RID: 5280 RVA: 0x0001AB80 File Offset: 0x00018D80
			// (set) Token: 0x060014A1 RID: 5281 RVA: 0x0001ABA4 File Offset: 0x00018DA4
			[Obsolete("birth1 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
			public ParticleSystem birth1
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetBirth(this.m_ParticleSystem, 1);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetBirth(this.m_ParticleSystem, 1, value);
				}
			}

			// Token: 0x17000499 RID: 1177
			// (get) Token: 0x060014A2 RID: 5282 RVA: 0x0001ABB4 File Offset: 0x00018DB4
			// (set) Token: 0x060014A3 RID: 5283 RVA: 0x0001ABD8 File Offset: 0x00018DD8
			[Obsolete("collision0 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
			public ParticleSystem collision0
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetCollision(this.m_ParticleSystem, 0);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetCollision(this.m_ParticleSystem, 0, value);
				}
			}

			// Token: 0x1700049A RID: 1178
			// (get) Token: 0x060014A4 RID: 5284 RVA: 0x0001ABE8 File Offset: 0x00018DE8
			// (set) Token: 0x060014A5 RID: 5285 RVA: 0x0001AC0C File Offset: 0x00018E0C
			[Obsolete("collision1 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
			public ParticleSystem collision1
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetCollision(this.m_ParticleSystem, 1);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetCollision(this.m_ParticleSystem, 1, value);
				}
			}

			// Token: 0x1700049B RID: 1179
			// (get) Token: 0x060014A6 RID: 5286 RVA: 0x0001AC1C File Offset: 0x00018E1C
			// (set) Token: 0x060014A7 RID: 5287 RVA: 0x0001AC40 File Offset: 0x00018E40
			[Obsolete("death0 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
			public ParticleSystem death0
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetDeath(this.m_ParticleSystem, 0);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetDeath(this.m_ParticleSystem, 0, value);
				}
			}

			// Token: 0x1700049C RID: 1180
			// (get) Token: 0x060014A8 RID: 5288 RVA: 0x0001AC50 File Offset: 0x00018E50
			// (set) Token: 0x060014A9 RID: 5289 RVA: 0x0001AC74 File Offset: 0x00018E74
			[Obsolete("death1 property is deprecated. Use AddSubEmitter, RemoveSubEmitter, SetSubEmitterSystem and GetSubEmitterSystem instead.")]
			public ParticleSystem death1
			{
				get
				{
					return ParticleSystem.SubEmittersModule.GetDeath(this.m_ParticleSystem, 1);
				}
				set
				{
					ParticleSystem.SubEmittersModule.SetDeath(this.m_ParticleSystem, 1, value);
				}
			}

			// Token: 0x060014AA RID: 5290
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060014AB RID: 5291
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060014AC RID: 5292
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetSubEmittersCount(ParticleSystem system);

			// Token: 0x060014AD RID: 5293
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetBirth(ParticleSystem system, int index, ParticleSystem value);

			// Token: 0x060014AE RID: 5294
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystem GetBirth(ParticleSystem system, int index);

			// Token: 0x060014AF RID: 5295
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCollision(ParticleSystem system, int index, ParticleSystem value);

			// Token: 0x060014B0 RID: 5296
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystem GetCollision(ParticleSystem system, int index);

			// Token: 0x060014B1 RID: 5297
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDeath(ParticleSystem system, int index, ParticleSystem value);

			// Token: 0x060014B2 RID: 5298
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystem GetDeath(ParticleSystem system, int index);

			// Token: 0x060014B3 RID: 5299
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void AddSubEmitter(ParticleSystem system, ParticleSystem subEmitter, int type, int properties);

			// Token: 0x060014B4 RID: 5300
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void RemoveSubEmitter(ParticleSystem system, int index);

			// Token: 0x060014B5 RID: 5301
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSubEmitterSystem(ParticleSystem system, int index, ParticleSystem subEmitter);

			// Token: 0x060014B6 RID: 5302
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSubEmitterType(ParticleSystem system, int index, int type);

			// Token: 0x060014B7 RID: 5303
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSubEmitterProperties(ParticleSystem system, int index, int properties);

			// Token: 0x060014B8 RID: 5304
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern ParticleSystem GetSubEmitterSystem(ParticleSystem system, int index);

			// Token: 0x060014B9 RID: 5305
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetSubEmitterType(ParticleSystem system, int index);

			// Token: 0x060014BA RID: 5306
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetSubEmitterProperties(ParticleSystem system, int index);

			// Token: 0x04000328 RID: 808
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000125 RID: 293
		public struct TextureSheetAnimationModule
		{
			// Token: 0x060014BB RID: 5307 RVA: 0x0001AC84 File Offset: 0x00018E84
			internal TextureSheetAnimationModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x1700049D RID: 1181
			// (get) Token: 0x060014BD RID: 5309 RVA: 0x0001ACA0 File Offset: 0x00018EA0
			// (set) Token: 0x060014BC RID: 5308 RVA: 0x0001AC90 File Offset: 0x00018E90
			public bool enabled
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700049E RID: 1182
			// (get) Token: 0x060014BF RID: 5311 RVA: 0x0001ACD0 File Offset: 0x00018ED0
			// (set) Token: 0x060014BE RID: 5310 RVA: 0x0001ACC0 File Offset: 0x00018EC0
			public int numTilesX
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetNumTilesX(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetNumTilesX(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x1700049F RID: 1183
			// (get) Token: 0x060014C1 RID: 5313 RVA: 0x0001AD00 File Offset: 0x00018F00
			// (set) Token: 0x060014C0 RID: 5312 RVA: 0x0001ACF0 File Offset: 0x00018EF0
			public int numTilesY
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetNumTilesY(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetNumTilesY(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004A0 RID: 1184
			// (get) Token: 0x060014C3 RID: 5315 RVA: 0x0001AD30 File Offset: 0x00018F30
			// (set) Token: 0x060014C2 RID: 5314 RVA: 0x0001AD20 File Offset: 0x00018F20
			public ParticleSystemAnimationType animation
			{
				get
				{
					return (ParticleSystemAnimationType)ParticleSystem.TextureSheetAnimationModule.GetAnimationType(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetAnimationType(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x170004A1 RID: 1185
			// (get) Token: 0x060014C5 RID: 5317 RVA: 0x0001AD60 File Offset: 0x00018F60
			// (set) Token: 0x060014C4 RID: 5316 RVA: 0x0001AD50 File Offset: 0x00018F50
			public bool useRandomRow
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetUseRandomRow(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetUseRandomRow(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004A2 RID: 1186
			// (get) Token: 0x060014C7 RID: 5319 RVA: 0x0001AD90 File Offset: 0x00018F90
			// (set) Token: 0x060014C6 RID: 5318 RVA: 0x0001AD80 File Offset: 0x00018F80
			public ParticleSystem.MinMaxCurve frameOverTime
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.TextureSheetAnimationModule.GetFrameOverTime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetFrameOverTime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170004A3 RID: 1187
			// (get) Token: 0x060014C8 RID: 5320 RVA: 0x0001ADBC File Offset: 0x00018FBC
			// (set) Token: 0x060014C9 RID: 5321 RVA: 0x0001ADDC File Offset: 0x00018FDC
			public float frameOverTimeMultiplier
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetFrameOverTimeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetFrameOverTimeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004A4 RID: 1188
			// (get) Token: 0x060014CB RID: 5323 RVA: 0x0001ADFC File Offset: 0x00018FFC
			// (set) Token: 0x060014CA RID: 5322 RVA: 0x0001ADEC File Offset: 0x00018FEC
			public ParticleSystem.MinMaxCurve startFrame
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.TextureSheetAnimationModule.GetStartFrame(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetStartFrame(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170004A5 RID: 1189
			// (get) Token: 0x060014CC RID: 5324 RVA: 0x0001AE28 File Offset: 0x00019028
			// (set) Token: 0x060014CD RID: 5325 RVA: 0x0001AE48 File Offset: 0x00019048
			public float startFrameMultiplier
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetStartFrameMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetStartFrameMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004A6 RID: 1190
			// (get) Token: 0x060014CF RID: 5327 RVA: 0x0001AE68 File Offset: 0x00019068
			// (set) Token: 0x060014CE RID: 5326 RVA: 0x0001AE58 File Offset: 0x00019058
			public int cycleCount
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetCycleCount(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetCycleCount(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004A7 RID: 1191
			// (get) Token: 0x060014D1 RID: 5329 RVA: 0x0001AE98 File Offset: 0x00019098
			// (set) Token: 0x060014D0 RID: 5328 RVA: 0x0001AE88 File Offset: 0x00019088
			public int rowIndex
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetRowIndex(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetRowIndex(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004A8 RID: 1192
			// (get) Token: 0x060014D3 RID: 5331 RVA: 0x0001AEC8 File Offset: 0x000190C8
			// (set) Token: 0x060014D2 RID: 5330 RVA: 0x0001AEB8 File Offset: 0x000190B8
			public UVChannelFlags uvChannelMask
			{
				get
				{
					return (UVChannelFlags)ParticleSystem.TextureSheetAnimationModule.GetUVChannelMask(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetUVChannelMask(this.m_ParticleSystem, (int)value);
				}
			}

			// Token: 0x170004A9 RID: 1193
			// (get) Token: 0x060014D4 RID: 5332 RVA: 0x0001AEE8 File Offset: 0x000190E8
			// (set) Token: 0x060014D5 RID: 5333 RVA: 0x0001AF08 File Offset: 0x00019108
			public float flipU
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetFlipU(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetFlipU(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004AA RID: 1194
			// (get) Token: 0x060014D6 RID: 5334 RVA: 0x0001AF18 File Offset: 0x00019118
			// (set) Token: 0x060014D7 RID: 5335 RVA: 0x0001AF38 File Offset: 0x00019138
			public float flipV
			{
				get
				{
					return ParticleSystem.TextureSheetAnimationModule.GetFlipV(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TextureSheetAnimationModule.SetFlipV(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x060014D8 RID: 5336
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x060014D9 RID: 5337
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x060014DA RID: 5338
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetNumTilesX(ParticleSystem system, int value);

			// Token: 0x060014DB RID: 5339
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetNumTilesX(ParticleSystem system);

			// Token: 0x060014DC RID: 5340
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetNumTilesY(ParticleSystem system, int value);

			// Token: 0x060014DD RID: 5341
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetNumTilesY(ParticleSystem system);

			// Token: 0x060014DE RID: 5342
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetAnimationType(ParticleSystem system, int value);

			// Token: 0x060014DF RID: 5343
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetAnimationType(ParticleSystem system);

			// Token: 0x060014E0 RID: 5344
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseRandomRow(ParticleSystem system, bool value);

			// Token: 0x060014E1 RID: 5345
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseRandomRow(ParticleSystem system);

			// Token: 0x060014E2 RID: 5346
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFrameOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060014E3 RID: 5347
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetFrameOverTime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060014E4 RID: 5348
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFrameOverTimeMultiplier(ParticleSystem system, float value);

			// Token: 0x060014E5 RID: 5349
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetFrameOverTimeMultiplier(ParticleSystem system);

			// Token: 0x060014E6 RID: 5350
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartFrame(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060014E7 RID: 5351
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetStartFrame(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x060014E8 RID: 5352
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetStartFrameMultiplier(ParticleSystem system, float value);

			// Token: 0x060014E9 RID: 5353
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetStartFrameMultiplier(ParticleSystem system);

			// Token: 0x060014EA RID: 5354
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetCycleCount(ParticleSystem system, int value);

			// Token: 0x060014EB RID: 5355
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetCycleCount(ParticleSystem system);

			// Token: 0x060014EC RID: 5356
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRowIndex(ParticleSystem system, int value);

			// Token: 0x060014ED RID: 5357
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetRowIndex(ParticleSystem system);

			// Token: 0x060014EE RID: 5358
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUVChannelMask(ParticleSystem system, int value);

			// Token: 0x060014EF RID: 5359
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetUVChannelMask(ParticleSystem system);

			// Token: 0x060014F0 RID: 5360
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFlipU(ParticleSystem system, float value);

			// Token: 0x060014F1 RID: 5361
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetFlipU(ParticleSystem system);

			// Token: 0x060014F2 RID: 5362
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetFlipV(ParticleSystem system, float value);

			// Token: 0x060014F3 RID: 5363
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetFlipV(ParticleSystem system);

			// Token: 0x04000329 RID: 809
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000126 RID: 294
		public struct LightsModule
		{
			// Token: 0x060014F4 RID: 5364 RVA: 0x0001AF48 File Offset: 0x00019148
			internal LightsModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x170004AB RID: 1195
			// (get) Token: 0x060014F6 RID: 5366 RVA: 0x0001AF64 File Offset: 0x00019164
			// (set) Token: 0x060014F5 RID: 5365 RVA: 0x0001AF54 File Offset: 0x00019154
			public bool enabled
			{
				get
				{
					return ParticleSystem.LightsModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004AC RID: 1196
			// (get) Token: 0x060014F7 RID: 5367 RVA: 0x0001AF84 File Offset: 0x00019184
			// (set) Token: 0x060014F8 RID: 5368 RVA: 0x0001AFA4 File Offset: 0x000191A4
			public float ratio
			{
				get
				{
					return ParticleSystem.LightsModule.GetRatio(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetRatio(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004AD RID: 1197
			// (get) Token: 0x060014FA RID: 5370 RVA: 0x0001AFC4 File Offset: 0x000191C4
			// (set) Token: 0x060014F9 RID: 5369 RVA: 0x0001AFB4 File Offset: 0x000191B4
			public bool useRandomDistribution
			{
				get
				{
					return ParticleSystem.LightsModule.GetUseRandomDistribution(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetUseRandomDistribution(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004AE RID: 1198
			// (get) Token: 0x060014FC RID: 5372 RVA: 0x0001AFF4 File Offset: 0x000191F4
			// (set) Token: 0x060014FB RID: 5371 RVA: 0x0001AFE4 File Offset: 0x000191E4
			public Light light
			{
				get
				{
					return ParticleSystem.LightsModule.GetLightPrefab(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetLightPrefab(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004AF RID: 1199
			// (get) Token: 0x060014FE RID: 5374 RVA: 0x0001B024 File Offset: 0x00019224
			// (set) Token: 0x060014FD RID: 5373 RVA: 0x0001B014 File Offset: 0x00019214
			public bool useParticleColor
			{
				get
				{
					return ParticleSystem.LightsModule.GetUseParticleColor(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetUseParticleColor(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004B0 RID: 1200
			// (get) Token: 0x06001500 RID: 5376 RVA: 0x0001B054 File Offset: 0x00019254
			// (set) Token: 0x060014FF RID: 5375 RVA: 0x0001B044 File Offset: 0x00019244
			public bool sizeAffectsRange
			{
				get
				{
					return ParticleSystem.LightsModule.GetSizeAffectsRange(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetSizeAffectsRange(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004B1 RID: 1201
			// (get) Token: 0x06001502 RID: 5378 RVA: 0x0001B084 File Offset: 0x00019284
			// (set) Token: 0x06001501 RID: 5377 RVA: 0x0001B074 File Offset: 0x00019274
			public bool alphaAffectsIntensity
			{
				get
				{
					return ParticleSystem.LightsModule.GetAlphaAffectsIntensity(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetAlphaAffectsIntensity(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004B2 RID: 1202
			// (get) Token: 0x06001504 RID: 5380 RVA: 0x0001B0B4 File Offset: 0x000192B4
			// (set) Token: 0x06001503 RID: 5379 RVA: 0x0001B0A4 File Offset: 0x000192A4
			public ParticleSystem.MinMaxCurve range
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LightsModule.GetRange(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LightsModule.SetRange(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170004B3 RID: 1203
			// (get) Token: 0x06001505 RID: 5381 RVA: 0x0001B0E0 File Offset: 0x000192E0
			// (set) Token: 0x06001506 RID: 5382 RVA: 0x0001B100 File Offset: 0x00019300
			public float rangeMultiplier
			{
				get
				{
					return ParticleSystem.LightsModule.GetRangeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetRangeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004B4 RID: 1204
			// (get) Token: 0x06001508 RID: 5384 RVA: 0x0001B120 File Offset: 0x00019320
			// (set) Token: 0x06001507 RID: 5383 RVA: 0x0001B110 File Offset: 0x00019310
			public ParticleSystem.MinMaxCurve intensity
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.LightsModule.GetIntensity(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.LightsModule.SetIntensity(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170004B5 RID: 1205
			// (get) Token: 0x06001509 RID: 5385 RVA: 0x0001B14C File Offset: 0x0001934C
			// (set) Token: 0x0600150A RID: 5386 RVA: 0x0001B16C File Offset: 0x0001936C
			public float intensityMultiplier
			{
				get
				{
					return ParticleSystem.LightsModule.GetIntensityMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetIntensityMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004B6 RID: 1206
			// (get) Token: 0x0600150C RID: 5388 RVA: 0x0001B18C File Offset: 0x0001938C
			// (set) Token: 0x0600150B RID: 5387 RVA: 0x0001B17C File Offset: 0x0001937C
			public int maxLights
			{
				get
				{
					return ParticleSystem.LightsModule.GetMaxLights(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.LightsModule.SetMaxLights(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x0600150D RID: 5389
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x0600150E RID: 5390
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x0600150F RID: 5391
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRatio(ParticleSystem system, float value);

			// Token: 0x06001510 RID: 5392
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRatio(ParticleSystem system);

			// Token: 0x06001511 RID: 5393
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseRandomDistribution(ParticleSystem system, bool value);

			// Token: 0x06001512 RID: 5394
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseRandomDistribution(ParticleSystem system);

			// Token: 0x06001513 RID: 5395
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLightPrefab(ParticleSystem system, Light value);

			// Token: 0x06001514 RID: 5396
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern Light GetLightPrefab(ParticleSystem system);

			// Token: 0x06001515 RID: 5397
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetUseParticleColor(ParticleSystem system, bool value);

			// Token: 0x06001516 RID: 5398
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetUseParticleColor(ParticleSystem system);

			// Token: 0x06001517 RID: 5399
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSizeAffectsRange(ParticleSystem system, bool value);

			// Token: 0x06001518 RID: 5400
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSizeAffectsRange(ParticleSystem system);

			// Token: 0x06001519 RID: 5401
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetAlphaAffectsIntensity(ParticleSystem system, bool value);

			// Token: 0x0600151A RID: 5402
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetAlphaAffectsIntensity(ParticleSystem system);

			// Token: 0x0600151B RID: 5403
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRange(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600151C RID: 5404
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetRange(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600151D RID: 5405
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRangeMultiplier(ParticleSystem system, float value);

			// Token: 0x0600151E RID: 5406
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRangeMultiplier(ParticleSystem system);

			// Token: 0x0600151F RID: 5407
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetIntensity(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001520 RID: 5408
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetIntensity(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001521 RID: 5409
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetIntensityMultiplier(ParticleSystem system, float value);

			// Token: 0x06001522 RID: 5410
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetIntensityMultiplier(ParticleSystem system);

			// Token: 0x06001523 RID: 5411
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMaxLights(ParticleSystem system, int value);

			// Token: 0x06001524 RID: 5412
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetMaxLights(ParticleSystem system);

			// Token: 0x0400032A RID: 810
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000127 RID: 295
		public struct TrailModule
		{
			// Token: 0x06001525 RID: 5413 RVA: 0x0001B1AC File Offset: 0x000193AC
			internal TrailModule(ParticleSystem particleSystem)
			{
				this.m_ParticleSystem = particleSystem;
			}

			// Token: 0x170004B7 RID: 1207
			// (get) Token: 0x06001527 RID: 5415 RVA: 0x0001B1C8 File Offset: 0x000193C8
			// (set) Token: 0x06001526 RID: 5414 RVA: 0x0001B1B8 File Offset: 0x000193B8
			public bool enabled
			{
				get
				{
					return ParticleSystem.TrailModule.GetEnabled(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetEnabled(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004B8 RID: 1208
			// (get) Token: 0x06001528 RID: 5416 RVA: 0x0001B1E8 File Offset: 0x000193E8
			// (set) Token: 0x06001529 RID: 5417 RVA: 0x0001B208 File Offset: 0x00019408
			public float ratio
			{
				get
				{
					return ParticleSystem.TrailModule.GetRatio(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetRatio(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004B9 RID: 1209
			// (get) Token: 0x0600152B RID: 5419 RVA: 0x0001B228 File Offset: 0x00019428
			// (set) Token: 0x0600152A RID: 5418 RVA: 0x0001B218 File Offset: 0x00019418
			public ParticleSystem.MinMaxCurve lifetime
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.TrailModule.GetLifetime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TrailModule.SetLifetime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170004BA RID: 1210
			// (get) Token: 0x0600152C RID: 5420 RVA: 0x0001B254 File Offset: 0x00019454
			// (set) Token: 0x0600152D RID: 5421 RVA: 0x0001B274 File Offset: 0x00019474
			public float lifetimeMultiplier
			{
				get
				{
					return ParticleSystem.TrailModule.GetLifetimeMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetLifetimeMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004BB RID: 1211
			// (get) Token: 0x0600152F RID: 5423 RVA: 0x0001B294 File Offset: 0x00019494
			// (set) Token: 0x0600152E RID: 5422 RVA: 0x0001B284 File Offset: 0x00019484
			public float minVertexDistance
			{
				get
				{
					return ParticleSystem.TrailModule.GetMinVertexDistance(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetMinVertexDistance(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004BC RID: 1212
			// (get) Token: 0x06001530 RID: 5424 RVA: 0x0001B2B4 File Offset: 0x000194B4
			// (set) Token: 0x06001531 RID: 5425 RVA: 0x0001B2D4 File Offset: 0x000194D4
			public ParticleSystemTrailTextureMode textureMode
			{
				get
				{
					return (ParticleSystemTrailTextureMode)ParticleSystem.TrailModule.GetTextureMode(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetTextureMode(this.m_ParticleSystem, (float)value);
				}
			}

			// Token: 0x170004BD RID: 1213
			// (get) Token: 0x06001533 RID: 5427 RVA: 0x0001B2F4 File Offset: 0x000194F4
			// (set) Token: 0x06001532 RID: 5426 RVA: 0x0001B2E4 File Offset: 0x000194E4
			public bool worldSpace
			{
				get
				{
					return ParticleSystem.TrailModule.GetWorldSpace(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetWorldSpace(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004BE RID: 1214
			// (get) Token: 0x06001535 RID: 5429 RVA: 0x0001B324 File Offset: 0x00019524
			// (set) Token: 0x06001534 RID: 5428 RVA: 0x0001B314 File Offset: 0x00019514
			public bool dieWithParticles
			{
				get
				{
					return ParticleSystem.TrailModule.GetDieWithParticles(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetDieWithParticles(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004BF RID: 1215
			// (get) Token: 0x06001537 RID: 5431 RVA: 0x0001B354 File Offset: 0x00019554
			// (set) Token: 0x06001536 RID: 5430 RVA: 0x0001B344 File Offset: 0x00019544
			public bool sizeAffectsWidth
			{
				get
				{
					return ParticleSystem.TrailModule.GetSizeAffectsWidth(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetSizeAffectsWidth(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004C0 RID: 1216
			// (get) Token: 0x06001539 RID: 5433 RVA: 0x0001B384 File Offset: 0x00019584
			// (set) Token: 0x06001538 RID: 5432 RVA: 0x0001B374 File Offset: 0x00019574
			public bool sizeAffectsLifetime
			{
				get
				{
					return ParticleSystem.TrailModule.GetSizeAffectsLifetime(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetSizeAffectsLifetime(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004C1 RID: 1217
			// (get) Token: 0x0600153B RID: 5435 RVA: 0x0001B3B4 File Offset: 0x000195B4
			// (set) Token: 0x0600153A RID: 5434 RVA: 0x0001B3A4 File Offset: 0x000195A4
			public bool inheritParticleColor
			{
				get
				{
					return ParticleSystem.TrailModule.GetInheritParticleColor(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetInheritParticleColor(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004C2 RID: 1218
			// (get) Token: 0x0600153D RID: 5437 RVA: 0x0001B3E4 File Offset: 0x000195E4
			// (set) Token: 0x0600153C RID: 5436 RVA: 0x0001B3D4 File Offset: 0x000195D4
			public ParticleSystem.MinMaxGradient colorOverLifetime
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.TrailModule.GetColorOverLifetime(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TrailModule.SetColorOverLifetime(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170004C3 RID: 1219
			// (get) Token: 0x0600153F RID: 5439 RVA: 0x0001B420 File Offset: 0x00019620
			// (set) Token: 0x0600153E RID: 5438 RVA: 0x0001B410 File Offset: 0x00019610
			public ParticleSystem.MinMaxCurve widthOverTrail
			{
				get
				{
					ParticleSystem.MinMaxCurve result = default(ParticleSystem.MinMaxCurve);
					ParticleSystem.TrailModule.GetWidthOverTrail(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TrailModule.SetWidthOverTrail(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x170004C4 RID: 1220
			// (get) Token: 0x06001540 RID: 5440 RVA: 0x0001B44C File Offset: 0x0001964C
			// (set) Token: 0x06001541 RID: 5441 RVA: 0x0001B46C File Offset: 0x0001966C
			public float widthOverTrailMultiplier
			{
				get
				{
					return ParticleSystem.TrailModule.GetWidthOverTrailMultiplier(this.m_ParticleSystem);
				}
				set
				{
					ParticleSystem.TrailModule.SetWidthOverTrailMultiplier(this.m_ParticleSystem, value);
				}
			}

			// Token: 0x170004C5 RID: 1221
			// (get) Token: 0x06001543 RID: 5443 RVA: 0x0001B48C File Offset: 0x0001968C
			// (set) Token: 0x06001542 RID: 5442 RVA: 0x0001B47C File Offset: 0x0001967C
			public ParticleSystem.MinMaxGradient colorOverTrail
			{
				get
				{
					ParticleSystem.MinMaxGradient result = default(ParticleSystem.MinMaxGradient);
					ParticleSystem.TrailModule.GetColorOverTrail(this.m_ParticleSystem, ref result);
					return result;
				}
				set
				{
					ParticleSystem.TrailModule.SetColorOverTrail(this.m_ParticleSystem, ref value);
				}
			}

			// Token: 0x06001544 RID: 5444
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetEnabled(ParticleSystem system, bool value);

			// Token: 0x06001545 RID: 5445
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetEnabled(ParticleSystem system);

			// Token: 0x06001546 RID: 5446
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetRatio(ParticleSystem system, float value);

			// Token: 0x06001547 RID: 5447
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetRatio(ParticleSystem system);

			// Token: 0x06001548 RID: 5448
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x06001549 RID: 5449
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetLifetime(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600154A RID: 5450
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetLifetimeMultiplier(ParticleSystem system, float value);

			// Token: 0x0600154B RID: 5451
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetLifetimeMultiplier(ParticleSystem system);

			// Token: 0x0600154C RID: 5452
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetMinVertexDistance(ParticleSystem system, float value);

			// Token: 0x0600154D RID: 5453
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetMinVertexDistance(ParticleSystem system);

			// Token: 0x0600154E RID: 5454
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetTextureMode(ParticleSystem system, float value);

			// Token: 0x0600154F RID: 5455
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern int GetTextureMode(ParticleSystem system);

			// Token: 0x06001550 RID: 5456
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWorldSpace(ParticleSystem system, bool value);

			// Token: 0x06001551 RID: 5457
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetWorldSpace(ParticleSystem system);

			// Token: 0x06001552 RID: 5458
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetDieWithParticles(ParticleSystem system, bool value);

			// Token: 0x06001553 RID: 5459
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetDieWithParticles(ParticleSystem system);

			// Token: 0x06001554 RID: 5460
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSizeAffectsWidth(ParticleSystem system, bool value);

			// Token: 0x06001555 RID: 5461
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSizeAffectsWidth(ParticleSystem system);

			// Token: 0x06001556 RID: 5462
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetSizeAffectsLifetime(ParticleSystem system, bool value);

			// Token: 0x06001557 RID: 5463
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetSizeAffectsLifetime(ParticleSystem system);

			// Token: 0x06001558 RID: 5464
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetInheritParticleColor(ParticleSystem system, bool value);

			// Token: 0x06001559 RID: 5465
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern bool GetInheritParticleColor(ParticleSystem system);

			// Token: 0x0600155A RID: 5466
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColorOverLifetime(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0600155B RID: 5467
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetColorOverLifetime(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0600155C RID: 5468
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWidthOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600155D RID: 5469
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetWidthOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxCurve curve);

			// Token: 0x0600155E RID: 5470
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetWidthOverTrailMultiplier(ParticleSystem system, float value);

			// Token: 0x0600155F RID: 5471
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetWidthOverTrailMultiplier(ParticleSystem system);

			// Token: 0x06001560 RID: 5472
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void SetColorOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x06001561 RID: 5473
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void GetColorOverTrail(ParticleSystem system, ref ParticleSystem.MinMaxGradient gradient);

			// Token: 0x0400032B RID: 811
			private ParticleSystem m_ParticleSystem;
		}

		// Token: 0x02000128 RID: 296
		[RequiredByNativeCode("particleSystemParticle", Optional = true)]
		public struct Particle
		{
			// Token: 0x170004C6 RID: 1222
			// (get) Token: 0x06001562 RID: 5474 RVA: 0x0001B4B8 File Offset: 0x000196B8
			// (set) Token: 0x06001563 RID: 5475 RVA: 0x0001B4D4 File Offset: 0x000196D4
			public Vector3 position
			{
				get
				{
					return this.m_Position;
				}
				set
				{
					this.m_Position = value;
				}
			}

			// Token: 0x170004C7 RID: 1223
			// (get) Token: 0x06001564 RID: 5476 RVA: 0x0001B4E0 File Offset: 0x000196E0
			// (set) Token: 0x06001565 RID: 5477 RVA: 0x0001B4FC File Offset: 0x000196FC
			public Vector3 velocity
			{
				get
				{
					return this.m_Velocity;
				}
				set
				{
					this.m_Velocity = value;
				}
			}

			// Token: 0x170004C8 RID: 1224
			// (get) Token: 0x06001566 RID: 5478 RVA: 0x0001B508 File Offset: 0x00019708
			// (set) Token: 0x06001567 RID: 5479 RVA: 0x0001B524 File Offset: 0x00019724
			[Obsolete("Please use Particle.remainingLifetime instead. (UnityUpgradable) -> UnityEngine.ParticleSystem/Particle.remainingLifetime")]
			public float lifetime
			{
				get
				{
					return this.m_Lifetime;
				}
				set
				{
					this.m_Lifetime = value;
				}
			}

			// Token: 0x170004C9 RID: 1225
			// (get) Token: 0x06001568 RID: 5480 RVA: 0x0001B530 File Offset: 0x00019730
			// (set) Token: 0x06001569 RID: 5481 RVA: 0x0001B54C File Offset: 0x0001974C
			public float remainingLifetime
			{
				get
				{
					return this.m_Lifetime;
				}
				set
				{
					this.m_Lifetime = value;
				}
			}

			// Token: 0x170004CA RID: 1226
			// (get) Token: 0x0600156A RID: 5482 RVA: 0x0001B558 File Offset: 0x00019758
			// (set) Token: 0x0600156B RID: 5483 RVA: 0x0001B574 File Offset: 0x00019774
			public float startLifetime
			{
				get
				{
					return this.m_StartLifetime;
				}
				set
				{
					this.m_StartLifetime = value;
				}
			}

			// Token: 0x170004CB RID: 1227
			// (get) Token: 0x0600156C RID: 5484 RVA: 0x0001B580 File Offset: 0x00019780
			// (set) Token: 0x0600156D RID: 5485 RVA: 0x0001B5A0 File Offset: 0x000197A0
			public float startSize
			{
				get
				{
					return this.m_StartSize.x;
				}
				set
				{
					this.m_StartSize = new Vector3(value, value, value);
				}
			}

			// Token: 0x170004CC RID: 1228
			// (get) Token: 0x0600156E RID: 5486 RVA: 0x0001B5B4 File Offset: 0x000197B4
			// (set) Token: 0x0600156F RID: 5487 RVA: 0x0001B5D0 File Offset: 0x000197D0
			public Vector3 startSize3D
			{
				get
				{
					return this.m_StartSize;
				}
				set
				{
					this.m_StartSize = value;
				}
			}

			// Token: 0x170004CD RID: 1229
			// (get) Token: 0x06001570 RID: 5488 RVA: 0x0001B5DC File Offset: 0x000197DC
			// (set) Token: 0x06001571 RID: 5489 RVA: 0x0001B5F8 File Offset: 0x000197F8
			public Vector3 axisOfRotation
			{
				get
				{
					return this.m_AxisOfRotation;
				}
				set
				{
					this.m_AxisOfRotation = value;
				}
			}

			// Token: 0x170004CE RID: 1230
			// (get) Token: 0x06001572 RID: 5490 RVA: 0x0001B604 File Offset: 0x00019804
			// (set) Token: 0x06001573 RID: 5491 RVA: 0x0001B62C File Offset: 0x0001982C
			public float rotation
			{
				get
				{
					return this.m_Rotation.z * 57.29578f;
				}
				set
				{
					this.m_Rotation = new Vector3(0f, 0f, value * 0.017453292f);
				}
			}

			// Token: 0x170004CF RID: 1231
			// (get) Token: 0x06001574 RID: 5492 RVA: 0x0001B64C File Offset: 0x0001984C
			// (set) Token: 0x06001575 RID: 5493 RVA: 0x0001B674 File Offset: 0x00019874
			public Vector3 rotation3D
			{
				get
				{
					return this.m_Rotation * 57.29578f;
				}
				set
				{
					this.m_Rotation = value * 0.017453292f;
				}
			}

			// Token: 0x170004D0 RID: 1232
			// (get) Token: 0x06001576 RID: 5494 RVA: 0x0001B688 File Offset: 0x00019888
			// (set) Token: 0x06001577 RID: 5495 RVA: 0x0001B6B0 File Offset: 0x000198B0
			public float angularVelocity
			{
				get
				{
					return this.m_AngularVelocity.z * 57.29578f;
				}
				set
				{
					this.m_AngularVelocity.z = value * 0.017453292f;
				}
			}

			// Token: 0x170004D1 RID: 1233
			// (get) Token: 0x06001578 RID: 5496 RVA: 0x0001B6C8 File Offset: 0x000198C8
			// (set) Token: 0x06001579 RID: 5497 RVA: 0x0001B6F0 File Offset: 0x000198F0
			public Vector3 angularVelocity3D
			{
				get
				{
					return this.m_AngularVelocity * 57.29578f;
				}
				set
				{
					this.m_AngularVelocity = value * 0.017453292f;
				}
			}

			// Token: 0x170004D2 RID: 1234
			// (get) Token: 0x0600157A RID: 5498 RVA: 0x0001B704 File Offset: 0x00019904
			// (set) Token: 0x0600157B RID: 5499 RVA: 0x0001B720 File Offset: 0x00019920
			public Color32 startColor
			{
				get
				{
					return this.m_StartColor;
				}
				set
				{
					this.m_StartColor = value;
				}
			}

			// Token: 0x170004D3 RID: 1235
			// (get) Token: 0x0600157C RID: 5500 RVA: 0x0001B72C File Offset: 0x0001992C
			// (set) Token: 0x0600157D RID: 5501 RVA: 0x0001B754 File Offset: 0x00019954
			[Obsolete("randomValue property is deprecated. Use randomSeed instead to control random behavior of particles.")]
			public float randomValue
			{
				get
				{
					return BitConverter.ToSingle(BitConverter.GetBytes(this.m_RandomSeed), 0);
				}
				set
				{
					this.m_RandomSeed = BitConverter.ToUInt32(BitConverter.GetBytes(value), 0);
				}
			}

			// Token: 0x170004D4 RID: 1236
			// (get) Token: 0x0600157E RID: 5502 RVA: 0x0001B76C File Offset: 0x0001996C
			// (set) Token: 0x0600157F RID: 5503 RVA: 0x0001B788 File Offset: 0x00019988
			public uint randomSeed
			{
				get
				{
					return this.m_RandomSeed;
				}
				set
				{
					this.m_RandomSeed = value;
				}
			}

			// Token: 0x06001580 RID: 5504 RVA: 0x0001B794 File Offset: 0x00019994
			public float GetCurrentSize(ParticleSystem system)
			{
				return ParticleSystem.Particle.GetCurrentSize(system, ref this);
			}

			// Token: 0x06001581 RID: 5505 RVA: 0x0001B7B0 File Offset: 0x000199B0
			public Vector3 GetCurrentSize3D(ParticleSystem system)
			{
				return ParticleSystem.Particle.GetCurrentSize3D(system, ref this);
			}

			// Token: 0x06001582 RID: 5506 RVA: 0x0001B7CC File Offset: 0x000199CC
			public Color32 GetCurrentColor(ParticleSystem system)
			{
				return ParticleSystem.Particle.GetCurrentColor(system, ref this);
			}

			// Token: 0x06001583 RID: 5507
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern float GetCurrentSize(ParticleSystem system, ref ParticleSystem.Particle particle);

			// Token: 0x06001584 RID: 5508 RVA: 0x0001B7E8 File Offset: 0x000199E8
			private static Vector3 GetCurrentSize3D(ParticleSystem system, ref ParticleSystem.Particle particle)
			{
				Vector3 result;
				ParticleSystem.Particle.INTERNAL_CALL_GetCurrentSize3D(system, ref particle, out result);
				return result;
			}

			// Token: 0x06001585 RID: 5509
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetCurrentSize3D(ParticleSystem system, ref ParticleSystem.Particle particle, out Vector3 value);

			// Token: 0x06001586 RID: 5510 RVA: 0x0001B808 File Offset: 0x00019A08
			private static Color32 GetCurrentColor(ParticleSystem system, ref ParticleSystem.Particle particle)
			{
				Color32 result;
				ParticleSystem.Particle.INTERNAL_CALL_GetCurrentColor(system, ref particle, out result);
				return result;
			}

			// Token: 0x06001587 RID: 5511
			[MethodImpl(MethodImplOptions.InternalCall)]
			private static extern void INTERNAL_CALL_GetCurrentColor(ParticleSystem system, ref ParticleSystem.Particle particle, out Color32 value);

			// Token: 0x170004D5 RID: 1237
			// (get) Token: 0x06001588 RID: 5512 RVA: 0x0001B828 File Offset: 0x00019A28
			// (set) Token: 0x06001589 RID: 5513 RVA: 0x0001B848 File Offset: 0x00019A48
			[Obsolete("size property is deprecated. Use startSize or GetCurrentSize() instead.")]
			public float size
			{
				get
				{
					return this.m_StartSize.x;
				}
				set
				{
					this.m_StartSize = new Vector3(value, value, value);
				}
			}

			// Token: 0x170004D6 RID: 1238
			// (get) Token: 0x0600158A RID: 5514 RVA: 0x0001B85C File Offset: 0x00019A5C
			// (set) Token: 0x0600158B RID: 5515 RVA: 0x0001B878 File Offset: 0x00019A78
			[Obsolete("color property is deprecated. Use startColor or GetCurrentColor() instead.")]
			public Color32 color
			{
				get
				{
					return this.m_StartColor;
				}
				set
				{
					this.m_StartColor = value;
				}
			}

			// Token: 0x0400032C RID: 812
			private Vector3 m_Position;

			// Token: 0x0400032D RID: 813
			private Vector3 m_Velocity;

			// Token: 0x0400032E RID: 814
			private Vector3 m_AnimatedVelocity;

			// Token: 0x0400032F RID: 815
			private Vector3 m_InitialVelocity;

			// Token: 0x04000330 RID: 816
			private Vector3 m_AxisOfRotation;

			// Token: 0x04000331 RID: 817
			private Vector3 m_Rotation;

			// Token: 0x04000332 RID: 818
			private Vector3 m_AngularVelocity;

			// Token: 0x04000333 RID: 819
			private Vector3 m_StartSize;

			// Token: 0x04000334 RID: 820
			private Color32 m_StartColor;

			// Token: 0x04000335 RID: 821
			private uint m_RandomSeed;

			// Token: 0x04000336 RID: 822
			private float m_Lifetime;

			// Token: 0x04000337 RID: 823
			private float m_StartLifetime;

			// Token: 0x04000338 RID: 824
			private float m_EmitAccumulator0;

			// Token: 0x04000339 RID: 825
			private float m_EmitAccumulator1;
		}

		// Token: 0x02000129 RID: 297
		public struct EmitParams
		{
			// Token: 0x170004D7 RID: 1239
			// (get) Token: 0x0600158C RID: 5516 RVA: 0x0001B884 File Offset: 0x00019A84
			// (set) Token: 0x0600158D RID: 5517 RVA: 0x0001B8A4 File Offset: 0x00019AA4
			public Vector3 position
			{
				get
				{
					return this.m_Particle.position;
				}
				set
				{
					this.m_Particle.position = value;
					this.m_PositionSet = true;
				}
			}

			// Token: 0x170004D8 RID: 1240
			// (get) Token: 0x0600158E RID: 5518 RVA: 0x0001B8BC File Offset: 0x00019ABC
			// (set) Token: 0x0600158F RID: 5519 RVA: 0x0001B8D8 File Offset: 0x00019AD8
			public bool applyShapeToPosition
			{
				get
				{
					return this.m_ApplyShapeToPosition;
				}
				set
				{
					this.m_ApplyShapeToPosition = value;
				}
			}

			// Token: 0x170004D9 RID: 1241
			// (get) Token: 0x06001590 RID: 5520 RVA: 0x0001B8E4 File Offset: 0x00019AE4
			// (set) Token: 0x06001591 RID: 5521 RVA: 0x0001B904 File Offset: 0x00019B04
			public Vector3 velocity
			{
				get
				{
					return this.m_Particle.velocity;
				}
				set
				{
					this.m_Particle.velocity = value;
					this.m_VelocitySet = true;
				}
			}

			// Token: 0x170004DA RID: 1242
			// (get) Token: 0x06001592 RID: 5522 RVA: 0x0001B91C File Offset: 0x00019B1C
			// (set) Token: 0x06001593 RID: 5523 RVA: 0x0001B93C File Offset: 0x00019B3C
			public float startLifetime
			{
				get
				{
					return this.m_Particle.startLifetime;
				}
				set
				{
					this.m_Particle.startLifetime = value;
					this.m_StartLifetimeSet = true;
				}
			}

			// Token: 0x170004DB RID: 1243
			// (get) Token: 0x06001594 RID: 5524 RVA: 0x0001B954 File Offset: 0x00019B54
			// (set) Token: 0x06001595 RID: 5525 RVA: 0x0001B974 File Offset: 0x00019B74
			public float startSize
			{
				get
				{
					return this.m_Particle.startSize;
				}
				set
				{
					this.m_Particle.startSize = value;
					this.m_StartSizeSet = true;
				}
			}

			// Token: 0x170004DC RID: 1244
			// (get) Token: 0x06001596 RID: 5526 RVA: 0x0001B98C File Offset: 0x00019B8C
			// (set) Token: 0x06001597 RID: 5527 RVA: 0x0001B9AC File Offset: 0x00019BAC
			public Vector3 startSize3D
			{
				get
				{
					return this.m_Particle.startSize3D;
				}
				set
				{
					this.m_Particle.startSize3D = value;
					this.m_StartSizeSet = true;
				}
			}

			// Token: 0x170004DD RID: 1245
			// (get) Token: 0x06001598 RID: 5528 RVA: 0x0001B9C4 File Offset: 0x00019BC4
			// (set) Token: 0x06001599 RID: 5529 RVA: 0x0001B9E4 File Offset: 0x00019BE4
			public Vector3 axisOfRotation
			{
				get
				{
					return this.m_Particle.axisOfRotation;
				}
				set
				{
					this.m_Particle.axisOfRotation = value;
					this.m_AxisOfRotationSet = true;
				}
			}

			// Token: 0x170004DE RID: 1246
			// (get) Token: 0x0600159A RID: 5530 RVA: 0x0001B9FC File Offset: 0x00019BFC
			// (set) Token: 0x0600159B RID: 5531 RVA: 0x0001BA1C File Offset: 0x00019C1C
			public float rotation
			{
				get
				{
					return this.m_Particle.rotation;
				}
				set
				{
					this.m_Particle.rotation = value;
					this.m_RotationSet = true;
				}
			}

			// Token: 0x170004DF RID: 1247
			// (get) Token: 0x0600159C RID: 5532 RVA: 0x0001BA34 File Offset: 0x00019C34
			// (set) Token: 0x0600159D RID: 5533 RVA: 0x0001BA54 File Offset: 0x00019C54
			public Vector3 rotation3D
			{
				get
				{
					return this.m_Particle.rotation3D;
				}
				set
				{
					this.m_Particle.rotation3D = value;
					this.m_RotationSet = true;
				}
			}

			// Token: 0x170004E0 RID: 1248
			// (get) Token: 0x0600159E RID: 5534 RVA: 0x0001BA6C File Offset: 0x00019C6C
			// (set) Token: 0x0600159F RID: 5535 RVA: 0x0001BA8C File Offset: 0x00019C8C
			public float angularVelocity
			{
				get
				{
					return this.m_Particle.angularVelocity;
				}
				set
				{
					this.m_Particle.angularVelocity = value;
					this.m_AngularVelocitySet = true;
				}
			}

			// Token: 0x170004E1 RID: 1249
			// (get) Token: 0x060015A0 RID: 5536 RVA: 0x0001BAA4 File Offset: 0x00019CA4
			// (set) Token: 0x060015A1 RID: 5537 RVA: 0x0001BAC4 File Offset: 0x00019CC4
			public Vector3 angularVelocity3D
			{
				get
				{
					return this.m_Particle.angularVelocity3D;
				}
				set
				{
					this.m_Particle.angularVelocity3D = value;
					this.m_AngularVelocitySet = true;
				}
			}

			// Token: 0x170004E2 RID: 1250
			// (get) Token: 0x060015A2 RID: 5538 RVA: 0x0001BADC File Offset: 0x00019CDC
			// (set) Token: 0x060015A3 RID: 5539 RVA: 0x0001BAFC File Offset: 0x00019CFC
			public Color32 startColor
			{
				get
				{
					return this.m_Particle.startColor;
				}
				set
				{
					this.m_Particle.startColor = value;
					this.m_StartColorSet = true;
				}
			}

			// Token: 0x170004E3 RID: 1251
			// (get) Token: 0x060015A4 RID: 5540 RVA: 0x0001BB14 File Offset: 0x00019D14
			// (set) Token: 0x060015A5 RID: 5541 RVA: 0x0001BB34 File Offset: 0x00019D34
			public uint randomSeed
			{
				get
				{
					return this.m_Particle.randomSeed;
				}
				set
				{
					this.m_Particle.randomSeed = value;
					this.m_RandomSeedSet = true;
				}
			}

			// Token: 0x060015A6 RID: 5542 RVA: 0x0001BB4C File Offset: 0x00019D4C
			public void ResetPosition()
			{
				this.m_PositionSet = false;
			}

			// Token: 0x060015A7 RID: 5543 RVA: 0x0001BB58 File Offset: 0x00019D58
			public void ResetVelocity()
			{
				this.m_VelocitySet = false;
			}

			// Token: 0x060015A8 RID: 5544 RVA: 0x0001BB64 File Offset: 0x00019D64
			public void ResetAxisOfRotation()
			{
				this.m_AxisOfRotationSet = false;
			}

			// Token: 0x060015A9 RID: 5545 RVA: 0x0001BB70 File Offset: 0x00019D70
			public void ResetRotation()
			{
				this.m_RotationSet = false;
			}

			// Token: 0x060015AA RID: 5546 RVA: 0x0001BB7C File Offset: 0x00019D7C
			public void ResetAngularVelocity()
			{
				this.m_AngularVelocitySet = false;
			}

			// Token: 0x060015AB RID: 5547 RVA: 0x0001BB88 File Offset: 0x00019D88
			public void ResetStartSize()
			{
				this.m_StartSizeSet = false;
			}

			// Token: 0x060015AC RID: 5548 RVA: 0x0001BB94 File Offset: 0x00019D94
			public void ResetStartColor()
			{
				this.m_StartColorSet = false;
			}

			// Token: 0x060015AD RID: 5549 RVA: 0x0001BBA0 File Offset: 0x00019DA0
			public void ResetRandomSeed()
			{
				this.m_RandomSeedSet = false;
			}

			// Token: 0x060015AE RID: 5550 RVA: 0x0001BBAC File Offset: 0x00019DAC
			public void ResetStartLifetime()
			{
				this.m_StartLifetimeSet = false;
			}

			// Token: 0x0400033A RID: 826
			internal ParticleSystem.Particle m_Particle;

			// Token: 0x0400033B RID: 827
			internal bool m_PositionSet;

			// Token: 0x0400033C RID: 828
			internal bool m_VelocitySet;

			// Token: 0x0400033D RID: 829
			internal bool m_AxisOfRotationSet;

			// Token: 0x0400033E RID: 830
			internal bool m_RotationSet;

			// Token: 0x0400033F RID: 831
			internal bool m_AngularVelocitySet;

			// Token: 0x04000340 RID: 832
			internal bool m_StartSizeSet;

			// Token: 0x04000341 RID: 833
			internal bool m_StartColorSet;

			// Token: 0x04000342 RID: 834
			internal bool m_RandomSeedSet;

			// Token: 0x04000343 RID: 835
			internal bool m_StartLifetimeSet;

			// Token: 0x04000344 RID: 836
			internal bool m_ApplyShapeToPosition;
		}

		// Token: 0x0200012A RID: 298
		// (Invoke) Token: 0x060015B0 RID: 5552
		internal delegate bool IteratorDelegate(ParticleSystem ps);
	}
}
