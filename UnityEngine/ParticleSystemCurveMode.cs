﻿using System;

namespace UnityEngine
{
	// Token: 0x020000FC RID: 252
	public enum ParticleSystemCurveMode
	{
		// Token: 0x0400029E RID: 670
		Constant,
		// Token: 0x0400029F RID: 671
		Curve,
		// Token: 0x040002A0 RID: 672
		TwoCurves,
		// Token: 0x040002A1 RID: 673
		TwoConstants
	}
}
