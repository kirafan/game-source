﻿using System;
using System.Runtime.CompilerServices;

namespace UnityEngine
{
	// Token: 0x020001AA RID: 426
	public sealed class AudioHighPassFilter : Behaviour
	{
		// Token: 0x1700074D RID: 1869
		// (get) Token: 0x06001DA0 RID: 7584
		// (set) Token: 0x06001DA1 RID: 7585
		public extern float cutoffFrequency { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700074E RID: 1870
		// (get) Token: 0x06001DA2 RID: 7586
		// (set) Token: 0x06001DA3 RID: 7587
		public extern float highpassResonanceQ { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
