﻿using System;

namespace UnityEngine
{
	// Token: 0x020002D9 RID: 729
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = false)]
	public class ConstructorSafeAttribute : Attribute
	{
	}
}
