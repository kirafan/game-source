﻿using System;

namespace UnityEngine
{
	// Token: 0x020000F8 RID: 248
	public enum ParticleSystemSortMode
	{
		// Token: 0x0400028D RID: 653
		None,
		// Token: 0x0400028E RID: 654
		Distance,
		// Token: 0x0400028F RID: 655
		OldestInFront,
		// Token: 0x04000290 RID: 656
		YoungestInFront
	}
}
