﻿using System;
using System.Collections.Generic;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020002DD RID: 733
	[UsedByNativeCode]
	internal struct DiagnosticSwitch
	{
		// Token: 0x06002C8E RID: 11406 RVA: 0x000463E0 File Offset: 0x000445E0
		[UsedByNativeCode]
		private static void AppendDiagnosticSwitchToList(List<DiagnosticSwitch> list, string name, string description, DiagnosticSwitchFlags flags, object value, object minValue, object maxValue, object persistentValue, EnumInfo enumInfo)
		{
			list.Add(new DiagnosticSwitch
			{
				name = name,
				description = description,
				flags = flags,
				value = value,
				minValue = minValue,
				maxValue = maxValue,
				persistentValue = persistentValue,
				enumInfo = enumInfo
			});
		}

		// Token: 0x04000AC2 RID: 2754
		public string name;

		// Token: 0x04000AC3 RID: 2755
		public string description;

		// Token: 0x04000AC4 RID: 2756
		public DiagnosticSwitchFlags flags;

		// Token: 0x04000AC5 RID: 2757
		public object value;

		// Token: 0x04000AC6 RID: 2758
		public object minValue;

		// Token: 0x04000AC7 RID: 2759
		public object maxValue;

		// Token: 0x04000AC8 RID: 2760
		public object persistentValue;

		// Token: 0x04000AC9 RID: 2761
		public EnumInfo enumInfo;
	}
}
