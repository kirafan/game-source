﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x020000D5 RID: 213
	public class Transform : Component, IEnumerable
	{
		// Token: 0x06000EB5 RID: 3765 RVA: 0x00013C04 File Offset: 0x00011E04
		protected Transform()
		{
		}

		// Token: 0x17000335 RID: 821
		// (get) Token: 0x06000EB6 RID: 3766 RVA: 0x00013C10 File Offset: 0x00011E10
		// (set) Token: 0x06000EB7 RID: 3767 RVA: 0x00013C30 File Offset: 0x00011E30
		public Vector3 position
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_position(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_position(ref value);
			}
		}

		// Token: 0x06000EB8 RID: 3768
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_position(out Vector3 value);

		// Token: 0x06000EB9 RID: 3769
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_position(ref Vector3 value);

		// Token: 0x17000336 RID: 822
		// (get) Token: 0x06000EBA RID: 3770 RVA: 0x00013C3C File Offset: 0x00011E3C
		// (set) Token: 0x06000EBB RID: 3771 RVA: 0x00013C5C File Offset: 0x00011E5C
		public Vector3 localPosition
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_localPosition(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localPosition(ref value);
			}
		}

		// Token: 0x06000EBC RID: 3772
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localPosition(out Vector3 value);

		// Token: 0x06000EBD RID: 3773
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localPosition(ref Vector3 value);

		// Token: 0x06000EBE RID: 3774 RVA: 0x00013C68 File Offset: 0x00011E68
		internal Vector3 GetLocalEulerAngles(RotationOrder order)
		{
			Vector3 result;
			Transform.INTERNAL_CALL_GetLocalEulerAngles(this, order, out result);
			return result;
		}

		// Token: 0x06000EBF RID: 3775
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetLocalEulerAngles(Transform self, RotationOrder order, out Vector3 value);

		// Token: 0x06000EC0 RID: 3776 RVA: 0x00013C88 File Offset: 0x00011E88
		internal void SetLocalEulerAngles(Vector3 euler, RotationOrder order)
		{
			Transform.INTERNAL_CALL_SetLocalEulerAngles(this, ref euler, order);
		}

		// Token: 0x06000EC1 RID: 3777
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetLocalEulerAngles(Transform self, ref Vector3 euler, RotationOrder order);

		// Token: 0x17000337 RID: 823
		// (get) Token: 0x06000EC2 RID: 3778 RVA: 0x00013C94 File Offset: 0x00011E94
		// (set) Token: 0x06000EC3 RID: 3779 RVA: 0x00013CB8 File Offset: 0x00011EB8
		public Vector3 eulerAngles
		{
			get
			{
				return this.rotation.eulerAngles;
			}
			set
			{
				this.rotation = Quaternion.Euler(value);
			}
		}

		// Token: 0x17000338 RID: 824
		// (get) Token: 0x06000EC4 RID: 3780 RVA: 0x00013CC8 File Offset: 0x00011EC8
		// (set) Token: 0x06000EC5 RID: 3781 RVA: 0x00013CEC File Offset: 0x00011EEC
		public Vector3 localEulerAngles
		{
			get
			{
				return this.localRotation.eulerAngles;
			}
			set
			{
				this.localRotation = Quaternion.Euler(value);
			}
		}

		// Token: 0x17000339 RID: 825
		// (get) Token: 0x06000EC6 RID: 3782 RVA: 0x00013CFC File Offset: 0x00011EFC
		// (set) Token: 0x06000EC7 RID: 3783 RVA: 0x00013D24 File Offset: 0x00011F24
		public Vector3 right
		{
			get
			{
				return this.rotation * Vector3.right;
			}
			set
			{
				this.rotation = Quaternion.FromToRotation(Vector3.right, value);
			}
		}

		// Token: 0x1700033A RID: 826
		// (get) Token: 0x06000EC8 RID: 3784 RVA: 0x00013D38 File Offset: 0x00011F38
		// (set) Token: 0x06000EC9 RID: 3785 RVA: 0x00013D60 File Offset: 0x00011F60
		public Vector3 up
		{
			get
			{
				return this.rotation * Vector3.up;
			}
			set
			{
				this.rotation = Quaternion.FromToRotation(Vector3.up, value);
			}
		}

		// Token: 0x1700033B RID: 827
		// (get) Token: 0x06000ECA RID: 3786 RVA: 0x00013D74 File Offset: 0x00011F74
		// (set) Token: 0x06000ECB RID: 3787 RVA: 0x00013D9C File Offset: 0x00011F9C
		public Vector3 forward
		{
			get
			{
				return this.rotation * Vector3.forward;
			}
			set
			{
				this.rotation = Quaternion.LookRotation(value);
			}
		}

		// Token: 0x1700033C RID: 828
		// (get) Token: 0x06000ECC RID: 3788 RVA: 0x00013DAC File Offset: 0x00011FAC
		// (set) Token: 0x06000ECD RID: 3789 RVA: 0x00013DCC File Offset: 0x00011FCC
		public Quaternion rotation
		{
			get
			{
				Quaternion result;
				this.INTERNAL_get_rotation(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_rotation(ref value);
			}
		}

		// Token: 0x06000ECE RID: 3790
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_rotation(out Quaternion value);

		// Token: 0x06000ECF RID: 3791
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_rotation(ref Quaternion value);

		// Token: 0x1700033D RID: 829
		// (get) Token: 0x06000ED0 RID: 3792 RVA: 0x00013DD8 File Offset: 0x00011FD8
		// (set) Token: 0x06000ED1 RID: 3793 RVA: 0x00013DF8 File Offset: 0x00011FF8
		public Quaternion localRotation
		{
			get
			{
				Quaternion result;
				this.INTERNAL_get_localRotation(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localRotation(ref value);
			}
		}

		// Token: 0x06000ED2 RID: 3794
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localRotation(out Quaternion value);

		// Token: 0x06000ED3 RID: 3795
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localRotation(ref Quaternion value);

		// Token: 0x1700033E RID: 830
		// (get) Token: 0x06000ED4 RID: 3796 RVA: 0x00013E04 File Offset: 0x00012004
		// (set) Token: 0x06000ED5 RID: 3797 RVA: 0x00013E24 File Offset: 0x00012024
		public Vector3 localScale
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_localScale(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_localScale(ref value);
			}
		}

		// Token: 0x06000ED6 RID: 3798
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localScale(out Vector3 value);

		// Token: 0x06000ED7 RID: 3799
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_localScale(ref Vector3 value);

		// Token: 0x1700033F RID: 831
		// (get) Token: 0x06000ED8 RID: 3800 RVA: 0x00013E30 File Offset: 0x00012030
		// (set) Token: 0x06000ED9 RID: 3801 RVA: 0x00013E4C File Offset: 0x0001204C
		public Transform parent
		{
			get
			{
				return this.parentInternal;
			}
			set
			{
				if (this is RectTransform)
				{
					Debug.LogWarning("Parent of RectTransform is being set with parent property. Consider using the SetParent method instead, with the worldPositionStays argument set to false. This will retain local orientation and scale rather than world orientation and scale, which can prevent common UI scaling issues.", this);
				}
				this.parentInternal = value;
			}
		}

		// Token: 0x17000340 RID: 832
		// (get) Token: 0x06000EDA RID: 3802
		// (set) Token: 0x06000EDB RID: 3803
		internal extern Transform parentInternal { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000EDC RID: 3804 RVA: 0x00013E6C File Offset: 0x0001206C
		public void SetParent(Transform parent)
		{
			this.SetParent(parent, true);
		}

		// Token: 0x06000EDD RID: 3805
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetParent(Transform parent, bool worldPositionStays);

		// Token: 0x17000341 RID: 833
		// (get) Token: 0x06000EDE RID: 3806 RVA: 0x00013E78 File Offset: 0x00012078
		public Matrix4x4 worldToLocalMatrix
		{
			get
			{
				Matrix4x4 result;
				this.INTERNAL_get_worldToLocalMatrix(out result);
				return result;
			}
		}

		// Token: 0x06000EDF RID: 3807
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_worldToLocalMatrix(out Matrix4x4 value);

		// Token: 0x17000342 RID: 834
		// (get) Token: 0x06000EE0 RID: 3808 RVA: 0x00013E98 File Offset: 0x00012098
		public Matrix4x4 localToWorldMatrix
		{
			get
			{
				Matrix4x4 result;
				this.INTERNAL_get_localToWorldMatrix(out result);
				return result;
			}
		}

		// Token: 0x06000EE1 RID: 3809
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_localToWorldMatrix(out Matrix4x4 value);

		// Token: 0x06000EE2 RID: 3810 RVA: 0x00013EB8 File Offset: 0x000120B8
		[ExcludeFromDocs]
		public void Translate(Vector3 translation)
		{
			Space relativeTo = Space.Self;
			this.Translate(translation, relativeTo);
		}

		// Token: 0x06000EE3 RID: 3811 RVA: 0x00013ED0 File Offset: 0x000120D0
		public void Translate(Vector3 translation, [DefaultValue("Space.Self")] Space relativeTo)
		{
			if (relativeTo == Space.World)
			{
				this.position += translation;
			}
			else
			{
				this.position += this.TransformDirection(translation);
			}
		}

		// Token: 0x06000EE4 RID: 3812 RVA: 0x00013F08 File Offset: 0x00012108
		[ExcludeFromDocs]
		public void Translate(float x, float y, float z)
		{
			Space relativeTo = Space.Self;
			this.Translate(x, y, z, relativeTo);
		}

		// Token: 0x06000EE5 RID: 3813 RVA: 0x00013F24 File Offset: 0x00012124
		public void Translate(float x, float y, float z, [DefaultValue("Space.Self")] Space relativeTo)
		{
			this.Translate(new Vector3(x, y, z), relativeTo);
		}

		// Token: 0x06000EE6 RID: 3814 RVA: 0x00013F38 File Offset: 0x00012138
		public void Translate(Vector3 translation, Transform relativeTo)
		{
			if (relativeTo)
			{
				this.position += relativeTo.TransformDirection(translation);
			}
			else
			{
				this.position += translation;
			}
		}

		// Token: 0x06000EE7 RID: 3815 RVA: 0x00013F78 File Offset: 0x00012178
		public void Translate(float x, float y, float z, Transform relativeTo)
		{
			this.Translate(new Vector3(x, y, z), relativeTo);
		}

		// Token: 0x06000EE8 RID: 3816 RVA: 0x00013F8C File Offset: 0x0001218C
		[ExcludeFromDocs]
		public void Rotate(Vector3 eulerAngles)
		{
			Space relativeTo = Space.Self;
			this.Rotate(eulerAngles, relativeTo);
		}

		// Token: 0x06000EE9 RID: 3817 RVA: 0x00013FA4 File Offset: 0x000121A4
		public void Rotate(Vector3 eulerAngles, [DefaultValue("Space.Self")] Space relativeTo)
		{
			Quaternion rhs = Quaternion.Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z);
			if (relativeTo == Space.Self)
			{
				this.localRotation *= rhs;
			}
			else
			{
				this.rotation *= Quaternion.Inverse(this.rotation) * rhs * this.rotation;
			}
		}

		// Token: 0x06000EEA RID: 3818 RVA: 0x0001401C File Offset: 0x0001221C
		[ExcludeFromDocs]
		public void Rotate(float xAngle, float yAngle, float zAngle)
		{
			Space relativeTo = Space.Self;
			this.Rotate(xAngle, yAngle, zAngle, relativeTo);
		}

		// Token: 0x06000EEB RID: 3819 RVA: 0x00014038 File Offset: 0x00012238
		public void Rotate(float xAngle, float yAngle, float zAngle, [DefaultValue("Space.Self")] Space relativeTo)
		{
			this.Rotate(new Vector3(xAngle, yAngle, zAngle), relativeTo);
		}

		// Token: 0x06000EEC RID: 3820 RVA: 0x0001404C File Offset: 0x0001224C
		internal void RotateAroundInternal(Vector3 axis, float angle)
		{
			Transform.INTERNAL_CALL_RotateAroundInternal(this, ref axis, angle);
		}

		// Token: 0x06000EED RID: 3821
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_RotateAroundInternal(Transform self, ref Vector3 axis, float angle);

		// Token: 0x06000EEE RID: 3822 RVA: 0x00014058 File Offset: 0x00012258
		[ExcludeFromDocs]
		public void Rotate(Vector3 axis, float angle)
		{
			Space relativeTo = Space.Self;
			this.Rotate(axis, angle, relativeTo);
		}

		// Token: 0x06000EEF RID: 3823 RVA: 0x00014074 File Offset: 0x00012274
		public void Rotate(Vector3 axis, float angle, [DefaultValue("Space.Self")] Space relativeTo)
		{
			if (relativeTo == Space.Self)
			{
				this.RotateAroundInternal(base.transform.TransformDirection(axis), angle * 0.017453292f);
			}
			else
			{
				this.RotateAroundInternal(axis, angle * 0.017453292f);
			}
		}

		// Token: 0x06000EF0 RID: 3824 RVA: 0x000140AC File Offset: 0x000122AC
		public void RotateAround(Vector3 point, Vector3 axis, float angle)
		{
			Vector3 vector = this.position;
			Quaternion rotation = Quaternion.AngleAxis(angle, axis);
			Vector3 vector2 = vector - point;
			vector2 = rotation * vector2;
			vector = point + vector2;
			this.position = vector;
			this.RotateAroundInternal(axis, angle * 0.017453292f);
		}

		// Token: 0x06000EF1 RID: 3825 RVA: 0x000140F8 File Offset: 0x000122F8
		[ExcludeFromDocs]
		public void LookAt(Transform target)
		{
			Vector3 up = Vector3.up;
			this.LookAt(target, up);
		}

		// Token: 0x06000EF2 RID: 3826 RVA: 0x00014114 File Offset: 0x00012314
		public void LookAt(Transform target, [DefaultValue("Vector3.up")] Vector3 worldUp)
		{
			if (target)
			{
				this.LookAt(target.position, worldUp);
			}
		}

		// Token: 0x06000EF3 RID: 3827 RVA: 0x00014130 File Offset: 0x00012330
		public void LookAt(Vector3 worldPosition, [DefaultValue("Vector3.up")] Vector3 worldUp)
		{
			Transform.INTERNAL_CALL_LookAt(this, ref worldPosition, ref worldUp);
		}

		// Token: 0x06000EF4 RID: 3828 RVA: 0x00014140 File Offset: 0x00012340
		[ExcludeFromDocs]
		public void LookAt(Vector3 worldPosition)
		{
			Vector3 up = Vector3.up;
			Transform.INTERNAL_CALL_LookAt(this, ref worldPosition, ref up);
		}

		// Token: 0x06000EF5 RID: 3829
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_LookAt(Transform self, ref Vector3 worldPosition, ref Vector3 worldUp);

		// Token: 0x06000EF6 RID: 3830 RVA: 0x00014160 File Offset: 0x00012360
		public Vector3 TransformDirection(Vector3 direction)
		{
			Vector3 result;
			Transform.INTERNAL_CALL_TransformDirection(this, ref direction, out result);
			return result;
		}

		// Token: 0x06000EF7 RID: 3831
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_TransformDirection(Transform self, ref Vector3 direction, out Vector3 value);

		// Token: 0x06000EF8 RID: 3832 RVA: 0x00014180 File Offset: 0x00012380
		public Vector3 TransformDirection(float x, float y, float z)
		{
			return this.TransformDirection(new Vector3(x, y, z));
		}

		// Token: 0x06000EF9 RID: 3833 RVA: 0x000141A4 File Offset: 0x000123A4
		public Vector3 InverseTransformDirection(Vector3 direction)
		{
			Vector3 result;
			Transform.INTERNAL_CALL_InverseTransformDirection(this, ref direction, out result);
			return result;
		}

		// Token: 0x06000EFA RID: 3834
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_InverseTransformDirection(Transform self, ref Vector3 direction, out Vector3 value);

		// Token: 0x06000EFB RID: 3835 RVA: 0x000141C4 File Offset: 0x000123C4
		public Vector3 InverseTransformDirection(float x, float y, float z)
		{
			return this.InverseTransformDirection(new Vector3(x, y, z));
		}

		// Token: 0x06000EFC RID: 3836 RVA: 0x000141E8 File Offset: 0x000123E8
		public Vector3 TransformVector(Vector3 vector)
		{
			Vector3 result;
			Transform.INTERNAL_CALL_TransformVector(this, ref vector, out result);
			return result;
		}

		// Token: 0x06000EFD RID: 3837
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_TransformVector(Transform self, ref Vector3 vector, out Vector3 value);

		// Token: 0x06000EFE RID: 3838 RVA: 0x00014208 File Offset: 0x00012408
		public Vector3 TransformVector(float x, float y, float z)
		{
			return this.TransformVector(new Vector3(x, y, z));
		}

		// Token: 0x06000EFF RID: 3839 RVA: 0x0001422C File Offset: 0x0001242C
		public Vector3 InverseTransformVector(Vector3 vector)
		{
			Vector3 result;
			Transform.INTERNAL_CALL_InverseTransformVector(this, ref vector, out result);
			return result;
		}

		// Token: 0x06000F00 RID: 3840
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_InverseTransformVector(Transform self, ref Vector3 vector, out Vector3 value);

		// Token: 0x06000F01 RID: 3841 RVA: 0x0001424C File Offset: 0x0001244C
		public Vector3 InverseTransformVector(float x, float y, float z)
		{
			return this.InverseTransformVector(new Vector3(x, y, z));
		}

		// Token: 0x06000F02 RID: 3842 RVA: 0x00014270 File Offset: 0x00012470
		public Vector3 TransformPoint(Vector3 position)
		{
			Vector3 result;
			Transform.INTERNAL_CALL_TransformPoint(this, ref position, out result);
			return result;
		}

		// Token: 0x06000F03 RID: 3843
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_TransformPoint(Transform self, ref Vector3 position, out Vector3 value);

		// Token: 0x06000F04 RID: 3844 RVA: 0x00014290 File Offset: 0x00012490
		public Vector3 TransformPoint(float x, float y, float z)
		{
			return this.TransformPoint(new Vector3(x, y, z));
		}

		// Token: 0x06000F05 RID: 3845 RVA: 0x000142B4 File Offset: 0x000124B4
		public Vector3 InverseTransformPoint(Vector3 position)
		{
			Vector3 result;
			Transform.INTERNAL_CALL_InverseTransformPoint(this, ref position, out result);
			return result;
		}

		// Token: 0x06000F06 RID: 3846
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_InverseTransformPoint(Transform self, ref Vector3 position, out Vector3 value);

		// Token: 0x06000F07 RID: 3847 RVA: 0x000142D4 File Offset: 0x000124D4
		public Vector3 InverseTransformPoint(float x, float y, float z)
		{
			return this.InverseTransformPoint(new Vector3(x, y, z));
		}

		// Token: 0x17000343 RID: 835
		// (get) Token: 0x06000F08 RID: 3848
		public extern Transform root { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x06000F09 RID: 3849
		public extern int childCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000F0A RID: 3850
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void DetachChildren();

		// Token: 0x06000F0B RID: 3851
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetAsFirstSibling();

		// Token: 0x06000F0C RID: 3852
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetAsLastSibling();

		// Token: 0x06000F0D RID: 3853
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetSiblingIndex(int index);

		// Token: 0x06000F0E RID: 3854
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetSiblingIndex();

		// Token: 0x06000F0F RID: 3855
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Transform Find(string name);

		// Token: 0x17000345 RID: 837
		// (get) Token: 0x06000F10 RID: 3856 RVA: 0x000142F8 File Offset: 0x000124F8
		public Vector3 lossyScale
		{
			get
			{
				Vector3 result;
				this.INTERNAL_get_lossyScale(out result);
				return result;
			}
		}

		// Token: 0x06000F11 RID: 3857
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_lossyScale(out Vector3 value);

		// Token: 0x06000F12 RID: 3858
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool IsChildOf(Transform parent);

		// Token: 0x17000346 RID: 838
		// (get) Token: 0x06000F13 RID: 3859
		// (set) Token: 0x06000F14 RID: 3860
		public extern bool hasChanged { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000F15 RID: 3861 RVA: 0x00014318 File Offset: 0x00012518
		public Transform FindChild(string name)
		{
			return this.Find(name);
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x00014334 File Offset: 0x00012534
		public IEnumerator GetEnumerator()
		{
			return new Transform.Enumerator(this);
		}

		// Token: 0x06000F17 RID: 3863 RVA: 0x00014350 File Offset: 0x00012550
		[Obsolete("use Transform.Rotate instead.")]
		public void RotateAround(Vector3 axis, float angle)
		{
			Transform.INTERNAL_CALL_RotateAround(this, ref axis, angle);
		}

		// Token: 0x06000F18 RID: 3864
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_RotateAround(Transform self, ref Vector3 axis, float angle);

		// Token: 0x06000F19 RID: 3865 RVA: 0x0001435C File Offset: 0x0001255C
		[Obsolete("use Transform.Rotate instead.")]
		public void RotateAroundLocal(Vector3 axis, float angle)
		{
			Transform.INTERNAL_CALL_RotateAroundLocal(this, ref axis, angle);
		}

		// Token: 0x06000F1A RID: 3866
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_RotateAroundLocal(Transform self, ref Vector3 axis, float angle);

		// Token: 0x06000F1B RID: 3867
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern Transform GetChild(int index);

		// Token: 0x06000F1C RID: 3868
		[Obsolete("use Transform.childCount instead.")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetChildCount();

		// Token: 0x17000347 RID: 839
		// (get) Token: 0x06000F1D RID: 3869
		// (set) Token: 0x06000F1E RID: 3870
		public extern int hierarchyCapacity { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000348 RID: 840
		// (get) Token: 0x06000F1F RID: 3871
		public extern int hierarchyCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x020000D6 RID: 214
		private sealed class Enumerator : IEnumerator
		{
			// Token: 0x06000F20 RID: 3872 RVA: 0x00014368 File Offset: 0x00012568
			internal Enumerator(Transform outer)
			{
				this.outer = outer;
			}

			// Token: 0x17000349 RID: 841
			// (get) Token: 0x06000F21 RID: 3873 RVA: 0x00014380 File Offset: 0x00012580
			public object Current
			{
				get
				{
					return this.outer.GetChild(this.currentIndex);
				}
			}

			// Token: 0x06000F22 RID: 3874 RVA: 0x000143A8 File Offset: 0x000125A8
			public bool MoveNext()
			{
				int childCount = this.outer.childCount;
				return ++this.currentIndex < childCount;
			}

			// Token: 0x06000F23 RID: 3875 RVA: 0x000143E0 File Offset: 0x000125E0
			public void Reset()
			{
				this.currentIndex = -1;
			}

			// Token: 0x04000211 RID: 529
			private Transform outer;

			// Token: 0x04000212 RID: 530
			private int currentIndex = -1;
		}
	}
}
