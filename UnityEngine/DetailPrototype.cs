﻿using System;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020001F1 RID: 497
	[UsedByNativeCode]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class DetailPrototype
	{
		// Token: 0x17000840 RID: 2112
		// (get) Token: 0x060021A0 RID: 8608 RVA: 0x00027044 File Offset: 0x00025244
		// (set) Token: 0x060021A1 RID: 8609 RVA: 0x00027060 File Offset: 0x00025260
		public GameObject prototype
		{
			get
			{
				return this.m_Prototype;
			}
			set
			{
				this.m_Prototype = value;
			}
		}

		// Token: 0x17000841 RID: 2113
		// (get) Token: 0x060021A2 RID: 8610 RVA: 0x0002706C File Offset: 0x0002526C
		// (set) Token: 0x060021A3 RID: 8611 RVA: 0x00027088 File Offset: 0x00025288
		public Texture2D prototypeTexture
		{
			get
			{
				return this.m_PrototypeTexture;
			}
			set
			{
				this.m_PrototypeTexture = value;
			}
		}

		// Token: 0x17000842 RID: 2114
		// (get) Token: 0x060021A4 RID: 8612 RVA: 0x00027094 File Offset: 0x00025294
		// (set) Token: 0x060021A5 RID: 8613 RVA: 0x000270B0 File Offset: 0x000252B0
		public float minWidth
		{
			get
			{
				return this.m_MinWidth;
			}
			set
			{
				this.m_MinWidth = value;
			}
		}

		// Token: 0x17000843 RID: 2115
		// (get) Token: 0x060021A6 RID: 8614 RVA: 0x000270BC File Offset: 0x000252BC
		// (set) Token: 0x060021A7 RID: 8615 RVA: 0x000270D8 File Offset: 0x000252D8
		public float maxWidth
		{
			get
			{
				return this.m_MaxWidth;
			}
			set
			{
				this.m_MaxWidth = value;
			}
		}

		// Token: 0x17000844 RID: 2116
		// (get) Token: 0x060021A8 RID: 8616 RVA: 0x000270E4 File Offset: 0x000252E4
		// (set) Token: 0x060021A9 RID: 8617 RVA: 0x00027100 File Offset: 0x00025300
		public float minHeight
		{
			get
			{
				return this.m_MinHeight;
			}
			set
			{
				this.m_MinHeight = value;
			}
		}

		// Token: 0x17000845 RID: 2117
		// (get) Token: 0x060021AA RID: 8618 RVA: 0x0002710C File Offset: 0x0002530C
		// (set) Token: 0x060021AB RID: 8619 RVA: 0x00027128 File Offset: 0x00025328
		public float maxHeight
		{
			get
			{
				return this.m_MaxHeight;
			}
			set
			{
				this.m_MaxHeight = value;
			}
		}

		// Token: 0x17000846 RID: 2118
		// (get) Token: 0x060021AC RID: 8620 RVA: 0x00027134 File Offset: 0x00025334
		// (set) Token: 0x060021AD RID: 8621 RVA: 0x00027150 File Offset: 0x00025350
		public float noiseSpread
		{
			get
			{
				return this.m_NoiseSpread;
			}
			set
			{
				this.m_NoiseSpread = value;
			}
		}

		// Token: 0x17000847 RID: 2119
		// (get) Token: 0x060021AE RID: 8622 RVA: 0x0002715C File Offset: 0x0002535C
		// (set) Token: 0x060021AF RID: 8623 RVA: 0x00027178 File Offset: 0x00025378
		public float bendFactor
		{
			get
			{
				return this.m_BendFactor;
			}
			set
			{
				this.m_BendFactor = value;
			}
		}

		// Token: 0x17000848 RID: 2120
		// (get) Token: 0x060021B0 RID: 8624 RVA: 0x00027184 File Offset: 0x00025384
		// (set) Token: 0x060021B1 RID: 8625 RVA: 0x000271A0 File Offset: 0x000253A0
		public Color healthyColor
		{
			get
			{
				return this.m_HealthyColor;
			}
			set
			{
				this.m_HealthyColor = value;
			}
		}

		// Token: 0x17000849 RID: 2121
		// (get) Token: 0x060021B2 RID: 8626 RVA: 0x000271AC File Offset: 0x000253AC
		// (set) Token: 0x060021B3 RID: 8627 RVA: 0x000271C8 File Offset: 0x000253C8
		public Color dryColor
		{
			get
			{
				return this.m_DryColor;
			}
			set
			{
				this.m_DryColor = value;
			}
		}

		// Token: 0x1700084A RID: 2122
		// (get) Token: 0x060021B4 RID: 8628 RVA: 0x000271D4 File Offset: 0x000253D4
		// (set) Token: 0x060021B5 RID: 8629 RVA: 0x000271F0 File Offset: 0x000253F0
		public DetailRenderMode renderMode
		{
			get
			{
				return (DetailRenderMode)this.m_RenderMode;
			}
			set
			{
				this.m_RenderMode = (int)value;
			}
		}

		// Token: 0x1700084B RID: 2123
		// (get) Token: 0x060021B6 RID: 8630 RVA: 0x000271FC File Offset: 0x000253FC
		// (set) Token: 0x060021B7 RID: 8631 RVA: 0x00027220 File Offset: 0x00025420
		public bool usePrototypeMesh
		{
			get
			{
				return this.m_UsePrototypeMesh != 0;
			}
			set
			{
				this.m_UsePrototypeMesh = ((!value) ? 0 : 1);
			}
		}

		// Token: 0x040005BC RID: 1468
		private GameObject m_Prototype = null;

		// Token: 0x040005BD RID: 1469
		private Texture2D m_PrototypeTexture = null;

		// Token: 0x040005BE RID: 1470
		private Color m_HealthyColor = new Color(0.2627451f, 0.9764706f, 0.16470589f, 1f);

		// Token: 0x040005BF RID: 1471
		private Color m_DryColor = new Color(0.8039216f, 0.7372549f, 0.101960786f, 1f);

		// Token: 0x040005C0 RID: 1472
		private float m_MinWidth = 1f;

		// Token: 0x040005C1 RID: 1473
		private float m_MaxWidth = 2f;

		// Token: 0x040005C2 RID: 1474
		private float m_MinHeight = 1f;

		// Token: 0x040005C3 RID: 1475
		private float m_MaxHeight = 2f;

		// Token: 0x040005C4 RID: 1476
		private float m_NoiseSpread = 0.1f;

		// Token: 0x040005C5 RID: 1477
		private float m_BendFactor = 0.1f;

		// Token: 0x040005C6 RID: 1478
		private int m_RenderMode = 2;

		// Token: 0x040005C7 RID: 1479
		private int m_UsePrototypeMesh = 0;
	}
}
