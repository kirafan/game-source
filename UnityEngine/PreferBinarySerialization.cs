﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x0200036C RID: 876
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class PreferBinarySerialization : Attribute
	{
	}
}
