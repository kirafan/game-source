﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000205 RID: 517
	public sealed class Font : Object
	{
		// Token: 0x06002301 RID: 8961 RVA: 0x00028AA4 File Offset: 0x00026CA4
		public Font()
		{
			Font.Internal_CreateFont(this, null);
		}

		// Token: 0x06002302 RID: 8962 RVA: 0x00028AB4 File Offset: 0x00026CB4
		public Font(string name)
		{
			Font.Internal_CreateFont(this, name);
		}

		// Token: 0x06002303 RID: 8963 RVA: 0x00028AC4 File Offset: 0x00026CC4
		private Font(string[] names, int size)
		{
			Font.Internal_CreateDynamicFont(this, names, size);
		}

		// Token: 0x06002304 RID: 8964
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string[] GetOSInstalledFontNames();

		// Token: 0x06002305 RID: 8965
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateFont([Writable] Font _font, string name);

		// Token: 0x06002306 RID: 8966
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CreateDynamicFont([Writable] Font _font, string[] _names, int size);

		// Token: 0x06002307 RID: 8967 RVA: 0x00028AD8 File Offset: 0x00026CD8
		public static Font CreateDynamicFontFromOSFont(string fontname, int size)
		{
			return new Font(new string[]
			{
				fontname
			}, size);
		}

		// Token: 0x06002308 RID: 8968 RVA: 0x00028B00 File Offset: 0x00026D00
		public static Font CreateDynamicFontFromOSFont(string[] fontnames, int size)
		{
			return new Font(fontnames, size);
		}

		// Token: 0x170008C0 RID: 2240
		// (get) Token: 0x06002309 RID: 8969
		// (set) Token: 0x0600230A RID: 8970
		public extern Material material { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600230B RID: 8971
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool HasCharacter(char c);

		// Token: 0x170008C1 RID: 2241
		// (get) Token: 0x0600230C RID: 8972
		// (set) Token: 0x0600230D RID: 8973
		public extern string[] fontNames { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170008C2 RID: 2242
		// (get) Token: 0x0600230E RID: 8974
		// (set) Token: 0x0600230F RID: 8975
		public extern CharacterInfo[] characterInfo { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002310 RID: 8976
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void RequestCharactersInTexture(string characters, [UnityEngine.Internal.DefaultValue("0")] int size, [UnityEngine.Internal.DefaultValue("FontStyle.Normal")] FontStyle style);

		// Token: 0x06002311 RID: 8977 RVA: 0x00028B20 File Offset: 0x00026D20
		[ExcludeFromDocs]
		public void RequestCharactersInTexture(string characters, int size)
		{
			FontStyle style = FontStyle.Normal;
			this.RequestCharactersInTexture(characters, size, style);
		}

		// Token: 0x06002312 RID: 8978 RVA: 0x00028B3C File Offset: 0x00026D3C
		[ExcludeFromDocs]
		public void RequestCharactersInTexture(string characters)
		{
			FontStyle style = FontStyle.Normal;
			int size = 0;
			this.RequestCharactersInTexture(characters, size, style);
		}

		// Token: 0x1400000B RID: 11
		// (add) Token: 0x06002313 RID: 8979 RVA: 0x00028B58 File Offset: 0x00026D58
		// (remove) Token: 0x06002314 RID: 8980 RVA: 0x00028B8C File Offset: 0x00026D8C
		public static event Action<Font> textureRebuilt;

		// Token: 0x06002315 RID: 8981 RVA: 0x00028BC0 File Offset: 0x00026DC0
		[RequiredByNativeCode]
		private static void InvokeTextureRebuilt_Internal(Font font)
		{
			Action<Font> action = Font.textureRebuilt;
			if (action != null)
			{
				action(font);
			}
			if (font.m_FontTextureRebuildCallback != null)
			{
				font.m_FontTextureRebuildCallback();
			}
		}

		// Token: 0x1400000C RID: 12
		// (add) Token: 0x06002316 RID: 8982 RVA: 0x00028BF8 File Offset: 0x00026DF8
		// (remove) Token: 0x06002317 RID: 8983 RVA: 0x00028C30 File Offset: 0x00026E30
		private event Font.FontTextureRebuildCallback m_FontTextureRebuildCallback;

		// Token: 0x170008C3 RID: 2243
		// (get) Token: 0x06002318 RID: 8984 RVA: 0x00028C68 File Offset: 0x00026E68
		// (set) Token: 0x06002319 RID: 8985 RVA: 0x00028C84 File Offset: 0x00026E84
		[EditorBrowsable(EditorBrowsableState.Never)]
		[Obsolete("Font.textureRebuildCallback has been deprecated. Use Font.textureRebuilt instead.")]
		public Font.FontTextureRebuildCallback textureRebuildCallback
		{
			get
			{
				return this.m_FontTextureRebuildCallback;
			}
			set
			{
				this.m_FontTextureRebuildCallback = value;
			}
		}

		// Token: 0x0600231A RID: 8986 RVA: 0x00028C90 File Offset: 0x00026E90
		public static int GetMaxVertsForString(string str)
		{
			return str.Length * 4 + 4;
		}

		// Token: 0x0600231B RID: 8987
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern bool GetCharacterInfo(char ch, out CharacterInfo info, [UnityEngine.Internal.DefaultValue("0")] int size, [UnityEngine.Internal.DefaultValue("FontStyle.Normal")] FontStyle style);

		// Token: 0x0600231C RID: 8988 RVA: 0x00028CB0 File Offset: 0x00026EB0
		[ExcludeFromDocs]
		public bool GetCharacterInfo(char ch, out CharacterInfo info, int size)
		{
			FontStyle style = FontStyle.Normal;
			return this.GetCharacterInfo(ch, out info, size, style);
		}

		// Token: 0x0600231D RID: 8989 RVA: 0x00028CD4 File Offset: 0x00026ED4
		[ExcludeFromDocs]
		public bool GetCharacterInfo(char ch, out CharacterInfo info)
		{
			FontStyle style = FontStyle.Normal;
			int size = 0;
			return this.GetCharacterInfo(ch, out info, size, style);
		}

		// Token: 0x170008C4 RID: 2244
		// (get) Token: 0x0600231E RID: 8990
		public extern bool dynamic { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008C5 RID: 2245
		// (get) Token: 0x0600231F RID: 8991
		public extern int ascent { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008C6 RID: 2246
		// (get) Token: 0x06002320 RID: 8992
		public extern int lineHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170008C7 RID: 2247
		// (get) Token: 0x06002321 RID: 8993
		public extern int fontSize { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x02000206 RID: 518
		// (Invoke) Token: 0x06002323 RID: 8995
		[EditorBrowsable(EditorBrowsableState.Never)]
		public delegate void FontTextureRebuildCallback();
	}
}
