﻿using System;

namespace UnityEngine
{
	// Token: 0x02000069 RID: 105
	public enum AndroidActivityIndicatorStyle
	{
		// Token: 0x040000A6 RID: 166
		DontShow = -1,
		// Token: 0x040000A7 RID: 167
		Large,
		// Token: 0x040000A8 RID: 168
		InversedLarge,
		// Token: 0x040000A9 RID: 169
		Small,
		// Token: 0x040000AA RID: 170
		InversedSmall
	}
}
