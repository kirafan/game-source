﻿using System;

namespace UnityEngine
{
	// Token: 0x0200022B RID: 555
	internal sealed class GUIAspectSizer : GUILayoutEntry
	{
		// Token: 0x06002592 RID: 9618 RVA: 0x000318FC File Offset: 0x0002FAFC
		public GUIAspectSizer(float aspect, GUILayoutOption[] options) : base(0f, 0f, 0f, 0f, GUIStyle.none)
		{
			this.aspect = aspect;
			this.ApplyOptions(options);
		}

		// Token: 0x06002593 RID: 9619 RVA: 0x0003192C File Offset: 0x0002FB2C
		public override void CalcHeight()
		{
			this.minHeight = (this.maxHeight = this.rect.width / this.aspect);
		}

		// Token: 0x04000815 RID: 2069
		private float aspect;
	}
}
