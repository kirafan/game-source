﻿using System;

namespace UnityEngine
{
	// Token: 0x02000395 RID: 917
	public class WaitForSecondsRealtime : CustomYieldInstruction
	{
		// Token: 0x06002F6D RID: 12141 RVA: 0x0004DE88 File Offset: 0x0004C088
		public WaitForSecondsRealtime(float time)
		{
			this.waitTime = Time.realtimeSinceStartup + time;
		}

		// Token: 0x17000B16 RID: 2838
		// (get) Token: 0x06002F6E RID: 12142 RVA: 0x0004DEA0 File Offset: 0x0004C0A0
		public override bool keepWaiting
		{
			get
			{
				return Time.realtimeSinceStartup < this.waitTime;
			}
		}

		// Token: 0x04000D93 RID: 3475
		private float waitTime;
	}
}
