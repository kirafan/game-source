﻿using System;

namespace UnityEngine
{
	// Token: 0x020002BA RID: 698
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public sealed class DisallowMultipleComponent : Attribute
	{
	}
}
