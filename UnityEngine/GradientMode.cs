﻿using System;

namespace UnityEngine
{
	// Token: 0x0200003D RID: 61
	public enum GradientMode
	{
		// Token: 0x0400005E RID: 94
		Blend,
		// Token: 0x0400005F RID: 95
		Fixed
	}
}
