﻿using System;

namespace UnityEngine
{
	// Token: 0x02000090 RID: 144
	public enum ConnectionTesterStatus
	{
		// Token: 0x04000132 RID: 306
		Error = -2,
		// Token: 0x04000133 RID: 307
		Undetermined,
		// Token: 0x04000134 RID: 308
		[Obsolete("No longer returned, use newer connection tester enums instead.")]
		PrivateIPNoNATPunchthrough,
		// Token: 0x04000135 RID: 309
		[Obsolete("No longer returned, use newer connection tester enums instead.")]
		PrivateIPHasNATPunchThrough,
		// Token: 0x04000136 RID: 310
		PublicIPIsConnectable,
		// Token: 0x04000137 RID: 311
		PublicIPPortBlocked,
		// Token: 0x04000138 RID: 312
		PublicIPNoServerStarted,
		// Token: 0x04000139 RID: 313
		LimitedNATPunchthroughPortRestricted,
		// Token: 0x0400013A RID: 314
		LimitedNATPunchthroughSymmetric,
		// Token: 0x0400013B RID: 315
		NATpunchthroughFullCone,
		// Token: 0x0400013C RID: 316
		NATpunchthroughAddressRestrictedCone
	}
}
