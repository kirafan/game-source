﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Internal;

namespace UnityEngine
{
	// Token: 0x0200009E RID: 158
	public sealed class MasterServer
	{
		// Token: 0x17000289 RID: 649
		// (get) Token: 0x06000B14 RID: 2836
		// (set) Token: 0x06000B15 RID: 2837
		public static extern string ipAddress { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700028A RID: 650
		// (get) Token: 0x06000B16 RID: 2838
		// (set) Token: 0x06000B17 RID: 2839
		public static extern int port { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06000B18 RID: 2840
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void RequestHostList(string gameTypeName);

		// Token: 0x06000B19 RID: 2841
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern HostData[] PollHostList();

		// Token: 0x06000B1A RID: 2842
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void RegisterHost(string gameTypeName, string gameName, [DefaultValue("\"\"")] string comment);

		// Token: 0x06000B1B RID: 2843 RVA: 0x0000FAA8 File Offset: 0x0000DCA8
		[ExcludeFromDocs]
		public static void RegisterHost(string gameTypeName, string gameName)
		{
			string comment = "";
			MasterServer.RegisterHost(gameTypeName, gameName, comment);
		}

		// Token: 0x06000B1C RID: 2844
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void UnregisterHost();

		// Token: 0x06000B1D RID: 2845
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ClearHostList();

		// Token: 0x1700028B RID: 651
		// (get) Token: 0x06000B1E RID: 2846
		// (set) Token: 0x06000B1F RID: 2847
		public static extern int updateRate { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700028C RID: 652
		// (get) Token: 0x06000B20 RID: 2848
		// (set) Token: 0x06000B21 RID: 2849
		public static extern bool dedicatedServer { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }
	}
}
