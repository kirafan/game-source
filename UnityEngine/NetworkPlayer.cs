﻿using System;
using System.Runtime.CompilerServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000097 RID: 151
	[RequiredByNativeCode]
	public struct NetworkPlayer
	{
		// Token: 0x06000A49 RID: 2633 RVA: 0x0000EF64 File Offset: 0x0000D164
		public NetworkPlayer(string ip, int port)
		{
			Debug.LogError("Not yet implemented");
			this.index = 0;
		}

		// Token: 0x06000A4A RID: 2634
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string Internal_GetIPAddress(int index);

		// Token: 0x06000A4B RID: 2635
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetPort(int index);

		// Token: 0x06000A4C RID: 2636
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string Internal_GetExternalIP();

		// Token: 0x06000A4D RID: 2637
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetExternalPort();

		// Token: 0x06000A4E RID: 2638
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string Internal_GetLocalIP();

		// Token: 0x06000A4F RID: 2639
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetLocalPort();

		// Token: 0x06000A50 RID: 2640
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int Internal_GetPlayerIndex();

		// Token: 0x06000A51 RID: 2641
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string Internal_GetGUID(int index);

		// Token: 0x06000A52 RID: 2642
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string Internal_GetLocalGUID();

		// Token: 0x06000A53 RID: 2643 RVA: 0x0000EF78 File Offset: 0x0000D178
		public static bool operator ==(NetworkPlayer lhs, NetworkPlayer rhs)
		{
			return lhs.index == rhs.index;
		}

		// Token: 0x06000A54 RID: 2644 RVA: 0x0000EFA0 File Offset: 0x0000D1A0
		public static bool operator !=(NetworkPlayer lhs, NetworkPlayer rhs)
		{
			return lhs.index != rhs.index;
		}

		// Token: 0x06000A55 RID: 2645 RVA: 0x0000EFC8 File Offset: 0x0000D1C8
		public override int GetHashCode()
		{
			return this.index.GetHashCode();
		}

		// Token: 0x06000A56 RID: 2646 RVA: 0x0000EFF0 File Offset: 0x0000D1F0
		public override bool Equals(object other)
		{
			return other is NetworkPlayer && ((NetworkPlayer)other).index == this.index;
		}

		// Token: 0x17000259 RID: 601
		// (get) Token: 0x06000A57 RID: 2647 RVA: 0x0000F030 File Offset: 0x0000D230
		public string ipAddress
		{
			get
			{
				string result;
				if (this.index == NetworkPlayer.Internal_GetPlayerIndex())
				{
					result = NetworkPlayer.Internal_GetLocalIP();
				}
				else
				{
					result = NetworkPlayer.Internal_GetIPAddress(this.index);
				}
				return result;
			}
		}

		// Token: 0x1700025A RID: 602
		// (get) Token: 0x06000A58 RID: 2648 RVA: 0x0000F06C File Offset: 0x0000D26C
		public int port
		{
			get
			{
				int result;
				if (this.index == NetworkPlayer.Internal_GetPlayerIndex())
				{
					result = NetworkPlayer.Internal_GetLocalPort();
				}
				else
				{
					result = NetworkPlayer.Internal_GetPort(this.index);
				}
				return result;
			}
		}

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x06000A59 RID: 2649 RVA: 0x0000F0A8 File Offset: 0x0000D2A8
		public string guid
		{
			get
			{
				string result;
				if (this.index == NetworkPlayer.Internal_GetPlayerIndex())
				{
					result = NetworkPlayer.Internal_GetLocalGUID();
				}
				else
				{
					result = NetworkPlayer.Internal_GetGUID(this.index);
				}
				return result;
			}
		}

		// Token: 0x06000A5A RID: 2650 RVA: 0x0000F0E4 File Offset: 0x0000D2E4
		public override string ToString()
		{
			return this.index.ToString();
		}

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x06000A5B RID: 2651 RVA: 0x0000F10C File Offset: 0x0000D30C
		public string externalIP
		{
			get
			{
				return NetworkPlayer.Internal_GetExternalIP();
			}
		}

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x06000A5C RID: 2652 RVA: 0x0000F128 File Offset: 0x0000D328
		public int externalPort
		{
			get
			{
				return NetworkPlayer.Internal_GetExternalPort();
			}
		}

		// Token: 0x1700025E RID: 606
		// (get) Token: 0x06000A5D RID: 2653 RVA: 0x0000F144 File Offset: 0x0000D344
		internal static NetworkPlayer unassigned
		{
			get
			{
				NetworkPlayer result;
				result.index = -1;
				return result;
			}
		}

		// Token: 0x04000163 RID: 355
		internal int index;
	}
}
