﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x02000237 RID: 567
	[RequiredByNativeCode]
	[Serializable]
	[StructLayout(LayoutKind.Sequential)]
	public sealed class GUIStyle
	{
		// Token: 0x0600261F RID: 9759 RVA: 0x00033B30 File Offset: 0x00031D30
		public GUIStyle()
		{
			this.Init();
		}

		// Token: 0x06002620 RID: 9760 RVA: 0x00033B40 File Offset: 0x00031D40
		public GUIStyle(GUIStyle other)
		{
			this.InitCopy(other);
		}

		// Token: 0x06002621 RID: 9761 RVA: 0x00033B50 File Offset: 0x00031D50
		~GUIStyle()
		{
			this.Cleanup();
		}

		// Token: 0x06002622 RID: 9762 RVA: 0x00033B80 File Offset: 0x00031D80
		internal static void CleanupRoots()
		{
			GUIStyle.s_None = null;
		}

		// Token: 0x06002623 RID: 9763 RVA: 0x00033B8C File Offset: 0x00031D8C
		internal void InternalOnAfterDeserialize()
		{
			this.m_FontInternal = this.GetFontInternalDuringLoadingThread();
			this.m_Normal = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(0));
			this.m_Hover = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(1));
			this.m_Active = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(2));
			this.m_Focused = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(3));
			this.m_OnNormal = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(4));
			this.m_OnHover = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(5));
			this.m_OnActive = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(6));
			this.m_OnFocused = GUIStyleState.ProduceGUIStyleStateFromDeserialization(this, this.GetStyleStatePtr(7));
		}

		// Token: 0x1700093D RID: 2365
		// (get) Token: 0x06002624 RID: 9764 RVA: 0x00033C40 File Offset: 0x00031E40
		// (set) Token: 0x06002625 RID: 9765 RVA: 0x00033C7C File Offset: 0x00031E7C
		public GUIStyleState normal
		{
			get
			{
				if (this.m_Normal == null)
				{
					this.m_Normal = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(0));
				}
				return this.m_Normal;
			}
			set
			{
				this.AssignStyleState(0, value.m_Ptr);
			}
		}

		// Token: 0x1700093E RID: 2366
		// (get) Token: 0x06002626 RID: 9766 RVA: 0x00033C8C File Offset: 0x00031E8C
		// (set) Token: 0x06002627 RID: 9767 RVA: 0x00033CC8 File Offset: 0x00031EC8
		public GUIStyleState hover
		{
			get
			{
				if (this.m_Hover == null)
				{
					this.m_Hover = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(1));
				}
				return this.m_Hover;
			}
			set
			{
				this.AssignStyleState(1, value.m_Ptr);
			}
		}

		// Token: 0x1700093F RID: 2367
		// (get) Token: 0x06002628 RID: 9768 RVA: 0x00033CD8 File Offset: 0x00031ED8
		// (set) Token: 0x06002629 RID: 9769 RVA: 0x00033D14 File Offset: 0x00031F14
		public GUIStyleState active
		{
			get
			{
				if (this.m_Active == null)
				{
					this.m_Active = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(2));
				}
				return this.m_Active;
			}
			set
			{
				this.AssignStyleState(2, value.m_Ptr);
			}
		}

		// Token: 0x17000940 RID: 2368
		// (get) Token: 0x0600262A RID: 9770 RVA: 0x00033D24 File Offset: 0x00031F24
		// (set) Token: 0x0600262B RID: 9771 RVA: 0x00033D60 File Offset: 0x00031F60
		public GUIStyleState onNormal
		{
			get
			{
				if (this.m_OnNormal == null)
				{
					this.m_OnNormal = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(4));
				}
				return this.m_OnNormal;
			}
			set
			{
				this.AssignStyleState(4, value.m_Ptr);
			}
		}

		// Token: 0x17000941 RID: 2369
		// (get) Token: 0x0600262C RID: 9772 RVA: 0x00033D70 File Offset: 0x00031F70
		// (set) Token: 0x0600262D RID: 9773 RVA: 0x00033DAC File Offset: 0x00031FAC
		public GUIStyleState onHover
		{
			get
			{
				if (this.m_OnHover == null)
				{
					this.m_OnHover = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(5));
				}
				return this.m_OnHover;
			}
			set
			{
				this.AssignStyleState(5, value.m_Ptr);
			}
		}

		// Token: 0x17000942 RID: 2370
		// (get) Token: 0x0600262E RID: 9774 RVA: 0x00033DBC File Offset: 0x00031FBC
		// (set) Token: 0x0600262F RID: 9775 RVA: 0x00033DF8 File Offset: 0x00031FF8
		public GUIStyleState onActive
		{
			get
			{
				if (this.m_OnActive == null)
				{
					this.m_OnActive = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(6));
				}
				return this.m_OnActive;
			}
			set
			{
				this.AssignStyleState(6, value.m_Ptr);
			}
		}

		// Token: 0x17000943 RID: 2371
		// (get) Token: 0x06002630 RID: 9776 RVA: 0x00033E08 File Offset: 0x00032008
		// (set) Token: 0x06002631 RID: 9777 RVA: 0x00033E44 File Offset: 0x00032044
		public GUIStyleState focused
		{
			get
			{
				if (this.m_Focused == null)
				{
					this.m_Focused = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(3));
				}
				return this.m_Focused;
			}
			set
			{
				this.AssignStyleState(3, value.m_Ptr);
			}
		}

		// Token: 0x17000944 RID: 2372
		// (get) Token: 0x06002632 RID: 9778 RVA: 0x00033E54 File Offset: 0x00032054
		// (set) Token: 0x06002633 RID: 9779 RVA: 0x00033E90 File Offset: 0x00032090
		public GUIStyleState onFocused
		{
			get
			{
				if (this.m_OnFocused == null)
				{
					this.m_OnFocused = GUIStyleState.GetGUIStyleState(this, this.GetStyleStatePtr(7));
				}
				return this.m_OnFocused;
			}
			set
			{
				this.AssignStyleState(7, value.m_Ptr);
			}
		}

		// Token: 0x17000945 RID: 2373
		// (get) Token: 0x06002634 RID: 9780 RVA: 0x00033EA0 File Offset: 0x000320A0
		// (set) Token: 0x06002635 RID: 9781 RVA: 0x00033EDC File Offset: 0x000320DC
		public RectOffset border
		{
			get
			{
				if (this.m_Border == null)
				{
					this.m_Border = new RectOffset(this, this.GetRectOffsetPtr(0));
				}
				return this.m_Border;
			}
			set
			{
				this.AssignRectOffset(0, value.m_Ptr);
			}
		}

		// Token: 0x17000946 RID: 2374
		// (get) Token: 0x06002636 RID: 9782 RVA: 0x00033EEC File Offset: 0x000320EC
		// (set) Token: 0x06002637 RID: 9783 RVA: 0x00033F28 File Offset: 0x00032128
		public RectOffset margin
		{
			get
			{
				if (this.m_Margin == null)
				{
					this.m_Margin = new RectOffset(this, this.GetRectOffsetPtr(1));
				}
				return this.m_Margin;
			}
			set
			{
				this.AssignRectOffset(1, value.m_Ptr);
			}
		}

		// Token: 0x17000947 RID: 2375
		// (get) Token: 0x06002638 RID: 9784 RVA: 0x00033F38 File Offset: 0x00032138
		// (set) Token: 0x06002639 RID: 9785 RVA: 0x00033F74 File Offset: 0x00032174
		public RectOffset padding
		{
			get
			{
				if (this.m_Padding == null)
				{
					this.m_Padding = new RectOffset(this, this.GetRectOffsetPtr(2));
				}
				return this.m_Padding;
			}
			set
			{
				this.AssignRectOffset(2, value.m_Ptr);
			}
		}

		// Token: 0x17000948 RID: 2376
		// (get) Token: 0x0600263A RID: 9786 RVA: 0x00033F84 File Offset: 0x00032184
		// (set) Token: 0x0600263B RID: 9787 RVA: 0x00033FC0 File Offset: 0x000321C0
		public RectOffset overflow
		{
			get
			{
				if (this.m_Overflow == null)
				{
					this.m_Overflow = new RectOffset(this, this.GetRectOffsetPtr(3));
				}
				return this.m_Overflow;
			}
			set
			{
				this.AssignRectOffset(3, value.m_Ptr);
			}
		}

		// Token: 0x17000949 RID: 2377
		// (get) Token: 0x0600263C RID: 9788 RVA: 0x00033FD0 File Offset: 0x000321D0
		// (set) Token: 0x0600263D RID: 9789 RVA: 0x00033FEC File Offset: 0x000321EC
		[Obsolete("warning Don't use clipOffset - put things inside BeginGroup instead. This functionality will be removed in a later version.")]
		public Vector2 clipOffset
		{
			get
			{
				return this.Internal_clipOffset;
			}
			set
			{
				this.Internal_clipOffset = value;
			}
		}

		// Token: 0x1700094A RID: 2378
		// (get) Token: 0x0600263E RID: 9790 RVA: 0x00033FF8 File Offset: 0x000321F8
		// (set) Token: 0x0600263F RID: 9791 RVA: 0x00034014 File Offset: 0x00032214
		public Font font
		{
			get
			{
				return this.GetFontInternal();
			}
			set
			{
				this.SetFontInternal(value);
				this.m_FontInternal = value;
			}
		}

		// Token: 0x1700094B RID: 2379
		// (get) Token: 0x06002640 RID: 9792 RVA: 0x00034028 File Offset: 0x00032228
		public float lineHeight
		{
			get
			{
				return Mathf.Round(GUIStyle.Internal_GetLineHeight(this.m_Ptr));
			}
		}

		// Token: 0x06002641 RID: 9793 RVA: 0x00034050 File Offset: 0x00032250
		private static void Internal_Draw(IntPtr target, Rect position, GUIContent content, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			Internal_DrawArguments internal_DrawArguments = default(Internal_DrawArguments);
			internal_DrawArguments.target = target;
			internal_DrawArguments.position = position;
			internal_DrawArguments.isHover = ((!isHover) ? 0 : 1);
			internal_DrawArguments.isActive = ((!isActive) ? 0 : 1);
			internal_DrawArguments.on = ((!on) ? 0 : 1);
			internal_DrawArguments.hasKeyboardFocus = ((!hasKeyboardFocus) ? 0 : 1);
			GUIStyle.Internal_Draw(content, ref internal_DrawArguments);
		}

		// Token: 0x06002642 RID: 9794 RVA: 0x000340D4 File Offset: 0x000322D4
		public void Draw(Rect position, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			GUIStyle.Internal_Draw(this.m_Ptr, position, GUIContent.none, isHover, isActive, on, hasKeyboardFocus);
		}

		// Token: 0x06002643 RID: 9795 RVA: 0x000340F0 File Offset: 0x000322F0
		public void Draw(Rect position, string text, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			GUIStyle.Internal_Draw(this.m_Ptr, position, GUIContent.Temp(text), isHover, isActive, on, hasKeyboardFocus);
		}

		// Token: 0x06002644 RID: 9796 RVA: 0x0003410C File Offset: 0x0003230C
		public void Draw(Rect position, Texture image, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			GUIStyle.Internal_Draw(this.m_Ptr, position, GUIContent.Temp(image), isHover, isActive, on, hasKeyboardFocus);
		}

		// Token: 0x06002645 RID: 9797 RVA: 0x00034128 File Offset: 0x00032328
		public void Draw(Rect position, GUIContent content, bool isHover, bool isActive, bool on, bool hasKeyboardFocus)
		{
			GUIStyle.Internal_Draw(this.m_Ptr, position, content, isHover, isActive, on, hasKeyboardFocus);
		}

		// Token: 0x06002646 RID: 9798 RVA: 0x00034140 File Offset: 0x00032340
		public void Draw(Rect position, GUIContent content, int controlID)
		{
			this.Draw(position, content, controlID, false);
		}

		// Token: 0x06002647 RID: 9799 RVA: 0x00034150 File Offset: 0x00032350
		public void Draw(Rect position, GUIContent content, int controlID, bool on)
		{
			if (content != null)
			{
				GUIStyle.Internal_Draw2(this.m_Ptr, position, content, controlID, on);
			}
			else
			{
				Debug.LogError("Style.Draw may not be called with GUIContent that is null.");
			}
		}

		// Token: 0x06002648 RID: 9800 RVA: 0x00034178 File Offset: 0x00032378
		public void DrawCursor(Rect position, GUIContent content, int controlID, int Character)
		{
			Event current = Event.current;
			if (current.type == EventType.Repaint)
			{
				Color cursorColor = new Color(0f, 0f, 0f, 0f);
				float cursorFlashSpeed = GUI.skin.settings.cursorFlashSpeed;
				float num = (Time.realtimeSinceStartup - GUIStyle.Internal_GetCursorFlashOffset()) % cursorFlashSpeed / cursorFlashSpeed;
				if (cursorFlashSpeed == 0f || num < 0.5f)
				{
					cursorColor = GUI.skin.settings.cursorColor;
				}
				GUIStyle.Internal_DrawCursor(this.m_Ptr, position, content, Character, cursorColor);
			}
		}

		// Token: 0x06002649 RID: 9801 RVA: 0x00034210 File Offset: 0x00032410
		internal void DrawWithTextSelection(Rect position, GUIContent content, int controlID, int firstSelectedCharacter, int lastSelectedCharacter, bool drawSelectionAsComposition)
		{
			Event current = Event.current;
			Color cursorColor = new Color(0f, 0f, 0f, 0f);
			float cursorFlashSpeed = GUI.skin.settings.cursorFlashSpeed;
			float num = (Time.realtimeSinceStartup - GUIStyle.Internal_GetCursorFlashOffset()) % cursorFlashSpeed / cursorFlashSpeed;
			if (cursorFlashSpeed == 0f || num < 0.5f)
			{
				cursorColor = GUI.skin.settings.cursorColor;
			}
			Internal_DrawWithTextSelectionArguments internal_DrawWithTextSelectionArguments = default(Internal_DrawWithTextSelectionArguments);
			internal_DrawWithTextSelectionArguments.target = this.m_Ptr;
			internal_DrawWithTextSelectionArguments.position = position;
			internal_DrawWithTextSelectionArguments.firstPos = firstSelectedCharacter;
			internal_DrawWithTextSelectionArguments.lastPos = lastSelectedCharacter;
			internal_DrawWithTextSelectionArguments.cursorColor = cursorColor;
			internal_DrawWithTextSelectionArguments.selectionColor = GUI.skin.settings.selectionColor;
			internal_DrawWithTextSelectionArguments.isHover = ((!position.Contains(current.mousePosition)) ? 0 : 1);
			internal_DrawWithTextSelectionArguments.isActive = ((controlID != GUIUtility.hotControl) ? 0 : 1);
			internal_DrawWithTextSelectionArguments.on = 0;
			internal_DrawWithTextSelectionArguments.hasKeyboardFocus = ((controlID != GUIUtility.keyboardControl || !GUIStyle.showKeyboardFocus) ? 0 : 1);
			internal_DrawWithTextSelectionArguments.drawSelectionAsComposition = ((!drawSelectionAsComposition) ? 0 : 1);
			GUIStyle.Internal_DrawWithTextSelection(content, ref internal_DrawWithTextSelectionArguments);
		}

		// Token: 0x0600264A RID: 9802 RVA: 0x00034354 File Offset: 0x00032554
		public void DrawWithTextSelection(Rect position, GUIContent content, int controlID, int firstSelectedCharacter, int lastSelectedCharacter)
		{
			this.DrawWithTextSelection(position, content, controlID, firstSelectedCharacter, lastSelectedCharacter, false);
		}

		// Token: 0x0600264B RID: 9803 RVA: 0x00034368 File Offset: 0x00032568
		public static implicit operator GUIStyle(string str)
		{
			GUIStyle result;
			if (GUISkin.current == null)
			{
				Debug.LogError("Unable to use a named GUIStyle without a current skin. Most likely you need to move your GUIStyle initialization code to OnGUI");
				result = GUISkin.error;
			}
			else
			{
				result = GUISkin.current.GetStyle(str);
			}
			return result;
		}

		// Token: 0x1700094C RID: 2380
		// (get) Token: 0x0600264C RID: 9804 RVA: 0x000343B0 File Offset: 0x000325B0
		public static GUIStyle none
		{
			get
			{
				if (GUIStyle.s_None == null)
				{
					GUIStyle.s_None = new GUIStyle();
				}
				return GUIStyle.s_None;
			}
		}

		// Token: 0x0600264D RID: 9805 RVA: 0x000343E0 File Offset: 0x000325E0
		public Vector2 GetCursorPixelPosition(Rect position, GUIContent content, int cursorStringIndex)
		{
			Vector2 result;
			GUIStyle.Internal_GetCursorPixelPosition(this.m_Ptr, position, content, cursorStringIndex, out result);
			return result;
		}

		// Token: 0x0600264E RID: 9806 RVA: 0x00034408 File Offset: 0x00032608
		public int GetCursorStringIndex(Rect position, GUIContent content, Vector2 cursorPixelPosition)
		{
			return GUIStyle.Internal_GetCursorStringIndex(this.m_Ptr, position, content, cursorPixelPosition);
		}

		// Token: 0x0600264F RID: 9807 RVA: 0x0003442C File Offset: 0x0003262C
		internal int GetNumCharactersThatFitWithinWidth(string text, float width)
		{
			return GUIStyle.Internal_GetNumCharactersThatFitWithinWidth(this.m_Ptr, text, width);
		}

		// Token: 0x06002650 RID: 9808 RVA: 0x00034450 File Offset: 0x00032650
		public Vector2 CalcSize(GUIContent content)
		{
			Vector2 result;
			GUIStyle.Internal_CalcSize(this.m_Ptr, content, out result);
			return result;
		}

		// Token: 0x06002651 RID: 9809 RVA: 0x00034474 File Offset: 0x00032674
		internal Vector2 CalcSizeWithConstraints(GUIContent content, Vector2 constraints)
		{
			Vector2 result;
			GUIStyle.Internal_CalcSizeWithConstraints(this.m_Ptr, content, constraints, out result);
			return result;
		}

		// Token: 0x06002652 RID: 9810 RVA: 0x0003449C File Offset: 0x0003269C
		public Vector2 CalcScreenSize(Vector2 contentSize)
		{
			return new Vector2((this.fixedWidth == 0f) ? Mathf.Ceil(contentSize.x + (float)this.padding.left + (float)this.padding.right) : this.fixedWidth, (this.fixedHeight == 0f) ? Mathf.Ceil(contentSize.y + (float)this.padding.top + (float)this.padding.bottom) : this.fixedHeight);
		}

		// Token: 0x06002653 RID: 9811 RVA: 0x00034538 File Offset: 0x00032738
		public float CalcHeight(GUIContent content, float width)
		{
			return GUIStyle.Internal_CalcHeight(this.m_Ptr, content, width);
		}

		// Token: 0x1700094D RID: 2381
		// (get) Token: 0x06002654 RID: 9812 RVA: 0x0003455C File Offset: 0x0003275C
		public bool isHeightDependantOnWidth
		{
			get
			{
				return this.fixedHeight == 0f && this.wordWrap && this.imagePosition != ImagePosition.ImageOnly;
			}
		}

		// Token: 0x06002655 RID: 9813 RVA: 0x000345A0 File Offset: 0x000327A0
		public void CalcMinMaxWidth(GUIContent content, out float minWidth, out float maxWidth)
		{
			GUIStyle.Internal_CalcMinMaxWidth(this.m_Ptr, content, out minWidth, out maxWidth);
		}

		// Token: 0x06002656 RID: 9814 RVA: 0x000345B4 File Offset: 0x000327B4
		public override string ToString()
		{
			return UnityString.Format("GUIStyle '{0}'", new object[]
			{
				this.name
			});
		}

		// Token: 0x06002657 RID: 9815
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Init();

		// Token: 0x06002658 RID: 9816
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void InitCopy(GUIStyle other);

		// Token: 0x06002659 RID: 9817
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Cleanup();

		// Token: 0x1700094E RID: 2382
		// (get) Token: 0x0600265A RID: 9818
		// (set) Token: 0x0600265B RID: 9819
		public extern string name { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600265C RID: 9820 RVA: 0x000345E4 File Offset: 0x000327E4
		[ThreadAndSerializationSafe]
		private IntPtr GetStyleStatePtr(int idx)
		{
			IntPtr result;
			GUIStyle.INTERNAL_CALL_GetStyleStatePtr(this, idx, out result);
			return result;
		}

		// Token: 0x0600265D RID: 9821
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetStyleStatePtr(GUIStyle self, int idx, out IntPtr value);

		// Token: 0x0600265E RID: 9822
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AssignStyleState(int idx, IntPtr srcStyleState);

		// Token: 0x0600265F RID: 9823 RVA: 0x00034604 File Offset: 0x00032804
		private IntPtr GetRectOffsetPtr(int idx)
		{
			IntPtr result;
			GUIStyle.INTERNAL_CALL_GetRectOffsetPtr(this, idx, out result);
			return result;
		}

		// Token: 0x06002660 RID: 9824
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_GetRectOffsetPtr(GUIStyle self, int idx, out IntPtr value);

		// Token: 0x06002661 RID: 9825
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void AssignRectOffset(int idx, IntPtr srcRectOffset);

		// Token: 0x1700094F RID: 2383
		// (get) Token: 0x06002662 RID: 9826
		// (set) Token: 0x06002663 RID: 9827
		public extern ImagePosition imagePosition { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000950 RID: 2384
		// (get) Token: 0x06002664 RID: 9828
		// (set) Token: 0x06002665 RID: 9829
		public extern TextAnchor alignment { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000951 RID: 2385
		// (get) Token: 0x06002666 RID: 9830
		// (set) Token: 0x06002667 RID: 9831
		public extern bool wordWrap { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000952 RID: 2386
		// (get) Token: 0x06002668 RID: 9832
		// (set) Token: 0x06002669 RID: 9833
		public extern TextClipping clipping { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000953 RID: 2387
		// (get) Token: 0x0600266A RID: 9834 RVA: 0x00034624 File Offset: 0x00032824
		// (set) Token: 0x0600266B RID: 9835 RVA: 0x00034644 File Offset: 0x00032844
		public Vector2 contentOffset
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_contentOffset(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_contentOffset(ref value);
			}
		}

		// Token: 0x0600266C RID: 9836
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_contentOffset(out Vector2 value);

		// Token: 0x0600266D RID: 9837
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_contentOffset(ref Vector2 value);

		// Token: 0x17000954 RID: 2388
		// (get) Token: 0x0600266E RID: 9838 RVA: 0x00034650 File Offset: 0x00032850
		// (set) Token: 0x0600266F RID: 9839 RVA: 0x00034670 File Offset: 0x00032870
		internal Vector2 Internal_clipOffset
		{
			get
			{
				Vector2 result;
				this.INTERNAL_get_Internal_clipOffset(out result);
				return result;
			}
			set
			{
				this.INTERNAL_set_Internal_clipOffset(ref value);
			}
		}

		// Token: 0x06002670 RID: 9840
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_get_Internal_clipOffset(out Vector2 value);

		// Token: 0x06002671 RID: 9841
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void INTERNAL_set_Internal_clipOffset(ref Vector2 value);

		// Token: 0x17000955 RID: 2389
		// (get) Token: 0x06002672 RID: 9842
		// (set) Token: 0x06002673 RID: 9843
		public extern float fixedWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000956 RID: 2390
		// (get) Token: 0x06002674 RID: 9844
		// (set) Token: 0x06002675 RID: 9845
		public extern float fixedHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000957 RID: 2391
		// (get) Token: 0x06002676 RID: 9846
		// (set) Token: 0x06002677 RID: 9847
		public extern bool stretchWidth { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000958 RID: 2392
		// (get) Token: 0x06002678 RID: 9848
		// (set) Token: 0x06002679 RID: 9849
		public extern bool stretchHeight { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x0600267A RID: 9850
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float Internal_GetLineHeight(IntPtr target);

		// Token: 0x0600267B RID: 9851
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetFontInternal(Font value);

		// Token: 0x0600267C RID: 9852
		[ThreadAndSerializationSafe]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Font GetFontInternalDuringLoadingThread();

		// Token: 0x0600267D RID: 9853
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Font GetFontInternal();

		// Token: 0x17000959 RID: 2393
		// (get) Token: 0x0600267E RID: 9854
		// (set) Token: 0x0600267F RID: 9855
		public extern int fontSize { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700095A RID: 2394
		// (get) Token: 0x06002680 RID: 9856
		// (set) Token: 0x06002681 RID: 9857
		public extern FontStyle fontStyle { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x1700095B RID: 2395
		// (get) Token: 0x06002682 RID: 9858
		// (set) Token: 0x06002683 RID: 9859
		public extern bool richText { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06002684 RID: 9860
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_Draw(GUIContent content, ref Internal_DrawArguments arguments);

		// Token: 0x06002685 RID: 9861 RVA: 0x0003467C File Offset: 0x0003287C
		private static void Internal_Draw2(IntPtr style, Rect position, GUIContent content, int controlID, bool on)
		{
			GUIStyle.INTERNAL_CALL_Internal_Draw2(style, ref position, content, controlID, on);
		}

		// Token: 0x06002686 RID: 9862
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_Draw2(IntPtr style, ref Rect position, GUIContent content, int controlID, bool on);

		// Token: 0x06002687 RID: 9863 RVA: 0x0003468C File Offset: 0x0003288C
		internal void SetMouseTooltip(string tooltip, Rect screenRect)
		{
			GUIStyle.INTERNAL_CALL_SetMouseTooltip(this, tooltip, ref screenRect);
		}

		// Token: 0x06002688 RID: 9864
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_SetMouseTooltip(GUIStyle self, string tooltip, ref Rect screenRect);

		// Token: 0x06002689 RID: 9865 RVA: 0x00034698 File Offset: 0x00032898
		private static void Internal_DrawPrefixLabel(IntPtr style, Rect position, GUIContent content, int controlID, bool on)
		{
			GUIStyle.INTERNAL_CALL_Internal_DrawPrefixLabel(style, ref position, content, controlID, on);
		}

		// Token: 0x0600268A RID: 9866
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_DrawPrefixLabel(IntPtr style, ref Rect position, GUIContent content, int controlID, bool on);

		// Token: 0x0600268B RID: 9867
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float Internal_GetCursorFlashOffset();

		// Token: 0x0600268C RID: 9868 RVA: 0x000346A8 File Offset: 0x000328A8
		private static void Internal_DrawCursor(IntPtr target, Rect position, GUIContent content, int pos, Color cursorColor)
		{
			GUIStyle.INTERNAL_CALL_Internal_DrawCursor(target, ref position, content, pos, ref cursorColor);
		}

		// Token: 0x0600268D RID: 9869
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_DrawCursor(IntPtr target, ref Rect position, GUIContent content, int pos, ref Color cursorColor);

		// Token: 0x0600268E RID: 9870
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_DrawWithTextSelection(GUIContent content, ref Internal_DrawWithTextSelectionArguments arguments);

		// Token: 0x0600268F RID: 9871
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void SetDefaultFont(Font font);

		// Token: 0x06002690 RID: 9872 RVA: 0x000346B8 File Offset: 0x000328B8
		internal static void Internal_GetCursorPixelPosition(IntPtr target, Rect position, GUIContent content, int cursorStringIndex, out Vector2 ret)
		{
			GUIStyle.INTERNAL_CALL_Internal_GetCursorPixelPosition(target, ref position, content, cursorStringIndex, out ret);
		}

		// Token: 0x06002691 RID: 9873
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_GetCursorPixelPosition(IntPtr target, ref Rect position, GUIContent content, int cursorStringIndex, out Vector2 ret);

		// Token: 0x06002692 RID: 9874 RVA: 0x000346C8 File Offset: 0x000328C8
		internal static int Internal_GetCursorStringIndex(IntPtr target, Rect position, GUIContent content, Vector2 cursorPixelPosition)
		{
			return GUIStyle.INTERNAL_CALL_Internal_GetCursorStringIndex(target, ref position, content, ref cursorPixelPosition);
		}

		// Token: 0x06002693 RID: 9875
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int INTERNAL_CALL_Internal_GetCursorStringIndex(IntPtr target, ref Rect position, GUIContent content, ref Vector2 cursorPixelPosition);

		// Token: 0x06002694 RID: 9876
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int Internal_GetNumCharactersThatFitWithinWidth(IntPtr target, string text, float width);

		// Token: 0x06002695 RID: 9877
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void Internal_CalcSize(IntPtr target, GUIContent content, out Vector2 ret);

		// Token: 0x06002696 RID: 9878 RVA: 0x000346E8 File Offset: 0x000328E8
		internal static void Internal_CalcSizeWithConstraints(IntPtr target, GUIContent content, Vector2 maxSize, out Vector2 ret)
		{
			GUIStyle.INTERNAL_CALL_Internal_CalcSizeWithConstraints(target, content, ref maxSize, out ret);
		}

		// Token: 0x06002697 RID: 9879
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void INTERNAL_CALL_Internal_CalcSizeWithConstraints(IntPtr target, GUIContent content, ref Vector2 maxSize, out Vector2 ret);

		// Token: 0x06002698 RID: 9880
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern float Internal_CalcHeight(IntPtr target, GUIContent content, float width);

		// Token: 0x06002699 RID: 9881
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Internal_CalcMinMaxWidth(IntPtr target, GUIContent content, out float minWidth, out float maxWidth);

		// Token: 0x04000858 RID: 2136
		[NonSerialized]
		internal IntPtr m_Ptr;

		// Token: 0x04000859 RID: 2137
		[NonSerialized]
		private GUIStyleState m_Normal;

		// Token: 0x0400085A RID: 2138
		[NonSerialized]
		private GUIStyleState m_Hover;

		// Token: 0x0400085B RID: 2139
		[NonSerialized]
		private GUIStyleState m_Active;

		// Token: 0x0400085C RID: 2140
		[NonSerialized]
		private GUIStyleState m_Focused;

		// Token: 0x0400085D RID: 2141
		[NonSerialized]
		private GUIStyleState m_OnNormal;

		// Token: 0x0400085E RID: 2142
		[NonSerialized]
		private GUIStyleState m_OnHover;

		// Token: 0x0400085F RID: 2143
		[NonSerialized]
		private GUIStyleState m_OnActive;

		// Token: 0x04000860 RID: 2144
		[NonSerialized]
		private GUIStyleState m_OnFocused;

		// Token: 0x04000861 RID: 2145
		[NonSerialized]
		private RectOffset m_Border;

		// Token: 0x04000862 RID: 2146
		[NonSerialized]
		private RectOffset m_Padding;

		// Token: 0x04000863 RID: 2147
		[NonSerialized]
		private RectOffset m_Margin;

		// Token: 0x04000864 RID: 2148
		[NonSerialized]
		private RectOffset m_Overflow;

		// Token: 0x04000865 RID: 2149
		[NonSerialized]
		private Font m_FontInternal;

		// Token: 0x04000866 RID: 2150
		internal static bool showKeyboardFocus = true;

		// Token: 0x04000867 RID: 2151
		private static GUIStyle s_None;
	}
}
