﻿using System;

namespace UnityEngine
{
	// Token: 0x02000200 RID: 512
	public enum HorizontalWrapMode
	{
		// Token: 0x04000620 RID: 1568
		Wrap,
		// Token: 0x04000621 RID: 1569
		Overflow
	}
}
