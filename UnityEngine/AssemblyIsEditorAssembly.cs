﻿using System;
using UnityEngine.Scripting;

namespace UnityEngine
{
	// Token: 0x020002DB RID: 731
	[RequiredByNativeCode]
	[AttributeUsage(AttributeTargets.Assembly)]
	public class AssemblyIsEditorAssembly : Attribute
	{
	}
}
