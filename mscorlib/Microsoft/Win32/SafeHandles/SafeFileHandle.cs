﻿using System;
using System.IO;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x02000076 RID: 118
	public sealed class SafeFileHandle : SafeHandleZeroOrMinusOneIsInvalid
	{
		// Token: 0x0600077C RID: 1916 RVA: 0x00017CB4 File Offset: 0x00015EB4
		public SafeFileHandle(IntPtr preexistingHandle, bool ownsHandle) : base(ownsHandle)
		{
			base.SetHandle(preexistingHandle);
		}

		// Token: 0x0600077D RID: 1917 RVA: 0x00017CC4 File Offset: 0x00015EC4
		internal SafeFileHandle() : base(true)
		{
		}

		// Token: 0x0600077E RID: 1918 RVA: 0x00017CD0 File Offset: 0x00015ED0
		protected override bool ReleaseHandle()
		{
			MonoIOError monoIOError;
			MonoIO.Close(this.handle, out monoIOError);
			return monoIOError == MonoIOError.ERROR_SUCCESS;
		}
	}
}
