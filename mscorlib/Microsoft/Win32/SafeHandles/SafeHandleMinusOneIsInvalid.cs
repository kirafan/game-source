﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x02000075 RID: 117
	public abstract class SafeHandleMinusOneIsInvalid : SafeHandle, IDisposable
	{
		// Token: 0x0600077A RID: 1914 RVA: 0x00017C90 File Offset: 0x00015E90
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		protected SafeHandleMinusOneIsInvalid(bool ownsHandle) : base((IntPtr)0, ownsHandle)
		{
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x0600077B RID: 1915 RVA: 0x00017CA0 File Offset: 0x00015EA0
		public override bool IsInvalid
		{
			get
			{
				return this.handle == (IntPtr)(-1);
			}
		}
	}
}
