﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x02000072 RID: 114
	public abstract class CriticalHandleZeroOrMinusOneIsInvalid : CriticalHandle, IDisposable
	{
		// Token: 0x06000774 RID: 1908 RVA: 0x00017BE8 File Offset: 0x00015DE8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		protected CriticalHandleZeroOrMinusOneIsInvalid() : base((IntPtr)(-1))
		{
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x06000775 RID: 1909 RVA: 0x00017BF8 File Offset: 0x00015DF8
		public override bool IsInvalid
		{
			get
			{
				return this.handle == (IntPtr)(-1) || this.handle == IntPtr.Zero;
			}
		}
	}
}
