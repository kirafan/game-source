﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x02000074 RID: 116
	public abstract class SafeHandleZeroOrMinusOneIsInvalid : SafeHandle, IDisposable
	{
		// Token: 0x06000778 RID: 1912 RVA: 0x00017C48 File Offset: 0x00015E48
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		protected SafeHandleZeroOrMinusOneIsInvalid(bool ownsHandle) : base((IntPtr)0, ownsHandle)
		{
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x06000779 RID: 1913 RVA: 0x00017C58 File Offset: 0x00015E58
		public override bool IsInvalid
		{
			get
			{
				return this.handle == (IntPtr)(-1) || this.handle == (IntPtr)0;
			}
		}
	}
}
