﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x02000073 RID: 115
	public abstract class CriticalHandleMinusOneIsInvalid : CriticalHandle, IDisposable
	{
		// Token: 0x06000776 RID: 1910 RVA: 0x00017C24 File Offset: 0x00015E24
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		protected CriticalHandleMinusOneIsInvalid() : base((IntPtr)(-1))
		{
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x06000777 RID: 1911 RVA: 0x00017C34 File Offset: 0x00015E34
		public override bool IsInvalid
		{
			get
			{
				return this.handle == (IntPtr)(-1);
			}
		}
	}
}
