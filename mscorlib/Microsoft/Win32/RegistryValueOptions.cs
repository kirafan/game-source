﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x0200006C RID: 108
	[Flags]
	public enum RegistryValueOptions
	{
		// Token: 0x040000FE RID: 254
		None = 0,
		// Token: 0x040000FF RID: 255
		DoNotExpandEnvironmentNames = 1
	}
}
