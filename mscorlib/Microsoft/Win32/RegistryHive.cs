﻿using System;
using System.Runtime.InteropServices;

namespace Microsoft.Win32
{
	// Token: 0x0200006A RID: 106
	[ComVisible(true)]
	[Serializable]
	public enum RegistryHive
	{
		// Token: 0x040000EE RID: 238
		ClassesRoot = -2147483648,
		// Token: 0x040000EF RID: 239
		CurrentConfig = -2147483643,
		// Token: 0x040000F0 RID: 240
		CurrentUser = -2147483647,
		// Token: 0x040000F1 RID: 241
		DynData = -2147483642,
		// Token: 0x040000F2 RID: 242
		LocalMachine = -2147483646,
		// Token: 0x040000F3 RID: 243
		PerformanceData = -2147483644,
		// Token: 0x040000F4 RID: 244
		Users = -2147483645
	}
}
