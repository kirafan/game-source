﻿using System;

namespace Microsoft.Win32
{
	// Token: 0x02000068 RID: 104
	public enum RegistryKeyPermissionCheck
	{
		// Token: 0x040000E2 RID: 226
		Default,
		// Token: 0x040000E3 RID: 227
		ReadSubTree,
		// Token: 0x040000E4 RID: 228
		ReadWriteSubTree
	}
}
