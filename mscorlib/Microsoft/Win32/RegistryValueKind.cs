﻿using System;
using System.Runtime.InteropServices;

namespace Microsoft.Win32
{
	// Token: 0x0200006B RID: 107
	[ComVisible(true)]
	public enum RegistryValueKind
	{
		// Token: 0x040000F6 RID: 246
		Unknown,
		// Token: 0x040000F7 RID: 247
		String,
		// Token: 0x040000F8 RID: 248
		ExpandString,
		// Token: 0x040000F9 RID: 249
		Binary,
		// Token: 0x040000FA RID: 250
		DWord,
		// Token: 0x040000FB RID: 251
		MultiString = 7,
		// Token: 0x040000FC RID: 252
		QWord = 11
	}
}
