﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;

namespace Microsoft.Win32
{
	// Token: 0x02000067 RID: 103
	[ComVisible(true)]
	public sealed class RegistryKey : MarshalByRefObject, IDisposable
	{
		// Token: 0x060006EB RID: 1771 RVA: 0x000153B0 File Offset: 0x000135B0
		internal RegistryKey(RegistryHive hiveId) : this(hiveId, new IntPtr((int)hiveId), false)
		{
		}

		// Token: 0x060006EC RID: 1772 RVA: 0x000153C0 File Offset: 0x000135C0
		internal RegistryKey(RegistryHive hiveId, IntPtr keyHandle, bool remoteRoot)
		{
			this.hive = hiveId;
			this.handle = keyHandle;
			this.qname = RegistryKey.GetHiveName(hiveId);
			this.isRemoteRoot = remoteRoot;
			this.isWritable = true;
		}

		// Token: 0x060006ED RID: 1773 RVA: 0x00015408 File Offset: 0x00013608
		internal RegistryKey(object data, string keyName, bool writable)
		{
			this.handle = data;
			this.qname = keyName;
			this.isWritable = writable;
		}

		// Token: 0x060006EE RID: 1774 RVA: 0x00015428 File Offset: 0x00013628
		static RegistryKey()
		{
			if (Path.DirectorySeparatorChar == '\\')
			{
				RegistryKey.RegistryApi = new Win32RegistryApi();
			}
			else
			{
				RegistryKey.RegistryApi = new UnixRegistryApi();
			}
		}

		// Token: 0x060006EF RID: 1775 RVA: 0x00015450 File Offset: 0x00013650
		void IDisposable.Dispose()
		{
			GC.SuppressFinalize(this);
			this.Close();
		}

		// Token: 0x060006F0 RID: 1776 RVA: 0x00015460 File Offset: 0x00013660
		~RegistryKey()
		{
			this.Close();
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x060006F1 RID: 1777 RVA: 0x0001549C File Offset: 0x0001369C
		public string Name
		{
			get
			{
				return this.qname;
			}
		}

		// Token: 0x060006F2 RID: 1778 RVA: 0x000154A4 File Offset: 0x000136A4
		public void Flush()
		{
			RegistryKey.RegistryApi.Flush(this);
		}

		// Token: 0x060006F3 RID: 1779 RVA: 0x000154B4 File Offset: 0x000136B4
		public void Close()
		{
			this.Flush();
			if (!this.isRemoteRoot && this.IsRoot)
			{
				return;
			}
			RegistryKey.RegistryApi.Close(this);
			this.handle = null;
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x060006F4 RID: 1780 RVA: 0x000154F0 File Offset: 0x000136F0
		public int SubKeyCount
		{
			get
			{
				this.AssertKeyStillValid();
				return RegistryKey.RegistryApi.SubKeyCount(this);
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060006F5 RID: 1781 RVA: 0x00015504 File Offset: 0x00013704
		public int ValueCount
		{
			get
			{
				this.AssertKeyStillValid();
				return RegistryKey.RegistryApi.ValueCount(this);
			}
		}

		// Token: 0x060006F6 RID: 1782 RVA: 0x00015518 File Offset: 0x00013718
		public void SetValue(string name, object value)
		{
			this.AssertKeyStillValid();
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (name != null)
			{
				this.AssertKeyNameLength(name);
			}
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			RegistryKey.RegistryApi.SetValue(this, name, value);
		}

		// Token: 0x060006F7 RID: 1783 RVA: 0x0001556C File Offset: 0x0001376C
		[ComVisible(false)]
		public void SetValue(string name, object value, RegistryValueKind valueKind)
		{
			this.AssertKeyStillValid();
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (name != null)
			{
				this.AssertKeyNameLength(name);
			}
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			RegistryKey.RegistryApi.SetValue(this, name, value, valueKind);
		}

		// Token: 0x060006F8 RID: 1784 RVA: 0x000155C4 File Offset: 0x000137C4
		public RegistryKey OpenSubKey(string name)
		{
			return this.OpenSubKey(name, false);
		}

		// Token: 0x060006F9 RID: 1785 RVA: 0x000155D0 File Offset: 0x000137D0
		public RegistryKey OpenSubKey(string name, bool writable)
		{
			this.AssertKeyStillValid();
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			this.AssertKeyNameLength(name);
			return RegistryKey.RegistryApi.OpenSubKey(this, name, writable);
		}

		// Token: 0x060006FA RID: 1786 RVA: 0x00015608 File Offset: 0x00013808
		public object GetValue(string name)
		{
			return this.GetValue(name, null);
		}

		// Token: 0x060006FB RID: 1787 RVA: 0x00015614 File Offset: 0x00013814
		public object GetValue(string name, object defaultValue)
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.GetValue(this, name, defaultValue, RegistryValueOptions.None);
		}

		// Token: 0x060006FC RID: 1788 RVA: 0x00015638 File Offset: 0x00013838
		[ComVisible(false)]
		public object GetValue(string name, object defaultValue, RegistryValueOptions options)
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.GetValue(this, name, defaultValue, options);
		}

		// Token: 0x060006FD RID: 1789 RVA: 0x0001565C File Offset: 0x0001385C
		[ComVisible(false)]
		public RegistryValueKind GetValueKind(string name)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060006FE RID: 1790 RVA: 0x00015664 File Offset: 0x00013864
		public RegistryKey CreateSubKey(string subkey)
		{
			this.AssertKeyStillValid();
			this.AssertKeyNameNotNull(subkey);
			this.AssertKeyNameLength(subkey);
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			return RegistryKey.RegistryApi.CreateSubKey(this, subkey);
		}

		// Token: 0x060006FF RID: 1791 RVA: 0x000156A8 File Offset: 0x000138A8
		[ComVisible(false)]
		public RegistryKey CreateSubKey(string subkey, RegistryKeyPermissionCheck permissionCheck)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000700 RID: 1792 RVA: 0x000156B0 File Offset: 0x000138B0
		[ComVisible(false)]
		public RegistryKey CreateSubKey(string subkey, RegistryKeyPermissionCheck permissionCheck, RegistrySecurity registrySecurity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000701 RID: 1793 RVA: 0x000156B8 File Offset: 0x000138B8
		public void DeleteSubKey(string subkey)
		{
			this.DeleteSubKey(subkey, true);
		}

		// Token: 0x06000702 RID: 1794 RVA: 0x000156C4 File Offset: 0x000138C4
		public void DeleteSubKey(string subkey, bool throwOnMissingSubKey)
		{
			this.AssertKeyStillValid();
			this.AssertKeyNameNotNull(subkey);
			this.AssertKeyNameLength(subkey);
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			RegistryKey registryKey = this.OpenSubKey(subkey);
			if (registryKey == null)
			{
				if (throwOnMissingSubKey)
				{
					throw new ArgumentException("Cannot delete a subkey tree because the subkey does not exist.");
				}
				return;
			}
			else
			{
				if (registryKey.SubKeyCount > 0)
				{
					throw new InvalidOperationException("Registry key has subkeys and recursive removes are not supported by this method.");
				}
				registryKey.Close();
				RegistryKey.RegistryApi.DeleteKey(this, subkey, throwOnMissingSubKey);
				return;
			}
		}

		// Token: 0x06000703 RID: 1795 RVA: 0x00015748 File Offset: 0x00013948
		public void DeleteSubKeyTree(string subkey)
		{
			this.AssertKeyStillValid();
			this.AssertKeyNameNotNull(subkey);
			this.AssertKeyNameLength(subkey);
			RegistryKey registryKey = this.OpenSubKey(subkey, true);
			if (registryKey == null)
			{
				throw new ArgumentException("Cannot delete a subkey tree because the subkey does not exist.");
			}
			registryKey.DeleteChildKeysAndValues();
			registryKey.Close();
			this.DeleteSubKey(subkey, false);
		}

		// Token: 0x06000704 RID: 1796 RVA: 0x00015798 File Offset: 0x00013998
		public void DeleteValue(string name)
		{
			this.DeleteValue(name, true);
		}

		// Token: 0x06000705 RID: 1797 RVA: 0x000157A4 File Offset: 0x000139A4
		public void DeleteValue(string name, bool throwOnMissingValue)
		{
			this.AssertKeyStillValid();
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (!this.IsWritable)
			{
				throw new UnauthorizedAccessException("Cannot write to the registry key.");
			}
			RegistryKey.RegistryApi.DeleteValue(this, name, throwOnMissingValue);
		}

		// Token: 0x06000706 RID: 1798 RVA: 0x000157EC File Offset: 0x000139EC
		public RegistrySecurity GetAccessControl()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000707 RID: 1799 RVA: 0x000157F4 File Offset: 0x000139F4
		public RegistrySecurity GetAccessControl(AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000708 RID: 1800 RVA: 0x000157FC File Offset: 0x000139FC
		public string[] GetSubKeyNames()
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.GetSubKeyNames(this);
		}

		// Token: 0x06000709 RID: 1801 RVA: 0x00015810 File Offset: 0x00013A10
		public string[] GetValueNames()
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.GetValueNames(this);
		}

		// Token: 0x0600070A RID: 1802 RVA: 0x00015824 File Offset: 0x00013A24
		[MonoTODO("Not implemented on unix")]
		public static RegistryKey OpenRemoteBaseKey(RegistryHive hKey, string machineName)
		{
			if (machineName == null)
			{
				throw new ArgumentNullException("machineName");
			}
			return RegistryKey.RegistryApi.OpenRemoteBaseKey(hKey, machineName);
		}

		// Token: 0x0600070B RID: 1803 RVA: 0x00015844 File Offset: 0x00013A44
		[ComVisible(false)]
		public RegistryKey OpenSubKey(string name, RegistryKeyPermissionCheck permissionCheck)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600070C RID: 1804 RVA: 0x0001584C File Offset: 0x00013A4C
		[ComVisible(false)]
		public RegistryKey OpenSubKey(string name, RegistryKeyPermissionCheck permissionCheck, RegistryRights rights)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600070D RID: 1805 RVA: 0x00015854 File Offset: 0x00013A54
		public void SetAccessControl(RegistrySecurity registrySecurity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600070E RID: 1806 RVA: 0x0001585C File Offset: 0x00013A5C
		public override string ToString()
		{
			this.AssertKeyStillValid();
			return RegistryKey.RegistryApi.ToString(this);
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x0600070F RID: 1807 RVA: 0x00015870 File Offset: 0x00013A70
		internal bool IsRoot
		{
			get
			{
				return this.hive != null;
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x06000710 RID: 1808 RVA: 0x00015880 File Offset: 0x00013A80
		private bool IsWritable
		{
			get
			{
				return this.isWritable;
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x06000711 RID: 1809 RVA: 0x00015888 File Offset: 0x00013A88
		internal RegistryHive Hive
		{
			get
			{
				if (!this.IsRoot)
				{
					throw new NotSupportedException();
				}
				return (RegistryHive)((int)this.hive);
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x06000712 RID: 1810 RVA: 0x000158A8 File Offset: 0x00013AA8
		internal object Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x06000713 RID: 1811 RVA: 0x000158B0 File Offset: 0x00013AB0
		private void AssertKeyStillValid()
		{
			if (this.handle == null)
			{
				throw new ObjectDisposedException("Microsoft.Win32.RegistryKey");
			}
		}

		// Token: 0x06000714 RID: 1812 RVA: 0x000158C8 File Offset: 0x00013AC8
		private void AssertKeyNameNotNull(string subKeyName)
		{
			if (subKeyName == null)
			{
				throw new ArgumentNullException("name");
			}
		}

		// Token: 0x06000715 RID: 1813 RVA: 0x000158DC File Offset: 0x00013ADC
		private void AssertKeyNameLength(string name)
		{
			if (name.Length > 255)
			{
				throw new ArgumentException("Name of registry key cannot be greater than 255 characters");
			}
		}

		// Token: 0x06000716 RID: 1814 RVA: 0x000158FC File Offset: 0x00013AFC
		private void DeleteChildKeysAndValues()
		{
			if (this.IsRoot)
			{
				return;
			}
			string[] subKeyNames = this.GetSubKeyNames();
			foreach (string text in subKeyNames)
			{
				RegistryKey registryKey = this.OpenSubKey(text, true);
				registryKey.DeleteChildKeysAndValues();
				registryKey.Close();
				this.DeleteSubKey(text, false);
			}
			string[] valueNames = this.GetValueNames();
			foreach (string name in valueNames)
			{
				this.DeleteValue(name, false);
			}
		}

		// Token: 0x06000717 RID: 1815 RVA: 0x0001598C File Offset: 0x00013B8C
		internal static string DecodeString(byte[] data)
		{
			string text = Encoding.Unicode.GetString(data);
			int num = text.IndexOf('\0');
			if (num != -1)
			{
				text = text.TrimEnd(new char[1]);
			}
			return text;
		}

		// Token: 0x06000718 RID: 1816 RVA: 0x000159C4 File Offset: 0x00013BC4
		internal static IOException CreateMarkedForDeletionException()
		{
			throw new IOException("Illegal operation attempted on a registry key that has been marked for deletion.");
		}

		// Token: 0x06000719 RID: 1817 RVA: 0x000159D0 File Offset: 0x00013BD0
		private static string GetHiveName(RegistryHive hive)
		{
			switch (hive + -2147483648)
			{
			case (RegistryHive)0:
				return "HKEY_CLASSES_ROOT";
			case (RegistryHive)1:
				return "HKEY_CURRENT_USER";
			case (RegistryHive)2:
				return "HKEY_LOCAL_MACHINE";
			case (RegistryHive)3:
				return "HKEY_USERS";
			case (RegistryHive)4:
				return "HKEY_PERFORMANCE_DATA";
			case (RegistryHive)5:
				return "HKEY_CURRENT_CONFIG";
			case (RegistryHive)6:
				return "HKEY_DYN_DATA";
			default:
				throw new NotImplementedException(string.Format("Registry hive '{0}' is not implemented.", hive.ToString()));
			}
		}

		// Token: 0x040000DB RID: 219
		private object handle;

		// Token: 0x040000DC RID: 220
		private object hive;

		// Token: 0x040000DD RID: 221
		private readonly string qname;

		// Token: 0x040000DE RID: 222
		private readonly bool isRemoteRoot;

		// Token: 0x040000DF RID: 223
		private readonly bool isWritable;

		// Token: 0x040000E0 RID: 224
		private static readonly IRegistryApi RegistryApi;
	}
}
