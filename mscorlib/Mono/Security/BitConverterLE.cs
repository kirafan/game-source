﻿using System;

namespace Mono.Security
{
	// Token: 0x0200009F RID: 159
	internal sealed class BitConverterLE
	{
		// Token: 0x06000912 RID: 2322 RVA: 0x000234B8 File Offset: 0x000216B8
		private BitConverterLE()
		{
		}

		// Token: 0x06000913 RID: 2323 RVA: 0x000234C0 File Offset: 0x000216C0
		private unsafe static byte[] GetUShortBytes(byte* bytes)
		{
			if (BitConverter.IsLittleEndian)
			{
				return new byte[]
				{
					*bytes,
					bytes[1]
				};
			}
			return new byte[]
			{
				bytes[1],
				*bytes
			};
		}

		// Token: 0x06000914 RID: 2324 RVA: 0x000234F4 File Offset: 0x000216F4
		private unsafe static byte[] GetUIntBytes(byte* bytes)
		{
			if (BitConverter.IsLittleEndian)
			{
				return new byte[]
				{
					*bytes,
					bytes[1],
					bytes[2],
					bytes[3]
				};
			}
			return new byte[]
			{
				bytes[3],
				bytes[2],
				bytes[1],
				*bytes
			};
		}

		// Token: 0x06000915 RID: 2325 RVA: 0x0002354C File Offset: 0x0002174C
		private unsafe static byte[] GetULongBytes(byte* bytes)
		{
			if (BitConverter.IsLittleEndian)
			{
				return new byte[]
				{
					*bytes,
					bytes[1],
					bytes[2],
					bytes[3],
					bytes[4],
					bytes[5],
					bytes[6],
					bytes[7]
				};
			}
			return new byte[]
			{
				bytes[7],
				bytes[6],
				bytes[5],
				bytes[4],
				bytes[3],
				bytes[2],
				bytes[1],
				*bytes
			};
		}

		// Token: 0x06000916 RID: 2326 RVA: 0x000235DC File Offset: 0x000217DC
		internal static byte[] GetBytes(bool value)
		{
			return new byte[]
			{
				(!value) ? 0 : 1
			};
		}

		// Token: 0x06000917 RID: 2327 RVA: 0x000235F4 File Offset: 0x000217F4
		internal unsafe static byte[] GetBytes(char value)
		{
			return BitConverterLE.GetUShortBytes((byte*)(&value));
		}

		// Token: 0x06000918 RID: 2328 RVA: 0x00023600 File Offset: 0x00021800
		internal unsafe static byte[] GetBytes(short value)
		{
			return BitConverterLE.GetUShortBytes((byte*)(&value));
		}

		// Token: 0x06000919 RID: 2329 RVA: 0x0002360C File Offset: 0x0002180C
		internal unsafe static byte[] GetBytes(int value)
		{
			return BitConverterLE.GetUIntBytes((byte*)(&value));
		}

		// Token: 0x0600091A RID: 2330 RVA: 0x00023618 File Offset: 0x00021818
		internal unsafe static byte[] GetBytes(long value)
		{
			return BitConverterLE.GetULongBytes((byte*)(&value));
		}

		// Token: 0x0600091B RID: 2331 RVA: 0x00023624 File Offset: 0x00021824
		internal unsafe static byte[] GetBytes(ushort value)
		{
			return BitConverterLE.GetUShortBytes((byte*)(&value));
		}

		// Token: 0x0600091C RID: 2332 RVA: 0x00023630 File Offset: 0x00021830
		internal unsafe static byte[] GetBytes(uint value)
		{
			return BitConverterLE.GetUIntBytes((byte*)(&value));
		}

		// Token: 0x0600091D RID: 2333 RVA: 0x0002363C File Offset: 0x0002183C
		internal unsafe static byte[] GetBytes(ulong value)
		{
			return BitConverterLE.GetULongBytes((byte*)(&value));
		}

		// Token: 0x0600091E RID: 2334 RVA: 0x00023648 File Offset: 0x00021848
		internal unsafe static byte[] GetBytes(float value)
		{
			return BitConverterLE.GetUIntBytes((byte*)(&value));
		}

		// Token: 0x0600091F RID: 2335 RVA: 0x00023654 File Offset: 0x00021854
		internal unsafe static byte[] GetBytes(double value)
		{
			return BitConverterLE.GetULongBytes((byte*)(&value));
		}

		// Token: 0x06000920 RID: 2336 RVA: 0x00023660 File Offset: 0x00021860
		private unsafe static void UShortFromBytes(byte* dst, byte[] src, int startIndex)
		{
			if (BitConverter.IsLittleEndian)
			{
				*dst = src[startIndex];
				dst[1] = src[startIndex + 1];
			}
			else
			{
				*dst = src[startIndex + 1];
				dst[1] = src[startIndex];
			}
		}

		// Token: 0x06000921 RID: 2337 RVA: 0x00023690 File Offset: 0x00021890
		private unsafe static void UIntFromBytes(byte* dst, byte[] src, int startIndex)
		{
			if (BitConverter.IsLittleEndian)
			{
				*dst = src[startIndex];
				dst[1] = src[startIndex + 1];
				dst[2] = src[startIndex + 2];
				dst[3] = src[startIndex + 3];
			}
			else
			{
				*dst = src[startIndex + 3];
				dst[1] = src[startIndex + 2];
				dst[2] = src[startIndex + 1];
				dst[3] = src[startIndex];
			}
		}

		// Token: 0x06000922 RID: 2338 RVA: 0x000236EC File Offset: 0x000218EC
		private unsafe static void ULongFromBytes(byte* dst, byte[] src, int startIndex)
		{
			if (BitConverter.IsLittleEndian)
			{
				for (int i = 0; i < 8; i++)
				{
					dst[i] = src[startIndex + i];
				}
			}
			else
			{
				for (int j = 0; j < 8; j++)
				{
					dst[j] = src[startIndex + (7 - j)];
				}
			}
		}

		// Token: 0x06000923 RID: 2339 RVA: 0x00023740 File Offset: 0x00021940
		internal static bool ToBoolean(byte[] value, int startIndex)
		{
			return value[startIndex] != 0;
		}

		// Token: 0x06000924 RID: 2340 RVA: 0x0002374C File Offset: 0x0002194C
		internal unsafe static char ToChar(byte[] value, int startIndex)
		{
			char result;
			BitConverterLE.UShortFromBytes((byte*)(&result), value, startIndex);
			return result;
		}

		// Token: 0x06000925 RID: 2341 RVA: 0x00023764 File Offset: 0x00021964
		internal unsafe static short ToInt16(byte[] value, int startIndex)
		{
			short result;
			BitConverterLE.UShortFromBytes((byte*)(&result), value, startIndex);
			return result;
		}

		// Token: 0x06000926 RID: 2342 RVA: 0x0002377C File Offset: 0x0002197C
		internal unsafe static int ToInt32(byte[] value, int startIndex)
		{
			int result;
			BitConverterLE.UIntFromBytes((byte*)(&result), value, startIndex);
			return result;
		}

		// Token: 0x06000927 RID: 2343 RVA: 0x00023794 File Offset: 0x00021994
		internal unsafe static long ToInt64(byte[] value, int startIndex)
		{
			long result;
			BitConverterLE.ULongFromBytes((byte*)(&result), value, startIndex);
			return result;
		}

		// Token: 0x06000928 RID: 2344 RVA: 0x000237AC File Offset: 0x000219AC
		internal unsafe static ushort ToUInt16(byte[] value, int startIndex)
		{
			ushort result;
			BitConverterLE.UShortFromBytes((byte*)(&result), value, startIndex);
			return result;
		}

		// Token: 0x06000929 RID: 2345 RVA: 0x000237C4 File Offset: 0x000219C4
		internal unsafe static uint ToUInt32(byte[] value, int startIndex)
		{
			uint result;
			BitConverterLE.UIntFromBytes((byte*)(&result), value, startIndex);
			return result;
		}

		// Token: 0x0600092A RID: 2346 RVA: 0x000237DC File Offset: 0x000219DC
		internal unsafe static ulong ToUInt64(byte[] value, int startIndex)
		{
			ulong result;
			BitConverterLE.ULongFromBytes((byte*)(&result), value, startIndex);
			return result;
		}

		// Token: 0x0600092B RID: 2347 RVA: 0x000237F4 File Offset: 0x000219F4
		internal unsafe static float ToSingle(byte[] value, int startIndex)
		{
			float result;
			BitConverterLE.UIntFromBytes((byte*)(&result), value, startIndex);
			return result;
		}

		// Token: 0x0600092C RID: 2348 RVA: 0x0002380C File Offset: 0x00021A0C
		internal unsafe static double ToDouble(byte[] value, int startIndex)
		{
			double result;
			BitConverterLE.ULongFromBytes((byte*)(&result), value, startIndex);
			return result;
		}
	}
}
