﻿using System;
using System.Collections;

namespace Mono.Security.X509
{
	// Token: 0x020000C9 RID: 201
	[Serializable]
	internal class X509CertificateCollection : CollectionBase, IEnumerable
	{
		// Token: 0x06000B50 RID: 2896 RVA: 0x000346C4 File Offset: 0x000328C4
		public X509CertificateCollection()
		{
		}

		// Token: 0x06000B51 RID: 2897 RVA: 0x000346CC File Offset: 0x000328CC
		public X509CertificateCollection(X509Certificate[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x06000B52 RID: 2898 RVA: 0x000346DC File Offset: 0x000328DC
		public X509CertificateCollection(X509CertificateCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x06000B53 RID: 2899 RVA: 0x000346EC File Offset: 0x000328EC
		IEnumerator IEnumerable.GetEnumerator()
		{
			return base.InnerList.GetEnumerator();
		}

		// Token: 0x17000184 RID: 388
		public X509Certificate this[int index]
		{
			get
			{
				return (X509Certificate)base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}

		// Token: 0x06000B56 RID: 2902 RVA: 0x00034720 File Offset: 0x00032920
		public int Add(X509Certificate value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			return base.InnerList.Add(value);
		}

		// Token: 0x06000B57 RID: 2903 RVA: 0x00034740 File Offset: 0x00032940
		public void AddRange(X509Certificate[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				base.InnerList.Add(value[i]);
			}
		}

		// Token: 0x06000B58 RID: 2904 RVA: 0x00034784 File Offset: 0x00032984
		public void AddRange(X509CertificateCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.InnerList.Count; i++)
			{
				base.InnerList.Add(value[i]);
			}
		}

		// Token: 0x06000B59 RID: 2905 RVA: 0x000347D4 File Offset: 0x000329D4
		public bool Contains(X509Certificate value)
		{
			return this.IndexOf(value) != -1;
		}

		// Token: 0x06000B5A RID: 2906 RVA: 0x000347E4 File Offset: 0x000329E4
		public void CopyTo(X509Certificate[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		// Token: 0x06000B5B RID: 2907 RVA: 0x000347F4 File Offset: 0x000329F4
		public new X509CertificateCollection.X509CertificateEnumerator GetEnumerator()
		{
			return new X509CertificateCollection.X509CertificateEnumerator(this);
		}

		// Token: 0x06000B5C RID: 2908 RVA: 0x000347FC File Offset: 0x000329FC
		public override int GetHashCode()
		{
			return base.InnerList.GetHashCode();
		}

		// Token: 0x06000B5D RID: 2909 RVA: 0x0003480C File Offset: 0x00032A0C
		public int IndexOf(X509Certificate value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			byte[] hash = value.Hash;
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				X509Certificate x509Certificate = (X509Certificate)base.InnerList[i];
				if (this.Compare(x509Certificate.Hash, hash))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000B5E RID: 2910 RVA: 0x00034874 File Offset: 0x00032A74
		public void Insert(int index, X509Certificate value)
		{
			base.InnerList.Insert(index, value);
		}

		// Token: 0x06000B5F RID: 2911 RVA: 0x00034884 File Offset: 0x00032A84
		public void Remove(X509Certificate value)
		{
			base.InnerList.Remove(value);
		}

		// Token: 0x06000B60 RID: 2912 RVA: 0x00034894 File Offset: 0x00032A94
		private bool Compare(byte[] array1, byte[] array2)
		{
			if (array1 == null && array2 == null)
			{
				return true;
			}
			if (array1 == null || array2 == null)
			{
				return false;
			}
			if (array1.Length != array2.Length)
			{
				return false;
			}
			for (int i = 0; i < array1.Length; i++)
			{
				if (array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x020000CA RID: 202
		public class X509CertificateEnumerator : IEnumerator
		{
			// Token: 0x06000B61 RID: 2913 RVA: 0x000348EC File Offset: 0x00032AEC
			public X509CertificateEnumerator(X509CertificateCollection mappings)
			{
				this.enumerator = ((IEnumerable)mappings).GetEnumerator();
			}

			// Token: 0x17000185 RID: 389
			// (get) Token: 0x06000B62 RID: 2914 RVA: 0x00034900 File Offset: 0x00032B00
			object IEnumerator.Current
			{
				get
				{
					return this.enumerator.Current;
				}
			}

			// Token: 0x06000B63 RID: 2915 RVA: 0x00034910 File Offset: 0x00032B10
			bool IEnumerator.MoveNext()
			{
				return this.enumerator.MoveNext();
			}

			// Token: 0x06000B64 RID: 2916 RVA: 0x00034920 File Offset: 0x00032B20
			void IEnumerator.Reset()
			{
				this.enumerator.Reset();
			}

			// Token: 0x17000186 RID: 390
			// (get) Token: 0x06000B65 RID: 2917 RVA: 0x00034930 File Offset: 0x00032B30
			public X509Certificate Current
			{
				get
				{
					return (X509Certificate)this.enumerator.Current;
				}
			}

			// Token: 0x06000B66 RID: 2918 RVA: 0x00034944 File Offset: 0x00032B44
			public bool MoveNext()
			{
				return this.enumerator.MoveNext();
			}

			// Token: 0x06000B67 RID: 2919 RVA: 0x00034954 File Offset: 0x00032B54
			public void Reset()
			{
				this.enumerator.Reset();
			}

			// Token: 0x04000301 RID: 769
			private IEnumerator enumerator;
		}
	}
}
