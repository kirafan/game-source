﻿using System;

// Token: 0x02000065 RID: 101
internal sealed class Locale
{
	// Token: 0x060006D9 RID: 1753 RVA: 0x00015398 File Offset: 0x00013598
	private Locale()
	{
	}

	// Token: 0x060006DA RID: 1754 RVA: 0x000153A0 File Offset: 0x000135A0
	public static string GetText(string msg)
	{
		return msg;
	}

	// Token: 0x060006DB RID: 1755 RVA: 0x000153A4 File Offset: 0x000135A4
	public static string GetText(string fmt, params object[] args)
	{
		return string.Format(fmt, args);
	}
}
