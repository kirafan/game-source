﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000137 RID: 311
	[ComVisible(true)]
	[Serializable]
	public class FieldAccessException : MemberAccessException
	{
		// Token: 0x0600112F RID: 4399 RVA: 0x00046160 File Offset: 0x00044360
		public FieldAccessException() : base(Locale.GetText("Attempt to access a private/protected field failed."))
		{
			base.HResult = -2146233081;
		}

		// Token: 0x06001130 RID: 4400 RVA: 0x00046180 File Offset: 0x00044380
		public FieldAccessException(string message) : base(message)
		{
			base.HResult = -2146233081;
		}

		// Token: 0x06001131 RID: 4401 RVA: 0x00046194 File Offset: 0x00044394
		protected FieldAccessException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06001132 RID: 4402 RVA: 0x000461A0 File Offset: 0x000443A0
		public FieldAccessException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233081;
		}

		// Token: 0x040004FE RID: 1278
		private const int Result = -2146233081;
	}
}
