﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200005B RID: 91
	// (Invoke) Token: 0x06000690 RID: 1680
	[ComVisible(true)]
	[Serializable]
	public delegate void AsyncCallback(IAsyncResult ar);
}
