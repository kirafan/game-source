﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200001D RID: 29
	[ComVisible(true)]
	public interface ICloneable
	{
		// Token: 0x0600027B RID: 635
		object Clone();
	}
}
