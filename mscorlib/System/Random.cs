﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200016C RID: 364
	[ComVisible(true)]
	[Serializable]
	public class Random
	{
		// Token: 0x06001359 RID: 4953 RVA: 0x0004D6AC File Offset: 0x0004B8AC
		public Random() : this(Environment.TickCount)
		{
		}

		// Token: 0x0600135A RID: 4954 RVA: 0x0004D6BC File Offset: 0x0004B8BC
		public Random(int Seed)
		{
			int num = 161803398 - Math.Abs(Seed);
			this.SeedArray[55] = num;
			int num2 = 1;
			for (int i = 1; i < 55; i++)
			{
				int num3 = 21 * i % 55;
				this.SeedArray[num3] = num2;
				num2 = num - num2;
				if (num2 < 0)
				{
					num2 += int.MaxValue;
				}
				num = this.SeedArray[num3];
			}
			for (int j = 1; j < 5; j++)
			{
				for (int k = 1; k < 56; k++)
				{
					this.SeedArray[k] -= this.SeedArray[1 + (k + 30) % 55];
					if (this.SeedArray[k] < 0)
					{
						this.SeedArray[k] += int.MaxValue;
					}
				}
			}
			this.inext = 0;
			this.inextp = 31;
		}

		// Token: 0x0600135B RID: 4955 RVA: 0x0004D7B8 File Offset: 0x0004B9B8
		protected virtual double Sample()
		{
			if (++this.inext >= 56)
			{
				this.inext = 1;
			}
			if (++this.inextp >= 56)
			{
				this.inextp = 1;
			}
			int num = this.SeedArray[this.inext] - this.SeedArray[this.inextp];
			if (num < 0)
			{
				num += int.MaxValue;
			}
			this.SeedArray[this.inext] = num;
			return (double)num * 4.656612875245797E-10;
		}

		// Token: 0x0600135C RID: 4956 RVA: 0x0004D848 File Offset: 0x0004BA48
		public virtual int Next()
		{
			return (int)(this.Sample() * 2147483647.0);
		}

		// Token: 0x0600135D RID: 4957 RVA: 0x0004D85C File Offset: 0x0004BA5C
		public virtual int Next(int maxValue)
		{
			if (maxValue < 0)
			{
				throw new ArgumentOutOfRangeException(Locale.GetText("Max value is less than min value."));
			}
			return (int)(this.Sample() * (double)maxValue);
		}

		// Token: 0x0600135E RID: 4958 RVA: 0x0004D880 File Offset: 0x0004BA80
		public virtual int Next(int minValue, int maxValue)
		{
			if (minValue > maxValue)
			{
				throw new ArgumentOutOfRangeException(Locale.GetText("Min value is greater than max value."));
			}
			uint num = (uint)(maxValue - minValue);
			if (num <= 1U)
			{
				return minValue;
			}
			return (int)((ulong)((uint)(this.Sample() * num)) + (ulong)((long)minValue));
		}

		// Token: 0x0600135F RID: 4959 RVA: 0x0004D8C4 File Offset: 0x0004BAC4
		public virtual void NextBytes(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			for (int i = 0; i < buffer.Length; i++)
			{
				buffer[i] = (byte)(this.Sample() * 256.0);
			}
		}

		// Token: 0x06001360 RID: 4960 RVA: 0x0004D90C File Offset: 0x0004BB0C
		public virtual double NextDouble()
		{
			return this.Sample();
		}

		// Token: 0x0400059E RID: 1438
		private const int MBIG = 2147483647;

		// Token: 0x0400059F RID: 1439
		private const int MSEED = 161803398;

		// Token: 0x040005A0 RID: 1440
		private const int MZ = 0;

		// Token: 0x040005A1 RID: 1441
		private int inext;

		// Token: 0x040005A2 RID: 1442
		private int inextp;

		// Token: 0x040005A3 RID: 1443
		private int[] SeedArray = new int[56];
	}
}
