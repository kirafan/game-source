﻿using System;
using System.Collections;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000028 RID: 40
	[ComVisible(true)]
	[Serializable]
	public abstract class Enum : ValueType, IFormattable, IConvertible, IComparable
	{
		// Token: 0x060003B9 RID: 953 RVA: 0x0000E954 File Offset: 0x0000CB54
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this.Value, provider);
		}

		// Token: 0x060003BA RID: 954 RVA: 0x0000E964 File Offset: 0x0000CB64
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this.Value, provider);
		}

		// Token: 0x060003BB RID: 955 RVA: 0x0000E974 File Offset: 0x0000CB74
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this.Value, provider);
		}

		// Token: 0x060003BC RID: 956 RVA: 0x0000E984 File Offset: 0x0000CB84
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this.Value, provider);
		}

		// Token: 0x060003BD RID: 957 RVA: 0x0000E994 File Offset: 0x0000CB94
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this.Value, provider);
		}

		// Token: 0x060003BE RID: 958 RVA: 0x0000E9A4 File Offset: 0x0000CBA4
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this.Value, provider);
		}

		// Token: 0x060003BF RID: 959 RVA: 0x0000E9B4 File Offset: 0x0000CBB4
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this.Value, provider);
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x0000E9C4 File Offset: 0x0000CBC4
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this.Value, provider);
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x0000E9D4 File Offset: 0x0000CBD4
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this.Value, provider);
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x0000E9E4 File Offset: 0x0000CBE4
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this.Value, provider);
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x0000E9F4 File Offset: 0x0000CBF4
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this.Value, provider);
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x0000EA04 File Offset: 0x0000CC04
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			if (targetType == typeof(string))
			{
				return this.ToString(provider);
			}
			return Convert.ToType(this.Value, targetType, provider, false);
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x0000EA48 File Offset: 0x0000CC48
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this.Value, provider);
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x0000EA58 File Offset: 0x0000CC58
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this.Value, provider);
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x0000EA68 File Offset: 0x0000CC68
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this.Value, provider);
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x0000EA78 File Offset: 0x0000CC78
		public TypeCode GetTypeCode()
		{
			return Type.GetTypeCode(Enum.GetUnderlyingType(base.GetType()));
		}

		// Token: 0x060003C9 RID: 969
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern object get_value();

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x060003CA RID: 970 RVA: 0x0000EA8C File Offset: 0x0000CC8C
		private object Value
		{
			get
			{
				return this.get_value();
			}
		}

		// Token: 0x060003CB RID: 971 RVA: 0x0000EA94 File Offset: 0x0000CC94
		[ComVisible(true)]
		public static Array GetValues(Type enumType)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			MonoEnumInfo monoEnumInfo;
			MonoEnumInfo.GetInfo(enumType, out monoEnumInfo);
			return (Array)monoEnumInfo.values.Clone();
		}

		// Token: 0x060003CC RID: 972 RVA: 0x0000EAE8 File Offset: 0x0000CCE8
		[ComVisible(true)]
		public static string[] GetNames(Type enumType)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.");
			}
			MonoEnumInfo monoEnumInfo;
			MonoEnumInfo.GetInfo(enumType, out monoEnumInfo);
			return (string[])monoEnumInfo.names.Clone();
		}

		// Token: 0x060003CD RID: 973 RVA: 0x0000EB38 File Offset: 0x0000CD38
		private static int FindPosition(object value, Array values)
		{
			if (!(values is byte[]) && !(values is ushort[]) && !(values is uint[]) && !(values is ulong[]))
			{
				if (values is int[])
				{
					return Array.BinarySearch(values, value, MonoEnumInfo.int_comparer);
				}
				if (values is short[])
				{
					return Array.BinarySearch(values, value, MonoEnumInfo.short_comparer);
				}
				if (values is sbyte[])
				{
					return Array.BinarySearch(values, value, MonoEnumInfo.sbyte_comparer);
				}
				if (values is long[])
				{
					return Array.BinarySearch(values, value, MonoEnumInfo.long_comparer);
				}
			}
			return Array.BinarySearch(values, value);
		}

		// Token: 0x060003CE RID: 974 RVA: 0x0000EBDC File Offset: 0x0000CDDC
		[ComVisible(true)]
		public static string GetName(Type enumType, object value)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			value = Enum.ToObject(enumType, value);
			MonoEnumInfo monoEnumInfo;
			MonoEnumInfo.GetInfo(enumType, out monoEnumInfo);
			int num = Enum.FindPosition(value, monoEnumInfo.values);
			return (num < 0) ? null : monoEnumInfo.names[num];
		}

		// Token: 0x060003CF RID: 975 RVA: 0x0000EC5C File Offset: 0x0000CE5C
		[ComVisible(true)]
		public static bool IsDefined(Type enumType, object value)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			MonoEnumInfo monoEnumInfo;
			MonoEnumInfo.GetInfo(enumType, out monoEnumInfo);
			Type type = value.GetType();
			if (type == typeof(string))
			{
				return ((IList)monoEnumInfo.names).Contains(value);
			}
			if (type == monoEnumInfo.utype || type == enumType)
			{
				value = Enum.ToObject(enumType, value);
				MonoEnumInfo.GetInfo(enumType, out monoEnumInfo);
				return Enum.FindPosition(value, monoEnumInfo.values) >= 0;
			}
			throw new ArgumentException("The value parameter is not the correct type.It must be type String or the same type as the underlying typeof the Enum.");
		}

		// Token: 0x060003D0 RID: 976
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Type get_underlying_type(Type enumType);

		// Token: 0x060003D1 RID: 977 RVA: 0x0000ED18 File Offset: 0x0000CF18
		[ComVisible(true)]
		public static Type GetUnderlyingType(Type enumType)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			return Enum.get_underlying_type(enumType);
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x0000ED58 File Offset: 0x0000CF58
		[ComVisible(true)]
		public static object Parse(Type enumType, string value)
		{
			return Enum.Parse(enumType, value, false);
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x0000ED64 File Offset: 0x0000CF64
		private static int FindName(Hashtable name_hash, string[] names, string name, bool ignoreCase)
		{
			if (!ignoreCase)
			{
				if (name_hash != null)
				{
					object obj = name_hash[name];
					if (obj != null)
					{
						return (int)obj;
					}
				}
				else
				{
					for (int i = 0; i < names.Length; i++)
					{
						if (name == names[i])
						{
							return i;
						}
					}
				}
			}
			else
			{
				for (int j = 0; j < names.Length; j++)
				{
					if (string.Compare(name, names[j], ignoreCase, CultureInfo.InvariantCulture) == 0)
					{
						return j;
					}
				}
			}
			return -1;
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x0000EDEC File Offset: 0x0000CFEC
		private static ulong GetValue(object value, TypeCode typeCode)
		{
			switch (typeCode)
			{
			case TypeCode.SByte:
				return (ulong)((byte)((sbyte)value));
			case TypeCode.Byte:
				return (ulong)((byte)value);
			case TypeCode.Int16:
				return (ulong)((ushort)((short)value));
			case TypeCode.UInt16:
				return (ulong)((ushort)value);
			case TypeCode.Int32:
				return (ulong)((int)value);
			case TypeCode.UInt32:
				return (ulong)((uint)value);
			case TypeCode.Int64:
				return (ulong)((long)value);
			case TypeCode.UInt64:
				return (ulong)value;
			default:
				throw new ArgumentException("typeCode is not a valid type code for an Enum");
			}
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x0000EE74 File Offset: 0x0000D074
		[ComVisible(true)]
		public static object Parse(Type enumType, string value, bool ignoreCase)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			value = value.Trim();
			if (value.Length == 0)
			{
				throw new ArgumentException("An empty string is not considered a valid value.");
			}
			MonoEnumInfo monoEnumInfo;
			MonoEnumInfo.GetInfo(enumType, out monoEnumInfo);
			int num = Enum.FindName(monoEnumInfo.name_hash, monoEnumInfo.names, value, ignoreCase);
			if (num >= 0)
			{
				return monoEnumInfo.values.GetValue(num);
			}
			TypeCode typeCode = ((Enum)monoEnumInfo.values.GetValue(0)).GetTypeCode();
			if (value.IndexOf(',') != -1)
			{
				string[] array = value.Split(Enum.split_char);
				ulong num2 = 0UL;
				for (int i = 0; i < array.Length; i++)
				{
					num = Enum.FindName(monoEnumInfo.name_hash, monoEnumInfo.names, array[i].Trim(), ignoreCase);
					if (num < 0)
					{
						throw new ArgumentException("The requested value was not found.");
					}
					num2 |= Enum.GetValue(monoEnumInfo.values.GetValue(num), typeCode);
				}
				return Enum.ToObject(enumType, num2);
			}
			switch (typeCode)
			{
			case TypeCode.SByte:
			{
				sbyte value2;
				if (sbyte.TryParse(value, out value2))
				{
					return Enum.ToObject(enumType, value2);
				}
				break;
			}
			case TypeCode.Byte:
			{
				byte value3;
				if (byte.TryParse(value, out value3))
				{
					return Enum.ToObject(enumType, value3);
				}
				break;
			}
			case TypeCode.Int16:
			{
				short value4;
				if (short.TryParse(value, out value4))
				{
					return Enum.ToObject(enumType, value4);
				}
				break;
			}
			case TypeCode.UInt16:
			{
				ushort value5;
				if (ushort.TryParse(value, out value5))
				{
					return Enum.ToObject(enumType, value5);
				}
				break;
			}
			case TypeCode.Int32:
			{
				int value6;
				if (int.TryParse(value, out value6))
				{
					return Enum.ToObject(enumType, value6);
				}
				break;
			}
			case TypeCode.UInt32:
			{
				uint value7;
				if (uint.TryParse(value, out value7))
				{
					return Enum.ToObject(enumType, value7);
				}
				break;
			}
			case TypeCode.Int64:
			{
				long value8;
				if (long.TryParse(value, out value8))
				{
					return Enum.ToObject(enumType, value8);
				}
				break;
			}
			case TypeCode.UInt64:
			{
				ulong value9;
				if (ulong.TryParse(value, out value9))
				{
					return Enum.ToObject(enumType, value9);
				}
				break;
			}
			}
			throw new ArgumentException(string.Format("The requested value '{0}' was not found.", value));
		}

		// Token: 0x060003D6 RID: 982
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int compare_value_to(object other);

		// Token: 0x060003D7 RID: 983 RVA: 0x0000F0CC File Offset: 0x0000D2CC
		public int CompareTo(object target)
		{
			if (target == null)
			{
				return 1;
			}
			Type type = base.GetType();
			if (target.GetType() != type)
			{
				throw new ArgumentException(string.Format("Object must be the same type as the enum. The type passed in was {0}; the enum type was {1}.", target.GetType(), type));
			}
			return this.compare_value_to(target);
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x0000F114 File Offset: 0x0000D314
		public override string ToString()
		{
			return this.ToString("G");
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x0000F124 File Offset: 0x0000D324
		[Obsolete("Provider is ignored, just use ToString")]
		public string ToString(IFormatProvider provider)
		{
			return this.ToString("G", provider);
		}

		// Token: 0x060003DA RID: 986 RVA: 0x0000F134 File Offset: 0x0000D334
		public string ToString(string format)
		{
			if (format == string.Empty || format == null)
			{
				format = "G";
			}
			return Enum.Format(base.GetType(), this.Value, format);
		}

		// Token: 0x060003DB RID: 987 RVA: 0x0000F170 File Offset: 0x0000D370
		[Obsolete("Provider is ignored, just use ToString")]
		public string ToString(string format, IFormatProvider provider)
		{
			if (format == string.Empty || format == null)
			{
				format = "G";
			}
			return Enum.Format(base.GetType(), this.Value, format);
		}

		// Token: 0x060003DC RID: 988 RVA: 0x0000F1AC File Offset: 0x0000D3AC
		[ComVisible(true)]
		public static object ToObject(Type enumType, byte value)
		{
			return Enum.ToObject(enumType, value);
		}

		// Token: 0x060003DD RID: 989 RVA: 0x0000F1BC File Offset: 0x0000D3BC
		[ComVisible(true)]
		public static object ToObject(Type enumType, short value)
		{
			return Enum.ToObject(enumType, value);
		}

		// Token: 0x060003DE RID: 990 RVA: 0x0000F1CC File Offset: 0x0000D3CC
		[ComVisible(true)]
		public static object ToObject(Type enumType, int value)
		{
			return Enum.ToObject(enumType, value);
		}

		// Token: 0x060003DF RID: 991 RVA: 0x0000F1DC File Offset: 0x0000D3DC
		[ComVisible(true)]
		public static object ToObject(Type enumType, long value)
		{
			return Enum.ToObject(enumType, value);
		}

		// Token: 0x060003E0 RID: 992
		[ComVisible(true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern object ToObject(Type enumType, object value);

		// Token: 0x060003E1 RID: 993 RVA: 0x0000F1EC File Offset: 0x0000D3EC
		[CLSCompliant(false)]
		[ComVisible(true)]
		public static object ToObject(Type enumType, sbyte value)
		{
			return Enum.ToObject(enumType, value);
		}

		// Token: 0x060003E2 RID: 994 RVA: 0x0000F1FC File Offset: 0x0000D3FC
		[ComVisible(true)]
		[CLSCompliant(false)]
		public static object ToObject(Type enumType, ushort value)
		{
			return Enum.ToObject(enumType, value);
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x0000F20C File Offset: 0x0000D40C
		[ComVisible(true)]
		[CLSCompliant(false)]
		public static object ToObject(Type enumType, uint value)
		{
			return Enum.ToObject(enumType, value);
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x0000F21C File Offset: 0x0000D41C
		[ComVisible(true)]
		[CLSCompliant(false)]
		public static object ToObject(Type enumType, ulong value)
		{
			return Enum.ToObject(enumType, value);
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x0000F22C File Offset: 0x0000D42C
		public override bool Equals(object obj)
		{
			return ValueType.DefaultEquals(this, obj);
		}

		// Token: 0x060003E6 RID: 998
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int get_hashcode();

		// Token: 0x060003E7 RID: 999 RVA: 0x0000F238 File Offset: 0x0000D438
		public override int GetHashCode()
		{
			return this.get_hashcode();
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x0000F240 File Offset: 0x0000D440
		private static string FormatSpecifier_X(Type enumType, object value, bool upper)
		{
			switch (Type.GetTypeCode(enumType))
			{
			case TypeCode.SByte:
				return ((sbyte)value).ToString((!upper) ? "x2" : "X2");
			case TypeCode.Byte:
				return ((byte)value).ToString((!upper) ? "x2" : "X2");
			case TypeCode.Int16:
				return ((short)value).ToString((!upper) ? "x4" : "X4");
			case TypeCode.UInt16:
				return ((ushort)value).ToString((!upper) ? "x4" : "X4");
			case TypeCode.Int32:
				return ((int)value).ToString((!upper) ? "x8" : "X8");
			case TypeCode.UInt32:
				return ((uint)value).ToString((!upper) ? "x8" : "X8");
			case TypeCode.Int64:
				return ((long)value).ToString((!upper) ? "x16" : "X16");
			case TypeCode.UInt64:
				return ((ulong)value).ToString((!upper) ? "x16" : "X16");
			default:
				throw new Exception("Invalid type code for enumeration.");
			}
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x0000F3B0 File Offset: 0x0000D5B0
		private static string FormatFlags(Type enumType, object value)
		{
			string text = string.Empty;
			MonoEnumInfo monoEnumInfo;
			MonoEnumInfo.GetInfo(enumType, out monoEnumInfo);
			string text2 = value.ToString();
			if (text2 == "0")
			{
				text = Enum.GetName(enumType, value);
				if (text == null)
				{
					text = text2;
				}
				return text;
			}
			switch (((Enum)monoEnumInfo.values.GetValue(0)).GetTypeCode())
			{
			case TypeCode.SByte:
			{
				sbyte b = (sbyte)value;
				for (int i = monoEnumInfo.values.Length - 1; i >= 0; i--)
				{
					sbyte b2 = (sbyte)monoEnumInfo.values.GetValue(i);
					if ((int)b2 != 0)
					{
						if (((int)b & (int)b2) == (int)b2)
						{
							text = monoEnumInfo.names[i] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
							b = (sbyte)((int)b - (int)b2);
						}
					}
				}
				if ((int)b != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.Byte:
			{
				byte b3 = (byte)value;
				for (int j = monoEnumInfo.values.Length - 1; j >= 0; j--)
				{
					byte b4 = (byte)monoEnumInfo.values.GetValue(j);
					if (b4 != 0)
					{
						if ((b3 & b4) == b4)
						{
							text = monoEnumInfo.names[j] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
							b3 -= b4;
						}
					}
				}
				if (b3 != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.Int16:
			{
				short num = (short)value;
				for (int k = monoEnumInfo.values.Length - 1; k >= 0; k--)
				{
					short num2 = (short)monoEnumInfo.values.GetValue(k);
					if (num2 != 0)
					{
						if ((num & num2) == num2)
						{
							text = monoEnumInfo.names[k] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
							num -= num2;
						}
					}
				}
				if (num != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.UInt16:
			{
				ushort num3 = (ushort)value;
				for (int l = monoEnumInfo.values.Length - 1; l >= 0; l--)
				{
					ushort num4 = (ushort)monoEnumInfo.values.GetValue(l);
					if (num4 != 0)
					{
						if ((num3 & num4) == num4)
						{
							text = monoEnumInfo.names[l] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
							num3 -= num4;
						}
					}
				}
				if (num3 != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.Int32:
			{
				int num5 = (int)value;
				for (int m = monoEnumInfo.values.Length - 1; m >= 0; m--)
				{
					int num6 = (int)monoEnumInfo.values.GetValue(m);
					if (num6 != 0)
					{
						if ((num5 & num6) == num6)
						{
							text = monoEnumInfo.names[m] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
							num5 -= num6;
						}
					}
				}
				if (num5 != 0)
				{
					return text2;
				}
				break;
			}
			case TypeCode.UInt32:
			{
				uint num7 = (uint)value;
				for (int n = monoEnumInfo.values.Length - 1; n >= 0; n--)
				{
					uint num8 = (uint)monoEnumInfo.values.GetValue(n);
					if (num8 != 0U)
					{
						if ((num7 & num8) == num8)
						{
							text = monoEnumInfo.names[n] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
							num7 -= num8;
						}
					}
				}
				if (num7 != 0U)
				{
					return text2;
				}
				break;
			}
			case TypeCode.Int64:
			{
				long num9 = (long)value;
				for (int num10 = monoEnumInfo.values.Length - 1; num10 >= 0; num10--)
				{
					long num11 = (long)monoEnumInfo.values.GetValue(num10);
					if (num11 != 0L)
					{
						if ((num9 & num11) == num11)
						{
							text = monoEnumInfo.names[num10] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
							num9 -= num11;
						}
					}
				}
				if (num9 != 0L)
				{
					return text2;
				}
				break;
			}
			case TypeCode.UInt64:
			{
				ulong num12 = (ulong)value;
				for (int num13 = monoEnumInfo.values.Length - 1; num13 >= 0; num13--)
				{
					ulong num14 = (ulong)monoEnumInfo.values.GetValue(num13);
					if (num14 != 0UL)
					{
						if ((num12 & num14) == num14)
						{
							text = monoEnumInfo.names[num13] + ((!(text == string.Empty)) ? ", " : string.Empty) + text;
							num12 -= num14;
						}
					}
				}
				if (num12 != 0UL)
				{
					return text2;
				}
				break;
			}
			}
			if (text == string.Empty)
			{
				return text2;
			}
			return text;
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x0000F93C File Offset: 0x0000DB3C
		[ComVisible(true)]
		public static string Format(Type enumType, object value, string format)
		{
			if (enumType == null)
			{
				throw new ArgumentNullException("enumType");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}
			if (!enumType.IsEnum)
			{
				throw new ArgumentException("enumType is not an Enum type.", "enumType");
			}
			Type type = value.GetType();
			Type underlyingType = Enum.GetUnderlyingType(enumType);
			if (type.IsEnum)
			{
				if (type != enumType)
				{
					throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Object must be the same type as the enum. The type passed in was {0}; the enum type was {1}.", new object[]
					{
						type.FullName,
						enumType.FullName
					}));
				}
			}
			else if (type != underlyingType)
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Enum underlying type and the object must be the same type or object. Type passed in was {0}; the enum underlying type was {1}.", new object[]
				{
					type.FullName,
					underlyingType.FullName
				}));
			}
			if (format.Length != 1)
			{
				throw new FormatException("Format String can be only \"G\",\"g\",\"X\",\"x\",\"F\",\"f\",\"D\" or \"d\".");
			}
			char c = format[0];
			string text;
			if (c == 'G' || c == 'g')
			{
				if (!enumType.IsDefined(typeof(FlagsAttribute), false))
				{
					text = Enum.GetName(enumType, value);
					if (text == null)
					{
						text = value.ToString();
					}
					return text;
				}
				c = 'f';
			}
			if (c == 'f' || c == 'F')
			{
				return Enum.FormatFlags(enumType, value);
			}
			text = string.Empty;
			char c2 = c;
			if (c2 != 'D')
			{
				if (c2 == 'X')
				{
					return Enum.FormatSpecifier_X(enumType, value, true);
				}
				if (c2 != 'd')
				{
					if (c2 != 'x')
					{
						throw new FormatException("Format String can be only \"G\",\"g\",\"X\",\"x\",\"F\",\"f\",\"D\" or \"d\".");
					}
					return Enum.FormatSpecifier_X(enumType, value, false);
				}
			}
			if (underlyingType == typeof(ulong))
			{
				text = Convert.ToUInt64(value).ToString();
			}
			else
			{
				text = Convert.ToInt64(value).ToString();
			}
			return text;
		}

		// Token: 0x04000065 RID: 101
		private static char[] split_char = new char[]
		{
			','
		};
	}
}
