﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020006ED RID: 1773
	// (Invoke) Token: 0x060043A8 RID: 17320
	[ComVisible(true)]
	[Serializable]
	public delegate Assembly ResolveEventHandler(object sender, ResolveEventArgs args);
}
