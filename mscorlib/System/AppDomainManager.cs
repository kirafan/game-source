﻿using System;
using System.Reflection;
using System.Runtime.Hosting;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Threading;

namespace System
{
	// Token: 0x020000FA RID: 250
	[ComVisible(true)]
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
	[PermissionSet(SecurityAction.InheritanceDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
	public class AppDomainManager : MarshalByRefObject
	{
		// Token: 0x06000D1C RID: 3356 RVA: 0x0003A2B8 File Offset: 0x000384B8
		public AppDomainManager()
		{
			this._flags = AppDomainManagerInitializationOptions.None;
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x06000D1D RID: 3357 RVA: 0x0003A2C8 File Offset: 0x000384C8
		public virtual ApplicationActivator ApplicationActivator
		{
			get
			{
				if (this._activator == null)
				{
					this._activator = new ApplicationActivator();
				}
				return this._activator;
			}
		}

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x06000D1E RID: 3358 RVA: 0x0003A2E8 File Offset: 0x000384E8
		public virtual Assembly EntryAssembly
		{
			get
			{
				return Assembly.GetEntryAssembly();
			}
		}

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x06000D1F RID: 3359 RVA: 0x0003A2F0 File Offset: 0x000384F0
		[MonoTODO]
		public virtual HostExecutionContextManager HostExecutionContextManager
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06000D20 RID: 3360 RVA: 0x0003A2F8 File Offset: 0x000384F8
		public virtual HostSecurityManager HostSecurityManager
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170001DB RID: 475
		// (get) Token: 0x06000D21 RID: 3361 RVA: 0x0003A2FC File Offset: 0x000384FC
		// (set) Token: 0x06000D22 RID: 3362 RVA: 0x0003A304 File Offset: 0x00038504
		public AppDomainManagerInitializationOptions InitializationFlags
		{
			get
			{
				return this._flags;
			}
			set
			{
				this._flags = value;
			}
		}

		// Token: 0x06000D23 RID: 3363 RVA: 0x0003A310 File Offset: 0x00038510
		public virtual AppDomain CreateDomain(string friendlyName, Evidence securityInfo, AppDomainSetup appDomainInfo)
		{
			this.InitializeNewDomain(appDomainInfo);
			AppDomain appDomain = AppDomainManager.CreateDomainHelper(friendlyName, securityInfo, appDomainInfo);
			if ((this.HostSecurityManager.Flags & HostSecurityManagerOptions.HostPolicyLevel) == HostSecurityManagerOptions.HostPolicyLevel)
			{
				PolicyLevel domainPolicy = this.HostSecurityManager.DomainPolicy;
				if (domainPolicy != null)
				{
					appDomain.SetAppDomainPolicy(domainPolicy);
				}
			}
			return appDomain;
		}

		// Token: 0x06000D24 RID: 3364 RVA: 0x0003A35C File Offset: 0x0003855C
		public virtual void InitializeNewDomain(AppDomainSetup appDomainInfo)
		{
		}

		// Token: 0x06000D25 RID: 3365 RVA: 0x0003A360 File Offset: 0x00038560
		public virtual bool CheckSecuritySettings(SecurityState state)
		{
			return false;
		}

		// Token: 0x06000D26 RID: 3366 RVA: 0x0003A364 File Offset: 0x00038564
		protected static AppDomain CreateDomainHelper(string friendlyName, Evidence securityInfo, AppDomainSetup appDomainInfo)
		{
			return AppDomain.CreateDomain(friendlyName, securityInfo, appDomainInfo);
		}

		// Token: 0x0400037D RID: 893
		private ApplicationActivator _activator;

		// Token: 0x0400037E RID: 894
		private AppDomainManagerInitializationOptions _flags;
	}
}
