﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200015E RID: 350
	[ComVisible(true)]
	[Serializable]
	public class NotImplementedException : SystemException
	{
		// Token: 0x0600129B RID: 4763 RVA: 0x000495D4 File Offset: 0x000477D4
		public NotImplementedException() : base(Locale.GetText("The requested feature is not implemented."))
		{
			base.HResult = -2147467263;
		}

		// Token: 0x0600129C RID: 4764 RVA: 0x000495F4 File Offset: 0x000477F4
		public NotImplementedException(string message) : base(message)
		{
			base.HResult = -2147467263;
		}

		// Token: 0x0600129D RID: 4765 RVA: 0x00049608 File Offset: 0x00047808
		public NotImplementedException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2147467263;
		}

		// Token: 0x0600129E RID: 4766 RVA: 0x00049620 File Offset: 0x00047820
		protected NotImplementedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0400054B RID: 1355
		private const int Result = -2147467263;
	}
}
