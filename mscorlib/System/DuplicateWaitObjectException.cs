﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200012B RID: 299
	[ComVisible(true)]
	[Serializable]
	public class DuplicateWaitObjectException : ArgumentException
	{
		// Token: 0x060010DE RID: 4318 RVA: 0x00045170 File Offset: 0x00043370
		public DuplicateWaitObjectException() : base(Locale.GetText("Duplicate objects in argument."))
		{
			base.HResult = -2146233047;
		}

		// Token: 0x060010DF RID: 4319 RVA: 0x00045190 File Offset: 0x00043390
		public DuplicateWaitObjectException(string parameterName) : base(Locale.GetText("Duplicate objects in argument."), parameterName)
		{
			base.HResult = -2146233047;
		}

		// Token: 0x060010E0 RID: 4320 RVA: 0x000451B0 File Offset: 0x000433B0
		public DuplicateWaitObjectException(string parameterName, string message) : base(message, parameterName)
		{
			base.HResult = -2146233047;
		}

		// Token: 0x060010E1 RID: 4321 RVA: 0x000451C8 File Offset: 0x000433C8
		public DuplicateWaitObjectException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233047;
		}

		// Token: 0x060010E2 RID: 4322 RVA: 0x000451E0 File Offset: 0x000433E0
		protected DuplicateWaitObjectException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040004D2 RID: 1234
		private const int Result = -2146233047;
	}
}
