﻿using System;

namespace System
{
	// Token: 0x02000148 RID: 328
	public interface IServiceProvider
	{
		// Token: 0x060011C6 RID: 4550
		object GetService(Type serviceType);
	}
}
