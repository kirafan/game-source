﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000143 RID: 323
	[ComVisible(true)]
	[Serializable]
	public sealed class IndexOutOfRangeException : SystemException
	{
		// Token: 0x060011B2 RID: 4530 RVA: 0x00047090 File Offset: 0x00045290
		public IndexOutOfRangeException() : base(Locale.GetText("Array index is out of range."))
		{
		}

		// Token: 0x060011B3 RID: 4531 RVA: 0x000470A4 File Offset: 0x000452A4
		public IndexOutOfRangeException(string message) : base(message)
		{
		}

		// Token: 0x060011B4 RID: 4532 RVA: 0x000470B0 File Offset: 0x000452B0
		public IndexOutOfRangeException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060011B5 RID: 4533 RVA: 0x000470BC File Offset: 0x000452BC
		internal IndexOutOfRangeException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
