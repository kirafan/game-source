﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020006E8 RID: 1768
	// (Invoke) Token: 0x06004394 RID: 17300
	[ComVisible(true)]
	[Serializable]
	public delegate void AssemblyLoadEventHandler(object sender, AssemblyLoadEventArgs args);
}
