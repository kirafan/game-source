﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200014A RID: 330
	[ComVisible(true)]
	[Serializable]
	public enum LoaderOptimization
	{
		// Token: 0x0400051F RID: 1311
		NotSpecified,
		// Token: 0x04000520 RID: 1312
		SingleDomain,
		// Token: 0x04000521 RID: 1313
		MultiDomain,
		// Token: 0x04000522 RID: 1314
		MultiDomainHost,
		// Token: 0x04000523 RID: 1315
		[Obsolete]
		DomainMask = 3,
		// Token: 0x04000524 RID: 1316
		[Obsolete]
		DisallowBindings
	}
}
