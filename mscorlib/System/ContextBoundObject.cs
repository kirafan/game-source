﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000118 RID: 280
	[ComVisible(true)]
	[Serializable]
	public abstract class ContextBoundObject : MarshalByRefObject
	{
	}
}
