﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000161 RID: 353
	[ComVisible(true)]
	public static class Nullable
	{
		// Token: 0x060012D1 RID: 4817 RVA: 0x00049748 File Offset: 0x00047948
		public static int Compare<T>(T? value1, T? value2) where T : struct
		{
			if (!value1.has_value)
			{
				return (!value2.has_value) ? 0 : -1;
			}
			if (!value2.has_value)
			{
				return 1;
			}
			return Comparer<T>.Default.Compare(value1.value, value2.value);
		}

		// Token: 0x060012D2 RID: 4818 RVA: 0x0004979C File Offset: 0x0004799C
		public static bool Equals<T>(T? value1, T? value2) where T : struct
		{
			return value1.has_value == value2.has_value && (!value1.has_value || EqualityComparer<T>.Default.Equals(value1.value, value2.value));
		}

		// Token: 0x060012D3 RID: 4819 RVA: 0x000497DC File Offset: 0x000479DC
		public static Type GetUnderlyingType(Type nullableType)
		{
			if (nullableType == null)
			{
				throw new ArgumentNullException("nullableType");
			}
			if (nullableType.IsGenericType && nullableType.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				return nullableType.GetGenericArguments()[0];
			}
			return null;
		}
	}
}
