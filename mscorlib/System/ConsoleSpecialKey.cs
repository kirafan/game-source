﻿using System;

namespace System
{
	// Token: 0x02000117 RID: 279
	[Serializable]
	public enum ConsoleSpecialKey
	{
		// Token: 0x0400047C RID: 1148
		ControlC,
		// Token: 0x0400047D RID: 1149
		ControlBreak
	}
}
