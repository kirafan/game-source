﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000120 RID: 288
	[ComVisible(true)]
	[Serializable]
	public enum DateTimeKind
	{
		// Token: 0x040004B3 RID: 1203
		Unspecified,
		// Token: 0x040004B4 RID: 1204
		Utc,
		// Token: 0x040004B5 RID: 1205
		Local
	}
}
