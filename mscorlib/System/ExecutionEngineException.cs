﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000136 RID: 310
	[ComVisible(true)]
	[Serializable]
	public sealed class ExecutionEngineException : SystemException
	{
		// Token: 0x0600112B RID: 4395 RVA: 0x00046128 File Offset: 0x00044328
		public ExecutionEngineException() : base(Locale.GetText("Internal error occurred."))
		{
		}

		// Token: 0x0600112C RID: 4396 RVA: 0x0004613C File Offset: 0x0004433C
		public ExecutionEngineException(string message) : base(message)
		{
		}

		// Token: 0x0600112D RID: 4397 RVA: 0x00046148 File Offset: 0x00044348
		public ExecutionEngineException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600112E RID: 4398 RVA: 0x00046154 File Offset: 0x00044354
		internal ExecutionEngineException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
