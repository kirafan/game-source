﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000165 RID: 357
	[ComVisible(true)]
	[Serializable]
	public class ObjectDisposedException : InvalidOperationException
	{
		// Token: 0x0600133A RID: 4922 RVA: 0x0004D338 File Offset: 0x0004B538
		public ObjectDisposedException(string objectName) : base(Locale.GetText("The object was used after being disposed."))
		{
			this.obj_name = objectName;
			this.msg = Locale.GetText("The object was used after being disposed.");
		}

		// Token: 0x0600133B RID: 4923 RVA: 0x0004D364 File Offset: 0x0004B564
		public ObjectDisposedException(string objectName, string message) : base(message)
		{
			this.obj_name = objectName;
			this.msg = message;
		}

		// Token: 0x0600133C RID: 4924 RVA: 0x0004D37C File Offset: 0x0004B57C
		public ObjectDisposedException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600133D RID: 4925 RVA: 0x0004D388 File Offset: 0x0004B588
		protected ObjectDisposedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.obj_name = info.GetString("ObjectName");
		}

		// Token: 0x170002BF RID: 703
		// (get) Token: 0x0600133E RID: 4926 RVA: 0x0004D3A4 File Offset: 0x0004B5A4
		public override string Message
		{
			get
			{
				return this.msg;
			}
		}

		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x0600133F RID: 4927 RVA: 0x0004D3AC File Offset: 0x0004B5AC
		public string ObjectName
		{
			get
			{
				return this.obj_name;
			}
		}

		// Token: 0x06001340 RID: 4928 RVA: 0x0004D3B4 File Offset: 0x0004B5B4
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("ObjectName", this.obj_name);
		}

		// Token: 0x0400058D RID: 1421
		private string obj_name;

		// Token: 0x0400058E RID: 1422
		private string msg;
	}
}
