﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000177 RID: 375
	[ComVisible(true)]
	[Serializable]
	public enum StringComparison
	{
		// Token: 0x040005B4 RID: 1460
		CurrentCulture,
		// Token: 0x040005B5 RID: 1461
		CurrentCultureIgnoreCase,
		// Token: 0x040005B6 RID: 1462
		InvariantCulture,
		// Token: 0x040005B7 RID: 1463
		InvariantCultureIgnoreCase,
		// Token: 0x040005B8 RID: 1464
		Ordinal,
		// Token: 0x040005B9 RID: 1465
		OrdinalIgnoreCase
	}
}
