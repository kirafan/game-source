﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System
{
	// Token: 0x02000180 RID: 384
	[ComVisible(true)]
	[Serializable]
	public struct TimeSpan : IComparable, IComparable<TimeSpan>, IEquatable<TimeSpan>
	{
		// Token: 0x06001400 RID: 5120 RVA: 0x00050CFC File Offset: 0x0004EEFC
		public TimeSpan(long ticks)
		{
			this._ticks = ticks;
		}

		// Token: 0x06001401 RID: 5121 RVA: 0x00050D08 File Offset: 0x0004EF08
		public TimeSpan(int hours, int minutes, int seconds)
		{
			this._ticks = TimeSpan.CalculateTicks(0, hours, minutes, seconds, 0);
		}

		// Token: 0x06001402 RID: 5122 RVA: 0x00050D1C File Offset: 0x0004EF1C
		public TimeSpan(int days, int hours, int minutes, int seconds)
		{
			this._ticks = TimeSpan.CalculateTicks(days, hours, minutes, seconds, 0);
		}

		// Token: 0x06001403 RID: 5123 RVA: 0x00050D30 File Offset: 0x0004EF30
		public TimeSpan(int days, int hours, int minutes, int seconds, int milliseconds)
		{
			this._ticks = TimeSpan.CalculateTicks(days, hours, minutes, seconds, milliseconds);
		}

		// Token: 0x06001405 RID: 5125 RVA: 0x00050D84 File Offset: 0x0004EF84
		internal static long CalculateTicks(int days, int hours, int minutes, int seconds, int milliseconds)
		{
			int num = hours * 3600;
			int num2 = minutes * 60;
			long num3 = (long)(num + num2 + seconds) * 1000L + (long)milliseconds;
			num3 *= 10000L;
			bool flag = false;
			if (days > 0)
			{
				long num4 = 864000000000L * (long)days;
				if (num3 < 0L)
				{
					long num5 = num3;
					num3 += num4;
					flag = (num5 > num3);
				}
				else
				{
					num3 += num4;
					flag = (num3 < 0L);
				}
			}
			else if (days < 0)
			{
				long num6 = 864000000000L * (long)days;
				if (num3 <= 0L)
				{
					num3 += num6;
					flag = (num3 > 0L);
				}
				else
				{
					long num7 = num3;
					num3 += num6;
					flag = (num3 > num7);
				}
			}
			if (flag)
			{
				throw new ArgumentOutOfRangeException(Locale.GetText("The timespan is too big or too small."));
			}
			return num3;
		}

		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x06001406 RID: 5126 RVA: 0x00050E50 File Offset: 0x0004F050
		public int Days
		{
			get
			{
				return (int)(this._ticks / 864000000000L);
			}
		}

		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x06001407 RID: 5127 RVA: 0x00050E64 File Offset: 0x0004F064
		public int Hours
		{
			get
			{
				return (int)(this._ticks % 864000000000L / 36000000000L);
			}
		}

		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x06001408 RID: 5128 RVA: 0x00050E84 File Offset: 0x0004F084
		public int Milliseconds
		{
			get
			{
				return (int)(this._ticks % 10000000L / 10000L);
			}
		}

		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x06001409 RID: 5129 RVA: 0x00050E9C File Offset: 0x0004F09C
		public int Minutes
		{
			get
			{
				return (int)(this._ticks % 36000000000L / 600000000L);
			}
		}

		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x0600140A RID: 5130 RVA: 0x00050EB8 File Offset: 0x0004F0B8
		public int Seconds
		{
			get
			{
				return (int)(this._ticks % 600000000L / 10000000L);
			}
		}

		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x0600140B RID: 5131 RVA: 0x00050ED0 File Offset: 0x0004F0D0
		public long Ticks
		{
			get
			{
				return this._ticks;
			}
		}

		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x0600140C RID: 5132 RVA: 0x00050ED8 File Offset: 0x0004F0D8
		public double TotalDays
		{
			get
			{
				return (double)this._ticks / 864000000000.0;
			}
		}

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x0600140D RID: 5133 RVA: 0x00050EEC File Offset: 0x0004F0EC
		public double TotalHours
		{
			get
			{
				return (double)this._ticks / 36000000000.0;
			}
		}

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x0600140E RID: 5134 RVA: 0x00050F00 File Offset: 0x0004F100
		public double TotalMilliseconds
		{
			get
			{
				return (double)this._ticks / 10000.0;
			}
		}

		// Token: 0x170002EC RID: 748
		// (get) Token: 0x0600140F RID: 5135 RVA: 0x00050F14 File Offset: 0x0004F114
		public double TotalMinutes
		{
			get
			{
				return (double)this._ticks / 600000000.0;
			}
		}

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06001410 RID: 5136 RVA: 0x00050F28 File Offset: 0x0004F128
		public double TotalSeconds
		{
			get
			{
				return (double)this._ticks / 10000000.0;
			}
		}

		// Token: 0x06001411 RID: 5137 RVA: 0x00050F3C File Offset: 0x0004F13C
		public TimeSpan Add(TimeSpan ts)
		{
			TimeSpan result;
			try
			{
				result = new TimeSpan(checked(this._ticks + ts.Ticks));
			}
			catch (OverflowException)
			{
				throw new OverflowException(Locale.GetText("Resulting timespan is too big."));
			}
			return result;
		}

		// Token: 0x06001412 RID: 5138 RVA: 0x00050F9C File Offset: 0x0004F19C
		public static int Compare(TimeSpan t1, TimeSpan t2)
		{
			if (t1._ticks < t2._ticks)
			{
				return -1;
			}
			if (t1._ticks > t2._ticks)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x06001413 RID: 5139 RVA: 0x00050FCC File Offset: 0x0004F1CC
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is TimeSpan))
			{
				throw new ArgumentException(Locale.GetText("Argument has to be a TimeSpan."), "value");
			}
			return TimeSpan.Compare(this, (TimeSpan)value);
		}

		// Token: 0x06001414 RID: 5140 RVA: 0x00051008 File Offset: 0x0004F208
		public int CompareTo(TimeSpan value)
		{
			return TimeSpan.Compare(this, value);
		}

		// Token: 0x06001415 RID: 5141 RVA: 0x00051018 File Offset: 0x0004F218
		public bool Equals(TimeSpan obj)
		{
			return obj._ticks == this._ticks;
		}

		// Token: 0x06001416 RID: 5142 RVA: 0x0005102C File Offset: 0x0004F22C
		public TimeSpan Duration()
		{
			TimeSpan result;
			try
			{
				result = new TimeSpan(Math.Abs(this._ticks));
			}
			catch (OverflowException)
			{
				throw new OverflowException(Locale.GetText("This TimeSpan value is MinValue so you cannot get the duration."));
			}
			return result;
		}

		// Token: 0x06001417 RID: 5143 RVA: 0x00051088 File Offset: 0x0004F288
		public override bool Equals(object value)
		{
			return value is TimeSpan && this._ticks == ((TimeSpan)value)._ticks;
		}

		// Token: 0x06001418 RID: 5144 RVA: 0x000510B8 File Offset: 0x0004F2B8
		public static bool Equals(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks == t2._ticks;
		}

		// Token: 0x06001419 RID: 5145 RVA: 0x000510CC File Offset: 0x0004F2CC
		public static TimeSpan FromDays(double value)
		{
			return TimeSpan.From(value, 864000000000L);
		}

		// Token: 0x0600141A RID: 5146 RVA: 0x000510E0 File Offset: 0x0004F2E0
		public static TimeSpan FromHours(double value)
		{
			return TimeSpan.From(value, 36000000000L);
		}

		// Token: 0x0600141B RID: 5147 RVA: 0x000510F4 File Offset: 0x0004F2F4
		public static TimeSpan FromMinutes(double value)
		{
			return TimeSpan.From(value, 600000000L);
		}

		// Token: 0x0600141C RID: 5148 RVA: 0x00051104 File Offset: 0x0004F304
		public static TimeSpan FromSeconds(double value)
		{
			return TimeSpan.From(value, 10000000L);
		}

		// Token: 0x0600141D RID: 5149 RVA: 0x00051114 File Offset: 0x0004F314
		public static TimeSpan FromMilliseconds(double value)
		{
			return TimeSpan.From(value, 10000L);
		}

		// Token: 0x0600141E RID: 5150 RVA: 0x00051124 File Offset: 0x0004F324
		private static TimeSpan From(double value, long tickMultiplicator)
		{
			if (double.IsNaN(value))
			{
				throw new ArgumentException(Locale.GetText("Value cannot be NaN."), "value");
			}
			if (double.IsNegativeInfinity(value) || double.IsPositiveInfinity(value) || value < (double)TimeSpan.MinValue.Ticks || value > (double)TimeSpan.MaxValue.Ticks)
			{
				throw new OverflowException(Locale.GetText("Outside range [MinValue,MaxValue]"));
			}
			TimeSpan result;
			try
			{
				value *= (double)(tickMultiplicator / 10000L);
				checked
				{
					long num = (long)Math.Round(value);
					result = new TimeSpan(num * 10000L);
				}
			}
			catch (OverflowException)
			{
				throw new OverflowException(Locale.GetText("Resulting timespan is too big."));
			}
			return result;
		}

		// Token: 0x0600141F RID: 5151 RVA: 0x00051200 File Offset: 0x0004F400
		public static TimeSpan FromTicks(long value)
		{
			return new TimeSpan(value);
		}

		// Token: 0x06001420 RID: 5152 RVA: 0x00051208 File Offset: 0x0004F408
		public override int GetHashCode()
		{
			return this._ticks.GetHashCode();
		}

		// Token: 0x06001421 RID: 5153 RVA: 0x00051218 File Offset: 0x0004F418
		public TimeSpan Negate()
		{
			if (this._ticks == TimeSpan.MinValue._ticks)
			{
				throw new OverflowException(Locale.GetText("This TimeSpan value is MinValue and cannot be negated."));
			}
			return new TimeSpan(-this._ticks);
		}

		// Token: 0x06001422 RID: 5154 RVA: 0x0005125C File Offset: 0x0004F45C
		public static TimeSpan Parse(string s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			TimeSpan.Parser parser = new TimeSpan.Parser(s);
			return parser.Execute();
		}

		// Token: 0x06001423 RID: 5155 RVA: 0x00051288 File Offset: 0x0004F488
		public static bool TryParse(string s, out TimeSpan result)
		{
			if (s == null)
			{
				result = TimeSpan.Zero;
				return false;
			}
			bool result2;
			try
			{
				result = TimeSpan.Parse(s);
				result2 = true;
			}
			catch
			{
				result = TimeSpan.Zero;
				result2 = false;
			}
			return result2;
		}

		// Token: 0x06001424 RID: 5156 RVA: 0x000512F8 File Offset: 0x0004F4F8
		public TimeSpan Subtract(TimeSpan ts)
		{
			TimeSpan result;
			try
			{
				result = new TimeSpan(checked(this._ticks - ts.Ticks));
			}
			catch (OverflowException)
			{
				throw new OverflowException(Locale.GetText("Resulting timespan is too big."));
			}
			return result;
		}

		// Token: 0x06001425 RID: 5157 RVA: 0x00051358 File Offset: 0x0004F558
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(14);
			if (this._ticks < 0L)
			{
				stringBuilder.Append('-');
			}
			if (this.Days != 0)
			{
				stringBuilder.Append(Math.Abs(this.Days));
				stringBuilder.Append('.');
			}
			stringBuilder.Append(Math.Abs(this.Hours).ToString("D2"));
			stringBuilder.Append(':');
			stringBuilder.Append(Math.Abs(this.Minutes).ToString("D2"));
			stringBuilder.Append(':');
			stringBuilder.Append(Math.Abs(this.Seconds).ToString("D2"));
			int num = (int)Math.Abs(this._ticks % 10000000L);
			if (num != 0)
			{
				stringBuilder.Append('.');
				stringBuilder.Append(num.ToString("D7"));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06001426 RID: 5158 RVA: 0x00051458 File Offset: 0x0004F658
		public static TimeSpan operator +(TimeSpan t1, TimeSpan t2)
		{
			return t1.Add(t2);
		}

		// Token: 0x06001427 RID: 5159 RVA: 0x00051464 File Offset: 0x0004F664
		public static bool operator ==(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks == t2._ticks;
		}

		// Token: 0x06001428 RID: 5160 RVA: 0x00051478 File Offset: 0x0004F678
		public static bool operator >(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks > t2._ticks;
		}

		// Token: 0x06001429 RID: 5161 RVA: 0x0005148C File Offset: 0x0004F68C
		public static bool operator >=(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks >= t2._ticks;
		}

		// Token: 0x0600142A RID: 5162 RVA: 0x000514A4 File Offset: 0x0004F6A4
		public static bool operator !=(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks != t2._ticks;
		}

		// Token: 0x0600142B RID: 5163 RVA: 0x000514BC File Offset: 0x0004F6BC
		public static bool operator <(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks < t2._ticks;
		}

		// Token: 0x0600142C RID: 5164 RVA: 0x000514D0 File Offset: 0x0004F6D0
		public static bool operator <=(TimeSpan t1, TimeSpan t2)
		{
			return t1._ticks <= t2._ticks;
		}

		// Token: 0x0600142D RID: 5165 RVA: 0x000514E8 File Offset: 0x0004F6E8
		public static TimeSpan operator -(TimeSpan t1, TimeSpan t2)
		{
			return t1.Subtract(t2);
		}

		// Token: 0x0600142E RID: 5166 RVA: 0x000514F4 File Offset: 0x0004F6F4
		public static TimeSpan operator -(TimeSpan t)
		{
			return t.Negate();
		}

		// Token: 0x0600142F RID: 5167 RVA: 0x00051500 File Offset: 0x0004F700
		public static TimeSpan operator +(TimeSpan t)
		{
			return t;
		}

		// Token: 0x040007C9 RID: 1993
		public const long TicksPerDay = 864000000000L;

		// Token: 0x040007CA RID: 1994
		public const long TicksPerHour = 36000000000L;

		// Token: 0x040007CB RID: 1995
		public const long TicksPerMillisecond = 10000L;

		// Token: 0x040007CC RID: 1996
		public const long TicksPerMinute = 600000000L;

		// Token: 0x040007CD RID: 1997
		public const long TicksPerSecond = 10000000L;

		// Token: 0x040007CE RID: 1998
		public static readonly TimeSpan MaxValue = new TimeSpan(long.MaxValue);

		// Token: 0x040007CF RID: 1999
		public static readonly TimeSpan MinValue = new TimeSpan(long.MinValue);

		// Token: 0x040007D0 RID: 2000
		public static readonly TimeSpan Zero = new TimeSpan(0L);

		// Token: 0x040007D1 RID: 2001
		private long _ticks;

		// Token: 0x02000181 RID: 385
		private class Parser
		{
			// Token: 0x06001430 RID: 5168 RVA: 0x00051504 File Offset: 0x0004F704
			public Parser(string src)
			{
				this._src = src;
				this._length = this._src.Length;
			}

			// Token: 0x170002EE RID: 750
			// (get) Token: 0x06001431 RID: 5169 RVA: 0x00051524 File Offset: 0x0004F724
			public bool AtEnd
			{
				get
				{
					return this._cur >= this._length;
				}
			}

			// Token: 0x06001432 RID: 5170 RVA: 0x00051538 File Offset: 0x0004F738
			private void ParseWhiteSpace()
			{
				while (!this.AtEnd && char.IsWhiteSpace(this._src, this._cur))
				{
					this._cur++;
				}
			}

			// Token: 0x06001433 RID: 5171 RVA: 0x0005157C File Offset: 0x0004F77C
			private bool ParseSign()
			{
				bool result = false;
				if (!this.AtEnd && this._src[this._cur] == '-')
				{
					result = true;
					this._cur++;
				}
				return result;
			}

			// Token: 0x06001434 RID: 5172 RVA: 0x000515C0 File Offset: 0x0004F7C0
			private int ParseInt(bool optional)
			{
				if (optional && this.AtEnd)
				{
					return 0;
				}
				int num = 0;
				int num2 = 0;
				while (!this.AtEnd && char.IsDigit(this._src, this._cur))
				{
					num = checked(num * 10 + (int)this._src[this._cur] - 48);
					this._cur++;
					num2++;
				}
				if (!optional && num2 == 0)
				{
					this.formatError = true;
				}
				return num;
			}

			// Token: 0x06001435 RID: 5173 RVA: 0x0005164C File Offset: 0x0004F84C
			private bool ParseOptDot()
			{
				if (this.AtEnd)
				{
					return false;
				}
				if (this._src[this._cur] == '.')
				{
					this._cur++;
					return true;
				}
				return false;
			}

			// Token: 0x06001436 RID: 5174 RVA: 0x00051690 File Offset: 0x0004F890
			private void ParseOptColon()
			{
				if (!this.AtEnd)
				{
					if (this._src[this._cur] == ':')
					{
						this._cur++;
					}
					else
					{
						this.formatError = true;
					}
				}
			}

			// Token: 0x06001437 RID: 5175 RVA: 0x000516D0 File Offset: 0x0004F8D0
			private long ParseTicks()
			{
				long num = 1000000L;
				long num2 = 0L;
				bool flag = false;
				while (num > 0L && !this.AtEnd && char.IsDigit(this._src, this._cur))
				{
					num2 += (long)(this._src[this._cur] - '0') * num;
					this._cur++;
					num /= 10L;
					flag = true;
				}
				if (!flag)
				{
					this.formatError = true;
				}
				return num2;
			}

			// Token: 0x06001438 RID: 5176 RVA: 0x00051758 File Offset: 0x0004F958
			public TimeSpan Execute()
			{
				int num = 0;
				this.ParseWhiteSpace();
				bool flag = this.ParseSign();
				int num2 = this.ParseInt(false);
				if (this.ParseOptDot())
				{
					num = this.ParseInt(true);
				}
				else if (!this.AtEnd)
				{
					num = num2;
					num2 = 0;
				}
				this.ParseOptColon();
				int num3 = this.ParseInt(true);
				this.ParseOptColon();
				int num4 = this.ParseInt(true);
				long num5;
				if (this.ParseOptDot())
				{
					num5 = this.ParseTicks();
				}
				else
				{
					num5 = 0L;
				}
				this.ParseWhiteSpace();
				if (!this.AtEnd)
				{
					this.formatError = true;
				}
				if (num > 23 || num3 > 59 || num4 > 59)
				{
					throw new OverflowException(Locale.GetText("Invalid time data."));
				}
				if (this.formatError)
				{
					throw new FormatException(Locale.GetText("Invalid format for TimeSpan.Parse."));
				}
				long num6 = TimeSpan.CalculateTicks(num2, num, num3, num4, 0);
				num6 = checked((!flag) ? (num6 + num5) : ((long)(unchecked((ulong)0) - (ulong)num6 - (ulong)num5)));
				return new TimeSpan(num6);
			}

			// Token: 0x040007D2 RID: 2002
			private string _src;

			// Token: 0x040007D3 RID: 2003
			private int _cur;

			// Token: 0x040007D4 RID: 2004
			private int _length;

			// Token: 0x040007D5 RID: 2005
			private bool formatError;
		}
	}
}
