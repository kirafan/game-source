﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200015C RID: 348
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	[ComVisible(true)]
	public sealed class NonSerializedAttribute : Attribute
	{
	}
}
