﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000178 RID: 376
	[ComVisible(true)]
	[Serializable]
	public class SystemException : Exception
	{
		// Token: 0x060013A2 RID: 5026 RVA: 0x0004E1A0 File Offset: 0x0004C3A0
		public SystemException() : base(Locale.GetText("A system exception has occurred."))
		{
			base.HResult = -2146233087;
		}

		// Token: 0x060013A3 RID: 5027 RVA: 0x0004E1C0 File Offset: 0x0004C3C0
		public SystemException(string message) : base(message)
		{
			base.HResult = -2146233087;
		}

		// Token: 0x060013A4 RID: 5028 RVA: 0x0004E1D4 File Offset: 0x0004C3D4
		protected SystemException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060013A5 RID: 5029 RVA: 0x0004E1E0 File Offset: 0x0004C3E0
		public SystemException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233087;
		}

		// Token: 0x040005BA RID: 1466
		private const int Result = -2146233087;
	}
}
