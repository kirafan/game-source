﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000019 RID: 25
	[ComVisible(true)]
	public interface IDisposable
	{
		// Token: 0x06000173 RID: 371
		void Dispose();
	}
}
