﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;

namespace System
{
	// Token: 0x0200014D RID: 333
	public static class Math
	{
		// Token: 0x060011D0 RID: 4560 RVA: 0x000473E0 File Offset: 0x000455E0
		public static decimal Abs(decimal value)
		{
			return (!(value < 0m)) ? value : (-value);
		}

		// Token: 0x060011D1 RID: 4561 RVA: 0x00047400 File Offset: 0x00045600
		public static double Abs(double value)
		{
			return (value >= 0.0) ? value : (-value);
		}

		// Token: 0x060011D2 RID: 4562 RVA: 0x0004741C File Offset: 0x0004561C
		public static float Abs(float value)
		{
			return (value >= 0f) ? value : (-value);
		}

		// Token: 0x060011D3 RID: 4563 RVA: 0x00047434 File Offset: 0x00045634
		public static int Abs(int value)
		{
			if (value == -2147483648)
			{
				throw new OverflowException(Locale.GetText("Value is too small."));
			}
			return (value >= 0) ? value : (-value);
		}

		// Token: 0x060011D4 RID: 4564 RVA: 0x0004746C File Offset: 0x0004566C
		public static long Abs(long value)
		{
			if (value == -9223372036854775808L)
			{
				throw new OverflowException(Locale.GetText("Value is too small."));
			}
			return (value >= 0L) ? value : (-value);
		}

		// Token: 0x060011D5 RID: 4565 RVA: 0x000474A0 File Offset: 0x000456A0
		[CLSCompliant(false)]
		public static sbyte Abs(sbyte value)
		{
			if ((int)value == -128)
			{
				throw new OverflowException(Locale.GetText("Value is too small."));
			}
			return (sbyte)(((int)value >= 0) ? ((int)value) : (-(sbyte)((int)value)));
		}

		// Token: 0x060011D6 RID: 4566 RVA: 0x000474DC File Offset: 0x000456DC
		public static short Abs(short value)
		{
			if (value == -32768)
			{
				throw new OverflowException(Locale.GetText("Value is too small."));
			}
			return (value >= 0) ? value : (-value);
		}

		// Token: 0x060011D7 RID: 4567 RVA: 0x0004750C File Offset: 0x0004570C
		public static decimal Ceiling(decimal d)
		{
			decimal num = Math.Floor(d);
			if (num != d)
			{
				num = ++num;
			}
			return num;
		}

		// Token: 0x060011D8 RID: 4568 RVA: 0x00047534 File Offset: 0x00045734
		public static double Ceiling(double a)
		{
			double num = Math.Floor(a);
			if (num != a)
			{
				num += 1.0;
			}
			return num;
		}

		// Token: 0x060011D9 RID: 4569 RVA: 0x0004755C File Offset: 0x0004575C
		public static long BigMul(int a, int b)
		{
			return (long)a * (long)b;
		}

		// Token: 0x060011DA RID: 4570 RVA: 0x00047564 File Offset: 0x00045764
		public static int DivRem(int a, int b, out int result)
		{
			result = a % b;
			return a / b;
		}

		// Token: 0x060011DB RID: 4571 RVA: 0x00047570 File Offset: 0x00045770
		public static long DivRem(long a, long b, out long result)
		{
			result = a % b;
			return a / b;
		}

		// Token: 0x060011DC RID: 4572
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Floor(double d);

		// Token: 0x060011DD RID: 4573 RVA: 0x0004757C File Offset: 0x0004577C
		public static double IEEERemainder(double x, double y)
		{
			if (y == 0.0)
			{
				return double.NaN;
			}
			double num = x - y * Math.Round(x / y);
			if (num != 0.0)
			{
				return num;
			}
			return (x <= 0.0) ? BitConverter.Int64BitsToDouble(long.MinValue) : 0.0;
		}

		// Token: 0x060011DE RID: 4574 RVA: 0x000475EC File Offset: 0x000457EC
		public static double Log(double a, double newBase)
		{
			double num = Math.Log(a) / Math.Log(newBase);
			return (num != 0.0) ? num : 0.0;
		}

		// Token: 0x060011DF RID: 4575 RVA: 0x00047628 File Offset: 0x00045828
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static byte Max(byte val1, byte val2)
		{
			return (val1 <= val2) ? val2 : val1;
		}

		// Token: 0x060011E0 RID: 4576 RVA: 0x00047638 File Offset: 0x00045838
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static decimal Max(decimal val1, decimal val2)
		{
			return (!(val1 > val2)) ? val2 : val1;
		}

		// Token: 0x060011E1 RID: 4577 RVA: 0x00047650 File Offset: 0x00045850
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static double Max(double val1, double val2)
		{
			if (double.IsNaN(val1) || double.IsNaN(val2))
			{
				return double.NaN;
			}
			return (val1 <= val2) ? val2 : val1;
		}

		// Token: 0x060011E2 RID: 4578 RVA: 0x0004768C File Offset: 0x0004588C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static float Max(float val1, float val2)
		{
			if (float.IsNaN(val1) || float.IsNaN(val2))
			{
				return float.NaN;
			}
			return (val1 <= val2) ? val2 : val1;
		}

		// Token: 0x060011E3 RID: 4579 RVA: 0x000476C4 File Offset: 0x000458C4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static int Max(int val1, int val2)
		{
			return (val1 <= val2) ? val2 : val1;
		}

		// Token: 0x060011E4 RID: 4580 RVA: 0x000476D4 File Offset: 0x000458D4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static long Max(long val1, long val2)
		{
			return (val1 <= val2) ? val2 : val1;
		}

		// Token: 0x060011E5 RID: 4581 RVA: 0x000476E4 File Offset: 0x000458E4
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static sbyte Max(sbyte val1, sbyte val2)
		{
			return ((int)val1 <= (int)val2) ? val2 : val1;
		}

		// Token: 0x060011E6 RID: 4582 RVA: 0x000476F8 File Offset: 0x000458F8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static short Max(short val1, short val2)
		{
			return (val1 <= val2) ? val2 : val1;
		}

		// Token: 0x060011E7 RID: 4583 RVA: 0x00047708 File Offset: 0x00045908
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static uint Max(uint val1, uint val2)
		{
			return (val1 <= val2) ? val2 : val1;
		}

		// Token: 0x060011E8 RID: 4584 RVA: 0x00047718 File Offset: 0x00045918
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static ulong Max(ulong val1, ulong val2)
		{
			return (val1 <= val2) ? val2 : val1;
		}

		// Token: 0x060011E9 RID: 4585 RVA: 0x00047728 File Offset: 0x00045928
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static ushort Max(ushort val1, ushort val2)
		{
			return (val1 <= val2) ? val2 : val1;
		}

		// Token: 0x060011EA RID: 4586 RVA: 0x00047738 File Offset: 0x00045938
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static byte Min(byte val1, byte val2)
		{
			return (val1 >= val2) ? val2 : val1;
		}

		// Token: 0x060011EB RID: 4587 RVA: 0x00047748 File Offset: 0x00045948
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static decimal Min(decimal val1, decimal val2)
		{
			return (!(val1 < val2)) ? val2 : val1;
		}

		// Token: 0x060011EC RID: 4588 RVA: 0x00047760 File Offset: 0x00045960
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static double Min(double val1, double val2)
		{
			if (double.IsNaN(val1) || double.IsNaN(val2))
			{
				return double.NaN;
			}
			return (val1 >= val2) ? val2 : val1;
		}

		// Token: 0x060011ED RID: 4589 RVA: 0x0004779C File Offset: 0x0004599C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static float Min(float val1, float val2)
		{
			if (float.IsNaN(val1) || float.IsNaN(val2))
			{
				return float.NaN;
			}
			return (val1 >= val2) ? val2 : val1;
		}

		// Token: 0x060011EE RID: 4590 RVA: 0x000477D4 File Offset: 0x000459D4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static int Min(int val1, int val2)
		{
			return (val1 >= val2) ? val2 : val1;
		}

		// Token: 0x060011EF RID: 4591 RVA: 0x000477E4 File Offset: 0x000459E4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static long Min(long val1, long val2)
		{
			return (val1 >= val2) ? val2 : val1;
		}

		// Token: 0x060011F0 RID: 4592 RVA: 0x000477F4 File Offset: 0x000459F4
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static sbyte Min(sbyte val1, sbyte val2)
		{
			return ((int)val1 >= (int)val2) ? val2 : val1;
		}

		// Token: 0x060011F1 RID: 4593 RVA: 0x00047808 File Offset: 0x00045A08
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static short Min(short val1, short val2)
		{
			return (val1 >= val2) ? val2 : val1;
		}

		// Token: 0x060011F2 RID: 4594 RVA: 0x00047818 File Offset: 0x00045A18
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static uint Min(uint val1, uint val2)
		{
			return (val1 >= val2) ? val2 : val1;
		}

		// Token: 0x060011F3 RID: 4595 RVA: 0x00047828 File Offset: 0x00045A28
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[CLSCompliant(false)]
		public static ulong Min(ulong val1, ulong val2)
		{
			return (val1 >= val2) ? val2 : val1;
		}

		// Token: 0x060011F4 RID: 4596 RVA: 0x00047838 File Offset: 0x00045A38
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[CLSCompliant(false)]
		public static ushort Min(ushort val1, ushort val2)
		{
			return (val1 >= val2) ? val2 : val1;
		}

		// Token: 0x060011F5 RID: 4597 RVA: 0x00047848 File Offset: 0x00045A48
		public static decimal Round(decimal d)
		{
			decimal num = decimal.Floor(d);
			decimal d2 = d - num;
			if ((d2 == 0.5m && 2.0m * (num / 2.0m - decimal.Floor(num / 2.0m)) != 0m) || d2 > 0.5m)
			{
				num = ++num;
			}
			return num;
		}

		// Token: 0x060011F6 RID: 4598 RVA: 0x000478E4 File Offset: 0x00045AE4
		public static decimal Round(decimal d, int decimals)
		{
			return decimal.Round(d, decimals);
		}

		// Token: 0x060011F7 RID: 4599 RVA: 0x000478F0 File Offset: 0x00045AF0
		public static decimal Round(decimal d, MidpointRounding mode)
		{
			if (mode != MidpointRounding.ToEven && mode != MidpointRounding.AwayFromZero)
			{
				throw new ArgumentException("The value '" + mode + "' is not valid for this usage of the type MidpointRounding.", "mode");
			}
			if (mode == MidpointRounding.ToEven)
			{
				return Math.Round(d);
			}
			return Math.RoundAwayFromZero(d);
		}

		// Token: 0x060011F8 RID: 4600 RVA: 0x00047940 File Offset: 0x00045B40
		private static decimal RoundAwayFromZero(decimal d)
		{
			decimal num = decimal.Floor(d);
			decimal d2 = d - num;
			if (num >= 0m && d2 >= 0.5m)
			{
				num = ++num;
			}
			else if (num < 0m && d2 > 0.5m)
			{
				num = ++num;
			}
			return num;
		}

		// Token: 0x060011F9 RID: 4601 RVA: 0x000479BC File Offset: 0x00045BBC
		public static decimal Round(decimal d, int decimals, MidpointRounding mode)
		{
			return decimal.Round(d, decimals, mode);
		}

		// Token: 0x060011FA RID: 4602
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Round(double a);

		// Token: 0x060011FB RID: 4603 RVA: 0x000479C8 File Offset: 0x00045BC8
		public static double Round(double value, int digits)
		{
			if (digits < 0 || digits > 15)
			{
				throw new ArgumentOutOfRangeException(Locale.GetText("Value is too small or too big."));
			}
			return Math.Round2(value, digits, false);
		}

		// Token: 0x060011FC RID: 4604
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern double Round2(double value, int digits, bool away_from_zero);

		// Token: 0x060011FD RID: 4605 RVA: 0x000479F4 File Offset: 0x00045BF4
		public static double Round(double value, MidpointRounding mode)
		{
			if (mode != MidpointRounding.ToEven && mode != MidpointRounding.AwayFromZero)
			{
				throw new ArgumentException("The value '" + mode + "' is not valid for this usage of the type MidpointRounding.", "mode");
			}
			if (mode == MidpointRounding.ToEven)
			{
				return Math.Round(value);
			}
			if (value > 0.0)
			{
				return Math.Floor(value + 0.5);
			}
			return Math.Ceiling(value - 0.5);
		}

		// Token: 0x060011FE RID: 4606 RVA: 0x00047A6C File Offset: 0x00045C6C
		public static double Round(double value, int digits, MidpointRounding mode)
		{
			if (mode != MidpointRounding.ToEven && mode != MidpointRounding.AwayFromZero)
			{
				throw new ArgumentException("The value '" + mode + "' is not valid for this usage of the type MidpointRounding.", "mode");
			}
			if (mode == MidpointRounding.ToEven)
			{
				return Math.Round(value, digits);
			}
			return Math.Round2(value, digits, true);
		}

		// Token: 0x060011FF RID: 4607 RVA: 0x00047ABC File Offset: 0x00045CBC
		public static double Truncate(double d)
		{
			if (d > 0.0)
			{
				return Math.Floor(d);
			}
			if (d < 0.0)
			{
				return Math.Ceiling(d);
			}
			return d;
		}

		// Token: 0x06001200 RID: 4608 RVA: 0x00047AEC File Offset: 0x00045CEC
		public static decimal Truncate(decimal d)
		{
			return decimal.Truncate(d);
		}

		// Token: 0x06001201 RID: 4609 RVA: 0x00047AF4 File Offset: 0x00045CF4
		public static decimal Floor(decimal d)
		{
			return decimal.Floor(d);
		}

		// Token: 0x06001202 RID: 4610 RVA: 0x00047AFC File Offset: 0x00045CFC
		public static int Sign(decimal value)
		{
			if (value > 0m)
			{
				return 1;
			}
			return (!(value == 0m)) ? -1 : 0;
		}

		// Token: 0x06001203 RID: 4611 RVA: 0x00047B2C File Offset: 0x00045D2C
		public static int Sign(double value)
		{
			if (double.IsNaN(value))
			{
				throw new ArithmeticException("NAN");
			}
			if (value > 0.0)
			{
				return 1;
			}
			return (value != 0.0) ? -1 : 0;
		}

		// Token: 0x06001204 RID: 4612 RVA: 0x00047B6C File Offset: 0x00045D6C
		public static int Sign(float value)
		{
			if (float.IsNaN(value))
			{
				throw new ArithmeticException("NAN");
			}
			if (value > 0f)
			{
				return 1;
			}
			return (value != 0f) ? -1 : 0;
		}

		// Token: 0x06001205 RID: 4613 RVA: 0x00047BA4 File Offset: 0x00045DA4
		public static int Sign(int value)
		{
			if (value > 0)
			{
				return 1;
			}
			return (value != 0) ? -1 : 0;
		}

		// Token: 0x06001206 RID: 4614 RVA: 0x00047BBC File Offset: 0x00045DBC
		public static int Sign(long value)
		{
			if (value > 0L)
			{
				return 1;
			}
			return (value != 0L) ? -1 : 0;
		}

		// Token: 0x06001207 RID: 4615 RVA: 0x00047BD8 File Offset: 0x00045DD8
		[CLSCompliant(false)]
		public static int Sign(sbyte value)
		{
			if ((int)value > 0)
			{
				return 1;
			}
			return ((int)value != 0) ? -1 : 0;
		}

		// Token: 0x06001208 RID: 4616 RVA: 0x00047BF4 File Offset: 0x00045DF4
		public static int Sign(short value)
		{
			if (value > 0)
			{
				return 1;
			}
			return (value != 0) ? -1 : 0;
		}

		// Token: 0x06001209 RID: 4617
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Sin(double a);

		// Token: 0x0600120A RID: 4618
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Cos(double d);

		// Token: 0x0600120B RID: 4619
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Tan(double a);

		// Token: 0x0600120C RID: 4620
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Sinh(double value);

		// Token: 0x0600120D RID: 4621
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Cosh(double value);

		// Token: 0x0600120E RID: 4622
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Tanh(double value);

		// Token: 0x0600120F RID: 4623
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Acos(double d);

		// Token: 0x06001210 RID: 4624
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Asin(double d);

		// Token: 0x06001211 RID: 4625
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Atan(double d);

		// Token: 0x06001212 RID: 4626
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Atan2(double y, double x);

		// Token: 0x06001213 RID: 4627
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Exp(double d);

		// Token: 0x06001214 RID: 4628
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Log(double d);

		// Token: 0x06001215 RID: 4629
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Log10(double d);

		// Token: 0x06001216 RID: 4630
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Pow(double x, double y);

		// Token: 0x06001217 RID: 4631
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double Sqrt(double d);

		// Token: 0x0400052B RID: 1323
		public const double E = 2.718281828459045;

		// Token: 0x0400052C RID: 1324
		public const double PI = 3.141592653589793;
	}
}
