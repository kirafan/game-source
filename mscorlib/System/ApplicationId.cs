﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System
{
	// Token: 0x020000FE RID: 254
	[ComVisible(true)]
	[Serializable]
	public sealed class ApplicationId
	{
		// Token: 0x06000D5C RID: 3420 RVA: 0x0003A8D4 File Offset: 0x00038AD4
		public ApplicationId(byte[] publicKeyToken, string name, Version version, string processorArchitecture, string culture)
		{
			if (publicKeyToken == null)
			{
				throw new ArgumentNullException("publicKeyToken");
			}
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (version == null)
			{
				throw new ArgumentNullException("version");
			}
			this._token = (byte[])publicKeyToken.Clone();
			this._name = name;
			this._version = version;
			this._proc = processorArchitecture;
			this._culture = culture;
		}

		// Token: 0x170001EF RID: 495
		// (get) Token: 0x06000D5D RID: 3421 RVA: 0x0003A950 File Offset: 0x00038B50
		public string Culture
		{
			get
			{
				return this._culture;
			}
		}

		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x06000D5E RID: 3422 RVA: 0x0003A958 File Offset: 0x00038B58
		public string Name
		{
			get
			{
				return this._name;
			}
		}

		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x06000D5F RID: 3423 RVA: 0x0003A960 File Offset: 0x00038B60
		public string ProcessorArchitecture
		{
			get
			{
				return this._proc;
			}
		}

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x06000D60 RID: 3424 RVA: 0x0003A968 File Offset: 0x00038B68
		public byte[] PublicKeyToken
		{
			get
			{
				return (byte[])this._token.Clone();
			}
		}

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x06000D61 RID: 3425 RVA: 0x0003A97C File Offset: 0x00038B7C
		public Version Version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x06000D62 RID: 3426 RVA: 0x0003A984 File Offset: 0x00038B84
		public ApplicationId Copy()
		{
			return new ApplicationId(this._token, this._name, this._version, this._proc, this._culture);
		}

		// Token: 0x06000D63 RID: 3427 RVA: 0x0003A9AC File Offset: 0x00038BAC
		public override bool Equals(object o)
		{
			if (o == null)
			{
				return false;
			}
			ApplicationId applicationId = o as ApplicationId;
			if (applicationId == null)
			{
				return false;
			}
			if (this._name != applicationId._name)
			{
				return false;
			}
			if (this._proc != applicationId._proc)
			{
				return false;
			}
			if (this._culture != applicationId._culture)
			{
				return false;
			}
			if (!this._version.Equals(applicationId._version))
			{
				return false;
			}
			if (this._token.Length != applicationId._token.Length)
			{
				return false;
			}
			for (int i = 0; i < this._token.Length; i++)
			{
				if (this._token[i] != applicationId._token[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000D64 RID: 3428 RVA: 0x0003AA78 File Offset: 0x00038C78
		public override int GetHashCode()
		{
			int num = this._name.GetHashCode() ^ this._version.GetHashCode();
			for (int i = 0; i < this._token.Length; i++)
			{
				num ^= (int)this._token[i];
			}
			return num;
		}

		// Token: 0x06000D65 RID: 3429 RVA: 0x0003AAC4 File Offset: 0x00038CC4
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(this._name);
			if (this._culture != null)
			{
				stringBuilder.AppendFormat(", culture=\"{0}\"", this._culture);
			}
			stringBuilder.AppendFormat(", version=\"{0}\", publicKeyToken=\"", this._version);
			for (int i = 0; i < this._token.Length; i++)
			{
				stringBuilder.Append(this._token[i].ToString("X2"));
			}
			if (this._proc != null)
			{
				stringBuilder.AppendFormat("\", processorArchitecture =\"{0}\"", this._proc);
			}
			else
			{
				stringBuilder.Append("\"");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04000397 RID: 919
		private byte[] _token;

		// Token: 0x04000398 RID: 920
		private string _name;

		// Token: 0x04000399 RID: 921
		private Version _version;

		// Token: 0x0400039A RID: 922
		private string _proc;

		// Token: 0x0400039B RID: 923
		private string _culture;
	}
}
