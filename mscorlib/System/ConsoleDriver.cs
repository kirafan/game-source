﻿using System;
using System.IO;
using System.Runtime.CompilerServices;

namespace System
{
	// Token: 0x02000113 RID: 275
	internal class ConsoleDriver
	{
		// Token: 0x06000E4D RID: 3661 RVA: 0x0003C9A4 File Offset: 0x0003ABA4
		static ConsoleDriver()
		{
			if (!ConsoleDriver.IsConsole)
			{
				ConsoleDriver.driver = ConsoleDriver.CreateNullConsoleDriver();
			}
			else if (Environment.IsRunningOnWindows)
			{
				ConsoleDriver.driver = ConsoleDriver.CreateWindowsConsoleDriver();
			}
			else
			{
				string environmentVariable = Environment.GetEnvironmentVariable("TERM");
				if (environmentVariable == "dumb")
				{
					ConsoleDriver.is_console = false;
					ConsoleDriver.driver = ConsoleDriver.CreateNullConsoleDriver();
				}
				else
				{
					ConsoleDriver.driver = ConsoleDriver.CreateTermInfoDriver(environmentVariable);
				}
			}
		}

		// Token: 0x06000E4E RID: 3662 RVA: 0x0003CA20 File Offset: 0x0003AC20
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static IConsoleDriver CreateNullConsoleDriver()
		{
			return new NullConsoleDriver();
		}

		// Token: 0x06000E4F RID: 3663 RVA: 0x0003CA28 File Offset: 0x0003AC28
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static IConsoleDriver CreateWindowsConsoleDriver()
		{
			return new WindowsConsoleDriver();
		}

		// Token: 0x06000E50 RID: 3664 RVA: 0x0003CA30 File Offset: 0x0003AC30
		[MethodImpl(MethodImplOptions.NoInlining)]
		private static IConsoleDriver CreateTermInfoDriver(string term)
		{
			return new TermInfoDriver(term);
		}

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x06000E51 RID: 3665 RVA: 0x0003CA38 File Offset: 0x0003AC38
		public static bool Initialized
		{
			get
			{
				return ConsoleDriver.driver.Initialized;
			}
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06000E52 RID: 3666 RVA: 0x0003CA44 File Offset: 0x0003AC44
		// (set) Token: 0x06000E53 RID: 3667 RVA: 0x0003CA50 File Offset: 0x0003AC50
		public static ConsoleColor BackgroundColor
		{
			get
			{
				return ConsoleDriver.driver.BackgroundColor;
			}
			set
			{
				if (value < ConsoleColor.Black || value > ConsoleColor.White)
				{
					throw new ArgumentOutOfRangeException("value", "Not a ConsoleColor value.");
				}
				ConsoleDriver.driver.BackgroundColor = value;
			}
		}

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x06000E54 RID: 3668 RVA: 0x0003CA88 File Offset: 0x0003AC88
		// (set) Token: 0x06000E55 RID: 3669 RVA: 0x0003CA94 File Offset: 0x0003AC94
		public static int BufferHeight
		{
			get
			{
				return ConsoleDriver.driver.BufferHeight;
			}
			set
			{
				ConsoleDriver.driver.BufferHeight = value;
			}
		}

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x06000E56 RID: 3670 RVA: 0x0003CAA4 File Offset: 0x0003ACA4
		// (set) Token: 0x06000E57 RID: 3671 RVA: 0x0003CAB0 File Offset: 0x0003ACB0
		public static int BufferWidth
		{
			get
			{
				return ConsoleDriver.driver.BufferWidth;
			}
			set
			{
				ConsoleDriver.driver.BufferWidth = value;
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x06000E58 RID: 3672 RVA: 0x0003CAC0 File Offset: 0x0003ACC0
		public static bool CapsLock
		{
			get
			{
				return ConsoleDriver.driver.CapsLock;
			}
		}

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x06000E59 RID: 3673 RVA: 0x0003CACC File Offset: 0x0003ACCC
		// (set) Token: 0x06000E5A RID: 3674 RVA: 0x0003CAD8 File Offset: 0x0003ACD8
		public static int CursorLeft
		{
			get
			{
				return ConsoleDriver.driver.CursorLeft;
			}
			set
			{
				ConsoleDriver.driver.CursorLeft = value;
			}
		}

		// Token: 0x17000220 RID: 544
		// (get) Token: 0x06000E5B RID: 3675 RVA: 0x0003CAE8 File Offset: 0x0003ACE8
		// (set) Token: 0x06000E5C RID: 3676 RVA: 0x0003CAF4 File Offset: 0x0003ACF4
		public static int CursorSize
		{
			get
			{
				return ConsoleDriver.driver.CursorSize;
			}
			set
			{
				ConsoleDriver.driver.CursorSize = value;
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000E5D RID: 3677 RVA: 0x0003CB04 File Offset: 0x0003AD04
		// (set) Token: 0x06000E5E RID: 3678 RVA: 0x0003CB10 File Offset: 0x0003AD10
		public static int CursorTop
		{
			get
			{
				return ConsoleDriver.driver.CursorTop;
			}
			set
			{
				ConsoleDriver.driver.CursorTop = value;
			}
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x06000E5F RID: 3679 RVA: 0x0003CB20 File Offset: 0x0003AD20
		// (set) Token: 0x06000E60 RID: 3680 RVA: 0x0003CB2C File Offset: 0x0003AD2C
		public static bool CursorVisible
		{
			get
			{
				return ConsoleDriver.driver.CursorVisible;
			}
			set
			{
				ConsoleDriver.driver.CursorVisible = value;
			}
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x06000E61 RID: 3681 RVA: 0x0003CB3C File Offset: 0x0003AD3C
		public static bool KeyAvailable
		{
			get
			{
				return ConsoleDriver.driver.KeyAvailable;
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x06000E62 RID: 3682 RVA: 0x0003CB48 File Offset: 0x0003AD48
		// (set) Token: 0x06000E63 RID: 3683 RVA: 0x0003CB54 File Offset: 0x0003AD54
		public static ConsoleColor ForegroundColor
		{
			get
			{
				return ConsoleDriver.driver.ForegroundColor;
			}
			set
			{
				if (value < ConsoleColor.Black || value > ConsoleColor.White)
				{
					throw new ArgumentOutOfRangeException("value", "Not a ConsoleColor value.");
				}
				ConsoleDriver.driver.ForegroundColor = value;
			}
		}

		// Token: 0x17000225 RID: 549
		// (get) Token: 0x06000E64 RID: 3684 RVA: 0x0003CB8C File Offset: 0x0003AD8C
		public static int LargestWindowHeight
		{
			get
			{
				return ConsoleDriver.driver.LargestWindowHeight;
			}
		}

		// Token: 0x17000226 RID: 550
		// (get) Token: 0x06000E65 RID: 3685 RVA: 0x0003CB98 File Offset: 0x0003AD98
		public static int LargestWindowWidth
		{
			get
			{
				return ConsoleDriver.driver.LargestWindowWidth;
			}
		}

		// Token: 0x17000227 RID: 551
		// (get) Token: 0x06000E66 RID: 3686 RVA: 0x0003CBA4 File Offset: 0x0003ADA4
		public static bool NumberLock
		{
			get
			{
				return ConsoleDriver.driver.NumberLock;
			}
		}

		// Token: 0x17000228 RID: 552
		// (get) Token: 0x06000E67 RID: 3687 RVA: 0x0003CBB0 File Offset: 0x0003ADB0
		// (set) Token: 0x06000E68 RID: 3688 RVA: 0x0003CBBC File Offset: 0x0003ADBC
		public static string Title
		{
			get
			{
				return ConsoleDriver.driver.Title;
			}
			set
			{
				ConsoleDriver.driver.Title = value;
			}
		}

		// Token: 0x17000229 RID: 553
		// (get) Token: 0x06000E69 RID: 3689 RVA: 0x0003CBCC File Offset: 0x0003ADCC
		// (set) Token: 0x06000E6A RID: 3690 RVA: 0x0003CBD8 File Offset: 0x0003ADD8
		public static bool TreatControlCAsInput
		{
			get
			{
				return ConsoleDriver.driver.TreatControlCAsInput;
			}
			set
			{
				ConsoleDriver.driver.TreatControlCAsInput = value;
			}
		}

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x06000E6B RID: 3691 RVA: 0x0003CBE8 File Offset: 0x0003ADE8
		// (set) Token: 0x06000E6C RID: 3692 RVA: 0x0003CBF4 File Offset: 0x0003ADF4
		public static int WindowHeight
		{
			get
			{
				return ConsoleDriver.driver.WindowHeight;
			}
			set
			{
				ConsoleDriver.driver.WindowHeight = value;
			}
		}

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x06000E6D RID: 3693 RVA: 0x0003CC04 File Offset: 0x0003AE04
		// (set) Token: 0x06000E6E RID: 3694 RVA: 0x0003CC10 File Offset: 0x0003AE10
		public static int WindowLeft
		{
			get
			{
				return ConsoleDriver.driver.WindowLeft;
			}
			set
			{
				ConsoleDriver.driver.WindowLeft = value;
			}
		}

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x06000E6F RID: 3695 RVA: 0x0003CC20 File Offset: 0x0003AE20
		// (set) Token: 0x06000E70 RID: 3696 RVA: 0x0003CC2C File Offset: 0x0003AE2C
		public static int WindowTop
		{
			get
			{
				return ConsoleDriver.driver.WindowTop;
			}
			set
			{
				ConsoleDriver.driver.WindowTop = value;
			}
		}

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x06000E71 RID: 3697 RVA: 0x0003CC3C File Offset: 0x0003AE3C
		// (set) Token: 0x06000E72 RID: 3698 RVA: 0x0003CC48 File Offset: 0x0003AE48
		public static int WindowWidth
		{
			get
			{
				return ConsoleDriver.driver.WindowWidth;
			}
			set
			{
				ConsoleDriver.driver.WindowWidth = value;
			}
		}

		// Token: 0x06000E73 RID: 3699 RVA: 0x0003CC58 File Offset: 0x0003AE58
		public static void Beep(int frequency, int duration)
		{
			ConsoleDriver.driver.Beep(frequency, duration);
		}

		// Token: 0x06000E74 RID: 3700 RVA: 0x0003CC68 File Offset: 0x0003AE68
		public static void Clear()
		{
			ConsoleDriver.driver.Clear();
		}

		// Token: 0x06000E75 RID: 3701 RVA: 0x0003CC74 File Offset: 0x0003AE74
		public static void MoveBufferArea(int sourceLeft, int sourceTop, int sourceWidth, int sourceHeight, int targetLeft, int targetTop)
		{
			ConsoleDriver.MoveBufferArea(sourceLeft, sourceTop, sourceWidth, sourceHeight, targetLeft, targetTop, ' ', ConsoleColor.Black, ConsoleColor.Black);
		}

		// Token: 0x06000E76 RID: 3702 RVA: 0x0003CC94 File Offset: 0x0003AE94
		public static void MoveBufferArea(int sourceLeft, int sourceTop, int sourceWidth, int sourceHeight, int targetLeft, int targetTop, char sourceChar, ConsoleColor sourceForeColor, ConsoleColor sourceBackColor)
		{
			ConsoleDriver.driver.MoveBufferArea(sourceLeft, sourceTop, sourceWidth, sourceHeight, targetLeft, targetTop, sourceChar, sourceForeColor, sourceBackColor);
		}

		// Token: 0x06000E77 RID: 3703 RVA: 0x0003CCBC File Offset: 0x0003AEBC
		public static void Init()
		{
			ConsoleDriver.driver.Init();
		}

		// Token: 0x06000E78 RID: 3704 RVA: 0x0003CCC8 File Offset: 0x0003AEC8
		public static int Read()
		{
			return (int)ConsoleDriver.ReadKey(false).KeyChar;
		}

		// Token: 0x06000E79 RID: 3705 RVA: 0x0003CCE4 File Offset: 0x0003AEE4
		public static string ReadLine()
		{
			return ConsoleDriver.driver.ReadLine();
		}

		// Token: 0x06000E7A RID: 3706 RVA: 0x0003CCF0 File Offset: 0x0003AEF0
		public static ConsoleKeyInfo ReadKey(bool intercept)
		{
			return ConsoleDriver.driver.ReadKey(intercept);
		}

		// Token: 0x06000E7B RID: 3707 RVA: 0x0003CD00 File Offset: 0x0003AF00
		public static void ResetColor()
		{
			ConsoleDriver.driver.ResetColor();
		}

		// Token: 0x06000E7C RID: 3708 RVA: 0x0003CD0C File Offset: 0x0003AF0C
		public static void SetBufferSize(int width, int height)
		{
			ConsoleDriver.driver.SetBufferSize(width, height);
		}

		// Token: 0x06000E7D RID: 3709 RVA: 0x0003CD1C File Offset: 0x0003AF1C
		public static void SetCursorPosition(int left, int top)
		{
			ConsoleDriver.driver.SetCursorPosition(left, top);
		}

		// Token: 0x06000E7E RID: 3710 RVA: 0x0003CD2C File Offset: 0x0003AF2C
		public static void SetWindowPosition(int left, int top)
		{
			ConsoleDriver.driver.SetWindowPosition(left, top);
		}

		// Token: 0x06000E7F RID: 3711 RVA: 0x0003CD3C File Offset: 0x0003AF3C
		public static void SetWindowSize(int width, int height)
		{
			ConsoleDriver.driver.SetWindowSize(width, height);
		}

		// Token: 0x1700022E RID: 558
		// (get) Token: 0x06000E80 RID: 3712 RVA: 0x0003CD4C File Offset: 0x0003AF4C
		public static bool IsConsole
		{
			get
			{
				if (ConsoleDriver.called_isatty)
				{
					return ConsoleDriver.is_console;
				}
				ConsoleDriver.is_console = (ConsoleDriver.Isatty(MonoIO.ConsoleOutput) && ConsoleDriver.Isatty(MonoIO.ConsoleInput));
				ConsoleDriver.called_isatty = true;
				return ConsoleDriver.is_console;
			}
		}

		// Token: 0x06000E81 RID: 3713
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Isatty(IntPtr handle);

		// Token: 0x06000E82 RID: 3714
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int InternalKeyAvailable(int ms_timeout);

		// Token: 0x06000E83 RID: 3715
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal unsafe static extern bool TtySetup(string keypadXmit, string teardown, out byte[] control_characters, out int* address);

		// Token: 0x06000E84 RID: 3716
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool SetEcho(bool wantEcho);

		// Token: 0x06000E85 RID: 3717
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool SetBreak(bool wantBreak);

		// Token: 0x040003DF RID: 991
		internal static IConsoleDriver driver;

		// Token: 0x040003E0 RID: 992
		private static bool is_console;

		// Token: 0x040003E1 RID: 993
		private static bool called_isatty;
	}
}
