﻿using System;
using System.Globalization;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200001F RID: 31
	[ComVisible(true)]
	[Serializable]
	public struct Single : IFormattable, IConvertible, IComparable, IComparable<float>, IEquatable<float>
	{
		// Token: 0x0600027D RID: 637 RVA: 0x0000ACF4 File Offset: 0x00008EF4
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this);
		}

		// Token: 0x0600027E RID: 638 RVA: 0x0000AD00 File Offset: 0x00008F00
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this);
		}

		// Token: 0x0600027F RID: 639 RVA: 0x0000AD0C File Offset: 0x00008F0C
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this);
		}

		// Token: 0x06000280 RID: 640 RVA: 0x0000AD18 File Offset: 0x00008F18
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this);
		}

		// Token: 0x06000281 RID: 641 RVA: 0x0000AD24 File Offset: 0x00008F24
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this);
		}

		// Token: 0x06000282 RID: 642 RVA: 0x0000AD30 File Offset: 0x00008F30
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this);
		}

		// Token: 0x06000283 RID: 643 RVA: 0x0000AD3C File Offset: 0x00008F3C
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		// Token: 0x06000284 RID: 644 RVA: 0x0000AD48 File Offset: 0x00008F48
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this);
		}

		// Token: 0x06000285 RID: 645 RVA: 0x0000AD54 File Offset: 0x00008F54
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		// Token: 0x06000286 RID: 646 RVA: 0x0000AD60 File Offset: 0x00008F60
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this);
		}

		// Token: 0x06000287 RID: 647 RVA: 0x0000AD6C File Offset: 0x00008F6C
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this);
		}

		// Token: 0x06000288 RID: 648 RVA: 0x0000AD78 File Offset: 0x00008F78
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			return Convert.ToType(this, targetType, provider, false);
		}

		// Token: 0x06000289 RID: 649 RVA: 0x0000ADA8 File Offset: 0x00008FA8
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this);
		}

		// Token: 0x0600028A RID: 650 RVA: 0x0000ADB4 File Offset: 0x00008FB4
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		// Token: 0x0600028B RID: 651 RVA: 0x0000ADC0 File Offset: 0x00008FC0
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		// Token: 0x0600028C RID: 652 RVA: 0x0000ADCC File Offset: 0x00008FCC
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is float))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.Single."));
			}
			float num = (float)value;
			if (float.IsPositiveInfinity(this) && float.IsPositiveInfinity(num))
			{
				return 0;
			}
			if (float.IsNegativeInfinity(this) && float.IsNegativeInfinity(num))
			{
				return 0;
			}
			if (float.IsNaN(num))
			{
				if (float.IsNaN(this))
				{
					return 0;
				}
				return 1;
			}
			else if (float.IsNaN(this))
			{
				if (float.IsNaN(num))
				{
					return 0;
				}
				return -1;
			}
			else
			{
				if (this == num)
				{
					return 0;
				}
				if (this > num)
				{
					return 1;
				}
				return -1;
			}
		}

		// Token: 0x0600028D RID: 653 RVA: 0x0000AE80 File Offset: 0x00009080
		public override bool Equals(object obj)
		{
			if (!(obj is float))
			{
				return false;
			}
			float num = (float)obj;
			if (float.IsNaN(num))
			{
				return float.IsNaN(this);
			}
			return num == this;
		}

		// Token: 0x0600028E RID: 654 RVA: 0x0000AEBC File Offset: 0x000090BC
		public int CompareTo(float value)
		{
			if (float.IsPositiveInfinity(this) && float.IsPositiveInfinity(value))
			{
				return 0;
			}
			if (float.IsNegativeInfinity(this) && float.IsNegativeInfinity(value))
			{
				return 0;
			}
			if (float.IsNaN(value))
			{
				if (float.IsNaN(this))
				{
					return 0;
				}
				return 1;
			}
			else if (float.IsNaN(this))
			{
				if (float.IsNaN(value))
				{
					return 0;
				}
				return -1;
			}
			else
			{
				if (this == value)
				{
					return 0;
				}
				if (this > value)
				{
					return 1;
				}
				return -1;
			}
		}

		// Token: 0x0600028F RID: 655 RVA: 0x0000AF48 File Offset: 0x00009148
		public bool Equals(float obj)
		{
			if (float.IsNaN(obj))
			{
				return float.IsNaN(this);
			}
			return obj == this;
		}

		// Token: 0x06000290 RID: 656 RVA: 0x0000AF64 File Offset: 0x00009164
		public override int GetHashCode()
		{
			return (int)this;
		}

		// Token: 0x06000291 RID: 657 RVA: 0x0000AF78 File Offset: 0x00009178
		public static bool IsInfinity(float f)
		{
			return f == float.PositiveInfinity || f == float.NegativeInfinity;
		}

		// Token: 0x06000292 RID: 658 RVA: 0x0000AF90 File Offset: 0x00009190
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static bool IsNaN(float f)
		{
			return f != f;
		}

		// Token: 0x06000293 RID: 659 RVA: 0x0000AF9C File Offset: 0x0000919C
		public static bool IsNegativeInfinity(float f)
		{
			return f < 0f && (f == float.NegativeInfinity || f == float.PositiveInfinity);
		}

		// Token: 0x06000294 RID: 660 RVA: 0x0000AFD0 File Offset: 0x000091D0
		public static bool IsPositiveInfinity(float f)
		{
			return f > 0f && (f == float.NegativeInfinity || f == float.PositiveInfinity);
		}

		// Token: 0x06000295 RID: 661 RVA: 0x0000B004 File Offset: 0x00009204
		public static float Parse(string s)
		{
			double num = double.Parse(s, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent, null);
			if (num - 3.4028234663852886E+38 > 3.6147112457961776E+29 && !double.IsPositiveInfinity(num))
			{
				throw new OverflowException();
			}
			return (float)num;
		}

		// Token: 0x06000296 RID: 662 RVA: 0x0000B04C File Offset: 0x0000924C
		public static float Parse(string s, IFormatProvider provider)
		{
			double num = double.Parse(s, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent, provider);
			if (num - 3.4028234663852886E+38 > 3.6147112457961776E+29 && !double.IsPositiveInfinity(num))
			{
				throw new OverflowException();
			}
			return (float)num;
		}

		// Token: 0x06000297 RID: 663 RVA: 0x0000B094 File Offset: 0x00009294
		public static float Parse(string s, NumberStyles style)
		{
			double num = double.Parse(s, style, null);
			if (num - 3.4028234663852886E+38 > 3.6147112457961776E+29 && !double.IsPositiveInfinity(num))
			{
				throw new OverflowException();
			}
			return (float)num;
		}

		// Token: 0x06000298 RID: 664 RVA: 0x0000B0D8 File Offset: 0x000092D8
		public static float Parse(string s, NumberStyles style, IFormatProvider provider)
		{
			double num = double.Parse(s, style, provider);
			if (num - 3.4028234663852886E+38 > 3.6147112457961776E+29 && !double.IsPositiveInfinity(num))
			{
				throw new OverflowException();
			}
			return (float)num;
		}

		// Token: 0x06000299 RID: 665 RVA: 0x0000B11C File Offset: 0x0000931C
		public static bool TryParse(string s, NumberStyles style, IFormatProvider provider, out float result)
		{
			double num;
			Exception ex;
			if (!double.Parse(s, style, provider, true, out num, out ex))
			{
				result = 0f;
				return false;
			}
			if (num - 3.4028234663852886E+38 > 3.6147112457961776E+29 && !double.IsPositiveInfinity(num))
			{
				result = 0f;
				return false;
			}
			result = (float)num;
			return true;
		}

		// Token: 0x0600029A RID: 666 RVA: 0x0000B178 File Offset: 0x00009378
		public static bool TryParse(string s, out float result)
		{
			return float.TryParse(s, NumberStyles.Any, null, out result);
		}

		// Token: 0x0600029B RID: 667 RVA: 0x0000B188 File Offset: 0x00009388
		public override string ToString()
		{
			return NumberFormatter.NumberToString(this, null);
		}

		// Token: 0x0600029C RID: 668 RVA: 0x0000B194 File Offset: 0x00009394
		public string ToString(IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(this, provider);
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000B1A0 File Offset: 0x000093A0
		public string ToString(string format)
		{
			return this.ToString(format, null);
		}

		// Token: 0x0600029E RID: 670 RVA: 0x0000B1AC File Offset: 0x000093AC
		public string ToString(string format, IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(format, this, provider);
		}

		// Token: 0x0600029F RID: 671 RVA: 0x0000B1B8 File Offset: 0x000093B8
		public TypeCode GetTypeCode()
		{
			return TypeCode.Single;
		}

		// Token: 0x0400002C RID: 44
		public const float Epsilon = 1E-45f;

		// Token: 0x0400002D RID: 45
		public const float MaxValue = 3.4028235E+38f;

		// Token: 0x0400002E RID: 46
		public const float MinValue = -3.4028235E+38f;

		// Token: 0x0400002F RID: 47
		public const float NaN = float.NaN;

		// Token: 0x04000030 RID: 48
		public const float PositiveInfinity = float.PositiveInfinity;

		// Token: 0x04000031 RID: 49
		public const float NegativeInfinity = float.NegativeInfinity;

		// Token: 0x04000032 RID: 50
		private const double MaxValueEpsilon = 3.6147112457961776E+29;

		// Token: 0x04000033 RID: 51
		internal float m_value;
	}
}
