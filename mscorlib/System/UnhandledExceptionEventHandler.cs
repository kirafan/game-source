﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020006EE RID: 1774
	// (Invoke) Token: 0x060043AC RID: 17324
	[ComVisible(true)]
	[Serializable]
	public delegate void UnhandledExceptionEventHandler(object sender, UnhandledExceptionEventArgs e);
}
