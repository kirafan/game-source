﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000016 RID: 22
	[CLSCompliant(false)]
	[ComVisible(true)]
	[Serializable]
	public struct UInt16 : IFormattable, IConvertible, IComparable, IComparable<ushort>, IEquatable<ushort>
	{
		// Token: 0x06000150 RID: 336 RVA: 0x00005CB4 File Offset: 0x00003EB4
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this);
		}

		// Token: 0x06000151 RID: 337 RVA: 0x00005CC0 File Offset: 0x00003EC0
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this);
		}

		// Token: 0x06000152 RID: 338 RVA: 0x00005CCC File Offset: 0x00003ECC
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this);
		}

		// Token: 0x06000153 RID: 339 RVA: 0x00005CD8 File Offset: 0x00003ED8
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this);
		}

		// Token: 0x06000154 RID: 340 RVA: 0x00005CE4 File Offset: 0x00003EE4
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this);
		}

		// Token: 0x06000155 RID: 341 RVA: 0x00005CF0 File Offset: 0x00003EF0
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this);
		}

		// Token: 0x06000156 RID: 342 RVA: 0x00005CFC File Offset: 0x00003EFC
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		// Token: 0x06000157 RID: 343 RVA: 0x00005D08 File Offset: 0x00003F08
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this);
		}

		// Token: 0x06000158 RID: 344 RVA: 0x00005D14 File Offset: 0x00003F14
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00005D20 File Offset: 0x00003F20
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this);
		}

		// Token: 0x0600015A RID: 346 RVA: 0x00005D2C File Offset: 0x00003F2C
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this);
		}

		// Token: 0x0600015B RID: 347 RVA: 0x00005D38 File Offset: 0x00003F38
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			return Convert.ToType(this, targetType, provider, false);
		}

		// Token: 0x0600015C RID: 348 RVA: 0x00005D68 File Offset: 0x00003F68
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return this;
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00005D6C File Offset: 0x00003F6C
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		// Token: 0x0600015E RID: 350 RVA: 0x00005D78 File Offset: 0x00003F78
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		// Token: 0x0600015F RID: 351 RVA: 0x00005D84 File Offset: 0x00003F84
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is ushort))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.UInt16."));
			}
			return (int)(this - (ushort)value);
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00005DC0 File Offset: 0x00003FC0
		public override bool Equals(object obj)
		{
			return obj is ushort && (ushort)obj == this;
		}

		// Token: 0x06000161 RID: 353 RVA: 0x00005DDC File Offset: 0x00003FDC
		public override int GetHashCode()
		{
			return (int)this;
		}

		// Token: 0x06000162 RID: 354 RVA: 0x00005DE0 File Offset: 0x00003FE0
		public int CompareTo(ushort value)
		{
			return (int)(this - value);
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00005DE8 File Offset: 0x00003FE8
		public bool Equals(ushort obj)
		{
			return obj == this;
		}

		// Token: 0x06000164 RID: 356 RVA: 0x00005DF0 File Offset: 0x00003FF0
		[CLSCompliant(false)]
		public static ushort Parse(string s, IFormatProvider provider)
		{
			return ushort.Parse(s, NumberStyles.Integer, provider);
		}

		// Token: 0x06000165 RID: 357 RVA: 0x00005DFC File Offset: 0x00003FFC
		[CLSCompliant(false)]
		public static ushort Parse(string s, NumberStyles style)
		{
			return ushort.Parse(s, style, null);
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00005E08 File Offset: 0x00004008
		[CLSCompliant(false)]
		public static ushort Parse(string s, NumberStyles style, IFormatProvider provider)
		{
			uint num = uint.Parse(s, style, provider);
			if (num > 65535U)
			{
				throw new OverflowException(Locale.GetText("Value too large."));
			}
			return (ushort)num;
		}

		// Token: 0x06000167 RID: 359 RVA: 0x00005E3C File Offset: 0x0000403C
		[CLSCompliant(false)]
		public static ushort Parse(string s)
		{
			return ushort.Parse(s, NumberStyles.Number, null);
		}

		// Token: 0x06000168 RID: 360 RVA: 0x00005E48 File Offset: 0x00004048
		[CLSCompliant(false)]
		public static bool TryParse(string s, out ushort result)
		{
			return ushort.TryParse(s, NumberStyles.Integer, null, out result);
		}

		// Token: 0x06000169 RID: 361 RVA: 0x00005E54 File Offset: 0x00004054
		[CLSCompliant(false)]
		public static bool TryParse(string s, NumberStyles style, IFormatProvider provider, out ushort result)
		{
			result = 0;
			uint num;
			if (!uint.TryParse(s, style, provider, out num))
			{
				return false;
			}
			if (num > 65535U)
			{
				return false;
			}
			result = (ushort)num;
			return true;
		}

		// Token: 0x0600016A RID: 362 RVA: 0x00005E88 File Offset: 0x00004088
		public override string ToString()
		{
			return NumberFormatter.NumberToString((int)this, null);
		}

		// Token: 0x0600016B RID: 363 RVA: 0x00005E94 File Offset: 0x00004094
		public string ToString(IFormatProvider provider)
		{
			return NumberFormatter.NumberToString((int)this, provider);
		}

		// Token: 0x0600016C RID: 364 RVA: 0x00005EA0 File Offset: 0x000040A0
		public string ToString(string format)
		{
			return this.ToString(format, null);
		}

		// Token: 0x0600016D RID: 365 RVA: 0x00005EAC File Offset: 0x000040AC
		public string ToString(string format, IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(format, this, provider);
		}

		// Token: 0x0600016E RID: 366 RVA: 0x00005EB8 File Offset: 0x000040B8
		public TypeCode GetTypeCode()
		{
			return TypeCode.UInt16;
		}

		// Token: 0x0400001B RID: 27
		public const ushort MaxValue = 65535;

		// Token: 0x0400001C RID: 28
		public const ushort MinValue = 0;

		// Token: 0x0400001D RID: 29
		internal ushort m_value;
	}
}
