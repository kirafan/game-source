﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000168 RID: 360
	[ComVisible(true)]
	[Serializable]
	public class OutOfMemoryException : SystemException
	{
		// Token: 0x0600134D RID: 4941 RVA: 0x0004D5A4 File Offset: 0x0004B7A4
		public OutOfMemoryException() : base(Locale.GetText("Out of memory."))
		{
			base.HResult = -2147024882;
		}

		// Token: 0x0600134E RID: 4942 RVA: 0x0004D5C4 File Offset: 0x0004B7C4
		public OutOfMemoryException(string message) : base(message)
		{
			base.HResult = -2147024882;
		}

		// Token: 0x0600134F RID: 4943 RVA: 0x0004D5D8 File Offset: 0x0004B7D8
		public OutOfMemoryException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147024882;
		}

		// Token: 0x06001350 RID: 4944 RVA: 0x0004D5F0 File Offset: 0x0004B7F0
		protected OutOfMemoryException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000593 RID: 1427
		private const int Result = -2147024882;
	}
}
