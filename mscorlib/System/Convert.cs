﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace System
{
	// Token: 0x0200011C RID: 284
	public static class Convert
	{
		// Token: 0x06000E9C RID: 3740
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern byte[] InternalFromBase64String(string str, bool allowWhitespaceOnly);

		// Token: 0x06000E9D RID: 3741
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern byte[] InternalFromBase64CharArray(char[] arr, int offset, int length);

		// Token: 0x06000E9E RID: 3742 RVA: 0x0003D0C0 File Offset: 0x0003B2C0
		public static byte[] FromBase64CharArray(char[] inArray, int offset, int length)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset < 0");
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length < 0");
			}
			if (offset > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offset + length > array.Length");
			}
			return Convert.InternalFromBase64CharArray(inArray, offset, length);
		}

		// Token: 0x06000E9F RID: 3743 RVA: 0x0003D120 File Offset: 0x0003B320
		public static byte[] FromBase64String(string s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			if (s.Length == 0)
			{
				return new byte[0];
			}
			return Convert.InternalFromBase64String(s, true);
		}

		// Token: 0x06000EA0 RID: 3744 RVA: 0x0003D158 File Offset: 0x0003B358
		public static TypeCode GetTypeCode(object value)
		{
			if (value == null)
			{
				return TypeCode.Empty;
			}
			return Type.GetTypeCode(value.GetType());
		}

		// Token: 0x06000EA1 RID: 3745 RVA: 0x0003D170 File Offset: 0x0003B370
		public static bool IsDBNull(object value)
		{
			return value is DBNull;
		}

		// Token: 0x06000EA2 RID: 3746 RVA: 0x0003D180 File Offset: 0x0003B380
		public static int ToBase64CharArray(byte[] inArray, int offsetIn, int length, char[] outArray, int offsetOut)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (outArray == null)
			{
				throw new ArgumentNullException("outArray");
			}
			if (offsetIn < 0 || length < 0 || offsetOut < 0)
			{
				throw new ArgumentOutOfRangeException("offsetIn, length, offsetOut < 0");
			}
			if (offsetIn > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offsetIn + length > array.Length");
			}
			byte[] bytes = ToBase64Transform.InternalTransformFinalBlock(inArray, offsetIn, length);
			char[] chars = new ASCIIEncoding().GetChars(bytes);
			if (offsetOut > outArray.Length - chars.Length)
			{
				throw new ArgumentOutOfRangeException("offsetOut + cOutArr.Length > outArray.Length");
			}
			Array.Copy(chars, 0, outArray, offsetOut, chars.Length);
			return chars.Length;
		}

		// Token: 0x06000EA3 RID: 3747 RVA: 0x0003D224 File Offset: 0x0003B424
		public static string ToBase64String(byte[] inArray)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			return Convert.ToBase64String(inArray, 0, inArray.Length);
		}

		// Token: 0x06000EA4 RID: 3748 RVA: 0x0003D244 File Offset: 0x0003B444
		public static string ToBase64String(byte[] inArray, int offset, int length)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (offset < 0 || length < 0)
			{
				throw new ArgumentOutOfRangeException("offset < 0 || length < 0");
			}
			if (offset > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offset + length > array.Length");
			}
			byte[] bytes = ToBase64Transform.InternalTransformFinalBlock(inArray, offset, length);
			return new ASCIIEncoding().GetString(bytes);
		}

		// Token: 0x06000EA5 RID: 3749 RVA: 0x0003D2A8 File Offset: 0x0003B4A8
		[ComVisible(false)]
		public static string ToBase64String(byte[] inArray, Base64FormattingOptions options)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			return Convert.ToBase64String(inArray, 0, inArray.Length, options);
		}

		// Token: 0x06000EA6 RID: 3750 RVA: 0x0003D2C8 File Offset: 0x0003B4C8
		[ComVisible(false)]
		public static string ToBase64String(byte[] inArray, int offset, int length, Base64FormattingOptions options)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (offset < 0 || length < 0)
			{
				throw new ArgumentOutOfRangeException("offset < 0 || length < 0");
			}
			if (offset > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offset + length > array.Length");
			}
			if (length == 0)
			{
				return string.Empty;
			}
			if (options == Base64FormattingOptions.InsertLineBreaks)
			{
				return Convert.ToBase64StringBuilderWithLine(inArray, offset, length).ToString();
			}
			return Encoding.ASCII.GetString(ToBase64Transform.InternalTransformFinalBlock(inArray, offset, length));
		}

		// Token: 0x06000EA7 RID: 3751 RVA: 0x0003D348 File Offset: 0x0003B548
		[ComVisible(false)]
		public static int ToBase64CharArray(byte[] inArray, int offsetIn, int length, char[] outArray, int offsetOut, Base64FormattingOptions options)
		{
			if (inArray == null)
			{
				throw new ArgumentNullException("inArray");
			}
			if (outArray == null)
			{
				throw new ArgumentNullException("outArray");
			}
			if (offsetIn < 0 || length < 0 || offsetOut < 0)
			{
				throw new ArgumentOutOfRangeException("offsetIn, length, offsetOut < 0");
			}
			if (offsetIn > inArray.Length - length)
			{
				throw new ArgumentOutOfRangeException("offsetIn + length > array.Length");
			}
			if (length == 0)
			{
				return 0;
			}
			if (options == Base64FormattingOptions.InsertLineBreaks)
			{
				StringBuilder stringBuilder = Convert.ToBase64StringBuilderWithLine(inArray, offsetIn, length);
				stringBuilder.CopyTo(0, outArray, offsetOut, stringBuilder.Length);
				return stringBuilder.Length;
			}
			byte[] bytes = ToBase64Transform.InternalTransformFinalBlock(inArray, offsetIn, length);
			char[] chars = Encoding.ASCII.GetChars(bytes);
			if (offsetOut > outArray.Length - chars.Length)
			{
				throw new ArgumentOutOfRangeException("offsetOut + cOutArr.Length > outArray.Length");
			}
			Array.Copy(chars, 0, outArray, offsetOut, chars.Length);
			return chars.Length;
		}

		// Token: 0x06000EA8 RID: 3752 RVA: 0x0003D41C File Offset: 0x0003B61C
		private static StringBuilder ToBase64StringBuilderWithLine(byte[] inArray, int offset, int length)
		{
			StringBuilder stringBuilder = new StringBuilder();
			int num2;
			int num = Math.DivRem(length, 57, out num2);
			for (int i = 0; i < num; i++)
			{
				byte[] bytes = ToBase64Transform.InternalTransformFinalBlock(inArray, offset, 57);
				stringBuilder.AppendLine(Encoding.ASCII.GetString(bytes));
				offset += 57;
			}
			if (num2 == 0)
			{
				int length2 = Environment.NewLine.Length;
				stringBuilder.Remove(stringBuilder.Length - length2, length2);
			}
			else
			{
				byte[] bytes2 = ToBase64Transform.InternalTransformFinalBlock(inArray, offset, num2);
				stringBuilder.Append(Encoding.ASCII.GetString(bytes2));
			}
			return stringBuilder;
		}

		// Token: 0x06000EA9 RID: 3753 RVA: 0x0003D4B8 File Offset: 0x0003B6B8
		public static bool ToBoolean(bool value)
		{
			return value;
		}

		// Token: 0x06000EAA RID: 3754 RVA: 0x0003D4BC File Offset: 0x0003B6BC
		public static bool ToBoolean(byte value)
		{
			return value != 0;
		}

		// Token: 0x06000EAB RID: 3755 RVA: 0x0003D4C8 File Offset: 0x0003B6C8
		public static bool ToBoolean(char value)
		{
			throw new InvalidCastException(Locale.GetText("Can't convert char to bool"));
		}

		// Token: 0x06000EAC RID: 3756 RVA: 0x0003D4DC File Offset: 0x0003B6DC
		public static bool ToBoolean(DateTime value)
		{
			throw new InvalidCastException(Locale.GetText("Can't convert date to bool"));
		}

		// Token: 0x06000EAD RID: 3757 RVA: 0x0003D4F0 File Offset: 0x0003B6F0
		public static bool ToBoolean(decimal value)
		{
			return value != 0m;
		}

		// Token: 0x06000EAE RID: 3758 RVA: 0x0003D500 File Offset: 0x0003B700
		public static bool ToBoolean(double value)
		{
			return value != 0.0;
		}

		// Token: 0x06000EAF RID: 3759 RVA: 0x0003D514 File Offset: 0x0003B714
		public static bool ToBoolean(float value)
		{
			return value != 0f;
		}

		// Token: 0x06000EB0 RID: 3760 RVA: 0x0003D524 File Offset: 0x0003B724
		public static bool ToBoolean(int value)
		{
			return value != 0;
		}

		// Token: 0x06000EB1 RID: 3761 RVA: 0x0003D530 File Offset: 0x0003B730
		public static bool ToBoolean(long value)
		{
			return value != 0L;
		}

		// Token: 0x06000EB2 RID: 3762 RVA: 0x0003D53C File Offset: 0x0003B73C
		[CLSCompliant(false)]
		public static bool ToBoolean(sbyte value)
		{
			return (int)value != 0;
		}

		// Token: 0x06000EB3 RID: 3763 RVA: 0x0003D548 File Offset: 0x0003B748
		public static bool ToBoolean(short value)
		{
			return value != 0;
		}

		// Token: 0x06000EB4 RID: 3764 RVA: 0x0003D554 File Offset: 0x0003B754
		public static bool ToBoolean(string value)
		{
			return value != null && bool.Parse(value);
		}

		// Token: 0x06000EB5 RID: 3765 RVA: 0x0003D564 File Offset: 0x0003B764
		public static bool ToBoolean(string value, IFormatProvider provider)
		{
			return value != null && bool.Parse(value);
		}

		// Token: 0x06000EB6 RID: 3766 RVA: 0x0003D574 File Offset: 0x0003B774
		[CLSCompliant(false)]
		public static bool ToBoolean(uint value)
		{
			return value != 0U;
		}

		// Token: 0x06000EB7 RID: 3767 RVA: 0x0003D580 File Offset: 0x0003B780
		[CLSCompliant(false)]
		public static bool ToBoolean(ulong value)
		{
			return value != 0UL;
		}

		// Token: 0x06000EB8 RID: 3768 RVA: 0x0003D58C File Offset: 0x0003B78C
		[CLSCompliant(false)]
		public static bool ToBoolean(ushort value)
		{
			return value != 0;
		}

		// Token: 0x06000EB9 RID: 3769 RVA: 0x0003D598 File Offset: 0x0003B798
		public static bool ToBoolean(object value)
		{
			return value != null && Convert.ToBoolean(value, null);
		}

		// Token: 0x06000EBA RID: 3770 RVA: 0x0003D5AC File Offset: 0x0003B7AC
		public static bool ToBoolean(object value, IFormatProvider provider)
		{
			return value != null && ((IConvertible)value).ToBoolean(provider);
		}

		// Token: 0x06000EBB RID: 3771 RVA: 0x0003D5C4 File Offset: 0x0003B7C4
		public static byte ToByte(bool value)
		{
			return (!value) ? 0 : 1;
		}

		// Token: 0x06000EBC RID: 3772 RVA: 0x0003D5D4 File Offset: 0x0003B7D4
		public static byte ToByte(byte value)
		{
			return value;
		}

		// Token: 0x06000EBD RID: 3773 RVA: 0x0003D5D8 File Offset: 0x0003B7D8
		public static byte ToByte(char value)
		{
			if (value > 'ÿ')
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue"));
			}
			return (byte)value;
		}

		// Token: 0x06000EBE RID: 3774 RVA: 0x0003D5F8 File Offset: 0x0003B7F8
		public static byte ToByte(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EBF RID: 3775 RVA: 0x0003D604 File Offset: 0x0003B804
		public static byte ToByte(decimal value)
		{
			if (value > 255m || value < 0m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			return (byte)Math.Round(value);
		}

		// Token: 0x06000EC0 RID: 3776 RVA: 0x0003D654 File Offset: 0x0003B854
		public static byte ToByte(double value)
		{
			if (value > 255.0 || value < 0.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			if (double.IsNaN(value) || double.IsInfinity(value))
			{
				throw new OverflowException(Locale.GetText("Value is equal to Double.NaN, Double.PositiveInfinity, or Double.NegativeInfinity"));
			}
			return (byte)Math.Round(value);
		}

		// Token: 0x06000EC1 RID: 3777 RVA: 0x0003D6BC File Offset: 0x0003B8BC
		public static byte ToByte(float value)
		{
			if (value > 255f || value < 0f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.Minalue"));
			}
			if (float.IsNaN(value) || float.IsInfinity(value))
			{
				throw new OverflowException(Locale.GetText("Value is equal to Single.NaN, Single.PositiveInfinity, or Single.NegativeInfinity"));
			}
			return (byte)Math.Round((double)value);
		}

		// Token: 0x06000EC2 RID: 3778 RVA: 0x0003D720 File Offset: 0x0003B920
		public static byte ToByte(int value)
		{
			if (value > 255 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			return (byte)value;
		}

		// Token: 0x06000EC3 RID: 3779 RVA: 0x0003D754 File Offset: 0x0003B954
		public static byte ToByte(long value)
		{
			if (value > 255L || value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			return (byte)value;
		}

		// Token: 0x06000EC4 RID: 3780 RVA: 0x0003D788 File Offset: 0x0003B988
		[CLSCompliant(false)]
		public static byte ToByte(sbyte value)
		{
			if ((int)value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than Byte.MinValue"));
			}
			return (byte)value;
		}

		// Token: 0x06000EC5 RID: 3781 RVA: 0x0003D7A4 File Offset: 0x0003B9A4
		public static byte ToByte(short value)
		{
			if (value > 255 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue or less than Byte.MinValue"));
			}
			return (byte)value;
		}

		// Token: 0x06000EC6 RID: 3782 RVA: 0x0003D7D8 File Offset: 0x0003B9D8
		public static byte ToByte(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return byte.Parse(value);
		}

		// Token: 0x06000EC7 RID: 3783 RVA: 0x0003D7E8 File Offset: 0x0003B9E8
		public static byte ToByte(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return byte.Parse(value, provider);
		}

		// Token: 0x06000EC8 RID: 3784 RVA: 0x0003D7FC File Offset: 0x0003B9FC
		public static byte ToByte(string value, int fromBase)
		{
			int num = Convert.ConvertFromBase(value, fromBase, true);
			if (num < 0 || num > 255)
			{
				throw new OverflowException();
			}
			return (byte)num;
		}

		// Token: 0x06000EC9 RID: 3785 RVA: 0x0003D82C File Offset: 0x0003BA2C
		[CLSCompliant(false)]
		public static byte ToByte(uint value)
		{
			if (value > 255U)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue"));
			}
			return (byte)value;
		}

		// Token: 0x06000ECA RID: 3786 RVA: 0x0003D84C File Offset: 0x0003BA4C
		[CLSCompliant(false)]
		public static byte ToByte(ulong value)
		{
			if (value > 255UL)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue"));
			}
			return (byte)value;
		}

		// Token: 0x06000ECB RID: 3787 RVA: 0x0003D86C File Offset: 0x0003BA6C
		[CLSCompliant(false)]
		public static byte ToByte(ushort value)
		{
			if (value > 255)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Byte.MaxValue"));
			}
			return (byte)value;
		}

		// Token: 0x06000ECC RID: 3788 RVA: 0x0003D88C File Offset: 0x0003BA8C
		public static byte ToByte(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return Convert.ToByte(value, null);
		}

		// Token: 0x06000ECD RID: 3789 RVA: 0x0003D8A0 File Offset: 0x0003BAA0
		public static byte ToByte(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToByte(provider);
		}

		// Token: 0x06000ECE RID: 3790 RVA: 0x0003D8B8 File Offset: 0x0003BAB8
		public static char ToChar(bool value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000ECF RID: 3791 RVA: 0x0003D8C4 File Offset: 0x0003BAC4
		public static char ToChar(byte value)
		{
			return (char)value;
		}

		// Token: 0x06000ED0 RID: 3792 RVA: 0x0003D8C8 File Offset: 0x0003BAC8
		public static char ToChar(char value)
		{
			return value;
		}

		// Token: 0x06000ED1 RID: 3793 RVA: 0x0003D8CC File Offset: 0x0003BACC
		public static char ToChar(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000ED2 RID: 3794 RVA: 0x0003D8D8 File Offset: 0x0003BAD8
		public static char ToChar(decimal value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000ED3 RID: 3795 RVA: 0x0003D8E4 File Offset: 0x0003BAE4
		public static char ToChar(double value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000ED4 RID: 3796 RVA: 0x0003D8F0 File Offset: 0x0003BAF0
		public static char ToChar(int value)
		{
			if (value > 65535 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Char.MaxValue or less than Char.MinValue"));
			}
			return (char)value;
		}

		// Token: 0x06000ED5 RID: 3797 RVA: 0x0003D924 File Offset: 0x0003BB24
		public static char ToChar(long value)
		{
			if (value > 65535L || value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Char.MaxValue or less than Char.MinValue"));
			}
			return (char)value;
		}

		// Token: 0x06000ED6 RID: 3798 RVA: 0x0003D958 File Offset: 0x0003BB58
		public static char ToChar(float value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000ED7 RID: 3799 RVA: 0x0003D964 File Offset: 0x0003BB64
		[CLSCompliant(false)]
		public static char ToChar(sbyte value)
		{
			if ((int)value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than Char.MinValue"));
			}
			return (char)value;
		}

		// Token: 0x06000ED8 RID: 3800 RVA: 0x0003D980 File Offset: 0x0003BB80
		public static char ToChar(short value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than Char.MinValue"));
			}
			return (char)value;
		}

		// Token: 0x06000ED9 RID: 3801 RVA: 0x0003D99C File Offset: 0x0003BB9C
		public static char ToChar(string value)
		{
			return char.Parse(value);
		}

		// Token: 0x06000EDA RID: 3802 RVA: 0x0003D9A4 File Offset: 0x0003BBA4
		public static char ToChar(string value, IFormatProvider provider)
		{
			return char.Parse(value);
		}

		// Token: 0x06000EDB RID: 3803 RVA: 0x0003D9AC File Offset: 0x0003BBAC
		[CLSCompliant(false)]
		public static char ToChar(uint value)
		{
			if (value > 65535U)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Char.MaxValue"));
			}
			return (char)value;
		}

		// Token: 0x06000EDC RID: 3804 RVA: 0x0003D9CC File Offset: 0x0003BBCC
		[CLSCompliant(false)]
		public static char ToChar(ulong value)
		{
			if (value > 65535UL)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Char.MaxValue"));
			}
			return (char)value;
		}

		// Token: 0x06000EDD RID: 3805 RVA: 0x0003D9EC File Offset: 0x0003BBEC
		[CLSCompliant(false)]
		public static char ToChar(ushort value)
		{
			return (char)value;
		}

		// Token: 0x06000EDE RID: 3806 RVA: 0x0003D9F0 File Offset: 0x0003BBF0
		public static char ToChar(object value)
		{
			if (value == null)
			{
				return '\0';
			}
			return Convert.ToChar(value, null);
		}

		// Token: 0x06000EDF RID: 3807 RVA: 0x0003DA04 File Offset: 0x0003BC04
		public static char ToChar(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return '\0';
			}
			return ((IConvertible)value).ToChar(provider);
		}

		// Token: 0x06000EE0 RID: 3808 RVA: 0x0003DA1C File Offset: 0x0003BC1C
		public static DateTime ToDateTime(string value)
		{
			if (value == null)
			{
				return DateTime.MinValue;
			}
			return DateTime.Parse(value);
		}

		// Token: 0x06000EE1 RID: 3809 RVA: 0x0003DA30 File Offset: 0x0003BC30
		public static DateTime ToDateTime(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return DateTime.MinValue;
			}
			return DateTime.Parse(value, provider);
		}

		// Token: 0x06000EE2 RID: 3810 RVA: 0x0003DA48 File Offset: 0x0003BC48
		public static DateTime ToDateTime(bool value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EE3 RID: 3811 RVA: 0x0003DA54 File Offset: 0x0003BC54
		public static DateTime ToDateTime(byte value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EE4 RID: 3812 RVA: 0x0003DA60 File Offset: 0x0003BC60
		public static DateTime ToDateTime(char value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EE5 RID: 3813 RVA: 0x0003DA6C File Offset: 0x0003BC6C
		public static DateTime ToDateTime(DateTime value)
		{
			return value;
		}

		// Token: 0x06000EE6 RID: 3814 RVA: 0x0003DA70 File Offset: 0x0003BC70
		public static DateTime ToDateTime(decimal value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EE7 RID: 3815 RVA: 0x0003DA7C File Offset: 0x0003BC7C
		public static DateTime ToDateTime(double value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EE8 RID: 3816 RVA: 0x0003DA88 File Offset: 0x0003BC88
		public static DateTime ToDateTime(short value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EE9 RID: 3817 RVA: 0x0003DA94 File Offset: 0x0003BC94
		public static DateTime ToDateTime(int value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EEA RID: 3818 RVA: 0x0003DAA0 File Offset: 0x0003BCA0
		public static DateTime ToDateTime(long value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EEB RID: 3819 RVA: 0x0003DAAC File Offset: 0x0003BCAC
		public static DateTime ToDateTime(float value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EEC RID: 3820 RVA: 0x0003DAB8 File Offset: 0x0003BCB8
		public static DateTime ToDateTime(object value)
		{
			if (value == null)
			{
				return DateTime.MinValue;
			}
			return Convert.ToDateTime(value, null);
		}

		// Token: 0x06000EED RID: 3821 RVA: 0x0003DAD0 File Offset: 0x0003BCD0
		public static DateTime ToDateTime(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return DateTime.MinValue;
			}
			return ((IConvertible)value).ToDateTime(provider);
		}

		// Token: 0x06000EEE RID: 3822 RVA: 0x0003DAEC File Offset: 0x0003BCEC
		[CLSCompliant(false)]
		public static DateTime ToDateTime(sbyte value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EEF RID: 3823 RVA: 0x0003DAF8 File Offset: 0x0003BCF8
		[CLSCompliant(false)]
		public static DateTime ToDateTime(ushort value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EF0 RID: 3824 RVA: 0x0003DB04 File Offset: 0x0003BD04
		[CLSCompliant(false)]
		public static DateTime ToDateTime(uint value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EF1 RID: 3825 RVA: 0x0003DB10 File Offset: 0x0003BD10
		[CLSCompliant(false)]
		public static DateTime ToDateTime(ulong value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EF2 RID: 3826 RVA: 0x0003DB1C File Offset: 0x0003BD1C
		public static decimal ToDecimal(bool value)
		{
			return (!value) ? 0 : 1;
		}

		// Token: 0x06000EF3 RID: 3827 RVA: 0x0003DB30 File Offset: 0x0003BD30
		public static decimal ToDecimal(byte value)
		{
			return value;
		}

		// Token: 0x06000EF4 RID: 3828 RVA: 0x0003DB38 File Offset: 0x0003BD38
		public static decimal ToDecimal(char value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EF5 RID: 3829 RVA: 0x0003DB44 File Offset: 0x0003BD44
		public static decimal ToDecimal(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000EF6 RID: 3830 RVA: 0x0003DB50 File Offset: 0x0003BD50
		public static decimal ToDecimal(decimal value)
		{
			return value;
		}

		// Token: 0x06000EF7 RID: 3831 RVA: 0x0003DB54 File Offset: 0x0003BD54
		public static decimal ToDecimal(double value)
		{
			return (decimal)value;
		}

		// Token: 0x06000EF8 RID: 3832 RVA: 0x0003DB5C File Offset: 0x0003BD5C
		public static decimal ToDecimal(float value)
		{
			return (decimal)value;
		}

		// Token: 0x06000EF9 RID: 3833 RVA: 0x0003DB64 File Offset: 0x0003BD64
		public static decimal ToDecimal(int value)
		{
			return value;
		}

		// Token: 0x06000EFA RID: 3834 RVA: 0x0003DB6C File Offset: 0x0003BD6C
		public static decimal ToDecimal(long value)
		{
			return value;
		}

		// Token: 0x06000EFB RID: 3835 RVA: 0x0003DB74 File Offset: 0x0003BD74
		[CLSCompliant(false)]
		public static decimal ToDecimal(sbyte value)
		{
			return value;
		}

		// Token: 0x06000EFC RID: 3836 RVA: 0x0003DB7C File Offset: 0x0003BD7C
		public static decimal ToDecimal(short value)
		{
			return value;
		}

		// Token: 0x06000EFD RID: 3837 RVA: 0x0003DB84 File Offset: 0x0003BD84
		public static decimal ToDecimal(string value)
		{
			if (value == null)
			{
				return 0m;
			}
			return decimal.Parse(value);
		}

		// Token: 0x06000EFE RID: 3838 RVA: 0x0003DB9C File Offset: 0x0003BD9C
		public static decimal ToDecimal(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0m;
			}
			return decimal.Parse(value, provider);
		}

		// Token: 0x06000EFF RID: 3839 RVA: 0x0003DBB4 File Offset: 0x0003BDB4
		[CLSCompliant(false)]
		public static decimal ToDecimal(uint value)
		{
			return value;
		}

		// Token: 0x06000F00 RID: 3840 RVA: 0x0003DBBC File Offset: 0x0003BDBC
		[CLSCompliant(false)]
		public static decimal ToDecimal(ulong value)
		{
			return value;
		}

		// Token: 0x06000F01 RID: 3841 RVA: 0x0003DBC4 File Offset: 0x0003BDC4
		[CLSCompliant(false)]
		public static decimal ToDecimal(ushort value)
		{
			return value;
		}

		// Token: 0x06000F02 RID: 3842 RVA: 0x0003DBCC File Offset: 0x0003BDCC
		public static decimal ToDecimal(object value)
		{
			if (value == null)
			{
				return 0m;
			}
			return Convert.ToDecimal(value, null);
		}

		// Token: 0x06000F03 RID: 3843 RVA: 0x0003DBE4 File Offset: 0x0003BDE4
		public static decimal ToDecimal(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0m;
			}
			return ((IConvertible)value).ToDecimal(provider);
		}

		// Token: 0x06000F04 RID: 3844 RVA: 0x0003DC00 File Offset: 0x0003BE00
		public static double ToDouble(bool value)
		{
			return (double)((!value) ? 0 : 1);
		}

		// Token: 0x06000F05 RID: 3845 RVA: 0x0003DC10 File Offset: 0x0003BE10
		public static double ToDouble(byte value)
		{
			return (double)value;
		}

		// Token: 0x06000F06 RID: 3846 RVA: 0x0003DC14 File Offset: 0x0003BE14
		public static double ToDouble(char value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000F07 RID: 3847 RVA: 0x0003DC20 File Offset: 0x0003BE20
		public static double ToDouble(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000F08 RID: 3848 RVA: 0x0003DC2C File Offset: 0x0003BE2C
		public static double ToDouble(decimal value)
		{
			return (double)value;
		}

		// Token: 0x06000F09 RID: 3849 RVA: 0x0003DC34 File Offset: 0x0003BE34
		public static double ToDouble(double value)
		{
			return value;
		}

		// Token: 0x06000F0A RID: 3850 RVA: 0x0003DC38 File Offset: 0x0003BE38
		public static double ToDouble(float value)
		{
			return (double)value;
		}

		// Token: 0x06000F0B RID: 3851 RVA: 0x0003DC3C File Offset: 0x0003BE3C
		public static double ToDouble(int value)
		{
			return (double)value;
		}

		// Token: 0x06000F0C RID: 3852 RVA: 0x0003DC40 File Offset: 0x0003BE40
		public static double ToDouble(long value)
		{
			return (double)value;
		}

		// Token: 0x06000F0D RID: 3853 RVA: 0x0003DC44 File Offset: 0x0003BE44
		[CLSCompliant(false)]
		public static double ToDouble(sbyte value)
		{
			return (double)value;
		}

		// Token: 0x06000F0E RID: 3854 RVA: 0x0003DC48 File Offset: 0x0003BE48
		public static double ToDouble(short value)
		{
			return (double)value;
		}

		// Token: 0x06000F0F RID: 3855 RVA: 0x0003DC4C File Offset: 0x0003BE4C
		public static double ToDouble(string value)
		{
			if (value == null)
			{
				return 0.0;
			}
			return double.Parse(value);
		}

		// Token: 0x06000F10 RID: 3856 RVA: 0x0003DC64 File Offset: 0x0003BE64
		public static double ToDouble(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0.0;
			}
			return double.Parse(value, provider);
		}

		// Token: 0x06000F11 RID: 3857 RVA: 0x0003DC80 File Offset: 0x0003BE80
		[CLSCompliant(false)]
		public static double ToDouble(uint value)
		{
			return value;
		}

		// Token: 0x06000F12 RID: 3858 RVA: 0x0003DC88 File Offset: 0x0003BE88
		[CLSCompliant(false)]
		public static double ToDouble(ulong value)
		{
			return value;
		}

		// Token: 0x06000F13 RID: 3859 RVA: 0x0003DC90 File Offset: 0x0003BE90
		[CLSCompliant(false)]
		public static double ToDouble(ushort value)
		{
			return (double)value;
		}

		// Token: 0x06000F14 RID: 3860 RVA: 0x0003DC94 File Offset: 0x0003BE94
		public static double ToDouble(object value)
		{
			if (value == null)
			{
				return 0.0;
			}
			return Convert.ToDouble(value, null);
		}

		// Token: 0x06000F15 RID: 3861 RVA: 0x0003DCB0 File Offset: 0x0003BEB0
		public static double ToDouble(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0.0;
			}
			return ((IConvertible)value).ToDouble(provider);
		}

		// Token: 0x06000F16 RID: 3862 RVA: 0x0003DCD0 File Offset: 0x0003BED0
		public static short ToInt16(bool value)
		{
			return (!value) ? 0 : 1;
		}

		// Token: 0x06000F17 RID: 3863 RVA: 0x0003DCE0 File Offset: 0x0003BEE0
		public static short ToInt16(byte value)
		{
			return (short)value;
		}

		// Token: 0x06000F18 RID: 3864 RVA: 0x0003DCE4 File Offset: 0x0003BEE4
		public static short ToInt16(char value)
		{
			if (value > '翿')
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue"));
			}
			return (short)value;
		}

		// Token: 0x06000F19 RID: 3865 RVA: 0x0003DD04 File Offset: 0x0003BF04
		public static short ToInt16(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000F1A RID: 3866 RVA: 0x0003DD10 File Offset: 0x0003BF10
		public static short ToInt16(decimal value)
		{
			if (value > 32767m || value < -32768m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)Math.Round(value);
		}

		// Token: 0x06000F1B RID: 3867 RVA: 0x0003DD64 File Offset: 0x0003BF64
		public static short ToInt16(double value)
		{
			if (value > 32767.0 || value < -32768.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)Math.Round(value);
		}

		// Token: 0x06000F1C RID: 3868 RVA: 0x0003DD9C File Offset: 0x0003BF9C
		public static short ToInt16(float value)
		{
			if (value > 32767f || value < -32768f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)Math.Round((double)value);
		}

		// Token: 0x06000F1D RID: 3869 RVA: 0x0003DDD8 File Offset: 0x0003BFD8
		public static short ToInt16(int value)
		{
			if (value > 32767 || value < -32768)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)value;
		}

		// Token: 0x06000F1E RID: 3870 RVA: 0x0003DE10 File Offset: 0x0003C010
		public static short ToInt16(long value)
		{
			if (value > 32767L || value < -32768L)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue or less than Int16.MinValue"));
			}
			return (short)value;
		}

		// Token: 0x06000F1F RID: 3871 RVA: 0x0003DE48 File Offset: 0x0003C048
		[CLSCompliant(false)]
		public static short ToInt16(sbyte value)
		{
			return (short)value;
		}

		// Token: 0x06000F20 RID: 3872 RVA: 0x0003DE4C File Offset: 0x0003C04C
		public static short ToInt16(short value)
		{
			return value;
		}

		// Token: 0x06000F21 RID: 3873 RVA: 0x0003DE50 File Offset: 0x0003C050
		public static short ToInt16(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return short.Parse(value);
		}

		// Token: 0x06000F22 RID: 3874 RVA: 0x0003DE60 File Offset: 0x0003C060
		public static short ToInt16(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return short.Parse(value, provider);
		}

		// Token: 0x06000F23 RID: 3875 RVA: 0x0003DE74 File Offset: 0x0003C074
		public static short ToInt16(string value, int fromBase)
		{
			int num = Convert.ConvertFromBase(value, fromBase, false);
			if (fromBase != 10)
			{
				if (num > 65535)
				{
					throw new OverflowException("Value was either too large or too small for an Int16.");
				}
				if (num > 32767)
				{
					return Convert.ToInt16(-(65536 - num));
				}
			}
			return Convert.ToInt16(num);
		}

		// Token: 0x06000F24 RID: 3876 RVA: 0x0003DEC8 File Offset: 0x0003C0C8
		[CLSCompliant(false)]
		public static short ToInt16(uint value)
		{
			if ((ulong)value > 32767UL)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue"));
			}
			return (short)value;
		}

		// Token: 0x06000F25 RID: 3877 RVA: 0x0003DEEC File Offset: 0x0003C0EC
		[CLSCompliant(false)]
		public static short ToInt16(ulong value)
		{
			if (value > 32767UL)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue"));
			}
			return (short)value;
		}

		// Token: 0x06000F26 RID: 3878 RVA: 0x0003DF0C File Offset: 0x0003C10C
		[CLSCompliant(false)]
		public static short ToInt16(ushort value)
		{
			if (value > 32767)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int16.MaxValue"));
			}
			return (short)value;
		}

		// Token: 0x06000F27 RID: 3879 RVA: 0x0003DF2C File Offset: 0x0003C12C
		public static short ToInt16(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return Convert.ToInt16(value, null);
		}

		// Token: 0x06000F28 RID: 3880 RVA: 0x0003DF40 File Offset: 0x0003C140
		public static short ToInt16(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToInt16(provider);
		}

		// Token: 0x06000F29 RID: 3881 RVA: 0x0003DF58 File Offset: 0x0003C158
		public static int ToInt32(bool value)
		{
			return (!value) ? 0 : 1;
		}

		// Token: 0x06000F2A RID: 3882 RVA: 0x0003DF68 File Offset: 0x0003C168
		public static int ToInt32(byte value)
		{
			return (int)value;
		}

		// Token: 0x06000F2B RID: 3883 RVA: 0x0003DF6C File Offset: 0x0003C16C
		public static int ToInt32(char value)
		{
			return (int)value;
		}

		// Token: 0x06000F2C RID: 3884 RVA: 0x0003DF70 File Offset: 0x0003C170
		public static int ToInt32(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000F2D RID: 3885 RVA: 0x0003DF7C File Offset: 0x0003C17C
		public static int ToInt32(decimal value)
		{
			if (value > 2147483647m || value < -2147483648m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue or less than Int32.MinValue"));
			}
			return (int)Math.Round(value);
		}

		// Token: 0x06000F2E RID: 3886 RVA: 0x0003DFD0 File Offset: 0x0003C1D0
		public static int ToInt32(double value)
		{
			if (value > 2147483647.0 || value < -2147483648.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue or less than Int32.MinValue"));
			}
			return checked((int)Math.Round(value));
		}

		// Token: 0x06000F2F RID: 3887 RVA: 0x0003E008 File Offset: 0x0003C208
		public static int ToInt32(float value)
		{
			if (value > 2.1474836E+09f || value < -2.1474836E+09f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue or less than Int32.MinValue"));
			}
			return checked((int)Math.Round((double)value));
		}

		// Token: 0x06000F30 RID: 3888 RVA: 0x0003E044 File Offset: 0x0003C244
		public static int ToInt32(int value)
		{
			return value;
		}

		// Token: 0x06000F31 RID: 3889 RVA: 0x0003E048 File Offset: 0x0003C248
		public static int ToInt32(long value)
		{
			if (value > 2147483647L || value < -2147483648L)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue or less than Int32.MinValue"));
			}
			return (int)value;
		}

		// Token: 0x06000F32 RID: 3890 RVA: 0x0003E080 File Offset: 0x0003C280
		[CLSCompliant(false)]
		public static int ToInt32(sbyte value)
		{
			return (int)value;
		}

		// Token: 0x06000F33 RID: 3891 RVA: 0x0003E084 File Offset: 0x0003C284
		public static int ToInt32(short value)
		{
			return (int)value;
		}

		// Token: 0x06000F34 RID: 3892 RVA: 0x0003E088 File Offset: 0x0003C288
		public static int ToInt32(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return int.Parse(value);
		}

		// Token: 0x06000F35 RID: 3893 RVA: 0x0003E098 File Offset: 0x0003C298
		public static int ToInt32(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return int.Parse(value, provider);
		}

		// Token: 0x06000F36 RID: 3894 RVA: 0x0003E0AC File Offset: 0x0003C2AC
		public static int ToInt32(string value, int fromBase)
		{
			return Convert.ConvertFromBase(value, fromBase, false);
		}

		// Token: 0x06000F37 RID: 3895 RVA: 0x0003E0B8 File Offset: 0x0003C2B8
		[CLSCompliant(false)]
		public static int ToInt32(uint value)
		{
			if (value > 2147483647U)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue"));
			}
			return (int)value;
		}

		// Token: 0x06000F38 RID: 3896 RVA: 0x0003E0D8 File Offset: 0x0003C2D8
		[CLSCompliant(false)]
		public static int ToInt32(ulong value)
		{
			if (value > 2147483647UL)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int32.MaxValue"));
			}
			return (int)value;
		}

		// Token: 0x06000F39 RID: 3897 RVA: 0x0003E0F8 File Offset: 0x0003C2F8
		[CLSCompliant(false)]
		public static int ToInt32(ushort value)
		{
			return (int)value;
		}

		// Token: 0x06000F3A RID: 3898 RVA: 0x0003E0FC File Offset: 0x0003C2FC
		public static int ToInt32(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return Convert.ToInt32(value, null);
		}

		// Token: 0x06000F3B RID: 3899 RVA: 0x0003E110 File Offset: 0x0003C310
		public static int ToInt32(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToInt32(provider);
		}

		// Token: 0x06000F3C RID: 3900 RVA: 0x0003E128 File Offset: 0x0003C328
		public static long ToInt64(bool value)
		{
			return (!value) ? 0L : 1L;
		}

		// Token: 0x06000F3D RID: 3901 RVA: 0x0003E138 File Offset: 0x0003C338
		public static long ToInt64(byte value)
		{
			return (long)((ulong)value);
		}

		// Token: 0x06000F3E RID: 3902 RVA: 0x0003E13C File Offset: 0x0003C33C
		public static long ToInt64(char value)
		{
			return (long)value;
		}

		// Token: 0x06000F3F RID: 3903 RVA: 0x0003E140 File Offset: 0x0003C340
		public static long ToInt64(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000F40 RID: 3904 RVA: 0x0003E14C File Offset: 0x0003C34C
		public static long ToInt64(decimal value)
		{
			if (value > 9223372036854775807m || value < -9223372036854775808m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int64.MaxValue or less than Int64.MinValue"));
			}
			return (long)Math.Round(value);
		}

		// Token: 0x06000F41 RID: 3905 RVA: 0x0003E1A8 File Offset: 0x0003C3A8
		public static long ToInt64(double value)
		{
			if (value > 9.223372036854776E+18 || value < -9.223372036854776E+18)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int64.MaxValue or less than Int64.MinValue"));
			}
			return (long)Math.Round(value);
		}

		// Token: 0x06000F42 RID: 3906 RVA: 0x0003E1E0 File Offset: 0x0003C3E0
		public static long ToInt64(float value)
		{
			if (value > 9.223372E+18f || value < -9.223372E+18f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int64.MaxValue or less than Int64.MinValue"));
			}
			return (long)Math.Round((double)value);
		}

		// Token: 0x06000F43 RID: 3907 RVA: 0x0003E21C File Offset: 0x0003C41C
		public static long ToInt64(int value)
		{
			return (long)value;
		}

		// Token: 0x06000F44 RID: 3908 RVA: 0x0003E220 File Offset: 0x0003C420
		public static long ToInt64(long value)
		{
			return value;
		}

		// Token: 0x06000F45 RID: 3909 RVA: 0x0003E224 File Offset: 0x0003C424
		[CLSCompliant(false)]
		public static long ToInt64(sbyte value)
		{
			return (long)value;
		}

		// Token: 0x06000F46 RID: 3910 RVA: 0x0003E228 File Offset: 0x0003C428
		public static long ToInt64(short value)
		{
			return (long)value;
		}

		// Token: 0x06000F47 RID: 3911 RVA: 0x0003E22C File Offset: 0x0003C42C
		public static long ToInt64(string value)
		{
			if (value == null)
			{
				return 0L;
			}
			return long.Parse(value);
		}

		// Token: 0x06000F48 RID: 3912 RVA: 0x0003E240 File Offset: 0x0003C440
		public static long ToInt64(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0L;
			}
			return long.Parse(value, provider);
		}

		// Token: 0x06000F49 RID: 3913 RVA: 0x0003E254 File Offset: 0x0003C454
		public static long ToInt64(string value, int fromBase)
		{
			return Convert.ConvertFromBase64(value, fromBase, false);
		}

		// Token: 0x06000F4A RID: 3914 RVA: 0x0003E260 File Offset: 0x0003C460
		[CLSCompliant(false)]
		public static long ToInt64(uint value)
		{
			return (long)((ulong)value);
		}

		// Token: 0x06000F4B RID: 3915 RVA: 0x0003E264 File Offset: 0x0003C464
		[CLSCompliant(false)]
		public static long ToInt64(ulong value)
		{
			if (value > 9223372036854775807UL)
			{
				throw new OverflowException(Locale.GetText("Value is greater than Int64.MaxValue"));
			}
			return (long)value;
		}

		// Token: 0x06000F4C RID: 3916 RVA: 0x0003E294 File Offset: 0x0003C494
		[CLSCompliant(false)]
		public static long ToInt64(ushort value)
		{
			return (long)((ulong)value);
		}

		// Token: 0x06000F4D RID: 3917 RVA: 0x0003E298 File Offset: 0x0003C498
		public static long ToInt64(object value)
		{
			if (value == null)
			{
				return 0L;
			}
			return Convert.ToInt64(value, null);
		}

		// Token: 0x06000F4E RID: 3918 RVA: 0x0003E2AC File Offset: 0x0003C4AC
		public static long ToInt64(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0L;
			}
			return ((IConvertible)value).ToInt64(provider);
		}

		// Token: 0x06000F4F RID: 3919 RVA: 0x0003E2C4 File Offset: 0x0003C4C4
		[CLSCompliant(false)]
		public static sbyte ToSByte(bool value)
		{
			return (!value) ? 0 : 1;
		}

		// Token: 0x06000F50 RID: 3920 RVA: 0x0003E2D4 File Offset: 0x0003C4D4
		[CLSCompliant(false)]
		public static sbyte ToSByte(byte value)
		{
			if (value > 127)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		// Token: 0x06000F51 RID: 3921 RVA: 0x0003E2F0 File Offset: 0x0003C4F0
		[CLSCompliant(false)]
		public static sbyte ToSByte(char value)
		{
			if (value > '\u007f')
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		// Token: 0x06000F52 RID: 3922 RVA: 0x0003E30C File Offset: 0x0003C50C
		[CLSCompliant(false)]
		public static sbyte ToSByte(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000F53 RID: 3923 RVA: 0x0003E318 File Offset: 0x0003C518
		[CLSCompliant(false)]
		public static sbyte ToSByte(decimal value)
		{
			if (value > 127m || value < -128m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)Math.Round(value);
		}

		// Token: 0x06000F54 RID: 3924 RVA: 0x0003E364 File Offset: 0x0003C564
		[CLSCompliant(false)]
		public static sbyte ToSByte(double value)
		{
			if (value > 127.0 || value < -128.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)Math.Round(value);
		}

		// Token: 0x06000F55 RID: 3925 RVA: 0x0003E39C File Offset: 0x0003C59C
		[CLSCompliant(false)]
		public static sbyte ToSByte(float value)
		{
			if (value > 127f || value < -128f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.Minalue"));
			}
			return (sbyte)Math.Round((double)value);
		}

		// Token: 0x06000F56 RID: 3926 RVA: 0x0003E3D8 File Offset: 0x0003C5D8
		[CLSCompliant(false)]
		public static sbyte ToSByte(int value)
		{
			if (value > 127 || value < -128)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)value;
		}

		// Token: 0x06000F57 RID: 3927 RVA: 0x0003E408 File Offset: 0x0003C608
		[CLSCompliant(false)]
		public static sbyte ToSByte(long value)
		{
			if (value > 127L || value < -128L)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)value;
		}

		// Token: 0x06000F58 RID: 3928 RVA: 0x0003E43C File Offset: 0x0003C63C
		[CLSCompliant(false)]
		public static sbyte ToSByte(sbyte value)
		{
			return value;
		}

		// Token: 0x06000F59 RID: 3929 RVA: 0x0003E440 File Offset: 0x0003C640
		[CLSCompliant(false)]
		public static sbyte ToSByte(short value)
		{
			if (value > 127 || value < -128)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue or less than SByte.MinValue"));
			}
			return (sbyte)value;
		}

		// Token: 0x06000F5A RID: 3930 RVA: 0x0003E470 File Offset: 0x0003C670
		[CLSCompliant(false)]
		public static sbyte ToSByte(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return sbyte.Parse(value);
		}

		// Token: 0x06000F5B RID: 3931 RVA: 0x0003E480 File Offset: 0x0003C680
		[CLSCompliant(false)]
		public static sbyte ToSByte(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			return sbyte.Parse(value, provider);
		}

		// Token: 0x06000F5C RID: 3932 RVA: 0x0003E49C File Offset: 0x0003C69C
		[CLSCompliant(false)]
		public static sbyte ToSByte(string value, int fromBase)
		{
			int num = Convert.ConvertFromBase(value, fromBase, false);
			if (fromBase != 10 && num > 127)
			{
				return Convert.ToSByte(-(256 - num));
			}
			return Convert.ToSByte(num);
		}

		// Token: 0x06000F5D RID: 3933 RVA: 0x0003E4D8 File Offset: 0x0003C6D8
		[CLSCompliant(false)]
		public static sbyte ToSByte(uint value)
		{
			if ((ulong)value > 127UL)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		// Token: 0x06000F5E RID: 3934 RVA: 0x0003E4F8 File Offset: 0x0003C6F8
		[CLSCompliant(false)]
		public static sbyte ToSByte(ulong value)
		{
			if (value > 127UL)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		// Token: 0x06000F5F RID: 3935 RVA: 0x0003E518 File Offset: 0x0003C718
		[CLSCompliant(false)]
		public static sbyte ToSByte(ushort value)
		{
			if (value > 127)
			{
				throw new OverflowException(Locale.GetText("Value is greater than SByte.MaxValue"));
			}
			return (sbyte)value;
		}

		// Token: 0x06000F60 RID: 3936 RVA: 0x0003E534 File Offset: 0x0003C734
		[CLSCompliant(false)]
		public static sbyte ToSByte(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return Convert.ToSByte(value, null);
		}

		// Token: 0x06000F61 RID: 3937 RVA: 0x0003E548 File Offset: 0x0003C748
		[CLSCompliant(false)]
		public static sbyte ToSByte(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToSByte(provider);
		}

		// Token: 0x06000F62 RID: 3938 RVA: 0x0003E560 File Offset: 0x0003C760
		public static float ToSingle(bool value)
		{
			return (float)((!value) ? 0 : 1);
		}

		// Token: 0x06000F63 RID: 3939 RVA: 0x0003E570 File Offset: 0x0003C770
		public static float ToSingle(byte value)
		{
			return (float)value;
		}

		// Token: 0x06000F64 RID: 3940 RVA: 0x0003E574 File Offset: 0x0003C774
		public static float ToSingle(char value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000F65 RID: 3941 RVA: 0x0003E580 File Offset: 0x0003C780
		public static float ToSingle(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000F66 RID: 3942 RVA: 0x0003E58C File Offset: 0x0003C78C
		public static float ToSingle(decimal value)
		{
			return (float)value;
		}

		// Token: 0x06000F67 RID: 3943 RVA: 0x0003E594 File Offset: 0x0003C794
		public static float ToSingle(double value)
		{
			return (float)value;
		}

		// Token: 0x06000F68 RID: 3944 RVA: 0x0003E598 File Offset: 0x0003C798
		public static float ToSingle(float value)
		{
			return value;
		}

		// Token: 0x06000F69 RID: 3945 RVA: 0x0003E59C File Offset: 0x0003C79C
		public static float ToSingle(int value)
		{
			return (float)value;
		}

		// Token: 0x06000F6A RID: 3946 RVA: 0x0003E5A0 File Offset: 0x0003C7A0
		public static float ToSingle(long value)
		{
			return (float)value;
		}

		// Token: 0x06000F6B RID: 3947 RVA: 0x0003E5A4 File Offset: 0x0003C7A4
		[CLSCompliant(false)]
		public static float ToSingle(sbyte value)
		{
			return (float)value;
		}

		// Token: 0x06000F6C RID: 3948 RVA: 0x0003E5A8 File Offset: 0x0003C7A8
		public static float ToSingle(short value)
		{
			return (float)value;
		}

		// Token: 0x06000F6D RID: 3949 RVA: 0x0003E5AC File Offset: 0x0003C7AC
		public static float ToSingle(string value)
		{
			if (value == null)
			{
				return 0f;
			}
			return float.Parse(value);
		}

		// Token: 0x06000F6E RID: 3950 RVA: 0x0003E5C0 File Offset: 0x0003C7C0
		public static float ToSingle(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0f;
			}
			return float.Parse(value, provider);
		}

		// Token: 0x06000F6F RID: 3951 RVA: 0x0003E5D8 File Offset: 0x0003C7D8
		[CLSCompliant(false)]
		public static float ToSingle(uint value)
		{
			return value;
		}

		// Token: 0x06000F70 RID: 3952 RVA: 0x0003E5E0 File Offset: 0x0003C7E0
		[CLSCompliant(false)]
		public static float ToSingle(ulong value)
		{
			return value;
		}

		// Token: 0x06000F71 RID: 3953 RVA: 0x0003E5E8 File Offset: 0x0003C7E8
		[CLSCompliant(false)]
		public static float ToSingle(ushort value)
		{
			return (float)value;
		}

		// Token: 0x06000F72 RID: 3954 RVA: 0x0003E5EC File Offset: 0x0003C7EC
		public static float ToSingle(object value)
		{
			if (value == null)
			{
				return 0f;
			}
			return Convert.ToSingle(value, null);
		}

		// Token: 0x06000F73 RID: 3955 RVA: 0x0003E604 File Offset: 0x0003C804
		public static float ToSingle(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0f;
			}
			return ((IConvertible)value).ToSingle(provider);
		}

		// Token: 0x06000F74 RID: 3956 RVA: 0x0003E620 File Offset: 0x0003C820
		public static string ToString(bool value)
		{
			return value.ToString();
		}

		// Token: 0x06000F75 RID: 3957 RVA: 0x0003E62C File Offset: 0x0003C82C
		public static string ToString(bool value, IFormatProvider provider)
		{
			return value.ToString();
		}

		// Token: 0x06000F76 RID: 3958 RVA: 0x0003E638 File Offset: 0x0003C838
		public static string ToString(byte value)
		{
			return value.ToString();
		}

		// Token: 0x06000F77 RID: 3959 RVA: 0x0003E644 File Offset: 0x0003C844
		public static string ToString(byte value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F78 RID: 3960 RVA: 0x0003E650 File Offset: 0x0003C850
		public static string ToString(byte value, int toBase)
		{
			if (value == 0)
			{
				return "0";
			}
			if (toBase == 10)
			{
				return value.ToString();
			}
			byte[] bytes = BitConverter.GetBytes((short)value);
			if (toBase == 2)
			{
				return Convert.ConvertToBase2(bytes);
			}
			if (toBase == 8)
			{
				return Convert.ConvertToBase8(bytes);
			}
			if (toBase != 16)
			{
				throw new ArgumentException(Locale.GetText("toBase is not valid."));
			}
			return Convert.ConvertToBase16(bytes);
		}

		// Token: 0x06000F79 RID: 3961 RVA: 0x0003E6C4 File Offset: 0x0003C8C4
		public static string ToString(char value)
		{
			return value.ToString();
		}

		// Token: 0x06000F7A RID: 3962 RVA: 0x0003E6D0 File Offset: 0x0003C8D0
		public static string ToString(char value, IFormatProvider provider)
		{
			return value.ToString();
		}

		// Token: 0x06000F7B RID: 3963 RVA: 0x0003E6DC File Offset: 0x0003C8DC
		public static string ToString(DateTime value)
		{
			return value.ToString();
		}

		// Token: 0x06000F7C RID: 3964 RVA: 0x0003E6E8 File Offset: 0x0003C8E8
		public static string ToString(DateTime value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F7D RID: 3965 RVA: 0x0003E6F4 File Offset: 0x0003C8F4
		public static string ToString(decimal value)
		{
			return value.ToString();
		}

		// Token: 0x06000F7E RID: 3966 RVA: 0x0003E700 File Offset: 0x0003C900
		public static string ToString(decimal value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F7F RID: 3967 RVA: 0x0003E70C File Offset: 0x0003C90C
		public static string ToString(double value)
		{
			return value.ToString();
		}

		// Token: 0x06000F80 RID: 3968 RVA: 0x0003E718 File Offset: 0x0003C918
		public static string ToString(double value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F81 RID: 3969 RVA: 0x0003E724 File Offset: 0x0003C924
		public static string ToString(float value)
		{
			return value.ToString();
		}

		// Token: 0x06000F82 RID: 3970 RVA: 0x0003E730 File Offset: 0x0003C930
		public static string ToString(float value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F83 RID: 3971 RVA: 0x0003E73C File Offset: 0x0003C93C
		public static string ToString(int value)
		{
			return value.ToString();
		}

		// Token: 0x06000F84 RID: 3972 RVA: 0x0003E748 File Offset: 0x0003C948
		public static string ToString(int value, int toBase)
		{
			if (value == 0)
			{
				return "0";
			}
			if (toBase == 10)
			{
				return value.ToString();
			}
			byte[] bytes = BitConverter.GetBytes(value);
			if (toBase == 2)
			{
				return Convert.ConvertToBase2(bytes);
			}
			if (toBase == 8)
			{
				return Convert.ConvertToBase8(bytes);
			}
			if (toBase != 16)
			{
				throw new ArgumentException(Locale.GetText("toBase is not valid."));
			}
			return Convert.ConvertToBase16(bytes);
		}

		// Token: 0x06000F85 RID: 3973 RVA: 0x0003E7BC File Offset: 0x0003C9BC
		public static string ToString(int value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F86 RID: 3974 RVA: 0x0003E7C8 File Offset: 0x0003C9C8
		public static string ToString(long value)
		{
			return value.ToString();
		}

		// Token: 0x06000F87 RID: 3975 RVA: 0x0003E7D4 File Offset: 0x0003C9D4
		public static string ToString(long value, int toBase)
		{
			if (value == 0L)
			{
				return "0";
			}
			if (toBase == 10)
			{
				return value.ToString();
			}
			byte[] bytes = BitConverter.GetBytes(value);
			if (toBase == 2)
			{
				return Convert.ConvertToBase2(bytes);
			}
			if (toBase == 8)
			{
				return Convert.ConvertToBase8(bytes);
			}
			if (toBase != 16)
			{
				throw new ArgumentException(Locale.GetText("toBase is not valid."));
			}
			return Convert.ConvertToBase16(bytes);
		}

		// Token: 0x06000F88 RID: 3976 RVA: 0x0003E848 File Offset: 0x0003CA48
		public static string ToString(long value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F89 RID: 3977 RVA: 0x0003E854 File Offset: 0x0003CA54
		public static string ToString(object value)
		{
			return Convert.ToString(value, null);
		}

		// Token: 0x06000F8A RID: 3978 RVA: 0x0003E860 File Offset: 0x0003CA60
		public static string ToString(object value, IFormatProvider provider)
		{
			if (value is IConvertible)
			{
				return ((IConvertible)value).ToString(provider);
			}
			if (value != null)
			{
				return value.ToString();
			}
			return string.Empty;
		}

		// Token: 0x06000F8B RID: 3979 RVA: 0x0003E898 File Offset: 0x0003CA98
		[CLSCompliant(false)]
		public static string ToString(sbyte value)
		{
			return value.ToString();
		}

		// Token: 0x06000F8C RID: 3980 RVA: 0x0003E8A4 File Offset: 0x0003CAA4
		[CLSCompliant(false)]
		public static string ToString(sbyte value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F8D RID: 3981 RVA: 0x0003E8B0 File Offset: 0x0003CAB0
		public static string ToString(short value)
		{
			return value.ToString();
		}

		// Token: 0x06000F8E RID: 3982 RVA: 0x0003E8BC File Offset: 0x0003CABC
		public static string ToString(short value, int toBase)
		{
			if (value == 0)
			{
				return "0";
			}
			if (toBase == 10)
			{
				return value.ToString();
			}
			byte[] bytes = BitConverter.GetBytes(value);
			if (toBase == 2)
			{
				return Convert.ConvertToBase2(bytes);
			}
			if (toBase == 8)
			{
				return Convert.ConvertToBase8(bytes);
			}
			if (toBase != 16)
			{
				throw new ArgumentException(Locale.GetText("toBase is not valid."));
			}
			return Convert.ConvertToBase16(bytes);
		}

		// Token: 0x06000F8F RID: 3983 RVA: 0x0003E930 File Offset: 0x0003CB30
		public static string ToString(short value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F90 RID: 3984 RVA: 0x0003E93C File Offset: 0x0003CB3C
		public static string ToString(string value)
		{
			return value;
		}

		// Token: 0x06000F91 RID: 3985 RVA: 0x0003E940 File Offset: 0x0003CB40
		public static string ToString(string value, IFormatProvider provider)
		{
			return value;
		}

		// Token: 0x06000F92 RID: 3986 RVA: 0x0003E944 File Offset: 0x0003CB44
		[CLSCompliant(false)]
		public static string ToString(uint value)
		{
			return value.ToString();
		}

		// Token: 0x06000F93 RID: 3987 RVA: 0x0003E950 File Offset: 0x0003CB50
		[CLSCompliant(false)]
		public static string ToString(uint value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F94 RID: 3988 RVA: 0x0003E95C File Offset: 0x0003CB5C
		[CLSCompliant(false)]
		public static string ToString(ulong value)
		{
			return value.ToString();
		}

		// Token: 0x06000F95 RID: 3989 RVA: 0x0003E968 File Offset: 0x0003CB68
		[CLSCompliant(false)]
		public static string ToString(ulong value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F96 RID: 3990 RVA: 0x0003E974 File Offset: 0x0003CB74
		[CLSCompliant(false)]
		public static string ToString(ushort value)
		{
			return value.ToString();
		}

		// Token: 0x06000F97 RID: 3991 RVA: 0x0003E980 File Offset: 0x0003CB80
		[CLSCompliant(false)]
		public static string ToString(ushort value, IFormatProvider provider)
		{
			return value.ToString(provider);
		}

		// Token: 0x06000F98 RID: 3992 RVA: 0x0003E98C File Offset: 0x0003CB8C
		[CLSCompliant(false)]
		public static ushort ToUInt16(bool value)
		{
			return (!value) ? 0 : 1;
		}

		// Token: 0x06000F99 RID: 3993 RVA: 0x0003E99C File Offset: 0x0003CB9C
		[CLSCompliant(false)]
		public static ushort ToUInt16(byte value)
		{
			return (ushort)value;
		}

		// Token: 0x06000F9A RID: 3994 RVA: 0x0003E9A0 File Offset: 0x0003CBA0
		[CLSCompliant(false)]
		public static ushort ToUInt16(char value)
		{
			return (ushort)value;
		}

		// Token: 0x06000F9B RID: 3995 RVA: 0x0003E9A4 File Offset: 0x0003CBA4
		[CLSCompliant(false)]
		public static ushort ToUInt16(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000F9C RID: 3996 RVA: 0x0003E9B0 File Offset: 0x0003CBB0
		[CLSCompliant(false)]
		public static ushort ToUInt16(decimal value)
		{
			if (value > 65535m || value < 0m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)Math.Round(value);
		}

		// Token: 0x06000F9D RID: 3997 RVA: 0x0003EA00 File Offset: 0x0003CC00
		[CLSCompliant(false)]
		public static ushort ToUInt16(double value)
		{
			if (value > 65535.0 || value < 0.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)Math.Round(value);
		}

		// Token: 0x06000F9E RID: 3998 RVA: 0x0003EA38 File Offset: 0x0003CC38
		[CLSCompliant(false)]
		public static ushort ToUInt16(float value)
		{
			if (value > 65535f || value < 0f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)Math.Round((double)value);
		}

		// Token: 0x06000F9F RID: 3999 RVA: 0x0003EA74 File Offset: 0x0003CC74
		[CLSCompliant(false)]
		public static ushort ToUInt16(int value)
		{
			if (value > 65535 || value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)value;
		}

		// Token: 0x06000FA0 RID: 4000 RVA: 0x0003EAA8 File Offset: 0x0003CCA8
		[CLSCompliant(false)]
		public static ushort ToUInt16(long value)
		{
			if (value > 65535L || value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue or less than UInt16.MinValue"));
			}
			return (ushort)value;
		}

		// Token: 0x06000FA1 RID: 4001 RVA: 0x0003EADC File Offset: 0x0003CCDC
		[CLSCompliant(false)]
		public static ushort ToUInt16(sbyte value)
		{
			if ((int)value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt16.MinValue"));
			}
			return (ushort)value;
		}

		// Token: 0x06000FA2 RID: 4002 RVA: 0x0003EAF8 File Offset: 0x0003CCF8
		[CLSCompliant(false)]
		public static ushort ToUInt16(short value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt16.MinValue"));
			}
			return (ushort)value;
		}

		// Token: 0x06000FA3 RID: 4003 RVA: 0x0003EB14 File Offset: 0x0003CD14
		[CLSCompliant(false)]
		public static ushort ToUInt16(string value)
		{
			if (value == null)
			{
				return 0;
			}
			return ushort.Parse(value);
		}

		// Token: 0x06000FA4 RID: 4004 RVA: 0x0003EB24 File Offset: 0x0003CD24
		[CLSCompliant(false)]
		public static ushort ToUInt16(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ushort.Parse(value, provider);
		}

		// Token: 0x06000FA5 RID: 4005 RVA: 0x0003EB38 File Offset: 0x0003CD38
		[CLSCompliant(false)]
		public static ushort ToUInt16(string value, int fromBase)
		{
			return Convert.ToUInt16(Convert.ConvertFromBase(value, fromBase, true));
		}

		// Token: 0x06000FA6 RID: 4006 RVA: 0x0003EB48 File Offset: 0x0003CD48
		[CLSCompliant(false)]
		public static ushort ToUInt16(uint value)
		{
			if (value > 65535U)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue"));
			}
			return (ushort)value;
		}

		// Token: 0x06000FA7 RID: 4007 RVA: 0x0003EB68 File Offset: 0x0003CD68
		[CLSCompliant(false)]
		public static ushort ToUInt16(ulong value)
		{
			if (value > 65535UL)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt16.MaxValue"));
			}
			return (ushort)value;
		}

		// Token: 0x06000FA8 RID: 4008 RVA: 0x0003EB88 File Offset: 0x0003CD88
		[CLSCompliant(false)]
		public static ushort ToUInt16(ushort value)
		{
			return value;
		}

		// Token: 0x06000FA9 RID: 4009 RVA: 0x0003EB8C File Offset: 0x0003CD8C
		[CLSCompliant(false)]
		public static ushort ToUInt16(object value)
		{
			if (value == null)
			{
				return 0;
			}
			return Convert.ToUInt16(value, null);
		}

		// Token: 0x06000FAA RID: 4010 RVA: 0x0003EBA0 File Offset: 0x0003CDA0
		[CLSCompliant(false)]
		public static ushort ToUInt16(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0;
			}
			return ((IConvertible)value).ToUInt16(provider);
		}

		// Token: 0x06000FAB RID: 4011 RVA: 0x0003EBB8 File Offset: 0x0003CDB8
		[CLSCompliant(false)]
		public static uint ToUInt32(bool value)
		{
			return (!value) ? 0U : 1U;
		}

		// Token: 0x06000FAC RID: 4012 RVA: 0x0003EBC8 File Offset: 0x0003CDC8
		[CLSCompliant(false)]
		public static uint ToUInt32(byte value)
		{
			return (uint)value;
		}

		// Token: 0x06000FAD RID: 4013 RVA: 0x0003EBCC File Offset: 0x0003CDCC
		[CLSCompliant(false)]
		public static uint ToUInt32(char value)
		{
			return (uint)value;
		}

		// Token: 0x06000FAE RID: 4014 RVA: 0x0003EBD0 File Offset: 0x0003CDD0
		[CLSCompliant(false)]
		public static uint ToUInt32(DateTime value)
		{
			throw new InvalidCastException("This conversion is not supported.");
		}

		// Token: 0x06000FAF RID: 4015 RVA: 0x0003EBDC File Offset: 0x0003CDDC
		[CLSCompliant(false)]
		public static uint ToUInt32(decimal value)
		{
			if (value > 4294967295m || value < 0m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue or less than UInt32.MinValue"));
			}
			return (uint)Math.Round(value);
		}

		// Token: 0x06000FB0 RID: 4016 RVA: 0x0003EC2C File Offset: 0x0003CE2C
		[CLSCompliant(false)]
		public static uint ToUInt32(double value)
		{
			if (value > 4294967295.0 || value < 0.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue or less than UInt32.MinValue"));
			}
			return (uint)Math.Round(value);
		}

		// Token: 0x06000FB1 RID: 4017 RVA: 0x0003EC64 File Offset: 0x0003CE64
		[CLSCompliant(false)]
		public static uint ToUInt32(float value)
		{
			if (value > 4.2949673E+09f || value < 0f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue or less than UInt32.MinValue"));
			}
			return (uint)Math.Round((double)value);
		}

		// Token: 0x06000FB2 RID: 4018 RVA: 0x0003ECA0 File Offset: 0x0003CEA0
		[CLSCompliant(false)]
		public static uint ToUInt32(int value)
		{
			if ((long)value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt32.MinValue"));
			}
			return (uint)value;
		}

		// Token: 0x06000FB3 RID: 4019 RVA: 0x0003ECBC File Offset: 0x0003CEBC
		[CLSCompliant(false)]
		public static uint ToUInt32(long value)
		{
			if (value > (long)((ulong)-1) || value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue or less than UInt32.MinValue"));
			}
			return (uint)value;
		}

		// Token: 0x06000FB4 RID: 4020 RVA: 0x0003ECEC File Offset: 0x0003CEEC
		[CLSCompliant(false)]
		public static uint ToUInt32(sbyte value)
		{
			if ((long)value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt32.MinValue"));
			}
			return (uint)value;
		}

		// Token: 0x06000FB5 RID: 4021 RVA: 0x0003ED0C File Offset: 0x0003CF0C
		[CLSCompliant(false)]
		public static uint ToUInt32(short value)
		{
			if ((long)value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt32.MinValue"));
			}
			return (uint)value;
		}

		// Token: 0x06000FB6 RID: 4022 RVA: 0x0003ED2C File Offset: 0x0003CF2C
		[CLSCompliant(false)]
		public static uint ToUInt32(string value)
		{
			if (value == null)
			{
				return 0U;
			}
			return uint.Parse(value);
		}

		// Token: 0x06000FB7 RID: 4023 RVA: 0x0003ED3C File Offset: 0x0003CF3C
		[CLSCompliant(false)]
		public static uint ToUInt32(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0U;
			}
			return uint.Parse(value, provider);
		}

		// Token: 0x06000FB8 RID: 4024 RVA: 0x0003ED50 File Offset: 0x0003CF50
		[CLSCompliant(false)]
		public static uint ToUInt32(string value, int fromBase)
		{
			return (uint)Convert.ConvertFromBase(value, fromBase, true);
		}

		// Token: 0x06000FB9 RID: 4025 RVA: 0x0003ED5C File Offset: 0x0003CF5C
		[CLSCompliant(false)]
		public static uint ToUInt32(uint value)
		{
			return value;
		}

		// Token: 0x06000FBA RID: 4026 RVA: 0x0003ED60 File Offset: 0x0003CF60
		[CLSCompliant(false)]
		public static uint ToUInt32(ulong value)
		{
			if (value > (ulong)-1)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt32.MaxValue"));
			}
			return (uint)value;
		}

		// Token: 0x06000FBB RID: 4027 RVA: 0x0003ED7C File Offset: 0x0003CF7C
		[CLSCompliant(false)]
		public static uint ToUInt32(ushort value)
		{
			return (uint)value;
		}

		// Token: 0x06000FBC RID: 4028 RVA: 0x0003ED80 File Offset: 0x0003CF80
		[CLSCompliant(false)]
		public static uint ToUInt32(object value)
		{
			if (value == null)
			{
				return 0U;
			}
			return Convert.ToUInt32(value, null);
		}

		// Token: 0x06000FBD RID: 4029 RVA: 0x0003ED94 File Offset: 0x0003CF94
		[CLSCompliant(false)]
		public static uint ToUInt32(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0U;
			}
			return ((IConvertible)value).ToUInt32(provider);
		}

		// Token: 0x06000FBE RID: 4030 RVA: 0x0003EDAC File Offset: 0x0003CFAC
		[CLSCompliant(false)]
		public static ulong ToUInt64(bool value)
		{
			return (ulong)((!value) ? 0L : 1L);
		}

		// Token: 0x06000FBF RID: 4031 RVA: 0x0003EDBC File Offset: 0x0003CFBC
		[CLSCompliant(false)]
		public static ulong ToUInt64(byte value)
		{
			return (ulong)value;
		}

		// Token: 0x06000FC0 RID: 4032 RVA: 0x0003EDC0 File Offset: 0x0003CFC0
		[CLSCompliant(false)]
		public static ulong ToUInt64(char value)
		{
			return (ulong)value;
		}

		// Token: 0x06000FC1 RID: 4033 RVA: 0x0003EDC4 File Offset: 0x0003CFC4
		[CLSCompliant(false)]
		public static ulong ToUInt64(DateTime value)
		{
			throw new InvalidCastException("The conversion is not supported.");
		}

		// Token: 0x06000FC2 RID: 4034 RVA: 0x0003EDD0 File Offset: 0x0003CFD0
		[CLSCompliant(false)]
		public static ulong ToUInt64(decimal value)
		{
			if (value > 18446744073709551615m || value < 0m)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt64.MaxValue or less than UInt64.MinValue"));
			}
			return (ulong)Math.Round(value);
		}

		// Token: 0x06000FC3 RID: 4035 RVA: 0x0003EE20 File Offset: 0x0003D020
		[CLSCompliant(false)]
		public static ulong ToUInt64(double value)
		{
			if (value > 1.8446744073709552E+19 || value < 0.0)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt64.MaxValue or less than UInt64.MinValue"));
			}
			return (ulong)Math.Round(value);
		}

		// Token: 0x06000FC4 RID: 4036 RVA: 0x0003EE58 File Offset: 0x0003D058
		[CLSCompliant(false)]
		public static ulong ToUInt64(float value)
		{
			if (value > 1.8446744E+19f || value < 0f)
			{
				throw new OverflowException(Locale.GetText("Value is greater than UInt64.MaxValue or less than UInt64.MinValue"));
			}
			return (ulong)Math.Round((double)value);
		}

		// Token: 0x06000FC5 RID: 4037 RVA: 0x0003EE94 File Offset: 0x0003D094
		[CLSCompliant(false)]
		public static ulong ToUInt64(int value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt64.MinValue"));
			}
			return (ulong)((long)value);
		}

		// Token: 0x06000FC6 RID: 4038 RVA: 0x0003EEB0 File Offset: 0x0003D0B0
		[CLSCompliant(false)]
		public static ulong ToUInt64(long value)
		{
			if (value < 0L)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt64.MinValue"));
			}
			return (ulong)value;
		}

		// Token: 0x06000FC7 RID: 4039 RVA: 0x0003EECC File Offset: 0x0003D0CC
		[CLSCompliant(false)]
		public static ulong ToUInt64(sbyte value)
		{
			if ((int)value < 0)
			{
				throw new OverflowException("Value is less than UInt64.MinValue");
			}
			return (ulong)((long)value);
		}

		// Token: 0x06000FC8 RID: 4040 RVA: 0x0003EEE4 File Offset: 0x0003D0E4
		[CLSCompliant(false)]
		public static ulong ToUInt64(short value)
		{
			if (value < 0)
			{
				throw new OverflowException(Locale.GetText("Value is less than UInt64.MinValue"));
			}
			return (ulong)((long)value);
		}

		// Token: 0x06000FC9 RID: 4041 RVA: 0x0003EF00 File Offset: 0x0003D100
		[CLSCompliant(false)]
		public static ulong ToUInt64(string value)
		{
			if (value == null)
			{
				return 0UL;
			}
			return ulong.Parse(value);
		}

		// Token: 0x06000FCA RID: 4042 RVA: 0x0003EF14 File Offset: 0x0003D114
		[CLSCompliant(false)]
		public static ulong ToUInt64(string value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0UL;
			}
			return ulong.Parse(value, provider);
		}

		// Token: 0x06000FCB RID: 4043 RVA: 0x0003EF28 File Offset: 0x0003D128
		[CLSCompliant(false)]
		public static ulong ToUInt64(string value, int fromBase)
		{
			return (ulong)Convert.ConvertFromBase64(value, fromBase, true);
		}

		// Token: 0x06000FCC RID: 4044 RVA: 0x0003EF34 File Offset: 0x0003D134
		[CLSCompliant(false)]
		public static ulong ToUInt64(uint value)
		{
			return (ulong)value;
		}

		// Token: 0x06000FCD RID: 4045 RVA: 0x0003EF38 File Offset: 0x0003D138
		[CLSCompliant(false)]
		public static ulong ToUInt64(ulong value)
		{
			return value;
		}

		// Token: 0x06000FCE RID: 4046 RVA: 0x0003EF3C File Offset: 0x0003D13C
		[CLSCompliant(false)]
		public static ulong ToUInt64(ushort value)
		{
			return (ulong)value;
		}

		// Token: 0x06000FCF RID: 4047 RVA: 0x0003EF40 File Offset: 0x0003D140
		[CLSCompliant(false)]
		public static ulong ToUInt64(object value)
		{
			if (value == null)
			{
				return 0UL;
			}
			return Convert.ToUInt64(value, null);
		}

		// Token: 0x06000FD0 RID: 4048 RVA: 0x0003EF54 File Offset: 0x0003D154
		[CLSCompliant(false)]
		public static ulong ToUInt64(object value, IFormatProvider provider)
		{
			if (value == null)
			{
				return 0UL;
			}
			return ((IConvertible)value).ToUInt64(provider);
		}

		// Token: 0x06000FD1 RID: 4049 RVA: 0x0003EF6C File Offset: 0x0003D16C
		public static object ChangeType(object value, Type conversionType)
		{
			if (value != null && conversionType == null)
			{
				throw new ArgumentNullException("conversionType");
			}
			CultureInfo currentCulture = CultureInfo.CurrentCulture;
			IFormatProvider provider;
			if (conversionType == typeof(DateTime))
			{
				provider = currentCulture.DateTimeFormat;
			}
			else
			{
				provider = currentCulture.NumberFormat;
			}
			return Convert.ToType(value, conversionType, provider, true);
		}

		// Token: 0x06000FD2 RID: 4050 RVA: 0x0003EFC4 File Offset: 0x0003D1C4
		public static object ChangeType(object value, TypeCode typeCode)
		{
			CultureInfo currentCulture = CultureInfo.CurrentCulture;
			Type type = Convert.conversionTable[(int)typeCode];
			IFormatProvider provider;
			if (type == typeof(DateTime))
			{
				provider = currentCulture.DateTimeFormat;
			}
			else
			{
				provider = currentCulture.NumberFormat;
			}
			return Convert.ToType(value, type, provider, true);
		}

		// Token: 0x06000FD3 RID: 4051 RVA: 0x0003F00C File Offset: 0x0003D20C
		public static object ChangeType(object value, Type conversionType, IFormatProvider provider)
		{
			if (value != null && conversionType == null)
			{
				throw new ArgumentNullException("conversionType");
			}
			return Convert.ToType(value, conversionType, provider, true);
		}

		// Token: 0x06000FD4 RID: 4052 RVA: 0x0003F03C File Offset: 0x0003D23C
		public static object ChangeType(object value, TypeCode typeCode, IFormatProvider provider)
		{
			Type conversionType = Convert.conversionTable[(int)typeCode];
			return Convert.ToType(value, conversionType, provider, true);
		}

		// Token: 0x06000FD5 RID: 4053 RVA: 0x0003F05C File Offset: 0x0003D25C
		private static bool NotValidBase(int value)
		{
			return value != 2 && value != 8 && value != 10 && value != 16;
		}

		// Token: 0x06000FD6 RID: 4054 RVA: 0x0003F080 File Offset: 0x0003D280
		private static int ConvertFromBase(string value, int fromBase, bool unsigned)
		{
			if (Convert.NotValidBase(fromBase))
			{
				throw new ArgumentException("fromBase is not valid.");
			}
			if (value == null)
			{
				return 0;
			}
			int num = 0;
			int num2 = 0;
			int i = 0;
			int length = value.Length;
			bool flag = false;
			if (fromBase != 10)
			{
				if (fromBase != 16)
				{
					if (value.Substring(i, 1) == "-")
					{
						throw new ArgumentException("String cannot contain a minus sign if the base is not 10.");
					}
				}
				else
				{
					if (value.Substring(i, 1) == "-")
					{
						throw new ArgumentException("String cannot contain a minus sign if the base is not 10.");
					}
					if (length >= i + 2 && value[i] == '0' && (value[i + 1] == 'x' || value[i + 1] == 'X'))
					{
						i += 2;
					}
				}
			}
			else if (value.Substring(i, 1) == "-")
			{
				if (unsigned)
				{
					throw new OverflowException(Locale.GetText("The string was being parsed as an unsigned number and could not have a negative sign."));
				}
				flag = true;
				i++;
			}
			if (length == i)
			{
				throw new FormatException("Could not find any parsable digits.");
			}
			if (value[i] == '+')
			{
				i++;
			}
			while (i < length)
			{
				char c = value[i++];
				int num3;
				if (char.IsNumber(c))
				{
					num3 = (int)(c - '0');
				}
				else if (char.IsLetter(c))
				{
					num3 = (int)(char.ToLowerInvariant(c) - 'a' + '\n');
				}
				else
				{
					if (num > 0)
					{
						throw new FormatException("Additional unparsable characters are at the end of the string.");
					}
					throw new FormatException("Could not find any parsable digits.");
				}
				if (num3 >= fromBase)
				{
					if (num > 0)
					{
						throw new FormatException("Additional unparsable characters are at the end of the string.");
					}
					throw new FormatException("Could not find any parsable digits.");
				}
				else
				{
					num2 = fromBase * num2 + num3;
					num++;
				}
			}
			if (num == 0)
			{
				throw new FormatException("Could not find any parsable digits.");
			}
			if (flag)
			{
				return -num2;
			}
			return num2;
		}

		// Token: 0x06000FD7 RID: 4055 RVA: 0x0003F278 File Offset: 0x0003D478
		private static long ConvertFromBase64(string value, int fromBase, bool unsigned)
		{
			if (Convert.NotValidBase(fromBase))
			{
				throw new ArgumentException("fromBase is not valid.");
			}
			if (value == null)
			{
				return 0L;
			}
			int num = 0;
			long num2 = 0L;
			bool flag = false;
			int i = 0;
			int length = value.Length;
			if (fromBase != 10)
			{
				if (fromBase != 16)
				{
					if (value.Substring(i, 1) == "-")
					{
						throw new ArgumentException("String cannot contain a minus sign if the base is not 10.");
					}
				}
				else
				{
					if (value.Substring(i, 1) == "-")
					{
						throw new ArgumentException("String cannot contain a minus sign if the base is not 10.");
					}
					if (length >= i + 2 && value[i] == '0' && (value[i + 1] == 'x' || value[i + 1] == 'X'))
					{
						i += 2;
					}
				}
			}
			else if (value.Substring(i, 1) == "-")
			{
				if (unsigned)
				{
					throw new OverflowException(Locale.GetText("The string was being parsed as an unsigned number and could not have a negative sign."));
				}
				flag = true;
				i++;
			}
			if (length == i)
			{
				throw new FormatException("Could not find any parsable digits.");
			}
			if (value[i] == '+')
			{
				i++;
			}
			while (i < length)
			{
				char c = value[i++];
				int num3;
				if (char.IsNumber(c))
				{
					num3 = (int)(c - '0');
				}
				else if (char.IsLetter(c))
				{
					num3 = (int)(char.ToLowerInvariant(c) - 'a' + '\n');
				}
				else
				{
					if (num > 0)
					{
						throw new FormatException("Additional unparsable characters are at the end of the string.");
					}
					throw new FormatException("Could not find any parsable digits.");
				}
				if (num3 >= fromBase)
				{
					if (num > 0)
					{
						throw new FormatException("Additional unparsable characters are at the end of the string.");
					}
					throw new FormatException("Could not find any parsable digits.");
				}
				else
				{
					num2 = (long)fromBase * num2 + (long)num3;
					num++;
				}
			}
			if (num == 0)
			{
				throw new FormatException("Could not find any parsable digits.");
			}
			if (flag)
			{
				return -1L * num2;
			}
			return num2;
		}

		// Token: 0x06000FD8 RID: 4056 RVA: 0x0003F488 File Offset: 0x0003D688
		private static void EndianSwap(ref byte[] value)
		{
			byte[] array = new byte[value.Length];
			for (int i = 0; i < value.Length; i++)
			{
				array[i] = value[value.Length - 1 - i];
			}
			value = array;
		}

		// Token: 0x06000FD9 RID: 4057 RVA: 0x0003F4C8 File Offset: 0x0003D6C8
		private static string ConvertToBase2(byte[] value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				Convert.EndianSwap(ref value);
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = value.Length - 1; i >= 0; i--)
			{
				byte b = value[i];
				for (int j = 0; j < 8; j++)
				{
					if ((b & 128) == 128)
					{
						stringBuilder.Append('1');
					}
					else if (stringBuilder.Length > 0)
					{
						stringBuilder.Append('0');
					}
					b = (byte)(b << 1);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000FDA RID: 4058 RVA: 0x0003F558 File Offset: 0x0003D758
		private static string ConvertToBase8(byte[] value)
		{
			switch (value.Length)
			{
			case 1:
			{
				ulong num = (ulong)value[0];
				goto IL_74;
			}
			case 2:
			{
				ulong num = (ulong)BitConverter.ToUInt16(value, 0);
				goto IL_74;
			}
			case 4:
			{
				ulong num = (ulong)BitConverter.ToUInt32(value, 0);
				goto IL_74;
			}
			case 8:
			{
				ulong num = BitConverter.ToUInt64(value, 0);
				goto IL_74;
			}
			}
			throw new ArgumentException("value");
			IL_74:
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 21; i >= 0; i--)
			{
				ulong num;
				char c = (char)(num >> i * 3 & 7UL);
				if (c != '\0' || stringBuilder.Length > 0)
				{
					c += '0';
					stringBuilder.Append(c);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000FDB RID: 4059 RVA: 0x0003F628 File Offset: 0x0003D828
		private static string ConvertToBase16(byte[] value)
		{
			if (!BitConverter.IsLittleEndian)
			{
				Convert.EndianSwap(ref value);
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = value.Length - 1; i >= 0; i--)
			{
				char c = (char)(value[i] >> 4 & 15);
				if (c != '\0' || stringBuilder.Length > 0)
				{
					if (c < '\n')
					{
						c += '0';
					}
					else
					{
						c -= '\n';
						c += 'a';
					}
					stringBuilder.Append(c);
				}
				char c2 = (char)(value[i] & 15);
				if (c2 != '\0' || stringBuilder.Length > 0)
				{
					if (c2 < '\n')
					{
						c2 += '0';
					}
					else
					{
						c2 -= '\n';
						c2 += 'a';
					}
					stringBuilder.Append(c2);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000FDC RID: 4060 RVA: 0x0003F6EC File Offset: 0x0003D8EC
		internal static object ToType(object value, Type conversionType, IFormatProvider provider, bool try_target_to_type)
		{
			if (value == null)
			{
				if (conversionType != null && conversionType.IsValueType)
				{
					throw new InvalidCastException("Null object can not be converted to a value type.");
				}
				return null;
			}
			else
			{
				if (conversionType == null)
				{
					throw new InvalidCastException("Cannot cast to destination type.");
				}
				if (value.GetType() == conversionType)
				{
					return value;
				}
				if (value is IConvertible)
				{
					IConvertible convertible = (IConvertible)value;
					if (conversionType == Convert.conversionTable[0])
					{
						throw new ArgumentNullException();
					}
					if (conversionType == Convert.conversionTable[1])
					{
						return value;
					}
					if (conversionType == Convert.conversionTable[2])
					{
						throw new InvalidCastException("Cannot cast to DBNull, it's not IConvertible");
					}
					if (conversionType == Convert.conversionTable[3])
					{
						return convertible.ToBoolean(provider);
					}
					if (conversionType == Convert.conversionTable[4])
					{
						return convertible.ToChar(provider);
					}
					if (conversionType == Convert.conversionTable[5])
					{
						return convertible.ToSByte(provider);
					}
					if (conversionType == Convert.conversionTable[6])
					{
						return convertible.ToByte(provider);
					}
					if (conversionType == Convert.conversionTable[7])
					{
						return convertible.ToInt16(provider);
					}
					if (conversionType == Convert.conversionTable[8])
					{
						return convertible.ToUInt16(provider);
					}
					if (conversionType == Convert.conversionTable[9])
					{
						return convertible.ToInt32(provider);
					}
					if (conversionType == Convert.conversionTable[10])
					{
						return convertible.ToUInt32(provider);
					}
					if (conversionType == Convert.conversionTable[11])
					{
						return convertible.ToInt64(provider);
					}
					if (conversionType == Convert.conversionTable[12])
					{
						return convertible.ToUInt64(provider);
					}
					if (conversionType == Convert.conversionTable[13])
					{
						return convertible.ToSingle(provider);
					}
					if (conversionType == Convert.conversionTable[14])
					{
						return convertible.ToDouble(provider);
					}
					if (conversionType == Convert.conversionTable[15])
					{
						return convertible.ToDecimal(provider);
					}
					if (conversionType == Convert.conversionTable[16])
					{
						return convertible.ToDateTime(provider);
					}
					if (conversionType == Convert.conversionTable[18])
					{
						return convertible.ToString(provider);
					}
					if (try_target_to_type)
					{
						return convertible.ToType(conversionType, provider);
					}
				}
				throw new InvalidCastException(Locale.GetText("Value is not a convertible object: " + value.GetType().ToString() + " to " + conversionType.FullName));
			}
		}

		// Token: 0x04000490 RID: 1168
		private const int MaxBytesPerLine = 57;

		// Token: 0x04000491 RID: 1169
		public static readonly object DBNull = System.DBNull.Value;

		// Token: 0x04000492 RID: 1170
		private static readonly Type[] conversionTable = new Type[]
		{
			null,
			typeof(object),
			typeof(DBNull),
			typeof(bool),
			typeof(char),
			typeof(sbyte),
			typeof(byte),
			typeof(short),
			typeof(ushort),
			typeof(int),
			typeof(uint),
			typeof(long),
			typeof(ulong),
			typeof(float),
			typeof(double),
			typeof(decimal),
			typeof(DateTime),
			null,
			typeof(string)
		};
	}
}
