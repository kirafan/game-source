﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000135 RID: 309
	[ComVisible(true)]
	[Serializable]
	public class EventArgs
	{
		// Token: 0x040004FD RID: 1277
		public static readonly EventArgs Empty = new EventArgs();
	}
}
