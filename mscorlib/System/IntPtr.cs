﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000023 RID: 35
	[ComVisible(true)]
	[Serializable]
	public struct IntPtr : ISerializable
	{
		// Token: 0x0600035D RID: 861 RVA: 0x0000D8A8 File Offset: 0x0000BAA8
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public IntPtr(int value)
		{
			this.m_value = value;
		}

		// Token: 0x0600035E RID: 862 RVA: 0x0000D8B4 File Offset: 0x0000BAB4
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public IntPtr(long value)
		{
			this.m_value = value;
		}

		// Token: 0x0600035F RID: 863 RVA: 0x0000D8C0 File Offset: 0x0000BAC0
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public unsafe IntPtr(void* value)
		{
			this.m_value = value;
		}

		// Token: 0x06000360 RID: 864 RVA: 0x0000D8CC File Offset: 0x0000BACC
		private IntPtr(SerializationInfo info, StreamingContext context)
		{
			long @int = info.GetInt64("value");
			this.m_value = @int;
		}

		// Token: 0x06000361 RID: 865 RVA: 0x0000D8F0 File Offset: 0x0000BAF0
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("value", this.ToInt64());
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000362 RID: 866 RVA: 0x0000D920 File Offset: 0x0000BB20
		public unsafe static int Size
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return sizeof(void*);
			}
		}

		// Token: 0x06000363 RID: 867 RVA: 0x0000D928 File Offset: 0x0000BB28
		public override bool Equals(object obj)
		{
			return obj is IntPtr && ((IntPtr)obj).m_value == this.m_value;
		}

		// Token: 0x06000364 RID: 868 RVA: 0x0000D958 File Offset: 0x0000BB58
		public override int GetHashCode()
		{
			return this.m_value;
		}

		// Token: 0x06000365 RID: 869 RVA: 0x0000D964 File Offset: 0x0000BB64
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public int ToInt32()
		{
			return this.m_value;
		}

		// Token: 0x06000366 RID: 870 RVA: 0x0000D970 File Offset: 0x0000BB70
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public long ToInt64()
		{
			if (IntPtr.Size == 4)
			{
				return (long)this.m_value;
			}
			return this.m_value;
		}

		// Token: 0x06000367 RID: 871 RVA: 0x0000D990 File Offset: 0x0000BB90
		[CLSCompliant(false)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public unsafe void* ToPointer()
		{
			return this.m_value;
		}

		// Token: 0x06000368 RID: 872 RVA: 0x0000D998 File Offset: 0x0000BB98
		public override string ToString()
		{
			return this.ToString(null);
		}

		// Token: 0x06000369 RID: 873 RVA: 0x0000D9A4 File Offset: 0x0000BBA4
		public string ToString(string format)
		{
			if (IntPtr.Size == 4)
			{
				return this.m_value.ToString(format);
			}
			return this.m_value.ToString(format);
		}

		// Token: 0x0600036A RID: 874 RVA: 0x0000D9E0 File Offset: 0x0000BBE0
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static bool operator ==(IntPtr value1, IntPtr value2)
		{
			return value1.m_value == value2.m_value;
		}

		// Token: 0x0600036B RID: 875 RVA: 0x0000D9F4 File Offset: 0x0000BBF4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static bool operator !=(IntPtr value1, IntPtr value2)
		{
			return value1.m_value != value2.m_value;
		}

		// Token: 0x0600036C RID: 876 RVA: 0x0000DA0C File Offset: 0x0000BC0C
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static explicit operator IntPtr(int value)
		{
			return new IntPtr(value);
		}

		// Token: 0x0600036D RID: 877 RVA: 0x0000DA14 File Offset: 0x0000BC14
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static explicit operator IntPtr(long value)
		{
			return new IntPtr(value);
		}

		// Token: 0x0600036E RID: 878 RVA: 0x0000DA1C File Offset: 0x0000BC1C
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		[CLSCompliant(false)]
		public unsafe static explicit operator IntPtr(void* value)
		{
			return new IntPtr(value);
		}

		// Token: 0x0600036F RID: 879 RVA: 0x0000DA24 File Offset: 0x0000BC24
		public static explicit operator int(IntPtr value)
		{
			return value.m_value;
		}

		// Token: 0x06000370 RID: 880 RVA: 0x0000DA30 File Offset: 0x0000BC30
		public static explicit operator long(IntPtr value)
		{
			return value.ToInt64();
		}

		// Token: 0x06000371 RID: 881 RVA: 0x0000DA3C File Offset: 0x0000BC3C
		[CLSCompliant(false)]
		public unsafe static explicit operator void*(IntPtr value)
		{
			return value.m_value;
		}

		// Token: 0x04000056 RID: 86
		private unsafe void* m_value;

		// Token: 0x04000057 RID: 87
		public static readonly IntPtr Zero;
	}
}
