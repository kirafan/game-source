﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000190 RID: 400
	[ComVisible(true)]
	[Serializable]
	public class TypeUnloadedException : SystemException
	{
		// Token: 0x06001468 RID: 5224 RVA: 0x00052210 File Offset: 0x00050410
		public TypeUnloadedException() : base(Locale.GetText("Cannot access an unloaded class."))
		{
		}

		// Token: 0x06001469 RID: 5225 RVA: 0x00052224 File Offset: 0x00050424
		public TypeUnloadedException(string message) : base(message)
		{
		}

		// Token: 0x0600146A RID: 5226 RVA: 0x00052230 File Offset: 0x00050430
		protected TypeUnloadedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0600146B RID: 5227 RVA: 0x0005223C File Offset: 0x0005043C
		public TypeUnloadedException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
