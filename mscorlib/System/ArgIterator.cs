﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200005E RID: 94
	[StructLayout(LayoutKind.Auto)]
	public struct ArgIterator
	{
		// Token: 0x0600069E RID: 1694 RVA: 0x00014E64 File Offset: 0x00013064
		public ArgIterator(RuntimeArgumentHandle arglist)
		{
			this.sig = IntPtr.Zero;
			this.args = IntPtr.Zero;
			this.next_arg = (this.num_args = 0);
			this.Setup(arglist.args, IntPtr.Zero);
		}

		// Token: 0x0600069F RID: 1695 RVA: 0x00014EAC File Offset: 0x000130AC
		[CLSCompliant(false)]
		public unsafe ArgIterator(RuntimeArgumentHandle arglist, void* ptr)
		{
			this.sig = IntPtr.Zero;
			this.args = IntPtr.Zero;
			this.next_arg = (this.num_args = 0);
			this.Setup(arglist.args, (IntPtr)ptr);
		}

		// Token: 0x060006A0 RID: 1696
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Setup(IntPtr argsp, IntPtr start);

		// Token: 0x060006A1 RID: 1697 RVA: 0x00014EF4 File Offset: 0x000130F4
		public void End()
		{
			this.next_arg = this.num_args;
		}

		// Token: 0x060006A2 RID: 1698 RVA: 0x00014F04 File Offset: 0x00013104
		public override bool Equals(object o)
		{
			throw new NotSupportedException(Locale.GetText("ArgIterator does not support Equals."));
		}

		// Token: 0x060006A3 RID: 1699 RVA: 0x00014F18 File Offset: 0x00013118
		public override int GetHashCode()
		{
			return this.sig.GetHashCode();
		}

		// Token: 0x060006A4 RID: 1700 RVA: 0x00014F28 File Offset: 0x00013128
		[CLSCompliant(false)]
		public TypedReference GetNextArg()
		{
			if (this.num_args == this.next_arg)
			{
				throw new InvalidOperationException(Locale.GetText("Invalid iterator position."));
			}
			return this.IntGetNextArg();
		}

		// Token: 0x060006A5 RID: 1701
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern TypedReference IntGetNextArg();

		// Token: 0x060006A6 RID: 1702 RVA: 0x00014F54 File Offset: 0x00013154
		[CLSCompliant(false)]
		public TypedReference GetNextArg(RuntimeTypeHandle rth)
		{
			if (this.num_args == this.next_arg)
			{
				throw new InvalidOperationException(Locale.GetText("Invalid iterator position."));
			}
			return this.IntGetNextArg(rth.Value);
		}

		// Token: 0x060006A7 RID: 1703
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern TypedReference IntGetNextArg(IntPtr rth);

		// Token: 0x060006A8 RID: 1704 RVA: 0x00014F90 File Offset: 0x00013190
		public RuntimeTypeHandle GetNextArgType()
		{
			if (this.num_args == this.next_arg)
			{
				throw new InvalidOperationException(Locale.GetText("Invalid iterator position."));
			}
			return new RuntimeTypeHandle(this.IntGetNextArgType());
		}

		// Token: 0x060006A9 RID: 1705
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern IntPtr IntGetNextArgType();

		// Token: 0x060006AA RID: 1706 RVA: 0x00014FCC File Offset: 0x000131CC
		public int GetRemainingCount()
		{
			return this.num_args - this.next_arg;
		}

		// Token: 0x040000B7 RID: 183
		private IntPtr sig;

		// Token: 0x040000B8 RID: 184
		private IntPtr args;

		// Token: 0x040000B9 RID: 185
		private int next_arg;

		// Token: 0x040000BA RID: 186
		private int num_args;
	}
}
