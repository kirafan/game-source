﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000004 RID: 4
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_Attribute))]
	[AttributeUsage(AttributeTargets.All)]
	[ClassInterface(ClassInterfaceType.None)]
	[Serializable]
	public abstract class Attribute : _Attribute
	{
		// Token: 0x06000016 RID: 22 RVA: 0x00002220 File Offset: 0x00000420
		void _Attribute.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000017 RID: 23 RVA: 0x00002228 File Offset: 0x00000428
		void _Attribute.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00002230 File Offset: 0x00000430
		void _Attribute.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000019 RID: 25 RVA: 0x00002238 File Offset: 0x00000438
		void _Attribute.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x0600001A RID: 26 RVA: 0x00002240 File Offset: 0x00000440
		public virtual object TypeId
		{
			get
			{
				return base.GetType();
			}
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002248 File Offset: 0x00000448
		private static void CheckParameters(object element, Type attributeType)
		{
			if (element == null)
			{
				throw new ArgumentNullException("element");
			}
			if (attributeType == null)
			{
				throw new ArgumentNullException("attributeType");
			}
			if (!typeof(Attribute).IsAssignableFrom(attributeType))
			{
				throw new ArgumentException(Locale.GetText("Type is not derived from System.Attribute."), "attributeType");
			}
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000022A4 File Offset: 0x000004A4
		private static Attribute FindAttribute(object[] attributes)
		{
			if (attributes.Length > 1)
			{
				throw new AmbiguousMatchException(Locale.GetText("<element> has more than one attribute of type <attribute_type>"));
			}
			if (attributes.Length < 1)
			{
				return null;
			}
			return (Attribute)attributes[0];
		}

		// Token: 0x0600001D RID: 29 RVA: 0x000022E0 File Offset: 0x000004E0
		public static Attribute GetCustomAttribute(ParameterInfo element, Type attributeType)
		{
			return Attribute.GetCustomAttribute(element, attributeType, true);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000022EC File Offset: 0x000004EC
		public static Attribute GetCustomAttribute(MemberInfo element, Type attributeType)
		{
			return Attribute.GetCustomAttribute(element, attributeType, true);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x000022F8 File Offset: 0x000004F8
		public static Attribute GetCustomAttribute(Assembly element, Type attributeType)
		{
			return Attribute.GetCustomAttribute(element, attributeType, true);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002304 File Offset: 0x00000504
		public static Attribute GetCustomAttribute(Module element, Type attributeType)
		{
			return Attribute.GetCustomAttribute(element, attributeType, true);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002310 File Offset: 0x00000510
		public static Attribute GetCustomAttribute(Module element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			object[] customAttributes = element.GetCustomAttributes(attributeType, inherit);
			return Attribute.FindAttribute(customAttributes);
		}

		// Token: 0x06000022 RID: 34 RVA: 0x00002334 File Offset: 0x00000534
		public static Attribute GetCustomAttribute(Assembly element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			object[] customAttributes = element.GetCustomAttributes(attributeType, inherit);
			return Attribute.FindAttribute(customAttributes);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002358 File Offset: 0x00000558
		public static Attribute GetCustomAttribute(ParameterInfo element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			object[] customAttributes = element.GetCustomAttributes(attributeType, inherit);
			return Attribute.FindAttribute(customAttributes);
		}

		// Token: 0x06000024 RID: 36 RVA: 0x0000237C File Offset: 0x0000057C
		public static Attribute GetCustomAttribute(MemberInfo element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			return MonoCustomAttrs.GetCustomAttribute(element, attributeType, inherit);
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002390 File Offset: 0x00000590
		public static Attribute[] GetCustomAttributes(Assembly element)
		{
			return Attribute.GetCustomAttributes(element, true);
		}

		// Token: 0x06000026 RID: 38 RVA: 0x0000239C File Offset: 0x0000059C
		public static Attribute[] GetCustomAttributes(ParameterInfo element)
		{
			return Attribute.GetCustomAttributes(element, true);
		}

		// Token: 0x06000027 RID: 39 RVA: 0x000023A8 File Offset: 0x000005A8
		public static Attribute[] GetCustomAttributes(MemberInfo element)
		{
			return Attribute.GetCustomAttributes(element, true);
		}

		// Token: 0x06000028 RID: 40 RVA: 0x000023B4 File Offset: 0x000005B4
		public static Attribute[] GetCustomAttributes(Module element)
		{
			return Attribute.GetCustomAttributes(element, true);
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000023C0 File Offset: 0x000005C0
		public static Attribute[] GetCustomAttributes(Assembly element, Type attributeType)
		{
			return Attribute.GetCustomAttributes(element, attributeType, true);
		}

		// Token: 0x0600002A RID: 42 RVA: 0x000023CC File Offset: 0x000005CC
		public static Attribute[] GetCustomAttributes(Module element, Type attributeType)
		{
			return Attribute.GetCustomAttributes(element, attributeType, true);
		}

		// Token: 0x0600002B RID: 43 RVA: 0x000023D8 File Offset: 0x000005D8
		public static Attribute[] GetCustomAttributes(ParameterInfo element, Type attributeType)
		{
			return Attribute.GetCustomAttributes(element, attributeType, true);
		}

		// Token: 0x0600002C RID: 44 RVA: 0x000023E4 File Offset: 0x000005E4
		public static Attribute[] GetCustomAttributes(MemberInfo element, Type type)
		{
			return Attribute.GetCustomAttributes(element, type, true);
		}

		// Token: 0x0600002D RID: 45 RVA: 0x000023F0 File Offset: 0x000005F0
		public static Attribute[] GetCustomAttributes(Assembly element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			return (Attribute[])element.GetCustomAttributes(attributeType, inherit);
		}

		// Token: 0x0600002E RID: 46 RVA: 0x00002408 File Offset: 0x00000608
		public static Attribute[] GetCustomAttributes(ParameterInfo element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			return (Attribute[])element.GetCustomAttributes(attributeType, inherit);
		}

		// Token: 0x0600002F RID: 47 RVA: 0x00002420 File Offset: 0x00000620
		public static Attribute[] GetCustomAttributes(Module element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			return (Attribute[])element.GetCustomAttributes(attributeType, inherit);
		}

		// Token: 0x06000030 RID: 48 RVA: 0x00002438 File Offset: 0x00000638
		public static Attribute[] GetCustomAttributes(MemberInfo element, Type type, bool inherit)
		{
			Attribute.CheckParameters(element, type);
			MemberTypes memberType = element.MemberType;
			if (memberType == MemberTypes.Property)
			{
				return (Attribute[])MonoCustomAttrs.GetCustomAttributes(element, type, inherit);
			}
			return (Attribute[])element.GetCustomAttributes(type, inherit);
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00002478 File Offset: 0x00000678
		public static Attribute[] GetCustomAttributes(Module element, bool inherit)
		{
			Attribute.CheckParameters(element, typeof(Attribute));
			return (Attribute[])element.GetCustomAttributes(inherit);
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00002498 File Offset: 0x00000698
		public static Attribute[] GetCustomAttributes(Assembly element, bool inherit)
		{
			Attribute.CheckParameters(element, typeof(Attribute));
			return (Attribute[])element.GetCustomAttributes(inherit);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x000024B8 File Offset: 0x000006B8
		public static Attribute[] GetCustomAttributes(MemberInfo element, bool inherit)
		{
			Attribute.CheckParameters(element, typeof(Attribute));
			MemberTypes memberType = element.MemberType;
			if (memberType == MemberTypes.Property)
			{
				return (Attribute[])MonoCustomAttrs.GetCustomAttributes(element, inherit);
			}
			return (Attribute[])element.GetCustomAttributes(typeof(Attribute), inherit);
		}

		// Token: 0x06000034 RID: 52 RVA: 0x00002508 File Offset: 0x00000708
		public static Attribute[] GetCustomAttributes(ParameterInfo element, bool inherit)
		{
			Attribute.CheckParameters(element, typeof(Attribute));
			return (Attribute[])element.GetCustomAttributes(inherit);
		}

		// Token: 0x06000035 RID: 53 RVA: 0x00002528 File Offset: 0x00000728
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002530 File Offset: 0x00000730
		public virtual bool IsDefaultAttribute()
		{
			return false;
		}

		// Token: 0x06000037 RID: 55 RVA: 0x00002534 File Offset: 0x00000734
		public static bool IsDefined(Module element, Type attributeType)
		{
			return Attribute.IsDefined(element, attributeType, false);
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00002540 File Offset: 0x00000740
		public static bool IsDefined(ParameterInfo element, Type attributeType)
		{
			return Attribute.IsDefined(element, attributeType, true);
		}

		// Token: 0x06000039 RID: 57 RVA: 0x0000254C File Offset: 0x0000074C
		public static bool IsDefined(MemberInfo element, Type attributeType)
		{
			return Attribute.IsDefined(element, attributeType, true);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00002558 File Offset: 0x00000758
		public static bool IsDefined(Assembly element, Type attributeType)
		{
			return Attribute.IsDefined(element, attributeType, true);
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00002564 File Offset: 0x00000764
		public static bool IsDefined(MemberInfo element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			MemberTypes memberType = element.MemberType;
			if (memberType != MemberTypes.Constructor && memberType != MemberTypes.Event && memberType != MemberTypes.Field && memberType != MemberTypes.Method && memberType != MemberTypes.Property && memberType != MemberTypes.TypeInfo && memberType != MemberTypes.NestedType)
			{
				throw new NotSupportedException(Locale.GetText("Element is not a constructor, method, property, event, type or field."));
			}
			if (memberType == MemberTypes.Property)
			{
				return MonoCustomAttrs.IsDefined(element, attributeType, inherit);
			}
			return element.IsDefined(attributeType, inherit);
		}

		// Token: 0x0600003C RID: 60 RVA: 0x000025E0 File Offset: 0x000007E0
		public static bool IsDefined(Assembly element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			return element.IsDefined(attributeType, inherit);
		}

		// Token: 0x0600003D RID: 61 RVA: 0x000025F4 File Offset: 0x000007F4
		public static bool IsDefined(Module element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			return element.IsDefined(attributeType, inherit);
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00002608 File Offset: 0x00000808
		public static bool IsDefined(ParameterInfo element, Type attributeType, bool inherit)
		{
			Attribute.CheckParameters(element, attributeType);
			return element.IsDefined(attributeType, inherit) || Attribute.IsDefined(element.Member, attributeType, inherit);
		}

		// Token: 0x0600003F RID: 63 RVA: 0x00002638 File Offset: 0x00000838
		public virtual bool Match(object obj)
		{
			return this.Equals(obj);
		}

		// Token: 0x06000040 RID: 64 RVA: 0x00002644 File Offset: 0x00000844
		public override bool Equals(object obj)
		{
			return obj != null && obj is Attribute && ValueType.DefaultEquals(this, obj);
		}
	}
}
