﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace System
{
	// Token: 0x0200005C RID: 92
	[ComVisible(true)]
	public interface IAsyncResult
	{
		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000693 RID: 1683
		object AsyncState { get; }

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000694 RID: 1684
		WaitHandle AsyncWaitHandle { get; }

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x06000695 RID: 1685
		bool CompletedSynchronously { get; }

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x06000696 RID: 1686
		bool IsCompleted { get; }
	}
}
