﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000015 RID: 21
	[ComVisible(true)]
	[Serializable]
	public struct Int16 : IFormattable, IConvertible, IComparable, IComparable<short>, IEquatable<short>
	{
		// Token: 0x06000130 RID: 304 RVA: 0x00005898 File Offset: 0x00003A98
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this);
		}

		// Token: 0x06000131 RID: 305 RVA: 0x000058A4 File Offset: 0x00003AA4
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this);
		}

		// Token: 0x06000132 RID: 306 RVA: 0x000058B0 File Offset: 0x00003AB0
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this);
		}

		// Token: 0x06000133 RID: 307 RVA: 0x000058BC File Offset: 0x00003ABC
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this);
		}

		// Token: 0x06000134 RID: 308 RVA: 0x000058C8 File Offset: 0x00003AC8
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this);
		}

		// Token: 0x06000135 RID: 309 RVA: 0x000058D4 File Offset: 0x00003AD4
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this);
		}

		// Token: 0x06000136 RID: 310 RVA: 0x000058E0 File Offset: 0x00003AE0
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		// Token: 0x06000137 RID: 311 RVA: 0x000058EC File Offset: 0x00003AEC
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this);
		}

		// Token: 0x06000138 RID: 312 RVA: 0x000058F8 File Offset: 0x00003AF8
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		// Token: 0x06000139 RID: 313 RVA: 0x00005904 File Offset: 0x00003B04
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this);
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00005910 File Offset: 0x00003B10
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this);
		}

		// Token: 0x0600013B RID: 315 RVA: 0x0000591C File Offset: 0x00003B1C
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			return Convert.ToType(this, targetType, provider, false);
		}

		// Token: 0x0600013C RID: 316 RVA: 0x0000594C File Offset: 0x00003B4C
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this);
		}

		// Token: 0x0600013D RID: 317 RVA: 0x00005958 File Offset: 0x00003B58
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		// Token: 0x0600013E RID: 318 RVA: 0x00005964 File Offset: 0x00003B64
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00005970 File Offset: 0x00003B70
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is short))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.Int16"));
			}
			short num = (short)value;
			if (this == num)
			{
				return 0;
			}
			if (this > num)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06000140 RID: 320 RVA: 0x000059BC File Offset: 0x00003BBC
		public override bool Equals(object obj)
		{
			return obj is short && (short)obj == this;
		}

		// Token: 0x06000141 RID: 321 RVA: 0x000059D8 File Offset: 0x00003BD8
		public override int GetHashCode()
		{
			return (int)this;
		}

		// Token: 0x06000142 RID: 322 RVA: 0x000059DC File Offset: 0x00003BDC
		public int CompareTo(short value)
		{
			if (this == value)
			{
				return 0;
			}
			if (this > value)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06000143 RID: 323 RVA: 0x000059F4 File Offset: 0x00003BF4
		public bool Equals(short obj)
		{
			return obj == this;
		}

		// Token: 0x06000144 RID: 324 RVA: 0x000059FC File Offset: 0x00003BFC
		internal static bool Parse(string s, bool tryParse, out short result, out Exception exc)
		{
			short num = 0;
			int num2 = 1;
			bool flag = false;
			result = 0;
			exc = null;
			if (s == null)
			{
				if (!tryParse)
				{
					exc = new ArgumentNullException("s");
				}
				return false;
			}
			int length = s.Length;
			int i;
			char c;
			for (i = 0; i < length; i++)
			{
				c = s[i];
				if (!char.IsWhiteSpace(c))
				{
					break;
				}
			}
			if (i == length)
			{
				if (!tryParse)
				{
					exc = int.GetFormatException();
				}
				return false;
			}
			c = s[i];
			if (c == '+')
			{
				i++;
			}
			else if (c == '-')
			{
				num2 = -1;
				i++;
			}
			while (i < length)
			{
				c = s[i];
				if (c >= '0' && c <= '9')
				{
					byte b = (byte)(c - '0');
					if (num <= 3276)
					{
						if (num != 3276)
						{
							num = num * 10 + (short)b;
							flag = true;
							goto IL_154;
						}
						if (b <= 7 || (num2 != 1 && b <= 8))
						{
							if (num2 == -1)
							{
								num = (short)((int)num * num2 * 10 - (int)b);
							}
							else
							{
								num = num * 10 + (short)b;
							}
							if (int.ProcessTrailingWhitespace(tryParse, s, i + 1, ref exc))
							{
								result = num;
								return true;
							}
						}
					}
					if (!tryParse)
					{
						exc = new OverflowException("Value is too large");
					}
					return false;
				}
				if (!int.ProcessTrailingWhitespace(tryParse, s, i, ref exc))
				{
					return false;
				}
				IL_154:
				i++;
			}
			if (!flag)
			{
				if (!tryParse)
				{
					exc = int.GetFormatException();
				}
				return false;
			}
			if (num2 == -1)
			{
				result = (short)((int)num * num2);
			}
			else
			{
				result = num;
			}
			return true;
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00005BA8 File Offset: 0x00003DA8
		public static short Parse(string s, IFormatProvider provider)
		{
			return short.Parse(s, NumberStyles.Integer, provider);
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00005BB4 File Offset: 0x00003DB4
		public static short Parse(string s, NumberStyles style)
		{
			return short.Parse(s, style, null);
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00005BC0 File Offset: 0x00003DC0
		public static short Parse(string s, NumberStyles style, IFormatProvider provider)
		{
			int num = int.Parse(s, style, provider);
			if (num > 32767 || num < -32768)
			{
				throw new OverflowException("Value too large or too small.");
			}
			return (short)num;
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00005BFC File Offset: 0x00003DFC
		public static short Parse(string s)
		{
			short result;
			Exception ex;
			if (!short.Parse(s, false, out result, out ex))
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00005C1C File Offset: 0x00003E1C
		public static bool TryParse(string s, out short result)
		{
			Exception ex;
			if (!short.Parse(s, true, out result, out ex))
			{
				result = 0;
				return false;
			}
			return true;
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00005C40 File Offset: 0x00003E40
		public static bool TryParse(string s, NumberStyles style, IFormatProvider provider, out short result)
		{
			result = 0;
			int num;
			if (!int.TryParse(s, style, provider, out num))
			{
				return false;
			}
			if (num > 32767 || num < -32768)
			{
				return false;
			}
			result = (short)num;
			return true;
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00005C80 File Offset: 0x00003E80
		public override string ToString()
		{
			return NumberFormatter.NumberToString((int)this, null);
		}

		// Token: 0x0600014C RID: 332 RVA: 0x00005C8C File Offset: 0x00003E8C
		public string ToString(IFormatProvider provider)
		{
			return NumberFormatter.NumberToString((int)this, provider);
		}

		// Token: 0x0600014D RID: 333 RVA: 0x00005C98 File Offset: 0x00003E98
		public string ToString(string format)
		{
			return this.ToString(format, null);
		}

		// Token: 0x0600014E RID: 334 RVA: 0x00005CA4 File Offset: 0x00003EA4
		public string ToString(string format, IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(format, this, provider);
		}

		// Token: 0x0600014F RID: 335 RVA: 0x00005CB0 File Offset: 0x00003EB0
		public TypeCode GetTypeCode()
		{
			return TypeCode.Int16;
		}

		// Token: 0x04000018 RID: 24
		public const short MaxValue = 32767;

		// Token: 0x04000019 RID: 25
		public const short MinValue = -32768;

		// Token: 0x0400001A RID: 26
		internal short m_value;
	}
}
