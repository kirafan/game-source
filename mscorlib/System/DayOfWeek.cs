﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000123 RID: 291
	[ComVisible(true)]
	[Serializable]
	public enum DayOfWeek
	{
		// Token: 0x040004BB RID: 1211
		Sunday,
		// Token: 0x040004BC RID: 1212
		Monday,
		// Token: 0x040004BD RID: 1213
		Tuesday,
		// Token: 0x040004BE RID: 1214
		Wednesday,
		// Token: 0x040004BF RID: 1215
		Thursday,
		// Token: 0x040004C0 RID: 1216
		Friday,
		// Token: 0x040004C1 RID: 1217
		Saturday
	}
}
