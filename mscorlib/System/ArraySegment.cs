﻿using System;

namespace System
{
	// Token: 0x020006D3 RID: 1747
	[Serializable]
	public struct ArraySegment<T>
	{
		// Token: 0x0600433C RID: 17212 RVA: 0x000E5B00 File Offset: 0x000E3D00
		public ArraySegment(T[] array, int offset, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "Non-negative number required.");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "Non-negative number required.");
			}
			if (offset > array.Length)
			{
				throw new ArgumentException("out of bounds");
			}
			if (array.Length - offset < count)
			{
				throw new ArgumentException("out of bounds", "offset");
			}
			this.array = array;
			this.offset = offset;
			this.count = count;
		}

		// Token: 0x0600433D RID: 17213 RVA: 0x000E5B90 File Offset: 0x000E3D90
		public ArraySegment(T[] array)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			this.array = array;
			this.offset = 0;
			this.count = array.Length;
		}

		// Token: 0x17000C8E RID: 3214
		// (get) Token: 0x0600433E RID: 17214 RVA: 0x000E5BC8 File Offset: 0x000E3DC8
		public T[] Array
		{
			get
			{
				return this.array;
			}
		}

		// Token: 0x17000C8F RID: 3215
		// (get) Token: 0x0600433F RID: 17215 RVA: 0x000E5BD0 File Offset: 0x000E3DD0
		public int Offset
		{
			get
			{
				return this.offset;
			}
		}

		// Token: 0x17000C90 RID: 3216
		// (get) Token: 0x06004340 RID: 17216 RVA: 0x000E5BD8 File Offset: 0x000E3DD8
		public int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x06004341 RID: 17217 RVA: 0x000E5BE0 File Offset: 0x000E3DE0
		public override bool Equals(object obj)
		{
			return obj is ArraySegment<T> && this.Equals((ArraySegment<T>)obj);
		}

		// Token: 0x06004342 RID: 17218 RVA: 0x000E5BFC File Offset: 0x000E3DFC
		public bool Equals(ArraySegment<T> obj)
		{
			return this.array == obj.Array && this.offset == obj.Offset && this.count == obj.Count;
		}

		// Token: 0x06004343 RID: 17219 RVA: 0x000E5C38 File Offset: 0x000E3E38
		public override int GetHashCode()
		{
			return this.array.GetHashCode() ^ this.offset ^ this.count;
		}

		// Token: 0x06004344 RID: 17220 RVA: 0x000E5C54 File Offset: 0x000E3E54
		public static bool operator ==(ArraySegment<T> a, ArraySegment<T> b)
		{
			return a.Equals(b);
		}

		// Token: 0x06004345 RID: 17221 RVA: 0x000E5C60 File Offset: 0x000E3E60
		public static bool operator !=(ArraySegment<T> a, ArraySegment<T> b)
		{
			return !a.Equals(b);
		}

		// Token: 0x04001C5B RID: 7259
		private T[] array;

		// Token: 0x04001C5C RID: 7260
		private int offset;

		// Token: 0x04001C5D RID: 7261
		private int count;
	}
}
