﻿using System;

namespace System
{
	// Token: 0x02000111 RID: 273
	[Serializable]
	public sealed class ConsoleCancelEventArgs : EventArgs
	{
		// Token: 0x06000E48 RID: 3656 RVA: 0x0003C970 File Offset: 0x0003AB70
		internal ConsoleCancelEventArgs(ConsoleSpecialKey key)
		{
			this.specialKey = key;
		}

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x06000E49 RID: 3657 RVA: 0x0003C980 File Offset: 0x0003AB80
		// (set) Token: 0x06000E4A RID: 3658 RVA: 0x0003C988 File Offset: 0x0003AB88
		public bool Cancel
		{
			get
			{
				return this.cancel;
			}
			set
			{
				this.cancel = value;
			}
		}

		// Token: 0x17000219 RID: 537
		// (get) Token: 0x06000E4B RID: 3659 RVA: 0x0003C994 File Offset: 0x0003AB94
		public ConsoleSpecialKey SpecialKey
		{
			get
			{
				return this.specialKey;
			}
		}

		// Token: 0x040003CC RID: 972
		private bool cancel;

		// Token: 0x040003CD RID: 973
		private ConsoleSpecialKey specialKey;
	}
}
