﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200016B RID: 363
	[ComVisible(true)]
	[Serializable]
	public class PlatformNotSupportedException : NotSupportedException
	{
		// Token: 0x06001355 RID: 4949 RVA: 0x0004D654 File Offset: 0x0004B854
		public PlatformNotSupportedException() : base(Locale.GetText("This platform is not supported."))
		{
			base.HResult = -2146233031;
		}

		// Token: 0x06001356 RID: 4950 RVA: 0x0004D674 File Offset: 0x0004B874
		public PlatformNotSupportedException(string message) : base(message)
		{
			base.HResult = -2146233031;
		}

		// Token: 0x06001357 RID: 4951 RVA: 0x0004D688 File Offset: 0x0004B888
		protected PlatformNotSupportedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06001358 RID: 4952 RVA: 0x0004D694 File Offset: 0x0004B894
		public PlatformNotSupportedException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233031;
		}

		// Token: 0x0400059D RID: 1437
		private const int Result = -2146233031;
	}
}
