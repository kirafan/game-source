﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200011A RID: 282
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public class ContextStaticAttribute : Attribute
	{
	}
}
