﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200012C RID: 300
	[ComVisible(true)]
	[Serializable]
	public class EntryPointNotFoundException : TypeLoadException
	{
		// Token: 0x060010E3 RID: 4323 RVA: 0x000451EC File Offset: 0x000433EC
		public EntryPointNotFoundException() : base(Locale.GetText("Cannot load class because of missing entry method."))
		{
			base.HResult = -2146233053;
		}

		// Token: 0x060010E4 RID: 4324 RVA: 0x0004520C File Offset: 0x0004340C
		public EntryPointNotFoundException(string message) : base(message)
		{
			base.HResult = -2146233053;
		}

		// Token: 0x060010E5 RID: 4325 RVA: 0x00045220 File Offset: 0x00043420
		protected EntryPointNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060010E6 RID: 4326 RVA: 0x0004522C File Offset: 0x0004342C
		public EntryPointNotFoundException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233053;
		}

		// Token: 0x040004D3 RID: 1235
		private const int Result = -2146233053;
	}
}
