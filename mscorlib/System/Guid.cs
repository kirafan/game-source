﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;
using Mono.Security;

namespace System
{
	// Token: 0x0200013D RID: 317
	[ComVisible(true)]
	[Serializable]
	public struct Guid : IFormattable, IComparable, IComparable<Guid>, IEquatable<Guid>
	{
		// Token: 0x06001148 RID: 4424 RVA: 0x00046284 File Offset: 0x00044484
		public Guid(byte[] b)
		{
			Guid.CheckArray(b, 16);
			this._a = BitConverterLE.ToInt32(b, 0);
			this._b = BitConverterLE.ToInt16(b, 4);
			this._c = BitConverterLE.ToInt16(b, 6);
			this._d = b[8];
			this._e = b[9];
			this._f = b[10];
			this._g = b[11];
			this._h = b[12];
			this._i = b[13];
			this._j = b[14];
			this._k = b[15];
		}

		// Token: 0x06001149 RID: 4425 RVA: 0x00046310 File Offset: 0x00044510
		public Guid(string g)
		{
			Guid.CheckNull(g);
			g = g.Trim();
			Guid.GuidParser guidParser = new Guid.GuidParser(g);
			Guid guid = guidParser.Parse();
			this = guid;
		}

		// Token: 0x0600114A RID: 4426 RVA: 0x00046340 File Offset: 0x00044540
		public Guid(int a, short b, short c, byte[] d)
		{
			Guid.CheckArray(d, 8);
			this._a = a;
			this._b = b;
			this._c = c;
			this._d = d[0];
			this._e = d[1];
			this._f = d[2];
			this._g = d[3];
			this._h = d[4];
			this._i = d[5];
			this._j = d[6];
			this._k = d[7];
		}

		// Token: 0x0600114B RID: 4427 RVA: 0x000463BC File Offset: 0x000445BC
		public Guid(int a, short b, short c, byte d, byte e, byte f, byte g, byte h, byte i, byte j, byte k)
		{
			this._a = a;
			this._b = b;
			this._c = c;
			this._d = d;
			this._e = e;
			this._f = f;
			this._g = g;
			this._h = h;
			this._i = i;
			this._j = j;
			this._k = k;
		}

		// Token: 0x0600114C RID: 4428 RVA: 0x00046420 File Offset: 0x00044620
		[CLSCompliant(false)]
		public Guid(uint a, ushort b, ushort c, byte d, byte e, byte f, byte g, byte h, byte i, byte j, byte k)
		{
			this = new Guid((int)a, (short)b, (short)c, d, e, f, g, h, i, j, k);
		}

		// Token: 0x0600114E RID: 4430 RVA: 0x00046474 File Offset: 0x00044674
		private static void CheckNull(object o)
		{
			if (o == null)
			{
				throw new ArgumentNullException(Locale.GetText("Value cannot be null."));
			}
		}

		// Token: 0x0600114F RID: 4431 RVA: 0x0004648C File Offset: 0x0004468C
		private static void CheckLength(byte[] o, int l)
		{
			if (o.Length != l)
			{
				throw new ArgumentException(string.Format(Locale.GetText("Array should be exactly {0} bytes long."), l));
			}
		}

		// Token: 0x06001150 RID: 4432 RVA: 0x000464C0 File Offset: 0x000446C0
		private static void CheckArray(byte[] o, int l)
		{
			Guid.CheckNull(o);
			Guid.CheckLength(o, l);
		}

		// Token: 0x06001151 RID: 4433 RVA: 0x000464D0 File Offset: 0x000446D0
		private static int Compare(int x, int y)
		{
			if (x < y)
			{
				return -1;
			}
			return 1;
		}

		// Token: 0x06001152 RID: 4434 RVA: 0x000464DC File Offset: 0x000446DC
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is Guid))
			{
				throw new ArgumentException("value", Locale.GetText("Argument of System.Guid.CompareTo should be a Guid."));
			}
			return this.CompareTo((Guid)value);
		}

		// Token: 0x06001153 RID: 4435 RVA: 0x00046520 File Offset: 0x00044720
		public override bool Equals(object o)
		{
			return o is Guid && this.CompareTo((Guid)o) == 0;
		}

		// Token: 0x06001154 RID: 4436 RVA: 0x00046540 File Offset: 0x00044740
		public int CompareTo(Guid value)
		{
			if (this._a != value._a)
			{
				return Guid.Compare(this._a, value._a);
			}
			if (this._b != value._b)
			{
				return Guid.Compare((int)this._b, (int)value._b);
			}
			if (this._c != value._c)
			{
				return Guid.Compare((int)this._c, (int)value._c);
			}
			if (this._d != value._d)
			{
				return Guid.Compare((int)this._d, (int)value._d);
			}
			if (this._e != value._e)
			{
				return Guid.Compare((int)this._e, (int)value._e);
			}
			if (this._f != value._f)
			{
				return Guid.Compare((int)this._f, (int)value._f);
			}
			if (this._g != value._g)
			{
				return Guid.Compare((int)this._g, (int)value._g);
			}
			if (this._h != value._h)
			{
				return Guid.Compare((int)this._h, (int)value._h);
			}
			if (this._i != value._i)
			{
				return Guid.Compare((int)this._i, (int)value._i);
			}
			if (this._j != value._j)
			{
				return Guid.Compare((int)this._j, (int)value._j);
			}
			if (this._k != value._k)
			{
				return Guid.Compare((int)this._k, (int)value._k);
			}
			return 0;
		}

		// Token: 0x06001155 RID: 4437 RVA: 0x000466E8 File Offset: 0x000448E8
		public bool Equals(Guid g)
		{
			return this.CompareTo(g) == 0;
		}

		// Token: 0x06001156 RID: 4438 RVA: 0x000466F4 File Offset: 0x000448F4
		public override int GetHashCode()
		{
			int num = this._a;
			num ^= ((int)this._b << 16 | (int)this._c);
			num ^= (int)this._d << 24;
			num ^= (int)this._e << 16;
			num ^= (int)this._f << 8;
			num ^= (int)this._g;
			num ^= (int)this._h << 24;
			num ^= (int)this._i << 16;
			num ^= (int)this._j << 8;
			return num ^ (int)this._k;
		}

		// Token: 0x06001157 RID: 4439 RVA: 0x00046774 File Offset: 0x00044974
		private static char ToHex(int b)
		{
			return (char)((b >= 10) ? (97 + b - 10) : (48 + b));
		}

		// Token: 0x06001158 RID: 4440 RVA: 0x00046790 File Offset: 0x00044990
		public static Guid NewGuid()
		{
			byte[] array = new byte[16];
			object rngAccess = Guid._rngAccess;
			lock (rngAccess)
			{
				if (Guid._rng == null)
				{
					Guid._rng = RandomNumberGenerator.Create();
				}
				Guid._rng.GetBytes(array);
			}
			Guid result = new Guid(array);
			result._d = ((result._d & 63) | 128);
			result._c = (short)(((long)result._c & 4095L) | 16384L);
			return result;
		}

		// Token: 0x06001159 RID: 4441 RVA: 0x00046838 File Offset: 0x00044A38
		internal static byte[] FastNewGuidArray()
		{
			byte[] array = new byte[16];
			object rngAccess = Guid._rngAccess;
			lock (rngAccess)
			{
				if (Guid._rng != null)
				{
					Guid._fastRng = Guid._rng;
				}
				if (Guid._fastRng == null)
				{
					Guid._fastRng = new RNGCryptoServiceProvider();
				}
				Guid._fastRng.GetBytes(array);
			}
			array[8] = ((array[8] & 63) | 128);
			array[7] = ((array[7] & 15) | 64);
			return array;
		}

		// Token: 0x0600115A RID: 4442 RVA: 0x000468D4 File Offset: 0x00044AD4
		public byte[] ToByteArray()
		{
			byte[] array = new byte[16];
			int num = 0;
			byte[] bytes = BitConverterLE.GetBytes(this._a);
			for (int i = 0; i < 4; i++)
			{
				array[num++] = bytes[i];
			}
			bytes = BitConverterLE.GetBytes(this._b);
			for (int i = 0; i < 2; i++)
			{
				array[num++] = bytes[i];
			}
			bytes = BitConverterLE.GetBytes(this._c);
			for (int i = 0; i < 2; i++)
			{
				array[num++] = bytes[i];
			}
			array[8] = this._d;
			array[9] = this._e;
			array[10] = this._f;
			array[11] = this._g;
			array[12] = this._h;
			array[13] = this._i;
			array[14] = this._j;
			array[15] = this._k;
			return array;
		}

		// Token: 0x0600115B RID: 4443 RVA: 0x000469B4 File Offset: 0x00044BB4
		private static void AppendInt(StringBuilder builder, int value)
		{
			builder.Append(Guid.ToHex(value >> 28 & 15));
			builder.Append(Guid.ToHex(value >> 24 & 15));
			builder.Append(Guid.ToHex(value >> 20 & 15));
			builder.Append(Guid.ToHex(value >> 16 & 15));
			builder.Append(Guid.ToHex(value >> 12 & 15));
			builder.Append(Guid.ToHex(value >> 8 & 15));
			builder.Append(Guid.ToHex(value >> 4 & 15));
			builder.Append(Guid.ToHex(value & 15));
		}

		// Token: 0x0600115C RID: 4444 RVA: 0x00046A54 File Offset: 0x00044C54
		private static void AppendShort(StringBuilder builder, short value)
		{
			builder.Append(Guid.ToHex(value >> 12 & 15));
			builder.Append(Guid.ToHex(value >> 8 & 15));
			builder.Append(Guid.ToHex(value >> 4 & 15));
			builder.Append(Guid.ToHex((int)(value & 15)));
		}

		// Token: 0x0600115D RID: 4445 RVA: 0x00046AA8 File Offset: 0x00044CA8
		private static void AppendByte(StringBuilder builder, byte value)
		{
			builder.Append(Guid.ToHex(value >> 4 & 15));
			builder.Append(Guid.ToHex((int)(value & 15)));
		}

		// Token: 0x0600115E RID: 4446 RVA: 0x00046AD8 File Offset: 0x00044CD8
		private string BaseToString(bool h, bool p, bool b)
		{
			StringBuilder stringBuilder = new StringBuilder(40);
			if (p)
			{
				stringBuilder.Append('(');
			}
			else if (b)
			{
				stringBuilder.Append('{');
			}
			Guid.AppendInt(stringBuilder, this._a);
			if (h)
			{
				stringBuilder.Append('-');
			}
			Guid.AppendShort(stringBuilder, this._b);
			if (h)
			{
				stringBuilder.Append('-');
			}
			Guid.AppendShort(stringBuilder, this._c);
			if (h)
			{
				stringBuilder.Append('-');
			}
			Guid.AppendByte(stringBuilder, this._d);
			Guid.AppendByte(stringBuilder, this._e);
			if (h)
			{
				stringBuilder.Append('-');
			}
			Guid.AppendByte(stringBuilder, this._f);
			Guid.AppendByte(stringBuilder, this._g);
			Guid.AppendByte(stringBuilder, this._h);
			Guid.AppendByte(stringBuilder, this._i);
			Guid.AppendByte(stringBuilder, this._j);
			Guid.AppendByte(stringBuilder, this._k);
			if (p)
			{
				stringBuilder.Append(')');
			}
			else if (b)
			{
				stringBuilder.Append('}');
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0600115F RID: 4447 RVA: 0x00046BFC File Offset: 0x00044DFC
		public override string ToString()
		{
			return this.BaseToString(true, false, false);
		}

		// Token: 0x06001160 RID: 4448 RVA: 0x00046C08 File Offset: 0x00044E08
		public string ToString(string format)
		{
			bool h = true;
			bool p = false;
			bool b = false;
			if (format != null)
			{
				string a = format.ToLowerInvariant();
				if (a == "b")
				{
					b = true;
				}
				else if (a == "p")
				{
					p = true;
				}
				else if (a == "n")
				{
					h = false;
				}
				else if (a != "d" && a != string.Empty)
				{
					throw new FormatException(Locale.GetText("Argument to Guid.ToString(string format) should be \"b\", \"B\", \"d\", \"D\", \"n\", \"N\", \"p\" or \"P\""));
				}
			}
			return this.BaseToString(h, p, b);
		}

		// Token: 0x06001161 RID: 4449 RVA: 0x00046CA8 File Offset: 0x00044EA8
		public string ToString(string format, IFormatProvider provider)
		{
			return this.ToString(format);
		}

		// Token: 0x06001162 RID: 4450 RVA: 0x00046CB4 File Offset: 0x00044EB4
		public static bool operator ==(Guid a, Guid b)
		{
			return a.Equals(b);
		}

		// Token: 0x06001163 RID: 4451 RVA: 0x00046CC0 File Offset: 0x00044EC0
		public static bool operator !=(Guid a, Guid b)
		{
			return !a.Equals(b);
		}

		// Token: 0x0400050A RID: 1290
		private int _a;

		// Token: 0x0400050B RID: 1291
		private short _b;

		// Token: 0x0400050C RID: 1292
		private short _c;

		// Token: 0x0400050D RID: 1293
		private byte _d;

		// Token: 0x0400050E RID: 1294
		private byte _e;

		// Token: 0x0400050F RID: 1295
		private byte _f;

		// Token: 0x04000510 RID: 1296
		private byte _g;

		// Token: 0x04000511 RID: 1297
		private byte _h;

		// Token: 0x04000512 RID: 1298
		private byte _i;

		// Token: 0x04000513 RID: 1299
		private byte _j;

		// Token: 0x04000514 RID: 1300
		private byte _k;

		// Token: 0x04000515 RID: 1301
		public static readonly Guid Empty = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

		// Token: 0x04000516 RID: 1302
		private static object _rngAccess = new object();

		// Token: 0x04000517 RID: 1303
		private static RandomNumberGenerator _rng;

		// Token: 0x04000518 RID: 1304
		private static RandomNumberGenerator _fastRng;

		// Token: 0x0200013E RID: 318
		internal class GuidParser
		{
			// Token: 0x06001164 RID: 4452 RVA: 0x00046CD0 File Offset: 0x00044ED0
			public GuidParser(string src)
			{
				this._src = src;
				this.Reset();
			}

			// Token: 0x06001165 RID: 4453 RVA: 0x00046CE8 File Offset: 0x00044EE8
			private void Reset()
			{
				this._cur = 0;
				this._length = this._src.Length;
			}

			// Token: 0x06001166 RID: 4454 RVA: 0x00046D04 File Offset: 0x00044F04
			private bool AtEnd()
			{
				return this._cur >= this._length;
			}

			// Token: 0x06001167 RID: 4455 RVA: 0x00046D18 File Offset: 0x00044F18
			private void ThrowFormatException()
			{
				throw new FormatException(Locale.GetText("Invalid format for Guid.Guid(string)."));
			}

			// Token: 0x06001168 RID: 4456 RVA: 0x00046D2C File Offset: 0x00044F2C
			private ulong ParseHex(int length, bool strictLength)
			{
				ulong num = 0UL;
				bool flag = false;
				int num2 = 0;
				while (!flag && num2 < length)
				{
					if (this.AtEnd())
					{
						if (strictLength || num2 == 0)
						{
							this.ThrowFormatException();
						}
						else
						{
							flag = true;
						}
					}
					else
					{
						char c = char.ToLowerInvariant(this._src[this._cur]);
						if (char.IsDigit(c))
						{
							num = num * 16UL + (ulong)c - 48UL;
							this._cur++;
						}
						else if (c >= 'a' && c <= 'f')
						{
							num = num * 16UL + (ulong)c - 97UL + 10UL;
							this._cur++;
						}
						else if (strictLength || num2 == 0)
						{
							this.ThrowFormatException();
						}
						else
						{
							flag = true;
						}
					}
					num2++;
				}
				return num;
			}

			// Token: 0x06001169 RID: 4457 RVA: 0x00046E10 File Offset: 0x00045010
			private bool ParseOptChar(char c)
			{
				if (!this.AtEnd() && this._src[this._cur] == c)
				{
					this._cur++;
					return true;
				}
				return false;
			}

			// Token: 0x0600116A RID: 4458 RVA: 0x00046E48 File Offset: 0x00045048
			private void ParseChar(char c)
			{
				if (!this.ParseOptChar(c))
				{
					this.ThrowFormatException();
				}
			}

			// Token: 0x0600116B RID: 4459 RVA: 0x00046E6C File Offset: 0x0004506C
			private Guid ParseGuid1()
			{
				bool flag = true;
				char c = '}';
				byte[] array = new byte[8];
				bool flag2 = this.ParseOptChar('{');
				if (!flag2)
				{
					flag2 = this.ParseOptChar('(');
					if (flag2)
					{
						c = ')';
					}
				}
				int a = (int)this.ParseHex(8, true);
				if (flag2)
				{
					this.ParseChar('-');
				}
				else
				{
					flag = this.ParseOptChar('-');
				}
				short b = (short)this.ParseHex(4, true);
				if (flag)
				{
					this.ParseChar('-');
				}
				short c2 = (short)this.ParseHex(4, true);
				if (flag)
				{
					this.ParseChar('-');
				}
				for (int i = 0; i < 8; i++)
				{
					array[i] = (byte)this.ParseHex(2, true);
					if (i == 1 && flag)
					{
						this.ParseChar('-');
					}
				}
				if (flag2 && !this.ParseOptChar(c))
				{
					this.ThrowFormatException();
				}
				return new Guid(a, b, c2, array);
			}

			// Token: 0x0600116C RID: 4460 RVA: 0x00046F60 File Offset: 0x00045160
			private void ParseHexPrefix()
			{
				this.ParseChar('0');
				this.ParseChar('x');
			}

			// Token: 0x0600116D RID: 4461 RVA: 0x00046F74 File Offset: 0x00045174
			private Guid ParseGuid2()
			{
				byte[] array = new byte[8];
				this.ParseChar('{');
				this.ParseHexPrefix();
				int a = (int)this.ParseHex(8, false);
				this.ParseChar(',');
				this.ParseHexPrefix();
				short b = (short)this.ParseHex(4, false);
				this.ParseChar(',');
				this.ParseHexPrefix();
				short c = (short)this.ParseHex(4, false);
				this.ParseChar(',');
				this.ParseChar('{');
				for (int i = 0; i < 8; i++)
				{
					this.ParseHexPrefix();
					array[i] = (byte)this.ParseHex(2, false);
					if (i != 7)
					{
						this.ParseChar(',');
					}
				}
				this.ParseChar('}');
				this.ParseChar('}');
				return new Guid(a, b, c, array);
			}

			// Token: 0x0600116E RID: 4462 RVA: 0x00047034 File Offset: 0x00045234
			public Guid Parse()
			{
				Guid result;
				try
				{
					result = this.ParseGuid1();
				}
				catch (FormatException)
				{
					this.Reset();
					result = this.ParseGuid2();
				}
				if (!this.AtEnd())
				{
					this.ThrowFormatException();
				}
				return result;
			}

			// Token: 0x04000519 RID: 1305
			private string _src;

			// Token: 0x0400051A RID: 1306
			private int _length;

			// Token: 0x0400051B RID: 1307
			private int _cur;
		}
	}
}
