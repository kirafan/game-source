﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000106 RID: 262
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum AttributeTargets
	{
		// Token: 0x040003A7 RID: 935
		Assembly = 1,
		// Token: 0x040003A8 RID: 936
		Module = 2,
		// Token: 0x040003A9 RID: 937
		Class = 4,
		// Token: 0x040003AA RID: 938
		Struct = 8,
		// Token: 0x040003AB RID: 939
		Enum = 16,
		// Token: 0x040003AC RID: 940
		Constructor = 32,
		// Token: 0x040003AD RID: 941
		Method = 64,
		// Token: 0x040003AE RID: 942
		Property = 128,
		// Token: 0x040003AF RID: 943
		Field = 256,
		// Token: 0x040003B0 RID: 944
		Event = 512,
		// Token: 0x040003B1 RID: 945
		Interface = 1024,
		// Token: 0x040003B2 RID: 946
		Parameter = 2048,
		// Token: 0x040003B3 RID: 947
		Delegate = 4096,
		// Token: 0x040003B4 RID: 948
		ReturnValue = 8192,
		// Token: 0x040003B5 RID: 949
		GenericParameter = 16384,
		// Token: 0x040003B6 RID: 950
		All = 32767
	}
}
