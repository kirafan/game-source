﻿using System;

namespace System.Security
{
	// Token: 0x0200054B RID: 1355
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	[MonoTODO("Only supported by the runtime when CoreCLR is enabled")]
	public sealed class SecuritySafeCriticalAttribute : Attribute
	{
	}
}
