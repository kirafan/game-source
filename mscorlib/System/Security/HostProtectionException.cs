﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Security
{
	// Token: 0x02000533 RID: 1331
	[ComVisible(true)]
	[MonoTODO("Not supported in the runtime")]
	[Serializable]
	public class HostProtectionException : SystemException
	{
		// Token: 0x0600347B RID: 13435 RVA: 0x000AC664 File Offset: 0x000AA864
		public HostProtectionException()
		{
		}

		// Token: 0x0600347C RID: 13436 RVA: 0x000AC66C File Offset: 0x000AA86C
		public HostProtectionException(string message) : base(message)
		{
		}

		// Token: 0x0600347D RID: 13437 RVA: 0x000AC678 File Offset: 0x000AA878
		public HostProtectionException(string message, Exception e) : base(message, e)
		{
		}

		// Token: 0x0600347E RID: 13438 RVA: 0x000AC684 File Offset: 0x000AA884
		public HostProtectionException(string message, HostProtectionResource protectedResources, HostProtectionResource demandedResources) : base(message)
		{
			this._protected = protectedResources;
			this._demanded = demandedResources;
		}

		// Token: 0x0600347F RID: 13439 RVA: 0x000AC69C File Offset: 0x000AA89C
		protected HostProtectionException(SerializationInfo info, StreamingContext context)
		{
			this.GetObjectData(info, context);
		}

		// Token: 0x170009D1 RID: 2513
		// (get) Token: 0x06003480 RID: 13440 RVA: 0x000AC6AC File Offset: 0x000AA8AC
		public HostProtectionResource DemandedResources
		{
			get
			{
				return this._demanded;
			}
		}

		// Token: 0x170009D2 RID: 2514
		// (get) Token: 0x06003481 RID: 13441 RVA: 0x000AC6B4 File Offset: 0x000AA8B4
		public HostProtectionResource ProtectedResources
		{
			get
			{
				return this._protected;
			}
		}

		// Token: 0x06003482 RID: 13442 RVA: 0x000AC6BC File Offset: 0x000AA8BC
		[MonoTODO]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
		}

		// Token: 0x06003483 RID: 13443 RVA: 0x000AC6D0 File Offset: 0x000AA8D0
		[MonoTODO]
		public override string ToString()
		{
			return base.ToString();
		}

		// Token: 0x04001617 RID: 5655
		private HostProtectionResource _protected;

		// Token: 0x04001618 RID: 5656
		private HostProtectionResource _demanded;
	}
}
