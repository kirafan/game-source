﻿using System;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security
{
	// Token: 0x02000532 RID: 1330
	[MonoTODO("CAS support is experimental (and unsupported).")]
	[ComVisible(true)]
	[PermissionSet(SecurityAction.InheritanceDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
	[Serializable]
	public abstract class CodeAccessPermission : IPermission, ISecurityEncodable, IStackWalk
	{
		// Token: 0x0600345E RID: 13406 RVA: 0x000AC060 File Offset: 0x000AA260
		[MonoTODO("CAS support is experimental (and unsupported). Imperative mode is not implemented.")]
		public void Assert()
		{
		}

		// Token: 0x0600345F RID: 13407 RVA: 0x000AC064 File Offset: 0x000AA264
		internal bool CheckAssert(CodeAccessPermission asserted)
		{
			return asserted != null && asserted.GetType() == base.GetType() && this.IsSubsetOf(asserted);
		}

		// Token: 0x06003460 RID: 13408 RVA: 0x000AC094 File Offset: 0x000AA294
		internal bool CheckDemand(CodeAccessPermission target)
		{
			return target != null && target.GetType() == base.GetType() && this.IsSubsetOf(target);
		}

		// Token: 0x06003461 RID: 13409 RVA: 0x000AC0C4 File Offset: 0x000AA2C4
		internal bool CheckDeny(CodeAccessPermission denied)
		{
			if (denied == null)
			{
				return true;
			}
			Type type = denied.GetType();
			return type != base.GetType() || this.Intersect(denied) == null || denied.IsSubsetOf(PermissionBuilder.Create(type));
		}

		// Token: 0x06003462 RID: 13410 RVA: 0x000AC10C File Offset: 0x000AA30C
		internal bool CheckPermitOnly(CodeAccessPermission target)
		{
			return target != null && target.GetType() == base.GetType() && this.IsSubsetOf(target);
		}

		// Token: 0x06003463 RID: 13411
		public abstract IPermission Copy();

		// Token: 0x06003464 RID: 13412 RVA: 0x000AC13C File Offset: 0x000AA33C
		public void Demand()
		{
		}

		// Token: 0x06003465 RID: 13413 RVA: 0x000AC140 File Offset: 0x000AA340
		[MonoTODO("CAS support is experimental (and unsupported). Imperative mode is not implemented.")]
		public void Deny()
		{
		}

		// Token: 0x06003466 RID: 13414 RVA: 0x000AC144 File Offset: 0x000AA344
		[ComVisible(false)]
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (obj.GetType() != base.GetType())
			{
				return false;
			}
			CodeAccessPermission codeAccessPermission = obj as CodeAccessPermission;
			return this.IsSubsetOf(codeAccessPermission) && codeAccessPermission.IsSubsetOf(this);
		}

		// Token: 0x06003467 RID: 13415
		public abstract void FromXml(SecurityElement elem);

		// Token: 0x06003468 RID: 13416 RVA: 0x000AC18C File Offset: 0x000AA38C
		[ComVisible(false)]
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06003469 RID: 13417
		public abstract IPermission Intersect(IPermission target);

		// Token: 0x0600346A RID: 13418
		public abstract bool IsSubsetOf(IPermission target);

		// Token: 0x0600346B RID: 13419 RVA: 0x000AC194 File Offset: 0x000AA394
		public override string ToString()
		{
			SecurityElement securityElement = this.ToXml();
			return securityElement.ToString();
		}

		// Token: 0x0600346C RID: 13420
		public abstract SecurityElement ToXml();

		// Token: 0x0600346D RID: 13421 RVA: 0x000AC1B0 File Offset: 0x000AA3B0
		public virtual IPermission Union(IPermission other)
		{
			if (other != null)
			{
				throw new NotSupportedException();
			}
			return null;
		}

		// Token: 0x0600346E RID: 13422 RVA: 0x000AC1C0 File Offset: 0x000AA3C0
		[MonoTODO("CAS support is experimental (and unsupported). Imperative mode is not implemented.")]
		public void PermitOnly()
		{
		}

		// Token: 0x0600346F RID: 13423 RVA: 0x000AC1C4 File Offset: 0x000AA3C4
		[MonoTODO("CAS support is experimental (and unsupported). Imperative mode is not implemented.")]
		public static void RevertAll()
		{
		}

		// Token: 0x06003470 RID: 13424 RVA: 0x000AC1C8 File Offset: 0x000AA3C8
		[MonoTODO("CAS support is experimental (and unsupported). Imperative mode is not implemented.")]
		public static void RevertAssert()
		{
		}

		// Token: 0x06003471 RID: 13425 RVA: 0x000AC1CC File Offset: 0x000AA3CC
		[MonoTODO("CAS support is experimental (and unsupported). Imperative mode is not implemented.")]
		public static void RevertDeny()
		{
		}

		// Token: 0x06003472 RID: 13426 RVA: 0x000AC1D0 File Offset: 0x000AA3D0
		[MonoTODO("CAS support is experimental (and unsupported). Imperative mode is not implemented.")]
		public static void RevertPermitOnly()
		{
		}

		// Token: 0x06003473 RID: 13427 RVA: 0x000AC1D4 File Offset: 0x000AA3D4
		internal SecurityElement Element(int version)
		{
			SecurityElement securityElement = new SecurityElement("IPermission");
			Type type = base.GetType();
			securityElement.AddAttribute("class", type.FullName + ", " + type.Assembly.ToString().Replace('"', '\''));
			securityElement.AddAttribute("version", version.ToString());
			return securityElement;
		}

		// Token: 0x06003474 RID: 13428 RVA: 0x000AC238 File Offset: 0x000AA438
		internal static PermissionState CheckPermissionState(PermissionState state, bool allowUnrestricted)
		{
			if (state != PermissionState.None)
			{
				if (state != PermissionState.Unrestricted)
				{
					string message = string.Format(Locale.GetText("Invalid enum {0}"), state);
					throw new ArgumentException(message, "state");
				}
			}
			return state;
		}

		// Token: 0x06003475 RID: 13429 RVA: 0x000AC288 File Offset: 0x000AA488
		internal static int CheckSecurityElement(SecurityElement se, string parameterName, int minimumVersion, int maximumVersion)
		{
			if (se == null)
			{
				throw new ArgumentNullException(parameterName);
			}
			if (se.Tag != "IPermission")
			{
				string message = string.Format(Locale.GetText("Invalid tag {0}"), se.Tag);
				throw new ArgumentException(message, parameterName);
			}
			int num = minimumVersion;
			string text = se.Attribute("version");
			if (text != null)
			{
				try
				{
					num = int.Parse(text);
				}
				catch (Exception innerException)
				{
					string text2 = Locale.GetText("Couldn't parse version from '{0}'.");
					text2 = string.Format(text2, text);
					throw new ArgumentException(text2, parameterName, innerException);
				}
			}
			if (num < minimumVersion || num > maximumVersion)
			{
				string text3 = Locale.GetText("Unknown version '{0}', expected versions between ['{1}','{2}'].");
				text3 = string.Format(text3, num, minimumVersion, maximumVersion);
				throw new ArgumentException(text3, parameterName);
			}
			return num;
		}

		// Token: 0x06003476 RID: 13430 RVA: 0x000AC378 File Offset: 0x000AA578
		internal static bool IsUnrestricted(SecurityElement se)
		{
			string text = se.Attribute("Unrestricted");
			return text != null && string.Compare(text, bool.TrueString, true, CultureInfo.InvariantCulture) == 0;
		}

		// Token: 0x06003477 RID: 13431 RVA: 0x000AC3B0 File Offset: 0x000AA5B0
		internal bool ProcessFrame(SecurityFrame frame)
		{
			if (frame.PermitOnly != null)
			{
				bool flag = frame.PermitOnly.IsUnrestricted();
				if (!flag)
				{
					foreach (object obj in frame.PermitOnly)
					{
						IPermission permission = (IPermission)obj;
						if (this.CheckPermitOnly(permission as CodeAccessPermission))
						{
							flag = true;
							break;
						}
					}
				}
				if (!flag)
				{
					CodeAccessPermission.ThrowSecurityException(this, "PermitOnly", frame, SecurityAction.Demand, null);
				}
			}
			if (frame.Deny != null)
			{
				if (frame.Deny.IsUnrestricted())
				{
					CodeAccessPermission.ThrowSecurityException(this, "Deny", frame, SecurityAction.Demand, null);
				}
				foreach (object obj2 in frame.Deny)
				{
					IPermission permission2 = (IPermission)obj2;
					if (!this.CheckDeny(permission2 as CodeAccessPermission))
					{
						CodeAccessPermission.ThrowSecurityException(this, "Deny", frame, SecurityAction.Demand, permission2);
					}
				}
			}
			if (frame.Assert != null)
			{
				if (frame.Assert.IsUnrestricted())
				{
					return true;
				}
				foreach (object obj3 in frame.Assert)
				{
					IPermission permission3 = (IPermission)obj3;
					if (this.CheckAssert(permission3 as CodeAccessPermission))
					{
						return true;
					}
				}
				return false;
			}
			return false;
		}

		// Token: 0x06003478 RID: 13432 RVA: 0x000AC5B0 File Offset: 0x000AA7B0
		internal static void ThrowInvalidPermission(IPermission target, Type expected)
		{
			string text = Locale.GetText("Invalid permission type '{0}', expected type '{1}'.");
			text = string.Format(text, target.GetType(), expected);
			throw new ArgumentException(text, "target");
		}

		// Token: 0x06003479 RID: 13433 RVA: 0x000AC5E4 File Offset: 0x000AA7E4
		internal static void ThrowExecutionEngineException(SecurityAction stackmod)
		{
			string text = Locale.GetText("No {0} modifier is present on the current stack frame.");
			text = text + Environment.NewLine + "Currently only declarative stack modifiers are supported.";
			throw new ExecutionEngineException(string.Format(text, stackmod));
		}

		// Token: 0x0600347A RID: 13434 RVA: 0x000AC620 File Offset: 0x000AA820
		internal static void ThrowSecurityException(object demanded, string message, SecurityFrame frame, SecurityAction action, IPermission failed)
		{
			Assembly assembly = frame.Assembly;
			throw new SecurityException(Locale.GetText(message), assembly.UnprotectedGetName(), assembly.GrantedPermissionSet, assembly.DeniedPermissionSet, frame.Method, action, demanded, failed, assembly.UnprotectedGetEvidence());
		}
	}
}
