﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security
{
	// Token: 0x0200053B RID: 1339
	[ComVisible(true)]
	[Serializable]
	public sealed class NamedPermissionSet : PermissionSet
	{
		// Token: 0x06003499 RID: 13465 RVA: 0x000AC820 File Offset: 0x000AAA20
		internal NamedPermissionSet()
		{
		}

		// Token: 0x0600349A RID: 13466 RVA: 0x000AC828 File Offset: 0x000AAA28
		public NamedPermissionSet(string name, PermissionSet permSet) : base(permSet)
		{
			this.Name = name;
		}

		// Token: 0x0600349B RID: 13467 RVA: 0x000AC838 File Offset: 0x000AAA38
		public NamedPermissionSet(string name, PermissionState state) : base(state)
		{
			this.Name = name;
		}

		// Token: 0x0600349C RID: 13468 RVA: 0x000AC848 File Offset: 0x000AAA48
		public NamedPermissionSet(NamedPermissionSet permSet) : base(permSet)
		{
			this.name = permSet.name;
			this.description = permSet.description;
		}

		// Token: 0x0600349D RID: 13469 RVA: 0x000AC86C File Offset: 0x000AAA6C
		public NamedPermissionSet(string name) : this(name, PermissionState.Unrestricted)
		{
		}

		// Token: 0x170009D6 RID: 2518
		// (get) Token: 0x0600349E RID: 13470 RVA: 0x000AC878 File Offset: 0x000AAA78
		// (set) Token: 0x0600349F RID: 13471 RVA: 0x000AC880 File Offset: 0x000AAA80
		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		// Token: 0x170009D7 RID: 2519
		// (get) Token: 0x060034A0 RID: 13472 RVA: 0x000AC88C File Offset: 0x000AAA8C
		// (set) Token: 0x060034A1 RID: 13473 RVA: 0x000AC894 File Offset: 0x000AAA94
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				if (value == null || value == string.Empty)
				{
					throw new ArgumentException(Locale.GetText("invalid name"));
				}
				this.name = value;
			}
		}

		// Token: 0x060034A2 RID: 13474 RVA: 0x000AC8C4 File Offset: 0x000AAAC4
		public override PermissionSet Copy()
		{
			return new NamedPermissionSet(this);
		}

		// Token: 0x060034A3 RID: 13475 RVA: 0x000AC8CC File Offset: 0x000AAACC
		public NamedPermissionSet Copy(string name)
		{
			return new NamedPermissionSet(this)
			{
				Name = name
			};
		}

		// Token: 0x060034A4 RID: 13476 RVA: 0x000AC8E8 File Offset: 0x000AAAE8
		public override void FromXml(SecurityElement et)
		{
			base.FromXml(et);
			this.name = et.Attribute("Name");
			this.description = et.Attribute("Description");
			if (this.description == null)
			{
				this.description = string.Empty;
			}
		}

		// Token: 0x060034A5 RID: 13477 RVA: 0x000AC934 File Offset: 0x000AAB34
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.ToXml();
			if (this.name != null)
			{
				securityElement.AddAttribute("Name", this.name);
			}
			if (this.description != null)
			{
				securityElement.AddAttribute("Description", this.description);
			}
			return securityElement;
		}

		// Token: 0x060034A6 RID: 13478 RVA: 0x000AC984 File Offset: 0x000AAB84
		[ComVisible(false)]
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			NamedPermissionSet namedPermissionSet = obj as NamedPermissionSet;
			return namedPermissionSet != null && this.name == namedPermissionSet.Name && base.Equals(obj);
		}

		// Token: 0x060034A7 RID: 13479 RVA: 0x000AC9C8 File Offset: 0x000AABC8
		[ComVisible(false)]
		public override int GetHashCode()
		{
			int num = base.GetHashCode();
			if (this.name != null)
			{
				num ^= this.name.GetHashCode();
			}
			return num;
		}

		// Token: 0x04001621 RID: 5665
		private string name;

		// Token: 0x04001622 RID: 5666
		private string description;
	}
}
