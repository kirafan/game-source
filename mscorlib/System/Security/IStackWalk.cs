﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	// Token: 0x0200053A RID: 1338
	[ComVisible(true)]
	public interface IStackWalk
	{
		// Token: 0x06003495 RID: 13461
		void Assert();

		// Token: 0x06003496 RID: 13462
		void Demand();

		// Token: 0x06003497 RID: 13463
		void Deny();

		// Token: 0x06003498 RID: 13464
		void PermitOnly();
	}
}
