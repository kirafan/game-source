﻿using System;

namespace System.Security
{
	// Token: 0x02000542 RID: 1346
	public enum SecurityCriticalScope
	{
		// Token: 0x04001642 RID: 5698
		Explicit,
		// Token: 0x04001643 RID: 5699
		Everything
	}
}
