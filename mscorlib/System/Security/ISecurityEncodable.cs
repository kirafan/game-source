﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	// Token: 0x02000538 RID: 1336
	[ComVisible(true)]
	public interface ISecurityEncodable
	{
		// Token: 0x06003491 RID: 13457
		void FromXml(SecurityElement e);

		// Token: 0x06003492 RID: 13458
		SecurityElement ToXml();
	}
}
