﻿using System;

namespace System.Security
{
	// Token: 0x02000541 RID: 1345
	[MonoTODO("Only supported by the runtime when CoreCLR is enabled")]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Module | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Delegate, AllowMultiple = false, Inherited = false)]
	public sealed class SecurityCriticalAttribute : Attribute
	{
		// Token: 0x06003503 RID: 13571 RVA: 0x000AEC6C File Offset: 0x000ACE6C
		public SecurityCriticalAttribute()
		{
			this._scope = SecurityCriticalScope.Explicit;
		}

		// Token: 0x06003504 RID: 13572 RVA: 0x000AEC7C File Offset: 0x000ACE7C
		public SecurityCriticalAttribute(SecurityCriticalScope scope)
		{
			if (scope != SecurityCriticalScope.Everything)
			{
				this._scope = SecurityCriticalScope.Explicit;
			}
			else
			{
				this._scope = SecurityCriticalScope.Everything;
			}
		}

		// Token: 0x170009E3 RID: 2531
		// (get) Token: 0x06003505 RID: 13573 RVA: 0x000AECB8 File Offset: 0x000ACEB8
		public SecurityCriticalScope Scope
		{
			get
			{
				return this._scope;
			}
		}

		// Token: 0x04001640 RID: 5696
		private SecurityCriticalScope _scope;
	}
}
