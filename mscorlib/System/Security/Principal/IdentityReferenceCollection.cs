﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x0200065F RID: 1631
	[ComVisible(false)]
	public class IdentityReferenceCollection : IEnumerable, ICollection<IdentityReference>, IEnumerable<IdentityReference>
	{
		// Token: 0x06003E14 RID: 15892 RVA: 0x000D5B18 File Offset: 0x000D3D18
		public IdentityReferenceCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x06003E15 RID: 15893 RVA: 0x000D5B2C File Offset: 0x000D3D2C
		public IdentityReferenceCollection(int capacity)
		{
			this._list = new ArrayList(capacity);
		}

		// Token: 0x06003E16 RID: 15894 RVA: 0x000D5B40 File Offset: 0x000D3D40
		IEnumerator IEnumerable.GetEnumerator()
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000BB8 RID: 3000
		// (get) Token: 0x06003E17 RID: 15895 RVA: 0x000D5B48 File Offset: 0x000D3D48
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x17000BB9 RID: 3001
		// (get) Token: 0x06003E18 RID: 15896 RVA: 0x000D5B58 File Offset: 0x000D3D58
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000BBA RID: 3002
		public IdentityReference this[int index]
		{
			get
			{
				if (index >= this._list.Count)
				{
					return null;
				}
				return (IdentityReference)this._list[index];
			}
			set
			{
				this._list[index] = value;
			}
		}

		// Token: 0x06003E1B RID: 15899 RVA: 0x000D5BA0 File Offset: 0x000D3DA0
		public void Add(IdentityReference identity)
		{
			this._list.Add(identity);
		}

		// Token: 0x06003E1C RID: 15900 RVA: 0x000D5BB0 File Offset: 0x000D3DB0
		public void Clear()
		{
			this._list.Clear();
		}

		// Token: 0x06003E1D RID: 15901 RVA: 0x000D5BC0 File Offset: 0x000D3DC0
		public bool Contains(IdentityReference identity)
		{
			foreach (object obj in this._list)
			{
				IdentityReference identityReference = (IdentityReference)obj;
				if (identityReference.Equals(identity))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003E1E RID: 15902 RVA: 0x000D5C40 File Offset: 0x000D3E40
		public void CopyTo(IdentityReference[] array, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003E1F RID: 15903 RVA: 0x000D5C48 File Offset: 0x000D3E48
		public IEnumerator<IdentityReference> GetEnumerator()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003E20 RID: 15904 RVA: 0x000D5C50 File Offset: 0x000D3E50
		public bool Remove(IdentityReference identity)
		{
			foreach (object obj in this._list)
			{
				IdentityReference identityReference = (IdentityReference)obj;
				if (identityReference.Equals(identity))
				{
					this._list.Remove(identityReference);
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003E21 RID: 15905 RVA: 0x000D5CDC File Offset: 0x000D3EDC
		public IdentityReferenceCollection Translate(Type targetType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003E22 RID: 15906 RVA: 0x000D5CE4 File Offset: 0x000D3EE4
		public IdentityReferenceCollection Translate(Type targetType, bool forceSuccess)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04001AB6 RID: 6838
		private ArrayList _list;
	}
}
