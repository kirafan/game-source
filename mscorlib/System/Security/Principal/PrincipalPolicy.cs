﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000664 RID: 1636
	[ComVisible(true)]
	[Serializable]
	public enum PrincipalPolicy
	{
		// Token: 0x04001AB9 RID: 6841
		UnauthenticatedPrincipal,
		// Token: 0x04001ABA RID: 6842
		NoPrincipal,
		// Token: 0x04001ABB RID: 6843
		WindowsPrincipal
	}
}
