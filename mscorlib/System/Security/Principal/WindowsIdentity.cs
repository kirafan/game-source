﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Security.Principal
{
	// Token: 0x0200066B RID: 1643
	[ComVisible(true)]
	[Serializable]
	public class WindowsIdentity : IDisposable, ISerializable, IDeserializationCallback, IIdentity
	{
		// Token: 0x06003E50 RID: 15952 RVA: 0x000D619C File Offset: 0x000D439C
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public WindowsIdentity(IntPtr userToken) : this(userToken, null, WindowsAccountType.Normal, false)
		{
		}

		// Token: 0x06003E51 RID: 15953 RVA: 0x000D61A8 File Offset: 0x000D43A8
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public WindowsIdentity(IntPtr userToken, string type) : this(userToken, type, WindowsAccountType.Normal, false)
		{
		}

		// Token: 0x06003E52 RID: 15954 RVA: 0x000D61B4 File Offset: 0x000D43B4
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public WindowsIdentity(IntPtr userToken, string type, WindowsAccountType acctType) : this(userToken, type, acctType, false)
		{
		}

		// Token: 0x06003E53 RID: 15955 RVA: 0x000D61C0 File Offset: 0x000D43C0
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public WindowsIdentity(IntPtr userToken, string type, WindowsAccountType acctType, bool isAuthenticated)
		{
			this._type = type;
			this._account = acctType;
			this._authenticated = isAuthenticated;
			this._name = null;
			this.SetToken(userToken);
		}

		// Token: 0x06003E54 RID: 15956 RVA: 0x000D61F8 File Offset: 0x000D43F8
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public WindowsIdentity(string sUserPrincipalName) : this(sUserPrincipalName, null)
		{
		}

		// Token: 0x06003E55 RID: 15957 RVA: 0x000D6204 File Offset: 0x000D4404
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public WindowsIdentity(string sUserPrincipalName, string type)
		{
			if (sUserPrincipalName == null)
			{
				throw new NullReferenceException("sUserPrincipalName");
			}
			IntPtr userToken = WindowsIdentity.GetUserToken(sUserPrincipalName);
			if (!WindowsIdentity.IsPosix && userToken == IntPtr.Zero)
			{
				throw new ArgumentException("only for Windows Server 2003 +");
			}
			this._authenticated = true;
			this._account = WindowsAccountType.Normal;
			this._type = type;
			this.SetToken(userToken);
		}

		// Token: 0x06003E56 RID: 15958 RVA: 0x000D6270 File Offset: 0x000D4470
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public WindowsIdentity(SerializationInfo info, StreamingContext context)
		{
			this._info = info;
		}

		// Token: 0x06003E58 RID: 15960 RVA: 0x000D628C File Offset: 0x000D448C
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			this._token = (IntPtr)this._info.GetValue("m_userToken", typeof(IntPtr));
			this._name = this._info.GetString("m_name");
			if (this._name != null)
			{
				string tokenName = WindowsIdentity.GetTokenName(this._token);
				if (tokenName != this._name)
				{
					throw new SerializationException("Token-Name mismatch.");
				}
			}
			else
			{
				this._name = WindowsIdentity.GetTokenName(this._token);
				if (this._name == string.Empty || this._name == null)
				{
					throw new SerializationException("Token doesn't match a user.");
				}
			}
			this._type = this._info.GetString("m_type");
			this._account = (WindowsAccountType)((int)this._info.GetValue("m_acctType", typeof(WindowsAccountType)));
			this._authenticated = this._info.GetBoolean("m_isAuthenticated");
		}

		// Token: 0x06003E59 RID: 15961 RVA: 0x000D639C File Offset: 0x000D459C
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("m_userToken", this._token);
			info.AddValue("m_name", this._name);
			info.AddValue("m_type", this._type);
			info.AddValue("m_acctType", this._account);
			info.AddValue("m_isAuthenticated", this._authenticated);
		}

		// Token: 0x06003E5A RID: 15962 RVA: 0x000D6408 File Offset: 0x000D4608
		[ComVisible(false)]
		public void Dispose()
		{
			this._token = IntPtr.Zero;
		}

		// Token: 0x06003E5B RID: 15963 RVA: 0x000D6418 File Offset: 0x000D4618
		[ComVisible(false)]
		protected virtual void Dispose(bool disposing)
		{
			this._token = IntPtr.Zero;
		}

		// Token: 0x06003E5C RID: 15964 RVA: 0x000D6428 File Offset: 0x000D4628
		public static WindowsIdentity GetAnonymous()
		{
			WindowsIdentity windowsIdentity;
			if (WindowsIdentity.IsPosix)
			{
				windowsIdentity = new WindowsIdentity("nobody");
				windowsIdentity._account = WindowsAccountType.Anonymous;
				windowsIdentity._authenticated = false;
				windowsIdentity._type = string.Empty;
			}
			else
			{
				windowsIdentity = new WindowsIdentity(IntPtr.Zero, string.Empty, WindowsAccountType.Anonymous, false);
				windowsIdentity._name = string.Empty;
			}
			return windowsIdentity;
		}

		// Token: 0x06003E5D RID: 15965 RVA: 0x000D6488 File Offset: 0x000D4688
		public static WindowsIdentity GetCurrent()
		{
			return new WindowsIdentity(WindowsIdentity.GetCurrentToken(), null, WindowsAccountType.Normal, true);
		}

		// Token: 0x06003E5E RID: 15966 RVA: 0x000D6498 File Offset: 0x000D4698
		[MonoTODO("need icall changes")]
		public static WindowsIdentity GetCurrent(bool ifImpersonating)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003E5F RID: 15967 RVA: 0x000D64A0 File Offset: 0x000D46A0
		[MonoTODO("need icall changes")]
		public static WindowsIdentity GetCurrent(TokenAccessLevels desiredAccess)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003E60 RID: 15968 RVA: 0x000D64A8 File Offset: 0x000D46A8
		public virtual WindowsImpersonationContext Impersonate()
		{
			return new WindowsImpersonationContext(this._token);
		}

		// Token: 0x06003E61 RID: 15969 RVA: 0x000D64B8 File Offset: 0x000D46B8
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public static WindowsImpersonationContext Impersonate(IntPtr userToken)
		{
			return new WindowsImpersonationContext(userToken);
		}

		// Token: 0x17000BC4 RID: 3012
		// (get) Token: 0x06003E62 RID: 15970 RVA: 0x000D64C0 File Offset: 0x000D46C0
		public string AuthenticationType
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x17000BC5 RID: 3013
		// (get) Token: 0x06003E63 RID: 15971 RVA: 0x000D64C8 File Offset: 0x000D46C8
		public virtual bool IsAnonymous
		{
			get
			{
				return this._account == WindowsAccountType.Anonymous;
			}
		}

		// Token: 0x17000BC6 RID: 3014
		// (get) Token: 0x06003E64 RID: 15972 RVA: 0x000D64D4 File Offset: 0x000D46D4
		public virtual bool IsAuthenticated
		{
			get
			{
				return this._authenticated;
			}
		}

		// Token: 0x17000BC7 RID: 3015
		// (get) Token: 0x06003E65 RID: 15973 RVA: 0x000D64DC File Offset: 0x000D46DC
		public virtual bool IsGuest
		{
			get
			{
				return this._account == WindowsAccountType.Guest;
			}
		}

		// Token: 0x17000BC8 RID: 3016
		// (get) Token: 0x06003E66 RID: 15974 RVA: 0x000D64E8 File Offset: 0x000D46E8
		public virtual bool IsSystem
		{
			get
			{
				return this._account == WindowsAccountType.System;
			}
		}

		// Token: 0x17000BC9 RID: 3017
		// (get) Token: 0x06003E67 RID: 15975 RVA: 0x000D64F4 File Offset: 0x000D46F4
		public virtual string Name
		{
			get
			{
				if (this._name == null)
				{
					this._name = WindowsIdentity.GetTokenName(this._token);
				}
				return this._name;
			}
		}

		// Token: 0x17000BCA RID: 3018
		// (get) Token: 0x06003E68 RID: 15976 RVA: 0x000D6524 File Offset: 0x000D4724
		public virtual IntPtr Token
		{
			get
			{
				return this._token;
			}
		}

		// Token: 0x17000BCB RID: 3019
		// (get) Token: 0x06003E69 RID: 15977 RVA: 0x000D652C File Offset: 0x000D472C
		[MonoTODO("not implemented")]
		public IdentityReferenceCollection Groups
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000BCC RID: 3020
		// (get) Token: 0x06003E6A RID: 15978 RVA: 0x000D6534 File Offset: 0x000D4734
		[MonoTODO("not implemented")]
		[ComVisible(false)]
		public TokenImpersonationLevel ImpersonationLevel
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000BCD RID: 3021
		// (get) Token: 0x06003E6B RID: 15979 RVA: 0x000D653C File Offset: 0x000D473C
		[MonoTODO("not implemented")]
		[ComVisible(false)]
		public SecurityIdentifier Owner
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000BCE RID: 3022
		// (get) Token: 0x06003E6C RID: 15980 RVA: 0x000D6544 File Offset: 0x000D4744
		[ComVisible(false)]
		[MonoTODO("not implemented")]
		public SecurityIdentifier User
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000BCF RID: 3023
		// (get) Token: 0x06003E6D RID: 15981 RVA: 0x000D654C File Offset: 0x000D474C
		private static bool IsPosix
		{
			get
			{
				int platform = (int)Environment.Platform;
				return platform == 128 || platform == 4 || platform == 6;
			}
		}

		// Token: 0x06003E6E RID: 15982 RVA: 0x000D6578 File Offset: 0x000D4778
		private void SetToken(IntPtr token)
		{
			if (WindowsIdentity.IsPosix)
			{
				this._token = token;
				if (this._type == null)
				{
					this._type = "POSIX";
				}
				if (this._token == IntPtr.Zero)
				{
					this._account = WindowsAccountType.System;
				}
			}
			else
			{
				if (token == WindowsIdentity.invalidWindows && this._account != WindowsAccountType.Anonymous)
				{
					throw new ArgumentException("Invalid token");
				}
				this._token = token;
				if (this._type == null)
				{
					this._type = "NTLM";
				}
			}
		}

		// Token: 0x06003E6F RID: 15983
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string[] _GetRoles(IntPtr token);

		// Token: 0x06003E70 RID: 15984
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern IntPtr GetCurrentToken();

		// Token: 0x06003E71 RID: 15985
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetTokenName(IntPtr token);

		// Token: 0x06003E72 RID: 15986
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetUserToken(string username);

		// Token: 0x04001B21 RID: 6945
		private IntPtr _token;

		// Token: 0x04001B22 RID: 6946
		private string _type;

		// Token: 0x04001B23 RID: 6947
		private WindowsAccountType _account;

		// Token: 0x04001B24 RID: 6948
		private bool _authenticated;

		// Token: 0x04001B25 RID: 6949
		private string _name;

		// Token: 0x04001B26 RID: 6950
		private SerializationInfo _info;

		// Token: 0x04001B27 RID: 6951
		private static IntPtr invalidWindows = IntPtr.Zero;
	}
}
