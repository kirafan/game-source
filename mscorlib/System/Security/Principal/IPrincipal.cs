﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000662 RID: 1634
	[ComVisible(true)]
	public interface IPrincipal
	{
		// Token: 0x17000BBF RID: 3007
		// (get) Token: 0x06003E2F RID: 15919
		IIdentity Identity { get; }

		// Token: 0x06003E30 RID: 15920
		bool IsInRole(string role);
	}
}
