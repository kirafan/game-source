﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x0200066D RID: 1645
	[ComVisible(true)]
	[Serializable]
	public class WindowsPrincipal : IPrincipal
	{
		// Token: 0x06003E7B RID: 15995 RVA: 0x000D66C4 File Offset: 0x000D48C4
		public WindowsPrincipal(WindowsIdentity ntIdentity)
		{
			if (ntIdentity == null)
			{
				throw new ArgumentNullException("ntIdentity");
			}
			this._identity = ntIdentity;
		}

		// Token: 0x17000BD0 RID: 3024
		// (get) Token: 0x06003E7C RID: 15996 RVA: 0x000D66E4 File Offset: 0x000D48E4
		public virtual IIdentity Identity
		{
			get
			{
				return this._identity;
			}
		}

		// Token: 0x06003E7D RID: 15997 RVA: 0x000D66EC File Offset: 0x000D48EC
		public virtual bool IsInRole(int rid)
		{
			if (WindowsPrincipal.IsPosix)
			{
				return WindowsPrincipal.IsMemberOfGroupId(this.Token, (IntPtr)rid);
			}
			string role;
			switch (rid)
			{
			case 544:
				role = "BUILTIN\\Administrators";
				break;
			case 545:
				role = "BUILTIN\\Users";
				break;
			case 546:
				role = "BUILTIN\\Guests";
				break;
			case 547:
				role = "BUILTIN\\Power Users";
				break;
			case 548:
				role = "BUILTIN\\Account Operators";
				break;
			case 549:
				role = "BUILTIN\\System Operators";
				break;
			case 550:
				role = "BUILTIN\\Print Operators";
				break;
			case 551:
				role = "BUILTIN\\Backup Operators";
				break;
			case 552:
				role = "BUILTIN\\Replicator";
				break;
			default:
				return false;
			}
			return this.IsInRole(role);
		}

		// Token: 0x06003E7E RID: 15998 RVA: 0x000D67BC File Offset: 0x000D49BC
		public virtual bool IsInRole(string role)
		{
			if (role == null)
			{
				return false;
			}
			if (WindowsPrincipal.IsPosix)
			{
				return WindowsPrincipal.IsMemberOfGroupName(this.Token, role);
			}
			if (this.m_roles == null)
			{
				this.m_roles = WindowsIdentity._GetRoles(this.Token);
			}
			role = role.ToUpperInvariant();
			foreach (string text in this.m_roles)
			{
				if (text != null && role == text.ToUpperInvariant())
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003E7F RID: 15999 RVA: 0x000D6848 File Offset: 0x000D4A48
		public virtual bool IsInRole(WindowsBuiltInRole role)
		{
			if (!WindowsPrincipal.IsPosix)
			{
				return this.IsInRole((int)role);
			}
			if (role != WindowsBuiltInRole.Administrator)
			{
				return false;
			}
			string role2 = "root";
			return this.IsInRole(role2);
		}

		// Token: 0x06003E80 RID: 16000 RVA: 0x000D6890 File Offset: 0x000D4A90
		[MonoTODO("not implemented")]
		[ComVisible(false)]
		public virtual bool IsInRole(SecurityIdentifier sid)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000BD1 RID: 3025
		// (get) Token: 0x06003E81 RID: 16001 RVA: 0x000D6898 File Offset: 0x000D4A98
		private static bool IsPosix
		{
			get
			{
				int platform = (int)Environment.Platform;
				return platform == 128 || platform == 4 || platform == 6;
			}
		}

		// Token: 0x17000BD2 RID: 3026
		// (get) Token: 0x06003E82 RID: 16002 RVA: 0x000D68C4 File Offset: 0x000D4AC4
		private IntPtr Token
		{
			get
			{
				return this._identity.Token;
			}
		}

		// Token: 0x06003E83 RID: 16003
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsMemberOfGroupId(IntPtr user, IntPtr group);

		// Token: 0x06003E84 RID: 16004
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsMemberOfGroupName(IntPtr user, string group);

		// Token: 0x04001B2A RID: 6954
		private WindowsIdentity _identity;

		// Token: 0x04001B2B RID: 6955
		private string[] m_roles;
	}
}
