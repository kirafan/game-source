﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x0200065C RID: 1628
	[ComVisible(true)]
	[Serializable]
	public class GenericIdentity : IIdentity
	{
		// Token: 0x06003E07 RID: 15879 RVA: 0x000D5984 File Offset: 0x000D3B84
		public GenericIdentity(string name, string type)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			this.m_name = name;
			this.m_type = type;
		}

		// Token: 0x06003E08 RID: 15880 RVA: 0x000D59C8 File Offset: 0x000D3BC8
		public GenericIdentity(string name) : this(name, string.Empty)
		{
		}

		// Token: 0x17000BB3 RID: 2995
		// (get) Token: 0x06003E09 RID: 15881 RVA: 0x000D59D8 File Offset: 0x000D3BD8
		public virtual string AuthenticationType
		{
			get
			{
				return this.m_type;
			}
		}

		// Token: 0x17000BB4 RID: 2996
		// (get) Token: 0x06003E0A RID: 15882 RVA: 0x000D59E0 File Offset: 0x000D3BE0
		public virtual string Name
		{
			get
			{
				return this.m_name;
			}
		}

		// Token: 0x17000BB5 RID: 2997
		// (get) Token: 0x06003E0B RID: 15883 RVA: 0x000D59E8 File Offset: 0x000D3BE8
		public virtual bool IsAuthenticated
		{
			get
			{
				return this.m_name.Length > 0;
			}
		}

		// Token: 0x04001AB1 RID: 6833
		private string m_name;

		// Token: 0x04001AB2 RID: 6834
		private string m_type;
	}
}
