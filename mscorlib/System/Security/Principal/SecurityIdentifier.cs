﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000665 RID: 1637
	[ComVisible(false)]
	[MonoTODO("not implemented")]
	public sealed class SecurityIdentifier : IdentityReference, IComparable<SecurityIdentifier>
	{
		// Token: 0x06003E3B RID: 15931 RVA: 0x000D5F20 File Offset: 0x000D4120
		public SecurityIdentifier(string sddlForm)
		{
			if (sddlForm == null)
			{
				throw new ArgumentNullException("sddlForm");
			}
			this._value = sddlForm.ToUpperInvariant();
		}

		// Token: 0x06003E3C RID: 15932 RVA: 0x000D5F48 File Offset: 0x000D4148
		public SecurityIdentifier(byte[] binaryForm, int offset)
		{
			if (binaryForm == null)
			{
				throw new ArgumentNullException("binaryForm");
			}
			if (offset < 0 || offset > binaryForm.Length - 1)
			{
				throw new ArgumentException("offset");
			}
			throw new NotImplementedException();
		}

		// Token: 0x06003E3D RID: 15933 RVA: 0x000D5F84 File Offset: 0x000D4184
		public SecurityIdentifier(IntPtr binaryForm)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003E3E RID: 15934 RVA: 0x000D5F94 File Offset: 0x000D4194
		public SecurityIdentifier(WellKnownSidType sidType, SecurityIdentifier domainSid)
		{
			switch (sidType)
			{
			case WellKnownSidType.AccountAdministratorSid:
			case WellKnownSidType.AccountGuestSid:
			case WellKnownSidType.AccountKrbtgtSid:
			case WellKnownSidType.AccountDomainAdminsSid:
			case WellKnownSidType.AccountDomainUsersSid:
			case WellKnownSidType.AccountDomainGuestsSid:
			case WellKnownSidType.AccountComputersSid:
			case WellKnownSidType.AccountControllersSid:
			case WellKnownSidType.AccountCertAdminsSid:
			case WellKnownSidType.AccountSchemaAdminsSid:
			case WellKnownSidType.AccountEnterpriseAdminsSid:
			case WellKnownSidType.AccountPolicyAdminsSid:
			case WellKnownSidType.AccountRasAndIasServersSid:
				if (domainSid == null)
				{
					throw new ArgumentNullException("domainSid");
				}
				break;
			default:
				if (sidType == WellKnownSidType.LogonIdsSid)
				{
					throw new ArgumentException("sidType");
				}
				break;
			}
		}

		// Token: 0x17000BC1 RID: 3009
		// (get) Token: 0x06003E40 RID: 15936 RVA: 0x000D6024 File Offset: 0x000D4224
		public SecurityIdentifier AccountDomainSid
		{
			get
			{
				throw new ArgumentNullException("AccountDomainSid");
			}
		}

		// Token: 0x17000BC2 RID: 3010
		// (get) Token: 0x06003E41 RID: 15937 RVA: 0x000D6030 File Offset: 0x000D4230
		public int BinaryLength
		{
			get
			{
				return -1;
			}
		}

		// Token: 0x17000BC3 RID: 3011
		// (get) Token: 0x06003E42 RID: 15938 RVA: 0x000D6034 File Offset: 0x000D4234
		public override string Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x06003E43 RID: 15939 RVA: 0x000D603C File Offset: 0x000D423C
		public int CompareTo(SecurityIdentifier sid)
		{
			return this.Value.CompareTo(sid.Value);
		}

		// Token: 0x06003E44 RID: 15940 RVA: 0x000D6050 File Offset: 0x000D4250
		public override bool Equals(object o)
		{
			return this.Equals(o as SecurityIdentifier);
		}

		// Token: 0x06003E45 RID: 15941 RVA: 0x000D6060 File Offset: 0x000D4260
		public bool Equals(SecurityIdentifier sid)
		{
			return !(sid == null) && sid.Value == this.Value;
		}

		// Token: 0x06003E46 RID: 15942 RVA: 0x000D608C File Offset: 0x000D428C
		public void GetBinaryForm(byte[] binaryForm, int offset)
		{
			if (binaryForm == null)
			{
				throw new ArgumentNullException("binaryForm");
			}
			if (offset < 0 || offset > binaryForm.Length - 1 - this.BinaryLength)
			{
				throw new ArgumentException("offset");
			}
		}

		// Token: 0x06003E47 RID: 15943 RVA: 0x000D60C4 File Offset: 0x000D42C4
		public override int GetHashCode()
		{
			return this.Value.GetHashCode();
		}

		// Token: 0x06003E48 RID: 15944 RVA: 0x000D60D4 File Offset: 0x000D42D4
		public bool IsAccountSid()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003E49 RID: 15945 RVA: 0x000D60DC File Offset: 0x000D42DC
		public bool IsEqualDomainSid(SecurityIdentifier sid)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003E4A RID: 15946 RVA: 0x000D60E4 File Offset: 0x000D42E4
		public override bool IsValidTargetType(Type targetType)
		{
			return targetType == typeof(SecurityIdentifier) || targetType == typeof(NTAccount);
		}

		// Token: 0x06003E4B RID: 15947 RVA: 0x000D610C File Offset: 0x000D430C
		public bool IsWellKnown(WellKnownSidType type)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003E4C RID: 15948 RVA: 0x000D6114 File Offset: 0x000D4314
		public override string ToString()
		{
			return this.Value;
		}

		// Token: 0x06003E4D RID: 15949 RVA: 0x000D611C File Offset: 0x000D431C
		public override IdentityReference Translate(Type targetType)
		{
			if (targetType == typeof(SecurityIdentifier))
			{
				return this;
			}
			return null;
		}

		// Token: 0x06003E4E RID: 15950 RVA: 0x000D6134 File Offset: 0x000D4334
		public static bool operator ==(SecurityIdentifier left, SecurityIdentifier right)
		{
			if (left == null)
			{
				return right == null;
			}
			return right != null && left.Value == right.Value;
		}

		// Token: 0x06003E4F RID: 15951 RVA: 0x000D6168 File Offset: 0x000D4368
		public static bool operator !=(SecurityIdentifier left, SecurityIdentifier right)
		{
			if (left == null)
			{
				return right != null;
			}
			return right == null || left.Value != right.Value;
		}

		// Token: 0x04001ABC RID: 6844
		private string _value;

		// Token: 0x04001ABD RID: 6845
		public static readonly int MaxBinaryLength;

		// Token: 0x04001ABE RID: 6846
		public static readonly int MinBinaryLength;
	}
}
