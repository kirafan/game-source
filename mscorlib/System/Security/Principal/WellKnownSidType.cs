﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000668 RID: 1640
	[ComVisible(false)]
	public enum WellKnownSidType
	{
		// Token: 0x04001AD4 RID: 6868
		NullSid,
		// Token: 0x04001AD5 RID: 6869
		WorldSid,
		// Token: 0x04001AD6 RID: 6870
		LocalSid,
		// Token: 0x04001AD7 RID: 6871
		CreatorOwnerSid,
		// Token: 0x04001AD8 RID: 6872
		CreatorGroupSid,
		// Token: 0x04001AD9 RID: 6873
		CreatorOwnerServerSid,
		// Token: 0x04001ADA RID: 6874
		CreatorGroupServerSid,
		// Token: 0x04001ADB RID: 6875
		NTAuthoritySid,
		// Token: 0x04001ADC RID: 6876
		DialupSid,
		// Token: 0x04001ADD RID: 6877
		NetworkSid,
		// Token: 0x04001ADE RID: 6878
		BatchSid,
		// Token: 0x04001ADF RID: 6879
		InteractiveSid,
		// Token: 0x04001AE0 RID: 6880
		ServiceSid,
		// Token: 0x04001AE1 RID: 6881
		AnonymousSid,
		// Token: 0x04001AE2 RID: 6882
		ProxySid,
		// Token: 0x04001AE3 RID: 6883
		EnterpriseControllersSid,
		// Token: 0x04001AE4 RID: 6884
		SelfSid,
		// Token: 0x04001AE5 RID: 6885
		AuthenticatedUserSid,
		// Token: 0x04001AE6 RID: 6886
		RestrictedCodeSid,
		// Token: 0x04001AE7 RID: 6887
		TerminalServerSid,
		// Token: 0x04001AE8 RID: 6888
		RemoteLogonIdSid,
		// Token: 0x04001AE9 RID: 6889
		LogonIdsSid,
		// Token: 0x04001AEA RID: 6890
		LocalSystemSid,
		// Token: 0x04001AEB RID: 6891
		LocalServiceSid,
		// Token: 0x04001AEC RID: 6892
		NetworkServiceSid,
		// Token: 0x04001AED RID: 6893
		BuiltinDomainSid,
		// Token: 0x04001AEE RID: 6894
		BuiltinAdministratorsSid,
		// Token: 0x04001AEF RID: 6895
		BuiltinUsersSid,
		// Token: 0x04001AF0 RID: 6896
		BuiltinGuestsSid,
		// Token: 0x04001AF1 RID: 6897
		BuiltinPowerUsersSid,
		// Token: 0x04001AF2 RID: 6898
		BuiltinAccountOperatorsSid,
		// Token: 0x04001AF3 RID: 6899
		BuiltinSystemOperatorsSid,
		// Token: 0x04001AF4 RID: 6900
		BuiltinPrintOperatorsSid,
		// Token: 0x04001AF5 RID: 6901
		BuiltinBackupOperatorsSid,
		// Token: 0x04001AF6 RID: 6902
		BuiltinReplicatorSid,
		// Token: 0x04001AF7 RID: 6903
		BuiltinPreWindows2000CompatibleAccessSid,
		// Token: 0x04001AF8 RID: 6904
		BuiltinRemoteDesktopUsersSid,
		// Token: 0x04001AF9 RID: 6905
		BuiltinNetworkConfigurationOperatorsSid,
		// Token: 0x04001AFA RID: 6906
		AccountAdministratorSid,
		// Token: 0x04001AFB RID: 6907
		AccountGuestSid,
		// Token: 0x04001AFC RID: 6908
		AccountKrbtgtSid,
		// Token: 0x04001AFD RID: 6909
		AccountDomainAdminsSid,
		// Token: 0x04001AFE RID: 6910
		AccountDomainUsersSid,
		// Token: 0x04001AFF RID: 6911
		AccountDomainGuestsSid,
		// Token: 0x04001B00 RID: 6912
		AccountComputersSid,
		// Token: 0x04001B01 RID: 6913
		AccountControllersSid,
		// Token: 0x04001B02 RID: 6914
		AccountCertAdminsSid,
		// Token: 0x04001B03 RID: 6915
		AccountSchemaAdminsSid,
		// Token: 0x04001B04 RID: 6916
		AccountEnterpriseAdminsSid,
		// Token: 0x04001B05 RID: 6917
		AccountPolicyAdminsSid,
		// Token: 0x04001B06 RID: 6918
		AccountRasAndIasServersSid,
		// Token: 0x04001B07 RID: 6919
		NtlmAuthenticationSid,
		// Token: 0x04001B08 RID: 6920
		DigestAuthenticationSid,
		// Token: 0x04001B09 RID: 6921
		SChannelAuthenticationSid,
		// Token: 0x04001B0A RID: 6922
		ThisOrganizationSid,
		// Token: 0x04001B0B RID: 6923
		OtherOrganizationSid,
		// Token: 0x04001B0C RID: 6924
		BuiltinIncomingForestTrustBuildersSid,
		// Token: 0x04001B0D RID: 6925
		BuiltinPerformanceMonitoringUsersSid,
		// Token: 0x04001B0E RID: 6926
		BuiltinPerformanceLoggingUsersSid,
		// Token: 0x04001B0F RID: 6927
		BuiltinAuthorizationAccessSid,
		// Token: 0x04001B10 RID: 6928
		WinBuiltinTerminalServerLicenseServersSid,
		// Token: 0x04001B11 RID: 6929
		MaxDefined = 60
	}
}
