﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000667 RID: 1639
	[ComVisible(true)]
	[Serializable]
	public enum TokenImpersonationLevel
	{
		// Token: 0x04001ACE RID: 6862
		Anonymous = 1,
		// Token: 0x04001ACF RID: 6863
		Delegation = 4,
		// Token: 0x04001AD0 RID: 6864
		Identification = 2,
		// Token: 0x04001AD1 RID: 6865
		Impersonation,
		// Token: 0x04001AD2 RID: 6866
		None = 0
	}
}
