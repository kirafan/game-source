﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000669 RID: 1641
	[ComVisible(true)]
	[Serializable]
	public enum WindowsAccountType
	{
		// Token: 0x04001B13 RID: 6931
		Normal,
		// Token: 0x04001B14 RID: 6932
		Guest,
		// Token: 0x04001B15 RID: 6933
		System,
		// Token: 0x04001B16 RID: 6934
		Anonymous
	}
}
