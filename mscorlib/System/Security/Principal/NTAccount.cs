﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000663 RID: 1635
	[ComVisible(false)]
	public sealed class NTAccount : IdentityReference
	{
		// Token: 0x06003E31 RID: 15921 RVA: 0x000D5D5C File Offset: 0x000D3F5C
		public NTAccount(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException(Locale.GetText("Empty"), "name");
			}
			this._value = name.ToUpper();
		}

		// Token: 0x06003E32 RID: 15922 RVA: 0x000D5DAC File Offset: 0x000D3FAC
		public NTAccount(string domainName, string accountName)
		{
			if (accountName == null)
			{
				throw new ArgumentNullException("accountName");
			}
			if (accountName.Length == 0)
			{
				throw new ArgumentException(Locale.GetText("Empty"), "accountName");
			}
			if (domainName == null)
			{
				this._value = domainName.ToUpper();
			}
			else
			{
				this._value = domainName.ToUpper() + "\\" + domainName.ToUpper();
			}
		}

		// Token: 0x17000BC0 RID: 3008
		// (get) Token: 0x06003E33 RID: 15923 RVA: 0x000D5E24 File Offset: 0x000D4024
		public override string Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x06003E34 RID: 15924 RVA: 0x000D5E2C File Offset: 0x000D402C
		public override bool Equals(object o)
		{
			NTAccount ntaccount = o as NTAccount;
			return !(ntaccount == null) && ntaccount.Value == this.Value;
		}

		// Token: 0x06003E35 RID: 15925 RVA: 0x000D5E60 File Offset: 0x000D4060
		public override int GetHashCode()
		{
			return this.Value.GetHashCode();
		}

		// Token: 0x06003E36 RID: 15926 RVA: 0x000D5E70 File Offset: 0x000D4070
		public override bool IsValidTargetType(Type targetType)
		{
			return targetType == typeof(NTAccount) || targetType == typeof(SecurityIdentifier);
		}

		// Token: 0x06003E37 RID: 15927 RVA: 0x000D5E98 File Offset: 0x000D4098
		public override string ToString()
		{
			return this.Value;
		}

		// Token: 0x06003E38 RID: 15928 RVA: 0x000D5EA0 File Offset: 0x000D40A0
		public override IdentityReference Translate(Type targetType)
		{
			if (targetType == typeof(NTAccount))
			{
				return this;
			}
			return null;
		}

		// Token: 0x06003E39 RID: 15929 RVA: 0x000D5EB8 File Offset: 0x000D40B8
		public static bool operator ==(NTAccount left, NTAccount right)
		{
			if (left == null)
			{
				return right == null;
			}
			return right != null && left.Value == right.Value;
		}

		// Token: 0x06003E3A RID: 15930 RVA: 0x000D5EEC File Offset: 0x000D40EC
		public static bool operator !=(NTAccount left, NTAccount right)
		{
			if (left == null)
			{
				return right != null;
			}
			return right == null || left.Value != right.Value;
		}

		// Token: 0x04001AB7 RID: 6839
		private string _value;
	}
}
