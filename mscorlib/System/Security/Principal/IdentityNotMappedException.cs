﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Security.Principal
{
	// Token: 0x0200065E RID: 1630
	[ComVisible(false)]
	[Serializable]
	public sealed class IdentityNotMappedException : SystemException
	{
		// Token: 0x06003E0F RID: 15887 RVA: 0x000D5AC8 File Offset: 0x000D3CC8
		public IdentityNotMappedException() : base(Locale.GetText("Couldn't translate some identities."))
		{
		}

		// Token: 0x06003E10 RID: 15888 RVA: 0x000D5ADC File Offset: 0x000D3CDC
		public IdentityNotMappedException(string message) : base(message)
		{
		}

		// Token: 0x06003E11 RID: 15889 RVA: 0x000D5AE8 File Offset: 0x000D3CE8
		public IdentityNotMappedException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x17000BB7 RID: 2999
		// (get) Token: 0x06003E12 RID: 15890 RVA: 0x000D5AF4 File Offset: 0x000D3CF4
		public IdentityReferenceCollection UnmappedIdentities
		{
			get
			{
				if (this._coll == null)
				{
					this._coll = new IdentityReferenceCollection();
				}
				return this._coll;
			}
		}

		// Token: 0x06003E13 RID: 15891 RVA: 0x000D5B14 File Offset: 0x000D3D14
		[MonoTODO("not implemented")]
		public override void GetObjectData(SerializationInfo serializationInfo, StreamingContext streamingContext)
		{
		}

		// Token: 0x04001AB5 RID: 6837
		private IdentityReferenceCollection _coll;
	}
}
