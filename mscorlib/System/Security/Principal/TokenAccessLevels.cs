﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000666 RID: 1638
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum TokenAccessLevels
	{
		// Token: 0x04001AC0 RID: 6848
		AssignPrimary = 1,
		// Token: 0x04001AC1 RID: 6849
		Duplicate = 2,
		// Token: 0x04001AC2 RID: 6850
		Impersonate = 4,
		// Token: 0x04001AC3 RID: 6851
		Query = 8,
		// Token: 0x04001AC4 RID: 6852
		QuerySource = 16,
		// Token: 0x04001AC5 RID: 6853
		AdjustPrivileges = 32,
		// Token: 0x04001AC6 RID: 6854
		AdjustGroups = 64,
		// Token: 0x04001AC7 RID: 6855
		AdjustDefault = 128,
		// Token: 0x04001AC8 RID: 6856
		AdjustSessionId = 256,
		// Token: 0x04001AC9 RID: 6857
		Read = 131080,
		// Token: 0x04001ACA RID: 6858
		Write = 131296,
		// Token: 0x04001ACB RID: 6859
		AllAccess = 983551,
		// Token: 0x04001ACC RID: 6860
		MaximumAllowed = 33554432
	}
}
