﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000661 RID: 1633
	[ComVisible(true)]
	public interface IIdentity
	{
		// Token: 0x17000BBC RID: 3004
		// (get) Token: 0x06003E2C RID: 15916
		string AuthenticationType { get; }

		// Token: 0x17000BBD RID: 3005
		// (get) Token: 0x06003E2D RID: 15917
		bool IsAuthenticated { get; }

		// Token: 0x17000BBE RID: 3006
		// (get) Token: 0x06003E2E RID: 15918
		string Name { get; }
	}
}
