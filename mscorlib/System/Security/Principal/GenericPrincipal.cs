﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x0200065D RID: 1629
	[ComVisible(true)]
	[Serializable]
	public class GenericPrincipal : IPrincipal
	{
		// Token: 0x06003E0C RID: 15884 RVA: 0x000D59F8 File Offset: 0x000D3BF8
		public GenericPrincipal(IIdentity identity, string[] roles)
		{
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			this.m_identity = identity;
			if (roles != null)
			{
				this.m_roles = new string[roles.Length];
				for (int i = 0; i < roles.Length; i++)
				{
					this.m_roles[i] = roles[i];
				}
			}
		}

		// Token: 0x17000BB6 RID: 2998
		// (get) Token: 0x06003E0D RID: 15885 RVA: 0x000D5A58 File Offset: 0x000D3C58
		public virtual IIdentity Identity
		{
			get
			{
				return this.m_identity;
			}
		}

		// Token: 0x06003E0E RID: 15886 RVA: 0x000D5A60 File Offset: 0x000D3C60
		public virtual bool IsInRole(string role)
		{
			if (this.m_roles == null)
			{
				return false;
			}
			int length = role.Length;
			foreach (string text in this.m_roles)
			{
				if (text != null && length == text.Length && string.Compare(role, 0, text, 0, length, true) == 0)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x04001AB3 RID: 6835
		private IIdentity m_identity;

		// Token: 0x04001AB4 RID: 6836
		private string[] m_roles;
	}
}
