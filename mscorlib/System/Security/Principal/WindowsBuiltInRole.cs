﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x0200066A RID: 1642
	[ComVisible(true)]
	[Serializable]
	public enum WindowsBuiltInRole
	{
		// Token: 0x04001B18 RID: 6936
		Administrator = 544,
		// Token: 0x04001B19 RID: 6937
		User,
		// Token: 0x04001B1A RID: 6938
		Guest,
		// Token: 0x04001B1B RID: 6939
		PowerUser,
		// Token: 0x04001B1C RID: 6940
		AccountOperator,
		// Token: 0x04001B1D RID: 6941
		SystemOperator,
		// Token: 0x04001B1E RID: 6942
		PrintOperator,
		// Token: 0x04001B1F RID: 6943
		BackupOperator,
		// Token: 0x04001B20 RID: 6944
		Replicator
	}
}
