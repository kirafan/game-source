﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Principal
{
	// Token: 0x02000660 RID: 1632
	[ComVisible(false)]
	public abstract class IdentityReference
	{
		// Token: 0x06003E23 RID: 15907 RVA: 0x000D5CEC File Offset: 0x000D3EEC
		internal IdentityReference()
		{
		}

		// Token: 0x17000BBB RID: 3003
		// (get) Token: 0x06003E24 RID: 15908
		public abstract string Value { get; }

		// Token: 0x06003E25 RID: 15909
		public abstract override bool Equals(object o);

		// Token: 0x06003E26 RID: 15910
		public abstract override int GetHashCode();

		// Token: 0x06003E27 RID: 15911
		public abstract bool IsValidTargetType(Type targetType);

		// Token: 0x06003E28 RID: 15912
		public abstract override string ToString();

		// Token: 0x06003E29 RID: 15913
		public abstract IdentityReference Translate(Type targetType);

		// Token: 0x06003E2A RID: 15914 RVA: 0x000D5CF4 File Offset: 0x000D3EF4
		public static bool operator ==(IdentityReference left, IdentityReference right)
		{
			if (left == null)
			{
				return right == null;
			}
			return right != null && left.Value == right.Value;
		}

		// Token: 0x06003E2B RID: 15915 RVA: 0x000D5D28 File Offset: 0x000D3F28
		public static bool operator !=(IdentityReference left, IdentityReference right)
		{
			if (left == null)
			{
				return right != null;
			}
			return right == null || left.Value != right.Value;
		}
	}
}
