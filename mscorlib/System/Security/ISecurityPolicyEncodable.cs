﻿using System;
using System.Runtime.InteropServices;
using System.Security.Policy;

namespace System.Security
{
	// Token: 0x02000539 RID: 1337
	[ComVisible(true)]
	public interface ISecurityPolicyEncodable
	{
		// Token: 0x06003493 RID: 13459
		void FromXml(SecurityElement e, PolicyLevel level);

		// Token: 0x06003494 RID: 13460
		SecurityElement ToXml(PolicyLevel level);
	}
}
