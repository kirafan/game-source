﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Security
{
	// Token: 0x02000553 RID: 1363
	[ComVisible(true)]
	[Serializable]
	public sealed class XmlSyntaxException : SystemException
	{
		// Token: 0x06003591 RID: 13713 RVA: 0x000B13BC File Offset: 0x000AF5BC
		public XmlSyntaxException()
		{
		}

		// Token: 0x06003592 RID: 13714 RVA: 0x000B13C4 File Offset: 0x000AF5C4
		public XmlSyntaxException(int lineNumber) : base(string.Format(Locale.GetText("Invalid syntax on line {0}."), lineNumber))
		{
		}

		// Token: 0x06003593 RID: 13715 RVA: 0x000B13E4 File Offset: 0x000AF5E4
		public XmlSyntaxException(int lineNumber, string message) : base(string.Format(Locale.GetText("Invalid syntax on line {0} - {1}."), lineNumber, message))
		{
		}

		// Token: 0x06003594 RID: 13716 RVA: 0x000B1404 File Offset: 0x000AF604
		public XmlSyntaxException(string message) : base(message)
		{
		}

		// Token: 0x06003595 RID: 13717 RVA: 0x000B1410 File Offset: 0x000AF610
		public XmlSyntaxException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06003596 RID: 13718 RVA: 0x000B141C File Offset: 0x000AF61C
		internal XmlSyntaxException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
