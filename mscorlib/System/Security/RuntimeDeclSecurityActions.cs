﻿using System;

namespace System.Security
{
	// Token: 0x02000549 RID: 1353
	internal struct RuntimeDeclSecurityActions
	{
		// Token: 0x0400166A RID: 5738
		public RuntimeDeclSecurityEntry cas;

		// Token: 0x0400166B RID: 5739
		public RuntimeDeclSecurityEntry noncas;

		// Token: 0x0400166C RID: 5740
		public RuntimeDeclSecurityEntry choice;
	}
}
