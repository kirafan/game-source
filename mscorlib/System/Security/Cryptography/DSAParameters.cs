﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005AB RID: 1451
	[ComVisible(true)]
	[Serializable]
	public struct DSAParameters
	{
		// Token: 0x04001847 RID: 6215
		public int Counter;

		// Token: 0x04001848 RID: 6216
		public byte[] G;

		// Token: 0x04001849 RID: 6217
		public byte[] J;

		// Token: 0x0400184A RID: 6218
		public byte[] P;

		// Token: 0x0400184B RID: 6219
		public byte[] Q;

		// Token: 0x0400184C RID: 6220
		public byte[] Seed;

		// Token: 0x0400184D RID: 6221
		[NonSerialized]
		public byte[] X;

		// Token: 0x0400184E RID: 6222
		public byte[] Y;
	}
}
