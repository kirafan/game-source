﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005B4 RID: 1460
	[ComVisible(true)]
	public class HMACSHA1 : HMAC
	{
		// Token: 0x0600382A RID: 14378 RVA: 0x000B67E8 File Offset: 0x000B49E8
		public HMACSHA1() : this(KeyBuilder.Key(8))
		{
		}

		// Token: 0x0600382B RID: 14379 RVA: 0x000B67F8 File Offset: 0x000B49F8
		public HMACSHA1(byte[] key)
		{
			base.HashName = "SHA1";
			this.HashSizeValue = 160;
			this.Key = key;
		}

		// Token: 0x0600382C RID: 14380 RVA: 0x000B6820 File Offset: 0x000B4A20
		public HMACSHA1(byte[] key, bool useManagedSha1)
		{
			base.HashName = "System.Security.Cryptography.SHA1" + ((!useManagedSha1) ? "CryptoServiceProvider" : "Managed");
			this.HashSizeValue = 160;
			this.Key = key;
		}
	}
}
