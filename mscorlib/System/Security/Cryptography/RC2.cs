﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005C5 RID: 1477
	[ComVisible(true)]
	public abstract class RC2 : SymmetricAlgorithm
	{
		// Token: 0x06003887 RID: 14471 RVA: 0x000B83AC File Offset: 0x000B65AC
		protected RC2()
		{
			this.KeySizeValue = 128;
			this.BlockSizeValue = 64;
			this.FeedbackSizeValue = 8;
			this.LegalKeySizesValue = new KeySizes[1];
			this.LegalKeySizesValue[0] = new KeySizes(40, 128, 8);
			this.LegalBlockSizesValue = new KeySizes[1];
			this.LegalBlockSizesValue[0] = new KeySizes(64, 64, 0);
		}

		// Token: 0x06003888 RID: 14472 RVA: 0x000B8418 File Offset: 0x000B6618
		public new static RC2 Create()
		{
			return RC2.Create("System.Security.Cryptography.RC2");
		}

		// Token: 0x06003889 RID: 14473 RVA: 0x000B8424 File Offset: 0x000B6624
		public new static RC2 Create(string AlgName)
		{
			return (RC2)CryptoConfig.CreateFromName(AlgName);
		}

		// Token: 0x17000ABB RID: 2747
		// (get) Token: 0x0600388A RID: 14474 RVA: 0x000B8434 File Offset: 0x000B6634
		// (set) Token: 0x0600388B RID: 14475 RVA: 0x000B8450 File Offset: 0x000B6650
		public virtual int EffectiveKeySize
		{
			get
			{
				if (this.EffectiveKeySizeValue == 0)
				{
					return this.KeySizeValue;
				}
				return this.EffectiveKeySizeValue;
			}
			set
			{
				this.EffectiveKeySizeValue = value;
			}
		}

		// Token: 0x17000ABC RID: 2748
		// (get) Token: 0x0600388C RID: 14476 RVA: 0x000B845C File Offset: 0x000B665C
		// (set) Token: 0x0600388D RID: 14477 RVA: 0x000B8464 File Offset: 0x000B6664
		public override int KeySize
		{
			get
			{
				return base.KeySize;
			}
			set
			{
				base.KeySize = value;
				this.EffectiveKeySizeValue = value;
			}
		}

		// Token: 0x0400188A RID: 6282
		protected int EffectiveKeySizeValue;
	}
}
