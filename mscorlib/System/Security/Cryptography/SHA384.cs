﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005DF RID: 1503
	[ComVisible(true)]
	public abstract class SHA384 : HashAlgorithm
	{
		// Token: 0x06003963 RID: 14691 RVA: 0x000C4B00 File Offset: 0x000C2D00
		protected SHA384()
		{
			this.HashSizeValue = 384;
		}

		// Token: 0x06003964 RID: 14692 RVA: 0x000C4B14 File Offset: 0x000C2D14
		public new static SHA384 Create()
		{
			return SHA384.Create("System.Security.Cryptography.SHA384");
		}

		// Token: 0x06003965 RID: 14693 RVA: 0x000C4B20 File Offset: 0x000C2D20
		public new static SHA384 Create(string hashName)
		{
			return (SHA384)CryptoConfig.CreateFromName(hashName);
		}
	}
}
