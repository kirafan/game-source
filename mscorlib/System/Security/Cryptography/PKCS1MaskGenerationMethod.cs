﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005C3 RID: 1475
	[ComVisible(true)]
	public class PKCS1MaskGenerationMethod : MaskGenerationMethod
	{
		// Token: 0x0600387E RID: 14462 RVA: 0x000B832C File Offset: 0x000B652C
		public PKCS1MaskGenerationMethod()
		{
			this.hashName = "SHA1";
		}

		// Token: 0x17000ABA RID: 2746
		// (get) Token: 0x0600387F RID: 14463 RVA: 0x000B8340 File Offset: 0x000B6540
		// (set) Token: 0x06003880 RID: 14464 RVA: 0x000B8348 File Offset: 0x000B6548
		public string HashName
		{
			get
			{
				return this.hashName;
			}
			set
			{
				this.hashName = ((value != null) ? value : "SHA1");
			}
		}

		// Token: 0x06003881 RID: 14465 RVA: 0x000B8364 File Offset: 0x000B6564
		public override byte[] GenerateMask(byte[] rgbSeed, int cbReturn)
		{
			HashAlgorithm hash = HashAlgorithm.Create(this.hashName);
			return PKCS1.MGF1(hash, rgbSeed, cbReturn);
		}

		// Token: 0x04001889 RID: 6281
		private string hashName;
	}
}
