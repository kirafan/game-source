﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005A1 RID: 1441
	[ComVisible(true)]
	[Serializable]
	public enum CryptoStreamMode
	{
		// Token: 0x0400181B RID: 6171
		Read,
		// Token: 0x0400181C RID: 6172
		Write
	}
}
