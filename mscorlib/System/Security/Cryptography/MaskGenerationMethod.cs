﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005BE RID: 1470
	[ComVisible(true)]
	public abstract class MaskGenerationMethod
	{
		// Token: 0x0600385C RID: 14428
		[ComVisible(true)]
		public abstract byte[] GenerateMask(byte[] rgbSeed, int cbReturn);
	}
}
