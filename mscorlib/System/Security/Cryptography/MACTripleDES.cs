﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005BD RID: 1469
	[ComVisible(true)]
	public class MACTripleDES : KeyedHashAlgorithm
	{
		// Token: 0x06003850 RID: 14416 RVA: 0x000B6BB0 File Offset: 0x000B4DB0
		public MACTripleDES()
		{
			this.Setup("TripleDES", null);
		}

		// Token: 0x06003851 RID: 14417 RVA: 0x000B6BC4 File Offset: 0x000B4DC4
		public MACTripleDES(byte[] rgbKey)
		{
			if (rgbKey == null)
			{
				throw new ArgumentNullException("rgbKey");
			}
			this.Setup("TripleDES", rgbKey);
		}

		// Token: 0x06003852 RID: 14418 RVA: 0x000B6BEC File Offset: 0x000B4DEC
		public MACTripleDES(string strTripleDES, byte[] rgbKey)
		{
			if (rgbKey == null)
			{
				throw new ArgumentNullException("rgbKey");
			}
			if (strTripleDES == null)
			{
				this.Setup("TripleDES", rgbKey);
			}
			else
			{
				this.Setup(strTripleDES, rgbKey);
			}
		}

		// Token: 0x06003853 RID: 14419 RVA: 0x000B6C30 File Offset: 0x000B4E30
		private void Setup(string strTripleDES, byte[] rgbKey)
		{
			this.tdes = TripleDES.Create(strTripleDES);
			this.tdes.Padding = PaddingMode.Zeros;
			if (rgbKey != null)
			{
				this.tdes.Key = rgbKey;
			}
			this.HashSizeValue = this.tdes.BlockSize;
			this.Key = this.tdes.Key;
			this.mac = new MACAlgorithm(this.tdes);
			this.m_disposed = false;
		}

		// Token: 0x06003854 RID: 14420 RVA: 0x000B6CA4 File Offset: 0x000B4EA4
		~MACTripleDES()
		{
			this.Dispose(false);
		}

		// Token: 0x17000AB6 RID: 2742
		// (get) Token: 0x06003855 RID: 14421 RVA: 0x000B6CE0 File Offset: 0x000B4EE0
		// (set) Token: 0x06003856 RID: 14422 RVA: 0x000B6CF0 File Offset: 0x000B4EF0
		[ComVisible(false)]
		public PaddingMode Padding
		{
			get
			{
				return this.tdes.Padding;
			}
			set
			{
				this.tdes.Padding = value;
			}
		}

		// Token: 0x06003857 RID: 14423 RVA: 0x000B6D00 File Offset: 0x000B4F00
		protected override void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				if (this.KeyValue != null)
				{
					Array.Clear(this.KeyValue, 0, this.KeyValue.Length);
				}
				if (this.tdes != null)
				{
					this.tdes.Clear();
				}
				if (disposing)
				{
					this.KeyValue = null;
					this.tdes = null;
				}
				base.Dispose(disposing);
				this.m_disposed = true;
			}
		}

		// Token: 0x06003858 RID: 14424 RVA: 0x000B6D70 File Offset: 0x000B4F70
		public override void Initialize()
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException("MACTripleDES");
			}
			this.State = 0;
			this.mac.Initialize(this.KeyValue);
		}

		// Token: 0x06003859 RID: 14425 RVA: 0x000B6DAC File Offset: 0x000B4FAC
		protected override void HashCore(byte[] rgbData, int ibStart, int cbSize)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException("MACTripleDES");
			}
			if (this.State == 0)
			{
				this.Initialize();
				this.State = 1;
			}
			this.mac.Core(rgbData, ibStart, cbSize);
		}

		// Token: 0x0600385A RID: 14426 RVA: 0x000B6DF8 File Offset: 0x000B4FF8
		protected override byte[] HashFinal()
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException("MACTripleDES");
			}
			this.State = 0;
			return this.mac.Final();
		}

		// Token: 0x0400186E RID: 6254
		private TripleDES tdes;

		// Token: 0x0400186F RID: 6255
		private MACAlgorithm mac;

		// Token: 0x04001870 RID: 6256
		private bool m_disposed;
	}
}
