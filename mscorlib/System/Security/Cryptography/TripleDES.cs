﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005E9 RID: 1513
	[ComVisible(true)]
	public abstract class TripleDES : SymmetricAlgorithm
	{
		// Token: 0x060039C2 RID: 14786 RVA: 0x000C6234 File Offset: 0x000C4434
		protected TripleDES()
		{
			this.KeySizeValue = 192;
			this.BlockSizeValue = 64;
			this.FeedbackSizeValue = 8;
			this.LegalKeySizesValue = new KeySizes[1];
			this.LegalKeySizesValue[0] = new KeySizes(128, 192, 64);
			this.LegalBlockSizesValue = new KeySizes[1];
			this.LegalBlockSizesValue[0] = new KeySizes(64, 64, 0);
		}

		// Token: 0x17000AE6 RID: 2790
		// (get) Token: 0x060039C3 RID: 14787 RVA: 0x000C62A4 File Offset: 0x000C44A4
		// (set) Token: 0x060039C4 RID: 14788 RVA: 0x000C62F0 File Offset: 0x000C44F0
		public override byte[] Key
		{
			get
			{
				if (this.KeyValue == null)
				{
					this.GenerateKey();
					while (TripleDES.IsWeakKey(this.KeyValue))
					{
						this.GenerateKey();
					}
				}
				return (byte[])this.KeyValue.Clone();
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Key");
				}
				if (TripleDES.IsWeakKey(value))
				{
					throw new CryptographicException(Locale.GetText("Weak Key"));
				}
				this.KeyValue = (byte[])value.Clone();
			}
		}

		// Token: 0x060039C5 RID: 14789 RVA: 0x000C6330 File Offset: 0x000C4530
		public static bool IsWeakKey(byte[] rgbKey)
		{
			if (rgbKey == null)
			{
				throw new CryptographicException(Locale.GetText("Null Key"));
			}
			if (rgbKey.Length == 16)
			{
				for (int i = 0; i < 8; i++)
				{
					if (rgbKey[i] != rgbKey[i + 8])
					{
						return false;
					}
				}
			}
			else
			{
				if (rgbKey.Length != 24)
				{
					throw new CryptographicException(Locale.GetText("Wrong Key Length"));
				}
				bool flag = true;
				for (int j = 0; j < 8; j++)
				{
					if (rgbKey[j] != rgbKey[j + 8])
					{
						flag = false;
						break;
					}
				}
				if (!flag)
				{
					for (int k = 8; k < 16; k++)
					{
						if (rgbKey[k] != rgbKey[k + 8])
						{
							return false;
						}
					}
				}
			}
			return true;
		}

		// Token: 0x060039C6 RID: 14790 RVA: 0x000C63F4 File Offset: 0x000C45F4
		public new static TripleDES Create()
		{
			return TripleDES.Create("System.Security.Cryptography.TripleDES");
		}

		// Token: 0x060039C7 RID: 14791 RVA: 0x000C6400 File Offset: 0x000C4600
		public new static TripleDES Create(string str)
		{
			return (TripleDES)CryptoConfig.CreateFromName(str);
		}
	}
}
