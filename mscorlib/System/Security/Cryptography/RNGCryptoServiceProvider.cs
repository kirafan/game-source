﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Security.Cryptography
{
	// Token: 0x020005CF RID: 1487
	[ComVisible(true)]
	public sealed class RNGCryptoServiceProvider : RandomNumberGenerator
	{
		// Token: 0x060038DF RID: 14559 RVA: 0x000C26F4 File Offset: 0x000C08F4
		public RNGCryptoServiceProvider()
		{
			this._handle = RNGCryptoServiceProvider.RngInitialize(null);
			this.Check();
		}

		// Token: 0x060038E0 RID: 14560 RVA: 0x000C2710 File Offset: 0x000C0910
		public RNGCryptoServiceProvider(byte[] rgb)
		{
			this._handle = RNGCryptoServiceProvider.RngInitialize(rgb);
			this.Check();
		}

		// Token: 0x060038E1 RID: 14561 RVA: 0x000C272C File Offset: 0x000C092C
		public RNGCryptoServiceProvider(CspParameters cspParams)
		{
			this._handle = RNGCryptoServiceProvider.RngInitialize(null);
			this.Check();
		}

		// Token: 0x060038E2 RID: 14562 RVA: 0x000C2748 File Offset: 0x000C0948
		public RNGCryptoServiceProvider(string str)
		{
			if (str == null)
			{
				this._handle = RNGCryptoServiceProvider.RngInitialize(null);
			}
			else
			{
				this._handle = RNGCryptoServiceProvider.RngInitialize(Encoding.UTF8.GetBytes(str));
			}
			this.Check();
		}

		// Token: 0x060038E3 RID: 14563 RVA: 0x000C2790 File Offset: 0x000C0990
		static RNGCryptoServiceProvider()
		{
			if (RNGCryptoServiceProvider.RngOpen())
			{
				RNGCryptoServiceProvider._lock = new object();
			}
		}

		// Token: 0x060038E4 RID: 14564 RVA: 0x000C27A8 File Offset: 0x000C09A8
		private void Check()
		{
			if (this._handle == IntPtr.Zero)
			{
				throw new CryptographicException(Locale.GetText("Couldn't access random source."));
			}
		}

		// Token: 0x060038E5 RID: 14565
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool RngOpen();

		// Token: 0x060038E6 RID: 14566
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr RngInitialize(byte[] seed);

		// Token: 0x060038E7 RID: 14567
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr RngGetBytes(IntPtr handle, byte[] data);

		// Token: 0x060038E8 RID: 14568
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void RngClose(IntPtr handle);

		// Token: 0x060038E9 RID: 14569 RVA: 0x000C27D0 File Offset: 0x000C09D0
		public override void GetBytes(byte[] data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			if (RNGCryptoServiceProvider._lock == null)
			{
				this._handle = RNGCryptoServiceProvider.RngGetBytes(this._handle, data);
			}
			else
			{
				object @lock = RNGCryptoServiceProvider._lock;
				lock (@lock)
				{
					this._handle = RNGCryptoServiceProvider.RngGetBytes(this._handle, data);
				}
			}
			this.Check();
		}

		// Token: 0x060038EA RID: 14570 RVA: 0x000C285C File Offset: 0x000C0A5C
		public override void GetNonZeroBytes(byte[] data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			byte[] array = new byte[data.Length * 2];
			int i = 0;
			while (i < data.Length)
			{
				this._handle = RNGCryptoServiceProvider.RngGetBytes(this._handle, array);
				this.Check();
				for (int j = 0; j < array.Length; j++)
				{
					if (i == data.Length)
					{
						break;
					}
					if (array[j] != 0)
					{
						data[i++] = array[j];
					}
				}
			}
		}

		// Token: 0x060038EB RID: 14571 RVA: 0x000C28E4 File Offset: 0x000C0AE4
		~RNGCryptoServiceProvider()
		{
			if (this._handle != IntPtr.Zero)
			{
				RNGCryptoServiceProvider.RngClose(this._handle);
				this._handle = IntPtr.Zero;
			}
		}

		// Token: 0x040018B1 RID: 6321
		private static object _lock;

		// Token: 0x040018B2 RID: 6322
		private IntPtr _handle;
	}
}
