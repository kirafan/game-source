﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x02000597 RID: 1431
	[ComVisible(true)]
	public abstract class AsymmetricSignatureDeformatter
	{
		// Token: 0x0600374A RID: 14154
		public abstract void SetHashAlgorithm(string strName);

		// Token: 0x0600374B RID: 14155
		public abstract void SetKey(AsymmetricAlgorithm key);

		// Token: 0x0600374C RID: 14156
		public abstract bool VerifySignature(byte[] rgbHash, byte[] rgbSignature);

		// Token: 0x0600374D RID: 14157 RVA: 0x000B2B9C File Offset: 0x000B0D9C
		public virtual bool VerifySignature(HashAlgorithm hash, byte[] rgbSignature)
		{
			if (hash == null)
			{
				throw new ArgumentNullException("hash");
			}
			this.SetHashAlgorithm(hash.ToString());
			return this.VerifySignature(hash.Hash, rgbSignature);
		}
	}
}
