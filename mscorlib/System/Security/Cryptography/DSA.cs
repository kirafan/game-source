﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using Mono.Security;

namespace System.Security.Cryptography
{
	// Token: 0x020005A9 RID: 1449
	[ComVisible(true)]
	public abstract class DSA : AsymmetricAlgorithm
	{
		// Token: 0x060037C4 RID: 14276 RVA: 0x000B5220 File Offset: 0x000B3420
		public new static DSA Create()
		{
			return DSA.Create("System.Security.Cryptography.DSA");
		}

		// Token: 0x060037C5 RID: 14277 RVA: 0x000B522C File Offset: 0x000B342C
		public new static DSA Create(string algName)
		{
			return (DSA)CryptoConfig.CreateFromName(algName);
		}

		// Token: 0x060037C6 RID: 14278
		public abstract byte[] CreateSignature(byte[] rgbHash);

		// Token: 0x060037C7 RID: 14279
		public abstract DSAParameters ExportParameters(bool includePrivateParameters);

		// Token: 0x060037C8 RID: 14280 RVA: 0x000B523C File Offset: 0x000B343C
		internal void ZeroizePrivateKey(DSAParameters parameters)
		{
			if (parameters.X != null)
			{
				Array.Clear(parameters.X, 0, parameters.X.Length);
			}
		}

		// Token: 0x060037C9 RID: 14281 RVA: 0x000B526C File Offset: 0x000B346C
		public override void FromXmlString(string xmlString)
		{
			if (xmlString == null)
			{
				throw new ArgumentNullException("xmlString");
			}
			DSAParameters parameters = default(DSAParameters);
			try
			{
				parameters.P = AsymmetricAlgorithm.GetNamedParam(xmlString, "P");
				parameters.Q = AsymmetricAlgorithm.GetNamedParam(xmlString, "Q");
				parameters.G = AsymmetricAlgorithm.GetNamedParam(xmlString, "G");
				parameters.J = AsymmetricAlgorithm.GetNamedParam(xmlString, "J");
				parameters.Y = AsymmetricAlgorithm.GetNamedParam(xmlString, "Y");
				parameters.X = AsymmetricAlgorithm.GetNamedParam(xmlString, "X");
				parameters.Seed = AsymmetricAlgorithm.GetNamedParam(xmlString, "Seed");
				byte[] namedParam = AsymmetricAlgorithm.GetNamedParam(xmlString, "PgenCounter");
				if (namedParam != null)
				{
					byte[] array = new byte[4];
					Buffer.BlockCopy(namedParam, 0, array, 0, namedParam.Length);
					parameters.Counter = BitConverterLE.ToInt32(array, 0);
				}
				this.ImportParameters(parameters);
			}
			catch
			{
				this.ZeroizePrivateKey(parameters);
				throw;
			}
			finally
			{
				this.ZeroizePrivateKey(parameters);
			}
		}

		// Token: 0x060037CA RID: 14282
		public abstract void ImportParameters(DSAParameters parameters);

		// Token: 0x060037CB RID: 14283 RVA: 0x000B539C File Offset: 0x000B359C
		public override string ToXmlString(bool includePrivateParameters)
		{
			StringBuilder stringBuilder = new StringBuilder();
			DSAParameters parameters = this.ExportParameters(includePrivateParameters);
			try
			{
				stringBuilder.Append("<DSAKeyValue>");
				stringBuilder.Append("<P>");
				stringBuilder.Append(Convert.ToBase64String(parameters.P));
				stringBuilder.Append("</P>");
				stringBuilder.Append("<Q>");
				stringBuilder.Append(Convert.ToBase64String(parameters.Q));
				stringBuilder.Append("</Q>");
				stringBuilder.Append("<G>");
				stringBuilder.Append(Convert.ToBase64String(parameters.G));
				stringBuilder.Append("</G>");
				stringBuilder.Append("<Y>");
				stringBuilder.Append(Convert.ToBase64String(parameters.Y));
				stringBuilder.Append("</Y>");
				if (parameters.J != null)
				{
					stringBuilder.Append("<J>");
					stringBuilder.Append(Convert.ToBase64String(parameters.J));
					stringBuilder.Append("</J>");
				}
				if (parameters.Seed != null)
				{
					stringBuilder.Append("<Seed>");
					stringBuilder.Append(Convert.ToBase64String(parameters.Seed));
					stringBuilder.Append("</Seed>");
					stringBuilder.Append("<PgenCounter>");
					if (parameters.Counter != 0)
					{
						byte[] bytes = BitConverterLE.GetBytes(parameters.Counter);
						int num = bytes.Length;
						while (bytes[num - 1] == 0)
						{
							num--;
						}
						stringBuilder.Append(Convert.ToBase64String(bytes, 0, num));
					}
					else
					{
						stringBuilder.Append("AA==");
					}
					stringBuilder.Append("</PgenCounter>");
				}
				if (parameters.X != null)
				{
					stringBuilder.Append("<X>");
					stringBuilder.Append(Convert.ToBase64String(parameters.X));
					stringBuilder.Append("</X>");
				}
				else if (includePrivateParameters)
				{
					throw new ArgumentNullException("X");
				}
				stringBuilder.Append("</DSAKeyValue>");
			}
			catch
			{
				this.ZeroizePrivateKey(parameters);
				throw;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060037CC RID: 14284
		public abstract bool VerifySignature(byte[] rgbHash, byte[] rgbSignature);
	}
}
