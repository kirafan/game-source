﻿using System;
using System.Runtime.InteropServices;
using System.Security.AccessControl;

namespace System.Security.Cryptography
{
	// Token: 0x020005A3 RID: 1443
	[ComVisible(true)]
	public sealed class CspParameters
	{
		// Token: 0x0600379C RID: 14236 RVA: 0x000B45A8 File Offset: 0x000B27A8
		public CspParameters() : this(1)
		{
		}

		// Token: 0x0600379D RID: 14237 RVA: 0x000B45B4 File Offset: 0x000B27B4
		public CspParameters(int dwTypeIn) : this(dwTypeIn, null)
		{
		}

		// Token: 0x0600379E RID: 14238 RVA: 0x000B45C0 File Offset: 0x000B27C0
		public CspParameters(int dwTypeIn, string strProviderNameIn) : this(dwTypeIn, null, null)
		{
		}

		// Token: 0x0600379F RID: 14239 RVA: 0x000B45CC File Offset: 0x000B27CC
		public CspParameters(int dwTypeIn, string strProviderNameIn, string strContainerNameIn)
		{
			this.ProviderType = dwTypeIn;
			this.ProviderName = strProviderNameIn;
			this.KeyContainerName = strContainerNameIn;
			this.KeyNumber = -1;
		}

		// Token: 0x060037A0 RID: 14240 RVA: 0x000B45FC File Offset: 0x000B27FC
		public CspParameters(int providerType, string providerName, string keyContainerName, CryptoKeySecurity cryptoKeySecurity, IntPtr parentWindowHandle) : this(providerType, providerName, keyContainerName)
		{
			if (cryptoKeySecurity != null)
			{
				this.CryptoKeySecurity = cryptoKeySecurity;
			}
			this._windowHandle = parentWindowHandle;
		}

		// Token: 0x060037A1 RID: 14241 RVA: 0x000B462C File Offset: 0x000B282C
		public CspParameters(int providerType, string providerName, string keyContainerName, CryptoKeySecurity cryptoKeySecurity, SecureString keyPassword) : this(providerType, providerName, keyContainerName)
		{
			if (cryptoKeySecurity != null)
			{
				this.CryptoKeySecurity = cryptoKeySecurity;
			}
			this._password = keyPassword;
		}

		// Token: 0x17000A91 RID: 2705
		// (get) Token: 0x060037A2 RID: 14242 RVA: 0x000B465C File Offset: 0x000B285C
		// (set) Token: 0x060037A3 RID: 14243 RVA: 0x000B4664 File Offset: 0x000B2864
		public CspProviderFlags Flags
		{
			get
			{
				return this._Flags;
			}
			set
			{
				this._Flags = value;
			}
		}

		// Token: 0x17000A92 RID: 2706
		// (get) Token: 0x060037A4 RID: 14244 RVA: 0x000B4670 File Offset: 0x000B2870
		// (set) Token: 0x060037A5 RID: 14245 RVA: 0x000B4678 File Offset: 0x000B2878
		[MonoTODO("access control isn't implemented")]
		public CryptoKeySecurity CryptoKeySecurity
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A93 RID: 2707
		// (get) Token: 0x060037A6 RID: 14246 RVA: 0x000B4680 File Offset: 0x000B2880
		// (set) Token: 0x060037A7 RID: 14247 RVA: 0x000B4688 File Offset: 0x000B2888
		public SecureString KeyPassword
		{
			get
			{
				return this._password;
			}
			set
			{
				this._password = value;
			}
		}

		// Token: 0x17000A94 RID: 2708
		// (get) Token: 0x060037A8 RID: 14248 RVA: 0x000B4694 File Offset: 0x000B2894
		// (set) Token: 0x060037A9 RID: 14249 RVA: 0x000B469C File Offset: 0x000B289C
		public IntPtr ParentWindowHandle
		{
			get
			{
				return this._windowHandle;
			}
			set
			{
				this._windowHandle = value;
			}
		}

		// Token: 0x0400181F RID: 6175
		private CspProviderFlags _Flags;

		// Token: 0x04001820 RID: 6176
		public string KeyContainerName;

		// Token: 0x04001821 RID: 6177
		public int KeyNumber;

		// Token: 0x04001822 RID: 6178
		public string ProviderName;

		// Token: 0x04001823 RID: 6179
		public int ProviderType;

		// Token: 0x04001824 RID: 6180
		private SecureString _password;

		// Token: 0x04001825 RID: 6181
		private IntPtr _windowHandle;
	}
}
