﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005AC RID: 1452
	[ComVisible(true)]
	public class DSASignatureDeformatter : AsymmetricSignatureDeformatter
	{
		// Token: 0x060037EA RID: 14314 RVA: 0x000B5A88 File Offset: 0x000B3C88
		public DSASignatureDeformatter()
		{
		}

		// Token: 0x060037EB RID: 14315 RVA: 0x000B5A90 File Offset: 0x000B3C90
		public DSASignatureDeformatter(AsymmetricAlgorithm key)
		{
			this.SetKey(key);
		}

		// Token: 0x060037EC RID: 14316 RVA: 0x000B5AA0 File Offset: 0x000B3CA0
		public override void SetHashAlgorithm(string strName)
		{
			if (strName == null)
			{
				throw new ArgumentNullException("strName");
			}
			try
			{
				SHA1.Create(strName);
			}
			catch (InvalidCastException)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("DSA requires SHA1"));
			}
		}

		// Token: 0x060037ED RID: 14317 RVA: 0x000B5AFC File Offset: 0x000B3CFC
		public override void SetKey(AsymmetricAlgorithm key)
		{
			if (key != null)
			{
				this.dsa = (DSA)key;
				return;
			}
			throw new ArgumentNullException("key");
		}

		// Token: 0x060037EE RID: 14318 RVA: 0x000B5B2C File Offset: 0x000B3D2C
		public override bool VerifySignature(byte[] rgbHash, byte[] rgbSignature)
		{
			if (this.dsa == null)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("missing key"));
			}
			return this.dsa.VerifySignature(rgbHash, rgbSignature);
		}

		// Token: 0x0400184F RID: 6223
		private DSA dsa;
	}
}
