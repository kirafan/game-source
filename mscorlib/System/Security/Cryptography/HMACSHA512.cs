﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005B7 RID: 1463
	[ComVisible(true)]
	public class HMACSHA512 : HMAC
	{
		// Token: 0x06003834 RID: 14388 RVA: 0x000B6954 File Offset: 0x000B4B54
		public HMACSHA512() : this(KeyBuilder.Key(8))
		{
			this.ProduceLegacyHmacValues = HMACSHA512.legacy_mode;
		}

		// Token: 0x06003835 RID: 14389 RVA: 0x000B6970 File Offset: 0x000B4B70
		public HMACSHA512(byte[] key)
		{
			this.ProduceLegacyHmacValues = HMACSHA512.legacy_mode;
			base.HashName = "SHA512";
			this.HashSizeValue = 512;
			this.Key = key;
		}

		// Token: 0x17000AAC RID: 2732
		// (get) Token: 0x06003837 RID: 14391 RVA: 0x000B69C8 File Offset: 0x000B4BC8
		// (set) Token: 0x06003838 RID: 14392 RVA: 0x000B69D0 File Offset: 0x000B4BD0
		public bool ProduceLegacyHmacValues
		{
			get
			{
				return this.legacy;
			}
			set
			{
				this.legacy = value;
				base.BlockSizeValue = ((!this.legacy) ? 128 : 64);
			}
		}

		// Token: 0x04001865 RID: 6245
		private static bool legacy_mode = Environment.GetEnvironmentVariable("legacyHMACMode") == "1";

		// Token: 0x04001866 RID: 6246
		private bool legacy;
	}
}
