﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005D7 RID: 1495
	[ComVisible(true)]
	public class RSAPKCS1SignatureDeformatter : AsymmetricSignatureDeformatter
	{
		// Token: 0x06003939 RID: 14649 RVA: 0x000C3730 File Offset: 0x000C1930
		public RSAPKCS1SignatureDeformatter()
		{
		}

		// Token: 0x0600393A RID: 14650 RVA: 0x000C3738 File Offset: 0x000C1938
		public RSAPKCS1SignatureDeformatter(AsymmetricAlgorithm key)
		{
			this.SetKey(key);
		}

		// Token: 0x0600393B RID: 14651 RVA: 0x000C3748 File Offset: 0x000C1948
		public override void SetHashAlgorithm(string strName)
		{
			if (strName == null)
			{
				throw new ArgumentNullException("strName");
			}
			this.hashName = strName;
		}

		// Token: 0x0600393C RID: 14652 RVA: 0x000C3764 File Offset: 0x000C1964
		public override void SetKey(AsymmetricAlgorithm key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.rsa = (RSA)key;
		}

		// Token: 0x0600393D RID: 14653 RVA: 0x000C3784 File Offset: 0x000C1984
		public override bool VerifySignature(byte[] rgbHash, byte[] rgbSignature)
		{
			if (this.rsa == null)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("No public key available."));
			}
			if (this.hashName == null)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("Missing hash algorithm."));
			}
			if (rgbHash == null)
			{
				throw new ArgumentNullException("rgbHash");
			}
			if (rgbSignature == null)
			{
				throw new ArgumentNullException("rgbSignature");
			}
			return PKCS1.Verify_v15(this.rsa, HashAlgorithm.Create(this.hashName), rgbHash, rgbSignature);
		}

		// Token: 0x040018CC RID: 6348
		private RSA rsa;

		// Token: 0x040018CD RID: 6349
		private string hashName;
	}
}
