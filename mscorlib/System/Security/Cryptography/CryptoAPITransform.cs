﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Cryptography
{
	// Token: 0x0200059B RID: 1435
	[ComVisible(true)]
	public sealed class CryptoAPITransform : IDisposable, ICryptoTransform
	{
		// Token: 0x06003754 RID: 14164 RVA: 0x000B2C50 File Offset: 0x000B0E50
		internal CryptoAPITransform()
		{
			this.m_disposed = false;
		}

		// Token: 0x06003755 RID: 14165 RVA: 0x000B2C60 File Offset: 0x000B0E60
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x17000A7A RID: 2682
		// (get) Token: 0x06003756 RID: 14166 RVA: 0x000B2C70 File Offset: 0x000B0E70
		public bool CanReuseTransform
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000A7B RID: 2683
		// (get) Token: 0x06003757 RID: 14167 RVA: 0x000B2C74 File Offset: 0x000B0E74
		public bool CanTransformMultipleBlocks
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000A7C RID: 2684
		// (get) Token: 0x06003758 RID: 14168 RVA: 0x000B2C78 File Offset: 0x000B0E78
		public int InputBlockSize
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000A7D RID: 2685
		// (get) Token: 0x06003759 RID: 14169 RVA: 0x000B2C7C File Offset: 0x000B0E7C
		public IntPtr KeyHandle
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
			get
			{
				return IntPtr.Zero;
			}
		}

		// Token: 0x17000A7E RID: 2686
		// (get) Token: 0x0600375A RID: 14170 RVA: 0x000B2C84 File Offset: 0x000B0E84
		public int OutputBlockSize
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x0600375B RID: 14171 RVA: 0x000B2C88 File Offset: 0x000B0E88
		public void Clear()
		{
			this.Dispose(false);
		}

		// Token: 0x0600375C RID: 14172 RVA: 0x000B2C94 File Offset: 0x000B0E94
		private void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				if (disposing)
				{
				}
				this.m_disposed = true;
			}
		}

		// Token: 0x0600375D RID: 14173 RVA: 0x000B2CB0 File Offset: 0x000B0EB0
		public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
		{
			return 0;
		}

		// Token: 0x0600375E RID: 14174 RVA: 0x000B2CB4 File Offset: 0x000B0EB4
		public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			return null;
		}

		// Token: 0x0600375F RID: 14175 RVA: 0x000B2CB8 File Offset: 0x000B0EB8
		[ComVisible(false)]
		public void Reset()
		{
		}

		// Token: 0x0400177A RID: 6010
		private bool m_disposed;
	}
}
