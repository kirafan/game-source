﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005B9 RID: 1465
	[ComVisible(true)]
	public interface ICspAsymmetricAlgorithm
	{
		// Token: 0x0600383F RID: 14399
		byte[] ExportCspBlob(bool includePrivateParameters);

		// Token: 0x06003840 RID: 14400
		void ImportCspBlob(byte[] rawData);

		// Token: 0x17000AB1 RID: 2737
		// (get) Token: 0x06003841 RID: 14401
		CspKeyContainerInfo CspKeyContainerInfo { get; }
	}
}
