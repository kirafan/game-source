﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005D2 RID: 1490
	[ComVisible(true)]
	public class RSAOAEPKeyExchangeDeformatter : AsymmetricKeyExchangeDeformatter
	{
		// Token: 0x06003918 RID: 14616 RVA: 0x000C3484 File Offset: 0x000C1684
		public RSAOAEPKeyExchangeDeformatter()
		{
		}

		// Token: 0x06003919 RID: 14617 RVA: 0x000C348C File Offset: 0x000C168C
		public RSAOAEPKeyExchangeDeformatter(AsymmetricAlgorithm key)
		{
			this.SetKey(key);
		}

		// Token: 0x17000ACD RID: 2765
		// (get) Token: 0x0600391A RID: 14618 RVA: 0x000C349C File Offset: 0x000C169C
		// (set) Token: 0x0600391B RID: 14619 RVA: 0x000C34A0 File Offset: 0x000C16A0
		public override string Parameters
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x0600391C RID: 14620 RVA: 0x000C34A4 File Offset: 0x000C16A4
		public override byte[] DecryptKeyExchange(byte[] rgbData)
		{
			if (this.rsa == null)
			{
				string text = Locale.GetText("No RSA key specified");
				throw new CryptographicUnexpectedOperationException(text);
			}
			SHA1 hash = SHA1.Create();
			byte[] array = PKCS1.Decrypt_OAEP(this.rsa, hash, rgbData);
			if (array != null)
			{
				return array;
			}
			throw new CryptographicException(Locale.GetText("OAEP decoding error."));
		}

		// Token: 0x0600391D RID: 14621 RVA: 0x000C34FC File Offset: 0x000C16FC
		public override void SetKey(AsymmetricAlgorithm key)
		{
			this.rsa = (RSA)key;
		}

		// Token: 0x040018BC RID: 6332
		private RSA rsa;
	}
}
