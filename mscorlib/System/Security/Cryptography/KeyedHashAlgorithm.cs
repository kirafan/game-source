﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005BA RID: 1466
	[ComVisible(true)]
	public abstract class KeyedHashAlgorithm : HashAlgorithm
	{
		// Token: 0x06003843 RID: 14403 RVA: 0x000B6A0C File Offset: 0x000B4C0C
		~KeyedHashAlgorithm()
		{
			this.Dispose(false);
		}

		// Token: 0x17000AB2 RID: 2738
		// (get) Token: 0x06003844 RID: 14404 RVA: 0x000B6A48 File Offset: 0x000B4C48
		// (set) Token: 0x06003845 RID: 14405 RVA: 0x000B6A5C File Offset: 0x000B4C5C
		public virtual byte[] Key
		{
			get
			{
				return (byte[])this.KeyValue.Clone();
			}
			set
			{
				if (this.State != 0)
				{
					throw new CryptographicException(Locale.GetText("Key can't be changed at this state."));
				}
				this.ZeroizeKey();
				this.KeyValue = (byte[])value.Clone();
			}
		}

		// Token: 0x06003846 RID: 14406 RVA: 0x000B6A9C File Offset: 0x000B4C9C
		protected override void Dispose(bool disposing)
		{
			this.ZeroizeKey();
			base.Dispose(disposing);
		}

		// Token: 0x06003847 RID: 14407 RVA: 0x000B6AAC File Offset: 0x000B4CAC
		private void ZeroizeKey()
		{
			if (this.KeyValue != null)
			{
				Array.Clear(this.KeyValue, 0, this.KeyValue.Length);
			}
		}

		// Token: 0x06003848 RID: 14408 RVA: 0x000B6AD0 File Offset: 0x000B4CD0
		public new static KeyedHashAlgorithm Create()
		{
			return KeyedHashAlgorithm.Create("System.Security.Cryptography.KeyedHashAlgorithm");
		}

		// Token: 0x06003849 RID: 14409 RVA: 0x000B6ADC File Offset: 0x000B4CDC
		public new static KeyedHashAlgorithm Create(string algName)
		{
			return (KeyedHashAlgorithm)CryptoConfig.CreateFromName(algName);
		}

		// Token: 0x04001867 RID: 6247
		protected byte[] KeyValue;
	}
}
