﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005E8 RID: 1512
	[ComVisible(true)]
	public class ToBase64Transform : IDisposable, ICryptoTransform
	{
		// Token: 0x060039B6 RID: 14774 RVA: 0x000C5EB0 File Offset: 0x000C40B0
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060039B7 RID: 14775 RVA: 0x000C5EC0 File Offset: 0x000C40C0
		~ToBase64Transform()
		{
			this.Dispose(false);
		}

		// Token: 0x17000AE2 RID: 2786
		// (get) Token: 0x060039B8 RID: 14776 RVA: 0x000C5EFC File Offset: 0x000C40FC
		public bool CanTransformMultipleBlocks
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000AE3 RID: 2787
		// (get) Token: 0x060039B9 RID: 14777 RVA: 0x000C5F00 File Offset: 0x000C4100
		public virtual bool CanReuseTransform
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000AE4 RID: 2788
		// (get) Token: 0x060039BA RID: 14778 RVA: 0x000C5F04 File Offset: 0x000C4104
		public int InputBlockSize
		{
			get
			{
				return 3;
			}
		}

		// Token: 0x17000AE5 RID: 2789
		// (get) Token: 0x060039BB RID: 14779 RVA: 0x000C5F08 File Offset: 0x000C4108
		public int OutputBlockSize
		{
			get
			{
				return 4;
			}
		}

		// Token: 0x060039BC RID: 14780 RVA: 0x000C5F0C File Offset: 0x000C410C
		public void Clear()
		{
			this.Dispose(true);
		}

		// Token: 0x060039BD RID: 14781 RVA: 0x000C5F18 File Offset: 0x000C4118
		protected virtual void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				if (disposing)
				{
				}
				this.m_disposed = true;
			}
		}

		// Token: 0x060039BE RID: 14782 RVA: 0x000C5F34 File Offset: 0x000C4134
		public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException("TransformBlock");
			}
			if (inputBuffer == null)
			{
				throw new ArgumentNullException("inputBuffer");
			}
			if (outputBuffer == null)
			{
				throw new ArgumentNullException("outputBuffer");
			}
			if (inputCount < 0)
			{
				throw new ArgumentException("inputCount", "< 0");
			}
			if (inputCount > inputBuffer.Length)
			{
				throw new ArgumentException("inputCount", Locale.GetText("Overflow"));
			}
			if (inputOffset < 0)
			{
				throw new ArgumentOutOfRangeException("inputOffset", "< 0");
			}
			if (inputOffset > inputBuffer.Length - inputCount)
			{
				throw new ArgumentException("inputOffset", Locale.GetText("Overflow"));
			}
			if (outputOffset < 0)
			{
				throw new ArgumentOutOfRangeException("outputOffset", "< 0");
			}
			if (outputOffset > outputBuffer.Length - inputCount)
			{
				throw new ArgumentException("outputOffset", Locale.GetText("Overflow"));
			}
			ToBase64Transform.InternalTransformBlock(inputBuffer, inputOffset, inputCount, outputBuffer, outputOffset);
			return this.OutputBlockSize;
		}

		// Token: 0x060039BF RID: 14783 RVA: 0x000C6034 File Offset: 0x000C4234
		internal static void InternalTransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
		{
			byte[] encodeTable = Base64Constants.EncodeTable;
			int num = (int)inputBuffer[inputOffset];
			int num2 = (int)inputBuffer[inputOffset + 1];
			int num3 = (int)inputBuffer[inputOffset + 2];
			outputBuffer[outputOffset] = encodeTable[num >> 2];
			outputBuffer[outputOffset + 1] = encodeTable[(num << 4 & 48) | num2 >> 4];
			outputBuffer[outputOffset + 2] = encodeTable[(num2 << 2 & 60) | num3 >> 6];
			outputBuffer[outputOffset + 3] = encodeTable[num3 & 63];
		}

		// Token: 0x060039C0 RID: 14784 RVA: 0x000C6090 File Offset: 0x000C4290
		public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException("TransformFinalBlock");
			}
			if (inputBuffer == null)
			{
				throw new ArgumentNullException("inputBuffer");
			}
			if (inputCount < 0)
			{
				throw new ArgumentException("inputCount", "< 0");
			}
			if (inputOffset > inputBuffer.Length - inputCount)
			{
				throw new ArgumentException("inputCount", Locale.GetText("Overflow"));
			}
			if (inputCount > this.InputBlockSize)
			{
				throw new ArgumentOutOfRangeException(Locale.GetText("Invalid input length"));
			}
			return ToBase64Transform.InternalTransformFinalBlock(inputBuffer, inputOffset, inputCount);
		}

		// Token: 0x060039C1 RID: 14785 RVA: 0x000C6120 File Offset: 0x000C4320
		internal static byte[] InternalTransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			int num = 3;
			int num2 = 4;
			int num3 = inputCount / num;
			int num4 = inputCount % num;
			byte[] array = new byte[(inputCount == 0) ? 0 : ((inputCount + 2) / num * num2)];
			int num5 = 0;
			for (int i = 0; i < num3; i++)
			{
				ToBase64Transform.InternalTransformBlock(inputBuffer, inputOffset, num, array, num5);
				inputOffset += num;
				num5 += num2;
			}
			byte[] encodeTable = Base64Constants.EncodeTable;
			switch (num4)
			{
			case 1:
			{
				int num6 = (int)inputBuffer[inputOffset];
				array[num5] = encodeTable[num6 >> 2];
				array[num5 + 1] = encodeTable[num6 << 4 & 48];
				array[num5 + 2] = 61;
				array[num5 + 3] = 61;
				break;
			}
			case 2:
			{
				int num6 = (int)inputBuffer[inputOffset];
				int num7 = (int)inputBuffer[inputOffset + 1];
				array[num5] = encodeTable[num6 >> 2];
				array[num5 + 1] = encodeTable[(num6 << 4 & 48) | num7 >> 4];
				array[num5 + 2] = encodeTable[num7 << 2 & 60];
				array[num5 + 3] = 61;
				break;
			}
			}
			return array;
		}

		// Token: 0x0400190C RID: 6412
		private const int inputBlockSize = 3;

		// Token: 0x0400190D RID: 6413
		private const int outputBlockSize = 4;

		// Token: 0x0400190E RID: 6414
		private bool m_disposed;
	}
}
