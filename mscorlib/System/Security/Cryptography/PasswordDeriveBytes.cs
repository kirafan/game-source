﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Security.Cryptography
{
	// Token: 0x020005C2 RID: 1474
	[ComVisible(true)]
	public class PasswordDeriveBytes : DeriveBytes
	{
		// Token: 0x0600386A RID: 14442 RVA: 0x000B7D60 File Offset: 0x000B5F60
		public PasswordDeriveBytes(string strPassword, byte[] rgbSalt)
		{
			this.Prepare(strPassword, rgbSalt, "SHA1", 100);
		}

		// Token: 0x0600386B RID: 14443 RVA: 0x000B7D78 File Offset: 0x000B5F78
		public PasswordDeriveBytes(string strPassword, byte[] rgbSalt, CspParameters cspParams)
		{
			this.Prepare(strPassword, rgbSalt, "SHA1", 100);
			if (cspParams != null)
			{
				throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono for PasswordDeriveBytes."));
			}
		}

		// Token: 0x0600386C RID: 14444 RVA: 0x000B7DA8 File Offset: 0x000B5FA8
		public PasswordDeriveBytes(string strPassword, byte[] rgbSalt, string strHashName, int iterations)
		{
			this.Prepare(strPassword, rgbSalt, strHashName, iterations);
		}

		// Token: 0x0600386D RID: 14445 RVA: 0x000B7DBC File Offset: 0x000B5FBC
		public PasswordDeriveBytes(string strPassword, byte[] rgbSalt, string strHashName, int iterations, CspParameters cspParams)
		{
			this.Prepare(strPassword, rgbSalt, strHashName, iterations);
			if (cspParams != null)
			{
				throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono for PasswordDeriveBytes."));
			}
		}

		// Token: 0x0600386E RID: 14446 RVA: 0x000B7DF4 File Offset: 0x000B5FF4
		public PasswordDeriveBytes(byte[] password, byte[] salt)
		{
			this.Prepare(password, salt, "SHA1", 100);
		}

		// Token: 0x0600386F RID: 14447 RVA: 0x000B7E0C File Offset: 0x000B600C
		public PasswordDeriveBytes(byte[] password, byte[] salt, CspParameters cspParams)
		{
			this.Prepare(password, salt, "SHA1", 100);
			if (cspParams != null)
			{
				throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono for PasswordDeriveBytes."));
			}
		}

		// Token: 0x06003870 RID: 14448 RVA: 0x000B7E3C File Offset: 0x000B603C
		public PasswordDeriveBytes(byte[] password, byte[] salt, string hashName, int iterations)
		{
			this.Prepare(password, salt, hashName, iterations);
		}

		// Token: 0x06003871 RID: 14449 RVA: 0x000B7E50 File Offset: 0x000B6050
		public PasswordDeriveBytes(byte[] password, byte[] salt, string hashName, int iterations, CspParameters cspParams)
		{
			this.Prepare(password, salt, hashName, iterations);
			if (cspParams != null)
			{
				throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono for PasswordDeriveBytes."));
			}
		}

		// Token: 0x06003872 RID: 14450 RVA: 0x000B7E88 File Offset: 0x000B6088
		~PasswordDeriveBytes()
		{
			if (this.initial != null)
			{
				Array.Clear(this.initial, 0, this.initial.Length);
				this.initial = null;
			}
			Array.Clear(this.password, 0, this.password.Length);
		}

		// Token: 0x06003873 RID: 14451 RVA: 0x000B7EF8 File Offset: 0x000B60F8
		private void Prepare(string strPassword, byte[] rgbSalt, string strHashName, int iterations)
		{
			if (strPassword == null)
			{
				throw new ArgumentNullException("strPassword");
			}
			byte[] bytes = Encoding.UTF8.GetBytes(strPassword);
			this.Prepare(bytes, rgbSalt, strHashName, iterations);
			Array.Clear(bytes, 0, bytes.Length);
		}

		// Token: 0x06003874 RID: 14452 RVA: 0x000B7F38 File Offset: 0x000B6138
		private void Prepare(byte[] password, byte[] rgbSalt, string strHashName, int iterations)
		{
			if (password == null)
			{
				throw new ArgumentNullException("password");
			}
			this.password = (byte[])password.Clone();
			this.Salt = rgbSalt;
			this.HashName = strHashName;
			this.IterationCount = iterations;
			this.state = 0;
		}

		// Token: 0x17000AB7 RID: 2743
		// (get) Token: 0x06003875 RID: 14453 RVA: 0x000B7F84 File Offset: 0x000B6184
		// (set) Token: 0x06003876 RID: 14454 RVA: 0x000B7F8C File Offset: 0x000B618C
		public string HashName
		{
			get
			{
				return this.HashNameValue;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("HashName");
				}
				if (this.state != 0)
				{
					throw new CryptographicException(Locale.GetText("Can't change this property at this stage"));
				}
				this.HashNameValue = value;
			}
		}

		// Token: 0x17000AB8 RID: 2744
		// (get) Token: 0x06003877 RID: 14455 RVA: 0x000B7FC4 File Offset: 0x000B61C4
		// (set) Token: 0x06003878 RID: 14456 RVA: 0x000B7FCC File Offset: 0x000B61CC
		public int IterationCount
		{
			get
			{
				return this.IterationsValue;
			}
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("> 0", "IterationCount");
				}
				if (this.state != 0)
				{
					throw new CryptographicException(Locale.GetText("Can't change this property at this stage"));
				}
				this.IterationsValue = value;
			}
		}

		// Token: 0x17000AB9 RID: 2745
		// (get) Token: 0x06003879 RID: 14457 RVA: 0x000B8008 File Offset: 0x000B6208
		// (set) Token: 0x0600387A RID: 14458 RVA: 0x000B8028 File Offset: 0x000B6228
		public byte[] Salt
		{
			get
			{
				if (this.SaltValue == null)
				{
					return null;
				}
				return (byte[])this.SaltValue.Clone();
			}
			set
			{
				if (this.state != 0)
				{
					throw new CryptographicException(Locale.GetText("Can't change this property at this stage"));
				}
				if (value != null)
				{
					this.SaltValue = (byte[])value.Clone();
				}
				else
				{
					this.SaltValue = null;
				}
			}
		}

		// Token: 0x0600387B RID: 14459 RVA: 0x000B8074 File Offset: 0x000B6274
		public byte[] CryptDeriveKey(string algname, string alghashname, int keySize, byte[] rgbIV)
		{
			if (keySize > 128)
			{
				throw new CryptographicException(Locale.GetText("Key Size can't be greater than 128 bits"));
			}
			throw new NotSupportedException(Locale.GetText("CspParameters not supported by Mono"));
		}

		// Token: 0x0600387C RID: 14460 RVA: 0x000B80AC File Offset: 0x000B62AC
		[Obsolete("see Rfc2898DeriveBytes for PKCS#5 v2 support")]
		public override byte[] GetBytes(int cb)
		{
			if (cb < 1)
			{
				throw new IndexOutOfRangeException("cb");
			}
			if (this.state == 0)
			{
				this.Reset();
				this.state = 1;
			}
			byte[] array = new byte[cb];
			int i = 0;
			int num = Math.Max(1, this.IterationsValue - 1);
			if (this.output == null)
			{
				this.output = this.initial;
				for (int j = 0; j < num - 1; j++)
				{
					this.output = this.hash.ComputeHash(this.output);
				}
			}
			while (i < cb)
			{
				byte[] array2;
				if (this.hashnumber == 0)
				{
					array2 = this.hash.ComputeHash(this.output);
				}
				else
				{
					if (this.hashnumber >= 1000)
					{
						throw new CryptographicException(Locale.GetText("too long"));
					}
					string text = Convert.ToString(this.hashnumber);
					array2 = new byte[this.output.Length + text.Length];
					for (int k = 0; k < text.Length; k++)
					{
						array2[k] = (byte)text[k];
					}
					Buffer.BlockCopy(this.output, 0, array2, text.Length, this.output.Length);
					array2 = this.hash.ComputeHash(array2);
				}
				int val = array2.Length - this.position;
				int num2 = Math.Min(cb - i, val);
				Buffer.BlockCopy(array2, this.position, array, i, num2);
				i += num2;
				this.position += num2;
				while (this.position >= array2.Length)
				{
					this.position -= array2.Length;
					this.hashnumber++;
				}
			}
			return array;
		}

		// Token: 0x0600387D RID: 14461 RVA: 0x000B8284 File Offset: 0x000B6484
		public override void Reset()
		{
			this.state = 0;
			this.position = 0;
			this.hashnumber = 0;
			this.hash = HashAlgorithm.Create(this.HashNameValue);
			if (this.SaltValue != null)
			{
				this.hash.TransformBlock(this.password, 0, this.password.Length, this.password, 0);
				this.hash.TransformFinalBlock(this.SaltValue, 0, this.SaltValue.Length);
				this.initial = this.hash.Hash;
			}
			else
			{
				this.initial = this.hash.ComputeHash(this.password);
			}
		}

		// Token: 0x0400187F RID: 6271
		private string HashNameValue;

		// Token: 0x04001880 RID: 6272
		private byte[] SaltValue;

		// Token: 0x04001881 RID: 6273
		private int IterationsValue;

		// Token: 0x04001882 RID: 6274
		private HashAlgorithm hash;

		// Token: 0x04001883 RID: 6275
		private int state;

		// Token: 0x04001884 RID: 6276
		private byte[] password;

		// Token: 0x04001885 RID: 6277
		private byte[] initial;

		// Token: 0x04001886 RID: 6278
		private byte[] output;

		// Token: 0x04001887 RID: 6279
		private int position;

		// Token: 0x04001888 RID: 6280
		private int hashnumber;
	}
}
