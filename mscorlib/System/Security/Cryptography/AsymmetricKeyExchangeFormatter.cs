﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x02000596 RID: 1430
	[ComVisible(true)]
	public abstract class AsymmetricKeyExchangeFormatter
	{
		// Token: 0x17000A79 RID: 2681
		// (get) Token: 0x06003745 RID: 14149
		public abstract string Parameters { get; }

		// Token: 0x06003746 RID: 14150
		public abstract byte[] CreateKeyExchange(byte[] data);

		// Token: 0x06003747 RID: 14151
		public abstract byte[] CreateKeyExchange(byte[] data, Type symAlgType);

		// Token: 0x06003748 RID: 14152
		public abstract void SetKey(AsymmetricAlgorithm key);
	}
}
