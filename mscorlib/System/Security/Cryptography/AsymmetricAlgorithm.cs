﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x02000594 RID: 1428
	[ComVisible(true)]
	public abstract class AsymmetricAlgorithm : IDisposable
	{
		// Token: 0x06003732 RID: 14130 RVA: 0x000B2A90 File Offset: 0x000B0C90
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x17000A74 RID: 2676
		// (get) Token: 0x06003733 RID: 14131
		public abstract string KeyExchangeAlgorithm { get; }

		// Token: 0x17000A75 RID: 2677
		// (get) Token: 0x06003734 RID: 14132 RVA: 0x000B2AA0 File Offset: 0x000B0CA0
		// (set) Token: 0x06003735 RID: 14133 RVA: 0x000B2AA8 File Offset: 0x000B0CA8
		public virtual int KeySize
		{
			get
			{
				return this.KeySizeValue;
			}
			set
			{
				if (!KeySizes.IsLegalKeySize(this.LegalKeySizesValue, value))
				{
					throw new CryptographicException(Locale.GetText("Key size not supported by algorithm."));
				}
				this.KeySizeValue = value;
			}
		}

		// Token: 0x17000A76 RID: 2678
		// (get) Token: 0x06003736 RID: 14134 RVA: 0x000B2AE0 File Offset: 0x000B0CE0
		public virtual KeySizes[] LegalKeySizes
		{
			get
			{
				return this.LegalKeySizesValue;
			}
		}

		// Token: 0x17000A77 RID: 2679
		// (get) Token: 0x06003737 RID: 14135
		public abstract string SignatureAlgorithm { get; }

		// Token: 0x06003738 RID: 14136 RVA: 0x000B2AE8 File Offset: 0x000B0CE8
		public void Clear()
		{
			this.Dispose(false);
		}

		// Token: 0x06003739 RID: 14137
		protected abstract void Dispose(bool disposing);

		// Token: 0x0600373A RID: 14138
		public abstract void FromXmlString(string xmlString);

		// Token: 0x0600373B RID: 14139
		public abstract string ToXmlString(bool includePrivateParameters);

		// Token: 0x0600373C RID: 14140 RVA: 0x000B2AF4 File Offset: 0x000B0CF4
		public static AsymmetricAlgorithm Create()
		{
			return AsymmetricAlgorithm.Create("System.Security.Cryptography.AsymmetricAlgorithm");
		}

		// Token: 0x0600373D RID: 14141 RVA: 0x000B2B00 File Offset: 0x000B0D00
		public static AsymmetricAlgorithm Create(string algName)
		{
			return (AsymmetricAlgorithm)CryptoConfig.CreateFromName(algName);
		}

		// Token: 0x0600373E RID: 14142 RVA: 0x000B2B10 File Offset: 0x000B0D10
		internal static byte[] GetNamedParam(string xml, string param)
		{
			string text = "<" + param + ">";
			int num = xml.IndexOf(text);
			if (num == -1)
			{
				return null;
			}
			string value = "</" + param + ">";
			int num2 = xml.IndexOf(value);
			if (num2 == -1 || num2 <= num)
			{
				return null;
			}
			num += text.Length;
			string s = xml.Substring(num, num2 - num);
			return Convert.FromBase64String(s);
		}

		// Token: 0x04001770 RID: 6000
		protected int KeySizeValue;

		// Token: 0x04001771 RID: 6001
		protected KeySizes[] LegalKeySizesValue;
	}
}
