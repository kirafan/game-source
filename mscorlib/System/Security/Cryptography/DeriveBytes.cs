﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005A5 RID: 1445
	[ComVisible(true)]
	public abstract class DeriveBytes
	{
		// Token: 0x060037AB RID: 14251
		public abstract byte[] GetBytes(int cb);

		// Token: 0x060037AC RID: 14252
		public abstract void Reset();
	}
}
