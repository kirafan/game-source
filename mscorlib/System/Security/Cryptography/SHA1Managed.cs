﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005DC RID: 1500
	[ComVisible(true)]
	public class SHA1Managed : SHA1
	{
		// Token: 0x06003955 RID: 14677 RVA: 0x000C44E0 File Offset: 0x000C26E0
		public SHA1Managed()
		{
			this.sha = new SHA1Internal();
		}

		// Token: 0x06003956 RID: 14678 RVA: 0x000C44F4 File Offset: 0x000C26F4
		protected override void HashCore(byte[] rgb, int ibStart, int cbSize)
		{
			this.State = 1;
			this.sha.HashCore(rgb, ibStart, cbSize);
		}

		// Token: 0x06003957 RID: 14679 RVA: 0x000C450C File Offset: 0x000C270C
		protected override byte[] HashFinal()
		{
			this.State = 0;
			return this.sha.HashFinal();
		}

		// Token: 0x06003958 RID: 14680 RVA: 0x000C4520 File Offset: 0x000C2720
		public override void Initialize()
		{
			this.sha.Initialize();
		}

		// Token: 0x040018D8 RID: 6360
		private SHA1Internal sha;
	}
}
