﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Security.Cryptography
{
	// Token: 0x0200059E RID: 1438
	[ComVisible(true)]
	[Serializable]
	public class CryptographicException : SystemException, _Exception
	{
		// Token: 0x06003772 RID: 14194 RVA: 0x000B3B08 File Offset: 0x000B1D08
		public CryptographicException() : base(Locale.GetText("Error occured during a cryptographic operation."))
		{
			base.HResult = -2146233296;
		}

		// Token: 0x06003773 RID: 14195 RVA: 0x000B3B28 File Offset: 0x000B1D28
		public CryptographicException(int hr)
		{
			base.HResult = hr;
		}

		// Token: 0x06003774 RID: 14196 RVA: 0x000B3B38 File Offset: 0x000B1D38
		public CryptographicException(string message) : base(message)
		{
			base.HResult = -2146233296;
		}

		// Token: 0x06003775 RID: 14197 RVA: 0x000B3B4C File Offset: 0x000B1D4C
		public CryptographicException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233296;
		}

		// Token: 0x06003776 RID: 14198 RVA: 0x000B3B64 File Offset: 0x000B1D64
		public CryptographicException(string format, string insert) : base(string.Format(format, insert))
		{
			base.HResult = -2146233296;
		}

		// Token: 0x06003777 RID: 14199 RVA: 0x000B3B80 File Offset: 0x000B1D80
		protected CryptographicException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
