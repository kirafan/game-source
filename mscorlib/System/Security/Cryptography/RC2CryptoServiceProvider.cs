﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005C6 RID: 1478
	[ComVisible(true)]
	public sealed class RC2CryptoServiceProvider : RC2
	{
		// Token: 0x17000ABD RID: 2749
		// (get) Token: 0x0600388F RID: 14479 RVA: 0x000B847C File Offset: 0x000B667C
		// (set) Token: 0x06003890 RID: 14480 RVA: 0x000B8484 File Offset: 0x000B6684
		public override int EffectiveKeySize
		{
			get
			{
				return base.EffectiveKeySize;
			}
			set
			{
				if (value != this.KeySizeValue)
				{
					throw new CryptographicUnexpectedOperationException(Locale.GetText("Effective key size must match key size for compatibility"));
				}
				base.EffectiveKeySize = value;
			}
		}

		// Token: 0x06003891 RID: 14481 RVA: 0x000B84AC File Offset: 0x000B66AC
		public override ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new RC2Transform(this, false, rgbKey, rgbIV);
		}

		// Token: 0x06003892 RID: 14482 RVA: 0x000B84B8 File Offset: 0x000B66B8
		public override ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new RC2Transform(this, true, rgbKey, rgbIV);
		}

		// Token: 0x06003893 RID: 14483 RVA: 0x000B84C4 File Offset: 0x000B66C4
		public override void GenerateIV()
		{
			this.IVValue = KeyBuilder.IV(this.BlockSizeValue >> 3);
		}

		// Token: 0x06003894 RID: 14484 RVA: 0x000B84DC File Offset: 0x000B66DC
		public override void GenerateKey()
		{
			this.KeyValue = KeyBuilder.Key(this.KeySizeValue >> 3);
		}

		// Token: 0x17000ABE RID: 2750
		// (get) Token: 0x06003895 RID: 14485 RVA: 0x000B84F4 File Offset: 0x000B66F4
		// (set) Token: 0x06003896 RID: 14486 RVA: 0x000B84FC File Offset: 0x000B66FC
		[MonoTODO("Use salt in algorithm")]
		[ComVisible(false)]
		public bool UseSalt
		{
			get
			{
				return this._useSalt;
			}
			set
			{
				this._useSalt = value;
			}
		}

		// Token: 0x0400188B RID: 6283
		private bool _useSalt;
	}
}
