﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005BB RID: 1467
	[ComVisible(true)]
	[Serializable]
	public enum KeyNumber
	{
		// Token: 0x04001869 RID: 6249
		Exchange = 1,
		// Token: 0x0400186A RID: 6250
		Signature
	}
}
