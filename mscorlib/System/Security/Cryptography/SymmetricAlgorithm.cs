﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005E7 RID: 1511
	[ComVisible(true)]
	public abstract class SymmetricAlgorithm : IDisposable
	{
		// Token: 0x06003997 RID: 14743 RVA: 0x000C5B3C File Offset: 0x000C3D3C
		protected SymmetricAlgorithm()
		{
			this.ModeValue = CipherMode.CBC;
			this.PaddingValue = PaddingMode.PKCS7;
			this.m_disposed = false;
		}

		// Token: 0x06003998 RID: 14744 RVA: 0x000C5B5C File Offset: 0x000C3D5C
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06003999 RID: 14745 RVA: 0x000C5B6C File Offset: 0x000C3D6C
		~SymmetricAlgorithm()
		{
			this.Dispose(false);
		}

		// Token: 0x0600399A RID: 14746 RVA: 0x000C5BA8 File Offset: 0x000C3DA8
		public void Clear()
		{
			this.Dispose(true);
		}

		// Token: 0x0600399B RID: 14747 RVA: 0x000C5BB4 File Offset: 0x000C3DB4
		protected virtual void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				if (this.KeyValue != null)
				{
					Array.Clear(this.KeyValue, 0, this.KeyValue.Length);
					this.KeyValue = null;
				}
				if (disposing)
				{
				}
				this.m_disposed = true;
			}
		}

		// Token: 0x17000AD9 RID: 2777
		// (get) Token: 0x0600399C RID: 14748 RVA: 0x000C5C00 File Offset: 0x000C3E00
		// (set) Token: 0x0600399D RID: 14749 RVA: 0x000C5C08 File Offset: 0x000C3E08
		public virtual int BlockSize
		{
			get
			{
				return this.BlockSizeValue;
			}
			set
			{
				if (!KeySizes.IsLegalKeySize(this.LegalBlockSizesValue, value))
				{
					throw new CryptographicException(Locale.GetText("block size not supported by algorithm"));
				}
				if (this.BlockSizeValue != value)
				{
					this.BlockSizeValue = value;
					this.IVValue = null;
				}
			}
		}

		// Token: 0x17000ADA RID: 2778
		// (get) Token: 0x0600399E RID: 14750 RVA: 0x000C5C48 File Offset: 0x000C3E48
		// (set) Token: 0x0600399F RID: 14751 RVA: 0x000C5C50 File Offset: 0x000C3E50
		public virtual int FeedbackSize
		{
			get
			{
				return this.FeedbackSizeValue;
			}
			set
			{
				if (value <= 0 || value > this.BlockSizeValue)
				{
					throw new CryptographicException(Locale.GetText("feedback size larger than block size"));
				}
				this.FeedbackSizeValue = value;
			}
		}

		// Token: 0x17000ADB RID: 2779
		// (get) Token: 0x060039A0 RID: 14752 RVA: 0x000C5C88 File Offset: 0x000C3E88
		// (set) Token: 0x060039A1 RID: 14753 RVA: 0x000C5CAC File Offset: 0x000C3EAC
		public virtual byte[] IV
		{
			get
			{
				if (this.IVValue == null)
				{
					this.GenerateIV();
				}
				return (byte[])this.IVValue.Clone();
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("IV");
				}
				if (value.Length << 3 != this.BlockSizeValue)
				{
					throw new CryptographicException(Locale.GetText("IV length is different than block size"));
				}
				this.IVValue = (byte[])value.Clone();
			}
		}

		// Token: 0x17000ADC RID: 2780
		// (get) Token: 0x060039A2 RID: 14754 RVA: 0x000C5CFC File Offset: 0x000C3EFC
		// (set) Token: 0x060039A3 RID: 14755 RVA: 0x000C5D20 File Offset: 0x000C3F20
		public virtual byte[] Key
		{
			get
			{
				if (this.KeyValue == null)
				{
					this.GenerateKey();
				}
				return (byte[])this.KeyValue.Clone();
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Key");
				}
				int num = value.Length << 3;
				if (!KeySizes.IsLegalKeySize(this.LegalKeySizesValue, num))
				{
					throw new CryptographicException(Locale.GetText("Key size not supported by algorithm"));
				}
				this.KeySizeValue = num;
				this.KeyValue = (byte[])value.Clone();
			}
		}

		// Token: 0x17000ADD RID: 2781
		// (get) Token: 0x060039A4 RID: 14756 RVA: 0x000C5D80 File Offset: 0x000C3F80
		// (set) Token: 0x060039A5 RID: 14757 RVA: 0x000C5D88 File Offset: 0x000C3F88
		public virtual int KeySize
		{
			get
			{
				return this.KeySizeValue;
			}
			set
			{
				if (!KeySizes.IsLegalKeySize(this.LegalKeySizesValue, value))
				{
					throw new CryptographicException(Locale.GetText("Key size not supported by algorithm"));
				}
				this.KeySizeValue = value;
				this.KeyValue = null;
			}
		}

		// Token: 0x17000ADE RID: 2782
		// (get) Token: 0x060039A6 RID: 14758 RVA: 0x000C5DBC File Offset: 0x000C3FBC
		public virtual KeySizes[] LegalBlockSizes
		{
			get
			{
				return this.LegalBlockSizesValue;
			}
		}

		// Token: 0x17000ADF RID: 2783
		// (get) Token: 0x060039A7 RID: 14759 RVA: 0x000C5DC4 File Offset: 0x000C3FC4
		public virtual KeySizes[] LegalKeySizes
		{
			get
			{
				return this.LegalKeySizesValue;
			}
		}

		// Token: 0x17000AE0 RID: 2784
		// (get) Token: 0x060039A8 RID: 14760 RVA: 0x000C5DCC File Offset: 0x000C3FCC
		// (set) Token: 0x060039A9 RID: 14761 RVA: 0x000C5DD4 File Offset: 0x000C3FD4
		public virtual CipherMode Mode
		{
			get
			{
				return this.ModeValue;
			}
			set
			{
				if (!Enum.IsDefined(this.ModeValue.GetType(), value))
				{
					throw new CryptographicException(Locale.GetText("Cipher mode not available"));
				}
				this.ModeValue = value;
			}
		}

		// Token: 0x17000AE1 RID: 2785
		// (get) Token: 0x060039AA RID: 14762 RVA: 0x000C5E10 File Offset: 0x000C4010
		// (set) Token: 0x060039AB RID: 14763 RVA: 0x000C5E18 File Offset: 0x000C4018
		public virtual PaddingMode Padding
		{
			get
			{
				return this.PaddingValue;
			}
			set
			{
				if (!Enum.IsDefined(this.PaddingValue.GetType(), value))
				{
					throw new CryptographicException(Locale.GetText("Padding mode not available"));
				}
				this.PaddingValue = value;
			}
		}

		// Token: 0x060039AC RID: 14764 RVA: 0x000C5E54 File Offset: 0x000C4054
		public virtual ICryptoTransform CreateDecryptor()
		{
			return this.CreateDecryptor(this.Key, this.IV);
		}

		// Token: 0x060039AD RID: 14765
		public abstract ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgbIV);

		// Token: 0x060039AE RID: 14766 RVA: 0x000C5E68 File Offset: 0x000C4068
		public virtual ICryptoTransform CreateEncryptor()
		{
			return this.CreateEncryptor(this.Key, this.IV);
		}

		// Token: 0x060039AF RID: 14767
		public abstract ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgbIV);

		// Token: 0x060039B0 RID: 14768
		public abstract void GenerateIV();

		// Token: 0x060039B1 RID: 14769
		public abstract void GenerateKey();

		// Token: 0x060039B2 RID: 14770 RVA: 0x000C5E7C File Offset: 0x000C407C
		public bool ValidKeySize(int bitLength)
		{
			return KeySizes.IsLegalKeySize(this.LegalKeySizesValue, bitLength);
		}

		// Token: 0x060039B3 RID: 14771 RVA: 0x000C5E8C File Offset: 0x000C408C
		public static SymmetricAlgorithm Create()
		{
			return SymmetricAlgorithm.Create("System.Security.Cryptography.SymmetricAlgorithm");
		}

		// Token: 0x060039B4 RID: 14772 RVA: 0x000C5E98 File Offset: 0x000C4098
		public static SymmetricAlgorithm Create(string algName)
		{
			return (SymmetricAlgorithm)CryptoConfig.CreateFromName(algName);
		}

		// Token: 0x04001902 RID: 6402
		protected int BlockSizeValue;

		// Token: 0x04001903 RID: 6403
		protected byte[] IVValue;

		// Token: 0x04001904 RID: 6404
		protected int KeySizeValue;

		// Token: 0x04001905 RID: 6405
		protected byte[] KeyValue;

		// Token: 0x04001906 RID: 6406
		protected KeySizes[] LegalBlockSizesValue;

		// Token: 0x04001907 RID: 6407
		protected KeySizes[] LegalKeySizesValue;

		// Token: 0x04001908 RID: 6408
		protected int FeedbackSizeValue;

		// Token: 0x04001909 RID: 6409
		protected CipherMode ModeValue;

		// Token: 0x0400190A RID: 6410
		protected PaddingMode PaddingValue;

		// Token: 0x0400190B RID: 6411
		private bool m_disposed;
	}
}
