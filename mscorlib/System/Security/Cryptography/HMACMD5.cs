﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005B2 RID: 1458
	[ComVisible(true)]
	public class HMACMD5 : HMAC
	{
		// Token: 0x06003826 RID: 14374 RVA: 0x000B6778 File Offset: 0x000B4978
		public HMACMD5() : this(KeyBuilder.Key(8))
		{
		}

		// Token: 0x06003827 RID: 14375 RVA: 0x000B6788 File Offset: 0x000B4988
		public HMACMD5(byte[] key)
		{
			base.HashName = "MD5";
			this.HashSizeValue = 128;
			this.Key = key;
		}
	}
}
