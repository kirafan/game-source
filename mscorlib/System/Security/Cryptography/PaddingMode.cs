﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005C1 RID: 1473
	[ComVisible(true)]
	[Serializable]
	public enum PaddingMode
	{
		// Token: 0x0400187A RID: 6266
		None = 1,
		// Token: 0x0400187B RID: 6267
		PKCS7,
		// Token: 0x0400187C RID: 6268
		Zeros,
		// Token: 0x0400187D RID: 6269
		ANSIX923,
		// Token: 0x0400187E RID: 6270
		ISO10126
	}
}
