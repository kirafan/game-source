﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005CA RID: 1482
	[ComVisible(true)]
	public sealed class RijndaelManaged : Rijndael
	{
		// Token: 0x060038AA RID: 14506 RVA: 0x000B96A4 File Offset: 0x000B78A4
		public override void GenerateIV()
		{
			this.IVValue = KeyBuilder.IV(this.BlockSizeValue >> 3);
		}

		// Token: 0x060038AB RID: 14507 RVA: 0x000B96BC File Offset: 0x000B78BC
		public override void GenerateKey()
		{
			this.KeyValue = KeyBuilder.Key(this.KeySizeValue >> 3);
		}

		// Token: 0x060038AC RID: 14508 RVA: 0x000B96D4 File Offset: 0x000B78D4
		public override ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new RijndaelManagedTransform(this, false, rgbKey, rgbIV);
		}

		// Token: 0x060038AD RID: 14509 RVA: 0x000B96E0 File Offset: 0x000B78E0
		public override ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new RijndaelManagedTransform(this, true, rgbKey, rgbIV);
		}
	}
}
