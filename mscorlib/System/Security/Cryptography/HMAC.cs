﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005B1 RID: 1457
	[ComVisible(true)]
	public abstract class HMAC : KeyedHashAlgorithm
	{
		// Token: 0x06003817 RID: 14359 RVA: 0x000B64B0 File Offset: 0x000B46B0
		protected HMAC()
		{
			this._disposed = false;
			this._blockSizeValue = 64;
		}

		// Token: 0x17000AA7 RID: 2727
		// (get) Token: 0x06003818 RID: 14360 RVA: 0x000B64C8 File Offset: 0x000B46C8
		// (set) Token: 0x06003819 RID: 14361 RVA: 0x000B64D0 File Offset: 0x000B46D0
		protected int BlockSizeValue
		{
			get
			{
				return this._blockSizeValue;
			}
			set
			{
				this._blockSizeValue = value;
			}
		}

		// Token: 0x17000AA8 RID: 2728
		// (get) Token: 0x0600381A RID: 14362 RVA: 0x000B64DC File Offset: 0x000B46DC
		// (set) Token: 0x0600381B RID: 14363 RVA: 0x000B64E4 File Offset: 0x000B46E4
		public string HashName
		{
			get
			{
				return this._hashName;
			}
			set
			{
				this._hashName = value;
				this._algo = HashAlgorithm.Create(this._hashName);
			}
		}

		// Token: 0x17000AA9 RID: 2729
		// (get) Token: 0x0600381C RID: 14364 RVA: 0x000B6500 File Offset: 0x000B4700
		// (set) Token: 0x0600381D RID: 14365 RVA: 0x000B6514 File Offset: 0x000B4714
		public override byte[] Key
		{
			get
			{
				return (byte[])base.Key.Clone();
			}
			set
			{
				if (value != null && value.Length > 64)
				{
					base.Key = this._algo.ComputeHash(value);
				}
				else
				{
					base.Key = (byte[])value.Clone();
				}
			}
		}

		// Token: 0x17000AAA RID: 2730
		// (get) Token: 0x0600381E RID: 14366 RVA: 0x000B655C File Offset: 0x000B475C
		internal BlockProcessor Block
		{
			get
			{
				if (this._block == null)
				{
					this._block = new BlockProcessor(this._algo, this.BlockSizeValue >> 3);
				}
				return this._block;
			}
		}

		// Token: 0x0600381F RID: 14367 RVA: 0x000B6594 File Offset: 0x000B4794
		private byte[] KeySetup(byte[] key, byte padding)
		{
			byte[] array = new byte[this.BlockSizeValue];
			for (int i = 0; i < key.Length; i++)
			{
				array[i] = (key[i] ^ padding);
			}
			for (int j = key.Length; j < this.BlockSizeValue; j++)
			{
				array[j] = padding;
			}
			return array;
		}

		// Token: 0x06003820 RID: 14368 RVA: 0x000B65E8 File Offset: 0x000B47E8
		protected override void Dispose(bool disposing)
		{
			if (!this._disposed)
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x06003821 RID: 14369 RVA: 0x000B65FC File Offset: 0x000B47FC
		protected override void HashCore(byte[] rgb, int ib, int cb)
		{
			if (this._disposed)
			{
				throw new ObjectDisposedException("HMACSHA1");
			}
			if (this.State == 0)
			{
				this.Initialize();
				this.State = 1;
			}
			this.Block.Core(rgb, ib, cb);
		}

		// Token: 0x06003822 RID: 14370 RVA: 0x000B6648 File Offset: 0x000B4848
		protected override byte[] HashFinal()
		{
			if (this._disposed)
			{
				throw new ObjectDisposedException("HMAC");
			}
			this.State = 0;
			this.Block.Final();
			byte[] hash = this._algo.Hash;
			byte[] array = this.KeySetup(this.Key, 92);
			this._algo.Initialize();
			this._algo.TransformBlock(array, 0, array.Length, array, 0);
			this._algo.TransformFinalBlock(hash, 0, hash.Length);
			byte[] hash2 = this._algo.Hash;
			this._algo.Initialize();
			Array.Clear(array, 0, array.Length);
			Array.Clear(hash, 0, hash.Length);
			return hash2;
		}

		// Token: 0x06003823 RID: 14371 RVA: 0x000B66F4 File Offset: 0x000B48F4
		public override void Initialize()
		{
			if (this._disposed)
			{
				throw new ObjectDisposedException("HMAC");
			}
			this.State = 0;
			this.Block.Initialize();
			byte[] array = this.KeySetup(this.Key, 54);
			this._algo.Initialize();
			this.Block.Core(array);
			Array.Clear(array, 0, array.Length);
		}

		// Token: 0x06003824 RID: 14372 RVA: 0x000B675C File Offset: 0x000B495C
		public new static HMAC Create()
		{
			return HMAC.Create("System.Security.Cryptography.HMAC");
		}

		// Token: 0x06003825 RID: 14373 RVA: 0x000B6768 File Offset: 0x000B4968
		public new static HMAC Create(string algorithmName)
		{
			return (HMAC)CryptoConfig.CreateFromName(algorithmName);
		}

		// Token: 0x0400185E RID: 6238
		private bool _disposed;

		// Token: 0x0400185F RID: 6239
		private string _hashName;

		// Token: 0x04001860 RID: 6240
		private HashAlgorithm _algo;

		// Token: 0x04001861 RID: 6241
		private BlockProcessor _block;

		// Token: 0x04001862 RID: 6242
		private int _blockSizeValue;
	}
}
