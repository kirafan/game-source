﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Security.Cryptography
{
	// Token: 0x0200059F RID: 1439
	[ComVisible(true)]
	[Serializable]
	public class CryptographicUnexpectedOperationException : CryptographicException
	{
		// Token: 0x06003778 RID: 14200 RVA: 0x000B3B8C File Offset: 0x000B1D8C
		public CryptographicUnexpectedOperationException() : base(Locale.GetText("Unexpected error occured during a cryptographic operation."))
		{
			base.HResult = -2146233295;
		}

		// Token: 0x06003779 RID: 14201 RVA: 0x000B3BAC File Offset: 0x000B1DAC
		public CryptographicUnexpectedOperationException(string message) : base(message)
		{
			base.HResult = -2146233295;
		}

		// Token: 0x0600377A RID: 14202 RVA: 0x000B3BC0 File Offset: 0x000B1DC0
		public CryptographicUnexpectedOperationException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233295;
		}

		// Token: 0x0600377B RID: 14203 RVA: 0x000B3BD8 File Offset: 0x000B1DD8
		public CryptographicUnexpectedOperationException(string format, string insert) : base(string.Format(format, insert))
		{
			base.HResult = -2146233295;
		}

		// Token: 0x0600377C RID: 14204 RVA: 0x000B3BF4 File Offset: 0x000B1DF4
		protected CryptographicUnexpectedOperationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
