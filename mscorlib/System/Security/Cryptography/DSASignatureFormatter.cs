﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005AD RID: 1453
	[ComVisible(true)]
	public class DSASignatureFormatter : AsymmetricSignatureFormatter
	{
		// Token: 0x060037EF RID: 14319 RVA: 0x000B5B64 File Offset: 0x000B3D64
		public DSASignatureFormatter()
		{
		}

		// Token: 0x060037F0 RID: 14320 RVA: 0x000B5B6C File Offset: 0x000B3D6C
		public DSASignatureFormatter(AsymmetricAlgorithm key)
		{
			this.SetKey(key);
		}

		// Token: 0x060037F1 RID: 14321 RVA: 0x000B5B7C File Offset: 0x000B3D7C
		public override byte[] CreateSignature(byte[] rgbHash)
		{
			if (this.dsa == null)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("missing key"));
			}
			return this.dsa.CreateSignature(rgbHash);
		}

		// Token: 0x060037F2 RID: 14322 RVA: 0x000B5BA8 File Offset: 0x000B3DA8
		public override void SetHashAlgorithm(string strName)
		{
			if (strName == null)
			{
				throw new ArgumentNullException("strName");
			}
			try
			{
				SHA1.Create(strName);
			}
			catch (InvalidCastException)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("DSA requires SHA1"));
			}
		}

		// Token: 0x060037F3 RID: 14323 RVA: 0x000B5C04 File Offset: 0x000B3E04
		public override void SetKey(AsymmetricAlgorithm key)
		{
			if (key != null)
			{
				this.dsa = (DSA)key;
				return;
			}
			throw new ArgumentNullException("key");
		}

		// Token: 0x04001850 RID: 6224
		private DSA dsa;
	}
}
