﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005DD RID: 1501
	[ComVisible(true)]
	public abstract class SHA256 : HashAlgorithm
	{
		// Token: 0x06003959 RID: 14681 RVA: 0x000C4530 File Offset: 0x000C2730
		protected SHA256()
		{
			this.HashSizeValue = 256;
		}

		// Token: 0x0600395A RID: 14682 RVA: 0x000C4544 File Offset: 0x000C2744
		public new static SHA256 Create()
		{
			return SHA256.Create("System.Security.Cryptography.SHA256");
		}

		// Token: 0x0600395B RID: 14683 RVA: 0x000C4550 File Offset: 0x000C2750
		public new static SHA256 Create(string hashName)
		{
			return (SHA256)CryptoConfig.CreateFromName(hashName);
		}
	}
}
