﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005E1 RID: 1505
	[ComVisible(true)]
	public abstract class SHA512 : HashAlgorithm
	{
		// Token: 0x06003971 RID: 14705 RVA: 0x000C5184 File Offset: 0x000C3384
		protected SHA512()
		{
			this.HashSizeValue = 512;
		}

		// Token: 0x06003972 RID: 14706 RVA: 0x000C5198 File Offset: 0x000C3398
		public new static SHA512 Create()
		{
			return SHA512.Create("System.Security.Cryptography.SHA512");
		}

		// Token: 0x06003973 RID: 14707 RVA: 0x000C51A4 File Offset: 0x000C33A4
		public new static SHA512 Create(string hashName)
		{
			return (SHA512)CryptoConfig.CreateFromName(hashName);
		}
	}
}
