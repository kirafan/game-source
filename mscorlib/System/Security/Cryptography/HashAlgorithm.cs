﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005B0 RID: 1456
	[ComVisible(true)]
	public abstract class HashAlgorithm : IDisposable, ICryptoTransform
	{
		// Token: 0x06003803 RID: 14339 RVA: 0x000B61A0 File Offset: 0x000B43A0
		protected HashAlgorithm()
		{
			this.disposed = false;
		}

		// Token: 0x06003804 RID: 14340 RVA: 0x000B61B0 File Offset: 0x000B43B0
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x17000AA1 RID: 2721
		// (get) Token: 0x06003805 RID: 14341 RVA: 0x000B61C0 File Offset: 0x000B43C0
		public virtual bool CanTransformMultipleBlocks
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000AA2 RID: 2722
		// (get) Token: 0x06003806 RID: 14342 RVA: 0x000B61C4 File Offset: 0x000B43C4
		public virtual bool CanReuseTransform
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06003807 RID: 14343 RVA: 0x000B61C8 File Offset: 0x000B43C8
		public void Clear()
		{
			this.Dispose(true);
		}

		// Token: 0x06003808 RID: 14344 RVA: 0x000B61D4 File Offset: 0x000B43D4
		public byte[] ComputeHash(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return this.ComputeHash(buffer, 0, buffer.Length);
		}

		// Token: 0x06003809 RID: 14345 RVA: 0x000B61F4 File Offset: 0x000B43F4
		public byte[] ComputeHash(byte[] buffer, int offset, int count)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("HashAlgorithm");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentException("count", "< 0");
			}
			if (offset > buffer.Length - count)
			{
				throw new ArgumentException("offset + count", Locale.GetText("Overflow"));
			}
			this.HashCore(buffer, offset, count);
			this.HashValue = this.HashFinal();
			this.Initialize();
			return this.HashValue;
		}

		// Token: 0x0600380A RID: 14346 RVA: 0x000B6298 File Offset: 0x000B4498
		public byte[] ComputeHash(Stream inputStream)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("HashAlgorithm");
			}
			byte[] array = new byte[4096];
			for (int i = inputStream.Read(array, 0, 4096); i > 0; i = inputStream.Read(array, 0, 4096))
			{
				this.HashCore(array, 0, i);
			}
			this.HashValue = this.HashFinal();
			this.Initialize();
			return this.HashValue;
		}

		// Token: 0x0600380B RID: 14347 RVA: 0x000B6310 File Offset: 0x000B4510
		public static HashAlgorithm Create()
		{
			return HashAlgorithm.Create("System.Security.Cryptography.HashAlgorithm");
		}

		// Token: 0x0600380C RID: 14348 RVA: 0x000B631C File Offset: 0x000B451C
		public static HashAlgorithm Create(string hashName)
		{
			return (HashAlgorithm)CryptoConfig.CreateFromName(hashName);
		}

		// Token: 0x17000AA3 RID: 2723
		// (get) Token: 0x0600380D RID: 14349 RVA: 0x000B632C File Offset: 0x000B452C
		public virtual byte[] Hash
		{
			get
			{
				if (this.HashValue == null)
				{
					throw new CryptographicUnexpectedOperationException(Locale.GetText("No hash value computed."));
				}
				return this.HashValue;
			}
		}

		// Token: 0x0600380E RID: 14350
		protected abstract void HashCore(byte[] array, int ibStart, int cbSize);

		// Token: 0x0600380F RID: 14351
		protected abstract byte[] HashFinal();

		// Token: 0x17000AA4 RID: 2724
		// (get) Token: 0x06003810 RID: 14352 RVA: 0x000B6350 File Offset: 0x000B4550
		public virtual int HashSize
		{
			get
			{
				return this.HashSizeValue;
			}
		}

		// Token: 0x06003811 RID: 14353
		public abstract void Initialize();

		// Token: 0x06003812 RID: 14354 RVA: 0x000B6358 File Offset: 0x000B4558
		protected virtual void Dispose(bool disposing)
		{
			this.disposed = true;
		}

		// Token: 0x17000AA5 RID: 2725
		// (get) Token: 0x06003813 RID: 14355 RVA: 0x000B6364 File Offset: 0x000B4564
		public virtual int InputBlockSize
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000AA6 RID: 2726
		// (get) Token: 0x06003814 RID: 14356 RVA: 0x000B6368 File Offset: 0x000B4568
		public virtual int OutputBlockSize
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06003815 RID: 14357 RVA: 0x000B636C File Offset: 0x000B456C
		public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
		{
			if (inputBuffer == null)
			{
				throw new ArgumentNullException("inputBuffer");
			}
			if (inputOffset < 0)
			{
				throw new ArgumentOutOfRangeException("inputOffset", "< 0");
			}
			if (inputCount < 0)
			{
				throw new ArgumentException("inputCount");
			}
			if (inputOffset < 0 || inputOffset > inputBuffer.Length - inputCount)
			{
				throw new ArgumentException("inputBuffer");
			}
			if (outputBuffer != null)
			{
				if (outputOffset < 0)
				{
					throw new ArgumentOutOfRangeException("outputOffset", "< 0");
				}
				if (outputOffset > outputBuffer.Length - inputCount)
				{
					throw new ArgumentException("outputOffset + inputCount", Locale.GetText("Overflow"));
				}
			}
			this.HashCore(inputBuffer, inputOffset, inputCount);
			if (outputBuffer != null)
			{
				Buffer.BlockCopy(inputBuffer, inputOffset, outputBuffer, outputOffset, inputCount);
			}
			return inputCount;
		}

		// Token: 0x06003816 RID: 14358 RVA: 0x000B6430 File Offset: 0x000B4630
		public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			if (inputBuffer == null)
			{
				throw new ArgumentNullException("inputBuffer");
			}
			if (inputCount < 0)
			{
				throw new ArgumentException("inputCount");
			}
			if (inputOffset > inputBuffer.Length - inputCount)
			{
				throw new ArgumentException("inputOffset + inputCount", Locale.GetText("Overflow"));
			}
			byte[] array = new byte[inputCount];
			Buffer.BlockCopy(inputBuffer, inputOffset, array, 0, inputCount);
			this.HashCore(inputBuffer, inputOffset, inputCount);
			this.HashValue = this.HashFinal();
			this.Initialize();
			return array;
		}

		// Token: 0x0400185A RID: 6234
		protected internal byte[] HashValue;

		// Token: 0x0400185B RID: 6235
		protected int HashSizeValue;

		// Token: 0x0400185C RID: 6236
		protected int State;

		// Token: 0x0400185D RID: 6237
		private bool disposed;
	}
}
