﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005A8 RID: 1448
	[ComVisible(true)]
	public sealed class DESCryptoServiceProvider : DES
	{
		// Token: 0x060037BF RID: 14271 RVA: 0x000B51DC File Offset: 0x000B33DC
		public override ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new DESTransform(this, false, rgbKey, rgbIV);
		}

		// Token: 0x060037C0 RID: 14272 RVA: 0x000B51E8 File Offset: 0x000B33E8
		public override ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new DESTransform(this, true, rgbKey, rgbIV);
		}

		// Token: 0x060037C1 RID: 14273 RVA: 0x000B51F4 File Offset: 0x000B33F4
		public override void GenerateIV()
		{
			this.IVValue = KeyBuilder.IV(DESTransform.BLOCK_BYTE_SIZE);
		}

		// Token: 0x060037C2 RID: 14274 RVA: 0x000B5208 File Offset: 0x000B3408
		public override void GenerateKey()
		{
			this.KeyValue = DESTransform.GetStrongKey();
		}
	}
}
