﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005D6 RID: 1494
	[ComVisible(true)]
	public class RSAPKCS1KeyExchangeFormatter : AsymmetricKeyExchangeFormatter
	{
		// Token: 0x06003930 RID: 14640 RVA: 0x000C3660 File Offset: 0x000C1860
		public RSAPKCS1KeyExchangeFormatter()
		{
		}

		// Token: 0x06003931 RID: 14641 RVA: 0x000C3668 File Offset: 0x000C1868
		public RSAPKCS1KeyExchangeFormatter(AsymmetricAlgorithm key)
		{
			this.SetRSAKey(key);
		}

		// Token: 0x17000AD3 RID: 2771
		// (get) Token: 0x06003932 RID: 14642 RVA: 0x000C3678 File Offset: 0x000C1878
		// (set) Token: 0x06003933 RID: 14643 RVA: 0x000C3680 File Offset: 0x000C1880
		public RandomNumberGenerator Rng
		{
			get
			{
				return this.random;
			}
			set
			{
				this.random = value;
			}
		}

		// Token: 0x17000AD4 RID: 2772
		// (get) Token: 0x06003934 RID: 14644 RVA: 0x000C368C File Offset: 0x000C188C
		public override string Parameters
		{
			get
			{
				return "<enc:KeyEncryptionMethod enc:Algorithm=\"http://www.microsoft.com/xml/security/algorithm/PKCS1-v1.5-KeyEx\" xmlns:enc=\"http://www.microsoft.com/xml/security/encryption/v1.0\" />";
			}
		}

		// Token: 0x06003935 RID: 14645 RVA: 0x000C3694 File Offset: 0x000C1894
		public override byte[] CreateKeyExchange(byte[] rgbData)
		{
			if (rgbData == null)
			{
				throw new ArgumentNullException("rgbData");
			}
			if (this.rsa == null)
			{
				string text = Locale.GetText("No RSA key specified");
				throw new CryptographicUnexpectedOperationException(text);
			}
			if (this.random == null)
			{
				this.random = RandomNumberGenerator.Create();
			}
			return PKCS1.Encrypt_v15(this.rsa, this.random, rgbData);
		}

		// Token: 0x06003936 RID: 14646 RVA: 0x000C36F8 File Offset: 0x000C18F8
		public override byte[] CreateKeyExchange(byte[] rgbData, Type symAlgType)
		{
			return this.CreateKeyExchange(rgbData);
		}

		// Token: 0x06003937 RID: 14647 RVA: 0x000C3704 File Offset: 0x000C1904
		private void SetRSAKey(AsymmetricAlgorithm key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.rsa = (RSA)key;
		}

		// Token: 0x06003938 RID: 14648 RVA: 0x000C3724 File Offset: 0x000C1924
		public override void SetKey(AsymmetricAlgorithm key)
		{
			this.SetRSAKey(key);
		}

		// Token: 0x040018CA RID: 6346
		private RSA rsa;

		// Token: 0x040018CB RID: 6347
		private RandomNumberGenerator random;
	}
}
