﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005B3 RID: 1459
	[ComVisible(true)]
	public class HMACRIPEMD160 : HMAC
	{
		// Token: 0x06003828 RID: 14376 RVA: 0x000B67B0 File Offset: 0x000B49B0
		public HMACRIPEMD160() : this(KeyBuilder.Key(8))
		{
		}

		// Token: 0x06003829 RID: 14377 RVA: 0x000B67C0 File Offset: 0x000B49C0
		public HMACRIPEMD160(byte[] key)
		{
			base.HashName = "RIPEMD160";
			this.HashSizeValue = 160;
			this.Key = key;
		}
	}
}
