﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005D3 RID: 1491
	[ComVisible(true)]
	public class RSAOAEPKeyExchangeFormatter : AsymmetricKeyExchangeFormatter
	{
		// Token: 0x0600391E RID: 14622 RVA: 0x000C350C File Offset: 0x000C170C
		public RSAOAEPKeyExchangeFormatter()
		{
			this.rsa = null;
		}

		// Token: 0x0600391F RID: 14623 RVA: 0x000C351C File Offset: 0x000C171C
		public RSAOAEPKeyExchangeFormatter(AsymmetricAlgorithm key)
		{
			this.SetKey(key);
		}

		// Token: 0x17000ACE RID: 2766
		// (get) Token: 0x06003920 RID: 14624 RVA: 0x000C352C File Offset: 0x000C172C
		// (set) Token: 0x06003921 RID: 14625 RVA: 0x000C3534 File Offset: 0x000C1734
		public byte[] Parameter
		{
			get
			{
				return this.param;
			}
			set
			{
				this.param = value;
			}
		}

		// Token: 0x17000ACF RID: 2767
		// (get) Token: 0x06003922 RID: 14626 RVA: 0x000C3540 File Offset: 0x000C1740
		public override string Parameters
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000AD0 RID: 2768
		// (get) Token: 0x06003923 RID: 14627 RVA: 0x000C3544 File Offset: 0x000C1744
		// (set) Token: 0x06003924 RID: 14628 RVA: 0x000C354C File Offset: 0x000C174C
		public RandomNumberGenerator Rng
		{
			get
			{
				return this.random;
			}
			set
			{
				this.random = value;
			}
		}

		// Token: 0x06003925 RID: 14629 RVA: 0x000C3558 File Offset: 0x000C1758
		public override byte[] CreateKeyExchange(byte[] rgbData)
		{
			if (this.random == null)
			{
				this.random = RandomNumberGenerator.Create();
			}
			if (this.rsa == null)
			{
				string text = Locale.GetText("No RSA key specified");
				throw new CryptographicUnexpectedOperationException(text);
			}
			SHA1 hash = SHA1.Create();
			return PKCS1.Encrypt_OAEP(this.rsa, hash, this.random, rgbData);
		}

		// Token: 0x06003926 RID: 14630 RVA: 0x000C35B4 File Offset: 0x000C17B4
		public override byte[] CreateKeyExchange(byte[] rgbData, Type symAlgType)
		{
			return this.CreateKeyExchange(rgbData);
		}

		// Token: 0x06003927 RID: 14631 RVA: 0x000C35C0 File Offset: 0x000C17C0
		public override void SetKey(AsymmetricAlgorithm key)
		{
			this.rsa = (RSA)key;
		}

		// Token: 0x040018BD RID: 6333
		private RSA rsa;

		// Token: 0x040018BE RID: 6334
		private RandomNumberGenerator random;

		// Token: 0x040018BF RID: 6335
		private byte[] param;
	}
}
