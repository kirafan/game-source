﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x02000598 RID: 1432
	[ComVisible(true)]
	public abstract class AsymmetricSignatureFormatter
	{
		// Token: 0x0600374F RID: 14159
		public abstract void SetHashAlgorithm(string strName);

		// Token: 0x06003750 RID: 14160
		public abstract void SetKey(AsymmetricAlgorithm key);

		// Token: 0x06003751 RID: 14161
		public abstract byte[] CreateSignature(byte[] rgbHash);

		// Token: 0x06003752 RID: 14162 RVA: 0x000B2BDC File Offset: 0x000B0DDC
		public virtual byte[] CreateSignature(HashAlgorithm hash)
		{
			if (hash == null)
			{
				throw new ArgumentNullException("hash");
			}
			this.SetHashAlgorithm(hash.ToString());
			return this.CreateSignature(hash.Hash);
		}
	}
}
