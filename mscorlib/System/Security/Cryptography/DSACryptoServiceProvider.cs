﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005AA RID: 1450
	[ComVisible(true)]
	public sealed class DSACryptoServiceProvider : DSA, ICspAsymmetricAlgorithm
	{
		// Token: 0x060037CD RID: 14285 RVA: 0x000B55D4 File Offset: 0x000B37D4
		public DSACryptoServiceProvider() : this(1024, null)
		{
		}

		// Token: 0x060037CE RID: 14286 RVA: 0x000B55E4 File Offset: 0x000B37E4
		public DSACryptoServiceProvider(CspParameters parameters) : this(1024, parameters)
		{
		}

		// Token: 0x060037CF RID: 14287 RVA: 0x000B55F4 File Offset: 0x000B37F4
		public DSACryptoServiceProvider(int dwKeySize) : this(dwKeySize, null)
		{
		}

		// Token: 0x060037D0 RID: 14288 RVA: 0x000B5600 File Offset: 0x000B3800
		public DSACryptoServiceProvider(int dwKeySize, CspParameters parameters)
		{
			this.LegalKeySizesValue = new KeySizes[1];
			this.LegalKeySizesValue[0] = new KeySizes(512, 1024, 64);
			this.KeySize = dwKeySize;
			this.dsa = new DSAManaged(dwKeySize);
			this.dsa.KeyGenerated += this.OnKeyGenerated;
			this.persistKey = (parameters != null);
			if (parameters == null)
			{
				parameters = new CspParameters(13);
				if (DSACryptoServiceProvider.useMachineKeyStore)
				{
					parameters.Flags |= CspProviderFlags.UseMachineKeyStore;
				}
				this.store = new KeyPairPersistence(parameters);
			}
			else
			{
				this.store = new KeyPairPersistence(parameters);
				this.store.Load();
				if (this.store.KeyValue != null)
				{
					this.persisted = true;
					this.FromXmlString(this.store.KeyValue);
				}
			}
		}

		// Token: 0x060037D2 RID: 14290 RVA: 0x000B56F4 File Offset: 0x000B38F4
		~DSACryptoServiceProvider()
		{
			this.Dispose(false);
		}

		// Token: 0x17000A96 RID: 2710
		// (get) Token: 0x060037D3 RID: 14291 RVA: 0x000B5730 File Offset: 0x000B3930
		public override string KeyExchangeAlgorithm
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000A97 RID: 2711
		// (get) Token: 0x060037D4 RID: 14292 RVA: 0x000B5734 File Offset: 0x000B3934
		public override int KeySize
		{
			get
			{
				return this.dsa.KeySize;
			}
		}

		// Token: 0x17000A98 RID: 2712
		// (get) Token: 0x060037D5 RID: 14293 RVA: 0x000B5744 File Offset: 0x000B3944
		// (set) Token: 0x060037D6 RID: 14294 RVA: 0x000B574C File Offset: 0x000B394C
		public bool PersistKeyInCsp
		{
			get
			{
				return this.persistKey;
			}
			set
			{
				this.persistKey = value;
			}
		}

		// Token: 0x17000A99 RID: 2713
		// (get) Token: 0x060037D7 RID: 14295 RVA: 0x000B5758 File Offset: 0x000B3958
		[ComVisible(false)]
		public bool PublicOnly
		{
			get
			{
				return this.dsa.PublicOnly;
			}
		}

		// Token: 0x17000A9A RID: 2714
		// (get) Token: 0x060037D8 RID: 14296 RVA: 0x000B5768 File Offset: 0x000B3968
		public override string SignatureAlgorithm
		{
			get
			{
				return "http://www.w3.org/2000/09/xmldsig#dsa-sha1";
			}
		}

		// Token: 0x17000A9B RID: 2715
		// (get) Token: 0x060037D9 RID: 14297 RVA: 0x000B5770 File Offset: 0x000B3970
		// (set) Token: 0x060037DA RID: 14298 RVA: 0x000B5778 File Offset: 0x000B3978
		public static bool UseMachineKeyStore
		{
			get
			{
				return DSACryptoServiceProvider.useMachineKeyStore;
			}
			set
			{
				DSACryptoServiceProvider.useMachineKeyStore = value;
			}
		}

		// Token: 0x060037DB RID: 14299 RVA: 0x000B5780 File Offset: 0x000B3980
		public override DSAParameters ExportParameters(bool includePrivateParameters)
		{
			if (includePrivateParameters && !this.privateKeyExportable)
			{
				throw new CryptographicException(Locale.GetText("Cannot export private key"));
			}
			return this.dsa.ExportParameters(includePrivateParameters);
		}

		// Token: 0x060037DC RID: 14300 RVA: 0x000B57B0 File Offset: 0x000B39B0
		public override void ImportParameters(DSAParameters parameters)
		{
			this.dsa.ImportParameters(parameters);
		}

		// Token: 0x060037DD RID: 14301 RVA: 0x000B57C0 File Offset: 0x000B39C0
		public override byte[] CreateSignature(byte[] rgbHash)
		{
			return this.dsa.CreateSignature(rgbHash);
		}

		// Token: 0x060037DE RID: 14302 RVA: 0x000B57D0 File Offset: 0x000B39D0
		public byte[] SignData(byte[] buffer)
		{
			HashAlgorithm hashAlgorithm = SHA1.Create();
			byte[] rgbHash = hashAlgorithm.ComputeHash(buffer);
			return this.dsa.CreateSignature(rgbHash);
		}

		// Token: 0x060037DF RID: 14303 RVA: 0x000B57F8 File Offset: 0x000B39F8
		public byte[] SignData(byte[] buffer, int offset, int count)
		{
			HashAlgorithm hashAlgorithm = SHA1.Create();
			byte[] rgbHash = hashAlgorithm.ComputeHash(buffer, offset, count);
			return this.dsa.CreateSignature(rgbHash);
		}

		// Token: 0x060037E0 RID: 14304 RVA: 0x000B5824 File Offset: 0x000B3A24
		public byte[] SignData(Stream inputStream)
		{
			HashAlgorithm hashAlgorithm = SHA1.Create();
			byte[] rgbHash = hashAlgorithm.ComputeHash(inputStream);
			return this.dsa.CreateSignature(rgbHash);
		}

		// Token: 0x060037E1 RID: 14305 RVA: 0x000B584C File Offset: 0x000B3A4C
		public byte[] SignHash(byte[] rgbHash, string str)
		{
			if (string.Compare(str, "SHA1", true, CultureInfo.InvariantCulture) != 0)
			{
				throw new CryptographicException(Locale.GetText("Only SHA1 is supported."));
			}
			return this.dsa.CreateSignature(rgbHash);
		}

		// Token: 0x060037E2 RID: 14306 RVA: 0x000B588C File Offset: 0x000B3A8C
		public bool VerifyData(byte[] rgbData, byte[] rgbSignature)
		{
			HashAlgorithm hashAlgorithm = SHA1.Create();
			byte[] rgbHash = hashAlgorithm.ComputeHash(rgbData);
			return this.dsa.VerifySignature(rgbHash, rgbSignature);
		}

		// Token: 0x060037E3 RID: 14307 RVA: 0x000B58B4 File Offset: 0x000B3AB4
		public bool VerifyHash(byte[] rgbHash, string str, byte[] rgbSignature)
		{
			if (str == null)
			{
				str = "SHA1";
			}
			if (string.Compare(str, "SHA1", true, CultureInfo.InvariantCulture) != 0)
			{
				throw new CryptographicException(Locale.GetText("Only SHA1 is supported."));
			}
			return this.dsa.VerifySignature(rgbHash, rgbSignature);
		}

		// Token: 0x060037E4 RID: 14308 RVA: 0x000B5904 File Offset: 0x000B3B04
		public override bool VerifySignature(byte[] rgbHash, byte[] rgbSignature)
		{
			return this.dsa.VerifySignature(rgbHash, rgbSignature);
		}

		// Token: 0x060037E5 RID: 14309 RVA: 0x000B5914 File Offset: 0x000B3B14
		protected override void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				if (this.persisted && !this.persistKey)
				{
					this.store.Remove();
				}
				if (this.dsa != null)
				{
					this.dsa.Clear();
				}
				this.m_disposed = true;
			}
		}

		// Token: 0x060037E6 RID: 14310 RVA: 0x000B596C File Offset: 0x000B3B6C
		private void OnKeyGenerated(object sender, EventArgs e)
		{
			if (this.persistKey && !this.persisted)
			{
				this.store.KeyValue = this.ToXmlString(!this.dsa.PublicOnly);
				this.store.Save();
				this.persisted = true;
			}
		}

		// Token: 0x17000A9C RID: 2716
		// (get) Token: 0x060037E7 RID: 14311 RVA: 0x000B59C0 File Offset: 0x000B3BC0
		[MonoTODO("call into KeyPairPersistence to get details")]
		[ComVisible(false)]
		public CspKeyContainerInfo CspKeyContainerInfo
		{
			get
			{
				return null;
			}
		}

		// Token: 0x060037E8 RID: 14312 RVA: 0x000B59C4 File Offset: 0x000B3BC4
		[ComVisible(false)]
		public byte[] ExportCspBlob(bool includePrivateParameters)
		{
			byte[] result;
			if (includePrivateParameters)
			{
				result = CryptoConvert.ToCapiPrivateKeyBlob(this);
			}
			else
			{
				result = CryptoConvert.ToCapiPublicKeyBlob(this);
			}
			return result;
		}

		// Token: 0x060037E9 RID: 14313 RVA: 0x000B59F0 File Offset: 0x000B3BF0
		[ComVisible(false)]
		public void ImportCspBlob(byte[] keyBlob)
		{
			if (keyBlob == null)
			{
				throw new ArgumentNullException("keyBlob");
			}
			DSA dsa = CryptoConvert.FromCapiKeyBlobDSA(keyBlob);
			if (dsa is DSACryptoServiceProvider)
			{
				DSAParameters parameters = dsa.ExportParameters(!(dsa as DSACryptoServiceProvider).PublicOnly);
				this.ImportParameters(parameters);
			}
			else
			{
				try
				{
					DSAParameters parameters2 = dsa.ExportParameters(true);
					this.ImportParameters(parameters2);
				}
				catch
				{
					DSAParameters parameters3 = dsa.ExportParameters(false);
					this.ImportParameters(parameters3);
				}
			}
		}

		// Token: 0x0400183F RID: 6207
		private const int PROV_DSS_DH = 13;

		// Token: 0x04001840 RID: 6208
		private KeyPairPersistence store;

		// Token: 0x04001841 RID: 6209
		private bool persistKey;

		// Token: 0x04001842 RID: 6210
		private bool persisted;

		// Token: 0x04001843 RID: 6211
		private bool privateKeyExportable = true;

		// Token: 0x04001844 RID: 6212
		private bool m_disposed;

		// Token: 0x04001845 RID: 6213
		private DSAManaged dsa;

		// Token: 0x04001846 RID: 6214
		private static bool useMachineKeyStore;
	}
}
