﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005C8 RID: 1480
	[ComVisible(true)]
	public class Rfc2898DeriveBytes : DeriveBytes
	{
		// Token: 0x0600389A RID: 14490 RVA: 0x000B929C File Offset: 0x000B749C
		public Rfc2898DeriveBytes(string password, byte[] salt) : this(password, salt, 1000)
		{
		}

		// Token: 0x0600389B RID: 14491 RVA: 0x000B92AC File Offset: 0x000B74AC
		public Rfc2898DeriveBytes(string password, byte[] salt, int iterations)
		{
			if (password == null)
			{
				throw new ArgumentNullException("password");
			}
			this.Salt = salt;
			this.IterationCount = iterations;
			this._hmac = new HMACSHA1(Encoding.UTF8.GetBytes(password));
		}

		// Token: 0x0600389C RID: 14492 RVA: 0x000B92F4 File Offset: 0x000B74F4
		public Rfc2898DeriveBytes(byte[] password, byte[] salt, int iterations)
		{
			if (password == null)
			{
				throw new ArgumentNullException("password");
			}
			this.Salt = salt;
			this.IterationCount = iterations;
			this._hmac = new HMACSHA1(password);
		}

		// Token: 0x0600389D RID: 14493 RVA: 0x000B9334 File Offset: 0x000B7534
		public Rfc2898DeriveBytes(string password, int saltSize) : this(password, saltSize, 1000)
		{
		}

		// Token: 0x0600389E RID: 14494 RVA: 0x000B9344 File Offset: 0x000B7544
		public Rfc2898DeriveBytes(string password, int saltSize, int iterations)
		{
			if (password == null)
			{
				throw new ArgumentNullException("password");
			}
			if (saltSize < 0)
			{
				throw new ArgumentOutOfRangeException("invalid salt length");
			}
			this.Salt = KeyBuilder.Key(saltSize);
			this.IterationCount = iterations;
			this._hmac = new HMACSHA1(Encoding.UTF8.GetBytes(password));
		}

		// Token: 0x17000ABF RID: 2751
		// (get) Token: 0x0600389F RID: 14495 RVA: 0x000B93A4 File Offset: 0x000B75A4
		// (set) Token: 0x060038A0 RID: 14496 RVA: 0x000B93AC File Offset: 0x000B75AC
		public int IterationCount
		{
			get
			{
				return this._iteration;
			}
			set
			{
				if (value < 1)
				{
					throw new ArgumentOutOfRangeException("IterationCount < 1");
				}
				this._iteration = value;
			}
		}

		// Token: 0x17000AC0 RID: 2752
		// (get) Token: 0x060038A1 RID: 14497 RVA: 0x000B93C8 File Offset: 0x000B75C8
		// (set) Token: 0x060038A2 RID: 14498 RVA: 0x000B93DC File Offset: 0x000B75DC
		public byte[] Salt
		{
			get
			{
				return (byte[])this._salt.Clone();
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Salt");
				}
				if (value.Length < 8)
				{
					throw new ArgumentException("Salt < 8 bytes");
				}
				this._salt = (byte[])value.Clone();
			}
		}

		// Token: 0x060038A3 RID: 14499 RVA: 0x000B9420 File Offset: 0x000B7620
		private byte[] F(byte[] s, int c, int i)
		{
			s[s.Length - 4] = (byte)(i >> 24);
			s[s.Length - 3] = (byte)(i >> 16);
			s[s.Length - 2] = (byte)(i >> 8);
			s[s.Length - 1] = (byte)i;
			byte[] array = this._hmac.ComputeHash(s);
			byte[] buffer = array;
			for (int j = 1; j < c; j++)
			{
				byte[] array2 = this._hmac.ComputeHash(buffer);
				for (int k = 0; k < 20; k++)
				{
					array[k] ^= array2[k];
				}
				buffer = array2;
			}
			return array;
		}

		// Token: 0x060038A4 RID: 14500 RVA: 0x000B94B0 File Offset: 0x000B76B0
		public override byte[] GetBytes(int cb)
		{
			if (cb < 1)
			{
				throw new ArgumentOutOfRangeException("cb");
			}
			int num = cb / 20;
			int num2 = cb % 20;
			if (num2 != 0)
			{
				num++;
			}
			byte[] array = new byte[cb];
			int num3 = 0;
			if (this._pos > 0)
			{
				int num4 = Math.Min(20 - this._pos, cb);
				Buffer.BlockCopy(this._buffer, this._pos, array, 0, num4);
				if (num4 >= cb)
				{
					return array;
				}
				this._pos = 0;
				num3 = num4;
			}
			byte[] array2 = new byte[this._salt.Length + 4];
			Buffer.BlockCopy(this._salt, 0, array2, 0, this._salt.Length);
			for (int i = 1; i <= num; i++)
			{
				this._buffer = this.F(array2, this._iteration, ++this._f);
				int num5 = (i != num) ? 20 : (array.Length - num3);
				Buffer.BlockCopy(this._buffer, this._pos, array, num3, num5);
				num3 += this._pos + num5;
				this._pos = ((num5 != 20) ? num5 : 0);
			}
			return array;
		}

		// Token: 0x060038A5 RID: 14501 RVA: 0x000B95E8 File Offset: 0x000B77E8
		public override void Reset()
		{
			this._buffer = null;
			this._pos = 0;
			this._f = 0;
		}

		// Token: 0x04001893 RID: 6291
		private const int defaultIterations = 1000;

		// Token: 0x04001894 RID: 6292
		private int _iteration;

		// Token: 0x04001895 RID: 6293
		private byte[] _salt;

		// Token: 0x04001896 RID: 6294
		private HMACSHA1 _hmac;

		// Token: 0x04001897 RID: 6295
		private byte[] _buffer;

		// Token: 0x04001898 RID: 6296
		private int _pos;

		// Token: 0x04001899 RID: 6297
		private int _f;
	}
}
