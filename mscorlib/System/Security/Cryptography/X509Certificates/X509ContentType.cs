﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020005EE RID: 1518
	[ComVisible(true)]
	public enum X509ContentType
	{
		// Token: 0x04001920 RID: 6432
		Unknown,
		// Token: 0x04001921 RID: 6433
		Cert,
		// Token: 0x04001922 RID: 6434
		SerializedCert,
		// Token: 0x04001923 RID: 6435
		Pfx,
		// Token: 0x04001924 RID: 6436
		SerializedStore,
		// Token: 0x04001925 RID: 6437
		Pkcs7,
		// Token: 0x04001926 RID: 6438
		Authenticode,
		// Token: 0x04001927 RID: 6439
		Pkcs12 = 3
	}
}
