﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;
using Mono.Security.Authenticode;
using Mono.Security.X509;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020005EC RID: 1516
	[MonoTODO("X509ContentType.SerializedCert isn't supported (anywhere in the class)")]
	[ComVisible(true)]
	[Serializable]
	public class X509Certificate : ISerializable, IDeserializationCallback
	{
		// Token: 0x060039D0 RID: 14800 RVA: 0x000C6628 File Offset: 0x000C4828
		internal X509Certificate(byte[] data, bool dates)
		{
			if (data != null)
			{
				this.Import(data, null, X509KeyStorageFlags.DefaultKeySet);
				this.hideDates = !dates;
			}
		}

		// Token: 0x060039D1 RID: 14801 RVA: 0x000C6654 File Offset: 0x000C4854
		public X509Certificate(byte[] data) : this(data, true)
		{
		}

		// Token: 0x060039D2 RID: 14802 RVA: 0x000C6660 File Offset: 0x000C4860
		public X509Certificate(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				throw new ArgumentException("Invalid handle.");
			}
			this.InitFromHandle(handle);
		}

		// Token: 0x060039D3 RID: 14803 RVA: 0x000C6698 File Offset: 0x000C4898
		public X509Certificate(X509Certificate cert)
		{
			if (cert == null)
			{
				throw new ArgumentNullException("cert");
			}
			if (cert != null)
			{
				byte[] rawCertData = cert.GetRawCertData();
				if (rawCertData != null)
				{
					this.x509 = new X509Certificate(rawCertData);
				}
				this.hideDates = false;
			}
		}

		// Token: 0x060039D4 RID: 14804 RVA: 0x000C66E4 File Offset: 0x000C48E4
		public X509Certificate()
		{
		}

		// Token: 0x060039D5 RID: 14805 RVA: 0x000C66EC File Offset: 0x000C48EC
		public X509Certificate(byte[] rawData, string password)
		{
			this.Import(rawData, password, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x060039D6 RID: 14806 RVA: 0x000C6700 File Offset: 0x000C4900
		[MonoTODO("SecureString support is incomplete")]
		public X509Certificate(byte[] rawData, SecureString password)
		{
			this.Import(rawData, password, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x060039D7 RID: 14807 RVA: 0x000C6714 File Offset: 0x000C4914
		public X509Certificate(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(rawData, password, keyStorageFlags);
		}

		// Token: 0x060039D8 RID: 14808 RVA: 0x000C6728 File Offset: 0x000C4928
		[MonoTODO("SecureString support is incomplete")]
		public X509Certificate(byte[] rawData, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(rawData, password, keyStorageFlags);
		}

		// Token: 0x060039D9 RID: 14809 RVA: 0x000C673C File Offset: 0x000C493C
		public X509Certificate(string fileName)
		{
			this.Import(fileName, null, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x060039DA RID: 14810 RVA: 0x000C6750 File Offset: 0x000C4950
		public X509Certificate(string fileName, string password)
		{
			this.Import(fileName, password, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x060039DB RID: 14811 RVA: 0x000C6764 File Offset: 0x000C4964
		[MonoTODO("SecureString support is incomplete")]
		public X509Certificate(string fileName, SecureString password)
		{
			this.Import(fileName, password, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x060039DC RID: 14812 RVA: 0x000C6778 File Offset: 0x000C4978
		public X509Certificate(string fileName, string password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(fileName, password, keyStorageFlags);
		}

		// Token: 0x060039DD RID: 14813 RVA: 0x000C678C File Offset: 0x000C498C
		[MonoTODO("SecureString support is incomplete")]
		public X509Certificate(string fileName, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(fileName, password, keyStorageFlags);
		}

		// Token: 0x060039DE RID: 14814 RVA: 0x000C67A0 File Offset: 0x000C49A0
		public X509Certificate(SerializationInfo info, StreamingContext context)
		{
			byte[] rawData = (byte[])info.GetValue("RawData", typeof(byte[]));
			this.Import(rawData, null, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x060039DF RID: 14815 RVA: 0x000C67D8 File Offset: 0x000C49D8
		void IDeserializationCallback.OnDeserialization(object sender)
		{
		}

		// Token: 0x060039E0 RID: 14816 RVA: 0x000C67DC File Offset: 0x000C49DC
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("RawData", this.x509.RawData);
		}

		// Token: 0x060039E1 RID: 14817 RVA: 0x000C67F4 File Offset: 0x000C49F4
		private string tostr(byte[] data)
		{
			if (data != null)
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < data.Length; i++)
				{
					stringBuilder.Append(data[i].ToString("X2"));
				}
				return stringBuilder.ToString();
			}
			return null;
		}

		// Token: 0x060039E2 RID: 14818 RVA: 0x000C6844 File Offset: 0x000C4A44
		public static X509Certificate CreateFromCertFile(string filename)
		{
			byte[] data = X509Certificate.Load(filename);
			return new X509Certificate(data);
		}

		// Token: 0x060039E3 RID: 14819 RVA: 0x000C6860 File Offset: 0x000C4A60
		[MonoTODO("Incomplete - minimal validation in this version")]
		public static X509Certificate CreateFromSignedFile(string filename)
		{
			try
			{
				AuthenticodeDeformatter authenticodeDeformatter = new AuthenticodeDeformatter(filename);
				if (authenticodeDeformatter.SigningCertificate != null)
				{
					return new X509Certificate(authenticodeDeformatter.SigningCertificate.RawData);
				}
			}
			catch (SecurityException)
			{
				throw;
			}
			catch (Exception inner)
			{
				string text = Locale.GetText("Couldn't extract digital signature from {0}.", new object[]
				{
					filename
				});
				throw new COMException(text, inner);
			}
			throw new CryptographicException(Locale.GetText("{0} isn't signed.", new object[]
			{
				filename
			}));
		}

		// Token: 0x060039E4 RID: 14820 RVA: 0x000C6914 File Offset: 0x000C4B14
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		private void InitFromHandle(IntPtr handle)
		{
			if (handle != IntPtr.Zero)
			{
				X509Certificate.CertificateContext certificateContext = (X509Certificate.CertificateContext)Marshal.PtrToStructure(handle, typeof(X509Certificate.CertificateContext));
				byte[] array = new byte[certificateContext.cbCertEncoded];
				Marshal.Copy(certificateContext.pbCertEncoded, array, 0, (int)certificateContext.cbCertEncoded);
				this.x509 = new X509Certificate(array);
			}
		}

		// Token: 0x060039E5 RID: 14821 RVA: 0x000C6978 File Offset: 0x000C4B78
		public virtual bool Equals(X509Certificate other)
		{
			if (other == null)
			{
				return false;
			}
			if (other.x509 == null)
			{
				if (this.x509 == null)
				{
					return true;
				}
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			else
			{
				byte[] rawData = other.x509.RawData;
				if (rawData == null)
				{
					return this.x509 == null || this.x509.RawData == null;
				}
				if (this.x509 == null)
				{
					return false;
				}
				if (this.x509.RawData == null)
				{
					return false;
				}
				if (rawData.Length == this.x509.RawData.Length)
				{
					for (int i = 0; i < rawData.Length; i++)
					{
						if (rawData[i] != this.x509.RawData[i])
						{
							return false;
						}
					}
					return true;
				}
				return false;
			}
		}

		// Token: 0x060039E6 RID: 14822 RVA: 0x000C6A48 File Offset: 0x000C4C48
		public virtual byte[] GetCertHash()
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			if (this.cachedCertificateHash == null && this.x509 != null)
			{
				SHA1 sha = SHA1.Create();
				this.cachedCertificateHash = sha.ComputeHash(this.x509.RawData);
			}
			return this.cachedCertificateHash;
		}

		// Token: 0x060039E7 RID: 14823 RVA: 0x000C6AAC File Offset: 0x000C4CAC
		public virtual string GetCertHashString()
		{
			return this.tostr(this.GetCertHash());
		}

		// Token: 0x060039E8 RID: 14824 RVA: 0x000C6ABC File Offset: 0x000C4CBC
		public virtual string GetEffectiveDateString()
		{
			if (this.hideDates)
			{
				return null;
			}
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			return this.x509.ValidFrom.ToLocalTime().ToString();
		}

		// Token: 0x060039E9 RID: 14825 RVA: 0x000C6B0C File Offset: 0x000C4D0C
		public virtual string GetExpirationDateString()
		{
			if (this.hideDates)
			{
				return null;
			}
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			return this.x509.ValidUntil.ToLocalTime().ToString();
		}

		// Token: 0x060039EA RID: 14826 RVA: 0x000C6B5C File Offset: 0x000C4D5C
		public virtual string GetFormat()
		{
			return "X509";
		}

		// Token: 0x060039EB RID: 14827 RVA: 0x000C6B64 File Offset: 0x000C4D64
		public override int GetHashCode()
		{
			if (this.x509 == null)
			{
				return 0;
			}
			if (this.cachedCertificateHash == null)
			{
				this.GetCertHash();
			}
			if (this.cachedCertificateHash != null && this.cachedCertificateHash.Length >= 4)
			{
				return (int)this.cachedCertificateHash[0] << 24 | (int)this.cachedCertificateHash[1] << 16 | (int)this.cachedCertificateHash[2] << 8 | (int)this.cachedCertificateHash[3];
			}
			return 0;
		}

		// Token: 0x060039EC RID: 14828 RVA: 0x000C6BD8 File Offset: 0x000C4DD8
		[Obsolete("Use the Issuer property.")]
		public virtual string GetIssuerName()
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			return this.x509.IssuerName;
		}

		// Token: 0x060039ED RID: 14829 RVA: 0x000C6C0C File Offset: 0x000C4E0C
		public virtual string GetKeyAlgorithm()
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			return this.x509.KeyAlgorithm;
		}

		// Token: 0x060039EE RID: 14830 RVA: 0x000C6C40 File Offset: 0x000C4E40
		public virtual byte[] GetKeyAlgorithmParameters()
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			byte[] keyAlgorithmParameters = this.x509.KeyAlgorithmParameters;
			if (keyAlgorithmParameters == null)
			{
				throw new CryptographicException(Locale.GetText("Parameters not part of the certificate"));
			}
			return keyAlgorithmParameters;
		}

		// Token: 0x060039EF RID: 14831 RVA: 0x000C6C8C File Offset: 0x000C4E8C
		public virtual string GetKeyAlgorithmParametersString()
		{
			return this.tostr(this.GetKeyAlgorithmParameters());
		}

		// Token: 0x060039F0 RID: 14832 RVA: 0x000C6C9C File Offset: 0x000C4E9C
		[Obsolete("Use the Subject property.")]
		public virtual string GetName()
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			return this.x509.SubjectName;
		}

		// Token: 0x060039F1 RID: 14833 RVA: 0x000C6CD0 File Offset: 0x000C4ED0
		public virtual byte[] GetPublicKey()
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			return this.x509.PublicKey;
		}

		// Token: 0x060039F2 RID: 14834 RVA: 0x000C6D04 File Offset: 0x000C4F04
		public virtual string GetPublicKeyString()
		{
			return this.tostr(this.GetPublicKey());
		}

		// Token: 0x060039F3 RID: 14835 RVA: 0x000C6D14 File Offset: 0x000C4F14
		public virtual byte[] GetRawCertData()
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			return this.x509.RawData;
		}

		// Token: 0x060039F4 RID: 14836 RVA: 0x000C6D48 File Offset: 0x000C4F48
		public virtual string GetRawCertDataString()
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			return this.tostr(this.x509.RawData);
		}

		// Token: 0x060039F5 RID: 14837 RVA: 0x000C6D84 File Offset: 0x000C4F84
		public virtual byte[] GetSerialNumber()
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			return this.x509.SerialNumber;
		}

		// Token: 0x060039F6 RID: 14838 RVA: 0x000C6DB8 File Offset: 0x000C4FB8
		public virtual string GetSerialNumberString()
		{
			byte[] serialNumber = this.GetSerialNumber();
			Array.Reverse(serialNumber);
			return this.tostr(serialNumber);
		}

		// Token: 0x060039F7 RID: 14839 RVA: 0x000C6DDC File Offset: 0x000C4FDC
		public override string ToString()
		{
			return base.ToString();
		}

		// Token: 0x060039F8 RID: 14840 RVA: 0x000C6DE4 File Offset: 0x000C4FE4
		public virtual string ToString(bool fVerbose)
		{
			if (!fVerbose || this.x509 == null)
			{
				return base.ToString();
			}
			string newLine = Environment.NewLine;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("[Subject]{0}  {1}{0}{0}", newLine, this.Subject);
			stringBuilder.AppendFormat("[Issuer]{0}  {1}{0}{0}", newLine, this.Issuer);
			stringBuilder.AppendFormat("[Not Before]{0}  {1}{0}{0}", newLine, this.GetEffectiveDateString());
			stringBuilder.AppendFormat("[Not After]{0}  {1}{0}{0}", newLine, this.GetExpirationDateString());
			stringBuilder.AppendFormat("[Thumbprint]{0}  {1}{0}", newLine, this.GetCertHashString());
			stringBuilder.Append(newLine);
			return stringBuilder.ToString();
		}

		// Token: 0x060039F9 RID: 14841 RVA: 0x000C6E84 File Offset: 0x000C5084
		private static byte[] Load(string fileName)
		{
			byte[] array = null;
			using (FileStream fileStream = File.OpenRead(fileName))
			{
				array = new byte[fileStream.Length];
				fileStream.Read(array, 0, array.Length);
				fileStream.Close();
			}
			return array;
		}

		// Token: 0x17000AE7 RID: 2791
		// (get) Token: 0x060039FA RID: 14842 RVA: 0x000C6EE8 File Offset: 0x000C50E8
		public string Issuer
		{
			get
			{
				if (this.x509 == null)
				{
					throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
				}
				if (this.issuer_name == null)
				{
					this.issuer_name = X501.ToString(this.x509.GetIssuerName(), true, ", ", true);
				}
				return this.issuer_name;
			}
		}

		// Token: 0x17000AE8 RID: 2792
		// (get) Token: 0x060039FB RID: 14843 RVA: 0x000C6F40 File Offset: 0x000C5140
		public string Subject
		{
			get
			{
				if (this.x509 == null)
				{
					throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
				}
				if (this.subject_name == null)
				{
					this.subject_name = X501.ToString(this.x509.GetSubjectName(), true, ", ", true);
				}
				return this.subject_name;
			}
		}

		// Token: 0x17000AE9 RID: 2793
		// (get) Token: 0x060039FC RID: 14844 RVA: 0x000C6F98 File Offset: 0x000C5198
		[ComVisible(false)]
		public IntPtr Handle
		{
			get
			{
				return IntPtr.Zero;
			}
		}

		// Token: 0x060039FD RID: 14845 RVA: 0x000C6FA0 File Offset: 0x000C51A0
		[ComVisible(false)]
		public override bool Equals(object obj)
		{
			X509Certificate x509Certificate = obj as X509Certificate;
			return x509Certificate != null && this.Equals(x509Certificate);
		}

		// Token: 0x060039FE RID: 14846 RVA: 0x000C6FC4 File Offset: 0x000C51C4
		[MonoTODO("X509ContentType.Pfx/Pkcs12 and SerializedCert are not supported")]
		[ComVisible(false)]
		public virtual byte[] Export(X509ContentType contentType)
		{
			return this.Export(contentType, null);
		}

		// Token: 0x060039FF RID: 14847 RVA: 0x000C6FD0 File Offset: 0x000C51D0
		[MonoTODO("X509ContentType.Pfx/Pkcs12 and SerializedCert are not supported")]
		[ComVisible(false)]
		public virtual byte[] Export(X509ContentType contentType, string password)
		{
			byte[] password2 = (password != null) ? Encoding.UTF8.GetBytes(password) : null;
			return this.Export(contentType, password2);
		}

		// Token: 0x06003A00 RID: 14848 RVA: 0x000C7000 File Offset: 0x000C5200
		[MonoTODO("X509ContentType.Pfx/Pkcs12 and SerializedCert are not supported. SecureString support is incomplete.")]
		public virtual byte[] Export(X509ContentType contentType, SecureString password)
		{
			byte[] password2 = (password != null) ? password.GetBuffer() : null;
			return this.Export(contentType, password2);
		}

		// Token: 0x06003A01 RID: 14849 RVA: 0x000C7028 File Offset: 0x000C5228
		internal byte[] Export(X509ContentType contentType, byte[] password)
		{
			if (this.x509 == null)
			{
				throw new CryptographicException(Locale.GetText("Certificate instance is empty."));
			}
			byte[] rawData;
			try
			{
				switch (contentType)
				{
				case X509ContentType.Cert:
					rawData = this.x509.RawData;
					break;
				case X509ContentType.SerializedCert:
					throw new NotSupportedException();
				case X509ContentType.Pfx:
					throw new NotSupportedException();
				default:
				{
					string text = Locale.GetText("This certificate format '{0}' cannot be exported.", new object[]
					{
						contentType
					});
					throw new CryptographicException(text);
				}
				}
			}
			finally
			{
				if (password != null)
				{
					Array.Clear(password, 0, password.Length);
				}
			}
			return rawData;
		}

		// Token: 0x06003A02 RID: 14850 RVA: 0x000C70DC File Offset: 0x000C52DC
		[ComVisible(false)]
		public virtual void Import(byte[] rawData)
		{
			this.Import(rawData, null, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06003A03 RID: 14851 RVA: 0x000C70E8 File Offset: 0x000C52E8
		[MonoTODO("missing KeyStorageFlags support")]
		[ComVisible(false)]
		public virtual void Import(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Reset();
			if (password == null)
			{
				try
				{
					this.x509 = new X509Certificate(rawData);
				}
				catch (Exception inner)
				{
					try
					{
						PKCS12 pkcs = new PKCS12(rawData);
						if (pkcs.Certificates.Count > 0)
						{
							this.x509 = pkcs.Certificates[0];
						}
						else
						{
							this.x509 = null;
						}
					}
					catch
					{
						string text = Locale.GetText("Unable to decode certificate.");
						throw new CryptographicException(text, inner);
					}
				}
			}
			else
			{
				try
				{
					PKCS12 pkcs2 = new PKCS12(rawData, password);
					if (pkcs2.Certificates.Count > 0)
					{
						this.x509 = pkcs2.Certificates[0];
					}
					else
					{
						this.x509 = null;
					}
				}
				catch
				{
					this.x509 = new X509Certificate(rawData);
				}
			}
		}

		// Token: 0x06003A04 RID: 14852 RVA: 0x000C720C File Offset: 0x000C540C
		[MonoTODO("SecureString support is incomplete")]
		public virtual void Import(byte[] rawData, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(rawData, null, keyStorageFlags);
		}

		// Token: 0x06003A05 RID: 14853 RVA: 0x000C7218 File Offset: 0x000C5418
		[ComVisible(false)]
		public virtual void Import(string fileName)
		{
			byte[] rawData = X509Certificate.Load(fileName);
			this.Import(rawData, null, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06003A06 RID: 14854 RVA: 0x000C7238 File Offset: 0x000C5438
		[ComVisible(false)]
		[MonoTODO("missing KeyStorageFlags support")]
		public virtual void Import(string fileName, string password, X509KeyStorageFlags keyStorageFlags)
		{
			byte[] rawData = X509Certificate.Load(fileName);
			this.Import(rawData, password, keyStorageFlags);
		}

		// Token: 0x06003A07 RID: 14855 RVA: 0x000C7258 File Offset: 0x000C5458
		[MonoTODO("SecureString support is incomplete, missing KeyStorageFlags support")]
		public virtual void Import(string fileName, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			byte[] rawData = X509Certificate.Load(fileName);
			this.Import(rawData, null, keyStorageFlags);
		}

		// Token: 0x06003A08 RID: 14856 RVA: 0x000C7278 File Offset: 0x000C5478
		[ComVisible(false)]
		public virtual void Reset()
		{
			this.x509 = null;
			this.issuer_name = null;
			this.subject_name = null;
			this.hideDates = false;
			this.cachedCertificateHash = null;
		}

		// Token: 0x04001915 RID: 6421
		private X509Certificate x509;

		// Token: 0x04001916 RID: 6422
		private bool hideDates;

		// Token: 0x04001917 RID: 6423
		private byte[] cachedCertificateHash;

		// Token: 0x04001918 RID: 6424
		private string issuer_name;

		// Token: 0x04001919 RID: 6425
		private string subject_name;

		// Token: 0x020005ED RID: 1517
		internal struct CertificateContext
		{
			// Token: 0x0400191A RID: 6426
			public uint dwCertEncodingType;

			// Token: 0x0400191B RID: 6427
			public IntPtr pbCertEncoded;

			// Token: 0x0400191C RID: 6428
			public uint cbCertEncoded;

			// Token: 0x0400191D RID: 6429
			public IntPtr pCertInfo;

			// Token: 0x0400191E RID: 6430
			public IntPtr hCertStore;
		}
	}
}
