﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x020005EF RID: 1519
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum X509KeyStorageFlags
	{
		// Token: 0x04001929 RID: 6441
		DefaultKeySet = 0,
		// Token: 0x0400192A RID: 6442
		UserKeySet = 1,
		// Token: 0x0400192B RID: 6443
		MachineKeySet = 2,
		// Token: 0x0400192C RID: 6444
		Exportable = 4,
		// Token: 0x0400192D RID: 6445
		UserProtected = 8,
		// Token: 0x0400192E RID: 6446
		PersistKeySet = 16
	}
}
