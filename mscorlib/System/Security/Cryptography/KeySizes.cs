﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005BC RID: 1468
	[ComVisible(true)]
	public sealed class KeySizes
	{
		// Token: 0x0600384A RID: 14410 RVA: 0x000B6AEC File Offset: 0x000B4CEC
		public KeySizes(int minSize, int maxSize, int skipSize)
		{
			this._maxSize = maxSize;
			this._minSize = minSize;
			this._skipSize = skipSize;
		}

		// Token: 0x17000AB3 RID: 2739
		// (get) Token: 0x0600384B RID: 14411 RVA: 0x000B6B0C File Offset: 0x000B4D0C
		public int MaxSize
		{
			get
			{
				return this._maxSize;
			}
		}

		// Token: 0x17000AB4 RID: 2740
		// (get) Token: 0x0600384C RID: 14412 RVA: 0x000B6B14 File Offset: 0x000B4D14
		public int MinSize
		{
			get
			{
				return this._minSize;
			}
		}

		// Token: 0x17000AB5 RID: 2741
		// (get) Token: 0x0600384D RID: 14413 RVA: 0x000B6B1C File Offset: 0x000B4D1C
		public int SkipSize
		{
			get
			{
				return this._skipSize;
			}
		}

		// Token: 0x0600384E RID: 14414 RVA: 0x000B6B24 File Offset: 0x000B4D24
		internal bool IsLegal(int keySize)
		{
			int num = keySize - this.MinSize;
			bool flag = num >= 0 && keySize <= this.MaxSize;
			return (this.SkipSize != 0) ? (flag && num % this.SkipSize == 0) : flag;
		}

		// Token: 0x0600384F RID: 14415 RVA: 0x000B6B78 File Offset: 0x000B4D78
		internal static bool IsLegalKeySize(KeySizes[] legalKeys, int size)
		{
			foreach (KeySizes keySizes in legalKeys)
			{
				if (keySizes.IsLegal(size))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0400186B RID: 6251
		private int _maxSize;

		// Token: 0x0400186C RID: 6252
		private int _minSize;

		// Token: 0x0400186D RID: 6253
		private int _skipSize;
	}
}
