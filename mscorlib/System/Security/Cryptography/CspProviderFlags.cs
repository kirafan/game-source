﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005A4 RID: 1444
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum CspProviderFlags
	{
		// Token: 0x04001827 RID: 6183
		UseMachineKeyStore = 1,
		// Token: 0x04001828 RID: 6184
		UseDefaultKeyContainer = 2,
		// Token: 0x04001829 RID: 6185
		UseExistingKey = 8,
		// Token: 0x0400182A RID: 6186
		NoFlags = 0,
		// Token: 0x0400182B RID: 6187
		NoPrompt = 64,
		// Token: 0x0400182C RID: 6188
		UseArchivableKey = 16,
		// Token: 0x0400182D RID: 6189
		UseNonExportableKey = 4,
		// Token: 0x0400182E RID: 6190
		UseUserProtectedKey = 32
	}
}
