﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005D8 RID: 1496
	[ComVisible(true)]
	public class RSAPKCS1SignatureFormatter : AsymmetricSignatureFormatter
	{
		// Token: 0x0600393E RID: 14654 RVA: 0x000C3804 File Offset: 0x000C1A04
		public RSAPKCS1SignatureFormatter()
		{
		}

		// Token: 0x0600393F RID: 14655 RVA: 0x000C380C File Offset: 0x000C1A0C
		public RSAPKCS1SignatureFormatter(AsymmetricAlgorithm key)
		{
			this.SetKey(key);
		}

		// Token: 0x06003940 RID: 14656 RVA: 0x000C381C File Offset: 0x000C1A1C
		public override byte[] CreateSignature(byte[] rgbHash)
		{
			if (this.rsa == null)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("No key pair available."));
			}
			if (this.hash == null)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("Missing hash algorithm."));
			}
			if (rgbHash == null)
			{
				throw new ArgumentNullException("rgbHash");
			}
			return PKCS1.Sign_v15(this.rsa, this.hash, rgbHash);
		}

		// Token: 0x06003941 RID: 14657 RVA: 0x000C3884 File Offset: 0x000C1A84
		public override void SetHashAlgorithm(string strName)
		{
			this.hash = HashAlgorithm.Create(strName);
		}

		// Token: 0x06003942 RID: 14658 RVA: 0x000C3894 File Offset: 0x000C1A94
		public override void SetKey(AsymmetricAlgorithm key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.rsa = (RSA)key;
		}

		// Token: 0x040018CE RID: 6350
		private RSA rsa;

		// Token: 0x040018CF RID: 6351
		private HashAlgorithm hash;
	}
}
