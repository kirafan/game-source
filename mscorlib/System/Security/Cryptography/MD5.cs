﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005BF RID: 1471
	[ComVisible(true)]
	public abstract class MD5 : HashAlgorithm
	{
		// Token: 0x0600385D RID: 14429 RVA: 0x000B6E38 File Offset: 0x000B5038
		protected MD5()
		{
			this.HashSizeValue = 128;
		}

		// Token: 0x0600385E RID: 14430 RVA: 0x000B6E4C File Offset: 0x000B504C
		public new static MD5 Create()
		{
			return MD5.Create("System.Security.Cryptography.MD5");
		}

		// Token: 0x0600385F RID: 14431 RVA: 0x000B6E58 File Offset: 0x000B5058
		public new static MD5 Create(string algName)
		{
			return (MD5)CryptoConfig.CreateFromName(algName);
		}
	}
}
