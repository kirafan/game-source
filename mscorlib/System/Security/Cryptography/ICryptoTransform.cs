﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005B8 RID: 1464
	[ComVisible(true)]
	public interface ICryptoTransform : IDisposable
	{
		// Token: 0x17000AAD RID: 2733
		// (get) Token: 0x06003839 RID: 14393
		bool CanReuseTransform { get; }

		// Token: 0x17000AAE RID: 2734
		// (get) Token: 0x0600383A RID: 14394
		bool CanTransformMultipleBlocks { get; }

		// Token: 0x17000AAF RID: 2735
		// (get) Token: 0x0600383B RID: 14395
		int InputBlockSize { get; }

		// Token: 0x17000AB0 RID: 2736
		// (get) Token: 0x0600383C RID: 14396
		int OutputBlockSize { get; }

		// Token: 0x0600383D RID: 14397
		int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset);

		// Token: 0x0600383E RID: 14398
		byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount);
	}
}
