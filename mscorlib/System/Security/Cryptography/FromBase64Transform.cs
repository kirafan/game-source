﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005AF RID: 1455
	[ComVisible(true)]
	public class FromBase64Transform : IDisposable, ICryptoTransform
	{
		// Token: 0x060037F4 RID: 14324 RVA: 0x000B5C34 File Offset: 0x000B3E34
		public FromBase64Transform() : this(FromBase64TransformMode.IgnoreWhiteSpaces)
		{
		}

		// Token: 0x060037F5 RID: 14325 RVA: 0x000B5C40 File Offset: 0x000B3E40
		public FromBase64Transform(FromBase64TransformMode whitespaces)
		{
			this.mode = whitespaces;
			this.accumulator = new byte[4];
			this.accPtr = 0;
			this.m_disposed = false;
		}

		// Token: 0x060037F6 RID: 14326 RVA: 0x000B5C6C File Offset: 0x000B3E6C
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060037F7 RID: 14327 RVA: 0x000B5C7C File Offset: 0x000B3E7C
		~FromBase64Transform()
		{
			this.Dispose(false);
		}

		// Token: 0x17000A9D RID: 2717
		// (get) Token: 0x060037F8 RID: 14328 RVA: 0x000B5CB8 File Offset: 0x000B3EB8
		public bool CanTransformMultipleBlocks
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A9E RID: 2718
		// (get) Token: 0x060037F9 RID: 14329 RVA: 0x000B5CBC File Offset: 0x000B3EBC
		public virtual bool CanReuseTransform
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000A9F RID: 2719
		// (get) Token: 0x060037FA RID: 14330 RVA: 0x000B5CC0 File Offset: 0x000B3EC0
		public int InputBlockSize
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x17000AA0 RID: 2720
		// (get) Token: 0x060037FB RID: 14331 RVA: 0x000B5CC4 File Offset: 0x000B3EC4
		public int OutputBlockSize
		{
			get
			{
				return 3;
			}
		}

		// Token: 0x060037FC RID: 14332 RVA: 0x000B5CC8 File Offset: 0x000B3EC8
		public void Clear()
		{
			this.Dispose(true);
		}

		// Token: 0x060037FD RID: 14333 RVA: 0x000B5CD4 File Offset: 0x000B3ED4
		protected virtual void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				if (this.accumulator != null)
				{
					Array.Clear(this.accumulator, 0, this.accumulator.Length);
				}
				if (disposing)
				{
					this.accumulator = null;
				}
				this.m_disposed = true;
			}
		}

		// Token: 0x060037FE RID: 14334 RVA: 0x000B5D20 File Offset: 0x000B3F20
		private byte lookup(byte input)
		{
			if ((int)input >= this.lookupTable.Length)
			{
				throw new FormatException(Locale.GetText("Invalid character in a Base-64 string."));
			}
			byte b = this.lookupTable[(int)input];
			if (b == 255)
			{
				throw new FormatException(Locale.GetText("Invalid character in a Base-64 string."));
			}
			return b;
		}

		// Token: 0x060037FF RID: 14335 RVA: 0x000B5D70 File Offset: 0x000B3F70
		private int ProcessBlock(byte[] output, int offset)
		{
			int num = 0;
			if (this.accumulator[3] == 61)
			{
				num++;
			}
			if (this.accumulator[2] == 61)
			{
				num++;
			}
			this.lookupTable = Base64Constants.DecodeTable;
			switch (num)
			{
			case 0:
			{
				int num2 = (int)this.lookup(this.accumulator[0]);
				int num3 = (int)this.lookup(this.accumulator[1]);
				int num4 = (int)this.lookup(this.accumulator[2]);
				int num5 = (int)this.lookup(this.accumulator[3]);
				output[offset++] = (byte)(num2 << 2 | num3 >> 4);
				output[offset++] = (byte)(num3 << 4 | num4 >> 2);
				output[offset] = (byte)(num4 << 6 | num5);
				break;
			}
			case 1:
			{
				int num2 = (int)this.lookup(this.accumulator[0]);
				int num3 = (int)this.lookup(this.accumulator[1]);
				int num4 = (int)this.lookup(this.accumulator[2]);
				output[offset++] = (byte)(num2 << 2 | num3 >> 4);
				output[offset] = (byte)(num3 << 4 | num4 >> 2);
				break;
			}
			case 2:
			{
				int num2 = (int)this.lookup(this.accumulator[0]);
				int num3 = (int)this.lookup(this.accumulator[1]);
				output[offset] = (byte)(num2 << 2 | num3 >> 4);
				break;
			}
			}
			return 3 - num;
		}

		// Token: 0x06003800 RID: 14336 RVA: 0x000B5EB8 File Offset: 0x000B40B8
		private void CheckInputParameters(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			if (inputBuffer == null)
			{
				throw new ArgumentNullException("inputBuffer");
			}
			if (inputOffset < 0)
			{
				throw new ArgumentOutOfRangeException("inputOffset", "< 0");
			}
			if (inputCount > inputBuffer.Length)
			{
				throw new OutOfMemoryException("inputCount " + Locale.GetText("Overflow"));
			}
			if (inputOffset > inputBuffer.Length - inputCount)
			{
				throw new ArgumentException("inputOffset", Locale.GetText("Overflow"));
			}
			if (inputCount < 0)
			{
				throw new OverflowException("inputCount < 0");
			}
		}

		// Token: 0x06003801 RID: 14337 RVA: 0x000B5F44 File Offset: 0x000B4144
		public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException("FromBase64Transform");
			}
			this.CheckInputParameters(inputBuffer, inputOffset, inputCount);
			if (outputBuffer == null || outputOffset < 0)
			{
				throw new FormatException("outputBuffer");
			}
			int num = 0;
			while (inputCount > 0)
			{
				if (this.accPtr < 4)
				{
					byte b = inputBuffer[inputOffset++];
					if (this.mode == FromBase64TransformMode.IgnoreWhiteSpaces)
					{
						if (!char.IsWhiteSpace((char)b))
						{
							this.accumulator[this.accPtr++] = b;
						}
					}
					else
					{
						this.accumulator[this.accPtr++] = b;
					}
				}
				if (this.accPtr == 4)
				{
					num += this.ProcessBlock(outputBuffer, outputOffset);
					outputOffset += 3;
					this.accPtr = 0;
				}
				inputCount--;
			}
			return num;
		}

		// Token: 0x06003802 RID: 14338 RVA: 0x000B6028 File Offset: 0x000B4228
		public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException("FromBase64Transform");
			}
			this.CheckInputParameters(inputBuffer, inputOffset, inputCount);
			int num = 0;
			int num2 = 0;
			if (this.mode == FromBase64TransformMode.IgnoreWhiteSpaces)
			{
				int num3 = inputOffset;
				for (int i = 0; i < inputCount; i++)
				{
					if (char.IsWhiteSpace((char)inputBuffer[num3]))
					{
						num++;
					}
					num3++;
				}
				if (num == inputCount)
				{
					return new byte[0];
				}
				int num4 = inputOffset + inputCount - 1;
				int j = Math.Min(2, inputCount);
				while (j > 0)
				{
					char c = (char)inputBuffer[num4--];
					if (c == '=')
					{
						num2++;
						j--;
					}
					else if (!char.IsWhiteSpace(c))
					{
						break;
					}
				}
			}
			else
			{
				if (inputBuffer[inputOffset + inputCount - 1] == 61)
				{
					num2++;
				}
				if (inputBuffer[inputOffset + inputCount - 2] == 61)
				{
					num2++;
				}
			}
			if (inputCount < 4 && num2 < 2)
			{
				if (this.accPtr > 2 && this.accumulator[3] == 61)
				{
					num2++;
				}
				if (this.accPtr > 1 && this.accumulator[2] == 61)
				{
					num2++;
				}
			}
			int num5 = (this.accPtr + inputCount - num >> 2) * 3 - num2;
			if (num5 <= 0)
			{
				return new byte[0];
			}
			byte[] array = new byte[num5];
			this.TransformBlock(inputBuffer, inputOffset, inputCount, array, 0);
			return array;
		}

		// Token: 0x04001854 RID: 6228
		private const byte TerminatorByte = 61;

		// Token: 0x04001855 RID: 6229
		private FromBase64TransformMode mode;

		// Token: 0x04001856 RID: 6230
		private byte[] accumulator;

		// Token: 0x04001857 RID: 6231
		private int accPtr;

		// Token: 0x04001858 RID: 6232
		private bool m_disposed;

		// Token: 0x04001859 RID: 6233
		private byte[] lookupTable;
	}
}
