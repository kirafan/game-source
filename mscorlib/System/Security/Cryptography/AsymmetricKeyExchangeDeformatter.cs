﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x02000595 RID: 1429
	[ComVisible(true)]
	public abstract class AsymmetricKeyExchangeDeformatter
	{
		// Token: 0x17000A78 RID: 2680
		// (get) Token: 0x06003740 RID: 14144
		// (set) Token: 0x06003741 RID: 14145
		public abstract string Parameters { get; set; }

		// Token: 0x06003742 RID: 14146
		public abstract byte[] DecryptKeyExchange(byte[] rgb);

		// Token: 0x06003743 RID: 14147
		public abstract void SetKey(AsymmetricAlgorithm key);
	}
}
