﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x0200059A RID: 1434
	[ComVisible(true)]
	[Serializable]
	public enum CipherMode
	{
		// Token: 0x04001775 RID: 6005
		CBC = 1,
		// Token: 0x04001776 RID: 6006
		ECB,
		// Token: 0x04001777 RID: 6007
		OFB,
		// Token: 0x04001778 RID: 6008
		CFB,
		// Token: 0x04001779 RID: 6009
		CTS
	}
}
