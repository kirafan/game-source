﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005D5 RID: 1493
	[ComVisible(true)]
	public class RSAPKCS1KeyExchangeDeformatter : AsymmetricKeyExchangeDeformatter
	{
		// Token: 0x06003928 RID: 14632 RVA: 0x000C35D0 File Offset: 0x000C17D0
		public RSAPKCS1KeyExchangeDeformatter()
		{
		}

		// Token: 0x06003929 RID: 14633 RVA: 0x000C35D8 File Offset: 0x000C17D8
		public RSAPKCS1KeyExchangeDeformatter(AsymmetricAlgorithm key)
		{
			this.SetKey(key);
		}

		// Token: 0x17000AD1 RID: 2769
		// (get) Token: 0x0600392A RID: 14634 RVA: 0x000C35E8 File Offset: 0x000C17E8
		// (set) Token: 0x0600392B RID: 14635 RVA: 0x000C35EC File Offset: 0x000C17EC
		public override string Parameters
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x17000AD2 RID: 2770
		// (get) Token: 0x0600392C RID: 14636 RVA: 0x000C35F0 File Offset: 0x000C17F0
		// (set) Token: 0x0600392D RID: 14637 RVA: 0x000C35F8 File Offset: 0x000C17F8
		public RandomNumberGenerator RNG
		{
			get
			{
				return this.random;
			}
			set
			{
				this.random = value;
			}
		}

		// Token: 0x0600392E RID: 14638 RVA: 0x000C3604 File Offset: 0x000C1804
		public override byte[] DecryptKeyExchange(byte[] rgbIn)
		{
			if (this.rsa == null)
			{
				throw new CryptographicUnexpectedOperationException(Locale.GetText("No key pair available."));
			}
			byte[] array = PKCS1.Decrypt_v15(this.rsa, rgbIn);
			if (array != null)
			{
				return array;
			}
			throw new CryptographicException(Locale.GetText("PKCS1 decoding error."));
		}

		// Token: 0x0600392F RID: 14639 RVA: 0x000C3650 File Offset: 0x000C1850
		public override void SetKey(AsymmetricAlgorithm key)
		{
			this.rsa = (RSA)key;
		}

		// Token: 0x040018C8 RID: 6344
		private RSA rsa;

		// Token: 0x040018C9 RID: 6345
		private RandomNumberGenerator random;
	}
}
