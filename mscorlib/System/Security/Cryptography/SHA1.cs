﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005D9 RID: 1497
	[ComVisible(true)]
	public abstract class SHA1 : HashAlgorithm
	{
		// Token: 0x06003943 RID: 14659 RVA: 0x000C38B4 File Offset: 0x000C1AB4
		protected SHA1()
		{
			this.HashSizeValue = 160;
		}

		// Token: 0x06003944 RID: 14660 RVA: 0x000C38C8 File Offset: 0x000C1AC8
		public new static SHA1 Create()
		{
			return SHA1.Create("System.Security.Cryptography.SHA1");
		}

		// Token: 0x06003945 RID: 14661 RVA: 0x000C38D4 File Offset: 0x000C1AD4
		public new static SHA1 Create(string hashName)
		{
			return (SHA1)CryptoConfig.CreateFromName(hashName);
		}
	}
}
