﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005D4 RID: 1492
	[ComVisible(true)]
	[Serializable]
	public struct RSAParameters
	{
		// Token: 0x040018C0 RID: 6336
		[NonSerialized]
		public byte[] P;

		// Token: 0x040018C1 RID: 6337
		[NonSerialized]
		public byte[] Q;

		// Token: 0x040018C2 RID: 6338
		[NonSerialized]
		public byte[] D;

		// Token: 0x040018C3 RID: 6339
		[NonSerialized]
		public byte[] DP;

		// Token: 0x040018C4 RID: 6340
		[NonSerialized]
		public byte[] DQ;

		// Token: 0x040018C5 RID: 6341
		[NonSerialized]
		public byte[] InverseQ;

		// Token: 0x040018C6 RID: 6342
		public byte[] Modulus;

		// Token: 0x040018C7 RID: 6343
		public byte[] Exponent;
	}
}
