﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005AE RID: 1454
	[ComVisible(true)]
	[Serializable]
	public enum FromBase64TransformMode
	{
		// Token: 0x04001852 RID: 6226
		IgnoreWhiteSpaces,
		// Token: 0x04001853 RID: 6227
		DoNotIgnoreWhiteSpaces
	}
}
