﻿using System;
using System.Runtime.InteropServices;
using System.Security.AccessControl;

namespace System.Security.Cryptography
{
	// Token: 0x020005A2 RID: 1442
	[ComVisible(true)]
	public sealed class CspKeyContainerInfo
	{
		// Token: 0x0600378E RID: 14222 RVA: 0x000B44FC File Offset: 0x000B26FC
		public CspKeyContainerInfo(CspParameters parameters)
		{
			this._params = parameters;
			this._random = true;
		}

		// Token: 0x17000A84 RID: 2692
		// (get) Token: 0x0600378F RID: 14223 RVA: 0x000B4514 File Offset: 0x000B2714
		public bool Accessible
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000A85 RID: 2693
		// (get) Token: 0x06003790 RID: 14224 RVA: 0x000B4518 File Offset: 0x000B2718
		public CryptoKeySecurity CryptoKeySecurity
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000A86 RID: 2694
		// (get) Token: 0x06003791 RID: 14225 RVA: 0x000B451C File Offset: 0x000B271C
		public bool Exportable
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000A87 RID: 2695
		// (get) Token: 0x06003792 RID: 14226 RVA: 0x000B4520 File Offset: 0x000B2720
		public bool HardwareDevice
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A88 RID: 2696
		// (get) Token: 0x06003793 RID: 14227 RVA: 0x000B4524 File Offset: 0x000B2724
		public string KeyContainerName
		{
			get
			{
				return this._params.KeyContainerName;
			}
		}

		// Token: 0x17000A89 RID: 2697
		// (get) Token: 0x06003794 RID: 14228 RVA: 0x000B4534 File Offset: 0x000B2734
		public KeyNumber KeyNumber
		{
			get
			{
				return (KeyNumber)this._params.KeyNumber;
			}
		}

		// Token: 0x17000A8A RID: 2698
		// (get) Token: 0x06003795 RID: 14229 RVA: 0x000B4544 File Offset: 0x000B2744
		public bool MachineKeyStore
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A8B RID: 2699
		// (get) Token: 0x06003796 RID: 14230 RVA: 0x000B4548 File Offset: 0x000B2748
		public bool Protected
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A8C RID: 2700
		// (get) Token: 0x06003797 RID: 14231 RVA: 0x000B454C File Offset: 0x000B274C
		public string ProviderName
		{
			get
			{
				return this._params.ProviderName;
			}
		}

		// Token: 0x17000A8D RID: 2701
		// (get) Token: 0x06003798 RID: 14232 RVA: 0x000B455C File Offset: 0x000B275C
		public int ProviderType
		{
			get
			{
				return this._params.ProviderType;
			}
		}

		// Token: 0x17000A8E RID: 2702
		// (get) Token: 0x06003799 RID: 14233 RVA: 0x000B456C File Offset: 0x000B276C
		public bool RandomlyGenerated
		{
			get
			{
				return this._random;
			}
		}

		// Token: 0x17000A8F RID: 2703
		// (get) Token: 0x0600379A RID: 14234 RVA: 0x000B4574 File Offset: 0x000B2774
		public bool Removable
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A90 RID: 2704
		// (get) Token: 0x0600379B RID: 14235 RVA: 0x000B4578 File Offset: 0x000B2778
		public string UniqueKeyContainerName
		{
			get
			{
				return this._params.ProviderName + "\\" + this._params.KeyContainerName;
			}
		}

		// Token: 0x0400181D RID: 6173
		private CspParameters _params;

		// Token: 0x0400181E RID: 6174
		internal bool _random;
	}
}
