﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005CD RID: 1485
	[ComVisible(true)]
	public abstract class RIPEMD160 : HashAlgorithm
	{
		// Token: 0x060038C4 RID: 14532 RVA: 0x000C0FC4 File Offset: 0x000BF1C4
		protected RIPEMD160()
		{
			this.HashSizeValue = 160;
		}

		// Token: 0x060038C5 RID: 14533 RVA: 0x000C0FD8 File Offset: 0x000BF1D8
		public new static RIPEMD160 Create()
		{
			return RIPEMD160.Create("System.Security.Cryptography.RIPEMD160");
		}

		// Token: 0x060038C6 RID: 14534 RVA: 0x000C0FE4 File Offset: 0x000BF1E4
		public new static RIPEMD160 Create(string hashName)
		{
			return (RIPEMD160)CryptoConfig.CreateFromName(hashName);
		}
	}
}
