﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005B5 RID: 1461
	[ComVisible(true)]
	public class HMACSHA256 : HMAC
	{
		// Token: 0x0600382D RID: 14381 RVA: 0x000B686C File Offset: 0x000B4A6C
		public HMACSHA256() : this(KeyBuilder.Key(8))
		{
		}

		// Token: 0x0600382E RID: 14382 RVA: 0x000B687C File Offset: 0x000B4A7C
		public HMACSHA256(byte[] key)
		{
			base.HashName = "SHA256";
			this.HashSizeValue = 256;
			this.Key = key;
		}
	}
}
