﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005E4 RID: 1508
	[ComVisible(true)]
	public class SignatureDescription
	{
		// Token: 0x06003987 RID: 14727 RVA: 0x000C58C8 File Offset: 0x000C3AC8
		public SignatureDescription()
		{
		}

		// Token: 0x06003988 RID: 14728 RVA: 0x000C58D0 File Offset: 0x000C3AD0
		public SignatureDescription(SecurityElement el)
		{
			if (el == null)
			{
				throw new ArgumentNullException("el");
			}
			SecurityElement securityElement = el.SearchForChildByTag("Deformatter");
			this._DeformatterAlgorithm = ((securityElement != null) ? securityElement.Text : null);
			securityElement = el.SearchForChildByTag("Digest");
			this._DigestAlgorithm = ((securityElement != null) ? securityElement.Text : null);
			securityElement = el.SearchForChildByTag("Formatter");
			this._FormatterAlgorithm = ((securityElement != null) ? securityElement.Text : null);
			securityElement = el.SearchForChildByTag("Key");
			this._KeyAlgorithm = ((securityElement != null) ? securityElement.Text : null);
		}

		// Token: 0x17000AD5 RID: 2773
		// (get) Token: 0x06003989 RID: 14729 RVA: 0x000C5984 File Offset: 0x000C3B84
		// (set) Token: 0x0600398A RID: 14730 RVA: 0x000C598C File Offset: 0x000C3B8C
		public string DeformatterAlgorithm
		{
			get
			{
				return this._DeformatterAlgorithm;
			}
			set
			{
				this._DeformatterAlgorithm = value;
			}
		}

		// Token: 0x17000AD6 RID: 2774
		// (get) Token: 0x0600398B RID: 14731 RVA: 0x000C5998 File Offset: 0x000C3B98
		// (set) Token: 0x0600398C RID: 14732 RVA: 0x000C59A0 File Offset: 0x000C3BA0
		public string DigestAlgorithm
		{
			get
			{
				return this._DigestAlgorithm;
			}
			set
			{
				this._DigestAlgorithm = value;
			}
		}

		// Token: 0x17000AD7 RID: 2775
		// (get) Token: 0x0600398D RID: 14733 RVA: 0x000C59AC File Offset: 0x000C3BAC
		// (set) Token: 0x0600398E RID: 14734 RVA: 0x000C59B4 File Offset: 0x000C3BB4
		public string FormatterAlgorithm
		{
			get
			{
				return this._FormatterAlgorithm;
			}
			set
			{
				this._FormatterAlgorithm = value;
			}
		}

		// Token: 0x17000AD8 RID: 2776
		// (get) Token: 0x0600398F RID: 14735 RVA: 0x000C59C0 File Offset: 0x000C3BC0
		// (set) Token: 0x06003990 RID: 14736 RVA: 0x000C59C8 File Offset: 0x000C3BC8
		public string KeyAlgorithm
		{
			get
			{
				return this._KeyAlgorithm;
			}
			set
			{
				this._KeyAlgorithm = value;
			}
		}

		// Token: 0x06003991 RID: 14737 RVA: 0x000C59D4 File Offset: 0x000C3BD4
		public virtual AsymmetricSignatureDeformatter CreateDeformatter(AsymmetricAlgorithm key)
		{
			if (this._DeformatterAlgorithm == null)
			{
				throw new ArgumentNullException("DeformatterAlgorithm");
			}
			AsymmetricSignatureDeformatter asymmetricSignatureDeformatter = (AsymmetricSignatureDeformatter)CryptoConfig.CreateFromName(this._DeformatterAlgorithm);
			if (this._KeyAlgorithm == null)
			{
				throw new NullReferenceException("KeyAlgorithm");
			}
			asymmetricSignatureDeformatter.SetKey(key);
			return asymmetricSignatureDeformatter;
		}

		// Token: 0x06003992 RID: 14738 RVA: 0x000C5A28 File Offset: 0x000C3C28
		public virtual HashAlgorithm CreateDigest()
		{
			if (this._DigestAlgorithm == null)
			{
				throw new ArgumentNullException("DigestAlgorithm");
			}
			return (HashAlgorithm)CryptoConfig.CreateFromName(this._DigestAlgorithm);
		}

		// Token: 0x06003993 RID: 14739 RVA: 0x000C5A5C File Offset: 0x000C3C5C
		public virtual AsymmetricSignatureFormatter CreateFormatter(AsymmetricAlgorithm key)
		{
			if (this._FormatterAlgorithm == null)
			{
				throw new ArgumentNullException("FormatterAlgorithm");
			}
			AsymmetricSignatureFormatter asymmetricSignatureFormatter = (AsymmetricSignatureFormatter)CryptoConfig.CreateFromName(this._FormatterAlgorithm);
			if (this._KeyAlgorithm == null)
			{
				throw new NullReferenceException("KeyAlgorithm");
			}
			asymmetricSignatureFormatter.SetKey(key);
			return asymmetricSignatureFormatter;
		}

		// Token: 0x040018FE RID: 6398
		private string _DeformatterAlgorithm;

		// Token: 0x040018FF RID: 6399
		private string _DigestAlgorithm;

		// Token: 0x04001900 RID: 6400
		private string _FormatterAlgorithm;

		// Token: 0x04001901 RID: 6401
		private string _KeyAlgorithm;
	}
}
