﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005C4 RID: 1476
	[ComVisible(true)]
	public abstract class RandomNumberGenerator
	{
		// Token: 0x06003883 RID: 14467 RVA: 0x000B8390 File Offset: 0x000B6590
		public static RandomNumberGenerator Create()
		{
			return RandomNumberGenerator.Create("System.Security.Cryptography.RandomNumberGenerator");
		}

		// Token: 0x06003884 RID: 14468 RVA: 0x000B839C File Offset: 0x000B659C
		public static RandomNumberGenerator Create(string rngName)
		{
			return (RandomNumberGenerator)CryptoConfig.CreateFromName(rngName);
		}

		// Token: 0x06003885 RID: 14469
		public abstract void GetBytes(byte[] data);

		// Token: 0x06003886 RID: 14470
		public abstract void GetNonZeroBytes(byte[] data);
	}
}
