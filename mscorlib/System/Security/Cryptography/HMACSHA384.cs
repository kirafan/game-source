﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005B6 RID: 1462
	[ComVisible(true)]
	public class HMACSHA384 : HMAC
	{
		// Token: 0x0600382F RID: 14383 RVA: 0x000B68A4 File Offset: 0x000B4AA4
		public HMACSHA384() : this(KeyBuilder.Key(8))
		{
			this.ProduceLegacyHmacValues = HMACSHA384.legacy_mode;
		}

		// Token: 0x06003830 RID: 14384 RVA: 0x000B68C0 File Offset: 0x000B4AC0
		public HMACSHA384(byte[] key)
		{
			this.ProduceLegacyHmacValues = HMACSHA384.legacy_mode;
			base.HashName = "SHA384";
			this.HashSizeValue = 384;
			this.Key = key;
		}

		// Token: 0x17000AAB RID: 2731
		// (get) Token: 0x06003832 RID: 14386 RVA: 0x000B6918 File Offset: 0x000B4B18
		// (set) Token: 0x06003833 RID: 14387 RVA: 0x000B6920 File Offset: 0x000B4B20
		public bool ProduceLegacyHmacValues
		{
			get
			{
				return this.legacy;
			}
			set
			{
				this.legacy = value;
				base.BlockSizeValue = ((!this.legacy) ? 128 : 64);
			}
		}

		// Token: 0x04001863 RID: 6243
		private static bool legacy_mode = Environment.GetEnvironmentVariable("legacyHMACMode") == "1";

		// Token: 0x04001864 RID: 6244
		private bool legacy;
	}
}
