﻿using System;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005EA RID: 1514
	[ComVisible(true)]
	public sealed class TripleDESCryptoServiceProvider : TripleDES
	{
		// Token: 0x060039C9 RID: 14793 RVA: 0x000C6418 File Offset: 0x000C4618
		public override void GenerateIV()
		{
			this.IVValue = KeyBuilder.IV(this.BlockSizeValue >> 3);
		}

		// Token: 0x060039CA RID: 14794 RVA: 0x000C6430 File Offset: 0x000C4630
		public override void GenerateKey()
		{
			this.KeyValue = TripleDESTransform.GetStrongKey();
		}

		// Token: 0x060039CB RID: 14795 RVA: 0x000C6440 File Offset: 0x000C4640
		public override ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new TripleDESTransform(this, false, rgbKey, rgbIV);
		}

		// Token: 0x060039CC RID: 14796 RVA: 0x000C644C File Offset: 0x000C464C
		public override ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new TripleDESTransform(this, true, rgbKey, rgbIV);
		}
	}
}
