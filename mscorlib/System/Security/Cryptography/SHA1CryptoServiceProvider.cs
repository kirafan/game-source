﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005DB RID: 1499
	[ComVisible(true)]
	public sealed class SHA1CryptoServiceProvider : SHA1
	{
		// Token: 0x0600394F RID: 14671 RVA: 0x000C4448 File Offset: 0x000C2648
		public SHA1CryptoServiceProvider()
		{
			this.sha = new SHA1Internal();
		}

		// Token: 0x06003950 RID: 14672 RVA: 0x000C445C File Offset: 0x000C265C
		~SHA1CryptoServiceProvider()
		{
			this.Dispose(false);
		}

		// Token: 0x06003951 RID: 14673 RVA: 0x000C4498 File Offset: 0x000C2698
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		// Token: 0x06003952 RID: 14674 RVA: 0x000C44A4 File Offset: 0x000C26A4
		protected override void HashCore(byte[] rgb, int ibStart, int cbSize)
		{
			this.State = 1;
			this.sha.HashCore(rgb, ibStart, cbSize);
		}

		// Token: 0x06003953 RID: 14675 RVA: 0x000C44BC File Offset: 0x000C26BC
		protected override byte[] HashFinal()
		{
			this.State = 0;
			return this.sha.HashFinal();
		}

		// Token: 0x06003954 RID: 14676 RVA: 0x000C44D0 File Offset: 0x000C26D0
		public override void Initialize()
		{
			this.sha.Initialize();
		}

		// Token: 0x040018D7 RID: 6359
		private SHA1Internal sha;
	}
}
