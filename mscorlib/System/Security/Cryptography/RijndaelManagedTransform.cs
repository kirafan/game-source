﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Cryptography
{
	// Token: 0x020005CC RID: 1484
	[ComVisible(true)]
	public sealed class RijndaelManagedTransform : IDisposable, ICryptoTransform
	{
		// Token: 0x060038B9 RID: 14521 RVA: 0x000C0F04 File Offset: 0x000BF104
		internal RijndaelManagedTransform(Rijndael algo, bool encryption, byte[] key, byte[] iv)
		{
			this._st = new RijndaelTransform(algo, encryption, key, iv);
			this._bs = algo.BlockSize;
		}

		// Token: 0x060038BA RID: 14522 RVA: 0x000C0F34 File Offset: 0x000BF134
		void IDisposable.Dispose()
		{
			this._st.Clear();
		}

		// Token: 0x17000AC1 RID: 2753
		// (get) Token: 0x060038BB RID: 14523 RVA: 0x000C0F44 File Offset: 0x000BF144
		public int BlockSizeValue
		{
			get
			{
				return this._bs;
			}
		}

		// Token: 0x17000AC2 RID: 2754
		// (get) Token: 0x060038BC RID: 14524 RVA: 0x000C0F4C File Offset: 0x000BF14C
		public bool CanTransformMultipleBlocks
		{
			get
			{
				return this._st.CanTransformMultipleBlocks;
			}
		}

		// Token: 0x17000AC3 RID: 2755
		// (get) Token: 0x060038BD RID: 14525 RVA: 0x000C0F5C File Offset: 0x000BF15C
		public bool CanReuseTransform
		{
			get
			{
				return this._st.CanReuseTransform;
			}
		}

		// Token: 0x17000AC4 RID: 2756
		// (get) Token: 0x060038BE RID: 14526 RVA: 0x000C0F6C File Offset: 0x000BF16C
		public int InputBlockSize
		{
			get
			{
				return this._st.InputBlockSize;
			}
		}

		// Token: 0x17000AC5 RID: 2757
		// (get) Token: 0x060038BF RID: 14527 RVA: 0x000C0F7C File Offset: 0x000BF17C
		public int OutputBlockSize
		{
			get
			{
				return this._st.OutputBlockSize;
			}
		}

		// Token: 0x060038C0 RID: 14528 RVA: 0x000C0F8C File Offset: 0x000BF18C
		public void Clear()
		{
			this._st.Clear();
		}

		// Token: 0x060038C1 RID: 14529 RVA: 0x000C0F9C File Offset: 0x000BF19C
		[MonoTODO("Reset does nothing since CanReuseTransform return false.")]
		public void Reset()
		{
		}

		// Token: 0x060038C2 RID: 14530 RVA: 0x000C0FA0 File Offset: 0x000BF1A0
		public int TransformBlock(byte[] inputBuffer, int inputOffset, int inputCount, byte[] outputBuffer, int outputOffset)
		{
			return this._st.TransformBlock(inputBuffer, inputOffset, inputCount, outputBuffer, outputOffset);
		}

		// Token: 0x060038C3 RID: 14531 RVA: 0x000C0FB4 File Offset: 0x000BF1B4
		public byte[] TransformFinalBlock(byte[] inputBuffer, int inputOffset, int inputCount)
		{
			return this._st.TransformFinalBlock(inputBuffer, inputOffset, inputCount);
		}

		// Token: 0x040018A9 RID: 6313
		private RijndaelTransform _st;

		// Token: 0x040018AA RID: 6314
		private int _bs;
	}
}
