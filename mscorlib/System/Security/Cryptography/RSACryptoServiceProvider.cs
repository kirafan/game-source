﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x020005D1 RID: 1489
	[ComVisible(true)]
	public sealed class RSACryptoServiceProvider : RSA, ICspAsymmetricAlgorithm
	{
		// Token: 0x060038F6 RID: 14582 RVA: 0x000C2D94 File Offset: 0x000C0F94
		public RSACryptoServiceProvider()
		{
			this.Common(1024, null);
		}

		// Token: 0x060038F7 RID: 14583 RVA: 0x000C2DB0 File Offset: 0x000C0FB0
		public RSACryptoServiceProvider(CspParameters parameters)
		{
			this.Common(1024, parameters);
		}

		// Token: 0x060038F8 RID: 14584 RVA: 0x000C2DCC File Offset: 0x000C0FCC
		public RSACryptoServiceProvider(int dwKeySize)
		{
			this.Common(dwKeySize, null);
		}

		// Token: 0x060038F9 RID: 14585 RVA: 0x000C2DE4 File Offset: 0x000C0FE4
		public RSACryptoServiceProvider(int dwKeySize, CspParameters parameters)
		{
			this.Common(dwKeySize, parameters);
		}

		// Token: 0x060038FB RID: 14587 RVA: 0x000C2E00 File Offset: 0x000C1000
		private void Common(int dwKeySize, CspParameters p)
		{
			this.LegalKeySizesValue = new KeySizes[1];
			this.LegalKeySizesValue[0] = new KeySizes(384, 16384, 8);
			base.KeySize = dwKeySize;
			this.rsa = new RSAManaged(this.KeySize);
			this.rsa.KeyGenerated += this.OnKeyGenerated;
			this.persistKey = (p != null);
			if (p == null)
			{
				p = new CspParameters(1);
				if (RSACryptoServiceProvider.useMachineKeyStore)
				{
					p.Flags |= CspProviderFlags.UseMachineKeyStore;
				}
				this.store = new KeyPairPersistence(p);
			}
			else
			{
				this.store = new KeyPairPersistence(p);
				this.store.Load();
				if (this.store.KeyValue != null)
				{
					this.persisted = true;
					this.FromXmlString(this.store.KeyValue);
				}
			}
		}

		// Token: 0x17000AC6 RID: 2758
		// (get) Token: 0x060038FC RID: 14588 RVA: 0x000C2EE4 File Offset: 0x000C10E4
		// (set) Token: 0x060038FD RID: 14589 RVA: 0x000C2EEC File Offset: 0x000C10EC
		public static bool UseMachineKeyStore
		{
			get
			{
				return RSACryptoServiceProvider.useMachineKeyStore;
			}
			set
			{
				RSACryptoServiceProvider.useMachineKeyStore = value;
			}
		}

		// Token: 0x060038FE RID: 14590 RVA: 0x000C2EF4 File Offset: 0x000C10F4
		~RSACryptoServiceProvider()
		{
			this.Dispose(false);
		}

		// Token: 0x17000AC7 RID: 2759
		// (get) Token: 0x060038FF RID: 14591 RVA: 0x000C2F30 File Offset: 0x000C1130
		public override string KeyExchangeAlgorithm
		{
			get
			{
				return "RSA-PKCS1-KeyEx";
			}
		}

		// Token: 0x17000AC8 RID: 2760
		// (get) Token: 0x06003900 RID: 14592 RVA: 0x000C2F38 File Offset: 0x000C1138
		public override int KeySize
		{
			get
			{
				if (this.rsa == null)
				{
					return this.KeySizeValue;
				}
				return this.rsa.KeySize;
			}
		}

		// Token: 0x17000AC9 RID: 2761
		// (get) Token: 0x06003901 RID: 14593 RVA: 0x000C2F58 File Offset: 0x000C1158
		// (set) Token: 0x06003902 RID: 14594 RVA: 0x000C2F60 File Offset: 0x000C1160
		public bool PersistKeyInCsp
		{
			get
			{
				return this.persistKey;
			}
			set
			{
				this.persistKey = value;
				if (this.persistKey)
				{
					this.OnKeyGenerated(this.rsa, null);
				}
			}
		}

		// Token: 0x17000ACA RID: 2762
		// (get) Token: 0x06003903 RID: 14595 RVA: 0x000C2F84 File Offset: 0x000C1184
		[ComVisible(false)]
		public bool PublicOnly
		{
			get
			{
				return this.rsa.PublicOnly;
			}
		}

		// Token: 0x17000ACB RID: 2763
		// (get) Token: 0x06003904 RID: 14596 RVA: 0x000C2F94 File Offset: 0x000C1194
		public override string SignatureAlgorithm
		{
			get
			{
				return "http://www.w3.org/2000/09/xmldsig#rsa-sha1";
			}
		}

		// Token: 0x06003905 RID: 14597 RVA: 0x000C2F9C File Offset: 0x000C119C
		public byte[] Decrypt(byte[] rgb, bool fOAEP)
		{
			if (this.m_disposed)
			{
				throw new ObjectDisposedException("rsa");
			}
			AsymmetricKeyExchangeDeformatter asymmetricKeyExchangeDeformatter;
			if (fOAEP)
			{
				asymmetricKeyExchangeDeformatter = new RSAOAEPKeyExchangeDeformatter(this.rsa);
			}
			else
			{
				asymmetricKeyExchangeDeformatter = new RSAPKCS1KeyExchangeDeformatter(this.rsa);
			}
			return asymmetricKeyExchangeDeformatter.DecryptKeyExchange(rgb);
		}

		// Token: 0x06003906 RID: 14598 RVA: 0x000C2FEC File Offset: 0x000C11EC
		public override byte[] DecryptValue(byte[] rgb)
		{
			if (!this.rsa.IsCrtPossible)
			{
				throw new CryptographicException("Incomplete private key - missing CRT.");
			}
			return this.rsa.DecryptValue(rgb);
		}

		// Token: 0x06003907 RID: 14599 RVA: 0x000C3018 File Offset: 0x000C1218
		public byte[] Encrypt(byte[] rgb, bool fOAEP)
		{
			AsymmetricKeyExchangeFormatter asymmetricKeyExchangeFormatter;
			if (fOAEP)
			{
				asymmetricKeyExchangeFormatter = new RSAOAEPKeyExchangeFormatter(this.rsa);
			}
			else
			{
				asymmetricKeyExchangeFormatter = new RSAPKCS1KeyExchangeFormatter(this.rsa);
			}
			return asymmetricKeyExchangeFormatter.CreateKeyExchange(rgb);
		}

		// Token: 0x06003908 RID: 14600 RVA: 0x000C3054 File Offset: 0x000C1254
		public override byte[] EncryptValue(byte[] rgb)
		{
			return this.rsa.EncryptValue(rgb);
		}

		// Token: 0x06003909 RID: 14601 RVA: 0x000C3064 File Offset: 0x000C1264
		public override RSAParameters ExportParameters(bool includePrivateParameters)
		{
			if (includePrivateParameters && !this.privateKeyExportable)
			{
				throw new CryptographicException("cannot export private key");
			}
			return this.rsa.ExportParameters(includePrivateParameters);
		}

		// Token: 0x0600390A RID: 14602 RVA: 0x000C309C File Offset: 0x000C129C
		public override void ImportParameters(RSAParameters parameters)
		{
			this.rsa.ImportParameters(parameters);
		}

		// Token: 0x0600390B RID: 14603 RVA: 0x000C30AC File Offset: 0x000C12AC
		private HashAlgorithm GetHash(object halg)
		{
			if (halg == null)
			{
				throw new ArgumentNullException("halg");
			}
			HashAlgorithm result;
			if (halg is string)
			{
				result = HashAlgorithm.Create((string)halg);
			}
			else if (halg is HashAlgorithm)
			{
				result = (HashAlgorithm)halg;
			}
			else
			{
				if (!(halg is Type))
				{
					throw new ArgumentException("halg");
				}
				result = (HashAlgorithm)Activator.CreateInstance((Type)halg);
			}
			return result;
		}

		// Token: 0x0600390C RID: 14604 RVA: 0x000C312C File Offset: 0x000C132C
		public byte[] SignData(byte[] buffer, object halg)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			return this.SignData(buffer, 0, buffer.Length, halg);
		}

		// Token: 0x0600390D RID: 14605 RVA: 0x000C314C File Offset: 0x000C134C
		public byte[] SignData(Stream inputStream, object halg)
		{
			HashAlgorithm hash = this.GetHash(halg);
			byte[] hashValue = hash.ComputeHash(inputStream);
			return PKCS1.Sign_v15(this, hash, hashValue);
		}

		// Token: 0x0600390E RID: 14606 RVA: 0x000C3174 File Offset: 0x000C1374
		public byte[] SignData(byte[] buffer, int offset, int count, object halg)
		{
			HashAlgorithm hash = this.GetHash(halg);
			byte[] hashValue = hash.ComputeHash(buffer, offset, count);
			return PKCS1.Sign_v15(this, hash, hashValue);
		}

		// Token: 0x0600390F RID: 14607 RVA: 0x000C319C File Offset: 0x000C139C
		private string GetHashNameFromOID(string oid)
		{
			if (oid != null)
			{
				if (RSACryptoServiceProvider.<>f__switch$map2D == null)
				{
					RSACryptoServiceProvider.<>f__switch$map2D = new Dictionary<string, int>(2)
					{
						{
							"1.3.14.3.2.26",
							0
						},
						{
							"1.2.840.113549.2.5",
							1
						}
					};
				}
				int num;
				if (RSACryptoServiceProvider.<>f__switch$map2D.TryGetValue(oid, out num))
				{
					if (num == 0)
					{
						return "SHA1";
					}
					if (num == 1)
					{
						return "MD5";
					}
				}
			}
			throw new NotSupportedException(oid + " is an unsupported hash algorithm for RSA signing");
		}

		// Token: 0x06003910 RID: 14608 RVA: 0x000C3220 File Offset: 0x000C1420
		public byte[] SignHash(byte[] rgbHash, string str)
		{
			if (rgbHash == null)
			{
				throw new ArgumentNullException("rgbHash");
			}
			string hashName = (str != null) ? this.GetHashNameFromOID(str) : "SHA1";
			HashAlgorithm hash = HashAlgorithm.Create(hashName);
			return PKCS1.Sign_v15(this, hash, rgbHash);
		}

		// Token: 0x06003911 RID: 14609 RVA: 0x000C3268 File Offset: 0x000C1468
		public bool VerifyData(byte[] buffer, object halg, byte[] signature)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (signature == null)
			{
				throw new ArgumentNullException("signature");
			}
			HashAlgorithm hash = this.GetHash(halg);
			byte[] hashValue = hash.ComputeHash(buffer);
			return PKCS1.Verify_v15(this, hash, hashValue, signature);
		}

		// Token: 0x06003912 RID: 14610 RVA: 0x000C32B0 File Offset: 0x000C14B0
		public bool VerifyHash(byte[] rgbHash, string str, byte[] rgbSignature)
		{
			if (rgbHash == null)
			{
				throw new ArgumentNullException("rgbHash");
			}
			if (rgbSignature == null)
			{
				throw new ArgumentNullException("rgbSignature");
			}
			string hashName = (str != null) ? this.GetHashNameFromOID(str) : "SHA1";
			HashAlgorithm hash = HashAlgorithm.Create(hashName);
			return PKCS1.Verify_v15(this, hash, rgbHash, rgbSignature);
		}

		// Token: 0x06003913 RID: 14611 RVA: 0x000C3308 File Offset: 0x000C1508
		protected override void Dispose(bool disposing)
		{
			if (!this.m_disposed)
			{
				if (this.persisted && !this.persistKey)
				{
					this.store.Remove();
				}
				if (this.rsa != null)
				{
					this.rsa.Clear();
				}
				this.m_disposed = true;
			}
		}

		// Token: 0x06003914 RID: 14612 RVA: 0x000C3360 File Offset: 0x000C1560
		private void OnKeyGenerated(object sender, EventArgs e)
		{
			if (this.persistKey && !this.persisted)
			{
				this.store.KeyValue = this.ToXmlString(!this.rsa.PublicOnly);
				this.store.Save();
				this.persisted = true;
			}
		}

		// Token: 0x17000ACC RID: 2764
		// (get) Token: 0x06003915 RID: 14613 RVA: 0x000C33B4 File Offset: 0x000C15B4
		[ComVisible(false)]
		[MonoTODO("Always return null")]
		public CspKeyContainerInfo CspKeyContainerInfo
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06003916 RID: 14614 RVA: 0x000C33B8 File Offset: 0x000C15B8
		[ComVisible(false)]
		public byte[] ExportCspBlob(bool includePrivateParameters)
		{
			byte[] array;
			if (includePrivateParameters)
			{
				array = CryptoConvert.ToCapiPrivateKeyBlob(this);
			}
			else
			{
				array = CryptoConvert.ToCapiPublicKeyBlob(this);
			}
			array[5] = 164;
			return array;
		}

		// Token: 0x06003917 RID: 14615 RVA: 0x000C33EC File Offset: 0x000C15EC
		[ComVisible(false)]
		public void ImportCspBlob(byte[] keyBlob)
		{
			if (keyBlob == null)
			{
				throw new ArgumentNullException("keyBlob");
			}
			RSA rsa = CryptoConvert.FromCapiKeyBlob(keyBlob);
			if (rsa is RSACryptoServiceProvider)
			{
				RSAParameters parameters = rsa.ExportParameters(!(rsa as RSACryptoServiceProvider).PublicOnly);
				this.ImportParameters(parameters);
			}
			else
			{
				try
				{
					RSAParameters parameters2 = rsa.ExportParameters(true);
					this.ImportParameters(parameters2);
				}
				catch
				{
					RSAParameters parameters3 = rsa.ExportParameters(false);
					this.ImportParameters(parameters3);
				}
			}
		}

		// Token: 0x040018B3 RID: 6323
		private const int PROV_RSA_FULL = 1;

		// Token: 0x040018B4 RID: 6324
		private KeyPairPersistence store;

		// Token: 0x040018B5 RID: 6325
		private bool persistKey;

		// Token: 0x040018B6 RID: 6326
		private bool persisted;

		// Token: 0x040018B7 RID: 6327
		private bool privateKeyExportable = true;

		// Token: 0x040018B8 RID: 6328
		private bool m_disposed;

		// Token: 0x040018B9 RID: 6329
		private RSAManaged rsa;

		// Token: 0x040018BA RID: 6330
		private static bool useMachineKeyStore;
	}
}
