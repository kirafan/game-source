﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200056A RID: 1386
	[Flags]
	public enum CryptoKeyRights
	{
		// Token: 0x040016E5 RID: 5861
		ReadData = 1,
		// Token: 0x040016E6 RID: 5862
		WriteData = 2,
		// Token: 0x040016E7 RID: 5863
		ReadExtendedAttributes = 8,
		// Token: 0x040016E8 RID: 5864
		WriteExtendedAttributes = 16,
		// Token: 0x040016E9 RID: 5865
		ReadAttributes = 128,
		// Token: 0x040016EA RID: 5866
		WriteAttributes = 256,
		// Token: 0x040016EB RID: 5867
		Delete = 65536,
		// Token: 0x040016EC RID: 5868
		ReadPermissions = 131072,
		// Token: 0x040016ED RID: 5869
		ChangePermissions = 262144,
		// Token: 0x040016EE RID: 5870
		TakeOwnership = 524288,
		// Token: 0x040016EF RID: 5871
		Synchronize = 1048576,
		// Token: 0x040016F0 RID: 5872
		FullControl = 2032027,
		// Token: 0x040016F1 RID: 5873
		GenericAll = 268435456,
		// Token: 0x040016F2 RID: 5874
		GenericExecute = 536870912,
		// Token: 0x040016F3 RID: 5875
		GenericWrite = 1073741824,
		// Token: 0x040016F4 RID: 5876
		GenericRead = -2147483648
	}
}
