﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000557 RID: 1367
	public enum AccessControlType
	{
		// Token: 0x0400168D RID: 5773
		Allow,
		// Token: 0x0400168E RID: 5774
		Deny
	}
}
