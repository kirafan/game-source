﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000592 RID: 1426
	[Flags]
	public enum SecurityInfos
	{
		// Token: 0x0400176C RID: 5996
		Owner = 1,
		// Token: 0x0400176D RID: 5997
		Group = 2,
		// Token: 0x0400176E RID: 5998
		DiscretionaryAcl = 4,
		// Token: 0x0400176F RID: 5999
		SystemAcl = 8
	}
}
