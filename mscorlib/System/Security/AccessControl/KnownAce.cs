﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200057D RID: 1405
	public abstract class KnownAce : GenericAce
	{
		// Token: 0x06003681 RID: 13953 RVA: 0x000B2288 File Offset: 0x000B0488
		internal KnownAce(InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags) : base(inheritanceFlags, propagationFlags)
		{
		}

		// Token: 0x17000A43 RID: 2627
		// (get) Token: 0x06003682 RID: 13954 RVA: 0x000B2294 File Offset: 0x000B0494
		// (set) Token: 0x06003683 RID: 13955 RVA: 0x000B229C File Offset: 0x000B049C
		public int AccessMask
		{
			get
			{
				return this.access_mask;
			}
			set
			{
				this.access_mask = value;
			}
		}

		// Token: 0x17000A44 RID: 2628
		// (get) Token: 0x06003684 RID: 13956 RVA: 0x000B22A8 File Offset: 0x000B04A8
		// (set) Token: 0x06003685 RID: 13957 RVA: 0x000B22B0 File Offset: 0x000B04B0
		public SecurityIdentifier SecurityIdentifier
		{
			get
			{
				return this.identifier;
			}
			set
			{
				this.identifier = value;
			}
		}

		// Token: 0x04001726 RID: 5926
		private int access_mask;

		// Token: 0x04001727 RID: 5927
		private SecurityIdentifier identifier;
	}
}
