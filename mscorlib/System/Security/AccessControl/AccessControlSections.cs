﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000556 RID: 1366
	[Flags]
	public enum AccessControlSections
	{
		// Token: 0x04001686 RID: 5766
		None = 0,
		// Token: 0x04001687 RID: 5767
		Audit = 1,
		// Token: 0x04001688 RID: 5768
		Access = 2,
		// Token: 0x04001689 RID: 5769
		Owner = 4,
		// Token: 0x0400168A RID: 5770
		Group = 8,
		// Token: 0x0400168B RID: 5771
		All = 15
	}
}
