﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000576 RID: 1398
	public sealed class FileSystemAuditRule : AuditRule
	{
		// Token: 0x06003640 RID: 13888 RVA: 0x000B1FBC File Offset: 0x000B01BC
		public FileSystemAuditRule(IdentityReference identity, FileSystemRights fileSystemRights, AuditFlags flags) : this(identity, fileSystemRights, InheritanceFlags.None, PropagationFlags.None, flags)
		{
		}

		// Token: 0x06003641 RID: 13889 RVA: 0x000B1FCC File Offset: 0x000B01CC
		public FileSystemAuditRule(string identity, FileSystemRights fileSystemRights, AuditFlags flags) : this(new SecurityIdentifier(identity), fileSystemRights, flags)
		{
		}

		// Token: 0x06003642 RID: 13890 RVA: 0x000B1FDC File Offset: 0x000B01DC
		public FileSystemAuditRule(IdentityReference identity, FileSystemRights fileSystemRights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags) : base(identity, 0, false, inheritanceFlags, propagationFlags, flags)
		{
			this.rights = fileSystemRights;
		}

		// Token: 0x06003643 RID: 13891 RVA: 0x000B1FF4 File Offset: 0x000B01F4
		public FileSystemAuditRule(string identity, FileSystemRights fileSystemRights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags) : this(new SecurityIdentifier(identity), fileSystemRights, inheritanceFlags, propagationFlags, flags)
		{
		}

		// Token: 0x17000A2D RID: 2605
		// (get) Token: 0x06003644 RID: 13892 RVA: 0x000B2008 File Offset: 0x000B0208
		public FileSystemRights FileSystemRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x04001702 RID: 5890
		private FileSystemRights rights;
	}
}
