﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200055B RID: 1371
	public enum AceQualifier
	{
		// Token: 0x0400169E RID: 5790
		AccessAllowed,
		// Token: 0x0400169F RID: 5791
		AccessDenied,
		// Token: 0x040016A0 RID: 5792
		SystemAudit,
		// Token: 0x040016A1 RID: 5793
		SystemAlarm
	}
}
