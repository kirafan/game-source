﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000555 RID: 1365
	public enum AccessControlModification
	{
		// Token: 0x0400167F RID: 5759
		Add,
		// Token: 0x04001680 RID: 5760
		Set,
		// Token: 0x04001681 RID: 5761
		Reset,
		// Token: 0x04001682 RID: 5762
		Remove,
		// Token: 0x04001683 RID: 5763
		RemoveAll,
		// Token: 0x04001684 RID: 5764
		RemoveSpecific
	}
}
