﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000575 RID: 1397
	public sealed class FileSystemAccessRule : AccessRule
	{
		// Token: 0x0600363B RID: 13883 RVA: 0x000B1F64 File Offset: 0x000B0164
		public FileSystemAccessRule(IdentityReference identity, FileSystemRights fileSystemRights, AccessControlType type) : this(identity, fileSystemRights, InheritanceFlags.None, PropagationFlags.None, type)
		{
		}

		// Token: 0x0600363C RID: 13884 RVA: 0x000B1F74 File Offset: 0x000B0174
		public FileSystemAccessRule(string identity, FileSystemRights fileSystemRights, AccessControlType type) : this(new SecurityIdentifier(identity), fileSystemRights, InheritanceFlags.None, PropagationFlags.None, type)
		{
		}

		// Token: 0x0600363D RID: 13885 RVA: 0x000B1F88 File Offset: 0x000B0188
		public FileSystemAccessRule(IdentityReference identity, FileSystemRights fileSystemRights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type) : base(identity, (int)fileSystemRights, false, inheritanceFlags, propagationFlags, type)
		{
			this.rights = fileSystemRights;
		}

		// Token: 0x0600363E RID: 13886 RVA: 0x000B1FA0 File Offset: 0x000B01A0
		public FileSystemAccessRule(string identity, FileSystemRights fileSystemRights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type) : this(new SecurityIdentifier(identity), fileSystemRights, inheritanceFlags, propagationFlags, type)
		{
		}

		// Token: 0x17000A2C RID: 2604
		// (get) Token: 0x0600363F RID: 13887 RVA: 0x000B1FB4 File Offset: 0x000B01B4
		public FileSystemRights FileSystemRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x04001701 RID: 5889
		private FileSystemRights rights;
	}
}
