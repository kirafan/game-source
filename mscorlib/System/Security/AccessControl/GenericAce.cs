﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000579 RID: 1401
	public abstract class GenericAce
	{
		// Token: 0x06003657 RID: 13911 RVA: 0x000B20C4 File Offset: 0x000B02C4
		internal GenericAce(InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			this.inheritance = inheritanceFlags;
			this.propagation = propagationFlags;
		}

		// Token: 0x06003658 RID: 13912 RVA: 0x000B20DC File Offset: 0x000B02DC
		internal GenericAce(AceType type)
		{
			if (type <= AceType.SystemAlarmCallbackObject)
			{
				throw new ArgumentOutOfRangeException("type");
			}
			this.ace_type = type;
		}

		// Token: 0x17000A31 RID: 2609
		// (get) Token: 0x06003659 RID: 13913 RVA: 0x000B210C File Offset: 0x000B030C
		// (set) Token: 0x0600365A RID: 13914 RVA: 0x000B2114 File Offset: 0x000B0314
		public AceFlags AceFlags
		{
			get
			{
				return this.aceflags;
			}
			set
			{
				this.aceflags = value;
			}
		}

		// Token: 0x17000A32 RID: 2610
		// (get) Token: 0x0600365B RID: 13915 RVA: 0x000B2120 File Offset: 0x000B0320
		public AceType AceType
		{
			get
			{
				return this.ace_type;
			}
		}

		// Token: 0x17000A33 RID: 2611
		// (get) Token: 0x0600365C RID: 13916 RVA: 0x000B2128 File Offset: 0x000B0328
		public AuditFlags AuditFlags
		{
			get
			{
				AuditFlags auditFlags = AuditFlags.None;
				if ((byte)(this.aceflags & AceFlags.SuccessfulAccess) != 0)
				{
					auditFlags |= AuditFlags.Success;
				}
				if ((byte)(this.aceflags & AceFlags.FailedAccess) != 0)
				{
					auditFlags |= AuditFlags.Failure;
				}
				return auditFlags;
			}
		}

		// Token: 0x17000A34 RID: 2612
		// (get) Token: 0x0600365D RID: 13917
		public abstract int BinaryLength { get; }

		// Token: 0x17000A35 RID: 2613
		// (get) Token: 0x0600365E RID: 13918 RVA: 0x000B2164 File Offset: 0x000B0364
		public InheritanceFlags InheritanceFlags
		{
			get
			{
				return this.inheritance;
			}
		}

		// Token: 0x17000A36 RID: 2614
		// (get) Token: 0x0600365F RID: 13919 RVA: 0x000B216C File Offset: 0x000B036C
		[MonoTODO]
		public bool IsInherited
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A37 RID: 2615
		// (get) Token: 0x06003660 RID: 13920 RVA: 0x000B2170 File Offset: 0x000B0370
		public PropagationFlags PropagationFlags
		{
			get
			{
				return this.propagation;
			}
		}

		// Token: 0x06003661 RID: 13921 RVA: 0x000B2178 File Offset: 0x000B0378
		[MonoTODO]
		public GenericAce Copy()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003662 RID: 13922 RVA: 0x000B2180 File Offset: 0x000B0380
		[MonoTODO]
		public static GenericAce CreateFromBinaryForm(byte[] binaryForm, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003663 RID: 13923 RVA: 0x000B2188 File Offset: 0x000B0388
		[MonoTODO]
		public sealed override bool Equals(object o)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003664 RID: 13924
		[MonoTODO]
		public abstract void GetBinaryForm(byte[] binaryForm, int offset);

		// Token: 0x06003665 RID: 13925 RVA: 0x000B2190 File Offset: 0x000B0390
		[MonoTODO]
		public sealed override int GetHashCode()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003666 RID: 13926 RVA: 0x000B2198 File Offset: 0x000B0398
		[MonoTODO]
		public static bool operator ==(GenericAce left, GenericAce right)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003667 RID: 13927 RVA: 0x000B21A0 File Offset: 0x000B03A0
		[MonoTODO]
		public static bool operator !=(GenericAce left, GenericAce right)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0400171B RID: 5915
		private InheritanceFlags inheritance;

		// Token: 0x0400171C RID: 5916
		private PropagationFlags propagation;

		// Token: 0x0400171D RID: 5917
		private AceFlags aceflags;

		// Token: 0x0400171E RID: 5918
		private AceType ace_type;
	}
}
