﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200056F RID: 1391
	public sealed class DiscretionaryAcl : CommonAcl
	{
		// Token: 0x06003618 RID: 13848 RVA: 0x000B1D50 File Offset: 0x000AFF50
		public DiscretionaryAcl(bool isContainer, bool isDS, int capacity) : this(isContainer, isDS, 0, capacity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003619 RID: 13849 RVA: 0x000B1D64 File Offset: 0x000AFF64
		public DiscretionaryAcl(bool isContainer, bool isDS, RawAcl rawAcl) : base(isContainer, isDS, 0)
		{
		}

		// Token: 0x0600361A RID: 13850 RVA: 0x000B1D70 File Offset: 0x000AFF70
		public DiscretionaryAcl(bool isContainer, bool isDS, byte revision, int capacity) : base(isContainer, isDS, revision, capacity)
		{
		}

		// Token: 0x0600361B RID: 13851 RVA: 0x000B1D80 File Offset: 0x000AFF80
		public void AddAccess(AccessControlType accessType, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600361C RID: 13852 RVA: 0x000B1D88 File Offset: 0x000AFF88
		public void AddAccess(AccessControlType accessType, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600361D RID: 13853 RVA: 0x000B1D90 File Offset: 0x000AFF90
		public bool RemoveAccess(AccessControlType accessType, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600361E RID: 13854 RVA: 0x000B1D98 File Offset: 0x000AFF98
		public bool RemoveAccess(AccessControlType accessType, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600361F RID: 13855 RVA: 0x000B1DA0 File Offset: 0x000AFFA0
		public void RemoveAccessSpecific(AccessControlType accessType, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003620 RID: 13856 RVA: 0x000B1DA8 File Offset: 0x000AFFA8
		public void RemoveAccessSpecific(AccessControlType accessType, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003621 RID: 13857 RVA: 0x000B1DB0 File Offset: 0x000AFFB0
		public void SetAccess(AccessControlType accessType, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003622 RID: 13858 RVA: 0x000B1DB8 File Offset: 0x000AFFB8
		public void SetAccess(AccessControlType accessType, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}
	}
}
