﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200058C RID: 1420
	public sealed class RawSecurityDescriptor : GenericSecurityDescriptor
	{
		// Token: 0x060036FE RID: 14078 RVA: 0x000B287C File Offset: 0x000B0A7C
		public RawSecurityDescriptor(string sddlForm)
		{
		}

		// Token: 0x060036FF RID: 14079 RVA: 0x000B2884 File Offset: 0x000B0A84
		public RawSecurityDescriptor(byte[] binaryForm, int offset)
		{
		}

		// Token: 0x06003700 RID: 14080 RVA: 0x000B288C File Offset: 0x000B0A8C
		public RawSecurityDescriptor(ControlFlags flags, SecurityIdentifier owner, SecurityIdentifier group, RawAcl systemAcl, RawAcl discretionaryAcl)
		{
		}

		// Token: 0x17000A69 RID: 2665
		// (get) Token: 0x06003701 RID: 14081 RVA: 0x000B2894 File Offset: 0x000B0A94
		public override ControlFlags ControlFlags
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A6A RID: 2666
		// (get) Token: 0x06003702 RID: 14082 RVA: 0x000B289C File Offset: 0x000B0A9C
		// (set) Token: 0x06003703 RID: 14083 RVA: 0x000B28A4 File Offset: 0x000B0AA4
		public RawAcl DiscretionaryAcl
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A6B RID: 2667
		// (get) Token: 0x06003704 RID: 14084 RVA: 0x000B28AC File Offset: 0x000B0AAC
		// (set) Token: 0x06003705 RID: 14085 RVA: 0x000B28B4 File Offset: 0x000B0AB4
		public override SecurityIdentifier Group
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A6C RID: 2668
		// (get) Token: 0x06003706 RID: 14086 RVA: 0x000B28BC File Offset: 0x000B0ABC
		// (set) Token: 0x06003707 RID: 14087 RVA: 0x000B28C4 File Offset: 0x000B0AC4
		public override SecurityIdentifier Owner
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A6D RID: 2669
		// (get) Token: 0x06003708 RID: 14088 RVA: 0x000B28CC File Offset: 0x000B0ACC
		// (set) Token: 0x06003709 RID: 14089 RVA: 0x000B28D4 File Offset: 0x000B0AD4
		public byte ResourceManagerControl
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A6E RID: 2670
		// (get) Token: 0x0600370A RID: 14090 RVA: 0x000B28DC File Offset: 0x000B0ADC
		// (set) Token: 0x0600370B RID: 14091 RVA: 0x000B28E4 File Offset: 0x000B0AE4
		public RawAcl SystemAcl
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0600370C RID: 14092 RVA: 0x000B28EC File Offset: 0x000B0AEC
		public void SetFlags(ControlFlags flags)
		{
			throw new NotImplementedException();
		}
	}
}
