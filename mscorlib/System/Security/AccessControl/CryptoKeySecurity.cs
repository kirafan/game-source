﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200056B RID: 1387
	public sealed class CryptoKeySecurity : NativeObjectSecurity
	{
		// Token: 0x060035EB RID: 13803 RVA: 0x000B1B54 File Offset: 0x000AFD54
		[MonoTODO]
		public CryptoKeySecurity()
		{
		}

		// Token: 0x060035EC RID: 13804 RVA: 0x000B1B5C File Offset: 0x000AFD5C
		[MonoTODO]
		public CryptoKeySecurity(CommonSecurityDescriptor securityDescriptor)
		{
		}

		// Token: 0x17000A22 RID: 2594
		// (get) Token: 0x060035ED RID: 13805 RVA: 0x000B1B64 File Offset: 0x000AFD64
		public override Type AccessRightType
		{
			get
			{
				return typeof(CryptoKeyRights);
			}
		}

		// Token: 0x17000A23 RID: 2595
		// (get) Token: 0x060035EE RID: 13806 RVA: 0x000B1B70 File Offset: 0x000AFD70
		public override Type AccessRuleType
		{
			get
			{
				return typeof(CryptoKeyAccessRule);
			}
		}

		// Token: 0x17000A24 RID: 2596
		// (get) Token: 0x060035EF RID: 13807 RVA: 0x000B1B7C File Offset: 0x000AFD7C
		public override Type AuditRuleType
		{
			get
			{
				return typeof(CryptoKeyAuditRule);
			}
		}

		// Token: 0x060035F0 RID: 13808 RVA: 0x000B1B88 File Offset: 0x000AFD88
		public sealed override AccessRule AccessRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type)
		{
			return new CryptoKeyAccessRule(identityReference, (CryptoKeyRights)accessMask, type);
		}

		// Token: 0x060035F1 RID: 13809 RVA: 0x000B1B94 File Offset: 0x000AFD94
		[MonoTODO]
		public void AddAccessRule(CryptoKeyAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035F2 RID: 13810 RVA: 0x000B1B9C File Offset: 0x000AFD9C
		[MonoTODO]
		public bool RemoveAccessRule(CryptoKeyAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035F3 RID: 13811 RVA: 0x000B1BA4 File Offset: 0x000AFDA4
		[MonoTODO]
		public void RemoveAccessRuleAll(CryptoKeyAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035F4 RID: 13812 RVA: 0x000B1BAC File Offset: 0x000AFDAC
		[MonoTODO]
		public void RemoveAccessRuleSpecific(CryptoKeyAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035F5 RID: 13813 RVA: 0x000B1BB4 File Offset: 0x000AFDB4
		[MonoTODO]
		public void ResetAccessRule(CryptoKeyAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035F6 RID: 13814 RVA: 0x000B1BBC File Offset: 0x000AFDBC
		[MonoTODO]
		public void SetAccessRule(CryptoKeyAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035F7 RID: 13815 RVA: 0x000B1BC4 File Offset: 0x000AFDC4
		public sealed override AuditRule AuditRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags)
		{
			return new CryptoKeyAuditRule(identityReference, (CryptoKeyRights)accessMask, flags);
		}

		// Token: 0x060035F8 RID: 13816 RVA: 0x000B1BD0 File Offset: 0x000AFDD0
		[MonoTODO]
		public void AddAuditRule(CryptoKeyAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035F9 RID: 13817 RVA: 0x000B1BD8 File Offset: 0x000AFDD8
		[MonoTODO]
		public bool RemoveAuditRule(CryptoKeyAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035FA RID: 13818 RVA: 0x000B1BE0 File Offset: 0x000AFDE0
		[MonoTODO]
		public void RemoveAuditRuleAll(CryptoKeyAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035FB RID: 13819 RVA: 0x000B1BE8 File Offset: 0x000AFDE8
		[MonoTODO]
		public void RemoveAuditRuleSpecific(CryptoKeyAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035FC RID: 13820 RVA: 0x000B1BF0 File Offset: 0x000AFDF0
		[MonoTODO]
		public void SetAuditRule(CryptoKeyAuditRule rule)
		{
			throw new NotImplementedException();
		}
	}
}
