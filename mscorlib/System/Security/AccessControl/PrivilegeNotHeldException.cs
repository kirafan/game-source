﻿using System;
using System.Runtime.Serialization;

namespace System.Security.AccessControl
{
	// Token: 0x02000588 RID: 1416
	[Serializable]
	public sealed class PrivilegeNotHeldException : UnauthorizedAccessException, ISerializable
	{
		// Token: 0x060036E9 RID: 14057 RVA: 0x000B2714 File Offset: 0x000B0914
		public PrivilegeNotHeldException()
		{
		}

		// Token: 0x060036EA RID: 14058 RVA: 0x000B271C File Offset: 0x000B091C
		public PrivilegeNotHeldException(string privilege) : base(privilege)
		{
		}

		// Token: 0x060036EB RID: 14059 RVA: 0x000B2728 File Offset: 0x000B0928
		public PrivilegeNotHeldException(string privilege, Exception inner) : base(privilege, inner)
		{
		}

		// Token: 0x17000A61 RID: 2657
		// (get) Token: 0x060036EC RID: 14060 RVA: 0x000B2734 File Offset: 0x000B0934
		public string PrivilegeName
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x060036ED RID: 14061 RVA: 0x000B273C File Offset: 0x000B093C
		[MonoTODO]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}
	}
}
