﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000590 RID: 1424
	public sealed class RegistrySecurity : NativeObjectSecurity
	{
		// Token: 0x17000A71 RID: 2673
		// (get) Token: 0x06003716 RID: 14102 RVA: 0x000B2984 File Offset: 0x000B0B84
		public override Type AccessRightType
		{
			get
			{
				return typeof(RegistryRights);
			}
		}

		// Token: 0x17000A72 RID: 2674
		// (get) Token: 0x06003717 RID: 14103 RVA: 0x000B2990 File Offset: 0x000B0B90
		public override Type AccessRuleType
		{
			get
			{
				return typeof(RegistryAccessRule);
			}
		}

		// Token: 0x17000A73 RID: 2675
		// (get) Token: 0x06003718 RID: 14104 RVA: 0x000B299C File Offset: 0x000B0B9C
		public override Type AuditRuleType
		{
			get
			{
				return typeof(RegistryAuditRule);
			}
		}

		// Token: 0x06003719 RID: 14105 RVA: 0x000B29A8 File Offset: 0x000B0BA8
		public override AccessRule AccessRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type)
		{
			return new RegistryAccessRule(identityReference, (RegistryRights)accessMask, inheritanceFlags, propagationFlags, type);
		}

		// Token: 0x0600371A RID: 14106 RVA: 0x000B29B8 File Offset: 0x000B0BB8
		public void AddAccessRule(RegistryAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600371B RID: 14107 RVA: 0x000B29C0 File Offset: 0x000B0BC0
		public void AddAuditRule(RegistryAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600371C RID: 14108 RVA: 0x000B29C8 File Offset: 0x000B0BC8
		public override AuditRule AuditRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags)
		{
			return new RegistryAuditRule(identityReference, (RegistryRights)accessMask, inheritanceFlags, propagationFlags, flags);
		}

		// Token: 0x0600371D RID: 14109 RVA: 0x000B29D8 File Offset: 0x000B0BD8
		public bool RemoveAccessRule(RegistryAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600371E RID: 14110 RVA: 0x000B29E0 File Offset: 0x000B0BE0
		public void RemoveAccessRuleAll(RegistryAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600371F RID: 14111 RVA: 0x000B29E8 File Offset: 0x000B0BE8
		public void RemoveAccessRuleSpecific(RegistryAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003720 RID: 14112 RVA: 0x000B29F0 File Offset: 0x000B0BF0
		public bool RemoveAuditRule(RegistryAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003721 RID: 14113 RVA: 0x000B29F8 File Offset: 0x000B0BF8
		public void RemoveAuditRuleAll(RegistryAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003722 RID: 14114 RVA: 0x000B2A00 File Offset: 0x000B0C00
		public void RemoveAuditRuleSpecific(RegistryAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003723 RID: 14115 RVA: 0x000B2A08 File Offset: 0x000B0C08
		public void ResetAccessRule(RegistryAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003724 RID: 14116 RVA: 0x000B2A10 File Offset: 0x000B0C10
		public void SetAccessRule(RegistryAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003725 RID: 14117 RVA: 0x000B2A18 File Offset: 0x000B0C18
		public void SetAuditRule(RegistryAuditRule rule)
		{
			throw new NotImplementedException();
		}
	}
}
