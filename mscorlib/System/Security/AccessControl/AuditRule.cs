﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200055E RID: 1374
	public abstract class AuditRule : AuthorizationRule
	{
		// Token: 0x0600359E RID: 13726 RVA: 0x000B1510 File Offset: 0x000AF710
		protected AuditRule(IdentityReference identity, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags auditFlags) : base(identity, accessMask, isInherited, inheritanceFlags, propagationFlags)
		{
			if (!(identity is SecurityIdentifier))
			{
				throw new ArgumentException("identity");
			}
			if (accessMask == 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			this.auditFlags = auditFlags;
		}

		// Token: 0x17000A06 RID: 2566
		// (get) Token: 0x0600359F RID: 13727 RVA: 0x000B154C File Offset: 0x000AF74C
		public AuditFlags AuditFlags
		{
			get
			{
				return this.auditFlags;
			}
		}

		// Token: 0x040016B9 RID: 5817
		private AuditFlags auditFlags;
	}
}
