﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000583 RID: 1411
	public abstract class ObjectAccessRule : AccessRule
	{
		// Token: 0x060036A8 RID: 13992 RVA: 0x000B242C File Offset: 0x000B062C
		protected ObjectAccessRule(IdentityReference identity, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, Guid objectType, Guid inheritedObjectType, AccessControlType type) : base(identity, accessMask, isInherited, inheritanceFlags, propagationFlags, type)
		{
			this.object_type = objectType;
			this.inherited_object_type = inheritedObjectType;
		}

		// Token: 0x17000A4A RID: 2634
		// (get) Token: 0x060036A9 RID: 13993 RVA: 0x000B2450 File Offset: 0x000B0650
		public Guid InheritedObjectType
		{
			get
			{
				return this.inherited_object_type;
			}
		}

		// Token: 0x17000A4B RID: 2635
		// (get) Token: 0x060036AA RID: 13994 RVA: 0x000B2458 File Offset: 0x000B0658
		public ObjectAceFlags ObjectFlags
		{
			get
			{
				ObjectAceFlags objectAceFlags = ObjectAceFlags.None;
				if (this.object_type != Guid.Empty)
				{
					objectAceFlags |= ObjectAceFlags.ObjectAceTypePresent;
				}
				if (this.inherited_object_type != Guid.Empty)
				{
					objectAceFlags |= ObjectAceFlags.InheritedObjectAceTypePresent;
				}
				return objectAceFlags;
			}
		}

		// Token: 0x17000A4C RID: 2636
		// (get) Token: 0x060036AB RID: 13995 RVA: 0x000B249C File Offset: 0x000B069C
		public Guid ObjectType
		{
			get
			{
				return this.object_type;
			}
		}

		// Token: 0x04001732 RID: 5938
		private Guid object_type;

		// Token: 0x04001733 RID: 5939
		private Guid inherited_object_type;
	}
}
