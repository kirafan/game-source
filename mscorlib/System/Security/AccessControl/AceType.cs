﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200055C RID: 1372
	public enum AceType
	{
		// Token: 0x040016A3 RID: 5795
		AccessAllowed,
		// Token: 0x040016A4 RID: 5796
		AccessDenied,
		// Token: 0x040016A5 RID: 5797
		SystemAudit,
		// Token: 0x040016A6 RID: 5798
		SystemAlarm,
		// Token: 0x040016A7 RID: 5799
		AccessAllowedCompound,
		// Token: 0x040016A8 RID: 5800
		AccessAllowedObject,
		// Token: 0x040016A9 RID: 5801
		AccessDeniedObject,
		// Token: 0x040016AA RID: 5802
		SystemAuditObject,
		// Token: 0x040016AB RID: 5803
		SystemAlarmObject,
		// Token: 0x040016AC RID: 5804
		AccessAllowedCallback,
		// Token: 0x040016AD RID: 5805
		AccessDeniedCallback,
		// Token: 0x040016AE RID: 5806
		AccessAllowedCallbackObject,
		// Token: 0x040016AF RID: 5807
		AccessDeniedCallbackObject,
		// Token: 0x040016B0 RID: 5808
		SystemAuditCallback,
		// Token: 0x040016B1 RID: 5809
		SystemAlarmCallback,
		// Token: 0x040016B2 RID: 5810
		SystemAuditCallbackObject,
		// Token: 0x040016B3 RID: 5811
		SystemAlarmCallbackObject,
		// Token: 0x040016B4 RID: 5812
		MaxDefinedAceType = 16
	}
}
