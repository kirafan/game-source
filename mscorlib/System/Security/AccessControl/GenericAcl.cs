﻿using System;
using System.Collections;

namespace System.Security.AccessControl
{
	// Token: 0x0200057A RID: 1402
	public abstract class GenericAcl : IEnumerable, ICollection
	{
		// Token: 0x0600366A RID: 13930 RVA: 0x000B21C8 File Offset: 0x000B03C8
		void ICollection.CopyTo(Array array, int index)
		{
			this.CopyTo((GenericAce[])array, index);
		}

		// Token: 0x0600366B RID: 13931 RVA: 0x000B21D8 File Offset: 0x000B03D8
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x17000A38 RID: 2616
		// (get) Token: 0x0600366C RID: 13932
		public abstract int BinaryLength { get; }

		// Token: 0x17000A39 RID: 2617
		// (get) Token: 0x0600366D RID: 13933
		public abstract int Count { get; }

		// Token: 0x17000A3A RID: 2618
		// (get) Token: 0x0600366E RID: 13934 RVA: 0x000B21E0 File Offset: 0x000B03E0
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000A3B RID: 2619
		public abstract GenericAce this[int index]
		{
			get;
			set;
		}

		// Token: 0x17000A3C RID: 2620
		// (get) Token: 0x06003671 RID: 13937
		public abstract byte Revision { get; }

		// Token: 0x17000A3D RID: 2621
		// (get) Token: 0x06003672 RID: 13938 RVA: 0x000B21E4 File Offset: 0x000B03E4
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06003673 RID: 13939 RVA: 0x000B21E8 File Offset: 0x000B03E8
		public void CopyTo(GenericAce[] array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0 || array.Length - index < this.Count)
			{
				throw new ArgumentOutOfRangeException("index", "Index must be non-negative integer and must not exceed array length - count");
			}
			for (int i = 0; i < this.Count; i++)
			{
				array[i + index] = this[i];
			}
		}

		// Token: 0x06003674 RID: 13940
		public abstract void GetBinaryForm(byte[] binaryForm, int offset);

		// Token: 0x06003675 RID: 13941 RVA: 0x000B2250 File Offset: 0x000B0450
		public AceEnumerator GetEnumerator()
		{
			return new AceEnumerator(this);
		}

		// Token: 0x0400171F RID: 5919
		public static readonly byte AclRevision = 2;

		// Token: 0x04001720 RID: 5920
		public static readonly byte AclRevisionDS = 4;

		// Token: 0x04001721 RID: 5921
		public static readonly int MaxBinaryLength = 65536;
	}
}
