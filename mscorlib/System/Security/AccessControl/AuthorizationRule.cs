﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200055F RID: 1375
	public abstract class AuthorizationRule
	{
		// Token: 0x060035A0 RID: 13728 RVA: 0x000B1554 File Offset: 0x000AF754
		internal AuthorizationRule()
		{
		}

		// Token: 0x060035A1 RID: 13729 RVA: 0x000B155C File Offset: 0x000AF75C
		protected internal AuthorizationRule(IdentityReference identity, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			if (!(identity is SecurityIdentifier))
			{
				throw new ArgumentException("identity");
			}
			if (accessMask == 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			this.identity = identity;
			this.accessMask = accessMask;
			this.isInherited = isInherited;
			this.inheritanceFlags = inheritanceFlags;
			this.propagationFlags = propagationFlags;
		}

		// Token: 0x17000A07 RID: 2567
		// (get) Token: 0x060035A2 RID: 13730 RVA: 0x000B15B8 File Offset: 0x000AF7B8
		public IdentityReference IdentityReference
		{
			get
			{
				return this.identity;
			}
		}

		// Token: 0x17000A08 RID: 2568
		// (get) Token: 0x060035A3 RID: 13731 RVA: 0x000B15C0 File Offset: 0x000AF7C0
		public InheritanceFlags InheritanceFlags
		{
			get
			{
				return this.inheritanceFlags;
			}
		}

		// Token: 0x17000A09 RID: 2569
		// (get) Token: 0x060035A4 RID: 13732 RVA: 0x000B15C8 File Offset: 0x000AF7C8
		public bool IsInherited
		{
			get
			{
				return this.isInherited;
			}
		}

		// Token: 0x17000A0A RID: 2570
		// (get) Token: 0x060035A5 RID: 13733 RVA: 0x000B15D0 File Offset: 0x000AF7D0
		public PropagationFlags PropagationFlags
		{
			get
			{
				return this.propagationFlags;
			}
		}

		// Token: 0x17000A0B RID: 2571
		// (get) Token: 0x060035A6 RID: 13734 RVA: 0x000B15D8 File Offset: 0x000AF7D8
		protected internal int AccessMask
		{
			get
			{
				return this.accessMask;
			}
		}

		// Token: 0x040016BA RID: 5818
		private IdentityReference identity;

		// Token: 0x040016BB RID: 5819
		private int accessMask;

		// Token: 0x040016BC RID: 5820
		private bool isInherited;

		// Token: 0x040016BD RID: 5821
		private InheritanceFlags inheritanceFlags;

		// Token: 0x040016BE RID: 5822
		private PropagationFlags propagationFlags;
	}
}
