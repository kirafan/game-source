﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200058E RID: 1422
	public sealed class RegistryAuditRule : AuditRule
	{
		// Token: 0x06003712 RID: 14098 RVA: 0x000B2948 File Offset: 0x000B0B48
		public RegistryAuditRule(IdentityReference identity, RegistryRights registryRights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags) : base(identity, 0, false, inheritanceFlags, propagationFlags, flags)
		{
			this.rights = registryRights;
		}

		// Token: 0x06003713 RID: 14099 RVA: 0x000B2960 File Offset: 0x000B0B60
		public RegistryAuditRule(string identity, RegistryRights registryRights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags) : this(new SecurityIdentifier(identity), registryRights, inheritanceFlags, propagationFlags, flags)
		{
		}

		// Token: 0x17000A70 RID: 2672
		// (get) Token: 0x06003714 RID: 14100 RVA: 0x000B2974 File Offset: 0x000B0B74
		public RegistryRights RegistryRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x0400174D RID: 5965
		private RegistryRights rights;
	}
}
