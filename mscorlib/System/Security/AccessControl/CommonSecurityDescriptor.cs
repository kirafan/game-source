﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000564 RID: 1380
	public sealed class CommonSecurityDescriptor : GenericSecurityDescriptor
	{
		// Token: 0x060035CB RID: 13771 RVA: 0x000B1980 File Offset: 0x000AFB80
		public CommonSecurityDescriptor(bool isContainer, bool isDS, RawSecurityDescriptor rawSecurityDescriptor)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035CC RID: 13772 RVA: 0x000B1990 File Offset: 0x000AFB90
		public CommonSecurityDescriptor(bool isContainer, bool isDS, string sddlForm)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035CD RID: 13773 RVA: 0x000B19A0 File Offset: 0x000AFBA0
		public CommonSecurityDescriptor(bool isContainer, bool isDS, byte[] binaryForm, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035CE RID: 13774 RVA: 0x000B19B0 File Offset: 0x000AFBB0
		public CommonSecurityDescriptor(bool isContainer, bool isDS, ControlFlags flags, SecurityIdentifier owner, SecurityIdentifier group, SystemAcl systemAcl, DiscretionaryAcl discretionaryAcl)
		{
			this.isContainer = isContainer;
			this.isDS = isDS;
			this.flags = flags;
			this.owner = owner;
			this.group = group;
			this.systemAcl = systemAcl;
			this.discretionaryAcl = discretionaryAcl;
			throw new NotImplementedException();
		}

		// Token: 0x17000A15 RID: 2581
		// (get) Token: 0x060035CF RID: 13775 RVA: 0x000B1A00 File Offset: 0x000AFC00
		public override ControlFlags ControlFlags
		{
			get
			{
				return this.flags;
			}
		}

		// Token: 0x17000A16 RID: 2582
		// (get) Token: 0x060035D0 RID: 13776 RVA: 0x000B1A08 File Offset: 0x000AFC08
		// (set) Token: 0x060035D1 RID: 13777 RVA: 0x000B1A10 File Offset: 0x000AFC10
		public DiscretionaryAcl DiscretionaryAcl
		{
			get
			{
				return this.discretionaryAcl;
			}
			set
			{
				if (value == null)
				{
				}
				this.discretionaryAcl = value;
			}
		}

		// Token: 0x17000A17 RID: 2583
		// (get) Token: 0x060035D2 RID: 13778 RVA: 0x000B1A20 File Offset: 0x000AFC20
		// (set) Token: 0x060035D3 RID: 13779 RVA: 0x000B1A28 File Offset: 0x000AFC28
		public override SecurityIdentifier Group
		{
			get
			{
				return this.group;
			}
			set
			{
				this.group = value;
			}
		}

		// Token: 0x17000A18 RID: 2584
		// (get) Token: 0x060035D4 RID: 13780 RVA: 0x000B1A34 File Offset: 0x000AFC34
		public bool IsContainer
		{
			get
			{
				return this.isContainer;
			}
		}

		// Token: 0x17000A19 RID: 2585
		// (get) Token: 0x060035D5 RID: 13781 RVA: 0x000B1A3C File Offset: 0x000AFC3C
		public bool IsDiscretionaryAclCanonical
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A1A RID: 2586
		// (get) Token: 0x060035D6 RID: 13782 RVA: 0x000B1A44 File Offset: 0x000AFC44
		public bool IsDS
		{
			get
			{
				return this.isDS;
			}
		}

		// Token: 0x17000A1B RID: 2587
		// (get) Token: 0x060035D7 RID: 13783 RVA: 0x000B1A4C File Offset: 0x000AFC4C
		public bool IsSystemAclCanonical
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A1C RID: 2588
		// (get) Token: 0x060035D8 RID: 13784 RVA: 0x000B1A54 File Offset: 0x000AFC54
		// (set) Token: 0x060035D9 RID: 13785 RVA: 0x000B1A5C File Offset: 0x000AFC5C
		public override SecurityIdentifier Owner
		{
			get
			{
				return this.owner;
			}
			set
			{
				this.owner = value;
			}
		}

		// Token: 0x17000A1D RID: 2589
		// (get) Token: 0x060035DA RID: 13786 RVA: 0x000B1A68 File Offset: 0x000AFC68
		// (set) Token: 0x060035DB RID: 13787 RVA: 0x000B1A70 File Offset: 0x000AFC70
		public SystemAcl SystemAcl
		{
			get
			{
				return this.systemAcl;
			}
			set
			{
				this.systemAcl = value;
			}
		}

		// Token: 0x060035DC RID: 13788 RVA: 0x000B1A7C File Offset: 0x000AFC7C
		public void PurgeAccessControl(SecurityIdentifier sid)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035DD RID: 13789 RVA: 0x000B1A84 File Offset: 0x000AFC84
		public void PurgeAudit(SecurityIdentifier sid)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035DE RID: 13790 RVA: 0x000B1A8C File Offset: 0x000AFC8C
		public void SetDiscretionaryAclProtection(bool isProtected, bool preserveInheritance)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035DF RID: 13791 RVA: 0x000B1A94 File Offset: 0x000AFC94
		public void SetSystemAclProtection(bool isProtected, bool preserveInheritance)
		{
			throw new NotImplementedException();
		}

		// Token: 0x040016C6 RID: 5830
		private bool isContainer;

		// Token: 0x040016C7 RID: 5831
		private bool isDS;

		// Token: 0x040016C8 RID: 5832
		private ControlFlags flags;

		// Token: 0x040016C9 RID: 5833
		private SecurityIdentifier owner;

		// Token: 0x040016CA RID: 5834
		private SecurityIdentifier group;

		// Token: 0x040016CB RID: 5835
		private SystemAcl systemAcl;

		// Token: 0x040016CC RID: 5836
		private DiscretionaryAcl discretionaryAcl;
	}
}
