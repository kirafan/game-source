﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000567 RID: 1383
	[Flags]
	public enum ControlFlags
	{
		// Token: 0x040016D1 RID: 5841
		None = 0,
		// Token: 0x040016D2 RID: 5842
		OwnerDefaulted = 1,
		// Token: 0x040016D3 RID: 5843
		GroupDefaulted = 2,
		// Token: 0x040016D4 RID: 5844
		DiscretionaryAclPresent = 4,
		// Token: 0x040016D5 RID: 5845
		DiscretionaryAclDefaulted = 8,
		// Token: 0x040016D6 RID: 5846
		SystemAclPresent = 16,
		// Token: 0x040016D7 RID: 5847
		SystemAclDefaulted = 32,
		// Token: 0x040016D8 RID: 5848
		DiscretionaryAclUntrusted = 64,
		// Token: 0x040016D9 RID: 5849
		ServerSecurity = 128,
		// Token: 0x040016DA RID: 5850
		DiscretionaryAclAutoInheritRequired = 256,
		// Token: 0x040016DB RID: 5851
		SystemAclAutoInheritRequired = 512,
		// Token: 0x040016DC RID: 5852
		DiscretionaryAclAutoInherited = 1024,
		// Token: 0x040016DD RID: 5853
		SystemAclAutoInherited = 2048,
		// Token: 0x040016DE RID: 5854
		DiscretionaryAclProtected = 4096,
		// Token: 0x040016DF RID: 5855
		SystemAclProtected = 8192,
		// Token: 0x040016E0 RID: 5856
		RMControlValid = 16384,
		// Token: 0x040016E1 RID: 5857
		SelfRelative = 32768
	}
}
