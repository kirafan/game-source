﻿using System;
using System.Collections.Generic;

namespace System.Security.AccessControl
{
	// Token: 0x0200058B RID: 1419
	public sealed class RawAcl : GenericAcl
	{
		// Token: 0x060036F4 RID: 14068 RVA: 0x000B27C8 File Offset: 0x000B09C8
		public RawAcl(byte revision, int capacity)
		{
			this.revision = revision;
			this.list = new List<GenericAce>(capacity);
		}

		// Token: 0x060036F5 RID: 14069 RVA: 0x000B27E4 File Offset: 0x000B09E4
		public RawAcl(byte[] binaryForm, int offset) : this(0, 10)
		{
		}

		// Token: 0x17000A65 RID: 2661
		// (get) Token: 0x060036F6 RID: 14070 RVA: 0x000B27F0 File Offset: 0x000B09F0
		[MonoTODO]
		public override int BinaryLength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A66 RID: 2662
		// (get) Token: 0x060036F7 RID: 14071 RVA: 0x000B27F8 File Offset: 0x000B09F8
		public override int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000A67 RID: 2663
		public override GenericAce this[int index]
		{
			get
			{
				return this.list[index];
			}
			set
			{
				this.list[index] = value;
			}
		}

		// Token: 0x17000A68 RID: 2664
		// (get) Token: 0x060036FA RID: 14074 RVA: 0x000B2828 File Offset: 0x000B0A28
		public override byte Revision
		{
			get
			{
				return this.revision;
			}
		}

		// Token: 0x060036FB RID: 14075 RVA: 0x000B2830 File Offset: 0x000B0A30
		[MonoTODO]
		public override void GetBinaryForm(byte[] binaryForm, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036FC RID: 14076 RVA: 0x000B2838 File Offset: 0x000B0A38
		public void InsertAce(int index, GenericAce ace)
		{
			if (ace == null)
			{
				throw new ArgumentNullException("ace");
			}
			this.list.Insert(index, ace);
		}

		// Token: 0x060036FD RID: 14077 RVA: 0x000B286C File Offset: 0x000B0A6C
		public void RemoveAce(int index)
		{
			this.list.RemoveAt(index);
		}

		// Token: 0x0400174A RID: 5962
		private byte revision;

		// Token: 0x0400174B RID: 5963
		private List<GenericAce> list;
	}
}
