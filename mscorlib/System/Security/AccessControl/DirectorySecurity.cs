﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200056E RID: 1390
	public sealed class DirectorySecurity : FileSystemSecurity
	{
		// Token: 0x06003616 RID: 13846 RVA: 0x000B1D30 File Offset: 0x000AFF30
		public DirectorySecurity() : base(true)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x06003617 RID: 13847 RVA: 0x000B1D40 File Offset: 0x000AFF40
		public DirectorySecurity(string name, AccessControlSections includeSections) : base(true, name, includeSections)
		{
			throw new PlatformNotSupportedException();
		}
	}
}
