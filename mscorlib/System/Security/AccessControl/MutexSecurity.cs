﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000581 RID: 1409
	public sealed class MutexSecurity : NativeObjectSecurity
	{
		// Token: 0x0600368B RID: 13963 RVA: 0x000B230C File Offset: 0x000B050C
		public MutexSecurity()
		{
		}

		// Token: 0x0600368C RID: 13964 RVA: 0x000B2314 File Offset: 0x000B0514
		public MutexSecurity(string name, AccessControlSections includeSections)
		{
		}

		// Token: 0x17000A47 RID: 2631
		// (get) Token: 0x0600368D RID: 13965 RVA: 0x000B231C File Offset: 0x000B051C
		public override Type AccessRightType
		{
			get
			{
				return typeof(MutexRights);
			}
		}

		// Token: 0x17000A48 RID: 2632
		// (get) Token: 0x0600368E RID: 13966 RVA: 0x000B2328 File Offset: 0x000B0528
		public override Type AccessRuleType
		{
			get
			{
				return typeof(MutexAccessRule);
			}
		}

		// Token: 0x17000A49 RID: 2633
		// (get) Token: 0x0600368F RID: 13967 RVA: 0x000B2334 File Offset: 0x000B0534
		public override Type AuditRuleType
		{
			get
			{
				return typeof(MutexAuditRule);
			}
		}

		// Token: 0x06003690 RID: 13968 RVA: 0x000B2340 File Offset: 0x000B0540
		public override AccessRule AccessRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type)
		{
			return new MutexAccessRule(identityReference, (MutexRights)accessMask, type);
		}

		// Token: 0x06003691 RID: 13969 RVA: 0x000B234C File Offset: 0x000B054C
		[MonoTODO]
		public void AddAccessRule(MutexAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003692 RID: 13970 RVA: 0x000B2354 File Offset: 0x000B0554
		[MonoTODO]
		public bool RemoveAccessRule(MutexAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003693 RID: 13971 RVA: 0x000B235C File Offset: 0x000B055C
		[MonoTODO]
		public void RemoveAccessRuleAll(MutexAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003694 RID: 13972 RVA: 0x000B2364 File Offset: 0x000B0564
		[MonoTODO]
		public void RemoveAccessRuleSpecific(MutexAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003695 RID: 13973 RVA: 0x000B236C File Offset: 0x000B056C
		[MonoTODO]
		public void ResetAccessRule(MutexAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003696 RID: 13974 RVA: 0x000B2374 File Offset: 0x000B0574
		[MonoTODO]
		public void SetAccessRule(MutexAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003697 RID: 13975 RVA: 0x000B237C File Offset: 0x000B057C
		public override AuditRule AuditRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags)
		{
			return new MutexAuditRule(identityReference, (MutexRights)accessMask, flags);
		}

		// Token: 0x06003698 RID: 13976 RVA: 0x000B2388 File Offset: 0x000B0588
		[MonoTODO]
		public void AddAuditRule(MutexAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003699 RID: 13977 RVA: 0x000B2390 File Offset: 0x000B0590
		[MonoTODO]
		public bool RemoveAuditRule(MutexAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600369A RID: 13978 RVA: 0x000B2398 File Offset: 0x000B0598
		[MonoTODO]
		public void RemoveAuditRuleAll(MutexAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600369B RID: 13979 RVA: 0x000B23A0 File Offset: 0x000B05A0
		[MonoTODO]
		public void RemoveAuditRuleSpecific(MutexAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600369C RID: 13980 RVA: 0x000B23A8 File Offset: 0x000B05A8
		[MonoTODO]
		public void SetAuditRule(MutexAuditRule rule)
		{
			throw new NotImplementedException();
		}
	}
}
