﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000589 RID: 1417
	[Flags]
	public enum PropagationFlags
	{
		// Token: 0x04001744 RID: 5956
		None = 0,
		// Token: 0x04001745 RID: 5957
		NoPropagateInherit = 1,
		// Token: 0x04001746 RID: 5958
		InheritOnly = 2
	}
}
