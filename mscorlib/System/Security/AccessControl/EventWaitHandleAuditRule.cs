﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000571 RID: 1393
	public sealed class EventWaitHandleAuditRule : AuditRule
	{
		// Token: 0x06003626 RID: 13862 RVA: 0x000B1DF0 File Offset: 0x000AFFF0
		public EventWaitHandleAuditRule(IdentityReference identity, EventWaitHandleRights eventRights, AuditFlags flags) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, flags)
		{
			if (eventRights < EventWaitHandleRights.Modify || eventRights > EventWaitHandleRights.FullControl)
			{
				throw new ArgumentOutOfRangeException("eventRights");
			}
			if (flags < AuditFlags.None || flags > AuditFlags.Failure)
			{
				throw new ArgumentOutOfRangeException("flags");
			}
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			if (eventRights == (EventWaitHandleRights)0)
			{
				throw new ArgumentNullException("eventRights");
			}
			if (flags == AuditFlags.None)
			{
				throw new ArgumentException("flags");
			}
			if (!(identity is SecurityIdentifier))
			{
				throw new ArgumentException("identity");
			}
			this.rights = eventRights;
		}

		// Token: 0x17000A28 RID: 2600
		// (get) Token: 0x06003627 RID: 13863 RVA: 0x000B1E98 File Offset: 0x000B0098
		public EventWaitHandleRights EventWaitHandleRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x040016F8 RID: 5880
		private EventWaitHandleRights rights;
	}
}
