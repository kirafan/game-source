﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000569 RID: 1385
	public sealed class CryptoKeyAuditRule : AuditRule
	{
		// Token: 0x060035E8 RID: 13800 RVA: 0x000B1B24 File Offset: 0x000AFD24
		public CryptoKeyAuditRule(IdentityReference identity, CryptoKeyRights cryptoKeyRights, AuditFlags flags) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, flags)
		{
			this.rights = cryptoKeyRights;
		}

		// Token: 0x060035E9 RID: 13801 RVA: 0x000B1B3C File Offset: 0x000AFD3C
		public CryptoKeyAuditRule(string identity, CryptoKeyRights cryptoKeyRights, AuditFlags flags) : this(new SecurityIdentifier(identity), cryptoKeyRights, flags)
		{
		}

		// Token: 0x17000A21 RID: 2593
		// (get) Token: 0x060035EA RID: 13802 RVA: 0x000B1B4C File Offset: 0x000AFD4C
		public CryptoKeyRights CryptoKeyRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x040016E3 RID: 5859
		private CryptoKeyRights rights;
	}
}
