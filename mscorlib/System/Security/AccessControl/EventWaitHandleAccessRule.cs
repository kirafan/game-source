﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000570 RID: 1392
	public sealed class EventWaitHandleAccessRule : AccessRule
	{
		// Token: 0x06003623 RID: 13859 RVA: 0x000B1DC0 File Offset: 0x000AFFC0
		public EventWaitHandleAccessRule(IdentityReference identity, EventWaitHandleRights eventRights, AccessControlType type) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, AccessControlType.Allow)
		{
			this.rights = eventRights;
		}

		// Token: 0x06003624 RID: 13860 RVA: 0x000B1DD8 File Offset: 0x000AFFD8
		public EventWaitHandleAccessRule(string identity, EventWaitHandleRights eventRights, AccessControlType type) : this(new SecurityIdentifier(identity), eventRights, type)
		{
		}

		// Token: 0x17000A27 RID: 2599
		// (get) Token: 0x06003625 RID: 13861 RVA: 0x000B1DE8 File Offset: 0x000AFFE8
		public EventWaitHandleRights EventWaitHandleRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x040016F7 RID: 5879
		private EventWaitHandleRights rights;
	}
}
