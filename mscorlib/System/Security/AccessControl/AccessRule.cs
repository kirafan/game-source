﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000558 RID: 1368
	public abstract class AccessRule : AuthorizationRule
	{
		// Token: 0x06003597 RID: 13719 RVA: 0x000B1428 File Offset: 0x000AF628
		protected AccessRule(IdentityReference identity, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type) : base(identity, accessMask, isInherited, inheritanceFlags, propagationFlags)
		{
			if (!(identity is SecurityIdentifier))
			{
				throw new ArgumentException("identity");
			}
			if (type < AccessControlType.Allow || type > AccessControlType.Deny)
			{
				throw new ArgumentException("type");
			}
			if (accessMask == 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			this.type = type;
		}

		// Token: 0x17000A03 RID: 2563
		// (get) Token: 0x06003598 RID: 13720 RVA: 0x000B1488 File Offset: 0x000AF688
		public AccessControlType AccessControlType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x0400168F RID: 5775
		private AccessControlType type;
	}
}
