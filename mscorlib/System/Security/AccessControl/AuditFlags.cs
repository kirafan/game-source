﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200055D RID: 1373
	[Flags]
	public enum AuditFlags
	{
		// Token: 0x040016B6 RID: 5814
		None = 0,
		// Token: 0x040016B7 RID: 5815
		Success = 1,
		// Token: 0x040016B8 RID: 5816
		Failure = 2
	}
}
