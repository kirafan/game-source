﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000591 RID: 1425
	public enum ResourceType
	{
		// Token: 0x0400175E RID: 5982
		Unknown,
		// Token: 0x0400175F RID: 5983
		FileObject,
		// Token: 0x04001760 RID: 5984
		Service,
		// Token: 0x04001761 RID: 5985
		Printer,
		// Token: 0x04001762 RID: 5986
		RegistryKey,
		// Token: 0x04001763 RID: 5987
		LMShare,
		// Token: 0x04001764 RID: 5988
		KernelObject,
		// Token: 0x04001765 RID: 5989
		WindowObject,
		// Token: 0x04001766 RID: 5990
		DSObject,
		// Token: 0x04001767 RID: 5991
		DSObjectAll,
		// Token: 0x04001768 RID: 5992
		ProviderDefined,
		// Token: 0x04001769 RID: 5993
		WmiGuidObject,
		// Token: 0x0400176A RID: 5994
		RegistryWow6432Key
	}
}
