﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000585 RID: 1413
	[Flags]
	public enum ObjectAceFlags
	{
		// Token: 0x04001738 RID: 5944
		None = 0,
		// Token: 0x04001739 RID: 5945
		ObjectAceTypePresent = 1,
		// Token: 0x0400173A RID: 5946
		InheritedObjectAceTypePresent = 2
	}
}
