﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000578 RID: 1400
	public abstract class FileSystemSecurity : NativeObjectSecurity
	{
		// Token: 0x06003645 RID: 13893 RVA: 0x000B2010 File Offset: 0x000B0210
		internal FileSystemSecurity(bool isContainer) : base(isContainer, ResourceType.FileObject)
		{
		}

		// Token: 0x06003646 RID: 13894 RVA: 0x000B201C File Offset: 0x000B021C
		internal FileSystemSecurity(bool isContainer, string name, AccessControlSections includeSections) : base(isContainer, ResourceType.FileObject, name, includeSections)
		{
		}

		// Token: 0x17000A2E RID: 2606
		// (get) Token: 0x06003647 RID: 13895 RVA: 0x000B2028 File Offset: 0x000B0228
		public override Type AccessRightType
		{
			get
			{
				return typeof(FileSystemRights);
			}
		}

		// Token: 0x17000A2F RID: 2607
		// (get) Token: 0x06003648 RID: 13896 RVA: 0x000B2034 File Offset: 0x000B0234
		public override Type AccessRuleType
		{
			get
			{
				return typeof(FileSystemAccessRule);
			}
		}

		// Token: 0x17000A30 RID: 2608
		// (get) Token: 0x06003649 RID: 13897 RVA: 0x000B2040 File Offset: 0x000B0240
		public override Type AuditRuleType
		{
			get
			{
				return typeof(FileSystemAuditRule);
			}
		}

		// Token: 0x0600364A RID: 13898 RVA: 0x000B204C File Offset: 0x000B024C
		[MonoTODO]
		public sealed override AccessRule AccessRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type)
		{
			return new FileSystemAccessRule(identityReference, (FileSystemRights)accessMask, inheritanceFlags, propagationFlags, type);
		}

		// Token: 0x0600364B RID: 13899 RVA: 0x000B205C File Offset: 0x000B025C
		[MonoTODO]
		public void AddAccessRule(FileSystemAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600364C RID: 13900 RVA: 0x000B2064 File Offset: 0x000B0264
		[MonoTODO]
		public bool RemoveAccessRule(FileSystemAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600364D RID: 13901 RVA: 0x000B206C File Offset: 0x000B026C
		[MonoTODO]
		public void RemoveAccessRuleAll(FileSystemAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600364E RID: 13902 RVA: 0x000B2074 File Offset: 0x000B0274
		[MonoTODO]
		public void RemoveAccessRuleSpecific(FileSystemAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600364F RID: 13903 RVA: 0x000B207C File Offset: 0x000B027C
		[MonoTODO]
		public void ResetAccessRule(FileSystemAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003650 RID: 13904 RVA: 0x000B2084 File Offset: 0x000B0284
		[MonoTODO]
		public void SetAccessRule(FileSystemAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003651 RID: 13905 RVA: 0x000B208C File Offset: 0x000B028C
		[MonoTODO]
		public sealed override AuditRule AuditRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags)
		{
			return new FileSystemAuditRule(identityReference, (FileSystemRights)accessMask, inheritanceFlags, propagationFlags, flags);
		}

		// Token: 0x06003652 RID: 13906 RVA: 0x000B209C File Offset: 0x000B029C
		[MonoTODO]
		public void AddAuditRule(FileSystemAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003653 RID: 13907 RVA: 0x000B20A4 File Offset: 0x000B02A4
		[MonoTODO]
		public bool RemoveAuditRule(FileSystemAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003654 RID: 13908 RVA: 0x000B20AC File Offset: 0x000B02AC
		[MonoTODO]
		public void RemoveAuditRuleAll(FileSystemAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003655 RID: 13909 RVA: 0x000B20B4 File Offset: 0x000B02B4
		[MonoTODO]
		public void RemoveAuditRuleSpecific(FileSystemAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003656 RID: 13910 RVA: 0x000B20BC File Offset: 0x000B02BC
		[MonoTODO]
		public void SetAuditRule(FileSystemAuditRule rule)
		{
			throw new NotImplementedException();
		}
	}
}
