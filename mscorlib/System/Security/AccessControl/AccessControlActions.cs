﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000554 RID: 1364
	[Flags]
	public enum AccessControlActions
	{
		// Token: 0x0400167B RID: 5755
		None = 0,
		// Token: 0x0400167C RID: 5756
		View = 1,
		// Token: 0x0400167D RID: 5757
		Change = 2
	}
}
