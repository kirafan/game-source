﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200056D RID: 1389
	public abstract class DirectoryObjectSecurity : ObjectSecurity
	{
		// Token: 0x06003603 RID: 13827 RVA: 0x000B1C70 File Offset: 0x000AFE70
		protected DirectoryObjectSecurity() : base(false, true)
		{
		}

		// Token: 0x06003604 RID: 13828 RVA: 0x000B1C7C File Offset: 0x000AFE7C
		protected DirectoryObjectSecurity(CommonSecurityDescriptor securityDescriptor) : base(securityDescriptor != null && securityDescriptor.IsContainer, true)
		{
			if (securityDescriptor == null)
			{
				throw new ArgumentNullException("securityDescriptor");
			}
		}

		// Token: 0x06003605 RID: 13829 RVA: 0x000B1CA8 File Offset: 0x000AFEA8
		public virtual AccessRule AccessRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003606 RID: 13830 RVA: 0x000B1CB0 File Offset: 0x000AFEB0
		public virtual AuditRule AuditRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003607 RID: 13831 RVA: 0x000B1CB8 File Offset: 0x000AFEB8
		public AuthorizationRuleCollection GetAccessRules(bool includeExplicit, bool includeInherited, Type targetType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003608 RID: 13832 RVA: 0x000B1CC0 File Offset: 0x000AFEC0
		public AuthorizationRuleCollection GetAuditRules(bool includeExplicit, bool includeInherited, Type targetType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003609 RID: 13833 RVA: 0x000B1CC8 File Offset: 0x000AFEC8
		protected void AddAccessRule(ObjectAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600360A RID: 13834 RVA: 0x000B1CD0 File Offset: 0x000AFED0
		protected void AddAuditRule(ObjectAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600360B RID: 13835 RVA: 0x000B1CD8 File Offset: 0x000AFED8
		protected override bool ModifyAccess(AccessControlModification modification, AccessRule rule, out bool modified)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600360C RID: 13836 RVA: 0x000B1CE0 File Offset: 0x000AFEE0
		protected override bool ModifyAudit(AccessControlModification modification, AuditRule rule, out bool modified)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600360D RID: 13837 RVA: 0x000B1CE8 File Offset: 0x000AFEE8
		protected bool RemoveAccessRule(ObjectAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600360E RID: 13838 RVA: 0x000B1CF0 File Offset: 0x000AFEF0
		protected void RemoveAccessRuleAll(ObjectAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600360F RID: 13839 RVA: 0x000B1CF8 File Offset: 0x000AFEF8
		protected void RemoveAccessRuleSpecific(ObjectAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003610 RID: 13840 RVA: 0x000B1D00 File Offset: 0x000AFF00
		protected bool RemoveAuditRule(ObjectAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003611 RID: 13841 RVA: 0x000B1D08 File Offset: 0x000AFF08
		protected void RemoveAuditRuleAll(ObjectAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003612 RID: 13842 RVA: 0x000B1D10 File Offset: 0x000AFF10
		protected void RemoveAuditRuleSpecific(ObjectAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003613 RID: 13843 RVA: 0x000B1D18 File Offset: 0x000AFF18
		protected void ResetAccessRule(ObjectAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003614 RID: 13844 RVA: 0x000B1D20 File Offset: 0x000AFF20
		protected void SetAccessRule(ObjectAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003615 RID: 13845 RVA: 0x000B1D28 File Offset: 0x000AFF28
		protected void SetAuditRule(ObjectAuditRule rule)
		{
			throw new NotImplementedException();
		}
	}
}
