﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200057B RID: 1403
	public abstract class GenericSecurityDescriptor
	{
		// Token: 0x17000A3E RID: 2622
		// (get) Token: 0x06003677 RID: 13943 RVA: 0x000B2260 File Offset: 0x000B0460
		public int BinaryLength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A3F RID: 2623
		// (get) Token: 0x06003678 RID: 13944
		public abstract ControlFlags ControlFlags { get; }

		// Token: 0x17000A40 RID: 2624
		// (get) Token: 0x06003679 RID: 13945
		// (set) Token: 0x0600367A RID: 13946
		public abstract SecurityIdentifier Group { get; set; }

		// Token: 0x17000A41 RID: 2625
		// (get) Token: 0x0600367B RID: 13947
		// (set) Token: 0x0600367C RID: 13948
		public abstract SecurityIdentifier Owner { get; set; }

		// Token: 0x17000A42 RID: 2626
		// (get) Token: 0x0600367D RID: 13949 RVA: 0x000B2268 File Offset: 0x000B0468
		public static byte Revision
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0600367E RID: 13950 RVA: 0x000B2270 File Offset: 0x000B0470
		public void GetBinaryForm(byte[] binaryForm, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600367F RID: 13951 RVA: 0x000B2278 File Offset: 0x000B0478
		public string GetSddlForm(AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003680 RID: 13952 RVA: 0x000B2280 File Offset: 0x000B0480
		public static bool IsSddlConversionSupported()
		{
			throw new NotImplementedException();
		}
	}
}
