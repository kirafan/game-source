﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000593 RID: 1427
	public sealed class SystemAcl : CommonAcl
	{
		// Token: 0x06003726 RID: 14118 RVA: 0x000B2A20 File Offset: 0x000B0C20
		public SystemAcl(bool isContainer, bool isDS, int capacity) : this(isContainer, isDS, 0, capacity)
		{
		}

		// Token: 0x06003727 RID: 14119 RVA: 0x000B2A2C File Offset: 0x000B0C2C
		public SystemAcl(bool isContainer, bool isDS, RawAcl rawAcl) : this(isContainer, isDS, 0)
		{
		}

		// Token: 0x06003728 RID: 14120 RVA: 0x000B2A38 File Offset: 0x000B0C38
		public SystemAcl(bool isContainer, bool isDS, byte revision, int capacity) : base(isContainer, isDS, revision, capacity)
		{
		}

		// Token: 0x06003729 RID: 14121 RVA: 0x000B2A48 File Offset: 0x000B0C48
		public void AddAudit(AuditFlags auditFlags, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600372A RID: 14122 RVA: 0x000B2A50 File Offset: 0x000B0C50
		public void AddAudit(AuditFlags auditFlags, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600372B RID: 14123 RVA: 0x000B2A58 File Offset: 0x000B0C58
		public bool RemoveAudit(AuditFlags auditFlags, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600372C RID: 14124 RVA: 0x000B2A60 File Offset: 0x000B0C60
		public bool RemoveAudit(AuditFlags auditFlags, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600372D RID: 14125 RVA: 0x000B2A68 File Offset: 0x000B0C68
		public void RemoveAuditSpecific(AuditFlags auditFlags, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600372E RID: 14126 RVA: 0x000B2A70 File Offset: 0x000B0C70
		public void RemoveAuditSpecific(AuditFlags auditFlags, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600372F RID: 14127 RVA: 0x000B2A78 File Offset: 0x000B0C78
		public void SetAudit(AuditFlags auditFlags, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003730 RID: 14128 RVA: 0x000B2A80 File Offset: 0x000B0C80
		public void SetAudit(AuditFlags auditFlags, SecurityIdentifier sid, int accessMask, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, ObjectAceFlags objectFlags, Guid objectType, Guid inheritedObjectType)
		{
			throw new NotImplementedException();
		}
	}
}
