﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000573 RID: 1395
	public sealed class EventWaitHandleSecurity : NativeObjectSecurity
	{
		// Token: 0x06003628 RID: 13864 RVA: 0x000B1EA0 File Offset: 0x000B00A0
		public EventWaitHandleSecurity()
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000A29 RID: 2601
		// (get) Token: 0x06003629 RID: 13865 RVA: 0x000B1EB0 File Offset: 0x000B00B0
		public override Type AccessRightType
		{
			get
			{
				return typeof(EventWaitHandleRights);
			}
		}

		// Token: 0x17000A2A RID: 2602
		// (get) Token: 0x0600362A RID: 13866 RVA: 0x000B1EBC File Offset: 0x000B00BC
		public override Type AccessRuleType
		{
			get
			{
				return typeof(EventWaitHandleAccessRule);
			}
		}

		// Token: 0x17000A2B RID: 2603
		// (get) Token: 0x0600362B RID: 13867 RVA: 0x000B1EC8 File Offset: 0x000B00C8
		public override Type AuditRuleType
		{
			get
			{
				return typeof(EventWaitHandleAuditRule);
			}
		}

		// Token: 0x0600362C RID: 13868 RVA: 0x000B1ED4 File Offset: 0x000B00D4
		public override AccessRule AccessRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type)
		{
			return new EventWaitHandleAccessRule(identityReference, (EventWaitHandleRights)accessMask, type);
		}

		// Token: 0x0600362D RID: 13869 RVA: 0x000B1EE0 File Offset: 0x000B00E0
		[MonoTODO]
		public void AddAccessRule(EventWaitHandleAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600362E RID: 13870 RVA: 0x000B1EE8 File Offset: 0x000B00E8
		[MonoTODO]
		public bool RemoveAccessRule(EventWaitHandleAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600362F RID: 13871 RVA: 0x000B1EF0 File Offset: 0x000B00F0
		[MonoTODO]
		public void RemoveAccessRuleAll(EventWaitHandleAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003630 RID: 13872 RVA: 0x000B1EF8 File Offset: 0x000B00F8
		[MonoTODO]
		public void RemoveAccessRuleSpecific(EventWaitHandleAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003631 RID: 13873 RVA: 0x000B1F00 File Offset: 0x000B0100
		[MonoTODO]
		public void ResetAccessRule(EventWaitHandleAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003632 RID: 13874 RVA: 0x000B1F08 File Offset: 0x000B0108
		[MonoTODO]
		public void SetAccessRule(EventWaitHandleAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003633 RID: 13875 RVA: 0x000B1F10 File Offset: 0x000B0110
		public override AuditRule AuditRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags)
		{
			return new EventWaitHandleAuditRule(identityReference, (EventWaitHandleRights)accessMask, flags);
		}

		// Token: 0x06003634 RID: 13876 RVA: 0x000B1F1C File Offset: 0x000B011C
		[MonoTODO]
		public void AddAuditRule(EventWaitHandleAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003635 RID: 13877 RVA: 0x000B1F24 File Offset: 0x000B0124
		[MonoTODO]
		public bool RemoveAuditRule(EventWaitHandleAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003636 RID: 13878 RVA: 0x000B1F2C File Offset: 0x000B012C
		[MonoTODO]
		public void RemoveAuditRuleAll(EventWaitHandleAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003637 RID: 13879 RVA: 0x000B1F34 File Offset: 0x000B0134
		[MonoTODO]
		public void RemoveAuditRuleSpecific(EventWaitHandleAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003638 RID: 13880 RVA: 0x000B1F3C File Offset: 0x000B013C
		[MonoTODO]
		public void SetAuditRule(EventWaitHandleAuditRule rule)
		{
			throw new NotImplementedException();
		}
	}
}
