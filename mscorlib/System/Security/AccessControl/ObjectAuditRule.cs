﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000586 RID: 1414
	public abstract class ObjectAuditRule : AuditRule
	{
		// Token: 0x060036B6 RID: 14006 RVA: 0x000B253C File Offset: 0x000B073C
		protected ObjectAuditRule(IdentityReference identity, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, Guid objectType, Guid inheritedObjectType, AuditFlags auditFlags) : base(identity, accessMask, isInherited, inheritanceFlags, propagationFlags, auditFlags)
		{
			this.object_type = objectType;
			this.inherited_object_type = inheritedObjectType;
		}

		// Token: 0x17000A51 RID: 2641
		// (get) Token: 0x060036B7 RID: 14007 RVA: 0x000B2560 File Offset: 0x000B0760
		public Guid InheritedObjectType
		{
			get
			{
				return this.inherited_object_type;
			}
		}

		// Token: 0x17000A52 RID: 2642
		// (get) Token: 0x060036B8 RID: 14008 RVA: 0x000B2568 File Offset: 0x000B0768
		public ObjectAceFlags ObjectFlags
		{
			get
			{
				ObjectAceFlags objectAceFlags = ObjectAceFlags.None;
				if (this.object_type != Guid.Empty)
				{
					objectAceFlags |= ObjectAceFlags.ObjectAceTypePresent;
				}
				if (this.inherited_object_type != Guid.Empty)
				{
					objectAceFlags |= ObjectAceFlags.InheritedObjectAceTypePresent;
				}
				return objectAceFlags;
			}
		}

		// Token: 0x17000A53 RID: 2643
		// (get) Token: 0x060036B9 RID: 14009 RVA: 0x000B25AC File Offset: 0x000B07AC
		public Guid ObjectType
		{
			get
			{
				return this.object_type;
			}
		}

		// Token: 0x0400173B RID: 5947
		private Guid inherited_object_type;

		// Token: 0x0400173C RID: 5948
		private Guid object_type;
	}
}
