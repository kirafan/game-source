﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.AccessControl
{
	// Token: 0x02000582 RID: 1410
	public abstract class NativeObjectSecurity : CommonObjectSecurity
	{
		// Token: 0x0600369D RID: 13981 RVA: 0x000B23B0 File Offset: 0x000B05B0
		internal NativeObjectSecurity() : base(false)
		{
		}

		// Token: 0x0600369E RID: 13982 RVA: 0x000B23BC File Offset: 0x000B05BC
		protected NativeObjectSecurity(bool isContainer, ResourceType resourceType) : base(isContainer)
		{
		}

		// Token: 0x0600369F RID: 13983 RVA: 0x000B23C8 File Offset: 0x000B05C8
		protected NativeObjectSecurity(bool isContainer, ResourceType resourceType, NativeObjectSecurity.ExceptionFromErrorCode exceptionFromErrorCode, object exceptionContext) : this(isContainer, resourceType)
		{
		}

		// Token: 0x060036A0 RID: 13984 RVA: 0x000B23D4 File Offset: 0x000B05D4
		protected NativeObjectSecurity(bool isContainer, ResourceType resourceType, SafeHandle handle, AccessControlSections includeSections) : this(isContainer, resourceType)
		{
		}

		// Token: 0x060036A1 RID: 13985 RVA: 0x000B23E0 File Offset: 0x000B05E0
		protected NativeObjectSecurity(bool isContainer, ResourceType resourceType, string name, AccessControlSections includeSections) : this(isContainer, resourceType)
		{
		}

		// Token: 0x060036A2 RID: 13986 RVA: 0x000B23EC File Offset: 0x000B05EC
		protected NativeObjectSecurity(bool isContainer, ResourceType resourceType, SafeHandle handle, AccessControlSections includeSections, NativeObjectSecurity.ExceptionFromErrorCode exceptionFromErrorCode, object exceptionContext) : this(isContainer, resourceType, handle, includeSections)
		{
		}

		// Token: 0x060036A3 RID: 13987 RVA: 0x000B23FC File Offset: 0x000B05FC
		protected NativeObjectSecurity(bool isContainer, ResourceType resourceType, string name, AccessControlSections includeSections, NativeObjectSecurity.ExceptionFromErrorCode exceptionFromErrorCode, object exceptionContext) : this(isContainer, resourceType, name, includeSections)
		{
		}

		// Token: 0x060036A4 RID: 13988 RVA: 0x000B240C File Offset: 0x000B060C
		protected sealed override void Persist(SafeHandle handle, AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036A5 RID: 13989 RVA: 0x000B2414 File Offset: 0x000B0614
		protected sealed override void Persist(string name, AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036A6 RID: 13990 RVA: 0x000B241C File Offset: 0x000B061C
		protected void Persist(SafeHandle handle, AccessControlSections includeSections, object exceptionContext)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036A7 RID: 13991 RVA: 0x000B2424 File Offset: 0x000B0624
		protected void Persist(string name, AccessControlSections includeSections, object exceptionContext)
		{
			throw new NotImplementedException();
		}

		// Token: 0x020006E4 RID: 1764
		// (Invoke) Token: 0x06004384 RID: 17284
		protected internal delegate Exception ExceptionFromErrorCode(int errorCode, string name, SafeHandle handle, object context);
	}
}
