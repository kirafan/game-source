﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000574 RID: 1396
	public sealed class FileSecurity : FileSystemSecurity
	{
		// Token: 0x06003639 RID: 13881 RVA: 0x000B1F44 File Offset: 0x000B0144
		public FileSecurity() : base(false)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x0600363A RID: 13882 RVA: 0x000B1F54 File Offset: 0x000B0154
		public FileSecurity(string fileName, AccessControlSections includeSections) : base(false, fileName, includeSections)
		{
			throw new PlatformNotSupportedException();
		}
	}
}
