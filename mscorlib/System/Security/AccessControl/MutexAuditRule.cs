﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200057F RID: 1407
	public sealed class MutexAuditRule : AuditRule
	{
		// Token: 0x06003689 RID: 13961 RVA: 0x000B22EC File Offset: 0x000B04EC
		public MutexAuditRule(IdentityReference identity, MutexRights eventRights, AuditFlags flags) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, flags)
		{
			this.rights = eventRights;
		}

		// Token: 0x17000A46 RID: 2630
		// (get) Token: 0x0600368A RID: 13962 RVA: 0x000B2304 File Offset: 0x000B0504
		public MutexRights MutexRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x04001729 RID: 5929
		private MutexRights rights;
	}
}
