﻿using System;
using System.Collections.Generic;

namespace System.Security.AccessControl
{
	// Token: 0x02000563 RID: 1379
	[MonoTODO("required for NativeObjectSecurity - implementation is missing")]
	public abstract class CommonObjectSecurity : ObjectSecurity
	{
		// Token: 0x060035BB RID: 13755 RVA: 0x000B1708 File Offset: 0x000AF908
		protected CommonObjectSecurity(bool isContainer) : base(isContainer, false)
		{
		}

		// Token: 0x060035BC RID: 13756 RVA: 0x000B1728 File Offset: 0x000AF928
		public AuthorizationRuleCollection GetAccessRules(bool includeExplicit, bool includeInherited, Type targetType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035BD RID: 13757 RVA: 0x000B1730 File Offset: 0x000AF930
		public AuthorizationRuleCollection GetAuditRules(bool includeExplicit, bool includeInherited, Type targetType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035BE RID: 13758 RVA: 0x000B1738 File Offset: 0x000AF938
		protected void AddAccessRule(AccessRule rule)
		{
			this.access_rules.Add(rule);
			base.AccessRulesModified = true;
		}

		// Token: 0x060035BF RID: 13759 RVA: 0x000B1750 File Offset: 0x000AF950
		protected bool RemoveAccessRule(AccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035C0 RID: 13760 RVA: 0x000B1758 File Offset: 0x000AF958
		protected void RemoveAccessRuleAll(AccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035C1 RID: 13761 RVA: 0x000B1760 File Offset: 0x000AF960
		protected void RemoveAccessRuleSpecific(AccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035C2 RID: 13762 RVA: 0x000B1768 File Offset: 0x000AF968
		protected void ResetAccessRule(AccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035C3 RID: 13763 RVA: 0x000B1770 File Offset: 0x000AF970
		protected void SetAccessRule(AccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035C4 RID: 13764 RVA: 0x000B1778 File Offset: 0x000AF978
		protected override bool ModifyAccess(AccessControlModification modification, AccessRule rule, out bool modified)
		{
			foreach (AccessRule accessRule in this.access_rules)
			{
				if (rule == accessRule)
				{
					switch (modification)
					{
					case AccessControlModification.Add:
						this.AddAccessRule(rule);
						break;
					case AccessControlModification.Set:
						this.SetAccessRule(rule);
						break;
					case AccessControlModification.Reset:
						this.ResetAccessRule(rule);
						break;
					case AccessControlModification.Remove:
						this.RemoveAccessRule(rule);
						break;
					case AccessControlModification.RemoveAll:
						this.RemoveAccessRuleAll(rule);
						break;
					case AccessControlModification.RemoveSpecific:
						this.RemoveAccessRuleSpecific(rule);
						break;
					}
					modified = true;
					return true;
				}
			}
			modified = false;
			return false;
		}

		// Token: 0x060035C5 RID: 13765 RVA: 0x000B1864 File Offset: 0x000AFA64
		protected void AddAuditRule(AuditRule rule)
		{
			this.audit_rules.Add(rule);
			base.AuditRulesModified = true;
		}

		// Token: 0x060035C6 RID: 13766 RVA: 0x000B187C File Offset: 0x000AFA7C
		protected bool RemoveAuditRule(AuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035C7 RID: 13767 RVA: 0x000B1884 File Offset: 0x000AFA84
		protected void RemoveAuditRuleAll(AuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035C8 RID: 13768 RVA: 0x000B188C File Offset: 0x000AFA8C
		protected void RemoveAuditRuleSpecific(AuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035C9 RID: 13769 RVA: 0x000B1894 File Offset: 0x000AFA94
		protected void SetAuditRule(AuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035CA RID: 13770 RVA: 0x000B189C File Offset: 0x000AFA9C
		protected override bool ModifyAudit(AccessControlModification modification, AuditRule rule, out bool modified)
		{
			foreach (AuditRule auditRule in this.audit_rules)
			{
				if (rule == auditRule)
				{
					switch (modification)
					{
					case AccessControlModification.Add:
						this.AddAuditRule(rule);
						break;
					case AccessControlModification.Set:
						this.SetAuditRule(rule);
						break;
					case AccessControlModification.Remove:
						this.RemoveAuditRule(rule);
						break;
					case AccessControlModification.RemoveAll:
						this.RemoveAuditRuleAll(rule);
						break;
					case AccessControlModification.RemoveSpecific:
						this.RemoveAuditRuleSpecific(rule);
						break;
					}
					base.AuditRulesModified = true;
					modified = true;
					return true;
				}
			}
			modified = false;
			return false;
		}

		// Token: 0x040016C4 RID: 5828
		private List<AccessRule> access_rules = new List<AccessRule>();

		// Token: 0x040016C5 RID: 5829
		private List<AuditRule> audit_rules = new List<AuditRule>();
	}
}
