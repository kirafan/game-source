﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000584 RID: 1412
	public sealed class ObjectAce : QualifiedAce
	{
		// Token: 0x060036AC RID: 13996 RVA: 0x000B24A4 File Offset: 0x000B06A4
		public ObjectAce(AceFlags aceFlags, AceQualifier qualifier, int accessMask, SecurityIdentifier sid, ObjectAceFlags flags, Guid type, Guid inheritedType, bool isCallback, byte[] opaque) : base(InheritanceFlags.None, PropagationFlags.None, qualifier, isCallback, opaque)
		{
			base.AceFlags = aceFlags;
			base.SecurityIdentifier = sid;
			this.object_ace_flags = flags;
			this.object_ace_type = type;
			this.inherited_object_type = inheritedType;
		}

		// Token: 0x17000A4D RID: 2637
		// (get) Token: 0x060036AD RID: 13997 RVA: 0x000B24E8 File Offset: 0x000B06E8
		[MonoTODO]
		public override int BinaryLength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A4E RID: 2638
		// (get) Token: 0x060036AE RID: 13998 RVA: 0x000B24F0 File Offset: 0x000B06F0
		// (set) Token: 0x060036AF RID: 13999 RVA: 0x000B24F8 File Offset: 0x000B06F8
		public Guid InheritedObjectAceType
		{
			get
			{
				return this.inherited_object_type;
			}
			set
			{
				this.inherited_object_type = value;
			}
		}

		// Token: 0x17000A4F RID: 2639
		// (get) Token: 0x060036B0 RID: 14000 RVA: 0x000B2504 File Offset: 0x000B0704
		// (set) Token: 0x060036B1 RID: 14001 RVA: 0x000B250C File Offset: 0x000B070C
		public ObjectAceFlags ObjectAceFlags
		{
			get
			{
				return this.object_ace_flags;
			}
			set
			{
				this.object_ace_flags = value;
			}
		}

		// Token: 0x17000A50 RID: 2640
		// (get) Token: 0x060036B2 RID: 14002 RVA: 0x000B2518 File Offset: 0x000B0718
		// (set) Token: 0x060036B3 RID: 14003 RVA: 0x000B2520 File Offset: 0x000B0720
		public Guid ObjectAceType
		{
			get
			{
				return this.object_ace_type;
			}
			set
			{
				this.object_ace_type = value;
			}
		}

		// Token: 0x060036B4 RID: 14004 RVA: 0x000B252C File Offset: 0x000B072C
		[MonoTODO]
		public override void GetBinaryForm(byte[] binaryForm, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036B5 RID: 14005 RVA: 0x000B2534 File Offset: 0x000B0734
		[MonoTODO]
		public static int MaxOpaqueLength(bool isCallback)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04001734 RID: 5940
		private Guid object_ace_type;

		// Token: 0x04001735 RID: 5941
		private Guid inherited_object_type;

		// Token: 0x04001736 RID: 5942
		private ObjectAceFlags object_ace_flags;
	}
}
