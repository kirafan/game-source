﻿using System;
using System.Collections;

namespace System.Security.AccessControl
{
	// Token: 0x02000560 RID: 1376
	public sealed class AuthorizationRuleCollection : ReadOnlyCollectionBase
	{
		// Token: 0x060035A7 RID: 13735 RVA: 0x000B15E0 File Offset: 0x000AF7E0
		private AuthorizationRuleCollection(AuthorizationRule[] rules)
		{
			base.InnerList.AddRange(rules);
		}

		// Token: 0x17000A0C RID: 2572
		public AuthorizationRule this[int index]
		{
			get
			{
				return (AuthorizationRule)base.InnerList[index];
			}
		}

		// Token: 0x060035A9 RID: 13737 RVA: 0x000B1608 File Offset: 0x000AF808
		public void CopyTo(AuthorizationRule[] rules, int index)
		{
			base.InnerList.CopyTo(rules, index);
		}
	}
}
