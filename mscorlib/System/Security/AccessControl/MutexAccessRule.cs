﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200057E RID: 1406
	public sealed class MutexAccessRule : AccessRule
	{
		// Token: 0x06003686 RID: 13958 RVA: 0x000B22BC File Offset: 0x000B04BC
		public MutexAccessRule(IdentityReference identity, MutexRights eventRights, AccessControlType type) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, type)
		{
			this.rights = eventRights;
		}

		// Token: 0x06003687 RID: 13959 RVA: 0x000B22D4 File Offset: 0x000B04D4
		public MutexAccessRule(string identity, MutexRights eventRights, AccessControlType type) : this(new SecurityIdentifier(identity), eventRights, type)
		{
		}

		// Token: 0x17000A45 RID: 2629
		// (get) Token: 0x06003688 RID: 13960 RVA: 0x000B22E4 File Offset: 0x000B04E4
		public MutexRights MutexRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x04001728 RID: 5928
		private MutexRights rights;
	}
}
