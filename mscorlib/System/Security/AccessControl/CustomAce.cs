﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200056C RID: 1388
	public sealed class CustomAce : GenericAce
	{
		// Token: 0x060035FD RID: 13821 RVA: 0x000B1BF8 File Offset: 0x000AFDF8
		public CustomAce(AceType type, AceFlags flags, byte[] opaque) : base(type)
		{
			base.AceFlags = flags;
			this.SetOpaque(opaque);
		}

		// Token: 0x17000A25 RID: 2597
		// (get) Token: 0x060035FE RID: 13822 RVA: 0x000B1C10 File Offset: 0x000AFE10
		[MonoTODO]
		public override int BinaryLength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A26 RID: 2598
		// (get) Token: 0x060035FF RID: 13823 RVA: 0x000B1C18 File Offset: 0x000AFE18
		public int OpaqueLength
		{
			get
			{
				return this.opaque.Length;
			}
		}

		// Token: 0x06003600 RID: 13824 RVA: 0x000B1C24 File Offset: 0x000AFE24
		[MonoTODO]
		public override void GetBinaryForm(byte[] binaryForm, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003601 RID: 13825 RVA: 0x000B1C2C File Offset: 0x000AFE2C
		public byte[] GetOpaque()
		{
			return (byte[])this.opaque.Clone();
		}

		// Token: 0x06003602 RID: 13826 RVA: 0x000B1C40 File Offset: 0x000AFE40
		public void SetOpaque(byte[] opaque)
		{
			if (opaque == null)
			{
				throw new ArgumentNullException("opaque");
			}
			this.opaque = (byte[])opaque.Clone();
		}

		// Token: 0x040016F5 RID: 5877
		private byte[] opaque;

		// Token: 0x040016F6 RID: 5878
		[MonoTODO]
		public static readonly int MaxOpaqueLength;
	}
}
