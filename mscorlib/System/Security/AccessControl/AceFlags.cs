﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200055A RID: 1370
	[Flags]
	public enum AceFlags : byte
	{
		// Token: 0x04001693 RID: 5779
		None = 0,
		// Token: 0x04001694 RID: 5780
		ObjectInherit = 1,
		// Token: 0x04001695 RID: 5781
		ContainerInherit = 2,
		// Token: 0x04001696 RID: 5782
		NoPropagateInherit = 4,
		// Token: 0x04001697 RID: 5783
		InheritOnly = 8,
		// Token: 0x04001698 RID: 5784
		InheritanceFlags = 15,
		// Token: 0x04001699 RID: 5785
		Inherited = 16,
		// Token: 0x0400169A RID: 5786
		SuccessfulAccess = 64,
		// Token: 0x0400169B RID: 5787
		FailedAccess = 128,
		// Token: 0x0400169C RID: 5788
		AuditFlags = 192
	}
}
