﻿using System;
using System.Collections.Generic;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000562 RID: 1378
	public abstract class CommonAcl : GenericAcl
	{
		// Token: 0x060035AE RID: 13742 RVA: 0x000B1650 File Offset: 0x000AF850
		internal CommonAcl(bool isContainer, bool isDS, byte revision) : this(isContainer, isDS, revision, 10)
		{
		}

		// Token: 0x060035AF RID: 13743 RVA: 0x000B1660 File Offset: 0x000AF860
		internal CommonAcl(bool isContainer, bool isDS, byte revision, int capacity)
		{
			this.is_container = isContainer;
			this.is_ds = isDS;
			this.revision = revision;
			this.list = new List<GenericAce>(capacity);
		}

		// Token: 0x17000A0E RID: 2574
		// (get) Token: 0x060035B0 RID: 13744 RVA: 0x000B1698 File Offset: 0x000AF898
		[MonoTODO]
		public sealed override int BinaryLength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A0F RID: 2575
		// (get) Token: 0x060035B1 RID: 13745 RVA: 0x000B16A0 File Offset: 0x000AF8A0
		public sealed override int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000A10 RID: 2576
		// (get) Token: 0x060035B2 RID: 13746 RVA: 0x000B16B0 File Offset: 0x000AF8B0
		[MonoTODO]
		public bool IsCanonical
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A11 RID: 2577
		// (get) Token: 0x060035B3 RID: 13747 RVA: 0x000B16B8 File Offset: 0x000AF8B8
		public bool IsContainer
		{
			get
			{
				return this.is_container;
			}
		}

		// Token: 0x17000A12 RID: 2578
		// (get) Token: 0x060035B4 RID: 13748 RVA: 0x000B16C0 File Offset: 0x000AF8C0
		public bool IsDS
		{
			get
			{
				return this.is_ds;
			}
		}

		// Token: 0x17000A13 RID: 2579
		public sealed override GenericAce this[int index]
		{
			get
			{
				return this.list[index];
			}
			set
			{
				this.list[index] = value;
			}
		}

		// Token: 0x17000A14 RID: 2580
		// (get) Token: 0x060035B7 RID: 13751 RVA: 0x000B16E8 File Offset: 0x000AF8E8
		public sealed override byte Revision
		{
			get
			{
				return this.revision;
			}
		}

		// Token: 0x060035B8 RID: 13752 RVA: 0x000B16F0 File Offset: 0x000AF8F0
		[MonoTODO]
		public sealed override void GetBinaryForm(byte[] binaryForm, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035B9 RID: 13753 RVA: 0x000B16F8 File Offset: 0x000AF8F8
		[MonoTODO]
		public void Purge(SecurityIdentifier sid)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035BA RID: 13754 RVA: 0x000B1700 File Offset: 0x000AF900
		[MonoTODO]
		public void RemoveInheritedAces()
		{
			throw new NotImplementedException();
		}

		// Token: 0x040016BF RID: 5823
		private const int default_capacity = 10;

		// Token: 0x040016C0 RID: 5824
		private bool is_container;

		// Token: 0x040016C1 RID: 5825
		private bool is_ds;

		// Token: 0x040016C2 RID: 5826
		private byte revision;

		// Token: 0x040016C3 RID: 5827
		private List<GenericAce> list;
	}
}
