﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200058F RID: 1423
	[Flags]
	public enum RegistryRights
	{
		// Token: 0x0400174F RID: 5967
		QueryValues = 1,
		// Token: 0x04001750 RID: 5968
		SetValue = 2,
		// Token: 0x04001751 RID: 5969
		CreateSubKey = 4,
		// Token: 0x04001752 RID: 5970
		EnumerateSubKeys = 8,
		// Token: 0x04001753 RID: 5971
		Notify = 16,
		// Token: 0x04001754 RID: 5972
		CreateLink = 32,
		// Token: 0x04001755 RID: 5973
		Delete = 65536,
		// Token: 0x04001756 RID: 5974
		ReadPermissions = 131072,
		// Token: 0x04001757 RID: 5975
		WriteKey = 131078,
		// Token: 0x04001758 RID: 5976
		ReadKey = 131097,
		// Token: 0x04001759 RID: 5977
		ExecuteKey = 131097,
		// Token: 0x0400175A RID: 5978
		ChangePermissions = 262144,
		// Token: 0x0400175B RID: 5979
		TakeOwnership = 524288,
		// Token: 0x0400175C RID: 5980
		FullControl = 983103
	}
}
