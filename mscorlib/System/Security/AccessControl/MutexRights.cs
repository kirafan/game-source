﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000580 RID: 1408
	[Flags]
	public enum MutexRights
	{
		// Token: 0x0400172B RID: 5931
		Modify = 1,
		// Token: 0x0400172C RID: 5932
		Delete = 65536,
		// Token: 0x0400172D RID: 5933
		ReadPermissions = 131072,
		// Token: 0x0400172E RID: 5934
		ChangePermissions = 262144,
		// Token: 0x0400172F RID: 5935
		TakeOwnership = 524288,
		// Token: 0x04001730 RID: 5936
		Synchronize = 1048576,
		// Token: 0x04001731 RID: 5937
		FullControl = 2031617
	}
}
