﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200058A RID: 1418
	public abstract class QualifiedAce : KnownAce
	{
		// Token: 0x060036EE RID: 14062 RVA: 0x000B2744 File Offset: 0x000B0944
		internal QualifiedAce(InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AceQualifier aceQualifier, bool isCallback, byte[] opaque) : base(inheritanceFlags, propagationFlags)
		{
			this.ace_qualifier = aceQualifier;
			this.is_callback = isCallback;
			this.SetOpaque(opaque);
		}

		// Token: 0x17000A62 RID: 2658
		// (get) Token: 0x060036EF RID: 14063 RVA: 0x000B2768 File Offset: 0x000B0968
		public AceQualifier AceQualifier
		{
			get
			{
				return this.ace_qualifier;
			}
		}

		// Token: 0x17000A63 RID: 2659
		// (get) Token: 0x060036F0 RID: 14064 RVA: 0x000B2770 File Offset: 0x000B0970
		public bool IsCallback
		{
			get
			{
				return this.is_callback;
			}
		}

		// Token: 0x17000A64 RID: 2660
		// (get) Token: 0x060036F1 RID: 14065 RVA: 0x000B2778 File Offset: 0x000B0978
		public int OpaqueLength
		{
			get
			{
				return this.opaque.Length;
			}
		}

		// Token: 0x060036F2 RID: 14066 RVA: 0x000B2784 File Offset: 0x000B0984
		public byte[] GetOpaque()
		{
			return (byte[])this.opaque.Clone();
		}

		// Token: 0x060036F3 RID: 14067 RVA: 0x000B2798 File Offset: 0x000B0998
		public void SetOpaque(byte[] opaque)
		{
			if (opaque == null)
			{
				throw new ArgumentNullException("opaque");
			}
			this.opaque = (byte[])opaque.Clone();
		}

		// Token: 0x04001747 RID: 5959
		private AceQualifier ace_qualifier;

		// Token: 0x04001748 RID: 5960
		private bool is_callback;

		// Token: 0x04001749 RID: 5961
		private byte[] opaque;
	}
}
