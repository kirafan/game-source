﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x0200057C RID: 1404
	[Flags]
	public enum InheritanceFlags
	{
		// Token: 0x04001723 RID: 5923
		None = 0,
		// Token: 0x04001724 RID: 5924
		ContainerInherit = 1,
		// Token: 0x04001725 RID: 5925
		ObjectInherit = 2
	}
}
