﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000577 RID: 1399
	[Flags]
	public enum FileSystemRights
	{
		// Token: 0x04001704 RID: 5892
		ListDirectory = 1,
		// Token: 0x04001705 RID: 5893
		ReadData = 1,
		// Token: 0x04001706 RID: 5894
		CreateFiles = 2,
		// Token: 0x04001707 RID: 5895
		WriteData = 2,
		// Token: 0x04001708 RID: 5896
		AppendData = 4,
		// Token: 0x04001709 RID: 5897
		CreateDirectories = 4,
		// Token: 0x0400170A RID: 5898
		ReadExtendedAttributes = 8,
		// Token: 0x0400170B RID: 5899
		WriteExtendedAttributes = 16,
		// Token: 0x0400170C RID: 5900
		ExecuteFile = 32,
		// Token: 0x0400170D RID: 5901
		Traverse = 32,
		// Token: 0x0400170E RID: 5902
		DeleteSubdirectoriesAndFiles = 64,
		// Token: 0x0400170F RID: 5903
		ReadAttributes = 128,
		// Token: 0x04001710 RID: 5904
		WriteAttributes = 256,
		// Token: 0x04001711 RID: 5905
		Write = 278,
		// Token: 0x04001712 RID: 5906
		Delete = 65536,
		// Token: 0x04001713 RID: 5907
		ReadPermissions = 131072,
		// Token: 0x04001714 RID: 5908
		Read = 131209,
		// Token: 0x04001715 RID: 5909
		ReadAndExecute = 131241,
		// Token: 0x04001716 RID: 5910
		Modify = 197055,
		// Token: 0x04001717 RID: 5911
		ChangePermissions = 262144,
		// Token: 0x04001718 RID: 5912
		TakeOwnership = 524288,
		// Token: 0x04001719 RID: 5913
		Synchronize = 1048576,
		// Token: 0x0400171A RID: 5914
		FullControl = 2032127
	}
}
