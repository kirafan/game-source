﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000561 RID: 1377
	public sealed class CommonAce : QualifiedAce
	{
		// Token: 0x060035AA RID: 13738 RVA: 0x000B1618 File Offset: 0x000AF818
		public CommonAce(AceFlags flags, AceQualifier qualifier, int accessMask, SecurityIdentifier sid, bool isCallback, byte[] opaque) : base(InheritanceFlags.None, PropagationFlags.None, qualifier, isCallback, opaque)
		{
			base.AccessMask = accessMask;
			base.SecurityIdentifier = sid;
		}

		// Token: 0x17000A0D RID: 2573
		// (get) Token: 0x060035AB RID: 13739 RVA: 0x000B1638 File Offset: 0x000AF838
		[MonoTODO]
		public override int BinaryLength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x060035AC RID: 13740 RVA: 0x000B1640 File Offset: 0x000AF840
		[MonoTODO]
		public override void GetBinaryForm(byte[] binaryForm, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060035AD RID: 13741 RVA: 0x000B1648 File Offset: 0x000AF848
		[MonoTODO]
		public static int MaxOpaqueLength(bool isCallback)
		{
			throw new NotImplementedException();
		}
	}
}
