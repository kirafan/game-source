﻿using System;
using System.Collections;

namespace System.Security.AccessControl
{
	// Token: 0x02000559 RID: 1369
	public sealed class AceEnumerator : IEnumerator
	{
		// Token: 0x06003599 RID: 13721 RVA: 0x000B1490 File Offset: 0x000AF690
		internal AceEnumerator(GenericAcl owner)
		{
			this.owner = owner;
		}

		// Token: 0x17000A04 RID: 2564
		// (get) Token: 0x0600359A RID: 13722 RVA: 0x000B14A8 File Offset: 0x000AF6A8
		object IEnumerator.Current
		{
			get
			{
				return this.Current;
			}
		}

		// Token: 0x17000A05 RID: 2565
		// (get) Token: 0x0600359B RID: 13723 RVA: 0x000B14B0 File Offset: 0x000AF6B0
		public GenericAce Current
		{
			get
			{
				return (this.current >= 0) ? this.owner[this.current] : null;
			}
		}

		// Token: 0x0600359C RID: 13724 RVA: 0x000B14D8 File Offset: 0x000AF6D8
		public bool MoveNext()
		{
			if (this.current + 1 == this.owner.Count)
			{
				return false;
			}
			this.current++;
			return true;
		}

		// Token: 0x0600359D RID: 13725 RVA: 0x000B1504 File Offset: 0x000AF704
		public void Reset()
		{
			this.current = -1;
		}

		// Token: 0x04001690 RID: 5776
		private GenericAcl owner;

		// Token: 0x04001691 RID: 5777
		private int current = -1;
	}
}
