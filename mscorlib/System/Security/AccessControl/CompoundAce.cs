﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000565 RID: 1381
	public sealed class CompoundAce : KnownAce
	{
		// Token: 0x060035E0 RID: 13792 RVA: 0x000B1A9C File Offset: 0x000AFC9C
		public CompoundAce(AceFlags flags, int accessMask, CompoundAceType compoundAceType, SecurityIdentifier sid) : base(InheritanceFlags.None, PropagationFlags.None)
		{
			this.compound_ace_type = compoundAceType;
			base.AceFlags = flags;
			base.AccessMask = accessMask;
			base.SecurityIdentifier = sid;
		}

		// Token: 0x17000A1E RID: 2590
		// (get) Token: 0x060035E1 RID: 13793 RVA: 0x000B1AD0 File Offset: 0x000AFCD0
		[MonoTODO]
		public override int BinaryLength
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A1F RID: 2591
		// (get) Token: 0x060035E2 RID: 13794 RVA: 0x000B1AD8 File Offset: 0x000AFCD8
		// (set) Token: 0x060035E3 RID: 13795 RVA: 0x000B1AE0 File Offset: 0x000AFCE0
		public CompoundAceType CompoundAceType
		{
			get
			{
				return this.compound_ace_type;
			}
			set
			{
				this.compound_ace_type = value;
			}
		}

		// Token: 0x060035E4 RID: 13796 RVA: 0x000B1AEC File Offset: 0x000AFCEC
		[MonoTODO]
		public override void GetBinaryForm(byte[] binaryForm, int offset)
		{
			throw new NotImplementedException();
		}

		// Token: 0x040016CD RID: 5837
		private CompoundAceType compound_ace_type;
	}
}
