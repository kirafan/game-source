﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x0200058D RID: 1421
	public sealed class RegistryAccessRule : AccessRule
	{
		// Token: 0x0600370D RID: 14093 RVA: 0x000B28F4 File Offset: 0x000B0AF4
		public RegistryAccessRule(IdentityReference identity, RegistryRights registryRights, AccessControlType type) : this(identity, registryRights, InheritanceFlags.None, PropagationFlags.None, type)
		{
		}

		// Token: 0x0600370E RID: 14094 RVA: 0x000B2904 File Offset: 0x000B0B04
		public RegistryAccessRule(string identity, RegistryRights registryRights, AccessControlType type) : this(new SecurityIdentifier(identity), registryRights, type)
		{
		}

		// Token: 0x0600370F RID: 14095 RVA: 0x000B2914 File Offset: 0x000B0B14
		public RegistryAccessRule(IdentityReference identity, RegistryRights registryRights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type) : base(identity, 0, false, inheritanceFlags, propagationFlags, type)
		{
			this.rights = registryRights;
		}

		// Token: 0x06003710 RID: 14096 RVA: 0x000B292C File Offset: 0x000B0B2C
		public RegistryAccessRule(string identity, RegistryRights registryRights, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type) : this(new SecurityIdentifier(identity), registryRights, inheritanceFlags, propagationFlags, type)
		{
		}

		// Token: 0x17000A6F RID: 2671
		// (get) Token: 0x06003711 RID: 14097 RVA: 0x000B2940 File Offset: 0x000B0B40
		public RegistryRights RegistryRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x0400174C RID: 5964
		private RegistryRights rights;
	}
}
