﻿using System;

namespace System.Security.AccessControl
{
	// Token: 0x02000572 RID: 1394
	[Flags]
	public enum EventWaitHandleRights
	{
		// Token: 0x040016FA RID: 5882
		Modify = 2,
		// Token: 0x040016FB RID: 5883
		Delete = 65536,
		// Token: 0x040016FC RID: 5884
		ReadPermissions = 131072,
		// Token: 0x040016FD RID: 5885
		ChangePermissions = 262144,
		// Token: 0x040016FE RID: 5886
		TakeOwnership = 524288,
		// Token: 0x040016FF RID: 5887
		Synchronize = 1048576,
		// Token: 0x04001700 RID: 5888
		FullControl = 2031619
	}
}
