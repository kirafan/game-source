﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000587 RID: 1415
	public abstract class ObjectSecurity
	{
		// Token: 0x060036BA RID: 14010 RVA: 0x000B25B4 File Offset: 0x000B07B4
		internal ObjectSecurity()
		{
		}

		// Token: 0x060036BB RID: 14011 RVA: 0x000B25BC File Offset: 0x000B07BC
		protected ObjectSecurity(bool isContainer, bool isDS)
		{
			this.is_container = isContainer;
			this.is_ds = isDS;
		}

		// Token: 0x17000A54 RID: 2644
		// (get) Token: 0x060036BC RID: 14012
		public abstract Type AccessRightType { get; }

		// Token: 0x17000A55 RID: 2645
		// (get) Token: 0x060036BD RID: 14013
		public abstract Type AccessRuleType { get; }

		// Token: 0x17000A56 RID: 2646
		// (get) Token: 0x060036BE RID: 14014
		public abstract Type AuditRuleType { get; }

		// Token: 0x17000A57 RID: 2647
		// (get) Token: 0x060036BF RID: 14015 RVA: 0x000B25D4 File Offset: 0x000B07D4
		[MonoTODO]
		public bool AreAccessRulesCanonical
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A58 RID: 2648
		// (get) Token: 0x060036C0 RID: 14016 RVA: 0x000B25DC File Offset: 0x000B07DC
		[MonoTODO]
		public bool AreAccessRulesProtected
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A59 RID: 2649
		// (get) Token: 0x060036C1 RID: 14017 RVA: 0x000B25E4 File Offset: 0x000B07E4
		[MonoTODO]
		public bool AreAuditRulesCanonical
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A5A RID: 2650
		// (get) Token: 0x060036C2 RID: 14018 RVA: 0x000B25EC File Offset: 0x000B07EC
		[MonoTODO]
		public bool AreAuditRulesProtected
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000A5B RID: 2651
		// (get) Token: 0x060036C3 RID: 14019 RVA: 0x000B25F4 File Offset: 0x000B07F4
		// (set) Token: 0x060036C4 RID: 14020 RVA: 0x000B25FC File Offset: 0x000B07FC
		protected bool AccessRulesModified
		{
			get
			{
				return this.access_rules_modified;
			}
			set
			{
				this.access_rules_modified = value;
			}
		}

		// Token: 0x17000A5C RID: 2652
		// (get) Token: 0x060036C5 RID: 14021 RVA: 0x000B2608 File Offset: 0x000B0808
		// (set) Token: 0x060036C6 RID: 14022 RVA: 0x000B2610 File Offset: 0x000B0810
		protected bool AuditRulesModified
		{
			get
			{
				return this.audit_rules_modified;
			}
			set
			{
				this.audit_rules_modified = value;
			}
		}

		// Token: 0x17000A5D RID: 2653
		// (get) Token: 0x060036C7 RID: 14023 RVA: 0x000B261C File Offset: 0x000B081C
		// (set) Token: 0x060036C8 RID: 14024 RVA: 0x000B2624 File Offset: 0x000B0824
		protected bool GroupModified
		{
			get
			{
				return this.group_modified;
			}
			set
			{
				this.group_modified = value;
			}
		}

		// Token: 0x17000A5E RID: 2654
		// (get) Token: 0x060036C9 RID: 14025 RVA: 0x000B2630 File Offset: 0x000B0830
		protected bool IsContainer
		{
			get
			{
				return this.is_container;
			}
		}

		// Token: 0x17000A5F RID: 2655
		// (get) Token: 0x060036CA RID: 14026 RVA: 0x000B2638 File Offset: 0x000B0838
		protected bool IsDS
		{
			get
			{
				return this.is_ds;
			}
		}

		// Token: 0x17000A60 RID: 2656
		// (get) Token: 0x060036CB RID: 14027 RVA: 0x000B2640 File Offset: 0x000B0840
		// (set) Token: 0x060036CC RID: 14028 RVA: 0x000B2648 File Offset: 0x000B0848
		protected bool OwnerModified
		{
			get
			{
				return this.owner_modified;
			}
			set
			{
				this.owner_modified = value;
			}
		}

		// Token: 0x060036CD RID: 14029
		public abstract AccessRule AccessRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type);

		// Token: 0x060036CE RID: 14030
		public abstract AuditRule AuditRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags);

		// Token: 0x060036CF RID: 14031 RVA: 0x000B2654 File Offset: 0x000B0854
		[MonoTODO]
		public IdentityReference GetGroup(Type targetType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D0 RID: 14032 RVA: 0x000B265C File Offset: 0x000B085C
		[MonoTODO]
		public IdentityReference GetOwner(Type targetType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D1 RID: 14033 RVA: 0x000B2664 File Offset: 0x000B0864
		[MonoTODO]
		public byte[] GetSecurityDescriptorBinaryForm()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D2 RID: 14034 RVA: 0x000B266C File Offset: 0x000B086C
		[MonoTODO]
		public string GetSecurityDescriptorSddlForm(AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D3 RID: 14035 RVA: 0x000B2674 File Offset: 0x000B0874
		[MonoTODO]
		public static bool IsSddlConversionSupported()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D4 RID: 14036 RVA: 0x000B267C File Offset: 0x000B087C
		[MonoTODO]
		public virtual bool ModifyAccessRule(AccessControlModification modification, AccessRule rule, out bool modified)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D5 RID: 14037 RVA: 0x000B2684 File Offset: 0x000B0884
		[MonoTODO]
		public virtual bool ModifyAuditRule(AccessControlModification modification, AuditRule rule, out bool modified)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D6 RID: 14038 RVA: 0x000B268C File Offset: 0x000B088C
		[MonoTODO]
		public virtual void PurgeAccessRules(IdentityReference identity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D7 RID: 14039 RVA: 0x000B2694 File Offset: 0x000B0894
		[MonoTODO]
		public virtual void PurgeAuditRules(IdentityReference identity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D8 RID: 14040 RVA: 0x000B269C File Offset: 0x000B089C
		[MonoTODO]
		public void SetAccessRuleProtection(bool isProtected, bool preserveInheritance)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036D9 RID: 14041 RVA: 0x000B26A4 File Offset: 0x000B08A4
		[MonoTODO]
		public void SetAuditRuleProtection(bool isProtected, bool preserveInheritance)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036DA RID: 14042 RVA: 0x000B26AC File Offset: 0x000B08AC
		[MonoTODO]
		public void SetGroup(IdentityReference identity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036DB RID: 14043 RVA: 0x000B26B4 File Offset: 0x000B08B4
		[MonoTODO]
		public void SetOwner(IdentityReference identity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036DC RID: 14044 RVA: 0x000B26BC File Offset: 0x000B08BC
		[MonoTODO]
		public void SetSecurityDescriptorBinaryForm(byte[] binaryForm)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036DD RID: 14045 RVA: 0x000B26C4 File Offset: 0x000B08C4
		[MonoTODO]
		public void SetSecurityDescriptorBinaryForm(byte[] binaryForm, AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036DE RID: 14046 RVA: 0x000B26CC File Offset: 0x000B08CC
		[MonoTODO]
		public void SetSecurityDescriptorSddlForm(string sddlForm)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036DF RID: 14047 RVA: 0x000B26D4 File Offset: 0x000B08D4
		[MonoTODO]
		public void SetSecurityDescriptorSddlForm(string sddlForm, AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036E0 RID: 14048
		protected abstract bool ModifyAccess(AccessControlModification modification, AccessRule rule, out bool modified);

		// Token: 0x060036E1 RID: 14049
		protected abstract bool ModifyAudit(AccessControlModification modification, AuditRule rule, out bool modified);

		// Token: 0x060036E2 RID: 14050 RVA: 0x000B26DC File Offset: 0x000B08DC
		[MonoTODO]
		protected virtual void Persist(SafeHandle handle, AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036E3 RID: 14051 RVA: 0x000B26E4 File Offset: 0x000B08E4
		[MonoTODO]
		protected virtual void Persist(string name, AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036E4 RID: 14052 RVA: 0x000B26EC File Offset: 0x000B08EC
		[MonoTODO]
		protected virtual void Persist(bool enableOwnershipPrivilege, string name, AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036E5 RID: 14053 RVA: 0x000B26F4 File Offset: 0x000B08F4
		[MonoTODO]
		protected void ReadLock()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036E6 RID: 14054 RVA: 0x000B26FC File Offset: 0x000B08FC
		[MonoTODO]
		protected void ReadUnlock()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036E7 RID: 14055 RVA: 0x000B2704 File Offset: 0x000B0904
		[MonoTODO]
		protected void WriteLock()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060036E8 RID: 14056 RVA: 0x000B270C File Offset: 0x000B090C
		[MonoTODO]
		protected void WriteUnlock()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0400173D RID: 5949
		private bool is_container;

		// Token: 0x0400173E RID: 5950
		private bool is_ds;

		// Token: 0x0400173F RID: 5951
		private bool access_rules_modified;

		// Token: 0x04001740 RID: 5952
		private bool audit_rules_modified;

		// Token: 0x04001741 RID: 5953
		private bool group_modified;

		// Token: 0x04001742 RID: 5954
		private bool owner_modified;
	}
}
