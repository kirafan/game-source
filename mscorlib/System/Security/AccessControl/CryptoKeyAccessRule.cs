﻿using System;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000568 RID: 1384
	public sealed class CryptoKeyAccessRule : AccessRule
	{
		// Token: 0x060035E5 RID: 13797 RVA: 0x000B1AF4 File Offset: 0x000AFCF4
		public CryptoKeyAccessRule(IdentityReference identity, CryptoKeyRights cryptoKeyRights, AccessControlType type) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, AccessControlType.Allow)
		{
			this.rights = cryptoKeyRights;
		}

		// Token: 0x060035E6 RID: 13798 RVA: 0x000B1B0C File Offset: 0x000AFD0C
		public CryptoKeyAccessRule(string identity, CryptoKeyRights cryptoKeyRights, AccessControlType type) : this(new SecurityIdentifier(identity), cryptoKeyRights, type)
		{
		}

		// Token: 0x17000A20 RID: 2592
		// (get) Token: 0x060035E7 RID: 13799 RVA: 0x000B1B1C File Offset: 0x000AFD1C
		public CryptoKeyRights CryptoKeyRights
		{
			get
			{
				return this.rights;
			}
		}

		// Token: 0x040016E2 RID: 5858
		private CryptoKeyRights rights;
	}
}
