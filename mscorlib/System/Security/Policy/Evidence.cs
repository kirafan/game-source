﻿using System;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;
using Mono.Security.Authenticode;

namespace System.Security.Policy
{
	// Token: 0x0200063A RID: 1594
	[MonoTODO("Serialization format not compatible with .NET")]
	[ComVisible(true)]
	[Serializable]
	public sealed class Evidence : IEnumerable, ICollection
	{
		// Token: 0x06003CB0 RID: 15536 RVA: 0x000D09DC File Offset: 0x000CEBDC
		public Evidence()
		{
		}

		// Token: 0x06003CB1 RID: 15537 RVA: 0x000D09E4 File Offset: 0x000CEBE4
		public Evidence(Evidence evidence)
		{
			if (evidence != null)
			{
				this.Merge(evidence);
			}
		}

		// Token: 0x06003CB2 RID: 15538 RVA: 0x000D09FC File Offset: 0x000CEBFC
		public Evidence(object[] hostEvidence, object[] assemblyEvidence)
		{
			if (hostEvidence != null)
			{
				this.HostEvidenceList.AddRange(hostEvidence);
			}
			if (assemblyEvidence != null)
			{
				this.AssemblyEvidenceList.AddRange(assemblyEvidence);
			}
		}

		// Token: 0x17000B7F RID: 2943
		// (get) Token: 0x06003CB3 RID: 15539 RVA: 0x000D0A34 File Offset: 0x000CEC34
		public int Count
		{
			get
			{
				int num = 0;
				if (this.hostEvidenceList != null)
				{
					num += this.hostEvidenceList.Count;
				}
				if (this.assemblyEvidenceList != null)
				{
					num += this.assemblyEvidenceList.Count;
				}
				return num;
			}
		}

		// Token: 0x17000B80 RID: 2944
		// (get) Token: 0x06003CB4 RID: 15540 RVA: 0x000D0A78 File Offset: 0x000CEC78
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000B81 RID: 2945
		// (get) Token: 0x06003CB5 RID: 15541 RVA: 0x000D0A7C File Offset: 0x000CEC7C
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000B82 RID: 2946
		// (get) Token: 0x06003CB6 RID: 15542 RVA: 0x000D0A80 File Offset: 0x000CEC80
		// (set) Token: 0x06003CB7 RID: 15543 RVA: 0x000D0A88 File Offset: 0x000CEC88
		public bool Locked
		{
			get
			{
				return this._locked;
			}
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence\"/>\n</PermissionSet>\n")]
			set
			{
				this._locked = value;
			}
		}

		// Token: 0x17000B83 RID: 2947
		// (get) Token: 0x06003CB8 RID: 15544 RVA: 0x000D0A94 File Offset: 0x000CEC94
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000B84 RID: 2948
		// (get) Token: 0x06003CB9 RID: 15545 RVA: 0x000D0A98 File Offset: 0x000CEC98
		internal ArrayList HostEvidenceList
		{
			get
			{
				if (this.hostEvidenceList == null)
				{
					this.hostEvidenceList = ArrayList.Synchronized(new ArrayList());
				}
				return this.hostEvidenceList;
			}
		}

		// Token: 0x17000B85 RID: 2949
		// (get) Token: 0x06003CBA RID: 15546 RVA: 0x000D0ABC File Offset: 0x000CECBC
		internal ArrayList AssemblyEvidenceList
		{
			get
			{
				if (this.assemblyEvidenceList == null)
				{
					this.assemblyEvidenceList = ArrayList.Synchronized(new ArrayList());
				}
				return this.assemblyEvidenceList;
			}
		}

		// Token: 0x06003CBB RID: 15547 RVA: 0x000D0AE0 File Offset: 0x000CECE0
		public void AddAssembly(object id)
		{
			this.AssemblyEvidenceList.Add(id);
			this._hashCode = 0;
		}

		// Token: 0x06003CBC RID: 15548 RVA: 0x000D0AF8 File Offset: 0x000CECF8
		public void AddHost(object id)
		{
			if (this._locked && SecurityManager.SecurityEnabled)
			{
				new SecurityPermission(SecurityPermissionFlag.ControlEvidence).Demand();
			}
			this.HostEvidenceList.Add(id);
			this._hashCode = 0;
		}

		// Token: 0x06003CBD RID: 15549 RVA: 0x000D0B3C File Offset: 0x000CED3C
		[ComVisible(false)]
		public void Clear()
		{
			if (this.hostEvidenceList != null)
			{
				this.hostEvidenceList.Clear();
			}
			if (this.assemblyEvidenceList != null)
			{
				this.assemblyEvidenceList.Clear();
			}
			this._hashCode = 0;
		}

		// Token: 0x06003CBE RID: 15550 RVA: 0x000D0B74 File Offset: 0x000CED74
		public void CopyTo(Array array, int index)
		{
			int num = 0;
			if (this.hostEvidenceList != null)
			{
				num = this.hostEvidenceList.Count;
				if (num > 0)
				{
					this.hostEvidenceList.CopyTo(array, index);
				}
			}
			if (this.assemblyEvidenceList != null && this.assemblyEvidenceList.Count > 0)
			{
				this.assemblyEvidenceList.CopyTo(array, index + num);
			}
		}

		// Token: 0x06003CBF RID: 15551 RVA: 0x000D0BDC File Offset: 0x000CEDDC
		[ComVisible(false)]
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			Evidence evidence = obj as Evidence;
			if (evidence == null)
			{
				return false;
			}
			if (this.HostEvidenceList.Count != evidence.HostEvidenceList.Count)
			{
				return false;
			}
			if (this.AssemblyEvidenceList.Count != evidence.AssemblyEvidenceList.Count)
			{
				return false;
			}
			for (int i = 0; i < this.hostEvidenceList.Count; i++)
			{
				bool flag = false;
				int j = 0;
				while (j < evidence.hostEvidenceList.Count)
				{
					if (this.hostEvidenceList[i].Equals(evidence.hostEvidenceList[j]))
					{
						flag = true;
						break;
					}
					i++;
				}
				if (!flag)
				{
					return false;
				}
			}
			for (int k = 0; k < this.assemblyEvidenceList.Count; k++)
			{
				bool flag2 = false;
				int l = 0;
				while (l < evidence.assemblyEvidenceList.Count)
				{
					if (this.assemblyEvidenceList[k].Equals(evidence.assemblyEvidenceList[l]))
					{
						flag2 = true;
						break;
					}
					k++;
				}
				if (!flag2)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06003CC0 RID: 15552 RVA: 0x000D0D20 File Offset: 0x000CEF20
		public IEnumerator GetEnumerator()
		{
			IEnumerator hostenum = null;
			if (this.hostEvidenceList != null)
			{
				hostenum = this.hostEvidenceList.GetEnumerator();
			}
			IEnumerator assemblyenum = null;
			if (this.assemblyEvidenceList != null)
			{
				assemblyenum = this.assemblyEvidenceList.GetEnumerator();
			}
			return new Evidence.EvidenceEnumerator(hostenum, assemblyenum);
		}

		// Token: 0x06003CC1 RID: 15553 RVA: 0x000D0D68 File Offset: 0x000CEF68
		public IEnumerator GetAssemblyEnumerator()
		{
			return this.AssemblyEvidenceList.GetEnumerator();
		}

		// Token: 0x06003CC2 RID: 15554 RVA: 0x000D0D78 File Offset: 0x000CEF78
		[ComVisible(false)]
		public override int GetHashCode()
		{
			if (this._hashCode == 0)
			{
				if (this.hostEvidenceList != null)
				{
					for (int i = 0; i < this.hostEvidenceList.Count; i++)
					{
						this._hashCode ^= this.hostEvidenceList[i].GetHashCode();
					}
				}
				if (this.assemblyEvidenceList != null)
				{
					for (int j = 0; j < this.assemblyEvidenceList.Count; j++)
					{
						this._hashCode ^= this.assemblyEvidenceList[j].GetHashCode();
					}
				}
			}
			return this._hashCode;
		}

		// Token: 0x06003CC3 RID: 15555 RVA: 0x000D0E20 File Offset: 0x000CF020
		public IEnumerator GetHostEnumerator()
		{
			return this.HostEvidenceList.GetEnumerator();
		}

		// Token: 0x06003CC4 RID: 15556 RVA: 0x000D0E30 File Offset: 0x000CF030
		public void Merge(Evidence evidence)
		{
			if (evidence != null && evidence.Count > 0)
			{
				if (evidence.hostEvidenceList != null)
				{
					foreach (object id in evidence.hostEvidenceList)
					{
						this.AddHost(id);
					}
				}
				if (evidence.assemblyEvidenceList != null)
				{
					foreach (object id2 in evidence.assemblyEvidenceList)
					{
						this.AddAssembly(id2);
					}
				}
				this._hashCode = 0;
			}
		}

		// Token: 0x06003CC5 RID: 15557 RVA: 0x000D0F28 File Offset: 0x000CF128
		[ComVisible(false)]
		public void RemoveType(Type t)
		{
			for (int i = this.hostEvidenceList.Count; i >= 0; i--)
			{
				if (this.hostEvidenceList.GetType() == t)
				{
					this.hostEvidenceList.RemoveAt(i);
					this._hashCode = 0;
				}
			}
			for (int j = this.assemblyEvidenceList.Count; j >= 0; j--)
			{
				if (this.assemblyEvidenceList.GetType() == t)
				{
					this.assemblyEvidenceList.RemoveAt(j);
					this._hashCode = 0;
				}
			}
		}

		// Token: 0x06003CC6 RID: 15558
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsAuthenticodePresent(Assembly a);

		// Token: 0x06003CC7 RID: 15559 RVA: 0x000D0FB8 File Offset: 0x000CF1B8
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Unrestricted=\"true\"/>\n</PermissionSet>\n")]
		internal static Evidence GetDefaultHostEvidence(Assembly a)
		{
			Evidence evidence = new Evidence();
			string escapedCodeBase = a.EscapedCodeBase;
			evidence.AddHost(Zone.CreateFromUrl(escapedCodeBase));
			evidence.AddHost(new Url(escapedCodeBase));
			evidence.AddHost(new Hash(a));
			if (string.Compare("FILE://", 0, escapedCodeBase, 0, 7, true, CultureInfo.InvariantCulture) != 0)
			{
				evidence.AddHost(Site.CreateFromUrl(escapedCodeBase));
			}
			AssemblyName assemblyName = a.UnprotectedGetName();
			byte[] publicKey = assemblyName.GetPublicKey();
			if (publicKey != null && publicKey.Length > 0)
			{
				StrongNamePublicKeyBlob blob = new StrongNamePublicKeyBlob(publicKey);
				evidence.AddHost(new StrongName(blob, assemblyName.Name, assemblyName.Version));
			}
			if (Evidence.IsAuthenticodePresent(a))
			{
				AuthenticodeDeformatter authenticodeDeformatter = new AuthenticodeDeformatter(a.Location);
				if (authenticodeDeformatter.SigningCertificate != null)
				{
					X509Certificate x509Certificate = new X509Certificate(authenticodeDeformatter.SigningCertificate.RawData);
					if (x509Certificate.GetHashCode() != 0)
					{
						evidence.AddHost(new Publisher(x509Certificate));
					}
				}
			}
			if (a.GlobalAssemblyCache)
			{
				evidence.AddHost(new GacInstalled());
			}
			AppDomainManager domainManager = AppDomain.CurrentDomain.DomainManager;
			if (domainManager != null && (domainManager.HostSecurityManager.Flags & HostSecurityManagerOptions.HostAssemblyEvidence) == HostSecurityManagerOptions.HostAssemblyEvidence)
			{
				evidence = domainManager.HostSecurityManager.ProvideAssemblyEvidence(a, evidence);
			}
			return evidence;
		}

		// Token: 0x04001A6B RID: 6763
		private bool _locked;

		// Token: 0x04001A6C RID: 6764
		private ArrayList hostEvidenceList;

		// Token: 0x04001A6D RID: 6765
		private ArrayList assemblyEvidenceList;

		// Token: 0x04001A6E RID: 6766
		private int _hashCode;

		// Token: 0x0200063B RID: 1595
		private class EvidenceEnumerator : IEnumerator
		{
			// Token: 0x06003CC8 RID: 15560 RVA: 0x000D10F8 File Offset: 0x000CF2F8
			public EvidenceEnumerator(IEnumerator hostenum, IEnumerator assemblyenum)
			{
				this.hostEnum = hostenum;
				this.assemblyEnum = assemblyenum;
				this.currentEnum = this.hostEnum;
			}

			// Token: 0x06003CC9 RID: 15561 RVA: 0x000D1128 File Offset: 0x000CF328
			public bool MoveNext()
			{
				if (this.currentEnum == null)
				{
					return false;
				}
				bool flag = this.currentEnum.MoveNext();
				if (!flag && this.hostEnum == this.currentEnum && this.assemblyEnum != null)
				{
					this.currentEnum = this.assemblyEnum;
					flag = this.assemblyEnum.MoveNext();
				}
				return flag;
			}

			// Token: 0x06003CCA RID: 15562 RVA: 0x000D118C File Offset: 0x000CF38C
			public void Reset()
			{
				if (this.hostEnum != null)
				{
					this.hostEnum.Reset();
					this.currentEnum = this.hostEnum;
				}
				else
				{
					this.currentEnum = this.assemblyEnum;
				}
				if (this.assemblyEnum != null)
				{
					this.assemblyEnum.Reset();
				}
			}

			// Token: 0x17000B86 RID: 2950
			// (get) Token: 0x06003CCB RID: 15563 RVA: 0x000D11E4 File Offset: 0x000CF3E4
			public object Current
			{
				get
				{
					return this.currentEnum.Current;
				}
			}

			// Token: 0x04001A6F RID: 6767
			private IEnumerator currentEnum;

			// Token: 0x04001A70 RID: 6768
			private IEnumerator hostEnum;

			// Token: 0x04001A71 RID: 6769
			private IEnumerator assemblyEnum;
		}
	}
}
