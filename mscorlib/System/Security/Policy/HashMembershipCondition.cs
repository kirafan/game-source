﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using Mono.Security.Cryptography;

namespace System.Security.Policy
{
	// Token: 0x02000641 RID: 1601
	[ComVisible(true)]
	[Serializable]
	public sealed class HashMembershipCondition : ISerializable, IDeserializationCallback, ISecurityEncodable, ISecurityPolicyEncodable, IMembershipCondition
	{
		// Token: 0x06003D00 RID: 15616 RVA: 0x000D1BBC File Offset: 0x000CFDBC
		internal HashMembershipCondition()
		{
		}

		// Token: 0x06003D01 RID: 15617 RVA: 0x000D1BCC File Offset: 0x000CFDCC
		public HashMembershipCondition(HashAlgorithm hashAlg, byte[] value)
		{
			if (hashAlg == null)
			{
				throw new ArgumentNullException("hashAlg");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			this.hash_algorithm = hashAlg;
			this.hash_value = (byte[])value.Clone();
		}

		// Token: 0x06003D02 RID: 15618 RVA: 0x000D1C20 File Offset: 0x000CFE20
		[MonoTODO("fx 2.0")]
		void IDeserializationCallback.OnDeserialization(object sender)
		{
		}

		// Token: 0x06003D03 RID: 15619 RVA: 0x000D1C24 File Offset: 0x000CFE24
		[MonoTODO("fx 2.0")]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
		}

		// Token: 0x17000B8D RID: 2957
		// (get) Token: 0x06003D04 RID: 15620 RVA: 0x000D1C28 File Offset: 0x000CFE28
		// (set) Token: 0x06003D05 RID: 15621 RVA: 0x000D1C48 File Offset: 0x000CFE48
		public HashAlgorithm HashAlgorithm
		{
			get
			{
				if (this.hash_algorithm == null)
				{
					this.hash_algorithm = new SHA1Managed();
				}
				return this.hash_algorithm;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("HashAlgorithm");
				}
				this.hash_algorithm = value;
			}
		}

		// Token: 0x17000B8E RID: 2958
		// (get) Token: 0x06003D06 RID: 15622 RVA: 0x000D1C64 File Offset: 0x000CFE64
		// (set) Token: 0x06003D07 RID: 15623 RVA: 0x000D1C94 File Offset: 0x000CFE94
		public byte[] HashValue
		{
			get
			{
				if (this.hash_value == null)
				{
					throw new ArgumentException(Locale.GetText("No HashValue available."));
				}
				return (byte[])this.hash_value.Clone();
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("HashValue");
				}
				this.hash_value = (byte[])value.Clone();
			}
		}

		// Token: 0x06003D08 RID: 15624 RVA: 0x000D1CC4 File Offset: 0x000CFEC4
		public bool Check(Evidence evidence)
		{
			if (evidence == null)
			{
				return false;
			}
			IEnumerator hostEnumerator = evidence.GetHostEnumerator();
			while (hostEnumerator.MoveNext())
			{
				object obj = hostEnumerator.Current;
				Hash hash = obj as Hash;
				if (hash != null)
				{
					if (this.Compare(this.hash_value, hash.GenerateHash(this.hash_algorithm)))
					{
						return true;
					}
					break;
				}
			}
			return false;
		}

		// Token: 0x06003D09 RID: 15625 RVA: 0x000D1D2C File Offset: 0x000CFF2C
		public IMembershipCondition Copy()
		{
			return new HashMembershipCondition(this.hash_algorithm, this.hash_value);
		}

		// Token: 0x06003D0A RID: 15626 RVA: 0x000D1D40 File Offset: 0x000CFF40
		public override bool Equals(object o)
		{
			HashMembershipCondition hashMembershipCondition = o as HashMembershipCondition;
			return hashMembershipCondition != null && hashMembershipCondition.HashAlgorithm == this.hash_algorithm && this.Compare(this.hash_value, hashMembershipCondition.hash_value);
		}

		// Token: 0x06003D0B RID: 15627 RVA: 0x000D1D84 File Offset: 0x000CFF84
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003D0C RID: 15628 RVA: 0x000D1D90 File Offset: 0x000CFF90
		public SecurityElement ToXml(PolicyLevel level)
		{
			SecurityElement securityElement = MembershipConditionHelper.Element(typeof(HashMembershipCondition), this.version);
			securityElement.AddAttribute("HashValue", CryptoConvert.ToHex(this.HashValue));
			securityElement.AddAttribute("HashAlgorithm", this.hash_algorithm.GetType().FullName);
			return securityElement;
		}

		// Token: 0x06003D0D RID: 15629 RVA: 0x000D1DE8 File Offset: 0x000CFFE8
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003D0E RID: 15630 RVA: 0x000D1DF4 File Offset: 0x000CFFF4
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			MembershipConditionHelper.CheckSecurityElement(e, "e", this.version, this.version);
			this.hash_value = CryptoConvert.FromHex(e.Attribute("HashValue"));
			string text = e.Attribute("HashAlgorithm");
			this.hash_algorithm = ((text != null) ? HashAlgorithm.Create(text) : null);
		}

		// Token: 0x06003D0F RID: 15631 RVA: 0x000D1E54 File Offset: 0x000D0054
		public override int GetHashCode()
		{
			int num = this.hash_algorithm.GetType().GetHashCode();
			if (this.hash_value != null)
			{
				foreach (byte b in this.hash_value)
				{
					num ^= (int)b;
				}
			}
			return num;
		}

		// Token: 0x06003D10 RID: 15632 RVA: 0x000D1EA4 File Offset: 0x000D00A4
		public override string ToString()
		{
			Type type = this.HashAlgorithm.GetType();
			return string.Format("Hash - {0} {1} = {2}", type.FullName, type.Assembly, CryptoConvert.ToHex(this.HashValue));
		}

		// Token: 0x06003D11 RID: 15633 RVA: 0x000D1EE0 File Offset: 0x000D00E0
		private bool Compare(byte[] expected, byte[] actual)
		{
			if (expected.Length != actual.Length)
			{
				return false;
			}
			int num = expected.Length;
			for (int i = 0; i < num; i++)
			{
				if (expected[i] != actual[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x04001A78 RID: 6776
		private readonly int version = 1;

		// Token: 0x04001A79 RID: 6777
		private HashAlgorithm hash_algorithm;

		// Token: 0x04001A7A RID: 6778
		private byte[] hash_value;
	}
}
