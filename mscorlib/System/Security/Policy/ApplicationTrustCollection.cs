﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000632 RID: 1586
	[ComVisible(true)]
	public sealed class ApplicationTrustCollection : IEnumerable, ICollection
	{
		// Token: 0x06003C5E RID: 15454 RVA: 0x000CF6D0 File Offset: 0x000CD8D0
		internal ApplicationTrustCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x06003C5F RID: 15455 RVA: 0x000CF6E4 File Offset: 0x000CD8E4
		void ICollection.CopyTo(Array array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x06003C60 RID: 15456 RVA: 0x000CF6F4 File Offset: 0x000CD8F4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new ApplicationTrustEnumerator(this);
		}

		// Token: 0x17000B67 RID: 2919
		// (get) Token: 0x06003C61 RID: 15457 RVA: 0x000CF6FC File Offset: 0x000CD8FC
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x17000B68 RID: 2920
		// (get) Token: 0x06003C62 RID: 15458 RVA: 0x000CF70C File Offset: 0x000CD90C
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000B69 RID: 2921
		// (get) Token: 0x06003C63 RID: 15459 RVA: 0x000CF710 File Offset: 0x000CD910
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000B6A RID: 2922
		public ApplicationTrust this[int index]
		{
			get
			{
				return (ApplicationTrust)this._list[index];
			}
		}

		// Token: 0x17000B6B RID: 2923
		public ApplicationTrust this[string appFullName]
		{
			get
			{
				for (int i = 0; i < this._list.Count; i++)
				{
					ApplicationTrust applicationTrust = this._list[i] as ApplicationTrust;
					if (applicationTrust.ApplicationIdentity.FullName == appFullName)
					{
						return applicationTrust;
					}
				}
				return null;
			}
		}

		// Token: 0x06003C66 RID: 15462 RVA: 0x000CF77C File Offset: 0x000CD97C
		public int Add(ApplicationTrust trust)
		{
			if (trust == null)
			{
				throw new ArgumentNullException("trust");
			}
			if (trust.ApplicationIdentity == null)
			{
				throw new ArgumentException(Locale.GetText("ApplicationTrust.ApplicationIdentity can't be null."), "trust");
			}
			return this._list.Add(trust);
		}

		// Token: 0x06003C67 RID: 15463 RVA: 0x000CF7BC File Offset: 0x000CD9BC
		public void AddRange(ApplicationTrust[] trusts)
		{
			if (trusts == null)
			{
				throw new ArgumentNullException("trusts");
			}
			foreach (ApplicationTrust applicationTrust in trusts)
			{
				if (applicationTrust.ApplicationIdentity == null)
				{
					throw new ArgumentException(Locale.GetText("ApplicationTrust.ApplicationIdentity can't be null."), "trust");
				}
				this._list.Add(applicationTrust);
			}
		}

		// Token: 0x06003C68 RID: 15464 RVA: 0x000CF824 File Offset: 0x000CDA24
		public void AddRange(ApplicationTrustCollection trusts)
		{
			if (trusts == null)
			{
				throw new ArgumentNullException("trusts");
			}
			foreach (ApplicationTrust applicationTrust in trusts)
			{
				if (applicationTrust.ApplicationIdentity == null)
				{
					throw new ArgumentException(Locale.GetText("ApplicationTrust.ApplicationIdentity can't be null."), "trust");
				}
				this._list.Add(applicationTrust);
			}
		}

		// Token: 0x06003C69 RID: 15465 RVA: 0x000CF890 File Offset: 0x000CDA90
		public void Clear()
		{
			this._list.Clear();
		}

		// Token: 0x06003C6A RID: 15466 RVA: 0x000CF8A0 File Offset: 0x000CDAA0
		public void CopyTo(ApplicationTrust[] array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x06003C6B RID: 15467 RVA: 0x000CF8B0 File Offset: 0x000CDAB0
		public ApplicationTrustCollection Find(ApplicationIdentity applicationIdentity, ApplicationVersionMatch versionMatch)
		{
			if (applicationIdentity == null)
			{
				throw new ArgumentNullException("applicationIdentity");
			}
			string text = applicationIdentity.FullName;
			if (versionMatch != ApplicationVersionMatch.MatchExactVersion)
			{
				if (versionMatch != ApplicationVersionMatch.MatchAllVersions)
				{
					throw new ArgumentException("versionMatch");
				}
				int num = text.IndexOf(", Version=");
				if (num >= 0)
				{
					text = text.Substring(0, num);
				}
			}
			ApplicationTrustCollection applicationTrustCollection = new ApplicationTrustCollection();
			foreach (object obj in this._list)
			{
				ApplicationTrust applicationTrust = (ApplicationTrust)obj;
				if (applicationTrust.ApplicationIdentity.FullName.StartsWith(text))
				{
					applicationTrustCollection.Add(applicationTrust);
				}
			}
			return applicationTrustCollection;
		}

		// Token: 0x06003C6C RID: 15468 RVA: 0x000CF9A4 File Offset: 0x000CDBA4
		public ApplicationTrustEnumerator GetEnumerator()
		{
			return new ApplicationTrustEnumerator(this);
		}

		// Token: 0x06003C6D RID: 15469 RVA: 0x000CF9AC File Offset: 0x000CDBAC
		public void Remove(ApplicationTrust trust)
		{
			if (trust == null)
			{
				throw new ArgumentNullException("trust");
			}
			if (trust.ApplicationIdentity == null)
			{
				throw new ArgumentException(Locale.GetText("ApplicationTrust.ApplicationIdentity can't be null."), "trust");
			}
			this.RemoveAllInstances(trust);
		}

		// Token: 0x06003C6E RID: 15470 RVA: 0x000CF9F4 File Offset: 0x000CDBF4
		public void Remove(ApplicationIdentity applicationIdentity, ApplicationVersionMatch versionMatch)
		{
			ApplicationTrustCollection applicationTrustCollection = this.Find(applicationIdentity, versionMatch);
			foreach (ApplicationTrust trust in applicationTrustCollection)
			{
				this.RemoveAllInstances(trust);
			}
		}

		// Token: 0x06003C6F RID: 15471 RVA: 0x000CFA30 File Offset: 0x000CDC30
		public void RemoveRange(ApplicationTrust[] trusts)
		{
			if (trusts == null)
			{
				throw new ArgumentNullException("trusts");
			}
			foreach (ApplicationTrust trust in trusts)
			{
				this.RemoveAllInstances(trust);
			}
		}

		// Token: 0x06003C70 RID: 15472 RVA: 0x000CFA70 File Offset: 0x000CDC70
		public void RemoveRange(ApplicationTrustCollection trusts)
		{
			if (trusts == null)
			{
				throw new ArgumentNullException("trusts");
			}
			foreach (ApplicationTrust trust in trusts)
			{
				this.RemoveAllInstances(trust);
			}
		}

		// Token: 0x06003C71 RID: 15473 RVA: 0x000CFAB4 File Offset: 0x000CDCB4
		internal void RemoveAllInstances(ApplicationTrust trust)
		{
			for (int i = this._list.Count - 1; i >= 0; i--)
			{
				if (trust.Equals(this._list[i]))
				{
					this._list.RemoveAt(i);
				}
			}
		}

		// Token: 0x04001A35 RID: 6709
		private ArrayList _list;
	}
}
