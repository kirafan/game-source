﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x0200062D RID: 1581
	[ComVisible(true)]
	[Serializable]
	public sealed class ApplicationDirectory : IBuiltInEvidence
	{
		// Token: 0x06003C2E RID: 15406 RVA: 0x000CEE7C File Offset: 0x000CD07C
		public ApplicationDirectory(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length < 1)
			{
				throw new FormatException(Locale.GetText("Empty"));
			}
			this.directory = name;
		}

		// Token: 0x06003C2F RID: 15407 RVA: 0x000CEEC4 File Offset: 0x000CD0C4
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return ((!verbose) ? 1 : 3) + this.directory.Length;
		}

		// Token: 0x06003C30 RID: 15408 RVA: 0x000CEEE0 File Offset: 0x000CD0E0
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return 0;
		}

		// Token: 0x06003C31 RID: 15409 RVA: 0x000CEEE4 File Offset: 0x000CD0E4
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			return 0;
		}

		// Token: 0x17000B5B RID: 2907
		// (get) Token: 0x06003C32 RID: 15410 RVA: 0x000CEEE8 File Offset: 0x000CD0E8
		public string Directory
		{
			get
			{
				return this.directory;
			}
		}

		// Token: 0x06003C33 RID: 15411 RVA: 0x000CEEF0 File Offset: 0x000CD0F0
		public object Copy()
		{
			return new ApplicationDirectory(this.Directory);
		}

		// Token: 0x06003C34 RID: 15412 RVA: 0x000CEF00 File Offset: 0x000CD100
		public override bool Equals(object o)
		{
			ApplicationDirectory applicationDirectory = o as ApplicationDirectory;
			if (applicationDirectory != null)
			{
				this.ThrowOnInvalid(applicationDirectory.directory);
				return this.directory == applicationDirectory.directory;
			}
			return false;
		}

		// Token: 0x06003C35 RID: 15413 RVA: 0x000CEF3C File Offset: 0x000CD13C
		public override int GetHashCode()
		{
			return this.Directory.GetHashCode();
		}

		// Token: 0x06003C36 RID: 15414 RVA: 0x000CEF4C File Offset: 0x000CD14C
		public override string ToString()
		{
			this.ThrowOnInvalid(this.Directory);
			SecurityElement securityElement = new SecurityElement("System.Security.Policy.ApplicationDirectory");
			securityElement.AddAttribute("version", "1");
			securityElement.AddChild(new SecurityElement("Directory", this.directory));
			return securityElement.ToString();
		}

		// Token: 0x06003C37 RID: 15415 RVA: 0x000CEF9C File Offset: 0x000CD19C
		private void ThrowOnInvalid(string appdir)
		{
			if (appdir.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				string text = Locale.GetText("Invalid character(s) in directory {0}");
				throw new ArgumentException(string.Format(text, appdir), "other");
			}
		}

		// Token: 0x04001A25 RID: 6693
		private string directory;
	}
}
