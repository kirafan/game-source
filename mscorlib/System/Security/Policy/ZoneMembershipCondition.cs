﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x0200065B RID: 1627
	[ComVisible(true)]
	[Serializable]
	public sealed class ZoneMembershipCondition : ISecurityEncodable, ISecurityPolicyEncodable, IConstantMembershipCondition, IMembershipCondition
	{
		// Token: 0x06003DFA RID: 15866 RVA: 0x000D5794 File Offset: 0x000D3994
		internal ZoneMembershipCondition()
		{
		}

		// Token: 0x06003DFB RID: 15867 RVA: 0x000D57A4 File Offset: 0x000D39A4
		public ZoneMembershipCondition(SecurityZone zone)
		{
			this.SecurityZone = zone;
		}

		// Token: 0x17000BB2 RID: 2994
		// (get) Token: 0x06003DFC RID: 15868 RVA: 0x000D57BC File Offset: 0x000D39BC
		// (set) Token: 0x06003DFD RID: 15869 RVA: 0x000D57C4 File Offset: 0x000D39C4
		public SecurityZone SecurityZone
		{
			get
			{
				return this.zone;
			}
			set
			{
				if (!Enum.IsDefined(typeof(SecurityZone), value))
				{
					throw new ArgumentException(Locale.GetText("invalid zone"));
				}
				if (value == SecurityZone.NoZone)
				{
					throw new ArgumentException(Locale.GetText("NoZone isn't valid for membership condition"));
				}
				this.zone = value;
			}
		}

		// Token: 0x06003DFE RID: 15870 RVA: 0x000D581C File Offset: 0x000D3A1C
		public bool Check(Evidence evidence)
		{
			if (evidence == null)
			{
				return false;
			}
			IEnumerator hostEnumerator = evidence.GetHostEnumerator();
			while (hostEnumerator.MoveNext())
			{
				object obj = hostEnumerator.Current;
				Zone zone = obj as Zone;
				if (zone != null && zone.SecurityZone == this.zone)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003DFF RID: 15871 RVA: 0x000D5870 File Offset: 0x000D3A70
		public IMembershipCondition Copy()
		{
			return new ZoneMembershipCondition(this.zone);
		}

		// Token: 0x06003E00 RID: 15872 RVA: 0x000D5880 File Offset: 0x000D3A80
		public override bool Equals(object o)
		{
			ZoneMembershipCondition zoneMembershipCondition = o as ZoneMembershipCondition;
			return zoneMembershipCondition != null && zoneMembershipCondition.SecurityZone == this.zone;
		}

		// Token: 0x06003E01 RID: 15873 RVA: 0x000D58AC File Offset: 0x000D3AAC
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003E02 RID: 15874 RVA: 0x000D58B8 File Offset: 0x000D3AB8
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			MembershipConditionHelper.CheckSecurityElement(e, "e", this.version, this.version);
			string text = e.Attribute("Zone");
			if (text != null)
			{
				this.zone = (SecurityZone)((int)Enum.Parse(typeof(SecurityZone), text));
			}
		}

		// Token: 0x06003E03 RID: 15875 RVA: 0x000D590C File Offset: 0x000D3B0C
		public override int GetHashCode()
		{
			return this.zone.GetHashCode();
		}

		// Token: 0x06003E04 RID: 15876 RVA: 0x000D5920 File Offset: 0x000D3B20
		public override string ToString()
		{
			return "Zone - " + this.zone;
		}

		// Token: 0x06003E05 RID: 15877 RVA: 0x000D5938 File Offset: 0x000D3B38
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003E06 RID: 15878 RVA: 0x000D5944 File Offset: 0x000D3B44
		public SecurityElement ToXml(PolicyLevel level)
		{
			SecurityElement securityElement = MembershipConditionHelper.Element(typeof(ZoneMembershipCondition), this.version);
			securityElement.AddAttribute("Zone", this.zone.ToString());
			return securityElement;
		}

		// Token: 0x04001AAF RID: 6831
		private readonly int version = 1;

		// Token: 0x04001AB0 RID: 6832
		private SecurityZone zone;
	}
}
