﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Security.Policy
{
	// Token: 0x0200064B RID: 1611
	[ComVisible(true)]
	[Serializable]
	public class PolicyException : SystemException, _Exception
	{
		// Token: 0x06003D3D RID: 15677 RVA: 0x000D28BC File Offset: 0x000D0ABC
		public PolicyException() : base(Locale.GetText("Cannot run because of policy."))
		{
		}

		// Token: 0x06003D3E RID: 15678 RVA: 0x000D28D0 File Offset: 0x000D0AD0
		public PolicyException(string message) : base(message)
		{
		}

		// Token: 0x06003D3F RID: 15679 RVA: 0x000D28DC File Offset: 0x000D0ADC
		protected PolicyException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06003D40 RID: 15680 RVA: 0x000D28E8 File Offset: 0x000D0AE8
		public PolicyException(string message, Exception exception) : base(message, exception)
		{
		}
	}
}
