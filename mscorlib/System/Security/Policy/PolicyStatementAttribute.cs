﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x0200064E RID: 1614
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum PolicyStatementAttribute
	{
		// Token: 0x04001A8F RID: 6799
		Nothing = 0,
		// Token: 0x04001A90 RID: 6800
		Exclusive = 1,
		// Token: 0x04001A91 RID: 6801
		LevelFinal = 2,
		// Token: 0x04001A92 RID: 6802
		All = 3
	}
}
