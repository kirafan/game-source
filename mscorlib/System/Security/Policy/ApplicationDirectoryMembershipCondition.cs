﻿using System;
using System.Collections;
using System.Globalization;
using System.Reflection;
using System.Runtime.InteropServices;
using Mono.Security;

namespace System.Security.Policy
{
	// Token: 0x0200062E RID: 1582
	[ComVisible(true)]
	[Serializable]
	public sealed class ApplicationDirectoryMembershipCondition : ISecurityEncodable, ISecurityPolicyEncodable, IConstantMembershipCondition, IMembershipCondition
	{
		// Token: 0x06003C39 RID: 15417 RVA: 0x000CEFE8 File Offset: 0x000CD1E8
		public bool Check(Evidence evidence)
		{
			if (evidence == null)
			{
				return false;
			}
			string codeBase = Assembly.GetCallingAssembly().CodeBase;
			Uri uri = new Uri(codeBase);
			Url url = new Url(codeBase);
			bool flag = false;
			bool flag2 = false;
			IEnumerator hostEnumerator = evidence.GetHostEnumerator();
			while (hostEnumerator.MoveNext())
			{
				object obj = hostEnumerator.Current;
				if (!flag && obj is ApplicationDirectory)
				{
					ApplicationDirectory applicationDirectory = obj as ApplicationDirectory;
					string directory = applicationDirectory.Directory;
					flag = (string.Compare(directory, 0, uri.ToString(), 0, directory.Length, true, CultureInfo.InvariantCulture) == 0);
				}
				else if (!flag2 && obj is Url)
				{
					flag2 = url.Equals(obj);
				}
				if (flag && flag2)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003C3A RID: 15418 RVA: 0x000CF0B4 File Offset: 0x000CD2B4
		public IMembershipCondition Copy()
		{
			return new ApplicationDirectoryMembershipCondition();
		}

		// Token: 0x06003C3B RID: 15419 RVA: 0x000CF0BC File Offset: 0x000CD2BC
		public override bool Equals(object o)
		{
			return o is ApplicationDirectoryMembershipCondition;
		}

		// Token: 0x06003C3C RID: 15420 RVA: 0x000CF0C8 File Offset: 0x000CD2C8
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003C3D RID: 15421 RVA: 0x000CF0D4 File Offset: 0x000CD2D4
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			MembershipConditionHelper.CheckSecurityElement(e, "e", this.version, this.version);
		}

		// Token: 0x06003C3E RID: 15422 RVA: 0x000CF0F0 File Offset: 0x000CD2F0
		public override int GetHashCode()
		{
			return typeof(ApplicationDirectoryMembershipCondition).GetHashCode();
		}

		// Token: 0x06003C3F RID: 15423 RVA: 0x000CF104 File Offset: 0x000CD304
		public override string ToString()
		{
			return "ApplicationDirectory";
		}

		// Token: 0x06003C40 RID: 15424 RVA: 0x000CF10C File Offset: 0x000CD30C
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003C41 RID: 15425 RVA: 0x000CF118 File Offset: 0x000CD318
		public SecurityElement ToXml(PolicyLevel level)
		{
			return MembershipConditionHelper.Element(typeof(ApplicationDirectoryMembershipCondition), this.version);
		}

		// Token: 0x04001A26 RID: 6694
		private readonly int version = 1;
	}
}
