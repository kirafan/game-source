﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;

namespace System.Security.Policy
{
	// Token: 0x02000640 RID: 1600
	[ComVisible(true)]
	[Serializable]
	public sealed class Hash : ISerializable, IBuiltInEvidence
	{
		// Token: 0x06003CF2 RID: 15602 RVA: 0x000D18DC File Offset: 0x000CFADC
		public Hash(Assembly assembly)
		{
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			this.assembly = assembly;
		}

		// Token: 0x06003CF3 RID: 15603 RVA: 0x000D18FC File Offset: 0x000CFAFC
		internal Hash()
		{
		}

		// Token: 0x06003CF4 RID: 15604 RVA: 0x000D1904 File Offset: 0x000CFB04
		internal Hash(SerializationInfo info, StreamingContext context)
		{
			this.data = (byte[])info.GetValue("RawData", typeof(byte[]));
		}

		// Token: 0x06003CF5 RID: 15605 RVA: 0x000D1938 File Offset: 0x000CFB38
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return (!verbose) ? 0 : 5;
		}

		// Token: 0x06003CF6 RID: 15606 RVA: 0x000D1948 File Offset: 0x000CFB48
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return 0;
		}

		// Token: 0x06003CF7 RID: 15607 RVA: 0x000D194C File Offset: 0x000CFB4C
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			return 0;
		}

		// Token: 0x17000B8B RID: 2955
		// (get) Token: 0x06003CF8 RID: 15608 RVA: 0x000D1950 File Offset: 0x000CFB50
		public byte[] MD5
		{
			get
			{
				if (this._md5 != null)
				{
					return this._md5;
				}
				if (this.assembly == null && this._sha1 != null)
				{
					string text = Locale.GetText("No assembly data. This instance was initialized with an MSHA1 digest value.");
					throw new SecurityException(text);
				}
				HashAlgorithm hashAlg = System.Security.Cryptography.MD5.Create();
				this._md5 = this.GenerateHash(hashAlg);
				return this._md5;
			}
		}

		// Token: 0x17000B8C RID: 2956
		// (get) Token: 0x06003CF9 RID: 15609 RVA: 0x000D19B0 File Offset: 0x000CFBB0
		public byte[] SHA1
		{
			get
			{
				if (this._sha1 != null)
				{
					return this._sha1;
				}
				if (this.assembly == null && this._md5 != null)
				{
					string text = Locale.GetText("No assembly data. This instance was initialized with an MD5 digest value.");
					throw new SecurityException(text);
				}
				HashAlgorithm hashAlg = System.Security.Cryptography.SHA1.Create();
				this._sha1 = this.GenerateHash(hashAlg);
				return this._sha1;
			}
		}

		// Token: 0x06003CFA RID: 15610 RVA: 0x000D1A10 File Offset: 0x000CFC10
		public byte[] GenerateHash(HashAlgorithm hashAlg)
		{
			if (hashAlg == null)
			{
				throw new ArgumentNullException("hashAlg");
			}
			return hashAlg.ComputeHash(this.GetData());
		}

		// Token: 0x06003CFB RID: 15611 RVA: 0x000D1A30 File Offset: 0x000CFC30
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("RawData", this.GetData());
		}

		// Token: 0x06003CFC RID: 15612 RVA: 0x000D1A60 File Offset: 0x000CFC60
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement(base.GetType().FullName);
			securityElement.AddAttribute("version", "1");
			StringBuilder stringBuilder = new StringBuilder();
			byte[] array = this.GetData();
			for (int i = 0; i < array.Length; i++)
			{
				stringBuilder.Append(array[i].ToString("X2"));
			}
			securityElement.AddChild(new SecurityElement("RawData", stringBuilder.ToString()));
			return securityElement.ToString();
		}

		// Token: 0x06003CFD RID: 15613 RVA: 0x000D1AE4 File Offset: 0x000CFCE4
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Unrestricted=\"true\"/>\n</PermissionSet>\n")]
		private byte[] GetData()
		{
			if (this.assembly == null && this.data == null)
			{
				string text = Locale.GetText("No assembly data.");
				throw new SecurityException(text);
			}
			if (this.data == null)
			{
				FileStream fileStream = new FileStream(this.assembly.Location, FileMode.Open, FileAccess.Read);
				this.data = new byte[fileStream.Length];
				fileStream.Read(this.data, 0, (int)fileStream.Length);
			}
			return this.data;
		}

		// Token: 0x06003CFE RID: 15614 RVA: 0x000D1B64 File Offset: 0x000CFD64
		public static Hash CreateMD5(byte[] md5)
		{
			if (md5 == null)
			{
				throw new ArgumentNullException("md5");
			}
			return new Hash
			{
				_md5 = md5
			};
		}

		// Token: 0x06003CFF RID: 15615 RVA: 0x000D1B90 File Offset: 0x000CFD90
		public static Hash CreateSHA1(byte[] sha1)
		{
			if (sha1 == null)
			{
				throw new ArgumentNullException("sha1");
			}
			return new Hash
			{
				_sha1 = sha1
			};
		}

		// Token: 0x04001A74 RID: 6772
		private Assembly assembly;

		// Token: 0x04001A75 RID: 6773
		private byte[] data;

		// Token: 0x04001A76 RID: 6774
		internal byte[] _md5;

		// Token: 0x04001A77 RID: 6775
		internal byte[] _sha1;
	}
}
