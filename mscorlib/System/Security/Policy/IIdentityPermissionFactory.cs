﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000645 RID: 1605
	[ComVisible(true)]
	public interface IIdentityPermissionFactory
	{
		// Token: 0x06003D16 RID: 15638
		IPermission CreateIdentityPermission(Evidence evidence);
	}
}
