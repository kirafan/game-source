﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x0200063C RID: 1596
	[ComVisible(true)]
	[Serializable]
	public sealed class FileCodeGroup : CodeGroup
	{
		// Token: 0x06003CCC RID: 15564 RVA: 0x000D11F4 File Offset: 0x000CF3F4
		public FileCodeGroup(IMembershipCondition membershipCondition, FileIOPermissionAccess access) : base(membershipCondition, null)
		{
			this.m_access = access;
		}

		// Token: 0x06003CCD RID: 15565 RVA: 0x000D1208 File Offset: 0x000CF408
		internal FileCodeGroup(SecurityElement e, PolicyLevel level) : base(e, level)
		{
		}

		// Token: 0x06003CCE RID: 15566 RVA: 0x000D1214 File Offset: 0x000CF414
		public override CodeGroup Copy()
		{
			FileCodeGroup fileCodeGroup = new FileCodeGroup(base.MembershipCondition, this.m_access);
			fileCodeGroup.Name = base.Name;
			fileCodeGroup.Description = base.Description;
			foreach (object obj in base.Children)
			{
				CodeGroup codeGroup = (CodeGroup)obj;
				fileCodeGroup.AddChild(codeGroup.Copy());
			}
			return fileCodeGroup;
		}

		// Token: 0x17000B87 RID: 2951
		// (get) Token: 0x06003CCF RID: 15567 RVA: 0x000D12B4 File Offset: 0x000CF4B4
		public override string MergeLogic
		{
			get
			{
				return "Union";
			}
		}

		// Token: 0x06003CD0 RID: 15568 RVA: 0x000D12BC File Offset: 0x000CF4BC
		public override PolicyStatement Resolve(Evidence evidence)
		{
			if (evidence == null)
			{
				throw new ArgumentNullException("evidence");
			}
			if (!base.MembershipCondition.Check(evidence))
			{
				return null;
			}
			PermissionSet permissionSet = null;
			if (base.PolicyStatement == null)
			{
				permissionSet = new PermissionSet(PermissionState.None);
			}
			else
			{
				permissionSet = base.PolicyStatement.PermissionSet.Copy();
			}
			if (base.Children.Count > 0)
			{
				foreach (object obj in base.Children)
				{
					CodeGroup codeGroup = (CodeGroup)obj;
					PolicyStatement policyStatement = codeGroup.Resolve(evidence);
					if (policyStatement != null)
					{
						permissionSet = permissionSet.Union(policyStatement.PermissionSet);
					}
				}
			}
			PolicyStatement policyStatement2;
			if (base.PolicyStatement != null)
			{
				policyStatement2 = base.PolicyStatement.Copy();
			}
			else
			{
				policyStatement2 = PolicyStatement.Empty();
			}
			policyStatement2.PermissionSet = permissionSet;
			return policyStatement2;
		}

		// Token: 0x06003CD1 RID: 15569 RVA: 0x000D13D4 File Offset: 0x000CF5D4
		public override CodeGroup ResolveMatchingCodeGroups(Evidence evidence)
		{
			if (evidence == null)
			{
				throw new ArgumentNullException("evidence");
			}
			if (!base.MembershipCondition.Check(evidence))
			{
				return null;
			}
			FileCodeGroup fileCodeGroup = new FileCodeGroup(base.MembershipCondition, this.m_access);
			foreach (object obj in base.Children)
			{
				CodeGroup codeGroup = (CodeGroup)obj;
				CodeGroup codeGroup2 = codeGroup.ResolveMatchingCodeGroups(evidence);
				if (codeGroup2 != null)
				{
					fileCodeGroup.AddChild(codeGroup2);
				}
			}
			return fileCodeGroup;
		}

		// Token: 0x17000B88 RID: 2952
		// (get) Token: 0x06003CD2 RID: 15570 RVA: 0x000D148C File Offset: 0x000CF68C
		public override string AttributeString
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000B89 RID: 2953
		// (get) Token: 0x06003CD3 RID: 15571 RVA: 0x000D1490 File Offset: 0x000CF690
		public override string PermissionSetName
		{
			get
			{
				return "Same directory FileIO - " + this.m_access.ToString();
			}
		}

		// Token: 0x06003CD4 RID: 15572 RVA: 0x000D14AC File Offset: 0x000CF6AC
		public override bool Equals(object o)
		{
			return o is FileCodeGroup && this.m_access == ((FileCodeGroup)o).m_access && base.Equals((CodeGroup)o, false);
		}

		// Token: 0x06003CD5 RID: 15573 RVA: 0x000D14EC File Offset: 0x000CF6EC
		public override int GetHashCode()
		{
			return this.m_access.GetHashCode();
		}

		// Token: 0x06003CD6 RID: 15574 RVA: 0x000D1500 File Offset: 0x000CF700
		protected override void ParseXml(SecurityElement e, PolicyLevel level)
		{
			string text = e.Attribute("Access");
			if (text != null)
			{
				this.m_access = (FileIOPermissionAccess)((int)Enum.Parse(typeof(FileIOPermissionAccess), text, true));
			}
			else
			{
				this.m_access = FileIOPermissionAccess.NoAccess;
			}
		}

		// Token: 0x06003CD7 RID: 15575 RVA: 0x000D1548 File Offset: 0x000CF748
		protected override void CreateXml(SecurityElement element, PolicyLevel level)
		{
			element.AddAttribute("Access", this.m_access.ToString());
		}

		// Token: 0x04001A72 RID: 6770
		private FileIOPermissionAccess m_access;
	}
}
