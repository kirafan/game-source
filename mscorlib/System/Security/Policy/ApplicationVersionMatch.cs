﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000634 RID: 1588
	[ComVisible(true)]
	public enum ApplicationVersionMatch
	{
		// Token: 0x04001A38 RID: 6712
		MatchExactVersion,
		// Token: 0x04001A39 RID: 6713
		MatchAllVersions
	}
}
