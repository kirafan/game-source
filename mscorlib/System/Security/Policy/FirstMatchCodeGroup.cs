﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x0200063D RID: 1597
	[ComVisible(true)]
	[Serializable]
	public sealed class FirstMatchCodeGroup : CodeGroup
	{
		// Token: 0x06003CD8 RID: 15576 RVA: 0x000D1568 File Offset: 0x000CF768
		public FirstMatchCodeGroup(IMembershipCondition membershipCondition, PolicyStatement policy) : base(membershipCondition, policy)
		{
		}

		// Token: 0x06003CD9 RID: 15577 RVA: 0x000D1574 File Offset: 0x000CF774
		internal FirstMatchCodeGroup(SecurityElement e, PolicyLevel level) : base(e, level)
		{
		}

		// Token: 0x17000B8A RID: 2954
		// (get) Token: 0x06003CDA RID: 15578 RVA: 0x000D1580 File Offset: 0x000CF780
		public override string MergeLogic
		{
			get
			{
				return "First Match";
			}
		}

		// Token: 0x06003CDB RID: 15579 RVA: 0x000D1588 File Offset: 0x000CF788
		public override CodeGroup Copy()
		{
			FirstMatchCodeGroup firstMatchCodeGroup = this.CopyNoChildren();
			foreach (object obj in base.Children)
			{
				CodeGroup codeGroup = (CodeGroup)obj;
				firstMatchCodeGroup.AddChild(codeGroup.Copy());
			}
			return firstMatchCodeGroup;
		}

		// Token: 0x06003CDC RID: 15580 RVA: 0x000D1604 File Offset: 0x000CF804
		public override PolicyStatement Resolve(Evidence evidence)
		{
			if (evidence == null)
			{
				throw new ArgumentNullException("evidence");
			}
			if (!base.MembershipCondition.Check(evidence))
			{
				return null;
			}
			foreach (object obj in base.Children)
			{
				CodeGroup codeGroup = (CodeGroup)obj;
				PolicyStatement policyStatement = codeGroup.Resolve(evidence);
				if (policyStatement != null)
				{
					return policyStatement;
				}
			}
			return base.PolicyStatement;
		}

		// Token: 0x06003CDD RID: 15581 RVA: 0x000D16B0 File Offset: 0x000CF8B0
		public override CodeGroup ResolveMatchingCodeGroups(Evidence evidence)
		{
			if (evidence == null)
			{
				throw new ArgumentNullException("evidence");
			}
			if (!base.MembershipCondition.Check(evidence))
			{
				return null;
			}
			foreach (object obj in base.Children)
			{
				CodeGroup codeGroup = (CodeGroup)obj;
				if (codeGroup.Resolve(evidence) != null)
				{
					return codeGroup.Copy();
				}
			}
			return this.CopyNoChildren();
		}

		// Token: 0x06003CDE RID: 15582 RVA: 0x000D175C File Offset: 0x000CF95C
		private FirstMatchCodeGroup CopyNoChildren()
		{
			return new FirstMatchCodeGroup(base.MembershipCondition, base.PolicyStatement)
			{
				Name = base.Name,
				Description = base.Description
			};
		}
	}
}
