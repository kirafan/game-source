﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x0200063E RID: 1598
	[ComVisible(true)]
	[Serializable]
	public sealed class GacInstalled : IBuiltInEvidence, IIdentityPermissionFactory
	{
		// Token: 0x06003CE0 RID: 15584 RVA: 0x000D179C File Offset: 0x000CF99C
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return 1;
		}

		// Token: 0x06003CE1 RID: 15585 RVA: 0x000D17A0 File Offset: 0x000CF9A0
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return position;
		}

		// Token: 0x06003CE2 RID: 15586 RVA: 0x000D17A4 File Offset: 0x000CF9A4
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			buffer[position] = '\t';
			return position + 1;
		}

		// Token: 0x06003CE3 RID: 15587 RVA: 0x000D17B0 File Offset: 0x000CF9B0
		public object Copy()
		{
			return new GacInstalled();
		}

		// Token: 0x06003CE4 RID: 15588 RVA: 0x000D17B8 File Offset: 0x000CF9B8
		public IPermission CreateIdentityPermission(Evidence evidence)
		{
			return new GacIdentityPermission();
		}

		// Token: 0x06003CE5 RID: 15589 RVA: 0x000D17C0 File Offset: 0x000CF9C0
		public override bool Equals(object o)
		{
			return o != null && o is GacInstalled;
		}

		// Token: 0x06003CE6 RID: 15590 RVA: 0x000D17D4 File Offset: 0x000CF9D4
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x06003CE7 RID: 15591 RVA: 0x000D17D8 File Offset: 0x000CF9D8
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement(base.GetType().FullName);
			securityElement.AddAttribute("version", "1");
			return securityElement.ToString();
		}
	}
}
