﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x02000636 RID: 1590
	[ComVisible(true)]
	[Serializable]
	public abstract class CodeGroup
	{
		// Token: 0x06003C7F RID: 15487 RVA: 0x000CFC70 File Offset: 0x000CDE70
		protected CodeGroup(IMembershipCondition membershipCondition, PolicyStatement policy)
		{
			if (membershipCondition == null)
			{
				throw new ArgumentNullException("membershipCondition");
			}
			if (policy != null)
			{
				this.m_policy = policy.Copy();
			}
			this.m_membershipCondition = membershipCondition.Copy();
		}

		// Token: 0x06003C80 RID: 15488 RVA: 0x000CFCC0 File Offset: 0x000CDEC0
		internal CodeGroup(SecurityElement e, PolicyLevel level)
		{
			this.FromXml(e, level);
		}

		// Token: 0x06003C81 RID: 15489
		public abstract CodeGroup Copy();

		// Token: 0x17000B70 RID: 2928
		// (get) Token: 0x06003C82 RID: 15490
		public abstract string MergeLogic { get; }

		// Token: 0x06003C83 RID: 15491
		public abstract PolicyStatement Resolve(Evidence evidence);

		// Token: 0x06003C84 RID: 15492
		public abstract CodeGroup ResolveMatchingCodeGroups(Evidence evidence);

		// Token: 0x17000B71 RID: 2929
		// (get) Token: 0x06003C85 RID: 15493 RVA: 0x000CFCDC File Offset: 0x000CDEDC
		// (set) Token: 0x06003C86 RID: 15494 RVA: 0x000CFCE4 File Offset: 0x000CDEE4
		public PolicyStatement PolicyStatement
		{
			get
			{
				return this.m_policy;
			}
			set
			{
				this.m_policy = value;
			}
		}

		// Token: 0x17000B72 RID: 2930
		// (get) Token: 0x06003C87 RID: 15495 RVA: 0x000CFCF0 File Offset: 0x000CDEF0
		// (set) Token: 0x06003C88 RID: 15496 RVA: 0x000CFCF8 File Offset: 0x000CDEF8
		public string Description
		{
			get
			{
				return this.m_description;
			}
			set
			{
				this.m_description = value;
			}
		}

		// Token: 0x17000B73 RID: 2931
		// (get) Token: 0x06003C89 RID: 15497 RVA: 0x000CFD04 File Offset: 0x000CDF04
		// (set) Token: 0x06003C8A RID: 15498 RVA: 0x000CFD0C File Offset: 0x000CDF0C
		public IMembershipCondition MembershipCondition
		{
			get
			{
				return this.m_membershipCondition;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentException("value");
				}
				this.m_membershipCondition = value;
			}
		}

		// Token: 0x17000B74 RID: 2932
		// (get) Token: 0x06003C8B RID: 15499 RVA: 0x000CFD28 File Offset: 0x000CDF28
		// (set) Token: 0x06003C8C RID: 15500 RVA: 0x000CFD30 File Offset: 0x000CDF30
		public string Name
		{
			get
			{
				return this.m_name;
			}
			set
			{
				this.m_name = value;
			}
		}

		// Token: 0x17000B75 RID: 2933
		// (get) Token: 0x06003C8D RID: 15501 RVA: 0x000CFD3C File Offset: 0x000CDF3C
		// (set) Token: 0x06003C8E RID: 15502 RVA: 0x000CFD44 File Offset: 0x000CDF44
		public IList Children
		{
			get
			{
				return this.m_children;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.m_children = new ArrayList(value);
			}
		}

		// Token: 0x17000B76 RID: 2934
		// (get) Token: 0x06003C8F RID: 15503 RVA: 0x000CFD64 File Offset: 0x000CDF64
		public virtual string AttributeString
		{
			get
			{
				if (this.m_policy != null)
				{
					return this.m_policy.AttributeString;
				}
				return null;
			}
		}

		// Token: 0x17000B77 RID: 2935
		// (get) Token: 0x06003C90 RID: 15504 RVA: 0x000CFD80 File Offset: 0x000CDF80
		public virtual string PermissionSetName
		{
			get
			{
				if (this.m_policy == null)
				{
					return null;
				}
				if (this.m_policy.PermissionSet is NamedPermissionSet)
				{
					return ((NamedPermissionSet)this.m_policy.PermissionSet).Name;
				}
				return null;
			}
		}

		// Token: 0x06003C91 RID: 15505 RVA: 0x000CFDC8 File Offset: 0x000CDFC8
		public void AddChild(CodeGroup group)
		{
			if (group == null)
			{
				throw new ArgumentNullException("group");
			}
			this.m_children.Add(group.Copy());
		}

		// Token: 0x06003C92 RID: 15506 RVA: 0x000CFDF0 File Offset: 0x000CDFF0
		public override bool Equals(object o)
		{
			CodeGroup codeGroup = o as CodeGroup;
			return codeGroup != null && this.Equals(codeGroup, false);
		}

		// Token: 0x06003C93 RID: 15507 RVA: 0x000CFE14 File Offset: 0x000CE014
		public bool Equals(CodeGroup cg, bool compareChildren)
		{
			if (cg.Name != this.Name)
			{
				return false;
			}
			if (cg.Description != this.Description)
			{
				return false;
			}
			if (!cg.MembershipCondition.Equals(this.m_membershipCondition))
			{
				return false;
			}
			if (compareChildren)
			{
				int count = cg.Children.Count;
				if (this.Children.Count != count)
				{
					return false;
				}
				for (int i = 0; i < count; i++)
				{
					if (!((CodeGroup)this.Children[i]).Equals((CodeGroup)cg.Children[i], false))
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06003C94 RID: 15508 RVA: 0x000CFED0 File Offset: 0x000CE0D0
		public void RemoveChild(CodeGroup group)
		{
			if (group != null)
			{
				this.m_children.Remove(group);
			}
		}

		// Token: 0x06003C95 RID: 15509 RVA: 0x000CFEE4 File Offset: 0x000CE0E4
		public override int GetHashCode()
		{
			int num = this.m_membershipCondition.GetHashCode();
			if (this.m_policy != null)
			{
				num += this.m_policy.GetHashCode();
			}
			return num;
		}

		// Token: 0x06003C96 RID: 15510 RVA: 0x000CFF18 File Offset: 0x000CE118
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003C97 RID: 15511 RVA: 0x000CFF24 File Offset: 0x000CE124
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			if (e == null)
			{
				throw new ArgumentNullException("e");
			}
			string text = e.Attribute("PermissionSetName");
			PermissionSet permissionSet;
			if (text != null && level != null)
			{
				permissionSet = level.GetNamedPermissionSet(text);
			}
			else
			{
				SecurityElement securityElement = e.SearchForChildByTag("PermissionSet");
				if (securityElement != null)
				{
					Type type = Type.GetType(securityElement.Attribute("class"));
					permissionSet = (PermissionSet)Activator.CreateInstance(type, true);
					permissionSet.FromXml(securityElement);
				}
				else
				{
					permissionSet = new PermissionSet(new PermissionSet(PermissionState.None));
				}
			}
			this.m_policy = new PolicyStatement(permissionSet);
			this.m_children.Clear();
			if (e.Children != null && e.Children.Count > 0)
			{
				foreach (object obj in e.Children)
				{
					SecurityElement securityElement2 = (SecurityElement)obj;
					if (securityElement2.Tag == "CodeGroup")
					{
						this.AddChild(CodeGroup.CreateFromXml(securityElement2, level));
					}
				}
			}
			this.m_membershipCondition = null;
			SecurityElement securityElement3 = e.SearchForChildByTag("IMembershipCondition");
			if (securityElement3 != null)
			{
				string text2 = securityElement3.Attribute("class");
				Type type2 = Type.GetType(text2);
				if (type2 == null)
				{
					type2 = Type.GetType("System.Security.Policy." + text2);
				}
				this.m_membershipCondition = (IMembershipCondition)Activator.CreateInstance(type2, true);
				this.m_membershipCondition.FromXml(securityElement3, level);
			}
			this.m_name = e.Attribute("Name");
			this.m_description = e.Attribute("Description");
			this.ParseXml(e, level);
		}

		// Token: 0x06003C98 RID: 15512 RVA: 0x000D0104 File Offset: 0x000CE304
		protected virtual void ParseXml(SecurityElement e, PolicyLevel level)
		{
		}

		// Token: 0x06003C99 RID: 15513 RVA: 0x000D0108 File Offset: 0x000CE308
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003C9A RID: 15514 RVA: 0x000D0114 File Offset: 0x000CE314
		public SecurityElement ToXml(PolicyLevel level)
		{
			SecurityElement securityElement = new SecurityElement("CodeGroup");
			securityElement.AddAttribute("class", base.GetType().AssemblyQualifiedName);
			securityElement.AddAttribute("version", "1");
			if (this.Name != null)
			{
				securityElement.AddAttribute("Name", this.Name);
			}
			if (this.Description != null)
			{
				securityElement.AddAttribute("Description", this.Description);
			}
			if (this.MembershipCondition != null)
			{
				securityElement.AddChild(this.MembershipCondition.ToXml());
			}
			if (this.PolicyStatement != null && this.PolicyStatement.PermissionSet != null)
			{
				securityElement.AddChild(this.PolicyStatement.PermissionSet.ToXml());
			}
			foreach (object obj in this.Children)
			{
				CodeGroup codeGroup = (CodeGroup)obj;
				securityElement.AddChild(codeGroup.ToXml());
			}
			this.CreateXml(securityElement, level);
			return securityElement;
		}

		// Token: 0x06003C9B RID: 15515 RVA: 0x000D0248 File Offset: 0x000CE448
		protected virtual void CreateXml(SecurityElement element, PolicyLevel level)
		{
		}

		// Token: 0x06003C9C RID: 15516 RVA: 0x000D024C File Offset: 0x000CE44C
		internal static CodeGroup CreateFromXml(SecurityElement se, PolicyLevel level)
		{
			string text = se.Attribute("class");
			string text2 = text;
			int num = text2.IndexOf(",");
			if (num > 0)
			{
				text2 = text2.Substring(0, num);
			}
			num = text2.LastIndexOf(".");
			if (num > 0)
			{
				text2 = text2.Substring(num + 1);
			}
			string text3 = text2;
			switch (text3)
			{
			case "FileCodeGroup":
				return new FileCodeGroup(se, level);
			case "FirstMatchCodeGroup":
				return new FirstMatchCodeGroup(se, level);
			case "NetCodeGroup":
				return new NetCodeGroup(se, level);
			case "UnionCodeGroup":
				return new UnionCodeGroup(se, level);
			}
			Type type = Type.GetType(text);
			CodeGroup codeGroup = (CodeGroup)Activator.CreateInstance(type, true);
			codeGroup.FromXml(se, level);
			return codeGroup;
		}

		// Token: 0x04001A40 RID: 6720
		private PolicyStatement m_policy;

		// Token: 0x04001A41 RID: 6721
		private IMembershipCondition m_membershipCondition;

		// Token: 0x04001A42 RID: 6722
		private string m_description;

		// Token: 0x04001A43 RID: 6723
		private string m_name;

		// Token: 0x04001A44 RID: 6724
		private ArrayList m_children = new ArrayList();
	}
}
