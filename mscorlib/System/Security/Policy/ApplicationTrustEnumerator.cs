﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000633 RID: 1587
	[ComVisible(true)]
	public sealed class ApplicationTrustEnumerator : IEnumerator
	{
		// Token: 0x06003C72 RID: 15474 RVA: 0x000CFB04 File Offset: 0x000CDD04
		internal ApplicationTrustEnumerator(ApplicationTrustCollection collection)
		{
			this.e = collection.GetEnumerator();
		}

		// Token: 0x17000B6C RID: 2924
		// (get) Token: 0x06003C73 RID: 15475 RVA: 0x000CFB18 File Offset: 0x000CDD18
		object IEnumerator.Current
		{
			get
			{
				return this.e.Current;
			}
		}

		// Token: 0x17000B6D RID: 2925
		// (get) Token: 0x06003C74 RID: 15476 RVA: 0x000CFB28 File Offset: 0x000CDD28
		public ApplicationTrust Current
		{
			get
			{
				return (ApplicationTrust)this.e.Current;
			}
		}

		// Token: 0x06003C75 RID: 15477 RVA: 0x000CFB3C File Offset: 0x000CDD3C
		public bool MoveNext()
		{
			return this.e.MoveNext();
		}

		// Token: 0x06003C76 RID: 15478 RVA: 0x000CFB4C File Offset: 0x000CDD4C
		public void Reset()
		{
			this.e.Reset();
		}

		// Token: 0x04001A36 RID: 6710
		private IEnumerator e;
	}
}
