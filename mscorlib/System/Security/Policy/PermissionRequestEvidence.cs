﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x0200064A RID: 1610
	[ComVisible(true)]
	[Serializable]
	public sealed class PermissionRequestEvidence : IBuiltInEvidence
	{
		// Token: 0x06003D34 RID: 15668 RVA: 0x000D26C4 File Offset: 0x000D08C4
		public PermissionRequestEvidence(PermissionSet request, PermissionSet optional, PermissionSet denied)
		{
			if (request != null)
			{
				this.requested = new PermissionSet(request);
			}
			if (optional != null)
			{
				this.optional = new PermissionSet(optional);
			}
			if (denied != null)
			{
				this.denied = new PermissionSet(denied);
			}
		}

		// Token: 0x06003D35 RID: 15669 RVA: 0x000D2710 File Offset: 0x000D0910
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			int num = (!verbose) ? 1 : 3;
			if (this.requested != null)
			{
				int num2 = this.requested.ToXml().ToString().Length + ((!verbose) ? 0 : 5);
				num += num2;
			}
			if (this.optional != null)
			{
				int num3 = this.optional.ToXml().ToString().Length + ((!verbose) ? 0 : 5);
				num += num3;
			}
			if (this.denied != null)
			{
				int num4 = this.denied.ToXml().ToString().Length + ((!verbose) ? 0 : 5);
				num += num4;
			}
			return num;
		}

		// Token: 0x06003D36 RID: 15670 RVA: 0x000D27C8 File Offset: 0x000D09C8
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return 0;
		}

		// Token: 0x06003D37 RID: 15671 RVA: 0x000D27CC File Offset: 0x000D09CC
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			return 0;
		}

		// Token: 0x17000B92 RID: 2962
		// (get) Token: 0x06003D38 RID: 15672 RVA: 0x000D27D0 File Offset: 0x000D09D0
		public PermissionSet DeniedPermissions
		{
			get
			{
				return this.denied;
			}
		}

		// Token: 0x17000B93 RID: 2963
		// (get) Token: 0x06003D39 RID: 15673 RVA: 0x000D27D8 File Offset: 0x000D09D8
		public PermissionSet OptionalPermissions
		{
			get
			{
				return this.optional;
			}
		}

		// Token: 0x17000B94 RID: 2964
		// (get) Token: 0x06003D3A RID: 15674 RVA: 0x000D27E0 File Offset: 0x000D09E0
		public PermissionSet RequestedPermissions
		{
			get
			{
				return this.requested;
			}
		}

		// Token: 0x06003D3B RID: 15675 RVA: 0x000D27E8 File Offset: 0x000D09E8
		public PermissionRequestEvidence Copy()
		{
			return new PermissionRequestEvidence(this.requested, this.optional, this.denied);
		}

		// Token: 0x06003D3C RID: 15676 RVA: 0x000D2804 File Offset: 0x000D0A04
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement("System.Security.Policy.PermissionRequestEvidence");
			securityElement.AddAttribute("version", "1");
			if (this.requested != null)
			{
				SecurityElement securityElement2 = new SecurityElement("Request");
				securityElement2.AddChild(this.requested.ToXml());
				securityElement.AddChild(securityElement2);
			}
			if (this.optional != null)
			{
				SecurityElement securityElement3 = new SecurityElement("Optional");
				securityElement3.AddChild(this.optional.ToXml());
				securityElement.AddChild(securityElement3);
			}
			if (this.denied != null)
			{
				SecurityElement securityElement4 = new SecurityElement("Denied");
				securityElement4.AddChild(this.denied.ToXml());
				securityElement.AddChild(securityElement4);
			}
			return securityElement.ToString();
		}

		// Token: 0x04001A81 RID: 6785
		private PermissionSet requested;

		// Token: 0x04001A82 RID: 6786
		private PermissionSet optional;

		// Token: 0x04001A83 RID: 6787
		private PermissionSet denied;
	}
}
