﻿using System;
using System.Collections;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000652 RID: 1618
	[ComVisible(true)]
	[Serializable]
	public sealed class SiteMembershipCondition : ISecurityEncodable, ISecurityPolicyEncodable, IConstantMembershipCondition, IMembershipCondition
	{
		// Token: 0x06003D95 RID: 15765 RVA: 0x000D4594 File Offset: 0x000D2794
		internal SiteMembershipCondition()
		{
		}

		// Token: 0x06003D96 RID: 15766 RVA: 0x000D45A4 File Offset: 0x000D27A4
		public SiteMembershipCondition(string site)
		{
			this.Site = site;
		}

		// Token: 0x17000BA1 RID: 2977
		// (get) Token: 0x06003D97 RID: 15767 RVA: 0x000D45BC File Offset: 0x000D27BC
		// (set) Token: 0x06003D98 RID: 15768 RVA: 0x000D45C4 File Offset: 0x000D27C4
		public string Site
		{
			get
			{
				return this._site;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("site");
				}
				if (!System.Security.Policy.Site.IsValid(value))
				{
					throw new ArgumentException("invalid site");
				}
				this._site = value;
			}
		}

		// Token: 0x06003D99 RID: 15769 RVA: 0x000D4600 File Offset: 0x000D2800
		public bool Check(Evidence evidence)
		{
			if (evidence == null)
			{
				return false;
			}
			IEnumerator hostEnumerator = evidence.GetHostEnumerator();
			while (hostEnumerator.MoveNext())
			{
				if (hostEnumerator.Current is Site)
				{
					string[] array = this._site.Split(new char[]
					{
						'.'
					});
					string[] array2 = (hostEnumerator.Current as Site).origin_site.Split(new char[]
					{
						'.'
					});
					int i = array.Length - 1;
					int num = array2.Length - 1;
					while (i >= 0)
					{
						if (i == 0)
						{
							return string.Compare(array[0], "*", true, CultureInfo.InvariantCulture) == 0;
						}
						if (string.Compare(array[i], array2[num], true, CultureInfo.InvariantCulture) != 0)
						{
							return false;
						}
						i--;
						num--;
					}
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003D9A RID: 15770 RVA: 0x000D46D0 File Offset: 0x000D28D0
		public IMembershipCondition Copy()
		{
			return new SiteMembershipCondition(this._site);
		}

		// Token: 0x06003D9B RID: 15771 RVA: 0x000D46E0 File Offset: 0x000D28E0
		public override bool Equals(object o)
		{
			if (o == null)
			{
				return false;
			}
			if (o is SiteMembershipCondition)
			{
				Site site = new Site((o as SiteMembershipCondition)._site);
				return site.Equals(new Site(this._site));
			}
			return false;
		}

		// Token: 0x06003D9C RID: 15772 RVA: 0x000D4724 File Offset: 0x000D2924
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003D9D RID: 15773 RVA: 0x000D4730 File Offset: 0x000D2930
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			MembershipConditionHelper.CheckSecurityElement(e, "e", this.version, this.version);
			this._site = e.Attribute("Site");
		}

		// Token: 0x06003D9E RID: 15774 RVA: 0x000D475C File Offset: 0x000D295C
		public override int GetHashCode()
		{
			return this._site.GetHashCode();
		}

		// Token: 0x06003D9F RID: 15775 RVA: 0x000D476C File Offset: 0x000D296C
		public override string ToString()
		{
			return "Site - " + this._site;
		}

		// Token: 0x06003DA0 RID: 15776 RVA: 0x000D4780 File Offset: 0x000D2980
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003DA1 RID: 15777 RVA: 0x000D478C File Offset: 0x000D298C
		public SecurityElement ToXml(PolicyLevel level)
		{
			SecurityElement securityElement = MembershipConditionHelper.Element(typeof(SiteMembershipCondition), this.version);
			securityElement.AddAttribute("Site", this._site);
			return securityElement;
		}

		// Token: 0x04001A97 RID: 6807
		private readonly int version = 1;

		// Token: 0x04001A98 RID: 6808
		private string _site;
	}
}
