﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Permissions;
using Mono.Security.Cryptography;

namespace System.Security.Policy
{
	// Token: 0x02000631 RID: 1585
	[ComVisible(true)]
	[Serializable]
	public sealed class ApplicationTrust : ISecurityEncodable
	{
		// Token: 0x06003C4E RID: 15438 RVA: 0x000CF26C File Offset: 0x000CD46C
		public ApplicationTrust()
		{
			this.fullTrustAssemblies = new List<StrongName>(0);
		}

		// Token: 0x06003C4F RID: 15439 RVA: 0x000CF280 File Offset: 0x000CD480
		public ApplicationTrust(ApplicationIdentity applicationIdentity) : this()
		{
			if (applicationIdentity == null)
			{
				throw new ArgumentNullException("applicationIdentity");
			}
			this._appid = applicationIdentity;
		}

		// Token: 0x06003C50 RID: 15440 RVA: 0x000CF2A0 File Offset: 0x000CD4A0
		internal ApplicationTrust(PermissionSet defaultGrantSet, IEnumerable<StrongName> fullTrustAssemblies)
		{
			if (defaultGrantSet == null)
			{
				throw new ArgumentNullException("defaultGrantSet");
			}
			this._defaultPolicy = new PolicyStatement(defaultGrantSet);
			if (fullTrustAssemblies == null)
			{
				throw new ArgumentNullException("fullTrustAssemblies");
			}
			this.fullTrustAssemblies = new List<StrongName>();
			foreach (StrongName strongName in fullTrustAssemblies)
			{
				if (strongName == null)
				{
					throw new ArgumentException("fullTrustAssemblies contains an assembly that does not have a StrongName");
				}
				this.fullTrustAssemblies.Add((StrongName)strongName.Copy());
			}
		}

		// Token: 0x17000B62 RID: 2914
		// (get) Token: 0x06003C51 RID: 15441 RVA: 0x000CF360 File Offset: 0x000CD560
		// (set) Token: 0x06003C52 RID: 15442 RVA: 0x000CF368 File Offset: 0x000CD568
		public ApplicationIdentity ApplicationIdentity
		{
			get
			{
				return this._appid;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("ApplicationIdentity");
				}
				this._appid = value;
			}
		}

		// Token: 0x17000B63 RID: 2915
		// (get) Token: 0x06003C53 RID: 15443 RVA: 0x000CF384 File Offset: 0x000CD584
		// (set) Token: 0x06003C54 RID: 15444 RVA: 0x000CF3A4 File Offset: 0x000CD5A4
		public PolicyStatement DefaultGrantSet
		{
			get
			{
				if (this._defaultPolicy == null)
				{
					this._defaultPolicy = this.GetDefaultGrantSet();
				}
				return this._defaultPolicy;
			}
			set
			{
				this._defaultPolicy = value;
			}
		}

		// Token: 0x17000B64 RID: 2916
		// (get) Token: 0x06003C55 RID: 15445 RVA: 0x000CF3B0 File Offset: 0x000CD5B0
		// (set) Token: 0x06003C56 RID: 15446 RVA: 0x000CF3B8 File Offset: 0x000CD5B8
		public object ExtraInfo
		{
			get
			{
				return this._xtranfo;
			}
			set
			{
				this._xtranfo = value;
			}
		}

		// Token: 0x17000B65 RID: 2917
		// (get) Token: 0x06003C57 RID: 15447 RVA: 0x000CF3C4 File Offset: 0x000CD5C4
		// (set) Token: 0x06003C58 RID: 15448 RVA: 0x000CF3CC File Offset: 0x000CD5CC
		public bool IsApplicationTrustedToRun
		{
			get
			{
				return this._trustrun;
			}
			set
			{
				this._trustrun = value;
			}
		}

		// Token: 0x17000B66 RID: 2918
		// (get) Token: 0x06003C59 RID: 15449 RVA: 0x000CF3D8 File Offset: 0x000CD5D8
		// (set) Token: 0x06003C5A RID: 15450 RVA: 0x000CF3E0 File Offset: 0x000CD5E0
		public bool Persist
		{
			get
			{
				return this._persist;
			}
			set
			{
				this._persist = value;
			}
		}

		// Token: 0x06003C5B RID: 15451 RVA: 0x000CF3EC File Offset: 0x000CD5EC
		public void FromXml(SecurityElement element)
		{
			if (element == null)
			{
				throw new ArgumentNullException("element");
			}
			if (element.Tag != "ApplicationTrust")
			{
				throw new ArgumentException("element");
			}
			string text = element.Attribute("FullName");
			if (text != null)
			{
				this._appid = new ApplicationIdentity(text);
			}
			else
			{
				this._appid = null;
			}
			this._defaultPolicy = null;
			SecurityElement securityElement = element.SearchForChildByTag("DefaultGrant");
			if (securityElement != null)
			{
				for (int i = 0; i < securityElement.Children.Count; i++)
				{
					SecurityElement securityElement2 = securityElement.Children[i] as SecurityElement;
					if (securityElement2.Tag == "PolicyStatement")
					{
						this.DefaultGrantSet.FromXml(securityElement2, null);
						break;
					}
				}
			}
			if (!bool.TryParse(element.Attribute("TrustedToRun"), out this._trustrun))
			{
				this._trustrun = false;
			}
			if (!bool.TryParse(element.Attribute("Persist"), out this._persist))
			{
				this._persist = false;
			}
			this._xtranfo = null;
			SecurityElement securityElement3 = element.SearchForChildByTag("ExtraInfo");
			if (securityElement3 != null)
			{
				text = securityElement3.Attribute("Data");
				if (text != null)
				{
					byte[] buffer = CryptoConvert.FromHex(text);
					using (MemoryStream memoryStream = new MemoryStream(buffer))
					{
						BinaryFormatter binaryFormatter = new BinaryFormatter();
						this._xtranfo = binaryFormatter.Deserialize(memoryStream);
					}
				}
			}
		}

		// Token: 0x06003C5C RID: 15452 RVA: 0x000CF58C File Offset: 0x000CD78C
		public SecurityElement ToXml()
		{
			SecurityElement securityElement = new SecurityElement("ApplicationTrust");
			securityElement.AddAttribute("version", "1");
			if (this._appid != null)
			{
				securityElement.AddAttribute("FullName", this._appid.FullName);
			}
			if (this._trustrun)
			{
				securityElement.AddAttribute("TrustedToRun", "true");
			}
			if (this._persist)
			{
				securityElement.AddAttribute("Persist", "true");
			}
			SecurityElement securityElement2 = new SecurityElement("DefaultGrant");
			securityElement2.AddChild(this.DefaultGrantSet.ToXml());
			securityElement.AddChild(securityElement2);
			if (this._xtranfo != null)
			{
				byte[] input = null;
				using (MemoryStream memoryStream = new MemoryStream())
				{
					BinaryFormatter binaryFormatter = new BinaryFormatter();
					binaryFormatter.Serialize(memoryStream, this._xtranfo);
					input = memoryStream.ToArray();
				}
				SecurityElement securityElement3 = new SecurityElement("ExtraInfo");
				securityElement3.AddAttribute("Data", CryptoConvert.ToHex(input));
				securityElement.AddChild(securityElement3);
			}
			return securityElement;
		}

		// Token: 0x06003C5D RID: 15453 RVA: 0x000CF6B4 File Offset: 0x000CD8B4
		private PolicyStatement GetDefaultGrantSet()
		{
			PermissionSet permSet = new PermissionSet(PermissionState.None);
			return new PolicyStatement(permSet);
		}

		// Token: 0x04001A2F RID: 6703
		private ApplicationIdentity _appid;

		// Token: 0x04001A30 RID: 6704
		private PolicyStatement _defaultPolicy;

		// Token: 0x04001A31 RID: 6705
		private object _xtranfo;

		// Token: 0x04001A32 RID: 6706
		private bool _trustrun;

		// Token: 0x04001A33 RID: 6707
		private bool _persist;

		// Token: 0x04001A34 RID: 6708
		private IList<StrongName> fullTrustAssemblies;
	}
}
