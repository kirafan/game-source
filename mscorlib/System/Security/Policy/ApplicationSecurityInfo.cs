﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x0200062F RID: 1583
	[ComVisible(true)]
	public sealed class ApplicationSecurityInfo
	{
		// Token: 0x06003C42 RID: 15426 RVA: 0x000CF13C File Offset: 0x000CD33C
		public ApplicationSecurityInfo(ActivationContext activationContext)
		{
			if (activationContext == null)
			{
				throw new ArgumentNullException("activationContext");
			}
			this._context = activationContext;
		}

		// Token: 0x17000B5C RID: 2908
		// (get) Token: 0x06003C43 RID: 15427 RVA: 0x000CF15C File Offset: 0x000CD35C
		// (set) Token: 0x06003C44 RID: 15428 RVA: 0x000CF164 File Offset: 0x000CD364
		public Evidence ApplicationEvidence
		{
			get
			{
				return this._evidence;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("ApplicationEvidence");
				}
				this._evidence = value;
			}
		}

		// Token: 0x17000B5D RID: 2909
		// (get) Token: 0x06003C45 RID: 15429 RVA: 0x000CF180 File Offset: 0x000CD380
		// (set) Token: 0x06003C46 RID: 15430 RVA: 0x000CF188 File Offset: 0x000CD388
		public ApplicationId ApplicationId
		{
			get
			{
				return this._appid;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("ApplicationId");
				}
				this._appid = value;
			}
		}

		// Token: 0x17000B5E RID: 2910
		// (get) Token: 0x06003C47 RID: 15431 RVA: 0x000CF1A4 File Offset: 0x000CD3A4
		// (set) Token: 0x06003C48 RID: 15432 RVA: 0x000CF1C0 File Offset: 0x000CD3C0
		public PermissionSet DefaultRequestSet
		{
			get
			{
				if (this._defaultSet == null)
				{
					return new PermissionSet(PermissionState.None);
				}
				return this._defaultSet;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("DefaultRequestSet");
				}
				this._defaultSet = value;
			}
		}

		// Token: 0x17000B5F RID: 2911
		// (get) Token: 0x06003C49 RID: 15433 RVA: 0x000CF1DC File Offset: 0x000CD3DC
		// (set) Token: 0x06003C4A RID: 15434 RVA: 0x000CF1E4 File Offset: 0x000CD3E4
		public ApplicationId DeploymentId
		{
			get
			{
				return this._deployid;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("DeploymentId");
				}
				this._deployid = value;
			}
		}

		// Token: 0x04001A27 RID: 6695
		private ActivationContext _context;

		// Token: 0x04001A28 RID: 6696
		private Evidence _evidence;

		// Token: 0x04001A29 RID: 6697
		private ApplicationId _appid;

		// Token: 0x04001A2A RID: 6698
		private PermissionSet _defaultSet;

		// Token: 0x04001A2B RID: 6699
		private ApplicationId _deployid;
	}
}
