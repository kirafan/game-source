﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000635 RID: 1589
	[ComVisible(true)]
	[Serializable]
	public class CodeConnectAccess
	{
		// Token: 0x06003C77 RID: 15479 RVA: 0x000CFB5C File Offset: 0x000CDD5C
		[MonoTODO("(2.0) validations incomplete")]
		public CodeConnectAccess(string allowScheme, int allowPort)
		{
			if (allowScheme == null || allowScheme.Length == 0)
			{
				throw new ArgumentOutOfRangeException("allowScheme");
			}
			if (allowPort < 0 || allowPort > 65535)
			{
				throw new ArgumentOutOfRangeException("allowPort");
			}
			this._scheme = allowScheme;
			this._port = allowPort;
		}

		// Token: 0x17000B6E RID: 2926
		// (get) Token: 0x06003C79 RID: 15481 RVA: 0x000CFBE8 File Offset: 0x000CDDE8
		public int Port
		{
			get
			{
				return this._port;
			}
		}

		// Token: 0x17000B6F RID: 2927
		// (get) Token: 0x06003C7A RID: 15482 RVA: 0x000CFBF0 File Offset: 0x000CDDF0
		public string Scheme
		{
			get
			{
				return this._scheme;
			}
		}

		// Token: 0x06003C7B RID: 15483 RVA: 0x000CFBF8 File Offset: 0x000CDDF8
		public override bool Equals(object o)
		{
			CodeConnectAccess codeConnectAccess = o as CodeConnectAccess;
			return codeConnectAccess != null && this._scheme == codeConnectAccess._scheme && this._port == codeConnectAccess._port;
		}

		// Token: 0x06003C7C RID: 15484 RVA: 0x000CFC3C File Offset: 0x000CDE3C
		public override int GetHashCode()
		{
			return this._scheme.GetHashCode() ^ this._port;
		}

		// Token: 0x06003C7D RID: 15485 RVA: 0x000CFC50 File Offset: 0x000CDE50
		public static CodeConnectAccess CreateAnySchemeAccess(int allowPort)
		{
			return new CodeConnectAccess(CodeConnectAccess.AnyScheme, allowPort);
		}

		// Token: 0x06003C7E RID: 15486 RVA: 0x000CFC60 File Offset: 0x000CDE60
		public static CodeConnectAccess CreateOriginSchemeAccess(int allowPort)
		{
			return new CodeConnectAccess(CodeConnectAccess.OriginScheme, allowPort);
		}

		// Token: 0x04001A3A RID: 6714
		public static readonly string AnyScheme = "*";

		// Token: 0x04001A3B RID: 6715
		public static readonly int DefaultPort = -3;

		// Token: 0x04001A3C RID: 6716
		public static readonly int OriginPort = -4;

		// Token: 0x04001A3D RID: 6717
		public static readonly string OriginScheme = "$origin";

		// Token: 0x04001A3E RID: 6718
		private string _scheme;

		// Token: 0x04001A3F RID: 6719
		private int _port;
	}
}
