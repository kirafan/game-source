﻿using System;
using System.Collections;
using System.Globalization;
using System.Runtime.InteropServices;
using Mono.Security;

namespace System.Security.Policy
{
	// Token: 0x02000659 RID: 1625
	[ComVisible(true)]
	[Serializable]
	public sealed class UrlMembershipCondition : ISecurityEncodable, ISecurityPolicyEncodable, IConstantMembershipCondition, IMembershipCondition
	{
		// Token: 0x06003DE1 RID: 15841 RVA: 0x000D5294 File Offset: 0x000D3494
		public UrlMembershipCondition(string url)
		{
			if (url == null)
			{
				throw new ArgumentNullException("url");
			}
			this.CheckUrl(url);
			this.userUrl = url;
			this.url = new Url(url);
		}

		// Token: 0x06003DE2 RID: 15842 RVA: 0x000D52DC File Offset: 0x000D34DC
		internal UrlMembershipCondition(Url url, string userUrl)
		{
			this.url = (Url)url.Copy();
			this.userUrl = userUrl;
		}

		// Token: 0x17000BB0 RID: 2992
		// (get) Token: 0x06003DE3 RID: 15843 RVA: 0x000D5304 File Offset: 0x000D3504
		// (set) Token: 0x06003DE4 RID: 15844 RVA: 0x000D5334 File Offset: 0x000D3534
		public string Url
		{
			get
			{
				if (this.userUrl == null)
				{
					this.userUrl = this.url.Value;
				}
				return this.userUrl;
			}
			set
			{
				this.url = new Url(value);
			}
		}

		// Token: 0x06003DE5 RID: 15845 RVA: 0x000D5344 File Offset: 0x000D3544
		public bool Check(Evidence evidence)
		{
			if (evidence == null)
			{
				return false;
			}
			string value = this.url.Value;
			int num = value.LastIndexOf("*");
			if (num == -1)
			{
				num = value.Length;
			}
			IEnumerator hostEnumerator = evidence.GetHostEnumerator();
			while (hostEnumerator.MoveNext())
			{
				if (hostEnumerator.Current is Url && string.Compare(value, 0, (hostEnumerator.Current as Url).Value, 0, num, true, CultureInfo.InvariantCulture) == 0)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003DE6 RID: 15846 RVA: 0x000D53D0 File Offset: 0x000D35D0
		public IMembershipCondition Copy()
		{
			return new UrlMembershipCondition(this.url, this.userUrl);
		}

		// Token: 0x06003DE7 RID: 15847 RVA: 0x000D53E4 File Offset: 0x000D35E4
		public override bool Equals(object o)
		{
			UrlMembershipCondition urlMembershipCondition = o as UrlMembershipCondition;
			if (o == null)
			{
				return false;
			}
			string value = this.url.Value;
			int num = value.Length;
			if (value[num - 1] == '*')
			{
				num--;
				if (value[num - 1] == '/')
				{
					num--;
				}
			}
			return string.Compare(value, 0, urlMembershipCondition.Url, 0, num, true, CultureInfo.InvariantCulture) == 0;
		}

		// Token: 0x06003DE8 RID: 15848 RVA: 0x000D5454 File Offset: 0x000D3654
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003DE9 RID: 15849 RVA: 0x000D5460 File Offset: 0x000D3660
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			MembershipConditionHelper.CheckSecurityElement(e, "e", this.version, this.version);
			string text = e.Attribute("Url");
			if (text != null)
			{
				this.CheckUrl(text);
				this.url = new Url(text);
			}
			else
			{
				this.url = null;
			}
			this.userUrl = text;
		}

		// Token: 0x06003DEA RID: 15850 RVA: 0x000D54C0 File Offset: 0x000D36C0
		public override int GetHashCode()
		{
			return this.url.GetHashCode();
		}

		// Token: 0x06003DEB RID: 15851 RVA: 0x000D54D0 File Offset: 0x000D36D0
		public override string ToString()
		{
			return "Url - " + this.Url;
		}

		// Token: 0x06003DEC RID: 15852 RVA: 0x000D54E4 File Offset: 0x000D36E4
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003DED RID: 15853 RVA: 0x000D54F0 File Offset: 0x000D36F0
		public SecurityElement ToXml(PolicyLevel level)
		{
			SecurityElement securityElement = MembershipConditionHelper.Element(typeof(UrlMembershipCondition), this.version);
			securityElement.AddAttribute("Url", this.userUrl);
			return securityElement;
		}

		// Token: 0x06003DEE RID: 15854 RVA: 0x000D5528 File Offset: 0x000D3728
		internal void CheckUrl(string url)
		{
			int num = url.IndexOf(Uri.SchemeDelimiter);
			string uriString = (num >= 0) ? url : ("file://" + url);
			Uri uri = new Uri(uriString, false, false);
			if (uri.Host.IndexOf('*') >= 1)
			{
				string text = Locale.GetText("Invalid * character in url");
				throw new ArgumentException(text, "name");
			}
		}

		// Token: 0x04001AAB RID: 6827
		private readonly int version = 1;

		// Token: 0x04001AAC RID: 6828
		private Url url;

		// Token: 0x04001AAD RID: 6829
		private string userUrl;
	}
}
