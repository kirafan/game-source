﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x02000653 RID: 1619
	[ComVisible(true)]
	[Serializable]
	public sealed class StrongName : IBuiltInEvidence, IIdentityPermissionFactory
	{
		// Token: 0x06003DA2 RID: 15778 RVA: 0x000D47C4 File Offset: 0x000D29C4
		public StrongName(StrongNamePublicKeyBlob blob, string name, Version version)
		{
			if (blob == null)
			{
				throw new ArgumentNullException("blob");
			}
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (version == null)
			{
				throw new ArgumentNullException("version");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException(Locale.GetText("Empty"), "name");
			}
			this.publickey = blob;
			this.name = name;
			this.version = version;
		}

		// Token: 0x06003DA3 RID: 15779 RVA: 0x000D4848 File Offset: 0x000D2A48
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return ((!verbose) ? 1 : 5) + this.name.Length;
		}

		// Token: 0x06003DA4 RID: 15780 RVA: 0x000D4864 File Offset: 0x000D2A64
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return 0;
		}

		// Token: 0x06003DA5 RID: 15781 RVA: 0x000D4868 File Offset: 0x000D2A68
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			return 0;
		}

		// Token: 0x17000BA2 RID: 2978
		// (get) Token: 0x06003DA6 RID: 15782 RVA: 0x000D486C File Offset: 0x000D2A6C
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000BA3 RID: 2979
		// (get) Token: 0x06003DA7 RID: 15783 RVA: 0x000D4874 File Offset: 0x000D2A74
		public StrongNamePublicKeyBlob PublicKey
		{
			get
			{
				return this.publickey;
			}
		}

		// Token: 0x17000BA4 RID: 2980
		// (get) Token: 0x06003DA8 RID: 15784 RVA: 0x000D487C File Offset: 0x000D2A7C
		public Version Version
		{
			get
			{
				return this.version;
			}
		}

		// Token: 0x06003DA9 RID: 15785 RVA: 0x000D4884 File Offset: 0x000D2A84
		public object Copy()
		{
			return new StrongName(this.publickey, this.name, this.version);
		}

		// Token: 0x06003DAA RID: 15786 RVA: 0x000D48A0 File Offset: 0x000D2AA0
		public IPermission CreateIdentityPermission(Evidence evidence)
		{
			return new StrongNameIdentityPermission(this.publickey, this.name, this.version);
		}

		// Token: 0x06003DAB RID: 15787 RVA: 0x000D48BC File Offset: 0x000D2ABC
		public override bool Equals(object o)
		{
			StrongName strongName = o as StrongName;
			return strongName != null && !(this.name != strongName.Name) && this.Version.Equals(strongName.Version) && this.PublicKey.Equals(strongName.PublicKey);
		}

		// Token: 0x06003DAC RID: 15788 RVA: 0x000D491C File Offset: 0x000D2B1C
		public override int GetHashCode()
		{
			return this.publickey.GetHashCode();
		}

		// Token: 0x06003DAD RID: 15789 RVA: 0x000D492C File Offset: 0x000D2B2C
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement(typeof(StrongName).Name);
			securityElement.AddAttribute("version", "1");
			securityElement.AddAttribute("Key", this.publickey.ToString());
			securityElement.AddAttribute("Name", this.name);
			securityElement.AddAttribute("Version", this.version.ToString());
			return securityElement.ToString();
		}

		// Token: 0x04001A99 RID: 6809
		private StrongNamePublicKeyBlob publickey;

		// Token: 0x04001A9A RID: 6810
		private string name;

		// Token: 0x04001A9B RID: 6811
		private Version version;
	}
}
