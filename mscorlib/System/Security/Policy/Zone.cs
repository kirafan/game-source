﻿using System;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Mono.Security;

namespace System.Security.Policy
{
	// Token: 0x0200065A RID: 1626
	[ComVisible(true)]
	[Serializable]
	public sealed class Zone : IBuiltInEvidence, IIdentityPermissionFactory
	{
		// Token: 0x06003DEF RID: 15855 RVA: 0x000D5590 File Offset: 0x000D3790
		public Zone(SecurityZone zone)
		{
			if (!Enum.IsDefined(typeof(SecurityZone), zone))
			{
				string message = string.Format(Locale.GetText("Invalid zone {0}."), zone);
				throw new ArgumentException(message, "zone");
			}
			this.zone = zone;
		}

		// Token: 0x06003DF0 RID: 15856 RVA: 0x000D55E8 File Offset: 0x000D37E8
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return 3;
		}

		// Token: 0x06003DF1 RID: 15857 RVA: 0x000D55EC File Offset: 0x000D37EC
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			int num = (int)buffer[position++];
			num += (int)buffer[position++];
			return position;
		}

		// Token: 0x06003DF2 RID: 15858 RVA: 0x000D5610 File Offset: 0x000D3810
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			buffer[position++] = '\u0003';
			buffer[position++] = (char)(this.zone >> 16);
			buffer[position++] = (char)(this.zone & (SecurityZone)65535);
			return position;
		}

		// Token: 0x17000BB1 RID: 2993
		// (get) Token: 0x06003DF3 RID: 15859 RVA: 0x000D5644 File Offset: 0x000D3844
		public SecurityZone SecurityZone
		{
			get
			{
				return this.zone;
			}
		}

		// Token: 0x06003DF4 RID: 15860 RVA: 0x000D564C File Offset: 0x000D384C
		public object Copy()
		{
			return new Zone(this.zone);
		}

		// Token: 0x06003DF5 RID: 15861 RVA: 0x000D565C File Offset: 0x000D385C
		public IPermission CreateIdentityPermission(Evidence evidence)
		{
			return new ZoneIdentityPermission(this.zone);
		}

		// Token: 0x06003DF6 RID: 15862 RVA: 0x000D566C File Offset: 0x000D386C
		[MonoTODO("Not user configurable yet")]
		public static Zone CreateFromUrl(string url)
		{
			if (url == null)
			{
				throw new ArgumentNullException("url");
			}
			SecurityZone securityZone = SecurityZone.NoZone;
			if (url.Length == 0)
			{
				return new Zone(securityZone);
			}
			Uri uri = new Uri(url);
			if (securityZone == SecurityZone.NoZone)
			{
				if (uri.IsFile)
				{
					if (File.Exists(uri.LocalPath))
					{
						securityZone = SecurityZone.MyComputer;
					}
					else if (string.Compare("FILE://", 0, url, 0, 7, true, CultureInfo.InvariantCulture) == 0)
					{
						securityZone = SecurityZone.Intranet;
					}
					else
					{
						securityZone = SecurityZone.Internet;
					}
				}
				else if (uri.IsLoopback)
				{
					securityZone = SecurityZone.Intranet;
				}
				else
				{
					securityZone = SecurityZone.Internet;
				}
			}
			return new Zone(securityZone);
		}

		// Token: 0x06003DF7 RID: 15863 RVA: 0x000D5710 File Offset: 0x000D3910
		public override bool Equals(object o)
		{
			Zone zone = o as Zone;
			return zone != null && zone.zone == this.zone;
		}

		// Token: 0x06003DF8 RID: 15864 RVA: 0x000D573C File Offset: 0x000D393C
		public override int GetHashCode()
		{
			return (int)this.zone;
		}

		// Token: 0x06003DF9 RID: 15865 RVA: 0x000D5744 File Offset: 0x000D3944
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement("System.Security.Policy.Zone");
			securityElement.AddAttribute("version", "1");
			securityElement.AddChild(new SecurityElement("Zone", this.zone.ToString()));
			return securityElement.ToString();
		}

		// Token: 0x04001AAE RID: 6830
		private SecurityZone zone;
	}
}
