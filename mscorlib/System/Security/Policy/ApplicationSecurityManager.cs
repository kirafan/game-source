﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x02000630 RID: 1584
	[ComVisible(true)]
	public static class ApplicationSecurityManager
	{
		// Token: 0x17000B60 RID: 2912
		// (get) Token: 0x06003C4B RID: 15435 RVA: 0x000CF200 File Offset: 0x000CD400
		public static IApplicationTrustManager ApplicationTrustManager
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				if (ApplicationSecurityManager._appTrustManager == null)
				{
					ApplicationSecurityManager._appTrustManager = new MonoTrustManager();
				}
				return ApplicationSecurityManager._appTrustManager;
			}
		}

		// Token: 0x17000B61 RID: 2913
		// (get) Token: 0x06003C4C RID: 15436 RVA: 0x000CF21C File Offset: 0x000CD41C
		public static ApplicationTrustCollection UserApplicationTrusts
		{
			get
			{
				if (ApplicationSecurityManager._userAppTrusts == null)
				{
					ApplicationSecurityManager._userAppTrusts = new ApplicationTrustCollection();
				}
				return ApplicationSecurityManager._userAppTrusts;
			}
		}

		// Token: 0x06003C4D RID: 15437 RVA: 0x000CF238 File Offset: 0x000CD438
		[MonoTODO("Missing application manifest support")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
		public static bool DetermineApplicationTrust(ActivationContext activationContext, TrustManagerContext context)
		{
			if (activationContext == null)
			{
				throw new NullReferenceException("activationContext");
			}
			ApplicationTrust applicationTrust = ApplicationSecurityManager.ApplicationTrustManager.DetermineApplicationTrust(activationContext, context);
			return applicationTrust.IsApplicationTrustedToRun;
		}

		// Token: 0x04001A2C RID: 6700
		private const string config = "ApplicationTrust.config";

		// Token: 0x04001A2D RID: 6701
		private static IApplicationTrustManager _appTrustManager;

		// Token: 0x04001A2E RID: 6702
		private static ApplicationTrustCollection _userAppTrusts;
	}
}
