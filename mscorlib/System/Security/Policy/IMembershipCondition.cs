﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000646 RID: 1606
	[ComVisible(true)]
	public interface IMembershipCondition : ISecurityEncodable, ISecurityPolicyEncodable
	{
		// Token: 0x06003D17 RID: 15639
		bool Check(Evidence evidence);

		// Token: 0x06003D18 RID: 15640
		IMembershipCondition Copy();

		// Token: 0x06003D19 RID: 15641
		bool Equals(object obj);

		// Token: 0x06003D1A RID: 15642
		string ToString();
	}
}
