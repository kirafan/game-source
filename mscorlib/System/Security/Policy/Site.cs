﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Mono.Security;

namespace System.Security.Policy
{
	// Token: 0x02000651 RID: 1617
	[ComVisible(true)]
	[Serializable]
	public sealed class Site : IBuiltInEvidence, IIdentityPermissionFactory
	{
		// Token: 0x06003D88 RID: 15752 RVA: 0x000D4284 File Offset: 0x000D2484
		public Site(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("url");
			}
			if (!Site.IsValid(name))
			{
				throw new ArgumentException(Locale.GetText("name is not valid"));
			}
			this.origin_site = name;
		}

		// Token: 0x06003D89 RID: 15753 RVA: 0x000D42C0 File Offset: 0x000D24C0
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return ((!verbose) ? 1 : 3) + this.origin_site.Length;
		}

		// Token: 0x06003D8A RID: 15754 RVA: 0x000D42DC File Offset: 0x000D24DC
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return 0;
		}

		// Token: 0x06003D8B RID: 15755 RVA: 0x000D42E0 File Offset: 0x000D24E0
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			return 0;
		}

		// Token: 0x06003D8C RID: 15756 RVA: 0x000D42E4 File Offset: 0x000D24E4
		public static Site CreateFromUrl(string url)
		{
			if (url == null)
			{
				throw new ArgumentNullException("url");
			}
			if (url.Length == 0)
			{
				throw new FormatException(Locale.GetText("Empty URL."));
			}
			string text = Site.UrlToSite(url);
			if (text == null)
			{
				string message = string.Format(Locale.GetText("Invalid URL '{0}'."), url);
				throw new ArgumentException(message, "url");
			}
			return new Site(text);
		}

		// Token: 0x06003D8D RID: 15757 RVA: 0x000D4350 File Offset: 0x000D2550
		public object Copy()
		{
			return new Site(this.origin_site);
		}

		// Token: 0x06003D8E RID: 15758 RVA: 0x000D4360 File Offset: 0x000D2560
		public IPermission CreateIdentityPermission(Evidence evidence)
		{
			return new SiteIdentityPermission(this.origin_site);
		}

		// Token: 0x06003D8F RID: 15759 RVA: 0x000D4370 File Offset: 0x000D2570
		public override bool Equals(object o)
		{
			Site site = o as Site;
			return site != null && string.Compare(site.Name, this.origin_site, true, CultureInfo.InvariantCulture) == 0;
		}

		// Token: 0x06003D90 RID: 15760 RVA: 0x000D43A8 File Offset: 0x000D25A8
		public override int GetHashCode()
		{
			return this.origin_site.GetHashCode();
		}

		// Token: 0x06003D91 RID: 15761 RVA: 0x000D43B8 File Offset: 0x000D25B8
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement("System.Security.Policy.Site");
			securityElement.AddAttribute("version", "1");
			securityElement.AddChild(new SecurityElement("Name", this.origin_site));
			return securityElement.ToString();
		}

		// Token: 0x17000BA0 RID: 2976
		// (get) Token: 0x06003D92 RID: 15762 RVA: 0x000D43FC File Offset: 0x000D25FC
		public string Name
		{
			get
			{
				return this.origin_site;
			}
		}

		// Token: 0x06003D93 RID: 15763 RVA: 0x000D4404 File Offset: 0x000D2604
		internal static bool IsValid(string name)
		{
			if (name == string.Empty)
			{
				return false;
			}
			if (name.Length == 1 && name == ".")
			{
				return false;
			}
			string[] array = name.Split(new char[]
			{
				'.'
			});
			for (int i = 0; i < array.Length; i++)
			{
				string text = array[i];
				if (i != 0 || !(text == "*"))
				{
					foreach (char value in text)
					{
						int num = Convert.ToInt32(value);
						if (num != 33 && num != 45 && (num < 35 || num > 41) && (num < 48 || num > 57) && (num < 64 || num > 90) && (num < 94 || num > 95) && (num < 97 || num > 123) && (num < 125 || num > 126))
						{
							return false;
						}
					}
				}
			}
			return true;
		}

		// Token: 0x06003D94 RID: 15764 RVA: 0x000D4548 File Offset: 0x000D2748
		internal static string UrlToSite(string url)
		{
			if (url == null)
			{
				return null;
			}
			Uri uri = new Uri(url);
			if (uri.Scheme == Uri.UriSchemeFile)
			{
				return null;
			}
			string host = uri.Host;
			return (!Site.IsValid(host)) ? null : host;
		}

		// Token: 0x04001A96 RID: 6806
		internal string origin_site;
	}
}
