﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using Mono.Security.Cryptography;

namespace System.Security.Policy
{
	// Token: 0x02000650 RID: 1616
	[ComVisible(true)]
	[Serializable]
	public sealed class PublisherMembershipCondition : ISecurityEncodable, ISecurityPolicyEncodable, IConstantMembershipCondition, IMembershipCondition
	{
		// Token: 0x06003D7B RID: 15739 RVA: 0x000D40A8 File Offset: 0x000D22A8
		internal PublisherMembershipCondition()
		{
		}

		// Token: 0x06003D7C RID: 15740 RVA: 0x000D40B8 File Offset: 0x000D22B8
		public PublisherMembershipCondition(X509Certificate certificate)
		{
			if (certificate == null)
			{
				throw new ArgumentNullException("certificate");
			}
			if (certificate.GetHashCode() == 0)
			{
				throw new ArgumentException("certificate");
			}
			this.x509 = certificate;
		}

		// Token: 0x17000B9F RID: 2975
		// (get) Token: 0x06003D7D RID: 15741 RVA: 0x000D40F8 File Offset: 0x000D22F8
		// (set) Token: 0x06003D7E RID: 15742 RVA: 0x000D4100 File Offset: 0x000D2300
		public X509Certificate Certificate
		{
			get
			{
				return this.x509;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.x509 = value;
			}
		}

		// Token: 0x06003D7F RID: 15743 RVA: 0x000D411C File Offset: 0x000D231C
		public bool Check(Evidence evidence)
		{
			if (evidence == null)
			{
				return false;
			}
			IEnumerator hostEnumerator = evidence.GetHostEnumerator();
			while (hostEnumerator.MoveNext())
			{
				if (hostEnumerator.Current is Publisher && this.x509.Equals((hostEnumerator.Current as Publisher).Certificate))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003D80 RID: 15744 RVA: 0x000D417C File Offset: 0x000D237C
		public IMembershipCondition Copy()
		{
			return new PublisherMembershipCondition(this.x509);
		}

		// Token: 0x06003D81 RID: 15745 RVA: 0x000D418C File Offset: 0x000D238C
		public override bool Equals(object o)
		{
			PublisherMembershipCondition publisherMembershipCondition = o as PublisherMembershipCondition;
			return publisherMembershipCondition != null && this.x509.Equals(publisherMembershipCondition.Certificate);
		}

		// Token: 0x06003D82 RID: 15746 RVA: 0x000D41BC File Offset: 0x000D23BC
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003D83 RID: 15747 RVA: 0x000D41C8 File Offset: 0x000D23C8
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			MembershipConditionHelper.CheckSecurityElement(e, "e", this.version, this.version);
			string text = e.Attribute("X509Certificate");
			if (text != null)
			{
				byte[] data = CryptoConvert.FromHex(text);
				this.x509 = new X509Certificate(data);
			}
		}

		// Token: 0x06003D84 RID: 15748 RVA: 0x000D4214 File Offset: 0x000D2414
		public override int GetHashCode()
		{
			return this.x509.GetHashCode();
		}

		// Token: 0x06003D85 RID: 15749 RVA: 0x000D4224 File Offset: 0x000D2424
		public override string ToString()
		{
			return "Publisher - " + this.x509.GetPublicKeyString();
		}

		// Token: 0x06003D86 RID: 15750 RVA: 0x000D423C File Offset: 0x000D243C
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003D87 RID: 15751 RVA: 0x000D4248 File Offset: 0x000D2448
		public SecurityElement ToXml(PolicyLevel level)
		{
			SecurityElement securityElement = MembershipConditionHelper.Element(typeof(PublisherMembershipCondition), this.version);
			securityElement.AddAttribute("X509Certificate", this.x509.GetRawCertDataString());
			return securityElement;
		}

		// Token: 0x04001A94 RID: 6804
		private readonly int version = 1;

		// Token: 0x04001A95 RID: 6805
		private X509Certificate x509;
	}
}
