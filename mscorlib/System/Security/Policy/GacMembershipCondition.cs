﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x0200063F RID: 1599
	[ComVisible(true)]
	[Serializable]
	public sealed class GacMembershipCondition : ISecurityEncodable, ISecurityPolicyEncodable, IConstantMembershipCondition, IMembershipCondition
	{
		// Token: 0x06003CE9 RID: 15593 RVA: 0x000D181C File Offset: 0x000CFA1C
		public bool Check(Evidence evidence)
		{
			if (evidence == null)
			{
				return false;
			}
			IEnumerator hostEnumerator = evidence.GetHostEnumerator();
			while (hostEnumerator.MoveNext())
			{
				if (hostEnumerator.Current is GacInstalled)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003CEA RID: 15594 RVA: 0x000D185C File Offset: 0x000CFA5C
		public IMembershipCondition Copy()
		{
			return new GacMembershipCondition();
		}

		// Token: 0x06003CEB RID: 15595 RVA: 0x000D1864 File Offset: 0x000CFA64
		public override bool Equals(object o)
		{
			return o != null && o is GacMembershipCondition;
		}

		// Token: 0x06003CEC RID: 15596 RVA: 0x000D1878 File Offset: 0x000CFA78
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003CED RID: 15597 RVA: 0x000D1884 File Offset: 0x000CFA84
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			MembershipConditionHelper.CheckSecurityElement(e, "e", this.version, this.version);
		}

		// Token: 0x06003CEE RID: 15598 RVA: 0x000D18A0 File Offset: 0x000CFAA0
		public override int GetHashCode()
		{
			return 0;
		}

		// Token: 0x06003CEF RID: 15599 RVA: 0x000D18A4 File Offset: 0x000CFAA4
		public override string ToString()
		{
			return "GAC";
		}

		// Token: 0x06003CF0 RID: 15600 RVA: 0x000D18AC File Offset: 0x000CFAAC
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003CF1 RID: 15601 RVA: 0x000D18B8 File Offset: 0x000CFAB8
		public SecurityElement ToXml(PolicyLevel level)
		{
			return MembershipConditionHelper.Element(typeof(GacMembershipCondition), this.version);
		}

		// Token: 0x04001A73 RID: 6771
		private readonly int version = 1;
	}
}
