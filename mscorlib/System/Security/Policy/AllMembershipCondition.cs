﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x0200062C RID: 1580
	[ComVisible(true)]
	[Serializable]
	public sealed class AllMembershipCondition : ISecurityEncodable, ISecurityPolicyEncodable, IConstantMembershipCondition, IMembershipCondition
	{
		// Token: 0x06003C25 RID: 15397 RVA: 0x000CEDF0 File Offset: 0x000CCFF0
		public bool Check(Evidence evidence)
		{
			return true;
		}

		// Token: 0x06003C26 RID: 15398 RVA: 0x000CEDF4 File Offset: 0x000CCFF4
		public IMembershipCondition Copy()
		{
			return new AllMembershipCondition();
		}

		// Token: 0x06003C27 RID: 15399 RVA: 0x000CEDFC File Offset: 0x000CCFFC
		public override bool Equals(object o)
		{
			return o is AllMembershipCondition;
		}

		// Token: 0x06003C28 RID: 15400 RVA: 0x000CEE08 File Offset: 0x000CD008
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003C29 RID: 15401 RVA: 0x000CEE14 File Offset: 0x000CD014
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			MembershipConditionHelper.CheckSecurityElement(e, "e", this.version, this.version);
		}

		// Token: 0x06003C2A RID: 15402 RVA: 0x000CEE30 File Offset: 0x000CD030
		public override int GetHashCode()
		{
			return typeof(AllMembershipCondition).GetHashCode();
		}

		// Token: 0x06003C2B RID: 15403 RVA: 0x000CEE44 File Offset: 0x000CD044
		public override string ToString()
		{
			return "All code";
		}

		// Token: 0x06003C2C RID: 15404 RVA: 0x000CEE4C File Offset: 0x000CD04C
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003C2D RID: 15405 RVA: 0x000CEE58 File Offset: 0x000CD058
		public SecurityElement ToXml(PolicyLevel level)
		{
			return MembershipConditionHelper.Element(typeof(AllMembershipCondition), this.version);
		}

		// Token: 0x04001A24 RID: 6692
		private readonly int version = 1;
	}
}
