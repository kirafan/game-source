﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;

namespace System.Security.Policy
{
	// Token: 0x02000654 RID: 1620
	[ComVisible(true)]
	[Serializable]
	public sealed class StrongNameMembershipCondition : ISecurityEncodable, ISecurityPolicyEncodable, IConstantMembershipCondition, IMembershipCondition
	{
		// Token: 0x06003DAE RID: 15790 RVA: 0x000D49A4 File Offset: 0x000D2BA4
		public StrongNameMembershipCondition(StrongNamePublicKeyBlob blob, string name, Version version)
		{
			if (blob == null)
			{
				throw new ArgumentNullException("blob");
			}
			this.blob = blob;
			this.name = name;
			if (version != null)
			{
				this.assemblyVersion = (Version)version.Clone();
			}
		}

		// Token: 0x06003DAF RID: 15791 RVA: 0x000D49FC File Offset: 0x000D2BFC
		internal StrongNameMembershipCondition(SecurityElement e)
		{
			this.FromXml(e);
		}

		// Token: 0x06003DB0 RID: 15792 RVA: 0x000D4A14 File Offset: 0x000D2C14
		internal StrongNameMembershipCondition()
		{
		}

		// Token: 0x17000BA5 RID: 2981
		// (get) Token: 0x06003DB1 RID: 15793 RVA: 0x000D4A24 File Offset: 0x000D2C24
		// (set) Token: 0x06003DB2 RID: 15794 RVA: 0x000D4A2C File Offset: 0x000D2C2C
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000BA6 RID: 2982
		// (get) Token: 0x06003DB3 RID: 15795 RVA: 0x000D4A38 File Offset: 0x000D2C38
		// (set) Token: 0x06003DB4 RID: 15796 RVA: 0x000D4A40 File Offset: 0x000D2C40
		public Version Version
		{
			get
			{
				return this.assemblyVersion;
			}
			set
			{
				this.assemblyVersion = value;
			}
		}

		// Token: 0x17000BA7 RID: 2983
		// (get) Token: 0x06003DB5 RID: 15797 RVA: 0x000D4A4C File Offset: 0x000D2C4C
		// (set) Token: 0x06003DB6 RID: 15798 RVA: 0x000D4A54 File Offset: 0x000D2C54
		public StrongNamePublicKeyBlob PublicKey
		{
			get
			{
				return this.blob;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("PublicKey");
				}
				this.blob = value;
			}
		}

		// Token: 0x06003DB7 RID: 15799 RVA: 0x000D4A70 File Offset: 0x000D2C70
		public bool Check(Evidence evidence)
		{
			if (evidence == null)
			{
				return false;
			}
			IEnumerator hostEnumerator = evidence.GetHostEnumerator();
			while (hostEnumerator.MoveNext())
			{
				object obj = hostEnumerator.Current;
				StrongName strongName = obj as StrongName;
				if (strongName != null)
				{
					return strongName.PublicKey.Equals(this.blob) && (this.name == null || !(this.name != strongName.Name)) && (!(this.assemblyVersion != null) || this.assemblyVersion.Equals(strongName.Version));
				}
			}
			return false;
		}

		// Token: 0x06003DB8 RID: 15800 RVA: 0x000D4B18 File Offset: 0x000D2D18
		public IMembershipCondition Copy()
		{
			return new StrongNameMembershipCondition(this.blob, this.name, this.assemblyVersion);
		}

		// Token: 0x06003DB9 RID: 15801 RVA: 0x000D4B34 File Offset: 0x000D2D34
		public override bool Equals(object o)
		{
			StrongNameMembershipCondition strongNameMembershipCondition = o as StrongNameMembershipCondition;
			if (strongNameMembershipCondition == null)
			{
				return false;
			}
			if (!strongNameMembershipCondition.PublicKey.Equals(this.PublicKey))
			{
				return false;
			}
			if (this.name != strongNameMembershipCondition.Name)
			{
				return false;
			}
			if (this.assemblyVersion != null)
			{
				return this.assemblyVersion.Equals(strongNameMembershipCondition.Version);
			}
			return strongNameMembershipCondition.Version == null;
		}

		// Token: 0x06003DBA RID: 15802 RVA: 0x000D4BB0 File Offset: 0x000D2DB0
		public override int GetHashCode()
		{
			return this.blob.GetHashCode();
		}

		// Token: 0x06003DBB RID: 15803 RVA: 0x000D4BC0 File Offset: 0x000D2DC0
		public void FromXml(SecurityElement e)
		{
			this.FromXml(e, null);
		}

		// Token: 0x06003DBC RID: 15804 RVA: 0x000D4BCC File Offset: 0x000D2DCC
		public void FromXml(SecurityElement e, PolicyLevel level)
		{
			MembershipConditionHelper.CheckSecurityElement(e, "e", this.version, this.version);
			this.blob = StrongNamePublicKeyBlob.FromString(e.Attribute("PublicKeyBlob"));
			this.name = e.Attribute("Name");
			string text = e.Attribute("AssemblyVersion");
			if (text == null)
			{
				this.assemblyVersion = null;
			}
			else
			{
				this.assemblyVersion = new Version(text);
			}
		}

		// Token: 0x06003DBD RID: 15805 RVA: 0x000D4C44 File Offset: 0x000D2E44
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder("StrongName - ");
			stringBuilder.Append(this.blob);
			if (this.name != null)
			{
				stringBuilder.AppendFormat(" name = {0}", this.name);
			}
			if (this.assemblyVersion != null)
			{
				stringBuilder.AppendFormat(" version = {0}", this.assemblyVersion);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06003DBE RID: 15806 RVA: 0x000D4CB0 File Offset: 0x000D2EB0
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003DBF RID: 15807 RVA: 0x000D4CBC File Offset: 0x000D2EBC
		public SecurityElement ToXml(PolicyLevel level)
		{
			SecurityElement securityElement = MembershipConditionHelper.Element(typeof(StrongNameMembershipCondition), this.version);
			if (this.blob != null)
			{
				securityElement.AddAttribute("PublicKeyBlob", this.blob.ToString());
			}
			if (this.name != null)
			{
				securityElement.AddAttribute("Name", this.name);
			}
			if (this.assemblyVersion != null)
			{
				string text = this.assemblyVersion.ToString();
				if (text != "0.0")
				{
					securityElement.AddAttribute("AssemblyVersion", text);
				}
			}
			return securityElement;
		}

		// Token: 0x04001A9C RID: 6812
		private readonly int version = 1;

		// Token: 0x04001A9D RID: 6813
		private StrongNamePublicKeyBlob blob;

		// Token: 0x04001A9E RID: 6814
		private string name;

		// Token: 0x04001A9F RID: 6815
		private Version assemblyVersion;
	}
}
