﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Mono.Security;

namespace System.Security.Policy
{
	// Token: 0x02000658 RID: 1624
	[ComVisible(true)]
	[Serializable]
	public sealed class Url : IBuiltInEvidence, IIdentityPermissionFactory
	{
		// Token: 0x06003DD5 RID: 15829 RVA: 0x000D5080 File Offset: 0x000D3280
		public Url(string name) : this(name, false)
		{
		}

		// Token: 0x06003DD6 RID: 15830 RVA: 0x000D508C File Offset: 0x000D328C
		internal Url(string name, bool validated)
		{
			this.origin_url = ((!validated) ? this.Prepare(name) : name);
		}

		// Token: 0x06003DD7 RID: 15831 RVA: 0x000D50B0 File Offset: 0x000D32B0
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return ((!verbose) ? 1 : 3) + this.origin_url.Length;
		}

		// Token: 0x06003DD8 RID: 15832 RVA: 0x000D50CC File Offset: 0x000D32CC
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return 0;
		}

		// Token: 0x06003DD9 RID: 15833 RVA: 0x000D50D0 File Offset: 0x000D32D0
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			return 0;
		}

		// Token: 0x06003DDA RID: 15834 RVA: 0x000D50D4 File Offset: 0x000D32D4
		public object Copy()
		{
			return new Url(this.origin_url, true);
		}

		// Token: 0x06003DDB RID: 15835 RVA: 0x000D50E4 File Offset: 0x000D32E4
		public IPermission CreateIdentityPermission(Evidence evidence)
		{
			return new UrlIdentityPermission(this.origin_url);
		}

		// Token: 0x06003DDC RID: 15836 RVA: 0x000D50F4 File Offset: 0x000D32F4
		public override bool Equals(object o)
		{
			Url url = o as Url;
			if (url == null)
			{
				return false;
			}
			string text = url.Value;
			string text2 = this.origin_url;
			if (text.IndexOf(Uri.SchemeDelimiter) < 0)
			{
				text = "file://" + text;
			}
			if (text2.IndexOf(Uri.SchemeDelimiter) < 0)
			{
				text2 = "file://" + text2;
			}
			return string.Compare(text, text2, true, CultureInfo.InvariantCulture) == 0;
		}

		// Token: 0x06003DDD RID: 15837 RVA: 0x000D5168 File Offset: 0x000D3368
		public override int GetHashCode()
		{
			string text = this.origin_url;
			if (text.IndexOf(Uri.SchemeDelimiter) < 0)
			{
				text = "file://" + text;
			}
			return text.GetHashCode();
		}

		// Token: 0x06003DDE RID: 15838 RVA: 0x000D51A0 File Offset: 0x000D33A0
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement("System.Security.Policy.Url");
			securityElement.AddAttribute("version", "1");
			securityElement.AddChild(new SecurityElement("Url", this.origin_url));
			return securityElement.ToString();
		}

		// Token: 0x17000BAF RID: 2991
		// (get) Token: 0x06003DDF RID: 15839 RVA: 0x000D51E4 File Offset: 0x000D33E4
		public string Value
		{
			get
			{
				return this.origin_url;
			}
		}

		// Token: 0x06003DE0 RID: 15840 RVA: 0x000D51EC File Offset: 0x000D33EC
		private string Prepare(string url)
		{
			if (url == null)
			{
				throw new ArgumentNullException("Url");
			}
			if (url == string.Empty)
			{
				throw new FormatException(Locale.GetText("Invalid (empty) Url"));
			}
			int num = url.IndexOf(Uri.SchemeDelimiter);
			if (num > 0)
			{
				if (url.StartsWith("file://"))
				{
					url = "file://" + url.Substring(7);
				}
				Uri uri = new Uri(url, false, false);
				url = uri.ToString();
			}
			int num2 = url.Length - 1;
			if (url[num2] == '/')
			{
				url = url.Substring(0, num2);
			}
			return url;
		}

		// Token: 0x04001AAA RID: 6826
		private string origin_url;
	}
}
