﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000655 RID: 1621
	[ComVisible(true)]
	public class TrustManagerContext
	{
		// Token: 0x06003DC0 RID: 15808 RVA: 0x000D4D58 File Offset: 0x000D2F58
		public TrustManagerContext() : this(TrustManagerUIContext.Run)
		{
		}

		// Token: 0x06003DC1 RID: 15809 RVA: 0x000D4D64 File Offset: 0x000D2F64
		public TrustManagerContext(TrustManagerUIContext uiContext)
		{
			this._ignorePersistedDecision = false;
			this._noPrompt = false;
			this._keepAlive = false;
			this._persist = false;
			this._ui = uiContext;
		}

		// Token: 0x17000BA8 RID: 2984
		// (get) Token: 0x06003DC2 RID: 15810 RVA: 0x000D4D90 File Offset: 0x000D2F90
		// (set) Token: 0x06003DC3 RID: 15811 RVA: 0x000D4D98 File Offset: 0x000D2F98
		public virtual bool IgnorePersistedDecision
		{
			get
			{
				return this._ignorePersistedDecision;
			}
			set
			{
				this._ignorePersistedDecision = value;
			}
		}

		// Token: 0x17000BA9 RID: 2985
		// (get) Token: 0x06003DC4 RID: 15812 RVA: 0x000D4DA4 File Offset: 0x000D2FA4
		// (set) Token: 0x06003DC5 RID: 15813 RVA: 0x000D4DAC File Offset: 0x000D2FAC
		public virtual bool KeepAlive
		{
			get
			{
				return this._keepAlive;
			}
			set
			{
				this._keepAlive = value;
			}
		}

		// Token: 0x17000BAA RID: 2986
		// (get) Token: 0x06003DC6 RID: 15814 RVA: 0x000D4DB8 File Offset: 0x000D2FB8
		// (set) Token: 0x06003DC7 RID: 15815 RVA: 0x000D4DC0 File Offset: 0x000D2FC0
		public virtual bool NoPrompt
		{
			get
			{
				return this._noPrompt;
			}
			set
			{
				this._noPrompt = value;
			}
		}

		// Token: 0x17000BAB RID: 2987
		// (get) Token: 0x06003DC8 RID: 15816 RVA: 0x000D4DCC File Offset: 0x000D2FCC
		// (set) Token: 0x06003DC9 RID: 15817 RVA: 0x000D4DD4 File Offset: 0x000D2FD4
		public virtual bool Persist
		{
			get
			{
				return this._persist;
			}
			set
			{
				this._persist = value;
			}
		}

		// Token: 0x17000BAC RID: 2988
		// (get) Token: 0x06003DCA RID: 15818 RVA: 0x000D4DE0 File Offset: 0x000D2FE0
		// (set) Token: 0x06003DCB RID: 15819 RVA: 0x000D4DE8 File Offset: 0x000D2FE8
		public virtual ApplicationIdentity PreviousApplicationIdentity
		{
			get
			{
				return this._previousId;
			}
			set
			{
				this._previousId = value;
			}
		}

		// Token: 0x17000BAD RID: 2989
		// (get) Token: 0x06003DCC RID: 15820 RVA: 0x000D4DF4 File Offset: 0x000D2FF4
		// (set) Token: 0x06003DCD RID: 15821 RVA: 0x000D4DFC File Offset: 0x000D2FFC
		public virtual TrustManagerUIContext UIContext
		{
			get
			{
				return this._ui;
			}
			set
			{
				this._ui = value;
			}
		}

		// Token: 0x04001AA0 RID: 6816
		private bool _ignorePersistedDecision;

		// Token: 0x04001AA1 RID: 6817
		private bool _noPrompt;

		// Token: 0x04001AA2 RID: 6818
		private bool _keepAlive;

		// Token: 0x04001AA3 RID: 6819
		private bool _persist;

		// Token: 0x04001AA4 RID: 6820
		private ApplicationIdentity _previousId;

		// Token: 0x04001AA5 RID: 6821
		private TrustManagerUIContext _ui;
	}
}
