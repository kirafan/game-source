﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000642 RID: 1602
	[ComVisible(true)]
	public interface IApplicationTrustManager : ISecurityEncodable
	{
		// Token: 0x06003D12 RID: 15634
		ApplicationTrust DetermineApplicationTrust(ActivationContext activationContext, TrustManagerContext context);
	}
}
