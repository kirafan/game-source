﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Policy
{
	// Token: 0x02000656 RID: 1622
	[ComVisible(true)]
	public enum TrustManagerUIContext
	{
		// Token: 0x04001AA7 RID: 6823
		Install,
		// Token: 0x04001AA8 RID: 6824
		Upgrade,
		// Token: 0x04001AA9 RID: 6825
		Run
	}
}
