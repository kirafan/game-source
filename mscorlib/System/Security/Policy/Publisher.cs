﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x0200064F RID: 1615
	[ComVisible(true)]
	[Serializable]
	public sealed class Publisher : IBuiltInEvidence, IIdentityPermissionFactory
	{
		// Token: 0x06003D71 RID: 15729 RVA: 0x000D3F4C File Offset: 0x000D214C
		public Publisher(X509Certificate cert)
		{
			if (cert == null)
			{
				throw new ArgumentNullException("cert");
			}
			if (cert.GetHashCode() == 0)
			{
				throw new ArgumentException("cert");
			}
			this.m_cert = cert;
		}

		// Token: 0x06003D72 RID: 15730 RVA: 0x000D3F90 File Offset: 0x000D2190
		int IBuiltInEvidence.GetRequiredSize(bool verbose)
		{
			return ((!verbose) ? 1 : 3) + this.m_cert.GetRawCertData().Length;
		}

		// Token: 0x06003D73 RID: 15731 RVA: 0x000D3FB0 File Offset: 0x000D21B0
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.InitFromBuffer(char[] buffer, int position)
		{
			return 0;
		}

		// Token: 0x06003D74 RID: 15732 RVA: 0x000D3FB4 File Offset: 0x000D21B4
		[MonoTODO("IBuiltInEvidence")]
		int IBuiltInEvidence.OutputToBuffer(char[] buffer, int position, bool verbose)
		{
			return 0;
		}

		// Token: 0x17000B9E RID: 2974
		// (get) Token: 0x06003D75 RID: 15733 RVA: 0x000D3FB8 File Offset: 0x000D21B8
		public X509Certificate Certificate
		{
			get
			{
				if (this.m_cert.GetHashCode() == 0)
				{
					throw new ArgumentException("m_cert");
				}
				return this.m_cert;
			}
		}

		// Token: 0x06003D76 RID: 15734 RVA: 0x000D3FDC File Offset: 0x000D21DC
		public object Copy()
		{
			return new Publisher(this.m_cert);
		}

		// Token: 0x06003D77 RID: 15735 RVA: 0x000D3FEC File Offset: 0x000D21EC
		public IPermission CreateIdentityPermission(Evidence evidence)
		{
			return new PublisherIdentityPermission(this.m_cert);
		}

		// Token: 0x06003D78 RID: 15736 RVA: 0x000D3FFC File Offset: 0x000D21FC
		public override bool Equals(object o)
		{
			Publisher publisher = o as Publisher;
			if (publisher == null)
			{
				throw new ArgumentException("o", Locale.GetText("not a Publisher instance."));
			}
			return this.m_cert.Equals(publisher.Certificate);
		}

		// Token: 0x06003D79 RID: 15737 RVA: 0x000D403C File Offset: 0x000D223C
		public override int GetHashCode()
		{
			return this.m_cert.GetHashCode();
		}

		// Token: 0x06003D7A RID: 15738 RVA: 0x000D404C File Offset: 0x000D224C
		public override string ToString()
		{
			SecurityElement securityElement = new SecurityElement("System.Security.Policy.Publisher");
			securityElement.AddAttribute("version", "1");
			SecurityElement securityElement2 = new SecurityElement("X509v3Certificate");
			string rawCertDataString = this.m_cert.GetRawCertDataString();
			if (rawCertDataString != null)
			{
				securityElement2.Text = rawCertDataString;
			}
			securityElement.AddChild(securityElement2);
			return securityElement.ToString();
		}

		// Token: 0x04001A93 RID: 6803
		private X509Certificate m_cert;
	}
}
