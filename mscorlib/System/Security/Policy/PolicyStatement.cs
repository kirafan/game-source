﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Security.Policy
{
	// Token: 0x0200064D RID: 1613
	[ComVisible(true)]
	[Serializable]
	public sealed class PolicyStatement : ISecurityEncodable, ISecurityPolicyEncodable
	{
		// Token: 0x06003D62 RID: 15714 RVA: 0x000D3CB0 File Offset: 0x000D1EB0
		public PolicyStatement(PermissionSet permSet) : this(permSet, PolicyStatementAttribute.Nothing)
		{
		}

		// Token: 0x06003D63 RID: 15715 RVA: 0x000D3CBC File Offset: 0x000D1EBC
		public PolicyStatement(PermissionSet permSet, PolicyStatementAttribute attributes)
		{
			if (permSet != null)
			{
				this.perms = permSet.Copy();
				this.perms.SetReadOnly(true);
			}
			this.attrs = attributes;
		}

		// Token: 0x17000B9B RID: 2971
		// (get) Token: 0x06003D64 RID: 15716 RVA: 0x000D3CF4 File Offset: 0x000D1EF4
		// (set) Token: 0x06003D65 RID: 15717 RVA: 0x000D3D20 File Offset: 0x000D1F20
		public PermissionSet PermissionSet
		{
			get
			{
				if (this.perms == null)
				{
					this.perms = new PermissionSet(PermissionState.None);
					this.perms.SetReadOnly(true);
				}
				return this.perms;
			}
			set
			{
				this.perms = value;
			}
		}

		// Token: 0x17000B9C RID: 2972
		// (get) Token: 0x06003D66 RID: 15718 RVA: 0x000D3D2C File Offset: 0x000D1F2C
		// (set) Token: 0x06003D67 RID: 15719 RVA: 0x000D3D34 File Offset: 0x000D1F34
		public PolicyStatementAttribute Attributes
		{
			get
			{
				return this.attrs;
			}
			set
			{
				switch (value)
				{
				case PolicyStatementAttribute.Nothing:
				case PolicyStatementAttribute.Exclusive:
				case PolicyStatementAttribute.LevelFinal:
				case PolicyStatementAttribute.All:
					this.attrs = value;
					return;
				default:
				{
					string text = Locale.GetText("Invalid value for {0}.");
					throw new ArgumentException(string.Format(text, "PolicyStatementAttribute"));
				}
				}
			}
		}

		// Token: 0x17000B9D RID: 2973
		// (get) Token: 0x06003D68 RID: 15720 RVA: 0x000D3D88 File Offset: 0x000D1F88
		public string AttributeString
		{
			get
			{
				switch (this.attrs)
				{
				case PolicyStatementAttribute.Exclusive:
					return "Exclusive";
				case PolicyStatementAttribute.LevelFinal:
					return "LevelFinal";
				case PolicyStatementAttribute.All:
					return "Exclusive LevelFinal";
				default:
					return string.Empty;
				}
			}
		}

		// Token: 0x06003D69 RID: 15721 RVA: 0x000D3DCC File Offset: 0x000D1FCC
		public PolicyStatement Copy()
		{
			return new PolicyStatement(this.perms, this.attrs);
		}

		// Token: 0x06003D6A RID: 15722 RVA: 0x000D3DE0 File Offset: 0x000D1FE0
		public void FromXml(SecurityElement et)
		{
			this.FromXml(et, null);
		}

		// Token: 0x06003D6B RID: 15723 RVA: 0x000D3DEC File Offset: 0x000D1FEC
		public void FromXml(SecurityElement et, PolicyLevel level)
		{
			if (et == null)
			{
				throw new ArgumentNullException("et");
			}
			if (et.Tag != "PolicyStatement")
			{
				throw new ArgumentException(Locale.GetText("Invalid tag."));
			}
			string text = et.Attribute("Attributes");
			if (text != null)
			{
				this.attrs = (PolicyStatementAttribute)((int)Enum.Parse(typeof(PolicyStatementAttribute), text));
			}
			SecurityElement et2 = et.SearchForChildByTag("PermissionSet");
			this.PermissionSet.FromXml(et2);
		}

		// Token: 0x06003D6C RID: 15724 RVA: 0x000D3E74 File Offset: 0x000D2074
		public SecurityElement ToXml()
		{
			return this.ToXml(null);
		}

		// Token: 0x06003D6D RID: 15725 RVA: 0x000D3E80 File Offset: 0x000D2080
		public SecurityElement ToXml(PolicyLevel level)
		{
			SecurityElement securityElement = new SecurityElement("PolicyStatement");
			securityElement.AddAttribute("version", "1");
			if (this.attrs != PolicyStatementAttribute.Nothing)
			{
				securityElement.AddAttribute("Attributes", this.attrs.ToString());
			}
			securityElement.AddChild(this.PermissionSet.ToXml());
			return securityElement;
		}

		// Token: 0x06003D6E RID: 15726 RVA: 0x000D3EE0 File Offset: 0x000D20E0
		[ComVisible(false)]
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			PolicyStatement policyStatement = obj as PolicyStatement;
			return policyStatement != null && this.PermissionSet.Equals(obj) && this.attrs == policyStatement.attrs;
		}

		// Token: 0x06003D6F RID: 15727 RVA: 0x000D3F28 File Offset: 0x000D2128
		[ComVisible(false)]
		public override int GetHashCode()
		{
			return this.PermissionSet.GetHashCode() ^ (int)this.attrs;
		}

		// Token: 0x06003D70 RID: 15728 RVA: 0x000D3F3C File Offset: 0x000D213C
		internal static PolicyStatement Empty()
		{
			return new PolicyStatement(new PermissionSet(PermissionState.None));
		}

		// Token: 0x04001A8C RID: 6796
		private PermissionSet perms;

		// Token: 0x04001A8D RID: 6797
		private PolicyStatementAttribute attrs;
	}
}
