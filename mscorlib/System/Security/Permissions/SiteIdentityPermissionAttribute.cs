﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200061F RID: 1567
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class SiteIdentityPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003BBA RID: 15290 RVA: 0x000CD2DC File Offset: 0x000CB4DC
		public SiteIdentityPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000B4C RID: 2892
		// (get) Token: 0x06003BBB RID: 15291 RVA: 0x000CD2E8 File Offset: 0x000CB4E8
		// (set) Token: 0x06003BBC RID: 15292 RVA: 0x000CD2F0 File Offset: 0x000CB4F0
		public string Site
		{
			get
			{
				return this.site;
			}
			set
			{
				this.site = value;
			}
		}

		// Token: 0x06003BBD RID: 15293 RVA: 0x000CD2FC File Offset: 0x000CB4FC
		public override IPermission CreatePermission()
		{
			SiteIdentityPermission result;
			if (base.Unrestricted)
			{
				result = new SiteIdentityPermission(PermissionState.Unrestricted);
			}
			else if (this.site == null)
			{
				result = new SiteIdentityPermission(PermissionState.None);
			}
			else
			{
				result = new SiteIdentityPermission(this.site);
			}
			return result;
		}

		// Token: 0x04001A04 RID: 6660
		private string site;
	}
}
