﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005F8 RID: 1528
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum FileIOPermissionAccess
	{
		// Token: 0x0400194E RID: 6478
		NoAccess = 0,
		// Token: 0x0400194F RID: 6479
		Read = 1,
		// Token: 0x04001950 RID: 6480
		Write = 2,
		// Token: 0x04001951 RID: 6481
		Append = 4,
		// Token: 0x04001952 RID: 6482
		PathDiscovery = 8,
		// Token: 0x04001953 RID: 6483
		AllAccess = 15
	}
}
