﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200060A RID: 1546
	[ComVisible(true)]
	[Serializable]
	public sealed class KeyContainerPermissionAccessEntryEnumerator : IEnumerator
	{
		// Token: 0x06003AE8 RID: 15080 RVA: 0x000CA16C File Offset: 0x000C836C
		internal KeyContainerPermissionAccessEntryEnumerator(ArrayList list)
		{
			this.e = list.GetEnumerator();
		}

		// Token: 0x17000B17 RID: 2839
		// (get) Token: 0x06003AE9 RID: 15081 RVA: 0x000CA180 File Offset: 0x000C8380
		object IEnumerator.Current
		{
			get
			{
				return this.e.Current;
			}
		}

		// Token: 0x17000B18 RID: 2840
		// (get) Token: 0x06003AEA RID: 15082 RVA: 0x000CA190 File Offset: 0x000C8390
		public KeyContainerPermissionAccessEntry Current
		{
			get
			{
				return (KeyContainerPermissionAccessEntry)this.e.Current;
			}
		}

		// Token: 0x06003AEB RID: 15083 RVA: 0x000CA1A4 File Offset: 0x000C83A4
		public bool MoveNext()
		{
			return this.e.MoveNext();
		}

		// Token: 0x06003AEC RID: 15084 RVA: 0x000CA1B4 File Offset: 0x000C83B4
		public void Reset()
		{
			this.e.Reset();
		}

		// Token: 0x0400199E RID: 6558
		private IEnumerator e;
	}
}
