﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000619 RID: 1561
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class RegistryPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003B6C RID: 15212 RVA: 0x000CC638 File Offset: 0x000CA838
		public RegistryPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000B34 RID: 2868
		// (get) Token: 0x06003B6D RID: 15213 RVA: 0x000CC644 File Offset: 0x000CA844
		// (set) Token: 0x06003B6E RID: 15214 RVA: 0x000CC650 File Offset: 0x000CA850
		[Obsolete("use newer properties")]
		public string All
		{
			get
			{
				throw new NotSupportedException("All");
			}
			set
			{
				this.create = value;
				this.read = value;
				this.write = value;
			}
		}

		// Token: 0x17000B35 RID: 2869
		// (get) Token: 0x06003B6F RID: 15215 RVA: 0x000CC668 File Offset: 0x000CA868
		// (set) Token: 0x06003B70 RID: 15216 RVA: 0x000CC670 File Offset: 0x000CA870
		public string Create
		{
			get
			{
				return this.create;
			}
			set
			{
				this.create = value;
			}
		}

		// Token: 0x17000B36 RID: 2870
		// (get) Token: 0x06003B71 RID: 15217 RVA: 0x000CC67C File Offset: 0x000CA87C
		// (set) Token: 0x06003B72 RID: 15218 RVA: 0x000CC684 File Offset: 0x000CA884
		public string Read
		{
			get
			{
				return this.read;
			}
			set
			{
				this.read = value;
			}
		}

		// Token: 0x17000B37 RID: 2871
		// (get) Token: 0x06003B73 RID: 15219 RVA: 0x000CC690 File Offset: 0x000CA890
		// (set) Token: 0x06003B74 RID: 15220 RVA: 0x000CC698 File Offset: 0x000CA898
		public string Write
		{
			get
			{
				return this.write;
			}
			set
			{
				this.write = value;
			}
		}

		// Token: 0x17000B38 RID: 2872
		// (get) Token: 0x06003B75 RID: 15221 RVA: 0x000CC6A4 File Offset: 0x000CA8A4
		// (set) Token: 0x06003B76 RID: 15222 RVA: 0x000CC6AC File Offset: 0x000CA8AC
		public string ChangeAccessControl
		{
			get
			{
				return this.changeAccessControl;
			}
			set
			{
				this.changeAccessControl = value;
			}
		}

		// Token: 0x17000B39 RID: 2873
		// (get) Token: 0x06003B77 RID: 15223 RVA: 0x000CC6B8 File Offset: 0x000CA8B8
		// (set) Token: 0x06003B78 RID: 15224 RVA: 0x000CC6C0 File Offset: 0x000CA8C0
		public string ViewAccessControl
		{
			get
			{
				return this.viewAccessControl;
			}
			set
			{
				this.viewAccessControl = value;
			}
		}

		// Token: 0x17000B3A RID: 2874
		// (get) Token: 0x06003B79 RID: 15225 RVA: 0x000CC6CC File Offset: 0x000CA8CC
		// (set) Token: 0x06003B7A RID: 15226 RVA: 0x000CC6D4 File Offset: 0x000CA8D4
		public string ViewAndModify
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				this.create = value;
				this.read = value;
				this.write = value;
			}
		}

		// Token: 0x06003B7B RID: 15227 RVA: 0x000CC6EC File Offset: 0x000CA8EC
		public override IPermission CreatePermission()
		{
			RegistryPermission registryPermission;
			if (base.Unrestricted)
			{
				registryPermission = new RegistryPermission(PermissionState.Unrestricted);
			}
			else
			{
				registryPermission = new RegistryPermission(PermissionState.None);
				if (this.create != null)
				{
					registryPermission.AddPathList(RegistryPermissionAccess.Create, this.create);
				}
				if (this.read != null)
				{
					registryPermission.AddPathList(RegistryPermissionAccess.Read, this.read);
				}
				if (this.write != null)
				{
					registryPermission.AddPathList(RegistryPermissionAccess.Write, this.write);
				}
			}
			return registryPermission;
		}

		// Token: 0x040019DE RID: 6622
		private string create;

		// Token: 0x040019DF RID: 6623
		private string read;

		// Token: 0x040019E0 RID: 6624
		private string write;

		// Token: 0x040019E1 RID: 6625
		private string changeAccessControl;

		// Token: 0x040019E2 RID: 6626
		private string viewAccessControl;
	}
}
