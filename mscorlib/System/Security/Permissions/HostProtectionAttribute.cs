﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005FC RID: 1532
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Delegate, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class HostProtectionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003A7C RID: 14972 RVA: 0x000C8F7C File Offset: 0x000C717C
		public HostProtectionAttribute() : base(SecurityAction.LinkDemand)
		{
		}

		// Token: 0x06003A7D RID: 14973 RVA: 0x000C8F88 File Offset: 0x000C7188
		public HostProtectionAttribute(SecurityAction action) : base(action)
		{
			if (action != SecurityAction.LinkDemand)
			{
				string message = string.Format(Locale.GetText("Only {0} is accepted."), SecurityAction.LinkDemand);
				throw new ArgumentException(message, "action");
			}
		}

		// Token: 0x17000AFC RID: 2812
		// (get) Token: 0x06003A7E RID: 14974 RVA: 0x000C8FC8 File Offset: 0x000C71C8
		// (set) Token: 0x06003A7F RID: 14975 RVA: 0x000C8FD8 File Offset: 0x000C71D8
		public bool ExternalProcessMgmt
		{
			get
			{
				return (this._resources & HostProtectionResource.ExternalProcessMgmt) != HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.ExternalProcessMgmt;
				}
				else
				{
					this._resources &= ~HostProtectionResource.ExternalProcessMgmt;
				}
			}
		}

		// Token: 0x17000AFD RID: 2813
		// (get) Token: 0x06003A80 RID: 14976 RVA: 0x000C9010 File Offset: 0x000C7210
		// (set) Token: 0x06003A81 RID: 14977 RVA: 0x000C9024 File Offset: 0x000C7224
		public bool ExternalThreading
		{
			get
			{
				return (this._resources & HostProtectionResource.ExternalThreading) != HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.ExternalThreading;
				}
				else
				{
					this._resources &= ~HostProtectionResource.ExternalThreading;
				}
			}
		}

		// Token: 0x17000AFE RID: 2814
		// (get) Token: 0x06003A82 RID: 14978 RVA: 0x000C9050 File Offset: 0x000C7250
		// (set) Token: 0x06003A83 RID: 14979 RVA: 0x000C9064 File Offset: 0x000C7264
		public bool MayLeakOnAbort
		{
			get
			{
				return (this._resources & HostProtectionResource.MayLeakOnAbort) != HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.MayLeakOnAbort;
				}
				else
				{
					this._resources &= ~HostProtectionResource.MayLeakOnAbort;
				}
			}
		}

		// Token: 0x17000AFF RID: 2815
		// (get) Token: 0x06003A84 RID: 14980 RVA: 0x000C9098 File Offset: 0x000C7298
		// (set) Token: 0x06003A85 RID: 14981 RVA: 0x000C90AC File Offset: 0x000C72AC
		[ComVisible(true)]
		public bool SecurityInfrastructure
		{
			get
			{
				return (this._resources & HostProtectionResource.SecurityInfrastructure) != HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.SecurityInfrastructure;
				}
				else
				{
					this._resources &= ~HostProtectionResource.SecurityInfrastructure;
				}
			}
		}

		// Token: 0x17000B00 RID: 2816
		// (get) Token: 0x06003A86 RID: 14982 RVA: 0x000C90D8 File Offset: 0x000C72D8
		// (set) Token: 0x06003A87 RID: 14983 RVA: 0x000C90E8 File Offset: 0x000C72E8
		public bool SelfAffectingProcessMgmt
		{
			get
			{
				return (this._resources & HostProtectionResource.SelfAffectingProcessMgmt) != HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.SelfAffectingProcessMgmt;
				}
				else
				{
					this._resources &= ~HostProtectionResource.SelfAffectingProcessMgmt;
				}
			}
		}

		// Token: 0x17000B01 RID: 2817
		// (get) Token: 0x06003A88 RID: 14984 RVA: 0x000C9120 File Offset: 0x000C7320
		// (set) Token: 0x06003A89 RID: 14985 RVA: 0x000C9134 File Offset: 0x000C7334
		public bool SelfAffectingThreading
		{
			get
			{
				return (this._resources & HostProtectionResource.SelfAffectingThreading) != HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.SelfAffectingThreading;
				}
				else
				{
					this._resources &= ~HostProtectionResource.SelfAffectingThreading;
				}
			}
		}

		// Token: 0x17000B02 RID: 2818
		// (get) Token: 0x06003A8A RID: 14986 RVA: 0x000C9160 File Offset: 0x000C7360
		// (set) Token: 0x06003A8B RID: 14987 RVA: 0x000C9170 File Offset: 0x000C7370
		public bool SharedState
		{
			get
			{
				return (this._resources & HostProtectionResource.SharedState) != HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.SharedState;
				}
				else
				{
					this._resources &= ~HostProtectionResource.SharedState;
				}
			}
		}

		// Token: 0x17000B03 RID: 2819
		// (get) Token: 0x06003A8C RID: 14988 RVA: 0x000C91A8 File Offset: 0x000C73A8
		// (set) Token: 0x06003A8D RID: 14989 RVA: 0x000C91B8 File Offset: 0x000C73B8
		public bool Synchronization
		{
			get
			{
				return (this._resources & HostProtectionResource.Synchronization) != HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.Synchronization;
				}
				else
				{
					this._resources &= ~HostProtectionResource.Synchronization;
				}
			}
		}

		// Token: 0x17000B04 RID: 2820
		// (get) Token: 0x06003A8E RID: 14990 RVA: 0x000C91F0 File Offset: 0x000C73F0
		// (set) Token: 0x06003A8F RID: 14991 RVA: 0x000C9204 File Offset: 0x000C7404
		public bool UI
		{
			get
			{
				return (this._resources & HostProtectionResource.UI) != HostProtectionResource.None;
			}
			set
			{
				if (value)
				{
					this._resources |= HostProtectionResource.UI;
				}
				else
				{
					this._resources &= ~HostProtectionResource.UI;
				}
			}
		}

		// Token: 0x17000B05 RID: 2821
		// (get) Token: 0x06003A90 RID: 14992 RVA: 0x000C9238 File Offset: 0x000C7438
		// (set) Token: 0x06003A91 RID: 14993 RVA: 0x000C9240 File Offset: 0x000C7440
		public HostProtectionResource Resources
		{
			get
			{
				return this._resources;
			}
			set
			{
				this._resources = value;
			}
		}

		// Token: 0x06003A92 RID: 14994 RVA: 0x000C924C File Offset: 0x000C744C
		public override IPermission CreatePermission()
		{
			return new HostProtectionPermission(this._resources);
		}

		// Token: 0x0400195D RID: 6493
		private HostProtectionResource _resources;
	}
}
