﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000618 RID: 1560
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum RegistryPermissionAccess
	{
		// Token: 0x040019D9 RID: 6617
		NoAccess = 0,
		// Token: 0x040019DA RID: 6618
		Read = 1,
		// Token: 0x040019DB RID: 6619
		Write = 2,
		// Token: 0x040019DC RID: 6620
		Create = 4,
		// Token: 0x040019DD RID: 6621
		AllAccess = 7
	}
}
