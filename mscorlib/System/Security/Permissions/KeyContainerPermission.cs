﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000607 RID: 1543
	[ComVisible(true)]
	[Serializable]
	public sealed class KeyContainerPermission : CodeAccessPermission, IBuiltInPermission, IUnrestrictedPermission
	{
		// Token: 0x06003ABA RID: 15034 RVA: 0x000C9A90 File Offset: 0x000C7C90
		public KeyContainerPermission(PermissionState state)
		{
			if (CodeAccessPermission.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this._flags = KeyContainerPermissionFlags.AllFlags;
			}
		}

		// Token: 0x06003ABB RID: 15035 RVA: 0x000C9AB0 File Offset: 0x000C7CB0
		public KeyContainerPermission(KeyContainerPermissionFlags flags)
		{
			this.SetFlags(flags);
		}

		// Token: 0x06003ABC RID: 15036 RVA: 0x000C9AC0 File Offset: 0x000C7CC0
		public KeyContainerPermission(KeyContainerPermissionFlags flags, KeyContainerPermissionAccessEntry[] accessList)
		{
			this.SetFlags(flags);
			if (accessList != null)
			{
				foreach (KeyContainerPermissionAccessEntry accessEntry in accessList)
				{
					this._accessEntries.Add(accessEntry);
				}
			}
		}

		// Token: 0x06003ABD RID: 15037 RVA: 0x000C9B08 File Offset: 0x000C7D08
		int IBuiltInPermission.GetTokenIndex()
		{
			return 16;
		}

		// Token: 0x17000B0B RID: 2827
		// (get) Token: 0x06003ABE RID: 15038 RVA: 0x000C9B0C File Offset: 0x000C7D0C
		public KeyContainerPermissionAccessEntryCollection AccessEntries
		{
			get
			{
				return this._accessEntries;
			}
		}

		// Token: 0x17000B0C RID: 2828
		// (get) Token: 0x06003ABF RID: 15039 RVA: 0x000C9B14 File Offset: 0x000C7D14
		public KeyContainerPermissionFlags Flags
		{
			get
			{
				return this._flags;
			}
		}

		// Token: 0x06003AC0 RID: 15040 RVA: 0x000C9B1C File Offset: 0x000C7D1C
		public override IPermission Copy()
		{
			if (this._accessEntries.Count == 0)
			{
				return new KeyContainerPermission(this._flags);
			}
			KeyContainerPermissionAccessEntry[] array = new KeyContainerPermissionAccessEntry[this._accessEntries.Count];
			this._accessEntries.CopyTo(array, 0);
			return new KeyContainerPermission(this._flags, array);
		}

		// Token: 0x06003AC1 RID: 15041 RVA: 0x000C9B70 File Offset: 0x000C7D70
		[MonoTODO("(2.0) missing support for AccessEntries")]
		public override void FromXml(SecurityElement securityElement)
		{
			CodeAccessPermission.CheckSecurityElement(securityElement, "securityElement", 1, 1);
			if (CodeAccessPermission.IsUnrestricted(securityElement))
			{
				this._flags = KeyContainerPermissionFlags.AllFlags;
			}
			else
			{
				this._flags = (KeyContainerPermissionFlags)((int)Enum.Parse(typeof(KeyContainerPermissionFlags), securityElement.Attribute("Flags")));
			}
		}

		// Token: 0x06003AC2 RID: 15042 RVA: 0x000C9BCC File Offset: 0x000C7DCC
		[MonoTODO("(2.0)")]
		public override IPermission Intersect(IPermission target)
		{
			return null;
		}

		// Token: 0x06003AC3 RID: 15043 RVA: 0x000C9BD0 File Offset: 0x000C7DD0
		[MonoTODO("(2.0)")]
		public override bool IsSubsetOf(IPermission target)
		{
			return false;
		}

		// Token: 0x06003AC4 RID: 15044 RVA: 0x000C9BD4 File Offset: 0x000C7DD4
		public bool IsUnrestricted()
		{
			return this._flags == KeyContainerPermissionFlags.AllFlags;
		}

		// Token: 0x06003AC5 RID: 15045 RVA: 0x000C9BE4 File Offset: 0x000C7DE4
		[MonoTODO("(2.0) missing support for AccessEntries")]
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this.IsUnrestricted())
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			return securityElement;
		}

		// Token: 0x06003AC6 RID: 15046 RVA: 0x000C9C1C File Offset: 0x000C7E1C
		public override IPermission Union(IPermission target)
		{
			KeyContainerPermission keyContainerPermission = this.Cast(target);
			if (keyContainerPermission == null)
			{
				return this.Copy();
			}
			KeyContainerPermissionAccessEntryCollection keyContainerPermissionAccessEntryCollection = new KeyContainerPermissionAccessEntryCollection();
			foreach (KeyContainerPermissionAccessEntry accessEntry in this._accessEntries)
			{
				keyContainerPermissionAccessEntryCollection.Add(accessEntry);
			}
			foreach (KeyContainerPermissionAccessEntry accessEntry2 in keyContainerPermission._accessEntries)
			{
				if (this._accessEntries.IndexOf(accessEntry2) == -1)
				{
					keyContainerPermissionAccessEntryCollection.Add(accessEntry2);
				}
			}
			if (keyContainerPermissionAccessEntryCollection.Count == 0)
			{
				return new KeyContainerPermission(this._flags | keyContainerPermission._flags);
			}
			KeyContainerPermissionAccessEntry[] array = new KeyContainerPermissionAccessEntry[keyContainerPermissionAccessEntryCollection.Count];
			keyContainerPermissionAccessEntryCollection.CopyTo(array, 0);
			return new KeyContainerPermission(this._flags | keyContainerPermission._flags, array);
		}

		// Token: 0x06003AC7 RID: 15047 RVA: 0x000C9CFC File Offset: 0x000C7EFC
		private void SetFlags(KeyContainerPermissionFlags flags)
		{
			if ((flags & KeyContainerPermissionFlags.AllFlags) != KeyContainerPermissionFlags.NoFlags)
			{
				string message = string.Format(Locale.GetText("Invalid enum {0}"), flags);
				throw new ArgumentException(message, "KeyContainerPermissionFlags");
			}
			this._flags = flags;
		}

		// Token: 0x06003AC8 RID: 15048 RVA: 0x000C9D40 File Offset: 0x000C7F40
		private KeyContainerPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			KeyContainerPermission keyContainerPermission = target as KeyContainerPermission;
			if (keyContainerPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(KeyContainerPermission));
			}
			return keyContainerPermission;
		}

		// Token: 0x04001994 RID: 6548
		private const int version = 1;

		// Token: 0x04001995 RID: 6549
		private KeyContainerPermissionAccessEntryCollection _accessEntries;

		// Token: 0x04001996 RID: 6550
		private KeyContainerPermissionFlags _flags;
	}
}
