﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000616 RID: 1558
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum ReflectionPermissionFlag
	{
		// Token: 0x040019CD RID: 6605
		NoFlags = 0,
		// Token: 0x040019CE RID: 6606
		[Obsolete("not used anymore")]
		TypeInformation = 1,
		// Token: 0x040019CF RID: 6607
		MemberAccess = 2,
		// Token: 0x040019D0 RID: 6608
		ReflectionEmit = 4,
		// Token: 0x040019D1 RID: 6609
		AllFlags = 7,
		// Token: 0x040019D2 RID: 6610
		[ComVisible(false)]
		RestrictedMemberAccess = 8
	}
}
