﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000615 RID: 1557
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class ReflectionPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003B4A RID: 15178 RVA: 0x000CB8EC File Offset: 0x000C9AEC
		public ReflectionPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000B2F RID: 2863
		// (get) Token: 0x06003B4B RID: 15179 RVA: 0x000CB8F8 File Offset: 0x000C9AF8
		// (set) Token: 0x06003B4C RID: 15180 RVA: 0x000CB900 File Offset: 0x000C9B00
		public ReflectionPermissionFlag Flags
		{
			get
			{
				return this.flags;
			}
			set
			{
				this.flags = value;
				this.memberAccess = ((this.flags & ReflectionPermissionFlag.MemberAccess) == ReflectionPermissionFlag.MemberAccess);
				this.reflectionEmit = ((this.flags & ReflectionPermissionFlag.ReflectionEmit) == ReflectionPermissionFlag.ReflectionEmit);
				this.typeInfo = ((this.flags & ReflectionPermissionFlag.TypeInformation) == ReflectionPermissionFlag.TypeInformation);
			}
		}

		// Token: 0x17000B30 RID: 2864
		// (get) Token: 0x06003B4D RID: 15181 RVA: 0x000CB948 File Offset: 0x000C9B48
		// (set) Token: 0x06003B4E RID: 15182 RVA: 0x000CB950 File Offset: 0x000C9B50
		public bool MemberAccess
		{
			get
			{
				return this.memberAccess;
			}
			set
			{
				if (value)
				{
					this.flags |= ReflectionPermissionFlag.MemberAccess;
				}
				else
				{
					this.flags -= 2;
				}
				this.memberAccess = value;
			}
		}

		// Token: 0x17000B31 RID: 2865
		// (get) Token: 0x06003B4F RID: 15183 RVA: 0x000CB98C File Offset: 0x000C9B8C
		// (set) Token: 0x06003B50 RID: 15184 RVA: 0x000CB994 File Offset: 0x000C9B94
		public bool ReflectionEmit
		{
			get
			{
				return this.reflectionEmit;
			}
			set
			{
				if (value)
				{
					this.flags |= ReflectionPermissionFlag.ReflectionEmit;
				}
				else
				{
					this.flags -= 4;
				}
				this.reflectionEmit = value;
			}
		}

		// Token: 0x17000B32 RID: 2866
		// (get) Token: 0x06003B51 RID: 15185 RVA: 0x000CB9D0 File Offset: 0x000C9BD0
		// (set) Token: 0x06003B52 RID: 15186 RVA: 0x000CB9E0 File Offset: 0x000C9BE0
		public bool RestrictedMemberAccess
		{
			get
			{
				return (this.flags & ReflectionPermissionFlag.RestrictedMemberAccess) == ReflectionPermissionFlag.RestrictedMemberAccess;
			}
			set
			{
				if (value)
				{
					this.flags |= ReflectionPermissionFlag.RestrictedMemberAccess;
				}
				else
				{
					this.flags -= 8;
				}
			}
		}

		// Token: 0x17000B33 RID: 2867
		// (get) Token: 0x06003B53 RID: 15187 RVA: 0x000CBA0C File Offset: 0x000C9C0C
		// (set) Token: 0x06003B54 RID: 15188 RVA: 0x000CBA14 File Offset: 0x000C9C14
		[Obsolete("not enforced in 2.0+")]
		public bool TypeInformation
		{
			get
			{
				return this.typeInfo;
			}
			set
			{
				if (value)
				{
					this.flags |= ReflectionPermissionFlag.TypeInformation;
				}
				else
				{
					this.flags--;
				}
				this.typeInfo = value;
			}
		}

		// Token: 0x06003B55 RID: 15189 RVA: 0x000CBA50 File Offset: 0x000C9C50
		public override IPermission CreatePermission()
		{
			ReflectionPermission result;
			if (base.Unrestricted)
			{
				result = new ReflectionPermission(PermissionState.Unrestricted);
			}
			else
			{
				result = new ReflectionPermission(this.flags);
			}
			return result;
		}

		// Token: 0x040019C8 RID: 6600
		private ReflectionPermissionFlag flags;

		// Token: 0x040019C9 RID: 6601
		private bool memberAccess;

		// Token: 0x040019CA RID: 6602
		private bool reflectionEmit;

		// Token: 0x040019CB RID: 6603
		private bool typeInfo;
	}
}
