﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000611 RID: 1553
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class PrincipalPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003B21 RID: 15137 RVA: 0x000CB128 File Offset: 0x000C9328
		public PrincipalPermissionAttribute(SecurityAction action) : base(action)
		{
			this.authenticated = true;
		}

		// Token: 0x17000B27 RID: 2855
		// (get) Token: 0x06003B22 RID: 15138 RVA: 0x000CB138 File Offset: 0x000C9338
		// (set) Token: 0x06003B23 RID: 15139 RVA: 0x000CB140 File Offset: 0x000C9340
		public bool Authenticated
		{
			get
			{
				return this.authenticated;
			}
			set
			{
				this.authenticated = value;
			}
		}

		// Token: 0x17000B28 RID: 2856
		// (get) Token: 0x06003B24 RID: 15140 RVA: 0x000CB14C File Offset: 0x000C934C
		// (set) Token: 0x06003B25 RID: 15141 RVA: 0x000CB154 File Offset: 0x000C9354
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000B29 RID: 2857
		// (get) Token: 0x06003B26 RID: 15142 RVA: 0x000CB160 File Offset: 0x000C9360
		// (set) Token: 0x06003B27 RID: 15143 RVA: 0x000CB168 File Offset: 0x000C9368
		public string Role
		{
			get
			{
				return this.role;
			}
			set
			{
				this.role = value;
			}
		}

		// Token: 0x06003B28 RID: 15144 RVA: 0x000CB174 File Offset: 0x000C9374
		public override IPermission CreatePermission()
		{
			PrincipalPermission result;
			if (base.Unrestricted)
			{
				result = new PrincipalPermission(PermissionState.Unrestricted);
			}
			else
			{
				result = new PrincipalPermission(this.name, this.role, this.authenticated);
			}
			return result;
		}

		// Token: 0x040019BE RID: 6590
		private bool authenticated;

		// Token: 0x040019BF RID: 6591
		private string name;

		// Token: 0x040019C0 RID: 6592
		private string role;
	}
}
