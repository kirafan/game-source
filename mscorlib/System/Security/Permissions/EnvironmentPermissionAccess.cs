﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005F2 RID: 1522
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum EnvironmentPermissionAccess
	{
		// Token: 0x04001934 RID: 6452
		NoAccess = 0,
		// Token: 0x04001935 RID: 6453
		Read = 1,
		// Token: 0x04001936 RID: 6454
		Write = 2,
		// Token: 0x04001937 RID: 6455
		AllAccess = 3
	}
}
