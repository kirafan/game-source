﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200060B RID: 1547
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class KeyContainerPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003AED RID: 15085 RVA: 0x000CA1C4 File Offset: 0x000C83C4
		public KeyContainerPermissionAttribute(SecurityAction action) : base(action)
		{
			this._spec = -1;
			this._type = -1;
		}

		// Token: 0x17000B19 RID: 2841
		// (get) Token: 0x06003AEE RID: 15086 RVA: 0x000CA1DC File Offset: 0x000C83DC
		// (set) Token: 0x06003AEF RID: 15087 RVA: 0x000CA1E4 File Offset: 0x000C83E4
		public KeyContainerPermissionFlags Flags
		{
			get
			{
				return this._flags;
			}
			set
			{
				this._flags = value;
			}
		}

		// Token: 0x17000B1A RID: 2842
		// (get) Token: 0x06003AF0 RID: 15088 RVA: 0x000CA1F0 File Offset: 0x000C83F0
		// (set) Token: 0x06003AF1 RID: 15089 RVA: 0x000CA1F8 File Offset: 0x000C83F8
		public string KeyContainerName
		{
			get
			{
				return this._containerName;
			}
			set
			{
				this._containerName = value;
			}
		}

		// Token: 0x17000B1B RID: 2843
		// (get) Token: 0x06003AF2 RID: 15090 RVA: 0x000CA204 File Offset: 0x000C8404
		// (set) Token: 0x06003AF3 RID: 15091 RVA: 0x000CA20C File Offset: 0x000C840C
		public int KeySpec
		{
			get
			{
				return this._spec;
			}
			set
			{
				this._spec = value;
			}
		}

		// Token: 0x17000B1C RID: 2844
		// (get) Token: 0x06003AF4 RID: 15092 RVA: 0x000CA218 File Offset: 0x000C8418
		// (set) Token: 0x06003AF5 RID: 15093 RVA: 0x000CA220 File Offset: 0x000C8420
		public string KeyStore
		{
			get
			{
				return this._store;
			}
			set
			{
				this._store = value;
			}
		}

		// Token: 0x17000B1D RID: 2845
		// (get) Token: 0x06003AF6 RID: 15094 RVA: 0x000CA22C File Offset: 0x000C842C
		// (set) Token: 0x06003AF7 RID: 15095 RVA: 0x000CA234 File Offset: 0x000C8434
		public string ProviderName
		{
			get
			{
				return this._providerName;
			}
			set
			{
				this._providerName = value;
			}
		}

		// Token: 0x17000B1E RID: 2846
		// (get) Token: 0x06003AF8 RID: 15096 RVA: 0x000CA240 File Offset: 0x000C8440
		// (set) Token: 0x06003AF9 RID: 15097 RVA: 0x000CA248 File Offset: 0x000C8448
		public int ProviderType
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}

		// Token: 0x06003AFA RID: 15098 RVA: 0x000CA254 File Offset: 0x000C8454
		public override IPermission CreatePermission()
		{
			if (base.Unrestricted)
			{
				return new KeyContainerPermission(PermissionState.Unrestricted);
			}
			if (this.EmptyEntry())
			{
				return new KeyContainerPermission(this._flags);
			}
			KeyContainerPermissionAccessEntry[] accessList = new KeyContainerPermissionAccessEntry[]
			{
				new KeyContainerPermissionAccessEntry(this._store, this._providerName, this._type, this._containerName, this._spec, this._flags)
			};
			return new KeyContainerPermission(this._flags, accessList);
		}

		// Token: 0x06003AFB RID: 15099 RVA: 0x000CA2CC File Offset: 0x000C84CC
		private bool EmptyEntry()
		{
			return this._containerName == null && this._spec == 0 && this._store == null && this._providerName == null && this._type == 0;
		}

		// Token: 0x0400199F RID: 6559
		private KeyContainerPermissionFlags _flags;

		// Token: 0x040019A0 RID: 6560
		private string _containerName;

		// Token: 0x040019A1 RID: 6561
		private int _spec;

		// Token: 0x040019A2 RID: 6562
		private string _store;

		// Token: 0x040019A3 RID: 6563
		private string _providerName;

		// Token: 0x040019A4 RID: 6564
		private int _type;
	}
}
