﻿using System;
using System.Collections;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000620 RID: 1568
	[ComVisible(true)]
	[Serializable]
	public sealed class StrongNameIdentityPermission : CodeAccessPermission, IBuiltInPermission
	{
		// Token: 0x06003BBE RID: 15294 RVA: 0x000CD348 File Offset: 0x000CB548
		public StrongNameIdentityPermission(PermissionState state)
		{
			this._state = CodeAccessPermission.CheckPermissionState(state, true);
			this._list = new ArrayList();
			this._list.Add(StrongNameIdentityPermission.SNIP.CreateDefault());
		}

		// Token: 0x06003BBF RID: 15295 RVA: 0x000CD38C File Offset: 0x000CB58C
		public StrongNameIdentityPermission(StrongNamePublicKeyBlob blob, string name, Version version)
		{
			if (blob == null)
			{
				throw new ArgumentNullException("blob");
			}
			if (name != null && name.Length == 0)
			{
				throw new ArgumentException("name");
			}
			this._state = PermissionState.None;
			this._list = new ArrayList();
			this._list.Add(new StrongNameIdentityPermission.SNIP(blob, name, version));
		}

		// Token: 0x06003BC0 RID: 15296 RVA: 0x000CD3F8 File Offset: 0x000CB5F8
		internal StrongNameIdentityPermission(StrongNameIdentityPermission snip)
		{
			this._state = snip._state;
			this._list = new ArrayList(snip._list.Count);
			foreach (object obj in snip._list)
			{
				StrongNameIdentityPermission.SNIP snip2 = (StrongNameIdentityPermission.SNIP)obj;
				this._list.Add(new StrongNameIdentityPermission.SNIP(snip2.PublicKey, snip2.Name, snip2.AssemblyVersion));
			}
		}

		// Token: 0x06003BC2 RID: 15298 RVA: 0x000CD4C4 File Offset: 0x000CB6C4
		int IBuiltInPermission.GetTokenIndex()
		{
			return 12;
		}

		// Token: 0x17000B4D RID: 2893
		// (get) Token: 0x06003BC3 RID: 15299 RVA: 0x000CD4C8 File Offset: 0x000CB6C8
		// (set) Token: 0x06003BC4 RID: 15300 RVA: 0x000CD508 File Offset: 0x000CB708
		public string Name
		{
			get
			{
				if (this._list.Count > 1)
				{
					throw new NotSupportedException();
				}
				return ((StrongNameIdentityPermission.SNIP)this._list[0]).Name;
			}
			set
			{
				if (value != null && value.Length == 0)
				{
					throw new ArgumentException("name");
				}
				if (this._list.Count > 1)
				{
					this.ResetToDefault();
				}
				StrongNameIdentityPermission.SNIP snip = (StrongNameIdentityPermission.SNIP)this._list[0];
				snip.Name = value;
				this._list[0] = snip;
			}
		}

		// Token: 0x17000B4E RID: 2894
		// (get) Token: 0x06003BC5 RID: 15301 RVA: 0x000CD574 File Offset: 0x000CB774
		// (set) Token: 0x06003BC6 RID: 15302 RVA: 0x000CD5B4 File Offset: 0x000CB7B4
		public StrongNamePublicKeyBlob PublicKey
		{
			get
			{
				if (this._list.Count > 1)
				{
					throw new NotSupportedException();
				}
				return ((StrongNameIdentityPermission.SNIP)this._list[0]).PublicKey;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (this._list.Count > 1)
				{
					this.ResetToDefault();
				}
				StrongNameIdentityPermission.SNIP snip = (StrongNameIdentityPermission.SNIP)this._list[0];
				snip.PublicKey = value;
				this._list[0] = snip;
			}
		}

		// Token: 0x17000B4F RID: 2895
		// (get) Token: 0x06003BC7 RID: 15303 RVA: 0x000CD618 File Offset: 0x000CB818
		// (set) Token: 0x06003BC8 RID: 15304 RVA: 0x000CD658 File Offset: 0x000CB858
		public Version Version
		{
			get
			{
				if (this._list.Count > 1)
				{
					throw new NotSupportedException();
				}
				return ((StrongNameIdentityPermission.SNIP)this._list[0]).AssemblyVersion;
			}
			set
			{
				if (this._list.Count > 1)
				{
					this.ResetToDefault();
				}
				StrongNameIdentityPermission.SNIP snip = (StrongNameIdentityPermission.SNIP)this._list[0];
				snip.AssemblyVersion = value;
				this._list[0] = snip;
			}
		}

		// Token: 0x06003BC9 RID: 15305 RVA: 0x000CD6A8 File Offset: 0x000CB8A8
		internal void ResetToDefault()
		{
			this._list.Clear();
			this._list.Add(StrongNameIdentityPermission.SNIP.CreateDefault());
		}

		// Token: 0x06003BCA RID: 15306 RVA: 0x000CD6CC File Offset: 0x000CB8CC
		public override IPermission Copy()
		{
			if (this.IsEmpty())
			{
				return new StrongNameIdentityPermission(PermissionState.None);
			}
			return new StrongNameIdentityPermission(this);
		}

		// Token: 0x06003BCB RID: 15307 RVA: 0x000CD6E8 File Offset: 0x000CB8E8
		public override void FromXml(SecurityElement e)
		{
			CodeAccessPermission.CheckSecurityElement(e, "e", 1, 1);
			this._list.Clear();
			if (e.Children != null && e.Children.Count > 0)
			{
				foreach (object obj in e.Children)
				{
					SecurityElement se = (SecurityElement)obj;
					this._list.Add(this.FromSecurityElement(se));
				}
			}
			else
			{
				this._list.Add(this.FromSecurityElement(e));
			}
		}

		// Token: 0x06003BCC RID: 15308 RVA: 0x000CD7BC File Offset: 0x000CB9BC
		private StrongNameIdentityPermission.SNIP FromSecurityElement(SecurityElement se)
		{
			string name = se.Attribute("Name");
			StrongNamePublicKeyBlob pk = StrongNamePublicKeyBlob.FromString(se.Attribute("PublicKeyBlob"));
			string text = se.Attribute("AssemblyVersion");
			Version version = (text != null) ? new Version(text) : null;
			return new StrongNameIdentityPermission.SNIP(pk, name, version);
		}

		// Token: 0x06003BCD RID: 15309 RVA: 0x000CD810 File Offset: 0x000CBA10
		public override IPermission Intersect(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			StrongNameIdentityPermission strongNameIdentityPermission = target as StrongNameIdentityPermission;
			if (strongNameIdentityPermission == null)
			{
				throw new ArgumentException(Locale.GetText("Wrong permission type."));
			}
			if (this.IsEmpty() || strongNameIdentityPermission.IsEmpty())
			{
				return null;
			}
			if (!this.Match(strongNameIdentityPermission.Name))
			{
				return null;
			}
			string name = (this.Name.Length >= strongNameIdentityPermission.Name.Length) ? strongNameIdentityPermission.Name : this.Name;
			if (!this.Version.Equals(strongNameIdentityPermission.Version))
			{
				return null;
			}
			if (!this.PublicKey.Equals(strongNameIdentityPermission.PublicKey))
			{
				return null;
			}
			return new StrongNameIdentityPermission(this.PublicKey, name, this.Version);
		}

		// Token: 0x06003BCE RID: 15310 RVA: 0x000CD8DC File Offset: 0x000CBADC
		public override bool IsSubsetOf(IPermission target)
		{
			StrongNameIdentityPermission strongNameIdentityPermission = this.Cast(target);
			if (strongNameIdentityPermission == null)
			{
				return this.IsEmpty();
			}
			if (this.IsEmpty())
			{
				return true;
			}
			if (this.IsUnrestricted())
			{
				return strongNameIdentityPermission.IsUnrestricted();
			}
			if (strongNameIdentityPermission.IsUnrestricted())
			{
				return true;
			}
			foreach (object obj in this._list)
			{
				StrongNameIdentityPermission.SNIP snip = (StrongNameIdentityPermission.SNIP)obj;
				foreach (object obj2 in strongNameIdentityPermission._list)
				{
					StrongNameIdentityPermission.SNIP target2 = (StrongNameIdentityPermission.SNIP)obj2;
					if (!snip.IsSubsetOf(target2))
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06003BCF RID: 15311 RVA: 0x000CDA00 File Offset: 0x000CBC00
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this._list.Count > 1)
			{
				foreach (object obj in this._list)
				{
					StrongNameIdentityPermission.SNIP snip = (StrongNameIdentityPermission.SNIP)obj;
					SecurityElement securityElement2 = new SecurityElement("StrongName");
					this.ToSecurityElement(securityElement2, snip);
					securityElement.AddChild(securityElement2);
				}
			}
			else if (this._list.Count == 1)
			{
				StrongNameIdentityPermission.SNIP snip2 = (StrongNameIdentityPermission.SNIP)this._list[0];
				if (!this.IsEmpty(snip2))
				{
					this.ToSecurityElement(securityElement, snip2);
				}
			}
			return securityElement;
		}

		// Token: 0x06003BD0 RID: 15312 RVA: 0x000CDAE0 File Offset: 0x000CBCE0
		private void ToSecurityElement(SecurityElement se, StrongNameIdentityPermission.SNIP snip)
		{
			if (snip.PublicKey != null)
			{
				se.AddAttribute("PublicKeyBlob", snip.PublicKey.ToString());
			}
			if (snip.Name != null)
			{
				se.AddAttribute("Name", snip.Name);
			}
			if (snip.AssemblyVersion != null)
			{
				se.AddAttribute("AssemblyVersion", snip.AssemblyVersion.ToString());
			}
		}

		// Token: 0x06003BD1 RID: 15313 RVA: 0x000CDB58 File Offset: 0x000CBD58
		public override IPermission Union(IPermission target)
		{
			StrongNameIdentityPermission strongNameIdentityPermission = this.Cast(target);
			if (strongNameIdentityPermission == null || strongNameIdentityPermission.IsEmpty())
			{
				return this.Copy();
			}
			if (this.IsEmpty())
			{
				return strongNameIdentityPermission.Copy();
			}
			StrongNameIdentityPermission strongNameIdentityPermission2 = (StrongNameIdentityPermission)this.Copy();
			foreach (object obj in strongNameIdentityPermission._list)
			{
				StrongNameIdentityPermission.SNIP snip = (StrongNameIdentityPermission.SNIP)obj;
				if (!this.IsEmpty(snip) && !this.Contains(snip))
				{
					strongNameIdentityPermission2._list.Add(snip);
				}
			}
			return strongNameIdentityPermission2;
		}

		// Token: 0x06003BD2 RID: 15314 RVA: 0x000CDC2C File Offset: 0x000CBE2C
		private bool IsUnrestricted()
		{
			return this._state == PermissionState.Unrestricted;
		}

		// Token: 0x06003BD3 RID: 15315 RVA: 0x000CDC38 File Offset: 0x000CBE38
		private bool Contains(StrongNameIdentityPermission.SNIP snip)
		{
			foreach (object obj in this._list)
			{
				StrongNameIdentityPermission.SNIP snip2 = (StrongNameIdentityPermission.SNIP)obj;
				bool flag = (snip2.PublicKey == null && snip.PublicKey == null) || (snip2.PublicKey != null && snip2.PublicKey.Equals(snip.PublicKey));
				bool flag2 = snip2.IsNameSubsetOf(snip.Name);
				bool flag3 = (snip2.AssemblyVersion == null && snip.AssemblyVersion == null) || (snip2.AssemblyVersion != null && snip2.AssemblyVersion.Equals(snip.AssemblyVersion));
				if (flag && flag2 && flag3)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003BD4 RID: 15316 RVA: 0x000CDD60 File Offset: 0x000CBF60
		private bool IsEmpty(StrongNameIdentityPermission.SNIP snip)
		{
			return this.PublicKey == null && (this.Name == null || this.Name.Length <= 0) && (this.Version == null || StrongNameIdentityPermission.defaultVersion.Equals(this.Version));
		}

		// Token: 0x06003BD5 RID: 15317 RVA: 0x000CDDBC File Offset: 0x000CBFBC
		private bool IsEmpty()
		{
			return !this.IsUnrestricted() && this._list.Count <= 1 && this.PublicKey == null && (this.Name == null || this.Name.Length <= 0) && (this.Version == null || StrongNameIdentityPermission.defaultVersion.Equals(this.Version));
		}

		// Token: 0x06003BD6 RID: 15318 RVA: 0x000CDE38 File Offset: 0x000CC038
		private StrongNameIdentityPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			StrongNameIdentityPermission strongNameIdentityPermission = target as StrongNameIdentityPermission;
			if (strongNameIdentityPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(StrongNameIdentityPermission));
			}
			return strongNameIdentityPermission;
		}

		// Token: 0x06003BD7 RID: 15319 RVA: 0x000CDE6C File Offset: 0x000CC06C
		private bool Match(string target)
		{
			if (this.Name == null || target == null)
			{
				return false;
			}
			int num = this.Name.LastIndexOf('*');
			int num2 = target.LastIndexOf('*');
			int length;
			if (num == -1 && num2 == -1)
			{
				length = Math.Max(this.Name.Length, target.Length);
			}
			else if (num == -1)
			{
				length = num2;
			}
			else if (num2 == -1)
			{
				length = num;
			}
			else
			{
				length = Math.Min(num, num2);
			}
			return string.Compare(this.Name, 0, target, 0, length, true, CultureInfo.InvariantCulture) == 0;
		}

		// Token: 0x04001A05 RID: 6661
		private const int version = 1;

		// Token: 0x04001A06 RID: 6662
		private static Version defaultVersion = new Version(0, 0);

		// Token: 0x04001A07 RID: 6663
		private PermissionState _state;

		// Token: 0x04001A08 RID: 6664
		private ArrayList _list;

		// Token: 0x02000621 RID: 1569
		private struct SNIP
		{
			// Token: 0x06003BD8 RID: 15320 RVA: 0x000CDF10 File Offset: 0x000CC110
			internal SNIP(StrongNamePublicKeyBlob pk, string name, Version version)
			{
				this.PublicKey = pk;
				this.Name = name;
				this.AssemblyVersion = version;
			}

			// Token: 0x06003BD9 RID: 15321 RVA: 0x000CDF28 File Offset: 0x000CC128
			internal static StrongNameIdentityPermission.SNIP CreateDefault()
			{
				return new StrongNameIdentityPermission.SNIP(null, string.Empty, (Version)StrongNameIdentityPermission.defaultVersion.Clone());
			}

			// Token: 0x06003BDA RID: 15322 RVA: 0x000CDF44 File Offset: 0x000CC144
			internal bool IsNameSubsetOf(string target)
			{
				if (this.Name == null)
				{
					return target == null;
				}
				if (target == null)
				{
					return true;
				}
				int num = this.Name.LastIndexOf('*');
				if (num == 0)
				{
					return true;
				}
				if (num == -1)
				{
					num = this.Name.Length;
				}
				return string.Compare(this.Name, 0, target, 0, num, true, CultureInfo.InvariantCulture) == 0;
			}

			// Token: 0x06003BDB RID: 15323 RVA: 0x000CDFAC File Offset: 0x000CC1AC
			internal bool IsSubsetOf(StrongNameIdentityPermission.SNIP target)
			{
				return (this.PublicKey != null && this.PublicKey.Equals(target.PublicKey)) || (this.IsNameSubsetOf(target.Name) && (!(this.AssemblyVersion != null) || this.AssemblyVersion.Equals(target.AssemblyVersion)) && this.PublicKey == null && target.PublicKey == null);
			}

			// Token: 0x04001A09 RID: 6665
			public StrongNamePublicKeyBlob PublicKey;

			// Token: 0x04001A0A RID: 6666
			public string Name;

			// Token: 0x04001A0B RID: 6667
			public Version AssemblyVersion;
		}
	}
}
