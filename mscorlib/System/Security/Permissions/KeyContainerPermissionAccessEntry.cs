﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography;

namespace System.Security.Permissions
{
	// Token: 0x02000608 RID: 1544
	[ComVisible(true)]
	[Serializable]
	public sealed class KeyContainerPermissionAccessEntry
	{
		// Token: 0x06003AC9 RID: 15049 RVA: 0x000C9D74 File Offset: 0x000C7F74
		public KeyContainerPermissionAccessEntry(CspParameters parameters, KeyContainerPermissionFlags flags)
		{
			if (parameters == null)
			{
				throw new ArgumentNullException("parameters");
			}
			this.ProviderName = parameters.ProviderName;
			this.ProviderType = parameters.ProviderType;
			this.KeyContainerName = parameters.KeyContainerName;
			this.KeySpec = parameters.KeyNumber;
			this.Flags = flags;
		}

		// Token: 0x06003ACA RID: 15050 RVA: 0x000C9DD0 File Offset: 0x000C7FD0
		public KeyContainerPermissionAccessEntry(string keyContainerName, KeyContainerPermissionFlags flags)
		{
			this.KeyContainerName = keyContainerName;
			this.Flags = flags;
		}

		// Token: 0x06003ACB RID: 15051 RVA: 0x000C9DE8 File Offset: 0x000C7FE8
		public KeyContainerPermissionAccessEntry(string keyStore, string providerName, int providerType, string keyContainerName, int keySpec, KeyContainerPermissionFlags flags)
		{
			this.KeyStore = keyStore;
			this.ProviderName = providerName;
			this.ProviderType = providerType;
			this.KeyContainerName = keyContainerName;
			this.KeySpec = keySpec;
			this.Flags = flags;
		}

		// Token: 0x17000B0D RID: 2829
		// (get) Token: 0x06003ACC RID: 15052 RVA: 0x000C9E28 File Offset: 0x000C8028
		// (set) Token: 0x06003ACD RID: 15053 RVA: 0x000C9E30 File Offset: 0x000C8030
		public KeyContainerPermissionFlags Flags
		{
			get
			{
				return this._flags;
			}
			set
			{
				if ((value & KeyContainerPermissionFlags.AllFlags) != KeyContainerPermissionFlags.NoFlags)
				{
					string message = string.Format(Locale.GetText("Invalid enum {0}"), value);
					throw new ArgumentException(message, "KeyContainerPermissionFlags");
				}
				this._flags = value;
			}
		}

		// Token: 0x17000B0E RID: 2830
		// (get) Token: 0x06003ACE RID: 15054 RVA: 0x000C9E74 File Offset: 0x000C8074
		// (set) Token: 0x06003ACF RID: 15055 RVA: 0x000C9E7C File Offset: 0x000C807C
		public string KeyContainerName
		{
			get
			{
				return this._containerName;
			}
			set
			{
				this._containerName = value;
			}
		}

		// Token: 0x17000B0F RID: 2831
		// (get) Token: 0x06003AD0 RID: 15056 RVA: 0x000C9E88 File Offset: 0x000C8088
		// (set) Token: 0x06003AD1 RID: 15057 RVA: 0x000C9E90 File Offset: 0x000C8090
		public int KeySpec
		{
			get
			{
				return this._spec;
			}
			set
			{
				this._spec = value;
			}
		}

		// Token: 0x17000B10 RID: 2832
		// (get) Token: 0x06003AD2 RID: 15058 RVA: 0x000C9E9C File Offset: 0x000C809C
		// (set) Token: 0x06003AD3 RID: 15059 RVA: 0x000C9EA4 File Offset: 0x000C80A4
		public string KeyStore
		{
			get
			{
				return this._store;
			}
			set
			{
				this._store = value;
			}
		}

		// Token: 0x17000B11 RID: 2833
		// (get) Token: 0x06003AD4 RID: 15060 RVA: 0x000C9EB0 File Offset: 0x000C80B0
		// (set) Token: 0x06003AD5 RID: 15061 RVA: 0x000C9EB8 File Offset: 0x000C80B8
		public string ProviderName
		{
			get
			{
				return this._providerName;
			}
			set
			{
				this._providerName = value;
			}
		}

		// Token: 0x17000B12 RID: 2834
		// (get) Token: 0x06003AD6 RID: 15062 RVA: 0x000C9EC4 File Offset: 0x000C80C4
		// (set) Token: 0x06003AD7 RID: 15063 RVA: 0x000C9ECC File Offset: 0x000C80CC
		public int ProviderType
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}

		// Token: 0x06003AD8 RID: 15064 RVA: 0x000C9ED8 File Offset: 0x000C80D8
		public override bool Equals(object o)
		{
			if (o == null)
			{
				return false;
			}
			KeyContainerPermissionAccessEntry keyContainerPermissionAccessEntry = o as KeyContainerPermissionAccessEntry;
			return keyContainerPermissionAccessEntry != null && this._flags == keyContainerPermissionAccessEntry._flags && !(this._containerName != keyContainerPermissionAccessEntry._containerName) && !(this._store != keyContainerPermissionAccessEntry._store) && !(this._providerName != keyContainerPermissionAccessEntry._providerName) && this._type == keyContainerPermissionAccessEntry._type;
		}

		// Token: 0x06003AD9 RID: 15065 RVA: 0x000C9F6C File Offset: 0x000C816C
		public override int GetHashCode()
		{
			int num = this._type ^ this._spec ^ (int)this._flags;
			if (this._containerName != null)
			{
				num ^= this._containerName.GetHashCode();
			}
			if (this._store != null)
			{
				num ^= this._store.GetHashCode();
			}
			if (this._providerName != null)
			{
				num ^= this._providerName.GetHashCode();
			}
			return num;
		}

		// Token: 0x04001997 RID: 6551
		private KeyContainerPermissionFlags _flags;

		// Token: 0x04001998 RID: 6552
		private string _containerName;

		// Token: 0x04001999 RID: 6553
		private int _spec;

		// Token: 0x0400199A RID: 6554
		private string _store;

		// Token: 0x0400199B RID: 6555
		private string _providerName;

		// Token: 0x0400199C RID: 6556
		private int _type;
	}
}
