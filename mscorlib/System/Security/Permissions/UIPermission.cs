﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000624 RID: 1572
	[ComVisible(true)]
	[Serializable]
	public sealed class UIPermission : CodeAccessPermission, IBuiltInPermission, IUnrestrictedPermission
	{
		// Token: 0x06003BEA RID: 15338 RVA: 0x000CE2DC File Offset: 0x000CC4DC
		public UIPermission(PermissionState state)
		{
			if (CodeAccessPermission.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this._clipboard = UIPermissionClipboard.AllClipboard;
				this._window = UIPermissionWindow.AllWindows;
			}
		}

		// Token: 0x06003BEB RID: 15339 RVA: 0x000CE300 File Offset: 0x000CC500
		public UIPermission(UIPermissionClipboard clipboardFlag)
		{
			this.Clipboard = clipboardFlag;
		}

		// Token: 0x06003BEC RID: 15340 RVA: 0x000CE310 File Offset: 0x000CC510
		public UIPermission(UIPermissionWindow windowFlag)
		{
			this.Window = windowFlag;
		}

		// Token: 0x06003BED RID: 15341 RVA: 0x000CE320 File Offset: 0x000CC520
		public UIPermission(UIPermissionWindow windowFlag, UIPermissionClipboard clipboardFlag)
		{
			this.Clipboard = clipboardFlag;
			this.Window = windowFlag;
		}

		// Token: 0x06003BEE RID: 15342 RVA: 0x000CE338 File Offset: 0x000CC538
		int IBuiltInPermission.GetTokenIndex()
		{
			return 7;
		}

		// Token: 0x17000B53 RID: 2899
		// (get) Token: 0x06003BEF RID: 15343 RVA: 0x000CE33C File Offset: 0x000CC53C
		// (set) Token: 0x06003BF0 RID: 15344 RVA: 0x000CE344 File Offset: 0x000CC544
		public UIPermissionClipboard Clipboard
		{
			get
			{
				return this._clipboard;
			}
			set
			{
				if (!Enum.IsDefined(typeof(UIPermissionClipboard), value))
				{
					string message = string.Format(Locale.GetText("Invalid enum {0}"), value);
					throw new ArgumentException(message, "UIPermissionClipboard");
				}
				this._clipboard = value;
			}
		}

		// Token: 0x17000B54 RID: 2900
		// (get) Token: 0x06003BF1 RID: 15345 RVA: 0x000CE394 File Offset: 0x000CC594
		// (set) Token: 0x06003BF2 RID: 15346 RVA: 0x000CE39C File Offset: 0x000CC59C
		public UIPermissionWindow Window
		{
			get
			{
				return this._window;
			}
			set
			{
				if (!Enum.IsDefined(typeof(UIPermissionWindow), value))
				{
					string message = string.Format(Locale.GetText("Invalid enum {0}"), value);
					throw new ArgumentException(message, "UIPermissionWindow");
				}
				this._window = value;
			}
		}

		// Token: 0x06003BF3 RID: 15347 RVA: 0x000CE3EC File Offset: 0x000CC5EC
		public override IPermission Copy()
		{
			return new UIPermission(this._window, this._clipboard);
		}

		// Token: 0x06003BF4 RID: 15348 RVA: 0x000CE400 File Offset: 0x000CC600
		public override void FromXml(SecurityElement esd)
		{
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			if (CodeAccessPermission.IsUnrestricted(esd))
			{
				this._window = UIPermissionWindow.AllWindows;
				this._clipboard = UIPermissionClipboard.AllClipboard;
			}
			else
			{
				string text = esd.Attribute("Window");
				if (text == null)
				{
					this._window = UIPermissionWindow.NoWindows;
				}
				else
				{
					this._window = (UIPermissionWindow)((int)Enum.Parse(typeof(UIPermissionWindow), text));
				}
				string text2 = esd.Attribute("Clipboard");
				if (text2 == null)
				{
					this._clipboard = UIPermissionClipboard.NoClipboard;
				}
				else
				{
					this._clipboard = (UIPermissionClipboard)((int)Enum.Parse(typeof(UIPermissionClipboard), text2));
				}
			}
		}

		// Token: 0x06003BF5 RID: 15349 RVA: 0x000CE4AC File Offset: 0x000CC6AC
		public override IPermission Intersect(IPermission target)
		{
			UIPermission uipermission = this.Cast(target);
			if (uipermission == null)
			{
				return null;
			}
			UIPermissionWindow uipermissionWindow = (this._window >= uipermission._window) ? uipermission._window : this._window;
			UIPermissionClipboard uipermissionClipboard = (this._clipboard >= uipermission._clipboard) ? uipermission._clipboard : this._clipboard;
			if (this.IsEmpty(uipermissionWindow, uipermissionClipboard))
			{
				return null;
			}
			return new UIPermission(uipermissionWindow, uipermissionClipboard);
		}

		// Token: 0x06003BF6 RID: 15350 RVA: 0x000CE528 File Offset: 0x000CC728
		public override bool IsSubsetOf(IPermission target)
		{
			UIPermission uipermission = this.Cast(target);
			if (uipermission == null)
			{
				return this.IsEmpty(this._window, this._clipboard);
			}
			return uipermission.IsUnrestricted() || (this._window <= uipermission._window && this._clipboard <= uipermission._clipboard);
		}

		// Token: 0x06003BF7 RID: 15351 RVA: 0x000CE588 File Offset: 0x000CC788
		public bool IsUnrestricted()
		{
			return this._window == UIPermissionWindow.AllWindows && this._clipboard == UIPermissionClipboard.AllClipboard;
		}

		// Token: 0x06003BF8 RID: 15352 RVA: 0x000CE5A4 File Offset: 0x000CC7A4
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this._window == UIPermissionWindow.AllWindows && this._clipboard == UIPermissionClipboard.AllClipboard)
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			else
			{
				if (this._window != UIPermissionWindow.NoWindows)
				{
					securityElement.AddAttribute("Window", this._window.ToString());
				}
				if (this._clipboard != UIPermissionClipboard.NoClipboard)
				{
					securityElement.AddAttribute("Clipboard", this._clipboard.ToString());
				}
			}
			return securityElement;
		}

		// Token: 0x06003BF9 RID: 15353 RVA: 0x000CE634 File Offset: 0x000CC834
		public override IPermission Union(IPermission target)
		{
			UIPermission uipermission = this.Cast(target);
			if (uipermission == null)
			{
				return this.Copy();
			}
			UIPermissionWindow uipermissionWindow = (this._window <= uipermission._window) ? uipermission._window : this._window;
			UIPermissionClipboard uipermissionClipboard = (this._clipboard <= uipermission._clipboard) ? uipermission._clipboard : this._clipboard;
			if (this.IsEmpty(uipermissionWindow, uipermissionClipboard))
			{
				return null;
			}
			return new UIPermission(uipermissionWindow, uipermissionClipboard);
		}

		// Token: 0x06003BFA RID: 15354 RVA: 0x000CE6B4 File Offset: 0x000CC8B4
		private bool IsEmpty(UIPermissionWindow w, UIPermissionClipboard c)
		{
			return w == UIPermissionWindow.NoWindows && c == UIPermissionClipboard.NoClipboard;
		}

		// Token: 0x06003BFB RID: 15355 RVA: 0x000CE6C4 File Offset: 0x000CC8C4
		private UIPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			UIPermission uipermission = target as UIPermission;
			if (uipermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(UIPermission));
			}
			return uipermission;
		}

		// Token: 0x04001A10 RID: 6672
		private const int version = 1;

		// Token: 0x04001A11 RID: 6673
		private UIPermissionWindow _window;

		// Token: 0x04001A12 RID: 6674
		private UIPermissionClipboard _clipboard;
	}
}
