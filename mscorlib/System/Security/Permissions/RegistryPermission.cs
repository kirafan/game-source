﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;

namespace System.Security.Permissions
{
	// Token: 0x02000617 RID: 1559
	[ComVisible(true)]
	[Serializable]
	public sealed class RegistryPermission : CodeAccessPermission, IBuiltInPermission, IUnrestrictedPermission
	{
		// Token: 0x06003B56 RID: 15190 RVA: 0x000CBA84 File Offset: 0x000C9C84
		public RegistryPermission(PermissionState state)
		{
			this._state = CodeAccessPermission.CheckPermissionState(state, true);
			this.createList = new ArrayList();
			this.readList = new ArrayList();
			this.writeList = new ArrayList();
		}

		// Token: 0x06003B57 RID: 15191 RVA: 0x000CBAC8 File Offset: 0x000C9CC8
		public RegistryPermission(RegistryPermissionAccess access, string pathList)
		{
			this._state = PermissionState.None;
			this.createList = new ArrayList();
			this.readList = new ArrayList();
			this.writeList = new ArrayList();
			this.AddPathList(access, pathList);
		}

		// Token: 0x06003B58 RID: 15192 RVA: 0x000CBB0C File Offset: 0x000C9D0C
		public RegistryPermission(RegistryPermissionAccess access, AccessControlActions control, string pathList)
		{
			if (!Enum.IsDefined(typeof(AccessControlActions), control))
			{
				string message = string.Format(Locale.GetText("Invalid enum {0}"), control);
				throw new ArgumentException(message, "AccessControlActions");
			}
			this._state = PermissionState.None;
			this.AddPathList(access, control, pathList);
		}

		// Token: 0x06003B59 RID: 15193 RVA: 0x000CBB6C File Offset: 0x000C9D6C
		int IBuiltInPermission.GetTokenIndex()
		{
			return 5;
		}

		// Token: 0x06003B5A RID: 15194 RVA: 0x000CBB70 File Offset: 0x000C9D70
		public void AddPathList(RegistryPermissionAccess access, string pathList)
		{
			if (pathList == null)
			{
				throw new ArgumentNullException("pathList");
			}
			switch (access)
			{
			case RegistryPermissionAccess.NoAccess:
				return;
			case RegistryPermissionAccess.Read:
				this.AddWithUnionKey(this.readList, pathList);
				return;
			case RegistryPermissionAccess.Write:
				this.AddWithUnionKey(this.writeList, pathList);
				return;
			case RegistryPermissionAccess.Create:
				this.AddWithUnionKey(this.createList, pathList);
				return;
			case RegistryPermissionAccess.AllAccess:
				this.AddWithUnionKey(this.createList, pathList);
				this.AddWithUnionKey(this.readList, pathList);
				this.AddWithUnionKey(this.writeList, pathList);
				return;
			}
			this.ThrowInvalidFlag(access, false);
		}

		// Token: 0x06003B5B RID: 15195 RVA: 0x000CBC30 File Offset: 0x000C9E30
		[MonoTODO("(2.0) Access Control isn't implemented")]
		public void AddPathList(RegistryPermissionAccess access, AccessControlActions control, string pathList)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003B5C RID: 15196 RVA: 0x000CBC38 File Offset: 0x000C9E38
		public string GetPathList(RegistryPermissionAccess access)
		{
			switch (access)
			{
			case RegistryPermissionAccess.NoAccess:
			case RegistryPermissionAccess.AllAccess:
				this.ThrowInvalidFlag(access, true);
				goto IL_6E;
			case RegistryPermissionAccess.Read:
				return this.GetPathList(this.readList);
			case RegistryPermissionAccess.Write:
				return this.GetPathList(this.writeList);
			case RegistryPermissionAccess.Create:
				return this.GetPathList(this.createList);
			}
			this.ThrowInvalidFlag(access, false);
			IL_6E:
			return null;
		}

		// Token: 0x06003B5D RID: 15197 RVA: 0x000CBCB4 File Offset: 0x000C9EB4
		public void SetPathList(RegistryPermissionAccess access, string pathList)
		{
			if (pathList == null)
			{
				throw new ArgumentNullException("pathList");
			}
			switch (access)
			{
			case RegistryPermissionAccess.NoAccess:
				return;
			case RegistryPermissionAccess.Read:
			{
				this.readList.Clear();
				string[] array = pathList.Split(new char[]
				{
					';'
				});
				foreach (string value in array)
				{
					this.readList.Add(value);
				}
				return;
			}
			case RegistryPermissionAccess.Write:
			{
				this.writeList.Clear();
				string[] array = pathList.Split(new char[]
				{
					';'
				});
				foreach (string value2 in array)
				{
					this.writeList.Add(value2);
				}
				return;
			}
			case RegistryPermissionAccess.Create:
			{
				this.createList.Clear();
				string[] array = pathList.Split(new char[]
				{
					';'
				});
				foreach (string value3 in array)
				{
					this.createList.Add(value3);
				}
				return;
			}
			case RegistryPermissionAccess.AllAccess:
			{
				this.createList.Clear();
				this.readList.Clear();
				this.writeList.Clear();
				string[] array = pathList.Split(new char[]
				{
					';'
				});
				foreach (string value4 in array)
				{
					this.createList.Add(value4);
					this.readList.Add(value4);
					this.writeList.Add(value4);
				}
				return;
			}
			}
			this.ThrowInvalidFlag(access, false);
		}

		// Token: 0x06003B5E RID: 15198 RVA: 0x000CBE88 File Offset: 0x000CA088
		public override IPermission Copy()
		{
			RegistryPermission registryPermission = new RegistryPermission(this._state);
			string pathList = this.GetPathList(RegistryPermissionAccess.Create);
			if (pathList != null)
			{
				registryPermission.SetPathList(RegistryPermissionAccess.Create, pathList);
			}
			pathList = this.GetPathList(RegistryPermissionAccess.Read);
			if (pathList != null)
			{
				registryPermission.SetPathList(RegistryPermissionAccess.Read, pathList);
			}
			pathList = this.GetPathList(RegistryPermissionAccess.Write);
			if (pathList != null)
			{
				registryPermission.SetPathList(RegistryPermissionAccess.Write, pathList);
			}
			return registryPermission;
		}

		// Token: 0x06003B5F RID: 15199 RVA: 0x000CBEE4 File Offset: 0x000CA0E4
		public override void FromXml(SecurityElement esd)
		{
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			if (CodeAccessPermission.IsUnrestricted(esd))
			{
				this._state = PermissionState.Unrestricted;
			}
			string text = esd.Attribute("Create");
			if (text != null && text.Length > 0)
			{
				this.SetPathList(RegistryPermissionAccess.Create, text);
			}
			string text2 = esd.Attribute("Read");
			if (text2 != null && text2.Length > 0)
			{
				this.SetPathList(RegistryPermissionAccess.Read, text2);
			}
			string text3 = esd.Attribute("Write");
			if (text3 != null && text3.Length > 0)
			{
				this.SetPathList(RegistryPermissionAccess.Write, text3);
			}
		}

		// Token: 0x06003B60 RID: 15200 RVA: 0x000CBF94 File Offset: 0x000CA194
		public override IPermission Intersect(IPermission target)
		{
			RegistryPermission registryPermission = this.Cast(target);
			if (registryPermission == null)
			{
				return null;
			}
			if (this.IsUnrestricted())
			{
				return registryPermission.Copy();
			}
			if (registryPermission.IsUnrestricted())
			{
				return this.Copy();
			}
			RegistryPermission registryPermission2 = new RegistryPermission(PermissionState.None);
			this.IntersectKeys(this.createList, registryPermission.createList, registryPermission2.createList);
			this.IntersectKeys(this.readList, registryPermission.readList, registryPermission2.readList);
			this.IntersectKeys(this.writeList, registryPermission.writeList, registryPermission2.writeList);
			return (!registryPermission2.IsEmpty()) ? registryPermission2 : null;
		}

		// Token: 0x06003B61 RID: 15201 RVA: 0x000CC038 File Offset: 0x000CA238
		public override bool IsSubsetOf(IPermission target)
		{
			RegistryPermission registryPermission = this.Cast(target);
			if (registryPermission == null)
			{
				return false;
			}
			if (registryPermission.IsEmpty())
			{
				return this.IsEmpty();
			}
			if (this.IsUnrestricted())
			{
				return registryPermission.IsUnrestricted();
			}
			return registryPermission.IsUnrestricted() || (this.KeyIsSubsetOf(this.createList, registryPermission.createList) && this.KeyIsSubsetOf(this.readList, registryPermission.readList) && this.KeyIsSubsetOf(this.writeList, registryPermission.writeList));
		}

		// Token: 0x06003B62 RID: 15202 RVA: 0x000CC0D4 File Offset: 0x000CA2D4
		public bool IsUnrestricted()
		{
			return this._state == PermissionState.Unrestricted;
		}

		// Token: 0x06003B63 RID: 15203 RVA: 0x000CC0E0 File Offset: 0x000CA2E0
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this._state == PermissionState.Unrestricted)
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			else
			{
				string pathList = this.GetPathList(RegistryPermissionAccess.Create);
				if (pathList != null)
				{
					securityElement.AddAttribute("Create", pathList);
				}
				pathList = this.GetPathList(RegistryPermissionAccess.Read);
				if (pathList != null)
				{
					securityElement.AddAttribute("Read", pathList);
				}
				pathList = this.GetPathList(RegistryPermissionAccess.Write);
				if (pathList != null)
				{
					securityElement.AddAttribute("Write", pathList);
				}
			}
			return securityElement;
		}

		// Token: 0x06003B64 RID: 15204 RVA: 0x000CC168 File Offset: 0x000CA368
		public override IPermission Union(IPermission other)
		{
			RegistryPermission registryPermission = this.Cast(other);
			if (registryPermission == null)
			{
				return this.Copy();
			}
			if (this.IsUnrestricted() || registryPermission.IsUnrestricted())
			{
				return new RegistryPermission(PermissionState.Unrestricted);
			}
			if (this.IsEmpty() && registryPermission.IsEmpty())
			{
				return null;
			}
			RegistryPermission registryPermission2 = (RegistryPermission)this.Copy();
			string pathList = registryPermission.GetPathList(RegistryPermissionAccess.Create);
			if (pathList != null)
			{
				registryPermission2.AddPathList(RegistryPermissionAccess.Create, pathList);
			}
			pathList = registryPermission.GetPathList(RegistryPermissionAccess.Read);
			if (pathList != null)
			{
				registryPermission2.AddPathList(RegistryPermissionAccess.Read, pathList);
			}
			pathList = registryPermission.GetPathList(RegistryPermissionAccess.Write);
			if (pathList != null)
			{
				registryPermission2.AddPathList(RegistryPermissionAccess.Write, pathList);
			}
			return registryPermission2;
		}

		// Token: 0x06003B65 RID: 15205 RVA: 0x000CC210 File Offset: 0x000CA410
		private bool IsEmpty()
		{
			return this._state == PermissionState.None && this.createList.Count == 0 && this.readList.Count == 0 && this.writeList.Count == 0;
		}

		// Token: 0x06003B66 RID: 15206 RVA: 0x000CC25C File Offset: 0x000CA45C
		private RegistryPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			RegistryPermission registryPermission = target as RegistryPermission;
			if (registryPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(RegistryPermission));
			}
			return registryPermission;
		}

		// Token: 0x06003B67 RID: 15207 RVA: 0x000CC290 File Offset: 0x000CA490
		internal void ThrowInvalidFlag(RegistryPermissionAccess flag, bool context)
		{
			string text;
			if (context)
			{
				text = Locale.GetText("Unknown flag '{0}'.");
			}
			else
			{
				text = Locale.GetText("Invalid flag '{0}' in this context.");
			}
			throw new ArgumentException(string.Format(text, flag), "flag");
		}

		// Token: 0x06003B68 RID: 15208 RVA: 0x000CC2D8 File Offset: 0x000CA4D8
		private string GetPathList(ArrayList list)
		{
			if (this.IsUnrestricted())
			{
				return string.Empty;
			}
			if (list.Count == 0)
			{
				return string.Empty;
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (object obj in list)
			{
				string value = (string)obj;
				stringBuilder.Append(value);
				stringBuilder.Append(";");
			}
			string text = stringBuilder.ToString();
			int length = text.Length;
			if (length > 0)
			{
				return text.Substring(0, length - 1);
			}
			return string.Empty;
		}

		// Token: 0x06003B69 RID: 15209 RVA: 0x000CC3A4 File Offset: 0x000CA5A4
		internal bool KeyIsSubsetOf(IList local, IList target)
		{
			bool flag = false;
			foreach (object obj in local)
			{
				string text = (string)obj;
				foreach (object obj2 in target)
				{
					string value = (string)obj2;
					if (text.StartsWith(value))
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06003B6A RID: 15210 RVA: 0x000CC48C File Offset: 0x000CA68C
		internal void AddWithUnionKey(IList list, string pathList)
		{
			string[] array = pathList.Split(new char[]
			{
				';'
			});
			foreach (string text in array)
			{
				int count = list.Count;
				if (count == 0)
				{
					list.Add(text);
				}
				else
				{
					for (int j = 0; j < count; j++)
					{
						string text2 = (string)list[j];
						if (text2.StartsWith(text))
						{
							list[j] = text;
						}
						else if (!text.StartsWith(text2))
						{
							list.Add(text);
						}
					}
				}
			}
		}

		// Token: 0x06003B6B RID: 15211 RVA: 0x000CC53C File Offset: 0x000CA73C
		internal void IntersectKeys(IList local, IList target, IList result)
		{
			foreach (object obj in local)
			{
				string text = (string)obj;
				foreach (object obj2 in target)
				{
					string text2 = (string)obj2;
					if (text2.Length > text.Length)
					{
						if (text2.StartsWith(text))
						{
							result.Add(text2);
						}
					}
					else if (text.StartsWith(text2))
					{
						result.Add(text);
					}
				}
			}
		}

		// Token: 0x040019D3 RID: 6611
		private const int version = 1;

		// Token: 0x040019D4 RID: 6612
		private PermissionState _state;

		// Token: 0x040019D5 RID: 6613
		private ArrayList createList;

		// Token: 0x040019D6 RID: 6614
		private ArrayList readList;

		// Token: 0x040019D7 RID: 6615
		private ArrayList writeList;
	}
}
