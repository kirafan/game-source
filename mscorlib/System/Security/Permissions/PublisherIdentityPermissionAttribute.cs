﻿using System;
using System.Runtime.InteropServices;
using System.Security.Cryptography.X509Certificates;
using Mono.Security.Cryptography;

namespace System.Security.Permissions
{
	// Token: 0x02000613 RID: 1555
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class PublisherIdentityPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003B35 RID: 15157 RVA: 0x000CB444 File Offset: 0x000C9644
		public PublisherIdentityPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000B2B RID: 2859
		// (get) Token: 0x06003B36 RID: 15158 RVA: 0x000CB450 File Offset: 0x000C9650
		// (set) Token: 0x06003B37 RID: 15159 RVA: 0x000CB458 File Offset: 0x000C9658
		public string CertFile
		{
			get
			{
				return this.certFile;
			}
			set
			{
				this.certFile = value;
			}
		}

		// Token: 0x17000B2C RID: 2860
		// (get) Token: 0x06003B38 RID: 15160 RVA: 0x000CB464 File Offset: 0x000C9664
		// (set) Token: 0x06003B39 RID: 15161 RVA: 0x000CB46C File Offset: 0x000C966C
		public string SignedFile
		{
			get
			{
				return this.signedFile;
			}
			set
			{
				this.signedFile = value;
			}
		}

		// Token: 0x17000B2D RID: 2861
		// (get) Token: 0x06003B3A RID: 15162 RVA: 0x000CB478 File Offset: 0x000C9678
		// (set) Token: 0x06003B3B RID: 15163 RVA: 0x000CB480 File Offset: 0x000C9680
		public string X509Certificate
		{
			get
			{
				return this.x509data;
			}
			set
			{
				this.x509data = value;
			}
		}

		// Token: 0x06003B3C RID: 15164 RVA: 0x000CB48C File Offset: 0x000C968C
		public override IPermission CreatePermission()
		{
			if (base.Unrestricted)
			{
				return new PublisherIdentityPermission(PermissionState.Unrestricted);
			}
			if (this.x509data != null)
			{
				byte[] data = CryptoConvert.FromHex(this.x509data);
				X509Certificate certificate = new X509Certificate(data);
				return new PublisherIdentityPermission(certificate);
			}
			if (this.certFile != null)
			{
				X509Certificate certificate = System.Security.Cryptography.X509Certificates.X509Certificate.CreateFromCertFile(this.certFile);
				return new PublisherIdentityPermission(certificate);
			}
			if (this.signedFile != null)
			{
				X509Certificate certificate = System.Security.Cryptography.X509Certificates.X509Certificate.CreateFromSignedFile(this.signedFile);
				return new PublisherIdentityPermission(certificate);
			}
			return new PublisherIdentityPermission(PermissionState.None);
		}

		// Token: 0x040019C3 RID: 6595
		private string certFile;

		// Token: 0x040019C4 RID: 6596
		private string signedFile;

		// Token: 0x040019C5 RID: 6597
		private string x509data;
	}
}
