﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200061D RID: 1565
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum SecurityPermissionFlag
	{
		// Token: 0x040019F1 RID: 6641
		NoFlags = 0,
		// Token: 0x040019F2 RID: 6642
		Assertion = 1,
		// Token: 0x040019F3 RID: 6643
		UnmanagedCode = 2,
		// Token: 0x040019F4 RID: 6644
		SkipVerification = 4,
		// Token: 0x040019F5 RID: 6645
		Execution = 8,
		// Token: 0x040019F6 RID: 6646
		ControlThread = 16,
		// Token: 0x040019F7 RID: 6647
		ControlEvidence = 32,
		// Token: 0x040019F8 RID: 6648
		ControlPolicy = 64,
		// Token: 0x040019F9 RID: 6649
		SerializationFormatter = 128,
		// Token: 0x040019FA RID: 6650
		ControlDomainPolicy = 256,
		// Token: 0x040019FB RID: 6651
		ControlPrincipal = 512,
		// Token: 0x040019FC RID: 6652
		ControlAppDomain = 1024,
		// Token: 0x040019FD RID: 6653
		RemotingConfiguration = 2048,
		// Token: 0x040019FE RID: 6654
		Infrastructure = 4096,
		// Token: 0x040019FF RID: 6655
		BindingRedirects = 8192,
		// Token: 0x04001A00 RID: 6656
		AllFlags = 16383
	}
}
