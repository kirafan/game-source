﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200061C RID: 1564
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class SecurityPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003B8A RID: 15242 RVA: 0x000CCA58 File Offset: 0x000CAC58
		public SecurityPermissionAttribute(SecurityAction action) : base(action)
		{
			this.m_Flags = SecurityPermissionFlag.NoFlags;
		}

		// Token: 0x17000B3C RID: 2876
		// (get) Token: 0x06003B8B RID: 15243 RVA: 0x000CCA68 File Offset: 0x000CAC68
		// (set) Token: 0x06003B8C RID: 15244 RVA: 0x000CCA78 File Offset: 0x000CAC78
		public bool Assertion
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.Assertion) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.Assertion;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.Assertion;
				}
			}
		}

		// Token: 0x17000B3D RID: 2877
		// (get) Token: 0x06003B8D RID: 15245 RVA: 0x000CCAB0 File Offset: 0x000CACB0
		// (set) Token: 0x06003B8E RID: 15246 RVA: 0x000CCAC4 File Offset: 0x000CACC4
		public bool BindingRedirects
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.BindingRedirects) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.BindingRedirects;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.BindingRedirects;
				}
			}
		}

		// Token: 0x17000B3E RID: 2878
		// (get) Token: 0x06003B8F RID: 15247 RVA: 0x000CCAF8 File Offset: 0x000CACF8
		// (set) Token: 0x06003B90 RID: 15248 RVA: 0x000CCB0C File Offset: 0x000CAD0C
		public bool ControlAppDomain
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlAppDomain) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlAppDomain;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.ControlAppDomain;
				}
			}
		}

		// Token: 0x17000B3F RID: 2879
		// (get) Token: 0x06003B91 RID: 15249 RVA: 0x000CCB40 File Offset: 0x000CAD40
		// (set) Token: 0x06003B92 RID: 15250 RVA: 0x000CCB54 File Offset: 0x000CAD54
		public bool ControlDomainPolicy
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlDomainPolicy) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlDomainPolicy;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.ControlDomainPolicy;
				}
			}
		}

		// Token: 0x17000B40 RID: 2880
		// (get) Token: 0x06003B93 RID: 15251 RVA: 0x000CCB88 File Offset: 0x000CAD88
		// (set) Token: 0x06003B94 RID: 15252 RVA: 0x000CCB9C File Offset: 0x000CAD9C
		public bool ControlEvidence
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlEvidence) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlEvidence;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.ControlEvidence;
				}
			}
		}

		// Token: 0x17000B41 RID: 2881
		// (get) Token: 0x06003B95 RID: 15253 RVA: 0x000CCBC8 File Offset: 0x000CADC8
		// (set) Token: 0x06003B96 RID: 15254 RVA: 0x000CCBDC File Offset: 0x000CADDC
		public bool ControlPolicy
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlPolicy) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlPolicy;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.ControlPolicy;
				}
			}
		}

		// Token: 0x17000B42 RID: 2882
		// (get) Token: 0x06003B97 RID: 15255 RVA: 0x000CCC08 File Offset: 0x000CAE08
		// (set) Token: 0x06003B98 RID: 15256 RVA: 0x000CCC1C File Offset: 0x000CAE1C
		public bool ControlPrincipal
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlPrincipal) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlPrincipal;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.ControlPrincipal;
				}
			}
		}

		// Token: 0x17000B43 RID: 2883
		// (get) Token: 0x06003B99 RID: 15257 RVA: 0x000CCC50 File Offset: 0x000CAE50
		// (set) Token: 0x06003B9A RID: 15258 RVA: 0x000CCC64 File Offset: 0x000CAE64
		public bool ControlThread
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.ControlThread) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.ControlThread;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.ControlThread;
				}
			}
		}

		// Token: 0x17000B44 RID: 2884
		// (get) Token: 0x06003B9B RID: 15259 RVA: 0x000CCC90 File Offset: 0x000CAE90
		// (set) Token: 0x06003B9C RID: 15260 RVA: 0x000CCCA0 File Offset: 0x000CAEA0
		public bool Execution
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.Execution) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.Execution;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.Execution;
				}
			}
		}

		// Token: 0x17000B45 RID: 2885
		// (get) Token: 0x06003B9D RID: 15261 RVA: 0x000CCCD8 File Offset: 0x000CAED8
		// (set) Token: 0x06003B9E RID: 15262 RVA: 0x000CCCEC File Offset: 0x000CAEEC
		[ComVisible(true)]
		public bool Infrastructure
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.Infrastructure) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.Infrastructure;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.Infrastructure;
				}
			}
		}

		// Token: 0x17000B46 RID: 2886
		// (get) Token: 0x06003B9F RID: 15263 RVA: 0x000CCD20 File Offset: 0x000CAF20
		// (set) Token: 0x06003BA0 RID: 15264 RVA: 0x000CCD34 File Offset: 0x000CAF34
		public bool RemotingConfiguration
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.RemotingConfiguration) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.RemotingConfiguration;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.RemotingConfiguration;
				}
			}
		}

		// Token: 0x17000B47 RID: 2887
		// (get) Token: 0x06003BA1 RID: 15265 RVA: 0x000CCD68 File Offset: 0x000CAF68
		// (set) Token: 0x06003BA2 RID: 15266 RVA: 0x000CCD7C File Offset: 0x000CAF7C
		public bool SerializationFormatter
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.SerializationFormatter) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.SerializationFormatter;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.SerializationFormatter;
				}
			}
		}

		// Token: 0x17000B48 RID: 2888
		// (get) Token: 0x06003BA3 RID: 15267 RVA: 0x000CCDB0 File Offset: 0x000CAFB0
		// (set) Token: 0x06003BA4 RID: 15268 RVA: 0x000CCDC0 File Offset: 0x000CAFC0
		public bool SkipVerification
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.SkipVerification) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.SkipVerification;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.SkipVerification;
				}
			}
		}

		// Token: 0x17000B49 RID: 2889
		// (get) Token: 0x06003BA5 RID: 15269 RVA: 0x000CCDF8 File Offset: 0x000CAFF8
		// (set) Token: 0x06003BA6 RID: 15270 RVA: 0x000CCE08 File Offset: 0x000CB008
		public bool UnmanagedCode
		{
			get
			{
				return (this.m_Flags & SecurityPermissionFlag.UnmanagedCode) != SecurityPermissionFlag.NoFlags;
			}
			set
			{
				if (value)
				{
					this.m_Flags |= SecurityPermissionFlag.UnmanagedCode;
				}
				else
				{
					this.m_Flags &= ~SecurityPermissionFlag.UnmanagedCode;
				}
			}
		}

		// Token: 0x06003BA7 RID: 15271 RVA: 0x000CCE40 File Offset: 0x000CB040
		public override IPermission CreatePermission()
		{
			SecurityPermission result;
			if (base.Unrestricted)
			{
				result = new SecurityPermission(PermissionState.Unrestricted);
			}
			else
			{
				result = new SecurityPermission(this.m_Flags);
			}
			return result;
		}

		// Token: 0x17000B4A RID: 2890
		// (get) Token: 0x06003BA8 RID: 15272 RVA: 0x000CCE74 File Offset: 0x000CB074
		// (set) Token: 0x06003BA9 RID: 15273 RVA: 0x000CCE7C File Offset: 0x000CB07C
		public SecurityPermissionFlag Flags
		{
			get
			{
				return this.m_Flags;
			}
			set
			{
				this.m_Flags = value;
			}
		}

		// Token: 0x040019EF RID: 6639
		private SecurityPermissionFlag m_Flags;
	}
}
