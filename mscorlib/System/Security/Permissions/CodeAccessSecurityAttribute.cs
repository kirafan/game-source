﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005F0 RID: 1520
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public abstract class CodeAccessSecurityAttribute : SecurityAttribute
	{
		// Token: 0x06003A09 RID: 14857 RVA: 0x000C72A0 File Offset: 0x000C54A0
		protected CodeAccessSecurityAttribute(SecurityAction action) : base(action)
		{
		}
	}
}
