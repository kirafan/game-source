﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005FB RID: 1531
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class GacIdentityPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003A7A RID: 14970 RVA: 0x000C8F68 File Offset: 0x000C7168
		public GacIdentityPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x06003A7B RID: 14971 RVA: 0x000C8F74 File Offset: 0x000C7174
		public override IPermission CreatePermission()
		{
			return new GacIdentityPermission();
		}
	}
}
