﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200062A RID: 1578
	[ComVisible(true)]
	[Serializable]
	public sealed class ZoneIdentityPermission : CodeAccessPermission, IBuiltInPermission
	{
		// Token: 0x06003C14 RID: 15380 RVA: 0x000CEB3C File Offset: 0x000CCD3C
		public ZoneIdentityPermission(PermissionState state)
		{
			CodeAccessPermission.CheckPermissionState(state, false);
			this.zone = SecurityZone.NoZone;
		}

		// Token: 0x06003C15 RID: 15381 RVA: 0x000CEB54 File Offset: 0x000CCD54
		public ZoneIdentityPermission(SecurityZone zone)
		{
			this.SecurityZone = zone;
		}

		// Token: 0x06003C16 RID: 15382 RVA: 0x000CEB64 File Offset: 0x000CCD64
		int IBuiltInPermission.GetTokenIndex()
		{
			return 14;
		}

		// Token: 0x06003C17 RID: 15383 RVA: 0x000CEB68 File Offset: 0x000CCD68
		public override IPermission Copy()
		{
			return new ZoneIdentityPermission(this.zone);
		}

		// Token: 0x06003C18 RID: 15384 RVA: 0x000CEB78 File Offset: 0x000CCD78
		public override bool IsSubsetOf(IPermission target)
		{
			ZoneIdentityPermission zoneIdentityPermission = this.Cast(target);
			if (zoneIdentityPermission == null)
			{
				return this.zone == SecurityZone.NoZone;
			}
			return this.zone == SecurityZone.NoZone || this.zone == zoneIdentityPermission.zone;
		}

		// Token: 0x06003C19 RID: 15385 RVA: 0x000CEBBC File Offset: 0x000CCDBC
		public override IPermission Union(IPermission target)
		{
			ZoneIdentityPermission zoneIdentityPermission = this.Cast(target);
			if (zoneIdentityPermission == null)
			{
				IPermission result;
				if (this.zone == SecurityZone.NoZone)
				{
					IPermission permission = null;
					result = permission;
				}
				else
				{
					result = this.Copy();
				}
				return result;
			}
			if (this.zone == zoneIdentityPermission.zone || zoneIdentityPermission.zone == SecurityZone.NoZone)
			{
				return this.Copy();
			}
			if (this.zone == SecurityZone.NoZone)
			{
				return zoneIdentityPermission.Copy();
			}
			throw new ArgumentException(Locale.GetText("Union impossible"));
		}

		// Token: 0x06003C1A RID: 15386 RVA: 0x000CEC38 File Offset: 0x000CCE38
		public override IPermission Intersect(IPermission target)
		{
			ZoneIdentityPermission zoneIdentityPermission = this.Cast(target);
			if (zoneIdentityPermission == null || this.zone == SecurityZone.NoZone)
			{
				return null;
			}
			if (this.zone == zoneIdentityPermission.zone)
			{
				return this.Copy();
			}
			return null;
		}

		// Token: 0x06003C1B RID: 15387 RVA: 0x000CEC7C File Offset: 0x000CCE7C
		public override void FromXml(SecurityElement esd)
		{
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			string text = esd.Attribute("Zone");
			if (text == null)
			{
				this.zone = SecurityZone.NoZone;
			}
			else
			{
				this.zone = (SecurityZone)((int)Enum.Parse(typeof(SecurityZone), text));
			}
		}

		// Token: 0x06003C1C RID: 15388 RVA: 0x000CECD0 File Offset: 0x000CCED0
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this.zone != SecurityZone.NoZone)
			{
				securityElement.AddAttribute("Zone", this.zone.ToString());
			}
			return securityElement;
		}

		// Token: 0x17000B59 RID: 2905
		// (get) Token: 0x06003C1D RID: 15389 RVA: 0x000CED10 File Offset: 0x000CCF10
		// (set) Token: 0x06003C1E RID: 15390 RVA: 0x000CED18 File Offset: 0x000CCF18
		public SecurityZone SecurityZone
		{
			get
			{
				return this.zone;
			}
			set
			{
				if (!Enum.IsDefined(typeof(SecurityZone), value))
				{
					string message = string.Format(Locale.GetText("Invalid enum {0}"), value);
					throw new ArgumentException(message, "SecurityZone");
				}
				this.zone = value;
			}
		}

		// Token: 0x06003C1F RID: 15391 RVA: 0x000CED68 File Offset: 0x000CCF68
		private ZoneIdentityPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			ZoneIdentityPermission zoneIdentityPermission = target as ZoneIdentityPermission;
			if (zoneIdentityPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(ZoneIdentityPermission));
			}
			return zoneIdentityPermission;
		}

		// Token: 0x04001A21 RID: 6689
		private const int version = 1;

		// Token: 0x04001A22 RID: 6690
		private SecurityZone zone;
	}
}
