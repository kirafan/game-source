﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Threading;

namespace System.Security.Permissions
{
	// Token: 0x0200060F RID: 1551
	[ComVisible(true)]
	[Serializable]
	public sealed class PrincipalPermission : IPermission, ISecurityEncodable, IBuiltInPermission, IUnrestrictedPermission
	{
		// Token: 0x06003B0A RID: 15114 RVA: 0x000CA570 File Offset: 0x000C8770
		public PrincipalPermission(PermissionState state)
		{
			this.principals = new ArrayList();
			if (CodeAccessPermission.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				PrincipalPermission.PrincipalInfo value = new PrincipalPermission.PrincipalInfo(null, null, true);
				this.principals.Add(value);
			}
		}

		// Token: 0x06003B0B RID: 15115 RVA: 0x000CA5B4 File Offset: 0x000C87B4
		public PrincipalPermission(string name, string role) : this(name, role, true)
		{
		}

		// Token: 0x06003B0C RID: 15116 RVA: 0x000CA5C0 File Offset: 0x000C87C0
		public PrincipalPermission(string name, string role, bool isAuthenticated)
		{
			this.principals = new ArrayList();
			PrincipalPermission.PrincipalInfo value = new PrincipalPermission.PrincipalInfo(name, role, isAuthenticated);
			this.principals.Add(value);
		}

		// Token: 0x06003B0D RID: 15117 RVA: 0x000CA5F4 File Offset: 0x000C87F4
		internal PrincipalPermission(ArrayList principals)
		{
			this.principals = (ArrayList)principals.Clone();
		}

		// Token: 0x06003B0E RID: 15118 RVA: 0x000CA610 File Offset: 0x000C8810
		int IBuiltInPermission.GetTokenIndex()
		{
			return 8;
		}

		// Token: 0x06003B0F RID: 15119 RVA: 0x000CA614 File Offset: 0x000C8814
		public IPermission Copy()
		{
			return new PrincipalPermission(this.principals);
		}

		// Token: 0x06003B10 RID: 15120 RVA: 0x000CA624 File Offset: 0x000C8824
		public void Demand()
		{
			IPrincipal currentPrincipal = Thread.CurrentPrincipal;
			if (currentPrincipal == null)
			{
				throw new SecurityException("no Principal");
			}
			if (this.principals.Count > 0)
			{
				bool flag = false;
				foreach (object obj in this.principals)
				{
					PrincipalPermission.PrincipalInfo principalInfo = (PrincipalPermission.PrincipalInfo)obj;
					if ((principalInfo.Name == null || principalInfo.Name == currentPrincipal.Identity.Name) && (principalInfo.Role == null || currentPrincipal.IsInRole(principalInfo.Role)) && ((principalInfo.IsAuthenticated && currentPrincipal.Identity.IsAuthenticated) || !principalInfo.IsAuthenticated))
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					throw new SecurityException("Demand for principal refused.");
				}
			}
		}

		// Token: 0x06003B11 RID: 15121 RVA: 0x000CA73C File Offset: 0x000C893C
		public void FromXml(SecurityElement elem)
		{
			this.CheckSecurityElement(elem, "elem", 1, 1);
			this.principals.Clear();
			if (elem.Children != null)
			{
				foreach (object obj in elem.Children)
				{
					SecurityElement securityElement = (SecurityElement)obj;
					if (securityElement.Tag != "Identity")
					{
						throw new ArgumentException("not IPermission/Identity");
					}
					string name = securityElement.Attribute("ID");
					string role = securityElement.Attribute("Role");
					string text = securityElement.Attribute("Authenticated");
					bool isAuthenticated = false;
					if (text != null)
					{
						try
						{
							isAuthenticated = bool.Parse(text);
						}
						catch
						{
						}
					}
					PrincipalPermission.PrincipalInfo value = new PrincipalPermission.PrincipalInfo(name, role, isAuthenticated);
					this.principals.Add(value);
				}
			}
		}

		// Token: 0x06003B12 RID: 15122 RVA: 0x000CA860 File Offset: 0x000C8A60
		public IPermission Intersect(IPermission target)
		{
			PrincipalPermission principalPermission = this.Cast(target);
			if (principalPermission == null)
			{
				return null;
			}
			if (this.IsUnrestricted())
			{
				return principalPermission.Copy();
			}
			if (principalPermission.IsUnrestricted())
			{
				return this.Copy();
			}
			PrincipalPermission principalPermission2 = new PrincipalPermission(PermissionState.None);
			foreach (object obj in this.principals)
			{
				PrincipalPermission.PrincipalInfo principalInfo = (PrincipalPermission.PrincipalInfo)obj;
				foreach (object obj2 in principalPermission.principals)
				{
					PrincipalPermission.PrincipalInfo principalInfo2 = (PrincipalPermission.PrincipalInfo)obj2;
					if (principalInfo.IsAuthenticated == principalInfo2.IsAuthenticated)
					{
						string text = null;
						if (principalInfo.Name == principalInfo2.Name || principalInfo2.Name == null)
						{
							text = principalInfo.Name;
						}
						else if (principalInfo.Name == null)
						{
							text = principalInfo2.Name;
						}
						string text2 = null;
						if (principalInfo.Role == principalInfo2.Role || principalInfo2.Role == null)
						{
							text2 = principalInfo.Role;
						}
						else if (principalInfo.Role == null)
						{
							text2 = principalInfo2.Role;
						}
						if (text != null || text2 != null)
						{
							PrincipalPermission.PrincipalInfo value = new PrincipalPermission.PrincipalInfo(text, text2, principalInfo.IsAuthenticated);
							principalPermission2.principals.Add(value);
						}
					}
				}
			}
			return (principalPermission2.principals.Count <= 0) ? null : principalPermission2;
		}

		// Token: 0x06003B13 RID: 15123 RVA: 0x000CAA4C File Offset: 0x000C8C4C
		public bool IsSubsetOf(IPermission target)
		{
			PrincipalPermission principalPermission = this.Cast(target);
			if (principalPermission == null)
			{
				return this.IsEmpty();
			}
			if (this.IsUnrestricted())
			{
				return principalPermission.IsUnrestricted();
			}
			if (principalPermission.IsUnrestricted())
			{
				return true;
			}
			foreach (object obj in this.principals)
			{
				PrincipalPermission.PrincipalInfo principalInfo = (PrincipalPermission.PrincipalInfo)obj;
				bool flag = false;
				foreach (object obj2 in principalPermission.principals)
				{
					PrincipalPermission.PrincipalInfo principalInfo2 = (PrincipalPermission.PrincipalInfo)obj2;
					if ((principalInfo.Name == principalInfo2.Name || principalInfo2.Name == null) && (principalInfo.Role == principalInfo2.Role || principalInfo2.Role == null) && principalInfo.IsAuthenticated == principalInfo2.IsAuthenticated)
					{
						flag = true;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06003B14 RID: 15124 RVA: 0x000CABB8 File Offset: 0x000C8DB8
		public bool IsUnrestricted()
		{
			foreach (object obj in this.principals)
			{
				PrincipalPermission.PrincipalInfo principalInfo = (PrincipalPermission.PrincipalInfo)obj;
				if (principalInfo.Name == null && principalInfo.Role == null && principalInfo.IsAuthenticated)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003B15 RID: 15125 RVA: 0x000CAC4C File Offset: 0x000C8E4C
		public override string ToString()
		{
			return this.ToXml().ToString();
		}

		// Token: 0x06003B16 RID: 15126 RVA: 0x000CAC5C File Offset: 0x000C8E5C
		public SecurityElement ToXml()
		{
			SecurityElement securityElement = new SecurityElement("Permission");
			Type type = base.GetType();
			securityElement.AddAttribute("class", type.FullName + ", " + type.Assembly.ToString().Replace('"', '\''));
			securityElement.AddAttribute("version", 1.ToString());
			foreach (object obj in this.principals)
			{
				PrincipalPermission.PrincipalInfo principalInfo = (PrincipalPermission.PrincipalInfo)obj;
				SecurityElement securityElement2 = new SecurityElement("Identity");
				if (principalInfo.Name != null)
				{
					securityElement2.AddAttribute("ID", principalInfo.Name);
				}
				if (principalInfo.Role != null)
				{
					securityElement2.AddAttribute("Role", principalInfo.Role);
				}
				if (principalInfo.IsAuthenticated)
				{
					securityElement2.AddAttribute("Authenticated", "true");
				}
				securityElement.AddChild(securityElement2);
			}
			return securityElement;
		}

		// Token: 0x06003B17 RID: 15127 RVA: 0x000CAD88 File Offset: 0x000C8F88
		public IPermission Union(IPermission other)
		{
			PrincipalPermission principalPermission = this.Cast(other);
			if (principalPermission == null)
			{
				return this.Copy();
			}
			if (this.IsUnrestricted() || principalPermission.IsUnrestricted())
			{
				return new PrincipalPermission(PermissionState.Unrestricted);
			}
			PrincipalPermission principalPermission2 = new PrincipalPermission(this.principals);
			foreach (object obj in principalPermission.principals)
			{
				PrincipalPermission.PrincipalInfo value = (PrincipalPermission.PrincipalInfo)obj;
				principalPermission2.principals.Add(value);
			}
			return principalPermission2;
		}

		// Token: 0x06003B18 RID: 15128 RVA: 0x000CAE40 File Offset: 0x000C9040
		[ComVisible(false)]
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			PrincipalPermission principalPermission = obj as PrincipalPermission;
			if (principalPermission == null)
			{
				return false;
			}
			if (this.principals.Count != principalPermission.principals.Count)
			{
				return false;
			}
			foreach (object obj2 in this.principals)
			{
				PrincipalPermission.PrincipalInfo principalInfo = (PrincipalPermission.PrincipalInfo)obj2;
				bool flag = false;
				foreach (object obj3 in principalPermission.principals)
				{
					PrincipalPermission.PrincipalInfo principalInfo2 = (PrincipalPermission.PrincipalInfo)obj3;
					if ((principalInfo.Name == principalInfo2.Name || principalInfo2.Name == null) && (principalInfo.Role == principalInfo2.Role || principalInfo2.Role == null) && principalInfo.IsAuthenticated == principalInfo2.IsAuthenticated)
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06003B19 RID: 15129 RVA: 0x000CAFB4 File Offset: 0x000C91B4
		[ComVisible(false)]
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06003B1A RID: 15130 RVA: 0x000CAFBC File Offset: 0x000C91BC
		private PrincipalPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			PrincipalPermission principalPermission = target as PrincipalPermission;
			if (principalPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(PrincipalPermission));
			}
			return principalPermission;
		}

		// Token: 0x06003B1B RID: 15131 RVA: 0x000CAFF0 File Offset: 0x000C91F0
		private bool IsEmpty()
		{
			return this.principals.Count == 0;
		}

		// Token: 0x06003B1C RID: 15132 RVA: 0x000CB000 File Offset: 0x000C9200
		internal int CheckSecurityElement(SecurityElement se, string parameterName, int minimumVersion, int maximumVersion)
		{
			if (se == null)
			{
				throw new ArgumentNullException(parameterName);
			}
			if (se.Tag != "Permission")
			{
				string message = string.Format(Locale.GetText("Invalid tag {0}"), se.Tag);
				throw new ArgumentException(message, parameterName);
			}
			int num = minimumVersion;
			string text = se.Attribute("version");
			if (text != null)
			{
				try
				{
					num = int.Parse(text);
				}
				catch (Exception innerException)
				{
					string text2 = Locale.GetText("Couldn't parse version from '{0}'.");
					text2 = string.Format(text2, text);
					throw new ArgumentException(text2, parameterName, innerException);
				}
			}
			if (num < minimumVersion || num > maximumVersion)
			{
				string text3 = Locale.GetText("Unknown version '{0}', expected versions between ['{1}','{2}'].");
				text3 = string.Format(text3, num, minimumVersion, maximumVersion);
				throw new ArgumentException(text3, parameterName);
			}
			return num;
		}

		// Token: 0x040019B9 RID: 6585
		private const int version = 1;

		// Token: 0x040019BA RID: 6586
		private ArrayList principals;

		// Token: 0x02000610 RID: 1552
		internal class PrincipalInfo
		{
			// Token: 0x06003B1D RID: 15133 RVA: 0x000CB0F0 File Offset: 0x000C92F0
			public PrincipalInfo(string name, string role, bool isAuthenticated)
			{
				this._name = name;
				this._role = role;
				this._isAuthenticated = isAuthenticated;
			}

			// Token: 0x17000B24 RID: 2852
			// (get) Token: 0x06003B1E RID: 15134 RVA: 0x000CB110 File Offset: 0x000C9310
			public string Name
			{
				get
				{
					return this._name;
				}
			}

			// Token: 0x17000B25 RID: 2853
			// (get) Token: 0x06003B1F RID: 15135 RVA: 0x000CB118 File Offset: 0x000C9318
			public string Role
			{
				get
				{
					return this._role;
				}
			}

			// Token: 0x17000B26 RID: 2854
			// (get) Token: 0x06003B20 RID: 15136 RVA: 0x000CB120 File Offset: 0x000C9320
			public bool IsAuthenticated
			{
				get
				{
					return this._isAuthenticated;
				}
			}

			// Token: 0x040019BB RID: 6587
			private string _name;

			// Token: 0x040019BC RID: 6588
			private string _role;

			// Token: 0x040019BD RID: 6589
			private bool _isAuthenticated;
		}
	}
}
