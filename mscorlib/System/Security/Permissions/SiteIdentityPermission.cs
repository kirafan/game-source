﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200061E RID: 1566
	[ComVisible(true)]
	[Serializable]
	public sealed class SiteIdentityPermission : CodeAccessPermission, IBuiltInPermission
	{
		// Token: 0x06003BAA RID: 15274 RVA: 0x000CCE88 File Offset: 0x000CB088
		public SiteIdentityPermission(PermissionState state)
		{
			CodeAccessPermission.CheckPermissionState(state, false);
		}

		// Token: 0x06003BAB RID: 15275 RVA: 0x000CCE98 File Offset: 0x000CB098
		public SiteIdentityPermission(string site)
		{
			this.Site = site;
		}

		// Token: 0x06003BAD RID: 15277 RVA: 0x000CCEC4 File Offset: 0x000CB0C4
		int IBuiltInPermission.GetTokenIndex()
		{
			return 11;
		}

		// Token: 0x17000B4B RID: 2891
		// (get) Token: 0x06003BAE RID: 15278 RVA: 0x000CCEC8 File Offset: 0x000CB0C8
		// (set) Token: 0x06003BAF RID: 15279 RVA: 0x000CCEE8 File Offset: 0x000CB0E8
		public string Site
		{
			get
			{
				if (this.IsEmpty())
				{
					throw new NullReferenceException("No site.");
				}
				return this._site;
			}
			set
			{
				if (!this.IsValid(value))
				{
					throw new ArgumentException("Invalid site.");
				}
				this._site = value;
			}
		}

		// Token: 0x06003BB0 RID: 15280 RVA: 0x000CCF08 File Offset: 0x000CB108
		public override IPermission Copy()
		{
			if (this.IsEmpty())
			{
				return new SiteIdentityPermission(PermissionState.None);
			}
			return new SiteIdentityPermission(this._site);
		}

		// Token: 0x06003BB1 RID: 15281 RVA: 0x000CCF28 File Offset: 0x000CB128
		public override void FromXml(SecurityElement esd)
		{
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			string text = esd.Attribute("Site");
			if (text != null)
			{
				this.Site = text;
			}
		}

		// Token: 0x06003BB2 RID: 15282 RVA: 0x000CCF5C File Offset: 0x000CB15C
		public override IPermission Intersect(IPermission target)
		{
			SiteIdentityPermission siteIdentityPermission = this.Cast(target);
			if (siteIdentityPermission == null || this.IsEmpty())
			{
				return null;
			}
			if (this.Match(siteIdentityPermission._site))
			{
				string site = (this._site.Length <= siteIdentityPermission._site.Length) ? siteIdentityPermission._site : this._site;
				return new SiteIdentityPermission(site);
			}
			return null;
		}

		// Token: 0x06003BB3 RID: 15283 RVA: 0x000CCFCC File Offset: 0x000CB1CC
		public override bool IsSubsetOf(IPermission target)
		{
			SiteIdentityPermission siteIdentityPermission = this.Cast(target);
			if (siteIdentityPermission == null)
			{
				return this.IsEmpty();
			}
			if (this._site == null && siteIdentityPermission._site == null)
			{
				return true;
			}
			if (this._site == null || siteIdentityPermission._site == null)
			{
				return false;
			}
			int num = siteIdentityPermission._site.IndexOf('*');
			if (num == -1)
			{
				return this._site == siteIdentityPermission._site;
			}
			return this._site.EndsWith(siteIdentityPermission._site.Substring(num + 1));
		}

		// Token: 0x06003BB4 RID: 15284 RVA: 0x000CD060 File Offset: 0x000CB260
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this._site != null)
			{
				securityElement.AddAttribute("Site", this._site);
			}
			return securityElement;
		}

		// Token: 0x06003BB5 RID: 15285 RVA: 0x000CD094 File Offset: 0x000CB294
		public override IPermission Union(IPermission target)
		{
			SiteIdentityPermission siteIdentityPermission = this.Cast(target);
			if (siteIdentityPermission == null || siteIdentityPermission.IsEmpty())
			{
				return this.Copy();
			}
			if (this.IsEmpty())
			{
				return siteIdentityPermission.Copy();
			}
			if (this.Match(siteIdentityPermission._site))
			{
				string site = (this._site.Length >= siteIdentityPermission._site.Length) ? siteIdentityPermission._site : this._site;
				return new SiteIdentityPermission(site);
			}
			throw new ArgumentException(Locale.GetText("Cannot union two different sites."), "target");
		}

		// Token: 0x06003BB6 RID: 15286 RVA: 0x000CD12C File Offset: 0x000CB32C
		private bool IsEmpty()
		{
			return this._site == null;
		}

		// Token: 0x06003BB7 RID: 15287 RVA: 0x000CD138 File Offset: 0x000CB338
		private SiteIdentityPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			SiteIdentityPermission siteIdentityPermission = target as SiteIdentityPermission;
			if (siteIdentityPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(SiteIdentityPermission));
			}
			return siteIdentityPermission;
		}

		// Token: 0x06003BB8 RID: 15288 RVA: 0x000CD16C File Offset: 0x000CB36C
		private bool IsValid(string s)
		{
			if (s == null || s.Length == 0)
			{
				return false;
			}
			for (int i = 0; i < s.Length; i++)
			{
				ushort num = (ushort)s[i];
				if (num < 33 || num > 126)
				{
					return false;
				}
				if (num == 42 && s.Length > 1 && (s[i + 1] != '.' || i > 0))
				{
					return false;
				}
				if (!SiteIdentityPermission.valid[(int)(num - 33)])
				{
					return false;
				}
			}
			return s.Length != 1 || s[0] != '.';
		}

		// Token: 0x06003BB9 RID: 15289 RVA: 0x000CD218 File Offset: 0x000CB418
		private bool Match(string target)
		{
			if (this._site == null || target == null)
			{
				return false;
			}
			int num = this._site.IndexOf('*');
			int num2 = target.IndexOf('*');
			if (num == -1 && num2 == -1)
			{
				return this._site == target;
			}
			if (num == -1)
			{
				return this._site.EndsWith(target.Substring(num2 + 1));
			}
			if (num2 == -1)
			{
				return target.EndsWith(this._site.Substring(num + 1));
			}
			string text = this._site.Substring(num + 1);
			target = target.Substring(num2 + 1);
			if (text.Length > target.Length)
			{
				return text.EndsWith(target);
			}
			return target.EndsWith(text);
		}

		// Token: 0x04001A01 RID: 6657
		private const int version = 1;

		// Token: 0x04001A02 RID: 6658
		private string _site;

		// Token: 0x04001A03 RID: 6659
		private static bool[] valid = new bool[]
		{
			true,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			true,
			true,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			false,
			false,
			false,
			false,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			false,
			true,
			true,
			false,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			true,
			false,
			true,
			true
		};
	}
}
