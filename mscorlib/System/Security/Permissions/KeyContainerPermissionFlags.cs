﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200060C RID: 1548
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum KeyContainerPermissionFlags
	{
		// Token: 0x040019A6 RID: 6566
		NoFlags = 0,
		// Token: 0x040019A7 RID: 6567
		Create = 1,
		// Token: 0x040019A8 RID: 6568
		Open = 2,
		// Token: 0x040019A9 RID: 6569
		Delete = 4,
		// Token: 0x040019AA RID: 6570
		Import = 16,
		// Token: 0x040019AB RID: 6571
		Export = 32,
		// Token: 0x040019AC RID: 6572
		Sign = 256,
		// Token: 0x040019AD RID: 6573
		Decrypt = 512,
		// Token: 0x040019AE RID: 6574
		ViewAcl = 4096,
		// Token: 0x040019AF RID: 6575
		ChangeAcl = 8192,
		// Token: 0x040019B0 RID: 6576
		AllFlags = 13111
	}
}
