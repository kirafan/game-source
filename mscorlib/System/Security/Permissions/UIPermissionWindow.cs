﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000627 RID: 1575
	[ComVisible(true)]
	[Serializable]
	public enum UIPermissionWindow
	{
		// Token: 0x04001A1A RID: 6682
		NoWindows,
		// Token: 0x04001A1B RID: 6683
		SafeSubWindows,
		// Token: 0x04001A1C RID: 6684
		SafeTopLevelWindows,
		// Token: 0x04001A1D RID: 6685
		AllWindows
	}
}
