﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.AccessControl;

namespace System.Security.Permissions
{
	// Token: 0x020005F7 RID: 1527
	[ComVisible(true)]
	[Serializable]
	public sealed class FileIOPermission : CodeAccessPermission, IBuiltInPermission, IUnrestrictedPermission
	{
		// Token: 0x06003A36 RID: 14902 RVA: 0x000C7FAC File Offset: 0x000C61AC
		public FileIOPermission(PermissionState state)
		{
			if (CodeAccessPermission.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this.m_Unrestricted = true;
				this.m_AllFilesAccess = FileIOPermissionAccess.AllAccess;
				this.m_AllLocalFilesAccess = FileIOPermissionAccess.AllAccess;
			}
			this.CreateLists();
		}

		// Token: 0x06003A37 RID: 14903 RVA: 0x000C7FEC File Offset: 0x000C61EC
		public FileIOPermission(FileIOPermissionAccess access, string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			this.CreateLists();
			this.AddPathList(access, path);
		}

		// Token: 0x06003A38 RID: 14904 RVA: 0x000C8020 File Offset: 0x000C6220
		public FileIOPermission(FileIOPermissionAccess access, string[] pathList)
		{
			if (pathList == null)
			{
				throw new ArgumentNullException("pathList");
			}
			this.CreateLists();
			this.AddPathList(access, pathList);
		}

		// Token: 0x06003A39 RID: 14905 RVA: 0x000C8054 File Offset: 0x000C6254
		[MonoTODO("(2.0) Access Control isn't implemented")]
		public FileIOPermission(FileIOPermissionAccess access, AccessControlActions control, string path)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003A3A RID: 14906 RVA: 0x000C8064 File Offset: 0x000C6264
		[MonoTODO("(2.0) Access Control isn't implemented")]
		public FileIOPermission(FileIOPermissionAccess access, AccessControlActions control, string[] pathList)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003A3C RID: 14908 RVA: 0x000C808C File Offset: 0x000C628C
		int IBuiltInPermission.GetTokenIndex()
		{
			return 2;
		}

		// Token: 0x06003A3D RID: 14909 RVA: 0x000C8090 File Offset: 0x000C6290
		internal void CreateLists()
		{
			this.readList = new ArrayList();
			this.writeList = new ArrayList();
			this.appendList = new ArrayList();
			this.pathList = new ArrayList();
		}

		// Token: 0x17000AF0 RID: 2800
		// (get) Token: 0x06003A3E RID: 14910 RVA: 0x000C80CC File Offset: 0x000C62CC
		// (set) Token: 0x06003A3F RID: 14911 RVA: 0x000C80D4 File Offset: 0x000C62D4
		public FileIOPermissionAccess AllFiles
		{
			get
			{
				return this.m_AllFilesAccess;
			}
			set
			{
				if (!this.m_Unrestricted)
				{
					this.m_AllFilesAccess = value;
				}
			}
		}

		// Token: 0x17000AF1 RID: 2801
		// (get) Token: 0x06003A40 RID: 14912 RVA: 0x000C80E8 File Offset: 0x000C62E8
		// (set) Token: 0x06003A41 RID: 14913 RVA: 0x000C80F0 File Offset: 0x000C62F0
		public FileIOPermissionAccess AllLocalFiles
		{
			get
			{
				return this.m_AllLocalFilesAccess;
			}
			set
			{
				if (!this.m_Unrestricted)
				{
					this.m_AllLocalFilesAccess = value;
				}
			}
		}

		// Token: 0x06003A42 RID: 14914 RVA: 0x000C8104 File Offset: 0x000C6304
		public void AddPathList(FileIOPermissionAccess access, string path)
		{
			if ((FileIOPermissionAccess.AllAccess & access) != access)
			{
				FileIOPermission.ThrowInvalidFlag(access, true);
			}
			FileIOPermission.ThrowIfInvalidPath(path);
			this.AddPathInternal(access, path);
		}

		// Token: 0x06003A43 RID: 14915 RVA: 0x000C8128 File Offset: 0x000C6328
		public void AddPathList(FileIOPermissionAccess access, string[] pathList)
		{
			if ((FileIOPermissionAccess.AllAccess & access) != access)
			{
				FileIOPermission.ThrowInvalidFlag(access, true);
			}
			FileIOPermission.ThrowIfInvalidPath(pathList);
			foreach (string path in pathList)
			{
				this.AddPathInternal(access, path);
			}
		}

		// Token: 0x06003A44 RID: 14916 RVA: 0x000C8170 File Offset: 0x000C6370
		internal void AddPathInternal(FileIOPermissionAccess access, string path)
		{
			path = Path.InsecureGetFullPath(path);
			if ((access & FileIOPermissionAccess.Read) == FileIOPermissionAccess.Read)
			{
				this.readList.Add(path);
			}
			if ((access & FileIOPermissionAccess.Write) == FileIOPermissionAccess.Write)
			{
				this.writeList.Add(path);
			}
			if ((access & FileIOPermissionAccess.Append) == FileIOPermissionAccess.Append)
			{
				this.appendList.Add(path);
			}
			if ((access & FileIOPermissionAccess.PathDiscovery) == FileIOPermissionAccess.PathDiscovery)
			{
				this.pathList.Add(path);
			}
		}

		// Token: 0x06003A45 RID: 14917 RVA: 0x000C81E0 File Offset: 0x000C63E0
		public override IPermission Copy()
		{
			if (this.m_Unrestricted)
			{
				return new FileIOPermission(PermissionState.Unrestricted);
			}
			return new FileIOPermission(PermissionState.None)
			{
				readList = (ArrayList)this.readList.Clone(),
				writeList = (ArrayList)this.writeList.Clone(),
				appendList = (ArrayList)this.appendList.Clone(),
				pathList = (ArrayList)this.pathList.Clone(),
				m_AllFilesAccess = this.m_AllFilesAccess,
				m_AllLocalFilesAccess = this.m_AllLocalFilesAccess
			};
		}

		// Token: 0x06003A46 RID: 14918 RVA: 0x000C8278 File Offset: 0x000C6478
		public override void FromXml(SecurityElement esd)
		{
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			if (CodeAccessPermission.IsUnrestricted(esd))
			{
				this.m_Unrestricted = true;
			}
			else
			{
				this.m_Unrestricted = false;
				string text = esd.Attribute("Read");
				if (text != null)
				{
					string[] array = text.Split(new char[]
					{
						';'
					});
					this.AddPathList(FileIOPermissionAccess.Read, array);
				}
				text = esd.Attribute("Write");
				if (text != null)
				{
					string[] array = text.Split(new char[]
					{
						';'
					});
					this.AddPathList(FileIOPermissionAccess.Write, array);
				}
				text = esd.Attribute("Append");
				if (text != null)
				{
					string[] array = text.Split(new char[]
					{
						';'
					});
					this.AddPathList(FileIOPermissionAccess.Append, array);
				}
				text = esd.Attribute("PathDiscovery");
				if (text != null)
				{
					string[] array = text.Split(new char[]
					{
						';'
					});
					this.AddPathList(FileIOPermissionAccess.PathDiscovery, array);
				}
			}
		}

		// Token: 0x06003A47 RID: 14919 RVA: 0x000C8364 File Offset: 0x000C6564
		public string[] GetPathList(FileIOPermissionAccess access)
		{
			if ((FileIOPermissionAccess.AllAccess & access) != access)
			{
				FileIOPermission.ThrowInvalidFlag(access, true);
			}
			ArrayList arrayList = new ArrayList();
			switch (access)
			{
			case FileIOPermissionAccess.NoAccess:
				goto IL_9D;
			case FileIOPermissionAccess.Read:
				arrayList.AddRange(this.readList);
				goto IL_9D;
			case FileIOPermissionAccess.Write:
				arrayList.AddRange(this.writeList);
				goto IL_9D;
			case FileIOPermissionAccess.Append:
				arrayList.AddRange(this.appendList);
				goto IL_9D;
			case FileIOPermissionAccess.PathDiscovery:
				arrayList.AddRange(this.pathList);
				goto IL_9D;
			}
			FileIOPermission.ThrowInvalidFlag(access, false);
			IL_9D:
			return (arrayList.Count <= 0) ? null : ((string[])arrayList.ToArray(typeof(string)));
		}

		// Token: 0x06003A48 RID: 14920 RVA: 0x000C8438 File Offset: 0x000C6638
		public override IPermission Intersect(IPermission target)
		{
			FileIOPermission fileIOPermission = FileIOPermission.Cast(target);
			if (fileIOPermission == null)
			{
				return null;
			}
			if (this.IsUnrestricted())
			{
				return fileIOPermission.Copy();
			}
			if (fileIOPermission.IsUnrestricted())
			{
				return this.Copy();
			}
			FileIOPermission fileIOPermission2 = new FileIOPermission(PermissionState.None);
			fileIOPermission2.AllFiles = (this.m_AllFilesAccess & fileIOPermission.AllFiles);
			fileIOPermission2.AllLocalFiles = (this.m_AllLocalFilesAccess & fileIOPermission.AllLocalFiles);
			FileIOPermission.IntersectKeys(this.readList, fileIOPermission.readList, fileIOPermission2.readList);
			FileIOPermission.IntersectKeys(this.writeList, fileIOPermission.writeList, fileIOPermission2.writeList);
			FileIOPermission.IntersectKeys(this.appendList, fileIOPermission.appendList, fileIOPermission2.appendList);
			FileIOPermission.IntersectKeys(this.pathList, fileIOPermission.pathList, fileIOPermission2.pathList);
			return (!fileIOPermission2.IsEmpty()) ? fileIOPermission2 : null;
		}

		// Token: 0x06003A49 RID: 14921 RVA: 0x000C8514 File Offset: 0x000C6714
		public override bool IsSubsetOf(IPermission target)
		{
			FileIOPermission fileIOPermission = FileIOPermission.Cast(target);
			if (fileIOPermission == null)
			{
				return false;
			}
			if (fileIOPermission.IsEmpty())
			{
				return this.IsEmpty();
			}
			if (this.IsUnrestricted())
			{
				return fileIOPermission.IsUnrestricted();
			}
			return fileIOPermission.IsUnrestricted() || ((this.m_AllFilesAccess & fileIOPermission.AllFiles) == this.m_AllFilesAccess && (this.m_AllLocalFilesAccess & fileIOPermission.AllLocalFiles) == this.m_AllLocalFilesAccess && FileIOPermission.KeyIsSubsetOf(this.appendList, fileIOPermission.appendList) && FileIOPermission.KeyIsSubsetOf(this.readList, fileIOPermission.readList) && FileIOPermission.KeyIsSubsetOf(this.writeList, fileIOPermission.writeList) && FileIOPermission.KeyIsSubsetOf(this.pathList, fileIOPermission.pathList));
		}

		// Token: 0x06003A4A RID: 14922 RVA: 0x000C85F8 File Offset: 0x000C67F8
		public bool IsUnrestricted()
		{
			return this.m_Unrestricted;
		}

		// Token: 0x06003A4B RID: 14923 RVA: 0x000C8600 File Offset: 0x000C6800
		public void SetPathList(FileIOPermissionAccess access, string path)
		{
			if ((FileIOPermissionAccess.AllAccess & access) != access)
			{
				FileIOPermission.ThrowInvalidFlag(access, true);
			}
			FileIOPermission.ThrowIfInvalidPath(path);
			this.Clear(access);
			this.AddPathInternal(access, path);
		}

		// Token: 0x06003A4C RID: 14924 RVA: 0x000C8634 File Offset: 0x000C6834
		public void SetPathList(FileIOPermissionAccess access, string[] pathList)
		{
			if ((FileIOPermissionAccess.AllAccess & access) != access)
			{
				FileIOPermission.ThrowInvalidFlag(access, true);
			}
			FileIOPermission.ThrowIfInvalidPath(pathList);
			this.Clear(access);
			foreach (string path in pathList)
			{
				this.AddPathInternal(access, path);
			}
		}

		// Token: 0x06003A4D RID: 14925 RVA: 0x000C8684 File Offset: 0x000C6884
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this.m_Unrestricted)
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			else
			{
				string[] array = this.GetPathList(FileIOPermissionAccess.Append);
				if (array != null && array.Length > 0)
				{
					securityElement.AddAttribute("Append", string.Join(";", array));
				}
				array = this.GetPathList(FileIOPermissionAccess.Read);
				if (array != null && array.Length > 0)
				{
					securityElement.AddAttribute("Read", string.Join(";", array));
				}
				array = this.GetPathList(FileIOPermissionAccess.Write);
				if (array != null && array.Length > 0)
				{
					securityElement.AddAttribute("Write", string.Join(";", array));
				}
				array = this.GetPathList(FileIOPermissionAccess.PathDiscovery);
				if (array != null && array.Length > 0)
				{
					securityElement.AddAttribute("PathDiscovery", string.Join(";", array));
				}
			}
			return securityElement;
		}

		// Token: 0x06003A4E RID: 14926 RVA: 0x000C8770 File Offset: 0x000C6970
		public override IPermission Union(IPermission other)
		{
			FileIOPermission fileIOPermission = FileIOPermission.Cast(other);
			if (fileIOPermission == null)
			{
				return this.Copy();
			}
			if (this.IsUnrestricted() || fileIOPermission.IsUnrestricted())
			{
				return new FileIOPermission(PermissionState.Unrestricted);
			}
			if (this.IsEmpty() && fileIOPermission.IsEmpty())
			{
				return null;
			}
			FileIOPermission fileIOPermission2 = (FileIOPermission)this.Copy();
			fileIOPermission2.AllFiles |= fileIOPermission.AllFiles;
			fileIOPermission2.AllLocalFiles |= fileIOPermission.AllLocalFiles;
			string[] array = fileIOPermission.GetPathList(FileIOPermissionAccess.Read);
			if (array != null)
			{
				FileIOPermission.UnionKeys(fileIOPermission2.readList, array);
			}
			array = fileIOPermission.GetPathList(FileIOPermissionAccess.Write);
			if (array != null)
			{
				FileIOPermission.UnionKeys(fileIOPermission2.writeList, array);
			}
			array = fileIOPermission.GetPathList(FileIOPermissionAccess.Append);
			if (array != null)
			{
				FileIOPermission.UnionKeys(fileIOPermission2.appendList, array);
			}
			array = fileIOPermission.GetPathList(FileIOPermissionAccess.PathDiscovery);
			if (array != null)
			{
				FileIOPermission.UnionKeys(fileIOPermission2.pathList, array);
			}
			return fileIOPermission2;
		}

		// Token: 0x06003A4F RID: 14927 RVA: 0x000C8864 File Offset: 0x000C6A64
		[MonoTODO("(2.0)")]
		[ComVisible(false)]
		public override bool Equals(object obj)
		{
			return false;
		}

		// Token: 0x06003A50 RID: 14928 RVA: 0x000C8868 File Offset: 0x000C6A68
		[MonoTODO("(2.0)")]
		[ComVisible(false)]
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06003A51 RID: 14929 RVA: 0x000C8870 File Offset: 0x000C6A70
		private bool IsEmpty()
		{
			return !this.m_Unrestricted && this.appendList.Count == 0 && this.readList.Count == 0 && this.writeList.Count == 0 && this.pathList.Count == 0;
		}

		// Token: 0x06003A52 RID: 14930 RVA: 0x000C88CC File Offset: 0x000C6ACC
		private static FileIOPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			FileIOPermission fileIOPermission = target as FileIOPermission;
			if (fileIOPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(FileIOPermission));
			}
			return fileIOPermission;
		}

		// Token: 0x06003A53 RID: 14931 RVA: 0x000C8900 File Offset: 0x000C6B00
		internal static void ThrowInvalidFlag(FileIOPermissionAccess access, bool context)
		{
			string text;
			if (context)
			{
				text = Locale.GetText("Unknown flag '{0}'.");
			}
			else
			{
				text = Locale.GetText("Invalid flag '{0}' in this context.");
			}
			throw new ArgumentException(string.Format(text, access), "access");
		}

		// Token: 0x06003A54 RID: 14932 RVA: 0x000C8948 File Offset: 0x000C6B48
		internal static void ThrowIfInvalidPath(string path)
		{
			string directoryName = Path.GetDirectoryName(path);
			if (directoryName != null && directoryName.LastIndexOfAny(FileIOPermission.BadPathNameCharacters) >= 0)
			{
				string message = string.Format(Locale.GetText("Invalid path characters in path: '{0}'"), path);
				throw new ArgumentException(message, "path");
			}
			string fileName = Path.GetFileName(path);
			if (fileName != null && fileName.LastIndexOfAny(FileIOPermission.BadFileNameCharacters) >= 0)
			{
				string message2 = string.Format(Locale.GetText("Invalid filename characters in path: '{0}'"), path);
				throw new ArgumentException(message2, "path");
			}
			if (!Path.IsPathRooted(path))
			{
				string text = Locale.GetText("Absolute path information is required.");
				throw new ArgumentException(text, "path");
			}
		}

		// Token: 0x06003A55 RID: 14933 RVA: 0x000C89F0 File Offset: 0x000C6BF0
		internal static void ThrowIfInvalidPath(string[] paths)
		{
			foreach (string path in paths)
			{
				FileIOPermission.ThrowIfInvalidPath(path);
			}
		}

		// Token: 0x06003A56 RID: 14934 RVA: 0x000C8A20 File Offset: 0x000C6C20
		internal void Clear(FileIOPermissionAccess access)
		{
			if ((access & FileIOPermissionAccess.Read) == FileIOPermissionAccess.Read)
			{
				this.readList.Clear();
			}
			if ((access & FileIOPermissionAccess.Write) == FileIOPermissionAccess.Write)
			{
				this.writeList.Clear();
			}
			if ((access & FileIOPermissionAccess.Append) == FileIOPermissionAccess.Append)
			{
				this.appendList.Clear();
			}
			if ((access & FileIOPermissionAccess.PathDiscovery) == FileIOPermissionAccess.PathDiscovery)
			{
				this.pathList.Clear();
			}
		}

		// Token: 0x06003A57 RID: 14935 RVA: 0x000C8A80 File Offset: 0x000C6C80
		internal static bool KeyIsSubsetOf(IList local, IList target)
		{
			bool flag = false;
			foreach (object obj in local)
			{
				string path = (string)obj;
				foreach (object obj2 in target)
				{
					string subset = (string)obj2;
					if (Path.IsPathSubsetOf(subset, path))
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06003A58 RID: 14936 RVA: 0x000C8B68 File Offset: 0x000C6D68
		internal static void UnionKeys(IList list, string[] paths)
		{
			foreach (string text in paths)
			{
				int count = list.Count;
				if (count == 0)
				{
					list.Add(text);
				}
				else
				{
					int j;
					for (j = 0; j < count; j++)
					{
						string text2 = (string)list[j];
						if (Path.IsPathSubsetOf(text, text2))
						{
							list[j] = text;
							break;
						}
						if (Path.IsPathSubsetOf(text2, text))
						{
							break;
						}
					}
					if (j == count)
					{
						list.Add(text);
					}
				}
			}
		}

		// Token: 0x06003A59 RID: 14937 RVA: 0x000C8C0C File Offset: 0x000C6E0C
		internal static void IntersectKeys(IList local, IList target, IList result)
		{
			foreach (object obj in local)
			{
				string text = (string)obj;
				foreach (object obj2 in target)
				{
					string text2 = (string)obj2;
					if (text2.Length > text.Length)
					{
						if (Path.IsPathSubsetOf(text, text2))
						{
							result.Add(text2);
						}
					}
					else if (Path.IsPathSubsetOf(text2, text))
					{
						result.Add(text);
					}
				}
			}
		}

		// Token: 0x04001943 RID: 6467
		private const int version = 1;

		// Token: 0x04001944 RID: 6468
		private static char[] BadPathNameCharacters = Path.GetInvalidPathChars();

		// Token: 0x04001945 RID: 6469
		private static char[] BadFileNameCharacters = Path.GetInvalidFileNameChars();

		// Token: 0x04001946 RID: 6470
		private bool m_Unrestricted;

		// Token: 0x04001947 RID: 6471
		private FileIOPermissionAccess m_AllFilesAccess;

		// Token: 0x04001948 RID: 6472
		private FileIOPermissionAccess m_AllLocalFilesAccess;

		// Token: 0x04001949 RID: 6473
		private ArrayList readList;

		// Token: 0x0400194A RID: 6474
		private ArrayList writeList;

		// Token: 0x0400194B RID: 6475
		private ArrayList appendList;

		// Token: 0x0400194C RID: 6476
		private ArrayList pathList;
	}
}
