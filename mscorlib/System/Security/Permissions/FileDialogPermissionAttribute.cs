﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005F6 RID: 1526
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class FileDialogPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003A30 RID: 14896 RVA: 0x000C7F28 File Offset: 0x000C6128
		public FileDialogPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000AEE RID: 2798
		// (get) Token: 0x06003A31 RID: 14897 RVA: 0x000C7F34 File Offset: 0x000C6134
		// (set) Token: 0x06003A32 RID: 14898 RVA: 0x000C7F3C File Offset: 0x000C613C
		public bool Open
		{
			get
			{
				return this.canOpen;
			}
			set
			{
				this.canOpen = value;
			}
		}

		// Token: 0x17000AEF RID: 2799
		// (get) Token: 0x06003A33 RID: 14899 RVA: 0x000C7F48 File Offset: 0x000C6148
		// (set) Token: 0x06003A34 RID: 14900 RVA: 0x000C7F50 File Offset: 0x000C6150
		public bool Save
		{
			get
			{
				return this.canSave;
			}
			set
			{
				this.canSave = value;
			}
		}

		// Token: 0x06003A35 RID: 14901 RVA: 0x000C7F5C File Offset: 0x000C615C
		public override IPermission CreatePermission()
		{
			FileDialogPermission result;
			if (base.Unrestricted)
			{
				result = new FileDialogPermission(PermissionState.Unrestricted);
			}
			else
			{
				FileDialogPermissionAccess fileDialogPermissionAccess = FileDialogPermissionAccess.None;
				if (this.canOpen)
				{
					fileDialogPermissionAccess |= FileDialogPermissionAccess.Open;
				}
				if (this.canSave)
				{
					fileDialogPermissionAccess |= FileDialogPermissionAccess.Save;
				}
				result = new FileDialogPermission(fileDialogPermissionAccess);
			}
			return result;
		}

		// Token: 0x04001941 RID: 6465
		private bool canOpen;

		// Token: 0x04001942 RID: 6466
		private bool canSave;
	}
}
