﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200061B RID: 1563
	[ComVisible(true)]
	[Serializable]
	public sealed class SecurityPermission : CodeAccessPermission, IBuiltInPermission, IUnrestrictedPermission
	{
		// Token: 0x06003B7C RID: 15228 RVA: 0x000CC764 File Offset: 0x000CA964
		public SecurityPermission(PermissionState state)
		{
			if (CodeAccessPermission.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this.flags = SecurityPermissionFlag.AllFlags;
			}
			else
			{
				this.flags = SecurityPermissionFlag.NoFlags;
			}
		}

		// Token: 0x06003B7D RID: 15229 RVA: 0x000CC79C File Offset: 0x000CA99C
		public SecurityPermission(SecurityPermissionFlag flag)
		{
			this.Flags = flag;
		}

		// Token: 0x06003B7E RID: 15230 RVA: 0x000CC7AC File Offset: 0x000CA9AC
		int IBuiltInPermission.GetTokenIndex()
		{
			return 6;
		}

		// Token: 0x17000B3B RID: 2875
		// (get) Token: 0x06003B7F RID: 15231 RVA: 0x000CC7B0 File Offset: 0x000CA9B0
		// (set) Token: 0x06003B80 RID: 15232 RVA: 0x000CC7B8 File Offset: 0x000CA9B8
		public SecurityPermissionFlag Flags
		{
			get
			{
				return this.flags;
			}
			set
			{
				if ((value & SecurityPermissionFlag.AllFlags) != value)
				{
					string message = string.Format(Locale.GetText("Invalid flags {0}"), value);
					throw new ArgumentException(message, "SecurityPermissionFlag");
				}
				this.flags = value;
			}
		}

		// Token: 0x06003B81 RID: 15233 RVA: 0x000CC7FC File Offset: 0x000CA9FC
		public bool IsUnrestricted()
		{
			return this.flags == SecurityPermissionFlag.AllFlags;
		}

		// Token: 0x06003B82 RID: 15234 RVA: 0x000CC80C File Offset: 0x000CAA0C
		public override IPermission Copy()
		{
			return new SecurityPermission(this.flags);
		}

		// Token: 0x06003B83 RID: 15235 RVA: 0x000CC81C File Offset: 0x000CAA1C
		public override IPermission Intersect(IPermission target)
		{
			SecurityPermission securityPermission = this.Cast(target);
			if (securityPermission == null)
			{
				return null;
			}
			if (this.IsEmpty() || securityPermission.IsEmpty())
			{
				return null;
			}
			if (this.IsUnrestricted() && securityPermission.IsUnrestricted())
			{
				return new SecurityPermission(PermissionState.Unrestricted);
			}
			if (this.IsUnrestricted())
			{
				return securityPermission.Copy();
			}
			if (securityPermission.IsUnrestricted())
			{
				return this.Copy();
			}
			SecurityPermissionFlag securityPermissionFlag = this.flags & securityPermission.flags;
			if (securityPermissionFlag == SecurityPermissionFlag.NoFlags)
			{
				return null;
			}
			return new SecurityPermission(securityPermissionFlag);
		}

		// Token: 0x06003B84 RID: 15236 RVA: 0x000CC8B0 File Offset: 0x000CAAB0
		public override IPermission Union(IPermission target)
		{
			SecurityPermission securityPermission = this.Cast(target);
			if (securityPermission == null)
			{
				return this.Copy();
			}
			if (this.IsUnrestricted() || securityPermission.IsUnrestricted())
			{
				return new SecurityPermission(PermissionState.Unrestricted);
			}
			return new SecurityPermission(this.flags | securityPermission.flags);
		}

		// Token: 0x06003B85 RID: 15237 RVA: 0x000CC904 File Offset: 0x000CAB04
		public override bool IsSubsetOf(IPermission target)
		{
			SecurityPermission securityPermission = this.Cast(target);
			if (securityPermission == null)
			{
				return this.IsEmpty();
			}
			return securityPermission.IsUnrestricted() || (!this.IsUnrestricted() && (this.flags & ~securityPermission.flags) == SecurityPermissionFlag.NoFlags);
		}

		// Token: 0x06003B86 RID: 15238 RVA: 0x000CC954 File Offset: 0x000CAB54
		public override void FromXml(SecurityElement esd)
		{
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			if (CodeAccessPermission.IsUnrestricted(esd))
			{
				this.flags = SecurityPermissionFlag.AllFlags;
			}
			else
			{
				string text = esd.Attribute("Flags");
				if (text == null)
				{
					this.flags = SecurityPermissionFlag.NoFlags;
				}
				else
				{
					this.flags = (SecurityPermissionFlag)((int)Enum.Parse(typeof(SecurityPermissionFlag), text));
				}
			}
		}

		// Token: 0x06003B87 RID: 15239 RVA: 0x000CC9C4 File Offset: 0x000CABC4
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this.IsUnrestricted())
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			else
			{
				securityElement.AddAttribute("Flags", this.flags.ToString());
			}
			return securityElement;
		}

		// Token: 0x06003B88 RID: 15240 RVA: 0x000CCA18 File Offset: 0x000CAC18
		private bool IsEmpty()
		{
			return this.flags == SecurityPermissionFlag.NoFlags;
		}

		// Token: 0x06003B89 RID: 15241 RVA: 0x000CCA24 File Offset: 0x000CAC24
		private SecurityPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			SecurityPermission securityPermission = target as SecurityPermission;
			if (securityPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(SecurityPermission));
			}
			return securityPermission;
		}

		// Token: 0x040019ED RID: 6637
		private const int version = 1;

		// Token: 0x040019EE RID: 6638
		private SecurityPermissionFlag flags;
	}
}
