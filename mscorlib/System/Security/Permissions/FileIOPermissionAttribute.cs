﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005F9 RID: 1529
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class FileIOPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003A5A RID: 14938 RVA: 0x000C8D08 File Offset: 0x000C6F08
		public FileIOPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000AF2 RID: 2802
		// (get) Token: 0x06003A5B RID: 14939 RVA: 0x000C8D14 File Offset: 0x000C6F14
		// (set) Token: 0x06003A5C RID: 14940 RVA: 0x000C8D20 File Offset: 0x000C6F20
		[Obsolete("use newer properties")]
		public string All
		{
			get
			{
				throw new NotSupportedException("All");
			}
			set
			{
				this.append = value;
				this.path = value;
				this.read = value;
				this.write = value;
			}
		}

		// Token: 0x17000AF3 RID: 2803
		// (get) Token: 0x06003A5D RID: 14941 RVA: 0x000C8D40 File Offset: 0x000C6F40
		// (set) Token: 0x06003A5E RID: 14942 RVA: 0x000C8D48 File Offset: 0x000C6F48
		public string Append
		{
			get
			{
				return this.append;
			}
			set
			{
				this.append = value;
			}
		}

		// Token: 0x17000AF4 RID: 2804
		// (get) Token: 0x06003A5F RID: 14943 RVA: 0x000C8D54 File Offset: 0x000C6F54
		// (set) Token: 0x06003A60 RID: 14944 RVA: 0x000C8D5C File Offset: 0x000C6F5C
		public string PathDiscovery
		{
			get
			{
				return this.path;
			}
			set
			{
				this.path = value;
			}
		}

		// Token: 0x17000AF5 RID: 2805
		// (get) Token: 0x06003A61 RID: 14945 RVA: 0x000C8D68 File Offset: 0x000C6F68
		// (set) Token: 0x06003A62 RID: 14946 RVA: 0x000C8D70 File Offset: 0x000C6F70
		public string Read
		{
			get
			{
				return this.read;
			}
			set
			{
				this.read = value;
			}
		}

		// Token: 0x17000AF6 RID: 2806
		// (get) Token: 0x06003A63 RID: 14947 RVA: 0x000C8D7C File Offset: 0x000C6F7C
		// (set) Token: 0x06003A64 RID: 14948 RVA: 0x000C8D84 File Offset: 0x000C6F84
		public string Write
		{
			get
			{
				return this.write;
			}
			set
			{
				this.write = value;
			}
		}

		// Token: 0x17000AF7 RID: 2807
		// (get) Token: 0x06003A65 RID: 14949 RVA: 0x000C8D90 File Offset: 0x000C6F90
		// (set) Token: 0x06003A66 RID: 14950 RVA: 0x000C8D98 File Offset: 0x000C6F98
		public FileIOPermissionAccess AllFiles
		{
			get
			{
				return this.allFiles;
			}
			set
			{
				this.allFiles = value;
			}
		}

		// Token: 0x17000AF8 RID: 2808
		// (get) Token: 0x06003A67 RID: 14951 RVA: 0x000C8DA4 File Offset: 0x000C6FA4
		// (set) Token: 0x06003A68 RID: 14952 RVA: 0x000C8DAC File Offset: 0x000C6FAC
		public FileIOPermissionAccess AllLocalFiles
		{
			get
			{
				return this.allLocalFiles;
			}
			set
			{
				this.allLocalFiles = value;
			}
		}

		// Token: 0x17000AF9 RID: 2809
		// (get) Token: 0x06003A69 RID: 14953 RVA: 0x000C8DB8 File Offset: 0x000C6FB8
		// (set) Token: 0x06003A6A RID: 14954 RVA: 0x000C8DC0 File Offset: 0x000C6FC0
		public string ChangeAccessControl
		{
			get
			{
				return this.changeAccessControl;
			}
			set
			{
				this.changeAccessControl = value;
			}
		}

		// Token: 0x17000AFA RID: 2810
		// (get) Token: 0x06003A6B RID: 14955 RVA: 0x000C8DCC File Offset: 0x000C6FCC
		// (set) Token: 0x06003A6C RID: 14956 RVA: 0x000C8DD4 File Offset: 0x000C6FD4
		public string ViewAccessControl
		{
			get
			{
				return this.viewAccessControl;
			}
			set
			{
				this.viewAccessControl = value;
			}
		}

		// Token: 0x17000AFB RID: 2811
		// (get) Token: 0x06003A6D RID: 14957 RVA: 0x000C8DE0 File Offset: 0x000C6FE0
		// (set) Token: 0x06003A6E RID: 14958 RVA: 0x000C8DE8 File Offset: 0x000C6FE8
		public string ViewAndModify
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				this.append = value;
				this.path = value;
				this.read = value;
				this.write = value;
			}
		}

		// Token: 0x06003A6F RID: 14959 RVA: 0x000C8E08 File Offset: 0x000C7008
		public override IPermission CreatePermission()
		{
			FileIOPermission fileIOPermission;
			if (base.Unrestricted)
			{
				fileIOPermission = new FileIOPermission(PermissionState.Unrestricted);
			}
			else
			{
				fileIOPermission = new FileIOPermission(PermissionState.None);
				if (this.append != null)
				{
					fileIOPermission.AddPathList(FileIOPermissionAccess.Append, this.append);
				}
				if (this.path != null)
				{
					fileIOPermission.AddPathList(FileIOPermissionAccess.PathDiscovery, this.path);
				}
				if (this.read != null)
				{
					fileIOPermission.AddPathList(FileIOPermissionAccess.Read, this.read);
				}
				if (this.write != null)
				{
					fileIOPermission.AddPathList(FileIOPermissionAccess.Write, this.write);
				}
			}
			return fileIOPermission;
		}

		// Token: 0x04001954 RID: 6484
		private string append;

		// Token: 0x04001955 RID: 6485
		private string path;

		// Token: 0x04001956 RID: 6486
		private string read;

		// Token: 0x04001957 RID: 6487
		private string write;

		// Token: 0x04001958 RID: 6488
		private FileIOPermissionAccess allFiles;

		// Token: 0x04001959 RID: 6489
		private FileIOPermissionAccess allLocalFiles;

		// Token: 0x0400195A RID: 6490
		private string changeAccessControl;

		// Token: 0x0400195B RID: 6491
		private string viewAccessControl;
	}
}
