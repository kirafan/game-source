﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000605 RID: 1541
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public abstract class IsolatedStoragePermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003AB4 RID: 15028 RVA: 0x000C9A5C File Offset: 0x000C7C5C
		protected IsolatedStoragePermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000B09 RID: 2825
		// (get) Token: 0x06003AB5 RID: 15029 RVA: 0x000C9A68 File Offset: 0x000C7C68
		// (set) Token: 0x06003AB6 RID: 15030 RVA: 0x000C9A70 File Offset: 0x000C7C70
		public IsolatedStorageContainment UsageAllowed
		{
			get
			{
				return this.usage_allowed;
			}
			set
			{
				this.usage_allowed = value;
			}
		}

		// Token: 0x17000B0A RID: 2826
		// (get) Token: 0x06003AB7 RID: 15031 RVA: 0x000C9A7C File Offset: 0x000C7C7C
		// (set) Token: 0x06003AB8 RID: 15032 RVA: 0x000C9A84 File Offset: 0x000C7C84
		public long UserQuota
		{
			get
			{
				return this.user_quota;
			}
			set
			{
				this.user_quota = value;
			}
		}

		// Token: 0x04001992 RID: 6546
		private IsolatedStorageContainment usage_allowed;

		// Token: 0x04001993 RID: 6547
		private long user_quota;
	}
}
