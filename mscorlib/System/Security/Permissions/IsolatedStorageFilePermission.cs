﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000602 RID: 1538
	[ComVisible(true)]
	[Serializable]
	public sealed class IsolatedStorageFilePermission : IsolatedStoragePermission, IBuiltInPermission
	{
		// Token: 0x06003AA1 RID: 15009 RVA: 0x000C94D8 File Offset: 0x000C76D8
		public IsolatedStorageFilePermission(PermissionState state) : base(state)
		{
		}

		// Token: 0x06003AA2 RID: 15010 RVA: 0x000C94E4 File Offset: 0x000C76E4
		int IBuiltInPermission.GetTokenIndex()
		{
			return 3;
		}

		// Token: 0x06003AA3 RID: 15011 RVA: 0x000C94E8 File Offset: 0x000C76E8
		public override IPermission Copy()
		{
			return new IsolatedStorageFilePermission(PermissionState.None)
			{
				m_userQuota = this.m_userQuota,
				m_machineQuota = this.m_machineQuota,
				m_expirationDays = this.m_expirationDays,
				m_permanentData = this.m_permanentData,
				m_allowed = this.m_allowed
			};
		}

		// Token: 0x06003AA4 RID: 15012 RVA: 0x000C953C File Offset: 0x000C773C
		public override IPermission Intersect(IPermission target)
		{
			IsolatedStorageFilePermission isolatedStorageFilePermission = this.Cast(target);
			if (isolatedStorageFilePermission == null)
			{
				return null;
			}
			if (base.IsEmpty() && isolatedStorageFilePermission.IsEmpty())
			{
				return null;
			}
			return new IsolatedStorageFilePermission(PermissionState.None)
			{
				m_userQuota = ((this.m_userQuota >= isolatedStorageFilePermission.m_userQuota) ? isolatedStorageFilePermission.m_userQuota : this.m_userQuota),
				m_machineQuota = ((this.m_machineQuota >= isolatedStorageFilePermission.m_machineQuota) ? isolatedStorageFilePermission.m_machineQuota : this.m_machineQuota),
				m_expirationDays = ((this.m_expirationDays >= isolatedStorageFilePermission.m_expirationDays) ? isolatedStorageFilePermission.m_expirationDays : this.m_expirationDays),
				m_permanentData = (this.m_permanentData && isolatedStorageFilePermission.m_permanentData),
				UsageAllowed = ((this.m_allowed >= isolatedStorageFilePermission.m_allowed) ? isolatedStorageFilePermission.m_allowed : this.m_allowed)
			};
		}

		// Token: 0x06003AA5 RID: 15013 RVA: 0x000C9634 File Offset: 0x000C7834
		public override bool IsSubsetOf(IPermission target)
		{
			IsolatedStorageFilePermission isolatedStorageFilePermission = this.Cast(target);
			if (isolatedStorageFilePermission == null)
			{
				return base.IsEmpty();
			}
			return isolatedStorageFilePermission.IsUnrestricted() || (this.m_userQuota <= isolatedStorageFilePermission.m_userQuota && this.m_machineQuota <= isolatedStorageFilePermission.m_machineQuota && this.m_expirationDays <= isolatedStorageFilePermission.m_expirationDays && this.m_permanentData == isolatedStorageFilePermission.m_permanentData && this.m_allowed <= isolatedStorageFilePermission.m_allowed);
		}

		// Token: 0x06003AA6 RID: 15014 RVA: 0x000C96C4 File Offset: 0x000C78C4
		public override IPermission Union(IPermission target)
		{
			IsolatedStorageFilePermission isolatedStorageFilePermission = this.Cast(target);
			if (isolatedStorageFilePermission == null)
			{
				return this.Copy();
			}
			return new IsolatedStorageFilePermission(PermissionState.None)
			{
				m_userQuota = ((this.m_userQuota <= isolatedStorageFilePermission.m_userQuota) ? isolatedStorageFilePermission.m_userQuota : this.m_userQuota),
				m_machineQuota = ((this.m_machineQuota <= isolatedStorageFilePermission.m_machineQuota) ? isolatedStorageFilePermission.m_machineQuota : this.m_machineQuota),
				m_expirationDays = ((this.m_expirationDays <= isolatedStorageFilePermission.m_expirationDays) ? isolatedStorageFilePermission.m_expirationDays : this.m_expirationDays),
				m_permanentData = (this.m_permanentData || isolatedStorageFilePermission.m_permanentData),
				UsageAllowed = ((this.m_allowed <= isolatedStorageFilePermission.m_allowed) ? isolatedStorageFilePermission.m_allowed : this.m_allowed)
			};
		}

		// Token: 0x06003AA7 RID: 15015 RVA: 0x000C97A8 File Offset: 0x000C79A8
		[MonoTODO("(2.0) new override - something must have been added ???")]
		[ComVisible(false)]
		public override SecurityElement ToXml()
		{
			return base.ToXml();
		}

		// Token: 0x06003AA8 RID: 15016 RVA: 0x000C97B0 File Offset: 0x000C79B0
		private IsolatedStorageFilePermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			IsolatedStorageFilePermission isolatedStorageFilePermission = target as IsolatedStorageFilePermission;
			if (isolatedStorageFilePermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(IsolatedStorageFilePermission));
			}
			return isolatedStorageFilePermission;
		}

		// Token: 0x0400198B RID: 6539
		private const int version = 1;
	}
}
