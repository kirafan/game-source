﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x02000600 RID: 1536
	internal enum BuiltInToken
	{
		// Token: 0x0400196D RID: 6509
		Environment,
		// Token: 0x0400196E RID: 6510
		FileDialog,
		// Token: 0x0400196F RID: 6511
		FileIO,
		// Token: 0x04001970 RID: 6512
		IsolatedStorageFile,
		// Token: 0x04001971 RID: 6513
		Reflection,
		// Token: 0x04001972 RID: 6514
		Registry,
		// Token: 0x04001973 RID: 6515
		Security,
		// Token: 0x04001974 RID: 6516
		UI,
		// Token: 0x04001975 RID: 6517
		Principal,
		// Token: 0x04001976 RID: 6518
		HostProtection,
		// Token: 0x04001977 RID: 6519
		PublisherIdentity,
		// Token: 0x04001978 RID: 6520
		SiteIdentity,
		// Token: 0x04001979 RID: 6521
		StrongNameIdentity,
		// Token: 0x0400197A RID: 6522
		UrlIdentity,
		// Token: 0x0400197B RID: 6523
		ZoneIdentity,
		// Token: 0x0400197C RID: 6524
		GacIdentity,
		// Token: 0x0400197D RID: 6525
		KeyContainer
	}
}
