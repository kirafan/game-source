﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200062B RID: 1579
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class ZoneIdentityPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003C20 RID: 15392 RVA: 0x000CED9C File Offset: 0x000CCF9C
		public ZoneIdentityPermissionAttribute(SecurityAction action) : base(action)
		{
			this.zone = SecurityZone.NoZone;
		}

		// Token: 0x17000B5A RID: 2906
		// (get) Token: 0x06003C21 RID: 15393 RVA: 0x000CEDAC File Offset: 0x000CCFAC
		// (set) Token: 0x06003C22 RID: 15394 RVA: 0x000CEDB4 File Offset: 0x000CCFB4
		public SecurityZone Zone
		{
			get
			{
				return this.zone;
			}
			set
			{
				this.zone = value;
			}
		}

		// Token: 0x06003C23 RID: 15395 RVA: 0x000CEDC0 File Offset: 0x000CCFC0
		public override IPermission CreatePermission()
		{
			if (base.Unrestricted)
			{
				return new ZoneIdentityPermission(PermissionState.Unrestricted);
			}
			return new ZoneIdentityPermission(this.zone);
		}

		// Token: 0x04001A23 RID: 6691
		private SecurityZone zone;
	}
}
