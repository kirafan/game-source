﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000606 RID: 1542
	[ComVisible(true)]
	public interface IUnrestrictedPermission
	{
		// Token: 0x06003AB9 RID: 15033
		bool IsUnrestricted();
	}
}
