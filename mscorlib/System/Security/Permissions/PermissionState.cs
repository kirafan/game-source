﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200060E RID: 1550
	[ComVisible(true)]
	[Serializable]
	public enum PermissionState
	{
		// Token: 0x040019B7 RID: 6583
		Unrestricted = 1,
		// Token: 0x040019B8 RID: 6584
		None = 0
	}
}
