﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005FE RID: 1534
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum HostProtectionResource
	{
		// Token: 0x04001961 RID: 6497
		None = 0,
		// Token: 0x04001962 RID: 6498
		Synchronization = 1,
		// Token: 0x04001963 RID: 6499
		SharedState = 2,
		// Token: 0x04001964 RID: 6500
		ExternalProcessMgmt = 4,
		// Token: 0x04001965 RID: 6501
		SelfAffectingProcessMgmt = 8,
		// Token: 0x04001966 RID: 6502
		ExternalThreading = 16,
		// Token: 0x04001967 RID: 6503
		SelfAffectingThreading = 32,
		// Token: 0x04001968 RID: 6504
		SecurityInfrastructure = 64,
		// Token: 0x04001969 RID: 6505
		UI = 128,
		// Token: 0x0400196A RID: 6506
		MayLeakOnAbort = 256,
		// Token: 0x0400196B RID: 6507
		All = 511
	}
}
