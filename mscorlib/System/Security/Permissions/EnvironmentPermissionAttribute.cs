﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005F3 RID: 1523
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class EnvironmentPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003A1B RID: 14875 RVA: 0x000C7BDC File Offset: 0x000C5DDC
		public EnvironmentPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000AEA RID: 2794
		// (get) Token: 0x06003A1C RID: 14876 RVA: 0x000C7BE8 File Offset: 0x000C5DE8
		// (set) Token: 0x06003A1D RID: 14877 RVA: 0x000C7BF4 File Offset: 0x000C5DF4
		public string All
		{
			get
			{
				throw new NotSupportedException("All");
			}
			set
			{
				this.read = value;
				this.write = value;
			}
		}

		// Token: 0x17000AEB RID: 2795
		// (get) Token: 0x06003A1E RID: 14878 RVA: 0x000C7C04 File Offset: 0x000C5E04
		// (set) Token: 0x06003A1F RID: 14879 RVA: 0x000C7C0C File Offset: 0x000C5E0C
		public string Read
		{
			get
			{
				return this.read;
			}
			set
			{
				this.read = value;
			}
		}

		// Token: 0x17000AEC RID: 2796
		// (get) Token: 0x06003A20 RID: 14880 RVA: 0x000C7C18 File Offset: 0x000C5E18
		// (set) Token: 0x06003A21 RID: 14881 RVA: 0x000C7C20 File Offset: 0x000C5E20
		public string Write
		{
			get
			{
				return this.write;
			}
			set
			{
				this.write = value;
			}
		}

		// Token: 0x06003A22 RID: 14882 RVA: 0x000C7C2C File Offset: 0x000C5E2C
		public override IPermission CreatePermission()
		{
			EnvironmentPermission environmentPermission;
			if (base.Unrestricted)
			{
				environmentPermission = new EnvironmentPermission(PermissionState.Unrestricted);
			}
			else
			{
				environmentPermission = new EnvironmentPermission(PermissionState.None);
				if (this.read != null)
				{
					environmentPermission.AddPathList(EnvironmentPermissionAccess.Read, this.read);
				}
				if (this.write != null)
				{
					environmentPermission.AddPathList(EnvironmentPermissionAccess.Write, this.write);
				}
			}
			return environmentPermission;
		}

		// Token: 0x04001938 RID: 6456
		private string read;

		// Token: 0x04001939 RID: 6457
		private string write;
	}
}
