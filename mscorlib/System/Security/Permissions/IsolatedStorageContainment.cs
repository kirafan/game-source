﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000601 RID: 1537
	[ComVisible(true)]
	[Serializable]
	public enum IsolatedStorageContainment
	{
		// Token: 0x0400197F RID: 6527
		None,
		// Token: 0x04001980 RID: 6528
		DomainIsolationByUser = 16,
		// Token: 0x04001981 RID: 6529
		AssemblyIsolationByUser = 32,
		// Token: 0x04001982 RID: 6530
		DomainIsolationByRoamingUser = 80,
		// Token: 0x04001983 RID: 6531
		AssemblyIsolationByRoamingUser = 96,
		// Token: 0x04001984 RID: 6532
		AdministerIsolatedStorageByUser = 112,
		// Token: 0x04001985 RID: 6533
		UnrestrictedIsolatedStorage = 240,
		// Token: 0x04001986 RID: 6534
		ApplicationIsolationByUser = 21,
		// Token: 0x04001987 RID: 6535
		DomainIsolationByMachine = 48,
		// Token: 0x04001988 RID: 6536
		AssemblyIsolationByMachine = 64,
		// Token: 0x04001989 RID: 6537
		ApplicationIsolationByMachine = 69,
		// Token: 0x0400198A RID: 6538
		ApplicationIsolationByRoamingUser = 101
	}
}
