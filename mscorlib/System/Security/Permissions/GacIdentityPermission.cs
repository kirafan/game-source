﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005FA RID: 1530
	[ComVisible(true)]
	[Serializable]
	public sealed class GacIdentityPermission : CodeAccessPermission, IBuiltInPermission
	{
		// Token: 0x06003A70 RID: 14960 RVA: 0x000C8E98 File Offset: 0x000C7098
		public GacIdentityPermission()
		{
		}

		// Token: 0x06003A71 RID: 14961 RVA: 0x000C8EA0 File Offset: 0x000C70A0
		public GacIdentityPermission(PermissionState state)
		{
			CodeAccessPermission.CheckPermissionState(state, false);
		}

		// Token: 0x06003A72 RID: 14962 RVA: 0x000C8EB0 File Offset: 0x000C70B0
		int IBuiltInPermission.GetTokenIndex()
		{
			return 15;
		}

		// Token: 0x06003A73 RID: 14963 RVA: 0x000C8EB4 File Offset: 0x000C70B4
		public override IPermission Copy()
		{
			return new GacIdentityPermission();
		}

		// Token: 0x06003A74 RID: 14964 RVA: 0x000C8EBC File Offset: 0x000C70BC
		public override IPermission Intersect(IPermission target)
		{
			if (this.Cast(target) == null)
			{
				return null;
			}
			return this.Copy();
		}

		// Token: 0x06003A75 RID: 14965 RVA: 0x000C8EE0 File Offset: 0x000C70E0
		public override bool IsSubsetOf(IPermission target)
		{
			GacIdentityPermission gacIdentityPermission = this.Cast(target);
			return gacIdentityPermission != null;
		}

		// Token: 0x06003A76 RID: 14966 RVA: 0x000C8EFC File Offset: 0x000C70FC
		public override IPermission Union(IPermission target)
		{
			this.Cast(target);
			return this.Copy();
		}

		// Token: 0x06003A77 RID: 14967 RVA: 0x000C8F0C File Offset: 0x000C710C
		public override void FromXml(SecurityElement securityElement)
		{
			CodeAccessPermission.CheckSecurityElement(securityElement, "securityElement", 1, 1);
		}

		// Token: 0x06003A78 RID: 14968 RVA: 0x000C8F1C File Offset: 0x000C711C
		public override SecurityElement ToXml()
		{
			return base.Element(1);
		}

		// Token: 0x06003A79 RID: 14969 RVA: 0x000C8F34 File Offset: 0x000C7134
		private GacIdentityPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			GacIdentityPermission gacIdentityPermission = target as GacIdentityPermission;
			if (gacIdentityPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(GacIdentityPermission));
			}
			return gacIdentityPermission;
		}

		// Token: 0x0400195C RID: 6492
		private const int version = 1;
	}
}
