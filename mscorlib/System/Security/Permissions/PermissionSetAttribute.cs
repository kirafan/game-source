﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Text;
using Mono.Security.Cryptography;
using Mono.Xml;

namespace System.Security.Permissions
{
	// Token: 0x0200060D RID: 1549
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class PermissionSetAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003AFC RID: 15100 RVA: 0x000CA31C File Offset: 0x000C851C
		public PermissionSetAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000B1F RID: 2847
		// (get) Token: 0x06003AFD RID: 15101 RVA: 0x000CA328 File Offset: 0x000C8528
		// (set) Token: 0x06003AFE RID: 15102 RVA: 0x000CA330 File Offset: 0x000C8530
		public string File
		{
			get
			{
				return this.file;
			}
			set
			{
				this.file = value;
			}
		}

		// Token: 0x17000B20 RID: 2848
		// (get) Token: 0x06003AFF RID: 15103 RVA: 0x000CA33C File Offset: 0x000C853C
		// (set) Token: 0x06003B00 RID: 15104 RVA: 0x000CA344 File Offset: 0x000C8544
		public string Hex
		{
			get
			{
				return this.hex;
			}
			set
			{
				this.hex = value;
			}
		}

		// Token: 0x17000B21 RID: 2849
		// (get) Token: 0x06003B01 RID: 15105 RVA: 0x000CA350 File Offset: 0x000C8550
		// (set) Token: 0x06003B02 RID: 15106 RVA: 0x000CA358 File Offset: 0x000C8558
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000B22 RID: 2850
		// (get) Token: 0x06003B03 RID: 15107 RVA: 0x000CA364 File Offset: 0x000C8564
		// (set) Token: 0x06003B04 RID: 15108 RVA: 0x000CA36C File Offset: 0x000C856C
		public bool UnicodeEncoded
		{
			get
			{
				return this.isUnicodeEncoded;
			}
			set
			{
				this.isUnicodeEncoded = value;
			}
		}

		// Token: 0x17000B23 RID: 2851
		// (get) Token: 0x06003B05 RID: 15109 RVA: 0x000CA378 File Offset: 0x000C8578
		// (set) Token: 0x06003B06 RID: 15110 RVA: 0x000CA380 File Offset: 0x000C8580
		public string XML
		{
			get
			{
				return this.xml;
			}
			set
			{
				this.xml = value;
			}
		}

		// Token: 0x06003B07 RID: 15111 RVA: 0x000CA38C File Offset: 0x000C858C
		public override IPermission CreatePermission()
		{
			return null;
		}

		// Token: 0x06003B08 RID: 15112 RVA: 0x000CA390 File Offset: 0x000C8590
		private PermissionSet CreateFromXml(string xml)
		{
			SecurityParser securityParser = new SecurityParser();
			try
			{
				securityParser.LoadXml(xml);
			}
			catch (SmallXmlParserException ex)
			{
				throw new XmlSyntaxException(ex.Line, ex.ToString());
			}
			SecurityElement securityElement = securityParser.ToXml();
			string text = securityElement.Attribute("class");
			if (text == null)
			{
				return null;
			}
			PermissionState state = PermissionState.None;
			if (CodeAccessPermission.IsUnrestricted(securityElement))
			{
				state = PermissionState.Unrestricted;
			}
			if (text.EndsWith("NamedPermissionSet"))
			{
				NamedPermissionSet namedPermissionSet = new NamedPermissionSet(securityElement.Attribute("Name"), state);
				namedPermissionSet.FromXml(securityElement);
				return namedPermissionSet;
			}
			if (text.EndsWith("PermissionSet"))
			{
				PermissionSet permissionSet = new PermissionSet(state);
				permissionSet.FromXml(securityElement);
				return permissionSet;
			}
			return null;
		}

		// Token: 0x06003B09 RID: 15113 RVA: 0x000CA464 File Offset: 0x000C8664
		public PermissionSet CreatePermissionSet()
		{
			PermissionSet result = null;
			if (base.Unrestricted)
			{
				result = new PermissionSet(PermissionState.Unrestricted);
			}
			else
			{
				result = new PermissionSet(PermissionState.None);
				if (this.name != null)
				{
					return PolicyLevel.CreateAppDomainLevel().GetNamedPermissionSet(this.name);
				}
				if (this.file != null)
				{
					Encoding encoding = (!this.isUnicodeEncoded) ? Encoding.ASCII : Encoding.Unicode;
					using (StreamReader streamReader = new StreamReader(this.file, encoding))
					{
						result = this.CreateFromXml(streamReader.ReadToEnd());
					}
				}
				else if (this.xml != null)
				{
					result = this.CreateFromXml(this.xml);
				}
				else if (this.hex != null)
				{
					Encoding ascii = Encoding.ASCII;
					byte[] array = CryptoConvert.FromHex(this.hex);
					result = this.CreateFromXml(ascii.GetString(array, 0, array.Length));
				}
			}
			return result;
		}

		// Token: 0x040019B1 RID: 6577
		private string file;

		// Token: 0x040019B2 RID: 6578
		private string name;

		// Token: 0x040019B3 RID: 6579
		private bool isUnicodeEncoded;

		// Token: 0x040019B4 RID: 6580
		private string xml;

		// Token: 0x040019B5 RID: 6581
		private string hex;
	}
}
