﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000604 RID: 1540
	[ComVisible(true)]
	[PermissionSet(SecurityAction.InheritanceDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
	[Serializable]
	public abstract class IsolatedStoragePermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x06003AAB RID: 15019 RVA: 0x000C9838 File Offset: 0x000C7A38
		protected IsolatedStoragePermission(PermissionState state)
		{
			if (CodeAccessPermission.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this.UsageAllowed = IsolatedStorageContainment.UnrestrictedIsolatedStorage;
			}
		}

		// Token: 0x17000B07 RID: 2823
		// (get) Token: 0x06003AAC RID: 15020 RVA: 0x000C9858 File Offset: 0x000C7A58
		// (set) Token: 0x06003AAD RID: 15021 RVA: 0x000C9860 File Offset: 0x000C7A60
		public long UserQuota
		{
			get
			{
				return this.m_userQuota;
			}
			set
			{
				this.m_userQuota = value;
			}
		}

		// Token: 0x17000B08 RID: 2824
		// (get) Token: 0x06003AAE RID: 15022 RVA: 0x000C986C File Offset: 0x000C7A6C
		// (set) Token: 0x06003AAF RID: 15023 RVA: 0x000C9874 File Offset: 0x000C7A74
		public IsolatedStorageContainment UsageAllowed
		{
			get
			{
				return this.m_allowed;
			}
			set
			{
				if (!Enum.IsDefined(typeof(IsolatedStorageContainment), value))
				{
					string message = string.Format(Locale.GetText("Invalid enum {0}"), value);
					throw new ArgumentException(message, "IsolatedStorageContainment");
				}
				this.m_allowed = value;
				if (this.m_allowed == IsolatedStorageContainment.UnrestrictedIsolatedStorage)
				{
					this.m_userQuota = long.MaxValue;
					this.m_machineQuota = long.MaxValue;
					this.m_expirationDays = long.MaxValue;
					this.m_permanentData = true;
				}
			}
		}

		// Token: 0x06003AB0 RID: 15024 RVA: 0x000C9908 File Offset: 0x000C7B08
		public bool IsUnrestricted()
		{
			return IsolatedStorageContainment.UnrestrictedIsolatedStorage == this.m_allowed;
		}

		// Token: 0x06003AB1 RID: 15025 RVA: 0x000C9918 File Offset: 0x000C7B18
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this.m_allowed == IsolatedStorageContainment.UnrestrictedIsolatedStorage)
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			else
			{
				securityElement.AddAttribute("Allowed", this.m_allowed.ToString());
				if (this.m_userQuota > 0L)
				{
					securityElement.AddAttribute("UserQuota", this.m_userQuota.ToString());
				}
			}
			return securityElement;
		}

		// Token: 0x06003AB2 RID: 15026 RVA: 0x000C9994 File Offset: 0x000C7B94
		public override void FromXml(SecurityElement esd)
		{
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			this.m_userQuota = 0L;
			this.m_machineQuota = 0L;
			this.m_expirationDays = 0L;
			this.m_permanentData = false;
			this.m_allowed = IsolatedStorageContainment.None;
			if (CodeAccessPermission.IsUnrestricted(esd))
			{
				this.UsageAllowed = IsolatedStorageContainment.UnrestrictedIsolatedStorage;
			}
			else
			{
				string text = esd.Attribute("Allowed");
				if (text != null)
				{
					this.UsageAllowed = (IsolatedStorageContainment)((int)Enum.Parse(typeof(IsolatedStorageContainment), text));
				}
				text = esd.Attribute("UserQuota");
				if (text != null)
				{
					Exception ex;
					long.Parse(text, true, out this.m_userQuota, out ex);
				}
			}
		}

		// Token: 0x06003AB3 RID: 15027 RVA: 0x000C9A40 File Offset: 0x000C7C40
		internal bool IsEmpty()
		{
			return this.m_userQuota == 0L && this.m_allowed == IsolatedStorageContainment.None;
		}

		// Token: 0x0400198C RID: 6540
		private const int version = 1;

		// Token: 0x0400198D RID: 6541
		internal long m_userQuota;

		// Token: 0x0400198E RID: 6542
		internal long m_machineQuota;

		// Token: 0x0400198F RID: 6543
		internal long m_expirationDays;

		// Token: 0x04001990 RID: 6544
		internal bool m_permanentData;

		// Token: 0x04001991 RID: 6545
		internal IsolatedStorageContainment m_allowed;
	}
}
