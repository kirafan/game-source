﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000626 RID: 1574
	[ComVisible(true)]
	[Serializable]
	public enum UIPermissionClipboard
	{
		// Token: 0x04001A16 RID: 6678
		NoClipboard,
		// Token: 0x04001A17 RID: 6679
		OwnClipboard,
		// Token: 0x04001A18 RID: 6680
		AllClipboard
	}
}
