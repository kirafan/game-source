﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000622 RID: 1570
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class StrongNameIdentityPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003BDC RID: 15324 RVA: 0x000CE034 File Offset: 0x000CC234
		public StrongNameIdentityPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000B50 RID: 2896
		// (get) Token: 0x06003BDD RID: 15325 RVA: 0x000CE040 File Offset: 0x000CC240
		// (set) Token: 0x06003BDE RID: 15326 RVA: 0x000CE048 File Offset: 0x000CC248
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000B51 RID: 2897
		// (get) Token: 0x06003BDF RID: 15327 RVA: 0x000CE054 File Offset: 0x000CC254
		// (set) Token: 0x06003BE0 RID: 15328 RVA: 0x000CE05C File Offset: 0x000CC25C
		public string PublicKey
		{
			get
			{
				return this.key;
			}
			set
			{
				this.key = value;
			}
		}

		// Token: 0x17000B52 RID: 2898
		// (get) Token: 0x06003BE1 RID: 15329 RVA: 0x000CE068 File Offset: 0x000CC268
		// (set) Token: 0x06003BE2 RID: 15330 RVA: 0x000CE070 File Offset: 0x000CC270
		public string Version
		{
			get
			{
				return this.version;
			}
			set
			{
				this.version = value;
			}
		}

		// Token: 0x06003BE3 RID: 15331 RVA: 0x000CE07C File Offset: 0x000CC27C
		public override IPermission CreatePermission()
		{
			if (base.Unrestricted)
			{
				return new StrongNameIdentityPermission(PermissionState.Unrestricted);
			}
			if (this.name == null && this.key == null && this.version == null)
			{
				return new StrongNameIdentityPermission(PermissionState.None);
			}
			if (this.key == null)
			{
				throw new ArgumentException(Locale.GetText("PublicKey is required"));
			}
			StrongNamePublicKeyBlob blob = StrongNamePublicKeyBlob.FromString(this.key);
			Version version = null;
			if (this.version != null)
			{
				version = new Version(this.version);
			}
			return new StrongNameIdentityPermission(blob, this.name, version);
		}

		// Token: 0x04001A0C RID: 6668
		private string name;

		// Token: 0x04001A0D RID: 6669
		private string key;

		// Token: 0x04001A0E RID: 6670
		private string version;
	}
}
