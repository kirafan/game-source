﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x0200061A RID: 1562
	[ComVisible(true)]
	[Serializable]
	public enum SecurityAction
	{
		// Token: 0x040019E4 RID: 6628
		Demand = 2,
		// Token: 0x040019E5 RID: 6629
		Assert,
		// Token: 0x040019E6 RID: 6630
		Deny,
		// Token: 0x040019E7 RID: 6631
		PermitOnly,
		// Token: 0x040019E8 RID: 6632
		LinkDemand,
		// Token: 0x040019E9 RID: 6633
		InheritanceDemand,
		// Token: 0x040019EA RID: 6634
		RequestMinimum,
		// Token: 0x040019EB RID: 6635
		RequestOptional,
		// Token: 0x040019EC RID: 6636
		RequestRefuse
	}
}
