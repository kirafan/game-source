﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005F4 RID: 1524
	[ComVisible(true)]
	[Serializable]
	public sealed class FileDialogPermission : CodeAccessPermission, IBuiltInPermission, IUnrestrictedPermission
	{
		// Token: 0x06003A23 RID: 14883 RVA: 0x000C7C8C File Offset: 0x000C5E8C
		public FileDialogPermission(PermissionState state)
		{
			if (CodeAccessPermission.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this._access = FileDialogPermissionAccess.OpenSave;
			}
			else
			{
				this._access = FileDialogPermissionAccess.None;
			}
		}

		// Token: 0x06003A24 RID: 14884 RVA: 0x000C7CC0 File Offset: 0x000C5EC0
		public FileDialogPermission(FileDialogPermissionAccess access)
		{
			this.Access = access;
		}

		// Token: 0x06003A25 RID: 14885 RVA: 0x000C7CD0 File Offset: 0x000C5ED0
		int IBuiltInPermission.GetTokenIndex()
		{
			return 1;
		}

		// Token: 0x17000AED RID: 2797
		// (get) Token: 0x06003A26 RID: 14886 RVA: 0x000C7CD4 File Offset: 0x000C5ED4
		// (set) Token: 0x06003A27 RID: 14887 RVA: 0x000C7CDC File Offset: 0x000C5EDC
		public FileDialogPermissionAccess Access
		{
			get
			{
				return this._access;
			}
			set
			{
				if (!Enum.IsDefined(typeof(FileDialogPermissionAccess), value))
				{
					string message = string.Format(Locale.GetText("Invalid enum {0}"), value);
					throw new ArgumentException(message, "FileDialogPermissionAccess");
				}
				this._access = value;
			}
		}

		// Token: 0x06003A28 RID: 14888 RVA: 0x000C7D2C File Offset: 0x000C5F2C
		public override IPermission Copy()
		{
			return new FileDialogPermission(this._access);
		}

		// Token: 0x06003A29 RID: 14889 RVA: 0x000C7D3C File Offset: 0x000C5F3C
		public override void FromXml(SecurityElement esd)
		{
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			if (CodeAccessPermission.IsUnrestricted(esd))
			{
				this._access = FileDialogPermissionAccess.OpenSave;
			}
			else
			{
				string text = esd.Attribute("Access");
				if (text == null)
				{
					this._access = FileDialogPermissionAccess.None;
				}
				else
				{
					this._access = (FileDialogPermissionAccess)((int)Enum.Parse(typeof(FileDialogPermissionAccess), text));
				}
			}
		}

		// Token: 0x06003A2A RID: 14890 RVA: 0x000C7DA8 File Offset: 0x000C5FA8
		public override IPermission Intersect(IPermission target)
		{
			FileDialogPermission fileDialogPermission = this.Cast(target);
			if (fileDialogPermission == null)
			{
				return null;
			}
			FileDialogPermissionAccess fileDialogPermissionAccess = this._access & fileDialogPermission._access;
			return (fileDialogPermissionAccess != FileDialogPermissionAccess.None) ? new FileDialogPermission(fileDialogPermissionAccess) : null;
		}

		// Token: 0x06003A2B RID: 14891 RVA: 0x000C7DE8 File Offset: 0x000C5FE8
		public override bool IsSubsetOf(IPermission target)
		{
			FileDialogPermission fileDialogPermission = this.Cast(target);
			return fileDialogPermission != null && (this._access & fileDialogPermission._access) == this._access;
		}

		// Token: 0x06003A2C RID: 14892 RVA: 0x000C7E1C File Offset: 0x000C601C
		public bool IsUnrestricted()
		{
			return this._access == FileDialogPermissionAccess.OpenSave;
		}

		// Token: 0x06003A2D RID: 14893 RVA: 0x000C7E28 File Offset: 0x000C6028
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			switch (this._access)
			{
			case FileDialogPermissionAccess.Open:
				securityElement.AddAttribute("Access", "Open");
				break;
			case FileDialogPermissionAccess.Save:
				securityElement.AddAttribute("Access", "Save");
				break;
			case FileDialogPermissionAccess.OpenSave:
				securityElement.AddAttribute("Unrestricted", "true");
				break;
			}
			return securityElement;
		}

		// Token: 0x06003A2E RID: 14894 RVA: 0x000C7EA0 File Offset: 0x000C60A0
		public override IPermission Union(IPermission target)
		{
			FileDialogPermission fileDialogPermission = this.Cast(target);
			if (fileDialogPermission == null)
			{
				return this.Copy();
			}
			if (this.IsUnrestricted() || fileDialogPermission.IsUnrestricted())
			{
				return new FileDialogPermission(PermissionState.Unrestricted);
			}
			return new FileDialogPermission(this._access | fileDialogPermission._access);
		}

		// Token: 0x06003A2F RID: 14895 RVA: 0x000C7EF4 File Offset: 0x000C60F4
		private FileDialogPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			FileDialogPermission fileDialogPermission = target as FileDialogPermission;
			if (fileDialogPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(FileDialogPermission));
			}
			return fileDialogPermission;
		}

		// Token: 0x0400193A RID: 6458
		private const int version = 1;

		// Token: 0x0400193B RID: 6459
		private FileDialogPermissionAccess _access;
	}
}
