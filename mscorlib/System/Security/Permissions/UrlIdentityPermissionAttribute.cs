﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000629 RID: 1577
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class UrlIdentityPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003C10 RID: 15376 RVA: 0x000CEAE8 File Offset: 0x000CCCE8
		public UrlIdentityPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000B58 RID: 2904
		// (get) Token: 0x06003C11 RID: 15377 RVA: 0x000CEAF4 File Offset: 0x000CCCF4
		// (set) Token: 0x06003C12 RID: 15378 RVA: 0x000CEAFC File Offset: 0x000CCCFC
		public string Url
		{
			get
			{
				return this.url;
			}
			set
			{
				this.url = value;
			}
		}

		// Token: 0x06003C13 RID: 15379 RVA: 0x000CEB08 File Offset: 0x000CCD08
		public override IPermission CreatePermission()
		{
			if (base.Unrestricted)
			{
				return new UrlIdentityPermission(PermissionState.Unrestricted);
			}
			if (this.url == null)
			{
				return new UrlIdentityPermission(PermissionState.None);
			}
			return new UrlIdentityPermission(this.url);
		}

		// Token: 0x04001A20 RID: 6688
		private string url;
	}
}
