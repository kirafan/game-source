﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000625 RID: 1573
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class UIPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06003BFC RID: 15356 RVA: 0x000CE6F8 File Offset: 0x000CC8F8
		public UIPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x17000B55 RID: 2901
		// (get) Token: 0x06003BFD RID: 15357 RVA: 0x000CE704 File Offset: 0x000CC904
		// (set) Token: 0x06003BFE RID: 15358 RVA: 0x000CE70C File Offset: 0x000CC90C
		public UIPermissionClipboard Clipboard
		{
			get
			{
				return this.clipboard;
			}
			set
			{
				this.clipboard = value;
			}
		}

		// Token: 0x17000B56 RID: 2902
		// (get) Token: 0x06003BFF RID: 15359 RVA: 0x000CE718 File Offset: 0x000CC918
		// (set) Token: 0x06003C00 RID: 15360 RVA: 0x000CE720 File Offset: 0x000CC920
		public UIPermissionWindow Window
		{
			get
			{
				return this.window;
			}
			set
			{
				this.window = value;
			}
		}

		// Token: 0x06003C01 RID: 15361 RVA: 0x000CE72C File Offset: 0x000CC92C
		public override IPermission CreatePermission()
		{
			UIPermission result;
			if (base.Unrestricted)
			{
				result = new UIPermission(PermissionState.Unrestricted);
			}
			else
			{
				result = new UIPermission(this.window, this.clipboard);
			}
			return result;
		}

		// Token: 0x04001A13 RID: 6675
		private UIPermissionClipboard clipboard;

		// Token: 0x04001A14 RID: 6676
		private UIPermissionWindow window;
	}
}
