﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x020005F5 RID: 1525
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum FileDialogPermissionAccess
	{
		// Token: 0x0400193D RID: 6461
		None = 0,
		// Token: 0x0400193E RID: 6462
		Open = 1,
		// Token: 0x0400193F RID: 6463
		Save = 2,
		// Token: 0x04001940 RID: 6464
		OpenSave = 3
	}
}
