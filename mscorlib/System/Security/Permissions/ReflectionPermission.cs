﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000614 RID: 1556
	[ComVisible(true)]
	[Serializable]
	public sealed class ReflectionPermission : CodeAccessPermission, IBuiltInPermission, IUnrestrictedPermission
	{
		// Token: 0x06003B3D RID: 15165 RVA: 0x000CB514 File Offset: 0x000C9714
		public ReflectionPermission(PermissionState state)
		{
			if (CodeAccessPermission.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this.flags = ReflectionPermissionFlag.AllFlags;
			}
			else
			{
				this.flags = ReflectionPermissionFlag.NoFlags;
			}
		}

		// Token: 0x06003B3E RID: 15166 RVA: 0x000CB548 File Offset: 0x000C9748
		public ReflectionPermission(ReflectionPermissionFlag flag)
		{
			this.Flags = flag;
		}

		// Token: 0x06003B3F RID: 15167 RVA: 0x000CB558 File Offset: 0x000C9758
		int IBuiltInPermission.GetTokenIndex()
		{
			return 4;
		}

		// Token: 0x17000B2E RID: 2862
		// (get) Token: 0x06003B40 RID: 15168 RVA: 0x000CB55C File Offset: 0x000C975C
		// (set) Token: 0x06003B41 RID: 15169 RVA: 0x000CB564 File Offset: 0x000C9764
		public ReflectionPermissionFlag Flags
		{
			get
			{
				return this.flags;
			}
			set
			{
				if ((value & (ReflectionPermissionFlag.TypeInformation | ReflectionPermissionFlag.MemberAccess | ReflectionPermissionFlag.ReflectionEmit | ReflectionPermissionFlag.RestrictedMemberAccess)) != value)
				{
					string message = string.Format(Locale.GetText("Invalid flags {0}"), value);
					throw new ArgumentException(message, "ReflectionPermissionFlag");
				}
				this.flags = value;
			}
		}

		// Token: 0x06003B42 RID: 15170 RVA: 0x000CB5A4 File Offset: 0x000C97A4
		public override IPermission Copy()
		{
			return new ReflectionPermission(this.flags);
		}

		// Token: 0x06003B43 RID: 15171 RVA: 0x000CB5B4 File Offset: 0x000C97B4
		public override void FromXml(SecurityElement esd)
		{
			CodeAccessPermission.CheckSecurityElement(esd, "esd", 1, 1);
			if (CodeAccessPermission.IsUnrestricted(esd))
			{
				this.flags = ReflectionPermissionFlag.AllFlags;
			}
			else
			{
				this.flags = ReflectionPermissionFlag.NoFlags;
				string text = esd.Attributes["Flags"] as string;
				if (text.IndexOf("MemberAccess") >= 0)
				{
					this.flags |= ReflectionPermissionFlag.MemberAccess;
				}
				if (text.IndexOf("ReflectionEmit") >= 0)
				{
					this.flags |= ReflectionPermissionFlag.ReflectionEmit;
				}
				if (text.IndexOf("TypeInformation") >= 0)
				{
					this.flags |= ReflectionPermissionFlag.TypeInformation;
				}
			}
		}

		// Token: 0x06003B44 RID: 15172 RVA: 0x000CB660 File Offset: 0x000C9860
		public override IPermission Intersect(IPermission target)
		{
			ReflectionPermission reflectionPermission = this.Cast(target);
			if (reflectionPermission == null)
			{
				return null;
			}
			if (this.IsUnrestricted())
			{
				if (reflectionPermission.Flags == ReflectionPermissionFlag.NoFlags)
				{
					return null;
				}
				return reflectionPermission.Copy();
			}
			else
			{
				if (!reflectionPermission.IsUnrestricted())
				{
					ReflectionPermission reflectionPermission2 = (ReflectionPermission)reflectionPermission.Copy();
					reflectionPermission2.Flags &= this.flags;
					return (reflectionPermission2.Flags != ReflectionPermissionFlag.NoFlags) ? reflectionPermission2 : null;
				}
				if (this.flags == ReflectionPermissionFlag.NoFlags)
				{
					return null;
				}
				return this.Copy();
			}
		}

		// Token: 0x06003B45 RID: 15173 RVA: 0x000CB6EC File Offset: 0x000C98EC
		public override bool IsSubsetOf(IPermission target)
		{
			ReflectionPermission reflectionPermission = this.Cast(target);
			if (reflectionPermission == null)
			{
				return this.flags == ReflectionPermissionFlag.NoFlags;
			}
			if (this.IsUnrestricted())
			{
				return reflectionPermission.IsUnrestricted();
			}
			return reflectionPermission.IsUnrestricted() || (this.flags & reflectionPermission.Flags) == this.flags;
		}

		// Token: 0x06003B46 RID: 15174 RVA: 0x000CB748 File Offset: 0x000C9948
		public bool IsUnrestricted()
		{
			return this.flags == ReflectionPermissionFlag.AllFlags;
		}

		// Token: 0x06003B47 RID: 15175 RVA: 0x000CB754 File Offset: 0x000C9954
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = base.Element(1);
			if (this.IsUnrestricted())
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			else if (this.flags == ReflectionPermissionFlag.NoFlags)
			{
				securityElement.AddAttribute("Flags", "NoFlags");
			}
			else if ((this.flags & ReflectionPermissionFlag.AllFlags) == ReflectionPermissionFlag.AllFlags)
			{
				securityElement.AddAttribute("Flags", "AllFlags");
			}
			else
			{
				string text = string.Empty;
				if ((this.flags & ReflectionPermissionFlag.MemberAccess) == ReflectionPermissionFlag.MemberAccess)
				{
					text = "MemberAccess";
				}
				if ((this.flags & ReflectionPermissionFlag.ReflectionEmit) == ReflectionPermissionFlag.ReflectionEmit)
				{
					if (text.Length > 0)
					{
						text += ", ";
					}
					text += "ReflectionEmit";
				}
				if ((this.flags & ReflectionPermissionFlag.TypeInformation) == ReflectionPermissionFlag.TypeInformation)
				{
					if (text.Length > 0)
					{
						text += ", ";
					}
					text += "TypeInformation";
				}
				securityElement.AddAttribute("Flags", text);
			}
			return securityElement;
		}

		// Token: 0x06003B48 RID: 15176 RVA: 0x000CB858 File Offset: 0x000C9A58
		public override IPermission Union(IPermission other)
		{
			ReflectionPermission reflectionPermission = this.Cast(other);
			if (other == null)
			{
				return this.Copy();
			}
			if (this.IsUnrestricted() || reflectionPermission.IsUnrestricted())
			{
				return new ReflectionPermission(PermissionState.Unrestricted);
			}
			ReflectionPermission reflectionPermission2 = (ReflectionPermission)reflectionPermission.Copy();
			reflectionPermission2.Flags |= this.flags;
			return reflectionPermission2;
		}

		// Token: 0x06003B49 RID: 15177 RVA: 0x000CB8B8 File Offset: 0x000C9AB8
		private ReflectionPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			ReflectionPermission reflectionPermission = target as ReflectionPermission;
			if (reflectionPermission == null)
			{
				CodeAccessPermission.ThrowInvalidPermission(target, typeof(ReflectionPermission));
			}
			return reflectionPermission;
		}

		// Token: 0x040019C6 RID: 6598
		private const int version = 1;

		// Token: 0x040019C7 RID: 6599
		private ReflectionPermissionFlag flags;
	}
}
