﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000046 RID: 70
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public abstract class SecurityAttribute : Attribute
	{
		// Token: 0x06000662 RID: 1634 RVA: 0x00014AFC File Offset: 0x00012CFC
		protected SecurityAttribute(SecurityAction action)
		{
			this.Action = action;
		}

		// Token: 0x06000663 RID: 1635
		public abstract IPermission CreatePermission();

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x06000664 RID: 1636 RVA: 0x00014B0C File Offset: 0x00012D0C
		// (set) Token: 0x06000665 RID: 1637 RVA: 0x00014B14 File Offset: 0x00012D14
		public bool Unrestricted
		{
			get
			{
				return this.m_Unrestricted;
			}
			set
			{
				this.m_Unrestricted = value;
			}
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x06000666 RID: 1638 RVA: 0x00014B20 File Offset: 0x00012D20
		// (set) Token: 0x06000667 RID: 1639 RVA: 0x00014B28 File Offset: 0x00012D28
		public SecurityAction Action
		{
			get
			{
				return this.m_Action;
			}
			set
			{
				this.m_Action = value;
			}
		}

		// Token: 0x0400009B RID: 155
		private SecurityAction m_Action;

		// Token: 0x0400009C RID: 156
		private bool m_Unrestricted;
	}
}
