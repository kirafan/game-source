﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Security.Permissions
{
	// Token: 0x02000609 RID: 1545
	[ComVisible(true)]
	[Serializable]
	public sealed class KeyContainerPermissionAccessEntryCollection : IEnumerable, ICollection
	{
		// Token: 0x06003ADA RID: 15066 RVA: 0x000C9FDC File Offset: 0x000C81DC
		internal KeyContainerPermissionAccessEntryCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x06003ADB RID: 15067 RVA: 0x000C9FF0 File Offset: 0x000C81F0
		internal KeyContainerPermissionAccessEntryCollection(KeyContainerPermissionAccessEntry[] entries)
		{
			if (entries != null)
			{
				foreach (KeyContainerPermissionAccessEntry accessEntry in entries)
				{
					this.Add(accessEntry);
				}
			}
		}

		// Token: 0x06003ADC RID: 15068 RVA: 0x000CA02C File Offset: 0x000C822C
		void ICollection.CopyTo(Array array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x06003ADD RID: 15069 RVA: 0x000CA03C File Offset: 0x000C823C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new KeyContainerPermissionAccessEntryEnumerator(this._list);
		}

		// Token: 0x17000B13 RID: 2835
		// (get) Token: 0x06003ADE RID: 15070 RVA: 0x000CA04C File Offset: 0x000C824C
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x17000B14 RID: 2836
		// (get) Token: 0x06003ADF RID: 15071 RVA: 0x000CA05C File Offset: 0x000C825C
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000B15 RID: 2837
		public KeyContainerPermissionAccessEntry this[int index]
		{
			get
			{
				return (KeyContainerPermissionAccessEntry)this._list[index];
			}
		}

		// Token: 0x17000B16 RID: 2838
		// (get) Token: 0x06003AE1 RID: 15073 RVA: 0x000CA074 File Offset: 0x000C8274
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06003AE2 RID: 15074 RVA: 0x000CA078 File Offset: 0x000C8278
		public int Add(KeyContainerPermissionAccessEntry accessEntry)
		{
			return this._list.Add(accessEntry);
		}

		// Token: 0x06003AE3 RID: 15075 RVA: 0x000CA088 File Offset: 0x000C8288
		public void Clear()
		{
			this._list.Clear();
		}

		// Token: 0x06003AE4 RID: 15076 RVA: 0x000CA098 File Offset: 0x000C8298
		public void CopyTo(KeyContainerPermissionAccessEntry[] array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x06003AE5 RID: 15077 RVA: 0x000CA0A8 File Offset: 0x000C82A8
		public KeyContainerPermissionAccessEntryEnumerator GetEnumerator()
		{
			return new KeyContainerPermissionAccessEntryEnumerator(this._list);
		}

		// Token: 0x06003AE6 RID: 15078 RVA: 0x000CA0B8 File Offset: 0x000C82B8
		public int IndexOf(KeyContainerPermissionAccessEntry accessEntry)
		{
			if (accessEntry == null)
			{
				throw new ArgumentNullException("accessEntry");
			}
			for (int i = 0; i < this._list.Count; i++)
			{
				if (accessEntry.Equals(this._list[i]))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06003AE7 RID: 15079 RVA: 0x000CA10C File Offset: 0x000C830C
		public void Remove(KeyContainerPermissionAccessEntry accessEntry)
		{
			if (accessEntry == null)
			{
				throw new ArgumentNullException("accessEntry");
			}
			for (int i = 0; i < this._list.Count; i++)
			{
				if (accessEntry.Equals(this._list[i]))
				{
					this._list.RemoveAt(i);
				}
			}
		}

		// Token: 0x0400199D RID: 6557
		private ArrayList _list;
	}
}
