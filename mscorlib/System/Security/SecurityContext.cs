﻿using System;
using System.Security.Permissions;
using System.Security.Principal;
using System.Threading;

namespace System.Security
{
	// Token: 0x02000540 RID: 1344
	public sealed class SecurityContext
	{
		// Token: 0x060034F1 RID: 13553 RVA: 0x000AE9C4 File Offset: 0x000ACBC4
		internal SecurityContext()
		{
		}

		// Token: 0x060034F2 RID: 13554 RVA: 0x000AE9CC File Offset: 0x000ACBCC
		internal SecurityContext(SecurityContext sc)
		{
			this._capture = true;
			this._winid = sc._winid;
			if (sc._stack != null)
			{
				this._stack = sc._stack.CreateCopy();
			}
		}

		// Token: 0x060034F3 RID: 13555 RVA: 0x000AEA04 File Offset: 0x000ACC04
		public SecurityContext CreateCopy()
		{
			if (!this._capture)
			{
				throw new InvalidOperationException();
			}
			return new SecurityContext(this);
		}

		// Token: 0x060034F4 RID: 13556 RVA: 0x000AEA20 File Offset: 0x000ACC20
		public static SecurityContext Capture()
		{
			SecurityContext securityContext = Thread.CurrentThread.ExecutionContext.SecurityContext;
			if (securityContext.FlowSuppressed)
			{
				return null;
			}
			return new SecurityContext
			{
				_capture = true,
				_winid = WindowsIdentity.GetCurrentToken(),
				_stack = CompressedStack.Capture()
			};
		}

		// Token: 0x170009DF RID: 2527
		// (get) Token: 0x060034F5 RID: 13557 RVA: 0x000AEA70 File Offset: 0x000ACC70
		// (set) Token: 0x060034F6 RID: 13558 RVA: 0x000AEA78 File Offset: 0x000ACC78
		internal bool FlowSuppressed
		{
			get
			{
				return this._suppressFlow;
			}
			set
			{
				this._suppressFlow = value;
			}
		}

		// Token: 0x170009E0 RID: 2528
		// (get) Token: 0x060034F7 RID: 13559 RVA: 0x000AEA84 File Offset: 0x000ACC84
		// (set) Token: 0x060034F8 RID: 13560 RVA: 0x000AEA8C File Offset: 0x000ACC8C
		internal bool WindowsIdentityFlowSuppressed
		{
			get
			{
				return this._suppressFlowWindowsIdentity;
			}
			set
			{
				this._suppressFlowWindowsIdentity = value;
			}
		}

		// Token: 0x170009E1 RID: 2529
		// (get) Token: 0x060034F9 RID: 13561 RVA: 0x000AEA98 File Offset: 0x000ACC98
		// (set) Token: 0x060034FA RID: 13562 RVA: 0x000AEAA0 File Offset: 0x000ACCA0
		internal CompressedStack CompressedStack
		{
			get
			{
				return this._stack;
			}
			set
			{
				this._stack = value;
			}
		}

		// Token: 0x170009E2 RID: 2530
		// (get) Token: 0x060034FB RID: 13563 RVA: 0x000AEAAC File Offset: 0x000ACCAC
		// (set) Token: 0x060034FC RID: 13564 RVA: 0x000AEAB4 File Offset: 0x000ACCB4
		internal IntPtr IdentityToken
		{
			get
			{
				return this._winid;
			}
			set
			{
				this._winid = value;
			}
		}

		// Token: 0x060034FD RID: 13565 RVA: 0x000AEAC0 File Offset: 0x000ACCC0
		public static bool IsFlowSuppressed()
		{
			return Thread.CurrentThread.ExecutionContext.SecurityContext.FlowSuppressed;
		}

		// Token: 0x060034FE RID: 13566 RVA: 0x000AEAD8 File Offset: 0x000ACCD8
		public static bool IsWindowsIdentityFlowSuppressed()
		{
			return Thread.CurrentThread.ExecutionContext.SecurityContext.WindowsIdentityFlowSuppressed;
		}

		// Token: 0x060034FF RID: 13567 RVA: 0x000AEAF0 File Offset: 0x000ACCF0
		public static void RestoreFlow()
		{
			SecurityContext securityContext = Thread.CurrentThread.ExecutionContext.SecurityContext;
			if (!securityContext.FlowSuppressed && !securityContext.WindowsIdentityFlowSuppressed)
			{
				throw new InvalidOperationException();
			}
			securityContext.FlowSuppressed = false;
			securityContext.WindowsIdentityFlowSuppressed = false;
		}

		// Token: 0x06003500 RID: 13568 RVA: 0x000AEB38 File Offset: 0x000ACD38
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
		public static void Run(SecurityContext securityContext, ContextCallback callback, object state)
		{
			if (securityContext == null)
			{
				throw new InvalidOperationException(Locale.GetText("Null SecurityContext"));
			}
			SecurityContext securityContext2 = Thread.CurrentThread.ExecutionContext.SecurityContext;
			IPrincipal currentPrincipal = Thread.CurrentPrincipal;
			try
			{
				if (securityContext2.IdentityToken != IntPtr.Zero)
				{
					Thread.CurrentPrincipal = new WindowsPrincipal(new WindowsIdentity(securityContext2.IdentityToken));
				}
				if (securityContext.CompressedStack != null)
				{
					CompressedStack.Run(securityContext.CompressedStack, callback, state);
				}
				else
				{
					callback(state);
				}
			}
			finally
			{
				if (currentPrincipal != null && securityContext2.IdentityToken != IntPtr.Zero)
				{
					Thread.CurrentPrincipal = currentPrincipal;
				}
			}
		}

		// Token: 0x06003501 RID: 13569 RVA: 0x000AEC04 File Offset: 0x000ACE04
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
		public static AsyncFlowControl SuppressFlow()
		{
			Thread currentThread = Thread.CurrentThread;
			currentThread.ExecutionContext.SecurityContext.FlowSuppressed = true;
			currentThread.ExecutionContext.SecurityContext.WindowsIdentityFlowSuppressed = true;
			return new AsyncFlowControl(currentThread, AsyncFlowControlType.Security);
		}

		// Token: 0x06003502 RID: 13570 RVA: 0x000AEC40 File Offset: 0x000ACE40
		public static AsyncFlowControl SuppressFlowWindowsIdentity()
		{
			Thread currentThread = Thread.CurrentThread;
			currentThread.ExecutionContext.SecurityContext.WindowsIdentityFlowSuppressed = true;
			return new AsyncFlowControl(currentThread, AsyncFlowControlType.Security);
		}

		// Token: 0x0400163B RID: 5691
		private bool _capture;

		// Token: 0x0400163C RID: 5692
		private IntPtr _winid;

		// Token: 0x0400163D RID: 5693
		private CompressedStack _stack;

		// Token: 0x0400163E RID: 5694
		private bool _suppressFlowWindowsIdentity;

		// Token: 0x0400163F RID: 5695
		private bool _suppressFlow;
	}
}
