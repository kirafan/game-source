﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;

namespace System.Security
{
	// Token: 0x02000548 RID: 1352
	[ComVisible(true)]
	[Serializable]
	public class SecurityException : SystemException
	{
		// Token: 0x06003536 RID: 13622 RVA: 0x000AFE50 File Offset: 0x000AE050
		public SecurityException() : base(Locale.GetText("A security error has been detected."))
		{
			base.HResult = -2146233078;
		}

		// Token: 0x06003537 RID: 13623 RVA: 0x000AFE70 File Offset: 0x000AE070
		public SecurityException(string message) : base(message)
		{
			base.HResult = -2146233078;
		}

		// Token: 0x06003538 RID: 13624 RVA: 0x000AFE84 File Offset: 0x000AE084
		protected SecurityException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			base.HResult = -2146233078;
			SerializationInfoEnumerator enumerator = info.GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (enumerator.Name == "PermissionState")
				{
					this.permissionState = (string)enumerator.Value;
					break;
				}
			}
		}

		// Token: 0x06003539 RID: 13625 RVA: 0x000AFEE8 File Offset: 0x000AE0E8
		public SecurityException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233078;
		}

		// Token: 0x0600353A RID: 13626 RVA: 0x000AFF00 File Offset: 0x000AE100
		public SecurityException(string message, Type type) : base(message)
		{
			base.HResult = -2146233078;
			this.permissionType = type;
		}

		// Token: 0x0600353B RID: 13627 RVA: 0x000AFF1C File Offset: 0x000AE11C
		public SecurityException(string message, Type type, string state) : base(message)
		{
			base.HResult = -2146233078;
			this.permissionType = type;
			this.permissionState = state;
		}

		// Token: 0x0600353C RID: 13628 RVA: 0x000AFF4C File Offset: 0x000AE14C
		internal SecurityException(string message, PermissionSet granted, PermissionSet refused) : base(message)
		{
			base.HResult = -2146233078;
			this._granted = granted.ToString();
			this._refused = refused.ToString();
		}

		// Token: 0x0600353D RID: 13629 RVA: 0x000AFF84 File Offset: 0x000AE184
		public SecurityException(string message, object deny, object permitOnly, MethodInfo method, object demanded, IPermission permThatFailed) : base(message)
		{
			base.HResult = -2146233078;
			this._denyset = deny;
			this._permitset = permitOnly;
			this._method = method;
			this._demanded = demanded;
			this._firstperm = permThatFailed;
		}

		// Token: 0x0600353E RID: 13630 RVA: 0x000AFFCC File Offset: 0x000AE1CC
		public SecurityException(string message, AssemblyName assemblyName, PermissionSet grant, PermissionSet refused, MethodInfo method, SecurityAction action, object demanded, IPermission permThatFailed, Evidence evidence) : base(message)
		{
			base.HResult = -2146233078;
			this._assembly = assemblyName;
			this._granted = ((grant != null) ? grant.ToString() : string.Empty);
			this._refused = ((refused != null) ? refused.ToString() : string.Empty);
			this._method = method;
			this._action = action;
			this._demanded = demanded;
			this._firstperm = permThatFailed;
			if (this._firstperm != null)
			{
				this.permissionType = this._firstperm.GetType();
			}
			this._evidence = evidence;
		}

		// Token: 0x170009F1 RID: 2545
		// (get) Token: 0x0600353F RID: 13631 RVA: 0x000B0070 File Offset: 0x000AE270
		// (set) Token: 0x06003540 RID: 13632 RVA: 0x000B0078 File Offset: 0x000AE278
		[ComVisible(false)]
		public SecurityAction Action
		{
			get
			{
				return this._action;
			}
			set
			{
				this._action = value;
			}
		}

		// Token: 0x170009F2 RID: 2546
		// (get) Token: 0x06003541 RID: 13633 RVA: 0x000B0084 File Offset: 0x000AE284
		// (set) Token: 0x06003542 RID: 13634 RVA: 0x000B008C File Offset: 0x000AE28C
		[ComVisible(false)]
		public object DenySetInstance
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this._denyset;
			}
			set
			{
				this._denyset = value;
			}
		}

		// Token: 0x170009F3 RID: 2547
		// (get) Token: 0x06003543 RID: 13635 RVA: 0x000B0098 File Offset: 0x000AE298
		// (set) Token: 0x06003544 RID: 13636 RVA: 0x000B00A0 File Offset: 0x000AE2A0
		[ComVisible(false)]
		public AssemblyName FailedAssemblyInfo
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this._assembly;
			}
			set
			{
				this._assembly = value;
			}
		}

		// Token: 0x170009F4 RID: 2548
		// (get) Token: 0x06003545 RID: 13637 RVA: 0x000B00AC File Offset: 0x000AE2AC
		// (set) Token: 0x06003546 RID: 13638 RVA: 0x000B00B4 File Offset: 0x000AE2B4
		[ComVisible(false)]
		public MethodInfo Method
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this._method;
			}
			set
			{
				this._method = value;
			}
		}

		// Token: 0x170009F5 RID: 2549
		// (get) Token: 0x06003547 RID: 13639 RVA: 0x000B00C0 File Offset: 0x000AE2C0
		// (set) Token: 0x06003548 RID: 13640 RVA: 0x000B00C8 File Offset: 0x000AE2C8
		[ComVisible(false)]
		public object PermitOnlySetInstance
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this._permitset;
			}
			set
			{
				this._permitset = value;
			}
		}

		// Token: 0x170009F6 RID: 2550
		// (get) Token: 0x06003549 RID: 13641 RVA: 0x000B00D4 File Offset: 0x000AE2D4
		// (set) Token: 0x0600354A RID: 13642 RVA: 0x000B00DC File Offset: 0x000AE2DC
		public string Url
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this._url;
			}
			set
			{
				this._url = value;
			}
		}

		// Token: 0x170009F7 RID: 2551
		// (get) Token: 0x0600354B RID: 13643 RVA: 0x000B00E8 File Offset: 0x000AE2E8
		// (set) Token: 0x0600354C RID: 13644 RVA: 0x000B00F0 File Offset: 0x000AE2F0
		public SecurityZone Zone
		{
			get
			{
				return this._zone;
			}
			set
			{
				this._zone = value;
			}
		}

		// Token: 0x170009F8 RID: 2552
		// (get) Token: 0x0600354D RID: 13645 RVA: 0x000B00FC File Offset: 0x000AE2FC
		// (set) Token: 0x0600354E RID: 13646 RVA: 0x000B0104 File Offset: 0x000AE304
		[ComVisible(false)]
		public object Demanded
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this._demanded;
			}
			set
			{
				this._demanded = value;
			}
		}

		// Token: 0x170009F9 RID: 2553
		// (get) Token: 0x0600354F RID: 13647 RVA: 0x000B0110 File Offset: 0x000AE310
		// (set) Token: 0x06003550 RID: 13648 RVA: 0x000B0118 File Offset: 0x000AE318
		public IPermission FirstPermissionThatFailed
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this._firstperm;
			}
			set
			{
				this._firstperm = value;
			}
		}

		// Token: 0x170009FA RID: 2554
		// (get) Token: 0x06003551 RID: 13649 RVA: 0x000B0124 File Offset: 0x000AE324
		// (set) Token: 0x06003552 RID: 13650 RVA: 0x000B012C File Offset: 0x000AE32C
		public string PermissionState
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this.permissionState;
			}
			set
			{
				this.permissionState = value;
			}
		}

		// Token: 0x170009FB RID: 2555
		// (get) Token: 0x06003553 RID: 13651 RVA: 0x000B0138 File Offset: 0x000AE338
		// (set) Token: 0x06003554 RID: 13652 RVA: 0x000B0140 File Offset: 0x000AE340
		public Type PermissionType
		{
			get
			{
				return this.permissionType;
			}
			set
			{
				this.permissionType = value;
			}
		}

		// Token: 0x170009FC RID: 2556
		// (get) Token: 0x06003555 RID: 13653 RVA: 0x000B014C File Offset: 0x000AE34C
		// (set) Token: 0x06003556 RID: 13654 RVA: 0x000B0154 File Offset: 0x000AE354
		public string GrantedSet
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this._granted;
			}
			set
			{
				this._granted = value;
			}
		}

		// Token: 0x170009FD RID: 2557
		// (get) Token: 0x06003557 RID: 13655 RVA: 0x000B0160 File Offset: 0x000AE360
		// (set) Token: 0x06003558 RID: 13656 RVA: 0x000B0168 File Offset: 0x000AE368
		public string RefusedSet
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this._refused;
			}
			set
			{
				this._refused = value;
			}
		}

		// Token: 0x06003559 RID: 13657 RVA: 0x000B0174 File Offset: 0x000AE374
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			try
			{
				info.AddValue("PermissionState", this.PermissionState);
			}
			catch (SecurityException)
			{
			}
		}

		// Token: 0x0600355A RID: 13658 RVA: 0x000B01C4 File Offset: 0x000AE3C4
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(base.ToString());
			try
			{
				if (this.permissionType != null)
				{
					stringBuilder.AppendFormat("{0}Type: {1}", Environment.NewLine, this.PermissionType);
				}
				if (this._method != null)
				{
					string text = this._method.ToString();
					int startIndex = text.IndexOf(" ") + 1;
					stringBuilder.AppendFormat("{0}Method: {1} {2}.{3}", new object[]
					{
						Environment.NewLine,
						this._method.ReturnType.Name,
						this._method.ReflectedType,
						text.Substring(startIndex)
					});
				}
				if (this.permissionState != null)
				{
					stringBuilder.AppendFormat("{0}State: {1}", Environment.NewLine, this.PermissionState);
				}
				if (this._granted != null && this._granted.Length > 0)
				{
					stringBuilder.AppendFormat("{0}Granted: {1}", Environment.NewLine, this.GrantedSet);
				}
				if (this._refused != null && this._refused.Length > 0)
				{
					stringBuilder.AppendFormat("{0}Refused: {1}", Environment.NewLine, this.RefusedSet);
				}
				if (this._demanded != null)
				{
					stringBuilder.AppendFormat("{0}Demanded: {1}", Environment.NewLine, this.Demanded);
				}
				if (this._firstperm != null)
				{
					stringBuilder.AppendFormat("{0}Failed Permission: {1}", Environment.NewLine, this.FirstPermissionThatFailed);
				}
				if (this._evidence != null)
				{
					stringBuilder.AppendFormat("{0}Evidences:", Environment.NewLine);
					foreach (object obj in this._evidence)
					{
						if (!(obj is Hash))
						{
							stringBuilder.AppendFormat("{0}\t{1}", Environment.NewLine, obj);
						}
					}
				}
			}
			catch (SecurityException)
			{
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0400165C RID: 5724
		private string permissionState;

		// Token: 0x0400165D RID: 5725
		private Type permissionType;

		// Token: 0x0400165E RID: 5726
		private string _granted;

		// Token: 0x0400165F RID: 5727
		private string _refused;

		// Token: 0x04001660 RID: 5728
		private object _demanded;

		// Token: 0x04001661 RID: 5729
		private IPermission _firstperm;

		// Token: 0x04001662 RID: 5730
		private MethodInfo _method;

		// Token: 0x04001663 RID: 5731
		private Evidence _evidence;

		// Token: 0x04001664 RID: 5732
		private SecurityAction _action;

		// Token: 0x04001665 RID: 5733
		private object _denyset;

		// Token: 0x04001666 RID: 5734
		private object _permitset;

		// Token: 0x04001667 RID: 5735
		private AssemblyName _assembly;

		// Token: 0x04001668 RID: 5736
		private string _url;

		// Token: 0x04001669 RID: 5737
		private SecurityZone _zone;
	}
}
