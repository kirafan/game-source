﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	// Token: 0x02000531 RID: 1329
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	public sealed class AllowPartiallyTrustedCallersAttribute : Attribute
	{
	}
}
