﻿using System;

namespace System.Security
{
	// Token: 0x0200054D RID: 1357
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	[MonoTODO("Only supported by the runtime when CoreCLR is enabled")]
	public sealed class SecurityTransparentAttribute : Attribute
	{
	}
}
