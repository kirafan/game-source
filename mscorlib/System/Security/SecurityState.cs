﻿using System;

namespace System.Security
{
	// Token: 0x0200054C RID: 1356
	public abstract class SecurityState
	{
		// Token: 0x06003587 RID: 13703
		public abstract void EnsureState();

		// Token: 0x06003588 RID: 13704 RVA: 0x000B1348 File Offset: 0x000AF548
		public bool IsStateAvailable()
		{
			AppDomainManager domainManager = AppDomain.CurrentDomain.DomainManager;
			return domainManager != null && domainManager.CheckSecuritySettings(this);
		}
	}
}
