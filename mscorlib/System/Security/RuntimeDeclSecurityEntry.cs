﻿using System;

namespace System.Security
{
	// Token: 0x02000545 RID: 1349
	internal struct RuntimeDeclSecurityEntry
	{
		// Token: 0x0400164F RID: 5711
		public IntPtr blob;

		// Token: 0x04001650 RID: 5712
		public int size;

		// Token: 0x04001651 RID: 5713
		public int index;
	}
}
