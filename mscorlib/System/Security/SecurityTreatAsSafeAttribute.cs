﻿using System;

namespace System.Security
{
	// Token: 0x0200054E RID: 1358
	[AttributeUsage(AttributeTargets.All, AllowMultiple = false, Inherited = false)]
	[MonoTODO("Not supported by the runtime")]
	public sealed class SecurityTreatAsSafeAttribute : Attribute
	{
	}
}
