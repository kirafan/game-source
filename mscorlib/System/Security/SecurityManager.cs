﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;

namespace System.Security
{
	// Token: 0x0200054A RID: 1354
	[ComVisible(true)]
	public static class SecurityManager
	{
		// Token: 0x0600355B RID: 13659 RVA: 0x000B03F4 File Offset: 0x000AE5F4
		static SecurityManager()
		{
			SecurityManager._lockObject = new object();
		}

		// Token: 0x170009FE RID: 2558
		// (get) Token: 0x0600355C RID: 13660
		// (set) Token: 0x0600355D RID: 13661
		public static extern bool CheckExecutionRights { [MethodImpl(MethodImplOptions.InternalCall)] get; [PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x170009FF RID: 2559
		// (get) Token: 0x0600355E RID: 13662
		// (set) Token: 0x0600355F RID: 13663
		[Obsolete("The security manager cannot be turned off on MS runtime")]
		public static extern bool SecurityEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; [PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")] [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x06003560 RID: 13664 RVA: 0x000B040C File Offset: 0x000AE60C
		[MonoTODO("CAS support is experimental (and unsupported). This method only works in FullTrust.")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.StrongNameIdentityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                PublicKeyBlob=\"2100000000000000000400000000000000\"/>\n</PermissionSet>\n")]
		public static void GetZoneAndOrigin(out ArrayList zone, out ArrayList origin)
		{
			zone = new ArrayList();
			origin = new ArrayList();
		}

		// Token: 0x06003561 RID: 13665 RVA: 0x000B041C File Offset: 0x000AE61C
		public static bool IsGranted(IPermission perm)
		{
			return perm == null || !SecurityManager.SecurityEnabled || SecurityManager.IsGranted(Assembly.GetCallingAssembly(), perm);
		}

		// Token: 0x06003562 RID: 13666 RVA: 0x000B0440 File Offset: 0x000AE640
		internal static bool IsGranted(Assembly a, IPermission perm)
		{
			PermissionSet grantedPermissionSet = a.GrantedPermissionSet;
			if (grantedPermissionSet != null && !grantedPermissionSet.IsUnrestricted())
			{
				CodeAccessPermission target = (CodeAccessPermission)grantedPermissionSet.GetPermission(perm.GetType());
				if (!perm.IsSubsetOf(target))
				{
					return false;
				}
			}
			PermissionSet deniedPermissionSet = a.DeniedPermissionSet;
			if (deniedPermissionSet != null && !deniedPermissionSet.IsEmpty())
			{
				if (deniedPermissionSet.IsUnrestricted())
				{
					return false;
				}
				CodeAccessPermission codeAccessPermission = (CodeAccessPermission)a.DeniedPermissionSet.GetPermission(perm.GetType());
				if (codeAccessPermission != null && perm.IsSubsetOf(codeAccessPermission))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06003563 RID: 13667 RVA: 0x000B04D8 File Offset: 0x000AE6D8
		internal static IPermission CheckPermissionSet(Assembly a, PermissionSet ps, bool noncas)
		{
			if (ps.IsEmpty())
			{
				return null;
			}
			foreach (object obj in ps)
			{
				IPermission permission = (IPermission)obj;
				if (!noncas && permission is CodeAccessPermission)
				{
					if (!SecurityManager.IsGranted(a, permission))
					{
						return permission;
					}
				}
				else
				{
					try
					{
						permission.Demand();
					}
					catch (SecurityException)
					{
						return permission;
					}
				}
			}
			return null;
		}

		// Token: 0x06003564 RID: 13668 RVA: 0x000B05A4 File Offset: 0x000AE7A4
		internal static IPermission CheckPermissionSet(AppDomain ad, PermissionSet ps)
		{
			if (ps == null || ps.IsEmpty())
			{
				return null;
			}
			PermissionSet grantedPermissionSet = ad.GrantedPermissionSet;
			if (grantedPermissionSet == null)
			{
				return null;
			}
			if (grantedPermissionSet.IsUnrestricted())
			{
				return null;
			}
			if (ps.IsUnrestricted())
			{
				return new SecurityPermission(SecurityPermissionFlag.NoFlags);
			}
			foreach (object obj in ps)
			{
				IPermission permission = (IPermission)obj;
				if (permission is CodeAccessPermission)
				{
					CodeAccessPermission codeAccessPermission = (CodeAccessPermission)grantedPermissionSet.GetPermission(permission.GetType());
					if (codeAccessPermission == null)
					{
						if ((!grantedPermissionSet.IsUnrestricted() || !(permission is IUnrestrictedPermission)) && !permission.IsSubsetOf(null))
						{
							return permission;
						}
					}
					else if (!permission.IsSubsetOf(codeAccessPermission))
					{
						return permission;
					}
				}
				else
				{
					try
					{
						permission.Demand();
					}
					catch (SecurityException)
					{
						return permission;
					}
				}
			}
			return null;
		}

		// Token: 0x06003565 RID: 13669 RVA: 0x000B06EC File Offset: 0x000AE8EC
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
		public static PolicyLevel LoadPolicyLevelFromFile(string path, PolicyLevelType type)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			PolicyLevel policyLevel = null;
			try
			{
				policyLevel = new PolicyLevel(type.ToString(), type);
				policyLevel.LoadFromFile(path);
			}
			catch (Exception innerException)
			{
				throw new ArgumentException(Locale.GetText("Invalid policy XML"), innerException);
			}
			return policyLevel;
		}

		// Token: 0x06003566 RID: 13670 RVA: 0x000B0760 File Offset: 0x000AE960
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
		public static PolicyLevel LoadPolicyLevelFromString(string str, PolicyLevelType type)
		{
			if (str == null)
			{
				throw new ArgumentNullException("str");
			}
			PolicyLevel policyLevel = null;
			try
			{
				policyLevel = new PolicyLevel(type.ToString(), type);
				policyLevel.LoadFromString(str);
			}
			catch (Exception innerException)
			{
				throw new ArgumentException(Locale.GetText("Invalid policy XML"), innerException);
			}
			return policyLevel;
		}

		// Token: 0x06003567 RID: 13671 RVA: 0x000B07D4 File Offset: 0x000AE9D4
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
		public static IEnumerator PolicyHierarchy()
		{
			return SecurityManager.Hierarchy;
		}

		// Token: 0x06003568 RID: 13672 RVA: 0x000B07DC File Offset: 0x000AE9DC
		public static PermissionSet ResolvePolicy(Evidence evidence)
		{
			if (evidence == null)
			{
				return new PermissionSet(PermissionState.None);
			}
			PermissionSet permissionSet = null;
			IEnumerator hierarchy = SecurityManager.Hierarchy;
			while (hierarchy.MoveNext())
			{
				object obj = hierarchy.Current;
				PolicyLevel pl = (PolicyLevel)obj;
				if (SecurityManager.ResolvePolicyLevel(ref permissionSet, pl, evidence))
				{
					break;
				}
			}
			SecurityManager.ResolveIdentityPermissions(permissionSet, evidence);
			return permissionSet;
		}

		// Token: 0x06003569 RID: 13673 RVA: 0x000B0838 File Offset: 0x000AEA38
		[MonoTODO("(2.0) more tests are needed")]
		public static PermissionSet ResolvePolicy(Evidence[] evidences)
		{
			if (evidences == null || evidences.Length == 0 || (evidences.Length == 1 && evidences[0].Count == 0))
			{
				return new PermissionSet(PermissionState.None);
			}
			PermissionSet permissionSet = SecurityManager.ResolvePolicy(evidences[0]);
			for (int i = 1; i < evidences.Length; i++)
			{
				permissionSet = permissionSet.Intersect(SecurityManager.ResolvePolicy(evidences[i]));
			}
			return permissionSet;
		}

		// Token: 0x0600356A RID: 13674 RVA: 0x000B08A0 File Offset: 0x000AEAA0
		public static PermissionSet ResolveSystemPolicy(Evidence evidence)
		{
			if (evidence == null)
			{
				return new PermissionSet(PermissionState.None);
			}
			PermissionSet permissionSet = null;
			IEnumerator hierarchy = SecurityManager.Hierarchy;
			while (hierarchy.MoveNext())
			{
				object obj = hierarchy.Current;
				PolicyLevel policyLevel = (PolicyLevel)obj;
				if (policyLevel.Type == PolicyLevelType.AppDomain)
				{
					break;
				}
				if (SecurityManager.ResolvePolicyLevel(ref permissionSet, policyLevel, evidence))
				{
					break;
				}
			}
			SecurityManager.ResolveIdentityPermissions(permissionSet, evidence);
			return permissionSet;
		}

		// Token: 0x0600356B RID: 13675 RVA: 0x000B090C File Offset: 0x000AEB0C
		public static PermissionSet ResolvePolicy(Evidence evidence, PermissionSet reqdPset, PermissionSet optPset, PermissionSet denyPset, out PermissionSet denied)
		{
			PermissionSet permissionSet = SecurityManager.ResolvePolicy(evidence);
			if (reqdPset != null && !reqdPset.IsSubsetOf(permissionSet))
			{
				throw new PolicyException(Locale.GetText("Policy doesn't grant the minimal permissions required to execute the assembly."));
			}
			if (SecurityManager.CheckExecutionRights)
			{
				bool flag = false;
				if (permissionSet != null)
				{
					if (permissionSet.IsUnrestricted())
					{
						flag = true;
					}
					else
					{
						IPermission permission = permissionSet.GetPermission(typeof(SecurityPermission));
						flag = SecurityManager._execution.IsSubsetOf(permission);
					}
				}
				if (!flag)
				{
					throw new PolicyException(Locale.GetText("Policy doesn't grant the right to execute the assembly."));
				}
			}
			denied = denyPset;
			return permissionSet;
		}

		// Token: 0x0600356C RID: 13676 RVA: 0x000B09A0 File Offset: 0x000AEBA0
		public static IEnumerator ResolvePolicyGroups(Evidence evidence)
		{
			if (evidence == null)
			{
				throw new ArgumentNullException("evidence");
			}
			ArrayList arrayList = new ArrayList();
			IEnumerator hierarchy = SecurityManager.Hierarchy;
			while (hierarchy.MoveNext())
			{
				object obj = hierarchy.Current;
				PolicyLevel policyLevel = (PolicyLevel)obj;
				CodeGroup value = policyLevel.ResolveMatchingCodeGroups(evidence);
				arrayList.Add(value);
			}
			return arrayList.GetEnumerator();
		}

		// Token: 0x0600356D RID: 13677 RVA: 0x000B09FC File Offset: 0x000AEBFC
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
		public static void SavePolicy()
		{
			IEnumerator hierarchy = SecurityManager.Hierarchy;
			while (hierarchy.MoveNext())
			{
				object obj = hierarchy.Current;
				PolicyLevel policyLevel = obj as PolicyLevel;
				policyLevel.Save();
			}
		}

		// Token: 0x0600356E RID: 13678 RVA: 0x000B0A34 File Offset: 0x000AEC34
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
		public static void SavePolicyLevel(PolicyLevel level)
		{
			level.Save();
		}

		// Token: 0x17000A00 RID: 2560
		// (get) Token: 0x0600356F RID: 13679 RVA: 0x000B0A3C File Offset: 0x000AEC3C
		private static IEnumerator Hierarchy
		{
			get
			{
				object lockObject = SecurityManager._lockObject;
				lock (lockObject)
				{
					if (SecurityManager._hierarchy == null)
					{
						SecurityManager.InitializePolicyHierarchy();
					}
				}
				return SecurityManager._hierarchy.GetEnumerator();
			}
		}

		// Token: 0x06003570 RID: 13680 RVA: 0x000B0A98 File Offset: 0x000AEC98
		private static void InitializePolicyHierarchy()
		{
			string directoryName = Path.GetDirectoryName(Environment.GetMachineConfigPath());
			string path = Path.Combine(Environment.InternalGetFolderPath(Environment.SpecialFolder.ApplicationData), "mono");
			PolicyLevel policyLevel = new PolicyLevel("Enterprise", PolicyLevelType.Enterprise);
			SecurityManager._level = policyLevel;
			policyLevel.LoadFromFile(Path.Combine(directoryName, "enterprisesec.config"));
			PolicyLevel policyLevel2 = new PolicyLevel("Machine", PolicyLevelType.Machine);
			SecurityManager._level = policyLevel2;
			policyLevel2.LoadFromFile(Path.Combine(directoryName, "security.config"));
			PolicyLevel policyLevel3 = new PolicyLevel("User", PolicyLevelType.User);
			SecurityManager._level = policyLevel3;
			policyLevel3.LoadFromFile(Path.Combine(path, "security.config"));
			SecurityManager._hierarchy = ArrayList.Synchronized(new ArrayList
			{
				policyLevel,
				policyLevel2,
				policyLevel3
			});
			SecurityManager._level = null;
		}

		// Token: 0x06003571 RID: 13681 RVA: 0x000B0B64 File Offset: 0x000AED64
		internal static bool ResolvePolicyLevel(ref PermissionSet ps, PolicyLevel pl, Evidence evidence)
		{
			PolicyStatement policyStatement = pl.Resolve(evidence);
			if (policyStatement != null)
			{
				if (ps == null)
				{
					ps = policyStatement.PermissionSet;
				}
				else
				{
					ps = ps.Intersect(policyStatement.PermissionSet);
					if (ps == null)
					{
						ps = new PermissionSet(PermissionState.None);
					}
				}
				if ((policyStatement.Attributes & PolicyStatementAttribute.LevelFinal) == PolicyStatementAttribute.LevelFinal)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06003572 RID: 13682 RVA: 0x000B0BC4 File Offset: 0x000AEDC4
		internal static void ResolveIdentityPermissions(PermissionSet ps, Evidence evidence)
		{
			if (ps.IsUnrestricted())
			{
				return;
			}
			IEnumerator hostEnumerator = evidence.GetHostEnumerator();
			while (hostEnumerator.MoveNext())
			{
				object obj = hostEnumerator.Current;
				IIdentityPermissionFactory identityPermissionFactory = obj as IIdentityPermissionFactory;
				if (identityPermissionFactory != null)
				{
					IPermission perm = identityPermissionFactory.CreateIdentityPermission(evidence);
					ps.AddPermission(perm);
				}
			}
		}

		// Token: 0x17000A01 RID: 2561
		// (get) Token: 0x06003573 RID: 13683 RVA: 0x000B0C18 File Offset: 0x000AEE18
		// (set) Token: 0x06003574 RID: 13684 RVA: 0x000B0C20 File Offset: 0x000AEE20
		internal static PolicyLevel ResolvingPolicyLevel
		{
			get
			{
				return SecurityManager._level;
			}
			set
			{
				SecurityManager._level = value;
			}
		}

		// Token: 0x06003575 RID: 13685 RVA: 0x000B0C28 File Offset: 0x000AEE28
		internal static PermissionSet Decode(IntPtr permissions, int length)
		{
			PermissionSet permissionSet = null;
			object lockObject = SecurityManager._lockObject;
			lock (lockObject)
			{
				if (SecurityManager._declsecCache == null)
				{
					SecurityManager._declsecCache = new Hashtable();
				}
				object key = (int)permissions;
				permissionSet = (PermissionSet)SecurityManager._declsecCache[key];
				if (permissionSet == null)
				{
					byte[] array = new byte[length];
					Marshal.Copy(permissions, array, 0, length);
					permissionSet = SecurityManager.Decode(array);
					permissionSet.DeclarativeSecurity = true;
					SecurityManager._declsecCache.Add(key, permissionSet);
				}
			}
			return permissionSet;
		}

		// Token: 0x06003576 RID: 13686 RVA: 0x000B0CD0 File Offset: 0x000AEED0
		internal static PermissionSet Decode(byte[] encodedPermissions)
		{
			if (encodedPermissions == null || encodedPermissions.Length < 1)
			{
				throw new SecurityException("Invalid metadata format.");
			}
			byte b = encodedPermissions[0];
			if (b == 46)
			{
				return PermissionSet.CreateFromBinaryFormat(encodedPermissions);
			}
			if (b != 60)
			{
				throw new SecurityException(Locale.GetText("Unknown metadata format."));
			}
			string @string = Encoding.Unicode.GetString(encodedPermissions);
			return new PermissionSet(@string);
		}

		// Token: 0x17000A02 RID: 2562
		// (get) Token: 0x06003577 RID: 13687 RVA: 0x000B0D3C File Offset: 0x000AEF3C
		private static IPermission UnmanagedCode
		{
			get
			{
				object lockObject = SecurityManager._lockObject;
				lock (lockObject)
				{
					if (SecurityManager._unmanagedCode == null)
					{
						SecurityManager._unmanagedCode = new SecurityPermission(SecurityPermissionFlag.UnmanagedCode);
					}
				}
				return SecurityManager._unmanagedCode;
			}
		}

		// Token: 0x06003578 RID: 13688
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern bool GetLinkDemandSecurity(MethodBase method, RuntimeDeclSecurityActions* cdecl, RuntimeDeclSecurityActions* mdecl);

		// Token: 0x06003579 RID: 13689 RVA: 0x000B0D98 File Offset: 0x000AEF98
		internal unsafe static void ReflectedLinkDemandInvoke(MethodBase mb)
		{
			RuntimeDeclSecurityActions runtimeDeclSecurityActions;
			RuntimeDeclSecurityActions runtimeDeclSecurityActions2;
			if (!SecurityManager.GetLinkDemandSecurity(mb, &runtimeDeclSecurityActions, &runtimeDeclSecurityActions2))
			{
				return;
			}
			PermissionSet permissionSet = null;
			if (runtimeDeclSecurityActions.cas.size > 0)
			{
				permissionSet = SecurityManager.Decode(runtimeDeclSecurityActions.cas.blob, runtimeDeclSecurityActions.cas.size);
			}
			if (runtimeDeclSecurityActions.noncas.size > 0)
			{
				PermissionSet permissionSet2 = SecurityManager.Decode(runtimeDeclSecurityActions.noncas.blob, runtimeDeclSecurityActions.noncas.size);
				permissionSet = ((permissionSet != null) ? permissionSet.Union(permissionSet2) : permissionSet2);
			}
			if (runtimeDeclSecurityActions2.cas.size > 0)
			{
				PermissionSet permissionSet3 = SecurityManager.Decode(runtimeDeclSecurityActions2.cas.blob, runtimeDeclSecurityActions2.cas.size);
				permissionSet = ((permissionSet != null) ? permissionSet.Union(permissionSet3) : permissionSet3);
			}
			if (runtimeDeclSecurityActions2.noncas.size > 0)
			{
				PermissionSet permissionSet4 = SecurityManager.Decode(runtimeDeclSecurityActions2.noncas.blob, runtimeDeclSecurityActions2.noncas.size);
				permissionSet = ((permissionSet != null) ? permissionSet.Union(permissionSet4) : permissionSet4);
			}
			if (permissionSet != null)
			{
				permissionSet.Demand();
			}
		}

		// Token: 0x0600357A RID: 13690 RVA: 0x000B0EC8 File Offset: 0x000AF0C8
		internal unsafe static bool ReflectedLinkDemandQuery(MethodBase mb)
		{
			RuntimeDeclSecurityActions runtimeDeclSecurityActions;
			RuntimeDeclSecurityActions runtimeDeclSecurityActions2;
			return !SecurityManager.GetLinkDemandSecurity(mb, &runtimeDeclSecurityActions, &runtimeDeclSecurityActions2) || SecurityManager.LinkDemand(mb.ReflectedType.Assembly, &runtimeDeclSecurityActions, &runtimeDeclSecurityActions2);
		}

		// Token: 0x0600357B RID: 13691 RVA: 0x000B0EFC File Offset: 0x000AF0FC
		private unsafe static bool LinkDemand(Assembly a, RuntimeDeclSecurityActions* klass, RuntimeDeclSecurityActions* method)
		{
			bool result;
			try
			{
				bool flag = true;
				if (klass->cas.size > 0)
				{
					PermissionSet ps = SecurityManager.Decode(klass->cas.blob, klass->cas.size);
					flag = (SecurityManager.CheckPermissionSet(a, ps, false) == null);
				}
				if (flag && klass->noncas.size > 0)
				{
					PermissionSet ps = SecurityManager.Decode(klass->noncas.blob, klass->noncas.size);
					flag = (SecurityManager.CheckPermissionSet(a, ps, true) == null);
				}
				if (flag && method->cas.size > 0)
				{
					PermissionSet ps = SecurityManager.Decode(method->cas.blob, method->cas.size);
					flag = (SecurityManager.CheckPermissionSet(a, ps, false) == null);
				}
				if (flag && method->noncas.size > 0)
				{
					PermissionSet ps = SecurityManager.Decode(method->noncas.blob, method->noncas.size);
					flag = (SecurityManager.CheckPermissionSet(a, ps, true) == null);
				}
				result = flag;
			}
			catch (SecurityException)
			{
				result = false;
			}
			return result;
		}

		// Token: 0x0600357C RID: 13692 RVA: 0x000B103C File Offset: 0x000AF23C
		private static bool LinkDemandFullTrust(Assembly a)
		{
			PermissionSet grantedPermissionSet = a.GrantedPermissionSet;
			if (grantedPermissionSet != null && !grantedPermissionSet.IsUnrestricted())
			{
				return false;
			}
			PermissionSet deniedPermissionSet = a.DeniedPermissionSet;
			return deniedPermissionSet == null || deniedPermissionSet.IsEmpty();
		}

		// Token: 0x0600357D RID: 13693 RVA: 0x000B1080 File Offset: 0x000AF280
		private static bool LinkDemandUnmanaged(Assembly a)
		{
			return SecurityManager.IsGranted(a, SecurityManager.UnmanagedCode);
		}

		// Token: 0x0600357E RID: 13694 RVA: 0x000B1090 File Offset: 0x000AF290
		private static void LinkDemandSecurityException(int securityViolation, IntPtr methodHandle)
		{
			RuntimeMethodHandle handle = new RuntimeMethodHandle(methodHandle);
			MethodInfo methodInfo = (MethodInfo)MethodBase.GetMethodFromHandle(handle);
			Assembly assembly = methodInfo.DeclaringType.Assembly;
			AssemblyName assemblyName = null;
			PermissionSet grant = null;
			PermissionSet refused = null;
			object demanded = null;
			IPermission permThatFailed = null;
			if (assembly != null)
			{
				assemblyName = assembly.UnprotectedGetName();
				grant = assembly.GrantedPermissionSet;
				refused = assembly.DeniedPermissionSet;
			}
			string text;
			switch (securityViolation)
			{
			case 1:
				text = Locale.GetText("Permissions refused to call this method.");
				goto IL_E5;
			case 2:
				text = Locale.GetText("Partially trusted callers aren't allowed to call into this assembly.");
				demanded = DefaultPolicies.FullTrust;
				goto IL_E5;
			case 4:
				text = Locale.GetText("Calling internal calls is restricted to ECMA signed assemblies.");
				goto IL_E5;
			case 8:
				text = Locale.GetText("Calling unmanaged code isn't allowed from this assembly.");
				demanded = SecurityManager._unmanagedCode;
				permThatFailed = SecurityManager._unmanagedCode;
				goto IL_E5;
			}
			text = Locale.GetText("JIT time LinkDemand failed.");
			IL_E5:
			throw new SecurityException(text, assemblyName, grant, refused, methodInfo, SecurityAction.LinkDemand, demanded, permThatFailed, null);
		}

		// Token: 0x0600357F RID: 13695 RVA: 0x000B1198 File Offset: 0x000AF398
		private static void InheritanceDemandSecurityException(int securityViolation, Assembly a, Type t, MethodInfo method)
		{
			AssemblyName assemblyName = null;
			PermissionSet grant = null;
			PermissionSet refused = null;
			if (a != null)
			{
				assemblyName = a.UnprotectedGetName();
				grant = a.GrantedPermissionSet;
				refused = a.DeniedPermissionSet;
			}
			string message;
			if (securityViolation != 1)
			{
				if (securityViolation != 2)
				{
					message = Locale.GetText("Load time InheritDemand failed.");
				}
				else
				{
					message = Locale.GetText("Method override refused.");
				}
			}
			else
			{
				message = string.Format(Locale.GetText("Class inheritance refused for {0}."), t);
			}
			throw new SecurityException(message, assemblyName, grant, refused, method, SecurityAction.InheritanceDemand, null, null, null);
		}

		// Token: 0x06003580 RID: 13696 RVA: 0x000B1224 File Offset: 0x000AF424
		private static void ThrowException(Exception ex)
		{
			throw ex;
		}

		// Token: 0x06003581 RID: 13697 RVA: 0x000B1228 File Offset: 0x000AF428
		private unsafe static bool InheritanceDemand(AppDomain ad, Assembly a, RuntimeDeclSecurityActions* actions)
		{
			bool result;
			try
			{
				bool flag = true;
				if (actions->cas.size > 0)
				{
					PermissionSet ps = SecurityManager.Decode(actions->cas.blob, actions->cas.size);
					flag = (SecurityManager.CheckPermissionSet(a, ps, false) == null);
					if (flag)
					{
						flag = (SecurityManager.CheckPermissionSet(ad, ps) == null);
					}
				}
				if (actions->noncas.size > 0)
				{
					PermissionSet ps = SecurityManager.Decode(actions->noncas.blob, actions->noncas.size);
					flag = (SecurityManager.CheckPermissionSet(a, ps, true) == null);
					if (flag)
					{
						flag = (SecurityManager.CheckPermissionSet(ad, ps) == null);
					}
				}
				result = flag;
			}
			catch (SecurityException)
			{
				result = false;
			}
			return result;
		}

		// Token: 0x06003582 RID: 13698 RVA: 0x000B1304 File Offset: 0x000AF504
		private static void DemandUnmanaged()
		{
			SecurityManager.UnmanagedCode.Demand();
		}

		// Token: 0x06003583 RID: 13699 RVA: 0x000B1310 File Offset: 0x000AF510
		private static void InternalDemand(IntPtr permissions, int length)
		{
			PermissionSet permissionSet = SecurityManager.Decode(permissions, length);
			permissionSet.Demand();
		}

		// Token: 0x06003584 RID: 13700 RVA: 0x000B132C File Offset: 0x000AF52C
		private static void InternalDemandChoice(IntPtr permissions, int length)
		{
			throw new SecurityException("SecurityAction.DemandChoice was removed from 2.0");
		}

		// Token: 0x0400166D RID: 5741
		private static object _lockObject;

		// Token: 0x0400166E RID: 5742
		private static ArrayList _hierarchy;

		// Token: 0x0400166F RID: 5743
		private static IPermission _unmanagedCode;

		// Token: 0x04001670 RID: 5744
		private static Hashtable _declsecCache;

		// Token: 0x04001671 RID: 5745
		private static PolicyLevel _level;

		// Token: 0x04001672 RID: 5746
		private static SecurityPermission _execution = new SecurityPermission(SecurityPermissionFlag.Execution);
	}
}
