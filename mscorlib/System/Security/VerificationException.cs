﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Security
{
	// Token: 0x02000552 RID: 1362
	[ComVisible(true)]
	[Serializable]
	public class VerificationException : SystemException
	{
		// Token: 0x0600358D RID: 13709 RVA: 0x000B1390 File Offset: 0x000AF590
		public VerificationException()
		{
		}

		// Token: 0x0600358E RID: 13710 RVA: 0x000B1398 File Offset: 0x000AF598
		public VerificationException(string message) : base(message)
		{
		}

		// Token: 0x0600358F RID: 13711 RVA: 0x000B13A4 File Offset: 0x000AF5A4
		protected VerificationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06003590 RID: 13712 RVA: 0x000B13B0 File Offset: 0x000AF5B0
		public VerificationException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
