﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	// Token: 0x0200054F RID: 1359
	[ComVisible(true)]
	[Serializable]
	public enum SecurityZone
	{
		// Token: 0x04001674 RID: 5748
		MyComputer,
		// Token: 0x04001675 RID: 5749
		Intranet,
		// Token: 0x04001676 RID: 5750
		Trusted,
		// Token: 0x04001677 RID: 5751
		Internet,
		// Token: 0x04001678 RID: 5752
		Untrusted,
		// Token: 0x04001679 RID: 5753
		NoZone = -1
	}
}
