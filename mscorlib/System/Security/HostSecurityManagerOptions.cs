﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	// Token: 0x02000535 RID: 1333
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum HostSecurityManagerOptions
	{
		// Token: 0x0400161A RID: 5658
		None = 0,
		// Token: 0x0400161B RID: 5659
		HostAppDomainEvidence = 1,
		// Token: 0x0400161C RID: 5660
		HostPolicyLevel = 2,
		// Token: 0x0400161D RID: 5661
		HostAssemblyEvidence = 4,
		// Token: 0x0400161E RID: 5662
		HostDetermineApplicationTrust = 8,
		// Token: 0x0400161F RID: 5663
		HostResolvePolicy = 16,
		// Token: 0x04001620 RID: 5664
		AllFlags = 31
	}
}
