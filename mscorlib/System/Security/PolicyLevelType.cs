﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	// Token: 0x0200053E RID: 1342
	[ComVisible(true)]
	[Serializable]
	public enum PolicyLevelType
	{
		// Token: 0x04001631 RID: 5681
		User,
		// Token: 0x04001632 RID: 5682
		Machine,
		// Token: 0x04001633 RID: 5683
		Enterprise,
		// Token: 0x04001634 RID: 5684
		AppDomain
	}
}
