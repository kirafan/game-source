﻿using System;
using System.Reflection;
using System.Runtime.Hosting;
using System.Runtime.InteropServices;
using System.Security.Policy;

namespace System.Security
{
	// Token: 0x02000534 RID: 1332
	[ComVisible(true)]
	[Serializable]
	public class HostSecurityManager
	{
		// Token: 0x170009D3 RID: 2515
		// (get) Token: 0x06003485 RID: 13445 RVA: 0x000AC6E0 File Offset: 0x000AA8E0
		public virtual PolicyLevel DomainPolicy
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170009D4 RID: 2516
		// (get) Token: 0x06003486 RID: 13446 RVA: 0x000AC6E4 File Offset: 0x000AA8E4
		public virtual HostSecurityManagerOptions Flags
		{
			get
			{
				return HostSecurityManagerOptions.AllFlags;
			}
		}

		// Token: 0x06003487 RID: 13447 RVA: 0x000AC6E8 File Offset: 0x000AA8E8
		public virtual ApplicationTrust DetermineApplicationTrust(Evidence applicationEvidence, Evidence activatorEvidence, TrustManagerContext context)
		{
			if (applicationEvidence == null)
			{
				throw new ArgumentNullException("applicationEvidence");
			}
			ActivationArguments activationArguments = null;
			foreach (object obj in applicationEvidence)
			{
				activationArguments = (obj as ActivationArguments);
				if (activationArguments != null)
				{
					break;
				}
			}
			if (activationArguments == null)
			{
				string text = Locale.GetText("No {0} found in {1}.");
				throw new ArgumentException(string.Format(text, "ActivationArguments", "Evidence"), "applicationEvidence");
			}
			if (activationArguments.ActivationContext == null)
			{
				string text2 = Locale.GetText("No {0} found in {1}.");
				throw new ArgumentException(string.Format(text2, "ActivationContext", "ActivationArguments"), "applicationEvidence");
			}
			if (!ApplicationSecurityManager.DetermineApplicationTrust(activationArguments.ActivationContext, context))
			{
				return null;
			}
			if (activationArguments.ApplicationIdentity == null)
			{
				return new ApplicationTrust();
			}
			return new ApplicationTrust(activationArguments.ApplicationIdentity);
		}

		// Token: 0x06003488 RID: 13448 RVA: 0x000AC7FC File Offset: 0x000AA9FC
		public virtual Evidence ProvideAppDomainEvidence(Evidence inputEvidence)
		{
			return inputEvidence;
		}

		// Token: 0x06003489 RID: 13449 RVA: 0x000AC800 File Offset: 0x000AAA00
		public virtual Evidence ProvideAssemblyEvidence(Assembly loadedAssembly, Evidence inputEvidence)
		{
			return inputEvidence;
		}

		// Token: 0x0600348A RID: 13450 RVA: 0x000AC804 File Offset: 0x000AAA04
		public virtual PermissionSet ResolvePolicy(Evidence evidence)
		{
			if (evidence == null)
			{
				throw new NullReferenceException("evidence");
			}
			return SecurityManager.ResolvePolicy(evidence);
		}
	}
}
