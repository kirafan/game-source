﻿using System;
using System.Runtime.InteropServices;

namespace System.Security
{
	// Token: 0x02000537 RID: 1335
	[ComVisible(true)]
	public interface IPermission : ISecurityEncodable
	{
		// Token: 0x0600348C RID: 13452
		IPermission Copy();

		// Token: 0x0600348D RID: 13453
		void Demand();

		// Token: 0x0600348E RID: 13454
		IPermission Intersect(IPermission target);

		// Token: 0x0600348F RID: 13455
		bool IsSubsetOf(IPermission target);

		// Token: 0x06003490 RID: 13456
		IPermission Union(IPermission target);
	}
}
