﻿using System;
using System.Runtime.InteropServices;
using System.Security.Policy;

namespace System.Security
{
	// Token: 0x02000536 RID: 1334
	[ComVisible(true)]
	public interface IEvidenceFactory
	{
		// Token: 0x170009D5 RID: 2517
		// (get) Token: 0x0600348B RID: 13451
		Evidence Evidence { get; }
	}
}
