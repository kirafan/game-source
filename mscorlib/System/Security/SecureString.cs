﻿using System;
using System.Runtime.ConstrainedExecution;

namespace System.Security
{
	// Token: 0x0200053F RID: 1343
	[MonoTODO("work in progress - encryption is missing")]
	public sealed class SecureString : CriticalFinalizerObject, IDisposable
	{
		// Token: 0x060034E0 RID: 13536 RVA: 0x000AE340 File Offset: 0x000AC540
		public SecureString()
		{
			this.Alloc(8, false);
		}

		// Token: 0x060034E1 RID: 13537 RVA: 0x000AE350 File Offset: 0x000AC550
		[CLSCompliant(false)]
		public unsafe SecureString(char* value, int length)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (length < 0 || length > 65536)
			{
				throw new ArgumentOutOfRangeException("length", "< 0 || > 65536");
			}
			this.length = length;
			this.Alloc(length, false);
			int num = 0;
			for (int i = 0; i < length; i++)
			{
				char c = *(value++);
				this.data[num++] = (byte)(c >> 8);
				this.data[num++] = (byte)c;
			}
			this.Encrypt();
		}

		// Token: 0x170009DE RID: 2526
		// (get) Token: 0x060034E3 RID: 13539 RVA: 0x000AE3EC File Offset: 0x000AC5EC
		public int Length
		{
			get
			{
				if (this.disposed)
				{
					throw new ObjectDisposedException("SecureString");
				}
				return this.length;
			}
		}

		// Token: 0x060034E4 RID: 13540 RVA: 0x000AE40C File Offset: 0x000AC60C
		public void AppendChar(char c)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("SecureString");
			}
			if (this.read_only)
			{
				throw new InvalidOperationException(Locale.GetText("SecureString is read-only."));
			}
			if (this.length == 65536)
			{
				throw new ArgumentOutOfRangeException("length", "> 65536");
			}
			try
			{
				this.Decrypt();
				int num = this.length * 2;
				this.Alloc(++this.length, true);
				this.data[num++] = (byte)(c >> 8);
				this.data[num++] = (byte)c;
			}
			finally
			{
				this.Encrypt();
			}
		}

		// Token: 0x060034E5 RID: 13541 RVA: 0x000AE4D8 File Offset: 0x000AC6D8
		public void Clear()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("SecureString");
			}
			if (this.read_only)
			{
				throw new InvalidOperationException(Locale.GetText("SecureString is read-only."));
			}
			Array.Clear(this.data, 0, this.data.Length);
			this.length = 0;
		}

		// Token: 0x060034E6 RID: 13542 RVA: 0x000AE534 File Offset: 0x000AC734
		public SecureString Copy()
		{
			return new SecureString
			{
				data = (byte[])this.data.Clone(),
				length = this.length
			};
		}

		// Token: 0x060034E7 RID: 13543 RVA: 0x000AE56C File Offset: 0x000AC76C
		public void Dispose()
		{
			this.disposed = true;
			if (this.data != null)
			{
				Array.Clear(this.data, 0, this.data.Length);
				this.data = null;
			}
			this.length = 0;
		}

		// Token: 0x060034E8 RID: 13544 RVA: 0x000AE5B0 File Offset: 0x000AC7B0
		public void InsertAt(int index, char c)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("SecureString");
			}
			if (this.read_only)
			{
				throw new InvalidOperationException(Locale.GetText("SecureString is read-only."));
			}
			if (index < 0 || index > this.length)
			{
				throw new ArgumentOutOfRangeException("index", "< 0 || > length");
			}
			if (this.length >= 65536)
			{
				string text = Locale.GetText("Maximum string size is '{0}'.", new object[]
				{
					65536
				});
				throw new ArgumentOutOfRangeException("index", text);
			}
			try
			{
				this.Decrypt();
				this.Alloc(++this.length, true);
				int num = index * 2;
				Buffer.BlockCopy(this.data, num, this.data, num + 2, this.data.Length - num - 2);
				this.data[num++] = (byte)(c >> 8);
				this.data[num] = (byte)c;
			}
			finally
			{
				this.Encrypt();
			}
		}

		// Token: 0x060034E9 RID: 13545 RVA: 0x000AE6D0 File Offset: 0x000AC8D0
		public bool IsReadOnly()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("SecureString");
			}
			return this.read_only;
		}

		// Token: 0x060034EA RID: 13546 RVA: 0x000AE6F0 File Offset: 0x000AC8F0
		public void MakeReadOnly()
		{
			this.read_only = true;
		}

		// Token: 0x060034EB RID: 13547 RVA: 0x000AE6FC File Offset: 0x000AC8FC
		public void RemoveAt(int index)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("SecureString");
			}
			if (this.read_only)
			{
				throw new InvalidOperationException(Locale.GetText("SecureString is read-only."));
			}
			if (index < 0 || index >= this.length)
			{
				throw new ArgumentOutOfRangeException("index", "< 0 || > length");
			}
			try
			{
				this.Decrypt();
				Buffer.BlockCopy(this.data, index + 1, this.data, index, this.data.Length - index - 1);
				this.Alloc(--this.length, true);
			}
			finally
			{
				this.Encrypt();
			}
		}

		// Token: 0x060034EC RID: 13548 RVA: 0x000AE7C4 File Offset: 0x000AC9C4
		public void SetAt(int index, char c)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("SecureString");
			}
			if (this.read_only)
			{
				throw new InvalidOperationException(Locale.GetText("SecureString is read-only."));
			}
			if (index < 0 || index >= this.length)
			{
				throw new ArgumentOutOfRangeException("index", "< 0 || > length");
			}
			try
			{
				this.Decrypt();
				int num = index * 2;
				this.data[num++] = (byte)(c >> 8);
				this.data[num] = (byte)c;
			}
			finally
			{
				this.Encrypt();
			}
		}

		// Token: 0x060034ED RID: 13549 RVA: 0x000AE874 File Offset: 0x000ACA74
		private void Encrypt()
		{
			if (this.data == null || this.data.Length > 0)
			{
			}
		}

		// Token: 0x060034EE RID: 13550 RVA: 0x000AE890 File Offset: 0x000ACA90
		private void Decrypt()
		{
			if (this.data == null || this.data.Length > 0)
			{
			}
		}

		// Token: 0x060034EF RID: 13551 RVA: 0x000AE8AC File Offset: 0x000ACAAC
		private void Alloc(int length, bool realloc)
		{
			if (length < 0 || length > 65536)
			{
				throw new ArgumentOutOfRangeException("length", "< 0 || > 65536");
			}
			int num = (length >> 3) + (((length & 7) != 0) ? 1 : 0) << 4;
			if (realloc && this.data != null && num == this.data.Length)
			{
				return;
			}
			if (realloc)
			{
				byte[] array = new byte[num];
				Array.Copy(this.data, 0, array, 0, Math.Min(this.data.Length, array.Length));
				Array.Clear(this.data, 0, this.data.Length);
				this.data = array;
			}
			else
			{
				this.data = new byte[num];
			}
		}

		// Token: 0x060034F0 RID: 13552 RVA: 0x000AE968 File Offset: 0x000ACB68
		internal byte[] GetBuffer()
		{
			byte[] array = new byte[this.length << 1];
			try
			{
				this.Decrypt();
				Buffer.BlockCopy(this.data, 0, array, 0, array.Length);
			}
			finally
			{
				this.Encrypt();
			}
			return array;
		}

		// Token: 0x04001635 RID: 5685
		private const int BlockSize = 16;

		// Token: 0x04001636 RID: 5686
		private const int MaxSize = 65536;

		// Token: 0x04001637 RID: 5687
		private int length;

		// Token: 0x04001638 RID: 5688
		private bool disposed;

		// Token: 0x04001639 RID: 5689
		private bool read_only;

		// Token: 0x0400163A RID: 5690
		private byte[] data;
	}
}
