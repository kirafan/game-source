﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200012A RID: 298
	[ComVisible(true)]
	[Flags]
	public enum AppDomainManagerInitializationOptions
	{
		// Token: 0x040004D0 RID: 1232
		None = 0,
		// Token: 0x040004D1 RID: 1233
		RegisterWithHost = 1
	}
}
