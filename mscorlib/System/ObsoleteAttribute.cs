﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200003F RID: 63
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false)]
	[Serializable]
	public sealed class ObsoleteAttribute : Attribute
	{
		// Token: 0x06000650 RID: 1616 RVA: 0x00014A24 File Offset: 0x00012C24
		public ObsoleteAttribute()
		{
		}

		// Token: 0x06000651 RID: 1617 RVA: 0x00014A2C File Offset: 0x00012C2C
		public ObsoleteAttribute(string message)
		{
			this._message = message;
		}

		// Token: 0x06000652 RID: 1618 RVA: 0x00014A3C File Offset: 0x00012C3C
		public ObsoleteAttribute(string message, bool error)
		{
			this._message = message;
			this._error = error;
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000653 RID: 1619 RVA: 0x00014A54 File Offset: 0x00012C54
		public string Message
		{
			get
			{
				return this._message;
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x06000654 RID: 1620 RVA: 0x00014A5C File Offset: 0x00012C5C
		public bool IsError
		{
			get
			{
				return this._error;
			}
		}

		// Token: 0x04000083 RID: 131
		private string _message;

		// Token: 0x04000084 RID: 132
		private bool _error;
	}
}
