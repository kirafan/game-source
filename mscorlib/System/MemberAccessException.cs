﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200014E RID: 334
	[ComVisible(true)]
	[Serializable]
	public class MemberAccessException : SystemException
	{
		// Token: 0x06001218 RID: 4632 RVA: 0x00047C0C File Offset: 0x00045E0C
		public MemberAccessException() : base(Locale.GetText("Cannot access a class member."))
		{
			base.HResult = -2146233062;
		}

		// Token: 0x06001219 RID: 4633 RVA: 0x00047C2C File Offset: 0x00045E2C
		public MemberAccessException(string message) : base(message)
		{
			base.HResult = -2146233062;
		}

		// Token: 0x0600121A RID: 4634 RVA: 0x00047C40 File Offset: 0x00045E40
		protected MemberAccessException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0600121B RID: 4635 RVA: 0x00047C4C File Offset: 0x00045E4C
		public MemberAccessException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233062;
		}

		// Token: 0x0400052D RID: 1325
		private const int Result = -2146233062;
	}
}
