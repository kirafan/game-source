﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200010C RID: 268
	[ComVisible(true)]
	[Serializable]
	public sealed class CharEnumerator : IEnumerator, IDisposable, ICloneable, IEnumerator<char>
	{
		// Token: 0x06000DC3 RID: 3523 RVA: 0x0003B9AC File Offset: 0x00039BAC
		internal CharEnumerator(string s)
		{
			this.str = s;
			this.index = -1;
			this.length = s.Length;
		}

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x06000DC4 RID: 3524 RVA: 0x0003B9DC File Offset: 0x00039BDC
		object IEnumerator.Current
		{
			get
			{
				return this.Current;
			}
		}

		// Token: 0x06000DC5 RID: 3525 RVA: 0x0003B9EC File Offset: 0x00039BEC
		void IDisposable.Dispose()
		{
		}

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x06000DC6 RID: 3526 RVA: 0x0003B9F0 File Offset: 0x00039BF0
		public char Current
		{
			get
			{
				if (this.index == -1 || this.index >= this.length)
				{
					throw new InvalidOperationException(Locale.GetText("The position is not valid."));
				}
				return this.str[this.index];
			}
		}

		// Token: 0x06000DC7 RID: 3527 RVA: 0x0003BA3C File Offset: 0x00039C3C
		public object Clone()
		{
			return new CharEnumerator(this.str)
			{
				index = this.index
			};
		}

		// Token: 0x06000DC8 RID: 3528 RVA: 0x0003BA64 File Offset: 0x00039C64
		public bool MoveNext()
		{
			this.index++;
			if (this.index >= this.length)
			{
				this.index = this.length;
				return false;
			}
			return true;
		}

		// Token: 0x06000DC9 RID: 3529 RVA: 0x0003BAA0 File Offset: 0x00039CA0
		public void Reset()
		{
			this.index = -1;
		}

		// Token: 0x040003C0 RID: 960
		private string str;

		// Token: 0x040003C1 RID: 961
		private int index;

		// Token: 0x040003C2 RID: 962
		private int length;
	}
}
