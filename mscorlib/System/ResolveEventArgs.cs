﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200016E RID: 366
	[ComVisible(true)]
	public class ResolveEventArgs : EventArgs
	{
		// Token: 0x06001365 RID: 4965 RVA: 0x0004D96C File Offset: 0x0004BB6C
		public ResolveEventArgs(string name)
		{
			this.m_Name = name;
		}

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x06001366 RID: 4966 RVA: 0x0004D97C File Offset: 0x0004BB7C
		public string Name
		{
			get
			{
				return this.m_Name;
			}
		}

		// Token: 0x040005A5 RID: 1445
		private string m_Name;
	}
}
