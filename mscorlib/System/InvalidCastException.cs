﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000145 RID: 325
	[ComVisible(true)]
	[Serializable]
	public class InvalidCastException : SystemException
	{
		// Token: 0x060011B9 RID: 4537 RVA: 0x000470F0 File Offset: 0x000452F0
		public InvalidCastException() : base(Locale.GetText("Cannot cast from source type to destination type."))
		{
			base.HResult = -2147467262;
		}

		// Token: 0x060011BA RID: 4538 RVA: 0x00047110 File Offset: 0x00045310
		public InvalidCastException(string message) : base(message)
		{
			base.HResult = -2147467262;
		}

		// Token: 0x060011BB RID: 4539 RVA: 0x00047124 File Offset: 0x00045324
		public InvalidCastException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467262;
		}

		// Token: 0x060011BC RID: 4540 RVA: 0x0004713C File Offset: 0x0004533C
		public InvalidCastException(string message, int errorCode) : base(message)
		{
			base.HResult = errorCode;
		}

		// Token: 0x060011BD RID: 4541 RVA: 0x0004714C File Offset: 0x0004534C
		protected InvalidCastException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0400051C RID: 1308
		private const int Result = -2147467262;
	}
}
