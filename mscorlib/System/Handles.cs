﻿using System;

namespace System
{
	// Token: 0x0200019F RID: 415
	internal enum Handles
	{
		// Token: 0x04000843 RID: 2115
		STD_INPUT = -10,
		// Token: 0x04000844 RID: 2116
		STD_OUTPUT = -11,
		// Token: 0x04000845 RID: 2117
		STD_ERROR = -12
	}
}
