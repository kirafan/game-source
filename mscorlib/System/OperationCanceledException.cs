﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000167 RID: 359
	[ComVisible(true)]
	[Serializable]
	public class OperationCanceledException : SystemException
	{
		// Token: 0x06001349 RID: 4937 RVA: 0x0004D54C File Offset: 0x0004B74C
		public OperationCanceledException() : base(Locale.GetText("The operation was canceled."))
		{
			base.HResult = -2146233029;
		}

		// Token: 0x0600134A RID: 4938 RVA: 0x0004D56C File Offset: 0x0004B76C
		public OperationCanceledException(string message) : base(message)
		{
			base.HResult = -2146233029;
		}

		// Token: 0x0600134B RID: 4939 RVA: 0x0004D580 File Offset: 0x0004B780
		public OperationCanceledException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233029;
		}

		// Token: 0x0600134C RID: 4940 RVA: 0x0004D598 File Offset: 0x0004B798
		protected OperationCanceledException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000592 RID: 1426
		private const int Result = -2146233029;
	}
}
