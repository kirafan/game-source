﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000172 RID: 370
	[AttributeUsage(AttributeTargets.Method)]
	[ComVisible(true)]
	public sealed class STAThreadAttribute : Attribute
	{
	}
}
