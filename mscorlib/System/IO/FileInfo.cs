﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.AccessControl;

namespace System.IO
{
	// Token: 0x0200023D RID: 573
	[ComVisible(true)]
	[Serializable]
	public sealed class FileInfo : FileSystemInfo
	{
		// Token: 0x06001DB1 RID: 7601 RVA: 0x0006DD9C File Offset: 0x0006BF9C
		public FileInfo(string fileName)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			base.CheckPath(fileName);
			this.OriginalPath = fileName;
			this.FullPath = Path.GetFullPath(fileName);
		}

		// Token: 0x06001DB2 RID: 7602 RVA: 0x0006DDD0 File Offset: 0x0006BFD0
		private FileInfo(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06001DB3 RID: 7603 RVA: 0x0006DDDC File Offset: 0x0006BFDC
		internal override void InternalRefresh()
		{
			this.exists = File.Exists(this.FullPath);
		}

		// Token: 0x170004F1 RID: 1265
		// (get) Token: 0x06001DB4 RID: 7604 RVA: 0x0006DDF0 File Offset: 0x0006BFF0
		public override bool Exists
		{
			get
			{
				base.Refresh(false);
				return this.stat.Attributes != MonoIO.InvalidFileAttributes && (this.stat.Attributes & FileAttributes.Directory) == (FileAttributes)0 && this.exists;
			}
		}

		// Token: 0x170004F2 RID: 1266
		// (get) Token: 0x06001DB5 RID: 7605 RVA: 0x0006DE2C File Offset: 0x0006C02C
		public override string Name
		{
			get
			{
				return Path.GetFileName(this.FullPath);
			}
		}

		// Token: 0x170004F3 RID: 1267
		// (get) Token: 0x06001DB6 RID: 7606 RVA: 0x0006DE3C File Offset: 0x0006C03C
		// (set) Token: 0x06001DB7 RID: 7607 RVA: 0x0006DE88 File Offset: 0x0006C088
		public bool IsReadOnly
		{
			get
			{
				if (!this.Exists)
				{
					throw new FileNotFoundException("Could not find file \"" + this.OriginalPath + "\".", this.OriginalPath);
				}
				return (this.stat.Attributes & FileAttributes.ReadOnly) != (FileAttributes)0;
			}
			set
			{
				if (!this.Exists)
				{
					throw new FileNotFoundException("Could not find file \"" + this.OriginalPath + "\".", this.OriginalPath);
				}
				FileAttributes fileAttributes = File.GetAttributes(this.FullPath);
				if (value)
				{
					fileAttributes |= FileAttributes.ReadOnly;
				}
				else
				{
					fileAttributes &= ~FileAttributes.ReadOnly;
				}
				File.SetAttributes(this.FullPath, fileAttributes);
			}
		}

		// Token: 0x06001DB8 RID: 7608 RVA: 0x0006DEF0 File Offset: 0x0006C0F0
		[ComVisible(false)]
		[MonoLimitation("File encryption isn't supported (even on NTFS).")]
		public void Encrypt()
		{
			throw new NotSupportedException(Locale.GetText("File encryption isn't supported on any file system."));
		}

		// Token: 0x06001DB9 RID: 7609 RVA: 0x0006DF04 File Offset: 0x0006C104
		[MonoLimitation("File encryption isn't supported (even on NTFS).")]
		[ComVisible(false)]
		public void Decrypt()
		{
			throw new NotSupportedException(Locale.GetText("File encryption isn't supported on any file system."));
		}

		// Token: 0x170004F4 RID: 1268
		// (get) Token: 0x06001DBA RID: 7610 RVA: 0x0006DF18 File Offset: 0x0006C118
		public long Length
		{
			get
			{
				if (!this.Exists)
				{
					throw new FileNotFoundException("Could not find file \"" + this.OriginalPath + "\".", this.OriginalPath);
				}
				return this.stat.Length;
			}
		}

		// Token: 0x170004F5 RID: 1269
		// (get) Token: 0x06001DBB RID: 7611 RVA: 0x0006DF54 File Offset: 0x0006C154
		public string DirectoryName
		{
			get
			{
				return Path.GetDirectoryName(this.FullPath);
			}
		}

		// Token: 0x170004F6 RID: 1270
		// (get) Token: 0x06001DBC RID: 7612 RVA: 0x0006DF64 File Offset: 0x0006C164
		public DirectoryInfo Directory
		{
			get
			{
				return new DirectoryInfo(this.DirectoryName);
			}
		}

		// Token: 0x06001DBD RID: 7613 RVA: 0x0006DF74 File Offset: 0x0006C174
		public StreamReader OpenText()
		{
			return new StreamReader(this.Open(FileMode.Open, FileAccess.Read));
		}

		// Token: 0x06001DBE RID: 7614 RVA: 0x0006DF84 File Offset: 0x0006C184
		public StreamWriter CreateText()
		{
			return new StreamWriter(this.Open(FileMode.Create, FileAccess.Write));
		}

		// Token: 0x06001DBF RID: 7615 RVA: 0x0006DF94 File Offset: 0x0006C194
		public StreamWriter AppendText()
		{
			return new StreamWriter(this.Open(FileMode.Append, FileAccess.Write));
		}

		// Token: 0x06001DC0 RID: 7616 RVA: 0x0006DFA4 File Offset: 0x0006C1A4
		public FileStream Create()
		{
			return File.Create(this.FullPath);
		}

		// Token: 0x06001DC1 RID: 7617 RVA: 0x0006DFB4 File Offset: 0x0006C1B4
		public FileStream OpenRead()
		{
			return this.Open(FileMode.Open, FileAccess.Read, FileShare.Read);
		}

		// Token: 0x06001DC2 RID: 7618 RVA: 0x0006DFC0 File Offset: 0x0006C1C0
		public FileStream OpenWrite()
		{
			return this.Open(FileMode.OpenOrCreate, FileAccess.Write);
		}

		// Token: 0x06001DC3 RID: 7619 RVA: 0x0006DFCC File Offset: 0x0006C1CC
		public FileStream Open(FileMode mode)
		{
			return this.Open(mode, FileAccess.ReadWrite);
		}

		// Token: 0x06001DC4 RID: 7620 RVA: 0x0006DFD8 File Offset: 0x0006C1D8
		public FileStream Open(FileMode mode, FileAccess access)
		{
			return this.Open(mode, access, FileShare.None);
		}

		// Token: 0x06001DC5 RID: 7621 RVA: 0x0006DFE4 File Offset: 0x0006C1E4
		public FileStream Open(FileMode mode, FileAccess access, FileShare share)
		{
			return new FileStream(this.FullPath, mode, access, share);
		}

		// Token: 0x06001DC6 RID: 7622 RVA: 0x0006DFF4 File Offset: 0x0006C1F4
		public override void Delete()
		{
			MonoIOError error;
			if (!MonoIO.Exists(this.FullPath, out error))
			{
				return;
			}
			if (MonoIO.ExistsDirectory(this.FullPath, out error))
			{
				throw new UnauthorizedAccessException("Access to the path \"" + this.FullPath + "\" is denied.");
			}
			if (!MonoIO.DeleteFile(this.FullPath, out error))
			{
				throw MonoIO.GetException(this.OriginalPath, error);
			}
		}

		// Token: 0x06001DC7 RID: 7623 RVA: 0x0006E060 File Offset: 0x0006C260
		public void MoveTo(string destFileName)
		{
			if (destFileName == null)
			{
				throw new ArgumentNullException("destFileName");
			}
			if (destFileName == this.Name || destFileName == this.FullName)
			{
				return;
			}
			if (!File.Exists(this.FullPath))
			{
				throw new FileNotFoundException();
			}
			File.Move(this.FullPath, destFileName);
			this.FullPath = Path.GetFullPath(destFileName);
		}

		// Token: 0x06001DC8 RID: 7624 RVA: 0x0006E0D0 File Offset: 0x0006C2D0
		public FileInfo CopyTo(string destFileName)
		{
			return this.CopyTo(destFileName, false);
		}

		// Token: 0x06001DC9 RID: 7625 RVA: 0x0006E0DC File Offset: 0x0006C2DC
		public FileInfo CopyTo(string destFileName, bool overwrite)
		{
			if (destFileName == null)
			{
				throw new ArgumentNullException("destFileName");
			}
			if (destFileName.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.", "destFileName");
			}
			string fullPath = Path.GetFullPath(destFileName);
			if (overwrite && File.Exists(fullPath))
			{
				File.Delete(fullPath);
			}
			File.Copy(this.FullPath, fullPath);
			return new FileInfo(fullPath);
		}

		// Token: 0x06001DCA RID: 7626 RVA: 0x0006E148 File Offset: 0x0006C348
		public override string ToString()
		{
			return this.OriginalPath;
		}

		// Token: 0x06001DCB RID: 7627 RVA: 0x0006E150 File Offset: 0x0006C350
		public FileSecurity GetAccessControl()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001DCC RID: 7628 RVA: 0x0006E158 File Offset: 0x0006C358
		public FileSecurity GetAccessControl(AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001DCD RID: 7629 RVA: 0x0006E160 File Offset: 0x0006C360
		[ComVisible(false)]
		public FileInfo Replace(string destinationFileName, string destinationBackupFileName)
		{
			if (!this.Exists)
			{
				throw new FileNotFoundException();
			}
			if (destinationFileName == null)
			{
				throw new ArgumentNullException("destinationFileName");
			}
			if (destinationFileName.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.", "destinationFileName");
			}
			string fullPath = Path.GetFullPath(destinationFileName);
			if (!File.Exists(fullPath))
			{
				throw new FileNotFoundException();
			}
			FileAttributes attributes = File.GetAttributes(fullPath);
			if ((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly)
			{
				throw new UnauthorizedAccessException();
			}
			if (destinationBackupFileName != null)
			{
				if (destinationBackupFileName.Length == 0)
				{
					throw new ArgumentException("An empty file name is not valid.", "destinationBackupFileName");
				}
				File.Copy(fullPath, Path.GetFullPath(destinationBackupFileName), true);
			}
			File.Copy(this.FullPath, fullPath, true);
			File.Delete(this.FullPath);
			return new FileInfo(fullPath);
		}

		// Token: 0x06001DCE RID: 7630 RVA: 0x0006E228 File Offset: 0x0006C428
		[ComVisible(false)]
		public FileInfo Replace(string destinationFileName, string destinationBackupFileName, bool ignoreMetadataErrors)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001DCF RID: 7631 RVA: 0x0006E230 File Offset: 0x0006C430
		public void SetAccessControl(FileSecurity fileSecurity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000B17 RID: 2839
		private bool exists;
	}
}
