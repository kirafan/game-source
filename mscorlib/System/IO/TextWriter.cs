﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.IO
{
	// Token: 0x0200025E RID: 606
	[ComVisible(true)]
	[Serializable]
	public abstract class TextWriter : MarshalByRefObject, IDisposable
	{
		// Token: 0x06001F5A RID: 8026 RVA: 0x0007400C File Offset: 0x0007220C
		protected TextWriter()
		{
			this.CoreNewLine = Environment.NewLine.ToCharArray();
		}

		// Token: 0x06001F5B RID: 8027 RVA: 0x00074024 File Offset: 0x00072224
		protected TextWriter(IFormatProvider formatProvider)
		{
			this.CoreNewLine = Environment.NewLine.ToCharArray();
			this.internalFormatProvider = formatProvider;
		}

		// Token: 0x17000541 RID: 1345
		// (get) Token: 0x06001F5D RID: 8029
		public abstract Encoding Encoding { get; }

		// Token: 0x17000542 RID: 1346
		// (get) Token: 0x06001F5E RID: 8030 RVA: 0x00074050 File Offset: 0x00072250
		public virtual IFormatProvider FormatProvider
		{
			get
			{
				return this.internalFormatProvider;
			}
		}

		// Token: 0x17000543 RID: 1347
		// (get) Token: 0x06001F5F RID: 8031 RVA: 0x00074058 File Offset: 0x00072258
		// (set) Token: 0x06001F60 RID: 8032 RVA: 0x00074068 File Offset: 0x00072268
		public virtual string NewLine
		{
			get
			{
				return new string(this.CoreNewLine);
			}
			set
			{
				if (value == null)
				{
					value = Environment.NewLine;
				}
				this.CoreNewLine = value.ToCharArray();
			}
		}

		// Token: 0x06001F61 RID: 8033 RVA: 0x00074084 File Offset: 0x00072284
		public virtual void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x06001F62 RID: 8034 RVA: 0x00074090 File Offset: 0x00072290
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				GC.SuppressFinalize(this);
			}
		}

		// Token: 0x06001F63 RID: 8035 RVA: 0x000740A0 File Offset: 0x000722A0
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06001F64 RID: 8036 RVA: 0x000740B0 File Offset: 0x000722B0
		public virtual void Flush()
		{
		}

		// Token: 0x06001F65 RID: 8037 RVA: 0x000740B4 File Offset: 0x000722B4
		public static TextWriter Synchronized(TextWriter writer)
		{
			return TextWriter.Synchronized(writer, false);
		}

		// Token: 0x06001F66 RID: 8038 RVA: 0x000740C0 File Offset: 0x000722C0
		internal static TextWriter Synchronized(TextWriter writer, bool neverClose)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer is null");
			}
			if (writer is SynchronizedWriter)
			{
				return writer;
			}
			return new SynchronizedWriter(writer, neverClose);
		}

		// Token: 0x06001F67 RID: 8039 RVA: 0x000740E8 File Offset: 0x000722E8
		public virtual void Write(bool value)
		{
			this.Write(value.ToString());
		}

		// Token: 0x06001F68 RID: 8040 RVA: 0x000740F8 File Offset: 0x000722F8
		public virtual void Write(char value)
		{
		}

		// Token: 0x06001F69 RID: 8041 RVA: 0x000740FC File Offset: 0x000722FC
		public virtual void Write(char[] buffer)
		{
			if (buffer == null)
			{
				return;
			}
			this.Write(buffer, 0, buffer.Length);
		}

		// Token: 0x06001F6A RID: 8042 RVA: 0x00074110 File Offset: 0x00072310
		public virtual void Write(decimal value)
		{
			this.Write(value.ToString(this.internalFormatProvider));
		}

		// Token: 0x06001F6B RID: 8043 RVA: 0x00074128 File Offset: 0x00072328
		public virtual void Write(double value)
		{
			this.Write(value.ToString(this.internalFormatProvider));
		}

		// Token: 0x06001F6C RID: 8044 RVA: 0x00074140 File Offset: 0x00072340
		public virtual void Write(int value)
		{
			this.Write(value.ToString(this.internalFormatProvider));
		}

		// Token: 0x06001F6D RID: 8045 RVA: 0x00074158 File Offset: 0x00072358
		public virtual void Write(long value)
		{
			this.Write(value.ToString(this.internalFormatProvider));
		}

		// Token: 0x06001F6E RID: 8046 RVA: 0x00074170 File Offset: 0x00072370
		public virtual void Write(object value)
		{
			if (value != null)
			{
				this.Write(value.ToString());
			}
		}

		// Token: 0x06001F6F RID: 8047 RVA: 0x00074184 File Offset: 0x00072384
		public virtual void Write(float value)
		{
			this.Write(value.ToString(this.internalFormatProvider));
		}

		// Token: 0x06001F70 RID: 8048 RVA: 0x0007419C File Offset: 0x0007239C
		public virtual void Write(string value)
		{
			if (value != null)
			{
				this.Write(value.ToCharArray());
			}
		}

		// Token: 0x06001F71 RID: 8049 RVA: 0x000741B0 File Offset: 0x000723B0
		[CLSCompliant(false)]
		public virtual void Write(uint value)
		{
			this.Write(value.ToString(this.internalFormatProvider));
		}

		// Token: 0x06001F72 RID: 8050 RVA: 0x000741C8 File Offset: 0x000723C8
		[CLSCompliant(false)]
		public virtual void Write(ulong value)
		{
			this.Write(value.ToString(this.internalFormatProvider));
		}

		// Token: 0x06001F73 RID: 8051 RVA: 0x000741E0 File Offset: 0x000723E0
		public virtual void Write(string format, object arg0)
		{
			this.Write(string.Format(format, arg0));
		}

		// Token: 0x06001F74 RID: 8052 RVA: 0x000741F0 File Offset: 0x000723F0
		public virtual void Write(string format, params object[] arg)
		{
			this.Write(string.Format(format, arg));
		}

		// Token: 0x06001F75 RID: 8053 RVA: 0x00074200 File Offset: 0x00072400
		public virtual void Write(char[] buffer, int index, int count)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (index < 0 || index > buffer.Length)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (count < 0 || index > buffer.Length - count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			while (count > 0)
			{
				this.Write(buffer[index]);
				count--;
				index++;
			}
		}

		// Token: 0x06001F76 RID: 8054 RVA: 0x00074278 File Offset: 0x00072478
		public virtual void Write(string format, object arg0, object arg1)
		{
			this.Write(string.Format(format, arg0, arg1));
		}

		// Token: 0x06001F77 RID: 8055 RVA: 0x00074288 File Offset: 0x00072488
		public virtual void Write(string format, object arg0, object arg1, object arg2)
		{
			this.Write(string.Format(format, arg0, arg1, arg2));
		}

		// Token: 0x06001F78 RID: 8056 RVA: 0x0007429C File Offset: 0x0007249C
		public virtual void WriteLine()
		{
			this.Write(this.CoreNewLine);
		}

		// Token: 0x06001F79 RID: 8057 RVA: 0x000742AC File Offset: 0x000724AC
		public virtual void WriteLine(bool value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F7A RID: 8058 RVA: 0x000742BC File Offset: 0x000724BC
		public virtual void WriteLine(char value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F7B RID: 8059 RVA: 0x000742CC File Offset: 0x000724CC
		public virtual void WriteLine(char[] buffer)
		{
			this.Write(buffer);
			this.WriteLine();
		}

		// Token: 0x06001F7C RID: 8060 RVA: 0x000742DC File Offset: 0x000724DC
		public virtual void WriteLine(decimal value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F7D RID: 8061 RVA: 0x000742EC File Offset: 0x000724EC
		public virtual void WriteLine(double value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F7E RID: 8062 RVA: 0x000742FC File Offset: 0x000724FC
		public virtual void WriteLine(int value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F7F RID: 8063 RVA: 0x0007430C File Offset: 0x0007250C
		public virtual void WriteLine(long value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F80 RID: 8064 RVA: 0x0007431C File Offset: 0x0007251C
		public virtual void WriteLine(object value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F81 RID: 8065 RVA: 0x0007432C File Offset: 0x0007252C
		public virtual void WriteLine(float value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F82 RID: 8066 RVA: 0x0007433C File Offset: 0x0007253C
		public virtual void WriteLine(string value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F83 RID: 8067 RVA: 0x0007434C File Offset: 0x0007254C
		[CLSCompliant(false)]
		public virtual void WriteLine(uint value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F84 RID: 8068 RVA: 0x0007435C File Offset: 0x0007255C
		[CLSCompliant(false)]
		public virtual void WriteLine(ulong value)
		{
			this.Write(value);
			this.WriteLine();
		}

		// Token: 0x06001F85 RID: 8069 RVA: 0x0007436C File Offset: 0x0007256C
		public virtual void WriteLine(string format, object arg0)
		{
			this.Write(format, arg0);
			this.WriteLine();
		}

		// Token: 0x06001F86 RID: 8070 RVA: 0x0007437C File Offset: 0x0007257C
		public virtual void WriteLine(string format, params object[] arg)
		{
			this.Write(format, arg);
			this.WriteLine();
		}

		// Token: 0x06001F87 RID: 8071 RVA: 0x0007438C File Offset: 0x0007258C
		public virtual void WriteLine(char[] buffer, int index, int count)
		{
			this.Write(buffer, index, count);
			this.WriteLine();
		}

		// Token: 0x06001F88 RID: 8072 RVA: 0x000743A0 File Offset: 0x000725A0
		public virtual void WriteLine(string format, object arg0, object arg1)
		{
			this.Write(format, arg0, arg1);
			this.WriteLine();
		}

		// Token: 0x06001F89 RID: 8073 RVA: 0x000743B4 File Offset: 0x000725B4
		public virtual void WriteLine(string format, object arg0, object arg1, object arg2)
		{
			this.Write(format, arg0, arg1, arg2);
			this.WriteLine();
		}

		// Token: 0x04000BCE RID: 3022
		protected char[] CoreNewLine;

		// Token: 0x04000BCF RID: 3023
		internal IFormatProvider internalFormatProvider;

		// Token: 0x04000BD0 RID: 3024
		public static readonly TextWriter Null = new TextWriter.NullTextWriter();

		// Token: 0x0200025F RID: 607
		private sealed class NullTextWriter : TextWriter
		{
			// Token: 0x17000544 RID: 1348
			// (get) Token: 0x06001F8B RID: 8075 RVA: 0x000743D0 File Offset: 0x000725D0
			public override Encoding Encoding
			{
				get
				{
					return Encoding.Default;
				}
			}

			// Token: 0x06001F8C RID: 8076 RVA: 0x000743D8 File Offset: 0x000725D8
			public override void Write(string s)
			{
			}

			// Token: 0x06001F8D RID: 8077 RVA: 0x000743DC File Offset: 0x000725DC
			public override void Write(char value)
			{
			}

			// Token: 0x06001F8E RID: 8078 RVA: 0x000743E0 File Offset: 0x000725E0
			public override void Write(char[] value, int index, int count)
			{
			}
		}
	}
}
