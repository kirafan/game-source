﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x02000241 RID: 577
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum FileOptions
	{
		// Token: 0x04000B27 RID: 2855
		None = 0,
		// Token: 0x04000B28 RID: 2856
		Encrypted = 16384,
		// Token: 0x04000B29 RID: 2857
		DeleteOnClose = 67108864,
		// Token: 0x04000B2A RID: 2858
		SequentialScan = 134217728,
		// Token: 0x04000B2B RID: 2859
		RandomAccess = 268435456,
		// Token: 0x04000B2C RID: 2860
		Asynchronous = 1073741824,
		// Token: 0x04000B2D RID: 2861
		WriteThrough = -2147483648
	}
}
