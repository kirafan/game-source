﻿using System;
using System.Threading;

namespace System.IO
{
	// Token: 0x02000244 RID: 580
	internal class FileStreamAsyncResult : IAsyncResult
	{
		// Token: 0x06001E1F RID: 7711 RVA: 0x0006FC18 File Offset: 0x0006DE18
		public FileStreamAsyncResult(AsyncCallback cb, object state)
		{
			this.state = state;
			this.realcb = cb;
			if (this.realcb != null)
			{
				this.cb = new AsyncCallback(FileStreamAsyncResult.CBWrapper);
			}
			this.wh = new ManualResetEvent(false);
		}

		// Token: 0x06001E20 RID: 7712 RVA: 0x0006FC58 File Offset: 0x0006DE58
		private static void CBWrapper(IAsyncResult ares)
		{
			FileStreamAsyncResult fileStreamAsyncResult = (FileStreamAsyncResult)ares;
			fileStreamAsyncResult.realcb.BeginInvoke(ares, null, null);
		}

		// Token: 0x06001E21 RID: 7713 RVA: 0x0006FC7C File Offset: 0x0006DE7C
		public void SetComplete(Exception e)
		{
			this.exc = e;
			this.completed = true;
			this.wh.Set();
			if (this.cb != null)
			{
				this.cb(this);
			}
		}

		// Token: 0x06001E22 RID: 7714 RVA: 0x0006FCB0 File Offset: 0x0006DEB0
		public void SetComplete(Exception e, int nbytes)
		{
			this.BytesRead = nbytes;
			this.SetComplete(e);
		}

		// Token: 0x06001E23 RID: 7715 RVA: 0x0006FCC0 File Offset: 0x0006DEC0
		public void SetComplete(Exception e, int nbytes, bool synch)
		{
			this.completedSynch = synch;
			this.SetComplete(e, nbytes);
		}

		// Token: 0x17000506 RID: 1286
		// (get) Token: 0x06001E24 RID: 7716 RVA: 0x0006FCD4 File Offset: 0x0006DED4
		public object AsyncState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x17000507 RID: 1287
		// (get) Token: 0x06001E25 RID: 7717 RVA: 0x0006FCDC File Offset: 0x0006DEDC
		public bool CompletedSynchronously
		{
			get
			{
				return this.completedSynch;
			}
		}

		// Token: 0x17000508 RID: 1288
		// (get) Token: 0x06001E26 RID: 7718 RVA: 0x0006FCE4 File Offset: 0x0006DEE4
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				return this.wh;
			}
		}

		// Token: 0x17000509 RID: 1289
		// (get) Token: 0x06001E27 RID: 7719 RVA: 0x0006FCEC File Offset: 0x0006DEEC
		public bool IsCompleted
		{
			get
			{
				return this.completed;
			}
		}

		// Token: 0x1700050A RID: 1290
		// (get) Token: 0x06001E28 RID: 7720 RVA: 0x0006FCF4 File Offset: 0x0006DEF4
		public Exception Exception
		{
			get
			{
				return this.exc;
			}
		}

		// Token: 0x1700050B RID: 1291
		// (get) Token: 0x06001E29 RID: 7721 RVA: 0x0006FCFC File Offset: 0x0006DEFC
		// (set) Token: 0x06001E2A RID: 7722 RVA: 0x0006FD04 File Offset: 0x0006DF04
		public bool Done
		{
			get
			{
				return this.done;
			}
			set
			{
				this.done = value;
			}
		}

		// Token: 0x04000B45 RID: 2885
		private object state;

		// Token: 0x04000B46 RID: 2886
		private bool completed;

		// Token: 0x04000B47 RID: 2887
		private bool done;

		// Token: 0x04000B48 RID: 2888
		private Exception exc;

		// Token: 0x04000B49 RID: 2889
		private ManualResetEvent wh;

		// Token: 0x04000B4A RID: 2890
		private AsyncCallback cb;

		// Token: 0x04000B4B RID: 2891
		private bool completedSynch;

		// Token: 0x04000B4C RID: 2892
		public byte[] Buffer;

		// Token: 0x04000B4D RID: 2893
		public int Offset;

		// Token: 0x04000B4E RID: 2894
		public int Count;

		// Token: 0x04000B4F RID: 2895
		public int OriginalCount;

		// Token: 0x04000B50 RID: 2896
		public int BytesRead;

		// Token: 0x04000B51 RID: 2897
		private AsyncCallback realcb;
	}
}
