﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.IO
{
	// Token: 0x02000239 RID: 569
	[ComVisible(true)]
	[Serializable]
	public class EndOfStreamException : IOException
	{
		// Token: 0x06001D77 RID: 7543 RVA: 0x0006CEBC File Offset: 0x0006B0BC
		public EndOfStreamException() : base(Locale.GetText("Failed to read past end of stream."))
		{
		}

		// Token: 0x06001D78 RID: 7544 RVA: 0x0006CED0 File Offset: 0x0006B0D0
		public EndOfStreamException(string message) : base(message)
		{
		}

		// Token: 0x06001D79 RID: 7545 RVA: 0x0006CEDC File Offset: 0x0006B0DC
		protected EndOfStreamException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06001D7A RID: 7546 RVA: 0x0006CEE8 File Offset: 0x0006B0E8
		public EndOfStreamException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
