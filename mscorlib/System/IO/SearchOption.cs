﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x0200024E RID: 590
	[ComVisible(true)]
	[Serializable]
	public enum SearchOption
	{
		// Token: 0x04000B90 RID: 2960
		TopDirectoryOnly,
		// Token: 0x04000B91 RID: 2961
		AllDirectories
	}
}
