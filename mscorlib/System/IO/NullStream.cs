﻿using System;

namespace System.IO
{
	// Token: 0x02000254 RID: 596
	internal class NullStream : Stream
	{
		// Token: 0x1700052C RID: 1324
		// (get) Token: 0x06001EDA RID: 7898 RVA: 0x00072554 File Offset: 0x00070754
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700052D RID: 1325
		// (get) Token: 0x06001EDB RID: 7899 RVA: 0x00072558 File Offset: 0x00070758
		public override bool CanSeek
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700052E RID: 1326
		// (get) Token: 0x06001EDC RID: 7900 RVA: 0x0007255C File Offset: 0x0007075C
		public override bool CanWrite
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700052F RID: 1327
		// (get) Token: 0x06001EDD RID: 7901 RVA: 0x00072560 File Offset: 0x00070760
		public override long Length
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x17000530 RID: 1328
		// (get) Token: 0x06001EDE RID: 7902 RVA: 0x00072564 File Offset: 0x00070764
		// (set) Token: 0x06001EDF RID: 7903 RVA: 0x00072568 File Offset: 0x00070768
		public override long Position
		{
			get
			{
				return 0L;
			}
			set
			{
			}
		}

		// Token: 0x06001EE0 RID: 7904 RVA: 0x0007256C File Offset: 0x0007076C
		public override void Flush()
		{
		}

		// Token: 0x06001EE1 RID: 7905 RVA: 0x00072570 File Offset: 0x00070770
		public override int Read(byte[] buffer, int offset, int count)
		{
			return 0;
		}

		// Token: 0x06001EE2 RID: 7906 RVA: 0x00072574 File Offset: 0x00070774
		public override int ReadByte()
		{
			return -1;
		}

		// Token: 0x06001EE3 RID: 7907 RVA: 0x00072578 File Offset: 0x00070778
		public override long Seek(long offset, SeekOrigin origin)
		{
			return 0L;
		}

		// Token: 0x06001EE4 RID: 7908 RVA: 0x0007257C File Offset: 0x0007077C
		public override void SetLength(long value)
		{
		}

		// Token: 0x06001EE5 RID: 7909 RVA: 0x00072580 File Offset: 0x00070780
		public override void Write(byte[] buffer, int offset, int count)
		{
		}

		// Token: 0x06001EE6 RID: 7910 RVA: 0x00072584 File Offset: 0x00070784
		public override void WriteByte(byte value)
		{
		}
	}
}
