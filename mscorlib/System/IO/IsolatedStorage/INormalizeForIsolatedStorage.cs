﻿using System;
using System.Runtime.InteropServices;

namespace System.IO.IsolatedStorage
{
	// Token: 0x02000264 RID: 612
	[ComVisible(true)]
	public interface INormalizeForIsolatedStorage
	{
		// Token: 0x06001FED RID: 8173
		object Normalize();
	}
}
