﻿using System;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace System.IO.IsolatedStorage
{
	// Token: 0x02000265 RID: 613
	[ComVisible(true)]
	public abstract class IsolatedStorage : MarshalByRefObject
	{
		// Token: 0x1700054F RID: 1359
		// (get) Token: 0x06001FEF RID: 8175 RVA: 0x00075C30 File Offset: 0x00073E30
		[MonoTODO("requires manifest support")]
		[ComVisible(false)]
		public object ApplicationIdentity
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				if ((this.storage_scope & IsolatedStorageScope.Application) == IsolatedStorageScope.None)
				{
					throw new InvalidOperationException(Locale.GetText("Invalid Isolation Scope."));
				}
				if (this._applicationIdentity == null)
				{
					throw new InvalidOperationException(Locale.GetText("Identity unavailable."));
				}
				throw new NotImplementedException(Locale.GetText("CAS related"));
			}
		}

		// Token: 0x17000550 RID: 1360
		// (get) Token: 0x06001FF0 RID: 8176 RVA: 0x00075C88 File Offset: 0x00073E88
		public object AssemblyIdentity
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				if ((this.storage_scope & IsolatedStorageScope.Assembly) == IsolatedStorageScope.None)
				{
					throw new InvalidOperationException(Locale.GetText("Invalid Isolation Scope."));
				}
				if (this._assemblyIdentity == null)
				{
					throw new InvalidOperationException(Locale.GetText("Identity unavailable."));
				}
				return this._assemblyIdentity;
			}
		}

		// Token: 0x17000551 RID: 1361
		// (get) Token: 0x06001FF1 RID: 8177 RVA: 0x00075CD4 File Offset: 0x00073ED4
		[CLSCompliant(false)]
		public virtual ulong CurrentSize
		{
			get
			{
				throw new InvalidOperationException(Locale.GetText("IsolatedStorage does not have a preset CurrentSize."));
			}
		}

		// Token: 0x17000552 RID: 1362
		// (get) Token: 0x06001FF2 RID: 8178 RVA: 0x00075CE8 File Offset: 0x00073EE8
		public object DomainIdentity
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				if ((this.storage_scope & IsolatedStorageScope.Domain) == IsolatedStorageScope.None)
				{
					throw new InvalidOperationException(Locale.GetText("Invalid Isolation Scope."));
				}
				if (this._domainIdentity == null)
				{
					throw new InvalidOperationException(Locale.GetText("Identity unavailable."));
				}
				return this._domainIdentity;
			}
		}

		// Token: 0x17000553 RID: 1363
		// (get) Token: 0x06001FF3 RID: 8179 RVA: 0x00075D34 File Offset: 0x00073F34
		[CLSCompliant(false)]
		public virtual ulong MaximumSize
		{
			get
			{
				throw new InvalidOperationException(Locale.GetText("IsolatedStorage does not have a preset MaximumSize."));
			}
		}

		// Token: 0x17000554 RID: 1364
		// (get) Token: 0x06001FF4 RID: 8180 RVA: 0x00075D48 File Offset: 0x00073F48
		public IsolatedStorageScope Scope
		{
			get
			{
				return this.storage_scope;
			}
		}

		// Token: 0x17000555 RID: 1365
		// (get) Token: 0x06001FF5 RID: 8181 RVA: 0x00075D50 File Offset: 0x00073F50
		protected virtual char SeparatorExternal
		{
			get
			{
				return Path.DirectorySeparatorChar;
			}
		}

		// Token: 0x17000556 RID: 1366
		// (get) Token: 0x06001FF6 RID: 8182 RVA: 0x00075D58 File Offset: 0x00073F58
		protected virtual char SeparatorInternal
		{
			get
			{
				return '.';
			}
		}

		// Token: 0x06001FF7 RID: 8183
		protected abstract IsolatedStoragePermission GetPermission(PermissionSet ps);

		// Token: 0x06001FF8 RID: 8184 RVA: 0x00075D5C File Offset: 0x00073F5C
		protected void InitStore(IsolatedStorageScope scope, Type domainEvidenceType, Type assemblyEvidenceType)
		{
			switch (scope)
			{
			case IsolatedStorageScope.User | IsolatedStorageScope.Assembly:
			case IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly:
				throw new NotImplementedException(scope.ToString());
			}
			throw new ArgumentException(scope.ToString());
		}

		// Token: 0x06001FF9 RID: 8185 RVA: 0x00075DA8 File Offset: 0x00073FA8
		[MonoTODO("requires manifest support")]
		protected void InitStore(IsolatedStorageScope scope, Type appEvidenceType)
		{
			if (AppDomain.CurrentDomain.ApplicationIdentity == null)
			{
				throw new IsolatedStorageException(Locale.GetText("No ApplicationIdentity available for AppDomain."));
			}
			if (appEvidenceType == null)
			{
			}
			this.storage_scope = scope;
		}

		// Token: 0x06001FFA RID: 8186
		public abstract void Remove();

		// Token: 0x04000BDD RID: 3037
		internal IsolatedStorageScope storage_scope;

		// Token: 0x04000BDE RID: 3038
		internal object _assemblyIdentity;

		// Token: 0x04000BDF RID: 3039
		internal object _domainIdentity;

		// Token: 0x04000BE0 RID: 3040
		internal object _applicationIdentity;
	}
}
