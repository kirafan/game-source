﻿using System;
using System.Runtime.InteropServices;

namespace System.IO.IsolatedStorage
{
	// Token: 0x0200026B RID: 619
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum IsolatedStorageScope
	{
		// Token: 0x04000BED RID: 3053
		None = 0,
		// Token: 0x04000BEE RID: 3054
		User = 1,
		// Token: 0x04000BEF RID: 3055
		Domain = 2,
		// Token: 0x04000BF0 RID: 3056
		Assembly = 4,
		// Token: 0x04000BF1 RID: 3057
		Roaming = 8,
		// Token: 0x04000BF2 RID: 3058
		Machine = 16,
		// Token: 0x04000BF3 RID: 3059
		Application = 32
	}
}
