﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;

namespace System.IO.IsolatedStorage
{
	// Token: 0x0200026A RID: 618
	[ComVisible(true)]
	public class IsolatedStorageFileStream : FileStream
	{
		// Token: 0x0600202C RID: 8236 RVA: 0x00076D20 File Offset: 0x00074F20
		public IsolatedStorageFileStream(string path, FileMode mode) : this(path, mode, (mode != FileMode.Append) ? FileAccess.ReadWrite : FileAccess.Write, FileShare.Read, 8192, null)
		{
		}

		// Token: 0x0600202D RID: 8237 RVA: 0x00076D4C File Offset: 0x00074F4C
		public IsolatedStorageFileStream(string path, FileMode mode, FileAccess access) : this(path, mode, access, (access != FileAccess.Write) ? FileShare.Read : FileShare.None, 8192, null)
		{
		}

		// Token: 0x0600202E RID: 8238 RVA: 0x00076D78 File Offset: 0x00074F78
		public IsolatedStorageFileStream(string path, FileMode mode, FileAccess access, FileShare share) : this(path, mode, access, share, 8192, null)
		{
		}

		// Token: 0x0600202F RID: 8239 RVA: 0x00076D8C File Offset: 0x00074F8C
		public IsolatedStorageFileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize) : this(path, mode, access, share, bufferSize, null)
		{
		}

		// Token: 0x06002030 RID: 8240 RVA: 0x00076D9C File Offset: 0x00074F9C
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Unrestricted=\"true\"/>\n</PermissionSet>\n")]
		public IsolatedStorageFileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, IsolatedStorageFile isf) : base(IsolatedStorageFileStream.CreateIsolatedPath(isf, path, mode), mode, access, share, bufferSize, false, true)
		{
		}

		// Token: 0x06002031 RID: 8241 RVA: 0x00076DC0 File Offset: 0x00074FC0
		public IsolatedStorageFileStream(string path, FileMode mode, FileAccess access, FileShare share, IsolatedStorageFile isf) : this(path, mode, access, share, 8192, isf)
		{
		}

		// Token: 0x06002032 RID: 8242 RVA: 0x00076DD4 File Offset: 0x00074FD4
		public IsolatedStorageFileStream(string path, FileMode mode, FileAccess access, IsolatedStorageFile isf) : this(path, mode, access, (access != FileAccess.Write) ? FileShare.Read : FileShare.None, 8192, isf)
		{
		}

		// Token: 0x06002033 RID: 8243 RVA: 0x00076E00 File Offset: 0x00075000
		public IsolatedStorageFileStream(string path, FileMode mode, IsolatedStorageFile isf) : this(path, mode, (mode != FileMode.Append) ? FileAccess.ReadWrite : FileAccess.Write, FileShare.Read, 8192, isf)
		{
		}

		// Token: 0x06002034 RID: 8244 RVA: 0x00076E2C File Offset: 0x0007502C
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"TypeInformation\"/>\n</PermissionSet>\n")]
		private static string CreateIsolatedPath(IsolatedStorageFile isf, string path, FileMode mode)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (!Enum.IsDefined(typeof(FileMode), mode))
			{
				throw new ArgumentException("mode");
			}
			if (isf == null)
			{
				StackFrame stackFrame = new StackFrame(3);
				isf = IsolatedStorageFile.GetStore(IsolatedStorageScope.User | IsolatedStorageScope.Domain | IsolatedStorageScope.Assembly, IsolatedStorageFile.GetDomainIdentityFromEvidence(AppDomain.CurrentDomain.Evidence), IsolatedStorageFile.GetAssemblyIdentityFromEvidence(stackFrame.GetMethod().ReflectedType.Assembly.UnprotectedGetEvidence()));
			}
			FileInfo fileInfo = new FileInfo(isf.Root);
			if (!fileInfo.Directory.Exists)
			{
				fileInfo.Directory.Create();
			}
			if (Path.IsPathRooted(path))
			{
				string pathRoot = Path.GetPathRoot(path);
				path = path.Remove(0, pathRoot.Length);
			}
			string text = Path.Combine(isf.Root, path);
			string fullPath = Path.GetFullPath(text);
			fullPath = Path.GetFullPath(text);
			if (!fullPath.StartsWith(isf.Root))
			{
				throw new IsolatedStorageException();
			}
			fileInfo = new FileInfo(text);
			if (!fileInfo.Directory.Exists)
			{
				string text2 = Locale.GetText("Could not find a part of the path \"{0}\".");
				throw new DirectoryNotFoundException(string.Format(text2, path));
			}
			return text;
		}

		// Token: 0x1700055B RID: 1371
		// (get) Token: 0x06002035 RID: 8245 RVA: 0x00076F5C File Offset: 0x0007515C
		public override bool CanRead
		{
			get
			{
				return base.CanRead;
			}
		}

		// Token: 0x1700055C RID: 1372
		// (get) Token: 0x06002036 RID: 8246 RVA: 0x00076F64 File Offset: 0x00075164
		public override bool CanSeek
		{
			get
			{
				return base.CanSeek;
			}
		}

		// Token: 0x1700055D RID: 1373
		// (get) Token: 0x06002037 RID: 8247 RVA: 0x00076F6C File Offset: 0x0007516C
		public override bool CanWrite
		{
			get
			{
				return base.CanWrite;
			}
		}

		// Token: 0x1700055E RID: 1374
		// (get) Token: 0x06002038 RID: 8248 RVA: 0x00076F74 File Offset: 0x00075174
		public override SafeFileHandle SafeFileHandle
		{
			[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
			get
			{
				throw new IsolatedStorageException(Locale.GetText("Information is restricted"));
			}
		}

		// Token: 0x1700055F RID: 1375
		// (get) Token: 0x06002039 RID: 8249 RVA: 0x00076F88 File Offset: 0x00075188
		[Obsolete("Use SafeFileHandle - once available")]
		public override IntPtr Handle
		{
			[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
			get
			{
				throw new IsolatedStorageException(Locale.GetText("Information is restricted"));
			}
		}

		// Token: 0x17000560 RID: 1376
		// (get) Token: 0x0600203A RID: 8250 RVA: 0x00076F9C File Offset: 0x0007519C
		public override bool IsAsync
		{
			get
			{
				return base.IsAsync;
			}
		}

		// Token: 0x17000561 RID: 1377
		// (get) Token: 0x0600203B RID: 8251 RVA: 0x00076FA4 File Offset: 0x000751A4
		public override long Length
		{
			get
			{
				return base.Length;
			}
		}

		// Token: 0x17000562 RID: 1378
		// (get) Token: 0x0600203C RID: 8252 RVA: 0x00076FAC File Offset: 0x000751AC
		// (set) Token: 0x0600203D RID: 8253 RVA: 0x00076FB4 File Offset: 0x000751B4
		public override long Position
		{
			get
			{
				return base.Position;
			}
			set
			{
				base.Position = value;
			}
		}

		// Token: 0x0600203E RID: 8254 RVA: 0x00076FC0 File Offset: 0x000751C0
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int numBytes, AsyncCallback userCallback, object stateObject)
		{
			return base.BeginRead(buffer, offset, numBytes, userCallback, stateObject);
		}

		// Token: 0x0600203F RID: 8255 RVA: 0x00076FD0 File Offset: 0x000751D0
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int numBytes, AsyncCallback userCallback, object stateObject)
		{
			return base.BeginWrite(buffer, offset, numBytes, userCallback, stateObject);
		}

		// Token: 0x06002040 RID: 8256 RVA: 0x00076FE0 File Offset: 0x000751E0
		public override int EndRead(IAsyncResult asyncResult)
		{
			return base.EndRead(asyncResult);
		}

		// Token: 0x06002041 RID: 8257 RVA: 0x00076FEC File Offset: 0x000751EC
		public override void EndWrite(IAsyncResult asyncResult)
		{
			base.EndWrite(asyncResult);
		}

		// Token: 0x06002042 RID: 8258 RVA: 0x00076FF8 File Offset: 0x000751F8
		public override void Flush()
		{
			base.Flush();
		}

		// Token: 0x06002043 RID: 8259 RVA: 0x00077000 File Offset: 0x00075200
		public override int Read(byte[] buffer, int offset, int count)
		{
			return base.Read(buffer, offset, count);
		}

		// Token: 0x06002044 RID: 8260 RVA: 0x0007700C File Offset: 0x0007520C
		public override int ReadByte()
		{
			return base.ReadByte();
		}

		// Token: 0x06002045 RID: 8261 RVA: 0x00077014 File Offset: 0x00075214
		public override long Seek(long offset, SeekOrigin origin)
		{
			return base.Seek(offset, origin);
		}

		// Token: 0x06002046 RID: 8262 RVA: 0x00077020 File Offset: 0x00075220
		public override void SetLength(long value)
		{
			base.SetLength(value);
		}

		// Token: 0x06002047 RID: 8263 RVA: 0x0007702C File Offset: 0x0007522C
		public override void Write(byte[] buffer, int offset, int count)
		{
			base.Write(buffer, offset, count);
		}

		// Token: 0x06002048 RID: 8264 RVA: 0x00077038 File Offset: 0x00075238
		public override void WriteByte(byte value)
		{
			base.WriteByte(value);
		}

		// Token: 0x06002049 RID: 8265 RVA: 0x00077044 File Offset: 0x00075244
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}
	}
}
