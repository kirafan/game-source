﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.IO.IsolatedStorage
{
	// Token: 0x02000266 RID: 614
	[ComVisible(true)]
	[Serializable]
	public class IsolatedStorageException : Exception
	{
		// Token: 0x06001FFB RID: 8187 RVA: 0x00075DE4 File Offset: 0x00073FE4
		public IsolatedStorageException() : base(Locale.GetText("An Isolated storage operation failed."))
		{
		}

		// Token: 0x06001FFC RID: 8188 RVA: 0x00075DF8 File Offset: 0x00073FF8
		public IsolatedStorageException(string message) : base(message)
		{
		}

		// Token: 0x06001FFD RID: 8189 RVA: 0x00075E04 File Offset: 0x00074004
		public IsolatedStorageException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06001FFE RID: 8190 RVA: 0x00075E10 File Offset: 0x00074010
		protected IsolatedStorageException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
