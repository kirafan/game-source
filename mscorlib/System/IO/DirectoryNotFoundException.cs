﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.IO
{
	// Token: 0x02000234 RID: 564
	[ComVisible(true)]
	[Serializable]
	public class DirectoryNotFoundException : IOException
	{
		// Token: 0x06001D59 RID: 7513 RVA: 0x0006CA24 File Offset: 0x0006AC24
		public DirectoryNotFoundException() : base("Directory not found")
		{
		}

		// Token: 0x06001D5A RID: 7514 RVA: 0x0006CA34 File Offset: 0x0006AC34
		public DirectoryNotFoundException(string message) : base(message)
		{
		}

		// Token: 0x06001D5B RID: 7515 RVA: 0x0006CA40 File Offset: 0x0006AC40
		public DirectoryNotFoundException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06001D5C RID: 7516 RVA: 0x0006CA4C File Offset: 0x0006AC4C
		protected DirectoryNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
