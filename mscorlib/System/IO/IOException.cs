﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.IO
{
	// Token: 0x02000246 RID: 582
	[ComVisible(true)]
	[Serializable]
	public class IOException : SystemException
	{
		// Token: 0x06001E45 RID: 7749 RVA: 0x00070050 File Offset: 0x0006E250
		public IOException() : base("I/O Error")
		{
		}

		// Token: 0x06001E46 RID: 7750 RVA: 0x00070060 File Offset: 0x0006E260
		public IOException(string message) : base(message)
		{
		}

		// Token: 0x06001E47 RID: 7751 RVA: 0x0007006C File Offset: 0x0006E26C
		public IOException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06001E48 RID: 7752 RVA: 0x00070078 File Offset: 0x0006E278
		protected IOException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06001E49 RID: 7753 RVA: 0x00070084 File Offset: 0x0006E284
		public IOException(string message, int hresult) : base(message)
		{
			base.HResult = hresult;
		}
	}
}
