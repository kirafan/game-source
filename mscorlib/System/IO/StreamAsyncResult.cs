﻿using System;
using System.Threading;

namespace System.IO
{
	// Token: 0x02000255 RID: 597
	internal class StreamAsyncResult : IAsyncResult
	{
		// Token: 0x06001EE7 RID: 7911 RVA: 0x00072588 File Offset: 0x00070788
		public StreamAsyncResult(object state)
		{
			this.state = state;
		}

		// Token: 0x06001EE8 RID: 7912 RVA: 0x000725A0 File Offset: 0x000707A0
		public void SetComplete(Exception e)
		{
			this.exc = e;
			this.completed = true;
			lock (this)
			{
				if (this.wh != null)
				{
					this.wh.Set();
				}
			}
		}

		// Token: 0x06001EE9 RID: 7913 RVA: 0x00072604 File Offset: 0x00070804
		public void SetComplete(Exception e, int nbytes)
		{
			this.nbytes = nbytes;
			this.SetComplete(e);
		}

		// Token: 0x17000531 RID: 1329
		// (get) Token: 0x06001EEA RID: 7914 RVA: 0x00072614 File Offset: 0x00070814
		public object AsyncState
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x17000532 RID: 1330
		// (get) Token: 0x06001EEB RID: 7915 RVA: 0x0007261C File Offset: 0x0007081C
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				WaitHandle result;
				lock (this)
				{
					if (this.wh == null)
					{
						this.wh = new ManualResetEvent(this.completed);
					}
					result = this.wh;
				}
				return result;
			}
		}

		// Token: 0x17000533 RID: 1331
		// (get) Token: 0x06001EEC RID: 7916 RVA: 0x00072684 File Offset: 0x00070884
		public virtual bool CompletedSynchronously
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000534 RID: 1332
		// (get) Token: 0x06001EED RID: 7917 RVA: 0x00072688 File Offset: 0x00070888
		public bool IsCompleted
		{
			get
			{
				return this.completed;
			}
		}

		// Token: 0x17000535 RID: 1333
		// (get) Token: 0x06001EEE RID: 7918 RVA: 0x00072690 File Offset: 0x00070890
		public Exception Exception
		{
			get
			{
				return this.exc;
			}
		}

		// Token: 0x17000536 RID: 1334
		// (get) Token: 0x06001EEF RID: 7919 RVA: 0x00072698 File Offset: 0x00070898
		public int NBytes
		{
			get
			{
				return this.nbytes;
			}
		}

		// Token: 0x17000537 RID: 1335
		// (get) Token: 0x06001EF0 RID: 7920 RVA: 0x000726A0 File Offset: 0x000708A0
		// (set) Token: 0x06001EF1 RID: 7921 RVA: 0x000726A8 File Offset: 0x000708A8
		public bool Done
		{
			get
			{
				return this.done;
			}
			set
			{
				this.done = value;
			}
		}

		// Token: 0x04000BA4 RID: 2980
		private object state;

		// Token: 0x04000BA5 RID: 2981
		private bool completed;

		// Token: 0x04000BA6 RID: 2982
		private bool done;

		// Token: 0x04000BA7 RID: 2983
		private Exception exc;

		// Token: 0x04000BA8 RID: 2984
		private int nbytes = -1;

		// Token: 0x04000BA9 RID: 2985
		private ManualResetEvent wh;
	}
}
