﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x02000252 RID: 594
	[ComVisible(true)]
	[Serializable]
	public enum SeekOrigin
	{
		// Token: 0x04000BA0 RID: 2976
		Begin,
		// Token: 0x04000BA1 RID: 2977
		Current,
		// Token: 0x04000BA2 RID: 2978
		End
	}
}
