﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x02000259 RID: 601
	[ComVisible(true)]
	[Serializable]
	public class StringReader : TextReader
	{
		// Token: 0x06001F2F RID: 7983 RVA: 0x00073890 File Offset: 0x00071A90
		public StringReader(string s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			this.source = s;
			this.nextChar = 0;
			this.sourceLength = s.Length;
		}

		// Token: 0x06001F30 RID: 7984 RVA: 0x000738C4 File Offset: 0x00071AC4
		public override void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x06001F31 RID: 7985 RVA: 0x000738D0 File Offset: 0x00071AD0
		protected override void Dispose(bool disposing)
		{
			this.source = null;
			base.Dispose(disposing);
		}

		// Token: 0x06001F32 RID: 7986 RVA: 0x000738E0 File Offset: 0x00071AE0
		public override int Peek()
		{
			this.CheckObjectDisposedException();
			if (this.nextChar >= this.sourceLength)
			{
				return -1;
			}
			return (int)this.source[this.nextChar];
		}

		// Token: 0x06001F33 RID: 7987 RVA: 0x00073918 File Offset: 0x00071B18
		public override int Read()
		{
			this.CheckObjectDisposedException();
			if (this.nextChar >= this.sourceLength)
			{
				return -1;
			}
			return (int)this.source[this.nextChar++];
		}

		// Token: 0x06001F34 RID: 7988 RVA: 0x0007395C File Offset: 0x00071B5C
		public override int Read([In] [Out] char[] buffer, int index, int count)
		{
			this.CheckObjectDisposedException();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentException();
			}
			if (index < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			int num;
			if (this.nextChar > this.sourceLength - count)
			{
				num = this.sourceLength - this.nextChar;
			}
			else
			{
				num = count;
			}
			this.source.CopyTo(this.nextChar, buffer, index, num);
			this.nextChar += num;
			return num;
		}

		// Token: 0x06001F35 RID: 7989 RVA: 0x000739F0 File Offset: 0x00071BF0
		public override string ReadLine()
		{
			this.CheckObjectDisposedException();
			int i;
			for (i = this.nextChar; i < this.sourceLength; i++)
			{
				char c = this.source[i];
				if (c == '\r' || c == '\n')
				{
					string result = this.source.Substring(this.nextChar, i - this.nextChar);
					this.nextChar = i + 1;
					if (c == '\r' && this.nextChar < this.sourceLength && this.source[this.nextChar] == '\n')
					{
						this.nextChar++;
					}
					return result;
				}
			}
			if (i > this.nextChar)
			{
				string result2 = this.source.Substring(this.nextChar, i - this.nextChar);
				this.nextChar = i;
				return result2;
			}
			return null;
		}

		// Token: 0x06001F36 RID: 7990 RVA: 0x00073AD0 File Offset: 0x00071CD0
		public override string ReadToEnd()
		{
			this.CheckObjectDisposedException();
			string result = this.source.Substring(this.nextChar, this.sourceLength - this.nextChar);
			this.nextChar = this.sourceLength;
			return result;
		}

		// Token: 0x06001F37 RID: 7991 RVA: 0x00073B10 File Offset: 0x00071D10
		private void CheckObjectDisposedException()
		{
			if (this.source == null)
			{
				throw new ObjectDisposedException("StringReader", Locale.GetText("Cannot read from a closed StringReader"));
			}
		}

		// Token: 0x04000BC7 RID: 3015
		private string source;

		// Token: 0x04000BC8 RID: 3016
		private int nextChar;

		// Token: 0x04000BC9 RID: 3017
		private int sourceLength;
	}
}
