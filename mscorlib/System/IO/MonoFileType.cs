﻿using System;

namespace System.IO
{
	// Token: 0x0200024A RID: 586
	internal enum MonoFileType
	{
		// Token: 0x04000B7C RID: 2940
		Unknown,
		// Token: 0x04000B7D RID: 2941
		Disk,
		// Token: 0x04000B7E RID: 2942
		Char,
		// Token: 0x04000B7F RID: 2943
		Pipe,
		// Token: 0x04000B80 RID: 2944
		Remote = 32768
	}
}
