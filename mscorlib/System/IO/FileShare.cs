﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x02000242 RID: 578
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum FileShare
	{
		// Token: 0x04000B2F RID: 2863
		None = 0,
		// Token: 0x04000B30 RID: 2864
		Read = 1,
		// Token: 0x04000B31 RID: 2865
		Write = 2,
		// Token: 0x04000B32 RID: 2866
		ReadWrite = 3,
		// Token: 0x04000B33 RID: 2867
		Delete = 4,
		// Token: 0x04000B34 RID: 2868
		Inheritable = 16
	}
}
