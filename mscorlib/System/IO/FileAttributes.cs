﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x0200023C RID: 572
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum FileAttributes
	{
		// Token: 0x04000B09 RID: 2825
		Archive = 32,
		// Token: 0x04000B0A RID: 2826
		Compressed = 2048,
		// Token: 0x04000B0B RID: 2827
		Device = 64,
		// Token: 0x04000B0C RID: 2828
		Directory = 16,
		// Token: 0x04000B0D RID: 2829
		Encrypted = 16384,
		// Token: 0x04000B0E RID: 2830
		Hidden = 2,
		// Token: 0x04000B0F RID: 2831
		Normal = 128,
		// Token: 0x04000B10 RID: 2832
		NotContentIndexed = 8192,
		// Token: 0x04000B11 RID: 2833
		Offline = 4096,
		// Token: 0x04000B12 RID: 2834
		ReadOnly = 1,
		// Token: 0x04000B13 RID: 2835
		ReparsePoint = 1024,
		// Token: 0x04000B14 RID: 2836
		SparseFile = 512,
		// Token: 0x04000B15 RID: 2837
		System = 4,
		// Token: 0x04000B16 RID: 2838
		Temporary = 256
	}
}
