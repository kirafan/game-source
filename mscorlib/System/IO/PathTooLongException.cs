﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.IO
{
	// Token: 0x0200024D RID: 589
	[ComVisible(true)]
	[Serializable]
	public class PathTooLongException : IOException
	{
		// Token: 0x06001EB1 RID: 7857 RVA: 0x00071FA8 File Offset: 0x000701A8
		public PathTooLongException() : base(Locale.GetText("Pathname is longer than the maximum length"))
		{
		}

		// Token: 0x06001EB2 RID: 7858 RVA: 0x00071FBC File Offset: 0x000701BC
		public PathTooLongException(string message) : base(message)
		{
		}

		// Token: 0x06001EB3 RID: 7859 RVA: 0x00071FC8 File Offset: 0x000701C8
		protected PathTooLongException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06001EB4 RID: 7860 RVA: 0x00071FD4 File Offset: 0x000701D4
		public PathTooLongException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
