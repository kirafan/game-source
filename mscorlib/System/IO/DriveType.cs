﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x02000238 RID: 568
	[ComVisible(true)]
	[Serializable]
	public enum DriveType
	{
		// Token: 0x04000AFC RID: 2812
		CDRom = 5,
		// Token: 0x04000AFD RID: 2813
		Fixed = 3,
		// Token: 0x04000AFE RID: 2814
		Network,
		// Token: 0x04000AFF RID: 2815
		NoRootDirectory = 1,
		// Token: 0x04000B00 RID: 2816
		Ram = 6,
		// Token: 0x04000B01 RID: 2817
		Removable = 2,
		// Token: 0x04000B02 RID: 2818
		Unknown = 0
	}
}
