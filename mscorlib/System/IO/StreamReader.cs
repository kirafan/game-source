﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.IO
{
	// Token: 0x02000256 RID: 598
	[ComVisible(true)]
	[Serializable]
	public class StreamReader : TextReader
	{
		// Token: 0x06001EF2 RID: 7922 RVA: 0x000726B4 File Offset: 0x000708B4
		internal StreamReader()
		{
		}

		// Token: 0x06001EF3 RID: 7923 RVA: 0x000726BC File Offset: 0x000708BC
		public StreamReader(Stream stream) : this(stream, Encoding.UTF8Unmarked, true, 1024)
		{
		}

		// Token: 0x06001EF4 RID: 7924 RVA: 0x000726D0 File Offset: 0x000708D0
		public StreamReader(Stream stream, bool detectEncodingFromByteOrderMarks) : this(stream, Encoding.UTF8Unmarked, detectEncodingFromByteOrderMarks, 1024)
		{
		}

		// Token: 0x06001EF5 RID: 7925 RVA: 0x000726E4 File Offset: 0x000708E4
		public StreamReader(Stream stream, Encoding encoding) : this(stream, encoding, true, 1024)
		{
		}

		// Token: 0x06001EF6 RID: 7926 RVA: 0x000726F4 File Offset: 0x000708F4
		public StreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks) : this(stream, encoding, detectEncodingFromByteOrderMarks, 1024)
		{
		}

		// Token: 0x06001EF7 RID: 7927 RVA: 0x00072704 File Offset: 0x00070904
		public StreamReader(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
		{
			this.Initialize(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize);
		}

		// Token: 0x06001EF8 RID: 7928 RVA: 0x00072718 File Offset: 0x00070918
		public StreamReader(string path) : this(path, Encoding.UTF8Unmarked, true, 4096)
		{
		}

		// Token: 0x06001EF9 RID: 7929 RVA: 0x0007272C File Offset: 0x0007092C
		public StreamReader(string path, bool detectEncodingFromByteOrderMarks) : this(path, Encoding.UTF8Unmarked, detectEncodingFromByteOrderMarks, 4096)
		{
		}

		// Token: 0x06001EFA RID: 7930 RVA: 0x00072740 File Offset: 0x00070940
		public StreamReader(string path, Encoding encoding) : this(path, encoding, true, 4096)
		{
		}

		// Token: 0x06001EFB RID: 7931 RVA: 0x00072750 File Offset: 0x00070950
		public StreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks) : this(path, encoding, detectEncodingFromByteOrderMarks, 4096)
		{
		}

		// Token: 0x06001EFC RID: 7932 RVA: 0x00072760 File Offset: 0x00070960
		public StreamReader(string path, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (string.Empty == path)
			{
				throw new ArgumentException("Empty path not allowed");
			}
			if (path.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("path contains invalid characters");
			}
			if (encoding == null)
			{
				throw new ArgumentNullException("encoding");
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize", "The minimum size of the buffer must be positive");
			}
			Stream stream = File.OpenRead(path);
			this.Initialize(stream, encoding, detectEncodingFromByteOrderMarks, bufferSize);
		}

		// Token: 0x06001EFE RID: 7934 RVA: 0x00072804 File Offset: 0x00070A04
		internal void Initialize(Stream stream, Encoding encoding, bool detectEncodingFromByteOrderMarks, int bufferSize)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (encoding == null)
			{
				throw new ArgumentNullException("encoding");
			}
			if (!stream.CanRead)
			{
				throw new ArgumentException("Cannot read stream");
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize", "The minimum size of the buffer must be positive");
			}
			if (bufferSize < 128)
			{
				bufferSize = 128;
			}
			this.base_stream = stream;
			this.input_buffer = new byte[bufferSize];
			this.buffer_size = bufferSize;
			this.encoding = encoding;
			this.decoder = encoding.GetDecoder();
			byte[] preamble = encoding.GetPreamble();
			this.do_checks = ((!detectEncodingFromByteOrderMarks) ? 0 : 1);
			this.do_checks += ((preamble.Length != 0) ? 2 : 0);
			this.decoded_buffer = new char[encoding.GetMaxCharCount(bufferSize) + 1];
			this.decoded_count = 0;
			this.pos = 0;
		}

		// Token: 0x17000538 RID: 1336
		// (get) Token: 0x06001EFF RID: 7935 RVA: 0x000728FC File Offset: 0x00070AFC
		public virtual Stream BaseStream
		{
			get
			{
				return this.base_stream;
			}
		}

		// Token: 0x17000539 RID: 1337
		// (get) Token: 0x06001F00 RID: 7936 RVA: 0x00072904 File Offset: 0x00070B04
		public virtual Encoding CurrentEncoding
		{
			get
			{
				if (this.encoding == null)
				{
					throw new Exception();
				}
				return this.encoding;
			}
		}

		// Token: 0x1700053A RID: 1338
		// (get) Token: 0x06001F01 RID: 7937 RVA: 0x00072920 File Offset: 0x00070B20
		public bool EndOfStream
		{
			get
			{
				return this.Peek() < 0;
			}
		}

		// Token: 0x06001F02 RID: 7938 RVA: 0x0007292C File Offset: 0x00070B2C
		public override void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x06001F03 RID: 7939 RVA: 0x00072938 File Offset: 0x00070B38
		protected override void Dispose(bool disposing)
		{
			if (disposing && this.base_stream != null)
			{
				this.base_stream.Close();
			}
			this.input_buffer = null;
			this.decoded_buffer = null;
			this.encoding = null;
			this.decoder = null;
			this.base_stream = null;
			base.Dispose(disposing);
		}

		// Token: 0x06001F04 RID: 7940 RVA: 0x0007298C File Offset: 0x00070B8C
		private int DoChecks(int count)
		{
			if ((this.do_checks & 2) == 2)
			{
				byte[] preamble = this.encoding.GetPreamble();
				int num = preamble.Length;
				if (count >= num)
				{
					int i;
					for (i = 0; i < num; i++)
					{
						if (this.input_buffer[i] != preamble[i])
						{
							break;
						}
					}
					if (i == num)
					{
						return i;
					}
				}
			}
			if ((this.do_checks & 1) == 1)
			{
				if (count < 2)
				{
					return 0;
				}
				if (this.input_buffer[0] == 254 && this.input_buffer[1] == 255)
				{
					this.encoding = Encoding.BigEndianUnicode;
					return 2;
				}
				if (count < 3)
				{
					return 0;
				}
				if (this.input_buffer[0] == 239 && this.input_buffer[1] == 187 && this.input_buffer[2] == 191)
				{
					this.encoding = Encoding.UTF8Unmarked;
					return 3;
				}
				if (count < 4)
				{
					if (this.input_buffer[0] == 255 && this.input_buffer[1] == 254 && this.input_buffer[2] != 0)
					{
						this.encoding = Encoding.Unicode;
						return 2;
					}
					return 0;
				}
				else
				{
					if (this.input_buffer[0] == 0 && this.input_buffer[1] == 0 && this.input_buffer[2] == 254 && this.input_buffer[3] == 255)
					{
						this.encoding = Encoding.BigEndianUTF32;
						return 4;
					}
					if (this.input_buffer[0] == 255 && this.input_buffer[1] == 254)
					{
						if (this.input_buffer[2] == 0 && this.input_buffer[3] == 0)
						{
							this.encoding = Encoding.UTF32;
							return 4;
						}
						this.encoding = Encoding.Unicode;
						return 2;
					}
				}
			}
			return 0;
		}

		// Token: 0x06001F05 RID: 7941 RVA: 0x00072B70 File Offset: 0x00070D70
		public void DiscardBufferedData()
		{
			this.pos = (this.decoded_count = 0);
			this.mayBlock = false;
			this.decoder = this.encoding.GetDecoder();
		}

		// Token: 0x06001F06 RID: 7942 RVA: 0x00072BA8 File Offset: 0x00070DA8
		private int ReadBuffer()
		{
			this.pos = 0;
			this.decoded_count = 0;
			int num = 0;
			for (;;)
			{
				int num2 = this.base_stream.Read(this.input_buffer, 0, this.buffer_size);
				if (num2 <= 0)
				{
					break;
				}
				this.mayBlock = (num2 < this.buffer_size);
				if (this.do_checks > 0)
				{
					Encoding encoding = this.encoding;
					num = this.DoChecks(num2);
					if (encoding != this.encoding)
					{
						int num3 = encoding.GetMaxCharCount(this.buffer_size) + 1;
						int num4 = this.encoding.GetMaxCharCount(this.buffer_size) + 1;
						if (num3 != num4)
						{
							this.decoded_buffer = new char[num4];
						}
						this.decoder = this.encoding.GetDecoder();
					}
					this.do_checks = 0;
					num2 -= num;
				}
				this.decoded_count += this.decoder.GetChars(this.input_buffer, num, num2, this.decoded_buffer, 0);
				num = 0;
				if (this.decoded_count != 0)
				{
					goto Block_5;
				}
			}
			return 0;
			Block_5:
			return this.decoded_count;
		}

		// Token: 0x06001F07 RID: 7943 RVA: 0x00072CB0 File Offset: 0x00070EB0
		public override int Peek()
		{
			if (this.base_stream == null)
			{
				throw new ObjectDisposedException("StreamReader", "Cannot read from a closed StreamReader");
			}
			if (this.pos >= this.decoded_count && this.ReadBuffer() == 0)
			{
				return -1;
			}
			return (int)this.decoded_buffer[this.pos];
		}

		// Token: 0x06001F08 RID: 7944 RVA: 0x00072D04 File Offset: 0x00070F04
		internal bool DataAvailable()
		{
			return this.pos < this.decoded_count;
		}

		// Token: 0x06001F09 RID: 7945 RVA: 0x00072D14 File Offset: 0x00070F14
		public override int Read()
		{
			if (this.base_stream == null)
			{
				throw new ObjectDisposedException("StreamReader", "Cannot read from a closed StreamReader");
			}
			if (this.pos >= this.decoded_count && this.ReadBuffer() == 0)
			{
				return -1;
			}
			return (int)this.decoded_buffer[this.pos++];
		}

		// Token: 0x06001F0A RID: 7946 RVA: 0x00072D74 File Offset: 0x00070F74
		public override int Read([In] [Out] char[] buffer, int index, int count)
		{
			if (this.base_stream == null)
			{
				throw new ObjectDisposedException("StreamReader", "Cannot read from a closed StreamReader");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			if (index > buffer.Length - count)
			{
				throw new ArgumentException("index + count > buffer.Length");
			}
			int num = 0;
			while (count > 0)
			{
				if (this.pos >= this.decoded_count && this.ReadBuffer() == 0)
				{
					return (num <= 0) ? 0 : num;
				}
				int num2 = Math.Min(this.decoded_count - this.pos, count);
				Array.Copy(this.decoded_buffer, this.pos, buffer, index, num2);
				this.pos += num2;
				index += num2;
				count -= num2;
				num += num2;
				if (this.mayBlock)
				{
					break;
				}
			}
			return num;
		}

		// Token: 0x06001F0B RID: 7947 RVA: 0x00072E80 File Offset: 0x00071080
		private int FindNextEOL()
		{
			while (this.pos < this.decoded_count)
			{
				char c = this.decoded_buffer[this.pos];
				if (c == '\n')
				{
					this.pos++;
					int num = (!this.foundCR) ? (this.pos - 1) : (this.pos - 2);
					if (num < 0)
					{
						num = 0;
					}
					this.foundCR = false;
					return num;
				}
				if (this.foundCR)
				{
					this.foundCR = false;
					if (this.pos == 0)
					{
						return -2;
					}
					return this.pos - 1;
				}
				else
				{
					this.foundCR = (c == '\r');
					this.pos++;
				}
			}
			return -1;
		}

		// Token: 0x06001F0C RID: 7948 RVA: 0x00072F40 File Offset: 0x00071140
		public override string ReadLine()
		{
			if (this.base_stream == null)
			{
				throw new ObjectDisposedException("StreamReader", "Cannot read from a closed StreamReader");
			}
			if (this.pos >= this.decoded_count && this.ReadBuffer() == 0)
			{
				return null;
			}
			int num = this.pos;
			int num2 = this.FindNextEOL();
			if (num2 < this.decoded_count && num2 >= num)
			{
				return new string(this.decoded_buffer, num, num2 - num);
			}
			if (num2 == -2)
			{
				return this.line_builder.ToString(0, this.line_builder.Length);
			}
			if (this.line_builder == null)
			{
				this.line_builder = new StringBuilder();
			}
			else
			{
				this.line_builder.Length = 0;
			}
			for (;;)
			{
				if (this.foundCR)
				{
					this.decoded_count--;
				}
				this.line_builder.Append(this.decoded_buffer, num, this.decoded_count - num);
				if (this.ReadBuffer() == 0)
				{
					break;
				}
				num = this.pos;
				num2 = this.FindNextEOL();
				if (num2 < this.decoded_count && num2 >= num)
				{
					goto Block_12;
				}
				if (num2 == -2)
				{
					goto Block_14;
				}
			}
			if (this.line_builder.Capacity > 32768)
			{
				StringBuilder stringBuilder = this.line_builder;
				this.line_builder = null;
				return stringBuilder.ToString(0, stringBuilder.Length);
			}
			return this.line_builder.ToString(0, this.line_builder.Length);
			Block_12:
			this.line_builder.Append(this.decoded_buffer, num, num2 - num);
			if (this.line_builder.Capacity > 32768)
			{
				StringBuilder stringBuilder2 = this.line_builder;
				this.line_builder = null;
				return stringBuilder2.ToString(0, stringBuilder2.Length);
			}
			return this.line_builder.ToString(0, this.line_builder.Length);
			Block_14:
			return this.line_builder.ToString(0, this.line_builder.Length);
		}

		// Token: 0x06001F0D RID: 7949 RVA: 0x0007312C File Offset: 0x0007132C
		public override string ReadToEnd()
		{
			if (this.base_stream == null)
			{
				throw new ObjectDisposedException("StreamReader", "Cannot read from a closed StreamReader");
			}
			StringBuilder stringBuilder = new StringBuilder();
			int num = this.decoded_buffer.Length;
			char[] array = new char[num];
			int charCount;
			while ((charCount = this.Read(array, 0, num)) > 0)
			{
				stringBuilder.Append(array, 0, charCount);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04000BAA RID: 2986
		private const int DefaultBufferSize = 1024;

		// Token: 0x04000BAB RID: 2987
		private const int DefaultFileBufferSize = 4096;

		// Token: 0x04000BAC RID: 2988
		private const int MinimumBufferSize = 128;

		// Token: 0x04000BAD RID: 2989
		private byte[] input_buffer;

		// Token: 0x04000BAE RID: 2990
		private char[] decoded_buffer;

		// Token: 0x04000BAF RID: 2991
		private int decoded_count;

		// Token: 0x04000BB0 RID: 2992
		private int pos;

		// Token: 0x04000BB1 RID: 2993
		private int buffer_size;

		// Token: 0x04000BB2 RID: 2994
		private int do_checks;

		// Token: 0x04000BB3 RID: 2995
		private Encoding encoding;

		// Token: 0x04000BB4 RID: 2996
		private Decoder decoder;

		// Token: 0x04000BB5 RID: 2997
		private Stream base_stream;

		// Token: 0x04000BB6 RID: 2998
		private bool mayBlock;

		// Token: 0x04000BB7 RID: 2999
		private StringBuilder line_builder;

		// Token: 0x04000BB8 RID: 3000
		public new static readonly StreamReader Null = new StreamReader.NullStreamReader();

		// Token: 0x04000BB9 RID: 3001
		private bool foundCR;

		// Token: 0x02000257 RID: 599
		private class NullStreamReader : StreamReader
		{
			// Token: 0x06001F0F RID: 7951 RVA: 0x00073198 File Offset: 0x00071398
			public override int Peek()
			{
				return -1;
			}

			// Token: 0x06001F10 RID: 7952 RVA: 0x0007319C File Offset: 0x0007139C
			public override int Read()
			{
				return -1;
			}

			// Token: 0x06001F11 RID: 7953 RVA: 0x000731A0 File Offset: 0x000713A0
			public override int Read([In] [Out] char[] buffer, int index, int count)
			{
				return 0;
			}

			// Token: 0x06001F12 RID: 7954 RVA: 0x000731A4 File Offset: 0x000713A4
			public override string ReadLine()
			{
				return null;
			}

			// Token: 0x06001F13 RID: 7955 RVA: 0x000731A8 File Offset: 0x000713A8
			public override string ReadToEnd()
			{
				return string.Empty;
			}

			// Token: 0x1700053B RID: 1339
			// (get) Token: 0x06001F14 RID: 7956 RVA: 0x000731B0 File Offset: 0x000713B0
			public override Stream BaseStream
			{
				get
				{
					return Stream.Null;
				}
			}

			// Token: 0x1700053C RID: 1340
			// (get) Token: 0x06001F15 RID: 7957 RVA: 0x000731B8 File Offset: 0x000713B8
			public override Encoding CurrentEncoding
			{
				get
				{
					return Encoding.Unicode;
				}
			}
		}
	}
}
