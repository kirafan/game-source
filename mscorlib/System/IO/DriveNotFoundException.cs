﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.IO
{
	// Token: 0x02000237 RID: 567
	[ComVisible(true)]
	[Serializable]
	public class DriveNotFoundException : IOException
	{
		// Token: 0x06001D73 RID: 7539 RVA: 0x0006CE6C File Offset: 0x0006B06C
		public DriveNotFoundException() : base("Attempted to access a drive that is not available.")
		{
			base.HResult = -2147024893;
		}

		// Token: 0x06001D74 RID: 7540 RVA: 0x0006CE84 File Offset: 0x0006B084
		public DriveNotFoundException(string message) : base(message)
		{
			base.HResult = -2147024893;
		}

		// Token: 0x06001D75 RID: 7541 RVA: 0x0006CE98 File Offset: 0x0006B098
		public DriveNotFoundException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147024893;
		}

		// Token: 0x06001D76 RID: 7542 RVA: 0x0006CEB0 File Offset: 0x0006B0B0
		protected DriveNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000AFA RID: 2810
		private const int ErrorCode = -2147024893;
	}
}
