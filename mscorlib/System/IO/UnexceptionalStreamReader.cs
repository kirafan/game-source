﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.IO
{
	// Token: 0x02000261 RID: 609
	internal class UnexceptionalStreamReader : StreamReader
	{
		// Token: 0x06001FB9 RID: 8121 RVA: 0x00074FFC File Offset: 0x000731FC
		public UnexceptionalStreamReader(Stream stream) : base(stream)
		{
		}

		// Token: 0x06001FBA RID: 8122 RVA: 0x00075008 File Offset: 0x00073208
		public UnexceptionalStreamReader(Stream stream, bool detect_encoding_from_bytemarks) : base(stream, detect_encoding_from_bytemarks)
		{
		}

		// Token: 0x06001FBB RID: 8123 RVA: 0x00075014 File Offset: 0x00073214
		public UnexceptionalStreamReader(Stream stream, Encoding encoding) : base(stream, encoding)
		{
		}

		// Token: 0x06001FBC RID: 8124 RVA: 0x00075020 File Offset: 0x00073220
		public UnexceptionalStreamReader(Stream stream, Encoding encoding, bool detect_encoding_from_bytemarks) : base(stream, encoding, detect_encoding_from_bytemarks)
		{
		}

		// Token: 0x06001FBD RID: 8125 RVA: 0x0007502C File Offset: 0x0007322C
		public UnexceptionalStreamReader(Stream stream, Encoding encoding, bool detect_encoding_from_bytemarks, int buffer_size) : base(stream, encoding, detect_encoding_from_bytemarks, buffer_size)
		{
		}

		// Token: 0x06001FBE RID: 8126 RVA: 0x0007503C File Offset: 0x0007323C
		public UnexceptionalStreamReader(string path) : base(path)
		{
		}

		// Token: 0x06001FBF RID: 8127 RVA: 0x00075048 File Offset: 0x00073248
		public UnexceptionalStreamReader(string path, bool detect_encoding_from_bytemarks) : base(path, detect_encoding_from_bytemarks)
		{
		}

		// Token: 0x06001FC0 RID: 8128 RVA: 0x00075054 File Offset: 0x00073254
		public UnexceptionalStreamReader(string path, Encoding encoding) : base(path, encoding)
		{
		}

		// Token: 0x06001FC1 RID: 8129 RVA: 0x00075060 File Offset: 0x00073260
		public UnexceptionalStreamReader(string path, Encoding encoding, bool detect_encoding_from_bytemarks) : base(path, encoding, detect_encoding_from_bytemarks)
		{
		}

		// Token: 0x06001FC2 RID: 8130 RVA: 0x0007506C File Offset: 0x0007326C
		public UnexceptionalStreamReader(string path, Encoding encoding, bool detect_encoding_from_bytemarks, int buffer_size) : base(path, encoding, detect_encoding_from_bytemarks, buffer_size)
		{
		}

		// Token: 0x06001FC3 RID: 8131 RVA: 0x0007507C File Offset: 0x0007327C
		static UnexceptionalStreamReader()
		{
			string newLine = Environment.NewLine;
			if (newLine.Length == 1)
			{
				UnexceptionalStreamReader.newlineChar = newLine[0];
			}
		}

		// Token: 0x06001FC4 RID: 8132 RVA: 0x000750BC File Offset: 0x000732BC
		public override int Peek()
		{
			try
			{
				return base.Peek();
			}
			catch (IOException)
			{
			}
			return -1;
		}

		// Token: 0x06001FC5 RID: 8133 RVA: 0x00075100 File Offset: 0x00073300
		public override int Read()
		{
			try
			{
				return base.Read();
			}
			catch (IOException)
			{
			}
			return -1;
		}

		// Token: 0x06001FC6 RID: 8134 RVA: 0x00075144 File Offset: 0x00073344
		public override int Read([In] [Out] char[] dest_buffer, int index, int count)
		{
			if (dest_buffer == null)
			{
				throw new ArgumentNullException("dest_buffer");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			if (index > dest_buffer.Length - count)
			{
				throw new ArgumentException("index + count > dest_buffer.Length");
			}
			int num = 0;
			char c = UnexceptionalStreamReader.newlineChar;
			try
			{
				while (count > 0)
				{
					int num2 = base.Read();
					if (num2 < 0)
					{
						break;
					}
					num++;
					count--;
					dest_buffer[index] = (char)num2;
					if (c != '\0')
					{
						if ((char)num2 == c)
						{
							return num;
						}
					}
					else if (this.CheckEOL((char)num2))
					{
						return num;
					}
					index++;
				}
			}
			catch (IOException)
			{
			}
			return num;
		}

		// Token: 0x06001FC7 RID: 8135 RVA: 0x00075238 File Offset: 0x00073438
		private bool CheckEOL(char current)
		{
			int i = 0;
			while (i < UnexceptionalStreamReader.newline.Length)
			{
				if (!UnexceptionalStreamReader.newline[i])
				{
					if (current == Environment.NewLine[i])
					{
						UnexceptionalStreamReader.newline[i] = true;
						return i == UnexceptionalStreamReader.newline.Length - 1;
					}
					break;
				}
				else
				{
					i++;
				}
			}
			for (int j = 0; j < UnexceptionalStreamReader.newline.Length; j++)
			{
				UnexceptionalStreamReader.newline[j] = false;
			}
			return false;
		}

		// Token: 0x06001FC8 RID: 8136 RVA: 0x000752B8 File Offset: 0x000734B8
		public override string ReadLine()
		{
			try
			{
				return base.ReadLine();
			}
			catch (IOException)
			{
			}
			return null;
		}

		// Token: 0x06001FC9 RID: 8137 RVA: 0x000752FC File Offset: 0x000734FC
		public override string ReadToEnd()
		{
			try
			{
				return base.ReadToEnd();
			}
			catch (IOException)
			{
			}
			return null;
		}

		// Token: 0x04000BD3 RID: 3027
		private static bool[] newline = new bool[Environment.NewLine.Length];

		// Token: 0x04000BD4 RID: 3028
		private static char newlineChar;
	}
}
