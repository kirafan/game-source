﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x0200023B RID: 571
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum FileAccess
	{
		// Token: 0x04000B05 RID: 2821
		Read = 1,
		// Token: 0x04000B06 RID: 2822
		Write = 2,
		// Token: 0x04000B07 RID: 2823
		ReadWrite = 3
	}
}
