﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text;

namespace System.IO
{
	// Token: 0x02000235 RID: 565
	[ComVisible(true)]
	[Serializable]
	public sealed class DriveInfo : ISerializable
	{
		// Token: 0x06001D5D RID: 7517 RVA: 0x0006CA58 File Offset: 0x0006AC58
		private DriveInfo(DriveInfo._DriveType _drive_type, string path, string fstype)
		{
			this._drive_type = _drive_type;
			this.drive_format = fstype;
			this.path = path;
		}

		// Token: 0x06001D5E RID: 7518 RVA: 0x0006CA78 File Offset: 0x0006AC78
		public DriveInfo(string driveName)
		{
			DriveInfo[] drives = DriveInfo.GetDrives();
			foreach (DriveInfo driveInfo in drives)
			{
				if (driveInfo.path == driveName)
				{
					this.path = driveInfo.path;
					this.drive_format = driveInfo.drive_format;
					this.path = driveInfo.path;
					return;
				}
			}
			throw new ArgumentException("The drive name does not exist", "driveName");
		}

		// Token: 0x06001D5F RID: 7519 RVA: 0x0006CAF0 File Offset: 0x0006ACF0
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001D60 RID: 7520 RVA: 0x0006CAF8 File Offset: 0x0006ACF8
		private static void GetDiskFreeSpace(string path, out ulong availableFreeSpace, out ulong totalSize, out ulong totalFreeSpace)
		{
			MonoIOError error;
			if (!DriveInfo.GetDiskFreeSpaceInternal(path, out availableFreeSpace, out totalSize, out totalFreeSpace, out error))
			{
				throw MonoIO.GetException(path, error);
			}
		}

		// Token: 0x170004E7 RID: 1255
		// (get) Token: 0x06001D61 RID: 7521 RVA: 0x0006CB20 File Offset: 0x0006AD20
		public long AvailableFreeSpace
		{
			get
			{
				ulong num;
				ulong num2;
				ulong num3;
				DriveInfo.GetDiskFreeSpace(this.path, out num, out num2, out num3);
				return (long)((num <= 9223372036854775807UL) ? num : 9223372036854775807UL);
			}
		}

		// Token: 0x170004E8 RID: 1256
		// (get) Token: 0x06001D62 RID: 7522 RVA: 0x0006CB5C File Offset: 0x0006AD5C
		public long TotalFreeSpace
		{
			get
			{
				ulong num;
				ulong num2;
				ulong num3;
				DriveInfo.GetDiskFreeSpace(this.path, out num, out num2, out num3);
				return (long)((num3 <= 9223372036854775807UL) ? num3 : 9223372036854775807UL);
			}
		}

		// Token: 0x170004E9 RID: 1257
		// (get) Token: 0x06001D63 RID: 7523 RVA: 0x0006CB98 File Offset: 0x0006AD98
		public long TotalSize
		{
			get
			{
				ulong num;
				ulong num2;
				ulong num3;
				DriveInfo.GetDiskFreeSpace(this.path, out num, out num2, out num3);
				return (long)((num2 <= 9223372036854775807UL) ? num2 : 9223372036854775807UL);
			}
		}

		// Token: 0x170004EA RID: 1258
		// (get) Token: 0x06001D64 RID: 7524 RVA: 0x0006CBD4 File Offset: 0x0006ADD4
		// (set) Token: 0x06001D65 RID: 7525 RVA: 0x0006CBF0 File Offset: 0x0006ADF0
		[MonoTODO("Currently get only works on Mono/Unix; set not implemented")]
		public string VolumeLabel
		{
			get
			{
				if (this._drive_type != DriveInfo._DriveType.Windows)
				{
					return this.path;
				}
				return this.path;
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170004EB RID: 1259
		// (get) Token: 0x06001D66 RID: 7526 RVA: 0x0006CBF8 File Offset: 0x0006ADF8
		public string DriveFormat
		{
			get
			{
				return this.drive_format;
			}
		}

		// Token: 0x170004EC RID: 1260
		// (get) Token: 0x06001D67 RID: 7527 RVA: 0x0006CC00 File Offset: 0x0006AE00
		public DriveType DriveType
		{
			get
			{
				return (DriveType)DriveInfo.GetDriveTypeInternal(this.path);
			}
		}

		// Token: 0x170004ED RID: 1261
		// (get) Token: 0x06001D68 RID: 7528 RVA: 0x0006CC10 File Offset: 0x0006AE10
		public string Name
		{
			get
			{
				return this.path;
			}
		}

		// Token: 0x170004EE RID: 1262
		// (get) Token: 0x06001D69 RID: 7529 RVA: 0x0006CC18 File Offset: 0x0006AE18
		public DirectoryInfo RootDirectory
		{
			get
			{
				return new DirectoryInfo(this.path);
			}
		}

		// Token: 0x170004EF RID: 1263
		// (get) Token: 0x06001D6A RID: 7530 RVA: 0x0006CC28 File Offset: 0x0006AE28
		[MonoTODO("It always returns true")]
		public bool IsReady
		{
			get
			{
				return this._drive_type == DriveInfo._DriveType.Windows || true;
			}
		}

		// Token: 0x06001D6B RID: 7531 RVA: 0x0006CC3C File Offset: 0x0006AE3C
		private static StreamReader TryOpen(string name)
		{
			if (File.Exists(name))
			{
				return new StreamReader(name, Encoding.ASCII);
			}
			return null;
		}

		// Token: 0x06001D6C RID: 7532 RVA: 0x0006CC58 File Offset: 0x0006AE58
		private static DriveInfo[] LinuxGetDrives()
		{
			DriveInfo[] result;
			using (StreamReader streamReader = DriveInfo.TryOpen("/proc/mounts"))
			{
				ArrayList arrayList = new ArrayList();
				string text;
				while ((text = streamReader.ReadLine()) != null)
				{
					if (!text.StartsWith("rootfs"))
					{
						int num = text.IndexOf(' ');
						if (num != -1)
						{
							string text2 = text.Substring(num + 1);
							num = text2.IndexOf(' ');
							if (num != -1)
							{
								string text3 = text2.Substring(0, num);
								text2 = text2.Substring(num + 1);
								num = text2.IndexOf(' ');
								if (num != -1)
								{
									string fstype = text2.Substring(0, num);
									arrayList.Add(new DriveInfo(DriveInfo._DriveType.Linux, text3, fstype));
								}
							}
						}
					}
				}
				result = (DriveInfo[])arrayList.ToArray(typeof(DriveInfo));
			}
			return result;
		}

		// Token: 0x06001D6D RID: 7533 RVA: 0x0006CD68 File Offset: 0x0006AF68
		private static DriveInfo[] UnixGetDrives()
		{
			DriveInfo[] array = null;
			try
			{
				using (StreamReader streamReader = DriveInfo.TryOpen("/proc/sys/kernel/ostype"))
				{
					if (streamReader != null)
					{
						string a = streamReader.ReadLine();
						if (a == "Linux")
						{
							array = DriveInfo.LinuxGetDrives();
						}
					}
				}
				if (array != null)
				{
					return array;
				}
			}
			catch (Exception)
			{
			}
			return new DriveInfo[]
			{
				new DriveInfo(DriveInfo._DriveType.GenericUnix, "/", "unixfs")
			};
		}

		// Token: 0x06001D6E RID: 7534 RVA: 0x0006CE24 File Offset: 0x0006B024
		private static DriveInfo[] WindowsGetDrives()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001D6F RID: 7535 RVA: 0x0006CE2C File Offset: 0x0006B02C
		[MonoTODO("Currently only implemented on Mono/Linux")]
		public static DriveInfo[] GetDrives()
		{
			int platform = (int)Environment.Platform;
			if (platform == 4 || platform == 128 || platform == 6)
			{
				return DriveInfo.UnixGetDrives();
			}
			return DriveInfo.WindowsGetDrives();
		}

		// Token: 0x06001D70 RID: 7536 RVA: 0x0006CE64 File Offset: 0x0006B064
		public override string ToString()
		{
			return this.Name;
		}

		// Token: 0x06001D71 RID: 7537
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool GetDiskFreeSpaceInternal(string pathName, out ulong freeBytesAvail, out ulong totalNumberOfBytes, out ulong totalNumberOfFreeBytes, out MonoIOError error);

		// Token: 0x06001D72 RID: 7538
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern uint GetDriveTypeInternal(string rootPathName);

		// Token: 0x04000AF3 RID: 2803
		private DriveInfo._DriveType _drive_type;

		// Token: 0x04000AF4 RID: 2804
		private string drive_format;

		// Token: 0x04000AF5 RID: 2805
		private string path;

		// Token: 0x02000236 RID: 566
		private enum _DriveType
		{
			// Token: 0x04000AF7 RID: 2807
			GenericUnix,
			// Token: 0x04000AF8 RID: 2808
			Linux,
			// Token: 0x04000AF9 RID: 2809
			Windows
		}
	}
}
