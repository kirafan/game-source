﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.AccessControl;
using System.Security.Permissions;

namespace System.IO
{
	// Token: 0x02000232 RID: 562
	[ComVisible(true)]
	public static class Directory
	{
		// Token: 0x06001D14 RID: 7444 RVA: 0x0006B938 File Offset: 0x00069B38
		public static DirectoryInfo CreateDirectory(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException("Path is empty");
			}
			if (path.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Path contains invalid chars");
			}
			if (path.Trim().Length == 0)
			{
				throw new ArgumentException("Only blank characters in path");
			}
			if (File.Exists(path))
			{
				throw new IOException("Cannot create " + path + " because a file with the same name already exists.");
			}
			if (path == ":")
			{
				throw new ArgumentException("Only ':' In path");
			}
			return Directory.CreateDirectoriesInternal(path);
		}

		// Token: 0x06001D15 RID: 7445 RVA: 0x0006B9E8 File Offset: 0x00069BE8
		[MonoTODO("DirectorySecurity not implemented")]
		public static DirectoryInfo CreateDirectory(string path, DirectorySecurity directorySecurity)
		{
			return Directory.CreateDirectory(path);
		}

		// Token: 0x06001D16 RID: 7446 RVA: 0x0006B9F0 File Offset: 0x00069BF0
		private static DirectoryInfo CreateDirectoriesInternal(string path)
		{
			if (SecurityManager.SecurityEnabled)
			{
				new FileIOPermission(FileIOPermissionAccess.Read | FileIOPermissionAccess.Write, path).Demand();
			}
			DirectoryInfo directoryInfo = new DirectoryInfo(path, true);
			if (directoryInfo.Parent != null && !directoryInfo.Parent.Exists)
			{
				directoryInfo.Parent.Create();
			}
			MonoIOError monoIOError;
			if (!MonoIO.CreateDirectory(path, out monoIOError) && monoIOError != MonoIOError.ERROR_ALREADY_EXISTS && monoIOError != MonoIOError.ERROR_FILE_EXISTS)
			{
				throw MonoIO.GetException(path, monoIOError);
			}
			return directoryInfo;
		}

		// Token: 0x06001D17 RID: 7447 RVA: 0x0006BA6C File Offset: 0x00069C6C
		public static void Delete(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException("Path is empty");
			}
			if (path.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Path contains invalid chars");
			}
			if (path.Trim().Length == 0)
			{
				throw new ArgumentException("Only blank characters in path");
			}
			if (path == ":")
			{
				throw new NotSupportedException("Only ':' In path");
			}
			MonoIOError monoIOError;
			bool flag;
			if (MonoIO.ExistsSymlink(path, out monoIOError))
			{
				flag = MonoIO.DeleteFile(path, out monoIOError);
			}
			else
			{
				flag = MonoIO.RemoveDirectory(path, out monoIOError);
			}
			if (flag)
			{
				return;
			}
			if (monoIOError != MonoIOError.ERROR_FILE_NOT_FOUND)
			{
				throw MonoIO.GetException(path, monoIOError);
			}
			if (File.Exists(path))
			{
				throw new IOException("Directory does not exist, but a file of the same name exist.");
			}
			throw new DirectoryNotFoundException("Directory does not exist.");
		}

		// Token: 0x06001D18 RID: 7448 RVA: 0x0006BB4C File Offset: 0x00069D4C
		private static void RecursiveDelete(string path)
		{
			foreach (string path2 in Directory.GetDirectories(path))
			{
				MonoIOError monoIOError;
				if (MonoIO.ExistsSymlink(path2, out monoIOError))
				{
					MonoIO.DeleteFile(path2, out monoIOError);
				}
				else
				{
					Directory.RecursiveDelete(path2);
				}
			}
			foreach (string path3 in Directory.GetFiles(path))
			{
				File.Delete(path3);
			}
			Directory.Delete(path);
		}

		// Token: 0x06001D19 RID: 7449 RVA: 0x0006BBD0 File Offset: 0x00069DD0
		public static void Delete(string path, bool recursive)
		{
			Directory.CheckPathExceptions(path);
			if (recursive)
			{
				Directory.RecursiveDelete(path);
			}
			else
			{
				Directory.Delete(path);
			}
		}

		// Token: 0x06001D1A RID: 7450 RVA: 0x0006BBF0 File Offset: 0x00069DF0
		public static bool Exists(string path)
		{
			MonoIOError monoIOError;
			return path != null && MonoIO.ExistsDirectory(path, out monoIOError);
		}

		// Token: 0x06001D1B RID: 7451 RVA: 0x0006BC10 File Offset: 0x00069E10
		public static DateTime GetLastAccessTime(string path)
		{
			return File.GetLastAccessTime(path);
		}

		// Token: 0x06001D1C RID: 7452 RVA: 0x0006BC18 File Offset: 0x00069E18
		public static DateTime GetLastAccessTimeUtc(string path)
		{
			return Directory.GetLastAccessTime(path).ToUniversalTime();
		}

		// Token: 0x06001D1D RID: 7453 RVA: 0x0006BC34 File Offset: 0x00069E34
		public static DateTime GetLastWriteTime(string path)
		{
			return File.GetLastWriteTime(path);
		}

		// Token: 0x06001D1E RID: 7454 RVA: 0x0006BC3C File Offset: 0x00069E3C
		public static DateTime GetLastWriteTimeUtc(string path)
		{
			return Directory.GetLastWriteTime(path).ToUniversalTime();
		}

		// Token: 0x06001D1F RID: 7455 RVA: 0x0006BC58 File Offset: 0x00069E58
		public static DateTime GetCreationTime(string path)
		{
			return File.GetCreationTime(path);
		}

		// Token: 0x06001D20 RID: 7456 RVA: 0x0006BC60 File Offset: 0x00069E60
		public static DateTime GetCreationTimeUtc(string path)
		{
			return Directory.GetCreationTime(path).ToUniversalTime();
		}

		// Token: 0x06001D21 RID: 7457 RVA: 0x0006BC7C File Offset: 0x00069E7C
		public static string GetCurrentDirectory()
		{
			MonoIOError monoIOError;
			string currentDirectory = MonoIO.GetCurrentDirectory(out monoIOError);
			if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(monoIOError);
			}
			if (currentDirectory != null && currentDirectory.Length > 0 && SecurityManager.SecurityEnabled)
			{
				new FileIOPermission(FileIOPermissionAccess.PathDiscovery, currentDirectory).Demand();
			}
			return currentDirectory;
		}

		// Token: 0x06001D22 RID: 7458 RVA: 0x0006BCC8 File Offset: 0x00069EC8
		public static string[] GetDirectories(string path)
		{
			return Directory.GetDirectories(path, "*");
		}

		// Token: 0x06001D23 RID: 7459 RVA: 0x0006BCD8 File Offset: 0x00069ED8
		public static string[] GetDirectories(string path, string searchPattern)
		{
			return Directory.GetFileSystemEntries(path, searchPattern, FileAttributes.Directory, FileAttributes.Directory);
		}

		// Token: 0x06001D24 RID: 7460 RVA: 0x0006BCE8 File Offset: 0x00069EE8
		public static string[] GetDirectories(string path, string searchPattern, SearchOption searchOption)
		{
			if (searchOption == SearchOption.TopDirectoryOnly)
			{
				return Directory.GetDirectories(path, searchPattern);
			}
			ArrayList arrayList = new ArrayList();
			Directory.GetDirectoriesRecurse(path, searchPattern, arrayList);
			return (string[])arrayList.ToArray(typeof(string));
		}

		// Token: 0x06001D25 RID: 7461 RVA: 0x0006BD28 File Offset: 0x00069F28
		private static void GetDirectoriesRecurse(string path, string searchPattern, ArrayList all)
		{
			all.AddRange(Directory.GetDirectories(path, searchPattern));
			foreach (string path2 in Directory.GetDirectories(path))
			{
				Directory.GetDirectoriesRecurse(path2, searchPattern, all);
			}
		}

		// Token: 0x06001D26 RID: 7462 RVA: 0x0006BD6C File Offset: 0x00069F6C
		public static string GetDirectoryRoot(string path)
		{
			return new string(Path.DirectorySeparatorChar, 1);
		}

		// Token: 0x06001D27 RID: 7463 RVA: 0x0006BD7C File Offset: 0x00069F7C
		public static string[] GetFiles(string path)
		{
			return Directory.GetFiles(path, "*");
		}

		// Token: 0x06001D28 RID: 7464 RVA: 0x0006BD8C File Offset: 0x00069F8C
		public static string[] GetFiles(string path, string searchPattern)
		{
			return Directory.GetFileSystemEntries(path, searchPattern, FileAttributes.Directory, (FileAttributes)0);
		}

		// Token: 0x06001D29 RID: 7465 RVA: 0x0006BD98 File Offset: 0x00069F98
		public static string[] GetFiles(string path, string searchPattern, SearchOption searchOption)
		{
			if (searchOption == SearchOption.TopDirectoryOnly)
			{
				return Directory.GetFiles(path, searchPattern);
			}
			ArrayList arrayList = new ArrayList();
			Directory.GetFilesRecurse(path, searchPattern, arrayList);
			return (string[])arrayList.ToArray(typeof(string));
		}

		// Token: 0x06001D2A RID: 7466 RVA: 0x0006BDD8 File Offset: 0x00069FD8
		private static void GetFilesRecurse(string path, string searchPattern, ArrayList all)
		{
			all.AddRange(Directory.GetFiles(path, searchPattern));
			foreach (string path2 in Directory.GetDirectories(path))
			{
				Directory.GetFilesRecurse(path2, searchPattern, all);
			}
		}

		// Token: 0x06001D2B RID: 7467 RVA: 0x0006BE1C File Offset: 0x0006A01C
		public static string[] GetFileSystemEntries(string path)
		{
			return Directory.GetFileSystemEntries(path, "*");
		}

		// Token: 0x06001D2C RID: 7468 RVA: 0x0006BE2C File Offset: 0x0006A02C
		public static string[] GetFileSystemEntries(string path, string searchPattern)
		{
			return Directory.GetFileSystemEntries(path, searchPattern, (FileAttributes)0, (FileAttributes)0);
		}

		// Token: 0x06001D2D RID: 7469 RVA: 0x0006BE38 File Offset: 0x0006A038
		public static string[] GetLogicalDrives()
		{
			return Environment.GetLogicalDrives();
		}

		// Token: 0x06001D2E RID: 7470 RVA: 0x0006BE40 File Offset: 0x0006A040
		private static bool IsRootDirectory(string path)
		{
			return (Path.DirectorySeparatorChar == '/' && path == "/") || (Path.DirectorySeparatorChar == '\\' && path.Length == 3 && path.EndsWith(":\\"));
		}

		// Token: 0x06001D2F RID: 7471 RVA: 0x0006BE98 File Offset: 0x0006A098
		public static DirectoryInfo GetParent(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Path contains invalid characters");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException("The Path do not have a valid format");
			}
			if (Directory.IsRootDirectory(path))
			{
				return null;
			}
			string text = Path.GetDirectoryName(path);
			if (text.Length == 0)
			{
				text = Directory.GetCurrentDirectory();
			}
			return new DirectoryInfo(text);
		}

		// Token: 0x06001D30 RID: 7472 RVA: 0x0006BF14 File Offset: 0x0006A114
		public static void Move(string sourceDirName, string destDirName)
		{
			if (sourceDirName == null)
			{
				throw new ArgumentNullException("sourceDirName");
			}
			if (destDirName == null)
			{
				throw new ArgumentNullException("destDirName");
			}
			if (sourceDirName.Trim().Length == 0 || sourceDirName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Invalid source directory name: " + sourceDirName, "sourceDirName");
			}
			if (destDirName.Trim().Length == 0 || destDirName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Invalid target directory name: " + destDirName, "destDirName");
			}
			if (sourceDirName == destDirName)
			{
				throw new IOException("Source and destination path must be different.");
			}
			if (Directory.Exists(destDirName))
			{
				throw new IOException(destDirName + " already exists.");
			}
			if (!Directory.Exists(sourceDirName) && !File.Exists(sourceDirName))
			{
				throw new DirectoryNotFoundException(sourceDirName + " does not exist");
			}
			MonoIOError error;
			if (!MonoIO.MoveFile(sourceDirName, destDirName, out error))
			{
				throw MonoIO.GetException(error);
			}
		}

		// Token: 0x06001D31 RID: 7473 RVA: 0x0006C020 File Offset: 0x0006A220
		public static void SetAccessControl(string path, DirectorySecurity directorySecurity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001D32 RID: 7474 RVA: 0x0006C028 File Offset: 0x0006A228
		public static void SetCreationTime(string path, DateTime creationTime)
		{
			File.SetCreationTime(path, creationTime);
		}

		// Token: 0x06001D33 RID: 7475 RVA: 0x0006C034 File Offset: 0x0006A234
		public static void SetCreationTimeUtc(string path, DateTime creationTimeUtc)
		{
			Directory.SetCreationTime(path, creationTimeUtc.ToLocalTime());
		}

		// Token: 0x06001D34 RID: 7476 RVA: 0x0006C044 File Offset: 0x0006A244
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static void SetCurrentDirectory(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Trim().Length == 0)
			{
				throw new ArgumentException("path string must not be an empty string or whitespace string");
			}
			if (!Directory.Exists(path))
			{
				throw new DirectoryNotFoundException("Directory \"" + path + "\" not found.");
			}
			MonoIOError monoIOError;
			MonoIO.SetCurrentDirectory(path, out monoIOError);
			if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(path, monoIOError);
			}
		}

		// Token: 0x06001D35 RID: 7477 RVA: 0x0006C0B8 File Offset: 0x0006A2B8
		public static void SetLastAccessTime(string path, DateTime lastAccessTime)
		{
			File.SetLastAccessTime(path, lastAccessTime);
		}

		// Token: 0x06001D36 RID: 7478 RVA: 0x0006C0C4 File Offset: 0x0006A2C4
		public static void SetLastAccessTimeUtc(string path, DateTime lastAccessTimeUtc)
		{
			Directory.SetLastAccessTime(path, lastAccessTimeUtc.ToLocalTime());
		}

		// Token: 0x06001D37 RID: 7479 RVA: 0x0006C0D4 File Offset: 0x0006A2D4
		public static void SetLastWriteTime(string path, DateTime lastWriteTime)
		{
			File.SetLastWriteTime(path, lastWriteTime);
		}

		// Token: 0x06001D38 RID: 7480 RVA: 0x0006C0E0 File Offset: 0x0006A2E0
		public static void SetLastWriteTimeUtc(string path, DateTime lastWriteTimeUtc)
		{
			Directory.SetLastWriteTime(path, lastWriteTimeUtc.ToLocalTime());
		}

		// Token: 0x06001D39 RID: 7481 RVA: 0x0006C0F0 File Offset: 0x0006A2F0
		private static void CheckPathExceptions(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException("Path is Empty");
			}
			if (path.Trim().Length == 0)
			{
				throw new ArgumentException("Only blank characters in path");
			}
			if (path.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Path contains invalid chars");
			}
		}

		// Token: 0x06001D3A RID: 7482 RVA: 0x0006C15C File Offset: 0x0006A35C
		private static string[] GetFileSystemEntries(string path, string searchPattern, FileAttributes mask, FileAttributes attrs)
		{
			if (path == null || searchPattern == null)
			{
				throw new ArgumentNullException();
			}
			if (searchPattern.Length == 0)
			{
				return new string[0];
			}
			if (path.Trim().Length == 0)
			{
				throw new ArgumentException("The Path does not have a valid format");
			}
			string path2 = Path.Combine(path, searchPattern);
			string directoryName = Path.GetDirectoryName(path2);
			if (directoryName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Path contains invalid characters");
			}
			MonoIOError monoIOError;
			if (directoryName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				if (path.IndexOfAny(SearchPattern.InvalidChars) == -1)
				{
					throw new ArgumentException("Path contains invalid characters", "path");
				}
				throw new ArgumentException("Pattern contains invalid characters", "pattern");
			}
			else if (!MonoIO.ExistsDirectory(directoryName, out monoIOError))
			{
				MonoIOError monoIOError2;
				if (monoIOError == MonoIOError.ERROR_SUCCESS && MonoIO.ExistsFile(directoryName, out monoIOError2))
				{
					return new string[]
					{
						directoryName
					};
				}
				if (monoIOError != MonoIOError.ERROR_PATH_NOT_FOUND)
				{
					throw MonoIO.GetException(directoryName, monoIOError);
				}
				if (directoryName.IndexOfAny(SearchPattern.WildcardChars) == -1)
				{
					throw new DirectoryNotFoundException("Directory '" + directoryName + "' not found.");
				}
				if (path.IndexOfAny(SearchPattern.WildcardChars) == -1)
				{
					throw new ArgumentException("Pattern is invalid", "searchPattern");
				}
				throw new ArgumentException("Path is invalid", "path");
			}
			else
			{
				string path_with_pattern = Path.Combine(directoryName, searchPattern);
				string[] fileSystemEntries = MonoIO.GetFileSystemEntries(path, path_with_pattern, (int)attrs, (int)mask, out monoIOError);
				if (monoIOError != MonoIOError.ERROR_SUCCESS)
				{
					throw MonoIO.GetException(directoryName, monoIOError);
				}
				return fileSystemEntries;
			}
		}

		// Token: 0x06001D3B RID: 7483 RVA: 0x0006C2D0 File Offset: 0x0006A4D0
		[MonoNotSupported("DirectorySecurity isn't implemented")]
		public static DirectorySecurity GetAccessControl(string path, AccessControlSections includeSections)
		{
			throw new PlatformNotSupportedException();
		}

		// Token: 0x06001D3C RID: 7484 RVA: 0x0006C2D8 File Offset: 0x0006A4D8
		[MonoNotSupported("DirectorySecurity isn't implemented")]
		public static DirectorySecurity GetAccessControl(string path)
		{
			throw new PlatformNotSupportedException();
		}
	}
}
