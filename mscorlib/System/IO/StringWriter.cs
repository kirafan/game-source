﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.IO
{
	// Token: 0x0200025A RID: 602
	[MonoTODO("Serialization format not compatible with .NET")]
	[ComVisible(true)]
	[Serializable]
	public class StringWriter : TextWriter
	{
		// Token: 0x06001F38 RID: 7992 RVA: 0x00073B40 File Offset: 0x00071D40
		public StringWriter() : this(new StringBuilder())
		{
		}

		// Token: 0x06001F39 RID: 7993 RVA: 0x00073B50 File Offset: 0x00071D50
		public StringWriter(IFormatProvider formatProvider) : this(new StringBuilder(), formatProvider)
		{
		}

		// Token: 0x06001F3A RID: 7994 RVA: 0x00073B60 File Offset: 0x00071D60
		public StringWriter(StringBuilder sb) : this(sb, null)
		{
		}

		// Token: 0x06001F3B RID: 7995 RVA: 0x00073B6C File Offset: 0x00071D6C
		public StringWriter(StringBuilder sb, IFormatProvider formatProvider)
		{
			if (sb == null)
			{
				throw new ArgumentNullException("sb");
			}
			this.internalString = sb;
			this.internalFormatProvider = formatProvider;
		}

		// Token: 0x17000540 RID: 1344
		// (get) Token: 0x06001F3C RID: 7996 RVA: 0x00073B94 File Offset: 0x00071D94
		public override Encoding Encoding
		{
			get
			{
				return Encoding.Unicode;
			}
		}

		// Token: 0x06001F3D RID: 7997 RVA: 0x00073B9C File Offset: 0x00071D9C
		public override void Close()
		{
			this.Dispose(true);
			this.disposed = true;
		}

		// Token: 0x06001F3E RID: 7998 RVA: 0x00073BAC File Offset: 0x00071DAC
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
			this.disposed = true;
		}

		// Token: 0x06001F3F RID: 7999 RVA: 0x00073BBC File Offset: 0x00071DBC
		public virtual StringBuilder GetStringBuilder()
		{
			return this.internalString;
		}

		// Token: 0x06001F40 RID: 8000 RVA: 0x00073BC4 File Offset: 0x00071DC4
		public override string ToString()
		{
			return this.internalString.ToString();
		}

		// Token: 0x06001F41 RID: 8001 RVA: 0x00073BD4 File Offset: 0x00071DD4
		public override void Write(char value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("StringReader", Locale.GetText("Cannot write to a closed StringWriter"));
			}
			this.internalString.Append(value);
		}

		// Token: 0x06001F42 RID: 8002 RVA: 0x00073C04 File Offset: 0x00071E04
		public override void Write(string value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("StringReader", Locale.GetText("Cannot write to a closed StringWriter"));
			}
			this.internalString.Append(value);
		}

		// Token: 0x06001F43 RID: 8003 RVA: 0x00073C34 File Offset: 0x00071E34
		public override void Write(char[] buffer, int index, int count)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("StringReader", Locale.GetText("Cannot write to a closed StringWriter"));
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			if (index > buffer.Length - count)
			{
				throw new ArgumentException("index + count > buffer.Length");
			}
			this.internalString.Append(buffer, index, count);
		}

		// Token: 0x04000BCA RID: 3018
		private StringBuilder internalString;

		// Token: 0x04000BCB RID: 3019
		private bool disposed;
	}
}
