﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace System.IO
{
	// Token: 0x02000258 RID: 600
	[ComVisible(true)]
	[Serializable]
	public class StreamWriter : TextWriter
	{
		// Token: 0x06001F16 RID: 7958 RVA: 0x000731C0 File Offset: 0x000713C0
		public StreamWriter(Stream stream) : this(stream, Encoding.UTF8Unmarked, 1024)
		{
		}

		// Token: 0x06001F17 RID: 7959 RVA: 0x000731D4 File Offset: 0x000713D4
		public StreamWriter(Stream stream, Encoding encoding) : this(stream, encoding, 1024)
		{
		}

		// Token: 0x06001F18 RID: 7960 RVA: 0x000731E4 File Offset: 0x000713E4
		public StreamWriter(Stream stream, Encoding encoding, int bufferSize)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (encoding == null)
			{
				throw new ArgumentNullException("encoding");
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize");
			}
			if (!stream.CanWrite)
			{
				throw new ArgumentException("Can not write to stream");
			}
			this.internalStream = stream;
			this.Initialize(encoding, bufferSize);
		}

		// Token: 0x06001F19 RID: 7961 RVA: 0x00073250 File Offset: 0x00071450
		public StreamWriter(string path) : this(path, false, Encoding.UTF8Unmarked, 4096)
		{
		}

		// Token: 0x06001F1A RID: 7962 RVA: 0x00073264 File Offset: 0x00071464
		public StreamWriter(string path, bool append) : this(path, append, Encoding.UTF8Unmarked, 4096)
		{
		}

		// Token: 0x06001F1B RID: 7963 RVA: 0x00073278 File Offset: 0x00071478
		public StreamWriter(string path, bool append, Encoding encoding) : this(path, append, encoding, 4096)
		{
		}

		// Token: 0x06001F1C RID: 7964 RVA: 0x00073288 File Offset: 0x00071488
		public StreamWriter(string path, bool append, Encoding encoding, int bufferSize)
		{
			if (encoding == null)
			{
				throw new ArgumentNullException("encoding");
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize");
			}
			FileMode mode;
			if (append)
			{
				mode = FileMode.Append;
			}
			else
			{
				mode = FileMode.Create;
			}
			this.internalStream = new FileStream(path, mode, FileAccess.Write, FileShare.Read);
			if (append)
			{
				this.internalStream.Position = this.internalStream.Length;
			}
			else
			{
				this.internalStream.SetLength(0L);
			}
			this.Initialize(encoding, bufferSize);
		}

		// Token: 0x06001F1E RID: 7966 RVA: 0x0007332C File Offset: 0x0007152C
		internal void Initialize(Encoding encoding, int bufferSize)
		{
			this.internalEncoding = encoding;
			this.decode_pos = (this.byte_pos = 0);
			int num = Math.Max(bufferSize, 256);
			this.decode_buf = new char[num];
			this.byte_buf = new byte[encoding.GetMaxByteCount(num)];
			if (this.internalStream.CanSeek && this.internalStream.Position > 0L)
			{
				this.preamble_done = true;
			}
		}

		// Token: 0x1700053D RID: 1341
		// (get) Token: 0x06001F1F RID: 7967 RVA: 0x000733A4 File Offset: 0x000715A4
		// (set) Token: 0x06001F20 RID: 7968 RVA: 0x000733AC File Offset: 0x000715AC
		public virtual bool AutoFlush
		{
			get
			{
				return this.iflush;
			}
			set
			{
				this.iflush = value;
				if (this.iflush)
				{
					this.Flush();
				}
			}
		}

		// Token: 0x1700053E RID: 1342
		// (get) Token: 0x06001F21 RID: 7969 RVA: 0x000733C8 File Offset: 0x000715C8
		public virtual Stream BaseStream
		{
			get
			{
				return this.internalStream;
			}
		}

		// Token: 0x1700053F RID: 1343
		// (get) Token: 0x06001F22 RID: 7970 RVA: 0x000733D0 File Offset: 0x000715D0
		public override Encoding Encoding
		{
			get
			{
				return this.internalEncoding;
			}
		}

		// Token: 0x06001F23 RID: 7971 RVA: 0x000733D8 File Offset: 0x000715D8
		protected override void Dispose(bool disposing)
		{
			Exception ex = null;
			if (!this.DisposedAlready && disposing && this.internalStream != null)
			{
				try
				{
					this.Flush();
				}
				catch (Exception ex2)
				{
					ex = ex2;
				}
				this.DisposedAlready = true;
				try
				{
					this.internalStream.Close();
				}
				catch (Exception ex3)
				{
					if (ex == null)
					{
						ex = ex3;
					}
				}
			}
			this.internalStream = null;
			this.byte_buf = null;
			this.internalEncoding = null;
			this.decode_buf = null;
			if (ex != null)
			{
				throw ex;
			}
		}

		// Token: 0x06001F24 RID: 7972 RVA: 0x00073494 File Offset: 0x00071694
		public override void Flush()
		{
			if (this.DisposedAlready)
			{
				throw new ObjectDisposedException("StreamWriter");
			}
			this.Decode();
			if (this.byte_pos > 0)
			{
				this.FlushBytes();
				this.internalStream.Flush();
			}
		}

		// Token: 0x06001F25 RID: 7973 RVA: 0x000734DC File Offset: 0x000716DC
		private void FlushBytes()
		{
			if (!this.preamble_done && this.byte_pos > 0)
			{
				byte[] preamble = this.internalEncoding.GetPreamble();
				if (preamble.Length > 0)
				{
					this.internalStream.Write(preamble, 0, preamble.Length);
				}
				this.preamble_done = true;
			}
			this.internalStream.Write(this.byte_buf, 0, this.byte_pos);
			this.byte_pos = 0;
		}

		// Token: 0x06001F26 RID: 7974 RVA: 0x0007354C File Offset: 0x0007174C
		private void Decode()
		{
			if (this.byte_pos > 0)
			{
				this.FlushBytes();
			}
			if (this.decode_pos > 0)
			{
				int bytes = this.internalEncoding.GetBytes(this.decode_buf, 0, this.decode_pos, this.byte_buf, this.byte_pos);
				this.byte_pos += bytes;
				this.decode_pos = 0;
			}
		}

		// Token: 0x06001F27 RID: 7975 RVA: 0x000735B4 File Offset: 0x000717B4
		public override void Write(char[] buffer, int index, int count)
		{
			if (this.DisposedAlready)
			{
				throw new ObjectDisposedException("StreamWriter");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			if (index > buffer.Length - count)
			{
				throw new ArgumentException("index + count > buffer.Length");
			}
			this.LowLevelWrite(buffer, index, count);
			if (this.iflush)
			{
				this.Flush();
			}
		}

		// Token: 0x06001F28 RID: 7976 RVA: 0x00073648 File Offset: 0x00071848
		private void LowLevelWrite(char[] buffer, int index, int count)
		{
			while (count > 0)
			{
				int num = this.decode_buf.Length - this.decode_pos;
				if (num == 0)
				{
					this.Decode();
					num = this.decode_buf.Length;
				}
				if (num > count)
				{
					num = count;
				}
				Buffer.BlockCopy(buffer, index * 2, this.decode_buf, this.decode_pos * 2, num * 2);
				count -= num;
				index += num;
				this.decode_pos += num;
			}
		}

		// Token: 0x06001F29 RID: 7977 RVA: 0x000736C4 File Offset: 0x000718C4
		private void LowLevelWrite(string s)
		{
			int i = s.Length;
			int num = 0;
			while (i > 0)
			{
				int num2 = this.decode_buf.Length - this.decode_pos;
				if (num2 == 0)
				{
					this.Decode();
					num2 = this.decode_buf.Length;
				}
				if (num2 > i)
				{
					num2 = i;
				}
				for (int j = 0; j < num2; j++)
				{
					this.decode_buf[j + this.decode_pos] = s[j + num];
				}
				i -= num2;
				num += num2;
				this.decode_pos += num2;
			}
		}

		// Token: 0x06001F2A RID: 7978 RVA: 0x00073754 File Offset: 0x00071954
		public override void Write(char value)
		{
			if (this.DisposedAlready)
			{
				throw new ObjectDisposedException("StreamWriter");
			}
			if (this.decode_pos >= this.decode_buf.Length)
			{
				this.Decode();
			}
			this.decode_buf[this.decode_pos++] = value;
			if (this.iflush)
			{
				this.Flush();
			}
		}

		// Token: 0x06001F2B RID: 7979 RVA: 0x000737BC File Offset: 0x000719BC
		public override void Write(char[] buffer)
		{
			if (this.DisposedAlready)
			{
				throw new ObjectDisposedException("StreamWriter");
			}
			if (buffer != null)
			{
				this.LowLevelWrite(buffer, 0, buffer.Length);
			}
			if (this.iflush)
			{
				this.Flush();
			}
		}

		// Token: 0x06001F2C RID: 7980 RVA: 0x00073804 File Offset: 0x00071A04
		public override void Write(string value)
		{
			if (this.DisposedAlready)
			{
				throw new ObjectDisposedException("StreamWriter");
			}
			if (value != null)
			{
				this.LowLevelWrite(value);
			}
			if (this.iflush)
			{
				this.Flush();
			}
		}

		// Token: 0x06001F2D RID: 7981 RVA: 0x00073848 File Offset: 0x00071A48
		public override void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x06001F2E RID: 7982 RVA: 0x00073854 File Offset: 0x00071A54
		~StreamWriter()
		{
			this.Dispose(false);
		}

		// Token: 0x04000BBA RID: 3002
		private const int DefaultBufferSize = 1024;

		// Token: 0x04000BBB RID: 3003
		private const int DefaultFileBufferSize = 4096;

		// Token: 0x04000BBC RID: 3004
		private const int MinimumBufferSize = 256;

		// Token: 0x04000BBD RID: 3005
		private Encoding internalEncoding;

		// Token: 0x04000BBE RID: 3006
		private Stream internalStream;

		// Token: 0x04000BBF RID: 3007
		private bool iflush;

		// Token: 0x04000BC0 RID: 3008
		private byte[] byte_buf;

		// Token: 0x04000BC1 RID: 3009
		private int byte_pos;

		// Token: 0x04000BC2 RID: 3010
		private char[] decode_buf;

		// Token: 0x04000BC3 RID: 3011
		private int decode_pos;

		// Token: 0x04000BC4 RID: 3012
		private bool DisposedAlready;

		// Token: 0x04000BC5 RID: 3013
		private bool preamble_done;

		// Token: 0x04000BC6 RID: 3014
		public new static readonly StreamWriter Null = new StreamWriter(Stream.Null, Encoding.UTF8Unmarked, 1);
	}
}
