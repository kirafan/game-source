﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.AccessControl;

namespace System.IO
{
	// Token: 0x02000233 RID: 563
	[ComVisible(true)]
	[Serializable]
	public sealed class DirectoryInfo : FileSystemInfo
	{
		// Token: 0x06001D3D RID: 7485 RVA: 0x0006C2E0 File Offset: 0x0006A4E0
		public DirectoryInfo(string path) : this(path, false)
		{
		}

		// Token: 0x06001D3E RID: 7486 RVA: 0x0006C2EC File Offset: 0x0006A4EC
		internal DirectoryInfo(string path, bool simpleOriginalPath)
		{
			base.CheckPath(path);
			this.FullPath = Path.GetFullPath(path);
			if (simpleOriginalPath)
			{
				this.OriginalPath = Path.GetFileName(path);
			}
			else
			{
				this.OriginalPath = path;
			}
			this.Initialize();
		}

		// Token: 0x06001D3F RID: 7487 RVA: 0x0006C338 File Offset: 0x0006A538
		private DirectoryInfo(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.Initialize();
		}

		// Token: 0x06001D40 RID: 7488 RVA: 0x0006C348 File Offset: 0x0006A548
		private void Initialize()
		{
			int num = this.FullPath.Length - 1;
			if (num > 1 && this.FullPath[num] == Path.DirectorySeparatorChar)
			{
				num--;
			}
			int num2 = this.FullPath.LastIndexOf(Path.DirectorySeparatorChar, num);
			if (num2 == -1 || (num2 == 0 && num == 0))
			{
				this.current = this.FullPath;
				this.parent = null;
			}
			else
			{
				this.current = this.FullPath.Substring(num2 + 1, num - num2);
				if (num2 == 0 && !Environment.IsRunningOnWindows)
				{
					this.parent = Path.DirectorySeparatorStr;
				}
				else
				{
					this.parent = this.FullPath.Substring(0, num2);
				}
				if (Environment.IsRunningOnWindows && this.parent.Length == 2 && this.parent[1] == ':' && char.IsLetter(this.parent[0]))
				{
					this.parent += Path.DirectorySeparatorChar;
				}
			}
		}

		// Token: 0x170004E3 RID: 1251
		// (get) Token: 0x06001D41 RID: 7489 RVA: 0x0006C46C File Offset: 0x0006A66C
		public override bool Exists
		{
			get
			{
				base.Refresh(false);
				return this.stat.Attributes != MonoIO.InvalidFileAttributes && (this.stat.Attributes & FileAttributes.Directory) != (FileAttributes)0;
			}
		}

		// Token: 0x170004E4 RID: 1252
		// (get) Token: 0x06001D42 RID: 7490 RVA: 0x0006C4B0 File Offset: 0x0006A6B0
		public override string Name
		{
			get
			{
				return this.current;
			}
		}

		// Token: 0x170004E5 RID: 1253
		// (get) Token: 0x06001D43 RID: 7491 RVA: 0x0006C4B8 File Offset: 0x0006A6B8
		public DirectoryInfo Parent
		{
			get
			{
				if (this.parent == null || this.parent.Length == 0)
				{
					return null;
				}
				return new DirectoryInfo(this.parent);
			}
		}

		// Token: 0x170004E6 RID: 1254
		// (get) Token: 0x06001D44 RID: 7492 RVA: 0x0006C4F0 File Offset: 0x0006A6F0
		public DirectoryInfo Root
		{
			get
			{
				string pathRoot = Path.GetPathRoot(this.FullPath);
				if (pathRoot == null)
				{
					return null;
				}
				return new DirectoryInfo(pathRoot);
			}
		}

		// Token: 0x06001D45 RID: 7493 RVA: 0x0006C518 File Offset: 0x0006A718
		public void Create()
		{
			Directory.CreateDirectory(this.FullPath);
		}

		// Token: 0x06001D46 RID: 7494 RVA: 0x0006C528 File Offset: 0x0006A728
		public DirectoryInfo CreateSubdirectory(string path)
		{
			base.CheckPath(path);
			path = Path.Combine(this.FullPath, path);
			Directory.CreateDirectory(path);
			return new DirectoryInfo(path);
		}

		// Token: 0x06001D47 RID: 7495 RVA: 0x0006C558 File Offset: 0x0006A758
		public FileInfo[] GetFiles()
		{
			return this.GetFiles("*");
		}

		// Token: 0x06001D48 RID: 7496 RVA: 0x0006C568 File Offset: 0x0006A768
		public FileInfo[] GetFiles(string searchPattern)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			string[] files = Directory.GetFiles(this.FullPath, searchPattern);
			FileInfo[] array = new FileInfo[files.Length];
			int num = 0;
			foreach (string fileName in files)
			{
				array[num++] = new FileInfo(fileName);
			}
			return array;
		}

		// Token: 0x06001D49 RID: 7497 RVA: 0x0006C5D0 File Offset: 0x0006A7D0
		public DirectoryInfo[] GetDirectories()
		{
			return this.GetDirectories("*");
		}

		// Token: 0x06001D4A RID: 7498 RVA: 0x0006C5E0 File Offset: 0x0006A7E0
		public DirectoryInfo[] GetDirectories(string searchPattern)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			string[] directories = Directory.GetDirectories(this.FullPath, searchPattern);
			DirectoryInfo[] array = new DirectoryInfo[directories.Length];
			int num = 0;
			foreach (string path in directories)
			{
				array[num++] = new DirectoryInfo(path);
			}
			return array;
		}

		// Token: 0x06001D4B RID: 7499 RVA: 0x0006C648 File Offset: 0x0006A848
		public FileSystemInfo[] GetFileSystemInfos()
		{
			return this.GetFileSystemInfos("*");
		}

		// Token: 0x06001D4C RID: 7500 RVA: 0x0006C658 File Offset: 0x0006A858
		public FileSystemInfo[] GetFileSystemInfos(string searchPattern)
		{
			if (searchPattern == null)
			{
				throw new ArgumentNullException("searchPattern");
			}
			if (!Directory.Exists(this.FullPath))
			{
				throw new IOException("Invalid directory");
			}
			string[] directories = Directory.GetDirectories(this.FullPath, searchPattern);
			string[] files = Directory.GetFiles(this.FullPath, searchPattern);
			FileSystemInfo[] array = new FileSystemInfo[directories.Length + files.Length];
			int num = 0;
			foreach (string path in directories)
			{
				array[num++] = new DirectoryInfo(path);
			}
			foreach (string fileName in files)
			{
				array[num++] = new FileInfo(fileName);
			}
			return array;
		}

		// Token: 0x06001D4D RID: 7501 RVA: 0x0006C720 File Offset: 0x0006A920
		public override void Delete()
		{
			this.Delete(false);
		}

		// Token: 0x06001D4E RID: 7502 RVA: 0x0006C72C File Offset: 0x0006A92C
		public void Delete(bool recursive)
		{
			Directory.Delete(this.FullPath, recursive);
		}

		// Token: 0x06001D4F RID: 7503 RVA: 0x0006C73C File Offset: 0x0006A93C
		public void MoveTo(string destDirName)
		{
			if (destDirName == null)
			{
				throw new ArgumentNullException("destDirName");
			}
			if (destDirName.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.", "destDirName");
			}
			Directory.Move(this.FullPath, Path.GetFullPath(destDirName));
		}

		// Token: 0x06001D50 RID: 7504 RVA: 0x0006C77C File Offset: 0x0006A97C
		public override string ToString()
		{
			return this.OriginalPath;
		}

		// Token: 0x06001D51 RID: 7505 RVA: 0x0006C784 File Offset: 0x0006A984
		public DirectoryInfo[] GetDirectories(string searchPattern, SearchOption searchOption)
		{
			if (searchOption == SearchOption.TopDirectoryOnly)
			{
				return this.GetDirectories(searchPattern);
			}
			if (searchOption != SearchOption.AllDirectories)
			{
				string text = Locale.GetText("Invalid enum value '{0}' for '{1}'.", new object[]
				{
					searchOption,
					"SearchOption"
				});
				throw new ArgumentOutOfRangeException("searchOption", text);
			}
			Queue queue = new Queue(this.GetDirectories(searchPattern));
			Queue queue2 = new Queue();
			while (queue.Count > 0)
			{
				DirectoryInfo directoryInfo = (DirectoryInfo)queue.Dequeue();
				DirectoryInfo[] directories = directoryInfo.GetDirectories(searchPattern);
				foreach (DirectoryInfo obj in directories)
				{
					queue.Enqueue(obj);
				}
				queue2.Enqueue(directoryInfo);
			}
			DirectoryInfo[] array2 = new DirectoryInfo[queue2.Count];
			queue2.CopyTo(array2, 0);
			return array2;
		}

		// Token: 0x06001D52 RID: 7506 RVA: 0x0006C864 File Offset: 0x0006AA64
		internal int GetFilesSubdirs(ArrayList l, string pattern)
		{
			FileInfo[] array = null;
			try
			{
				array = this.GetFiles(pattern);
			}
			catch (UnauthorizedAccessException)
			{
				return 0;
			}
			int num = array.Length;
			l.Add(array);
			foreach (DirectoryInfo directoryInfo in this.GetDirectories())
			{
				num += directoryInfo.GetFilesSubdirs(l, pattern);
			}
			return num;
		}

		// Token: 0x06001D53 RID: 7507 RVA: 0x0006C8EC File Offset: 0x0006AAEC
		public FileInfo[] GetFiles(string searchPattern, SearchOption searchOption)
		{
			if (searchOption == SearchOption.TopDirectoryOnly)
			{
				return this.GetFiles(searchPattern);
			}
			if (searchOption != SearchOption.AllDirectories)
			{
				string text = Locale.GetText("Invalid enum value '{0}' for '{1}'.", new object[]
				{
					searchOption,
					"SearchOption"
				});
				throw new ArgumentOutOfRangeException("searchOption", text);
			}
			ArrayList arrayList = new ArrayList();
			int filesSubdirs = this.GetFilesSubdirs(arrayList, searchPattern);
			int num = 0;
			FileInfo[] array = new FileInfo[filesSubdirs];
			foreach (object obj in arrayList)
			{
				FileInfo[] array2 = (FileInfo[])obj;
				array2.CopyTo(array, num);
				num += array2.Length;
			}
			return array;
		}

		// Token: 0x06001D54 RID: 7508 RVA: 0x0006C9D0 File Offset: 0x0006ABD0
		[MonoLimitation("DirectorySecurity isn't implemented")]
		public void Create(DirectorySecurity directorySecurity)
		{
			if (directorySecurity != null)
			{
				throw new UnauthorizedAccessException();
			}
			this.Create();
		}

		// Token: 0x06001D55 RID: 7509 RVA: 0x0006C9E4 File Offset: 0x0006ABE4
		[MonoLimitation("DirectorySecurity isn't implemented")]
		public DirectoryInfo CreateSubdirectory(string path, DirectorySecurity directorySecurity)
		{
			if (directorySecurity != null)
			{
				throw new UnauthorizedAccessException();
			}
			return this.CreateSubdirectory(path);
		}

		// Token: 0x06001D56 RID: 7510 RVA: 0x0006C9FC File Offset: 0x0006ABFC
		[MonoNotSupported("DirectorySecurity isn't implemented")]
		public DirectorySecurity GetAccessControl()
		{
			throw new UnauthorizedAccessException();
		}

		// Token: 0x06001D57 RID: 7511 RVA: 0x0006CA04 File Offset: 0x0006AC04
		[MonoNotSupported("DirectorySecurity isn't implemented")]
		public DirectorySecurity GetAccessControl(AccessControlSections includeSections)
		{
			throw new UnauthorizedAccessException();
		}

		// Token: 0x06001D58 RID: 7512 RVA: 0x0006CA0C File Offset: 0x0006AC0C
		[MonoLimitation("DirectorySecurity isn't implemented")]
		public void SetAccessControl(DirectorySecurity directorySecurity)
		{
			if (directorySecurity != null)
			{
				throw new ArgumentNullException("directorySecurity");
			}
			throw new UnauthorizedAccessException();
		}

		// Token: 0x04000AF1 RID: 2801
		private string current;

		// Token: 0x04000AF2 RID: 2802
		private string parent;
	}
}
