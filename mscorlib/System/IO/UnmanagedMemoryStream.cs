﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x02000263 RID: 611
	[CLSCompliant(false)]
	public class UnmanagedMemoryStream : Stream
	{
		// Token: 0x06001FD6 RID: 8150 RVA: 0x000754C8 File Offset: 0x000736C8
		protected UnmanagedMemoryStream()
		{
			this.closed = true;
		}

		// Token: 0x06001FD7 RID: 8151 RVA: 0x000754D8 File Offset: 0x000736D8
		public unsafe UnmanagedMemoryStream(byte* pointer, long length)
		{
			this.Initialize(pointer, length, length, FileAccess.Read);
		}

		// Token: 0x06001FD8 RID: 8152 RVA: 0x000754EC File Offset: 0x000736EC
		public unsafe UnmanagedMemoryStream(byte* pointer, long length, long capacity, FileAccess access)
		{
			this.Initialize(pointer, length, capacity, access);
		}

		// Token: 0x14000013 RID: 19
		// (add) Token: 0x06001FD9 RID: 8153 RVA: 0x00075500 File Offset: 0x00073700
		// (remove) Token: 0x06001FDA RID: 8154 RVA: 0x0007551C File Offset: 0x0007371C
		internal event EventHandler Closed;

		// Token: 0x17000548 RID: 1352
		// (get) Token: 0x06001FDB RID: 8155 RVA: 0x00075538 File Offset: 0x00073738
		public override bool CanRead
		{
			get
			{
				return !this.closed && this.fileaccess != FileAccess.Write;
			}
		}

		// Token: 0x17000549 RID: 1353
		// (get) Token: 0x06001FDC RID: 8156 RVA: 0x00075554 File Offset: 0x00073754
		public override bool CanSeek
		{
			get
			{
				return !this.closed;
			}
		}

		// Token: 0x1700054A RID: 1354
		// (get) Token: 0x06001FDD RID: 8157 RVA: 0x00075560 File Offset: 0x00073760
		public override bool CanWrite
		{
			get
			{
				return !this.closed && this.fileaccess != FileAccess.Read;
			}
		}

		// Token: 0x1700054B RID: 1355
		// (get) Token: 0x06001FDE RID: 8158 RVA: 0x0007557C File Offset: 0x0007377C
		public long Capacity
		{
			get
			{
				if (this.closed)
				{
					throw new ObjectDisposedException("The stream is closed");
				}
				return this.capacity;
			}
		}

		// Token: 0x1700054C RID: 1356
		// (get) Token: 0x06001FDF RID: 8159 RVA: 0x0007559C File Offset: 0x0007379C
		public override long Length
		{
			get
			{
				if (this.closed)
				{
					throw new ObjectDisposedException("The stream is closed");
				}
				return this.length;
			}
		}

		// Token: 0x1700054D RID: 1357
		// (get) Token: 0x06001FE0 RID: 8160 RVA: 0x000755BC File Offset: 0x000737BC
		// (set) Token: 0x06001FE1 RID: 8161 RVA: 0x000755DC File Offset: 0x000737DC
		public override long Position
		{
			get
			{
				if (this.closed)
				{
					throw new ObjectDisposedException("The stream is closed");
				}
				return this.current_position;
			}
			set
			{
				if (this.closed)
				{
					throw new ObjectDisposedException("The stream is closed");
				}
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException("value", "Non-negative number required.");
				}
				if (value > 2147483647L)
				{
					throw new ArgumentOutOfRangeException("value", "The position is larger than Int32.MaxValue.");
				}
				this.current_position = value;
			}
		}

		// Token: 0x1700054E RID: 1358
		// (get) Token: 0x06001FE2 RID: 8162 RVA: 0x0007563C File Offset: 0x0007383C
		// (set) Token: 0x06001FE3 RID: 8163 RVA: 0x00075690 File Offset: 0x00073890
		public unsafe byte* PositionPointer
		{
			get
			{
				if (this.closed)
				{
					throw new ObjectDisposedException("The stream is closed");
				}
				if (this.current_position >= this.length)
				{
					throw new IndexOutOfRangeException("value");
				}
				return (byte*)((void*)this.initial_pointer) + this.current_position;
			}
			set
			{
				if (this.closed)
				{
					throw new ObjectDisposedException("The stream is closed");
				}
				if (value < (byte*)((void*)this.initial_pointer))
				{
					throw new IOException("Address is below the inital address");
				}
				this.Position = (long)(value - (void*)this.initial_pointer);
			}
		}

		// Token: 0x06001FE4 RID: 8164 RVA: 0x000756E4 File Offset: 0x000738E4
		public override int Read([In] [Out] byte[] buffer, int offset, int count)
		{
			if (this.closed)
			{
				throw new ObjectDisposedException("The stream is closed");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "Non-negative number required.");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "Non-negative number required.");
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException("The length of the buffer array minus the offset parameter is less than the count parameter");
			}
			if (this.fileaccess == FileAccess.Write)
			{
				throw new NotSupportedException("Stream does not support reading");
			}
			if (this.current_position >= this.length)
			{
				return 0;
			}
			int num = (this.current_position + (long)count >= this.length) ? ((int)(this.length - this.current_position)) : count;
			Marshal.Copy(new IntPtr(this.initial_pointer.ToInt64() + this.current_position), buffer, offset, num);
			this.current_position += (long)num;
			return num;
		}

		// Token: 0x06001FE5 RID: 8165 RVA: 0x000757E0 File Offset: 0x000739E0
		public override int ReadByte()
		{
			if (this.closed)
			{
				throw new ObjectDisposedException("The stream is closed");
			}
			if (this.fileaccess == FileAccess.Write)
			{
				throw new NotSupportedException("Stream does not support reading");
			}
			if (this.current_position >= this.length)
			{
				return -1;
			}
			IntPtr ptr = this.initial_pointer;
			long num;
			this.current_position = (num = this.current_position) + 1L;
			return (int)Marshal.ReadByte(ptr, (int)num);
		}

		// Token: 0x06001FE6 RID: 8166 RVA: 0x0007584C File Offset: 0x00073A4C
		public override long Seek(long offset, SeekOrigin loc)
		{
			if (this.closed)
			{
				throw new ObjectDisposedException("The stream is closed");
			}
			long num;
			switch (loc)
			{
			case SeekOrigin.Begin:
				if (offset < 0L)
				{
					throw new IOException("An attempt was made to seek before the beginning of the stream");
				}
				num = this.initial_position;
				break;
			case SeekOrigin.Current:
				num = this.current_position;
				break;
			case SeekOrigin.End:
				num = this.length;
				break;
			default:
				throw new ArgumentException("Invalid SeekOrigin option");
			}
			num += offset;
			if (num < this.initial_position)
			{
				throw new IOException("An attempt was made to seek before the beginning of the stream");
			}
			this.current_position = num;
			return this.current_position;
		}

		// Token: 0x06001FE7 RID: 8167 RVA: 0x000758F4 File Offset: 0x00073AF4
		public override void SetLength(long value)
		{
			if (this.closed)
			{
				throw new ObjectDisposedException("The stream is closed");
			}
			if (value < 0L)
			{
				throw new ArgumentOutOfRangeException("length", "Non-negative number required.");
			}
			if (value > this.capacity)
			{
				throw new IOException("Unable to expand length of this stream beyond its capacity.");
			}
			if (this.fileaccess == FileAccess.Read)
			{
				throw new NotSupportedException("Stream does not support writing.");
			}
			this.length = value;
			if (this.length < this.current_position)
			{
				this.current_position = this.length;
			}
		}

		// Token: 0x06001FE8 RID: 8168 RVA: 0x00075984 File Offset: 0x00073B84
		public override void Flush()
		{
			if (this.closed)
			{
				throw new ObjectDisposedException("The stream is closed");
			}
		}

		// Token: 0x06001FE9 RID: 8169 RVA: 0x0007599C File Offset: 0x00073B9C
		protected override void Dispose(bool disposing)
		{
			if (this.closed)
			{
				return;
			}
			this.closed = true;
			if (this.Closed != null)
			{
				this.Closed(this, null);
			}
		}

		// Token: 0x06001FEA RID: 8170 RVA: 0x000759CC File Offset: 0x00073BCC
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (this.closed)
			{
				throw new ObjectDisposedException("The stream is closed");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("The buffer parameter is a null reference");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "Non-negative number required.");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "Non-negative number required.");
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException("The length of the buffer array minus the offset parameter is less than the count parameter");
			}
			if (this.current_position > this.capacity - (long)count)
			{
				throw new NotSupportedException("Unable to expand length of this stream beyond its capacity.");
			}
			if (this.fileaccess == FileAccess.Read)
			{
				throw new NotSupportedException("Stream does not support writing.");
			}
			for (int i = 0; i < count; i++)
			{
				IntPtr ptr = this.initial_pointer;
				long num;
				this.current_position = (num = this.current_position) + 1L;
				Marshal.WriteByte(ptr, (int)num, buffer[offset + i]);
			}
			if (this.current_position > this.length)
			{
				this.length = this.current_position;
			}
		}

		// Token: 0x06001FEB RID: 8171 RVA: 0x00075ACC File Offset: 0x00073CCC
		public override void WriteByte(byte value)
		{
			if (this.closed)
			{
				throw new ObjectDisposedException("The stream is closed");
			}
			if (this.current_position == this.capacity)
			{
				throw new NotSupportedException("The current position is at the end of the capacity of the stream");
			}
			if (this.fileaccess == FileAccess.Read)
			{
				throw new NotSupportedException("Stream does not support writing.");
			}
			Marshal.WriteByte(this.initial_pointer, (int)this.current_position, value);
			this.current_position += 1L;
			if (this.current_position > this.length)
			{
				this.length = this.current_position;
			}
		}

		// Token: 0x06001FEC RID: 8172 RVA: 0x00075B64 File Offset: 0x00073D64
		protected unsafe void Initialize(byte* pointer, long length, long capacity, FileAccess access)
		{
			if (pointer == null)
			{
				throw new ArgumentNullException("pointer");
			}
			if (length < 0L)
			{
				throw new ArgumentOutOfRangeException("length", "Non-negative number required.");
			}
			if (capacity < 0L)
			{
				throw new ArgumentOutOfRangeException("capacity", "Non-negative number required.");
			}
			if (length > capacity)
			{
				throw new ArgumentOutOfRangeException("length", "The length cannot be greater than the capacity.");
			}
			if (access < FileAccess.Read || access > FileAccess.ReadWrite)
			{
				throw new ArgumentOutOfRangeException("access", "Enum value was out of legal range.");
			}
			this.fileaccess = access;
			this.length = length;
			this.capacity = capacity;
			this.initial_position = 0L;
			this.current_position = this.initial_position;
			this.initial_pointer = new IntPtr((void*)pointer);
			this.closed = false;
		}

		// Token: 0x04000BD5 RID: 3029
		private long length;

		// Token: 0x04000BD6 RID: 3030
		private bool closed;

		// Token: 0x04000BD7 RID: 3031
		private long capacity;

		// Token: 0x04000BD8 RID: 3032
		private FileAccess fileaccess;

		// Token: 0x04000BD9 RID: 3033
		private IntPtr initial_pointer;

		// Token: 0x04000BDA RID: 3034
		private long initial_position;

		// Token: 0x04000BDB RID: 3035
		private long current_position;
	}
}
