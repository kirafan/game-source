﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.IO
{
	// Token: 0x02000245 RID: 581
	[ComVisible(true)]
	[PermissionSet(SecurityAction.InheritanceDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.FileIOPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Unrestricted=\"true\"/>\n</PermissionSet>\n")]
	[Serializable]
	public abstract class FileSystemInfo : MarshalByRefObject, ISerializable
	{
		// Token: 0x06001E2B RID: 7723 RVA: 0x0006FD10 File Offset: 0x0006DF10
		protected FileSystemInfo()
		{
			this.valid = false;
			this.FullPath = null;
		}

		// Token: 0x06001E2C RID: 7724 RVA: 0x0006FD28 File Offset: 0x0006DF28
		protected FileSystemInfo(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.FullPath = info.GetString("FullPath");
			this.OriginalPath = info.GetString("OriginalPath");
		}

		// Token: 0x06001E2D RID: 7725 RVA: 0x0006FD64 File Offset: 0x0006DF64
		[ComVisible(false)]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("OriginalPath", this.OriginalPath, typeof(string));
			info.AddValue("FullPath", this.FullPath, typeof(string));
		}

		// Token: 0x1700050C RID: 1292
		// (get) Token: 0x06001E2E RID: 7726
		public abstract bool Exists { get; }

		// Token: 0x1700050D RID: 1293
		// (get) Token: 0x06001E2F RID: 7727
		public abstract string Name { get; }

		// Token: 0x1700050E RID: 1294
		// (get) Token: 0x06001E30 RID: 7728 RVA: 0x0006FDA8 File Offset: 0x0006DFA8
		public virtual string FullName
		{
			get
			{
				return this.FullPath;
			}
		}

		// Token: 0x1700050F RID: 1295
		// (get) Token: 0x06001E31 RID: 7729 RVA: 0x0006FDB0 File Offset: 0x0006DFB0
		public string Extension
		{
			get
			{
				return Path.GetExtension(this.Name);
			}
		}

		// Token: 0x17000510 RID: 1296
		// (get) Token: 0x06001E32 RID: 7730 RVA: 0x0006FDC0 File Offset: 0x0006DFC0
		// (set) Token: 0x06001E33 RID: 7731 RVA: 0x0006FDD4 File Offset: 0x0006DFD4
		public FileAttributes Attributes
		{
			get
			{
				this.Refresh(false);
				return this.stat.Attributes;
			}
			set
			{
				MonoIOError error;
				if (!MonoIO.SetFileAttributes(this.FullName, value, out error))
				{
					throw MonoIO.GetException(this.FullName, error);
				}
				this.Refresh(true);
			}
		}

		// Token: 0x17000511 RID: 1297
		// (get) Token: 0x06001E34 RID: 7732 RVA: 0x0006FE08 File Offset: 0x0006E008
		// (set) Token: 0x06001E35 RID: 7733 RVA: 0x0006FE24 File Offset: 0x0006E024
		public DateTime CreationTime
		{
			get
			{
				this.Refresh(false);
				return DateTime.FromFileTime(this.stat.CreationTime);
			}
			set
			{
				long creation_time = value.ToFileTime();
				MonoIOError error;
				if (!MonoIO.SetFileTime(this.FullName, creation_time, -1L, -1L, out error))
				{
					throw MonoIO.GetException(this.FullName, error);
				}
				this.Refresh(true);
			}
		}

		// Token: 0x17000512 RID: 1298
		// (get) Token: 0x06001E36 RID: 7734 RVA: 0x0006FE64 File Offset: 0x0006E064
		// (set) Token: 0x06001E37 RID: 7735 RVA: 0x0006FE80 File Offset: 0x0006E080
		[ComVisible(false)]
		public DateTime CreationTimeUtc
		{
			get
			{
				return this.CreationTime.ToUniversalTime();
			}
			set
			{
				this.CreationTime = value.ToLocalTime();
			}
		}

		// Token: 0x17000513 RID: 1299
		// (get) Token: 0x06001E38 RID: 7736 RVA: 0x0006FE90 File Offset: 0x0006E090
		// (set) Token: 0x06001E39 RID: 7737 RVA: 0x0006FEAC File Offset: 0x0006E0AC
		public DateTime LastAccessTime
		{
			get
			{
				this.Refresh(false);
				return DateTime.FromFileTime(this.stat.LastAccessTime);
			}
			set
			{
				long last_access_time = value.ToFileTime();
				MonoIOError error;
				if (!MonoIO.SetFileTime(this.FullName, -1L, last_access_time, -1L, out error))
				{
					throw MonoIO.GetException(this.FullName, error);
				}
				this.Refresh(true);
			}
		}

		// Token: 0x17000514 RID: 1300
		// (get) Token: 0x06001E3A RID: 7738 RVA: 0x0006FEEC File Offset: 0x0006E0EC
		// (set) Token: 0x06001E3B RID: 7739 RVA: 0x0006FF10 File Offset: 0x0006E110
		[ComVisible(false)]
		public DateTime LastAccessTimeUtc
		{
			get
			{
				this.Refresh(false);
				return this.LastAccessTime.ToUniversalTime();
			}
			set
			{
				this.LastAccessTime = value.ToLocalTime();
			}
		}

		// Token: 0x17000515 RID: 1301
		// (get) Token: 0x06001E3C RID: 7740 RVA: 0x0006FF20 File Offset: 0x0006E120
		// (set) Token: 0x06001E3D RID: 7741 RVA: 0x0006FF3C File Offset: 0x0006E13C
		public DateTime LastWriteTime
		{
			get
			{
				this.Refresh(false);
				return DateTime.FromFileTime(this.stat.LastWriteTime);
			}
			set
			{
				long last_write_time = value.ToFileTime();
				MonoIOError error;
				if (!MonoIO.SetFileTime(this.FullName, -1L, -1L, last_write_time, out error))
				{
					throw MonoIO.GetException(this.FullName, error);
				}
				this.Refresh(true);
			}
		}

		// Token: 0x17000516 RID: 1302
		// (get) Token: 0x06001E3E RID: 7742 RVA: 0x0006FF7C File Offset: 0x0006E17C
		// (set) Token: 0x06001E3F RID: 7743 RVA: 0x0006FFA0 File Offset: 0x0006E1A0
		[ComVisible(false)]
		public DateTime LastWriteTimeUtc
		{
			get
			{
				this.Refresh(false);
				return this.LastWriteTime.ToUniversalTime();
			}
			set
			{
				this.LastWriteTime = value.ToLocalTime();
			}
		}

		// Token: 0x06001E40 RID: 7744
		public abstract void Delete();

		// Token: 0x06001E41 RID: 7745 RVA: 0x0006FFB0 File Offset: 0x0006E1B0
		public void Refresh()
		{
			this.Refresh(true);
		}

		// Token: 0x06001E42 RID: 7746 RVA: 0x0006FFBC File Offset: 0x0006E1BC
		internal void Refresh(bool force)
		{
			if (this.valid && !force)
			{
				return;
			}
			MonoIOError monoIOError;
			MonoIO.GetFileStat(this.FullName, out this.stat, out monoIOError);
			this.valid = true;
			this.InternalRefresh();
		}

		// Token: 0x06001E43 RID: 7747 RVA: 0x0006FFFC File Offset: 0x0006E1FC
		internal virtual void InternalRefresh()
		{
		}

		// Token: 0x06001E44 RID: 7748 RVA: 0x00070000 File Offset: 0x0006E200
		internal void CheckPath(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.");
			}
			if (path.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Illegal characters in path.");
			}
		}

		// Token: 0x04000B52 RID: 2898
		protected string FullPath;

		// Token: 0x04000B53 RID: 2899
		protected string OriginalPath;

		// Token: 0x04000B54 RID: 2900
		internal MonoIOStat stat;

		// Token: 0x04000B55 RID: 2901
		internal bool valid;
	}
}
