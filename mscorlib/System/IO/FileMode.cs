﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x0200023F RID: 575
	[ComVisible(true)]
	[Serializable]
	public enum FileMode
	{
		// Token: 0x04000B1D RID: 2845
		CreateNew = 1,
		// Token: 0x04000B1E RID: 2846
		Create,
		// Token: 0x04000B1F RID: 2847
		Open,
		// Token: 0x04000B20 RID: 2848
		OpenOrCreate,
		// Token: 0x04000B21 RID: 2849
		Truncate,
		// Token: 0x04000B22 RID: 2850
		Append
	}
}
