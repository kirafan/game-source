﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.IO
{
	// Token: 0x02000253 RID: 595
	[ComVisible(true)]
	[Serializable]
	public abstract class Stream : MarshalByRefObject, IDisposable
	{
		// Token: 0x17000524 RID: 1316
		// (get) Token: 0x06001EBE RID: 7870
		public abstract bool CanRead { get; }

		// Token: 0x17000525 RID: 1317
		// (get) Token: 0x06001EBF RID: 7871
		public abstract bool CanSeek { get; }

		// Token: 0x17000526 RID: 1318
		// (get) Token: 0x06001EC0 RID: 7872
		public abstract bool CanWrite { get; }

		// Token: 0x17000527 RID: 1319
		// (get) Token: 0x06001EC1 RID: 7873 RVA: 0x000722B8 File Offset: 0x000704B8
		[ComVisible(false)]
		public virtual bool CanTimeout
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000528 RID: 1320
		// (get) Token: 0x06001EC2 RID: 7874
		public abstract long Length { get; }

		// Token: 0x17000529 RID: 1321
		// (get) Token: 0x06001EC3 RID: 7875
		// (set) Token: 0x06001EC4 RID: 7876
		public abstract long Position { get; set; }

		// Token: 0x06001EC5 RID: 7877 RVA: 0x000722BC File Offset: 0x000704BC
		public void Dispose()
		{
			this.Close();
		}

		// Token: 0x06001EC6 RID: 7878 RVA: 0x000722C4 File Offset: 0x000704C4
		protected virtual void Dispose(bool disposing)
		{
		}

		// Token: 0x06001EC7 RID: 7879 RVA: 0x000722C8 File Offset: 0x000704C8
		public virtual void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x1700052A RID: 1322
		// (get) Token: 0x06001EC8 RID: 7880 RVA: 0x000722D4 File Offset: 0x000704D4
		// (set) Token: 0x06001EC9 RID: 7881 RVA: 0x000722E0 File Offset: 0x000704E0
		[ComVisible(false)]
		public virtual int ReadTimeout
		{
			get
			{
				throw new InvalidOperationException("Timeouts are not supported on this stream.");
			}
			set
			{
				throw new InvalidOperationException("Timeouts are not supported on this stream.");
			}
		}

		// Token: 0x1700052B RID: 1323
		// (get) Token: 0x06001ECA RID: 7882 RVA: 0x000722EC File Offset: 0x000704EC
		// (set) Token: 0x06001ECB RID: 7883 RVA: 0x000722F8 File Offset: 0x000704F8
		[ComVisible(false)]
		public virtual int WriteTimeout
		{
			get
			{
				throw new InvalidOperationException("Timeouts are not supported on this stream.");
			}
			set
			{
				throw new InvalidOperationException("Timeouts are not supported on this stream.");
			}
		}

		// Token: 0x06001ECC RID: 7884 RVA: 0x00072304 File Offset: 0x00070504
		public static Stream Synchronized(Stream stream)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001ECD RID: 7885 RVA: 0x0007230C File Offset: 0x0007050C
		[Obsolete("CreateWaitHandle is due for removal.  Use \"new ManualResetEvent(false)\" instead.")]
		protected virtual WaitHandle CreateWaitHandle()
		{
			return new ManualResetEvent(false);
		}

		// Token: 0x06001ECE RID: 7886
		public abstract void Flush();

		// Token: 0x06001ECF RID: 7887
		public abstract int Read([In] [Out] byte[] buffer, int offset, int count);

		// Token: 0x06001ED0 RID: 7888 RVA: 0x00072314 File Offset: 0x00070514
		public virtual int ReadByte()
		{
			byte[] array = new byte[1];
			if (this.Read(array, 0, 1) == 1)
			{
				return (int)array[0];
			}
			return -1;
		}

		// Token: 0x06001ED1 RID: 7889
		public abstract long Seek(long offset, SeekOrigin origin);

		// Token: 0x06001ED2 RID: 7890
		public abstract void SetLength(long value);

		// Token: 0x06001ED3 RID: 7891
		public abstract void Write(byte[] buffer, int offset, int count);

		// Token: 0x06001ED4 RID: 7892 RVA: 0x0007233C File Offset: 0x0007053C
		public virtual void WriteByte(byte value)
		{
			this.Write(new byte[]
			{
				value
			}, 0, 1);
		}

		// Token: 0x06001ED5 RID: 7893 RVA: 0x00072360 File Offset: 0x00070560
		public virtual IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (!this.CanRead)
			{
				throw new NotSupportedException("This stream does not support reading");
			}
			StreamAsyncResult streamAsyncResult = new StreamAsyncResult(state);
			try
			{
				int nbytes = this.Read(buffer, offset, count);
				streamAsyncResult.SetComplete(null, nbytes);
			}
			catch (Exception e)
			{
				streamAsyncResult.SetComplete(e, 0);
			}
			if (callback != null)
			{
				callback(streamAsyncResult);
			}
			return streamAsyncResult;
		}

		// Token: 0x06001ED6 RID: 7894 RVA: 0x000723DC File Offset: 0x000705DC
		public virtual IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (!this.CanWrite)
			{
				throw new NotSupportedException("This stream does not support writing");
			}
			StreamAsyncResult streamAsyncResult = new StreamAsyncResult(state);
			try
			{
				this.Write(buffer, offset, count);
				streamAsyncResult.SetComplete(null);
			}
			catch (Exception complete)
			{
				streamAsyncResult.SetComplete(complete);
			}
			if (callback != null)
			{
				callback.BeginInvoke(streamAsyncResult, null, null);
			}
			return streamAsyncResult;
		}

		// Token: 0x06001ED7 RID: 7895 RVA: 0x00072458 File Offset: 0x00070658
		public virtual int EndRead(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			StreamAsyncResult streamAsyncResult = asyncResult as StreamAsyncResult;
			if (streamAsyncResult == null || streamAsyncResult.NBytes == -1)
			{
				throw new ArgumentException("Invalid IAsyncResult", "asyncResult");
			}
			if (streamAsyncResult.Done)
			{
				throw new InvalidOperationException("EndRead already called.");
			}
			streamAsyncResult.Done = true;
			if (streamAsyncResult.Exception != null)
			{
				throw streamAsyncResult.Exception;
			}
			return streamAsyncResult.NBytes;
		}

		// Token: 0x06001ED8 RID: 7896 RVA: 0x000724D4 File Offset: 0x000706D4
		public virtual void EndWrite(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			StreamAsyncResult streamAsyncResult = asyncResult as StreamAsyncResult;
			if (streamAsyncResult == null || streamAsyncResult.NBytes != -1)
			{
				throw new ArgumentException("Invalid IAsyncResult", "asyncResult");
			}
			if (streamAsyncResult.Done)
			{
				throw new InvalidOperationException("EndWrite already called.");
			}
			streamAsyncResult.Done = true;
			if (streamAsyncResult.Exception != null)
			{
				throw streamAsyncResult.Exception;
			}
		}

		// Token: 0x04000BA3 RID: 2979
		public static readonly Stream Null = new NullStream();
	}
}
