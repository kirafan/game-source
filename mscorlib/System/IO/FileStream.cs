﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Security.AccessControl;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;

namespace System.IO
{
	// Token: 0x02000243 RID: 579
	[ComVisible(true)]
	public class FileStream : Stream
	{
		// Token: 0x06001DE6 RID: 7654 RVA: 0x0006E61C File Offset: 0x0006C81C
		[Obsolete("Use FileStream(SafeFileHandle handle, FileAccess access) instead")]
		public FileStream(IntPtr handle, FileAccess access) : this(handle, access, true, 8192, false)
		{
		}

		// Token: 0x06001DE7 RID: 7655 RVA: 0x0006E630 File Offset: 0x0006C830
		[Obsolete("Use FileStream(SafeFileHandle handle, FileAccess access) instead")]
		public FileStream(IntPtr handle, FileAccess access, bool ownsHandle) : this(handle, access, ownsHandle, 8192, false)
		{
		}

		// Token: 0x06001DE8 RID: 7656 RVA: 0x0006E644 File Offset: 0x0006C844
		[Obsolete("Use FileStream(SafeFileHandle handle, FileAccess access, int bufferSize) instead")]
		public FileStream(IntPtr handle, FileAccess access, bool ownsHandle, int bufferSize) : this(handle, access, ownsHandle, bufferSize, false)
		{
		}

		// Token: 0x06001DE9 RID: 7657 RVA: 0x0006E654 File Offset: 0x0006C854
		[Obsolete("Use FileStream(SafeFileHandle handle, FileAccess access, int bufferSize, bool isAsync) instead")]
		public FileStream(IntPtr handle, FileAccess access, bool ownsHandle, int bufferSize, bool isAsync) : this(handle, access, ownsHandle, bufferSize, isAsync, false)
		{
		}

		// Token: 0x06001DEA RID: 7658 RVA: 0x0006E664 File Offset: 0x0006C864
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		internal FileStream(IntPtr handle, FileAccess access, bool ownsHandle, int bufferSize, bool isAsync, bool noBuffering)
		{
			this.name = "[Unknown]";
			base..ctor();
			this.handle = MonoIO.InvalidHandle;
			if (handle == this.handle)
			{
				throw new ArgumentException("handle", Locale.GetText("Invalid."));
			}
			if (access < FileAccess.Read || access > FileAccess.ReadWrite)
			{
				throw new ArgumentOutOfRangeException("access");
			}
			MonoIOError monoIOError;
			MonoFileType fileType = MonoIO.GetFileType(handle, out monoIOError);
			if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(this.name, monoIOError);
			}
			if (fileType == MonoFileType.Unknown)
			{
				throw new IOException("Invalid handle.");
			}
			if (fileType == MonoFileType.Disk)
			{
				this.canseek = true;
			}
			else
			{
				this.canseek = false;
			}
			this.handle = handle;
			this.access = access;
			this.owner = ownsHandle;
			this.async = isAsync;
			this.anonymous = false;
			this.InitBuffer(bufferSize, noBuffering);
			if (this.canseek)
			{
				this.buf_start = MonoIO.Seek(handle, 0L, SeekOrigin.Current, out monoIOError);
				if (monoIOError != MonoIOError.ERROR_SUCCESS)
				{
					throw MonoIO.GetException(this.name, monoIOError);
				}
			}
			this.append_startpos = 0L;
		}

		// Token: 0x06001DEB RID: 7659 RVA: 0x0006E778 File Offset: 0x0006C978
		public FileStream(string path, FileMode mode) : this(path, mode, (mode != FileMode.Append) ? FileAccess.ReadWrite : FileAccess.Write, FileShare.Read, 8192, false, FileOptions.None)
		{
		}

		// Token: 0x06001DEC RID: 7660 RVA: 0x0006E7A4 File Offset: 0x0006C9A4
		public FileStream(string path, FileMode mode, FileAccess access) : this(path, mode, access, (access != FileAccess.Write) ? FileShare.Read : FileShare.None, 8192, false, false)
		{
		}

		// Token: 0x06001DED RID: 7661 RVA: 0x0006E7D0 File Offset: 0x0006C9D0
		public FileStream(string path, FileMode mode, FileAccess access, FileShare share) : this(path, mode, access, share, 8192, false, FileOptions.None)
		{
		}

		// Token: 0x06001DEE RID: 7662 RVA: 0x0006E7F0 File Offset: 0x0006C9F0
		public FileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize) : this(path, mode, access, share, bufferSize, false, FileOptions.None)
		{
		}

		// Token: 0x06001DEF RID: 7663 RVA: 0x0006E80C File Offset: 0x0006CA0C
		public FileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, bool useAsync) : this(path, mode, access, share, bufferSize, useAsync, FileOptions.None)
		{
		}

		// Token: 0x06001DF0 RID: 7664 RVA: 0x0006E82C File Offset: 0x0006CA2C
		public FileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, FileOptions options) : this(path, mode, access, share, bufferSize, false, options)
		{
		}

		// Token: 0x06001DF1 RID: 7665 RVA: 0x0006E84C File Offset: 0x0006CA4C
		public FileStream(SafeFileHandle handle, FileAccess access) : this(handle, access, 8192, false)
		{
		}

		// Token: 0x06001DF2 RID: 7666 RVA: 0x0006E85C File Offset: 0x0006CA5C
		public FileStream(SafeFileHandle handle, FileAccess access, int bufferSize) : this(handle, access, bufferSize, false)
		{
		}

		// Token: 0x06001DF3 RID: 7667 RVA: 0x0006E868 File Offset: 0x0006CA68
		[MonoLimitation("Need to use SafeFileHandle instead of underlying handle")]
		public FileStream(SafeFileHandle handle, FileAccess access, int bufferSize, bool isAsync) : this(handle.DangerousGetHandle(), access, false, bufferSize, isAsync)
		{
			this.safeHandle = handle;
		}

		// Token: 0x06001DF4 RID: 7668 RVA: 0x0006E890 File Offset: 0x0006CA90
		public FileStream(string path, FileMode mode, FileSystemRights rights, FileShare share, int bufferSize, FileOptions options)
		{
			this.name = "[Unknown]";
			base..ctor();
			throw new NotImplementedException();
		}

		// Token: 0x06001DF5 RID: 7669 RVA: 0x0006E8A8 File Offset: 0x0006CAA8
		public FileStream(string path, FileMode mode, FileSystemRights rights, FileShare share, int bufferSize, FileOptions options, FileSecurity fileSecurity)
		{
			this.name = "[Unknown]";
			base..ctor();
			throw new NotImplementedException();
		}

		// Token: 0x06001DF6 RID: 7670 RVA: 0x0006E8C0 File Offset: 0x0006CAC0
		internal FileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, bool isAsync, bool anonymous) : this(path, mode, access, share, bufferSize, anonymous, (!isAsync) ? FileOptions.None : FileOptions.Asynchronous)
		{
		}

		// Token: 0x06001DF7 RID: 7671 RVA: 0x0006E8F0 File Offset: 0x0006CAF0
		internal FileStream(string path, FileMode mode, FileAccess access, FileShare share, int bufferSize, bool anonymous, FileOptions options)
		{
			this.name = "[Unknown]";
			base..ctor();
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException("Path is empty");
			}
			share &= ~FileShare.Inheritable;
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize", "Positive number required.");
			}
			if (mode < FileMode.CreateNew || mode > FileMode.Append)
			{
				throw new ArgumentOutOfRangeException("mode", "Enum value was out of legal range.");
			}
			if (access < FileAccess.Read || access > FileAccess.ReadWrite)
			{
				throw new ArgumentOutOfRangeException("access", "Enum value was out of legal range.");
			}
			if (share < FileShare.None || share > (FileShare.Read | FileShare.Write | FileShare.Delete))
			{
				throw new ArgumentOutOfRangeException("share", "Enum value was out of legal range.");
			}
			if (path.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("Name has invalid chars");
			}
			if (Directory.Exists(path))
			{
				string text = Locale.GetText("Access to the path '{0}' is denied.");
				throw new UnauthorizedAccessException(string.Format(text, this.GetSecureFileName(path, false)));
			}
			if (mode == FileMode.Append && (access & FileAccess.Read) == FileAccess.Read)
			{
				throw new ArgumentException("Append access can be requested only in write-only mode.");
			}
			if ((access & FileAccess.Write) == (FileAccess)0 && mode != FileMode.Open && mode != FileMode.OpenOrCreate)
			{
				string text2 = Locale.GetText("Combining FileMode: {0} with FileAccess: {1} is invalid.");
				throw new ArgumentException(string.Format(text2, access, mode));
			}
			string directoryName;
			if (Path.DirectorySeparatorChar != '/' && path.IndexOf('/') >= 0)
			{
				directoryName = Path.GetDirectoryName(Path.GetFullPath(path));
			}
			else
			{
				directoryName = Path.GetDirectoryName(path);
			}
			if (directoryName.Length > 0)
			{
				string fullPath = Path.GetFullPath(directoryName);
				if (!Directory.Exists(fullPath))
				{
					string text3 = Locale.GetText("Could not find a part of the path \"{0}\".");
					string arg = (!anonymous) ? Path.GetFullPath(path) : directoryName;
					throw new DirectoryNotFoundException(string.Format(text3, arg));
				}
			}
			if (access == FileAccess.Read && mode != FileMode.Create && mode != FileMode.OpenOrCreate && mode != FileMode.CreateNew && !File.Exists(path))
			{
				string text4 = Locale.GetText("Could not find file \"{0}\".");
				string secureFileName = this.GetSecureFileName(path);
				throw new FileNotFoundException(string.Format(text4, secureFileName), secureFileName);
			}
			if (!anonymous)
			{
				this.name = path;
			}
			MonoIOError error;
			this.handle = MonoIO.Open(path, mode, access, share, options, out error);
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw MonoIO.GetException(this.GetSecureFileName(path), error);
			}
			this.access = access;
			this.owner = true;
			this.anonymous = anonymous;
			if (MonoIO.GetFileType(this.handle, out error) == MonoFileType.Disk)
			{
				this.canseek = true;
				this.async = ((options & FileOptions.Asynchronous) != FileOptions.None);
			}
			else
			{
				this.canseek = false;
				this.async = false;
			}
			if (access == FileAccess.Read && this.canseek && bufferSize == 8192)
			{
				long length = this.Length;
				if ((long)bufferSize > length)
				{
					bufferSize = (int)((length >= 1000L) ? length : 1000L);
				}
			}
			this.InitBuffer(bufferSize, false);
			if (mode == FileMode.Append)
			{
				this.Seek(0L, SeekOrigin.End);
				this.append_startpos = this.Position;
			}
			else
			{
				this.append_startpos = 0L;
			}
		}

		// Token: 0x170004FD RID: 1277
		// (get) Token: 0x06001DF8 RID: 7672 RVA: 0x0006EC34 File Offset: 0x0006CE34
		public override bool CanRead
		{
			get
			{
				return this.access == FileAccess.Read || this.access == FileAccess.ReadWrite;
			}
		}

		// Token: 0x170004FE RID: 1278
		// (get) Token: 0x06001DF9 RID: 7673 RVA: 0x0006EC50 File Offset: 0x0006CE50
		public override bool CanWrite
		{
			get
			{
				return this.access == FileAccess.Write || this.access == FileAccess.ReadWrite;
			}
		}

		// Token: 0x170004FF RID: 1279
		// (get) Token: 0x06001DFA RID: 7674 RVA: 0x0006EC6C File Offset: 0x0006CE6C
		public override bool CanSeek
		{
			get
			{
				return this.canseek;
			}
		}

		// Token: 0x17000500 RID: 1280
		// (get) Token: 0x06001DFB RID: 7675 RVA: 0x0006EC74 File Offset: 0x0006CE74
		public virtual bool IsAsync
		{
			get
			{
				return this.async;
			}
		}

		// Token: 0x17000501 RID: 1281
		// (get) Token: 0x06001DFC RID: 7676 RVA: 0x0006EC7C File Offset: 0x0006CE7C
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000502 RID: 1282
		// (get) Token: 0x06001DFD RID: 7677 RVA: 0x0006EC84 File Offset: 0x0006CE84
		public override long Length
		{
			get
			{
				if (this.handle == MonoIO.InvalidHandle)
				{
					throw new ObjectDisposedException("Stream has been closed");
				}
				if (!this.CanSeek)
				{
					throw new NotSupportedException("The stream does not support seeking");
				}
				this.FlushBufferIfDirty();
				MonoIOError monoIOError;
				long length = MonoIO.GetLength(this.handle, out monoIOError);
				if (monoIOError != MonoIOError.ERROR_SUCCESS)
				{
					throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
				}
				return length;
			}
		}

		// Token: 0x17000503 RID: 1283
		// (get) Token: 0x06001DFE RID: 7678 RVA: 0x0006ECF8 File Offset: 0x0006CEF8
		// (set) Token: 0x06001DFF RID: 7679 RVA: 0x0006ED4C File Offset: 0x0006CF4C
		public override long Position
		{
			get
			{
				if (this.handle == MonoIO.InvalidHandle)
				{
					throw new ObjectDisposedException("Stream has been closed");
				}
				if (!this.CanSeek)
				{
					throw new NotSupportedException("The stream does not support seeking");
				}
				return this.buf_start + (long)this.buf_offset;
			}
			set
			{
				if (this.handle == MonoIO.InvalidHandle)
				{
					throw new ObjectDisposedException("Stream has been closed");
				}
				if (!this.CanSeek)
				{
					throw new NotSupportedException("The stream does not support seeking");
				}
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException("Attempt to set the position to a negative value");
				}
				this.Seek(value, SeekOrigin.Begin);
			}
		}

		// Token: 0x17000504 RID: 1284
		// (get) Token: 0x06001E00 RID: 7680 RVA: 0x0006EDAC File Offset: 0x0006CFAC
		[Obsolete("Use SafeFileHandle instead")]
		public virtual IntPtr Handle
		{
			[PermissionSet(SecurityAction.InheritanceDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
			[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
			get
			{
				return this.handle;
			}
		}

		// Token: 0x17000505 RID: 1285
		// (get) Token: 0x06001E01 RID: 7681 RVA: 0x0006EDB4 File Offset: 0x0006CFB4
		public virtual SafeFileHandle SafeFileHandle
		{
			[PermissionSet(SecurityAction.InheritanceDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
			[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
			get
			{
				SafeFileHandle result;
				if (this.safeHandle != null)
				{
					result = this.safeHandle;
				}
				else
				{
					result = new SafeFileHandle(this.handle, this.owner);
				}
				this.FlushBuffer();
				return result;
			}
		}

		// Token: 0x06001E02 RID: 7682 RVA: 0x0006EDF4 File Offset: 0x0006CFF4
		public override int ReadByte()
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (!this.CanRead)
			{
				throw new NotSupportedException("Stream does not support reading");
			}
			if (this.buf_size != 0)
			{
				if (this.buf_offset >= this.buf_length)
				{
					this.RefillBuffer();
					if (this.buf_length == 0)
					{
						return -1;
					}
				}
				return (int)this.buf[this.buf_offset++];
			}
			if (this.ReadData(this.handle, this.buf, 0, 1) == 0)
			{
				return -1;
			}
			return (int)this.buf[0];
		}

		// Token: 0x06001E03 RID: 7683 RVA: 0x0006EEA4 File Offset: 0x0006D0A4
		public override void WriteByte(byte value)
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (!this.CanWrite)
			{
				throw new NotSupportedException("Stream does not support writing");
			}
			if (this.buf_offset == this.buf_size)
			{
				this.FlushBuffer();
			}
			if (this.buf_size == 0)
			{
				this.buf[0] = value;
				this.buf_dirty = true;
				this.buf_length = 1;
				this.FlushBuffer();
				return;
			}
			this.buf[this.buf_offset++] = value;
			if (this.buf_offset > this.buf_length)
			{
				this.buf_length = this.buf_offset;
			}
			this.buf_dirty = true;
		}

		// Token: 0x06001E04 RID: 7684 RVA: 0x0006EF64 File Offset: 0x0006D164
		public override int Read([In] [Out] byte[] array, int offset, int count)
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (!this.CanRead)
			{
				throw new NotSupportedException("Stream does not support reading");
			}
			int num = array.Length;
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			if (offset > num)
			{
				throw new ArgumentException("destination offset is beyond array size");
			}
			if (offset > num - count)
			{
				throw new ArgumentException("Reading would overrun buffer");
			}
			if (this.async)
			{
				IAsyncResult asyncResult = this.BeginRead(array, offset, count, null, null);
				return this.EndRead(asyncResult);
			}
			return this.ReadInternal(array, offset, count);
		}

		// Token: 0x06001E05 RID: 7685 RVA: 0x0006F038 File Offset: 0x0006D238
		private int ReadInternal(byte[] dest, int offset, int count)
		{
			int num = 0;
			int num2 = this.ReadSegment(dest, offset, count);
			num += num2;
			count -= num2;
			if (count == 0)
			{
				return num;
			}
			if (count > this.buf_size)
			{
				this.FlushBuffer();
				num2 = this.ReadData(this.handle, dest, offset + num, count);
				this.buf_start += (long)num2;
			}
			else
			{
				this.RefillBuffer();
				num2 = this.ReadSegment(dest, offset + num, count);
			}
			return num + num2;
		}

		// Token: 0x06001E06 RID: 7686 RVA: 0x0006F0B4 File Offset: 0x0006D2B4
		public override IAsyncResult BeginRead(byte[] array, int offset, int numBytes, AsyncCallback userCallback, object stateObject)
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (!this.CanRead)
			{
				throw new NotSupportedException("This stream does not support reading");
			}
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (numBytes < 0)
			{
				throw new ArgumentOutOfRangeException("numBytes", "Must be >= 0");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "Must be >= 0");
			}
			if (numBytes > array.Length - offset)
			{
				throw new ArgumentException("Buffer too small. numBytes/offset wrong.");
			}
			if (!this.async)
			{
				return base.BeginRead(array, offset, numBytes, userCallback, stateObject);
			}
			FileStream.ReadDelegate readDelegate = new FileStream.ReadDelegate(this.ReadInternal);
			return readDelegate.BeginInvoke(array, offset, numBytes, userCallback, stateObject);
		}

		// Token: 0x06001E07 RID: 7687 RVA: 0x0006F180 File Offset: 0x0006D380
		public override int EndRead(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			if (!this.async)
			{
				return base.EndRead(asyncResult);
			}
			AsyncResult asyncResult2 = asyncResult as AsyncResult;
			if (asyncResult2 == null)
			{
				throw new ArgumentException("Invalid IAsyncResult", "asyncResult");
			}
			FileStream.ReadDelegate readDelegate = asyncResult2.AsyncDelegate as FileStream.ReadDelegate;
			if (readDelegate == null)
			{
				throw new ArgumentException("Invalid IAsyncResult", "asyncResult");
			}
			return readDelegate.EndInvoke(asyncResult);
		}

		// Token: 0x06001E08 RID: 7688 RVA: 0x0006F1F8 File Offset: 0x0006D3F8
		public override void Write(byte[] array, int offset, int count)
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			if (offset > array.Length - count)
			{
				throw new ArgumentException("Reading would overrun buffer");
			}
			if (!this.CanWrite)
			{
				throw new NotSupportedException("Stream does not support writing");
			}
			if (this.async)
			{
				IAsyncResult asyncResult = this.BeginWrite(array, offset, count, null, null);
				this.EndWrite(asyncResult);
				return;
			}
			this.WriteInternal(array, offset, count);
		}

		// Token: 0x06001E09 RID: 7689 RVA: 0x0006F2B8 File Offset: 0x0006D4B8
		private void WriteInternal(byte[] src, int offset, int count)
		{
			if (count > this.buf_size)
			{
				this.FlushBuffer();
				int i = count;
				while (i > 0)
				{
					MonoIOError monoIOError;
					int num = MonoIO.Write(this.handle, src, offset, i, out monoIOError);
					if (monoIOError != MonoIOError.ERROR_SUCCESS)
					{
						throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
					}
					i -= num;
					offset += num;
				}
				this.buf_start += (long)count;
			}
			else
			{
				int num2 = 0;
				while (count > 0)
				{
					int num3 = this.WriteSegment(src, offset + num2, count);
					num2 += num3;
					count -= num3;
					if (count == 0)
					{
						break;
					}
					this.FlushBuffer();
				}
			}
		}

		// Token: 0x06001E0A RID: 7690 RVA: 0x0006F364 File Offset: 0x0006D564
		public override IAsyncResult BeginWrite(byte[] array, int offset, int numBytes, AsyncCallback userCallback, object stateObject)
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (!this.CanWrite)
			{
				throw new NotSupportedException("This stream does not support writing");
			}
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (numBytes < 0)
			{
				throw new ArgumentOutOfRangeException("numBytes", "Must be >= 0");
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "Must be >= 0");
			}
			if (numBytes > array.Length - offset)
			{
				throw new ArgumentException("array too small. numBytes/offset wrong.");
			}
			if (!this.async)
			{
				return base.BeginWrite(array, offset, numBytes, userCallback, stateObject);
			}
			FileStreamAsyncResult fileStreamAsyncResult = new FileStreamAsyncResult(userCallback, stateObject);
			fileStreamAsyncResult.BytesRead = -1;
			fileStreamAsyncResult.Count = numBytes;
			fileStreamAsyncResult.OriginalCount = numBytes;
			if (this.buf_dirty)
			{
				MemoryStream memoryStream = new MemoryStream();
				this.FlushBuffer(memoryStream);
				memoryStream.Write(array, offset, numBytes);
				offset = 0;
				numBytes = (int)memoryStream.Length;
			}
			FileStream.WriteDelegate writeDelegate = new FileStream.WriteDelegate(this.WriteInternal);
			return writeDelegate.BeginInvoke(array, offset, numBytes, userCallback, stateObject);
		}

		// Token: 0x06001E0B RID: 7691 RVA: 0x0006F47C File Offset: 0x0006D67C
		public override void EndWrite(IAsyncResult asyncResult)
		{
			if (asyncResult == null)
			{
				throw new ArgumentNullException("asyncResult");
			}
			if (!this.async)
			{
				base.EndWrite(asyncResult);
				return;
			}
			AsyncResult asyncResult2 = asyncResult as AsyncResult;
			if (asyncResult2 == null)
			{
				throw new ArgumentException("Invalid IAsyncResult", "asyncResult");
			}
			FileStream.WriteDelegate writeDelegate = asyncResult2.AsyncDelegate as FileStream.WriteDelegate;
			if (writeDelegate == null)
			{
				throw new ArgumentException("Invalid IAsyncResult", "asyncResult");
			}
			writeDelegate.EndInvoke(asyncResult);
		}

		// Token: 0x06001E0C RID: 7692 RVA: 0x0006F4F4 File Offset: 0x0006D6F4
		public override long Seek(long offset, SeekOrigin origin)
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (!this.CanSeek)
			{
				throw new NotSupportedException("The stream does not support seeking");
			}
			long num;
			switch (origin)
			{
			case SeekOrigin.Begin:
				num = offset;
				break;
			case SeekOrigin.Current:
				num = this.Position + offset;
				break;
			case SeekOrigin.End:
				num = this.Length + offset;
				break;
			default:
				throw new ArgumentException("origin", "Invalid SeekOrigin");
			}
			if (num < 0L)
			{
				throw new IOException("Attempted to Seek before the beginning of the stream");
			}
			if (num < this.append_startpos)
			{
				throw new IOException("Can't seek back over pre-existing data in append mode");
			}
			this.FlushBuffer();
			MonoIOError monoIOError;
			this.buf_start = MonoIO.Seek(this.handle, num, SeekOrigin.Begin, out monoIOError);
			if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
			}
			return this.buf_start;
		}

		// Token: 0x06001E0D RID: 7693 RVA: 0x0006F5E8 File Offset: 0x0006D7E8
		public override void SetLength(long value)
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (!this.CanSeek)
			{
				throw new NotSupportedException("The stream does not support seeking");
			}
			if (!this.CanWrite)
			{
				throw new NotSupportedException("The stream does not support writing");
			}
			if (value < 0L)
			{
				throw new ArgumentOutOfRangeException("value is less than 0");
			}
			this.Flush();
			MonoIOError monoIOError;
			MonoIO.SetLength(this.handle, value, out monoIOError);
			if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
			}
			if (this.Position > value)
			{
				this.Position = value;
			}
		}

		// Token: 0x06001E0E RID: 7694 RVA: 0x0006F698 File Offset: 0x0006D898
		public override void Flush()
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			this.FlushBuffer();
		}

		// Token: 0x06001E0F RID: 7695 RVA: 0x0006F6CC File Offset: 0x0006D8CC
		public virtual void Lock(long position, long length)
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (position < 0L)
			{
				throw new ArgumentOutOfRangeException("position must not be negative");
			}
			if (length < 0L)
			{
				throw new ArgumentOutOfRangeException("length must not be negative");
			}
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			MonoIOError monoIOError;
			MonoIO.Lock(this.handle, position, length, out monoIOError);
			if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
			}
		}

		// Token: 0x06001E10 RID: 7696 RVA: 0x0006F768 File Offset: 0x0006D968
		public virtual void Unlock(long position, long length)
		{
			if (this.handle == MonoIO.InvalidHandle)
			{
				throw new ObjectDisposedException("Stream has been closed");
			}
			if (position < 0L)
			{
				throw new ArgumentOutOfRangeException("position must not be negative");
			}
			if (length < 0L)
			{
				throw new ArgumentOutOfRangeException("length must not be negative");
			}
			MonoIOError monoIOError;
			MonoIO.Unlock(this.handle, position, length, out monoIOError);
			if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
			}
		}

		// Token: 0x06001E11 RID: 7697 RVA: 0x0006F7E4 File Offset: 0x0006D9E4
		~FileStream()
		{
			this.Dispose(false);
		}

		// Token: 0x06001E12 RID: 7698 RVA: 0x0006F820 File Offset: 0x0006DA20
		protected override void Dispose(bool disposing)
		{
			Exception ex = null;
			if (this.handle != MonoIO.InvalidHandle)
			{
				try
				{
					this.FlushBuffer();
				}
				catch (Exception ex2)
				{
					ex = ex2;
				}
				if (this.owner)
				{
					MonoIOError monoIOError;
					MonoIO.Close(this.handle, out monoIOError);
					if (monoIOError != MonoIOError.ERROR_SUCCESS)
					{
						throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
					}
					this.handle = MonoIO.InvalidHandle;
				}
			}
			this.canseek = false;
			this.access = (FileAccess)0;
			if (disposing)
			{
				this.buf = null;
			}
			if (disposing)
			{
				GC.SuppressFinalize(this);
			}
			if (ex != null)
			{
				throw ex;
			}
		}

		// Token: 0x06001E13 RID: 7699 RVA: 0x0006F8E0 File Offset: 0x0006DAE0
		public FileSecurity GetAccessControl()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001E14 RID: 7700 RVA: 0x0006F8E8 File Offset: 0x0006DAE8
		public void SetAccessControl(FileSecurity fileSecurity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001E15 RID: 7701 RVA: 0x0006F8F0 File Offset: 0x0006DAF0
		private int ReadSegment(byte[] dest, int dest_offset, int count)
		{
			if (count > this.buf_length - this.buf_offset)
			{
				count = this.buf_length - this.buf_offset;
			}
			if (count > 0)
			{
				Buffer.BlockCopy(this.buf, this.buf_offset, dest, dest_offset, count);
				this.buf_offset += count;
			}
			return count;
		}

		// Token: 0x06001E16 RID: 7702 RVA: 0x0006F94C File Offset: 0x0006DB4C
		private int WriteSegment(byte[] src, int src_offset, int count)
		{
			if (count > this.buf_size - this.buf_offset)
			{
				count = this.buf_size - this.buf_offset;
			}
			if (count > 0)
			{
				Buffer.BlockCopy(src, src_offset, this.buf, this.buf_offset, count);
				this.buf_offset += count;
				if (this.buf_offset > this.buf_length)
				{
					this.buf_length = this.buf_offset;
				}
				this.buf_dirty = true;
			}
			return count;
		}

		// Token: 0x06001E17 RID: 7703 RVA: 0x0006F9CC File Offset: 0x0006DBCC
		private void FlushBuffer(Stream st)
		{
			if (this.buf_dirty)
			{
				if (this.CanSeek)
				{
					MonoIOError monoIOError;
					MonoIO.Seek(this.handle, this.buf_start, SeekOrigin.Begin, out monoIOError);
					if (monoIOError != MonoIOError.ERROR_SUCCESS)
					{
						throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
					}
				}
				if (st == null)
				{
					MonoIOError monoIOError;
					MonoIO.Write(this.handle, this.buf, 0, this.buf_length, out monoIOError);
					if (monoIOError != MonoIOError.ERROR_SUCCESS)
					{
						throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
					}
				}
				else
				{
					st.Write(this.buf, 0, this.buf_length);
				}
			}
			this.buf_start += (long)this.buf_offset;
			this.buf_offset = (this.buf_length = 0);
			this.buf_dirty = false;
		}

		// Token: 0x06001E18 RID: 7704 RVA: 0x0006FA9C File Offset: 0x0006DC9C
		private void FlushBuffer()
		{
			this.FlushBuffer(null);
		}

		// Token: 0x06001E19 RID: 7705 RVA: 0x0006FAA8 File Offset: 0x0006DCA8
		private void FlushBufferIfDirty()
		{
			if (this.buf_dirty)
			{
				this.FlushBuffer(null);
			}
		}

		// Token: 0x06001E1A RID: 7706 RVA: 0x0006FABC File Offset: 0x0006DCBC
		private void RefillBuffer()
		{
			this.FlushBuffer(null);
			this.buf_length = this.ReadData(this.handle, this.buf, 0, this.buf_size);
		}

		// Token: 0x06001E1B RID: 7707 RVA: 0x0006FAF0 File Offset: 0x0006DCF0
		private int ReadData(IntPtr handle, byte[] buf, int offset, int count)
		{
			MonoIOError monoIOError;
			int num = MonoIO.Read(handle, buf, offset, count, out monoIOError);
			if (monoIOError == MonoIOError.ERROR_BROKEN_PIPE)
			{
				num = 0;
			}
			else if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(this.GetSecureFileName(this.name), monoIOError);
			}
			if (num == -1)
			{
				throw new IOException();
			}
			return num;
		}

		// Token: 0x06001E1C RID: 7708 RVA: 0x0006FB44 File Offset: 0x0006DD44
		private void InitBuffer(int size, bool noBuffering)
		{
			if (noBuffering)
			{
				size = 0;
				this.buf = new byte[1];
			}
			else
			{
				if (size <= 0)
				{
					throw new ArgumentOutOfRangeException("bufferSize", "Positive number required.");
				}
				if (size < 8)
				{
					size = 8;
				}
				this.buf = new byte[size];
			}
			this.buf_size = size;
			this.buf_start = 0L;
			this.buf_offset = (this.buf_length = 0);
			this.buf_dirty = false;
		}

		// Token: 0x06001E1D RID: 7709 RVA: 0x0006FBC0 File Offset: 0x0006DDC0
		private string GetSecureFileName(string filename)
		{
			return (!this.anonymous) ? Path.GetFullPath(filename) : Path.GetFileName(filename);
		}

		// Token: 0x06001E1E RID: 7710 RVA: 0x0006FBE0 File Offset: 0x0006DDE0
		private string GetSecureFileName(string filename, bool full)
		{
			return (!this.anonymous) ? ((!full) ? filename : Path.GetFullPath(filename)) : Path.GetFileName(filename);
		}

		// Token: 0x04000B35 RID: 2869
		internal const int DefaultBufferSize = 8192;

		// Token: 0x04000B36 RID: 2870
		private FileAccess access;

		// Token: 0x04000B37 RID: 2871
		private bool owner;

		// Token: 0x04000B38 RID: 2872
		private bool async;

		// Token: 0x04000B39 RID: 2873
		private bool canseek;

		// Token: 0x04000B3A RID: 2874
		private long append_startpos;

		// Token: 0x04000B3B RID: 2875
		private bool anonymous;

		// Token: 0x04000B3C RID: 2876
		private byte[] buf;

		// Token: 0x04000B3D RID: 2877
		private int buf_size;

		// Token: 0x04000B3E RID: 2878
		private int buf_length;

		// Token: 0x04000B3F RID: 2879
		private int buf_offset;

		// Token: 0x04000B40 RID: 2880
		private bool buf_dirty;

		// Token: 0x04000B41 RID: 2881
		private long buf_start;

		// Token: 0x04000B42 RID: 2882
		private string name;

		// Token: 0x04000B43 RID: 2883
		private IntPtr handle;

		// Token: 0x04000B44 RID: 2884
		private SafeFileHandle safeHandle;

		// Token: 0x020006DA RID: 1754
		// (Invoke) Token: 0x0600435C RID: 17244
		private delegate int ReadDelegate(byte[] buffer, int offset, int count);

		// Token: 0x020006DB RID: 1755
		// (Invoke) Token: 0x06004360 RID: 17248
		private delegate void WriteDelegate(byte[] buffer, int offset, int count);
	}
}
