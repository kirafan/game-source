﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using Mono.Security;

namespace System.IO
{
	// Token: 0x02000230 RID: 560
	[ComVisible(true)]
	[Serializable]
	public class BinaryWriter : IDisposable
	{
		// Token: 0x06001CE6 RID: 7398 RVA: 0x0006AA3C File Offset: 0x00068C3C
		protected BinaryWriter() : this(Stream.Null, Encoding.UTF8UnmarkedUnsafe)
		{
		}

		// Token: 0x06001CE7 RID: 7399 RVA: 0x0006AA50 File Offset: 0x00068C50
		public BinaryWriter(Stream output) : this(output, Encoding.UTF8UnmarkedUnsafe)
		{
		}

		// Token: 0x06001CE8 RID: 7400 RVA: 0x0006AA60 File Offset: 0x00068C60
		public BinaryWriter(Stream output, Encoding encoding)
		{
			if (output == null)
			{
				throw new ArgumentNullException("output");
			}
			if (encoding == null)
			{
				throw new ArgumentNullException("encoding");
			}
			if (!output.CanWrite)
			{
				throw new ArgumentException(Locale.GetText("Stream does not support writing or already closed."));
			}
			this.OutStream = output;
			this.m_encoding = encoding;
			this.buffer = new byte[16];
		}

		// Token: 0x06001CEA RID: 7402 RVA: 0x0006AAD8 File Offset: 0x00068CD8
		void IDisposable.Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x170004DD RID: 1245
		// (get) Token: 0x06001CEB RID: 7403 RVA: 0x0006AAE4 File Offset: 0x00068CE4
		public virtual Stream BaseStream
		{
			get
			{
				return this.OutStream;
			}
		}

		// Token: 0x06001CEC RID: 7404 RVA: 0x0006AAEC File Offset: 0x00068CEC
		public virtual void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x06001CED RID: 7405 RVA: 0x0006AAF8 File Offset: 0x00068CF8
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && this.OutStream != null)
			{
				this.OutStream.Close();
			}
			this.buffer = null;
			this.m_encoding = null;
			this.disposed = true;
		}

		// Token: 0x06001CEE RID: 7406 RVA: 0x0006AB2C File Offset: 0x00068D2C
		public virtual void Flush()
		{
			this.OutStream.Flush();
		}

		// Token: 0x06001CEF RID: 7407 RVA: 0x0006AB3C File Offset: 0x00068D3C
		public virtual long Seek(int offset, SeekOrigin origin)
		{
			return this.OutStream.Seek((long)offset, origin);
		}

		// Token: 0x06001CF0 RID: 7408 RVA: 0x0006AB4C File Offset: 0x00068D4C
		public virtual void Write(bool value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			this.buffer[0] = ((!value) ? 0 : 1);
			this.OutStream.Write(this.buffer, 0, 1);
		}

		// Token: 0x06001CF1 RID: 7409 RVA: 0x0006ABA0 File Offset: 0x00068DA0
		public virtual void Write(byte value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			this.OutStream.WriteByte(value);
		}

		// Token: 0x06001CF2 RID: 7410 RVA: 0x0006ABCC File Offset: 0x00068DCC
		public virtual void Write(byte[] buffer)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			this.OutStream.Write(buffer, 0, buffer.Length);
		}

		// Token: 0x06001CF3 RID: 7411 RVA: 0x0006AC18 File Offset: 0x00068E18
		public virtual void Write(byte[] buffer, int index, int count)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			this.OutStream.Write(buffer, index, count);
		}

		// Token: 0x06001CF4 RID: 7412 RVA: 0x0006AC60 File Offset: 0x00068E60
		public virtual void Write(char ch)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			char[] chars = new char[]
			{
				ch
			};
			byte[] bytes = this.m_encoding.GetBytes(chars, 0, 1);
			this.OutStream.Write(bytes, 0, bytes.Length);
		}

		// Token: 0x06001CF5 RID: 7413 RVA: 0x0006ACB4 File Offset: 0x00068EB4
		public virtual void Write(char[] chars)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			byte[] bytes = this.m_encoding.GetBytes(chars, 0, chars.Length);
			this.OutStream.Write(bytes, 0, bytes.Length);
		}

		// Token: 0x06001CF6 RID: 7414 RVA: 0x0006AD10 File Offset: 0x00068F10
		public virtual void Write(char[] chars, int index, int count)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			byte[] bytes = this.m_encoding.GetBytes(chars, index, count);
			this.OutStream.Write(bytes, 0, bytes.Length);
		}

		// Token: 0x06001CF7 RID: 7415 RVA: 0x0006AD68 File Offset: 0x00068F68
		public unsafe virtual void Write(decimal value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			if (BitConverter.IsLittleEndian)
			{
				for (int i = 0; i < 16; i++)
				{
					if (i < 4)
					{
						this.buffer[i + 12] = *(ref value + i);
					}
					else if (i < 8)
					{
						this.buffer[i + 4] = *(ref value + i);
					}
					else if (i < 12)
					{
						this.buffer[i - 8] = *(ref value + i);
					}
					else
					{
						this.buffer[i - 8] = *(ref value + i);
					}
				}
			}
			else
			{
				for (int j = 0; j < 16; j++)
				{
					if (j < 4)
					{
						this.buffer[15 - j] = *(ref value + j);
					}
					else if (j < 8)
					{
						this.buffer[15 - j] = *(ref value + j);
					}
					else if (j < 12)
					{
						this.buffer[11 - j] = *(ref value + j);
					}
					else
					{
						this.buffer[19 - j] = *(ref value + j);
					}
				}
			}
			this.OutStream.Write(this.buffer, 0, 16);
		}

		// Token: 0x06001CF8 RID: 7416 RVA: 0x0006AE9C File Offset: 0x0006909C
		public virtual void Write(double value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			this.OutStream.Write(BitConverterLE.GetBytes(value), 0, 8);
		}

		// Token: 0x06001CF9 RID: 7417 RVA: 0x0006AED8 File Offset: 0x000690D8
		public virtual void Write(short value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			this.buffer[0] = (byte)value;
			this.buffer[1] = (byte)(value >> 8);
			this.OutStream.Write(this.buffer, 0, 2);
		}

		// Token: 0x06001CFA RID: 7418 RVA: 0x0006AF2C File Offset: 0x0006912C
		public virtual void Write(int value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			this.buffer[0] = (byte)value;
			this.buffer[1] = (byte)(value >> 8);
			this.buffer[2] = (byte)(value >> 16);
			this.buffer[3] = (byte)(value >> 24);
			this.OutStream.Write(this.buffer, 0, 4);
		}

		// Token: 0x06001CFB RID: 7419 RVA: 0x0006AF98 File Offset: 0x00069198
		public virtual void Write(long value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			int i = 0;
			int num = 0;
			while (i < 8)
			{
				this.buffer[i] = (byte)(value >> num);
				i++;
				num += 8;
			}
			this.OutStream.Write(this.buffer, 0, 8);
		}

		// Token: 0x06001CFC RID: 7420 RVA: 0x0006AFFC File Offset: 0x000691FC
		[CLSCompliant(false)]
		public virtual void Write(sbyte value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			this.buffer[0] = (byte)value;
			this.OutStream.Write(this.buffer, 0, 1);
		}

		// Token: 0x06001CFD RID: 7421 RVA: 0x0006B044 File Offset: 0x00069244
		public virtual void Write(float value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			this.OutStream.Write(BitConverterLE.GetBytes(value), 0, 4);
		}

		// Token: 0x06001CFE RID: 7422 RVA: 0x0006B080 File Offset: 0x00069280
		public virtual void Write(string value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			int byteCount = this.m_encoding.GetByteCount(value);
			this.Write7BitEncodedInt(byteCount);
			if (this.stringBuffer == null)
			{
				this.stringBuffer = new byte[512];
				this.maxCharsPerRound = 512 / this.m_encoding.GetMaxByteCount(1);
			}
			int num = 0;
			int num2;
			for (int i = value.Length; i > 0; i -= num2)
			{
				num2 = ((i <= this.maxCharsPerRound) ? i : this.maxCharsPerRound);
				int bytes = this.m_encoding.GetBytes(value, num, num2, this.stringBuffer, 0);
				this.OutStream.Write(this.stringBuffer, 0, bytes);
				num += num2;
			}
		}

		// Token: 0x06001CFF RID: 7423 RVA: 0x0006B150 File Offset: 0x00069350
		[CLSCompliant(false)]
		public virtual void Write(ushort value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			this.buffer[0] = (byte)value;
			this.buffer[1] = (byte)(value >> 8);
			this.OutStream.Write(this.buffer, 0, 2);
		}

		// Token: 0x06001D00 RID: 7424 RVA: 0x0006B1A4 File Offset: 0x000693A4
		[CLSCompliant(false)]
		public virtual void Write(uint value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			this.buffer[0] = (byte)value;
			this.buffer[1] = (byte)(value >> 8);
			this.buffer[2] = (byte)(value >> 16);
			this.buffer[3] = (byte)(value >> 24);
			this.OutStream.Write(this.buffer, 0, 4);
		}

		// Token: 0x06001D01 RID: 7425 RVA: 0x0006B210 File Offset: 0x00069410
		[CLSCompliant(false)]
		public virtual void Write(ulong value)
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BinaryWriter", "Cannot write to a closed BinaryWriter");
			}
			int i = 0;
			int num = 0;
			while (i < 8)
			{
				this.buffer[i] = (byte)(value >> num);
				i++;
				num += 8;
			}
			this.OutStream.Write(this.buffer, 0, 8);
		}

		// Token: 0x06001D02 RID: 7426 RVA: 0x0006B274 File Offset: 0x00069474
		protected void Write7BitEncodedInt(int value)
		{
			do
			{
				int num = value >> 7 & 33554431;
				byte b = (byte)(value & 127);
				if (num != 0)
				{
					b |= 128;
				}
				this.Write(b);
				value = num;
			}
			while (value != 0);
		}

		// Token: 0x04000AE4 RID: 2788
		public static readonly BinaryWriter Null = new BinaryWriter();

		// Token: 0x04000AE5 RID: 2789
		protected Stream OutStream;

		// Token: 0x04000AE6 RID: 2790
		private Encoding m_encoding;

		// Token: 0x04000AE7 RID: 2791
		private byte[] buffer;

		// Token: 0x04000AE8 RID: 2792
		private bool disposed;

		// Token: 0x04000AE9 RID: 2793
		private byte[] stringBuffer;

		// Token: 0x04000AEA RID: 2794
		private int maxCharsPerRound;
	}
}
