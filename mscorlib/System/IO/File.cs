﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Text;

namespace System.IO
{
	// Token: 0x0200023A RID: 570
	[ComVisible(true)]
	public static class File
	{
		// Token: 0x06001D7B RID: 7547 RVA: 0x0006CEF4 File Offset: 0x0006B0F4
		public static void AppendAllText(string path, string contents)
		{
			using (TextWriter textWriter = new StreamWriter(path, true))
			{
				textWriter.Write(contents);
			}
		}

		// Token: 0x06001D7C RID: 7548 RVA: 0x0006CF40 File Offset: 0x0006B140
		public static void AppendAllText(string path, string contents, Encoding encoding)
		{
			using (TextWriter textWriter = new StreamWriter(path, true, encoding))
			{
				textWriter.Write(contents);
			}
		}

		// Token: 0x06001D7D RID: 7549 RVA: 0x0006CF8C File Offset: 0x0006B18C
		public static StreamWriter AppendText(string path)
		{
			return new StreamWriter(path, true);
		}

		// Token: 0x06001D7E RID: 7550 RVA: 0x0006CF98 File Offset: 0x0006B198
		public static void Copy(string sourceFileName, string destFileName)
		{
			File.Copy(sourceFileName, destFileName, false);
		}

		// Token: 0x06001D7F RID: 7551 RVA: 0x0006CFA4 File Offset: 0x0006B1A4
		public static void Copy(string sourceFileName, string destFileName, bool overwrite)
		{
			if (sourceFileName == null)
			{
				throw new ArgumentNullException("sourceFileName");
			}
			if (destFileName == null)
			{
				throw new ArgumentNullException("destFileName");
			}
			if (sourceFileName.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.", "sourceFileName");
			}
			if (sourceFileName.Trim().Length == 0 || sourceFileName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("The file name is not valid.");
			}
			if (destFileName.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.", "destFileName");
			}
			if (destFileName.Trim().Length == 0 || destFileName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("The file name is not valid.");
			}
			MonoIOError error;
			if (!MonoIO.Exists(sourceFileName, out error))
			{
				throw new FileNotFoundException(Locale.GetText("{0} does not exist", new object[]
				{
					sourceFileName
				}), sourceFileName);
			}
			if ((File.GetAttributes(sourceFileName) & FileAttributes.Directory) == FileAttributes.Directory)
			{
				throw new ArgumentException(Locale.GetText("{0} is a directory", new object[]
				{
					sourceFileName
				}));
			}
			if (MonoIO.Exists(destFileName, out error))
			{
				if ((File.GetAttributes(destFileName) & FileAttributes.Directory) == FileAttributes.Directory)
				{
					throw new ArgumentException(Locale.GetText("{0} is a directory", new object[]
					{
						destFileName
					}));
				}
				if (!overwrite)
				{
					throw new IOException(Locale.GetText("{0} already exists", new object[]
					{
						destFileName
					}));
				}
			}
			string directoryName = Path.GetDirectoryName(destFileName);
			if (directoryName != string.Empty && !Directory.Exists(directoryName))
			{
				throw new DirectoryNotFoundException(Locale.GetText("Destination directory not found: {0}", new object[]
				{
					directoryName
				}));
			}
			if (!MonoIO.CopyFile(sourceFileName, destFileName, overwrite, out error))
			{
				string text = Locale.GetText("{0}\" or \"{1}", new object[]
				{
					sourceFileName,
					destFileName
				});
				throw MonoIO.GetException(text, error);
			}
		}

		// Token: 0x06001D80 RID: 7552 RVA: 0x0006D178 File Offset: 0x0006B378
		public static FileStream Create(string path)
		{
			return File.Create(path, 8192);
		}

		// Token: 0x06001D81 RID: 7553 RVA: 0x0006D188 File Offset: 0x0006B388
		public static FileStream Create(string path, int bufferSize)
		{
			return new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.None, bufferSize);
		}

		// Token: 0x06001D82 RID: 7554 RVA: 0x0006D194 File Offset: 0x0006B394
		[MonoTODO("options not implemented")]
		public static FileStream Create(string path, int bufferSize, FileOptions options)
		{
			return File.Create(path, bufferSize, options, null);
		}

		// Token: 0x06001D83 RID: 7555 RVA: 0x0006D1A0 File Offset: 0x0006B3A0
		[MonoTODO("options and fileSecurity not implemented")]
		public static FileStream Create(string path, int bufferSize, FileOptions options, FileSecurity fileSecurity)
		{
			return new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.None, bufferSize, options);
		}

		// Token: 0x06001D84 RID: 7556 RVA: 0x0006D1B0 File Offset: 0x0006B3B0
		public static StreamWriter CreateText(string path)
		{
			return new StreamWriter(path, false);
		}

		// Token: 0x06001D85 RID: 7557 RVA: 0x0006D1BC File Offset: 0x0006B3BC
		public static void Delete(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Trim().Length == 0 || path.IndexOfAny(Path.InvalidPathChars) >= 0)
			{
				throw new ArgumentException("path");
			}
			if (Directory.Exists(path))
			{
				throw new UnauthorizedAccessException(Locale.GetText("{0} is a directory", new object[]
				{
					path
				}));
			}
			string directoryName = Path.GetDirectoryName(path);
			if (directoryName != string.Empty && !Directory.Exists(directoryName))
			{
				throw new DirectoryNotFoundException(Locale.GetText("Could not find a part of the path \"{0}\".", new object[]
				{
					path
				}));
			}
			MonoIOError monoIOError;
			if (!MonoIO.DeleteFile(path, out monoIOError) && monoIOError != MonoIOError.ERROR_FILE_NOT_FOUND)
			{
				throw MonoIO.GetException(path, monoIOError);
			}
		}

		// Token: 0x06001D86 RID: 7558 RVA: 0x0006D284 File Offset: 0x0006B484
		public static bool Exists(string path)
		{
			MonoIOError monoIOError;
			return path != null && path.Trim().Length != 0 && path.IndexOfAny(Path.InvalidPathChars) < 0 && MonoIO.ExistsFile(path, out monoIOError);
		}

		// Token: 0x06001D87 RID: 7559 RVA: 0x0006D2C4 File Offset: 0x0006B4C4
		public static FileSecurity GetAccessControl(string path)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001D88 RID: 7560 RVA: 0x0006D2CC File Offset: 0x0006B4CC
		public static FileSecurity GetAccessControl(string path, AccessControlSections includeSections)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001D89 RID: 7561 RVA: 0x0006D2D4 File Offset: 0x0006B4D4
		public static FileAttributes GetAttributes(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Trim().Length == 0)
			{
				throw new ArgumentException(Locale.GetText("Path is empty"));
			}
			if (path.IndexOfAny(Path.InvalidPathChars) >= 0)
			{
				throw new ArgumentException(Locale.GetText("Path contains invalid chars"));
			}
			MonoIOError monoIOError;
			FileAttributes fileAttributes = MonoIO.GetFileAttributes(path, out monoIOError);
			if (monoIOError != MonoIOError.ERROR_SUCCESS)
			{
				throw MonoIO.GetException(path, monoIOError);
			}
			return fileAttributes;
		}

		// Token: 0x06001D8A RID: 7562 RVA: 0x0006D34C File Offset: 0x0006B54C
		public static DateTime GetCreationTime(string path)
		{
			File.CheckPathExceptions(path);
			MonoIOStat monoIOStat;
			MonoIOError monoIOError;
			if (MonoIO.GetFileStat(path, out monoIOStat, out monoIOError))
			{
				return DateTime.FromFileTime(monoIOStat.CreationTime);
			}
			if (monoIOError == MonoIOError.ERROR_PATH_NOT_FOUND || monoIOError == MonoIOError.ERROR_FILE_NOT_FOUND)
			{
				return File.DefaultLocalFileTime;
			}
			throw new IOException(path);
		}

		// Token: 0x06001D8B RID: 7563 RVA: 0x0006D398 File Offset: 0x0006B598
		public static DateTime GetCreationTimeUtc(string path)
		{
			return File.GetCreationTime(path).ToUniversalTime();
		}

		// Token: 0x06001D8C RID: 7564 RVA: 0x0006D3B4 File Offset: 0x0006B5B4
		public static DateTime GetLastAccessTime(string path)
		{
			File.CheckPathExceptions(path);
			MonoIOStat monoIOStat;
			MonoIOError monoIOError;
			if (MonoIO.GetFileStat(path, out monoIOStat, out monoIOError))
			{
				return DateTime.FromFileTime(monoIOStat.LastAccessTime);
			}
			if (monoIOError == MonoIOError.ERROR_PATH_NOT_FOUND || monoIOError == MonoIOError.ERROR_FILE_NOT_FOUND)
			{
				return File.DefaultLocalFileTime;
			}
			throw new IOException(path);
		}

		// Token: 0x06001D8D RID: 7565 RVA: 0x0006D400 File Offset: 0x0006B600
		public static DateTime GetLastAccessTimeUtc(string path)
		{
			return File.GetLastAccessTime(path).ToUniversalTime();
		}

		// Token: 0x06001D8E RID: 7566 RVA: 0x0006D41C File Offset: 0x0006B61C
		public static DateTime GetLastWriteTime(string path)
		{
			File.CheckPathExceptions(path);
			MonoIOStat monoIOStat;
			MonoIOError monoIOError;
			if (MonoIO.GetFileStat(path, out monoIOStat, out monoIOError))
			{
				return DateTime.FromFileTime(monoIOStat.LastWriteTime);
			}
			if (monoIOError == MonoIOError.ERROR_PATH_NOT_FOUND || monoIOError == MonoIOError.ERROR_FILE_NOT_FOUND)
			{
				return File.DefaultLocalFileTime;
			}
			throw new IOException(path);
		}

		// Token: 0x06001D8F RID: 7567 RVA: 0x0006D468 File Offset: 0x0006B668
		public static DateTime GetLastWriteTimeUtc(string path)
		{
			return File.GetLastWriteTime(path).ToUniversalTime();
		}

		// Token: 0x06001D90 RID: 7568 RVA: 0x0006D484 File Offset: 0x0006B684
		public static void Move(string sourceFileName, string destFileName)
		{
			if (sourceFileName == null)
			{
				throw new ArgumentNullException("sourceFileName");
			}
			if (destFileName == null)
			{
				throw new ArgumentNullException("destFileName");
			}
			if (sourceFileName.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.", "sourceFileName");
			}
			if (sourceFileName.Trim().Length == 0 || sourceFileName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("The file name is not valid.");
			}
			if (destFileName.Length == 0)
			{
				throw new ArgumentException("An empty file name is not valid.", "destFileName");
			}
			if (destFileName.Trim().Length == 0 || destFileName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("The file name is not valid.");
			}
			MonoIOError monoIOError;
			if (!MonoIO.Exists(sourceFileName, out monoIOError))
			{
				throw new FileNotFoundException(Locale.GetText("{0} does not exist", new object[]
				{
					sourceFileName
				}), sourceFileName);
			}
			string directoryName = Path.GetDirectoryName(destFileName);
			if (directoryName != string.Empty && !Directory.Exists(directoryName))
			{
				throw new DirectoryNotFoundException(Locale.GetText("Could not find a part of the path."));
			}
			if (MonoIO.MoveFile(sourceFileName, destFileName, out monoIOError))
			{
				return;
			}
			if (monoIOError == MonoIOError.ERROR_ALREADY_EXISTS)
			{
				throw MonoIO.GetException(monoIOError);
			}
			if (monoIOError == MonoIOError.ERROR_SHARING_VIOLATION)
			{
				throw MonoIO.GetException(sourceFileName, monoIOError);
			}
			throw MonoIO.GetException(monoIOError);
		}

		// Token: 0x06001D91 RID: 7569 RVA: 0x0006D5D4 File Offset: 0x0006B7D4
		public static FileStream Open(string path, FileMode mode)
		{
			return new FileStream(path, mode, (mode != FileMode.Append) ? FileAccess.ReadWrite : FileAccess.Write, FileShare.None);
		}

		// Token: 0x06001D92 RID: 7570 RVA: 0x0006D5EC File Offset: 0x0006B7EC
		public static FileStream Open(string path, FileMode mode, FileAccess access)
		{
			return new FileStream(path, mode, access, FileShare.None);
		}

		// Token: 0x06001D93 RID: 7571 RVA: 0x0006D5F8 File Offset: 0x0006B7F8
		public static FileStream Open(string path, FileMode mode, FileAccess access, FileShare share)
		{
			return new FileStream(path, mode, access, share);
		}

		// Token: 0x06001D94 RID: 7572 RVA: 0x0006D604 File Offset: 0x0006B804
		public static FileStream OpenRead(string path)
		{
			return new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
		}

		// Token: 0x06001D95 RID: 7573 RVA: 0x0006D610 File Offset: 0x0006B810
		public static StreamReader OpenText(string path)
		{
			return new StreamReader(path);
		}

		// Token: 0x06001D96 RID: 7574 RVA: 0x0006D618 File Offset: 0x0006B818
		public static FileStream OpenWrite(string path)
		{
			return new FileStream(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.None);
		}

		// Token: 0x06001D97 RID: 7575 RVA: 0x0006D624 File Offset: 0x0006B824
		public static void Replace(string sourceFileName, string destinationFileName, string destinationBackupFileName)
		{
			File.Replace(sourceFileName, destinationFileName, destinationBackupFileName, false);
		}

		// Token: 0x06001D98 RID: 7576 RVA: 0x0006D630 File Offset: 0x0006B830
		public static void Replace(string sourceFileName, string destinationFileName, string destinationBackupFileName, bool ignoreMetadataErrors)
		{
			if (sourceFileName == null)
			{
				throw new ArgumentNullException("sourceFileName");
			}
			if (destinationFileName == null)
			{
				throw new ArgumentNullException("destinationFileName");
			}
			if (sourceFileName.Trim().Length == 0 || sourceFileName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("sourceFileName");
			}
			if (destinationFileName.Trim().Length == 0 || destinationFileName.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException("destinationFileName");
			}
			string fullPath = Path.GetFullPath(sourceFileName);
			string fullPath2 = Path.GetFullPath(destinationFileName);
			MonoIOError error;
			if (MonoIO.ExistsDirectory(fullPath, out error))
			{
				throw new IOException(Locale.GetText("{0} is a directory", new object[]
				{
					sourceFileName
				}));
			}
			if (MonoIO.ExistsDirectory(fullPath2, out error))
			{
				throw new IOException(Locale.GetText("{0} is a directory", new object[]
				{
					destinationFileName
				}));
			}
			if (!File.Exists(fullPath))
			{
				throw new FileNotFoundException(Locale.GetText("{0} does not exist", new object[]
				{
					sourceFileName
				}), sourceFileName);
			}
			if (!File.Exists(fullPath2))
			{
				throw new FileNotFoundException(Locale.GetText("{0} does not exist", new object[]
				{
					destinationFileName
				}), destinationFileName);
			}
			if (fullPath == fullPath2)
			{
				throw new IOException(Locale.GetText("Source and destination arguments are the same file."));
			}
			string text = null;
			if (destinationBackupFileName != null)
			{
				if (destinationBackupFileName.Trim().Length == 0 || destinationBackupFileName.IndexOfAny(Path.InvalidPathChars) != -1)
				{
					throw new ArgumentException("destinationBackupFileName");
				}
				text = Path.GetFullPath(destinationBackupFileName);
				if (MonoIO.ExistsDirectory(text, out error))
				{
					throw new IOException(Locale.GetText("{0} is a directory", new object[]
					{
						destinationBackupFileName
					}));
				}
				if (fullPath == text)
				{
					throw new IOException(Locale.GetText("Source and backup arguments are the same file."));
				}
				if (fullPath2 == text)
				{
					throw new IOException(Locale.GetText("Destination and backup arguments are the same file."));
				}
			}
			if (!MonoIO.ReplaceFile(fullPath, fullPath2, text, ignoreMetadataErrors, out error))
			{
				throw MonoIO.GetException(error);
			}
		}

		// Token: 0x06001D99 RID: 7577 RVA: 0x0006D82C File Offset: 0x0006BA2C
		public static void SetAccessControl(string path, FileSecurity fileSecurity)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001D9A RID: 7578 RVA: 0x0006D834 File Offset: 0x0006BA34
		public static void SetAttributes(string path, FileAttributes fileAttributes)
		{
			File.CheckPathExceptions(path);
			MonoIOError error;
			if (!MonoIO.SetFileAttributes(path, fileAttributes, out error))
			{
				throw MonoIO.GetException(path, error);
			}
		}

		// Token: 0x06001D9B RID: 7579 RVA: 0x0006D860 File Offset: 0x0006BA60
		public static void SetCreationTime(string path, DateTime creationTime)
		{
			File.CheckPathExceptions(path);
			MonoIOError error;
			if (!MonoIO.Exists(path, out error))
			{
				throw MonoIO.GetException(path, error);
			}
			if (!MonoIO.SetCreationTime(path, creationTime, out error))
			{
				throw MonoIO.GetException(path, error);
			}
		}

		// Token: 0x06001D9C RID: 7580 RVA: 0x0006D8A0 File Offset: 0x0006BAA0
		public static void SetCreationTimeUtc(string path, DateTime creationTimeUtc)
		{
			File.SetCreationTime(path, creationTimeUtc.ToLocalTime());
		}

		// Token: 0x06001D9D RID: 7581 RVA: 0x0006D8B0 File Offset: 0x0006BAB0
		public static void SetLastAccessTime(string path, DateTime lastAccessTime)
		{
			File.CheckPathExceptions(path);
			MonoIOError error;
			if (!MonoIO.Exists(path, out error))
			{
				throw MonoIO.GetException(path, error);
			}
			if (!MonoIO.SetLastAccessTime(path, lastAccessTime, out error))
			{
				throw MonoIO.GetException(path, error);
			}
		}

		// Token: 0x06001D9E RID: 7582 RVA: 0x0006D8F0 File Offset: 0x0006BAF0
		public static void SetLastAccessTimeUtc(string path, DateTime lastAccessTimeUtc)
		{
			File.SetLastAccessTime(path, lastAccessTimeUtc.ToLocalTime());
		}

		// Token: 0x06001D9F RID: 7583 RVA: 0x0006D900 File Offset: 0x0006BB00
		public static void SetLastWriteTime(string path, DateTime lastWriteTime)
		{
			File.CheckPathExceptions(path);
			MonoIOError error;
			if (!MonoIO.Exists(path, out error))
			{
				throw MonoIO.GetException(path, error);
			}
			if (!MonoIO.SetLastWriteTime(path, lastWriteTime, out error))
			{
				throw MonoIO.GetException(path, error);
			}
		}

		// Token: 0x06001DA0 RID: 7584 RVA: 0x0006D940 File Offset: 0x0006BB40
		public static void SetLastWriteTimeUtc(string path, DateTime lastWriteTimeUtc)
		{
			File.SetLastWriteTime(path, lastWriteTimeUtc.ToLocalTime());
		}

		// Token: 0x06001DA1 RID: 7585 RVA: 0x0006D950 File Offset: 0x0006BB50
		private static void CheckPathExceptions(string path)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path.Length == 0)
			{
				throw new ArgumentException(Locale.GetText("Path is empty"));
			}
			if (path.Trim().Length == 0)
			{
				throw new ArgumentException(Locale.GetText("Path is empty"));
			}
			if (path.IndexOfAny(Path.InvalidPathChars) != -1)
			{
				throw new ArgumentException(Locale.GetText("Path contains invalid chars"));
			}
		}

		// Token: 0x06001DA2 RID: 7586 RVA: 0x0006D9CC File Offset: 0x0006BBCC
		public static byte[] ReadAllBytes(string path)
		{
			byte[] result;
			using (FileStream fileStream = File.OpenRead(path))
			{
				long length = fileStream.Length;
				if (length > 2147483647L)
				{
					throw new IOException("Reading more than 2GB with this call is not supported");
				}
				int num = 0;
				int i = (int)length;
				byte[] array = new byte[length];
				while (i > 0)
				{
					int num2 = fileStream.Read(array, num, i);
					if (num2 == 0)
					{
						throw new IOException("Unexpected end of stream");
					}
					num += num2;
					i -= num2;
				}
				result = array;
			}
			return result;
		}

		// Token: 0x06001DA3 RID: 7587 RVA: 0x0006DA7C File Offset: 0x0006BC7C
		public static string[] ReadAllLines(string path)
		{
			string[] result;
			using (StreamReader streamReader = File.OpenText(path))
			{
				result = File.ReadAllLines(streamReader);
			}
			return result;
		}

		// Token: 0x06001DA4 RID: 7588 RVA: 0x0006DACC File Offset: 0x0006BCCC
		public static string[] ReadAllLines(string path, Encoding encoding)
		{
			string[] result;
			using (StreamReader streamReader = new StreamReader(path, encoding))
			{
				result = File.ReadAllLines(streamReader);
			}
			return result;
		}

		// Token: 0x06001DA5 RID: 7589 RVA: 0x0006DB1C File Offset: 0x0006BD1C
		private static string[] ReadAllLines(StreamReader reader)
		{
			List<string> list = new List<string>();
			while (!reader.EndOfStream)
			{
				list.Add(reader.ReadLine());
			}
			return list.ToArray();
		}

		// Token: 0x06001DA6 RID: 7590 RVA: 0x0006DB54 File Offset: 0x0006BD54
		public static string ReadAllText(string path)
		{
			return File.ReadAllText(path, Encoding.UTF8Unmarked);
		}

		// Token: 0x06001DA7 RID: 7591 RVA: 0x0006DB64 File Offset: 0x0006BD64
		public static string ReadAllText(string path, Encoding encoding)
		{
			string result;
			using (StreamReader streamReader = new StreamReader(path, encoding))
			{
				result = streamReader.ReadToEnd();
			}
			return result;
		}

		// Token: 0x06001DA8 RID: 7592 RVA: 0x0006DBB4 File Offset: 0x0006BDB4
		public static void WriteAllBytes(string path, byte[] bytes)
		{
			using (Stream stream = File.Create(path))
			{
				stream.Write(bytes, 0, bytes.Length);
			}
		}

		// Token: 0x06001DA9 RID: 7593 RVA: 0x0006DC04 File Offset: 0x0006BE04
		public static void WriteAllLines(string path, string[] contents)
		{
			using (StreamWriter streamWriter = new StreamWriter(path))
			{
				File.WriteAllLines(streamWriter, contents);
			}
		}

		// Token: 0x06001DAA RID: 7594 RVA: 0x0006DC50 File Offset: 0x0006BE50
		public static void WriteAllLines(string path, string[] contents, Encoding encoding)
		{
			using (StreamWriter streamWriter = new StreamWriter(path, false, encoding))
			{
				File.WriteAllLines(streamWriter, contents);
			}
		}

		// Token: 0x06001DAB RID: 7595 RVA: 0x0006DC9C File Offset: 0x0006BE9C
		private static void WriteAllLines(StreamWriter writer, string[] contents)
		{
			foreach (string value in contents)
			{
				writer.WriteLine(value);
			}
		}

		// Token: 0x06001DAC RID: 7596 RVA: 0x0006DCCC File Offset: 0x0006BECC
		public static void WriteAllText(string path, string contents)
		{
			File.WriteAllText(path, contents, Encoding.UTF8Unmarked);
		}

		// Token: 0x06001DAD RID: 7597 RVA: 0x0006DCDC File Offset: 0x0006BEDC
		public static void WriteAllText(string path, string contents, Encoding encoding)
		{
			using (StreamWriter streamWriter = new StreamWriter(path, false, encoding))
			{
				streamWriter.Write(contents);
			}
		}

		// Token: 0x170004F0 RID: 1264
		// (get) Token: 0x06001DAE RID: 7598 RVA: 0x0006DD28 File Offset: 0x0006BF28
		private static DateTime DefaultLocalFileTime
		{
			get
			{
				DateTime? dateTime = File.defaultLocalFileTime;
				if (dateTime == null)
				{
					DateTime dateTime2 = new DateTime(1601, 1, 1);
					File.defaultLocalFileTime = new DateTime?(dateTime2.ToLocalTime());
				}
				return File.defaultLocalFileTime.Value;
			}
		}

		// Token: 0x06001DAF RID: 7599 RVA: 0x0006DD74 File Offset: 0x0006BF74
		[MonoLimitation("File encryption isn't supported (even on NTFS).")]
		public static void Encrypt(string path)
		{
			throw new NotSupportedException(Locale.GetText("File encryption isn't supported on any file system."));
		}

		// Token: 0x06001DB0 RID: 7600 RVA: 0x0006DD88 File Offset: 0x0006BF88
		[MonoLimitation("File encryption isn't supported (even on NTFS).")]
		public static void Decrypt(string path)
		{
			throw new NotSupportedException(Locale.GetText("File encryption isn't supported on any file system."));
		}

		// Token: 0x04000B03 RID: 2819
		private static DateTime? defaultLocalFileTime;
	}
}
