﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x02000247 RID: 583
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with .NET")]
	[Serializable]
	public class MemoryStream : Stream
	{
		// Token: 0x06001E4A RID: 7754 RVA: 0x00070094 File Offset: 0x0006E294
		public MemoryStream() : this(0)
		{
		}

		// Token: 0x06001E4B RID: 7755 RVA: 0x000700A0 File Offset: 0x0006E2A0
		public MemoryStream(int capacity)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity");
			}
			this.canWrite = true;
			this.capacity = capacity;
			this.internalBuffer = new byte[capacity];
			this.expandable = true;
			this.allowGetBuffer = true;
		}

		// Token: 0x06001E4C RID: 7756 RVA: 0x000700F0 File Offset: 0x0006E2F0
		public MemoryStream(byte[] buffer)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			this.InternalConstructor(buffer, 0, buffer.Length, true, false);
		}

		// Token: 0x06001E4D RID: 7757 RVA: 0x00070124 File Offset: 0x0006E324
		public MemoryStream(byte[] buffer, bool writable)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			this.InternalConstructor(buffer, 0, buffer.Length, writable, false);
		}

		// Token: 0x06001E4E RID: 7758 RVA: 0x00070158 File Offset: 0x0006E358
		public MemoryStream(byte[] buffer, int index, int count)
		{
			this.InternalConstructor(buffer, index, count, true, false);
		}

		// Token: 0x06001E4F RID: 7759 RVA: 0x00070178 File Offset: 0x0006E378
		public MemoryStream(byte[] buffer, int index, int count, bool writable)
		{
			this.InternalConstructor(buffer, index, count, writable, false);
		}

		// Token: 0x06001E50 RID: 7760 RVA: 0x00070198 File Offset: 0x0006E398
		public MemoryStream(byte[] buffer, int index, int count, bool writable, bool publiclyVisible)
		{
			this.InternalConstructor(buffer, index, count, writable, publiclyVisible);
		}

		// Token: 0x06001E51 RID: 7761 RVA: 0x000701B8 File Offset: 0x0006E3B8
		private void InternalConstructor(byte[] buffer, int index, int count, bool writable, bool publicallyVisible)
		{
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (index < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException("index or count is less than 0.");
			}
			if (buffer.Length - index < count)
			{
				throw new ArgumentException("index+count", "The size of the buffer is less than index + count.");
			}
			this.canWrite = writable;
			this.internalBuffer = buffer;
			this.capacity = count + index;
			this.length = this.capacity;
			this.position = index;
			this.initialIndex = index;
			this.allowGetBuffer = publicallyVisible;
			this.expandable = false;
		}

		// Token: 0x06001E52 RID: 7762 RVA: 0x0007024C File Offset: 0x0006E44C
		private void CheckIfClosedThrowDisposed()
		{
			if (this.streamClosed)
			{
				throw new ObjectDisposedException("MemoryStream");
			}
		}

		// Token: 0x17000517 RID: 1303
		// (get) Token: 0x06001E53 RID: 7763 RVA: 0x00070264 File Offset: 0x0006E464
		public override bool CanRead
		{
			get
			{
				return !this.streamClosed;
			}
		}

		// Token: 0x17000518 RID: 1304
		// (get) Token: 0x06001E54 RID: 7764 RVA: 0x00070270 File Offset: 0x0006E470
		public override bool CanSeek
		{
			get
			{
				return !this.streamClosed;
			}
		}

		// Token: 0x17000519 RID: 1305
		// (get) Token: 0x06001E55 RID: 7765 RVA: 0x0007027C File Offset: 0x0006E47C
		public override bool CanWrite
		{
			get
			{
				return !this.streamClosed && this.canWrite;
			}
		}

		// Token: 0x1700051A RID: 1306
		// (get) Token: 0x06001E56 RID: 7766 RVA: 0x00070294 File Offset: 0x0006E494
		// (set) Token: 0x06001E57 RID: 7767 RVA: 0x000702AC File Offset: 0x0006E4AC
		public virtual int Capacity
		{
			get
			{
				this.CheckIfClosedThrowDisposed();
				return this.capacity - this.initialIndex;
			}
			set
			{
				this.CheckIfClosedThrowDisposed();
				if (value == this.capacity)
				{
					return;
				}
				if (!this.expandable)
				{
					throw new NotSupportedException("Cannot expand this MemoryStream");
				}
				if (value < 0 || value < this.length)
				{
					throw new ArgumentOutOfRangeException("value", string.Concat(new object[]
					{
						"New capacity cannot be negative or less than the current capacity ",
						value,
						" ",
						this.capacity
					}));
				}
				byte[] dst = null;
				if (value != 0)
				{
					dst = new byte[value];
					Buffer.BlockCopy(this.internalBuffer, 0, dst, 0, this.length);
				}
				this.dirty_bytes = 0;
				this.internalBuffer = dst;
				this.capacity = value;
			}
		}

		// Token: 0x1700051B RID: 1307
		// (get) Token: 0x06001E58 RID: 7768 RVA: 0x0007036C File Offset: 0x0006E56C
		public override long Length
		{
			get
			{
				this.CheckIfClosedThrowDisposed();
				return (long)(this.length - this.initialIndex);
			}
		}

		// Token: 0x1700051C RID: 1308
		// (get) Token: 0x06001E59 RID: 7769 RVA: 0x00070384 File Offset: 0x0006E584
		// (set) Token: 0x06001E5A RID: 7770 RVA: 0x0007039C File Offset: 0x0006E59C
		public override long Position
		{
			get
			{
				this.CheckIfClosedThrowDisposed();
				return (long)(this.position - this.initialIndex);
			}
			set
			{
				this.CheckIfClosedThrowDisposed();
				if (value < 0L)
				{
					throw new ArgumentOutOfRangeException("value", "Position cannot be negative");
				}
				if (value > 2147483647L)
				{
					throw new ArgumentOutOfRangeException("value", "Position must be non-negative and less than 2^31 - 1 - origin");
				}
				this.position = this.initialIndex + (int)value;
			}
		}

		// Token: 0x06001E5B RID: 7771 RVA: 0x000703F4 File Offset: 0x0006E5F4
		protected override void Dispose(bool disposing)
		{
			this.streamClosed = true;
			this.expandable = false;
		}

		// Token: 0x06001E5C RID: 7772 RVA: 0x00070404 File Offset: 0x0006E604
		public override void Flush()
		{
		}

		// Token: 0x06001E5D RID: 7773 RVA: 0x00070408 File Offset: 0x0006E608
		public virtual byte[] GetBuffer()
		{
			if (!this.allowGetBuffer)
			{
				throw new UnauthorizedAccessException();
			}
			return this.internalBuffer;
		}

		// Token: 0x06001E5E RID: 7774 RVA: 0x00070424 File Offset: 0x0006E624
		public override int Read([In] [Out] byte[] buffer, int offset, int count)
		{
			this.CheckIfClosedThrowDisposed();
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException("offset or count less than zero.");
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException("offset+count", "The size of the buffer is less than offset + count.");
			}
			if (this.position >= this.length || count == 0)
			{
				return 0;
			}
			if (this.position > this.length - count)
			{
				count = this.length - this.position;
			}
			Buffer.BlockCopy(this.internalBuffer, this.position, buffer, offset, count);
			this.position += count;
			return count;
		}

		// Token: 0x06001E5F RID: 7775 RVA: 0x000704DC File Offset: 0x0006E6DC
		public override int ReadByte()
		{
			this.CheckIfClosedThrowDisposed();
			if (this.position >= this.length)
			{
				return -1;
			}
			return (int)this.internalBuffer[this.position++];
		}

		// Token: 0x06001E60 RID: 7776 RVA: 0x0007051C File Offset: 0x0006E71C
		public override long Seek(long offset, SeekOrigin loc)
		{
			this.CheckIfClosedThrowDisposed();
			if (offset > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("Offset out of range. " + offset);
			}
			int num;
			switch (loc)
			{
			case SeekOrigin.Begin:
				if (offset < 0L)
				{
					throw new IOException("Attempted to seek before start of MemoryStream.");
				}
				num = this.initialIndex;
				break;
			case SeekOrigin.Current:
				num = this.position;
				break;
			case SeekOrigin.End:
				num = this.length;
				break;
			default:
				throw new ArgumentException("loc", "Invalid SeekOrigin");
			}
			num += (int)offset;
			if (num < this.initialIndex)
			{
				throw new IOException("Attempted to seek before start of MemoryStream.");
			}
			this.position = num;
			return (long)this.position;
		}

		// Token: 0x06001E61 RID: 7777 RVA: 0x000705DC File Offset: 0x0006E7DC
		private int CalculateNewCapacity(int minimum)
		{
			if (minimum < 256)
			{
				minimum = 256;
			}
			if (minimum < this.capacity * 2)
			{
				minimum = this.capacity * 2;
			}
			return minimum;
		}

		// Token: 0x06001E62 RID: 7778 RVA: 0x0007060C File Offset: 0x0006E80C
		private void Expand(int newSize)
		{
			if (newSize > this.capacity)
			{
				this.Capacity = this.CalculateNewCapacity(newSize);
			}
			else if (this.dirty_bytes > 0)
			{
				Array.Clear(this.internalBuffer, this.length, this.dirty_bytes);
				this.dirty_bytes = 0;
			}
		}

		// Token: 0x06001E63 RID: 7779 RVA: 0x00070664 File Offset: 0x0006E864
		public override void SetLength(long value)
		{
			if (!this.expandable && value > (long)this.capacity)
			{
				throw new NotSupportedException("Expanding this MemoryStream is not supported");
			}
			this.CheckIfClosedThrowDisposed();
			if (!this.canWrite)
			{
				throw new NotSupportedException(Locale.GetText("Cannot write to this MemoryStream"));
			}
			if (value < 0L || value + (long)this.initialIndex > 2147483647L)
			{
				throw new ArgumentOutOfRangeException();
			}
			int num = (int)value + this.initialIndex;
			if (num > this.length)
			{
				this.Expand(num);
			}
			else if (num < this.length)
			{
				this.dirty_bytes += this.length - num;
			}
			this.length = num;
			if (this.position > this.length)
			{
				this.position = this.length;
			}
		}

		// Token: 0x06001E64 RID: 7780 RVA: 0x00070740 File Offset: 0x0006E940
		public virtual byte[] ToArray()
		{
			int num = this.length - this.initialIndex;
			byte[] array = new byte[num];
			if (this.internalBuffer != null)
			{
				Buffer.BlockCopy(this.internalBuffer, this.initialIndex, array, 0, num);
			}
			return array;
		}

		// Token: 0x06001E65 RID: 7781 RVA: 0x00070784 File Offset: 0x0006E984
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.CheckIfClosedThrowDisposed();
			if (!this.canWrite)
			{
				throw new NotSupportedException("Cannot write to this stream.");
			}
			if (buffer == null)
			{
				throw new ArgumentNullException("buffer");
			}
			if (offset < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (buffer.Length - offset < count)
			{
				throw new ArgumentException("offset+count", "The size of the buffer is less than offset + count.");
			}
			if (this.position > this.length - count)
			{
				this.Expand(this.position + count);
			}
			Buffer.BlockCopy(buffer, offset, this.internalBuffer, this.position, count);
			this.position += count;
			if (this.position >= this.length)
			{
				this.length = this.position;
			}
		}

		// Token: 0x06001E66 RID: 7782 RVA: 0x00070850 File Offset: 0x0006EA50
		public override void WriteByte(byte value)
		{
			this.CheckIfClosedThrowDisposed();
			if (!this.canWrite)
			{
				throw new NotSupportedException("Cannot write to this stream.");
			}
			if (this.position >= this.length)
			{
				this.Expand(this.position + 1);
				this.length = this.position + 1;
			}
			this.internalBuffer[this.position++] = value;
		}

		// Token: 0x06001E67 RID: 7783 RVA: 0x000708C0 File Offset: 0x0006EAC0
		public virtual void WriteTo(Stream stream)
		{
			this.CheckIfClosedThrowDisposed();
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			stream.Write(this.internalBuffer, this.initialIndex, this.length - this.initialIndex);
		}

		// Token: 0x04000B56 RID: 2902
		private bool canWrite;

		// Token: 0x04000B57 RID: 2903
		private bool allowGetBuffer;

		// Token: 0x04000B58 RID: 2904
		private int capacity;

		// Token: 0x04000B59 RID: 2905
		private int length;

		// Token: 0x04000B5A RID: 2906
		private byte[] internalBuffer;

		// Token: 0x04000B5B RID: 2907
		private int initialIndex;

		// Token: 0x04000B5C RID: 2908
		private bool expandable;

		// Token: 0x04000B5D RID: 2909
		private bool streamClosed;

		// Token: 0x04000B5E RID: 2910
		private int position;

		// Token: 0x04000B5F RID: 2911
		private int dirty_bytes;
	}
}
