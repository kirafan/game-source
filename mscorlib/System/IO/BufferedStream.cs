﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x02000231 RID: 561
	[ComVisible(true)]
	public sealed class BufferedStream : Stream
	{
		// Token: 0x06001D03 RID: 7427 RVA: 0x0006B2B0 File Offset: 0x000694B0
		public BufferedStream(Stream stream) : this(stream, 4096)
		{
		}

		// Token: 0x06001D04 RID: 7428 RVA: 0x0006B2C0 File Offset: 0x000694C0
		public BufferedStream(Stream stream, int bufferSize)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize", "<= 0");
			}
			if (!stream.CanRead && !stream.CanWrite)
			{
				throw new ObjectDisposedException(Locale.GetText("Cannot access a closed Stream."));
			}
			this.m_stream = stream;
			this.m_buffer = new byte[bufferSize];
		}

		// Token: 0x170004DE RID: 1246
		// (get) Token: 0x06001D05 RID: 7429 RVA: 0x0006B334 File Offset: 0x00069534
		public override bool CanRead
		{
			get
			{
				return this.m_stream.CanRead;
			}
		}

		// Token: 0x170004DF RID: 1247
		// (get) Token: 0x06001D06 RID: 7430 RVA: 0x0006B344 File Offset: 0x00069544
		public override bool CanWrite
		{
			get
			{
				return this.m_stream.CanWrite;
			}
		}

		// Token: 0x170004E0 RID: 1248
		// (get) Token: 0x06001D07 RID: 7431 RVA: 0x0006B354 File Offset: 0x00069554
		public override bool CanSeek
		{
			get
			{
				return this.m_stream.CanSeek;
			}
		}

		// Token: 0x170004E1 RID: 1249
		// (get) Token: 0x06001D08 RID: 7432 RVA: 0x0006B364 File Offset: 0x00069564
		public override long Length
		{
			get
			{
				this.Flush();
				return this.m_stream.Length;
			}
		}

		// Token: 0x170004E2 RID: 1250
		// (get) Token: 0x06001D09 RID: 7433 RVA: 0x0006B378 File Offset: 0x00069578
		// (set) Token: 0x06001D0A RID: 7434 RVA: 0x0006B39C File Offset: 0x0006959C
		public override long Position
		{
			get
			{
				this.CheckObjectDisposedException();
				return this.m_stream.Position - (long)this.m_buffer_read_ahead + (long)this.m_buffer_pos;
			}
			set
			{
				if (value < this.Position && this.Position - value <= (long)this.m_buffer_pos && this.m_buffer_reading)
				{
					this.m_buffer_pos -= (int)(this.Position - value);
				}
				else if (value > this.Position && value - this.Position < (long)(this.m_buffer_read_ahead - this.m_buffer_pos) && this.m_buffer_reading)
				{
					this.m_buffer_pos += (int)(value - this.Position);
				}
				else
				{
					this.Flush();
					this.m_stream.Position = value;
				}
			}
		}

		// Token: 0x06001D0B RID: 7435 RVA: 0x0006B450 File Offset: 0x00069650
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (this.m_buffer != null)
			{
				this.Flush();
			}
			this.m_stream.Close();
			this.m_buffer = null;
			this.disposed = true;
		}

		// Token: 0x06001D0C RID: 7436 RVA: 0x0006B494 File Offset: 0x00069694
		public override void Flush()
		{
			this.CheckObjectDisposedException();
			if (this.m_buffer_reading)
			{
				if (this.CanSeek)
				{
					this.m_stream.Position = this.Position;
				}
			}
			else if (this.m_buffer_pos > 0)
			{
				this.m_stream.Write(this.m_buffer, 0, this.m_buffer_pos);
			}
			this.m_buffer_read_ahead = 0;
			this.m_buffer_pos = 0;
		}

		// Token: 0x06001D0D RID: 7437 RVA: 0x0006B508 File Offset: 0x00069708
		public override long Seek(long offset, SeekOrigin origin)
		{
			this.CheckObjectDisposedException();
			if (!this.CanSeek)
			{
				throw new NotSupportedException(Locale.GetText("Non seekable stream."));
			}
			this.Flush();
			return this.m_stream.Seek(offset, origin);
		}

		// Token: 0x06001D0E RID: 7438 RVA: 0x0006B54C File Offset: 0x0006974C
		public override void SetLength(long value)
		{
			this.CheckObjectDisposedException();
			if (value < 0L)
			{
				throw new ArgumentOutOfRangeException("value must be positive");
			}
			if (!this.m_stream.CanWrite && !this.m_stream.CanSeek)
			{
				throw new NotSupportedException("the stream cannot seek nor write.");
			}
			if (this.m_stream == null || (!this.m_stream.CanRead && !this.m_stream.CanWrite))
			{
				throw new IOException("the stream is not open");
			}
			this.m_stream.SetLength(value);
			if (this.Position > value)
			{
				this.Position = value;
			}
		}

		// Token: 0x06001D0F RID: 7439 RVA: 0x0006B5F4 File Offset: 0x000697F4
		public override int ReadByte()
		{
			this.CheckObjectDisposedException();
			byte[] array = new byte[1];
			if (this.Read(array, 0, 1) == 1)
			{
				return (int)array[0];
			}
			return -1;
		}

		// Token: 0x06001D10 RID: 7440 RVA: 0x0006B624 File Offset: 0x00069824
		public override void WriteByte(byte value)
		{
			this.CheckObjectDisposedException();
			this.Write(new byte[]
			{
				value
			}, 0, 1);
		}

		// Token: 0x06001D11 RID: 7441 RVA: 0x0006B64C File Offset: 0x0006984C
		public override int Read([In] [Out] byte[] array, int offset, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			this.CheckObjectDisposedException();
			if (!this.m_stream.CanRead)
			{
				throw new NotSupportedException(Locale.GetText("Cannot read from stream"));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			if (array.Length - offset < count)
			{
				throw new ArgumentException("array.Length - offset < count");
			}
			if (!this.m_buffer_reading)
			{
				this.Flush();
				this.m_buffer_reading = true;
			}
			if (count <= this.m_buffer_read_ahead - this.m_buffer_pos)
			{
				Buffer.BlockCopyInternal(this.m_buffer, this.m_buffer_pos, array, offset, count);
				this.m_buffer_pos += count;
				if (this.m_buffer_pos == this.m_buffer_read_ahead)
				{
					this.m_buffer_pos = 0;
					this.m_buffer_read_ahead = 0;
				}
				return count;
			}
			int num = this.m_buffer_read_ahead - this.m_buffer_pos;
			Buffer.BlockCopyInternal(this.m_buffer, this.m_buffer_pos, array, offset, num);
			this.m_buffer_pos = 0;
			this.m_buffer_read_ahead = 0;
			offset += num;
			count -= num;
			if (count >= this.m_buffer.Length)
			{
				num += this.m_stream.Read(array, offset, count);
			}
			else
			{
				this.m_buffer_read_ahead = this.m_stream.Read(this.m_buffer, 0, this.m_buffer.Length);
				if (count < this.m_buffer_read_ahead)
				{
					Buffer.BlockCopyInternal(this.m_buffer, 0, array, offset, count);
					this.m_buffer_pos = count;
					num += count;
				}
				else
				{
					Buffer.BlockCopyInternal(this.m_buffer, 0, array, offset, this.m_buffer_read_ahead);
					num += this.m_buffer_read_ahead;
					this.m_buffer_read_ahead = 0;
				}
			}
			return num;
		}

		// Token: 0x06001D12 RID: 7442 RVA: 0x0006B814 File Offset: 0x00069A14
		public override void Write(byte[] array, int offset, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			this.CheckObjectDisposedException();
			if (!this.m_stream.CanWrite)
			{
				throw new NotSupportedException(Locale.GetText("Cannot write to stream"));
			}
			if (offset < 0)
			{
				throw new ArgumentOutOfRangeException("offset", "< 0");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", "< 0");
			}
			if (array.Length - offset < count)
			{
				throw new ArgumentException("array.Length - offset < count");
			}
			if (this.m_buffer_reading)
			{
				this.Flush();
				this.m_buffer_reading = false;
			}
			if (this.m_buffer_pos >= this.m_buffer.Length - count)
			{
				this.Flush();
				this.m_stream.Write(array, offset, count);
			}
			else
			{
				Buffer.BlockCopyInternal(array, offset, this.m_buffer, this.m_buffer_pos, count);
				this.m_buffer_pos += count;
			}
		}

		// Token: 0x06001D13 RID: 7443 RVA: 0x0006B908 File Offset: 0x00069B08
		private void CheckObjectDisposedException()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("BufferedStream", Locale.GetText("Stream is closed"));
			}
		}

		// Token: 0x04000AEB RID: 2795
		private Stream m_stream;

		// Token: 0x04000AEC RID: 2796
		private byte[] m_buffer;

		// Token: 0x04000AED RID: 2797
		private int m_buffer_pos;

		// Token: 0x04000AEE RID: 2798
		private int m_buffer_read_ahead;

		// Token: 0x04000AEF RID: 2799
		private bool m_buffer_reading;

		// Token: 0x04000AF0 RID: 2800
		private bool disposed;
	}
}
