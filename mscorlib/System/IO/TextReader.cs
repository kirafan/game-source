﻿using System;
using System.Runtime.InteropServices;

namespace System.IO
{
	// Token: 0x0200025B RID: 603
	[ComVisible(true)]
	[Serializable]
	public abstract class TextReader : MarshalByRefObject, IDisposable
	{
		// Token: 0x06001F46 RID: 8006 RVA: 0x00073CDC File Offset: 0x00071EDC
		public virtual void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x06001F47 RID: 8007 RVA: 0x00073CE8 File Offset: 0x00071EE8
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x06001F48 RID: 8008 RVA: 0x00073CF4 File Offset: 0x00071EF4
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				GC.SuppressFinalize(this);
			}
		}

		// Token: 0x06001F49 RID: 8009 RVA: 0x00073D04 File Offset: 0x00071F04
		public virtual int Peek()
		{
			return -1;
		}

		// Token: 0x06001F4A RID: 8010 RVA: 0x00073D08 File Offset: 0x00071F08
		public virtual int Read()
		{
			return -1;
		}

		// Token: 0x06001F4B RID: 8011 RVA: 0x00073D0C File Offset: 0x00071F0C
		public virtual int Read([In] [Out] char[] buffer, int index, int count)
		{
			int i;
			for (i = 0; i < count; i++)
			{
				int num;
				if ((num = this.Read()) == -1)
				{
					return i;
				}
				buffer[index + i] = (char)num;
			}
			return i;
		}

		// Token: 0x06001F4C RID: 8012 RVA: 0x00073D44 File Offset: 0x00071F44
		public virtual int ReadBlock([In] [Out] char[] buffer, int index, int count)
		{
			int num = 0;
			int num2;
			do
			{
				num2 = this.Read(buffer, index, count);
				index += num2;
				num += num2;
				count -= num2;
			}
			while (num2 != 0 && count > 0);
			return num;
		}

		// Token: 0x06001F4D RID: 8013 RVA: 0x00073D7C File Offset: 0x00071F7C
		public virtual string ReadLine()
		{
			return string.Empty;
		}

		// Token: 0x06001F4E RID: 8014 RVA: 0x00073D84 File Offset: 0x00071F84
		public virtual string ReadToEnd()
		{
			return string.Empty;
		}

		// Token: 0x06001F4F RID: 8015 RVA: 0x00073D8C File Offset: 0x00071F8C
		public static TextReader Synchronized(TextReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader is null");
			}
			if (reader is SynchronizedReader)
			{
				return reader;
			}
			return new SynchronizedReader(reader);
		}

		// Token: 0x04000BCC RID: 3020
		public static readonly TextReader Null = new TextReader.NullTextReader();

		// Token: 0x0200025C RID: 604
		private class NullTextReader : TextReader
		{
			// Token: 0x06001F51 RID: 8017 RVA: 0x00073DC8 File Offset: 0x00071FC8
			public override string ReadLine()
			{
				return null;
			}
		}
	}
}
