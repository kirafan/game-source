﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace System.IO
{
	// Token: 0x0200023E RID: 574
	[ComVisible(true)]
	[Serializable]
	public class FileLoadException : IOException
	{
		// Token: 0x06001DD0 RID: 7632 RVA: 0x0006E238 File Offset: 0x0006C438
		public FileLoadException() : base(Locale.GetText("I/O Error"))
		{
			base.HResult = -2147024894;
			this.msg = Locale.GetText("I/O Error");
		}

		// Token: 0x06001DD1 RID: 7633 RVA: 0x0006E270 File Offset: 0x0006C470
		public FileLoadException(string message) : base(message)
		{
			base.HResult = -2147024894;
			this.msg = message;
		}

		// Token: 0x06001DD2 RID: 7634 RVA: 0x0006E28C File Offset: 0x0006C48C
		public FileLoadException(string message, string fileName) : base(message)
		{
			base.HResult = -2147024894;
			this.msg = message;
			this.fileName = fileName;
		}

		// Token: 0x06001DD3 RID: 7635 RVA: 0x0006E2BC File Offset: 0x0006C4BC
		public FileLoadException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2147024894;
			this.msg = message;
		}

		// Token: 0x06001DD4 RID: 7636 RVA: 0x0006E2D8 File Offset: 0x0006C4D8
		public FileLoadException(string message, string fileName, Exception inner) : base(message, inner)
		{
			base.HResult = -2147024894;
			this.msg = message;
			this.fileName = fileName;
		}

		// Token: 0x06001DD5 RID: 7637 RVA: 0x0006E2FC File Offset: 0x0006C4FC
		protected FileLoadException(SerializationInfo info, StreamingContext context)
		{
			this.fileName = info.GetString("FileLoad_FileName");
			this.fusionLog = info.GetString("FileLoad_FusionLog");
		}

		// Token: 0x170004F7 RID: 1271
		// (get) Token: 0x06001DD6 RID: 7638 RVA: 0x0006E334 File Offset: 0x0006C534
		public override string Message
		{
			get
			{
				return this.msg;
			}
		}

		// Token: 0x170004F8 RID: 1272
		// (get) Token: 0x06001DD7 RID: 7639 RVA: 0x0006E33C File Offset: 0x0006C53C
		public string FileName
		{
			get
			{
				return this.fileName;
			}
		}

		// Token: 0x170004F9 RID: 1273
		// (get) Token: 0x06001DD8 RID: 7640 RVA: 0x0006E344 File Offset: 0x0006C544
		public string FusionLog
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this.fusionLog;
			}
		}

		// Token: 0x06001DD9 RID: 7641 RVA: 0x0006E34C File Offset: 0x0006C54C
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("FileLoad_FileName", this.fileName);
			info.AddValue("FileLoad_FusionLog", this.fusionLog);
		}

		// Token: 0x06001DDA RID: 7642 RVA: 0x0006E384 File Offset: 0x0006C584
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(this.GetType().FullName);
			stringBuilder.AppendFormat(": {0}", this.msg);
			if (this.fileName != null)
			{
				stringBuilder.AppendFormat(" : {0}", this.fileName);
			}
			if (this.InnerException != null)
			{
				stringBuilder.AppendFormat(" ----> {0}", this.InnerException);
			}
			if (this.StackTrace != null)
			{
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(this.StackTrace);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04000B18 RID: 2840
		private const int Result = -2147024894;

		// Token: 0x04000B19 RID: 2841
		private string msg;

		// Token: 0x04000B1A RID: 2842
		private string fileName;

		// Token: 0x04000B1B RID: 2843
		private string fusionLog;
	}
}
