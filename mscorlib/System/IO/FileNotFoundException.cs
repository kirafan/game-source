﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace System.IO
{
	// Token: 0x02000240 RID: 576
	[ComVisible(true)]
	[Serializable]
	public class FileNotFoundException : IOException
	{
		// Token: 0x06001DDB RID: 7643 RVA: 0x0006E418 File Offset: 0x0006C618
		public FileNotFoundException() : base(Locale.GetText("Unable to find the specified file."))
		{
			base.HResult = -2146232799;
		}

		// Token: 0x06001DDC RID: 7644 RVA: 0x0006E438 File Offset: 0x0006C638
		public FileNotFoundException(string message) : base(message)
		{
			base.HResult = -2146232799;
		}

		// Token: 0x06001DDD RID: 7645 RVA: 0x0006E44C File Offset: 0x0006C64C
		public FileNotFoundException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146232799;
		}

		// Token: 0x06001DDE RID: 7646 RVA: 0x0006E464 File Offset: 0x0006C664
		public FileNotFoundException(string message, string fileName) : base(message)
		{
			base.HResult = -2146232799;
			this.fileName = fileName;
		}

		// Token: 0x06001DDF RID: 7647 RVA: 0x0006E480 File Offset: 0x0006C680
		public FileNotFoundException(string message, string fileName, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146232799;
			this.fileName = fileName;
		}

		// Token: 0x06001DE0 RID: 7648 RVA: 0x0006E49C File Offset: 0x0006C69C
		protected FileNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.fileName = info.GetString("FileNotFound_FileName");
			this.fusionLog = info.GetString("FileNotFound_FusionLog");
		}

		// Token: 0x170004FA RID: 1274
		// (get) Token: 0x06001DE1 RID: 7649 RVA: 0x0006E4D4 File Offset: 0x0006C6D4
		public string FileName
		{
			get
			{
				return this.fileName;
			}
		}

		// Token: 0x170004FB RID: 1275
		// (get) Token: 0x06001DE2 RID: 7650 RVA: 0x0006E4DC File Offset: 0x0006C6DC
		public string FusionLog
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this.fusionLog;
			}
		}

		// Token: 0x170004FC RID: 1276
		// (get) Token: 0x06001DE3 RID: 7651 RVA: 0x0006E4E4 File Offset: 0x0006C6E4
		public override string Message
		{
			get
			{
				if (this.message == null && this.fileName != null)
				{
					return string.Format(CultureInfo.CurrentCulture, "Could not load file or assembly '{0}' or one of its dependencies. The system cannot find the file specified.", new object[]
					{
						this.fileName
					});
				}
				return this.message;
			}
		}

		// Token: 0x06001DE4 RID: 7652 RVA: 0x0006E530 File Offset: 0x0006C730
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("FileNotFound_FileName", this.fileName);
			info.AddValue("FileNotFound_FusionLog", this.fusionLog);
		}

		// Token: 0x06001DE5 RID: 7653 RVA: 0x0006E568 File Offset: 0x0006C768
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(this.GetType().FullName);
			stringBuilder.AppendFormat(": {0}", this.Message);
			if (this.fileName != null && this.fileName.Length > 0)
			{
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.AppendFormat("File name: '{0}'", this.fileName);
			}
			if (this.InnerException != null)
			{
				stringBuilder.AppendFormat(" ---> {0}", this.InnerException);
			}
			if (this.StackTrace != null)
			{
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(this.StackTrace);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04000B23 RID: 2851
		private const int Result = -2146232799;

		// Token: 0x04000B24 RID: 2852
		private string fileName;

		// Token: 0x04000B25 RID: 2853
		private string fusionLog;
	}
}
