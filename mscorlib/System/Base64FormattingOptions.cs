﻿using System;

namespace System
{
	// Token: 0x02000108 RID: 264
	[Flags]
	public enum Base64FormattingOptions
	{
		// Token: 0x040003BB RID: 955
		InsertLineBreaks = 1,
		// Token: 0x040003BC RID: 956
		None = 0
	}
}
