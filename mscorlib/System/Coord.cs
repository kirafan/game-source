﻿using System;

namespace System
{
	// Token: 0x0200019C RID: 412
	internal struct Coord
	{
		// Token: 0x060014A2 RID: 5282 RVA: 0x000532F4 File Offset: 0x000514F4
		public Coord(int x, int y)
		{
			this.X = (short)x;
			this.Y = (short)y;
		}

		// Token: 0x04000837 RID: 2103
		public short X;

		// Token: 0x04000838 RID: 2104
		public short Y;
	}
}
