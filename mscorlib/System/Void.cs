﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000032 RID: 50
	[ComVisible(true)]
	[Serializable]
	public struct Void
	{
	}
}
