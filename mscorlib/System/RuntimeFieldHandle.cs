﻿using System;
using System.Reflection;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200003B RID: 59
	[ComVisible(true)]
	[MonoTODO("Serialization needs tests")]
	[Serializable]
	public struct RuntimeFieldHandle : ISerializable
	{
		// Token: 0x06000639 RID: 1593 RVA: 0x00014648 File Offset: 0x00012848
		internal RuntimeFieldHandle(IntPtr v)
		{
			this.value = v;
		}

		// Token: 0x0600063A RID: 1594 RVA: 0x00014654 File Offset: 0x00012854
		private RuntimeFieldHandle(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			MonoField monoField = (MonoField)info.GetValue("FieldObj", typeof(MonoField));
			this.value = monoField.FieldHandle.Value;
			if (this.value == IntPtr.Zero)
			{
				throw new SerializationException(Locale.GetText("Insufficient state."));
			}
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x0600063B RID: 1595 RVA: 0x000146C8 File Offset: 0x000128C8
		public IntPtr Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x0600063C RID: 1596 RVA: 0x000146D0 File Offset: 0x000128D0
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			if (this.value == IntPtr.Zero)
			{
				throw new SerializationException("Object fields may not be properly initialized");
			}
			info.AddValue("FieldObj", (MonoField)FieldInfo.GetFieldFromHandle(this), typeof(MonoField));
		}

		// Token: 0x0600063D RID: 1597 RVA: 0x00014734 File Offset: 0x00012934
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public override bool Equals(object obj)
		{
			return obj != null && base.GetType() == obj.GetType() && this.value == ((RuntimeFieldHandle)obj).Value;
		}

		// Token: 0x0600063E RID: 1598 RVA: 0x00014780 File Offset: 0x00012980
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public bool Equals(RuntimeFieldHandle handle)
		{
			return this.value == handle.Value;
		}

		// Token: 0x0600063F RID: 1599 RVA: 0x00014794 File Offset: 0x00012994
		public override int GetHashCode()
		{
			return this.value.GetHashCode();
		}

		// Token: 0x06000640 RID: 1600 RVA: 0x000147A4 File Offset: 0x000129A4
		public static bool operator ==(RuntimeFieldHandle left, RuntimeFieldHandle right)
		{
			return left.Equals(right);
		}

		// Token: 0x06000641 RID: 1601 RVA: 0x000147B0 File Offset: 0x000129B0
		public static bool operator !=(RuntimeFieldHandle left, RuntimeFieldHandle right)
		{
			return !left.Equals(right);
		}

		// Token: 0x04000081 RID: 129
		private IntPtr value;
	}
}
