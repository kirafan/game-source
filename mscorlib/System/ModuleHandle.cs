﻿using System;
using System.Reflection;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000170 RID: 368
	[ComVisible(true)]
	public struct ModuleHandle
	{
		// Token: 0x06001372 RID: 4978 RVA: 0x0004DB0C File Offset: 0x0004BD0C
		internal ModuleHandle(IntPtr v)
		{
			this.value = v;
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06001374 RID: 4980 RVA: 0x0004DB2C File Offset: 0x0004BD2C
		internal IntPtr Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06001375 RID: 4981 RVA: 0x0004DB34 File Offset: 0x0004BD34
		public int MDStreamVersion
		{
			get
			{
				if (this.value == IntPtr.Zero)
				{
					throw new ArgumentNullException(string.Empty, "Invalid handle");
				}
				return Module.GetMDStreamVersion(this.value);
			}
		}

		// Token: 0x06001376 RID: 4982 RVA: 0x0004DB74 File Offset: 0x0004BD74
		internal void GetPEKind(out PortableExecutableKinds peKind, out ImageFileMachine machine)
		{
			if (this.value == IntPtr.Zero)
			{
				throw new ArgumentNullException(string.Empty, "Invalid handle");
			}
			Module.GetPEKind(this.value, out peKind, out machine);
		}

		// Token: 0x06001377 RID: 4983 RVA: 0x0004DBB4 File Offset: 0x0004BDB4
		public RuntimeFieldHandle ResolveFieldHandle(int fieldToken)
		{
			return this.ResolveFieldHandle(fieldToken, null, null);
		}

		// Token: 0x06001378 RID: 4984 RVA: 0x0004DBC0 File Offset: 0x0004BDC0
		public RuntimeMethodHandle ResolveMethodHandle(int methodToken)
		{
			return this.ResolveMethodHandle(methodToken, null, null);
		}

		// Token: 0x06001379 RID: 4985 RVA: 0x0004DBCC File Offset: 0x0004BDCC
		public RuntimeTypeHandle ResolveTypeHandle(int typeToken)
		{
			return this.ResolveTypeHandle(typeToken, null, null);
		}

		// Token: 0x0600137A RID: 4986 RVA: 0x0004DBD8 File Offset: 0x0004BDD8
		private IntPtr[] ptrs_from_handles(RuntimeTypeHandle[] handles)
		{
			if (handles == null)
			{
				return null;
			}
			IntPtr[] array = new IntPtr[handles.Length];
			for (int i = 0; i < handles.Length; i++)
			{
				array[i] = handles[i].Value;
			}
			return array;
		}

		// Token: 0x0600137B RID: 4987 RVA: 0x0004DC24 File Offset: 0x0004BE24
		public RuntimeTypeHandle ResolveTypeHandle(int typeToken, RuntimeTypeHandle[] typeInstantiationContext, RuntimeTypeHandle[] methodInstantiationContext)
		{
			if (this.value == IntPtr.Zero)
			{
				throw new ArgumentNullException(string.Empty, "Invalid handle");
			}
			ResolveTokenError resolveTokenError;
			IntPtr intPtr = Module.ResolveTypeToken(this.value, typeToken, this.ptrs_from_handles(typeInstantiationContext), this.ptrs_from_handles(methodInstantiationContext), out resolveTokenError);
			if (intPtr == IntPtr.Zero)
			{
				throw new TypeLoadException(string.Format("Could not load type '0x{0:x}' from assembly '0x{1:x}'", typeToken, this.value.ToInt64()));
			}
			return new RuntimeTypeHandle(intPtr);
		}

		// Token: 0x0600137C RID: 4988 RVA: 0x0004DCB0 File Offset: 0x0004BEB0
		public RuntimeMethodHandle ResolveMethodHandle(int methodToken, RuntimeTypeHandle[] typeInstantiationContext, RuntimeTypeHandle[] methodInstantiationContext)
		{
			if (this.value == IntPtr.Zero)
			{
				throw new ArgumentNullException(string.Empty, "Invalid handle");
			}
			ResolveTokenError resolveTokenError;
			IntPtr intPtr = Module.ResolveMethodToken(this.value, methodToken, this.ptrs_from_handles(typeInstantiationContext), this.ptrs_from_handles(methodInstantiationContext), out resolveTokenError);
			if (intPtr == IntPtr.Zero)
			{
				throw new Exception(string.Format("Could not load method '0x{0:x}' from assembly '0x{1:x}'", methodToken, this.value.ToInt64()));
			}
			return new RuntimeMethodHandle(intPtr);
		}

		// Token: 0x0600137D RID: 4989 RVA: 0x0004DD3C File Offset: 0x0004BF3C
		public RuntimeFieldHandle ResolveFieldHandle(int fieldToken, RuntimeTypeHandle[] typeInstantiationContext, RuntimeTypeHandle[] methodInstantiationContext)
		{
			if (this.value == IntPtr.Zero)
			{
				throw new ArgumentNullException(string.Empty, "Invalid handle");
			}
			ResolveTokenError resolveTokenError;
			IntPtr intPtr = Module.ResolveFieldToken(this.value, fieldToken, this.ptrs_from_handles(typeInstantiationContext), this.ptrs_from_handles(methodInstantiationContext), out resolveTokenError);
			if (intPtr == IntPtr.Zero)
			{
				throw new Exception(string.Format("Could not load field '0x{0:x}' from assembly '0x{1:x}'", fieldToken, this.value.ToInt64()));
			}
			return new RuntimeFieldHandle(intPtr);
		}

		// Token: 0x0600137E RID: 4990 RVA: 0x0004DDC8 File Offset: 0x0004BFC8
		public RuntimeFieldHandle GetRuntimeFieldHandleFromMetadataToken(int fieldToken)
		{
			return this.ResolveFieldHandle(fieldToken);
		}

		// Token: 0x0600137F RID: 4991 RVA: 0x0004DDD4 File Offset: 0x0004BFD4
		public RuntimeMethodHandle GetRuntimeMethodHandleFromMetadataToken(int methodToken)
		{
			return this.ResolveMethodHandle(methodToken);
		}

		// Token: 0x06001380 RID: 4992 RVA: 0x0004DDE0 File Offset: 0x0004BFE0
		public RuntimeTypeHandle GetRuntimeTypeHandleFromMetadataToken(int typeToken)
		{
			return this.ResolveTypeHandle(typeToken);
		}

		// Token: 0x06001381 RID: 4993 RVA: 0x0004DDEC File Offset: 0x0004BFEC
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public override bool Equals(object obj)
		{
			return obj != null && base.GetType() == obj.GetType() && this.value == ((ModuleHandle)obj).Value;
		}

		// Token: 0x06001382 RID: 4994 RVA: 0x0004DE38 File Offset: 0x0004C038
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public bool Equals(ModuleHandle handle)
		{
			return this.value == handle.Value;
		}

		// Token: 0x06001383 RID: 4995 RVA: 0x0004DE4C File Offset: 0x0004C04C
		public override int GetHashCode()
		{
			return this.value.GetHashCode();
		}

		// Token: 0x06001384 RID: 4996 RVA: 0x0004DE5C File Offset: 0x0004C05C
		public static bool operator ==(ModuleHandle left, ModuleHandle right)
		{
			return object.Equals(left, right);
		}

		// Token: 0x06001385 RID: 4997 RVA: 0x0004DE70 File Offset: 0x0004C070
		public static bool operator !=(ModuleHandle left, ModuleHandle right)
		{
			return !object.Equals(left, right);
		}

		// Token: 0x040005A7 RID: 1447
		private IntPtr value;

		// Token: 0x040005A8 RID: 1448
		public static readonly ModuleHandle EmptyHandle = new ModuleHandle(IntPtr.Zero);
	}
}
