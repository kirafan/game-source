﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000171 RID: 369
	[ComVisible(true)]
	[Serializable]
	public sealed class StackOverflowException : SystemException
	{
		// Token: 0x06001386 RID: 4998 RVA: 0x0004DE88 File Offset: 0x0004C088
		public StackOverflowException() : base(Locale.GetText("The requested operation caused a stack overflow."))
		{
		}

		// Token: 0x06001387 RID: 4999 RVA: 0x0004DE9C File Offset: 0x0004C09C
		public StackOverflowException(string message) : base(message)
		{
		}

		// Token: 0x06001388 RID: 5000 RVA: 0x0004DEA8 File Offset: 0x0004C0A8
		public StackOverflowException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06001389 RID: 5001 RVA: 0x0004DEB4 File Offset: 0x0004C0B4
		internal StackOverflowException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
