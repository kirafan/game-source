﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000186 RID: 390
	[ComVisible(true)]
	[Serializable]
	public class TimeoutException : SystemException
	{
		// Token: 0x06001450 RID: 5200 RVA: 0x00051F40 File Offset: 0x00050140
		public TimeoutException() : base(Locale.GetText("The operation has timed-out."))
		{
			base.HResult = -2146233083;
		}

		// Token: 0x06001451 RID: 5201 RVA: 0x00051F60 File Offset: 0x00050160
		public TimeoutException(string message) : base(message)
		{
			base.HResult = -2146233083;
		}

		// Token: 0x06001452 RID: 5202 RVA: 0x00051F74 File Offset: 0x00050174
		public TimeoutException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233083;
		}

		// Token: 0x06001453 RID: 5203 RVA: 0x00051F8C File Offset: 0x0005018C
		protected TimeoutException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040007E9 RID: 2025
		private const int Result = -2146233083;
	}
}
