﻿using System;
using System.Collections;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200011E RID: 286
	[Serializable]
	[StructLayout(LayoutKind.Auto)]
	public struct DateTime : IFormattable, IConvertible, IComparable, IComparable<DateTime>, IEquatable<DateTime>
	{
		// Token: 0x06000FE0 RID: 4064 RVA: 0x0003F998 File Offset: 0x0003DB98
		public DateTime(long ticks)
		{
			this.ticks = new TimeSpan(ticks);
			if (ticks < DateTime.MinValue.Ticks || ticks > DateTime.MaxValue.Ticks)
			{
				string text = Locale.GetText("Value {0} is outside the valid range [{1},{2}].", new object[]
				{
					ticks,
					DateTime.MinValue.Ticks,
					DateTime.MaxValue.Ticks
				});
				throw new ArgumentOutOfRangeException("ticks", text);
			}
			this.kind = DateTimeKind.Unspecified;
		}

		// Token: 0x06000FE1 RID: 4065 RVA: 0x0003FA30 File Offset: 0x0003DC30
		public DateTime(int year, int month, int day)
		{
			this = new DateTime(year, month, day, 0, 0, 0, 0);
		}

		// Token: 0x06000FE2 RID: 4066 RVA: 0x0003FA4C File Offset: 0x0003DC4C
		public DateTime(int year, int month, int day, int hour, int minute, int second)
		{
			this = new DateTime(year, month, day, hour, minute, second, 0);
		}

		// Token: 0x06000FE3 RID: 4067 RVA: 0x0003FA6C File Offset: 0x0003DC6C
		public DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond)
		{
			if (year < 1 || year > 9999 || month < 1 || month > 12 || day < 1 || day > DateTime.DaysInMonth(year, month) || hour < 0 || hour > 23 || minute < 0 || minute > 59 || second < 0 || second > 59 || millisecond < 0 || millisecond > 999)
			{
				throw new ArgumentOutOfRangeException("Parameters describe an unrepresentable DateTime.");
			}
			this.ticks = new TimeSpan(DateTime.AbsoluteDays(year, month, day), hour, minute, second, millisecond);
			this.kind = DateTimeKind.Unspecified;
		}

		// Token: 0x06000FE4 RID: 4068 RVA: 0x0003FB24 File Offset: 0x0003DD24
		public DateTime(int year, int month, int day, Calendar calendar)
		{
			this = new DateTime(year, month, day, 0, 0, 0, 0, calendar);
		}

		// Token: 0x06000FE5 RID: 4069 RVA: 0x0003FB40 File Offset: 0x0003DD40
		public DateTime(int year, int month, int day, int hour, int minute, int second, Calendar calendar)
		{
			this = new DateTime(year, month, day, hour, minute, second, 0, calendar);
		}

		// Token: 0x06000FE6 RID: 4070 RVA: 0x0003FB60 File Offset: 0x0003DD60
		public DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, Calendar calendar)
		{
			if (calendar == null)
			{
				throw new ArgumentNullException("calendar");
			}
			this.ticks = calendar.ToDateTime(year, month, day, hour, minute, second, millisecond).ticks;
			this.kind = DateTimeKind.Unspecified;
		}

		// Token: 0x06000FE7 RID: 4071 RVA: 0x0003FBA8 File Offset: 0x0003DDA8
		internal DateTime(bool check, TimeSpan value)
		{
			if (check && (value.Ticks < DateTime.MinValue.Ticks || value.Ticks > DateTime.MaxValue.Ticks))
			{
				throw new ArgumentOutOfRangeException();
			}
			this.ticks = value;
			this.kind = DateTimeKind.Unspecified;
		}

		// Token: 0x06000FE8 RID: 4072 RVA: 0x0003FC04 File Offset: 0x0003DE04
		public DateTime(long ticks, DateTimeKind kind)
		{
			this = new DateTime(ticks);
			this.CheckDateTimeKind(kind);
			this.kind = kind;
		}

		// Token: 0x06000FE9 RID: 4073 RVA: 0x0003FC1C File Offset: 0x0003DE1C
		public DateTime(int year, int month, int day, int hour, int minute, int second, DateTimeKind kind)
		{
			this = new DateTime(year, month, day, hour, minute, second);
			this.CheckDateTimeKind(kind);
			this.kind = kind;
		}

		// Token: 0x06000FEA RID: 4074 RVA: 0x0003FC40 File Offset: 0x0003DE40
		public DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, DateTimeKind kind)
		{
			this = new DateTime(year, month, day, hour, minute, second, millisecond);
			this.CheckDateTimeKind(kind);
			this.kind = kind;
		}

		// Token: 0x06000FEB RID: 4075 RVA: 0x0003FC70 File Offset: 0x0003DE70
		public DateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, Calendar calendar, DateTimeKind kind)
		{
			this = new DateTime(year, month, day, hour, minute, second, millisecond, calendar);
			this.CheckDateTimeKind(kind);
			this.kind = kind;
		}

		// Token: 0x06000FED RID: 4077 RVA: 0x0003FF80 File Offset: 0x0003E180
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FEE RID: 4078 RVA: 0x0003FF88 File Offset: 0x0003E188
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FEF RID: 4079 RVA: 0x0003FF90 File Offset: 0x0003E190
		char IConvertible.ToChar(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FF0 RID: 4080 RVA: 0x0003FF98 File Offset: 0x0003E198
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return this;
		}

		// Token: 0x06000FF1 RID: 4081 RVA: 0x0003FFA0 File Offset: 0x0003E1A0
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FF2 RID: 4082 RVA: 0x0003FFA8 File Offset: 0x0003E1A8
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FF3 RID: 4083 RVA: 0x0003FFB0 File Offset: 0x0003E1B0
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FF4 RID: 4084 RVA: 0x0003FFB8 File Offset: 0x0003E1B8
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FF5 RID: 4085 RVA: 0x0003FFC0 File Offset: 0x0003E1C0
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FF6 RID: 4086 RVA: 0x0003FFC8 File Offset: 0x0003E1C8
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FF7 RID: 4087 RVA: 0x0003FFD0 File Offset: 0x0003E1D0
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FF8 RID: 4088 RVA: 0x0003FFD8 File Offset: 0x0003E1D8
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			if (targetType == typeof(DateTime))
			{
				return this;
			}
			if (targetType == typeof(string))
			{
				return this.ToString(provider);
			}
			if (targetType == typeof(object))
			{
				return this;
			}
			throw new InvalidCastException();
		}

		// Token: 0x06000FF9 RID: 4089 RVA: 0x0004004C File Offset: 0x0003E24C
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FFA RID: 4090 RVA: 0x00040054 File Offset: 0x0003E254
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FFB RID: 4091 RVA: 0x0004005C File Offset: 0x0003E25C
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000FFC RID: 4092 RVA: 0x00040064 File Offset: 0x0003E264
		private static int AbsoluteDays(int year, int month, int day)
		{
			int num = 0;
			int i = 1;
			int[] array = (!DateTime.IsLeapYear(year)) ? DateTime.daysmonth : DateTime.daysmonthleap;
			while (i < month)
			{
				num += array[i++];
			}
			return day - 1 + num + 365 * (year - 1) + (year - 1) / 4 - (year - 1) / 100 + (year - 1) / 400;
		}

		// Token: 0x06000FFD RID: 4093 RVA: 0x000400CC File Offset: 0x0003E2CC
		private int FromTicks(DateTime.Which what)
		{
			int num = 1;
			int[] array = DateTime.daysmonth;
			int i = this.ticks.Days;
			int num2 = i / 146097;
			i -= num2 * 146097;
			int num3 = i / 36524;
			if (num3 == 4)
			{
				num3 = 3;
			}
			i -= num3 * 36524;
			int num4 = i / 1461;
			i -= num4 * 1461;
			int num5 = i / 365;
			if (num5 == 4)
			{
				num5 = 3;
			}
			if (what == DateTime.Which.Year)
			{
				return num2 * 400 + num3 * 100 + num4 * 4 + num5 + 1;
			}
			i -= num5 * 365;
			if (what == DateTime.Which.DayYear)
			{
				return i + 1;
			}
			if (num5 == 3 && (num3 == 3 || num4 != 24))
			{
				array = DateTime.daysmonthleap;
			}
			while (i >= array[num])
			{
				i -= array[num++];
			}
			if (what == DateTime.Which.Month)
			{
				return num;
			}
			return i + 1;
		}

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x06000FFE RID: 4094 RVA: 0x000401CC File Offset: 0x0003E3CC
		public DateTime Date
		{
			get
			{
				return new DateTime(this.Year, this.Month, this.Day)
				{
					kind = this.kind
				};
			}
		}

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x06000FFF RID: 4095 RVA: 0x00040200 File Offset: 0x0003E400
		public int Month
		{
			get
			{
				return this.FromTicks(DateTime.Which.Month);
			}
		}

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06001000 RID: 4096 RVA: 0x0004020C File Offset: 0x0003E40C
		public int Day
		{
			get
			{
				return this.FromTicks(DateTime.Which.Day);
			}
		}

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06001001 RID: 4097 RVA: 0x00040218 File Offset: 0x0003E418
		public DayOfWeek DayOfWeek
		{
			get
			{
				return (this.ticks.Days + DayOfWeek.Monday) % (DayOfWeek)7;
			}
		}

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06001002 RID: 4098 RVA: 0x0004022C File Offset: 0x0003E42C
		public int DayOfYear
		{
			get
			{
				return this.FromTicks(DateTime.Which.DayYear);
			}
		}

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06001003 RID: 4099 RVA: 0x00040238 File Offset: 0x0003E438
		public TimeSpan TimeOfDay
		{
			get
			{
				return new TimeSpan(this.ticks.Ticks % 864000000000L);
			}
		}

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06001004 RID: 4100 RVA: 0x00040254 File Offset: 0x0003E454
		public int Hour
		{
			get
			{
				return this.ticks.Hours;
			}
		}

		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06001005 RID: 4101 RVA: 0x00040264 File Offset: 0x0003E464
		public int Minute
		{
			get
			{
				return this.ticks.Minutes;
			}
		}

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06001006 RID: 4102 RVA: 0x00040274 File Offset: 0x0003E474
		public int Second
		{
			get
			{
				return this.ticks.Seconds;
			}
		}

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x06001007 RID: 4103 RVA: 0x00040284 File Offset: 0x0003E484
		public int Millisecond
		{
			get
			{
				return this.ticks.Milliseconds;
			}
		}

		// Token: 0x06001008 RID: 4104
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern long GetTimeMonotonic();

		// Token: 0x06001009 RID: 4105
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern long GetNow();

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x0600100A RID: 4106 RVA: 0x00040294 File Offset: 0x0003E494
		public static DateTime Now
		{
			get
			{
				long now = DateTime.GetNow();
				DateTime dateTime = new DateTime(now);
				if (now - DateTime.last_now > 600000000L)
				{
					DateTime.to_local_time_span_object = TimeZone.CurrentTimeZone.GetLocalTimeDiff(dateTime);
					DateTime.last_now = now;
				}
				DateTime result = dateTime + (TimeSpan)DateTime.to_local_time_span_object;
				result.kind = DateTimeKind.Local;
				return result;
			}
		}

		// Token: 0x1700023D RID: 573
		// (get) Token: 0x0600100B RID: 4107 RVA: 0x000402F8 File Offset: 0x0003E4F8
		public long Ticks
		{
			get
			{
				return this.ticks.Ticks;
			}
		}

		// Token: 0x1700023E RID: 574
		// (get) Token: 0x0600100C RID: 4108 RVA: 0x00040308 File Offset: 0x0003E508
		public static DateTime Today
		{
			get
			{
				DateTime now = DateTime.Now;
				return new DateTime(now.Year, now.Month, now.Day)
				{
					kind = now.kind
				};
			}
		}

		// Token: 0x1700023F RID: 575
		// (get) Token: 0x0600100D RID: 4109 RVA: 0x00040348 File Offset: 0x0003E548
		public static DateTime UtcNow
		{
			get
			{
				return new DateTime(DateTime.GetNow(), DateTimeKind.Utc);
			}
		}

		// Token: 0x17000240 RID: 576
		// (get) Token: 0x0600100E RID: 4110 RVA: 0x00040358 File Offset: 0x0003E558
		public int Year
		{
			get
			{
				return this.FromTicks(DateTime.Which.Year);
			}
		}

		// Token: 0x17000241 RID: 577
		// (get) Token: 0x0600100F RID: 4111 RVA: 0x00040364 File Offset: 0x0003E564
		public DateTimeKind Kind
		{
			get
			{
				return this.kind;
			}
		}

		// Token: 0x06001010 RID: 4112 RVA: 0x0004036C File Offset: 0x0003E56C
		public DateTime Add(TimeSpan value)
		{
			DateTime result = this.AddTicks(value.Ticks);
			result.kind = this.kind;
			return result;
		}

		// Token: 0x06001011 RID: 4113 RVA: 0x00040398 File Offset: 0x0003E598
		public DateTime AddDays(double value)
		{
			return this.AddMilliseconds(Math.Round(value * 86400000.0));
		}

		// Token: 0x06001012 RID: 4114 RVA: 0x000403B0 File Offset: 0x0003E5B0
		public DateTime AddTicks(long value)
		{
			if (value + this.ticks.Ticks > 3155378975999999999L || value + this.ticks.Ticks < 0L)
			{
				throw new ArgumentOutOfRangeException();
			}
			return new DateTime(value + this.ticks.Ticks)
			{
				kind = this.kind
			};
		}

		// Token: 0x06001013 RID: 4115 RVA: 0x00040414 File Offset: 0x0003E614
		public DateTime AddHours(double value)
		{
			return this.AddMilliseconds(value * 3600000.0);
		}

		// Token: 0x06001014 RID: 4116 RVA: 0x00040428 File Offset: 0x0003E628
		public DateTime AddMilliseconds(double value)
		{
			if (value * 10000.0 > 9.223372036854776E+18 || value * 10000.0 < -9.223372036854776E+18)
			{
				throw new ArgumentOutOfRangeException();
			}
			long value2 = (long)Math.Round(value * 10000.0);
			return this.AddTicks(value2);
		}

		// Token: 0x06001015 RID: 4117 RVA: 0x00040488 File Offset: 0x0003E688
		private DateTime AddRoundedMilliseconds(double ms)
		{
			if (ms * 10000.0 > 9.223372036854776E+18 || ms * 10000.0 < -9.223372036854776E+18)
			{
				throw new ArgumentOutOfRangeException();
			}
			long value = (long)(ms += ((ms <= 0.0) ? -0.5 : 0.5)) * 10000L;
			return this.AddTicks(value);
		}

		// Token: 0x06001016 RID: 4118 RVA: 0x00040508 File Offset: 0x0003E708
		public DateTime AddMinutes(double value)
		{
			return this.AddMilliseconds(value * 60000.0);
		}

		// Token: 0x06001017 RID: 4119 RVA: 0x0004051C File Offset: 0x0003E71C
		public DateTime AddMonths(int months)
		{
			int num = this.Day;
			int num2 = this.Month + months % 12;
			int num3 = this.Year + months / 12;
			if (num2 < 1)
			{
				num2 = 12 + num2;
				num3--;
			}
			else if (num2 > 12)
			{
				num2 -= 12;
				num3++;
			}
			int num4 = DateTime.DaysInMonth(num3, num2);
			if (num > num4)
			{
				num = num4;
			}
			DateTime dateTime = new DateTime(num3, num2, num);
			dateTime.kind = this.kind;
			return dateTime.Add(this.TimeOfDay);
		}

		// Token: 0x06001018 RID: 4120 RVA: 0x000405A4 File Offset: 0x0003E7A4
		public DateTime AddSeconds(double value)
		{
			return this.AddMilliseconds(value * 1000.0);
		}

		// Token: 0x06001019 RID: 4121 RVA: 0x000405B8 File Offset: 0x0003E7B8
		public DateTime AddYears(int value)
		{
			return this.AddMonths(value * 12);
		}

		// Token: 0x0600101A RID: 4122 RVA: 0x000405C4 File Offset: 0x0003E7C4
		public static int Compare(DateTime t1, DateTime t2)
		{
			if (t1.ticks < t2.ticks)
			{
				return -1;
			}
			if (t1.ticks > t2.ticks)
			{
				return 1;
			}
			return 0;
		}

		// Token: 0x0600101B RID: 4123 RVA: 0x000405FC File Offset: 0x0003E7FC
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is DateTime))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.DateTime"));
			}
			return DateTime.Compare(this, (DateTime)value);
		}

		// Token: 0x0600101C RID: 4124 RVA: 0x00040640 File Offset: 0x0003E840
		public bool IsDaylightSavingTime()
		{
			return this.kind != DateTimeKind.Utc && TimeZone.CurrentTimeZone.IsDaylightSavingTime(this);
		}

		// Token: 0x0600101D RID: 4125 RVA: 0x00040660 File Offset: 0x0003E860
		public int CompareTo(DateTime value)
		{
			return DateTime.Compare(this, value);
		}

		// Token: 0x0600101E RID: 4126 RVA: 0x00040670 File Offset: 0x0003E870
		public bool Equals(DateTime value)
		{
			return value.ticks == this.ticks;
		}

		// Token: 0x0600101F RID: 4127 RVA: 0x00040684 File Offset: 0x0003E884
		public long ToBinary()
		{
			DateTimeKind dateTimeKind = this.kind;
			if (dateTimeKind == DateTimeKind.Utc)
			{
				return this.Ticks | 4611686018427387904L;
			}
			if (dateTimeKind != DateTimeKind.Local)
			{
				return this.Ticks;
			}
			return this.ToUniversalTime().Ticks | long.MinValue;
		}

		// Token: 0x06001020 RID: 4128 RVA: 0x000406DC File Offset: 0x0003E8DC
		public static DateTime FromBinary(long dateData)
		{
			ulong num = (ulong)dateData >> 62;
			if (num == (ulong)0)
			{
				return new DateTime(dateData, DateTimeKind.Unspecified);
			}
			if (num != (ulong)1)
			{
				DateTime dateTime = new DateTime(dateData & 4611686018427387903L, DateTimeKind.Utc);
				return dateTime.ToLocalTime();
			}
			return new DateTime(dateData ^ 4611686018427387904L, DateTimeKind.Utc);
		}

		// Token: 0x06001021 RID: 4129 RVA: 0x00040738 File Offset: 0x0003E938
		public static DateTime SpecifyKind(DateTime value, DateTimeKind kind)
		{
			return new DateTime(value.Ticks, kind);
		}

		// Token: 0x06001022 RID: 4130 RVA: 0x00040748 File Offset: 0x0003E948
		public static int DaysInMonth(int year, int month)
		{
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (year < 1 || year > 9999)
			{
				throw new ArgumentOutOfRangeException();
			}
			int[] array = (!DateTime.IsLeapYear(year)) ? DateTime.daysmonth : DateTime.daysmonthleap;
			return array[month];
		}

		// Token: 0x06001023 RID: 4131 RVA: 0x000407A0 File Offset: 0x0003E9A0
		public override bool Equals(object value)
		{
			return value is DateTime && ((DateTime)value).ticks == this.ticks;
		}

		// Token: 0x06001024 RID: 4132 RVA: 0x000407D4 File Offset: 0x0003E9D4
		public static bool Equals(DateTime t1, DateTime t2)
		{
			return t1.ticks == t2.ticks;
		}

		// Token: 0x06001025 RID: 4133 RVA: 0x000407EC File Offset: 0x0003E9EC
		public static DateTime FromFileTime(long fileTime)
		{
			if (fileTime < 0L)
			{
				throw new ArgumentOutOfRangeException("fileTime", "< 0");
			}
			DateTime dateTime = new DateTime(504911232000000000L + fileTime);
			return dateTime.ToLocalTime();
		}

		// Token: 0x06001026 RID: 4134 RVA: 0x0004082C File Offset: 0x0003EA2C
		public static DateTime FromFileTimeUtc(long fileTime)
		{
			if (fileTime < 0L)
			{
				throw new ArgumentOutOfRangeException("fileTime", "< 0");
			}
			return new DateTime(504911232000000000L + fileTime);
		}

		// Token: 0x06001027 RID: 4135 RVA: 0x00040864 File Offset: 0x0003EA64
		public static DateTime FromOADate(double d)
		{
			if (d <= -657435.0 || d >= 2958466.0)
			{
				throw new ArgumentException("d", "[-657435,2958466]");
			}
			DateTime result = new DateTime(599264352000000000L);
			if (d < 0.0)
			{
				double num = Math.Ceiling(d);
				result = result.AddRoundedMilliseconds(num * 86400000.0);
				double num2 = num - d;
				result = result.AddRoundedMilliseconds(num2 * 86400000.0);
			}
			else
			{
				result = result.AddRoundedMilliseconds(d * 86400000.0);
			}
			return result;
		}

		// Token: 0x06001028 RID: 4136 RVA: 0x00040908 File Offset: 0x0003EB08
		public string[] GetDateTimeFormats()
		{
			return this.GetDateTimeFormats(CultureInfo.CurrentCulture);
		}

		// Token: 0x06001029 RID: 4137 RVA: 0x00040918 File Offset: 0x0003EB18
		public string[] GetDateTimeFormats(char format)
		{
			if ("dDgGfFmMrRstTuUyY".IndexOf(format) < 0)
			{
				throw new FormatException("Invalid format character.");
			}
			return new string[]
			{
				this.ToString(format.ToString())
			};
		}

		// Token: 0x0600102A RID: 4138 RVA: 0x0004095C File Offset: 0x0003EB5C
		public string[] GetDateTimeFormats(IFormatProvider provider)
		{
			DateTimeFormatInfo provider2 = (DateTimeFormatInfo)provider.GetFormat(typeof(DateTimeFormatInfo));
			ArrayList arrayList = new ArrayList();
			foreach (char format in "dDgGfFmMrRstTuUyY")
			{
				arrayList.AddRange(this.GetDateTimeFormats(format, provider2));
			}
			return arrayList.ToArray(typeof(string)) as string[];
		}

		// Token: 0x0600102B RID: 4139 RVA: 0x000409D4 File Offset: 0x0003EBD4
		public string[] GetDateTimeFormats(char format, IFormatProvider provider)
		{
			if ("dDgGfFmMrRstTuUyY".IndexOf(format) < 0)
			{
				throw new FormatException("Invalid format character.");
			}
			bool adjustutc = false;
			if (format == 'U')
			{
				adjustutc = true;
			}
			DateTimeFormatInfo dateTimeFormatInfo = (DateTimeFormatInfo)provider.GetFormat(typeof(DateTimeFormatInfo));
			return this.GetDateTimeFormats(adjustutc, dateTimeFormatInfo.GetAllRawDateTimePatterns(format), dateTimeFormatInfo);
		}

		// Token: 0x0600102C RID: 4140 RVA: 0x00040A3C File Offset: 0x0003EC3C
		private string[] GetDateTimeFormats(bool adjustutc, string[] patterns, DateTimeFormatInfo dfi)
		{
			string[] array = new string[patterns.Length];
			DateTime dt = (!adjustutc) ? this : this.ToUniversalTime();
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = DateTimeUtils.ToString(dt, patterns[i], dfi);
			}
			return array;
		}

		// Token: 0x0600102D RID: 4141 RVA: 0x00040A8C File Offset: 0x0003EC8C
		private void CheckDateTimeKind(DateTimeKind kind)
		{
			if (kind != DateTimeKind.Unspecified && kind != DateTimeKind.Utc && kind != DateTimeKind.Local)
			{
				throw new ArgumentException("Invalid DateTimeKind value.", "kind");
			}
		}

		// Token: 0x0600102E RID: 4142 RVA: 0x00040AC0 File Offset: 0x0003ECC0
		public override int GetHashCode()
		{
			return (int)this.ticks.Ticks;
		}

		// Token: 0x0600102F RID: 4143 RVA: 0x00040AD0 File Offset: 0x0003ECD0
		public TypeCode GetTypeCode()
		{
			return TypeCode.DateTime;
		}

		// Token: 0x06001030 RID: 4144 RVA: 0x00040AD4 File Offset: 0x0003ECD4
		public static bool IsLeapYear(int year)
		{
			if (year < 1 || year > 9999)
			{
				throw new ArgumentOutOfRangeException();
			}
			return (year % 4 == 0 && year % 100 != 0) || year % 400 == 0;
		}

		// Token: 0x06001031 RID: 4145 RVA: 0x00040B18 File Offset: 0x0003ED18
		public static DateTime Parse(string s)
		{
			return DateTime.Parse(s, null);
		}

		// Token: 0x06001032 RID: 4146 RVA: 0x00040B24 File Offset: 0x0003ED24
		public static DateTime Parse(string s, IFormatProvider provider)
		{
			return DateTime.Parse(s, provider, DateTimeStyles.AllowWhiteSpaces);
		}

		// Token: 0x06001033 RID: 4147 RVA: 0x00040B30 File Offset: 0x0003ED30
		public static DateTime Parse(string s, IFormatProvider provider, DateTimeStyles styles)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			Exception ex = null;
			DateTime result;
			DateTimeOffset dateTimeOffset;
			if (!DateTime.CoreParse(s, provider, styles, out result, out dateTimeOffset, true, ref ex))
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x06001034 RID: 4148 RVA: 0x00040B68 File Offset: 0x0003ED68
		internal static bool CoreParse(string s, IFormatProvider provider, DateTimeStyles styles, out DateTime result, out DateTimeOffset dto, bool setExceptionOnError, ref Exception exception)
		{
			dto = new DateTimeOffset(0L, TimeSpan.Zero);
			if (s == null || s.Length == 0)
			{
				if (setExceptionOnError)
				{
					exception = new FormatException("String was not recognized as a valid DateTime.");
				}
				result = DateTime.MinValue;
				return false;
			}
			if (provider == null)
			{
				provider = CultureInfo.CurrentCulture;
			}
			DateTimeFormatInfo instance = DateTimeFormatInfo.GetInstance(provider);
			string[] array = DateTime.YearMonthDayFormats(instance, setExceptionOnError, ref exception);
			if (array == null)
			{
				result = DateTime.MinValue;
				return false;
			}
			bool flag = false;
			foreach (string firstPart in array)
			{
				bool flag2 = false;
				if (DateTime._DoParse(s, firstPart, string.Empty, false, out result, out dto, instance, styles, true, ref flag2, ref flag))
				{
					return true;
				}
				if (flag2)
				{
					for (int j = 0; j < DateTime.ParseTimeFormats.Length; j++)
					{
						if (DateTime._DoParse(s, firstPart, DateTime.ParseTimeFormats[j], false, out result, out dto, instance, styles, true, ref flag2, ref flag))
						{
							return true;
						}
					}
				}
			}
			int num = instance.MonthDayPattern.IndexOf('d');
			int num2 = instance.MonthDayPattern.IndexOf('M');
			if (num == -1 || num2 == -1)
			{
				result = DateTime.MinValue;
				if (setExceptionOnError)
				{
					exception = new FormatException(Locale.GetText("Order of month and date is not defined by {0}", new object[]
					{
						instance.MonthDayPattern
					}));
				}
				return false;
			}
			bool flag3 = num < num2;
			string[] array2 = (!flag3) ? DateTime.MonthDayShortFormats : DateTime.DayMonthShortFormats;
			for (int k = 0; k < array2.Length; k++)
			{
				bool flag4 = false;
				if (DateTime._DoParse(s, array2[k], string.Empty, false, out result, out dto, instance, styles, true, ref flag4, ref flag))
				{
					return true;
				}
			}
			for (int l = 0; l < DateTime.ParseTimeFormats.Length; l++)
			{
				string firstPart2 = DateTime.ParseTimeFormats[l];
				bool flag5 = false;
				if (DateTime._DoParse(s, firstPart2, string.Empty, false, out result, out dto, instance, styles, false, ref flag5, ref flag))
				{
					return true;
				}
				if (flag5)
				{
					for (int m = 0; m < array2.Length; m++)
					{
						if (DateTime._DoParse(s, firstPart2, array2[m], false, out result, out dto, instance, styles, false, ref flag5, ref flag))
						{
							return true;
						}
					}
					foreach (string text in array)
					{
						if (text[text.Length - 1] != 'T')
						{
							if (DateTime._DoParse(s, firstPart2, text, false, out result, out dto, instance, styles, false, ref flag5, ref flag))
							{
								return true;
							}
						}
					}
				}
			}
			if (DateTime.ParseExact(s, instance.GetAllDateTimePatternsInternal(), instance, styles, out result, false, ref flag, setExceptionOnError, ref exception))
			{
				return true;
			}
			if (!setExceptionOnError)
			{
				return false;
			}
			exception = new FormatException("String was not recognized as a valid DateTime.");
			return false;
		}

		// Token: 0x06001035 RID: 4149 RVA: 0x00040E48 File Offset: 0x0003F048
		public static DateTime ParseExact(string s, string format, IFormatProvider provider)
		{
			return DateTime.ParseExact(s, format, provider, DateTimeStyles.None);
		}

		// Token: 0x06001036 RID: 4150 RVA: 0x00040E54 File Offset: 0x0003F054
		private static string[] YearMonthDayFormats(DateTimeFormatInfo dfi, bool setExceptionOnError, ref Exception exc)
		{
			int num = dfi.ShortDatePattern.IndexOf('d');
			int num2 = dfi.ShortDatePattern.IndexOf('M');
			int num3 = dfi.ShortDatePattern.IndexOf('y');
			if (num == -1 || num2 == -1 || num3 == -1)
			{
				if (setExceptionOnError)
				{
					exc = new FormatException(Locale.GetText("Order of year, month and date is not defined by {0}", new object[]
					{
						dfi.ShortDatePattern
					}));
				}
				return null;
			}
			if (num3 < num2)
			{
				if (num2 < num)
				{
					return DateTime.ParseYearMonthDayFormats;
				}
				if (num3 < num)
				{
					return DateTime.ParseYearDayMonthFormats;
				}
				if (setExceptionOnError)
				{
					exc = new FormatException(Locale.GetText("Order of date, year and month defined by {0} is not supported", new object[]
					{
						dfi.ShortDatePattern
					}));
				}
				return null;
			}
			else
			{
				if (num < num2)
				{
					return DateTime.ParseDayMonthYearFormats;
				}
				if (num < num3)
				{
					return DateTime.ParseMonthDayYearFormats;
				}
				if (setExceptionOnError)
				{
					exc = new FormatException(Locale.GetText("Order of month, year and date defined by {0} is not supported", new object[]
					{
						dfi.ShortDatePattern
					}));
				}
				return null;
			}
		}

		// Token: 0x06001037 RID: 4151 RVA: 0x00040F54 File Offset: 0x0003F154
		private static int _ParseNumber(string s, int valuePos, int min_digits, int digits, bool leadingzero, bool sloppy_parsing, out int num_parsed)
		{
			int num = 0;
			if (sloppy_parsing)
			{
				leadingzero = false;
			}
			if (!leadingzero)
			{
				int num2 = 0;
				int i = valuePos;
				while (i < s.Length && i < digits + valuePos)
				{
					if (!char.IsDigit(s[i]))
					{
						break;
					}
					num2++;
					i++;
				}
				digits = num2;
			}
			if (digits < min_digits)
			{
				num_parsed = -1;
				return 0;
			}
			if (s.Length - valuePos < digits)
			{
				num_parsed = -1;
				return 0;
			}
			for (int i = valuePos; i < digits + valuePos; i++)
			{
				char c = s[i];
				if (!char.IsDigit(c))
				{
					num_parsed = -1;
					return 0;
				}
				num = num * 10 + (int)((byte)(c - '0'));
			}
			num_parsed = digits;
			return num;
		}

		// Token: 0x06001038 RID: 4152 RVA: 0x00041014 File Offset: 0x0003F214
		private static int _ParseEnum(string s, int sPos, string[] values, string[] invValues, bool exact, out int num_parsed)
		{
			for (int i = values.Length - 1; i >= 0; i--)
			{
				if (!exact && invValues[i].Length > values[i].Length)
				{
					if (invValues[i].Length > 0 && DateTime._ParseString(s, sPos, 0, invValues[i], out num_parsed))
					{
						return i;
					}
					if (values[i].Length > 0 && DateTime._ParseString(s, sPos, 0, values[i], out num_parsed))
					{
						return i;
					}
				}
				else
				{
					if (values[i].Length > 0 && DateTime._ParseString(s, sPos, 0, values[i], out num_parsed))
					{
						return i;
					}
					if (!exact && invValues[i].Length > 0 && DateTime._ParseString(s, sPos, 0, invValues[i], out num_parsed))
					{
						return i;
					}
				}
			}
			num_parsed = -1;
			return -1;
		}

		// Token: 0x06001039 RID: 4153 RVA: 0x000410EC File Offset: 0x0003F2EC
		private static bool _ParseString(string s, int sPos, int maxlength, string value, out int num_parsed)
		{
			if (maxlength <= 0)
			{
				maxlength = value.Length;
			}
			if (sPos + maxlength <= s.Length && string.Compare(s, sPos, value, 0, maxlength, true, CultureInfo.InvariantCulture) == 0)
			{
				num_parsed = maxlength;
				return true;
			}
			num_parsed = -1;
			return false;
		}

		// Token: 0x0600103A RID: 4154 RVA: 0x00041138 File Offset: 0x0003F338
		private static bool _ParseAmPm(string s, int valuePos, int num, DateTimeFormatInfo dfi, bool exact, out int num_parsed, ref int ampm)
		{
			num_parsed = -1;
			if (ampm != -1)
			{
				return false;
			}
			if (DateTime.IsLetter(s, valuePos))
			{
				DateTimeFormatInfo invariantInfo = DateTimeFormatInfo.InvariantInfo;
				if ((!exact && DateTime._ParseString(s, valuePos, num, invariantInfo.PMDesignator, out num_parsed)) || (dfi.PMDesignator != string.Empty && DateTime._ParseString(s, valuePos, num, dfi.PMDesignator, out num_parsed)))
				{
					ampm = 1;
				}
				else
				{
					if ((exact || !DateTime._ParseString(s, valuePos, num, invariantInfo.AMDesignator, out num_parsed)) && !DateTime._ParseString(s, valuePos, num, dfi.AMDesignator, out num_parsed))
					{
						return false;
					}
					if (exact || num_parsed != 0)
					{
						ampm = 0;
					}
				}
				return true;
			}
			if (dfi.AMDesignator != string.Empty)
			{
				return false;
			}
			if (exact)
			{
				ampm = 0;
			}
			num_parsed = 0;
			return true;
		}

		// Token: 0x0600103B RID: 4155 RVA: 0x0004122C File Offset: 0x0003F42C
		private static bool _ParseTimeSeparator(string s, int sPos, DateTimeFormatInfo dfi, bool exact, out int num_parsed)
		{
			return DateTime._ParseString(s, sPos, 0, dfi.TimeSeparator, out num_parsed) || (!exact && DateTime._ParseString(s, sPos, 0, ":", out num_parsed));
		}

		// Token: 0x0600103C RID: 4156 RVA: 0x0004126C File Offset: 0x0003F46C
		private static bool _ParseDateSeparator(string s, int sPos, DateTimeFormatInfo dfi, bool exact, out int num_parsed)
		{
			num_parsed = -1;
			if (exact && s[sPos] != '/')
			{
				return false;
			}
			if (DateTime._ParseTimeSeparator(s, sPos, dfi, exact, out num_parsed) || char.IsDigit(s[sPos]) || char.IsLetter(s[sPos]))
			{
				return false;
			}
			num_parsed = 1;
			return true;
		}

		// Token: 0x0600103D RID: 4157 RVA: 0x000412CC File Offset: 0x0003F4CC
		private static bool IsLetter(string s, int pos)
		{
			return pos < s.Length && char.IsLetter(s[pos]);
		}

		// Token: 0x0600103E RID: 4158 RVA: 0x000412EC File Offset: 0x0003F4EC
		private static bool _DoParse(string s, string firstPart, string secondPart, bool exact, out DateTime result, out DateTimeOffset dto, DateTimeFormatInfo dfi, DateTimeStyles style, bool firstPartIsDate, ref bool incompleteFormat, ref bool longYear)
		{
			bool flag = false;
			bool flag2 = false;
			bool sloppy_parsing = false;
			dto = new DateTimeOffset(0L, TimeSpan.Zero);
			bool flag3 = !exact && secondPart != null;
			incompleteFormat = false;
			int num = 0;
			string text = firstPart;
			bool flag4 = false;
			DateTimeFormatInfo invariantInfo = DateTimeFormatInfo.InvariantInfo;
			if (text.Length == 1)
			{
				text = DateTimeUtils.GetStandardPattern(text[0], dfi, out flag, out flag2);
			}
			result = new DateTime(0L);
			if (text == null)
			{
				return false;
			}
			if (s == null)
			{
				return false;
			}
			if ((style & DateTimeStyles.AllowLeadingWhite) != DateTimeStyles.None)
			{
				text = text.TrimStart(null);
				s = s.TrimStart(null);
			}
			if ((style & DateTimeStyles.AllowTrailingWhite) != DateTimeStyles.None)
			{
				text = text.TrimEnd(null);
				s = s.TrimEnd(null);
			}
			if (flag2)
			{
				dfi = invariantInfo;
			}
			if ((style & DateTimeStyles.AllowInnerWhite) != DateTimeStyles.None)
			{
				sloppy_parsing = true;
			}
			string text2 = text;
			int length = text.Length;
			int num2 = 0;
			int num3 = 0;
			if (length == 0)
			{
				return false;
			}
			int num4 = -1;
			int num5 = -1;
			int num6 = -1;
			int num7 = -1;
			int num8 = -1;
			int num9 = -1;
			int num10 = -1;
			double num11 = -1.0;
			int num12 = -1;
			int num13 = -1;
			int num14 = -1;
			int num15 = -1;
			bool flag5 = true;
			while (num != s.Length)
			{
				int num16 = 0;
				if (flag3 && num2 + num3 == 0)
				{
					bool flag6 = DateTime.IsLetter(s, num);
					if (flag6)
					{
						if (s[num] == 'Z')
						{
							num16 = 1;
						}
						else
						{
							DateTime._ParseString(s, num, 0, "GMT", out num16);
						}
						if (num16 > 0 && !DateTime.IsLetter(s, num + num16))
						{
							num += num16;
							flag = true;
							continue;
						}
					}
					if (!flag4 && DateTime._ParseAmPm(s, num, 0, dfi, exact, out num16, ref num12))
					{
						if (DateTime.IsLetter(s, num + num16))
						{
							num12 = -1;
						}
						else if (num16 > 0)
						{
							num += num16;
							continue;
						}
					}
					if (!flag4 && num5 == -1 && flag6)
					{
						num5 = DateTime._ParseEnum(s, num, dfi.RawDayNames, invariantInfo.RawDayNames, exact, out num16);
						if (num5 == -1)
						{
							num5 = DateTime._ParseEnum(s, num, dfi.RawAbbreviatedDayNames, invariantInfo.RawAbbreviatedDayNames, exact, out num16);
						}
						if (num5 != -1 && !DateTime.IsLetter(s, num + num16))
						{
							num += num16;
							continue;
						}
						num5 = -1;
					}
					if (char.IsWhiteSpace(s[num]) || s[num] == ',')
					{
						num++;
						continue;
					}
					num16 = 0;
				}
				if (num2 + num3 >= length)
				{
					if (flag3 && num3 == 0)
					{
						flag4 = (flag5 && firstPart[firstPart.Length - 1] == 'T');
						if (flag5 || !(text == string.Empty))
						{
							num2 = 0;
							if (flag5)
							{
								text = secondPart;
							}
							else
							{
								text = string.Empty;
							}
							text2 = text;
							length = text2.Length;
							flag5 = false;
							continue;
						}
					}
					IL_EA9:
					if (num2 + 1 < length && text2[num2] == '.' && text2[num2 + 1] == 'F')
					{
						num2++;
						while (num2 < length && text2[num2] == 'F')
						{
							num2++;
						}
					}
					while (num2 < length && text2[num2] == 'K')
					{
						num2++;
					}
					if (num2 < length)
					{
						return false;
					}
					if (s.Length > num)
					{
						if (num == 0)
						{
							return false;
						}
						if (char.IsDigit(s[num]) && char.IsDigit(s[num - 1]))
						{
							return false;
						}
						if (char.IsLetter(s[num]) && char.IsLetter(s[num - 1]))
						{
							return false;
						}
						incompleteFormat = true;
						return false;
					}
					else
					{
						if (num8 == -1)
						{
							num8 = 0;
						}
						if (num9 == -1)
						{
							num9 = 0;
						}
						if (num10 == -1)
						{
							num10 = 0;
						}
						if (num11 == -1.0)
						{
							num11 = 0.0;
						}
						if (num4 == -1 && num6 == -1 && num7 == -1)
						{
							if ((style & DateTimeStyles.NoCurrentDateDefault) != DateTimeStyles.None)
							{
								num4 = 1;
								num6 = 1;
								num7 = 1;
							}
							else
							{
								num4 = DateTime.Today.Day;
								num6 = DateTime.Today.Month;
								num7 = DateTime.Today.Year;
							}
						}
						if (num4 == -1)
						{
							num4 = 1;
						}
						if (num6 == -1)
						{
							num6 = 1;
						}
						if (num7 == -1)
						{
							if ((style & DateTimeStyles.NoCurrentDateDefault) != DateTimeStyles.None)
							{
								num7 = 1;
							}
							else
							{
								num7 = DateTime.Today.Year;
							}
						}
						if (num12 == 0 && num8 == 12)
						{
							num8 = 0;
						}
						if (num12 == 1 && (!flag3 || num8 < 12))
						{
							num8 += 12;
						}
						if (num7 < 1 || num7 > 9999 || num6 < 1 || num6 > 12 || num4 < 1 || num4 > DateTime.DaysInMonth(num7, num6) || num8 < 0 || num8 > 23 || num9 < 0 || num9 > 59 || num10 < 0 || num10 > 59)
						{
							return false;
						}
						result = new DateTime(num7, num6, num4, num8, num9, num10, 0);
						result = result.AddSeconds(num11);
						if (num5 != -1 && num5 != (int)result.DayOfWeek)
						{
							return false;
						}
						if (num13 == -1)
						{
							if (result != DateTime.MinValue)
							{
								try
								{
									dto = new DateTimeOffset(result);
								}
								catch
								{
								}
							}
						}
						else
						{
							if (num15 == -1)
							{
								num15 = 0;
							}
							if (num14 == -1)
							{
								num14 = 0;
							}
							if (num13 == 1)
							{
								num14 = -num14;
								num15 = -num15;
							}
							try
							{
								dto = new DateTimeOffset(result, new TimeSpan(num14, num15, 0));
							}
							catch
							{
							}
						}
						bool flag7 = (style & DateTimeStyles.AdjustToUniversal) != DateTimeStyles.None;
						if (num13 != -1)
						{
							long num17 = (result.ticks - dto.Offset).Ticks;
							if (num17 < 0L)
							{
								num17 += 864000000000L;
							}
							result = new DateTime(false, new TimeSpan(num17));
							result.kind = DateTimeKind.Utc;
							if ((style & DateTimeStyles.RoundtripKind) != DateTimeStyles.None)
							{
								result = result.ToLocalTime();
							}
						}
						else if (flag || (style & DateTimeStyles.AssumeUniversal) != DateTimeStyles.None)
						{
							result.kind = DateTimeKind.Utc;
						}
						else if ((style & DateTimeStyles.AssumeLocal) != DateTimeStyles.None)
						{
							result.kind = DateTimeKind.Local;
						}
						bool flag8 = !flag7 && (style & DateTimeStyles.RoundtripKind) == DateTimeStyles.None;
						if (result.kind != DateTimeKind.Unspecified)
						{
							if (flag7)
							{
								result = result.ToUniversalTime();
							}
							else if (flag8)
							{
								result = result.ToLocalTime();
							}
						}
						return true;
					}
				}
				else
				{
					bool leadingzero = true;
					if (text2[num2] == '\'')
					{
						num3 = 1;
						while (num2 + num3 < length)
						{
							if (text2[num2 + num3] == '\'')
							{
								break;
							}
							if (num == s.Length || s[num] != text2[num2 + num3])
							{
								return false;
							}
							num++;
							num3++;
						}
						num2 += num3 + 1;
						num3 = 0;
					}
					else if (text2[num2] == '"')
					{
						num3 = 1;
						while (num2 + num3 < length)
						{
							if (text2[num2 + num3] == '"')
							{
								break;
							}
							if (num == s.Length || s[num] != text2[num2 + num3])
							{
								return false;
							}
							num++;
							num3++;
						}
						num2 += num3 + 1;
						num3 = 0;
					}
					else if (text2[num2] == '\\')
					{
						num2 += num3 + 1;
						num3 = 0;
						if (num2 >= length)
						{
							return false;
						}
						if (s[num] != text2[num2])
						{
							return false;
						}
						num++;
						num2++;
					}
					else if (text2[num2] == '%')
					{
						num2++;
					}
					else if (char.IsWhiteSpace(s[num]) || (s[num] == ',' && ((!exact && text2[num2] == '/') || char.IsWhiteSpace(text2[num2]))))
					{
						num++;
						num3 = 0;
						if (exact && (style & DateTimeStyles.AllowInnerWhite) == DateTimeStyles.None)
						{
							if (!char.IsWhiteSpace(text2[num2]))
							{
								return false;
							}
							num2++;
						}
						else
						{
							int i;
							for (i = num; i < s.Length; i++)
							{
								if (!char.IsWhiteSpace(s[i]) && s[i] != ',')
								{
									break;
								}
							}
							num = i;
							for (i = num2; i < text2.Length; i++)
							{
								if (!char.IsWhiteSpace(text2[i]) && text2[i] != ',')
								{
									break;
								}
							}
							num2 = i;
							if (!exact && num2 < text2.Length && text2[num2] == '/' && !DateTime._ParseDateSeparator(s, num, dfi, exact, out num16))
							{
								num2++;
							}
						}
					}
					else if (num2 + num3 + 1 < length && text2[num2 + num3 + 1] == text2[num2 + num3])
					{
						num3++;
					}
					else
					{
						char c = text2[num2];
						switch (c)
						{
						case 'F':
							leadingzero = false;
							goto IL_A82;
						case 'G':
							if (s[num] != 'G')
							{
								return false;
							}
							if (num2 + 2 < length && num + 2 < s.Length && text2[num2 + 1] == 'M' && s[num + 1] == 'M' && text2[num2 + 2] == 'T' && s[num + 2] == 'T')
							{
								flag = true;
								num3 = 2;
								num16 = 3;
							}
							else
							{
								num3 = 0;
								num16 = 1;
							}
							break;
						case 'H':
							if (num8 != -1 || (!flag3 && num12 >= 0))
							{
								return false;
							}
							if (num3 == 0)
							{
								num8 = DateTime._ParseNumber(s, num, 1, 2, false, sloppy_parsing, out num16);
							}
							else
							{
								num8 = DateTime._ParseNumber(s, num, 1, 2, true, sloppy_parsing, out num16);
							}
							if (num8 >= 24)
							{
								return false;
							}
							break;
						default:
							switch (c)
							{
							case 's':
								if (num10 != -1)
								{
									return false;
								}
								if (num3 == 0)
								{
									num10 = DateTime._ParseNumber(s, num, 1, 2, false, sloppy_parsing, out num16);
								}
								else
								{
									num10 = DateTime._ParseNumber(s, num, 1, 2, true, sloppy_parsing, out num16);
								}
								if (num10 >= 60)
								{
									return false;
								}
								break;
							case 't':
								if (!DateTime._ParseAmPm(s, num, (num3 <= 0) ? 1 : 0, dfi, exact, out num16, ref num12))
								{
									return false;
								}
								break;
							default:
								switch (c)
								{
								case 'd':
									if ((num3 < 2 && num4 != -1) || (num3 >= 2 && num5 != -1))
									{
										return false;
									}
									if (num3 == 0)
									{
										num4 = DateTime._ParseNumber(s, num, 1, 2, false, sloppy_parsing, out num16);
									}
									else if (num3 == 1)
									{
										num4 = DateTime._ParseNumber(s, num, 1, 2, true, sloppy_parsing, out num16);
									}
									else if (num3 == 2)
									{
										num5 = DateTime._ParseEnum(s, num, dfi.RawAbbreviatedDayNames, invariantInfo.RawAbbreviatedDayNames, exact, out num16);
									}
									else
									{
										num5 = DateTime._ParseEnum(s, num, dfi.RawDayNames, invariantInfo.RawDayNames, exact, out num16);
									}
									break;
								default:
									if (c != '/')
									{
										if (c != ':')
										{
											if (c != 'Z')
											{
												if (c != 'm')
												{
													if (s[num] != text2[num2])
													{
														return false;
													}
													num3 = 0;
													num16 = 1;
												}
												else
												{
													if (num9 != -1)
													{
														return false;
													}
													if (num3 == 0)
													{
														num9 = DateTime._ParseNumber(s, num, 1, 2, false, sloppy_parsing, out num16);
													}
													else
													{
														num9 = DateTime._ParseNumber(s, num, 1, 2, true, sloppy_parsing, out num16);
													}
													if (num9 >= 60)
													{
														return false;
													}
												}
											}
											else
											{
												if (s[num] != 'Z')
												{
													return false;
												}
												num3 = 0;
												num16 = 1;
												flag = true;
											}
										}
										else if (!DateTime._ParseTimeSeparator(s, num, dfi, exact, out num16))
										{
											return false;
										}
									}
									else
									{
										if (!DateTime._ParseDateSeparator(s, num, dfi, exact, out num16))
										{
											return false;
										}
										num3 = 0;
									}
									break;
								case 'f':
									goto IL_A82;
								case 'h':
									if (num8 != -1)
									{
										return false;
									}
									if (num3 == 0)
									{
										num8 = DateTime._ParseNumber(s, num, 1, 2, false, sloppy_parsing, out num16);
									}
									else
									{
										num8 = DateTime._ParseNumber(s, num, 1, 2, true, sloppy_parsing, out num16);
									}
									if (num8 > 12)
									{
										return false;
									}
									if (num8 == 12)
									{
										num8 = 0;
									}
									break;
								}
								break;
							case 'y':
								if (num7 != -1)
								{
									return false;
								}
								if (num3 == 0)
								{
									num7 = DateTime._ParseNumber(s, num, 1, 2, false, sloppy_parsing, out num16);
								}
								else if (num3 < 3)
								{
									num7 = DateTime._ParseNumber(s, num, 1, 2, true, sloppy_parsing, out num16);
								}
								else
								{
									num7 = DateTime._ParseNumber(s, num, (!exact) ? 3 : 4, 4, false, sloppy_parsing, out num16);
									if (num7 >= 1000 && num16 == 4 && !longYear && s.Length > 4 + num)
									{
										int num18 = 0;
										int num19 = DateTime._ParseNumber(s, num, 5, 5, false, sloppy_parsing, out num18);
										longYear = (num19 > 9999);
									}
									num3 = 3;
								}
								if (num16 <= 2)
								{
									num7 += ((num7 >= 30) ? 1900 : 2000);
								}
								break;
							case 'z':
								if (num13 != -1)
								{
									return false;
								}
								if (s[num] == '+')
								{
									num13 = 0;
								}
								else
								{
									if (s[num] != '-')
									{
										return false;
									}
									num13 = 1;
								}
								num++;
								if (num3 == 0)
								{
									num14 = DateTime._ParseNumber(s, num, 1, 2, false, sloppy_parsing, out num16);
								}
								else if (num3 == 1)
								{
									num14 = DateTime._ParseNumber(s, num, 1, 2, true, sloppy_parsing, out num16);
								}
								else
								{
									num14 = DateTime._ParseNumber(s, num, 1, 2, true, true, out num16);
									num += num16;
									if (num16 < 0)
									{
										return false;
									}
									num16 = 0;
									if ((num < s.Length && char.IsDigit(s[num])) || DateTime._ParseTimeSeparator(s, num, dfi, exact, out num16))
									{
										num += num16;
										num15 = DateTime._ParseNumber(s, num, 1, 2, true, sloppy_parsing, out num16);
										if (num16 < 0)
										{
											return false;
										}
									}
									else
									{
										if (!flag3)
										{
											return false;
										}
										num16 = 0;
									}
								}
								break;
							}
							break;
						case 'K':
							if (s[num] == 'Z')
							{
								num++;
								flag = true;
							}
							else if (s[num] == '+' || s[num] == '-')
							{
								if (num13 != -1)
								{
									return false;
								}
								if (s[num] == '+')
								{
									num13 = 0;
								}
								else if (s[num] == '-')
								{
									num13 = 1;
								}
								num++;
								num14 = DateTime._ParseNumber(s, num, 0, 2, true, sloppy_parsing, out num16);
								num += num16;
								if (num16 < 0)
								{
									return false;
								}
								if (char.IsDigit(s[num]))
								{
									num16 = 0;
								}
								else if (!DateTime._ParseString(s, num, 0, dfi.TimeSeparator, out num16))
								{
									return false;
								}
								num += num16;
								num15 = DateTime._ParseNumber(s, num, 0, 2, true, sloppy_parsing, out num16);
								num3 = 2;
								if (num16 < 0)
								{
									return false;
								}
							}
							break;
						case 'M':
							if (num6 != -1)
							{
								return false;
							}
							if (flag3)
							{
								num16 = -1;
								if (num3 == 0 || num3 == 3)
								{
									num6 = DateTime._ParseNumber(s, num, 1, 2, false, sloppy_parsing, out num16);
								}
								if (num3 > 1 && num16 == -1)
								{
									num6 = DateTime._ParseEnum(s, num, dfi.RawMonthNames, invariantInfo.RawMonthNames, exact, out num16) + 1;
								}
								if (num3 > 1 && num16 == -1)
								{
									num6 = DateTime._ParseEnum(s, num, dfi.RawAbbreviatedMonthNames, invariantInfo.RawAbbreviatedMonthNames, exact, out num16) + 1;
								}
							}
							else if (num3 == 0)
							{
								num6 = DateTime._ParseNumber(s, num, 1, 2, false, sloppy_parsing, out num16);
							}
							else if (num3 == 1)
							{
								num6 = DateTime._ParseNumber(s, num, 1, 2, true, sloppy_parsing, out num16);
							}
							else if (num3 == 2)
							{
								num6 = DateTime._ParseEnum(s, num, dfi.RawAbbreviatedMonthNames, invariantInfo.RawAbbreviatedMonthNames, exact, out num16) + 1;
							}
							else
							{
								num6 = DateTime._ParseEnum(s, num, dfi.RawMonthNames, invariantInfo.RawMonthNames, exact, out num16) + 1;
							}
							break;
						}
						IL_DF3:
						if (num16 < 0)
						{
							return false;
						}
						num += num16;
						if (!exact && !flag3)
						{
							c = text2[num2];
							if (c == 'F' || c == 'f' || c == 'm' || c == 's' || c == 'z')
							{
								if (s.Length > num && s[num] == 'Z' && (num2 + 1 == text2.Length || text2[num2 + 1] != 'Z'))
								{
									flag = true;
									num++;
								}
							}
						}
						num2 = num2 + num3 + 1;
						num3 = 0;
						continue;
						IL_A82:
						if (num3 > 6 || num11 != -1.0)
						{
							return false;
						}
						double num20 = (double)DateTime._ParseNumber(s, num, 0, num3 + 1, leadingzero, sloppy_parsing, out num16);
						if (num16 == -1)
						{
							return false;
						}
						num11 = num20 / Math.Pow(10.0, (double)num16);
						goto IL_DF3;
					}
				}
			}
			goto IL_EA9;
		}

		// Token: 0x0600103F RID: 4159 RVA: 0x0004260C File Offset: 0x0004080C
		public static DateTime ParseExact(string s, string format, IFormatProvider provider, DateTimeStyles style)
		{
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}
			return DateTime.ParseExact(s, new string[]
			{
				format
			}, provider, style);
		}

		// Token: 0x06001040 RID: 4160 RVA: 0x00042640 File Offset: 0x00040840
		public static DateTime ParseExact(string s, string[] formats, IFormatProvider provider, DateTimeStyles style)
		{
			DateTimeFormatInfo instance = DateTimeFormatInfo.GetInstance(provider);
			DateTime.CheckStyle(style);
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			if (formats == null)
			{
				throw new ArgumentNullException("formats");
			}
			if (formats.Length == 0)
			{
				throw new FormatException("Format specifier was invalid.");
			}
			bool flag = false;
			Exception ex = null;
			DateTime result;
			if (!DateTime.ParseExact(s, formats, instance, style, out result, true, ref flag, true, ref ex))
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x06001041 RID: 4161 RVA: 0x000426AC File Offset: 0x000408AC
		private static void CheckStyle(DateTimeStyles style)
		{
			if ((style & DateTimeStyles.RoundtripKind) != DateTimeStyles.None && ((style & DateTimeStyles.AdjustToUniversal) != DateTimeStyles.None || (style & DateTimeStyles.AssumeLocal) != DateTimeStyles.None || (style & DateTimeStyles.AssumeUniversal) != DateTimeStyles.None))
			{
				throw new ArgumentException("The DateTimeStyles value RoundtripKind cannot be used with the values AssumeLocal, Asersal or AdjustToUniversal.", "style");
			}
			if ((style & DateTimeStyles.AssumeUniversal) != DateTimeStyles.None && (style & DateTimeStyles.AssumeLocal) != DateTimeStyles.None)
			{
				throw new ArgumentException("The DateTimeStyles values AssumeLocal and AssumeUniversal cannot be used together.", "style");
			}
		}

		// Token: 0x06001042 RID: 4162 RVA: 0x00042714 File Offset: 0x00040914
		public static bool TryParse(string s, out DateTime result)
		{
			if (s != null)
			{
				try
				{
					Exception ex = null;
					DateTimeOffset dateTimeOffset;
					return DateTime.CoreParse(s, null, DateTimeStyles.AllowWhiteSpaces, out result, out dateTimeOffset, false, ref ex);
				}
				catch
				{
				}
			}
			result = DateTime.MinValue;
			return false;
		}

		// Token: 0x06001043 RID: 4163 RVA: 0x00042774 File Offset: 0x00040974
		public static bool TryParse(string s, IFormatProvider provider, DateTimeStyles styles, out DateTime result)
		{
			if (s != null)
			{
				try
				{
					Exception ex = null;
					DateTimeOffset dateTimeOffset;
					return DateTime.CoreParse(s, provider, styles, out result, out dateTimeOffset, false, ref ex);
				}
				catch
				{
				}
			}
			result = DateTime.MinValue;
			return false;
		}

		// Token: 0x06001044 RID: 4164 RVA: 0x000427D4 File Offset: 0x000409D4
		public static bool TryParseExact(string s, string format, IFormatProvider provider, DateTimeStyles style, out DateTime result)
		{
			return DateTime.TryParseExact(s, new string[]
			{
				format
			}, provider, style, out result);
		}

		// Token: 0x06001045 RID: 4165 RVA: 0x000427F8 File Offset: 0x000409F8
		public static bool TryParseExact(string s, string[] formats, IFormatProvider provider, DateTimeStyles style, out DateTime result)
		{
			bool result2;
			try
			{
				DateTimeFormatInfo instance = DateTimeFormatInfo.GetInstance(provider);
				bool flag = false;
				Exception ex = null;
				result2 = DateTime.ParseExact(s, formats, instance, style, out result, true, ref flag, false, ref ex);
			}
			catch
			{
				result = DateTime.MinValue;
				result2 = false;
			}
			return result2;
		}

		// Token: 0x06001046 RID: 4166 RVA: 0x00042864 File Offset: 0x00040A64
		private static bool ParseExact(string s, string[] formats, DateTimeFormatInfo dfi, DateTimeStyles style, out DateTime ret, bool exact, ref bool longYear, bool setExceptionOnError, ref Exception exception)
		{
			bool flag = false;
			for (int i = 0; i < formats.Length; i++)
			{
				string text = formats[i];
				if (text == null || text == string.Empty)
				{
					break;
				}
				DateTime dateTime;
				DateTimeOffset dateTimeOffset;
				if (DateTime._DoParse(s, formats[i], null, exact, out dateTime, out dateTimeOffset, dfi, style, false, ref flag, ref longYear))
				{
					ret = dateTime;
					return true;
				}
			}
			if (setExceptionOnError)
			{
				exception = new FormatException("Invalid format string");
			}
			ret = DateTime.MinValue;
			return false;
		}

		// Token: 0x06001047 RID: 4167 RVA: 0x000428F0 File Offset: 0x00040AF0
		public TimeSpan Subtract(DateTime value)
		{
			return new TimeSpan(this.ticks.Ticks) - value.ticks;
		}

		// Token: 0x06001048 RID: 4168 RVA: 0x00042910 File Offset: 0x00040B10
		public DateTime Subtract(TimeSpan value)
		{
			TimeSpan value2 = new TimeSpan(this.ticks.Ticks) - value;
			return new DateTime(true, value2)
			{
				kind = this.kind
			};
		}

		// Token: 0x06001049 RID: 4169 RVA: 0x0004294C File Offset: 0x00040B4C
		public long ToFileTime()
		{
			DateTime dateTime = this.ToUniversalTime();
			if (dateTime.Ticks < 504911232000000000L)
			{
				throw new ArgumentOutOfRangeException("file time is not valid");
			}
			return dateTime.Ticks - 504911232000000000L;
		}

		// Token: 0x0600104A RID: 4170 RVA: 0x00042994 File Offset: 0x00040B94
		public long ToFileTimeUtc()
		{
			if (this.Ticks < 504911232000000000L)
			{
				throw new ArgumentOutOfRangeException("file time is not valid");
			}
			return this.Ticks - 504911232000000000L;
		}

		// Token: 0x0600104B RID: 4171 RVA: 0x000429C8 File Offset: 0x00040BC8
		public string ToLongDateString()
		{
			return this.ToString("D");
		}

		// Token: 0x0600104C RID: 4172 RVA: 0x000429D8 File Offset: 0x00040BD8
		public string ToLongTimeString()
		{
			return this.ToString("T");
		}

		// Token: 0x0600104D RID: 4173 RVA: 0x000429E8 File Offset: 0x00040BE8
		public double ToOADate()
		{
			long num = this.Ticks;
			if (num == 0L)
			{
				return 0.0;
			}
			if (num < 31242239136000000L)
			{
				return -657434.999;
			}
			TimeSpan timeSpan = new TimeSpan(this.Ticks - 599264352000000000L);
			double num2 = timeSpan.TotalDays;
			if (num < 599264352000000000L)
			{
				double num3 = Math.Ceiling(num2);
				num2 = num3 - 2.0 - (num2 - num3);
			}
			else if (num2 >= 2958466.0)
			{
				num2 = 2958465.99999999;
			}
			return num2;
		}

		// Token: 0x0600104E RID: 4174 RVA: 0x00042A8C File Offset: 0x00040C8C
		public string ToShortDateString()
		{
			return this.ToString("d");
		}

		// Token: 0x0600104F RID: 4175 RVA: 0x00042A9C File Offset: 0x00040C9C
		public string ToShortTimeString()
		{
			return this.ToString("t");
		}

		// Token: 0x06001050 RID: 4176 RVA: 0x00042AAC File Offset: 0x00040CAC
		public override string ToString()
		{
			return this.ToString("G", null);
		}

		// Token: 0x06001051 RID: 4177 RVA: 0x00042ABC File Offset: 0x00040CBC
		public string ToString(IFormatProvider provider)
		{
			return this.ToString(null, provider);
		}

		// Token: 0x06001052 RID: 4178 RVA: 0x00042AC8 File Offset: 0x00040CC8
		public string ToString(string format)
		{
			return this.ToString(format, null);
		}

		// Token: 0x06001053 RID: 4179 RVA: 0x00042AD4 File Offset: 0x00040CD4
		public string ToString(string format, IFormatProvider provider)
		{
			DateTimeFormatInfo instance = DateTimeFormatInfo.GetInstance(provider);
			if (format == null || format == string.Empty)
			{
				format = "G";
			}
			bool flag = false;
			bool flag2 = false;
			if (format.Length == 1)
			{
				char c = format[0];
				format = DateTimeUtils.GetStandardPattern(c, instance, out flag, out flag2);
				if (c == 'U')
				{
					return DateTimeUtils.ToString(this.ToUniversalTime(), format, instance);
				}
				if (format == null)
				{
					throw new FormatException("format is not one of the format specifier characters defined for DateTimeFormatInfo");
				}
			}
			return DateTimeUtils.ToString(this, format, instance);
		}

		// Token: 0x06001054 RID: 4180 RVA: 0x00042B60 File Offset: 0x00040D60
		public DateTime ToLocalTime()
		{
			return TimeZone.CurrentTimeZone.ToLocalTime(this);
		}

		// Token: 0x06001055 RID: 4181 RVA: 0x00042B74 File Offset: 0x00040D74
		public DateTime ToUniversalTime()
		{
			return TimeZone.CurrentTimeZone.ToUniversalTime(this);
		}

		// Token: 0x06001056 RID: 4182 RVA: 0x00042B88 File Offset: 0x00040D88
		public static DateTime operator +(DateTime d, TimeSpan t)
		{
			return new DateTime(true, d.ticks + t)
			{
				kind = d.kind
			};
		}

		// Token: 0x06001057 RID: 4183 RVA: 0x00042BBC File Offset: 0x00040DBC
		public static bool operator ==(DateTime d1, DateTime d2)
		{
			return d1.ticks == d2.ticks;
		}

		// Token: 0x06001058 RID: 4184 RVA: 0x00042BD4 File Offset: 0x00040DD4
		public static bool operator >(DateTime t1, DateTime t2)
		{
			return t1.ticks > t2.ticks;
		}

		// Token: 0x06001059 RID: 4185 RVA: 0x00042BEC File Offset: 0x00040DEC
		public static bool operator >=(DateTime t1, DateTime t2)
		{
			return t1.ticks >= t2.ticks;
		}

		// Token: 0x0600105A RID: 4186 RVA: 0x00042C04 File Offset: 0x00040E04
		public static bool operator !=(DateTime d1, DateTime d2)
		{
			return d1.ticks != d2.ticks;
		}

		// Token: 0x0600105B RID: 4187 RVA: 0x00042C1C File Offset: 0x00040E1C
		public static bool operator <(DateTime t1, DateTime t2)
		{
			return t1.ticks < t2.ticks;
		}

		// Token: 0x0600105C RID: 4188 RVA: 0x00042C34 File Offset: 0x00040E34
		public static bool operator <=(DateTime t1, DateTime t2)
		{
			return t1.ticks <= t2.ticks;
		}

		// Token: 0x0600105D RID: 4189 RVA: 0x00042C4C File Offset: 0x00040E4C
		public static TimeSpan operator -(DateTime d1, DateTime d2)
		{
			return new TimeSpan((d1.ticks - d2.ticks).Ticks);
		}

		// Token: 0x0600105E RID: 4190 RVA: 0x00042C7C File Offset: 0x00040E7C
		public static DateTime operator -(DateTime d, TimeSpan t)
		{
			return new DateTime(true, d.ticks - t)
			{
				kind = d.kind
			};
		}

		// Token: 0x04000494 RID: 1172
		private const int dp400 = 146097;

		// Token: 0x04000495 RID: 1173
		private const int dp100 = 36524;

		// Token: 0x04000496 RID: 1174
		private const int dp4 = 1461;

		// Token: 0x04000497 RID: 1175
		private const long w32file_epoch = 504911232000000000L;

		// Token: 0x04000498 RID: 1176
		private const long MAX_VALUE_TICKS = 3155378975999999999L;

		// Token: 0x04000499 RID: 1177
		internal const long UnixEpoch = 621355968000000000L;

		// Token: 0x0400049A RID: 1178
		private const long ticks18991230 = 599264352000000000L;

		// Token: 0x0400049B RID: 1179
		private const double OAMinValue = -657435.0;

		// Token: 0x0400049C RID: 1180
		private const double OAMaxValue = 2958466.0;

		// Token: 0x0400049D RID: 1181
		private const string formatExceptionMessage = "String was not recognized as a valid DateTime.";

		// Token: 0x0400049E RID: 1182
		private TimeSpan ticks;

		// Token: 0x0400049F RID: 1183
		private DateTimeKind kind;

		// Token: 0x040004A0 RID: 1184
		public static readonly DateTime MaxValue = new DateTime(false, new TimeSpan(3155378975999999999L));

		// Token: 0x040004A1 RID: 1185
		public static readonly DateTime MinValue = new DateTime(false, new TimeSpan(0L));

		// Token: 0x040004A2 RID: 1186
		private static readonly string[] ParseTimeFormats = new string[]
		{
			"H:m:s.fffffffzzz",
			"H:m:s.fffffff",
			"H:m:s tt zzz",
			"H:m:szzz",
			"H:m:s",
			"H:mzzz",
			"H:m",
			"H tt",
			"H'時'm'分's'秒'"
		};

		// Token: 0x040004A3 RID: 1187
		private static readonly string[] ParseYearDayMonthFormats = new string[]
		{
			"yyyy/M/dT",
			"M/yyyy/dT",
			"yyyy'年'M'月'd'日",
			"yyyy/d/MMMM",
			"yyyy/MMM/d",
			"d/MMMM/yyyy",
			"MMM/d/yyyy",
			"d/yyyy/MMMM",
			"MMM/yyyy/d",
			"yy/d/M"
		};

		// Token: 0x040004A4 RID: 1188
		private static readonly string[] ParseYearMonthDayFormats = new string[]
		{
			"yyyy/M/dT",
			"M/yyyy/dT",
			"yyyy'年'M'月'd'日",
			"yyyy/MMMM/d",
			"yyyy/d/MMM",
			"MMMM/d/yyyy",
			"d/MMM/yyyy",
			"MMMM/yyyy/d",
			"d/yyyy/MMM",
			"yy/MMMM/d",
			"yy/d/MMM",
			"MMM/yy/d"
		};

		// Token: 0x040004A5 RID: 1189
		private static readonly string[] ParseDayMonthYearFormats = new string[]
		{
			"yyyy/M/dT",
			"M/yyyy/dT",
			"yyyy'年'M'月'd'日",
			"yyyy/MMMM/d",
			"yyyy/d/MMM",
			"d/MMMM/yyyy",
			"MMM/d/yyyy",
			"MMMM/yyyy/d",
			"d/yyyy/MMM",
			"d/MMMM/yy",
			"yy/MMM/d",
			"d/yy/MMM",
			"yy/d/MMM",
			"MMM/d/yy",
			"MMM/yy/d"
		};

		// Token: 0x040004A6 RID: 1190
		private static readonly string[] ParseMonthDayYearFormats = new string[]
		{
			"yyyy/M/dT",
			"M/yyyy/dT",
			"yyyy'年'M'月'd'日",
			"yyyy/MMMM/d",
			"yyyy/d/MMM",
			"MMMM/d/yyyy",
			"d/MMM/yyyy",
			"MMMM/yyyy/d",
			"d/yyyy/MMM",
			"MMMM/d/yy",
			"MMM/yy/d",
			"d/MMM/yy",
			"yy/MMM/d",
			"d/yy/MMM",
			"yy/d/MMM"
		};

		// Token: 0x040004A7 RID: 1191
		private static readonly string[] MonthDayShortFormats = new string[]
		{
			"MMMM/d",
			"d/MMM",
			"yyyy/MMMM"
		};

		// Token: 0x040004A8 RID: 1192
		private static readonly string[] DayMonthShortFormats = new string[]
		{
			"d/MMMM",
			"MMM/yy",
			"yyyy/MMMM"
		};

		// Token: 0x040004A9 RID: 1193
		private static readonly int[] daysmonth = new int[]
		{
			0,
			31,
			28,
			31,
			30,
			31,
			30,
			31,
			31,
			30,
			31,
			30,
			31
		};

		// Token: 0x040004AA RID: 1194
		private static readonly int[] daysmonthleap = new int[]
		{
			0,
			31,
			29,
			31,
			30,
			31,
			30,
			31,
			31,
			30,
			31,
			30,
			31
		};

		// Token: 0x040004AB RID: 1195
		private static object to_local_time_span_object;

		// Token: 0x040004AC RID: 1196
		private static long last_now;

		// Token: 0x0200011F RID: 287
		private enum Which
		{
			// Token: 0x040004AE RID: 1198
			Day,
			// Token: 0x040004AF RID: 1199
			DayYear,
			// Token: 0x040004B0 RID: 1200
			Month,
			// Token: 0x040004B1 RID: 1201
			Year
		}
	}
}
