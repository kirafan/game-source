﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration.Assemblies;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Remoting.Messaging;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Security.Principal;
using System.Threading;
using Mono.Security;

namespace System
{
	// Token: 0x020000F7 RID: 247
	[ComDefaultInterface(typeof(_AppDomain))]
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	public sealed class AppDomain : MarshalByRefObject, _AppDomain, IEvidenceFactory
	{
		// Token: 0x06000C86 RID: 3206 RVA: 0x00038C74 File Offset: 0x00036E74
		private AppDomain()
		{
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x06000C87 RID: 3207 RVA: 0x00038C7C File Offset: 0x00036E7C
		// (remove) Token: 0x06000C88 RID: 3208 RVA: 0x00038C98 File Offset: 0x00036E98
		[method: PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public event AssemblyLoadEventHandler AssemblyLoad;

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x06000C89 RID: 3209 RVA: 0x00038CB4 File Offset: 0x00036EB4
		// (remove) Token: 0x06000C8A RID: 3210 RVA: 0x00038CD0 File Offset: 0x00036ED0
		[method: PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public event ResolveEventHandler AssemblyResolve;

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x06000C8B RID: 3211 RVA: 0x00038CEC File Offset: 0x00036EEC
		// (remove) Token: 0x06000C8C RID: 3212 RVA: 0x00038D08 File Offset: 0x00036F08
		[method: PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public event EventHandler DomainUnload;

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x06000C8D RID: 3213 RVA: 0x00038D24 File Offset: 0x00036F24
		// (remove) Token: 0x06000C8E RID: 3214 RVA: 0x00038D40 File Offset: 0x00036F40
		[method: PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public event EventHandler ProcessExit;

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x06000C8F RID: 3215 RVA: 0x00038D5C File Offset: 0x00036F5C
		// (remove) Token: 0x06000C90 RID: 3216 RVA: 0x00038D78 File Offset: 0x00036F78
		[method: PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public event ResolveEventHandler ResourceResolve;

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x06000C91 RID: 3217 RVA: 0x00038D94 File Offset: 0x00036F94
		// (remove) Token: 0x06000C92 RID: 3218 RVA: 0x00038DB0 File Offset: 0x00036FB0
		[method: PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public event ResolveEventHandler TypeResolve;

		// Token: 0x14000009 RID: 9
		// (add) Token: 0x06000C93 RID: 3219 RVA: 0x00038DCC File Offset: 0x00036FCC
		// (remove) Token: 0x06000C94 RID: 3220 RVA: 0x00038DE8 File Offset: 0x00036FE8
		[method: PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public event UnhandledExceptionEventHandler UnhandledException;

		// Token: 0x1400000A RID: 10
		// (add) Token: 0x06000C95 RID: 3221 RVA: 0x00038E04 File Offset: 0x00037004
		// (remove) Token: 0x06000C96 RID: 3222 RVA: 0x00038E20 File Offset: 0x00037020
		public event ResolveEventHandler ReflectionOnlyAssemblyResolve;

		// Token: 0x06000C97 RID: 3223 RVA: 0x00038E3C File Offset: 0x0003703C
		void _AppDomain.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C98 RID: 3224 RVA: 0x00038E44 File Offset: 0x00037044
		void _AppDomain.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C99 RID: 3225 RVA: 0x00038E4C File Offset: 0x0003704C
		void _AppDomain.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C9A RID: 3226 RVA: 0x00038E54 File Offset: 0x00037054
		void _AppDomain.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C9B RID: 3227
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern AppDomainSetup getSetup();

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000C9C RID: 3228 RVA: 0x00038E5C File Offset: 0x0003705C
		private AppDomainSetup SetupInformationNoCopy
		{
			get
			{
				return this.getSetup();
			}
		}

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000C9D RID: 3229 RVA: 0x00038E64 File Offset: 0x00037064
		public AppDomainSetup SetupInformation
		{
			get
			{
				AppDomainSetup setup = this.getSetup();
				return new AppDomainSetup(setup);
			}
		}

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x06000C9E RID: 3230 RVA: 0x00038E80 File Offset: 0x00037080
		[MonoTODO]
		public ApplicationTrust ApplicationTrust
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x06000C9F RID: 3231 RVA: 0x00038E88 File Offset: 0x00037088
		public string BaseDirectory
		{
			get
			{
				string applicationBase = this.SetupInformationNoCopy.ApplicationBase;
				if (SecurityManager.SecurityEnabled && applicationBase != null && applicationBase.Length > 0)
				{
					new FileIOPermission(FileIOPermissionAccess.PathDiscovery, applicationBase).Demand();
				}
				return applicationBase;
			}
		}

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x06000CA0 RID: 3232 RVA: 0x00038ECC File Offset: 0x000370CC
		public string RelativeSearchPath
		{
			get
			{
				string privateBinPath = this.SetupInformationNoCopy.PrivateBinPath;
				if (SecurityManager.SecurityEnabled && privateBinPath != null && privateBinPath.Length > 0)
				{
					new FileIOPermission(FileIOPermissionAccess.PathDiscovery, privateBinPath).Demand();
				}
				return privateBinPath;
			}
		}

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x06000CA1 RID: 3233 RVA: 0x00038F10 File Offset: 0x00037110
		public string DynamicDirectory
		{
			get
			{
				AppDomainSetup setupInformationNoCopy = this.SetupInformationNoCopy;
				if (setupInformationNoCopy.DynamicBase == null)
				{
					return null;
				}
				string text = Path.Combine(setupInformationNoCopy.DynamicBase, setupInformationNoCopy.ApplicationName);
				if (SecurityManager.SecurityEnabled && text != null && text.Length > 0)
				{
					new FileIOPermission(FileIOPermissionAccess.PathDiscovery, text).Demand();
				}
				return text;
			}
		}

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x06000CA2 RID: 3234 RVA: 0x00038F6C File Offset: 0x0003716C
		public bool ShadowCopyFiles
		{
			get
			{
				return this.SetupInformationNoCopy.ShadowCopyFiles == "true";
			}
		}

		// Token: 0x06000CA3 RID: 3235
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string getFriendlyName();

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x06000CA4 RID: 3236 RVA: 0x00038F84 File Offset: 0x00037184
		public string FriendlyName
		{
			get
			{
				return this.getFriendlyName();
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x06000CA5 RID: 3237 RVA: 0x00038F8C File Offset: 0x0003718C
		public Evidence Evidence
		{
			get
			{
				if (this._evidence == null)
				{
					lock (this)
					{
						Assembly entryAssembly = Assembly.GetEntryAssembly();
						if (entryAssembly == null)
						{
							if (this == AppDomain.DefaultDomain)
							{
								return new Evidence();
							}
							this._evidence = AppDomain.DefaultDomain.Evidence;
						}
						else
						{
							this._evidence = Evidence.GetDefaultHostEvidence(entryAssembly);
						}
					}
				}
				return new Evidence(this._evidence);
			}
		}

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x06000CA6 RID: 3238 RVA: 0x00039024 File Offset: 0x00037224
		internal IPrincipal DefaultPrincipal
		{
			get
			{
				if (AppDomain._principal == null)
				{
					switch (this._principalPolicy)
					{
					case PrincipalPolicy.UnauthenticatedPrincipal:
						AppDomain._principal = new GenericPrincipal(new GenericIdentity(string.Empty, string.Empty), null);
						break;
					case PrincipalPolicy.WindowsPrincipal:
						AppDomain._principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
						break;
					}
				}
				return AppDomain._principal;
			}
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x06000CA7 RID: 3239 RVA: 0x00039094 File Offset: 0x00037294
		internal PermissionSet GrantedPermissionSet
		{
			get
			{
				return this._granted;
			}
		}

		// Token: 0x06000CA8 RID: 3240
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AppDomain getCurDomain();

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000CA9 RID: 3241 RVA: 0x0003909C File Offset: 0x0003729C
		public static AppDomain CurrentDomain
		{
			get
			{
				return AppDomain.getCurDomain();
			}
		}

		// Token: 0x06000CAA RID: 3242
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AppDomain getRootDomain();

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000CAB RID: 3243 RVA: 0x000390A4 File Offset: 0x000372A4
		internal static AppDomain DefaultDomain
		{
			get
			{
				if (AppDomain.default_domain == null)
				{
					AppDomain rootDomain = AppDomain.getRootDomain();
					if (rootDomain == AppDomain.CurrentDomain)
					{
						AppDomain.default_domain = rootDomain;
					}
					else
					{
						AppDomain.default_domain = (AppDomain)RemotingServices.GetDomainProxy(rootDomain);
					}
				}
				return AppDomain.default_domain;
			}
		}

		// Token: 0x06000CAC RID: 3244 RVA: 0x000390EC File Offset: 0x000372EC
		[Obsolete("AppDomain.AppendPrivatePath has been deprecated. Please investigate the use of AppDomainSetup.PrivateBinPath instead.")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public void AppendPrivatePath(string path)
		{
			if (path == null || path.Length == 0)
			{
				return;
			}
			AppDomainSetup setupInformationNoCopy = this.SetupInformationNoCopy;
			string text = setupInformationNoCopy.PrivateBinPath;
			if (text == null || text.Length == 0)
			{
				setupInformationNoCopy.PrivateBinPath = path;
				return;
			}
			text = text.Trim();
			if (text[text.Length - 1] != Path.PathSeparator)
			{
				text += Path.PathSeparator;
			}
			setupInformationNoCopy.PrivateBinPath = text + path;
		}

		// Token: 0x06000CAD RID: 3245 RVA: 0x00039170 File Offset: 0x00037370
		[Obsolete("AppDomain.ClearPrivatePath has been deprecated. Please investigate the use of AppDomainSetup.PrivateBinPath instead.")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public void ClearPrivatePath()
		{
			this.SetupInformationNoCopy.PrivateBinPath = string.Empty;
		}

		// Token: 0x06000CAE RID: 3246 RVA: 0x00039184 File Offset: 0x00037384
		[Obsolete("Use AppDomainSetup.ShadowCopyDirectories")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public void ClearShadowCopyPath()
		{
			this.SetupInformationNoCopy.ShadowCopyDirectories = string.Empty;
		}

		// Token: 0x06000CAF RID: 3247 RVA: 0x00039198 File Offset: 0x00037398
		public ObjectHandle CreateComInstanceFrom(string assemblyName, string typeName)
		{
			return Activator.CreateComInstanceFrom(assemblyName, typeName);
		}

		// Token: 0x06000CB0 RID: 3248 RVA: 0x000391A4 File Offset: 0x000373A4
		public ObjectHandle CreateComInstanceFrom(string assemblyFile, string typeName, byte[] hashValue, AssemblyHashAlgorithm hashAlgorithm)
		{
			return Activator.CreateComInstanceFrom(assemblyFile, typeName, hashValue, hashAlgorithm);
		}

		// Token: 0x06000CB1 RID: 3249 RVA: 0x000391B0 File Offset: 0x000373B0
		public ObjectHandle CreateInstance(string assemblyName, string typeName)
		{
			if (assemblyName == null)
			{
				throw new ArgumentNullException("assemblyName");
			}
			return Activator.CreateInstance(assemblyName, typeName);
		}

		// Token: 0x06000CB2 RID: 3250 RVA: 0x000391CC File Offset: 0x000373CC
		public ObjectHandle CreateInstance(string assemblyName, string typeName, object[] activationAttributes)
		{
			if (assemblyName == null)
			{
				throw new ArgumentNullException("assemblyName");
			}
			return Activator.CreateInstance(assemblyName, typeName, activationAttributes);
		}

		// Token: 0x06000CB3 RID: 3251 RVA: 0x000391E8 File Offset: 0x000373E8
		public ObjectHandle CreateInstance(string assemblyName, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityAttributes)
		{
			if (assemblyName == null)
			{
				throw new ArgumentNullException("assemblyName");
			}
			return Activator.CreateInstance(assemblyName, typeName, ignoreCase, bindingAttr, binder, args, culture, activationAttributes, securityAttributes);
		}

		// Token: 0x06000CB4 RID: 3252 RVA: 0x0003921C File Offset: 0x0003741C
		public object CreateInstanceAndUnwrap(string assemblyName, string typeName)
		{
			ObjectHandle objectHandle = this.CreateInstance(assemblyName, typeName);
			return (objectHandle == null) ? null : objectHandle.Unwrap();
		}

		// Token: 0x06000CB5 RID: 3253 RVA: 0x00039244 File Offset: 0x00037444
		public object CreateInstanceAndUnwrap(string assemblyName, string typeName, object[] activationAttributes)
		{
			ObjectHandle objectHandle = this.CreateInstance(assemblyName, typeName, activationAttributes);
			return (objectHandle == null) ? null : objectHandle.Unwrap();
		}

		// Token: 0x06000CB6 RID: 3254 RVA: 0x00039270 File Offset: 0x00037470
		public object CreateInstanceAndUnwrap(string assemblyName, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityAttributes)
		{
			ObjectHandle objectHandle = this.CreateInstance(assemblyName, typeName, ignoreCase, bindingAttr, binder, args, culture, activationAttributes, securityAttributes);
			return (objectHandle == null) ? null : objectHandle.Unwrap();
		}

		// Token: 0x06000CB7 RID: 3255 RVA: 0x000392A8 File Offset: 0x000374A8
		public ObjectHandle CreateInstanceFrom(string assemblyFile, string typeName)
		{
			if (assemblyFile == null)
			{
				throw new ArgumentNullException("assemblyFile");
			}
			return Activator.CreateInstanceFrom(assemblyFile, typeName);
		}

		// Token: 0x06000CB8 RID: 3256 RVA: 0x000392C4 File Offset: 0x000374C4
		public ObjectHandle CreateInstanceFrom(string assemblyFile, string typeName, object[] activationAttributes)
		{
			if (assemblyFile == null)
			{
				throw new ArgumentNullException("assemblyFile");
			}
			return Activator.CreateInstanceFrom(assemblyFile, typeName, activationAttributes);
		}

		// Token: 0x06000CB9 RID: 3257 RVA: 0x000392E0 File Offset: 0x000374E0
		public ObjectHandle CreateInstanceFrom(string assemblyFile, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityAttributes)
		{
			if (assemblyFile == null)
			{
				throw new ArgumentNullException("assemblyFile");
			}
			return Activator.CreateInstanceFrom(assemblyFile, typeName, ignoreCase, bindingAttr, binder, args, culture, activationAttributes, securityAttributes);
		}

		// Token: 0x06000CBA RID: 3258 RVA: 0x00039314 File Offset: 0x00037514
		public object CreateInstanceFromAndUnwrap(string assemblyName, string typeName)
		{
			ObjectHandle objectHandle = this.CreateInstanceFrom(assemblyName, typeName);
			return (objectHandle == null) ? null : objectHandle.Unwrap();
		}

		// Token: 0x06000CBB RID: 3259 RVA: 0x0003933C File Offset: 0x0003753C
		public object CreateInstanceFromAndUnwrap(string assemblyName, string typeName, object[] activationAttributes)
		{
			ObjectHandle objectHandle = this.CreateInstanceFrom(assemblyName, typeName, activationAttributes);
			return (objectHandle == null) ? null : objectHandle.Unwrap();
		}

		// Token: 0x06000CBC RID: 3260 RVA: 0x00039368 File Offset: 0x00037568
		public object CreateInstanceFromAndUnwrap(string assemblyName, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityAttributes)
		{
			ObjectHandle objectHandle = this.CreateInstanceFrom(assemblyName, typeName, ignoreCase, bindingAttr, binder, args, culture, activationAttributes, securityAttributes);
			return (objectHandle == null) ? null : objectHandle.Unwrap();
		}

		// Token: 0x06000CBD RID: 3261 RVA: 0x000393A0 File Offset: 0x000375A0
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access)
		{
			return this.DefineDynamicAssembly(name, access, null, null, null, null, null, false);
		}

		// Token: 0x06000CBE RID: 3262 RVA: 0x000393BC File Offset: 0x000375BC
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, Evidence evidence)
		{
			return this.DefineDynamicAssembly(name, access, null, evidence, null, null, null, false);
		}

		// Token: 0x06000CBF RID: 3263 RVA: 0x000393D8 File Offset: 0x000375D8
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir)
		{
			return this.DefineDynamicAssembly(name, access, dir, null, null, null, null, false);
		}

		// Token: 0x06000CC0 RID: 3264 RVA: 0x000393F4 File Offset: 0x000375F4
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir, Evidence evidence)
		{
			return this.DefineDynamicAssembly(name, access, dir, evidence, null, null, null, false);
		}

		// Token: 0x06000CC1 RID: 3265 RVA: 0x00039410 File Offset: 0x00037610
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions)
		{
			return this.DefineDynamicAssembly(name, access, null, null, requiredPermissions, optionalPermissions, refusedPermissions, false);
		}

		// Token: 0x06000CC2 RID: 3266 RVA: 0x00039430 File Offset: 0x00037630
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, Evidence evidence, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions)
		{
			return this.DefineDynamicAssembly(name, access, null, evidence, requiredPermissions, optionalPermissions, refusedPermissions, false);
		}

		// Token: 0x06000CC3 RID: 3267 RVA: 0x00039450 File Offset: 0x00037650
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions)
		{
			return this.DefineDynamicAssembly(name, access, dir, null, requiredPermissions, optionalPermissions, refusedPermissions, false);
		}

		// Token: 0x06000CC4 RID: 3268 RVA: 0x00039470 File Offset: 0x00037670
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir, Evidence evidence, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions)
		{
			return this.DefineDynamicAssembly(name, access, dir, evidence, requiredPermissions, optionalPermissions, refusedPermissions, false);
		}

		// Token: 0x06000CC5 RID: 3269 RVA: 0x00039490 File Offset: 0x00037690
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir, Evidence evidence, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions, bool isSynchronized)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			AppDomain.ValidateAssemblyName(name.Name);
			AssemblyBuilder assemblyBuilder = new AssemblyBuilder(name, dir, access, false);
			assemblyBuilder.AddPermissionRequests(requiredPermissions, optionalPermissions, refusedPermissions);
			return assemblyBuilder;
		}

		// Token: 0x06000CC6 RID: 3270 RVA: 0x000394D0 File Offset: 0x000376D0
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir, Evidence evidence, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions, bool isSynchronized, IEnumerable<CustomAttributeBuilder> assemblyAttributes)
		{
			AssemblyBuilder assemblyBuilder = this.DefineDynamicAssembly(name, access, dir, evidence, requiredPermissions, optionalPermissions, refusedPermissions, isSynchronized);
			if (assemblyAttributes != null)
			{
				foreach (CustomAttributeBuilder customAttribute in assemblyAttributes)
				{
					assemblyBuilder.SetCustomAttribute(customAttribute);
				}
			}
			return assemblyBuilder;
		}

		// Token: 0x06000CC7 RID: 3271 RVA: 0x0003954C File Offset: 0x0003774C
		public AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, IEnumerable<CustomAttributeBuilder> assemblyAttributes)
		{
			return this.DefineDynamicAssembly(name, access, null, null, null, null, null, false, assemblyAttributes);
		}

		// Token: 0x06000CC8 RID: 3272 RVA: 0x00039568 File Offset: 0x00037768
		internal AssemblyBuilder DefineInternalDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access)
		{
			return new AssemblyBuilder(name, null, access, true);
		}

		// Token: 0x06000CC9 RID: 3273 RVA: 0x00039574 File Offset: 0x00037774
		public void DoCallBack(CrossAppDomainDelegate callBackDelegate)
		{
			if (callBackDelegate != null)
			{
				callBackDelegate();
			}
		}

		// Token: 0x06000CCA RID: 3274 RVA: 0x00039584 File Offset: 0x00037784
		public int ExecuteAssembly(string assemblyFile)
		{
			return this.ExecuteAssembly(assemblyFile, null, null);
		}

		// Token: 0x06000CCB RID: 3275 RVA: 0x00039590 File Offset: 0x00037790
		public int ExecuteAssembly(string assemblyFile, Evidence assemblySecurity)
		{
			return this.ExecuteAssembly(assemblyFile, assemblySecurity, null);
		}

		// Token: 0x06000CCC RID: 3276 RVA: 0x0003959C File Offset: 0x0003779C
		public int ExecuteAssembly(string assemblyFile, Evidence assemblySecurity, string[] args)
		{
			Assembly a = Assembly.LoadFrom(assemblyFile, assemblySecurity);
			return this.ExecuteAssemblyInternal(a, args);
		}

		// Token: 0x06000CCD RID: 3277 RVA: 0x000395BC File Offset: 0x000377BC
		public int ExecuteAssembly(string assemblyFile, Evidence assemblySecurity, string[] args, byte[] hashValue, AssemblyHashAlgorithm hashAlgorithm)
		{
			Assembly a = Assembly.LoadFrom(assemblyFile, assemblySecurity, hashValue, hashAlgorithm);
			return this.ExecuteAssemblyInternal(a, args);
		}

		// Token: 0x06000CCE RID: 3278 RVA: 0x000395E0 File Offset: 0x000377E0
		private int ExecuteAssemblyInternal(Assembly a, string[] args)
		{
			if (a.EntryPoint == null)
			{
				throw new MissingMethodException("Entry point not found in assembly '" + a.FullName + "'.");
			}
			return this.ExecuteAssembly(a, args);
		}

		// Token: 0x06000CCF RID: 3279
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int ExecuteAssembly(Assembly a, string[] args);

		// Token: 0x06000CD0 RID: 3280
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Assembly[] GetAssemblies(bool refOnly);

		// Token: 0x06000CD1 RID: 3281 RVA: 0x0003961C File Offset: 0x0003781C
		public Assembly[] GetAssemblies()
		{
			return this.GetAssemblies(false);
		}

		// Token: 0x06000CD2 RID: 3282
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern object GetData(string name);

		// Token: 0x06000CD3 RID: 3283 RVA: 0x00039628 File Offset: 0x00037828
		public new Type GetType()
		{
			return base.GetType();
		}

		// Token: 0x06000CD4 RID: 3284 RVA: 0x00039630 File Offset: 0x00037830
		public override object InitializeLifetimeService()
		{
			return null;
		}

		// Token: 0x06000CD5 RID: 3285
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Assembly LoadAssembly(string assemblyRef, Evidence securityEvidence, bool refOnly);

		// Token: 0x06000CD6 RID: 3286 RVA: 0x00039634 File Offset: 0x00037834
		public Assembly Load(AssemblyName assemblyRef)
		{
			return this.Load(assemblyRef, null);
		}

		// Token: 0x06000CD7 RID: 3287 RVA: 0x00039640 File Offset: 0x00037840
		internal Assembly LoadSatellite(AssemblyName assemblyRef, bool throwOnError)
		{
			if (assemblyRef == null)
			{
				throw new ArgumentNullException("assemblyRef");
			}
			Assembly assembly = this.LoadAssembly(assemblyRef.FullName, null, false);
			if (assembly == null && throwOnError)
			{
				throw new FileNotFoundException(null, assemblyRef.Name);
			}
			return assembly;
		}

		// Token: 0x06000CD8 RID: 3288 RVA: 0x00039688 File Offset: 0x00037888
		public Assembly Load(AssemblyName assemblyRef, Evidence assemblySecurity)
		{
			if (assemblyRef == null)
			{
				throw new ArgumentNullException("assemblyRef");
			}
			if (assemblyRef.Name == null || assemblyRef.Name.Length == 0)
			{
				if (assemblyRef.CodeBase != null)
				{
					return Assembly.LoadFrom(assemblyRef.CodeBase, assemblySecurity);
				}
				throw new ArgumentException(Locale.GetText("assemblyRef.Name cannot be empty."), "assemblyRef");
			}
			else
			{
				Assembly assembly = this.LoadAssembly(assemblyRef.FullName, assemblySecurity, false);
				if (assembly != null)
				{
					return assembly;
				}
				if (assemblyRef.CodeBase == null)
				{
					throw new FileNotFoundException(null, assemblyRef.Name);
				}
				string text = assemblyRef.CodeBase;
				if (text.ToLower(CultureInfo.InvariantCulture).StartsWith("file://"))
				{
					text = new Uri(text).LocalPath;
				}
				try
				{
					assembly = Assembly.LoadFrom(text, assemblySecurity);
				}
				catch
				{
					throw new FileNotFoundException(null, assemblyRef.Name);
				}
				AssemblyName name = assembly.GetName();
				if (assemblyRef.Name != name.Name)
				{
					throw new FileNotFoundException(null, assemblyRef.Name);
				}
				if (assemblyRef.Version != new Version() && assemblyRef.Version != name.Version)
				{
					throw new FileNotFoundException(null, assemblyRef.Name);
				}
				if (assemblyRef.CultureInfo != null && assemblyRef.CultureInfo.Equals(name))
				{
					throw new FileNotFoundException(null, assemblyRef.Name);
				}
				byte[] publicKeyToken = assemblyRef.GetPublicKeyToken();
				if (publicKeyToken != null)
				{
					byte[] publicKeyToken2 = name.GetPublicKeyToken();
					if (publicKeyToken2 == null || publicKeyToken.Length != publicKeyToken2.Length)
					{
						throw new FileNotFoundException(null, assemblyRef.Name);
					}
					for (int i = publicKeyToken.Length - 1; i >= 0; i--)
					{
						if (publicKeyToken2[i] != publicKeyToken[i])
						{
							throw new FileNotFoundException(null, assemblyRef.Name);
						}
					}
				}
				return assembly;
			}
		}

		// Token: 0x06000CD9 RID: 3289 RVA: 0x0003987C File Offset: 0x00037A7C
		public Assembly Load(string assemblyString)
		{
			return this.Load(assemblyString, null, false);
		}

		// Token: 0x06000CDA RID: 3290 RVA: 0x00039888 File Offset: 0x00037A88
		public Assembly Load(string assemblyString, Evidence assemblySecurity)
		{
			return this.Load(assemblyString, assemblySecurity, false);
		}

		// Token: 0x06000CDB RID: 3291 RVA: 0x00039894 File Offset: 0x00037A94
		internal Assembly Load(string assemblyString, Evidence assemblySecurity, bool refonly)
		{
			if (assemblyString == null)
			{
				throw new ArgumentNullException("assemblyString");
			}
			if (assemblyString.Length == 0)
			{
				throw new ArgumentException("assemblyString cannot have zero length");
			}
			Assembly assembly = this.LoadAssembly(assemblyString, assemblySecurity, refonly);
			if (assembly == null)
			{
				throw new FileNotFoundException(null, assemblyString);
			}
			return assembly;
		}

		// Token: 0x06000CDC RID: 3292 RVA: 0x000398E4 File Offset: 0x00037AE4
		public Assembly Load(byte[] rawAssembly)
		{
			return this.Load(rawAssembly, null, null);
		}

		// Token: 0x06000CDD RID: 3293 RVA: 0x000398F0 File Offset: 0x00037AF0
		public Assembly Load(byte[] rawAssembly, byte[] rawSymbolStore)
		{
			return this.Load(rawAssembly, rawSymbolStore, null);
		}

		// Token: 0x06000CDE RID: 3294
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Assembly LoadAssemblyRaw(byte[] rawAssembly, byte[] rawSymbolStore, Evidence securityEvidence, bool refonly);

		// Token: 0x06000CDF RID: 3295 RVA: 0x000398FC File Offset: 0x00037AFC
		public Assembly Load(byte[] rawAssembly, byte[] rawSymbolStore, Evidence securityEvidence)
		{
			return this.Load(rawAssembly, rawSymbolStore, securityEvidence, false);
		}

		// Token: 0x06000CE0 RID: 3296 RVA: 0x00039908 File Offset: 0x00037B08
		internal Assembly Load(byte[] rawAssembly, byte[] rawSymbolStore, Evidence securityEvidence, bool refonly)
		{
			if (rawAssembly == null)
			{
				throw new ArgumentNullException("rawAssembly");
			}
			Assembly assembly = this.LoadAssemblyRaw(rawAssembly, rawSymbolStore, securityEvidence, refonly);
			assembly.FromByteArray = true;
			return assembly;
		}

		// Token: 0x06000CE1 RID: 3297 RVA: 0x0003993C File Offset: 0x00037B3C
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
		public void SetAppDomainPolicy(PolicyLevel domainPolicy)
		{
			if (domainPolicy == null)
			{
				throw new ArgumentNullException("domainPolicy");
			}
			if (this._granted != null)
			{
				throw new PolicyException(Locale.GetText("An AppDomain policy is already specified."));
			}
			if (this.IsFinalizingForUnload())
			{
				throw new AppDomainUnloadedException();
			}
			PolicyStatement policyStatement = domainPolicy.Resolve(this._evidence);
			this._granted = policyStatement.PermissionSet;
		}

		// Token: 0x06000CE2 RID: 3298 RVA: 0x000399A0 File Offset: 0x00037BA0
		[Obsolete("Use AppDomainSetup.SetCachePath")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public void SetCachePath(string path)
		{
			this.SetupInformationNoCopy.CachePath = path;
		}

		// Token: 0x06000CE3 RID: 3299 RVA: 0x000399B0 File Offset: 0x00037BB0
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public void SetPrincipalPolicy(PrincipalPolicy policy)
		{
			if (this.IsFinalizingForUnload())
			{
				throw new AppDomainUnloadedException();
			}
			this._principalPolicy = policy;
			AppDomain._principal = null;
		}

		// Token: 0x06000CE4 RID: 3300 RVA: 0x000399D0 File Offset: 0x00037BD0
		[Obsolete("Use AppDomainSetup.ShadowCopyFiles")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public void SetShadowCopyFiles()
		{
			this.SetupInformationNoCopy.ShadowCopyFiles = "true";
		}

		// Token: 0x06000CE5 RID: 3301 RVA: 0x000399E4 File Offset: 0x00037BE4
		[Obsolete("Use AppDomainSetup.ShadowCopyDirectories")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public void SetShadowCopyPath(string path)
		{
			this.SetupInformationNoCopy.ShadowCopyDirectories = path;
		}

		// Token: 0x06000CE6 RID: 3302 RVA: 0x000399F4 File Offset: 0x00037BF4
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
		public void SetThreadPrincipal(IPrincipal principal)
		{
			if (principal == null)
			{
				throw new ArgumentNullException("principal");
			}
			if (AppDomain._principal != null)
			{
				throw new PolicyException(Locale.GetText("principal already present."));
			}
			if (this.IsFinalizingForUnload())
			{
				throw new AppDomainUnloadedException();
			}
			AppDomain._principal = principal;
		}

		// Token: 0x06000CE7 RID: 3303
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AppDomain InternalSetDomainByID(int domain_id);

		// Token: 0x06000CE8 RID: 3304
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AppDomain InternalSetDomain(AppDomain context);

		// Token: 0x06000CE9 RID: 3305
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalPushDomainRef(AppDomain domain);

		// Token: 0x06000CEA RID: 3306
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalPushDomainRefByID(int domain_id);

		// Token: 0x06000CEB RID: 3307
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalPopDomainRef();

		// Token: 0x06000CEC RID: 3308
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Context InternalSetContext(Context context);

		// Token: 0x06000CED RID: 3309
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Context InternalGetContext();

		// Token: 0x06000CEE RID: 3310
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Context InternalGetDefaultContext();

		// Token: 0x06000CEF RID: 3311
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string InternalGetProcessGuid(string newguid);

		// Token: 0x06000CF0 RID: 3312 RVA: 0x00039A44 File Offset: 0x00037C44
		internal static object InvokeInDomain(AppDomain domain, MethodInfo method, object obj, object[] args)
		{
			AppDomain currentDomain = AppDomain.CurrentDomain;
			bool flag = false;
			object result;
			try
			{
				AppDomain.InternalPushDomainRef(domain);
				flag = true;
				AppDomain.InternalSetDomain(domain);
				Exception ex;
				object obj2 = ((MonoMethod)method).InternalInvoke(obj, args, out ex);
				if (ex != null)
				{
					throw ex;
				}
				result = obj2;
			}
			finally
			{
				AppDomain.InternalSetDomain(currentDomain);
				if (flag)
				{
					AppDomain.InternalPopDomainRef();
				}
			}
			return result;
		}

		// Token: 0x06000CF1 RID: 3313 RVA: 0x00039AC0 File Offset: 0x00037CC0
		internal static object InvokeInDomainByID(int domain_id, MethodInfo method, object obj, object[] args)
		{
			AppDomain currentDomain = AppDomain.CurrentDomain;
			bool flag = false;
			object result;
			try
			{
				AppDomain.InternalPushDomainRefByID(domain_id);
				flag = true;
				AppDomain.InternalSetDomainByID(domain_id);
				Exception ex;
				object obj2 = ((MonoMethod)method).InternalInvoke(obj, args, out ex);
				if (ex != null)
				{
					throw ex;
				}
				result = obj2;
			}
			finally
			{
				AppDomain.InternalSetDomain(currentDomain);
				if (flag)
				{
					AppDomain.InternalPopDomainRef();
				}
			}
			return result;
		}

		// Token: 0x06000CF2 RID: 3314 RVA: 0x00039B3C File Offset: 0x00037D3C
		internal static string GetProcessGuid()
		{
			if (AppDomain._process_guid == null)
			{
				AppDomain._process_guid = AppDomain.InternalGetProcessGuid(Guid.NewGuid().ToString());
			}
			return AppDomain._process_guid;
		}

		// Token: 0x06000CF3 RID: 3315 RVA: 0x00039B70 File Offset: 0x00037D70
		public static AppDomain CreateDomain(string friendlyName)
		{
			return AppDomain.CreateDomain(friendlyName, null, null);
		}

		// Token: 0x06000CF4 RID: 3316 RVA: 0x00039B7C File Offset: 0x00037D7C
		public static AppDomain CreateDomain(string friendlyName, Evidence securityInfo)
		{
			return AppDomain.CreateDomain(friendlyName, securityInfo, null);
		}

		// Token: 0x06000CF5 RID: 3317
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern AppDomain createDomain(string friendlyName, AppDomainSetup info);

		// Token: 0x06000CF6 RID: 3318 RVA: 0x00039B88 File Offset: 0x00037D88
		[MonoLimitation("Currently it does not allow the setup in the other domain")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public static AppDomain CreateDomain(string friendlyName, Evidence securityInfo, AppDomainSetup info)
		{
			if (friendlyName == null)
			{
				throw new ArgumentNullException("friendlyName");
			}
			AppDomain defaultDomain = AppDomain.DefaultDomain;
			if (info == null)
			{
				if (defaultDomain == null)
				{
					info = new AppDomainSetup();
				}
				else
				{
					info = defaultDomain.SetupInformation;
				}
			}
			else
			{
				info = new AppDomainSetup(info);
			}
			if (defaultDomain != null)
			{
				if (!info.Equals(defaultDomain.SetupInformation))
				{
					if (info.ApplicationBase == null)
					{
						info.ApplicationBase = defaultDomain.SetupInformation.ApplicationBase;
					}
					if (info.ConfigurationFile == null)
					{
						info.ConfigurationFile = Path.GetFileName(defaultDomain.SetupInformation.ConfigurationFile);
					}
				}
			}
			else if (info.ConfigurationFile == null)
			{
				info.ConfigurationFile = "[I don't have a config file]";
			}
			AppDomain appDomain = (AppDomain)RemotingServices.GetDomainProxy(AppDomain.createDomain(friendlyName, info));
			if (securityInfo == null)
			{
				if (defaultDomain == null)
				{
					appDomain._evidence = null;
				}
				else
				{
					appDomain._evidence = defaultDomain.Evidence;
				}
			}
			else
			{
				appDomain._evidence = new Evidence(securityInfo);
			}
			if (info.AppDomainInitializer != null)
			{
				if (!info.AppDomainInitializer.Method.IsStatic)
				{
					throw new ArgumentException("Non-static methods cannot be invoked as an appdomain initializer");
				}
				AppDomain.Loader @object = new AppDomain.Loader(info.AppDomainInitializer.Method.DeclaringType.Assembly.Location);
				appDomain.DoCallBack(new CrossAppDomainDelegate(@object.Load));
				AppDomain.Initializer object2 = new AppDomain.Initializer(info.AppDomainInitializer, info.AppDomainInitializerArguments);
				appDomain.DoCallBack(new CrossAppDomainDelegate(object2.Initialize));
			}
			return appDomain;
		}

		// Token: 0x06000CF7 RID: 3319 RVA: 0x00039D14 File Offset: 0x00037F14
		public static AppDomain CreateDomain(string friendlyName, Evidence securityInfo, string appBasePath, string appRelativeSearchPath, bool shadowCopyFiles)
		{
			return AppDomain.CreateDomain(friendlyName, securityInfo, AppDomain.CreateDomainSetup(appBasePath, appRelativeSearchPath, shadowCopyFiles));
		}

		// Token: 0x06000CF8 RID: 3320 RVA: 0x00039D28 File Offset: 0x00037F28
		public static AppDomain CreateDomain(string friendlyName, Evidence securityInfo, AppDomainSetup info, PermissionSet grantSet, params System.Security.Policy.StrongName[] fullTrustAssemblies)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.ApplicationTrust = new ApplicationTrust(grantSet, fullTrustAssemblies ?? new System.Security.Policy.StrongName[0]);
			return AppDomain.CreateDomain(friendlyName, securityInfo, info);
		}

		// Token: 0x06000CF9 RID: 3321 RVA: 0x00039D6C File Offset: 0x00037F6C
		private static AppDomainSetup CreateDomainSetup(string appBasePath, string appRelativeSearchPath, bool shadowCopyFiles)
		{
			AppDomainSetup appDomainSetup = new AppDomainSetup();
			appDomainSetup.ApplicationBase = appBasePath;
			appDomainSetup.PrivateBinPath = appRelativeSearchPath;
			if (shadowCopyFiles)
			{
				appDomainSetup.ShadowCopyFiles = "true";
			}
			else
			{
				appDomainSetup.ShadowCopyFiles = "false";
			}
			return appDomainSetup;
		}

		// Token: 0x06000CFA RID: 3322
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool InternalIsFinalizingForUnload(int domain_id);

		// Token: 0x06000CFB RID: 3323 RVA: 0x00039DB0 File Offset: 0x00037FB0
		public bool IsFinalizingForUnload()
		{
			return AppDomain.InternalIsFinalizingForUnload(this.getDomainID());
		}

		// Token: 0x06000CFC RID: 3324
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalUnload(int domain_id);

		// Token: 0x06000CFD RID: 3325 RVA: 0x00039DC0 File Offset: 0x00037FC0
		private int getDomainID()
		{
			return Thread.GetDomainID();
		}

		// Token: 0x06000CFE RID: 3326 RVA: 0x00039DC8 File Offset: 0x00037FC8
		[ReliabilityContract(Consistency.MayCorruptAppDomain, Cer.MayFail)]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public static void Unload(AppDomain domain)
		{
			if (domain == null)
			{
				throw new ArgumentNullException("domain");
			}
			AppDomain.InternalUnload(domain.getDomainID());
		}

		// Token: 0x06000CFF RID: 3327
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetData(string name, object data);

		// Token: 0x06000D00 RID: 3328 RVA: 0x00039DE8 File Offset: 0x00037FE8
		[MonoTODO]
		public void SetData(string name, object data, IPermission permission)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000D01 RID: 3329 RVA: 0x00039DF0 File Offset: 0x00037FF0
		[Obsolete("Use AppDomainSetup.DynamicBase")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		public void SetDynamicBase(string path)
		{
			this.SetupInformationNoCopy.DynamicBase = path;
		}

		// Token: 0x06000D02 RID: 3330 RVA: 0x00039E00 File Offset: 0x00038000
		[Obsolete("AppDomain.GetCurrentThreadId has been deprecated because it does not provide a stable Id when managed threads are running on fibers (aka lightweight threads). To get a stable identifier for a managed thread, use the ManagedThreadId property on Thread.'")]
		public static int GetCurrentThreadId()
		{
			return Thread.CurrentThreadId;
		}

		// Token: 0x06000D03 RID: 3331 RVA: 0x00039E08 File Offset: 0x00038008
		public override string ToString()
		{
			return this.getFriendlyName();
		}

		// Token: 0x06000D04 RID: 3332 RVA: 0x00039E10 File Offset: 0x00038010
		private static void ValidateAssemblyName(string name)
		{
			if (name == null || name.Length == 0)
			{
				throw new ArgumentException("The Name of AssemblyName cannot be null or a zero-length string.");
			}
			bool flag = true;
			for (int i = 0; i < name.Length; i++)
			{
				char c = name[i];
				if (i == 0 && char.IsWhiteSpace(c))
				{
					flag = false;
					break;
				}
				if (c == '/' || c == '\\' || c == ':')
				{
					flag = false;
					break;
				}
			}
			if (!flag)
			{
				throw new ArgumentException("The Name of AssemblyName cannot start with whitespace, or contain '/', '\\'  or ':'.");
			}
		}

		// Token: 0x06000D05 RID: 3333 RVA: 0x00039EA4 File Offset: 0x000380A4
		private void DoAssemblyLoad(Assembly assembly)
		{
			if (this.AssemblyLoad == null)
			{
				return;
			}
			this.AssemblyLoad(this, new AssemblyLoadEventArgs(assembly));
		}

		// Token: 0x06000D06 RID: 3334 RVA: 0x00039EC4 File Offset: 0x000380C4
		private Assembly DoAssemblyResolve(string name, bool refonly)
		{
			ResolveEventHandler resolveEventHandler;
			if (refonly)
			{
				resolveEventHandler = this.ReflectionOnlyAssemblyResolve;
			}
			else
			{
				resolveEventHandler = this.AssemblyResolve;
			}
			if (resolveEventHandler == null)
			{
				return null;
			}
			Hashtable hashtable;
			if (refonly)
			{
				hashtable = AppDomain.assembly_resolve_in_progress_refonly;
				if (hashtable == null)
				{
					hashtable = new Hashtable();
					AppDomain.assembly_resolve_in_progress_refonly = hashtable;
				}
			}
			else
			{
				hashtable = AppDomain.assembly_resolve_in_progress;
				if (hashtable == null)
				{
					hashtable = new Hashtable();
					AppDomain.assembly_resolve_in_progress = hashtable;
				}
			}
			string text = (string)hashtable[name];
			if (text != null)
			{
				return null;
			}
			hashtable[name] = name;
			Assembly result;
			try
			{
				Delegate[] invocationList = resolveEventHandler.GetInvocationList();
				foreach (Delegate @delegate in invocationList)
				{
					ResolveEventHandler resolveEventHandler2 = (ResolveEventHandler)@delegate;
					Assembly assembly = resolveEventHandler2(this, new ResolveEventArgs(name));
					if (assembly != null)
					{
						return assembly;
					}
				}
				result = null;
			}
			finally
			{
				hashtable.Remove(name);
			}
			return result;
		}

		// Token: 0x06000D07 RID: 3335 RVA: 0x00039FD0 File Offset: 0x000381D0
		internal Assembly DoTypeResolve(object name_or_tb)
		{
			if (this.TypeResolve == null)
			{
				return null;
			}
			string text;
			if (name_or_tb is TypeBuilder)
			{
				text = ((TypeBuilder)name_or_tb).FullName;
			}
			else
			{
				text = (string)name_or_tb;
			}
			Hashtable hashtable = AppDomain.type_resolve_in_progress;
			if (hashtable == null)
			{
				hashtable = new Hashtable();
				AppDomain.type_resolve_in_progress = hashtable;
			}
			if (hashtable.Contains(text))
			{
				return null;
			}
			hashtable[text] = text;
			Assembly result;
			try
			{
				foreach (Delegate @delegate in this.TypeResolve.GetInvocationList())
				{
					ResolveEventHandler resolveEventHandler = (ResolveEventHandler)@delegate;
					Assembly assembly = resolveEventHandler(this, new ResolveEventArgs(text));
					if (assembly != null)
					{
						return assembly;
					}
				}
				result = null;
			}
			finally
			{
				hashtable.Remove(text);
			}
			return result;
		}

		// Token: 0x06000D08 RID: 3336 RVA: 0x0003A0C0 File Offset: 0x000382C0
		private void DoDomainUnload()
		{
			if (this.DomainUnload != null)
			{
				this.DomainUnload(this, null);
			}
		}

		// Token: 0x06000D09 RID: 3337 RVA: 0x0003A0DC File Offset: 0x000382DC
		internal byte[] GetMarshalledDomainObjRef()
		{
			ObjRef obj = RemotingServices.Marshal(AppDomain.CurrentDomain, null, typeof(AppDomain));
			return CADSerializer.SerializeObject(obj).GetBuffer();
		}

		// Token: 0x06000D0A RID: 3338 RVA: 0x0003A10C File Offset: 0x0003830C
		internal void ProcessMessageInDomain(byte[] arrRequest, CADMethodCallMessage cadMsg, out byte[] arrResponse, out CADMethodReturnMessage cadMrm)
		{
			IMessage msg;
			if (arrRequest != null)
			{
				msg = CADSerializer.DeserializeMessage(new MemoryStream(arrRequest), null);
			}
			else
			{
				msg = new MethodCall(cadMsg);
			}
			IMessage message = ChannelServices.SyncDispatchMessage(msg);
			cadMrm = CADMethodReturnMessage.Create(message);
			if (cadMrm == null)
			{
				arrResponse = CADSerializer.SerializeMessage(message).GetBuffer();
			}
			else
			{
				arrResponse = null;
			}
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06000D0B RID: 3339 RVA: 0x0003A168 File Offset: 0x00038368
		public AppDomainManager DomainManager
		{
			get
			{
				return this._domain_manager;
			}
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x06000D0C RID: 3340 RVA: 0x0003A170 File Offset: 0x00038370
		public ActivationContext ActivationContext
		{
			get
			{
				return this._activation;
			}
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x06000D0D RID: 3341 RVA: 0x0003A178 File Offset: 0x00038378
		public ApplicationIdentity ApplicationIdentity
		{
			get
			{
				return this._applicationIdentity;
			}
		}

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x06000D0E RID: 3342 RVA: 0x0003A180 File Offset: 0x00038380
		public int Id
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return this.getDomainID();
			}
		}

		// Token: 0x06000D0F RID: 3343 RVA: 0x0003A188 File Offset: 0x00038388
		[MonoTODO("This routine only returns the parameter currently")]
		[ComVisible(false)]
		public string ApplyPolicy(string assemblyName)
		{
			if (assemblyName == null)
			{
				throw new ArgumentNullException("assemblyName");
			}
			if (assemblyName.Length == 0)
			{
				throw new ArgumentException("assemblyName");
			}
			return assemblyName;
		}

		// Token: 0x06000D10 RID: 3344 RVA: 0x0003A1C0 File Offset: 0x000383C0
		public static AppDomain CreateDomain(string friendlyName, Evidence securityInfo, string appBasePath, string appRelativeSearchPath, bool shadowCopyFiles, AppDomainInitializer adInit, string[] adInitArgs)
		{
			AppDomainSetup appDomainSetup = AppDomain.CreateDomainSetup(appBasePath, appRelativeSearchPath, shadowCopyFiles);
			appDomainSetup.AppDomainInitializerArguments = adInitArgs;
			appDomainSetup.AppDomainInitializer = adInit;
			return AppDomain.CreateDomain(friendlyName, securityInfo, appDomainSetup);
		}

		// Token: 0x06000D11 RID: 3345 RVA: 0x0003A1F0 File Offset: 0x000383F0
		public int ExecuteAssemblyByName(string assemblyName)
		{
			return this.ExecuteAssemblyByName(assemblyName, null, null);
		}

		// Token: 0x06000D12 RID: 3346 RVA: 0x0003A1FC File Offset: 0x000383FC
		public int ExecuteAssemblyByName(string assemblyName, Evidence assemblySecurity)
		{
			return this.ExecuteAssemblyByName(assemblyName, assemblySecurity, null);
		}

		// Token: 0x06000D13 RID: 3347 RVA: 0x0003A208 File Offset: 0x00038408
		public int ExecuteAssemblyByName(string assemblyName, Evidence assemblySecurity, params string[] args)
		{
			Assembly a = Assembly.Load(assemblyName, assemblySecurity);
			return this.ExecuteAssemblyInternal(a, args);
		}

		// Token: 0x06000D14 RID: 3348 RVA: 0x0003A228 File Offset: 0x00038428
		public int ExecuteAssemblyByName(AssemblyName assemblyName, Evidence assemblySecurity, params string[] args)
		{
			Assembly a = Assembly.Load(assemblyName, assemblySecurity);
			return this.ExecuteAssemblyInternal(a, args);
		}

		// Token: 0x06000D15 RID: 3349 RVA: 0x0003A248 File Offset: 0x00038448
		public bool IsDefaultAppDomain()
		{
			return object.ReferenceEquals(this, AppDomain.DefaultDomain);
		}

		// Token: 0x06000D16 RID: 3350 RVA: 0x0003A258 File Offset: 0x00038458
		public Assembly[] ReflectionOnlyGetAssemblies()
		{
			return this.GetAssemblies(true);
		}

		// Token: 0x06000D17 RID: 3351 RVA: 0x0003A264 File Offset: 0x00038464
		object GetLifetimeService()
		{
			return base.GetLifetimeService();
		}

		// Token: 0x04000365 RID: 869
		private IntPtr _mono_app_domain;

		// Token: 0x04000366 RID: 870
		private static string _process_guid;

		// Token: 0x04000367 RID: 871
		[ThreadStatic]
		private static Hashtable type_resolve_in_progress;

		// Token: 0x04000368 RID: 872
		[ThreadStatic]
		private static Hashtable assembly_resolve_in_progress;

		// Token: 0x04000369 RID: 873
		[ThreadStatic]
		private static Hashtable assembly_resolve_in_progress_refonly;

		// Token: 0x0400036A RID: 874
		private Evidence _evidence;

		// Token: 0x0400036B RID: 875
		private PermissionSet _granted;

		// Token: 0x0400036C RID: 876
		private PrincipalPolicy _principalPolicy;

		// Token: 0x0400036D RID: 877
		[ThreadStatic]
		private static IPrincipal _principal;

		// Token: 0x0400036E RID: 878
		private static AppDomain default_domain;

		// Token: 0x0400036F RID: 879
		private AppDomainManager _domain_manager;

		// Token: 0x04000370 RID: 880
		private ActivationContext _activation;

		// Token: 0x04000371 RID: 881
		private ApplicationIdentity _applicationIdentity;

		// Token: 0x020000F8 RID: 248
		[Serializable]
		private class Loader
		{
			// Token: 0x06000D18 RID: 3352 RVA: 0x0003A26C File Offset: 0x0003846C
			public Loader(string assembly)
			{
				this.assembly = assembly;
			}

			// Token: 0x06000D19 RID: 3353 RVA: 0x0003A27C File Offset: 0x0003847C
			public void Load()
			{
				Assembly.LoadFrom(this.assembly);
			}

			// Token: 0x0400037A RID: 890
			private string assembly;
		}

		// Token: 0x020000F9 RID: 249
		[Serializable]
		private class Initializer
		{
			// Token: 0x06000D1A RID: 3354 RVA: 0x0003A28C File Offset: 0x0003848C
			public Initializer(AppDomainInitializer initializer, string[] arguments)
			{
				this.initializer = initializer;
				this.arguments = arguments;
			}

			// Token: 0x06000D1B RID: 3355 RVA: 0x0003A2A4 File Offset: 0x000384A4
			public void Initialize()
			{
				this.initializer(this.arguments);
			}

			// Token: 0x0400037B RID: 891
			private AppDomainInitializer initializer;

			// Token: 0x0400037C RID: 892
			private string[] arguments;
		}
	}
}
