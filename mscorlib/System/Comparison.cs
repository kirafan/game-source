﻿using System;

namespace System
{
	// Token: 0x02000701 RID: 1793
	// (Invoke) Token: 0x060043F8 RID: 17400
	public delegate int Comparison<T>(T x, T y);
}
