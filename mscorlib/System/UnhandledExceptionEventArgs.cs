﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000192 RID: 402
	[ComVisible(true)]
	[Serializable]
	public class UnhandledExceptionEventArgs : EventArgs
	{
		// Token: 0x06001470 RID: 5232 RVA: 0x000522A0 File Offset: 0x000504A0
		public UnhandledExceptionEventArgs(object exception, bool isTerminating)
		{
			this.exception = exception;
			this.m_isTerminating = isTerminating;
		}

		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06001471 RID: 5233 RVA: 0x000522B8 File Offset: 0x000504B8
		public object ExceptionObject
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return this.exception;
			}
		}

		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x06001472 RID: 5234 RVA: 0x000522C0 File Offset: 0x000504C0
		public bool IsTerminating
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return this.m_isTerminating;
			}
		}

		// Token: 0x04000803 RID: 2051
		private object exception;

		// Token: 0x04000804 RID: 2052
		private bool m_isTerminating;
	}
}
