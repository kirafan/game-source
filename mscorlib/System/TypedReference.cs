﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System
{
	// Token: 0x0200005D RID: 93
	[CLSCompliant(false)]
	[ComVisible(true)]
	public struct TypedReference
	{
		// Token: 0x06000697 RID: 1687 RVA: 0x00014D94 File Offset: 0x00012F94
		public override bool Equals(object o)
		{
			throw new NotSupportedException(Locale.GetText("This operation is not supported for this type."));
		}

		// Token: 0x06000698 RID: 1688 RVA: 0x00014DA8 File Offset: 0x00012FA8
		public override int GetHashCode()
		{
			if (this.type.Value == IntPtr.Zero)
			{
				return 0;
			}
			return Type.GetTypeFromHandle(this.type).GetHashCode();
		}

		// Token: 0x06000699 RID: 1689 RVA: 0x00014DE4 File Offset: 0x00012FE4
		public static Type GetTargetType(TypedReference value)
		{
			return Type.GetTypeFromHandle(value.type);
		}

		// Token: 0x0600069A RID: 1690 RVA: 0x00014DF4 File Offset: 0x00012FF4
		[MonoTODO]
		[CLSCompliant(false)]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"MemberAccess\"/>\n</PermissionSet>\n")]
		public static TypedReference MakeTypedReference(object target, FieldInfo[] flds)
		{
			if (target == null)
			{
				throw new ArgumentNullException("target");
			}
			if (flds == null)
			{
				throw new ArgumentNullException("flds");
			}
			if (flds.Length == 0)
			{
				throw new ArgumentException(Locale.GetText("flds has no elements"));
			}
			throw new NotImplementedException();
		}

		// Token: 0x0600069B RID: 1691 RVA: 0x00014E40 File Offset: 0x00013040
		[MonoTODO]
		[CLSCompliant(false)]
		public static void SetTypedReference(TypedReference target, object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			throw new NotImplementedException();
		}

		// Token: 0x0600069C RID: 1692 RVA: 0x00014E58 File Offset: 0x00013058
		public static RuntimeTypeHandle TargetTypeToken(TypedReference value)
		{
			return value.type;
		}

		// Token: 0x0600069D RID: 1693
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern object ToObject(TypedReference value);

		// Token: 0x040000B4 RID: 180
		private RuntimeTypeHandle type;

		// Token: 0x040000B5 RID: 181
		private IntPtr value;

		// Token: 0x040000B6 RID: 182
		private IntPtr klass;
	}
}
