﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000020 RID: 32
	[ComVisible(true)]
	[Serializable]
	public struct Double : IFormattable, IConvertible, IComparable, IComparable<double>, IEquatable<double>
	{
		// Token: 0x060002A0 RID: 672 RVA: 0x0000B1BC File Offset: 0x000093BC
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			return Convert.ToType(this, targetType, provider, false);
		}

		// Token: 0x060002A1 RID: 673 RVA: 0x0000B1EC File Offset: 0x000093EC
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this);
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x0000B1F8 File Offset: 0x000093F8
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this);
		}

		// Token: 0x060002A3 RID: 675 RVA: 0x0000B204 File Offset: 0x00009404
		char IConvertible.ToChar(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060002A4 RID: 676 RVA: 0x0000B20C File Offset: 0x0000940C
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060002A5 RID: 677 RVA: 0x0000B214 File Offset: 0x00009414
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this);
		}

		// Token: 0x060002A6 RID: 678 RVA: 0x0000B220 File Offset: 0x00009420
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this);
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x0000B22C File Offset: 0x0000942C
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x0000B238 File Offset: 0x00009438
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this);
		}

		// Token: 0x060002A9 RID: 681 RVA: 0x0000B244 File Offset: 0x00009444
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		// Token: 0x060002AA RID: 682 RVA: 0x0000B250 File Offset: 0x00009450
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this);
		}

		// Token: 0x060002AB RID: 683 RVA: 0x0000B25C File Offset: 0x0000945C
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this);
		}

		// Token: 0x060002AC RID: 684 RVA: 0x0000B268 File Offset: 0x00009468
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this);
		}

		// Token: 0x060002AD RID: 685 RVA: 0x0000B274 File Offset: 0x00009474
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		// Token: 0x060002AE RID: 686 RVA: 0x0000B280 File Offset: 0x00009480
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		// Token: 0x060002AF RID: 687 RVA: 0x0000B28C File Offset: 0x0000948C
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is double))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.Double"));
			}
			double num = (double)value;
			if (double.IsPositiveInfinity(this) && double.IsPositiveInfinity(num))
			{
				return 0;
			}
			if (double.IsNegativeInfinity(this) && double.IsNegativeInfinity(num))
			{
				return 0;
			}
			if (double.IsNaN(num))
			{
				if (double.IsNaN(this))
				{
					return 0;
				}
				return 1;
			}
			else if (double.IsNaN(this))
			{
				if (double.IsNaN(num))
				{
					return 0;
				}
				return -1;
			}
			else
			{
				if (this > num)
				{
					return 1;
				}
				if (this < num)
				{
					return -1;
				}
				return 0;
			}
		}

		// Token: 0x060002B0 RID: 688 RVA: 0x0000B340 File Offset: 0x00009540
		public override bool Equals(object obj)
		{
			if (!(obj is double))
			{
				return false;
			}
			double num = (double)obj;
			if (double.IsNaN(num))
			{
				return double.IsNaN(this);
			}
			return num == this;
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x0000B37C File Offset: 0x0000957C
		public int CompareTo(double value)
		{
			if (double.IsPositiveInfinity(this) && double.IsPositiveInfinity(value))
			{
				return 0;
			}
			if (double.IsNegativeInfinity(this) && double.IsNegativeInfinity(value))
			{
				return 0;
			}
			if (double.IsNaN(value))
			{
				if (double.IsNaN(this))
				{
					return 0;
				}
				return 1;
			}
			else if (double.IsNaN(this))
			{
				if (double.IsNaN(value))
				{
					return 0;
				}
				return -1;
			}
			else
			{
				if (this > value)
				{
					return 1;
				}
				if (this < value)
				{
					return -1;
				}
				return 0;
			}
		}

		// Token: 0x060002B2 RID: 690 RVA: 0x0000B408 File Offset: 0x00009608
		public bool Equals(double obj)
		{
			if (double.IsNaN(obj))
			{
				return double.IsNaN(this);
			}
			return obj == this;
		}

		// Token: 0x060002B3 RID: 691 RVA: 0x0000B438 File Offset: 0x00009638
		public override int GetHashCode()
		{
			double num = this;
			return num.GetHashCode();
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0000B450 File Offset: 0x00009650
		public static bool IsInfinity(double d)
		{
			return d == double.PositiveInfinity || d == double.NegativeInfinity;
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x0000B470 File Offset: 0x00009670
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static bool IsNaN(double d)
		{
			return d != d;
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x0000B47C File Offset: 0x0000967C
		public static bool IsNegativeInfinity(double d)
		{
			return d < 0.0 && (d == double.NegativeInfinity || d == double.PositiveInfinity);
		}

		// Token: 0x060002B7 RID: 695 RVA: 0x0000B4BC File Offset: 0x000096BC
		public static bool IsPositiveInfinity(double d)
		{
			return d > 0.0 && (d == double.NegativeInfinity || d == double.PositiveInfinity);
		}

		// Token: 0x060002B8 RID: 696 RVA: 0x0000B4FC File Offset: 0x000096FC
		public static double Parse(string s)
		{
			return double.Parse(s, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent, null);
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x0000B50C File Offset: 0x0000970C
		public static double Parse(string s, IFormatProvider provider)
		{
			return double.Parse(s, NumberStyles.AllowLeadingWhite | NumberStyles.AllowTrailingWhite | NumberStyles.AllowLeadingSign | NumberStyles.AllowDecimalPoint | NumberStyles.AllowThousands | NumberStyles.AllowExponent, provider);
		}

		// Token: 0x060002BA RID: 698 RVA: 0x0000B51C File Offset: 0x0000971C
		public static double Parse(string s, NumberStyles style)
		{
			return double.Parse(s, style, null);
		}

		// Token: 0x060002BB RID: 699 RVA: 0x0000B528 File Offset: 0x00009728
		public static double Parse(string s, NumberStyles style, IFormatProvider provider)
		{
			double result;
			Exception ex;
			if (!double.Parse(s, style, provider, false, out result, out ex))
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x060002BC RID: 700 RVA: 0x0000B54C File Offset: 0x0000974C
		internal unsafe static bool Parse(string s, NumberStyles style, IFormatProvider provider, bool tryParse, out double result, out Exception exc)
		{
			result = 0.0;
			exc = null;
			if (s == null)
			{
				if (!tryParse)
				{
					exc = new ArgumentNullException("s");
				}
				return false;
			}
			if (s.Length == 0)
			{
				if (!tryParse)
				{
					exc = new FormatException();
				}
				return false;
			}
			if ((style & NumberStyles.AllowHexSpecifier) != NumberStyles.None)
			{
				string text = Locale.GetText("Double doesn't support parsing with '{0}'.", new object[]
				{
					"AllowHexSpecifier"
				});
				throw new ArgumentException(text);
			}
			if (style > NumberStyles.Any)
			{
				if (!tryParse)
				{
					exc = new ArgumentException();
				}
				return false;
			}
			NumberFormatInfo instance = NumberFormatInfo.GetInstance(provider);
			if (instance == null)
			{
				throw new Exception("How did this happen?");
			}
			int length = s.Length;
			int num = 0;
			int i = 0;
			bool flag = (style & NumberStyles.AllowLeadingWhite) != NumberStyles.None;
			bool flag2 = (style & NumberStyles.AllowTrailingWhite) != NumberStyles.None;
			if (flag)
			{
				while (i < length && char.IsWhiteSpace(s[i]))
				{
					i++;
				}
				if (i == length)
				{
					if (!tryParse)
					{
						exc = int.GetFormatException();
					}
					return false;
				}
			}
			int num2 = s.Length - 1;
			if (flag2)
			{
				while (char.IsWhiteSpace(s[num2]))
				{
					num2--;
				}
			}
			if (double.TryParseStringConstant(instance.NaNSymbol, s, i, num2))
			{
				result = double.NaN;
				return true;
			}
			if (double.TryParseStringConstant(instance.PositiveInfinitySymbol, s, i, num2))
			{
				result = double.PositiveInfinity;
				return true;
			}
			if (double.TryParseStringConstant(instance.NegativeInfinitySymbol, s, i, num2))
			{
				result = double.NegativeInfinity;
				return true;
			}
			byte[] array = new byte[length + 1];
			int num3 = 1;
			string text2 = null;
			string text3 = null;
			string text4 = null;
			int num4 = 0;
			int num5 = 0;
			int num6 = 0;
			if ((style & NumberStyles.AllowDecimalPoint) != NumberStyles.None)
			{
				text2 = instance.NumberDecimalSeparator;
				num4 = text2.Length;
			}
			if ((style & NumberStyles.AllowThousands) != NumberStyles.None)
			{
				text3 = instance.NumberGroupSeparator;
				num5 = text3.Length;
			}
			if ((style & NumberStyles.AllowCurrencySymbol) != NumberStyles.None)
			{
				text4 = instance.CurrencySymbol;
				num6 = text4.Length;
			}
			string positiveSign = instance.PositiveSign;
			string negativeSign = instance.NegativeSign;
			while (i < length)
			{
				char c = s[i];
				if (c == '\0')
				{
					i = length;
				}
				else
				{
					switch (num3)
					{
					case 1:
						if ((style & NumberStyles.AllowLeadingSign) != NumberStyles.None)
						{
							if (c == positiveSign[0] && s.Substring(i, positiveSign.Length) == positiveSign)
							{
								num3 = 2;
								i += positiveSign.Length - 1;
								goto IL_624;
							}
							if (c == negativeSign[0] && s.Substring(i, negativeSign.Length) == negativeSign)
							{
								num3 = 2;
								array[num++] = 45;
								i += negativeSign.Length - 1;
								goto IL_624;
							}
						}
						num3 = 2;
						goto IL_304;
					case 2:
						goto IL_304;
					case 3:
						goto IL_429;
					case 4:
						if (char.IsDigit(c))
						{
							num3 = 5;
							goto IL_599;
						}
						if (c == positiveSign[0] && s.Substring(i, positiveSign.Length) == positiveSign)
						{
							num3 = 2;
							i += positiveSign.Length - 1;
							goto IL_624;
						}
						if (c == negativeSign[0] && s.Substring(i, negativeSign.Length) == negativeSign)
						{
							num3 = 2;
							array[num++] = 45;
							i += negativeSign.Length - 1;
							goto IL_624;
						}
						if (char.IsWhiteSpace(c))
						{
							goto IL_5E7;
						}
						if (!tryParse)
						{
							exc = new FormatException("Unknown char: " + c);
						}
						return false;
					case 5:
						goto IL_599;
					case 6:
						goto IL_5E7;
					}
					IL_617:
					if (num3 == 7)
					{
						break;
					}
					goto IL_624;
					IL_429:
					if (char.IsDigit(c))
					{
						array[num++] = (byte)c;
						goto IL_617;
					}
					if (c == 'e' || c == 'E')
					{
						if ((style & NumberStyles.AllowExponent) == NumberStyles.None)
						{
							if (!tryParse)
							{
								exc = new FormatException("Unknown char: " + c);
							}
							return false;
						}
						array[num++] = (byte)c;
						num3 = 4;
						goto IL_617;
					}
					else
					{
						if (char.IsWhiteSpace(c))
						{
							goto IL_5E7;
						}
						if (!tryParse)
						{
							exc = new FormatException("Unknown char: " + c);
						}
						return false;
					}
					IL_304:
					if (char.IsDigit(c))
					{
						array[num++] = (byte)c;
						goto IL_617;
					}
					if (c == 'e' || c == 'E')
					{
						goto IL_429;
					}
					if (num4 > 0 && text2[0] == c && string.CompareOrdinal(s, i, text2, 0, num4) == 0)
					{
						array[num++] = 46;
						i += num4 - 1;
						num3 = 3;
						goto IL_617;
					}
					if (num5 > 0 && text3[0] == c && s.Substring(i, num5) == text3)
					{
						i += num5 - 1;
						num3 = 2;
						goto IL_617;
					}
					if (num6 > 0 && text4[0] == c && s.Substring(i, num6) == text4)
					{
						i += num6 - 1;
						num3 = 2;
						goto IL_617;
					}
					if (char.IsWhiteSpace(c))
					{
						goto IL_5E7;
					}
					if (!tryParse)
					{
						exc = new FormatException("Unknown char: " + c);
					}
					return false;
					IL_599:
					if (char.IsDigit(c))
					{
						array[num++] = (byte)c;
						goto IL_617;
					}
					if (!char.IsWhiteSpace(c))
					{
						if (!tryParse)
						{
							exc = new FormatException("Unknown char: " + c);
						}
						return false;
					}
					IL_5E7:
					if (flag2 && char.IsWhiteSpace(c))
					{
						num3 = 6;
						goto IL_617;
					}
					if (!tryParse)
					{
						exc = new FormatException("Unknown char");
					}
					return false;
				}
				IL_624:
				i++;
			}
			array[num] = 0;
			double num7;
			if (!double.ParseImpl(&array[0], out num7))
			{
				if (!tryParse)
				{
					exc = int.GetFormatException();
				}
				return false;
			}
			if (double.IsPositiveInfinity(num7) || double.IsNegativeInfinity(num7))
			{
				if (!tryParse)
				{
					exc = new OverflowException();
				}
				return false;
			}
			result = num7;
			return true;
		}

		// Token: 0x060002BD RID: 701 RVA: 0x0000BBE8 File Offset: 0x00009DE8
		private static bool TryParseStringConstant(string format, string s, int start, int end)
		{
			return end - start + 1 == format.Length && string.CompareOrdinal(format, 0, s, start, format.Length) == 0;
		}

		// Token: 0x060002BE RID: 702
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern bool ParseImpl(byte* byte_ptr, out double value);

		// Token: 0x060002BF RID: 703 RVA: 0x0000BC1C File Offset: 0x00009E1C
		public static bool TryParse(string s, NumberStyles style, IFormatProvider provider, out double result)
		{
			Exception ex;
			if (!double.Parse(s, style, provider, true, out result, out ex))
			{
				result = 0.0;
				return false;
			}
			return true;
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x0000BC48 File Offset: 0x00009E48
		public static bool TryParse(string s, out double result)
		{
			return double.TryParse(s, NumberStyles.Any, null, out result);
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x0000BC58 File Offset: 0x00009E58
		public override string ToString()
		{
			return NumberFormatter.NumberToString(this, null);
		}

		// Token: 0x060002C2 RID: 706 RVA: 0x0000BC64 File Offset: 0x00009E64
		public string ToString(IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(this, provider);
		}

		// Token: 0x060002C3 RID: 707 RVA: 0x0000BC70 File Offset: 0x00009E70
		public string ToString(string format)
		{
			return this.ToString(format, null);
		}

		// Token: 0x060002C4 RID: 708 RVA: 0x0000BC7C File Offset: 0x00009E7C
		public string ToString(string format, IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(format, this, provider);
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x0000BC88 File Offset: 0x00009E88
		public TypeCode GetTypeCode()
		{
			return TypeCode.Double;
		}

		// Token: 0x04000034 RID: 52
		public const double Epsilon = 5E-324;

		// Token: 0x04000035 RID: 53
		public const double MaxValue = 1.7976931348623157E+308;

		// Token: 0x04000036 RID: 54
		public const double MinValue = -1.7976931348623157E+308;

		// Token: 0x04000037 RID: 55
		public const double NaN = double.NaN;

		// Token: 0x04000038 RID: 56
		public const double NegativeInfinity = double.NegativeInfinity;

		// Token: 0x04000039 RID: 57
		public const double PositiveInfinity = double.PositiveInfinity;

		// Token: 0x0400003A RID: 58
		private const int State_AllowSign = 1;

		// Token: 0x0400003B RID: 59
		private const int State_Digits = 2;

		// Token: 0x0400003C RID: 60
		private const int State_Decimal = 3;

		// Token: 0x0400003D RID: 61
		private const int State_ExponentSign = 4;

		// Token: 0x0400003E RID: 62
		private const int State_Exponent = 5;

		// Token: 0x0400003F RID: 63
		private const int State_ConsumeWhiteSpace = 6;

		// Token: 0x04000040 RID: 64
		private const int State_Exit = 7;

		// Token: 0x04000041 RID: 65
		internal double m_value;
	}
}
