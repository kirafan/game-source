﻿using System;

namespace System
{
	// Token: 0x0200019D RID: 413
	internal struct SmallRect
	{
		// Token: 0x060014A3 RID: 5283 RVA: 0x00053308 File Offset: 0x00051508
		public SmallRect(int left, int top, int right, int bottom)
		{
			this.Left = (short)left;
			this.Top = (short)top;
			this.Right = (short)right;
			this.Bottom = (short)bottom;
		}

		// Token: 0x04000839 RID: 2105
		public short Left;

		// Token: 0x0400083A RID: 2106
		public short Top;

		// Token: 0x0400083B RID: 2107
		public short Right;

		// Token: 0x0400083C RID: 2108
		public short Bottom;
	}
}
