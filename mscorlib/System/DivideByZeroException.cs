﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000128 RID: 296
	[ComVisible(true)]
	[Serializable]
	public class DivideByZeroException : ArithmeticException
	{
		// Token: 0x060010D6 RID: 4310 RVA: 0x000450C0 File Offset: 0x000432C0
		public DivideByZeroException() : base(Locale.GetText("Division by zero"))
		{
			base.HResult = -2147352558;
		}

		// Token: 0x060010D7 RID: 4311 RVA: 0x000450E0 File Offset: 0x000432E0
		public DivideByZeroException(string message) : base(message)
		{
			base.HResult = -2147352558;
		}

		// Token: 0x060010D8 RID: 4312 RVA: 0x000450F4 File Offset: 0x000432F4
		public DivideByZeroException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147352558;
		}

		// Token: 0x060010D9 RID: 4313 RVA: 0x0004510C File Offset: 0x0004330C
		protected DivideByZeroException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040004CD RID: 1229
		private const int Result = -2147352558;
	}
}
