﻿using System;

namespace System
{
	// Token: 0x02000060 RID: 96
	[Serializable]
	public struct Nullable<T> where T : struct
	{
		// Token: 0x060006B3 RID: 1715 RVA: 0x000150E0 File Offset: 0x000132E0
		public Nullable(T value)
		{
			this.has_value = true;
			this.value = value;
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060006B4 RID: 1716 RVA: 0x000150F0 File Offset: 0x000132F0
		public bool HasValue
		{
			get
			{
				return this.has_value;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x060006B5 RID: 1717 RVA: 0x000150F8 File Offset: 0x000132F8
		public T Value
		{
			get
			{
				if (!this.has_value)
				{
					throw new InvalidOperationException("Nullable object must have a value.");
				}
				return this.value;
			}
		}

		// Token: 0x060006B6 RID: 1718 RVA: 0x00015118 File Offset: 0x00013318
		public override bool Equals(object other)
		{
			if (other == null)
			{
				return !this.has_value;
			}
			return other is T? && this.Equals((T?)other);
		}

		// Token: 0x060006B7 RID: 1719 RVA: 0x00015144 File Offset: 0x00013344
		private bool Equals(T? other)
		{
			return other.has_value == this.has_value && (!this.has_value || other.value.Equals(this.value));
		}

		// Token: 0x060006B8 RID: 1720 RVA: 0x00015190 File Offset: 0x00013390
		public override int GetHashCode()
		{
			if (!this.has_value)
			{
				return 0;
			}
			return this.value.GetHashCode();
		}

		// Token: 0x060006B9 RID: 1721 RVA: 0x000151B0 File Offset: 0x000133B0
		public T GetValueOrDefault()
		{
			return (!this.has_value) ? default(T) : this.value;
		}

		// Token: 0x060006BA RID: 1722 RVA: 0x000151DC File Offset: 0x000133DC
		public T GetValueOrDefault(T defaultValue)
		{
			return (!this.has_value) ? defaultValue : this.value;
		}

		// Token: 0x060006BB RID: 1723 RVA: 0x000151F8 File Offset: 0x000133F8
		public override string ToString()
		{
			if (this.has_value)
			{
				return this.value.ToString();
			}
			return string.Empty;
		}

		// Token: 0x060006BC RID: 1724 RVA: 0x00015228 File Offset: 0x00013428
		private static object Box(T? o)
		{
			if (!o.has_value)
			{
				return null;
			}
			return o.value;
		}

		// Token: 0x060006BD RID: 1725 RVA: 0x00015244 File Offset: 0x00013444
		private static T? Unbox(object o)
		{
			if (o == null)
			{
				return null;
			}
			return new T?((T)((object)o));
		}

		// Token: 0x060006BE RID: 1726 RVA: 0x0001526C File Offset: 0x0001346C
		public static implicit operator T?(T value)
		{
			return new T?(value);
		}

		// Token: 0x060006BF RID: 1727 RVA: 0x00015274 File Offset: 0x00013474
		public static explicit operator T(T? value)
		{
			return value.Value;
		}

		// Token: 0x040000BC RID: 188
		internal T value;

		// Token: 0x040000BD RID: 189
		internal bool has_value;
	}
}
