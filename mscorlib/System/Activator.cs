﻿using System;
using System.Configuration.Assemblies;
using System.Globalization;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Activation;
using System.Security.Permissions;
using System.Security.Policy;
using System.Text;

namespace System
{
	// Token: 0x020000F6 RID: 246
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_Activator))]
	[ComVisible(true)]
	public sealed class Activator : _Activator
	{
		// Token: 0x06000C67 RID: 3175 RVA: 0x000385F4 File Offset: 0x000367F4
		private Activator()
		{
		}

		// Token: 0x06000C68 RID: 3176 RVA: 0x000385FC File Offset: 0x000367FC
		void _Activator.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C69 RID: 3177 RVA: 0x00038604 File Offset: 0x00036804
		void _Activator.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C6A RID: 3178 RVA: 0x0003860C File Offset: 0x0003680C
		void _Activator.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C6B RID: 3179 RVA: 0x00038614 File Offset: 0x00036814
		void _Activator.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C6C RID: 3180 RVA: 0x0003861C File Offset: 0x0003681C
		[MonoTODO("No COM support")]
		public static ObjectHandle CreateComInstanceFrom(string assemblyName, string typeName)
		{
			if (assemblyName == null)
			{
				throw new ArgumentNullException("assemblyName");
			}
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}
			if (assemblyName.Length == 0)
			{
				throw new ArgumentException("assemblyName");
			}
			throw new NotImplementedException();
		}

		// Token: 0x06000C6D RID: 3181 RVA: 0x0003865C File Offset: 0x0003685C
		[MonoTODO("Mono does not support COM")]
		public static ObjectHandle CreateComInstanceFrom(string assemblyName, string typeName, byte[] hashValue, AssemblyHashAlgorithm hashAlgorithm)
		{
			if (assemblyName == null)
			{
				throw new ArgumentNullException("assemblyName");
			}
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}
			if (assemblyName.Length == 0)
			{
				throw new ArgumentException("assemblyName");
			}
			throw new NotImplementedException();
		}

		// Token: 0x06000C6E RID: 3182 RVA: 0x0003869C File Offset: 0x0003689C
		public static ObjectHandle CreateInstanceFrom(string assemblyFile, string typeName)
		{
			return Activator.CreateInstanceFrom(assemblyFile, typeName, null);
		}

		// Token: 0x06000C6F RID: 3183 RVA: 0x000386A8 File Offset: 0x000368A8
		public static ObjectHandle CreateInstanceFrom(string assemblyFile, string typeName, object[] activationAttributes)
		{
			return Activator.CreateInstanceFrom(assemblyFile, typeName, false, BindingFlags.Instance | BindingFlags.Public | BindingFlags.CreateInstance, null, null, null, activationAttributes, null);
		}

		// Token: 0x06000C70 RID: 3184 RVA: 0x000386C8 File Offset: 0x000368C8
		public static ObjectHandle CreateInstanceFrom(string assemblyFile, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityInfo)
		{
			Assembly assembly = Assembly.LoadFrom(assemblyFile, securityInfo);
			if (assembly == null)
			{
				return null;
			}
			Type type = assembly.GetType(typeName, true, ignoreCase);
			if (type == null)
			{
				return null;
			}
			object obj = Activator.CreateInstance(type, bindingAttr, binder, args, culture, activationAttributes);
			return (obj == null) ? null : new ObjectHandle(obj);
		}

		// Token: 0x06000C71 RID: 3185 RVA: 0x0003871C File Offset: 0x0003691C
		public static ObjectHandle CreateInstance(string assemblyName, string typeName)
		{
			if (assemblyName == null)
			{
				assemblyName = Assembly.GetCallingAssembly().GetName().Name;
			}
			return Activator.CreateInstance(assemblyName, typeName, null);
		}

		// Token: 0x06000C72 RID: 3186 RVA: 0x00038748 File Offset: 0x00036948
		public static ObjectHandle CreateInstance(string assemblyName, string typeName, object[] activationAttributes)
		{
			if (assemblyName == null)
			{
				assemblyName = Assembly.GetCallingAssembly().GetName().Name;
			}
			return Activator.CreateInstance(assemblyName, typeName, false, BindingFlags.Instance | BindingFlags.Public | BindingFlags.CreateInstance, null, null, null, activationAttributes, null);
		}

		// Token: 0x06000C73 RID: 3187 RVA: 0x00038780 File Offset: 0x00036980
		public static ObjectHandle CreateInstance(string assemblyName, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityInfo)
		{
			Assembly assembly;
			if (assemblyName == null)
			{
				assembly = Assembly.GetCallingAssembly();
			}
			else
			{
				assembly = Assembly.Load(assemblyName, securityInfo);
			}
			Type type = assembly.GetType(typeName, true, ignoreCase);
			object obj = Activator.CreateInstance(type, bindingAttr, binder, args, culture, activationAttributes);
			return (obj == null) ? null : new ObjectHandle(obj);
		}

		// Token: 0x06000C74 RID: 3188 RVA: 0x000387D8 File Offset: 0x000369D8
		[MonoNotSupported("no ClickOnce in mono")]
		public static ObjectHandle CreateInstance(ActivationContext activationContext)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C75 RID: 3189 RVA: 0x000387E0 File Offset: 0x000369E0
		[MonoNotSupported("no ClickOnce in mono")]
		public static ObjectHandle CreateInstance(ActivationContext activationContext, string[] activationCustomData)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000C76 RID: 3190 RVA: 0x000387E8 File Offset: 0x000369E8
		public static ObjectHandle CreateInstanceFrom(AppDomain domain, string assemblyFile, string typeName)
		{
			if (domain == null)
			{
				throw new ArgumentNullException("domain");
			}
			return domain.CreateInstanceFrom(assemblyFile, typeName);
		}

		// Token: 0x06000C77 RID: 3191 RVA: 0x00038804 File Offset: 0x00036A04
		public static ObjectHandle CreateInstanceFrom(AppDomain domain, string assemblyFile, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityAttributes)
		{
			if (domain == null)
			{
				throw new ArgumentNullException("domain");
			}
			return domain.CreateInstanceFrom(assemblyFile, typeName, ignoreCase, bindingAttr, binder, args, culture, activationAttributes, securityAttributes);
		}

		// Token: 0x06000C78 RID: 3192 RVA: 0x00038838 File Offset: 0x00036A38
		public static ObjectHandle CreateInstance(AppDomain domain, string assemblyName, string typeName)
		{
			if (domain == null)
			{
				throw new ArgumentNullException("domain");
			}
			return domain.CreateInstance(assemblyName, typeName);
		}

		// Token: 0x06000C79 RID: 3193 RVA: 0x00038854 File Offset: 0x00036A54
		public static ObjectHandle CreateInstance(AppDomain domain, string assemblyName, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityAttributes)
		{
			if (domain == null)
			{
				throw new ArgumentNullException("domain");
			}
			return domain.CreateInstance(assemblyName, typeName, ignoreCase, bindingAttr, binder, args, culture, activationAttributes, securityAttributes);
		}

		// Token: 0x06000C7A RID: 3194 RVA: 0x00038888 File Offset: 0x00036A88
		public static T CreateInstance<T>()
		{
			return (T)((object)Activator.CreateInstance(typeof(T)));
		}

		// Token: 0x06000C7B RID: 3195 RVA: 0x000388A0 File Offset: 0x00036AA0
		public static object CreateInstance(Type type)
		{
			return Activator.CreateInstance(type, false);
		}

		// Token: 0x06000C7C RID: 3196 RVA: 0x000388AC File Offset: 0x00036AAC
		public static object CreateInstance(Type type, params object[] args)
		{
			return Activator.CreateInstance(type, args, new object[0]);
		}

		// Token: 0x06000C7D RID: 3197 RVA: 0x000388BC File Offset: 0x00036ABC
		public static object CreateInstance(Type type, object[] args, object[] activationAttributes)
		{
			return Activator.CreateInstance(type, BindingFlags.Default, Binder.DefaultBinder, args, null, activationAttributes);
		}

		// Token: 0x06000C7E RID: 3198 RVA: 0x000388D0 File Offset: 0x00036AD0
		public static object CreateInstance(Type type, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture)
		{
			return Activator.CreateInstance(type, bindingAttr, binder, args, culture, new object[0]);
		}

		// Token: 0x06000C7F RID: 3199 RVA: 0x000388E4 File Offset: 0x00036AE4
		public static object CreateInstance(Type type, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes)
		{
			Activator.CheckType(type);
			if (type.ContainsGenericParameters)
			{
				throw new ArgumentException(type + " is an open generic type", "type");
			}
			if ((bindingAttr & (BindingFlags.IgnoreCase | BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy)) == BindingFlags.Default)
			{
				bindingAttr |= (BindingFlags.Instance | BindingFlags.Public);
			}
			int num = 0;
			if (args != null)
			{
				num = args.Length;
			}
			Type[] array = (num != 0) ? new Type[num] : Type.EmptyTypes;
			for (int i = 0; i < num; i++)
			{
				if (args[i] != null)
				{
					array[i] = args[i].GetType();
				}
			}
			if (binder == null)
			{
				binder = Binder.DefaultBinder;
			}
			ConstructorInfo constructorInfo = (ConstructorInfo)binder.SelectMethod(bindingAttr, type.GetConstructors(bindingAttr), array, null);
			if (constructorInfo != null)
			{
				Activator.CheckAbstractType(type);
				if (activationAttributes != null && activationAttributes.Length > 0)
				{
					if (!type.IsMarshalByRef)
					{
						string text = Locale.GetText("Type '{0}' doesn't derive from MarshalByRefObject.", new object[]
						{
							type.FullName
						});
						throw new NotSupportedException(text);
					}
					object obj = ActivationServices.CreateProxyFromAttributes(type, activationAttributes);
					if (obj != null)
					{
						constructorInfo.Invoke(obj, bindingAttr, binder, args, culture);
						return obj;
					}
				}
				return constructorInfo.Invoke(bindingAttr, binder, args, culture);
			}
			if (type.IsValueType && array.Length == 0)
			{
				return Activator.CreateInstanceInternal(type);
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (Type type2 in array)
			{
				stringBuilder.Append((type2 == null) ? "(unknown)" : type2.ToString());
				stringBuilder.Append(", ");
			}
			if (stringBuilder.Length > 2)
			{
				stringBuilder.Length -= 2;
			}
			throw new MissingMethodException(string.Format(Locale.GetText("No constructor found for {0}::.ctor({1})"), type.FullName, stringBuilder));
		}

		// Token: 0x06000C80 RID: 3200 RVA: 0x00038AB4 File Offset: 0x00036CB4
		public static object CreateInstance(Type type, bool nonPublic)
		{
			Activator.CheckType(type);
			if (type.ContainsGenericParameters)
			{
				throw new ArgumentException(type + " is an open generic type", "type");
			}
			Activator.CheckAbstractType(type);
			MonoType monoType = type as MonoType;
			ConstructorInfo constructorInfo;
			if (monoType != null)
			{
				constructorInfo = monoType.GetDefaultConstructor();
				if (!nonPublic && constructorInfo != null && !constructorInfo.IsPublic)
				{
					constructorInfo = null;
				}
			}
			else
			{
				BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.Public;
				if (nonPublic)
				{
					bindingFlags |= BindingFlags.NonPublic;
				}
				constructorInfo = type.GetConstructor(bindingFlags, null, CallingConventions.Any, Type.EmptyTypes, null);
			}
			if (constructorInfo != null)
			{
				return constructorInfo.Invoke(null);
			}
			if (type.IsValueType)
			{
				return Activator.CreateInstanceInternal(type);
			}
			throw new MissingMethodException(Locale.GetText("Default constructor not found."), ".ctor() of " + type.FullName);
		}

		// Token: 0x06000C81 RID: 3201 RVA: 0x00038B80 File Offset: 0x00036D80
		private static void CheckType(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (type == typeof(TypedReference) || type == typeof(ArgIterator) || type == typeof(void) || type == typeof(RuntimeArgumentHandle))
			{
				string text = Locale.GetText("CreateInstance cannot be used to create this type ({0}).", new object[]
				{
					type.FullName
				});
				throw new NotSupportedException(text);
			}
		}

		// Token: 0x06000C82 RID: 3202 RVA: 0x00038C00 File Offset: 0x00036E00
		private static void CheckAbstractType(Type type)
		{
			if (type.IsAbstract)
			{
				string text = Locale.GetText("Cannot create an abstract class '{0}'.", new object[]
				{
					type.FullName
				});
				throw new MissingMethodException(text);
			}
		}

		// Token: 0x06000C83 RID: 3203 RVA: 0x00038C3C File Offset: 0x00036E3C
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"RemotingConfiguration\"/>\n</PermissionSet>\n")]
		public static object GetObject(Type type, string url)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			return RemotingServices.Connect(type, url);
		}

		// Token: 0x06000C84 RID: 3204 RVA: 0x00038C58 File Offset: 0x00036E58
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"RemotingConfiguration\"/>\n</PermissionSet>\n")]
		public static object GetObject(Type type, string url, object state)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			return RemotingServices.Connect(type, url, state);
		}

		// Token: 0x06000C85 RID: 3205
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern object CreateInstanceInternal(Type type);

		// Token: 0x04000363 RID: 867
		private const BindingFlags _flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.CreateInstance;

		// Token: 0x04000364 RID: 868
		private const BindingFlags _accessFlags = BindingFlags.IgnoreCase | BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.FlattenHierarchy;
	}
}
