﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000153 RID: 339
	[ComVisible(true)]
	[Serializable]
	public class MissingMethodException : MissingMemberException
	{
		// Token: 0x0600122D RID: 4653 RVA: 0x00047EB4 File Offset: 0x000460B4
		public MissingMethodException() : base(Locale.GetText("Cannot find the requested method."))
		{
			base.HResult = -2146233069;
		}

		// Token: 0x0600122E RID: 4654 RVA: 0x00047ED4 File Offset: 0x000460D4
		public MissingMethodException(string message) : base(message)
		{
			base.HResult = -2146233069;
		}

		// Token: 0x0600122F RID: 4655 RVA: 0x00047EE8 File Offset: 0x000460E8
		protected MissingMethodException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06001230 RID: 4656 RVA: 0x00047EF4 File Offset: 0x000460F4
		public MissingMethodException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233069;
		}

		// Token: 0x06001231 RID: 4657 RVA: 0x00047F0C File Offset: 0x0004610C
		public MissingMethodException(string className, string methodName) : base(className, methodName)
		{
			base.HResult = -2146233069;
		}

		// Token: 0x17000291 RID: 657
		// (get) Token: 0x06001232 RID: 4658 RVA: 0x00047F24 File Offset: 0x00046124
		public override string Message
		{
			get
			{
				if (this.ClassName == null)
				{
					return base.Message;
				}
				string text = Locale.GetText("Method not found: '{0}.{1}'.");
				return string.Format(text, this.ClassName, this.MemberName);
			}
		}

		// Token: 0x04000537 RID: 1335
		private const int Result = -2146233069;
	}
}
