﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200005A RID: 90
	[ComVisible(true)]
	public struct RuntimeArgumentHandle
	{
		// Token: 0x040000B3 RID: 179
		internal IntPtr args;
	}
}
