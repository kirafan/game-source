﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;

namespace System
{
	// Token: 0x0200013A RID: 314
	public static class GC
	{
		// Token: 0x1700026C RID: 620
		// (get) Token: 0x06001138 RID: 4408
		public static extern int MaxGeneration { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001139 RID: 4409
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InternalCollect(int generation);

		// Token: 0x0600113A RID: 4410 RVA: 0x00046218 File Offset: 0x00044418
		public static void Collect()
		{
			GC.InternalCollect(GC.MaxGeneration);
		}

		// Token: 0x0600113B RID: 4411 RVA: 0x00046224 File Offset: 0x00044424
		public static void Collect(int generation)
		{
			if (generation < 0)
			{
				throw new ArgumentOutOfRangeException("generation");
			}
			GC.InternalCollect(generation);
		}

		// Token: 0x0600113C RID: 4412 RVA: 0x00046240 File Offset: 0x00044440
		[MonoDocumentationNote("mode parameter ignored")]
		public static void Collect(int generation, GCCollectionMode mode)
		{
			GC.Collect(generation);
		}

		// Token: 0x0600113D RID: 4413
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetGeneration(object obj);

		// Token: 0x0600113E RID: 4414 RVA: 0x00046248 File Offset: 0x00044448
		public static int GetGeneration(WeakReference wo)
		{
			object target = wo.Target;
			if (target == null)
			{
				throw new ArgumentException();
			}
			return GC.GetGeneration(target);
		}

		// Token: 0x0600113F RID: 4415
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long GetTotalMemory(bool forceFullCollection);

		// Token: 0x06001140 RID: 4416
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void KeepAlive(object obj);

		// Token: 0x06001141 RID: 4417
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void ReRegisterForFinalize(object obj);

		// Token: 0x06001142 RID: 4418
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void SuppressFinalize(object obj);

		// Token: 0x06001143 RID: 4419
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WaitForPendingFinalizers();

		// Token: 0x06001144 RID: 4420
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int CollectionCount(int generation);

		// Token: 0x06001145 RID: 4421
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void RecordPressure(long bytesAllocated);

		// Token: 0x06001146 RID: 4422 RVA: 0x00046270 File Offset: 0x00044470
		public static void AddMemoryPressure(long bytesAllocated)
		{
			GC.RecordPressure(bytesAllocated);
		}

		// Token: 0x06001147 RID: 4423 RVA: 0x00046278 File Offset: 0x00044478
		public static void RemoveMemoryPressure(long bytesAllocated)
		{
			GC.RecordPressure(-bytesAllocated);
		}
	}
}
