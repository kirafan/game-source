﻿using System;
using System.Runtime.InteropServices;

namespace System.Deployment.Internal
{
	// Token: 0x020001D8 RID: 472
	[ComVisible(false)]
	public static class InternalActivationContextHelper
	{
		// Token: 0x0600184A RID: 6218 RVA: 0x0005D508 File Offset: 0x0005B708
		[MonoTODO]
		public static object GetActivationContextData(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600184B RID: 6219 RVA: 0x0005D510 File Offset: 0x0005B710
		[MonoTODO]
		public static object GetApplicationComponentManifest(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600184C RID: 6220 RVA: 0x0005D518 File Offset: 0x0005B718
		[MonoTODO("2.0 SP1 member")]
		public static byte[] GetApplicationManifestBytes(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600184D RID: 6221 RVA: 0x0005D520 File Offset: 0x0005B720
		[MonoTODO]
		public static object GetDeploymentComponentManifest(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600184E RID: 6222 RVA: 0x0005D528 File Offset: 0x0005B728
		[MonoTODO("2.0 SP1 member")]
		public static byte[] GetDeploymentManifestBytes(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600184F RID: 6223 RVA: 0x0005D530 File Offset: 0x0005B730
		[MonoTODO]
		public static bool IsFirstRun(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001850 RID: 6224 RVA: 0x0005D538 File Offset: 0x0005B738
		[MonoTODO]
		public static void PrepareForExecution(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}
	}
}
