﻿using System;
using System.Runtime.InteropServices;

namespace System.Deployment.Internal
{
	// Token: 0x020001D9 RID: 473
	[ComVisible(false)]
	public static class InternalApplicationIdentityHelper
	{
		// Token: 0x06001851 RID: 6225 RVA: 0x0005D540 File Offset: 0x0005B740
		[MonoTODO("2.0 SP1 member")]
		public static object GetActivationContextData(ActivationContext appInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06001852 RID: 6226 RVA: 0x0005D548 File Offset: 0x0005B748
		[MonoTODO]
		public static object GetInternalAppId(ApplicationIdentity id)
		{
			throw new NotImplementedException();
		}
	}
}
