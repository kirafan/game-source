﻿using System;

namespace System
{
	// Token: 0x020006EB RID: 1771
	// (Invoke) Token: 0x060043A0 RID: 17312
	[Serializable]
	public delegate void EventHandler<TEventArgs>(object sender, TEventArgs e) where TEventArgs : EventArgs;
}
