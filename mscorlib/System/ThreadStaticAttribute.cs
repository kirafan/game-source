﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200017F RID: 383
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public class ThreadStaticAttribute : Attribute
	{
	}
}
