﻿using System;

namespace System
{
	// Token: 0x0200013C RID: 316
	public enum GCNotificationStatus
	{
		// Token: 0x04000505 RID: 1285
		Succeeded,
		// Token: 0x04000506 RID: 1286
		Failed,
		// Token: 0x04000507 RID: 1287
		Canceled,
		// Token: 0x04000508 RID: 1288
		Timeout,
		// Token: 0x04000509 RID: 1289
		NotApplicable
	}
}
