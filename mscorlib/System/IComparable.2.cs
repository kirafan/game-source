﻿using System;

namespace System
{
	// Token: 0x0200000A RID: 10
	public interface IComparable<T>
	{
		// Token: 0x06000082 RID: 130
		int CompareTo(T other);
	}
}
