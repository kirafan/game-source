﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200015F RID: 351
	[ComVisible(true)]
	[Serializable]
	public class NotSupportedException : SystemException
	{
		// Token: 0x0600129F RID: 4767 RVA: 0x0004962C File Offset: 0x0004782C
		public NotSupportedException() : base(Locale.GetText("Operation is not supported."))
		{
			base.HResult = -2146233067;
		}

		// Token: 0x060012A0 RID: 4768 RVA: 0x0004964C File Offset: 0x0004784C
		public NotSupportedException(string message) : base(message)
		{
			base.HResult = -2146233067;
		}

		// Token: 0x060012A1 RID: 4769 RVA: 0x00049660 File Offset: 0x00047860
		public NotSupportedException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233067;
		}

		// Token: 0x060012A2 RID: 4770 RVA: 0x00049678 File Offset: 0x00047878
		protected NotSupportedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0400054C RID: 1356
		private const int Result = -2146233067;
	}
}
