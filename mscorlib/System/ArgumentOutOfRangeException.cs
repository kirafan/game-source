﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000102 RID: 258
	[ComVisible(true)]
	[Serializable]
	public class ArgumentOutOfRangeException : ArgumentException
	{
		// Token: 0x06000D79 RID: 3449 RVA: 0x0003AD9C File Offset: 0x00038F9C
		public ArgumentOutOfRangeException() : base(Locale.GetText("Argument is out of range."))
		{
			base.HResult = -2146233086;
		}

		// Token: 0x06000D7A RID: 3450 RVA: 0x0003ADBC File Offset: 0x00038FBC
		public ArgumentOutOfRangeException(string paramName) : base(Locale.GetText("Argument is out of range."), paramName)
		{
			base.HResult = -2146233086;
		}

		// Token: 0x06000D7B RID: 3451 RVA: 0x0003ADDC File Offset: 0x00038FDC
		public ArgumentOutOfRangeException(string paramName, string message) : base(message, paramName)
		{
			base.HResult = -2146233086;
		}

		// Token: 0x06000D7C RID: 3452 RVA: 0x0003ADF4 File Offset: 0x00038FF4
		public ArgumentOutOfRangeException(string paramName, object actualValue, string message) : base(message, paramName)
		{
			this.actual_value = actualValue;
			base.HResult = -2146233086;
		}

		// Token: 0x06000D7D RID: 3453 RVA: 0x0003AE10 File Offset: 0x00039010
		protected ArgumentOutOfRangeException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.actual_value = info.GetString("ActualValue");
		}

		// Token: 0x06000D7E RID: 3454 RVA: 0x0003AE2C File Offset: 0x0003902C
		public ArgumentOutOfRangeException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233086;
		}

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x06000D7F RID: 3455 RVA: 0x0003AE44 File Offset: 0x00039044
		public virtual object ActualValue
		{
			get
			{
				return this.actual_value;
			}
		}

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x06000D80 RID: 3456 RVA: 0x0003AE4C File Offset: 0x0003904C
		public override string Message
		{
			get
			{
				string message = base.Message;
				if (this.actual_value == null)
				{
					return message;
				}
				return message + Environment.NewLine + this.actual_value;
			}
		}

		// Token: 0x06000D81 RID: 3457 RVA: 0x0003AE80 File Offset: 0x00039080
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("ActualValue", this.actual_value);
		}

		// Token: 0x040003A1 RID: 929
		private const int Result = -2146233086;

		// Token: 0x040003A2 RID: 930
		private object actual_value;
	}
}
