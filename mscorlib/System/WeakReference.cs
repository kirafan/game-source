﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000198 RID: 408
	[ComVisible(true)]
	[Serializable]
	public class WeakReference : ISerializable
	{
		// Token: 0x06001498 RID: 5272 RVA: 0x00053158 File Offset: 0x00051358
		public WeakReference(object target) : this(target, false)
		{
		}

		// Token: 0x06001499 RID: 5273 RVA: 0x00053164 File Offset: 0x00051364
		public WeakReference(object target, bool trackResurrection)
		{
			this.isLongReference = trackResurrection;
			this.AllocateHandle(target);
		}

		// Token: 0x0600149A RID: 5274 RVA: 0x0005317C File Offset: 0x0005137C
		protected WeakReference(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.isLongReference = info.GetBoolean("TrackResurrection");
			object value = info.GetValue("TrackedObject", typeof(object));
			this.AllocateHandle(value);
		}

		// Token: 0x0600149B RID: 5275 RVA: 0x000531D0 File Offset: 0x000513D0
		private void AllocateHandle(object target)
		{
			if (this.isLongReference)
			{
				this.gcHandle = GCHandle.Alloc(target, GCHandleType.WeakTrackResurrection);
			}
			else
			{
				this.gcHandle = GCHandle.Alloc(target, GCHandleType.Weak);
			}
		}

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x0600149C RID: 5276 RVA: 0x00053208 File Offset: 0x00051408
		public virtual bool IsAlive
		{
			get
			{
				return this.Target != null;
			}
		}

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x0600149D RID: 5277 RVA: 0x00053218 File Offset: 0x00051418
		// (set) Token: 0x0600149E RID: 5278 RVA: 0x00053228 File Offset: 0x00051428
		public virtual object Target
		{
			get
			{
				return this.gcHandle.Target;
			}
			set
			{
				this.gcHandle.Target = value;
			}
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x0600149F RID: 5279 RVA: 0x00053238 File Offset: 0x00051438
		public virtual bool TrackResurrection
		{
			get
			{
				return this.isLongReference;
			}
		}

		// Token: 0x060014A0 RID: 5280 RVA: 0x00053240 File Offset: 0x00051440
		~WeakReference()
		{
			this.gcHandle.Free();
		}

		// Token: 0x060014A1 RID: 5281 RVA: 0x00053280 File Offset: 0x00051480
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("TrackResurrection", this.TrackResurrection);
			try
			{
				info.AddValue("TrackedObject", this.Target);
			}
			catch (Exception)
			{
				info.AddValue("TrackedObject", null);
			}
		}

		// Token: 0x04000828 RID: 2088
		private bool isLongReference;

		// Token: 0x04000829 RID: 2089
		private GCHandle gcHandle;
	}
}
