﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;

namespace System.Diagnostics
{
	// Token: 0x020001E5 RID: 485
	[ComVisible(true)]
	[MonoTODO("Serialized objects are not compatible with MS.NET")]
	[Serializable]
	public class StackFrame
	{
		// Token: 0x06001884 RID: 6276 RVA: 0x0005D8DC File Offset: 0x0005BADC
		public StackFrame()
		{
			bool flag = StackFrame.get_frame_info(2, false, out this.methodBase, out this.ilOffset, out this.nativeOffset, out this.fileName, out this.lineNumber, out this.columnNumber);
		}

		// Token: 0x06001885 RID: 6277 RVA: 0x0005D92C File Offset: 0x0005BB2C
		public StackFrame(bool fNeedFileInfo)
		{
			bool flag = StackFrame.get_frame_info(2, fNeedFileInfo, out this.methodBase, out this.ilOffset, out this.nativeOffset, out this.fileName, out this.lineNumber, out this.columnNumber);
		}

		// Token: 0x06001886 RID: 6278 RVA: 0x0005D97C File Offset: 0x0005BB7C
		public StackFrame(int skipFrames)
		{
			bool flag = StackFrame.get_frame_info(skipFrames + 2, false, out this.methodBase, out this.ilOffset, out this.nativeOffset, out this.fileName, out this.lineNumber, out this.columnNumber);
		}

		// Token: 0x06001887 RID: 6279 RVA: 0x0005D9CC File Offset: 0x0005BBCC
		public StackFrame(int skipFrames, bool fNeedFileInfo)
		{
			bool flag = StackFrame.get_frame_info(skipFrames + 2, fNeedFileInfo, out this.methodBase, out this.ilOffset, out this.nativeOffset, out this.fileName, out this.lineNumber, out this.columnNumber);
		}

		// Token: 0x06001888 RID: 6280 RVA: 0x0005DA1C File Offset: 0x0005BC1C
		public StackFrame(string fileName, int lineNumber)
		{
			bool flag = StackFrame.get_frame_info(2, false, out this.methodBase, out this.ilOffset, out this.nativeOffset, out fileName, out lineNumber, out this.columnNumber);
			this.fileName = fileName;
			this.lineNumber = lineNumber;
			this.columnNumber = 0;
		}

		// Token: 0x06001889 RID: 6281 RVA: 0x0005DA78 File Offset: 0x0005BC78
		public StackFrame(string fileName, int lineNumber, int colNumber)
		{
			bool flag = StackFrame.get_frame_info(2, false, out this.methodBase, out this.ilOffset, out this.nativeOffset, out fileName, out lineNumber, out this.columnNumber);
			this.fileName = fileName;
			this.lineNumber = lineNumber;
			this.columnNumber = colNumber;
		}

		// Token: 0x0600188A RID: 6282
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool get_frame_info(int skip, bool needFileInfo, out MethodBase method, out int iloffset, out int native_offset, out string file, out int line, out int column);

		// Token: 0x0600188B RID: 6283 RVA: 0x0005DAD4 File Offset: 0x0005BCD4
		public virtual int GetFileLineNumber()
		{
			return this.lineNumber;
		}

		// Token: 0x0600188C RID: 6284 RVA: 0x0005DADC File Offset: 0x0005BCDC
		public virtual int GetFileColumnNumber()
		{
			return this.columnNumber;
		}

		// Token: 0x0600188D RID: 6285 RVA: 0x0005DAE4 File Offset: 0x0005BCE4
		public virtual string GetFileName()
		{
			if (SecurityManager.SecurityEnabled && this.fileName != null && this.fileName.Length > 0)
			{
				string fullPath = Path.GetFullPath(this.fileName);
				new FileIOPermission(FileIOPermissionAccess.PathDiscovery, fullPath).Demand();
			}
			return this.fileName;
		}

		// Token: 0x0600188E RID: 6286 RVA: 0x0005DB38 File Offset: 0x0005BD38
		internal string GetSecureFileName()
		{
			string result = "<filename unknown>";
			if (this.fileName == null)
			{
				return result;
			}
			try
			{
				result = this.GetFileName();
			}
			catch (SecurityException)
			{
			}
			return result;
		}

		// Token: 0x0600188F RID: 6287 RVA: 0x0005DB88 File Offset: 0x0005BD88
		public virtual int GetILOffset()
		{
			return this.ilOffset;
		}

		// Token: 0x06001890 RID: 6288 RVA: 0x0005DB90 File Offset: 0x0005BD90
		public virtual MethodBase GetMethod()
		{
			return this.methodBase;
		}

		// Token: 0x06001891 RID: 6289 RVA: 0x0005DB98 File Offset: 0x0005BD98
		public virtual int GetNativeOffset()
		{
			return this.nativeOffset;
		}

		// Token: 0x06001892 RID: 6290 RVA: 0x0005DBA0 File Offset: 0x0005BDA0
		internal string GetInternalMethodName()
		{
			return this.internalMethodName;
		}

		// Token: 0x06001893 RID: 6291 RVA: 0x0005DBA8 File Offset: 0x0005BDA8
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (this.methodBase == null)
			{
				stringBuilder.Append(Locale.GetText("<unknown method>"));
			}
			else
			{
				stringBuilder.Append(this.methodBase.Name);
			}
			stringBuilder.Append(Locale.GetText(" at "));
			if (this.ilOffset == -1)
			{
				stringBuilder.Append(Locale.GetText("<unknown offset>"));
			}
			else
			{
				stringBuilder.Append(Locale.GetText("offset "));
				stringBuilder.Append(this.ilOffset);
			}
			stringBuilder.Append(Locale.GetText(" in file:line:column "));
			stringBuilder.Append(this.GetSecureFileName());
			stringBuilder.AppendFormat(":{0}:{1}", this.lineNumber, this.columnNumber);
			return stringBuilder.ToString();
		}

		// Token: 0x040008F3 RID: 2291
		public const int OFFSET_UNKNOWN = -1;

		// Token: 0x040008F4 RID: 2292
		private int ilOffset = -1;

		// Token: 0x040008F5 RID: 2293
		private int nativeOffset = -1;

		// Token: 0x040008F6 RID: 2294
		private MethodBase methodBase;

		// Token: 0x040008F7 RID: 2295
		private string fileName;

		// Token: 0x040008F8 RID: 2296
		private int lineNumber;

		// Token: 0x040008F9 RID: 2297
		private int columnNumber;

		// Token: 0x040008FA RID: 2298
		private string internalMethodName;
	}
}
