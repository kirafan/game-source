﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x020001E4 RID: 484
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
	[ComVisible(true)]
	public sealed class DebuggerVisualizerAttribute : Attribute
	{
		// Token: 0x06001876 RID: 6262 RVA: 0x0005D770 File Offset: 0x0005B970
		public DebuggerVisualizerAttribute(string visualizerTypeName)
		{
			this.visualizerName = visualizerTypeName;
		}

		// Token: 0x06001877 RID: 6263 RVA: 0x0005D780 File Offset: 0x0005B980
		public DebuggerVisualizerAttribute(Type visualizer)
		{
			if (visualizer == null)
			{
				throw new ArgumentNullException("visualizer");
			}
			this.visualizerName = visualizer.AssemblyQualifiedName;
		}

		// Token: 0x06001878 RID: 6264 RVA: 0x0005D7A8 File Offset: 0x0005B9A8
		public DebuggerVisualizerAttribute(string visualizerTypeName, string visualizerObjectSourceTypeName)
		{
			this.visualizerName = visualizerTypeName;
			this.visualizerSourceName = visualizerObjectSourceTypeName;
		}

		// Token: 0x06001879 RID: 6265 RVA: 0x0005D7C0 File Offset: 0x0005B9C0
		public DebuggerVisualizerAttribute(string visualizerTypeName, Type visualizerObjectSource)
		{
			if (visualizerObjectSource == null)
			{
				throw new ArgumentNullException("visualizerObjectSource");
			}
			this.visualizerName = visualizerTypeName;
			this.visualizerSourceName = visualizerObjectSource.AssemblyQualifiedName;
		}

		// Token: 0x0600187A RID: 6266 RVA: 0x0005D7F8 File Offset: 0x0005B9F8
		public DebuggerVisualizerAttribute(Type visualizer, string visualizerObjectSourceTypeName)
		{
			if (visualizer == null)
			{
				throw new ArgumentNullException("visualizer");
			}
			this.visualizerName = visualizer.AssemblyQualifiedName;
			this.visualizerSourceName = visualizerObjectSourceTypeName;
		}

		// Token: 0x0600187B RID: 6267 RVA: 0x0005D830 File Offset: 0x0005BA30
		public DebuggerVisualizerAttribute(Type visualizer, Type visualizerObjectSource)
		{
			if (visualizer == null)
			{
				throw new ArgumentNullException("visualizer");
			}
			if (visualizerObjectSource == null)
			{
				throw new ArgumentNullException("visualizerObjectSource");
			}
			this.visualizerName = visualizer.AssemblyQualifiedName;
			this.visualizerSourceName = visualizerObjectSource.AssemblyQualifiedName;
		}

		// Token: 0x170003E3 RID: 995
		// (get) Token: 0x0600187C RID: 6268 RVA: 0x0005D880 File Offset: 0x0005BA80
		// (set) Token: 0x0600187D RID: 6269 RVA: 0x0005D888 File Offset: 0x0005BA88
		public string Description
		{
			get
			{
				return this.description;
			}
			set
			{
				this.description = value;
			}
		}

		// Token: 0x170003E4 RID: 996
		// (get) Token: 0x0600187E RID: 6270 RVA: 0x0005D894 File Offset: 0x0005BA94
		// (set) Token: 0x0600187F RID: 6271 RVA: 0x0005D89C File Offset: 0x0005BA9C
		public Type Target
		{
			get
			{
				return this.target;
			}
			set
			{
				this.target = value;
				this.targetTypeName = this.target.AssemblyQualifiedName;
			}
		}

		// Token: 0x170003E5 RID: 997
		// (get) Token: 0x06001880 RID: 6272 RVA: 0x0005D8B8 File Offset: 0x0005BAB8
		// (set) Token: 0x06001881 RID: 6273 RVA: 0x0005D8C0 File Offset: 0x0005BAC0
		public string TargetTypeName
		{
			get
			{
				return this.targetTypeName;
			}
			set
			{
				this.targetTypeName = value;
			}
		}

		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x06001882 RID: 6274 RVA: 0x0005D8CC File Offset: 0x0005BACC
		public string VisualizerObjectSourceTypeName
		{
			get
			{
				return this.visualizerSourceName;
			}
		}

		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x06001883 RID: 6275 RVA: 0x0005D8D4 File Offset: 0x0005BAD4
		public string VisualizerTypeName
		{
			get
			{
				return this.visualizerName;
			}
		}

		// Token: 0x040008EE RID: 2286
		private string description;

		// Token: 0x040008EF RID: 2287
		private string visualizerSourceName;

		// Token: 0x040008F0 RID: 2288
		private string visualizerName;

		// Token: 0x040008F1 RID: 2289
		private string targetTypeName;

		// Token: 0x040008F2 RID: 2290
		private Type target;
	}
}
