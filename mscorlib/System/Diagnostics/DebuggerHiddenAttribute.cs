﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x02000054 RID: 84
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property, Inherited = false)]
	[Serializable]
	public sealed class DebuggerHiddenAttribute : Attribute
	{
	}
}
