﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x02000045 RID: 69
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
	[ComVisible(true)]
	[Serializable]
	public sealed class ConditionalAttribute : Attribute
	{
		// Token: 0x06000660 RID: 1632 RVA: 0x00014AE4 File Offset: 0x00012CE4
		public ConditionalAttribute(string conditionString)
		{
			this.myCondition = conditionString;
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x06000661 RID: 1633 RVA: 0x00014AF4 File Offset: 0x00012CF4
		public string ConditionString
		{
			get
			{
				return this.myCondition;
			}
		}

		// Token: 0x0400009A RID: 154
		private string myCondition;
	}
}
