﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x020001DD RID: 477
	[ComVisible(true)]
	public enum DebuggerBrowsableState
	{
		// Token: 0x040008E2 RID: 2274
		Never,
		// Token: 0x040008E3 RID: 2275
		Collapsed = 2,
		// Token: 0x040008E4 RID: 2276
		RootHidden
	}
}
