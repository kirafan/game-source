﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace System.Diagnostics
{
	// Token: 0x020001E6 RID: 486
	[MonoTODO("Serialized objects are not compatible with .NET")]
	[ComVisible(true)]
	[Serializable]
	public class StackTrace
	{
		// Token: 0x06001894 RID: 6292 RVA: 0x0005DC88 File Offset: 0x0005BE88
		public StackTrace()
		{
			this.init_frames(0, false);
		}

		// Token: 0x06001895 RID: 6293 RVA: 0x0005DC98 File Offset: 0x0005BE98
		public StackTrace(bool fNeedFileInfo)
		{
			this.init_frames(0, fNeedFileInfo);
		}

		// Token: 0x06001896 RID: 6294 RVA: 0x0005DCA8 File Offset: 0x0005BEA8
		public StackTrace(int skipFrames)
		{
			this.init_frames(skipFrames, false);
		}

		// Token: 0x06001897 RID: 6295 RVA: 0x0005DCB8 File Offset: 0x0005BEB8
		public StackTrace(int skipFrames, bool fNeedFileInfo)
		{
			this.init_frames(skipFrames, fNeedFileInfo);
		}

		// Token: 0x06001898 RID: 6296 RVA: 0x0005DCC8 File Offset: 0x0005BEC8
		public StackTrace(Exception e) : this(e, 0, false)
		{
		}

		// Token: 0x06001899 RID: 6297 RVA: 0x0005DCD4 File Offset: 0x0005BED4
		public StackTrace(Exception e, bool fNeedFileInfo) : this(e, 0, fNeedFileInfo)
		{
		}

		// Token: 0x0600189A RID: 6298 RVA: 0x0005DCE0 File Offset: 0x0005BEE0
		public StackTrace(Exception e, int skipFrames) : this(e, skipFrames, false)
		{
		}

		// Token: 0x0600189B RID: 6299 RVA: 0x0005DCEC File Offset: 0x0005BEEC
		public StackTrace(Exception e, int skipFrames, bool fNeedFileInfo) : this(e, skipFrames, fNeedFileInfo, false)
		{
		}

		// Token: 0x0600189C RID: 6300 RVA: 0x0005DCF8 File Offset: 0x0005BEF8
		internal StackTrace(Exception e, int skipFrames, bool fNeedFileInfo, bool returnNativeFrames)
		{
			if (e == null)
			{
				throw new ArgumentNullException("e");
			}
			if (skipFrames < 0)
			{
				throw new ArgumentOutOfRangeException("< 0", "skipFrames");
			}
			this.frames = StackTrace.get_trace(e, skipFrames, fNeedFileInfo);
			if (!returnNativeFrames)
			{
				bool flag = false;
				for (int i = 0; i < this.frames.Length; i++)
				{
					if (this.frames[i].GetMethod() == null)
					{
						flag = true;
					}
				}
				if (flag)
				{
					ArrayList arrayList = new ArrayList();
					for (int j = 0; j < this.frames.Length; j++)
					{
						if (this.frames[j].GetMethod() != null)
						{
							arrayList.Add(this.frames[j]);
						}
					}
					this.frames = (StackFrame[])arrayList.ToArray(typeof(StackFrame));
				}
			}
		}

		// Token: 0x0600189D RID: 6301 RVA: 0x0005DDD8 File Offset: 0x0005BFD8
		public StackTrace(StackFrame frame)
		{
			this.frames = new StackFrame[1];
			this.frames[0] = frame;
		}

		// Token: 0x0600189E RID: 6302 RVA: 0x0005DDF8 File Offset: 0x0005BFF8
		[MonoTODO("Not possible to create StackTraces from other threads")]
		public StackTrace(Thread targetThread, bool needFileInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600189F RID: 6303 RVA: 0x0005DE08 File Offset: 0x0005C008
		private void init_frames(int skipFrames, bool fNeedFileInfo)
		{
			if (skipFrames < 0)
			{
				throw new ArgumentOutOfRangeException("< 0", "skipFrames");
			}
			ArrayList arrayList = new ArrayList();
			skipFrames += 2;
			StackFrame stackFrame;
			while ((stackFrame = new StackFrame(skipFrames, fNeedFileInfo)) != null && stackFrame.GetMethod() != null)
			{
				arrayList.Add(stackFrame);
				skipFrames++;
			}
			this.debug_info = fNeedFileInfo;
			this.frames = (StackFrame[])arrayList.ToArray(typeof(StackFrame));
		}

		// Token: 0x060018A0 RID: 6304
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern StackFrame[] get_trace(Exception e, int skipFrames, bool fNeedFileInfo);

		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x060018A1 RID: 6305 RVA: 0x0005DE84 File Offset: 0x0005C084
		public virtual int FrameCount
		{
			get
			{
				return (this.frames != null) ? this.frames.Length : 0;
			}
		}

		// Token: 0x060018A2 RID: 6306 RVA: 0x0005DEA0 File Offset: 0x0005C0A0
		public virtual StackFrame GetFrame(int index)
		{
			if (index < 0 || index >= this.FrameCount)
			{
				return null;
			}
			return this.frames[index];
		}

		// Token: 0x060018A3 RID: 6307 RVA: 0x0005DEC0 File Offset: 0x0005C0C0
		[ComVisible(false)]
		public virtual StackFrame[] GetFrames()
		{
			return this.frames;
		}

		// Token: 0x060018A4 RID: 6308 RVA: 0x0005DEC8 File Offset: 0x0005C0C8
		public override string ToString()
		{
			string value = string.Format("{0}   {1} ", Environment.NewLine, Locale.GetText("at"));
			string text = Locale.GetText("<unknown method>");
			string text2 = Locale.GetText(" in {0}:line {1}");
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this.FrameCount; i++)
			{
				StackFrame frame = this.GetFrame(i);
				if (i > 0)
				{
					stringBuilder.Append(value);
				}
				else
				{
					stringBuilder.AppendFormat("   {0} ", Locale.GetText("at"));
				}
				MethodBase method = frame.GetMethod();
				if (method != null)
				{
					stringBuilder.AppendFormat("{0}.{1}", method.DeclaringType.FullName, method.Name);
					stringBuilder.Append("(");
					ParameterInfo[] parameters = method.GetParameters();
					for (int j = 0; j < parameters.Length; j++)
					{
						if (j > 0)
						{
							stringBuilder.Append(", ");
						}
						Type type = parameters[j].ParameterType;
						bool isByRef = type.IsByRef;
						if (isByRef)
						{
							type = type.GetElementType();
						}
						if (type.IsClass && type.Namespace != string.Empty)
						{
							stringBuilder.Append(type.Namespace);
							stringBuilder.Append(".");
						}
						stringBuilder.Append(type.Name);
						if (isByRef)
						{
							stringBuilder.Append(" ByRef");
						}
						stringBuilder.AppendFormat(" {0}", parameters[j].Name);
					}
					stringBuilder.Append(")");
				}
				else
				{
					stringBuilder.Append(text);
				}
				if (this.debug_info)
				{
					string secureFileName = frame.GetSecureFileName();
					if (secureFileName != "<filename unknown>")
					{
						stringBuilder.AppendFormat(text2, secureFileName, frame.GetFileLineNumber());
					}
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x040008FB RID: 2299
		public const int METHODS_TO_SKIP = 0;

		// Token: 0x040008FC RID: 2300
		private StackFrame[] frames;

		// Token: 0x040008FD RID: 2301
		private bool debug_info;
	}
}
