﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001EC RID: 492
	[ComVisible(true)]
	public interface ISymbolMethod
	{
		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x060018BE RID: 6334
		ISymbolScope RootScope { get; }

		// Token: 0x170003F7 RID: 1015
		// (get) Token: 0x060018BF RID: 6335
		int SequencePointCount { get; }

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x060018C0 RID: 6336
		SymbolToken Token { get; }

		// Token: 0x060018C1 RID: 6337
		ISymbolNamespace GetNamespace();

		// Token: 0x060018C2 RID: 6338
		int GetOffset(ISymbolDocument document, int line, int column);

		// Token: 0x060018C3 RID: 6339
		ISymbolVariable[] GetParameters();

		// Token: 0x060018C4 RID: 6340
		int[] GetRanges(ISymbolDocument document, int line, int column);

		// Token: 0x060018C5 RID: 6341
		ISymbolScope GetScope(int offset);

		// Token: 0x060018C6 RID: 6342
		void GetSequencePoints(int[] offsets, ISymbolDocument[] documents, int[] lines, int[] columns, int[] endLines, int[] endColumns);

		// Token: 0x060018C7 RID: 6343
		bool GetSourceStartEnd(ISymbolDocument[] docs, int[] lines, int[] columns);
	}
}
