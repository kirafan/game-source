﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001E9 RID: 489
	[ComVisible(true)]
	public interface ISymbolBinder1
	{
		// Token: 0x060018B1 RID: 6321
		ISymbolReader GetReader(IntPtr importer, string filename, string searchPath);
	}
}
