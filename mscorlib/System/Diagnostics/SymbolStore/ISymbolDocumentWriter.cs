﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001EB RID: 491
	[ComVisible(true)]
	public interface ISymbolDocumentWriter
	{
		// Token: 0x060018BC RID: 6332
		void SetCheckSum(Guid algorithmId, byte[] checkSum);

		// Token: 0x060018BD RID: 6333
		void SetSource(byte[] source);
	}
}
