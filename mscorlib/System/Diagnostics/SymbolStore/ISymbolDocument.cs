﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001EA RID: 490
	[ComVisible(true)]
	public interface ISymbolDocument
	{
		// Token: 0x170003EF RID: 1007
		// (get) Token: 0x060018B2 RID: 6322
		Guid CheckSumAlgorithmId { get; }

		// Token: 0x170003F0 RID: 1008
		// (get) Token: 0x060018B3 RID: 6323
		Guid DocumentType { get; }

		// Token: 0x170003F1 RID: 1009
		// (get) Token: 0x060018B4 RID: 6324
		bool HasEmbeddedSource { get; }

		// Token: 0x170003F2 RID: 1010
		// (get) Token: 0x060018B5 RID: 6325
		Guid Language { get; }

		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x060018B6 RID: 6326
		Guid LanguageVendor { get; }

		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x060018B7 RID: 6327
		int SourceLength { get; }

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x060018B8 RID: 6328
		string URL { get; }

		// Token: 0x060018B9 RID: 6329
		int FindClosestLine(int line);

		// Token: 0x060018BA RID: 6330
		byte[] GetCheckSum();

		// Token: 0x060018BB RID: 6331
		byte[] GetSourceRange(int startLine, int startColumn, int endLine, int endColumn);
	}
}
