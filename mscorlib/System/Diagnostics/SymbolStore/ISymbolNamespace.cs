﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001ED RID: 493
	[ComVisible(true)]
	public interface ISymbolNamespace
	{
		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x060018C8 RID: 6344
		string Name { get; }

		// Token: 0x060018C9 RID: 6345
		ISymbolNamespace[] GetNamespaces();

		// Token: 0x060018CA RID: 6346
		ISymbolVariable[] GetVariables();
	}
}
