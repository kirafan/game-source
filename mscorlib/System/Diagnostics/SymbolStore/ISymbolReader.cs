﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001EE RID: 494
	[ComVisible(true)]
	public interface ISymbolReader
	{
		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x060018CB RID: 6347
		SymbolToken UserEntryPoint { get; }

		// Token: 0x060018CC RID: 6348
		ISymbolDocument GetDocument(string url, Guid language, Guid languageVendor, Guid documentType);

		// Token: 0x060018CD RID: 6349
		ISymbolDocument[] GetDocuments();

		// Token: 0x060018CE RID: 6350
		ISymbolVariable[] GetGlobalVariables();

		// Token: 0x060018CF RID: 6351
		ISymbolMethod GetMethod(SymbolToken method);

		// Token: 0x060018D0 RID: 6352
		ISymbolMethod GetMethod(SymbolToken method, int version);

		// Token: 0x060018D1 RID: 6353
		ISymbolMethod GetMethodFromDocumentPosition(ISymbolDocument document, int line, int column);

		// Token: 0x060018D2 RID: 6354
		ISymbolNamespace[] GetNamespaces();

		// Token: 0x060018D3 RID: 6355
		byte[] GetSymAttribute(SymbolToken parent, string name);

		// Token: 0x060018D4 RID: 6356
		ISymbolVariable[] GetVariables(SymbolToken parent);
	}
}
