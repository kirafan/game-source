﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001F3 RID: 499
	[ComVisible(true)]
	public struct SymbolToken
	{
		// Token: 0x060018F9 RID: 6393 RVA: 0x0005E138 File Offset: 0x0005C338
		public SymbolToken(int val)
		{
			this._val = val;
		}

		// Token: 0x060018FA RID: 6394 RVA: 0x0005E144 File Offset: 0x0005C344
		public override bool Equals(object obj)
		{
			return obj is SymbolToken && ((SymbolToken)obj).GetToken() == this._val;
		}

		// Token: 0x060018FB RID: 6395 RVA: 0x0005E174 File Offset: 0x0005C374
		public bool Equals(SymbolToken obj)
		{
			return obj.GetToken() == this._val;
		}

		// Token: 0x060018FC RID: 6396 RVA: 0x0005E188 File Offset: 0x0005C388
		public override int GetHashCode()
		{
			return this._val.GetHashCode();
		}

		// Token: 0x060018FD RID: 6397 RVA: 0x0005E198 File Offset: 0x0005C398
		public int GetToken()
		{
			return this._val;
		}

		// Token: 0x060018FE RID: 6398 RVA: 0x0005E1A0 File Offset: 0x0005C3A0
		public static bool operator ==(SymbolToken a, SymbolToken b)
		{
			return a.Equals(b);
		}

		// Token: 0x060018FF RID: 6399 RVA: 0x0005E1AC File Offset: 0x0005C3AC
		public static bool operator !=(SymbolToken a, SymbolToken b)
		{
			return !a.Equals(b);
		}

		// Token: 0x0400090F RID: 2319
		private int _val;
	}
}
