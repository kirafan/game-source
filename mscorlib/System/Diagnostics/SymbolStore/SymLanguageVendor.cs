﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001F6 RID: 502
	[ComVisible(true)]
	public class SymLanguageVendor
	{
		// Token: 0x0400091C RID: 2332
		public static readonly Guid Microsoft;
	}
}
