﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001F4 RID: 500
	[ComVisible(true)]
	public class SymDocumentType
	{
		// Token: 0x04000910 RID: 2320
		public static readonly Guid Text;
	}
}
