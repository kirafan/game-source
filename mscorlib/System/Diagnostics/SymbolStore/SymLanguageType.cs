﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001F5 RID: 501
	[ComVisible(true)]
	public class SymLanguageType
	{
		// Token: 0x04000911 RID: 2321
		public static readonly Guid Basic;

		// Token: 0x04000912 RID: 2322
		public static readonly Guid C;

		// Token: 0x04000913 RID: 2323
		public static readonly Guid Cobol;

		// Token: 0x04000914 RID: 2324
		public static readonly Guid CPlusPlus;

		// Token: 0x04000915 RID: 2325
		public static readonly Guid CSharp;

		// Token: 0x04000916 RID: 2326
		public static readonly Guid ILAssembly;

		// Token: 0x04000917 RID: 2327
		public static readonly Guid Java;

		// Token: 0x04000918 RID: 2328
		public static readonly Guid JScript;

		// Token: 0x04000919 RID: 2329
		public static readonly Guid MCPlusPlus;

		// Token: 0x0400091A RID: 2330
		public static readonly Guid Pascal;

		// Token: 0x0400091B RID: 2331
		public static readonly Guid SMC;
	}
}
