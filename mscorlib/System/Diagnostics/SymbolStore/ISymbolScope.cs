﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001EF RID: 495
	[ComVisible(true)]
	public interface ISymbolScope
	{
		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x060018D5 RID: 6357
		int EndOffset { get; }

		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x060018D6 RID: 6358
		ISymbolMethod Method { get; }

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x060018D7 RID: 6359
		ISymbolScope Parent { get; }

		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x060018D8 RID: 6360
		int StartOffset { get; }

		// Token: 0x060018D9 RID: 6361
		ISymbolScope[] GetChildren();

		// Token: 0x060018DA RID: 6362
		ISymbolVariable[] GetLocals();

		// Token: 0x060018DB RID: 6363
		ISymbolNamespace[] GetNamespaces();
	}
}
