﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001F0 RID: 496
	[ComVisible(true)]
	public interface ISymbolVariable
	{
		// Token: 0x170003FF RID: 1023
		// (get) Token: 0x060018DC RID: 6364
		int AddressField1 { get; }

		// Token: 0x17000400 RID: 1024
		// (get) Token: 0x060018DD RID: 6365
		int AddressField2 { get; }

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x060018DE RID: 6366
		int AddressField3 { get; }

		// Token: 0x17000402 RID: 1026
		// (get) Token: 0x060018DF RID: 6367
		SymAddressKind AddressKind { get; }

		// Token: 0x17000403 RID: 1027
		// (get) Token: 0x060018E0 RID: 6368
		object Attributes { get; }

		// Token: 0x17000404 RID: 1028
		// (get) Token: 0x060018E1 RID: 6369
		int EndOffset { get; }

		// Token: 0x17000405 RID: 1029
		// (get) Token: 0x060018E2 RID: 6370
		string Name { get; }

		// Token: 0x17000406 RID: 1030
		// (get) Token: 0x060018E3 RID: 6371
		int StartOffset { get; }

		// Token: 0x060018E4 RID: 6372
		byte[] GetSignature();
	}
}
