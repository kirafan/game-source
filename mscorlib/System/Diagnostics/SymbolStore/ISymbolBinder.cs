﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001E8 RID: 488
	[ComVisible(true)]
	public interface ISymbolBinder
	{
		// Token: 0x060018B0 RID: 6320
		[Obsolete("This interface is not 64-bit clean.  Use ISymbolBinder1 instead", false)]
		ISymbolReader GetReader(int importer, string filename, string searchPath);
	}
}
