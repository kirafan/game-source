﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001F1 RID: 497
	[ComVisible(true)]
	public interface ISymbolWriter
	{
		// Token: 0x060018E5 RID: 6373
		void Close();

		// Token: 0x060018E6 RID: 6374
		void CloseMethod();

		// Token: 0x060018E7 RID: 6375
		void CloseNamespace();

		// Token: 0x060018E8 RID: 6376
		void CloseScope(int endOffset);

		// Token: 0x060018E9 RID: 6377
		ISymbolDocumentWriter DefineDocument(string url, Guid language, Guid languageVendor, Guid documentType);

		// Token: 0x060018EA RID: 6378
		void DefineField(SymbolToken parent, string name, FieldAttributes attributes, byte[] signature, SymAddressKind addrKind, int addr1, int addr2, int addr3);

		// Token: 0x060018EB RID: 6379
		void DefineGlobalVariable(string name, FieldAttributes attributes, byte[] signature, SymAddressKind addrKind, int addr1, int addr2, int addr3);

		// Token: 0x060018EC RID: 6380
		void DefineLocalVariable(string name, FieldAttributes attributes, byte[] signature, SymAddressKind addrKind, int addr1, int addr2, int addr3, int startOffset, int endOffset);

		// Token: 0x060018ED RID: 6381
		void DefineParameter(string name, ParameterAttributes attributes, int sequence, SymAddressKind addrKind, int addr1, int addr2, int addr3);

		// Token: 0x060018EE RID: 6382
		void DefineSequencePoints(ISymbolDocumentWriter document, int[] offsets, int[] lines, int[] columns, int[] endLines, int[] endColumns);

		// Token: 0x060018EF RID: 6383
		void Initialize(IntPtr emitter, string filename, bool fFullBuild);

		// Token: 0x060018F0 RID: 6384
		void OpenMethod(SymbolToken method);

		// Token: 0x060018F1 RID: 6385
		void OpenNamespace(string name);

		// Token: 0x060018F2 RID: 6386
		int OpenScope(int startOffset);

		// Token: 0x060018F3 RID: 6387
		void SetMethodSourceRange(ISymbolDocumentWriter startDoc, int startLine, int startColumn, ISymbolDocumentWriter endDoc, int endLine, int endColumn);

		// Token: 0x060018F4 RID: 6388
		void SetScopeRange(int scopeID, int startOffset, int endOffset);

		// Token: 0x060018F5 RID: 6389
		void SetSymAttribute(SymbolToken parent, string name, byte[] data);

		// Token: 0x060018F6 RID: 6390
		void SetUnderlyingWriter(IntPtr underlyingWriter);

		// Token: 0x060018F7 RID: 6391
		void SetUserEntryPoint(SymbolToken entryMethod);

		// Token: 0x060018F8 RID: 6392
		void UsingNamespace(string fullName);
	}
}
