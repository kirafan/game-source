﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics.SymbolStore
{
	// Token: 0x020001F2 RID: 498
	[ComVisible(true)]
	[Serializable]
	public enum SymAddressKind
	{
		// Token: 0x04000905 RID: 2309
		ILOffset = 1,
		// Token: 0x04000906 RID: 2310
		NativeRVA,
		// Token: 0x04000907 RID: 2311
		NativeRegister,
		// Token: 0x04000908 RID: 2312
		NativeRegisterRelative,
		// Token: 0x04000909 RID: 2313
		NativeOffset,
		// Token: 0x0400090A RID: 2314
		NativeRegisterRegister,
		// Token: 0x0400090B RID: 2315
		NativeRegisterStack,
		// Token: 0x0400090C RID: 2316
		NativeStackRegister,
		// Token: 0x0400090D RID: 2317
		BitField,
		// Token: 0x0400090E RID: 2318
		NativeSectionOffset
	}
}
