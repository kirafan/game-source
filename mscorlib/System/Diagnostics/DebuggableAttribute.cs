﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x020001DA RID: 474
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Module)]
	[ComVisible(true)]
	public sealed class DebuggableAttribute : Attribute
	{
		// Token: 0x06001853 RID: 6227 RVA: 0x0005D550 File Offset: 0x0005B750
		public DebuggableAttribute(bool isJITTrackingEnabled, bool isJITOptimizerDisabled)
		{
			this.JITTrackingEnabledFlag = isJITTrackingEnabled;
			this.JITOptimizerDisabledFlag = isJITOptimizerDisabled;
			if (isJITTrackingEnabled)
			{
				this.debuggingModes |= DebuggableAttribute.DebuggingModes.Default;
			}
			if (isJITOptimizerDisabled)
			{
				this.debuggingModes |= DebuggableAttribute.DebuggingModes.DisableOptimizations;
			}
		}

		// Token: 0x06001854 RID: 6228 RVA: 0x0005D5A0 File Offset: 0x0005B7A0
		public DebuggableAttribute(DebuggableAttribute.DebuggingModes modes)
		{
			this.debuggingModes = modes;
			this.JITTrackingEnabledFlag = ((this.debuggingModes & DebuggableAttribute.DebuggingModes.Default) != DebuggableAttribute.DebuggingModes.None);
			this.JITOptimizerDisabledFlag = ((this.debuggingModes & DebuggableAttribute.DebuggingModes.DisableOptimizations) != DebuggableAttribute.DebuggingModes.None);
		}

		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x06001855 RID: 6229 RVA: 0x0005D5DC File Offset: 0x0005B7DC
		public DebuggableAttribute.DebuggingModes DebuggingFlags
		{
			get
			{
				return this.debuggingModes;
			}
		}

		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x06001856 RID: 6230 RVA: 0x0005D5E4 File Offset: 0x0005B7E4
		public bool IsJITTrackingEnabled
		{
			get
			{
				return this.JITTrackingEnabledFlag;
			}
		}

		// Token: 0x170003D8 RID: 984
		// (get) Token: 0x06001857 RID: 6231 RVA: 0x0005D5EC File Offset: 0x0005B7EC
		public bool IsJITOptimizerDisabled
		{
			get
			{
				return this.JITOptimizerDisabledFlag;
			}
		}

		// Token: 0x040008D7 RID: 2263
		private bool JITTrackingEnabledFlag;

		// Token: 0x040008D8 RID: 2264
		private bool JITOptimizerDisabledFlag;

		// Token: 0x040008D9 RID: 2265
		private DebuggableAttribute.DebuggingModes debuggingModes;

		// Token: 0x020001DB RID: 475
		[Flags]
		[ComVisible(true)]
		public enum DebuggingModes
		{
			// Token: 0x040008DB RID: 2267
			None = 0,
			// Token: 0x040008DC RID: 2268
			Default = 1,
			// Token: 0x040008DD RID: 2269
			IgnoreSymbolStoreSequencePoints = 2,
			// Token: 0x040008DE RID: 2270
			EnableEditAndContinue = 4,
			// Token: 0x040008DF RID: 2271
			DisableOptimizations = 256
		}
	}
}
