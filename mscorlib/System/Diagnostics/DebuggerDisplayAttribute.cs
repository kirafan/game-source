﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x020001DF RID: 479
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Delegate, AllowMultiple = true)]
	[ComVisible(true)]
	public sealed class DebuggerDisplayAttribute : Attribute
	{
		// Token: 0x06001862 RID: 6242 RVA: 0x0005D63C File Offset: 0x0005B83C
		public DebuggerDisplayAttribute(string value)
		{
			if (value == null)
			{
				value = string.Empty;
			}
			this.value = value;
			this.type = string.Empty;
			this.name = string.Empty;
		}

		// Token: 0x170003DB RID: 987
		// (get) Token: 0x06001863 RID: 6243 RVA: 0x0005D67C File Offset: 0x0005B87C
		public string Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x170003DC RID: 988
		// (get) Token: 0x06001864 RID: 6244 RVA: 0x0005D684 File Offset: 0x0005B884
		// (set) Token: 0x06001865 RID: 6245 RVA: 0x0005D68C File Offset: 0x0005B88C
		public Type Target
		{
			get
			{
				return this.target_type;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.target_type = value;
				this.target_type_name = this.target_type.AssemblyQualifiedName;
			}
		}

		// Token: 0x170003DD RID: 989
		// (get) Token: 0x06001866 RID: 6246 RVA: 0x0005D6B8 File Offset: 0x0005B8B8
		// (set) Token: 0x06001867 RID: 6247 RVA: 0x0005D6C0 File Offset: 0x0005B8C0
		public string TargetTypeName
		{
			get
			{
				return this.target_type_name;
			}
			set
			{
				this.target_type_name = value;
			}
		}

		// Token: 0x170003DE RID: 990
		// (get) Token: 0x06001868 RID: 6248 RVA: 0x0005D6CC File Offset: 0x0005B8CC
		// (set) Token: 0x06001869 RID: 6249 RVA: 0x0005D6D4 File Offset: 0x0005B8D4
		public string Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		// Token: 0x170003DF RID: 991
		// (get) Token: 0x0600186A RID: 6250 RVA: 0x0005D6E0 File Offset: 0x0005B8E0
		// (set) Token: 0x0600186B RID: 6251 RVA: 0x0005D6E8 File Offset: 0x0005B8E8
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x040008E6 RID: 2278
		private string value;

		// Token: 0x040008E7 RID: 2279
		private string type;

		// Token: 0x040008E8 RID: 2280
		private string name;

		// Token: 0x040008E9 RID: 2281
		private string target_type_name;

		// Token: 0x040008EA RID: 2282
		private Type target_type;
	}
}
