﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x020001DC RID: 476
	[MonoTODO("The Debugger class is not functional")]
	[ComVisible(true)]
	public sealed class Debugger
	{
		// Token: 0x170003D9 RID: 985
		// (get) Token: 0x0600185A RID: 6234 RVA: 0x0005D608 File Offset: 0x0005B808
		public static bool IsAttached
		{
			get
			{
				return Debugger.IsAttached_internal();
			}
		}

		// Token: 0x0600185B RID: 6235
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool IsAttached_internal();

		// Token: 0x0600185C RID: 6236 RVA: 0x0005D610 File Offset: 0x0005B810
		public static void Break()
		{
		}

		// Token: 0x0600185D RID: 6237 RVA: 0x0005D614 File Offset: 0x0005B814
		public static bool IsLogging()
		{
			return false;
		}

		// Token: 0x0600185E RID: 6238 RVA: 0x0005D618 File Offset: 0x0005B818
		[MonoTODO("Not implemented")]
		public static bool Launch()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600185F RID: 6239 RVA: 0x0005D620 File Offset: 0x0005B820
		public static void Log(int level, string category, string message)
		{
		}

		// Token: 0x040008E0 RID: 2272
		public static readonly string DefaultCategory = string.Empty;
	}
}
