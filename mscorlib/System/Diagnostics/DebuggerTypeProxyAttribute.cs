﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x020001E3 RID: 483
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct, AllowMultiple = true)]
	public sealed class DebuggerTypeProxyAttribute : Attribute
	{
		// Token: 0x0600186F RID: 6255 RVA: 0x0005D70C File Offset: 0x0005B90C
		public DebuggerTypeProxyAttribute(string typeName)
		{
			this.proxy_type_name = typeName;
		}

		// Token: 0x06001870 RID: 6256 RVA: 0x0005D71C File Offset: 0x0005B91C
		public DebuggerTypeProxyAttribute(Type type)
		{
			this.proxy_type_name = type.Name;
		}

		// Token: 0x170003E0 RID: 992
		// (get) Token: 0x06001871 RID: 6257 RVA: 0x0005D730 File Offset: 0x0005B930
		public string ProxyTypeName
		{
			get
			{
				return this.proxy_type_name;
			}
		}

		// Token: 0x170003E1 RID: 993
		// (get) Token: 0x06001872 RID: 6258 RVA: 0x0005D738 File Offset: 0x0005B938
		// (set) Token: 0x06001873 RID: 6259 RVA: 0x0005D740 File Offset: 0x0005B940
		public Type Target
		{
			get
			{
				return this.target_type;
			}
			set
			{
				this.target_type = value;
				this.target_type_name = this.target_type.Name;
			}
		}

		// Token: 0x170003E2 RID: 994
		// (get) Token: 0x06001874 RID: 6260 RVA: 0x0005D75C File Offset: 0x0005B95C
		// (set) Token: 0x06001875 RID: 6261 RVA: 0x0005D764 File Offset: 0x0005B964
		public string TargetTypeName
		{
			get
			{
				return this.target_type_name;
			}
			set
			{
				this.target_type_name = value;
			}
		}

		// Token: 0x040008EB RID: 2283
		private string proxy_type_name;

		// Token: 0x040008EC RID: 2284
		private string target_type_name;

		// Token: 0x040008ED RID: 2285
		private Type target_type;
	}
}
