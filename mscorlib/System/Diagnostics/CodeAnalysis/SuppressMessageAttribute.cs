﻿using System;

namespace System.Diagnostics.CodeAnalysis
{
	// Token: 0x020001E7 RID: 487
	[AttributeUsage(AttributeTargets.All, Inherited = false, AllowMultiple = true)]
	[Conditional("CODE_ANALYSIS")]
	public sealed class SuppressMessageAttribute : Attribute
	{
		// Token: 0x060018A5 RID: 6309 RVA: 0x0005E0C0 File Offset: 0x0005C2C0
		public SuppressMessageAttribute(string category, string checkId)
		{
			this.category = category;
			this.checkId = checkId;
		}

		// Token: 0x170003E9 RID: 1001
		// (get) Token: 0x060018A6 RID: 6310 RVA: 0x0005E0D8 File Offset: 0x0005C2D8
		public string Category
		{
			get
			{
				return this.category;
			}
		}

		// Token: 0x170003EA RID: 1002
		// (get) Token: 0x060018A7 RID: 6311 RVA: 0x0005E0E0 File Offset: 0x0005C2E0
		public string CheckId
		{
			get
			{
				return this.checkId;
			}
		}

		// Token: 0x170003EB RID: 1003
		// (get) Token: 0x060018A8 RID: 6312 RVA: 0x0005E0E8 File Offset: 0x0005C2E8
		// (set) Token: 0x060018A9 RID: 6313 RVA: 0x0005E0F0 File Offset: 0x0005C2F0
		public string Justification
		{
			get
			{
				return this.justification;
			}
			set
			{
				this.justification = value;
			}
		}

		// Token: 0x170003EC RID: 1004
		// (get) Token: 0x060018AA RID: 6314 RVA: 0x0005E0FC File Offset: 0x0005C2FC
		// (set) Token: 0x060018AB RID: 6315 RVA: 0x0005E104 File Offset: 0x0005C304
		public string MessageId
		{
			get
			{
				return this.messageId;
			}
			set
			{
				this.messageId = value;
			}
		}

		// Token: 0x170003ED RID: 1005
		// (get) Token: 0x060018AC RID: 6316 RVA: 0x0005E110 File Offset: 0x0005C310
		// (set) Token: 0x060018AD RID: 6317 RVA: 0x0005E118 File Offset: 0x0005C318
		public string Scope
		{
			get
			{
				return this.scope;
			}
			set
			{
				this.scope = value;
			}
		}

		// Token: 0x170003EE RID: 1006
		// (get) Token: 0x060018AE RID: 6318 RVA: 0x0005E124 File Offset: 0x0005C324
		// (set) Token: 0x060018AF RID: 6319 RVA: 0x0005E12C File Offset: 0x0005C32C
		public string Target
		{
			get
			{
				return this.target;
			}
			set
			{
				this.target = value;
			}
		}

		// Token: 0x040008FE RID: 2302
		private string category;

		// Token: 0x040008FF RID: 2303
		private string checkId;

		// Token: 0x04000900 RID: 2304
		private string justification;

		// Token: 0x04000901 RID: 2305
		private string messageId;

		// Token: 0x04000902 RID: 2306
		private string scope;

		// Token: 0x04000903 RID: 2307
		private string target;
	}
}
