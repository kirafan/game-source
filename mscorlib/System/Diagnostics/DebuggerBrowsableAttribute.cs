﻿using System;
using System.Runtime.InteropServices;

namespace System.Diagnostics
{
	// Token: 0x020001DE RID: 478
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
	public sealed class DebuggerBrowsableAttribute : Attribute
	{
		// Token: 0x06001860 RID: 6240 RVA: 0x0005D624 File Offset: 0x0005B824
		public DebuggerBrowsableAttribute(DebuggerBrowsableState state)
		{
			this.state = state;
		}

		// Token: 0x170003DA RID: 986
		// (get) Token: 0x06001861 RID: 6241 RVA: 0x0005D634 File Offset: 0x0005B834
		public DebuggerBrowsableState State
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x040008E5 RID: 2277
		private DebuggerBrowsableState state;
	}
}
