﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000191 RID: 401
	[ComVisible(true)]
	[Serializable]
	public class UnauthorizedAccessException : SystemException
	{
		// Token: 0x0600146C RID: 5228 RVA: 0x00052248 File Offset: 0x00050448
		public UnauthorizedAccessException() : base(Locale.GetText("Access to the requested resource is not authorized."))
		{
			base.HResult = -2146233088;
		}

		// Token: 0x0600146D RID: 5229 RVA: 0x00052268 File Offset: 0x00050468
		public UnauthorizedAccessException(string message) : base(message)
		{
			base.HResult = -2146233088;
		}

		// Token: 0x0600146E RID: 5230 RVA: 0x0005227C File Offset: 0x0005047C
		public UnauthorizedAccessException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233088;
		}

		// Token: 0x0600146F RID: 5231 RVA: 0x00052294 File Offset: 0x00050494
		protected UnauthorizedAccessException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000802 RID: 2050
		private const int Result = -2146233088;
	}
}
