﻿using System;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x020006C7 RID: 1735
	[DebuggerDisplay("{value}", Name = "[{key}]")]
	[Serializable]
	public struct KeyValuePair<TKey, TValue>
	{
		// Token: 0x06004271 RID: 17009 RVA: 0x000E3C54 File Offset: 0x000E1E54
		public KeyValuePair(TKey key, TValue value)
		{
			this.Key = key;
			this.Value = value;
		}

		// Token: 0x17000C68 RID: 3176
		// (get) Token: 0x06004272 RID: 17010 RVA: 0x000E3C64 File Offset: 0x000E1E64
		// (set) Token: 0x06004273 RID: 17011 RVA: 0x000E3C6C File Offset: 0x000E1E6C
		public TKey Key
		{
			get
			{
				return this.key;
			}
			private set
			{
				this.key = value;
			}
		}

		// Token: 0x17000C69 RID: 3177
		// (get) Token: 0x06004274 RID: 17012 RVA: 0x000E3C78 File Offset: 0x000E1E78
		// (set) Token: 0x06004275 RID: 17013 RVA: 0x000E3C80 File Offset: 0x000E1E80
		public TValue Value
		{
			get
			{
				return this.value;
			}
			private set
			{
				this.value = value;
			}
		}

		// Token: 0x06004276 RID: 17014 RVA: 0x000E3C8C File Offset: 0x000E1E8C
		public override string ToString()
		{
			string[] array = new string[5];
			array[0] = "[";
			int num = 1;
			string text;
			if (this.Key != null)
			{
				TKey tkey = this.Key;
				text = tkey.ToString();
			}
			else
			{
				text = string.Empty;
			}
			array[num] = text;
			array[2] = ", ";
			int num2 = 3;
			string text2;
			if (this.Value != null)
			{
				TValue tvalue = this.Value;
				text2 = tvalue.ToString();
			}
			else
			{
				text2 = string.Empty;
			}
			array[num2] = text2;
			array[4] = "]";
			return string.Concat(array);
		}

		// Token: 0x04001C47 RID: 7239
		private TKey key;

		// Token: 0x04001C48 RID: 7240
		private TValue value;
	}
}
