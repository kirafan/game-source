﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x02000030 RID: 48
	public interface ICollection<T> : IEnumerable, IEnumerable<T>
	{
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060004B2 RID: 1202
		int Count { get; }

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060004B3 RID: 1203
		bool IsReadOnly { get; }

		// Token: 0x060004B4 RID: 1204
		void Add(T item);

		// Token: 0x060004B5 RID: 1205
		void Clear();

		// Token: 0x060004B6 RID: 1206
		bool Contains(T item);

		// Token: 0x060004B7 RID: 1207
		void CopyTo(T[] array, int arrayIndex);

		// Token: 0x060004B8 RID: 1208
		bool Remove(T item);
	}
}
