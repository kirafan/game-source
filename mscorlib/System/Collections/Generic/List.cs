﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;

namespace System.Collections.Generic
{
	// Token: 0x020006CC RID: 1740
	[DebuggerDisplay("Count={Count}")]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView<>))]
	[Serializable]
	public class List<T> : IEnumerable, ICollection, IList, ICollection<T>, IEnumerable<T>, IList<T>
	{
		// Token: 0x06004288 RID: 17032 RVA: 0x000E3E9C File Offset: 0x000E209C
		public List()
		{
			this._items = List<T>.EmptyArray;
		}

		// Token: 0x06004289 RID: 17033 RVA: 0x000E3EB0 File Offset: 0x000E20B0
		public List(IEnumerable<T> collection)
		{
			this.CheckCollection(collection);
			ICollection<T> collection2 = collection as ICollection<T>;
			if (collection2 == null)
			{
				this._items = List<T>.EmptyArray;
				this.AddEnumerable(collection);
			}
			else
			{
				this._items = new T[collection2.Count];
				this.AddCollection(collection2);
			}
		}

		// Token: 0x0600428A RID: 17034 RVA: 0x000E3F08 File Offset: 0x000E2108
		public List(int capacity)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity");
			}
			this._items = new T[capacity];
		}

		// Token: 0x0600428B RID: 17035 RVA: 0x000E3F3C File Offset: 0x000E213C
		internal List(T[] data, int size)
		{
			this._items = data;
			this._size = size;
		}

		// Token: 0x0600428D RID: 17037 RVA: 0x000E3F64 File Offset: 0x000E2164
		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x0600428E RID: 17038 RVA: 0x000E3F74 File Offset: 0x000E2174
		void ICollection.CopyTo(Array array, int arrayIndex)
		{
			Array.Copy(this._items, 0, array, arrayIndex, this._size);
		}

		// Token: 0x0600428F RID: 17039 RVA: 0x000E3F8C File Offset: 0x000E218C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06004290 RID: 17040 RVA: 0x000E3F9C File Offset: 0x000E219C
		int IList.Add(object item)
		{
			try
			{
				this.Add((T)((object)item));
				return this._size - 1;
			}
			catch (NullReferenceException)
			{
			}
			catch (InvalidCastException)
			{
			}
			throw new ArgumentException("item");
		}

		// Token: 0x06004291 RID: 17041 RVA: 0x000E4014 File Offset: 0x000E2214
		bool IList.Contains(object item)
		{
			try
			{
				return this.Contains((T)((object)item));
			}
			catch (NullReferenceException)
			{
			}
			catch (InvalidCastException)
			{
			}
			return false;
		}

		// Token: 0x06004292 RID: 17042 RVA: 0x000E407C File Offset: 0x000E227C
		int IList.IndexOf(object item)
		{
			try
			{
				return this.IndexOf((T)((object)item));
			}
			catch (NullReferenceException)
			{
			}
			catch (InvalidCastException)
			{
			}
			return -1;
		}

		// Token: 0x06004293 RID: 17043 RVA: 0x000E40E4 File Offset: 0x000E22E4
		void IList.Insert(int index, object item)
		{
			this.CheckIndex(index);
			try
			{
				this.Insert(index, (T)((object)item));
				return;
			}
			catch (NullReferenceException)
			{
			}
			catch (InvalidCastException)
			{
			}
			throw new ArgumentException("item");
		}

		// Token: 0x06004294 RID: 17044 RVA: 0x000E415C File Offset: 0x000E235C
		void IList.Remove(object item)
		{
			try
			{
				this.Remove((T)((object)item));
			}
			catch (NullReferenceException)
			{
			}
			catch (InvalidCastException)
			{
			}
		}

		// Token: 0x17000C6B RID: 3179
		// (get) Token: 0x06004295 RID: 17045 RVA: 0x000E41C0 File Offset: 0x000E23C0
		bool ICollection<T>.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000C6C RID: 3180
		// (get) Token: 0x06004296 RID: 17046 RVA: 0x000E41C4 File Offset: 0x000E23C4
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000C6D RID: 3181
		// (get) Token: 0x06004297 RID: 17047 RVA: 0x000E41C8 File Offset: 0x000E23C8
		object ICollection.SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000C6E RID: 3182
		// (get) Token: 0x06004298 RID: 17048 RVA: 0x000E41CC File Offset: 0x000E23CC
		bool IList.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000C6F RID: 3183
		// (get) Token: 0x06004299 RID: 17049 RVA: 0x000E41D0 File Offset: 0x000E23D0
		bool IList.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000C70 RID: 3184
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				try
				{
					this[index] = (T)((object)value);
					return;
				}
				catch (NullReferenceException)
				{
				}
				catch (InvalidCastException)
				{
				}
				throw new ArgumentException("value");
			}
		}

		// Token: 0x0600429C RID: 17052 RVA: 0x000E4254 File Offset: 0x000E2454
		public void Add(T item)
		{
			if (this._size == this._items.Length)
			{
				this.GrowIfNeeded(1);
			}
			this._items[this._size++] = item;
			this._version++;
		}

		// Token: 0x0600429D RID: 17053 RVA: 0x000E42A8 File Offset: 0x000E24A8
		private void GrowIfNeeded(int newCount)
		{
			int num = this._size + newCount;
			if (num > this._items.Length)
			{
				this.Capacity = Math.Max(Math.Max(this.Capacity * 2, 4), num);
			}
		}

		// Token: 0x0600429E RID: 17054 RVA: 0x000E42E8 File Offset: 0x000E24E8
		private void CheckRange(int idx, int count)
		{
			if (idx < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (idx + count > this._size)
			{
				throw new ArgumentException("index and count exceed length of list");
			}
		}

		// Token: 0x0600429F RID: 17055 RVA: 0x000E4328 File Offset: 0x000E2528
		private void AddCollection(ICollection<T> collection)
		{
			int count = collection.Count;
			if (count == 0)
			{
				return;
			}
			this.GrowIfNeeded(count);
			collection.CopyTo(this._items, this._size);
			this._size += count;
		}

		// Token: 0x060042A0 RID: 17056 RVA: 0x000E436C File Offset: 0x000E256C
		private void AddEnumerable(IEnumerable<T> enumerable)
		{
			foreach (T item in enumerable)
			{
				this.Add(item);
			}
		}

		// Token: 0x060042A1 RID: 17057 RVA: 0x000E43CC File Offset: 0x000E25CC
		public void AddRange(IEnumerable<T> collection)
		{
			this.CheckCollection(collection);
			ICollection<T> collection2 = collection as ICollection<T>;
			if (collection2 != null)
			{
				this.AddCollection(collection2);
			}
			else
			{
				this.AddEnumerable(collection);
			}
			this._version++;
		}

		// Token: 0x060042A2 RID: 17058 RVA: 0x000E4410 File Offset: 0x000E2610
		public ReadOnlyCollection<T> AsReadOnly()
		{
			return new ReadOnlyCollection<T>(this);
		}

		// Token: 0x060042A3 RID: 17059 RVA: 0x000E4418 File Offset: 0x000E2618
		public int BinarySearch(T item)
		{
			return Array.BinarySearch<T>(this._items, 0, this._size, item);
		}

		// Token: 0x060042A4 RID: 17060 RVA: 0x000E4430 File Offset: 0x000E2630
		public int BinarySearch(T item, IComparer<T> comparer)
		{
			return Array.BinarySearch<T>(this._items, 0, this._size, item, comparer);
		}

		// Token: 0x060042A5 RID: 17061 RVA: 0x000E4448 File Offset: 0x000E2648
		public int BinarySearch(int index, int count, T item, IComparer<T> comparer)
		{
			this.CheckRange(index, count);
			return Array.BinarySearch<T>(this._items, index, count, item, comparer);
		}

		// Token: 0x060042A6 RID: 17062 RVA: 0x000E4470 File Offset: 0x000E2670
		public void Clear()
		{
			Array.Clear(this._items, 0, this._items.Length);
			this._size = 0;
			this._version++;
		}

		// Token: 0x060042A7 RID: 17063 RVA: 0x000E449C File Offset: 0x000E269C
		public bool Contains(T item)
		{
			return Array.IndexOf<T>(this._items, item, 0, this._size) != -1;
		}

		// Token: 0x060042A8 RID: 17064 RVA: 0x000E44B8 File Offset: 0x000E26B8
		public List<TOutput> ConvertAll<TOutput>(Converter<T, TOutput> converter)
		{
			if (converter == null)
			{
				throw new ArgumentNullException("converter");
			}
			List<TOutput> list = new List<TOutput>(this._size);
			for (int i = 0; i < this._size; i++)
			{
				list._items[i] = converter(this._items[i]);
			}
			list._size = this._size;
			return list;
		}

		// Token: 0x060042A9 RID: 17065 RVA: 0x000E4524 File Offset: 0x000E2724
		public void CopyTo(T[] array)
		{
			Array.Copy(this._items, 0, array, 0, this._size);
		}

		// Token: 0x060042AA RID: 17066 RVA: 0x000E453C File Offset: 0x000E273C
		public void CopyTo(T[] array, int arrayIndex)
		{
			Array.Copy(this._items, 0, array, arrayIndex, this._size);
		}

		// Token: 0x060042AB RID: 17067 RVA: 0x000E4554 File Offset: 0x000E2754
		public void CopyTo(int index, T[] array, int arrayIndex, int count)
		{
			this.CheckRange(index, count);
			Array.Copy(this._items, index, array, arrayIndex, count);
		}

		// Token: 0x060042AC RID: 17068 RVA: 0x000E457C File Offset: 0x000E277C
		public bool Exists(Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			return this.GetIndex(0, this._size, match) != -1;
		}

		// Token: 0x060042AD RID: 17069 RVA: 0x000E4598 File Offset: 0x000E2798
		public T Find(Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			int index = this.GetIndex(0, this._size, match);
			return (index == -1) ? default(T) : this._items[index];
		}

		// Token: 0x060042AE RID: 17070 RVA: 0x000E45DC File Offset: 0x000E27DC
		private static void CheckMatch(Predicate<T> match)
		{
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
		}

		// Token: 0x060042AF RID: 17071 RVA: 0x000E45F0 File Offset: 0x000E27F0
		public List<T> FindAll(Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			if (this._size <= 65536)
			{
				return this.FindAllStackBits(match);
			}
			return this.FindAllList(match);
		}

		// Token: 0x060042B0 RID: 17072 RVA: 0x000E4618 File Offset: 0x000E2818
		private unsafe List<T> FindAllStackBits(Predicate<T> match)
		{
			uint* ptr = stackalloc uint[checked(unchecked(this._size / 32 + 1) * 4)];
			uint* ptr2 = ptr;
			int num = 0;
			uint num2 = 2147483648U;
			for (int i = 0; i < this._size; i++)
			{
				if (match(this._items[i]))
				{
					*ptr2 |= num2;
					num++;
				}
				num2 >>= 1;
				if (num2 == 0U)
				{
					ptr2++;
					num2 = 2147483648U;
				}
			}
			T[] array = new T[num];
			num2 = 2147483648U;
			ptr2 = ptr;
			int num3 = 0;
			int num4 = 0;
			while (num4 < this._size && num3 < num)
			{
				if ((*ptr2 & num2) == num2)
				{
					array[num3++] = this._items[num4];
				}
				num2 >>= 1;
				if (num2 == 0U)
				{
					ptr2++;
					num2 = 2147483648U;
				}
				num4++;
			}
			return new List<T>(array, num);
		}

		// Token: 0x060042B1 RID: 17073 RVA: 0x000E470C File Offset: 0x000E290C
		private List<T> FindAllList(Predicate<T> match)
		{
			List<T> list = new List<T>();
			for (int i = 0; i < this._size; i++)
			{
				if (match(this._items[i]))
				{
					list.Add(this._items[i]);
				}
			}
			return list;
		}

		// Token: 0x060042B2 RID: 17074 RVA: 0x000E4760 File Offset: 0x000E2960
		public int FindIndex(Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			return this.GetIndex(0, this._size, match);
		}

		// Token: 0x060042B3 RID: 17075 RVA: 0x000E4778 File Offset: 0x000E2978
		public int FindIndex(int startIndex, Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			this.CheckIndex(startIndex);
			return this.GetIndex(startIndex, this._size - startIndex, match);
		}

		// Token: 0x060042B4 RID: 17076 RVA: 0x000E47A4 File Offset: 0x000E29A4
		public int FindIndex(int startIndex, int count, Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			this.CheckRange(startIndex, count);
			return this.GetIndex(startIndex, count, match);
		}

		// Token: 0x060042B5 RID: 17077 RVA: 0x000E47C8 File Offset: 0x000E29C8
		private int GetIndex(int startIndex, int count, Predicate<T> match)
		{
			int num = startIndex + count;
			for (int i = startIndex; i < num; i++)
			{
				if (match(this._items[i]))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x060042B6 RID: 17078 RVA: 0x000E4808 File Offset: 0x000E2A08
		public T FindLast(Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			int lastIndex = this.GetLastIndex(0, this._size, match);
			return (lastIndex != -1) ? this[lastIndex] : default(T);
		}

		// Token: 0x060042B7 RID: 17079 RVA: 0x000E4848 File Offset: 0x000E2A48
		public int FindLastIndex(Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			return this.GetLastIndex(0, this._size, match);
		}

		// Token: 0x060042B8 RID: 17080 RVA: 0x000E4860 File Offset: 0x000E2A60
		public int FindLastIndex(int startIndex, Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			this.CheckIndex(startIndex);
			return this.GetLastIndex(0, startIndex + 1, match);
		}

		// Token: 0x060042B9 RID: 17081 RVA: 0x000E4888 File Offset: 0x000E2A88
		public int FindLastIndex(int startIndex, int count, Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			int num = startIndex - count + 1;
			this.CheckRange(num, count);
			return this.GetLastIndex(num, count, match);
		}

		// Token: 0x060042BA RID: 17082 RVA: 0x000E48B4 File Offset: 0x000E2AB4
		private int GetLastIndex(int startIndex, int count, Predicate<T> match)
		{
			int num = startIndex + count;
			while (num != startIndex)
			{
				if (match(this._items[--num]))
				{
					return num;
				}
			}
			return -1;
		}

		// Token: 0x060042BB RID: 17083 RVA: 0x000E48F0 File Offset: 0x000E2AF0
		public void ForEach(Action<T> action)
		{
			if (action == null)
			{
				throw new ArgumentNullException("action");
			}
			for (int i = 0; i < this._size; i++)
			{
				action(this._items[i]);
			}
		}

		// Token: 0x060042BC RID: 17084 RVA: 0x000E4938 File Offset: 0x000E2B38
		public List<T>.Enumerator GetEnumerator()
		{
			return new List<T>.Enumerator(this);
		}

		// Token: 0x060042BD RID: 17085 RVA: 0x000E4940 File Offset: 0x000E2B40
		public List<T> GetRange(int index, int count)
		{
			this.CheckRange(index, count);
			T[] array = new T[count];
			Array.Copy(this._items, index, array, 0, count);
			return new List<T>(array, count);
		}

		// Token: 0x060042BE RID: 17086 RVA: 0x000E4974 File Offset: 0x000E2B74
		public int IndexOf(T item)
		{
			return Array.IndexOf<T>(this._items, item, 0, this._size);
		}

		// Token: 0x060042BF RID: 17087 RVA: 0x000E498C File Offset: 0x000E2B8C
		public int IndexOf(T item, int index)
		{
			this.CheckIndex(index);
			return Array.IndexOf<T>(this._items, item, index, this._size - index);
		}

		// Token: 0x060042C0 RID: 17088 RVA: 0x000E49B8 File Offset: 0x000E2BB8
		public int IndexOf(T item, int index, int count)
		{
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if (index + count > this._size)
			{
				throw new ArgumentOutOfRangeException("index and count exceed length of list");
			}
			return Array.IndexOf<T>(this._items, item, index, count);
		}

		// Token: 0x060042C1 RID: 17089 RVA: 0x000E4A10 File Offset: 0x000E2C10
		private void Shift(int start, int delta)
		{
			if (delta < 0)
			{
				start -= delta;
			}
			if (start < this._size)
			{
				Array.Copy(this._items, start, this._items, start + delta, this._size - start);
			}
			this._size += delta;
			if (delta < 0)
			{
				Array.Clear(this._items, this._size, -delta);
			}
		}

		// Token: 0x060042C2 RID: 17090 RVA: 0x000E4A7C File Offset: 0x000E2C7C
		private void CheckIndex(int index)
		{
			if (index < 0 || index > this._size)
			{
				throw new ArgumentOutOfRangeException("index");
			}
		}

		// Token: 0x060042C3 RID: 17091 RVA: 0x000E4A9C File Offset: 0x000E2C9C
		public void Insert(int index, T item)
		{
			this.CheckIndex(index);
			if (this._size == this._items.Length)
			{
				this.GrowIfNeeded(1);
			}
			this.Shift(index, 1);
			this._items[index] = item;
			this._version++;
		}

		// Token: 0x060042C4 RID: 17092 RVA: 0x000E4AF0 File Offset: 0x000E2CF0
		private void CheckCollection(IEnumerable<T> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
		}

		// Token: 0x060042C5 RID: 17093 RVA: 0x000E4B04 File Offset: 0x000E2D04
		public void InsertRange(int index, IEnumerable<T> collection)
		{
			this.CheckCollection(collection);
			this.CheckIndex(index);
			if (collection == this)
			{
				T[] array = new T[this._size];
				this.CopyTo(array, 0);
				this.GrowIfNeeded(this._size);
				this.Shift(index, array.Length);
				Array.Copy(array, 0, this._items, index, array.Length);
			}
			else
			{
				ICollection<T> collection2 = collection as ICollection<T>;
				if (collection2 != null)
				{
					this.InsertCollection(index, collection2);
				}
				else
				{
					this.InsertEnumeration(index, collection);
				}
			}
			this._version++;
		}

		// Token: 0x060042C6 RID: 17094 RVA: 0x000E4B98 File Offset: 0x000E2D98
		private void InsertCollection(int index, ICollection<T> collection)
		{
			int count = collection.Count;
			this.GrowIfNeeded(count);
			this.Shift(index, count);
			collection.CopyTo(this._items, index);
		}

		// Token: 0x060042C7 RID: 17095 RVA: 0x000E4BC8 File Offset: 0x000E2DC8
		private void InsertEnumeration(int index, IEnumerable<T> enumerable)
		{
			foreach (T item in enumerable)
			{
				this.Insert(index++, item);
			}
		}

		// Token: 0x060042C8 RID: 17096 RVA: 0x000E4C2C File Offset: 0x000E2E2C
		public int LastIndexOf(T item)
		{
			return Array.LastIndexOf<T>(this._items, item, this._size - 1, this._size);
		}

		// Token: 0x060042C9 RID: 17097 RVA: 0x000E4C48 File Offset: 0x000E2E48
		public int LastIndexOf(T item, int index)
		{
			this.CheckIndex(index);
			return Array.LastIndexOf<T>(this._items, item, index, index + 1);
		}

		// Token: 0x060042CA RID: 17098 RVA: 0x000E4C6C File Offset: 0x000E2E6C
		public int LastIndexOf(T item, int index, int count)
		{
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", index, "index is negative");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", count, "count is negative");
			}
			if (index - count + 1 < 0)
			{
				throw new ArgumentOutOfRangeException("cound", count, "count is too large");
			}
			return Array.LastIndexOf<T>(this._items, item, index, count);
		}

		// Token: 0x060042CB RID: 17099 RVA: 0x000E4CE4 File Offset: 0x000E2EE4
		public bool Remove(T item)
		{
			int num = this.IndexOf(item);
			if (num != -1)
			{
				this.RemoveAt(num);
			}
			return num != -1;
		}

		// Token: 0x060042CC RID: 17100 RVA: 0x000E4D10 File Offset: 0x000E2F10
		public int RemoveAll(Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			int i;
			for (i = 0; i < this._size; i++)
			{
				if (match(this._items[i]))
				{
					break;
				}
			}
			if (i == this._size)
			{
				return 0;
			}
			this._version++;
			int j;
			for (j = i + 1; j < this._size; j++)
			{
				if (!match(this._items[j]))
				{
					this._items[i++] = this._items[j];
				}
			}
			if (j - i > 0)
			{
				Array.Clear(this._items, i, j - i);
			}
			this._size = i;
			return j - i;
		}

		// Token: 0x060042CD RID: 17101 RVA: 0x000E4DE4 File Offset: 0x000E2FE4
		public void RemoveAt(int index)
		{
			if (index < 0 || index >= this._size)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			this.Shift(index, -1);
			Array.Clear(this._items, this._size, 1);
			this._version++;
		}

		// Token: 0x060042CE RID: 17102 RVA: 0x000E4E38 File Offset: 0x000E3038
		public void RemoveRange(int index, int count)
		{
			this.CheckRange(index, count);
			if (count > 0)
			{
				this.Shift(index, -count);
				Array.Clear(this._items, this._size, count);
				this._version++;
			}
		}

		// Token: 0x060042CF RID: 17103 RVA: 0x000E4E80 File Offset: 0x000E3080
		public void Reverse()
		{
			Array.Reverse(this._items, 0, this._size);
			this._version++;
		}

		// Token: 0x060042D0 RID: 17104 RVA: 0x000E4EB0 File Offset: 0x000E30B0
		public void Reverse(int index, int count)
		{
			this.CheckRange(index, count);
			Array.Reverse(this._items, index, count);
			this._version++;
		}

		// Token: 0x060042D1 RID: 17105 RVA: 0x000E4EE0 File Offset: 0x000E30E0
		public void Sort()
		{
			Array.Sort<T>(this._items, 0, this._size, Comparer<T>.Default);
			this._version++;
		}

		// Token: 0x060042D2 RID: 17106 RVA: 0x000E4F08 File Offset: 0x000E3108
		public void Sort(IComparer<T> comparer)
		{
			Array.Sort<T>(this._items, 0, this._size, comparer);
			this._version++;
		}

		// Token: 0x060042D3 RID: 17107 RVA: 0x000E4F2C File Offset: 0x000E312C
		public void Sort(Comparison<T> comparison)
		{
			Array.Sort<T>(this._items, this._size, comparison);
			this._version++;
		}

		// Token: 0x060042D4 RID: 17108 RVA: 0x000E4F5C File Offset: 0x000E315C
		public void Sort(int index, int count, IComparer<T> comparer)
		{
			this.CheckRange(index, count);
			Array.Sort<T>(this._items, index, count, comparer);
			this._version++;
		}

		// Token: 0x060042D5 RID: 17109 RVA: 0x000E4F90 File Offset: 0x000E3190
		public T[] ToArray()
		{
			T[] array = new T[this._size];
			Array.Copy(this._items, array, this._size);
			return array;
		}

		// Token: 0x060042D6 RID: 17110 RVA: 0x000E4FBC File Offset: 0x000E31BC
		public void TrimExcess()
		{
			this.Capacity = this._size;
		}

		// Token: 0x060042D7 RID: 17111 RVA: 0x000E4FCC File Offset: 0x000E31CC
		public bool TrueForAll(Predicate<T> match)
		{
			List<T>.CheckMatch(match);
			for (int i = 0; i < this._size; i++)
			{
				if (!match(this._items[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x17000C71 RID: 3185
		// (get) Token: 0x060042D8 RID: 17112 RVA: 0x000E5010 File Offset: 0x000E3210
		// (set) Token: 0x060042D9 RID: 17113 RVA: 0x000E501C File Offset: 0x000E321C
		public int Capacity
		{
			get
			{
				return this._items.Length;
			}
			set
			{
				if (value < this._size)
				{
					throw new ArgumentOutOfRangeException();
				}
				Array.Resize<T>(ref this._items, value);
			}
		}

		// Token: 0x17000C72 RID: 3186
		// (get) Token: 0x060042DA RID: 17114 RVA: 0x000E503C File Offset: 0x000E323C
		public int Count
		{
			get
			{
				return this._size;
			}
		}

		// Token: 0x17000C73 RID: 3187
		public T this[int index]
		{
			get
			{
				if (index >= this._size)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				return this._items[index];
			}
			set
			{
				this.CheckIndex(index);
				if (index == this._size)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				this._items[index] = value;
			}
		}

		// Token: 0x04001C4A RID: 7242
		private const int DefaultCapacity = 4;

		// Token: 0x04001C4B RID: 7243
		private T[] _items;

		// Token: 0x04001C4C RID: 7244
		private int _size;

		// Token: 0x04001C4D RID: 7245
		private int _version;

		// Token: 0x04001C4E RID: 7246
		private static readonly T[] EmptyArray = new T[0];

		// Token: 0x020006CD RID: 1741
		[Serializable]
		public struct Enumerator : IEnumerator, IDisposable, IEnumerator<T>
		{
			// Token: 0x060042DD RID: 17117 RVA: 0x000E509C File Offset: 0x000E329C
			internal Enumerator(List<T> l)
			{
				this.l = l;
				this.ver = l._version;
			}

			// Token: 0x060042DE RID: 17118 RVA: 0x000E50B4 File Offset: 0x000E32B4
			void IEnumerator.Reset()
			{
				this.VerifyState();
				this.next = 0;
			}

			// Token: 0x17000C74 RID: 3188
			// (get) Token: 0x060042DF RID: 17119 RVA: 0x000E50C4 File Offset: 0x000E32C4
			object IEnumerator.Current
			{
				get
				{
					this.VerifyState();
					if (this.next <= 0)
					{
						throw new InvalidOperationException();
					}
					return this.current;
				}
			}

			// Token: 0x060042E0 RID: 17120 RVA: 0x000E50EC File Offset: 0x000E32EC
			public void Dispose()
			{
				this.l = null;
			}

			// Token: 0x060042E1 RID: 17121 RVA: 0x000E50F8 File Offset: 0x000E32F8
			private void VerifyState()
			{
				if (this.l == null)
				{
					throw new ObjectDisposedException(base.GetType().FullName);
				}
				if (this.ver != this.l._version)
				{
					throw new InvalidOperationException("Collection was modified; enumeration operation may not execute.");
				}
			}

			// Token: 0x060042E2 RID: 17122 RVA: 0x000E514C File Offset: 0x000E334C
			public bool MoveNext()
			{
				this.VerifyState();
				if (this.next < 0)
				{
					return false;
				}
				if (this.next < this.l._size)
				{
					this.current = this.l._items[this.next++];
					return true;
				}
				this.next = -1;
				return false;
			}

			// Token: 0x17000C75 RID: 3189
			// (get) Token: 0x060042E3 RID: 17123 RVA: 0x000E51B4 File Offset: 0x000E33B4
			public T Current
			{
				get
				{
					return this.current;
				}
			}

			// Token: 0x04001C4F RID: 7247
			private List<T> l;

			// Token: 0x04001C50 RID: 7248
			private int next;

			// Token: 0x04001C51 RID: 7249
			private int ver;

			// Token: 0x04001C52 RID: 7250
			private T current;
		}
	}
}
