﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x0200001A RID: 26
	public interface IEnumerator<T> : IEnumerator, IDisposable
	{
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000174 RID: 372
		T Current { get; }
	}
}
