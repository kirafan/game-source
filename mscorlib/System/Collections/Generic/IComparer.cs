﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020006C4 RID: 1732
	public interface IComparer<T>
	{
		// Token: 0x06004266 RID: 16998
		int Compare(T x, T y);
	}
}
