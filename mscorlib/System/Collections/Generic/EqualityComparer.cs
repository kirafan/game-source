﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020006C8 RID: 1736
	[Serializable]
	public abstract class EqualityComparer<T> : IEqualityComparer<T>, IEqualityComparer
	{
		// Token: 0x06004278 RID: 17016 RVA: 0x000E3D28 File Offset: 0x000E1F28
		static EqualityComparer()
		{
			if (typeof(IEquatable<T>).IsAssignableFrom(typeof(T)))
			{
				EqualityComparer<T>._default = (EqualityComparer<T>)Activator.CreateInstance(typeof(GenericEqualityComparer<>).MakeGenericType(new Type[]
				{
					typeof(T)
				}));
			}
			else
			{
				EqualityComparer<T>._default = new EqualityComparer<T>.DefaultComparer();
			}
		}

		// Token: 0x06004279 RID: 17017 RVA: 0x000E3D94 File Offset: 0x000E1F94
		int IEqualityComparer.GetHashCode(object obj)
		{
			return this.GetHashCode((T)((object)obj));
		}

		// Token: 0x0600427A RID: 17018 RVA: 0x000E3DA4 File Offset: 0x000E1FA4
		bool IEqualityComparer.Equals(object x, object y)
		{
			return this.Equals((T)((object)x), (T)((object)y));
		}

		// Token: 0x0600427B RID: 17019
		public abstract int GetHashCode(T obj);

		// Token: 0x0600427C RID: 17020
		public abstract bool Equals(T x, T y);

		// Token: 0x17000C6A RID: 3178
		// (get) Token: 0x0600427D RID: 17021 RVA: 0x000E3DB8 File Offset: 0x000E1FB8
		public static EqualityComparer<T> Default
		{
			get
			{
				return EqualityComparer<T>._default;
			}
		}

		// Token: 0x04001C49 RID: 7241
		private static readonly EqualityComparer<T> _default;

		// Token: 0x020006C9 RID: 1737
		[Serializable]
		private sealed class DefaultComparer : EqualityComparer<T>
		{
			// Token: 0x0600427F RID: 17023 RVA: 0x000E3DC8 File Offset: 0x000E1FC8
			public override int GetHashCode(T obj)
			{
				if (obj == null)
				{
					return 0;
				}
				return obj.GetHashCode();
			}

			// Token: 0x06004280 RID: 17024 RVA: 0x000E3DE4 File Offset: 0x000E1FE4
			public override bool Equals(T x, T y)
			{
				if (x == null)
				{
					return y == null;
				}
				return x.Equals(y);
			}
		}
	}
}
