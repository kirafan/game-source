﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Collections.Generic
{
	// Token: 0x020006BD RID: 1725
	[DebuggerDisplay("Count={Count}")]
	[ComVisible(false)]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView<, >))]
	[Serializable]
	public class Dictionary<TKey, TValue> : IEnumerable, ISerializable, ICollection, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>, IDictionary<TKey, TValue>, IDictionary, IDeserializationCallback
	{
		// Token: 0x060041EE RID: 16878 RVA: 0x000E2558 File Offset: 0x000E0758
		public Dictionary()
		{
			this.Init(10, null);
		}

		// Token: 0x060041EF RID: 16879 RVA: 0x000E256C File Offset: 0x000E076C
		public Dictionary(IEqualityComparer<TKey> comparer)
		{
			this.Init(10, comparer);
		}

		// Token: 0x060041F0 RID: 16880 RVA: 0x000E2580 File Offset: 0x000E0780
		public Dictionary(IDictionary<TKey, TValue> dictionary) : this(dictionary, null)
		{
		}

		// Token: 0x060041F1 RID: 16881 RVA: 0x000E258C File Offset: 0x000E078C
		public Dictionary(int capacity)
		{
			this.Init(capacity, null);
		}

		// Token: 0x060041F2 RID: 16882 RVA: 0x000E259C File Offset: 0x000E079C
		public Dictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
		{
			if (dictionary == null)
			{
				throw new ArgumentNullException("dictionary");
			}
			int capacity = dictionary.Count;
			this.Init(capacity, comparer);
			foreach (KeyValuePair<TKey, TValue> keyValuePair in dictionary)
			{
				this.Add(keyValuePair.Key, keyValuePair.Value);
			}
		}

		// Token: 0x060041F3 RID: 16883 RVA: 0x000E2630 File Offset: 0x000E0830
		public Dictionary(int capacity, IEqualityComparer<TKey> comparer)
		{
			this.Init(capacity, comparer);
		}

		// Token: 0x060041F4 RID: 16884 RVA: 0x000E2640 File Offset: 0x000E0840
		protected Dictionary(SerializationInfo info, StreamingContext context)
		{
			this.serialization_info = info;
		}

		// Token: 0x17000C3F RID: 3135
		// (get) Token: 0x060041F5 RID: 16885 RVA: 0x000E2650 File Offset: 0x000E0850
		ICollection<TKey> IDictionary<TKey, TValue>.Keys
		{
			get
			{
				return this.Keys;
			}
		}

		// Token: 0x17000C40 RID: 3136
		// (get) Token: 0x060041F6 RID: 16886 RVA: 0x000E2658 File Offset: 0x000E0858
		ICollection<TValue> IDictionary<TKey, TValue>.Values
		{
			get
			{
				return this.Values;
			}
		}

		// Token: 0x17000C41 RID: 3137
		// (get) Token: 0x060041F7 RID: 16887 RVA: 0x000E2660 File Offset: 0x000E0860
		ICollection IDictionary.Keys
		{
			get
			{
				return this.Keys;
			}
		}

		// Token: 0x17000C42 RID: 3138
		// (get) Token: 0x060041F8 RID: 16888 RVA: 0x000E2668 File Offset: 0x000E0868
		ICollection IDictionary.Values
		{
			get
			{
				return this.Values;
			}
		}

		// Token: 0x17000C43 RID: 3139
		// (get) Token: 0x060041F9 RID: 16889 RVA: 0x000E2670 File Offset: 0x000E0870
		bool IDictionary.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000C44 RID: 3140
		// (get) Token: 0x060041FA RID: 16890 RVA: 0x000E2674 File Offset: 0x000E0874
		bool IDictionary.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000C45 RID: 3141
		object IDictionary.this[object key]
		{
			get
			{
				if (key is TKey && this.ContainsKey((TKey)((object)key)))
				{
					return this[this.ToTKey(key)];
				}
				return null;
			}
			set
			{
				this[this.ToTKey(key)] = this.ToTValue(value);
			}
		}

		// Token: 0x060041FD RID: 16893 RVA: 0x000E26D0 File Offset: 0x000E08D0
		void IDictionary.Add(object key, object value)
		{
			this.Add(this.ToTKey(key), this.ToTValue(value));
		}

		// Token: 0x060041FE RID: 16894 RVA: 0x000E26E8 File Offset: 0x000E08E8
		bool IDictionary.Contains(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			return key is TKey && this.ContainsKey((TKey)((object)key));
		}

		// Token: 0x060041FF RID: 16895 RVA: 0x000E2720 File Offset: 0x000E0920
		void IDictionary.Remove(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			if (key is TKey)
			{
				this.Remove((TKey)((object)key));
			}
		}

		// Token: 0x17000C46 RID: 3142
		// (get) Token: 0x06004200 RID: 16896 RVA: 0x000E274C File Offset: 0x000E094C
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000C47 RID: 3143
		// (get) Token: 0x06004201 RID: 16897 RVA: 0x000E2750 File Offset: 0x000E0950
		object ICollection.SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000C48 RID: 3144
		// (get) Token: 0x06004202 RID: 16898 RVA: 0x000E2754 File Offset: 0x000E0954
		bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06004203 RID: 16899 RVA: 0x000E2758 File Offset: 0x000E0958
		void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> keyValuePair)
		{
			this.Add(keyValuePair.Key, keyValuePair.Value);
		}

		// Token: 0x06004204 RID: 16900 RVA: 0x000E2770 File Offset: 0x000E0970
		bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> keyValuePair)
		{
			return this.ContainsKeyValuePair(keyValuePair);
		}

		// Token: 0x06004205 RID: 16901 RVA: 0x000E277C File Offset: 0x000E097C
		void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
		{
			this.CopyTo(array, index);
		}

		// Token: 0x06004206 RID: 16902 RVA: 0x000E2788 File Offset: 0x000E0988
		bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> keyValuePair)
		{
			return this.ContainsKeyValuePair(keyValuePair) && this.Remove(keyValuePair.Key);
		}

		// Token: 0x06004207 RID: 16903 RVA: 0x000E27A8 File Offset: 0x000E09A8
		void ICollection.CopyTo(Array array, int index)
		{
			KeyValuePair<TKey, TValue>[] array2 = array as KeyValuePair<TKey, TValue>[];
			if (array2 != null)
			{
				this.CopyTo(array2, index);
				return;
			}
			this.CopyToCheck(array, index);
			DictionaryEntry[] array3 = array as DictionaryEntry[];
			if (array3 != null)
			{
				this.Do_CopyTo<DictionaryEntry, DictionaryEntry>(array3, index, (TKey key, TValue value) => new DictionaryEntry(key, value));
				return;
			}
			this.Do_ICollectionCopyTo<KeyValuePair<TKey, TValue>>(array, index, new Dictionary<TKey, TValue>.Transform<KeyValuePair<TKey, TValue>>(Dictionary<TKey, TValue>.make_pair));
		}

		// Token: 0x06004208 RID: 16904 RVA: 0x000E281C File Offset: 0x000E0A1C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new Dictionary<TKey, TValue>.Enumerator(this);
		}

		// Token: 0x06004209 RID: 16905 RVA: 0x000E282C File Offset: 0x000E0A2C
		IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
		{
			return new Dictionary<TKey, TValue>.Enumerator(this);
		}

		// Token: 0x0600420A RID: 16906 RVA: 0x000E283C File Offset: 0x000E0A3C
		IDictionaryEnumerator IDictionary.GetEnumerator()
		{
			return new Dictionary<TKey, TValue>.ShimEnumerator(this);
		}

		// Token: 0x17000C49 RID: 3145
		// (get) Token: 0x0600420B RID: 16907 RVA: 0x000E2844 File Offset: 0x000E0A44
		public int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x17000C4A RID: 3146
		public TValue this[TKey key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				int num = this.hcp.GetHashCode(key) | int.MinValue;
				for (int num2 = this.table[(num & int.MaxValue) % this.table.Length] - 1; num2 != -1; num2 = this.linkSlots[num2].Next)
				{
					if (this.linkSlots[num2].HashCode == num && this.hcp.Equals(this.keySlots[num2], key))
					{
						return this.valueSlots[num2];
					}
				}
				throw new KeyNotFoundException();
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				int num = this.hcp.GetHashCode(key) | int.MinValue;
				int num2 = (num & int.MaxValue) % this.table.Length;
				int num3 = this.table[num2] - 1;
				int num4 = -1;
				if (num3 != -1)
				{
					while (this.linkSlots[num3].HashCode != num || !this.hcp.Equals(this.keySlots[num3], key))
					{
						num4 = num3;
						num3 = this.linkSlots[num3].Next;
						if (num3 == -1)
						{
							break;
						}
					}
				}
				if (num3 == -1)
				{
					if (++this.count > this.threshold)
					{
						this.Resize();
						num2 = (num & int.MaxValue) % this.table.Length;
					}
					num3 = this.emptySlot;
					if (num3 == -1)
					{
						num3 = this.touchedSlots++;
					}
					else
					{
						this.emptySlot = this.linkSlots[num3].Next;
					}
					this.linkSlots[num3].Next = this.table[num2] - 1;
					this.table[num2] = num3 + 1;
					this.linkSlots[num3].HashCode = num;
					this.keySlots[num3] = key;
				}
				else if (num4 != -1)
				{
					this.linkSlots[num4].Next = this.linkSlots[num3].Next;
					this.linkSlots[num3].Next = this.table[num2] - 1;
					this.table[num2] = num3 + 1;
				}
				this.valueSlots[num3] = value;
				this.generation++;
			}
		}

		// Token: 0x0600420E RID: 16910 RVA: 0x000E2AE0 File Offset: 0x000E0CE0
		private void Init(int capacity, IEqualityComparer<TKey> hcp)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity");
			}
			this.hcp = ((hcp == null) ? EqualityComparer<TKey>.Default : hcp);
			if (capacity == 0)
			{
				capacity = 10;
			}
			capacity = (int)((float)capacity / 0.9f) + 1;
			this.InitArrays(capacity);
			this.generation = 0;
		}

		// Token: 0x0600420F RID: 16911 RVA: 0x000E2B40 File Offset: 0x000E0D40
		private void InitArrays(int size)
		{
			this.table = new int[size];
			this.linkSlots = new Link[size];
			this.emptySlot = -1;
			this.keySlots = new TKey[size];
			this.valueSlots = new TValue[size];
			this.touchedSlots = 0;
			this.threshold = (int)((float)this.table.Length * 0.9f);
			if (this.threshold == 0 && this.table.Length > 0)
			{
				this.threshold = 1;
			}
		}

		// Token: 0x06004210 RID: 16912 RVA: 0x000E2BC4 File Offset: 0x000E0DC4
		private void CopyToCheck(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (index > array.Length)
			{
				throw new ArgumentException("index larger than largest valid index of array");
			}
			if (array.Length - index < this.Count)
			{
				throw new ArgumentException("Destination array cannot hold the requested elements!");
			}
		}

		// Token: 0x06004211 RID: 16913 RVA: 0x000E2C2C File Offset: 0x000E0E2C
		private void Do_CopyTo<TRet, TElem>(TElem[] array, int index, Dictionary<TKey, TValue>.Transform<TRet> transform) where TRet : TElem
		{
			for (int i = 0; i < this.touchedSlots; i++)
			{
				if ((this.linkSlots[i].HashCode & -2147483648) != 0)
				{
					array[index++] = (TElem)((object)transform(this.keySlots[i], this.valueSlots[i]));
				}
			}
		}

		// Token: 0x06004212 RID: 16914 RVA: 0x000E2CA0 File Offset: 0x000E0EA0
		private static KeyValuePair<TKey, TValue> make_pair(TKey key, TValue value)
		{
			return new KeyValuePair<TKey, TValue>(key, value);
		}

		// Token: 0x06004213 RID: 16915 RVA: 0x000E2CAC File Offset: 0x000E0EAC
		private static TKey pick_key(TKey key, TValue value)
		{
			return key;
		}

		// Token: 0x06004214 RID: 16916 RVA: 0x000E2CB0 File Offset: 0x000E0EB0
		private static TValue pick_value(TKey key, TValue value)
		{
			return value;
		}

		// Token: 0x06004215 RID: 16917 RVA: 0x000E2CB4 File Offset: 0x000E0EB4
		private void CopyTo(KeyValuePair<TKey, TValue>[] array, int index)
		{
			this.CopyToCheck(array, index);
			this.Do_CopyTo<KeyValuePair<TKey, TValue>, KeyValuePair<TKey, TValue>>(array, index, new Dictionary<TKey, TValue>.Transform<KeyValuePair<TKey, TValue>>(Dictionary<TKey, TValue>.make_pair));
		}

		// Token: 0x06004216 RID: 16918 RVA: 0x000E2CE0 File Offset: 0x000E0EE0
		private void Do_ICollectionCopyTo<TRet>(Array array, int index, Dictionary<TKey, TValue>.Transform<TRet> transform)
		{
			Type typeFromHandle = typeof(TRet);
			Type elementType = array.GetType().GetElementType();
			try
			{
				if ((typeFromHandle.IsPrimitive || elementType.IsPrimitive) && !elementType.IsAssignableFrom(typeFromHandle))
				{
					throw new Exception();
				}
				this.Do_CopyTo<TRet, object>((object[])array, index, transform);
			}
			catch (Exception innerException)
			{
				throw new ArgumentException("Cannot copy source collection elements to destination array", "array", innerException);
			}
		}

		// Token: 0x06004217 RID: 16919 RVA: 0x000E2D74 File Offset: 0x000E0F74
		private void Resize()
		{
			int num = Hashtable.ToPrime(this.table.Length << 1 | 1);
			int[] array = new int[num];
			Link[] array2 = new Link[num];
			for (int i = 0; i < this.table.Length; i++)
			{
				for (int num2 = this.table[i] - 1; num2 != -1; num2 = this.linkSlots[num2].Next)
				{
					int num3 = array2[num2].HashCode = (this.hcp.GetHashCode(this.keySlots[num2]) | int.MinValue);
					int num4 = (num3 & int.MaxValue) % num;
					array2[num2].Next = array[num4] - 1;
					array[num4] = num2 + 1;
				}
			}
			this.table = array;
			this.linkSlots = array2;
			TKey[] destinationArray = new TKey[num];
			TValue[] destinationArray2 = new TValue[num];
			Array.Copy(this.keySlots, 0, destinationArray, 0, this.touchedSlots);
			Array.Copy(this.valueSlots, 0, destinationArray2, 0, this.touchedSlots);
			this.keySlots = destinationArray;
			this.valueSlots = destinationArray2;
			this.threshold = (int)((float)num * 0.9f);
		}

		// Token: 0x06004218 RID: 16920 RVA: 0x000E2EA8 File Offset: 0x000E10A8
		public void Add(TKey key, TValue value)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			int num = this.hcp.GetHashCode(key) | int.MinValue;
			int num2 = (num & int.MaxValue) % this.table.Length;
			int num3;
			for (num3 = this.table[num2] - 1; num3 != -1; num3 = this.linkSlots[num3].Next)
			{
				if (this.linkSlots[num3].HashCode == num && this.hcp.Equals(this.keySlots[num3], key))
				{
					throw new ArgumentException("An element with the same key already exists in the dictionary.");
				}
			}
			if (++this.count > this.threshold)
			{
				this.Resize();
				num2 = (num & int.MaxValue) % this.table.Length;
			}
			num3 = this.emptySlot;
			if (num3 == -1)
			{
				num3 = this.touchedSlots++;
			}
			else
			{
				this.emptySlot = this.linkSlots[num3].Next;
			}
			this.linkSlots[num3].HashCode = num;
			this.linkSlots[num3].Next = this.table[num2] - 1;
			this.table[num2] = num3 + 1;
			this.keySlots[num3] = key;
			this.valueSlots[num3] = value;
			this.generation++;
		}

		// Token: 0x17000C4B RID: 3147
		// (get) Token: 0x06004219 RID: 16921 RVA: 0x000E3028 File Offset: 0x000E1228
		public IEqualityComparer<TKey> Comparer
		{
			get
			{
				return this.hcp;
			}
		}

		// Token: 0x0600421A RID: 16922 RVA: 0x000E3030 File Offset: 0x000E1230
		public void Clear()
		{
			this.count = 0;
			Array.Clear(this.table, 0, this.table.Length);
			Array.Clear(this.keySlots, 0, this.keySlots.Length);
			Array.Clear(this.valueSlots, 0, this.valueSlots.Length);
			Array.Clear(this.linkSlots, 0, this.linkSlots.Length);
			this.emptySlot = -1;
			this.touchedSlots = 0;
			this.generation++;
		}

		// Token: 0x0600421B RID: 16923 RVA: 0x000E30B0 File Offset: 0x000E12B0
		public bool ContainsKey(TKey key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			int num = this.hcp.GetHashCode(key) | int.MinValue;
			for (int num2 = this.table[(num & int.MaxValue) % this.table.Length] - 1; num2 != -1; num2 = this.linkSlots[num2].Next)
			{
				if (this.linkSlots[num2].HashCode == num && this.hcp.Equals(this.keySlots[num2], key))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600421C RID: 16924 RVA: 0x000E3158 File Offset: 0x000E1358
		public bool ContainsValue(TValue value)
		{
			IEqualityComparer<TValue> @default = EqualityComparer<TValue>.Default;
			for (int i = 0; i < this.table.Length; i++)
			{
				for (int num = this.table[i] - 1; num != -1; num = this.linkSlots[num].Next)
				{
					if (@default.Equals(this.valueSlots[num], value))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x0600421D RID: 16925 RVA: 0x000E31C8 File Offset: 0x000E13C8
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("Version", this.generation);
			info.AddValue("Comparer", this.hcp);
			KeyValuePair<TKey, TValue>[] array = null;
			if (this.count > 0)
			{
				array = new KeyValuePair<TKey, TValue>[this.count];
				this.CopyTo(array, 0);
			}
			info.AddValue("HashSize", this.table.Length);
			info.AddValue("KeyValuePairs", array);
		}

		// Token: 0x0600421E RID: 16926 RVA: 0x000E324C File Offset: 0x000E144C
		public virtual void OnDeserialization(object sender)
		{
			if (this.serialization_info == null)
			{
				return;
			}
			this.generation = this.serialization_info.GetInt32("Version");
			this.hcp = (IEqualityComparer<TKey>)this.serialization_info.GetValue("Comparer", typeof(IEqualityComparer<TKey>));
			int num = this.serialization_info.GetInt32("HashSize");
			KeyValuePair<TKey, TValue>[] array = (KeyValuePair<TKey, TValue>[])this.serialization_info.GetValue("KeyValuePairs", typeof(KeyValuePair<TKey, TValue>[]));
			if (num < 10)
			{
				num = 10;
			}
			this.InitArrays(num);
			this.count = 0;
			if (array != null)
			{
				for (int i = 0; i < array.Length; i++)
				{
					this.Add(array[i].Key, array[i].Value);
				}
			}
			this.generation++;
			this.serialization_info = null;
		}

		// Token: 0x0600421F RID: 16927 RVA: 0x000E3338 File Offset: 0x000E1538
		public bool Remove(TKey key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			int num = this.hcp.GetHashCode(key) | int.MinValue;
			int num2 = (num & int.MaxValue) % this.table.Length;
			int num3 = this.table[num2] - 1;
			if (num3 == -1)
			{
				return false;
			}
			int num4 = -1;
			while (this.linkSlots[num3].HashCode != num || !this.hcp.Equals(this.keySlots[num3], key))
			{
				num4 = num3;
				num3 = this.linkSlots[num3].Next;
				if (num3 == -1)
				{
					IL_A4:
					if (num3 == -1)
					{
						return false;
					}
					this.count--;
					if (num4 == -1)
					{
						this.table[num2] = this.linkSlots[num3].Next + 1;
					}
					else
					{
						this.linkSlots[num4].Next = this.linkSlots[num3].Next;
					}
					this.linkSlots[num3].Next = this.emptySlot;
					this.emptySlot = num3;
					this.linkSlots[num3].HashCode = 0;
					this.keySlots[num3] = default(TKey);
					this.valueSlots[num3] = default(TValue);
					this.generation++;
					return true;
				}
			}
			goto IL_A4;
		}

		// Token: 0x06004220 RID: 16928 RVA: 0x000E34B4 File Offset: 0x000E16B4
		public bool TryGetValue(TKey key, out TValue value)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			int num = this.hcp.GetHashCode(key) | int.MinValue;
			for (int num2 = this.table[(num & int.MaxValue) % this.table.Length] - 1; num2 != -1; num2 = this.linkSlots[num2].Next)
			{
				if (this.linkSlots[num2].HashCode == num && this.hcp.Equals(this.keySlots[num2], key))
				{
					value = this.valueSlots[num2];
					return true;
				}
			}
			value = default(TValue);
			return false;
		}

		// Token: 0x17000C4C RID: 3148
		// (get) Token: 0x06004221 RID: 16929 RVA: 0x000E357C File Offset: 0x000E177C
		public Dictionary<TKey, TValue>.KeyCollection Keys
		{
			get
			{
				return new Dictionary<TKey, TValue>.KeyCollection(this);
			}
		}

		// Token: 0x17000C4D RID: 3149
		// (get) Token: 0x06004222 RID: 16930 RVA: 0x000E3584 File Offset: 0x000E1784
		public Dictionary<TKey, TValue>.ValueCollection Values
		{
			get
			{
				return new Dictionary<TKey, TValue>.ValueCollection(this);
			}
		}

		// Token: 0x06004223 RID: 16931 RVA: 0x000E358C File Offset: 0x000E178C
		private TKey ToTKey(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			if (!(key is TKey))
			{
				throw new ArgumentException("not of type: " + typeof(TKey).ToString(), "key");
			}
			return (TKey)((object)key);
		}

		// Token: 0x06004224 RID: 16932 RVA: 0x000E35E0 File Offset: 0x000E17E0
		private TValue ToTValue(object value)
		{
			if (value == null && !typeof(TValue).IsValueType)
			{
				return default(TValue);
			}
			if (!(value is TValue))
			{
				throw new ArgumentException("not of type: " + typeof(TValue).ToString(), "value");
			}
			return (TValue)((object)value);
		}

		// Token: 0x06004225 RID: 16933 RVA: 0x000E3648 File Offset: 0x000E1848
		private bool ContainsKeyValuePair(KeyValuePair<TKey, TValue> pair)
		{
			TValue y;
			return this.TryGetValue(pair.Key, out y) && EqualityComparer<TValue>.Default.Equals(pair.Value, y);
		}

		// Token: 0x06004226 RID: 16934 RVA: 0x000E3680 File Offset: 0x000E1880
		public Dictionary<TKey, TValue>.Enumerator GetEnumerator()
		{
			return new Dictionary<TKey, TValue>.Enumerator(this);
		}

		// Token: 0x04001C2E RID: 7214
		private const int INITIAL_SIZE = 10;

		// Token: 0x04001C2F RID: 7215
		private const float DEFAULT_LOAD_FACTOR = 0.9f;

		// Token: 0x04001C30 RID: 7216
		private const int NO_SLOT = -1;

		// Token: 0x04001C31 RID: 7217
		private const int HASH_FLAG = -2147483648;

		// Token: 0x04001C32 RID: 7218
		private int[] table;

		// Token: 0x04001C33 RID: 7219
		private Link[] linkSlots;

		// Token: 0x04001C34 RID: 7220
		private TKey[] keySlots;

		// Token: 0x04001C35 RID: 7221
		private TValue[] valueSlots;

		// Token: 0x04001C36 RID: 7222
		private int touchedSlots;

		// Token: 0x04001C37 RID: 7223
		private int emptySlot;

		// Token: 0x04001C38 RID: 7224
		private int count;

		// Token: 0x04001C39 RID: 7225
		private int threshold;

		// Token: 0x04001C3A RID: 7226
		private IEqualityComparer<TKey> hcp;

		// Token: 0x04001C3B RID: 7227
		private SerializationInfo serialization_info;

		// Token: 0x04001C3C RID: 7228
		private int generation;

		// Token: 0x020006BE RID: 1726
		[Serializable]
		private class ShimEnumerator : IEnumerator, IDictionaryEnumerator
		{
			// Token: 0x06004228 RID: 16936 RVA: 0x000E369C File Offset: 0x000E189C
			public ShimEnumerator(Dictionary<TKey, TValue> host)
			{
				this.host_enumerator = host.GetEnumerator();
			}

			// Token: 0x06004229 RID: 16937 RVA: 0x000E36B0 File Offset: 0x000E18B0
			public void Dispose()
			{
				this.host_enumerator.Dispose();
			}

			// Token: 0x0600422A RID: 16938 RVA: 0x000E36C0 File Offset: 0x000E18C0
			public bool MoveNext()
			{
				return this.host_enumerator.MoveNext();
			}

			// Token: 0x17000C4E RID: 3150
			// (get) Token: 0x0600422B RID: 16939 RVA: 0x000E36D0 File Offset: 0x000E18D0
			public DictionaryEntry Entry
			{
				get
				{
					return ((IDictionaryEnumerator)this.host_enumerator).Entry;
				}
			}

			// Token: 0x17000C4F RID: 3151
			// (get) Token: 0x0600422C RID: 16940 RVA: 0x000E36E4 File Offset: 0x000E18E4
			public object Key
			{
				get
				{
					KeyValuePair<TKey, TValue> keyValuePair = this.host_enumerator.Current;
					return keyValuePair.Key;
				}
			}

			// Token: 0x17000C50 RID: 3152
			// (get) Token: 0x0600422D RID: 16941 RVA: 0x000E370C File Offset: 0x000E190C
			public object Value
			{
				get
				{
					KeyValuePair<TKey, TValue> keyValuePair = this.host_enumerator.Current;
					return keyValuePair.Value;
				}
			}

			// Token: 0x17000C51 RID: 3153
			// (get) Token: 0x0600422E RID: 16942 RVA: 0x000E3734 File Offset: 0x000E1934
			public object Current
			{
				get
				{
					return this.Entry;
				}
			}

			// Token: 0x0600422F RID: 16943 RVA: 0x000E3744 File Offset: 0x000E1944
			public void Reset()
			{
				this.host_enumerator.Reset();
			}

			// Token: 0x04001C3E RID: 7230
			private Dictionary<TKey, TValue>.Enumerator host_enumerator;
		}

		// Token: 0x020006BF RID: 1727
		[Serializable]
		public struct Enumerator : IEnumerator, IDisposable, IEnumerator<KeyValuePair<TKey, TValue>>, IDictionaryEnumerator
		{
			// Token: 0x06004230 RID: 16944 RVA: 0x000E3754 File Offset: 0x000E1954
			internal Enumerator(Dictionary<TKey, TValue> dictionary)
			{
				this.dictionary = dictionary;
				this.stamp = dictionary.generation;
			}

			// Token: 0x17000C52 RID: 3154
			// (get) Token: 0x06004231 RID: 16945 RVA: 0x000E376C File Offset: 0x000E196C
			object IEnumerator.Current
			{
				get
				{
					this.VerifyCurrent();
					return this.current;
				}
			}

			// Token: 0x06004232 RID: 16946 RVA: 0x000E3780 File Offset: 0x000E1980
			void IEnumerator.Reset()
			{
				this.Reset();
			}

			// Token: 0x17000C53 RID: 3155
			// (get) Token: 0x06004233 RID: 16947 RVA: 0x000E3788 File Offset: 0x000E1988
			DictionaryEntry IDictionaryEnumerator.Entry
			{
				get
				{
					this.VerifyCurrent();
					return new DictionaryEntry(this.current.Key, this.current.Value);
				}
			}

			// Token: 0x17000C54 RID: 3156
			// (get) Token: 0x06004234 RID: 16948 RVA: 0x000E37C0 File Offset: 0x000E19C0
			object IDictionaryEnumerator.Key
			{
				get
				{
					return this.CurrentKey;
				}
			}

			// Token: 0x17000C55 RID: 3157
			// (get) Token: 0x06004235 RID: 16949 RVA: 0x000E37D0 File Offset: 0x000E19D0
			object IDictionaryEnumerator.Value
			{
				get
				{
					return this.CurrentValue;
				}
			}

			// Token: 0x06004236 RID: 16950 RVA: 0x000E37E0 File Offset: 0x000E19E0
			public bool MoveNext()
			{
				this.VerifyState();
				if (this.next < 0)
				{
					return false;
				}
				while (this.next < this.dictionary.touchedSlots)
				{
					int num = this.next++;
					if ((this.dictionary.linkSlots[num].HashCode & -2147483648) != 0)
					{
						this.current = new KeyValuePair<TKey, TValue>(this.dictionary.keySlots[num], this.dictionary.valueSlots[num]);
						return true;
					}
				}
				this.next = -1;
				return false;
			}

			// Token: 0x17000C56 RID: 3158
			// (get) Token: 0x06004237 RID: 16951 RVA: 0x000E3888 File Offset: 0x000E1A88
			public KeyValuePair<TKey, TValue> Current
			{
				get
				{
					return this.current;
				}
			}

			// Token: 0x17000C57 RID: 3159
			// (get) Token: 0x06004238 RID: 16952 RVA: 0x000E3890 File Offset: 0x000E1A90
			internal TKey CurrentKey
			{
				get
				{
					this.VerifyCurrent();
					return this.current.Key;
				}
			}

			// Token: 0x17000C58 RID: 3160
			// (get) Token: 0x06004239 RID: 16953 RVA: 0x000E38A4 File Offset: 0x000E1AA4
			internal TValue CurrentValue
			{
				get
				{
					this.VerifyCurrent();
					return this.current.Value;
				}
			}

			// Token: 0x0600423A RID: 16954 RVA: 0x000E38B8 File Offset: 0x000E1AB8
			internal void Reset()
			{
				this.VerifyState();
				this.next = 0;
			}

			// Token: 0x0600423B RID: 16955 RVA: 0x000E38C8 File Offset: 0x000E1AC8
			private void VerifyState()
			{
				if (this.dictionary == null)
				{
					throw new ObjectDisposedException(null);
				}
				if (this.dictionary.generation != this.stamp)
				{
					throw new InvalidOperationException("out of sync");
				}
			}

			// Token: 0x0600423C RID: 16956 RVA: 0x000E3900 File Offset: 0x000E1B00
			private void VerifyCurrent()
			{
				this.VerifyState();
				if (this.next <= 0)
				{
					throw new InvalidOperationException("Current is not valid");
				}
			}

			// Token: 0x0600423D RID: 16957 RVA: 0x000E3920 File Offset: 0x000E1B20
			public void Dispose()
			{
				this.dictionary = null;
			}

			// Token: 0x04001C3F RID: 7231
			private Dictionary<TKey, TValue> dictionary;

			// Token: 0x04001C40 RID: 7232
			private int next;

			// Token: 0x04001C41 RID: 7233
			private int stamp;

			// Token: 0x04001C42 RID: 7234
			internal KeyValuePair<TKey, TValue> current;
		}

		// Token: 0x020006C0 RID: 1728
		[DebuggerTypeProxy(typeof(CollectionDebuggerView<, >))]
		[DebuggerDisplay("Count={Count}")]
		[Serializable]
		public sealed class KeyCollection : IEnumerable, ICollection, ICollection<TKey>, IEnumerable<TKey>
		{
			// Token: 0x0600423E RID: 16958 RVA: 0x000E392C File Offset: 0x000E1B2C
			public KeyCollection(Dictionary<TKey, TValue> dictionary)
			{
				if (dictionary == null)
				{
					throw new ArgumentNullException("dictionary");
				}
				this.dictionary = dictionary;
			}

			// Token: 0x0600423F RID: 16959 RVA: 0x000E394C File Offset: 0x000E1B4C
			void ICollection<TKey>.Add(TKey item)
			{
				throw new NotSupportedException("this is a read-only collection");
			}

			// Token: 0x06004240 RID: 16960 RVA: 0x000E3958 File Offset: 0x000E1B58
			void ICollection<TKey>.Clear()
			{
				throw new NotSupportedException("this is a read-only collection");
			}

			// Token: 0x06004241 RID: 16961 RVA: 0x000E3964 File Offset: 0x000E1B64
			bool ICollection<TKey>.Contains(TKey item)
			{
				return this.dictionary.ContainsKey(item);
			}

			// Token: 0x06004242 RID: 16962 RVA: 0x000E3974 File Offset: 0x000E1B74
			bool ICollection<TKey>.Remove(TKey item)
			{
				throw new NotSupportedException("this is a read-only collection");
			}

			// Token: 0x06004243 RID: 16963 RVA: 0x000E3980 File Offset: 0x000E1B80
			IEnumerator<TKey> IEnumerable<TKey>.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x06004244 RID: 16964 RVA: 0x000E3990 File Offset: 0x000E1B90
			void ICollection.CopyTo(Array array, int index)
			{
				TKey[] array2 = array as TKey[];
				if (array2 != null)
				{
					this.CopyTo(array2, index);
					return;
				}
				this.dictionary.CopyToCheck(array, index);
				this.dictionary.Do_ICollectionCopyTo<TKey>(array, index, new Dictionary<TKey, TValue>.Transform<TKey>(Dictionary<TKey, TValue>.pick_key));
			}

			// Token: 0x06004245 RID: 16965 RVA: 0x000E39DC File Offset: 0x000E1BDC
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x17000C59 RID: 3161
			// (get) Token: 0x06004246 RID: 16966 RVA: 0x000E39EC File Offset: 0x000E1BEC
			bool ICollection<TKey>.IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000C5A RID: 3162
			// (get) Token: 0x06004247 RID: 16967 RVA: 0x000E39F0 File Offset: 0x000E1BF0
			bool ICollection.IsSynchronized
			{
				get
				{
					return false;
				}
			}

			// Token: 0x17000C5B RID: 3163
			// (get) Token: 0x06004248 RID: 16968 RVA: 0x000E39F4 File Offset: 0x000E1BF4
			object ICollection.SyncRoot
			{
				get
				{
					return ((ICollection)this.dictionary).SyncRoot;
				}
			}

			// Token: 0x06004249 RID: 16969 RVA: 0x000E3A04 File Offset: 0x000E1C04
			public void CopyTo(TKey[] array, int index)
			{
				this.dictionary.CopyToCheck(array, index);
				this.dictionary.Do_CopyTo<TKey, TKey>(array, index, new Dictionary<TKey, TValue>.Transform<TKey>(Dictionary<TKey, TValue>.pick_key));
			}

			// Token: 0x0600424A RID: 16970 RVA: 0x000E3A38 File Offset: 0x000E1C38
			public Dictionary<TKey, TValue>.KeyCollection.Enumerator GetEnumerator()
			{
				return new Dictionary<TKey, TValue>.KeyCollection.Enumerator(this.dictionary);
			}

			// Token: 0x17000C5C RID: 3164
			// (get) Token: 0x0600424B RID: 16971 RVA: 0x000E3A48 File Offset: 0x000E1C48
			public int Count
			{
				get
				{
					return this.dictionary.Count;
				}
			}

			// Token: 0x04001C43 RID: 7235
			private Dictionary<TKey, TValue> dictionary;

			// Token: 0x020006C1 RID: 1729
			[Serializable]
			public struct Enumerator : IEnumerator, IDisposable, IEnumerator<TKey>
			{
				// Token: 0x0600424C RID: 16972 RVA: 0x000E3A58 File Offset: 0x000E1C58
				internal Enumerator(Dictionary<TKey, TValue> host)
				{
					this.host_enumerator = host.GetEnumerator();
				}

				// Token: 0x17000C5D RID: 3165
				// (get) Token: 0x0600424D RID: 16973 RVA: 0x000E3A68 File Offset: 0x000E1C68
				object IEnumerator.Current
				{
					get
					{
						return this.host_enumerator.CurrentKey;
					}
				}

				// Token: 0x0600424E RID: 16974 RVA: 0x000E3A7C File Offset: 0x000E1C7C
				void IEnumerator.Reset()
				{
					this.host_enumerator.Reset();
				}

				// Token: 0x0600424F RID: 16975 RVA: 0x000E3A8C File Offset: 0x000E1C8C
				public void Dispose()
				{
					this.host_enumerator.Dispose();
				}

				// Token: 0x06004250 RID: 16976 RVA: 0x000E3A9C File Offset: 0x000E1C9C
				public bool MoveNext()
				{
					return this.host_enumerator.MoveNext();
				}

				// Token: 0x17000C5E RID: 3166
				// (get) Token: 0x06004251 RID: 16977 RVA: 0x000E3AAC File Offset: 0x000E1CAC
				public TKey Current
				{
					get
					{
						return this.host_enumerator.current.Key;
					}
				}

				// Token: 0x04001C44 RID: 7236
				private Dictionary<TKey, TValue>.Enumerator host_enumerator;
			}
		}

		// Token: 0x020006C2 RID: 1730
		[DebuggerDisplay("Count={Count}")]
		[DebuggerTypeProxy(typeof(CollectionDebuggerView<, >))]
		[Serializable]
		public sealed class ValueCollection : IEnumerable, ICollection, ICollection<TValue>, IEnumerable<TValue>
		{
			// Token: 0x06004252 RID: 16978 RVA: 0x000E3AC0 File Offset: 0x000E1CC0
			public ValueCollection(Dictionary<TKey, TValue> dictionary)
			{
				if (dictionary == null)
				{
					throw new ArgumentNullException("dictionary");
				}
				this.dictionary = dictionary;
			}

			// Token: 0x06004253 RID: 16979 RVA: 0x000E3AE0 File Offset: 0x000E1CE0
			void ICollection<TValue>.Add(TValue item)
			{
				throw new NotSupportedException("this is a read-only collection");
			}

			// Token: 0x06004254 RID: 16980 RVA: 0x000E3AEC File Offset: 0x000E1CEC
			void ICollection<TValue>.Clear()
			{
				throw new NotSupportedException("this is a read-only collection");
			}

			// Token: 0x06004255 RID: 16981 RVA: 0x000E3AF8 File Offset: 0x000E1CF8
			bool ICollection<TValue>.Contains(TValue item)
			{
				return this.dictionary.ContainsValue(item);
			}

			// Token: 0x06004256 RID: 16982 RVA: 0x000E3B08 File Offset: 0x000E1D08
			bool ICollection<TValue>.Remove(TValue item)
			{
				throw new NotSupportedException("this is a read-only collection");
			}

			// Token: 0x06004257 RID: 16983 RVA: 0x000E3B14 File Offset: 0x000E1D14
			IEnumerator<TValue> IEnumerable<TValue>.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x06004258 RID: 16984 RVA: 0x000E3B24 File Offset: 0x000E1D24
			void ICollection.CopyTo(Array array, int index)
			{
				TValue[] array2 = array as TValue[];
				if (array2 != null)
				{
					this.CopyTo(array2, index);
					return;
				}
				this.dictionary.CopyToCheck(array, index);
				this.dictionary.Do_ICollectionCopyTo<TValue>(array, index, new Dictionary<TKey, TValue>.Transform<TValue>(Dictionary<TKey, TValue>.pick_value));
			}

			// Token: 0x06004259 RID: 16985 RVA: 0x000E3B70 File Offset: 0x000E1D70
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x17000C5F RID: 3167
			// (get) Token: 0x0600425A RID: 16986 RVA: 0x000E3B80 File Offset: 0x000E1D80
			bool ICollection<TValue>.IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000C60 RID: 3168
			// (get) Token: 0x0600425B RID: 16987 RVA: 0x000E3B84 File Offset: 0x000E1D84
			bool ICollection.IsSynchronized
			{
				get
				{
					return false;
				}
			}

			// Token: 0x17000C61 RID: 3169
			// (get) Token: 0x0600425C RID: 16988 RVA: 0x000E3B88 File Offset: 0x000E1D88
			object ICollection.SyncRoot
			{
				get
				{
					return ((ICollection)this.dictionary).SyncRoot;
				}
			}

			// Token: 0x0600425D RID: 16989 RVA: 0x000E3B98 File Offset: 0x000E1D98
			public void CopyTo(TValue[] array, int index)
			{
				this.dictionary.CopyToCheck(array, index);
				this.dictionary.Do_CopyTo<TValue, TValue>(array, index, new Dictionary<TKey, TValue>.Transform<TValue>(Dictionary<TKey, TValue>.pick_value));
			}

			// Token: 0x0600425E RID: 16990 RVA: 0x000E3BCC File Offset: 0x000E1DCC
			public Dictionary<TKey, TValue>.ValueCollection.Enumerator GetEnumerator()
			{
				return new Dictionary<TKey, TValue>.ValueCollection.Enumerator(this.dictionary);
			}

			// Token: 0x17000C62 RID: 3170
			// (get) Token: 0x0600425F RID: 16991 RVA: 0x000E3BDC File Offset: 0x000E1DDC
			public int Count
			{
				get
				{
					return this.dictionary.Count;
				}
			}

			// Token: 0x04001C45 RID: 7237
			private Dictionary<TKey, TValue> dictionary;

			// Token: 0x020006C3 RID: 1731
			[Serializable]
			public struct Enumerator : IEnumerator, IDisposable, IEnumerator<TValue>
			{
				// Token: 0x06004260 RID: 16992 RVA: 0x000E3BEC File Offset: 0x000E1DEC
				internal Enumerator(Dictionary<TKey, TValue> host)
				{
					this.host_enumerator = host.GetEnumerator();
				}

				// Token: 0x17000C63 RID: 3171
				// (get) Token: 0x06004261 RID: 16993 RVA: 0x000E3BFC File Offset: 0x000E1DFC
				object IEnumerator.Current
				{
					get
					{
						return this.host_enumerator.CurrentValue;
					}
				}

				// Token: 0x06004262 RID: 16994 RVA: 0x000E3C10 File Offset: 0x000E1E10
				void IEnumerator.Reset()
				{
					this.host_enumerator.Reset();
				}

				// Token: 0x06004263 RID: 16995 RVA: 0x000E3C20 File Offset: 0x000E1E20
				public void Dispose()
				{
					this.host_enumerator.Dispose();
				}

				// Token: 0x06004264 RID: 16996 RVA: 0x000E3C30 File Offset: 0x000E1E30
				public bool MoveNext()
				{
					return this.host_enumerator.MoveNext();
				}

				// Token: 0x17000C64 RID: 3172
				// (get) Token: 0x06004265 RID: 16997 RVA: 0x000E3C40 File Offset: 0x000E1E40
				public TValue Current
				{
					get
					{
						return this.host_enumerator.current.Value;
					}
				}

				// Token: 0x04001C46 RID: 7238
				private Dictionary<TKey, TValue>.Enumerator host_enumerator;
			}
		}

		// Token: 0x020006E5 RID: 1765
		// (Invoke) Token: 0x06004388 RID: 17288
		private delegate TRet Transform<TRet>(TKey key, TValue value);
	}
}
