﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020006C5 RID: 1733
	public interface IEqualityComparer<T>
	{
		// Token: 0x06004267 RID: 16999
		bool Equals(T x, T y);

		// Token: 0x06004268 RID: 17000
		int GetHashCode(T obj);
	}
}
