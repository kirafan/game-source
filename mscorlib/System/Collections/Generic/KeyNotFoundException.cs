﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Collections.Generic
{
	// Token: 0x020006CB RID: 1739
	[ComVisible(true)]
	[Serializable]
	public class KeyNotFoundException : SystemException, ISerializable
	{
		// Token: 0x06004284 RID: 17028 RVA: 0x000E3E68 File Offset: 0x000E2068
		public KeyNotFoundException() : base("The given key was not present in the dictionary.")
		{
		}

		// Token: 0x06004285 RID: 17029 RVA: 0x000E3E78 File Offset: 0x000E2078
		public KeyNotFoundException(string message) : base(message)
		{
		}

		// Token: 0x06004286 RID: 17030 RVA: 0x000E3E84 File Offset: 0x000E2084
		public KeyNotFoundException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06004287 RID: 17031 RVA: 0x000E3E90 File Offset: 0x000E2090
		protected KeyNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
