﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020006C6 RID: 1734
	public interface IDictionary<TKey, TValue> : IEnumerable, ICollection<KeyValuePair<TKey, TValue>>, IEnumerable<KeyValuePair<TKey, TValue>>
	{
		// Token: 0x06004269 RID: 17001
		void Add(TKey key, TValue value);

		// Token: 0x0600426A RID: 17002
		bool ContainsKey(TKey key);

		// Token: 0x0600426B RID: 17003
		bool Remove(TKey key);

		// Token: 0x0600426C RID: 17004
		bool TryGetValue(TKey key, out TValue value);

		// Token: 0x17000C65 RID: 3173
		TValue this[TKey key]
		{
			get;
			set;
		}

		// Token: 0x17000C66 RID: 3174
		// (get) Token: 0x0600426F RID: 17007
		ICollection<TKey> Keys { get; }

		// Token: 0x17000C67 RID: 3175
		// (get) Token: 0x06004270 RID: 17008
		ICollection<TValue> Values { get; }
	}
}
