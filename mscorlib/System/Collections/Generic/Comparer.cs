﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020006D4 RID: 1748
	[Serializable]
	public abstract class Comparer<T> : IComparer<T>, IComparer
	{
		// Token: 0x06004347 RID: 17223 RVA: 0x000E5C78 File Offset: 0x000E3E78
		static Comparer()
		{
			if (typeof(IComparable<T>).IsAssignableFrom(typeof(T)))
			{
				Comparer<T>._default = (Comparer<T>)Activator.CreateInstance(typeof(GenericComparer<>).MakeGenericType(new Type[]
				{
					typeof(T)
				}));
			}
			else
			{
				Comparer<T>._default = new Comparer<T>.DefaultComparer();
			}
		}

		// Token: 0x06004348 RID: 17224 RVA: 0x000E5CE4 File Offset: 0x000E3EE4
		int IComparer.Compare(object x, object y)
		{
			if (x == null)
			{
				return (y != null) ? -1 : 0;
			}
			if (y == null)
			{
				return 1;
			}
			if (x is T && y is T)
			{
				return this.Compare((T)((object)x), (T)((object)y));
			}
			throw new ArgumentException();
		}

		// Token: 0x06004349 RID: 17225
		public abstract int Compare(T x, T y);

		// Token: 0x17000C91 RID: 3217
		// (get) Token: 0x0600434A RID: 17226 RVA: 0x000E5D3C File Offset: 0x000E3F3C
		public static Comparer<T> Default
		{
			get
			{
				return Comparer<T>._default;
			}
		}

		// Token: 0x04001C5E RID: 7262
		private static readonly Comparer<T> _default;

		// Token: 0x020006D5 RID: 1749
		private sealed class DefaultComparer : Comparer<T>
		{
			// Token: 0x0600434C RID: 17228 RVA: 0x000E5D4C File Offset: 0x000E3F4C
			public override int Compare(T x, T y)
			{
				if (x == null)
				{
					return (y != null) ? -1 : 0;
				}
				if (y == null)
				{
					return 1;
				}
				if (x is IComparable<T>)
				{
					return ((IComparable<T>)((object)x)).CompareTo(y);
				}
				if (x is IComparable)
				{
					return ((IComparable)((object)x)).CompareTo(y);
				}
				throw new ArgumentException("does not implement right interface");
			}
		}
	}
}
