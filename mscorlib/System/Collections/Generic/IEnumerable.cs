﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x0200001E RID: 30
	public interface IEnumerable<T> : IEnumerable
	{
		// Token: 0x0600027C RID: 636
		IEnumerator<T> GetEnumerator();
	}
}
