﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x0200002F RID: 47
	public interface IList<T> : IEnumerable, ICollection<T>, IEnumerable<T>
	{
		// Token: 0x060004AD RID: 1197
		int IndexOf(T item);

		// Token: 0x060004AE RID: 1198
		void Insert(int index, T item);

		// Token: 0x060004AF RID: 1199
		void RemoveAt(int index);

		// Token: 0x17000025 RID: 37
		T this[int index]
		{
			get;
			set;
		}
	}
}
