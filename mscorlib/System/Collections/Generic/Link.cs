﻿using System;

namespace System.Collections.Generic
{
	// Token: 0x020006BC RID: 1724
	internal struct Link
	{
		// Token: 0x04001C2C RID: 7212
		public int HashCode;

		// Token: 0x04001C2D RID: 7213
		public int Next;
	}
}
