﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001CA RID: 458
	[ComVisible(true)]
	[Serializable]
	public abstract class ReadOnlyCollectionBase : IEnumerable, ICollection
	{
		// Token: 0x060017A0 RID: 6048 RVA: 0x0005B2A0 File Offset: 0x000594A0
		protected ReadOnlyCollectionBase()
		{
			this.list = new ArrayList();
		}

		// Token: 0x060017A1 RID: 6049 RVA: 0x0005B2B4 File Offset: 0x000594B4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x060017A2 RID: 6050 RVA: 0x0005B2BC File Offset: 0x000594BC
		void ICollection.CopyTo(Array array, int index)
		{
			ArrayList innerList = this.InnerList;
			lock (innerList)
			{
				this.InnerList.CopyTo(array, index);
			}
		}

		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x060017A3 RID: 6051 RVA: 0x0005B30C File Offset: 0x0005950C
		object ICollection.SyncRoot
		{
			get
			{
				return this.InnerList.SyncRoot;
			}
		}

		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x060017A4 RID: 6052 RVA: 0x0005B31C File Offset: 0x0005951C
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.InnerList.IsSynchronized;
			}
		}

		// Token: 0x170003AA RID: 938
		// (get) Token: 0x060017A5 RID: 6053 RVA: 0x0005B32C File Offset: 0x0005952C
		public virtual int Count
		{
			get
			{
				return this.InnerList.Count;
			}
		}

		// Token: 0x060017A6 RID: 6054 RVA: 0x0005B33C File Offset: 0x0005953C
		public virtual IEnumerator GetEnumerator()
		{
			return this.InnerList.GetEnumerator();
		}

		// Token: 0x170003AB RID: 939
		// (get) Token: 0x060017A7 RID: 6055 RVA: 0x0005B34C File Offset: 0x0005954C
		protected ArrayList InnerList
		{
			get
			{
				return this.list;
			}
		}

		// Token: 0x040008A7 RID: 2215
		private ArrayList list;
	}
}
