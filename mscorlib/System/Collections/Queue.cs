﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001C7 RID: 455
	[DebuggerDisplay("Count={Count}")]
	[ComVisible(true)]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView))]
	[Serializable]
	public class Queue : IEnumerable, ICloneable, ICollection
	{
		// Token: 0x0600177A RID: 6010 RVA: 0x0005A858 File Offset: 0x00058A58
		public Queue() : this(32, 2f)
		{
		}

		// Token: 0x0600177B RID: 6011 RVA: 0x0005A868 File Offset: 0x00058A68
		public Queue(int capacity) : this(capacity, 2f)
		{
		}

		// Token: 0x0600177C RID: 6012 RVA: 0x0005A878 File Offset: 0x00058A78
		public Queue(ICollection col) : this((col != null) ? col.Count : 32)
		{
			if (col == null)
			{
				throw new ArgumentNullException("col");
			}
			foreach (object obj in col)
			{
				this.Enqueue(obj);
			}
		}

		// Token: 0x0600177D RID: 6013 RVA: 0x0005A908 File Offset: 0x00058B08
		public Queue(int capacity, float growFactor)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity", "Needs a non-negative number");
			}
			if (growFactor < 1f || growFactor > 10f)
			{
				throw new ArgumentOutOfRangeException("growFactor", "Queue growth factor must be between 1.0 and 10.0, inclusive");
			}
			this._array = new object[capacity];
			this._growFactor = (int)(growFactor * 100f);
		}

		// Token: 0x170003A1 RID: 929
		// (get) Token: 0x0600177E RID: 6014 RVA: 0x0005A974 File Offset: 0x00058B74
		public virtual int Count
		{
			get
			{
				return this._size;
			}
		}

		// Token: 0x170003A2 RID: 930
		// (get) Token: 0x0600177F RID: 6015 RVA: 0x0005A97C File Offset: 0x00058B7C
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170003A3 RID: 931
		// (get) Token: 0x06001780 RID: 6016 RVA: 0x0005A980 File Offset: 0x00058B80
		public virtual object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06001781 RID: 6017 RVA: 0x0005A984 File Offset: 0x00058B84
		public virtual void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (array.Rank > 1 || (index != 0 && index >= array.Length) || this._size > array.Length - index)
			{
				throw new ArgumentException();
			}
			int num = this._array.Length;
			int num2 = num - this._head;
			Array.Copy(this._array, this._head, array, index, Math.Min(this._size, num2));
			if (this._size > num2)
			{
				Array.Copy(this._array, 0, array, index + num2, this._size - num2);
			}
		}

		// Token: 0x06001782 RID: 6018 RVA: 0x0005AA40 File Offset: 0x00058C40
		public virtual IEnumerator GetEnumerator()
		{
			return new Queue.QueueEnumerator(this);
		}

		// Token: 0x06001783 RID: 6019 RVA: 0x0005AA48 File Offset: 0x00058C48
		public virtual object Clone()
		{
			Queue queue = new Queue(this._array.Length);
			queue._growFactor = this._growFactor;
			Array.Copy(this._array, 0, queue._array, 0, this._array.Length);
			queue._head = this._head;
			queue._size = this._size;
			queue._tail = this._tail;
			return queue;
		}

		// Token: 0x06001784 RID: 6020 RVA: 0x0005AAB0 File Offset: 0x00058CB0
		public virtual void Clear()
		{
			this._version++;
			this._head = 0;
			this._size = 0;
			this._tail = 0;
			for (int i = this._array.Length - 1; i >= 0; i--)
			{
				this._array[i] = null;
			}
		}

		// Token: 0x06001785 RID: 6021 RVA: 0x0005AB04 File Offset: 0x00058D04
		public virtual bool Contains(object obj)
		{
			int num = this._head + this._size;
			if (obj == null)
			{
				for (int i = this._head; i < num; i++)
				{
					if (this._array[i % this._array.Length] == null)
					{
						return true;
					}
				}
			}
			else
			{
				for (int j = this._head; j < num; j++)
				{
					if (obj.Equals(this._array[j % this._array.Length]))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x06001786 RID: 6022 RVA: 0x0005AB90 File Offset: 0x00058D90
		public virtual object Dequeue()
		{
			this._version++;
			if (this._size < 1)
			{
				throw new InvalidOperationException();
			}
			object result = this._array[this._head];
			this._array[this._head] = null;
			this._head = (this._head + 1) % this._array.Length;
			this._size--;
			return result;
		}

		// Token: 0x06001787 RID: 6023 RVA: 0x0005AC00 File Offset: 0x00058E00
		public virtual void Enqueue(object obj)
		{
			this._version++;
			if (this._size == this._array.Length)
			{
				this.grow();
			}
			this._array[this._tail] = obj;
			this._tail = (this._tail + 1) % this._array.Length;
			this._size++;
		}

		// Token: 0x06001788 RID: 6024 RVA: 0x0005AC68 File Offset: 0x00058E68
		public virtual object Peek()
		{
			if (this._size < 1)
			{
				throw new InvalidOperationException();
			}
			return this._array[this._head];
		}

		// Token: 0x06001789 RID: 6025 RVA: 0x0005AC8C File Offset: 0x00058E8C
		public static Queue Synchronized(Queue queue)
		{
			if (queue == null)
			{
				throw new ArgumentNullException("queue");
			}
			return new Queue.SyncQueue(queue);
		}

		// Token: 0x0600178A RID: 6026 RVA: 0x0005ACA8 File Offset: 0x00058EA8
		public virtual object[] ToArray()
		{
			object[] array = new object[this._size];
			this.CopyTo(array, 0);
			return array;
		}

		// Token: 0x0600178B RID: 6027 RVA: 0x0005ACCC File Offset: 0x00058ECC
		public virtual void TrimToSize()
		{
			this._version++;
			object[] array = new object[this._size];
			this.CopyTo(array, 0);
			this._array = array;
			this._head = 0;
			this._tail = 0;
		}

		// Token: 0x0600178C RID: 6028 RVA: 0x0005AD10 File Offset: 0x00058F10
		private void grow()
		{
			int num = this._array.Length * this._growFactor / 100;
			if (num < this._array.Length + 1)
			{
				num = this._array.Length + 1;
			}
			object[] array = new object[num];
			this.CopyTo(array, 0);
			this._array = array;
			this._head = 0;
			this._tail = this._head + this._size;
		}

		// Token: 0x0400089D RID: 2205
		private object[] _array;

		// Token: 0x0400089E RID: 2206
		private int _head;

		// Token: 0x0400089F RID: 2207
		private int _size;

		// Token: 0x040008A0 RID: 2208
		private int _tail;

		// Token: 0x040008A1 RID: 2209
		private int _growFactor;

		// Token: 0x040008A2 RID: 2210
		private int _version;

		// Token: 0x020001C8 RID: 456
		private class SyncQueue : Queue
		{
			// Token: 0x0600178D RID: 6029 RVA: 0x0005AD7C File Offset: 0x00058F7C
			internal SyncQueue(Queue queue)
			{
				this.queue = queue;
			}

			// Token: 0x170003A4 RID: 932
			// (get) Token: 0x0600178E RID: 6030 RVA: 0x0005AD8C File Offset: 0x00058F8C
			public override int Count
			{
				get
				{
					Queue obj = this.queue;
					int count;
					lock (obj)
					{
						count = this.queue.Count;
					}
					return count;
				}
			}

			// Token: 0x170003A5 RID: 933
			// (get) Token: 0x0600178F RID: 6031 RVA: 0x0005ADE0 File Offset: 0x00058FE0
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x170003A6 RID: 934
			// (get) Token: 0x06001790 RID: 6032 RVA: 0x0005ADE4 File Offset: 0x00058FE4
			public override object SyncRoot
			{
				get
				{
					return this.queue.SyncRoot;
				}
			}

			// Token: 0x06001791 RID: 6033 RVA: 0x0005ADF4 File Offset: 0x00058FF4
			public override void CopyTo(Array array, int index)
			{
				Queue obj = this.queue;
				lock (obj)
				{
					this.queue.CopyTo(array, index);
				}
			}

			// Token: 0x06001792 RID: 6034 RVA: 0x0005AE44 File Offset: 0x00059044
			public override IEnumerator GetEnumerator()
			{
				Queue obj = this.queue;
				IEnumerator enumerator;
				lock (obj)
				{
					enumerator = this.queue.GetEnumerator();
				}
				return enumerator;
			}

			// Token: 0x06001793 RID: 6035 RVA: 0x0005AE98 File Offset: 0x00059098
			public override object Clone()
			{
				Queue obj = this.queue;
				object result;
				lock (obj)
				{
					result = new Queue.SyncQueue((Queue)this.queue.Clone());
				}
				return result;
			}

			// Token: 0x06001794 RID: 6036 RVA: 0x0005AEF8 File Offset: 0x000590F8
			public override void Clear()
			{
				Queue obj = this.queue;
				lock (obj)
				{
					this.queue.Clear();
				}
			}

			// Token: 0x06001795 RID: 6037 RVA: 0x0005AF48 File Offset: 0x00059148
			public override void TrimToSize()
			{
				Queue obj = this.queue;
				lock (obj)
				{
					this.queue.TrimToSize();
				}
			}

			// Token: 0x06001796 RID: 6038 RVA: 0x0005AF98 File Offset: 0x00059198
			public override bool Contains(object obj)
			{
				Queue obj2 = this.queue;
				bool result;
				lock (obj2)
				{
					result = this.queue.Contains(obj);
				}
				return result;
			}

			// Token: 0x06001797 RID: 6039 RVA: 0x0005AFF0 File Offset: 0x000591F0
			public override object Dequeue()
			{
				Queue obj = this.queue;
				object result;
				lock (obj)
				{
					result = this.queue.Dequeue();
				}
				return result;
			}

			// Token: 0x06001798 RID: 6040 RVA: 0x0005B044 File Offset: 0x00059244
			public override void Enqueue(object obj)
			{
				Queue obj2 = this.queue;
				lock (obj2)
				{
					this.queue.Enqueue(obj);
				}
			}

			// Token: 0x06001799 RID: 6041 RVA: 0x0005B094 File Offset: 0x00059294
			public override object Peek()
			{
				Queue obj = this.queue;
				object result;
				lock (obj)
				{
					result = this.queue.Peek();
				}
				return result;
			}

			// Token: 0x0600179A RID: 6042 RVA: 0x0005B0E8 File Offset: 0x000592E8
			public override object[] ToArray()
			{
				Queue obj = this.queue;
				object[] result;
				lock (obj)
				{
					result = this.queue.ToArray();
				}
				return result;
			}

			// Token: 0x040008A3 RID: 2211
			private Queue queue;
		}

		// Token: 0x020001C9 RID: 457
		[Serializable]
		private class QueueEnumerator : IEnumerator, ICloneable
		{
			// Token: 0x0600179B RID: 6043 RVA: 0x0005B13C File Offset: 0x0005933C
			internal QueueEnumerator(Queue q)
			{
				this.queue = q;
				this._version = q._version;
				this.current = -1;
			}

			// Token: 0x0600179C RID: 6044 RVA: 0x0005B16C File Offset: 0x0005936C
			public object Clone()
			{
				return new Queue.QueueEnumerator(this.queue)
				{
					_version = this._version,
					current = this.current
				};
			}

			// Token: 0x170003A7 RID: 935
			// (get) Token: 0x0600179D RID: 6045 RVA: 0x0005B1A0 File Offset: 0x000593A0
			public virtual object Current
			{
				get
				{
					if (this._version != this.queue._version || this.current < 0 || this.current >= this.queue._size)
					{
						throw new InvalidOperationException();
					}
					return this.queue._array[(this.queue._head + this.current) % this.queue._array.Length];
				}
			}

			// Token: 0x0600179E RID: 6046 RVA: 0x0005B218 File Offset: 0x00059418
			public virtual bool MoveNext()
			{
				if (this._version != this.queue._version)
				{
					throw new InvalidOperationException();
				}
				if (this.current >= this.queue._size - 1)
				{
					this.current = int.MaxValue;
					return false;
				}
				this.current++;
				return true;
			}

			// Token: 0x0600179F RID: 6047 RVA: 0x0005B278 File Offset: 0x00059478
			public virtual void Reset()
			{
				if (this._version != this.queue._version)
				{
					throw new InvalidOperationException();
				}
				this.current = -1;
			}

			// Token: 0x040008A4 RID: 2212
			private Queue queue;

			// Token: 0x040008A5 RID: 2213
			private int _version;

			// Token: 0x040008A6 RID: 2214
			private int current;
		}
	}
}
