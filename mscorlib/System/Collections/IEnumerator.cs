﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x02000017 RID: 23
	[ComVisible(true)]
	[Guid("496B0ABF-CDEE-11D3-88E8-00902754C43A")]
	public interface IEnumerator
	{
		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600016F RID: 367
		object Current { get; }

		// Token: 0x06000170 RID: 368
		bool MoveNext();

		// Token: 0x06000171 RID: 369
		void Reset();
	}
}
