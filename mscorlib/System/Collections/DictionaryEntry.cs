﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001B9 RID: 441
	[ComVisible(true)]
	[DebuggerDisplay("{_value}", Name = "[{_key}]")]
	[Serializable]
	public struct DictionaryEntry
	{
		// Token: 0x060016FC RID: 5884 RVA: 0x00059108 File Offset: 0x00057308
		public DictionaryEntry(object key, object value)
		{
			this._key = key;
			this._value = value;
		}

		// Token: 0x1700037A RID: 890
		// (get) Token: 0x060016FD RID: 5885 RVA: 0x00059118 File Offset: 0x00057318
		// (set) Token: 0x060016FE RID: 5886 RVA: 0x00059120 File Offset: 0x00057320
		public object Key
		{
			get
			{
				return this._key;
			}
			set
			{
				this._key = value;
			}
		}

		// Token: 0x1700037B RID: 891
		// (get) Token: 0x060016FF RID: 5887 RVA: 0x0005912C File Offset: 0x0005732C
		// (set) Token: 0x06001700 RID: 5888 RVA: 0x00059134 File Offset: 0x00057334
		public object Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x0400087B RID: 2171
		private object _key;

		// Token: 0x0400087C RID: 2172
		private object _value;
	}
}
