﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001A3 RID: 419
	[ComVisible(true)]
	[DebuggerDisplay("Count={Count}")]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView))]
	[Serializable]
	public class ArrayList : IEnumerable, ICloneable, ICollection, IList
	{
		// Token: 0x06001541 RID: 5441 RVA: 0x0005423C File Offset: 0x0005243C
		public ArrayList()
		{
			this._items = ArrayList.EmptyArray;
		}

		// Token: 0x06001542 RID: 5442 RVA: 0x00054250 File Offset: 0x00052450
		public ArrayList(ICollection c)
		{
			if (c == null)
			{
				throw new ArgumentNullException("c");
			}
			Array array = c as Array;
			if (array != null && array.Rank != 1)
			{
				throw new RankException();
			}
			this._items = new object[c.Count];
			this.AddRange(c);
		}

		// Token: 0x06001543 RID: 5443 RVA: 0x000542AC File Offset: 0x000524AC
		public ArrayList(int capacity)
		{
			if (capacity < 0)
			{
				ArrayList.ThrowNewArgumentOutOfRangeException("capacity", capacity, "The initial capacity can't be smaller than zero.");
			}
			if (capacity == 0)
			{
				capacity = 4;
			}
			this._items = new object[capacity];
		}

		// Token: 0x06001544 RID: 5444 RVA: 0x000542E8 File Offset: 0x000524E8
		private ArrayList(int initialCapacity, bool forceZeroSize)
		{
			if (forceZeroSize)
			{
				this._items = null;
				return;
			}
			throw new InvalidOperationException("Use ArrayList(int)");
		}

		// Token: 0x06001545 RID: 5445 RVA: 0x00054310 File Offset: 0x00052510
		private ArrayList(object[] array, int index, int count)
		{
			if (count == 0)
			{
				this._items = new object[4];
			}
			else
			{
				this._items = new object[count];
			}
			Array.Copy(array, index, this._items, 0, count);
			this._size = count;
		}

		// Token: 0x17000321 RID: 801
		public virtual object this[int index]
		{
			get
			{
				if (index < 0 || index >= this._size)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Index is less than 0 or more than or equal to the list count.");
				}
				return this._items[index];
			}
			set
			{
				if (index < 0 || index >= this._size)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Index is less than 0 or more than or equal to the list count.");
				}
				this._items[index] = value;
				this._version++;
			}
		}

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x06001549 RID: 5449 RVA: 0x000543F8 File Offset: 0x000525F8
		public virtual int Count
		{
			get
			{
				return this._size;
			}
		}

		// Token: 0x17000323 RID: 803
		// (get) Token: 0x0600154A RID: 5450 RVA: 0x00054400 File Offset: 0x00052600
		// (set) Token: 0x0600154B RID: 5451 RVA: 0x0005440C File Offset: 0x0005260C
		public virtual int Capacity
		{
			get
			{
				return this._items.Length;
			}
			set
			{
				if (value < this._size)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("Capacity", value, "Must be more than count.");
				}
				object[] array = new object[value];
				Array.Copy(this._items, 0, array, 0, this._size);
				this._items = array;
			}
		}

		// Token: 0x17000324 RID: 804
		// (get) Token: 0x0600154C RID: 5452 RVA: 0x0005445C File Offset: 0x0005265C
		public virtual bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000325 RID: 805
		// (get) Token: 0x0600154D RID: 5453 RVA: 0x00054460 File Offset: 0x00052660
		public virtual bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000326 RID: 806
		// (get) Token: 0x0600154E RID: 5454 RVA: 0x00054464 File Offset: 0x00052664
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000327 RID: 807
		// (get) Token: 0x0600154F RID: 5455 RVA: 0x00054468 File Offset: 0x00052668
		public virtual object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06001550 RID: 5456 RVA: 0x0005446C File Offset: 0x0005266C
		private void EnsureCapacity(int count)
		{
			if (count <= this._items.Length)
			{
				return;
			}
			int i = this._items.Length << 1;
			if (i == 0)
			{
				i = 4;
			}
			while (i < count)
			{
				i <<= 1;
			}
			object[] array = new object[i];
			Array.Copy(this._items, 0, array, 0, this._items.Length);
			this._items = array;
		}

		// Token: 0x06001551 RID: 5457 RVA: 0x000544D0 File Offset: 0x000526D0
		private void Shift(int index, int count)
		{
			if (count > 0)
			{
				if (this._size + count > this._items.Length)
				{
					int i;
					for (i = ((this._items.Length <= 0) ? 1 : (this._items.Length << 1)); i < this._size + count; i <<= 1)
					{
					}
					object[] array = new object[i];
					Array.Copy(this._items, 0, array, 0, index);
					Array.Copy(this._items, index, array, index + count, this._size - index);
					this._items = array;
				}
				else
				{
					Array.Copy(this._items, index, this._items, index + count, this._size - index);
				}
			}
			else if (count < 0)
			{
				int num = index - count;
				Array.Copy(this._items, num, this._items, index, this._size - num);
				Array.Clear(this._items, this._size + count, -count);
			}
		}

		// Token: 0x06001552 RID: 5458 RVA: 0x000545C8 File Offset: 0x000527C8
		public virtual int Add(object value)
		{
			if (this._items.Length <= this._size)
			{
				this.EnsureCapacity(this._size + 1);
			}
			this._items[this._size] = value;
			this._version++;
			return this._size++;
		}

		// Token: 0x06001553 RID: 5459 RVA: 0x00054624 File Offset: 0x00052824
		public virtual void Clear()
		{
			Array.Clear(this._items, 0, this._size);
			this._size = 0;
			this._version++;
		}

		// Token: 0x06001554 RID: 5460 RVA: 0x00054650 File Offset: 0x00052850
		public virtual bool Contains(object item)
		{
			return this.IndexOf(item, 0, this._size) > -1;
		}

		// Token: 0x06001555 RID: 5461 RVA: 0x00054664 File Offset: 0x00052864
		internal virtual bool Contains(object value, int startIndex, int count)
		{
			return this.IndexOf(value, startIndex, count) > -1;
		}

		// Token: 0x06001556 RID: 5462 RVA: 0x00054674 File Offset: 0x00052874
		public virtual int IndexOf(object value)
		{
			return this.IndexOf(value, 0);
		}

		// Token: 0x06001557 RID: 5463 RVA: 0x00054680 File Offset: 0x00052880
		public virtual int IndexOf(object value, int startIndex)
		{
			return this.IndexOf(value, startIndex, this._size - startIndex);
		}

		// Token: 0x06001558 RID: 5464 RVA: 0x00054694 File Offset: 0x00052894
		public virtual int IndexOf(object value, int startIndex, int count)
		{
			if (startIndex < 0 || startIndex > this._size)
			{
				ArrayList.ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "Does not specify valid index.");
			}
			if (count < 0)
			{
				ArrayList.ThrowNewArgumentOutOfRangeException("count", count, "Can't be less than 0.");
			}
			if (startIndex > this._size - count)
			{
				throw new ArgumentOutOfRangeException("count", "Start index and count do not specify a valid range.");
			}
			return Array.IndexOf<object>(this._items, value, startIndex, count);
		}

		// Token: 0x06001559 RID: 5465 RVA: 0x00054714 File Offset: 0x00052914
		public virtual int LastIndexOf(object value)
		{
			return this.LastIndexOf(value, this._size - 1);
		}

		// Token: 0x0600155A RID: 5466 RVA: 0x00054728 File Offset: 0x00052928
		public virtual int LastIndexOf(object value, int startIndex)
		{
			return this.LastIndexOf(value, startIndex, startIndex + 1);
		}

		// Token: 0x0600155B RID: 5467 RVA: 0x00054738 File Offset: 0x00052938
		public virtual int LastIndexOf(object value, int startIndex, int count)
		{
			return Array.LastIndexOf<object>(this._items, value, startIndex, count);
		}

		// Token: 0x0600155C RID: 5468 RVA: 0x00054748 File Offset: 0x00052948
		public virtual void Insert(int index, object value)
		{
			if (index < 0 || index > this._size)
			{
				ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
			}
			this.Shift(index, 1);
			this._items[index] = value;
			this._size++;
			this._version++;
		}

		// Token: 0x0600155D RID: 5469 RVA: 0x000547AC File Offset: 0x000529AC
		public virtual void InsertRange(int index, ICollection c)
		{
			if (c == null)
			{
				throw new ArgumentNullException("c");
			}
			if (index < 0 || index > this._size)
			{
				ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
			}
			int count = c.Count;
			if (this._items.Length < this._size + count)
			{
				this.EnsureCapacity(this._size + count);
			}
			if (index < this._size)
			{
				Array.Copy(this._items, index, this._items, index + count, this._size - index);
			}
			if (this == c.SyncRoot)
			{
				Array.Copy(this._items, 0, this._items, index, index);
				Array.Copy(this._items, index + count, this._items, index << 1, this._size - index);
			}
			else
			{
				c.CopyTo(this._items, index);
			}
			this._size += c.Count;
			this._version++;
		}

		// Token: 0x0600155E RID: 5470 RVA: 0x000548B8 File Offset: 0x00052AB8
		public virtual void Remove(object obj)
		{
			int num = this.IndexOf(obj);
			if (num > -1)
			{
				this.RemoveAt(num);
			}
			this._version++;
		}

		// Token: 0x0600155F RID: 5471 RVA: 0x000548EC File Offset: 0x00052AEC
		public virtual void RemoveAt(int index)
		{
			if (index < 0 || index >= this._size)
			{
				ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Less than 0 or more than list count.");
			}
			this.Shift(index, -1);
			this._size--;
			this._version++;
		}

		// Token: 0x06001560 RID: 5472 RVA: 0x00054948 File Offset: 0x00052B48
		public virtual void RemoveRange(int index, int count)
		{
			ArrayList.CheckRange(index, count, this._size);
			this.Shift(index, -count);
			this._size -= count;
			this._version++;
		}

		// Token: 0x06001561 RID: 5473 RVA: 0x00054988 File Offset: 0x00052B88
		public virtual void Reverse()
		{
			Array.Reverse(this._items, 0, this._size);
			this._version++;
		}

		// Token: 0x06001562 RID: 5474 RVA: 0x000549B8 File Offset: 0x00052BB8
		public virtual void Reverse(int index, int count)
		{
			ArrayList.CheckRange(index, count, this._size);
			Array.Reverse(this._items, index, count);
			this._version++;
		}

		// Token: 0x06001563 RID: 5475 RVA: 0x000549F0 File Offset: 0x00052BF0
		public virtual void CopyTo(Array array)
		{
			Array.Copy(this._items, array, this._size);
		}

		// Token: 0x06001564 RID: 5476 RVA: 0x00054A04 File Offset: 0x00052C04
		public virtual void CopyTo(Array array, int arrayIndex)
		{
			this.CopyTo(0, array, arrayIndex, this._size);
		}

		// Token: 0x06001565 RID: 5477 RVA: 0x00054A18 File Offset: 0x00052C18
		public virtual void CopyTo(int index, Array array, int arrayIndex, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank != 1)
			{
				throw new ArgumentException("Must have only 1 dimensions.", "array");
			}
			Array.Copy(this._items, index, array, arrayIndex, count);
		}

		// Token: 0x06001566 RID: 5478 RVA: 0x00054A58 File Offset: 0x00052C58
		public virtual IEnumerator GetEnumerator()
		{
			return new ArrayList.SimpleEnumerator(this);
		}

		// Token: 0x06001567 RID: 5479 RVA: 0x00054A60 File Offset: 0x00052C60
		public virtual IEnumerator GetEnumerator(int index, int count)
		{
			ArrayList.CheckRange(index, count, this._size);
			return new ArrayList.ArrayListEnumerator(this, index, count);
		}

		// Token: 0x06001568 RID: 5480 RVA: 0x00054A78 File Offset: 0x00052C78
		public virtual void AddRange(ICollection c)
		{
			this.InsertRange(this._size, c);
		}

		// Token: 0x06001569 RID: 5481 RVA: 0x00054A88 File Offset: 0x00052C88
		public virtual int BinarySearch(object value)
		{
			int result;
			try
			{
				result = Array.BinarySearch<object>(this._items, 0, this._size, value);
			}
			catch (InvalidOperationException ex)
			{
				throw new ArgumentException(ex.Message);
			}
			return result;
		}

		// Token: 0x0600156A RID: 5482 RVA: 0x00054AE4 File Offset: 0x00052CE4
		public virtual int BinarySearch(object value, IComparer comparer)
		{
			int result;
			try
			{
				result = Array.BinarySearch(this._items, 0, this._size, value, comparer);
			}
			catch (InvalidOperationException ex)
			{
				throw new ArgumentException(ex.Message);
			}
			return result;
		}

		// Token: 0x0600156B RID: 5483 RVA: 0x00054B40 File Offset: 0x00052D40
		public virtual int BinarySearch(int index, int count, object value, IComparer comparer)
		{
			int result;
			try
			{
				result = Array.BinarySearch(this._items, index, count, value, comparer);
			}
			catch (InvalidOperationException ex)
			{
				throw new ArgumentException(ex.Message);
			}
			return result;
		}

		// Token: 0x0600156C RID: 5484 RVA: 0x00054B98 File Offset: 0x00052D98
		public virtual ArrayList GetRange(int index, int count)
		{
			ArrayList.CheckRange(index, count, this._size);
			if (this.IsSynchronized)
			{
				return ArrayList.Synchronized(new ArrayList.RangedArrayList(this, index, count));
			}
			return new ArrayList.RangedArrayList(this, index, count);
		}

		// Token: 0x0600156D RID: 5485 RVA: 0x00054BD4 File Offset: 0x00052DD4
		public virtual void SetRange(int index, ICollection c)
		{
			if (c == null)
			{
				throw new ArgumentNullException("c");
			}
			if (index < 0 || index + c.Count > this._size)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			c.CopyTo(this._items, index);
			this._version++;
		}

		// Token: 0x0600156E RID: 5486 RVA: 0x00054C34 File Offset: 0x00052E34
		public virtual void TrimToSize()
		{
			if (this._items.Length > this._size)
			{
				object[] array;
				if (this._size == 0)
				{
					array = new object[4];
				}
				else
				{
					array = new object[this._size];
				}
				Array.Copy(this._items, 0, array, 0, this._size);
				this._items = array;
			}
		}

		// Token: 0x0600156F RID: 5487 RVA: 0x00054C94 File Offset: 0x00052E94
		public virtual void Sort()
		{
			Array.Sort<object>(this._items, 0, this._size);
			this._version++;
		}

		// Token: 0x06001570 RID: 5488 RVA: 0x00054CC4 File Offset: 0x00052EC4
		public virtual void Sort(IComparer comparer)
		{
			Array.Sort(this._items, 0, this._size, comparer);
		}

		// Token: 0x06001571 RID: 5489 RVA: 0x00054CDC File Offset: 0x00052EDC
		public virtual void Sort(int index, int count, IComparer comparer)
		{
			ArrayList.CheckRange(index, count, this._size);
			Array.Sort(this._items, index, count, comparer);
		}

		// Token: 0x06001572 RID: 5490 RVA: 0x00054CFC File Offset: 0x00052EFC
		public virtual object[] ToArray()
		{
			object[] array = new object[this._size];
			this.CopyTo(array);
			return array;
		}

		// Token: 0x06001573 RID: 5491 RVA: 0x00054D20 File Offset: 0x00052F20
		public virtual Array ToArray(Type type)
		{
			Array array = Array.CreateInstance(type, this._size);
			this.CopyTo(array);
			return array;
		}

		// Token: 0x06001574 RID: 5492 RVA: 0x00054D44 File Offset: 0x00052F44
		public virtual object Clone()
		{
			return new ArrayList(this._items, 0, this._size);
		}

		// Token: 0x06001575 RID: 5493 RVA: 0x00054D58 File Offset: 0x00052F58
		internal static void CheckRange(int index, int count, int listCount)
		{
			if (index < 0)
			{
				ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Can't be less than 0.");
			}
			if (count < 0)
			{
				ArrayList.ThrowNewArgumentOutOfRangeException("count", count, "Can't be less than 0.");
			}
			if (index > listCount - count)
			{
				throw new ArgumentException("Index and count do not denote a valid range of elements.", "index");
			}
		}

		// Token: 0x06001576 RID: 5494 RVA: 0x00054DB8 File Offset: 0x00052FB8
		internal static void ThrowNewArgumentOutOfRangeException(string name, object actual, string message)
		{
			throw new ArgumentOutOfRangeException(name, actual, message);
		}

		// Token: 0x06001577 RID: 5495 RVA: 0x00054DC4 File Offset: 0x00052FC4
		public static ArrayList Adapter(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			ArrayList arrayList = list as ArrayList;
			if (arrayList != null)
			{
				return arrayList;
			}
			arrayList = new ArrayList.ArrayListAdapter(list);
			if (list.IsSynchronized)
			{
				return ArrayList.Synchronized(arrayList);
			}
			return arrayList;
		}

		// Token: 0x06001578 RID: 5496 RVA: 0x00054E0C File Offset: 0x0005300C
		public static ArrayList Synchronized(ArrayList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsSynchronized)
			{
				return list;
			}
			return new ArrayList.SynchronizedArrayListWrapper(list);
		}

		// Token: 0x06001579 RID: 5497 RVA: 0x00054E40 File Offset: 0x00053040
		public static IList Synchronized(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsSynchronized)
			{
				return list;
			}
			return new ArrayList.SynchronizedListWrapper(list);
		}

		// Token: 0x0600157A RID: 5498 RVA: 0x00054E74 File Offset: 0x00053074
		public static ArrayList ReadOnly(ArrayList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsReadOnly)
			{
				return list;
			}
			return new ArrayList.ReadOnlyArrayListWrapper(list);
		}

		// Token: 0x0600157B RID: 5499 RVA: 0x00054EA8 File Offset: 0x000530A8
		public static IList ReadOnly(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsReadOnly)
			{
				return list;
			}
			return new ArrayList.ReadOnlyListWrapper(list);
		}

		// Token: 0x0600157C RID: 5500 RVA: 0x00054EDC File Offset: 0x000530DC
		public static ArrayList FixedSize(ArrayList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsFixedSize)
			{
				return list;
			}
			return new ArrayList.FixedSizeArrayListWrapper(list);
		}

		// Token: 0x0600157D RID: 5501 RVA: 0x00054F10 File Offset: 0x00053110
		public static IList FixedSize(IList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			if (list.IsFixedSize)
			{
				return list;
			}
			return new ArrayList.FixedSizeListWrapper(list);
		}

		// Token: 0x0600157E RID: 5502 RVA: 0x00054F44 File Offset: 0x00053144
		public static ArrayList Repeat(object value, int count)
		{
			ArrayList arrayList = new ArrayList(count);
			for (int i = 0; i < count; i++)
			{
				arrayList.Add(value);
			}
			return arrayList;
		}

		// Token: 0x0400084B RID: 2123
		private const int DefaultInitialCapacity = 4;

		// Token: 0x0400084C RID: 2124
		private int _size;

		// Token: 0x0400084D RID: 2125
		private object[] _items;

		// Token: 0x0400084E RID: 2126
		private int _version;

		// Token: 0x0400084F RID: 2127
		private static readonly object[] EmptyArray = new object[0];

		// Token: 0x020001A4 RID: 420
		private sealed class ArrayListEnumerator : IEnumerator, ICloneable
		{
			// Token: 0x0600157F RID: 5503 RVA: 0x00054F74 File Offset: 0x00053174
			public ArrayListEnumerator(ArrayList list) : this(list, 0, list.Count)
			{
			}

			// Token: 0x06001580 RID: 5504 RVA: 0x00054F84 File Offset: 0x00053184
			public ArrayListEnumerator(ArrayList list, int index, int count)
			{
				this.m_List = list;
				this.m_Index = index;
				this.m_Count = count;
				this.m_Pos = this.m_Index - 1;
				this.m_Current = null;
				this.m_ExpectedStateChanges = list._version;
			}

			// Token: 0x06001581 RID: 5505 RVA: 0x00054FD0 File Offset: 0x000531D0
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x17000328 RID: 808
			// (get) Token: 0x06001582 RID: 5506 RVA: 0x00054FD8 File Offset: 0x000531D8
			public object Current
			{
				get
				{
					if (this.m_Pos == this.m_Index - 1)
					{
						throw new InvalidOperationException("Enumerator unusable (Reset pending, or past end of array.");
					}
					return this.m_Current;
				}
			}

			// Token: 0x06001583 RID: 5507 RVA: 0x0005500C File Offset: 0x0005320C
			public bool MoveNext()
			{
				if (this.m_List._version != this.m_ExpectedStateChanges)
				{
					throw new InvalidOperationException("List has changed.");
				}
				this.m_Pos++;
				if (this.m_Pos - this.m_Index < this.m_Count)
				{
					this.m_Current = this.m_List[this.m_Pos];
					return true;
				}
				return false;
			}

			// Token: 0x06001584 RID: 5508 RVA: 0x0005507C File Offset: 0x0005327C
			public void Reset()
			{
				this.m_Current = null;
				this.m_Pos = this.m_Index - 1;
			}

			// Token: 0x04000850 RID: 2128
			private int m_Pos;

			// Token: 0x04000851 RID: 2129
			private int m_Index;

			// Token: 0x04000852 RID: 2130
			private int m_Count;

			// Token: 0x04000853 RID: 2131
			private object m_Current;

			// Token: 0x04000854 RID: 2132
			private ArrayList m_List;

			// Token: 0x04000855 RID: 2133
			private int m_ExpectedStateChanges;
		}

		// Token: 0x020001A5 RID: 421
		private sealed class SimpleEnumerator : IEnumerator, ICloneable
		{
			// Token: 0x06001585 RID: 5509 RVA: 0x00055094 File Offset: 0x00053294
			public SimpleEnumerator(ArrayList list)
			{
				this.list = list;
				this.index = -1;
				this.version = list._version;
				this.currentElement = ArrayList.SimpleEnumerator.endFlag;
			}

			// Token: 0x06001587 RID: 5511 RVA: 0x000550D0 File Offset: 0x000532D0
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x06001588 RID: 5512 RVA: 0x000550D8 File Offset: 0x000532D8
			public bool MoveNext()
			{
				if (this.version != this.list._version)
				{
					throw new InvalidOperationException("List has changed.");
				}
				if (++this.index < this.list.Count)
				{
					this.currentElement = this.list[this.index];
					return true;
				}
				this.currentElement = ArrayList.SimpleEnumerator.endFlag;
				return false;
			}

			// Token: 0x17000329 RID: 809
			// (get) Token: 0x06001589 RID: 5513 RVA: 0x0005514C File Offset: 0x0005334C
			public object Current
			{
				get
				{
					if (this.currentElement != ArrayList.SimpleEnumerator.endFlag)
					{
						return this.currentElement;
					}
					if (this.index == -1)
					{
						throw new InvalidOperationException("Enumerator not started");
					}
					throw new InvalidOperationException("Enumerator ended");
				}
			}

			// Token: 0x0600158A RID: 5514 RVA: 0x00055194 File Offset: 0x00053394
			public void Reset()
			{
				if (this.version != this.list._version)
				{
					throw new InvalidOperationException("List has changed.");
				}
				this.currentElement = ArrayList.SimpleEnumerator.endFlag;
				this.index = -1;
			}

			// Token: 0x04000856 RID: 2134
			private ArrayList list;

			// Token: 0x04000857 RID: 2135
			private int index;

			// Token: 0x04000858 RID: 2136
			private int version;

			// Token: 0x04000859 RID: 2137
			private object currentElement;

			// Token: 0x0400085A RID: 2138
			private static object endFlag = new object();
		}

		// Token: 0x020001A6 RID: 422
		[Serializable]
		private sealed class ArrayListAdapter : ArrayList
		{
			// Token: 0x0600158B RID: 5515 RVA: 0x000551CC File Offset: 0x000533CC
			public ArrayListAdapter(IList adaptee) : base(0, true)
			{
				this.m_Adaptee = adaptee;
			}

			// Token: 0x1700032A RID: 810
			public override object this[int index]
			{
				get
				{
					return this.m_Adaptee[index];
				}
				set
				{
					this.m_Adaptee[index] = value;
				}
			}

			// Token: 0x1700032B RID: 811
			// (get) Token: 0x0600158E RID: 5518 RVA: 0x00055200 File Offset: 0x00053400
			public override int Count
			{
				get
				{
					return this.m_Adaptee.Count;
				}
			}

			// Token: 0x1700032C RID: 812
			// (get) Token: 0x0600158F RID: 5519 RVA: 0x00055210 File Offset: 0x00053410
			// (set) Token: 0x06001590 RID: 5520 RVA: 0x00055220 File Offset: 0x00053420
			public override int Capacity
			{
				get
				{
					return this.m_Adaptee.Count;
				}
				set
				{
					if (value < this.m_Adaptee.Count)
					{
						throw new ArgumentException("capacity");
					}
				}
			}

			// Token: 0x1700032D RID: 813
			// (get) Token: 0x06001591 RID: 5521 RVA: 0x00055240 File Offset: 0x00053440
			public override bool IsFixedSize
			{
				get
				{
					return this.m_Adaptee.IsFixedSize;
				}
			}

			// Token: 0x1700032E RID: 814
			// (get) Token: 0x06001592 RID: 5522 RVA: 0x00055250 File Offset: 0x00053450
			public override bool IsReadOnly
			{
				get
				{
					return this.m_Adaptee.IsReadOnly;
				}
			}

			// Token: 0x1700032F RID: 815
			// (get) Token: 0x06001593 RID: 5523 RVA: 0x00055260 File Offset: 0x00053460
			public override object SyncRoot
			{
				get
				{
					return this.m_Adaptee.SyncRoot;
				}
			}

			// Token: 0x06001594 RID: 5524 RVA: 0x00055270 File Offset: 0x00053470
			public override int Add(object value)
			{
				return this.m_Adaptee.Add(value);
			}

			// Token: 0x06001595 RID: 5525 RVA: 0x00055280 File Offset: 0x00053480
			public override void Clear()
			{
				this.m_Adaptee.Clear();
			}

			// Token: 0x06001596 RID: 5526 RVA: 0x00055290 File Offset: 0x00053490
			public override bool Contains(object value)
			{
				return this.m_Adaptee.Contains(value);
			}

			// Token: 0x06001597 RID: 5527 RVA: 0x000552A0 File Offset: 0x000534A0
			public override int IndexOf(object value)
			{
				return this.m_Adaptee.IndexOf(value);
			}

			// Token: 0x06001598 RID: 5528 RVA: 0x000552B0 File Offset: 0x000534B0
			public override int IndexOf(object value, int startIndex)
			{
				return this.IndexOf(value, startIndex, this.m_Adaptee.Count - startIndex);
			}

			// Token: 0x06001599 RID: 5529 RVA: 0x000552C8 File Offset: 0x000534C8
			public override int IndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0 || startIndex > this.m_Adaptee.Count)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "Does not specify valid index.");
				}
				if (count < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("count", count, "Can't be less than 0.");
				}
				if (startIndex > this.m_Adaptee.Count - count)
				{
					throw new ArgumentOutOfRangeException("count", "Start index and count do not specify a valid range.");
				}
				if (value == null)
				{
					for (int i = startIndex; i < startIndex + count; i++)
					{
						if (this.m_Adaptee[i] == null)
						{
							return i;
						}
					}
				}
				else
				{
					for (int j = startIndex; j < startIndex + count; j++)
					{
						if (value.Equals(this.m_Adaptee[j]))
						{
							return j;
						}
					}
				}
				return -1;
			}

			// Token: 0x0600159A RID: 5530 RVA: 0x000553A4 File Offset: 0x000535A4
			public override int LastIndexOf(object value)
			{
				return this.LastIndexOf(value, this.m_Adaptee.Count - 1);
			}

			// Token: 0x0600159B RID: 5531 RVA: 0x000553BC File Offset: 0x000535BC
			public override int LastIndexOf(object value, int startIndex)
			{
				return this.LastIndexOf(value, startIndex, startIndex + 1);
			}

			// Token: 0x0600159C RID: 5532 RVA: 0x000553CC File Offset: 0x000535CC
			public override int LastIndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "< 0");
				}
				if (count < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("count", count, "count is negative.");
				}
				if (startIndex - count + 1 < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("count", count, "count is too large.");
				}
				if (value == null)
				{
					for (int i = startIndex; i > startIndex - count; i--)
					{
						if (this.m_Adaptee[i] == null)
						{
							return i;
						}
					}
				}
				else
				{
					for (int j = startIndex; j > startIndex - count; j--)
					{
						if (value.Equals(this.m_Adaptee[j]))
						{
							return j;
						}
					}
				}
				return -1;
			}

			// Token: 0x0600159D RID: 5533 RVA: 0x00055494 File Offset: 0x00053694
			public override void Insert(int index, object value)
			{
				this.m_Adaptee.Insert(index, value);
			}

			// Token: 0x0600159E RID: 5534 RVA: 0x000554A4 File Offset: 0x000536A4
			public override void InsertRange(int index, ICollection c)
			{
				if (c == null)
				{
					throw new ArgumentNullException("c");
				}
				if (index > this.m_Adaptee.Count)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				foreach (object value in c)
				{
					this.m_Adaptee.Insert(index++, value);
				}
			}

			// Token: 0x0600159F RID: 5535 RVA: 0x0005554C File Offset: 0x0005374C
			public override void Remove(object value)
			{
				this.m_Adaptee.Remove(value);
			}

			// Token: 0x060015A0 RID: 5536 RVA: 0x0005555C File Offset: 0x0005375C
			public override void RemoveAt(int index)
			{
				this.m_Adaptee.RemoveAt(index);
			}

			// Token: 0x060015A1 RID: 5537 RVA: 0x0005556C File Offset: 0x0005376C
			public override void RemoveRange(int index, int count)
			{
				ArrayList.CheckRange(index, count, this.m_Adaptee.Count);
				for (int i = 0; i < count; i++)
				{
					this.m_Adaptee.RemoveAt(index);
				}
			}

			// Token: 0x060015A2 RID: 5538 RVA: 0x000555AC File Offset: 0x000537AC
			public override void Reverse()
			{
				this.Reverse(0, this.m_Adaptee.Count);
			}

			// Token: 0x060015A3 RID: 5539 RVA: 0x000555C0 File Offset: 0x000537C0
			public override void Reverse(int index, int count)
			{
				ArrayList.CheckRange(index, count, this.m_Adaptee.Count);
				for (int i = 0; i < count / 2; i++)
				{
					object value = this.m_Adaptee[i + index];
					this.m_Adaptee[i + index] = this.m_Adaptee[index + count - i + index - 1];
					this.m_Adaptee[index + count - i + index - 1] = value;
				}
			}

			// Token: 0x060015A4 RID: 5540 RVA: 0x0005563C File Offset: 0x0005383C
			public override void SetRange(int index, ICollection c)
			{
				if (c == null)
				{
					throw new ArgumentNullException("c");
				}
				if (index < 0 || index + c.Count > this.m_Adaptee.Count)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				int num = index;
				foreach (object value in c)
				{
					this.m_Adaptee[num++] = value;
				}
			}

			// Token: 0x060015A5 RID: 5541 RVA: 0x000556E8 File Offset: 0x000538E8
			public override void CopyTo(Array array)
			{
				this.m_Adaptee.CopyTo(array, 0);
			}

			// Token: 0x060015A6 RID: 5542 RVA: 0x000556F8 File Offset: 0x000538F8
			public override void CopyTo(Array array, int index)
			{
				this.m_Adaptee.CopyTo(array, index);
			}

			// Token: 0x060015A7 RID: 5543 RVA: 0x00055708 File Offset: 0x00053908
			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				if (index < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Can't be less than zero.");
				}
				if (arrayIndex < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("arrayIndex", arrayIndex, "Can't be less than zero.");
				}
				if (count < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Can't be less than zero.");
				}
				if (index >= this.m_Adaptee.Count)
				{
					throw new ArgumentException("Can't be more or equal to list count.", "index");
				}
				if (array.Rank > 1)
				{
					throw new ArgumentException("Can't copy into multi-dimensional array.");
				}
				if (arrayIndex >= array.Length)
				{
					throw new ArgumentException("arrayIndex can't be greater than array.Length - 1.");
				}
				if (array.Length - arrayIndex + 1 < count)
				{
					throw new ArgumentException("Destination array is too small.");
				}
				if (index > this.m_Adaptee.Count - count)
				{
					throw new ArgumentException("Index and count do not denote a valid range of elements.", "index");
				}
				for (int i = 0; i < count; i++)
				{
					array.SetValue(this.m_Adaptee[index + i], arrayIndex + i);
				}
			}

			// Token: 0x17000330 RID: 816
			// (get) Token: 0x060015A8 RID: 5544 RVA: 0x00055824 File Offset: 0x00053A24
			public override bool IsSynchronized
			{
				get
				{
					return this.m_Adaptee.IsSynchronized;
				}
			}

			// Token: 0x060015A9 RID: 5545 RVA: 0x00055834 File Offset: 0x00053A34
			public override IEnumerator GetEnumerator()
			{
				return this.m_Adaptee.GetEnumerator();
			}

			// Token: 0x060015AA RID: 5546 RVA: 0x00055844 File Offset: 0x00053A44
			public override IEnumerator GetEnumerator(int index, int count)
			{
				ArrayList.CheckRange(index, count, this.m_Adaptee.Count);
				return new ArrayList.ArrayListAdapter.EnumeratorWithRange(this.m_Adaptee.GetEnumerator(), index, count);
			}

			// Token: 0x060015AB RID: 5547 RVA: 0x00055878 File Offset: 0x00053A78
			public override void AddRange(ICollection c)
			{
				foreach (object value in c)
				{
					this.m_Adaptee.Add(value);
				}
			}

			// Token: 0x060015AC RID: 5548 RVA: 0x000558E4 File Offset: 0x00053AE4
			public override int BinarySearch(object value)
			{
				return this.BinarySearch(value, null);
			}

			// Token: 0x060015AD RID: 5549 RVA: 0x000558F0 File Offset: 0x00053AF0
			public override int BinarySearch(object value, IComparer comparer)
			{
				return this.BinarySearch(0, this.m_Adaptee.Count, value, comparer);
			}

			// Token: 0x060015AE RID: 5550 RVA: 0x00055908 File Offset: 0x00053B08
			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				ArrayList.CheckRange(index, count, this.m_Adaptee.Count);
				if (comparer == null)
				{
					comparer = Comparer.Default;
				}
				int i = index;
				int num = index + count - 1;
				while (i <= num)
				{
					int num2 = i + (num - i) / 2;
					int num3 = comparer.Compare(value, this.m_Adaptee[num2]);
					if (num3 < 0)
					{
						num = num2 - 1;
					}
					else
					{
						if (num3 <= 0)
						{
							return num2;
						}
						i = num2 + 1;
					}
				}
				return ~i;
			}

			// Token: 0x060015AF RID: 5551 RVA: 0x0005598C File Offset: 0x00053B8C
			public override object Clone()
			{
				return new ArrayList.ArrayListAdapter(this.m_Adaptee);
			}

			// Token: 0x060015B0 RID: 5552 RVA: 0x0005599C File Offset: 0x00053B9C
			public override ArrayList GetRange(int index, int count)
			{
				ArrayList.CheckRange(index, count, this.m_Adaptee.Count);
				return new ArrayList.RangedArrayList(this, index, count);
			}

			// Token: 0x060015B1 RID: 5553 RVA: 0x000559B8 File Offset: 0x00053BB8
			public override void TrimToSize()
			{
			}

			// Token: 0x060015B2 RID: 5554 RVA: 0x000559BC File Offset: 0x00053BBC
			public override void Sort()
			{
				this.Sort(Comparer.Default);
			}

			// Token: 0x060015B3 RID: 5555 RVA: 0x000559CC File Offset: 0x00053BCC
			public override void Sort(IComparer comparer)
			{
				this.Sort(0, this.m_Adaptee.Count, comparer);
			}

			// Token: 0x060015B4 RID: 5556 RVA: 0x000559E4 File Offset: 0x00053BE4
			public override void Sort(int index, int count, IComparer comparer)
			{
				ArrayList.CheckRange(index, count, this.m_Adaptee.Count);
				if (comparer == null)
				{
					comparer = Comparer.Default;
				}
				ArrayList.ArrayListAdapter.QuickSort(this.m_Adaptee, index, index + count - 1, comparer);
			}

			// Token: 0x060015B5 RID: 5557 RVA: 0x00055A24 File Offset: 0x00053C24
			private static void Swap(IList list, int x, int y)
			{
				object value = list[x];
				list[x] = list[y];
				list[y] = value;
			}

			// Token: 0x060015B6 RID: 5558 RVA: 0x00055A50 File Offset: 0x00053C50
			internal static void QuickSort(IList list, int left, int right, IComparer comparer)
			{
				if (left >= right)
				{
					return;
				}
				int num = left + (right - left) / 2;
				if (comparer.Compare(list[num], list[left]) < 0)
				{
					ArrayList.ArrayListAdapter.Swap(list, num, left);
				}
				if (comparer.Compare(list[right], list[left]) < 0)
				{
					ArrayList.ArrayListAdapter.Swap(list, right, left);
				}
				if (comparer.Compare(list[right], list[num]) < 0)
				{
					ArrayList.ArrayListAdapter.Swap(list, right, num);
				}
				if (right - left + 1 <= 3)
				{
					return;
				}
				ArrayList.ArrayListAdapter.Swap(list, right - 1, num);
				object y = list[right - 1];
				int num2 = left;
				int num3 = right - 1;
				for (;;)
				{
					while (comparer.Compare(list[++num2], y) < 0)
					{
					}
					while (comparer.Compare(list[--num3], y) > 0)
					{
					}
					if (num2 >= num3)
					{
						break;
					}
					ArrayList.ArrayListAdapter.Swap(list, num2, num3);
				}
				ArrayList.ArrayListAdapter.Swap(list, right - 1, num2);
				ArrayList.ArrayListAdapter.QuickSort(list, left, num2 - 1, comparer);
				ArrayList.ArrayListAdapter.QuickSort(list, num2 + 1, right, comparer);
			}

			// Token: 0x060015B7 RID: 5559 RVA: 0x00055B78 File Offset: 0x00053D78
			public override object[] ToArray()
			{
				object[] array = new object[this.m_Adaptee.Count];
				this.m_Adaptee.CopyTo(array, 0);
				return array;
			}

			// Token: 0x060015B8 RID: 5560 RVA: 0x00055BA4 File Offset: 0x00053DA4
			public override Array ToArray(Type elementType)
			{
				Array array = Array.CreateInstance(elementType, this.m_Adaptee.Count);
				this.m_Adaptee.CopyTo(array, 0);
				return array;
			}

			// Token: 0x0400085B RID: 2139
			private IList m_Adaptee;

			// Token: 0x020001A7 RID: 423
			private sealed class EnumeratorWithRange : IEnumerator, ICloneable
			{
				// Token: 0x060015B9 RID: 5561 RVA: 0x00055BD4 File Offset: 0x00053DD4
				public EnumeratorWithRange(IEnumerator enumerator, int index, int count)
				{
					this.m_Count = 0;
					this.m_StartIndex = index;
					this.m_MaxCount = count;
					this.m_Enumerator = enumerator;
					this.Reset();
				}

				// Token: 0x060015BA RID: 5562 RVA: 0x00055C0C File Offset: 0x00053E0C
				public object Clone()
				{
					return base.MemberwiseClone();
				}

				// Token: 0x17000331 RID: 817
				// (get) Token: 0x060015BB RID: 5563 RVA: 0x00055C14 File Offset: 0x00053E14
				public object Current
				{
					get
					{
						return this.m_Enumerator.Current;
					}
				}

				// Token: 0x060015BC RID: 5564 RVA: 0x00055C24 File Offset: 0x00053E24
				public bool MoveNext()
				{
					if (this.m_Count >= this.m_MaxCount)
					{
						return false;
					}
					this.m_Count++;
					return this.m_Enumerator.MoveNext();
				}

				// Token: 0x060015BD RID: 5565 RVA: 0x00055C60 File Offset: 0x00053E60
				public void Reset()
				{
					this.m_Count = 0;
					this.m_Enumerator.Reset();
					for (int i = 0; i < this.m_StartIndex; i++)
					{
						this.m_Enumerator.MoveNext();
					}
				}

				// Token: 0x0400085C RID: 2140
				private int m_StartIndex;

				// Token: 0x0400085D RID: 2141
				private int m_Count;

				// Token: 0x0400085E RID: 2142
				private int m_MaxCount;

				// Token: 0x0400085F RID: 2143
				private IEnumerator m_Enumerator;
			}
		}

		// Token: 0x020001A8 RID: 424
		[Serializable]
		private class ArrayListWrapper : ArrayList
		{
			// Token: 0x060015BE RID: 5566 RVA: 0x00055CA4 File Offset: 0x00053EA4
			public ArrayListWrapper(ArrayList innerArrayList)
			{
				this.m_InnerArrayList = innerArrayList;
			}

			// Token: 0x17000332 RID: 818
			public override object this[int index]
			{
				get
				{
					return this.m_InnerArrayList[index];
				}
				set
				{
					this.m_InnerArrayList[index] = value;
				}
			}

			// Token: 0x17000333 RID: 819
			// (get) Token: 0x060015C1 RID: 5569 RVA: 0x00055CD4 File Offset: 0x00053ED4
			public override int Count
			{
				get
				{
					return this.m_InnerArrayList.Count;
				}
			}

			// Token: 0x17000334 RID: 820
			// (get) Token: 0x060015C2 RID: 5570 RVA: 0x00055CE4 File Offset: 0x00053EE4
			// (set) Token: 0x060015C3 RID: 5571 RVA: 0x00055CF4 File Offset: 0x00053EF4
			public override int Capacity
			{
				get
				{
					return this.m_InnerArrayList.Capacity;
				}
				set
				{
					this.m_InnerArrayList.Capacity = value;
				}
			}

			// Token: 0x17000335 RID: 821
			// (get) Token: 0x060015C4 RID: 5572 RVA: 0x00055D04 File Offset: 0x00053F04
			public override bool IsFixedSize
			{
				get
				{
					return this.m_InnerArrayList.IsFixedSize;
				}
			}

			// Token: 0x17000336 RID: 822
			// (get) Token: 0x060015C5 RID: 5573 RVA: 0x00055D14 File Offset: 0x00053F14
			public override bool IsReadOnly
			{
				get
				{
					return this.m_InnerArrayList.IsReadOnly;
				}
			}

			// Token: 0x17000337 RID: 823
			// (get) Token: 0x060015C6 RID: 5574 RVA: 0x00055D24 File Offset: 0x00053F24
			public override bool IsSynchronized
			{
				get
				{
					return this.m_InnerArrayList.IsSynchronized;
				}
			}

			// Token: 0x17000338 RID: 824
			// (get) Token: 0x060015C7 RID: 5575 RVA: 0x00055D34 File Offset: 0x00053F34
			public override object SyncRoot
			{
				get
				{
					return this.m_InnerArrayList.SyncRoot;
				}
			}

			// Token: 0x060015C8 RID: 5576 RVA: 0x00055D44 File Offset: 0x00053F44
			public override int Add(object value)
			{
				return this.m_InnerArrayList.Add(value);
			}

			// Token: 0x060015C9 RID: 5577 RVA: 0x00055D54 File Offset: 0x00053F54
			public override void Clear()
			{
				this.m_InnerArrayList.Clear();
			}

			// Token: 0x060015CA RID: 5578 RVA: 0x00055D64 File Offset: 0x00053F64
			public override bool Contains(object value)
			{
				return this.m_InnerArrayList.Contains(value);
			}

			// Token: 0x060015CB RID: 5579 RVA: 0x00055D74 File Offset: 0x00053F74
			public override int IndexOf(object value)
			{
				return this.m_InnerArrayList.IndexOf(value);
			}

			// Token: 0x060015CC RID: 5580 RVA: 0x00055D84 File Offset: 0x00053F84
			public override int IndexOf(object value, int startIndex)
			{
				return this.m_InnerArrayList.IndexOf(value, startIndex);
			}

			// Token: 0x060015CD RID: 5581 RVA: 0x00055D94 File Offset: 0x00053F94
			public override int IndexOf(object value, int startIndex, int count)
			{
				return this.m_InnerArrayList.IndexOf(value, startIndex, count);
			}

			// Token: 0x060015CE RID: 5582 RVA: 0x00055DA4 File Offset: 0x00053FA4
			public override int LastIndexOf(object value)
			{
				return this.m_InnerArrayList.LastIndexOf(value);
			}

			// Token: 0x060015CF RID: 5583 RVA: 0x00055DB4 File Offset: 0x00053FB4
			public override int LastIndexOf(object value, int startIndex)
			{
				return this.m_InnerArrayList.LastIndexOf(value, startIndex);
			}

			// Token: 0x060015D0 RID: 5584 RVA: 0x00055DC4 File Offset: 0x00053FC4
			public override int LastIndexOf(object value, int startIndex, int count)
			{
				return this.m_InnerArrayList.LastIndexOf(value, startIndex, count);
			}

			// Token: 0x060015D1 RID: 5585 RVA: 0x00055DD4 File Offset: 0x00053FD4
			public override void Insert(int index, object value)
			{
				this.m_InnerArrayList.Insert(index, value);
			}

			// Token: 0x060015D2 RID: 5586 RVA: 0x00055DE4 File Offset: 0x00053FE4
			public override void InsertRange(int index, ICollection c)
			{
				this.m_InnerArrayList.InsertRange(index, c);
			}

			// Token: 0x060015D3 RID: 5587 RVA: 0x00055DF4 File Offset: 0x00053FF4
			public override void Remove(object value)
			{
				this.m_InnerArrayList.Remove(value);
			}

			// Token: 0x060015D4 RID: 5588 RVA: 0x00055E04 File Offset: 0x00054004
			public override void RemoveAt(int index)
			{
				this.m_InnerArrayList.RemoveAt(index);
			}

			// Token: 0x060015D5 RID: 5589 RVA: 0x00055E14 File Offset: 0x00054014
			public override void RemoveRange(int index, int count)
			{
				this.m_InnerArrayList.RemoveRange(index, count);
			}

			// Token: 0x060015D6 RID: 5590 RVA: 0x00055E24 File Offset: 0x00054024
			public override void Reverse()
			{
				this.m_InnerArrayList.Reverse();
			}

			// Token: 0x060015D7 RID: 5591 RVA: 0x00055E34 File Offset: 0x00054034
			public override void Reverse(int index, int count)
			{
				this.m_InnerArrayList.Reverse(index, count);
			}

			// Token: 0x060015D8 RID: 5592 RVA: 0x00055E44 File Offset: 0x00054044
			public override void SetRange(int index, ICollection c)
			{
				this.m_InnerArrayList.SetRange(index, c);
			}

			// Token: 0x060015D9 RID: 5593 RVA: 0x00055E54 File Offset: 0x00054054
			public override void CopyTo(Array array)
			{
				this.m_InnerArrayList.CopyTo(array);
			}

			// Token: 0x060015DA RID: 5594 RVA: 0x00055E64 File Offset: 0x00054064
			public override void CopyTo(Array array, int index)
			{
				this.m_InnerArrayList.CopyTo(array, index);
			}

			// Token: 0x060015DB RID: 5595 RVA: 0x00055E74 File Offset: 0x00054074
			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				this.m_InnerArrayList.CopyTo(index, array, arrayIndex, count);
			}

			// Token: 0x060015DC RID: 5596 RVA: 0x00055E88 File Offset: 0x00054088
			public override IEnumerator GetEnumerator()
			{
				return this.m_InnerArrayList.GetEnumerator();
			}

			// Token: 0x060015DD RID: 5597 RVA: 0x00055E98 File Offset: 0x00054098
			public override IEnumerator GetEnumerator(int index, int count)
			{
				return this.m_InnerArrayList.GetEnumerator(index, count);
			}

			// Token: 0x060015DE RID: 5598 RVA: 0x00055EA8 File Offset: 0x000540A8
			public override void AddRange(ICollection c)
			{
				this.m_InnerArrayList.AddRange(c);
			}

			// Token: 0x060015DF RID: 5599 RVA: 0x00055EB8 File Offset: 0x000540B8
			public override int BinarySearch(object value)
			{
				return this.m_InnerArrayList.BinarySearch(value);
			}

			// Token: 0x060015E0 RID: 5600 RVA: 0x00055EC8 File Offset: 0x000540C8
			public override int BinarySearch(object value, IComparer comparer)
			{
				return this.m_InnerArrayList.BinarySearch(value, comparer);
			}

			// Token: 0x060015E1 RID: 5601 RVA: 0x00055ED8 File Offset: 0x000540D8
			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				return this.m_InnerArrayList.BinarySearch(index, count, value, comparer);
			}

			// Token: 0x060015E2 RID: 5602 RVA: 0x00055EEC File Offset: 0x000540EC
			public override object Clone()
			{
				return this.m_InnerArrayList.Clone();
			}

			// Token: 0x060015E3 RID: 5603 RVA: 0x00055EFC File Offset: 0x000540FC
			public override ArrayList GetRange(int index, int count)
			{
				return this.m_InnerArrayList.GetRange(index, count);
			}

			// Token: 0x060015E4 RID: 5604 RVA: 0x00055F0C File Offset: 0x0005410C
			public override void TrimToSize()
			{
				this.m_InnerArrayList.TrimToSize();
			}

			// Token: 0x060015E5 RID: 5605 RVA: 0x00055F1C File Offset: 0x0005411C
			public override void Sort()
			{
				this.m_InnerArrayList.Sort();
			}

			// Token: 0x060015E6 RID: 5606 RVA: 0x00055F2C File Offset: 0x0005412C
			public override void Sort(IComparer comparer)
			{
				this.m_InnerArrayList.Sort(comparer);
			}

			// Token: 0x060015E7 RID: 5607 RVA: 0x00055F3C File Offset: 0x0005413C
			public override void Sort(int index, int count, IComparer comparer)
			{
				this.m_InnerArrayList.Sort(index, count, comparer);
			}

			// Token: 0x060015E8 RID: 5608 RVA: 0x00055F4C File Offset: 0x0005414C
			public override object[] ToArray()
			{
				return this.m_InnerArrayList.ToArray();
			}

			// Token: 0x060015E9 RID: 5609 RVA: 0x00055F5C File Offset: 0x0005415C
			public override Array ToArray(Type elementType)
			{
				return this.m_InnerArrayList.ToArray(elementType);
			}

			// Token: 0x04000860 RID: 2144
			protected ArrayList m_InnerArrayList;
		}

		// Token: 0x020001A9 RID: 425
		[Serializable]
		private sealed class SynchronizedArrayListWrapper : ArrayList.ArrayListWrapper
		{
			// Token: 0x060015EA RID: 5610 RVA: 0x00055F6C File Offset: 0x0005416C
			internal SynchronizedArrayListWrapper(ArrayList innerArrayList) : base(innerArrayList)
			{
				this.m_SyncRoot = innerArrayList.SyncRoot;
			}

			// Token: 0x17000339 RID: 825
			public override object this[int index]
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					object result;
					lock (syncRoot)
					{
						result = this.m_InnerArrayList[index];
					}
					return result;
				}
				set
				{
					object syncRoot = this.m_SyncRoot;
					lock (syncRoot)
					{
						this.m_InnerArrayList[index] = value;
					}
				}
			}

			// Token: 0x1700033A RID: 826
			// (get) Token: 0x060015ED RID: 5613 RVA: 0x0005602C File Offset: 0x0005422C
			public override int Count
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					int count;
					lock (syncRoot)
					{
						count = this.m_InnerArrayList.Count;
					}
					return count;
				}
			}

			// Token: 0x1700033B RID: 827
			// (get) Token: 0x060015EE RID: 5614 RVA: 0x00056080 File Offset: 0x00054280
			// (set) Token: 0x060015EF RID: 5615 RVA: 0x000560D4 File Offset: 0x000542D4
			public override int Capacity
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					int capacity;
					lock (syncRoot)
					{
						capacity = this.m_InnerArrayList.Capacity;
					}
					return capacity;
				}
				set
				{
					object syncRoot = this.m_SyncRoot;
					lock (syncRoot)
					{
						this.m_InnerArrayList.Capacity = value;
					}
				}
			}

			// Token: 0x1700033C RID: 828
			// (get) Token: 0x060015F0 RID: 5616 RVA: 0x00056124 File Offset: 0x00054324
			public override bool IsFixedSize
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					bool isFixedSize;
					lock (syncRoot)
					{
						isFixedSize = this.m_InnerArrayList.IsFixedSize;
					}
					return isFixedSize;
				}
			}

			// Token: 0x1700033D RID: 829
			// (get) Token: 0x060015F1 RID: 5617 RVA: 0x00056178 File Offset: 0x00054378
			public override bool IsReadOnly
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					bool isReadOnly;
					lock (syncRoot)
					{
						isReadOnly = this.m_InnerArrayList.IsReadOnly;
					}
					return isReadOnly;
				}
			}

			// Token: 0x1700033E RID: 830
			// (get) Token: 0x060015F2 RID: 5618 RVA: 0x000561CC File Offset: 0x000543CC
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x1700033F RID: 831
			// (get) Token: 0x060015F3 RID: 5619 RVA: 0x000561D0 File Offset: 0x000543D0
			public override object SyncRoot
			{
				get
				{
					return this.m_SyncRoot;
				}
			}

			// Token: 0x060015F4 RID: 5620 RVA: 0x000561D8 File Offset: 0x000543D8
			public override int Add(object value)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.Add(value);
				}
				return result;
			}

			// Token: 0x060015F5 RID: 5621 RVA: 0x00056230 File Offset: 0x00054430
			public override void Clear()
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.Clear();
				}
			}

			// Token: 0x060015F6 RID: 5622 RVA: 0x00056280 File Offset: 0x00054480
			public override bool Contains(object value)
			{
				object syncRoot = this.m_SyncRoot;
				bool result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.Contains(value);
				}
				return result;
			}

			// Token: 0x060015F7 RID: 5623 RVA: 0x000562D8 File Offset: 0x000544D8
			public override int IndexOf(object value)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.IndexOf(value);
				}
				return result;
			}

			// Token: 0x060015F8 RID: 5624 RVA: 0x00056330 File Offset: 0x00054530
			public override int IndexOf(object value, int startIndex)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.IndexOf(value, startIndex);
				}
				return result;
			}

			// Token: 0x060015F9 RID: 5625 RVA: 0x00056388 File Offset: 0x00054588
			public override int IndexOf(object value, int startIndex, int count)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.IndexOf(value, startIndex, count);
				}
				return result;
			}

			// Token: 0x060015FA RID: 5626 RVA: 0x000563E0 File Offset: 0x000545E0
			public override int LastIndexOf(object value)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.LastIndexOf(value);
				}
				return result;
			}

			// Token: 0x060015FB RID: 5627 RVA: 0x00056438 File Offset: 0x00054638
			public override int LastIndexOf(object value, int startIndex)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.LastIndexOf(value, startIndex);
				}
				return result;
			}

			// Token: 0x060015FC RID: 5628 RVA: 0x00056490 File Offset: 0x00054690
			public override int LastIndexOf(object value, int startIndex, int count)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.LastIndexOf(value, startIndex, count);
				}
				return result;
			}

			// Token: 0x060015FD RID: 5629 RVA: 0x000564E8 File Offset: 0x000546E8
			public override void Insert(int index, object value)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.Insert(index, value);
				}
			}

			// Token: 0x060015FE RID: 5630 RVA: 0x00056538 File Offset: 0x00054738
			public override void InsertRange(int index, ICollection c)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.InsertRange(index, c);
				}
			}

			// Token: 0x060015FF RID: 5631 RVA: 0x00056588 File Offset: 0x00054788
			public override void Remove(object value)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.Remove(value);
				}
			}

			// Token: 0x06001600 RID: 5632 RVA: 0x000565D8 File Offset: 0x000547D8
			public override void RemoveAt(int index)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.RemoveAt(index);
				}
			}

			// Token: 0x06001601 RID: 5633 RVA: 0x00056628 File Offset: 0x00054828
			public override void RemoveRange(int index, int count)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.RemoveRange(index, count);
				}
			}

			// Token: 0x06001602 RID: 5634 RVA: 0x00056678 File Offset: 0x00054878
			public override void Reverse()
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.Reverse();
				}
			}

			// Token: 0x06001603 RID: 5635 RVA: 0x000566C8 File Offset: 0x000548C8
			public override void Reverse(int index, int count)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.Reverse(index, count);
				}
			}

			// Token: 0x06001604 RID: 5636 RVA: 0x00056718 File Offset: 0x00054918
			public override void CopyTo(Array array)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.CopyTo(array);
				}
			}

			// Token: 0x06001605 RID: 5637 RVA: 0x00056768 File Offset: 0x00054968
			public override void CopyTo(Array array, int index)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.CopyTo(array, index);
				}
			}

			// Token: 0x06001606 RID: 5638 RVA: 0x000567B8 File Offset: 0x000549B8
			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.CopyTo(index, array, arrayIndex, count);
				}
			}

			// Token: 0x06001607 RID: 5639 RVA: 0x0005680C File Offset: 0x00054A0C
			public override IEnumerator GetEnumerator()
			{
				object syncRoot = this.m_SyncRoot;
				IEnumerator enumerator;
				lock (syncRoot)
				{
					enumerator = this.m_InnerArrayList.GetEnumerator();
				}
				return enumerator;
			}

			// Token: 0x06001608 RID: 5640 RVA: 0x00056860 File Offset: 0x00054A60
			public override IEnumerator GetEnumerator(int index, int count)
			{
				object syncRoot = this.m_SyncRoot;
				IEnumerator enumerator;
				lock (syncRoot)
				{
					enumerator = this.m_InnerArrayList.GetEnumerator(index, count);
				}
				return enumerator;
			}

			// Token: 0x06001609 RID: 5641 RVA: 0x000568B8 File Offset: 0x00054AB8
			public override void AddRange(ICollection c)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.AddRange(c);
				}
			}

			// Token: 0x0600160A RID: 5642 RVA: 0x00056908 File Offset: 0x00054B08
			public override int BinarySearch(object value)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.BinarySearch(value);
				}
				return result;
			}

			// Token: 0x0600160B RID: 5643 RVA: 0x00056960 File Offset: 0x00054B60
			public override int BinarySearch(object value, IComparer comparer)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.BinarySearch(value, comparer);
				}
				return result;
			}

			// Token: 0x0600160C RID: 5644 RVA: 0x000569B8 File Offset: 0x00054BB8
			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.BinarySearch(index, count, value, comparer);
				}
				return result;
			}

			// Token: 0x0600160D RID: 5645 RVA: 0x00056A14 File Offset: 0x00054C14
			public override object Clone()
			{
				object syncRoot = this.m_SyncRoot;
				object result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.Clone();
				}
				return result;
			}

			// Token: 0x0600160E RID: 5646 RVA: 0x00056A68 File Offset: 0x00054C68
			public override ArrayList GetRange(int index, int count)
			{
				object syncRoot = this.m_SyncRoot;
				ArrayList range;
				lock (syncRoot)
				{
					range = this.m_InnerArrayList.GetRange(index, count);
				}
				return range;
			}

			// Token: 0x0600160F RID: 5647 RVA: 0x00056AC0 File Offset: 0x00054CC0
			public override void TrimToSize()
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.TrimToSize();
				}
			}

			// Token: 0x06001610 RID: 5648 RVA: 0x00056B10 File Offset: 0x00054D10
			public override void Sort()
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.Sort();
				}
			}

			// Token: 0x06001611 RID: 5649 RVA: 0x00056B60 File Offset: 0x00054D60
			public override void Sort(IComparer comparer)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.Sort(comparer);
				}
			}

			// Token: 0x06001612 RID: 5650 RVA: 0x00056BB0 File Offset: 0x00054DB0
			public override void Sort(int index, int count, IComparer comparer)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerArrayList.Sort(index, count, comparer);
				}
			}

			// Token: 0x06001613 RID: 5651 RVA: 0x00056C00 File Offset: 0x00054E00
			public override object[] ToArray()
			{
				object syncRoot = this.m_SyncRoot;
				object[] result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.ToArray();
				}
				return result;
			}

			// Token: 0x06001614 RID: 5652 RVA: 0x00056C54 File Offset: 0x00054E54
			public override Array ToArray(Type elementType)
			{
				object syncRoot = this.m_SyncRoot;
				Array result;
				lock (syncRoot)
				{
					result = this.m_InnerArrayList.ToArray(elementType);
				}
				return result;
			}

			// Token: 0x04000861 RID: 2145
			private object m_SyncRoot;
		}

		// Token: 0x020001AA RID: 426
		[Serializable]
		private class FixedSizeArrayListWrapper : ArrayList.ArrayListWrapper
		{
			// Token: 0x06001615 RID: 5653 RVA: 0x00056CAC File Offset: 0x00054EAC
			public FixedSizeArrayListWrapper(ArrayList innerList) : base(innerList)
			{
			}

			// Token: 0x17000340 RID: 832
			// (get) Token: 0x06001616 RID: 5654 RVA: 0x00056CB8 File Offset: 0x00054EB8
			protected virtual string ErrorMessage
			{
				get
				{
					return "Can't add or remove from a fixed-size list.";
				}
			}

			// Token: 0x17000341 RID: 833
			// (get) Token: 0x06001617 RID: 5655 RVA: 0x00056CC0 File Offset: 0x00054EC0
			// (set) Token: 0x06001618 RID: 5656 RVA: 0x00056CC8 File Offset: 0x00054EC8
			public override int Capacity
			{
				get
				{
					return base.Capacity;
				}
				set
				{
					throw new NotSupportedException(this.ErrorMessage);
				}
			}

			// Token: 0x17000342 RID: 834
			// (get) Token: 0x06001619 RID: 5657 RVA: 0x00056CD8 File Offset: 0x00054ED8
			public override bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x0600161A RID: 5658 RVA: 0x00056CDC File Offset: 0x00054EDC
			public override int Add(object value)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600161B RID: 5659 RVA: 0x00056CEC File Offset: 0x00054EEC
			public override void AddRange(ICollection c)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600161C RID: 5660 RVA: 0x00056CFC File Offset: 0x00054EFC
			public override void Clear()
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600161D RID: 5661 RVA: 0x00056D0C File Offset: 0x00054F0C
			public override void Insert(int index, object value)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600161E RID: 5662 RVA: 0x00056D1C File Offset: 0x00054F1C
			public override void InsertRange(int index, ICollection c)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600161F RID: 5663 RVA: 0x00056D2C File Offset: 0x00054F2C
			public override void Remove(object value)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x06001620 RID: 5664 RVA: 0x00056D3C File Offset: 0x00054F3C
			public override void RemoveAt(int index)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x06001621 RID: 5665 RVA: 0x00056D4C File Offset: 0x00054F4C
			public override void RemoveRange(int index, int count)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x06001622 RID: 5666 RVA: 0x00056D5C File Offset: 0x00054F5C
			public override void TrimToSize()
			{
				throw new NotSupportedException(this.ErrorMessage);
			}
		}

		// Token: 0x020001AB RID: 427
		[Serializable]
		private sealed class ReadOnlyArrayListWrapper : ArrayList.FixedSizeArrayListWrapper
		{
			// Token: 0x06001623 RID: 5667 RVA: 0x00056D6C File Offset: 0x00054F6C
			public ReadOnlyArrayListWrapper(ArrayList innerArrayList) : base(innerArrayList)
			{
			}

			// Token: 0x17000343 RID: 835
			// (get) Token: 0x06001624 RID: 5668 RVA: 0x00056D78 File Offset: 0x00054F78
			protected override string ErrorMessage
			{
				get
				{
					return "Can't modify a readonly list.";
				}
			}

			// Token: 0x17000344 RID: 836
			// (get) Token: 0x06001625 RID: 5669 RVA: 0x00056D80 File Offset: 0x00054F80
			public override bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000345 RID: 837
			public override object this[int index]
			{
				get
				{
					return this.m_InnerArrayList[index];
				}
				set
				{
					throw new NotSupportedException(this.ErrorMessage);
				}
			}

			// Token: 0x06001628 RID: 5672 RVA: 0x00056DA4 File Offset: 0x00054FA4
			public override void Reverse()
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x06001629 RID: 5673 RVA: 0x00056DB4 File Offset: 0x00054FB4
			public override void Reverse(int index, int count)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600162A RID: 5674 RVA: 0x00056DC4 File Offset: 0x00054FC4
			public override void SetRange(int index, ICollection c)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600162B RID: 5675 RVA: 0x00056DD4 File Offset: 0x00054FD4
			public override void Sort()
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600162C RID: 5676 RVA: 0x00056DE4 File Offset: 0x00054FE4
			public override void Sort(IComparer comparer)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600162D RID: 5677 RVA: 0x00056DF4 File Offset: 0x00054FF4
			public override void Sort(int index, int count, IComparer comparer)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}
		}

		// Token: 0x020001AC RID: 428
		[Serializable]
		private sealed class RangedArrayList : ArrayList.ArrayListWrapper
		{
			// Token: 0x0600162E RID: 5678 RVA: 0x00056E04 File Offset: 0x00055004
			public RangedArrayList(ArrayList innerList, int index, int count) : base(innerList)
			{
				this.m_InnerIndex = index;
				this.m_InnerCount = count;
				this.m_InnerStateChanges = innerList._version;
			}

			// Token: 0x17000346 RID: 838
			// (get) Token: 0x0600162F RID: 5679 RVA: 0x00056E28 File Offset: 0x00055028
			public override bool IsSynchronized
			{
				get
				{
					return false;
				}
			}

			// Token: 0x17000347 RID: 839
			public override object this[int index]
			{
				get
				{
					if (index < 0 || index > this.m_InnerCount)
					{
						throw new ArgumentOutOfRangeException("index");
					}
					return this.m_InnerArrayList[this.m_InnerIndex + index];
				}
				set
				{
					if (index < 0 || index > this.m_InnerCount)
					{
						throw new ArgumentOutOfRangeException("index");
					}
					this.m_InnerArrayList[this.m_InnerIndex + index] = value;
				}
			}

			// Token: 0x17000348 RID: 840
			// (get) Token: 0x06001632 RID: 5682 RVA: 0x00056EA0 File Offset: 0x000550A0
			public override int Count
			{
				get
				{
					this.VerifyStateChanges();
					return this.m_InnerCount;
				}
			}

			// Token: 0x17000349 RID: 841
			// (get) Token: 0x06001633 RID: 5683 RVA: 0x00056EB0 File Offset: 0x000550B0
			// (set) Token: 0x06001634 RID: 5684 RVA: 0x00056EC0 File Offset: 0x000550C0
			public override int Capacity
			{
				get
				{
					return this.m_InnerArrayList.Capacity;
				}
				set
				{
					if (value < this.m_InnerCount)
					{
						throw new ArgumentOutOfRangeException();
					}
				}
			}

			// Token: 0x06001635 RID: 5685 RVA: 0x00056ED4 File Offset: 0x000550D4
			private void VerifyStateChanges()
			{
				if (this.m_InnerStateChanges != this.m_InnerArrayList._version)
				{
					throw new InvalidOperationException("ArrayList view is invalid because the underlying ArrayList was modified.");
				}
			}

			// Token: 0x06001636 RID: 5686 RVA: 0x00056EF8 File Offset: 0x000550F8
			public override int Add(object value)
			{
				this.VerifyStateChanges();
				this.m_InnerArrayList.Insert(this.m_InnerIndex + this.m_InnerCount, value);
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
				return ++this.m_InnerCount;
			}

			// Token: 0x06001637 RID: 5687 RVA: 0x00056F48 File Offset: 0x00055148
			public override void Clear()
			{
				this.VerifyStateChanges();
				this.m_InnerArrayList.RemoveRange(this.m_InnerIndex, this.m_InnerCount);
				this.m_InnerCount = 0;
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x06001638 RID: 5688 RVA: 0x00056F8C File Offset: 0x0005518C
			public override bool Contains(object value)
			{
				return this.m_InnerArrayList.Contains(value, this.m_InnerIndex, this.m_InnerCount);
			}

			// Token: 0x06001639 RID: 5689 RVA: 0x00056FA8 File Offset: 0x000551A8
			public override int IndexOf(object value)
			{
				return this.IndexOf(value, 0);
			}

			// Token: 0x0600163A RID: 5690 RVA: 0x00056FB4 File Offset: 0x000551B4
			public override int IndexOf(object value, int startIndex)
			{
				return this.IndexOf(value, startIndex, this.m_InnerCount - startIndex);
			}

			// Token: 0x0600163B RID: 5691 RVA: 0x00056FC8 File Offset: 0x000551C8
			public override int IndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0 || startIndex > this.m_InnerCount)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "Does not specify valid index.");
				}
				if (count < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("count", count, "Can't be less than 0.");
				}
				if (startIndex > this.m_InnerCount - count)
				{
					throw new ArgumentOutOfRangeException("count", "Start index and count do not specify a valid range.");
				}
				int num = this.m_InnerArrayList.IndexOf(value, this.m_InnerIndex + startIndex, count);
				if (num == -1)
				{
					return -1;
				}
				return num - this.m_InnerIndex;
			}

			// Token: 0x0600163C RID: 5692 RVA: 0x00057060 File Offset: 0x00055260
			public override int LastIndexOf(object value)
			{
				return this.LastIndexOf(value, this.m_InnerCount - 1);
			}

			// Token: 0x0600163D RID: 5693 RVA: 0x00057074 File Offset: 0x00055274
			public override int LastIndexOf(object value, int startIndex)
			{
				return this.LastIndexOf(value, startIndex, startIndex + 1);
			}

			// Token: 0x0600163E RID: 5694 RVA: 0x00057084 File Offset: 0x00055284
			public override int LastIndexOf(object value, int startIndex, int count)
			{
				if (startIndex < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("startIndex", startIndex, "< 0");
				}
				if (count < 0)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("count", count, "count is negative.");
				}
				int num = this.m_InnerArrayList.LastIndexOf(value, this.m_InnerIndex + startIndex, count);
				if (num == -1)
				{
					return -1;
				}
				return num - this.m_InnerIndex;
			}

			// Token: 0x0600163F RID: 5695 RVA: 0x000570F0 File Offset: 0x000552F0
			public override void Insert(int index, object value)
			{
				this.VerifyStateChanges();
				if (index < 0 || index > this.m_InnerCount)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				this.m_InnerArrayList.Insert(this.m_InnerIndex + index, value);
				this.m_InnerCount++;
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x06001640 RID: 5696 RVA: 0x00057160 File Offset: 0x00055360
			public override void InsertRange(int index, ICollection c)
			{
				this.VerifyStateChanges();
				if (index < 0 || index > this.m_InnerCount)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				this.m_InnerArrayList.InsertRange(this.m_InnerIndex + index, c);
				this.m_InnerCount += c.Count;
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x06001641 RID: 5697 RVA: 0x000571D4 File Offset: 0x000553D4
			public override void Remove(object value)
			{
				this.VerifyStateChanges();
				int num = this.IndexOf(value);
				if (num > -1)
				{
					this.RemoveAt(num);
				}
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x06001642 RID: 5698 RVA: 0x00057210 File Offset: 0x00055410
			public override void RemoveAt(int index)
			{
				this.VerifyStateChanges();
				if (index < 0 || index > this.m_InnerCount)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				this.m_InnerArrayList.RemoveAt(this.m_InnerIndex + index);
				this.m_InnerCount--;
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x06001643 RID: 5699 RVA: 0x00057280 File Offset: 0x00055480
			public override void RemoveRange(int index, int count)
			{
				this.VerifyStateChanges();
				ArrayList.CheckRange(index, count, this.m_InnerCount);
				this.m_InnerArrayList.RemoveRange(this.m_InnerIndex + index, count);
				this.m_InnerCount -= count;
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x06001644 RID: 5700 RVA: 0x000572D4 File Offset: 0x000554D4
			public override void Reverse()
			{
				this.Reverse(0, this.m_InnerCount);
			}

			// Token: 0x06001645 RID: 5701 RVA: 0x000572E4 File Offset: 0x000554E4
			public override void Reverse(int index, int count)
			{
				this.VerifyStateChanges();
				ArrayList.CheckRange(index, count, this.m_InnerCount);
				this.m_InnerArrayList.Reverse(this.m_InnerIndex + index, count);
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x06001646 RID: 5702 RVA: 0x0005732C File Offset: 0x0005552C
			public override void SetRange(int index, ICollection c)
			{
				this.VerifyStateChanges();
				if (index < 0 || index > this.m_InnerCount)
				{
					ArrayList.ThrowNewArgumentOutOfRangeException("index", index, "Index must be >= 0 and <= Count.");
				}
				this.m_InnerArrayList.SetRange(this.m_InnerIndex + index, c);
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x06001647 RID: 5703 RVA: 0x0005738C File Offset: 0x0005558C
			public override void CopyTo(Array array)
			{
				this.CopyTo(array, 0);
			}

			// Token: 0x06001648 RID: 5704 RVA: 0x00057398 File Offset: 0x00055598
			public override void CopyTo(Array array, int index)
			{
				this.CopyTo(0, array, index, this.m_InnerCount);
			}

			// Token: 0x06001649 RID: 5705 RVA: 0x000573AC File Offset: 0x000555AC
			public override void CopyTo(int index, Array array, int arrayIndex, int count)
			{
				ArrayList.CheckRange(index, count, this.m_InnerCount);
				this.m_InnerArrayList.CopyTo(this.m_InnerIndex + index, array, arrayIndex, count);
			}

			// Token: 0x0600164A RID: 5706 RVA: 0x000573D4 File Offset: 0x000555D4
			public override IEnumerator GetEnumerator()
			{
				return this.GetEnumerator(0, this.m_InnerCount);
			}

			// Token: 0x0600164B RID: 5707 RVA: 0x000573E4 File Offset: 0x000555E4
			public override IEnumerator GetEnumerator(int index, int count)
			{
				ArrayList.CheckRange(index, count, this.m_InnerCount);
				return this.m_InnerArrayList.GetEnumerator(this.m_InnerIndex + index, count);
			}

			// Token: 0x0600164C RID: 5708 RVA: 0x00057408 File Offset: 0x00055608
			public override void AddRange(ICollection c)
			{
				this.VerifyStateChanges();
				this.m_InnerArrayList.InsertRange(this.m_InnerCount, c);
				this.m_InnerCount += c.Count;
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x0600164D RID: 5709 RVA: 0x00057454 File Offset: 0x00055654
			public override int BinarySearch(object value)
			{
				return this.BinarySearch(0, this.m_InnerCount, value, Comparer.Default);
			}

			// Token: 0x0600164E RID: 5710 RVA: 0x0005746C File Offset: 0x0005566C
			public override int BinarySearch(object value, IComparer comparer)
			{
				return this.BinarySearch(0, this.m_InnerCount, value, comparer);
			}

			// Token: 0x0600164F RID: 5711 RVA: 0x00057480 File Offset: 0x00055680
			public override int BinarySearch(int index, int count, object value, IComparer comparer)
			{
				ArrayList.CheckRange(index, count, this.m_InnerCount);
				return this.m_InnerArrayList.BinarySearch(this.m_InnerIndex + index, count, value, comparer);
			}

			// Token: 0x06001650 RID: 5712 RVA: 0x000574B4 File Offset: 0x000556B4
			public override object Clone()
			{
				return new ArrayList.RangedArrayList((ArrayList)this.m_InnerArrayList.Clone(), this.m_InnerIndex, this.m_InnerCount);
			}

			// Token: 0x06001651 RID: 5713 RVA: 0x000574D8 File Offset: 0x000556D8
			public override ArrayList GetRange(int index, int count)
			{
				ArrayList.CheckRange(index, count, this.m_InnerCount);
				return new ArrayList.RangedArrayList(this, index, count);
			}

			// Token: 0x06001652 RID: 5714 RVA: 0x000574F0 File Offset: 0x000556F0
			public override void TrimToSize()
			{
				throw new NotSupportedException();
			}

			// Token: 0x06001653 RID: 5715 RVA: 0x000574F8 File Offset: 0x000556F8
			public override void Sort()
			{
				this.Sort(Comparer.Default);
			}

			// Token: 0x06001654 RID: 5716 RVA: 0x00057508 File Offset: 0x00055708
			public override void Sort(IComparer comparer)
			{
				this.Sort(0, this.m_InnerCount, comparer);
			}

			// Token: 0x06001655 RID: 5717 RVA: 0x00057518 File Offset: 0x00055718
			public override void Sort(int index, int count, IComparer comparer)
			{
				this.VerifyStateChanges();
				ArrayList.CheckRange(index, count, this.m_InnerCount);
				this.m_InnerArrayList.Sort(this.m_InnerIndex + index, count, comparer);
				this.m_InnerStateChanges = this.m_InnerArrayList._version;
			}

			// Token: 0x06001656 RID: 5718 RVA: 0x00057560 File Offset: 0x00055760
			public override object[] ToArray()
			{
				object[] array = new object[this.m_InnerCount];
				this.m_InnerArrayList.CopyTo(this.m_InnerIndex, array, 0, this.m_InnerCount);
				return array;
			}

			// Token: 0x06001657 RID: 5719 RVA: 0x00057594 File Offset: 0x00055794
			public override Array ToArray(Type elementType)
			{
				Array array = Array.CreateInstance(elementType, this.m_InnerCount);
				this.m_InnerArrayList.CopyTo(this.m_InnerIndex, array, 0, this.m_InnerCount);
				return array;
			}

			// Token: 0x04000862 RID: 2146
			private int m_InnerIndex;

			// Token: 0x04000863 RID: 2147
			private int m_InnerCount;

			// Token: 0x04000864 RID: 2148
			private int m_InnerStateChanges;
		}

		// Token: 0x020001AD RID: 429
		[Serializable]
		private sealed class SynchronizedListWrapper : ArrayList.ListWrapper
		{
			// Token: 0x06001658 RID: 5720 RVA: 0x000575C8 File Offset: 0x000557C8
			public SynchronizedListWrapper(IList innerList) : base(innerList)
			{
				this.m_SyncRoot = innerList.SyncRoot;
			}

			// Token: 0x1700034A RID: 842
			// (get) Token: 0x06001659 RID: 5721 RVA: 0x000575E0 File Offset: 0x000557E0
			public override int Count
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					int count;
					lock (syncRoot)
					{
						count = this.m_InnerList.Count;
					}
					return count;
				}
			}

			// Token: 0x1700034B RID: 843
			// (get) Token: 0x0600165A RID: 5722 RVA: 0x00057634 File Offset: 0x00055834
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x1700034C RID: 844
			// (get) Token: 0x0600165B RID: 5723 RVA: 0x00057638 File Offset: 0x00055838
			public override object SyncRoot
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					object syncRoot2;
					lock (syncRoot)
					{
						syncRoot2 = this.m_InnerList.SyncRoot;
					}
					return syncRoot2;
				}
			}

			// Token: 0x1700034D RID: 845
			// (get) Token: 0x0600165C RID: 5724 RVA: 0x0005768C File Offset: 0x0005588C
			public override bool IsFixedSize
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					bool isFixedSize;
					lock (syncRoot)
					{
						isFixedSize = this.m_InnerList.IsFixedSize;
					}
					return isFixedSize;
				}
			}

			// Token: 0x1700034E RID: 846
			// (get) Token: 0x0600165D RID: 5725 RVA: 0x000576E0 File Offset: 0x000558E0
			public override bool IsReadOnly
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					bool isReadOnly;
					lock (syncRoot)
					{
						isReadOnly = this.m_InnerList.IsReadOnly;
					}
					return isReadOnly;
				}
			}

			// Token: 0x1700034F RID: 847
			public override object this[int index]
			{
				get
				{
					object syncRoot = this.m_SyncRoot;
					object result;
					lock (syncRoot)
					{
						result = this.m_InnerList[index];
					}
					return result;
				}
				set
				{
					object syncRoot = this.m_SyncRoot;
					lock (syncRoot)
					{
						this.m_InnerList[index] = value;
					}
				}
			}

			// Token: 0x06001660 RID: 5728 RVA: 0x000577DC File Offset: 0x000559DC
			public override int Add(object value)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerList.Add(value);
				}
				return result;
			}

			// Token: 0x06001661 RID: 5729 RVA: 0x00057834 File Offset: 0x00055A34
			public override void Clear()
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerList.Clear();
				}
			}

			// Token: 0x06001662 RID: 5730 RVA: 0x00057884 File Offset: 0x00055A84
			public override bool Contains(object value)
			{
				object syncRoot = this.m_SyncRoot;
				bool result;
				lock (syncRoot)
				{
					result = this.m_InnerList.Contains(value);
				}
				return result;
			}

			// Token: 0x06001663 RID: 5731 RVA: 0x000578DC File Offset: 0x00055ADC
			public override int IndexOf(object value)
			{
				object syncRoot = this.m_SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.m_InnerList.IndexOf(value);
				}
				return result;
			}

			// Token: 0x06001664 RID: 5732 RVA: 0x00057934 File Offset: 0x00055B34
			public override void Insert(int index, object value)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerList.Insert(index, value);
				}
			}

			// Token: 0x06001665 RID: 5733 RVA: 0x00057984 File Offset: 0x00055B84
			public override void Remove(object value)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerList.Remove(value);
				}
			}

			// Token: 0x06001666 RID: 5734 RVA: 0x000579D4 File Offset: 0x00055BD4
			public override void RemoveAt(int index)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerList.RemoveAt(index);
				}
			}

			// Token: 0x06001667 RID: 5735 RVA: 0x00057A24 File Offset: 0x00055C24
			public override void CopyTo(Array array, int index)
			{
				object syncRoot = this.m_SyncRoot;
				lock (syncRoot)
				{
					this.m_InnerList.CopyTo(array, index);
				}
			}

			// Token: 0x06001668 RID: 5736 RVA: 0x00057A74 File Offset: 0x00055C74
			public override IEnumerator GetEnumerator()
			{
				object syncRoot = this.m_SyncRoot;
				IEnumerator enumerator;
				lock (syncRoot)
				{
					enumerator = this.m_InnerList.GetEnumerator();
				}
				return enumerator;
			}

			// Token: 0x04000865 RID: 2149
			private object m_SyncRoot;
		}

		// Token: 0x020001AE RID: 430
		[Serializable]
		private class FixedSizeListWrapper : ArrayList.ListWrapper
		{
			// Token: 0x06001669 RID: 5737 RVA: 0x00057AC8 File Offset: 0x00055CC8
			public FixedSizeListWrapper(IList innerList) : base(innerList)
			{
			}

			// Token: 0x17000350 RID: 848
			// (get) Token: 0x0600166A RID: 5738 RVA: 0x00057AD4 File Offset: 0x00055CD4
			protected virtual string ErrorMessage
			{
				get
				{
					return "List is fixed-size.";
				}
			}

			// Token: 0x17000351 RID: 849
			// (get) Token: 0x0600166B RID: 5739 RVA: 0x00057ADC File Offset: 0x00055CDC
			public override bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x0600166C RID: 5740 RVA: 0x00057AE0 File Offset: 0x00055CE0
			public override int Add(object value)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600166D RID: 5741 RVA: 0x00057AF0 File Offset: 0x00055CF0
			public override void Clear()
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600166E RID: 5742 RVA: 0x00057B00 File Offset: 0x00055D00
			public override void Insert(int index, object value)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x0600166F RID: 5743 RVA: 0x00057B10 File Offset: 0x00055D10
			public override void Remove(object value)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}

			// Token: 0x06001670 RID: 5744 RVA: 0x00057B20 File Offset: 0x00055D20
			public override void RemoveAt(int index)
			{
				throw new NotSupportedException(this.ErrorMessage);
			}
		}

		// Token: 0x020001AF RID: 431
		[Serializable]
		private sealed class ReadOnlyListWrapper : ArrayList.FixedSizeListWrapper
		{
			// Token: 0x06001671 RID: 5745 RVA: 0x00057B30 File Offset: 0x00055D30
			public ReadOnlyListWrapper(IList innerList) : base(innerList)
			{
			}

			// Token: 0x17000352 RID: 850
			// (get) Token: 0x06001672 RID: 5746 RVA: 0x00057B3C File Offset: 0x00055D3C
			protected override string ErrorMessage
			{
				get
				{
					return "List is read-only.";
				}
			}

			// Token: 0x17000353 RID: 851
			// (get) Token: 0x06001673 RID: 5747 RVA: 0x00057B44 File Offset: 0x00055D44
			public override bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000354 RID: 852
			public override object this[int index]
			{
				get
				{
					return this.m_InnerList[index];
				}
				set
				{
					throw new NotSupportedException(this.ErrorMessage);
				}
			}
		}

		// Token: 0x020001B0 RID: 432
		[Serializable]
		private class ListWrapper : IEnumerable, ICollection, IList
		{
			// Token: 0x06001676 RID: 5750 RVA: 0x00057B68 File Offset: 0x00055D68
			public ListWrapper(IList innerList)
			{
				this.m_InnerList = innerList;
			}

			// Token: 0x17000355 RID: 853
			public virtual object this[int index]
			{
				get
				{
					return this.m_InnerList[index];
				}
				set
				{
					this.m_InnerList[index] = value;
				}
			}

			// Token: 0x17000356 RID: 854
			// (get) Token: 0x06001679 RID: 5753 RVA: 0x00057B98 File Offset: 0x00055D98
			public virtual int Count
			{
				get
				{
					return this.m_InnerList.Count;
				}
			}

			// Token: 0x17000357 RID: 855
			// (get) Token: 0x0600167A RID: 5754 RVA: 0x00057BA8 File Offset: 0x00055DA8
			public virtual bool IsSynchronized
			{
				get
				{
					return this.m_InnerList.IsSynchronized;
				}
			}

			// Token: 0x17000358 RID: 856
			// (get) Token: 0x0600167B RID: 5755 RVA: 0x00057BB8 File Offset: 0x00055DB8
			public virtual object SyncRoot
			{
				get
				{
					return this.m_InnerList.SyncRoot;
				}
			}

			// Token: 0x17000359 RID: 857
			// (get) Token: 0x0600167C RID: 5756 RVA: 0x00057BC8 File Offset: 0x00055DC8
			public virtual bool IsFixedSize
			{
				get
				{
					return this.m_InnerList.IsFixedSize;
				}
			}

			// Token: 0x1700035A RID: 858
			// (get) Token: 0x0600167D RID: 5757 RVA: 0x00057BD8 File Offset: 0x00055DD8
			public virtual bool IsReadOnly
			{
				get
				{
					return this.m_InnerList.IsReadOnly;
				}
			}

			// Token: 0x0600167E RID: 5758 RVA: 0x00057BE8 File Offset: 0x00055DE8
			public virtual int Add(object value)
			{
				return this.m_InnerList.Add(value);
			}

			// Token: 0x0600167F RID: 5759 RVA: 0x00057BF8 File Offset: 0x00055DF8
			public virtual void Clear()
			{
				this.m_InnerList.Clear();
			}

			// Token: 0x06001680 RID: 5760 RVA: 0x00057C08 File Offset: 0x00055E08
			public virtual bool Contains(object value)
			{
				return this.m_InnerList.Contains(value);
			}

			// Token: 0x06001681 RID: 5761 RVA: 0x00057C18 File Offset: 0x00055E18
			public virtual int IndexOf(object value)
			{
				return this.m_InnerList.IndexOf(value);
			}

			// Token: 0x06001682 RID: 5762 RVA: 0x00057C28 File Offset: 0x00055E28
			public virtual void Insert(int index, object value)
			{
				this.m_InnerList.Insert(index, value);
			}

			// Token: 0x06001683 RID: 5763 RVA: 0x00057C38 File Offset: 0x00055E38
			public virtual void Remove(object value)
			{
				this.m_InnerList.Remove(value);
			}

			// Token: 0x06001684 RID: 5764 RVA: 0x00057C48 File Offset: 0x00055E48
			public virtual void RemoveAt(int index)
			{
				this.m_InnerList.RemoveAt(index);
			}

			// Token: 0x06001685 RID: 5765 RVA: 0x00057C58 File Offset: 0x00055E58
			public virtual void CopyTo(Array array, int index)
			{
				this.m_InnerList.CopyTo(array, index);
			}

			// Token: 0x06001686 RID: 5766 RVA: 0x00057C68 File Offset: 0x00055E68
			public virtual IEnumerator GetEnumerator()
			{
				return this.m_InnerList.GetEnumerator();
			}

			// Token: 0x04000866 RID: 2150
			protected IList m_InnerList;
		}
	}
}
