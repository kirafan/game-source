﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001B8 RID: 440
	[ComVisible(true)]
	[Serializable]
	public abstract class DictionaryBase : IEnumerable, ICollection, IDictionary
	{
		// Token: 0x060016DE RID: 5854 RVA: 0x00058D88 File Offset: 0x00056F88
		protected DictionaryBase()
		{
			this.hashtable = new Hashtable();
		}

		// Token: 0x17000370 RID: 880
		// (get) Token: 0x060016DF RID: 5855 RVA: 0x00058D9C File Offset: 0x00056F9C
		bool IDictionary.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x060016E0 RID: 5856 RVA: 0x00058DA0 File Offset: 0x00056FA0
		bool IDictionary.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000372 RID: 882
		object IDictionary.this[object key]
		{
			get
			{
				object obj = this.hashtable[key];
				this.OnGet(key, obj);
				return obj;
			}
			set
			{
				this.OnValidate(key, value);
				object obj = this.hashtable[key];
				this.OnSet(key, obj, value);
				this.hashtable[key] = value;
				try
				{
					this.OnSetComplete(key, obj, value);
				}
				catch
				{
					this.hashtable[key] = obj;
					throw;
				}
			}
		}

		// Token: 0x17000373 RID: 883
		// (get) Token: 0x060016E3 RID: 5859 RVA: 0x00058E40 File Offset: 0x00057040
		ICollection IDictionary.Keys
		{
			get
			{
				return this.hashtable.Keys;
			}
		}

		// Token: 0x17000374 RID: 884
		// (get) Token: 0x060016E4 RID: 5860 RVA: 0x00058E50 File Offset: 0x00057050
		ICollection IDictionary.Values
		{
			get
			{
				return this.hashtable.Values;
			}
		}

		// Token: 0x060016E5 RID: 5861 RVA: 0x00058E60 File Offset: 0x00057060
		void IDictionary.Add(object key, object value)
		{
			this.OnValidate(key, value);
			this.OnInsert(key, value);
			this.hashtable.Add(key, value);
			try
			{
				this.OnInsertComplete(key, value);
			}
			catch
			{
				this.hashtable.Remove(key);
				throw;
			}
		}

		// Token: 0x060016E6 RID: 5862 RVA: 0x00058EC8 File Offset: 0x000570C8
		void IDictionary.Remove(object key)
		{
			if (!this.hashtable.Contains(key))
			{
				return;
			}
			object value = this.hashtable[key];
			this.OnValidate(key, value);
			this.OnRemove(key, value);
			this.hashtable.Remove(key);
			try
			{
				this.OnRemoveComplete(key, value);
			}
			catch
			{
				this.hashtable[key] = value;
				throw;
			}
		}

		// Token: 0x060016E7 RID: 5863 RVA: 0x00058F50 File Offset: 0x00057150
		bool IDictionary.Contains(object key)
		{
			return this.hashtable.Contains(key);
		}

		// Token: 0x17000375 RID: 885
		// (get) Token: 0x060016E8 RID: 5864 RVA: 0x00058F60 File Offset: 0x00057160
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.hashtable.IsSynchronized;
			}
		}

		// Token: 0x17000376 RID: 886
		// (get) Token: 0x060016E9 RID: 5865 RVA: 0x00058F70 File Offset: 0x00057170
		object ICollection.SyncRoot
		{
			get
			{
				return this.hashtable.SyncRoot;
			}
		}

		// Token: 0x060016EA RID: 5866 RVA: 0x00058F80 File Offset: 0x00057180
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.hashtable.GetEnumerator();
		}

		// Token: 0x060016EB RID: 5867 RVA: 0x00058F90 File Offset: 0x00057190
		public void Clear()
		{
			this.OnClear();
			this.hashtable.Clear();
			this.OnClearComplete();
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x060016EC RID: 5868 RVA: 0x00058FB4 File Offset: 0x000571B4
		public int Count
		{
			get
			{
				return this.hashtable.Count;
			}
		}

		// Token: 0x17000378 RID: 888
		// (get) Token: 0x060016ED RID: 5869 RVA: 0x00058FC4 File Offset: 0x000571C4
		protected IDictionary Dictionary
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000379 RID: 889
		// (get) Token: 0x060016EE RID: 5870 RVA: 0x00058FC8 File Offset: 0x000571C8
		protected Hashtable InnerHashtable
		{
			get
			{
				return this.hashtable;
			}
		}

		// Token: 0x060016EF RID: 5871 RVA: 0x00058FD0 File Offset: 0x000571D0
		public void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index must be possitive");
			}
			if (array.Rank > 1)
			{
				throw new ArgumentException("array is multidimensional");
			}
			int length = array.Length;
			if (index > length)
			{
				throw new ArgumentException("index is larger than array size");
			}
			if (index + this.Count > length)
			{
				throw new ArgumentException("Copy will overlflow array");
			}
			this.DoCopy(array, index);
		}

		// Token: 0x060016F0 RID: 5872 RVA: 0x00059054 File Offset: 0x00057254
		private void DoCopy(Array array, int index)
		{
			foreach (object obj in this.hashtable)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				array.SetValue(dictionaryEntry, index++);
			}
		}

		// Token: 0x060016F1 RID: 5873 RVA: 0x000590D0 File Offset: 0x000572D0
		public IDictionaryEnumerator GetEnumerator()
		{
			return this.hashtable.GetEnumerator();
		}

		// Token: 0x060016F2 RID: 5874 RVA: 0x000590E0 File Offset: 0x000572E0
		protected virtual void OnClear()
		{
		}

		// Token: 0x060016F3 RID: 5875 RVA: 0x000590E4 File Offset: 0x000572E4
		protected virtual void OnClearComplete()
		{
		}

		// Token: 0x060016F4 RID: 5876 RVA: 0x000590E8 File Offset: 0x000572E8
		protected virtual object OnGet(object key, object currentValue)
		{
			return currentValue;
		}

		// Token: 0x060016F5 RID: 5877 RVA: 0x000590EC File Offset: 0x000572EC
		protected virtual void OnInsert(object key, object value)
		{
		}

		// Token: 0x060016F6 RID: 5878 RVA: 0x000590F0 File Offset: 0x000572F0
		protected virtual void OnInsertComplete(object key, object value)
		{
		}

		// Token: 0x060016F7 RID: 5879 RVA: 0x000590F4 File Offset: 0x000572F4
		protected virtual void OnSet(object key, object oldValue, object newValue)
		{
		}

		// Token: 0x060016F8 RID: 5880 RVA: 0x000590F8 File Offset: 0x000572F8
		protected virtual void OnSetComplete(object key, object oldValue, object newValue)
		{
		}

		// Token: 0x060016F9 RID: 5881 RVA: 0x000590FC File Offset: 0x000572FC
		protected virtual void OnRemove(object key, object value)
		{
		}

		// Token: 0x060016FA RID: 5882 RVA: 0x00059100 File Offset: 0x00057300
		protected virtual void OnRemoveComplete(object key, object value)
		{
		}

		// Token: 0x060016FB RID: 5883 RVA: 0x00059104 File Offset: 0x00057304
		protected virtual void OnValidate(object key, object value)
		{
		}

		// Token: 0x0400087A RID: 2170
		private Hashtable hashtable;
	}
}
