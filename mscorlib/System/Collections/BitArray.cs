﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001B1 RID: 433
	[ComVisible(true)]
	[Serializable]
	public sealed class BitArray : IEnumerable, ICloneable, ICollection
	{
		// Token: 0x06001687 RID: 5767 RVA: 0x00057C78 File Offset: 0x00055E78
		public BitArray(BitArray bits)
		{
			if (bits == null)
			{
				throw new ArgumentNullException("bits");
			}
			this.m_length = bits.m_length;
			this.m_array = new int[(this.m_length + 31) / 32];
			if (this.m_array.Length == 1)
			{
				this.m_array[0] = bits.m_array[0];
			}
			else
			{
				Array.Copy(bits.m_array, this.m_array, this.m_array.Length);
			}
		}

		// Token: 0x06001688 RID: 5768 RVA: 0x00057CFC File Offset: 0x00055EFC
		public BitArray(bool[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			this.m_length = values.Length;
			this.m_array = new int[(this.m_length + 31) / 32];
			for (int i = 0; i < values.Length; i++)
			{
				this[i] = values[i];
			}
		}

		// Token: 0x06001689 RID: 5769 RVA: 0x00057D60 File Offset: 0x00055F60
		public BitArray(byte[] bytes)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			this.m_length = bytes.Length * 8;
			this.m_array = new int[(this.m_length + 31) / 32];
			for (int i = 0; i < bytes.Length; i++)
			{
				this.setByte(i, bytes[i]);
			}
		}

		// Token: 0x0600168A RID: 5770 RVA: 0x00057DC4 File Offset: 0x00055FC4
		public BitArray(int[] values)
		{
			if (values == null)
			{
				throw new ArgumentNullException("values");
			}
			int num = values.Length;
			this.m_length = num * 32;
			this.m_array = new int[num];
			Array.Copy(values, this.m_array, num);
		}

		// Token: 0x0600168B RID: 5771 RVA: 0x00057E10 File Offset: 0x00056010
		public BitArray(int length)
		{
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length");
			}
			this.m_length = length;
			this.m_array = new int[(this.m_length + 31) / 32];
		}

		// Token: 0x0600168C RID: 5772 RVA: 0x00057E54 File Offset: 0x00056054
		public BitArray(int length, bool defaultValue) : this(length)
		{
			if (defaultValue)
			{
				for (int i = 0; i < this.m_array.Length; i++)
				{
					this.m_array[i] = -1;
				}
			}
		}

		// Token: 0x0600168D RID: 5773 RVA: 0x00057E90 File Offset: 0x00056090
		private BitArray(int[] array, int length)
		{
			this.m_array = array;
			this.m_length = length;
		}

		// Token: 0x0600168E RID: 5774 RVA: 0x00057EA8 File Offset: 0x000560A8
		private byte getByte(int byteIndex)
		{
			int num = byteIndex / 4;
			int num2 = byteIndex % 4 * 8;
			int num3 = this.m_array[num] & 255 << num2;
			return (byte)(num3 >> num2 & 255);
		}

		// Token: 0x0600168F RID: 5775 RVA: 0x00057EE0 File Offset: 0x000560E0
		private void setByte(int byteIndex, byte value)
		{
			int num = byteIndex / 4;
			int num2 = byteIndex % 4 * 8;
			this.m_array[num] &= ~(255 << num2);
			this.m_array[num] |= (int)value << num2;
			this._version++;
		}

		// Token: 0x06001690 RID: 5776 RVA: 0x00057F3C File Offset: 0x0005613C
		private void checkOperand(BitArray operand)
		{
			if (operand == null)
			{
				throw new ArgumentNullException();
			}
			if (operand.m_length != this.m_length)
			{
				throw new ArgumentException();
			}
		}

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x06001691 RID: 5777 RVA: 0x00057F64 File Offset: 0x00056164
		public int Count
		{
			get
			{
				return this.m_length;
			}
		}

		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06001692 RID: 5778 RVA: 0x00057F6C File Offset: 0x0005616C
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06001693 RID: 5779 RVA: 0x00057F70 File Offset: 0x00056170
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700035E RID: 862
		public bool this[int index]
		{
			get
			{
				return this.Get(index);
			}
			set
			{
				this.Set(index, value);
			}
		}

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06001696 RID: 5782 RVA: 0x00057F8C File Offset: 0x0005618C
		// (set) Token: 0x06001697 RID: 5783 RVA: 0x00057F94 File Offset: 0x00056194
		public int Length
		{
			get
			{
				return this.m_length;
			}
			set
			{
				if (this.m_length == value)
				{
					return;
				}
				if (value < 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				if (value > this.m_length)
				{
					int num = (value + 31) / 32;
					int num2 = (this.m_length + 31) / 32;
					if (num > this.m_array.Length)
					{
						int[] array = new int[num];
						Array.Copy(this.m_array, array, this.m_array.Length);
						this.m_array = array;
					}
					else
					{
						Array.Clear(this.m_array, num2, num - num2);
					}
					int num3 = this.m_length % 32;
					if (num3 > 0)
					{
						this.m_array[num2 - 1] &= (1 << num3) - 1;
					}
				}
				this.m_length = value;
				this._version++;
			}
		}

		// Token: 0x17000360 RID: 864
		// (get) Token: 0x06001698 RID: 5784 RVA: 0x00058064 File Offset: 0x00056264
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06001699 RID: 5785 RVA: 0x00058068 File Offset: 0x00056268
		public object Clone()
		{
			return new BitArray(this);
		}

		// Token: 0x0600169A RID: 5786 RVA: 0x00058070 File Offset: 0x00056270
		public void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (array.Rank != 1)
			{
				throw new ArgumentException("array", "Array rank must be 1");
			}
			if (index >= array.Length && this.m_length > 0)
			{
				throw new ArgumentException("index", "index is greater than array.Length");
			}
			if (array is bool[])
			{
				if (array.Length - index < this.m_length)
				{
					throw new ArgumentException();
				}
				bool[] array2 = (bool[])array;
				for (int i = 0; i < this.m_length; i++)
				{
					array2[index + i] = this[i];
				}
			}
			else if (array is byte[])
			{
				int num = (this.m_length + 7) / 8;
				if (array.Length - index < num)
				{
					throw new ArgumentException();
				}
				byte[] array3 = (byte[])array;
				for (int j = 0; j < num; j++)
				{
					array3[index + j] = this.getByte(j);
				}
			}
			else
			{
				if (!(array is int[]))
				{
					throw new ArgumentException("array", "Unsupported type");
				}
				Array.Copy(this.m_array, 0, array, index, (this.m_length + 31) / 32);
			}
		}

		// Token: 0x0600169B RID: 5787 RVA: 0x000581CC File Offset: 0x000563CC
		public BitArray Not()
		{
			int num = (this.m_length + 31) / 32;
			for (int i = 0; i < num; i++)
			{
				this.m_array[i] = ~this.m_array[i];
			}
			this._version++;
			return this;
		}

		// Token: 0x0600169C RID: 5788 RVA: 0x00058218 File Offset: 0x00056418
		public BitArray And(BitArray value)
		{
			this.checkOperand(value);
			int num = (this.m_length + 31) / 32;
			for (int i = 0; i < num; i++)
			{
				this.m_array[i] &= value.m_array[i];
			}
			this._version++;
			return this;
		}

		// Token: 0x0600169D RID: 5789 RVA: 0x00058274 File Offset: 0x00056474
		public BitArray Or(BitArray value)
		{
			this.checkOperand(value);
			int num = (this.m_length + 31) / 32;
			for (int i = 0; i < num; i++)
			{
				this.m_array[i] |= value.m_array[i];
			}
			this._version++;
			return this;
		}

		// Token: 0x0600169E RID: 5790 RVA: 0x000582D0 File Offset: 0x000564D0
		public BitArray Xor(BitArray value)
		{
			this.checkOperand(value);
			int num = (this.m_length + 31) / 32;
			for (int i = 0; i < num; i++)
			{
				this.m_array[i] ^= value.m_array[i];
			}
			this._version++;
			return this;
		}

		// Token: 0x0600169F RID: 5791 RVA: 0x0005832C File Offset: 0x0005652C
		public bool Get(int index)
		{
			if (index < 0 || index >= this.m_length)
			{
				throw new ArgumentOutOfRangeException();
			}
			return (this.m_array[index >> 5] & 1 << index) != 0;
		}

		// Token: 0x060016A0 RID: 5792 RVA: 0x00058364 File Offset: 0x00056564
		public void Set(int index, bool value)
		{
			if (index < 0 || index >= this.m_length)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (value)
			{
				this.m_array[index >> 5] |= 1 << index;
			}
			else
			{
				this.m_array[index >> 5] &= ~(1 << index);
			}
			this._version++;
		}

		// Token: 0x060016A1 RID: 5793 RVA: 0x000583E0 File Offset: 0x000565E0
		public void SetAll(bool value)
		{
			if (value)
			{
				for (int i = 0; i < this.m_array.Length; i++)
				{
					this.m_array[i] = -1;
				}
			}
			else
			{
				Array.Clear(this.m_array, 0, this.m_array.Length);
			}
			this._version++;
		}

		// Token: 0x060016A2 RID: 5794 RVA: 0x0005843C File Offset: 0x0005663C
		public IEnumerator GetEnumerator()
		{
			return new BitArray.BitArrayEnumerator(this);
		}

		// Token: 0x04000867 RID: 2151
		private int[] m_array;

		// Token: 0x04000868 RID: 2152
		private int m_length;

		// Token: 0x04000869 RID: 2153
		private int _version;

		// Token: 0x020001B2 RID: 434
		[Serializable]
		private class BitArrayEnumerator : IEnumerator, ICloneable
		{
			// Token: 0x060016A3 RID: 5795 RVA: 0x00058444 File Offset: 0x00056644
			public BitArrayEnumerator(BitArray ba)
			{
				this._index = -1;
				this._bitArray = ba;
				this._version = ba._version;
			}

			// Token: 0x060016A4 RID: 5796 RVA: 0x00058474 File Offset: 0x00056674
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x17000361 RID: 865
			// (get) Token: 0x060016A5 RID: 5797 RVA: 0x0005847C File Offset: 0x0005667C
			public object Current
			{
				get
				{
					if (this._index == -1)
					{
						throw new InvalidOperationException("Enum not started");
					}
					if (this._index >= this._bitArray.Count)
					{
						throw new InvalidOperationException("Enum Ended");
					}
					return this._current;
				}
			}

			// Token: 0x060016A6 RID: 5798 RVA: 0x000584CC File Offset: 0x000566CC
			public bool MoveNext()
			{
				this.checkVersion();
				if (this._index < this._bitArray.Count - 1)
				{
					this._current = this._bitArray[++this._index];
					return true;
				}
				this._index = this._bitArray.Count;
				return false;
			}

			// Token: 0x060016A7 RID: 5799 RVA: 0x00058530 File Offset: 0x00056730
			public void Reset()
			{
				this.checkVersion();
				this._index = -1;
			}

			// Token: 0x060016A8 RID: 5800 RVA: 0x00058540 File Offset: 0x00056740
			private void checkVersion()
			{
				if (this._version != this._bitArray._version)
				{
					throw new InvalidOperationException();
				}
			}

			// Token: 0x0400086A RID: 2154
			private BitArray _bitArray;

			// Token: 0x0400086B RID: 2155
			private bool _current;

			// Token: 0x0400086C RID: 2156
			private int _index;

			// Token: 0x0400086D RID: 2157
			private int _version;
		}
	}
}
