﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001D2 RID: 466
	[ComVisible(true)]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView))]
	[DebuggerDisplay("Count={Count}")]
	[Serializable]
	public class Stack : IEnumerable, ICloneable, ICollection
	{
		// Token: 0x0600181F RID: 6175 RVA: 0x0005CB5C File Offset: 0x0005AD5C
		public Stack()
		{
			this.contents = new object[16];
			this.capacity = 16;
		}

		// Token: 0x06001820 RID: 6176 RVA: 0x0005CB8C File Offset: 0x0005AD8C
		public Stack(ICollection col) : this((col != null) ? col.Count : 16)
		{
			if (col == null)
			{
				throw new ArgumentNullException("col");
			}
			foreach (object obj in col)
			{
				this.Push(obj);
			}
		}

		// Token: 0x06001821 RID: 6177 RVA: 0x0005CC1C File Offset: 0x0005AE1C
		public Stack(int initialCapacity)
		{
			if (initialCapacity < 0)
			{
				throw new ArgumentOutOfRangeException("initialCapacity");
			}
			this.capacity = initialCapacity;
			this.contents = new object[this.capacity];
		}

		// Token: 0x06001822 RID: 6178 RVA: 0x0005CC58 File Offset: 0x0005AE58
		private void Resize(int ncapacity)
		{
			ncapacity = Math.Max(ncapacity, 16);
			object[] destinationArray = new object[ncapacity];
			Array.Copy(this.contents, destinationArray, this.count);
			this.capacity = ncapacity;
			this.contents = destinationArray;
		}

		// Token: 0x06001823 RID: 6179 RVA: 0x0005CC98 File Offset: 0x0005AE98
		public static Stack Synchronized(Stack stack)
		{
			if (stack == null)
			{
				throw new ArgumentNullException("stack");
			}
			return new Stack.SyncStack(stack);
		}

		// Token: 0x170003CE RID: 974
		// (get) Token: 0x06001824 RID: 6180 RVA: 0x0005CCB4 File Offset: 0x0005AEB4
		public virtual int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x170003CF RID: 975
		// (get) Token: 0x06001825 RID: 6181 RVA: 0x0005CCBC File Offset: 0x0005AEBC
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170003D0 RID: 976
		// (get) Token: 0x06001826 RID: 6182 RVA: 0x0005CCC0 File Offset: 0x0005AEC0
		public virtual object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06001827 RID: 6183 RVA: 0x0005CCC4 File Offset: 0x0005AEC4
		public virtual void Clear()
		{
			this.modCount++;
			for (int i = 0; i < this.count; i++)
			{
				this.contents[i] = null;
			}
			this.count = 0;
			this.current = -1;
		}

		// Token: 0x06001828 RID: 6184 RVA: 0x0005CD10 File Offset: 0x0005AF10
		public virtual object Clone()
		{
			return new Stack(this.contents)
			{
				current = this.current,
				count = this.count
			};
		}

		// Token: 0x06001829 RID: 6185 RVA: 0x0005CD44 File Offset: 0x0005AF44
		public virtual bool Contains(object obj)
		{
			if (this.count == 0)
			{
				return false;
			}
			if (obj == null)
			{
				for (int i = 0; i < this.count; i++)
				{
					if (this.contents[i] == null)
					{
						return true;
					}
				}
			}
			else
			{
				for (int j = 0; j < this.count; j++)
				{
					if (obj.Equals(this.contents[j]))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x0600182A RID: 6186 RVA: 0x0005CDBC File Offset: 0x0005AFBC
		public virtual void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (array.Rank > 1 || (array.Length > 0 && index >= array.Length) || this.count > array.Length - index)
			{
				throw new ArgumentException();
			}
			for (int num = this.current; num != -1; num--)
			{
				array.SetValue(this.contents[num], this.count - (num + 1) + index);
			}
		}

		// Token: 0x0600182B RID: 6187 RVA: 0x0005CE5C File Offset: 0x0005B05C
		public virtual IEnumerator GetEnumerator()
		{
			return new Stack.Enumerator(this);
		}

		// Token: 0x0600182C RID: 6188 RVA: 0x0005CE64 File Offset: 0x0005B064
		public virtual object Peek()
		{
			if (this.current == -1)
			{
				throw new InvalidOperationException();
			}
			return this.contents[this.current];
		}

		// Token: 0x0600182D RID: 6189 RVA: 0x0005CE88 File Offset: 0x0005B088
		public virtual object Pop()
		{
			if (this.current == -1)
			{
				throw new InvalidOperationException();
			}
			this.modCount++;
			object result = this.contents[this.current];
			this.contents[this.current] = null;
			this.count--;
			this.current--;
			if (this.count <= this.capacity / 4 && this.count > 16)
			{
				this.Resize(this.capacity / 2);
			}
			return result;
		}

		// Token: 0x0600182E RID: 6190 RVA: 0x0005CF1C File Offset: 0x0005B11C
		public virtual void Push(object obj)
		{
			this.modCount++;
			if (this.capacity == this.count)
			{
				this.Resize(this.capacity * 2);
			}
			this.count++;
			this.current++;
			this.contents[this.current] = obj;
		}

		// Token: 0x0600182F RID: 6191 RVA: 0x0005CF80 File Offset: 0x0005B180
		public virtual object[] ToArray()
		{
			object[] array = new object[this.count];
			Array.Copy(this.contents, array, this.count);
			Array.Reverse(array);
			return array;
		}

		// Token: 0x040008C0 RID: 2240
		private const int default_capacity = 16;

		// Token: 0x040008C1 RID: 2241
		private object[] contents;

		// Token: 0x040008C2 RID: 2242
		private int current = -1;

		// Token: 0x040008C3 RID: 2243
		private int count;

		// Token: 0x040008C4 RID: 2244
		private int capacity;

		// Token: 0x040008C5 RID: 2245
		private int modCount;

		// Token: 0x020001D3 RID: 467
		[Serializable]
		private class SyncStack : Stack
		{
			// Token: 0x06001830 RID: 6192 RVA: 0x0005CFB4 File Offset: 0x0005B1B4
			internal SyncStack(Stack s)
			{
				this.stack = s;
			}

			// Token: 0x170003D1 RID: 977
			// (get) Token: 0x06001831 RID: 6193 RVA: 0x0005CFC4 File Offset: 0x0005B1C4
			public override int Count
			{
				get
				{
					Stack obj = this.stack;
					int count;
					lock (obj)
					{
						count = this.stack.Count;
					}
					return count;
				}
			}

			// Token: 0x170003D2 RID: 978
			// (get) Token: 0x06001832 RID: 6194 RVA: 0x0005D018 File Offset: 0x0005B218
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x170003D3 RID: 979
			// (get) Token: 0x06001833 RID: 6195 RVA: 0x0005D01C File Offset: 0x0005B21C
			public override object SyncRoot
			{
				get
				{
					return this.stack.SyncRoot;
				}
			}

			// Token: 0x06001834 RID: 6196 RVA: 0x0005D02C File Offset: 0x0005B22C
			public override void Clear()
			{
				Stack obj = this.stack;
				lock (obj)
				{
					this.stack.Clear();
				}
			}

			// Token: 0x06001835 RID: 6197 RVA: 0x0005D07C File Offset: 0x0005B27C
			public override object Clone()
			{
				Stack obj = this.stack;
				object result;
				lock (obj)
				{
					result = Stack.Synchronized((Stack)this.stack.Clone());
				}
				return result;
			}

			// Token: 0x06001836 RID: 6198 RVA: 0x0005D0DC File Offset: 0x0005B2DC
			public override bool Contains(object obj)
			{
				Stack obj2 = this.stack;
				bool result;
				lock (obj2)
				{
					result = this.stack.Contains(obj);
				}
				return result;
			}

			// Token: 0x06001837 RID: 6199 RVA: 0x0005D134 File Offset: 0x0005B334
			public override void CopyTo(Array array, int index)
			{
				Stack obj = this.stack;
				lock (obj)
				{
					this.stack.CopyTo(array, index);
				}
			}

			// Token: 0x06001838 RID: 6200 RVA: 0x0005D184 File Offset: 0x0005B384
			public override IEnumerator GetEnumerator()
			{
				Stack obj = this.stack;
				IEnumerator result;
				lock (obj)
				{
					result = new Stack.Enumerator(this.stack);
				}
				return result;
			}

			// Token: 0x06001839 RID: 6201 RVA: 0x0005D1D8 File Offset: 0x0005B3D8
			public override object Peek()
			{
				Stack obj = this.stack;
				object result;
				lock (obj)
				{
					result = this.stack.Peek();
				}
				return result;
			}

			// Token: 0x0600183A RID: 6202 RVA: 0x0005D22C File Offset: 0x0005B42C
			public override object Pop()
			{
				Stack obj = this.stack;
				object result;
				lock (obj)
				{
					result = this.stack.Pop();
				}
				return result;
			}

			// Token: 0x0600183B RID: 6203 RVA: 0x0005D280 File Offset: 0x0005B480
			public override void Push(object obj)
			{
				Stack obj2 = this.stack;
				lock (obj2)
				{
					this.stack.Push(obj);
				}
			}

			// Token: 0x0600183C RID: 6204 RVA: 0x0005D2D0 File Offset: 0x0005B4D0
			public override object[] ToArray()
			{
				Stack obj = this.stack;
				object[] result;
				lock (obj)
				{
					result = this.stack.ToArray();
				}
				return result;
			}

			// Token: 0x040008C6 RID: 2246
			private Stack stack;
		}

		// Token: 0x020001D4 RID: 468
		private class Enumerator : IEnumerator, ICloneable
		{
			// Token: 0x0600183D RID: 6205 RVA: 0x0005D324 File Offset: 0x0005B524
			internal Enumerator(Stack s)
			{
				this.stack = s;
				this.modCount = s.modCount;
				this.current = -2;
			}

			// Token: 0x0600183E RID: 6206 RVA: 0x0005D348 File Offset: 0x0005B548
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x170003D4 RID: 980
			// (get) Token: 0x0600183F RID: 6207 RVA: 0x0005D350 File Offset: 0x0005B550
			public virtual object Current
			{
				get
				{
					if (this.modCount != this.stack.modCount || this.current == -2 || this.current == -1 || this.current > this.stack.count)
					{
						throw new InvalidOperationException();
					}
					return this.stack.contents[this.current];
				}
			}

			// Token: 0x06001840 RID: 6208 RVA: 0x0005D3BC File Offset: 0x0005B5BC
			public virtual bool MoveNext()
			{
				if (this.modCount != this.stack.modCount)
				{
					throw new InvalidOperationException();
				}
				int num = this.current;
				if (num == -2)
				{
					this.current = this.stack.current;
					return this.current != -1;
				}
				if (num != -1)
				{
					this.current--;
					return this.current != -1;
				}
				return false;
			}

			// Token: 0x06001841 RID: 6209 RVA: 0x0005D43C File Offset: 0x0005B63C
			public virtual void Reset()
			{
				if (this.modCount != this.stack.modCount)
				{
					throw new InvalidOperationException();
				}
				this.current = -2;
			}

			// Token: 0x040008C7 RID: 2247
			private const int EOF = -1;

			// Token: 0x040008C8 RID: 2248
			private const int BOF = -2;

			// Token: 0x040008C9 RID: 2249
			private Stack stack;

			// Token: 0x040008CA RID: 2250
			private int modCount;

			// Token: 0x040008CB RID: 2251
			private int current;
		}
	}
}
