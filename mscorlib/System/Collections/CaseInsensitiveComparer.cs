﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001B3 RID: 435
	[ComVisible(true)]
	[Serializable]
	public class CaseInsensitiveComparer : IComparer
	{
		// Token: 0x060016A9 RID: 5801 RVA: 0x00058560 File Offset: 0x00056760
		public CaseInsensitiveComparer()
		{
			this.culture = CultureInfo.CurrentCulture;
		}

		// Token: 0x060016AA RID: 5802 RVA: 0x00058574 File Offset: 0x00056774
		private CaseInsensitiveComparer(bool invariant)
		{
		}

		// Token: 0x060016AB RID: 5803 RVA: 0x0005857C File Offset: 0x0005677C
		public CaseInsensitiveComparer(CultureInfo culture)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("culture");
			}
			if (culture.LCID != CultureInfo.InvariantCulture.LCID)
			{
				this.culture = culture;
			}
		}

		// Token: 0x17000362 RID: 866
		// (get) Token: 0x060016AD RID: 5805 RVA: 0x000585D4 File Offset: 0x000567D4
		public static CaseInsensitiveComparer Default
		{
			get
			{
				return CaseInsensitiveComparer.defaultComparer;
			}
		}

		// Token: 0x17000363 RID: 867
		// (get) Token: 0x060016AE RID: 5806 RVA: 0x000585DC File Offset: 0x000567DC
		public static CaseInsensitiveComparer DefaultInvariant
		{
			get
			{
				return CaseInsensitiveComparer.defaultInvariantComparer;
			}
		}

		// Token: 0x060016AF RID: 5807 RVA: 0x000585E4 File Offset: 0x000567E4
		public int Compare(object a, object b)
		{
			string text = a as string;
			string text2 = b as string;
			if (text == null || text2 == null)
			{
				return Comparer.Default.Compare(a, b);
			}
			if (this.culture != null)
			{
				return this.culture.CompareInfo.Compare(text, text2, CompareOptions.IgnoreCase);
			}
			return CultureInfo.InvariantCulture.CompareInfo.Compare(text, text2, CompareOptions.IgnoreCase);
		}

		// Token: 0x0400086E RID: 2158
		private static CaseInsensitiveComparer defaultComparer = new CaseInsensitiveComparer();

		// Token: 0x0400086F RID: 2159
		private static CaseInsensitiveComparer defaultInvariantComparer = new CaseInsensitiveComparer(true);

		// Token: 0x04000870 RID: 2160
		private CultureInfo culture;
	}
}
