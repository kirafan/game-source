﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001C5 RID: 453
	[ComVisible(true)]
	public interface IEqualityComparer
	{
		// Token: 0x06001777 RID: 6007
		bool Equals(object x, object y);

		// Token: 0x06001778 RID: 6008
		int GetHashCode(object obj);
	}
}
