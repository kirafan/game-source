﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001C6 RID: 454
	[ComVisible(true)]
	[Obsolete("Please use IEqualityComparer instead.")]
	public interface IHashCodeProvider
	{
		// Token: 0x06001779 RID: 6009
		int GetHashCode(object obj);
	}
}
