﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x02000018 RID: 24
	[Guid("496B0ABE-CDEE-11d3-88E8-00902754C43A")]
	[ComVisible(true)]
	public interface IEnumerable
	{
		// Token: 0x06000172 RID: 370
		[DispId(-4)]
		IEnumerator GetEnumerator();
	}
}
