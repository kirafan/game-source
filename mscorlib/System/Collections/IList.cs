﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x0200002E RID: 46
	[ComVisible(true)]
	public interface IList : IEnumerable, ICollection
	{
		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060004A2 RID: 1186
		bool IsFixedSize { get; }

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060004A3 RID: 1187
		bool IsReadOnly { get; }

		// Token: 0x17000024 RID: 36
		object this[int index]
		{
			get;
			set;
		}

		// Token: 0x060004A6 RID: 1190
		int Add(object value);

		// Token: 0x060004A7 RID: 1191
		void Clear();

		// Token: 0x060004A8 RID: 1192
		bool Contains(object value);

		// Token: 0x060004A9 RID: 1193
		int IndexOf(object value);

		// Token: 0x060004AA RID: 1194
		void Insert(int index, object value);

		// Token: 0x060004AB RID: 1195
		void Remove(object value);

		// Token: 0x060004AC RID: 1196
		void RemoveAt(int index);
	}
}
