﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001C4 RID: 452
	[ComVisible(true)]
	public interface IDictionaryEnumerator : IEnumerator
	{
		// Token: 0x1700039E RID: 926
		// (get) Token: 0x06001774 RID: 6004
		DictionaryEntry Entry { get; }

		// Token: 0x1700039F RID: 927
		// (get) Token: 0x06001775 RID: 6005
		object Key { get; }

		// Token: 0x170003A0 RID: 928
		// (get) Token: 0x06001776 RID: 6006
		object Value { get; }
	}
}
