﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x0200002D RID: 45
	[ComVisible(true)]
	public interface ICollection : IEnumerable
	{
		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600049E RID: 1182
		int Count { get; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600049F RID: 1183
		bool IsSynchronized { get; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060004A0 RID: 1184
		object SyncRoot { get; }

		// Token: 0x060004A1 RID: 1185
		void CopyTo(Array array, int index);
	}
}
