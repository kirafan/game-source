﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Collections
{
	// Token: 0x020001B6 RID: 438
	[ComVisible(true)]
	[Serializable]
	public sealed class Comparer : ISerializable, IComparer
	{
		// Token: 0x060016D7 RID: 5847 RVA: 0x00058C1C File Offset: 0x00056E1C
		private Comparer()
		{
		}

		// Token: 0x060016D8 RID: 5848 RVA: 0x00058C24 File Offset: 0x00056E24
		public Comparer(CultureInfo culture)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("culture");
			}
			this.m_compareInfo = culture.CompareInfo;
		}

		// Token: 0x060016DA RID: 5850 RVA: 0x00058C68 File Offset: 0x00056E68
		public int Compare(object a, object b)
		{
			if (a == b)
			{
				return 0;
			}
			if (a == null)
			{
				return -1;
			}
			if (b == null)
			{
				return 1;
			}
			if (this.m_compareInfo != null)
			{
				string text = a as string;
				string text2 = b as string;
				if (text != null && text2 != null)
				{
					return this.m_compareInfo.Compare(text, text2);
				}
			}
			if (a is IComparable)
			{
				return (a as IComparable).CompareTo(b);
			}
			if (b is IComparable)
			{
				return -(b as IComparable).CompareTo(a);
			}
			throw new ArgumentException(Locale.GetText("Neither 'a' nor 'b' implements IComparable."));
		}

		// Token: 0x060016DB RID: 5851 RVA: 0x00058D04 File Offset: 0x00056F04
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("CompareInfo", this.m_compareInfo, typeof(CompareInfo));
		}

		// Token: 0x04000876 RID: 2166
		public static readonly Comparer Default = new Comparer();

		// Token: 0x04000877 RID: 2167
		public static readonly Comparer DefaultInvariant = new Comparer(CultureInfo.InvariantCulture);

		// Token: 0x04000878 RID: 2168
		private CompareInfo m_compareInfo;
	}
}
