﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001B4 RID: 436
	[ComVisible(true)]
	[Obsolete("Please use StringComparer instead.")]
	[Serializable]
	public class CaseInsensitiveHashCodeProvider : IHashCodeProvider
	{
		// Token: 0x060016B0 RID: 5808 RVA: 0x0005864C File Offset: 0x0005684C
		public CaseInsensitiveHashCodeProvider()
		{
			CultureInfo currentCulture = CultureInfo.CurrentCulture;
			if (!CaseInsensitiveHashCodeProvider.AreEqual(currentCulture, CultureInfo.InvariantCulture))
			{
				this.m_text = CultureInfo.CurrentCulture.TextInfo;
			}
		}

		// Token: 0x060016B1 RID: 5809 RVA: 0x00058688 File Offset: 0x00056888
		public CaseInsensitiveHashCodeProvider(CultureInfo culture)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("culture");
			}
			if (!CaseInsensitiveHashCodeProvider.AreEqual(culture, CultureInfo.InvariantCulture))
			{
				this.m_text = culture.TextInfo;
			}
		}

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x060016B3 RID: 5811 RVA: 0x000586DC File Offset: 0x000568DC
		public static CaseInsensitiveHashCodeProvider Default
		{
			get
			{
				object obj = CaseInsensitiveHashCodeProvider.sync;
				CaseInsensitiveHashCodeProvider result;
				lock (obj)
				{
					if (CaseInsensitiveHashCodeProvider.singleton == null)
					{
						CaseInsensitiveHashCodeProvider.singleton = new CaseInsensitiveHashCodeProvider();
					}
					else if (CaseInsensitiveHashCodeProvider.singleton.m_text == null)
					{
						if (!CaseInsensitiveHashCodeProvider.AreEqual(CultureInfo.CurrentCulture, CultureInfo.InvariantCulture))
						{
							CaseInsensitiveHashCodeProvider.singleton = new CaseInsensitiveHashCodeProvider();
						}
					}
					else if (!CaseInsensitiveHashCodeProvider.AreEqual(CaseInsensitiveHashCodeProvider.singleton.m_text, CultureInfo.CurrentCulture))
					{
						CaseInsensitiveHashCodeProvider.singleton = new CaseInsensitiveHashCodeProvider();
					}
					result = CaseInsensitiveHashCodeProvider.singleton;
				}
				return result;
			}
		}

		// Token: 0x060016B4 RID: 5812 RVA: 0x00058798 File Offset: 0x00056998
		private static bool AreEqual(CultureInfo a, CultureInfo b)
		{
			return a.LCID == b.LCID;
		}

		// Token: 0x060016B5 RID: 5813 RVA: 0x000587A8 File Offset: 0x000569A8
		private static bool AreEqual(TextInfo info, CultureInfo culture)
		{
			return info.LCID == culture.LCID;
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x060016B6 RID: 5814 RVA: 0x000587B8 File Offset: 0x000569B8
		public static CaseInsensitiveHashCodeProvider DefaultInvariant
		{
			get
			{
				return CaseInsensitiveHashCodeProvider.singletonInvariant;
			}
		}

		// Token: 0x060016B7 RID: 5815 RVA: 0x000587C0 File Offset: 0x000569C0
		public int GetHashCode(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			string text = obj as string;
			if (text == null)
			{
				return obj.GetHashCode();
			}
			int num = 0;
			if (this.m_text != null && !CaseInsensitiveHashCodeProvider.AreEqual(this.m_text, CultureInfo.InvariantCulture))
			{
				foreach (char c in this.m_text.ToLower(text))
				{
					num = num * 31 + (int)c;
				}
			}
			else
			{
				for (int j = 0; j < text.Length; j++)
				{
					char c = char.ToLower(text[j], CultureInfo.InvariantCulture);
					num = num * 31 + (int)c;
				}
			}
			return num;
		}

		// Token: 0x04000871 RID: 2161
		private static readonly CaseInsensitiveHashCodeProvider singletonInvariant = new CaseInsensitiveHashCodeProvider(CultureInfo.InvariantCulture);

		// Token: 0x04000872 RID: 2162
		private static CaseInsensitiveHashCodeProvider singleton;

		// Token: 0x04000873 RID: 2163
		private static readonly object sync = new object();

		// Token: 0x04000874 RID: 2164
		private TextInfo m_text;
	}
}
