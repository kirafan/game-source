﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System.Collections.ObjectModel
{
	// Token: 0x020006D0 RID: 1744
	[ComVisible(false)]
	[Serializable]
	public class Collection<T> : IEnumerable, ICollection, IList, ICollection<T>, IList<T>, IEnumerable<T>
	{
		// Token: 0x060042E8 RID: 17128 RVA: 0x000E5234 File Offset: 0x000E3434
		public Collection()
		{
			List<T> list = new List<T>();
			IList list2 = list;
			this.syncRoot = list2.SyncRoot;
			this.list = list;
		}

		// Token: 0x060042E9 RID: 17129 RVA: 0x000E5264 File Offset: 0x000E3464
		public Collection(IList<T> list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			this.list = list;
			ICollection collection = list as ICollection;
			this.syncRoot = ((collection == null) ? new object() : collection.SyncRoot);
		}

		// Token: 0x17000C78 RID: 3192
		// (get) Token: 0x060042EA RID: 17130 RVA: 0x000E52B4 File Offset: 0x000E34B4
		bool ICollection<T>.IsReadOnly
		{
			get
			{
				return this.list.IsReadOnly;
			}
		}

		// Token: 0x060042EB RID: 17131 RVA: 0x000E52C4 File Offset: 0x000E34C4
		void ICollection.CopyTo(Array array, int index)
		{
			((ICollection)this.list).CopyTo(array, index);
		}

		// Token: 0x060042EC RID: 17132 RVA: 0x000E52D8 File Offset: 0x000E34D8
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x060042ED RID: 17133 RVA: 0x000E52E8 File Offset: 0x000E34E8
		int IList.Add(object value)
		{
			int count = this.list.Count;
			this.InsertItem(count, Collection<T>.ConvertItem(value));
			return count;
		}

		// Token: 0x060042EE RID: 17134 RVA: 0x000E5310 File Offset: 0x000E3510
		bool IList.Contains(object value)
		{
			return Collection<T>.IsValidItem(value) && this.list.Contains((T)((object)value));
		}

		// Token: 0x060042EF RID: 17135 RVA: 0x000E5330 File Offset: 0x000E3530
		int IList.IndexOf(object value)
		{
			if (Collection<T>.IsValidItem(value))
			{
				return this.list.IndexOf((T)((object)value));
			}
			return -1;
		}

		// Token: 0x060042F0 RID: 17136 RVA: 0x000E5350 File Offset: 0x000E3550
		void IList.Insert(int index, object value)
		{
			this.InsertItem(index, Collection<T>.ConvertItem(value));
		}

		// Token: 0x060042F1 RID: 17137 RVA: 0x000E5360 File Offset: 0x000E3560
		void IList.Remove(object value)
		{
			Collection<T>.CheckWritable(this.list);
			int index = this.IndexOf(Collection<T>.ConvertItem(value));
			this.RemoveItem(index);
		}

		// Token: 0x17000C79 RID: 3193
		// (get) Token: 0x060042F2 RID: 17138 RVA: 0x000E538C File Offset: 0x000E358C
		bool ICollection.IsSynchronized
		{
			get
			{
				return Collection<T>.IsSynchronized(this.list);
			}
		}

		// Token: 0x17000C7A RID: 3194
		// (get) Token: 0x060042F3 RID: 17139 RVA: 0x000E539C File Offset: 0x000E359C
		object ICollection.SyncRoot
		{
			get
			{
				return this.syncRoot;
			}
		}

		// Token: 0x17000C7B RID: 3195
		// (get) Token: 0x060042F4 RID: 17140 RVA: 0x000E53A4 File Offset: 0x000E35A4
		bool IList.IsFixedSize
		{
			get
			{
				return Collection<T>.IsFixedSize(this.list);
			}
		}

		// Token: 0x17000C7C RID: 3196
		// (get) Token: 0x060042F5 RID: 17141 RVA: 0x000E53B4 File Offset: 0x000E35B4
		bool IList.IsReadOnly
		{
			get
			{
				return this.list.IsReadOnly;
			}
		}

		// Token: 0x17000C7D RID: 3197
		object IList.this[int index]
		{
			get
			{
				return this.list[index];
			}
			set
			{
				this.SetItem(index, Collection<T>.ConvertItem(value));
			}
		}

		// Token: 0x060042F8 RID: 17144 RVA: 0x000E53E8 File Offset: 0x000E35E8
		public void Add(T item)
		{
			int count = this.list.Count;
			this.InsertItem(count, item);
		}

		// Token: 0x060042F9 RID: 17145 RVA: 0x000E540C File Offset: 0x000E360C
		public void Clear()
		{
			this.ClearItems();
		}

		// Token: 0x060042FA RID: 17146 RVA: 0x000E5414 File Offset: 0x000E3614
		protected virtual void ClearItems()
		{
			this.list.Clear();
		}

		// Token: 0x060042FB RID: 17147 RVA: 0x000E5424 File Offset: 0x000E3624
		public bool Contains(T item)
		{
			return this.list.Contains(item);
		}

		// Token: 0x060042FC RID: 17148 RVA: 0x000E5434 File Offset: 0x000E3634
		public void CopyTo(T[] array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x060042FD RID: 17149 RVA: 0x000E5444 File Offset: 0x000E3644
		public IEnumerator<T> GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x060042FE RID: 17150 RVA: 0x000E5454 File Offset: 0x000E3654
		public int IndexOf(T item)
		{
			return this.list.IndexOf(item);
		}

		// Token: 0x060042FF RID: 17151 RVA: 0x000E5464 File Offset: 0x000E3664
		public void Insert(int index, T item)
		{
			this.InsertItem(index, item);
		}

		// Token: 0x06004300 RID: 17152 RVA: 0x000E5470 File Offset: 0x000E3670
		protected virtual void InsertItem(int index, T item)
		{
			this.list.Insert(index, item);
		}

		// Token: 0x17000C7E RID: 3198
		// (get) Token: 0x06004301 RID: 17153 RVA: 0x000E5480 File Offset: 0x000E3680
		protected IList<T> Items
		{
			get
			{
				return this.list;
			}
		}

		// Token: 0x06004302 RID: 17154 RVA: 0x000E5488 File Offset: 0x000E3688
		public bool Remove(T item)
		{
			int num = this.IndexOf(item);
			if (num == -1)
			{
				return false;
			}
			this.RemoveItem(num);
			return true;
		}

		// Token: 0x06004303 RID: 17155 RVA: 0x000E54B0 File Offset: 0x000E36B0
		public void RemoveAt(int index)
		{
			this.RemoveItem(index);
		}

		// Token: 0x06004304 RID: 17156 RVA: 0x000E54BC File Offset: 0x000E36BC
		protected virtual void RemoveItem(int index)
		{
			this.list.RemoveAt(index);
		}

		// Token: 0x17000C7F RID: 3199
		// (get) Token: 0x06004305 RID: 17157 RVA: 0x000E54CC File Offset: 0x000E36CC
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000C80 RID: 3200
		public T this[int index]
		{
			get
			{
				return this.list[index];
			}
			set
			{
				this.SetItem(index, value);
			}
		}

		// Token: 0x06004308 RID: 17160 RVA: 0x000E54F8 File Offset: 0x000E36F8
		protected virtual void SetItem(int index, T item)
		{
			this.list[index] = item;
		}

		// Token: 0x06004309 RID: 17161 RVA: 0x000E5508 File Offset: 0x000E3708
		internal static bool IsValidItem(object item)
		{
			return item is T || (item == null && !typeof(T).IsValueType);
		}

		// Token: 0x0600430A RID: 17162 RVA: 0x000E5534 File Offset: 0x000E3734
		internal static T ConvertItem(object item)
		{
			if (Collection<T>.IsValidItem(item))
			{
				return (T)((object)item);
			}
			throw new ArgumentException("item");
		}

		// Token: 0x0600430B RID: 17163 RVA: 0x000E5554 File Offset: 0x000E3754
		internal static void CheckWritable(IList<T> list)
		{
			if (list.IsReadOnly)
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x0600430C RID: 17164 RVA: 0x000E5568 File Offset: 0x000E3768
		internal static bool IsSynchronized(IList<T> list)
		{
			ICollection collection = list as ICollection;
			return collection != null && collection.IsSynchronized;
		}

		// Token: 0x0600430D RID: 17165 RVA: 0x000E5590 File Offset: 0x000E3790
		internal static bool IsFixedSize(IList<T> list)
		{
			IList list2 = list as IList;
			return list2 != null && list2.IsFixedSize;
		}

		// Token: 0x04001C55 RID: 7253
		private IList<T> list;

		// Token: 0x04001C56 RID: 7254
		private object syncRoot;
	}
}
