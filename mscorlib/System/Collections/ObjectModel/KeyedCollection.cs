﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System.Collections.ObjectModel
{
	// Token: 0x020006D1 RID: 1745
	[ComVisible(false)]
	[Serializable]
	public abstract class KeyedCollection<TKey, TItem> : Collection<TItem>
	{
		// Token: 0x0600430E RID: 17166 RVA: 0x000E55B8 File Offset: 0x000E37B8
		protected KeyedCollection() : this(null, 0)
		{
		}

		// Token: 0x0600430F RID: 17167 RVA: 0x000E55C4 File Offset: 0x000E37C4
		protected KeyedCollection(IEqualityComparer<TKey> comparer) : this(comparer, 0)
		{
		}

		// Token: 0x06004310 RID: 17168 RVA: 0x000E55D0 File Offset: 0x000E37D0
		protected KeyedCollection(IEqualityComparer<TKey> comparer, int dictionaryCreationThreshold)
		{
			if (comparer != null)
			{
				this.comparer = comparer;
			}
			else
			{
				this.comparer = EqualityComparer<TKey>.Default;
			}
			this.dictionaryCreationThreshold = dictionaryCreationThreshold;
			if (dictionaryCreationThreshold == 0)
			{
				this.dictionary = new Dictionary<TKey, TItem>(this.comparer);
			}
		}

		// Token: 0x06004311 RID: 17169 RVA: 0x000E5620 File Offset: 0x000E3820
		public bool Contains(TKey key)
		{
			if (this.dictionary != null)
			{
				return this.dictionary.ContainsKey(key);
			}
			return this.IndexOfKey(key) >= 0;
		}

		// Token: 0x06004312 RID: 17170 RVA: 0x000E5648 File Offset: 0x000E3848
		private int IndexOfKey(TKey key)
		{
			for (int i = this.Count - 1; i >= 0; i--)
			{
				TKey keyForItem = this.GetKeyForItem(this[i]);
				if (this.comparer.Equals(key, keyForItem))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06004313 RID: 17171 RVA: 0x000E5694 File Offset: 0x000E3894
		public bool Remove(TKey key)
		{
			if (this.dictionary != null)
			{
				TItem item;
				return this.dictionary.TryGetValue(key, out item) && base.Remove(item);
			}
			int num = this.IndexOfKey(key);
			if (num == -1)
			{
				return false;
			}
			this.RemoveAt(num);
			return true;
		}

		// Token: 0x17000C81 RID: 3201
		// (get) Token: 0x06004314 RID: 17172 RVA: 0x000E56E4 File Offset: 0x000E38E4
		public IEqualityComparer<TKey> Comparer
		{
			get
			{
				return this.comparer;
			}
		}

		// Token: 0x17000C82 RID: 3202
		public TItem this[TKey key]
		{
			get
			{
				if (this.dictionary != null)
				{
					return this.dictionary[key];
				}
				int num = this.IndexOfKey(key);
				if (num >= 0)
				{
					return base[num];
				}
				throw new KeyNotFoundException();
			}
		}

		// Token: 0x06004316 RID: 17174 RVA: 0x000E5730 File Offset: 0x000E3930
		protected void ChangeItemKey(TItem item, TKey newKey)
		{
			if (!this.Contains(item))
			{
				throw new ArgumentException();
			}
			TKey keyForItem = this.GetKeyForItem(item);
			if (this.comparer.Equals(keyForItem, newKey))
			{
				return;
			}
			if (this.Contains(newKey))
			{
				throw new ArgumentException();
			}
			if (this.dictionary != null)
			{
				if (!this.dictionary.Remove(keyForItem))
				{
					throw new ArgumentException();
				}
				this.dictionary.Add(newKey, item);
			}
		}

		// Token: 0x06004317 RID: 17175 RVA: 0x000E57AC File Offset: 0x000E39AC
		protected override void ClearItems()
		{
			if (this.dictionary != null)
			{
				this.dictionary.Clear();
			}
			base.ClearItems();
		}

		// Token: 0x06004318 RID: 17176
		protected abstract TKey GetKeyForItem(TItem item);

		// Token: 0x06004319 RID: 17177 RVA: 0x000E57CC File Offset: 0x000E39CC
		protected override void InsertItem(int index, TItem item)
		{
			TKey keyForItem = this.GetKeyForItem(item);
			if (keyForItem == null)
			{
				throw new ArgumentNullException("GetKeyForItem(item)");
			}
			if (this.dictionary != null && this.dictionary.ContainsKey(keyForItem))
			{
				throw new ArgumentException("An element with the same key already exists in the dictionary.");
			}
			if (this.dictionary == null)
			{
				for (int i = 0; i < this.Count; i++)
				{
					if (this.comparer.Equals(keyForItem, this.GetKeyForItem(this[i])))
					{
						throw new ArgumentException("An element with the same key already exists in the dictionary.");
					}
				}
			}
			base.InsertItem(index, item);
			if (this.dictionary != null)
			{
				this.dictionary.Add(keyForItem, item);
			}
			else if (this.dictionaryCreationThreshold != -1 && this.Count > this.dictionaryCreationThreshold)
			{
				this.dictionary = new Dictionary<TKey, TItem>(this.comparer);
				for (int j = 0; j < this.Count; j++)
				{
					TItem titem = this[j];
					this.dictionary.Add(this.GetKeyForItem(titem), titem);
				}
			}
		}

		// Token: 0x0600431A RID: 17178 RVA: 0x000E58F0 File Offset: 0x000E3AF0
		protected override void RemoveItem(int index)
		{
			if (this.dictionary != null)
			{
				TKey keyForItem = this.GetKeyForItem(this[index]);
				this.dictionary.Remove(keyForItem);
			}
			base.RemoveItem(index);
		}

		// Token: 0x0600431B RID: 17179 RVA: 0x000E592C File Offset: 0x000E3B2C
		protected override void SetItem(int index, TItem item)
		{
			if (this.dictionary != null)
			{
				this.dictionary.Remove(this.GetKeyForItem(this[index]));
				this.dictionary.Add(this.GetKeyForItem(item), item);
			}
			base.SetItem(index, item);
		}

		// Token: 0x17000C83 RID: 3203
		// (get) Token: 0x0600431C RID: 17180 RVA: 0x000E5978 File Offset: 0x000E3B78
		protected IDictionary<TKey, TItem> Dictionary
		{
			get
			{
				return this.dictionary;
			}
		}

		// Token: 0x04001C57 RID: 7255
		private Dictionary<TKey, TItem> dictionary;

		// Token: 0x04001C58 RID: 7256
		private IEqualityComparer<TKey> comparer;

		// Token: 0x04001C59 RID: 7257
		private int dictionaryCreationThreshold;
	}
}
