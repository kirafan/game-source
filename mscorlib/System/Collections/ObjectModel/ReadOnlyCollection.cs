﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System.Collections.ObjectModel
{
	// Token: 0x020006D2 RID: 1746
	[ComVisible(false)]
	[Serializable]
	public class ReadOnlyCollection<T> : IEnumerable, ICollection, IList, ICollection<T>, IList<T>, IEnumerable<T>
	{
		// Token: 0x0600431D RID: 17181 RVA: 0x000E5980 File Offset: 0x000E3B80
		public ReadOnlyCollection(IList<T> list)
		{
			if (list == null)
			{
				throw new ArgumentNullException("list");
			}
			this.list = list;
		}

		// Token: 0x0600431E RID: 17182 RVA: 0x000E59A0 File Offset: 0x000E3BA0
		void ICollection<T>.Add(T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600431F RID: 17183 RVA: 0x000E59A8 File Offset: 0x000E3BA8
		void ICollection<T>.Clear()
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004320 RID: 17184 RVA: 0x000E59B0 File Offset: 0x000E3BB0
		void IList<T>.Insert(int index, T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004321 RID: 17185 RVA: 0x000E59B8 File Offset: 0x000E3BB8
		bool ICollection<T>.Remove(T item)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004322 RID: 17186 RVA: 0x000E59C0 File Offset: 0x000E3BC0
		void IList<T>.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000C84 RID: 3204
		T IList<T>.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000C85 RID: 3205
		// (get) Token: 0x06004325 RID: 17189 RVA: 0x000E59DC File Offset: 0x000E3BDC
		bool ICollection<T>.IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06004326 RID: 17190 RVA: 0x000E59E0 File Offset: 0x000E3BE0
		void ICollection.CopyTo(Array array, int index)
		{
			((ICollection)this.list).CopyTo(array, index);
		}

		// Token: 0x06004327 RID: 17191 RVA: 0x000E59F4 File Offset: 0x000E3BF4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x06004328 RID: 17192 RVA: 0x000E5A04 File Offset: 0x000E3C04
		int IList.Add(object value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06004329 RID: 17193 RVA: 0x000E5A0C File Offset: 0x000E3C0C
		void IList.Clear()
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600432A RID: 17194 RVA: 0x000E5A14 File Offset: 0x000E3C14
		bool IList.Contains(object value)
		{
			return Collection<T>.IsValidItem(value) && this.list.Contains((T)((object)value));
		}

		// Token: 0x0600432B RID: 17195 RVA: 0x000E5A34 File Offset: 0x000E3C34
		int IList.IndexOf(object value)
		{
			if (Collection<T>.IsValidItem(value))
			{
				return this.list.IndexOf((T)((object)value));
			}
			return -1;
		}

		// Token: 0x0600432C RID: 17196 RVA: 0x000E5A54 File Offset: 0x000E3C54
		void IList.Insert(int index, object value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600432D RID: 17197 RVA: 0x000E5A5C File Offset: 0x000E3C5C
		void IList.Remove(object value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600432E RID: 17198 RVA: 0x000E5A64 File Offset: 0x000E3C64
		void IList.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000C86 RID: 3206
		// (get) Token: 0x0600432F RID: 17199 RVA: 0x000E5A6C File Offset: 0x000E3C6C
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000C87 RID: 3207
		// (get) Token: 0x06004330 RID: 17200 RVA: 0x000E5A70 File Offset: 0x000E3C70
		object ICollection.SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000C88 RID: 3208
		// (get) Token: 0x06004331 RID: 17201 RVA: 0x000E5A74 File Offset: 0x000E3C74
		bool IList.IsFixedSize
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000C89 RID: 3209
		// (get) Token: 0x06004332 RID: 17202 RVA: 0x000E5A78 File Offset: 0x000E3C78
		bool IList.IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000C8A RID: 3210
		object IList.this[int index]
		{
			get
			{
				return this.list[index];
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x06004335 RID: 17205 RVA: 0x000E5A98 File Offset: 0x000E3C98
		public bool Contains(T value)
		{
			return this.list.Contains(value);
		}

		// Token: 0x06004336 RID: 17206 RVA: 0x000E5AA8 File Offset: 0x000E3CA8
		public void CopyTo(T[] array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x06004337 RID: 17207 RVA: 0x000E5AB8 File Offset: 0x000E3CB8
		public IEnumerator<T> GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x06004338 RID: 17208 RVA: 0x000E5AC8 File Offset: 0x000E3CC8
		public int IndexOf(T value)
		{
			return this.list.IndexOf(value);
		}

		// Token: 0x17000C8B RID: 3211
		// (get) Token: 0x06004339 RID: 17209 RVA: 0x000E5AD8 File Offset: 0x000E3CD8
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000C8C RID: 3212
		// (get) Token: 0x0600433A RID: 17210 RVA: 0x000E5AE8 File Offset: 0x000E3CE8
		protected IList<T> Items
		{
			get
			{
				return this.list;
			}
		}

		// Token: 0x17000C8D RID: 3213
		public T this[int index]
		{
			get
			{
				return this.list[index];
			}
		}

		// Token: 0x04001C5A RID: 7258
		private IList<T> list;
	}
}
