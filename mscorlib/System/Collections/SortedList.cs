﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001CB RID: 459
	[DebuggerDisplay("Count={Count}")]
	[ComVisible(true)]
	[Serializable]
	public class SortedList : IEnumerable, ICloneable, ICollection, IDictionary
	{
		// Token: 0x060017A8 RID: 6056 RVA: 0x0005B354 File Offset: 0x00059554
		public SortedList() : this(null, SortedList.INITIAL_SIZE)
		{
		}

		// Token: 0x060017A9 RID: 6057 RVA: 0x0005B364 File Offset: 0x00059564
		public SortedList(int initialCapacity) : this(null, initialCapacity)
		{
		}

		// Token: 0x060017AA RID: 6058 RVA: 0x0005B370 File Offset: 0x00059570
		public SortedList(IComparer comparer, int capacity)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity");
			}
			if (capacity == 0)
			{
				this.defaultCapacity = 0;
			}
			else
			{
				this.defaultCapacity = SortedList.INITIAL_SIZE;
			}
			this.comparer = comparer;
			this.InitTable(capacity, true);
		}

		// Token: 0x060017AB RID: 6059 RVA: 0x0005B3C4 File Offset: 0x000595C4
		public SortedList(IComparer comparer)
		{
			this.comparer = comparer;
			this.InitTable(SortedList.INITIAL_SIZE, true);
		}

		// Token: 0x060017AC RID: 6060 RVA: 0x0005B3E0 File Offset: 0x000595E0
		public SortedList(IDictionary d) : this(d, null)
		{
		}

		// Token: 0x060017AD RID: 6061 RVA: 0x0005B3EC File Offset: 0x000595EC
		public SortedList(IDictionary d, IComparer comparer)
		{
			if (d == null)
			{
				throw new ArgumentNullException("dictionary");
			}
			this.InitTable(d.Count, true);
			this.comparer = comparer;
			IDictionaryEnumerator enumerator = d.GetEnumerator();
			while (enumerator.MoveNext())
			{
				this.Add(enumerator.Key, enumerator.Value);
			}
		}

		// Token: 0x060017AF RID: 6063 RVA: 0x0005B45C File Offset: 0x0005965C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new SortedList.Enumerator(this, SortedList.EnumeratorMode.ENTRY_MODE);
		}

		// Token: 0x170003AC RID: 940
		// (get) Token: 0x060017B0 RID: 6064 RVA: 0x0005B468 File Offset: 0x00059668
		public virtual int Count
		{
			get
			{
				return this.inUse;
			}
		}

		// Token: 0x170003AD RID: 941
		// (get) Token: 0x060017B1 RID: 6065 RVA: 0x0005B470 File Offset: 0x00059670
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170003AE RID: 942
		// (get) Token: 0x060017B2 RID: 6066 RVA: 0x0005B474 File Offset: 0x00059674
		public virtual object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x170003AF RID: 943
		// (get) Token: 0x060017B3 RID: 6067 RVA: 0x0005B478 File Offset: 0x00059678
		public virtual bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x060017B4 RID: 6068 RVA: 0x0005B47C File Offset: 0x0005967C
		public virtual bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x060017B5 RID: 6069 RVA: 0x0005B480 File Offset: 0x00059680
		public virtual ICollection Keys
		{
			get
			{
				return new SortedList.ListKeys(this);
			}
		}

		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x060017B6 RID: 6070 RVA: 0x0005B488 File Offset: 0x00059688
		public virtual ICollection Values
		{
			get
			{
				return new SortedList.ListValues(this);
			}
		}

		// Token: 0x170003B3 RID: 947
		public virtual object this[object key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException();
				}
				return this.GetImpl(key);
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException();
				}
				if (this.IsReadOnly)
				{
					throw new NotSupportedException("SortedList is Read Only.");
				}
				if (this.Find(key) < 0 && this.IsFixedSize)
				{
					throw new NotSupportedException("Key not found and SortedList is fixed size.");
				}
				this.PutImpl(key, value, true);
			}
		}

		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x060017B9 RID: 6073 RVA: 0x0005B504 File Offset: 0x00059704
		// (set) Token: 0x060017BA RID: 6074 RVA: 0x0005B510 File Offset: 0x00059710
		public virtual int Capacity
		{
			get
			{
				return this.table.Length;
			}
			set
			{
				int num = this.table.Length;
				if (this.inUse > value)
				{
					throw new ArgumentOutOfRangeException("capacity too small");
				}
				if (value == 0)
				{
					SortedList.Slot[] destinationArray = new SortedList.Slot[this.defaultCapacity];
					Array.Copy(this.table, destinationArray, this.inUse);
					this.table = destinationArray;
				}
				else if (value > this.inUse)
				{
					SortedList.Slot[] destinationArray2 = new SortedList.Slot[value];
					Array.Copy(this.table, destinationArray2, this.inUse);
					this.table = destinationArray2;
				}
				else if (value > num)
				{
					SortedList.Slot[] destinationArray3 = new SortedList.Slot[value];
					Array.Copy(this.table, destinationArray3, num);
					this.table = destinationArray3;
				}
			}
		}

		// Token: 0x060017BB RID: 6075 RVA: 0x0005B5C0 File Offset: 0x000597C0
		public virtual void Add(object key, object value)
		{
			this.PutImpl(key, value, false);
		}

		// Token: 0x060017BC RID: 6076 RVA: 0x0005B5CC File Offset: 0x000597CC
		public virtual void Clear()
		{
			this.defaultCapacity = SortedList.INITIAL_SIZE;
			this.table = new SortedList.Slot[this.defaultCapacity];
			this.inUse = 0;
			this.modificationCount++;
		}

		// Token: 0x060017BD RID: 6077 RVA: 0x0005B600 File Offset: 0x00059800
		public virtual bool Contains(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException();
			}
			bool result;
			try
			{
				result = (this.Find(key) >= 0);
			}
			catch (Exception)
			{
				throw new InvalidOperationException();
			}
			return result;
		}

		// Token: 0x060017BE RID: 6078 RVA: 0x0005B65C File Offset: 0x0005985C
		public virtual IDictionaryEnumerator GetEnumerator()
		{
			return new SortedList.Enumerator(this, SortedList.EnumeratorMode.ENTRY_MODE);
		}

		// Token: 0x060017BF RID: 6079 RVA: 0x0005B668 File Offset: 0x00059868
		public virtual void Remove(object key)
		{
			int num = this.IndexOfKey(key);
			if (num >= 0)
			{
				this.RemoveAt(num);
			}
		}

		// Token: 0x060017C0 RID: 6080 RVA: 0x0005B68C File Offset: 0x0005988C
		public virtual void CopyTo(Array array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException();
			}
			if (arrayIndex < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (array.Rank > 1)
			{
				throw new ArgumentException("array is multi-dimensional");
			}
			if (arrayIndex >= array.Length)
			{
				throw new ArgumentNullException("arrayIndex is greater than or equal to array.Length");
			}
			if (this.Count > array.Length - arrayIndex)
			{
				throw new ArgumentNullException("Not enough space in array from arrayIndex to end of array");
			}
			IDictionaryEnumerator enumerator = this.GetEnumerator();
			int num = arrayIndex;
			while (enumerator.MoveNext())
			{
				array.SetValue(enumerator.Entry, num++);
			}
		}

		// Token: 0x060017C1 RID: 6081 RVA: 0x0005B730 File Offset: 0x00059930
		public virtual object Clone()
		{
			return new SortedList(this, this.comparer)
			{
				modificationCount = this.modificationCount
			};
		}

		// Token: 0x060017C2 RID: 6082 RVA: 0x0005B758 File Offset: 0x00059958
		public virtual IList GetKeyList()
		{
			return new SortedList.ListKeys(this);
		}

		// Token: 0x060017C3 RID: 6083 RVA: 0x0005B760 File Offset: 0x00059960
		public virtual IList GetValueList()
		{
			return new SortedList.ListValues(this);
		}

		// Token: 0x060017C4 RID: 6084 RVA: 0x0005B768 File Offset: 0x00059968
		public virtual void RemoveAt(int index)
		{
			SortedList.Slot[] array = this.table;
			int count = this.Count;
			if (index >= 0 && index < count)
			{
				if (index != count - 1)
				{
					Array.Copy(array, index + 1, array, index, count - 1 - index);
				}
				else
				{
					array[index].key = null;
					array[index].value = null;
				}
				this.inUse--;
				this.modificationCount++;
				return;
			}
			throw new ArgumentOutOfRangeException("index out of range");
		}

		// Token: 0x060017C5 RID: 6085 RVA: 0x0005B7F8 File Offset: 0x000599F8
		public virtual int IndexOfKey(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException();
			}
			int num = 0;
			try
			{
				num = this.Find(key);
			}
			catch (Exception)
			{
				throw new InvalidOperationException();
			}
			return num | num >> 31;
		}

		// Token: 0x060017C6 RID: 6086 RVA: 0x0005B850 File Offset: 0x00059A50
		public virtual int IndexOfValue(object value)
		{
			if (this.inUse == 0)
			{
				return -1;
			}
			for (int i = 0; i < this.inUse; i++)
			{
				SortedList.Slot slot = this.table[i];
				if (object.Equals(value, slot.value))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x060017C7 RID: 6087 RVA: 0x0005B8A8 File Offset: 0x00059AA8
		public virtual bool ContainsKey(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException();
			}
			bool result;
			try
			{
				result = this.Contains(key);
			}
			catch (Exception)
			{
				throw new InvalidOperationException();
			}
			return result;
		}

		// Token: 0x060017C8 RID: 6088 RVA: 0x0005B8FC File Offset: 0x00059AFC
		public virtual bool ContainsValue(object value)
		{
			return this.IndexOfValue(value) >= 0;
		}

		// Token: 0x060017C9 RID: 6089 RVA: 0x0005B90C File Offset: 0x00059B0C
		public virtual object GetByIndex(int index)
		{
			if (index >= 0 && index < this.Count)
			{
				return this.table[index].value;
			}
			throw new ArgumentOutOfRangeException("index out of range");
		}

		// Token: 0x060017CA RID: 6090 RVA: 0x0005B940 File Offset: 0x00059B40
		public virtual void SetByIndex(int index, object value)
		{
			if (index >= 0 && index < this.Count)
			{
				this.table[index].value = value;
				return;
			}
			throw new ArgumentOutOfRangeException("index out of range");
		}

		// Token: 0x060017CB RID: 6091 RVA: 0x0005B978 File Offset: 0x00059B78
		public virtual object GetKey(int index)
		{
			if (index >= 0 && index < this.Count)
			{
				return this.table[index].key;
			}
			throw new ArgumentOutOfRangeException("index out of range");
		}

		// Token: 0x060017CC RID: 6092 RVA: 0x0005B9AC File Offset: 0x00059BAC
		public static SortedList Synchronized(SortedList list)
		{
			if (list == null)
			{
				throw new ArgumentNullException(Locale.GetText("Base list is null."));
			}
			return new SortedList.SynchedSortedList(list);
		}

		// Token: 0x060017CD RID: 6093 RVA: 0x0005B9CC File Offset: 0x00059BCC
		public virtual void TrimToSize()
		{
			if (this.Count == 0)
			{
				this.Resize(this.defaultCapacity, false);
			}
			else
			{
				this.Resize(this.Count, true);
			}
		}

		// Token: 0x060017CE RID: 6094 RVA: 0x0005BA04 File Offset: 0x00059C04
		private void Resize(int n, bool copy)
		{
			SortedList.Slot[] sourceArray = this.table;
			SortedList.Slot[] destinationArray = new SortedList.Slot[n];
			if (copy)
			{
				Array.Copy(sourceArray, 0, destinationArray, 0, n);
			}
			this.table = destinationArray;
		}

		// Token: 0x060017CF RID: 6095 RVA: 0x0005BA38 File Offset: 0x00059C38
		private void EnsureCapacity(int n, int free)
		{
			SortedList.Slot[] array = this.table;
			SortedList.Slot[] array2 = null;
			int capacity = this.Capacity;
			bool flag = free >= 0 && free < this.Count;
			if (n > capacity)
			{
				array2 = new SortedList.Slot[n << 1];
			}
			if (array2 != null)
			{
				if (flag)
				{
					if (free > 0)
					{
						Array.Copy(array, 0, array2, 0, free);
					}
					int num = this.Count - free;
					if (num > 0)
					{
						Array.Copy(array, free, array2, free + 1, num);
					}
				}
				else
				{
					Array.Copy(array, array2, this.Count);
				}
				this.table = array2;
			}
			else if (flag)
			{
				Array.Copy(array, free, array, free + 1, this.Count - free);
			}
		}

		// Token: 0x060017D0 RID: 6096 RVA: 0x0005BAF4 File Offset: 0x00059CF4
		private void PutImpl(object key, object value, bool overwrite)
		{
			if (key == null)
			{
				throw new ArgumentNullException("null key");
			}
			SortedList.Slot[] array = this.table;
			int num = -1;
			try
			{
				num = this.Find(key);
			}
			catch (Exception)
			{
				throw new InvalidOperationException();
			}
			if (num >= 0)
			{
				if (!overwrite)
				{
					string text = Locale.GetText("Key '{0}' already exists in list.", new object[]
					{
						key
					});
					throw new ArgumentException(text);
				}
				array[num].value = value;
				this.modificationCount++;
				return;
			}
			else
			{
				num = ~num;
				if (num > this.Capacity + 1)
				{
					throw new Exception(string.Concat(new object[]
					{
						"SortedList::internal error (",
						key,
						", ",
						value,
						") at [",
						num,
						"]"
					}));
				}
				this.EnsureCapacity(this.Count + 1, num);
				array = this.table;
				array[num].key = key;
				array[num].value = value;
				this.inUse++;
				this.modificationCount++;
				return;
			}
		}

		// Token: 0x060017D1 RID: 6097 RVA: 0x0005BC34 File Offset: 0x00059E34
		private object GetImpl(object key)
		{
			int num = this.Find(key);
			if (num >= 0)
			{
				return this.table[num].value;
			}
			return null;
		}

		// Token: 0x060017D2 RID: 6098 RVA: 0x0005BC64 File Offset: 0x00059E64
		private void InitTable(int capacity, bool forceSize)
		{
			if (!forceSize && capacity < this.defaultCapacity)
			{
				capacity = this.defaultCapacity;
			}
			this.table = new SortedList.Slot[capacity];
			this.inUse = 0;
			this.modificationCount = 0;
		}

		// Token: 0x060017D3 RID: 6099 RVA: 0x0005BCA8 File Offset: 0x00059EA8
		private void CopyToArray(Array arr, int i, SortedList.EnumeratorMode mode)
		{
			if (arr == null)
			{
				throw new ArgumentNullException("arr");
			}
			if (i < 0 || i + this.Count > arr.Length)
			{
				throw new ArgumentOutOfRangeException("i");
			}
			IEnumerator enumerator = new SortedList.Enumerator(this, mode);
			while (enumerator.MoveNext())
			{
				object value = enumerator.Current;
				arr.SetValue(value, i++);
			}
		}

		// Token: 0x060017D4 RID: 6100 RVA: 0x0005BD18 File Offset: 0x00059F18
		private int Find(object key)
		{
			SortedList.Slot[] array = this.table;
			int count = this.Count;
			if (count == 0)
			{
				return -1;
			}
			IComparer comparer;
			if (this.comparer == null)
			{
				IComparer @default = Comparer.Default;
				comparer = @default;
			}
			else
			{
				comparer = this.comparer;
			}
			IComparer comparer2 = comparer;
			int i = 0;
			int num = count - 1;
			while (i <= num)
			{
				int num2 = i + num >> 1;
				int num3 = comparer2.Compare(array[num2].key, key);
				if (num3 == 0)
				{
					return num2;
				}
				if (num3 < 0)
				{
					i = num2 + 1;
				}
				else
				{
					num = num2 - 1;
				}
			}
			return ~i;
		}

		// Token: 0x040008A8 RID: 2216
		private static readonly int INITIAL_SIZE = 16;

		// Token: 0x040008A9 RID: 2217
		private int inUse;

		// Token: 0x040008AA RID: 2218
		private int modificationCount;

		// Token: 0x040008AB RID: 2219
		private SortedList.Slot[] table;

		// Token: 0x040008AC RID: 2220
		private IComparer comparer;

		// Token: 0x040008AD RID: 2221
		private int defaultCapacity;

		// Token: 0x020001CC RID: 460
		[Serializable]
		internal struct Slot
		{
			// Token: 0x040008AE RID: 2222
			internal object key;

			// Token: 0x040008AF RID: 2223
			internal object value;
		}

		// Token: 0x020001CD RID: 461
		private enum EnumeratorMode
		{
			// Token: 0x040008B1 RID: 2225
			KEY_MODE,
			// Token: 0x040008B2 RID: 2226
			VALUE_MODE,
			// Token: 0x040008B3 RID: 2227
			ENTRY_MODE
		}

		// Token: 0x020001CE RID: 462
		private sealed class Enumerator : IEnumerator, ICloneable, IDictionaryEnumerator
		{
			// Token: 0x060017D5 RID: 6101 RVA: 0x0005BDB4 File Offset: 0x00059FB4
			public Enumerator(SortedList host, SortedList.EnumeratorMode mode)
			{
				this.host = host;
				this.stamp = host.modificationCount;
				this.size = host.Count;
				this.mode = mode;
				this.Reset();
			}

			// Token: 0x060017D6 RID: 6102 RVA: 0x0005BDF4 File Offset: 0x00059FF4
			public Enumerator(SortedList host) : this(host, SortedList.EnumeratorMode.ENTRY_MODE)
			{
			}

			// Token: 0x060017D8 RID: 6104 RVA: 0x0005BE0C File Offset: 0x0005A00C
			public void Reset()
			{
				if (this.host.modificationCount != this.stamp || this.invalid)
				{
					throw new InvalidOperationException(SortedList.Enumerator.xstr);
				}
				this.pos = -1;
				this.currentKey = null;
				this.currentValue = null;
			}

			// Token: 0x060017D9 RID: 6105 RVA: 0x0005BE5C File Offset: 0x0005A05C
			public bool MoveNext()
			{
				if (this.host.modificationCount != this.stamp || this.invalid)
				{
					throw new InvalidOperationException(SortedList.Enumerator.xstr);
				}
				SortedList.Slot[] table = this.host.table;
				if (++this.pos < this.size)
				{
					SortedList.Slot slot = table[this.pos];
					this.currentKey = slot.key;
					this.currentValue = slot.value;
					return true;
				}
				this.currentKey = null;
				this.currentValue = null;
				return false;
			}

			// Token: 0x170003B5 RID: 949
			// (get) Token: 0x060017DA RID: 6106 RVA: 0x0005BEFC File Offset: 0x0005A0FC
			public DictionaryEntry Entry
			{
				get
				{
					if (this.invalid || this.pos >= this.size || this.pos == -1)
					{
						throw new InvalidOperationException(SortedList.Enumerator.xstr);
					}
					return new DictionaryEntry(this.currentKey, this.currentValue);
				}
			}

			// Token: 0x170003B6 RID: 950
			// (get) Token: 0x060017DB RID: 6107 RVA: 0x0005BF50 File Offset: 0x0005A150
			public object Key
			{
				get
				{
					if (this.invalid || this.pos >= this.size || this.pos == -1)
					{
						throw new InvalidOperationException(SortedList.Enumerator.xstr);
					}
					return this.currentKey;
				}
			}

			// Token: 0x170003B7 RID: 951
			// (get) Token: 0x060017DC RID: 6108 RVA: 0x0005BF8C File Offset: 0x0005A18C
			public object Value
			{
				get
				{
					if (this.invalid || this.pos >= this.size || this.pos == -1)
					{
						throw new InvalidOperationException(SortedList.Enumerator.xstr);
					}
					return this.currentValue;
				}
			}

			// Token: 0x170003B8 RID: 952
			// (get) Token: 0x060017DD RID: 6109 RVA: 0x0005BFC8 File Offset: 0x0005A1C8
			public object Current
			{
				get
				{
					if (this.invalid || this.pos >= this.size || this.pos == -1)
					{
						throw new InvalidOperationException(SortedList.Enumerator.xstr);
					}
					switch (this.mode)
					{
					case SortedList.EnumeratorMode.KEY_MODE:
						return this.currentKey;
					case SortedList.EnumeratorMode.VALUE_MODE:
						return this.currentValue;
					case SortedList.EnumeratorMode.ENTRY_MODE:
						return this.Entry;
					default:
						throw new NotSupportedException(this.mode + " is not a supported mode.");
					}
				}
			}

			// Token: 0x060017DE RID: 6110 RVA: 0x0005C05C File Offset: 0x0005A25C
			public object Clone()
			{
				return new SortedList.Enumerator(this.host, this.mode)
				{
					stamp = this.stamp,
					pos = this.pos,
					size = this.size,
					currentKey = this.currentKey,
					currentValue = this.currentValue,
					invalid = this.invalid
				};
			}

			// Token: 0x040008B4 RID: 2228
			private SortedList host;

			// Token: 0x040008B5 RID: 2229
			private int stamp;

			// Token: 0x040008B6 RID: 2230
			private int pos;

			// Token: 0x040008B7 RID: 2231
			private int size;

			// Token: 0x040008B8 RID: 2232
			private SortedList.EnumeratorMode mode;

			// Token: 0x040008B9 RID: 2233
			private object currentKey;

			// Token: 0x040008BA RID: 2234
			private object currentValue;

			// Token: 0x040008BB RID: 2235
			private bool invalid;

			// Token: 0x040008BC RID: 2236
			private static readonly string xstr = "SortedList.Enumerator: snapshot out of sync.";
		}

		// Token: 0x020001CF RID: 463
		[Serializable]
		private class ListKeys : IEnumerable, ICollection, IList
		{
			// Token: 0x060017DF RID: 6111 RVA: 0x0005C0C4 File Offset: 0x0005A2C4
			public ListKeys(SortedList host)
			{
				if (host == null)
				{
					throw new ArgumentNullException();
				}
				this.host = host;
			}

			// Token: 0x170003B9 RID: 953
			// (get) Token: 0x060017E0 RID: 6112 RVA: 0x0005C0E0 File Offset: 0x0005A2E0
			public virtual int Count
			{
				get
				{
					return this.host.Count;
				}
			}

			// Token: 0x170003BA RID: 954
			// (get) Token: 0x060017E1 RID: 6113 RVA: 0x0005C0F0 File Offset: 0x0005A2F0
			public virtual bool IsSynchronized
			{
				get
				{
					return this.host.IsSynchronized;
				}
			}

			// Token: 0x170003BB RID: 955
			// (get) Token: 0x060017E2 RID: 6114 RVA: 0x0005C100 File Offset: 0x0005A300
			public virtual object SyncRoot
			{
				get
				{
					return this.host.SyncRoot;
				}
			}

			// Token: 0x060017E3 RID: 6115 RVA: 0x0005C110 File Offset: 0x0005A310
			public virtual void CopyTo(Array array, int arrayIndex)
			{
				this.host.CopyToArray(array, arrayIndex, SortedList.EnumeratorMode.KEY_MODE);
			}

			// Token: 0x170003BC RID: 956
			// (get) Token: 0x060017E4 RID: 6116 RVA: 0x0005C120 File Offset: 0x0005A320
			public virtual bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x170003BD RID: 957
			// (get) Token: 0x060017E5 RID: 6117 RVA: 0x0005C124 File Offset: 0x0005A324
			public virtual bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x170003BE RID: 958
			public virtual object this[int index]
			{
				get
				{
					return this.host.GetKey(index);
				}
				set
				{
					throw new NotSupportedException("attempt to modify a key");
				}
			}

			// Token: 0x060017E8 RID: 6120 RVA: 0x0005C144 File Offset: 0x0005A344
			public virtual int Add(object value)
			{
				throw new NotSupportedException("IList::Add not supported");
			}

			// Token: 0x060017E9 RID: 6121 RVA: 0x0005C150 File Offset: 0x0005A350
			public virtual void Clear()
			{
				throw new NotSupportedException("IList::Clear not supported");
			}

			// Token: 0x060017EA RID: 6122 RVA: 0x0005C15C File Offset: 0x0005A35C
			public virtual bool Contains(object key)
			{
				return this.host.Contains(key);
			}

			// Token: 0x060017EB RID: 6123 RVA: 0x0005C16C File Offset: 0x0005A36C
			public virtual int IndexOf(object key)
			{
				return this.host.IndexOfKey(key);
			}

			// Token: 0x060017EC RID: 6124 RVA: 0x0005C17C File Offset: 0x0005A37C
			public virtual void Insert(int index, object value)
			{
				throw new NotSupportedException("IList::Insert not supported");
			}

			// Token: 0x060017ED RID: 6125 RVA: 0x0005C188 File Offset: 0x0005A388
			public virtual void Remove(object value)
			{
				throw new NotSupportedException("IList::Remove not supported");
			}

			// Token: 0x060017EE RID: 6126 RVA: 0x0005C194 File Offset: 0x0005A394
			public virtual void RemoveAt(int index)
			{
				throw new NotSupportedException("IList::RemoveAt not supported");
			}

			// Token: 0x060017EF RID: 6127 RVA: 0x0005C1A0 File Offset: 0x0005A3A0
			public virtual IEnumerator GetEnumerator()
			{
				return new SortedList.Enumerator(this.host, SortedList.EnumeratorMode.KEY_MODE);
			}

			// Token: 0x040008BD RID: 2237
			private SortedList host;
		}

		// Token: 0x020001D0 RID: 464
		[Serializable]
		private class ListValues : IEnumerable, ICollection, IList
		{
			// Token: 0x060017F0 RID: 6128 RVA: 0x0005C1B0 File Offset: 0x0005A3B0
			public ListValues(SortedList host)
			{
				if (host == null)
				{
					throw new ArgumentNullException();
				}
				this.host = host;
			}

			// Token: 0x170003BF RID: 959
			// (get) Token: 0x060017F1 RID: 6129 RVA: 0x0005C1CC File Offset: 0x0005A3CC
			public virtual int Count
			{
				get
				{
					return this.host.Count;
				}
			}

			// Token: 0x170003C0 RID: 960
			// (get) Token: 0x060017F2 RID: 6130 RVA: 0x0005C1DC File Offset: 0x0005A3DC
			public virtual bool IsSynchronized
			{
				get
				{
					return this.host.IsSynchronized;
				}
			}

			// Token: 0x170003C1 RID: 961
			// (get) Token: 0x060017F3 RID: 6131 RVA: 0x0005C1EC File Offset: 0x0005A3EC
			public virtual object SyncRoot
			{
				get
				{
					return this.host.SyncRoot;
				}
			}

			// Token: 0x060017F4 RID: 6132 RVA: 0x0005C1FC File Offset: 0x0005A3FC
			public virtual void CopyTo(Array array, int arrayIndex)
			{
				this.host.CopyToArray(array, arrayIndex, SortedList.EnumeratorMode.VALUE_MODE);
			}

			// Token: 0x170003C2 RID: 962
			// (get) Token: 0x060017F5 RID: 6133 RVA: 0x0005C20C File Offset: 0x0005A40C
			public virtual bool IsFixedSize
			{
				get
				{
					return true;
				}
			}

			// Token: 0x170003C3 RID: 963
			// (get) Token: 0x060017F6 RID: 6134 RVA: 0x0005C210 File Offset: 0x0005A410
			public virtual bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x170003C4 RID: 964
			public virtual object this[int index]
			{
				get
				{
					return this.host.GetByIndex(index);
				}
				set
				{
					throw new NotSupportedException("This operation is not supported on GetValueList return");
				}
			}

			// Token: 0x060017F9 RID: 6137 RVA: 0x0005C230 File Offset: 0x0005A430
			public virtual int Add(object value)
			{
				throw new NotSupportedException("IList::Add not supported");
			}

			// Token: 0x060017FA RID: 6138 RVA: 0x0005C23C File Offset: 0x0005A43C
			public virtual void Clear()
			{
				throw new NotSupportedException("IList::Clear not supported");
			}

			// Token: 0x060017FB RID: 6139 RVA: 0x0005C248 File Offset: 0x0005A448
			public virtual bool Contains(object value)
			{
				return this.host.ContainsValue(value);
			}

			// Token: 0x060017FC RID: 6140 RVA: 0x0005C258 File Offset: 0x0005A458
			public virtual int IndexOf(object value)
			{
				return this.host.IndexOfValue(value);
			}

			// Token: 0x060017FD RID: 6141 RVA: 0x0005C268 File Offset: 0x0005A468
			public virtual void Insert(int index, object value)
			{
				throw new NotSupportedException("IList::Insert not supported");
			}

			// Token: 0x060017FE RID: 6142 RVA: 0x0005C274 File Offset: 0x0005A474
			public virtual void Remove(object value)
			{
				throw new NotSupportedException("IList::Remove not supported");
			}

			// Token: 0x060017FF RID: 6143 RVA: 0x0005C280 File Offset: 0x0005A480
			public virtual void RemoveAt(int index)
			{
				throw new NotSupportedException("IList::RemoveAt not supported");
			}

			// Token: 0x06001800 RID: 6144 RVA: 0x0005C28C File Offset: 0x0005A48C
			public virtual IEnumerator GetEnumerator()
			{
				return new SortedList.Enumerator(this.host, SortedList.EnumeratorMode.VALUE_MODE);
			}

			// Token: 0x040008BE RID: 2238
			private SortedList host;
		}

		// Token: 0x020001D1 RID: 465
		private class SynchedSortedList : SortedList
		{
			// Token: 0x06001801 RID: 6145 RVA: 0x0005C29C File Offset: 0x0005A49C
			public SynchedSortedList(SortedList host)
			{
				if (host == null)
				{
					throw new ArgumentNullException();
				}
				this.host = host;
			}

			// Token: 0x170003C5 RID: 965
			// (get) Token: 0x06001802 RID: 6146 RVA: 0x0005C2B8 File Offset: 0x0005A4B8
			// (set) Token: 0x06001803 RID: 6147 RVA: 0x0005C314 File Offset: 0x0005A514
			public override int Capacity
			{
				get
				{
					object syncRoot = this.host.SyncRoot;
					int capacity;
					lock (syncRoot)
					{
						capacity = this.host.Capacity;
					}
					return capacity;
				}
				set
				{
					object syncRoot = this.host.SyncRoot;
					lock (syncRoot)
					{
						this.host.Capacity = value;
					}
				}
			}

			// Token: 0x170003C6 RID: 966
			// (get) Token: 0x06001804 RID: 6148 RVA: 0x0005C368 File Offset: 0x0005A568
			public override int Count
			{
				get
				{
					return this.host.Count;
				}
			}

			// Token: 0x170003C7 RID: 967
			// (get) Token: 0x06001805 RID: 6149 RVA: 0x0005C378 File Offset: 0x0005A578
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x170003C8 RID: 968
			// (get) Token: 0x06001806 RID: 6150 RVA: 0x0005C37C File Offset: 0x0005A57C
			public override object SyncRoot
			{
				get
				{
					return this.host.SyncRoot;
				}
			}

			// Token: 0x170003C9 RID: 969
			// (get) Token: 0x06001807 RID: 6151 RVA: 0x0005C38C File Offset: 0x0005A58C
			public override bool IsFixedSize
			{
				get
				{
					return this.host.IsFixedSize;
				}
			}

			// Token: 0x170003CA RID: 970
			// (get) Token: 0x06001808 RID: 6152 RVA: 0x0005C39C File Offset: 0x0005A59C
			public override bool IsReadOnly
			{
				get
				{
					return this.host.IsReadOnly;
				}
			}

			// Token: 0x170003CB RID: 971
			// (get) Token: 0x06001809 RID: 6153 RVA: 0x0005C3AC File Offset: 0x0005A5AC
			public override ICollection Keys
			{
				get
				{
					ICollection result = null;
					object syncRoot = this.host.SyncRoot;
					lock (syncRoot)
					{
						result = this.host.Keys;
					}
					return result;
				}
			}

			// Token: 0x170003CC RID: 972
			// (get) Token: 0x0600180A RID: 6154 RVA: 0x0005C404 File Offset: 0x0005A604
			public override ICollection Values
			{
				get
				{
					ICollection result = null;
					object syncRoot = this.host.SyncRoot;
					lock (syncRoot)
					{
						result = this.host.Values;
					}
					return result;
				}
			}

			// Token: 0x170003CD RID: 973
			public override object this[object key]
			{
				get
				{
					object syncRoot = this.host.SyncRoot;
					object impl;
					lock (syncRoot)
					{
						impl = this.host.GetImpl(key);
					}
					return impl;
				}
				set
				{
					object syncRoot = this.host.SyncRoot;
					lock (syncRoot)
					{
						this.host.PutImpl(key, value, true);
					}
				}
			}

			// Token: 0x0600180D RID: 6157 RVA: 0x0005C510 File Offset: 0x0005A710
			public override void CopyTo(Array array, int arrayIndex)
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.CopyTo(array, arrayIndex);
				}
			}

			// Token: 0x0600180E RID: 6158 RVA: 0x0005C564 File Offset: 0x0005A764
			public override void Add(object key, object value)
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.PutImpl(key, value, false);
				}
			}

			// Token: 0x0600180F RID: 6159 RVA: 0x0005C5BC File Offset: 0x0005A7BC
			public override void Clear()
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.Clear();
				}
			}

			// Token: 0x06001810 RID: 6160 RVA: 0x0005C610 File Offset: 0x0005A810
			public override bool Contains(object key)
			{
				object syncRoot = this.host.SyncRoot;
				bool result;
				lock (syncRoot)
				{
					result = (this.host.Find(key) >= 0);
				}
				return result;
			}

			// Token: 0x06001811 RID: 6161 RVA: 0x0005C670 File Offset: 0x0005A870
			public override IDictionaryEnumerator GetEnumerator()
			{
				object syncRoot = this.host.SyncRoot;
				IDictionaryEnumerator enumerator;
				lock (syncRoot)
				{
					enumerator = this.host.GetEnumerator();
				}
				return enumerator;
			}

			// Token: 0x06001812 RID: 6162 RVA: 0x0005C6CC File Offset: 0x0005A8CC
			public override void Remove(object key)
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.Remove(key);
				}
			}

			// Token: 0x06001813 RID: 6163 RVA: 0x0005C720 File Offset: 0x0005A920
			public override bool ContainsKey(object key)
			{
				object syncRoot = this.host.SyncRoot;
				bool result;
				lock (syncRoot)
				{
					result = this.host.Contains(key);
				}
				return result;
			}

			// Token: 0x06001814 RID: 6164 RVA: 0x0005C77C File Offset: 0x0005A97C
			public override bool ContainsValue(object value)
			{
				object syncRoot = this.host.SyncRoot;
				bool result;
				lock (syncRoot)
				{
					result = this.host.ContainsValue(value);
				}
				return result;
			}

			// Token: 0x06001815 RID: 6165 RVA: 0x0005C7D8 File Offset: 0x0005A9D8
			public override object Clone()
			{
				object syncRoot = this.host.SyncRoot;
				object result;
				lock (syncRoot)
				{
					result = (this.host.Clone() as SortedList);
				}
				return result;
			}

			// Token: 0x06001816 RID: 6166 RVA: 0x0005C838 File Offset: 0x0005AA38
			public override object GetByIndex(int index)
			{
				object syncRoot = this.host.SyncRoot;
				object byIndex;
				lock (syncRoot)
				{
					byIndex = this.host.GetByIndex(index);
				}
				return byIndex;
			}

			// Token: 0x06001817 RID: 6167 RVA: 0x0005C894 File Offset: 0x0005AA94
			public override object GetKey(int index)
			{
				object syncRoot = this.host.SyncRoot;
				object key;
				lock (syncRoot)
				{
					key = this.host.GetKey(index);
				}
				return key;
			}

			// Token: 0x06001818 RID: 6168 RVA: 0x0005C8F0 File Offset: 0x0005AAF0
			public override IList GetKeyList()
			{
				object syncRoot = this.host.SyncRoot;
				IList result;
				lock (syncRoot)
				{
					result = new SortedList.ListKeys(this.host);
				}
				return result;
			}

			// Token: 0x06001819 RID: 6169 RVA: 0x0005C94C File Offset: 0x0005AB4C
			public override IList GetValueList()
			{
				object syncRoot = this.host.SyncRoot;
				IList result;
				lock (syncRoot)
				{
					result = new SortedList.ListValues(this.host);
				}
				return result;
			}

			// Token: 0x0600181A RID: 6170 RVA: 0x0005C9A8 File Offset: 0x0005ABA8
			public override void RemoveAt(int index)
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.RemoveAt(index);
				}
			}

			// Token: 0x0600181B RID: 6171 RVA: 0x0005C9FC File Offset: 0x0005ABFC
			public override int IndexOfKey(object key)
			{
				object syncRoot = this.host.SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.host.IndexOfKey(key);
				}
				return result;
			}

			// Token: 0x0600181C RID: 6172 RVA: 0x0005CA58 File Offset: 0x0005AC58
			public override int IndexOfValue(object val)
			{
				object syncRoot = this.host.SyncRoot;
				int result;
				lock (syncRoot)
				{
					result = this.host.IndexOfValue(val);
				}
				return result;
			}

			// Token: 0x0600181D RID: 6173 RVA: 0x0005CAB4 File Offset: 0x0005ACB4
			public override void SetByIndex(int index, object value)
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.SetByIndex(index, value);
				}
			}

			// Token: 0x0600181E RID: 6174 RVA: 0x0005CB08 File Offset: 0x0005AD08
			public override void TrimToSize()
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.TrimToSize();
				}
			}

			// Token: 0x040008BF RID: 2239
			private SortedList host;
		}
	}
}
