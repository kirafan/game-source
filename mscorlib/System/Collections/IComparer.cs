﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001C2 RID: 450
	[ComVisible(true)]
	public interface IComparer
	{
		// Token: 0x06001768 RID: 5992
		int Compare(object x, object y);
	}
}
