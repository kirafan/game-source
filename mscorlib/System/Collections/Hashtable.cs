﻿using System;
using System.Diagnostics;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Collections
{
	// Token: 0x020001BA RID: 442
	[ComVisible(true)]
	[DebuggerTypeProxy(typeof(CollectionDebuggerView))]
	[DebuggerDisplay("Count={Count}")]
	[Serializable]
	public class Hashtable : IEnumerable, ICloneable, ISerializable, ICollection, IDictionary, IDeserializationCallback
	{
		// Token: 0x06001701 RID: 5889 RVA: 0x00059140 File Offset: 0x00057340
		public Hashtable() : this(0, 1f)
		{
		}

		// Token: 0x06001702 RID: 5890 RVA: 0x00059150 File Offset: 0x00057350
		[Obsolete("Please use Hashtable(int, float, IEqualityComparer) instead")]
		public Hashtable(int capacity, float loadFactor, IHashCodeProvider hcp, IComparer comparer)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity", "negative capacity");
			}
			if (loadFactor < 0.1f || loadFactor > 1f || float.IsNaN(loadFactor))
			{
				throw new ArgumentOutOfRangeException("loadFactor", "load factor");
			}
			if (capacity == 0)
			{
				capacity++;
			}
			this.loadFactor = 0.75f * loadFactor;
			double num = (double)((float)capacity / this.loadFactor);
			if (num > 2147483647.0)
			{
				throw new ArgumentException("Size is too big");
			}
			int num2 = (int)num;
			num2 = Hashtable.ToPrime(num2);
			this.SetTable(new Hashtable.Slot[num2], new int[num2]);
			this.hcp = hcp;
			this.comparer = comparer;
			this.inUse = 0;
			this.modificationCount = 0;
		}

		// Token: 0x06001703 RID: 5891 RVA: 0x00059224 File Offset: 0x00057424
		public Hashtable(int capacity, float loadFactor) : this(capacity, loadFactor, null, null)
		{
		}

		// Token: 0x06001704 RID: 5892 RVA: 0x00059230 File Offset: 0x00057430
		public Hashtable(int capacity) : this(capacity, 1f)
		{
		}

		// Token: 0x06001705 RID: 5893 RVA: 0x00059240 File Offset: 0x00057440
		internal Hashtable(Hashtable source)
		{
			this.inUse = source.inUse;
			this.loadFactor = source.loadFactor;
			this.table = (Hashtable.Slot[])source.table.Clone();
			this.hashes = (int[])source.hashes.Clone();
			this.threshold = source.threshold;
			this.hcpRef = source.hcpRef;
			this.comparerRef = source.comparerRef;
			this.equalityComparer = source.equalityComparer;
		}

		// Token: 0x06001706 RID: 5894 RVA: 0x000592C8 File Offset: 0x000574C8
		[Obsolete("Please use Hashtable(int, IEqualityComparer) instead")]
		public Hashtable(int capacity, IHashCodeProvider hcp, IComparer comparer) : this(capacity, 1f, hcp, comparer)
		{
		}

		// Token: 0x06001707 RID: 5895 RVA: 0x000592D8 File Offset: 0x000574D8
		[Obsolete("Please use Hashtable(IDictionary, float, IEqualityComparer) instead")]
		public Hashtable(IDictionary d, float loadFactor, IHashCodeProvider hcp, IComparer comparer) : this((d == null) ? 0 : d.Count, loadFactor, hcp, comparer)
		{
			if (d == null)
			{
				throw new ArgumentNullException("dictionary");
			}
			IDictionaryEnumerator enumerator = d.GetEnumerator();
			while (enumerator.MoveNext())
			{
				this.Add(enumerator.Key, enumerator.Value);
			}
		}

		// Token: 0x06001708 RID: 5896 RVA: 0x0005933C File Offset: 0x0005753C
		public Hashtable(IDictionary d, float loadFactor) : this(d, loadFactor, null, null)
		{
		}

		// Token: 0x06001709 RID: 5897 RVA: 0x00059348 File Offset: 0x00057548
		public Hashtable(IDictionary d) : this(d, 1f)
		{
		}

		// Token: 0x0600170A RID: 5898 RVA: 0x00059358 File Offset: 0x00057558
		[Obsolete("Please use Hashtable(IDictionary, IEqualityComparer) instead")]
		public Hashtable(IDictionary d, IHashCodeProvider hcp, IComparer comparer) : this(d, 1f, hcp, comparer)
		{
		}

		// Token: 0x0600170B RID: 5899 RVA: 0x00059368 File Offset: 0x00057568
		[Obsolete("Please use Hashtable(IEqualityComparer) instead")]
		public Hashtable(IHashCodeProvider hcp, IComparer comparer) : this(1, 1f, hcp, comparer)
		{
		}

		// Token: 0x0600170C RID: 5900 RVA: 0x00059378 File Offset: 0x00057578
		protected Hashtable(SerializationInfo info, StreamingContext context)
		{
			this.serializationInfo = info;
		}

		// Token: 0x0600170D RID: 5901 RVA: 0x00059388 File Offset: 0x00057588
		public Hashtable(IDictionary d, IEqualityComparer equalityComparer) : this(d, 1f, equalityComparer)
		{
		}

		// Token: 0x0600170E RID: 5902 RVA: 0x00059398 File Offset: 0x00057598
		public Hashtable(IDictionary d, float loadFactor, IEqualityComparer equalityComparer) : this(d, loadFactor)
		{
			this.equalityComparer = equalityComparer;
		}

		// Token: 0x0600170F RID: 5903 RVA: 0x000593AC File Offset: 0x000575AC
		public Hashtable(IEqualityComparer equalityComparer) : this(1, 1f, equalityComparer)
		{
		}

		// Token: 0x06001710 RID: 5904 RVA: 0x000593BC File Offset: 0x000575BC
		public Hashtable(int capacity, IEqualityComparer equalityComparer) : this(capacity, 1f, equalityComparer)
		{
		}

		// Token: 0x06001711 RID: 5905 RVA: 0x000593CC File Offset: 0x000575CC
		public Hashtable(int capacity, float loadFactor, IEqualityComparer equalityComparer) : this(capacity, loadFactor)
		{
			this.equalityComparer = equalityComparer;
		}

		// Token: 0x06001713 RID: 5907 RVA: 0x000593FC File Offset: 0x000575FC
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new Hashtable.Enumerator(this, Hashtable.EnumeratorMode.ENTRY_MODE);
		}

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06001715 RID: 5909 RVA: 0x00059414 File Offset: 0x00057614
		// (set) Token: 0x06001714 RID: 5908 RVA: 0x00059408 File Offset: 0x00057608
		[Obsolete("Please use EqualityComparer property.")]
		protected IComparer comparer
		{
			get
			{
				return this.comparerRef;
			}
			set
			{
				this.comparerRef = value;
			}
		}

		// Token: 0x1700037D RID: 893
		// (get) Token: 0x06001717 RID: 5911 RVA: 0x00059428 File Offset: 0x00057628
		// (set) Token: 0x06001716 RID: 5910 RVA: 0x0005941C File Offset: 0x0005761C
		[Obsolete("Please use EqualityComparer property.")]
		protected IHashCodeProvider hcp
		{
			get
			{
				return this.hcpRef;
			}
			set
			{
				this.hcpRef = value;
			}
		}

		// Token: 0x1700037E RID: 894
		// (get) Token: 0x06001718 RID: 5912 RVA: 0x00059430 File Offset: 0x00057630
		protected IEqualityComparer EqualityComparer
		{
			get
			{
				return this.equalityComparer;
			}
		}

		// Token: 0x1700037F RID: 895
		// (get) Token: 0x06001719 RID: 5913 RVA: 0x00059438 File Offset: 0x00057638
		public virtual int Count
		{
			get
			{
				return this.inUse;
			}
		}

		// Token: 0x17000380 RID: 896
		// (get) Token: 0x0600171A RID: 5914 RVA: 0x00059440 File Offset: 0x00057640
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000381 RID: 897
		// (get) Token: 0x0600171B RID: 5915 RVA: 0x00059444 File Offset: 0x00057644
		public virtual object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000382 RID: 898
		// (get) Token: 0x0600171C RID: 5916 RVA: 0x00059448 File Offset: 0x00057648
		public virtual bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000383 RID: 899
		// (get) Token: 0x0600171D RID: 5917 RVA: 0x0005944C File Offset: 0x0005764C
		public virtual bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000384 RID: 900
		// (get) Token: 0x0600171E RID: 5918 RVA: 0x00059450 File Offset: 0x00057650
		public virtual ICollection Keys
		{
			get
			{
				if (this.hashKeys == null)
				{
					this.hashKeys = new Hashtable.HashKeys(this);
				}
				return this.hashKeys;
			}
		}

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x0600171F RID: 5919 RVA: 0x00059470 File Offset: 0x00057670
		public virtual ICollection Values
		{
			get
			{
				if (this.hashValues == null)
				{
					this.hashValues = new Hashtable.HashValues(this);
				}
				return this.hashValues;
			}
		}

		// Token: 0x17000386 RID: 902
		public virtual object this[object key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException("key", "null key");
				}
				Hashtable.Slot[] array = this.table;
				int[] array2 = this.hashes;
				uint num = (uint)array.Length;
				int num2 = this.GetHash(key) & int.MaxValue;
				uint num3 = (uint)num2;
				uint num4 = (uint)(((num2 >> 5) + 1) % (int)(num - 1U) + 1);
				for (uint num5 = num; num5 > 0U; num5 -= 1U)
				{
					num3 %= num;
					Hashtable.Slot slot = array[(int)((UIntPtr)num3)];
					int num6 = array2[(int)((UIntPtr)num3)];
					object key2 = slot.key;
					if (key2 == null)
					{
						break;
					}
					if (key2 == key || ((num6 & 2147483647) == num2 && this.KeyEquals(key, key2)))
					{
						return slot.value;
					}
					if ((num6 & -2147483648) == 0)
					{
						break;
					}
					num3 += num4;
				}
				return null;
			}
			set
			{
				this.PutImpl(key, value, true);
			}
		}

		// Token: 0x06001722 RID: 5922 RVA: 0x00059580 File Offset: 0x00057780
		public virtual void CopyTo(Array array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (arrayIndex < 0)
			{
				throw new ArgumentOutOfRangeException("arrayIndex");
			}
			if (array.Rank > 1)
			{
				throw new ArgumentException("array is multidimensional");
			}
			if (array.Length > 0 && arrayIndex >= array.Length)
			{
				throw new ArgumentException("arrayIndex is equal to or greater than array.Length");
			}
			if (arrayIndex + this.inUse > array.Length)
			{
				throw new ArgumentException("Not enough room from arrayIndex to end of array for this Hashtable");
			}
			IDictionaryEnumerator enumerator = this.GetEnumerator();
			int num = arrayIndex;
			while (enumerator.MoveNext())
			{
				array.SetValue(enumerator.Entry, num++);
			}
		}

		// Token: 0x06001723 RID: 5923 RVA: 0x00059638 File Offset: 0x00057838
		public virtual void Add(object key, object value)
		{
			this.PutImpl(key, value, false);
		}

		// Token: 0x06001724 RID: 5924 RVA: 0x00059644 File Offset: 0x00057844
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public virtual void Clear()
		{
			for (int i = 0; i < this.table.Length; i++)
			{
				this.table[i].key = null;
				this.table[i].value = null;
				this.hashes[i] = 0;
			}
			this.inUse = 0;
			this.modificationCount++;
		}

		// Token: 0x06001725 RID: 5925 RVA: 0x000596AC File Offset: 0x000578AC
		public virtual bool Contains(object key)
		{
			return this.Find(key) >= 0;
		}

		// Token: 0x06001726 RID: 5926 RVA: 0x000596BC File Offset: 0x000578BC
		public virtual IDictionaryEnumerator GetEnumerator()
		{
			return new Hashtable.Enumerator(this, Hashtable.EnumeratorMode.ENTRY_MODE);
		}

		// Token: 0x06001727 RID: 5927 RVA: 0x000596C8 File Offset: 0x000578C8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public virtual void Remove(object key)
		{
			int num = this.Find(key);
			if (num >= 0)
			{
				Hashtable.Slot[] array = this.table;
				int num2 = this.hashes[num];
				num2 &= int.MinValue;
				this.hashes[num] = num2;
				array[num].key = ((num2 == 0) ? null : Hashtable.KeyMarker.Removed);
				array[num].value = null;
				this.inUse--;
				this.modificationCount++;
			}
		}

		// Token: 0x06001728 RID: 5928 RVA: 0x0005974C File Offset: 0x0005794C
		public virtual bool ContainsKey(object key)
		{
			return this.Contains(key);
		}

		// Token: 0x06001729 RID: 5929 RVA: 0x00059758 File Offset: 0x00057958
		public virtual bool ContainsValue(object value)
		{
			int num = this.table.Length;
			Hashtable.Slot[] array = this.table;
			if (value == null)
			{
				for (int i = 0; i < num; i++)
				{
					Hashtable.Slot slot = array[i];
					if (slot.key != null && slot.key != Hashtable.KeyMarker.Removed && slot.value == null)
					{
						return true;
					}
				}
			}
			else
			{
				for (int j = 0; j < num; j++)
				{
					Hashtable.Slot slot2 = array[j];
					if (slot2.key != null && slot2.key != Hashtable.KeyMarker.Removed && value.Equals(slot2.value))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x0600172A RID: 5930 RVA: 0x00059824 File Offset: 0x00057A24
		public virtual object Clone()
		{
			return new Hashtable(this);
		}

		// Token: 0x0600172B RID: 5931 RVA: 0x0005982C File Offset: 0x00057A2C
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("LoadFactor", this.loadFactor);
			info.AddValue("Version", this.modificationCount);
			if (this.equalityComparer != null)
			{
				info.AddValue("KeyComparer", this.equalityComparer);
			}
			else
			{
				info.AddValue("Comparer", this.comparerRef);
			}
			if (this.hcpRef != null)
			{
				info.AddValue("HashCodeProvider", this.hcpRef);
			}
			info.AddValue("HashSize", this.table.Length);
			object[] array = new object[this.inUse];
			this.CopyToArray(array, 0, Hashtable.EnumeratorMode.KEY_MODE);
			object[] array2 = new object[this.inUse];
			this.CopyToArray(array2, 0, Hashtable.EnumeratorMode.VALUE_MODE);
			info.AddValue("Keys", array);
			info.AddValue("Values", array2);
			info.AddValue("equalityComparer", this.equalityComparer);
		}

		// Token: 0x0600172C RID: 5932 RVA: 0x00059920 File Offset: 0x00057B20
		[MonoTODO("Serialize equalityComparer")]
		public virtual void OnDeserialization(object sender)
		{
			if (this.serializationInfo == null)
			{
				return;
			}
			this.loadFactor = (float)this.serializationInfo.GetValue("LoadFactor", typeof(float));
			this.modificationCount = (int)this.serializationInfo.GetValue("Version", typeof(int));
			try
			{
				this.equalityComparer = (IEqualityComparer)this.serializationInfo.GetValue("KeyComparer", typeof(object));
			}
			catch
			{
			}
			if (this.equalityComparer == null)
			{
				this.comparerRef = (IComparer)this.serializationInfo.GetValue("Comparer", typeof(object));
			}
			try
			{
				this.hcpRef = (IHashCodeProvider)this.serializationInfo.GetValue("HashCodeProvider", typeof(object));
			}
			catch
			{
			}
			int num = (int)this.serializationInfo.GetValue("HashSize", typeof(int));
			object[] array = (object[])this.serializationInfo.GetValue("Keys", typeof(object[]));
			object[] array2 = (object[])this.serializationInfo.GetValue("Values", typeof(object[]));
			if (array.Length != array2.Length)
			{
				throw new SerializationException("Keys and values of uneven size");
			}
			num = Hashtable.ToPrime(num);
			this.SetTable(new Hashtable.Slot[num], new int[num]);
			for (int i = 0; i < array.Length; i++)
			{
				this.Add(array[i], array2[i]);
			}
			this.AdjustThreshold();
			this.serializationInfo = null;
		}

		// Token: 0x0600172D RID: 5933 RVA: 0x00059B04 File Offset: 0x00057D04
		public static Hashtable Synchronized(Hashtable table)
		{
			if (table == null)
			{
				throw new ArgumentNullException("table");
			}
			return new Hashtable.SyncHashtable(table);
		}

		// Token: 0x0600172E RID: 5934 RVA: 0x00059B20 File Offset: 0x00057D20
		protected virtual int GetHash(object key)
		{
			if (this.equalityComparer != null)
			{
				return this.equalityComparer.GetHashCode(key);
			}
			if (this.hcpRef == null)
			{
				return key.GetHashCode();
			}
			return this.hcpRef.GetHashCode(key);
		}

		// Token: 0x0600172F RID: 5935 RVA: 0x00059B64 File Offset: 0x00057D64
		protected virtual bool KeyEquals(object item, object key)
		{
			if (key == Hashtable.KeyMarker.Removed)
			{
				return false;
			}
			if (this.equalityComparer != null)
			{
				return this.equalityComparer.Equals(item, key);
			}
			if (this.comparerRef == null)
			{
				return item.Equals(key);
			}
			return this.comparerRef.Compare(item, key) == 0;
		}

		// Token: 0x06001730 RID: 5936 RVA: 0x00059BBC File Offset: 0x00057DBC
		private void AdjustThreshold()
		{
			int num = this.table.Length;
			this.threshold = (int)((float)num * this.loadFactor);
			if (this.threshold >= num)
			{
				this.threshold = num - 1;
			}
		}

		// Token: 0x06001731 RID: 5937 RVA: 0x00059BF8 File Offset: 0x00057DF8
		private void SetTable(Hashtable.Slot[] table, int[] hashes)
		{
			if (table == null)
			{
				throw new ArgumentNullException("table");
			}
			this.table = table;
			this.hashes = hashes;
			this.AdjustThreshold();
		}

		// Token: 0x06001732 RID: 5938 RVA: 0x00059C20 File Offset: 0x00057E20
		private int Find(object key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key", "null key");
			}
			Hashtable.Slot[] array = this.table;
			int[] array2 = this.hashes;
			uint num = (uint)array.Length;
			int num2 = this.GetHash(key) & int.MaxValue;
			uint num3 = (uint)num2;
			uint num4 = (uint)(((num2 >> 5) + 1) % (int)(num - 1U) + 1);
			for (uint num5 = num; num5 > 0U; num5 -= 1U)
			{
				num3 %= num;
				Hashtable.Slot slot = array[(int)((UIntPtr)num3)];
				int num6 = array2[(int)((UIntPtr)num3)];
				object key2 = slot.key;
				if (key2 == null)
				{
					break;
				}
				if (key2 == key || ((num6 & 2147483647) == num2 && this.KeyEquals(key, key2)))
				{
					return (int)num3;
				}
				if ((num6 & -2147483648) == 0)
				{
					break;
				}
				num3 += num4;
			}
			return -1;
		}

		// Token: 0x06001733 RID: 5939 RVA: 0x00059CFC File Offset: 0x00057EFC
		private void Rehash()
		{
			int num = this.table.Length;
			uint num2 = (uint)Hashtable.ToPrime(num << 1 | 1);
			Hashtable.Slot[] array = new Hashtable.Slot[num2];
			Hashtable.Slot[] array2 = this.table;
			int[] array3 = new int[num2];
			int[] array4 = this.hashes;
			for (int i = 0; i < num; i++)
			{
				Hashtable.Slot slot = array2[i];
				if (slot.key != null)
				{
					int num3 = array4[i] & int.MaxValue;
					uint num4 = (uint)num3;
					uint num5 = (uint)(((num3 >> 5) + 1) % (int)(num2 - 1U) + 1);
					uint num6 = num4 % num2;
					while (array[(int)((UIntPtr)num6)].key != null)
					{
						array3[(int)((UIntPtr)num6)] |= int.MinValue;
						num4 += num5;
						num6 = num4 % num2;
					}
					array[(int)((UIntPtr)num6)].key = slot.key;
					array[(int)((UIntPtr)num6)].value = slot.value;
					array3[(int)((UIntPtr)num6)] |= num3;
				}
			}
			this.modificationCount++;
			this.SetTable(array, array3);
		}

		// Token: 0x06001734 RID: 5940 RVA: 0x00059E28 File Offset: 0x00058028
		private void PutImpl(object key, object value, bool overwrite)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key", "null key");
			}
			if (this.inUse >= this.threshold)
			{
				this.Rehash();
			}
			uint num = (uint)this.table.Length;
			int num2 = this.GetHash(key) & int.MaxValue;
			uint num3 = (uint)num2;
			uint num4 = ((num3 >> 5) + 1U) % (num - 1U) + 1U;
			Hashtable.Slot[] array = this.table;
			int[] array2 = this.hashes;
			int num5 = -1;
			int num6 = 0;
			while ((long)num6 < (long)((ulong)num))
			{
				int num7 = (int)(num3 % num);
				Hashtable.Slot slot = array[num7];
				int num8 = array2[num7];
				if (num5 == -1 && slot.key == Hashtable.KeyMarker.Removed && (num8 & -2147483648) != 0)
				{
					num5 = num7;
				}
				if (slot.key == null || (slot.key == Hashtable.KeyMarker.Removed && (num8 & -2147483648) == 0))
				{
					if (num5 == -1)
					{
						num5 = num7;
					}
					break;
				}
				if ((num8 & 2147483647) == num2 && this.KeyEquals(key, slot.key))
				{
					if (overwrite)
					{
						array[num7].value = value;
						this.modificationCount++;
						return;
					}
					throw new ArgumentException("Key duplication when adding: " + key);
				}
				else
				{
					if (num5 == -1)
					{
						array2[num7] |= int.MinValue;
					}
					num3 += num4;
					num6++;
				}
			}
			if (num5 != -1)
			{
				array[num5].key = key;
				array[num5].value = value;
				array2[num5] |= num2;
				this.inUse++;
				this.modificationCount++;
			}
		}

		// Token: 0x06001735 RID: 5941 RVA: 0x00059FFC File Offset: 0x000581FC
		private void CopyToArray(Array arr, int i, Hashtable.EnumeratorMode mode)
		{
			IEnumerator enumerator = new Hashtable.Enumerator(this, mode);
			while (enumerator.MoveNext())
			{
				object value = enumerator.Current;
				arr.SetValue(value, i++);
			}
		}

		// Token: 0x06001736 RID: 5942 RVA: 0x0005A034 File Offset: 0x00058234
		internal static bool TestPrime(int x)
		{
			if ((x & 1) != 0)
			{
				int num = (int)Math.Sqrt((double)x);
				for (int i = 3; i < num; i += 2)
				{
					if (x % i == 0)
					{
						return false;
					}
				}
				return true;
			}
			return x == 2;
		}

		// Token: 0x06001737 RID: 5943 RVA: 0x0005A074 File Offset: 0x00058274
		internal static int CalcPrime(int x)
		{
			for (int i = (x & -2) - 1; i < 2147483647; i += 2)
			{
				if (Hashtable.TestPrime(i))
				{
					return i;
				}
			}
			return x;
		}

		// Token: 0x06001738 RID: 5944 RVA: 0x0005A0AC File Offset: 0x000582AC
		internal static int ToPrime(int x)
		{
			for (int i = 0; i < Hashtable.primeTbl.Length; i++)
			{
				if (x <= Hashtable.primeTbl[i])
				{
					return Hashtable.primeTbl[i];
				}
			}
			return Hashtable.CalcPrime(x);
		}

		// Token: 0x0400087D RID: 2173
		private const int CHAIN_MARKER = -2147483648;

		// Token: 0x0400087E RID: 2174
		private int inUse;

		// Token: 0x0400087F RID: 2175
		private int modificationCount;

		// Token: 0x04000880 RID: 2176
		private float loadFactor;

		// Token: 0x04000881 RID: 2177
		private Hashtable.Slot[] table;

		// Token: 0x04000882 RID: 2178
		private int[] hashes;

		// Token: 0x04000883 RID: 2179
		private int threshold;

		// Token: 0x04000884 RID: 2180
		private Hashtable.HashKeys hashKeys;

		// Token: 0x04000885 RID: 2181
		private Hashtable.HashValues hashValues;

		// Token: 0x04000886 RID: 2182
		private IHashCodeProvider hcpRef;

		// Token: 0x04000887 RID: 2183
		private IComparer comparerRef;

		// Token: 0x04000888 RID: 2184
		private SerializationInfo serializationInfo;

		// Token: 0x04000889 RID: 2185
		private IEqualityComparer equalityComparer;

		// Token: 0x0400088A RID: 2186
		private static readonly int[] primeTbl = new int[]
		{
			11,
			19,
			37,
			73,
			109,
			163,
			251,
			367,
			557,
			823,
			1237,
			1861,
			2777,
			4177,
			6247,
			9371,
			14057,
			21089,
			31627,
			47431,
			71143,
			106721,
			160073,
			240101,
			360163,
			540217,
			810343,
			1215497,
			1823231,
			2734867,
			4102283,
			6153409,
			9230113,
			13845163
		};

		// Token: 0x020001BB RID: 443
		[Serializable]
		internal struct Slot
		{
			// Token: 0x0400088B RID: 2187
			internal object key;

			// Token: 0x0400088C RID: 2188
			internal object value;
		}

		// Token: 0x020001BC RID: 444
		[Serializable]
		internal class KeyMarker : IObjectReference
		{
			// Token: 0x0600173B RID: 5947 RVA: 0x0005A100 File Offset: 0x00058300
			public object GetRealObject(StreamingContext context)
			{
				return Hashtable.KeyMarker.Removed;
			}

			// Token: 0x0400088D RID: 2189
			public static readonly Hashtable.KeyMarker Removed = new Hashtable.KeyMarker();
		}

		// Token: 0x020001BD RID: 445
		private enum EnumeratorMode
		{
			// Token: 0x0400088F RID: 2191
			KEY_MODE,
			// Token: 0x04000890 RID: 2192
			VALUE_MODE,
			// Token: 0x04000891 RID: 2193
			ENTRY_MODE
		}

		// Token: 0x020001BE RID: 446
		[Serializable]
		private sealed class Enumerator : IEnumerator, IDictionaryEnumerator
		{
			// Token: 0x0600173C RID: 5948 RVA: 0x0005A108 File Offset: 0x00058308
			public Enumerator(Hashtable host, Hashtable.EnumeratorMode mode)
			{
				this.host = host;
				this.stamp = host.modificationCount;
				this.size = host.table.Length;
				this.mode = mode;
				this.Reset();
			}

			// Token: 0x0600173D RID: 5949 RVA: 0x0005A14C File Offset: 0x0005834C
			public Enumerator(Hashtable host) : this(host, Hashtable.EnumeratorMode.KEY_MODE)
			{
			}

			// Token: 0x0600173F RID: 5951 RVA: 0x0005A164 File Offset: 0x00058364
			private void FailFast()
			{
				if (this.host.modificationCount != this.stamp)
				{
					throw new InvalidOperationException(Hashtable.Enumerator.xstr);
				}
			}

			// Token: 0x06001740 RID: 5952 RVA: 0x0005A188 File Offset: 0x00058388
			public void Reset()
			{
				this.FailFast();
				this.pos = -1;
				this.currentKey = null;
				this.currentValue = null;
			}

			// Token: 0x06001741 RID: 5953 RVA: 0x0005A1A8 File Offset: 0x000583A8
			public bool MoveNext()
			{
				this.FailFast();
				if (this.pos < this.size)
				{
					while (++this.pos < this.size)
					{
						Hashtable.Slot slot = this.host.table[this.pos];
						if (slot.key != null && slot.key != Hashtable.KeyMarker.Removed)
						{
							this.currentKey = slot.key;
							this.currentValue = slot.value;
							return true;
						}
					}
				}
				this.currentKey = null;
				this.currentValue = null;
				return false;
			}

			// Token: 0x17000387 RID: 903
			// (get) Token: 0x06001742 RID: 5954 RVA: 0x0005A254 File Offset: 0x00058454
			public DictionaryEntry Entry
			{
				get
				{
					if (this.currentKey == null)
					{
						throw new InvalidOperationException();
					}
					this.FailFast();
					return new DictionaryEntry(this.currentKey, this.currentValue);
				}
			}

			// Token: 0x17000388 RID: 904
			// (get) Token: 0x06001743 RID: 5955 RVA: 0x0005A28C File Offset: 0x0005848C
			public object Key
			{
				get
				{
					if (this.currentKey == null)
					{
						throw new InvalidOperationException();
					}
					this.FailFast();
					return this.currentKey;
				}
			}

			// Token: 0x17000389 RID: 905
			// (get) Token: 0x06001744 RID: 5956 RVA: 0x0005A2AC File Offset: 0x000584AC
			public object Value
			{
				get
				{
					if (this.currentKey == null)
					{
						throw new InvalidOperationException();
					}
					this.FailFast();
					return this.currentValue;
				}
			}

			// Token: 0x1700038A RID: 906
			// (get) Token: 0x06001745 RID: 5957 RVA: 0x0005A2CC File Offset: 0x000584CC
			public object Current
			{
				get
				{
					if (this.currentKey == null)
					{
						throw new InvalidOperationException();
					}
					switch (this.mode)
					{
					case Hashtable.EnumeratorMode.KEY_MODE:
						return this.currentKey;
					case Hashtable.EnumeratorMode.VALUE_MODE:
						return this.currentValue;
					case Hashtable.EnumeratorMode.ENTRY_MODE:
						return new DictionaryEntry(this.currentKey, this.currentValue);
					default:
						throw new Exception("should never happen");
					}
				}
			}

			// Token: 0x04000892 RID: 2194
			private Hashtable host;

			// Token: 0x04000893 RID: 2195
			private int stamp;

			// Token: 0x04000894 RID: 2196
			private int pos;

			// Token: 0x04000895 RID: 2197
			private int size;

			// Token: 0x04000896 RID: 2198
			private Hashtable.EnumeratorMode mode;

			// Token: 0x04000897 RID: 2199
			private object currentKey;

			// Token: 0x04000898 RID: 2200
			private object currentValue;

			// Token: 0x04000899 RID: 2201
			private static readonly string xstr = "Hashtable.Enumerator: snapshot out of sync.";
		}

		// Token: 0x020001BF RID: 447
		[DebuggerTypeProxy(typeof(CollectionDebuggerView))]
		[DebuggerDisplay("Count={Count}")]
		[Serializable]
		private class HashKeys : IEnumerable, ICollection
		{
			// Token: 0x06001746 RID: 5958 RVA: 0x0005A338 File Offset: 0x00058538
			public HashKeys(Hashtable host)
			{
				if (host == null)
				{
					throw new ArgumentNullException();
				}
				this.host = host;
			}

			// Token: 0x1700038B RID: 907
			// (get) Token: 0x06001747 RID: 5959 RVA: 0x0005A354 File Offset: 0x00058554
			public virtual int Count
			{
				get
				{
					return this.host.Count;
				}
			}

			// Token: 0x1700038C RID: 908
			// (get) Token: 0x06001748 RID: 5960 RVA: 0x0005A364 File Offset: 0x00058564
			public virtual bool IsSynchronized
			{
				get
				{
					return this.host.IsSynchronized;
				}
			}

			// Token: 0x1700038D RID: 909
			// (get) Token: 0x06001749 RID: 5961 RVA: 0x0005A374 File Offset: 0x00058574
			public virtual object SyncRoot
			{
				get
				{
					return this.host.SyncRoot;
				}
			}

			// Token: 0x0600174A RID: 5962 RVA: 0x0005A384 File Offset: 0x00058584
			public virtual void CopyTo(Array array, int arrayIndex)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array");
				}
				if (array.Rank != 1)
				{
					throw new ArgumentException("array");
				}
				if (arrayIndex < 0)
				{
					throw new ArgumentOutOfRangeException("arrayIndex");
				}
				if (array.Length - arrayIndex < this.Count)
				{
					throw new ArgumentException("not enough space");
				}
				this.host.CopyToArray(array, arrayIndex, Hashtable.EnumeratorMode.KEY_MODE);
			}

			// Token: 0x0600174B RID: 5963 RVA: 0x0005A3F8 File Offset: 0x000585F8
			public virtual IEnumerator GetEnumerator()
			{
				return new Hashtable.Enumerator(this.host, Hashtable.EnumeratorMode.KEY_MODE);
			}

			// Token: 0x0400089A RID: 2202
			private Hashtable host;
		}

		// Token: 0x020001C0 RID: 448
		[DebuggerTypeProxy(typeof(CollectionDebuggerView))]
		[DebuggerDisplay("Count={Count}")]
		[Serializable]
		private class HashValues : IEnumerable, ICollection
		{
			// Token: 0x0600174C RID: 5964 RVA: 0x0005A408 File Offset: 0x00058608
			public HashValues(Hashtable host)
			{
				if (host == null)
				{
					throw new ArgumentNullException();
				}
				this.host = host;
			}

			// Token: 0x1700038E RID: 910
			// (get) Token: 0x0600174D RID: 5965 RVA: 0x0005A424 File Offset: 0x00058624
			public virtual int Count
			{
				get
				{
					return this.host.Count;
				}
			}

			// Token: 0x1700038F RID: 911
			// (get) Token: 0x0600174E RID: 5966 RVA: 0x0005A434 File Offset: 0x00058634
			public virtual bool IsSynchronized
			{
				get
				{
					return this.host.IsSynchronized;
				}
			}

			// Token: 0x17000390 RID: 912
			// (get) Token: 0x0600174F RID: 5967 RVA: 0x0005A444 File Offset: 0x00058644
			public virtual object SyncRoot
			{
				get
				{
					return this.host.SyncRoot;
				}
			}

			// Token: 0x06001750 RID: 5968 RVA: 0x0005A454 File Offset: 0x00058654
			public virtual void CopyTo(Array array, int arrayIndex)
			{
				if (array == null)
				{
					throw new ArgumentNullException("array");
				}
				if (array.Rank != 1)
				{
					throw new ArgumentException("array");
				}
				if (arrayIndex < 0)
				{
					throw new ArgumentOutOfRangeException("arrayIndex");
				}
				if (array.Length - arrayIndex < this.Count)
				{
					throw new ArgumentException("not enough space");
				}
				this.host.CopyToArray(array, arrayIndex, Hashtable.EnumeratorMode.VALUE_MODE);
			}

			// Token: 0x06001751 RID: 5969 RVA: 0x0005A4C8 File Offset: 0x000586C8
			public virtual IEnumerator GetEnumerator()
			{
				return new Hashtable.Enumerator(this.host, Hashtable.EnumeratorMode.VALUE_MODE);
			}

			// Token: 0x0400089B RID: 2203
			private Hashtable host;
		}

		// Token: 0x020001C1 RID: 449
		[Serializable]
		private class SyncHashtable : Hashtable, IEnumerable
		{
			// Token: 0x06001752 RID: 5970 RVA: 0x0005A4D8 File Offset: 0x000586D8
			public SyncHashtable(Hashtable host)
			{
				if (host == null)
				{
					throw new ArgumentNullException();
				}
				this.host = host;
			}

			// Token: 0x06001753 RID: 5971 RVA: 0x0005A4F4 File Offset: 0x000586F4
			internal SyncHashtable(SerializationInfo info, StreamingContext context)
			{
				this.host = (Hashtable)info.GetValue("ParentTable", typeof(Hashtable));
			}

			// Token: 0x06001754 RID: 5972 RVA: 0x0005A528 File Offset: 0x00058728
			IEnumerator IEnumerable.GetEnumerator()
			{
				return new Hashtable.Enumerator(this.host, Hashtable.EnumeratorMode.ENTRY_MODE);
			}

			// Token: 0x06001755 RID: 5973 RVA: 0x0005A538 File Offset: 0x00058738
			public override void GetObjectData(SerializationInfo info, StreamingContext context)
			{
				info.AddValue("ParentTable", this.host);
			}

			// Token: 0x17000391 RID: 913
			// (get) Token: 0x06001756 RID: 5974 RVA: 0x0005A54C File Offset: 0x0005874C
			public override int Count
			{
				get
				{
					return this.host.Count;
				}
			}

			// Token: 0x17000392 RID: 914
			// (get) Token: 0x06001757 RID: 5975 RVA: 0x0005A55C File Offset: 0x0005875C
			public override bool IsSynchronized
			{
				get
				{
					return true;
				}
			}

			// Token: 0x17000393 RID: 915
			// (get) Token: 0x06001758 RID: 5976 RVA: 0x0005A560 File Offset: 0x00058760
			public override object SyncRoot
			{
				get
				{
					return this.host.SyncRoot;
				}
			}

			// Token: 0x17000394 RID: 916
			// (get) Token: 0x06001759 RID: 5977 RVA: 0x0005A570 File Offset: 0x00058770
			public override bool IsFixedSize
			{
				get
				{
					return this.host.IsFixedSize;
				}
			}

			// Token: 0x17000395 RID: 917
			// (get) Token: 0x0600175A RID: 5978 RVA: 0x0005A580 File Offset: 0x00058780
			public override bool IsReadOnly
			{
				get
				{
					return this.host.IsReadOnly;
				}
			}

			// Token: 0x17000396 RID: 918
			// (get) Token: 0x0600175B RID: 5979 RVA: 0x0005A590 File Offset: 0x00058790
			public override ICollection Keys
			{
				get
				{
					ICollection result = null;
					object syncRoot = this.host.SyncRoot;
					lock (syncRoot)
					{
						result = this.host.Keys;
					}
					return result;
				}
			}

			// Token: 0x17000397 RID: 919
			// (get) Token: 0x0600175C RID: 5980 RVA: 0x0005A5E8 File Offset: 0x000587E8
			public override ICollection Values
			{
				get
				{
					ICollection result = null;
					object syncRoot = this.host.SyncRoot;
					lock (syncRoot)
					{
						result = this.host.Values;
					}
					return result;
				}
			}

			// Token: 0x17000398 RID: 920
			public override object this[object key]
			{
				get
				{
					return this.host[key];
				}
				set
				{
					object syncRoot = this.host.SyncRoot;
					lock (syncRoot)
					{
						this.host[key] = value;
					}
				}
			}

			// Token: 0x0600175F RID: 5983 RVA: 0x0005A6A4 File Offset: 0x000588A4
			public override void CopyTo(Array array, int arrayIndex)
			{
				this.host.CopyTo(array, arrayIndex);
			}

			// Token: 0x06001760 RID: 5984 RVA: 0x0005A6B4 File Offset: 0x000588B4
			public override void Add(object key, object value)
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.Add(key, value);
				}
			}

			// Token: 0x06001761 RID: 5985 RVA: 0x0005A708 File Offset: 0x00058908
			public override void Clear()
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.Clear();
				}
			}

			// Token: 0x06001762 RID: 5986 RVA: 0x0005A75C File Offset: 0x0005895C
			public override bool Contains(object key)
			{
				return this.host.Find(key) >= 0;
			}

			// Token: 0x06001763 RID: 5987 RVA: 0x0005A770 File Offset: 0x00058970
			public override IDictionaryEnumerator GetEnumerator()
			{
				return new Hashtable.Enumerator(this.host, Hashtable.EnumeratorMode.ENTRY_MODE);
			}

			// Token: 0x06001764 RID: 5988 RVA: 0x0005A780 File Offset: 0x00058980
			public override void Remove(object key)
			{
				object syncRoot = this.host.SyncRoot;
				lock (syncRoot)
				{
					this.host.Remove(key);
				}
			}

			// Token: 0x06001765 RID: 5989 RVA: 0x0005A7D4 File Offset: 0x000589D4
			public override bool ContainsKey(object key)
			{
				return this.host.Contains(key);
			}

			// Token: 0x06001766 RID: 5990 RVA: 0x0005A7E4 File Offset: 0x000589E4
			public override bool ContainsValue(object value)
			{
				return this.host.ContainsValue(value);
			}

			// Token: 0x06001767 RID: 5991 RVA: 0x0005A7F4 File Offset: 0x000589F4
			public override object Clone()
			{
				object syncRoot = this.host.SyncRoot;
				object result;
				lock (syncRoot)
				{
					result = new Hashtable.SyncHashtable((Hashtable)this.host.Clone());
				}
				return result;
			}

			// Token: 0x0400089C RID: 2204
			private Hashtable host;
		}
	}
}
