﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001C3 RID: 451
	[ComVisible(true)]
	public interface IDictionary : IEnumerable, ICollection
	{
		// Token: 0x17000399 RID: 921
		// (get) Token: 0x06001769 RID: 5993
		bool IsFixedSize { get; }

		// Token: 0x1700039A RID: 922
		// (get) Token: 0x0600176A RID: 5994
		bool IsReadOnly { get; }

		// Token: 0x1700039B RID: 923
		object this[object key]
		{
			get;
			set;
		}

		// Token: 0x1700039C RID: 924
		// (get) Token: 0x0600176D RID: 5997
		ICollection Keys { get; }

		// Token: 0x1700039D RID: 925
		// (get) Token: 0x0600176E RID: 5998
		ICollection Values { get; }

		// Token: 0x0600176F RID: 5999
		void Add(object key, object value);

		// Token: 0x06001770 RID: 6000
		void Clear();

		// Token: 0x06001771 RID: 6001
		bool Contains(object key);

		// Token: 0x06001772 RID: 6002
		IDictionaryEnumerator GetEnumerator();

		// Token: 0x06001773 RID: 6003
		void Remove(object key);
	}
}
