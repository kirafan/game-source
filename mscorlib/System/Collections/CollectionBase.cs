﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections
{
	// Token: 0x020001B5 RID: 437
	[ComVisible(true)]
	[Serializable]
	public abstract class CollectionBase : IEnumerable, ICollection, IList
	{
		// Token: 0x060016B8 RID: 5816 RVA: 0x00058884 File Offset: 0x00056A84
		protected CollectionBase()
		{
		}

		// Token: 0x060016B9 RID: 5817 RVA: 0x0005888C File Offset: 0x00056A8C
		protected CollectionBase(int capacity)
		{
			this.list = new ArrayList(capacity);
		}

		// Token: 0x060016BA RID: 5818 RVA: 0x000588A0 File Offset: 0x00056AA0
		void ICollection.CopyTo(Array array, int index)
		{
			this.InnerList.CopyTo(array, index);
		}

		// Token: 0x17000366 RID: 870
		// (get) Token: 0x060016BB RID: 5819 RVA: 0x000588B0 File Offset: 0x00056AB0
		object ICollection.SyncRoot
		{
			get
			{
				return this.InnerList.SyncRoot;
			}
		}

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x060016BC RID: 5820 RVA: 0x000588C0 File Offset: 0x00056AC0
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.InnerList.IsSynchronized;
			}
		}

		// Token: 0x060016BD RID: 5821 RVA: 0x000588D0 File Offset: 0x00056AD0
		int IList.Add(object value)
		{
			this.OnValidate(value);
			int count = this.InnerList.Count;
			this.OnInsert(count, value);
			this.InnerList.Add(value);
			try
			{
				this.OnInsertComplete(count, value);
			}
			catch
			{
				this.InnerList.RemoveAt(count);
				throw;
			}
			return count;
		}

		// Token: 0x060016BE RID: 5822 RVA: 0x00058944 File Offset: 0x00056B44
		bool IList.Contains(object value)
		{
			return this.InnerList.Contains(value);
		}

		// Token: 0x060016BF RID: 5823 RVA: 0x00058954 File Offset: 0x00056B54
		int IList.IndexOf(object value)
		{
			return this.InnerList.IndexOf(value);
		}

		// Token: 0x060016C0 RID: 5824 RVA: 0x00058964 File Offset: 0x00056B64
		void IList.Insert(int index, object value)
		{
			this.OnValidate(value);
			this.OnInsert(index, value);
			this.InnerList.Insert(index, value);
			try
			{
				this.OnInsertComplete(index, value);
			}
			catch
			{
				this.InnerList.RemoveAt(index);
				throw;
			}
		}

		// Token: 0x060016C1 RID: 5825 RVA: 0x000589CC File Offset: 0x00056BCC
		void IList.Remove(object value)
		{
			this.OnValidate(value);
			int num = this.InnerList.IndexOf(value);
			if (num == -1)
			{
				throw new ArgumentException("The element cannot be found.", "value");
			}
			this.OnRemove(num, value);
			this.InnerList.Remove(value);
			this.OnRemoveComplete(num, value);
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x060016C2 RID: 5826 RVA: 0x00058A20 File Offset: 0x00056C20
		bool IList.IsFixedSize
		{
			get
			{
				return this.InnerList.IsFixedSize;
			}
		}

		// Token: 0x17000369 RID: 873
		// (get) Token: 0x060016C3 RID: 5827 RVA: 0x00058A30 File Offset: 0x00056C30
		bool IList.IsReadOnly
		{
			get
			{
				return this.InnerList.IsReadOnly;
			}
		}

		// Token: 0x1700036A RID: 874
		object IList.this[int index]
		{
			get
			{
				return this.InnerList[index];
			}
			set
			{
				if (index < 0 || index >= this.InnerList.Count)
				{
					throw new ArgumentOutOfRangeException("index");
				}
				this.OnValidate(value);
				object obj = this.InnerList[index];
				this.OnSet(index, obj, value);
				this.InnerList[index] = value;
				try
				{
					this.OnSetComplete(index, obj, value);
				}
				catch
				{
					this.InnerList[index] = obj;
					throw;
				}
			}
		}

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x060016C6 RID: 5830 RVA: 0x00058AEC File Offset: 0x00056CEC
		public int Count
		{
			get
			{
				return this.InnerList.Count;
			}
		}

		// Token: 0x060016C7 RID: 5831 RVA: 0x00058AFC File Offset: 0x00056CFC
		public IEnumerator GetEnumerator()
		{
			return this.InnerList.GetEnumerator();
		}

		// Token: 0x060016C8 RID: 5832 RVA: 0x00058B0C File Offset: 0x00056D0C
		public void Clear()
		{
			this.OnClear();
			this.InnerList.Clear();
			this.OnClearComplete();
		}

		// Token: 0x060016C9 RID: 5833 RVA: 0x00058B30 File Offset: 0x00056D30
		public void RemoveAt(int index)
		{
			object value = this.InnerList[index];
			this.OnValidate(value);
			this.OnRemove(index, value);
			this.InnerList.RemoveAt(index);
			this.OnRemoveComplete(index, value);
		}

		// Token: 0x1700036C RID: 876
		// (get) Token: 0x060016CA RID: 5834 RVA: 0x00058B70 File Offset: 0x00056D70
		// (set) Token: 0x060016CB RID: 5835 RVA: 0x00058B94 File Offset: 0x00056D94
		[ComVisible(false)]
		public int Capacity
		{
			get
			{
				if (this.list == null)
				{
					this.list = new ArrayList();
				}
				return this.list.Capacity;
			}
			set
			{
				if (this.list == null)
				{
					this.list = new ArrayList();
				}
				this.list.Capacity = value;
			}
		}

		// Token: 0x1700036D RID: 877
		// (get) Token: 0x060016CC RID: 5836 RVA: 0x00058BC4 File Offset: 0x00056DC4
		protected ArrayList InnerList
		{
			get
			{
				if (this.list == null)
				{
					this.list = new ArrayList();
				}
				return this.list;
			}
		}

		// Token: 0x1700036E RID: 878
		// (get) Token: 0x060016CD RID: 5837 RVA: 0x00058BE4 File Offset: 0x00056DE4
		protected IList List
		{
			get
			{
				return this;
			}
		}

		// Token: 0x060016CE RID: 5838 RVA: 0x00058BE8 File Offset: 0x00056DE8
		protected virtual void OnClear()
		{
		}

		// Token: 0x060016CF RID: 5839 RVA: 0x00058BEC File Offset: 0x00056DEC
		protected virtual void OnClearComplete()
		{
		}

		// Token: 0x060016D0 RID: 5840 RVA: 0x00058BF0 File Offset: 0x00056DF0
		protected virtual void OnInsert(int index, object value)
		{
		}

		// Token: 0x060016D1 RID: 5841 RVA: 0x00058BF4 File Offset: 0x00056DF4
		protected virtual void OnInsertComplete(int index, object value)
		{
		}

		// Token: 0x060016D2 RID: 5842 RVA: 0x00058BF8 File Offset: 0x00056DF8
		protected virtual void OnRemove(int index, object value)
		{
		}

		// Token: 0x060016D3 RID: 5843 RVA: 0x00058BFC File Offset: 0x00056DFC
		protected virtual void OnRemoveComplete(int index, object value)
		{
		}

		// Token: 0x060016D4 RID: 5844 RVA: 0x00058C00 File Offset: 0x00056E00
		protected virtual void OnSet(int index, object oldValue, object newValue)
		{
		}

		// Token: 0x060016D5 RID: 5845 RVA: 0x00058C04 File Offset: 0x00056E04
		protected virtual void OnSetComplete(int index, object oldValue, object newValue)
		{
		}

		// Token: 0x060016D6 RID: 5846 RVA: 0x00058C08 File Offset: 0x00056E08
		protected virtual void OnValidate(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("CollectionBase.OnValidate: Invalid parameter value passed to method: null");
			}
		}

		// Token: 0x04000875 RID: 2165
		private ArrayList list;
	}
}
