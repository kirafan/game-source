﻿using System;

namespace System
{
	// Token: 0x02000703 RID: 1795
	// (Invoke) Token: 0x06004400 RID: 17408
	public delegate bool Predicate<T>(T obj);
}
