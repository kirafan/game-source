﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x020000FD RID: 253
	[ComVisible(true)]
	[Serializable]
	public class ApplicationException : Exception
	{
		// Token: 0x06000D58 RID: 3416 RVA: 0x0003A87C File Offset: 0x00038A7C
		public ApplicationException() : base(Locale.GetText("An application exception has occurred."))
		{
			base.HResult = -2146232832;
		}

		// Token: 0x06000D59 RID: 3417 RVA: 0x0003A89C File Offset: 0x00038A9C
		public ApplicationException(string message) : base(message)
		{
			base.HResult = -2146232832;
		}

		// Token: 0x06000D5A RID: 3418 RVA: 0x0003A8B0 File Offset: 0x00038AB0
		public ApplicationException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146232832;
		}

		// Token: 0x06000D5B RID: 3419 RVA: 0x0003A8C8 File Offset: 0x00038AC8
		protected ApplicationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000396 RID: 918
		private const int Result = -2146232832;
	}
}
