﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000100 RID: 256
	[ComVisible(true)]
	[Serializable]
	public class ArgumentException : SystemException
	{
		// Token: 0x06000D6B RID: 3435 RVA: 0x0003AC00 File Offset: 0x00038E00
		public ArgumentException() : base(Locale.GetText("Value does not fall within the expected range."))
		{
			base.HResult = -2147024809;
		}

		// Token: 0x06000D6C RID: 3436 RVA: 0x0003AC20 File Offset: 0x00038E20
		public ArgumentException(string message) : base(message)
		{
			base.HResult = -2147024809;
		}

		// Token: 0x06000D6D RID: 3437 RVA: 0x0003AC34 File Offset: 0x00038E34
		public ArgumentException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147024809;
		}

		// Token: 0x06000D6E RID: 3438 RVA: 0x0003AC4C File Offset: 0x00038E4C
		public ArgumentException(string message, string paramName) : base(message)
		{
			this.param_name = paramName;
			base.HResult = -2147024809;
		}

		// Token: 0x06000D6F RID: 3439 RVA: 0x0003AC68 File Offset: 0x00038E68
		public ArgumentException(string message, string paramName, Exception innerException) : base(message, innerException)
		{
			this.param_name = paramName;
			base.HResult = -2147024809;
		}

		// Token: 0x06000D70 RID: 3440 RVA: 0x0003AC84 File Offset: 0x00038E84
		protected ArgumentException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.param_name = info.GetString("ParamName");
		}

		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x06000D71 RID: 3441 RVA: 0x0003ACA0 File Offset: 0x00038EA0
		public virtual string ParamName
		{
			get
			{
				return this.param_name;
			}
		}

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x06000D72 RID: 3442 RVA: 0x0003ACA8 File Offset: 0x00038EA8
		public override string Message
		{
			get
			{
				if (this.ParamName != null && this.ParamName.Length != 0)
				{
					return base.Message + Environment.NewLine + Locale.GetText("Parameter name: ") + this.ParamName;
				}
				return base.Message;
			}
		}

		// Token: 0x06000D73 RID: 3443 RVA: 0x0003ACF8 File Offset: 0x00038EF8
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("ParamName", this.ParamName);
		}

		// Token: 0x0400039E RID: 926
		private const int Result = -2147024809;

		// Token: 0x0400039F RID: 927
		private string param_name;
	}
}
