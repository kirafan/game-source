﻿using System;

namespace System
{
	// Token: 0x02000144 RID: 324
	[Serializable]
	public sealed class InsufficientMemoryException : OutOfMemoryException
	{
		// Token: 0x060011B6 RID: 4534 RVA: 0x000470C8 File Offset: 0x000452C8
		public InsufficientMemoryException() : base("Insufficient memory")
		{
		}

		// Token: 0x060011B7 RID: 4535 RVA: 0x000470D8 File Offset: 0x000452D8
		public InsufficientMemoryException(string message) : base(message)
		{
		}

		// Token: 0x060011B8 RID: 4536 RVA: 0x000470E4 File Offset: 0x000452E4
		public InsufficientMemoryException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
