﻿using System;

namespace System
{
	// Token: 0x0200018B RID: 395
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoLimitationAttribute : MonoTODOAttribute
	{
		// Token: 0x0600145A RID: 5210 RVA: 0x00051FDC File Offset: 0x000501DC
		public MonoLimitationAttribute(string comment) : base(comment)
		{
		}
	}
}
