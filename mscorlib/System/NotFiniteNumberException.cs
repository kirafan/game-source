﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200015D RID: 349
	[ComVisible(true)]
	[Serializable]
	public class NotFiniteNumberException : ArithmeticException
	{
		// Token: 0x06001292 RID: 4754 RVA: 0x000494F4 File Offset: 0x000476F4
		public NotFiniteNumberException() : base(Locale.GetText("The number encountered was not a finite quantity."))
		{
			base.HResult = -2146233048;
		}

		// Token: 0x06001293 RID: 4755 RVA: 0x00049514 File Offset: 0x00047714
		public NotFiniteNumberException(double offendingNumber)
		{
			this.offending_number = offendingNumber;
			base.HResult = -2146233048;
		}

		// Token: 0x06001294 RID: 4756 RVA: 0x00049530 File Offset: 0x00047730
		public NotFiniteNumberException(string message) : base(message)
		{
			base.HResult = -2146233048;
		}

		// Token: 0x06001295 RID: 4757 RVA: 0x00049544 File Offset: 0x00047744
		public NotFiniteNumberException(string message, double offendingNumber) : base(message)
		{
			this.offending_number = offendingNumber;
			base.HResult = -2146233048;
		}

		// Token: 0x06001296 RID: 4758 RVA: 0x00049560 File Offset: 0x00047760
		public NotFiniteNumberException(string message, double offendingNumber, Exception innerException) : base(message, innerException)
		{
			this.offending_number = offendingNumber;
			base.HResult = -2146233048;
		}

		// Token: 0x06001297 RID: 4759 RVA: 0x0004957C File Offset: 0x0004777C
		protected NotFiniteNumberException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.offending_number = info.GetDouble("OffendingNumber");
		}

		// Token: 0x06001298 RID: 4760 RVA: 0x00049598 File Offset: 0x00047798
		public NotFiniteNumberException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233048;
		}

		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06001299 RID: 4761 RVA: 0x000495B0 File Offset: 0x000477B0
		public double OffendingNumber
		{
			get
			{
				return this.offending_number;
			}
		}

		// Token: 0x0600129A RID: 4762 RVA: 0x000495B8 File Offset: 0x000477B8
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("OffendingNumber", this.offending_number);
		}

		// Token: 0x04000549 RID: 1353
		private const int Result = -2146233048;

		// Token: 0x0400054A RID: 1354
		private double offending_number;
	}
}
