﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x020000FF RID: 255
	[ComVisible(false)]
	[Serializable]
	public sealed class ApplicationIdentity : ISerializable
	{
		// Token: 0x06000D66 RID: 3430 RVA: 0x0003AB80 File Offset: 0x00038D80
		public ApplicationIdentity(string applicationIdentityFullName)
		{
			if (applicationIdentityFullName == null)
			{
				throw new ArgumentNullException("applicationIdentityFullName");
			}
			if (applicationIdentityFullName.IndexOf(", Culture=") == -1)
			{
				this._fullName = applicationIdentityFullName + ", Culture=neutral";
			}
			else
			{
				this._fullName = applicationIdentityFullName;
			}
		}

		// Token: 0x06000D67 RID: 3431 RVA: 0x0003ABD4 File Offset: 0x00038DD4
		[MonoTODO("Missing serialization")]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
		}

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x06000D68 RID: 3432 RVA: 0x0003ABE8 File Offset: 0x00038DE8
		public string CodeBase
		{
			get
			{
				return this._codeBase;
			}
		}

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x06000D69 RID: 3433 RVA: 0x0003ABF0 File Offset: 0x00038DF0
		public string FullName
		{
			get
			{
				return this._fullName;
			}
		}

		// Token: 0x06000D6A RID: 3434 RVA: 0x0003ABF8 File Offset: 0x00038DF8
		public override string ToString()
		{
			return this._fullName;
		}

		// Token: 0x0400039C RID: 924
		private string _fullName;

		// Token: 0x0400039D RID: 925
		private string _codeBase;
	}
}
