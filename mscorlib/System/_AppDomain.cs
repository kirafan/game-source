﻿using System;
using System.Globalization;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;
using System.Security.Principal;

namespace System
{
	// Token: 0x020001A1 RID: 417
	[ComVisible(true)]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("05F696DC-2B29-3663-AD8B-C4389CF2A713")]
	public interface _AppDomain
	{
		// Token: 0x1400000C RID: 12
		// (add) Token: 0x060014EB RID: 5355
		// (remove) Token: 0x060014EC RID: 5356
		event AssemblyLoadEventHandler AssemblyLoad;

		// Token: 0x1400000D RID: 13
		// (add) Token: 0x060014ED RID: 5357
		// (remove) Token: 0x060014EE RID: 5358
		event ResolveEventHandler AssemblyResolve;

		// Token: 0x1400000E RID: 14
		// (add) Token: 0x060014EF RID: 5359
		// (remove) Token: 0x060014F0 RID: 5360
		event EventHandler DomainUnload;

		// Token: 0x1400000F RID: 15
		// (add) Token: 0x060014F1 RID: 5361
		// (remove) Token: 0x060014F2 RID: 5362
		event EventHandler ProcessExit;

		// Token: 0x14000010 RID: 16
		// (add) Token: 0x060014F3 RID: 5363
		// (remove) Token: 0x060014F4 RID: 5364
		event ResolveEventHandler ResourceResolve;

		// Token: 0x14000011 RID: 17
		// (add) Token: 0x060014F5 RID: 5365
		// (remove) Token: 0x060014F6 RID: 5366
		event ResolveEventHandler TypeResolve;

		// Token: 0x14000012 RID: 18
		// (add) Token: 0x060014F7 RID: 5367
		// (remove) Token: 0x060014F8 RID: 5368
		event UnhandledExceptionEventHandler UnhandledException;

		// Token: 0x17000317 RID: 791
		// (get) Token: 0x060014F9 RID: 5369
		string BaseDirectory { get; }

		// Token: 0x17000318 RID: 792
		// (get) Token: 0x060014FA RID: 5370
		string DynamicDirectory { get; }

		// Token: 0x17000319 RID: 793
		// (get) Token: 0x060014FB RID: 5371
		Evidence Evidence { get; }

		// Token: 0x1700031A RID: 794
		// (get) Token: 0x060014FC RID: 5372
		string FriendlyName { get; }

		// Token: 0x1700031B RID: 795
		// (get) Token: 0x060014FD RID: 5373
		string RelativeSearchPath { get; }

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x060014FE RID: 5374
		bool ShadowCopyFiles { get; }

		// Token: 0x060014FF RID: 5375
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		void AppendPrivatePath(string path);

		// Token: 0x06001500 RID: 5376
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		void ClearPrivatePath();

		// Token: 0x06001501 RID: 5377
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		void ClearShadowCopyPath();

		// Token: 0x06001502 RID: 5378
		ObjectHandle CreateInstance(string assemblyName, string typeName);

		// Token: 0x06001503 RID: 5379
		ObjectHandle CreateInstance(string assemblyName, string typeName, object[] activationAttributes);

		// Token: 0x06001504 RID: 5380
		ObjectHandle CreateInstance(string assemblyName, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityAttributes);

		// Token: 0x06001505 RID: 5381
		ObjectHandle CreateInstanceFrom(string assemblyFile, string typeName);

		// Token: 0x06001506 RID: 5382
		ObjectHandle CreateInstanceFrom(string assemblyFile, string typeName, object[] activationAttributes);

		// Token: 0x06001507 RID: 5383
		ObjectHandle CreateInstanceFrom(string assemblyFile, string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes, Evidence securityAttributes);

		// Token: 0x06001508 RID: 5384
		AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access);

		// Token: 0x06001509 RID: 5385
		AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, Evidence evidence);

		// Token: 0x0600150A RID: 5386
		AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir);

		// Token: 0x0600150B RID: 5387
		AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir, Evidence evidence);

		// Token: 0x0600150C RID: 5388
		AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions);

		// Token: 0x0600150D RID: 5389
		AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, Evidence evidence, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions);

		// Token: 0x0600150E RID: 5390
		AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions);

		// Token: 0x0600150F RID: 5391
		AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir, Evidence evidence, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions);

		// Token: 0x06001510 RID: 5392
		AssemblyBuilder DefineDynamicAssembly(AssemblyName name, AssemblyBuilderAccess access, string dir, Evidence evidence, PermissionSet requiredPermissions, PermissionSet optionalPermissions, PermissionSet refusedPermissions, bool isSynchronized);

		// Token: 0x06001511 RID: 5393
		void DoCallBack(CrossAppDomainDelegate theDelegate);

		// Token: 0x06001512 RID: 5394
		bool Equals(object other);

		// Token: 0x06001513 RID: 5395
		int ExecuteAssembly(string assemblyFile);

		// Token: 0x06001514 RID: 5396
		int ExecuteAssembly(string assemblyFile, Evidence assemblySecurity);

		// Token: 0x06001515 RID: 5397
		int ExecuteAssembly(string assemblyFile, Evidence assemblySecurity, string[] args);

		// Token: 0x06001516 RID: 5398
		Assembly[] GetAssemblies();

		// Token: 0x06001517 RID: 5399
		object GetData(string name);

		// Token: 0x06001518 RID: 5400
		int GetHashCode();

		// Token: 0x06001519 RID: 5401
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
		object GetLifetimeService();

		// Token: 0x0600151A RID: 5402
		Type GetType();

		// Token: 0x0600151B RID: 5403
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
		object InitializeLifetimeService();

		// Token: 0x0600151C RID: 5404
		Assembly Load(AssemblyName assemblyRef);

		// Token: 0x0600151D RID: 5405
		Assembly Load(byte[] rawAssembly);

		// Token: 0x0600151E RID: 5406
		Assembly Load(string assemblyString);

		// Token: 0x0600151F RID: 5407
		Assembly Load(AssemblyName assemblyRef, Evidence assemblySecurity);

		// Token: 0x06001520 RID: 5408
		Assembly Load(byte[] rawAssembly, byte[] rawSymbolStore);

		// Token: 0x06001521 RID: 5409
		Assembly Load(string assemblyString, Evidence assemblySecurity);

		// Token: 0x06001522 RID: 5410
		Assembly Load(byte[] rawAssembly, byte[] rawSymbolStore, Evidence securityEvidence);

		// Token: 0x06001523 RID: 5411
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		void SetAppDomainPolicy(PolicyLevel domainPolicy);

		// Token: 0x06001524 RID: 5412
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		void SetCachePath(string s);

		// Token: 0x06001525 RID: 5413
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		void SetData(string name, object data);

		// Token: 0x06001526 RID: 5414
		void SetPrincipalPolicy(PrincipalPolicy policy);

		// Token: 0x06001527 RID: 5415
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
		void SetShadowCopyPath(string s);

		// Token: 0x06001528 RID: 5416
		void SetThreadPrincipal(IPrincipal principal);

		// Token: 0x06001529 RID: 5417
		string ToString();

		// Token: 0x0600152A RID: 5418
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x0600152B RID: 5419
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x0600152C RID: 5420
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x0600152D RID: 5421
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
