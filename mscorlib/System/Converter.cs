﻿using System;

namespace System
{
	// Token: 0x02000702 RID: 1794
	// (Invoke) Token: 0x060043FC RID: 17404
	public delegate TOutput Converter<TInput, TOutput>(TInput input);
}
