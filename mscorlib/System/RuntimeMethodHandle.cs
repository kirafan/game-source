﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System
{
	// Token: 0x0200016F RID: 367
	[MonoTODO("Serialization needs tests")]
	[ComVisible(true)]
	[Serializable]
	public struct RuntimeMethodHandle : ISerializable
	{
		// Token: 0x06001367 RID: 4967 RVA: 0x0004D984 File Offset: 0x0004BB84
		internal RuntimeMethodHandle(IntPtr v)
		{
			this.value = v;
		}

		// Token: 0x06001368 RID: 4968 RVA: 0x0004D990 File Offset: 0x0004BB90
		private RuntimeMethodHandle(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			MonoMethod monoMethod = (MonoMethod)info.GetValue("MethodObj", typeof(MonoMethod));
			this.value = monoMethod.MethodHandle.Value;
			if (this.value == IntPtr.Zero)
			{
				throw new SerializationException(Locale.GetText("Insufficient state."));
			}
		}

		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x06001369 RID: 4969 RVA: 0x0004DA04 File Offset: 0x0004BC04
		public IntPtr Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x0600136A RID: 4970 RVA: 0x0004DA0C File Offset: 0x0004BC0C
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			if (this.value == IntPtr.Zero)
			{
				throw new SerializationException("Object fields may not be properly initialized");
			}
			info.AddValue("MethodObj", (MonoMethod)MethodBase.GetMethodFromHandle(this), typeof(MonoMethod));
		}

		// Token: 0x0600136B RID: 4971
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetFunctionPointer(IntPtr m);

		// Token: 0x0600136C RID: 4972 RVA: 0x0004DA70 File Offset: 0x0004BC70
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public IntPtr GetFunctionPointer()
		{
			return RuntimeMethodHandle.GetFunctionPointer(this.value);
		}

		// Token: 0x0600136D RID: 4973 RVA: 0x0004DA80 File Offset: 0x0004BC80
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public override bool Equals(object obj)
		{
			return obj != null && base.GetType() == obj.GetType() && this.value == ((RuntimeMethodHandle)obj).Value;
		}

		// Token: 0x0600136E RID: 4974 RVA: 0x0004DACC File Offset: 0x0004BCCC
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public bool Equals(RuntimeMethodHandle handle)
		{
			return this.value == handle.Value;
		}

		// Token: 0x0600136F RID: 4975 RVA: 0x0004DAE0 File Offset: 0x0004BCE0
		public override int GetHashCode()
		{
			return this.value.GetHashCode();
		}

		// Token: 0x06001370 RID: 4976 RVA: 0x0004DAF0 File Offset: 0x0004BCF0
		public static bool operator ==(RuntimeMethodHandle left, RuntimeMethodHandle right)
		{
			return left.Equals(right);
		}

		// Token: 0x06001371 RID: 4977 RVA: 0x0004DAFC File Offset: 0x0004BCFC
		public static bool operator !=(RuntimeMethodHandle left, RuntimeMethodHandle right)
		{
			return !left.Equals(right);
		}

		// Token: 0x040005A6 RID: 1446
		private IntPtr value;
	}
}
