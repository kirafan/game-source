﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000025 RID: 37
	[ComVisible(true)]
	[CLSCompliant(false)]
	[Serializable]
	public struct UIntPtr : ISerializable
	{
		// Token: 0x06000373 RID: 883 RVA: 0x0000DA48 File Offset: 0x0000BC48
		public UIntPtr(ulong value)
		{
			if (value > (ulong)-1 && UIntPtr.Size < 8)
			{
				throw new OverflowException(Locale.GetText("This isn't a 64bits machine."));
			}
			this._pointer = value;
		}

		// Token: 0x06000374 RID: 884 RVA: 0x0000DA78 File Offset: 0x0000BC78
		public UIntPtr(uint value)
		{
			this._pointer = value;
		}

		// Token: 0x06000375 RID: 885 RVA: 0x0000DA84 File Offset: 0x0000BC84
		[CLSCompliant(false)]
		public unsafe UIntPtr(void* value)
		{
			this._pointer = value;
		}

		// Token: 0x06000377 RID: 887 RVA: 0x0000DAA0 File Offset: 0x0000BCA0
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("pointer", this._pointer);
		}

		// Token: 0x06000378 RID: 888 RVA: 0x0000DAC8 File Offset: 0x0000BCC8
		public override bool Equals(object obj)
		{
			if (obj is UIntPtr)
			{
				UIntPtr uintPtr = (UIntPtr)obj;
				return this._pointer == uintPtr._pointer;
			}
			return false;
		}

		// Token: 0x06000379 RID: 889 RVA: 0x0000DAF8 File Offset: 0x0000BCF8
		public override int GetHashCode()
		{
			return this._pointer;
		}

		// Token: 0x0600037A RID: 890 RVA: 0x0000DB04 File Offset: 0x0000BD04
		public uint ToUInt32()
		{
			return this._pointer;
		}

		// Token: 0x0600037B RID: 891 RVA: 0x0000DB10 File Offset: 0x0000BD10
		public ulong ToUInt64()
		{
			return this._pointer;
		}

		// Token: 0x0600037C RID: 892 RVA: 0x0000DB1C File Offset: 0x0000BD1C
		[CLSCompliant(false)]
		public unsafe void* ToPointer()
		{
			return this._pointer;
		}

		// Token: 0x0600037D RID: 893 RVA: 0x0000DB24 File Offset: 0x0000BD24
		public override string ToString()
		{
			return this._pointer.ToString();
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x0600037E RID: 894 RVA: 0x0000DB40 File Offset: 0x0000BD40
		public unsafe static int Size
		{
			get
			{
				return sizeof(void*);
			}
		}

		// Token: 0x0600037F RID: 895 RVA: 0x0000DB48 File Offset: 0x0000BD48
		public static bool operator ==(UIntPtr value1, UIntPtr value2)
		{
			return value1._pointer == value2._pointer;
		}

		// Token: 0x06000380 RID: 896 RVA: 0x0000DB5C File Offset: 0x0000BD5C
		public static bool operator !=(UIntPtr value1, UIntPtr value2)
		{
			return value1._pointer != value2._pointer;
		}

		// Token: 0x06000381 RID: 897 RVA: 0x0000DB74 File Offset: 0x0000BD74
		public static explicit operator ulong(UIntPtr value)
		{
			return value._pointer;
		}

		// Token: 0x06000382 RID: 898 RVA: 0x0000DB80 File Offset: 0x0000BD80
		public static explicit operator uint(UIntPtr value)
		{
			return value._pointer;
		}

		// Token: 0x06000383 RID: 899 RVA: 0x0000DB8C File Offset: 0x0000BD8C
		public static explicit operator UIntPtr(ulong value)
		{
			return new UIntPtr(value);
		}

		// Token: 0x06000384 RID: 900 RVA: 0x0000DB94 File Offset: 0x0000BD94
		[CLSCompliant(false)]
		public unsafe static explicit operator UIntPtr(void* value)
		{
			return new UIntPtr(value);
		}

		// Token: 0x06000385 RID: 901 RVA: 0x0000DB9C File Offset: 0x0000BD9C
		[CLSCompliant(false)]
		public unsafe static explicit operator void*(UIntPtr value)
		{
			return value.ToPointer();
		}

		// Token: 0x06000386 RID: 902 RVA: 0x0000DBA8 File Offset: 0x0000BDA8
		public static explicit operator UIntPtr(uint value)
		{
			return new UIntPtr(value);
		}

		// Token: 0x04000058 RID: 88
		public static readonly UIntPtr Zero = new UIntPtr(0U);

		// Token: 0x04000059 RID: 89
		private unsafe void* _pointer;
	}
}
