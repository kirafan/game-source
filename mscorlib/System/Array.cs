﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000029 RID: 41
	[ComVisible(true)]
	[Serializable]
	public abstract class Array : IEnumerable, ICloneable, ICollection, IList
	{
		// Token: 0x060003EB RID: 1003 RVA: 0x0000FB28 File Offset: 0x0000DD28
		private Array()
		{
		}

		// Token: 0x17000010 RID: 16
		object IList.this[int index]
		{
			get
			{
				if (index >= this.Length)
				{
					throw new IndexOutOfRangeException("index");
				}
				if (this.Rank > 1)
				{
					throw new ArgumentException(Locale.GetText("Only single dimension arrays are supported."));
				}
				return this.GetValueImpl(index);
			}
			set
			{
				if (index >= this.Length)
				{
					throw new IndexOutOfRangeException("index");
				}
				if (this.Rank > 1)
				{
					throw new ArgumentException(Locale.GetText("Only single dimension arrays are supported."));
				}
				this.SetValueImpl(value, index);
			}
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x0000FBC0 File Offset: 0x0000DDC0
		int IList.Add(object value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x0000FBC8 File Offset: 0x0000DDC8
		void IList.Clear()
		{
			Array.Clear(this, this.GetLowerBound(0), this.Length);
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x0000FBE0 File Offset: 0x0000DDE0
		bool IList.Contains(object value)
		{
			if (this.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			int length = this.Length;
			for (int i = 0; i < length; i++)
			{
				if (object.Equals(this.GetValueImpl(i), value))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x0000FC38 File Offset: 0x0000DE38
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		int IList.IndexOf(object value)
		{
			if (this.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			int length = this.Length;
			for (int i = 0; i < length; i++)
			{
				if (object.Equals(this.GetValueImpl(i), value))
				{
					return i + this.GetLowerBound(0);
				}
			}
			return this.GetLowerBound(0) - 1;
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x0000FCA0 File Offset: 0x0000DEA0
		void IList.Insert(int index, object value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x0000FCA8 File Offset: 0x0000DEA8
		void IList.Remove(object value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x0000FCB0 File Offset: 0x0000DEB0
		void IList.RemoveAt(int index)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x060003F5 RID: 1013 RVA: 0x0000FCB8 File Offset: 0x0000DEB8
		int ICollection.Count
		{
			get
			{
				return this.Length;
			}
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x0000FCC0 File Offset: 0x0000DEC0
		internal int InternalArray__ICollection_get_Count()
		{
			return this.Length;
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x0000FCC8 File Offset: 0x0000DEC8
		internal bool InternalArray__ICollection_get_IsReadOnly()
		{
			return true;
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x0000FCCC File Offset: 0x0000DECC
		internal IEnumerator<T> InternalArray__IEnumerable_GetEnumerator<T>()
		{
			return new Array.InternalEnumerator<T>(this);
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x0000FCDC File Offset: 0x0000DEDC
		internal void InternalArray__ICollection_Clear()
		{
			throw new NotSupportedException("Collection is read-only");
		}

		// Token: 0x060003FA RID: 1018 RVA: 0x0000FCE8 File Offset: 0x0000DEE8
		internal void InternalArray__ICollection_Add<T>(T item)
		{
			throw new NotSupportedException("Collection is read-only");
		}

		// Token: 0x060003FB RID: 1019 RVA: 0x0000FCF4 File Offset: 0x0000DEF4
		internal bool InternalArray__ICollection_Remove<T>(T item)
		{
			throw new NotSupportedException("Collection is read-only");
		}

		// Token: 0x060003FC RID: 1020 RVA: 0x0000FD00 File Offset: 0x0000DF00
		internal bool InternalArray__ICollection_Contains<T>(T item)
		{
			if (this.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			int length = this.Length;
			for (int i = 0; i < length; i++)
			{
				T t;
				this.GetGenericValueImpl<T>(i, out t);
				if (item == null)
				{
					return t == null;
				}
				if (item.Equals(t))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060003FD RID: 1021 RVA: 0x0000FD80 File Offset: 0x0000DF80
		internal void InternalArray__ICollection_CopyTo<T>(T[] array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (this.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (index + this.GetLength(0) > array.GetLowerBound(0) + array.GetLength(0))
			{
				throw new ArgumentException("Destination array was not long enough. Check destIndex and length, and the array's lower bounds.");
			}
			if (array.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("Value has to be >= 0."));
			}
			Array.Copy(this, this.GetLowerBound(0), array, index, this.GetLength(0));
		}

		// Token: 0x060003FE RID: 1022 RVA: 0x0000FE30 File Offset: 0x0000E030
		internal void InternalArray__Insert<T>(int index, T item)
		{
			throw new NotSupportedException("Collection is read-only");
		}

		// Token: 0x060003FF RID: 1023 RVA: 0x0000FE3C File Offset: 0x0000E03C
		internal void InternalArray__RemoveAt(int index)
		{
			throw new NotSupportedException("Collection is read-only");
		}

		// Token: 0x06000400 RID: 1024 RVA: 0x0000FE48 File Offset: 0x0000E048
		internal int InternalArray__IndexOf<T>(T item)
		{
			if (this.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			int length = this.Length;
			int i = 0;
			while (i < length)
			{
				T t;
				this.GetGenericValueImpl<T>(i, out t);
				if (item == null)
				{
					if (t == null)
					{
						return i + this.GetLowerBound(0);
					}
					return this.GetLowerBound(0) - 1;
				}
				else
				{
					if (t.Equals(item))
					{
						return i + this.GetLowerBound(0);
					}
					i++;
				}
			}
			return this.GetLowerBound(0) - 1;
		}

		// Token: 0x06000401 RID: 1025 RVA: 0x0000FEE8 File Offset: 0x0000E0E8
		internal T InternalArray__get_Item<T>(int index)
		{
			if (index >= this.Length)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			T result;
			this.GetGenericValueImpl<T>(index, out result);
			return result;
		}

		// Token: 0x06000402 RID: 1026 RVA: 0x0000FF18 File Offset: 0x0000E118
		internal void InternalArray__set_Item<T>(int index, T item)
		{
			if (index >= this.Length)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			object[] array = this as object[];
			if (array != null)
			{
				array[index] = item;
				return;
			}
			this.SetGenericValueImpl<T>(index, ref item);
		}

		// Token: 0x06000403 RID: 1027
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void GetGenericValueImpl<T>(int pos, out T value);

		// Token: 0x06000404 RID: 1028
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetGenericValueImpl<T>(int pos, ref T value);

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000405 RID: 1029 RVA: 0x0000FF5C File Offset: 0x0000E15C
		public int Length
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				int num = this.GetLength(0);
				for (int i = 1; i < this.Rank; i++)
				{
					num *= this.GetLength(i);
				}
				return num;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000406 RID: 1030 RVA: 0x0000FF94 File Offset: 0x0000E194
		[ComVisible(false)]
		public long LongLength
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return (long)this.Length;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000407 RID: 1031 RVA: 0x0000FFA0 File Offset: 0x0000E1A0
		public int Rank
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return this.GetRank();
			}
		}

		// Token: 0x06000408 RID: 1032
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetRank();

		// Token: 0x06000409 RID: 1033
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetLength(int dimension);

		// Token: 0x0600040A RID: 1034 RVA: 0x0000FFA8 File Offset: 0x0000E1A8
		[ComVisible(false)]
		public long GetLongLength(int dimension)
		{
			return (long)this.GetLength(dimension);
		}

		// Token: 0x0600040B RID: 1035
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern int GetLowerBound(int dimension);

		// Token: 0x0600040C RID: 1036
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern object GetValue(params int[] indices);

		// Token: 0x0600040D RID: 1037
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern void SetValue(object value, params int[] indices);

		// Token: 0x0600040E RID: 1038
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern object GetValueImpl(int pos);

		// Token: 0x0600040F RID: 1039
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void SetValueImpl(object value, int pos);

		// Token: 0x06000410 RID: 1040
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool FastCopy(Array source, int source_idx, Array dest, int dest_idx, int length);

		// Token: 0x06000411 RID: 1041
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern Array CreateInstanceImpl(Type elementType, int[] lengths, int[] bounds);

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000412 RID: 1042 RVA: 0x0000FFB4 File Offset: 0x0000E1B4
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000413 RID: 1043 RVA: 0x0000FFB8 File Offset: 0x0000E1B8
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000414 RID: 1044 RVA: 0x0000FFBC File Offset: 0x0000E1BC
		public bool IsFixedSize
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000415 RID: 1045 RVA: 0x0000FFC0 File Offset: 0x0000E1C0
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000416 RID: 1046 RVA: 0x0000FFC4 File Offset: 0x0000E1C4
		public IEnumerator GetEnumerator()
		{
			return new Array.SimpleEnumerator(this);
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x0000FFCC File Offset: 0x0000E1CC
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public int GetUpperBound(int dimension)
		{
			return this.GetLowerBound(dimension) + this.GetLength(dimension) - 1;
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x0000FFE0 File Offset: 0x0000E1E0
		public object GetValue(int index)
		{
			if (this.Rank != 1)
			{
				throw new ArgumentException(Locale.GetText("Array was not a one-dimensional array."));
			}
			if (index < this.GetLowerBound(0) || index > this.GetUpperBound(0))
			{
				throw new IndexOutOfRangeException(Locale.GetText("Index has to be between upper and lower bound of the array."));
			}
			return this.GetValueImpl(index - this.GetLowerBound(0));
		}

		// Token: 0x06000419 RID: 1049 RVA: 0x00010044 File Offset: 0x0000E244
		public object GetValue(int index1, int index2)
		{
			int[] indices = new int[]
			{
				index1,
				index2
			};
			return this.GetValue(indices);
		}

		// Token: 0x0600041A RID: 1050 RVA: 0x00010068 File Offset: 0x0000E268
		public object GetValue(int index1, int index2, int index3)
		{
			int[] indices = new int[]
			{
				index1,
				index2,
				index3
			};
			return this.GetValue(indices);
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x00010090 File Offset: 0x0000E290
		[ComVisible(false)]
		public object GetValue(long index)
		{
			if (index < 0L || index > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			return this.GetValue((int)index);
		}

		// Token: 0x0600041C RID: 1052 RVA: 0x000100C4 File Offset: 0x0000E2C4
		[ComVisible(false)]
		public object GetValue(long index1, long index2)
		{
			if (index1 < 0L || index1 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index1", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			if (index2 < 0L || index2 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index2", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			return this.GetValue((int)index1, (int)index2);
		}

		// Token: 0x0600041D RID: 1053 RVA: 0x00010130 File Offset: 0x0000E330
		[ComVisible(false)]
		public object GetValue(long index1, long index2, long index3)
		{
			if (index1 < 0L || index1 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index1", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			if (index2 < 0L || index2 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index2", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			if (index3 < 0L || index3 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index3", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			return this.GetValue((int)index1, (int)index2, (int)index3);
		}

		// Token: 0x0600041E RID: 1054 RVA: 0x000101C4 File Offset: 0x0000E3C4
		[ComVisible(false)]
		public void SetValue(object value, long index)
		{
			if (index < 0L || index > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			this.SetValue(value, (int)index);
		}

		// Token: 0x0600041F RID: 1055 RVA: 0x00010204 File Offset: 0x0000E404
		[ComVisible(false)]
		public void SetValue(object value, long index1, long index2)
		{
			if (index1 < 0L || index1 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index1", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			if (index2 < 0L || index2 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index2", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			int[] indices = new int[]
			{
				(int)index1,
				(int)index2
			};
			this.SetValue(value, indices);
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x0001027C File Offset: 0x0000E47C
		[ComVisible(false)]
		public void SetValue(object value, long index1, long index2, long index3)
		{
			if (index1 < 0L || index1 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index1", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			if (index2 < 0L || index2 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index2", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			if (index3 < 0L || index3 > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index3", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			int[] indices = new int[]
			{
				(int)index1,
				(int)index2,
				(int)index3
			};
			this.SetValue(value, indices);
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x00010328 File Offset: 0x0000E528
		public void SetValue(object value, int index)
		{
			if (this.Rank != 1)
			{
				throw new ArgumentException(Locale.GetText("Array was not a one-dimensional array."));
			}
			if (index < this.GetLowerBound(0) || index > this.GetUpperBound(0))
			{
				throw new IndexOutOfRangeException(Locale.GetText("Index has to be >= lower bound and <= upper bound of the array."));
			}
			this.SetValueImpl(value, index - this.GetLowerBound(0));
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x0001038C File Offset: 0x0000E58C
		public void SetValue(object value, int index1, int index2)
		{
			int[] indices = new int[]
			{
				index1,
				index2
			};
			this.SetValue(value, indices);
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x000103B0 File Offset: 0x0000E5B0
		public void SetValue(object value, int index1, int index2, int index3)
		{
			int[] indices = new int[]
			{
				index1,
				index2,
				index3
			};
			this.SetValue(value, indices);
		}

		// Token: 0x06000424 RID: 1060 RVA: 0x000103DC File Offset: 0x0000E5DC
		public static Array CreateInstance(Type elementType, int length)
		{
			int[] lengths = new int[]
			{
				length
			};
			return Array.CreateInstance(elementType, lengths);
		}

		// Token: 0x06000425 RID: 1061 RVA: 0x000103FC File Offset: 0x0000E5FC
		public static Array CreateInstance(Type elementType, int length1, int length2)
		{
			int[] lengths = new int[]
			{
				length1,
				length2
			};
			return Array.CreateInstance(elementType, lengths);
		}

		// Token: 0x06000426 RID: 1062 RVA: 0x00010420 File Offset: 0x0000E620
		public static Array CreateInstance(Type elementType, int length1, int length2, int length3)
		{
			int[] lengths = new int[]
			{
				length1,
				length2,
				length3
			};
			return Array.CreateInstance(elementType, lengths);
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x00010448 File Offset: 0x0000E648
		public static Array CreateInstance(Type elementType, params int[] lengths)
		{
			if (elementType == null)
			{
				throw new ArgumentNullException("elementType");
			}
			if (lengths == null)
			{
				throw new ArgumentNullException("lengths");
			}
			if (lengths.Length > 255)
			{
				throw new TypeLoadException();
			}
			int[] bounds = null;
			elementType = elementType.UnderlyingSystemType;
			if (!elementType.IsSystemType)
			{
				throw new ArgumentException("Type must be a type provided by the runtime.", "elementType");
			}
			if (elementType.Equals(typeof(void)))
			{
				throw new NotSupportedException("Array type can not be void");
			}
			if (elementType.ContainsGenericParameters)
			{
				throw new NotSupportedException("Array type can not be an open generic type");
			}
			return Array.CreateInstanceImpl(elementType, lengths, bounds);
		}

		// Token: 0x06000428 RID: 1064 RVA: 0x000104F0 File Offset: 0x0000E6F0
		public static Array CreateInstance(Type elementType, int[] lengths, int[] lowerBounds)
		{
			if (elementType == null)
			{
				throw new ArgumentNullException("elementType");
			}
			if (lengths == null)
			{
				throw new ArgumentNullException("lengths");
			}
			if (lowerBounds == null)
			{
				throw new ArgumentNullException("lowerBounds");
			}
			elementType = elementType.UnderlyingSystemType;
			if (!elementType.IsSystemType)
			{
				throw new ArgumentException("Type must be a type provided by the runtime.", "elementType");
			}
			if (elementType.Equals(typeof(void)))
			{
				throw new NotSupportedException("Array type can not be void");
			}
			if (elementType.ContainsGenericParameters)
			{
				throw new NotSupportedException("Array type can not be an open generic type");
			}
			if (lengths.Length < 1)
			{
				throw new ArgumentException(Locale.GetText("Arrays must contain >= 1 elements."));
			}
			if (lengths.Length != lowerBounds.Length)
			{
				throw new ArgumentException(Locale.GetText("Arrays must be of same size."));
			}
			for (int i = 0; i < lowerBounds.Length; i++)
			{
				if (lengths[i] < 0)
				{
					throw new ArgumentOutOfRangeException("lengths", Locale.GetText("Each value has to be >= 0."));
				}
				if ((long)lowerBounds[i] + (long)lengths[i] > 2147483647L)
				{
					throw new ArgumentOutOfRangeException("lengths", Locale.GetText("Length + bound must not exceed Int32.MaxValue."));
				}
			}
			if (lengths.Length > 255)
			{
				throw new TypeLoadException();
			}
			return Array.CreateInstanceImpl(elementType, lengths, lowerBounds);
		}

		// Token: 0x06000429 RID: 1065 RVA: 0x00010634 File Offset: 0x0000E834
		private static int[] GetIntArray(long[] values)
		{
			int num = values.Length;
			int[] array = new int[num];
			for (int i = 0; i < num; i++)
			{
				long num2 = values[i];
				if (num2 < 0L || num2 > 2147483647L)
				{
					throw new ArgumentOutOfRangeException("values", Locale.GetText("Each value has to be >= 0 and <= Int32.MaxValue."));
				}
				array[i] = (int)num2;
			}
			return array;
		}

		// Token: 0x0600042A RID: 1066 RVA: 0x00010694 File Offset: 0x0000E894
		public static Array CreateInstance(Type elementType, params long[] lengths)
		{
			if (lengths == null)
			{
				throw new ArgumentNullException("lengths");
			}
			return Array.CreateInstance(elementType, Array.GetIntArray(lengths));
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x000106B4 File Offset: 0x0000E8B4
		[ComVisible(false)]
		public object GetValue(params long[] indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			return this.GetValue(Array.GetIntArray(indices));
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x000106D4 File Offset: 0x0000E8D4
		[ComVisible(false)]
		public void SetValue(object value, params long[] indices)
		{
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			this.SetValue(value, Array.GetIntArray(indices));
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x000106F4 File Offset: 0x0000E8F4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int BinarySearch(Array array, object value)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (value == null)
			{
				return -1;
			}
			if (array.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (array.Length == 0)
			{
				return -1;
			}
			if (!(value is IComparable))
			{
				throw new ArgumentException(Locale.GetText("value does not support IComparable."));
			}
			return Array.DoBinarySearch(array, array.GetLowerBound(0), array.GetLength(0), value, null);
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x00010774 File Offset: 0x0000E974
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int BinarySearch(Array array, object value, IComparer comparer)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (array.Length == 0)
			{
				return -1;
			}
			if (comparer == null && value != null && !(value is IComparable))
			{
				throw new ArgumentException(Locale.GetText("comparer is null and value does not support IComparable."));
			}
			return Array.DoBinarySearch(array, array.GetLowerBound(0), array.GetLength(0), value, comparer);
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x000107F8 File Offset: 0x0000E9F8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int BinarySearch(Array array, int index, int length, object value)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (index < array.GetLowerBound(0))
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("index is less than the lower bound of array."));
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length", Locale.GetText("Value has to be >= 0."));
			}
			if (index > array.GetLowerBound(0) + array.GetLength(0) - length)
			{
				throw new ArgumentException(Locale.GetText("index and length do not specify a valid range in array."));
			}
			if (array.Length == 0)
			{
				return -1;
			}
			if (value != null && !(value is IComparable))
			{
				throw new ArgumentException(Locale.GetText("value does not support IComparable"));
			}
			return Array.DoBinarySearch(array, index, length, value, null);
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x000108D0 File Offset: 0x0000EAD0
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int BinarySearch(Array array, int index, int length, object value, IComparer comparer)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (index < array.GetLowerBound(0))
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("index is less than the lower bound of array."));
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length", Locale.GetText("Value has to be >= 0."));
			}
			if (index > array.GetLowerBound(0) + array.GetLength(0) - length)
			{
				throw new ArgumentException(Locale.GetText("index and length do not specify a valid range in array."));
			}
			if (array.Length == 0)
			{
				return -1;
			}
			if (comparer == null && value != null && !(value is IComparable))
			{
				throw new ArgumentException(Locale.GetText("comparer is null and value does not support IComparable."));
			}
			return Array.DoBinarySearch(array, index, length, value, comparer);
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x000109B0 File Offset: 0x0000EBB0
		private static int DoBinarySearch(Array array, int index, int length, object value, IComparer comparer)
		{
			if (comparer == null)
			{
				comparer = Comparer.Default;
			}
			int i = index;
			int num = index + length - 1;
			try
			{
				while (i <= num)
				{
					int num2 = i + (num - i) / 2;
					object valueImpl = array.GetValueImpl(num2);
					int num3 = comparer.Compare(valueImpl, value);
					if (num3 == 0)
					{
						return num2;
					}
					if (num3 > 0)
					{
						num = num2 - 1;
					}
					else
					{
						i = num2 + 1;
					}
				}
			}
			catch (Exception innerException)
			{
				throw new InvalidOperationException(Locale.GetText("Comparer threw an exception."), innerException);
			}
			return ~i;
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x00010A60 File Offset: 0x0000EC60
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static void Clear(Array array, int index, int length)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (length < 0)
			{
				throw new IndexOutOfRangeException("length < 0");
			}
			int lowerBound = array.GetLowerBound(0);
			if (index < lowerBound)
			{
				throw new IndexOutOfRangeException("index < lower bound");
			}
			index -= lowerBound;
			if (index > array.Length - length)
			{
				throw new IndexOutOfRangeException("index + length > size");
			}
			Array.ClearInternal(array, index, length);
		}

		// Token: 0x06000433 RID: 1075
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ClearInternal(Array a, int index, int count);

		// Token: 0x06000434 RID: 1076
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern object Clone();

		// Token: 0x06000435 RID: 1077 RVA: 0x00010AD0 File Offset: 0x0000ECD0
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Copy(Array sourceArray, Array destinationArray, int length)
		{
			if (sourceArray == null)
			{
				throw new ArgumentNullException("sourceArray");
			}
			if (destinationArray == null)
			{
				throw new ArgumentNullException("destinationArray");
			}
			Array.Copy(sourceArray, sourceArray.GetLowerBound(0), destinationArray, destinationArray.GetLowerBound(0), length);
		}

		// Token: 0x06000436 RID: 1078 RVA: 0x00010B18 File Offset: 0x0000ED18
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Copy(Array sourceArray, int sourceIndex, Array destinationArray, int destinationIndex, int length)
		{
			if (sourceArray == null)
			{
				throw new ArgumentNullException("sourceArray");
			}
			if (destinationArray == null)
			{
				throw new ArgumentNullException("destinationArray");
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length", Locale.GetText("Value has to be >= 0."));
			}
			if (sourceIndex < 0)
			{
				throw new ArgumentOutOfRangeException("sourceIndex", Locale.GetText("Value has to be >= 0."));
			}
			if (destinationIndex < 0)
			{
				throw new ArgumentOutOfRangeException("destinationIndex", Locale.GetText("Value has to be >= 0."));
			}
			if (Array.FastCopy(sourceArray, sourceIndex, destinationArray, destinationIndex, length))
			{
				return;
			}
			int num = sourceIndex - sourceArray.GetLowerBound(0);
			int num2 = destinationIndex - destinationArray.GetLowerBound(0);
			if (num > sourceArray.Length - length)
			{
				throw new ArgumentException("length");
			}
			if (num2 > destinationArray.Length - length)
			{
				string message = "Destination array was not long enough. Check destIndex and length, and the array's lower bounds";
				throw new ArgumentException(message, string.Empty);
			}
			if (sourceArray.Rank != destinationArray.Rank)
			{
				throw new RankException(Locale.GetText("Arrays must be of same size."));
			}
			Type elementType = sourceArray.GetType().GetElementType();
			Type elementType2 = destinationArray.GetType().GetElementType();
			if (!object.ReferenceEquals(sourceArray, destinationArray) || num > num2)
			{
				for (int i = 0; i < length; i++)
				{
					object valueImpl = sourceArray.GetValueImpl(num + i);
					try
					{
						destinationArray.SetValueImpl(valueImpl, num2 + i);
					}
					catch
					{
						if (elementType.Equals(typeof(object)))
						{
							throw new InvalidCastException();
						}
						throw new ArrayTypeMismatchException(string.Format(Locale.GetText("(Types: source={0};  target={1})"), elementType.FullName, elementType2.FullName));
					}
				}
			}
			else
			{
				for (int j = length - 1; j >= 0; j--)
				{
					object valueImpl2 = sourceArray.GetValueImpl(num + j);
					try
					{
						destinationArray.SetValueImpl(valueImpl2, num2 + j);
					}
					catch
					{
						if (elementType.Equals(typeof(object)))
						{
							throw new InvalidCastException();
						}
						throw new ArrayTypeMismatchException(string.Format(Locale.GetText("(Types: source={0};  target={1})"), elementType.FullName, elementType2.FullName));
					}
				}
			}
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x00010D74 File Offset: 0x0000EF74
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Copy(Array sourceArray, long sourceIndex, Array destinationArray, long destinationIndex, long length)
		{
			if (sourceArray == null)
			{
				throw new ArgumentNullException("sourceArray");
			}
			if (destinationArray == null)
			{
				throw new ArgumentNullException("destinationArray");
			}
			if (sourceIndex < -2147483648L || sourceIndex > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("sourceIndex", Locale.GetText("Must be in the Int32 range."));
			}
			if (destinationIndex < -2147483648L || destinationIndex > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("destinationIndex", Locale.GetText("Must be in the Int32 range."));
			}
			if (length < 0L || length > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("length", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			Array.Copy(sourceArray, (int)sourceIndex, destinationArray, (int)destinationIndex, (int)length);
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x00010E38 File Offset: 0x0000F038
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Copy(Array sourceArray, Array destinationArray, long length)
		{
			if (length < 0L || length > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("length", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			Array.Copy(sourceArray, destinationArray, (int)length);
		}

		// Token: 0x06000439 RID: 1081 RVA: 0x00010E78 File Offset: 0x0000F078
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int IndexOf(Array array, object value)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.IndexOf(array, value, 0, array.Length);
		}

		// Token: 0x0600043A RID: 1082 RVA: 0x00010E9C File Offset: 0x0000F09C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int IndexOf(Array array, object value, int startIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.IndexOf(array, value, startIndex, array.Length - startIndex);
		}

		// Token: 0x0600043B RID: 1083 RVA: 0x00010EC0 File Offset: 0x0000F0C0
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int IndexOf(Array array, object value, int startIndex, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (count < 0 || startIndex < array.GetLowerBound(0) || startIndex - 1 > array.GetUpperBound(0) - count)
			{
				throw new ArgumentOutOfRangeException();
			}
			int num = startIndex + count;
			for (int i = startIndex; i < num; i++)
			{
				if (object.Equals(array.GetValueImpl(i), value))
				{
					return i;
				}
			}
			return array.GetLowerBound(0) - 1;
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x00010F58 File Offset: 0x0000F158
		public void Initialize()
		{
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x00010F5C File Offset: 0x0000F15C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int LastIndexOf(Array array, object value)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Length == 0)
			{
				return array.GetLowerBound(0) - 1;
			}
			return Array.LastIndexOf(array, value, array.Length - 1);
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x00010FA0 File Offset: 0x0000F1A0
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int LastIndexOf(Array array, object value, int startIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.LastIndexOf(array, value, startIndex, startIndex - array.GetLowerBound(0) + 1);
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x00010FD4 File Offset: 0x0000F1D4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int LastIndexOf(Array array, object value, int startIndex, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			int lowerBound = array.GetLowerBound(0);
			if (array.Length == 0)
			{
				return lowerBound - 1;
			}
			if (count < 0 || startIndex < lowerBound || startIndex > array.GetUpperBound(0) || startIndex - count + 1 < lowerBound)
			{
				throw new ArgumentOutOfRangeException();
			}
			for (int i = startIndex; i >= startIndex - count + 1; i--)
			{
				if (object.Equals(array.GetValueImpl(i), value))
				{
					return i;
				}
			}
			return lowerBound - 1;
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x00011080 File Offset: 0x0000F280
		private static Array.Swapper get_swapper(Array array)
		{
			if (array is int[])
			{
				return new Array.Swapper(array.int_swapper);
			}
			if (array is double[])
			{
				return new Array.Swapper(array.double_swapper);
			}
			if (array is object[])
			{
				return new Array.Swapper(array.obj_swapper);
			}
			return new Array.Swapper(array.slow_swapper);
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x000110E4 File Offset: 0x0000F2E4
		private static Array.Swapper get_swapper<T>(T[] array)
		{
			if (array is int[])
			{
				return new Array.Swapper(array.int_swapper);
			}
			if (array is double[])
			{
				return new Array.Swapper(array.double_swapper);
			}
			return new Array.Swapper(array.slow_swapper);
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x00011130 File Offset: 0x0000F330
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Reverse(Array array)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			Array.Reverse(array, array.GetLowerBound(0), array.GetLength(0));
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x00011158 File Offset: 0x0000F358
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Reverse(Array array, int index, int length)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (index < array.GetLowerBound(0) || length < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (index > array.GetUpperBound(0) + 1 - length)
			{
				throw new ArgumentException();
			}
			int num = index + length - 1;
			object[] array2 = array as object[];
			if (array2 != null)
			{
				while (index < num)
				{
					object obj = array2[index];
					array2[index] = array2[num];
					array2[num] = obj;
					index++;
					num--;
				}
				return;
			}
			int[] array3 = array as int[];
			if (array3 != null)
			{
				while (index < num)
				{
					int num2 = array3[index];
					array3[index] = array3[num];
					array3[num] = num2;
					index++;
					num--;
				}
				return;
			}
			double[] array4 = array as double[];
			if (array4 != null)
			{
				while (index < num)
				{
					double num3 = array4[index];
					array4[index] = array4[num];
					array4[num] = num3;
					index++;
					num--;
				}
				return;
			}
			Array.Swapper swapper = Array.get_swapper(array);
			while (index < num)
			{
				swapper(index, num);
				index++;
				num--;
			}
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x0001128C File Offset: 0x0000F48C
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort(Array array)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			Array.Sort(array, null, array.GetLowerBound(0), array.GetLength(0), null);
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x000112C0 File Offset: 0x0000F4C0
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort(Array keys, Array items)
		{
			if (keys == null)
			{
				throw new ArgumentNullException("keys");
			}
			Array.Sort(keys, items, keys.GetLowerBound(0), keys.GetLength(0), null);
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x000112F4 File Offset: 0x0000F4F4
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort(Array array, IComparer comparer)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			Array.Sort(array, null, array.GetLowerBound(0), array.GetLength(0), comparer);
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x00011328 File Offset: 0x0000F528
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort(Array array, int index, int length)
		{
			Array.Sort(array, null, index, length, null);
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x00011334 File Offset: 0x0000F534
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort(Array keys, Array items, IComparer comparer)
		{
			if (keys == null)
			{
				throw new ArgumentNullException("keys");
			}
			Array.Sort(keys, items, keys.GetLowerBound(0), keys.GetLength(0), comparer);
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x00011368 File Offset: 0x0000F568
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort(Array keys, Array items, int index, int length)
		{
			Array.Sort(keys, items, index, length, null);
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x00011374 File Offset: 0x0000F574
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort(Array array, int index, int length, IComparer comparer)
		{
			Array.Sort(array, null, index, length, comparer);
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x00011380 File Offset: 0x0000F580
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort(Array keys, Array items, int index, int length, IComparer comparer)
		{
			if (keys == null)
			{
				throw new ArgumentNullException("keys");
			}
			if (keys.Rank > 1 || (items != null && items.Rank > 1))
			{
				throw new RankException();
			}
			if (items != null && keys.GetLowerBound(0) != items.GetLowerBound(0))
			{
				throw new ArgumentException();
			}
			if (index < keys.GetLowerBound(0))
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length", Locale.GetText("Value has to be >= 0."));
			}
			if (keys.Length - (index + keys.GetLowerBound(0)) < length || (items != null && index > items.Length - length))
			{
				throw new ArgumentException();
			}
			if (length <= 1)
			{
				return;
			}
			if (comparer == null)
			{
				Array.Swapper swap_items;
				if (items == null)
				{
					swap_items = null;
				}
				else
				{
					swap_items = Array.get_swapper(items);
				}
				if (keys is double[])
				{
					Array.combsort(keys as double[], index, length, swap_items);
					return;
				}
				if (!(keys is uint[]) && keys is int[])
				{
					Array.combsort(keys as int[], index, length, swap_items);
					return;
				}
				if (keys is char[])
				{
					Array.combsort(keys as char[], index, length, swap_items);
					return;
				}
			}
			try
			{
				int high = index + length - 1;
				Array.qsort(keys, items, index, high, comparer);
			}
			catch (Exception innerException)
			{
				throw new InvalidOperationException(Locale.GetText("The comparer threw an exception."), innerException);
			}
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x0001150C File Offset: 0x0000F70C
		private void int_swapper(int i, int j)
		{
			int[] array = this as int[];
			int num = array[i];
			array[i] = array[j];
			array[j] = num;
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x00011530 File Offset: 0x0000F730
		private void obj_swapper(int i, int j)
		{
			object[] array = this as object[];
			object obj = array[i];
			array[i] = array[j];
			array[j] = obj;
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x00011554 File Offset: 0x0000F754
		private void slow_swapper(int i, int j)
		{
			object valueImpl = this.GetValueImpl(i);
			this.SetValueImpl(this.GetValue(j), i);
			this.SetValueImpl(valueImpl, j);
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x00011580 File Offset: 0x0000F780
		private void double_swapper(int i, int j)
		{
			double[] array = this as double[];
			double num = array[i];
			array[i] = array[j];
			array[j] = num;
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x000115A4 File Offset: 0x0000F7A4
		private static int new_gap(int gap)
		{
			gap = gap * 10 / 13;
			if (gap == 9 || gap == 10)
			{
				return 11;
			}
			if (gap < 1)
			{
				return 1;
			}
			return gap;
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x000115D8 File Offset: 0x0000F7D8
		private static void combsort(double[] array, int start, int size, Array.Swapper swap_items)
		{
			int num = size;
			bool flag;
			do
			{
				num = Array.new_gap(num);
				flag = false;
				int num2 = start + size - num;
				for (int i = start; i < num2; i++)
				{
					int num3 = i + num;
					if (array[i] > array[num3])
					{
						double num4 = array[i];
						array[i] = array[num3];
						array[num3] = num4;
						flag = true;
						if (swap_items != null)
						{
							swap_items(i, num3);
						}
					}
				}
			}
			while (num != 1 || flag);
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x00011654 File Offset: 0x0000F854
		private static void combsort(int[] array, int start, int size, Array.Swapper swap_items)
		{
			int num = size;
			bool flag;
			do
			{
				num = Array.new_gap(num);
				flag = false;
				int num2 = start + size - num;
				for (int i = start; i < num2; i++)
				{
					int num3 = i + num;
					if (array[i] > array[num3])
					{
						int num4 = array[i];
						array[i] = array[num3];
						array[num3] = num4;
						flag = true;
						if (swap_items != null)
						{
							swap_items(i, num3);
						}
					}
				}
			}
			while (num != 1 || flag);
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x000116D0 File Offset: 0x0000F8D0
		private static void combsort(char[] array, int start, int size, Array.Swapper swap_items)
		{
			int num = size;
			bool flag;
			do
			{
				num = Array.new_gap(num);
				flag = false;
				int num2 = start + size - num;
				for (int i = start; i < num2; i++)
				{
					int num3 = i + num;
					if (array[i] > array[num3])
					{
						char c = array[i];
						array[i] = array[num3];
						array[num3] = c;
						flag = true;
						if (swap_items != null)
						{
							swap_items(i, num3);
						}
					}
				}
			}
			while (num != 1 || flag);
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x0001174C File Offset: 0x0000F94C
		private static void qsort(Array keys, Array items, int low0, int high0, IComparer comparer)
		{
			if (low0 >= high0)
			{
				return;
			}
			int num = low0;
			int num2 = high0;
			int pos = num + (num2 - num) / 2;
			object valueImpl = keys.GetValueImpl(pos);
			for (;;)
			{
				while (num < high0 && Array.compare(keys.GetValueImpl(num), valueImpl, comparer) < 0)
				{
					num++;
				}
				while (num2 > low0 && Array.compare(valueImpl, keys.GetValueImpl(num2), comparer) < 0)
				{
					num2--;
				}
				if (num > num2)
				{
					break;
				}
				Array.swap(keys, items, num, num2);
				num++;
				num2--;
			}
			if (low0 < num2)
			{
				Array.qsort(keys, items, low0, num2, comparer);
			}
			if (num < high0)
			{
				Array.qsort(keys, items, num, high0, comparer);
			}
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x0001180C File Offset: 0x0000FA0C
		private static void swap(Array keys, Array items, int i, int j)
		{
			object valueImpl = keys.GetValueImpl(i);
			keys.SetValueImpl(keys.GetValue(j), i);
			keys.SetValueImpl(valueImpl, j);
			if (items != null)
			{
				valueImpl = items.GetValueImpl(i);
				items.SetValueImpl(items.GetValueImpl(j), i);
				items.SetValueImpl(valueImpl, j);
			}
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x0001185C File Offset: 0x0000FA5C
		private static int compare(object value1, object value2, IComparer comparer)
		{
			if (value1 == null)
			{
				return (value2 != null) ? -1 : 0;
			}
			if (value2 == null)
			{
				return 1;
			}
			if (comparer == null)
			{
				return ((IComparable)value1).CompareTo(value2);
			}
			return comparer.Compare(value1, value2);
		}

		// Token: 0x06000457 RID: 1111 RVA: 0x000118A0 File Offset: 0x0000FAA0
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort<T>(T[] array)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			Array.Sort<T, T>(array, null, 0, array.Length, null);
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x000118C0 File Offset: 0x0000FAC0
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort<TKey, TValue>(TKey[] keys, TValue[] items)
		{
			if (keys == null)
			{
				throw new ArgumentNullException("keys");
			}
			Array.Sort<TKey, TValue>(keys, items, 0, keys.Length, null);
		}

		// Token: 0x06000459 RID: 1113 RVA: 0x000118E0 File Offset: 0x0000FAE0
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort<T>(T[] array, IComparer<T> comparer)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			Array.Sort<T, T>(array, null, 0, array.Length, comparer);
		}

		// Token: 0x0600045A RID: 1114 RVA: 0x00011900 File Offset: 0x0000FB00
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort<TKey, TValue>(TKey[] keys, TValue[] items, IComparer<TKey> comparer)
		{
			if (keys == null)
			{
				throw new ArgumentNullException("keys");
			}
			Array.Sort<TKey, TValue>(keys, items, 0, keys.Length, comparer);
		}

		// Token: 0x0600045B RID: 1115 RVA: 0x00011920 File Offset: 0x0000FB20
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort<T>(T[] array, int index, int length)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			Array.Sort<T, T>(array, null, index, length, null);
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x00011940 File Offset: 0x0000FB40
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort<TKey, TValue>(TKey[] keys, TValue[] items, int index, int length)
		{
			Array.Sort<TKey, TValue>(keys, items, index, length, null);
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x0001194C File Offset: 0x0000FB4C
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort<T>(T[] array, int index, int length, IComparer<T> comparer)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			Array.Sort<T, T>(array, null, index, length, comparer);
		}

		// Token: 0x0600045E RID: 1118 RVA: 0x0001196C File Offset: 0x0000FB6C
		[ReliabilityContract(Consistency.MayCorruptInstance, Cer.MayFail)]
		public static void Sort<TKey, TValue>(TKey[] keys, TValue[] items, int index, int length, IComparer<TKey> comparer)
		{
			if (keys == null)
			{
				throw new ArgumentNullException("keys");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length");
			}
			if (keys.Length - index < length || (items != null && index > items.Length - length))
			{
				throw new ArgumentException();
			}
			if (length <= 1)
			{
				return;
			}
			if (comparer == null)
			{
				Array.Swapper swap_items;
				if (items == null)
				{
					swap_items = null;
				}
				else
				{
					swap_items = Array.get_swapper<TValue>(items);
				}
				if (keys is double[])
				{
					Array.combsort(keys as double[], index, length, swap_items);
					return;
				}
				if (!(keys is uint[]) && keys is int[])
				{
					Array.combsort(keys as int[], index, length, swap_items);
					return;
				}
				if (keys is char[])
				{
					Array.combsort(keys as char[], index, length, swap_items);
					return;
				}
			}
			try
			{
				int high = index + length - 1;
				Array.qsort<TKey, TValue>(keys, items, index, high, comparer);
			}
			catch (Exception innerException)
			{
				throw new InvalidOperationException(Locale.GetText("The comparer threw an exception."), innerException);
			}
		}

		// Token: 0x0600045F RID: 1119 RVA: 0x00011AA8 File Offset: 0x0000FCA8
		public static void Sort<T>(T[] array, Comparison<T> comparison)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			Array.Sort<T>(array, array.Length, comparison);
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x00011AC8 File Offset: 0x0000FCC8
		internal static void Sort<T>(T[] array, int length, Comparison<T> comparison)
		{
			if (comparison == null)
			{
				throw new ArgumentNullException("comparison");
			}
			if (length <= 1 || array.Length <= 1)
			{
				return;
			}
			try
			{
				int low = 0;
				int high = length - 1;
				Array.qsort<T>(array, low, high, comparison);
			}
			catch (Exception innerException)
			{
				throw new InvalidOperationException(Locale.GetText("Comparison threw an exception."), innerException);
			}
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x00011B40 File Offset: 0x0000FD40
		private static void qsort<K, V>(K[] keys, V[] items, int low0, int high0, IComparer<K> comparer)
		{
			if (low0 >= high0)
			{
				return;
			}
			int num = low0;
			int num2 = high0;
			int num3 = num + (num2 - num) / 2;
			K k = keys[num3];
			for (;;)
			{
				while (num < high0 && Array.compare<K>(keys[num], k, comparer) < 0)
				{
					num++;
				}
				while (num2 > low0 && Array.compare<K>(k, keys[num2], comparer) < 0)
				{
					num2--;
				}
				if (num > num2)
				{
					break;
				}
				Array.swap<K, V>(keys, items, num, num2);
				num++;
				num2--;
			}
			if (low0 < num2)
			{
				Array.qsort<K, V>(keys, items, low0, num2, comparer);
			}
			if (num < high0)
			{
				Array.qsort<K, V>(keys, items, num, high0, comparer);
			}
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x00011C00 File Offset: 0x0000FE00
		private static int compare<T>(T value1, T value2, IComparer<T> comparer)
		{
			if (comparer != null)
			{
				return comparer.Compare(value1, value2);
			}
			if (value1 == null)
			{
				return (value2 != null) ? -1 : 0;
			}
			if (value2 == null)
			{
				return 1;
			}
			if (value1 is IComparable<T>)
			{
				return ((IComparable<T>)((object)value1)).CompareTo(value2);
			}
			if (value1 is IComparable)
			{
				return ((IComparable)((object)value1)).CompareTo(value2);
			}
			string text = Locale.GetText("No IComparable or IComparable<{0}> interface found.");
			throw new InvalidOperationException(string.Format(text, typeof(T)));
		}

		// Token: 0x06000463 RID: 1123 RVA: 0x00011CB0 File Offset: 0x0000FEB0
		private static void qsort<T>(T[] array, int low0, int high0, Comparison<T> comparison)
		{
			if (low0 >= high0)
			{
				return;
			}
			int num = low0;
			int num2 = high0;
			int num3 = num + (num2 - num) / 2;
			T t = array[num3];
			for (;;)
			{
				while (num < high0 && comparison(array[num], t) < 0)
				{
					num++;
				}
				while (num2 > low0 && comparison(t, array[num2]) < 0)
				{
					num2--;
				}
				if (num > num2)
				{
					break;
				}
				Array.swap<T>(array, num, num2);
				num++;
				num2--;
			}
			if (low0 < num2)
			{
				Array.qsort<T>(array, low0, num2, comparison);
			}
			if (num < high0)
			{
				Array.qsort<T>(array, num, high0, comparison);
			}
		}

		// Token: 0x06000464 RID: 1124 RVA: 0x00011D68 File Offset: 0x0000FF68
		private static void swap<K, V>(K[] keys, V[] items, int i, int j)
		{
			K k = keys[i];
			keys[i] = keys[j];
			keys[j] = k;
			if (items != null)
			{
				V v = items[i];
				items[i] = items[j];
				items[j] = v;
			}
		}

		// Token: 0x06000465 RID: 1125 RVA: 0x00011DB8 File Offset: 0x0000FFB8
		private static void swap<T>(T[] array, int i, int j)
		{
			T t = array[i];
			array[i] = array[j];
			array[j] = t;
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x00011DE4 File Offset: 0x0000FFE4
		public void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (this.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (index + this.GetLength(0) > array.GetLowerBound(0) + array.GetLength(0))
			{
				throw new ArgumentException("Destination array was not long enough. Check destIndex and length, and the array's lower bounds.");
			}
			if (array.Rank > 1)
			{
				throw new RankException(Locale.GetText("Only single dimension arrays are supported."));
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("Value has to be >= 0."));
			}
			Array.Copy(this, this.GetLowerBound(0), array, index, this.GetLength(0));
		}

		// Token: 0x06000467 RID: 1127 RVA: 0x00011E94 File Offset: 0x00010094
		[ComVisible(false)]
		public void CopyTo(Array array, long index)
		{
			if (index < 0L || index > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("Value must be >= 0 and <= Int32.MaxValue."));
			}
			this.CopyTo(array, (int)index);
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x00011ED4 File Offset: 0x000100D4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static void Resize<T>(ref T[] array, int newSize)
		{
			Array.Resize<T>(ref array, (array != null) ? array.Length : 0, newSize);
		}

		// Token: 0x06000469 RID: 1129 RVA: 0x00011EF0 File Offset: 0x000100F0
		internal static void Resize<T>(ref T[] array, int length, int newSize)
		{
			if (newSize < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (array == null)
			{
				array = new T[newSize];
				return;
			}
			if (array.Length == newSize)
			{
				return;
			}
			T[] array2 = new T[newSize];
			Array.Copy(array, array2, Math.Min(newSize, length));
			array = array2;
		}

		// Token: 0x0600046A RID: 1130 RVA: 0x00011F40 File Offset: 0x00010140
		public static bool TrueForAll<T>(T[] array, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			foreach (T obj in array)
			{
				if (!match(obj))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600046B RID: 1131 RVA: 0x00011F9C File Offset: 0x0001019C
		public static void ForEach<T>(T[] array, Action<T> action)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (action == null)
			{
				throw new ArgumentNullException("action");
			}
			foreach (T obj in array)
			{
				action(obj);
			}
		}

		// Token: 0x0600046C RID: 1132 RVA: 0x00011FF0 File Offset: 0x000101F0
		public static TOutput[] ConvertAll<TInput, TOutput>(TInput[] array, Converter<TInput, TOutput> converter)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (converter == null)
			{
				throw new ArgumentNullException("converter");
			}
			TOutput[] array2 = new TOutput[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				array2[i] = converter(array[i]);
			}
			return array2;
		}

		// Token: 0x0600046D RID: 1133 RVA: 0x00012054 File Offset: 0x00010254
		public static int FindLastIndex<T>(T[] array, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.FindLastIndex<T>(array, 0, array.Length, match);
		}

		// Token: 0x0600046E RID: 1134 RVA: 0x00012074 File Offset: 0x00010274
		public static int FindLastIndex<T>(T[] array, int startIndex, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException();
			}
			return Array.FindLastIndex<T>(array, startIndex, array.Length - startIndex, match);
		}

		// Token: 0x0600046F RID: 1135 RVA: 0x00012090 File Offset: 0x00010290
		public static int FindLastIndex<T>(T[] array, int startIndex, int count, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			if (startIndex > array.Length || startIndex + count > array.Length)
			{
				throw new ArgumentOutOfRangeException();
			}
			for (int i = startIndex + count - 1; i >= startIndex; i--)
			{
				if (match(array[i]))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000470 RID: 1136 RVA: 0x00012104 File Offset: 0x00010304
		public static int FindIndex<T>(T[] array, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.FindIndex<T>(array, 0, array.Length, match);
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x00012124 File Offset: 0x00010324
		public static int FindIndex<T>(T[] array, int startIndex, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.FindIndex<T>(array, startIndex, array.Length - startIndex, match);
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x00012144 File Offset: 0x00010344
		public static int FindIndex<T>(T[] array, int startIndex, int count, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			if (startIndex > array.Length || startIndex + count > array.Length)
			{
				throw new ArgumentOutOfRangeException();
			}
			for (int i = startIndex; i < startIndex + count; i++)
			{
				if (match(array[i]))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x000121B8 File Offset: 0x000103B8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int BinarySearch<T>(T[] array, T value)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.BinarySearch<T>(array, 0, array.Length, value, null);
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x000121D8 File Offset: 0x000103D8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int BinarySearch<T>(T[] array, T value, IComparer<T> comparer)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.BinarySearch<T>(array, 0, array.Length, value, comparer);
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x000121F8 File Offset: 0x000103F8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int BinarySearch<T>(T[] array, int index, int length, T value)
		{
			return Array.BinarySearch<T>(array, index, length, value, null);
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x00012204 File Offset: 0x00010404
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int BinarySearch<T>(T[] array, int index, int length, T value, IComparer<T> comparer)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("index is less than the lower bound of array."));
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length", Locale.GetText("Value has to be >= 0."));
			}
			if (index > array.Length - length)
			{
				throw new ArgumentException(Locale.GetText("index and length do not specify a valid range in array."));
			}
			if (comparer == null)
			{
				comparer = Comparer<T>.Default;
			}
			int i = index;
			int num = index + length - 1;
			try
			{
				while (i <= num)
				{
					int num2 = i + (num - i) / 2;
					int num3 = comparer.Compare(value, array[num2]);
					if (num3 == 0)
					{
						return num2;
					}
					if (num3 < 0)
					{
						num = num2 - 1;
					}
					else
					{
						i = num2 + 1;
					}
				}
			}
			catch (Exception innerException)
			{
				throw new InvalidOperationException(Locale.GetText("Comparer threw an exception."), innerException);
			}
			return ~i;
		}

		// Token: 0x06000477 RID: 1143 RVA: 0x00012314 File Offset: 0x00010514
		public static int IndexOf<T>(T[] array, T value)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.IndexOf<T>(array, value, 0, array.Length);
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x00012334 File Offset: 0x00010534
		public static int IndexOf<T>(T[] array, T value, int startIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.IndexOf<T>(array, value, startIndex, array.Length - startIndex);
		}

		// Token: 0x06000479 RID: 1145 RVA: 0x00012354 File Offset: 0x00010554
		public static int IndexOf<T>(T[] array, T value, int startIndex, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (count < 0 || startIndex < array.GetLowerBound(0) || startIndex - 1 > array.GetUpperBound(0) - count)
			{
				throw new ArgumentOutOfRangeException();
			}
			int num = startIndex + count;
			EqualityComparer<T> @default = EqualityComparer<T>.Default;
			for (int i = startIndex; i < num; i++)
			{
				if (@default.Equals(array[i], value))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x000123D0 File Offset: 0x000105D0
		public static int LastIndexOf<T>(T[] array, T value)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (array.Length == 0)
			{
				return -1;
			}
			return Array.LastIndexOf<T>(array, value, array.Length - 1);
		}

		// Token: 0x0600047B RID: 1147 RVA: 0x000123FC File Offset: 0x000105FC
		public static int LastIndexOf<T>(T[] array, T value, int startIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return Array.LastIndexOf<T>(array, value, startIndex, startIndex + 1);
		}

		// Token: 0x0600047C RID: 1148 RVA: 0x0001241C File Offset: 0x0001061C
		public static int LastIndexOf<T>(T[] array, T value, int startIndex, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (count < 0 || startIndex < array.GetLowerBound(0) || startIndex > array.GetUpperBound(0) || startIndex - count + 1 < array.GetLowerBound(0))
			{
				throw new ArgumentOutOfRangeException();
			}
			EqualityComparer<T> @default = EqualityComparer<T>.Default;
			for (int i = startIndex; i >= startIndex - count + 1; i--)
			{
				if (@default.Equals(array[i], value))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x0600047D RID: 1149 RVA: 0x000124A4 File Offset: 0x000106A4
		public static T[] FindAll<T>(T[] array, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			int newSize = 0;
			T[] array2 = new T[array.Length];
			foreach (T t in array)
			{
				if (match(t))
				{
					array2[newSize++] = t;
				}
			}
			Array.Resize<T>(ref array2, newSize);
			return array2;
		}

		// Token: 0x0600047E RID: 1150 RVA: 0x00012524 File Offset: 0x00010724
		public static bool Exists<T>(T[] array, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			foreach (T obj in array)
			{
				if (match(obj))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600047F RID: 1151 RVA: 0x00012580 File Offset: 0x00010780
		public static ReadOnlyCollection<T> AsReadOnly<T>(T[] array)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			return new ReadOnlyCollection<T>(new Array.ArrayReadOnlyList<T>(array));
		}

		// Token: 0x06000480 RID: 1152 RVA: 0x000125A0 File Offset: 0x000107A0
		public static T Find<T>(T[] array, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			foreach (T t in array)
			{
				if (match(t))
				{
					return t;
				}
			}
			return default(T);
		}

		// Token: 0x06000481 RID: 1153 RVA: 0x00012604 File Offset: 0x00010804
		public static T FindLast<T>(T[] array, Predicate<T> match)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (match == null)
			{
				throw new ArgumentNullException("match");
			}
			for (int i = array.Length - 1; i >= 0; i--)
			{
				if (match(array[i]))
				{
					return array[i];
				}
			}
			return default(T);
		}

		// Token: 0x06000482 RID: 1154 RVA: 0x0001266C File Offset: 0x0001086C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static void ConstrainedCopy(Array sourceArray, int sourceIndex, Array destinationArray, int destinationIndex, int length)
		{
			Array.Copy(sourceArray, sourceIndex, destinationArray, destinationIndex, length);
		}

		// Token: 0x0200002A RID: 42
		internal struct InternalEnumerator<T> : IEnumerator, IDisposable, IEnumerator<T>
		{
			// Token: 0x06000483 RID: 1155 RVA: 0x0001267C File Offset: 0x0001087C
			internal InternalEnumerator(Array array)
			{
				this.array = array;
				this.idx = -2;
			}

			// Token: 0x06000484 RID: 1156 RVA: 0x00012690 File Offset: 0x00010890
			void IEnumerator.Reset()
			{
				this.idx = -2;
			}

			// Token: 0x17000019 RID: 25
			// (get) Token: 0x06000485 RID: 1157 RVA: 0x0001269C File Offset: 0x0001089C
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x06000486 RID: 1158 RVA: 0x000126AC File Offset: 0x000108AC
			public void Dispose()
			{
				this.idx = -2;
			}

			// Token: 0x06000487 RID: 1159 RVA: 0x000126B8 File Offset: 0x000108B8
			public bool MoveNext()
			{
				if (this.idx == -2)
				{
					this.idx = this.array.Length;
				}
				return this.idx != -1 && --this.idx != -1;
			}

			// Token: 0x1700001A RID: 26
			// (get) Token: 0x06000488 RID: 1160 RVA: 0x0001270C File Offset: 0x0001090C
			public T Current
			{
				get
				{
					if (this.idx == -2)
					{
						throw new InvalidOperationException("Enumeration has not started. Call MoveNext");
					}
					if (this.idx == -1)
					{
						throw new InvalidOperationException("Enumeration already finished");
					}
					return this.array.InternalArray__get_Item<T>(this.array.Length - 1 - this.idx);
				}
			}

			// Token: 0x04000066 RID: 102
			private const int NOT_STARTED = -2;

			// Token: 0x04000067 RID: 103
			private const int FINISHED = -1;

			// Token: 0x04000068 RID: 104
			private Array array;

			// Token: 0x04000069 RID: 105
			private int idx;
		}

		// Token: 0x0200002B RID: 43
		internal class SimpleEnumerator : IEnumerator, ICloneable
		{
			// Token: 0x06000489 RID: 1161 RVA: 0x00012768 File Offset: 0x00010968
			public SimpleEnumerator(Array arrayToEnumerate)
			{
				this.enumeratee = arrayToEnumerate;
				this.currentpos = -1;
				this.length = arrayToEnumerate.Length;
			}

			// Token: 0x1700001B RID: 27
			// (get) Token: 0x0600048A RID: 1162 RVA: 0x00012798 File Offset: 0x00010998
			public object Current
			{
				get
				{
					if (this.currentpos < 0)
					{
						throw new InvalidOperationException(Locale.GetText("Enumeration has not started."));
					}
					if (this.currentpos >= this.length)
					{
						throw new InvalidOperationException(Locale.GetText("Enumeration has already ended"));
					}
					return this.enumeratee.GetValueImpl(this.currentpos);
				}
			}

			// Token: 0x0600048B RID: 1163 RVA: 0x000127F4 File Offset: 0x000109F4
			public bool MoveNext()
			{
				if (this.currentpos < this.length)
				{
					this.currentpos++;
				}
				return this.currentpos < this.length;
			}

			// Token: 0x0600048C RID: 1164 RVA: 0x0001282C File Offset: 0x00010A2C
			public void Reset()
			{
				this.currentpos = -1;
			}

			// Token: 0x0600048D RID: 1165 RVA: 0x00012838 File Offset: 0x00010A38
			public object Clone()
			{
				return base.MemberwiseClone();
			}

			// Token: 0x0400006A RID: 106
			private Array enumeratee;

			// Token: 0x0400006B RID: 107
			private int currentpos;

			// Token: 0x0400006C RID: 108
			private int length;
		}

		// Token: 0x0200002C RID: 44
		private class ArrayReadOnlyList<T> : IEnumerable, IList<T>, ICollection<T>, IEnumerable<T>
		{
			// Token: 0x0600048E RID: 1166 RVA: 0x00012840 File Offset: 0x00010A40
			public ArrayReadOnlyList(T[] array)
			{
				this.array = array;
			}

			// Token: 0x0600048F RID: 1167 RVA: 0x00012850 File Offset: 0x00010A50
			IEnumerator IEnumerable.GetEnumerator()
			{
				return this.GetEnumerator();
			}

			// Token: 0x1700001C RID: 28
			public T this[int index]
			{
				get
				{
					if (index >= this.array.Length)
					{
						throw new ArgumentOutOfRangeException("index");
					}
					return this.array[index];
				}
				set
				{
					throw Array.ArrayReadOnlyList<T>.ReadOnlyError();
				}
			}

			// Token: 0x1700001D RID: 29
			// (get) Token: 0x06000492 RID: 1170 RVA: 0x00012888 File Offset: 0x00010A88
			public int Count
			{
				get
				{
					return this.array.Length;
				}
			}

			// Token: 0x1700001E RID: 30
			// (get) Token: 0x06000493 RID: 1171 RVA: 0x00012894 File Offset: 0x00010A94
			public bool IsReadOnly
			{
				get
				{
					return true;
				}
			}

			// Token: 0x06000494 RID: 1172 RVA: 0x00012898 File Offset: 0x00010A98
			public void Add(T item)
			{
				throw Array.ArrayReadOnlyList<T>.ReadOnlyError();
			}

			// Token: 0x06000495 RID: 1173 RVA: 0x000128A0 File Offset: 0x00010AA0
			public void Clear()
			{
				throw Array.ArrayReadOnlyList<T>.ReadOnlyError();
			}

			// Token: 0x06000496 RID: 1174 RVA: 0x000128A8 File Offset: 0x00010AA8
			public bool Contains(T item)
			{
				return Array.IndexOf<T>(this.array, item) >= 0;
			}

			// Token: 0x06000497 RID: 1175 RVA: 0x000128BC File Offset: 0x00010ABC
			public void CopyTo(T[] array, int index)
			{
				this.array.CopyTo(array, index);
			}

			// Token: 0x06000498 RID: 1176 RVA: 0x000128CC File Offset: 0x00010ACC
			public IEnumerator<T> GetEnumerator()
			{
				for (int i = 0; i < this.array.Length; i++)
				{
					yield return this.array[i];
				}
				yield break;
			}

			// Token: 0x06000499 RID: 1177 RVA: 0x000128E8 File Offset: 0x00010AE8
			public int IndexOf(T item)
			{
				return Array.IndexOf<T>(this.array, item);
			}

			// Token: 0x0600049A RID: 1178 RVA: 0x000128F8 File Offset: 0x00010AF8
			public void Insert(int index, T item)
			{
				throw Array.ArrayReadOnlyList<T>.ReadOnlyError();
			}

			// Token: 0x0600049B RID: 1179 RVA: 0x00012900 File Offset: 0x00010B00
			public bool Remove(T item)
			{
				throw Array.ArrayReadOnlyList<T>.ReadOnlyError();
			}

			// Token: 0x0600049C RID: 1180 RVA: 0x00012908 File Offset: 0x00010B08
			public void RemoveAt(int index)
			{
				throw Array.ArrayReadOnlyList<T>.ReadOnlyError();
			}

			// Token: 0x0600049D RID: 1181 RVA: 0x00012910 File Offset: 0x00010B10
			private static Exception ReadOnlyError()
			{
				return new NotSupportedException("This collection is read-only.");
			}

			// Token: 0x0400006D RID: 109
			private T[] array;
		}

		// Token: 0x02000031 RID: 49
		// (Invoke) Token: 0x060004BA RID: 1210
		private delegate void Swapper(int i, int j);
	}
}
