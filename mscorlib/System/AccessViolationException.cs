﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x020000F3 RID: 243
	[ComVisible(true)]
	[Serializable]
	public class AccessViolationException : SystemException
	{
		// Token: 0x06000C5A RID: 3162 RVA: 0x000384AC File Offset: 0x000366AC
		public AccessViolationException() : base(Locale.GetText("Attempted to read or write protected memory. This is often an indication that other memory has been corrupted."))
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06000C5B RID: 3163 RVA: 0x000384CC File Offset: 0x000366CC
		public AccessViolationException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06000C5C RID: 3164 RVA: 0x000384E0 File Offset: 0x000366E0
		public AccessViolationException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06000C5D RID: 3165 RVA: 0x000384F8 File Offset: 0x000366F8
		protected AccessViolationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0400035C RID: 860
		private const int Result = -2147467261;
	}
}
