﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x020000FC RID: 252
	[ComVisible(true)]
	[Serializable]
	public class AppDomainUnloadedException : SystemException
	{
		// Token: 0x06000D54 RID: 3412 RVA: 0x0003A824 File Offset: 0x00038A24
		public AppDomainUnloadedException() : base(Locale.GetText("Can't access an unloaded application domain."))
		{
			base.HResult = -2146234348;
		}

		// Token: 0x06000D55 RID: 3413 RVA: 0x0003A844 File Offset: 0x00038A44
		public AppDomainUnloadedException(string message) : base(message)
		{
			base.HResult = -2146234348;
		}

		// Token: 0x06000D56 RID: 3414 RVA: 0x0003A858 File Offset: 0x00038A58
		public AppDomainUnloadedException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146234348;
		}

		// Token: 0x06000D57 RID: 3415 RVA: 0x0003A870 File Offset: 0x00038A70
		protected AppDomainUnloadedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000395 RID: 917
		private const int Result = -2146234348;
	}
}
