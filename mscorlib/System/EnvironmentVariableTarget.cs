﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000134 RID: 308
	[ComVisible(true)]
	public enum EnvironmentVariableTarget
	{
		// Token: 0x040004FA RID: 1274
		Process,
		// Token: 0x040004FB RID: 1275
		User,
		// Token: 0x040004FC RID: 1276
		Machine
	}
}
