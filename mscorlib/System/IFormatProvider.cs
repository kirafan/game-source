﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000142 RID: 322
	[ComVisible(true)]
	public interface IFormatProvider
	{
		// Token: 0x060011B1 RID: 4529
		object GetFormat(Type formatType);
	}
}
