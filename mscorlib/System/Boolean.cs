﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000022 RID: 34
	[ComVisible(true)]
	[Serializable]
	public struct Boolean : IConvertible, IComparable, IComparable<bool>, IEquatable<bool>
	{
		// Token: 0x06000344 RID: 836 RVA: 0x0000D63C File Offset: 0x0000B83C
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			return Convert.ToType(this, targetType, provider, false);
		}

		// Token: 0x06000345 RID: 837 RVA: 0x0000D66C File Offset: 0x0000B86C
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return this;
		}

		// Token: 0x06000346 RID: 838 RVA: 0x0000D670 File Offset: 0x0000B870
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this);
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000D67C File Offset: 0x0000B87C
		char IConvertible.ToChar(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000348 RID: 840 RVA: 0x0000D684 File Offset: 0x0000B884
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000349 RID: 841 RVA: 0x0000D68C File Offset: 0x0000B88C
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this);
		}

		// Token: 0x0600034A RID: 842 RVA: 0x0000D698 File Offset: 0x0000B898
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this);
		}

		// Token: 0x0600034B RID: 843 RVA: 0x0000D6A4 File Offset: 0x0000B8A4
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		// Token: 0x0600034C RID: 844 RVA: 0x0000D6B0 File Offset: 0x0000B8B0
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this);
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000D6BC File Offset: 0x0000B8BC
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000D6C8 File Offset: 0x0000B8C8
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this);
		}

		// Token: 0x0600034F RID: 847 RVA: 0x0000D6D4 File Offset: 0x0000B8D4
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this);
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000D6E0 File Offset: 0x0000B8E0
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this);
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000D6EC File Offset: 0x0000B8EC
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		// Token: 0x06000352 RID: 850 RVA: 0x0000D6F8 File Offset: 0x0000B8F8
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		// Token: 0x06000353 RID: 851 RVA: 0x0000D704 File Offset: 0x0000B904
		public int CompareTo(object obj)
		{
			if (obj == null)
			{
				return 1;
			}
			if (!(obj is bool))
			{
				throw new ArgumentException(Locale.GetText("Object is not a Boolean."));
			}
			bool flag = (bool)obj;
			if (this && !flag)
			{
				return 1;
			}
			return (this != flag) ? -1 : 0;
		}

		// Token: 0x06000354 RID: 852 RVA: 0x0000D75C File Offset: 0x0000B95C
		public override bool Equals(object obj)
		{
			if (obj == null || !(obj is bool))
			{
				return false;
			}
			bool flag = (bool)obj;
			return (!this) ? (!flag) : flag;
		}

		// Token: 0x06000355 RID: 853 RVA: 0x0000D794 File Offset: 0x0000B994
		public int CompareTo(bool value)
		{
			if (this == value)
			{
				return 0;
			}
			return this ? 1 : -1;
		}

		// Token: 0x06000356 RID: 854 RVA: 0x0000D7B0 File Offset: 0x0000B9B0
		public bool Equals(bool obj)
		{
			return this == obj;
		}

		// Token: 0x06000357 RID: 855 RVA: 0x0000D7B8 File Offset: 0x0000B9B8
		public override int GetHashCode()
		{
			return (!this) ? 0 : 1;
		}

		// Token: 0x06000358 RID: 856 RVA: 0x0000D7C8 File Offset: 0x0000B9C8
		public static bool Parse(string value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			value = value.Trim();
			if (string.Compare(value, bool.TrueString, true, CultureInfo.InvariantCulture) == 0)
			{
				return true;
			}
			if (string.Compare(value, bool.FalseString, true, CultureInfo.InvariantCulture) == 0)
			{
				return false;
			}
			throw new FormatException(Locale.GetText("Value is not equivalent to either TrueString or FalseString."));
		}

		// Token: 0x06000359 RID: 857 RVA: 0x0000D830 File Offset: 0x0000BA30
		public static bool TryParse(string value, out bool result)
		{
			result = false;
			if (value == null)
			{
				return false;
			}
			value = value.Trim();
			if (string.Compare(value, bool.TrueString, true, CultureInfo.InvariantCulture) == 0)
			{
				result = true;
				return true;
			}
			return string.Compare(value, bool.FalseString, true, CultureInfo.InvariantCulture) == 0;
		}

		// Token: 0x0600035A RID: 858 RVA: 0x0000D884 File Offset: 0x0000BA84
		public override string ToString()
		{
			return (!this) ? bool.FalseString : bool.TrueString;
		}

		// Token: 0x0600035B RID: 859 RVA: 0x0000D89C File Offset: 0x0000BA9C
		public TypeCode GetTypeCode()
		{
			return TypeCode.Boolean;
		}

		// Token: 0x0600035C RID: 860 RVA: 0x0000D8A0 File Offset: 0x0000BAA0
		public string ToString(IFormatProvider provider)
		{
			return this.ToString();
		}

		// Token: 0x04000053 RID: 83
		public static readonly string FalseString = "False";

		// Token: 0x04000054 RID: 84
		public static readonly string TrueString = "True";

		// Token: 0x04000055 RID: 85
		internal bool m_value;
	}
}
