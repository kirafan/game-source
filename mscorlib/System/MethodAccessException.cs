﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200014F RID: 335
	[ComVisible(true)]
	[Serializable]
	public class MethodAccessException : MemberAccessException
	{
		// Token: 0x0600121C RID: 4636 RVA: 0x00047C64 File Offset: 0x00045E64
		public MethodAccessException() : base(Locale.GetText("Attempt to access a private/protected method failed."))
		{
			base.HResult = -2146233072;
		}

		// Token: 0x0600121D RID: 4637 RVA: 0x00047C84 File Offset: 0x00045E84
		public MethodAccessException(string message) : base(message)
		{
			base.HResult = -2146233072;
		}

		// Token: 0x0600121E RID: 4638 RVA: 0x00047C98 File Offset: 0x00045E98
		protected MethodAccessException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0600121F RID: 4639 RVA: 0x00047CA4 File Offset: 0x00045EA4
		public MethodAccessException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233072;
		}

		// Token: 0x0400052E RID: 1326
		private const int Result = -2146233072;
	}
}
