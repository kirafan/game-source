﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000014 RID: 20
	[CLSCompliant(false)]
	[ComVisible(true)]
	[Serializable]
	public struct SByte : IFormattable, IConvertible, IComparable, IComparable<sbyte>, IEquatable<sbyte>
	{
		// Token: 0x06000110 RID: 272 RVA: 0x00005480 File Offset: 0x00003680
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this);
		}

		// Token: 0x06000111 RID: 273 RVA: 0x0000548C File Offset: 0x0000368C
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this);
		}

		// Token: 0x06000112 RID: 274 RVA: 0x00005498 File Offset: 0x00003698
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this);
		}

		// Token: 0x06000113 RID: 275 RVA: 0x000054A4 File Offset: 0x000036A4
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			return Convert.ToDateTime(this);
		}

		// Token: 0x06000114 RID: 276 RVA: 0x000054B0 File Offset: 0x000036B0
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this);
		}

		// Token: 0x06000115 RID: 277 RVA: 0x000054BC File Offset: 0x000036BC
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this);
		}

		// Token: 0x06000116 RID: 278 RVA: 0x000054C8 File Offset: 0x000036C8
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		// Token: 0x06000117 RID: 279 RVA: 0x000054D4 File Offset: 0x000036D4
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this);
		}

		// Token: 0x06000118 RID: 280 RVA: 0x000054E0 File Offset: 0x000036E0
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		// Token: 0x06000119 RID: 281 RVA: 0x000054EC File Offset: 0x000036EC
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return this;
		}

		// Token: 0x0600011A RID: 282 RVA: 0x000054F0 File Offset: 0x000036F0
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this);
		}

		// Token: 0x0600011B RID: 283 RVA: 0x000054FC File Offset: 0x000036FC
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			return Convert.ToType(this, targetType, provider, false);
		}

		// Token: 0x0600011C RID: 284 RVA: 0x0000552C File Offset: 0x0000372C
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this);
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00005538 File Offset: 0x00003738
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		// Token: 0x0600011E RID: 286 RVA: 0x00005544 File Offset: 0x00003744
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		// Token: 0x0600011F RID: 287 RVA: 0x00005550 File Offset: 0x00003750
		public int CompareTo(object obj)
		{
			if (obj == null)
			{
				return 1;
			}
			if (!(obj is sbyte))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.SByte."));
			}
			sbyte b = (sbyte)obj;
			if ((int)this == (int)b)
			{
				return 0;
			}
			if ((int)this > (int)b)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06000120 RID: 288 RVA: 0x000055A0 File Offset: 0x000037A0
		public override bool Equals(object obj)
		{
			return obj is sbyte && (int)((sbyte)obj) == (int)this;
		}

		// Token: 0x06000121 RID: 289 RVA: 0x000055BC File Offset: 0x000037BC
		public override int GetHashCode()
		{
			return (int)this;
		}

		// Token: 0x06000122 RID: 290 RVA: 0x000055C4 File Offset: 0x000037C4
		public int CompareTo(sbyte value)
		{
			if ((int)this == (int)value)
			{
				return 0;
			}
			if ((int)this > (int)value)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06000123 RID: 291 RVA: 0x000055E0 File Offset: 0x000037E0
		public bool Equals(sbyte obj)
		{
			return (int)obj == (int)this;
		}

		// Token: 0x06000124 RID: 292 RVA: 0x000055EC File Offset: 0x000037EC
		internal static bool Parse(string s, bool tryParse, out sbyte result, out Exception exc)
		{
			int num = 0;
			bool flag = false;
			bool flag2 = false;
			result = 0;
			exc = null;
			if (s == null)
			{
				if (!tryParse)
				{
					exc = new ArgumentNullException("s");
				}
				return false;
			}
			int length = s.Length;
			int i;
			char c;
			for (i = 0; i < length; i++)
			{
				c = s[i];
				if (!char.IsWhiteSpace(c))
				{
					break;
				}
			}
			if (i == length)
			{
				if (!tryParse)
				{
					exc = int.GetFormatException();
				}
				return false;
			}
			c = s[i];
			if (c == '+')
			{
				i++;
			}
			else if (c == '-')
			{
				flag = true;
				i++;
			}
			while (i < length)
			{
				c = s[i];
				if (c >= '0' && c <= '9')
				{
					if (tryParse)
					{
						int num2 = num * 10 - (int)(c - '0');
						if (num2 < -128)
						{
							return false;
						}
						num = (int)((sbyte)num2);
					}
					else
					{
						num = checked(num * 10 - (int)(c - '0'));
					}
					flag2 = true;
					i++;
				}
				else
				{
					if (char.IsWhiteSpace(c))
					{
						for (i++; i < length; i++)
						{
							if (!char.IsWhiteSpace(s[i]))
							{
								if (!tryParse)
								{
									exc = int.GetFormatException();
								}
								return false;
							}
						}
						break;
					}
					if (!tryParse)
					{
						exc = int.GetFormatException();
					}
					return false;
				}
			}
			if (!flag2)
			{
				if (!tryParse)
				{
					exc = int.GetFormatException();
				}
				return false;
			}
			num = ((!flag) ? (-num) : num);
			if (num < -128 || num > 127)
			{
				if (!tryParse)
				{
					exc = new OverflowException();
				}
				return false;
			}
			result = (sbyte)num;
			return true;
		}

		// Token: 0x06000125 RID: 293 RVA: 0x00005798 File Offset: 0x00003998
		[CLSCompliant(false)]
		public static sbyte Parse(string s, IFormatProvider provider)
		{
			return sbyte.Parse(s, NumberStyles.Integer, provider);
		}

		// Token: 0x06000126 RID: 294 RVA: 0x000057A4 File Offset: 0x000039A4
		[CLSCompliant(false)]
		public static sbyte Parse(string s, NumberStyles style)
		{
			return sbyte.Parse(s, style, null);
		}

		// Token: 0x06000127 RID: 295 RVA: 0x000057B0 File Offset: 0x000039B0
		[CLSCompliant(false)]
		public static sbyte Parse(string s, NumberStyles style, IFormatProvider provider)
		{
			int num = int.Parse(s, style, provider);
			if (num > 127 || num < -128)
			{
				throw new OverflowException(Locale.GetText("Value too large or too small."));
			}
			return (sbyte)num;
		}

		// Token: 0x06000128 RID: 296 RVA: 0x000057E8 File Offset: 0x000039E8
		[CLSCompliant(false)]
		public static sbyte Parse(string s)
		{
			sbyte result;
			Exception ex;
			if (!sbyte.Parse(s, false, out result, out ex))
			{
				throw ex;
			}
			return result;
		}

		// Token: 0x06000129 RID: 297 RVA: 0x00005808 File Offset: 0x00003A08
		[CLSCompliant(false)]
		public static bool TryParse(string s, out sbyte result)
		{
			Exception ex;
			if (!sbyte.Parse(s, true, out result, out ex))
			{
				result = 0;
				return false;
			}
			return true;
		}

		// Token: 0x0600012A RID: 298 RVA: 0x0000582C File Offset: 0x00003A2C
		[CLSCompliant(false)]
		public static bool TryParse(string s, NumberStyles style, IFormatProvider provider, out sbyte result)
		{
			result = 0;
			int num;
			if (!int.TryParse(s, style, provider, out num))
			{
				return false;
			}
			if (num > 127 || num < -128)
			{
				return false;
			}
			result = (sbyte)num;
			return true;
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00005864 File Offset: 0x00003A64
		public override string ToString()
		{
			return NumberFormatter.NumberToString((int)this, null);
		}

		// Token: 0x0600012C RID: 300 RVA: 0x00005870 File Offset: 0x00003A70
		public string ToString(IFormatProvider provider)
		{
			return NumberFormatter.NumberToString((int)this, provider);
		}

		// Token: 0x0600012D RID: 301 RVA: 0x0000587C File Offset: 0x00003A7C
		public string ToString(string format)
		{
			return this.ToString(format, null);
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00005888 File Offset: 0x00003A88
		public string ToString(string format, IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(format, this, provider);
		}

		// Token: 0x0600012F RID: 303 RVA: 0x00005894 File Offset: 0x00003A94
		public TypeCode GetTypeCode()
		{
			return TypeCode.SByte;
		}

		// Token: 0x04000015 RID: 21
		public const sbyte MinValue = -128;

		// Token: 0x04000016 RID: 22
		public const sbyte MaxValue = 127;

		// Token: 0x04000017 RID: 23
		internal sbyte m_value;
	}
}
