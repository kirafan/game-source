﻿using System;

namespace System
{
	// Token: 0x02000700 RID: 1792
	// (Invoke) Token: 0x060043F4 RID: 17396
	public delegate void Action<T>(T obj);
}
