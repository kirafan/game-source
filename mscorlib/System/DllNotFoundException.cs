﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000129 RID: 297
	[ComVisible(true)]
	[Serializable]
	public class DllNotFoundException : TypeLoadException
	{
		// Token: 0x060010DA RID: 4314 RVA: 0x00045118 File Offset: 0x00043318
		public DllNotFoundException() : base(Locale.GetText("DLL not found."))
		{
			base.HResult = -2146233052;
		}

		// Token: 0x060010DB RID: 4315 RVA: 0x00045138 File Offset: 0x00043338
		public DllNotFoundException(string message) : base(message)
		{
			base.HResult = -2146233052;
		}

		// Token: 0x060010DC RID: 4316 RVA: 0x0004514C File Offset: 0x0004334C
		protected DllNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060010DD RID: 4317 RVA: 0x00045158 File Offset: 0x00043358
		public DllNotFoundException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233052;
		}

		// Token: 0x040004CE RID: 1230
		private const int Result = -2146233052;
	}
}
