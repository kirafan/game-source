﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200016A RID: 362
	[ComVisible(true)]
	[Serializable]
	public enum PlatformID
	{
		// Token: 0x04000596 RID: 1430
		Win32S,
		// Token: 0x04000597 RID: 1431
		Win32Windows,
		// Token: 0x04000598 RID: 1432
		Win32NT,
		// Token: 0x04000599 RID: 1433
		WinCE,
		// Token: 0x0400059A RID: 1434
		Unix,
		// Token: 0x0400059B RID: 1435
		Xbox,
		// Token: 0x0400059C RID: 1436
		MacOSX
	}
}
