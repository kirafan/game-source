﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200018E RID: 398
	[ComVisible(true)]
	[Serializable]
	public sealed class TypeInitializationException : SystemException
	{
		// Token: 0x0600145C RID: 5212 RVA: 0x00051FF4 File Offset: 0x000501F4
		public TypeInitializationException(string fullTypeName, Exception innerException) : base(Locale.GetText("An exception was thrown by the type initializer for ") + fullTypeName, innerException)
		{
			this.type_name = fullTypeName;
		}

		// Token: 0x0600145D RID: 5213 RVA: 0x00052014 File Offset: 0x00050214
		internal TypeInitializationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.type_name = info.GetString("TypeName");
		}

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x0600145E RID: 5214 RVA: 0x00052030 File Offset: 0x00050230
		public string TypeName
		{
			get
			{
				return this.type_name;
			}
		}

		// Token: 0x0600145F RID: 5215 RVA: 0x00052038 File Offset: 0x00050238
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("TypeName", this.type_name);
		}

		// Token: 0x040007FE RID: 2046
		private string type_name;
	}
}
