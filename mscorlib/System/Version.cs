﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000197 RID: 407
	[ComVisible(true)]
	[Serializable]
	public sealed class Version : IComparable, ICloneable, IComparable<Version>, IEquatable<Version>
	{
		// Token: 0x0600147D RID: 5245 RVA: 0x00052A00 File Offset: 0x00050C00
		public Version()
		{
			this.CheckedSet(2, 0, 0, -1, -1);
		}

		// Token: 0x0600147E RID: 5246 RVA: 0x00052A20 File Offset: 0x00050C20
		public Version(string version)
		{
			int major = -1;
			int minor = -1;
			int build = -1;
			int revision = -1;
			if (version == null)
			{
				throw new ArgumentNullException("version");
			}
			string[] array = version.Split(new char[]
			{
				'.'
			});
			int num = array.Length;
			if (num < 2 || num > 4)
			{
				throw new ArgumentException(Locale.GetText("There must be 2, 3 or 4 components in the version string."));
			}
			if (num > 0)
			{
				major = int.Parse(array[0]);
			}
			if (num > 1)
			{
				minor = int.Parse(array[1]);
			}
			if (num > 2)
			{
				build = int.Parse(array[2]);
			}
			if (num > 3)
			{
				revision = int.Parse(array[3]);
			}
			this.CheckedSet(num, major, minor, build, revision);
		}

		// Token: 0x0600147F RID: 5247 RVA: 0x00052AD4 File Offset: 0x00050CD4
		public Version(int major, int minor)
		{
			this.CheckedSet(2, major, minor, 0, 0);
		}

		// Token: 0x06001480 RID: 5248 RVA: 0x00052AF4 File Offset: 0x00050CF4
		public Version(int major, int minor, int build)
		{
			this.CheckedSet(3, major, minor, build, 0);
		}

		// Token: 0x06001481 RID: 5249 RVA: 0x00052B14 File Offset: 0x00050D14
		public Version(int major, int minor, int build, int revision)
		{
			this.CheckedSet(4, major, minor, build, revision);
		}

		// Token: 0x06001482 RID: 5250 RVA: 0x00052B34 File Offset: 0x00050D34
		private void CheckedSet(int defined, int major, int minor, int build, int revision)
		{
			if (major < 0)
			{
				throw new ArgumentOutOfRangeException("major");
			}
			this._Major = major;
			if (minor < 0)
			{
				throw new ArgumentOutOfRangeException("minor");
			}
			this._Minor = minor;
			if (defined == 2)
			{
				this._Build = -1;
				this._Revision = -1;
				return;
			}
			if (build < 0)
			{
				throw new ArgumentOutOfRangeException("build");
			}
			this._Build = build;
			if (defined == 3)
			{
				this._Revision = -1;
				return;
			}
			if (revision < 0)
			{
				throw new ArgumentOutOfRangeException("revision");
			}
			this._Revision = revision;
		}

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x06001483 RID: 5251 RVA: 0x00052BD0 File Offset: 0x00050DD0
		public int Build
		{
			get
			{
				return this._Build;
			}
		}

		// Token: 0x170002FB RID: 763
		// (get) Token: 0x06001484 RID: 5252 RVA: 0x00052BD8 File Offset: 0x00050DD8
		public int Major
		{
			get
			{
				return this._Major;
			}
		}

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06001485 RID: 5253 RVA: 0x00052BE0 File Offset: 0x00050DE0
		public int Minor
		{
			get
			{
				return this._Minor;
			}
		}

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06001486 RID: 5254 RVA: 0x00052BE8 File Offset: 0x00050DE8
		public int Revision
		{
			get
			{
				return this._Revision;
			}
		}

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06001487 RID: 5255 RVA: 0x00052BF0 File Offset: 0x00050DF0
		public short MajorRevision
		{
			get
			{
				return (short)(this._Revision >> 16);
			}
		}

		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06001488 RID: 5256 RVA: 0x00052BFC File Offset: 0x00050DFC
		public short MinorRevision
		{
			get
			{
				return (short)this._Revision;
			}
		}

		// Token: 0x06001489 RID: 5257 RVA: 0x00052C08 File Offset: 0x00050E08
		public object Clone()
		{
			if (this._Build == -1)
			{
				return new Version(this._Major, this._Minor);
			}
			if (this._Revision == -1)
			{
				return new Version(this._Major, this._Minor, this._Build);
			}
			return new Version(this._Major, this._Minor, this._Build, this._Revision);
		}

		// Token: 0x0600148A RID: 5258 RVA: 0x00052C74 File Offset: 0x00050E74
		public int CompareTo(object version)
		{
			if (version == null)
			{
				return 1;
			}
			if (!(version is Version))
			{
				throw new ArgumentException(Locale.GetText("Argument to Version.CompareTo must be a Version."));
			}
			return this.CompareTo((Version)version);
		}

		// Token: 0x0600148B RID: 5259 RVA: 0x00052CA8 File Offset: 0x00050EA8
		public override bool Equals(object obj)
		{
			return this.Equals(obj as Version);
		}

		// Token: 0x0600148C RID: 5260 RVA: 0x00052CB8 File Offset: 0x00050EB8
		public int CompareTo(Version value)
		{
			if (value == null)
			{
				return 1;
			}
			if (this._Major > value._Major)
			{
				return 1;
			}
			if (this._Major < value._Major)
			{
				return -1;
			}
			if (this._Minor > value._Minor)
			{
				return 1;
			}
			if (this._Minor < value._Minor)
			{
				return -1;
			}
			if (this._Build > value._Build)
			{
				return 1;
			}
			if (this._Build < value._Build)
			{
				return -1;
			}
			if (this._Revision > value._Revision)
			{
				return 1;
			}
			if (this._Revision < value._Revision)
			{
				return -1;
			}
			return 0;
		}

		// Token: 0x0600148D RID: 5261 RVA: 0x00052D6C File Offset: 0x00050F6C
		public bool Equals(Version obj)
		{
			return obj != null && obj._Major == this._Major && obj._Minor == this._Minor && obj._Build == this._Build && obj._Revision == this._Revision;
		}

		// Token: 0x0600148E RID: 5262 RVA: 0x00052DCC File Offset: 0x00050FCC
		public override int GetHashCode()
		{
			return this._Revision << 24 | this._Build << 16 | this._Minor << 8 | this._Major;
		}

		// Token: 0x0600148F RID: 5263 RVA: 0x00052DF4 File Offset: 0x00050FF4
		public override string ToString()
		{
			string text = this._Major.ToString() + "." + this._Minor.ToString();
			if (this._Build != -1)
			{
				text = text + "." + this._Build.ToString();
			}
			if (this._Revision != -1)
			{
				text = text + "." + this._Revision.ToString();
			}
			return text;
		}

		// Token: 0x06001490 RID: 5264 RVA: 0x00052E6C File Offset: 0x0005106C
		public string ToString(int fieldCount)
		{
			if (fieldCount == 0)
			{
				return string.Empty;
			}
			if (fieldCount == 1)
			{
				return this._Major.ToString();
			}
			if (fieldCount == 2)
			{
				return this._Major.ToString() + "." + this._Minor.ToString();
			}
			if (fieldCount == 3)
			{
				if (this._Build == -1)
				{
					throw new ArgumentException(Locale.GetText("fieldCount is larger than the number of components defined in this instance."));
				}
				return string.Concat(new string[]
				{
					this._Major.ToString(),
					".",
					this._Minor.ToString(),
					".",
					this._Build.ToString()
				});
			}
			else
			{
				if (fieldCount != 4)
				{
					throw new ArgumentException(Locale.GetText("Invalid fieldCount parameter: ") + fieldCount.ToString());
				}
				if (this._Build == -1 || this._Revision == -1)
				{
					throw new ArgumentException(Locale.GetText("fieldCount is larger than the number of components defined in this instance."));
				}
				return string.Concat(new string[]
				{
					this._Major.ToString(),
					".",
					this._Minor.ToString(),
					".",
					this._Build.ToString(),
					".",
					this._Revision.ToString()
				});
			}
		}

		// Token: 0x06001491 RID: 5265 RVA: 0x00052FD0 File Offset: 0x000511D0
		internal static Version CreateFromString(string info)
		{
			int major = 0;
			int minor = 0;
			int build = 0;
			int revision = 0;
			int num = 1;
			int num2 = -1;
			if (info == null)
			{
				return new Version(0, 0, 0, 0);
			}
			foreach (char c in info)
			{
				if (char.IsDigit(c))
				{
					if (num2 < 0)
					{
						num2 = (int)(c - '0');
					}
					else
					{
						num2 = num2 * 10 + (int)(c - '0');
					}
				}
				else if (num2 >= 0)
				{
					switch (num)
					{
					case 1:
						major = num2;
						break;
					case 2:
						minor = num2;
						break;
					case 3:
						build = num2;
						break;
					case 4:
						revision = num2;
						break;
					}
					num2 = -1;
					num++;
				}
				if (num == 5)
				{
					break;
				}
			}
			if (num2 >= 0)
			{
				switch (num)
				{
				case 1:
					major = num2;
					break;
				case 2:
					minor = num2;
					break;
				case 3:
					build = num2;
					break;
				case 4:
					revision = num2;
					break;
				}
			}
			return new Version(major, minor, build, revision);
		}

		// Token: 0x06001492 RID: 5266 RVA: 0x00053108 File Offset: 0x00051308
		public static bool operator ==(Version v1, Version v2)
		{
			return object.Equals(v1, v2);
		}

		// Token: 0x06001493 RID: 5267 RVA: 0x00053114 File Offset: 0x00051314
		public static bool operator !=(Version v1, Version v2)
		{
			return !object.Equals(v1, v2);
		}

		// Token: 0x06001494 RID: 5268 RVA: 0x00053120 File Offset: 0x00051320
		public static bool operator >(Version v1, Version v2)
		{
			return v1.CompareTo(v2) > 0;
		}

		// Token: 0x06001495 RID: 5269 RVA: 0x0005312C File Offset: 0x0005132C
		public static bool operator >=(Version v1, Version v2)
		{
			return v1.CompareTo(v2) >= 0;
		}

		// Token: 0x06001496 RID: 5270 RVA: 0x0005313C File Offset: 0x0005133C
		public static bool operator <(Version v1, Version v2)
		{
			return v1.CompareTo(v2) < 0;
		}

		// Token: 0x06001497 RID: 5271 RVA: 0x00053148 File Offset: 0x00051348
		public static bool operator <=(Version v1, Version v2)
		{
			return v1.CompareTo(v2) <= 0;
		}

		// Token: 0x04000823 RID: 2083
		private const int UNDEFINED = -1;

		// Token: 0x04000824 RID: 2084
		private int _Major;

		// Token: 0x04000825 RID: 2085
		private int _Minor;

		// Token: 0x04000826 RID: 2086
		private int _Build;

		// Token: 0x04000827 RID: 2087
		private int _Revision;
	}
}
