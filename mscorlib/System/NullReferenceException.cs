﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000162 RID: 354
	[ComVisible(true)]
	[Serializable]
	public class NullReferenceException : SystemException
	{
		// Token: 0x060012D4 RID: 4820 RVA: 0x00049824 File Offset: 0x00047A24
		public NullReferenceException() : base(Locale.GetText("A null value was found where an object instance was required."))
		{
			base.HResult = -2147467261;
		}

		// Token: 0x060012D5 RID: 4821 RVA: 0x00049844 File Offset: 0x00047A44
		public NullReferenceException(string message) : base(message)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x060012D6 RID: 4822 RVA: 0x00049858 File Offset: 0x00047A58
		public NullReferenceException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x060012D7 RID: 4823 RVA: 0x00049870 File Offset: 0x00047A70
		protected NullReferenceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0400054D RID: 1357
		private const int Result = -2147467261;
	}
}
