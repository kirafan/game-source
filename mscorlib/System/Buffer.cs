﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200010A RID: 266
	[ComVisible(true)]
	public static class Buffer
	{
		// Token: 0x06000DB7 RID: 3511 RVA: 0x0003B7DC File Offset: 0x000399DC
		public static int ByteLength(Array array)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			int num = Buffer.ByteLengthInternal(array);
			if (num < 0)
			{
				throw new ArgumentException(Locale.GetText("Object must be an array of primitives."));
			}
			return num;
		}

		// Token: 0x06000DB8 RID: 3512 RVA: 0x0003B81C File Offset: 0x00039A1C
		public static byte GetByte(Array array, int index)
		{
			if (index < 0 || index >= Buffer.ByteLength(array))
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("Value must be non-negative and less than the size of the collection."));
			}
			return Buffer.GetByteInternal(array, index);
		}

		// Token: 0x06000DB9 RID: 3513 RVA: 0x0003B850 File Offset: 0x00039A50
		public static void SetByte(Array array, int index, byte value)
		{
			if (index < 0 || index >= Buffer.ByteLength(array))
			{
				throw new ArgumentOutOfRangeException("index", Locale.GetText("Value must be non-negative and less than the size of the collection."));
			}
			Buffer.SetByteInternal(array, index, (int)value);
		}

		// Token: 0x06000DBA RID: 3514 RVA: 0x0003B890 File Offset: 0x00039A90
		public static void BlockCopy(Array src, int srcOffset, Array dst, int dstOffset, int count)
		{
			if (src == null)
			{
				throw new ArgumentNullException("src");
			}
			if (dst == null)
			{
				throw new ArgumentNullException("dst");
			}
			if (srcOffset < 0)
			{
				throw new ArgumentOutOfRangeException("srcOffset", Locale.GetText("Non-negative number required."));
			}
			if (dstOffset < 0)
			{
				throw new ArgumentOutOfRangeException("dstOffset", Locale.GetText("Non-negative number required."));
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count", Locale.GetText("Non-negative number required."));
			}
			if (!Buffer.BlockCopyInternal(src, srcOffset, dst, dstOffset, count) && (srcOffset > Buffer.ByteLength(src) - count || dstOffset > Buffer.ByteLength(dst) - count))
			{
				throw new ArgumentException(Locale.GetText("Offset and length were out of bounds for the array or count is greater than the number of elements from index to the end of the source collection."));
			}
		}

		// Token: 0x06000DBB RID: 3515
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int ByteLengthInternal(Array array);

		// Token: 0x06000DBC RID: 3516
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern byte GetByteInternal(Array array, int index);

		// Token: 0x06000DBD RID: 3517
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SetByteInternal(Array array, int index, int value);

		// Token: 0x06000DBE RID: 3518
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool BlockCopyInternal(Array src, int src_offset, Array dest, int dest_offset, int count);
	}
}
