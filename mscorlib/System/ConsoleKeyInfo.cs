﻿using System;

namespace System
{
	// Token: 0x02000115 RID: 277
	[Serializable]
	public struct ConsoleKeyInfo
	{
		// Token: 0x06000E86 RID: 3718 RVA: 0x0003CD98 File Offset: 0x0003AF98
		public ConsoleKeyInfo(char keyChar, ConsoleKey key, bool shift, bool alt, bool control)
		{
			this.key = key;
			this.keychar = keyChar;
			this.modifiers = (ConsoleModifiers)0;
			this.SetModifiers(shift, alt, control);
		}

		// Token: 0x06000E87 RID: 3719 RVA: 0x0003CDC8 File Offset: 0x0003AFC8
		internal ConsoleKeyInfo(ConsoleKeyInfo other)
		{
			this.key = other.key;
			this.keychar = other.keychar;
			this.modifiers = other.modifiers;
		}

		// Token: 0x06000E89 RID: 3721 RVA: 0x0003CE08 File Offset: 0x0003B008
		internal void SetKey(ConsoleKey key)
		{
			this.key = key;
		}

		// Token: 0x06000E8A RID: 3722 RVA: 0x0003CE14 File Offset: 0x0003B014
		internal void SetKeyChar(char keyChar)
		{
			this.keychar = keyChar;
		}

		// Token: 0x06000E8B RID: 3723 RVA: 0x0003CE20 File Offset: 0x0003B020
		internal void SetModifiers(bool shift, bool alt, bool control)
		{
			this.modifiers = ((!shift) ? ((ConsoleModifiers)0) : ConsoleModifiers.Shift);
			this.modifiers |= ((!alt) ? ((ConsoleModifiers)0) : ConsoleModifiers.Alt);
			this.modifiers |= ((!control) ? ((ConsoleModifiers)0) : ConsoleModifiers.Control);
		}

		// Token: 0x1700022F RID: 559
		// (get) Token: 0x06000E8C RID: 3724 RVA: 0x0003CE74 File Offset: 0x0003B074
		public ConsoleKey Key
		{
			get
			{
				return this.key;
			}
		}

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x06000E8D RID: 3725 RVA: 0x0003CE7C File Offset: 0x0003B07C
		public char KeyChar
		{
			get
			{
				return this.keychar;
			}
		}

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x06000E8E RID: 3726 RVA: 0x0003CE84 File Offset: 0x0003B084
		public ConsoleModifiers Modifiers
		{
			get
			{
				return this.modifiers;
			}
		}

		// Token: 0x06000E8F RID: 3727 RVA: 0x0003CE8C File Offset: 0x0003B08C
		public override bool Equals(object value)
		{
			return value is ConsoleKeyInfo && this.Equals((ConsoleKeyInfo)value);
		}

		// Token: 0x06000E90 RID: 3728 RVA: 0x0003CEA8 File Offset: 0x0003B0A8
		public bool Equals(ConsoleKeyInfo obj)
		{
			return this.key == obj.key && obj.keychar == this.keychar && obj.modifiers == this.modifiers;
		}

		// Token: 0x06000E91 RID: 3729 RVA: 0x0003CEEC File Offset: 0x0003B0EC
		public override int GetHashCode()
		{
			return this.key.GetHashCode() ^ this.keychar.GetHashCode() ^ this.modifiers.GetHashCode();
		}

		// Token: 0x06000E92 RID: 3730 RVA: 0x0003CF28 File Offset: 0x0003B128
		public static bool operator ==(ConsoleKeyInfo a, ConsoleKeyInfo b)
		{
			return a.Equals(b);
		}

		// Token: 0x06000E93 RID: 3731 RVA: 0x0003CF34 File Offset: 0x0003B134
		public static bool operator !=(ConsoleKeyInfo a, ConsoleKeyInfo b)
		{
			return !a.Equals(b);
		}

		// Token: 0x04000473 RID: 1139
		internal static ConsoleKeyInfo Empty = new ConsoleKeyInfo('\0', (ConsoleKey)0, false, false, false);

		// Token: 0x04000474 RID: 1140
		private ConsoleKey key;

		// Token: 0x04000475 RID: 1141
		private char keychar;

		// Token: 0x04000476 RID: 1142
		private ConsoleModifiers modifiers;
	}
}
