﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;

namespace System.Threading
{
	// Token: 0x020006BA RID: 1722
	[ComVisible(true)]
	public abstract class WaitHandle : MarshalByRefObject, IDisposable
	{
		// Token: 0x060041CA RID: 16842 RVA: 0x000E1E7C File Offset: 0x000E007C
		void IDisposable.Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060041CB RID: 16843
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool WaitAll_internal(WaitHandle[] handles, int ms, bool exitContext);

		// Token: 0x060041CC RID: 16844 RVA: 0x000E1E8C File Offset: 0x000E008C
		private static void CheckArray(WaitHandle[] handles, bool waitAll)
		{
			if (handles == null)
			{
				throw new ArgumentNullException("waitHandles");
			}
			int num = handles.Length;
			if (num > 64)
			{
				throw new NotSupportedException("Too many handles");
			}
			foreach (WaitHandle waitHandle in handles)
			{
				if (waitHandle == null)
				{
					throw new ArgumentNullException("waitHandles", "null handle");
				}
				if (waitHandle.safe_wait_handle == null)
				{
					throw new ArgumentException("null element found", "waitHandle");
				}
			}
		}

		// Token: 0x060041CD RID: 16845 RVA: 0x000E1F0C File Offset: 0x000E010C
		public static bool WaitAll(WaitHandle[] waitHandles)
		{
			WaitHandle.CheckArray(waitHandles, true);
			return WaitHandle.WaitAll_internal(waitHandles, -1, false);
		}

		// Token: 0x060041CE RID: 16846 RVA: 0x000E1F20 File Offset: 0x000E0120
		public static bool WaitAll(WaitHandle[] waitHandles, int millisecondsTimeout, bool exitContext)
		{
			WaitHandle.CheckArray(waitHandles, true);
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout");
			}
			bool result;
			try
			{
				if (exitContext)
				{
					SynchronizationAttribute.ExitContext();
				}
				result = WaitHandle.WaitAll_internal(waitHandles, millisecondsTimeout, false);
			}
			finally
			{
				if (exitContext)
				{
					SynchronizationAttribute.EnterContext();
				}
			}
			return result;
		}

		// Token: 0x060041CF RID: 16847 RVA: 0x000E1F90 File Offset: 0x000E0190
		public static bool WaitAll(WaitHandle[] waitHandles, TimeSpan timeout, bool exitContext)
		{
			WaitHandle.CheckArray(waitHandles, true);
			long num = (long)timeout.TotalMilliseconds;
			if (num < -1L || num > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("timeout");
			}
			bool result;
			try
			{
				if (exitContext)
				{
					SynchronizationAttribute.ExitContext();
				}
				result = WaitHandle.WaitAll_internal(waitHandles, (int)num, exitContext);
			}
			finally
			{
				if (exitContext)
				{
					SynchronizationAttribute.EnterContext();
				}
			}
			return result;
		}

		// Token: 0x060041D0 RID: 16848
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int WaitAny_internal(WaitHandle[] handles, int ms, bool exitContext);

		// Token: 0x060041D1 RID: 16849 RVA: 0x000E2014 File Offset: 0x000E0214
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int WaitAny(WaitHandle[] waitHandles)
		{
			WaitHandle.CheckArray(waitHandles, false);
			return WaitHandle.WaitAny_internal(waitHandles, -1, false);
		}

		// Token: 0x060041D2 RID: 16850 RVA: 0x000E2028 File Offset: 0x000E0228
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int WaitAny(WaitHandle[] waitHandles, int millisecondsTimeout, bool exitContext)
		{
			WaitHandle.CheckArray(waitHandles, false);
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout");
			}
			int result;
			try
			{
				if (exitContext)
				{
					SynchronizationAttribute.ExitContext();
				}
				result = WaitHandle.WaitAny_internal(waitHandles, millisecondsTimeout, exitContext);
			}
			finally
			{
				if (exitContext)
				{
					SynchronizationAttribute.EnterContext();
				}
			}
			return result;
		}

		// Token: 0x060041D3 RID: 16851 RVA: 0x000E2098 File Offset: 0x000E0298
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int WaitAny(WaitHandle[] waitHandles, TimeSpan timeout)
		{
			return WaitHandle.WaitAny(waitHandles, timeout, false);
		}

		// Token: 0x060041D4 RID: 16852 RVA: 0x000E20A4 File Offset: 0x000E02A4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int WaitAny(WaitHandle[] waitHandles, int millisecondsTimeout)
		{
			return WaitHandle.WaitAny(waitHandles, millisecondsTimeout, false);
		}

		// Token: 0x060041D5 RID: 16853 RVA: 0x000E20B0 File Offset: 0x000E02B0
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static int WaitAny(WaitHandle[] waitHandles, TimeSpan timeout, bool exitContext)
		{
			WaitHandle.CheckArray(waitHandles, false);
			long num = (long)timeout.TotalMilliseconds;
			if (num < -1L || num > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("timeout");
			}
			int result;
			try
			{
				if (exitContext)
				{
					SynchronizationAttribute.ExitContext();
				}
				result = WaitHandle.WaitAny_internal(waitHandles, (int)num, exitContext);
			}
			finally
			{
				if (exitContext)
				{
					SynchronizationAttribute.EnterContext();
				}
			}
			return result;
		}

		// Token: 0x060041D6 RID: 16854 RVA: 0x000E2134 File Offset: 0x000E0334
		public virtual void Close()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x17000C3D RID: 3133
		// (get) Token: 0x060041D7 RID: 16855 RVA: 0x000E2144 File Offset: 0x000E0344
		// (set) Token: 0x060041D8 RID: 16856 RVA: 0x000E2154 File Offset: 0x000E0354
		[Obsolete("In the profiles > 2.x, use SafeHandle instead of Handle")]
		public virtual IntPtr Handle
		{
			get
			{
				return this.safe_wait_handle.DangerousGetHandle();
			}
			[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
			[PermissionSet(SecurityAction.InheritanceDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
			set
			{
				if (value == WaitHandle.InvalidHandle)
				{
					this.safe_wait_handle = new SafeWaitHandle(WaitHandle.InvalidHandle, false);
				}
				else
				{
					this.safe_wait_handle = new SafeWaitHandle(value, true);
				}
			}
		}

		// Token: 0x060041D9 RID: 16857
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool WaitOne_internal(IntPtr handle, int ms, bool exitContext);

		// Token: 0x060041DA RID: 16858 RVA: 0x000E218C File Offset: 0x000E038C
		protected virtual void Dispose(bool explicitDisposing)
		{
			if (!this.disposed)
			{
				this.disposed = true;
				if (this.safe_wait_handle == null)
				{
					return;
				}
				lock (this)
				{
					if (this.safe_wait_handle != null)
					{
						this.safe_wait_handle.Dispose();
					}
				}
			}
		}

		// Token: 0x17000C3E RID: 3134
		// (get) Token: 0x060041DB RID: 16859 RVA: 0x000E2200 File Offset: 0x000E0400
		// (set) Token: 0x060041DC RID: 16860 RVA: 0x000E2208 File Offset: 0x000E0408
		public SafeWaitHandle SafeWaitHandle
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
			get
			{
				return this.safe_wait_handle;
			}
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			set
			{
				if (value == null)
				{
					this.safe_wait_handle = new SafeWaitHandle(WaitHandle.InvalidHandle, false);
				}
				else
				{
					this.safe_wait_handle = value;
				}
			}
		}

		// Token: 0x060041DD RID: 16861 RVA: 0x000E2230 File Offset: 0x000E0430
		public static bool SignalAndWait(WaitHandle toSignal, WaitHandle toWaitOn)
		{
			return WaitHandle.SignalAndWait(toSignal, toWaitOn, -1, false);
		}

		// Token: 0x060041DE RID: 16862 RVA: 0x000E223C File Offset: 0x000E043C
		public static bool SignalAndWait(WaitHandle toSignal, WaitHandle toWaitOn, int millisecondsTimeout, bool exitContext)
		{
			if (toSignal == null)
			{
				throw new ArgumentNullException("toSignal");
			}
			if (toWaitOn == null)
			{
				throw new ArgumentNullException("toWaitOn");
			}
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout");
			}
			return WaitHandle.SignalAndWait_Internal(toSignal.Handle, toWaitOn.Handle, millisecondsTimeout, exitContext);
		}

		// Token: 0x060041DF RID: 16863 RVA: 0x000E2290 File Offset: 0x000E0490
		public static bool SignalAndWait(WaitHandle toSignal, WaitHandle toWaitOn, TimeSpan timeout, bool exitContext)
		{
			double totalMilliseconds = timeout.TotalMilliseconds;
			if (totalMilliseconds > 2147483647.0)
			{
				throw new ArgumentOutOfRangeException("timeout");
			}
			return WaitHandle.SignalAndWait(toSignal, toWaitOn, Convert.ToInt32(totalMilliseconds), false);
		}

		// Token: 0x060041E0 RID: 16864
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool SignalAndWait_Internal(IntPtr toSignal, IntPtr toWaitOn, int ms, bool exitContext);

		// Token: 0x060041E1 RID: 16865 RVA: 0x000E22D0 File Offset: 0x000E04D0
		public virtual bool WaitOne()
		{
			this.CheckDisposed();
			bool flag = false;
			bool result;
			try
			{
				this.safe_wait_handle.DangerousAddRef(ref flag);
				result = this.WaitOne_internal(this.safe_wait_handle.DangerousGetHandle(), -1, false);
			}
			finally
			{
				if (flag)
				{
					this.safe_wait_handle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x060041E2 RID: 16866 RVA: 0x000E2340 File Offset: 0x000E0540
		public virtual bool WaitOne(int millisecondsTimeout, bool exitContext)
		{
			this.CheckDisposed();
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout");
			}
			bool flag = false;
			bool result;
			try
			{
				if (exitContext)
				{
					SynchronizationAttribute.ExitContext();
				}
				this.safe_wait_handle.DangerousAddRef(ref flag);
				result = this.WaitOne_internal(this.safe_wait_handle.DangerousGetHandle(), millisecondsTimeout, exitContext);
			}
			finally
			{
				if (exitContext)
				{
					SynchronizationAttribute.EnterContext();
				}
				if (flag)
				{
					this.safe_wait_handle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x060041E3 RID: 16867 RVA: 0x000E23D8 File Offset: 0x000E05D8
		public virtual bool WaitOne(int millisecondsTimeout)
		{
			return this.WaitOne(millisecondsTimeout, false);
		}

		// Token: 0x060041E4 RID: 16868 RVA: 0x000E23E4 File Offset: 0x000E05E4
		public virtual bool WaitOne(TimeSpan timeout)
		{
			return this.WaitOne(timeout, false);
		}

		// Token: 0x060041E5 RID: 16869 RVA: 0x000E23F0 File Offset: 0x000E05F0
		public virtual bool WaitOne(TimeSpan timeout, bool exitContext)
		{
			this.CheckDisposed();
			long num = (long)timeout.TotalMilliseconds;
			if (num < -1L || num > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("timeout");
			}
			bool flag = false;
			bool result;
			try
			{
				if (exitContext)
				{
					SynchronizationAttribute.ExitContext();
				}
				this.safe_wait_handle.DangerousAddRef(ref flag);
				result = this.WaitOne_internal(this.safe_wait_handle.DangerousGetHandle(), (int)num, exitContext);
			}
			finally
			{
				if (exitContext)
				{
					SynchronizationAttribute.EnterContext();
				}
				if (flag)
				{
					this.safe_wait_handle.DangerousRelease();
				}
			}
			return result;
		}

		// Token: 0x060041E6 RID: 16870 RVA: 0x000E24A0 File Offset: 0x000E06A0
		internal void CheckDisposed()
		{
			if (this.disposed || this.safe_wait_handle == null)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
		}

		// Token: 0x060041E7 RID: 16871 RVA: 0x000E24CC File Offset: 0x000E06CC
		public static bool WaitAll(WaitHandle[] waitHandles, int millisecondsTimeout)
		{
			return WaitHandle.WaitAll(waitHandles, millisecondsTimeout, false);
		}

		// Token: 0x060041E8 RID: 16872 RVA: 0x000E24D8 File Offset: 0x000E06D8
		public static bool WaitAll(WaitHandle[] waitHandles, TimeSpan timeout)
		{
			return WaitHandle.WaitAll(waitHandles, timeout, false);
		}

		// Token: 0x060041E9 RID: 16873 RVA: 0x000E24E4 File Offset: 0x000E06E4
		~WaitHandle()
		{
			this.Dispose(false);
		}

		// Token: 0x04001C28 RID: 7208
		public const int WaitTimeout = 258;

		// Token: 0x04001C29 RID: 7209
		private SafeWaitHandle safe_wait_handle;

		// Token: 0x04001C2A RID: 7210
		protected static readonly IntPtr InvalidHandle = (IntPtr)(-1);

		// Token: 0x04001C2B RID: 7211
		private bool disposed;
	}
}
