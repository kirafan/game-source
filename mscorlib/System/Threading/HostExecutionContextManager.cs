﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Security.Permissions;

namespace System.Threading
{
	// Token: 0x020006A0 RID: 1696
	public class HostExecutionContextManager
	{
		// Token: 0x06004088 RID: 16520 RVA: 0x000DF330 File Offset: 0x000DD530
		[MonoTODO]
		public virtual HostExecutionContext Capture()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06004089 RID: 16521 RVA: 0x000DF338 File Offset: 0x000DD538
		[MonoTODO]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public virtual void Revert(object previousState)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600408A RID: 16522 RVA: 0x000DF340 File Offset: 0x000DD540
		[MonoTODO]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
		public virtual object SetHostExecutionContext(HostExecutionContext hostExecutionContext)
		{
			throw new NotImplementedException();
		}
	}
}
