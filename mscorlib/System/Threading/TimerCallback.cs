﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006FD RID: 1789
	// (Invoke) Token: 0x060043E8 RID: 17384
	[ComVisible(true)]
	public delegate void TimerCallback(object state);
}
