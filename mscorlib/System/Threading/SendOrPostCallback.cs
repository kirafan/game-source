﻿using System;

namespace System.Threading
{
	// Token: 0x020006FB RID: 1787
	// (Invoke) Token: 0x060043E0 RID: 17376
	public delegate void SendOrPostCallback(object state);
}
