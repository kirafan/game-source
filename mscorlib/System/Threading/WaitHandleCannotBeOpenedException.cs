﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Threading
{
	// Token: 0x020006BB RID: 1723
	[ComVisible(false)]
	[Serializable]
	public class WaitHandleCannotBeOpenedException : ApplicationException
	{
		// Token: 0x060041EA RID: 16874 RVA: 0x000E2520 File Offset: 0x000E0720
		public WaitHandleCannotBeOpenedException() : base(Locale.GetText("Named handle doesn't exists."))
		{
		}

		// Token: 0x060041EB RID: 16875 RVA: 0x000E2534 File Offset: 0x000E0734
		public WaitHandleCannotBeOpenedException(string message) : base(message)
		{
		}

		// Token: 0x060041EC RID: 16876 RVA: 0x000E2540 File Offset: 0x000E0740
		public WaitHandleCannotBeOpenedException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060041ED RID: 16877 RVA: 0x000E254C File Offset: 0x000E074C
		protected WaitHandleCannotBeOpenedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
