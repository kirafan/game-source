﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Threading
{
	// Token: 0x020006AF RID: 1711
	[ComVisible(true)]
	[Serializable]
	public sealed class ThreadAbortException : SystemException
	{
		// Token: 0x0600418D RID: 16781 RVA: 0x000E1458 File Offset: 0x000DF658
		private ThreadAbortException() : base("Thread was being aborted")
		{
			base.HResult = -2146233040;
		}

		// Token: 0x0600418E RID: 16782 RVA: 0x000E1470 File Offset: 0x000DF670
		private ThreadAbortException(SerializationInfo info, StreamingContext sc) : base(info, sc)
		{
		}

		// Token: 0x17000C3B RID: 3131
		// (get) Token: 0x0600418F RID: 16783 RVA: 0x000E147C File Offset: 0x000DF67C
		public object ExceptionState
		{
			get
			{
				return Thread.CurrentThread.GetAbortExceptionState();
			}
		}
	}
}
