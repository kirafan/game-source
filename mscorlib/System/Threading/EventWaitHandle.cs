﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.AccessControl;

namespace System.Threading
{
	// Token: 0x0200069D RID: 1693
	[ComVisible(true)]
	public class EventWaitHandle : WaitHandle
	{
		// Token: 0x06004068 RID: 16488 RVA: 0x000DEF64 File Offset: 0x000DD164
		private EventWaitHandle(IntPtr handle)
		{
			this.Handle = handle;
		}

		// Token: 0x06004069 RID: 16489 RVA: 0x000DEF74 File Offset: 0x000DD174
		public EventWaitHandle(bool initialState, EventResetMode mode)
		{
			bool manual = this.IsManualReset(mode);
			bool flag;
			this.Handle = NativeEventCalls.CreateEvent_internal(manual, initialState, null, out flag);
		}

		// Token: 0x0600406A RID: 16490 RVA: 0x000DEFA0 File Offset: 0x000DD1A0
		public EventWaitHandle(bool initialState, EventResetMode mode, string name)
		{
			bool manual = this.IsManualReset(mode);
			bool flag;
			this.Handle = NativeEventCalls.CreateEvent_internal(manual, initialState, name, out flag);
		}

		// Token: 0x0600406B RID: 16491 RVA: 0x000DEFCC File Offset: 0x000DD1CC
		public EventWaitHandle(bool initialState, EventResetMode mode, string name, out bool createdNew)
		{
			bool manual = this.IsManualReset(mode);
			this.Handle = NativeEventCalls.CreateEvent_internal(manual, initialState, name, out createdNew);
		}

		// Token: 0x0600406C RID: 16492 RVA: 0x000DEFF8 File Offset: 0x000DD1F8
		[MonoTODO("Implement access control")]
		public EventWaitHandle(bool initialState, EventResetMode mode, string name, out bool createdNew, EventWaitHandleSecurity eventSecurity)
		{
			bool manual = this.IsManualReset(mode);
			this.Handle = NativeEventCalls.CreateEvent_internal(manual, initialState, name, out createdNew);
		}

		// Token: 0x0600406D RID: 16493 RVA: 0x000DF024 File Offset: 0x000DD224
		private bool IsManualReset(EventResetMode mode)
		{
			if (mode < EventResetMode.AutoReset || mode > EventResetMode.ManualReset)
			{
				throw new ArgumentException("mode");
			}
			return mode == EventResetMode.ManualReset;
		}

		// Token: 0x0600406E RID: 16494 RVA: 0x000DF044 File Offset: 0x000DD244
		[MonoTODO]
		public EventWaitHandleSecurity GetAccessControl()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600406F RID: 16495 RVA: 0x000DF04C File Offset: 0x000DD24C
		public static EventWaitHandle OpenExisting(string name)
		{
			return EventWaitHandle.OpenExisting(name, EventWaitHandleRights.Modify | EventWaitHandleRights.Synchronize);
		}

		// Token: 0x06004070 RID: 16496 RVA: 0x000DF05C File Offset: 0x000DD25C
		public static EventWaitHandle OpenExisting(string name, EventWaitHandleRights rights)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0 || name.Length > 260)
			{
				throw new ArgumentException("name", Locale.GetText("Invalid length [1-260]."));
			}
			MonoIOError monoIOError;
			IntPtr intPtr = NativeEventCalls.OpenEvent_internal(name, rights, out monoIOError);
			if (!(intPtr == (IntPtr)null))
			{
				return new EventWaitHandle(intPtr);
			}
			if (monoIOError == MonoIOError.ERROR_FILE_NOT_FOUND)
			{
				throw new WaitHandleCannotBeOpenedException(Locale.GetText("Named Event handle does not exist: ") + name);
			}
			if (monoIOError == MonoIOError.ERROR_ACCESS_DENIED)
			{
				throw new UnauthorizedAccessException();
			}
			throw new IOException(Locale.GetText("Win32 IO error: ") + monoIOError.ToString());
		}

		// Token: 0x06004071 RID: 16497 RVA: 0x000DF118 File Offset: 0x000DD318
		public bool Reset()
		{
			base.CheckDisposed();
			return NativeEventCalls.ResetEvent_internal(this.Handle);
		}

		// Token: 0x06004072 RID: 16498 RVA: 0x000DF12C File Offset: 0x000DD32C
		public bool Set()
		{
			base.CheckDisposed();
			return NativeEventCalls.SetEvent_internal(this.Handle);
		}

		// Token: 0x06004073 RID: 16499 RVA: 0x000DF140 File Offset: 0x000DD340
		[MonoTODO]
		public void SetAccessControl(EventWaitHandleSecurity eventSecurity)
		{
			throw new NotImplementedException();
		}
	}
}
