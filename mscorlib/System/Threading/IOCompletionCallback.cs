﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006F9 RID: 1785
	// (Invoke) Token: 0x060043D8 RID: 17368
	[ComVisible(true)]
	[CLSCompliant(false)]
	public unsafe delegate void IOCompletionCallback(uint errorCode, uint numBytes, NativeOverlapped* pOVERLAP);
}
