﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Permissions;

namespace System.Threading
{
	// Token: 0x020006A6 RID: 1702
	[ComVisible(true)]
	public sealed class Mutex : WaitHandle
	{
		// Token: 0x060040BE RID: 16574 RVA: 0x000DF7E8 File Offset: 0x000DD9E8
		private Mutex(IntPtr handle)
		{
			this.Handle = handle;
		}

		// Token: 0x060040BF RID: 16575 RVA: 0x000DF7F8 File Offset: 0x000DD9F8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public Mutex()
		{
			bool flag;
			this.Handle = Mutex.CreateMutex_internal(false, null, out flag);
		}

		// Token: 0x060040C0 RID: 16576 RVA: 0x000DF81C File Offset: 0x000DDA1C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public Mutex(bool initiallyOwned)
		{
			bool flag;
			this.Handle = Mutex.CreateMutex_internal(initiallyOwned, null, out flag);
		}

		// Token: 0x060040C1 RID: 16577 RVA: 0x000DF840 File Offset: 0x000DDA40
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public Mutex(bool initiallyOwned, string name)
		{
			bool flag;
			this.Handle = Mutex.CreateMutex_internal(initiallyOwned, name, out flag);
		}

		// Token: 0x060040C2 RID: 16578 RVA: 0x000DF864 File Offset: 0x000DDA64
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public Mutex(bool initiallyOwned, string name, out bool createdNew)
		{
			this.Handle = Mutex.CreateMutex_internal(initiallyOwned, name, out createdNew);
		}

		// Token: 0x060040C3 RID: 16579 RVA: 0x000DF87C File Offset: 0x000DDA7C
		[MonoTODO("Implement MutexSecurity")]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public Mutex(bool initiallyOwned, string name, out bool createdNew, MutexSecurity mutexSecurity)
		{
			this.Handle = Mutex.CreateMutex_internal(initiallyOwned, name, out createdNew);
		}

		// Token: 0x060040C4 RID: 16580
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr CreateMutex_internal(bool initiallyOwned, string name, out bool created);

		// Token: 0x060040C5 RID: 16581
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ReleaseMutex_internal(IntPtr handle);

		// Token: 0x060040C6 RID: 16582
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr OpenMutex_internal(string name, MutexRights rights, out MonoIOError error);

		// Token: 0x060040C7 RID: 16583 RVA: 0x000DF894 File Offset: 0x000DDA94
		public MutexSecurity GetAccessControl()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060040C8 RID: 16584 RVA: 0x000DF89C File Offset: 0x000DDA9C
		public static Mutex OpenExisting(string name)
		{
			return Mutex.OpenExisting(name, MutexRights.Modify | MutexRights.Synchronize);
		}

		// Token: 0x060040C9 RID: 16585 RVA: 0x000DF8AC File Offset: 0x000DDAAC
		public static Mutex OpenExisting(string name, MutexRights rights)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0 || name.Length > 260)
			{
				throw new ArgumentException("name", Locale.GetText("Invalid length [1-260]."));
			}
			MonoIOError monoIOError;
			IntPtr intPtr = Mutex.OpenMutex_internal(name, rights, out monoIOError);
			if (!(intPtr == (IntPtr)null))
			{
				return new Mutex(intPtr);
			}
			if (monoIOError == MonoIOError.ERROR_FILE_NOT_FOUND)
			{
				throw new WaitHandleCannotBeOpenedException(Locale.GetText("Named Mutex handle does not exist: ") + name);
			}
			if (monoIOError == MonoIOError.ERROR_ACCESS_DENIED)
			{
				throw new UnauthorizedAccessException();
			}
			throw new IOException(Locale.GetText("Win32 IO error: ") + monoIOError.ToString());
		}

		// Token: 0x060040CA RID: 16586 RVA: 0x000DF968 File Offset: 0x000DDB68
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public void ReleaseMutex()
		{
			if (!Mutex.ReleaseMutex_internal(this.Handle))
			{
				throw new ApplicationException("Mutex is not owned");
			}
		}

		// Token: 0x060040CB RID: 16587 RVA: 0x000DF994 File Offset: 0x000DDB94
		public void SetAccessControl(MutexSecurity mutexSecurity)
		{
			throw new NotImplementedException();
		}
	}
}
