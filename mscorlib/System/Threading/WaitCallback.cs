﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006FE RID: 1790
	// (Invoke) Token: 0x060043EC RID: 17388
	[ComVisible(true)]
	public delegate void WaitCallback(object state);
}
