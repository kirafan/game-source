﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Threading
{
	// Token: 0x020006B5 RID: 1717
	[ComVisible(true)]
	[Serializable]
	public class ThreadStateException : SystemException
	{
		// Token: 0x060041AB RID: 16811 RVA: 0x000E1674 File Offset: 0x000DF874
		public ThreadStateException() : base("Thread State Error")
		{
		}

		// Token: 0x060041AC RID: 16812 RVA: 0x000E1684 File Offset: 0x000DF884
		public ThreadStateException(string message) : base(message)
		{
		}

		// Token: 0x060041AD RID: 16813 RVA: 0x000E1690 File Offset: 0x000DF890
		protected ThreadStateException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060041AE RID: 16814 RVA: 0x000E169C File Offset: 0x000DF89C
		public ThreadStateException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
