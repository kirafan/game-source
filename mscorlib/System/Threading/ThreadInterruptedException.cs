﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Threading
{
	// Token: 0x020006B0 RID: 1712
	[ComVisible(true)]
	[Serializable]
	public class ThreadInterruptedException : SystemException
	{
		// Token: 0x06004190 RID: 16784 RVA: 0x000E1488 File Offset: 0x000DF688
		public ThreadInterruptedException() : base("Thread interrupted")
		{
		}

		// Token: 0x06004191 RID: 16785 RVA: 0x000E1498 File Offset: 0x000DF698
		public ThreadInterruptedException(string message) : base(message)
		{
		}

		// Token: 0x06004192 RID: 16786 RVA: 0x000E14A4 File Offset: 0x000DF6A4
		protected ThreadInterruptedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06004193 RID: 16787 RVA: 0x000E14B0 File Offset: 0x000DF6B0
		public ThreadInterruptedException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
