﻿using System;
using System.Runtime.Serialization;

namespace System.Threading
{
	// Token: 0x020006B3 RID: 1715
	[Serializable]
	public sealed class ThreadStartException : SystemException
	{
		// Token: 0x060041A7 RID: 16807 RVA: 0x000E1640 File Offset: 0x000DF840
		internal ThreadStartException() : base("Thread Start Error")
		{
		}

		// Token: 0x060041A8 RID: 16808 RVA: 0x000E1650 File Offset: 0x000DF850
		internal ThreadStartException(string message) : base(message)
		{
		}

		// Token: 0x060041A9 RID: 16809 RVA: 0x000E165C File Offset: 0x000DF85C
		internal ThreadStartException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060041AA RID: 16810 RVA: 0x000E1668 File Offset: 0x000DF868
		internal ThreadStartException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
