﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x0200069A RID: 1690
	[ComVisible(true)]
	public sealed class AutoResetEvent : EventWaitHandle
	{
		// Token: 0x0600405D RID: 16477 RVA: 0x000DECF4 File Offset: 0x000DCEF4
		public AutoResetEvent(bool initialState) : base(initialState, EventResetMode.AutoReset)
		{
		}
	}
}
