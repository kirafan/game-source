﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006A2 RID: 1698
	[ComVisible(true)]
	public struct LockCookie
	{
		// Token: 0x060040A0 RID: 16544 RVA: 0x000DF348 File Offset: 0x000DD548
		internal LockCookie(int thread_id)
		{
			this.ThreadId = thread_id;
			this.ReaderLocks = 0;
			this.WriterLocks = 0;
		}

		// Token: 0x060040A1 RID: 16545 RVA: 0x000DF360 File Offset: 0x000DD560
		internal LockCookie(int thread_id, int reader_locks, int writer_locks)
		{
			this.ThreadId = thread_id;
			this.ReaderLocks = reader_locks;
			this.WriterLocks = writer_locks;
		}

		// Token: 0x060040A2 RID: 16546 RVA: 0x000DF378 File Offset: 0x000DD578
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x060040A3 RID: 16547 RVA: 0x000DF38C File Offset: 0x000DD58C
		public bool Equals(LockCookie obj)
		{
			return this.ThreadId == obj.ThreadId && this.ReaderLocks == obj.ReaderLocks && this.WriterLocks == obj.WriterLocks;
		}

		// Token: 0x060040A4 RID: 16548 RVA: 0x000DF3C8 File Offset: 0x000DD5C8
		public override bool Equals(object obj)
		{
			return obj is LockCookie && obj.Equals(this);
		}

		// Token: 0x060040A5 RID: 16549 RVA: 0x000DF3E8 File Offset: 0x000DD5E8
		public static bool operator ==(LockCookie a, LockCookie b)
		{
			return a.Equals(b);
		}

		// Token: 0x060040A6 RID: 16550 RVA: 0x000DF3F4 File Offset: 0x000DD5F4
		public static bool operator !=(LockCookie a, LockCookie b)
		{
			return !a.Equals(b);
		}

		// Token: 0x04001BB6 RID: 7094
		internal int ThreadId;

		// Token: 0x04001BB7 RID: 7095
		internal int ReaderLocks;

		// Token: 0x04001BB8 RID: 7096
		internal int WriterLocks;
	}
}
