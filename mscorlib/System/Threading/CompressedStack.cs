﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;

namespace System.Threading
{
	// Token: 0x0200069B RID: 1691
	[Serializable]
	public sealed class CompressedStack : ISerializable
	{
		// Token: 0x0600405E RID: 16478 RVA: 0x000DED00 File Offset: 0x000DCF00
		internal CompressedStack(int length)
		{
			if (length > 0)
			{
				this._list = new ArrayList(length);
			}
		}

		// Token: 0x0600405F RID: 16479 RVA: 0x000DED1C File Offset: 0x000DCF1C
		internal CompressedStack(CompressedStack cs)
		{
			if (cs != null && cs._list != null)
			{
				this._list = (ArrayList)cs._list.Clone();
			}
		}

		// Token: 0x06004060 RID: 16480 RVA: 0x000DED4C File Offset: 0x000DCF4C
		[ComVisible(false)]
		public CompressedStack CreateCopy()
		{
			return new CompressedStack(this);
		}

		// Token: 0x06004061 RID: 16481 RVA: 0x000DED54 File Offset: 0x000DCF54
		public static CompressedStack Capture()
		{
			CompressedStack compressedStack = new CompressedStack(0);
			compressedStack._list = SecurityFrame.GetStack(1);
			CompressedStack compressedStack2 = Thread.CurrentThread.GetCompressedStack();
			if (compressedStack2 != null)
			{
				for (int i = 0; i < compressedStack2._list.Count; i++)
				{
					compressedStack._list.Add(compressedStack2._list[i]);
				}
			}
			return compressedStack;
		}

		// Token: 0x06004062 RID: 16482 RVA: 0x000DEDBC File Offset: 0x000DCFBC
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n   <IPermission class=\"System.Security.Permissions.StrongNameIdentityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                PublicKeyBlob=\"00000000000000000400000000000000\"/>\n</PermissionSet>\n")]
		public static CompressedStack GetCompressedStack()
		{
			CompressedStack compressedStack = Thread.CurrentThread.GetCompressedStack();
			if (compressedStack == null)
			{
				compressedStack = CompressedStack.Capture();
			}
			else
			{
				CompressedStack compressedStack2 = CompressedStack.Capture();
				for (int i = 0; i < compressedStack2._list.Count; i++)
				{
					compressedStack._list.Add(compressedStack2._list[i]);
				}
			}
			return compressedStack;
		}

		// Token: 0x06004063 RID: 16483 RVA: 0x000DEE20 File Offset: 0x000DD020
		[MonoTODO("incomplete")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"MemberAccess\"/>\n</PermissionSet>\n")]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
		}

		// Token: 0x06004064 RID: 16484 RVA: 0x000DEE34 File Offset: 0x000DD034
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
		public static void Run(CompressedStack compressedStack, ContextCallback callback, object state)
		{
			if (compressedStack == null)
			{
				throw new ArgumentException("compressedStack");
			}
			Thread currentThread = Thread.CurrentThread;
			CompressedStack compressedStack2 = null;
			try
			{
				compressedStack2 = currentThread.GetCompressedStack();
				currentThread.SetCompressedStack(compressedStack);
				callback(state);
			}
			finally
			{
				if (compressedStack2 != null)
				{
					currentThread.SetCompressedStack(compressedStack2);
				}
			}
		}

		// Token: 0x06004065 RID: 16485 RVA: 0x000DEEA0 File Offset: 0x000DD0A0
		internal bool Equals(CompressedStack cs)
		{
			if (this.IsEmpty())
			{
				return cs.IsEmpty();
			}
			if (cs.IsEmpty())
			{
				return false;
			}
			if (this._list.Count != cs._list.Count)
			{
				return false;
			}
			for (int i = 0; i < this._list.Count; i++)
			{
				SecurityFrame securityFrame = (SecurityFrame)this._list[i];
				SecurityFrame sf = (SecurityFrame)cs._list[i];
				if (!securityFrame.Equals(sf))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06004066 RID: 16486 RVA: 0x000DEF3C File Offset: 0x000DD13C
		internal bool IsEmpty()
		{
			return this._list == null || this._list.Count == 0;
		}

		// Token: 0x17000C1D RID: 3101
		// (get) Token: 0x06004067 RID: 16487 RVA: 0x000DEF5C File Offset: 0x000DD15C
		internal IList List
		{
			get
			{
				return this._list;
			}
		}

		// Token: 0x04001BAE RID: 7086
		private ArrayList _list;
	}
}
