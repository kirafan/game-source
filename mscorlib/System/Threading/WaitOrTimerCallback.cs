﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006FF RID: 1791
	// (Invoke) Token: 0x060043F0 RID: 17392
	[ComVisible(true)]
	public delegate void WaitOrTimerCallback(object state, bool timedOut);
}
