﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006A8 RID: 1704
	[ComVisible(true)]
	public struct NativeOverlapped
	{
		// Token: 0x04001BBB RID: 7099
		public IntPtr EventHandle;

		// Token: 0x04001BBC RID: 7100
		public IntPtr InternalHigh;

		// Token: 0x04001BBD RID: 7101
		public IntPtr InternalLow;

		// Token: 0x04001BBE RID: 7102
		public int OffsetHigh;

		// Token: 0x04001BBF RID: 7103
		public int OffsetLow;

		// Token: 0x04001BC0 RID: 7104
		internal int Handle1;

		// Token: 0x04001BC1 RID: 7105
		internal int Handle2;
	}
}
