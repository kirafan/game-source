﻿using System;
using System.Security;

namespace System.Threading
{
	// Token: 0x02000699 RID: 1689
	public struct AsyncFlowControl : IDisposable
	{
		// Token: 0x06004055 RID: 16469 RVA: 0x000DEBE4 File Offset: 0x000DCDE4
		internal AsyncFlowControl(Thread t, AsyncFlowControlType type)
		{
			this._t = t;
			this._type = type;
		}

		// Token: 0x06004056 RID: 16470 RVA: 0x000DEBF4 File Offset: 0x000DCDF4
		void IDisposable.Dispose()
		{
			if (this._t != null)
			{
				this.Undo();
				this._t = null;
				this._type = AsyncFlowControlType.None;
			}
		}

		// Token: 0x06004057 RID: 16471 RVA: 0x000DEC18 File Offset: 0x000DCE18
		public void Undo()
		{
			if (this._t == null)
			{
				throw new InvalidOperationException(Locale.GetText("Can only be called once."));
			}
			AsyncFlowControlType type = this._type;
			if (type != AsyncFlowControlType.Execution)
			{
				if (type == AsyncFlowControlType.Security)
				{
					SecurityContext.RestoreFlow();
				}
			}
			else
			{
				ExecutionContext.RestoreFlow();
			}
			this._t = null;
		}

		// Token: 0x06004058 RID: 16472 RVA: 0x000DEC78 File Offset: 0x000DCE78
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06004059 RID: 16473 RVA: 0x000DEC8C File Offset: 0x000DCE8C
		public override bool Equals(object obj)
		{
			return obj is AsyncFlowControl && obj.Equals(this);
		}

		// Token: 0x0600405A RID: 16474 RVA: 0x000DECAC File Offset: 0x000DCEAC
		public bool Equals(AsyncFlowControl obj)
		{
			return this._t == obj._t && this._type == obj._type;
		}

		// Token: 0x0600405B RID: 16475 RVA: 0x000DECD8 File Offset: 0x000DCED8
		public static bool operator ==(AsyncFlowControl a, AsyncFlowControl b)
		{
			return a.Equals(b);
		}

		// Token: 0x0600405C RID: 16476 RVA: 0x000DECE4 File Offset: 0x000DCEE4
		public static bool operator !=(AsyncFlowControl a, AsyncFlowControl b)
		{
			return !a.Equals(b);
		}

		// Token: 0x04001BAC RID: 7084
		private Thread _t;

		// Token: 0x04001BAD RID: 7085
		private AsyncFlowControlType _type;
	}
}
