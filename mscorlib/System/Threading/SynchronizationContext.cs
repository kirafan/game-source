﻿using System;
using System.Runtime.ConstrainedExecution;

namespace System.Threading
{
	// Token: 0x020006AC RID: 1708
	public class SynchronizationContext
	{
		// Token: 0x06004101 RID: 16641 RVA: 0x000E0728 File Offset: 0x000DE928
		public SynchronizationContext()
		{
		}

		// Token: 0x06004102 RID: 16642 RVA: 0x000E0730 File Offset: 0x000DE930
		internal SynchronizationContext(SynchronizationContext context)
		{
			SynchronizationContext.currentContext = context;
		}

		// Token: 0x17000C2A RID: 3114
		// (get) Token: 0x06004103 RID: 16643 RVA: 0x000E0740 File Offset: 0x000DE940
		public static SynchronizationContext Current
		{
			get
			{
				return SynchronizationContext.currentContext;
			}
		}

		// Token: 0x06004104 RID: 16644 RVA: 0x000E0748 File Offset: 0x000DE948
		public virtual SynchronizationContext CreateCopy()
		{
			return new SynchronizationContext(this);
		}

		// Token: 0x06004105 RID: 16645 RVA: 0x000E0750 File Offset: 0x000DE950
		public bool IsWaitNotificationRequired()
		{
			return this.notification_required;
		}

		// Token: 0x06004106 RID: 16646 RVA: 0x000E0758 File Offset: 0x000DE958
		public virtual void OperationCompleted()
		{
		}

		// Token: 0x06004107 RID: 16647 RVA: 0x000E075C File Offset: 0x000DE95C
		public virtual void OperationStarted()
		{
		}

		// Token: 0x06004108 RID: 16648 RVA: 0x000E0760 File Offset: 0x000DE960
		public virtual void Post(SendOrPostCallback d, object state)
		{
			ThreadPool.QueueUserWorkItem(new WaitCallback(d.Invoke), state);
		}

		// Token: 0x06004109 RID: 16649 RVA: 0x000E0778 File Offset: 0x000DE978
		public virtual void Send(SendOrPostCallback d, object state)
		{
			d(state);
		}

		// Token: 0x0600410A RID: 16650 RVA: 0x000E0784 File Offset: 0x000DE984
		public static void SetSynchronizationContext(SynchronizationContext syncContext)
		{
			SynchronizationContext.currentContext = syncContext;
		}

		// Token: 0x0600410B RID: 16651 RVA: 0x000E078C File Offset: 0x000DE98C
		[MonoTODO]
		protected void SetWaitNotificationRequired()
		{
			this.notification_required = true;
			throw new NotImplementedException();
		}

		// Token: 0x0600410C RID: 16652 RVA: 0x000E079C File Offset: 0x000DE99C
		[CLSCompliant(false)]
		[PrePrepareMethod]
		public virtual int Wait(IntPtr[] waitHandles, bool waitAll, int millisecondsTimeout)
		{
			return SynchronizationContext.WaitHelper(waitHandles, waitAll, millisecondsTimeout);
		}

		// Token: 0x0600410D RID: 16653 RVA: 0x000E07A8 File Offset: 0x000DE9A8
		[CLSCompliant(false)]
		[PrePrepareMethod]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		[MonoTODO]
		protected static int WaitHelper(IntPtr[] waitHandles, bool waitAll, int millisecondsTimeout)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04001BD6 RID: 7126
		private bool notification_required;

		// Token: 0x04001BD7 RID: 7127
		[ThreadStatic]
		private static SynchronizationContext currentContext;
	}
}
