﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Threading
{
	// Token: 0x020006B1 RID: 1713
	public static class ThreadPool
	{
		// Token: 0x06004194 RID: 16788 RVA: 0x000E14BC File Offset: 0x000DF6BC
		[Obsolete("This method is obsolete, use BindHandle(SafeHandle) instead")]
		public static bool BindHandle(IntPtr osHandle)
		{
			return true;
		}

		// Token: 0x06004195 RID: 16789 RVA: 0x000E14C0 File Offset: 0x000DF6C0
		public static bool BindHandle(SafeHandle osHandle)
		{
			if (osHandle == null)
			{
				throw new ArgumentNullException("osHandle");
			}
			return true;
		}

		// Token: 0x06004196 RID: 16790
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void GetAvailableThreads(out int workerThreads, out int completionPortThreads);

		// Token: 0x06004197 RID: 16791
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void GetMaxThreads(out int workerThreads, out int completionPortThreads);

		// Token: 0x06004198 RID: 16792
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void GetMinThreads(out int workerThreads, out int completionPortThreads);

		// Token: 0x06004199 RID: 16793
		[MonoTODO("The min number of completion port threads is not evaluated.")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlThread\"/>\n</PermissionSet>\n")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SetMinThreads(int workerThreads, int completionPortThreads);

		// Token: 0x0600419A RID: 16794
		[MonoTODO("The max number of threads cannot be decremented.")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlThread\"/>\n</PermissionSet>\n")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool SetMaxThreads(int workerThreads, int completionPortThreads);

		// Token: 0x0600419B RID: 16795 RVA: 0x000E14D4 File Offset: 0x000DF6D4
		public static bool QueueUserWorkItem(WaitCallback callBack)
		{
			return ThreadPool.QueueUserWorkItem(callBack, null);
		}

		// Token: 0x0600419C RID: 16796 RVA: 0x000E14E0 File Offset: 0x000DF6E0
		public static bool QueueUserWorkItem(WaitCallback callBack, object state)
		{
			if (callBack == null)
			{
				throw new ArgumentNullException("callBack");
			}
			return callBack.BeginInvoke(state, null, null) != null;
		}

		// Token: 0x0600419D RID: 16797 RVA: 0x000E1514 File Offset: 0x000DF714
		public static RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state, int millisecondsTimeOutInterval, bool executeOnlyOnce)
		{
			return ThreadPool.RegisterWaitForSingleObject(waitObject, callBack, state, (long)millisecondsTimeOutInterval, executeOnlyOnce);
		}

		// Token: 0x0600419E RID: 16798 RVA: 0x000E1524 File Offset: 0x000DF724
		public static RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state, long millisecondsTimeOutInterval, bool executeOnlyOnce)
		{
			if (millisecondsTimeOutInterval < -1L)
			{
				throw new ArgumentOutOfRangeException("timeout", "timeout < -1");
			}
			if (millisecondsTimeOutInterval > 2147483647L)
			{
				throw new NotSupportedException("Timeout is too big. Maximum is Int32.MaxValue");
			}
			TimeSpan timeout = new TimeSpan(0, 0, 0, 0, (int)millisecondsTimeOutInterval);
			RegisteredWaitHandle registeredWaitHandle = new RegisteredWaitHandle(waitObject, callBack, state, timeout, executeOnlyOnce);
			ThreadPool.QueueUserWorkItem(new WaitCallback(registeredWaitHandle.Wait), null);
			return registeredWaitHandle;
		}

		// Token: 0x0600419F RID: 16799 RVA: 0x000E1590 File Offset: 0x000DF790
		public static RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state, TimeSpan timeout, bool executeOnlyOnce)
		{
			return ThreadPool.RegisterWaitForSingleObject(waitObject, callBack, state, (long)timeout.TotalMilliseconds, executeOnlyOnce);
		}

		// Token: 0x060041A0 RID: 16800 RVA: 0x000E15A4 File Offset: 0x000DF7A4
		[CLSCompliant(false)]
		public static RegisteredWaitHandle RegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state, uint millisecondsTimeOutInterval, bool executeOnlyOnce)
		{
			return ThreadPool.RegisterWaitForSingleObject(waitObject, callBack, state, (long)((ulong)millisecondsTimeOutInterval), executeOnlyOnce);
		}

		// Token: 0x060041A1 RID: 16801 RVA: 0x000E15B4 File Offset: 0x000DF7B4
		[CLSCompliant(false)]
		public unsafe static bool UnsafeQueueNativeOverlapped(NativeOverlapped* overlapped)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060041A2 RID: 16802 RVA: 0x000E15BC File Offset: 0x000DF7BC
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
		public static bool UnsafeQueueUserWorkItem(WaitCallback callBack, object state)
		{
			IAsyncResult asyncResult = null;
			try
			{
				if (!ExecutionContext.IsFlowSuppressed())
				{
					ExecutionContext.SuppressFlow();
				}
				asyncResult = callBack.BeginInvoke(state, null, null);
			}
			finally
			{
				if (ExecutionContext.IsFlowSuppressed())
				{
					ExecutionContext.RestoreFlow();
				}
			}
			return asyncResult != null;
		}

		// Token: 0x060041A3 RID: 16803 RVA: 0x000E1620 File Offset: 0x000DF820
		[MonoTODO("Not implemented")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
		public static RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state, int millisecondsTimeOutInterval, bool executeOnlyOnce)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060041A4 RID: 16804 RVA: 0x000E1628 File Offset: 0x000DF828
		[MonoTODO("Not implemented")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
		public static RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state, long millisecondsTimeOutInterval, bool executeOnlyOnce)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060041A5 RID: 16805 RVA: 0x000E1630 File Offset: 0x000DF830
		[MonoTODO("Not implemented")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
		public static RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state, TimeSpan timeout, bool executeOnlyOnce)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060041A6 RID: 16806 RVA: 0x000E1638 File Offset: 0x000DF838
		[CLSCompliant(false)]
		[MonoTODO("Not implemented")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
		public static RegisteredWaitHandle UnsafeRegisterWaitForSingleObject(WaitHandle waitObject, WaitOrTimerCallback callBack, object state, uint millisecondsTimeOutInterval, bool executeOnlyOnce)
		{
			throw new NotImplementedException();
		}
	}
}
