﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Threading
{
	// Token: 0x020006A9 RID: 1705
	[ComVisible(true)]
	public class Overlapped
	{
		// Token: 0x060040D2 RID: 16594 RVA: 0x000DF9A4 File Offset: 0x000DDBA4
		public Overlapped()
		{
		}

		// Token: 0x060040D3 RID: 16595 RVA: 0x000DF9AC File Offset: 0x000DDBAC
		[Obsolete("Not 64bit compatible.  Please use the constructor that takes IntPtr for the event handle", false)]
		public Overlapped(int offsetLo, int offsetHi, int hEvent, IAsyncResult ar)
		{
			this.offsetL = offsetLo;
			this.offsetH = offsetHi;
			this.evt = hEvent;
			this.ares = ar;
		}

		// Token: 0x060040D4 RID: 16596 RVA: 0x000DF9D4 File Offset: 0x000DDBD4
		public Overlapped(int offsetLo, int offsetHi, IntPtr hEvent, IAsyncResult ar)
		{
			this.offsetL = offsetLo;
			this.offsetH = offsetHi;
			this.evt_ptr = hEvent;
			this.ares = ar;
		}

		// Token: 0x060040D5 RID: 16597 RVA: 0x000DF9FC File Offset: 0x000DDBFC
		[CLSCompliant(false)]
		public unsafe static void Free(NativeOverlapped* nativeOverlappedPtr)
		{
			if ((IntPtr)((void*)nativeOverlappedPtr) == IntPtr.Zero)
			{
				throw new ArgumentNullException("nativeOverlappedPtr");
			}
			Marshal.FreeHGlobal((IntPtr)((void*)nativeOverlappedPtr));
		}

		// Token: 0x060040D6 RID: 16598 RVA: 0x000DFA2C File Offset: 0x000DDC2C
		[CLSCompliant(false)]
		public unsafe static Overlapped Unpack(NativeOverlapped* nativeOverlappedPtr)
		{
			if ((IntPtr)((void*)nativeOverlappedPtr) == IntPtr.Zero)
			{
				throw new ArgumentNullException("nativeOverlappedPtr");
			}
			return new Overlapped
			{
				offsetL = nativeOverlappedPtr->OffsetLow,
				offsetH = nativeOverlappedPtr->OffsetHigh,
				evt = (int)nativeOverlappedPtr->EventHandle
			};
		}

		// Token: 0x060040D7 RID: 16599 RVA: 0x000DFA8C File Offset: 0x000DDC8C
		[CLSCompliant(false)]
		[Obsolete("Use Pack(iocb, userData) instead", false)]
		[MonoTODO("Security - we need to propagate the call stack")]
		public unsafe NativeOverlapped* Pack(IOCompletionCallback iocb)
		{
			NativeOverlapped* ptr = (NativeOverlapped*)((void*)Marshal.AllocHGlobal(Marshal.SizeOf(typeof(NativeOverlapped))));
			ptr->OffsetLow = this.offsetL;
			ptr->OffsetHigh = this.offsetH;
			ptr->EventHandle = (IntPtr)this.evt;
			return ptr;
		}

		// Token: 0x060040D8 RID: 16600 RVA: 0x000DFAE0 File Offset: 0x000DDCE0
		[MonoTODO("handle userData")]
		[CLSCompliant(false)]
		[ComVisible(false)]
		public unsafe NativeOverlapped* Pack(IOCompletionCallback iocb, object userData)
		{
			NativeOverlapped* ptr = (NativeOverlapped*)((void*)Marshal.AllocHGlobal(Marshal.SizeOf(typeof(NativeOverlapped))));
			ptr->OffsetLow = this.offsetL;
			ptr->OffsetHigh = this.offsetH;
			ptr->EventHandle = this.evt_ptr;
			return ptr;
		}

		// Token: 0x060040D9 RID: 16601 RVA: 0x000DFB2C File Offset: 0x000DDD2C
		[CLSCompliant(false)]
		[Obsolete("Use UnsafePack(iocb, userData) instead", false)]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
		public unsafe NativeOverlapped* UnsafePack(IOCompletionCallback iocb)
		{
			return this.Pack(iocb);
		}

		// Token: 0x060040DA RID: 16602 RVA: 0x000DFB38 File Offset: 0x000DDD38
		[ComVisible(false)]
		[CLSCompliant(false)]
		public unsafe NativeOverlapped* UnsafePack(IOCompletionCallback iocb, object userData)
		{
			return this.Pack(iocb, userData);
		}

		// Token: 0x17000C22 RID: 3106
		// (get) Token: 0x060040DB RID: 16603 RVA: 0x000DFB44 File Offset: 0x000DDD44
		// (set) Token: 0x060040DC RID: 16604 RVA: 0x000DFB4C File Offset: 0x000DDD4C
		public IAsyncResult AsyncResult
		{
			get
			{
				return this.ares;
			}
			set
			{
				this.ares = value;
			}
		}

		// Token: 0x17000C23 RID: 3107
		// (get) Token: 0x060040DD RID: 16605 RVA: 0x000DFB58 File Offset: 0x000DDD58
		// (set) Token: 0x060040DE RID: 16606 RVA: 0x000DFB60 File Offset: 0x000DDD60
		[Obsolete("Not 64bit compatible.  Use EventHandleIntPtr instead.", false)]
		public int EventHandle
		{
			get
			{
				return this.evt;
			}
			set
			{
				this.evt = value;
			}
		}

		// Token: 0x17000C24 RID: 3108
		// (get) Token: 0x060040DF RID: 16607 RVA: 0x000DFB6C File Offset: 0x000DDD6C
		// (set) Token: 0x060040E0 RID: 16608 RVA: 0x000DFB74 File Offset: 0x000DDD74
		[ComVisible(false)]
		public IntPtr EventHandleIntPtr
		{
			get
			{
				return this.evt_ptr;
			}
			set
			{
				this.evt_ptr = value;
			}
		}

		// Token: 0x17000C25 RID: 3109
		// (get) Token: 0x060040E1 RID: 16609 RVA: 0x000DFB80 File Offset: 0x000DDD80
		// (set) Token: 0x060040E2 RID: 16610 RVA: 0x000DFB88 File Offset: 0x000DDD88
		public int OffsetHigh
		{
			get
			{
				return this.offsetH;
			}
			set
			{
				this.offsetH = value;
			}
		}

		// Token: 0x17000C26 RID: 3110
		// (get) Token: 0x060040E3 RID: 16611 RVA: 0x000DFB94 File Offset: 0x000DDD94
		// (set) Token: 0x060040E4 RID: 16612 RVA: 0x000DFB9C File Offset: 0x000DDD9C
		public int OffsetLow
		{
			get
			{
				return this.offsetL;
			}
			set
			{
				this.offsetL = value;
			}
		}

		// Token: 0x04001BC2 RID: 7106
		private IAsyncResult ares;

		// Token: 0x04001BC3 RID: 7107
		private int offsetL;

		// Token: 0x04001BC4 RID: 7108
		private int offsetH;

		// Token: 0x04001BC5 RID: 7109
		private int evt;

		// Token: 0x04001BC6 RID: 7110
		private IntPtr evt_ptr;
	}
}
