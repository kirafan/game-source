﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x0200069C RID: 1692
	[ComVisible(false)]
	public enum EventResetMode
	{
		// Token: 0x04001BB0 RID: 7088
		AutoReset,
		// Token: 0x04001BB1 RID: 7089
		ManualReset
	}
}
