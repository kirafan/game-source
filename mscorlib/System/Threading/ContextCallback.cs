﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006F8 RID: 1784
	// (Invoke) Token: 0x060043D4 RID: 17364
	[ComVisible(true)]
	public delegate void ContextCallback(object state);
}
