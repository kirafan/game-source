﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006FA RID: 1786
	// (Invoke) Token: 0x060043DC RID: 17372
	[ComVisible(false)]
	public delegate void ParameterizedThreadStart(object obj);
}
