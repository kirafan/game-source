﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006B6 RID: 1718
	[ComVisible(true)]
	public static class Timeout
	{
		// Token: 0x04001C1D RID: 7197
		public const int Infinite = -1;
	}
}
