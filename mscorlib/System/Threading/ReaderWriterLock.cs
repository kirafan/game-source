﻿using System;
using System.Collections;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006AA RID: 1706
	[ComVisible(true)]
	public sealed class ReaderWriterLock : CriticalFinalizerObject
	{
		// Token: 0x060040E5 RID: 16613 RVA: 0x000DFBA8 File Offset: 0x000DDDA8
		public ReaderWriterLock()
		{
			this.writer_queue = new LockQueue(this);
			this.reader_locks = new Hashtable();
			GC.SuppressFinalize(this);
		}

		// Token: 0x060040E6 RID: 16614 RVA: 0x000DFBE0 File Offset: 0x000DDDE0
		[MonoTODO]
		~ReaderWriterLock()
		{
		}

		// Token: 0x17000C27 RID: 3111
		// (get) Token: 0x060040E7 RID: 16615 RVA: 0x000DFC18 File Offset: 0x000DDE18
		public bool IsReaderLockHeld
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				bool result;
				lock (this)
				{
					result = this.reader_locks.ContainsKey(Thread.CurrentThreadId);
				}
				return result;
			}
		}

		// Token: 0x17000C28 RID: 3112
		// (get) Token: 0x060040E8 RID: 16616 RVA: 0x000DFC74 File Offset: 0x000DDE74
		public bool IsWriterLockHeld
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				bool result;
				lock (this)
				{
					result = (this.state < 0 && Thread.CurrentThreadId == this.writer_lock_owner);
				}
				return result;
			}
		}

		// Token: 0x17000C29 RID: 3113
		// (get) Token: 0x060040E9 RID: 16617 RVA: 0x000DFCD4 File Offset: 0x000DDED4
		public int WriterSeqNum
		{
			get
			{
				int result;
				lock (this)
				{
					result = this.seq_num;
				}
				return result;
			}
		}

		// Token: 0x060040EA RID: 16618 RVA: 0x000DFD20 File Offset: 0x000DDF20
		public void AcquireReaderLock(int millisecondsTimeout)
		{
			this.AcquireReaderLock(millisecondsTimeout, 1);
		}

		// Token: 0x060040EB RID: 16619 RVA: 0x000DFD2C File Offset: 0x000DDF2C
		private void AcquireReaderLock(int millisecondsTimeout, int initialLockCount)
		{
			lock (this)
			{
				if (this.HasWriterLock())
				{
					this.AcquireWriterLock(millisecondsTimeout, initialLockCount);
				}
				else
				{
					object obj = this.reader_locks[Thread.CurrentThreadId];
					if (obj == null)
					{
						this.readers++;
						try
						{
							if (this.state < 0 || !this.writer_queue.IsEmpty)
							{
								while (Monitor.Wait(this, millisecondsTimeout))
								{
									if (this.state >= 0)
									{
										goto IL_89;
									}
								}
								throw new ApplicationException("Timeout expired");
							}
							IL_89:;
						}
						finally
						{
							this.readers--;
						}
						this.reader_locks[Thread.CurrentThreadId] = initialLockCount;
						this.state += initialLockCount;
					}
					else
					{
						this.reader_locks[Thread.CurrentThreadId] = (int)obj + 1;
						this.state++;
					}
				}
			}
		}

		// Token: 0x060040EC RID: 16620 RVA: 0x000DFE74 File Offset: 0x000DE074
		public void AcquireReaderLock(TimeSpan timeout)
		{
			int millisecondsTimeout = this.CheckTimeout(timeout);
			this.AcquireReaderLock(millisecondsTimeout, 1);
		}

		// Token: 0x060040ED RID: 16621 RVA: 0x000DFE94 File Offset: 0x000DE094
		public void AcquireWriterLock(int millisecondsTimeout)
		{
			this.AcquireWriterLock(millisecondsTimeout, 1);
		}

		// Token: 0x060040EE RID: 16622 RVA: 0x000DFEA0 File Offset: 0x000DE0A0
		private void AcquireWriterLock(int millisecondsTimeout, int initialLockCount)
		{
			lock (this)
			{
				if (this.HasWriterLock())
				{
					this.state--;
				}
				else
				{
					if (this.state != 0 || !this.writer_queue.IsEmpty)
					{
						while (this.writer_queue.Wait(millisecondsTimeout))
						{
							if (this.state == 0)
							{
								goto IL_68;
							}
						}
						throw new ApplicationException("Timeout expired");
					}
					IL_68:
					this.state = -initialLockCount;
					this.writer_lock_owner = Thread.CurrentThreadId;
					this.seq_num++;
				}
			}
		}

		// Token: 0x060040EF RID: 16623 RVA: 0x000DFF60 File Offset: 0x000DE160
		public void AcquireWriterLock(TimeSpan timeout)
		{
			int millisecondsTimeout = this.CheckTimeout(timeout);
			this.AcquireWriterLock(millisecondsTimeout, 1);
		}

		// Token: 0x060040F0 RID: 16624 RVA: 0x000DFF80 File Offset: 0x000DE180
		public bool AnyWritersSince(int seqNum)
		{
			bool result;
			lock (this)
			{
				result = (this.seq_num > seqNum);
			}
			return result;
		}

		// Token: 0x060040F1 RID: 16625 RVA: 0x000DFFD0 File Offset: 0x000DE1D0
		public void DowngradeFromWriterLock(ref LockCookie lockCookie)
		{
			lock (this)
			{
				if (!this.HasWriterLock())
				{
					throw new ApplicationException("The thread does not have the writer lock.");
				}
				this.state = lockCookie.ReaderLocks;
				this.reader_locks[Thread.CurrentThreadId] = this.state;
				if (this.readers > 0)
				{
					Monitor.PulseAll(this);
				}
			}
		}

		// Token: 0x060040F2 RID: 16626 RVA: 0x000E0064 File Offset: 0x000DE264
		public LockCookie ReleaseLock()
		{
			LockCookie lockCookie;
			lock (this)
			{
				lockCookie = this.GetLockCookie();
				if (lockCookie.WriterLocks != 0)
				{
					this.ReleaseWriterLock(lockCookie.WriterLocks);
				}
				else if (lockCookie.ReaderLocks != 0)
				{
					this.ReleaseReaderLock(lockCookie.ReaderLocks, lockCookie.ReaderLocks);
				}
			}
			return lockCookie;
		}

		// Token: 0x060040F3 RID: 16627 RVA: 0x000E00E8 File Offset: 0x000DE2E8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void ReleaseReaderLock()
		{
			lock (this)
			{
				if (!this.HasWriterLock())
				{
					if (this.state > 0)
					{
						object obj = this.reader_locks[Thread.CurrentThreadId];
						if (obj != null)
						{
							this.ReleaseReaderLock((int)obj, 1);
							return;
						}
					}
					throw new ApplicationException("The thread does not have any reader or writer locks.");
				}
				this.ReleaseWriterLock();
			}
		}

		// Token: 0x060040F4 RID: 16628 RVA: 0x000E0180 File Offset: 0x000DE380
		private void ReleaseReaderLock(int currentCount, int releaseCount)
		{
			int num = currentCount - releaseCount;
			if (num == 0)
			{
				this.reader_locks.Remove(Thread.CurrentThreadId);
			}
			else
			{
				this.reader_locks[Thread.CurrentThreadId] = num;
			}
			this.state -= releaseCount;
			if (this.state == 0 && !this.writer_queue.IsEmpty)
			{
				this.writer_queue.Pulse();
			}
		}

		// Token: 0x060040F5 RID: 16629 RVA: 0x000E0200 File Offset: 0x000DE400
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void ReleaseWriterLock()
		{
			lock (this)
			{
				if (!this.HasWriterLock())
				{
					throw new ApplicationException("The thread does not have the writer lock.");
				}
				this.ReleaseWriterLock(1);
			}
		}

		// Token: 0x060040F6 RID: 16630 RVA: 0x000E025C File Offset: 0x000DE45C
		private void ReleaseWriterLock(int releaseCount)
		{
			this.state += releaseCount;
			if (this.state == 0)
			{
				if (this.readers > 0)
				{
					Monitor.PulseAll(this);
				}
				else if (!this.writer_queue.IsEmpty)
				{
					this.writer_queue.Pulse();
				}
			}
		}

		// Token: 0x060040F7 RID: 16631 RVA: 0x000E02B4 File Offset: 0x000DE4B4
		public void RestoreLock(ref LockCookie lockCookie)
		{
			lock (this)
			{
				if (lockCookie.WriterLocks != 0)
				{
					this.AcquireWriterLock(-1, lockCookie.WriterLocks);
				}
				else if (lockCookie.ReaderLocks != 0)
				{
					this.AcquireReaderLock(-1, lockCookie.ReaderLocks);
				}
			}
		}

		// Token: 0x060040F8 RID: 16632 RVA: 0x000E0328 File Offset: 0x000DE528
		public LockCookie UpgradeToWriterLock(int millisecondsTimeout)
		{
			LockCookie lockCookie;
			lock (this)
			{
				lockCookie = this.GetLockCookie();
				if (lockCookie.WriterLocks != 0)
				{
					this.state--;
					return lockCookie;
				}
				if (lockCookie.ReaderLocks != 0)
				{
					this.ReleaseReaderLock(lockCookie.ReaderLocks, lockCookie.ReaderLocks);
				}
			}
			this.AcquireWriterLock(millisecondsTimeout);
			return lockCookie;
		}

		// Token: 0x060040F9 RID: 16633 RVA: 0x000E03B8 File Offset: 0x000DE5B8
		public LockCookie UpgradeToWriterLock(TimeSpan timeout)
		{
			int millisecondsTimeout = this.CheckTimeout(timeout);
			return this.UpgradeToWriterLock(millisecondsTimeout);
		}

		// Token: 0x060040FA RID: 16634 RVA: 0x000E03D4 File Offset: 0x000DE5D4
		private LockCookie GetLockCookie()
		{
			LockCookie result = new LockCookie(Thread.CurrentThreadId);
			if (this.HasWriterLock())
			{
				result.WriterLocks = -this.state;
			}
			else
			{
				object obj = this.reader_locks[Thread.CurrentThreadId];
				if (obj != null)
				{
					result.ReaderLocks = (int)obj;
				}
			}
			return result;
		}

		// Token: 0x060040FB RID: 16635 RVA: 0x000E0438 File Offset: 0x000DE638
		private bool HasWriterLock()
		{
			return this.state < 0 && Thread.CurrentThreadId == this.writer_lock_owner;
		}

		// Token: 0x060040FC RID: 16636 RVA: 0x000E0458 File Offset: 0x000DE658
		private int CheckTimeout(TimeSpan timeout)
		{
			int num = (int)timeout.TotalMilliseconds;
			if (num < -1)
			{
				throw new ArgumentOutOfRangeException("timeout", "Number must be either non-negative or -1");
			}
			return num;
		}

		// Token: 0x04001BC7 RID: 7111
		private int seq_num = 1;

		// Token: 0x04001BC8 RID: 7112
		private int state;

		// Token: 0x04001BC9 RID: 7113
		private int readers;

		// Token: 0x04001BCA RID: 7114
		private LockQueue writer_queue;

		// Token: 0x04001BCB RID: 7115
		private Hashtable reader_locks;

		// Token: 0x04001BCC RID: 7116
		private int writer_lock_owner;
	}
}
