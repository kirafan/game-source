﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006B4 RID: 1716
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum ThreadState
	{
		// Token: 0x04001C13 RID: 7187
		Running = 0,
		// Token: 0x04001C14 RID: 7188
		StopRequested = 1,
		// Token: 0x04001C15 RID: 7189
		SuspendRequested = 2,
		// Token: 0x04001C16 RID: 7190
		Background = 4,
		// Token: 0x04001C17 RID: 7191
		Unstarted = 8,
		// Token: 0x04001C18 RID: 7192
		Stopped = 16,
		// Token: 0x04001C19 RID: 7193
		WaitSleepJoin = 32,
		// Token: 0x04001C1A RID: 7194
		Suspended = 64,
		// Token: 0x04001C1B RID: 7195
		AbortRequested = 128,
		// Token: 0x04001C1C RID: 7196
		Aborted = 256
	}
}
