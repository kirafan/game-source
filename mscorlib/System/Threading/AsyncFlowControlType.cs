﻿using System;

namespace System.Threading
{
	// Token: 0x02000698 RID: 1688
	internal enum AsyncFlowControlType
	{
		// Token: 0x04001BA9 RID: 7081
		None,
		// Token: 0x04001BAA RID: 7082
		Execution,
		// Token: 0x04001BAB RID: 7083
		Security
	}
}
