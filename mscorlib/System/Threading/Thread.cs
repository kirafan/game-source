﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Permissions;
using System.Security.Principal;

namespace System.Threading
{
	// Token: 0x020006AE RID: 1710
	[ComDefaultInterface(typeof(_Thread))]
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	public sealed class Thread : CriticalFinalizerObject, _Thread
	{
		// Token: 0x06004112 RID: 16658 RVA: 0x000E07E4 File Offset: 0x000DE9E4
		public Thread(ThreadStart start)
		{
			if (start == null)
			{
				throw new ArgumentNullException("Null ThreadStart");
			}
			this.threadstart = start;
			this.Thread_init();
		}

		// Token: 0x06004113 RID: 16659 RVA: 0x000E0824 File Offset: 0x000DEA24
		public Thread(ThreadStart start, int maxStackSize)
		{
			if (start == null)
			{
				throw new ArgumentNullException("start");
			}
			if (maxStackSize < 131072)
			{
				throw new ArgumentException("< 128 kb", "maxStackSize");
			}
			this.threadstart = start;
			this.stack_size = maxStackSize;
			this.Thread_init();
		}

		// Token: 0x06004114 RID: 16660 RVA: 0x000E0888 File Offset: 0x000DEA88
		public Thread(ParameterizedThreadStart start)
		{
			if (start == null)
			{
				throw new ArgumentNullException("start");
			}
			this.threadstart = start;
			this.Thread_init();
		}

		// Token: 0x06004115 RID: 16661 RVA: 0x000E08C8 File Offset: 0x000DEAC8
		public Thread(ParameterizedThreadStart start, int maxStackSize)
		{
			if (start == null)
			{
				throw new ArgumentNullException("start");
			}
			if (maxStackSize < 131072)
			{
				throw new ArgumentException("< 128 kb", "maxStackSize");
			}
			this.threadstart = start;
			this.stack_size = maxStackSize;
			this.Thread_init();
		}

		// Token: 0x06004117 RID: 16663 RVA: 0x000E0944 File Offset: 0x000DEB44
		void _Thread.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06004118 RID: 16664 RVA: 0x000E094C File Offset: 0x000DEB4C
		void _Thread.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06004119 RID: 16665 RVA: 0x000E0954 File Offset: 0x000DEB54
		void _Thread.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600411A RID: 16666 RVA: 0x000E095C File Offset: 0x000DEB5C
		void _Thread.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000C2B RID: 3115
		// (get) Token: 0x0600411B RID: 16667 RVA: 0x000E0964 File Offset: 0x000DEB64
		public static Context CurrentContext
		{
			[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
			get
			{
				return AppDomain.InternalGetContext();
			}
		}

		// Token: 0x17000C2C RID: 3116
		// (get) Token: 0x0600411C RID: 16668 RVA: 0x000E096C File Offset: 0x000DEB6C
		// (set) Token: 0x0600411D RID: 16669 RVA: 0x000E09CC File Offset: 0x000DEBCC
		public static IPrincipal CurrentPrincipal
		{
			get
			{
				IPrincipal principal = null;
				Thread currentThread = Thread.CurrentThread;
				Thread obj = currentThread;
				lock (obj)
				{
					principal = currentThread._principal;
					if (principal == null)
					{
						principal = Thread.GetDomain().DefaultPrincipal;
					}
				}
				return principal;
			}
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlPrincipal\"/>\n</PermissionSet>\n")]
			set
			{
				Thread currentThread = Thread.CurrentThread;
				Thread obj = currentThread;
				lock (obj)
				{
					currentThread._principal = value;
				}
			}
		}

		// Token: 0x0600411E RID: 16670
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Thread CurrentThread_internal();

		// Token: 0x17000C2D RID: 3117
		// (get) Token: 0x0600411F RID: 16671 RVA: 0x000E0A18 File Offset: 0x000DEC18
		public static Thread CurrentThread
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
			get
			{
				return Thread.CurrentThread_internal();
			}
		}

		// Token: 0x17000C2E RID: 3118
		// (get) Token: 0x06004120 RID: 16672 RVA: 0x000E0A20 File Offset: 0x000DEC20
		internal static int CurrentThreadId
		{
			get
			{
				return (int)Thread.CurrentThread.thread_id;
			}
		}

		// Token: 0x06004121 RID: 16673 RVA: 0x000E0A30 File Offset: 0x000DEC30
		private static void InitDataStoreHash()
		{
			object obj = Thread.datastore_lock;
			lock (obj)
			{
				if (Thread.datastorehash == null)
				{
					Thread.datastorehash = Hashtable.Synchronized(new Hashtable());
				}
			}
		}

		// Token: 0x06004122 RID: 16674 RVA: 0x000E0A8C File Offset: 0x000DEC8C
		public static LocalDataStoreSlot AllocateNamedDataSlot(string name)
		{
			object obj = Thread.datastore_lock;
			LocalDataStoreSlot result;
			lock (obj)
			{
				if (Thread.datastorehash == null)
				{
					Thread.InitDataStoreHash();
				}
				LocalDataStoreSlot localDataStoreSlot = (LocalDataStoreSlot)Thread.datastorehash[name];
				if (localDataStoreSlot != null)
				{
					throw new ArgumentException("Named data slot already added");
				}
				localDataStoreSlot = Thread.AllocateDataSlot();
				Thread.datastorehash.Add(name, localDataStoreSlot);
				result = localDataStoreSlot;
			}
			return result;
		}

		// Token: 0x06004123 RID: 16675 RVA: 0x000E0B18 File Offset: 0x000DED18
		public static void FreeNamedDataSlot(string name)
		{
			object obj = Thread.datastore_lock;
			lock (obj)
			{
				if (Thread.datastorehash != null)
				{
					Thread.datastorehash.Remove(name);
				}
			}
		}

		// Token: 0x06004124 RID: 16676 RVA: 0x000E0B70 File Offset: 0x000DED70
		public static LocalDataStoreSlot AllocateDataSlot()
		{
			return new LocalDataStoreSlot(true);
		}

		// Token: 0x06004125 RID: 16677 RVA: 0x000E0B78 File Offset: 0x000DED78
		public static object GetData(LocalDataStoreSlot slot)
		{
			object[] array = Thread.local_slots;
			if (slot == null)
			{
				throw new ArgumentNullException("slot");
			}
			if (array != null && slot.slot < array.Length)
			{
				return array[slot.slot];
			}
			return null;
		}

		// Token: 0x06004126 RID: 16678 RVA: 0x000E0BBC File Offset: 0x000DEDBC
		public static void SetData(LocalDataStoreSlot slot, object data)
		{
			object[] array = Thread.local_slots;
			if (slot == null)
			{
				throw new ArgumentNullException("slot");
			}
			if (array == null)
			{
				array = new object[slot.slot + 2];
				Thread.local_slots = array;
			}
			else if (slot.slot >= array.Length)
			{
				object[] array2 = new object[slot.slot + 2];
				array.CopyTo(array2, 0);
				array = array2;
				Thread.local_slots = array;
			}
			array[slot.slot] = data;
		}

		// Token: 0x06004127 RID: 16679
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void FreeLocalSlotValues(int slot, bool thread_local);

		// Token: 0x06004128 RID: 16680 RVA: 0x000E0C34 File Offset: 0x000DEE34
		public static LocalDataStoreSlot GetNamedDataSlot(string name)
		{
			object obj = Thread.datastore_lock;
			LocalDataStoreSlot result;
			lock (obj)
			{
				if (Thread.datastorehash == null)
				{
					Thread.InitDataStoreHash();
				}
				LocalDataStoreSlot localDataStoreSlot = (LocalDataStoreSlot)Thread.datastorehash[name];
				if (localDataStoreSlot == null)
				{
					localDataStoreSlot = Thread.AllocateNamedDataSlot(name);
				}
				result = localDataStoreSlot;
			}
			return result;
		}

		// Token: 0x06004129 RID: 16681 RVA: 0x000E0CAC File Offset: 0x000DEEAC
		public static AppDomain GetDomain()
		{
			return AppDomain.CurrentDomain;
		}

		// Token: 0x0600412A RID: 16682
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetDomainID();

		// Token: 0x0600412B RID: 16683
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void ResetAbort_internal();

		// Token: 0x0600412C RID: 16684 RVA: 0x000E0CB4 File Offset: 0x000DEEB4
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlThread\"/>\n</PermissionSet>\n")]
		public static void ResetAbort()
		{
			Thread.ResetAbort_internal();
		}

		// Token: 0x0600412D RID: 16685
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Sleep_internal(int ms);

		// Token: 0x0600412E RID: 16686 RVA: 0x000E0CBC File Offset: 0x000DEEBC
		public static void Sleep(int millisecondsTimeout)
		{
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout", "Negative timeout");
			}
			Thread.Sleep_internal(millisecondsTimeout);
		}

		// Token: 0x0600412F RID: 16687 RVA: 0x000E0CDC File Offset: 0x000DEEDC
		public static void Sleep(TimeSpan timeout)
		{
			long num = (long)timeout.TotalMilliseconds;
			if (num < -1L || num > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("timeout", "timeout out of range");
			}
			Thread.Sleep_internal((int)num);
		}

		// Token: 0x06004130 RID: 16688
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern IntPtr Thread_internal(MulticastDelegate start);

		// Token: 0x06004131 RID: 16689
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Thread_init();

		// Token: 0x17000C2F RID: 3119
		// (get) Token: 0x06004132 RID: 16690 RVA: 0x000E0D20 File Offset: 0x000DEF20
		// (set) Token: 0x06004133 RID: 16691 RVA: 0x000E0D44 File Offset: 0x000DEF44
		[Obsolete("Deprecated in favor of GetApartmentState, SetApartmentState and TrySetApartmentState.")]
		public ApartmentState ApartmentState
		{
			get
			{
				if ((this.ThreadState & ThreadState.Stopped) != ThreadState.Running)
				{
					throw new ThreadStateException("Thread is dead; state can not be accessed.");
				}
				return (ApartmentState)this.apartment_state;
			}
			set
			{
				this.TrySetApartmentState(value);
			}
		}

		// Token: 0x06004134 RID: 16692
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern CultureInfo GetCachedCurrentCulture();

		// Token: 0x06004135 RID: 16693
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern byte[] GetSerializedCurrentCulture();

		// Token: 0x06004136 RID: 16694
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetCachedCurrentCulture(CultureInfo culture);

		// Token: 0x06004137 RID: 16695
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetSerializedCurrentCulture(byte[] culture);

		// Token: 0x06004138 RID: 16696
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern CultureInfo GetCachedCurrentUICulture();

		// Token: 0x06004139 RID: 16697
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern byte[] GetSerializedCurrentUICulture();

		// Token: 0x0600413A RID: 16698
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetCachedCurrentUICulture(CultureInfo culture);

		// Token: 0x0600413B RID: 16699
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetSerializedCurrentUICulture(byte[] culture);

		// Token: 0x17000C30 RID: 3120
		// (get) Token: 0x0600413C RID: 16700 RVA: 0x000E0D50 File Offset: 0x000DEF50
		// (set) Token: 0x0600413D RID: 16701 RVA: 0x000E0E40 File Offset: 0x000DF040
		public CultureInfo CurrentCulture
		{
			get
			{
				if (this.in_currentculture)
				{
					return CultureInfo.InvariantCulture;
				}
				CultureInfo cultureInfo = this.GetCachedCurrentCulture();
				if (cultureInfo != null)
				{
					return cultureInfo;
				}
				byte[] serializedCurrentCulture = this.GetSerializedCurrentCulture();
				if (serializedCurrentCulture == null)
				{
					object obj = Thread.culture_lock;
					lock (obj)
					{
						this.in_currentculture = true;
						cultureInfo = CultureInfo.ConstructCurrentCulture();
						this.SetCachedCurrentCulture(cultureInfo);
						this.in_currentculture = false;
						NumberFormatter.SetThreadCurrentCulture(cultureInfo);
						return cultureInfo;
					}
				}
				this.in_currentculture = true;
				try
				{
					BinaryFormatter binaryFormatter = new BinaryFormatter();
					MemoryStream serializationStream = new MemoryStream(serializedCurrentCulture);
					cultureInfo = (CultureInfo)binaryFormatter.Deserialize(serializationStream);
					this.SetCachedCurrentCulture(cultureInfo);
				}
				finally
				{
					this.in_currentculture = false;
				}
				NumberFormatter.SetThreadCurrentCulture(cultureInfo);
				return cultureInfo;
			}
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlThread\"/>\n</PermissionSet>\n")]
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				CultureInfo cachedCurrentCulture = this.GetCachedCurrentCulture();
				if (cachedCurrentCulture == value)
				{
					return;
				}
				value.CheckNeutral();
				this.in_currentculture = true;
				try
				{
					this.SetCachedCurrentCulture(value);
					byte[] array;
					if (value.IsReadOnly && value.cached_serialized_form != null)
					{
						array = value.cached_serialized_form;
					}
					else
					{
						BinaryFormatter binaryFormatter = new BinaryFormatter();
						MemoryStream memoryStream = new MemoryStream();
						binaryFormatter.Serialize(memoryStream, value);
						array = memoryStream.GetBuffer();
						if (value.IsReadOnly)
						{
							value.cached_serialized_form = array;
						}
					}
					this.SetSerializedCurrentCulture(array);
				}
				finally
				{
					this.in_currentculture = false;
				}
				NumberFormatter.SetThreadCurrentCulture(value);
			}
		}

		// Token: 0x17000C31 RID: 3121
		// (get) Token: 0x0600413E RID: 16702 RVA: 0x000E0F08 File Offset: 0x000DF108
		// (set) Token: 0x0600413F RID: 16703 RVA: 0x000E0FEC File Offset: 0x000DF1EC
		public CultureInfo CurrentUICulture
		{
			get
			{
				if (this.in_currentculture)
				{
					return CultureInfo.InvariantCulture;
				}
				CultureInfo cultureInfo = this.GetCachedCurrentUICulture();
				if (cultureInfo != null)
				{
					return cultureInfo;
				}
				byte[] serializedCurrentUICulture = this.GetSerializedCurrentUICulture();
				if (serializedCurrentUICulture == null)
				{
					object obj = Thread.culture_lock;
					lock (obj)
					{
						this.in_currentculture = true;
						cultureInfo = CultureInfo.ConstructCurrentUICulture();
						this.SetCachedCurrentUICulture(cultureInfo);
						this.in_currentculture = false;
						return cultureInfo;
					}
				}
				this.in_currentculture = true;
				try
				{
					BinaryFormatter binaryFormatter = new BinaryFormatter();
					MemoryStream serializationStream = new MemoryStream(serializedCurrentUICulture);
					cultureInfo = (CultureInfo)binaryFormatter.Deserialize(serializationStream);
					this.SetCachedCurrentUICulture(cultureInfo);
				}
				finally
				{
					this.in_currentculture = false;
				}
				return cultureInfo;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				CultureInfo cachedCurrentUICulture = this.GetCachedCurrentUICulture();
				if (cachedCurrentUICulture == value)
				{
					return;
				}
				this.in_currentculture = true;
				try
				{
					this.SetCachedCurrentUICulture(value);
					byte[] array;
					if (value.IsReadOnly && value.cached_serialized_form != null)
					{
						array = value.cached_serialized_form;
					}
					else
					{
						BinaryFormatter binaryFormatter = new BinaryFormatter();
						MemoryStream memoryStream = new MemoryStream();
						binaryFormatter.Serialize(memoryStream, value);
						array = memoryStream.GetBuffer();
						if (value.IsReadOnly)
						{
							value.cached_serialized_form = array;
						}
					}
					this.SetSerializedCurrentUICulture(array);
				}
				finally
				{
					this.in_currentculture = false;
				}
			}
		}

		// Token: 0x17000C32 RID: 3122
		// (get) Token: 0x06004140 RID: 16704 RVA: 0x000E10A8 File Offset: 0x000DF2A8
		public bool IsThreadPoolThread
		{
			get
			{
				return this.IsThreadPoolThreadInternal;
			}
		}

		// Token: 0x17000C33 RID: 3123
		// (get) Token: 0x06004141 RID: 16705 RVA: 0x000E10B0 File Offset: 0x000DF2B0
		// (set) Token: 0x06004142 RID: 16706 RVA: 0x000E10B8 File Offset: 0x000DF2B8
		internal bool IsThreadPoolThreadInternal
		{
			get
			{
				return this.threadpool_thread;
			}
			set
			{
				this.threadpool_thread = value;
			}
		}

		// Token: 0x17000C34 RID: 3124
		// (get) Token: 0x06004143 RID: 16707 RVA: 0x000E10C4 File Offset: 0x000DF2C4
		public bool IsAlive
		{
			get
			{
				ThreadState threadState = this.GetState();
				return (threadState & ThreadState.Aborted) == ThreadState.Running && (threadState & ThreadState.Stopped) == ThreadState.Running && (threadState & ThreadState.Unstarted) == ThreadState.Running;
			}
		}

		// Token: 0x17000C35 RID: 3125
		// (get) Token: 0x06004144 RID: 16708 RVA: 0x000E10F8 File Offset: 0x000DF2F8
		// (set) Token: 0x06004145 RID: 16709 RVA: 0x000E112C File Offset: 0x000DF32C
		public bool IsBackground
		{
			get
			{
				ThreadState threadState = this.GetState();
				if ((threadState & ThreadState.Stopped) != ThreadState.Running)
				{
					throw new ThreadStateException("Thread is dead; state can not be accessed.");
				}
				return (threadState & ThreadState.Background) != ThreadState.Running;
			}
			set
			{
				if (value)
				{
					this.SetState(ThreadState.Background);
				}
				else
				{
					this.ClrState(ThreadState.Background);
				}
			}
		}

		// Token: 0x06004146 RID: 16710
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string GetName_internal();

		// Token: 0x06004147 RID: 16711
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetName_internal(string name);

		// Token: 0x17000C36 RID: 3126
		// (get) Token: 0x06004148 RID: 16712 RVA: 0x000E1148 File Offset: 0x000DF348
		// (set) Token: 0x06004149 RID: 16713 RVA: 0x000E1150 File Offset: 0x000DF350
		public string Name
		{
			get
			{
				return this.GetName_internal();
			}
			set
			{
				this.SetName_internal(value);
			}
		}

		// Token: 0x17000C37 RID: 3127
		// (get) Token: 0x0600414A RID: 16714 RVA: 0x000E115C File Offset: 0x000DF35C
		// (set) Token: 0x0600414B RID: 16715 RVA: 0x000E1160 File Offset: 0x000DF360
		public ThreadPriority Priority
		{
			get
			{
				return ThreadPriority.Lowest;
			}
			set
			{
			}
		}

		// Token: 0x17000C38 RID: 3128
		// (get) Token: 0x0600414C RID: 16716 RVA: 0x000E1164 File Offset: 0x000DF364
		public ThreadState ThreadState
		{
			get
			{
				return this.GetState();
			}
		}

		// Token: 0x0600414D RID: 16717
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Abort_internal(object stateInfo);

		// Token: 0x0600414E RID: 16718 RVA: 0x000E116C File Offset: 0x000DF36C
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlThread\"/>\n</PermissionSet>\n")]
		public void Abort()
		{
			this.Abort_internal(null);
		}

		// Token: 0x0600414F RID: 16719 RVA: 0x000E1178 File Offset: 0x000DF378
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlThread\"/>\n</PermissionSet>\n")]
		public void Abort(object stateInfo)
		{
			this.Abort_internal(stateInfo);
		}

		// Token: 0x06004150 RID: 16720
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern object GetAbortExceptionState();

		// Token: 0x06004151 RID: 16721
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Interrupt_internal();

		// Token: 0x06004152 RID: 16722 RVA: 0x000E1184 File Offset: 0x000DF384
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlThread\"/>\n</PermissionSet>\n")]
		public void Interrupt()
		{
			this.Interrupt_internal();
		}

		// Token: 0x06004153 RID: 16723
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool Join_internal(int ms, IntPtr handle);

		// Token: 0x06004154 RID: 16724 RVA: 0x000E118C File Offset: 0x000DF38C
		public void Join()
		{
			this.Join_internal(-1, this.system_thread_handle);
		}

		// Token: 0x06004155 RID: 16725 RVA: 0x000E119C File Offset: 0x000DF39C
		public bool Join(int millisecondsTimeout)
		{
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout", "Timeout less than zero");
			}
			return this.Join_internal(millisecondsTimeout, this.system_thread_handle);
		}

		// Token: 0x06004156 RID: 16726 RVA: 0x000E11D0 File Offset: 0x000DF3D0
		public bool Join(TimeSpan timeout)
		{
			long num = (long)timeout.TotalMilliseconds;
			if (num < -1L || num > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("timeout", "timeout out of range");
			}
			return this.Join_internal((int)num, this.system_thread_handle);
		}

		// Token: 0x06004157 RID: 16727
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void MemoryBarrier();

		// Token: 0x06004158 RID: 16728
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Resume_internal();

		// Token: 0x06004159 RID: 16729 RVA: 0x000E1218 File Offset: 0x000DF418
		[Obsolete("")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlThread\"/>\n</PermissionSet>\n")]
		public void Resume()
		{
			this.Resume_internal();
		}

		// Token: 0x0600415A RID: 16730
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void SpinWait_nop();

		// Token: 0x0600415B RID: 16731 RVA: 0x000E1220 File Offset: 0x000DF420
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static void SpinWait(int iterations)
		{
			if (iterations < 0)
			{
				return;
			}
			while (iterations-- > 0)
			{
				Thread.SpinWait_nop();
			}
		}

		// Token: 0x0600415C RID: 16732 RVA: 0x000E1240 File Offset: 0x000DF440
		public void Start()
		{
			if (!ExecutionContext.IsFlowSuppressed())
			{
				this.ec_to_set = ExecutionContext.Capture();
			}
			if (Thread.CurrentThread._principal != null)
			{
				this._principal = Thread.CurrentThread._principal;
			}
			if (this.Thread_internal(this.threadstart) == (IntPtr)0)
			{
				throw new SystemException("Thread creation failed.");
			}
		}

		// Token: 0x0600415D RID: 16733
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Suspend_internal();

		// Token: 0x0600415E RID: 16734 RVA: 0x000E12A8 File Offset: 0x000DF4A8
		[Obsolete("")]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlThread\"/>\n</PermissionSet>\n")]
		public void Suspend()
		{
			this.Suspend_internal();
		}

		// Token: 0x0600415F RID: 16735
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void Thread_free_internal(IntPtr handle);

		// Token: 0x06004160 RID: 16736 RVA: 0x000E12B0 File Offset: 0x000DF4B0
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		~Thread()
		{
			this.Thread_free_internal(this.system_thread_handle);
		}

		// Token: 0x06004161 RID: 16737
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void SetState(ThreadState set);

		// Token: 0x06004162 RID: 16738
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void ClrState(ThreadState clr);

		// Token: 0x06004163 RID: 16739
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern ThreadState GetState();

		// Token: 0x06004164 RID: 16740
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern byte VolatileRead(ref byte address);

		// Token: 0x06004165 RID: 16741
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern double VolatileRead(ref double address);

		// Token: 0x06004166 RID: 16742
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern short VolatileRead(ref short address);

		// Token: 0x06004167 RID: 16743
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int VolatileRead(ref int address);

		// Token: 0x06004168 RID: 16744
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long VolatileRead(ref long address);

		// Token: 0x06004169 RID: 16745
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr VolatileRead(ref IntPtr address);

		// Token: 0x0600416A RID: 16746
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern object VolatileRead(ref object address);

		// Token: 0x0600416B RID: 16747
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern sbyte VolatileRead(ref sbyte address);

		// Token: 0x0600416C RID: 16748
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern float VolatileRead(ref float address);

		// Token: 0x0600416D RID: 16749
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern ushort VolatileRead(ref ushort address);

		// Token: 0x0600416E RID: 16750
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern uint VolatileRead(ref uint address);

		// Token: 0x0600416F RID: 16751
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern ulong VolatileRead(ref ulong address);

		// Token: 0x06004170 RID: 16752
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern UIntPtr VolatileRead(ref UIntPtr address);

		// Token: 0x06004171 RID: 16753
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref byte address, byte value);

		// Token: 0x06004172 RID: 16754
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref double address, double value);

		// Token: 0x06004173 RID: 16755
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref short address, short value);

		// Token: 0x06004174 RID: 16756
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref int address, int value);

		// Token: 0x06004175 RID: 16757
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref long address, long value);

		// Token: 0x06004176 RID: 16758
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref IntPtr address, IntPtr value);

		// Token: 0x06004177 RID: 16759
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref object address, object value);

		// Token: 0x06004178 RID: 16760
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref sbyte address, sbyte value);

		// Token: 0x06004179 RID: 16761
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref float address, float value);

		// Token: 0x0600417A RID: 16762
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref ushort address, ushort value);

		// Token: 0x0600417B RID: 16763
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref uint address, uint value);

		// Token: 0x0600417C RID: 16764
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref ulong address, ulong value);

		// Token: 0x0600417D RID: 16765
		[CLSCompliant(false)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void VolatileWrite(ref UIntPtr address, UIntPtr value);

		// Token: 0x0600417E RID: 16766 RVA: 0x000E12F4 File Offset: 0x000DF4F4
		private static int GetNewManagedId()
		{
			return Thread.GetNewManagedId_internal();
		}

		// Token: 0x0600417F RID: 16767
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetNewManagedId_internal();

		// Token: 0x17000C39 RID: 3129
		// (get) Token: 0x06004180 RID: 16768 RVA: 0x000E12FC File Offset: 0x000DF4FC
		[MonoTODO("limited to CompressedStack support")]
		public ExecutionContext ExecutionContext
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
			get
			{
				if (Thread._ec == null)
				{
					Thread._ec = new ExecutionContext();
				}
				return Thread._ec;
			}
		}

		// Token: 0x17000C3A RID: 3130
		// (get) Token: 0x06004181 RID: 16769 RVA: 0x000E1318 File Offset: 0x000DF518
		public int ManagedThreadId
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				if (this.managed_id == 0)
				{
					int newManagedId = Thread.GetNewManagedId();
					Interlocked.CompareExchange(ref this.managed_id, newManagedId, 0);
				}
				return this.managed_id;
			}
		}

		// Token: 0x06004182 RID: 16770 RVA: 0x000E134C File Offset: 0x000DF54C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static void BeginCriticalRegion()
		{
			Thread.CurrentThread.critical_region_level++;
		}

		// Token: 0x06004183 RID: 16771 RVA: 0x000E1364 File Offset: 0x000DF564
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static void EndCriticalRegion()
		{
			Thread.CurrentThread.critical_region_level--;
		}

		// Token: 0x06004184 RID: 16772 RVA: 0x000E137C File Offset: 0x000DF57C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static void BeginThreadAffinity()
		{
		}

		// Token: 0x06004185 RID: 16773 RVA: 0x000E1380 File Offset: 0x000DF580
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static void EndThreadAffinity()
		{
		}

		// Token: 0x06004186 RID: 16774 RVA: 0x000E1384 File Offset: 0x000DF584
		public ApartmentState GetApartmentState()
		{
			return (ApartmentState)this.apartment_state;
		}

		// Token: 0x06004187 RID: 16775 RVA: 0x000E138C File Offset: 0x000DF58C
		public void SetApartmentState(ApartmentState state)
		{
			if (!this.TrySetApartmentState(state))
			{
				throw new InvalidOperationException("Failed to set the specified COM apartment state.");
			}
		}

		// Token: 0x06004188 RID: 16776 RVA: 0x000E13A8 File Offset: 0x000DF5A8
		public bool TrySetApartmentState(ApartmentState state)
		{
			if (this != Thread.CurrentThread && (this.ThreadState & ThreadState.Unstarted) == ThreadState.Running)
			{
				throw new ThreadStateException("Thread was in an invalid state for the operation being executed.");
			}
			if (this.apartment_state != 2)
			{
				return false;
			}
			this.apartment_state = (byte)state;
			return true;
		}

		// Token: 0x06004189 RID: 16777 RVA: 0x000E13F0 File Offset: 0x000DF5F0
		[ComVisible(false)]
		public override int GetHashCode()
		{
			return this.ManagedThreadId;
		}

		// Token: 0x0600418A RID: 16778 RVA: 0x000E13F8 File Offset: 0x000DF5F8
		public void Start(object parameter)
		{
			this.start_obj = parameter;
			this.Start();
		}

		// Token: 0x0600418B RID: 16779 RVA: 0x000E1408 File Offset: 0x000DF608
		[Obsolete("see CompressedStack class")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n   <IPermission class=\"System.Security.Permissions.StrongNameIdentityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                PublicKeyBlob=\"00000000000000000400000000000000\"/>\n</PermissionSet>\n")]
		public CompressedStack GetCompressedStack()
		{
			CompressedStack compressedStack = this.ExecutionContext.SecurityContext.CompressedStack;
			return (compressedStack != null && !compressedStack.IsEmpty()) ? compressedStack.CreateCopy() : null;
		}

		// Token: 0x0600418C RID: 16780 RVA: 0x000E1444 File Offset: 0x000DF644
		[Obsolete("see CompressedStack class")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n   <IPermission class=\"System.Security.Permissions.StrongNameIdentityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                PublicKeyBlob=\"00000000000000000400000000000000\"/>\n</PermissionSet>\n")]
		public void SetCompressedStack(CompressedStack stack)
		{
			this.ExecutionContext.SecurityContext.CompressedStack = stack;
		}

		// Token: 0x04001BD8 RID: 7128
		private int lock_thread_id;

		// Token: 0x04001BD9 RID: 7129
		private IntPtr system_thread_handle;

		// Token: 0x04001BDA RID: 7130
		private object cached_culture_info;

		// Token: 0x04001BDB RID: 7131
		private IntPtr unused0;

		// Token: 0x04001BDC RID: 7132
		private bool threadpool_thread;

		// Token: 0x04001BDD RID: 7133
		private IntPtr name;

		// Token: 0x04001BDE RID: 7134
		private int name_len;

		// Token: 0x04001BDF RID: 7135
		private ThreadState state = ThreadState.Unstarted;

		// Token: 0x04001BE0 RID: 7136
		private object abort_exc;

		// Token: 0x04001BE1 RID: 7137
		private int abort_state_handle;

		// Token: 0x04001BE2 RID: 7138
		private long thread_id;

		// Token: 0x04001BE3 RID: 7139
		private IntPtr start_notify;

		// Token: 0x04001BE4 RID: 7140
		private IntPtr stack_ptr;

		// Token: 0x04001BE5 RID: 7141
		private UIntPtr static_data;

		// Token: 0x04001BE6 RID: 7142
		private IntPtr jit_data;

		// Token: 0x04001BE7 RID: 7143
		private IntPtr lock_data;

		// Token: 0x04001BE8 RID: 7144
		private object current_appcontext;

		// Token: 0x04001BE9 RID: 7145
		private int stack_size;

		// Token: 0x04001BEA RID: 7146
		private object start_obj;

		// Token: 0x04001BEB RID: 7147
		private IntPtr appdomain_refs;

		// Token: 0x04001BEC RID: 7148
		private int interruption_requested;

		// Token: 0x04001BED RID: 7149
		private IntPtr suspend_event;

		// Token: 0x04001BEE RID: 7150
		private IntPtr suspended_event;

		// Token: 0x04001BEF RID: 7151
		private IntPtr resume_event;

		// Token: 0x04001BF0 RID: 7152
		private IntPtr synch_cs;

		// Token: 0x04001BF1 RID: 7153
		private IntPtr serialized_culture_info;

		// Token: 0x04001BF2 RID: 7154
		private int serialized_culture_info_len;

		// Token: 0x04001BF3 RID: 7155
		private IntPtr serialized_ui_culture_info;

		// Token: 0x04001BF4 RID: 7156
		private int serialized_ui_culture_info_len;

		// Token: 0x04001BF5 RID: 7157
		private bool thread_dump_requested;

		// Token: 0x04001BF6 RID: 7158
		private IntPtr end_stack;

		// Token: 0x04001BF7 RID: 7159
		private bool thread_interrupt_requested;

		// Token: 0x04001BF8 RID: 7160
		private byte apartment_state = 2;

		// Token: 0x04001BF9 RID: 7161
		private volatile int critical_region_level;

		// Token: 0x04001BFA RID: 7162
		private int small_id;

		// Token: 0x04001BFB RID: 7163
		private IntPtr manage_callback;

		// Token: 0x04001BFC RID: 7164
		private object pending_exception;

		// Token: 0x04001BFD RID: 7165
		private ExecutionContext ec_to_set;

		// Token: 0x04001BFE RID: 7166
		private IntPtr interrupt_on_stop;

		// Token: 0x04001BFF RID: 7167
		private IntPtr unused3;

		// Token: 0x04001C00 RID: 7168
		private IntPtr unused4;

		// Token: 0x04001C01 RID: 7169
		private IntPtr unused5;

		// Token: 0x04001C02 RID: 7170
		private IntPtr unused6;

		// Token: 0x04001C03 RID: 7171
		[ThreadStatic]
		private static object[] local_slots;

		// Token: 0x04001C04 RID: 7172
		[ThreadStatic]
		private static ExecutionContext _ec;

		// Token: 0x04001C05 RID: 7173
		private MulticastDelegate threadstart;

		// Token: 0x04001C06 RID: 7174
		private int managed_id;

		// Token: 0x04001C07 RID: 7175
		private IPrincipal _principal;

		// Token: 0x04001C08 RID: 7176
		private static Hashtable datastorehash;

		// Token: 0x04001C09 RID: 7177
		private static object datastore_lock = new object();

		// Token: 0x04001C0A RID: 7178
		private bool in_currentculture;

		// Token: 0x04001C0B RID: 7179
		private static object culture_lock = new object();
	}
}
