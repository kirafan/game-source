﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006B2 RID: 1714
	[ComVisible(true)]
	[Serializable]
	public enum ThreadPriority
	{
		// Token: 0x04001C0D RID: 7181
		Lowest,
		// Token: 0x04001C0E RID: 7182
		BelowNormal,
		// Token: 0x04001C0F RID: 7183
		Normal,
		// Token: 0x04001C10 RID: 7184
		AboveNormal,
		// Token: 0x04001C11 RID: 7185
		Highest
	}
}
