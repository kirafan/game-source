﻿using System;

namespace System.Threading
{
	// Token: 0x0200069F RID: 1695
	[MonoTODO("Useless until the runtime supports it")]
	public class HostExecutionContext
	{
		// Token: 0x06004082 RID: 16514 RVA: 0x000DF2E4 File Offset: 0x000DD4E4
		public HostExecutionContext()
		{
			this._state = null;
		}

		// Token: 0x06004083 RID: 16515 RVA: 0x000DF2F4 File Offset: 0x000DD4F4
		public HostExecutionContext(object state)
		{
			this._state = state;
		}

		// Token: 0x06004084 RID: 16516 RVA: 0x000DF304 File Offset: 0x000DD504
		public virtual HostExecutionContext CreateCopy()
		{
			return new HostExecutionContext(this._state);
		}

		// Token: 0x17000C20 RID: 3104
		// (get) Token: 0x06004085 RID: 16517 RVA: 0x000DF314 File Offset: 0x000DD514
		// (set) Token: 0x06004086 RID: 16518 RVA: 0x000DF31C File Offset: 0x000DD51C
		protected internal object State
		{
			get
			{
				return this._state;
			}
			set
			{
				this._state = value;
			}
		}

		// Token: 0x04001BB5 RID: 7093
		private object _state;
	}
}
