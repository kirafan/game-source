﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Threading
{
	// Token: 0x020006AD RID: 1709
	[ComVisible(true)]
	[Serializable]
	public class SynchronizationLockException : SystemException
	{
		// Token: 0x0600410E RID: 16654 RVA: 0x000E07B0 File Offset: 0x000DE9B0
		public SynchronizationLockException() : base("Synchronization Error")
		{
		}

		// Token: 0x0600410F RID: 16655 RVA: 0x000E07C0 File Offset: 0x000DE9C0
		public SynchronizationLockException(string message) : base(message)
		{
		}

		// Token: 0x06004110 RID: 16656 RVA: 0x000E07CC File Offset: 0x000DE9CC
		protected SynchronizationLockException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06004111 RID: 16657 RVA: 0x000E07D8 File Offset: 0x000DE9D8
		public SynchronizationLockException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
