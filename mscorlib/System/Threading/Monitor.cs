﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;

namespace System.Threading
{
	// Token: 0x020006A5 RID: 1701
	[ComVisible(true)]
	public static class Monitor
	{
		// Token: 0x060040AD RID: 16557
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Monitor_try_enter(object obj, int ms);

		// Token: 0x060040AE RID: 16558
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Enter(object obj);

		// Token: 0x060040AF RID: 16559
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Exit(object obj);

		// Token: 0x060040B0 RID: 16560
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Monitor_pulse(object obj);

		// Token: 0x060040B1 RID: 16561
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Monitor_test_synchronised(object obj);

		// Token: 0x060040B2 RID: 16562 RVA: 0x000DF5A0 File Offset: 0x000DD7A0
		public static void Pulse(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (!Monitor.Monitor_test_synchronised(obj))
			{
				throw new SynchronizationLockException("Object is not synchronized");
			}
			Monitor.Monitor_pulse(obj);
		}

		// Token: 0x060040B3 RID: 16563
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void Monitor_pulse_all(object obj);

		// Token: 0x060040B4 RID: 16564 RVA: 0x000DF5D0 File Offset: 0x000DD7D0
		public static void PulseAll(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (!Monitor.Monitor_test_synchronised(obj))
			{
				throw new SynchronizationLockException("Object is not synchronized");
			}
			Monitor.Monitor_pulse_all(obj);
		}

		// Token: 0x060040B5 RID: 16565 RVA: 0x000DF600 File Offset: 0x000DD800
		public static bool TryEnter(object obj)
		{
			return Monitor.TryEnter(obj, 0);
		}

		// Token: 0x060040B6 RID: 16566 RVA: 0x000DF60C File Offset: 0x000DD80C
		public static bool TryEnter(object obj, int millisecondsTimeout)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (millisecondsTimeout == -1)
			{
				Monitor.Enter(obj);
				return true;
			}
			if (millisecondsTimeout < 0)
			{
				throw new ArgumentException("negative value for millisecondsTimeout", "millisecondsTimeout");
			}
			return Monitor.Monitor_try_enter(obj, millisecondsTimeout);
		}

		// Token: 0x060040B7 RID: 16567 RVA: 0x000DF658 File Offset: 0x000DD858
		public static bool TryEnter(object obj, TimeSpan timeout)
		{
			long num = (long)timeout.TotalMilliseconds;
			if (num < -1L || num > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("timeout", "timeout out of range");
			}
			return Monitor.TryEnter(obj, (int)num);
		}

		// Token: 0x060040B8 RID: 16568
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool Monitor_wait(object obj, int ms);

		// Token: 0x060040B9 RID: 16569 RVA: 0x000DF69C File Offset: 0x000DD89C
		public static bool Wait(object obj)
		{
			return Monitor.Wait(obj, -1);
		}

		// Token: 0x060040BA RID: 16570 RVA: 0x000DF6A8 File Offset: 0x000DD8A8
		public static bool Wait(object obj, int millisecondsTimeout)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout", "timeout out of range");
			}
			if (!Monitor.Monitor_test_synchronised(obj))
			{
				throw new SynchronizationLockException("Object is not synchronized");
			}
			return Monitor.Monitor_wait(obj, millisecondsTimeout);
		}

		// Token: 0x060040BB RID: 16571 RVA: 0x000DF6FC File Offset: 0x000DD8FC
		public static bool Wait(object obj, TimeSpan timeout)
		{
			long num = (long)timeout.TotalMilliseconds;
			if (num < -1L || num > 2147483647L)
			{
				throw new ArgumentOutOfRangeException("timeout", "timeout out of range");
			}
			return Monitor.Wait(obj, (int)num);
		}

		// Token: 0x060040BC RID: 16572 RVA: 0x000DF740 File Offset: 0x000DD940
		public static bool Wait(object obj, int millisecondsTimeout, bool exitContext)
		{
			bool result;
			try
			{
				if (exitContext)
				{
					SynchronizationAttribute.ExitContext();
				}
				result = Monitor.Wait(obj, millisecondsTimeout);
			}
			finally
			{
				if (exitContext)
				{
					SynchronizationAttribute.EnterContext();
				}
			}
			return result;
		}

		// Token: 0x060040BD RID: 16573 RVA: 0x000DF794 File Offset: 0x000DD994
		public static bool Wait(object obj, TimeSpan timeout, bool exitContext)
		{
			bool result;
			try
			{
				if (exitContext)
				{
					SynchronizationAttribute.ExitContext();
				}
				result = Monitor.Wait(obj, timeout);
			}
			finally
			{
				if (exitContext)
				{
					SynchronizationAttribute.EnterContext();
				}
			}
			return result;
		}
	}
}
