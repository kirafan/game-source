﻿using System;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;

namespace System.Threading
{
	// Token: 0x0200069E RID: 1694
	[Serializable]
	public sealed class ExecutionContext : ISerializable
	{
		// Token: 0x06004074 RID: 16500 RVA: 0x000DF148 File Offset: 0x000DD348
		internal ExecutionContext()
		{
		}

		// Token: 0x06004075 RID: 16501 RVA: 0x000DF150 File Offset: 0x000DD350
		internal ExecutionContext(ExecutionContext ec)
		{
			if (ec._sc != null)
			{
				this._sc = new SecurityContext(ec._sc);
			}
			this._suppressFlow = ec._suppressFlow;
			this._capture = true;
		}

		// Token: 0x06004076 RID: 16502 RVA: 0x000DF188 File Offset: 0x000DD388
		[MonoTODO]
		internal ExecutionContext(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06004077 RID: 16503 RVA: 0x000DF198 File Offset: 0x000DD398
		public static ExecutionContext Capture()
		{
			ExecutionContext executionContext = Thread.CurrentThread.ExecutionContext;
			if (executionContext.FlowSuppressed)
			{
				return null;
			}
			ExecutionContext executionContext2 = new ExecutionContext(executionContext);
			if (SecurityManager.SecurityEnabled)
			{
				executionContext2.SecurityContext = SecurityContext.Capture();
			}
			return executionContext2;
		}

		// Token: 0x06004078 RID: 16504 RVA: 0x000DF1DC File Offset: 0x000DD3DC
		public ExecutionContext CreateCopy()
		{
			if (!this._capture)
			{
				throw new InvalidOperationException();
			}
			return new ExecutionContext(this);
		}

		// Token: 0x06004079 RID: 16505 RVA: 0x000DF1F8 File Offset: 0x000DD3F8
		[MonoTODO]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.ReflectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"MemberAccess\"/>\n</PermissionSet>\n")]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			throw new NotImplementedException();
		}

		// Token: 0x17000C1E RID: 3102
		// (get) Token: 0x0600407A RID: 16506 RVA: 0x000DF210 File Offset: 0x000DD410
		// (set) Token: 0x0600407B RID: 16507 RVA: 0x000DF230 File Offset: 0x000DD430
		internal SecurityContext SecurityContext
		{
			get
			{
				if (this._sc == null)
				{
					this._sc = new SecurityContext();
				}
				return this._sc;
			}
			set
			{
				this._sc = value;
			}
		}

		// Token: 0x17000C1F RID: 3103
		// (get) Token: 0x0600407C RID: 16508 RVA: 0x000DF23C File Offset: 0x000DD43C
		// (set) Token: 0x0600407D RID: 16509 RVA: 0x000DF244 File Offset: 0x000DD444
		internal bool FlowSuppressed
		{
			get
			{
				return this._suppressFlow;
			}
			set
			{
				this._suppressFlow = value;
			}
		}

		// Token: 0x0600407E RID: 16510 RVA: 0x000DF250 File Offset: 0x000DD450
		public static bool IsFlowSuppressed()
		{
			return Thread.CurrentThread.ExecutionContext.FlowSuppressed;
		}

		// Token: 0x0600407F RID: 16511 RVA: 0x000DF264 File Offset: 0x000DD464
		public static void RestoreFlow()
		{
			ExecutionContext executionContext = Thread.CurrentThread.ExecutionContext;
			if (!executionContext.FlowSuppressed)
			{
				throw new InvalidOperationException();
			}
			executionContext.FlowSuppressed = false;
		}

		// Token: 0x06004080 RID: 16512 RVA: 0x000DF294 File Offset: 0x000DD494
		[MonoTODO("only the SecurityContext is considered")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"Infrastructure\"/>\n</PermissionSet>\n")]
		public static void Run(ExecutionContext executionContext, ContextCallback callback, object state)
		{
			if (executionContext == null)
			{
				throw new InvalidOperationException(Locale.GetText("Null ExecutionContext"));
			}
			SecurityContext.Run(executionContext.SecurityContext, callback, state);
		}

		// Token: 0x06004081 RID: 16513 RVA: 0x000DF2BC File Offset: 0x000DD4BC
		public static AsyncFlowControl SuppressFlow()
		{
			Thread currentThread = Thread.CurrentThread;
			currentThread.ExecutionContext.FlowSuppressed = true;
			return new AsyncFlowControl(currentThread, AsyncFlowControlType.Execution);
		}

		// Token: 0x04001BB2 RID: 7090
		private SecurityContext _sc;

		// Token: 0x04001BB3 RID: 7091
		private bool _suppressFlow;

		// Token: 0x04001BB4 RID: 7092
		private bool _capture;
	}
}
