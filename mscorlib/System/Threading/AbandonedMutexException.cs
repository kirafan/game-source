﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Threading
{
	// Token: 0x02000696 RID: 1686
	[ComVisible(false)]
	[Serializable]
	public class AbandonedMutexException : SystemException
	{
		// Token: 0x0600404C RID: 16460 RVA: 0x000DEB14 File Offset: 0x000DCD14
		public AbandonedMutexException() : base("Mutex was abandoned")
		{
		}

		// Token: 0x0600404D RID: 16461 RVA: 0x000DEB28 File Offset: 0x000DCD28
		public AbandonedMutexException(string message) : base(message)
		{
		}

		// Token: 0x0600404E RID: 16462 RVA: 0x000DEB38 File Offset: 0x000DCD38
		public AbandonedMutexException(int location, WaitHandle handle) : base("Mutex was abandoned")
		{
			this.mutex_index = location;
			this.mutex = (handle as Mutex);
		}

		// Token: 0x0600404F RID: 16463 RVA: 0x000DEB60 File Offset: 0x000DCD60
		protected AbandonedMutexException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06004050 RID: 16464 RVA: 0x000DEB74 File Offset: 0x000DCD74
		public AbandonedMutexException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06004051 RID: 16465 RVA: 0x000DEB88 File Offset: 0x000DCD88
		public AbandonedMutexException(string message, int location, WaitHandle handle) : base(message)
		{
			this.mutex_index = location;
			this.mutex = (handle as Mutex);
		}

		// Token: 0x06004052 RID: 16466 RVA: 0x000DEBAC File Offset: 0x000DCDAC
		public AbandonedMutexException(string message, Exception inner, int location, WaitHandle handle) : base(message, inner)
		{
			this.mutex_index = location;
			this.mutex = (handle as Mutex);
		}

		// Token: 0x17000C1B RID: 3099
		// (get) Token: 0x06004053 RID: 16467 RVA: 0x000DEBD4 File Offset: 0x000DCDD4
		public Mutex Mutex
		{
			get
			{
				return this.mutex;
			}
		}

		// Token: 0x17000C1C RID: 3100
		// (get) Token: 0x06004054 RID: 16468 RVA: 0x000DEBDC File Offset: 0x000DCDDC
		public int MutexIndex
		{
			get
			{
				return this.mutex_index;
			}
		}

		// Token: 0x04001BA2 RID: 7074
		private Mutex mutex;

		// Token: 0x04001BA3 RID: 7075
		private int mutex_index = -1;
	}
}
