﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x020006A4 RID: 1700
	[ComVisible(true)]
	public sealed class ManualResetEvent : EventWaitHandle
	{
		// Token: 0x060040AC RID: 16556 RVA: 0x000DF594 File Offset: 0x000DD794
		public ManualResetEvent(bool initialState) : base(initialState, EventResetMode.ManualReset)
		{
		}
	}
}
