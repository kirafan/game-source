﻿using System;
using System.Runtime.InteropServices;

namespace System.Threading
{
	// Token: 0x02000697 RID: 1687
	[ComVisible(true)]
	[Serializable]
	public enum ApartmentState
	{
		// Token: 0x04001BA5 RID: 7077
		STA,
		// Token: 0x04001BA6 RID: 7078
		MTA,
		// Token: 0x04001BA7 RID: 7079
		Unknown
	}
}
