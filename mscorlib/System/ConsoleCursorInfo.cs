﻿using System;

namespace System
{
	// Token: 0x02000199 RID: 409
	internal struct ConsoleCursorInfo
	{
		// Token: 0x0400082A RID: 2090
		public int Size;

		// Token: 0x0400082B RID: 2091
		public bool Visible;
	}
}
