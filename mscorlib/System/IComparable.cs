﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000009 RID: 9
	[ComVisible(true)]
	public interface IComparable
	{
		// Token: 0x06000081 RID: 129
		int CompareTo(object obj);
	}
}
