﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000150 RID: 336
	[ComVisible(true)]
	public enum MidpointRounding
	{
		// Token: 0x04000530 RID: 1328
		ToEven,
		// Token: 0x04000531 RID: 1329
		AwayFromZero
	}
}
