﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Resources
{
	// Token: 0x02000302 RID: 770
	[ComVisible(true)]
	public interface IResourceReader : IEnumerable, IDisposable
	{
		// Token: 0x060027D5 RID: 10197
		void Close();

		// Token: 0x060027D6 RID: 10198
		IDictionaryEnumerator GetEnumerator();
	}
}
