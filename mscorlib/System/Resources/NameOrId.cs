﻿using System;

namespace System.Resources
{
	// Token: 0x02000314 RID: 788
	internal class NameOrId
	{
		// Token: 0x06002850 RID: 10320 RVA: 0x00090C80 File Offset: 0x0008EE80
		public NameOrId(string name)
		{
			this.name = name;
		}

		// Token: 0x06002851 RID: 10321 RVA: 0x00090C90 File Offset: 0x0008EE90
		public NameOrId(int id)
		{
			this.id = id;
		}

		// Token: 0x1700072C RID: 1836
		// (get) Token: 0x06002852 RID: 10322 RVA: 0x00090CA0 File Offset: 0x0008EEA0
		public bool IsName
		{
			get
			{
				return this.name != null;
			}
		}

		// Token: 0x1700072D RID: 1837
		// (get) Token: 0x06002853 RID: 10323 RVA: 0x00090CB0 File Offset: 0x0008EEB0
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x1700072E RID: 1838
		// (get) Token: 0x06002854 RID: 10324 RVA: 0x00090CB8 File Offset: 0x0008EEB8
		public int Id
		{
			get
			{
				return this.id;
			}
		}

		// Token: 0x06002855 RID: 10325 RVA: 0x00090CC0 File Offset: 0x0008EEC0
		public override string ToString()
		{
			if (this.name != null)
			{
				return "Name(" + this.name + ")";
			}
			return "Id(" + this.id + ")";
		}

		// Token: 0x04001060 RID: 4192
		private string name;

		// Token: 0x04001061 RID: 4193
		private int id;
	}
}
