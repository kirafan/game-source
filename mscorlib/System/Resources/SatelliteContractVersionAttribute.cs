﻿using System;
using System.Runtime.InteropServices;

namespace System.Resources
{
	// Token: 0x02000311 RID: 785
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly)]
	public sealed class SatelliteContractVersionAttribute : Attribute
	{
		// Token: 0x0600284E RID: 10318 RVA: 0x00090C5C File Offset: 0x0008EE5C
		public SatelliteContractVersionAttribute(string version)
		{
			this.ver = new Version(version);
		}

		// Token: 0x1700072B RID: 1835
		// (get) Token: 0x0600284F RID: 10319 RVA: 0x00090C70 File Offset: 0x0008EE70
		public string Version
		{
			get
			{
				return this.ver.ToString();
			}
		}

		// Token: 0x04001047 RID: 4167
		private Version ver;
	}
}
