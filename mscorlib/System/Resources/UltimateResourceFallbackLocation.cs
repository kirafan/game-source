﻿using System;
using System.Runtime.InteropServices;

namespace System.Resources
{
	// Token: 0x02000312 RID: 786
	[ComVisible(true)]
	[Serializable]
	public enum UltimateResourceFallbackLocation
	{
		// Token: 0x04001049 RID: 4169
		MainAssembly,
		// Token: 0x0400104A RID: 4170
		Satellite
	}
}
