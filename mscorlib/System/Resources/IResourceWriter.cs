﻿using System;
using System.Runtime.InteropServices;

namespace System.Resources
{
	// Token: 0x02000303 RID: 771
	[ComVisible(true)]
	public interface IResourceWriter : IDisposable
	{
		// Token: 0x060027D7 RID: 10199
		void AddResource(string name, byte[] value);

		// Token: 0x060027D8 RID: 10200
		void AddResource(string name, object value);

		// Token: 0x060027D9 RID: 10201
		void AddResource(string name, string value);

		// Token: 0x060027DA RID: 10202
		void Close();

		// Token: 0x060027DB RID: 10203
		void Generate();
	}
}
