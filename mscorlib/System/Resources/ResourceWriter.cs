﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace System.Resources
{
	// Token: 0x0200030E RID: 782
	[ComVisible(true)]
	public sealed class ResourceWriter : IDisposable, IResourceWriter
	{
		// Token: 0x0600283A RID: 10298 RVA: 0x0009008C File Offset: 0x0008E28C
		public ResourceWriter(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (!stream.CanWrite)
			{
				throw new ArgumentException("Stream was not writable.");
			}
			this.stream = stream;
		}

		// Token: 0x0600283B RID: 10299 RVA: 0x000900E0 File Offset: 0x0008E2E0
		public ResourceWriter(string fileName)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			this.stream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
		}

		// Token: 0x0600283C RID: 10300 RVA: 0x00090118 File Offset: 0x0008E318
		public void AddResource(string name, byte[] value)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (this.resources == null)
			{
				throw new InvalidOperationException("The resource writer has already been closed and cannot be edited");
			}
			if (this.resources[name] != null)
			{
				throw new ArgumentException("Resource already present: " + name);
			}
			this.resources.Add(name, value);
		}

		// Token: 0x0600283D RID: 10301 RVA: 0x0009017C File Offset: 0x0008E37C
		public void AddResource(string name, object value)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (this.resources == null)
			{
				throw new InvalidOperationException("The resource writer has already been closed and cannot be edited");
			}
			if (this.resources[name] != null)
			{
				throw new ArgumentException("Resource already present: " + name);
			}
			this.resources.Add(name, value);
		}

		// Token: 0x0600283E RID: 10302 RVA: 0x000901E0 File Offset: 0x0008E3E0
		public void AddResource(string name, string value)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (this.resources == null)
			{
				throw new InvalidOperationException("The resource writer has already been closed and cannot be edited");
			}
			if (this.resources[name] != null)
			{
				throw new ArgumentException("Resource already present: " + name);
			}
			this.resources.Add(name, value);
		}

		// Token: 0x0600283F RID: 10303 RVA: 0x00090244 File Offset: 0x0008E444
		public void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x06002840 RID: 10304 RVA: 0x00090250 File Offset: 0x0008E450
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x06002841 RID: 10305 RVA: 0x0009025C File Offset: 0x0008E45C
		private void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (this.resources != null)
				{
					this.Generate();
				}
				if (this.stream != null)
				{
					this.stream.Close();
				}
				GC.SuppressFinalize(this);
			}
			this.resources = null;
			this.stream = null;
		}

		// Token: 0x06002842 RID: 10306 RVA: 0x000902AC File Offset: 0x0008E4AC
		public void AddResourceData(string name, string typeName, byte[] serializedData)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}
			if (serializedData == null)
			{
				throw new ArgumentNullException("serializedData");
			}
			this.AddResource(name, new ResourceWriter.TypeByNameObject(typeName, serializedData));
		}

		// Token: 0x06002843 RID: 10307 RVA: 0x000902FC File Offset: 0x0008E4FC
		public void Generate()
		{
			if (this.resources == null)
			{
				throw new InvalidOperationException("The resource writer has already been closed and cannot be edited");
			}
			BinaryWriter binaryWriter = new BinaryWriter(this.stream, Encoding.UTF8);
			IFormatter formatter = new BinaryFormatter(null, new StreamingContext(StreamingContextStates.File | StreamingContextStates.Persistence));
			binaryWriter.Write(ResourceManager.MagicNumber);
			binaryWriter.Write(ResourceManager.HeaderVersionNumber);
			MemoryStream memoryStream = new MemoryStream();
			BinaryWriter binaryWriter2 = new BinaryWriter(memoryStream, Encoding.UTF8);
			binaryWriter2.Write(typeof(ResourceReader).AssemblyQualifiedName);
			binaryWriter2.Write(typeof(RuntimeResourceSet).FullName);
			int num = (int)memoryStream.Length;
			binaryWriter.Write(num);
			binaryWriter.Write(memoryStream.GetBuffer(), 0, num);
			MemoryStream memoryStream2 = new MemoryStream();
			BinaryWriter binaryWriter3 = new BinaryWriter(memoryStream2, Encoding.Unicode);
			MemoryStream memoryStream3 = new MemoryStream();
			BinaryWriter binaryWriter4 = new BinaryWriter(memoryStream3, Encoding.UTF8);
			ArrayList arrayList = new ArrayList();
			int[] array = new int[this.resources.Count];
			int[] array2 = new int[this.resources.Count];
			int num2 = 0;
			IDictionaryEnumerator enumerator = this.resources.GetEnumerator();
			while (enumerator.MoveNext())
			{
				array[num2] = this.GetHash((string)enumerator.Key);
				array2[num2] = (int)binaryWriter3.BaseStream.Position;
				binaryWriter3.Write((string)enumerator.Key);
				binaryWriter3.Write((int)binaryWriter4.BaseStream.Position);
				if (enumerator.Value == null)
				{
					this.Write7BitEncodedInt(binaryWriter4, -1);
					num2++;
				}
				else
				{
					ResourceWriter.TypeByNameObject typeByNameObject = enumerator.Value as ResourceWriter.TypeByNameObject;
					Type type = (typeByNameObject == null) ? enumerator.Value.GetType() : null;
					object obj = (typeByNameObject == null) ? type : typeByNameObject.TypeName;
					switch ((type == null || type.IsEnum) ? TypeCode.Empty : Type.GetTypeCode(type))
					{
					case TypeCode.SByte:
					case TypeCode.Byte:
					case TypeCode.Int16:
					case TypeCode.UInt16:
					case TypeCode.Int32:
					case TypeCode.UInt32:
					case TypeCode.Int64:
					case TypeCode.UInt64:
					case TypeCode.Single:
					case TypeCode.Double:
					case TypeCode.Decimal:
					case TypeCode.DateTime:
					case TypeCode.String:
						break;
					case (TypeCode)17:
						goto IL_22E;
					default:
						goto IL_22E;
					}
					IL_2A1:
					if (typeByNameObject != null)
					{
						binaryWriter4.Write(typeByNameObject.Value);
					}
					else if (type == typeof(byte))
					{
						binaryWriter4.Write(4);
						binaryWriter4.Write((byte)enumerator.Value);
					}
					else if (type == typeof(decimal))
					{
						binaryWriter4.Write(14);
						binaryWriter4.Write((decimal)enumerator.Value);
					}
					else if (type == typeof(DateTime))
					{
						binaryWriter4.Write(15);
						binaryWriter4.Write(((DateTime)enumerator.Value).Ticks);
					}
					else if (type == typeof(double))
					{
						binaryWriter4.Write(13);
						binaryWriter4.Write((double)enumerator.Value);
					}
					else if (type == typeof(short))
					{
						binaryWriter4.Write(6);
						binaryWriter4.Write((short)enumerator.Value);
					}
					else if (type == typeof(int))
					{
						binaryWriter4.Write(8);
						binaryWriter4.Write((int)enumerator.Value);
					}
					else if (type == typeof(long))
					{
						binaryWriter4.Write(10);
						binaryWriter4.Write((long)enumerator.Value);
					}
					else if (type == typeof(sbyte))
					{
						binaryWriter4.Write(5);
						binaryWriter4.Write((sbyte)enumerator.Value);
					}
					else if (type == typeof(float))
					{
						binaryWriter4.Write(12);
						binaryWriter4.Write((float)enumerator.Value);
					}
					else if (type == typeof(string))
					{
						binaryWriter4.Write(1);
						binaryWriter4.Write((string)enumerator.Value);
					}
					else if (type == typeof(TimeSpan))
					{
						binaryWriter4.Write(16);
						binaryWriter4.Write(((TimeSpan)enumerator.Value).Ticks);
					}
					else if (type == typeof(ushort))
					{
						binaryWriter4.Write(7);
						binaryWriter4.Write((ushort)enumerator.Value);
					}
					else if (type == typeof(uint))
					{
						binaryWriter4.Write(9);
						binaryWriter4.Write((uint)enumerator.Value);
					}
					else if (type == typeof(ulong))
					{
						binaryWriter4.Write(11);
						binaryWriter4.Write((ulong)enumerator.Value);
					}
					else if (type == typeof(byte[]))
					{
						binaryWriter4.Write(32);
						byte[] array3 = (byte[])enumerator.Value;
						binaryWriter4.Write((uint)array3.Length);
						binaryWriter4.Write(array3, 0, array3.Length);
					}
					else if (type == typeof(MemoryStream))
					{
						binaryWriter4.Write(33);
						byte[] array4 = ((MemoryStream)enumerator.Value).ToArray();
						binaryWriter4.Write((uint)array4.Length);
						binaryWriter4.Write(array4, 0, array4.Length);
					}
					else
					{
						formatter.Serialize(binaryWriter4.BaseStream, enumerator.Value);
					}
					num2++;
					continue;
					IL_22E:
					if (type == typeof(TimeSpan))
					{
						goto IL_2A1;
					}
					if (type == typeof(MemoryStream))
					{
						goto IL_2A1;
					}
					if (type == typeof(byte[]))
					{
						goto IL_2A1;
					}
					if (!arrayList.Contains(obj))
					{
						arrayList.Add(obj);
					}
					this.Write7BitEncodedInt(binaryWriter4, 64 + arrayList.IndexOf(obj));
					goto IL_2A1;
				}
			}
			Array.Sort<int, int>(array, array2);
			binaryWriter.Write(2);
			binaryWriter.Write(this.resources.Count);
			binaryWriter.Write(arrayList.Count);
			foreach (object obj2 in arrayList)
			{
				if (obj2 is Type)
				{
					binaryWriter.Write(((Type)obj2).AssemblyQualifiedName);
				}
				else
				{
					binaryWriter.Write((string)obj2);
				}
			}
			int num3 = (int)(binaryWriter.BaseStream.Position & 7L);
			int num4 = 0;
			if (num3 != 0)
			{
				num4 = 8 - num3;
			}
			for (int i = 0; i < num4; i++)
			{
				binaryWriter.Write((byte)"PAD"[i % 3]);
			}
			for (int j = 0; j < this.resources.Count; j++)
			{
				binaryWriter.Write(array[j]);
			}
			for (int k = 0; k < this.resources.Count; k++)
			{
				binaryWriter.Write(array2[k]);
			}
			int value = (int)binaryWriter.BaseStream.Position + (int)memoryStream2.Length + 4;
			binaryWriter.Write(value);
			binaryWriter.Write(memoryStream2.GetBuffer(), 0, (int)memoryStream2.Length);
			binaryWriter.Write(memoryStream3.GetBuffer(), 0, (int)memoryStream3.Length);
			binaryWriter3.Close();
			binaryWriter4.Close();
			binaryWriter.Flush();
			this.resources = null;
		}

		// Token: 0x06002844 RID: 10308 RVA: 0x00090B08 File Offset: 0x0008ED08
		private int GetHash(string name)
		{
			uint num = 5381U;
			for (int i = 0; i < name.Length; i++)
			{
				num = ((num << 5) + num ^ (uint)name[i]);
			}
			return (int)num;
		}

		// Token: 0x06002845 RID: 10309 RVA: 0x00090B44 File Offset: 0x0008ED44
		private void Write7BitEncodedInt(BinaryWriter writer, int value)
		{
			do
			{
				int num = value >> 7 & 33554431;
				byte b = (byte)(value & 127);
				if (num != 0)
				{
					b |= 128;
				}
				writer.Write(b);
				value = num;
			}
			while (value != 0);
		}

		// Token: 0x1700072A RID: 1834
		// (get) Token: 0x06002846 RID: 10310 RVA: 0x00090B80 File Offset: 0x0008ED80
		internal Stream Stream
		{
			get
			{
				return this.stream;
			}
		}

		// Token: 0x04001043 RID: 4163
		private SortedList resources = new SortedList(StringComparer.OrdinalIgnoreCase);

		// Token: 0x04001044 RID: 4164
		private Stream stream;

		// Token: 0x0200030F RID: 783
		private class TypeByNameObject
		{
			// Token: 0x06002847 RID: 10311 RVA: 0x00090B88 File Offset: 0x0008ED88
			public TypeByNameObject(string typeName, byte[] value)
			{
				this.TypeName = typeName;
				this.Value = (byte[])value.Clone();
			}

			// Token: 0x04001045 RID: 4165
			public readonly string TypeName;

			// Token: 0x04001046 RID: 4166
			public readonly byte[] Value;
		}
	}
}
