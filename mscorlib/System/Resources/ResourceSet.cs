﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Resources
{
	// Token: 0x0200030D RID: 781
	[ComVisible(true)]
	[Serializable]
	public class ResourceSet : IEnumerable, IDisposable
	{
		// Token: 0x06002826 RID: 10278 RVA: 0x0008FCD8 File Offset: 0x0008DED8
		protected ResourceSet()
		{
			this.Table = new Hashtable();
			this.resources_read = true;
		}

		// Token: 0x06002827 RID: 10279 RVA: 0x0008FCF4 File Offset: 0x0008DEF4
		public ResourceSet(IResourceReader reader)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			this.Table = new Hashtable();
			this.Reader = reader;
		}

		// Token: 0x06002828 RID: 10280 RVA: 0x0008FD20 File Offset: 0x0008DF20
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public ResourceSet(Stream stream)
		{
			this.Table = new Hashtable();
			this.Reader = new ResourceReader(stream);
		}

		// Token: 0x06002829 RID: 10281 RVA: 0x0008FD40 File Offset: 0x0008DF40
		internal ResourceSet(UnmanagedMemoryStream stream)
		{
			this.Table = new Hashtable();
			this.Reader = new ResourceReader(stream);
		}

		// Token: 0x0600282A RID: 10282 RVA: 0x0008FD60 File Offset: 0x0008DF60
		public ResourceSet(string fileName)
		{
			this.Table = new Hashtable();
			this.Reader = new ResourceReader(fileName);
		}

		// Token: 0x0600282B RID: 10283 RVA: 0x0008FD80 File Offset: 0x0008DF80
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x0600282C RID: 10284 RVA: 0x0008FD88 File Offset: 0x0008DF88
		public virtual void Close()
		{
			this.Dispose();
		}

		// Token: 0x0600282D RID: 10285 RVA: 0x0008FD90 File Offset: 0x0008DF90
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x0600282E RID: 10286 RVA: 0x0008FDA0 File Offset: 0x0008DFA0
		protected virtual void Dispose(bool disposing)
		{
			if (disposing && this.Reader != null)
			{
				this.Reader.Close();
			}
			this.Reader = null;
			this.Table = null;
			this.disposed = true;
		}

		// Token: 0x0600282F RID: 10287 RVA: 0x0008FDD4 File Offset: 0x0008DFD4
		public virtual Type GetDefaultReader()
		{
			return typeof(ResourceReader);
		}

		// Token: 0x06002830 RID: 10288 RVA: 0x0008FDE0 File Offset: 0x0008DFE0
		public virtual Type GetDefaultWriter()
		{
			return typeof(ResourceWriter);
		}

		// Token: 0x06002831 RID: 10289 RVA: 0x0008FDEC File Offset: 0x0008DFEC
		[ComVisible(false)]
		public virtual IDictionaryEnumerator GetEnumerator()
		{
			if (this.disposed)
			{
				throw new ObjectDisposedException("ResourceSet is closed.");
			}
			this.ReadResources();
			return this.Table.GetEnumerator();
		}

		// Token: 0x06002832 RID: 10290 RVA: 0x0008FE18 File Offset: 0x0008E018
		private object GetObjectInternal(string name, bool ignoreCase)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (this.disposed)
			{
				throw new ObjectDisposedException("ResourceSet is closed.");
			}
			this.ReadResources();
			object obj = this.Table[name];
			if (obj != null)
			{
				return obj;
			}
			if (ignoreCase)
			{
				foreach (object obj2 in this.Table)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj2;
					string strA = (string)dictionaryEntry.Key;
					if (string.Compare(strA, name, true, CultureInfo.InvariantCulture) == 0)
					{
						return dictionaryEntry.Value;
					}
				}
			}
			return null;
		}

		// Token: 0x06002833 RID: 10291 RVA: 0x0008FEFC File Offset: 0x0008E0FC
		public virtual object GetObject(string name)
		{
			return this.GetObjectInternal(name, false);
		}

		// Token: 0x06002834 RID: 10292 RVA: 0x0008FF08 File Offset: 0x0008E108
		public virtual object GetObject(string name, bool ignoreCase)
		{
			return this.GetObjectInternal(name, ignoreCase);
		}

		// Token: 0x06002835 RID: 10293 RVA: 0x0008FF14 File Offset: 0x0008E114
		private string GetStringInternal(string name, bool ignoreCase)
		{
			object @object = this.GetObject(name, ignoreCase);
			if (@object == null)
			{
				return null;
			}
			string text = @object as string;
			if (text == null)
			{
				throw new InvalidOperationException(string.Format("Resource '{0}' is not a String. Use GetObject instead.", name));
			}
			return text;
		}

		// Token: 0x06002836 RID: 10294 RVA: 0x0008FF54 File Offset: 0x0008E154
		public virtual string GetString(string name)
		{
			return this.GetStringInternal(name, false);
		}

		// Token: 0x06002837 RID: 10295 RVA: 0x0008FF60 File Offset: 0x0008E160
		public virtual string GetString(string name, bool ignoreCase)
		{
			return this.GetStringInternal(name, ignoreCase);
		}

		// Token: 0x06002838 RID: 10296 RVA: 0x0008FF6C File Offset: 0x0008E16C
		protected virtual void ReadResources()
		{
			if (this.resources_read)
			{
				return;
			}
			if (this.Reader == null)
			{
				throw new ObjectDisposedException("ResourceSet is closed.");
			}
			Hashtable table = this.Table;
			lock (table)
			{
				if (!this.resources_read)
				{
					IDictionaryEnumerator enumerator = this.Reader.GetEnumerator();
					enumerator.Reset();
					while (enumerator.MoveNext())
					{
						this.Table.Add(enumerator.Key, enumerator.Value);
					}
					this.resources_read = true;
				}
			}
		}

		// Token: 0x06002839 RID: 10297 RVA: 0x00090020 File Offset: 0x0008E220
		internal UnmanagedMemoryStream GetStream(string name, bool ignoreCase)
		{
			if (this.Reader == null)
			{
				throw new ObjectDisposedException("ResourceSet is closed.");
			}
			IDictionaryEnumerator enumerator = this.Reader.GetEnumerator();
			enumerator.Reset();
			while (enumerator.MoveNext())
			{
				if (string.Compare(name, (string)enumerator.Key, ignoreCase) == 0)
				{
					return ((ResourceReader.ResourceEnumerator)enumerator).ValueAsStream;
				}
			}
			return null;
		}

		// Token: 0x0400103F RID: 4159
		[NonSerialized]
		protected IResourceReader Reader;

		// Token: 0x04001040 RID: 4160
		protected Hashtable Table;

		// Token: 0x04001041 RID: 4161
		private bool resources_read;

		// Token: 0x04001042 RID: 4162
		[NonSerialized]
		private bool disposed;
	}
}
