﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Resources
{
	// Token: 0x02000305 RID: 773
	[ComVisible(true)]
	[Serializable]
	public class MissingSatelliteAssemblyException : SystemException
	{
		// Token: 0x060027E0 RID: 10208 RVA: 0x0008DFF4 File Offset: 0x0008C1F4
		public MissingSatelliteAssemblyException() : base(Locale.GetText("The satellite assembly was not found for the required culture."))
		{
		}

		// Token: 0x060027E1 RID: 10209 RVA: 0x0008E008 File Offset: 0x0008C208
		public MissingSatelliteAssemblyException(string message) : base(message)
		{
		}

		// Token: 0x060027E2 RID: 10210 RVA: 0x0008E014 File Offset: 0x0008C214
		public MissingSatelliteAssemblyException(string message, string cultureName) : base(message)
		{
			this.culture = cultureName;
		}

		// Token: 0x060027E3 RID: 10211 RVA: 0x0008E024 File Offset: 0x0008C224
		protected MissingSatelliteAssemblyException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060027E4 RID: 10212 RVA: 0x0008E030 File Offset: 0x0008C230
		public MissingSatelliteAssemblyException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x1700071D RID: 1821
		// (get) Token: 0x060027E5 RID: 10213 RVA: 0x0008E03C File Offset: 0x0008C23C
		public string CultureName
		{
			get
			{
				return this.culture;
			}
		}

		// Token: 0x04001005 RID: 4101
		private string culture;
	}
}
