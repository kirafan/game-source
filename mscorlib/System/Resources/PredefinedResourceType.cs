﻿using System;

namespace System.Resources
{
	// Token: 0x02000308 RID: 776
	internal enum PredefinedResourceType
	{
		// Token: 0x04001016 RID: 4118
		Null,
		// Token: 0x04001017 RID: 4119
		String,
		// Token: 0x04001018 RID: 4120
		Bool,
		// Token: 0x04001019 RID: 4121
		Char,
		// Token: 0x0400101A RID: 4122
		Byte,
		// Token: 0x0400101B RID: 4123
		SByte,
		// Token: 0x0400101C RID: 4124
		Int16,
		// Token: 0x0400101D RID: 4125
		UInt16,
		// Token: 0x0400101E RID: 4126
		Int32,
		// Token: 0x0400101F RID: 4127
		UInt32,
		// Token: 0x04001020 RID: 4128
		Int64,
		// Token: 0x04001021 RID: 4129
		UInt64,
		// Token: 0x04001022 RID: 4130
		Single,
		// Token: 0x04001023 RID: 4131
		Double,
		// Token: 0x04001024 RID: 4132
		Decimal,
		// Token: 0x04001025 RID: 4133
		DateTime,
		// Token: 0x04001026 RID: 4134
		TimeSpan,
		// Token: 0x04001027 RID: 4135
		ByteArray = 32,
		// Token: 0x04001028 RID: 4136
		Stream,
		// Token: 0x04001029 RID: 4137
		FistCustom = 64
	}
}
