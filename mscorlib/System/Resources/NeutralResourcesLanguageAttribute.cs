﻿using System;
using System.Runtime.InteropServices;

namespace System.Resources
{
	// Token: 0x02000306 RID: 774
	[AttributeUsage(AttributeTargets.Assembly)]
	[ComVisible(true)]
	public sealed class NeutralResourcesLanguageAttribute : Attribute
	{
		// Token: 0x060027E6 RID: 10214 RVA: 0x0008E044 File Offset: 0x0008C244
		public NeutralResourcesLanguageAttribute(string cultureName)
		{
			if (cultureName == null)
			{
				throw new ArgumentNullException("culture is null");
			}
			this.culture = cultureName;
		}

		// Token: 0x060027E7 RID: 10215 RVA: 0x0008E064 File Offset: 0x0008C264
		public NeutralResourcesLanguageAttribute(string cultureName, UltimateResourceFallbackLocation location)
		{
			if (cultureName == null)
			{
				throw new ArgumentNullException("culture is null");
			}
			this.culture = cultureName;
			this.loc = location;
		}

		// Token: 0x1700071E RID: 1822
		// (get) Token: 0x060027E8 RID: 10216 RVA: 0x0008E08C File Offset: 0x0008C28C
		public string CultureName
		{
			get
			{
				return this.culture;
			}
		}

		// Token: 0x1700071F RID: 1823
		// (get) Token: 0x060027E9 RID: 10217 RVA: 0x0008E094 File Offset: 0x0008C294
		public UltimateResourceFallbackLocation Location
		{
			get
			{
				return this.loc;
			}
		}

		// Token: 0x04001006 RID: 4102
		private string culture;

		// Token: 0x04001007 RID: 4103
		private UltimateResourceFallbackLocation loc;
	}
}
