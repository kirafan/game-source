﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Resources
{
	// Token: 0x02000304 RID: 772
	[ComVisible(true)]
	[Serializable]
	public class MissingManifestResourceException : SystemException
	{
		// Token: 0x060027DC RID: 10204 RVA: 0x0008DFBC File Offset: 0x0008C1BC
		public MissingManifestResourceException() : base(Locale.GetText("The assembly does not contain the resources for the required culture."))
		{
		}

		// Token: 0x060027DD RID: 10205 RVA: 0x0008DFD0 File Offset: 0x0008C1D0
		public MissingManifestResourceException(string message) : base(message)
		{
		}

		// Token: 0x060027DE RID: 10206 RVA: 0x0008DFDC File Offset: 0x0008C1DC
		protected MissingManifestResourceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060027DF RID: 10207 RVA: 0x0008DFE8 File Offset: 0x0008C1E8
		public MissingManifestResourceException(string message, Exception inner) : base(message, inner)
		{
		}
	}
}
