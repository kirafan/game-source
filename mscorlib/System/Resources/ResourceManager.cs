﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Resources
{
	// Token: 0x02000307 RID: 775
	[ComVisible(true)]
	[Serializable]
	public class ResourceManager
	{
		// Token: 0x060027EA RID: 10218 RVA: 0x0008E09C File Offset: 0x0008C29C
		protected ResourceManager()
		{
		}

		// Token: 0x060027EB RID: 10219 RVA: 0x0008E0B4 File Offset: 0x0008C2B4
		public ResourceManager(Type resourceSource)
		{
			if (resourceSource == null)
			{
				throw new ArgumentNullException("resourceSource");
			}
			this.resourceSource = resourceSource;
			this.BaseNameField = resourceSource.Name;
			this.MainAssembly = resourceSource.Assembly;
			this.ResourceSets = ResourceManager.GetResourceSets(this.MainAssembly, this.BaseNameField);
			this.neutral_culture = ResourceManager.GetNeutralResourcesLanguage(this.MainAssembly);
		}

		// Token: 0x060027EC RID: 10220 RVA: 0x0008E130 File Offset: 0x0008C330
		public ResourceManager(string baseName, Assembly assembly)
		{
			if (baseName == null)
			{
				throw new ArgumentNullException("baseName");
			}
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			this.BaseNameField = baseName;
			this.MainAssembly = assembly;
			this.ResourceSets = ResourceManager.GetResourceSets(this.MainAssembly, this.BaseNameField);
			this.neutral_culture = ResourceManager.GetNeutralResourcesLanguage(this.MainAssembly);
		}

		// Token: 0x060027ED RID: 10221 RVA: 0x0008E1AC File Offset: 0x0008C3AC
		public ResourceManager(string baseName, Assembly assembly, Type usingResourceSet)
		{
			if (baseName == null)
			{
				throw new ArgumentNullException("baseName");
			}
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			this.BaseNameField = baseName;
			this.MainAssembly = assembly;
			this.ResourceSets = ResourceManager.GetResourceSets(this.MainAssembly, this.BaseNameField);
			this.resourceSetType = this.CheckResourceSetType(usingResourceSet, true);
			this.neutral_culture = ResourceManager.GetNeutralResourcesLanguage(this.MainAssembly);
		}

		// Token: 0x060027EE RID: 10222 RVA: 0x0008E238 File Offset: 0x0008C438
		private ResourceManager(string baseName, string resourceDir, Type usingResourceSet)
		{
			if (baseName == null)
			{
				throw new ArgumentNullException("baseName");
			}
			if (resourceDir == null)
			{
				throw new ArgumentNullException("resourceDir");
			}
			this.BaseNameField = baseName;
			this.resourceDir = resourceDir;
			this.resourceSetType = this.CheckResourceSetType(usingResourceSet, false);
			this.ResourceSets = ResourceManager.GetResourceSets(this.MainAssembly, this.BaseNameField);
		}

		// Token: 0x060027F0 RID: 10224 RVA: 0x0008E2DC File Offset: 0x0008C4DC
		private static Hashtable GetResourceSets(Assembly assembly, string basename)
		{
			Hashtable resourceCache = ResourceManager.ResourceCache;
			Hashtable result;
			lock (resourceCache)
			{
				string text = string.Empty;
				if (assembly != null)
				{
					text = assembly.FullName;
				}
				else
				{
					text = basename.GetHashCode().ToString() + "@@";
				}
				if (basename != null && basename != string.Empty)
				{
					text = text + "!" + basename;
				}
				else
				{
					text = text + "!" + text.GetHashCode();
				}
				Hashtable hashtable = ResourceManager.ResourceCache[text] as Hashtable;
				if (hashtable == null)
				{
					hashtable = Hashtable.Synchronized(new Hashtable());
					ResourceManager.ResourceCache[text] = hashtable;
				}
				result = hashtable;
			}
			return result;
		}

		// Token: 0x060027F1 RID: 10225 RVA: 0x0008E3C8 File Offset: 0x0008C5C8
		private Type CheckResourceSetType(Type usingResourceSet, bool verifyType)
		{
			if (usingResourceSet == null)
			{
				return this.resourceSetType;
			}
			if (verifyType && !typeof(ResourceSet).IsAssignableFrom(usingResourceSet))
			{
				throw new ArgumentException("Type parameter must refer to a subclass of ResourceSet.", "usingResourceSet");
			}
			return usingResourceSet;
		}

		// Token: 0x060027F2 RID: 10226 RVA: 0x0008E404 File Offset: 0x0008C604
		public static ResourceManager CreateFileBasedResourceManager(string baseName, string resourceDir, Type usingResourceSet)
		{
			return new ResourceManager(baseName, resourceDir, usingResourceSet);
		}

		// Token: 0x17000720 RID: 1824
		// (get) Token: 0x060027F3 RID: 10227 RVA: 0x0008E410 File Offset: 0x0008C610
		public virtual string BaseName
		{
			get
			{
				return this.BaseNameField;
			}
		}

		// Token: 0x17000721 RID: 1825
		// (get) Token: 0x060027F4 RID: 10228 RVA: 0x0008E418 File Offset: 0x0008C618
		// (set) Token: 0x060027F5 RID: 10229 RVA: 0x0008E420 File Offset: 0x0008C620
		public virtual bool IgnoreCase
		{
			get
			{
				return this.ignoreCase;
			}
			set
			{
				this.ignoreCase = value;
			}
		}

		// Token: 0x17000722 RID: 1826
		// (get) Token: 0x060027F6 RID: 10230 RVA: 0x0008E42C File Offset: 0x0008C62C
		public virtual Type ResourceSetType
		{
			get
			{
				return this.resourceSetType;
			}
		}

		// Token: 0x060027F7 RID: 10231 RVA: 0x0008E434 File Offset: 0x0008C634
		public virtual object GetObject(string name)
		{
			return this.GetObject(name, null);
		}

		// Token: 0x060027F8 RID: 10232 RVA: 0x0008E440 File Offset: 0x0008C640
		public virtual object GetObject(string name, CultureInfo culture)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (culture == null)
			{
				culture = CultureInfo.CurrentUICulture;
			}
			lock (this)
			{
				ResourceSet resourceSet = this.InternalGetResourceSet(culture, true, true);
				object @object;
				if (resourceSet != null)
				{
					@object = resourceSet.GetObject(name, this.ignoreCase);
					if (@object != null)
					{
						return @object;
					}
				}
				for (;;)
				{
					culture = culture.Parent;
					resourceSet = this.InternalGetResourceSet(culture, true, true);
					if (resourceSet != null)
					{
						@object = resourceSet.GetObject(name, this.ignoreCase);
						if (@object != null)
						{
							break;
						}
					}
					if (culture.Equals(this.neutral_culture) || culture.Equals(CultureInfo.InvariantCulture))
					{
						goto IL_A7;
					}
				}
				return @object;
				IL_A7:;
			}
			return null;
		}

		// Token: 0x060027F9 RID: 10233 RVA: 0x0008E520 File Offset: 0x0008C720
		public virtual ResourceSet GetResourceSet(CultureInfo culture, bool createIfNotExists, bool tryParents)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("culture");
			}
			ResourceSet result;
			lock (this)
			{
				result = this.InternalGetResourceSet(culture, createIfNotExists, tryParents);
			}
			return result;
		}

		// Token: 0x060027FA RID: 10234 RVA: 0x0008E580 File Offset: 0x0008C780
		public virtual string GetString(string name)
		{
			return this.GetString(name, null);
		}

		// Token: 0x060027FB RID: 10235 RVA: 0x0008E58C File Offset: 0x0008C78C
		public virtual string GetString(string name, CultureInfo culture)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (culture == null)
			{
				culture = CultureInfo.CurrentUICulture;
			}
			lock (this)
			{
				ResourceSet resourceSet = this.InternalGetResourceSet(culture, true, true);
				string @string;
				if (resourceSet != null)
				{
					@string = resourceSet.GetString(name, this.ignoreCase);
					if (@string != null)
					{
						return @string;
					}
				}
				for (;;)
				{
					culture = culture.Parent;
					resourceSet = this.InternalGetResourceSet(culture, true, true);
					if (resourceSet != null)
					{
						@string = resourceSet.GetString(name, this.ignoreCase);
						if (@string != null)
						{
							break;
						}
					}
					if (culture.Equals(this.neutral_culture) || culture.Equals(CultureInfo.InvariantCulture))
					{
						goto IL_A7;
					}
				}
				return @string;
				IL_A7:;
			}
			return null;
		}

		// Token: 0x060027FC RID: 10236 RVA: 0x0008E66C File Offset: 0x0008C86C
		protected virtual string GetResourceFileName(CultureInfo culture)
		{
			if (culture.Equals(CultureInfo.InvariantCulture))
			{
				return this.BaseNameField + ".resources";
			}
			return this.BaseNameField + "." + culture.Name + ".resources";
		}

		// Token: 0x060027FD RID: 10237 RVA: 0x0008E6B8 File Offset: 0x0008C8B8
		private string GetResourceFilePath(CultureInfo culture)
		{
			if (this.resourceDir != null)
			{
				return Path.Combine(this.resourceDir, this.GetResourceFileName(culture));
			}
			return this.GetResourceFileName(culture);
		}

		// Token: 0x060027FE RID: 10238 RVA: 0x0008E6E0 File Offset: 0x0008C8E0
		private Stream GetManifestResourceStreamNoCase(Assembly ass, string fn)
		{
			string manifestResourceName = this.GetManifestResourceName(fn);
			foreach (string text in ass.GetManifestResourceNames())
			{
				if (string.Compare(manifestResourceName, text, true, CultureInfo.InvariantCulture) == 0)
				{
					return ass.GetManifestResourceStream(text);
				}
			}
			return null;
		}

		// Token: 0x060027FF RID: 10239 RVA: 0x0008E730 File Offset: 0x0008C930
		[ComVisible(false)]
		[CLSCompliant(false)]
		public UnmanagedMemoryStream GetStream(string name)
		{
			return this.GetStream(name, null);
		}

		// Token: 0x06002800 RID: 10240 RVA: 0x0008E73C File Offset: 0x0008C93C
		[ComVisible(false)]
		[CLSCompliant(false)]
		public UnmanagedMemoryStream GetStream(string name, CultureInfo culture)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (culture == null)
			{
				culture = CultureInfo.CurrentUICulture;
			}
			ResourceSet resourceSet = this.InternalGetResourceSet(culture, true, true);
			return resourceSet.GetStream(name, this.ignoreCase);
		}

		// Token: 0x06002801 RID: 10241 RVA: 0x0008E780 File Offset: 0x0008C980
		protected virtual ResourceSet InternalGetResourceSet(CultureInfo culture, bool createIfNotExists, bool tryParents)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("key");
			}
			ResourceSet resourceSet = (ResourceSet)this.ResourceSets[culture];
			if (resourceSet != null)
			{
				return resourceSet;
			}
			if (ResourceManager.NonExistent.Contains(culture))
			{
				return null;
			}
			if (this.MainAssembly != null)
			{
				CultureInfo cultureInfo = culture;
				if (culture.Equals(this.neutral_culture))
				{
					cultureInfo = CultureInfo.InvariantCulture;
				}
				Stream stream = null;
				string resourceFileName = this.GetResourceFileName(cultureInfo);
				if (!cultureInfo.Equals(CultureInfo.InvariantCulture))
				{
					Version satelliteContractVersion = ResourceManager.GetSatelliteContractVersion(this.MainAssembly);
					try
					{
						Assembly satelliteAssemblyNoThrow = this.MainAssembly.GetSatelliteAssemblyNoThrow(cultureInfo, satelliteContractVersion);
						if (satelliteAssemblyNoThrow != null)
						{
							stream = satelliteAssemblyNoThrow.GetManifestResourceStream(resourceFileName);
							if (stream == null)
							{
								stream = this.GetManifestResourceStreamNoCase(satelliteAssemblyNoThrow, resourceFileName);
							}
						}
					}
					catch (Exception)
					{
					}
				}
				else
				{
					stream = this.MainAssembly.GetManifestResourceStream(this.resourceSource, resourceFileName);
					if (stream == null)
					{
						stream = this.GetManifestResourceStreamNoCase(this.MainAssembly, resourceFileName);
					}
				}
				if (stream != null && createIfNotExists)
				{
					object[] args = new object[]
					{
						stream
					};
					resourceSet = (ResourceSet)Activator.CreateInstance(this.resourceSetType, args);
				}
				else if (cultureInfo.Equals(CultureInfo.InvariantCulture))
				{
					throw this.AssemblyResourceMissing(resourceFileName);
				}
			}
			else if (this.resourceDir != null || this.BaseNameField != null)
			{
				string resourceFilePath = this.GetResourceFilePath(culture);
				if (createIfNotExists && File.Exists(resourceFilePath))
				{
					object[] args2 = new object[]
					{
						resourceFilePath
					};
					resourceSet = (ResourceSet)Activator.CreateInstance(this.resourceSetType, args2);
				}
				else if (culture.Equals(CultureInfo.InvariantCulture))
				{
					string message = string.Format("Could not find any resources appropriate for the specified culture (or the neutral culture) on disk.{0}baseName: {1}  locationInfo: {2}  fileName: {3}", new object[]
					{
						Environment.NewLine,
						this.BaseNameField,
						"<null>",
						this.GetResourceFileName(culture)
					});
					throw new MissingManifestResourceException(message);
				}
			}
			if (resourceSet == null && tryParents && !culture.Equals(CultureInfo.InvariantCulture))
			{
				resourceSet = this.InternalGetResourceSet(culture.Parent, createIfNotExists, tryParents);
			}
			if (resourceSet != null)
			{
				this.ResourceSets[culture] = resourceSet;
			}
			else
			{
				ResourceManager.NonExistent[culture] = culture;
			}
			return resourceSet;
		}

		// Token: 0x06002802 RID: 10242 RVA: 0x0008E9D8 File Offset: 0x0008CBD8
		public virtual void ReleaseAllResources()
		{
			lock (this)
			{
				foreach (object obj in this.ResourceSets.Values)
				{
					ResourceSet resourceSet = (ResourceSet)obj;
					resourceSet.Close();
				}
				this.ResourceSets.Clear();
			}
		}

		// Token: 0x06002803 RID: 10243 RVA: 0x0008EA84 File Offset: 0x0008CC84
		protected static CultureInfo GetNeutralResourcesLanguage(Assembly a)
		{
			object[] customAttributes = a.GetCustomAttributes(typeof(NeutralResourcesLanguageAttribute), false);
			if (customAttributes.Length == 0)
			{
				return CultureInfo.InvariantCulture;
			}
			NeutralResourcesLanguageAttribute neutralResourcesLanguageAttribute = (NeutralResourcesLanguageAttribute)customAttributes[0];
			return new CultureInfo(neutralResourcesLanguageAttribute.CultureName);
		}

		// Token: 0x06002804 RID: 10244 RVA: 0x0008EAC8 File Offset: 0x0008CCC8
		protected static Version GetSatelliteContractVersion(Assembly a)
		{
			object[] customAttributes = a.GetCustomAttributes(typeof(SatelliteContractVersionAttribute), false);
			if (customAttributes.Length == 0)
			{
				return null;
			}
			SatelliteContractVersionAttribute satelliteContractVersionAttribute = (SatelliteContractVersionAttribute)customAttributes[0];
			return new Version(satelliteContractVersionAttribute.Version);
		}

		// Token: 0x17000723 RID: 1827
		// (get) Token: 0x06002805 RID: 10245 RVA: 0x0008EB08 File Offset: 0x0008CD08
		// (set) Token: 0x06002806 RID: 10246 RVA: 0x0008EB10 File Offset: 0x0008CD10
		[MonoTODO("the property exists but is not respected")]
		protected UltimateResourceFallbackLocation FallbackLocation
		{
			get
			{
				return this.fallbackLocation;
			}
			set
			{
				this.fallbackLocation = value;
			}
		}

		// Token: 0x06002807 RID: 10247 RVA: 0x0008EB1C File Offset: 0x0008CD1C
		private MissingManifestResourceException AssemblyResourceMissing(string fileName)
		{
			AssemblyName assemblyName = (this.MainAssembly == null) ? null : this.MainAssembly.GetName();
			string manifestResourceName = this.GetManifestResourceName(fileName);
			string message = string.Format("Could not find any resources appropriate for the specified culture or the neutral culture.  Make sure \"{0}\" was correctly embedded or linked into assembly \"{1}\" at compile time, or that all the satellite assemblies required are loadable and fully signed.", manifestResourceName, (assemblyName == null) ? string.Empty : assemblyName.Name);
			throw new MissingManifestResourceException(message);
		}

		// Token: 0x06002808 RID: 10248 RVA: 0x0008EB78 File Offset: 0x0008CD78
		private string GetManifestResourceName(string fn)
		{
			string result;
			if (this.resourceSource != null)
			{
				if (this.resourceSource.Namespace != null && this.resourceSource.Namespace.Length > 0)
				{
					result = this.resourceSource.Namespace + "." + fn;
				}
				else
				{
					result = fn;
				}
			}
			else
			{
				result = fn;
			}
			return result;
		}

		// Token: 0x04001008 RID: 4104
		private static Hashtable ResourceCache = new Hashtable();

		// Token: 0x04001009 RID: 4105
		private static Hashtable NonExistent = Hashtable.Synchronized(new Hashtable());

		// Token: 0x0400100A RID: 4106
		public static readonly int HeaderVersionNumber = 1;

		// Token: 0x0400100B RID: 4107
		public static readonly int MagicNumber = -1091581234;

		// Token: 0x0400100C RID: 4108
		protected string BaseNameField;

		// Token: 0x0400100D RID: 4109
		protected Assembly MainAssembly;

		// Token: 0x0400100E RID: 4110
		protected Hashtable ResourceSets;

		// Token: 0x0400100F RID: 4111
		private bool ignoreCase;

		// Token: 0x04001010 RID: 4112
		private Type resourceSource;

		// Token: 0x04001011 RID: 4113
		private Type resourceSetType = typeof(RuntimeResourceSet);

		// Token: 0x04001012 RID: 4114
		private string resourceDir;

		// Token: 0x04001013 RID: 4115
		private CultureInfo neutral_culture;

		// Token: 0x04001014 RID: 4116
		private UltimateResourceFallbackLocation fallbackLocation;
	}
}
