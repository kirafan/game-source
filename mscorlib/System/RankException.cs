﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200016D RID: 365
	[ComVisible(true)]
	[Serializable]
	public class RankException : SystemException
	{
		// Token: 0x06001361 RID: 4961 RVA: 0x0004D914 File Offset: 0x0004BB14
		public RankException() : base(Locale.GetText("Two arrays must have the same number of dimensions."))
		{
			base.HResult = -2146233065;
		}

		// Token: 0x06001362 RID: 4962 RVA: 0x0004D934 File Offset: 0x0004BB34
		public RankException(string message) : base(message)
		{
			base.HResult = -2146233065;
		}

		// Token: 0x06001363 RID: 4963 RVA: 0x0004D948 File Offset: 0x0004BB48
		public RankException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233065;
		}

		// Token: 0x06001364 RID: 4964 RVA: 0x0004D960 File Offset: 0x0004BB60
		protected RankException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040005A4 RID: 1444
		private const int Result = -2146233065;
	}
}
