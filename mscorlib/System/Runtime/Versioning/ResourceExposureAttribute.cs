﻿using System;
using System.Diagnostics;

namespace System.Runtime.Versioning
{
	// Token: 0x0200052E RID: 1326
	[Conditional("RESOURCE_ANNOTATION_WORK")]
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field, Inherited = false)]
	public sealed class ResourceExposureAttribute : Attribute
	{
		// Token: 0x06003452 RID: 13394 RVA: 0x000ABE10 File Offset: 0x000AA010
		public ResourceExposureAttribute(ResourceScope exposureLevel)
		{
			this.exposure = exposureLevel;
		}

		// Token: 0x170009D0 RID: 2512
		// (get) Token: 0x06003453 RID: 13395 RVA: 0x000ABE20 File Offset: 0x000AA020
		public ResourceScope ResourceExposureLevel
		{
			get
			{
				return this.exposure;
			}
		}

		// Token: 0x0400160E RID: 5646
		private ResourceScope exposure;
	}
}
