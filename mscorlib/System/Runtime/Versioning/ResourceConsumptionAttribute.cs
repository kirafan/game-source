﻿using System;
using System.Diagnostics;

namespace System.Runtime.Versioning
{
	// Token: 0x0200052D RID: 1325
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Property, Inherited = false)]
	[Conditional("RESOURCE_ANNOTATION_WORK")]
	public sealed class ResourceConsumptionAttribute : Attribute
	{
		// Token: 0x0600344E RID: 13390 RVA: 0x000ABDD0 File Offset: 0x000A9FD0
		public ResourceConsumptionAttribute(ResourceScope resourceScope)
		{
			this.resource = resourceScope;
			this.consumption = resourceScope;
		}

		// Token: 0x0600344F RID: 13391 RVA: 0x000ABDE8 File Offset: 0x000A9FE8
		public ResourceConsumptionAttribute(ResourceScope resourceScope, ResourceScope consumptionScope)
		{
			this.resource = resourceScope;
			this.consumption = consumptionScope;
		}

		// Token: 0x170009CE RID: 2510
		// (get) Token: 0x06003450 RID: 13392 RVA: 0x000ABE00 File Offset: 0x000AA000
		public ResourceScope ConsumptionScope
		{
			get
			{
				return this.consumption;
			}
		}

		// Token: 0x170009CF RID: 2511
		// (get) Token: 0x06003451 RID: 13393 RVA: 0x000ABE08 File Offset: 0x000AA008
		public ResourceScope ResourceScope
		{
			get
			{
				return this.resource;
			}
		}

		// Token: 0x0400160C RID: 5644
		private ResourceScope resource;

		// Token: 0x0400160D RID: 5645
		private ResourceScope consumption;
	}
}
