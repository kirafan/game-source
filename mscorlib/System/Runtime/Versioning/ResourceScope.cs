﻿using System;

namespace System.Runtime.Versioning
{
	// Token: 0x0200052F RID: 1327
	[Flags]
	public enum ResourceScope
	{
		// Token: 0x04001610 RID: 5648
		None = 0,
		// Token: 0x04001611 RID: 5649
		Machine = 1,
		// Token: 0x04001612 RID: 5650
		Process = 2,
		// Token: 0x04001613 RID: 5651
		AppDomain = 4,
		// Token: 0x04001614 RID: 5652
		Library = 8,
		// Token: 0x04001615 RID: 5653
		Private = 16,
		// Token: 0x04001616 RID: 5654
		Assembly = 32
	}
}
