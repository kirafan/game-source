﻿using System;

namespace System.Runtime.Versioning
{
	// Token: 0x02000530 RID: 1328
	public static class VersioningHelper
	{
		// Token: 0x06003454 RID: 13396 RVA: 0x000ABE28 File Offset: 0x000AA028
		private static int GetDomainId()
		{
			return AppDomain.CurrentDomain.Id;
		}

		// Token: 0x06003455 RID: 13397 RVA: 0x000ABE34 File Offset: 0x000AA034
		private static int GetProcessId()
		{
			return 0;
		}

		// Token: 0x06003456 RID: 13398 RVA: 0x000ABE38 File Offset: 0x000AA038
		private static string SafeName(string name, bool process, bool appdomain)
		{
			if (process && appdomain)
			{
				return string.Concat(new string[]
				{
					name,
					"_",
					VersioningHelper.GetProcessId().ToString(),
					"_",
					VersioningHelper.GetDomainId().ToString()
				});
			}
			if (process)
			{
				return name + "_" + VersioningHelper.GetProcessId().ToString();
			}
			if (appdomain)
			{
				return name + "_" + VersioningHelper.GetDomainId().ToString();
			}
			return name;
		}

		// Token: 0x06003457 RID: 13399 RVA: 0x000ABED0 File Offset: 0x000AA0D0
		private static string ConvertFromMachine(string name, ResourceScope to, Type type)
		{
			switch (to)
			{
			case ResourceScope.Machine:
				return VersioningHelper.SafeName(name, false, false);
			case ResourceScope.Process:
				return VersioningHelper.SafeName(name, true, false);
			case ResourceScope.AppDomain:
				return VersioningHelper.SafeName(name, true, true);
			}
			throw new ArgumentException("to");
		}

		// Token: 0x06003458 RID: 13400 RVA: 0x000ABF24 File Offset: 0x000AA124
		private static string ConvertFromProcess(string name, ResourceScope to, Type type)
		{
			if (to < ResourceScope.Process || to >= ResourceScope.Private)
			{
				throw new ArgumentException("to");
			}
			bool appdomain = (to & ResourceScope.AppDomain) == ResourceScope.AppDomain;
			return VersioningHelper.SafeName(name, false, appdomain);
		}

		// Token: 0x06003459 RID: 13401 RVA: 0x000ABF5C File Offset: 0x000AA15C
		private static string ConvertFromAppDomain(string name, ResourceScope to, Type type)
		{
			if (to < ResourceScope.AppDomain || to >= ResourceScope.Private)
			{
				throw new ArgumentException("to");
			}
			return VersioningHelper.SafeName(name, false, false);
		}

		// Token: 0x0600345A RID: 13402 RVA: 0x000ABF8C File Offset: 0x000AA18C
		[MonoTODO("process id is always 0")]
		public static string MakeVersionSafeName(string name, ResourceScope from, ResourceScope to)
		{
			return VersioningHelper.MakeVersionSafeName(name, from, to, null);
		}

		// Token: 0x0600345B RID: 13403 RVA: 0x000ABF98 File Offset: 0x000AA198
		[MonoTODO("type?")]
		public static string MakeVersionSafeName(string name, ResourceScope from, ResourceScope to, Type type)
		{
			if ((from & ResourceScope.Private) != ResourceScope.None)
			{
				to &= ~(ResourceScope.Private | ResourceScope.Assembly);
			}
			else if ((from & ResourceScope.Assembly) != ResourceScope.None)
			{
				to &= ~ResourceScope.Assembly;
			}
			string name2 = (name != null) ? name : string.Empty;
			switch (from)
			{
			case ResourceScope.Machine:
				break;
			case ResourceScope.Process:
				goto IL_8F;
			default:
				switch (from)
				{
				case ResourceScope.Machine | ResourceScope.Private:
					break;
				case ResourceScope.Process | ResourceScope.Private:
					goto IL_8F;
				default:
					switch (from)
					{
					case ResourceScope.Machine | ResourceScope.Assembly:
						goto IL_86;
					case ResourceScope.Process | ResourceScope.Assembly:
						goto IL_8F;
					case ResourceScope.AppDomain | ResourceScope.Assembly:
						goto IL_98;
					}
					throw new ArgumentException("from");
				case ResourceScope.AppDomain | ResourceScope.Private:
					goto IL_98;
				}
				break;
			case ResourceScope.AppDomain:
				goto IL_98;
			}
			IL_86:
			return VersioningHelper.ConvertFromMachine(name2, to, type);
			IL_8F:
			return VersioningHelper.ConvertFromProcess(name2, to, type);
			IL_98:
			return VersioningHelper.ConvertFromAppDomain(name2, to, type);
		}
	}
}
