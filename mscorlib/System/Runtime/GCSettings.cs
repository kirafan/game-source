﻿using System;
using System.Runtime.ConstrainedExecution;

namespace System.Runtime
{
	// Token: 0x0200031E RID: 798
	public static class GCSettings
	{
		// Token: 0x17000742 RID: 1858
		// (get) Token: 0x0600288D RID: 10381 RVA: 0x00091DA4 File Offset: 0x0008FFA4
		[MonoTODO("Always returns false")]
		public static bool IsServerGC
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000743 RID: 1859
		// (get) Token: 0x0600288E RID: 10382 RVA: 0x00091DA8 File Offset: 0x0008FFA8
		// (set) Token: 0x0600288F RID: 10383 RVA: 0x00091DAC File Offset: 0x0008FFAC
		[MonoTODO("Always returns GCLatencyMode.Interactive and ignores set (.NET 2.0 SP1 member)")]
		public static GCLatencyMode LatencyMode
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return GCLatencyMode.Interactive;
			}
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			set
			{
			}
		}
	}
}
