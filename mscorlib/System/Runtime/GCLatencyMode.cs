﻿using System;

namespace System.Runtime
{
	// Token: 0x0200031D RID: 797
	[Serializable]
	public enum GCLatencyMode
	{
		// Token: 0x04001082 RID: 4226
		Batch,
		// Token: 0x04001083 RID: 4227
		Interactive,
		// Token: 0x04001084 RID: 4228
		LowLatency
	}
}
