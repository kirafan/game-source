﻿using System;

namespace System.Runtime.ConstrainedExecution
{
	// Token: 0x0200034A RID: 842
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, Inherited = false)]
	public sealed class PrePrepareMethodAttribute : Attribute
	{
	}
}
