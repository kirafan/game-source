﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.ConstrainedExecution
{
	// Token: 0x02000347 RID: 839
	[ComVisible(true)]
	public abstract class CriticalFinalizerObject
	{
		// Token: 0x060028B6 RID: 10422 RVA: 0x00091F5C File Offset: 0x0009015C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		protected CriticalFinalizerObject()
		{
		}

		// Token: 0x060028B7 RID: 10423 RVA: 0x00091F64 File Offset: 0x00090164
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		~CriticalFinalizerObject()
		{
		}
	}
}
