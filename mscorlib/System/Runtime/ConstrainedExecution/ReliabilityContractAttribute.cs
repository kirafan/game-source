﻿using System;

namespace System.Runtime.ConstrainedExecution
{
	// Token: 0x0200034B RID: 843
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method | AttributeTargets.Interface, Inherited = false)]
	public sealed class ReliabilityContractAttribute : Attribute
	{
		// Token: 0x060028B9 RID: 10425 RVA: 0x00091FA4 File Offset: 0x000901A4
		public ReliabilityContractAttribute(Consistency consistencyGuarantee, Cer cer)
		{
			this.consistency = consistencyGuarantee;
			this.cer = cer;
		}

		// Token: 0x1700074F RID: 1871
		// (get) Token: 0x060028BA RID: 10426 RVA: 0x00091FBC File Offset: 0x000901BC
		public Cer Cer
		{
			get
			{
				return this.cer;
			}
		}

		// Token: 0x17000750 RID: 1872
		// (get) Token: 0x060028BB RID: 10427 RVA: 0x00091FC4 File Offset: 0x000901C4
		public Consistency ConsistencyGuarantee
		{
			get
			{
				return this.consistency;
			}
		}

		// Token: 0x040010A8 RID: 4264
		private Consistency consistency;

		// Token: 0x040010A9 RID: 4265
		private Cer cer;
	}
}
