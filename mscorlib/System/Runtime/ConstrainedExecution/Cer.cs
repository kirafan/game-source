﻿using System;

namespace System.Runtime.ConstrainedExecution
{
	// Token: 0x02000348 RID: 840
	[Serializable]
	public enum Cer
	{
		// Token: 0x040010A0 RID: 4256
		None,
		// Token: 0x040010A1 RID: 4257
		MayFail,
		// Token: 0x040010A2 RID: 4258
		Success
	}
}
