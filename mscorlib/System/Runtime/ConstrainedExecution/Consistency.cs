﻿using System;

namespace System.Runtime.ConstrainedExecution
{
	// Token: 0x02000349 RID: 841
	[Serializable]
	public enum Consistency
	{
		// Token: 0x040010A4 RID: 4260
		MayCorruptAppDomain = 1,
		// Token: 0x040010A5 RID: 4261
		MayCorruptInstance,
		// Token: 0x040010A6 RID: 4262
		MayCorruptProcess = 0,
		// Token: 0x040010A7 RID: 4263
		WillNotCorruptState = 3
	}
}
