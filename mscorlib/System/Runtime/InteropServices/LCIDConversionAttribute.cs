﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003A9 RID: 937
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	[ComVisible(true)]
	public sealed class LCIDConversionAttribute : Attribute
	{
		// Token: 0x06002A9E RID: 10910 RVA: 0x00092B4C File Offset: 0x00090D4C
		public LCIDConversionAttribute(int lcid)
		{
			this.id = lcid;
		}

		// Token: 0x170007D8 RID: 2008
		// (get) Token: 0x06002A9F RID: 10911 RVA: 0x00092B5C File Offset: 0x00090D5C
		public int Value
		{
			get
			{
				return this.id;
			}
		}

		// Token: 0x04001156 RID: 4438
		private int id;
	}
}
