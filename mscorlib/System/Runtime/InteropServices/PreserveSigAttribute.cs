﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B0 RID: 944
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	[ComVisible(true)]
	public sealed class PreserveSigAttribute : Attribute
	{
	}
}
