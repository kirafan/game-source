﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003A6 RID: 934
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Interface, Inherited = false)]
	public sealed class InterfaceTypeAttribute : Attribute
	{
		// Token: 0x06002A93 RID: 10899 RVA: 0x00092A74 File Offset: 0x00090C74
		public InterfaceTypeAttribute(ComInterfaceType interfaceType)
		{
			this.intType = interfaceType;
		}

		// Token: 0x06002A94 RID: 10900 RVA: 0x00092A84 File Offset: 0x00090C84
		public InterfaceTypeAttribute(short interfaceType)
		{
			this.intType = (ComInterfaceType)interfaceType;
		}

		// Token: 0x170007D7 RID: 2007
		// (get) Token: 0x06002A95 RID: 10901 RVA: 0x00092A94 File Offset: 0x00090C94
		public ComInterfaceType Value
		{
			get
			{
				return this.intType;
			}
		}

		// Token: 0x04001153 RID: 4435
		private ComInterfaceType intType;
	}
}
