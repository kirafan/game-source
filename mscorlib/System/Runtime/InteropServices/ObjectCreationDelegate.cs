﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020006F4 RID: 1780
	// (Invoke) Token: 0x060043C4 RID: 17348
	[ComVisible(true)]
	public delegate IntPtr ObjectCreationDelegate(IntPtr aggregator);
}
