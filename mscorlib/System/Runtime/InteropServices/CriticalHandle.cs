﻿using System;
using System.Runtime.ConstrainedExecution;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000382 RID: 898
	public abstract class CriticalHandle : CriticalFinalizerObject, IDisposable
	{
		// Token: 0x06002A3B RID: 10811 RVA: 0x00092530 File Offset: 0x00090730
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		protected CriticalHandle(IntPtr invalidHandleValue)
		{
			this.handle = invalidHandleValue;
		}

		// Token: 0x06002A3C RID: 10812 RVA: 0x00092540 File Offset: 0x00090740
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		~CriticalHandle()
		{
			this.Dispose(false);
		}

		// Token: 0x06002A3D RID: 10813 RVA: 0x0009257C File Offset: 0x0009077C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void Close()
		{
			this.Dispose(true);
		}

		// Token: 0x06002A3E RID: 10814 RVA: 0x00092588 File Offset: 0x00090788
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x06002A3F RID: 10815 RVA: 0x00092594 File Offset: 0x00090794
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		protected virtual void Dispose(bool disposing)
		{
			if (this._disposed)
			{
				return;
			}
			this._disposed = true;
			if (this.IsInvalid)
			{
				return;
			}
			if (disposing && !this.IsInvalid && !this.ReleaseHandle())
			{
				GC.SuppressFinalize(this);
			}
		}

		// Token: 0x06002A40 RID: 10816
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		protected abstract bool ReleaseHandle();

		// Token: 0x06002A41 RID: 10817 RVA: 0x000925E8 File Offset: 0x000907E8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		protected void SetHandle(IntPtr handle)
		{
			this.handle = handle;
		}

		// Token: 0x06002A42 RID: 10818 RVA: 0x000925F4 File Offset: 0x000907F4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void SetHandleAsInvalid()
		{
			this._disposed = true;
		}

		// Token: 0x170007CA RID: 1994
		// (get) Token: 0x06002A43 RID: 10819 RVA: 0x00092600 File Offset: 0x00090800
		public bool IsClosed
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return this._disposed;
			}
		}

		// Token: 0x170007CB RID: 1995
		// (get) Token: 0x06002A44 RID: 10820
		public abstract bool IsInvalid { [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)] get; }

		// Token: 0x040010EB RID: 4331
		protected IntPtr handle;

		// Token: 0x040010EC RID: 4332
		private bool _disposed;
	}
}
