﻿using System;
using System.Threading;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000367 RID: 871
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("C281C7F1-4AA9-3517-961A-463CFED57E75")]
	[CLSCompliant(false)]
	[ComVisible(true)]
	[TypeLibImportClass(typeof(Thread))]
	public interface _Thread
	{
		// Token: 0x06002A06 RID: 10758
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06002A07 RID: 10759
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x06002A08 RID: 10760
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x06002A09 RID: 10761
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
