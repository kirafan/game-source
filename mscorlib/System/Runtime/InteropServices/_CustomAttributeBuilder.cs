﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000354 RID: 852
	[Guid("BE9ACCE8-AAFF-3B91-81AE-8211663F5CAD")]
	[TypeLibImportClass(typeof(CustomAttributeBuilder))]
	[CLSCompliant(false)]
	[ComVisible(true)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _CustomAttributeBuilder
	{
		// Token: 0x06002928 RID: 10536
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06002929 RID: 10537
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x0600292A RID: 10538
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x0600292B RID: 10539
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
