﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000380 RID: 896
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	[ComVisible(true)]
	public sealed class ComUnregisterFunctionAttribute : Attribute
	{
	}
}
