﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200036C RID: 876
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Interface, Inherited = false)]
	public sealed class AutomationProxyAttribute : Attribute
	{
		// Token: 0x06002A17 RID: 10775 RVA: 0x00092288 File Offset: 0x00090488
		public AutomationProxyAttribute(bool val)
		{
			this.val = val;
		}

		// Token: 0x170007BD RID: 1981
		// (get) Token: 0x06002A18 RID: 10776 RVA: 0x00092298 File Offset: 0x00090498
		public bool Value
		{
			get
			{
				return this.val;
			}
		}

		// Token: 0x040010B2 RID: 4274
		private bool val;
	}
}
