﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200036A RID: 874
	[ComVisible(true)]
	[Serializable]
	public struct ArrayWithOffset
	{
		// Token: 0x06002A0F RID: 10767 RVA: 0x000921C0 File Offset: 0x000903C0
		public ArrayWithOffset(object array, int offset)
		{
			this.array = array;
			this.offset = offset;
		}

		// Token: 0x06002A10 RID: 10768 RVA: 0x000921D0 File Offset: 0x000903D0
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (!(obj is ArrayWithOffset))
			{
				return false;
			}
			ArrayWithOffset arrayWithOffset = (ArrayWithOffset)obj;
			return arrayWithOffset.array == this.array && arrayWithOffset.offset == this.offset;
		}

		// Token: 0x06002A11 RID: 10769 RVA: 0x00092220 File Offset: 0x00090420
		public bool Equals(ArrayWithOffset obj)
		{
			return obj.array == this.array && obj.offset == this.offset;
		}

		// Token: 0x06002A12 RID: 10770 RVA: 0x00092254 File Offset: 0x00090454
		public override int GetHashCode()
		{
			return this.offset;
		}

		// Token: 0x06002A13 RID: 10771 RVA: 0x0009225C File Offset: 0x0009045C
		public object GetArray()
		{
			return this.array;
		}

		// Token: 0x06002A14 RID: 10772 RVA: 0x00092264 File Offset: 0x00090464
		public int GetOffset()
		{
			return this.offset;
		}

		// Token: 0x06002A15 RID: 10773 RVA: 0x0009226C File Offset: 0x0009046C
		public static bool operator ==(ArrayWithOffset a, ArrayWithOffset b)
		{
			return a.Equals(b);
		}

		// Token: 0x06002A16 RID: 10774 RVA: 0x00092278 File Offset: 0x00090478
		public static bool operator !=(ArrayWithOffset a, ArrayWithOffset b)
		{
			return !a.Equals(b);
		}

		// Token: 0x040010AD RID: 4269
		private object array;

		// Token: 0x040010AE RID: 4270
		private int offset;
	}
}
