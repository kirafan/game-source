﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D5 RID: 981
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Obsolete]
	[Guid("00020404-0000-0000-c000-000000000046")]
	[ComImport]
	public interface UCOMIEnumVARIANT
	{
		// Token: 0x06002BB3 RID: 11187
		[PreserveSig]
		int Next(int celt, int rgvar, int pceltFetched);

		// Token: 0x06002BB4 RID: 11188
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002BB5 RID: 11189
		[PreserveSig]
		int Reset();

		// Token: 0x06002BB6 RID: 11190
		void Clone(int ppenum);
	}
}
