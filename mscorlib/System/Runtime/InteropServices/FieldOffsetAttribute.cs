﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000059 RID: 89
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	[ComVisible(true)]
	public sealed class FieldOffsetAttribute : Attribute
	{
		// Token: 0x0600068D RID: 1677 RVA: 0x00014D7C File Offset: 0x00012F7C
		public FieldOffsetAttribute(int offset)
		{
			this.val = offset;
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x0600068E RID: 1678 RVA: 0x00014D8C File Offset: 0x00012F8C
		public int Value
		{
			get
			{
				return this.val;
			}
		}

		// Token: 0x040000B2 RID: 178
		private int val;
	}
}
