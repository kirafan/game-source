﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B7 RID: 951
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[Guid("475e398f-8afa-43a7-a3be-f4ef8d6787c9")]
	public class RegistrationServices : IRegistrationServices
	{
		// Token: 0x06002B5A RID: 11098 RVA: 0x000938F4 File Offset: 0x00091AF4
		[MonoTODO("implement")]
		public virtual Guid GetManagedCategoryGuid()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B5B RID: 11099 RVA: 0x000938FC File Offset: 0x00091AFC
		[MonoTODO("implement")]
		public virtual string GetProgIdForType(Type type)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B5C RID: 11100 RVA: 0x00093904 File Offset: 0x00091B04
		[MonoTODO("implement")]
		public virtual Type[] GetRegistrableTypesInAssembly(Assembly assembly)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B5D RID: 11101 RVA: 0x0009390C File Offset: 0x00091B0C
		[MonoTODO("implement")]
		public virtual bool RegisterAssembly(Assembly assembly, AssemblyRegistrationFlags flags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B5E RID: 11102 RVA: 0x00093914 File Offset: 0x00091B14
		[MonoTODO("implement")]
		public virtual void RegisterTypeForComClients(Type type, ref Guid g)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B5F RID: 11103 RVA: 0x0009391C File Offset: 0x00091B1C
		[MonoTODO("implement")]
		public virtual bool TypeRepresentsComType(Type type)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B60 RID: 11104 RVA: 0x00093924 File Offset: 0x00091B24
		[MonoTODO("implement")]
		public virtual bool TypeRequiresRegistration(Type type)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B61 RID: 11105 RVA: 0x0009392C File Offset: 0x00091B2C
		[MonoTODO("implement")]
		public virtual bool UnregisterAssembly(Assembly assembly)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B62 RID: 11106 RVA: 0x00093934 File Offset: 0x00091B34
		[MonoTODO("implement")]
		[ComVisible(false)]
		public virtual int RegisterTypeForComClients(Type type, RegistrationClassContext classContext, RegistrationConnectionType flags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B63 RID: 11107 RVA: 0x0009393C File Offset: 0x00091B3C
		[ComVisible(false)]
		[MonoTODO("implement")]
		public virtual void UnregisterTypeForComClients(int cookie)
		{
			throw new NotImplementedException();
		}
	}
}
