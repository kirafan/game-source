﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200038A RID: 906
	[ComVisible(true)]
	[Serializable]
	public sealed class ErrorWrapper
	{
		// Token: 0x06002A4C RID: 10828 RVA: 0x00092698 File Offset: 0x00090898
		public ErrorWrapper(Exception e)
		{
			this.errorCode = Marshal.GetHRForException(e);
		}

		// Token: 0x06002A4D RID: 10829 RVA: 0x000926AC File Offset: 0x000908AC
		public ErrorWrapper(int errorCode)
		{
			this.errorCode = errorCode;
		}

		// Token: 0x06002A4E RID: 10830 RVA: 0x000926BC File Offset: 0x000908BC
		public ErrorWrapper(object errorCode)
		{
			if (errorCode.GetType() != typeof(int))
			{
				throw new ArgumentException("errorCode has to be an int type");
			}
			this.errorCode = (int)errorCode;
		}

		// Token: 0x170007CF RID: 1999
		// (get) Token: 0x06002A4F RID: 10831 RVA: 0x000926FC File Offset: 0x000908FC
		public int ErrorCode
		{
			get
			{
				return this.errorCode;
			}
		}

		// Token: 0x040010FF RID: 4351
		private int errorCode;
	}
}
