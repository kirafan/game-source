﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200038D RID: 909
	[ComVisible(true)]
	public sealed class ExtensibleClassFactory
	{
		// Token: 0x06002A50 RID: 10832 RVA: 0x00092704 File Offset: 0x00090904
		private ExtensibleClassFactory()
		{
		}

		// Token: 0x06002A52 RID: 10834 RVA: 0x00092718 File Offset: 0x00090918
		internal static ObjectCreationDelegate GetObjectCreationCallback(Type t)
		{
			return ExtensibleClassFactory.hashtable[t] as ObjectCreationDelegate;
		}

		// Token: 0x06002A53 RID: 10835 RVA: 0x0009272C File Offset: 0x0009092C
		public static void RegisterObjectCreationCallback(ObjectCreationDelegate callback)
		{
			int i = 1;
			StackTrace stackTrace = new StackTrace(false);
			while (i < stackTrace.FrameCount)
			{
				StackFrame frame = stackTrace.GetFrame(i);
				MethodBase method = frame.GetMethod();
				if (method.MemberType == MemberTypes.Constructor && method.IsStatic)
				{
					ExtensibleClassFactory.hashtable.Add(method.DeclaringType, callback);
					return;
				}
				i++;
			}
			throw new InvalidOperationException("RegisterObjectCreationCallback must be called from .cctor of class derived from ComImport type.");
		}

		// Token: 0x0400110C RID: 4364
		private static Hashtable hashtable = new Hashtable();
	}
}
