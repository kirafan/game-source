﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000377 RID: 887
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	[ComVisible(true)]
	public sealed class ComCompatibleVersionAttribute : Attribute
	{
		// Token: 0x06002A22 RID: 10786 RVA: 0x00092310 File Offset: 0x00090510
		public ComCompatibleVersionAttribute(int major, int minor, int build, int revision)
		{
			this.major = major;
			this.minor = minor;
			this.build = build;
			this.revision = revision;
		}

		// Token: 0x170007C2 RID: 1986
		// (get) Token: 0x06002A23 RID: 10787 RVA: 0x00092338 File Offset: 0x00090538
		public int MajorVersion
		{
			get
			{
				return this.major;
			}
		}

		// Token: 0x170007C3 RID: 1987
		// (get) Token: 0x06002A24 RID: 10788 RVA: 0x00092340 File Offset: 0x00090540
		public int MinorVersion
		{
			get
			{
				return this.minor;
			}
		}

		// Token: 0x170007C4 RID: 1988
		// (get) Token: 0x06002A25 RID: 10789 RVA: 0x00092348 File Offset: 0x00090548
		public int BuildNumber
		{
			get
			{
				return this.build;
			}
		}

		// Token: 0x170007C5 RID: 1989
		// (get) Token: 0x06002A26 RID: 10790 RVA: 0x00092350 File Offset: 0x00090550
		public int RevisionNumber
		{
			get
			{
				return this.revision;
			}
		}

		// Token: 0x040010D9 RID: 4313
		private int major;

		// Token: 0x040010DA RID: 4314
		private int minor;

		// Token: 0x040010DB RID: 4315
		private int build;

		// Token: 0x040010DC RID: 4316
		private int revision;
	}
}
