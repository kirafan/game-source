﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D3 RID: 979
	[Obsolete]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00000102-0000-0000-c000-000000000046")]
	[ComImport]
	public interface UCOMIEnumMoniker
	{
		// Token: 0x06002BAB RID: 11179
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 0)] [Out] UCOMIMoniker[] rgelt, out int pceltFetched);

		// Token: 0x06002BAC RID: 11180
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002BAD RID: 11181
		[PreserveSig]
		int Reset();

		// Token: 0x06002BAE RID: 11182
		void Clone(out UCOMIEnumMoniker ppenum);
	}
}
