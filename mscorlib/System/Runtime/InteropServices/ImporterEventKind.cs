﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003A5 RID: 933
	[ComVisible(true)]
	[Serializable]
	public enum ImporterEventKind
	{
		// Token: 0x04001150 RID: 4432
		NOTIF_TYPECONVERTED,
		// Token: 0x04001151 RID: 4433
		NOTIF_CONVERTWARNING,
		// Token: 0x04001152 RID: 4434
		ERROR_REFTOINVALIDTYPELIB
	}
}
