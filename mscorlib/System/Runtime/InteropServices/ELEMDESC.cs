﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000388 RID: 904
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct ELEMDESC
	{
		// Token: 0x040010FB RID: 4347
		public TYPEDESC tdesc;

		// Token: 0x040010FC RID: 4348
		public ELEMDESC.DESCUNION desc;

		// Token: 0x02000389 RID: 905
		[ComVisible(false)]
		[StructLayout(LayoutKind.Explicit, CharSet = CharSet.Unicode)]
		public struct DESCUNION
		{
			// Token: 0x040010FD RID: 4349
			[FieldOffset(0)]
			public IDLDESC idldesc;

			// Token: 0x040010FE RID: 4350
			[FieldOffset(0)]
			public PARAMDESC paramdesc;
		}
	}
}
