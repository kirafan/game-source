﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200039A RID: 922
	[ComVisible(true)]
	[Obsolete]
	[Serializable]
	public enum IDispatchImplType
	{
		// Token: 0x04001139 RID: 4409
		SystemDefinedImpl,
		// Token: 0x0400113A RID: 4410
		InternalImpl,
		// Token: 0x0400113B RID: 4411
		CompatibleImpl
	}
}
