﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000397 RID: 919
	[ComVisible(true)]
	public interface ICustomFactory
	{
		// Token: 0x06002A77 RID: 10871
		MarshalByRefObject CreateInstance(Type serverType);
	}
}
