﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200037A RID: 890
	[ComVisible(true)]
	[Serializable]
	public class COMException : ExternalException
	{
		// Token: 0x06002A2A RID: 10794 RVA: 0x00092378 File Offset: 0x00090578
		public COMException()
		{
		}

		// Token: 0x06002A2B RID: 10795 RVA: 0x00092380 File Offset: 0x00090580
		public COMException(string message) : base(message)
		{
		}

		// Token: 0x06002A2C RID: 10796 RVA: 0x0009238C File Offset: 0x0009058C
		public COMException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06002A2D RID: 10797 RVA: 0x00092398 File Offset: 0x00090598
		public COMException(string message, int errorCode) : base(message, errorCode)
		{
		}

		// Token: 0x06002A2E RID: 10798 RVA: 0x000923A4 File Offset: 0x000905A4
		protected COMException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06002A2F RID: 10799 RVA: 0x000923B0 File Offset: 0x000905B0
		public override string ToString()
		{
			return string.Format("{0} (0x{1:x}): {2} {3}{4}{5}", new object[]
			{
				this.GetType(),
				base.HResult,
				this.Message,
				(this.InnerException != null) ? this.InnerException.ToString() : string.Empty,
				Environment.NewLine,
				(this.StackTrace == null) ? string.Empty : this.StackTrace
			});
		}
	}
}
