﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003BD RID: 957
	[Obsolete]
	[Serializable]
	public enum SYSKIND
	{
		// Token: 0x0400119C RID: 4508
		SYS_WIN16,
		// Token: 0x0400119D RID: 4509
		SYS_WIN32,
		// Token: 0x0400119E RID: 4510
		SYS_MAC
	}
}
