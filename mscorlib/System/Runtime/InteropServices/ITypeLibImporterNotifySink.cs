﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003A3 RID: 931
	[ComVisible(true)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("f1c3bf76-c3e4-11d3-88e7-00902754c43a")]
	public interface ITypeLibImporterNotifySink
	{
		// Token: 0x06002A8F RID: 10895
		void ReportEvent(ImporterEventKind eventKind, int eventCode, string eventMsg);

		// Token: 0x06002A90 RID: 10896
		Assembly ResolveRef([MarshalAs(UnmanagedType.Interface)] object typeLib);
	}
}
