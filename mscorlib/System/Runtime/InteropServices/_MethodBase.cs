﻿using System;
using System.Globalization;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200035C RID: 860
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[CLSCompliant(false)]
	[ComVisible(true)]
	[TypeLibImportClass(typeof(MethodBase))]
	[Guid("6240837A-707F-3181-8E98-A36AE086766B")]
	public interface _MethodBase
	{
		// Token: 0x0600297E RID: 10622
		bool Equals(object other);

		// Token: 0x0600297F RID: 10623
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x06002980 RID: 10624
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x06002981 RID: 10625
		int GetHashCode();

		// Token: 0x06002982 RID: 10626
		MethodImplAttributes GetMethodImplementationFlags();

		// Token: 0x06002983 RID: 10627
		ParameterInfo[] GetParameters();

		// Token: 0x06002984 RID: 10628
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06002985 RID: 10629
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x06002986 RID: 10630
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x06002987 RID: 10631
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);

		// Token: 0x06002988 RID: 10632
		Type GetType();

		// Token: 0x06002989 RID: 10633
		object Invoke(object obj, object[] parameters);

		// Token: 0x0600298A RID: 10634
		object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture);

		// Token: 0x0600298B RID: 10635
		bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x0600298C RID: 10636
		string ToString();

		// Token: 0x1700078A RID: 1930
		// (get) Token: 0x0600298D RID: 10637
		MethodAttributes Attributes { get; }

		// Token: 0x1700078B RID: 1931
		// (get) Token: 0x0600298E RID: 10638
		CallingConventions CallingConvention { get; }

		// Token: 0x1700078C RID: 1932
		// (get) Token: 0x0600298F RID: 10639
		Type DeclaringType { get; }

		// Token: 0x1700078D RID: 1933
		// (get) Token: 0x06002990 RID: 10640
		bool IsAbstract { get; }

		// Token: 0x1700078E RID: 1934
		// (get) Token: 0x06002991 RID: 10641
		bool IsAssembly { get; }

		// Token: 0x1700078F RID: 1935
		// (get) Token: 0x06002992 RID: 10642
		bool IsConstructor { get; }

		// Token: 0x17000790 RID: 1936
		// (get) Token: 0x06002993 RID: 10643
		bool IsFamily { get; }

		// Token: 0x17000791 RID: 1937
		// (get) Token: 0x06002994 RID: 10644
		bool IsFamilyAndAssembly { get; }

		// Token: 0x17000792 RID: 1938
		// (get) Token: 0x06002995 RID: 10645
		bool IsFamilyOrAssembly { get; }

		// Token: 0x17000793 RID: 1939
		// (get) Token: 0x06002996 RID: 10646
		bool IsFinal { get; }

		// Token: 0x17000794 RID: 1940
		// (get) Token: 0x06002997 RID: 10647
		bool IsHideBySig { get; }

		// Token: 0x17000795 RID: 1941
		// (get) Token: 0x06002998 RID: 10648
		bool IsPrivate { get; }

		// Token: 0x17000796 RID: 1942
		// (get) Token: 0x06002999 RID: 10649
		bool IsPublic { get; }

		// Token: 0x17000797 RID: 1943
		// (get) Token: 0x0600299A RID: 10650
		bool IsSpecialName { get; }

		// Token: 0x17000798 RID: 1944
		// (get) Token: 0x0600299B RID: 10651
		bool IsStatic { get; }

		// Token: 0x17000799 RID: 1945
		// (get) Token: 0x0600299C RID: 10652
		bool IsVirtual { get; }

		// Token: 0x1700079A RID: 1946
		// (get) Token: 0x0600299D RID: 10653
		MemberTypes MemberType { get; }

		// Token: 0x1700079B RID: 1947
		// (get) Token: 0x0600299E RID: 10654
		RuntimeMethodHandle MethodHandle { get; }

		// Token: 0x1700079C RID: 1948
		// (get) Token: 0x0600299F RID: 10655
		string Name { get; }

		// Token: 0x1700079D RID: 1949
		// (get) Token: 0x060029A0 RID: 10656
		Type ReflectedType { get; }
	}
}
