﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003C3 RID: 963
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	[Guid("f1c3bf79-c3e4-11d3-88e7-00902754c43a")]
	public sealed class TypeLibConverter : ITypeLibConverter
	{
		// Token: 0x06002B80 RID: 11136 RVA: 0x00093D18 File Offset: 0x00091F18
		[MonoTODO("implement")]
		[return: MarshalAs(UnmanagedType.Interface)]
		public object ConvertAssemblyToTypeLib(Assembly assembly, string strTypeLibName, TypeLibExporterFlags flags, ITypeLibExporterNotifySink notifySink)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B81 RID: 11137 RVA: 0x00093D20 File Offset: 0x00091F20
		[MonoTODO("implement")]
		public AssemblyBuilder ConvertTypeLibToAssembly([MarshalAs(UnmanagedType.Interface)] object typeLib, string asmFileName, int flags, ITypeLibImporterNotifySink notifySink, byte[] publicKey, StrongNameKeyPair keyPair, bool unsafeInterfaces)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B82 RID: 11138 RVA: 0x00093D28 File Offset: 0x00091F28
		[MonoTODO("implement")]
		public AssemblyBuilder ConvertTypeLibToAssembly([MarshalAs(UnmanagedType.Interface)] object typeLib, string asmFileName, TypeLibImporterFlags flags, ITypeLibImporterNotifySink notifySink, byte[] publicKey, StrongNameKeyPair keyPair, string asmNamespace, Version asmVersion)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B83 RID: 11139 RVA: 0x00093D30 File Offset: 0x00091F30
		[MonoTODO("implement")]
		public bool GetPrimaryInteropAssembly(Guid g, int major, int minor, int lcid, out string asmName, out string asmCodeBase)
		{
			throw new NotImplementedException();
		}
	}
}
