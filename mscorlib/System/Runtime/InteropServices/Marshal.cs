﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices.ComTypes;
using System.Security;
using System.Threading;
using Mono.Interop;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003AC RID: 940
	[SuppressUnmanagedCodeSecurity]
	public static class Marshal
	{
		// Token: 0x06002AA1 RID: 10913
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int AddRefInternal(IntPtr pUnk);

		// Token: 0x06002AA2 RID: 10914 RVA: 0x00092B94 File Offset: 0x00090D94
		public static int AddRef(IntPtr pUnk)
		{
			if (pUnk == IntPtr.Zero)
			{
				throw new ArgumentException("Value cannot be null.", "pUnk");
			}
			return Marshal.AddRefInternal(pUnk);
		}

		// Token: 0x06002AA3 RID: 10915
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr AllocCoTaskMem(int cb);

		// Token: 0x06002AA4 RID: 10916
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr AllocHGlobal(IntPtr cb);

		// Token: 0x06002AA5 RID: 10917 RVA: 0x00092BC8 File Offset: 0x00090DC8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static IntPtr AllocHGlobal(int cb)
		{
			return Marshal.AllocHGlobal((IntPtr)cb);
		}

		// Token: 0x06002AA6 RID: 10918 RVA: 0x00092BD8 File Offset: 0x00090DD8
		[MonoTODO]
		public static object BindToMoniker(string monikerName)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AA7 RID: 10919 RVA: 0x00092BE0 File Offset: 0x00090DE0
		[MonoTODO]
		public static void ChangeWrapperHandleStrength(object otp, bool fIsWeak)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AA8 RID: 10920
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void copy_to_unmanaged(Array source, int startIndex, IntPtr destination, int length);

		// Token: 0x06002AA9 RID: 10921
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void copy_from_unmanaged(IntPtr source, int startIndex, Array destination, int length);

		// Token: 0x06002AAA RID: 10922 RVA: 0x00092BE8 File Offset: 0x00090DE8
		public static void Copy(byte[] source, int startIndex, IntPtr destination, int length)
		{
			Marshal.copy_to_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AAB RID: 10923 RVA: 0x00092BF4 File Offset: 0x00090DF4
		public static void Copy(char[] source, int startIndex, IntPtr destination, int length)
		{
			Marshal.copy_to_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AAC RID: 10924 RVA: 0x00092C00 File Offset: 0x00090E00
		public static void Copy(short[] source, int startIndex, IntPtr destination, int length)
		{
			Marshal.copy_to_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AAD RID: 10925 RVA: 0x00092C0C File Offset: 0x00090E0C
		public static void Copy(int[] source, int startIndex, IntPtr destination, int length)
		{
			Marshal.copy_to_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AAE RID: 10926 RVA: 0x00092C18 File Offset: 0x00090E18
		public static void Copy(long[] source, int startIndex, IntPtr destination, int length)
		{
			Marshal.copy_to_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AAF RID: 10927 RVA: 0x00092C24 File Offset: 0x00090E24
		public static void Copy(float[] source, int startIndex, IntPtr destination, int length)
		{
			Marshal.copy_to_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB0 RID: 10928 RVA: 0x00092C30 File Offset: 0x00090E30
		public static void Copy(double[] source, int startIndex, IntPtr destination, int length)
		{
			Marshal.copy_to_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB1 RID: 10929 RVA: 0x00092C3C File Offset: 0x00090E3C
		public static void Copy(IntPtr[] source, int startIndex, IntPtr destination, int length)
		{
			Marshal.copy_to_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB2 RID: 10930 RVA: 0x00092C48 File Offset: 0x00090E48
		public static void Copy(IntPtr source, byte[] destination, int startIndex, int length)
		{
			Marshal.copy_from_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB3 RID: 10931 RVA: 0x00092C54 File Offset: 0x00090E54
		public static void Copy(IntPtr source, char[] destination, int startIndex, int length)
		{
			Marshal.copy_from_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB4 RID: 10932 RVA: 0x00092C60 File Offset: 0x00090E60
		public static void Copy(IntPtr source, short[] destination, int startIndex, int length)
		{
			Marshal.copy_from_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB5 RID: 10933 RVA: 0x00092C6C File Offset: 0x00090E6C
		public static void Copy(IntPtr source, int[] destination, int startIndex, int length)
		{
			Marshal.copy_from_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB6 RID: 10934 RVA: 0x00092C78 File Offset: 0x00090E78
		public static void Copy(IntPtr source, long[] destination, int startIndex, int length)
		{
			Marshal.copy_from_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB7 RID: 10935 RVA: 0x00092C84 File Offset: 0x00090E84
		public static void Copy(IntPtr source, float[] destination, int startIndex, int length)
		{
			Marshal.copy_from_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB8 RID: 10936 RVA: 0x00092C90 File Offset: 0x00090E90
		public static void Copy(IntPtr source, double[] destination, int startIndex, int length)
		{
			Marshal.copy_from_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002AB9 RID: 10937 RVA: 0x00092C9C File Offset: 0x00090E9C
		public static void Copy(IntPtr source, IntPtr[] destination, int startIndex, int length)
		{
			Marshal.copy_from_unmanaged(source, startIndex, destination, length);
		}

		// Token: 0x06002ABA RID: 10938 RVA: 0x00092CA8 File Offset: 0x00090EA8
		public static IntPtr CreateAggregatedObject(IntPtr pOuter, object o)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002ABB RID: 10939 RVA: 0x00092CB0 File Offset: 0x00090EB0
		public static object CreateWrapperOfType(object o, Type t)
		{
			__ComObject _ComObject = o as __ComObject;
			if (_ComObject == null)
			{
				throw new ArgumentException("o must derive from __ComObject", "o");
			}
			if (t == null)
			{
				throw new ArgumentNullException("t");
			}
			Type[] interfaces = o.GetType().GetInterfaces();
			foreach (Type type in interfaces)
			{
				if (type.IsImport && _ComObject.GetInterface(type) == IntPtr.Zero)
				{
					throw new InvalidCastException();
				}
			}
			return ComInteropProxy.GetProxy(_ComObject.IUnknown, t).GetTransparentProxy();
		}

		// Token: 0x06002ABC RID: 10940
		[ComVisible(true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void DestroyStructure(IntPtr ptr, Type structuretype);

		// Token: 0x06002ABD RID: 10941
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void FreeBSTR(IntPtr ptr);

		// Token: 0x06002ABE RID: 10942
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void FreeCoTaskMem(IntPtr ptr);

		// Token: 0x06002ABF RID: 10943
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void FreeHGlobal(IntPtr hglobal);

		// Token: 0x06002AC0 RID: 10944 RVA: 0x00092D50 File Offset: 0x00090F50
		private static void ClearBSTR(IntPtr ptr)
		{
			int num = Marshal.ReadInt32(ptr, -4);
			for (int i = 0; i < num; i++)
			{
				Marshal.WriteByte(ptr, i, 0);
			}
		}

		// Token: 0x06002AC1 RID: 10945 RVA: 0x00092D80 File Offset: 0x00090F80
		public static void ZeroFreeBSTR(IntPtr s)
		{
			Marshal.ClearBSTR(s);
			Marshal.FreeBSTR(s);
		}

		// Token: 0x06002AC2 RID: 10946 RVA: 0x00092D90 File Offset: 0x00090F90
		private static void ClearAnsi(IntPtr ptr)
		{
			int num = 0;
			while (Marshal.ReadByte(ptr, num) != 0)
			{
				Marshal.WriteByte(ptr, num, 0);
				num++;
			}
		}

		// Token: 0x06002AC3 RID: 10947 RVA: 0x00092DBC File Offset: 0x00090FBC
		private static void ClearUnicode(IntPtr ptr)
		{
			int num = 0;
			while (Marshal.ReadInt16(ptr, num) != 0)
			{
				Marshal.WriteInt16(ptr, num, 0);
				num += 2;
			}
		}

		// Token: 0x06002AC4 RID: 10948 RVA: 0x00092DE8 File Offset: 0x00090FE8
		public static void ZeroFreeCoTaskMemAnsi(IntPtr s)
		{
			Marshal.ClearAnsi(s);
			Marshal.FreeCoTaskMem(s);
		}

		// Token: 0x06002AC5 RID: 10949 RVA: 0x00092DF8 File Offset: 0x00090FF8
		public static void ZeroFreeCoTaskMemUnicode(IntPtr s)
		{
			Marshal.ClearUnicode(s);
			Marshal.FreeCoTaskMem(s);
		}

		// Token: 0x06002AC6 RID: 10950 RVA: 0x00092E08 File Offset: 0x00091008
		public static void ZeroFreeGlobalAllocAnsi(IntPtr s)
		{
			Marshal.ClearAnsi(s);
			Marshal.FreeHGlobal(s);
		}

		// Token: 0x06002AC7 RID: 10951 RVA: 0x00092E18 File Offset: 0x00091018
		public static void ZeroFreeGlobalAllocUnicode(IntPtr s)
		{
			Marshal.ClearUnicode(s);
			Marshal.FreeHGlobal(s);
		}

		// Token: 0x06002AC8 RID: 10952 RVA: 0x00092E28 File Offset: 0x00091028
		public static Guid GenerateGuidForType(Type type)
		{
			return type.GUID;
		}

		// Token: 0x06002AC9 RID: 10953 RVA: 0x00092E30 File Offset: 0x00091030
		[MonoTODO]
		public static string GenerateProgIdForType(Type type)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002ACA RID: 10954 RVA: 0x00092E38 File Offset: 0x00091038
		[MonoTODO]
		public static object GetActiveObject(string progID)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002ACB RID: 10955
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetCCW(object o, Type T);

		// Token: 0x06002ACC RID: 10956 RVA: 0x00092E40 File Offset: 0x00091040
		private static IntPtr GetComInterfaceForObjectInternal(object o, Type T)
		{
			if (Marshal.IsComObject(o))
			{
				return ((__ComObject)o).GetInterface(T);
			}
			return Marshal.GetCCW(o, T);
		}

		// Token: 0x06002ACD RID: 10957 RVA: 0x00092E64 File Offset: 0x00091064
		public static IntPtr GetComInterfaceForObject(object o, Type T)
		{
			IntPtr comInterfaceForObjectInternal = Marshal.GetComInterfaceForObjectInternal(o, T);
			Marshal.AddRef(comInterfaceForObjectInternal);
			return comInterfaceForObjectInternal;
		}

		// Token: 0x06002ACE RID: 10958 RVA: 0x00092E84 File Offset: 0x00091084
		[MonoTODO]
		public static IntPtr GetComInterfaceForObjectInContext(object o, Type t)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002ACF RID: 10959 RVA: 0x00092E8C File Offset: 0x0009108C
		[MonoNotSupported("MSDN states user code should never need to call this method.")]
		public static object GetComObjectData(object obj, object key)
		{
			throw new NotSupportedException("MSDN states user code should never need to call this method.");
		}

		// Token: 0x06002AD0 RID: 10960
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetComSlotForMethodInfoInternal(MemberInfo m);

		// Token: 0x06002AD1 RID: 10961 RVA: 0x00092E98 File Offset: 0x00091098
		public static int GetComSlotForMethodInfo(MemberInfo m)
		{
			if (m == null)
			{
				throw new ArgumentNullException("m");
			}
			if (!(m is MethodInfo))
			{
				throw new ArgumentException("The MemberInfo must be an interface method.", "m");
			}
			if (!m.DeclaringType.IsInterface)
			{
				throw new ArgumentException("The MemberInfo must be an interface method.", "m");
			}
			return Marshal.GetComSlotForMethodInfoInternal(m);
		}

		// Token: 0x06002AD2 RID: 10962 RVA: 0x00092EF8 File Offset: 0x000910F8
		[MonoTODO]
		public static int GetEndComSlot(Type t)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AD3 RID: 10963 RVA: 0x00092F00 File Offset: 0x00091100
		[MonoTODO]
		public static int GetExceptionCode()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AD4 RID: 10964 RVA: 0x00092F08 File Offset: 0x00091108
		[ComVisible(true)]
		[MonoTODO]
		public static IntPtr GetExceptionPointers()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AD5 RID: 10965 RVA: 0x00092F10 File Offset: 0x00091110
		public static IntPtr GetHINSTANCE(Module m)
		{
			if (m == null)
			{
				throw new ArgumentNullException("m");
			}
			return m.GetHINSTANCE();
		}

		// Token: 0x06002AD6 RID: 10966 RVA: 0x00092F2C File Offset: 0x0009112C
		[MonoTODO("SetErrorInfo")]
		public static int GetHRForException(Exception e)
		{
			return e.hresult;
		}

		// Token: 0x06002AD7 RID: 10967 RVA: 0x00092F34 File Offset: 0x00091134
		[MonoTODO]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static int GetHRForLastWin32Error()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AD8 RID: 10968
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetIDispatchForObjectInternal(object o);

		// Token: 0x06002AD9 RID: 10969 RVA: 0x00092F3C File Offset: 0x0009113C
		public static IntPtr GetIDispatchForObject(object o)
		{
			IntPtr idispatchForObjectInternal = Marshal.GetIDispatchForObjectInternal(o);
			Marshal.AddRef(idispatchForObjectInternal);
			return idispatchForObjectInternal;
		}

		// Token: 0x06002ADA RID: 10970 RVA: 0x00092F58 File Offset: 0x00091158
		[MonoTODO]
		public static IntPtr GetIDispatchForObjectInContext(object o)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002ADB RID: 10971 RVA: 0x00092F60 File Offset: 0x00091160
		[MonoTODO]
		public static IntPtr GetITypeInfoForType(Type t)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002ADC RID: 10972
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetIUnknownForObjectInternal(object o);

		// Token: 0x06002ADD RID: 10973 RVA: 0x00092F68 File Offset: 0x00091168
		public static IntPtr GetIUnknownForObject(object o)
		{
			IntPtr iunknownForObjectInternal = Marshal.GetIUnknownForObjectInternal(o);
			Marshal.AddRef(iunknownForObjectInternal);
			return iunknownForObjectInternal;
		}

		// Token: 0x06002ADE RID: 10974 RVA: 0x00092F84 File Offset: 0x00091184
		[MonoTODO]
		public static IntPtr GetIUnknownForObjectInContext(object o)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002ADF RID: 10975 RVA: 0x00092F8C File Offset: 0x0009118C
		[MonoTODO]
		[Obsolete("This method has been deprecated")]
		public static IntPtr GetManagedThunkForUnmanagedMethodPtr(IntPtr pfnMethodToWrap, IntPtr pbSignature, int cbSignature)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AE0 RID: 10976 RVA: 0x00092F94 File Offset: 0x00091194
		[MonoTODO]
		public static MemberInfo GetMethodInfoForComSlot(Type t, int slot, ref ComMemberType memberType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AE1 RID: 10977 RVA: 0x00092F9C File Offset: 0x0009119C
		public static void GetNativeVariantForObject(object obj, IntPtr pDstNativeVariant)
		{
			Variant variant = default(Variant);
			variant.SetValue(obj);
			Marshal.StructureToPtr(variant, pDstNativeVariant, false);
		}

		// Token: 0x06002AE2 RID: 10978
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern object GetObjectForCCW(IntPtr pUnk);

		// Token: 0x06002AE3 RID: 10979 RVA: 0x00092FC8 File Offset: 0x000911C8
		public static object GetObjectForIUnknown(IntPtr pUnk)
		{
			object obj = Marshal.GetObjectForCCW(pUnk);
			if (obj == null)
			{
				ComInteropProxy proxy = ComInteropProxy.GetProxy(pUnk, typeof(__ComObject));
				obj = proxy.GetTransparentProxy();
			}
			return obj;
		}

		// Token: 0x06002AE4 RID: 10980 RVA: 0x00092FFC File Offset: 0x000911FC
		public static object GetObjectForNativeVariant(IntPtr pSrcNativeVariant)
		{
			return ((Variant)Marshal.PtrToStructure(pSrcNativeVariant, typeof(Variant))).GetValue();
		}

		// Token: 0x06002AE5 RID: 10981 RVA: 0x00093028 File Offset: 0x00091228
		public static object[] GetObjectsForNativeVariants(IntPtr aSrcNativeVariant, int cVars)
		{
			if (cVars < 0)
			{
				throw new ArgumentOutOfRangeException("cVars", "cVars cannot be a negative number.");
			}
			object[] array = new object[cVars];
			for (int i = 0; i < cVars; i++)
			{
				array[i] = Marshal.GetObjectForNativeVariant((IntPtr)(aSrcNativeVariant.ToInt64() + (long)(i * Marshal.SizeOf(typeof(Variant)))));
			}
			return array;
		}

		// Token: 0x06002AE6 RID: 10982 RVA: 0x00093090 File Offset: 0x00091290
		[MonoTODO]
		public static int GetStartComSlot(Type t)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AE7 RID: 10983 RVA: 0x00093098 File Offset: 0x00091298
		[Obsolete("This method has been deprecated")]
		[MonoTODO]
		public static Thread GetThreadFromFiberCookie(int cookie)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AE8 RID: 10984 RVA: 0x000930A0 File Offset: 0x000912A0
		public static object GetTypedObjectForIUnknown(IntPtr pUnk, Type t)
		{
			ComInteropProxy comInteropProxy = new ComInteropProxy(pUnk, t);
			__ComObject _ComObject = (__ComObject)comInteropProxy.GetTransparentProxy();
			foreach (Type type in t.GetInterfaces())
			{
				if ((type.Attributes & TypeAttributes.Import) == TypeAttributes.Import && _ComObject.GetInterface(type) == IntPtr.Zero)
				{
					return null;
				}
			}
			return _ComObject;
		}

		// Token: 0x06002AE9 RID: 10985 RVA: 0x00093114 File Offset: 0x00091314
		[MonoTODO]
		public static Type GetTypeForITypeInfo(IntPtr piTypeInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AEA RID: 10986 RVA: 0x0009311C File Offset: 0x0009131C
		[Obsolete]
		[MonoTODO]
		public static string GetTypeInfoName(UCOMITypeInfo pTI)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AEB RID: 10987 RVA: 0x00093124 File Offset: 0x00091324
		public static string GetTypeInfoName(ITypeInfo typeInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AEC RID: 10988 RVA: 0x0009312C File Offset: 0x0009132C
		[Obsolete]
		[MonoTODO]
		public static Guid GetTypeLibGuid(UCOMITypeLib pTLB)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AED RID: 10989 RVA: 0x00093134 File Offset: 0x00091334
		[MonoTODO]
		public static Guid GetTypeLibGuid(ITypeLib typelib)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AEE RID: 10990 RVA: 0x0009313C File Offset: 0x0009133C
		[MonoTODO]
		public static Guid GetTypeLibGuidForAssembly(Assembly asm)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AEF RID: 10991 RVA: 0x00093144 File Offset: 0x00091344
		[MonoTODO]
		[Obsolete]
		public static int GetTypeLibLcid(UCOMITypeLib pTLB)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AF0 RID: 10992 RVA: 0x0009314C File Offset: 0x0009134C
		[MonoTODO]
		public static int GetTypeLibLcid(ITypeLib typelib)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AF1 RID: 10993 RVA: 0x00093154 File Offset: 0x00091354
		[MonoTODO]
		[Obsolete]
		public static string GetTypeLibName(UCOMITypeLib pTLB)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AF2 RID: 10994 RVA: 0x0009315C File Offset: 0x0009135C
		[MonoTODO]
		public static string GetTypeLibName(ITypeLib typelib)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AF3 RID: 10995 RVA: 0x00093164 File Offset: 0x00091364
		[MonoTODO]
		public static void GetTypeLibVersionForAssembly(Assembly inputAssembly, out int majorVersion, out int minorVersion)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AF4 RID: 10996 RVA: 0x0009316C File Offset: 0x0009136C
		public static object GetUniqueObjectForIUnknown(IntPtr unknown)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AF5 RID: 10997 RVA: 0x00093174 File Offset: 0x00091374
		[Obsolete("This method has been deprecated")]
		[MonoTODO]
		public static IntPtr GetUnmanagedThunkForManagedMethodPtr(IntPtr pfnMethodToWrap, IntPtr pbSignature, int cbSignature)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AF6 RID: 10998
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern bool IsComObject(object o);

		// Token: 0x06002AF7 RID: 10999 RVA: 0x0009317C File Offset: 0x0009137C
		[MonoTODO]
		public static bool IsTypeVisibleFromCom(Type t)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AF8 RID: 11000 RVA: 0x00093184 File Offset: 0x00091384
		[MonoTODO]
		public static int NumParamBytes(MethodInfo m)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002AF9 RID: 11001
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int GetLastWin32Error();

		// Token: 0x06002AFA RID: 11002
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr OffsetOf(Type t, string fieldName);

		// Token: 0x06002AFB RID: 11003
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Prelink(MethodInfo m);

		// Token: 0x06002AFC RID: 11004
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void PrelinkAll(Type c);

		// Token: 0x06002AFD RID: 11005
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string PtrToStringAnsi(IntPtr ptr);

		// Token: 0x06002AFE RID: 11006
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string PtrToStringAnsi(IntPtr ptr, int len);

		// Token: 0x06002AFF RID: 11007 RVA: 0x0009318C File Offset: 0x0009138C
		public static string PtrToStringAuto(IntPtr ptr)
		{
			return (Marshal.SystemDefaultCharSize != 2) ? Marshal.PtrToStringAnsi(ptr) : Marshal.PtrToStringUni(ptr);
		}

		// Token: 0x06002B00 RID: 11008 RVA: 0x000931AC File Offset: 0x000913AC
		public static string PtrToStringAuto(IntPtr ptr, int len)
		{
			return (Marshal.SystemDefaultCharSize != 2) ? Marshal.PtrToStringAnsi(ptr, len) : Marshal.PtrToStringUni(ptr, len);
		}

		// Token: 0x06002B01 RID: 11009
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string PtrToStringUni(IntPtr ptr);

		// Token: 0x06002B02 RID: 11010
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string PtrToStringUni(IntPtr ptr, int len);

		// Token: 0x06002B03 RID: 11011
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string PtrToStringBSTR(IntPtr ptr);

		// Token: 0x06002B04 RID: 11012
		[ComVisible(true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void PtrToStructure(IntPtr ptr, object structure);

		// Token: 0x06002B05 RID: 11013
		[ComVisible(true)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern object PtrToStructure(IntPtr ptr, Type structureType);

		// Token: 0x06002B06 RID: 11014
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int QueryInterfaceInternal(IntPtr pUnk, ref Guid iid, out IntPtr ppv);

		// Token: 0x06002B07 RID: 11015 RVA: 0x000931CC File Offset: 0x000913CC
		public static int QueryInterface(IntPtr pUnk, ref Guid iid, out IntPtr ppv)
		{
			if (pUnk == IntPtr.Zero)
			{
				throw new ArgumentException("Value cannot be null.", "pUnk");
			}
			return Marshal.QueryInterfaceInternal(pUnk, ref iid, out ppv);
		}

		// Token: 0x06002B08 RID: 11016 RVA: 0x00093204 File Offset: 0x00091404
		public static byte ReadByte(IntPtr ptr)
		{
			return Marshal.ReadByte(ptr, 0);
		}

		// Token: 0x06002B09 RID: 11017
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern byte ReadByte(IntPtr ptr, int ofs);

		// Token: 0x06002B0A RID: 11018 RVA: 0x00093210 File Offset: 0x00091410
		[MonoTODO]
		public static byte ReadByte([MarshalAs(UnmanagedType.AsAny)] [In] object ptr, int ofs)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B0B RID: 11019 RVA: 0x00093218 File Offset: 0x00091418
		public static short ReadInt16(IntPtr ptr)
		{
			return Marshal.ReadInt16(ptr, 0);
		}

		// Token: 0x06002B0C RID: 11020
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern short ReadInt16(IntPtr ptr, int ofs);

		// Token: 0x06002B0D RID: 11021 RVA: 0x00093224 File Offset: 0x00091424
		[MonoTODO]
		public static short ReadInt16([MarshalAs(UnmanagedType.AsAny)] [In] object ptr, int ofs)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B0E RID: 11022 RVA: 0x0009322C File Offset: 0x0009142C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static int ReadInt32(IntPtr ptr)
		{
			return Marshal.ReadInt32(ptr, 0);
		}

		// Token: 0x06002B0F RID: 11023
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int ReadInt32(IntPtr ptr, int ofs);

		// Token: 0x06002B10 RID: 11024 RVA: 0x00093238 File Offset: 0x00091438
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MonoTODO]
		public static int ReadInt32([MarshalAs(UnmanagedType.AsAny)] [In] object ptr, int ofs)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B11 RID: 11025 RVA: 0x00093240 File Offset: 0x00091440
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static long ReadInt64(IntPtr ptr)
		{
			return Marshal.ReadInt64(ptr, 0);
		}

		// Token: 0x06002B12 RID: 11026
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern long ReadInt64(IntPtr ptr, int ofs);

		// Token: 0x06002B13 RID: 11027 RVA: 0x0009324C File Offset: 0x0009144C
		[MonoTODO]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static long ReadInt64([MarshalAs(UnmanagedType.AsAny)] [In] object ptr, int ofs)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B14 RID: 11028 RVA: 0x00093254 File Offset: 0x00091454
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static IntPtr ReadIntPtr(IntPtr ptr)
		{
			return Marshal.ReadIntPtr(ptr, 0);
		}

		// Token: 0x06002B15 RID: 11029
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr ReadIntPtr(IntPtr ptr, int ofs);

		// Token: 0x06002B16 RID: 11030 RVA: 0x00093260 File Offset: 0x00091460
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MonoTODO]
		public static IntPtr ReadIntPtr([MarshalAs(UnmanagedType.AsAny)] [In] object ptr, int ofs)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B17 RID: 11031
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr ReAllocCoTaskMem(IntPtr pv, int cb);

		// Token: 0x06002B18 RID: 11032
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr ReAllocHGlobal(IntPtr pv, IntPtr cb);

		// Token: 0x06002B19 RID: 11033
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int ReleaseInternal(IntPtr pUnk);

		// Token: 0x06002B1A RID: 11034 RVA: 0x00093268 File Offset: 0x00091468
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public static int Release(IntPtr pUnk)
		{
			if (pUnk == IntPtr.Zero)
			{
				throw new ArgumentException("Value cannot be null.", "pUnk");
			}
			return Marshal.ReleaseInternal(pUnk);
		}

		// Token: 0x06002B1B RID: 11035
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int ReleaseComObjectInternal(object co);

		// Token: 0x06002B1C RID: 11036 RVA: 0x0009329C File Offset: 0x0009149C
		public static int ReleaseComObject(object o)
		{
			if (o == null)
			{
				throw new ArgumentException("Value cannot be null.", "o");
			}
			if (!Marshal.IsComObject(o))
			{
				throw new ArgumentException("Value must be a Com object.", "o");
			}
			return Marshal.ReleaseComObjectInternal(o);
		}

		// Token: 0x06002B1D RID: 11037 RVA: 0x000932D8 File Offset: 0x000914D8
		[Obsolete]
		[MonoTODO]
		public static void ReleaseThreadCache()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B1E RID: 11038 RVA: 0x000932E0 File Offset: 0x000914E0
		[MonoNotSupported("MSDN states user code should never need to call this method.")]
		public static bool SetComObjectData(object obj, object key, object data)
		{
			throw new NotSupportedException("MSDN states user code should never need to call this method.");
		}

		// Token: 0x06002B1F RID: 11039 RVA: 0x000932EC File Offset: 0x000914EC
		[ComVisible(true)]
		public static int SizeOf(object structure)
		{
			return Marshal.SizeOf(structure.GetType());
		}

		// Token: 0x06002B20 RID: 11040
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern int SizeOf(Type t);

		// Token: 0x06002B21 RID: 11041
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr StringToBSTR(string s);

		// Token: 0x06002B22 RID: 11042 RVA: 0x000932FC File Offset: 0x000914FC
		public static IntPtr StringToCoTaskMemAnsi(string s)
		{
			int num = s.Length + 1;
			IntPtr intPtr = Marshal.AllocCoTaskMem(num);
			byte[] array = new byte[num];
			for (int i = 0; i < s.Length; i++)
			{
				array[i] = (byte)s[i];
			}
			array[s.Length] = 0;
			Marshal.copy_to_unmanaged(array, 0, intPtr, num);
			return intPtr;
		}

		// Token: 0x06002B23 RID: 11043 RVA: 0x00093358 File Offset: 0x00091558
		public static IntPtr StringToCoTaskMemAuto(string s)
		{
			return (Marshal.SystemDefaultCharSize != 2) ? Marshal.StringToCoTaskMemAnsi(s) : Marshal.StringToCoTaskMemUni(s);
		}

		// Token: 0x06002B24 RID: 11044 RVA: 0x00093378 File Offset: 0x00091578
		public static IntPtr StringToCoTaskMemUni(string s)
		{
			int num = s.Length + 1;
			IntPtr intPtr = Marshal.AllocCoTaskMem(num * 2);
			char[] array = new char[num];
			s.CopyTo(0, array, 0, s.Length);
			array[s.Length] = '\0';
			Marshal.copy_to_unmanaged(array, 0, intPtr, num);
			return intPtr;
		}

		// Token: 0x06002B25 RID: 11045
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr StringToHGlobalAnsi(string s);

		// Token: 0x06002B26 RID: 11046 RVA: 0x000933C0 File Offset: 0x000915C0
		public static IntPtr StringToHGlobalAuto(string s)
		{
			return (Marshal.SystemDefaultCharSize != 2) ? Marshal.StringToHGlobalAnsi(s) : Marshal.StringToHGlobalUni(s);
		}

		// Token: 0x06002B27 RID: 11047
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr StringToHGlobalUni(string s);

		// Token: 0x06002B28 RID: 11048 RVA: 0x000933E0 File Offset: 0x000915E0
		public static IntPtr SecureStringToBSTR(SecureString s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			int length = s.Length;
			IntPtr intPtr = Marshal.AllocCoTaskMem((length + 1) * 2 + 4);
			byte[] array = null;
			Marshal.WriteInt32(intPtr, 0, length * 2);
			try
			{
				array = s.GetBuffer();
				for (int i = 0; i < length; i++)
				{
					Marshal.WriteInt16(intPtr, 4 + i * 2, (short)((int)array[i * 2] << 8 | (int)array[i * 2 + 1]));
				}
				Marshal.WriteInt16(intPtr, 4 + array.Length, 0);
			}
			finally
			{
				if (array != null)
				{
					int j = array.Length;
					while (j > 0)
					{
						j--;
						array[j] = 0;
					}
				}
			}
			return (IntPtr)((long)intPtr + 4L);
		}

		// Token: 0x06002B29 RID: 11049 RVA: 0x000934B4 File Offset: 0x000916B4
		public static IntPtr SecureStringToCoTaskMemAnsi(SecureString s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			int length = s.Length;
			IntPtr intPtr = Marshal.AllocCoTaskMem(length + 1);
			byte[] array = new byte[length + 1];
			try
			{
				byte[] buffer = s.GetBuffer();
				int i = 0;
				int num = 0;
				while (i < length)
				{
					array[i] = buffer[num + 1];
					buffer[num] = 0;
					buffer[num + 1] = 0;
					i++;
					num += 2;
				}
				array[i] = 0;
				Marshal.copy_to_unmanaged(array, 0, intPtr, length + 1);
			}
			finally
			{
				int j = length;
				while (j > 0)
				{
					j--;
					array[j] = 0;
				}
			}
			return intPtr;
		}

		// Token: 0x06002B2A RID: 11050 RVA: 0x00093578 File Offset: 0x00091778
		public static IntPtr SecureStringToCoTaskMemUnicode(SecureString s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			int length = s.Length;
			IntPtr intPtr = Marshal.AllocCoTaskMem(length * 2 + 2);
			byte[] array = null;
			try
			{
				array = s.GetBuffer();
				for (int i = 0; i < length; i++)
				{
					Marshal.WriteInt16(intPtr, i * 2, (short)((int)array[i * 2] << 8 | (int)array[i * 2 + 1]));
				}
				Marshal.WriteInt16(intPtr, array.Length, 0);
			}
			finally
			{
				if (array != null)
				{
					int j = array.Length;
					while (j > 0)
					{
						j--;
						array[j] = 0;
					}
				}
			}
			return intPtr;
		}

		// Token: 0x06002B2B RID: 11051 RVA: 0x0009362C File Offset: 0x0009182C
		public static IntPtr SecureStringToGlobalAllocAnsi(SecureString s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			return Marshal.SecureStringToCoTaskMemAnsi(s);
		}

		// Token: 0x06002B2C RID: 11052 RVA: 0x00093648 File Offset: 0x00091848
		public static IntPtr SecureStringToGlobalAllocUnicode(SecureString s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			return Marshal.SecureStringToCoTaskMemUnicode(s);
		}

		// Token: 0x06002B2D RID: 11053
		[ComVisible(true)]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void StructureToPtr(object structure, IntPtr ptr, bool fDeleteOld);

		// Token: 0x06002B2E RID: 11054 RVA: 0x00093664 File Offset: 0x00091864
		public static void ThrowExceptionForHR(int errorCode)
		{
			Exception exceptionForHR = Marshal.GetExceptionForHR(errorCode);
			if (exceptionForHR != null)
			{
				throw exceptionForHR;
			}
		}

		// Token: 0x06002B2F RID: 11055 RVA: 0x00093680 File Offset: 0x00091880
		public static void ThrowExceptionForHR(int errorCode, IntPtr errorInfo)
		{
			Exception exceptionForHR = Marshal.GetExceptionForHR(errorCode, errorInfo);
			if (exceptionForHR != null)
			{
				throw exceptionForHR;
			}
		}

		// Token: 0x06002B30 RID: 11056
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern IntPtr UnsafeAddrOfPinnedArrayElement(Array arr, int index);

		// Token: 0x06002B31 RID: 11057 RVA: 0x000936A0 File Offset: 0x000918A0
		public static void WriteByte(IntPtr ptr, byte val)
		{
			Marshal.WriteByte(ptr, 0, val);
		}

		// Token: 0x06002B32 RID: 11058
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WriteByte(IntPtr ptr, int ofs, byte val);

		// Token: 0x06002B33 RID: 11059 RVA: 0x000936AC File Offset: 0x000918AC
		[MonoTODO]
		public static void WriteByte([MarshalAs(UnmanagedType.AsAny)] [In] [Out] object ptr, int ofs, byte val)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B34 RID: 11060 RVA: 0x000936B4 File Offset: 0x000918B4
		public static void WriteInt16(IntPtr ptr, short val)
		{
			Marshal.WriteInt16(ptr, 0, val);
		}

		// Token: 0x06002B35 RID: 11061
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WriteInt16(IntPtr ptr, int ofs, short val);

		// Token: 0x06002B36 RID: 11062 RVA: 0x000936C0 File Offset: 0x000918C0
		[MonoTODO]
		public static void WriteInt16([MarshalAs(UnmanagedType.AsAny)] [In] [Out] object ptr, int ofs, short val)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B37 RID: 11063 RVA: 0x000936C8 File Offset: 0x000918C8
		public static void WriteInt16(IntPtr ptr, char val)
		{
			Marshal.WriteInt16(ptr, 0, val);
		}

		// Token: 0x06002B38 RID: 11064
		[MonoTODO]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WriteInt16(IntPtr ptr, int ofs, char val);

		// Token: 0x06002B39 RID: 11065 RVA: 0x000936D4 File Offset: 0x000918D4
		[MonoTODO]
		public static void WriteInt16([In] [Out] object ptr, int ofs, char val)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B3A RID: 11066 RVA: 0x000936DC File Offset: 0x000918DC
		public static void WriteInt32(IntPtr ptr, int val)
		{
			Marshal.WriteInt32(ptr, 0, val);
		}

		// Token: 0x06002B3B RID: 11067
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WriteInt32(IntPtr ptr, int ofs, int val);

		// Token: 0x06002B3C RID: 11068 RVA: 0x000936E8 File Offset: 0x000918E8
		[MonoTODO]
		public static void WriteInt32([MarshalAs(UnmanagedType.AsAny)] [In] [Out] object ptr, int ofs, int val)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B3D RID: 11069 RVA: 0x000936F0 File Offset: 0x000918F0
		public static void WriteInt64(IntPtr ptr, long val)
		{
			Marshal.WriteInt64(ptr, 0, val);
		}

		// Token: 0x06002B3E RID: 11070
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WriteInt64(IntPtr ptr, int ofs, long val);

		// Token: 0x06002B3F RID: 11071 RVA: 0x000936FC File Offset: 0x000918FC
		[MonoTODO]
		public static void WriteInt64([MarshalAs(UnmanagedType.AsAny)] [In] [Out] object ptr, int ofs, long val)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B40 RID: 11072 RVA: 0x00093704 File Offset: 0x00091904
		public static void WriteIntPtr(IntPtr ptr, IntPtr val)
		{
			Marshal.WriteIntPtr(ptr, 0, val);
		}

		// Token: 0x06002B41 RID: 11073
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void WriteIntPtr(IntPtr ptr, int ofs, IntPtr val);

		// Token: 0x06002B42 RID: 11074 RVA: 0x00093710 File Offset: 0x00091910
		[MonoTODO]
		public static void WriteIntPtr([MarshalAs(UnmanagedType.AsAny)] [In] [Out] object ptr, int ofs, IntPtr val)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002B43 RID: 11075 RVA: 0x00093718 File Offset: 0x00091918
		public static Exception GetExceptionForHR(int errorCode)
		{
			return Marshal.GetExceptionForHR(errorCode, IntPtr.Zero);
		}

		// Token: 0x06002B44 RID: 11076 RVA: 0x00093728 File Offset: 0x00091928
		public static Exception GetExceptionForHR(int errorCode, IntPtr errorInfo)
		{
			if (errorCode == -2147024882)
			{
				return new OutOfMemoryException();
			}
			if (errorCode == -2147024809)
			{
				return new ArgumentException();
			}
			if (errorCode < 0)
			{
				return new COMException(string.Empty, errorCode);
			}
			return null;
		}

		// Token: 0x06002B45 RID: 11077 RVA: 0x00093774 File Offset: 0x00091974
		public static int FinalReleaseComObject(object o)
		{
			while (Marshal.ReleaseComObject(o) != 0)
			{
			}
			return 0;
		}

		// Token: 0x06002B46 RID: 11078
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Delegate GetDelegateForFunctionPointerInternal(IntPtr ptr, Type t);

		// Token: 0x06002B47 RID: 11079 RVA: 0x00093788 File Offset: 0x00091988
		public static Delegate GetDelegateForFunctionPointer(IntPtr ptr, Type t)
		{
			if (t == null)
			{
				throw new ArgumentNullException("t");
			}
			if (!t.IsSubclassOf(typeof(MulticastDelegate)) || t == typeof(MulticastDelegate))
			{
				throw new ArgumentException("Type is not a delegate", "t");
			}
			if (ptr == IntPtr.Zero)
			{
				throw new ArgumentNullException("ptr");
			}
			return Marshal.GetDelegateForFunctionPointerInternal(ptr, t);
		}

		// Token: 0x06002B48 RID: 11080
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetFunctionPointerForDelegateInternal(Delegate d);

		// Token: 0x06002B49 RID: 11081 RVA: 0x00093800 File Offset: 0x00091A00
		public static IntPtr GetFunctionPointerForDelegate(Delegate d)
		{
			if (d == null)
			{
				throw new ArgumentNullException("d");
			}
			return Marshal.GetFunctionPointerForDelegateInternal(d);
		}

		// Token: 0x04001160 RID: 4448
		public static readonly int SystemMaxDBCSCharSize = 2;

		// Token: 0x04001161 RID: 4449
		public static readonly int SystemDefaultCharSize = (Environment.OSVersion.Platform != PlatformID.Win32NT) ? 1 : 2;
	}
}
