﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000036 RID: 54
	[ComVisible(true)]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("f7102fa9-cabb-3a74-a6da-b4567ef1b079")]
	[TypeLibImportClass(typeof(MemberInfo))]
	public interface _MemberInfo
	{
		// Token: 0x06000588 RID: 1416
		bool Equals(object other);

		// Token: 0x06000589 RID: 1417
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x0600058A RID: 1418
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x0600058B RID: 1419
		int GetHashCode();

		// Token: 0x0600058C RID: 1420
		Type GetType();

		// Token: 0x0600058D RID: 1421
		bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x0600058E RID: 1422
		string ToString();

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x0600058F RID: 1423
		Type DeclaringType { get; }

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x06000590 RID: 1424
		MemberTypes MemberType { get; }

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000591 RID: 1425
		string Name { get; }

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000592 RID: 1426
		Type ReflectedType { get; }

		// Token: 0x06000593 RID: 1427
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06000594 RID: 1428
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x06000595 RID: 1429
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x06000596 RID: 1430
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
