﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000396 RID: 918
	[ComVisible(true)]
	public interface ICustomAdapter
	{
		// Token: 0x06002A76 RID: 10870
		[return: MarshalAs(UnmanagedType.IUnknown)]
		object GetUnderlyingObject();
	}
}
