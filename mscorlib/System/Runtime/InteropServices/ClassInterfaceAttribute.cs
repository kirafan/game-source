﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000374 RID: 884
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class, Inherited = false)]
	public sealed class ClassInterfaceAttribute : Attribute
	{
		// Token: 0x06002A1D RID: 10781 RVA: 0x000922D0 File Offset: 0x000904D0
		public ClassInterfaceAttribute(short classInterfaceType)
		{
			this.ciType = (ClassInterfaceType)classInterfaceType;
		}

		// Token: 0x06002A1E RID: 10782 RVA: 0x000922E0 File Offset: 0x000904E0
		public ClassInterfaceAttribute(ClassInterfaceType classInterfaceType)
		{
			this.ciType = classInterfaceType;
		}

		// Token: 0x170007C0 RID: 1984
		// (get) Token: 0x06002A1F RID: 10783 RVA: 0x000922F0 File Offset: 0x000904F0
		public ClassInterfaceType Value
		{
			get
			{
				return this.ciType;
			}
		}

		// Token: 0x040010D3 RID: 4307
		private ClassInterfaceType ciType;
	}
}
