﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200035F RID: 863
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("C2323C25-F57F-3880-8A4D-12EBEA7A5852")]
	[ComVisible(true)]
	[CLSCompliant(false)]
	[TypeLibImportClass(typeof(MethodRental))]
	public interface _MethodRental
	{
		// Token: 0x060029CB RID: 10699
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060029CC RID: 10700
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060029CD RID: 10701
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060029CE RID: 10702
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
