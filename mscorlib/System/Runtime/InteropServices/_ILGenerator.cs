﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200035A RID: 858
	[Guid("A4924B27-6E3B-37F7-9B83-A4501955E6A7")]
	[CLSCompliant(false)]
	[ComVisible(true)]
	[TypeLibImportClass(typeof(ILGenerator))]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _ILGenerator
	{
		// Token: 0x06002976 RID: 10614
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06002977 RID: 10615
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x06002978 RID: 10616
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x06002979 RID: 10617
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
