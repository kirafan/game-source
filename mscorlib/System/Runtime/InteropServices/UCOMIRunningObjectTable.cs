﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D8 RID: 984
	[Guid("00000010-0000-0000-c000-000000000046")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Obsolete]
	[ComImport]
	public interface UCOMIRunningObjectTable
	{
		// Token: 0x06002BD1 RID: 11217
		void Register(int grfFlags, [MarshalAs(UnmanagedType.Interface)] object punkObject, UCOMIMoniker pmkObjectName, out int pdwRegister);

		// Token: 0x06002BD2 RID: 11218
		void Revoke(int dwRegister);

		// Token: 0x06002BD3 RID: 11219
		void IsRunning(UCOMIMoniker pmkObjectName);

		// Token: 0x06002BD4 RID: 11220
		void GetObject(UCOMIMoniker pmkObjectName, [MarshalAs(UnmanagedType.Interface)] out object ppunkObject);

		// Token: 0x06002BD5 RID: 11221
		void NoteChangeTime(int dwRegister, ref FILETIME pfiletime);

		// Token: 0x06002BD6 RID: 11222
		void GetTimeOfLastChange(UCOMIMoniker pmkObjectName, out FILETIME pfiletime);

		// Token: 0x06002BD7 RID: 11223
		void EnumRunning(out UCOMIEnumMoniker ppenumMoniker);
	}
}
