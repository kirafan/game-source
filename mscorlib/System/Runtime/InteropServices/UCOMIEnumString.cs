﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D4 RID: 980
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Obsolete]
	[Guid("00000101-0000-0000-c000-000000000046")]
	[ComImport]
	public interface UCOMIEnumString
	{
		// Token: 0x06002BAF RID: 11183
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.LPWStr, SizeConst = 0, SizeParamIndex = 0)] [Out] string[] rgelt, out int pceltFetched);

		// Token: 0x06002BB0 RID: 11184
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002BB1 RID: 11185
		[PreserveSig]
		int Reset();

		// Token: 0x06002BB2 RID: 11186
		void Clone(out UCOMIEnumString ppenum);
	}
}
