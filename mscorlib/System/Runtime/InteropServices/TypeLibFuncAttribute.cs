﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003C5 RID: 965
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	[ComVisible(true)]
	public sealed class TypeLibFuncAttribute : Attribute
	{
		// Token: 0x06002B84 RID: 11140 RVA: 0x00093D38 File Offset: 0x00091F38
		public TypeLibFuncAttribute(short flags)
		{
			this.flags = (TypeLibFuncFlags)flags;
		}

		// Token: 0x06002B85 RID: 11141 RVA: 0x00093D48 File Offset: 0x00091F48
		public TypeLibFuncAttribute(TypeLibFuncFlags flags)
		{
			this.flags = flags;
		}

		// Token: 0x170007DF RID: 2015
		// (get) Token: 0x06002B86 RID: 11142 RVA: 0x00093D58 File Offset: 0x00091F58
		public TypeLibFuncFlags Value
		{
			get
			{
				return this.flags;
			}
		}

		// Token: 0x040011DB RID: 4571
		private TypeLibFuncFlags flags;
	}
}
