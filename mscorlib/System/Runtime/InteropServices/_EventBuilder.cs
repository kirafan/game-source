﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000356 RID: 854
	[CLSCompliant(false)]
	[ComVisible(true)]
	[TypeLibImportClass(typeof(EventBuilder))]
	[Guid("AADABA99-895D-3D65-9760-B1F12621FAE8")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _EventBuilder
	{
		// Token: 0x06002930 RID: 10544
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06002931 RID: 10545
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x06002932 RID: 10546
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x06002933 RID: 10547
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
