﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200035D RID: 861
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[CLSCompliant(false)]
	[ComVisible(true)]
	[TypeLibImportClass(typeof(MethodBuilder))]
	[Guid("007D8A14-FDF3-363E-9A0B-FEC0618260A2")]
	public interface _MethodBuilder
	{
		// Token: 0x060029A1 RID: 10657
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060029A2 RID: 10658
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060029A3 RID: 10659
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060029A4 RID: 10660
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
