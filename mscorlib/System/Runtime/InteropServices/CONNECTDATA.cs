﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000381 RID: 897
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct CONNECTDATA
	{
		// Token: 0x040010E9 RID: 4329
		[MarshalAs(UnmanagedType.Interface)]
		public object pUnk;

		// Token: 0x040010EA RID: 4330
		public int dwCookie;
	}
}
