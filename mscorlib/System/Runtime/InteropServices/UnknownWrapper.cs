﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003DD RID: 989
	[ComVisible(true)]
	[Serializable]
	public sealed class UnknownWrapper
	{
		// Token: 0x06002C02 RID: 11266 RVA: 0x00093DF4 File Offset: 0x00091FF4
		public UnknownWrapper(object obj)
		{
			this.InternalObject = obj;
		}

		// Token: 0x170007E5 RID: 2021
		// (get) Token: 0x06002C03 RID: 11267 RVA: 0x00093E04 File Offset: 0x00092004
		public object WrappedObject
		{
			get
			{
				return this.InternalObject;
			}
		}

		// Token: 0x04001219 RID: 4633
		private object InternalObject;
	}
}
