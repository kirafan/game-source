﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000360 RID: 864
	[CLSCompliant(false)]
	[ComVisible(true)]
	[Guid("D002E9BA-D9E3-3749-B1D3-D565A08B13E7")]
	[TypeLibImportClass(typeof(Module))]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _Module
	{
		// Token: 0x060029CF RID: 10703
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060029D0 RID: 10704
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060029D1 RID: 10705
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060029D2 RID: 10706
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
