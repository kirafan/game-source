﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003C6 RID: 966
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum TypeLibFuncFlags
	{
		// Token: 0x040011DD RID: 4573
		FRestricted = 1,
		// Token: 0x040011DE RID: 4574
		FSource = 2,
		// Token: 0x040011DF RID: 4575
		FBindable = 4,
		// Token: 0x040011E0 RID: 4576
		FRequestEdit = 8,
		// Token: 0x040011E1 RID: 4577
		FDisplayBind = 16,
		// Token: 0x040011E2 RID: 4578
		FDefaultBind = 32,
		// Token: 0x040011E3 RID: 4579
		FHidden = 64,
		// Token: 0x040011E4 RID: 4580
		FUsesGetLastError = 128,
		// Token: 0x040011E5 RID: 4581
		FDefaultCollelem = 256,
		// Token: 0x040011E6 RID: 4582
		FUiDefault = 512,
		// Token: 0x040011E7 RID: 4583
		FNonBrowsable = 1024,
		// Token: 0x040011E8 RID: 4584
		FReplaceable = 2048,
		// Token: 0x040011E9 RID: 4585
		FImmediateBind = 4096
	}
}
