﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000370 RID: 880
	[ComVisible(true)]
	[Serializable]
	public sealed class BStrWrapper
	{
		// Token: 0x06002A1B RID: 10779 RVA: 0x000922B8 File Offset: 0x000904B8
		public BStrWrapper(string value)
		{
			this._value = value;
		}

		// Token: 0x170007BF RID: 1983
		// (get) Token: 0x06002A1C RID: 10780 RVA: 0x000922C8 File Offset: 0x000904C8
		public string WrappedObject
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x040010BC RID: 4284
		private string _value;
	}
}
