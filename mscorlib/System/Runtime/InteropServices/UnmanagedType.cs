﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003DF RID: 991
	[ComVisible(true)]
	[Serializable]
	public enum UnmanagedType
	{
		// Token: 0x04001220 RID: 4640
		Bool = 2,
		// Token: 0x04001221 RID: 4641
		I1,
		// Token: 0x04001222 RID: 4642
		U1,
		// Token: 0x04001223 RID: 4643
		I2,
		// Token: 0x04001224 RID: 4644
		U2,
		// Token: 0x04001225 RID: 4645
		I4,
		// Token: 0x04001226 RID: 4646
		U4,
		// Token: 0x04001227 RID: 4647
		I8,
		// Token: 0x04001228 RID: 4648
		U8,
		// Token: 0x04001229 RID: 4649
		R4,
		// Token: 0x0400122A RID: 4650
		R8,
		// Token: 0x0400122B RID: 4651
		Currency = 15,
		// Token: 0x0400122C RID: 4652
		BStr = 19,
		// Token: 0x0400122D RID: 4653
		LPStr,
		// Token: 0x0400122E RID: 4654
		LPWStr,
		// Token: 0x0400122F RID: 4655
		LPTStr,
		// Token: 0x04001230 RID: 4656
		ByValTStr,
		// Token: 0x04001231 RID: 4657
		IUnknown = 25,
		// Token: 0x04001232 RID: 4658
		IDispatch,
		// Token: 0x04001233 RID: 4659
		Struct,
		// Token: 0x04001234 RID: 4660
		Interface,
		// Token: 0x04001235 RID: 4661
		SafeArray,
		// Token: 0x04001236 RID: 4662
		ByValArray,
		// Token: 0x04001237 RID: 4663
		SysInt,
		// Token: 0x04001238 RID: 4664
		SysUInt,
		// Token: 0x04001239 RID: 4665
		VBByRefStr = 34,
		// Token: 0x0400123A RID: 4666
		AnsiBStr,
		// Token: 0x0400123B RID: 4667
		TBStr,
		// Token: 0x0400123C RID: 4668
		VariantBool,
		// Token: 0x0400123D RID: 4669
		FunctionPtr,
		// Token: 0x0400123E RID: 4670
		AsAny = 40,
		// Token: 0x0400123F RID: 4671
		LPArray = 42,
		// Token: 0x04001240 RID: 4672
		LPStruct,
		// Token: 0x04001241 RID: 4673
		CustomMarshaler,
		// Token: 0x04001242 RID: 4674
		Error
	}
}
