﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000385 RID: 901
	[ComVisible(true)]
	[Serializable]
	public sealed class DispatchWrapper
	{
		// Token: 0x06002A48 RID: 10824 RVA: 0x00092660 File Offset: 0x00090860
		public DispatchWrapper(object obj)
		{
			Marshal.GetIDispatchForObject(obj);
			this.wrappedObject = obj;
		}

		// Token: 0x170007CD RID: 1997
		// (get) Token: 0x06002A49 RID: 10825 RVA: 0x00092678 File Offset: 0x00090878
		public object WrappedObject
		{
			get
			{
				return this.wrappedObject;
			}
		}

		// Token: 0x040010F5 RID: 4341
		private object wrappedObject;
	}
}
