﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B1 RID: 945
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = true)]
	public sealed class PrimaryInteropAssemblyAttribute : Attribute
	{
		// Token: 0x06002B4F RID: 11087 RVA: 0x0009387C File Offset: 0x00091A7C
		public PrimaryInteropAssemblyAttribute(int major, int minor)
		{
			this.major = major;
			this.minor = minor;
		}

		// Token: 0x170007D9 RID: 2009
		// (get) Token: 0x06002B50 RID: 11088 RVA: 0x00093894 File Offset: 0x00091A94
		public int MajorVersion
		{
			get
			{
				return this.major;
			}
		}

		// Token: 0x170007DA RID: 2010
		// (get) Token: 0x06002B51 RID: 11089 RVA: 0x0009389C File Offset: 0x00091A9C
		public int MinorVersion
		{
			get
			{
				return this.minor;
			}
		}

		// Token: 0x0400116E RID: 4462
		private int major;

		// Token: 0x0400116F RID: 4463
		private int minor;
	}
}
