﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200039C RID: 924
	[Obsolete]
	[Flags]
	[Serializable]
	public enum IDLFLAG
	{
		// Token: 0x0400113F RID: 4415
		IDLFLAG_NONE = 0,
		// Token: 0x04001140 RID: 4416
		IDLFLAG_FIN = 1,
		// Token: 0x04001141 RID: 4417
		IDLFLAG_FOUT = 2,
		// Token: 0x04001142 RID: 4418
		IDLFLAG_FLCID = 4,
		// Token: 0x04001143 RID: 4419
		IDLFLAG_FRETVAL = 8
	}
}
