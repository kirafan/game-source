﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200036F RID: 879
	[Obsolete]
	[StructLayout(LayoutKind.Explicit, CharSet = CharSet.Unicode)]
	public struct BINDPTR
	{
		// Token: 0x040010B9 RID: 4281
		[FieldOffset(0)]
		public IntPtr lpfuncdesc;

		// Token: 0x040010BA RID: 4282
		[FieldOffset(0)]
		public IntPtr lptcomp;

		// Token: 0x040010BB RID: 4283
		[FieldOffset(0)]
		public IntPtr lpvardesc;
	}
}
