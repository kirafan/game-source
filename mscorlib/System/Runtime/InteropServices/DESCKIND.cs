﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000384 RID: 900
	[Obsolete]
	[Serializable]
	public enum DESCKIND
	{
		// Token: 0x040010EF RID: 4335
		DESCKIND_NONE,
		// Token: 0x040010F0 RID: 4336
		DESCKIND_FUNCDESC,
		// Token: 0x040010F1 RID: 4337
		DESCKIND_VARDESC,
		// Token: 0x040010F2 RID: 4338
		DESCKIND_TYPECOMP,
		// Token: 0x040010F3 RID: 4339
		DESCKIND_IMPLICITAPPOBJ,
		// Token: 0x040010F4 RID: 4340
		DESCKIND_MAX
	}
}
