﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000373 RID: 883
	[ComVisible(true)]
	[Serializable]
	public enum CharSet
	{
		// Token: 0x040010CF RID: 4303
		None = 1,
		// Token: 0x040010D0 RID: 4304
		Ansi,
		// Token: 0x040010D1 RID: 4305
		Unicode,
		// Token: 0x040010D2 RID: 4306
		Auto
	}
}
