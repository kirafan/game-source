﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Threading;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003BB RID: 955
	public abstract class SafeHandle : CriticalFinalizerObject, IDisposable
	{
		// Token: 0x06002B71 RID: 11121 RVA: 0x00093AB4 File Offset: 0x00091CB4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		protected SafeHandle(IntPtr invalidHandleValue, bool ownsHandle)
		{
			this.invalid_handle_value = invalidHandleValue;
			this.owns_handle = ownsHandle;
			this.refcount = 1;
		}

		// Token: 0x06002B72 RID: 11122 RVA: 0x00093AD4 File Offset: 0x00091CD4
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void Close()
		{
			if (this.refcount == 0)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
			int num;
			int num2;
			do
			{
				num = this.refcount;
				num2 = num - 1;
			}
			while (Interlocked.CompareExchange(ref this.refcount, num2, num) != num);
			if (num2 == 0 && this.owns_handle && !this.IsInvalid)
			{
				this.ReleaseHandle();
				this.handle = this.invalid_handle_value;
				this.refcount = -1;
			}
		}

		// Token: 0x06002B73 RID: 11123 RVA: 0x00093B54 File Offset: 0x00091D54
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public void DangerousAddRef(ref bool success)
		{
			if (this.refcount <= 0)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
			for (;;)
			{
				int num = this.refcount;
				int value = num + 1;
				if (num <= 0)
				{
					break;
				}
				if (Interlocked.CompareExchange(ref this.refcount, value, num) == num)
				{
					goto Block_3;
				}
			}
			throw new ObjectDisposedException(base.GetType().FullName);
			Block_3:
			success = true;
		}

		// Token: 0x06002B74 RID: 11124 RVA: 0x00093BB8 File Offset: 0x00091DB8
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public IntPtr DangerousGetHandle()
		{
			if (this.refcount <= 0)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
			return this.handle;
		}

		// Token: 0x06002B75 RID: 11125 RVA: 0x00093BE0 File Offset: 0x00091DE0
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void DangerousRelease()
		{
			if (this.refcount <= 0)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
			int num;
			int num2;
			do
			{
				num = this.refcount;
				num2 = num - 1;
			}
			while (Interlocked.CompareExchange(ref this.refcount, num2, num) != num);
			if (num2 == 0 && this.owns_handle && !this.IsInvalid)
			{
				this.ReleaseHandle();
				this.handle = this.invalid_handle_value;
			}
		}

		// Token: 0x06002B76 RID: 11126 RVA: 0x00093C58 File Offset: 0x00091E58
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06002B77 RID: 11127 RVA: 0x00093C68 File Offset: 0x00091E68
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public void SetHandleAsInvalid()
		{
			this.handle = this.invalid_handle_value;
		}

		// Token: 0x06002B78 RID: 11128 RVA: 0x00093C78 File Offset: 0x00091E78
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				this.Close();
			}
		}

		// Token: 0x06002B79 RID: 11129
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		protected abstract bool ReleaseHandle();

		// Token: 0x06002B7A RID: 11130 RVA: 0x00093C8C File Offset: 0x00091E8C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		protected void SetHandle(IntPtr handle)
		{
			this.handle = handle;
		}

		// Token: 0x170007DD RID: 2013
		// (get) Token: 0x06002B7B RID: 11131 RVA: 0x00093C98 File Offset: 0x00091E98
		public bool IsClosed
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return this.refcount <= 0;
			}
		}

		// Token: 0x170007DE RID: 2014
		// (get) Token: 0x06002B7C RID: 11132
		public abstract bool IsInvalid { [ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)] get; }

		// Token: 0x06002B7D RID: 11133 RVA: 0x00093CA8 File Offset: 0x00091EA8
		~SafeHandle()
		{
			if (this.owns_handle && !this.IsInvalid)
			{
				this.ReleaseHandle();
				this.handle = this.invalid_handle_value;
			}
		}

		// Token: 0x04001197 RID: 4503
		protected IntPtr handle;

		// Token: 0x04001198 RID: 4504
		private IntPtr invalid_handle_value;

		// Token: 0x04001199 RID: 4505
		private int refcount;

		// Token: 0x0400119A RID: 4506
		private bool owns_handle;
	}
}
