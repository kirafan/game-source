﻿using System;
using System.Globalization;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000353 RID: 851
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("E9A19478-9646-3679-9B10-8411AE1FD57D")]
	[TypeLibImportClass(typeof(ConstructorInfo))]
	[ComVisible(true)]
	public interface _ConstructorInfo
	{
		// Token: 0x06002903 RID: 10499
		bool Equals(object other);

		// Token: 0x06002904 RID: 10500
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x06002905 RID: 10501
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x06002906 RID: 10502
		int GetHashCode();

		// Token: 0x06002907 RID: 10503
		MethodImplAttributes GetMethodImplementationFlags();

		// Token: 0x06002908 RID: 10504
		ParameterInfo[] GetParameters();

		// Token: 0x06002909 RID: 10505
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x0600290A RID: 10506
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x0600290B RID: 10507
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x0600290C RID: 10508
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);

		// Token: 0x0600290D RID: 10509
		Type GetType();

		// Token: 0x0600290E RID: 10510
		object Invoke_5(object[] parameters);

		// Token: 0x0600290F RID: 10511
		object Invoke_3(object obj, object[] parameters);

		// Token: 0x06002910 RID: 10512
		object Invoke_4(BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture);

		// Token: 0x06002911 RID: 10513
		object Invoke_2(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture);

		// Token: 0x06002912 RID: 10514
		bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x06002913 RID: 10515
		string ToString();

		// Token: 0x1700075B RID: 1883
		// (get) Token: 0x06002914 RID: 10516
		MethodAttributes Attributes { get; }

		// Token: 0x1700075C RID: 1884
		// (get) Token: 0x06002915 RID: 10517
		CallingConventions CallingConvention { get; }

		// Token: 0x1700075D RID: 1885
		// (get) Token: 0x06002916 RID: 10518
		Type DeclaringType { get; }

		// Token: 0x1700075E RID: 1886
		// (get) Token: 0x06002917 RID: 10519
		bool IsAbstract { get; }

		// Token: 0x1700075F RID: 1887
		// (get) Token: 0x06002918 RID: 10520
		bool IsAssembly { get; }

		// Token: 0x17000760 RID: 1888
		// (get) Token: 0x06002919 RID: 10521
		bool IsConstructor { get; }

		// Token: 0x17000761 RID: 1889
		// (get) Token: 0x0600291A RID: 10522
		bool IsFamily { get; }

		// Token: 0x17000762 RID: 1890
		// (get) Token: 0x0600291B RID: 10523
		bool IsFamilyAndAssembly { get; }

		// Token: 0x17000763 RID: 1891
		// (get) Token: 0x0600291C RID: 10524
		bool IsFamilyOrAssembly { get; }

		// Token: 0x17000764 RID: 1892
		// (get) Token: 0x0600291D RID: 10525
		bool IsFinal { get; }

		// Token: 0x17000765 RID: 1893
		// (get) Token: 0x0600291E RID: 10526
		bool IsHideBySig { get; }

		// Token: 0x17000766 RID: 1894
		// (get) Token: 0x0600291F RID: 10527
		bool IsPrivate { get; }

		// Token: 0x17000767 RID: 1895
		// (get) Token: 0x06002920 RID: 10528
		bool IsPublic { get; }

		// Token: 0x17000768 RID: 1896
		// (get) Token: 0x06002921 RID: 10529
		bool IsSpecialName { get; }

		// Token: 0x17000769 RID: 1897
		// (get) Token: 0x06002922 RID: 10530
		bool IsStatic { get; }

		// Token: 0x1700076A RID: 1898
		// (get) Token: 0x06002923 RID: 10531
		bool IsVirtual { get; }

		// Token: 0x1700076B RID: 1899
		// (get) Token: 0x06002924 RID: 10532
		MemberTypes MemberType { get; }

		// Token: 0x1700076C RID: 1900
		// (get) Token: 0x06002925 RID: 10533
		RuntimeMethodHandle MethodHandle { get; }

		// Token: 0x1700076D RID: 1901
		// (get) Token: 0x06002926 RID: 10534
		string Name { get; }

		// Token: 0x1700076E RID: 1902
		// (get) Token: 0x06002927 RID: 10535
		Type ReflectedType { get; }
	}
}
