﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003CE RID: 974
	[Guid("0000000e-0000-0000-c000-000000000046")]
	[Obsolete]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[ComImport]
	public interface UCOMIBindCtx
	{
		// Token: 0x06002B92 RID: 11154
		void RegisterObjectBound([MarshalAs(UnmanagedType.Interface)] object punk);

		// Token: 0x06002B93 RID: 11155
		void RevokeObjectBound([MarshalAs(UnmanagedType.Interface)] object punk);

		// Token: 0x06002B94 RID: 11156
		void ReleaseBoundObjects();

		// Token: 0x06002B95 RID: 11157
		void SetBindOptions([In] ref BIND_OPTS pbindopts);

		// Token: 0x06002B96 RID: 11158
		void GetBindOptions(ref BIND_OPTS pbindopts);

		// Token: 0x06002B97 RID: 11159
		void GetRunningObjectTable(out UCOMIRunningObjectTable pprot);

		// Token: 0x06002B98 RID: 11160
		void RegisterObjectParam([MarshalAs(UnmanagedType.LPWStr)] string pszKey, [MarshalAs(UnmanagedType.Interface)] object punk);

		// Token: 0x06002B99 RID: 11161
		void GetObjectParam([MarshalAs(UnmanagedType.LPWStr)] string pszKey, [MarshalAs(UnmanagedType.Interface)] out object ppunk);

		// Token: 0x06002B9A RID: 11162
		void EnumObjectParam(out UCOMIEnumString ppenum);

		// Token: 0x06002B9B RID: 11163
		void RevokeObjectParam([MarshalAs(UnmanagedType.LPWStr)] string pszKey);
	}
}
