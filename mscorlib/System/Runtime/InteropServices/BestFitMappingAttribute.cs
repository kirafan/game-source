﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200036D RID: 877
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface, Inherited = false)]
	public sealed class BestFitMappingAttribute : Attribute
	{
		// Token: 0x06002A19 RID: 10777 RVA: 0x000922A0 File Offset: 0x000904A0
		public BestFitMappingAttribute(bool BestFitMapping)
		{
			this.bfm = BestFitMapping;
		}

		// Token: 0x170007BE RID: 1982
		// (get) Token: 0x06002A1A RID: 10778 RVA: 0x000922B0 File Offset: 0x000904B0
		public bool BestFitMapping
		{
			get
			{
				return this.bfm;
			}
		}

		// Token: 0x040010B3 RID: 4275
		private bool bfm;

		// Token: 0x040010B4 RID: 4276
		public bool ThrowOnUnmappableChar;
	}
}
