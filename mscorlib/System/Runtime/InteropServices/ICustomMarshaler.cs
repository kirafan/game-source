﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000398 RID: 920
	[ComVisible(true)]
	public interface ICustomMarshaler
	{
		// Token: 0x06002A78 RID: 10872
		void CleanUpManagedData(object ManagedObj);

		// Token: 0x06002A79 RID: 10873
		void CleanUpNativeData(IntPtr pNativeData);

		// Token: 0x06002A7A RID: 10874
		int GetNativeDataSize();

		// Token: 0x06002A7B RID: 10875
		IntPtr MarshalManagedToNative(object ManagedObj);

		// Token: 0x06002A7C RID: 10876
		object MarshalNativeToManaged(IntPtr pNativeData);
	}
}
