﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000357 RID: 855
	[TypeLibImportClass(typeof(EventInfo))]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("9DE59C64-D889-35A1-B897-587D74469E5B")]
	[ComVisible(true)]
	public interface _EventInfo
	{
		// Token: 0x06002934 RID: 10548
		void AddEventHandler(object target, Delegate handler);

		// Token: 0x06002935 RID: 10549
		bool Equals(object other);

		// Token: 0x06002936 RID: 10550
		MethodInfo GetAddMethod();

		// Token: 0x06002937 RID: 10551
		MethodInfo GetAddMethod(bool nonPublic);

		// Token: 0x06002938 RID: 10552
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x06002939 RID: 10553
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x0600293A RID: 10554
		int GetHashCode();

		// Token: 0x0600293B RID: 10555
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x0600293C RID: 10556
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x0600293D RID: 10557
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x0600293E RID: 10558
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);

		// Token: 0x0600293F RID: 10559
		MethodInfo GetRaiseMethod();

		// Token: 0x06002940 RID: 10560
		MethodInfo GetRaiseMethod(bool nonPublic);

		// Token: 0x06002941 RID: 10561
		MethodInfo GetRemoveMethod();

		// Token: 0x06002942 RID: 10562
		MethodInfo GetRemoveMethod(bool nonPublic);

		// Token: 0x06002943 RID: 10563
		Type GetType();

		// Token: 0x06002944 RID: 10564
		bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x06002945 RID: 10565
		void RemoveEventHandler(object target, Delegate handler);

		// Token: 0x06002946 RID: 10566
		string ToString();

		// Token: 0x1700076F RID: 1903
		// (get) Token: 0x06002947 RID: 10567
		EventAttributes Attributes { get; }

		// Token: 0x17000770 RID: 1904
		// (get) Token: 0x06002948 RID: 10568
		Type DeclaringType { get; }

		// Token: 0x17000771 RID: 1905
		// (get) Token: 0x06002949 RID: 10569
		Type EventHandlerType { get; }

		// Token: 0x17000772 RID: 1906
		// (get) Token: 0x0600294A RID: 10570
		bool IsMulticast { get; }

		// Token: 0x17000773 RID: 1907
		// (get) Token: 0x0600294B RID: 10571
		bool IsSpecialName { get; }

		// Token: 0x17000774 RID: 1908
		// (get) Token: 0x0600294C RID: 10572
		MemberTypes MemberType { get; }

		// Token: 0x17000775 RID: 1909
		// (get) Token: 0x0600294D RID: 10573
		string Name { get; }

		// Token: 0x17000776 RID: 1910
		// (get) Token: 0x0600294E RID: 10574
		Type ReflectedType { get; }
	}
}
