﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200038B RID: 907
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct EXCEPINFO
	{
		// Token: 0x04001100 RID: 4352
		public short wCode;

		// Token: 0x04001101 RID: 4353
		public short wReserved;

		// Token: 0x04001102 RID: 4354
		[MarshalAs(UnmanagedType.BStr)]
		public string bstrSource;

		// Token: 0x04001103 RID: 4355
		[MarshalAs(UnmanagedType.BStr)]
		public string bstrDescription;

		// Token: 0x04001104 RID: 4356
		[MarshalAs(UnmanagedType.BStr)]
		public string bstrHelpFile;

		// Token: 0x04001105 RID: 4357
		public int dwHelpContext;

		// Token: 0x04001106 RID: 4358
		public IntPtr pvReserved;

		// Token: 0x04001107 RID: 4359
		public IntPtr pfnDeferredFillIn;
	}
}
