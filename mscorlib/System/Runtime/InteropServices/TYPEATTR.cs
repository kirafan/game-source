﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003BE RID: 958
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct TYPEATTR
	{
		// Token: 0x0400119F RID: 4511
		public const int MEMBER_ID_NIL = -1;

		// Token: 0x040011A0 RID: 4512
		public Guid guid;

		// Token: 0x040011A1 RID: 4513
		public int lcid;

		// Token: 0x040011A2 RID: 4514
		public int dwReserved;

		// Token: 0x040011A3 RID: 4515
		public int memidConstructor;

		// Token: 0x040011A4 RID: 4516
		public int memidDestructor;

		// Token: 0x040011A5 RID: 4517
		public IntPtr lpstrSchema;

		// Token: 0x040011A6 RID: 4518
		public int cbSizeInstance;

		// Token: 0x040011A7 RID: 4519
		public TYPEKIND typekind;

		// Token: 0x040011A8 RID: 4520
		public short cFuncs;

		// Token: 0x040011A9 RID: 4521
		public short cVars;

		// Token: 0x040011AA RID: 4522
		public short cImplTypes;

		// Token: 0x040011AB RID: 4523
		public short cbSizeVft;

		// Token: 0x040011AC RID: 4524
		public short cbAlignment;

		// Token: 0x040011AD RID: 4525
		public TYPEFLAGS wTypeFlags;

		// Token: 0x040011AE RID: 4526
		public short wMajorVerNum;

		// Token: 0x040011AF RID: 4527
		public short wMinorVerNum;

		// Token: 0x040011B0 RID: 4528
		public TYPEDESC tdescAlias;

		// Token: 0x040011B1 RID: 4529
		public IDLDESC idldescType;
	}
}
