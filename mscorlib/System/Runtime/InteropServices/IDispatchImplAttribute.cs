﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000399 RID: 921
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
	[ComVisible(true)]
	[Obsolete]
	public sealed class IDispatchImplAttribute : Attribute
	{
		// Token: 0x06002A7D RID: 10877 RVA: 0x00092A34 File Offset: 0x00090C34
		public IDispatchImplAttribute(IDispatchImplType implType)
		{
			this.Impl = implType;
		}

		// Token: 0x06002A7E RID: 10878 RVA: 0x00092A44 File Offset: 0x00090C44
		public IDispatchImplAttribute(short implType)
		{
			this.Impl = (IDispatchImplType)implType;
		}

		// Token: 0x170007D5 RID: 2005
		// (get) Token: 0x06002A7F RID: 10879 RVA: 0x00092A54 File Offset: 0x00090C54
		public IDispatchImplType Value
		{
			get
			{
				return this.Impl;
			}
		}

		// Token: 0x04001137 RID: 4407
		private IDispatchImplType Impl;
	}
}
