﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003CA RID: 970
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum TypeLibTypeFlags
	{
		// Token: 0x040011FA RID: 4602
		FAppObject = 1,
		// Token: 0x040011FB RID: 4603
		FCanCreate = 2,
		// Token: 0x040011FC RID: 4604
		FLicensed = 4,
		// Token: 0x040011FD RID: 4605
		FPreDeclId = 8,
		// Token: 0x040011FE RID: 4606
		FHidden = 16,
		// Token: 0x040011FF RID: 4607
		FControl = 32,
		// Token: 0x04001200 RID: 4608
		FDual = 64,
		// Token: 0x04001201 RID: 4609
		FNonExtensible = 128,
		// Token: 0x04001202 RID: 4610
		FOleAutomation = 256,
		// Token: 0x04001203 RID: 4611
		FRestricted = 512,
		// Token: 0x04001204 RID: 4612
		FAggregatable = 1024,
		// Token: 0x04001205 RID: 4613
		FReplaceable = 2048,
		// Token: 0x04001206 RID: 4614
		FDispatchable = 4096,
		// Token: 0x04001207 RID: 4615
		FReverseBind = 8192
	}
}
