﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B6 RID: 950
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	[ComVisible(true)]
	public sealed class ProgIdAttribute : Attribute
	{
		// Token: 0x06002B57 RID: 11095 RVA: 0x000938D4 File Offset: 0x00091AD4
		public ProgIdAttribute(string progId)
		{
			this.pid = progId;
		}

		// Token: 0x170007DB RID: 2011
		// (get) Token: 0x06002B58 RID: 11096 RVA: 0x000938E4 File Offset: 0x00091AE4
		public string Value
		{
			get
			{
				return this.pid;
			}
		}

		// Token: 0x04001194 RID: 4500
		private string pid;
	}
}
