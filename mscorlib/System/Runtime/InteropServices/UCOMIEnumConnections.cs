﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D2 RID: 978
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Obsolete]
	[Guid("b196b287-bab4-101a-b69c-00aa00341d07")]
	[ComImport]
	public interface UCOMIEnumConnections
	{
		// Token: 0x06002BA7 RID: 11175
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 0)] [Out] CONNECTDATA[] rgelt, out int pceltFetched);

		// Token: 0x06002BA8 RID: 11176
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002BA9 RID: 11177
		[PreserveSig]
		void Reset();

		// Token: 0x06002BAA RID: 11178
		void Clone(out UCOMIEnumConnections ppenum);
	}
}
