﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003AA RID: 938
	[Obsolete]
	[Flags]
	[Serializable]
	public enum LIBFLAGS
	{
		// Token: 0x04001158 RID: 4440
		LIBFLAG_FRESTRICTED = 1,
		// Token: 0x04001159 RID: 4441
		LIBFLAG_FCONTROL = 2,
		// Token: 0x0400115A RID: 4442
		LIBFLAG_FHIDDEN = 4,
		// Token: 0x0400115B RID: 4443
		LIBFLAG_FHASDISKIMAGE = 8
	}
}
