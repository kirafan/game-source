﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000366 RID: 870
	[TypeLibImportClass(typeof(SignatureHelper))]
	[CLSCompliant(false)]
	[ComVisible(true)]
	[Guid("7D13DD37-5A04-393C-BBCA-A5FEA802893D")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _SignatureHelper
	{
		// Token: 0x06002A02 RID: 10754
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06002A03 RID: 10755
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x06002A04 RID: 10756
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x06002A05 RID: 10757
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
