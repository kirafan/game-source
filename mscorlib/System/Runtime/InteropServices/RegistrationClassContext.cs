﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B2 RID: 946
	[Flags]
	public enum RegistrationClassContext
	{
		// Token: 0x04001171 RID: 4465
		DisableActivateAsActivator = 32768,
		// Token: 0x04001172 RID: 4466
		EnableActivateAsActivator = 65536,
		// Token: 0x04001173 RID: 4467
		EnableCodeDownload = 8192,
		// Token: 0x04001174 RID: 4468
		FromDefaultContext = 131072,
		// Token: 0x04001175 RID: 4469
		InProcessHandler = 2,
		// Token: 0x04001176 RID: 4470
		InProcessHandler16 = 32,
		// Token: 0x04001177 RID: 4471
		InProcessServer = 1,
		// Token: 0x04001178 RID: 4472
		InProcessServer16 = 8,
		// Token: 0x04001179 RID: 4473
		LocalServer = 4,
		// Token: 0x0400117A RID: 4474
		NoCodeDownload = 1024,
		// Token: 0x0400117B RID: 4475
		NoCustomMarshal = 4096,
		// Token: 0x0400117C RID: 4476
		NoFailureLog = 16384,
		// Token: 0x0400117D RID: 4477
		RemoteServer = 16,
		// Token: 0x0400117E RID: 4478
		Reserved1 = 64,
		// Token: 0x0400117F RID: 4479
		Reserved2 = 128,
		// Token: 0x04001180 RID: 4480
		Reserved3 = 256,
		// Token: 0x04001181 RID: 4481
		Reserved4 = 512,
		// Token: 0x04001182 RID: 4482
		Reserved5 = 2048
	}
}
