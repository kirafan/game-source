﻿using System;
using System.IO;
using System.Reflection;
using System.Security;
using System.Security.Permissions;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B8 RID: 952
	[ComVisible(true)]
	public class RuntimeEnvironment
	{
		// Token: 0x170007DC RID: 2012
		// (get) Token: 0x06002B65 RID: 11109 RVA: 0x0009394C File Offset: 0x00091B4C
		public static string SystemConfigurationFile
		{
			get
			{
				string machineConfigPath = Environment.GetMachineConfigPath();
				if (SecurityManager.SecurityEnabled)
				{
					new FileIOPermission(FileIOPermissionAccess.PathDiscovery, machineConfigPath).Demand();
				}
				return machineConfigPath;
			}
		}

		// Token: 0x06002B66 RID: 11110 RVA: 0x00093978 File Offset: 0x00091B78
		public static bool FromGlobalAccessCache(Assembly a)
		{
			return a.GlobalAssemblyCache;
		}

		// Token: 0x06002B67 RID: 11111 RVA: 0x00093980 File Offset: 0x00091B80
		public static string GetRuntimeDirectory()
		{
			return Path.GetDirectoryName(typeof(int).Assembly.Location);
		}

		// Token: 0x06002B68 RID: 11112 RVA: 0x0009399C File Offset: 0x00091B9C
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static string GetSystemVersion()
		{
			return string.Concat(new object[]
			{
				"v",
				Environment.Version.Major,
				".",
				Environment.Version.Minor,
				".",
				Environment.Version.Build
			});
		}
	}
}
