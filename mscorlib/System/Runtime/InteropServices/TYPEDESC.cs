﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003BF RID: 959
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct TYPEDESC
	{
		// Token: 0x040011B2 RID: 4530
		public IntPtr lpValue;

		// Token: 0x040011B3 RID: 4531
		public short vt;
	}
}
