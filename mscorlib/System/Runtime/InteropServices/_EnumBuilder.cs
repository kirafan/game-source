﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000355 RID: 853
	[Guid("C7BD73DE-9F85-3290-88EE-090B8BDFE2DF")]
	[TypeLibImportClass(typeof(EnumBuilder))]
	[ComVisible(true)]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _EnumBuilder
	{
		// Token: 0x0600292C RID: 10540
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x0600292D RID: 10541
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x0600292E RID: 10542
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x0600292F RID: 10543
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
