﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000363 RID: 867
	[Guid("993634C4-E47A-32CC-BE08-85F567DC27D6")]
	[ComVisible(true)]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[TypeLibImportClass(typeof(ParameterInfo))]
	public interface _ParameterInfo
	{
		// Token: 0x060029DB RID: 10715
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060029DC RID: 10716
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060029DD RID: 10717
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060029DE RID: 10718
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
