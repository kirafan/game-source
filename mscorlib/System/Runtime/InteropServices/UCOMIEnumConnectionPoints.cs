﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D1 RID: 977
	[Obsolete]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("b196b285-bab4-101a-b69c-00aa00341d07")]
	[ComImport]
	public interface UCOMIEnumConnectionPoints
	{
		// Token: 0x06002BA3 RID: 11171
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 0)] [Out] UCOMIConnectionPoint[] rgelt, out int pceltFetched);

		// Token: 0x06002BA4 RID: 11172
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002BA5 RID: 11173
		[PreserveSig]
		int Reset();

		// Token: 0x06002BA6 RID: 11174
		void Clone(out UCOMIEnumConnectionPoints ppenum);
	}
}
