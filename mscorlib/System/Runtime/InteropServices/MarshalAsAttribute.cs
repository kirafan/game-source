﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000042 RID: 66
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue, Inherited = false)]
	public sealed class MarshalAsAttribute : Attribute
	{
		// Token: 0x0600065B RID: 1627 RVA: 0x00014AAC File Offset: 0x00012CAC
		public MarshalAsAttribute(short unmanagedType)
		{
			this.utype = (UnmanagedType)unmanagedType;
		}

		// Token: 0x0600065C RID: 1628 RVA: 0x00014ABC File Offset: 0x00012CBC
		public MarshalAsAttribute(UnmanagedType unmanagedType)
		{
			this.utype = unmanagedType;
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x0600065D RID: 1629 RVA: 0x00014ACC File Offset: 0x00012CCC
		public UnmanagedType Value
		{
			get
			{
				return this.utype;
			}
		}

		// Token: 0x04000090 RID: 144
		private UnmanagedType utype;

		// Token: 0x04000091 RID: 145
		public UnmanagedType ArraySubType;

		// Token: 0x04000092 RID: 146
		public string MarshalCookie;

		// Token: 0x04000093 RID: 147
		[ComVisible(true)]
		public string MarshalType;

		// Token: 0x04000094 RID: 148
		[ComVisible(true)]
		public Type MarshalTypeRef;

		// Token: 0x04000095 RID: 149
		public VarEnum SafeArraySubType;

		// Token: 0x04000096 RID: 150
		public int SizeConst;

		// Token: 0x04000097 RID: 151
		public short SizeParamIndex;

		// Token: 0x04000098 RID: 152
		public Type SafeArrayUserDefinedSubType;

		// Token: 0x04000099 RID: 153
		public int IidParameterIndex;
	}
}
