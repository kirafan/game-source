﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000351 RID: 849
	[TypeLibImportClass(typeof(AssemblyName))]
	[ComVisible(true)]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("B42B6AAC-317E-34D5-9FA9-093BB4160C50")]
	public interface _AssemblyName
	{
		// Token: 0x060028FB RID: 10491
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060028FC RID: 10492
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060028FD RID: 10493
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060028FE RID: 10494
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
