﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003C1 RID: 961
	[Obsolete]
	[Serializable]
	public enum TYPEKIND
	{
		// Token: 0x040011C5 RID: 4549
		TKIND_ENUM,
		// Token: 0x040011C6 RID: 4550
		TKIND_RECORD,
		// Token: 0x040011C7 RID: 4551
		TKIND_MODULE,
		// Token: 0x040011C8 RID: 4552
		TKIND_INTERFACE,
		// Token: 0x040011C9 RID: 4553
		TKIND_DISPATCH,
		// Token: 0x040011CA RID: 4554
		TKIND_COCLASS,
		// Token: 0x040011CB RID: 4555
		TKIND_ALIAS,
		// Token: 0x040011CC RID: 4556
		TKIND_UNION,
		// Token: 0x040011CD RID: 4557
		TKIND_MAX
	}
}
