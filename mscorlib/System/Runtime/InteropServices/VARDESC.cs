﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003E0 RID: 992
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct VARDESC
	{
		// Token: 0x04001243 RID: 4675
		public int memid;

		// Token: 0x04001244 RID: 4676
		public string lpstrSchema;

		// Token: 0x04001245 RID: 4677
		public ELEMDESC elemdescVar;

		// Token: 0x04001246 RID: 4678
		public short wVarFlags;

		// Token: 0x04001247 RID: 4679
		public VarEnum varkind;

		// Token: 0x020003E1 RID: 993
		[ComVisible(false)]
		[StructLayout(LayoutKind.Explicit, CharSet = CharSet.Unicode)]
		public struct DESCUNION
		{
			// Token: 0x04001248 RID: 4680
			[FieldOffset(0)]
			public IntPtr lpvarValue;

			// Token: 0x04001249 RID: 4681
			[FieldOffset(0)]
			public int oInst;
		}
	}
}
