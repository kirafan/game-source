﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200039B RID: 923
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct IDLDESC
	{
		// Token: 0x0400113C RID: 4412
		public int dwReserved;

		// Token: 0x0400113D RID: 4413
		public IDLFLAG wIDLFlags;
	}
}
