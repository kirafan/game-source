﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003A2 RID: 930
	[Guid("f1c3bf77-c3e4-11d3-88e7-00902754c43a")]
	[ComVisible(true)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface ITypeLibExporterNotifySink
	{
		// Token: 0x06002A8D RID: 10893
		void ReportEvent(ExporterEventKind eventKind, int eventCode, string eventMsg);

		// Token: 0x06002A8E RID: 10894
		[return: MarshalAs(UnmanagedType.Interface)]
		object ResolveRef(Assembly assembly);
	}
}
