﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003E4 RID: 996
	[Serializable]
	public sealed class VariantWrapper
	{
		// Token: 0x06002C06 RID: 11270 RVA: 0x00093E24 File Offset: 0x00092024
		public VariantWrapper(object obj)
		{
			this._wrappedObject = obj;
		}

		// Token: 0x170007E7 RID: 2023
		// (get) Token: 0x06002C07 RID: 11271 RVA: 0x00093E34 File Offset: 0x00092034
		public object WrappedObject
		{
			get
			{
				return this._wrappedObject;
			}
		}

		// Token: 0x04001285 RID: 4741
		private object _wrappedObject;
	}
}
