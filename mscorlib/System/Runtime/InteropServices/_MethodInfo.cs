﻿using System;
using System.Globalization;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200035E RID: 862
	[Guid("FFCC1B5D-ECB8-38DD-9B01-3DC8ABC2AA5F")]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[TypeLibImportClass(typeof(MethodInfo))]
	[ComVisible(true)]
	public interface _MethodInfo
	{
		// Token: 0x060029A5 RID: 10661
		bool Equals(object other);

		// Token: 0x060029A6 RID: 10662
		MethodInfo GetBaseDefinition();

		// Token: 0x060029A7 RID: 10663
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x060029A8 RID: 10664
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x060029A9 RID: 10665
		int GetHashCode();

		// Token: 0x060029AA RID: 10666
		MethodImplAttributes GetMethodImplementationFlags();

		// Token: 0x060029AB RID: 10667
		ParameterInfo[] GetParameters();

		// Token: 0x060029AC RID: 10668
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060029AD RID: 10669
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060029AE RID: 10670
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060029AF RID: 10671
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);

		// Token: 0x060029B0 RID: 10672
		Type GetType();

		// Token: 0x060029B1 RID: 10673
		object Invoke(object obj, object[] parameters);

		// Token: 0x060029B2 RID: 10674
		object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture);

		// Token: 0x060029B3 RID: 10675
		bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x060029B4 RID: 10676
		string ToString();

		// Token: 0x1700079E RID: 1950
		// (get) Token: 0x060029B5 RID: 10677
		MethodAttributes Attributes { get; }

		// Token: 0x1700079F RID: 1951
		// (get) Token: 0x060029B6 RID: 10678
		CallingConventions CallingConvention { get; }

		// Token: 0x170007A0 RID: 1952
		// (get) Token: 0x060029B7 RID: 10679
		Type DeclaringType { get; }

		// Token: 0x170007A1 RID: 1953
		// (get) Token: 0x060029B8 RID: 10680
		bool IsAbstract { get; }

		// Token: 0x170007A2 RID: 1954
		// (get) Token: 0x060029B9 RID: 10681
		bool IsAssembly { get; }

		// Token: 0x170007A3 RID: 1955
		// (get) Token: 0x060029BA RID: 10682
		bool IsConstructor { get; }

		// Token: 0x170007A4 RID: 1956
		// (get) Token: 0x060029BB RID: 10683
		bool IsFamily { get; }

		// Token: 0x170007A5 RID: 1957
		// (get) Token: 0x060029BC RID: 10684
		bool IsFamilyAndAssembly { get; }

		// Token: 0x170007A6 RID: 1958
		// (get) Token: 0x060029BD RID: 10685
		bool IsFamilyOrAssembly { get; }

		// Token: 0x170007A7 RID: 1959
		// (get) Token: 0x060029BE RID: 10686
		bool IsFinal { get; }

		// Token: 0x170007A8 RID: 1960
		// (get) Token: 0x060029BF RID: 10687
		bool IsHideBySig { get; }

		// Token: 0x170007A9 RID: 1961
		// (get) Token: 0x060029C0 RID: 10688
		bool IsPrivate { get; }

		// Token: 0x170007AA RID: 1962
		// (get) Token: 0x060029C1 RID: 10689
		bool IsPublic { get; }

		// Token: 0x170007AB RID: 1963
		// (get) Token: 0x060029C2 RID: 10690
		bool IsSpecialName { get; }

		// Token: 0x170007AC RID: 1964
		// (get) Token: 0x060029C3 RID: 10691
		bool IsStatic { get; }

		// Token: 0x170007AD RID: 1965
		// (get) Token: 0x060029C4 RID: 10692
		bool IsVirtual { get; }

		// Token: 0x170007AE RID: 1966
		// (get) Token: 0x060029C5 RID: 10693
		MemberTypes MemberType { get; }

		// Token: 0x170007AF RID: 1967
		// (get) Token: 0x060029C6 RID: 10694
		RuntimeMethodHandle MethodHandle { get; }

		// Token: 0x170007B0 RID: 1968
		// (get) Token: 0x060029C7 RID: 10695
		string Name { get; }

		// Token: 0x170007B1 RID: 1969
		// (get) Token: 0x060029C8 RID: 10696
		Type ReflectedType { get; }

		// Token: 0x170007B2 RID: 1970
		// (get) Token: 0x060029C9 RID: 10697
		Type ReturnType { get; }

		// Token: 0x170007B3 RID: 1971
		// (get) Token: 0x060029CA RID: 10698
		ICustomAttributeProvider ReturnTypeCustomAttributes { get; }
	}
}
