﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B4 RID: 948
	[ComVisible(true)]
	[Serializable]
	public class SEHException : ExternalException
	{
		// Token: 0x06002B52 RID: 11090 RVA: 0x000938A4 File Offset: 0x00091AA4
		public SEHException()
		{
		}

		// Token: 0x06002B53 RID: 11091 RVA: 0x000938AC File Offset: 0x00091AAC
		public SEHException(string message) : base(message)
		{
		}

		// Token: 0x06002B54 RID: 11092 RVA: 0x000938B8 File Offset: 0x00091AB8
		public SEHException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06002B55 RID: 11093 RVA: 0x000938C4 File Offset: 0x00091AC4
		protected SEHException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06002B56 RID: 11094 RVA: 0x000938D0 File Offset: 0x00091AD0
		public virtual bool CanResume()
		{
			return false;
		}
	}
}
