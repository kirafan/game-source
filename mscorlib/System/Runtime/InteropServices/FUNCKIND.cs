﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000392 RID: 914
	[Obsolete]
	[Serializable]
	public enum FUNCKIND
	{
		// Token: 0x0400112A RID: 4394
		FUNC_VIRTUAL,
		// Token: 0x0400112B RID: 4395
		FUNC_PUREVIRTUAL,
		// Token: 0x0400112C RID: 4396
		FUNC_NONVIRTUAL,
		// Token: 0x0400112D RID: 4397
		FUNC_STATIC,
		// Token: 0x0400112E RID: 4398
		FUNC_DISPATCH
	}
}
