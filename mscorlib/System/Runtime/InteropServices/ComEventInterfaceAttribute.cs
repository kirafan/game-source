﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200037B RID: 891
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Interface, Inherited = false)]
	public sealed class ComEventInterfaceAttribute : Attribute
	{
		// Token: 0x06002A30 RID: 10800 RVA: 0x00092438 File Offset: 0x00090638
		public ComEventInterfaceAttribute(Type SourceInterface, Type EventProvider)
		{
			this.si = SourceInterface;
			this.ep = EventProvider;
		}

		// Token: 0x170007C7 RID: 1991
		// (get) Token: 0x06002A31 RID: 10801 RVA: 0x00092450 File Offset: 0x00090650
		public Type EventProvider
		{
			get
			{
				return this.ep;
			}
		}

		// Token: 0x170007C8 RID: 1992
		// (get) Token: 0x06002A32 RID: 10802 RVA: 0x00092458 File Offset: 0x00090658
		public Type SourceInterface
		{
			get
			{
				return this.si;
			}
		}

		// Token: 0x040010DE RID: 4318
		private Type si;

		// Token: 0x040010DF RID: 4319
		private Type ep;
	}
}
