﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000361 RID: 865
	[CLSCompliant(false)]
	[ComVisible(true)]
	[TypeLibImportClass(typeof(ModuleBuilder))]
	[Guid("D05FFA9A-04AF-3519-8EE1-8D93AD73430B")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _ModuleBuilder
	{
		// Token: 0x060029D3 RID: 10707
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060029D4 RID: 10708
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060029D5 RID: 10709
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060029D6 RID: 10710
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
