﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000379 RID: 889
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class, Inherited = false)]
	public sealed class ComDefaultInterfaceAttribute : Attribute
	{
		// Token: 0x06002A28 RID: 10792 RVA: 0x00092360 File Offset: 0x00090560
		public ComDefaultInterfaceAttribute(Type defaultInterface)
		{
			this._type = defaultInterface;
		}

		// Token: 0x170007C6 RID: 1990
		// (get) Token: 0x06002A29 RID: 10793 RVA: 0x00092370 File Offset: 0x00090570
		public Type Value
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x040010DD RID: 4317
		private Type _type;
	}
}
