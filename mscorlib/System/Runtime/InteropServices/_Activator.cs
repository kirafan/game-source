﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200034E RID: 846
	[Guid("03973551-57A1-3900-A2B5-9083E3FF2943")]
	[TypeLibImportClass(typeof(Activator))]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[CLSCompliant(false)]
	[ComVisible(true)]
	public interface _Activator
	{
		// Token: 0x060028C7 RID: 10439
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060028C8 RID: 10440
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060028C9 RID: 10441
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060028CA RID: 10442
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
