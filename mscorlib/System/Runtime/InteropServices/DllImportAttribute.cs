﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000040 RID: 64
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	public sealed class DllImportAttribute : Attribute
	{
		// Token: 0x06000655 RID: 1621 RVA: 0x00014A64 File Offset: 0x00012C64
		public DllImportAttribute(string dllName)
		{
			this.Dll = dllName;
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x06000656 RID: 1622 RVA: 0x00014A74 File Offset: 0x00012C74
		public string Value
		{
			get
			{
				return this.Dll;
			}
		}

		// Token: 0x04000085 RID: 133
		public CallingConvention CallingConvention;

		// Token: 0x04000086 RID: 134
		public CharSet CharSet;

		// Token: 0x04000087 RID: 135
		private string Dll;

		// Token: 0x04000088 RID: 136
		public string EntryPoint;

		// Token: 0x04000089 RID: 137
		public bool ExactSpelling;

		// Token: 0x0400008A RID: 138
		public bool PreserveSig;

		// Token: 0x0400008B RID: 139
		public bool SetLastError;

		// Token: 0x0400008C RID: 140
		public bool BestFitMapping;

		// Token: 0x0400008D RID: 141
		public bool ThrowOnUnmappableChar;
	}
}
