﻿using System;
using System.Globalization;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000359 RID: 857
	[Guid("8A7C1442-A9FB-366B-80D8-4939FFA6DBE0")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[CLSCompliant(false)]
	[ComVisible(true)]
	[TypeLibImportClass(typeof(FieldInfo))]
	public interface _FieldInfo
	{
		// Token: 0x06002953 RID: 10579
		bool Equals(object other);

		// Token: 0x06002954 RID: 10580
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x06002955 RID: 10581
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x06002956 RID: 10582
		int GetHashCode();

		// Token: 0x06002957 RID: 10583
		Type GetType();

		// Token: 0x06002958 RID: 10584
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06002959 RID: 10585
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x0600295A RID: 10586
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x0600295B RID: 10587
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);

		// Token: 0x0600295C RID: 10588
		object GetValue(object obj);

		// Token: 0x0600295D RID: 10589
		object GetValueDirect(TypedReference obj);

		// Token: 0x0600295E RID: 10590
		bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x0600295F RID: 10591
		void SetValue(object obj, object value);

		// Token: 0x06002960 RID: 10592
		void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, CultureInfo culture);

		// Token: 0x06002961 RID: 10593
		void SetValueDirect(TypedReference obj, object value);

		// Token: 0x06002962 RID: 10594
		string ToString();

		// Token: 0x17000777 RID: 1911
		// (get) Token: 0x06002963 RID: 10595
		FieldAttributes Attributes { get; }

		// Token: 0x17000778 RID: 1912
		// (get) Token: 0x06002964 RID: 10596
		Type DeclaringType { get; }

		// Token: 0x17000779 RID: 1913
		// (get) Token: 0x06002965 RID: 10597
		RuntimeFieldHandle FieldHandle { get; }

		// Token: 0x1700077A RID: 1914
		// (get) Token: 0x06002966 RID: 10598
		Type FieldType { get; }

		// Token: 0x1700077B RID: 1915
		// (get) Token: 0x06002967 RID: 10599
		bool IsAssembly { get; }

		// Token: 0x1700077C RID: 1916
		// (get) Token: 0x06002968 RID: 10600
		bool IsFamily { get; }

		// Token: 0x1700077D RID: 1917
		// (get) Token: 0x06002969 RID: 10601
		bool IsFamilyAndAssembly { get; }

		// Token: 0x1700077E RID: 1918
		// (get) Token: 0x0600296A RID: 10602
		bool IsFamilyOrAssembly { get; }

		// Token: 0x1700077F RID: 1919
		// (get) Token: 0x0600296B RID: 10603
		bool IsInitOnly { get; }

		// Token: 0x17000780 RID: 1920
		// (get) Token: 0x0600296C RID: 10604
		bool IsLiteral { get; }

		// Token: 0x17000781 RID: 1921
		// (get) Token: 0x0600296D RID: 10605
		bool IsNotSerialized { get; }

		// Token: 0x17000782 RID: 1922
		// (get) Token: 0x0600296E RID: 10606
		bool IsPinvokeImpl { get; }

		// Token: 0x17000783 RID: 1923
		// (get) Token: 0x0600296F RID: 10607
		bool IsPrivate { get; }

		// Token: 0x17000784 RID: 1924
		// (get) Token: 0x06002970 RID: 10608
		bool IsPublic { get; }

		// Token: 0x17000785 RID: 1925
		// (get) Token: 0x06002971 RID: 10609
		bool IsSpecialName { get; }

		// Token: 0x17000786 RID: 1926
		// (get) Token: 0x06002972 RID: 10610
		bool IsStatic { get; }

		// Token: 0x17000787 RID: 1927
		// (get) Token: 0x06002973 RID: 10611
		MemberTypes MemberType { get; }

		// Token: 0x17000788 RID: 1928
		// (get) Token: 0x06002974 RID: 10612
		string Name { get; }

		// Token: 0x17000789 RID: 1929
		// (get) Token: 0x06002975 RID: 10613
		Type ReflectedType { get; }
	}
}
