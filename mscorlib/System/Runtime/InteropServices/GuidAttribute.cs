﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000048 RID: 72
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface | AttributeTargets.Delegate, Inherited = false)]
	[ComVisible(true)]
	public sealed class GuidAttribute : Attribute
	{
		// Token: 0x0600066A RID: 1642 RVA: 0x00014B44 File Offset: 0x00012D44
		public GuidAttribute(string guid)
		{
			this.guidValue = guid;
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x0600066B RID: 1643 RVA: 0x00014B54 File Offset: 0x00012D54
		public string Value
		{
			get
			{
				return this.guidValue;
			}
		}

		// Token: 0x0400009D RID: 157
		private string guidValue;
	}
}
