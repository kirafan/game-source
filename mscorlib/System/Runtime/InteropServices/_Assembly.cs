﻿using System;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Security.Policy;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200034F RID: 847
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	[TypeLibImportClass(typeof(Assembly))]
	[Guid("17156360-2F1A-384A-BC52-FDE93C215C5B")]
	[ComVisible(true)]
	[CLSCompliant(false)]
	public interface _Assembly
	{
		// Token: 0x14000016 RID: 22
		// (add) Token: 0x060028CB RID: 10443
		// (remove) Token: 0x060028CC RID: 10444
		event ModuleResolveEventHandler ModuleResolve;

		// Token: 0x060028CD RID: 10445
		string ToString();

		// Token: 0x060028CE RID: 10446
		bool Equals(object other);

		// Token: 0x060028CF RID: 10447
		int GetHashCode();

		// Token: 0x060028D0 RID: 10448
		Type GetType();

		// Token: 0x17000754 RID: 1876
		// (get) Token: 0x060028D1 RID: 10449
		string CodeBase { get; }

		// Token: 0x17000755 RID: 1877
		// (get) Token: 0x060028D2 RID: 10450
		string EscapedCodeBase { get; }

		// Token: 0x060028D3 RID: 10451
		AssemblyName GetName();

		// Token: 0x060028D4 RID: 10452
		AssemblyName GetName(bool copiedName);

		// Token: 0x17000756 RID: 1878
		// (get) Token: 0x060028D5 RID: 10453
		string FullName { get; }

		// Token: 0x17000757 RID: 1879
		// (get) Token: 0x060028D6 RID: 10454
		MethodInfo EntryPoint { get; }

		// Token: 0x060028D7 RID: 10455
		Type GetType(string name);

		// Token: 0x060028D8 RID: 10456
		Type GetType(string name, bool throwOnError);

		// Token: 0x060028D9 RID: 10457
		Type[] GetExportedTypes();

		// Token: 0x060028DA RID: 10458
		Type[] GetTypes();

		// Token: 0x060028DB RID: 10459
		Stream GetManifestResourceStream(Type type, string name);

		// Token: 0x060028DC RID: 10460
		Stream GetManifestResourceStream(string name);

		// Token: 0x060028DD RID: 10461
		FileStream GetFile(string name);

		// Token: 0x060028DE RID: 10462
		FileStream[] GetFiles();

		// Token: 0x060028DF RID: 10463
		FileStream[] GetFiles(bool getResourceModules);

		// Token: 0x060028E0 RID: 10464
		string[] GetManifestResourceNames();

		// Token: 0x060028E1 RID: 10465
		ManifestResourceInfo GetManifestResourceInfo(string resourceName);

		// Token: 0x17000758 RID: 1880
		// (get) Token: 0x060028E2 RID: 10466
		string Location { get; }

		// Token: 0x17000759 RID: 1881
		// (get) Token: 0x060028E3 RID: 10467
		Evidence Evidence { get; }

		// Token: 0x060028E4 RID: 10468
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x060028E5 RID: 10469
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x060028E6 RID: 10470
		bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x060028E7 RID: 10471
		void GetObjectData(SerializationInfo info, StreamingContext context);

		// Token: 0x060028E8 RID: 10472
		Type GetType(string name, bool throwOnError, bool ignoreCase);

		// Token: 0x060028E9 RID: 10473
		Assembly GetSatelliteAssembly(CultureInfo culture);

		// Token: 0x060028EA RID: 10474
		Assembly GetSatelliteAssembly(CultureInfo culture, Version version);

		// Token: 0x060028EB RID: 10475
		Module LoadModule(string moduleName, byte[] rawModule);

		// Token: 0x060028EC RID: 10476
		Module LoadModule(string moduleName, byte[] rawModule, byte[] rawSymbolStore);

		// Token: 0x060028ED RID: 10477
		object CreateInstance(string typeName);

		// Token: 0x060028EE RID: 10478
		object CreateInstance(string typeName, bool ignoreCase);

		// Token: 0x060028EF RID: 10479
		object CreateInstance(string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes);

		// Token: 0x060028F0 RID: 10480
		Module[] GetLoadedModules();

		// Token: 0x060028F1 RID: 10481
		Module[] GetLoadedModules(bool getResourceModules);

		// Token: 0x060028F2 RID: 10482
		Module[] GetModules();

		// Token: 0x060028F3 RID: 10483
		Module[] GetModules(bool getResourceModules);

		// Token: 0x060028F4 RID: 10484
		Module GetModule(string name);

		// Token: 0x060028F5 RID: 10485
		AssemblyName[] GetReferencedAssemblies();

		// Token: 0x1700075A RID: 1882
		// (get) Token: 0x060028F6 RID: 10486
		bool GlobalAssemblyCache { get; }
	}
}
