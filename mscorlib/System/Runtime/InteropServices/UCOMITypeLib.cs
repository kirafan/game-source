﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003DC RID: 988
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00020402-0000-0000-c000-000000000046")]
	[Obsolete]
	[ComImport]
	public interface UCOMITypeLib
	{
		// Token: 0x06002BF8 RID: 11256
		[PreserveSig]
		int GetTypeInfoCount();

		// Token: 0x06002BF9 RID: 11257
		void GetTypeInfo(int index, out UCOMITypeInfo ppTI);

		// Token: 0x06002BFA RID: 11258
		void GetTypeInfoType(int index, out TYPEKIND pTKind);

		// Token: 0x06002BFB RID: 11259
		void GetTypeInfoOfGuid(ref Guid guid, out UCOMITypeInfo ppTInfo);

		// Token: 0x06002BFC RID: 11260
		void GetLibAttr(out IntPtr ppTLibAttr);

		// Token: 0x06002BFD RID: 11261
		void GetTypeComp(out UCOMITypeComp ppTComp);

		// Token: 0x06002BFE RID: 11262
		void GetDocumentation(int index, out string strName, out string strDocString, out int dwHelpContext, out string strHelpFile);

		// Token: 0x06002BFF RID: 11263
		[return: MarshalAs(UnmanagedType.Bool)]
		bool IsName([MarshalAs(UnmanagedType.LPWStr)] string szNameBuf, int lHashVal);

		// Token: 0x06002C00 RID: 11264
		void FindName([MarshalAs(UnmanagedType.LPWStr)] string szNameBuf, int lHashVal, [MarshalAs(UnmanagedType.LPArray)] [Out] UCOMITypeInfo[] ppTInfo, [MarshalAs(UnmanagedType.LPArray)] [Out] int[] rgMemId, ref short pcFound);

		// Token: 0x06002C01 RID: 11265
		[PreserveSig]
		void ReleaseTLibAttr(IntPtr pTLibAttr);
	}
}
