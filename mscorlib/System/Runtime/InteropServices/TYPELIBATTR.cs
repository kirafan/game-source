﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003C2 RID: 962
	[Obsolete]
	[Serializable]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct TYPELIBATTR
	{
		// Token: 0x040011CE RID: 4558
		public Guid guid;

		// Token: 0x040011CF RID: 4559
		public int lcid;

		// Token: 0x040011D0 RID: 4560
		public SYSKIND syskind;

		// Token: 0x040011D1 RID: 4561
		public short wMajorVerNum;

		// Token: 0x040011D2 RID: 4562
		public short wMinorVerNum;

		// Token: 0x040011D3 RID: 4563
		public LIBFLAGS wLibFlags;
	}
}
