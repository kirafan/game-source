﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000043 RID: 67
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	public sealed class InAttribute : Attribute
	{
	}
}
