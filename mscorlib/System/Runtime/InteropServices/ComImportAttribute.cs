﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200004B RID: 75
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Interface, Inherited = false)]
	public sealed class ComImportAttribute : Attribute
	{
	}
}
