﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D0 RID: 976
	[Obsolete]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("b196b284-bab4-101a-b69c-00aa00341d07")]
	[ComImport]
	public interface UCOMIConnectionPointContainer
	{
		// Token: 0x06002BA1 RID: 11169
		void EnumConnectionPoints(out UCOMIEnumConnectionPoints ppEnum);

		// Token: 0x06002BA2 RID: 11170
		void FindConnectionPoint(ref Guid riid, out UCOMIConnectionPoint ppCP);
	}
}
