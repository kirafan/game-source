﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000387 RID: 903
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event, Inherited = false)]
	public sealed class DispIdAttribute : Attribute
	{
		// Token: 0x06002A4A RID: 10826 RVA: 0x00092680 File Offset: 0x00090880
		public DispIdAttribute(int dispId)
		{
			this.id = dispId;
		}

		// Token: 0x170007CE RID: 1998
		// (get) Token: 0x06002A4B RID: 10827 RVA: 0x00092690 File Offset: 0x00090890
		public int Value
		{
			get
			{
				return this.id;
			}
		}

		// Token: 0x040010FA RID: 4346
		private int id;
	}
}
