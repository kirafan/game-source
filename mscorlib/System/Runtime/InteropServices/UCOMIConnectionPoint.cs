﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003CF RID: 975
	[Obsolete]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("b196b286-bab4-101a-b69c-00aa00341d07")]
	[ComImport]
	public interface UCOMIConnectionPoint
	{
		// Token: 0x06002B9C RID: 11164
		void GetConnectionInterface(out Guid pIID);

		// Token: 0x06002B9D RID: 11165
		void GetConnectionPointContainer(out UCOMIConnectionPointContainer ppCPC);

		// Token: 0x06002B9E RID: 11166
		void Advise([MarshalAs(UnmanagedType.Interface)] object pUnkSink, out int pdwCookie);

		// Token: 0x06002B9F RID: 11167
		void Unadvise(int dwCookie);

		// Token: 0x06002BA0 RID: 11168
		void EnumConnections(out UCOMIEnumConnections ppEnum);
	}
}
