﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003CB RID: 971
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	public sealed class TypeLibVarAttribute : Attribute
	{
		// Token: 0x06002B8C RID: 11148 RVA: 0x00093DA4 File Offset: 0x00091FA4
		public TypeLibVarAttribute(short flags)
		{
			this.flags = (TypeLibVarFlags)flags;
		}

		// Token: 0x06002B8D RID: 11149 RVA: 0x00093DB4 File Offset: 0x00091FB4
		public TypeLibVarAttribute(TypeLibVarFlags flags)
		{
			this.flags = flags;
		}

		// Token: 0x170007E2 RID: 2018
		// (get) Token: 0x06002B8E RID: 11150 RVA: 0x00093DC4 File Offset: 0x00091FC4
		public TypeLibVarFlags Value
		{
			get
			{
				return this.flags;
			}
		}

		// Token: 0x04001208 RID: 4616
		private TypeLibVarFlags flags;
	}
}
