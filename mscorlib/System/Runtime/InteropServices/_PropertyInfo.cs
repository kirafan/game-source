﻿using System;
using System.Globalization;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000365 RID: 869
	[Guid("F59ED4E4-E68F-3218-BD77-061AA82824BF")]
	[ComVisible(true)]
	[TypeLibImportClass(typeof(PropertyInfo))]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _PropertyInfo
	{
		// Token: 0x060029E3 RID: 10723
		bool Equals(object other);

		// Token: 0x060029E4 RID: 10724
		MethodInfo[] GetAccessors();

		// Token: 0x060029E5 RID: 10725
		MethodInfo[] GetAccessors(bool nonPublic);

		// Token: 0x060029E6 RID: 10726
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x060029E7 RID: 10727
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x060029E8 RID: 10728
		MethodInfo GetGetMethod();

		// Token: 0x060029E9 RID: 10729
		MethodInfo GetGetMethod(bool nonPublic);

		// Token: 0x060029EA RID: 10730
		int GetHashCode();

		// Token: 0x060029EB RID: 10731
		ParameterInfo[] GetIndexParameters();

		// Token: 0x060029EC RID: 10732
		MethodInfo GetSetMethod();

		// Token: 0x060029ED RID: 10733
		MethodInfo GetSetMethod(bool nonPublic);

		// Token: 0x060029EE RID: 10734
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060029EF RID: 10735
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060029F0 RID: 10736
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060029F1 RID: 10737
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);

		// Token: 0x060029F2 RID: 10738
		Type GetType();

		// Token: 0x060029F3 RID: 10739
		object GetValue(object obj, object[] index);

		// Token: 0x060029F4 RID: 10740
		object GetValue(object obj, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture);

		// Token: 0x060029F5 RID: 10741
		bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x060029F6 RID: 10742
		void SetValue(object obj, object value, object[] index);

		// Token: 0x060029F7 RID: 10743
		void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture);

		// Token: 0x060029F8 RID: 10744
		string ToString();

		// Token: 0x170007B4 RID: 1972
		// (get) Token: 0x060029F9 RID: 10745
		PropertyAttributes Attributes { get; }

		// Token: 0x170007B5 RID: 1973
		// (get) Token: 0x060029FA RID: 10746
		bool CanRead { get; }

		// Token: 0x170007B6 RID: 1974
		// (get) Token: 0x060029FB RID: 10747
		bool CanWrite { get; }

		// Token: 0x170007B7 RID: 1975
		// (get) Token: 0x060029FC RID: 10748
		Type DeclaringType { get; }

		// Token: 0x170007B8 RID: 1976
		// (get) Token: 0x060029FD RID: 10749
		bool IsSpecialName { get; }

		// Token: 0x170007B9 RID: 1977
		// (get) Token: 0x060029FE RID: 10750
		MemberTypes MemberType { get; }

		// Token: 0x170007BA RID: 1978
		// (get) Token: 0x060029FF RID: 10751
		string Name { get; }

		// Token: 0x170007BB RID: 1979
		// (get) Token: 0x06002A00 RID: 10752
		Type PropertyType { get; }

		// Token: 0x170007BC RID: 1980
		// (get) Token: 0x06002A01 RID: 10753
		Type ReflectedType { get; }
	}
}
