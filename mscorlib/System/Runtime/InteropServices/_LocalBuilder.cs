﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200035B RID: 859
	[Guid("4E6350D1-A08B-3DEC-9A3E-C465F9AEEC0C")]
	[TypeLibImportClass(typeof(LocalBuilder))]
	[ComVisible(true)]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _LocalBuilder
	{
		// Token: 0x0600297A RID: 10618
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x0600297B RID: 10619
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x0600297C RID: 10620
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x0600297D RID: 10621
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
