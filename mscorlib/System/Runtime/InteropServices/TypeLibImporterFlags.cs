﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003C8 RID: 968
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum TypeLibImporterFlags
	{
		// Token: 0x040011EC RID: 4588
		PrimaryInteropAssembly = 1,
		// Token: 0x040011ED RID: 4589
		UnsafeInterfaces = 2,
		// Token: 0x040011EE RID: 4590
		SafeArrayAsSystemArray = 4,
		// Token: 0x040011EF RID: 4591
		TransformDispRetVals = 8,
		// Token: 0x040011F0 RID: 4592
		None = 0,
		// Token: 0x040011F1 RID: 4593
		PreventClassMembers = 16,
		// Token: 0x040011F2 RID: 4594
		ImportAsAgnostic = 2048,
		// Token: 0x040011F3 RID: 4595
		ImportAsItanium = 1024,
		// Token: 0x040011F4 RID: 4596
		ImportAsX64 = 512,
		// Token: 0x040011F5 RID: 4597
		ImportAsX86 = 256,
		// Token: 0x040011F6 RID: 4598
		ReflectionOnlyLoading = 4096,
		// Token: 0x040011F7 RID: 4599
		SerializableValueClasses = 32
	}
}
