﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003C9 RID: 969
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface, Inherited = false)]
	[ComVisible(true)]
	public sealed class TypeLibTypeAttribute : Attribute
	{
		// Token: 0x06002B89 RID: 11145 RVA: 0x00093D7C File Offset: 0x00091F7C
		public TypeLibTypeAttribute(short flags)
		{
			this.flags = (TypeLibTypeFlags)flags;
		}

		// Token: 0x06002B8A RID: 11146 RVA: 0x00093D8C File Offset: 0x00091F8C
		public TypeLibTypeAttribute(TypeLibTypeFlags flags)
		{
			this.flags = flags;
		}

		// Token: 0x170007E1 RID: 2017
		// (get) Token: 0x06002B8B RID: 11147 RVA: 0x00093D9C File Offset: 0x00091F9C
		public TypeLibTypeFlags Value
		{
			get
			{
				return this.flags;
			}
		}

		// Token: 0x040011F8 RID: 4600
		private TypeLibTypeFlags flags;
	}
}
