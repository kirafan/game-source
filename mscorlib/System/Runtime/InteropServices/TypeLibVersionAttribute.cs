﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003CD RID: 973
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = false, Inherited = false)]
	public sealed class TypeLibVersionAttribute : Attribute
	{
		// Token: 0x06002B8F RID: 11151 RVA: 0x00093DCC File Offset: 0x00091FCC
		public TypeLibVersionAttribute(int major, int minor)
		{
			this.major = major;
			this.minor = minor;
		}

		// Token: 0x170007E3 RID: 2019
		// (get) Token: 0x06002B90 RID: 11152 RVA: 0x00093DE4 File Offset: 0x00091FE4
		public int MajorVersion
		{
			get
			{
				return this.major;
			}
		}

		// Token: 0x170007E4 RID: 2020
		// (get) Token: 0x06002B91 RID: 11153 RVA: 0x00093DEC File Offset: 0x00091FEC
		public int MinorVersion
		{
			get
			{
				return this.minor;
			}
		}

		// Token: 0x04001217 RID: 4631
		private int major;

		// Token: 0x04001218 RID: 4632
		private int minor;
	}
}
