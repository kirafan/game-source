﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B9 RID: 953
	[ComVisible(true)]
	[Serializable]
	public class SafeArrayRankMismatchException : SystemException
	{
		// Token: 0x06002B69 RID: 11113 RVA: 0x00093A04 File Offset: 0x00091C04
		public SafeArrayRankMismatchException() : base(Locale.GetText("The incoming SAVEARRAY does not match the rank of the expected managed signature"))
		{
			base.HResult = -2146233032;
		}

		// Token: 0x06002B6A RID: 11114 RVA: 0x00093A24 File Offset: 0x00091C24
		public SafeArrayRankMismatchException(string message) : base(message)
		{
			base.HResult = -2146233032;
		}

		// Token: 0x06002B6B RID: 11115 RVA: 0x00093A38 File Offset: 0x00091C38
		public SafeArrayRankMismatchException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233032;
		}

		// Token: 0x06002B6C RID: 11116 RVA: 0x00093A50 File Offset: 0x00091C50
		protected SafeArrayRankMismatchException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04001195 RID: 4501
		private const int ErrorCode = -2146233032;
	}
}
