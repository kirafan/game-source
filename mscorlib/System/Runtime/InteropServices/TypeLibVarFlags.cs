﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003CC RID: 972
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum TypeLibVarFlags
	{
		// Token: 0x0400120A RID: 4618
		FReadOnly = 1,
		// Token: 0x0400120B RID: 4619
		FSource = 2,
		// Token: 0x0400120C RID: 4620
		FBindable = 4,
		// Token: 0x0400120D RID: 4621
		FRequestEdit = 8,
		// Token: 0x0400120E RID: 4622
		FDisplayBind = 16,
		// Token: 0x0400120F RID: 4623
		FDefaultBind = 32,
		// Token: 0x04001210 RID: 4624
		FHidden = 64,
		// Token: 0x04001211 RID: 4625
		FRestricted = 128,
		// Token: 0x04001212 RID: 4626
		FDefaultCollelem = 256,
		// Token: 0x04001213 RID: 4627
		FUiDefault = 512,
		// Token: 0x04001214 RID: 4628
		FNonBrowsable = 1024,
		// Token: 0x04001215 RID: 4629
		FReplaceable = 2048,
		// Token: 0x04001216 RID: 4630
		FImmediateBind = 4096
	}
}
