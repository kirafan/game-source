﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200039D RID: 925
	[Obsolete]
	[Flags]
	[Serializable]
	public enum IMPLTYPEFLAGS
	{
		// Token: 0x04001145 RID: 4421
		IMPLTYPEFLAG_FDEFAULT = 1,
		// Token: 0x04001146 RID: 4422
		IMPLTYPEFLAG_FSOURCE = 2,
		// Token: 0x04001147 RID: 4423
		IMPLTYPEFLAG_FRESTRICTED = 4,
		// Token: 0x04001148 RID: 4424
		IMPLTYPEFLAG_FDEFAULTVTABLE = 8
	}
}
