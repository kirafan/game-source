﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D9 RID: 985
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("0000000c-0000-0000-c000-000000000046")]
	[Obsolete]
	[ComImport]
	public interface UCOMIStream
	{
		// Token: 0x06002BD8 RID: 11224
		void Read([MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 1)] [Out] byte[] pv, int cb, IntPtr pcbRead);

		// Token: 0x06002BD9 RID: 11225
		void Write([MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 1)] byte[] pv, int cb, IntPtr pcbWritten);

		// Token: 0x06002BDA RID: 11226
		void Seek(long dlibMove, int dwOrigin, IntPtr plibNewPosition);

		// Token: 0x06002BDB RID: 11227
		void SetSize(long libNewSize);

		// Token: 0x06002BDC RID: 11228
		void CopyTo(UCOMIStream pstm, long cb, IntPtr pcbRead, IntPtr pcbWritten);

		// Token: 0x06002BDD RID: 11229
		void Commit(int grfCommitFlags);

		// Token: 0x06002BDE RID: 11230
		void Revert();

		// Token: 0x06002BDF RID: 11231
		void LockRegion(long libOffset, long cb, int dwLockType);

		// Token: 0x06002BE0 RID: 11232
		void UnlockRegion(long libOffset, long cb, int dwLockType);

		// Token: 0x06002BE1 RID: 11233
		void Stat(out STATSTG pstatstg, int grfStatFlag);

		// Token: 0x06002BE2 RID: 11234
		void Clone(out UCOMIStream ppstm);
	}
}
