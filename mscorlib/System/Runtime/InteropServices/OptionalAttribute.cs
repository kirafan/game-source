﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200004D RID: 77
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	[ComVisible(true)]
	public sealed class OptionalAttribute : Attribute
	{
	}
}
