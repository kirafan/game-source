﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200037C RID: 892
	[ComVisible(true)]
	[Serializable]
	public enum ComInterfaceType
	{
		// Token: 0x040010E1 RID: 4321
		InterfaceIsDual,
		// Token: 0x040010E2 RID: 4322
		InterfaceIsIUnknown,
		// Token: 0x040010E3 RID: 4323
		InterfaceIsIDispatch
	}
}
