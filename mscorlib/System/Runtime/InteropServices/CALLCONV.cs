﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000371 RID: 881
	[Obsolete]
	[Serializable]
	public enum CALLCONV
	{
		// Token: 0x040010BE RID: 4286
		CC_CDECL = 1,
		// Token: 0x040010BF RID: 4287
		CC_PASCAL,
		// Token: 0x040010C0 RID: 4288
		CC_MSCPASCAL = 2,
		// Token: 0x040010C1 RID: 4289
		CC_MACPASCAL,
		// Token: 0x040010C2 RID: 4290
		CC_STDCALL,
		// Token: 0x040010C3 RID: 4291
		CC_RESERVED,
		// Token: 0x040010C4 RID: 4292
		CC_SYSCALL,
		// Token: 0x040010C5 RID: 4293
		CC_MPWCDECL,
		// Token: 0x040010C6 RID: 4294
		CC_MPWPASCAL,
		// Token: 0x040010C7 RID: 4295
		CC_MAX
	}
}
