﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003BA RID: 954
	[ComVisible(true)]
	[Serializable]
	public class SafeArrayTypeMismatchException : SystemException
	{
		// Token: 0x06002B6D RID: 11117 RVA: 0x00093A5C File Offset: 0x00091C5C
		public SafeArrayTypeMismatchException() : base(Locale.GetText("The incoming SAVEARRAY does not match the expected managed signature"))
		{
			base.HResult = -2146233037;
		}

		// Token: 0x06002B6E RID: 11118 RVA: 0x00093A7C File Offset: 0x00091C7C
		public SafeArrayTypeMismatchException(string message) : base(message)
		{
			base.HResult = -2146233037;
		}

		// Token: 0x06002B6F RID: 11119 RVA: 0x00093A90 File Offset: 0x00091C90
		public SafeArrayTypeMismatchException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233037;
		}

		// Token: 0x06002B70 RID: 11120 RVA: 0x00093AA8 File Offset: 0x00091CA8
		protected SafeArrayTypeMismatchException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04001196 RID: 4502
		private const int ErrorCode = -2146233037;
	}
}
