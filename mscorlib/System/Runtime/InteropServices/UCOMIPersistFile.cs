﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D7 RID: 983
	[Obsolete]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("0000010b-0000-0000-c000-000000000046")]
	[ComImport]
	public interface UCOMIPersistFile
	{
		// Token: 0x06002BCB RID: 11211
		void GetClassID(out Guid pClassID);

		// Token: 0x06002BCC RID: 11212
		[PreserveSig]
		int IsDirty();

		// Token: 0x06002BCD RID: 11213
		void Load([MarshalAs(UnmanagedType.LPWStr)] string pszFileName, int dwMode);

		// Token: 0x06002BCE RID: 11214
		void Save([MarshalAs(UnmanagedType.LPWStr)] string pszFileName, [MarshalAs(UnmanagedType.Bool)] bool fRemember);

		// Token: 0x06002BCF RID: 11215
		void SaveCompleted([MarshalAs(UnmanagedType.LPWStr)] string pszFileName);

		// Token: 0x06002BD0 RID: 11216
		void GetCurFile([MarshalAs(UnmanagedType.LPWStr)] out string ppszFileName);
	}
}
