﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003A7 RID: 935
	[ComVisible(true)]
	[Serializable]
	public class InvalidComObjectException : SystemException
	{
		// Token: 0x06002A96 RID: 10902 RVA: 0x00092A9C File Offset: 0x00090C9C
		public InvalidComObjectException() : base(Locale.GetText("Invalid COM object is used"))
		{
			base.HResult = -2146233049;
		}

		// Token: 0x06002A97 RID: 10903 RVA: 0x00092ABC File Offset: 0x00090CBC
		public InvalidComObjectException(string message) : base(message)
		{
			base.HResult = -2146233049;
		}

		// Token: 0x06002A98 RID: 10904 RVA: 0x00092AD0 File Offset: 0x00090CD0
		public InvalidComObjectException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233049;
		}

		// Token: 0x06002A99 RID: 10905 RVA: 0x00092AE8 File Offset: 0x00090CE8
		protected InvalidComObjectException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04001154 RID: 4436
		private const int ErrorCode = -2146233049;
	}
}
