﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200004C RID: 76
	[AttributeUsage(AttributeTargets.Interface, Inherited = false)]
	[ComVisible(true)]
	public sealed class CoClassAttribute : Attribute
	{
		// Token: 0x06000671 RID: 1649 RVA: 0x00014B94 File Offset: 0x00012D94
		public CoClassAttribute(Type coClass)
		{
			this.klass = coClass;
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000672 RID: 1650 RVA: 0x00014BA4 File Offset: 0x00012DA4
		public Type CoClass
		{
			get
			{
				return this.klass;
			}
		}

		// Token: 0x040000A0 RID: 160
		private Type klass;
	}
}
