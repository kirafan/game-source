﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003AE RID: 942
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct PARAMDESC
	{
		// Token: 0x04001163 RID: 4451
		public IntPtr lpVarValue;

		// Token: 0x04001164 RID: 4452
		public PARAMFLAG wParamFlags;
	}
}
