﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000378 RID: 888
	[AttributeUsage(AttributeTargets.All, Inherited = false)]
	[ComVisible(true)]
	public sealed class ComConversionLossAttribute : Attribute
	{
	}
}
