﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000358 RID: 856
	[Guid("CE1A3BF5-975E-30CC-97C9-1EF70F8F3993")]
	[ComVisible(true)]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[TypeLibImportClass(typeof(FieldBuilder))]
	public interface _FieldBuilder
	{
		// Token: 0x0600294F RID: 10575
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06002950 RID: 10576
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x06002951 RID: 10577
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x06002952 RID: 10578
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
