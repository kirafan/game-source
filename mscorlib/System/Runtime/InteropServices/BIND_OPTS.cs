﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200036E RID: 878
	[Obsolete]
	public struct BIND_OPTS
	{
		// Token: 0x040010B5 RID: 4277
		public int cbStruct;

		// Token: 0x040010B6 RID: 4278
		public int grfFlags;

		// Token: 0x040010B7 RID: 4279
		public int grfMode;

		// Token: 0x040010B8 RID: 4280
		public int dwTickCountDeadline;
	}
}
