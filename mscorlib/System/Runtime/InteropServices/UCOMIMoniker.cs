﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003D6 RID: 982
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("0000000f-0000-0000-c000-000000000046")]
	[Obsolete]
	[ComImport]
	public interface UCOMIMoniker
	{
		// Token: 0x06002BB7 RID: 11191
		void GetClassID(out Guid pClassID);

		// Token: 0x06002BB8 RID: 11192
		[PreserveSig]
		int IsDirty();

		// Token: 0x06002BB9 RID: 11193
		void Load(UCOMIStream pStm);

		// Token: 0x06002BBA RID: 11194
		void Save(UCOMIStream pStm, [MarshalAs(UnmanagedType.Bool)] bool fClearDirty);

		// Token: 0x06002BBB RID: 11195
		void GetSizeMax(out long pcbSize);

		// Token: 0x06002BBC RID: 11196
		void BindToObject(UCOMIBindCtx pbc, UCOMIMoniker pmkToLeft, [In] ref Guid riidResult, [MarshalAs(UnmanagedType.Interface)] out object ppvResult);

		// Token: 0x06002BBD RID: 11197
		void BindToStorage(UCOMIBindCtx pbc, UCOMIMoniker pmkToLeft, [In] ref Guid riid, [MarshalAs(UnmanagedType.Interface)] out object ppvObj);

		// Token: 0x06002BBE RID: 11198
		void Reduce(UCOMIBindCtx pbc, int dwReduceHowFar, ref UCOMIMoniker ppmkToLeft, out UCOMIMoniker ppmkReduced);

		// Token: 0x06002BBF RID: 11199
		void ComposeWith(UCOMIMoniker pmkRight, [MarshalAs(UnmanagedType.Bool)] bool fOnlyIfNotGeneric, out UCOMIMoniker ppmkComposite);

		// Token: 0x06002BC0 RID: 11200
		void Enum([MarshalAs(UnmanagedType.Bool)] bool fForward, out UCOMIEnumMoniker ppenumMoniker);

		// Token: 0x06002BC1 RID: 11201
		void IsEqual(UCOMIMoniker pmkOtherMoniker);

		// Token: 0x06002BC2 RID: 11202
		void Hash(out int pdwHash);

		// Token: 0x06002BC3 RID: 11203
		void IsRunning(UCOMIBindCtx pbc, UCOMIMoniker pmkToLeft, UCOMIMoniker pmkNewlyRunning);

		// Token: 0x06002BC4 RID: 11204
		void GetTimeOfLastChange(UCOMIBindCtx pbc, UCOMIMoniker pmkToLeft, out FILETIME pFileTime);

		// Token: 0x06002BC5 RID: 11205
		void Inverse(out UCOMIMoniker ppmk);

		// Token: 0x06002BC6 RID: 11206
		void CommonPrefixWith(UCOMIMoniker pmkOther, out UCOMIMoniker ppmkPrefix);

		// Token: 0x06002BC7 RID: 11207
		void RelativePathTo(UCOMIMoniker pmkOther, out UCOMIMoniker ppmkRelPath);

		// Token: 0x06002BC8 RID: 11208
		void GetDisplayName(UCOMIBindCtx pbc, UCOMIMoniker pmkToLeft, [MarshalAs(UnmanagedType.LPWStr)] out string ppszDisplayName);

		// Token: 0x06002BC9 RID: 11209
		void ParseDisplayName(UCOMIBindCtx pbc, UCOMIMoniker pmkToLeft, [MarshalAs(UnmanagedType.LPWStr)] string pszDisplayName, out int pchEaten, out UCOMIMoniker ppmkOut);

		// Token: 0x06002BCA RID: 11210
		void IsSystemMoniker(out int pdwMksys);
	}
}
