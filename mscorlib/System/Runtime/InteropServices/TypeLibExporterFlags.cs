﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003C4 RID: 964
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum TypeLibExporterFlags
	{
		// Token: 0x040011D5 RID: 4565
		OnlyReferenceRegistered = 1,
		// Token: 0x040011D6 RID: 4566
		None = 0,
		// Token: 0x040011D7 RID: 4567
		CallerResolvedReferences = 2,
		// Token: 0x040011D8 RID: 4568
		OldNames = 4,
		// Token: 0x040011D9 RID: 4569
		ExportAs32Bit = 16,
		// Token: 0x040011DA RID: 4570
		ExportAs64Bit = 32
	}
}
