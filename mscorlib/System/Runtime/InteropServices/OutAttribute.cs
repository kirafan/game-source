﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200003E RID: 62
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Parameter, Inherited = false)]
	public sealed class OutAttribute : Attribute
	{
	}
}
