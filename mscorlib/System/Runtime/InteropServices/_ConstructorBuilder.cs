﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000352 RID: 850
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[CLSCompliant(false)]
	[ComVisible(true)]
	[Guid("ED3E4384-D7E2-3FA7-8FFD-8940D330519A")]
	[TypeLibImportClass(typeof(ConstructorBuilder))]
	public interface _ConstructorBuilder
	{
		// Token: 0x060028FF RID: 10495
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06002900 RID: 10496
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x06002901 RID: 10497
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x06002902 RID: 10498
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
