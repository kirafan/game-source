﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000376 RID: 886
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Parameter | AttributeTargets.ReturnValue, Inherited = false)]
	public sealed class ComAliasNameAttribute : Attribute
	{
		// Token: 0x06002A20 RID: 10784 RVA: 0x000922F8 File Offset: 0x000904F8
		public ComAliasNameAttribute(string alias)
		{
			this.val = alias;
		}

		// Token: 0x170007C1 RID: 1985
		// (get) Token: 0x06002A21 RID: 10785 RVA: 0x00092308 File Offset: 0x00090508
		public string Value
		{
			get
			{
				return this.val;
			}
		}

		// Token: 0x040010D8 RID: 4312
		private string val;
	}
}
