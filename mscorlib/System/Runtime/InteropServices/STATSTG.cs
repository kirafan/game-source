﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B5 RID: 949
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct STATSTG
	{
		// Token: 0x04001189 RID: 4489
		public string pwcsName;

		// Token: 0x0400118A RID: 4490
		public int type;

		// Token: 0x0400118B RID: 4491
		public long cbSize;

		// Token: 0x0400118C RID: 4492
		public FILETIME mtime;

		// Token: 0x0400118D RID: 4493
		public FILETIME ctime;

		// Token: 0x0400118E RID: 4494
		public FILETIME atime;

		// Token: 0x0400118F RID: 4495
		public int grfMode;

		// Token: 0x04001190 RID: 4496
		public int grfLocksSupported;

		// Token: 0x04001191 RID: 4497
		public Guid clsid;

		// Token: 0x04001192 RID: 4498
		public int grfStateBits;

		// Token: 0x04001193 RID: 4499
		public int reserved;
	}
}
