﻿using System;
using System.Reflection;

namespace System.Runtime.InteropServices.Expando
{
	// Token: 0x02000415 RID: 1045
	[Guid("afbf15e6-c37c-11d2-b88e-00a0c9b471b8")]
	[ComVisible(true)]
	public interface IExpando : IReflect
	{
		// Token: 0x06002CA8 RID: 11432
		FieldInfo AddField(string name);

		// Token: 0x06002CA9 RID: 11433
		MethodInfo AddMethod(string name, Delegate method);

		// Token: 0x06002CAA RID: 11434
		PropertyInfo AddProperty(string name);

		// Token: 0x06002CAB RID: 11435
		void RemoveMember(MemberInfo m);
	}
}
