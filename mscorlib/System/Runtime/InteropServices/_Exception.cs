﻿using System;
using System.Reflection;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200003A RID: 58
	[ComVisible(true)]
	[InterfaceType(ComInterfaceType.InterfaceIsDual)]
	[Guid("b36b5c63-42ef-38bc-a07e-0b34c98f164a")]
	[CLSCompliant(false)]
	public interface _Exception
	{
		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x0600062B RID: 1579
		// (set) Token: 0x0600062C RID: 1580
		string HelpLink { get; set; }

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x0600062D RID: 1581
		Exception InnerException { get; }

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x0600062E RID: 1582
		string Message { get; }

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x0600062F RID: 1583
		// (set) Token: 0x06000630 RID: 1584
		string Source { get; set; }

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000631 RID: 1585
		string StackTrace { get; }

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000632 RID: 1586
		MethodBase TargetSite { get; }

		// Token: 0x06000633 RID: 1587
		bool Equals(object obj);

		// Token: 0x06000634 RID: 1588
		Exception GetBaseException();

		// Token: 0x06000635 RID: 1589
		int GetHashCode();

		// Token: 0x06000636 RID: 1590
		void GetObjectData(SerializationInfo info, StreamingContext context);

		// Token: 0x06000637 RID: 1591
		Type GetType();

		// Token: 0x06000638 RID: 1592
		string ToString();
	}
}
