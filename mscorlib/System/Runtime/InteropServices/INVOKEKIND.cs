﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200039E RID: 926
	[Obsolete]
	[Serializable]
	public enum INVOKEKIND
	{
		// Token: 0x0400114A RID: 4426
		INVOKE_FUNC = 1,
		// Token: 0x0400114B RID: 4427
		INVOKE_PROPERTYGET,
		// Token: 0x0400114C RID: 4428
		INVOKE_PROPERTYPUT = 4,
		// Token: 0x0400114D RID: 4429
		INVOKE_PROPERTYPUTREF = 8
	}
}
