﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200036B RID: 875
	[ComVisible(true)]
	[Flags]
	public enum AssemblyRegistrationFlags
	{
		// Token: 0x040010B0 RID: 4272
		None = 0,
		// Token: 0x040010B1 RID: 4273
		SetCodeBase = 1
	}
}
