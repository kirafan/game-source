﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000395 RID: 917
	[ComVisible(true)]
	public struct HandleRef
	{
		// Token: 0x06002A71 RID: 10865 RVA: 0x000929FC File Offset: 0x00090BFC
		public HandleRef(object wrapper, IntPtr handle)
		{
			this.wrapper = wrapper;
			this.handle = handle;
		}

		// Token: 0x170007D3 RID: 2003
		// (get) Token: 0x06002A72 RID: 10866 RVA: 0x00092A0C File Offset: 0x00090C0C
		public IntPtr Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x170007D4 RID: 2004
		// (get) Token: 0x06002A73 RID: 10867 RVA: 0x00092A14 File Offset: 0x00090C14
		public object Wrapper
		{
			get
			{
				return this.wrapper;
			}
		}

		// Token: 0x06002A74 RID: 10868 RVA: 0x00092A1C File Offset: 0x00090C1C
		public static IntPtr ToIntPtr(HandleRef value)
		{
			return value.Handle;
		}

		// Token: 0x06002A75 RID: 10869 RVA: 0x00092A28 File Offset: 0x00090C28
		public static explicit operator IntPtr(HandleRef value)
		{
			return value.Handle;
		}

		// Token: 0x04001135 RID: 4405
		private object wrapper;

		// Token: 0x04001136 RID: 4406
		private IntPtr handle;
	}
}
