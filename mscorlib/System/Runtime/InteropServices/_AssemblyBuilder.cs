﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000350 RID: 848
	[ComVisible(true)]
	[TypeLibImportClass(typeof(AssemblyBuilder))]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("BEBB2505-8B54-3443-AEAD-142A16DD9CC7")]
	public interface _AssemblyBuilder
	{
		// Token: 0x060028F7 RID: 10487
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060028F8 RID: 10488
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060028F9 RID: 10489
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060028FA RID: 10490
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
