﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003AB RID: 939
	[ComVisible(true)]
	[Serializable]
	public enum LayoutKind
	{
		// Token: 0x0400115D RID: 4445
		Sequential,
		// Token: 0x0400115E RID: 4446
		Explicit = 2,
		// Token: 0x0400115F RID: 4447
		Auto
	}
}
