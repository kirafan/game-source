﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003DE RID: 990
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Delegate, Inherited = false, AllowMultiple = false)]
	public sealed class UnmanagedFunctionPointerAttribute : Attribute
	{
		// Token: 0x06002C04 RID: 11268 RVA: 0x00093E0C File Offset: 0x0009200C
		public UnmanagedFunctionPointerAttribute(CallingConvention callingConvention)
		{
			this.call_conv = callingConvention;
		}

		// Token: 0x170007E6 RID: 2022
		// (get) Token: 0x06002C05 RID: 11269 RVA: 0x00093E1C File Offset: 0x0009201C
		public CallingConvention CallingConvention
		{
			get
			{
				return this.call_conv;
			}
		}

		// Token: 0x0400121A RID: 4634
		private CallingConvention call_conv;

		// Token: 0x0400121B RID: 4635
		public CharSet CharSet;

		// Token: 0x0400121C RID: 4636
		public bool SetLastError;

		// Token: 0x0400121D RID: 4637
		public bool BestFitMapping;

		// Token: 0x0400121E RID: 4638
		public bool ThrowOnUnmappableChar;
	}
}
