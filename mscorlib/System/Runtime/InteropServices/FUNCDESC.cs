﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000390 RID: 912
	[Obsolete]
	public struct FUNCDESC
	{
		// Token: 0x0400110F RID: 4367
		public int memid;

		// Token: 0x04001110 RID: 4368
		public IntPtr lprgscode;

		// Token: 0x04001111 RID: 4369
		public IntPtr lprgelemdescParam;

		// Token: 0x04001112 RID: 4370
		public FUNCKIND funckind;

		// Token: 0x04001113 RID: 4371
		public INVOKEKIND invkind;

		// Token: 0x04001114 RID: 4372
		public CALLCONV callconv;

		// Token: 0x04001115 RID: 4373
		public short cParams;

		// Token: 0x04001116 RID: 4374
		public short cParamsOpt;

		// Token: 0x04001117 RID: 4375
		public short oVft;

		// Token: 0x04001118 RID: 4376
		public short cScodes;

		// Token: 0x04001119 RID: 4377
		public ELEMDESC elemdescFunc;

		// Token: 0x0400111A RID: 4378
		public short wFuncFlags;
	}
}
