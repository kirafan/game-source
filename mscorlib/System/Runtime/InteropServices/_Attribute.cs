﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000005 RID: 5
	[TypeLibImportClass(typeof(Attribute))]
	[ComVisible(true)]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("917B14D0-2D9E-38B8-92A9-381ACF52F7C0")]
	public interface _Attribute
	{
		// Token: 0x06000041 RID: 65
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x06000042 RID: 66
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x06000043 RID: 67
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x06000044 RID: 68
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
