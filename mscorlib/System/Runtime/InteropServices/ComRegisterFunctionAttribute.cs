﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200037E RID: 894
	[AttributeUsage(AttributeTargets.Method, Inherited = false)]
	[ComVisible(true)]
	public sealed class ComRegisterFunctionAttribute : Attribute
	{
	}
}
