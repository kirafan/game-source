﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200037F RID: 895
	[AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = true)]
	[ComVisible(true)]
	public sealed class ComSourceInterfacesAttribute : Attribute
	{
		// Token: 0x06002A34 RID: 10804 RVA: 0x00092468 File Offset: 0x00090668
		public ComSourceInterfacesAttribute(string sourceInterfaces)
		{
			this.internalValue = sourceInterfaces;
		}

		// Token: 0x06002A35 RID: 10805 RVA: 0x00092478 File Offset: 0x00090678
		public ComSourceInterfacesAttribute(Type sourceInterface)
		{
			this.internalValue = sourceInterface.ToString();
		}

		// Token: 0x06002A36 RID: 10806 RVA: 0x0009248C File Offset: 0x0009068C
		public ComSourceInterfacesAttribute(Type sourceInterface1, Type sourceInterface2)
		{
			this.internalValue = sourceInterface1.ToString() + sourceInterface2.ToString();
		}

		// Token: 0x06002A37 RID: 10807 RVA: 0x000924B8 File Offset: 0x000906B8
		public ComSourceInterfacesAttribute(Type sourceInterface1, Type sourceInterface2, Type sourceInterface3)
		{
			this.internalValue = sourceInterface1.ToString() + sourceInterface2.ToString() + sourceInterface3.ToString();
		}

		// Token: 0x06002A38 RID: 10808 RVA: 0x000924E8 File Offset: 0x000906E8
		public ComSourceInterfacesAttribute(Type sourceInterface1, Type sourceInterface2, Type sourceInterface3, Type sourceInterface4)
		{
			this.internalValue = sourceInterface1.ToString() + sourceInterface2.ToString() + sourceInterface3.ToString() + sourceInterface4.ToString();
		}

		// Token: 0x170007C9 RID: 1993
		// (get) Token: 0x06002A39 RID: 10809 RVA: 0x00092520 File Offset: 0x00090720
		public string Value
		{
			get
			{
				return this.internalValue;
			}
		}

		// Token: 0x040010E8 RID: 4328
		private string internalValue;
	}
}
