﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000375 RID: 885
	[ComVisible(true)]
	[Serializable]
	public enum ClassInterfaceType
	{
		// Token: 0x040010D5 RID: 4309
		None,
		// Token: 0x040010D6 RID: 4310
		AutoDispatch,
		// Token: 0x040010D7 RID: 4311
		AutoDual
	}
}
