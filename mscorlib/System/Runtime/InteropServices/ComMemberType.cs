﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200037D RID: 893
	[ComVisible(true)]
	[Serializable]
	public enum ComMemberType
	{
		// Token: 0x040010E5 RID: 4325
		Method,
		// Token: 0x040010E6 RID: 4326
		PropGet,
		// Token: 0x040010E7 RID: 4327
		PropSet
	}
}
