﻿using System;
using System.Reflection.Emit;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000362 RID: 866
	[TypeLibImportClass(typeof(ParameterBuilder))]
	[Guid("36329EBA-F97A-3565-BC07-0ED5C6EF19FC")]
	[ComVisible(true)]
	[CLSCompliant(false)]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	public interface _ParameterBuilder
	{
		// Token: 0x060029D7 RID: 10711
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060029D8 RID: 10712
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060029D9 RID: 10713
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060029DA RID: 10714
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);
	}
}
