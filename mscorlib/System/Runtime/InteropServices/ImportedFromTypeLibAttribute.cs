﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003A4 RID: 932
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class ImportedFromTypeLibAttribute : Attribute
	{
		// Token: 0x06002A91 RID: 10897 RVA: 0x00092A5C File Offset: 0x00090C5C
		public ImportedFromTypeLibAttribute(string tlbFile)
		{
			this.TlbFile = tlbFile;
		}

		// Token: 0x170007D6 RID: 2006
		// (get) Token: 0x06002A92 RID: 10898 RVA: 0x00092A6C File Offset: 0x00090C6C
		public string Value
		{
			get
			{
				return this.TlbFile;
			}
		}

		// Token: 0x0400114E RID: 4430
		private string TlbFile;
	}
}
