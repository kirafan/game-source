﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200038C RID: 908
	[ComVisible(true)]
	[Serializable]
	public enum ExporterEventKind
	{
		// Token: 0x04001109 RID: 4361
		NOTIF_TYPECONVERTED,
		// Token: 0x0400110A RID: 4362
		NOTIF_CONVERTWARNING,
		// Token: 0x0400110B RID: 4363
		ERROR_REFTOINVALIDASSEMBLY
	}
}
