﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200038F RID: 911
	[Obsolete]
	public struct FILETIME
	{
		// Token: 0x0400110D RID: 4365
		public int dwLowDateTime;

		// Token: 0x0400110E RID: 4366
		public int dwHighDateTime;
	}
}
