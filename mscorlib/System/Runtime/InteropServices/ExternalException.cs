﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200038E RID: 910
	[ComVisible(true)]
	[Serializable]
	public class ExternalException : SystemException
	{
		// Token: 0x06002A54 RID: 10836 RVA: 0x0009279C File Offset: 0x0009099C
		public ExternalException() : base(Locale.GetText("External exception"))
		{
			base.HResult = -2147467259;
		}

		// Token: 0x06002A55 RID: 10837 RVA: 0x000927BC File Offset: 0x000909BC
		public ExternalException(string message) : base(message)
		{
			base.HResult = -2147467259;
		}

		// Token: 0x06002A56 RID: 10838 RVA: 0x000927D0 File Offset: 0x000909D0
		protected ExternalException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06002A57 RID: 10839 RVA: 0x000927DC File Offset: 0x000909DC
		public ExternalException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2147467259;
		}

		// Token: 0x06002A58 RID: 10840 RVA: 0x000927F4 File Offset: 0x000909F4
		public ExternalException(string message, int errorCode) : base(message)
		{
			base.HResult = errorCode;
		}

		// Token: 0x170007D0 RID: 2000
		// (get) Token: 0x06002A59 RID: 10841 RVA: 0x00092804 File Offset: 0x00090A04
		public virtual int ErrorCode
		{
			get
			{
				return base.HResult;
			}
		}
	}
}
