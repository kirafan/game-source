﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003C7 RID: 967
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Interface, Inherited = false)]
	public sealed class TypeLibImportClassAttribute : Attribute
	{
		// Token: 0x06002B87 RID: 11143 RVA: 0x00093D60 File Offset: 0x00091F60
		public TypeLibImportClassAttribute(Type importClass)
		{
			this._importClass = importClass.ToString();
		}

		// Token: 0x170007E0 RID: 2016
		// (get) Token: 0x06002B88 RID: 11144 RVA: 0x00093D74 File Offset: 0x00091F74
		public string Value
		{
			get
			{
				return this._importClass;
			}
		}

		// Token: 0x040011EA RID: 4586
		private string _importClass;
	}
}
