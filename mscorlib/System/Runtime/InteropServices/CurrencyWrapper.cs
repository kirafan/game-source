﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000383 RID: 899
	[ComVisible(true)]
	[Serializable]
	public sealed class CurrencyWrapper
	{
		// Token: 0x06002A45 RID: 10821 RVA: 0x00092608 File Offset: 0x00090808
		public CurrencyWrapper(decimal obj)
		{
			this.currency = obj;
		}

		// Token: 0x06002A46 RID: 10822 RVA: 0x00092618 File Offset: 0x00090818
		public CurrencyWrapper(object obj)
		{
			if (obj.GetType() != typeof(decimal))
			{
				throw new ArgumentException("obj has to be a Decimal type");
			}
			this.currency = (decimal)obj;
		}

		// Token: 0x170007CC RID: 1996
		// (get) Token: 0x06002A47 RID: 10823 RVA: 0x00092658 File Offset: 0x00090858
		public decimal WrappedObject
		{
			get
			{
				return this.currency;
			}
		}

		// Token: 0x040010ED RID: 4333
		private decimal currency;
	}
}
