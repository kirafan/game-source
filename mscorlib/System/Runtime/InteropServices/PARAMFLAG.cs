﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003AF RID: 943
	[Obsolete]
	[Flags]
	[Serializable]
	public enum PARAMFLAG
	{
		// Token: 0x04001166 RID: 4454
		PARAMFLAG_NONE = 0,
		// Token: 0x04001167 RID: 4455
		PARAMFLAG_FIN = 1,
		// Token: 0x04001168 RID: 4456
		PARAMFLAG_FOUT = 2,
		// Token: 0x04001169 RID: 4457
		PARAMFLAG_FLCID = 4,
		// Token: 0x0400116A RID: 4458
		PARAMFLAG_FRETVAL = 8,
		// Token: 0x0400116B RID: 4459
		PARAMFLAG_FOPT = 16,
		// Token: 0x0400116C RID: 4460
		PARAMFLAG_FHASDEFAULT = 32,
		// Token: 0x0400116D RID: 4461
		PARAMFLAG_FHASCUSTDATA = 64
	}
}
