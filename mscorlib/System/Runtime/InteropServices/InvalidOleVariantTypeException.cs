﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003A8 RID: 936
	[ComVisible(true)]
	[Serializable]
	public class InvalidOleVariantTypeException : SystemException
	{
		// Token: 0x06002A9A RID: 10906 RVA: 0x00092AF4 File Offset: 0x00090CF4
		public InvalidOleVariantTypeException() : base(Locale.GetText("Found native variant type cannot be marshalled to managed code"))
		{
			base.HResult = -2146233039;
		}

		// Token: 0x06002A9B RID: 10907 RVA: 0x00092B14 File Offset: 0x00090D14
		public InvalidOleVariantTypeException(string message) : base(message)
		{
			base.HResult = -2146233039;
		}

		// Token: 0x06002A9C RID: 10908 RVA: 0x00092B28 File Offset: 0x00090D28
		public InvalidOleVariantTypeException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233039;
		}

		// Token: 0x06002A9D RID: 10909 RVA: 0x00092B40 File Offset: 0x00090D40
		protected InvalidOleVariantTypeException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04001155 RID: 4437
		private const int ErrorCode = -2146233039;
	}
}
