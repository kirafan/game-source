﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000369 RID: 873
	[AttributeUsage(AttributeTargets.Method, AllowMultiple = false, Inherited = false)]
	public sealed class AllowReversePInvokeCallsAttribute : Attribute
	{
	}
}
