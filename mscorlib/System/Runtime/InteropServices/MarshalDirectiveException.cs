﻿using System;
using System.Runtime.Serialization;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003AD RID: 941
	[ComVisible(true)]
	[Serializable]
	public class MarshalDirectiveException : SystemException
	{
		// Token: 0x06002B4A RID: 11082 RVA: 0x0009381C File Offset: 0x00091A1C
		public MarshalDirectiveException() : base(Locale.GetText("Unsupported MarshalAsAttribute found"))
		{
			base.HResult = -2146233035;
		}

		// Token: 0x06002B4B RID: 11083 RVA: 0x0009383C File Offset: 0x00091A3C
		public MarshalDirectiveException(string message) : base(message)
		{
			base.HResult = -2146233035;
		}

		// Token: 0x06002B4C RID: 11084 RVA: 0x00093850 File Offset: 0x00091A50
		public MarshalDirectiveException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233035;
		}

		// Token: 0x06002B4D RID: 11085 RVA: 0x00093868 File Offset: 0x00091A68
		protected MarshalDirectiveException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04001162 RID: 4450
		private const int ErrorCode = -2146233035;
	}
}
