﻿using System;
using System.Globalization;
using System.Reflection;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000038 RID: 56
	[ComVisible(true)]
	[Guid("BCA8B44D-AAD6-3A86-8AB7-03349F4F2DA2")]
	[TypeLibImportClass(typeof(Type))]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[CLSCompliant(false)]
	public interface _Type
	{
		// Token: 0x060005A3 RID: 1443
		bool Equals(object other);

		// Token: 0x060005A4 RID: 1444
		bool Equals(Type o);

		// Token: 0x060005A5 RID: 1445
		Type[] FindInterfaces(TypeFilter filter, object filterCriteria);

		// Token: 0x060005A6 RID: 1446
		MemberInfo[] FindMembers(MemberTypes memberType, BindingFlags bindingAttr, MemberFilter filter, object filterCriteria);

		// Token: 0x060005A7 RID: 1447
		int GetArrayRank();

		// Token: 0x060005A8 RID: 1448
		ConstructorInfo GetConstructor(Type[] types);

		// Token: 0x060005A9 RID: 1449
		ConstructorInfo GetConstructor(BindingFlags bindingAttr, Binder binder, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x060005AA RID: 1450
		ConstructorInfo GetConstructor(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x060005AB RID: 1451
		ConstructorInfo[] GetConstructors();

		// Token: 0x060005AC RID: 1452
		ConstructorInfo[] GetConstructors(BindingFlags bindingAttr);

		// Token: 0x060005AD RID: 1453
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x060005AE RID: 1454
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x060005AF RID: 1455
		MemberInfo[] GetDefaultMembers();

		// Token: 0x060005B0 RID: 1456
		Type GetElementType();

		// Token: 0x060005B1 RID: 1457
		EventInfo GetEvent(string name);

		// Token: 0x060005B2 RID: 1458
		EventInfo GetEvent(string name, BindingFlags bindingAttr);

		// Token: 0x060005B3 RID: 1459
		EventInfo[] GetEvents();

		// Token: 0x060005B4 RID: 1460
		EventInfo[] GetEvents(BindingFlags bindingAttr);

		// Token: 0x060005B5 RID: 1461
		FieldInfo GetField(string name);

		// Token: 0x060005B6 RID: 1462
		FieldInfo GetField(string name, BindingFlags bindingAttr);

		// Token: 0x060005B7 RID: 1463
		FieldInfo[] GetFields();

		// Token: 0x060005B8 RID: 1464
		FieldInfo[] GetFields(BindingFlags bindingAttr);

		// Token: 0x060005B9 RID: 1465
		int GetHashCode();

		// Token: 0x060005BA RID: 1466
		void GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId);

		// Token: 0x060005BB RID: 1467
		void GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo);

		// Token: 0x060005BC RID: 1468
		void GetTypeInfoCount(out uint pcTInfo);

		// Token: 0x060005BD RID: 1469
		void Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr);

		// Token: 0x060005BE RID: 1470
		Type GetInterface(string name);

		// Token: 0x060005BF RID: 1471
		Type GetInterface(string name, bool ignoreCase);

		// Token: 0x060005C0 RID: 1472
		InterfaceMapping GetInterfaceMap(Type interfaceType);

		// Token: 0x060005C1 RID: 1473
		Type[] GetInterfaces();

		// Token: 0x060005C2 RID: 1474
		MemberInfo[] GetMember(string name);

		// Token: 0x060005C3 RID: 1475
		MemberInfo[] GetMember(string name, MemberTypes type, BindingFlags bindingAttr);

		// Token: 0x060005C4 RID: 1476
		MemberInfo[] GetMember(string name, BindingFlags bindingAttr);

		// Token: 0x060005C5 RID: 1477
		MemberInfo[] GetMembers();

		// Token: 0x060005C6 RID: 1478
		MemberInfo[] GetMembers(BindingFlags bindingAttr);

		// Token: 0x060005C7 RID: 1479
		MethodInfo GetMethod(string name);

		// Token: 0x060005C8 RID: 1480
		MethodInfo GetMethod(string name, BindingFlags bindingAttr);

		// Token: 0x060005C9 RID: 1481
		MethodInfo GetMethod(string name, Type[] types);

		// Token: 0x060005CA RID: 1482
		MethodInfo GetMethod(string name, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x060005CB RID: 1483
		MethodInfo GetMethod(string name, BindingFlags bindingAttr, Binder binder, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x060005CC RID: 1484
		MethodInfo GetMethod(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x060005CD RID: 1485
		MethodInfo[] GetMethods();

		// Token: 0x060005CE RID: 1486
		MethodInfo[] GetMethods(BindingFlags bindingAttr);

		// Token: 0x060005CF RID: 1487
		Type GetNestedType(string name);

		// Token: 0x060005D0 RID: 1488
		Type GetNestedType(string name, BindingFlags bindingAttr);

		// Token: 0x060005D1 RID: 1489
		Type[] GetNestedTypes();

		// Token: 0x060005D2 RID: 1490
		Type[] GetNestedTypes(BindingFlags bindingAttr);

		// Token: 0x060005D3 RID: 1491
		PropertyInfo[] GetProperties();

		// Token: 0x060005D4 RID: 1492
		PropertyInfo[] GetProperties(BindingFlags bindingAttr);

		// Token: 0x060005D5 RID: 1493
		PropertyInfo GetProperty(string name);

		// Token: 0x060005D6 RID: 1494
		PropertyInfo GetProperty(string name, BindingFlags bindingAttr);

		// Token: 0x060005D7 RID: 1495
		PropertyInfo GetProperty(string name, Type returnType);

		// Token: 0x060005D8 RID: 1496
		PropertyInfo GetProperty(string name, Type[] types);

		// Token: 0x060005D9 RID: 1497
		PropertyInfo GetProperty(string name, Type returnType, Type[] types);

		// Token: 0x060005DA RID: 1498
		PropertyInfo GetProperty(string name, Type returnType, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x060005DB RID: 1499
		PropertyInfo GetProperty(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x060005DC RID: 1500
		Type GetType();

		// Token: 0x060005DD RID: 1501
		object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args);

		// Token: 0x060005DE RID: 1502
		object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, CultureInfo culture);

		// Token: 0x060005DF RID: 1503
		object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters);

		// Token: 0x060005E0 RID: 1504
		bool IsAssignableFrom(Type c);

		// Token: 0x060005E1 RID: 1505
		bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x060005E2 RID: 1506
		bool IsInstanceOfType(object o);

		// Token: 0x060005E3 RID: 1507
		bool IsSubclassOf(Type c);

		// Token: 0x060005E4 RID: 1508
		string ToString();

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060005E5 RID: 1509
		Assembly Assembly { get; }

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060005E6 RID: 1510
		string AssemblyQualifiedName { get; }

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060005E7 RID: 1511
		TypeAttributes Attributes { get; }

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060005E8 RID: 1512
		Type BaseType { get; }

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060005E9 RID: 1513
		Type DeclaringType { get; }

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060005EA RID: 1514
		string FullName { get; }

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060005EB RID: 1515
		Guid GUID { get; }

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060005EC RID: 1516
		bool HasElementType { get; }

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060005ED RID: 1517
		bool IsAbstract { get; }

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x060005EE RID: 1518
		bool IsAnsiClass { get; }

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060005EF RID: 1519
		bool IsArray { get; }

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060005F0 RID: 1520
		bool IsAutoClass { get; }

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060005F1 RID: 1521
		bool IsAutoLayout { get; }

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060005F2 RID: 1522
		bool IsByRef { get; }

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x060005F3 RID: 1523
		bool IsClass { get; }

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x060005F4 RID: 1524
		bool IsCOMObject { get; }

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x060005F5 RID: 1525
		bool IsContextful { get; }

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x060005F6 RID: 1526
		bool IsEnum { get; }

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x060005F7 RID: 1527
		bool IsExplicitLayout { get; }

		// Token: 0x17000080 RID: 128
		// (get) Token: 0x060005F8 RID: 1528
		bool IsImport { get; }

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x060005F9 RID: 1529
		bool IsInterface { get; }

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x060005FA RID: 1530
		bool IsLayoutSequential { get; }

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x060005FB RID: 1531
		bool IsMarshalByRef { get; }

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x060005FC RID: 1532
		bool IsNestedAssembly { get; }

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x060005FD RID: 1533
		bool IsNestedFamANDAssem { get; }

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060005FE RID: 1534
		bool IsNestedFamily { get; }

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x060005FF RID: 1535
		bool IsNestedFamORAssem { get; }

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000600 RID: 1536
		bool IsNestedPrivate { get; }

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000601 RID: 1537
		bool IsNestedPublic { get; }

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000602 RID: 1538
		bool IsNotPublic { get; }

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000603 RID: 1539
		bool IsPointer { get; }

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x06000604 RID: 1540
		bool IsPrimitive { get; }

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000605 RID: 1541
		bool IsPublic { get; }

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000606 RID: 1542
		bool IsSealed { get; }

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000607 RID: 1543
		bool IsSerializable { get; }

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000608 RID: 1544
		bool IsSpecialName { get; }

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x06000609 RID: 1545
		bool IsUnicodeClass { get; }

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x0600060A RID: 1546
		bool IsValueType { get; }

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x0600060B RID: 1547
		MemberTypes MemberType { get; }

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x0600060C RID: 1548
		Module Module { get; }

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x0600060D RID: 1549
		string Name { get; }

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x0600060E RID: 1550
		string Namespace { get; }

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x0600060F RID: 1551
		Type ReflectedType { get; }

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000610 RID: 1552
		RuntimeTypeHandle TypeHandle { get; }

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000611 RID: 1553
		ConstructorInfo TypeInitializer { get; }

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000612 RID: 1554
		Type UnderlyingSystemType { get; }
	}
}
