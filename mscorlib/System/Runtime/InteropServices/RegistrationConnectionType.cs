﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003B3 RID: 947
	[Flags]
	public enum RegistrationConnectionType
	{
		// Token: 0x04001184 RID: 4484
		MultipleUse = 1,
		// Token: 0x04001185 RID: 4485
		MultiSeparate = 2,
		// Token: 0x04001186 RID: 4486
		SingleUse = 0,
		// Token: 0x04001187 RID: 4487
		Suspended = 4,
		// Token: 0x04001188 RID: 4488
		Surrogate = 8
	}
}
