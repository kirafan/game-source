﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003E7 RID: 999
	[Serializable]
	public enum CALLCONV
	{
		// Token: 0x0400128E RID: 4750
		CC_CDECL = 1,
		// Token: 0x0400128F RID: 4751
		CC_PASCAL,
		// Token: 0x04001290 RID: 4752
		CC_MSCPASCAL = 2,
		// Token: 0x04001291 RID: 4753
		CC_MACPASCAL,
		// Token: 0x04001292 RID: 4754
		CC_STDCALL,
		// Token: 0x04001293 RID: 4755
		CC_RESERVED,
		// Token: 0x04001294 RID: 4756
		CC_SYSCALL,
		// Token: 0x04001295 RID: 4757
		CC_MPWCDECL,
		// Token: 0x04001296 RID: 4758
		CC_MPWPASCAL,
		// Token: 0x04001297 RID: 4759
		CC_MAX
	}
}
