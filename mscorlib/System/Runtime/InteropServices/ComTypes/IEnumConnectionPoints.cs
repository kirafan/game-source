﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003F7 RID: 1015
	[Guid("b196b285-bab4-101a-b69c-00aa00341d07")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[ComImport]
	public interface IEnumConnectionPoints
	{
		// Token: 0x06002C19 RID: 11289
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 0)] [Out] IConnectionPoint[] rgelt, IntPtr pceltFetched);

		// Token: 0x06002C1A RID: 11290
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002C1B RID: 11291
		void Reset();

		// Token: 0x06002C1C RID: 11292
		void Clone(out IEnumConnectionPoints ppenum);
	}
}
