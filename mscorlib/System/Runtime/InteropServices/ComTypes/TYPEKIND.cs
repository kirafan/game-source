﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x0200040F RID: 1039
	[Serializable]
	public enum TYPEKIND
	{
		// Token: 0x0400132C RID: 4908
		TKIND_ENUM,
		// Token: 0x0400132D RID: 4909
		TKIND_RECORD,
		// Token: 0x0400132E RID: 4910
		TKIND_MODULE,
		// Token: 0x0400132F RID: 4911
		TKIND_INTERFACE,
		// Token: 0x04001330 RID: 4912
		TKIND_DISPATCH,
		// Token: 0x04001331 RID: 4913
		TKIND_COCLASS,
		// Token: 0x04001332 RID: 4914
		TKIND_ALIAS,
		// Token: 0x04001333 RID: 4915
		TKIND_UNION,
		// Token: 0x04001334 RID: 4916
		TKIND_MAX
	}
}
