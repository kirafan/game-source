﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003EF RID: 1007
	public struct FUNCDESC
	{
		// Token: 0x040012B4 RID: 4788
		public int memid;

		// Token: 0x040012B5 RID: 4789
		public IntPtr lprgscode;

		// Token: 0x040012B6 RID: 4790
		public IntPtr lprgelemdescParam;

		// Token: 0x040012B7 RID: 4791
		public FUNCKIND funckind;

		// Token: 0x040012B8 RID: 4792
		public INVOKEKIND invkind;

		// Token: 0x040012B9 RID: 4793
		public CALLCONV callconv;

		// Token: 0x040012BA RID: 4794
		public short cParams;

		// Token: 0x040012BB RID: 4795
		public short cParamsOpt;

		// Token: 0x040012BC RID: 4796
		public short oVft;

		// Token: 0x040012BD RID: 4797
		public short cScodes;

		// Token: 0x040012BE RID: 4798
		public ELEMDESC elemdescFunc;

		// Token: 0x040012BF RID: 4799
		public short wFuncFlags;
	}
}
