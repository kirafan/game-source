﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x0200040A RID: 1034
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct STATSTG
	{
		// Token: 0x040012F6 RID: 4854
		public string pwcsName;

		// Token: 0x040012F7 RID: 4855
		public int type;

		// Token: 0x040012F8 RID: 4856
		public long cbSize;

		// Token: 0x040012F9 RID: 4857
		public FILETIME mtime;

		// Token: 0x040012FA RID: 4858
		public FILETIME ctime;

		// Token: 0x040012FB RID: 4859
		public FILETIME atime;

		// Token: 0x040012FC RID: 4860
		public int grfMode;

		// Token: 0x040012FD RID: 4861
		public int grfLocksSupported;

		// Token: 0x040012FE RID: 4862
		public Guid clsid;

		// Token: 0x040012FF RID: 4863
		public int grfStateBits;

		// Token: 0x04001300 RID: 4864
		public int reserved;
	}
}
