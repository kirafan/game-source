﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003EA RID: 1002
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct DISPPARAMS
	{
		// Token: 0x040012A1 RID: 4769
		public IntPtr rgvarg;

		// Token: 0x040012A2 RID: 4770
		public IntPtr rgdispidNamedArgs;

		// Token: 0x040012A3 RID: 4771
		public int cArgs;

		// Token: 0x040012A4 RID: 4772
		public int cNamedArgs;
	}
}
