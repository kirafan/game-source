﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000411 RID: 1041
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct VARDESC
	{
		// Token: 0x0400133B RID: 4923
		public int memid;

		// Token: 0x0400133C RID: 4924
		public string lpstrSchema;

		// Token: 0x0400133D RID: 4925
		public VARDESC.DESCUNION desc;

		// Token: 0x0400133E RID: 4926
		public ELEMDESC elemdescVar;

		// Token: 0x0400133F RID: 4927
		public short wVarFlags;

		// Token: 0x04001340 RID: 4928
		public VARKIND varkind;

		// Token: 0x02000412 RID: 1042
		[StructLayout(LayoutKind.Explicit, CharSet = CharSet.Unicode)]
		public struct DESCUNION
		{
			// Token: 0x04001341 RID: 4929
			[FieldOffset(0)]
			public IntPtr lpvarValue;

			// Token: 0x04001342 RID: 4930
			[FieldOffset(0)]
			public int oInst;
		}
	}
}
