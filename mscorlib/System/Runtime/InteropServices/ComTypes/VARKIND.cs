﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000414 RID: 1044
	[Serializable]
	public enum VARKIND
	{
		// Token: 0x04001352 RID: 4946
		VAR_PERINSTANCE,
		// Token: 0x04001353 RID: 4947
		VAR_STATIC,
		// Token: 0x04001354 RID: 4948
		VAR_CONST,
		// Token: 0x04001355 RID: 4949
		VAR_DISPATCH
	}
}
