﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003F6 RID: 1014
	[Flags]
	[Serializable]
	public enum IDLFLAG
	{
		// Token: 0x040012D7 RID: 4823
		IDLFLAG_NONE = 0,
		// Token: 0x040012D8 RID: 4824
		IDLFLAG_FIN = 1,
		// Token: 0x040012D9 RID: 4825
		IDLFLAG_FOUT = 2,
		// Token: 0x040012DA RID: 4826
		IDLFLAG_FLCID = 4,
		// Token: 0x040012DB RID: 4827
		IDLFLAG_FRETVAL = 8
	}
}
