﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003FC RID: 1020
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("0000000f-0000-0000-c000-000000000046")]
	[ComImport]
	public interface IMoniker
	{
		// Token: 0x06002C2D RID: 11309
		void GetClassID(out Guid pClassID);

		// Token: 0x06002C2E RID: 11310
		[PreserveSig]
		int IsDirty();

		// Token: 0x06002C2F RID: 11311
		void Load(IStream pStm);

		// Token: 0x06002C30 RID: 11312
		void Save(IStream pStm, [MarshalAs(UnmanagedType.Bool)] bool fClearDirty);

		// Token: 0x06002C31 RID: 11313
		void GetSizeMax(out long pcbSize);

		// Token: 0x06002C32 RID: 11314
		void BindToObject(IBindCtx pbc, IMoniker pmkToLeft, [In] ref Guid riidResult, [MarshalAs(UnmanagedType.Interface)] out object ppvResult);

		// Token: 0x06002C33 RID: 11315
		void BindToStorage(IBindCtx pbc, IMoniker pmkToLeft, [In] ref Guid riid, [MarshalAs(UnmanagedType.Interface)] out object ppvObj);

		// Token: 0x06002C34 RID: 11316
		void Reduce(IBindCtx pbc, int dwReduceHowFar, ref IMoniker ppmkToLeft, out IMoniker ppmkReduced);

		// Token: 0x06002C35 RID: 11317
		void ComposeWith(IMoniker pmkRight, [MarshalAs(UnmanagedType.Bool)] bool fOnlyIfNotGeneric, out IMoniker ppmkComposite);

		// Token: 0x06002C36 RID: 11318
		void Enum([MarshalAs(UnmanagedType.Bool)] bool fForward, out IEnumMoniker ppenumMoniker);

		// Token: 0x06002C37 RID: 11319
		[PreserveSig]
		int IsEqual(IMoniker pmkOtherMoniker);

		// Token: 0x06002C38 RID: 11320
		void Hash(out int pdwHash);

		// Token: 0x06002C39 RID: 11321
		[PreserveSig]
		int IsRunning(IBindCtx pbc, IMoniker pmkToLeft, IMoniker pmkNewlyRunning);

		// Token: 0x06002C3A RID: 11322
		void GetTimeOfLastChange(IBindCtx pbc, IMoniker pmkToLeft, out FILETIME pFileTime);

		// Token: 0x06002C3B RID: 11323
		void Inverse(out IMoniker ppmk);

		// Token: 0x06002C3C RID: 11324
		void CommonPrefixWith(IMoniker pmkOther, out IMoniker ppmkPrefix);

		// Token: 0x06002C3D RID: 11325
		void RelativePathTo(IMoniker pmkOther, out IMoniker ppmkRelPath);

		// Token: 0x06002C3E RID: 11326
		void GetDisplayName(IBindCtx pbc, IMoniker pmkToLeft, [MarshalAs(UnmanagedType.LPWStr)] out string ppszDisplayName);

		// Token: 0x06002C3F RID: 11327
		void ParseDisplayName(IBindCtx pbc, IMoniker pmkToLeft, [MarshalAs(UnmanagedType.LPWStr)] string pszDisplayName, out int pchEaten, out IMoniker ppmkOut);

		// Token: 0x06002C40 RID: 11328
		[PreserveSig]
		int IsSystemMoniker(out int pdwMksys);
	}
}
