﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000401 RID: 1025
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("0000000c-0000-0000-c000-000000000046")]
	[ComImport]
	public interface IStream
	{
		// Token: 0x06002C4E RID: 11342
		void Read([MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 1)] [Out] byte[] pv, int cb, IntPtr pcbRead);

		// Token: 0x06002C4F RID: 11343
		void Write([MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 1)] byte[] pv, int cb, IntPtr pcbWritten);

		// Token: 0x06002C50 RID: 11344
		void Seek(long dlibMove, int dwOrigin, IntPtr plibNewPosition);

		// Token: 0x06002C51 RID: 11345
		void SetSize(long libNewSize);

		// Token: 0x06002C52 RID: 11346
		void CopyTo(IStream pstm, long cb, IntPtr pcbRead, IntPtr pcbWritten);

		// Token: 0x06002C53 RID: 11347
		void Commit(int grfCommitFlags);

		// Token: 0x06002C54 RID: 11348
		void Revert();

		// Token: 0x06002C55 RID: 11349
		void LockRegion(long libOffset, long cb, int dwLockType);

		// Token: 0x06002C56 RID: 11350
		void UnlockRegion(long libOffset, long cb, int dwLockType);

		// Token: 0x06002C57 RID: 11351
		void Stat(out STATSTG pstatstg, int grfStatFlag);

		// Token: 0x06002C58 RID: 11352
		void Clone(out IStream ppstm);
	}
}
