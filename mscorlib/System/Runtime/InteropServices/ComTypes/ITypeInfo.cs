﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000403 RID: 1027
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00020401-0000-0000-c000-000000000046")]
	[ComImport]
	public interface ITypeInfo
	{
		// Token: 0x06002C5B RID: 11355
		void GetTypeAttr(out IntPtr ppTypeAttr);

		// Token: 0x06002C5C RID: 11356
		void GetTypeComp(out ITypeComp ppTComp);

		// Token: 0x06002C5D RID: 11357
		void GetFuncDesc(int index, out IntPtr ppFuncDesc);

		// Token: 0x06002C5E RID: 11358
		void GetVarDesc(int index, out IntPtr ppVarDesc);

		// Token: 0x06002C5F RID: 11359
		void GetNames(int memid, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 2)] [Out] string[] rgBstrNames, int cMaxNames, out int pcNames);

		// Token: 0x06002C60 RID: 11360
		void GetRefTypeOfImplType(int index, out int href);

		// Token: 0x06002C61 RID: 11361
		void GetImplTypeFlags(int index, out IMPLTYPEFLAGS pImplTypeFlags);

		// Token: 0x06002C62 RID: 11362
		void GetIDsOfNames([MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.LPWStr, SizeConst = 0, SizeParamIndex = 1)] [In] string[] rgszNames, int cNames, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 1)] [Out] int[] pMemId);

		// Token: 0x06002C63 RID: 11363
		void Invoke([MarshalAs(UnmanagedType.IUnknown)] object pvInstance, int memid, short wFlags, ref DISPPARAMS pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, out int puArgErr);

		// Token: 0x06002C64 RID: 11364
		void GetDocumentation(int index, out string strName, out string strDocString, out int dwHelpContext, out string strHelpFile);

		// Token: 0x06002C65 RID: 11365
		void GetDllEntry(int memid, INVOKEKIND invKind, IntPtr pBstrDllName, IntPtr pBstrName, IntPtr pwOrdinal);

		// Token: 0x06002C66 RID: 11366
		void GetRefTypeInfo(int hRef, out ITypeInfo ppTI);

		// Token: 0x06002C67 RID: 11367
		void AddressOfMember(int memid, INVOKEKIND invKind, out IntPtr ppv);

		// Token: 0x06002C68 RID: 11368
		void CreateInstance([MarshalAs(UnmanagedType.IUnknown)] object pUnkOuter, [In] ref Guid riid, [MarshalAs(UnmanagedType.IUnknown)] out object ppvObj);

		// Token: 0x06002C69 RID: 11369
		void GetMops(int memid, out string pBstrMops);

		// Token: 0x06002C6A RID: 11370
		void GetContainingTypeLib(out ITypeLib ppTLB, out int pIndex);

		// Token: 0x06002C6B RID: 11371
		[PreserveSig]
		void ReleaseTypeAttr(IntPtr pTypeAttr);

		// Token: 0x06002C6C RID: 11372
		[PreserveSig]
		void ReleaseFuncDesc(IntPtr pFuncDesc);

		// Token: 0x06002C6D RID: 11373
		[PreserveSig]
		void ReleaseVarDesc(IntPtr pVarDesc);
	}
}
