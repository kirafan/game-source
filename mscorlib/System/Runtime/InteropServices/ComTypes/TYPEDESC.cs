﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x0200040D RID: 1037
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct TYPEDESC
	{
		// Token: 0x04001319 RID: 4889
		public IntPtr lpValue;

		// Token: 0x0400131A RID: 4890
		public short vt;
	}
}
