﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000400 RID: 1024
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00000010-0000-0000-c000-000000000046")]
	[ComImport]
	public interface IRunningObjectTable
	{
		// Token: 0x06002C47 RID: 11335
		int Register(int grfFlags, [MarshalAs(UnmanagedType.Interface)] object punkObject, IMoniker pmkObjectName);

		// Token: 0x06002C48 RID: 11336
		void Revoke(int dwRegister);

		// Token: 0x06002C49 RID: 11337
		[PreserveSig]
		int IsRunning(IMoniker pmkObjectName);

		// Token: 0x06002C4A RID: 11338
		[PreserveSig]
		int GetObject(IMoniker pmkObjectName, [MarshalAs(UnmanagedType.Interface)] out object ppunkObject);

		// Token: 0x06002C4B RID: 11339
		void NoteChangeTime(int dwRegister, ref FILETIME pfiletime);

		// Token: 0x06002C4C RID: 11340
		[PreserveSig]
		int GetTimeOfLastChange(IMoniker pmkObjectName, out FILETIME pfiletime);

		// Token: 0x06002C4D RID: 11341
		void EnumRunning(out IEnumMoniker ppenumMoniker);
	}
}
