﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000409 RID: 1033
	[Flags]
	[Serializable]
	public enum PARAMFLAG
	{
		// Token: 0x040012EE RID: 4846
		PARAMFLAG_NONE = 0,
		// Token: 0x040012EF RID: 4847
		PARAMFLAG_FIN = 1,
		// Token: 0x040012F0 RID: 4848
		PARAMFLAG_FOUT = 2,
		// Token: 0x040012F1 RID: 4849
		PARAMFLAG_FLCID = 4,
		// Token: 0x040012F2 RID: 4850
		PARAMFLAG_FRETVAL = 8,
		// Token: 0x040012F3 RID: 4851
		PARAMFLAG_FOPT = 16,
		// Token: 0x040012F4 RID: 4852
		PARAMFLAG_FHASDEFAULT = 32,
		// Token: 0x040012F5 RID: 4853
		PARAMFLAG_FHASCUSTDATA = 64
	}
}
