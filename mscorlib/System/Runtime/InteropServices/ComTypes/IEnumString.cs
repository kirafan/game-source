﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003FA RID: 1018
	[Guid("00000101-0000-0000-c000-000000000046")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[ComImport]
	public interface IEnumString
	{
		// Token: 0x06002C25 RID: 11301
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 0)] [Out] string[] rgelt, IntPtr pceltFetched);

		// Token: 0x06002C26 RID: 11302
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002C27 RID: 11303
		void Reset();

		// Token: 0x06002C28 RID: 11304
		void Clone(out IEnumString ppenum);
	}
}
