﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003E5 RID: 997
	[StructLayout(LayoutKind.Explicit, CharSet = CharSet.Unicode)]
	public struct BINDPTR
	{
		// Token: 0x04001286 RID: 4742
		[FieldOffset(0)]
		public IntPtr lpfuncdesc;

		// Token: 0x04001287 RID: 4743
		[FieldOffset(0)]
		public IntPtr lptcomp;

		// Token: 0x04001288 RID: 4744
		[FieldOffset(0)]
		public IntPtr lpvardesc;
	}
}
