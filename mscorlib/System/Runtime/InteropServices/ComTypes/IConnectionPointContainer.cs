﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003F4 RID: 1012
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("b196b284-bab4-101a-b69c-00aa00341d07")]
	[ComImport]
	public interface IConnectionPointContainer
	{
		// Token: 0x06002C17 RID: 11287
		void EnumConnectionPoints(out IEnumConnectionPoints ppEnum);

		// Token: 0x06002C18 RID: 11288
		void FindConnectionPoint([In] ref Guid riid, out IConnectionPoint ppCP);
	}
}
