﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000408 RID: 1032
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct PARAMDESC
	{
		// Token: 0x040012EB RID: 4843
		public IntPtr lpVarValue;

		// Token: 0x040012EC RID: 4844
		public PARAMFLAG wParamFlags;
	}
}
