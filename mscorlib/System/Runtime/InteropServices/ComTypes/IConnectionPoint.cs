﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003F3 RID: 1011
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("b196b286-bab4-101a-b69c-00aa00341d07")]
	[ComImport]
	public interface IConnectionPoint
	{
		// Token: 0x06002C12 RID: 11282
		void GetConnectionInterface(out Guid pIID);

		// Token: 0x06002C13 RID: 11283
		void GetConnectionPointContainer(out IConnectionPointContainer ppCPC);

		// Token: 0x06002C14 RID: 11284
		void Advise([MarshalAs(UnmanagedType.Interface)] object pUnkSink, out int pdwCookie);

		// Token: 0x06002C15 RID: 11285
		void Unadvise(int dwCookie);

		// Token: 0x06002C16 RID: 11286
		void EnumConnections(out IEnumConnections ppEnum);
	}
}
