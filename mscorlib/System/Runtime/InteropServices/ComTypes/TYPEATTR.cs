﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x0200040C RID: 1036
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct TYPEATTR
	{
		// Token: 0x04001306 RID: 4870
		public const int MEMBER_ID_NIL = -1;

		// Token: 0x04001307 RID: 4871
		public Guid guid;

		// Token: 0x04001308 RID: 4872
		public int lcid;

		// Token: 0x04001309 RID: 4873
		public int dwReserved;

		// Token: 0x0400130A RID: 4874
		public int memidConstructor;

		// Token: 0x0400130B RID: 4875
		public int memidDestructor;

		// Token: 0x0400130C RID: 4876
		public IntPtr lpstrSchema;

		// Token: 0x0400130D RID: 4877
		public int cbSizeInstance;

		// Token: 0x0400130E RID: 4878
		public TYPEKIND typekind;

		// Token: 0x0400130F RID: 4879
		public short cFuncs;

		// Token: 0x04001310 RID: 4880
		public short cVars;

		// Token: 0x04001311 RID: 4881
		public short cImplTypes;

		// Token: 0x04001312 RID: 4882
		public short cbSizeVft;

		// Token: 0x04001313 RID: 4883
		public short cbAlignment;

		// Token: 0x04001314 RID: 4884
		public TYPEFLAGS wTypeFlags;

		// Token: 0x04001315 RID: 4885
		public short wMajorVerNum;

		// Token: 0x04001316 RID: 4886
		public short wMinorVerNum;

		// Token: 0x04001317 RID: 4887
		public TYPEDESC tdescAlias;

		// Token: 0x04001318 RID: 4888
		public IDLDESC idldescType;
	}
}
