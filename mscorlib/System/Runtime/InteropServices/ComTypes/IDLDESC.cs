﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003F5 RID: 1013
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct IDLDESC
	{
		// Token: 0x040012D4 RID: 4820
		public IntPtr dwReserved;

		// Token: 0x040012D5 RID: 4821
		public IDLFLAG wIDLFlags;
	}
}
