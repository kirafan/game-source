﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000406 RID: 1030
	[Guid("00020411-0000-0000-C000-000000000046")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[ComImport]
	public interface ITypeLib2 : ITypeLib
	{
		// Token: 0x06002C9A RID: 11418
		void FindName([MarshalAs(UnmanagedType.LPWStr)] string szNameBuf, int lHashVal, [MarshalAs(UnmanagedType.LPArray)] [Out] ITypeInfo[] ppTInfo, [MarshalAs(UnmanagedType.LPArray)] [Out] int[] rgMemId, ref short pcFound);

		// Token: 0x06002C9B RID: 11419
		void GetCustData(ref Guid guid, out object pVarVal);

		// Token: 0x06002C9C RID: 11420
		void GetDocumentation(int index, out string strName, out string strDocString, out int dwHelpContext, out string strHelpFile);

		// Token: 0x06002C9D RID: 11421
		void GetLibAttr(out IntPtr ppTLibAttr);

		// Token: 0x06002C9E RID: 11422
		void GetLibStatistics(IntPtr pcUniqueNames, out int pcchUniqueNames);

		// Token: 0x06002C9F RID: 11423
		[LCIDConversion(1)]
		void GetDocumentation2(int index, out string pbstrHelpString, out int pdwHelpStringContext, out string pbstrHelpStringDll);

		// Token: 0x06002CA0 RID: 11424
		void GetAllCustData(IntPtr pCustData);

		// Token: 0x06002CA1 RID: 11425
		void GetTypeComp(out ITypeComp ppTComp);

		// Token: 0x06002CA2 RID: 11426
		void GetTypeInfo(int index, out ITypeInfo ppTI);

		// Token: 0x06002CA3 RID: 11427
		void GetTypeInfoOfGuid(ref Guid guid, out ITypeInfo ppTInfo);

		// Token: 0x06002CA4 RID: 11428
		void GetTypeInfoType(int index, out TYPEKIND pTKind);

		// Token: 0x06002CA5 RID: 11429
		[return: MarshalAs(UnmanagedType.Bool)]
		bool IsName([MarshalAs(UnmanagedType.LPWStr)] string szNameBuf, int lHashVal);

		// Token: 0x06002CA6 RID: 11430
		[PreserveSig]
		void ReleaseTLibAttr(IntPtr pTLibAttr);

		// Token: 0x06002CA7 RID: 11431
		[PreserveSig]
		int GetTypeInfoCount();
	}
}
