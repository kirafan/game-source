﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003F9 RID: 1017
	[Guid("00000102-0000-0000-c000-000000000046")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[ComImport]
	public interface IEnumMoniker
	{
		// Token: 0x06002C21 RID: 11297
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 0)] [Out] IMoniker[] rgelt, IntPtr pceltFetched);

		// Token: 0x06002C22 RID: 11298
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002C23 RID: 11299
		void Reset();

		// Token: 0x06002C24 RID: 11300
		void Clone(out IEnumMoniker ppenum);
	}
}
