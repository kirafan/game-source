﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000407 RID: 1031
	[Flags]
	[Serializable]
	public enum LIBFLAGS
	{
		// Token: 0x040012E7 RID: 4839
		LIBFLAG_FRESTRICTED = 1,
		// Token: 0x040012E8 RID: 4840
		LIBFLAG_FCONTROL = 2,
		// Token: 0x040012E9 RID: 4841
		LIBFLAG_FHIDDEN = 4,
		// Token: 0x040012EA RID: 4842
		LIBFLAG_FHASDISKIMAGE = 8
	}
}
