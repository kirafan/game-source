﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000410 RID: 1040
	[Serializable]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct TYPELIBATTR
	{
		// Token: 0x04001335 RID: 4917
		public Guid guid;

		// Token: 0x04001336 RID: 4918
		public int lcid;

		// Token: 0x04001337 RID: 4919
		public SYSKIND syskind;

		// Token: 0x04001338 RID: 4920
		public short wMajorVerNum;

		// Token: 0x04001339 RID: 4921
		public short wMinorVerNum;

		// Token: 0x0400133A RID: 4922
		public LIBFLAGS wLibFlags;
	}
}
