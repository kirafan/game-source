﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003F2 RID: 1010
	[Guid("0000000e-0000-0000-c000-000000000046")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[ComImport]
	public interface IBindCtx
	{
		// Token: 0x06002C08 RID: 11272
		void RegisterObjectBound([MarshalAs(UnmanagedType.Interface)] object punk);

		// Token: 0x06002C09 RID: 11273
		void RevokeObjectBound([MarshalAs(UnmanagedType.Interface)] object punk);

		// Token: 0x06002C0A RID: 11274
		void ReleaseBoundObjects();

		// Token: 0x06002C0B RID: 11275
		void SetBindOptions([In] ref BIND_OPTS pbindopts);

		// Token: 0x06002C0C RID: 11276
		void GetBindOptions(ref BIND_OPTS pbindopts);

		// Token: 0x06002C0D RID: 11277
		void GetRunningObjectTable(out IRunningObjectTable pprot);

		// Token: 0x06002C0E RID: 11278
		void RegisterObjectParam([MarshalAs(UnmanagedType.LPWStr)] string pszKey, [MarshalAs(UnmanagedType.Interface)] object punk);

		// Token: 0x06002C0F RID: 11279
		void GetObjectParam([MarshalAs(UnmanagedType.LPWStr)] string pszKey, [MarshalAs(UnmanagedType.Interface)] out object ppunk);

		// Token: 0x06002C10 RID: 11280
		void EnumObjectParam(out IEnumString ppenum);

		// Token: 0x06002C11 RID: 11281
		[PreserveSig]
		int RevokeObjectParam([MarshalAs(UnmanagedType.LPWStr)] string pszKey);
	}
}
