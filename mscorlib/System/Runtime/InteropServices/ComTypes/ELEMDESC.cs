﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003EB RID: 1003
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct ELEMDESC
	{
		// Token: 0x040012A5 RID: 4773
		public TYPEDESC tdesc;

		// Token: 0x040012A6 RID: 4774
		public ELEMDESC.DESCUNION desc;

		// Token: 0x020003EC RID: 1004
		[StructLayout(LayoutKind.Explicit, CharSet = CharSet.Unicode)]
		public struct DESCUNION
		{
			// Token: 0x040012A7 RID: 4775
			[FieldOffset(0)]
			public IDLDESC idldesc;

			// Token: 0x040012A8 RID: 4776
			[FieldOffset(0)]
			public PARAMDESC paramdesc;
		}
	}
}
