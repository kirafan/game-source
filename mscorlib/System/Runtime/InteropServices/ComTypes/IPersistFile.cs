﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003FF RID: 1023
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("0000010b-0000-0000-c000-000000000046")]
	[ComImport]
	public interface IPersistFile
	{
		// Token: 0x06002C41 RID: 11329
		void GetClassID(out Guid pClassID);

		// Token: 0x06002C42 RID: 11330
		[PreserveSig]
		int IsDirty();

		// Token: 0x06002C43 RID: 11331
		void Load([MarshalAs(UnmanagedType.LPWStr)] string pszFileName, int dwMode);

		// Token: 0x06002C44 RID: 11332
		void Save([MarshalAs(UnmanagedType.LPWStr)] string pszFileName, [MarshalAs(UnmanagedType.Bool)] bool fRemember);

		// Token: 0x06002C45 RID: 11333
		void SaveCompleted([MarshalAs(UnmanagedType.LPWStr)] string pszFileName);

		// Token: 0x06002C46 RID: 11334
		void GetCurFile([MarshalAs(UnmanagedType.LPWStr)] out string ppszFileName);
	}
}
