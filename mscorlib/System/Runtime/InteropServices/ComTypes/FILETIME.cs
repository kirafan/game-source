﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003EE RID: 1006
	public struct FILETIME
	{
		// Token: 0x040012B2 RID: 4786
		public int dwLowDateTime;

		// Token: 0x040012B3 RID: 4787
		public int dwHighDateTime;
	}
}
