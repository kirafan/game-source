﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003E8 RID: 1000
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct CONNECTDATA
	{
		// Token: 0x04001298 RID: 4760
		[MarshalAs(UnmanagedType.Interface)]
		public object pUnk;

		// Token: 0x04001299 RID: 4761
		public int dwCookie;
	}
}
