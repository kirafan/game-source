﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x0200040B RID: 1035
	[Serializable]
	public enum SYSKIND
	{
		// Token: 0x04001302 RID: 4866
		SYS_WIN16,
		// Token: 0x04001303 RID: 4867
		SYS_WIN32,
		// Token: 0x04001304 RID: 4868
		SYS_MAC,
		// Token: 0x04001305 RID: 4869
		SYS_WIN64
	}
}
