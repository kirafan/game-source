﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000402 RID: 1026
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00020403-0000-0000-c000-000000000046")]
	[ComImport]
	public interface ITypeComp
	{
		// Token: 0x06002C59 RID: 11353
		void Bind([MarshalAs(UnmanagedType.LPWStr)] string szName, int lHashVal, short wFlags, out ITypeInfo ppTInfo, out DESCKIND pDescKind, out BINDPTR pBindPtr);

		// Token: 0x06002C5A RID: 11354
		void BindType([MarshalAs(UnmanagedType.LPWStr)] string szName, int lHashVal, out ITypeInfo ppTInfo, out ITypeComp ppTComp);
	}
}
