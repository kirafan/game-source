﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003FD RID: 1021
	[Flags]
	[Serializable]
	public enum IMPLTYPEFLAGS
	{
		// Token: 0x040012DD RID: 4829
		IMPLTYPEFLAG_FDEFAULT = 1,
		// Token: 0x040012DE RID: 4830
		IMPLTYPEFLAG_FSOURCE = 2,
		// Token: 0x040012DF RID: 4831
		IMPLTYPEFLAG_FRESTRICTED = 4,
		// Token: 0x040012E0 RID: 4832
		IMPLTYPEFLAG_FDEFAULTVTABLE = 8
	}
}
