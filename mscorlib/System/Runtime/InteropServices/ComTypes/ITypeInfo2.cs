﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000404 RID: 1028
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00020412-0000-0000-C000-000000000046")]
	[ComImport]
	public interface ITypeInfo2 : ITypeInfo
	{
		// Token: 0x06002C6E RID: 11374
		void AddressOfMember(int memid, INVOKEKIND invKind, out IntPtr ppv);

		// Token: 0x06002C6F RID: 11375
		void CreateInstance([MarshalAs(UnmanagedType.IUnknown)] object pUnkOuter, [In] ref Guid riid, [MarshalAs(UnmanagedType.IUnknown)] out object ppvObj);

		// Token: 0x06002C70 RID: 11376
		void GetContainingTypeLib(out ITypeLib ppTLB, out int pIndex);

		// Token: 0x06002C71 RID: 11377
		void GetDllEntry(int memid, INVOKEKIND invKind, IntPtr pBstrDllName, IntPtr pBstrName, IntPtr pwOrdinal);

		// Token: 0x06002C72 RID: 11378
		void GetDocumentation(int index, out string strName, out string strDocString, out int dwHelpContext, out string strHelpFile);

		// Token: 0x06002C73 RID: 11379
		void GetIDsOfNames([MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.LPWStr, SizeConst = 0, SizeParamIndex = 1)] [In] string[] rgszNames, int cNames, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 1)] [Out] int[] pMemId);

		// Token: 0x06002C74 RID: 11380
		void GetImplTypeFlags(int index, out IMPLTYPEFLAGS pImplTypeFlags);

		// Token: 0x06002C75 RID: 11381
		void GetTypeKind(out TYPEKIND pTypeKind);

		// Token: 0x06002C76 RID: 11382
		void GetTypeFlags(out int pTypeFlags);

		// Token: 0x06002C77 RID: 11383
		void GetFuncDesc(int index, out IntPtr ppFuncDesc);

		// Token: 0x06002C78 RID: 11384
		void GetMops(int memid, out string pBstrMops);

		// Token: 0x06002C79 RID: 11385
		void GetNames(int memid, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 2)] [Out] string[] rgBstrNames, int cMaxNames, out int pcNames);

		// Token: 0x06002C7A RID: 11386
		void GetRefTypeInfo(int hRef, out ITypeInfo ppTI);

		// Token: 0x06002C7B RID: 11387
		void GetRefTypeOfImplType(int index, out int href);

		// Token: 0x06002C7C RID: 11388
		void GetTypeAttr(out IntPtr ppTypeAttr);

		// Token: 0x06002C7D RID: 11389
		void GetTypeComp(out ITypeComp ppTComp);

		// Token: 0x06002C7E RID: 11390
		void GetVarDesc(int index, out IntPtr ppVarDesc);

		// Token: 0x06002C7F RID: 11391
		void GetFuncIndexOfMemId(int memid, INVOKEKIND invKind, out int pFuncIndex);

		// Token: 0x06002C80 RID: 11392
		void GetVarIndexOfMemId(int memid, out int pVarIndex);

		// Token: 0x06002C81 RID: 11393
		void GetCustData(ref Guid guid, out object pVarVal);

		// Token: 0x06002C82 RID: 11394
		void GetFuncCustData(int index, ref Guid guid, out object pVarVal);

		// Token: 0x06002C83 RID: 11395
		void GetParamCustData(int indexFunc, int indexParam, ref Guid guid, out object pVarVal);

		// Token: 0x06002C84 RID: 11396
		void GetVarCustData(int index, ref Guid guid, out object pVarVal);

		// Token: 0x06002C85 RID: 11397
		void GetImplTypeCustData(int index, ref Guid guid, out object pVarVal);

		// Token: 0x06002C86 RID: 11398
		[LCIDConversion(1)]
		void GetDocumentation2(int memid, out string pbstrHelpString, out int pdwHelpStringContext, out string pbstrHelpStringDll);

		// Token: 0x06002C87 RID: 11399
		void GetAllCustData(IntPtr pCustData);

		// Token: 0x06002C88 RID: 11400
		void GetAllFuncCustData(int index, IntPtr pCustData);

		// Token: 0x06002C89 RID: 11401
		void GetAllParamCustData(int indexFunc, int indexParam, IntPtr pCustData);

		// Token: 0x06002C8A RID: 11402
		void GetAllVarCustData(int index, IntPtr pCustData);

		// Token: 0x06002C8B RID: 11403
		void GetAllImplTypeCustData(int index, IntPtr pCustData);

		// Token: 0x06002C8C RID: 11404
		void Invoke([MarshalAs(UnmanagedType.IUnknown)] object pvInstance, int memid, short wFlags, ref DISPPARAMS pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, out int puArgErr);

		// Token: 0x06002C8D RID: 11405
		[PreserveSig]
		void ReleaseTypeAttr(IntPtr pTypeAttr);

		// Token: 0x06002C8E RID: 11406
		[PreserveSig]
		void ReleaseFuncDesc(IntPtr pFuncDesc);

		// Token: 0x06002C8F RID: 11407
		[PreserveSig]
		void ReleaseVarDesc(IntPtr pVarDesc);
	}
}
