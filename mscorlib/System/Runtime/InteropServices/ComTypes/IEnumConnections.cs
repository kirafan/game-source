﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003F8 RID: 1016
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("b196b287-bab4-101a-b69c-00aa00341d07")]
	[ComImport]
	public interface IEnumConnections
	{
		// Token: 0x06002C1D RID: 11293
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 0)] [Out] CONNECTDATA[] rgelt, IntPtr pceltFetched);

		// Token: 0x06002C1E RID: 11294
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002C1F RID: 11295
		void Reset();

		// Token: 0x06002C20 RID: 11296
		void Clone(out IEnumConnections ppenum);
	}
}
