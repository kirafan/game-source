﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003E6 RID: 998
	public struct BIND_OPTS
	{
		// Token: 0x04001289 RID: 4745
		public int cbStruct;

		// Token: 0x0400128A RID: 4746
		public int grfFlags;

		// Token: 0x0400128B RID: 4747
		public int grfMode;

		// Token: 0x0400128C RID: 4748
		public int dwTickCountDeadline;
	}
}
