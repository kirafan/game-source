﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003F1 RID: 1009
	[Serializable]
	public enum FUNCKIND
	{
		// Token: 0x040012CF RID: 4815
		FUNC_VIRTUAL,
		// Token: 0x040012D0 RID: 4816
		FUNC_PUREVIRTUAL,
		// Token: 0x040012D1 RID: 4817
		FUNC_NONVIRTUAL,
		// Token: 0x040012D2 RID: 4818
		FUNC_STATIC,
		// Token: 0x040012D3 RID: 4819
		FUNC_DISPATCH
	}
}
