﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003E9 RID: 1001
	[Serializable]
	public enum DESCKIND
	{
		// Token: 0x0400129B RID: 4763
		DESCKIND_NONE,
		// Token: 0x0400129C RID: 4764
		DESCKIND_FUNCDESC,
		// Token: 0x0400129D RID: 4765
		DESCKIND_VARDESC,
		// Token: 0x0400129E RID: 4766
		DESCKIND_TYPECOMP,
		// Token: 0x0400129F RID: 4767
		DESCKIND_IMPLICITAPPOBJ,
		// Token: 0x040012A0 RID: 4768
		DESCKIND_MAX
	}
}
