﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003FB RID: 1019
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00020404-0000-0000-c000-000000000046")]
	[ComImport]
	public interface IEnumVARIANT
	{
		// Token: 0x06002C29 RID: 11305
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 0)] [Out] object[] rgVar, IntPtr pceltFetched);

		// Token: 0x06002C2A RID: 11306
		[PreserveSig]
		int Skip(int celt);

		// Token: 0x06002C2B RID: 11307
		[PreserveSig]
		int Reset();

		// Token: 0x06002C2C RID: 11308
		IEnumVARIANT Clone();
	}
}
