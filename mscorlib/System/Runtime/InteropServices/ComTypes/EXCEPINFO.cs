﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003ED RID: 1005
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct EXCEPINFO
	{
		// Token: 0x040012A9 RID: 4777
		public short wCode;

		// Token: 0x040012AA RID: 4778
		public short wReserved;

		// Token: 0x040012AB RID: 4779
		[MarshalAs(UnmanagedType.BStr)]
		public string bstrSource;

		// Token: 0x040012AC RID: 4780
		[MarshalAs(UnmanagedType.BStr)]
		public string bstrDescription;

		// Token: 0x040012AD RID: 4781
		[MarshalAs(UnmanagedType.BStr)]
		public string bstrHelpFile;

		// Token: 0x040012AE RID: 4782
		public int dwHelpContext;

		// Token: 0x040012AF RID: 4783
		public IntPtr pvReserved;

		// Token: 0x040012B0 RID: 4784
		public IntPtr pfnDeferredFillIn;

		// Token: 0x040012B1 RID: 4785
		public int scode;
	}
}
