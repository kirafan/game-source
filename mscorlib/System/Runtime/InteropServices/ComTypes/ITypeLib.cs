﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x02000405 RID: 1029
	[Guid("00020402-0000-0000-c000-000000000046")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[ComImport]
	public interface ITypeLib
	{
		// Token: 0x06002C90 RID: 11408
		[PreserveSig]
		int GetTypeInfoCount();

		// Token: 0x06002C91 RID: 11409
		void GetTypeInfo(int index, out ITypeInfo ppTI);

		// Token: 0x06002C92 RID: 11410
		void GetTypeInfoType(int index, out TYPEKIND pTKind);

		// Token: 0x06002C93 RID: 11411
		void GetTypeInfoOfGuid(ref Guid guid, out ITypeInfo ppTInfo);

		// Token: 0x06002C94 RID: 11412
		void GetLibAttr(out IntPtr ppTLibAttr);

		// Token: 0x06002C95 RID: 11413
		void GetTypeComp(out ITypeComp ppTComp);

		// Token: 0x06002C96 RID: 11414
		void GetDocumentation(int index, out string strName, out string strDocString, out int dwHelpContext, out string strHelpFile);

		// Token: 0x06002C97 RID: 11415
		[return: MarshalAs(UnmanagedType.Bool)]
		bool IsName([MarshalAs(UnmanagedType.LPWStr)] string szNameBuf, int lHashVal);

		// Token: 0x06002C98 RID: 11416
		void FindName([MarshalAs(UnmanagedType.LPWStr)] string szNameBuf, int lHashVal, [MarshalAs(UnmanagedType.LPArray)] [Out] ITypeInfo[] ppTInfo, [MarshalAs(UnmanagedType.LPArray)] [Out] int[] rgMemId, ref short pcFound);

		// Token: 0x06002C99 RID: 11417
		[PreserveSig]
		void ReleaseTLibAttr(IntPtr pTLibAttr);
	}
}
