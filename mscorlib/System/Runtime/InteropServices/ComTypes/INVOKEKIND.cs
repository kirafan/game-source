﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020003FE RID: 1022
	[Flags]
	[Serializable]
	public enum INVOKEKIND
	{
		// Token: 0x040012E2 RID: 4834
		INVOKE_FUNC = 1,
		// Token: 0x040012E3 RID: 4835
		INVOKE_PROPERTYGET = 2,
		// Token: 0x040012E4 RID: 4836
		INVOKE_PROPERTYPUT = 4,
		// Token: 0x040012E5 RID: 4837
		INVOKE_PROPERTYPUTREF = 8
	}
}
