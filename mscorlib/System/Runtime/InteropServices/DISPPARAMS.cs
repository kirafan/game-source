﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000386 RID: 902
	[Obsolete]
	[StructLayout(LayoutKind.Sequential, CharSet = CharSet.Unicode)]
	public struct DISPPARAMS
	{
		// Token: 0x040010F6 RID: 4342
		public IntPtr rgvarg;

		// Token: 0x040010F7 RID: 4343
		public IntPtr rgdispidNamedArgs;

		// Token: 0x040010F8 RID: 4344
		public int cArgs;

		// Token: 0x040010F9 RID: 4345
		public int cNamedArgs;
	}
}
