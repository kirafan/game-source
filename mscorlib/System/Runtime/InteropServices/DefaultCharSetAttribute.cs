﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x0200004E RID: 78
	[AttributeUsage(AttributeTargets.Module, Inherited = false)]
	[ComVisible(true)]
	public sealed class DefaultCharSetAttribute : Attribute
	{
		// Token: 0x06000674 RID: 1652 RVA: 0x00014BB4 File Offset: 0x00012DB4
		public DefaultCharSetAttribute(CharSet charSet)
		{
			this._set = charSet;
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000675 RID: 1653 RVA: 0x00014BC4 File Offset: 0x00012DC4
		public CharSet CharSet
		{
			get
			{
				return this._set;
			}
		}

		// Token: 0x040000A1 RID: 161
		private CharSet _set;
	}
}
