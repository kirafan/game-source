﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003E2 RID: 994
	[ComVisible(true)]
	[Serializable]
	public enum VarEnum
	{
		// Token: 0x0400124B RID: 4683
		VT_EMPTY,
		// Token: 0x0400124C RID: 4684
		VT_NULL,
		// Token: 0x0400124D RID: 4685
		VT_I2,
		// Token: 0x0400124E RID: 4686
		VT_I4,
		// Token: 0x0400124F RID: 4687
		VT_R4,
		// Token: 0x04001250 RID: 4688
		VT_R8,
		// Token: 0x04001251 RID: 4689
		VT_CY,
		// Token: 0x04001252 RID: 4690
		VT_DATE,
		// Token: 0x04001253 RID: 4691
		VT_BSTR,
		// Token: 0x04001254 RID: 4692
		VT_DISPATCH,
		// Token: 0x04001255 RID: 4693
		VT_ERROR,
		// Token: 0x04001256 RID: 4694
		VT_BOOL,
		// Token: 0x04001257 RID: 4695
		VT_VARIANT,
		// Token: 0x04001258 RID: 4696
		VT_UNKNOWN,
		// Token: 0x04001259 RID: 4697
		VT_DECIMAL,
		// Token: 0x0400125A RID: 4698
		VT_I1 = 16,
		// Token: 0x0400125B RID: 4699
		VT_UI1,
		// Token: 0x0400125C RID: 4700
		VT_UI2,
		// Token: 0x0400125D RID: 4701
		VT_UI4,
		// Token: 0x0400125E RID: 4702
		VT_I8,
		// Token: 0x0400125F RID: 4703
		VT_UI8,
		// Token: 0x04001260 RID: 4704
		VT_INT,
		// Token: 0x04001261 RID: 4705
		VT_UINT,
		// Token: 0x04001262 RID: 4706
		VT_VOID,
		// Token: 0x04001263 RID: 4707
		VT_HRESULT,
		// Token: 0x04001264 RID: 4708
		VT_PTR,
		// Token: 0x04001265 RID: 4709
		VT_SAFEARRAY,
		// Token: 0x04001266 RID: 4710
		VT_CARRAY,
		// Token: 0x04001267 RID: 4711
		VT_USERDEFINED,
		// Token: 0x04001268 RID: 4712
		VT_LPSTR,
		// Token: 0x04001269 RID: 4713
		VT_LPWSTR,
		// Token: 0x0400126A RID: 4714
		VT_RECORD = 36,
		// Token: 0x0400126B RID: 4715
		VT_FILETIME = 64,
		// Token: 0x0400126C RID: 4716
		VT_BLOB,
		// Token: 0x0400126D RID: 4717
		VT_STREAM,
		// Token: 0x0400126E RID: 4718
		VT_STORAGE,
		// Token: 0x0400126F RID: 4719
		VT_STREAMED_OBJECT,
		// Token: 0x04001270 RID: 4720
		VT_STORED_OBJECT,
		// Token: 0x04001271 RID: 4721
		VT_BLOB_OBJECT,
		// Token: 0x04001272 RID: 4722
		VT_CF,
		// Token: 0x04001273 RID: 4723
		VT_CLSID,
		// Token: 0x04001274 RID: 4724
		VT_VECTOR = 4096,
		// Token: 0x04001275 RID: 4725
		VT_ARRAY = 8192,
		// Token: 0x04001276 RID: 4726
		VT_BYREF = 16384
	}
}
