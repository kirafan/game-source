﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000394 RID: 916
	[ComVisible(true)]
	[Serializable]
	public enum GCHandleType
	{
		// Token: 0x04001131 RID: 4401
		Weak,
		// Token: 0x04001132 RID: 4402
		WeakTrackResurrection,
		// Token: 0x04001133 RID: 4403
		Normal,
		// Token: 0x04001134 RID: 4404
		Pinned
	}
}
