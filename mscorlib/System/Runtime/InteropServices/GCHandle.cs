﻿using System;
using System.Runtime.CompilerServices;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000393 RID: 915
	[ComVisible(true)]
	[MonoTODO("Struct should be [StructLayout(LayoutKind.Sequential)] but will need to be reordered for that.")]
	public struct GCHandle
	{
		// Token: 0x06002A5A RID: 10842 RVA: 0x0009280C File Offset: 0x00090A0C
		private GCHandle(IntPtr h)
		{
			this.handle = (int)h;
		}

		// Token: 0x06002A5B RID: 10843 RVA: 0x0009281C File Offset: 0x00090A1C
		private GCHandle(object obj)
		{
			this = new GCHandle(obj, GCHandleType.Normal);
		}

		// Token: 0x06002A5C RID: 10844 RVA: 0x00092828 File Offset: 0x00090A28
		private GCHandle(object value, GCHandleType type)
		{
			if (type < GCHandleType.Weak || type > GCHandleType.Pinned)
			{
				type = GCHandleType.Normal;
			}
			this.handle = GCHandle.GetTargetHandle(value, 0, type);
		}

		// Token: 0x170007D1 RID: 2001
		// (get) Token: 0x06002A5D RID: 10845 RVA: 0x0009284C File Offset: 0x00090A4C
		public bool IsAllocated
		{
			get
			{
				return this.handle != 0;
			}
		}

		// Token: 0x170007D2 RID: 2002
		// (get) Token: 0x06002A5E RID: 10846 RVA: 0x0009285C File Offset: 0x00090A5C
		// (set) Token: 0x06002A5F RID: 10847 RVA: 0x00092890 File Offset: 0x00090A90
		public object Target
		{
			get
			{
				if (!this.IsAllocated)
				{
					throw new InvalidOperationException(Locale.GetText("Handle is not allocated"));
				}
				return GCHandle.GetTarget(this.handle);
			}
			set
			{
				this.handle = GCHandle.GetTargetHandle(value, this.handle, (GCHandleType)(-1));
			}
		}

		// Token: 0x06002A60 RID: 10848 RVA: 0x000928A8 File Offset: 0x00090AA8
		public IntPtr AddrOfPinnedObject()
		{
			IntPtr addrOfPinnedObject = GCHandle.GetAddrOfPinnedObject(this.handle);
			if (addrOfPinnedObject == (IntPtr)(-1))
			{
				throw new ArgumentException("Object contains non-primitive or non-blittable data.");
			}
			if (addrOfPinnedObject == (IntPtr)(-2))
			{
				throw new InvalidOperationException("Handle is not pinned.");
			}
			return addrOfPinnedObject;
		}

		// Token: 0x06002A61 RID: 10849 RVA: 0x000928FC File Offset: 0x00090AFC
		public static GCHandle Alloc(object value)
		{
			return new GCHandle(value);
		}

		// Token: 0x06002A62 RID: 10850 RVA: 0x00092904 File Offset: 0x00090B04
		public static GCHandle Alloc(object value, GCHandleType type)
		{
			return new GCHandle(value, type);
		}

		// Token: 0x06002A63 RID: 10851 RVA: 0x00092910 File Offset: 0x00090B10
		public void Free()
		{
			GCHandle.FreeHandle(this.handle);
			this.handle = 0;
		}

		// Token: 0x06002A64 RID: 10852
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool CheckCurrentDomain(int handle);

		// Token: 0x06002A65 RID: 10853
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern object GetTarget(int handle);

		// Token: 0x06002A66 RID: 10854
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int GetTargetHandle(object obj, int handle, GCHandleType type);

		// Token: 0x06002A67 RID: 10855
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void FreeHandle(int handle);

		// Token: 0x06002A68 RID: 10856
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern IntPtr GetAddrOfPinnedObject(int handle);

		// Token: 0x06002A69 RID: 10857 RVA: 0x00092924 File Offset: 0x00090B24
		public override bool Equals(object o)
		{
			return o != null && o is GCHandle && this.handle == ((GCHandle)o).handle;
		}

		// Token: 0x06002A6A RID: 10858 RVA: 0x0009295C File Offset: 0x00090B5C
		public override int GetHashCode()
		{
			return this.handle.GetHashCode();
		}

		// Token: 0x06002A6B RID: 10859 RVA: 0x0009296C File Offset: 0x00090B6C
		public static GCHandle FromIntPtr(IntPtr value)
		{
			return (GCHandle)value;
		}

		// Token: 0x06002A6C RID: 10860 RVA: 0x00092974 File Offset: 0x00090B74
		public static IntPtr ToIntPtr(GCHandle value)
		{
			return (IntPtr)value;
		}

		// Token: 0x06002A6D RID: 10861 RVA: 0x0009297C File Offset: 0x00090B7C
		public static explicit operator IntPtr(GCHandle value)
		{
			return (IntPtr)value.handle;
		}

		// Token: 0x06002A6E RID: 10862 RVA: 0x0009298C File Offset: 0x00090B8C
		public static explicit operator GCHandle(IntPtr value)
		{
			if (value == IntPtr.Zero)
			{
				throw new ArgumentException("GCHandle value cannot be zero");
			}
			if (!GCHandle.CheckCurrentDomain((int)value))
			{
				throw new ArgumentException("GCHandle value belongs to a different domain");
			}
			return new GCHandle(value);
		}

		// Token: 0x06002A6F RID: 10863 RVA: 0x000929D8 File Offset: 0x00090BD8
		public static bool operator ==(GCHandle a, GCHandle b)
		{
			return a.Equals(b);
		}

		// Token: 0x06002A70 RID: 10864 RVA: 0x000929E8 File Offset: 0x00090BE8
		public static bool operator !=(GCHandle a, GCHandle b)
		{
			return !a.Equals(b);
		}

		// Token: 0x0400112F RID: 4399
		private int handle;
	}
}
