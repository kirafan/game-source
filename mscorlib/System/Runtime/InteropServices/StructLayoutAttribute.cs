﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000058 RID: 88
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct, Inherited = false)]
	public sealed class StructLayoutAttribute : Attribute
	{
		// Token: 0x0600068A RID: 1674 RVA: 0x00014D34 File Offset: 0x00012F34
		public StructLayoutAttribute(short layoutKind)
		{
			this.lkind = (LayoutKind)layoutKind;
		}

		// Token: 0x0600068B RID: 1675 RVA: 0x00014D54 File Offset: 0x00012F54
		public StructLayoutAttribute(LayoutKind layoutKind)
		{
			this.lkind = layoutKind;
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x0600068C RID: 1676 RVA: 0x00014D74 File Offset: 0x00012F74
		public LayoutKind Value
		{
			get
			{
				return this.lkind;
			}
		}

		// Token: 0x040000AE RID: 174
		public CharSet CharSet = CharSet.Auto;

		// Token: 0x040000AF RID: 175
		public int Pack = 8;

		// Token: 0x040000B0 RID: 176
		public int Size;

		// Token: 0x040000B1 RID: 177
		private LayoutKind lkind;
	}
}
