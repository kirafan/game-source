﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000372 RID: 882
	[ComVisible(true)]
	[Serializable]
	public enum CallingConvention
	{
		// Token: 0x040010C9 RID: 4297
		Winapi = 1,
		// Token: 0x040010CA RID: 4298
		Cdecl,
		// Token: 0x040010CB RID: 4299
		StdCall,
		// Token: 0x040010CC RID: 4300
		ThisCall,
		// Token: 0x040010CD RID: 4301
		FastCall
	}
}
