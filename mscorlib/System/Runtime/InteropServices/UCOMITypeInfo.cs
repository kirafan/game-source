﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020003DB RID: 987
	[Obsolete]
	[Guid("00020401-0000-0000-c000-000000000046")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[ComImport]
	public interface UCOMITypeInfo
	{
		// Token: 0x06002BE5 RID: 11237
		void GetTypeAttr(out IntPtr ppTypeAttr);

		// Token: 0x06002BE6 RID: 11238
		void GetTypeComp(out UCOMITypeComp ppTComp);

		// Token: 0x06002BE7 RID: 11239
		void GetFuncDesc(int index, out IntPtr ppFuncDesc);

		// Token: 0x06002BE8 RID: 11240
		void GetVarDesc(int index, out IntPtr ppVarDesc);

		// Token: 0x06002BE9 RID: 11241
		void GetNames(int memid, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 2)] [Out] string[] rgBstrNames, int cMaxNames, out int pcNames);

		// Token: 0x06002BEA RID: 11242
		void GetRefTypeOfImplType(int index, out int href);

		// Token: 0x06002BEB RID: 11243
		void GetImplTypeFlags(int index, out int pImplTypeFlags);

		// Token: 0x06002BEC RID: 11244
		void GetIDsOfNames([MarshalAs(UnmanagedType.LPArray, ArraySubType = UnmanagedType.LPWStr, SizeConst = 0, SizeParamIndex = 1)] [In] string[] rgszNames, int cNames, [MarshalAs(UnmanagedType.LPArray, SizeConst = 0, SizeParamIndex = 1)] [Out] int[] pMemId);

		// Token: 0x06002BED RID: 11245
		void Invoke([MarshalAs(UnmanagedType.IUnknown)] object pvInstance, int memid, short wFlags, ref DISPPARAMS pDispParams, out object pVarResult, out EXCEPINFO pExcepInfo, out int puArgErr);

		// Token: 0x06002BEE RID: 11246
		void GetDocumentation(int index, out string strName, out string strDocString, out int dwHelpContext, out string strHelpFile);

		// Token: 0x06002BEF RID: 11247
		void GetDllEntry(int memid, INVOKEKIND invKind, out string pBstrDllName, out string pBstrName, out short pwOrdinal);

		// Token: 0x06002BF0 RID: 11248
		void GetRefTypeInfo(int hRef, out UCOMITypeInfo ppTI);

		// Token: 0x06002BF1 RID: 11249
		void AddressOfMember(int memid, INVOKEKIND invKind, out IntPtr ppv);

		// Token: 0x06002BF2 RID: 11250
		void CreateInstance([MarshalAs(UnmanagedType.IUnknown)] object pUnkOuter, ref Guid riid, [MarshalAs(UnmanagedType.IUnknown)] out object ppvObj);

		// Token: 0x06002BF3 RID: 11251
		void GetMops(int memid, out string pBstrMops);

		// Token: 0x06002BF4 RID: 11252
		void GetContainingTypeLib(out UCOMITypeLib ppTLB, out int pIndex);

		// Token: 0x06002BF5 RID: 11253
		void ReleaseTypeAttr(IntPtr pTypeAttr);

		// Token: 0x06002BF6 RID: 11254
		void ReleaseFuncDesc(IntPtr pFuncDesc);

		// Token: 0x06002BF7 RID: 11255
		void ReleaseVarDesc(IntPtr pVarDesc);
	}
}
