﻿using System;
using System.Runtime.ConstrainedExecution;

namespace System.Runtime
{
	// Token: 0x0200031F RID: 799
	public sealed class MemoryFailPoint : CriticalFinalizerObject, IDisposable
	{
		// Token: 0x06002890 RID: 10384 RVA: 0x00091DB0 File Offset: 0x0008FFB0
		[MonoTODO]
		public MemoryFailPoint(int sizeInMegabytes)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002891 RID: 10385 RVA: 0x00091DC0 File Offset: 0x0008FFC0
		~MemoryFailPoint()
		{
		}

		// Token: 0x06002892 RID: 10386 RVA: 0x00091DF8 File Offset: 0x0008FFF8
		[MonoTODO]
		public void Dispose()
		{
			throw new NotImplementedException();
		}
	}
}
