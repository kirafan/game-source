﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004EE RID: 1262
	[ComVisible(true)]
	[CLSCompliant(false)]
	[Serializable]
	public abstract class Formatter : IFormatter
	{
		// Token: 0x17000991 RID: 2449
		// (get) Token: 0x060032A2 RID: 12962
		// (set) Token: 0x060032A3 RID: 12963
		public abstract SerializationBinder Binder { get; set; }

		// Token: 0x17000992 RID: 2450
		// (get) Token: 0x060032A4 RID: 12964
		// (set) Token: 0x060032A5 RID: 12965
		public abstract StreamingContext Context { get; set; }

		// Token: 0x17000993 RID: 2451
		// (get) Token: 0x060032A6 RID: 12966
		// (set) Token: 0x060032A7 RID: 12967
		public abstract ISurrogateSelector SurrogateSelector { get; set; }

		// Token: 0x060032A8 RID: 12968
		public abstract object Deserialize(Stream serializationStream);

		// Token: 0x060032A9 RID: 12969 RVA: 0x000A45FC File Offset: 0x000A27FC
		protected virtual object GetNext(out long objID)
		{
			if (this.m_objectQueue.Count == 0)
			{
				objID = 0L;
				return null;
			}
			object obj = this.m_objectQueue.Dequeue();
			bool flag;
			objID = this.m_idGenerator.HasId(obj, out flag);
			return obj;
		}

		// Token: 0x060032AA RID: 12970 RVA: 0x000A463C File Offset: 0x000A283C
		protected virtual long Schedule(object obj)
		{
			if (obj == null)
			{
				return 0L;
			}
			bool flag;
			long id = this.m_idGenerator.GetId(obj, out flag);
			if (flag)
			{
				this.m_objectQueue.Enqueue(obj);
			}
			return id;
		}

		// Token: 0x060032AB RID: 12971
		public abstract void Serialize(Stream serializationStream, object graph);

		// Token: 0x060032AC RID: 12972
		protected abstract void WriteArray(object obj, string name, Type memberType);

		// Token: 0x060032AD RID: 12973
		protected abstract void WriteBoolean(bool val, string name);

		// Token: 0x060032AE RID: 12974
		protected abstract void WriteByte(byte val, string name);

		// Token: 0x060032AF RID: 12975
		protected abstract void WriteChar(char val, string name);

		// Token: 0x060032B0 RID: 12976
		protected abstract void WriteDateTime(DateTime val, string name);

		// Token: 0x060032B1 RID: 12977
		protected abstract void WriteDecimal(decimal val, string name);

		// Token: 0x060032B2 RID: 12978
		protected abstract void WriteDouble(double val, string name);

		// Token: 0x060032B3 RID: 12979
		protected abstract void WriteInt16(short val, string name);

		// Token: 0x060032B4 RID: 12980
		protected abstract void WriteInt32(int val, string name);

		// Token: 0x060032B5 RID: 12981
		protected abstract void WriteInt64(long val, string name);

		// Token: 0x060032B6 RID: 12982 RVA: 0x000A4674 File Offset: 0x000A2874
		protected virtual void WriteMember(string memberName, object data)
		{
			if (data == null)
			{
				this.WriteObjectRef(data, memberName, typeof(object));
			}
			Type type = data.GetType();
			if (type.IsArray)
			{
				this.WriteArray(data, memberName, type);
			}
			else if (type == typeof(bool))
			{
				this.WriteBoolean((bool)data, memberName);
			}
			else if (type == typeof(byte))
			{
				this.WriteByte((byte)data, memberName);
			}
			else if (type == typeof(char))
			{
				this.WriteChar((char)data, memberName);
			}
			else if (type == typeof(DateTime))
			{
				this.WriteDateTime((DateTime)data, memberName);
			}
			else if (type == typeof(decimal))
			{
				this.WriteDecimal((decimal)data, memberName);
			}
			else if (type == typeof(double))
			{
				this.WriteDouble((double)data, memberName);
			}
			else if (type == typeof(short))
			{
				this.WriteInt16((short)data, memberName);
			}
			else if (type == typeof(int))
			{
				this.WriteInt32((int)data, memberName);
			}
			else if (type == typeof(long))
			{
				this.WriteInt64((long)data, memberName);
			}
			else if (type == typeof(sbyte))
			{
				this.WriteSByte((sbyte)data, memberName);
			}
			else if (type == typeof(float))
			{
				this.WriteSingle((float)data, memberName);
			}
			else if (type == typeof(TimeSpan))
			{
				this.WriteTimeSpan((TimeSpan)data, memberName);
			}
			else if (type == typeof(ushort))
			{
				this.WriteUInt16((ushort)data, memberName);
			}
			else if (type == typeof(uint))
			{
				this.WriteUInt32((uint)data, memberName);
			}
			else if (type == typeof(ulong))
			{
				this.WriteUInt64((ulong)data, memberName);
			}
			else if (type.IsValueType)
			{
				this.WriteValueType(data, memberName, type);
			}
			this.WriteObjectRef(data, memberName, type);
		}

		// Token: 0x060032B7 RID: 12983
		protected abstract void WriteObjectRef(object obj, string name, Type memberType);

		// Token: 0x060032B8 RID: 12984
		[CLSCompliant(false)]
		protected abstract void WriteSByte(sbyte val, string name);

		// Token: 0x060032B9 RID: 12985
		protected abstract void WriteSingle(float val, string name);

		// Token: 0x060032BA RID: 12986
		protected abstract void WriteTimeSpan(TimeSpan val, string name);

		// Token: 0x060032BB RID: 12987
		[CLSCompliant(false)]
		protected abstract void WriteUInt16(ushort val, string name);

		// Token: 0x060032BC RID: 12988
		[CLSCompliant(false)]
		protected abstract void WriteUInt32(uint val, string name);

		// Token: 0x060032BD RID: 12989
		[CLSCompliant(false)]
		protected abstract void WriteUInt64(ulong val, string name);

		// Token: 0x060032BE RID: 12990
		protected abstract void WriteValueType(object obj, string name, Type memberType);

		// Token: 0x0400152C RID: 5420
		protected ObjectIDGenerator m_idGenerator = new ObjectIDGenerator();

		// Token: 0x0400152D RID: 5421
		protected Queue m_objectQueue = new Queue();
	}
}
