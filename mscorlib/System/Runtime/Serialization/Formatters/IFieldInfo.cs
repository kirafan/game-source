﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000512 RID: 1298
	[ComVisible(true)]
	public interface IFieldInfo
	{
		// Token: 0x170009AE RID: 2478
		// (get) Token: 0x0600338F RID: 13199
		// (set) Token: 0x06003390 RID: 13200
		string[] FieldNames { get; set; }

		// Token: 0x170009AF RID: 2479
		// (get) Token: 0x06003391 RID: 13201
		// (set) Token: 0x06003392 RID: 13202
		Type[] FieldTypes { get; set; }
	}
}
