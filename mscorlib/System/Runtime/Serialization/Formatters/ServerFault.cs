﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Metadata;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000516 RID: 1302
	[SoapType]
	[ComVisible(true)]
	[Serializable]
	public sealed class ServerFault
	{
		// Token: 0x060033A9 RID: 13225 RVA: 0x000A6DF4 File Offset: 0x000A4FF4
		public ServerFault(string exceptionType, string message, string stackTrace)
		{
			this.exceptionType = exceptionType;
			this.message = message;
			this.stackTrace = stackTrace;
		}

		// Token: 0x170009B6 RID: 2486
		// (get) Token: 0x060033AA RID: 13226 RVA: 0x000A6E14 File Offset: 0x000A5014
		// (set) Token: 0x060033AB RID: 13227 RVA: 0x000A6E1C File Offset: 0x000A501C
		public string ExceptionType
		{
			get
			{
				return this.exceptionType;
			}
			set
			{
				this.exceptionType = value;
			}
		}

		// Token: 0x170009B7 RID: 2487
		// (get) Token: 0x060033AC RID: 13228 RVA: 0x000A6E28 File Offset: 0x000A5028
		// (set) Token: 0x060033AD RID: 13229 RVA: 0x000A6E30 File Offset: 0x000A5030
		public string ExceptionMessage
		{
			get
			{
				return this.message;
			}
			set
			{
				this.message = value;
			}
		}

		// Token: 0x170009B8 RID: 2488
		// (get) Token: 0x060033AE RID: 13230 RVA: 0x000A6E3C File Offset: 0x000A503C
		// (set) Token: 0x060033AF RID: 13231 RVA: 0x000A6E44 File Offset: 0x000A5044
		public string StackTrace
		{
			get
			{
				return this.stackTrace;
			}
			set
			{
				this.stackTrace = value;
			}
		}

		// Token: 0x0400157E RID: 5502
		private string exceptionType;

		// Token: 0x0400157F RID: 5503
		private string message;

		// Token: 0x04001580 RID: 5504
		private string stackTrace;

		// Token: 0x04001581 RID: 5505
		private Exception exception;
	}
}
