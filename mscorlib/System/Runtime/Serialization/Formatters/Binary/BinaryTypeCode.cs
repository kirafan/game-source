﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000521 RID: 1313
	internal enum BinaryTypeCode : byte
	{
		// Token: 0x040015D0 RID: 5584
		Boolean = 1,
		// Token: 0x040015D1 RID: 5585
		Byte,
		// Token: 0x040015D2 RID: 5586
		Char,
		// Token: 0x040015D3 RID: 5587
		Decimal = 5,
		// Token: 0x040015D4 RID: 5588
		Double,
		// Token: 0x040015D5 RID: 5589
		Int16,
		// Token: 0x040015D6 RID: 5590
		Int32,
		// Token: 0x040015D7 RID: 5591
		Int64,
		// Token: 0x040015D8 RID: 5592
		SByte,
		// Token: 0x040015D9 RID: 5593
		Single,
		// Token: 0x040015DA RID: 5594
		TimeSpan,
		// Token: 0x040015DB RID: 5595
		DateTime,
		// Token: 0x040015DC RID: 5596
		UInt16,
		// Token: 0x040015DD RID: 5597
		UInt32,
		// Token: 0x040015DE RID: 5598
		UInt64,
		// Token: 0x040015DF RID: 5599
		Null,
		// Token: 0x040015E0 RID: 5600
		String
	}
}
