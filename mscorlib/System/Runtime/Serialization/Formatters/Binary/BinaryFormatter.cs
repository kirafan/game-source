﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Security.Permissions;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200051A RID: 1306
	[ComVisible(true)]
	public sealed class BinaryFormatter : IRemotingFormatter, IFormatter
	{
		// Token: 0x060033C9 RID: 13257 RVA: 0x000A7000 File Offset: 0x000A5200
		public BinaryFormatter()
		{
			this.surrogate_selector = BinaryFormatter.DefaultSurrogateSelector;
			this.context = new StreamingContext(StreamingContextStates.All);
		}

		// Token: 0x060033CA RID: 13258 RVA: 0x000A7034 File Offset: 0x000A5234
		public BinaryFormatter(ISurrogateSelector selector, StreamingContext context)
		{
			this.surrogate_selector = selector;
			this.context = context;
		}

		// Token: 0x170009C3 RID: 2499
		// (get) Token: 0x060033CB RID: 13259 RVA: 0x000A7064 File Offset: 0x000A5264
		// (set) Token: 0x060033CC RID: 13260 RVA: 0x000A706C File Offset: 0x000A526C
		public static ISurrogateSelector DefaultSurrogateSelector { get; set; }

		// Token: 0x170009C4 RID: 2500
		// (get) Token: 0x060033CD RID: 13261 RVA: 0x000A7074 File Offset: 0x000A5274
		// (set) Token: 0x060033CE RID: 13262 RVA: 0x000A707C File Offset: 0x000A527C
		public FormatterAssemblyStyle AssemblyFormat
		{
			get
			{
				return this.assembly_format;
			}
			set
			{
				this.assembly_format = value;
			}
		}

		// Token: 0x170009C5 RID: 2501
		// (get) Token: 0x060033CF RID: 13263 RVA: 0x000A7088 File Offset: 0x000A5288
		// (set) Token: 0x060033D0 RID: 13264 RVA: 0x000A7090 File Offset: 0x000A5290
		public SerializationBinder Binder
		{
			get
			{
				return this.binder;
			}
			set
			{
				this.binder = value;
			}
		}

		// Token: 0x170009C6 RID: 2502
		// (get) Token: 0x060033D1 RID: 13265 RVA: 0x000A709C File Offset: 0x000A529C
		// (set) Token: 0x060033D2 RID: 13266 RVA: 0x000A70A4 File Offset: 0x000A52A4
		public StreamingContext Context
		{
			get
			{
				return this.context;
			}
			set
			{
				this.context = value;
			}
		}

		// Token: 0x170009C7 RID: 2503
		// (get) Token: 0x060033D3 RID: 13267 RVA: 0x000A70B0 File Offset: 0x000A52B0
		// (set) Token: 0x060033D4 RID: 13268 RVA: 0x000A70B8 File Offset: 0x000A52B8
		public ISurrogateSelector SurrogateSelector
		{
			get
			{
				return this.surrogate_selector;
			}
			set
			{
				this.surrogate_selector = value;
			}
		}

		// Token: 0x170009C8 RID: 2504
		// (get) Token: 0x060033D5 RID: 13269 RVA: 0x000A70C4 File Offset: 0x000A52C4
		// (set) Token: 0x060033D6 RID: 13270 RVA: 0x000A70CC File Offset: 0x000A52CC
		public FormatterTypeStyle TypeFormat
		{
			get
			{
				return this.type_format;
			}
			set
			{
				this.type_format = value;
			}
		}

		// Token: 0x170009C9 RID: 2505
		// (get) Token: 0x060033D7 RID: 13271 RVA: 0x000A70D8 File Offset: 0x000A52D8
		// (set) Token: 0x060033D8 RID: 13272 RVA: 0x000A70E0 File Offset: 0x000A52E0
		public TypeFilterLevel FilterLevel
		{
			get
			{
				return this.filter_level;
			}
			set
			{
				this.filter_level = value;
			}
		}

		// Token: 0x060033D9 RID: 13273 RVA: 0x000A70EC File Offset: 0x000A52EC
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public object Deserialize(Stream serializationStream)
		{
			return this.NoCheckDeserialize(serializationStream, null);
		}

		// Token: 0x060033DA RID: 13274 RVA: 0x000A70F8 File Offset: 0x000A52F8
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public object Deserialize(Stream serializationStream, HeaderHandler handler)
		{
			return this.NoCheckDeserialize(serializationStream, handler);
		}

		// Token: 0x060033DB RID: 13275 RVA: 0x000A7104 File Offset: 0x000A5304
		private object NoCheckDeserialize(Stream serializationStream, HeaderHandler handler)
		{
			if (serializationStream == null)
			{
				throw new ArgumentNullException("serializationStream");
			}
			if (serializationStream.CanSeek && serializationStream.Length == 0L)
			{
				throw new SerializationException("serializationStream supports seeking, but its length is 0");
			}
			BinaryReader binaryReader = new BinaryReader(serializationStream);
			bool flag;
			this.ReadBinaryHeader(binaryReader, out flag);
			BinaryElement binaryElement = (BinaryElement)binaryReader.Read();
			if (binaryElement == BinaryElement.MethodCall)
			{
				return MessageFormatter.ReadMethodCall(binaryElement, binaryReader, flag, handler, this);
			}
			if (binaryElement == BinaryElement.MethodResponse)
			{
				return MessageFormatter.ReadMethodResponse(binaryElement, binaryReader, flag, handler, null, this);
			}
			ObjectReader objectReader = new ObjectReader(this);
			object result;
			Header[] headers;
			objectReader.ReadObjectGraph(binaryElement, binaryReader, flag, out result, out headers);
			if (handler != null)
			{
				handler(headers);
			}
			return result;
		}

		// Token: 0x060033DC RID: 13276 RVA: 0x000A71A8 File Offset: 0x000A53A8
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public object DeserializeMethodResponse(Stream serializationStream, HeaderHandler handler, IMethodCallMessage methodCallMessage)
		{
			return this.NoCheckDeserializeMethodResponse(serializationStream, handler, methodCallMessage);
		}

		// Token: 0x060033DD RID: 13277 RVA: 0x000A71B4 File Offset: 0x000A53B4
		private object NoCheckDeserializeMethodResponse(Stream serializationStream, HeaderHandler handler, IMethodCallMessage methodCallMessage)
		{
			if (serializationStream == null)
			{
				throw new ArgumentNullException("serializationStream");
			}
			if (serializationStream.CanSeek && serializationStream.Length == 0L)
			{
				throw new SerializationException("serializationStream supports seeking, but its length is 0");
			}
			BinaryReader reader = new BinaryReader(serializationStream);
			bool hasHeaders;
			this.ReadBinaryHeader(reader, out hasHeaders);
			return MessageFormatter.ReadMethodResponse(reader, hasHeaders, handler, methodCallMessage, this);
		}

		// Token: 0x060033DE RID: 13278 RVA: 0x000A7210 File Offset: 0x000A5410
		public void Serialize(Stream serializationStream, object graph)
		{
			this.Serialize(serializationStream, graph, null);
		}

		// Token: 0x060033DF RID: 13279 RVA: 0x000A721C File Offset: 0x000A541C
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public void Serialize(Stream serializationStream, object graph, Header[] headers)
		{
			if (serializationStream == null)
			{
				throw new ArgumentNullException("serializationStream");
			}
			BinaryWriter binaryWriter = new BinaryWriter(serializationStream);
			this.WriteBinaryHeader(binaryWriter, headers != null);
			if (graph is IMethodCallMessage)
			{
				MessageFormatter.WriteMethodCall(binaryWriter, graph, headers, this.surrogate_selector, this.context, this.assembly_format, this.type_format);
			}
			else if (graph is IMethodReturnMessage)
			{
				MessageFormatter.WriteMethodResponse(binaryWriter, graph, headers, this.surrogate_selector, this.context, this.assembly_format, this.type_format);
			}
			else
			{
				ObjectWriter objectWriter = new ObjectWriter(this.surrogate_selector, this.context, this.assembly_format, this.type_format);
				objectWriter.WriteObjectGraph(binaryWriter, graph, headers);
			}
			binaryWriter.Flush();
		}

		// Token: 0x060033E0 RID: 13280 RVA: 0x000A72DC File Offset: 0x000A54DC
		[ComVisible(false)]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public object UnsafeDeserialize(Stream serializationStream, HeaderHandler handler)
		{
			return this.NoCheckDeserialize(serializationStream, handler);
		}

		// Token: 0x060033E1 RID: 13281 RVA: 0x000A72E8 File Offset: 0x000A54E8
		[ComVisible(false)]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public object UnsafeDeserializeMethodResponse(Stream serializationStream, HeaderHandler handler, IMethodCallMessage methodCallMessage)
		{
			return this.NoCheckDeserializeMethodResponse(serializationStream, handler, methodCallMessage);
		}

		// Token: 0x060033E2 RID: 13282 RVA: 0x000A72F4 File Offset: 0x000A54F4
		private void WriteBinaryHeader(BinaryWriter writer, bool hasHeaders)
		{
			writer.Write(0);
			writer.Write(1);
			if (hasHeaders)
			{
				writer.Write(2);
			}
			else
			{
				writer.Write(-1);
			}
			writer.Write(1);
			writer.Write(0);
		}

		// Token: 0x060033E3 RID: 13283 RVA: 0x000A7338 File Offset: 0x000A5538
		private void ReadBinaryHeader(BinaryReader reader, out bool hasHeaders)
		{
			reader.ReadByte();
			reader.ReadInt32();
			int num = reader.ReadInt32();
			hasHeaders = (num == 2);
			reader.ReadInt32();
			reader.ReadInt32();
		}

		// Token: 0x0400158F RID: 5519
		private FormatterAssemblyStyle assembly_format;

		// Token: 0x04001590 RID: 5520
		private SerializationBinder binder;

		// Token: 0x04001591 RID: 5521
		private StreamingContext context;

		// Token: 0x04001592 RID: 5522
		private ISurrogateSelector surrogate_selector;

		// Token: 0x04001593 RID: 5523
		private FormatterTypeStyle type_format = FormatterTypeStyle.TypesAlways;

		// Token: 0x04001594 RID: 5524
		private TypeFilterLevel filter_level = TypeFilterLevel.Full;
	}
}
