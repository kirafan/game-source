﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200051E RID: 1310
	internal enum ArrayStructure : byte
	{
		// Token: 0x040015BC RID: 5564
		SingleDimensional,
		// Token: 0x040015BD RID: 5565
		Jagged,
		// Token: 0x040015BE RID: 5566
		MultiDimensional
	}
}
