﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x0200051F RID: 1311
	internal enum MethodFlags
	{
		// Token: 0x040015C0 RID: 5568
		NoArguments = 1,
		// Token: 0x040015C1 RID: 5569
		PrimitiveArguments,
		// Token: 0x040015C2 RID: 5570
		ArgumentsInSimpleArray = 4,
		// Token: 0x040015C3 RID: 5571
		ArgumentsInMultiArray = 8,
		// Token: 0x040015C4 RID: 5572
		ExcludeLogicalCallContext = 16,
		// Token: 0x040015C5 RID: 5573
		IncludesLogicalCallContext = 64,
		// Token: 0x040015C6 RID: 5574
		IncludesSignature = 128,
		// Token: 0x040015C7 RID: 5575
		FormatMask = 15,
		// Token: 0x040015C8 RID: 5576
		GenericArguments = 32768,
		// Token: 0x040015C9 RID: 5577
		NeedsInfoArrayMask = 32972
	}
}
