﻿using System;

namespace System.Runtime.Serialization.Formatters.Binary
{
	// Token: 0x02000520 RID: 1312
	internal enum ReturnTypeTag : byte
	{
		// Token: 0x040015CB RID: 5579
		Null = 2,
		// Token: 0x040015CC RID: 5580
		PrimitiveType = 8,
		// Token: 0x040015CD RID: 5581
		ObjectType = 16,
		// Token: 0x040015CE RID: 5582
		Exception = 32
	}
}
