﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000510 RID: 1296
	[ComVisible(true)]
	[Serializable]
	public enum FormatterAssemblyStyle
	{
		// Token: 0x04001578 RID: 5496
		Simple,
		// Token: 0x04001579 RID: 5497
		Full
	}
}
