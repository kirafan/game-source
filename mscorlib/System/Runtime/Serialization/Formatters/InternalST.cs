﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000514 RID: 1300
	[ComVisible(true)]
	public sealed class InternalST
	{
		// Token: 0x06003396 RID: 13206 RVA: 0x000A6DBC File Offset: 0x000A4FBC
		private InternalST()
		{
		}

		// Token: 0x06003397 RID: 13207 RVA: 0x000A6DC4 File Offset: 0x000A4FC4
		[Conditional("_LOGGING")]
		public static void InfoSoap(params object[] messages)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003398 RID: 13208 RVA: 0x000A6DCC File Offset: 0x000A4FCC
		public static Assembly LoadAssemblyFromString(string assemblyString)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003399 RID: 13209 RVA: 0x000A6DD4 File Offset: 0x000A4FD4
		public static void SerializationSetValue(FieldInfo fi, object target, object value)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600339A RID: 13210 RVA: 0x000A6DDC File Offset: 0x000A4FDC
		[Conditional("SER_LOGGING")]
		public static void Soap(params object[] messages)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600339B RID: 13211 RVA: 0x000A6DE4 File Offset: 0x000A4FE4
		[Conditional("_DEBUG")]
		public static void SoapAssert(bool condition, string message)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600339C RID: 13212 RVA: 0x000A6DEC File Offset: 0x000A4FEC
		public static bool SoapCheckEnabled()
		{
			throw new NotImplementedException();
		}
	}
}
