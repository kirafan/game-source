﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Metadata;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000517 RID: 1303
	[SoapType]
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapFault : ISerializable
	{
		// Token: 0x060033B0 RID: 13232 RVA: 0x000A6E50 File Offset: 0x000A5050
		public SoapFault()
		{
		}

		// Token: 0x060033B1 RID: 13233 RVA: 0x000A6E58 File Offset: 0x000A5058
		private SoapFault(SerializationInfo info, StreamingContext context)
		{
			this.code = info.GetString("faultcode");
			this.faultString = info.GetString("faultstring");
			this.detail = info.GetValue("detail", typeof(object));
		}

		// Token: 0x060033B2 RID: 13234 RVA: 0x000A6EA8 File Offset: 0x000A50A8
		public SoapFault(string faultCode, string faultString, string faultActor, ServerFault serverFault)
		{
			this.code = faultCode;
			this.actor = faultActor;
			this.faultString = faultString;
			this.detail = serverFault;
		}

		// Token: 0x170009B9 RID: 2489
		// (get) Token: 0x060033B3 RID: 13235 RVA: 0x000A6ED0 File Offset: 0x000A50D0
		// (set) Token: 0x060033B4 RID: 13236 RVA: 0x000A6ED8 File Offset: 0x000A50D8
		public object Detail
		{
			get
			{
				return this.detail;
			}
			set
			{
				this.detail = value;
			}
		}

		// Token: 0x170009BA RID: 2490
		// (get) Token: 0x060033B5 RID: 13237 RVA: 0x000A6EE4 File Offset: 0x000A50E4
		// (set) Token: 0x060033B6 RID: 13238 RVA: 0x000A6EEC File Offset: 0x000A50EC
		public string FaultActor
		{
			get
			{
				return this.actor;
			}
			set
			{
				this.actor = value;
			}
		}

		// Token: 0x170009BB RID: 2491
		// (get) Token: 0x060033B7 RID: 13239 RVA: 0x000A6EF8 File Offset: 0x000A50F8
		// (set) Token: 0x060033B8 RID: 13240 RVA: 0x000A6F00 File Offset: 0x000A5100
		public string FaultCode
		{
			get
			{
				return this.code;
			}
			set
			{
				this.code = value;
			}
		}

		// Token: 0x170009BC RID: 2492
		// (get) Token: 0x060033B9 RID: 13241 RVA: 0x000A6F0C File Offset: 0x000A510C
		// (set) Token: 0x060033BA RID: 13242 RVA: 0x000A6F14 File Offset: 0x000A5114
		public string FaultString
		{
			get
			{
				return this.faultString;
			}
			set
			{
				this.faultString = value;
			}
		}

		// Token: 0x060033BB RID: 13243 RVA: 0x000A6F20 File Offset: 0x000A5120
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("faultcode", this.code, typeof(string));
			info.AddValue("faultstring", this.faultString, typeof(string));
			info.AddValue("detail", this.detail, typeof(object));
		}

		// Token: 0x04001582 RID: 5506
		private string code;

		// Token: 0x04001583 RID: 5507
		private string actor;

		// Token: 0x04001584 RID: 5508
		private string faultString;

		// Token: 0x04001585 RID: 5509
		private object detail;
	}
}
