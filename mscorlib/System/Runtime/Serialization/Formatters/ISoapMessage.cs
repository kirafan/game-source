﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000515 RID: 1301
	[ComVisible(true)]
	public interface ISoapMessage
	{
		// Token: 0x170009B0 RID: 2480
		// (get) Token: 0x0600339D RID: 13213
		// (set) Token: 0x0600339E RID: 13214
		Header[] Headers { get; set; }

		// Token: 0x170009B1 RID: 2481
		// (get) Token: 0x0600339F RID: 13215
		// (set) Token: 0x060033A0 RID: 13216
		string MethodName { get; set; }

		// Token: 0x170009B2 RID: 2482
		// (get) Token: 0x060033A1 RID: 13217
		// (set) Token: 0x060033A2 RID: 13218
		string[] ParamNames { get; set; }

		// Token: 0x170009B3 RID: 2483
		// (get) Token: 0x060033A3 RID: 13219
		// (set) Token: 0x060033A4 RID: 13220
		Type[] ParamTypes { get; set; }

		// Token: 0x170009B4 RID: 2484
		// (get) Token: 0x060033A5 RID: 13221
		// (set) Token: 0x060033A6 RID: 13222
		object[] ParamValues { get; set; }

		// Token: 0x170009B5 RID: 2485
		// (get) Token: 0x060033A7 RID: 13223
		// (set) Token: 0x060033A8 RID: 13224
		string XmlNameSpace { get; set; }
	}
}
