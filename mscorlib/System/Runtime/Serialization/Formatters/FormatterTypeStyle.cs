﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000511 RID: 1297
	[ComVisible(true)]
	[Serializable]
	public enum FormatterTypeStyle
	{
		// Token: 0x0400157B RID: 5499
		TypesWhenNeeded,
		// Token: 0x0400157C RID: 5500
		TypesAlways,
		// Token: 0x0400157D RID: 5501
		XsdString
	}
}
