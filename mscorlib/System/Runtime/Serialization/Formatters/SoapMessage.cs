﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000518 RID: 1304
	[ComVisible(true)]
	[Serializable]
	public class SoapMessage : ISoapMessage
	{
		// Token: 0x170009BD RID: 2493
		// (get) Token: 0x060033BD RID: 13245 RVA: 0x000A6F88 File Offset: 0x000A5188
		// (set) Token: 0x060033BE RID: 13246 RVA: 0x000A6F90 File Offset: 0x000A5190
		public Header[] Headers
		{
			get
			{
				return this.headers;
			}
			set
			{
				this.headers = value;
			}
		}

		// Token: 0x170009BE RID: 2494
		// (get) Token: 0x060033BF RID: 13247 RVA: 0x000A6F9C File Offset: 0x000A519C
		// (set) Token: 0x060033C0 RID: 13248 RVA: 0x000A6FA4 File Offset: 0x000A51A4
		public string MethodName
		{
			get
			{
				return this.methodName;
			}
			set
			{
				this.methodName = value;
			}
		}

		// Token: 0x170009BF RID: 2495
		// (get) Token: 0x060033C1 RID: 13249 RVA: 0x000A6FB0 File Offset: 0x000A51B0
		// (set) Token: 0x060033C2 RID: 13250 RVA: 0x000A6FB8 File Offset: 0x000A51B8
		public string[] ParamNames
		{
			get
			{
				return this.paramNames;
			}
			set
			{
				this.paramNames = value;
			}
		}

		// Token: 0x170009C0 RID: 2496
		// (get) Token: 0x060033C3 RID: 13251 RVA: 0x000A6FC4 File Offset: 0x000A51C4
		// (set) Token: 0x060033C4 RID: 13252 RVA: 0x000A6FCC File Offset: 0x000A51CC
		public Type[] ParamTypes
		{
			get
			{
				return this.paramTypes;
			}
			set
			{
				this.paramTypes = value;
			}
		}

		// Token: 0x170009C1 RID: 2497
		// (get) Token: 0x060033C5 RID: 13253 RVA: 0x000A6FD8 File Offset: 0x000A51D8
		// (set) Token: 0x060033C6 RID: 13254 RVA: 0x000A6FE0 File Offset: 0x000A51E0
		public object[] ParamValues
		{
			get
			{
				return this.paramValues;
			}
			set
			{
				this.paramValues = value;
			}
		}

		// Token: 0x170009C2 RID: 2498
		// (get) Token: 0x060033C7 RID: 13255 RVA: 0x000A6FEC File Offset: 0x000A51EC
		// (set) Token: 0x060033C8 RID: 13256 RVA: 0x000A6FF4 File Offset: 0x000A51F4
		public string XmlNameSpace
		{
			get
			{
				return this.xmlNameSpace;
			}
			set
			{
				this.xmlNameSpace = value;
			}
		}

		// Token: 0x04001586 RID: 5510
		private Header[] headers;

		// Token: 0x04001587 RID: 5511
		private string methodName;

		// Token: 0x04001588 RID: 5512
		private string[] paramNames;

		// Token: 0x04001589 RID: 5513
		private Type[] paramTypes;

		// Token: 0x0400158A RID: 5514
		private object[] paramValues;

		// Token: 0x0400158B RID: 5515
		private string xmlNameSpace;
	}
}
