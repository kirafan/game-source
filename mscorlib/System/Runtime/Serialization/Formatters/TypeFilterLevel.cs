﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization.Formatters
{
	// Token: 0x02000519 RID: 1305
	[ComVisible(true)]
	public enum TypeFilterLevel
	{
		// Token: 0x0400158D RID: 5517
		Low = 2,
		// Token: 0x0400158E RID: 5518
		Full
	}
}
