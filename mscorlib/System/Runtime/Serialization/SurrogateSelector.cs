﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x0200050F RID: 1295
	[ComVisible(true)]
	public class SurrogateSelector : ISurrogateSelector
	{
		// Token: 0x0600338A RID: 13194 RVA: 0x000A6C2C File Offset: 0x000A4E2C
		public virtual void AddSurrogate(Type type, StreamingContext context, ISerializationSurrogate surrogate)
		{
			if (type == null || surrogate == null)
			{
				throw new ArgumentNullException("Null reference.");
			}
			string key = type.FullName + "#" + context.ToString();
			if (this.Surrogates.ContainsKey(key))
			{
				throw new ArgumentException("A surrogate for " + type.FullName + " already exists.");
			}
			this.Surrogates.Add(key, surrogate);
		}

		// Token: 0x0600338B RID: 13195 RVA: 0x000A6CA8 File Offset: 0x000A4EA8
		public virtual void ChainSelector(ISurrogateSelector selector)
		{
			if (selector == null)
			{
				throw new ArgumentNullException("Selector is null.");
			}
			if (this.nextSelector != null)
			{
				selector.ChainSelector(this.nextSelector);
			}
			this.nextSelector = selector;
		}

		// Token: 0x0600338C RID: 13196 RVA: 0x000A6CDC File Offset: 0x000A4EDC
		public virtual ISurrogateSelector GetNextSelector()
		{
			return this.nextSelector;
		}

		// Token: 0x0600338D RID: 13197 RVA: 0x000A6CE4 File Offset: 0x000A4EE4
		public virtual ISerializationSurrogate GetSurrogate(Type type, StreamingContext context, out ISurrogateSelector selector)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type is null.");
			}
			string key = type.FullName + "#" + context.ToString();
			ISerializationSurrogate serializationSurrogate = (ISerializationSurrogate)this.Surrogates[key];
			if (serializationSurrogate != null)
			{
				selector = this;
				return serializationSurrogate;
			}
			if (this.nextSelector != null)
			{
				return this.nextSelector.GetSurrogate(type, context, out selector);
			}
			selector = null;
			return null;
		}

		// Token: 0x0600338E RID: 13198 RVA: 0x000A6D5C File Offset: 0x000A4F5C
		public virtual void RemoveSurrogate(Type type, StreamingContext context)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type is null.");
			}
			string key = type.FullName + "#" + context.ToString();
			this.Surrogates.Remove(key);
		}

		// Token: 0x04001575 RID: 5493
		private Hashtable Surrogates = new Hashtable();

		// Token: 0x04001576 RID: 5494
		private ISurrogateSelector nextSelector;
	}
}
