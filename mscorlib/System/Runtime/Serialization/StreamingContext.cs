﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x0200050D RID: 1293
	[ComVisible(true)]
	[Serializable]
	public struct StreamingContext
	{
		// Token: 0x06003383 RID: 13187 RVA: 0x000A6B98 File Offset: 0x000A4D98
		public StreamingContext(StreamingContextStates state)
		{
			this.state = state;
			this.additional = null;
		}

		// Token: 0x06003384 RID: 13188 RVA: 0x000A6BA8 File Offset: 0x000A4DA8
		public StreamingContext(StreamingContextStates state, object additional)
		{
			this.state = state;
			this.additional = additional;
		}

		// Token: 0x170009AC RID: 2476
		// (get) Token: 0x06003385 RID: 13189 RVA: 0x000A6BB8 File Offset: 0x000A4DB8
		public object Context
		{
			get
			{
				return this.additional;
			}
		}

		// Token: 0x170009AD RID: 2477
		// (get) Token: 0x06003386 RID: 13190 RVA: 0x000A6BC0 File Offset: 0x000A4DC0
		public StreamingContextStates State
		{
			get
			{
				return this.state;
			}
		}

		// Token: 0x06003387 RID: 13191 RVA: 0x000A6BC8 File Offset: 0x000A4DC8
		public override bool Equals(object obj)
		{
			if (!(obj is StreamingContext))
			{
				return false;
			}
			StreamingContext streamingContext = (StreamingContext)obj;
			return streamingContext.state == this.state && streamingContext.additional == this.additional;
		}

		// Token: 0x06003388 RID: 13192 RVA: 0x000A6C10 File Offset: 0x000A4E10
		public override int GetHashCode()
		{
			return (int)this.state;
		}

		// Token: 0x04001569 RID: 5481
		private StreamingContextStates state;

		// Token: 0x0400156A RID: 5482
		private object additional;
	}
}
