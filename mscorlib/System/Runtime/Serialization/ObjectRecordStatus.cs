﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020004FF RID: 1279
	internal enum ObjectRecordStatus : byte
	{
		// Token: 0x04001544 RID: 5444
		Unregistered,
		// Token: 0x04001545 RID: 5445
		ReferenceUnsolved,
		// Token: 0x04001546 RID: 5446
		ReferenceSolvingDelayed,
		// Token: 0x04001547 RID: 5447
		ReferenceSolved
	}
}
