﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x02000505 RID: 1285
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	public sealed class OptionalFieldAttribute : Attribute
	{
		// Token: 0x1700099C RID: 2460
		// (get) Token: 0x06003332 RID: 13106 RVA: 0x000A5E88 File Offset: 0x000A4088
		// (set) Token: 0x06003333 RID: 13107 RVA: 0x000A5E90 File Offset: 0x000A4090
		public int VersionAdded
		{
			get
			{
				return this.version_added;
			}
			set
			{
				this.version_added = value;
			}
		}

		// Token: 0x04001555 RID: 5461
		private int version_added;
	}
}
