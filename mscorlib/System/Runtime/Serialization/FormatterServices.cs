﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Serialization.Formatters;
using System.Security;

namespace System.Runtime.Serialization
{
	// Token: 0x020004F0 RID: 1264
	[ComVisible(true)]
	public sealed class FormatterServices
	{
		// Token: 0x060032D1 RID: 13009 RVA: 0x000A4A98 File Offset: 0x000A2C98
		private FormatterServices()
		{
		}

		// Token: 0x060032D2 RID: 13010 RVA: 0x000A4AA0 File Offset: 0x000A2CA0
		public static object[] GetObjectData(object obj, MemberInfo[] members)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (members == null)
			{
				throw new ArgumentNullException("members");
			}
			int num = members.Length;
			object[] array = new object[num];
			for (int i = 0; i < num; i++)
			{
				MemberInfo memberInfo = members[i];
				if (memberInfo == null)
				{
					throw new ArgumentNullException(string.Format("members[{0}]", i));
				}
				if (memberInfo.MemberType != MemberTypes.Field)
				{
					throw new SerializationException(string.Format("members [{0}] is not a field.", i));
				}
				FieldInfo fieldInfo = memberInfo as FieldInfo;
				array[i] = fieldInfo.GetValue(obj);
			}
			return array;
		}

		// Token: 0x060032D3 RID: 13011 RVA: 0x000A4B44 File Offset: 0x000A2D44
		public static MemberInfo[] GetSerializableMembers(Type type)
		{
			StreamingContext context = new StreamingContext(StreamingContextStates.All);
			return FormatterServices.GetSerializableMembers(type, context);
		}

		// Token: 0x060032D4 RID: 13012 RVA: 0x000A4B64 File Offset: 0x000A2D64
		public static MemberInfo[] GetSerializableMembers(Type type, StreamingContext context)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			ArrayList arrayList = new ArrayList();
			for (Type type2 = type; type2 != null; type2 = type2.BaseType)
			{
				if (!type2.IsSerializable)
				{
					string message = string.Format("Type {0} in assembly {1} is not marked as serializable.", type2, type2.Assembly.FullName);
					throw new SerializationException(message);
				}
				FormatterServices.GetFields(type, type2, arrayList);
			}
			MemberInfo[] array = new MemberInfo[arrayList.Count];
			arrayList.CopyTo(array);
			return array;
		}

		// Token: 0x060032D5 RID: 13013 RVA: 0x000A4BE4 File Offset: 0x000A2DE4
		private static void GetFields(Type reflectedType, Type type, ArrayList fields)
		{
			FieldInfo[] fields2 = type.GetFields(BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
			foreach (FieldInfo fieldInfo in fields2)
			{
				if (!fieldInfo.IsNotSerialized)
				{
					MonoField monoField = fieldInfo as MonoField;
					if (monoField != null && reflectedType != type && !monoField.IsPublic)
					{
						string newName = type.Name + "+" + monoField.Name;
						fields.Add(monoField.Clone(newName));
					}
					else
					{
						fields.Add(fieldInfo);
					}
				}
			}
		}

		// Token: 0x060032D6 RID: 13014 RVA: 0x000A4C78 File Offset: 0x000A2E78
		public static Type GetTypeFromAssembly(Assembly assem, string name)
		{
			if (assem == null)
			{
				throw new ArgumentNullException("assem");
			}
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			return assem.GetType(name);
		}

		// Token: 0x060032D7 RID: 13015 RVA: 0x000A4CA4 File Offset: 0x000A2EA4
		public static object GetUninitializedObject(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (type == typeof(string))
			{
				throw new ArgumentException("Uninitialized Strings cannot be created.");
			}
			return ActivationServices.AllocateUninitializedClassInstance(type);
		}

		// Token: 0x060032D8 RID: 13016 RVA: 0x000A4CE4 File Offset: 0x000A2EE4
		public static object PopulateObjectMembers(object obj, MemberInfo[] members, object[] data)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (members == null)
			{
				throw new ArgumentNullException("members");
			}
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			int num = members.Length;
			if (num != data.Length)
			{
				throw new ArgumentException("different length in members and data");
			}
			for (int i = 0; i < num; i++)
			{
				MemberInfo memberInfo = members[i];
				if (memberInfo == null)
				{
					throw new ArgumentNullException(string.Format("members[{0}]", i));
				}
				if (memberInfo.MemberType != MemberTypes.Field)
				{
					throw new SerializationException(string.Format("members [{0}] is not a field.", i));
				}
				FieldInfo fieldInfo = memberInfo as FieldInfo;
				fieldInfo.SetValue(obj, data[i]);
			}
			return obj;
		}

		// Token: 0x060032D9 RID: 13017 RVA: 0x000A4DA4 File Offset: 0x000A2FA4
		public static void CheckTypeSecurity(Type t, TypeFilterLevel securityLevel)
		{
			if (securityLevel == TypeFilterLevel.Full)
			{
				return;
			}
			FormatterServices.CheckNotAssignable(typeof(DelegateSerializationHolder), t);
			FormatterServices.CheckNotAssignable(typeof(ISponsor), t);
			FormatterServices.CheckNotAssignable(typeof(IEnvoyInfo), t);
			FormatterServices.CheckNotAssignable(typeof(ObjRef), t);
		}

		// Token: 0x060032DA RID: 13018 RVA: 0x000A4DFC File Offset: 0x000A2FFC
		private static void CheckNotAssignable(Type basetype, Type type)
		{
			if (basetype.IsAssignableFrom(type))
			{
				string text = "Type " + basetype + " and the types derived from it";
				string text2 = text;
				text = string.Concat(new object[]
				{
					text2,
					" (such as ",
					type,
					") are not permitted to be deserialized at this security level"
				});
				throw new SecurityException(text);
			}
		}

		// Token: 0x060032DB RID: 13019 RVA: 0x000A4E54 File Offset: 0x000A3054
		public static object GetSafeUninitializedObject(Type type)
		{
			return FormatterServices.GetUninitializedObject(type);
		}

		// Token: 0x0400152E RID: 5422
		private const BindingFlags fieldFlags = BindingFlags.DeclaredOnly | BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
	}
}
