﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x02000509 RID: 1289
	[ComVisible(true)]
	[Serializable]
	public class SerializationException : SystemException
	{
		// Token: 0x06003347 RID: 13127 RVA: 0x000A61B4 File Offset: 0x000A43B4
		public SerializationException() : base("An error occurred during (de)serialization")
		{
		}

		// Token: 0x06003348 RID: 13128 RVA: 0x000A61C4 File Offset: 0x000A43C4
		public SerializationException(string message) : base(message)
		{
		}

		// Token: 0x06003349 RID: 13129 RVA: 0x000A61D0 File Offset: 0x000A43D0
		public SerializationException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600334A RID: 13130 RVA: 0x000A61DC File Offset: 0x000A43DC
		protected SerializationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
