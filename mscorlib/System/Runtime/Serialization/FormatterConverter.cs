﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004EF RID: 1263
	[ComVisible(true)]
	public class FormatterConverter : IFormatterConverter
	{
		// Token: 0x060032C0 RID: 12992 RVA: 0x000A48DC File Offset: 0x000A2ADC
		public object Convert(object value, Type type)
		{
			return System.Convert.ChangeType(value, type);
		}

		// Token: 0x060032C1 RID: 12993 RVA: 0x000A48E8 File Offset: 0x000A2AE8
		public object Convert(object value, TypeCode typeCode)
		{
			return System.Convert.ChangeType(value, typeCode);
		}

		// Token: 0x060032C2 RID: 12994 RVA: 0x000A48F4 File Offset: 0x000A2AF4
		public bool ToBoolean(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToBoolean(value);
		}

		// Token: 0x060032C3 RID: 12995 RVA: 0x000A4910 File Offset: 0x000A2B10
		public byte ToByte(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToByte(value);
		}

		// Token: 0x060032C4 RID: 12996 RVA: 0x000A492C File Offset: 0x000A2B2C
		public char ToChar(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToChar(value);
		}

		// Token: 0x060032C5 RID: 12997 RVA: 0x000A4948 File Offset: 0x000A2B48
		public DateTime ToDateTime(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToDateTime(value);
		}

		// Token: 0x060032C6 RID: 12998 RVA: 0x000A4964 File Offset: 0x000A2B64
		public decimal ToDecimal(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToDecimal(value);
		}

		// Token: 0x060032C7 RID: 12999 RVA: 0x000A4980 File Offset: 0x000A2B80
		public double ToDouble(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToDouble(value);
		}

		// Token: 0x060032C8 RID: 13000 RVA: 0x000A499C File Offset: 0x000A2B9C
		public short ToInt16(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToInt16(value);
		}

		// Token: 0x060032C9 RID: 13001 RVA: 0x000A49B8 File Offset: 0x000A2BB8
		public int ToInt32(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToInt32(value);
		}

		// Token: 0x060032CA RID: 13002 RVA: 0x000A49D4 File Offset: 0x000A2BD4
		public long ToInt64(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToInt64(value);
		}

		// Token: 0x060032CB RID: 13003 RVA: 0x000A49F0 File Offset: 0x000A2BF0
		public float ToSingle(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToSingle(value);
		}

		// Token: 0x060032CC RID: 13004 RVA: 0x000A4A0C File Offset: 0x000A2C0C
		public string ToString(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToString(value);
		}

		// Token: 0x060032CD RID: 13005 RVA: 0x000A4A28 File Offset: 0x000A2C28
		[CLSCompliant(false)]
		public sbyte ToSByte(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToSByte(value);
		}

		// Token: 0x060032CE RID: 13006 RVA: 0x000A4A44 File Offset: 0x000A2C44
		[CLSCompliant(false)]
		public ushort ToUInt16(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToUInt16(value);
		}

		// Token: 0x060032CF RID: 13007 RVA: 0x000A4A60 File Offset: 0x000A2C60
		[CLSCompliant(false)]
		public uint ToUInt32(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToUInt32(value);
		}

		// Token: 0x060032D0 RID: 13008 RVA: 0x000A4A7C File Offset: 0x000A2C7C
		[CLSCompliant(false)]
		public ulong ToUInt64(object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value is null.");
			}
			return System.Convert.ToUInt64(value);
		}
	}
}
