﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004F6 RID: 1270
	[ComVisible(true)]
	public interface ISurrogateSelector
	{
		// Token: 0x060032F9 RID: 13049
		void ChainSelector(ISurrogateSelector selector);

		// Token: 0x060032FA RID: 13050
		ISurrogateSelector GetNextSelector();

		// Token: 0x060032FB RID: 13051
		ISerializationSurrogate GetSurrogate(Type type, StreamingContext context, out ISurrogateSelector selector);
	}
}
