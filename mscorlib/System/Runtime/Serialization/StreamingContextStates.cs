﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x0200050E RID: 1294
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum StreamingContextStates
	{
		// Token: 0x0400156C RID: 5484
		CrossProcess = 1,
		// Token: 0x0400156D RID: 5485
		CrossMachine = 2,
		// Token: 0x0400156E RID: 5486
		File = 4,
		// Token: 0x0400156F RID: 5487
		Persistence = 8,
		// Token: 0x04001570 RID: 5488
		Remoting = 16,
		// Token: 0x04001571 RID: 5489
		Other = 32,
		// Token: 0x04001572 RID: 5490
		Clone = 64,
		// Token: 0x04001573 RID: 5491
		CrossAppDomain = 128,
		// Token: 0x04001574 RID: 5492
		All = 255
	}
}
