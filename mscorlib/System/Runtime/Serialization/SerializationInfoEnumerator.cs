﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x0200050B RID: 1291
	[ComVisible(true)]
	public sealed class SerializationInfoEnumerator : IEnumerator
	{
		// Token: 0x06003376 RID: 13174 RVA: 0x000A69D0 File Offset: 0x000A4BD0
		internal SerializationInfoEnumerator(ArrayList list)
		{
			this.enumerator = list.GetEnumerator();
		}

		// Token: 0x170009A7 RID: 2471
		// (get) Token: 0x06003377 RID: 13175 RVA: 0x000A69E4 File Offset: 0x000A4BE4
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		// Token: 0x170009A8 RID: 2472
		// (get) Token: 0x06003378 RID: 13176 RVA: 0x000A69F4 File Offset: 0x000A4BF4
		public SerializationEntry Current
		{
			get
			{
				return (SerializationEntry)this.enumerator.Current;
			}
		}

		// Token: 0x170009A9 RID: 2473
		// (get) Token: 0x06003379 RID: 13177 RVA: 0x000A6A08 File Offset: 0x000A4C08
		public string Name
		{
			get
			{
				SerializationEntry serializationEntry = this.Current;
				return serializationEntry.Name;
			}
		}

		// Token: 0x170009AA RID: 2474
		// (get) Token: 0x0600337A RID: 13178 RVA: 0x000A6A24 File Offset: 0x000A4C24
		public Type ObjectType
		{
			get
			{
				SerializationEntry serializationEntry = this.Current;
				return serializationEntry.ObjectType;
			}
		}

		// Token: 0x170009AB RID: 2475
		// (get) Token: 0x0600337B RID: 13179 RVA: 0x000A6A40 File Offset: 0x000A4C40
		public object Value
		{
			get
			{
				SerializationEntry serializationEntry = this.Current;
				return serializationEntry.Value;
			}
		}

		// Token: 0x0600337C RID: 13180 RVA: 0x000A6A5C File Offset: 0x000A4C5C
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		// Token: 0x0600337D RID: 13181 RVA: 0x000A6A6C File Offset: 0x000A4C6C
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x04001565 RID: 5477
		private IEnumerator enumerator;
	}
}
