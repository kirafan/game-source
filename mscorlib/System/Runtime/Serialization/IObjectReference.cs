﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004F4 RID: 1268
	[ComVisible(true)]
	public interface IObjectReference
	{
		// Token: 0x060032F6 RID: 13046
		object GetRealObject(StreamingContext context);
	}
}
