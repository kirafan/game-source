﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004F3 RID: 1267
	[CLSCompliant(false)]
	[ComVisible(true)]
	public interface IFormatterConverter
	{
		// Token: 0x060032E5 RID: 13029
		object Convert(object value, Type type);

		// Token: 0x060032E6 RID: 13030
		object Convert(object value, TypeCode typeCode);

		// Token: 0x060032E7 RID: 13031
		bool ToBoolean(object value);

		// Token: 0x060032E8 RID: 13032
		byte ToByte(object value);

		// Token: 0x060032E9 RID: 13033
		char ToChar(object value);

		// Token: 0x060032EA RID: 13034
		DateTime ToDateTime(object value);

		// Token: 0x060032EB RID: 13035
		decimal ToDecimal(object value);

		// Token: 0x060032EC RID: 13036
		double ToDouble(object value);

		// Token: 0x060032ED RID: 13037
		short ToInt16(object value);

		// Token: 0x060032EE RID: 13038
		int ToInt32(object value);

		// Token: 0x060032EF RID: 13039
		long ToInt64(object value);

		// Token: 0x060032F0 RID: 13040
		sbyte ToSByte(object value);

		// Token: 0x060032F1 RID: 13041
		float ToSingle(object value);

		// Token: 0x060032F2 RID: 13042
		string ToString(object value);

		// Token: 0x060032F3 RID: 13043
		ushort ToUInt16(object value);

		// Token: 0x060032F4 RID: 13044
		uint ToUInt32(object value);

		// Token: 0x060032F5 RID: 13045
		ulong ToUInt64(object value);
	}
}
