﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004F5 RID: 1269
	[ComVisible(true)]
	public interface ISerializationSurrogate
	{
		// Token: 0x060032F7 RID: 13047
		void GetObjectData(object obj, SerializationInfo info, StreamingContext context);

		// Token: 0x060032F8 RID: 13048
		object SetObjectData(object obj, SerializationInfo info, StreamingContext context, ISurrogateSelector selector);
	}
}
