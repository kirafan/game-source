﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x02000506 RID: 1286
	[ComVisible(true)]
	[Serializable]
	public abstract class SerializationBinder
	{
		// Token: 0x06003335 RID: 13109
		public abstract Type BindToType(string assemblyName, string typeName);
	}
}
