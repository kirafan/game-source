﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x02000508 RID: 1288
	[ComVisible(true)]
	public struct SerializationEntry
	{
		// Token: 0x06003343 RID: 13123 RVA: 0x000A6184 File Offset: 0x000A4384
		internal SerializationEntry(string name, Type type, object value)
		{
			this.name = name;
			this.objectType = type;
			this.value = value;
		}

		// Token: 0x170009A1 RID: 2465
		// (get) Token: 0x06003344 RID: 13124 RVA: 0x000A619C File Offset: 0x000A439C
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170009A2 RID: 2466
		// (get) Token: 0x06003345 RID: 13125 RVA: 0x000A61A4 File Offset: 0x000A43A4
		public Type ObjectType
		{
			get
			{
				return this.objectType;
			}
		}

		// Token: 0x170009A3 RID: 2467
		// (get) Token: 0x06003346 RID: 13126 RVA: 0x000A61AC File Offset: 0x000A43AC
		public object Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x0400155D RID: 5469
		private string name;

		// Token: 0x0400155E RID: 5470
		private Type objectType;

		// Token: 0x0400155F RID: 5471
		private object value;
	}
}
