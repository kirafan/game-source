﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x0200050A RID: 1290
	[ComVisible(true)]
	public sealed class SerializationInfo
	{
		// Token: 0x0600334B RID: 13131 RVA: 0x000A61E8 File Offset: 0x000A43E8
		private SerializationInfo(Type type)
		{
			this.assemblyName = type.Assembly.FullName;
			this.fullTypeName = type.FullName;
			this.converter = new FormatterConverter();
		}

		// Token: 0x0600334C RID: 13132 RVA: 0x000A623C File Offset: 0x000A443C
		private SerializationInfo(Type type, SerializationEntry[] data)
		{
			int num = data.Length;
			this.assemblyName = type.Assembly.FullName;
			this.fullTypeName = type.FullName;
			this.converter = new FormatterConverter();
			for (int i = 0; i < num; i++)
			{
				this.serialized.Add(data[i].Name, data[i]);
				this.values.Add(data[i]);
			}
		}

		// Token: 0x0600334D RID: 13133 RVA: 0x000A62E8 File Offset: 0x000A44E8
		[CLSCompliant(false)]
		public SerializationInfo(Type type, IFormatterConverter converter)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type", "Null argument");
			}
			if (converter == null)
			{
				throw new ArgumentNullException("converter", "Null argument");
			}
			this.converter = converter;
			this.assemblyName = type.Assembly.FullName;
			this.fullTypeName = type.FullName;
		}

		// Token: 0x170009A4 RID: 2468
		// (get) Token: 0x0600334E RID: 13134 RVA: 0x000A6364 File Offset: 0x000A4564
		// (set) Token: 0x0600334F RID: 13135 RVA: 0x000A636C File Offset: 0x000A456C
		public string AssemblyName
		{
			get
			{
				return this.assemblyName;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Argument is null.");
				}
				this.assemblyName = value;
			}
		}

		// Token: 0x170009A5 RID: 2469
		// (get) Token: 0x06003350 RID: 13136 RVA: 0x000A6388 File Offset: 0x000A4588
		// (set) Token: 0x06003351 RID: 13137 RVA: 0x000A6390 File Offset: 0x000A4590
		public string FullTypeName
		{
			get
			{
				return this.fullTypeName;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("Argument is null.");
				}
				this.fullTypeName = value;
			}
		}

		// Token: 0x170009A6 RID: 2470
		// (get) Token: 0x06003352 RID: 13138 RVA: 0x000A63AC File Offset: 0x000A45AC
		public int MemberCount
		{
			get
			{
				return this.serialized.Count;
			}
		}

		// Token: 0x06003353 RID: 13139 RVA: 0x000A63BC File Offset: 0x000A45BC
		public void AddValue(string name, object value, Type type)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name is null");
			}
			if (type == null)
			{
				throw new ArgumentNullException("type is null");
			}
			if (this.serialized.ContainsKey(name))
			{
				throw new SerializationException("Value has been serialized already.");
			}
			SerializationEntry serializationEntry = new SerializationEntry(name, type, value);
			this.serialized.Add(name, serializationEntry);
			this.values.Add(serializationEntry);
		}

		// Token: 0x06003354 RID: 13140 RVA: 0x000A6438 File Offset: 0x000A4638
		public object GetValue(string name, Type type)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name is null.");
			}
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (!this.serialized.ContainsKey(name))
			{
				throw new SerializationException("No element named " + name + " could be found.");
			}
			SerializationEntry serializationEntry = (SerializationEntry)this.serialized[name];
			if (serializationEntry.Value != null && !type.IsInstanceOfType(serializationEntry.Value))
			{
				return this.converter.Convert(serializationEntry.Value, type);
			}
			return serializationEntry.Value;
		}

		// Token: 0x06003355 RID: 13141 RVA: 0x000A64DC File Offset: 0x000A46DC
		public void SetType(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type is null.");
			}
			this.fullTypeName = type.FullName;
			this.assemblyName = type.Assembly.FullName;
		}

		// Token: 0x06003356 RID: 13142 RVA: 0x000A6518 File Offset: 0x000A4718
		public SerializationInfoEnumerator GetEnumerator()
		{
			return new SerializationInfoEnumerator(this.values);
		}

		// Token: 0x06003357 RID: 13143 RVA: 0x000A6528 File Offset: 0x000A4728
		public void AddValue(string name, short value)
		{
			this.AddValue(name, value, typeof(short));
		}

		// Token: 0x06003358 RID: 13144 RVA: 0x000A6544 File Offset: 0x000A4744
		[CLSCompliant(false)]
		public void AddValue(string name, ushort value)
		{
			this.AddValue(name, value, typeof(ushort));
		}

		// Token: 0x06003359 RID: 13145 RVA: 0x000A6560 File Offset: 0x000A4760
		public void AddValue(string name, int value)
		{
			this.AddValue(name, value, typeof(int));
		}

		// Token: 0x0600335A RID: 13146 RVA: 0x000A657C File Offset: 0x000A477C
		public void AddValue(string name, byte value)
		{
			this.AddValue(name, value, typeof(byte));
		}

		// Token: 0x0600335B RID: 13147 RVA: 0x000A6598 File Offset: 0x000A4798
		public void AddValue(string name, bool value)
		{
			this.AddValue(name, value, typeof(bool));
		}

		// Token: 0x0600335C RID: 13148 RVA: 0x000A65B4 File Offset: 0x000A47B4
		public void AddValue(string name, char value)
		{
			this.AddValue(name, value, typeof(char));
		}

		// Token: 0x0600335D RID: 13149 RVA: 0x000A65D0 File Offset: 0x000A47D0
		[CLSCompliant(false)]
		public void AddValue(string name, sbyte value)
		{
			this.AddValue(name, value, typeof(sbyte));
		}

		// Token: 0x0600335E RID: 13150 RVA: 0x000A65EC File Offset: 0x000A47EC
		public void AddValue(string name, double value)
		{
			this.AddValue(name, value, typeof(double));
		}

		// Token: 0x0600335F RID: 13151 RVA: 0x000A6608 File Offset: 0x000A4808
		public void AddValue(string name, decimal value)
		{
			this.AddValue(name, value, typeof(decimal));
		}

		// Token: 0x06003360 RID: 13152 RVA: 0x000A6624 File Offset: 0x000A4824
		public void AddValue(string name, DateTime value)
		{
			this.AddValue(name, value, typeof(DateTime));
		}

		// Token: 0x06003361 RID: 13153 RVA: 0x000A6640 File Offset: 0x000A4840
		public void AddValue(string name, float value)
		{
			this.AddValue(name, value, typeof(float));
		}

		// Token: 0x06003362 RID: 13154 RVA: 0x000A665C File Offset: 0x000A485C
		[CLSCompliant(false)]
		public void AddValue(string name, uint value)
		{
			this.AddValue(name, value, typeof(uint));
		}

		// Token: 0x06003363 RID: 13155 RVA: 0x000A6678 File Offset: 0x000A4878
		public void AddValue(string name, long value)
		{
			this.AddValue(name, value, typeof(long));
		}

		// Token: 0x06003364 RID: 13156 RVA: 0x000A6694 File Offset: 0x000A4894
		[CLSCompliant(false)]
		public void AddValue(string name, ulong value)
		{
			this.AddValue(name, value, typeof(ulong));
		}

		// Token: 0x06003365 RID: 13157 RVA: 0x000A66B0 File Offset: 0x000A48B0
		public void AddValue(string name, object value)
		{
			if (value == null)
			{
				this.AddValue(name, value, typeof(object));
			}
			else
			{
				this.AddValue(name, value, value.GetType());
			}
		}

		// Token: 0x06003366 RID: 13158 RVA: 0x000A66E8 File Offset: 0x000A48E8
		public bool GetBoolean(string name)
		{
			object value = this.GetValue(name, typeof(bool));
			return this.converter.ToBoolean(value);
		}

		// Token: 0x06003367 RID: 13159 RVA: 0x000A6714 File Offset: 0x000A4914
		public byte GetByte(string name)
		{
			object value = this.GetValue(name, typeof(byte));
			return this.converter.ToByte(value);
		}

		// Token: 0x06003368 RID: 13160 RVA: 0x000A6740 File Offset: 0x000A4940
		public char GetChar(string name)
		{
			object value = this.GetValue(name, typeof(char));
			return this.converter.ToChar(value);
		}

		// Token: 0x06003369 RID: 13161 RVA: 0x000A676C File Offset: 0x000A496C
		public DateTime GetDateTime(string name)
		{
			object value = this.GetValue(name, typeof(DateTime));
			return this.converter.ToDateTime(value);
		}

		// Token: 0x0600336A RID: 13162 RVA: 0x000A6798 File Offset: 0x000A4998
		public decimal GetDecimal(string name)
		{
			object value = this.GetValue(name, typeof(decimal));
			return this.converter.ToDecimal(value);
		}

		// Token: 0x0600336B RID: 13163 RVA: 0x000A67C4 File Offset: 0x000A49C4
		public double GetDouble(string name)
		{
			object value = this.GetValue(name, typeof(double));
			return this.converter.ToDouble(value);
		}

		// Token: 0x0600336C RID: 13164 RVA: 0x000A67F0 File Offset: 0x000A49F0
		public short GetInt16(string name)
		{
			object value = this.GetValue(name, typeof(short));
			return this.converter.ToInt16(value);
		}

		// Token: 0x0600336D RID: 13165 RVA: 0x000A681C File Offset: 0x000A4A1C
		public int GetInt32(string name)
		{
			object value = this.GetValue(name, typeof(int));
			return this.converter.ToInt32(value);
		}

		// Token: 0x0600336E RID: 13166 RVA: 0x000A6848 File Offset: 0x000A4A48
		public long GetInt64(string name)
		{
			object value = this.GetValue(name, typeof(long));
			return this.converter.ToInt64(value);
		}

		// Token: 0x0600336F RID: 13167 RVA: 0x000A6874 File Offset: 0x000A4A74
		[CLSCompliant(false)]
		public sbyte GetSByte(string name)
		{
			object value = this.GetValue(name, typeof(sbyte));
			return this.converter.ToSByte(value);
		}

		// Token: 0x06003370 RID: 13168 RVA: 0x000A68A0 File Offset: 0x000A4AA0
		public float GetSingle(string name)
		{
			object value = this.GetValue(name, typeof(float));
			return this.converter.ToSingle(value);
		}

		// Token: 0x06003371 RID: 13169 RVA: 0x000A68CC File Offset: 0x000A4ACC
		public string GetString(string name)
		{
			object value = this.GetValue(name, typeof(string));
			if (value == null)
			{
				return null;
			}
			return this.converter.ToString(value);
		}

		// Token: 0x06003372 RID: 13170 RVA: 0x000A6900 File Offset: 0x000A4B00
		[CLSCompliant(false)]
		public ushort GetUInt16(string name)
		{
			object value = this.GetValue(name, typeof(ushort));
			return this.converter.ToUInt16(value);
		}

		// Token: 0x06003373 RID: 13171 RVA: 0x000A692C File Offset: 0x000A4B2C
		[CLSCompliant(false)]
		public uint GetUInt32(string name)
		{
			object value = this.GetValue(name, typeof(uint));
			return this.converter.ToUInt32(value);
		}

		// Token: 0x06003374 RID: 13172 RVA: 0x000A6958 File Offset: 0x000A4B58
		[CLSCompliant(false)]
		public ulong GetUInt64(string name)
		{
			object value = this.GetValue(name, typeof(ulong));
			return this.converter.ToUInt64(value);
		}

		// Token: 0x06003375 RID: 13173 RVA: 0x000A6984 File Offset: 0x000A4B84
		private SerializationEntry[] get_entries()
		{
			SerializationEntry[] array = new SerializationEntry[this.MemberCount];
			int num = 0;
			foreach (SerializationEntry serializationEntry in this)
			{
				array[num++] = serializationEntry;
			}
			return array;
		}

		// Token: 0x04001560 RID: 5472
		private Hashtable serialized = new Hashtable();

		// Token: 0x04001561 RID: 5473
		private ArrayList values = new ArrayList();

		// Token: 0x04001562 RID: 5474
		private string assemblyName;

		// Token: 0x04001563 RID: 5475
		private string fullTypeName;

		// Token: 0x04001564 RID: 5476
		private IFormatterConverter converter;
	}
}
