﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020004FC RID: 1276
	internal class MultiArrayFixupRecord : BaseFixupRecord
	{
		// Token: 0x0600331A RID: 13082 RVA: 0x000A5850 File Offset: 0x000A3A50
		public MultiArrayFixupRecord(ObjectRecord objectToBeFixed, int[] indices, ObjectRecord objectRequired) : base(objectToBeFixed, objectRequired)
		{
			this._indices = indices;
		}

		// Token: 0x0600331B RID: 13083 RVA: 0x000A5864 File Offset: 0x000A3A64
		protected override void FixupImpl(ObjectManager manager)
		{
			this.ObjectToBeFixed.SetArrayValue(manager, this.ObjectRequired.ObjectInstance, this._indices);
		}

		// Token: 0x04001540 RID: 5440
		private int[] _indices;
	}
}
