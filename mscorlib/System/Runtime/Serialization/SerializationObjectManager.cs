﻿using System;
using System.Collections;

namespace System.Runtime.Serialization
{
	// Token: 0x0200050C RID: 1292
	public sealed class SerializationObjectManager
	{
		// Token: 0x0600337E RID: 13182 RVA: 0x000A6A7C File Offset: 0x000A4C7C
		public SerializationObjectManager(StreamingContext context)
		{
			this.context = context;
		}

		// Token: 0x14000017 RID: 23
		// (add) Token: 0x0600337F RID: 13183 RVA: 0x000A6A98 File Offset: 0x000A4C98
		// (remove) Token: 0x06003380 RID: 13184 RVA: 0x000A6AB4 File Offset: 0x000A4CB4
		private event SerializationCallbacks.CallbackHandler callbacks;

		// Token: 0x06003381 RID: 13185 RVA: 0x000A6AD0 File Offset: 0x000A4CD0
		public void RegisterObject(object obj)
		{
			if (this.seen.Contains(obj))
			{
				return;
			}
			SerializationCallbacks sc = SerializationCallbacks.GetSerializationCallbacks(obj.GetType());
			this.seen[obj] = 1;
			sc.RaiseOnSerializing(obj, this.context);
			if (sc.HasSerializedCallbacks)
			{
				this.callbacks = (SerializationCallbacks.CallbackHandler)Delegate.Combine(this.callbacks, new SerializationCallbacks.CallbackHandler(delegate(StreamingContext ctx)
				{
					sc.RaiseOnSerialized(obj, ctx);
				}));
			}
		}

		// Token: 0x06003382 RID: 13186 RVA: 0x000A6B78 File Offset: 0x000A4D78
		public void RaiseOnSerializedEvent()
		{
			if (this.callbacks != null)
			{
				this.callbacks(this.context);
			}
		}

		// Token: 0x04001566 RID: 5478
		private readonly StreamingContext context;

		// Token: 0x04001567 RID: 5479
		private readonly Hashtable seen = new Hashtable();
	}
}
