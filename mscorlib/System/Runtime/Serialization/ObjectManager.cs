﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004F9 RID: 1273
	[ComVisible(true)]
	public class ObjectManager
	{
		// Token: 0x06003304 RID: 13060 RVA: 0x000A4F98 File Offset: 0x000A3198
		public ObjectManager(ISurrogateSelector selector, StreamingContext context)
		{
			this._selector = selector;
			this._context = context;
		}

		// Token: 0x06003305 RID: 13061 RVA: 0x000A4FD0 File Offset: 0x000A31D0
		public virtual void DoFixups()
		{
			this._finalFixup = true;
			try
			{
				if (this._registeredObjectsCount < this._objectRecords.Count)
				{
					throw new SerializationException("There are some fixups that refer to objects that have not been registered");
				}
				ObjectRecord lastObjectRecord = this._lastObjectRecord;
				bool flag = true;
				ObjectRecord objectRecord2;
				for (ObjectRecord objectRecord = this._objectRecordChain; objectRecord != null; objectRecord = objectRecord2)
				{
					bool flag2 = !objectRecord.IsUnsolvedObjectReference || !flag;
					if (flag2)
					{
						flag2 = objectRecord.DoFixups(true, this, true);
					}
					if (flag2)
					{
						flag2 = objectRecord.LoadData(this, this._selector, this._context);
					}
					if (flag2)
					{
						if (objectRecord.OriginalObject is IDeserializationCallback)
						{
							this._deserializedRecords.Add(objectRecord);
						}
						SerializationCallbacks serializationCallbacks = SerializationCallbacks.GetSerializationCallbacks(objectRecord.OriginalObject.GetType());
						if (serializationCallbacks.HasDeserializedCallbacks)
						{
							this._onDeserializedCallbackRecords.Add(objectRecord);
						}
						objectRecord2 = objectRecord.Next;
					}
					else
					{
						if (objectRecord.ObjectInstance is IObjectReference && !flag)
						{
							if (objectRecord.Status == ObjectRecordStatus.ReferenceSolvingDelayed)
							{
								throw new SerializationException("The object with ID " + objectRecord.ObjectID + " could not be resolved");
							}
							objectRecord.Status = ObjectRecordStatus.ReferenceSolvingDelayed;
						}
						if (objectRecord != this._lastObjectRecord)
						{
							objectRecord2 = objectRecord.Next;
							objectRecord.Next = null;
							this._lastObjectRecord.Next = objectRecord;
							this._lastObjectRecord = objectRecord;
						}
						else
						{
							objectRecord2 = objectRecord;
						}
					}
					if (objectRecord == lastObjectRecord)
					{
						flag = false;
					}
				}
			}
			finally
			{
				this._finalFixup = false;
			}
		}

		// Token: 0x06003306 RID: 13062 RVA: 0x000A516C File Offset: 0x000A336C
		internal ObjectRecord GetObjectRecord(long objectID)
		{
			ObjectRecord objectRecord = (ObjectRecord)this._objectRecords[objectID];
			if (objectRecord == null)
			{
				if (this._finalFixup)
				{
					throw new SerializationException("The object with Id " + objectID + " has not been registered");
				}
				objectRecord = new ObjectRecord();
				objectRecord.ObjectID = objectID;
				this._objectRecords[objectID] = objectRecord;
			}
			if (!objectRecord.IsRegistered && this._finalFixup)
			{
				throw new SerializationException("The object with Id " + objectID + " has not been registered");
			}
			return objectRecord;
		}

		// Token: 0x06003307 RID: 13063 RVA: 0x000A5210 File Offset: 0x000A3410
		public virtual object GetObject(long objectID)
		{
			if (objectID <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectID", "The objectID parameter is less than or equal to zero");
			}
			ObjectRecord objectRecord = (ObjectRecord)this._objectRecords[objectID];
			if (objectRecord == null || !objectRecord.IsRegistered)
			{
				return null;
			}
			return objectRecord.ObjectInstance;
		}

		// Token: 0x06003308 RID: 13064 RVA: 0x000A5268 File Offset: 0x000A3468
		public virtual void RaiseDeserializationEvent()
		{
			for (int i = this._onDeserializedCallbackRecords.Count - 1; i >= 0; i--)
			{
				ObjectRecord objectRecord = (ObjectRecord)this._onDeserializedCallbackRecords[i];
				this.RaiseOnDeserializedEvent(objectRecord.OriginalObject);
			}
			for (int j = this._deserializedRecords.Count - 1; j >= 0; j--)
			{
				ObjectRecord objectRecord2 = (ObjectRecord)this._deserializedRecords[j];
				IDeserializationCallback deserializationCallback = objectRecord2.OriginalObject as IDeserializationCallback;
				if (deserializationCallback != null)
				{
					deserializationCallback.OnDeserialization(this);
				}
			}
		}

		// Token: 0x06003309 RID: 13065 RVA: 0x000A5300 File Offset: 0x000A3500
		public void RaiseOnDeserializingEvent(object obj)
		{
			SerializationCallbacks serializationCallbacks = SerializationCallbacks.GetSerializationCallbacks(obj.GetType());
			serializationCallbacks.RaiseOnDeserializing(obj, this._context);
		}

		// Token: 0x0600330A RID: 13066 RVA: 0x000A5328 File Offset: 0x000A3528
		private void RaiseOnDeserializedEvent(object obj)
		{
			SerializationCallbacks serializationCallbacks = SerializationCallbacks.GetSerializationCallbacks(obj.GetType());
			serializationCallbacks.RaiseOnDeserialized(obj, this._context);
		}

		// Token: 0x0600330B RID: 13067 RVA: 0x000A5350 File Offset: 0x000A3550
		private void AddFixup(BaseFixupRecord record)
		{
			record.ObjectToBeFixed.ChainFixup(record, true);
			record.ObjectRequired.ChainFixup(record, false);
		}

		// Token: 0x0600330C RID: 13068 RVA: 0x000A536C File Offset: 0x000A356C
		public virtual void RecordArrayElementFixup(long arrayToBeFixed, int index, long objectRequired)
		{
			if (arrayToBeFixed <= 0L)
			{
				throw new ArgumentOutOfRangeException("arrayToBeFixed", "The arrayToBeFixed parameter is less than or equal to zero");
			}
			if (objectRequired <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectRequired", "The objectRequired parameter is less than or equal to zero");
			}
			ArrayFixupRecord record = new ArrayFixupRecord(this.GetObjectRecord(arrayToBeFixed), index, this.GetObjectRecord(objectRequired));
			this.AddFixup(record);
		}

		// Token: 0x0600330D RID: 13069 RVA: 0x000A53C8 File Offset: 0x000A35C8
		public virtual void RecordArrayElementFixup(long arrayToBeFixed, int[] indices, long objectRequired)
		{
			if (arrayToBeFixed <= 0L)
			{
				throw new ArgumentOutOfRangeException("arrayToBeFixed", "The arrayToBeFixed parameter is less than or equal to zero");
			}
			if (objectRequired <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectRequired", "The objectRequired parameter is less than or equal to zero");
			}
			if (indices == null)
			{
				throw new ArgumentNullException("indices");
			}
			MultiArrayFixupRecord record = new MultiArrayFixupRecord(this.GetObjectRecord(arrayToBeFixed), indices, this.GetObjectRecord(objectRequired));
			this.AddFixup(record);
		}

		// Token: 0x0600330E RID: 13070 RVA: 0x000A5434 File Offset: 0x000A3634
		public virtual void RecordDelayedFixup(long objectToBeFixed, string memberName, long objectRequired)
		{
			if (objectToBeFixed <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectToBeFixed", "The objectToBeFixed parameter is less than or equal to zero");
			}
			if (objectRequired <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectRequired", "The objectRequired parameter is less than or equal to zero");
			}
			if (memberName == null)
			{
				throw new ArgumentNullException("memberName");
			}
			DelayedFixupRecord record = new DelayedFixupRecord(this.GetObjectRecord(objectToBeFixed), memberName, this.GetObjectRecord(objectRequired));
			this.AddFixup(record);
		}

		// Token: 0x0600330F RID: 13071 RVA: 0x000A54A0 File Offset: 0x000A36A0
		public virtual void RecordFixup(long objectToBeFixed, MemberInfo member, long objectRequired)
		{
			if (objectToBeFixed <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectToBeFixed", "The objectToBeFixed parameter is less than or equal to zero");
			}
			if (objectRequired <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectRequired", "The objectRequired parameter is less than or equal to zero");
			}
			if (member == null)
			{
				throw new ArgumentNullException("member");
			}
			FixupRecord record = new FixupRecord(this.GetObjectRecord(objectToBeFixed), member, this.GetObjectRecord(objectRequired));
			this.AddFixup(record);
		}

		// Token: 0x06003310 RID: 13072 RVA: 0x000A550C File Offset: 0x000A370C
		private void RegisterObjectInternal(object obj, ObjectRecord record)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			if (!record.IsRegistered)
			{
				record.ObjectInstance = obj;
				record.OriginalObject = obj;
				if (obj is IObjectReference)
				{
					record.Status = ObjectRecordStatus.ReferenceUnsolved;
				}
				else
				{
					record.Status = ObjectRecordStatus.ReferenceSolved;
				}
				if (this._selector != null)
				{
					record.Surrogate = this._selector.GetSurrogate(obj.GetType(), this._context, out record.SurrogateSelector);
					if (record.Surrogate != null)
					{
						record.Status = ObjectRecordStatus.ReferenceUnsolved;
					}
				}
				record.DoFixups(true, this, false);
				record.DoFixups(false, this, false);
				this._registeredObjectsCount++;
				if (this._objectRecordChain == null)
				{
					this._objectRecordChain = record;
					this._lastObjectRecord = record;
				}
				else
				{
					this._lastObjectRecord.Next = record;
					this._lastObjectRecord = record;
				}
				return;
			}
			if (record.OriginalObject != obj)
			{
				throw new SerializationException("An object with Id " + record.ObjectID + " has already been registered");
			}
		}

		// Token: 0x06003311 RID: 13073 RVA: 0x000A5624 File Offset: 0x000A3824
		public virtual void RegisterObject(object obj, long objectID)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj", "The obj parameter is null.");
			}
			if (objectID <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectID", "The objectID parameter is less than or equal to zero");
			}
			this.RegisterObjectInternal(obj, this.GetObjectRecord(objectID));
		}

		// Token: 0x06003312 RID: 13074 RVA: 0x000A5670 File Offset: 0x000A3870
		public void RegisterObject(object obj, long objectID, SerializationInfo info)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj", "The obj parameter is null.");
			}
			if (objectID <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectID", "The objectID parameter is less than or equal to zero");
			}
			ObjectRecord objectRecord = this.GetObjectRecord(objectID);
			objectRecord.Info = info;
			this.RegisterObjectInternal(obj, objectRecord);
		}

		// Token: 0x06003313 RID: 13075 RVA: 0x000A56C4 File Offset: 0x000A38C4
		public void RegisterObject(object obj, long objectID, SerializationInfo info, long idOfContainingObj, MemberInfo member)
		{
			this.RegisterObject(obj, objectID, info, idOfContainingObj, member, null);
		}

		// Token: 0x06003314 RID: 13076 RVA: 0x000A56D4 File Offset: 0x000A38D4
		public void RegisterObject(object obj, long objectID, SerializationInfo info, long idOfContainingObj, MemberInfo member, int[] arrayIndex)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj", "The obj parameter is null.");
			}
			if (objectID <= 0L)
			{
				throw new ArgumentOutOfRangeException("objectID", "The objectID parameter is less than or equal to zero");
			}
			ObjectRecord objectRecord = this.GetObjectRecord(objectID);
			objectRecord.Info = info;
			objectRecord.IdOfContainingObj = idOfContainingObj;
			objectRecord.Member = member;
			objectRecord.ArrayIndex = arrayIndex;
			this.RegisterObjectInternal(obj, objectRecord);
		}

		// Token: 0x04001532 RID: 5426
		private ObjectRecord _objectRecordChain;

		// Token: 0x04001533 RID: 5427
		private ObjectRecord _lastObjectRecord;

		// Token: 0x04001534 RID: 5428
		private ArrayList _deserializedRecords = new ArrayList();

		// Token: 0x04001535 RID: 5429
		private ArrayList _onDeserializedCallbackRecords = new ArrayList();

		// Token: 0x04001536 RID: 5430
		private Hashtable _objectRecords = new Hashtable();

		// Token: 0x04001537 RID: 5431
		private bool _finalFixup;

		// Token: 0x04001538 RID: 5432
		private ISurrogateSelector _selector;

		// Token: 0x04001539 RID: 5433
		private StreamingContext _context;

		// Token: 0x0400153A RID: 5434
		private int _registeredObjectsCount;
	}
}
