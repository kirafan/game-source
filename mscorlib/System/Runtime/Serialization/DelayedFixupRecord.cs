﻿using System;

namespace System.Runtime.Serialization
{
	// Token: 0x020004FE RID: 1278
	internal class DelayedFixupRecord : BaseFixupRecord
	{
		// Token: 0x0600331E RID: 13086 RVA: 0x000A58B8 File Offset: 0x000A3AB8
		public DelayedFixupRecord(ObjectRecord objectToBeFixed, string memberName, ObjectRecord objectRequired) : base(objectToBeFixed, objectRequired)
		{
			this._memberName = memberName;
		}

		// Token: 0x0600331F RID: 13087 RVA: 0x000A58CC File Offset: 0x000A3ACC
		protected override void FixupImpl(ObjectManager manager)
		{
			this.ObjectToBeFixed.SetMemberValue(manager, this._memberName, this.ObjectRequired.ObjectInstance);
		}

		// Token: 0x04001542 RID: 5442
		public string _memberName;
	}
}
