﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x02000024 RID: 36
	[ComVisible(true)]
	public interface ISerializable
	{
		// Token: 0x06000372 RID: 882
		void GetObjectData(SerializationInfo info, StreamingContext context);
	}
}
