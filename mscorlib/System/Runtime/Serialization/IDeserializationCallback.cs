﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004F1 RID: 1265
	[ComVisible(true)]
	public interface IDeserializationCallback
	{
		// Token: 0x060032DC RID: 13020
		void OnDeserialization(object sender);
	}
}
