﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004F7 RID: 1271
	[MonoTODO("Serialization format not compatible with.NET")]
	[ComVisible(true)]
	[Serializable]
	public class ObjectIDGenerator
	{
		// Token: 0x060032FC RID: 13052 RVA: 0x000A4E5C File Offset: 0x000A305C
		public ObjectIDGenerator()
		{
			this.table = new Hashtable(ObjectIDGenerator.comparer, ObjectIDGenerator.comparer);
			this.current = 1L;
		}

		// Token: 0x060032FE RID: 13054 RVA: 0x000A4E90 File Offset: 0x000A3090
		public virtual long GetId(object obj, out bool firstTime)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			object obj2 = this.table[obj];
			if (obj2 != null)
			{
				firstTime = false;
				return (long)obj2;
			}
			firstTime = true;
			this.table.Add(obj, this.current);
			long result;
			this.current = (result = this.current) + 1L;
			return result;
		}

		// Token: 0x060032FF RID: 13055 RVA: 0x000A4EF8 File Offset: 0x000A30F8
		public virtual long HasId(object obj, out bool firstTime)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			object obj2 = this.table[obj];
			if (obj2 != null)
			{
				firstTime = false;
				return (long)obj2;
			}
			firstTime = true;
			return 0L;
		}

		// Token: 0x17000997 RID: 2455
		// (get) Token: 0x06003300 RID: 13056 RVA: 0x000A4F38 File Offset: 0x000A3138
		internal long NextId
		{
			get
			{
				long result;
				this.current = (result = this.current) + 1L;
				return result;
			}
		}

		// Token: 0x0400152F RID: 5423
		private Hashtable table;

		// Token: 0x04001530 RID: 5424
		private long current;

		// Token: 0x04001531 RID: 5425
		private static ObjectIDGenerator.InstanceComparer comparer = new ObjectIDGenerator.InstanceComparer();

		// Token: 0x020004F8 RID: 1272
		private class InstanceComparer : IComparer, IHashCodeProvider
		{
			// Token: 0x06003302 RID: 13058 RVA: 0x000A4F60 File Offset: 0x000A3160
			int IComparer.Compare(object o1, object o2)
			{
				if (o1 is string)
				{
					return (!o1.Equals(o2)) ? 1 : 0;
				}
				return (o1 != o2) ? 1 : 0;
			}

			// Token: 0x06003303 RID: 13059 RVA: 0x000A4F90 File Offset: 0x000A3190
			int IHashCodeProvider.GetHashCode(object o)
			{
				return object.InternalGetHashCode(o);
			}
		}
	}
}
