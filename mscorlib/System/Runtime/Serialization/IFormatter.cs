﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Runtime.Serialization
{
	// Token: 0x020004F2 RID: 1266
	[ComVisible(true)]
	public interface IFormatter
	{
		// Token: 0x17000994 RID: 2452
		// (get) Token: 0x060032DD RID: 13021
		// (set) Token: 0x060032DE RID: 13022
		SerializationBinder Binder { get; set; }

		// Token: 0x17000995 RID: 2453
		// (get) Token: 0x060032DF RID: 13023
		// (set) Token: 0x060032E0 RID: 13024
		StreamingContext Context { get; set; }

		// Token: 0x17000996 RID: 2454
		// (get) Token: 0x060032E1 RID: 13025
		// (set) Token: 0x060032E2 RID: 13026
		ISurrogateSelector SurrogateSelector { get; set; }

		// Token: 0x060032E3 RID: 13027
		object Deserialize(Stream serializationStream);

		// Token: 0x060032E4 RID: 13028
		void Serialize(Stream serializationStream, object graph);
	}
}
