﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000452 RID: 1106
	[ComVisible(true)]
	public interface IChannelReceiverHook
	{
		// Token: 0x1700084E RID: 2126
		// (get) Token: 0x06002E74 RID: 11892
		string ChannelScheme { get; }

		// Token: 0x1700084F RID: 2127
		// (get) Token: 0x06002E75 RID: 11893
		IServerChannelSink ChannelSinkChain { get; }

		// Token: 0x17000850 RID: 2128
		// (get) Token: 0x06002E76 RID: 11894
		bool WantsToListen { get; }

		// Token: 0x06002E77 RID: 11895
		void AddHookChannelUri(string channelUri);
	}
}
