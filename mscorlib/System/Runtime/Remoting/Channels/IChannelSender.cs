﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000453 RID: 1107
	[ComVisible(true)]
	public interface IChannelSender : IChannel
	{
		// Token: 0x06002E78 RID: 11896
		IMessageSink CreateMessageSink(string url, object remoteChannelData, out string objectURI);
	}
}
