﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x0200045C RID: 1116
	[ComVisible(true)]
	public interface IServerResponseChannelSinkStack
	{
		// Token: 0x06002E89 RID: 11913
		void AsyncProcessResponse(IMessage msg, ITransportHeaders headers, Stream stream);

		// Token: 0x06002E8A RID: 11914
		Stream GetResponseStream(IMessage msg, ITransportHeaders headers);
	}
}
