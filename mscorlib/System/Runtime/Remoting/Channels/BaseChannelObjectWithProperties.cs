﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000446 RID: 1094
	[ComVisible(true)]
	public abstract class BaseChannelObjectWithProperties : IEnumerable, ICollection, IDictionary
	{
		// Token: 0x06002E2A RID: 11818 RVA: 0x000999E4 File Offset: 0x00097BE4
		protected BaseChannelObjectWithProperties()
		{
			this.table = new Hashtable();
		}

		// Token: 0x06002E2B RID: 11819 RVA: 0x000999F8 File Offset: 0x00097BF8
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.table.GetEnumerator();
		}

		// Token: 0x17000839 RID: 2105
		// (get) Token: 0x06002E2C RID: 11820 RVA: 0x00099A08 File Offset: 0x00097C08
		public virtual int Count
		{
			get
			{
				return this.table.Count;
			}
		}

		// Token: 0x1700083A RID: 2106
		// (get) Token: 0x06002E2D RID: 11821 RVA: 0x00099A18 File Offset: 0x00097C18
		public virtual bool IsFixedSize
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700083B RID: 2107
		// (get) Token: 0x06002E2E RID: 11822 RVA: 0x00099A1C File Offset: 0x00097C1C
		public virtual bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700083C RID: 2108
		// (get) Token: 0x06002E2F RID: 11823 RVA: 0x00099A20 File Offset: 0x00097C20
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700083D RID: 2109
		public virtual object this[object key]
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700083E RID: 2110
		// (get) Token: 0x06002E32 RID: 11826 RVA: 0x00099A34 File Offset: 0x00097C34
		public virtual ICollection Keys
		{
			get
			{
				return this.table.Keys;
			}
		}

		// Token: 0x1700083F RID: 2111
		// (get) Token: 0x06002E33 RID: 11827 RVA: 0x00099A44 File Offset: 0x00097C44
		public virtual IDictionary Properties
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000840 RID: 2112
		// (get) Token: 0x06002E34 RID: 11828 RVA: 0x00099A48 File Offset: 0x00097C48
		public virtual object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000841 RID: 2113
		// (get) Token: 0x06002E35 RID: 11829 RVA: 0x00099A4C File Offset: 0x00097C4C
		public virtual ICollection Values
		{
			get
			{
				return this.table.Values;
			}
		}

		// Token: 0x06002E36 RID: 11830 RVA: 0x00099A5C File Offset: 0x00097C5C
		public virtual void Add(object key, object value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002E37 RID: 11831 RVA: 0x00099A64 File Offset: 0x00097C64
		public virtual void Clear()
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002E38 RID: 11832 RVA: 0x00099A6C File Offset: 0x00097C6C
		public virtual bool Contains(object key)
		{
			return this.table.Contains(key);
		}

		// Token: 0x06002E39 RID: 11833 RVA: 0x00099A7C File Offset: 0x00097C7C
		public virtual void CopyTo(Array array, int index)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002E3A RID: 11834 RVA: 0x00099A84 File Offset: 0x00097C84
		public virtual IDictionaryEnumerator GetEnumerator()
		{
			return this.table.GetEnumerator();
		}

		// Token: 0x06002E3B RID: 11835 RVA: 0x00099A94 File Offset: 0x00097C94
		public virtual void Remove(object key)
		{
			throw new NotSupportedException();
		}

		// Token: 0x040013CD RID: 5069
		private Hashtable table;
	}
}
