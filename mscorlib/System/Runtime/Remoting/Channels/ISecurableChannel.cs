﻿using System;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x0200045B RID: 1115
	public interface ISecurableChannel
	{
		// Token: 0x17000854 RID: 2132
		// (get) Token: 0x06002E87 RID: 11911
		// (set) Token: 0x06002E88 RID: 11912
		bool IsSecured { get; set; }
	}
}
