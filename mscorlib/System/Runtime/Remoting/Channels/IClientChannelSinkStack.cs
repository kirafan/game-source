﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000457 RID: 1111
	[ComVisible(true)]
	public interface IClientChannelSinkStack : IClientResponseChannelSinkStack
	{
		// Token: 0x06002E82 RID: 11906
		object Pop(IClientChannelSink sink);

		// Token: 0x06002E83 RID: 11907
		void Push(IClientChannelSink sink, object state);
	}
}
