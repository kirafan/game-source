﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000456 RID: 1110
	[ComVisible(true)]
	public interface IClientChannelSinkProvider
	{
		// Token: 0x17000853 RID: 2131
		// (get) Token: 0x06002E7F RID: 11903
		// (set) Token: 0x06002E80 RID: 11904
		IClientChannelSinkProvider Next { get; set; }

		// Token: 0x06002E81 RID: 11905
		IClientChannelSink CreateSink(IChannelSender channel, string url, object remoteChannelData);
	}
}
