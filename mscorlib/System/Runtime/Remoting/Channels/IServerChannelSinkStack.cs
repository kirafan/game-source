﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000461 RID: 1121
	[ComVisible(true)]
	public interface IServerChannelSinkStack : IServerResponseChannelSinkStack
	{
		// Token: 0x06002E9F RID: 11935
		object Pop(IServerChannelSink sink);

		// Token: 0x06002EA0 RID: 11936
		void Push(IServerChannelSink sink, object state);

		// Token: 0x06002EA1 RID: 11937
		void ServerCallback(IAsyncResult ar);

		// Token: 0x06002EA2 RID: 11938
		void Store(IServerChannelSink sink, object state);

		// Token: 0x06002EA3 RID: 11939
		void StoreAndDispatch(IServerChannelSink sink, object state);
	}
}
