﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x0200045A RID: 1114
	[ComVisible(true)]
	public interface IClientResponseChannelSinkStack
	{
		// Token: 0x06002E84 RID: 11908
		void AsyncProcessResponse(ITransportHeaders headers, Stream stream);

		// Token: 0x06002E85 RID: 11909
		void DispatchException(Exception e);

		// Token: 0x06002E86 RID: 11910
		void DispatchReplyMessage(IMessage msg);
	}
}
