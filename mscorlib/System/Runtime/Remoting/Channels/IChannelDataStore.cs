﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000450 RID: 1104
	[ComVisible(true)]
	public interface IChannelDataStore
	{
		// Token: 0x1700084B RID: 2123
		// (get) Token: 0x06002E6D RID: 11885
		string[] ChannelUris { get; }

		// Token: 0x1700084C RID: 2124
		object this[object key]
		{
			get;
			set;
		}
	}
}
