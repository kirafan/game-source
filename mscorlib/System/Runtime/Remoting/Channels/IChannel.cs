﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x0200044F RID: 1103
	[ComVisible(true)]
	public interface IChannel
	{
		// Token: 0x17000849 RID: 2121
		// (get) Token: 0x06002E6A RID: 11882
		string ChannelName { get; }

		// Token: 0x1700084A RID: 2122
		// (get) Token: 0x06002E6B RID: 11883
		int ChannelPriority { get; }

		// Token: 0x06002E6C RID: 11884
		string Parse(string url, out string objectURI);
	}
}
