﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000448 RID: 1096
	[ComVisible(true)]
	public abstract class BaseChannelWithProperties : BaseChannelObjectWithProperties
	{
		// Token: 0x17000842 RID: 2114
		// (get) Token: 0x06002E3E RID: 11838 RVA: 0x00099AAC File Offset: 0x00097CAC
		public override IDictionary Properties
		{
			get
			{
				if (this.SinksWithProperties == null || this.SinksWithProperties.Properties == null)
				{
					return base.Properties;
				}
				IDictionary[] dics = new IDictionary[]
				{
					base.Properties,
					this.SinksWithProperties.Properties
				};
				return new AggregateDictionary(dics);
			}
		}

		// Token: 0x040013CE RID: 5070
		protected IChannelSinkBase SinksWithProperties;
	}
}
