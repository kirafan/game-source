﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000449 RID: 1097
	[ComVisible(true)]
	[Serializable]
	public class ChannelDataStore : IChannelDataStore
	{
		// Token: 0x06002E3F RID: 11839 RVA: 0x00099B00 File Offset: 0x00097D00
		public ChannelDataStore(string[] channelURIs)
		{
			this._channelURIs = channelURIs;
		}

		// Token: 0x17000843 RID: 2115
		// (get) Token: 0x06002E40 RID: 11840 RVA: 0x00099B10 File Offset: 0x00097D10
		// (set) Token: 0x06002E41 RID: 11841 RVA: 0x00099B18 File Offset: 0x00097D18
		public string[] ChannelUris
		{
			get
			{
				return this._channelURIs;
			}
			set
			{
				this._channelURIs = value;
			}
		}

		// Token: 0x17000844 RID: 2116
		public object this[object key]
		{
			get
			{
				if (this._extraData == null)
				{
					return null;
				}
				foreach (DictionaryEntry dictionaryEntry in this._extraData)
				{
					if (dictionaryEntry.Key.Equals(key))
					{
						return dictionaryEntry.Value;
					}
				}
				return null;
			}
			set
			{
				if (this._extraData == null)
				{
					this._extraData = new DictionaryEntry[]
					{
						new DictionaryEntry(key, value)
					};
				}
				else
				{
					DictionaryEntry[] array = new DictionaryEntry[this._extraData.Length + 1];
					this._extraData.CopyTo(array, 0);
					array[this._extraData.Length] = new DictionaryEntry(key, value);
					this._extraData = array;
				}
			}
		}

		// Token: 0x040013CF RID: 5071
		private string[] _channelURIs;

		// Token: 0x040013D0 RID: 5072
		private DictionaryEntry[] _extraData;
	}
}
