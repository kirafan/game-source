﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000463 RID: 1123
	[ComVisible(true)]
	public interface ITransportHeaders
	{
		// Token: 0x1700085A RID: 2138
		object this[object key]
		{
			get;
			set;
		}

		// Token: 0x06002EA6 RID: 11942
		IEnumerator GetEnumerator();
	}
}
