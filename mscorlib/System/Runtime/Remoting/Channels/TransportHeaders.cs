﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000467 RID: 1127
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with .NET")]
	[Serializable]
	public class TransportHeaders : ITransportHeaders
	{
		// Token: 0x06002EB3 RID: 11955 RVA: 0x0009ACF4 File Offset: 0x00098EF4
		public TransportHeaders()
		{
			this.hash_table = new Hashtable(CaseInsensitiveHashCodeProvider.DefaultInvariant, CaseInsensitiveComparer.DefaultInvariant);
		}

		// Token: 0x1700085E RID: 2142
		public object this[object key]
		{
			get
			{
				return this.hash_table[key];
			}
			set
			{
				this.hash_table[key] = value;
			}
		}

		// Token: 0x06002EB6 RID: 11958 RVA: 0x0009AD34 File Offset: 0x00098F34
		public IEnumerator GetEnumerator()
		{
			return this.hash_table.GetEnumerator();
		}

		// Token: 0x040013E6 RID: 5094
		private Hashtable hash_table;
	}
}
