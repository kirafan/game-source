﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x0200044E RID: 1102
	[ComVisible(true)]
	public class ClientChannelSinkStack : IClientChannelSinkStack, IClientResponseChannelSinkStack
	{
		// Token: 0x06002E63 RID: 11875 RVA: 0x0009AA38 File Offset: 0x00098C38
		public ClientChannelSinkStack()
		{
		}

		// Token: 0x06002E64 RID: 11876 RVA: 0x0009AA40 File Offset: 0x00098C40
		public ClientChannelSinkStack(IMessageSink replySink)
		{
			this._replySink = replySink;
		}

		// Token: 0x06002E65 RID: 11877 RVA: 0x0009AA50 File Offset: 0x00098C50
		public void AsyncProcessResponse(ITransportHeaders headers, Stream stream)
		{
			if (this._sinkStack == null)
			{
				throw new RemotingException("The current sink stack is empty");
			}
			ChanelSinkStackEntry sinkStack = this._sinkStack;
			this._sinkStack = this._sinkStack.Next;
			((IClientChannelSink)sinkStack.Sink).AsyncProcessResponse(this, sinkStack.State, headers, stream);
		}

		// Token: 0x06002E66 RID: 11878 RVA: 0x0009AAA4 File Offset: 0x00098CA4
		public void DispatchException(Exception e)
		{
			this.DispatchReplyMessage(new ReturnMessage(e, null));
		}

		// Token: 0x06002E67 RID: 11879 RVA: 0x0009AAB4 File Offset: 0x00098CB4
		public void DispatchReplyMessage(IMessage msg)
		{
			if (this._replySink != null)
			{
				this._replySink.SyncProcessMessage(msg);
			}
		}

		// Token: 0x06002E68 RID: 11880 RVA: 0x0009AAD0 File Offset: 0x00098CD0
		public object Pop(IClientChannelSink sink)
		{
			while (this._sinkStack != null)
			{
				ChanelSinkStackEntry sinkStack = this._sinkStack;
				this._sinkStack = this._sinkStack.Next;
				if (sinkStack.Sink == sink)
				{
					return sinkStack.State;
				}
			}
			throw new RemotingException("The current sink stack is empty, or the specified sink was never pushed onto the current stack");
		}

		// Token: 0x06002E69 RID: 11881 RVA: 0x0009AB24 File Offset: 0x00098D24
		public void Push(IClientChannelSink sink, object state)
		{
			this._sinkStack = new ChanelSinkStackEntry(sink, state, this._sinkStack);
		}

		// Token: 0x040013DC RID: 5084
		private IMessageSink _replySink;

		// Token: 0x040013DD RID: 5085
		private ChanelSinkStackEntry _sinkStack;
	}
}
