﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000455 RID: 1109
	[ComVisible(true)]
	public interface IClientChannelSink : IChannelSinkBase
	{
		// Token: 0x17000852 RID: 2130
		// (get) Token: 0x06002E7A RID: 11898
		IClientChannelSink NextChannelSink { get; }

		// Token: 0x06002E7B RID: 11899
		void AsyncProcessRequest(IClientChannelSinkStack sinkStack, IMessage msg, ITransportHeaders headers, Stream stream);

		// Token: 0x06002E7C RID: 11900
		void AsyncProcessResponse(IClientResponseChannelSinkStack sinkStack, object state, ITransportHeaders headers, Stream stream);

		// Token: 0x06002E7D RID: 11901
		Stream GetRequestStream(IMessage msg, ITransportHeaders headers);

		// Token: 0x06002E7E RID: 11902
		void ProcessMessage(IMessage msg, ITransportHeaders requestHeaders, Stream requestStream, out ITransportHeaders responseHeaders, out Stream responseStream);
	}
}
