﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000464 RID: 1124
	[ComVisible(true)]
	public class ServerChannelSinkStack : IServerResponseChannelSinkStack, IServerChannelSinkStack
	{
		// Token: 0x06002EA8 RID: 11944 RVA: 0x0009AB9C File Offset: 0x00098D9C
		public Stream GetResponseStream(IMessage msg, ITransportHeaders headers)
		{
			if (this._sinkStack == null)
			{
				throw new RemotingException("The sink stack is empty");
			}
			return ((IServerChannelSink)this._sinkStack.Sink).GetResponseStream(this, this._sinkStack.State, msg, headers);
		}

		// Token: 0x06002EA9 RID: 11945 RVA: 0x0009ABD8 File Offset: 0x00098DD8
		public object Pop(IServerChannelSink sink)
		{
			while (this._sinkStack != null)
			{
				ChanelSinkStackEntry sinkStack = this._sinkStack;
				this._sinkStack = this._sinkStack.Next;
				if (sinkStack.Sink == sink)
				{
					return sinkStack.State;
				}
			}
			throw new RemotingException("The current sink stack is empty, or the specified sink was never pushed onto the current stack");
		}

		// Token: 0x06002EAA RID: 11946 RVA: 0x0009AC2C File Offset: 0x00098E2C
		public void Push(IServerChannelSink sink, object state)
		{
			this._sinkStack = new ChanelSinkStackEntry(sink, state, this._sinkStack);
		}

		// Token: 0x06002EAB RID: 11947 RVA: 0x0009AC44 File Offset: 0x00098E44
		[MonoTODO]
		public void ServerCallback(IAsyncResult ar)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002EAC RID: 11948 RVA: 0x0009AC4C File Offset: 0x00098E4C
		[MonoTODO]
		public void Store(IServerChannelSink sink, object state)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002EAD RID: 11949 RVA: 0x0009AC54 File Offset: 0x00098E54
		[MonoTODO]
		public void StoreAndDispatch(IServerChannelSink sink, object state)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002EAE RID: 11950 RVA: 0x0009AC5C File Offset: 0x00098E5C
		public void AsyncProcessResponse(IMessage msg, ITransportHeaders headers, Stream stream)
		{
			if (this._sinkStack == null)
			{
				throw new RemotingException("The current sink stack is empty");
			}
			ChanelSinkStackEntry sinkStack = this._sinkStack;
			this._sinkStack = this._sinkStack.Next;
			((IServerChannelSink)sinkStack.Sink).AsyncProcessResponse(this, sinkStack.State, msg, headers, stream);
		}

		// Token: 0x040013DE RID: 5086
		private ChanelSinkStackEntry _sinkStack;
	}
}
