﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000454 RID: 1108
	[ComVisible(true)]
	public interface IChannelSinkBase
	{
		// Token: 0x17000851 RID: 2129
		// (get) Token: 0x06002E79 RID: 11897
		IDictionary Properties { get; }
	}
}
