﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000459 RID: 1113
	[ComVisible(true)]
	public interface IClientFormatterSinkProvider : IClientChannelSinkProvider
	{
	}
}
