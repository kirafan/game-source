﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x0200046D RID: 1133
	internal class AsyncRequest
	{
		// Token: 0x06002ED3 RID: 11987 RVA: 0x0009B240 File Offset: 0x00099440
		public AsyncRequest(IMessage msgRequest, IMessageSink replySink)
		{
			this.ReplySink = replySink;
			this.MsgRequest = msgRequest;
		}

		// Token: 0x040013F2 RID: 5106
		internal IMessageSink ReplySink;

		// Token: 0x040013F3 RID: 5107
		internal IMessage MsgRequest;
	}
}
