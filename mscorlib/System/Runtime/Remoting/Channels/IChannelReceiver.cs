﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000451 RID: 1105
	[ComVisible(true)]
	public interface IChannelReceiver : IChannel
	{
		// Token: 0x1700084D RID: 2125
		// (get) Token: 0x06002E70 RID: 11888
		object ChannelData { get; }

		// Token: 0x06002E71 RID: 11889
		string[] GetUrlsForUri(string objectURI);

		// Token: 0x06002E72 RID: 11890
		void StartListening(object data);

		// Token: 0x06002E73 RID: 11891
		void StopListening(object data);
	}
}
