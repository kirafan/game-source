﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000460 RID: 1120
	[ComVisible(true)]
	public interface IServerChannelSinkProvider
	{
		// Token: 0x17000859 RID: 2137
		// (get) Token: 0x06002E9B RID: 11931
		// (set) Token: 0x06002E9C RID: 11932
		IServerChannelSinkProvider Next { get; set; }

		// Token: 0x06002E9D RID: 11933
		IServerChannelSink CreateSink(IChannelReceiver channel);

		// Token: 0x06002E9E RID: 11934
		void GetChannelData(IChannelDataStore channelData);
	}
}
