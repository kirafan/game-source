﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000466 RID: 1126
	[ComVisible(true)]
	public class SinkProviderData
	{
		// Token: 0x06002EAF RID: 11951 RVA: 0x0009ACB4 File Offset: 0x00098EB4
		public SinkProviderData(string name)
		{
			this.sinkName = name;
			this.children = new ArrayList();
			this.properties = new Hashtable();
		}

		// Token: 0x1700085B RID: 2139
		// (get) Token: 0x06002EB0 RID: 11952 RVA: 0x0009ACDC File Offset: 0x00098EDC
		public IList Children
		{
			get
			{
				return this.children;
			}
		}

		// Token: 0x1700085C RID: 2140
		// (get) Token: 0x06002EB1 RID: 11953 RVA: 0x0009ACE4 File Offset: 0x00098EE4
		public string Name
		{
			get
			{
				return this.sinkName;
			}
		}

		// Token: 0x1700085D RID: 2141
		// (get) Token: 0x06002EB2 RID: 11954 RVA: 0x0009ACEC File Offset: 0x00098EEC
		public IDictionary Properties
		{
			get
			{
				return this.properties;
			}
		}

		// Token: 0x040013E3 RID: 5091
		private string sinkName;

		// Token: 0x040013E4 RID: 5092
		private ArrayList children;

		// Token: 0x040013E5 RID: 5093
		private Hashtable properties;
	}
}
