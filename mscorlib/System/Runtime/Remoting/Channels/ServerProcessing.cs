﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000465 RID: 1125
	[ComVisible(true)]
	[Serializable]
	public enum ServerProcessing
	{
		// Token: 0x040013E0 RID: 5088
		Complete,
		// Token: 0x040013E1 RID: 5089
		OneWay,
		// Token: 0x040013E2 RID: 5090
		Async
	}
}
