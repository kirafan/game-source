﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x0200044C RID: 1100
	internal class ExceptionFilterSink : IMessageSink
	{
		// Token: 0x06002E5E RID: 11870 RVA: 0x0009A9D4 File Offset: 0x00098BD4
		public ExceptionFilterSink(IMessage call, IMessageSink next)
		{
			this._call = call;
			this._next = next;
		}

		// Token: 0x06002E5F RID: 11871 RVA: 0x0009A9EC File Offset: 0x00098BEC
		public IMessage SyncProcessMessage(IMessage msg)
		{
			return this._next.SyncProcessMessage(ChannelServices.CheckReturnMessage(this._call, msg));
		}

		// Token: 0x06002E60 RID: 11872 RVA: 0x0009AA08 File Offset: 0x00098C08
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x17000848 RID: 2120
		// (get) Token: 0x06002E61 RID: 11873 RVA: 0x0009AA10 File Offset: 0x00098C10
		public IMessageSink NextSink
		{
			get
			{
				return this._next;
			}
		}

		// Token: 0x040013D7 RID: 5079
		private IMessageSink _next;

		// Token: 0x040013D8 RID: 5080
		private IMessage _call;
	}
}
