﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Channels
{
	// Token: 0x02000444 RID: 1092
	[ComVisible(true)]
	internal class AggregateDictionary : IEnumerable, ICollection, IDictionary
	{
		// Token: 0x06002E12 RID: 11794 RVA: 0x00099698 File Offset: 0x00097898
		public AggregateDictionary(IDictionary[] dics)
		{
			this.dictionaries = dics;
		}

		// Token: 0x06002E13 RID: 11795 RVA: 0x000996A8 File Offset: 0x000978A8
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new AggregateEnumerator(this.dictionaries);
		}

		// Token: 0x1700082D RID: 2093
		// (get) Token: 0x06002E14 RID: 11796 RVA: 0x000996B8 File Offset: 0x000978B8
		public bool IsFixedSize
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700082E RID: 2094
		// (get) Token: 0x06002E15 RID: 11797 RVA: 0x000996BC File Offset: 0x000978BC
		public bool IsReadOnly
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1700082F RID: 2095
		public object this[object key]
		{
			get
			{
				foreach (IDictionary dictionary in this.dictionaries)
				{
					if (dictionary.Contains(key))
					{
						return dictionary[key];
					}
				}
				return null;
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000830 RID: 2096
		// (get) Token: 0x06002E18 RID: 11800 RVA: 0x0009970C File Offset: 0x0009790C
		public ICollection Keys
		{
			get
			{
				if (this._keys != null)
				{
					return this._keys;
				}
				this._keys = new ArrayList();
				foreach (IDictionary dictionary in this.dictionaries)
				{
					this._keys.AddRange(dictionary.Keys);
				}
				return this._keys;
			}
		}

		// Token: 0x17000831 RID: 2097
		// (get) Token: 0x06002E19 RID: 11801 RVA: 0x0009976C File Offset: 0x0009796C
		public ICollection Values
		{
			get
			{
				if (this._values != null)
				{
					return this._values;
				}
				this._values = new ArrayList();
				foreach (IDictionary dictionary in this.dictionaries)
				{
					this._values.AddRange(dictionary.Values);
				}
				return this._values;
			}
		}

		// Token: 0x06002E1A RID: 11802 RVA: 0x000997CC File Offset: 0x000979CC
		public void Add(object key, object value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002E1B RID: 11803 RVA: 0x000997D4 File Offset: 0x000979D4
		public void Clear()
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002E1C RID: 11804 RVA: 0x000997DC File Offset: 0x000979DC
		public bool Contains(object ob)
		{
			foreach (IDictionary dictionary in this.dictionaries)
			{
				if (dictionary.Contains(ob))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002E1D RID: 11805 RVA: 0x00099818 File Offset: 0x00097A18
		public IDictionaryEnumerator GetEnumerator()
		{
			return new AggregateEnumerator(this.dictionaries);
		}

		// Token: 0x06002E1E RID: 11806 RVA: 0x00099828 File Offset: 0x00097A28
		public void Remove(object ob)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002E1F RID: 11807 RVA: 0x00099830 File Offset: 0x00097A30
		public void CopyTo(Array array, int index)
		{
			foreach (object value in this)
			{
				array.SetValue(value, index++);
			}
		}

		// Token: 0x17000832 RID: 2098
		// (get) Token: 0x06002E20 RID: 11808 RVA: 0x0009989C File Offset: 0x00097A9C
		public int Count
		{
			get
			{
				int num = 0;
				foreach (IDictionary dictionary in this.dictionaries)
				{
					num += dictionary.Count;
				}
				return num;
			}
		}

		// Token: 0x17000833 RID: 2099
		// (get) Token: 0x06002E21 RID: 11809 RVA: 0x000998D4 File Offset: 0x00097AD4
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000834 RID: 2100
		// (get) Token: 0x06002E22 RID: 11810 RVA: 0x000998D8 File Offset: 0x00097AD8
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x040013C7 RID: 5063
		private IDictionary[] dictionaries;

		// Token: 0x040013C8 RID: 5064
		private ArrayList _values;

		// Token: 0x040013C9 RID: 5065
		private ArrayList _keys;
	}
}
