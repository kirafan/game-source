﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	// Token: 0x02000434 RID: 1076
	[ComVisible(true)]
	public class TypeEntry
	{
		// Token: 0x06002DC7 RID: 11719 RVA: 0x000989C8 File Offset: 0x00096BC8
		protected TypeEntry()
		{
		}

		// Token: 0x17000812 RID: 2066
		// (get) Token: 0x06002DC8 RID: 11720 RVA: 0x000989D0 File Offset: 0x00096BD0
		// (set) Token: 0x06002DC9 RID: 11721 RVA: 0x000989D8 File Offset: 0x00096BD8
		public string AssemblyName
		{
			get
			{
				return this.assembly_name;
			}
			set
			{
				this.assembly_name = value;
			}
		}

		// Token: 0x17000813 RID: 2067
		// (get) Token: 0x06002DCA RID: 11722 RVA: 0x000989E4 File Offset: 0x00096BE4
		// (set) Token: 0x06002DCB RID: 11723 RVA: 0x000989EC File Offset: 0x00096BEC
		public string TypeName
		{
			get
			{
				return this.type_name;
			}
			set
			{
				this.type_name = value;
			}
		}

		// Token: 0x040013AD RID: 5037
		private string assembly_name;

		// Token: 0x040013AE RID: 5038
		private string type_name;
	}
}
