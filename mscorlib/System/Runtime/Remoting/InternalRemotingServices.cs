﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Metadata;

namespace System.Runtime.Remoting
{
	// Token: 0x0200041E RID: 1054
	[ComVisible(true)]
	public class InternalRemotingServices
	{
		// Token: 0x06002CD7 RID: 11479 RVA: 0x000941D4 File Offset: 0x000923D4
		[Conditional("_LOGGING")]
		public static void DebugOutChnl(string s)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002CD8 RID: 11480 RVA: 0x000941DC File Offset: 0x000923DC
		public static SoapAttribute GetCachedSoapAttribute(object reflectionObject)
		{
			object syncRoot = InternalRemotingServices._soapAttributes.SyncRoot;
			SoapAttribute result;
			lock (syncRoot)
			{
				SoapAttribute soapAttribute = InternalRemotingServices._soapAttributes[reflectionObject] as SoapAttribute;
				if (soapAttribute != null)
				{
					result = soapAttribute;
				}
				else
				{
					ICustomAttributeProvider customAttributeProvider = (ICustomAttributeProvider)reflectionObject;
					object[] customAttributes = customAttributeProvider.GetCustomAttributes(typeof(SoapAttribute), true);
					if (customAttributes.Length > 0)
					{
						soapAttribute = (SoapAttribute)customAttributes[0];
					}
					else if (reflectionObject is Type)
					{
						soapAttribute = new SoapTypeAttribute();
					}
					else if (reflectionObject is FieldInfo)
					{
						soapAttribute = new SoapFieldAttribute();
					}
					else if (reflectionObject is MethodBase)
					{
						soapAttribute = new SoapMethodAttribute();
					}
					else if (reflectionObject is ParameterInfo)
					{
						soapAttribute = new SoapParameterAttribute();
					}
					soapAttribute.SetReflectionObject(reflectionObject);
					InternalRemotingServices._soapAttributes[reflectionObject] = soapAttribute;
					result = soapAttribute;
				}
			}
			return result;
		}

		// Token: 0x06002CD9 RID: 11481 RVA: 0x000942E4 File Offset: 0x000924E4
		[Conditional("_DEBUG")]
		public static void RemotingAssert(bool condition, string message)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002CDA RID: 11482 RVA: 0x000942EC File Offset: 0x000924EC
		[Conditional("_LOGGING")]
		public static void RemotingTrace(params object[] messages)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002CDB RID: 11483 RVA: 0x000942F4 File Offset: 0x000924F4
		[CLSCompliant(false)]
		public static void SetServerIdentity(MethodCall m, object srvID)
		{
			Identity identity = srvID as Identity;
			if (identity == null)
			{
				throw new ArgumentException("srvID");
			}
			RemotingServices.SetMessageTargetIdentity(m, identity);
		}

		// Token: 0x04001366 RID: 4966
		private static Hashtable _soapAttributes = new Hashtable();
	}
}
