﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Channels;
using Mono.Xml;

namespace System.Runtime.Remoting
{
	// Token: 0x02000423 RID: 1059
	[ComVisible(true)]
	public static class RemotingConfiguration
	{
		// Token: 0x17000803 RID: 2051
		// (get) Token: 0x06002CFC RID: 11516 RVA: 0x00094894 File Offset: 0x00092A94
		public static string ApplicationId
		{
			get
			{
				RemotingConfiguration.applicationID = RemotingConfiguration.ApplicationName;
				return RemotingConfiguration.applicationID;
			}
		}

		// Token: 0x17000804 RID: 2052
		// (get) Token: 0x06002CFD RID: 11517 RVA: 0x000948A8 File Offset: 0x00092AA8
		// (set) Token: 0x06002CFE RID: 11518 RVA: 0x000948B0 File Offset: 0x00092AB0
		public static string ApplicationName
		{
			get
			{
				return RemotingConfiguration.applicationName;
			}
			set
			{
				RemotingConfiguration.applicationName = value;
			}
		}

		// Token: 0x17000805 RID: 2053
		// (get) Token: 0x06002CFF RID: 11519 RVA: 0x000948B8 File Offset: 0x00092AB8
		// (set) Token: 0x06002D00 RID: 11520 RVA: 0x000948C0 File Offset: 0x00092AC0
		[MonoTODO]
		public static CustomErrorsModes CustomErrorsMode
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000806 RID: 2054
		// (get) Token: 0x06002D01 RID: 11521 RVA: 0x000948C8 File Offset: 0x00092AC8
		public static string ProcessId
		{
			get
			{
				if (RemotingConfiguration.processGuid == null)
				{
					RemotingConfiguration.processGuid = AppDomain.GetProcessGuid();
				}
				return RemotingConfiguration.processGuid;
			}
		}

		// Token: 0x06002D02 RID: 11522 RVA: 0x000948E4 File Offset: 0x00092AE4
		[MonoTODO("Implement ensureSecurity")]
		public static void Configure(string filename, bool ensureSecurity)
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			lock (obj)
			{
				if (!RemotingConfiguration.defaultConfigRead)
				{
					RemotingConfiguration.ReadConfigFile(Environment.GetMachineConfigPath());
					RemotingConfiguration.defaultConfigRead = true;
				}
				if (filename != null)
				{
					RemotingConfiguration.ReadConfigFile(filename);
				}
			}
		}

		// Token: 0x06002D03 RID: 11523 RVA: 0x0009494C File Offset: 0x00092B4C
		[Obsolete("Use Configure(String,Boolean)")]
		public static void Configure(string filename)
		{
			RemotingConfiguration.Configure(filename, false);
		}

		// Token: 0x06002D04 RID: 11524 RVA: 0x00094958 File Offset: 0x00092B58
		private static void ReadConfigFile(string filename)
		{
			try
			{
				SmallXmlParser smallXmlParser = new SmallXmlParser();
				using (TextReader textReader = new StreamReader(filename))
				{
					ConfigHandler handler = new ConfigHandler(false);
					smallXmlParser.Parse(textReader, handler);
				}
			}
			catch (Exception ex)
			{
				throw new RemotingException("Configuration file '" + filename + "' could not be loaded: " + ex.Message, ex);
			}
		}

		// Token: 0x06002D05 RID: 11525 RVA: 0x000949F0 File Offset: 0x00092BF0
		internal static void LoadDefaultDelayedChannels()
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			lock (obj)
			{
				if (!RemotingConfiguration.defaultDelayedConfigRead && !RemotingConfiguration.defaultConfigRead)
				{
					SmallXmlParser smallXmlParser = new SmallXmlParser();
					using (TextReader textReader = new StreamReader(Environment.GetMachineConfigPath()))
					{
						ConfigHandler handler = new ConfigHandler(true);
						smallXmlParser.Parse(textReader, handler);
					}
					RemotingConfiguration.defaultDelayedConfigRead = true;
				}
			}
		}

		// Token: 0x06002D06 RID: 11526 RVA: 0x00094A9C File Offset: 0x00092C9C
		public static ActivatedClientTypeEntry[] GetRegisteredActivatedClientTypes()
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			ActivatedClientTypeEntry[] result;
			lock (obj)
			{
				ActivatedClientTypeEntry[] array = new ActivatedClientTypeEntry[RemotingConfiguration.activatedClientEntries.Count];
				RemotingConfiguration.activatedClientEntries.Values.CopyTo(array, 0);
				result = array;
			}
			return result;
		}

		// Token: 0x06002D07 RID: 11527 RVA: 0x00094B08 File Offset: 0x00092D08
		public static ActivatedServiceTypeEntry[] GetRegisteredActivatedServiceTypes()
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			ActivatedServiceTypeEntry[] result;
			lock (obj)
			{
				ActivatedServiceTypeEntry[] array = new ActivatedServiceTypeEntry[RemotingConfiguration.activatedServiceEntries.Count];
				RemotingConfiguration.activatedServiceEntries.Values.CopyTo(array, 0);
				result = array;
			}
			return result;
		}

		// Token: 0x06002D08 RID: 11528 RVA: 0x00094B74 File Offset: 0x00092D74
		public static WellKnownClientTypeEntry[] GetRegisteredWellKnownClientTypes()
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			WellKnownClientTypeEntry[] result;
			lock (obj)
			{
				WellKnownClientTypeEntry[] array = new WellKnownClientTypeEntry[RemotingConfiguration.wellKnownClientEntries.Count];
				RemotingConfiguration.wellKnownClientEntries.Values.CopyTo(array, 0);
				result = array;
			}
			return result;
		}

		// Token: 0x06002D09 RID: 11529 RVA: 0x00094BE0 File Offset: 0x00092DE0
		public static WellKnownServiceTypeEntry[] GetRegisteredWellKnownServiceTypes()
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			WellKnownServiceTypeEntry[] result;
			lock (obj)
			{
				WellKnownServiceTypeEntry[] array = new WellKnownServiceTypeEntry[RemotingConfiguration.wellKnownServiceEntries.Count];
				RemotingConfiguration.wellKnownServiceEntries.Values.CopyTo(array, 0);
				result = array;
			}
			return result;
		}

		// Token: 0x06002D0A RID: 11530 RVA: 0x00094C4C File Offset: 0x00092E4C
		public static bool IsActivationAllowed(Type svrType)
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			bool result;
			lock (obj)
			{
				result = RemotingConfiguration.activatedServiceEntries.ContainsKey(svrType);
			}
			return result;
		}

		// Token: 0x06002D0B RID: 11531 RVA: 0x00094CA0 File Offset: 0x00092EA0
		public static ActivatedClientTypeEntry IsRemotelyActivatedClientType(Type svrType)
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			ActivatedClientTypeEntry result;
			lock (obj)
			{
				result = (RemotingConfiguration.activatedClientEntries[svrType] as ActivatedClientTypeEntry);
			}
			return result;
		}

		// Token: 0x06002D0C RID: 11532 RVA: 0x00094CF8 File Offset: 0x00092EF8
		public static ActivatedClientTypeEntry IsRemotelyActivatedClientType(string typeName, string assemblyName)
		{
			return RemotingConfiguration.IsRemotelyActivatedClientType(Assembly.Load(assemblyName).GetType(typeName));
		}

		// Token: 0x06002D0D RID: 11533 RVA: 0x00094D0C File Offset: 0x00092F0C
		public static WellKnownClientTypeEntry IsWellKnownClientType(Type svrType)
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			WellKnownClientTypeEntry result;
			lock (obj)
			{
				result = (RemotingConfiguration.wellKnownClientEntries[svrType] as WellKnownClientTypeEntry);
			}
			return result;
		}

		// Token: 0x06002D0E RID: 11534 RVA: 0x00094D64 File Offset: 0x00092F64
		public static WellKnownClientTypeEntry IsWellKnownClientType(string typeName, string assemblyName)
		{
			return RemotingConfiguration.IsWellKnownClientType(Assembly.Load(assemblyName).GetType(typeName));
		}

		// Token: 0x06002D0F RID: 11535 RVA: 0x00094D78 File Offset: 0x00092F78
		public static void RegisterActivatedClientType(ActivatedClientTypeEntry entry)
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			lock (obj)
			{
				if (RemotingConfiguration.wellKnownClientEntries.ContainsKey(entry.ObjectType) || RemotingConfiguration.activatedClientEntries.ContainsKey(entry.ObjectType))
				{
					throw new RemotingException("Attempt to redirect activation of type '" + entry.ObjectType.FullName + "' which is already redirected.");
				}
				RemotingConfiguration.activatedClientEntries[entry.ObjectType] = entry;
				ActivationServices.EnableProxyActivation(entry.ObjectType, true);
			}
		}

		// Token: 0x06002D10 RID: 11536 RVA: 0x00094E20 File Offset: 0x00093020
		public static void RegisterActivatedClientType(Type type, string appUrl)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (appUrl == null)
			{
				throw new ArgumentNullException("appUrl");
			}
			RemotingConfiguration.RegisterActivatedClientType(new ActivatedClientTypeEntry(type, appUrl));
		}

		// Token: 0x06002D11 RID: 11537 RVA: 0x00094E5C File Offset: 0x0009305C
		public static void RegisterActivatedServiceType(ActivatedServiceTypeEntry entry)
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			lock (obj)
			{
				RemotingConfiguration.activatedServiceEntries.Add(entry.ObjectType, entry);
			}
		}

		// Token: 0x06002D12 RID: 11538 RVA: 0x00094EB0 File Offset: 0x000930B0
		public static void RegisterActivatedServiceType(Type type)
		{
			RemotingConfiguration.RegisterActivatedServiceType(new ActivatedServiceTypeEntry(type));
		}

		// Token: 0x06002D13 RID: 11539 RVA: 0x00094EC0 File Offset: 0x000930C0
		public static void RegisterWellKnownClientType(Type type, string objectUrl)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (objectUrl == null)
			{
				throw new ArgumentNullException("objectUrl");
			}
			RemotingConfiguration.RegisterWellKnownClientType(new WellKnownClientTypeEntry(type, objectUrl));
		}

		// Token: 0x06002D14 RID: 11540 RVA: 0x00094EFC File Offset: 0x000930FC
		public static void RegisterWellKnownClientType(WellKnownClientTypeEntry entry)
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			lock (obj)
			{
				if (RemotingConfiguration.wellKnownClientEntries.ContainsKey(entry.ObjectType) || RemotingConfiguration.activatedClientEntries.ContainsKey(entry.ObjectType))
				{
					throw new RemotingException("Attempt to redirect activation of type '" + entry.ObjectType.FullName + "' which is already redirected.");
				}
				RemotingConfiguration.wellKnownClientEntries[entry.ObjectType] = entry;
				ActivationServices.EnableProxyActivation(entry.ObjectType, true);
			}
		}

		// Token: 0x06002D15 RID: 11541 RVA: 0x00094FA4 File Offset: 0x000931A4
		public static void RegisterWellKnownServiceType(Type type, string objectUri, WellKnownObjectMode mode)
		{
			RemotingConfiguration.RegisterWellKnownServiceType(new WellKnownServiceTypeEntry(type, objectUri, mode));
		}

		// Token: 0x06002D16 RID: 11542 RVA: 0x00094FB4 File Offset: 0x000931B4
		public static void RegisterWellKnownServiceType(WellKnownServiceTypeEntry entry)
		{
			Hashtable obj = RemotingConfiguration.channelTemplates;
			lock (obj)
			{
				RemotingConfiguration.wellKnownServiceEntries[entry.ObjectUri] = entry;
				RemotingServices.CreateWellKnownServerIdentity(entry.ObjectType, entry.ObjectUri, entry.Mode);
			}
		}

		// Token: 0x06002D17 RID: 11543 RVA: 0x00095020 File Offset: 0x00093220
		internal static void RegisterChannelTemplate(ChannelData channel)
		{
			RemotingConfiguration.channelTemplates[channel.Id] = channel;
		}

		// Token: 0x06002D18 RID: 11544 RVA: 0x00095034 File Offset: 0x00093234
		internal static void RegisterClientProviderTemplate(ProviderData prov)
		{
			RemotingConfiguration.clientProviderTemplates[prov.Id] = prov;
		}

		// Token: 0x06002D19 RID: 11545 RVA: 0x00095048 File Offset: 0x00093248
		internal static void RegisterServerProviderTemplate(ProviderData prov)
		{
			RemotingConfiguration.serverProviderTemplates[prov.Id] = prov;
		}

		// Token: 0x06002D1A RID: 11546 RVA: 0x0009505C File Offset: 0x0009325C
		internal static void RegisterChannels(ArrayList channels, bool onlyDelayed)
		{
			foreach (object obj in channels)
			{
				ChannelData channelData = (ChannelData)obj;
				if (!onlyDelayed || !(channelData.DelayLoadAsClientChannel != "true"))
				{
					if (!RemotingConfiguration.defaultDelayedConfigRead || !(channelData.DelayLoadAsClientChannel == "true"))
					{
						if (channelData.Ref != null)
						{
							ChannelData channelData2 = (ChannelData)RemotingConfiguration.channelTemplates[channelData.Ref];
							if (channelData2 == null)
							{
								throw new RemotingException("Channel template '" + channelData.Ref + "' not found");
							}
							channelData.CopyFrom(channelData2);
						}
						foreach (object obj2 in channelData.ServerProviders)
						{
							ProviderData providerData = (ProviderData)obj2;
							if (providerData.Ref != null)
							{
								ProviderData providerData2 = (ProviderData)RemotingConfiguration.serverProviderTemplates[providerData.Ref];
								if (providerData2 == null)
								{
									throw new RemotingException("Provider template '" + providerData.Ref + "' not found");
								}
								providerData.CopyFrom(providerData2);
							}
						}
						foreach (object obj3 in channelData.ClientProviders)
						{
							ProviderData providerData3 = (ProviderData)obj3;
							if (providerData3.Ref != null)
							{
								ProviderData providerData4 = (ProviderData)RemotingConfiguration.clientProviderTemplates[providerData3.Ref];
								if (providerData4 == null)
								{
									throw new RemotingException("Provider template '" + providerData3.Ref + "' not found");
								}
								providerData3.CopyFrom(providerData4);
							}
						}
						ChannelServices.RegisterChannelConfig(channelData);
					}
				}
			}
		}

		// Token: 0x06002D1B RID: 11547 RVA: 0x000952B0 File Offset: 0x000934B0
		internal static void RegisterTypes(ArrayList types)
		{
			foreach (object obj in types)
			{
				TypeEntry typeEntry = (TypeEntry)obj;
				if (typeEntry is ActivatedClientTypeEntry)
				{
					RemotingConfiguration.RegisterActivatedClientType((ActivatedClientTypeEntry)typeEntry);
				}
				else if (typeEntry is ActivatedServiceTypeEntry)
				{
					RemotingConfiguration.RegisterActivatedServiceType((ActivatedServiceTypeEntry)typeEntry);
				}
				else if (typeEntry is WellKnownClientTypeEntry)
				{
					RemotingConfiguration.RegisterWellKnownClientType((WellKnownClientTypeEntry)typeEntry);
				}
				else if (typeEntry is WellKnownServiceTypeEntry)
				{
					RemotingConfiguration.RegisterWellKnownServiceType((WellKnownServiceTypeEntry)typeEntry);
				}
			}
		}

		// Token: 0x06002D1C RID: 11548 RVA: 0x0009537C File Offset: 0x0009357C
		public static bool CustomErrorsEnabled(bool isLocalRequest)
		{
			return !(RemotingConfiguration._errorMode == "off") && (RemotingConfiguration._errorMode == "on" || !isLocalRequest);
		}

		// Token: 0x06002D1D RID: 11549 RVA: 0x000953BC File Offset: 0x000935BC
		internal static void SetCustomErrorsMode(string mode)
		{
			if (mode == null)
			{
				throw new RemotingException("mode attribute is required");
			}
			string text = mode.ToLower();
			if (text != "on" && text != "off" && text != "remoteonly")
			{
				throw new RemotingException("Invalid custom error mode: " + mode);
			}
			RemotingConfiguration._errorMode = text;
		}

		// Token: 0x04001371 RID: 4977
		private static string applicationID = null;

		// Token: 0x04001372 RID: 4978
		private static string applicationName = null;

		// Token: 0x04001373 RID: 4979
		private static string processGuid = null;

		// Token: 0x04001374 RID: 4980
		private static bool defaultConfigRead = false;

		// Token: 0x04001375 RID: 4981
		private static bool defaultDelayedConfigRead = false;

		// Token: 0x04001376 RID: 4982
		private static string _errorMode;

		// Token: 0x04001377 RID: 4983
		private static Hashtable wellKnownClientEntries = new Hashtable();

		// Token: 0x04001378 RID: 4984
		private static Hashtable activatedClientEntries = new Hashtable();

		// Token: 0x04001379 RID: 4985
		private static Hashtable wellKnownServiceEntries = new Hashtable();

		// Token: 0x0400137A RID: 4986
		private static Hashtable activatedServiceEntries = new Hashtable();

		// Token: 0x0400137B RID: 4987
		private static Hashtable channelTemplates = new Hashtable();

		// Token: 0x0400137C RID: 4988
		private static Hashtable clientProviderTemplates = new Hashtable();

		// Token: 0x0400137D RID: 4989
		private static Hashtable serverProviderTemplates = new Hashtable();
	}
}
