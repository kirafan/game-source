﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting
{
	// Token: 0x02000428 RID: 1064
	[ComVisible(true)]
	[Serializable]
	public class RemotingException : SystemException
	{
		// Token: 0x06002D3E RID: 11582 RVA: 0x0009678C File Offset: 0x0009498C
		public RemotingException()
		{
		}

		// Token: 0x06002D3F RID: 11583 RVA: 0x00096794 File Offset: 0x00094994
		public RemotingException(string message) : base(message)
		{
		}

		// Token: 0x06002D40 RID: 11584 RVA: 0x000967A0 File Offset: 0x000949A0
		protected RemotingException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06002D41 RID: 11585 RVA: 0x000967AC File Offset: 0x000949AC
		public RemotingException(string message, Exception InnerException) : base(message, InnerException)
		{
		}
	}
}
