﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	// Token: 0x02000436 RID: 1078
	[ComVisible(true)]
	[Serializable]
	public enum WellKnownObjectMode
	{
		// Token: 0x040013B3 RID: 5043
		Singleton = 1,
		// Token: 0x040013B4 RID: 5044
		SingleCall
	}
}
