﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	// Token: 0x020004BF RID: 1215
	[ComVisible(true)]
	public class SoapAttribute : Attribute
	{
		// Token: 0x17000939 RID: 2361
		// (get) Token: 0x0600312B RID: 12587 RVA: 0x000A1E1C File Offset: 0x000A001C
		// (set) Token: 0x0600312C RID: 12588 RVA: 0x000A1E24 File Offset: 0x000A0024
		public virtual bool Embedded
		{
			get
			{
				return this._nested;
			}
			set
			{
				this._nested = value;
			}
		}

		// Token: 0x1700093A RID: 2362
		// (get) Token: 0x0600312D RID: 12589 RVA: 0x000A1E30 File Offset: 0x000A0030
		// (set) Token: 0x0600312E RID: 12590 RVA: 0x000A1E38 File Offset: 0x000A0038
		public virtual bool UseAttribute
		{
			get
			{
				return this._useAttribute;
			}
			set
			{
				this._useAttribute = value;
			}
		}

		// Token: 0x1700093B RID: 2363
		// (get) Token: 0x0600312F RID: 12591 RVA: 0x000A1E44 File Offset: 0x000A0044
		// (set) Token: 0x06003130 RID: 12592 RVA: 0x000A1E4C File Offset: 0x000A004C
		public virtual string XmlNamespace
		{
			get
			{
				return this.ProtXmlNamespace;
			}
			set
			{
				this.ProtXmlNamespace = value;
			}
		}

		// Token: 0x06003131 RID: 12593 RVA: 0x000A1E58 File Offset: 0x000A0058
		internal virtual void SetReflectionObject(object reflectionObject)
		{
			this.ReflectInfo = reflectionObject;
		}

		// Token: 0x040014CF RID: 5327
		private bool _nested;

		// Token: 0x040014D0 RID: 5328
		private bool _useAttribute;

		// Token: 0x040014D1 RID: 5329
		protected string ProtXmlNamespace;

		// Token: 0x040014D2 RID: 5330
		protected object ReflectInfo;
	}
}
