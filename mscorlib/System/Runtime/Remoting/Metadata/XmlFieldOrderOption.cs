﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	// Token: 0x020004C5 RID: 1221
	[ComVisible(true)]
	[Serializable]
	public enum XmlFieldOrderOption
	{
		// Token: 0x040014ED RID: 5357
		All,
		// Token: 0x040014EE RID: 5358
		Sequence,
		// Token: 0x040014EF RID: 5359
		Choice
	}
}
