﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004E4 RID: 1252
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapLanguage : ISoapXsd
	{
		// Token: 0x0600324F RID: 12879 RVA: 0x000A3600 File Offset: 0x000A1800
		public SoapLanguage()
		{
		}

		// Token: 0x06003250 RID: 12880 RVA: 0x000A3608 File Offset: 0x000A1808
		public SoapLanguage(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x17000988 RID: 2440
		// (get) Token: 0x06003251 RID: 12881 RVA: 0x000A361C File Offset: 0x000A181C
		// (set) Token: 0x06003252 RID: 12882 RVA: 0x000A3624 File Offset: 0x000A1824
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000989 RID: 2441
		// (get) Token: 0x06003253 RID: 12883 RVA: 0x000A3630 File Offset: 0x000A1830
		public static string XsdType
		{
			get
			{
				return "language";
			}
		}

		// Token: 0x06003254 RID: 12884 RVA: 0x000A3638 File Offset: 0x000A1838
		public string GetXsdType()
		{
			return SoapLanguage.XsdType;
		}

		// Token: 0x06003255 RID: 12885 RVA: 0x000A3640 File Offset: 0x000A1840
		public static SoapLanguage Parse(string value)
		{
			return new SoapLanguage(value);
		}

		// Token: 0x06003256 RID: 12886 RVA: 0x000A3648 File Offset: 0x000A1848
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x04001517 RID: 5399
		private string _value;
	}
}
