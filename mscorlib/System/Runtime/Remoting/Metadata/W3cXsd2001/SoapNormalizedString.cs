﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004D5 RID: 1237
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNormalizedString : ISoapXsd
	{
		// Token: 0x060031D3 RID: 12755 RVA: 0x000A2A14 File Offset: 0x000A0C14
		public SoapNormalizedString()
		{
		}

		// Token: 0x060031D4 RID: 12756 RVA: 0x000A2A1C File Offset: 0x000A0C1C
		public SoapNormalizedString(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x17000969 RID: 2409
		// (get) Token: 0x060031D5 RID: 12757 RVA: 0x000A2A30 File Offset: 0x000A0C30
		// (set) Token: 0x060031D6 RID: 12758 RVA: 0x000A2A38 File Offset: 0x000A0C38
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700096A RID: 2410
		// (get) Token: 0x060031D7 RID: 12759 RVA: 0x000A2A44 File Offset: 0x000A0C44
		public static string XsdType
		{
			get
			{
				return "normalizedString";
			}
		}

		// Token: 0x060031D8 RID: 12760 RVA: 0x000A2A4C File Offset: 0x000A0C4C
		public string GetXsdType()
		{
			return SoapNormalizedString.XsdType;
		}

		// Token: 0x060031D9 RID: 12761 RVA: 0x000A2A54 File Offset: 0x000A0C54
		public static SoapNormalizedString Parse(string value)
		{
			return new SoapNormalizedString(value);
		}

		// Token: 0x060031DA RID: 12762 RVA: 0x000A2A5C File Offset: 0x000A0C5C
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x04001504 RID: 5380
		private string _value;
	}
}
