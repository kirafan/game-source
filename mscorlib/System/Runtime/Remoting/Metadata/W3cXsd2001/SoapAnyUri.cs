﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004C7 RID: 1223
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapAnyUri : ISoapXsd
	{
		// Token: 0x0600315B RID: 12635 RVA: 0x000A217C File Offset: 0x000A037C
		public SoapAnyUri()
		{
		}

		// Token: 0x0600315C RID: 12636 RVA: 0x000A2184 File Offset: 0x000A0384
		public SoapAnyUri(string value)
		{
			this._value = value;
		}

		// Token: 0x1700094D RID: 2381
		// (get) Token: 0x0600315D RID: 12637 RVA: 0x000A2194 File Offset: 0x000A0394
		// (set) Token: 0x0600315E RID: 12638 RVA: 0x000A219C File Offset: 0x000A039C
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700094E RID: 2382
		// (get) Token: 0x0600315F RID: 12639 RVA: 0x000A21A8 File Offset: 0x000A03A8
		public static string XsdType
		{
			get
			{
				return "anyUri";
			}
		}

		// Token: 0x06003160 RID: 12640 RVA: 0x000A21B0 File Offset: 0x000A03B0
		public string GetXsdType()
		{
			return SoapAnyUri.XsdType;
		}

		// Token: 0x06003161 RID: 12641 RVA: 0x000A21B8 File Offset: 0x000A03B8
		public static SoapAnyUri Parse(string value)
		{
			return new SoapAnyUri(value);
		}

		// Token: 0x06003162 RID: 12642 RVA: 0x000A21C0 File Offset: 0x000A03C0
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040014F0 RID: 5360
		private string _value;
	}
}
