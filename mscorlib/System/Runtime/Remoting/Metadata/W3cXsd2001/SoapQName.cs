﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004E2 RID: 1250
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapQName : ISoapXsd
	{
		// Token: 0x06003239 RID: 12857 RVA: 0x000A3484 File Offset: 0x000A1684
		public SoapQName()
		{
		}

		// Token: 0x0600323A RID: 12858 RVA: 0x000A348C File Offset: 0x000A168C
		public SoapQName(string value)
		{
			this._name = value;
		}

		// Token: 0x0600323B RID: 12859 RVA: 0x000A349C File Offset: 0x000A169C
		public SoapQName(string key, string name)
		{
			this._key = key;
			this._name = name;
		}

		// Token: 0x0600323C RID: 12860 RVA: 0x000A34B4 File Offset: 0x000A16B4
		public SoapQName(string key, string name, string namespaceValue)
		{
			this._key = key;
			this._name = name;
			this._namespace = namespaceValue;
		}

		// Token: 0x17000982 RID: 2434
		// (get) Token: 0x0600323D RID: 12861 RVA: 0x000A34D4 File Offset: 0x000A16D4
		// (set) Token: 0x0600323E RID: 12862 RVA: 0x000A34DC File Offset: 0x000A16DC
		public string Key
		{
			get
			{
				return this._key;
			}
			set
			{
				this._key = value;
			}
		}

		// Token: 0x17000983 RID: 2435
		// (get) Token: 0x0600323F RID: 12863 RVA: 0x000A34E8 File Offset: 0x000A16E8
		// (set) Token: 0x06003240 RID: 12864 RVA: 0x000A34F0 File Offset: 0x000A16F0
		public string Name
		{
			get
			{
				return this._name;
			}
			set
			{
				this._name = value;
			}
		}

		// Token: 0x17000984 RID: 2436
		// (get) Token: 0x06003241 RID: 12865 RVA: 0x000A34FC File Offset: 0x000A16FC
		// (set) Token: 0x06003242 RID: 12866 RVA: 0x000A3504 File Offset: 0x000A1704
		public string Namespace
		{
			get
			{
				return this._namespace;
			}
			set
			{
				this._namespace = value;
			}
		}

		// Token: 0x17000985 RID: 2437
		// (get) Token: 0x06003243 RID: 12867 RVA: 0x000A3510 File Offset: 0x000A1710
		public static string XsdType
		{
			get
			{
				return "QName";
			}
		}

		// Token: 0x06003244 RID: 12868 RVA: 0x000A3518 File Offset: 0x000A1718
		public string GetXsdType()
		{
			return SoapQName.XsdType;
		}

		// Token: 0x06003245 RID: 12869 RVA: 0x000A3520 File Offset: 0x000A1720
		public static SoapQName Parse(string value)
		{
			SoapQName soapQName = new SoapQName();
			int num = value.IndexOf(':');
			if (num != -1)
			{
				soapQName.Key = value.Substring(0, num);
				soapQName.Name = value.Substring(num + 1);
			}
			else
			{
				soapQName.Name = value;
			}
			return soapQName;
		}

		// Token: 0x06003246 RID: 12870 RVA: 0x000A3570 File Offset: 0x000A1770
		public override string ToString()
		{
			if (this._key == null || this._key == string.Empty)
			{
				return this._name;
			}
			return this._key + ":" + this._name;
		}

		// Token: 0x04001513 RID: 5395
		private string _name;

		// Token: 0x04001514 RID: 5396
		private string _key;

		// Token: 0x04001515 RID: 5397
		private string _namespace;
	}
}
