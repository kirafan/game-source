﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004D3 RID: 1235
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapId : ISoapXsd
	{
		// Token: 0x060031C3 RID: 12739 RVA: 0x000A2974 File Offset: 0x000A0B74
		public SoapId()
		{
		}

		// Token: 0x060031C4 RID: 12740 RVA: 0x000A297C File Offset: 0x000A0B7C
		public SoapId(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x17000965 RID: 2405
		// (get) Token: 0x060031C5 RID: 12741 RVA: 0x000A2990 File Offset: 0x000A0B90
		// (set) Token: 0x060031C6 RID: 12742 RVA: 0x000A2998 File Offset: 0x000A0B98
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000966 RID: 2406
		// (get) Token: 0x060031C7 RID: 12743 RVA: 0x000A29A4 File Offset: 0x000A0BA4
		public static string XsdType
		{
			get
			{
				return "ID";
			}
		}

		// Token: 0x060031C8 RID: 12744 RVA: 0x000A29AC File Offset: 0x000A0BAC
		public string GetXsdType()
		{
			return SoapId.XsdType;
		}

		// Token: 0x060031C9 RID: 12745 RVA: 0x000A29B4 File Offset: 0x000A0BB4
		public static SoapId Parse(string value)
		{
			return new SoapId(value);
		}

		// Token: 0x060031CA RID: 12746 RVA: 0x000A29BC File Offset: 0x000A0BBC
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x04001502 RID: 5378
		private string _value;
	}
}
