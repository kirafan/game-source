﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004C9 RID: 1225
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapMonth : ISoapXsd
	{
		// Token: 0x0600316B RID: 12651 RVA: 0x000A2218 File Offset: 0x000A0418
		public SoapMonth()
		{
		}

		// Token: 0x0600316C RID: 12652 RVA: 0x000A2220 File Offset: 0x000A0420
		public SoapMonth(DateTime value)
		{
			this._value = value;
		}

		// Token: 0x17000951 RID: 2385
		// (get) Token: 0x0600316E RID: 12654 RVA: 0x000A2250 File Offset: 0x000A0450
		// (set) Token: 0x0600316F RID: 12655 RVA: 0x000A2258 File Offset: 0x000A0458
		public DateTime Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000952 RID: 2386
		// (get) Token: 0x06003170 RID: 12656 RVA: 0x000A2264 File Offset: 0x000A0464
		public static string XsdType
		{
			get
			{
				return "gMonth";
			}
		}

		// Token: 0x06003171 RID: 12657 RVA: 0x000A226C File Offset: 0x000A046C
		public string GetXsdType()
		{
			return SoapMonth.XsdType;
		}

		// Token: 0x06003172 RID: 12658 RVA: 0x000A2274 File Offset: 0x000A0474
		public static SoapMonth Parse(string value)
		{
			DateTime value2 = DateTime.ParseExact(value, SoapMonth._datetimeFormats, null, DateTimeStyles.None);
			return new SoapMonth(value2);
		}

		// Token: 0x06003173 RID: 12659 RVA: 0x000A2298 File Offset: 0x000A0498
		public override string ToString()
		{
			return this._value.ToString("--MM--", CultureInfo.InvariantCulture);
		}

		// Token: 0x040014F2 RID: 5362
		private static readonly string[] _datetimeFormats = new string[]
		{
			"--MM--",
			"--MM--zzz"
		};

		// Token: 0x040014F3 RID: 5363
		private DateTime _value;
	}
}
