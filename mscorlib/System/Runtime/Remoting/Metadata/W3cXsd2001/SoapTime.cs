﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004E6 RID: 1254
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapTime : ISoapXsd
	{
		// Token: 0x0600325F RID: 12895 RVA: 0x000A36A0 File Offset: 0x000A18A0
		public SoapTime()
		{
		}

		// Token: 0x06003260 RID: 12896 RVA: 0x000A36A8 File Offset: 0x000A18A8
		public SoapTime(DateTime value)
		{
			this._value = value;
		}

		// Token: 0x1700098C RID: 2444
		// (get) Token: 0x06003262 RID: 12898 RVA: 0x000A37A0 File Offset: 0x000A19A0
		// (set) Token: 0x06003263 RID: 12899 RVA: 0x000A37A8 File Offset: 0x000A19A8
		public DateTime Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700098D RID: 2445
		// (get) Token: 0x06003264 RID: 12900 RVA: 0x000A37B4 File Offset: 0x000A19B4
		public static string XsdType
		{
			get
			{
				return "time";
			}
		}

		// Token: 0x06003265 RID: 12901 RVA: 0x000A37BC File Offset: 0x000A19BC
		public string GetXsdType()
		{
			return SoapTime.XsdType;
		}

		// Token: 0x06003266 RID: 12902 RVA: 0x000A37C4 File Offset: 0x000A19C4
		public static SoapTime Parse(string value)
		{
			return new SoapTime(DateTime.ParseExact(value, SoapTime._datetimeFormats, null, DateTimeStyles.None));
		}

		// Token: 0x06003267 RID: 12903 RVA: 0x000A37D8 File Offset: 0x000A19D8
		public override string ToString()
		{
			return this._value.ToString("HH:mm:ss.fffffffzzz", CultureInfo.InvariantCulture);
		}

		// Token: 0x04001519 RID: 5401
		private static readonly string[] _datetimeFormats = new string[]
		{
			"HH:mm:ss",
			"HH:mm:ss.f",
			"HH:mm:ss.ff",
			"HH:mm:ss.fff",
			"HH:mm:ss.ffff",
			"HH:mm:ss.fffff",
			"HH:mm:ss.ffffff",
			"HH:mm:ss.fffffff",
			"HH:mm:sszzz",
			"HH:mm:ss.fzzz",
			"HH:mm:ss.ffzzz",
			"HH:mm:ss.fffzzz",
			"HH:mm:ss.ffffzzz",
			"HH:mm:ss.fffffzzz",
			"HH:mm:ss.ffffffzzz",
			"HH:mm:ss.fffffffzzz",
			"HH:mm:ssZ",
			"HH:mm:ss.fZ",
			"HH:mm:ss.ffZ",
			"HH:mm:ss.fffZ",
			"HH:mm:ss.ffffZ",
			"HH:mm:ss.fffffZ",
			"HH:mm:ss.ffffffZ",
			"HH:mm:ss.fffffffZ"
		};

		// Token: 0x0400151A RID: 5402
		private DateTime _value;
	}
}
