﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004CA RID: 1226
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNonNegativeInteger : ISoapXsd
	{
		// Token: 0x06003174 RID: 12660 RVA: 0x000A22B0 File Offset: 0x000A04B0
		public SoapNonNegativeInteger()
		{
		}

		// Token: 0x06003175 RID: 12661 RVA: 0x000A22B8 File Offset: 0x000A04B8
		public SoapNonNegativeInteger(decimal value)
		{
			if (value < 0m)
			{
				throw SoapHelper.GetException(this, "invalid " + value);
			}
			this._value = value;
		}

		// Token: 0x17000953 RID: 2387
		// (get) Token: 0x06003176 RID: 12662 RVA: 0x000A22F0 File Offset: 0x000A04F0
		// (set) Token: 0x06003177 RID: 12663 RVA: 0x000A22F8 File Offset: 0x000A04F8
		public decimal Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000954 RID: 2388
		// (get) Token: 0x06003178 RID: 12664 RVA: 0x000A2304 File Offset: 0x000A0504
		public static string XsdType
		{
			get
			{
				return "nonNegativeInteger";
			}
		}

		// Token: 0x06003179 RID: 12665 RVA: 0x000A230C File Offset: 0x000A050C
		public string GetXsdType()
		{
			return SoapNonNegativeInteger.XsdType;
		}

		// Token: 0x0600317A RID: 12666 RVA: 0x000A2314 File Offset: 0x000A0514
		public static SoapNonNegativeInteger Parse(string value)
		{
			return new SoapNonNegativeInteger(decimal.Parse(value));
		}

		// Token: 0x0600317B RID: 12667 RVA: 0x000A2324 File Offset: 0x000A0524
		public override string ToString()
		{
			return this._value.ToString();
		}

		// Token: 0x040014F4 RID: 5364
		private decimal _value;
	}
}
