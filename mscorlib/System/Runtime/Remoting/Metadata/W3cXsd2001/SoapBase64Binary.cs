﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004CC RID: 1228
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapBase64Binary : ISoapXsd
	{
		// Token: 0x06003184 RID: 12676 RVA: 0x000A2384 File Offset: 0x000A0584
		public SoapBase64Binary()
		{
		}

		// Token: 0x06003185 RID: 12677 RVA: 0x000A238C File Offset: 0x000A058C
		public SoapBase64Binary(byte[] value)
		{
			this._value = value;
		}

		// Token: 0x17000957 RID: 2391
		// (get) Token: 0x06003186 RID: 12678 RVA: 0x000A239C File Offset: 0x000A059C
		// (set) Token: 0x06003187 RID: 12679 RVA: 0x000A23A4 File Offset: 0x000A05A4
		public byte[] Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000958 RID: 2392
		// (get) Token: 0x06003188 RID: 12680 RVA: 0x000A23B0 File Offset: 0x000A05B0
		public static string XsdType
		{
			get
			{
				return "base64Binary";
			}
		}

		// Token: 0x06003189 RID: 12681 RVA: 0x000A23B8 File Offset: 0x000A05B8
		public string GetXsdType()
		{
			return SoapBase64Binary.XsdType;
		}

		// Token: 0x0600318A RID: 12682 RVA: 0x000A23C0 File Offset: 0x000A05C0
		public static SoapBase64Binary Parse(string value)
		{
			return new SoapBase64Binary(Convert.FromBase64String(value));
		}

		// Token: 0x0600318B RID: 12683 RVA: 0x000A23D0 File Offset: 0x000A05D0
		public override string ToString()
		{
			return Convert.ToBase64String(this._value);
		}

		// Token: 0x040014F6 RID: 5366
		private byte[] _value;
	}
}
