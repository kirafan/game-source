﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004DD RID: 1245
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNegativeInteger : ISoapXsd
	{
		// Token: 0x06003215 RID: 12821 RVA: 0x000A2E88 File Offset: 0x000A1088
		public SoapNegativeInteger()
		{
		}

		// Token: 0x06003216 RID: 12822 RVA: 0x000A2E90 File Offset: 0x000A1090
		public SoapNegativeInteger(decimal value)
		{
			if (value >= 0m)
			{
				throw SoapHelper.GetException(this, "invalid " + value);
			}
			this._value = value;
		}

		// Token: 0x17000979 RID: 2425
		// (get) Token: 0x06003217 RID: 12823 RVA: 0x000A2EC8 File Offset: 0x000A10C8
		// (set) Token: 0x06003218 RID: 12824 RVA: 0x000A2ED0 File Offset: 0x000A10D0
		public decimal Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700097A RID: 2426
		// (get) Token: 0x06003219 RID: 12825 RVA: 0x000A2EDC File Offset: 0x000A10DC
		public static string XsdType
		{
			get
			{
				return "negativeInteger";
			}
		}

		// Token: 0x0600321A RID: 12826 RVA: 0x000A2EE4 File Offset: 0x000A10E4
		public string GetXsdType()
		{
			return SoapNegativeInteger.XsdType;
		}

		// Token: 0x0600321B RID: 12827 RVA: 0x000A2EEC File Offset: 0x000A10EC
		public static SoapNegativeInteger Parse(string value)
		{
			return new SoapNegativeInteger(decimal.Parse(value));
		}

		// Token: 0x0600321C RID: 12828 RVA: 0x000A2EFC File Offset: 0x000A10FC
		public override string ToString()
		{
			return this._value.ToString();
		}

		// Token: 0x0400150F RID: 5391
		private decimal _value;
	}
}
