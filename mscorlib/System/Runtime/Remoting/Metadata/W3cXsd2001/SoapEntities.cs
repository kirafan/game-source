﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004E3 RID: 1251
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapEntities : ISoapXsd
	{
		// Token: 0x06003247 RID: 12871 RVA: 0x000A35B0 File Offset: 0x000A17B0
		public SoapEntities()
		{
		}

		// Token: 0x06003248 RID: 12872 RVA: 0x000A35B8 File Offset: 0x000A17B8
		public SoapEntities(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x17000986 RID: 2438
		// (get) Token: 0x06003249 RID: 12873 RVA: 0x000A35CC File Offset: 0x000A17CC
		// (set) Token: 0x0600324A RID: 12874 RVA: 0x000A35D4 File Offset: 0x000A17D4
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000987 RID: 2439
		// (get) Token: 0x0600324B RID: 12875 RVA: 0x000A35E0 File Offset: 0x000A17E0
		public static string XsdType
		{
			get
			{
				return "ENTITIES";
			}
		}

		// Token: 0x0600324C RID: 12876 RVA: 0x000A35E8 File Offset: 0x000A17E8
		public string GetXsdType()
		{
			return SoapEntities.XsdType;
		}

		// Token: 0x0600324D RID: 12877 RVA: 0x000A35F0 File Offset: 0x000A17F0
		public static SoapEntities Parse(string value)
		{
			return new SoapEntities(value);
		}

		// Token: 0x0600324E RID: 12878 RVA: 0x000A35F8 File Offset: 0x000A17F8
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x04001516 RID: 5398
		private string _value;
	}
}
