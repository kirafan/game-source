﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004DE RID: 1246
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapPositiveInteger : ISoapXsd
	{
		// Token: 0x0600321D RID: 12829 RVA: 0x000A2F0C File Offset: 0x000A110C
		public SoapPositiveInteger()
		{
		}

		// Token: 0x0600321E RID: 12830 RVA: 0x000A2F14 File Offset: 0x000A1114
		public SoapPositiveInteger(decimal value)
		{
			if (value <= 0m)
			{
				throw SoapHelper.GetException(this, "invalid " + value);
			}
			this._value = value;
		}

		// Token: 0x1700097B RID: 2427
		// (get) Token: 0x0600321F RID: 12831 RVA: 0x000A2F4C File Offset: 0x000A114C
		// (set) Token: 0x06003220 RID: 12832 RVA: 0x000A2F54 File Offset: 0x000A1154
		public decimal Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700097C RID: 2428
		// (get) Token: 0x06003221 RID: 12833 RVA: 0x000A2F60 File Offset: 0x000A1160
		public static string XsdType
		{
			get
			{
				return "positiveInteger";
			}
		}

		// Token: 0x06003222 RID: 12834 RVA: 0x000A2F68 File Offset: 0x000A1168
		public string GetXsdType()
		{
			return SoapPositiveInteger.XsdType;
		}

		// Token: 0x06003223 RID: 12835 RVA: 0x000A2F70 File Offset: 0x000A1170
		public static SoapPositiveInteger Parse(string value)
		{
			return new SoapPositiveInteger(decimal.Parse(value));
		}

		// Token: 0x06003224 RID: 12836 RVA: 0x000A2F80 File Offset: 0x000A1180
		public override string ToString()
		{
			return this._value.ToString();
		}

		// Token: 0x04001510 RID: 5392
		private decimal _value;
	}
}
