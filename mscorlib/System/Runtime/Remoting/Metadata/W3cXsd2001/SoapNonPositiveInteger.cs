﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004CF RID: 1231
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNonPositiveInteger : ISoapXsd
	{
		// Token: 0x060031A0 RID: 12704 RVA: 0x000A265C File Offset: 0x000A085C
		public SoapNonPositiveInteger()
		{
		}

		// Token: 0x060031A1 RID: 12705 RVA: 0x000A2664 File Offset: 0x000A0864
		public SoapNonPositiveInteger(decimal value)
		{
			if (value > 0m)
			{
				throw SoapHelper.GetException(this, "invalid " + value);
			}
			this._value = value;
		}

		// Token: 0x1700095D RID: 2397
		// (get) Token: 0x060031A2 RID: 12706 RVA: 0x000A269C File Offset: 0x000A089C
		// (set) Token: 0x060031A3 RID: 12707 RVA: 0x000A26A4 File Offset: 0x000A08A4
		public decimal Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700095E RID: 2398
		// (get) Token: 0x060031A4 RID: 12708 RVA: 0x000A26B0 File Offset: 0x000A08B0
		public static string XsdType
		{
			get
			{
				return "nonPositiveInteger";
			}
		}

		// Token: 0x060031A5 RID: 12709 RVA: 0x000A26B8 File Offset: 0x000A08B8
		public string GetXsdType()
		{
			return SoapNonPositiveInteger.XsdType;
		}

		// Token: 0x060031A6 RID: 12710 RVA: 0x000A26C0 File Offset: 0x000A08C0
		public static SoapNonPositiveInteger Parse(string value)
		{
			return new SoapNonPositiveInteger(decimal.Parse(value));
		}

		// Token: 0x060031A7 RID: 12711 RVA: 0x000A26D0 File Offset: 0x000A08D0
		public override string ToString()
		{
			return this._value.ToString();
		}

		// Token: 0x040014FB RID: 5371
		private decimal _value;
	}
}
