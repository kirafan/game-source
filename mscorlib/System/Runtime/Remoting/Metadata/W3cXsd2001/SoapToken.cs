﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004CB RID: 1227
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapToken : ISoapXsd
	{
		// Token: 0x0600317C RID: 12668 RVA: 0x000A2334 File Offset: 0x000A0534
		public SoapToken()
		{
		}

		// Token: 0x0600317D RID: 12669 RVA: 0x000A233C File Offset: 0x000A053C
		public SoapToken(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x17000955 RID: 2389
		// (get) Token: 0x0600317E RID: 12670 RVA: 0x000A2350 File Offset: 0x000A0550
		// (set) Token: 0x0600317F RID: 12671 RVA: 0x000A2358 File Offset: 0x000A0558
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000956 RID: 2390
		// (get) Token: 0x06003180 RID: 12672 RVA: 0x000A2364 File Offset: 0x000A0564
		public static string XsdType
		{
			get
			{
				return "token";
			}
		}

		// Token: 0x06003181 RID: 12673 RVA: 0x000A236C File Offset: 0x000A056C
		public string GetXsdType()
		{
			return SoapToken.XsdType;
		}

		// Token: 0x06003182 RID: 12674 RVA: 0x000A2374 File Offset: 0x000A0574
		public static SoapToken Parse(string value)
		{
			return new SoapToken(value);
		}

		// Token: 0x06003183 RID: 12675 RVA: 0x000A237C File Offset: 0x000A057C
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040014F5 RID: 5365
		private string _value;
	}
}
