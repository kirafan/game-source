﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004D1 RID: 1233
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapDate : ISoapXsd
	{
		// Token: 0x060031B4 RID: 12724 RVA: 0x000A2814 File Offset: 0x000A0A14
		public SoapDate()
		{
		}

		// Token: 0x060031B5 RID: 12725 RVA: 0x000A281C File Offset: 0x000A0A1C
		public SoapDate(DateTime value)
		{
			this._value = value;
		}

		// Token: 0x060031B6 RID: 12726 RVA: 0x000A282C File Offset: 0x000A0A2C
		public SoapDate(DateTime value, int sign)
		{
			this._value = value;
			this._sign = sign;
		}

		// Token: 0x17000962 RID: 2402
		// (get) Token: 0x060031B8 RID: 12728 RVA: 0x000A2884 File Offset: 0x000A0A84
		// (set) Token: 0x060031B9 RID: 12729 RVA: 0x000A288C File Offset: 0x000A0A8C
		public int Sign
		{
			get
			{
				return this._sign;
			}
			set
			{
				this._sign = value;
			}
		}

		// Token: 0x17000963 RID: 2403
		// (get) Token: 0x060031BA RID: 12730 RVA: 0x000A2898 File Offset: 0x000A0A98
		// (set) Token: 0x060031BB RID: 12731 RVA: 0x000A28A0 File Offset: 0x000A0AA0
		public DateTime Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000964 RID: 2404
		// (get) Token: 0x060031BC RID: 12732 RVA: 0x000A28AC File Offset: 0x000A0AAC
		public static string XsdType
		{
			get
			{
				return "date";
			}
		}

		// Token: 0x060031BD RID: 12733 RVA: 0x000A28B4 File Offset: 0x000A0AB4
		public string GetXsdType()
		{
			return SoapDate.XsdType;
		}

		// Token: 0x060031BE RID: 12734 RVA: 0x000A28BC File Offset: 0x000A0ABC
		public static SoapDate Parse(string value)
		{
			DateTime value2 = DateTime.ParseExact(value, SoapDate._datetimeFormats, null, DateTimeStyles.None);
			SoapDate soapDate = new SoapDate(value2);
			if (value.StartsWith("-"))
			{
				soapDate.Sign = -1;
			}
			else
			{
				soapDate.Sign = 0;
			}
			return soapDate;
		}

		// Token: 0x060031BF RID: 12735 RVA: 0x000A2904 File Offset: 0x000A0B04
		public override string ToString()
		{
			if (this._sign >= 0)
			{
				return this._value.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
			}
			return this._value.ToString("'-'yyyy-MM-dd", CultureInfo.InvariantCulture);
		}

		// Token: 0x040014FF RID: 5375
		private static readonly string[] _datetimeFormats = new string[]
		{
			"yyyy-MM-dd",
			"'+'yyyy-MM-dd",
			"'-'yyyy-MM-dd",
			"yyyy-MM-ddzzz",
			"'+'yyyy-MM-ddzzz",
			"'-'yyyy-MM-ddzzz"
		};

		// Token: 0x04001500 RID: 5376
		private int _sign;

		// Token: 0x04001501 RID: 5377
		private DateTime _value;
	}
}
