﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004D9 RID: 1241
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNcName : ISoapXsd
	{
		// Token: 0x060031F4 RID: 12788 RVA: 0x000A2D04 File Offset: 0x000A0F04
		public SoapNcName()
		{
		}

		// Token: 0x060031F5 RID: 12789 RVA: 0x000A2D0C File Offset: 0x000A0F0C
		public SoapNcName(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x17000971 RID: 2417
		// (get) Token: 0x060031F6 RID: 12790 RVA: 0x000A2D20 File Offset: 0x000A0F20
		// (set) Token: 0x060031F7 RID: 12791 RVA: 0x000A2D28 File Offset: 0x000A0F28
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000972 RID: 2418
		// (get) Token: 0x060031F8 RID: 12792 RVA: 0x000A2D34 File Offset: 0x000A0F34
		public static string XsdType
		{
			get
			{
				return "NCName";
			}
		}

		// Token: 0x060031F9 RID: 12793 RVA: 0x000A2D3C File Offset: 0x000A0F3C
		public string GetXsdType()
		{
			return SoapNcName.XsdType;
		}

		// Token: 0x060031FA RID: 12794 RVA: 0x000A2D44 File Offset: 0x000A0F44
		public static SoapNcName Parse(string value)
		{
			return new SoapNcName(value);
		}

		// Token: 0x060031FB RID: 12795 RVA: 0x000A2D4C File Offset: 0x000A0F4C
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x0400150A RID: 5386
		private string _value;
	}
}
