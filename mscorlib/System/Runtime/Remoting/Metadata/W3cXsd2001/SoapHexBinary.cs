﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004CD RID: 1229
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapHexBinary : ISoapXsd
	{
		// Token: 0x0600318C RID: 12684 RVA: 0x000A23E0 File Offset: 0x000A05E0
		public SoapHexBinary()
		{
		}

		// Token: 0x0600318D RID: 12685 RVA: 0x000A23F4 File Offset: 0x000A05F4
		public SoapHexBinary(byte[] value)
		{
			this._value = value;
		}

		// Token: 0x17000959 RID: 2393
		// (get) Token: 0x0600318E RID: 12686 RVA: 0x000A2410 File Offset: 0x000A0610
		// (set) Token: 0x0600318F RID: 12687 RVA: 0x000A2418 File Offset: 0x000A0618
		public byte[] Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700095A RID: 2394
		// (get) Token: 0x06003190 RID: 12688 RVA: 0x000A2424 File Offset: 0x000A0624
		public static string XsdType
		{
			get
			{
				return "hexBinary";
			}
		}

		// Token: 0x06003191 RID: 12689 RVA: 0x000A242C File Offset: 0x000A062C
		public string GetXsdType()
		{
			return SoapHexBinary.XsdType;
		}

		// Token: 0x06003192 RID: 12690 RVA: 0x000A2434 File Offset: 0x000A0634
		public static SoapHexBinary Parse(string value)
		{
			byte[] value2 = SoapHexBinary.FromBinHexString(value);
			return new SoapHexBinary(value2);
		}

		// Token: 0x06003193 RID: 12691 RVA: 0x000A2450 File Offset: 0x000A0650
		internal static byte[] FromBinHexString(string value)
		{
			char[] array = value.ToCharArray();
			byte[] array2 = new byte[array.Length / 2 + array.Length % 2];
			int num = array.Length;
			if (num % 2 != 0)
			{
				throw SoapHexBinary.CreateInvalidValueException(value);
			}
			int num2 = 0;
			for (int i = 0; i < num - 1; i += 2)
			{
				array2[num2] = SoapHexBinary.FromHex(array[i], value);
				byte[] array3 = array2;
				int num3 = num2;
				array3[num3] = (byte)(array3[num3] << 4);
				byte[] array4 = array2;
				int num4 = num2;
				array4[num4] += SoapHexBinary.FromHex(array[i + 1], value);
				num2++;
			}
			return array2;
		}

		// Token: 0x06003194 RID: 12692 RVA: 0x000A24DC File Offset: 0x000A06DC
		private static byte FromHex(char hexDigit, string value)
		{
			byte result;
			try
			{
				result = byte.Parse(hexDigit.ToString(), NumberStyles.HexNumber, CultureInfo.InvariantCulture);
			}
			catch (FormatException)
			{
				throw SoapHexBinary.CreateInvalidValueException(value);
			}
			return result;
		}

		// Token: 0x06003195 RID: 12693 RVA: 0x000A2534 File Offset: 0x000A0734
		private static Exception CreateInvalidValueException(string value)
		{
			return new RemotingException(string.Format(CultureInfo.InvariantCulture, "Invalid value '{0}' for xsd:{1}.", new object[]
			{
				value,
				SoapHexBinary.XsdType
			}));
		}

		// Token: 0x06003196 RID: 12694 RVA: 0x000A2568 File Offset: 0x000A0768
		public override string ToString()
		{
			this.sb.Length = 0;
			foreach (byte b in this._value)
			{
				this.sb.Append(b.ToString("X2"));
			}
			return this.sb.ToString();
		}

		// Token: 0x040014F7 RID: 5367
		private byte[] _value;

		// Token: 0x040014F8 RID: 5368
		private StringBuilder sb = new StringBuilder();
	}
}
