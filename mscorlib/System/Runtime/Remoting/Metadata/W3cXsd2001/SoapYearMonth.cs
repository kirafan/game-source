﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004D6 RID: 1238
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapYearMonth : ISoapXsd
	{
		// Token: 0x060031DB RID: 12763 RVA: 0x000A2A64 File Offset: 0x000A0C64
		public SoapYearMonth()
		{
		}

		// Token: 0x060031DC RID: 12764 RVA: 0x000A2A6C File Offset: 0x000A0C6C
		public SoapYearMonth(DateTime value)
		{
			this._value = value;
		}

		// Token: 0x060031DD RID: 12765 RVA: 0x000A2A7C File Offset: 0x000A0C7C
		public SoapYearMonth(DateTime value, int sign)
		{
			this._value = value;
			this._sign = sign;
		}

		// Token: 0x1700096B RID: 2411
		// (get) Token: 0x060031DF RID: 12767 RVA: 0x000A2AD4 File Offset: 0x000A0CD4
		// (set) Token: 0x060031E0 RID: 12768 RVA: 0x000A2ADC File Offset: 0x000A0CDC
		public int Sign
		{
			get
			{
				return this._sign;
			}
			set
			{
				this._sign = value;
			}
		}

		// Token: 0x1700096C RID: 2412
		// (get) Token: 0x060031E1 RID: 12769 RVA: 0x000A2AE8 File Offset: 0x000A0CE8
		// (set) Token: 0x060031E2 RID: 12770 RVA: 0x000A2AF0 File Offset: 0x000A0CF0
		public DateTime Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700096D RID: 2413
		// (get) Token: 0x060031E3 RID: 12771 RVA: 0x000A2AFC File Offset: 0x000A0CFC
		public static string XsdType
		{
			get
			{
				return "gYearMonth";
			}
		}

		// Token: 0x060031E4 RID: 12772 RVA: 0x000A2B04 File Offset: 0x000A0D04
		public string GetXsdType()
		{
			return SoapYearMonth.XsdType;
		}

		// Token: 0x060031E5 RID: 12773 RVA: 0x000A2B0C File Offset: 0x000A0D0C
		public static SoapYearMonth Parse(string value)
		{
			DateTime value2 = DateTime.ParseExact(value, SoapYearMonth._datetimeFormats, null, DateTimeStyles.None);
			SoapYearMonth soapYearMonth = new SoapYearMonth(value2);
			if (value.StartsWith("-"))
			{
				soapYearMonth.Sign = -1;
			}
			else
			{
				soapYearMonth.Sign = 0;
			}
			return soapYearMonth;
		}

		// Token: 0x060031E6 RID: 12774 RVA: 0x000A2B54 File Offset: 0x000A0D54
		public override string ToString()
		{
			if (this._sign >= 0)
			{
				return this._value.ToString("yyyy-MM", CultureInfo.InvariantCulture);
			}
			return this._value.ToString("'-'yyyy-MM", CultureInfo.InvariantCulture);
		}

		// Token: 0x04001505 RID: 5381
		private static readonly string[] _datetimeFormats = new string[]
		{
			"yyyy-MM",
			"'+'yyyy-MM",
			"'-'yyyy-MM",
			"yyyy-MMzzz",
			"'+'yyyy-MMzzz",
			"'-'yyyy-MMzzz"
		};

		// Token: 0x04001506 RID: 5382
		private int _sign;

		// Token: 0x04001507 RID: 5383
		private DateTime _value;
	}
}
