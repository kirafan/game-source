﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004CE RID: 1230
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapMonthDay : ISoapXsd
	{
		// Token: 0x06003197 RID: 12695 RVA: 0x000A25C4 File Offset: 0x000A07C4
		public SoapMonthDay()
		{
		}

		// Token: 0x06003198 RID: 12696 RVA: 0x000A25CC File Offset: 0x000A07CC
		public SoapMonthDay(DateTime value)
		{
			this._value = value;
		}

		// Token: 0x1700095B RID: 2395
		// (get) Token: 0x0600319A RID: 12698 RVA: 0x000A25FC File Offset: 0x000A07FC
		// (set) Token: 0x0600319B RID: 12699 RVA: 0x000A2604 File Offset: 0x000A0804
		public DateTime Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700095C RID: 2396
		// (get) Token: 0x0600319C RID: 12700 RVA: 0x000A2610 File Offset: 0x000A0810
		public static string XsdType
		{
			get
			{
				return "gMonthDay";
			}
		}

		// Token: 0x0600319D RID: 12701 RVA: 0x000A2618 File Offset: 0x000A0818
		public string GetXsdType()
		{
			return SoapMonthDay.XsdType;
		}

		// Token: 0x0600319E RID: 12702 RVA: 0x000A2620 File Offset: 0x000A0820
		public static SoapMonthDay Parse(string value)
		{
			DateTime value2 = DateTime.ParseExact(value, SoapMonthDay._datetimeFormats, null, DateTimeStyles.None);
			return new SoapMonthDay(value2);
		}

		// Token: 0x0600319F RID: 12703 RVA: 0x000A2644 File Offset: 0x000A0844
		public override string ToString()
		{
			return this._value.ToString("--MM-dd", CultureInfo.InvariantCulture);
		}

		// Token: 0x040014F9 RID: 5369
		private static readonly string[] _datetimeFormats = new string[]
		{
			"--MM-dd",
			"--MM-ddzzz"
		};

		// Token: 0x040014FA RID: 5370
		private DateTime _value;
	}
}
