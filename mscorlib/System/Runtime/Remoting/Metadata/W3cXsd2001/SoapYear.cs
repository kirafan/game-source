﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004D0 RID: 1232
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapYear : ISoapXsd
	{
		// Token: 0x060031A8 RID: 12712 RVA: 0x000A26E0 File Offset: 0x000A08E0
		public SoapYear()
		{
		}

		// Token: 0x060031A9 RID: 12713 RVA: 0x000A26E8 File Offset: 0x000A08E8
		public SoapYear(DateTime value)
		{
			this._value = value;
		}

		// Token: 0x060031AA RID: 12714 RVA: 0x000A26F8 File Offset: 0x000A08F8
		public SoapYear(DateTime value, int sign)
		{
			this._value = value;
			this._sign = sign;
		}

		// Token: 0x1700095F RID: 2399
		// (get) Token: 0x060031AC RID: 12716 RVA: 0x000A2750 File Offset: 0x000A0950
		// (set) Token: 0x060031AD RID: 12717 RVA: 0x000A2758 File Offset: 0x000A0958
		public int Sign
		{
			get
			{
				return this._sign;
			}
			set
			{
				this._sign = value;
			}
		}

		// Token: 0x17000960 RID: 2400
		// (get) Token: 0x060031AE RID: 12718 RVA: 0x000A2764 File Offset: 0x000A0964
		// (set) Token: 0x060031AF RID: 12719 RVA: 0x000A276C File Offset: 0x000A096C
		public DateTime Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000961 RID: 2401
		// (get) Token: 0x060031B0 RID: 12720 RVA: 0x000A2778 File Offset: 0x000A0978
		public static string XsdType
		{
			get
			{
				return "gYear";
			}
		}

		// Token: 0x060031B1 RID: 12721 RVA: 0x000A2780 File Offset: 0x000A0980
		public string GetXsdType()
		{
			return SoapYear.XsdType;
		}

		// Token: 0x060031B2 RID: 12722 RVA: 0x000A2788 File Offset: 0x000A0988
		public static SoapYear Parse(string value)
		{
			DateTime value2 = DateTime.ParseExact(value, SoapYear._datetimeFormats, null, DateTimeStyles.None);
			SoapYear soapYear = new SoapYear(value2);
			if (value.StartsWith("-"))
			{
				soapYear.Sign = -1;
			}
			else
			{
				soapYear.Sign = 0;
			}
			return soapYear;
		}

		// Token: 0x060031B3 RID: 12723 RVA: 0x000A27D0 File Offset: 0x000A09D0
		public override string ToString()
		{
			if (this._sign >= 0)
			{
				return this._value.ToString("yyyy", CultureInfo.InvariantCulture);
			}
			return this._value.ToString("'-'yyyy", CultureInfo.InvariantCulture);
		}

		// Token: 0x040014FC RID: 5372
		private static readonly string[] _datetimeFormats = new string[]
		{
			"yyyy",
			"'+'yyyy",
			"'-'yyyy",
			"yyyyzzz",
			"'+'yyyyzzz",
			"'-'yyyyzzz"
		};

		// Token: 0x040014FD RID: 5373
		private int _sign;

		// Token: 0x040014FE RID: 5374
		private DateTime _value;
	}
}
