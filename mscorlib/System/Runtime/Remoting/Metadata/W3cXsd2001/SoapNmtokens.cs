﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004E5 RID: 1253
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNmtokens : ISoapXsd
	{
		// Token: 0x06003257 RID: 12887 RVA: 0x000A3650 File Offset: 0x000A1850
		public SoapNmtokens()
		{
		}

		// Token: 0x06003258 RID: 12888 RVA: 0x000A3658 File Offset: 0x000A1858
		public SoapNmtokens(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x1700098A RID: 2442
		// (get) Token: 0x06003259 RID: 12889 RVA: 0x000A366C File Offset: 0x000A186C
		// (set) Token: 0x0600325A RID: 12890 RVA: 0x000A3674 File Offset: 0x000A1874
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700098B RID: 2443
		// (get) Token: 0x0600325B RID: 12891 RVA: 0x000A3680 File Offset: 0x000A1880
		public static string XsdType
		{
			get
			{
				return "NMTOKENS";
			}
		}

		// Token: 0x0600325C RID: 12892 RVA: 0x000A3688 File Offset: 0x000A1888
		public string GetXsdType()
		{
			return SoapNmtokens.XsdType;
		}

		// Token: 0x0600325D RID: 12893 RVA: 0x000A3690 File Offset: 0x000A1890
		public static SoapNmtokens Parse(string value)
		{
			return new SoapNmtokens(value);
		}

		// Token: 0x0600325E RID: 12894 RVA: 0x000A3698 File Offset: 0x000A1898
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x04001518 RID: 5400
		private string _value;
	}
}
