﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004E0 RID: 1248
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapInteger : ISoapXsd
	{
		// Token: 0x06003229 RID: 12841 RVA: 0x000A33D8 File Offset: 0x000A15D8
		public SoapInteger()
		{
		}

		// Token: 0x0600322A RID: 12842 RVA: 0x000A33E0 File Offset: 0x000A15E0
		public SoapInteger(decimal value)
		{
			this._value = value;
		}

		// Token: 0x1700097E RID: 2430
		// (get) Token: 0x0600322B RID: 12843 RVA: 0x000A33F0 File Offset: 0x000A15F0
		// (set) Token: 0x0600322C RID: 12844 RVA: 0x000A33F8 File Offset: 0x000A15F8
		public decimal Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x1700097F RID: 2431
		// (get) Token: 0x0600322D RID: 12845 RVA: 0x000A3404 File Offset: 0x000A1604
		public static string XsdType
		{
			get
			{
				return "integer";
			}
		}

		// Token: 0x0600322E RID: 12846 RVA: 0x000A340C File Offset: 0x000A160C
		public string GetXsdType()
		{
			return SoapInteger.XsdType;
		}

		// Token: 0x0600322F RID: 12847 RVA: 0x000A3414 File Offset: 0x000A1614
		public static SoapInteger Parse(string value)
		{
			return new SoapInteger(decimal.Parse(value));
		}

		// Token: 0x06003230 RID: 12848 RVA: 0x000A3424 File Offset: 0x000A1624
		public override string ToString()
		{
			return this._value.ToString();
		}

		// Token: 0x04001511 RID: 5393
		private decimal _value;
	}
}
