﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004C6 RID: 1222
	[ComVisible(true)]
	public interface ISoapXsd
	{
		// Token: 0x0600315A RID: 12634
		string GetXsdType();
	}
}
