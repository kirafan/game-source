﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004C8 RID: 1224
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapEntity : ISoapXsd
	{
		// Token: 0x06003163 RID: 12643 RVA: 0x000A21C8 File Offset: 0x000A03C8
		public SoapEntity()
		{
		}

		// Token: 0x06003164 RID: 12644 RVA: 0x000A21D0 File Offset: 0x000A03D0
		public SoapEntity(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x1700094F RID: 2383
		// (get) Token: 0x06003165 RID: 12645 RVA: 0x000A21E4 File Offset: 0x000A03E4
		// (set) Token: 0x06003166 RID: 12646 RVA: 0x000A21EC File Offset: 0x000A03EC
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000950 RID: 2384
		// (get) Token: 0x06003167 RID: 12647 RVA: 0x000A21F8 File Offset: 0x000A03F8
		public static string XsdType
		{
			get
			{
				return "ENTITY";
			}
		}

		// Token: 0x06003168 RID: 12648 RVA: 0x000A2200 File Offset: 0x000A0400
		public string GetXsdType()
		{
			return SoapEntity.XsdType;
		}

		// Token: 0x06003169 RID: 12649 RVA: 0x000A2208 File Offset: 0x000A0408
		public static SoapEntity Parse(string value)
		{
			return new SoapEntity(value);
		}

		// Token: 0x0600316A RID: 12650 RVA: 0x000A2210 File Offset: 0x000A0410
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x040014F1 RID: 5361
		private string _value;
	}
}
