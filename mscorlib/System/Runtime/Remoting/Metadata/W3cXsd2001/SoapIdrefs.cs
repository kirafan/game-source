﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004DC RID: 1244
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapIdrefs : ISoapXsd
	{
		// Token: 0x0600320D RID: 12813 RVA: 0x000A2E38 File Offset: 0x000A1038
		public SoapIdrefs()
		{
		}

		// Token: 0x0600320E RID: 12814 RVA: 0x000A2E40 File Offset: 0x000A1040
		public SoapIdrefs(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x17000977 RID: 2423
		// (get) Token: 0x0600320F RID: 12815 RVA: 0x000A2E54 File Offset: 0x000A1054
		// (set) Token: 0x06003210 RID: 12816 RVA: 0x000A2E5C File Offset: 0x000A105C
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000978 RID: 2424
		// (get) Token: 0x06003211 RID: 12817 RVA: 0x000A2E68 File Offset: 0x000A1068
		public static string XsdType
		{
			get
			{
				return "IDREFS";
			}
		}

		// Token: 0x06003212 RID: 12818 RVA: 0x000A2E70 File Offset: 0x000A1070
		public string GetXsdType()
		{
			return SoapIdrefs.XsdType;
		}

		// Token: 0x06003213 RID: 12819 RVA: 0x000A2E78 File Offset: 0x000A1078
		public static SoapIdrefs Parse(string value)
		{
			return new SoapIdrefs(value);
		}

		// Token: 0x06003214 RID: 12820 RVA: 0x000A2E80 File Offset: 0x000A1080
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x0400150E RID: 5390
		private string _value;
	}
}
