﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004E1 RID: 1249
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNmtoken : ISoapXsd
	{
		// Token: 0x06003231 RID: 12849 RVA: 0x000A3434 File Offset: 0x000A1634
		public SoapNmtoken()
		{
		}

		// Token: 0x06003232 RID: 12850 RVA: 0x000A343C File Offset: 0x000A163C
		public SoapNmtoken(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x17000980 RID: 2432
		// (get) Token: 0x06003233 RID: 12851 RVA: 0x000A3450 File Offset: 0x000A1650
		// (set) Token: 0x06003234 RID: 12852 RVA: 0x000A3458 File Offset: 0x000A1658
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000981 RID: 2433
		// (get) Token: 0x06003235 RID: 12853 RVA: 0x000A3464 File Offset: 0x000A1664
		public static string XsdType
		{
			get
			{
				return "NMTOKEN";
			}
		}

		// Token: 0x06003236 RID: 12854 RVA: 0x000A346C File Offset: 0x000A166C
		public string GetXsdType()
		{
			return SoapNmtoken.XsdType;
		}

		// Token: 0x06003237 RID: 12855 RVA: 0x000A3474 File Offset: 0x000A1674
		public static SoapNmtoken Parse(string value)
		{
			return new SoapNmtoken(value);
		}

		// Token: 0x06003238 RID: 12856 RVA: 0x000A347C File Offset: 0x000A167C
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x04001512 RID: 5394
		private string _value;
	}
}
