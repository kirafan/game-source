﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004DB RID: 1243
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapDay : ISoapXsd
	{
		// Token: 0x06003204 RID: 12804 RVA: 0x000A2DA0 File Offset: 0x000A0FA0
		public SoapDay()
		{
		}

		// Token: 0x06003205 RID: 12805 RVA: 0x000A2DA8 File Offset: 0x000A0FA8
		public SoapDay(DateTime value)
		{
			this._value = value;
		}

		// Token: 0x17000975 RID: 2421
		// (get) Token: 0x06003207 RID: 12807 RVA: 0x000A2DD8 File Offset: 0x000A0FD8
		// (set) Token: 0x06003208 RID: 12808 RVA: 0x000A2DE0 File Offset: 0x000A0FE0
		public DateTime Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000976 RID: 2422
		// (get) Token: 0x06003209 RID: 12809 RVA: 0x000A2DEC File Offset: 0x000A0FEC
		public static string XsdType
		{
			get
			{
				return "gDay";
			}
		}

		// Token: 0x0600320A RID: 12810 RVA: 0x000A2DF4 File Offset: 0x000A0FF4
		public string GetXsdType()
		{
			return SoapDay.XsdType;
		}

		// Token: 0x0600320B RID: 12811 RVA: 0x000A2DFC File Offset: 0x000A0FFC
		public static SoapDay Parse(string value)
		{
			DateTime value2 = DateTime.ParseExact(value, SoapDay._datetimeFormats, null, DateTimeStyles.None);
			return new SoapDay(value2);
		}

		// Token: 0x0600320C RID: 12812 RVA: 0x000A2E20 File Offset: 0x000A1020
		public override string ToString()
		{
			return this._value.ToString("---dd", CultureInfo.InvariantCulture);
		}

		// Token: 0x0400150C RID: 5388
		private static readonly string[] _datetimeFormats = new string[]
		{
			"---dd",
			"---ddzzz"
		};

		// Token: 0x0400150D RID: 5389
		private DateTime _value;
	}
}
