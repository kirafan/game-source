﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004D4 RID: 1236
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapName : ISoapXsd
	{
		// Token: 0x060031CB RID: 12747 RVA: 0x000A29C4 File Offset: 0x000A0BC4
		public SoapName()
		{
		}

		// Token: 0x060031CC RID: 12748 RVA: 0x000A29CC File Offset: 0x000A0BCC
		public SoapName(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x17000967 RID: 2407
		// (get) Token: 0x060031CD RID: 12749 RVA: 0x000A29E0 File Offset: 0x000A0BE0
		// (set) Token: 0x060031CE RID: 12750 RVA: 0x000A29E8 File Offset: 0x000A0BE8
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000968 RID: 2408
		// (get) Token: 0x060031CF RID: 12751 RVA: 0x000A29F4 File Offset: 0x000A0BF4
		public static string XsdType
		{
			get
			{
				return "Name";
			}
		}

		// Token: 0x060031D0 RID: 12752 RVA: 0x000A29FC File Offset: 0x000A0BFC
		public string GetXsdType()
		{
			return SoapName.XsdType;
		}

		// Token: 0x060031D1 RID: 12753 RVA: 0x000A2A04 File Offset: 0x000A0C04
		public static SoapName Parse(string value)
		{
			return new SoapName(value);
		}

		// Token: 0x060031D2 RID: 12754 RVA: 0x000A2A0C File Offset: 0x000A0C0C
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x04001503 RID: 5379
		private string _value;
	}
}
