﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004D8 RID: 1240
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapIdref : ISoapXsd
	{
		// Token: 0x060031EC RID: 12780 RVA: 0x000A2CB4 File Offset: 0x000A0EB4
		public SoapIdref()
		{
		}

		// Token: 0x060031ED RID: 12781 RVA: 0x000A2CBC File Offset: 0x000A0EBC
		public SoapIdref(string value)
		{
			this._value = SoapHelper.Normalize(value);
		}

		// Token: 0x1700096F RID: 2415
		// (get) Token: 0x060031EE RID: 12782 RVA: 0x000A2CD0 File Offset: 0x000A0ED0
		// (set) Token: 0x060031EF RID: 12783 RVA: 0x000A2CD8 File Offset: 0x000A0ED8
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000970 RID: 2416
		// (get) Token: 0x060031F0 RID: 12784 RVA: 0x000A2CE4 File Offset: 0x000A0EE4
		public static string XsdType
		{
			get
			{
				return "IDREF";
			}
		}

		// Token: 0x060031F1 RID: 12785 RVA: 0x000A2CEC File Offset: 0x000A0EEC
		public string GetXsdType()
		{
			return SoapIdref.XsdType;
		}

		// Token: 0x060031F2 RID: 12786 RVA: 0x000A2CF4 File Offset: 0x000A0EF4
		public static SoapIdref Parse(string value)
		{
			return new SoapIdref(value);
		}

		// Token: 0x060031F3 RID: 12787 RVA: 0x000A2CFC File Offset: 0x000A0EFC
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x04001509 RID: 5385
		private string _value;
	}
}
