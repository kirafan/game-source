﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata.W3cXsd2001
{
	// Token: 0x020004DA RID: 1242
	[ComVisible(true)]
	[Serializable]
	public sealed class SoapNotation : ISoapXsd
	{
		// Token: 0x060031FC RID: 12796 RVA: 0x000A2D54 File Offset: 0x000A0F54
		public SoapNotation()
		{
		}

		// Token: 0x060031FD RID: 12797 RVA: 0x000A2D5C File Offset: 0x000A0F5C
		public SoapNotation(string value)
		{
			this._value = value;
		}

		// Token: 0x17000973 RID: 2419
		// (get) Token: 0x060031FE RID: 12798 RVA: 0x000A2D6C File Offset: 0x000A0F6C
		// (set) Token: 0x060031FF RID: 12799 RVA: 0x000A2D74 File Offset: 0x000A0F74
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x17000974 RID: 2420
		// (get) Token: 0x06003200 RID: 12800 RVA: 0x000A2D80 File Offset: 0x000A0F80
		public static string XsdType
		{
			get
			{
				return "NOTATION";
			}
		}

		// Token: 0x06003201 RID: 12801 RVA: 0x000A2D88 File Offset: 0x000A0F88
		public string GetXsdType()
		{
			return SoapNotation.XsdType;
		}

		// Token: 0x06003202 RID: 12802 RVA: 0x000A2D90 File Offset: 0x000A0F90
		public static SoapNotation Parse(string value)
		{
			return new SoapNotation(value);
		}

		// Token: 0x06003203 RID: 12803 RVA: 0x000A2D98 File Offset: 0x000A0F98
		public override string ToString()
		{
			return this._value;
		}

		// Token: 0x0400150B RID: 5387
		private string _value;
	}
}
