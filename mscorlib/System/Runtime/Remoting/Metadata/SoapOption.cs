﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	// Token: 0x020004C2 RID: 1218
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum SoapOption
	{
		// Token: 0x040014DD RID: 5341
		None = 0,
		// Token: 0x040014DE RID: 5342
		AlwaysIncludeTypes = 1,
		// Token: 0x040014DF RID: 5343
		XsdString = 2,
		// Token: 0x040014E0 RID: 5344
		EmbedAll = 4,
		// Token: 0x040014E1 RID: 5345
		Option1 = 8,
		// Token: 0x040014E2 RID: 5346
		Option2 = 16
	}
}
