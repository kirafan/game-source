﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	// Token: 0x020004C4 RID: 1220
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface)]
	public sealed class SoapTypeAttribute : SoapAttribute
	{
		// Token: 0x17000944 RID: 2372
		// (get) Token: 0x06003149 RID: 12617 RVA: 0x000A2004 File Offset: 0x000A0204
		// (set) Token: 0x0600314A RID: 12618 RVA: 0x000A200C File Offset: 0x000A020C
		public SoapOption SoapOptions
		{
			get
			{
				return this._soapOption;
			}
			set
			{
				this._soapOption = value;
			}
		}

		// Token: 0x17000945 RID: 2373
		// (get) Token: 0x0600314B RID: 12619 RVA: 0x000A2018 File Offset: 0x000A0218
		// (set) Token: 0x0600314C RID: 12620 RVA: 0x000A2020 File Offset: 0x000A0220
		public override bool UseAttribute
		{
			get
			{
				return this._useAttribute;
			}
			set
			{
				this._useAttribute = value;
			}
		}

		// Token: 0x17000946 RID: 2374
		// (get) Token: 0x0600314D RID: 12621 RVA: 0x000A202C File Offset: 0x000A022C
		// (set) Token: 0x0600314E RID: 12622 RVA: 0x000A2034 File Offset: 0x000A0234
		public string XmlElementName
		{
			get
			{
				return this._xmlElementName;
			}
			set
			{
				this._isElement = (value != null);
				this._xmlElementName = value;
			}
		}

		// Token: 0x17000947 RID: 2375
		// (get) Token: 0x0600314F RID: 12623 RVA: 0x000A204C File Offset: 0x000A024C
		// (set) Token: 0x06003150 RID: 12624 RVA: 0x000A2054 File Offset: 0x000A0254
		public XmlFieldOrderOption XmlFieldOrder
		{
			get
			{
				return this._xmlFieldOrder;
			}
			set
			{
				this._xmlFieldOrder = value;
			}
		}

		// Token: 0x17000948 RID: 2376
		// (get) Token: 0x06003151 RID: 12625 RVA: 0x000A2060 File Offset: 0x000A0260
		// (set) Token: 0x06003152 RID: 12626 RVA: 0x000A2068 File Offset: 0x000A0268
		public override string XmlNamespace
		{
			get
			{
				return this._xmlNamespace;
			}
			set
			{
				this._isElement = (value != null);
				this._xmlNamespace = value;
			}
		}

		// Token: 0x17000949 RID: 2377
		// (get) Token: 0x06003153 RID: 12627 RVA: 0x000A2080 File Offset: 0x000A0280
		// (set) Token: 0x06003154 RID: 12628 RVA: 0x000A2088 File Offset: 0x000A0288
		public string XmlTypeName
		{
			get
			{
				return this._xmlTypeName;
			}
			set
			{
				this._isType = (value != null);
				this._xmlTypeName = value;
			}
		}

		// Token: 0x1700094A RID: 2378
		// (get) Token: 0x06003155 RID: 12629 RVA: 0x000A20A0 File Offset: 0x000A02A0
		// (set) Token: 0x06003156 RID: 12630 RVA: 0x000A20A8 File Offset: 0x000A02A8
		public string XmlTypeNamespace
		{
			get
			{
				return this._xmlTypeNamespace;
			}
			set
			{
				this._isType = (value != null);
				this._xmlTypeNamespace = value;
			}
		}

		// Token: 0x1700094B RID: 2379
		// (get) Token: 0x06003157 RID: 12631 RVA: 0x000A20C0 File Offset: 0x000A02C0
		internal bool IsInteropXmlElement
		{
			get
			{
				return this._isElement;
			}
		}

		// Token: 0x1700094C RID: 2380
		// (get) Token: 0x06003158 RID: 12632 RVA: 0x000A20C8 File Offset: 0x000A02C8
		internal bool IsInteropXmlType
		{
			get
			{
				return this._isType;
			}
		}

		// Token: 0x06003159 RID: 12633 RVA: 0x000A20D0 File Offset: 0x000A02D0
		internal override void SetReflectionObject(object reflectionObject)
		{
			Type type = (Type)reflectionObject;
			if (this._xmlElementName == null)
			{
				this._xmlElementName = type.Name;
			}
			if (this._xmlTypeName == null)
			{
				this._xmlTypeName = type.Name;
			}
			if (this._xmlTypeNamespace == null)
			{
				string assemblyName;
				if (type.Assembly == typeof(object).Assembly)
				{
					assemblyName = string.Empty;
				}
				else
				{
					assemblyName = type.Assembly.GetName().Name;
				}
				this._xmlTypeNamespace = SoapServices.CodeXmlNamespaceForClrTypeNamespace(type.Namespace, assemblyName);
			}
			if (this._xmlNamespace == null)
			{
				this._xmlNamespace = this._xmlTypeNamespace;
			}
		}

		// Token: 0x040014E3 RID: 5347
		private SoapOption _soapOption;

		// Token: 0x040014E4 RID: 5348
		private bool _useAttribute;

		// Token: 0x040014E5 RID: 5349
		private string _xmlElementName;

		// Token: 0x040014E6 RID: 5350
		private XmlFieldOrderOption _xmlFieldOrder;

		// Token: 0x040014E7 RID: 5351
		private string _xmlNamespace;

		// Token: 0x040014E8 RID: 5352
		private string _xmlTypeName;

		// Token: 0x040014E9 RID: 5353
		private string _xmlTypeNamespace;

		// Token: 0x040014EA RID: 5354
		private bool _isType;

		// Token: 0x040014EB RID: 5355
		private bool _isElement;
	}
}
