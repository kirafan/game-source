﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	// Token: 0x020004C1 RID: 1217
	[AttributeUsage(AttributeTargets.Method)]
	[ComVisible(true)]
	public sealed class SoapMethodAttribute : SoapAttribute
	{
		// Token: 0x1700093E RID: 2366
		// (get) Token: 0x0600313A RID: 12602 RVA: 0x000A1EDC File Offset: 0x000A00DC
		// (set) Token: 0x0600313B RID: 12603 RVA: 0x000A1EE4 File Offset: 0x000A00E4
		public string ResponseXmlElementName
		{
			get
			{
				return this._responseElement;
			}
			set
			{
				this._responseElement = value;
			}
		}

		// Token: 0x1700093F RID: 2367
		// (get) Token: 0x0600313C RID: 12604 RVA: 0x000A1EF0 File Offset: 0x000A00F0
		// (set) Token: 0x0600313D RID: 12605 RVA: 0x000A1EF8 File Offset: 0x000A00F8
		public string ResponseXmlNamespace
		{
			get
			{
				return this._responseNamespace;
			}
			set
			{
				this._responseNamespace = value;
			}
		}

		// Token: 0x17000940 RID: 2368
		// (get) Token: 0x0600313E RID: 12606 RVA: 0x000A1F04 File Offset: 0x000A0104
		// (set) Token: 0x0600313F RID: 12607 RVA: 0x000A1F0C File Offset: 0x000A010C
		public string ReturnXmlElementName
		{
			get
			{
				return this._returnElement;
			}
			set
			{
				this._returnElement = value;
			}
		}

		// Token: 0x17000941 RID: 2369
		// (get) Token: 0x06003140 RID: 12608 RVA: 0x000A1F18 File Offset: 0x000A0118
		// (set) Token: 0x06003141 RID: 12609 RVA: 0x000A1F20 File Offset: 0x000A0120
		public string SoapAction
		{
			get
			{
				return this._soapAction;
			}
			set
			{
				this._soapAction = value;
			}
		}

		// Token: 0x17000942 RID: 2370
		// (get) Token: 0x06003142 RID: 12610 RVA: 0x000A1F2C File Offset: 0x000A012C
		// (set) Token: 0x06003143 RID: 12611 RVA: 0x000A1F34 File Offset: 0x000A0134
		public override bool UseAttribute
		{
			get
			{
				return this._useAttribute;
			}
			set
			{
				this._useAttribute = value;
			}
		}

		// Token: 0x17000943 RID: 2371
		// (get) Token: 0x06003144 RID: 12612 RVA: 0x000A1F40 File Offset: 0x000A0140
		// (set) Token: 0x06003145 RID: 12613 RVA: 0x000A1F48 File Offset: 0x000A0148
		public override string XmlNamespace
		{
			get
			{
				return this._namespace;
			}
			set
			{
				this._namespace = value;
			}
		}

		// Token: 0x06003146 RID: 12614 RVA: 0x000A1F54 File Offset: 0x000A0154
		internal override void SetReflectionObject(object reflectionObject)
		{
			MethodBase methodBase = (MethodBase)reflectionObject;
			if (this._responseElement == null)
			{
				this._responseElement = methodBase.Name + "Response";
			}
			if (this._responseNamespace == null)
			{
				this._responseNamespace = SoapServices.GetXmlNamespaceForMethodResponse(methodBase);
			}
			if (this._returnElement == null)
			{
				this._returnElement = "return";
			}
			if (this._soapAction == null)
			{
				this._soapAction = SoapServices.GetXmlNamespaceForMethodCall(methodBase) + "#" + methodBase.Name;
			}
			if (this._namespace == null)
			{
				this._namespace = SoapServices.GetXmlNamespaceForMethodCall(methodBase);
			}
		}

		// Token: 0x040014D6 RID: 5334
		private string _responseElement;

		// Token: 0x040014D7 RID: 5335
		private string _responseNamespace;

		// Token: 0x040014D8 RID: 5336
		private string _returnElement;

		// Token: 0x040014D9 RID: 5337
		private string _soapAction;

		// Token: 0x040014DA RID: 5338
		private bool _useAttribute;

		// Token: 0x040014DB RID: 5339
		private string _namespace;
	}
}
