﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	// Token: 0x020004C3 RID: 1219
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Parameter)]
	public sealed class SoapParameterAttribute : SoapAttribute
	{
	}
}
