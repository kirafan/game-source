﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Metadata
{
	// Token: 0x020004C0 RID: 1216
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field)]
	public sealed class SoapFieldAttribute : SoapAttribute
	{
		// Token: 0x1700093C RID: 2364
		// (get) Token: 0x06003133 RID: 12595 RVA: 0x000A1E6C File Offset: 0x000A006C
		// (set) Token: 0x06003134 RID: 12596 RVA: 0x000A1E74 File Offset: 0x000A0074
		public int Order
		{
			get
			{
				return this._order;
			}
			set
			{
				this._order = value;
			}
		}

		// Token: 0x1700093D RID: 2365
		// (get) Token: 0x06003135 RID: 12597 RVA: 0x000A1E80 File Offset: 0x000A0080
		// (set) Token: 0x06003136 RID: 12598 RVA: 0x000A1E88 File Offset: 0x000A0088
		public string XmlElementName
		{
			get
			{
				return this._elementName;
			}
			set
			{
				this._isElement = (value != null);
				this._elementName = value;
			}
		}

		// Token: 0x06003137 RID: 12599 RVA: 0x000A1EA0 File Offset: 0x000A00A0
		public bool IsInteropXmlElement()
		{
			return this._isElement;
		}

		// Token: 0x06003138 RID: 12600 RVA: 0x000A1EA8 File Offset: 0x000A00A8
		internal override void SetReflectionObject(object reflectionObject)
		{
			FieldInfo fieldInfo = (FieldInfo)reflectionObject;
			if (this._elementName == null)
			{
				this._elementName = fieldInfo.Name;
			}
		}

		// Token: 0x040014D3 RID: 5331
		private int _order;

		// Token: 0x040014D4 RID: 5332
		private string _elementName;

		// Token: 0x040014D5 RID: 5333
		private bool _isElement;
	}
}
