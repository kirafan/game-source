﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200048F RID: 1167
	[ComVisible(true)]
	[Serializable]
	public sealed class CallContext
	{
		// Token: 0x06002F94 RID: 12180 RVA: 0x0009D604 File Offset: 0x0009B804
		private CallContext()
		{
		}

		// Token: 0x17000893 RID: 2195
		// (get) Token: 0x06002F95 RID: 12181 RVA: 0x0009D60C File Offset: 0x0009B80C
		// (set) Token: 0x06002F96 RID: 12182 RVA: 0x0009D614 File Offset: 0x0009B814
		public static object HostContext
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x06002F97 RID: 12183 RVA: 0x0009D61C File Offset: 0x0009B81C
		public static void FreeNamedDataSlot(string name)
		{
			CallContext.Datastore.Remove(name);
		}

		// Token: 0x06002F98 RID: 12184 RVA: 0x0009D62C File Offset: 0x0009B82C
		public static object GetData(string name)
		{
			return CallContext.Datastore[name];
		}

		// Token: 0x06002F99 RID: 12185 RVA: 0x0009D63C File Offset: 0x0009B83C
		public static void SetData(string name, object data)
		{
			CallContext.Datastore[name] = data;
		}

		// Token: 0x06002F9A RID: 12186 RVA: 0x0009D64C File Offset: 0x0009B84C
		[MonoTODO]
		public static object LogicalGetData(string name)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002F9B RID: 12187 RVA: 0x0009D654 File Offset: 0x0009B854
		[MonoTODO]
		public static void LogicalSetData(string name, object data)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002F9C RID: 12188 RVA: 0x0009D65C File Offset: 0x0009B85C
		public static Header[] GetHeaders()
		{
			return CallContext.Headers;
		}

		// Token: 0x06002F9D RID: 12189 RVA: 0x0009D664 File Offset: 0x0009B864
		public static void SetHeaders(Header[] headers)
		{
			CallContext.Headers = headers;
		}

		// Token: 0x06002F9E RID: 12190 RVA: 0x0009D66C File Offset: 0x0009B86C
		internal static LogicalCallContext CreateLogicalCallContext(bool createEmpty)
		{
			LogicalCallContext logicalCallContext = null;
			if (CallContext.datastore != null)
			{
				foreach (object obj in CallContext.datastore)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					if (dictionaryEntry.Value is ILogicalThreadAffinative)
					{
						if (logicalCallContext == null)
						{
							logicalCallContext = new LogicalCallContext();
						}
						logicalCallContext.SetData((string)dictionaryEntry.Key, dictionaryEntry.Value);
					}
				}
			}
			if (logicalCallContext == null && createEmpty)
			{
				return new LogicalCallContext();
			}
			return logicalCallContext;
		}

		// Token: 0x06002F9F RID: 12191 RVA: 0x0009D728 File Offset: 0x0009B928
		internal static object SetCurrentCallContext(LogicalCallContext ctx)
		{
			object result = CallContext.datastore;
			if (ctx != null && ctx.HasInfo)
			{
				CallContext.datastore = (Hashtable)ctx.Datastore.Clone();
			}
			else
			{
				CallContext.datastore = null;
			}
			return result;
		}

		// Token: 0x06002FA0 RID: 12192 RVA: 0x0009D770 File Offset: 0x0009B970
		internal static void UpdateCurrentCallContext(LogicalCallContext ctx)
		{
			Hashtable hashtable = ctx.Datastore;
			foreach (object obj in hashtable)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
				CallContext.SetData((string)dictionaryEntry.Key, dictionaryEntry.Value);
			}
		}

		// Token: 0x06002FA1 RID: 12193 RVA: 0x0009D7F4 File Offset: 0x0009B9F4
		internal static void RestoreCallContext(object oldContext)
		{
			CallContext.datastore = (Hashtable)oldContext;
		}

		// Token: 0x17000894 RID: 2196
		// (get) Token: 0x06002FA2 RID: 12194 RVA: 0x0009D804 File Offset: 0x0009BA04
		private static Hashtable Datastore
		{
			get
			{
				Hashtable hashtable = CallContext.datastore;
				if (hashtable == null)
				{
					return CallContext.datastore = new Hashtable();
				}
				return hashtable;
			}
		}

		// Token: 0x04001449 RID: 5193
		[ThreadStatic]
		private static Header[] Headers;

		// Token: 0x0400144A RID: 5194
		[ThreadStatic]
		private static Hashtable datastore;
	}
}
