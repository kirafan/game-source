﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020006F7 RID: 1783
	// (Invoke) Token: 0x060043D0 RID: 17360
	[ComVisible(true)]
	public delegate bool MessageSurrogateFilter(string key, object value);
}
