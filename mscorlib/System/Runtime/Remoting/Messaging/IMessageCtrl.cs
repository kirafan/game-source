﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200049B RID: 1179
	[ComVisible(true)]
	public interface IMessageCtrl
	{
		// Token: 0x06002FE6 RID: 12262
		void Cancel(int msToCancel);
	}
}
