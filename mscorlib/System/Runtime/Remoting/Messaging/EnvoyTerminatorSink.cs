﻿using System;
using System.Threading;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000496 RID: 1174
	[Serializable]
	internal class EnvoyTerminatorSink : IMessageSink
	{
		// Token: 0x06002FC9 RID: 12233 RVA: 0x0009DED8 File Offset: 0x0009C0D8
		public IMessage SyncProcessMessage(IMessage msg)
		{
			return Thread.CurrentContext.GetClientContextSinkChain().SyncProcessMessage(msg);
		}

		// Token: 0x06002FCA RID: 12234 RVA: 0x0009DEEC File Offset: 0x0009C0EC
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			return Thread.CurrentContext.GetClientContextSinkChain().AsyncProcessMessage(msg, replySink);
		}

		// Token: 0x170008A0 RID: 2208
		// (get) Token: 0x06002FCB RID: 12235 RVA: 0x0009DF00 File Offset: 0x0009C100
		public IMessageSink NextSink
		{
			get
			{
				return null;
			}
		}

		// Token: 0x04001459 RID: 5209
		public static EnvoyTerminatorSink Instance = new EnvoyTerminatorSink();
	}
}
