﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200049C RID: 1180
	[ComVisible(true)]
	public interface IMessageSink
	{
		// Token: 0x06002FE7 RID: 12263
		IMessage SyncProcessMessage(IMessage msg);

		// Token: 0x06002FE8 RID: 12264
		IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink);

		// Token: 0x170008B0 RID: 2224
		// (get) Token: 0x06002FE9 RID: 12265
		IMessageSink NextSink { get; }
	}
}
