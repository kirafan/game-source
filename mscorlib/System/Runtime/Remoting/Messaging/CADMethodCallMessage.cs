﻿using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting.Channels;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004BD RID: 1213
	internal class CADMethodCallMessage : CADMessageBase
	{
		// Token: 0x0600311B RID: 12571 RVA: 0x000A1990 File Offset: 0x0009FB90
		internal CADMethodCallMessage(IMethodCallMessage callMsg)
		{
			this._uri = callMsg.Uri;
			this.MethodHandle = callMsg.MethodBase.MethodHandle;
			this.FullTypeName = callMsg.MethodBase.DeclaringType.AssemblyQualifiedName;
			ArrayList arrayList = null;
			this._propertyCount = CADMessageBase.MarshalProperties(callMsg.Properties, ref arrayList);
			this._args = base.MarshalArguments(callMsg.Args, ref arrayList);
			base.SaveLogicalCallContext(callMsg, ref arrayList);
			if (arrayList != null)
			{
				MemoryStream memoryStream = CADSerializer.SerializeObject(arrayList.ToArray());
				this._serializedArgs = memoryStream.GetBuffer();
			}
		}

		// Token: 0x17000936 RID: 2358
		// (get) Token: 0x0600311C RID: 12572 RVA: 0x000A1A28 File Offset: 0x0009FC28
		internal string Uri
		{
			get
			{
				return this._uri;
			}
		}

		// Token: 0x0600311D RID: 12573 RVA: 0x000A1A30 File Offset: 0x0009FC30
		internal static CADMethodCallMessage Create(IMessage callMsg)
		{
			IMethodCallMessage methodCallMessage = callMsg as IMethodCallMessage;
			if (methodCallMessage == null)
			{
				return null;
			}
			return new CADMethodCallMessage(methodCallMessage);
		}

		// Token: 0x0600311E RID: 12574 RVA: 0x000A1A54 File Offset: 0x0009FC54
		internal ArrayList GetArguments()
		{
			ArrayList result = null;
			if (this._serializedArgs != null)
			{
				object[] c = (object[])CADSerializer.DeserializeObject(new MemoryStream(this._serializedArgs));
				result = new ArrayList(c);
				this._serializedArgs = null;
			}
			return result;
		}

		// Token: 0x0600311F RID: 12575 RVA: 0x000A1A94 File Offset: 0x0009FC94
		internal object[] GetArgs(ArrayList args)
		{
			return base.UnmarshalArguments(this._args, args);
		}

		// Token: 0x17000937 RID: 2359
		// (get) Token: 0x06003120 RID: 12576 RVA: 0x000A1AA4 File Offset: 0x0009FCA4
		internal int PropertiesCount
		{
			get
			{
				return this._propertyCount;
			}
		}

		// Token: 0x06003121 RID: 12577 RVA: 0x000A1AAC File Offset: 0x0009FCAC
		private static Type[] GetSignature(MethodBase methodBase, bool load)
		{
			ParameterInfo[] parameters = methodBase.GetParameters();
			Type[] array = new Type[parameters.Length];
			for (int i = 0; i < parameters.Length; i++)
			{
				if (load)
				{
					array[i] = Type.GetType(parameters[i].ParameterType.AssemblyQualifiedName, true);
				}
				else
				{
					array[i] = parameters[i].ParameterType;
				}
			}
			return array;
		}

		// Token: 0x06003122 RID: 12578 RVA: 0x000A1B0C File Offset: 0x0009FD0C
		internal MethodBase GetMethod()
		{
			Type type = Type.GetType(this.FullTypeName);
			MethodBase methodBase;
			if (type.IsGenericType || type.IsGenericTypeDefinition)
			{
				methodBase = MethodBase.GetMethodFromHandleNoGenericCheck(this.MethodHandle);
			}
			else
			{
				methodBase = MethodBase.GetMethodFromHandle(this.MethodHandle);
			}
			if (type == methodBase.DeclaringType)
			{
				return methodBase;
			}
			Type[] signature = CADMethodCallMessage.GetSignature(methodBase, true);
			if (methodBase.IsGenericMethod)
			{
				MethodBase[] methods = type.GetMethods(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
				Type[] genericArguments = methodBase.GetGenericArguments();
				foreach (MethodBase methodBase2 in methods)
				{
					if (methodBase2.IsGenericMethod && !(methodBase2.Name != methodBase.Name))
					{
						Type[] genericArguments2 = methodBase2.GetGenericArguments();
						if (genericArguments.Length == genericArguments2.Length)
						{
							MethodInfo methodInfo = ((MethodInfo)methodBase2).MakeGenericMethod(genericArguments);
							Type[] signature2 = CADMethodCallMessage.GetSignature(methodInfo, false);
							if (signature2.Length == signature.Length)
							{
								bool flag = false;
								for (int j = signature2.Length - 1; j >= 0; j--)
								{
									if (signature2[j] != signature[j])
									{
										flag = true;
										break;
									}
								}
								if (!flag)
								{
									return methodInfo;
								}
							}
						}
					}
				}
				return methodBase;
			}
			MethodBase method = type.GetMethod(methodBase.Name, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, signature, null);
			if (method == null)
			{
				throw new RemotingException(string.Concat(new object[]
				{
					"Method '",
					methodBase.Name,
					"' not found in type '",
					type,
					"'"
				}));
			}
			return method;
		}

		// Token: 0x040014CA RID: 5322
		private string _uri;

		// Token: 0x040014CB RID: 5323
		internal RuntimeMethodHandle MethodHandle;

		// Token: 0x040014CC RID: 5324
		internal string FullTypeName;
	}
}
