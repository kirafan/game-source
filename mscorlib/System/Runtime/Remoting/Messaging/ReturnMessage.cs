﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004B5 RID: 1205
	[ComVisible(true)]
	public class ReturnMessage : IInternalMessage, IMessage, IMethodMessage, IMethodReturnMessage
	{
		// Token: 0x060030E2 RID: 12514 RVA: 0x000A0B84 File Offset: 0x0009ED84
		public ReturnMessage(object ret, object[] outArgs, int outArgsCount, LogicalCallContext callCtx, IMethodCallMessage mcm)
		{
			this._returnValue = ret;
			this._args = outArgs;
			this._outArgsCount = outArgsCount;
			this._callCtx = callCtx;
			if (mcm != null)
			{
				this._uri = mcm.Uri;
				this._methodBase = mcm.MethodBase;
			}
			if (this._args == null)
			{
				this._args = new object[outArgsCount];
			}
		}

		// Token: 0x060030E3 RID: 12515 RVA: 0x000A0BEC File Offset: 0x0009EDEC
		public ReturnMessage(Exception e, IMethodCallMessage mcm)
		{
			this._exception = e;
			if (mcm != null)
			{
				this._methodBase = mcm.MethodBase;
				this._callCtx = mcm.LogicalCallContext;
			}
			this._args = new object[0];
		}

		// Token: 0x17000920 RID: 2336
		// (get) Token: 0x060030E4 RID: 12516 RVA: 0x000A0C30 File Offset: 0x0009EE30
		// (set) Token: 0x060030E5 RID: 12517 RVA: 0x000A0C38 File Offset: 0x0009EE38
		string IInternalMessage.Uri
		{
			get
			{
				return this.Uri;
			}
			set
			{
				this.Uri = value;
			}
		}

		// Token: 0x17000921 RID: 2337
		// (get) Token: 0x060030E6 RID: 12518 RVA: 0x000A0C44 File Offset: 0x0009EE44
		// (set) Token: 0x060030E7 RID: 12519 RVA: 0x000A0C4C File Offset: 0x0009EE4C
		Identity IInternalMessage.TargetIdentity
		{
			get
			{
				return this._targetIdentity;
			}
			set
			{
				this._targetIdentity = value;
			}
		}

		// Token: 0x17000922 RID: 2338
		// (get) Token: 0x060030E8 RID: 12520 RVA: 0x000A0C58 File Offset: 0x0009EE58
		public int ArgCount
		{
			get
			{
				return this._args.Length;
			}
		}

		// Token: 0x17000923 RID: 2339
		// (get) Token: 0x060030E9 RID: 12521 RVA: 0x000A0C64 File Offset: 0x0009EE64
		public object[] Args
		{
			get
			{
				return this._args;
			}
		}

		// Token: 0x17000924 RID: 2340
		// (get) Token: 0x060030EA RID: 12522 RVA: 0x000A0C6C File Offset: 0x0009EE6C
		public bool HasVarArgs
		{
			get
			{
				return this._methodBase != null && (this._methodBase.CallingConvention | CallingConventions.VarArgs) != (CallingConventions)0;
			}
		}

		// Token: 0x17000925 RID: 2341
		// (get) Token: 0x060030EB RID: 12523 RVA: 0x000A0C9C File Offset: 0x0009EE9C
		public LogicalCallContext LogicalCallContext
		{
			get
			{
				if (this._callCtx == null)
				{
					this._callCtx = new LogicalCallContext();
				}
				return this._callCtx;
			}
		}

		// Token: 0x17000926 RID: 2342
		// (get) Token: 0x060030EC RID: 12524 RVA: 0x000A0CBC File Offset: 0x0009EEBC
		public MethodBase MethodBase
		{
			get
			{
				return this._methodBase;
			}
		}

		// Token: 0x17000927 RID: 2343
		// (get) Token: 0x060030ED RID: 12525 RVA: 0x000A0CC4 File Offset: 0x0009EEC4
		public string MethodName
		{
			get
			{
				if (this._methodBase != null && this._methodName == null)
				{
					this._methodName = this._methodBase.Name;
				}
				return this._methodName;
			}
		}

		// Token: 0x17000928 RID: 2344
		// (get) Token: 0x060030EE RID: 12526 RVA: 0x000A0CF4 File Offset: 0x0009EEF4
		public object MethodSignature
		{
			get
			{
				if (this._methodBase != null && this._methodSignature == null)
				{
					ParameterInfo[] parameters = this._methodBase.GetParameters();
					this._methodSignature = new Type[parameters.Length];
					for (int i = 0; i < parameters.Length; i++)
					{
						this._methodSignature[i] = parameters[i].ParameterType;
					}
				}
				return this._methodSignature;
			}
		}

		// Token: 0x17000929 RID: 2345
		// (get) Token: 0x060030EF RID: 12527 RVA: 0x000A0D5C File Offset: 0x0009EF5C
		public virtual IDictionary Properties
		{
			get
			{
				if (this._properties == null)
				{
					this._properties = new MethodReturnDictionary(this);
				}
				return this._properties;
			}
		}

		// Token: 0x1700092A RID: 2346
		// (get) Token: 0x060030F0 RID: 12528 RVA: 0x000A0D7C File Offset: 0x0009EF7C
		public string TypeName
		{
			get
			{
				if (this._methodBase != null && this._typeName == null)
				{
					this._typeName = this._methodBase.DeclaringType.AssemblyQualifiedName;
				}
				return this._typeName;
			}
		}

		// Token: 0x1700092B RID: 2347
		// (get) Token: 0x060030F1 RID: 12529 RVA: 0x000A0DBC File Offset: 0x0009EFBC
		// (set) Token: 0x060030F2 RID: 12530 RVA: 0x000A0DC4 File Offset: 0x0009EFC4
		public string Uri
		{
			get
			{
				return this._uri;
			}
			set
			{
				this._uri = value;
			}
		}

		// Token: 0x060030F3 RID: 12531 RVA: 0x000A0DD0 File Offset: 0x0009EFD0
		public object GetArg(int argNum)
		{
			return this._args[argNum];
		}

		// Token: 0x060030F4 RID: 12532 RVA: 0x000A0DDC File Offset: 0x0009EFDC
		public string GetArgName(int index)
		{
			return this._methodBase.GetParameters()[index].Name;
		}

		// Token: 0x1700092C RID: 2348
		// (get) Token: 0x060030F5 RID: 12533 RVA: 0x000A0DF0 File Offset: 0x0009EFF0
		public Exception Exception
		{
			get
			{
				return this._exception;
			}
		}

		// Token: 0x1700092D RID: 2349
		// (get) Token: 0x060030F6 RID: 12534 RVA: 0x000A0DF8 File Offset: 0x0009EFF8
		public int OutArgCount
		{
			get
			{
				if (this._args == null || this._args.Length == 0)
				{
					return 0;
				}
				if (this._inArgInfo == null)
				{
					this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
				}
				return this._inArgInfo.GetInOutArgCount();
			}
		}

		// Token: 0x1700092E RID: 2350
		// (get) Token: 0x060030F7 RID: 12535 RVA: 0x000A0E48 File Offset: 0x0009F048
		public object[] OutArgs
		{
			get
			{
				if (this._outArgs == null && this._args != null)
				{
					if (this._inArgInfo == null)
					{
						this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
					}
					this._outArgs = this._inArgInfo.GetInOutArgs(this._args);
				}
				return this._outArgs;
			}
		}

		// Token: 0x1700092F RID: 2351
		// (get) Token: 0x060030F8 RID: 12536 RVA: 0x000A0EA8 File Offset: 0x0009F0A8
		public virtual object ReturnValue
		{
			get
			{
				return this._returnValue;
			}
		}

		// Token: 0x060030F9 RID: 12537 RVA: 0x000A0EB0 File Offset: 0x0009F0B0
		public object GetOutArg(int argNum)
		{
			if (this._inArgInfo == null)
			{
				this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
			}
			return this._args[this._inArgInfo.GetInOutArgIndex(argNum)];
		}

		// Token: 0x060030FA RID: 12538 RVA: 0x000A0EF0 File Offset: 0x0009F0F0
		public string GetOutArgName(int index)
		{
			if (this._inArgInfo == null)
			{
				this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
			}
			return this._inArgInfo.GetInOutArgName(index);
		}

		// Token: 0x040014B0 RID: 5296
		private object[] _outArgs;

		// Token: 0x040014B1 RID: 5297
		private object[] _args;

		// Token: 0x040014B2 RID: 5298
		private int _outArgsCount;

		// Token: 0x040014B3 RID: 5299
		private LogicalCallContext _callCtx;

		// Token: 0x040014B4 RID: 5300
		private object _returnValue;

		// Token: 0x040014B5 RID: 5301
		private string _uri;

		// Token: 0x040014B6 RID: 5302
		private Exception _exception;

		// Token: 0x040014B7 RID: 5303
		private MethodBase _methodBase;

		// Token: 0x040014B8 RID: 5304
		private string _methodName;

		// Token: 0x040014B9 RID: 5305
		private Type[] _methodSignature;

		// Token: 0x040014BA RID: 5306
		private string _typeName;

		// Token: 0x040014BB RID: 5307
		private MethodReturnDictionary _properties;

		// Token: 0x040014BC RID: 5308
		private Identity _targetIdentity;

		// Token: 0x040014BD RID: 5309
		private ArgInfo _inArgInfo;
	}
}
