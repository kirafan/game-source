﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004A0 RID: 1184
	[ComVisible(true)]
	public interface IRemotingFormatter : IFormatter
	{
		// Token: 0x06002FFF RID: 12287
		object Deserialize(Stream serializationStream, HeaderHandler handler);

		// Token: 0x06003000 RID: 12288
		void Serialize(Stream serializationStream, object graph, Header[] headers);
	}
}
