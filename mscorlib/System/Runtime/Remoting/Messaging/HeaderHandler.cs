﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020006F6 RID: 1782
	// (Invoke) Token: 0x060043CC RID: 17356
	[ComVisible(true)]
	public delegate object HeaderHandler(Header[] headers);
}
