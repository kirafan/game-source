﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004A5 RID: 1189
	[ComVisible(true)]
	[CLSCompliant(false)]
	[Serializable]
	public class MethodCall : ISerializable, IInternalMessage, IMessage, IMethodCallMessage, IMethodMessage, ISerializationRootObject
	{
		// Token: 0x06003010 RID: 12304 RVA: 0x0009E270 File Offset: 0x0009C470
		public MethodCall(Header[] h1)
		{
			this.Init();
			if (h1 == null || h1.Length == 0)
			{
				return;
			}
			foreach (Header header in h1)
			{
				this.InitMethodProperty(header.Name, header.Value);
			}
			this.ResolveMethod();
		}

		// Token: 0x06003011 RID: 12305 RVA: 0x0009E2CC File Offset: 0x0009C4CC
		internal MethodCall(SerializationInfo info, StreamingContext context)
		{
			this.Init();
			foreach (SerializationEntry serializationEntry in info)
			{
				this.InitMethodProperty(serializationEntry.Name, serializationEntry.Value);
			}
		}

		// Token: 0x06003012 RID: 12306 RVA: 0x0009E318 File Offset: 0x0009C518
		internal MethodCall(CADMethodCallMessage msg)
		{
			this._uri = string.Copy(msg.Uri);
			ArrayList arguments = msg.GetArguments();
			this._args = msg.GetArgs(arguments);
			this._callContext = msg.GetLogicalCallContext(arguments);
			if (this._callContext == null)
			{
				this._callContext = new LogicalCallContext();
			}
			this._methodBase = msg.GetMethod();
			this.Init();
			if (msg.PropertiesCount > 0)
			{
				CADMessageBase.UnmarshalProperties(this.Properties, msg.PropertiesCount, arguments);
			}
		}

		// Token: 0x06003013 RID: 12307 RVA: 0x0009E3A4 File Offset: 0x0009C5A4
		public MethodCall(IMessage msg)
		{
			if (msg is IMethodMessage)
			{
				this.CopyFrom((IMethodMessage)msg);
			}
			else
			{
				foreach (object obj in msg.Properties)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					this.InitMethodProperty((string)dictionaryEntry.Key, dictionaryEntry.Value);
				}
				this.Init();
			}
		}

		// Token: 0x06003014 RID: 12308 RVA: 0x0009E450 File Offset: 0x0009C650
		internal MethodCall(string uri, string typeName, string methodName, object[] args)
		{
			this._uri = uri;
			this._typeName = typeName;
			this._methodName = methodName;
			this._args = args;
			this.Init();
			this.ResolveMethod();
		}

		// Token: 0x06003015 RID: 12309 RVA: 0x0009E484 File Offset: 0x0009C684
		internal MethodCall()
		{
		}

		// Token: 0x170008C3 RID: 2243
		// (get) Token: 0x06003016 RID: 12310 RVA: 0x0009E48C File Offset: 0x0009C68C
		// (set) Token: 0x06003017 RID: 12311 RVA: 0x0009E494 File Offset: 0x0009C694
		string IInternalMessage.Uri
		{
			get
			{
				return this.Uri;
			}
			set
			{
				this.Uri = value;
			}
		}

		// Token: 0x170008C4 RID: 2244
		// (get) Token: 0x06003018 RID: 12312 RVA: 0x0009E4A0 File Offset: 0x0009C6A0
		// (set) Token: 0x06003019 RID: 12313 RVA: 0x0009E4A8 File Offset: 0x0009C6A8
		Identity IInternalMessage.TargetIdentity
		{
			get
			{
				return this._targetIdentity;
			}
			set
			{
				this._targetIdentity = value;
			}
		}

		// Token: 0x0600301A RID: 12314 RVA: 0x0009E4B4 File Offset: 0x0009C6B4
		internal void CopyFrom(IMethodMessage call)
		{
			this._uri = call.Uri;
			this._typeName = call.TypeName;
			this._methodName = call.MethodName;
			this._args = call.Args;
			this._methodSignature = (Type[])call.MethodSignature;
			this._methodBase = call.MethodBase;
			this._callContext = call.LogicalCallContext;
			this.Init();
		}

		// Token: 0x0600301B RID: 12315 RVA: 0x0009E520 File Offset: 0x0009C720
		internal virtual void InitMethodProperty(string key, object value)
		{
			switch (key)
			{
			case "__TypeName":
				this._typeName = (string)value;
				return;
			case "__MethodName":
				this._methodName = (string)value;
				return;
			case "__MethodSignature":
				this._methodSignature = (Type[])value;
				return;
			case "__Args":
				this._args = (object[])value;
				return;
			case "__CallContext":
				this._callContext = (LogicalCallContext)value;
				return;
			case "__Uri":
				this._uri = (string)value;
				return;
			case "__GenericArguments":
				this._genericArguments = (Type[])value;
				return;
			}
			this.Properties[key] = value;
		}

		// Token: 0x0600301C RID: 12316 RVA: 0x0009E644 File Offset: 0x0009C844
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("__TypeName", this._typeName);
			info.AddValue("__MethodName", this._methodName);
			info.AddValue("__MethodSignature", this._methodSignature);
			info.AddValue("__Args", this._args);
			info.AddValue("__CallContext", this._callContext);
			info.AddValue("__Uri", this._uri);
			info.AddValue("__GenericArguments", this._genericArguments);
			if (this.InternalProperties != null)
			{
				foreach (object obj in this.InternalProperties)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					info.AddValue((string)dictionaryEntry.Key, dictionaryEntry.Value);
				}
			}
		}

		// Token: 0x170008C5 RID: 2245
		// (get) Token: 0x0600301D RID: 12317 RVA: 0x0009E748 File Offset: 0x0009C948
		public int ArgCount
		{
			get
			{
				return this._args.Length;
			}
		}

		// Token: 0x170008C6 RID: 2246
		// (get) Token: 0x0600301E RID: 12318 RVA: 0x0009E754 File Offset: 0x0009C954
		public object[] Args
		{
			get
			{
				return this._args;
			}
		}

		// Token: 0x170008C7 RID: 2247
		// (get) Token: 0x0600301F RID: 12319 RVA: 0x0009E75C File Offset: 0x0009C95C
		public bool HasVarArgs
		{
			get
			{
				return (this.MethodBase.CallingConvention | CallingConventions.VarArgs) != (CallingConventions)0;
			}
		}

		// Token: 0x170008C8 RID: 2248
		// (get) Token: 0x06003020 RID: 12320 RVA: 0x0009E774 File Offset: 0x0009C974
		public int InArgCount
		{
			get
			{
				if (this._inArgInfo == null)
				{
					this._inArgInfo = new ArgInfo(this._methodBase, ArgInfoType.In);
				}
				return this._inArgInfo.GetInOutArgCount();
			}
		}

		// Token: 0x170008C9 RID: 2249
		// (get) Token: 0x06003021 RID: 12321 RVA: 0x0009E7AC File Offset: 0x0009C9AC
		public object[] InArgs
		{
			get
			{
				if (this._inArgInfo == null)
				{
					this._inArgInfo = new ArgInfo(this._methodBase, ArgInfoType.In);
				}
				return this._inArgInfo.GetInOutArgs(this._args);
			}
		}

		// Token: 0x170008CA RID: 2250
		// (get) Token: 0x06003022 RID: 12322 RVA: 0x0009E7E8 File Offset: 0x0009C9E8
		public LogicalCallContext LogicalCallContext
		{
			get
			{
				if (this._callContext == null)
				{
					this._callContext = new LogicalCallContext();
				}
				return this._callContext;
			}
		}

		// Token: 0x170008CB RID: 2251
		// (get) Token: 0x06003023 RID: 12323 RVA: 0x0009E808 File Offset: 0x0009CA08
		public MethodBase MethodBase
		{
			get
			{
				if (this._methodBase == null)
				{
					this.ResolveMethod();
				}
				return this._methodBase;
			}
		}

		// Token: 0x170008CC RID: 2252
		// (get) Token: 0x06003024 RID: 12324 RVA: 0x0009E824 File Offset: 0x0009CA24
		public string MethodName
		{
			get
			{
				if (this._methodName == null)
				{
					this._methodName = this._methodBase.Name;
				}
				return this._methodName;
			}
		}

		// Token: 0x170008CD RID: 2253
		// (get) Token: 0x06003025 RID: 12325 RVA: 0x0009E854 File Offset: 0x0009CA54
		public object MethodSignature
		{
			get
			{
				if (this._methodSignature == null && this._methodBase != null)
				{
					ParameterInfo[] parameters = this._methodBase.GetParameters();
					this._methodSignature = new Type[parameters.Length];
					for (int i = 0; i < parameters.Length; i++)
					{
						this._methodSignature[i] = parameters[i].ParameterType;
					}
				}
				return this._methodSignature;
			}
		}

		// Token: 0x170008CE RID: 2254
		// (get) Token: 0x06003026 RID: 12326 RVA: 0x0009E8BC File Offset: 0x0009CABC
		public virtual IDictionary Properties
		{
			get
			{
				if (this.ExternalProperties == null)
				{
					this.InitDictionary();
				}
				return this.ExternalProperties;
			}
		}

		// Token: 0x06003027 RID: 12327 RVA: 0x0009E8D8 File Offset: 0x0009CAD8
		internal virtual void InitDictionary()
		{
			MethodCallDictionary methodCallDictionary = new MethodCallDictionary(this);
			this.ExternalProperties = methodCallDictionary;
			this.InternalProperties = methodCallDictionary.GetInternalProperties();
		}

		// Token: 0x170008CF RID: 2255
		// (get) Token: 0x06003028 RID: 12328 RVA: 0x0009E900 File Offset: 0x0009CB00
		public string TypeName
		{
			get
			{
				if (this._typeName == null)
				{
					this._typeName = this._methodBase.DeclaringType.AssemblyQualifiedName;
				}
				return this._typeName;
			}
		}

		// Token: 0x170008D0 RID: 2256
		// (get) Token: 0x06003029 RID: 12329 RVA: 0x0009E92C File Offset: 0x0009CB2C
		// (set) Token: 0x0600302A RID: 12330 RVA: 0x0009E934 File Offset: 0x0009CB34
		public string Uri
		{
			get
			{
				return this._uri;
			}
			set
			{
				this._uri = value;
			}
		}

		// Token: 0x0600302B RID: 12331 RVA: 0x0009E940 File Offset: 0x0009CB40
		public object GetArg(int argNum)
		{
			return this._args[argNum];
		}

		// Token: 0x0600302C RID: 12332 RVA: 0x0009E94C File Offset: 0x0009CB4C
		public string GetArgName(int index)
		{
			return this._methodBase.GetParameters()[index].Name;
		}

		// Token: 0x0600302D RID: 12333 RVA: 0x0009E960 File Offset: 0x0009CB60
		public object GetInArg(int argNum)
		{
			if (this._inArgInfo == null)
			{
				this._inArgInfo = new ArgInfo(this._methodBase, ArgInfoType.In);
			}
			return this._args[this._inArgInfo.GetInOutArgIndex(argNum)];
		}

		// Token: 0x0600302E RID: 12334 RVA: 0x0009E9A0 File Offset: 0x0009CBA0
		public string GetInArgName(int index)
		{
			if (this._inArgInfo == null)
			{
				this._inArgInfo = new ArgInfo(this._methodBase, ArgInfoType.In);
			}
			return this._inArgInfo.GetInOutArgName(index);
		}

		// Token: 0x0600302F RID: 12335 RVA: 0x0009E9CC File Offset: 0x0009CBCC
		[MonoTODO]
		public virtual object HeaderHandler(Header[] h)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06003030 RID: 12336 RVA: 0x0009E9D4 File Offset: 0x0009CBD4
		public virtual void Init()
		{
		}

		// Token: 0x06003031 RID: 12337 RVA: 0x0009E9D8 File Offset: 0x0009CBD8
		public void ResolveMethod()
		{
			if (this._uri != null)
			{
				Type serverTypeForUri = RemotingServices.GetServerTypeForUri(this._uri);
				if (serverTypeForUri == null)
				{
					string str = (this._typeName == null) ? string.Empty : (" (" + this._typeName + ")");
					throw new RemotingException("Requested service not found" + str + ". No receiver for uri " + this._uri);
				}
				Type type = this.CastTo(this._typeName, serverTypeForUri);
				if (type == null)
				{
					throw new RemotingException(string.Concat(new string[]
					{
						"Cannot cast from client type '",
						this._typeName,
						"' to server type '",
						serverTypeForUri.FullName,
						"'"
					}));
				}
				this._methodBase = RemotingServices.GetMethodBaseFromName(type, this._methodName, this._methodSignature);
				if (this._methodBase == null)
				{
					throw new RemotingException(string.Concat(new object[]
					{
						"Method ",
						this._methodName,
						" not found in ",
						type
					}));
				}
				if (type != serverTypeForUri && type.IsInterface && !serverTypeForUri.IsInterface)
				{
					this._methodBase = RemotingServices.GetVirtualMethod(serverTypeForUri, this._methodBase);
					if (this._methodBase == null)
					{
						throw new RemotingException(string.Concat(new object[]
						{
							"Method ",
							this._methodName,
							" not found in ",
							serverTypeForUri
						}));
					}
				}
			}
			else
			{
				this._methodBase = RemotingServices.GetMethodBaseFromMethodMessage(this);
				if (this._methodBase == null)
				{
					throw new RemotingException("Method " + this._methodName + " not found in " + this.TypeName);
				}
			}
			if (this._methodBase.IsGenericMethod && this._methodBase.ContainsGenericParameters)
			{
				if (this.GenericArguments == null)
				{
					throw new RemotingException("The remoting infrastructure does not support open generic methods.");
				}
				this._methodBase = ((MethodInfo)this._methodBase).MakeGenericMethod(this.GenericArguments);
			}
		}

		// Token: 0x06003032 RID: 12338 RVA: 0x0009EBE0 File Offset: 0x0009CDE0
		private Type CastTo(string clientType, Type serverType)
		{
			clientType = MethodCall.GetTypeNameFromAssemblyQualifiedName(clientType);
			if (clientType == serverType.FullName)
			{
				return serverType;
			}
			for (Type baseType = serverType.BaseType; baseType != null; baseType = baseType.BaseType)
			{
				if (clientType == baseType.FullName)
				{
					return baseType;
				}
			}
			Type[] interfaces = serverType.GetInterfaces();
			foreach (Type type in interfaces)
			{
				if (clientType == type.FullName)
				{
					return type;
				}
			}
			return null;
		}

		// Token: 0x06003033 RID: 12339 RVA: 0x0009EC70 File Offset: 0x0009CE70
		private static string GetTypeNameFromAssemblyQualifiedName(string aqname)
		{
			int num = aqname.IndexOf("]]");
			int num2 = aqname.IndexOf(',', (num != -1) ? (num + 2) : 0);
			if (num2 != -1)
			{
				aqname = aqname.Substring(0, num2).Trim();
			}
			return aqname;
		}

		// Token: 0x06003034 RID: 12340 RVA: 0x0009ECBC File Offset: 0x0009CEBC
		[MonoTODO]
		public void RootSetObjectData(SerializationInfo info, StreamingContext ctx)
		{
			throw new NotImplementedException();
		}

		// Token: 0x170008D1 RID: 2257
		// (get) Token: 0x06003035 RID: 12341 RVA: 0x0009ECC4 File Offset: 0x0009CEC4
		private Type[] GenericArguments
		{
			get
			{
				if (this._genericArguments != null)
				{
					return this._genericArguments;
				}
				return this._genericArguments = this.MethodBase.GetGenericArguments();
			}
		}

		// Token: 0x04001463 RID: 5219
		private string _uri;

		// Token: 0x04001464 RID: 5220
		private string _typeName;

		// Token: 0x04001465 RID: 5221
		private string _methodName;

		// Token: 0x04001466 RID: 5222
		private object[] _args;

		// Token: 0x04001467 RID: 5223
		private Type[] _methodSignature;

		// Token: 0x04001468 RID: 5224
		private MethodBase _methodBase;

		// Token: 0x04001469 RID: 5225
		private LogicalCallContext _callContext;

		// Token: 0x0400146A RID: 5226
		private ArgInfo _inArgInfo;

		// Token: 0x0400146B RID: 5227
		private Identity _targetIdentity;

		// Token: 0x0400146C RID: 5228
		private Type[] _genericArguments;

		// Token: 0x0400146D RID: 5229
		protected IDictionary ExternalProperties;

		// Token: 0x0400146E RID: 5230
		protected IDictionary InternalProperties;
	}
}
