﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004A6 RID: 1190
	[ComVisible(true)]
	public class MethodCallMessageWrapper : InternalMessageWrapper, IMessage, IMethodCallMessage, IMethodMessage
	{
		// Token: 0x06003036 RID: 12342 RVA: 0x0009ECF8 File Offset: 0x0009CEF8
		public MethodCallMessageWrapper(IMethodCallMessage msg) : base(msg)
		{
			this._args = ((IMethodCallMessage)this.WrappedMessage).Args;
			this._inArgInfo = new ArgInfo(msg.MethodBase, ArgInfoType.In);
		}

		// Token: 0x170008D2 RID: 2258
		// (get) Token: 0x06003037 RID: 12343 RVA: 0x0009ED34 File Offset: 0x0009CF34
		public virtual int ArgCount
		{
			get
			{
				return ((IMethodCallMessage)this.WrappedMessage).ArgCount;
			}
		}

		// Token: 0x170008D3 RID: 2259
		// (get) Token: 0x06003038 RID: 12344 RVA: 0x0009ED48 File Offset: 0x0009CF48
		// (set) Token: 0x06003039 RID: 12345 RVA: 0x0009ED50 File Offset: 0x0009CF50
		public virtual object[] Args
		{
			get
			{
				return this._args;
			}
			set
			{
				this._args = value;
			}
		}

		// Token: 0x170008D4 RID: 2260
		// (get) Token: 0x0600303A RID: 12346 RVA: 0x0009ED5C File Offset: 0x0009CF5C
		public virtual bool HasVarArgs
		{
			get
			{
				return ((IMethodCallMessage)this.WrappedMessage).HasVarArgs;
			}
		}

		// Token: 0x170008D5 RID: 2261
		// (get) Token: 0x0600303B RID: 12347 RVA: 0x0009ED70 File Offset: 0x0009CF70
		public virtual int InArgCount
		{
			get
			{
				return this._inArgInfo.GetInOutArgCount();
			}
		}

		// Token: 0x170008D6 RID: 2262
		// (get) Token: 0x0600303C RID: 12348 RVA: 0x0009ED80 File Offset: 0x0009CF80
		public virtual object[] InArgs
		{
			get
			{
				return this._inArgInfo.GetInOutArgs(this._args);
			}
		}

		// Token: 0x170008D7 RID: 2263
		// (get) Token: 0x0600303D RID: 12349 RVA: 0x0009ED94 File Offset: 0x0009CF94
		public virtual LogicalCallContext LogicalCallContext
		{
			get
			{
				return ((IMethodCallMessage)this.WrappedMessage).LogicalCallContext;
			}
		}

		// Token: 0x170008D8 RID: 2264
		// (get) Token: 0x0600303E RID: 12350 RVA: 0x0009EDA8 File Offset: 0x0009CFA8
		public virtual MethodBase MethodBase
		{
			get
			{
				return ((IMethodCallMessage)this.WrappedMessage).MethodBase;
			}
		}

		// Token: 0x170008D9 RID: 2265
		// (get) Token: 0x0600303F RID: 12351 RVA: 0x0009EDBC File Offset: 0x0009CFBC
		public virtual string MethodName
		{
			get
			{
				return ((IMethodCallMessage)this.WrappedMessage).MethodName;
			}
		}

		// Token: 0x170008DA RID: 2266
		// (get) Token: 0x06003040 RID: 12352 RVA: 0x0009EDD0 File Offset: 0x0009CFD0
		public virtual object MethodSignature
		{
			get
			{
				return ((IMethodCallMessage)this.WrappedMessage).MethodSignature;
			}
		}

		// Token: 0x170008DB RID: 2267
		// (get) Token: 0x06003041 RID: 12353 RVA: 0x0009EDE4 File Offset: 0x0009CFE4
		public virtual IDictionary Properties
		{
			get
			{
				if (this._properties == null)
				{
					this._properties = new MethodCallMessageWrapper.DictionaryWrapper(this, this.WrappedMessage.Properties);
				}
				return this._properties;
			}
		}

		// Token: 0x170008DC RID: 2268
		// (get) Token: 0x06003042 RID: 12354 RVA: 0x0009EE1C File Offset: 0x0009D01C
		public virtual string TypeName
		{
			get
			{
				return ((IMethodCallMessage)this.WrappedMessage).TypeName;
			}
		}

		// Token: 0x170008DD RID: 2269
		// (get) Token: 0x06003043 RID: 12355 RVA: 0x0009EE30 File Offset: 0x0009D030
		// (set) Token: 0x06003044 RID: 12356 RVA: 0x0009EE44 File Offset: 0x0009D044
		public virtual string Uri
		{
			get
			{
				return ((IMethodCallMessage)this.WrappedMessage).Uri;
			}
			set
			{
				IInternalMessage internalMessage = this.WrappedMessage as IInternalMessage;
				if (internalMessage != null)
				{
					internalMessage.Uri = value;
				}
				else
				{
					this.Properties["__Uri"] = value;
				}
			}
		}

		// Token: 0x06003045 RID: 12357 RVA: 0x0009EE80 File Offset: 0x0009D080
		public virtual object GetArg(int argNum)
		{
			return this._args[argNum];
		}

		// Token: 0x06003046 RID: 12358 RVA: 0x0009EE8C File Offset: 0x0009D08C
		public virtual string GetArgName(int index)
		{
			return ((IMethodCallMessage)this.WrappedMessage).GetArgName(index);
		}

		// Token: 0x06003047 RID: 12359 RVA: 0x0009EEA0 File Offset: 0x0009D0A0
		public virtual object GetInArg(int argNum)
		{
			return this._args[this._inArgInfo.GetInOutArgIndex(argNum)];
		}

		// Token: 0x06003048 RID: 12360 RVA: 0x0009EEB8 File Offset: 0x0009D0B8
		public virtual string GetInArgName(int index)
		{
			return this._inArgInfo.GetInOutArgName(index);
		}

		// Token: 0x04001470 RID: 5232
		private object[] _args;

		// Token: 0x04001471 RID: 5233
		private ArgInfo _inArgInfo;

		// Token: 0x04001472 RID: 5234
		private MethodCallMessageWrapper.DictionaryWrapper _properties;

		// Token: 0x020004A7 RID: 1191
		private class DictionaryWrapper : MethodCallDictionary
		{
			// Token: 0x06003049 RID: 12361 RVA: 0x0009EEC8 File Offset: 0x0009D0C8
			public DictionaryWrapper(IMethodMessage message, IDictionary wrappedDictionary) : base(message)
			{
				this._wrappedDictionary = wrappedDictionary;
				base.MethodKeys = MethodCallMessageWrapper.DictionaryWrapper._keys;
			}

			// Token: 0x0600304B RID: 12363 RVA: 0x0009EEFC File Offset: 0x0009D0FC
			protected override IDictionary AllocInternalProperties()
			{
				return this._wrappedDictionary;
			}

			// Token: 0x0600304C RID: 12364 RVA: 0x0009EF04 File Offset: 0x0009D104
			protected override void SetMethodProperty(string key, object value)
			{
				if (key == "__Args")
				{
					((MethodCallMessageWrapper)this._message)._args = (object[])value;
				}
				else
				{
					base.SetMethodProperty(key, value);
				}
			}

			// Token: 0x0600304D RID: 12365 RVA: 0x0009EF3C File Offset: 0x0009D13C
			protected override object GetMethodProperty(string key)
			{
				if (key == "__Args")
				{
					return ((MethodCallMessageWrapper)this._message)._args;
				}
				return base.GetMethodProperty(key);
			}

			// Token: 0x04001473 RID: 5235
			private IDictionary _wrappedDictionary;

			// Token: 0x04001474 RID: 5236
			private static string[] _keys = new string[]
			{
				"__Args"
			};
		}
	}
}
