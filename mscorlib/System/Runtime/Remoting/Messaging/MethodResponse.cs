﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004A8 RID: 1192
	[ComVisible(true)]
	[CLSCompliant(false)]
	[Serializable]
	public class MethodResponse : ISerializable, IInternalMessage, IMessage, IMethodMessage, IMethodReturnMessage, ISerializationRootObject
	{
		// Token: 0x0600304E RID: 12366 RVA: 0x0009EF74 File Offset: 0x0009D174
		public MethodResponse(Header[] h1, IMethodCallMessage mcm)
		{
			if (mcm != null)
			{
				this._methodName = mcm.MethodName;
				this._uri = mcm.Uri;
				this._typeName = mcm.TypeName;
				this._methodBase = mcm.MethodBase;
				this._methodSignature = (Type[])mcm.MethodSignature;
				this._args = mcm.Args;
			}
			if (h1 != null)
			{
				foreach (Header header in h1)
				{
					this.InitMethodProperty(header.Name, header.Value);
				}
			}
		}

		// Token: 0x0600304F RID: 12367 RVA: 0x0009F00C File Offset: 0x0009D20C
		internal MethodResponse(Exception e, IMethodCallMessage msg)
		{
			this._callMsg = msg;
			if (msg != null)
			{
				this._uri = msg.Uri;
			}
			else
			{
				this._uri = string.Empty;
			}
			this._exception = e;
			this._returnValue = null;
			this._outArgs = new object[0];
		}

		// Token: 0x06003050 RID: 12368 RVA: 0x0009F064 File Offset: 0x0009D264
		internal MethodResponse(object returnValue, object[] outArgs, LogicalCallContext callCtx, IMethodCallMessage msg)
		{
			this._callMsg = msg;
			this._uri = msg.Uri;
			this._exception = null;
			this._returnValue = returnValue;
			this._args = outArgs;
		}

		// Token: 0x06003051 RID: 12369 RVA: 0x0009F0A4 File Offset: 0x0009D2A4
		internal MethodResponse(IMethodCallMessage msg, CADMethodReturnMessage retmsg)
		{
			this._callMsg = msg;
			this._methodBase = msg.MethodBase;
			this._uri = msg.Uri;
			this._methodName = msg.MethodName;
			ArrayList arguments = retmsg.GetArguments();
			this._exception = retmsg.GetException(arguments);
			this._returnValue = retmsg.GetReturnValue(arguments);
			this._args = retmsg.GetArgs(arguments);
			this._callContext = retmsg.GetLogicalCallContext(arguments);
			if (this._callContext == null)
			{
				this._callContext = new LogicalCallContext();
			}
			if (retmsg.PropertiesCount > 0)
			{
				CADMessageBase.UnmarshalProperties(this.Properties, retmsg.PropertiesCount, arguments);
			}
		}

		// Token: 0x06003052 RID: 12370 RVA: 0x0009F154 File Offset: 0x0009D354
		internal MethodResponse(SerializationInfo info, StreamingContext context)
		{
			foreach (SerializationEntry serializationEntry in info)
			{
				this.InitMethodProperty(serializationEntry.Name, serializationEntry.Value);
			}
		}

		// Token: 0x170008DE RID: 2270
		// (get) Token: 0x06003053 RID: 12371 RVA: 0x0009F19C File Offset: 0x0009D39C
		// (set) Token: 0x06003054 RID: 12372 RVA: 0x0009F1A4 File Offset: 0x0009D3A4
		string IInternalMessage.Uri
		{
			get
			{
				return this.Uri;
			}
			set
			{
				this.Uri = value;
			}
		}

		// Token: 0x170008DF RID: 2271
		// (get) Token: 0x06003055 RID: 12373 RVA: 0x0009F1B0 File Offset: 0x0009D3B0
		// (set) Token: 0x06003056 RID: 12374 RVA: 0x0009F1B8 File Offset: 0x0009D3B8
		Identity IInternalMessage.TargetIdentity
		{
			get
			{
				return this._targetIdentity;
			}
			set
			{
				this._targetIdentity = value;
			}
		}

		// Token: 0x06003057 RID: 12375 RVA: 0x0009F1C4 File Offset: 0x0009D3C4
		internal void InitMethodProperty(string key, object value)
		{
			switch (key)
			{
			case "__TypeName":
				this._typeName = (string)value;
				return;
			case "__MethodName":
				this._methodName = (string)value;
				return;
			case "__MethodSignature":
				this._methodSignature = (Type[])value;
				return;
			case "__Uri":
				this._uri = (string)value;
				return;
			case "__Return":
				this._returnValue = value;
				return;
			case "__OutArgs":
				this._args = (object[])value;
				return;
			case "__fault":
				this._exception = (Exception)value;
				return;
			case "__CallContext":
				this._callContext = (LogicalCallContext)value;
				return;
			}
			this.Properties[key] = value;
		}

		// Token: 0x170008E0 RID: 2272
		// (get) Token: 0x06003058 RID: 12376 RVA: 0x0009F324 File Offset: 0x0009D524
		public int ArgCount
		{
			get
			{
				if (this._args == null)
				{
					return 0;
				}
				return this._args.Length;
			}
		}

		// Token: 0x170008E1 RID: 2273
		// (get) Token: 0x06003059 RID: 12377 RVA: 0x0009F33C File Offset: 0x0009D53C
		public object[] Args
		{
			get
			{
				return this._args;
			}
		}

		// Token: 0x170008E2 RID: 2274
		// (get) Token: 0x0600305A RID: 12378 RVA: 0x0009F344 File Offset: 0x0009D544
		public Exception Exception
		{
			get
			{
				return this._exception;
			}
		}

		// Token: 0x170008E3 RID: 2275
		// (get) Token: 0x0600305B RID: 12379 RVA: 0x0009F34C File Offset: 0x0009D54C
		public bool HasVarArgs
		{
			get
			{
				return (this.MethodBase.CallingConvention | CallingConventions.VarArgs) != (CallingConventions)0;
			}
		}

		// Token: 0x170008E4 RID: 2276
		// (get) Token: 0x0600305C RID: 12380 RVA: 0x0009F364 File Offset: 0x0009D564
		public LogicalCallContext LogicalCallContext
		{
			get
			{
				if (this._callContext == null)
				{
					this._callContext = new LogicalCallContext();
				}
				return this._callContext;
			}
		}

		// Token: 0x170008E5 RID: 2277
		// (get) Token: 0x0600305D RID: 12381 RVA: 0x0009F384 File Offset: 0x0009D584
		public MethodBase MethodBase
		{
			get
			{
				if (this._methodBase == null)
				{
					if (this._callMsg != null)
					{
						this._methodBase = this._callMsg.MethodBase;
					}
					else if (this.MethodName != null && this.TypeName != null)
					{
						this._methodBase = RemotingServices.GetMethodBaseFromMethodMessage(this);
					}
				}
				return this._methodBase;
			}
		}

		// Token: 0x170008E6 RID: 2278
		// (get) Token: 0x0600305E RID: 12382 RVA: 0x0009F3E8 File Offset: 0x0009D5E8
		public string MethodName
		{
			get
			{
				if (this._methodName == null && this._callMsg != null)
				{
					this._methodName = this._callMsg.MethodName;
				}
				return this._methodName;
			}
		}

		// Token: 0x170008E7 RID: 2279
		// (get) Token: 0x0600305F RID: 12383 RVA: 0x0009F418 File Offset: 0x0009D618
		public object MethodSignature
		{
			get
			{
				if (this._methodSignature == null && this._callMsg != null)
				{
					this._methodSignature = (Type[])this._callMsg.MethodSignature;
				}
				return this._methodSignature;
			}
		}

		// Token: 0x170008E8 RID: 2280
		// (get) Token: 0x06003060 RID: 12384 RVA: 0x0009F458 File Offset: 0x0009D658
		public int OutArgCount
		{
			get
			{
				if (this._args == null || this._args.Length == 0)
				{
					return 0;
				}
				if (this._inArgInfo == null)
				{
					this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
				}
				return this._inArgInfo.GetInOutArgCount();
			}
		}

		// Token: 0x170008E9 RID: 2281
		// (get) Token: 0x06003061 RID: 12385 RVA: 0x0009F4A8 File Offset: 0x0009D6A8
		public object[] OutArgs
		{
			get
			{
				if (this._outArgs == null && this._args != null)
				{
					if (this._inArgInfo == null)
					{
						this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
					}
					this._outArgs = this._inArgInfo.GetInOutArgs(this._args);
				}
				return this._outArgs;
			}
		}

		// Token: 0x170008EA RID: 2282
		// (get) Token: 0x06003062 RID: 12386 RVA: 0x0009F508 File Offset: 0x0009D708
		public virtual IDictionary Properties
		{
			get
			{
				if (this.ExternalProperties == null)
				{
					MethodReturnDictionary methodReturnDictionary = new MethodReturnDictionary(this);
					this.ExternalProperties = methodReturnDictionary;
					this.InternalProperties = methodReturnDictionary.GetInternalProperties();
				}
				return this.ExternalProperties;
			}
		}

		// Token: 0x170008EB RID: 2283
		// (get) Token: 0x06003063 RID: 12387 RVA: 0x0009F540 File Offset: 0x0009D740
		public object ReturnValue
		{
			get
			{
				return this._returnValue;
			}
		}

		// Token: 0x170008EC RID: 2284
		// (get) Token: 0x06003064 RID: 12388 RVA: 0x0009F548 File Offset: 0x0009D748
		public string TypeName
		{
			get
			{
				if (this._typeName == null && this._callMsg != null)
				{
					this._typeName = this._callMsg.TypeName;
				}
				return this._typeName;
			}
		}

		// Token: 0x170008ED RID: 2285
		// (get) Token: 0x06003065 RID: 12389 RVA: 0x0009F578 File Offset: 0x0009D778
		// (set) Token: 0x06003066 RID: 12390 RVA: 0x0009F5A8 File Offset: 0x0009D7A8
		public string Uri
		{
			get
			{
				if (this._uri == null && this._callMsg != null)
				{
					this._uri = this._callMsg.Uri;
				}
				return this._uri;
			}
			set
			{
				this._uri = value;
			}
		}

		// Token: 0x06003067 RID: 12391 RVA: 0x0009F5B4 File Offset: 0x0009D7B4
		public object GetArg(int argNum)
		{
			if (this._args == null)
			{
				return null;
			}
			return this._args[argNum];
		}

		// Token: 0x06003068 RID: 12392 RVA: 0x0009F5CC File Offset: 0x0009D7CC
		public string GetArgName(int index)
		{
			return this.MethodBase.GetParameters()[index].Name;
		}

		// Token: 0x06003069 RID: 12393 RVA: 0x0009F5E0 File Offset: 0x0009D7E0
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (this._exception == null)
			{
				info.AddValue("__TypeName", this._typeName);
				info.AddValue("__MethodName", this._methodName);
				info.AddValue("__MethodSignature", this._methodSignature);
				info.AddValue("__Uri", this._uri);
				info.AddValue("__Return", this._returnValue);
				info.AddValue("__OutArgs", this._args);
			}
			else
			{
				info.AddValue("__fault", this._exception);
			}
			info.AddValue("__CallContext", this._callContext);
			if (this.InternalProperties != null)
			{
				foreach (object obj in this.InternalProperties)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					info.AddValue((string)dictionaryEntry.Key, dictionaryEntry.Value);
				}
			}
		}

		// Token: 0x0600306A RID: 12394 RVA: 0x0009F704 File Offset: 0x0009D904
		public object GetOutArg(int argNum)
		{
			if (this._args == null)
			{
				return null;
			}
			if (this._inArgInfo == null)
			{
				this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
			}
			return this._args[this._inArgInfo.GetInOutArgIndex(argNum)];
		}

		// Token: 0x0600306B RID: 12395 RVA: 0x0009F744 File Offset: 0x0009D944
		public string GetOutArgName(int index)
		{
			if (this._methodBase == null)
			{
				return "__method_" + index;
			}
			if (this._inArgInfo == null)
			{
				this._inArgInfo = new ArgInfo(this.MethodBase, ArgInfoType.Out);
			}
			return this._inArgInfo.GetInOutArgName(index);
		}

		// Token: 0x0600306C RID: 12396 RVA: 0x0009F798 File Offset: 0x0009D998
		[MonoTODO]
		public virtual object HeaderHandler(Header[] h)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600306D RID: 12397 RVA: 0x0009F7A0 File Offset: 0x0009D9A0
		[MonoTODO]
		public void RootSetObjectData(SerializationInfo info, StreamingContext ctx)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04001475 RID: 5237
		private string _methodName;

		// Token: 0x04001476 RID: 5238
		private string _uri;

		// Token: 0x04001477 RID: 5239
		private string _typeName;

		// Token: 0x04001478 RID: 5240
		private MethodBase _methodBase;

		// Token: 0x04001479 RID: 5241
		private object _returnValue;

		// Token: 0x0400147A RID: 5242
		private Exception _exception;

		// Token: 0x0400147B RID: 5243
		private Type[] _methodSignature;

		// Token: 0x0400147C RID: 5244
		private ArgInfo _inArgInfo;

		// Token: 0x0400147D RID: 5245
		private object[] _args;

		// Token: 0x0400147E RID: 5246
		private object[] _outArgs;

		// Token: 0x0400147F RID: 5247
		private IMethodCallMessage _callMsg;

		// Token: 0x04001480 RID: 5248
		private LogicalCallContext _callContext;

		// Token: 0x04001481 RID: 5249
		private Identity _targetIdentity;

		// Token: 0x04001482 RID: 5250
		protected IDictionary ExternalProperties;

		// Token: 0x04001483 RID: 5251
		protected IDictionary InternalProperties;
	}
}
