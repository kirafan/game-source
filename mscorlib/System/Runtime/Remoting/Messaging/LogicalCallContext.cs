﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004A3 RID: 1187
	[ComVisible(true)]
	[Serializable]
	public sealed class LogicalCallContext : ICloneable, ISerializable
	{
		// Token: 0x06003003 RID: 12291 RVA: 0x0009DFC4 File Offset: 0x0009C1C4
		internal LogicalCallContext()
		{
		}

		// Token: 0x06003004 RID: 12292 RVA: 0x0009DFD8 File Offset: 0x0009C1D8
		internal LogicalCallContext(SerializationInfo info, StreamingContext context)
		{
			foreach (SerializationEntry serializationEntry in info)
			{
				if (serializationEntry.Name == "__RemotingData")
				{
					this._remotingData = (CallContextRemotingData)serializationEntry.Value;
				}
				else
				{
					this.SetData(serializationEntry.Name, serializationEntry.Value);
				}
			}
		}

		// Token: 0x170008C0 RID: 2240
		// (get) Token: 0x06003005 RID: 12293 RVA: 0x0009E058 File Offset: 0x0009C258
		public bool HasInfo
		{
			get
			{
				return this._data != null && this._data.Count > 0;
			}
		}

		// Token: 0x06003006 RID: 12294 RVA: 0x0009E078 File Offset: 0x0009C278
		public void FreeNamedDataSlot(string name)
		{
			if (this._data != null)
			{
				this._data.Remove(name);
			}
		}

		// Token: 0x06003007 RID: 12295 RVA: 0x0009E094 File Offset: 0x0009C294
		public object GetData(string name)
		{
			if (this._data != null)
			{
				return this._data[name];
			}
			return null;
		}

		// Token: 0x06003008 RID: 12296 RVA: 0x0009E0B0 File Offset: 0x0009C2B0
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("__RemotingData", this._remotingData);
			if (this._data != null)
			{
				foreach (object obj in this._data)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					info.AddValue((string)dictionaryEntry.Key, dictionaryEntry.Value);
				}
			}
		}

		// Token: 0x06003009 RID: 12297 RVA: 0x0009E150 File Offset: 0x0009C350
		public void SetData(string name, object data)
		{
			if (this._data == null)
			{
				this._data = new Hashtable();
			}
			this._data[name] = data;
		}

		// Token: 0x0600300A RID: 12298 RVA: 0x0009E178 File Offset: 0x0009C378
		public object Clone()
		{
			LogicalCallContext logicalCallContext = new LogicalCallContext();
			logicalCallContext._remotingData = (CallContextRemotingData)this._remotingData.Clone();
			if (this._data != null)
			{
				logicalCallContext._data = new Hashtable();
				foreach (object obj in this._data)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj;
					logicalCallContext._data[dictionaryEntry.Key] = dictionaryEntry.Value;
				}
			}
			return logicalCallContext;
		}

		// Token: 0x170008C1 RID: 2241
		// (get) Token: 0x0600300B RID: 12299 RVA: 0x0009E22C File Offset: 0x0009C42C
		internal Hashtable Datastore
		{
			get
			{
				return this._data;
			}
		}

		// Token: 0x04001460 RID: 5216
		private Hashtable _data;

		// Token: 0x04001461 RID: 5217
		private CallContextRemotingData _remotingData = new CallContextRemotingData();
	}
}
