﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200049A RID: 1178
	[ComVisible(true)]
	public interface IMessage
	{
		// Token: 0x170008AF RID: 2223
		// (get) Token: 0x06002FE5 RID: 12261
		IDictionary Properties { get; }
	}
}
