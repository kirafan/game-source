﻿using System;
using System.Collections;
using System.Reflection;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000498 RID: 1176
	[Serializable]
	internal class ErrorMessage : IMessage, IMethodCallMessage, IMethodMessage
	{
		// Token: 0x170008A1 RID: 2209
		// (get) Token: 0x06002FD0 RID: 12240 RVA: 0x0009DF58 File Offset: 0x0009C158
		public int ArgCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x170008A2 RID: 2210
		// (get) Token: 0x06002FD1 RID: 12241 RVA: 0x0009DF5C File Offset: 0x0009C15C
		public object[] Args
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170008A3 RID: 2211
		// (get) Token: 0x06002FD2 RID: 12242 RVA: 0x0009DF60 File Offset: 0x0009C160
		public bool HasVarArgs
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170008A4 RID: 2212
		// (get) Token: 0x06002FD3 RID: 12243 RVA: 0x0009DF64 File Offset: 0x0009C164
		public MethodBase MethodBase
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170008A5 RID: 2213
		// (get) Token: 0x06002FD4 RID: 12244 RVA: 0x0009DF68 File Offset: 0x0009C168
		public string MethodName
		{
			get
			{
				return "unknown";
			}
		}

		// Token: 0x170008A6 RID: 2214
		// (get) Token: 0x06002FD5 RID: 12245 RVA: 0x0009DF70 File Offset: 0x0009C170
		public object MethodSignature
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170008A7 RID: 2215
		// (get) Token: 0x06002FD6 RID: 12246 RVA: 0x0009DF74 File Offset: 0x0009C174
		public virtual IDictionary Properties
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170008A8 RID: 2216
		// (get) Token: 0x06002FD7 RID: 12247 RVA: 0x0009DF78 File Offset: 0x0009C178
		public string TypeName
		{
			get
			{
				return "unknown";
			}
		}

		// Token: 0x170008A9 RID: 2217
		// (get) Token: 0x06002FD8 RID: 12248 RVA: 0x0009DF80 File Offset: 0x0009C180
		// (set) Token: 0x06002FD9 RID: 12249 RVA: 0x0009DF88 File Offset: 0x0009C188
		public string Uri
		{
			get
			{
				return this._uri;
			}
			set
			{
				this._uri = value;
			}
		}

		// Token: 0x06002FDA RID: 12250 RVA: 0x0009DF94 File Offset: 0x0009C194
		public object GetArg(int arg_num)
		{
			return null;
		}

		// Token: 0x06002FDB RID: 12251 RVA: 0x0009DF98 File Offset: 0x0009C198
		public string GetArgName(int arg_num)
		{
			return "unknown";
		}

		// Token: 0x170008AA RID: 2218
		// (get) Token: 0x06002FDC RID: 12252 RVA: 0x0009DFA0 File Offset: 0x0009C1A0
		public int InArgCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06002FDD RID: 12253 RVA: 0x0009DFA4 File Offset: 0x0009C1A4
		public string GetInArgName(int index)
		{
			return null;
		}

		// Token: 0x06002FDE RID: 12254 RVA: 0x0009DFA8 File Offset: 0x0009C1A8
		public object GetInArg(int argNum)
		{
			return null;
		}

		// Token: 0x170008AB RID: 2219
		// (get) Token: 0x06002FDF RID: 12255 RVA: 0x0009DFAC File Offset: 0x0009C1AC
		public object[] InArgs
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170008AC RID: 2220
		// (get) Token: 0x06002FE0 RID: 12256 RVA: 0x0009DFB0 File Offset: 0x0009C1B0
		public LogicalCallContext LogicalCallContext
		{
			get
			{
				return null;
			}
		}

		// Token: 0x0400145E RID: 5214
		private string _uri = "Exception";
	}
}
