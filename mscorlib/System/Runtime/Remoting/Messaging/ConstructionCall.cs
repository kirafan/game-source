﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Proxies;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000493 RID: 1171
	[CLSCompliant(false)]
	[ComVisible(true)]
	[Serializable]
	public class ConstructionCall : MethodCall, IConstructionCallMessage, IMessage, IMethodCallMessage, IMethodMessage
	{
		// Token: 0x06002FAB RID: 12203 RVA: 0x0009D9B0 File Offset: 0x0009BBB0
		public ConstructionCall(IMessage m) : base(m)
		{
			this._activationTypeName = this.TypeName;
			this._isContextOk = true;
		}

		// Token: 0x06002FAC RID: 12204 RVA: 0x0009D9CC File Offset: 0x0009BBCC
		internal ConstructionCall(Type type)
		{
			this._activationType = type;
			this._activationTypeName = type.AssemblyQualifiedName;
			this._isContextOk = true;
		}

		// Token: 0x06002FAD RID: 12205 RVA: 0x0009D9FC File Offset: 0x0009BBFC
		public ConstructionCall(Header[] headers) : base(headers)
		{
		}

		// Token: 0x06002FAE RID: 12206 RVA: 0x0009DA08 File Offset: 0x0009BC08
		internal ConstructionCall(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06002FAF RID: 12207 RVA: 0x0009DA14 File Offset: 0x0009BC14
		internal override void InitDictionary()
		{
			ConstructionCallDictionary constructionCallDictionary = new ConstructionCallDictionary(this);
			this.ExternalProperties = constructionCallDictionary;
			this.InternalProperties = constructionCallDictionary.GetInternalProperties();
		}

		// Token: 0x17000897 RID: 2199
		// (get) Token: 0x06002FB0 RID: 12208 RVA: 0x0009DA3C File Offset: 0x0009BC3C
		// (set) Token: 0x06002FB1 RID: 12209 RVA: 0x0009DA44 File Offset: 0x0009BC44
		internal bool IsContextOk
		{
			get
			{
				return this._isContextOk;
			}
			set
			{
				this._isContextOk = value;
			}
		}

		// Token: 0x17000898 RID: 2200
		// (get) Token: 0x06002FB2 RID: 12210 RVA: 0x0009DA50 File Offset: 0x0009BC50
		public Type ActivationType
		{
			get
			{
				if (this._activationType == null)
				{
					this._activationType = Type.GetType(this._activationTypeName);
				}
				return this._activationType;
			}
		}

		// Token: 0x17000899 RID: 2201
		// (get) Token: 0x06002FB3 RID: 12211 RVA: 0x0009DA80 File Offset: 0x0009BC80
		public string ActivationTypeName
		{
			get
			{
				return this._activationTypeName;
			}
		}

		// Token: 0x1700089A RID: 2202
		// (get) Token: 0x06002FB4 RID: 12212 RVA: 0x0009DA88 File Offset: 0x0009BC88
		// (set) Token: 0x06002FB5 RID: 12213 RVA: 0x0009DA90 File Offset: 0x0009BC90
		public IActivator Activator
		{
			get
			{
				return this._activator;
			}
			set
			{
				this._activator = value;
			}
		}

		// Token: 0x1700089B RID: 2203
		// (get) Token: 0x06002FB6 RID: 12214 RVA: 0x0009DA9C File Offset: 0x0009BC9C
		public object[] CallSiteActivationAttributes
		{
			get
			{
				return this._activationAttributes;
			}
		}

		// Token: 0x06002FB7 RID: 12215 RVA: 0x0009DAA4 File Offset: 0x0009BCA4
		internal void SetActivationAttributes(object[] attributes)
		{
			this._activationAttributes = attributes;
		}

		// Token: 0x1700089C RID: 2204
		// (get) Token: 0x06002FB8 RID: 12216 RVA: 0x0009DAB0 File Offset: 0x0009BCB0
		public IList ContextProperties
		{
			get
			{
				if (this._contextProperties == null)
				{
					this._contextProperties = new ArrayList();
				}
				return this._contextProperties;
			}
		}

		// Token: 0x06002FB9 RID: 12217 RVA: 0x0009DAD0 File Offset: 0x0009BCD0
		internal override void InitMethodProperty(string key, object value)
		{
			switch (key)
			{
			case "__Activator":
				this._activator = (IActivator)value;
				return;
			case "__CallSiteActivationAttributes":
				this._activationAttributes = (object[])value;
				return;
			case "__ActivationType":
				this._activationType = (Type)value;
				return;
			case "__ContextProperties":
				this._contextProperties = (IList)value;
				return;
			case "__ActivationTypeName":
				this._activationTypeName = (string)value;
				return;
			}
			base.InitMethodProperty(key, value);
		}

		// Token: 0x06002FBA RID: 12218 RVA: 0x0009DBB4 File Offset: 0x0009BDB4
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			IList list = this._contextProperties;
			if (list != null && list.Count == 0)
			{
				list = null;
			}
			info.AddValue("__Activator", this._activator);
			info.AddValue("__CallSiteActivationAttributes", this._activationAttributes);
			info.AddValue("__ActivationType", null);
			info.AddValue("__ContextProperties", list);
			info.AddValue("__ActivationTypeName", this._activationTypeName);
		}

		// Token: 0x1700089D RID: 2205
		// (get) Token: 0x06002FBB RID: 12219 RVA: 0x0009DC30 File Offset: 0x0009BE30
		public override IDictionary Properties
		{
			get
			{
				return base.Properties;
			}
		}

		// Token: 0x1700089E RID: 2206
		// (get) Token: 0x06002FBC RID: 12220 RVA: 0x0009DC38 File Offset: 0x0009BE38
		// (set) Token: 0x06002FBD RID: 12221 RVA: 0x0009DC40 File Offset: 0x0009BE40
		internal RemotingProxy SourceProxy
		{
			get
			{
				return this._sourceProxy;
			}
			set
			{
				this._sourceProxy = value;
			}
		}

		// Token: 0x0400144E RID: 5198
		private IActivator _activator;

		// Token: 0x0400144F RID: 5199
		private object[] _activationAttributes;

		// Token: 0x04001450 RID: 5200
		private IList _contextProperties;

		// Token: 0x04001451 RID: 5201
		private Type _activationType;

		// Token: 0x04001452 RID: 5202
		private string _activationTypeName;

		// Token: 0x04001453 RID: 5203
		private bool _isContextOk;

		// Token: 0x04001454 RID: 5204
		[NonSerialized]
		private RemotingProxy _sourceProxy;
	}
}
