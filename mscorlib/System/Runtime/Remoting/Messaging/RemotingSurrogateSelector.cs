﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004B2 RID: 1202
	[ComVisible(true)]
	public class RemotingSurrogateSelector : ISurrogateSelector
	{
		// Token: 0x1700091F RID: 2335
		// (get) Token: 0x060030D4 RID: 12500 RVA: 0x000A0A2C File Offset: 0x0009EC2C
		// (set) Token: 0x060030D5 RID: 12501 RVA: 0x000A0A34 File Offset: 0x0009EC34
		public MessageSurrogateFilter Filter
		{
			get
			{
				return this._filter;
			}
			set
			{
				this._filter = value;
			}
		}

		// Token: 0x060030D6 RID: 12502 RVA: 0x000A0A40 File Offset: 0x0009EC40
		public virtual void ChainSelector(ISurrogateSelector selector)
		{
			if (this._next != null)
			{
				selector.ChainSelector(this._next);
			}
			this._next = selector;
		}

		// Token: 0x060030D7 RID: 12503 RVA: 0x000A0A60 File Offset: 0x0009EC60
		public virtual ISurrogateSelector GetNextSelector()
		{
			return this._next;
		}

		// Token: 0x060030D8 RID: 12504 RVA: 0x000A0A68 File Offset: 0x0009EC68
		public object GetRootObject()
		{
			return this._rootObj;
		}

		// Token: 0x060030D9 RID: 12505 RVA: 0x000A0A70 File Offset: 0x0009EC70
		public virtual ISerializationSurrogate GetSurrogate(Type type, StreamingContext context, out ISurrogateSelector ssout)
		{
			if (type.IsMarshalByRef)
			{
				ssout = this;
				return RemotingSurrogateSelector._objRemotingSurrogate;
			}
			if (RemotingSurrogateSelector.s_cachedTypeObjRef.IsAssignableFrom(type))
			{
				ssout = this;
				return RemotingSurrogateSelector._objRefSurrogate;
			}
			if (this._next != null)
			{
				return this._next.GetSurrogate(type, context, out ssout);
			}
			ssout = null;
			return null;
		}

		// Token: 0x060030DA RID: 12506 RVA: 0x000A0AC8 File Offset: 0x0009ECC8
		public void SetRootObject(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException();
			}
			this._rootObj = obj;
		}

		// Token: 0x060030DB RID: 12507 RVA: 0x000A0AE0 File Offset: 0x0009ECE0
		[MonoTODO]
		public virtual void UseSoapFormat()
		{
			throw new NotImplementedException();
		}

		// Token: 0x040014AA RID: 5290
		private static Type s_cachedTypeObjRef = typeof(ObjRef);

		// Token: 0x040014AB RID: 5291
		private static ObjRefSurrogate _objRefSurrogate = new ObjRefSurrogate();

		// Token: 0x040014AC RID: 5292
		private static RemotingSurrogate _objRemotingSurrogate = new RemotingSurrogate();

		// Token: 0x040014AD RID: 5293
		private object _rootObj;

		// Token: 0x040014AE RID: 5294
		private MessageSurrogateFilter _filter;

		// Token: 0x040014AF RID: 5295
		private ISurrogateSelector _next;
	}
}
