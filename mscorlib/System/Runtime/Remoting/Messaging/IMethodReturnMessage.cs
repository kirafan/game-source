﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200049F RID: 1183
	[ComVisible(true)]
	public interface IMethodReturnMessage : IMessage, IMethodMessage
	{
		// Token: 0x170008BC RID: 2236
		// (get) Token: 0x06002FF9 RID: 12281
		Exception Exception { get; }

		// Token: 0x170008BD RID: 2237
		// (get) Token: 0x06002FFA RID: 12282
		int OutArgCount { get; }

		// Token: 0x170008BE RID: 2238
		// (get) Token: 0x06002FFB RID: 12283
		object[] OutArgs { get; }

		// Token: 0x170008BF RID: 2239
		// (get) Token: 0x06002FFC RID: 12284
		object ReturnValue { get; }

		// Token: 0x06002FFD RID: 12285
		object GetOutArg(int argNum);

		// Token: 0x06002FFE RID: 12286
		string GetOutArgName(int index);
	}
}
