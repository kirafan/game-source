﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004BB RID: 1211
	internal class CADObjRef
	{
		// Token: 0x0600310E RID: 12558 RVA: 0x000A1274 File Offset: 0x0009F474
		public CADObjRef(ObjRef o, int sourceDomain)
		{
			this.objref = o;
			this.SourceDomain = sourceDomain;
		}

		// Token: 0x17000934 RID: 2356
		// (get) Token: 0x0600310F RID: 12559 RVA: 0x000A128C File Offset: 0x0009F48C
		public string TypeName
		{
			get
			{
				return this.objref.TypeInfo.TypeName;
			}
		}

		// Token: 0x17000935 RID: 2357
		// (get) Token: 0x06003110 RID: 12560 RVA: 0x000A12A0 File Offset: 0x0009F4A0
		public string URI
		{
			get
			{
				return this.objref.URI;
			}
		}

		// Token: 0x040014C4 RID: 5316
		private ObjRef objref;

		// Token: 0x040014C5 RID: 5317
		public int SourceDomain;
	}
}
