﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200048C RID: 1164
	internal enum ArgInfoType : byte
	{
		// Token: 0x04001435 RID: 5173
		In,
		// Token: 0x04001436 RID: 5174
		Out
	}
}
