﻿using System;
using System.Runtime.InteropServices;
using System.Threading;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200048E RID: 1166
	[ComVisible(true)]
	public class AsyncResult : IAsyncResult, IMessageSink
	{
		// Token: 0x06002F83 RID: 12163 RVA: 0x0009D430 File Offset: 0x0009B630
		internal AsyncResult()
		{
		}

		// Token: 0x1700088B RID: 2187
		// (get) Token: 0x06002F84 RID: 12164 RVA: 0x0009D438 File Offset: 0x0009B638
		public virtual object AsyncState
		{
			get
			{
				return this.async_state;
			}
		}

		// Token: 0x1700088C RID: 2188
		// (get) Token: 0x06002F85 RID: 12165 RVA: 0x0009D440 File Offset: 0x0009B640
		public virtual WaitHandle AsyncWaitHandle
		{
			get
			{
				WaitHandle result;
				lock (this)
				{
					if (this.handle == null)
					{
						this.handle = new ManualResetEvent(this.completed);
					}
					result = this.handle;
				}
				return result;
			}
		}

		// Token: 0x1700088D RID: 2189
		// (get) Token: 0x06002F86 RID: 12166 RVA: 0x0009D4A8 File Offset: 0x0009B6A8
		public virtual bool CompletedSynchronously
		{
			get
			{
				return this.sync_completed;
			}
		}

		// Token: 0x1700088E RID: 2190
		// (get) Token: 0x06002F87 RID: 12167 RVA: 0x0009D4B0 File Offset: 0x0009B6B0
		public virtual bool IsCompleted
		{
			get
			{
				return this.completed;
			}
		}

		// Token: 0x1700088F RID: 2191
		// (get) Token: 0x06002F88 RID: 12168 RVA: 0x0009D4B8 File Offset: 0x0009B6B8
		// (set) Token: 0x06002F89 RID: 12169 RVA: 0x0009D4C0 File Offset: 0x0009B6C0
		public bool EndInvokeCalled
		{
			get
			{
				return this.endinvoke_called;
			}
			set
			{
				this.endinvoke_called = value;
			}
		}

		// Token: 0x17000890 RID: 2192
		// (get) Token: 0x06002F8A RID: 12170 RVA: 0x0009D4CC File Offset: 0x0009B6CC
		public virtual object AsyncDelegate
		{
			get
			{
				return this.async_delegate;
			}
		}

		// Token: 0x17000891 RID: 2193
		// (get) Token: 0x06002F8B RID: 12171 RVA: 0x0009D4D4 File Offset: 0x0009B6D4
		public IMessageSink NextSink
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06002F8C RID: 12172 RVA: 0x0009D4D8 File Offset: 0x0009B6D8
		public virtual IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002F8D RID: 12173 RVA: 0x0009D4E0 File Offset: 0x0009B6E0
		public virtual IMessage GetReplyMessage()
		{
			return this.reply_message;
		}

		// Token: 0x06002F8E RID: 12174 RVA: 0x0009D4E8 File Offset: 0x0009B6E8
		public virtual void SetMessageCtrl(IMessageCtrl mc)
		{
			this.message_ctrl = mc;
		}

		// Token: 0x06002F8F RID: 12175 RVA: 0x0009D4F4 File Offset: 0x0009B6F4
		internal void SetCompletedSynchronously(bool completed)
		{
			this.sync_completed = completed;
		}

		// Token: 0x06002F90 RID: 12176 RVA: 0x0009D500 File Offset: 0x0009B700
		internal IMessage EndInvoke()
		{
			lock (this)
			{
				if (this.completed)
				{
					return this.reply_message;
				}
			}
			this.AsyncWaitHandle.WaitOne();
			return this.reply_message;
		}

		// Token: 0x06002F91 RID: 12177 RVA: 0x0009D568 File Offset: 0x0009B768
		public virtual IMessage SyncProcessMessage(IMessage msg)
		{
			this.reply_message = msg;
			lock (this)
			{
				this.completed = true;
				if (this.handle != null)
				{
					((ManualResetEvent)this.AsyncWaitHandle).Set();
				}
			}
			if (this.async_callback != null)
			{
				AsyncCallback asyncCallback = (AsyncCallback)this.async_callback;
				asyncCallback(this);
			}
			return null;
		}

		// Token: 0x17000892 RID: 2194
		// (get) Token: 0x06002F92 RID: 12178 RVA: 0x0009D5F0 File Offset: 0x0009B7F0
		// (set) Token: 0x06002F93 RID: 12179 RVA: 0x0009D5F8 File Offset: 0x0009B7F8
		internal MonoMethodMessage CallMessage
		{
			get
			{
				return this.call_message;
			}
			set
			{
				this.call_message = value;
			}
		}

		// Token: 0x0400143A RID: 5178
		private object async_state;

		// Token: 0x0400143B RID: 5179
		private WaitHandle handle;

		// Token: 0x0400143C RID: 5180
		private object async_delegate;

		// Token: 0x0400143D RID: 5181
		private IntPtr data;

		// Token: 0x0400143E RID: 5182
		private object object_data;

		// Token: 0x0400143F RID: 5183
		private bool sync_completed;

		// Token: 0x04001440 RID: 5184
		private bool completed;

		// Token: 0x04001441 RID: 5185
		private bool endinvoke_called;

		// Token: 0x04001442 RID: 5186
		private object async_callback;

		// Token: 0x04001443 RID: 5187
		private ExecutionContext current;

		// Token: 0x04001444 RID: 5188
		private ExecutionContext original;

		// Token: 0x04001445 RID: 5189
		private int gchandle;

		// Token: 0x04001446 RID: 5190
		private MonoMethodMessage call_message;

		// Token: 0x04001447 RID: 5191
		private IMessageCtrl message_ctrl;

		// Token: 0x04001448 RID: 5192
		private IMessage reply_message;
	}
}
