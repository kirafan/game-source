﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004B0 RID: 1200
	internal enum CallType
	{
		// Token: 0x040014A6 RID: 5286
		Sync,
		// Token: 0x040014A7 RID: 5287
		BeginInvoke,
		// Token: 0x040014A8 RID: 5288
		EndInvoke,
		// Token: 0x040014A9 RID: 5289
		OneWay
	}
}
