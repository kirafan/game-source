﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000497 RID: 1175
	[ComVisible(true)]
	[Serializable]
	public class Header
	{
		// Token: 0x06002FCC RID: 12236 RVA: 0x0009DF04 File Offset: 0x0009C104
		public Header(string _Name, object _Value) : this(_Name, _Value, true)
		{
		}

		// Token: 0x06002FCD RID: 12237 RVA: 0x0009DF10 File Offset: 0x0009C110
		public Header(string _Name, object _Value, bool _MustUnderstand) : this(_Name, _Value, _MustUnderstand, null)
		{
		}

		// Token: 0x06002FCE RID: 12238 RVA: 0x0009DF1C File Offset: 0x0009C11C
		public Header(string _Name, object _Value, bool _MustUnderstand, string _HeaderNamespace)
		{
			this.Name = _Name;
			this.Value = _Value;
			this.MustUnderstand = _MustUnderstand;
			this.HeaderNamespace = _HeaderNamespace;
		}

		// Token: 0x0400145A RID: 5210
		public string HeaderNamespace;

		// Token: 0x0400145B RID: 5211
		public bool MustUnderstand;

		// Token: 0x0400145C RID: 5212
		public string Name;

		// Token: 0x0400145D RID: 5213
		public object Value;
	}
}
