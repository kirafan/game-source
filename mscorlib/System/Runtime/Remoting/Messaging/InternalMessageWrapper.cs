﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004A1 RID: 1185
	[ComVisible(true)]
	public class InternalMessageWrapper
	{
		// Token: 0x06003001 RID: 12289 RVA: 0x0009DFB4 File Offset: 0x0009C1B4
		public InternalMessageWrapper(IMessage msg)
		{
			this.WrappedMessage = msg;
		}

		// Token: 0x0400145F RID: 5215
		protected IMessage WrappedMessage;
	}
}
