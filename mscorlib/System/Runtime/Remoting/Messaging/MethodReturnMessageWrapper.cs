﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004AD RID: 1197
	[ComVisible(true)]
	public class MethodReturnMessageWrapper : InternalMessageWrapper, IMessage, IMethodMessage, IMethodReturnMessage
	{
		// Token: 0x06003094 RID: 12436 RVA: 0x000A0154 File Offset: 0x0009E354
		public MethodReturnMessageWrapper(IMethodReturnMessage msg) : base(msg)
		{
			if (msg.Exception != null)
			{
				this._exception = msg.Exception;
				this._args = new object[0];
			}
			else
			{
				this._args = msg.Args;
				this._return = msg.ReturnValue;
				if (msg.MethodBase != null)
				{
					this._outArgInfo = new ArgInfo(msg.MethodBase, ArgInfoType.Out);
				}
			}
		}

		// Token: 0x170008FD RID: 2301
		// (get) Token: 0x06003095 RID: 12437 RVA: 0x000A01C8 File Offset: 0x0009E3C8
		public virtual int ArgCount
		{
			get
			{
				return this._args.Length;
			}
		}

		// Token: 0x170008FE RID: 2302
		// (get) Token: 0x06003096 RID: 12438 RVA: 0x000A01D4 File Offset: 0x0009E3D4
		// (set) Token: 0x06003097 RID: 12439 RVA: 0x000A01DC File Offset: 0x0009E3DC
		public virtual object[] Args
		{
			get
			{
				return this._args;
			}
			set
			{
				this._args = value;
			}
		}

		// Token: 0x170008FF RID: 2303
		// (get) Token: 0x06003098 RID: 12440 RVA: 0x000A01E8 File Offset: 0x0009E3E8
		// (set) Token: 0x06003099 RID: 12441 RVA: 0x000A01F0 File Offset: 0x0009E3F0
		public virtual Exception Exception
		{
			get
			{
				return this._exception;
			}
			set
			{
				this._exception = value;
			}
		}

		// Token: 0x17000900 RID: 2304
		// (get) Token: 0x0600309A RID: 12442 RVA: 0x000A01FC File Offset: 0x0009E3FC
		public virtual bool HasVarArgs
		{
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).HasVarArgs;
			}
		}

		// Token: 0x17000901 RID: 2305
		// (get) Token: 0x0600309B RID: 12443 RVA: 0x000A0210 File Offset: 0x0009E410
		public virtual LogicalCallContext LogicalCallContext
		{
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).LogicalCallContext;
			}
		}

		// Token: 0x17000902 RID: 2306
		// (get) Token: 0x0600309C RID: 12444 RVA: 0x000A0224 File Offset: 0x0009E424
		public virtual MethodBase MethodBase
		{
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).MethodBase;
			}
		}

		// Token: 0x17000903 RID: 2307
		// (get) Token: 0x0600309D RID: 12445 RVA: 0x000A0238 File Offset: 0x0009E438
		public virtual string MethodName
		{
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).MethodName;
			}
		}

		// Token: 0x17000904 RID: 2308
		// (get) Token: 0x0600309E RID: 12446 RVA: 0x000A024C File Offset: 0x0009E44C
		public virtual object MethodSignature
		{
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).MethodSignature;
			}
		}

		// Token: 0x17000905 RID: 2309
		// (get) Token: 0x0600309F RID: 12447 RVA: 0x000A0260 File Offset: 0x0009E460
		public virtual int OutArgCount
		{
			get
			{
				return (this._outArgInfo == null) ? 0 : this._outArgInfo.GetInOutArgCount();
			}
		}

		// Token: 0x17000906 RID: 2310
		// (get) Token: 0x060030A0 RID: 12448 RVA: 0x000A0280 File Offset: 0x0009E480
		public virtual object[] OutArgs
		{
			get
			{
				return (this._outArgInfo == null) ? this._args : this._outArgInfo.GetInOutArgs(this._args);
			}
		}

		// Token: 0x17000907 RID: 2311
		// (get) Token: 0x060030A1 RID: 12449 RVA: 0x000A02AC File Offset: 0x0009E4AC
		public virtual IDictionary Properties
		{
			get
			{
				if (this._properties == null)
				{
					this._properties = new MethodReturnMessageWrapper.DictionaryWrapper(this, this.WrappedMessage.Properties);
				}
				return this._properties;
			}
		}

		// Token: 0x17000908 RID: 2312
		// (get) Token: 0x060030A2 RID: 12450 RVA: 0x000A02E4 File Offset: 0x0009E4E4
		// (set) Token: 0x060030A3 RID: 12451 RVA: 0x000A02EC File Offset: 0x0009E4EC
		public virtual object ReturnValue
		{
			get
			{
				return this._return;
			}
			set
			{
				this._return = value;
			}
		}

		// Token: 0x17000909 RID: 2313
		// (get) Token: 0x060030A4 RID: 12452 RVA: 0x000A02F8 File Offset: 0x0009E4F8
		public virtual string TypeName
		{
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).TypeName;
			}
		}

		// Token: 0x1700090A RID: 2314
		// (get) Token: 0x060030A5 RID: 12453 RVA: 0x000A030C File Offset: 0x0009E50C
		// (set) Token: 0x060030A6 RID: 12454 RVA: 0x000A0320 File Offset: 0x0009E520
		public string Uri
		{
			get
			{
				return ((IMethodReturnMessage)this.WrappedMessage).Uri;
			}
			set
			{
				this.Properties["__Uri"] = value;
			}
		}

		// Token: 0x060030A7 RID: 12455 RVA: 0x000A0334 File Offset: 0x0009E534
		public virtual object GetArg(int argNum)
		{
			return this._args[argNum];
		}

		// Token: 0x060030A8 RID: 12456 RVA: 0x000A0340 File Offset: 0x0009E540
		public virtual string GetArgName(int index)
		{
			return ((IMethodReturnMessage)this.WrappedMessage).GetArgName(index);
		}

		// Token: 0x060030A9 RID: 12457 RVA: 0x000A0354 File Offset: 0x0009E554
		public virtual object GetOutArg(int argNum)
		{
			return this._args[this._outArgInfo.GetInOutArgIndex(argNum)];
		}

		// Token: 0x060030AA RID: 12458 RVA: 0x000A036C File Offset: 0x0009E56C
		public virtual string GetOutArgName(int index)
		{
			return this._outArgInfo.GetInOutArgName(index);
		}

		// Token: 0x04001491 RID: 5265
		private object[] _args;

		// Token: 0x04001492 RID: 5266
		private ArgInfo _outArgInfo;

		// Token: 0x04001493 RID: 5267
		private MethodReturnMessageWrapper.DictionaryWrapper _properties;

		// Token: 0x04001494 RID: 5268
		private Exception _exception;

		// Token: 0x04001495 RID: 5269
		private object _return;

		// Token: 0x020004AE RID: 1198
		private class DictionaryWrapper : MethodReturnDictionary
		{
			// Token: 0x060030AB RID: 12459 RVA: 0x000A037C File Offset: 0x0009E57C
			public DictionaryWrapper(IMethodReturnMessage message, IDictionary wrappedDictionary) : base(message)
			{
				this._wrappedDictionary = wrappedDictionary;
				base.MethodKeys = MethodReturnMessageWrapper.DictionaryWrapper._keys;
			}

			// Token: 0x060030AD RID: 12461 RVA: 0x000A03B8 File Offset: 0x0009E5B8
			protected override IDictionary AllocInternalProperties()
			{
				return this._wrappedDictionary;
			}

			// Token: 0x060030AE RID: 12462 RVA: 0x000A03C0 File Offset: 0x0009E5C0
			protected override void SetMethodProperty(string key, object value)
			{
				if (key == "__Args")
				{
					((MethodReturnMessageWrapper)this._message)._args = (object[])value;
				}
				else if (key == "__Return")
				{
					((MethodReturnMessageWrapper)this._message)._return = value;
				}
				else
				{
					base.SetMethodProperty(key, value);
				}
			}

			// Token: 0x060030AF RID: 12463 RVA: 0x000A0428 File Offset: 0x0009E628
			protected override object GetMethodProperty(string key)
			{
				if (key == "__Args")
				{
					return ((MethodReturnMessageWrapper)this._message)._args;
				}
				if (key == "__Return")
				{
					return ((MethodReturnMessageWrapper)this._message)._return;
				}
				return base.GetMethodProperty(key);
			}

			// Token: 0x04001496 RID: 5270
			private IDictionary _wrappedDictionary;

			// Token: 0x04001497 RID: 5271
			private static string[] _keys = new string[]
			{
				"__Args",
				"__Return"
			};
		}
	}
}
