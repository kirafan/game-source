﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x02000495 RID: 1173
	[CLSCompliant(false)]
	[ComVisible(true)]
	[Serializable]
	public class ConstructionResponse : MethodResponse, IConstructionReturnMessage, IMessage, IMethodMessage, IMethodReturnMessage
	{
		// Token: 0x06002FC2 RID: 12226 RVA: 0x0009DE8C File Offset: 0x0009C08C
		public ConstructionResponse(Header[] h, IMethodCallMessage mcm) : base(h, mcm)
		{
		}

		// Token: 0x06002FC3 RID: 12227 RVA: 0x0009DE98 File Offset: 0x0009C098
		internal ConstructionResponse(object resultObject, LogicalCallContext callCtx, IMethodCallMessage msg) : base(resultObject, null, callCtx, msg)
		{
		}

		// Token: 0x06002FC4 RID: 12228 RVA: 0x0009DEA4 File Offset: 0x0009C0A4
		internal ConstructionResponse(Exception e, IMethodCallMessage msg) : base(e, msg)
		{
		}

		// Token: 0x06002FC5 RID: 12229 RVA: 0x0009DEB0 File Offset: 0x0009C0B0
		internal ConstructionResponse(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x1700089F RID: 2207
		// (get) Token: 0x06002FC6 RID: 12230 RVA: 0x0009DEBC File Offset: 0x0009C0BC
		public override IDictionary Properties
		{
			get
			{
				return base.Properties;
			}
		}
	}
}
