﻿using System;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x020004BA RID: 1210
	internal class CADArgHolder
	{
		// Token: 0x0600310D RID: 12557 RVA: 0x000A1264 File Offset: 0x0009F464
		public CADArgHolder(int i)
		{
			this.index = i;
		}

		// Token: 0x040014C3 RID: 5315
		public int index;
	}
}
