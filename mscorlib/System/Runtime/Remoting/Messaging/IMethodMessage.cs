﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200049E RID: 1182
	[ComVisible(true)]
	public interface IMethodMessage : IMessage
	{
		// Token: 0x170008B3 RID: 2227
		// (get) Token: 0x06002FEE RID: 12270
		int ArgCount { get; }

		// Token: 0x170008B4 RID: 2228
		// (get) Token: 0x06002FEF RID: 12271
		object[] Args { get; }

		// Token: 0x170008B5 RID: 2229
		// (get) Token: 0x06002FF0 RID: 12272
		bool HasVarArgs { get; }

		// Token: 0x170008B6 RID: 2230
		// (get) Token: 0x06002FF1 RID: 12273
		LogicalCallContext LogicalCallContext { get; }

		// Token: 0x170008B7 RID: 2231
		// (get) Token: 0x06002FF2 RID: 12274
		MethodBase MethodBase { get; }

		// Token: 0x170008B8 RID: 2232
		// (get) Token: 0x06002FF3 RID: 12275
		string MethodName { get; }

		// Token: 0x170008B9 RID: 2233
		// (get) Token: 0x06002FF4 RID: 12276
		object MethodSignature { get; }

		// Token: 0x170008BA RID: 2234
		// (get) Token: 0x06002FF5 RID: 12277
		string TypeName { get; }

		// Token: 0x170008BB RID: 2235
		// (get) Token: 0x06002FF6 RID: 12278
		string Uri { get; }

		// Token: 0x06002FF7 RID: 12279
		object GetArg(int argNum);

		// Token: 0x06002FF8 RID: 12280
		string GetArgName(int index);
	}
}
