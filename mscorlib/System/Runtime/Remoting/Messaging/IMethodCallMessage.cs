﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Messaging
{
	// Token: 0x0200049D RID: 1181
	[ComVisible(true)]
	public interface IMethodCallMessage : IMessage, IMethodMessage
	{
		// Token: 0x170008B1 RID: 2225
		// (get) Token: 0x06002FEA RID: 12266
		int InArgCount { get; }

		// Token: 0x170008B2 RID: 2226
		// (get) Token: 0x06002FEB RID: 12267
		object[] InArgs { get; }

		// Token: 0x06002FEC RID: 12268
		object GetInArg(int argNum);

		// Token: 0x06002FED RID: 12269
		string GetInArgName(int index);
	}
}
