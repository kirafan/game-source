﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting
{
	// Token: 0x02000429 RID: 1065
	[ComVisible(true)]
	[Serializable]
	public class RemotingTimeoutException : RemotingException
	{
		// Token: 0x06002D42 RID: 11586 RVA: 0x000967B8 File Offset: 0x000949B8
		public RemotingTimeoutException()
		{
		}

		// Token: 0x06002D43 RID: 11587 RVA: 0x000967C0 File Offset: 0x000949C0
		public RemotingTimeoutException(string message) : base(message)
		{
		}

		// Token: 0x06002D44 RID: 11588 RVA: 0x000967CC File Offset: 0x000949CC
		public RemotingTimeoutException(string message, Exception InnerException) : base(message, InnerException)
		{
		}

		// Token: 0x06002D45 RID: 11589 RVA: 0x000967D8 File Offset: 0x000949D8
		internal RemotingTimeoutException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
