﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Lifetime
{
	// Token: 0x02000484 RID: 1156
	[ComVisible(true)]
	public class ClientSponsor : MarshalByRefObject, ISponsor
	{
		// Token: 0x06002F3E RID: 12094 RVA: 0x0009C904 File Offset: 0x0009AB04
		public ClientSponsor()
		{
			this.renewal_time = new TimeSpan(0, 2, 0);
		}

		// Token: 0x06002F3F RID: 12095 RVA: 0x0009C928 File Offset: 0x0009AB28
		public ClientSponsor(TimeSpan renewalTime)
		{
			this.renewal_time = renewalTime;
		}

		// Token: 0x1700087B RID: 2171
		// (get) Token: 0x06002F40 RID: 12096 RVA: 0x0009C944 File Offset: 0x0009AB44
		// (set) Token: 0x06002F41 RID: 12097 RVA: 0x0009C94C File Offset: 0x0009AB4C
		public TimeSpan RenewalTime
		{
			get
			{
				return this.renewal_time;
			}
			set
			{
				this.renewal_time = value;
			}
		}

		// Token: 0x06002F42 RID: 12098 RVA: 0x0009C958 File Offset: 0x0009AB58
		public void Close()
		{
			foreach (object obj in this.registered_objects.Values)
			{
				MarshalByRefObject marshalByRefObject = (MarshalByRefObject)obj;
				ILease lease = marshalByRefObject.GetLifetimeService() as ILease;
				lease.Unregister(this);
			}
			this.registered_objects.Clear();
		}

		// Token: 0x06002F43 RID: 12099 RVA: 0x0009C9E4 File Offset: 0x0009ABE4
		~ClientSponsor()
		{
			this.Close();
		}

		// Token: 0x06002F44 RID: 12100 RVA: 0x0009CA20 File Offset: 0x0009AC20
		public override object InitializeLifetimeService()
		{
			return base.InitializeLifetimeService();
		}

		// Token: 0x06002F45 RID: 12101 RVA: 0x0009CA28 File Offset: 0x0009AC28
		public bool Register(MarshalByRefObject obj)
		{
			if (this.registered_objects.ContainsKey(obj))
			{
				return false;
			}
			ILease lease = obj.GetLifetimeService() as ILease;
			if (lease == null)
			{
				return false;
			}
			lease.Register(this);
			this.registered_objects.Add(obj, obj);
			return true;
		}

		// Token: 0x06002F46 RID: 12102 RVA: 0x0009CA74 File Offset: 0x0009AC74
		public TimeSpan Renewal(ILease lease)
		{
			return this.renewal_time;
		}

		// Token: 0x06002F47 RID: 12103 RVA: 0x0009CA7C File Offset: 0x0009AC7C
		public void Unregister(MarshalByRefObject obj)
		{
			if (!this.registered_objects.ContainsKey(obj))
			{
				return;
			}
			ILease lease = obj.GetLifetimeService() as ILease;
			lease.Unregister(this);
			this.registered_objects.Remove(obj);
		}

		// Token: 0x0400141C RID: 5148
		private TimeSpan renewal_time;

		// Token: 0x0400141D RID: 5149
		private Hashtable registered_objects = new Hashtable();
	}
}
