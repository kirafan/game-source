﻿using System;
using System.Collections;
using System.Threading;

namespace System.Runtime.Remoting.Lifetime
{
	// Token: 0x02000487 RID: 1159
	internal class Lease : MarshalByRefObject, ILease
	{
		// Token: 0x06002F55 RID: 12117 RVA: 0x0009CABC File Offset: 0x0009ACBC
		public Lease()
		{
			this._currentState = LeaseState.Initial;
			this._initialLeaseTime = LifetimeServices.LeaseTime;
			this._renewOnCallTime = LifetimeServices.RenewOnCallTime;
			this._sponsorshipTimeout = LifetimeServices.SponsorshipTimeout;
			this._leaseExpireTime = DateTime.Now + this._initialLeaseTime;
		}

		// Token: 0x17000881 RID: 2177
		// (get) Token: 0x06002F56 RID: 12118 RVA: 0x0009CB10 File Offset: 0x0009AD10
		public TimeSpan CurrentLeaseTime
		{
			get
			{
				return this._leaseExpireTime - DateTime.Now;
			}
		}

		// Token: 0x17000882 RID: 2178
		// (get) Token: 0x06002F57 RID: 12119 RVA: 0x0009CB24 File Offset: 0x0009AD24
		public LeaseState CurrentState
		{
			get
			{
				return this._currentState;
			}
		}

		// Token: 0x06002F58 RID: 12120 RVA: 0x0009CB2C File Offset: 0x0009AD2C
		public void Activate()
		{
			this._currentState = LeaseState.Active;
		}

		// Token: 0x17000883 RID: 2179
		// (get) Token: 0x06002F59 RID: 12121 RVA: 0x0009CB38 File Offset: 0x0009AD38
		// (set) Token: 0x06002F5A RID: 12122 RVA: 0x0009CB40 File Offset: 0x0009AD40
		public TimeSpan InitialLeaseTime
		{
			get
			{
				return this._initialLeaseTime;
			}
			set
			{
				if (this._currentState != LeaseState.Initial)
				{
					throw new RemotingException("InitialLeaseTime property can only be set when the lease is in initial state; state is " + this._currentState + ".");
				}
				this._initialLeaseTime = value;
				this._leaseExpireTime = DateTime.Now + this._initialLeaseTime;
				if (value == TimeSpan.Zero)
				{
					this._currentState = LeaseState.Null;
				}
			}
		}

		// Token: 0x17000884 RID: 2180
		// (get) Token: 0x06002F5B RID: 12123 RVA: 0x0009CBB0 File Offset: 0x0009ADB0
		// (set) Token: 0x06002F5C RID: 12124 RVA: 0x0009CBB8 File Offset: 0x0009ADB8
		public TimeSpan RenewOnCallTime
		{
			get
			{
				return this._renewOnCallTime;
			}
			set
			{
				if (this._currentState != LeaseState.Initial)
				{
					throw new RemotingException("RenewOnCallTime property can only be set when the lease is in initial state; state is " + this._currentState + ".");
				}
				this._renewOnCallTime = value;
			}
		}

		// Token: 0x17000885 RID: 2181
		// (get) Token: 0x06002F5D RID: 12125 RVA: 0x0009CBF0 File Offset: 0x0009ADF0
		// (set) Token: 0x06002F5E RID: 12126 RVA: 0x0009CBF8 File Offset: 0x0009ADF8
		public TimeSpan SponsorshipTimeout
		{
			get
			{
				return this._sponsorshipTimeout;
			}
			set
			{
				if (this._currentState != LeaseState.Initial)
				{
					throw new RemotingException("SponsorshipTimeout property can only be set when the lease is in initial state; state is " + this._currentState + ".");
				}
				this._sponsorshipTimeout = value;
			}
		}

		// Token: 0x06002F5F RID: 12127 RVA: 0x0009CC30 File Offset: 0x0009AE30
		public void Register(ISponsor obj)
		{
			this.Register(obj, TimeSpan.Zero);
		}

		// Token: 0x06002F60 RID: 12128 RVA: 0x0009CC40 File Offset: 0x0009AE40
		public void Register(ISponsor obj, TimeSpan renewalTime)
		{
			lock (this)
			{
				if (this._sponsors == null)
				{
					this._sponsors = new ArrayList();
				}
				this._sponsors.Add(obj);
			}
			if (renewalTime != TimeSpan.Zero)
			{
				this.Renew(renewalTime);
			}
		}

		// Token: 0x06002F61 RID: 12129 RVA: 0x0009CCB8 File Offset: 0x0009AEB8
		public TimeSpan Renew(TimeSpan renewalTime)
		{
			DateTime dateTime = DateTime.Now + renewalTime;
			if (dateTime > this._leaseExpireTime)
			{
				this._leaseExpireTime = dateTime;
			}
			return this.CurrentLeaseTime;
		}

		// Token: 0x06002F62 RID: 12130 RVA: 0x0009CCF0 File Offset: 0x0009AEF0
		public void Unregister(ISponsor obj)
		{
			lock (this)
			{
				if (this._sponsors != null)
				{
					for (int i = 0; i < this._sponsors.Count; i++)
					{
						if (object.ReferenceEquals(this._sponsors[i], obj))
						{
							this._sponsors.RemoveAt(i);
							break;
						}
					}
				}
			}
		}

		// Token: 0x06002F63 RID: 12131 RVA: 0x0009CD84 File Offset: 0x0009AF84
		internal void UpdateState()
		{
			if (this._currentState != LeaseState.Active)
			{
				return;
			}
			if (this.CurrentLeaseTime > TimeSpan.Zero)
			{
				return;
			}
			if (this._sponsors != null)
			{
				this._currentState = LeaseState.Renewing;
				lock (this)
				{
					this._renewingSponsors = new Queue(this._sponsors);
				}
				this.CheckNextSponsor();
			}
			else
			{
				this._currentState = LeaseState.Expired;
			}
		}

		// Token: 0x06002F64 RID: 12132 RVA: 0x0009CE1C File Offset: 0x0009B01C
		private void CheckNextSponsor()
		{
			if (this._renewingSponsors.Count == 0)
			{
				this._currentState = LeaseState.Expired;
				this._renewingSponsors = null;
				return;
			}
			ISponsor @object = (ISponsor)this._renewingSponsors.Peek();
			this._renewalDelegate = new Lease.RenewalDelegate(@object.Renewal);
			IAsyncResult asyncResult = this._renewalDelegate.BeginInvoke(this, null, null);
			ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, new WaitOrTimerCallback(this.ProcessSponsorResponse), asyncResult, this._sponsorshipTimeout, true);
		}

		// Token: 0x06002F65 RID: 12133 RVA: 0x0009CE9C File Offset: 0x0009B09C
		private void ProcessSponsorResponse(object state, bool timedOut)
		{
			if (!timedOut)
			{
				try
				{
					IAsyncResult result = (IAsyncResult)state;
					TimeSpan timeSpan = this._renewalDelegate.EndInvoke(result);
					if (timeSpan != TimeSpan.Zero)
					{
						this.Renew(timeSpan);
						this._currentState = LeaseState.Active;
						this._renewingSponsors = null;
						return;
					}
				}
				catch
				{
				}
			}
			this.Unregister((ISponsor)this._renewingSponsors.Dequeue());
			this.CheckNextSponsor();
		}

		// Token: 0x0400141E RID: 5150
		private DateTime _leaseExpireTime;

		// Token: 0x0400141F RID: 5151
		private LeaseState _currentState;

		// Token: 0x04001420 RID: 5152
		private TimeSpan _initialLeaseTime;

		// Token: 0x04001421 RID: 5153
		private TimeSpan _renewOnCallTime;

		// Token: 0x04001422 RID: 5154
		private TimeSpan _sponsorshipTimeout;

		// Token: 0x04001423 RID: 5155
		private ArrayList _sponsors;

		// Token: 0x04001424 RID: 5156
		private Queue _renewingSponsors;

		// Token: 0x04001425 RID: 5157
		private Lease.RenewalDelegate _renewalDelegate;

		// Token: 0x020006E2 RID: 1762
		// (Invoke) Token: 0x0600437C RID: 17276
		private delegate TimeSpan RenewalDelegate(ILease lease);
	}
}
