﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Lifetime
{
	// Token: 0x02000485 RID: 1157
	[ComVisible(true)]
	public interface ILease
	{
		// Token: 0x1700087C RID: 2172
		// (get) Token: 0x06002F48 RID: 12104
		TimeSpan CurrentLeaseTime { get; }

		// Token: 0x1700087D RID: 2173
		// (get) Token: 0x06002F49 RID: 12105
		LeaseState CurrentState { get; }

		// Token: 0x1700087E RID: 2174
		// (get) Token: 0x06002F4A RID: 12106
		// (set) Token: 0x06002F4B RID: 12107
		TimeSpan InitialLeaseTime { get; set; }

		// Token: 0x1700087F RID: 2175
		// (get) Token: 0x06002F4C RID: 12108
		// (set) Token: 0x06002F4D RID: 12109
		TimeSpan RenewOnCallTime { get; set; }

		// Token: 0x17000880 RID: 2176
		// (get) Token: 0x06002F4E RID: 12110
		// (set) Token: 0x06002F4F RID: 12111
		TimeSpan SponsorshipTimeout { get; set; }

		// Token: 0x06002F50 RID: 12112
		void Register(ISponsor obj);

		// Token: 0x06002F51 RID: 12113
		void Register(ISponsor obj, TimeSpan renewalTime);

		// Token: 0x06002F52 RID: 12114
		TimeSpan Renew(TimeSpan renewalTime);

		// Token: 0x06002F53 RID: 12115
		void Unregister(ISponsor obj);
	}
}
