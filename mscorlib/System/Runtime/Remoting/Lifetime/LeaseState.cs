﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Lifetime
{
	// Token: 0x0200048A RID: 1162
	[ComVisible(true)]
	[Serializable]
	public enum LeaseState
	{
		// Token: 0x0400142A RID: 5162
		Null,
		// Token: 0x0400142B RID: 5163
		Initial,
		// Token: 0x0400142C RID: 5164
		Active,
		// Token: 0x0400142D RID: 5165
		Renewing,
		// Token: 0x0400142E RID: 5166
		Expired
	}
}
