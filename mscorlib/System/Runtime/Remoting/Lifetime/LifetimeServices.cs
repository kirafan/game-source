﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Lifetime
{
	// Token: 0x0200048B RID: 1163
	[ComVisible(true)]
	public sealed class LifetimeServices
	{
		// Token: 0x06002F73 RID: 12147 RVA: 0x0009D21C File Offset: 0x0009B41C
		static LifetimeServices()
		{
			LifetimeServices._leaseManagerPollTime = TimeSpan.FromSeconds(10.0);
			LifetimeServices._leaseTime = TimeSpan.FromMinutes(5.0);
			LifetimeServices._renewOnCallTime = TimeSpan.FromMinutes(2.0);
			LifetimeServices._sponsorshipTimeout = TimeSpan.FromMinutes(2.0);
		}

		// Token: 0x17000887 RID: 2183
		// (get) Token: 0x06002F74 RID: 12148 RVA: 0x0009D280 File Offset: 0x0009B480
		// (set) Token: 0x06002F75 RID: 12149 RVA: 0x0009D288 File Offset: 0x0009B488
		public static TimeSpan LeaseManagerPollTime
		{
			get
			{
				return LifetimeServices._leaseManagerPollTime;
			}
			set
			{
				LifetimeServices._leaseManagerPollTime = value;
				LifetimeServices._leaseManager.SetPollTime(value);
			}
		}

		// Token: 0x17000888 RID: 2184
		// (get) Token: 0x06002F76 RID: 12150 RVA: 0x0009D29C File Offset: 0x0009B49C
		// (set) Token: 0x06002F77 RID: 12151 RVA: 0x0009D2A4 File Offset: 0x0009B4A4
		public static TimeSpan LeaseTime
		{
			get
			{
				return LifetimeServices._leaseTime;
			}
			set
			{
				LifetimeServices._leaseTime = value;
			}
		}

		// Token: 0x17000889 RID: 2185
		// (get) Token: 0x06002F78 RID: 12152 RVA: 0x0009D2AC File Offset: 0x0009B4AC
		// (set) Token: 0x06002F79 RID: 12153 RVA: 0x0009D2B4 File Offset: 0x0009B4B4
		public static TimeSpan RenewOnCallTime
		{
			get
			{
				return LifetimeServices._renewOnCallTime;
			}
			set
			{
				LifetimeServices._renewOnCallTime = value;
			}
		}

		// Token: 0x1700088A RID: 2186
		// (get) Token: 0x06002F7A RID: 12154 RVA: 0x0009D2BC File Offset: 0x0009B4BC
		// (set) Token: 0x06002F7B RID: 12155 RVA: 0x0009D2C4 File Offset: 0x0009B4C4
		public static TimeSpan SponsorshipTimeout
		{
			get
			{
				return LifetimeServices._sponsorshipTimeout;
			}
			set
			{
				LifetimeServices._sponsorshipTimeout = value;
			}
		}

		// Token: 0x06002F7C RID: 12156 RVA: 0x0009D2CC File Offset: 0x0009B4CC
		internal static void TrackLifetime(ServerIdentity identity)
		{
			LifetimeServices._leaseManager.TrackLifetime(identity);
		}

		// Token: 0x06002F7D RID: 12157 RVA: 0x0009D2DC File Offset: 0x0009B4DC
		internal static void StopTrackingLifetime(ServerIdentity identity)
		{
			LifetimeServices._leaseManager.StopTrackingLifetime(identity);
		}

		// Token: 0x0400142F RID: 5167
		private static TimeSpan _leaseManagerPollTime;

		// Token: 0x04001430 RID: 5168
		private static TimeSpan _leaseTime;

		// Token: 0x04001431 RID: 5169
		private static TimeSpan _renewOnCallTime;

		// Token: 0x04001432 RID: 5170
		private static TimeSpan _sponsorshipTimeout;

		// Token: 0x04001433 RID: 5171
		private static LeaseManager _leaseManager = new LeaseManager();
	}
}
