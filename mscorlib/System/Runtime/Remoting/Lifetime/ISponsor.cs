﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Lifetime
{
	// Token: 0x02000486 RID: 1158
	[ComVisible(true)]
	public interface ISponsor
	{
		// Token: 0x06002F54 RID: 12116
		TimeSpan Renewal(ILease lease);
	}
}
