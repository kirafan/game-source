﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting.Proxies
{
	// Token: 0x020004EA RID: 1258
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class)]
	public class ProxyAttribute : Attribute, IContextAttribute
	{
		// Token: 0x0600328E RID: 12942 RVA: 0x000A4224 File Offset: 0x000A2424
		public virtual MarshalByRefObject CreateInstance(Type serverType)
		{
			RemotingProxy remotingProxy = new RemotingProxy(serverType, ChannelServices.CrossContextUrl, null);
			return (MarshalByRefObject)remotingProxy.GetTransparentProxy();
		}

		// Token: 0x0600328F RID: 12943 RVA: 0x000A424C File Offset: 0x000A244C
		public virtual RealProxy CreateProxy(ObjRef objRef, Type serverType, object serverObject, Context serverContext)
		{
			return RemotingServices.GetRealProxy(RemotingServices.GetProxyForRemoteObject(objRef, serverType));
		}

		// Token: 0x06003290 RID: 12944 RVA: 0x000A425C File Offset: 0x000A245C
		[ComVisible(true)]
		public void GetPropertiesForNewContext(IConstructionCallMessage msg)
		{
		}

		// Token: 0x06003291 RID: 12945 RVA: 0x000A4260 File Offset: 0x000A2460
		[ComVisible(true)]
		public bool IsContextOK(Context ctx, IConstructionCallMessage msg)
		{
			return true;
		}
	}
}
