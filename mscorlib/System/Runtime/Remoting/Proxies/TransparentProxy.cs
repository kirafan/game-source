﻿using System;

namespace System.Runtime.Remoting.Proxies
{
	// Token: 0x020004E7 RID: 1255
	internal class TransparentProxy
	{
		// Token: 0x0400151B RID: 5403
		public RealProxy _rp;

		// Token: 0x0400151C RID: 5404
		private IntPtr _class;

		// Token: 0x0400151D RID: 5405
		private bool _custom_type_info;
	}
}
