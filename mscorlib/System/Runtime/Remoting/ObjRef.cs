﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Channels;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting
{
	// Token: 0x02000422 RID: 1058
	[ComVisible(true)]
	[Serializable]
	public class ObjRef : ISerializable, IObjectReference
	{
		// Token: 0x06002CE4 RID: 11492 RVA: 0x00094340 File Offset: 0x00092540
		public ObjRef()
		{
			this.UpdateChannelInfo();
		}

		// Token: 0x06002CE5 RID: 11493 RVA: 0x00094350 File Offset: 0x00092550
		internal ObjRef(string typeName, string uri, IChannelInfo cinfo)
		{
			this.uri = uri;
			this.channel_info = cinfo;
			this.typeInfo = new TypeInfo(Type.GetType(typeName, true));
		}

		// Token: 0x06002CE6 RID: 11494 RVA: 0x00094384 File Offset: 0x00092584
		internal ObjRef(ObjRef o, bool unmarshalAsProxy)
		{
			this.channel_info = o.channel_info;
			this.uri = o.uri;
			this.typeInfo = o.typeInfo;
			this.envoyInfo = o.envoyInfo;
			this.flags = o.flags;
			if (unmarshalAsProxy)
			{
				this.flags |= ObjRef.MarshalledObjectRef;
			}
		}

		// Token: 0x06002CE7 RID: 11495 RVA: 0x000943EC File Offset: 0x000925EC
		public ObjRef(MarshalByRefObject o, Type requestedType)
		{
			if (o == null)
			{
				throw new ArgumentNullException("o");
			}
			if (requestedType == null)
			{
				throw new ArgumentNullException("requestedType");
			}
			this.uri = RemotingServices.GetObjectUri(o);
			this.typeInfo = new TypeInfo(requestedType);
			if (!requestedType.IsInstanceOfType(o))
			{
				throw new RemotingException("The server object type cannot be cast to the requested type " + requestedType.FullName);
			}
			this.UpdateChannelInfo();
		}

		// Token: 0x06002CE8 RID: 11496 RVA: 0x00094464 File Offset: 0x00092664
		internal ObjRef(Type type, string url, object remoteChannelData)
		{
			this.uri = url;
			this.typeInfo = new TypeInfo(type);
			if (remoteChannelData != null)
			{
				this.channel_info = new ChannelInfo(remoteChannelData);
			}
			this.flags |= ObjRef.WellKnowObjectRef;
		}

		// Token: 0x06002CE9 RID: 11497 RVA: 0x000944A4 File Offset: 0x000926A4
		protected ObjRef(SerializationInfo info, StreamingContext context)
		{
			SerializationInfoEnumerator enumerator = info.GetEnumerator();
			bool flag = true;
			while (enumerator.MoveNext())
			{
				string name = enumerator.Name;
				switch (name)
				{
				case "uri":
					this.uri = (string)enumerator.Value;
					continue;
				case "typeInfo":
					this.typeInfo = (IRemotingTypeInfo)enumerator.Value;
					continue;
				case "channelInfo":
					this.channel_info = (IChannelInfo)enumerator.Value;
					continue;
				case "envoyInfo":
					this.envoyInfo = (IEnvoyInfo)enumerator.Value;
					continue;
				case "fIsMarshalled":
				{
					object value = enumerator.Value;
					int num2;
					if (value is string)
					{
						num2 = ((IConvertible)value).ToInt32(null);
					}
					else
					{
						num2 = (int)value;
					}
					if (num2 == 0)
					{
						flag = false;
					}
					continue;
				}
				case "objrefFlags":
					this.flags = Convert.ToInt32(enumerator.Value);
					continue;
				}
				throw new NotSupportedException();
			}
			if (flag)
			{
				this.flags |= ObjRef.MarshalledObjectRef;
			}
		}

		// Token: 0x06002CEB RID: 11499 RVA: 0x00094654 File Offset: 0x00092854
		internal bool IsPossibleToCAD()
		{
			return false;
		}

		// Token: 0x170007FD RID: 2045
		// (get) Token: 0x06002CEC RID: 11500 RVA: 0x00094658 File Offset: 0x00092858
		internal bool IsReferenceToWellKnow
		{
			get
			{
				return (this.flags & ObjRef.WellKnowObjectRef) > 0;
			}
		}

		// Token: 0x170007FE RID: 2046
		// (get) Token: 0x06002CED RID: 11501 RVA: 0x0009466C File Offset: 0x0009286C
		// (set) Token: 0x06002CEE RID: 11502 RVA: 0x00094674 File Offset: 0x00092874
		public virtual IChannelInfo ChannelInfo
		{
			[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
			get
			{
				return this.channel_info;
			}
			set
			{
				this.channel_info = value;
			}
		}

		// Token: 0x170007FF RID: 2047
		// (get) Token: 0x06002CEF RID: 11503 RVA: 0x00094680 File Offset: 0x00092880
		// (set) Token: 0x06002CF0 RID: 11504 RVA: 0x00094688 File Offset: 0x00092888
		public virtual IEnvoyInfo EnvoyInfo
		{
			get
			{
				return this.envoyInfo;
			}
			set
			{
				this.envoyInfo = value;
			}
		}

		// Token: 0x17000800 RID: 2048
		// (get) Token: 0x06002CF1 RID: 11505 RVA: 0x00094694 File Offset: 0x00092894
		// (set) Token: 0x06002CF2 RID: 11506 RVA: 0x0009469C File Offset: 0x0009289C
		public virtual IRemotingTypeInfo TypeInfo
		{
			get
			{
				return this.typeInfo;
			}
			set
			{
				this.typeInfo = value;
			}
		}

		// Token: 0x17000801 RID: 2049
		// (get) Token: 0x06002CF3 RID: 11507 RVA: 0x000946A8 File Offset: 0x000928A8
		// (set) Token: 0x06002CF4 RID: 11508 RVA: 0x000946B0 File Offset: 0x000928B0
		public virtual string URI
		{
			get
			{
				return this.uri;
			}
			set
			{
				this.uri = value;
			}
		}

		// Token: 0x06002CF5 RID: 11509 RVA: 0x000946BC File Offset: 0x000928BC
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.SetType(base.GetType());
			info.AddValue("uri", this.uri);
			info.AddValue("typeInfo", this.typeInfo, typeof(IRemotingTypeInfo));
			info.AddValue("envoyInfo", this.envoyInfo, typeof(IEnvoyInfo));
			info.AddValue("channelInfo", this.channel_info, typeof(IChannelInfo));
			info.AddValue("objrefFlags", this.flags);
		}

		// Token: 0x06002CF6 RID: 11510 RVA: 0x00094748 File Offset: 0x00092948
		public virtual object GetRealObject(StreamingContext context)
		{
			if ((this.flags & ObjRef.MarshalledObjectRef) > 0)
			{
				return RemotingServices.Unmarshal(this);
			}
			return this;
		}

		// Token: 0x06002CF7 RID: 11511 RVA: 0x00094764 File Offset: 0x00092964
		public bool IsFromThisAppDomain()
		{
			Identity identityForUri = RemotingServices.GetIdentityForUri(this.uri);
			return identityForUri != null && identityForUri.IsFromThisAppDomain;
		}

		// Token: 0x06002CF8 RID: 11512 RVA: 0x0009478C File Offset: 0x0009298C
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public bool IsFromThisProcess()
		{
			foreach (object obj in this.channel_info.ChannelData)
			{
				if (obj is CrossAppDomainData)
				{
					string processID = ((CrossAppDomainData)obj).ProcessID;
					return processID == RemotingConfiguration.ProcessId;
				}
			}
			return true;
		}

		// Token: 0x06002CF9 RID: 11513 RVA: 0x000947E4 File Offset: 0x000929E4
		internal void UpdateChannelInfo()
		{
			this.channel_info = new ChannelInfo();
		}

		// Token: 0x17000802 RID: 2050
		// (get) Token: 0x06002CFA RID: 11514 RVA: 0x000947F4 File Offset: 0x000929F4
		internal Type ServerType
		{
			get
			{
				if (this._serverType == null)
				{
					this._serverType = Type.GetType(this.typeInfo.TypeName);
				}
				return this._serverType;
			}
		}

		// Token: 0x04001368 RID: 4968
		private IChannelInfo channel_info;

		// Token: 0x04001369 RID: 4969
		private string uri;

		// Token: 0x0400136A RID: 4970
		private IRemotingTypeInfo typeInfo;

		// Token: 0x0400136B RID: 4971
		private IEnvoyInfo envoyInfo;

		// Token: 0x0400136C RID: 4972
		private int flags;

		// Token: 0x0400136D RID: 4973
		private Type _serverType;

		// Token: 0x0400136E RID: 4974
		private static int MarshalledObjectRef = 1;

		// Token: 0x0400136F RID: 4975
		private static int WellKnowObjectRef = 2;
	}
}
