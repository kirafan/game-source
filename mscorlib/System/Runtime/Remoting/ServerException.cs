﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Runtime.Remoting
{
	// Token: 0x0200042C RID: 1068
	[ComVisible(true)]
	[Serializable]
	public class ServerException : SystemException
	{
		// Token: 0x06002D84 RID: 11652 RVA: 0x00097B48 File Offset: 0x00095D48
		public ServerException()
		{
		}

		// Token: 0x06002D85 RID: 11653 RVA: 0x00097B50 File Offset: 0x00095D50
		public ServerException(string message) : base(message)
		{
		}

		// Token: 0x06002D86 RID: 11654 RVA: 0x00097B5C File Offset: 0x00095D5C
		public ServerException(string message, Exception InnerException) : base(message, InnerException)
		{
		}

		// Token: 0x06002D87 RID: 11655 RVA: 0x00097B68 File Offset: 0x00095D68
		internal ServerException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
