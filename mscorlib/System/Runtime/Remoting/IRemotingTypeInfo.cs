﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	// Token: 0x02000420 RID: 1056
	[ComVisible(true)]
	public interface IRemotingTypeInfo
	{
		// Token: 0x170007FC RID: 2044
		// (get) Token: 0x06002CDE RID: 11486
		// (set) Token: 0x06002CDF RID: 11487
		string TypeName { get; set; }

		// Token: 0x06002CE0 RID: 11488
		bool CanCastTo(Type fromType, object o);
	}
}
