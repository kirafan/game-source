﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200047C RID: 1148
	[ComVisible(true)]
	public interface IContributeObjectSink
	{
		// Token: 0x06002F1E RID: 12062
		IMessageSink GetObjectSink(MarshalByRefObject obj, IMessageSink nextSink);
	}
}
