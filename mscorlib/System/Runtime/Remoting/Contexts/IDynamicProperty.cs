﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200047F RID: 1151
	[ComVisible(true)]
	public interface IDynamicProperty
	{
		// Token: 0x17000875 RID: 2165
		// (get) Token: 0x06002F22 RID: 12066
		string Name { get; }
	}
}
