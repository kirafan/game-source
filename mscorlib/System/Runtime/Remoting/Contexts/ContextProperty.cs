﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x02000473 RID: 1139
	[ComVisible(true)]
	public class ContextProperty
	{
		// Token: 0x06002F06 RID: 12038 RVA: 0x0009C084 File Offset: 0x0009A284
		private ContextProperty(string name, object prop)
		{
			this.name = name;
			this.prop = prop;
		}

		// Token: 0x17000870 RID: 2160
		// (get) Token: 0x06002F07 RID: 12039 RVA: 0x0009C09C File Offset: 0x0009A29C
		public virtual string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x17000871 RID: 2161
		// (get) Token: 0x06002F08 RID: 12040 RVA: 0x0009C0A4 File Offset: 0x0009A2A4
		public virtual object Property
		{
			get
			{
				return this.prop;
			}
		}

		// Token: 0x04001406 RID: 5126
		private string name;

		// Token: 0x04001407 RID: 5127
		private object prop;
	}
}
