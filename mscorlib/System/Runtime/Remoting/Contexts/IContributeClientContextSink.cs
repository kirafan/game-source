﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x02000479 RID: 1145
	[ComVisible(true)]
	public interface IContributeClientContextSink
	{
		// Token: 0x06002F1B RID: 12059
		IMessageSink GetClientContextSink(IMessageSink nextSink);
	}
}
