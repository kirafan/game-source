﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x02000477 RID: 1143
	[ComVisible(true)]
	public interface IContextProperty
	{
		// Token: 0x17000874 RID: 2164
		// (get) Token: 0x06002F13 RID: 12051
		string Name { get; }

		// Token: 0x06002F14 RID: 12052
		void Freeze(Context newContext);

		// Token: 0x06002F15 RID: 12053
		bool IsNewContextOK(Context newCtx);
	}
}
