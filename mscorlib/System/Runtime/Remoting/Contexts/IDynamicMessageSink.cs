﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200047E RID: 1150
	[ComVisible(true)]
	public interface IDynamicMessageSink
	{
		// Token: 0x06002F20 RID: 12064
		void ProcessMessageFinish(IMessage replyMsg, bool bCliSide, bool bAsync);

		// Token: 0x06002F21 RID: 12065
		void ProcessMessageStart(IMessage reqMsg, bool bCliSide, bool bAsync);
	}
}
