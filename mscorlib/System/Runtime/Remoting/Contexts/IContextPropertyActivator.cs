﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x02000478 RID: 1144
	[ComVisible(true)]
	public interface IContextPropertyActivator
	{
		// Token: 0x06002F16 RID: 12054
		void CollectFromClientContext(IConstructionCallMessage msg);

		// Token: 0x06002F17 RID: 12055
		void CollectFromServerContext(IConstructionReturnMessage msg);

		// Token: 0x06002F18 RID: 12056
		bool DeliverClientContextToServerContext(IConstructionCallMessage msg);

		// Token: 0x06002F19 RID: 12057
		bool DeliverServerContextToClientContext(IConstructionReturnMessage msg);

		// Token: 0x06002F1A RID: 12058
		bool IsOKToActivate(IConstructionCallMessage msg);
	}
}
