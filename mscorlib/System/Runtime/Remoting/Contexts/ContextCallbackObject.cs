﻿using System;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x02000471 RID: 1137
	internal class ContextCallbackObject : ContextBoundObject
	{
		// Token: 0x06002EFD RID: 12029 RVA: 0x0009BF70 File Offset: 0x0009A170
		public void DoCallBack(CrossContextDelegate deleg)
		{
		}
	}
}
