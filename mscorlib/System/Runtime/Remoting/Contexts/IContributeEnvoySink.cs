﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200047B RID: 1147
	[ComVisible(true)]
	public interface IContributeEnvoySink
	{
		// Token: 0x06002F1D RID: 12061
		IMessageSink GetEnvoySink(MarshalByRefObject obj, IMessageSink nextSink);
	}
}
