﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Messaging;
using System.Threading;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x02000480 RID: 1152
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class)]
	[Serializable]
	public class SynchronizationAttribute : ContextAttribute, IContributeClientContextSink, IContributeServerContextSink
	{
		// Token: 0x06002F23 RID: 12067 RVA: 0x0009C360 File Offset: 0x0009A560
		public SynchronizationAttribute() : this(8, false)
		{
		}

		// Token: 0x06002F24 RID: 12068 RVA: 0x0009C36C File Offset: 0x0009A56C
		public SynchronizationAttribute(bool reEntrant) : this(8, reEntrant)
		{
		}

		// Token: 0x06002F25 RID: 12069 RVA: 0x0009C378 File Offset: 0x0009A578
		public SynchronizationAttribute(int flag) : this(flag, false)
		{
		}

		// Token: 0x06002F26 RID: 12070 RVA: 0x0009C384 File Offset: 0x0009A584
		public SynchronizationAttribute(int flag, bool reEntrant) : base("Synchronization")
		{
			if (flag != 1 && flag != 4 && flag != 8 && flag != 2)
			{
				throw new ArgumentException("flag");
			}
			this._bReEntrant = reEntrant;
			this._flavor = flag;
		}

		// Token: 0x17000876 RID: 2166
		// (get) Token: 0x06002F27 RID: 12071 RVA: 0x0009C3E0 File Offset: 0x0009A5E0
		public virtual bool IsReEntrant
		{
			get
			{
				return this._bReEntrant;
			}
		}

		// Token: 0x17000877 RID: 2167
		// (get) Token: 0x06002F28 RID: 12072 RVA: 0x0009C3E8 File Offset: 0x0009A5E8
		// (set) Token: 0x06002F29 RID: 12073 RVA: 0x0009C3F0 File Offset: 0x0009A5F0
		public virtual bool Locked
		{
			get
			{
				return this._locked;
			}
			set
			{
				if (value)
				{
					this._mutex.WaitOne();
					lock (this)
					{
						this._lockCount++;
						if (this._lockCount > 1)
						{
							this.ReleaseLock();
						}
						this._ownerThread = Thread.CurrentThread;
					}
				}
				else
				{
					lock (this)
					{
						while (this._lockCount > 0 && this._ownerThread == Thread.CurrentThread)
						{
							this._lockCount--;
							this._mutex.ReleaseMutex();
							this._ownerThread = null;
						}
					}
				}
			}
		}

		// Token: 0x06002F2A RID: 12074 RVA: 0x0009C4DC File Offset: 0x0009A6DC
		internal void AcquireLock()
		{
			this._mutex.WaitOne();
			lock (this)
			{
				this._ownerThread = Thread.CurrentThread;
				this._lockCount++;
			}
		}

		// Token: 0x06002F2B RID: 12075 RVA: 0x0009C540 File Offset: 0x0009A740
		internal void ReleaseLock()
		{
			lock (this)
			{
				if (this._lockCount > 0 && this._ownerThread == Thread.CurrentThread)
				{
					this._lockCount--;
					this._mutex.ReleaseMutex();
					this._ownerThread = null;
				}
			}
		}

		// Token: 0x06002F2C RID: 12076 RVA: 0x0009C5BC File Offset: 0x0009A7BC
		[ComVisible(true)]
		public override void GetPropertiesForNewContext(IConstructionCallMessage ctorMsg)
		{
			if (this._flavor != 1)
			{
				ctorMsg.ContextProperties.Add(this);
			}
		}

		// Token: 0x06002F2D RID: 12077 RVA: 0x0009C5D8 File Offset: 0x0009A7D8
		public virtual IMessageSink GetClientContextSink(IMessageSink nextSink)
		{
			return new SynchronizedClientContextSink(nextSink, this);
		}

		// Token: 0x06002F2E RID: 12078 RVA: 0x0009C5E4 File Offset: 0x0009A7E4
		public virtual IMessageSink GetServerContextSink(IMessageSink nextSink)
		{
			return new SynchronizedServerContextSink(nextSink, this);
		}

		// Token: 0x06002F2F RID: 12079 RVA: 0x0009C5F0 File Offset: 0x0009A7F0
		[ComVisible(true)]
		public override bool IsContextOK(Context ctx, IConstructionCallMessage msg)
		{
			SynchronizationAttribute synchronizationAttribute = ctx.GetProperty("Synchronization") as SynchronizationAttribute;
			switch (this._flavor)
			{
			case 1:
				return synchronizationAttribute == null;
			case 2:
				return true;
			case 4:
				return synchronizationAttribute != null;
			case 8:
				return false;
			}
			return false;
		}

		// Token: 0x06002F30 RID: 12080 RVA: 0x0009C654 File Offset: 0x0009A854
		internal static void ExitContext()
		{
			if (Thread.CurrentContext.IsDefaultContext)
			{
				return;
			}
			SynchronizationAttribute synchronizationAttribute = Thread.CurrentContext.GetProperty("Synchronization") as SynchronizationAttribute;
			if (synchronizationAttribute == null)
			{
				return;
			}
			synchronizationAttribute.Locked = false;
		}

		// Token: 0x06002F31 RID: 12081 RVA: 0x0009C694 File Offset: 0x0009A894
		internal static void EnterContext()
		{
			if (Thread.CurrentContext.IsDefaultContext)
			{
				return;
			}
			SynchronizationAttribute synchronizationAttribute = Thread.CurrentContext.GetProperty("Synchronization") as SynchronizationAttribute;
			if (synchronizationAttribute == null)
			{
				return;
			}
			synchronizationAttribute.Locked = true;
		}

		// Token: 0x0400140B RID: 5131
		public const int NOT_SUPPORTED = 1;

		// Token: 0x0400140C RID: 5132
		public const int SUPPORTED = 2;

		// Token: 0x0400140D RID: 5133
		public const int REQUIRED = 4;

		// Token: 0x0400140E RID: 5134
		public const int REQUIRES_NEW = 8;

		// Token: 0x0400140F RID: 5135
		private bool _bReEntrant;

		// Token: 0x04001410 RID: 5136
		private int _flavor;

		// Token: 0x04001411 RID: 5137
		[NonSerialized]
		private bool _locked;

		// Token: 0x04001412 RID: 5138
		[NonSerialized]
		private int _lockCount;

		// Token: 0x04001413 RID: 5139
		[NonSerialized]
		private Mutex _mutex = new Mutex(false);

		// Token: 0x04001414 RID: 5140
		[NonSerialized]
		private Thread _ownerThread;
	}
}
