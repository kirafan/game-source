﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x02000481 RID: 1153
	internal class SynchronizedClientContextSink : IMessageSink
	{
		// Token: 0x06002F32 RID: 12082 RVA: 0x0009C6D4 File Offset: 0x0009A8D4
		public SynchronizedClientContextSink(IMessageSink next, SynchronizationAttribute att)
		{
			this._att = att;
			this._next = next;
		}

		// Token: 0x17000878 RID: 2168
		// (get) Token: 0x06002F33 RID: 12083 RVA: 0x0009C6EC File Offset: 0x0009A8EC
		public IMessageSink NextSink
		{
			get
			{
				return this._next;
			}
		}

		// Token: 0x06002F34 RID: 12084 RVA: 0x0009C6F4 File Offset: 0x0009A8F4
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			if (this._att.IsReEntrant)
			{
				this._att.ReleaseLock();
				replySink = new SynchronizedContextReplySink(replySink, this._att, true);
			}
			return this._next.AsyncProcessMessage(msg, replySink);
		}

		// Token: 0x06002F35 RID: 12085 RVA: 0x0009C738 File Offset: 0x0009A938
		public IMessage SyncProcessMessage(IMessage msg)
		{
			if (this._att.IsReEntrant)
			{
				this._att.ReleaseLock();
			}
			IMessage result;
			try
			{
				result = this._next.SyncProcessMessage(msg);
			}
			finally
			{
				if (this._att.IsReEntrant)
				{
					this._att.AcquireLock();
				}
			}
			return result;
		}

		// Token: 0x04001415 RID: 5141
		private IMessageSink _next;

		// Token: 0x04001416 RID: 5142
		private SynchronizationAttribute _att;
	}
}
