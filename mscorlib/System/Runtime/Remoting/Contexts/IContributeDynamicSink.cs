﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200047A RID: 1146
	[ComVisible(true)]
	public interface IContributeDynamicSink
	{
		// Token: 0x06002F1C RID: 12060
		IDynamicMessageSink GetDynamicSink();
	}
}
