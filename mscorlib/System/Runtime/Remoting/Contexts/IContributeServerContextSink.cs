﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200047D RID: 1149
	[ComVisible(true)]
	public interface IContributeServerContextSink
	{
		// Token: 0x06002F1F RID: 12063
		IMessageSink GetServerContextSink(IMessageSink nextSink);
	}
}
