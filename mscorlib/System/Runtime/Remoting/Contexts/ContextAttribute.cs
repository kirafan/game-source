﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x02000472 RID: 1138
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class)]
	[Serializable]
	public class ContextAttribute : Attribute, IContextAttribute, IContextProperty
	{
		// Token: 0x06002EFE RID: 12030 RVA: 0x0009BF74 File Offset: 0x0009A174
		public ContextAttribute(string name)
		{
			this.AttributeName = name;
		}

		// Token: 0x1700086F RID: 2159
		// (get) Token: 0x06002EFF RID: 12031 RVA: 0x0009BF84 File Offset: 0x0009A184
		public virtual string Name
		{
			get
			{
				return this.AttributeName;
			}
		}

		// Token: 0x06002F00 RID: 12032 RVA: 0x0009BF8C File Offset: 0x0009A18C
		public override bool Equals(object o)
		{
			if (o == null)
			{
				return false;
			}
			if (!(o is ContextAttribute))
			{
				return false;
			}
			ContextAttribute contextAttribute = (ContextAttribute)o;
			return !(contextAttribute.AttributeName != this.AttributeName);
		}

		// Token: 0x06002F01 RID: 12033 RVA: 0x0009BFD0 File Offset: 0x0009A1D0
		public virtual void Freeze(Context newContext)
		{
		}

		// Token: 0x06002F02 RID: 12034 RVA: 0x0009BFD4 File Offset: 0x0009A1D4
		public override int GetHashCode()
		{
			if (this.AttributeName == null)
			{
				return 0;
			}
			return this.AttributeName.GetHashCode();
		}

		// Token: 0x06002F03 RID: 12035 RVA: 0x0009BFF0 File Offset: 0x0009A1F0
		public virtual void GetPropertiesForNewContext(IConstructionCallMessage ctorMsg)
		{
			if (ctorMsg == null)
			{
				throw new ArgumentNullException("ctorMsg");
			}
			IList contextProperties = ctorMsg.ContextProperties;
			contextProperties.Add(this);
		}

		// Token: 0x06002F04 RID: 12036 RVA: 0x0009C020 File Offset: 0x0009A220
		public virtual bool IsContextOK(Context ctx, IConstructionCallMessage ctorMsg)
		{
			if (ctorMsg == null)
			{
				throw new ArgumentNullException("ctorMsg");
			}
			if (ctx == null)
			{
				throw new ArgumentNullException("ctx");
			}
			if (!ctorMsg.ActivationType.IsContextful)
			{
				return true;
			}
			IContextProperty property = ctx.GetProperty(this.AttributeName);
			return property != null && this == property;
		}

		// Token: 0x06002F05 RID: 12037 RVA: 0x0009C080 File Offset: 0x0009A280
		public virtual bool IsNewContextOK(Context newCtx)
		{
			return true;
		}

		// Token: 0x04001405 RID: 5125
		protected string AttributeName;
	}
}
