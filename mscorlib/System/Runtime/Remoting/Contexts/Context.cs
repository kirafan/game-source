﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;
using System.Threading;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x0200046E RID: 1134
	[ComVisible(true)]
	public class Context
	{
		// Token: 0x06002ED4 RID: 11988 RVA: 0x0009B258 File Offset: 0x00099458
		public Context()
		{
			this.domain_id = Thread.GetDomainID();
			this.context_id = 1 + Context.global_count++;
		}

		// Token: 0x06002ED6 RID: 11990 RVA: 0x0009B298 File Offset: 0x00099498
		~Context()
		{
		}

		// Token: 0x17000866 RID: 2150
		// (get) Token: 0x06002ED7 RID: 11991 RVA: 0x0009B2D0 File Offset: 0x000994D0
		public static Context DefaultContext
		{
			get
			{
				return AppDomain.InternalGetDefaultContext();
			}
		}

		// Token: 0x17000867 RID: 2151
		// (get) Token: 0x06002ED8 RID: 11992 RVA: 0x0009B2D8 File Offset: 0x000994D8
		public virtual int ContextID
		{
			get
			{
				return this.context_id;
			}
		}

		// Token: 0x17000868 RID: 2152
		// (get) Token: 0x06002ED9 RID: 11993 RVA: 0x0009B2E0 File Offset: 0x000994E0
		public virtual IContextProperty[] ContextProperties
		{
			get
			{
				if (this.context_properties == null)
				{
					return new IContextProperty[0];
				}
				return (IContextProperty[])this.context_properties.ToArray(typeof(IContextProperty[]));
			}
		}

		// Token: 0x17000869 RID: 2153
		// (get) Token: 0x06002EDA RID: 11994 RVA: 0x0009B31C File Offset: 0x0009951C
		internal bool IsDefaultContext
		{
			get
			{
				return this.context_id == 0;
			}
		}

		// Token: 0x1700086A RID: 2154
		// (get) Token: 0x06002EDB RID: 11995 RVA: 0x0009B328 File Offset: 0x00099528
		internal bool NeedsContextSink
		{
			get
			{
				return this.context_id != 0 || (Context.global_dynamic_properties != null && Context.global_dynamic_properties.HasProperties) || (this.context_dynamic_properties != null && this.context_dynamic_properties.HasProperties);
			}
		}

		// Token: 0x06002EDC RID: 11996 RVA: 0x0009B378 File Offset: 0x00099578
		public static bool RegisterDynamicProperty(IDynamicProperty prop, ContextBoundObject obj, Context ctx)
		{
			DynamicPropertyCollection dynamicPropertyCollection = Context.GetDynamicPropertyCollection(obj, ctx);
			return dynamicPropertyCollection.RegisterDynamicProperty(prop);
		}

		// Token: 0x06002EDD RID: 11997 RVA: 0x0009B394 File Offset: 0x00099594
		public static bool UnregisterDynamicProperty(string name, ContextBoundObject obj, Context ctx)
		{
			DynamicPropertyCollection dynamicPropertyCollection = Context.GetDynamicPropertyCollection(obj, ctx);
			return dynamicPropertyCollection.UnregisterDynamicProperty(name);
		}

		// Token: 0x06002EDE RID: 11998 RVA: 0x0009B3B0 File Offset: 0x000995B0
		private static DynamicPropertyCollection GetDynamicPropertyCollection(ContextBoundObject obj, Context ctx)
		{
			if (ctx == null && obj != null)
			{
				if (RemotingServices.IsTransparentProxy(obj))
				{
					RealProxy realProxy = RemotingServices.GetRealProxy(obj);
					return realProxy.ObjectIdentity.ClientDynamicProperties;
				}
				return obj.ObjectIdentity.ServerDynamicProperties;
			}
			else
			{
				if (ctx != null && obj == null)
				{
					if (ctx.context_dynamic_properties == null)
					{
						ctx.context_dynamic_properties = new DynamicPropertyCollection();
					}
					return ctx.context_dynamic_properties;
				}
				if (ctx == null && obj == null)
				{
					if (Context.global_dynamic_properties == null)
					{
						Context.global_dynamic_properties = new DynamicPropertyCollection();
					}
					return Context.global_dynamic_properties;
				}
				throw new ArgumentException("Either obj or ctx must be null");
			}
		}

		// Token: 0x06002EDF RID: 11999 RVA: 0x0009B44C File Offset: 0x0009964C
		internal static void NotifyGlobalDynamicSinks(bool start, IMessage req_msg, bool client_site, bool async)
		{
			if (Context.global_dynamic_properties != null && Context.global_dynamic_properties.HasProperties)
			{
				Context.global_dynamic_properties.NotifyMessage(start, req_msg, client_site, async);
			}
		}

		// Token: 0x1700086B RID: 2155
		// (get) Token: 0x06002EE0 RID: 12000 RVA: 0x0009B478 File Offset: 0x00099678
		internal static bool HasGlobalDynamicSinks
		{
			get
			{
				return Context.global_dynamic_properties != null && Context.global_dynamic_properties.HasProperties;
			}
		}

		// Token: 0x06002EE1 RID: 12001 RVA: 0x0009B494 File Offset: 0x00099694
		internal void NotifyDynamicSinks(bool start, IMessage req_msg, bool client_site, bool async)
		{
			if (this.context_dynamic_properties != null && this.context_dynamic_properties.HasProperties)
			{
				this.context_dynamic_properties.NotifyMessage(start, req_msg, client_site, async);
			}
		}

		// Token: 0x1700086C RID: 2156
		// (get) Token: 0x06002EE2 RID: 12002 RVA: 0x0009B4C4 File Offset: 0x000996C4
		internal bool HasDynamicSinks
		{
			get
			{
				return this.context_dynamic_properties != null && this.context_dynamic_properties.HasProperties;
			}
		}

		// Token: 0x1700086D RID: 2157
		// (get) Token: 0x06002EE3 RID: 12003 RVA: 0x0009B4E0 File Offset: 0x000996E0
		internal bool HasExitSinks
		{
			get
			{
				return !(this.GetClientContextSinkChain() is ClientContextTerminatorSink) || this.HasDynamicSinks || Context.HasGlobalDynamicSinks;
			}
		}

		// Token: 0x06002EE4 RID: 12004 RVA: 0x0009B508 File Offset: 0x00099708
		public virtual IContextProperty GetProperty(string name)
		{
			if (this.context_properties == null)
			{
				return null;
			}
			foreach (object obj in this.context_properties)
			{
				IContextProperty contextProperty = (IContextProperty)obj;
				if (contextProperty.Name == name)
				{
					return contextProperty;
				}
			}
			return null;
		}

		// Token: 0x06002EE5 RID: 12005 RVA: 0x0009B598 File Offset: 0x00099798
		public virtual void SetProperty(IContextProperty prop)
		{
			if (prop == null)
			{
				throw new ArgumentNullException("IContextProperty");
			}
			if (this == Context.DefaultContext)
			{
				throw new InvalidOperationException("Can not add properties to default context");
			}
			if (this.frozen)
			{
				throw new InvalidOperationException("Context is Frozen");
			}
			if (this.context_properties == null)
			{
				this.context_properties = new ArrayList();
			}
			this.context_properties.Add(prop);
		}

		// Token: 0x06002EE6 RID: 12006 RVA: 0x0009B608 File Offset: 0x00099808
		public virtual void Freeze()
		{
			if (this.context_properties != null)
			{
				foreach (object obj in this.context_properties)
				{
					IContextProperty contextProperty = (IContextProperty)obj;
					contextProperty.Freeze(this);
				}
			}
		}

		// Token: 0x06002EE7 RID: 12007 RVA: 0x0009B684 File Offset: 0x00099884
		public override string ToString()
		{
			return "ContextID: " + this.context_id;
		}

		// Token: 0x06002EE8 RID: 12008 RVA: 0x0009B69C File Offset: 0x0009989C
		internal IMessageSink GetServerContextSinkChain()
		{
			if (this.server_context_sink_chain == null)
			{
				if (Context.default_server_context_sink == null)
				{
					Context.default_server_context_sink = new ServerContextTerminatorSink();
				}
				this.server_context_sink_chain = Context.default_server_context_sink;
				if (this.context_properties != null)
				{
					for (int i = this.context_properties.Count - 1; i >= 0; i--)
					{
						IContributeServerContextSink contributeServerContextSink = this.context_properties[i] as IContributeServerContextSink;
						if (contributeServerContextSink != null)
						{
							this.server_context_sink_chain = contributeServerContextSink.GetServerContextSink(this.server_context_sink_chain);
						}
					}
				}
			}
			return this.server_context_sink_chain;
		}

		// Token: 0x06002EE9 RID: 12009 RVA: 0x0009B72C File Offset: 0x0009992C
		internal IMessageSink GetClientContextSinkChain()
		{
			if (this.client_context_sink_chain == null)
			{
				this.client_context_sink_chain = new ClientContextTerminatorSink(this);
				if (this.context_properties != null)
				{
					foreach (object obj in this.context_properties)
					{
						IContextProperty contextProperty = (IContextProperty)obj;
						IContributeClientContextSink contributeClientContextSink = contextProperty as IContributeClientContextSink;
						if (contributeClientContextSink != null)
						{
							this.client_context_sink_chain = contributeClientContextSink.GetClientContextSink(this.client_context_sink_chain);
						}
					}
				}
			}
			return this.client_context_sink_chain;
		}

		// Token: 0x06002EEA RID: 12010 RVA: 0x0009B7DC File Offset: 0x000999DC
		internal IMessageSink CreateServerObjectSinkChain(MarshalByRefObject obj, bool forceInternalExecute)
		{
			IMessageSink messageSink = new StackBuilderSink(obj, forceInternalExecute);
			messageSink = new ServerObjectTerminatorSink(messageSink);
			messageSink = new LeaseSink(messageSink);
			if (this.context_properties != null)
			{
				for (int i = this.context_properties.Count - 1; i >= 0; i--)
				{
					IContextProperty contextProperty = (IContextProperty)this.context_properties[i];
					IContributeObjectSink contributeObjectSink = contextProperty as IContributeObjectSink;
					if (contributeObjectSink != null)
					{
						messageSink = contributeObjectSink.GetObjectSink(obj, messageSink);
					}
				}
			}
			return messageSink;
		}

		// Token: 0x06002EEB RID: 12011 RVA: 0x0009B854 File Offset: 0x00099A54
		internal IMessageSink CreateEnvoySink(MarshalByRefObject serverObject)
		{
			IMessageSink messageSink = EnvoyTerminatorSink.Instance;
			if (this.context_properties != null)
			{
				foreach (object obj in this.context_properties)
				{
					IContextProperty contextProperty = (IContextProperty)obj;
					IContributeEnvoySink contributeEnvoySink = contextProperty as IContributeEnvoySink;
					if (contributeEnvoySink != null)
					{
						messageSink = contributeEnvoySink.GetEnvoySink(serverObject, messageSink);
					}
				}
			}
			return messageSink;
		}

		// Token: 0x06002EEC RID: 12012 RVA: 0x0009B8E8 File Offset: 0x00099AE8
		internal static Context SwitchToContext(Context newContext)
		{
			return AppDomain.InternalSetContext(newContext);
		}

		// Token: 0x06002EED RID: 12013 RVA: 0x0009B8F0 File Offset: 0x00099AF0
		internal static Context CreateNewContext(IConstructionCallMessage msg)
		{
			Context context = new Context();
			foreach (object obj in msg.ContextProperties)
			{
				IContextProperty contextProperty = (IContextProperty)obj;
				if (context.GetProperty(contextProperty.Name) == null)
				{
					context.SetProperty(contextProperty);
				}
			}
			context.Freeze();
			foreach (object obj2 in msg.ContextProperties)
			{
				IContextProperty contextProperty2 = (IContextProperty)obj2;
				if (!contextProperty2.IsNewContextOK(context))
				{
					throw new RemotingException("A context property did not approve the candidate context for activating the object");
				}
			}
			return context;
		}

		// Token: 0x06002EEE RID: 12014 RVA: 0x0009B9F8 File Offset: 0x00099BF8
		public void DoCallBack(CrossContextDelegate deleg)
		{
			lock (this)
			{
				if (this.callback_object == null)
				{
					Context newContext = Context.SwitchToContext(this);
					this.callback_object = new ContextCallbackObject();
					Context.SwitchToContext(newContext);
				}
			}
			this.callback_object.DoCallBack(deleg);
		}

		// Token: 0x06002EEF RID: 12015 RVA: 0x0009BA68 File Offset: 0x00099C68
		public static LocalDataStoreSlot AllocateDataSlot()
		{
			return new LocalDataStoreSlot(false);
		}

		// Token: 0x06002EF0 RID: 12016 RVA: 0x0009BA70 File Offset: 0x00099C70
		public static LocalDataStoreSlot AllocateNamedDataSlot(string name)
		{
			object syncRoot = Context.namedSlots.SyncRoot;
			LocalDataStoreSlot result;
			lock (syncRoot)
			{
				LocalDataStoreSlot localDataStoreSlot = Context.AllocateDataSlot();
				Context.namedSlots.Add(name, localDataStoreSlot);
				result = localDataStoreSlot;
			}
			return result;
		}

		// Token: 0x06002EF1 RID: 12017 RVA: 0x0009BAD0 File Offset: 0x00099CD0
		public static void FreeNamedDataSlot(string name)
		{
			object syncRoot = Context.namedSlots.SyncRoot;
			lock (syncRoot)
			{
				Context.namedSlots.Remove(name);
			}
		}

		// Token: 0x06002EF2 RID: 12018 RVA: 0x0009BB24 File Offset: 0x00099D24
		public static object GetData(LocalDataStoreSlot slot)
		{
			Context currentContext = Thread.CurrentContext;
			Context obj = currentContext;
			object result;
			lock (obj)
			{
				if (currentContext.datastore != null && slot.slot < currentContext.datastore.Length)
				{
					result = currentContext.datastore[slot.slot];
				}
				else
				{
					result = null;
				}
			}
			return result;
		}

		// Token: 0x06002EF3 RID: 12019 RVA: 0x0009BBA0 File Offset: 0x00099DA0
		public static LocalDataStoreSlot GetNamedDataSlot(string name)
		{
			object syncRoot = Context.namedSlots.SyncRoot;
			LocalDataStoreSlot result;
			lock (syncRoot)
			{
				LocalDataStoreSlot localDataStoreSlot = Context.namedSlots[name] as LocalDataStoreSlot;
				if (localDataStoreSlot == null)
				{
					result = Context.AllocateNamedDataSlot(name);
				}
				else
				{
					result = localDataStoreSlot;
				}
			}
			return result;
		}

		// Token: 0x06002EF4 RID: 12020 RVA: 0x0009BC14 File Offset: 0x00099E14
		public static void SetData(LocalDataStoreSlot slot, object data)
		{
			Context currentContext = Thread.CurrentContext;
			Context obj = currentContext;
			lock (obj)
			{
				if (currentContext.datastore == null)
				{
					currentContext.datastore = new object[slot.slot + 2];
				}
				else if (slot.slot >= currentContext.datastore.Length)
				{
					object[] array = new object[slot.slot + 2];
					currentContext.datastore.CopyTo(array, 0);
					currentContext.datastore = array;
				}
				currentContext.datastore[slot.slot] = data;
			}
		}

		// Token: 0x040013F4 RID: 5108
		private int domain_id;

		// Token: 0x040013F5 RID: 5109
		private int context_id;

		// Token: 0x040013F6 RID: 5110
		private UIntPtr static_data;

		// Token: 0x040013F7 RID: 5111
		private static IMessageSink default_server_context_sink;

		// Token: 0x040013F8 RID: 5112
		private IMessageSink server_context_sink_chain;

		// Token: 0x040013F9 RID: 5113
		private IMessageSink client_context_sink_chain;

		// Token: 0x040013FA RID: 5114
		private object[] datastore;

		// Token: 0x040013FB RID: 5115
		private ArrayList context_properties;

		// Token: 0x040013FC RID: 5116
		private bool frozen;

		// Token: 0x040013FD RID: 5117
		private static int global_count;

		// Token: 0x040013FE RID: 5118
		private static Hashtable namedSlots = new Hashtable();

		// Token: 0x040013FF RID: 5119
		private static DynamicPropertyCollection global_dynamic_properties;

		// Token: 0x04001400 RID: 5120
		private DynamicPropertyCollection context_dynamic_properties;

		// Token: 0x04001401 RID: 5121
		private ContextCallbackObject callback_object;
	}
}
