﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x020006F5 RID: 1781
	// (Invoke) Token: 0x060043C8 RID: 17352
	[ComVisible(true)]
	public delegate void CrossContextDelegate();
}
