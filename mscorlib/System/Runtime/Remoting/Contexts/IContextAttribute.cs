﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;

namespace System.Runtime.Remoting.Contexts
{
	// Token: 0x02000476 RID: 1142
	[ComVisible(true)]
	public interface IContextAttribute
	{
		// Token: 0x06002F11 RID: 12049
		void GetPropertiesForNewContext(IConstructionCallMessage msg);

		// Token: 0x06002F12 RID: 12050
		bool IsContextOK(Context ctx, IConstructionCallMessage msg);
	}
}
