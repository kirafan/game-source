﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x0200043E RID: 1086
	[ComVisible(true)]
	public interface IActivator
	{
		// Token: 0x17000823 RID: 2083
		// (get) Token: 0x06002DF8 RID: 11768
		ActivatorLevel Level { get; }

		// Token: 0x17000824 RID: 2084
		// (get) Token: 0x06002DF9 RID: 11769
		// (set) Token: 0x06002DFA RID: 11770
		IActivator NextActivator { get; set; }

		// Token: 0x06002DFB RID: 11771
		IConstructionReturnMessage Activate(IConstructionCallMessage msg);
	}
}
