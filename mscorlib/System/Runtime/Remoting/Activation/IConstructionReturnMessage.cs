﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x02000440 RID: 1088
	[ComVisible(true)]
	public interface IConstructionReturnMessage : IMessage, IMethodMessage, IMethodReturnMessage
	{
	}
}
