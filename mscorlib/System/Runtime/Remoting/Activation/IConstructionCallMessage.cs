﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x0200043F RID: 1087
	[ComVisible(true)]
	public interface IConstructionCallMessage : IMessage, IMethodCallMessage, IMethodMessage
	{
		// Token: 0x17000825 RID: 2085
		// (get) Token: 0x06002DFC RID: 11772
		Type ActivationType { get; }

		// Token: 0x17000826 RID: 2086
		// (get) Token: 0x06002DFD RID: 11773
		string ActivationTypeName { get; }

		// Token: 0x17000827 RID: 2087
		// (get) Token: 0x06002DFE RID: 11774
		// (set) Token: 0x06002DFF RID: 11775
		IActivator Activator { get; set; }

		// Token: 0x17000828 RID: 2088
		// (get) Token: 0x06002E00 RID: 11776
		object[] CallSiteActivationAttributes { get; }

		// Token: 0x17000829 RID: 2089
		// (get) Token: 0x06002E01 RID: 11777
		IList ContextProperties { get; }
	}
}
