﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x0200043A RID: 1082
	[ComVisible(true)]
	[Serializable]
	public enum ActivatorLevel
	{
		// Token: 0x040013BD RID: 5053
		Construction = 4,
		// Token: 0x040013BE RID: 5054
		Context = 8,
		// Token: 0x040013BF RID: 5055
		AppDomain = 12,
		// Token: 0x040013C0 RID: 5056
		Process = 16,
		// Token: 0x040013C1 RID: 5057
		Machine = 20
	}
}
