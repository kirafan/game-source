﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x02000443 RID: 1091
	[ComVisible(true)]
	[Serializable]
	public sealed class UrlAttribute : ContextAttribute
	{
		// Token: 0x06002E0C RID: 11788 RVA: 0x00099640 File Offset: 0x00097840
		public UrlAttribute(string callsiteURL) : base(callsiteURL)
		{
			this.url = callsiteURL;
		}

		// Token: 0x1700082C RID: 2092
		// (get) Token: 0x06002E0D RID: 11789 RVA: 0x00099650 File Offset: 0x00097850
		public string UrlValue
		{
			get
			{
				return this.url;
			}
		}

		// Token: 0x06002E0E RID: 11790 RVA: 0x00099658 File Offset: 0x00097858
		public override bool Equals(object o)
		{
			return o is UrlAttribute && ((UrlAttribute)o).UrlValue == this.url;
		}

		// Token: 0x06002E0F RID: 11791 RVA: 0x00099680 File Offset: 0x00097880
		public override int GetHashCode()
		{
			return this.url.GetHashCode();
		}

		// Token: 0x06002E10 RID: 11792 RVA: 0x00099690 File Offset: 0x00097890
		[ComVisible(true)]
		public override void GetPropertiesForNewContext(IConstructionCallMessage ctorMsg)
		{
		}

		// Token: 0x06002E11 RID: 11793 RVA: 0x00099694 File Offset: 0x00097894
		[ComVisible(true)]
		public override bool IsContextOK(Context ctx, IConstructionCallMessage msg)
		{
			return true;
		}

		// Token: 0x040013C6 RID: 5062
		private string url;
	}
}
