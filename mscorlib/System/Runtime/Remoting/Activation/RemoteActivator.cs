﻿using System;
using System.Runtime.Remoting.Lifetime;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x02000441 RID: 1089
	internal class RemoteActivator : MarshalByRefObject, IActivator
	{
		// Token: 0x06002E03 RID: 11779 RVA: 0x000994B8 File Offset: 0x000976B8
		public IConstructionReturnMessage Activate(IConstructionCallMessage msg)
		{
			if (!RemotingConfiguration.IsActivationAllowed(msg.ActivationType))
			{
				throw new RemotingException("The type " + msg.ActivationTypeName + " is not allowed to be client activated");
			}
			object[] activationAttributes = new object[]
			{
				new RemoteActivationAttribute(msg.ContextProperties)
			};
			MarshalByRefObject obj = (MarshalByRefObject)Activator.CreateInstance(msg.ActivationType, msg.Args, activationAttributes);
			ObjRef resultObject = RemotingServices.Marshal(obj);
			return new ConstructionResponse(resultObject, null, msg);
		}

		// Token: 0x06002E04 RID: 11780 RVA: 0x0009952C File Offset: 0x0009772C
		public override object InitializeLifetimeService()
		{
			ILease lease = (ILease)base.InitializeLifetimeService();
			if (lease.CurrentState == LeaseState.Initial)
			{
				lease.InitialLeaseTime = TimeSpan.FromMinutes(30.0);
				lease.SponsorshipTimeout = TimeSpan.FromMinutes(1.0);
				lease.RenewOnCallTime = TimeSpan.FromMinutes(10.0);
			}
			return lease;
		}

		// Token: 0x1700082A RID: 2090
		// (get) Token: 0x06002E05 RID: 11781 RVA: 0x00099590 File Offset: 0x00097790
		public ActivatorLevel Level
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x1700082B RID: 2091
		// (get) Token: 0x06002E06 RID: 11782 RVA: 0x00099598 File Offset: 0x00097798
		// (set) Token: 0x06002E07 RID: 11783 RVA: 0x000995A0 File Offset: 0x000977A0
		public IActivator NextActivator
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}
	}
}
