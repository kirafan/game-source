﻿using System;
using System.Collections;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting.Activation
{
	// Token: 0x02000442 RID: 1090
	internal class RemoteActivationAttribute : Attribute, IContextAttribute
	{
		// Token: 0x06002E08 RID: 11784 RVA: 0x000995A8 File Offset: 0x000977A8
		public RemoteActivationAttribute()
		{
		}

		// Token: 0x06002E09 RID: 11785 RVA: 0x000995B0 File Offset: 0x000977B0
		public RemoteActivationAttribute(IList contextProperties)
		{
			this._contextProperties = contextProperties;
		}

		// Token: 0x06002E0A RID: 11786 RVA: 0x000995C0 File Offset: 0x000977C0
		public bool IsContextOK(Context ctx, IConstructionCallMessage ctor)
		{
			return false;
		}

		// Token: 0x06002E0B RID: 11787 RVA: 0x000995C4 File Offset: 0x000977C4
		public void GetPropertiesForNewContext(IConstructionCallMessage ctor)
		{
			if (this._contextProperties != null)
			{
				foreach (object value in this._contextProperties)
				{
					ctor.ContextProperties.Add(value);
				}
			}
		}

		// Token: 0x040013C5 RID: 5061
		private IList _contextProperties;
	}
}
