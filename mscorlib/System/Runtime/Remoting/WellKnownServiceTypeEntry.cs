﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting
{
	// Token: 0x02000438 RID: 1080
	[ComVisible(true)]
	public class WellKnownServiceTypeEntry : TypeEntry
	{
		// Token: 0x06002DD7 RID: 11735 RVA: 0x00098D60 File Offset: 0x00096F60
		public WellKnownServiceTypeEntry(Type type, string objectUri, WellKnownObjectMode mode)
		{
			base.AssemblyName = type.Assembly.FullName;
			base.TypeName = type.FullName;
			this.obj_type = type;
			this.obj_uri = objectUri;
			this.obj_mode = mode;
		}

		// Token: 0x06002DD8 RID: 11736 RVA: 0x00098DA8 File Offset: 0x00096FA8
		public WellKnownServiceTypeEntry(string typeName, string assemblyName, string objectUri, WellKnownObjectMode mode)
		{
			base.AssemblyName = assemblyName;
			base.TypeName = typeName;
			Assembly assembly = Assembly.Load(assemblyName);
			this.obj_type = assembly.GetType(typeName);
			this.obj_uri = objectUri;
			this.obj_mode = mode;
			if (this.obj_type == null)
			{
				throw new RemotingException("Type not found: " + typeName + ", " + assemblyName);
			}
		}

		// Token: 0x17000818 RID: 2072
		// (get) Token: 0x06002DD9 RID: 11737 RVA: 0x00098E10 File Offset: 0x00097010
		// (set) Token: 0x06002DDA RID: 11738 RVA: 0x00098E14 File Offset: 0x00097014
		public IContextAttribute[] ContextAttributes
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x17000819 RID: 2073
		// (get) Token: 0x06002DDB RID: 11739 RVA: 0x00098E18 File Offset: 0x00097018
		public WellKnownObjectMode Mode
		{
			get
			{
				return this.obj_mode;
			}
		}

		// Token: 0x1700081A RID: 2074
		// (get) Token: 0x06002DDC RID: 11740 RVA: 0x00098E20 File Offset: 0x00097020
		public Type ObjectType
		{
			get
			{
				return this.obj_type;
			}
		}

		// Token: 0x1700081B RID: 2075
		// (get) Token: 0x06002DDD RID: 11741 RVA: 0x00098E28 File Offset: 0x00097028
		public string ObjectUri
		{
			get
			{
				return this.obj_uri;
			}
		}

		// Token: 0x06002DDE RID: 11742 RVA: 0x00098E30 File Offset: 0x00097030
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				base.TypeName,
				", ",
				base.AssemblyName,
				" ",
				this.ObjectUri
			});
		}

		// Token: 0x040013B8 RID: 5048
		private Type obj_type;

		// Token: 0x040013B9 RID: 5049
		private string obj_uri;

		// Token: 0x040013BA RID: 5050
		private WellKnownObjectMode obj_mode;
	}
}
