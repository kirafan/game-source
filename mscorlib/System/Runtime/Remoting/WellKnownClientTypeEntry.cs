﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	// Token: 0x02000437 RID: 1079
	[ComVisible(true)]
	public class WellKnownClientTypeEntry : TypeEntry
	{
		// Token: 0x06002DD0 RID: 11728 RVA: 0x00098C4C File Offset: 0x00096E4C
		public WellKnownClientTypeEntry(Type type, string objectUrl)
		{
			base.AssemblyName = type.Assembly.FullName;
			base.TypeName = type.FullName;
			this.obj_type = type;
			this.obj_url = objectUrl;
		}

		// Token: 0x06002DD1 RID: 11729 RVA: 0x00098C8C File Offset: 0x00096E8C
		public WellKnownClientTypeEntry(string typeName, string assemblyName, string objectUrl)
		{
			this.obj_url = objectUrl;
			base.AssemblyName = assemblyName;
			base.TypeName = typeName;
			Assembly assembly = Assembly.Load(assemblyName);
			this.obj_type = assembly.GetType(typeName);
			if (this.obj_type == null)
			{
				throw new RemotingException("Type not found: " + typeName + ", " + assemblyName);
			}
		}

		// Token: 0x17000815 RID: 2069
		// (get) Token: 0x06002DD2 RID: 11730 RVA: 0x00098CEC File Offset: 0x00096EEC
		// (set) Token: 0x06002DD3 RID: 11731 RVA: 0x00098CF4 File Offset: 0x00096EF4
		public string ApplicationUrl
		{
			get
			{
				return this.app_url;
			}
			set
			{
				this.app_url = value;
			}
		}

		// Token: 0x17000816 RID: 2070
		// (get) Token: 0x06002DD4 RID: 11732 RVA: 0x00098D00 File Offset: 0x00096F00
		public Type ObjectType
		{
			get
			{
				return this.obj_type;
			}
		}

		// Token: 0x17000817 RID: 2071
		// (get) Token: 0x06002DD5 RID: 11733 RVA: 0x00098D08 File Offset: 0x00096F08
		public string ObjectUrl
		{
			get
			{
				return this.obj_url;
			}
		}

		// Token: 0x06002DD6 RID: 11734 RVA: 0x00098D10 File Offset: 0x00096F10
		public override string ToString()
		{
			if (this.ApplicationUrl != null)
			{
				return base.TypeName + base.AssemblyName + this.ObjectUrl + this.ApplicationUrl;
			}
			return base.TypeName + base.AssemblyName + this.ObjectUrl;
		}

		// Token: 0x040013B5 RID: 5045
		private Type obj_type;

		// Token: 0x040013B6 RID: 5046
		private string obj_url;

		// Token: 0x040013B7 RID: 5047
		private string app_url;
	}
}
