﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	// Token: 0x02000421 RID: 1057
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.AutoDual)]
	public class ObjectHandle : MarshalByRefObject, IObjectHandle
	{
		// Token: 0x06002CE1 RID: 11489 RVA: 0x00094320 File Offset: 0x00092520
		public ObjectHandle(object o)
		{
			this._wrapped = o;
		}

		// Token: 0x06002CE2 RID: 11490 RVA: 0x00094330 File Offset: 0x00092530
		public override object InitializeLifetimeService()
		{
			return base.InitializeLifetimeService();
		}

		// Token: 0x06002CE3 RID: 11491 RVA: 0x00094338 File Offset: 0x00092538
		public object Unwrap()
		{
			return this._wrapped;
		}

		// Token: 0x04001367 RID: 4967
		private object _wrapped;
	}
}
