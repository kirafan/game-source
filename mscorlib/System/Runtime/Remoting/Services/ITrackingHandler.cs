﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting.Services
{
	// Token: 0x020004EC RID: 1260
	[ComVisible(true)]
	public interface ITrackingHandler
	{
		// Token: 0x06003296 RID: 12950
		void DisconnectedObject(object obj);

		// Token: 0x06003297 RID: 12951
		void MarshaledObject(object obj, ObjRef or);

		// Token: 0x06003298 RID: 12952
		void UnmarshaledObject(object obj, ObjRef or);
	}
}
