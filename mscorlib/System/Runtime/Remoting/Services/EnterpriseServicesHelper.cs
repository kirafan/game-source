﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Activation;
using System.Runtime.Remoting.Messaging;
using System.Runtime.Remoting.Proxies;

namespace System.Runtime.Remoting.Services
{
	// Token: 0x020004EB RID: 1259
	[ComVisible(true)]
	public sealed class EnterpriseServicesHelper
	{
		// Token: 0x06003293 RID: 12947 RVA: 0x000A426C File Offset: 0x000A246C
		[ComVisible(true)]
		public static IConstructionReturnMessage CreateConstructionReturnMessage(IConstructionCallMessage ctorMsg, MarshalByRefObject retObj)
		{
			return new ConstructionResponse(retObj, null, ctorMsg);
		}

		// Token: 0x06003294 RID: 12948 RVA: 0x000A4278 File Offset: 0x000A2478
		[MonoTODO]
		public static void SwitchWrappers(RealProxy oldcp, RealProxy newcp)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06003295 RID: 12949 RVA: 0x000A4280 File Offset: 0x000A2480
		[MonoTODO]
		public static object WrapIUnknownWithComObject(IntPtr punk)
		{
			throw new NotSupportedException();
		}
	}
}
