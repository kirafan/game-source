﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting
{
	// Token: 0x0200041F RID: 1055
	[ComVisible(true)]
	public interface IEnvoyInfo
	{
		// Token: 0x170007FB RID: 2043
		// (get) Token: 0x06002CDC RID: 11484
		// (set) Token: 0x06002CDD RID: 11485
		IMessageSink EnvoySinks { get; set; }
	}
}
