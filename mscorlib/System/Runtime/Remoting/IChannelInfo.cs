﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	// Token: 0x0200041B RID: 1051
	[ComVisible(true)]
	public interface IChannelInfo
	{
		// Token: 0x170007EE RID: 2030
		// (get) Token: 0x06002CBD RID: 11453
		// (set) Token: 0x06002CBE RID: 11454
		object[] ChannelData { get; set; }
	}
}
