﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting
{
	// Token: 0x02000417 RID: 1047
	[ComVisible(true)]
	public class ActivatedServiceTypeEntry : TypeEntry
	{
		// Token: 0x06002CB3 RID: 11443 RVA: 0x00093F18 File Offset: 0x00092118
		public ActivatedServiceTypeEntry(Type type)
		{
			base.AssemblyName = type.Assembly.FullName;
			base.TypeName = type.FullName;
			this.obj_type = type;
		}

		// Token: 0x06002CB4 RID: 11444 RVA: 0x00093F50 File Offset: 0x00092150
		public ActivatedServiceTypeEntry(string typeName, string assemblyName)
		{
			base.AssemblyName = assemblyName;
			base.TypeName = typeName;
			Assembly assembly = Assembly.Load(assemblyName);
			this.obj_type = assembly.GetType(typeName);
			if (this.obj_type == null)
			{
				throw new RemotingException("Type not found: " + typeName + ", " + assemblyName);
			}
		}

		// Token: 0x170007EB RID: 2027
		// (get) Token: 0x06002CB5 RID: 11445 RVA: 0x00093FA8 File Offset: 0x000921A8
		// (set) Token: 0x06002CB6 RID: 11446 RVA: 0x00093FAC File Offset: 0x000921AC
		public IContextAttribute[] ContextAttributes
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x170007EC RID: 2028
		// (get) Token: 0x06002CB7 RID: 11447 RVA: 0x00093FB0 File Offset: 0x000921B0
		public Type ObjectType
		{
			get
			{
				return this.obj_type;
			}
		}

		// Token: 0x06002CB8 RID: 11448 RVA: 0x00093FB8 File Offset: 0x000921B8
		public override string ToString()
		{
			return base.AssemblyName + base.TypeName;
		}

		// Token: 0x04001358 RID: 4952
		private Type obj_type;
	}
}
