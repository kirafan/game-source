﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Remoting
{
	// Token: 0x02000418 RID: 1048
	[ComVisible(true)]
	public enum CustomErrorsModes
	{
		// Token: 0x0400135A RID: 4954
		On,
		// Token: 0x0400135B RID: 4955
		Off,
		// Token: 0x0400135C RID: 4956
		RemoteOnly
	}
}
