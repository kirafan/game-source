﻿using System;
using System.Runtime.Remoting.Messaging;

namespace System.Runtime.Remoting
{
	// Token: 0x02000431 RID: 1073
	internal class DisposerReplySink : IMessageSink
	{
		// Token: 0x06002DA2 RID: 11682 RVA: 0x0009801C File Offset: 0x0009621C
		public DisposerReplySink(IMessageSink next, IDisposable disposable)
		{
			this._next = next;
			this._disposable = disposable;
		}

		// Token: 0x06002DA3 RID: 11683 RVA: 0x00098034 File Offset: 0x00096234
		public IMessage SyncProcessMessage(IMessage msg)
		{
			this._disposable.Dispose();
			return this._next.SyncProcessMessage(msg);
		}

		// Token: 0x06002DA4 RID: 11684 RVA: 0x00098050 File Offset: 0x00096250
		public IMessageCtrl AsyncProcessMessage(IMessage msg, IMessageSink replySink)
		{
			throw new NotSupportedException();
		}

		// Token: 0x1700080D RID: 2061
		// (get) Token: 0x06002DA5 RID: 11685 RVA: 0x00098058 File Offset: 0x00096258
		public IMessageSink NextSink
		{
			get
			{
				return this._next;
			}
		}

		// Token: 0x040013A4 RID: 5028
		private IMessageSink _next;

		// Token: 0x040013A5 RID: 5029
		private IDisposable _disposable;
	}
}
