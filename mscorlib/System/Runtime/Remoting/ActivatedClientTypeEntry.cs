﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Remoting.Contexts;

namespace System.Runtime.Remoting
{
	// Token: 0x02000416 RID: 1046
	[ComVisible(true)]
	public class ActivatedClientTypeEntry : TypeEntry
	{
		// Token: 0x06002CAC RID: 11436 RVA: 0x00093E3C File Offset: 0x0009203C
		public ActivatedClientTypeEntry(Type type, string appUrl)
		{
			base.AssemblyName = type.Assembly.FullName;
			base.TypeName = type.FullName;
			this.applicationUrl = appUrl;
			this.obj_type = type;
		}

		// Token: 0x06002CAD RID: 11437 RVA: 0x00093E7C File Offset: 0x0009207C
		public ActivatedClientTypeEntry(string typeName, string assemblyName, string appUrl)
		{
			base.AssemblyName = assemblyName;
			base.TypeName = typeName;
			this.applicationUrl = appUrl;
			Assembly assembly = Assembly.Load(assemblyName);
			this.obj_type = assembly.GetType(typeName);
			if (this.obj_type == null)
			{
				throw new RemotingException("Type not found: " + typeName + ", " + assemblyName);
			}
		}

		// Token: 0x170007E8 RID: 2024
		// (get) Token: 0x06002CAE RID: 11438 RVA: 0x00093EDC File Offset: 0x000920DC
		public string ApplicationUrl
		{
			get
			{
				return this.applicationUrl;
			}
		}

		// Token: 0x170007E9 RID: 2025
		// (get) Token: 0x06002CAF RID: 11439 RVA: 0x00093EE4 File Offset: 0x000920E4
		// (set) Token: 0x06002CB0 RID: 11440 RVA: 0x00093EE8 File Offset: 0x000920E8
		public IContextAttribute[] ContextAttributes
		{
			get
			{
				return null;
			}
			set
			{
			}
		}

		// Token: 0x170007EA RID: 2026
		// (get) Token: 0x06002CB1 RID: 11441 RVA: 0x00093EEC File Offset: 0x000920EC
		public Type ObjectType
		{
			get
			{
				return this.obj_type;
			}
		}

		// Token: 0x06002CB2 RID: 11442 RVA: 0x00093EF4 File Offset: 0x000920F4
		public override string ToString()
		{
			return base.TypeName + base.AssemblyName + this.ApplicationUrl;
		}

		// Token: 0x04001356 RID: 4950
		private string applicationUrl;

		// Token: 0x04001357 RID: 4951
		private Type obj_type;
	}
}
