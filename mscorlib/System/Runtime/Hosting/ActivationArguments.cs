﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.Hosting
{
	// Token: 0x0200034C RID: 844
	[ComVisible(true)]
	[Serializable]
	public sealed class ActivationArguments
	{
		// Token: 0x060028BC RID: 10428 RVA: 0x00091FCC File Offset: 0x000901CC
		public ActivationArguments(ActivationContext activationData)
		{
			if (activationData == null)
			{
				throw new ArgumentNullException("activationData");
			}
			this._context = activationData;
			this._identity = activationData.Identity;
		}

		// Token: 0x060028BD RID: 10429 RVA: 0x00092004 File Offset: 0x00090204
		public ActivationArguments(ApplicationIdentity applicationIdentity)
		{
			if (applicationIdentity == null)
			{
				throw new ArgumentNullException("applicationIdentity");
			}
			this._identity = applicationIdentity;
		}

		// Token: 0x060028BE RID: 10430 RVA: 0x00092024 File Offset: 0x00090224
		public ActivationArguments(ActivationContext activationContext, string[] activationData)
		{
			if (activationContext == null)
			{
				throw new ArgumentNullException("activationContext");
			}
			this._context = activationContext;
			this._identity = activationContext.Identity;
			this._data = activationData;
		}

		// Token: 0x060028BF RID: 10431 RVA: 0x00092058 File Offset: 0x00090258
		public ActivationArguments(ApplicationIdentity applicationIdentity, string[] activationData)
		{
			if (applicationIdentity == null)
			{
				throw new ArgumentNullException("applicationIdentity");
			}
			this._identity = applicationIdentity;
			this._data = activationData;
		}

		// Token: 0x17000751 RID: 1873
		// (get) Token: 0x060028C0 RID: 10432 RVA: 0x00092080 File Offset: 0x00090280
		public ActivationContext ActivationContext
		{
			get
			{
				return this._context;
			}
		}

		// Token: 0x17000752 RID: 1874
		// (get) Token: 0x060028C1 RID: 10433 RVA: 0x00092088 File Offset: 0x00090288
		public string[] ActivationData
		{
			get
			{
				return this._data;
			}
		}

		// Token: 0x17000753 RID: 1875
		// (get) Token: 0x060028C2 RID: 10434 RVA: 0x00092090 File Offset: 0x00090290
		public ApplicationIdentity ApplicationIdentity
		{
			get
			{
				return this._identity;
			}
		}

		// Token: 0x040010AA RID: 4266
		private ActivationContext _context;

		// Token: 0x040010AB RID: 4267
		private ApplicationIdentity _identity;

		// Token: 0x040010AC RID: 4268
		private string[] _data;
	}
}
