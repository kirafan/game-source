﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200032A RID: 810
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter, Inherited = false)]
	[Serializable]
	public sealed class DateTimeConstantAttribute : CustomConstantAttribute
	{
		// Token: 0x0600289F RID: 10399 RVA: 0x00091E70 File Offset: 0x00090070
		public DateTimeConstantAttribute(long ticks)
		{
			this.ticks = ticks;
		}

		// Token: 0x17000747 RID: 1863
		// (get) Token: 0x060028A0 RID: 10400 RVA: 0x00091E80 File Offset: 0x00090080
		internal long Ticks
		{
			get
			{
				return this.ticks;
			}
		}

		// Token: 0x17000748 RID: 1864
		// (get) Token: 0x060028A1 RID: 10401 RVA: 0x00091E88 File Offset: 0x00090088
		public override object Value
		{
			get
			{
				return this.ticks;
			}
		}

		// Token: 0x04001089 RID: 4233
		private long ticks;
	}
}
