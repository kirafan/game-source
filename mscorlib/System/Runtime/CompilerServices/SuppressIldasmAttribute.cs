﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000345 RID: 837
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Module)]
	public sealed class SuppressIldasmAttribute : Attribute
	{
	}
}
