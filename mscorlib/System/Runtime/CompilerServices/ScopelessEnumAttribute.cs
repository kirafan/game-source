﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000344 RID: 836
	[AttributeUsage(AttributeTargets.Enum)]
	[Serializable]
	public sealed class ScopelessEnumAttribute : Attribute
	{
	}
}
