﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200032D RID: 813
	[ComVisible(true)]
	public class DiscardableAttribute : Attribute
	{
	}
}
