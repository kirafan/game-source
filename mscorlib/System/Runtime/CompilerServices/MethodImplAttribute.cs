﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000041 RID: 65
	[AttributeUsage(AttributeTargets.Constructor | AttributeTargets.Method, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class MethodImplAttribute : Attribute
	{
		// Token: 0x06000657 RID: 1623 RVA: 0x00014A7C File Offset: 0x00012C7C
		public MethodImplAttribute()
		{
		}

		// Token: 0x06000658 RID: 1624 RVA: 0x00014A84 File Offset: 0x00012C84
		public MethodImplAttribute(short value)
		{
			this._val = (MethodImplOptions)value;
		}

		// Token: 0x06000659 RID: 1625 RVA: 0x00014A94 File Offset: 0x00012C94
		public MethodImplAttribute(MethodImplOptions methodImplOptions)
		{
			this._val = methodImplOptions;
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x0600065A RID: 1626 RVA: 0x00014AA4 File Offset: 0x00012CA4
		public MethodImplOptions Value
		{
			get
			{
				return this._val;
			}
		}

		// Token: 0x0400008E RID: 142
		private MethodImplOptions _val;

		// Token: 0x0400008F RID: 143
		public MethodCodeType MethodCodeType;
	}
}
