﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000325 RID: 805
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum CompilationRelaxations
	{
		// Token: 0x04001087 RID: 4231
		NoStringInterning = 8
	}
}
