﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000320 RID: 800
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field)]
	public sealed class AccessedThroughPropertyAttribute : Attribute
	{
		// Token: 0x06002893 RID: 10387 RVA: 0x00091E00 File Offset: 0x00090000
		public AccessedThroughPropertyAttribute(string propertyName)
		{
			this.name = propertyName;
		}

		// Token: 0x17000744 RID: 1860
		// (get) Token: 0x06002894 RID: 10388 RVA: 0x00091E10 File Offset: 0x00090010
		public string PropertyName
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04001085 RID: 4229
		private string name;
	}
}
