﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000044 RID: 68
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Property, Inherited = true)]
	[Serializable]
	public sealed class IndexerNameAttribute : Attribute
	{
		// Token: 0x0600065F RID: 1631 RVA: 0x00014ADC File Offset: 0x00012CDC
		public IndexerNameAttribute(string indexerName)
		{
		}
	}
}
