﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200032F RID: 815
	[AttributeUsage(AttributeTargets.Struct)]
	[Serializable]
	public sealed class HasCopySemanticsAttribute : Attribute
	{
	}
}
