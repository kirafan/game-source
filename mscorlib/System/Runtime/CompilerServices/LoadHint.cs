﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200033E RID: 830
	[Serializable]
	public enum LoadHint
	{
		// Token: 0x0400108E RID: 4238
		Default,
		// Token: 0x0400108F RID: 4239
		Always,
		// Token: 0x04001090 RID: 4240
		Sometimes
	}
}
