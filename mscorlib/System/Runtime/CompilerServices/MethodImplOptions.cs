﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000340 RID: 832
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum MethodImplOptions
	{
		// Token: 0x04001097 RID: 4247
		Unmanaged = 4,
		// Token: 0x04001098 RID: 4248
		ForwardRef = 16,
		// Token: 0x04001099 RID: 4249
		InternalCall = 4096,
		// Token: 0x0400109A RID: 4250
		Synchronized = 32,
		// Token: 0x0400109B RID: 4251
		NoInlining = 8,
		// Token: 0x0400109C RID: 4252
		PreserveSig = 128,
		// Token: 0x0400109D RID: 4253
		NoOptimization = 64
	}
}
