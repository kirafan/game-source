﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000051 RID: 81
	[AttributeUsage(AttributeTargets.All)]
	[Serializable]
	public sealed class CompilerGeneratedAttribute : Attribute
	{
	}
}
