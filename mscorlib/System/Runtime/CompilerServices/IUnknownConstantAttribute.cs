﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000331 RID: 817
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter, Inherited = false)]
	[Serializable]
	public sealed class IUnknownConstantAttribute : CustomConstantAttribute
	{
		// Token: 0x1700074D RID: 1869
		// (get) Token: 0x060028AD RID: 10413 RVA: 0x00091F04 File Offset: 0x00090104
		public override object Value
		{
			get
			{
				return null;
			}
		}
	}
}
