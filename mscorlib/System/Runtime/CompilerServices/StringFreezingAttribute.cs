﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000346 RID: 838
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[Serializable]
	public sealed class StringFreezingAttribute : Attribute
	{
	}
}
