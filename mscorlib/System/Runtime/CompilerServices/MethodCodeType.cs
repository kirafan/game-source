﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200033F RID: 831
	[ComVisible(true)]
	[Serializable]
	public enum MethodCodeType
	{
		// Token: 0x04001092 RID: 4242
		IL,
		// Token: 0x04001093 RID: 4243
		Native,
		// Token: 0x04001094 RID: 4244
		OPTIL,
		// Token: 0x04001095 RID: 4245
		Runtime
	}
}
