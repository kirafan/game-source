﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200004F RID: 79
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true, Inherited = false)]
	public sealed class TypeForwardedToAttribute : Attribute
	{
		// Token: 0x06000676 RID: 1654 RVA: 0x00014BCC File Offset: 0x00012DCC
		public TypeForwardedToAttribute(Type destination)
		{
			this.destination = destination;
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000677 RID: 1655 RVA: 0x00014BDC File Offset: 0x00012DDC
		public Type Destination
		{
			get
			{
				return this.destination;
			}
		}

		// Token: 0x040000A2 RID: 162
		private Type destination;
	}
}
