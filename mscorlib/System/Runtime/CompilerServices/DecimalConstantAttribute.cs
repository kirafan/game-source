﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000057 RID: 87
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class DecimalConstantAttribute : Attribute
	{
		// Token: 0x06000687 RID: 1671 RVA: 0x00014C8C File Offset: 0x00012E8C
		[CLSCompliant(false)]
		public DecimalConstantAttribute(byte scale, byte sign, uint hi, uint mid, uint low)
		{
			this.scale = scale;
			this.sign = Convert.ToBoolean(sign);
			this.hi = (int)hi;
			this.mid = (int)mid;
			this.low = (int)low;
		}

		// Token: 0x06000688 RID: 1672 RVA: 0x00014CCC File Offset: 0x00012ECC
		public DecimalConstantAttribute(byte scale, byte sign, int hi, int mid, int low)
		{
			this.scale = scale;
			this.sign = Convert.ToBoolean(sign);
			this.hi = hi;
			this.mid = mid;
			this.low = low;
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x06000689 RID: 1673 RVA: 0x00014D0C File Offset: 0x00012F0C
		public decimal Value
		{
			get
			{
				return new decimal(this.low, this.mid, this.hi, this.sign, this.scale);
			}
		}

		// Token: 0x040000A9 RID: 169
		private byte scale;

		// Token: 0x040000AA RID: 170
		private bool sign;

		// Token: 0x040000AB RID: 171
		private int hi;

		// Token: 0x040000AC RID: 172
		private int mid;

		// Token: 0x040000AD RID: 173
		private int low;
	}
}
