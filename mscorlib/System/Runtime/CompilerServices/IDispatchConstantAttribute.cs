﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000330 RID: 816
	[AttributeUsage(AttributeTargets.Field | AttributeTargets.Parameter, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class IDispatchConstantAttribute : CustomConstantAttribute
	{
		// Token: 0x1700074C RID: 1868
		// (get) Token: 0x060028AB RID: 10411 RVA: 0x00091EF8 File Offset: 0x000900F8
		public override object Value
		{
			get
			{
				return null;
			}
		}
	}
}
