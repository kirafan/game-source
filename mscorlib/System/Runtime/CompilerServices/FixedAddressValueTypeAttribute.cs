﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200032E RID: 814
	[AttributeUsage(AttributeTargets.Field)]
	[Serializable]
	public sealed class FixedAddressValueTypeAttribute : Attribute
	{
	}
}
