﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000053 RID: 83
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false, AllowMultiple = false)]
	[Serializable]
	public sealed class RuntimeCompatibilityAttribute : Attribute
	{
		// Token: 0x170000BF RID: 191
		// (get) Token: 0x06000681 RID: 1665 RVA: 0x00014C50 File Offset: 0x00012E50
		// (set) Token: 0x06000682 RID: 1666 RVA: 0x00014C58 File Offset: 0x00012E58
		public bool WrapNonExceptionThrows
		{
			get
			{
				return this.wrap_non_exception_throws;
			}
			set
			{
				this.wrap_non_exception_throws = value;
			}
		}

		// Token: 0x040000A7 RID: 167
		private bool wrap_non_exception_throws;
	}
}
