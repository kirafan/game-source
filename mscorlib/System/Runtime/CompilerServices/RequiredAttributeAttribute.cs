﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000047 RID: 71
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Interface, AllowMultiple = true, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public sealed class RequiredAttributeAttribute : Attribute
	{
		// Token: 0x06000668 RID: 1640 RVA: 0x00014B34 File Offset: 0x00012D34
		public RequiredAttributeAttribute(Type requiredContract)
		{
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x06000669 RID: 1641 RVA: 0x00014B3C File Offset: 0x00012D3C
		public Type RequiredContract
		{
			get
			{
				throw new NotSupportedException();
			}
		}
	}
}
