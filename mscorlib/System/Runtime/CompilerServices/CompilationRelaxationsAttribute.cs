﻿using System;
using System.Runtime.InteropServices;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000326 RID: 806
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Module | AttributeTargets.Class | AttributeTargets.Method)]
	[ComVisible(true)]
	[Serializable]
	public class CompilationRelaxationsAttribute : Attribute
	{
		// Token: 0x06002899 RID: 10393 RVA: 0x00091E38 File Offset: 0x00090038
		public CompilationRelaxationsAttribute(int relaxations)
		{
			this.relax = relaxations;
		}

		// Token: 0x0600289A RID: 10394 RVA: 0x00091E48 File Offset: 0x00090048
		public CompilationRelaxationsAttribute(CompilationRelaxations relaxations)
		{
			this.relax = (int)relaxations;
		}

		// Token: 0x17000745 RID: 1861
		// (get) Token: 0x0600289B RID: 10395 RVA: 0x00091E58 File Offset: 0x00090058
		public int CompilationRelaxations
		{
			get
			{
				return this.relax;
			}
		}

		// Token: 0x04001088 RID: 4232
		private int relax;
	}
}
