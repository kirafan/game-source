﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200032C RID: 812
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true)]
	[Serializable]
	public sealed class DependencyAttribute : Attribute
	{
		// Token: 0x060028A4 RID: 10404 RVA: 0x00091EB0 File Offset: 0x000900B0
		public DependencyAttribute(string dependentAssemblyArgument, LoadHint loadHintArgument)
		{
			this.dependentAssembly = dependentAssemblyArgument;
			this.hint = loadHintArgument;
		}

		// Token: 0x1700074A RID: 1866
		// (get) Token: 0x060028A5 RID: 10405 RVA: 0x00091EC8 File Offset: 0x000900C8
		public string DependentAssembly
		{
			get
			{
				return this.dependentAssembly;
			}
		}

		// Token: 0x1700074B RID: 1867
		// (get) Token: 0x060028A6 RID: 10406 RVA: 0x00091ED0 File Offset: 0x000900D0
		public LoadHint LoadHint
		{
			get
			{
				return this.hint;
			}
		}

		// Token: 0x0400108B RID: 4235
		private string dependentAssembly;

		// Token: 0x0400108C RID: 4236
		private LoadHint hint;
	}
}
