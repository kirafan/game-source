﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000055 RID: 85
	[AttributeUsage(AttributeTargets.Struct)]
	[Serializable]
	public sealed class UnsafeValueTypeAttribute : Attribute
	{
	}
}
