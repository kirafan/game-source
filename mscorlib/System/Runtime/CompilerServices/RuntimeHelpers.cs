﻿using System;
using System.Runtime.ConstrainedExecution;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000061 RID: 97
	public static class RuntimeHelpers
	{
		// Token: 0x060006C0 RID: 1728
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void InitializeArray(Array array, IntPtr fldHandle);

		// Token: 0x060006C1 RID: 1729 RVA: 0x00015280 File Offset: 0x00013480
		public static void InitializeArray(Array array, RuntimeFieldHandle fldHandle)
		{
			if (array == null || fldHandle.Value == IntPtr.Zero)
			{
				throw new ArgumentNullException();
			}
			RuntimeHelpers.InitializeArray(array, fldHandle.Value);
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x060006C2 RID: 1730
		public static extern int OffsetToStringData { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060006C3 RID: 1731 RVA: 0x000152B4 File Offset: 0x000134B4
		public static int GetHashCode(object o)
		{
			return object.InternalGetHashCode(o);
		}

		// Token: 0x060006C4 RID: 1732 RVA: 0x000152BC File Offset: 0x000134BC
		public new static bool Equals(object o1, object o2)
		{
			if (o1 == o2)
			{
				return true;
			}
			if (o1 == null || o2 == null)
			{
				return false;
			}
			if (o1 is ValueType)
			{
				return ValueType.DefaultEquals(o1, o2);
			}
			return object.Equals(o1, o2);
		}

		// Token: 0x060006C5 RID: 1733
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern object GetObjectValue(object obj);

		// Token: 0x060006C6 RID: 1734
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void RunClassConstructor(IntPtr type);

		// Token: 0x060006C7 RID: 1735 RVA: 0x000152F0 File Offset: 0x000134F0
		public static void RunClassConstructor(RuntimeTypeHandle type)
		{
			if (type.Value == IntPtr.Zero)
			{
				throw new ArgumentException("Handle is not initialized.", "type");
			}
			RuntimeHelpers.RunClassConstructor(type.Value);
		}

		// Token: 0x060006C8 RID: 1736 RVA: 0x00015330 File Offset: 0x00013530
		[MonoTODO("Currently a no-op")]
		public static void ExecuteCodeWithGuaranteedCleanup(RuntimeHelpers.TryCode code, RuntimeHelpers.CleanupCode backoutCode, object userData)
		{
		}

		// Token: 0x060006C9 RID: 1737 RVA: 0x00015334 File Offset: 0x00013534
		[MonoTODO("Currently a no-op")]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static void PrepareConstrainedRegions()
		{
		}

		// Token: 0x060006CA RID: 1738 RVA: 0x00015338 File Offset: 0x00013538
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		[MonoTODO("Currently a no-op")]
		public static void PrepareConstrainedRegionsNoOP()
		{
		}

		// Token: 0x060006CB RID: 1739 RVA: 0x0001533C File Offset: 0x0001353C
		[MonoTODO("Currently a no-op")]
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.MayFail)]
		public static void ProbeForSufficientStack()
		{
		}

		// Token: 0x060006CC RID: 1740 RVA: 0x00015340 File Offset: 0x00013540
		[MonoTODO("Currently a no-op")]
		public static void PrepareDelegate(Delegate d)
		{
			if (d == null)
			{
				throw new ArgumentNullException("d");
			}
		}

		// Token: 0x060006CD RID: 1741 RVA: 0x00015354 File Offset: 0x00013554
		[MonoTODO("Currently a no-op")]
		public static void PrepareMethod(RuntimeMethodHandle method)
		{
		}

		// Token: 0x060006CE RID: 1742 RVA: 0x00015358 File Offset: 0x00013558
		[MonoTODO("Currently a no-op")]
		public static void PrepareMethod(RuntimeMethodHandle method, RuntimeTypeHandle[] instantiation)
		{
		}

		// Token: 0x060006CF RID: 1743 RVA: 0x0001535C File Offset: 0x0001355C
		public static void RunModuleConstructor(ModuleHandle module)
		{
			if (module == ModuleHandle.EmptyHandle)
			{
				throw new ArgumentException("Handle is not initialized.", "module");
			}
			RuntimeHelpers.RunModuleConstructor(module.Value);
		}

		// Token: 0x060006D0 RID: 1744
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void RunModuleConstructor(IntPtr module);

		// Token: 0x02000062 RID: 98
		// (Invoke) Token: 0x060006D2 RID: 1746
		public delegate void TryCode(object userData);

		// Token: 0x02000063 RID: 99
		// (Invoke) Token: 0x060006D6 RID: 1750
		public delegate void CleanupCode(object userData, bool exceptionThrown);
	}
}
