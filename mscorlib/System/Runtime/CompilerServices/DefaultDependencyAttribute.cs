﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x0200032B RID: 811
	[AttributeUsage(AttributeTargets.Assembly)]
	[Serializable]
	public sealed class DefaultDependencyAttribute : Attribute
	{
		// Token: 0x060028A2 RID: 10402 RVA: 0x00091E98 File Offset: 0x00090098
		public DefaultDependencyAttribute(LoadHint loadHintArgument)
		{
			this.hint = loadHintArgument;
		}

		// Token: 0x17000749 RID: 1865
		// (get) Token: 0x060028A3 RID: 10403 RVA: 0x00091EA8 File Offset: 0x000900A8
		public LoadHint LoadHint
		{
			get
			{
				return this.hint;
			}
		}

		// Token: 0x0400108A RID: 4234
		private LoadHint hint;
	}
}
