﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000052 RID: 82
	[AttributeUsage(AttributeTargets.Assembly, AllowMultiple = true, Inherited = false)]
	public sealed class InternalsVisibleToAttribute : Attribute
	{
		// Token: 0x0600067C RID: 1660 RVA: 0x00014C14 File Offset: 0x00012E14
		public InternalsVisibleToAttribute(string assemblyName)
		{
			this.assemblyName = assemblyName;
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x0600067D RID: 1661 RVA: 0x00014C2C File Offset: 0x00012E2C
		public string AssemblyName
		{
			get
			{
				return this.assemblyName;
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x0600067E RID: 1662 RVA: 0x00014C34 File Offset: 0x00012E34
		// (set) Token: 0x0600067F RID: 1663 RVA: 0x00014C3C File Offset: 0x00012E3C
		public bool AllInternalsVisible
		{
			get
			{
				return this.all_visible;
			}
			set
			{
				this.all_visible = value;
			}
		}

		// Token: 0x040000A5 RID: 165
		private string assemblyName;

		// Token: 0x040000A6 RID: 166
		private bool all_visible = true;
	}
}
