﻿using System;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000342 RID: 834
	[Serializable]
	public sealed class RuntimeWrappedException : Exception
	{
		// Token: 0x060028AF RID: 10415 RVA: 0x00091F10 File Offset: 0x00090110
		private RuntimeWrappedException()
		{
		}

		// Token: 0x1700074E RID: 1870
		// (get) Token: 0x060028B0 RID: 10416 RVA: 0x00091F18 File Offset: 0x00090118
		public object WrappedException
		{
			get
			{
				return this.wrapped_exception;
			}
		}

		// Token: 0x060028B1 RID: 10417 RVA: 0x00091F20 File Offset: 0x00090120
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("WrappedException", this.wrapped_exception);
		}

		// Token: 0x0400109E RID: 4254
		private object wrapped_exception;
	}
}
