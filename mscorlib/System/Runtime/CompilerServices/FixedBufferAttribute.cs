﻿using System;

namespace System.Runtime.CompilerServices
{
	// Token: 0x02000050 RID: 80
	[AttributeUsage(AttributeTargets.Field, Inherited = false)]
	public sealed class FixedBufferAttribute : Attribute
	{
		// Token: 0x06000678 RID: 1656 RVA: 0x00014BE4 File Offset: 0x00012DE4
		public FixedBufferAttribute(Type elementType, int length)
		{
			this.elementType = elementType;
			this.length = length;
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000679 RID: 1657 RVA: 0x00014BFC File Offset: 0x00012DFC
		public Type ElementType
		{
			get
			{
				return this.elementType;
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x0600067A RID: 1658 RVA: 0x00014C04 File Offset: 0x00012E04
		public int Length
		{
			get
			{
				return this.length;
			}
		}

		// Token: 0x040000A3 RID: 163
		private Type elementType;

		// Token: 0x040000A4 RID: 164
		private int length;
	}
}
