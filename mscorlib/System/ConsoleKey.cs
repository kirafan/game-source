﻿using System;

namespace System
{
	// Token: 0x02000114 RID: 276
	[Serializable]
	public enum ConsoleKey
	{
		// Token: 0x040003E3 RID: 995
		Backspace = 8,
		// Token: 0x040003E4 RID: 996
		Tab,
		// Token: 0x040003E5 RID: 997
		Clear = 12,
		// Token: 0x040003E6 RID: 998
		Enter,
		// Token: 0x040003E7 RID: 999
		Pause = 19,
		// Token: 0x040003E8 RID: 1000
		Escape = 27,
		// Token: 0x040003E9 RID: 1001
		Spacebar = 32,
		// Token: 0x040003EA RID: 1002
		PageUp,
		// Token: 0x040003EB RID: 1003
		PageDown,
		// Token: 0x040003EC RID: 1004
		End,
		// Token: 0x040003ED RID: 1005
		Home,
		// Token: 0x040003EE RID: 1006
		LeftArrow,
		// Token: 0x040003EF RID: 1007
		UpArrow,
		// Token: 0x040003F0 RID: 1008
		RightArrow,
		// Token: 0x040003F1 RID: 1009
		DownArrow,
		// Token: 0x040003F2 RID: 1010
		Select,
		// Token: 0x040003F3 RID: 1011
		Print,
		// Token: 0x040003F4 RID: 1012
		Execute,
		// Token: 0x040003F5 RID: 1013
		PrintScreen,
		// Token: 0x040003F6 RID: 1014
		Insert,
		// Token: 0x040003F7 RID: 1015
		Delete,
		// Token: 0x040003F8 RID: 1016
		Help,
		// Token: 0x040003F9 RID: 1017
		D0,
		// Token: 0x040003FA RID: 1018
		D1,
		// Token: 0x040003FB RID: 1019
		D2,
		// Token: 0x040003FC RID: 1020
		D3,
		// Token: 0x040003FD RID: 1021
		D4,
		// Token: 0x040003FE RID: 1022
		D5,
		// Token: 0x040003FF RID: 1023
		D6,
		// Token: 0x04000400 RID: 1024
		D7,
		// Token: 0x04000401 RID: 1025
		D8,
		// Token: 0x04000402 RID: 1026
		D9,
		// Token: 0x04000403 RID: 1027
		A = 65,
		// Token: 0x04000404 RID: 1028
		B,
		// Token: 0x04000405 RID: 1029
		C,
		// Token: 0x04000406 RID: 1030
		D,
		// Token: 0x04000407 RID: 1031
		E,
		// Token: 0x04000408 RID: 1032
		F,
		// Token: 0x04000409 RID: 1033
		G,
		// Token: 0x0400040A RID: 1034
		H,
		// Token: 0x0400040B RID: 1035
		I,
		// Token: 0x0400040C RID: 1036
		J,
		// Token: 0x0400040D RID: 1037
		K,
		// Token: 0x0400040E RID: 1038
		L,
		// Token: 0x0400040F RID: 1039
		M,
		// Token: 0x04000410 RID: 1040
		N,
		// Token: 0x04000411 RID: 1041
		O,
		// Token: 0x04000412 RID: 1042
		P,
		// Token: 0x04000413 RID: 1043
		Q,
		// Token: 0x04000414 RID: 1044
		R,
		// Token: 0x04000415 RID: 1045
		S,
		// Token: 0x04000416 RID: 1046
		T,
		// Token: 0x04000417 RID: 1047
		U,
		// Token: 0x04000418 RID: 1048
		V,
		// Token: 0x04000419 RID: 1049
		W,
		// Token: 0x0400041A RID: 1050
		X,
		// Token: 0x0400041B RID: 1051
		Y,
		// Token: 0x0400041C RID: 1052
		Z,
		// Token: 0x0400041D RID: 1053
		LeftWindows,
		// Token: 0x0400041E RID: 1054
		RightWindows,
		// Token: 0x0400041F RID: 1055
		Applications,
		// Token: 0x04000420 RID: 1056
		Sleep = 95,
		// Token: 0x04000421 RID: 1057
		NumPad0,
		// Token: 0x04000422 RID: 1058
		NumPad1,
		// Token: 0x04000423 RID: 1059
		NumPad2,
		// Token: 0x04000424 RID: 1060
		NumPad3,
		// Token: 0x04000425 RID: 1061
		NumPad4,
		// Token: 0x04000426 RID: 1062
		NumPad5,
		// Token: 0x04000427 RID: 1063
		NumPad6,
		// Token: 0x04000428 RID: 1064
		NumPad7,
		// Token: 0x04000429 RID: 1065
		NumPad8,
		// Token: 0x0400042A RID: 1066
		NumPad9,
		// Token: 0x0400042B RID: 1067
		Multiply,
		// Token: 0x0400042C RID: 1068
		Add,
		// Token: 0x0400042D RID: 1069
		Separator,
		// Token: 0x0400042E RID: 1070
		Subtract,
		// Token: 0x0400042F RID: 1071
		Decimal,
		// Token: 0x04000430 RID: 1072
		Divide,
		// Token: 0x04000431 RID: 1073
		F1,
		// Token: 0x04000432 RID: 1074
		F2,
		// Token: 0x04000433 RID: 1075
		F3,
		// Token: 0x04000434 RID: 1076
		F4,
		// Token: 0x04000435 RID: 1077
		F5,
		// Token: 0x04000436 RID: 1078
		F6,
		// Token: 0x04000437 RID: 1079
		F7,
		// Token: 0x04000438 RID: 1080
		F8,
		// Token: 0x04000439 RID: 1081
		F9,
		// Token: 0x0400043A RID: 1082
		F10,
		// Token: 0x0400043B RID: 1083
		F11,
		// Token: 0x0400043C RID: 1084
		F12,
		// Token: 0x0400043D RID: 1085
		F13,
		// Token: 0x0400043E RID: 1086
		F14,
		// Token: 0x0400043F RID: 1087
		F15,
		// Token: 0x04000440 RID: 1088
		F16,
		// Token: 0x04000441 RID: 1089
		F17,
		// Token: 0x04000442 RID: 1090
		F18,
		// Token: 0x04000443 RID: 1091
		F19,
		// Token: 0x04000444 RID: 1092
		F20,
		// Token: 0x04000445 RID: 1093
		F21,
		// Token: 0x04000446 RID: 1094
		F22,
		// Token: 0x04000447 RID: 1095
		F23,
		// Token: 0x04000448 RID: 1096
		F24,
		// Token: 0x04000449 RID: 1097
		BrowserBack = 166,
		// Token: 0x0400044A RID: 1098
		BrowserForward,
		// Token: 0x0400044B RID: 1099
		BrowserRefresh,
		// Token: 0x0400044C RID: 1100
		BrowserStop,
		// Token: 0x0400044D RID: 1101
		BrowserSearch,
		// Token: 0x0400044E RID: 1102
		BrowserFavorites,
		// Token: 0x0400044F RID: 1103
		BrowserHome,
		// Token: 0x04000450 RID: 1104
		VolumeMute,
		// Token: 0x04000451 RID: 1105
		VolumeDown,
		// Token: 0x04000452 RID: 1106
		VolumeUp,
		// Token: 0x04000453 RID: 1107
		MediaNext,
		// Token: 0x04000454 RID: 1108
		MediaPrevious,
		// Token: 0x04000455 RID: 1109
		MediaStop,
		// Token: 0x04000456 RID: 1110
		MediaPlay,
		// Token: 0x04000457 RID: 1111
		LaunchMail,
		// Token: 0x04000458 RID: 1112
		LaunchMediaSelect,
		// Token: 0x04000459 RID: 1113
		LaunchApp1,
		// Token: 0x0400045A RID: 1114
		LaunchApp2,
		// Token: 0x0400045B RID: 1115
		Oem1 = 186,
		// Token: 0x0400045C RID: 1116
		OemPlus,
		// Token: 0x0400045D RID: 1117
		OemComma,
		// Token: 0x0400045E RID: 1118
		OemMinus,
		// Token: 0x0400045F RID: 1119
		OemPeriod,
		// Token: 0x04000460 RID: 1120
		Oem2,
		// Token: 0x04000461 RID: 1121
		Oem3,
		// Token: 0x04000462 RID: 1122
		Oem4 = 219,
		// Token: 0x04000463 RID: 1123
		Oem5,
		// Token: 0x04000464 RID: 1124
		Oem6,
		// Token: 0x04000465 RID: 1125
		Oem7,
		// Token: 0x04000466 RID: 1126
		Oem8,
		// Token: 0x04000467 RID: 1127
		Oem102 = 226,
		// Token: 0x04000468 RID: 1128
		Process = 229,
		// Token: 0x04000469 RID: 1129
		Packet = 231,
		// Token: 0x0400046A RID: 1130
		Attention = 246,
		// Token: 0x0400046B RID: 1131
		CrSel,
		// Token: 0x0400046C RID: 1132
		ExSel,
		// Token: 0x0400046D RID: 1133
		EraseEndOfFile,
		// Token: 0x0400046E RID: 1134
		Play,
		// Token: 0x0400046F RID: 1135
		Zoom,
		// Token: 0x04000470 RID: 1136
		NoName,
		// Token: 0x04000471 RID: 1137
		Pa1,
		// Token: 0x04000472 RID: 1138
		OemClear
	}
}
