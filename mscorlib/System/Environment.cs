﻿using System;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;
using Microsoft.Win32;

namespace System
{
	// Token: 0x02000132 RID: 306
	[ComVisible(true)]
	public static class Environment
	{
		// Token: 0x17000256 RID: 598
		// (get) Token: 0x060010F8 RID: 4344 RVA: 0x000455FC File Offset: 0x000437FC
		public static string CommandLine
		{
			get
			{
				return string.Join(" ", Environment.GetCommandLineArgs());
			}
		}

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x060010F9 RID: 4345 RVA: 0x00045610 File Offset: 0x00043810
		// (set) Token: 0x060010FA RID: 4346 RVA: 0x00045618 File Offset: 0x00043818
		public static string CurrentDirectory
		{
			get
			{
				return Directory.GetCurrentDirectory();
			}
			set
			{
				Directory.SetCurrentDirectory(value);
			}
		}

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x060010FB RID: 4347
		// (set) Token: 0x060010FC RID: 4348
		public static extern int ExitCode { [MethodImpl(MethodImplOptions.InternalCall)] get; [MethodImpl(MethodImplOptions.InternalCall)] set; }

		// Token: 0x17000259 RID: 601
		// (get) Token: 0x060010FD RID: 4349
		public static extern bool HasShutdownStarted { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700025A RID: 602
		// (get) Token: 0x060010FE RID: 4350
		public static extern string EmbeddingHostName { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x060010FF RID: 4351
		public static extern bool SocketSecurityEnabled { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x06001100 RID: 4352 RVA: 0x00045620 File Offset: 0x00043820
		public static bool UnityWebSecurityEnabled
		{
			get
			{
				return Environment.SocketSecurityEnabled;
			}
		}

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x06001101 RID: 4353
		public static extern string MachineName { [PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Read=\"COMPUTERNAME\"/>\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700025E RID: 606
		// (get) Token: 0x06001102 RID: 4354
		public static extern string NewLine { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x06001103 RID: 4355
		internal static extern PlatformID Platform { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06001104 RID: 4356
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetOSVersionString();

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x06001105 RID: 4357 RVA: 0x00045628 File Offset: 0x00043828
		public static OperatingSystem OSVersion
		{
			get
			{
				if (Environment.os == null)
				{
					Version version = Version.CreateFromString(Environment.GetOSVersionString());
					PlatformID platform = Environment.Platform;
					Environment.os = new OperatingSystem(platform, version);
				}
				return Environment.os;
			}
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x06001106 RID: 4358 RVA: 0x00045664 File Offset: 0x00043864
		public static string StackTrace
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Unrestricted=\"true\"/>\n</PermissionSet>\n")]
			get
			{
				StackTrace stackTrace = new StackTrace(0, true);
				return stackTrace.ToString();
			}
		}

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x06001107 RID: 4359 RVA: 0x00045680 File Offset: 0x00043880
		public static string SystemDirectory
		{
			get
			{
				return Environment.GetFolderPath(Environment.SpecialFolder.System);
			}
		}

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x06001108 RID: 4360
		public static extern int TickCount { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x06001109 RID: 4361 RVA: 0x0004568C File Offset: 0x0004388C
		public static string UserDomainName
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Read=\"USERDOMAINNAME\"/>\n</PermissionSet>\n")]
			get
			{
				return Environment.MachineName;
			}
		}

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x0600110A RID: 4362 RVA: 0x00045694 File Offset: 0x00043894
		[MonoTODO("Currently always returns false, regardless of interactive state")]
		public static bool UserInteractive
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x0600110B RID: 4363
		public static extern string UserName { [PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Read=\"USERNAME;USER\"/>\n</PermissionSet>\n")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x0600110C RID: 4364 RVA: 0x00045698 File Offset: 0x00043898
		public static Version Version
		{
			get
			{
				return new Version("2.0.50727.1433");
			}
		}

		// Token: 0x17000268 RID: 616
		// (get) Token: 0x0600110D RID: 4365 RVA: 0x000456A4 File Offset: 0x000438A4
		[MonoTODO("Currently always returns zero")]
		public static long WorkingSet
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Unrestricted=\"true\"/>\n</PermissionSet>\n")]
			get
			{
				return 0L;
			}
		}

		// Token: 0x0600110E RID: 4366
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern void Exit(int exitCode);

		// Token: 0x0600110F RID: 4367 RVA: 0x000456A8 File Offset: 0x000438A8
		public static string ExpandEnvironmentVariables(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			int num = name.IndexOf('%');
			if (num == -1)
			{
				return name;
			}
			int length = name.Length;
			int num2;
			if (num == length - 1 || (num2 = name.IndexOf('%', num + 1)) == -1)
			{
				return name;
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(name, 0, num);
			Hashtable hashtable = null;
			do
			{
				string text = name.Substring(num + 1, num2 - num - 1);
				string text2 = Environment.GetEnvironmentVariable(text);
				if (text2 == null && Environment.IsRunningOnWindows)
				{
					if (hashtable == null)
					{
						hashtable = Environment.GetEnvironmentVariablesNoCase();
					}
					text2 = (hashtable[text] as string);
				}
				if (text2 == null)
				{
					stringBuilder.Append('%');
					stringBuilder.Append(text);
					num2--;
				}
				else
				{
					stringBuilder.Append(text2);
				}
				int num3 = num2;
				num = name.IndexOf('%', num2 + 1);
				num2 = ((num != -1 && num2 <= length - 1) ? name.IndexOf('%', num + 1) : -1);
				int count;
				if (num == -1 || num2 == -1)
				{
					count = length - num3 - 1;
				}
				else if (text2 != null)
				{
					count = num - num3 - 1;
				}
				else
				{
					count = num - num3;
				}
				if (num >= num3 || num == -1)
				{
					stringBuilder.Append(name, num3 + 1, count);
				}
			}
			while (num2 > -1 && num2 < length);
			return stringBuilder.ToString();
		}

		// Token: 0x06001110 RID: 4368
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Read=\"PATH\"/>\n</PermissionSet>\n")]
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern string[] GetCommandLineArgs();

		// Token: 0x06001111 RID: 4369
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string internalGetEnvironmentVariable(string variable);

		// Token: 0x06001112 RID: 4370 RVA: 0x0004581C File Offset: 0x00043A1C
		public static string GetEnvironmentVariable(string variable)
		{
			if (SecurityManager.SecurityEnabled)
			{
				new EnvironmentPermission(EnvironmentPermissionAccess.Read, variable).Demand();
			}
			return Environment.internalGetEnvironmentVariable(variable);
		}

		// Token: 0x06001113 RID: 4371 RVA: 0x0004583C File Offset: 0x00043A3C
		private static Hashtable GetEnvironmentVariablesNoCase()
		{
			Hashtable hashtable = new Hashtable(CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default);
			foreach (string text in Environment.GetEnvironmentVariableNames())
			{
				hashtable[text] = Environment.internalGetEnvironmentVariable(text);
			}
			return hashtable;
		}

		// Token: 0x06001114 RID: 4372 RVA: 0x00045888 File Offset: 0x00043A88
		public static IDictionary GetEnvironmentVariables()
		{
			StringBuilder stringBuilder = null;
			if (SecurityManager.SecurityEnabled)
			{
				stringBuilder = new StringBuilder();
			}
			Hashtable hashtable = new Hashtable();
			foreach (string text in Environment.GetEnvironmentVariableNames())
			{
				hashtable[text] = Environment.internalGetEnvironmentVariable(text);
				if (stringBuilder != null)
				{
					stringBuilder.Append(text);
					stringBuilder.Append(";");
				}
			}
			if (stringBuilder != null)
			{
				new EnvironmentPermission(EnvironmentPermissionAccess.Read, stringBuilder.ToString()).Demand();
			}
			return hashtable;
		}

		// Token: 0x06001115 RID: 4373
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string GetWindowsFolderPath(int folder);

		// Token: 0x06001116 RID: 4374 RVA: 0x00045910 File Offset: 0x00043B10
		public static string GetFolderPath(Environment.SpecialFolder folder)
		{
			string text;
			if (Environment.IsRunningOnWindows)
			{
				text = Environment.GetWindowsFolderPath((int)folder);
			}
			else
			{
				text = Environment.InternalGetFolderPath(folder);
			}
			if (text != null && text.Length > 0 && SecurityManager.SecurityEnabled)
			{
				new FileIOPermission(FileIOPermissionAccess.PathDiscovery, text).Demand();
			}
			return text;
		}

		// Token: 0x06001117 RID: 4375 RVA: 0x00045968 File Offset: 0x00043B68
		private static string ReadXdgUserDir(string config_dir, string home_dir, string key, string fallback)
		{
			string text = Environment.internalGetEnvironmentVariable(key);
			if (text != null && text != string.Empty)
			{
				return text;
			}
			string path = Path.Combine(config_dir, "user-dirs.dirs");
			if (!File.Exists(path))
			{
				return Path.Combine(home_dir, fallback);
			}
			try
			{
				using (StreamReader streamReader = new StreamReader(path))
				{
					string text2;
					while ((text2 = streamReader.ReadLine()) != null)
					{
						text2 = text2.Trim();
						int num = text2.IndexOf('=');
						if (num > 8 && text2.Substring(0, num) == key)
						{
							string text3 = text2.Substring(num + 1).Trim(new char[]
							{
								'"'
							});
							bool flag = false;
							if (text3.StartsWith("$HOME/"))
							{
								flag = true;
								text3 = text3.Substring(6);
							}
							else if (!text3.StartsWith("/"))
							{
								flag = true;
							}
							return (!flag) ? text3 : Path.Combine(home_dir, text3);
						}
					}
				}
			}
			catch (FileNotFoundException)
			{
			}
			return Path.Combine(home_dir, fallback);
		}

		// Token: 0x06001118 RID: 4376 RVA: 0x00045AC8 File Offset: 0x00043CC8
		internal static string InternalGetFolderPath(Environment.SpecialFolder folder)
		{
			string text = Environment.internalGetHome();
			string text2 = Environment.internalGetEnvironmentVariable("XDG_DATA_HOME");
			if (text2 == null || text2 == string.Empty)
			{
				text2 = Path.Combine(text, ".local");
				text2 = Path.Combine(text2, "share");
			}
			string text3 = Environment.internalGetEnvironmentVariable("XDG_CONFIG_HOME");
			if (text3 == null || text3 == string.Empty)
			{
				text3 = Path.Combine(text, ".config");
			}
			switch (folder)
			{
			case Environment.SpecialFolder.Desktop:
			case Environment.SpecialFolder.DesktopDirectory:
				return Environment.ReadXdgUserDir(text3, text, "XDG_DESKTOP_DIR", "Desktop");
			case Environment.SpecialFolder.Programs:
			case Environment.SpecialFolder.Favorites:
			case Environment.SpecialFolder.Startup:
			case Environment.SpecialFolder.Recent:
			case Environment.SpecialFolder.SendTo:
			case Environment.SpecialFolder.StartMenu:
			case Environment.SpecialFolder.Templates:
			case Environment.SpecialFolder.InternetCache:
			case Environment.SpecialFolder.Cookies:
			case Environment.SpecialFolder.History:
			case Environment.SpecialFolder.System:
			case Environment.SpecialFolder.ProgramFiles:
			case Environment.SpecialFolder.CommonProgramFiles:
				return string.Empty;
			case Environment.SpecialFolder.MyDocuments:
				return text;
			case Environment.SpecialFolder.MyMusic:
				return Environment.ReadXdgUserDir(text3, text, "XDG_MUSIC_DIR", "Music");
			case Environment.SpecialFolder.MyComputer:
				return string.Empty;
			case Environment.SpecialFolder.ApplicationData:
				return text3;
			case Environment.SpecialFolder.LocalApplicationData:
				return text2;
			case Environment.SpecialFolder.CommonApplicationData:
				return "/usr/share";
			case Environment.SpecialFolder.MyPictures:
				return Environment.ReadXdgUserDir(text3, text, "XDG_PICTURES_DIR", "Pictures");
			}
			throw new ArgumentException("Invalid SpecialFolder");
		}

		// Token: 0x06001119 RID: 4377 RVA: 0x00045C58 File Offset: 0x00043E58
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Unrestricted=\"true\"/>\n</PermissionSet>\n")]
		public static string[] GetLogicalDrives()
		{
			return Environment.GetLogicalDrivesInternal();
		}

		// Token: 0x0600111A RID: 4378
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void internalBroadcastSettingChange();

		// Token: 0x0600111B RID: 4379 RVA: 0x00045C60 File Offset: 0x00043E60
		public static string GetEnvironmentVariable(string variable, EnvironmentVariableTarget target)
		{
			switch (target)
			{
			case EnvironmentVariableTarget.Process:
				return Environment.GetEnvironmentVariable(variable);
			case EnvironmentVariableTarget.User:
				break;
			case EnvironmentVariableTarget.Machine:
				new EnvironmentPermission(PermissionState.Unrestricted).Demand();
				if (!Environment.IsRunningOnWindows)
				{
					return null;
				}
				using (RegistryKey registryKey = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment"))
				{
					object value = registryKey.GetValue(variable);
					return (value != null) ? value.ToString() : null;
				}
				break;
			default:
				goto IL_D7;
			}
			new EnvironmentPermission(PermissionState.Unrestricted).Demand();
			if (!Environment.IsRunningOnWindows)
			{
				return null;
			}
			using (RegistryKey registryKey2 = Registry.CurrentUser.OpenSubKey("Environment", false))
			{
				object value2 = registryKey2.GetValue(variable);
				return (value2 != null) ? value2.ToString() : null;
			}
			IL_D7:
			throw new ArgumentException("target");
		}

		// Token: 0x0600111C RID: 4380 RVA: 0x00045D88 File Offset: 0x00043F88
		public static IDictionary GetEnvironmentVariables(EnvironmentVariableTarget target)
		{
			IDictionary dictionary = new Hashtable();
			switch (target)
			{
			case EnvironmentVariableTarget.Process:
				dictionary = Environment.GetEnvironmentVariables();
				break;
			case EnvironmentVariableTarget.User:
				new EnvironmentPermission(PermissionState.Unrestricted).Demand();
				if (Environment.IsRunningOnWindows)
				{
					using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Environment"))
					{
						string[] valueNames = registryKey.GetValueNames();
						foreach (string text in valueNames)
						{
							dictionary.Add(text, registryKey.GetValue(text));
						}
					}
				}
				break;
			case EnvironmentVariableTarget.Machine:
				new EnvironmentPermission(PermissionState.Unrestricted).Demand();
				if (Environment.IsRunningOnWindows)
				{
					using (RegistryKey registryKey2 = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment"))
					{
						string[] valueNames2 = registryKey2.GetValueNames();
						foreach (string text2 in valueNames2)
						{
							dictionary.Add(text2, registryKey2.GetValue(text2));
						}
					}
				}
				break;
			default:
				throw new ArgumentException("target");
			}
			return dictionary;
		}

		// Token: 0x0600111D RID: 4381 RVA: 0x00045EF4 File Offset: 0x000440F4
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Unrestricted=\"true\"/>\n</PermissionSet>\n")]
		public static void SetEnvironmentVariable(string variable, string value)
		{
			Environment.SetEnvironmentVariable(variable, value, EnvironmentVariableTarget.Process);
		}

		// Token: 0x0600111E RID: 4382 RVA: 0x00045F00 File Offset: 0x00044100
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Unrestricted=\"true\"/>\n</PermissionSet>\n")]
		public static void SetEnvironmentVariable(string variable, string value, EnvironmentVariableTarget target)
		{
			if (variable == null)
			{
				throw new ArgumentNullException("variable");
			}
			if (variable == string.Empty)
			{
				throw new ArgumentException("String cannot be of zero length.", "variable");
			}
			if (variable.IndexOf('=') != -1)
			{
				throw new ArgumentException("Environment variable name cannot contain an equal character.", "variable");
			}
			if (variable[0] == '\0')
			{
				throw new ArgumentException("The first char in the string is the null character.", "variable");
			}
			switch (target)
			{
			case EnvironmentVariableTarget.Process:
				Environment.InternalSetEnvironmentVariable(variable, value);
				break;
			case EnvironmentVariableTarget.User:
				if (!Environment.IsRunningOnWindows)
				{
					return;
				}
				using (RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("Environment", true))
				{
					if (string.IsNullOrEmpty(value))
					{
						registryKey.DeleteValue(variable, false);
					}
					else
					{
						registryKey.SetValue(variable, value);
					}
					Environment.internalBroadcastSettingChange();
				}
				break;
			case EnvironmentVariableTarget.Machine:
				if (!Environment.IsRunningOnWindows)
				{
					return;
				}
				using (RegistryKey registryKey2 = Registry.LocalMachine.OpenSubKey("SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment", true))
				{
					if (string.IsNullOrEmpty(value))
					{
						registryKey2.DeleteValue(variable, false);
					}
					else
					{
						registryKey2.SetValue(variable, value);
					}
					Environment.internalBroadcastSettingChange();
				}
				break;
			default:
				throw new ArgumentException("target");
			}
		}

		// Token: 0x0600111F RID: 4383
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalSetEnvironmentVariable(string variable, string value);

		// Token: 0x06001120 RID: 4384 RVA: 0x0004608C File Offset: 0x0004428C
		[MonoTODO("Not implemented")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static void FailFast(string message)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x06001121 RID: 4385
		public static extern int ProcessorCount { [PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.EnvironmentPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Read=\"NUMBER_OF_PROCESSORS\"/>\n</PermissionSet>\n")] [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x06001122 RID: 4386 RVA: 0x00046094 File Offset: 0x00044294
		internal static bool IsRunningOnWindows
		{
			get
			{
				return Environment.Platform < PlatformID.Unix;
			}
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x06001123 RID: 4387 RVA: 0x000460A0 File Offset: 0x000442A0
		private static string GacPath
		{
			get
			{
				if (Environment.IsRunningOnWindows)
				{
					string fullName = new DirectoryInfo(Path.GetDirectoryName(typeof(int).Assembly.Location)).Parent.Parent.FullName;
					return Path.Combine(Path.Combine(fullName, "mono"), "gac");
				}
				return Path.Combine(Path.Combine(Environment.internalGetGacPath(), "mono"), "gac");
			}
		}

		// Token: 0x06001124 RID: 4388
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string internalGetGacPath();

		// Token: 0x06001125 RID: 4389
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetLogicalDrivesInternal();

		// Token: 0x06001126 RID: 4390
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern string[] GetEnvironmentVariableNames();

		// Token: 0x06001127 RID: 4391
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string GetMachineConfigPath();

		// Token: 0x06001128 RID: 4392
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string internalGetHome();

		// Token: 0x040004DF RID: 1247
		private const int mono_corlib_version = 82;

		// Token: 0x040004E0 RID: 1248
		private static OperatingSystem os;

		// Token: 0x02000133 RID: 307
		[ComVisible(true)]
		public enum SpecialFolder
		{
			// Token: 0x040004E2 RID: 1250
			MyDocuments = 5,
			// Token: 0x040004E3 RID: 1251
			Desktop = 0,
			// Token: 0x040004E4 RID: 1252
			MyComputer = 17,
			// Token: 0x040004E5 RID: 1253
			Programs = 2,
			// Token: 0x040004E6 RID: 1254
			Personal = 5,
			// Token: 0x040004E7 RID: 1255
			Favorites,
			// Token: 0x040004E8 RID: 1256
			Startup,
			// Token: 0x040004E9 RID: 1257
			Recent,
			// Token: 0x040004EA RID: 1258
			SendTo,
			// Token: 0x040004EB RID: 1259
			StartMenu = 11,
			// Token: 0x040004EC RID: 1260
			MyMusic = 13,
			// Token: 0x040004ED RID: 1261
			DesktopDirectory = 16,
			// Token: 0x040004EE RID: 1262
			Templates = 21,
			// Token: 0x040004EF RID: 1263
			ApplicationData = 26,
			// Token: 0x040004F0 RID: 1264
			LocalApplicationData = 28,
			// Token: 0x040004F1 RID: 1265
			InternetCache = 32,
			// Token: 0x040004F2 RID: 1266
			Cookies,
			// Token: 0x040004F3 RID: 1267
			History,
			// Token: 0x040004F4 RID: 1268
			CommonApplicationData,
			// Token: 0x040004F5 RID: 1269
			System = 37,
			// Token: 0x040004F6 RID: 1270
			ProgramFiles,
			// Token: 0x040004F7 RID: 1271
			MyPictures,
			// Token: 0x040004F8 RID: 1272
			CommonProgramFiles = 43
		}
	}
}
