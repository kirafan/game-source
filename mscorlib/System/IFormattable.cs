﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000007 RID: 7
	[ComVisible(true)]
	public interface IFormattable
	{
		// Token: 0x0600006F RID: 111
		string ToString(string format, IFormatProvider formatProvider);
	}
}
