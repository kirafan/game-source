﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000174 RID: 372
	[ComVisible(true)]
	[Serializable]
	public abstract class StringComparer : IComparer<string>, IEqualityComparer<string>, IComparer, IEqualityComparer
	{
		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x0600138D RID: 5005 RVA: 0x0004DF14 File Offset: 0x0004C114
		public static StringComparer CurrentCulture
		{
			get
			{
				return new CultureAwareComparer(CultureInfo.CurrentCulture, false);
			}
		}

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x0600138E RID: 5006 RVA: 0x0004DF24 File Offset: 0x0004C124
		public static StringComparer CurrentCultureIgnoreCase
		{
			get
			{
				return new CultureAwareComparer(CultureInfo.CurrentCulture, true);
			}
		}

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x0600138F RID: 5007 RVA: 0x0004DF34 File Offset: 0x0004C134
		public static StringComparer InvariantCulture
		{
			get
			{
				return StringComparer.invariantCulture;
			}
		}

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x06001390 RID: 5008 RVA: 0x0004DF3C File Offset: 0x0004C13C
		public static StringComparer InvariantCultureIgnoreCase
		{
			get
			{
				return StringComparer.invariantCultureIgnoreCase;
			}
		}

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x06001391 RID: 5009 RVA: 0x0004DF44 File Offset: 0x0004C144
		public static StringComparer Ordinal
		{
			get
			{
				return StringComparer.ordinal;
			}
		}

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x06001392 RID: 5010 RVA: 0x0004DF4C File Offset: 0x0004C14C
		public static StringComparer OrdinalIgnoreCase
		{
			get
			{
				return StringComparer.ordinalIgnoreCase;
			}
		}

		// Token: 0x06001393 RID: 5011 RVA: 0x0004DF54 File Offset: 0x0004C154
		public static StringComparer Create(CultureInfo culture, bool ignoreCase)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("culture");
			}
			return new CultureAwareComparer(culture, ignoreCase);
		}

		// Token: 0x06001394 RID: 5012 RVA: 0x0004DF70 File Offset: 0x0004C170
		public int Compare(object x, object y)
		{
			if (x == y)
			{
				return 0;
			}
			if (x == null)
			{
				return -1;
			}
			if (y == null)
			{
				return 1;
			}
			string text = x as string;
			if (text != null)
			{
				string text2 = y as string;
				if (text2 != null)
				{
					return this.Compare(text, text2);
				}
			}
			IComparable comparable = x as IComparable;
			if (comparable == null)
			{
				throw new ArgumentException();
			}
			return comparable.CompareTo(y);
		}

		// Token: 0x06001395 RID: 5013 RVA: 0x0004DFD4 File Offset: 0x0004C1D4
		public bool Equals(object x, object y)
		{
			if (x == y)
			{
				return true;
			}
			if (x == null || y == null)
			{
				return false;
			}
			string text = x as string;
			if (text != null)
			{
				string text2 = y as string;
				if (text2 != null)
				{
					return this.Equals(text, text2);
				}
			}
			return x.Equals(y);
		}

		// Token: 0x06001396 RID: 5014 RVA: 0x0004E024 File Offset: 0x0004C224
		public int GetHashCode(object obj)
		{
			if (obj == null)
			{
				throw new ArgumentNullException("obj");
			}
			string text = obj as string;
			return (text != null) ? this.GetHashCode(text) : obj.GetHashCode();
		}

		// Token: 0x06001397 RID: 5015
		public abstract int Compare(string x, string y);

		// Token: 0x06001398 RID: 5016
		public abstract bool Equals(string x, string y);

		// Token: 0x06001399 RID: 5017
		public abstract int GetHashCode(string obj);

		// Token: 0x040005AC RID: 1452
		private static StringComparer invariantCultureIgnoreCase = new CultureAwareComparer(CultureInfo.InvariantCulture, true);

		// Token: 0x040005AD RID: 1453
		private static StringComparer invariantCulture = new CultureAwareComparer(CultureInfo.InvariantCulture, false);

		// Token: 0x040005AE RID: 1454
		private static StringComparer ordinalIgnoreCase = new OrdinalComparer(true);

		// Token: 0x040005AF RID: 1455
		private static StringComparer ordinal = new OrdinalComparer(false);
	}
}
