﻿using System;

namespace System
{
	// Token: 0x02000196 RID: 406
	internal struct BRECORD
	{
		// Token: 0x04000821 RID: 2081
		private IntPtr pvRecord;

		// Token: 0x04000822 RID: 2082
		private IntPtr pRecInfo;
	}
}
