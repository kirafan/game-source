﻿using System;

namespace System
{
	// Token: 0x02000187 RID: 391
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true)]
	internal class MonoTODOAttribute : Attribute
	{
		// Token: 0x06001454 RID: 5204 RVA: 0x00051F98 File Offset: 0x00050198
		public MonoTODOAttribute()
		{
		}

		// Token: 0x06001455 RID: 5205 RVA: 0x00051FA0 File Offset: 0x000501A0
		public MonoTODOAttribute(string comment)
		{
			this.comment = comment;
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06001456 RID: 5206 RVA: 0x00051FB0 File Offset: 0x000501B0
		public string Comment
		{
			get
			{
				return this.comment;
			}
		}

		// Token: 0x040007EA RID: 2026
		private string comment;
	}
}
