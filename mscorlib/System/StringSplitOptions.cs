﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000173 RID: 371
	[Flags]
	[ComVisible(false)]
	public enum StringSplitOptions
	{
		// Token: 0x040005AA RID: 1450
		None = 0,
		// Token: 0x040005AB RID: 1451
		RemoveEmptyEntries = 1
	}
}
