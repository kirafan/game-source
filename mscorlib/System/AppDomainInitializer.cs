﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020006E7 RID: 1767
	// (Invoke) Token: 0x06004390 RID: 17296
	[ComVisible(true)]
	[Serializable]
	public delegate void AppDomainInitializer(string[] args);
}
