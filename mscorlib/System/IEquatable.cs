﻿using System;

namespace System
{
	// Token: 0x0200000E RID: 14
	public interface IEquatable<T>
	{
		// Token: 0x0600008C RID: 140
		bool Equals(T other);
	}
}
