﻿using System;

namespace System.Text
{
	// Token: 0x02000672 RID: 1650
	[Serializable]
	public sealed class DecoderExceptionFallback : DecoderFallback
	{
		// Token: 0x17000BD6 RID: 3030
		// (get) Token: 0x06003EB0 RID: 16048 RVA: 0x000D76F0 File Offset: 0x000D58F0
		public override int MaxCharCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06003EB1 RID: 16049 RVA: 0x000D76F4 File Offset: 0x000D58F4
		public override DecoderFallbackBuffer CreateFallbackBuffer()
		{
			return new DecoderExceptionFallbackBuffer();
		}

		// Token: 0x06003EB2 RID: 16050 RVA: 0x000D76FC File Offset: 0x000D58FC
		public override bool Equals(object value)
		{
			return value is DecoderExceptionFallback;
		}

		// Token: 0x06003EB3 RID: 16051 RVA: 0x000D7708 File Offset: 0x000D5908
		public override int GetHashCode()
		{
			return 0;
		}
	}
}
