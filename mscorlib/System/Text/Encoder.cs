﻿using System;
using System.Runtime.InteropServices;

namespace System.Text
{
	// Token: 0x02000679 RID: 1657
	[ComVisible(true)]
	[Serializable]
	public abstract class Encoder
	{
		// Token: 0x17000BE2 RID: 3042
		// (get) Token: 0x06003EDA RID: 16090 RVA: 0x000D79D8 File Offset: 0x000D5BD8
		// (set) Token: 0x06003EDB RID: 16091 RVA: 0x000D79E0 File Offset: 0x000D5BE0
		[ComVisible(false)]
		public EncoderFallback Fallback
		{
			get
			{
				return this.fallback;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				this.fallback = value;
				this.fallback_buffer = null;
			}
		}

		// Token: 0x17000BE3 RID: 3043
		// (get) Token: 0x06003EDC RID: 16092 RVA: 0x000D79FC File Offset: 0x000D5BFC
		[ComVisible(false)]
		public EncoderFallbackBuffer FallbackBuffer
		{
			get
			{
				if (this.fallback_buffer == null)
				{
					this.fallback_buffer = this.Fallback.CreateFallbackBuffer();
				}
				return this.fallback_buffer;
			}
		}

		// Token: 0x06003EDD RID: 16093
		public abstract int GetByteCount(char[] chars, int index, int count, bool flush);

		// Token: 0x06003EDE RID: 16094
		public abstract int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex, bool flush);

		// Token: 0x06003EDF RID: 16095 RVA: 0x000D7A2C File Offset: 0x000D5C2C
		[ComVisible(false)]
		[CLSCompliant(false)]
		public unsafe virtual int GetByteCount(char* chars, int count, bool flush)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			char[] array = new char[count];
			Marshal.Copy((IntPtr)((void*)chars), array, 0, count);
			return this.GetByteCount(array, 0, count, flush);
		}

		// Token: 0x06003EE0 RID: 16096 RVA: 0x000D7A7C File Offset: 0x000D5C7C
		[CLSCompliant(false)]
		[ComVisible(false)]
		public unsafe virtual int GetBytes(char* chars, int charCount, byte* bytes, int byteCount, bool flush)
		{
			this.CheckArguments(chars, charCount, bytes, byteCount);
			char[] array = new char[charCount];
			Marshal.Copy((IntPtr)((void*)chars), array, 0, charCount);
			byte[] array2 = new byte[byteCount];
			Marshal.Copy((IntPtr)((void*)bytes), array2, 0, byteCount);
			return this.GetBytes(array, 0, charCount, array2, 0, flush);
		}

		// Token: 0x06003EE1 RID: 16097 RVA: 0x000D7AD0 File Offset: 0x000D5CD0
		[ComVisible(false)]
		public virtual void Reset()
		{
			if (this.fallback_buffer != null)
			{
				this.fallback_buffer.Reset();
			}
		}

		// Token: 0x06003EE2 RID: 16098 RVA: 0x000D7AE8 File Offset: 0x000D5CE8
		[ComVisible(false)]
		[CLSCompliant(false)]
		public unsafe virtual void Convert(char* chars, int charCount, byte* bytes, int byteCount, bool flush, out int charsUsed, out int bytesUsed, out bool completed)
		{
			this.CheckArguments(chars, charCount, bytes, byteCount);
			charsUsed = charCount;
			for (;;)
			{
				bytesUsed = this.GetByteCount(chars, charsUsed, flush);
				if (bytesUsed <= byteCount)
				{
					break;
				}
				flush = false;
				charsUsed >>= 1;
			}
			completed = (charsUsed == charCount);
			bytesUsed = this.GetBytes(chars, charsUsed, bytes, byteCount, flush);
		}

		// Token: 0x06003EE3 RID: 16099 RVA: 0x000D7B50 File Offset: 0x000D5D50
		[ComVisible(false)]
		public virtual void Convert(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex, int byteCount, bool flush, out int charsUsed, out int bytesUsed, out bool completed)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (charIndex < 0 || chars.Length <= charIndex)
			{
				throw new ArgumentOutOfRangeException("charIndex");
			}
			if (charCount < 0 || chars.Length < charIndex + charCount)
			{
				throw new ArgumentOutOfRangeException("charCount");
			}
			if (byteIndex < 0 || bytes.Length <= byteIndex)
			{
				throw new ArgumentOutOfRangeException("byteIndex");
			}
			if (byteCount < 0 || bytes.Length < byteIndex + byteCount)
			{
				throw new ArgumentOutOfRangeException("byteCount");
			}
			charsUsed = charCount;
			for (;;)
			{
				bytesUsed = this.GetByteCount(chars, charIndex, charsUsed, flush);
				if (bytesUsed <= byteCount)
				{
					break;
				}
				flush = false;
				charsUsed >>= 1;
			}
			completed = (charsUsed == charCount);
			bytesUsed = this.GetBytes(chars, charIndex, charsUsed, bytes, byteIndex, flush);
		}

		// Token: 0x06003EE4 RID: 16100 RVA: 0x000D7C48 File Offset: 0x000D5E48
		private unsafe void CheckArguments(char* chars, int charCount, byte* bytes, int byteCount)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (charCount < 0)
			{
				throw new ArgumentOutOfRangeException("charCount");
			}
			if (byteCount < 0)
			{
				throw new ArgumentOutOfRangeException("byteCount");
			}
		}

		// Token: 0x04001B40 RID: 6976
		private EncoderFallback fallback = new EncoderReplacementFallback();

		// Token: 0x04001B41 RID: 6977
		private EncoderFallbackBuffer fallback_buffer;
	}
}
