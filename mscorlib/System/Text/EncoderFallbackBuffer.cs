﻿using System;

namespace System.Text
{
	// Token: 0x0200067D RID: 1661
	public abstract class EncoderFallbackBuffer
	{
		// Token: 0x17000BEA RID: 3050
		// (get) Token: 0x06003EF8 RID: 16120
		public abstract int Remaining { get; }

		// Token: 0x06003EF9 RID: 16121
		public abstract bool Fallback(char charUnknown, int index);

		// Token: 0x06003EFA RID: 16122
		public abstract bool Fallback(char charUnknownHigh, char charUnknownLow, int index);

		// Token: 0x06003EFB RID: 16123
		public abstract char GetNextChar();

		// Token: 0x06003EFC RID: 16124
		public abstract bool MovePrevious();

		// Token: 0x06003EFD RID: 16125 RVA: 0x000D7D3C File Offset: 0x000D5F3C
		public virtual void Reset()
		{
			while (this.GetNextChar() != '\0')
			{
			}
		}
	}
}
