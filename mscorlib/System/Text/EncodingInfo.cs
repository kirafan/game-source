﻿using System;

namespace System.Text
{
	// Token: 0x02000684 RID: 1668
	[Serializable]
	public sealed class EncodingInfo
	{
		// Token: 0x06003F63 RID: 16227 RVA: 0x000D96B4 File Offset: 0x000D78B4
		internal EncodingInfo(int cp)
		{
			this.codepage = cp;
		}

		// Token: 0x17000C0B RID: 3083
		// (get) Token: 0x06003F64 RID: 16228 RVA: 0x000D96C4 File Offset: 0x000D78C4
		public int CodePage
		{
			get
			{
				return this.codepage;
			}
		}

		// Token: 0x17000C0C RID: 3084
		// (get) Token: 0x06003F65 RID: 16229 RVA: 0x000D96CC File Offset: 0x000D78CC
		[MonoTODO]
		public string DisplayName
		{
			get
			{
				return this.Name;
			}
		}

		// Token: 0x17000C0D RID: 3085
		// (get) Token: 0x06003F66 RID: 16230 RVA: 0x000D96D4 File Offset: 0x000D78D4
		public string Name
		{
			get
			{
				if (this.encoding == null)
				{
					this.encoding = this.GetEncoding();
				}
				return this.encoding.WebName;
			}
		}

		// Token: 0x06003F67 RID: 16231 RVA: 0x000D9704 File Offset: 0x000D7904
		public override bool Equals(object value)
		{
			EncodingInfo encodingInfo = value as EncodingInfo;
			return encodingInfo != null && encodingInfo.codepage == this.codepage;
		}

		// Token: 0x06003F68 RID: 16232 RVA: 0x000D9730 File Offset: 0x000D7930
		public override int GetHashCode()
		{
			return this.codepage;
		}

		// Token: 0x06003F69 RID: 16233 RVA: 0x000D9738 File Offset: 0x000D7938
		public Encoding GetEncoding()
		{
			return Encoding.GetEncoding(this.codepage);
		}

		// Token: 0x04001B6D RID: 7021
		private readonly int codepage;

		// Token: 0x04001B6E RID: 7022
		private Encoding encoding;
	}
}
