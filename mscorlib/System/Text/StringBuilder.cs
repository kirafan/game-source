﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Text
{
	// Token: 0x0200068A RID: 1674
	[MonoTODO("Serialization format not compatible with .NET")]
	[ComVisible(true)]
	[Serializable]
	public sealed class StringBuilder : ISerializable
	{
		// Token: 0x06003F8A RID: 16266 RVA: 0x000DA10C File Offset: 0x000D830C
		public StringBuilder(string value, int startIndex, int length, int capacity) : this(value, startIndex, length, capacity, int.MaxValue)
		{
		}

		// Token: 0x06003F8B RID: 16267 RVA: 0x000DA120 File Offset: 0x000D8320
		private StringBuilder(string value, int startIndex, int length, int capacity, int maxCapacity)
		{
			if (value == null)
			{
				value = string.Empty;
			}
			if (startIndex < 0)
			{
				throw new ArgumentOutOfRangeException("startIndex", startIndex, "StartIndex cannot be less than zero.");
			}
			if (length < 0)
			{
				throw new ArgumentOutOfRangeException("length", length, "Length cannot be less than zero.");
			}
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity", capacity, "capacity must be greater than zero.");
			}
			if (maxCapacity < 1)
			{
				throw new ArgumentOutOfRangeException("maxCapacity", "maxCapacity is less than one.");
			}
			if (capacity > maxCapacity)
			{
				throw new ArgumentOutOfRangeException("capacity", "Capacity exceeds maximum capacity.");
			}
			if (startIndex > value.Length - length)
			{
				throw new ArgumentOutOfRangeException("startIndex", startIndex, "StartIndex and length must refer to a location within the string.");
			}
			if (capacity == 0)
			{
				if (maxCapacity > 16)
				{
					capacity = 16;
				}
				else
				{
					this._str = (this._cached_str = string.Empty);
				}
			}
			this._maxCapacity = maxCapacity;
			if (this._str == null)
			{
				this._str = string.InternalAllocateStr((length <= capacity) ? capacity : length);
			}
			if (length > 0)
			{
				string.CharCopy(this._str, 0, value, startIndex, length);
			}
			this._length = length;
		}

		// Token: 0x06003F8C RID: 16268 RVA: 0x000DA268 File Offset: 0x000D8468
		public StringBuilder() : this(null)
		{
		}

		// Token: 0x06003F8D RID: 16269 RVA: 0x000DA274 File Offset: 0x000D8474
		public StringBuilder(int capacity) : this(string.Empty, 0, 0, capacity)
		{
		}

		// Token: 0x06003F8E RID: 16270 RVA: 0x000DA284 File Offset: 0x000D8484
		public StringBuilder(int capacity, int maxCapacity) : this(string.Empty, 0, 0, capacity, maxCapacity)
		{
		}

		// Token: 0x06003F8F RID: 16271 RVA: 0x000DA298 File Offset: 0x000D8498
		public StringBuilder(string value)
		{
			if (value == null)
			{
				value = string.Empty;
			}
			this._length = value.Length;
			this._str = (this._cached_str = value);
			this._maxCapacity = int.MaxValue;
		}

		// Token: 0x06003F90 RID: 16272 RVA: 0x000DA2E0 File Offset: 0x000D84E0
		public StringBuilder(string value, int capacity) : this((value != null) ? value : string.Empty, 0, (value != null) ? value.Length : 0, capacity)
		{
		}

		// Token: 0x06003F91 RID: 16273 RVA: 0x000DA318 File Offset: 0x000D8518
		private StringBuilder(SerializationInfo info, StreamingContext context)
		{
			string text = info.GetString("m_StringValue");
			if (text == null)
			{
				text = string.Empty;
			}
			this._length = text.Length;
			this._str = (this._cached_str = text);
			this._maxCapacity = info.GetInt32("m_MaxCapacity");
			if (this._maxCapacity < 0)
			{
				this._maxCapacity = int.MaxValue;
			}
			this.Capacity = info.GetInt32("Capacity");
		}

		// Token: 0x06003F92 RID: 16274 RVA: 0x000DA398 File Offset: 0x000D8598
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("m_MaxCapacity", this._maxCapacity);
			info.AddValue("Capacity", this.Capacity);
			info.AddValue("m_StringValue", this.ToString());
			info.AddValue("m_currentThread", 0);
		}

		// Token: 0x17000C17 RID: 3095
		// (get) Token: 0x06003F93 RID: 16275 RVA: 0x000DA3E4 File Offset: 0x000D85E4
		public int MaxCapacity
		{
			get
			{
				return this._maxCapacity;
			}
		}

		// Token: 0x17000C18 RID: 3096
		// (get) Token: 0x06003F94 RID: 16276 RVA: 0x000DA3EC File Offset: 0x000D85EC
		// (set) Token: 0x06003F95 RID: 16277 RVA: 0x000DA418 File Offset: 0x000D8618
		public int Capacity
		{
			get
			{
				if (this._str.Length == 0)
				{
					return Math.Min(this._maxCapacity, 16);
				}
				return this._str.Length;
			}
			set
			{
				if (value < this._length)
				{
					throw new ArgumentException("Capacity must be larger than length");
				}
				if (value > this._maxCapacity)
				{
					throw new ArgumentOutOfRangeException("value", "Should be less than or equal to MaxCapacity");
				}
				this.InternalEnsureCapacity(value);
			}
		}

		// Token: 0x17000C19 RID: 3097
		// (get) Token: 0x06003F96 RID: 16278 RVA: 0x000DA460 File Offset: 0x000D8660
		// (set) Token: 0x06003F97 RID: 16279 RVA: 0x000DA468 File Offset: 0x000D8668
		public int Length
		{
			get
			{
				return this._length;
			}
			set
			{
				if (value < 0 || value > this._maxCapacity)
				{
					throw new ArgumentOutOfRangeException();
				}
				if (value == this._length)
				{
					return;
				}
				if (value < this._length)
				{
					this.InternalEnsureCapacity(value);
					this._length = value;
				}
				else
				{
					this.Append('\0', value - this._length);
				}
			}
		}

		// Token: 0x17000C1A RID: 3098
		[IndexerName("Chars")]
		public char this[int index]
		{
			get
			{
				if (index >= this._length || index < 0)
				{
					throw new IndexOutOfRangeException();
				}
				return this._str[index];
			}
			set
			{
				if (index >= this._length || index < 0)
				{
					throw new IndexOutOfRangeException();
				}
				if (this._cached_str != null)
				{
					this.InternalEnsureCapacity(this._length);
				}
				this._str.InternalSetChar(index, value);
			}
		}

		// Token: 0x06003F9A RID: 16282 RVA: 0x000DA534 File Offset: 0x000D8734
		public override string ToString()
		{
			if (this._length == 0)
			{
				return string.Empty;
			}
			if (this._cached_str != null)
			{
				return this._cached_str;
			}
			if (this._length < this._str.Length >> 1)
			{
				this._cached_str = this._str.SubstringUnchecked(0, this._length);
				return this._cached_str;
			}
			this._cached_str = this._str;
			this._str.InternalSetLength(this._length);
			return this._str;
		}

		// Token: 0x06003F9B RID: 16283 RVA: 0x000DA5C0 File Offset: 0x000D87C0
		public string ToString(int startIndex, int length)
		{
			if (startIndex < 0 || length < 0 || startIndex > this._length - length)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (startIndex == 0 && length == this._length)
			{
				return this.ToString();
			}
			return this._str.SubstringUnchecked(startIndex, length);
		}

		// Token: 0x06003F9C RID: 16284 RVA: 0x000DA618 File Offset: 0x000D8818
		public int EnsureCapacity(int capacity)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("Capacity must be greater than 0.");
			}
			if (capacity <= this._str.Length)
			{
				return this._str.Length;
			}
			this.InternalEnsureCapacity(capacity);
			return this._str.Length;
		}

		// Token: 0x06003F9D RID: 16285 RVA: 0x000DA668 File Offset: 0x000D8868
		public bool Equals(StringBuilder sb)
		{
			return sb != null && (this._length == sb.Length && this._str == sb._str);
		}

		// Token: 0x06003F9E RID: 16286 RVA: 0x000DA6A8 File Offset: 0x000D88A8
		public StringBuilder Remove(int startIndex, int length)
		{
			if (startIndex < 0 || length < 0 || startIndex > this._length - length)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (this._cached_str != null)
			{
				this.InternalEnsureCapacity(this._length);
			}
			if (this._length - (startIndex + length) > 0)
			{
				string.CharCopy(this._str, startIndex, this._str, startIndex + length, this._length - (startIndex + length));
			}
			this._length -= length;
			return this;
		}

		// Token: 0x06003F9F RID: 16287 RVA: 0x000DA72C File Offset: 0x000D892C
		public StringBuilder Replace(char oldChar, char newChar)
		{
			return this.Replace(oldChar, newChar, 0, this._length);
		}

		// Token: 0x06003FA0 RID: 16288 RVA: 0x000DA740 File Offset: 0x000D8940
		public StringBuilder Replace(char oldChar, char newChar, int startIndex, int count)
		{
			if (startIndex > this._length - count || startIndex < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (this._cached_str != null)
			{
				this.InternalEnsureCapacity(this._str.Length);
			}
			for (int i = startIndex; i < startIndex + count; i++)
			{
				if (this._str[i] == oldChar)
				{
					this._str.InternalSetChar(i, newChar);
				}
			}
			return this;
		}

		// Token: 0x06003FA1 RID: 16289 RVA: 0x000DA7C4 File Offset: 0x000D89C4
		public StringBuilder Replace(string oldValue, string newValue)
		{
			return this.Replace(oldValue, newValue, 0, this._length);
		}

		// Token: 0x06003FA2 RID: 16290 RVA: 0x000DA7D8 File Offset: 0x000D89D8
		public StringBuilder Replace(string oldValue, string newValue, int startIndex, int count)
		{
			if (oldValue == null)
			{
				throw new ArgumentNullException("The old value cannot be null.");
			}
			if (startIndex < 0 || count < 0 || startIndex > this._length - count)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (oldValue.Length == 0)
			{
				throw new ArgumentException("The old value cannot be zero length.");
			}
			string text = this._str.Substring(startIndex, count);
			string text2 = text.Replace(oldValue, newValue);
			if (text2 == text)
			{
				return this;
			}
			this.InternalEnsureCapacity(text2.Length + (this._length - count));
			if (text2.Length < count)
			{
				string.CharCopy(this._str, startIndex + text2.Length, this._str, startIndex + count, this._length - startIndex - count);
			}
			else if (text2.Length > count)
			{
				string.CharCopyReverse(this._str, startIndex + text2.Length, this._str, startIndex + count, this._length - startIndex - count);
			}
			string.CharCopy(this._str, startIndex, text2, 0, text2.Length);
			this._length = text2.Length + (this._length - count);
			return this;
		}

		// Token: 0x06003FA3 RID: 16291 RVA: 0x000DA904 File Offset: 0x000D8B04
		public StringBuilder Append(char[] value)
		{
			if (value == null)
			{
				return this;
			}
			int num = this._length + value.Length;
			if (this._cached_str != null || this._str.Length < num)
			{
				this.InternalEnsureCapacity(num);
			}
			string.CharCopy(this._str, this._length, value, 0, value.Length);
			this._length = num;
			return this;
		}

		// Token: 0x06003FA4 RID: 16292 RVA: 0x000DA968 File Offset: 0x000D8B68
		public StringBuilder Append(string value)
		{
			if (value == null)
			{
				return this;
			}
			if (this._length == 0 && value.Length < this._maxCapacity && value.Length > this._str.Length)
			{
				this._length = value.Length;
				this._cached_str = value;
				this._str = value;
				return this;
			}
			int num = this._length + value.Length;
			if (this._cached_str != null || this._str.Length < num)
			{
				this.InternalEnsureCapacity(num);
			}
			string.CharCopy(this._str, this._length, value, 0, value.Length);
			this._length = num;
			return this;
		}

		// Token: 0x06003FA5 RID: 16293 RVA: 0x000DAA20 File Offset: 0x000D8C20
		public StringBuilder Append(bool value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FA6 RID: 16294 RVA: 0x000DAA30 File Offset: 0x000D8C30
		public StringBuilder Append(byte value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FA7 RID: 16295 RVA: 0x000DAA40 File Offset: 0x000D8C40
		public StringBuilder Append(decimal value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FA8 RID: 16296 RVA: 0x000DAA50 File Offset: 0x000D8C50
		public StringBuilder Append(double value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FA9 RID: 16297 RVA: 0x000DAA60 File Offset: 0x000D8C60
		public StringBuilder Append(short value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FAA RID: 16298 RVA: 0x000DAA70 File Offset: 0x000D8C70
		public StringBuilder Append(int value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FAB RID: 16299 RVA: 0x000DAA80 File Offset: 0x000D8C80
		public StringBuilder Append(long value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FAC RID: 16300 RVA: 0x000DAA90 File Offset: 0x000D8C90
		public StringBuilder Append(object value)
		{
			if (value == null)
			{
				return this;
			}
			return this.Append(value.ToString());
		}

		// Token: 0x06003FAD RID: 16301 RVA: 0x000DAAA8 File Offset: 0x000D8CA8
		[CLSCompliant(false)]
		public StringBuilder Append(sbyte value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FAE RID: 16302 RVA: 0x000DAAB8 File Offset: 0x000D8CB8
		public StringBuilder Append(float value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FAF RID: 16303 RVA: 0x000DAAC8 File Offset: 0x000D8CC8
		[CLSCompliant(false)]
		public StringBuilder Append(ushort value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FB0 RID: 16304 RVA: 0x000DAAD8 File Offset: 0x000D8CD8
		[CLSCompliant(false)]
		public StringBuilder Append(uint value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FB1 RID: 16305 RVA: 0x000DAAE8 File Offset: 0x000D8CE8
		[CLSCompliant(false)]
		public StringBuilder Append(ulong value)
		{
			return this.Append(value.ToString());
		}

		// Token: 0x06003FB2 RID: 16306 RVA: 0x000DAAF8 File Offset: 0x000D8CF8
		public StringBuilder Append(char value)
		{
			int num = this._length + 1;
			if (this._cached_str != null || this._str.Length < num)
			{
				this.InternalEnsureCapacity(num);
			}
			this._str.InternalSetChar(this._length, value);
			this._length = num;
			return this;
		}

		// Token: 0x06003FB3 RID: 16307 RVA: 0x000DAB4C File Offset: 0x000D8D4C
		public StringBuilder Append(char value, int repeatCount)
		{
			if (repeatCount < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			this.InternalEnsureCapacity(this._length + repeatCount);
			for (int i = 0; i < repeatCount; i++)
			{
				this._str.InternalSetChar(this._length++, value);
			}
			return this;
		}

		// Token: 0x06003FB4 RID: 16308 RVA: 0x000DABA4 File Offset: 0x000D8DA4
		public StringBuilder Append(char[] value, int startIndex, int charCount)
		{
			if (value == null)
			{
				if (startIndex != 0 || charCount != 0)
				{
					throw new ArgumentNullException("value");
				}
				return this;
			}
			else
			{
				if (charCount < 0 || startIndex < 0 || startIndex > value.Length - charCount)
				{
					throw new ArgumentOutOfRangeException();
				}
				int num = this._length + charCount;
				this.InternalEnsureCapacity(num);
				string.CharCopy(this._str, this._length, value, startIndex, charCount);
				this._length = num;
				return this;
			}
		}

		// Token: 0x06003FB5 RID: 16309 RVA: 0x000DAC1C File Offset: 0x000D8E1C
		public StringBuilder Append(string value, int startIndex, int count)
		{
			if (value == null)
			{
				if (startIndex != 0 && count != 0)
				{
					throw new ArgumentNullException("value");
				}
				return this;
			}
			else
			{
				if (count < 0 || startIndex < 0 || startIndex > value.Length - count)
				{
					throw new ArgumentOutOfRangeException();
				}
				int num = this._length + count;
				if (this._cached_str != null || this._str.Length < num)
				{
					this.InternalEnsureCapacity(num);
				}
				string.CharCopy(this._str, this._length, value, startIndex, count);
				this._length = num;
				return this;
			}
		}

		// Token: 0x06003FB6 RID: 16310 RVA: 0x000DACB4 File Offset: 0x000D8EB4
		[ComVisible(false)]
		public StringBuilder AppendLine()
		{
			return this.Append(Environment.NewLine);
		}

		// Token: 0x06003FB7 RID: 16311 RVA: 0x000DACC4 File Offset: 0x000D8EC4
		[ComVisible(false)]
		public StringBuilder AppendLine(string value)
		{
			return this.Append(value).Append(Environment.NewLine);
		}

		// Token: 0x06003FB8 RID: 16312 RVA: 0x000DACD8 File Offset: 0x000D8ED8
		public StringBuilder AppendFormat(string format, params object[] args)
		{
			return this.AppendFormat(null, format, args);
		}

		// Token: 0x06003FB9 RID: 16313 RVA: 0x000DACE4 File Offset: 0x000D8EE4
		public StringBuilder AppendFormat(IFormatProvider provider, string format, params object[] args)
		{
			string.FormatHelper(this, provider, format, args);
			return this;
		}

		// Token: 0x06003FBA RID: 16314 RVA: 0x000DACF4 File Offset: 0x000D8EF4
		public StringBuilder AppendFormat(string format, object arg0)
		{
			return this.AppendFormat(null, format, new object[]
			{
				arg0
			});
		}

		// Token: 0x06003FBB RID: 16315 RVA: 0x000DAD08 File Offset: 0x000D8F08
		public StringBuilder AppendFormat(string format, object arg0, object arg1)
		{
			return this.AppendFormat(null, format, new object[]
			{
				arg0,
				arg1
			});
		}

		// Token: 0x06003FBC RID: 16316 RVA: 0x000DAD20 File Offset: 0x000D8F20
		public StringBuilder AppendFormat(string format, object arg0, object arg1, object arg2)
		{
			return this.AppendFormat(null, format, new object[]
			{
				arg0,
				arg1,
				arg2
			});
		}

		// Token: 0x06003FBD RID: 16317 RVA: 0x000DAD40 File Offset: 0x000D8F40
		public StringBuilder Insert(int index, char[] value)
		{
			return this.Insert(index, new string(value));
		}

		// Token: 0x06003FBE RID: 16318 RVA: 0x000DAD50 File Offset: 0x000D8F50
		public StringBuilder Insert(int index, string value)
		{
			if (index > this._length || index < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (value == null || value.Length == 0)
			{
				return this;
			}
			this.InternalEnsureCapacity(this._length + value.Length);
			string.CharCopyReverse(this._str, index + value.Length, this._str, index, this._length - index);
			string.CharCopy(this._str, index, value, 0, value.Length);
			this._length += value.Length;
			return this;
		}

		// Token: 0x06003FBF RID: 16319 RVA: 0x000DADE8 File Offset: 0x000D8FE8
		public StringBuilder Insert(int index, bool value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FC0 RID: 16320 RVA: 0x000DADF8 File Offset: 0x000D8FF8
		public StringBuilder Insert(int index, byte value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FC1 RID: 16321 RVA: 0x000DAE08 File Offset: 0x000D9008
		public StringBuilder Insert(int index, char value)
		{
			if (index > this._length || index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			this.InternalEnsureCapacity(this._length + 1);
			string.CharCopyReverse(this._str, index + 1, this._str, index, this._length - index);
			this._str.InternalSetChar(index, value);
			this._length++;
			return this;
		}

		// Token: 0x06003FC2 RID: 16322 RVA: 0x000DAE7C File Offset: 0x000D907C
		public StringBuilder Insert(int index, decimal value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FC3 RID: 16323 RVA: 0x000DAE8C File Offset: 0x000D908C
		public StringBuilder Insert(int index, double value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FC4 RID: 16324 RVA: 0x000DAE9C File Offset: 0x000D909C
		public StringBuilder Insert(int index, short value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FC5 RID: 16325 RVA: 0x000DAEAC File Offset: 0x000D90AC
		public StringBuilder Insert(int index, int value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FC6 RID: 16326 RVA: 0x000DAEBC File Offset: 0x000D90BC
		public StringBuilder Insert(int index, long value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FC7 RID: 16327 RVA: 0x000DAECC File Offset: 0x000D90CC
		public StringBuilder Insert(int index, object value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FC8 RID: 16328 RVA: 0x000DAEDC File Offset: 0x000D90DC
		[CLSCompliant(false)]
		public StringBuilder Insert(int index, sbyte value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FC9 RID: 16329 RVA: 0x000DAEEC File Offset: 0x000D90EC
		public StringBuilder Insert(int index, float value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FCA RID: 16330 RVA: 0x000DAEFC File Offset: 0x000D90FC
		[CLSCompliant(false)]
		public StringBuilder Insert(int index, ushort value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FCB RID: 16331 RVA: 0x000DAF0C File Offset: 0x000D910C
		[CLSCompliant(false)]
		public StringBuilder Insert(int index, uint value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FCC RID: 16332 RVA: 0x000DAF1C File Offset: 0x000D911C
		[CLSCompliant(false)]
		public StringBuilder Insert(int index, ulong value)
		{
			return this.Insert(index, value.ToString());
		}

		// Token: 0x06003FCD RID: 16333 RVA: 0x000DAF2C File Offset: 0x000D912C
		public StringBuilder Insert(int index, string value, int count)
		{
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (value != null && value != string.Empty)
			{
				for (int i = 0; i < count; i++)
				{
					this.Insert(index, value);
				}
			}
			return this;
		}

		// Token: 0x06003FCE RID: 16334 RVA: 0x000DAF78 File Offset: 0x000D9178
		public StringBuilder Insert(int index, char[] value, int startIndex, int charCount)
		{
			if (value == null)
			{
				if (startIndex == 0 && charCount == 0)
				{
					return this;
				}
				throw new ArgumentNullException("value");
			}
			else
			{
				if (charCount < 0 || startIndex < 0 || startIndex > value.Length - charCount)
				{
					throw new ArgumentOutOfRangeException();
				}
				return this.Insert(index, new string(value, startIndex, charCount));
			}
		}

		// Token: 0x06003FCF RID: 16335 RVA: 0x000DAFD8 File Offset: 0x000D91D8
		private void InternalEnsureCapacity(int size)
		{
			if (size > this._str.Length || this._cached_str == this._str)
			{
				int num = this._str.Length;
				if (size > num)
				{
					if (this._cached_str == this._str && num < 16)
					{
						num = 16;
					}
					num <<= 1;
					if (size > num)
					{
						num = size;
					}
					if (num >= 2147483647 || num < 0)
					{
						num = int.MaxValue;
					}
					if (num > this._maxCapacity && size <= this._maxCapacity)
					{
						num = this._maxCapacity;
					}
					if (num > this._maxCapacity)
					{
						throw new ArgumentOutOfRangeException("size", "capacity was less than the current size.");
					}
				}
				string text = string.InternalAllocateStr(num);
				if (this._length > 0)
				{
					string.CharCopy(text, 0, this._str, 0, this._length);
				}
				this._str = text;
			}
			this._cached_str = null;
		}

		// Token: 0x06003FD0 RID: 16336 RVA: 0x000DB0CC File Offset: 0x000D92CC
		[ComVisible(false)]
		public void CopyTo(int sourceIndex, char[] destination, int destinationIndex, int count)
		{
			if (destination == null)
			{
				throw new ArgumentNullException("destination");
			}
			if (this.Length - count < sourceIndex || destination.Length - count < destinationIndex || sourceIndex < 0 || destinationIndex < 0 || count < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			for (int i = 0; i < count; i++)
			{
				destination[destinationIndex + i] = this._str[sourceIndex + i];
			}
		}

		// Token: 0x04001B7E RID: 7038
		private const int constDefaultCapacity = 16;

		// Token: 0x04001B7F RID: 7039
		private int _length;

		// Token: 0x04001B80 RID: 7040
		private string _str;

		// Token: 0x04001B81 RID: 7041
		private string _cached_str;

		// Token: 0x04001B82 RID: 7042
		private int _maxCapacity;
	}
}
