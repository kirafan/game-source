﻿using System;
using System.Runtime.InteropServices;

namespace System.Text
{
	// Token: 0x02000671 RID: 1649
	[ComVisible(true)]
	[Serializable]
	public abstract class Decoder
	{
		// Token: 0x17000BD4 RID: 3028
		// (get) Token: 0x06003EA0 RID: 16032 RVA: 0x000D73A0 File Offset: 0x000D55A0
		// (set) Token: 0x06003EA1 RID: 16033 RVA: 0x000D73A8 File Offset: 0x000D55A8
		[ComVisible(false)]
		public DecoderFallback Fallback
		{
			get
			{
				return this.fallback;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException();
				}
				this.fallback = value;
				this.fallback_buffer = null;
			}
		}

		// Token: 0x17000BD5 RID: 3029
		// (get) Token: 0x06003EA2 RID: 16034 RVA: 0x000D73C4 File Offset: 0x000D55C4
		[ComVisible(false)]
		public DecoderFallbackBuffer FallbackBuffer
		{
			get
			{
				if (this.fallback_buffer == null)
				{
					this.fallback_buffer = this.fallback.CreateFallbackBuffer();
				}
				return this.fallback_buffer;
			}
		}

		// Token: 0x06003EA3 RID: 16035
		public abstract int GetCharCount(byte[] bytes, int index, int count);

		// Token: 0x06003EA4 RID: 16036
		public abstract int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex);

		// Token: 0x06003EA5 RID: 16037 RVA: 0x000D73F4 File Offset: 0x000D55F4
		[ComVisible(false)]
		public virtual int GetCharCount(byte[] bytes, int index, int count, bool flush)
		{
			if (flush)
			{
				this.Reset();
			}
			return this.GetCharCount(bytes, index, count);
		}

		// Token: 0x06003EA6 RID: 16038 RVA: 0x000D740C File Offset: 0x000D560C
		[ComVisible(false)]
		[CLSCompliant(false)]
		public unsafe virtual int GetCharCount(byte* bytes, int count, bool flush)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			byte[] array = new byte[count];
			Marshal.Copy((IntPtr)((void*)bytes), array, 0, count);
			return this.GetCharCount(array, 0, count, flush);
		}

		// Token: 0x06003EA7 RID: 16039 RVA: 0x000D745C File Offset: 0x000D565C
		public virtual int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex, bool flush)
		{
			this.CheckArguments(bytes, byteIndex, byteCount);
			this.CheckArguments(chars, charIndex);
			if (flush)
			{
				this.Reset();
			}
			return this.GetChars(bytes, byteIndex, byteCount, chars, charIndex);
		}

		// Token: 0x06003EA8 RID: 16040 RVA: 0x000D7498 File Offset: 0x000D5698
		[ComVisible(false)]
		[CLSCompliant(false)]
		public unsafe virtual int GetChars(byte* bytes, int byteCount, char* chars, int charCount, bool flush)
		{
			this.CheckArguments(chars, charCount, bytes, byteCount);
			char[] array = new char[charCount];
			Marshal.Copy((IntPtr)((void*)chars), array, 0, charCount);
			byte[] array2 = new byte[byteCount];
			Marshal.Copy((IntPtr)((void*)bytes), array2, 0, byteCount);
			return this.GetChars(array2, 0, byteCount, array, 0, flush);
		}

		// Token: 0x06003EA9 RID: 16041 RVA: 0x000D74EC File Offset: 0x000D56EC
		[ComVisible(false)]
		public virtual void Reset()
		{
			if (this.fallback_buffer != null)
			{
				this.fallback_buffer.Reset();
			}
		}

		// Token: 0x06003EAA RID: 16042 RVA: 0x000D7504 File Offset: 0x000D5704
		[CLSCompliant(false)]
		[ComVisible(false)]
		public unsafe virtual void Convert(byte* bytes, int byteCount, char* chars, int charCount, bool flush, out int bytesUsed, out int charsUsed, out bool completed)
		{
			this.CheckArguments(chars, charCount, bytes, byteCount);
			bytesUsed = byteCount;
			for (;;)
			{
				charsUsed = this.GetCharCount(bytes, bytesUsed, flush);
				if (charsUsed <= charCount)
				{
					break;
				}
				flush = false;
				bytesUsed >>= 1;
			}
			completed = (bytesUsed == byteCount);
			charsUsed = this.GetChars(bytes, bytesUsed, chars, charCount, flush);
		}

		// Token: 0x06003EAB RID: 16043 RVA: 0x000D756C File Offset: 0x000D576C
		[ComVisible(false)]
		public virtual void Convert(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex, int charCount, bool flush, out int bytesUsed, out int charsUsed, out bool completed)
		{
			this.CheckArguments(bytes, byteIndex, byteCount);
			this.CheckArguments(chars, charIndex);
			if (charCount < 0 || chars.Length < charIndex + charCount)
			{
				throw new ArgumentOutOfRangeException("charCount");
			}
			bytesUsed = byteCount;
			for (;;)
			{
				charsUsed = this.GetCharCount(bytes, byteIndex, bytesUsed, flush);
				if (charsUsed <= charCount)
				{
					break;
				}
				flush = false;
				bytesUsed >>= 1;
			}
			completed = (bytesUsed == byteCount);
			charsUsed = this.GetChars(bytes, byteIndex, bytesUsed, chars, charIndex, flush);
		}

		// Token: 0x06003EAC RID: 16044 RVA: 0x000D7600 File Offset: 0x000D5800
		private void CheckArguments(char[] chars, int charIndex)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (charIndex < 0 || chars.Length <= charIndex)
			{
				throw new ArgumentOutOfRangeException("charIndex");
			}
		}

		// Token: 0x06003EAD RID: 16045 RVA: 0x000D763C File Offset: 0x000D583C
		private void CheckArguments(byte[] bytes, int byteIndex, int byteCount)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (byteIndex < 0 || bytes.Length <= byteIndex)
			{
				throw new ArgumentOutOfRangeException("byteIndex");
			}
			if (byteCount < 0 || bytes.Length < byteIndex + byteCount)
			{
				throw new ArgumentOutOfRangeException("byteCount");
			}
		}

		// Token: 0x06003EAE RID: 16046 RVA: 0x000D7694 File Offset: 0x000D5894
		private unsafe void CheckArguments(char* chars, int charCount, byte* bytes, int byteCount)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (charCount < 0)
			{
				throw new ArgumentOutOfRangeException("charCount");
			}
			if (byteCount < 0)
			{
				throw new ArgumentOutOfRangeException("byteCount");
			}
		}

		// Token: 0x04001B34 RID: 6964
		private DecoderFallback fallback = new DecoderReplacementFallback();

		// Token: 0x04001B35 RID: 6965
		private DecoderFallbackBuffer fallback_buffer;
	}
}
