﻿using System;

namespace System.Text
{
	// Token: 0x0200067B RID: 1659
	public sealed class EncoderExceptionFallbackBuffer : EncoderFallbackBuffer
	{
		// Token: 0x17000BE5 RID: 3045
		// (get) Token: 0x06003EEB RID: 16107 RVA: 0x000D7CC8 File Offset: 0x000D5EC8
		public override int Remaining
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06003EEC RID: 16108 RVA: 0x000D7CCC File Offset: 0x000D5ECC
		public override bool Fallback(char charUnknown, int index)
		{
			throw new EncoderFallbackException(charUnknown, index);
		}

		// Token: 0x06003EED RID: 16109 RVA: 0x000D7CD8 File Offset: 0x000D5ED8
		public override bool Fallback(char charUnknownHigh, char charUnknownLow, int index)
		{
			throw new EncoderFallbackException(charUnknownHigh, charUnknownLow, index);
		}

		// Token: 0x06003EEE RID: 16110 RVA: 0x000D7CE4 File Offset: 0x000D5EE4
		public override char GetNextChar()
		{
			return '\0';
		}

		// Token: 0x06003EEF RID: 16111 RVA: 0x000D7CE8 File Offset: 0x000D5EE8
		public override bool MovePrevious()
		{
			return false;
		}
	}
}
