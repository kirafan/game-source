﻿using System;

namespace System.Text
{
	// Token: 0x0200067E RID: 1662
	[Serializable]
	public sealed class EncoderFallbackException : ArgumentException
	{
		// Token: 0x06003EFE RID: 16126 RVA: 0x000D7D50 File Offset: 0x000D5F50
		public EncoderFallbackException() : this(null)
		{
		}

		// Token: 0x06003EFF RID: 16127 RVA: 0x000D7D5C File Offset: 0x000D5F5C
		public EncoderFallbackException(string message)
		{
			this.index = -1;
			base..ctor(message);
		}

		// Token: 0x06003F00 RID: 16128 RVA: 0x000D7D6C File Offset: 0x000D5F6C
		public EncoderFallbackException(string message, Exception innerException)
		{
			this.index = -1;
			base..ctor(message, innerException);
		}

		// Token: 0x06003F01 RID: 16129 RVA: 0x000D7D80 File Offset: 0x000D5F80
		internal EncoderFallbackException(char charUnknown, int index)
		{
			this.index = -1;
			base..ctor(null);
			this.char_unknown = charUnknown;
			this.index = index;
		}

		// Token: 0x06003F02 RID: 16130 RVA: 0x000D7DA0 File Offset: 0x000D5FA0
		internal EncoderFallbackException(char charUnknownHigh, char charUnknownLow, int index)
		{
			this.index = -1;
			base..ctor(null);
			this.char_unknown_high = charUnknownHigh;
			this.char_unknown_low = charUnknownLow;
			this.index = index;
		}

		// Token: 0x17000BEB RID: 3051
		// (get) Token: 0x06003F03 RID: 16131 RVA: 0x000D7DC8 File Offset: 0x000D5FC8
		public char CharUnknown
		{
			get
			{
				return this.char_unknown;
			}
		}

		// Token: 0x17000BEC RID: 3052
		// (get) Token: 0x06003F04 RID: 16132 RVA: 0x000D7DD0 File Offset: 0x000D5FD0
		public char CharUnknownHigh
		{
			get
			{
				return this.char_unknown_high;
			}
		}

		// Token: 0x17000BED RID: 3053
		// (get) Token: 0x06003F05 RID: 16133 RVA: 0x000D7DD8 File Offset: 0x000D5FD8
		public char CharUnknownLow
		{
			get
			{
				return this.char_unknown_low;
			}
		}

		// Token: 0x17000BEE RID: 3054
		// (get) Token: 0x06003F06 RID: 16134 RVA: 0x000D7DE0 File Offset: 0x000D5FE0
		[MonoTODO]
		public int Index
		{
			get
			{
				return this.index;
			}
		}

		// Token: 0x06003F07 RID: 16135 RVA: 0x000D7DE8 File Offset: 0x000D5FE8
		[MonoTODO]
		public bool IsUnknownSurrogate()
		{
			throw new NotImplementedException();
		}

		// Token: 0x04001B45 RID: 6981
		private const string defaultMessage = "Failed to decode the input byte sequence to Unicode characters.";

		// Token: 0x04001B46 RID: 6982
		private char char_unknown;

		// Token: 0x04001B47 RID: 6983
		private char char_unknown_high;

		// Token: 0x04001B48 RID: 6984
		private char char_unknown_low;

		// Token: 0x04001B49 RID: 6985
		private int index;
	}
}
