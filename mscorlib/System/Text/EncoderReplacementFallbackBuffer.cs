﻿using System;

namespace System.Text
{
	// Token: 0x02000680 RID: 1664
	public sealed class EncoderReplacementFallbackBuffer : EncoderFallbackBuffer
	{
		// Token: 0x06003F0F RID: 16143 RVA: 0x000D7E7C File Offset: 0x000D607C
		public EncoderReplacementFallbackBuffer(EncoderReplacementFallback fallback)
		{
			if (fallback == null)
			{
				throw new ArgumentNullException("fallback");
			}
			this.replacement = fallback.DefaultString;
			this.current = 0;
		}

		// Token: 0x17000BF1 RID: 3057
		// (get) Token: 0x06003F10 RID: 16144 RVA: 0x000D7EB4 File Offset: 0x000D60B4
		public override int Remaining
		{
			get
			{
				return this.replacement.Length - this.current;
			}
		}

		// Token: 0x06003F11 RID: 16145 RVA: 0x000D7EC8 File Offset: 0x000D60C8
		public override bool Fallback(char charUnknown, int index)
		{
			return this.Fallback(index);
		}

		// Token: 0x06003F12 RID: 16146 RVA: 0x000D7ED4 File Offset: 0x000D60D4
		public override bool Fallback(char charUnknownHigh, char charUnknownLow, int index)
		{
			return this.Fallback(index);
		}

		// Token: 0x06003F13 RID: 16147 RVA: 0x000D7EE0 File Offset: 0x000D60E0
		private bool Fallback(int index)
		{
			if (this.fallback_assigned && this.Remaining != 0)
			{
				throw new ArgumentException("Reentrant Fallback method invocation occured. It might be because either this FallbackBuffer is incorrectly shared by multiple threads, invoked inside Encoding recursively, or Reset invocation is forgotten.");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			this.fallback_assigned = true;
			this.current = 0;
			return this.replacement.Length > 0;
		}

		// Token: 0x06003F14 RID: 16148 RVA: 0x000D7F3C File Offset: 0x000D613C
		public override char GetNextChar()
		{
			if (this.current >= this.replacement.Length)
			{
				return '\0';
			}
			return this.replacement[this.current++];
		}

		// Token: 0x06003F15 RID: 16149 RVA: 0x000D7F80 File Offset: 0x000D6180
		public override bool MovePrevious()
		{
			if (this.current == 0)
			{
				return false;
			}
			this.current--;
			return true;
		}

		// Token: 0x06003F16 RID: 16150 RVA: 0x000D7FA0 File Offset: 0x000D61A0
		public override void Reset()
		{
			this.current = 0;
		}

		// Token: 0x04001B4B RID: 6987
		private string replacement;

		// Token: 0x04001B4C RID: 6988
		private int current;

		// Token: 0x04001B4D RID: 6989
		private bool fallback_assigned;
	}
}
