﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Text
{
	// Token: 0x0200066E RID: 1646
	[MonoTODO("Serialization format not compatible with .NET")]
	[ComVisible(true)]
	[Serializable]
	public class ASCIIEncoding : Encoding
	{
		// Token: 0x06003E85 RID: 16005 RVA: 0x000D68D4 File Offset: 0x000D4AD4
		public ASCIIEncoding() : base(20127)
		{
			this.body_name = (this.header_name = (this.web_name = "us-ascii"));
			this.encoding_name = "US-ASCII";
			this.is_mail_news_display = true;
			this.is_mail_news_save = true;
		}

		// Token: 0x17000BD3 RID: 3027
		// (get) Token: 0x06003E86 RID: 16006 RVA: 0x000D6924 File Offset: 0x000D4B24
		[ComVisible(false)]
		public override bool IsSingleByte
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06003E87 RID: 16007 RVA: 0x000D6928 File Offset: 0x000D4B28
		public override int GetByteCount(char[] chars, int index, int count)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (index < 0 || index > chars.Length)
			{
				throw new ArgumentOutOfRangeException("index", Encoding._("ArgRange_Array"));
			}
			if (count < 0 || count > chars.Length - index)
			{
				throw new ArgumentOutOfRangeException("count", Encoding._("ArgRange_Array"));
			}
			return count;
		}

		// Token: 0x06003E88 RID: 16008 RVA: 0x000D6994 File Offset: 0x000D4B94
		public override int GetByteCount(string chars)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			return chars.Length;
		}

		// Token: 0x06003E89 RID: 16009 RVA: 0x000D69B0 File Offset: 0x000D4BB0
		public override int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex)
		{
			EncoderFallbackBuffer encoderFallbackBuffer = null;
			char[] array = null;
			return this.GetBytes(chars, charIndex, charCount, bytes, byteIndex, ref encoderFallbackBuffer, ref array);
		}

		// Token: 0x06003E8A RID: 16010 RVA: 0x000D69D4 File Offset: 0x000D4BD4
		private int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex, ref EncoderFallbackBuffer buffer, ref char[] fallback_chars)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (charIndex < 0 || charIndex > chars.Length)
			{
				throw new ArgumentOutOfRangeException("charIndex", Encoding._("ArgRange_Array"));
			}
			if (charCount < 0 || charCount > chars.Length - charIndex)
			{
				throw new ArgumentOutOfRangeException("charCount", Encoding._("ArgRange_Array"));
			}
			if (byteIndex < 0 || byteIndex > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("byteIndex", Encoding._("ArgRange_Array"));
			}
			if (bytes.Length - byteIndex < charCount)
			{
				throw new ArgumentException(Encoding._("Arg_InsufficientSpace"));
			}
			int num = charCount;
			while (num-- > 0)
			{
				char c = chars[charIndex++];
				if (c < '\u0080')
				{
					bytes[byteIndex++] = (byte)c;
				}
				else
				{
					if (buffer == null)
					{
						buffer = base.EncoderFallback.CreateFallbackBuffer();
					}
					if (char.IsSurrogate(c) && num > 1 && char.IsSurrogate(chars[charIndex]))
					{
						buffer.Fallback(c, chars[charIndex], charIndex++ - 1);
					}
					else
					{
						buffer.Fallback(c, charIndex - 1);
					}
					if (fallback_chars == null || fallback_chars.Length < buffer.Remaining)
					{
						fallback_chars = new char[buffer.Remaining];
					}
					for (int i = 0; i < fallback_chars.Length; i++)
					{
						fallback_chars[i] = buffer.GetNextChar();
					}
					byteIndex += this.GetBytes(fallback_chars, 0, fallback_chars.Length, bytes, byteIndex, ref buffer, ref fallback_chars);
				}
			}
			return charCount;
		}

		// Token: 0x06003E8B RID: 16011 RVA: 0x000D6B94 File Offset: 0x000D4D94
		public override int GetBytes(string chars, int charIndex, int charCount, byte[] bytes, int byteIndex)
		{
			EncoderFallbackBuffer encoderFallbackBuffer = null;
			char[] array = null;
			return this.GetBytes(chars, charIndex, charCount, bytes, byteIndex, ref encoderFallbackBuffer, ref array);
		}

		// Token: 0x06003E8C RID: 16012 RVA: 0x000D6BB8 File Offset: 0x000D4DB8
		private int GetBytes(string chars, int charIndex, int charCount, byte[] bytes, int byteIndex, ref EncoderFallbackBuffer buffer, ref char[] fallback_chars)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (charIndex < 0 || charIndex > chars.Length)
			{
				throw new ArgumentOutOfRangeException("charIndex", Encoding._("ArgRange_StringIndex"));
			}
			if (charCount < 0 || charCount > chars.Length - charIndex)
			{
				throw new ArgumentOutOfRangeException("charCount", Encoding._("ArgRange_StringRange"));
			}
			if (byteIndex < 0 || byteIndex > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("byteIndex", Encoding._("ArgRange_Array"));
			}
			if (bytes.Length - byteIndex < charCount)
			{
				throw new ArgumentException(Encoding._("Arg_InsufficientSpace"));
			}
			int num = charCount;
			while (num-- > 0)
			{
				char c = chars[charIndex++];
				if (c < '\u0080')
				{
					bytes[byteIndex++] = (byte)c;
				}
				else
				{
					if (buffer == null)
					{
						buffer = base.EncoderFallback.CreateFallbackBuffer();
					}
					if (char.IsSurrogate(c) && num > 1 && char.IsSurrogate(chars[charIndex]))
					{
						buffer.Fallback(c, chars[charIndex], charIndex++ - 1);
					}
					else
					{
						buffer.Fallback(c, charIndex - 1);
					}
					if (fallback_chars == null || fallback_chars.Length < buffer.Remaining)
					{
						fallback_chars = new char[buffer.Remaining];
					}
					for (int i = 0; i < fallback_chars.Length; i++)
					{
						fallback_chars[i] = buffer.GetNextChar();
					}
					byteIndex += this.GetBytes(fallback_chars, 0, fallback_chars.Length, bytes, byteIndex, ref buffer, ref fallback_chars);
				}
			}
			return charCount;
		}

		// Token: 0x06003E8D RID: 16013 RVA: 0x000D6D88 File Offset: 0x000D4F88
		public override int GetCharCount(byte[] bytes, int index, int count)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (index < 0 || index > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("index", Encoding._("ArgRange_Array"));
			}
			if (count < 0 || count > bytes.Length - index)
			{
				throw new ArgumentOutOfRangeException("count", Encoding._("ArgRange_Array"));
			}
			return count;
		}

		// Token: 0x06003E8E RID: 16014 RVA: 0x000D6DF4 File Offset: 0x000D4FF4
		public override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
		{
			DecoderFallbackBuffer decoderFallbackBuffer = null;
			return this.GetChars(bytes, byteIndex, byteCount, chars, charIndex, ref decoderFallbackBuffer);
		}

		// Token: 0x06003E8F RID: 16015 RVA: 0x000D6E14 File Offset: 0x000D5014
		private int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex, ref DecoderFallbackBuffer buffer)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (byteIndex < 0 || byteIndex > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("byteIndex", Encoding._("ArgRange_Array"));
			}
			if (byteCount < 0 || byteCount > bytes.Length - byteIndex)
			{
				throw new ArgumentOutOfRangeException("byteCount", Encoding._("ArgRange_Array"));
			}
			if (charIndex < 0 || charIndex > chars.Length)
			{
				throw new ArgumentOutOfRangeException("charIndex", Encoding._("ArgRange_Array"));
			}
			if (chars.Length - charIndex < byteCount)
			{
				throw new ArgumentException(Encoding._("Arg_InsufficientSpace"));
			}
			int num = byteCount;
			while (num-- > 0)
			{
				char c = (char)bytes[byteIndex++];
				if (c < '\u0080')
				{
					chars[charIndex++] = c;
				}
				else
				{
					if (buffer == null)
					{
						buffer = base.DecoderFallback.CreateFallbackBuffer();
					}
					buffer.Fallback(bytes, byteIndex);
					while (buffer.Remaining > 0)
					{
						chars[charIndex++] = buffer.GetNextChar();
					}
				}
			}
			return byteCount;
		}

		// Token: 0x06003E90 RID: 16016 RVA: 0x000D6F54 File Offset: 0x000D5154
		public override int GetMaxByteCount(int charCount)
		{
			if (charCount < 0)
			{
				throw new ArgumentOutOfRangeException("charCount", Encoding._("ArgRange_NonNegative"));
			}
			return charCount;
		}

		// Token: 0x06003E91 RID: 16017 RVA: 0x000D6F74 File Offset: 0x000D5174
		public override int GetMaxCharCount(int byteCount)
		{
			if (byteCount < 0)
			{
				throw new ArgumentOutOfRangeException("byteCount", Encoding._("ArgRange_NonNegative"));
			}
			return byteCount;
		}

		// Token: 0x06003E92 RID: 16018 RVA: 0x000D6F94 File Offset: 0x000D5194
		public unsafe override string GetString(byte[] bytes, int byteIndex, int byteCount)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (byteIndex < 0 || byteIndex > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("byteIndex", Encoding._("ArgRange_Array"));
			}
			if (byteCount < 0 || byteCount > bytes.Length - byteIndex)
			{
				throw new ArgumentOutOfRangeException("byteCount", Encoding._("ArgRange_Array"));
			}
			if (byteCount == 0)
			{
				return string.Empty;
			}
			fixed (byte* ptr = ref (bytes != null && bytes.Length != 0) ? ref bytes[0] : ref *null)
			{
				string text = string.InternalAllocateStr(byteCount);
				fixed (string text2 = text)
				{
					fixed (char* ptr2 = text2 + RuntimeHelpers.OffsetToStringData / 2)
					{
						byte* ptr3 = ptr + byteIndex;
						byte* ptr4 = ptr3 + byteCount;
						char* ptr5 = ptr2;
						while (ptr3 < ptr4)
						{
							byte b = *(ptr3++);
							*(ptr5++) = ((b > 127) ? '?' : ((char)b));
						}
						text2 = null;
						return text;
					}
				}
			}
		}

		// Token: 0x06003E93 RID: 16019 RVA: 0x000D7080 File Offset: 0x000D5280
		[CLSCompliant(false)]
		[ComVisible(false)]
		public unsafe override int GetBytes(char* chars, int charCount, byte* bytes, int byteCount)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (charCount < 0)
			{
				throw new ArgumentOutOfRangeException("charCount");
			}
			if (byteCount < 0)
			{
				throw new ArgumentOutOfRangeException("byteCount");
			}
			if (byteCount < charCount)
			{
				throw new ArgumentException("bytecount is less than the number of bytes required", "byteCount");
			}
			for (int i = 0; i < charCount; i++)
			{
				char c = chars[i];
				bytes[i] = (byte)((c >= '\u0080') ? '?' : c);
			}
			return charCount;
		}

		// Token: 0x06003E94 RID: 16020 RVA: 0x000D7120 File Offset: 0x000D5320
		[ComVisible(false)]
		[CLSCompliant(false)]
		public unsafe override int GetChars(byte* bytes, int byteCount, char* chars, int charCount)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (charCount < 0)
			{
				throw new ArgumentOutOfRangeException("charCount");
			}
			if (byteCount < 0)
			{
				throw new ArgumentOutOfRangeException("byteCount");
			}
			if (charCount < byteCount)
			{
				throw new ArgumentException("charcount is less than the number of bytes required", "charCount");
			}
			for (int i = 0; i < byteCount; i++)
			{
				byte b = bytes[i];
				chars[i] = ((b <= 127) ? ((char)b) : '?');
			}
			return byteCount;
		}

		// Token: 0x06003E95 RID: 16021 RVA: 0x000D71BC File Offset: 0x000D53BC
		[CLSCompliant(false)]
		[ComVisible(false)]
		public unsafe override int GetCharCount(byte* bytes, int count)
		{
			return count;
		}

		// Token: 0x06003E96 RID: 16022 RVA: 0x000D71C0 File Offset: 0x000D53C0
		[ComVisible(false)]
		[CLSCompliant(false)]
		public unsafe override int GetByteCount(char* chars, int count)
		{
			return count;
		}

		// Token: 0x06003E97 RID: 16023 RVA: 0x000D71C4 File Offset: 0x000D53C4
		[ComVisible(false)]
		[MonoTODO("we have simple override to match method signature.")]
		public override Decoder GetDecoder()
		{
			return base.GetDecoder();
		}

		// Token: 0x06003E98 RID: 16024 RVA: 0x000D71CC File Offset: 0x000D53CC
		[ComVisible(false)]
		[MonoTODO("we have simple override to match method signature.")]
		public override Encoder GetEncoder()
		{
			return base.GetEncoder();
		}

		// Token: 0x04001B2C RID: 6956
		internal const int ASCII_CODE_PAGE = 20127;
	}
}
