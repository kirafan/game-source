﻿using System;

namespace System.Text
{
	// Token: 0x02000673 RID: 1651
	public sealed class DecoderExceptionFallbackBuffer : DecoderFallbackBuffer
	{
		// Token: 0x17000BD7 RID: 3031
		// (get) Token: 0x06003EB5 RID: 16053 RVA: 0x000D7714 File Offset: 0x000D5914
		public override int Remaining
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06003EB6 RID: 16054 RVA: 0x000D7718 File Offset: 0x000D5918
		public override bool Fallback(byte[] bytesUnknown, int index)
		{
			throw new DecoderFallbackException(null, bytesUnknown, index);
		}

		// Token: 0x06003EB7 RID: 16055 RVA: 0x000D7724 File Offset: 0x000D5924
		public override char GetNextChar()
		{
			return '\0';
		}

		// Token: 0x06003EB8 RID: 16056 RVA: 0x000D7728 File Offset: 0x000D5928
		public override bool MovePrevious()
		{
			return false;
		}
	}
}
