﻿using System;

namespace System.Text
{
	// Token: 0x0200067A RID: 1658
	[Serializable]
	public sealed class EncoderExceptionFallback : EncoderFallback
	{
		// Token: 0x17000BE4 RID: 3044
		// (get) Token: 0x06003EE6 RID: 16102 RVA: 0x000D7CA4 File Offset: 0x000D5EA4
		public override int MaxCharCount
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06003EE7 RID: 16103 RVA: 0x000D7CA8 File Offset: 0x000D5EA8
		public override EncoderFallbackBuffer CreateFallbackBuffer()
		{
			return new EncoderExceptionFallbackBuffer();
		}

		// Token: 0x06003EE8 RID: 16104 RVA: 0x000D7CB0 File Offset: 0x000D5EB0
		public override bool Equals(object value)
		{
			return value is EncoderExceptionFallback;
		}

		// Token: 0x06003EE9 RID: 16105 RVA: 0x000D7CBC File Offset: 0x000D5EBC
		public override int GetHashCode()
		{
			return 0;
		}
	}
}
