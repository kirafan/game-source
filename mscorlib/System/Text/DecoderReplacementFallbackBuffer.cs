﻿using System;

namespace System.Text
{
	// Token: 0x02000678 RID: 1656
	public sealed class DecoderReplacementFallbackBuffer : DecoderFallbackBuffer
	{
		// Token: 0x06003ED3 RID: 16083 RVA: 0x000D786C File Offset: 0x000D5A6C
		public DecoderReplacementFallbackBuffer(DecoderReplacementFallback fallback)
		{
			if (fallback == null)
			{
				throw new ArgumentNullException("fallback");
			}
			this.replacement = fallback.DefaultString;
			this.current = 0;
		}

		// Token: 0x17000BE1 RID: 3041
		// (get) Token: 0x06003ED4 RID: 16084 RVA: 0x000D78A4 File Offset: 0x000D5AA4
		public override int Remaining
		{
			get
			{
				return (!this.fallback_assigned) ? 0 : (this.replacement.Length - this.current);
			}
		}

		// Token: 0x06003ED5 RID: 16085 RVA: 0x000D78CC File Offset: 0x000D5ACC
		public override bool Fallback(byte[] bytesUnknown, int index)
		{
			if (bytesUnknown == null)
			{
				throw new ArgumentNullException("bytesUnknown");
			}
			if (this.fallback_assigned && this.Remaining != 0)
			{
				throw new ArgumentException("Reentrant Fallback method invocation occured. It might be because either this FallbackBuffer is incorrectly shared by multiple threads, invoked inside Encoding recursively, or Reset invocation is forgotten.");
			}
			if (index < 0 || bytesUnknown.Length < index)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			this.fallback_assigned = true;
			this.current = 0;
			return this.replacement.Length > 0;
		}

		// Token: 0x06003ED6 RID: 16086 RVA: 0x000D7944 File Offset: 0x000D5B44
		public override char GetNextChar()
		{
			if (!this.fallback_assigned)
			{
				return '\0';
			}
			if (this.current >= this.replacement.Length)
			{
				return '\0';
			}
			return this.replacement[this.current++];
		}

		// Token: 0x06003ED7 RID: 16087 RVA: 0x000D7994 File Offset: 0x000D5B94
		public override bool MovePrevious()
		{
			if (this.current == 0)
			{
				return false;
			}
			this.current--;
			return true;
		}

		// Token: 0x06003ED8 RID: 16088 RVA: 0x000D79B4 File Offset: 0x000D5BB4
		public override void Reset()
		{
			this.fallback_assigned = false;
			this.current = 0;
		}

		// Token: 0x04001B3D RID: 6973
		private bool fallback_assigned;

		// Token: 0x04001B3E RID: 6974
		private int current;

		// Token: 0x04001B3F RID: 6975
		private string replacement;
	}
}
