﻿using System;

namespace System.Text
{
	// Token: 0x0200067C RID: 1660
	[Serializable]
	public abstract class EncoderFallback
	{
		// Token: 0x17000BE6 RID: 3046
		// (get) Token: 0x06003EF2 RID: 16114 RVA: 0x000D7D1C File Offset: 0x000D5F1C
		public static EncoderFallback ExceptionFallback
		{
			get
			{
				return EncoderFallback.exception_fallback;
			}
		}

		// Token: 0x17000BE7 RID: 3047
		// (get) Token: 0x06003EF3 RID: 16115
		public abstract int MaxCharCount { get; }

		// Token: 0x17000BE8 RID: 3048
		// (get) Token: 0x06003EF4 RID: 16116 RVA: 0x000D7D24 File Offset: 0x000D5F24
		public static EncoderFallback ReplacementFallback
		{
			get
			{
				return EncoderFallback.replacement_fallback;
			}
		}

		// Token: 0x17000BE9 RID: 3049
		// (get) Token: 0x06003EF5 RID: 16117 RVA: 0x000D7D2C File Offset: 0x000D5F2C
		internal static EncoderFallback StandardSafeFallback
		{
			get
			{
				return EncoderFallback.standard_safe_fallback;
			}
		}

		// Token: 0x06003EF6 RID: 16118
		public abstract EncoderFallbackBuffer CreateFallbackBuffer();

		// Token: 0x04001B42 RID: 6978
		private static EncoderFallback exception_fallback = new EncoderExceptionFallback();

		// Token: 0x04001B43 RID: 6979
		private static EncoderFallback replacement_fallback = new EncoderReplacementFallback();

		// Token: 0x04001B44 RID: 6980
		private static EncoderFallback standard_safe_fallback = new EncoderReplacementFallback("�");
	}
}
