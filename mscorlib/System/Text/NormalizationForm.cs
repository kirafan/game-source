﻿using System;
using System.Runtime.InteropServices;

namespace System.Text
{
	// Token: 0x02000689 RID: 1673
	[ComVisible(true)]
	public enum NormalizationForm
	{
		// Token: 0x04001B7A RID: 7034
		FormC = 1,
		// Token: 0x04001B7B RID: 7035
		FormD,
		// Token: 0x04001B7C RID: 7036
		FormKC = 5,
		// Token: 0x04001B7D RID: 7037
		FormKD
	}
}
