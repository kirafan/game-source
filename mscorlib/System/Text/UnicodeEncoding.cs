﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Text
{
	// Token: 0x0200068C RID: 1676
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with .NET")]
	[Serializable]
	public class UnicodeEncoding : Encoding
	{
		// Token: 0x06003FD4 RID: 16340 RVA: 0x000DB1C0 File Offset: 0x000D93C0
		public UnicodeEncoding() : this(false, true)
		{
			this.bigEndian = false;
			this.byteOrderMark = true;
		}

		// Token: 0x06003FD5 RID: 16341 RVA: 0x000DB1D8 File Offset: 0x000D93D8
		public UnicodeEncoding(bool bigEndian, bool byteOrderMark) : this(bigEndian, byteOrderMark, false)
		{
		}

		// Token: 0x06003FD6 RID: 16342 RVA: 0x000DB1E4 File Offset: 0x000D93E4
		public UnicodeEncoding(bool bigEndian, bool byteOrderMark, bool throwOnInvalidBytes) : base((!bigEndian) ? 1200 : 1201)
		{
			if (throwOnInvalidBytes)
			{
				base.SetFallbackInternal(null, new DecoderExceptionFallback());
			}
			else
			{
				base.SetFallbackInternal(null, new DecoderReplacementFallback("�"));
			}
			this.bigEndian = bigEndian;
			this.byteOrderMark = byteOrderMark;
			if (bigEndian)
			{
				this.body_name = "unicodeFFFE";
				this.encoding_name = "Unicode (Big-Endian)";
				this.header_name = "unicodeFFFE";
				this.is_browser_save = false;
				this.web_name = "unicodeFFFE";
			}
			else
			{
				this.body_name = "utf-16";
				this.encoding_name = "Unicode";
				this.header_name = "utf-16";
				this.is_browser_save = true;
				this.web_name = "utf-16";
			}
			this.windows_code_page = 1200;
		}

		// Token: 0x06003FD7 RID: 16343 RVA: 0x000DB2C0 File Offset: 0x000D94C0
		public override int GetByteCount(char[] chars, int index, int count)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (index < 0 || index > chars.Length)
			{
				throw new ArgumentOutOfRangeException("index", Encoding._("ArgRange_Array"));
			}
			if (count < 0 || count > chars.Length - index)
			{
				throw new ArgumentOutOfRangeException("count", Encoding._("ArgRange_Array"));
			}
			return count * 2;
		}

		// Token: 0x06003FD8 RID: 16344 RVA: 0x000DB330 File Offset: 0x000D9530
		public override int GetByteCount(string s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			return s.Length * 2;
		}

		// Token: 0x06003FD9 RID: 16345 RVA: 0x000DB34C File Offset: 0x000D954C
		[CLSCompliant(false)]
		[ComVisible(false)]
		public unsafe override int GetByteCount(char* chars, int count)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			return count * 2;
		}

		// Token: 0x06003FDA RID: 16346 RVA: 0x000DB380 File Offset: 0x000D9580
		public unsafe override int GetBytes(char[] chars, int charIndex, int charCount, byte[] bytes, int byteIndex)
		{
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (charIndex < 0 || charIndex > chars.Length)
			{
				throw new ArgumentOutOfRangeException("charIndex", Encoding._("ArgRange_Array"));
			}
			if (charCount < 0 || charCount > chars.Length - charIndex)
			{
				throw new ArgumentOutOfRangeException("charCount", Encoding._("ArgRange_Array"));
			}
			if (byteIndex < 0 || byteIndex > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("byteIndex", Encoding._("ArgRange_Array"));
			}
			if (charCount == 0)
			{
				return 0;
			}
			int byteCount = bytes.Length - byteIndex;
			if (bytes.Length == 0)
			{
				bytes = new byte[1];
			}
			fixed (char* ptr = ref (chars != null && chars.Length != 0) ? ref chars[0] : ref *null)
			{
				fixed (byte* ptr2 = ref (bytes != null && bytes.Length != 0) ? ref bytes[0] : ref *null)
				{
					return this.GetBytesInternal(ptr + charIndex, charCount, ptr2 + byteIndex, byteCount);
				}
			}
		}

		// Token: 0x06003FDB RID: 16347 RVA: 0x000DB494 File Offset: 0x000D9694
		public unsafe override int GetBytes(string s, int charIndex, int charCount, byte[] bytes, int byteIndex)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (charIndex < 0 || charIndex > s.Length)
			{
				throw new ArgumentOutOfRangeException("charIndex", Encoding._("ArgRange_StringIndex"));
			}
			if (charCount < 0 || charCount > s.Length - charIndex)
			{
				throw new ArgumentOutOfRangeException("charCount", Encoding._("ArgRange_StringRange"));
			}
			if (byteIndex < 0 || byteIndex > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("byteIndex", Encoding._("ArgRange_Array"));
			}
			if (charCount == 0)
			{
				return 0;
			}
			int byteCount = bytes.Length - byteIndex;
			if (bytes.Length == 0)
			{
				bytes = new byte[1];
			}
			fixed (char* ptr = s + RuntimeHelpers.OffsetToStringData / 2)
			{
				fixed (byte* ptr2 = ref (bytes != null && bytes.Length != 0) ? ref bytes[0] : ref *null)
				{
					return this.GetBytesInternal(ptr + charIndex, charCount, ptr2 + byteIndex, byteCount);
				}
			}
		}

		// Token: 0x06003FDC RID: 16348 RVA: 0x000DB59C File Offset: 0x000D979C
		[CLSCompliant(false)]
		[ComVisible(false)]
		public unsafe override int GetBytes(char* chars, int charCount, byte* bytes, int byteCount)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (charCount < 0)
			{
				throw new ArgumentOutOfRangeException("charCount");
			}
			if (byteCount < 0)
			{
				throw new ArgumentOutOfRangeException("byteCount");
			}
			return this.GetBytesInternal(chars, charCount, bytes, byteCount);
		}

		// Token: 0x06003FDD RID: 16349 RVA: 0x000DB5FC File Offset: 0x000D97FC
		private unsafe int GetBytesInternal(char* chars, int charCount, byte* bytes, int byteCount)
		{
			int num = charCount * 2;
			if (byteCount < num)
			{
				throw new ArgumentException(Encoding._("Arg_InsufficientSpace"));
			}
			UnicodeEncoding.CopyChars((byte*)chars, bytes, num, this.bigEndian);
			return num;
		}

		// Token: 0x06003FDE RID: 16350 RVA: 0x000DB634 File Offset: 0x000D9834
		public override int GetCharCount(byte[] bytes, int index, int count)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (index < 0 || index > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("index", Encoding._("ArgRange_Array"));
			}
			if (count < 0 || count > bytes.Length - index)
			{
				throw new ArgumentOutOfRangeException("count", Encoding._("ArgRange_Array"));
			}
			return count / 2;
		}

		// Token: 0x06003FDF RID: 16351 RVA: 0x000DB6A4 File Offset: 0x000D98A4
		[ComVisible(false)]
		[CLSCompliant(false)]
		public unsafe override int GetCharCount(byte* bytes, int count)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			return count / 2;
		}

		// Token: 0x06003FE0 RID: 16352 RVA: 0x000DB6D8 File Offset: 0x000D98D8
		public unsafe override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (byteIndex < 0 || byteIndex > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("byteIndex", Encoding._("ArgRange_Array"));
			}
			if (byteCount < 0 || byteCount > bytes.Length - byteIndex)
			{
				throw new ArgumentOutOfRangeException("byteCount", Encoding._("ArgRange_Array"));
			}
			if (charIndex < 0 || charIndex > chars.Length)
			{
				throw new ArgumentOutOfRangeException("charIndex", Encoding._("ArgRange_Array"));
			}
			if (byteCount == 0)
			{
				return 0;
			}
			int charCount = chars.Length - charIndex;
			if (chars.Length == 0)
			{
				chars = new char[1];
			}
			fixed (byte* ptr = ref (bytes != null && bytes.Length != 0) ? ref bytes[0] : ref *null)
			{
				fixed (char* ptr2 = ref (chars != null && chars.Length != 0) ? ref chars[0] : ref *null)
				{
					return this.GetCharsInternal(ptr + byteIndex, byteCount, ptr2 + charIndex, charCount);
				}
			}
		}

		// Token: 0x06003FE1 RID: 16353 RVA: 0x000DB7EC File Offset: 0x000D99EC
		[ComVisible(false)]
		[CLSCompliant(false)]
		public unsafe override int GetChars(byte* bytes, int byteCount, char* chars, int charCount)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (chars == null)
			{
				throw new ArgumentNullException("chars");
			}
			if (charCount < 0)
			{
				throw new ArgumentOutOfRangeException("charCount");
			}
			if (byteCount < 0)
			{
				throw new ArgumentOutOfRangeException("byteCount");
			}
			return this.GetCharsInternal(bytes, byteCount, chars, charCount);
		}

		// Token: 0x06003FE2 RID: 16354 RVA: 0x000DB84C File Offset: 0x000D9A4C
		[ComVisible(false)]
		public unsafe override string GetString(byte[] bytes, int index, int count)
		{
			if (bytes == null)
			{
				throw new ArgumentNullException("bytes");
			}
			if (index < 0 || index > bytes.Length)
			{
				throw new ArgumentOutOfRangeException("index", Encoding._("ArgRange_Array"));
			}
			if (count < 0 || count > bytes.Length - index)
			{
				throw new ArgumentOutOfRangeException("count", Encoding._("ArgRange_Array"));
			}
			if (count == 0)
			{
				return string.Empty;
			}
			int num = count / 2;
			string text = string.InternalAllocateStr(num);
			fixed (byte* ptr = ref (bytes != null && bytes.Length != 0) ? ref bytes[0] : ref *null)
			{
				fixed (string text2 = text)
				{
					fixed (char* chars = text2 + RuntimeHelpers.OffsetToStringData / 2)
					{
						this.GetCharsInternal(ptr + index, count, chars, num);
						text2 = null;
						ptr = null;
						return text;
					}
				}
			}
		}

		// Token: 0x06003FE3 RID: 16355 RVA: 0x000DB90C File Offset: 0x000D9B0C
		private unsafe int GetCharsInternal(byte* bytes, int byteCount, char* chars, int charCount)
		{
			int num = byteCount / 2;
			if (charCount < num)
			{
				throw new ArgumentException(Encoding._("Arg_InsufficientSpace"));
			}
			UnicodeEncoding.CopyChars(bytes, (byte*)chars, byteCount, this.bigEndian);
			return num;
		}

		// Token: 0x06003FE4 RID: 16356 RVA: 0x000DB944 File Offset: 0x000D9B44
		[ComVisible(false)]
		public override Encoder GetEncoder()
		{
			return base.GetEncoder();
		}

		// Token: 0x06003FE5 RID: 16357 RVA: 0x000DB94C File Offset: 0x000D9B4C
		public override int GetMaxByteCount(int charCount)
		{
			if (charCount < 0)
			{
				throw new ArgumentOutOfRangeException("charCount", Encoding._("ArgRange_NonNegative"));
			}
			return charCount * 2;
		}

		// Token: 0x06003FE6 RID: 16358 RVA: 0x000DB970 File Offset: 0x000D9B70
		public override int GetMaxCharCount(int byteCount)
		{
			if (byteCount < 0)
			{
				throw new ArgumentOutOfRangeException("byteCount", Encoding._("ArgRange_NonNegative"));
			}
			return byteCount / 2;
		}

		// Token: 0x06003FE7 RID: 16359 RVA: 0x000DB994 File Offset: 0x000D9B94
		public override Decoder GetDecoder()
		{
			return new UnicodeEncoding.UnicodeDecoder(this.bigEndian);
		}

		// Token: 0x06003FE8 RID: 16360 RVA: 0x000DB9A4 File Offset: 0x000D9BA4
		public override byte[] GetPreamble()
		{
			if (this.byteOrderMark)
			{
				byte[] array = new byte[2];
				if (this.bigEndian)
				{
					array[0] = 254;
					array[1] = byte.MaxValue;
				}
				else
				{
					array[0] = byte.MaxValue;
					array[1] = 254;
				}
				return array;
			}
			return new byte[0];
		}

		// Token: 0x06003FE9 RID: 16361 RVA: 0x000DB9FC File Offset: 0x000D9BFC
		public override bool Equals(object value)
		{
			UnicodeEncoding unicodeEncoding = value as UnicodeEncoding;
			return unicodeEncoding != null && (this.codePage == unicodeEncoding.codePage && this.bigEndian == unicodeEncoding.bigEndian) && this.byteOrderMark == unicodeEncoding.byteOrderMark;
		}

		// Token: 0x06003FEA RID: 16362 RVA: 0x000DBA4C File Offset: 0x000D9C4C
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x06003FEB RID: 16363 RVA: 0x000DBA54 File Offset: 0x000D9C54
		private unsafe static void CopyChars(byte* src, byte* dest, int count, bool bigEndian)
		{
			if (BitConverter.IsLittleEndian != bigEndian)
			{
				string.memcpy(dest, src, count & -2);
				return;
			}
			switch (count)
			{
			case 0:
				return;
			case 1:
				return;
			case 2:
				goto IL_220;
			case 3:
				goto IL_220;
			case 4:
				goto IL_1F1;
			case 5:
				goto IL_1F1;
			case 6:
				goto IL_1F1;
			case 7:
				goto IL_1F1;
			case 8:
				break;
			case 9:
				break;
			case 10:
				break;
			case 11:
				break;
			case 12:
				break;
			case 13:
				break;
			case 14:
				break;
			case 15:
				break;
			default:
				do
				{
					*dest = src[1];
					dest[1] = *src;
					dest[2] = src[3];
					dest[3] = src[2];
					dest[4] = src[5];
					dest[5] = src[4];
					dest[6] = src[7];
					dest[7] = src[6];
					dest[8] = src[9];
					dest[9] = src[8];
					dest[10] = src[11];
					dest[11] = src[10];
					dest[12] = src[13];
					dest[13] = src[12];
					dest[14] = src[15];
					dest[15] = src[14];
					dest += 16;
					src += 16;
					count -= 16;
				}
				while ((count & -16) != 0);
				switch (count)
				{
				case 0:
					return;
				case 1:
					return;
				case 2:
					goto IL_220;
				case 3:
					goto IL_220;
				case 4:
					goto IL_1F1;
				case 5:
					goto IL_1F1;
				case 6:
					goto IL_1F1;
				case 7:
					goto IL_1F1;
				}
				break;
			}
			*dest = src[1];
			dest[1] = *src;
			dest[2] = src[3];
			dest[3] = src[2];
			dest[4] = src[5];
			dest[5] = src[4];
			dest[6] = src[7];
			dest[7] = src[6];
			dest += 8;
			src += 8;
			if ((count & 4) == 0)
			{
				goto IL_217;
			}
			IL_1F1:
			*dest = src[1];
			dest[1] = *src;
			dest[2] = src[3];
			dest[3] = src[2];
			dest += 4;
			src += 4;
			IL_217:
			if ((count & 2) == 0)
			{
				return;
			}
			IL_220:
			*dest = src[1];
			dest[1] = *src;
		}

		// Token: 0x04001B85 RID: 7045
		internal const int UNICODE_CODE_PAGE = 1200;

		// Token: 0x04001B86 RID: 7046
		internal const int BIG_UNICODE_CODE_PAGE = 1201;

		// Token: 0x04001B87 RID: 7047
		public const int CharSize = 2;

		// Token: 0x04001B88 RID: 7048
		private bool bigEndian;

		// Token: 0x04001B89 RID: 7049
		private bool byteOrderMark;

		// Token: 0x0200068D RID: 1677
		private sealed class UnicodeDecoder : Decoder
		{
			// Token: 0x06003FEC RID: 16364 RVA: 0x000DBC90 File Offset: 0x000D9E90
			public UnicodeDecoder(bool bigEndian)
			{
				this.bigEndian = bigEndian;
				this.leftOverByte = -1;
			}

			// Token: 0x06003FED RID: 16365 RVA: 0x000DBCA8 File Offset: 0x000D9EA8
			public override int GetCharCount(byte[] bytes, int index, int count)
			{
				if (bytes == null)
				{
					throw new ArgumentNullException("bytes");
				}
				if (index < 0 || index > bytes.Length)
				{
					throw new ArgumentOutOfRangeException("index", Encoding._("ArgRange_Array"));
				}
				if (count < 0 || count > bytes.Length - index)
				{
					throw new ArgumentOutOfRangeException("count", Encoding._("ArgRange_Array"));
				}
				if (this.leftOverByte != -1)
				{
					return (count + 1) / 2;
				}
				return count / 2;
			}

			// Token: 0x06003FEE RID: 16366 RVA: 0x000DBD28 File Offset: 0x000D9F28
			public unsafe override int GetChars(byte[] bytes, int byteIndex, int byteCount, char[] chars, int charIndex)
			{
				if (bytes == null)
				{
					throw new ArgumentNullException("bytes");
				}
				if (chars == null)
				{
					throw new ArgumentNullException("chars");
				}
				if (byteIndex < 0 || byteIndex > bytes.Length)
				{
					throw new ArgumentOutOfRangeException("byteIndex", Encoding._("ArgRange_Array"));
				}
				if (byteCount < 0 || byteCount > bytes.Length - byteIndex)
				{
					throw new ArgumentOutOfRangeException("byteCount", Encoding._("ArgRange_Array"));
				}
				if (charIndex < 0 || charIndex > chars.Length)
				{
					throw new ArgumentOutOfRangeException("charIndex", Encoding._("ArgRange_Array"));
				}
				if (byteCount == 0)
				{
					return 0;
				}
				int num = this.leftOverByte;
				int num2;
				if (num != -1)
				{
					num2 = (byteCount + 1) / 2;
				}
				else
				{
					num2 = byteCount / 2;
				}
				if (chars.Length - charIndex < num2)
				{
					throw new ArgumentException(Encoding._("Arg_InsufficientSpace"));
				}
				if (num != -1)
				{
					if (this.bigEndian)
					{
						chars[charIndex] = (char)(num << 8 | (int)bytes[byteIndex]);
					}
					else
					{
						chars[charIndex] = (char)((int)bytes[byteIndex] << 8 | num);
					}
					charIndex++;
					byteIndex++;
					byteCount--;
				}
				if ((byteCount & -2) != 0)
				{
					fixed (byte* ptr = ref (bytes != null && bytes.Length != 0) ? ref bytes[0] : ref *null)
					{
						fixed (char* ptr2 = ref (chars != null && chars.Length != 0) ? ref chars[0] : ref *null)
						{
							UnicodeEncoding.CopyChars(ptr + byteIndex, (byte*)(ptr2 + charIndex), byteCount, this.bigEndian);
						}
					}
				}
				if ((byteCount & 1) == 0)
				{
					this.leftOverByte = -1;
				}
				else
				{
					this.leftOverByte = (int)bytes[byteCount + byteIndex - 1];
				}
				return num2;
			}

			// Token: 0x04001B8A RID: 7050
			private bool bigEndian;

			// Token: 0x04001B8B RID: 7051
			private int leftOverByte;
		}
	}
}
