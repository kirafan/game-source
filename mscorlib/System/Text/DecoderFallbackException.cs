﻿using System;

namespace System.Text
{
	// Token: 0x02000676 RID: 1654
	[Serializable]
	public sealed class DecoderFallbackException : ArgumentException
	{
		// Token: 0x06003EC6 RID: 16070 RVA: 0x000D7780 File Offset: 0x000D5980
		public DecoderFallbackException() : this(null)
		{
		}

		// Token: 0x06003EC7 RID: 16071 RVA: 0x000D778C File Offset: 0x000D598C
		public DecoderFallbackException(string message)
		{
			this.index = -1;
			base..ctor(message);
		}

		// Token: 0x06003EC8 RID: 16072 RVA: 0x000D779C File Offset: 0x000D599C
		public DecoderFallbackException(string message, Exception innerException)
		{
			this.index = -1;
			base..ctor(message, innerException);
		}

		// Token: 0x06003EC9 RID: 16073 RVA: 0x000D77B0 File Offset: 0x000D59B0
		public DecoderFallbackException(string message, byte[] bytesUnknown, int index)
		{
			this.index = -1;
			base..ctor(message);
			this.bytes_unknown = bytesUnknown;
			this.index = index;
		}

		// Token: 0x17000BDD RID: 3037
		// (get) Token: 0x06003ECA RID: 16074 RVA: 0x000D77D0 File Offset: 0x000D59D0
		[MonoTODO]
		public byte[] BytesUnknown
		{
			get
			{
				return this.bytes_unknown;
			}
		}

		// Token: 0x17000BDE RID: 3038
		// (get) Token: 0x06003ECB RID: 16075 RVA: 0x000D77D8 File Offset: 0x000D59D8
		[MonoTODO]
		public int Index
		{
			get
			{
				return this.index;
			}
		}

		// Token: 0x04001B39 RID: 6969
		private const string defaultMessage = "Failed to decode the input byte sequence to Unicode characters.";

		// Token: 0x04001B3A RID: 6970
		private byte[] bytes_unknown;

		// Token: 0x04001B3B RID: 6971
		private int index;
	}
}
