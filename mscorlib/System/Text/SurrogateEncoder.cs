﻿using System;
using System.Runtime.Serialization;

namespace System.Text
{
	// Token: 0x0200068B RID: 1675
	[Serializable]
	internal sealed class SurrogateEncoder : ISerializable, IObjectReference
	{
		// Token: 0x06003FD1 RID: 16337 RVA: 0x000DB148 File Offset: 0x000D9348
		private SurrogateEncoder(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.encoding = (Encoding)info.GetValue("m_encoding", typeof(Encoding));
		}

		// Token: 0x06003FD2 RID: 16338 RVA: 0x000DB184 File Offset: 0x000D9384
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new ArgumentException("This class cannot be serialized.");
		}

		// Token: 0x06003FD3 RID: 16339 RVA: 0x000DB190 File Offset: 0x000D9390
		public object GetRealObject(StreamingContext context)
		{
			if (this.realObject == null)
			{
				this.realObject = this.encoding.GetEncoder();
			}
			return this.realObject;
		}

		// Token: 0x04001B83 RID: 7043
		private Encoding encoding;

		// Token: 0x04001B84 RID: 7044
		private Encoder realObject;
	}
}
