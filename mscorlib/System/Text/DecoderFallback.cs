﻿using System;

namespace System.Text
{
	// Token: 0x02000674 RID: 1652
	[Serializable]
	public abstract class DecoderFallback
	{
		// Token: 0x17000BD8 RID: 3032
		// (get) Token: 0x06003EBB RID: 16059 RVA: 0x000D775C File Offset: 0x000D595C
		public static DecoderFallback ExceptionFallback
		{
			get
			{
				return DecoderFallback.exception_fallback;
			}
		}

		// Token: 0x17000BD9 RID: 3033
		// (get) Token: 0x06003EBC RID: 16060
		public abstract int MaxCharCount { get; }

		// Token: 0x17000BDA RID: 3034
		// (get) Token: 0x06003EBD RID: 16061 RVA: 0x000D7764 File Offset: 0x000D5964
		public static DecoderFallback ReplacementFallback
		{
			get
			{
				return DecoderFallback.replacement_fallback;
			}
		}

		// Token: 0x17000BDB RID: 3035
		// (get) Token: 0x06003EBE RID: 16062 RVA: 0x000D776C File Offset: 0x000D596C
		internal static DecoderFallback StandardSafeFallback
		{
			get
			{
				return DecoderFallback.standard_safe_fallback;
			}
		}

		// Token: 0x06003EBF RID: 16063
		public abstract DecoderFallbackBuffer CreateFallbackBuffer();

		// Token: 0x04001B36 RID: 6966
		private static DecoderFallback exception_fallback = new DecoderExceptionFallback();

		// Token: 0x04001B37 RID: 6967
		private static DecoderFallback replacement_fallback = new DecoderReplacementFallback();

		// Token: 0x04001B38 RID: 6968
		private static DecoderFallback standard_safe_fallback = new DecoderReplacementFallback("�");
	}
}
