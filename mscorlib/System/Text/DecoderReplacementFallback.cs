﻿using System;

namespace System.Text
{
	// Token: 0x02000677 RID: 1655
	[Serializable]
	public sealed class DecoderReplacementFallback : DecoderFallback
	{
		// Token: 0x06003ECC RID: 16076 RVA: 0x000D77E0 File Offset: 0x000D59E0
		public DecoderReplacementFallback() : this("?")
		{
		}

		// Token: 0x06003ECD RID: 16077 RVA: 0x000D77F0 File Offset: 0x000D59F0
		[MonoTODO]
		public DecoderReplacementFallback(string replacement)
		{
			if (replacement == null)
			{
				throw new ArgumentNullException();
			}
			this.replacement = replacement;
		}

		// Token: 0x17000BDF RID: 3039
		// (get) Token: 0x06003ECE RID: 16078 RVA: 0x000D780C File Offset: 0x000D5A0C
		public string DefaultString
		{
			get
			{
				return this.replacement;
			}
		}

		// Token: 0x17000BE0 RID: 3040
		// (get) Token: 0x06003ECF RID: 16079 RVA: 0x000D7814 File Offset: 0x000D5A14
		public override int MaxCharCount
		{
			get
			{
				return this.replacement.Length;
			}
		}

		// Token: 0x06003ED0 RID: 16080 RVA: 0x000D7824 File Offset: 0x000D5A24
		public override DecoderFallbackBuffer CreateFallbackBuffer()
		{
			return new DecoderReplacementFallbackBuffer(this);
		}

		// Token: 0x06003ED1 RID: 16081 RVA: 0x000D782C File Offset: 0x000D5A2C
		public override bool Equals(object value)
		{
			DecoderReplacementFallback decoderReplacementFallback = value as DecoderReplacementFallback;
			return decoderReplacementFallback != null && this.replacement == decoderReplacementFallback.replacement;
		}

		// Token: 0x06003ED2 RID: 16082 RVA: 0x000D785C File Offset: 0x000D5A5C
		public override int GetHashCode()
		{
			return this.replacement.GetHashCode();
		}

		// Token: 0x04001B3C RID: 6972
		private string replacement;
	}
}
