﻿using System;

namespace System.Text
{
	// Token: 0x02000675 RID: 1653
	public abstract class DecoderFallbackBuffer
	{
		// Token: 0x17000BDC RID: 3036
		// (get) Token: 0x06003EC1 RID: 16065
		public abstract int Remaining { get; }

		// Token: 0x06003EC2 RID: 16066
		public abstract bool Fallback(byte[] bytesUnknown, int index);

		// Token: 0x06003EC3 RID: 16067
		public abstract char GetNextChar();

		// Token: 0x06003EC4 RID: 16068
		public abstract bool MovePrevious();

		// Token: 0x06003EC5 RID: 16069 RVA: 0x000D777C File Offset: 0x000D597C
		public virtual void Reset()
		{
		}
	}
}
