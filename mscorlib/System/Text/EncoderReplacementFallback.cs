﻿using System;

namespace System.Text
{
	// Token: 0x0200067F RID: 1663
	[Serializable]
	public sealed class EncoderReplacementFallback : EncoderFallback
	{
		// Token: 0x06003F08 RID: 16136 RVA: 0x000D7DF0 File Offset: 0x000D5FF0
		public EncoderReplacementFallback() : this("?")
		{
		}

		// Token: 0x06003F09 RID: 16137 RVA: 0x000D7E00 File Offset: 0x000D6000
		[MonoTODO]
		public EncoderReplacementFallback(string replacement)
		{
			if (replacement == null)
			{
				throw new ArgumentNullException();
			}
			this.replacement = replacement;
		}

		// Token: 0x17000BEF RID: 3055
		// (get) Token: 0x06003F0A RID: 16138 RVA: 0x000D7E1C File Offset: 0x000D601C
		public string DefaultString
		{
			get
			{
				return this.replacement;
			}
		}

		// Token: 0x17000BF0 RID: 3056
		// (get) Token: 0x06003F0B RID: 16139 RVA: 0x000D7E24 File Offset: 0x000D6024
		public override int MaxCharCount
		{
			get
			{
				return this.replacement.Length;
			}
		}

		// Token: 0x06003F0C RID: 16140 RVA: 0x000D7E34 File Offset: 0x000D6034
		public override EncoderFallbackBuffer CreateFallbackBuffer()
		{
			return new EncoderReplacementFallbackBuffer(this);
		}

		// Token: 0x06003F0D RID: 16141 RVA: 0x000D7E3C File Offset: 0x000D603C
		public override bool Equals(object value)
		{
			EncoderReplacementFallback encoderReplacementFallback = value as EncoderReplacementFallback;
			return encoderReplacementFallback != null && this.replacement == encoderReplacementFallback.replacement;
		}

		// Token: 0x06003F0E RID: 16142 RVA: 0x000D7E6C File Offset: 0x000D606C
		public override int GetHashCode()
		{
			return this.replacement.GetHashCode();
		}

		// Token: 0x04001B4A RID: 6986
		private string replacement;
	}
}
