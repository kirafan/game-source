﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000139 RID: 313
	[ComVisible(true)]
	[Serializable]
	public class FormatException : SystemException
	{
		// Token: 0x06001134 RID: 4404 RVA: 0x000461C0 File Offset: 0x000443C0
		public FormatException() : base(Locale.GetText("Invalid format."))
		{
			base.HResult = -2146233033;
		}

		// Token: 0x06001135 RID: 4405 RVA: 0x000461E0 File Offset: 0x000443E0
		public FormatException(string message) : base(message)
		{
			base.HResult = -2146233033;
		}

		// Token: 0x06001136 RID: 4406 RVA: 0x000461F4 File Offset: 0x000443F4
		public FormatException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233033;
		}

		// Token: 0x06001137 RID: 4407 RVA: 0x0004620C File Offset: 0x0004440C
		protected FormatException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040004FF RID: 1279
		private const int Result = -2146233033;
	}
}
