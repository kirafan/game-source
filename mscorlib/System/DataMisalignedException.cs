﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200011D RID: 285
	[ComVisible(true)]
	[Serializable]
	public sealed class DataMisalignedException : SystemException
	{
		// Token: 0x06000FDD RID: 4061 RVA: 0x0003F94C File Offset: 0x0003DB4C
		public DataMisalignedException() : base(Locale.GetText("A datatype misalignment was detected in a load or store instruction."))
		{
			base.HResult = -2146233023;
		}

		// Token: 0x06000FDE RID: 4062 RVA: 0x0003F96C File Offset: 0x0003DB6C
		public DataMisalignedException(string message) : base(message)
		{
			base.HResult = -2146233023;
		}

		// Token: 0x06000FDF RID: 4063 RVA: 0x0003F980 File Offset: 0x0003DB80
		public DataMisalignedException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233023;
		}

		// Token: 0x04000493 RID: 1171
		private const int Result = -2146233023;
	}
}
