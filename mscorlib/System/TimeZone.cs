﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000182 RID: 386
	[ComVisible(true)]
	[Serializable]
	public abstract class TimeZone
	{
		// Token: 0x170002EF RID: 751
		// (get) Token: 0x0600143B RID: 5179 RVA: 0x0005187C File Offset: 0x0004FA7C
		public static TimeZone CurrentTimeZone
		{
			get
			{
				long now = DateTime.GetNow();
				object obj = TimeZone.tz_lock;
				TimeZone result;
				lock (obj)
				{
					if (TimeZone.currentTimeZone == null || now - TimeZone.timezone_check > 600000000L)
					{
						TimeZone.currentTimeZone = new CurrentSystemTimeZone(now);
						TimeZone.timezone_check = now;
					}
					result = TimeZone.currentTimeZone;
				}
				return result;
			}
		}

		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x0600143C RID: 5180
		public abstract string DaylightName { get; }

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x0600143D RID: 5181
		public abstract string StandardName { get; }

		// Token: 0x0600143E RID: 5182
		public abstract DaylightTime GetDaylightChanges(int year);

		// Token: 0x0600143F RID: 5183
		public abstract TimeSpan GetUtcOffset(DateTime time);

		// Token: 0x06001440 RID: 5184 RVA: 0x000518F8 File Offset: 0x0004FAF8
		public virtual bool IsDaylightSavingTime(DateTime time)
		{
			return TimeZone.IsDaylightSavingTime(time, this.GetDaylightChanges(time.Year));
		}

		// Token: 0x06001441 RID: 5185 RVA: 0x00051910 File Offset: 0x0004FB10
		public static bool IsDaylightSavingTime(DateTime time, DaylightTime daylightTimes)
		{
			if (daylightTimes == null)
			{
				throw new ArgumentNullException("daylightTimes");
			}
			if (daylightTimes.Start.Ticks == daylightTimes.End.Ticks)
			{
				return false;
			}
			if (daylightTimes.Start.Ticks < daylightTimes.End.Ticks)
			{
				if (daylightTimes.Start.Ticks < time.Ticks && daylightTimes.End.Ticks > time.Ticks)
				{
					return true;
				}
			}
			else if (time.Year == daylightTimes.Start.Year && time.Year == daylightTimes.End.Year && (time.Ticks < daylightTimes.End.Ticks || time.Ticks > daylightTimes.Start.Ticks))
			{
				return true;
			}
			return false;
		}

		// Token: 0x06001442 RID: 5186 RVA: 0x00051A20 File Offset: 0x0004FC20
		public virtual DateTime ToLocalTime(DateTime time)
		{
			if (time.Kind == DateTimeKind.Local)
			{
				return time;
			}
			TimeSpan utcOffset = this.GetUtcOffset(time);
			if (utcOffset.Ticks > 0L)
			{
				if (DateTime.MaxValue - utcOffset < time)
				{
					return DateTime.SpecifyKind(DateTime.MaxValue, DateTimeKind.Local);
				}
			}
			else if (utcOffset.Ticks < 0L && time.Ticks + utcOffset.Ticks < DateTime.MinValue.Ticks)
			{
				return DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Local);
			}
			DateTime dateTime = time.Add(utcOffset);
			DaylightTime daylightChanges = this.GetDaylightChanges(time.Year);
			if (daylightChanges.Delta.Ticks == 0L)
			{
				return DateTime.SpecifyKind(dateTime, DateTimeKind.Local);
			}
			if (dateTime < daylightChanges.End && daylightChanges.End.Subtract(daylightChanges.Delta) <= dateTime)
			{
				return DateTime.SpecifyKind(dateTime, DateTimeKind.Local);
			}
			TimeSpan utcOffset2 = this.GetUtcOffset(dateTime);
			return DateTime.SpecifyKind(time.Add(utcOffset2), DateTimeKind.Local);
		}

		// Token: 0x06001443 RID: 5187 RVA: 0x00051B38 File Offset: 0x0004FD38
		public virtual DateTime ToUniversalTime(DateTime time)
		{
			if (time.Kind == DateTimeKind.Utc)
			{
				return time;
			}
			TimeSpan utcOffset = this.GetUtcOffset(time);
			if (utcOffset.Ticks < 0L)
			{
				if (DateTime.MaxValue + utcOffset < time)
				{
					return DateTime.SpecifyKind(DateTime.MaxValue, DateTimeKind.Utc);
				}
			}
			else if (utcOffset.Ticks > 0L && DateTime.MinValue + utcOffset > time)
			{
				return DateTime.SpecifyKind(DateTime.MinValue, DateTimeKind.Utc);
			}
			return DateTime.SpecifyKind(new DateTime(time.Ticks - utcOffset.Ticks), DateTimeKind.Utc);
		}

		// Token: 0x06001444 RID: 5188 RVA: 0x00051BDC File Offset: 0x0004FDDC
		internal TimeSpan GetLocalTimeDiff(DateTime time)
		{
			return this.GetLocalTimeDiff(time, this.GetUtcOffset(time));
		}

		// Token: 0x06001445 RID: 5189 RVA: 0x00051BEC File Offset: 0x0004FDEC
		internal TimeSpan GetLocalTimeDiff(DateTime time, TimeSpan utc_offset)
		{
			DaylightTime daylightChanges = this.GetDaylightChanges(time.Year);
			if (daylightChanges.Delta.Ticks == 0L)
			{
				return utc_offset;
			}
			DateTime dateTime = time.Add(utc_offset);
			if (dateTime < daylightChanges.End && daylightChanges.End.Subtract(daylightChanges.Delta) <= dateTime)
			{
				return utc_offset;
			}
			if (dateTime >= daylightChanges.Start && daylightChanges.Start.Add(daylightChanges.Delta) > dateTime)
			{
				return utc_offset - daylightChanges.Delta;
			}
			return this.GetUtcOffset(dateTime);
		}

		// Token: 0x040007D6 RID: 2006
		private static TimeZone currentTimeZone;

		// Token: 0x040007D7 RID: 2007
		[NonSerialized]
		private static object tz_lock = new object();

		// Token: 0x040007D8 RID: 2008
		[NonSerialized]
		private static long timezone_check;
	}
}
