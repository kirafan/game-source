﻿using System;
using System.Runtime.ConstrainedExecution;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200003C RID: 60
	[ComVisible(true)]
	[MonoTODO("Serialization needs tests")]
	[Serializable]
	public struct RuntimeTypeHandle : ISerializable
	{
		// Token: 0x06000642 RID: 1602 RVA: 0x000147C0 File Offset: 0x000129C0
		internal RuntimeTypeHandle(IntPtr val)
		{
			this.value = val;
		}

		// Token: 0x06000643 RID: 1603 RVA: 0x000147CC File Offset: 0x000129CC
		private RuntimeTypeHandle(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			MonoType monoType = (MonoType)info.GetValue("TypeObj", typeof(MonoType));
			this.value = monoType.TypeHandle.Value;
			if (this.value == IntPtr.Zero)
			{
				throw new SerializationException(Locale.GetText("Insufficient state."));
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000644 RID: 1604 RVA: 0x00014840 File Offset: 0x00012A40
		public IntPtr Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x06000645 RID: 1605 RVA: 0x00014848 File Offset: 0x00012A48
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			if (this.value == IntPtr.Zero)
			{
				throw new SerializationException("Object fields may not be properly initialized");
			}
			info.AddValue("TypeObj", Type.GetTypeHandle(this), typeof(MonoType));
		}

		// Token: 0x06000646 RID: 1606 RVA: 0x000148B0 File Offset: 0x00012AB0
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public override bool Equals(object obj)
		{
			return obj != null && base.GetType() == obj.GetType() && this.value == ((RuntimeTypeHandle)obj).Value;
		}

		// Token: 0x06000647 RID: 1607 RVA: 0x000148FC File Offset: 0x00012AFC
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		public bool Equals(RuntimeTypeHandle handle)
		{
			return this.value == handle.Value;
		}

		// Token: 0x06000648 RID: 1608 RVA: 0x00014910 File Offset: 0x00012B10
		public override int GetHashCode()
		{
			return this.value.GetHashCode();
		}

		// Token: 0x06000649 RID: 1609 RVA: 0x00014920 File Offset: 0x00012B20
		[ReliabilityContract(Consistency.WillNotCorruptState, Cer.Success)]
		[CLSCompliant(false)]
		public ModuleHandle GetModuleHandle()
		{
			if (this.value == IntPtr.Zero)
			{
				throw new InvalidOperationException("Object fields may not be properly initialized");
			}
			return Type.GetTypeFromHandle(this).Module.ModuleHandle;
		}

		// Token: 0x0600064A RID: 1610 RVA: 0x00014958 File Offset: 0x00012B58
		public static bool operator ==(RuntimeTypeHandle left, object right)
		{
			return right != null && right is RuntimeTypeHandle && left.Equals((RuntimeTypeHandle)right);
		}

		// Token: 0x0600064B RID: 1611 RVA: 0x0001497C File Offset: 0x00012B7C
		public static bool operator !=(RuntimeTypeHandle left, object right)
		{
			return right == null || !(right is RuntimeTypeHandle) || !left.Equals((RuntimeTypeHandle)right);
		}

		// Token: 0x0600064C RID: 1612 RVA: 0x000149B0 File Offset: 0x00012BB0
		public static bool operator ==(object left, RuntimeTypeHandle right)
		{
			return left != null && left is RuntimeTypeHandle && ((RuntimeTypeHandle)left).Equals(right);
		}

		// Token: 0x0600064D RID: 1613 RVA: 0x000149E0 File Offset: 0x00012BE0
		public static bool operator !=(object left, RuntimeTypeHandle right)
		{
			return left == null || !(left is RuntimeTypeHandle) || !((RuntimeTypeHandle)left).Equals(right);
		}

		// Token: 0x04000082 RID: 130
		private IntPtr value;
	}
}
