﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000103 RID: 259
	[ComVisible(true)]
	[Serializable]
	public class ArithmeticException : SystemException
	{
		// Token: 0x06000D82 RID: 3458 RVA: 0x0003AE9C File Offset: 0x0003909C
		public ArithmeticException() : base(Locale.GetText("Overflow or underflow in the arithmetic operation."))
		{
			base.HResult = -2147024362;
		}

		// Token: 0x06000D83 RID: 3459 RVA: 0x0003AEBC File Offset: 0x000390BC
		public ArithmeticException(string message) : base(message)
		{
			base.HResult = -2147024362;
		}

		// Token: 0x06000D84 RID: 3460 RVA: 0x0003AED0 File Offset: 0x000390D0
		public ArithmeticException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147024362;
		}

		// Token: 0x06000D85 RID: 3461 RVA: 0x0003AEE8 File Offset: 0x000390E8
		protected ArithmeticException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040003A3 RID: 931
		private const int Result = -2147024362;
	}
}
