﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000166 RID: 358
	[ComVisible(true)]
	[Serializable]
	public sealed class OperatingSystem : ICloneable, ISerializable
	{
		// Token: 0x06001341 RID: 4929 RVA: 0x0004D3D0 File Offset: 0x0004B5D0
		public OperatingSystem(PlatformID platform, Version version)
		{
			if (version == null)
			{
				throw new ArgumentNullException("version");
			}
			this._platform = platform;
			this._version = version;
		}

		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x06001342 RID: 4930 RVA: 0x0004D414 File Offset: 0x0004B614
		public PlatformID Platform
		{
			get
			{
				return this._platform;
			}
		}

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x06001343 RID: 4931 RVA: 0x0004D41C File Offset: 0x0004B61C
		public Version Version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x06001344 RID: 4932 RVA: 0x0004D424 File Offset: 0x0004B624
		public string ServicePack
		{
			get
			{
				return this._servicePack;
			}
		}

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x06001345 RID: 4933 RVA: 0x0004D42C File Offset: 0x0004B62C
		public string VersionString
		{
			get
			{
				return this.ToString();
			}
		}

		// Token: 0x06001346 RID: 4934 RVA: 0x0004D434 File Offset: 0x0004B634
		public object Clone()
		{
			return new OperatingSystem(this._platform, this._version);
		}

		// Token: 0x06001347 RID: 4935 RVA: 0x0004D448 File Offset: 0x0004B648
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("_platform", this._platform);
			info.AddValue("_version", this._version);
			info.AddValue("_servicePack", this._servicePack);
		}

		// Token: 0x06001348 RID: 4936 RVA: 0x0004D490 File Offset: 0x0004B690
		public override string ToString()
		{
			int platform = (int)this._platform;
			string str;
			switch (platform)
			{
			case 0:
				str = "Microsoft Win32S";
				goto IL_96;
			case 1:
				str = "Microsoft Windows 98";
				goto IL_96;
			case 2:
				str = "Microsoft Windows NT";
				goto IL_96;
			case 3:
				str = "Microsoft Windows CE";
				goto IL_96;
			case 4:
				break;
			case 5:
				str = "XBox";
				goto IL_96;
			case 6:
				str = "OSX";
				goto IL_96;
			default:
				if (platform != 128)
				{
					str = Locale.GetText("<unknown>");
					goto IL_96;
				}
				break;
			}
			str = "Unix";
			IL_96:
			return str + " " + this._version.ToString();
		}

		// Token: 0x0400058F RID: 1423
		private PlatformID _platform;

		// Token: 0x04000590 RID: 1424
		private Version _version;

		// Token: 0x04000591 RID: 1425
		private string _servicePack = string.Empty;
	}
}
