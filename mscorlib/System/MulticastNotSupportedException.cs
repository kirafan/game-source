﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200015B RID: 347
	[ComVisible(true)]
	[Serializable]
	public sealed class MulticastNotSupportedException : SystemException
	{
		// Token: 0x0600128D RID: 4749 RVA: 0x000494B4 File Offset: 0x000476B4
		public MulticastNotSupportedException() : base(Locale.GetText("This operation cannot be performed with the specified delagates."))
		{
		}

		// Token: 0x0600128E RID: 4750 RVA: 0x000494C8 File Offset: 0x000476C8
		public MulticastNotSupportedException(string message) : base(message)
		{
		}

		// Token: 0x0600128F RID: 4751 RVA: 0x000494D4 File Offset: 0x000476D4
		public MulticastNotSupportedException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06001290 RID: 4752 RVA: 0x000494E0 File Offset: 0x000476E0
		internal MulticastNotSupportedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
