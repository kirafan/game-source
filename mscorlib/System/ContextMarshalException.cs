﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000119 RID: 281
	[Obsolete("this type is obsoleted in 2.0 profile")]
	[ComVisible(true)]
	[Serializable]
	public class ContextMarshalException : SystemException
	{
		// Token: 0x06000E95 RID: 3733 RVA: 0x0003CF4C File Offset: 0x0003B14C
		public ContextMarshalException() : base(Locale.GetText("Attempt to marshal and object across a context failed."))
		{
			base.HResult = -2146233084;
		}

		// Token: 0x06000E96 RID: 3734 RVA: 0x0003CF6C File Offset: 0x0003B16C
		public ContextMarshalException(string message) : base(message)
		{
			base.HResult = -2146233084;
		}

		// Token: 0x06000E97 RID: 3735 RVA: 0x0003CF80 File Offset: 0x0003B180
		protected ContextMarshalException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06000E98 RID: 3736 RVA: 0x0003CF8C File Offset: 0x0003B18C
		public ContextMarshalException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233084;
		}

		// Token: 0x0400047E RID: 1150
		private const int Result = -2146233084;
	}
}
