﻿using System;

namespace System
{
	// Token: 0x02000116 RID: 278
	[Flags]
	[Serializable]
	public enum ConsoleModifiers
	{
		// Token: 0x04000478 RID: 1144
		Alt = 1,
		// Token: 0x04000479 RID: 1145
		Shift = 2,
		// Token: 0x0400047A RID: 1146
		Control = 4
	}
}
