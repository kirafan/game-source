﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000008 RID: 8
	[ComVisible(true)]
	[CLSCompliant(false)]
	public interface IConvertible
	{
		// Token: 0x06000070 RID: 112
		TypeCode GetTypeCode();

		// Token: 0x06000071 RID: 113
		bool ToBoolean(IFormatProvider provider);

		// Token: 0x06000072 RID: 114
		byte ToByte(IFormatProvider provider);

		// Token: 0x06000073 RID: 115
		char ToChar(IFormatProvider provider);

		// Token: 0x06000074 RID: 116
		DateTime ToDateTime(IFormatProvider provider);

		// Token: 0x06000075 RID: 117
		decimal ToDecimal(IFormatProvider provider);

		// Token: 0x06000076 RID: 118
		double ToDouble(IFormatProvider provider);

		// Token: 0x06000077 RID: 119
		short ToInt16(IFormatProvider provider);

		// Token: 0x06000078 RID: 120
		int ToInt32(IFormatProvider provider);

		// Token: 0x06000079 RID: 121
		long ToInt64(IFormatProvider provider);

		// Token: 0x0600007A RID: 122
		sbyte ToSByte(IFormatProvider provider);

		// Token: 0x0600007B RID: 123
		float ToSingle(IFormatProvider provider);

		// Token: 0x0600007C RID: 124
		string ToString(IFormatProvider provider);

		// Token: 0x0600007D RID: 125
		object ToType(Type conversionType, IFormatProvider provider);

		// Token: 0x0600007E RID: 126
		ushort ToUInt16(IFormatProvider provider);

		// Token: 0x0600007F RID: 127
		uint ToUInt32(IFormatProvider provider);

		// Token: 0x06000080 RID: 128
		ulong ToUInt64(IFormatProvider provider);
	}
}
