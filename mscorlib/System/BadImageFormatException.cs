﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace System
{
	// Token: 0x02000107 RID: 263
	[ComVisible(true)]
	[Serializable]
	public class BadImageFormatException : SystemException
	{
		// Token: 0x06000D8C RID: 3468 RVA: 0x0003AF64 File Offset: 0x00039164
		public BadImageFormatException() : base(Locale.GetText("Format of the executable (.exe) or library (.dll) is invalid."))
		{
			base.HResult = -2147024885;
		}

		// Token: 0x06000D8D RID: 3469 RVA: 0x0003AF84 File Offset: 0x00039184
		public BadImageFormatException(string message) : base(message)
		{
			base.HResult = -2147024885;
		}

		// Token: 0x06000D8E RID: 3470 RVA: 0x0003AF98 File Offset: 0x00039198
		protected BadImageFormatException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.fileName = info.GetString("BadImageFormat_FileName");
			this.fusionLog = info.GetString("BadImageFormat_FusionLog");
		}

		// Token: 0x06000D8F RID: 3471 RVA: 0x0003AFD0 File Offset: 0x000391D0
		public BadImageFormatException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2147024885;
		}

		// Token: 0x06000D90 RID: 3472 RVA: 0x0003AFE8 File Offset: 0x000391E8
		public BadImageFormatException(string message, string fileName) : base(message)
		{
			this.fileName = fileName;
			base.HResult = -2147024885;
		}

		// Token: 0x06000D91 RID: 3473 RVA: 0x0003B004 File Offset: 0x00039204
		public BadImageFormatException(string message, string fileName, Exception inner) : base(message, inner)
		{
			this.fileName = fileName;
			base.HResult = -2147024885;
		}

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x06000D92 RID: 3474 RVA: 0x0003B020 File Offset: 0x00039220
		public override string Message
		{
			get
			{
				if (this.message == null)
				{
					return string.Format(CultureInfo.CurrentCulture, "Could not load file or assembly '{0}' or one of its dependencies. An attempt was made to load a program with an incorrect format.", new object[]
					{
						this.fileName
					});
				}
				return base.Message;
			}
		}

		// Token: 0x170001FC RID: 508
		// (get) Token: 0x06000D93 RID: 3475 RVA: 0x0003B060 File Offset: 0x00039260
		public string FileName
		{
			get
			{
				return this.fileName;
			}
		}

		// Token: 0x170001FD RID: 509
		// (get) Token: 0x06000D94 RID: 3476 RVA: 0x0003B068 File Offset: 0x00039268
		[MonoTODO("Probably not entirely correct. fusionLog needs to be set somehow (we are probably missing internal constuctor)")]
		public string FusionLog
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence, ControlPolicy\"/>\n</PermissionSet>\n")]
			get
			{
				return this.fusionLog;
			}
		}

		// Token: 0x06000D95 RID: 3477 RVA: 0x0003B070 File Offset: 0x00039270
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("BadImageFormat_FileName", this.fileName);
			info.AddValue("BadImageFormat_FusionLog", this.fusionLog);
		}

		// Token: 0x06000D96 RID: 3478 RVA: 0x0003B0A8 File Offset: 0x000392A8
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(this.GetType().FullName);
			stringBuilder.AppendFormat(": {0}", this.Message);
			if (this.fileName != null && this.fileName.Length > 0)
			{
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.AppendFormat("File name: '{0}'", this.fileName);
			}
			if (this.InnerException != null)
			{
				stringBuilder.AppendFormat(" ---> {0}", this.InnerException);
			}
			if (this.StackTrace != null)
			{
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(this.StackTrace);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x040003B7 RID: 951
		private const int Result = -2147024885;

		// Token: 0x040003B8 RID: 952
		private string fileName;

		// Token: 0x040003B9 RID: 953
		private string fusionLog;
	}
}
