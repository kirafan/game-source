﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000101 RID: 257
	[ComVisible(true)]
	[Serializable]
	public class ArgumentNullException : ArgumentException
	{
		// Token: 0x06000D74 RID: 3444 RVA: 0x0003AD20 File Offset: 0x00038F20
		public ArgumentNullException() : base(Locale.GetText("Argument cannot be null."))
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06000D75 RID: 3445 RVA: 0x0003AD40 File Offset: 0x00038F40
		public ArgumentNullException(string paramName) : base(Locale.GetText("Argument cannot be null."), paramName)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06000D76 RID: 3446 RVA: 0x0003AD60 File Offset: 0x00038F60
		public ArgumentNullException(string paramName, string message) : base(message, paramName)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06000D77 RID: 3447 RVA: 0x0003AD78 File Offset: 0x00038F78
		public ArgumentNullException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2147467261;
		}

		// Token: 0x06000D78 RID: 3448 RVA: 0x0003AD90 File Offset: 0x00038F90
		protected ArgumentNullException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040003A0 RID: 928
		private const int Result = -2147467261;
	}
}
