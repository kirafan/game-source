﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200001B RID: 27
	[ComVisible(true)]
	[Serializable]
	public struct Char : IConvertible, IComparable, IComparable<char>, IEquatable<char>
	{
		// Token: 0x06000175 RID: 373 RVA: 0x00005EBC File Offset: 0x000040BC
		static Char()
		{
			char.GetDataTablePointers(out char.category_data, out char.numeric_data, out char.numeric_data_values, out char.to_lower_data_low, out char.to_lower_data_high, out char.to_upper_data_low, out char.to_upper_data_high);
		}

		// Token: 0x06000176 RID: 374 RVA: 0x00005EF4 File Offset: 0x000040F4
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			return Convert.ToType(this, targetType, provider, false);
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00005F24 File Offset: 0x00004124
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000178 RID: 376 RVA: 0x00005F2C File Offset: 0x0000412C
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return Convert.ToByte(this);
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00005F38 File Offset: 0x00004138
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return this;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00005F3C File Offset: 0x0000413C
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00005F44 File Offset: 0x00004144
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00005F4C File Offset: 0x0000414C
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00005F54 File Offset: 0x00004154
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00005F60 File Offset: 0x00004160
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this);
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00005F6C File Offset: 0x0000416C
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00005F78 File Offset: 0x00004178
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this);
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00005F84 File Offset: 0x00004184
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00005F8C File Offset: 0x0000418C
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this);
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00005F98 File Offset: 0x00004198
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00005FA4 File Offset: 0x000041A4
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		// Token: 0x06000185 RID: 389
		[MethodImpl(MethodImplOptions.InternalCall)]
		private unsafe static extern void GetDataTablePointers(out byte* category_data, out byte* numeric_data, out double* numeric_data_values, out ushort* to_lower_data_low, out ushort* to_lower_data_high, out ushort* to_upper_data_low, out ushort* to_upper_data_high);

		// Token: 0x06000186 RID: 390 RVA: 0x00005FB0 File Offset: 0x000041B0
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is char))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.Char"));
			}
			char c = (char)value;
			if (this == c)
			{
				return 0;
			}
			if (this > c)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06000187 RID: 391 RVA: 0x00005FFC File Offset: 0x000041FC
		public override bool Equals(object obj)
		{
			return obj is char && (char)obj == this;
		}

		// Token: 0x06000188 RID: 392 RVA: 0x00006018 File Offset: 0x00004218
		public int CompareTo(char value)
		{
			if (this == value)
			{
				return 0;
			}
			if (this > value)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00006030 File Offset: 0x00004230
		public static string ConvertFromUtf32(int utf32)
		{
			if (utf32 < 0 || utf32 > 1114111)
			{
				throw new ArgumentOutOfRangeException("utf32", "The argument must be from 0 to 0x10FFFF.");
			}
			if (55296 <= utf32 && utf32 <= 57343)
			{
				throw new ArgumentOutOfRangeException("utf32", "The argument must not be in surrogate pair range.");
			}
			if (utf32 < 65536)
			{
				return new string((char)utf32, 1);
			}
			utf32 -= 65536;
			return new string(new char[]
			{
				(char)((utf32 >> 10) + 55296),
				(char)(utf32 % 1024 + 56320)
			});
		}

		// Token: 0x0600018A RID: 394 RVA: 0x000060CC File Offset: 0x000042CC
		public static int ConvertToUtf32(char highSurrogate, char lowSurrogate)
		{
			if (highSurrogate < '\ud800' || '\udbff' < highSurrogate)
			{
				throw new ArgumentOutOfRangeException("highSurrogate");
			}
			if (lowSurrogate < '\udc00' || '\udfff' < lowSurrogate)
			{
				throw new ArgumentOutOfRangeException("lowSurrogate");
			}
			return 65536 + (int)((int)(highSurrogate - '\ud800') << 10) + (int)(lowSurrogate - '\udc00');
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00006134 File Offset: 0x00004334
		public static int ConvertToUtf32(string s, int index)
		{
			char.CheckParameter(s, index);
			if (!char.IsSurrogate(s[index]))
			{
				return (int)s[index];
			}
			if (!char.IsHighSurrogate(s[index]) || index == s.Length - 1 || !char.IsLowSurrogate(s[index + 1]))
			{
				throw new ArgumentException(string.Format("The string contains invalid surrogate pair character at {0}", index));
			}
			return char.ConvertToUtf32(s[index], s[index + 1]);
		}

		// Token: 0x0600018C RID: 396 RVA: 0x000061C0 File Offset: 0x000043C0
		public bool Equals(char obj)
		{
			return this == obj;
		}

		// Token: 0x0600018D RID: 397 RVA: 0x000061C8 File Offset: 0x000043C8
		public static bool IsSurrogatePair(char highSurrogate, char lowSurrogate)
		{
			return '\ud800' <= highSurrogate && highSurrogate <= '\udbff' && '\udc00' <= lowSurrogate && lowSurrogate <= '\udfff';
		}

		// Token: 0x0600018E RID: 398 RVA: 0x000061FC File Offset: 0x000043FC
		public static bool IsSurrogatePair(string s, int index)
		{
			char.CheckParameter(s, index);
			return index + 1 < s.Length && char.IsSurrogatePair(s[index], s[index + 1]);
		}

		// Token: 0x0600018F RID: 399 RVA: 0x00006238 File Offset: 0x00004438
		public override int GetHashCode()
		{
			return (int)this;
		}

		// Token: 0x06000190 RID: 400 RVA: 0x0000623C File Offset: 0x0000443C
		public unsafe static double GetNumericValue(char c)
		{
			if (c <= '㊉')
			{
				return char.numeric_data_values[char.numeric_data[c]];
			}
			if (c >= '０' && c <= '９')
			{
				return (double)(c - '０');
			}
			return -1.0;
		}

		// Token: 0x06000191 RID: 401 RVA: 0x00006290 File Offset: 0x00004490
		public static double GetNumericValue(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.GetNumericValue(s[index]);
		}

		// Token: 0x06000192 RID: 402 RVA: 0x000062A8 File Offset: 0x000044A8
		public unsafe static UnicodeCategory GetUnicodeCategory(char c)
		{
			return (UnicodeCategory)char.category_data[c];
		}

		// Token: 0x06000193 RID: 403 RVA: 0x000062B4 File Offset: 0x000044B4
		public static UnicodeCategory GetUnicodeCategory(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.GetUnicodeCategory(s[index]);
		}

		// Token: 0x06000194 RID: 404 RVA: 0x000062CC File Offset: 0x000044CC
		public unsafe static bool IsControl(char c)
		{
			return char.category_data[c] == 14;
		}

		// Token: 0x06000195 RID: 405 RVA: 0x000062DC File Offset: 0x000044DC
		public static bool IsControl(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsControl(s[index]);
		}

		// Token: 0x06000196 RID: 406 RVA: 0x000062F4 File Offset: 0x000044F4
		public unsafe static bool IsDigit(char c)
		{
			return char.category_data[c] == 8;
		}

		// Token: 0x06000197 RID: 407 RVA: 0x00006304 File Offset: 0x00004504
		public static bool IsDigit(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsDigit(s[index]);
		}

		// Token: 0x06000198 RID: 408 RVA: 0x0000631C File Offset: 0x0000451C
		public static bool IsHighSurrogate(char c)
		{
			return c >= '\ud800' && c <= '\udbff';
		}

		// Token: 0x06000199 RID: 409 RVA: 0x00006338 File Offset: 0x00004538
		public static bool IsHighSurrogate(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsHighSurrogate(s[index]);
		}

		// Token: 0x0600019A RID: 410 RVA: 0x00006350 File Offset: 0x00004550
		public unsafe static bool IsLetter(char c)
		{
			return char.category_data[c] <= 4;
		}

		// Token: 0x0600019B RID: 411 RVA: 0x00006360 File Offset: 0x00004560
		public static bool IsLetter(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsLetter(s[index]);
		}

		// Token: 0x0600019C RID: 412 RVA: 0x00006378 File Offset: 0x00004578
		public unsafe static bool IsLetterOrDigit(char c)
		{
			int num = (int)char.category_data[c];
			return num <= 4 || num == 8;
		}

		// Token: 0x0600019D RID: 413 RVA: 0x0000639C File Offset: 0x0000459C
		public static bool IsLetterOrDigit(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsLetterOrDigit(s[index]);
		}

		// Token: 0x0600019E RID: 414 RVA: 0x000063B4 File Offset: 0x000045B4
		public unsafe static bool IsLower(char c)
		{
			return char.category_data[c] == 1;
		}

		// Token: 0x0600019F RID: 415 RVA: 0x000063C4 File Offset: 0x000045C4
		public static bool IsLower(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsLower(s[index]);
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x000063DC File Offset: 0x000045DC
		public static bool IsLowSurrogate(char c)
		{
			return c >= '\udc00' && c <= '\udfff';
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x000063F8 File Offset: 0x000045F8
		public static bool IsLowSurrogate(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsLowSurrogate(s[index]);
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x00006410 File Offset: 0x00004610
		public unsafe static bool IsNumber(char c)
		{
			int num = (int)char.category_data[c];
			return num >= 8 && num <= 10;
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x00006438 File Offset: 0x00004638
		public static bool IsNumber(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsNumber(s[index]);
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00006450 File Offset: 0x00004650
		public unsafe static bool IsPunctuation(char c)
		{
			int num = (int)char.category_data[c];
			return num >= 18 && num <= 24;
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x0000647C File Offset: 0x0000467C
		public static bool IsPunctuation(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsPunctuation(s[index]);
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x00006494 File Offset: 0x00004694
		public unsafe static bool IsSeparator(char c)
		{
			int num = (int)char.category_data[c];
			return num >= 11 && num <= 13;
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x000064C0 File Offset: 0x000046C0
		public static bool IsSeparator(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsSeparator(s[index]);
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x000064D8 File Offset: 0x000046D8
		public unsafe static bool IsSurrogate(char c)
		{
			return char.category_data[c] == 16;
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x000064E8 File Offset: 0x000046E8
		public static bool IsSurrogate(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsSurrogate(s[index]);
		}

		// Token: 0x060001AA RID: 426 RVA: 0x00006500 File Offset: 0x00004700
		public unsafe static bool IsSymbol(char c)
		{
			int num = (int)char.category_data[c];
			return num >= 25 && num <= 28;
		}

		// Token: 0x060001AB RID: 427 RVA: 0x0000652C File Offset: 0x0000472C
		public static bool IsSymbol(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsSymbol(s[index]);
		}

		// Token: 0x060001AC RID: 428 RVA: 0x00006544 File Offset: 0x00004744
		public unsafe static bool IsUpper(char c)
		{
			return char.category_data[c] == 0;
		}

		// Token: 0x060001AD RID: 429 RVA: 0x00006554 File Offset: 0x00004754
		public static bool IsUpper(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsUpper(s[index]);
		}

		// Token: 0x060001AE RID: 430 RVA: 0x0000656C File Offset: 0x0000476C
		public unsafe static bool IsWhiteSpace(char c)
		{
			int num = (int)char.category_data[c];
			return num > 10 && (num <= 13 || (c >= '\t' && c <= '\r') || c == '\u0085' || c == '\u205f');
		}

		// Token: 0x060001AF RID: 431 RVA: 0x000065BC File Offset: 0x000047BC
		public static bool IsWhiteSpace(string s, int index)
		{
			char.CheckParameter(s, index);
			return char.IsWhiteSpace(s[index]);
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x000065D4 File Offset: 0x000047D4
		private static void CheckParameter(string s, int index)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			if (index < 0 || index >= s.Length)
			{
				throw new ArgumentOutOfRangeException(Locale.GetText("The value of index is less than zero, or greater than or equal to the length of s."));
			}
		}

		// Token: 0x060001B1 RID: 433 RVA: 0x00006618 File Offset: 0x00004818
		public static bool TryParse(string s, out char result)
		{
			if (s == null || s.Length != 1)
			{
				result = '\0';
				return false;
			}
			result = s[0];
			return true;
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x0000663C File Offset: 0x0000483C
		public static char Parse(string s)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			if (s.Length != 1)
			{
				throw new FormatException(Locale.GetText("s contains more than one character."));
			}
			return s[0];
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x00006680 File Offset: 0x00004880
		public static char ToLower(char c)
		{
			return CultureInfo.CurrentCulture.TextInfo.ToLower(c);
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x00006694 File Offset: 0x00004894
		public unsafe static char ToLowerInvariant(char c)
		{
			if (c <= 'Ⓩ')
			{
				return (char)char.to_lower_data_low[c];
			}
			if (c >= 'Ａ')
			{
				return (char)char.to_lower_data_high[c - 'Ａ'];
			}
			return c;
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x000066CC File Offset: 0x000048CC
		public static char ToLower(char c, CultureInfo culture)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("culture");
			}
			if (culture.LCID == 127)
			{
				return char.ToLowerInvariant(c);
			}
			return culture.TextInfo.ToLower(c);
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x0000670C File Offset: 0x0000490C
		public static char ToUpper(char c)
		{
			return CultureInfo.CurrentCulture.TextInfo.ToUpper(c);
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x00006720 File Offset: 0x00004920
		public unsafe static char ToUpperInvariant(char c)
		{
			if (c <= 'ⓩ')
			{
				return (char)char.to_upper_data_low[c];
			}
			if (c >= 'Ａ')
			{
				return (char)char.to_upper_data_high[c - 'Ａ'];
			}
			return c;
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00006758 File Offset: 0x00004958
		public static char ToUpper(char c, CultureInfo culture)
		{
			if (culture == null)
			{
				throw new ArgumentNullException("culture");
			}
			if (culture.LCID == 127)
			{
				return char.ToUpperInvariant(c);
			}
			return culture.TextInfo.ToUpper(c);
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x00006798 File Offset: 0x00004998
		public override string ToString()
		{
			return new string(this, 1);
		}

		// Token: 0x060001BA RID: 442 RVA: 0x000067A4 File Offset: 0x000049A4
		public static string ToString(char c)
		{
			return new string(c, 1);
		}

		// Token: 0x060001BB RID: 443 RVA: 0x000067B0 File Offset: 0x000049B0
		public string ToString(IFormatProvider provider)
		{
			return new string(this, 1);
		}

		// Token: 0x060001BC RID: 444 RVA: 0x000067BC File Offset: 0x000049BC
		public TypeCode GetTypeCode()
		{
			return TypeCode.Char;
		}

		// Token: 0x0400001E RID: 30
		public const char MaxValue = '￿';

		// Token: 0x0400001F RID: 31
		public const char MinValue = '\0';

		// Token: 0x04000020 RID: 32
		internal char m_value;

		// Token: 0x04000021 RID: 33
		private unsafe static readonly byte* category_data;

		// Token: 0x04000022 RID: 34
		private unsafe static readonly byte* numeric_data;

		// Token: 0x04000023 RID: 35
		private unsafe static readonly double* numeric_data_values;

		// Token: 0x04000024 RID: 36
		private unsafe static readonly ushort* to_lower_data_low;

		// Token: 0x04000025 RID: 37
		private unsafe static readonly ushort* to_lower_data_high;

		// Token: 0x04000026 RID: 38
		private unsafe static readonly ushort* to_upper_data_low;

		// Token: 0x04000027 RID: 39
		private unsafe static readonly ushort* to_upper_data_high;
	}
}
