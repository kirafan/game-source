﻿using System;

namespace System
{
	// Token: 0x02000112 RID: 274
	[Serializable]
	public enum ConsoleColor
	{
		// Token: 0x040003CF RID: 975
		Black,
		// Token: 0x040003D0 RID: 976
		DarkBlue,
		// Token: 0x040003D1 RID: 977
		DarkGreen,
		// Token: 0x040003D2 RID: 978
		DarkCyan,
		// Token: 0x040003D3 RID: 979
		DarkRed,
		// Token: 0x040003D4 RID: 980
		DarkMagenta,
		// Token: 0x040003D5 RID: 981
		DarkYellow,
		// Token: 0x040003D6 RID: 982
		Gray,
		// Token: 0x040003D7 RID: 983
		DarkGray,
		// Token: 0x040003D8 RID: 984
		Blue,
		// Token: 0x040003D9 RID: 985
		Green,
		// Token: 0x040003DA RID: 986
		Cyan,
		// Token: 0x040003DB RID: 987
		Red,
		// Token: 0x040003DC RID: 988
		Magenta,
		// Token: 0x040003DD RID: 989
		Yellow,
		// Token: 0x040003DE RID: 990
		White
	}
}
