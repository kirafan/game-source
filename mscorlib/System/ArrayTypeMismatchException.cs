﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000104 RID: 260
	[ComVisible(true)]
	[Serializable]
	public class ArrayTypeMismatchException : SystemException
	{
		// Token: 0x06000D86 RID: 3462 RVA: 0x0003AEF4 File Offset: 0x000390F4
		public ArrayTypeMismatchException() : base(Locale.GetText("Source array type cannot be assigned to destination array type."))
		{
			base.HResult = -2146233085;
		}

		// Token: 0x06000D87 RID: 3463 RVA: 0x0003AF14 File Offset: 0x00039114
		public ArrayTypeMismatchException(string message) : base(message)
		{
			base.HResult = -2146233085;
		}

		// Token: 0x06000D88 RID: 3464 RVA: 0x0003AF28 File Offset: 0x00039128
		public ArrayTypeMismatchException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233085;
		}

		// Token: 0x06000D89 RID: 3465 RVA: 0x0003AF40 File Offset: 0x00039140
		protected ArrayTypeMismatchException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x040003A4 RID: 932
		private const int Result = -2146233085;
	}
}
