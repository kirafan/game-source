﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200000B RID: 11
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Delegate, Inherited = false, AllowMultiple = false)]
	[ComVisible(true)]
	public sealed class SerializableAttribute : Attribute
	{
	}
}
