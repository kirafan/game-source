﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000147 RID: 327
	[ComVisible(true)]
	[Serializable]
	public sealed class InvalidProgramException : SystemException
	{
		// Token: 0x060011C2 RID: 4546 RVA: 0x000471B0 File Offset: 0x000453B0
		public InvalidProgramException() : base(Locale.GetText("Metadata is invalid."))
		{
		}

		// Token: 0x060011C3 RID: 4547 RVA: 0x000471C4 File Offset: 0x000453C4
		public InvalidProgramException(string message) : base(message)
		{
		}

		// Token: 0x060011C4 RID: 4548 RVA: 0x000471D0 File Offset: 0x000453D0
		public InvalidProgramException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x060011C5 RID: 4549 RVA: 0x000471DC File Offset: 0x000453DC
		internal InvalidProgramException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
