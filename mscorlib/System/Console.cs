﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using System.Text;

namespace System
{
	// Token: 0x0200010F RID: 271
	public static class Console
	{
		// Token: 0x06000DD9 RID: 3545 RVA: 0x0003BFFC File Offset: 0x0003A1FC
		static Console()
		{
			if (Environment.IsRunningOnWindows)
			{
				try
				{
					Console.inputEncoding = Encoding.GetEncoding(Console.WindowsConsole.GetInputCodePage());
					Console.outputEncoding = Encoding.GetEncoding(Console.WindowsConsole.GetOutputCodePage());
				}
				catch
				{
					Console.inputEncoding = (Console.outputEncoding = Encoding.Default);
				}
			}
			else
			{
				int num = 0;
				Encoding.InternalCodePage(ref num);
				if (num != -1 && ((num & 268435455) == 3 || (num & 268435456) != 0))
				{
					Console.inputEncoding = (Console.outputEncoding = Encoding.UTF8Unmarked);
				}
				else
				{
					Console.inputEncoding = (Console.outputEncoding = Encoding.Default);
				}
			}
			Console.SetEncodings(Console.inputEncoding, Console.outputEncoding);
		}

		// Token: 0x1400000B RID: 11
		// (add) Token: 0x06000DDA RID: 3546 RVA: 0x0003C0DC File Offset: 0x0003A2DC
		// (remove) Token: 0x06000DDB RID: 3547 RVA: 0x0003C110 File Offset: 0x0003A310
		public static event ConsoleCancelEventHandler CancelKeyPress
		{
			add
			{
				if (!ConsoleDriver.Initialized)
				{
					ConsoleDriver.Init();
				}
				Console.cancel_event = (ConsoleCancelEventHandler)Delegate.Combine(Console.cancel_event, value);
			}
			remove
			{
				if (!ConsoleDriver.Initialized)
				{
					ConsoleDriver.Init();
				}
				Console.cancel_event = (ConsoleCancelEventHandler)Delegate.Remove(Console.cancel_event, value);
			}
		}

		// Token: 0x06000DDC RID: 3548 RVA: 0x0003C144 File Offset: 0x0003A344
		private static void SetEncodings(Encoding inputEncoding, Encoding outputEncoding)
		{
			Console.stderr = new UnexceptionalStreamWriter(Console.OpenStandardError(0), outputEncoding);
			((StreamWriter)Console.stderr).AutoFlush = true;
			Console.stderr = TextWriter.Synchronized(Console.stderr, true);
			if (!Environment.IsRunningOnWindows && ConsoleDriver.IsConsole)
			{
				Console.stdout = TextWriter.Synchronized(new CStreamWriter(Console.OpenStandardOutput(0), outputEncoding)
				{
					AutoFlush = true
				}, true);
				Console.stdin = new CStreamReader(Console.OpenStandardInput(0), inputEncoding);
			}
			else
			{
				Console.stdout = new UnexceptionalStreamWriter(Console.OpenStandardOutput(0), outputEncoding);
				((StreamWriter)Console.stdout).AutoFlush = true;
				Console.stdout = TextWriter.Synchronized(Console.stdout, true);
				Console.stdin = new UnexceptionalStreamReader(Console.OpenStandardInput(0), inputEncoding);
				Console.stdin = TextReader.Synchronized(Console.stdin);
			}
			GC.SuppressFinalize(Console.stdout);
			GC.SuppressFinalize(Console.stderr);
			GC.SuppressFinalize(Console.stdin);
		}

		// Token: 0x17000200 RID: 512
		// (get) Token: 0x06000DDD RID: 3549 RVA: 0x0003C23C File Offset: 0x0003A43C
		public static TextWriter Error
		{
			get
			{
				return Console.stderr;
			}
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x06000DDE RID: 3550 RVA: 0x0003C244 File Offset: 0x0003A444
		public static TextWriter Out
		{
			get
			{
				return Console.stdout;
			}
		}

		// Token: 0x17000202 RID: 514
		// (get) Token: 0x06000DDF RID: 3551 RVA: 0x0003C24C File Offset: 0x0003A44C
		public static TextReader In
		{
			get
			{
				return Console.stdin;
			}
		}

		// Token: 0x06000DE0 RID: 3552 RVA: 0x0003C254 File Offset: 0x0003A454
		public static Stream OpenStandardError()
		{
			return Console.OpenStandardError(0);
		}

		// Token: 0x06000DE1 RID: 3553 RVA: 0x0003C25C File Offset: 0x0003A45C
		private static Stream Open(IntPtr handle, FileAccess access, int bufferSize)
		{
			Stream result;
			try
			{
				result = new FileStream(handle, access, false, bufferSize, false, bufferSize == 0);
			}
			catch (IOException)
			{
				result = new NullStream();
			}
			return result;
		}

		// Token: 0x06000DE2 RID: 3554 RVA: 0x0003C2B0 File Offset: 0x0003A4B0
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static Stream OpenStandardError(int bufferSize)
		{
			return Console.Open(MonoIO.ConsoleError, FileAccess.Write, bufferSize);
		}

		// Token: 0x06000DE3 RID: 3555 RVA: 0x0003C2C0 File Offset: 0x0003A4C0
		public static Stream OpenStandardInput()
		{
			return Console.OpenStandardInput(0);
		}

		// Token: 0x06000DE4 RID: 3556 RVA: 0x0003C2C8 File Offset: 0x0003A4C8
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static Stream OpenStandardInput(int bufferSize)
		{
			return Console.Open(MonoIO.ConsoleInput, FileAccess.Read, bufferSize);
		}

		// Token: 0x06000DE5 RID: 3557 RVA: 0x0003C2D8 File Offset: 0x0003A4D8
		public static Stream OpenStandardOutput()
		{
			return Console.OpenStandardOutput(0);
		}

		// Token: 0x06000DE6 RID: 3558 RVA: 0x0003C2E0 File Offset: 0x0003A4E0
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static Stream OpenStandardOutput(int bufferSize)
		{
			return Console.Open(MonoIO.ConsoleOutput, FileAccess.Write, bufferSize);
		}

		// Token: 0x06000DE7 RID: 3559 RVA: 0x0003C2F0 File Offset: 0x0003A4F0
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static void SetError(TextWriter newError)
		{
			if (newError == null)
			{
				throw new ArgumentNullException("newError");
			}
			Console.stderr = newError;
		}

		// Token: 0x06000DE8 RID: 3560 RVA: 0x0003C30C File Offset: 0x0003A50C
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static void SetIn(TextReader newIn)
		{
			if (newIn == null)
			{
				throw new ArgumentNullException("newIn");
			}
			Console.stdin = newIn;
		}

		// Token: 0x06000DE9 RID: 3561 RVA: 0x0003C328 File Offset: 0x0003A528
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static void SetOut(TextWriter newOut)
		{
			if (newOut == null)
			{
				throw new ArgumentNullException("newOut");
			}
			Console.stdout = newOut;
		}

		// Token: 0x06000DEA RID: 3562 RVA: 0x0003C344 File Offset: 0x0003A544
		public static void Write(bool value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DEB RID: 3563 RVA: 0x0003C354 File Offset: 0x0003A554
		public static void Write(char value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DEC RID: 3564 RVA: 0x0003C364 File Offset: 0x0003A564
		public static void Write(char[] buffer)
		{
			Console.stdout.Write(buffer);
		}

		// Token: 0x06000DED RID: 3565 RVA: 0x0003C374 File Offset: 0x0003A574
		public static void Write(decimal value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DEE RID: 3566 RVA: 0x0003C384 File Offset: 0x0003A584
		public static void Write(double value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DEF RID: 3567 RVA: 0x0003C394 File Offset: 0x0003A594
		public static void Write(int value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DF0 RID: 3568 RVA: 0x0003C3A4 File Offset: 0x0003A5A4
		public static void Write(long value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DF1 RID: 3569 RVA: 0x0003C3B4 File Offset: 0x0003A5B4
		public static void Write(object value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DF2 RID: 3570 RVA: 0x0003C3C4 File Offset: 0x0003A5C4
		public static void Write(float value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DF3 RID: 3571 RVA: 0x0003C3D4 File Offset: 0x0003A5D4
		public static void Write(string value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DF4 RID: 3572 RVA: 0x0003C3E4 File Offset: 0x0003A5E4
		[CLSCompliant(false)]
		public static void Write(uint value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DF5 RID: 3573 RVA: 0x0003C3F4 File Offset: 0x0003A5F4
		[CLSCompliant(false)]
		public static void Write(ulong value)
		{
			Console.stdout.Write(value);
		}

		// Token: 0x06000DF6 RID: 3574 RVA: 0x0003C404 File Offset: 0x0003A604
		public static void Write(string format, object arg0)
		{
			Console.stdout.Write(format, arg0);
		}

		// Token: 0x06000DF7 RID: 3575 RVA: 0x0003C414 File Offset: 0x0003A614
		public static void Write(string format, params object[] arg)
		{
			Console.stdout.Write(format, arg);
		}

		// Token: 0x06000DF8 RID: 3576 RVA: 0x0003C424 File Offset: 0x0003A624
		public static void Write(char[] buffer, int index, int count)
		{
			Console.stdout.Write(buffer, index, count);
		}

		// Token: 0x06000DF9 RID: 3577 RVA: 0x0003C434 File Offset: 0x0003A634
		public static void Write(string format, object arg0, object arg1)
		{
			Console.stdout.Write(format, arg0, arg1);
		}

		// Token: 0x06000DFA RID: 3578 RVA: 0x0003C444 File Offset: 0x0003A644
		public static void Write(string format, object arg0, object arg1, object arg2)
		{
			Console.stdout.Write(format, arg0, arg1, arg2);
		}

		// Token: 0x06000DFB RID: 3579 RVA: 0x0003C454 File Offset: 0x0003A654
		[CLSCompliant(false)]
		public static void Write(string format, object arg0, object arg1, object arg2, object arg3, __arglist)
		{
			ArgIterator argIterator = new ArgIterator(__arglist);
			int remainingCount = argIterator.GetRemainingCount();
			object[] array = new object[remainingCount + 4];
			array[0] = arg0;
			array[1] = arg1;
			array[2] = arg2;
			array[3] = arg3;
			for (int i = 0; i < remainingCount; i++)
			{
				TypedReference nextArg = argIterator.GetNextArg();
				array[i + 4] = TypedReference.ToObject(nextArg);
			}
			Console.stdout.Write(string.Format(format, array));
		}

		// Token: 0x06000DFC RID: 3580 RVA: 0x0003C4C4 File Offset: 0x0003A6C4
		public static void WriteLine()
		{
			Console.stdout.WriteLine();
		}

		// Token: 0x06000DFD RID: 3581 RVA: 0x0003C4D0 File Offset: 0x0003A6D0
		public static void WriteLine(bool value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000DFE RID: 3582 RVA: 0x0003C4E0 File Offset: 0x0003A6E0
		public static void WriteLine(char value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000DFF RID: 3583 RVA: 0x0003C4F0 File Offset: 0x0003A6F0
		public static void WriteLine(char[] buffer)
		{
			Console.stdout.WriteLine(buffer);
		}

		// Token: 0x06000E00 RID: 3584 RVA: 0x0003C500 File Offset: 0x0003A700
		public static void WriteLine(decimal value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000E01 RID: 3585 RVA: 0x0003C510 File Offset: 0x0003A710
		public static void WriteLine(double value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000E02 RID: 3586 RVA: 0x0003C520 File Offset: 0x0003A720
		public static void WriteLine(int value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000E03 RID: 3587 RVA: 0x0003C530 File Offset: 0x0003A730
		public static void WriteLine(long value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000E04 RID: 3588 RVA: 0x0003C540 File Offset: 0x0003A740
		public static void WriteLine(object value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000E05 RID: 3589 RVA: 0x0003C550 File Offset: 0x0003A750
		public static void WriteLine(float value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000E06 RID: 3590 RVA: 0x0003C560 File Offset: 0x0003A760
		public static void WriteLine(string value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000E07 RID: 3591 RVA: 0x0003C570 File Offset: 0x0003A770
		[CLSCompliant(false)]
		public static void WriteLine(uint value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000E08 RID: 3592 RVA: 0x0003C580 File Offset: 0x0003A780
		[CLSCompliant(false)]
		public static void WriteLine(ulong value)
		{
			Console.stdout.WriteLine(value);
		}

		// Token: 0x06000E09 RID: 3593 RVA: 0x0003C590 File Offset: 0x0003A790
		public static void WriteLine(string format, object arg0)
		{
			Console.stdout.WriteLine(format, arg0);
		}

		// Token: 0x06000E0A RID: 3594 RVA: 0x0003C5A0 File Offset: 0x0003A7A0
		public static void WriteLine(string format, params object[] arg)
		{
			Console.stdout.WriteLine(format, arg);
		}

		// Token: 0x06000E0B RID: 3595 RVA: 0x0003C5B0 File Offset: 0x0003A7B0
		public static void WriteLine(char[] buffer, int index, int count)
		{
			Console.stdout.WriteLine(buffer, index, count);
		}

		// Token: 0x06000E0C RID: 3596 RVA: 0x0003C5C0 File Offset: 0x0003A7C0
		public static void WriteLine(string format, object arg0, object arg1)
		{
			Console.stdout.WriteLine(format, arg0, arg1);
		}

		// Token: 0x06000E0D RID: 3597 RVA: 0x0003C5D0 File Offset: 0x0003A7D0
		public static void WriteLine(string format, object arg0, object arg1, object arg2)
		{
			Console.stdout.WriteLine(format, arg0, arg1, arg2);
		}

		// Token: 0x06000E0E RID: 3598 RVA: 0x0003C5E0 File Offset: 0x0003A7E0
		[CLSCompliant(false)]
		public static void WriteLine(string format, object arg0, object arg1, object arg2, object arg3, __arglist)
		{
			ArgIterator argIterator = new ArgIterator(__arglist);
			int remainingCount = argIterator.GetRemainingCount();
			object[] array = new object[remainingCount + 4];
			array[0] = arg0;
			array[1] = arg1;
			array[2] = arg2;
			array[3] = arg3;
			for (int i = 0; i < remainingCount; i++)
			{
				TypedReference nextArg = argIterator.GetNextArg();
				array[i + 4] = TypedReference.ToObject(nextArg);
			}
			Console.stdout.WriteLine(string.Format(format, array));
		}

		// Token: 0x06000E0F RID: 3599 RVA: 0x0003C650 File Offset: 0x0003A850
		public static int Read()
		{
			if (Console.stdin is CStreamReader && ConsoleDriver.IsConsole)
			{
				return ConsoleDriver.Read();
			}
			return Console.stdin.Read();
		}

		// Token: 0x06000E10 RID: 3600 RVA: 0x0003C67C File Offset: 0x0003A87C
		public static string ReadLine()
		{
			if (Console.stdin is CStreamReader && ConsoleDriver.IsConsole)
			{
				return ConsoleDriver.ReadLine();
			}
			return Console.stdin.ReadLine();
		}

		// Token: 0x17000203 RID: 515
		// (get) Token: 0x06000E11 RID: 3601 RVA: 0x0003C6A8 File Offset: 0x0003A8A8
		// (set) Token: 0x06000E12 RID: 3602 RVA: 0x0003C6B0 File Offset: 0x0003A8B0
		public static Encoding InputEncoding
		{
			get
			{
				return Console.inputEncoding;
			}
			set
			{
				Console.inputEncoding = value;
				Console.SetEncodings(Console.inputEncoding, Console.outputEncoding);
			}
		}

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x06000E13 RID: 3603 RVA: 0x0003C6C8 File Offset: 0x0003A8C8
		// (set) Token: 0x06000E14 RID: 3604 RVA: 0x0003C6D0 File Offset: 0x0003A8D0
		public static Encoding OutputEncoding
		{
			get
			{
				return Console.outputEncoding;
			}
			set
			{
				Console.outputEncoding = value;
				Console.SetEncodings(Console.inputEncoding, Console.outputEncoding);
			}
		}

		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000E15 RID: 3605 RVA: 0x0003C6E8 File Offset: 0x0003A8E8
		// (set) Token: 0x06000E16 RID: 3606 RVA: 0x0003C6F0 File Offset: 0x0003A8F0
		public static ConsoleColor BackgroundColor
		{
			get
			{
				return ConsoleDriver.BackgroundColor;
			}
			set
			{
				ConsoleDriver.BackgroundColor = value;
			}
		}

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06000E17 RID: 3607 RVA: 0x0003C6F8 File Offset: 0x0003A8F8
		// (set) Token: 0x06000E18 RID: 3608 RVA: 0x0003C700 File Offset: 0x0003A900
		public static int BufferHeight
		{
			get
			{
				return ConsoleDriver.BufferHeight;
			}
			[MonoLimitation("Implemented only on Windows")]
			set
			{
				ConsoleDriver.BufferHeight = value;
			}
		}

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x06000E19 RID: 3609 RVA: 0x0003C708 File Offset: 0x0003A908
		// (set) Token: 0x06000E1A RID: 3610 RVA: 0x0003C710 File Offset: 0x0003A910
		public static int BufferWidth
		{
			get
			{
				return ConsoleDriver.BufferWidth;
			}
			[MonoLimitation("Implemented only on Windows")]
			set
			{
				ConsoleDriver.BufferWidth = value;
			}
		}

		// Token: 0x17000208 RID: 520
		// (get) Token: 0x06000E1B RID: 3611 RVA: 0x0003C718 File Offset: 0x0003A918
		[MonoLimitation("Implemented only on Windows")]
		public static bool CapsLock
		{
			get
			{
				return ConsoleDriver.CapsLock;
			}
		}

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x06000E1C RID: 3612 RVA: 0x0003C720 File Offset: 0x0003A920
		// (set) Token: 0x06000E1D RID: 3613 RVA: 0x0003C728 File Offset: 0x0003A928
		public static int CursorLeft
		{
			get
			{
				return ConsoleDriver.CursorLeft;
			}
			set
			{
				ConsoleDriver.CursorLeft = value;
			}
		}

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06000E1E RID: 3614 RVA: 0x0003C730 File Offset: 0x0003A930
		// (set) Token: 0x06000E1F RID: 3615 RVA: 0x0003C738 File Offset: 0x0003A938
		public static int CursorTop
		{
			get
			{
				return ConsoleDriver.CursorTop;
			}
			set
			{
				ConsoleDriver.CursorTop = value;
			}
		}

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x06000E20 RID: 3616 RVA: 0x0003C740 File Offset: 0x0003A940
		// (set) Token: 0x06000E21 RID: 3617 RVA: 0x0003C748 File Offset: 0x0003A948
		public static int CursorSize
		{
			get
			{
				return ConsoleDriver.CursorSize;
			}
			set
			{
				ConsoleDriver.CursorSize = value;
			}
		}

		// Token: 0x1700020C RID: 524
		// (get) Token: 0x06000E22 RID: 3618 RVA: 0x0003C750 File Offset: 0x0003A950
		// (set) Token: 0x06000E23 RID: 3619 RVA: 0x0003C758 File Offset: 0x0003A958
		public static bool CursorVisible
		{
			get
			{
				return ConsoleDriver.CursorVisible;
			}
			set
			{
				ConsoleDriver.CursorVisible = value;
			}
		}

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x06000E24 RID: 3620 RVA: 0x0003C760 File Offset: 0x0003A960
		// (set) Token: 0x06000E25 RID: 3621 RVA: 0x0003C768 File Offset: 0x0003A968
		public static ConsoleColor ForegroundColor
		{
			get
			{
				return ConsoleDriver.ForegroundColor;
			}
			set
			{
				ConsoleDriver.ForegroundColor = value;
			}
		}

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000E26 RID: 3622 RVA: 0x0003C770 File Offset: 0x0003A970
		public static bool KeyAvailable
		{
			get
			{
				return ConsoleDriver.KeyAvailable;
			}
		}

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x06000E27 RID: 3623 RVA: 0x0003C778 File Offset: 0x0003A978
		public static int LargestWindowHeight
		{
			get
			{
				return ConsoleDriver.LargestWindowHeight;
			}
		}

		// Token: 0x17000210 RID: 528
		// (get) Token: 0x06000E28 RID: 3624 RVA: 0x0003C780 File Offset: 0x0003A980
		public static int LargestWindowWidth
		{
			get
			{
				return ConsoleDriver.LargestWindowWidth;
			}
		}

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x06000E29 RID: 3625 RVA: 0x0003C788 File Offset: 0x0003A988
		[MonoLimitation("Only works on windows")]
		public static bool NumberLock
		{
			get
			{
				return ConsoleDriver.NumberLock;
			}
		}

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000E2A RID: 3626 RVA: 0x0003C790 File Offset: 0x0003A990
		// (set) Token: 0x06000E2B RID: 3627 RVA: 0x0003C798 File Offset: 0x0003A998
		public static string Title
		{
			get
			{
				return ConsoleDriver.Title;
			}
			set
			{
				ConsoleDriver.Title = value;
			}
		}

		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06000E2C RID: 3628 RVA: 0x0003C7A0 File Offset: 0x0003A9A0
		// (set) Token: 0x06000E2D RID: 3629 RVA: 0x0003C7A8 File Offset: 0x0003A9A8
		public static bool TreatControlCAsInput
		{
			get
			{
				return ConsoleDriver.TreatControlCAsInput;
			}
			set
			{
				ConsoleDriver.TreatControlCAsInput = value;
			}
		}

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06000E2E RID: 3630 RVA: 0x0003C7B0 File Offset: 0x0003A9B0
		// (set) Token: 0x06000E2F RID: 3631 RVA: 0x0003C7B8 File Offset: 0x0003A9B8
		[MonoLimitation("Only works on windows")]
		public static int WindowHeight
		{
			get
			{
				return ConsoleDriver.WindowHeight;
			}
			set
			{
				ConsoleDriver.WindowHeight = value;
			}
		}

		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000E30 RID: 3632 RVA: 0x0003C7C0 File Offset: 0x0003A9C0
		// (set) Token: 0x06000E31 RID: 3633 RVA: 0x0003C7C8 File Offset: 0x0003A9C8
		[MonoLimitation("Only works on windows")]
		public static int WindowLeft
		{
			get
			{
				return ConsoleDriver.WindowLeft;
			}
			set
			{
				ConsoleDriver.WindowLeft = value;
			}
		}

		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06000E32 RID: 3634 RVA: 0x0003C7D0 File Offset: 0x0003A9D0
		// (set) Token: 0x06000E33 RID: 3635 RVA: 0x0003C7D8 File Offset: 0x0003A9D8
		[MonoLimitation("Only works on windows")]
		public static int WindowTop
		{
			get
			{
				return ConsoleDriver.WindowTop;
			}
			set
			{
				ConsoleDriver.WindowTop = value;
			}
		}

		// Token: 0x17000217 RID: 535
		// (get) Token: 0x06000E34 RID: 3636 RVA: 0x0003C7E0 File Offset: 0x0003A9E0
		// (set) Token: 0x06000E35 RID: 3637 RVA: 0x0003C7E8 File Offset: 0x0003A9E8
		[MonoLimitation("Only works on windows")]
		public static int WindowWidth
		{
			get
			{
				return ConsoleDriver.WindowWidth;
			}
			set
			{
				ConsoleDriver.WindowWidth = value;
			}
		}

		// Token: 0x06000E36 RID: 3638 RVA: 0x0003C7F0 File Offset: 0x0003A9F0
		public static void Beep()
		{
			Console.Beep(1000, 500);
		}

		// Token: 0x06000E37 RID: 3639 RVA: 0x0003C804 File Offset: 0x0003AA04
		public static void Beep(int frequency, int duration)
		{
			if (frequency < 37 || frequency > 32767)
			{
				throw new ArgumentOutOfRangeException("frequency");
			}
			if (duration <= 0)
			{
				throw new ArgumentOutOfRangeException("duration");
			}
			ConsoleDriver.Beep(frequency, duration);
		}

		// Token: 0x06000E38 RID: 3640 RVA: 0x0003C840 File Offset: 0x0003AA40
		public static void Clear()
		{
			ConsoleDriver.Clear();
		}

		// Token: 0x06000E39 RID: 3641 RVA: 0x0003C848 File Offset: 0x0003AA48
		[MonoLimitation("Implemented only on Windows")]
		public static void MoveBufferArea(int sourceLeft, int sourceTop, int sourceWidth, int sourceHeight, int targetLeft, int targetTop)
		{
			ConsoleDriver.MoveBufferArea(sourceLeft, sourceTop, sourceWidth, sourceHeight, targetLeft, targetTop);
		}

		// Token: 0x06000E3A RID: 3642 RVA: 0x0003C858 File Offset: 0x0003AA58
		[MonoLimitation("Implemented only on Windows")]
		public static void MoveBufferArea(int sourceLeft, int sourceTop, int sourceWidth, int sourceHeight, int targetLeft, int targetTop, char sourceChar, ConsoleColor sourceForeColor, ConsoleColor sourceBackColor)
		{
			ConsoleDriver.MoveBufferArea(sourceLeft, sourceTop, sourceWidth, sourceHeight, targetLeft, targetTop, sourceChar, sourceForeColor, sourceBackColor);
		}

		// Token: 0x06000E3B RID: 3643 RVA: 0x0003C878 File Offset: 0x0003AA78
		public static ConsoleKeyInfo ReadKey()
		{
			return Console.ReadKey(false);
		}

		// Token: 0x06000E3C RID: 3644 RVA: 0x0003C880 File Offset: 0x0003AA80
		public static ConsoleKeyInfo ReadKey(bool intercept)
		{
			return ConsoleDriver.ReadKey(intercept);
		}

		// Token: 0x06000E3D RID: 3645 RVA: 0x0003C888 File Offset: 0x0003AA88
		public static void ResetColor()
		{
			ConsoleDriver.ResetColor();
		}

		// Token: 0x06000E3E RID: 3646 RVA: 0x0003C890 File Offset: 0x0003AA90
		[MonoLimitation("Only works on windows")]
		public static void SetBufferSize(int width, int height)
		{
			ConsoleDriver.SetBufferSize(width, height);
		}

		// Token: 0x06000E3F RID: 3647 RVA: 0x0003C89C File Offset: 0x0003AA9C
		public static void SetCursorPosition(int left, int top)
		{
			ConsoleDriver.SetCursorPosition(left, top);
		}

		// Token: 0x06000E40 RID: 3648 RVA: 0x0003C8A8 File Offset: 0x0003AAA8
		public static void SetWindowPosition(int left, int top)
		{
			ConsoleDriver.SetWindowPosition(left, top);
		}

		// Token: 0x06000E41 RID: 3649 RVA: 0x0003C8B4 File Offset: 0x0003AAB4
		public static void SetWindowSize(int width, int height)
		{
			ConsoleDriver.SetWindowSize(width, height);
		}

		// Token: 0x06000E42 RID: 3650 RVA: 0x0003C8C0 File Offset: 0x0003AAC0
		internal static void DoConsoleCancelEvent()
		{
			bool flag = true;
			if (Console.cancel_event != null)
			{
				ConsoleCancelEventArgs consoleCancelEventArgs = new ConsoleCancelEventArgs(ConsoleSpecialKey.ControlC);
				Delegate[] invocationList = Console.cancel_event.GetInvocationList();
				foreach (ConsoleCancelEventHandler consoleCancelEventHandler in invocationList)
				{
					try
					{
						consoleCancelEventHandler(null, consoleCancelEventArgs);
					}
					catch
					{
					}
				}
				flag = !consoleCancelEventArgs.Cancel;
			}
			if (flag)
			{
				Environment.Exit(58);
			}
		}

		// Token: 0x040003C5 RID: 965
		internal static TextWriter stdout;

		// Token: 0x040003C6 RID: 966
		private static TextWriter stderr;

		// Token: 0x040003C7 RID: 967
		private static TextReader stdin;

		// Token: 0x040003C8 RID: 968
		private static Encoding inputEncoding;

		// Token: 0x040003C9 RID: 969
		private static Encoding outputEncoding;

		// Token: 0x040003CA RID: 970
		private static ConsoleCancelEventHandler cancel_event;

		// Token: 0x040003CB RID: 971
		private static readonly Console.InternalCancelHandler cancel_handler = new Console.InternalCancelHandler(Console.DoConsoleCancelEvent);

		// Token: 0x02000110 RID: 272
		private class WindowsConsole
		{
			// Token: 0x06000E44 RID: 3652
			[DllImport("kernel32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
			private static extern int GetConsoleCP();

			// Token: 0x06000E45 RID: 3653
			[DllImport("kernel32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
			private static extern int GetConsoleOutputCP();

			// Token: 0x06000E46 RID: 3654 RVA: 0x0003C960 File Offset: 0x0003AB60
			[MethodImpl(MethodImplOptions.NoInlining)]
			public static int GetInputCodePage()
			{
				return Console.WindowsConsole.GetConsoleCP();
			}

			// Token: 0x06000E47 RID: 3655 RVA: 0x0003C968 File Offset: 0x0003AB68
			[MethodImpl(MethodImplOptions.NoInlining)]
			public static int GetOutputCodePage()
			{
				return Console.WindowsConsole.GetConsoleOutputCP();
			}
		}

		// Token: 0x020006D9 RID: 1753
		// (Invoke) Token: 0x06004358 RID: 17240
		private delegate void InternalCancelHandler();
	}
}
