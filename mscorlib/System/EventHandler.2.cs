﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x020006EC RID: 1772
	// (Invoke) Token: 0x060043A4 RID: 17316
	[ComVisible(true)]
	[Serializable]
	public delegate void EventHandler(object sender, EventArgs e);
}
