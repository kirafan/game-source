﻿using System;

namespace System
{
	// Token: 0x0200013B RID: 315
	[Serializable]
	public enum GCCollectionMode
	{
		// Token: 0x04000501 RID: 1281
		Default,
		// Token: 0x04000502 RID: 1282
		Forced,
		// Token: 0x04000503 RID: 1283
		Optimized
	}
}
