﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000124 RID: 292
	[ComVisible(true)]
	[Serializable]
	public sealed class DBNull : IConvertible, ISerializable
	{
		// Token: 0x060010B9 RID: 4281 RVA: 0x00044D24 File Offset: 0x00042F24
		private DBNull()
		{
		}

		// Token: 0x060010BA RID: 4282 RVA: 0x00044D2C File Offset: 0x00042F2C
		private DBNull(SerializationInfo info, StreamingContext context)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060010BC RID: 4284 RVA: 0x00044D48 File Offset: 0x00042F48
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010BD RID: 4285 RVA: 0x00044D50 File Offset: 0x00042F50
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010BE RID: 4286 RVA: 0x00044D58 File Offset: 0x00042F58
		char IConvertible.ToChar(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010BF RID: 4287 RVA: 0x00044D60 File Offset: 0x00042F60
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010C0 RID: 4288 RVA: 0x00044D68 File Offset: 0x00042F68
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010C1 RID: 4289 RVA: 0x00044D70 File Offset: 0x00042F70
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010C2 RID: 4290 RVA: 0x00044D78 File Offset: 0x00042F78
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010C3 RID: 4291 RVA: 0x00044D80 File Offset: 0x00042F80
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010C4 RID: 4292 RVA: 0x00044D88 File Offset: 0x00042F88
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010C5 RID: 4293 RVA: 0x00044D90 File Offset: 0x00042F90
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010C6 RID: 4294 RVA: 0x00044D98 File Offset: 0x00042F98
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010C7 RID: 4295 RVA: 0x00044DA0 File Offset: 0x00042FA0
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == typeof(string))
			{
				return string.Empty;
			}
			if (targetType == typeof(DBNull))
			{
				return this;
			}
			throw new InvalidCastException();
		}

		// Token: 0x060010C8 RID: 4296 RVA: 0x00044DD0 File Offset: 0x00042FD0
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010C9 RID: 4297 RVA: 0x00044DD8 File Offset: 0x00042FD8
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010CA RID: 4298 RVA: 0x00044DE0 File Offset: 0x00042FE0
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060010CB RID: 4299 RVA: 0x00044DE8 File Offset: 0x00042FE8
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			UnitySerializationHolder.GetDBNullData(this, info, context);
		}

		// Token: 0x060010CC RID: 4300 RVA: 0x00044DF4 File Offset: 0x00042FF4
		public TypeCode GetTypeCode()
		{
			return TypeCode.DBNull;
		}

		// Token: 0x060010CD RID: 4301 RVA: 0x00044DF8 File Offset: 0x00042FF8
		public override string ToString()
		{
			return string.Empty;
		}

		// Token: 0x060010CE RID: 4302 RVA: 0x00044E00 File Offset: 0x00043000
		public string ToString(IFormatProvider provider)
		{
			return string.Empty;
		}

		// Token: 0x040004C2 RID: 1218
		public static readonly DBNull Value = new DBNull();
	}
}
