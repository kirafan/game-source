﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200018F RID: 399
	[ComVisible(true)]
	[Serializable]
	public class TypeLoadException : SystemException
	{
		// Token: 0x06001460 RID: 5216 RVA: 0x00052054 File Offset: 0x00050254
		public TypeLoadException() : base(Locale.GetText("A type load exception has occurred."))
		{
			base.HResult = -2146233054;
		}

		// Token: 0x06001461 RID: 5217 RVA: 0x00052074 File Offset: 0x00050274
		public TypeLoadException(string message) : base(message)
		{
			base.HResult = -2146233054;
		}

		// Token: 0x06001462 RID: 5218 RVA: 0x00052088 File Offset: 0x00050288
		public TypeLoadException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233054;
		}

		// Token: 0x06001463 RID: 5219 RVA: 0x000520A0 File Offset: 0x000502A0
		internal TypeLoadException(string className, string assemblyName) : this()
		{
			this.className = className;
			this.assemblyName = assemblyName;
		}

		// Token: 0x06001464 RID: 5220 RVA: 0x000520B8 File Offset: 0x000502B8
		protected TypeLoadException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.className = info.GetString("TypeLoadClassName");
			this.assemblyName = info.GetString("TypeLoadAssemblyName");
		}

		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06001465 RID: 5221 RVA: 0x000520F8 File Offset: 0x000502F8
		public override string Message
		{
			get
			{
				if (this.className == null)
				{
					return base.Message;
				}
				if (this.assemblyName != null && this.assemblyName != string.Empty)
				{
					return string.Format("Could not load type '{0}' from assembly '{1}'.", this.className, this.assemblyName);
				}
				return string.Format("Could not load type '{0}'.", this.className);
			}
		}

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x06001466 RID: 5222 RVA: 0x00052160 File Offset: 0x00050360
		public string TypeName
		{
			get
			{
				if (this.className == null)
				{
					return string.Empty;
				}
				return this.className;
			}
		}

		// Token: 0x06001467 RID: 5223 RVA: 0x0005217C File Offset: 0x0005037C
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			base.GetObjectData(info, context);
			info.AddValue("TypeLoadClassName", this.className, typeof(string));
			info.AddValue("TypeLoadAssemblyName", this.assemblyName, typeof(string));
			info.AddValue("TypeLoadMessageArg", string.Empty, typeof(string));
			info.AddValue("TypeLoadResourceID", 0, typeof(int));
		}

		// Token: 0x040007FF RID: 2047
		private const int Result = -2146233054;

		// Token: 0x04000800 RID: 2048
		private string className;

		// Token: 0x04000801 RID: 2049
		private string assemblyName;
	}
}
