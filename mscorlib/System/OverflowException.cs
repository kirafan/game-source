﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000169 RID: 361
	[ComVisible(true)]
	[Serializable]
	public class OverflowException : ArithmeticException
	{
		// Token: 0x06001351 RID: 4945 RVA: 0x0004D5FC File Offset: 0x0004B7FC
		public OverflowException() : base(Locale.GetText("Number overflow."))
		{
			base.HResult = -2146233066;
		}

		// Token: 0x06001352 RID: 4946 RVA: 0x0004D61C File Offset: 0x0004B81C
		public OverflowException(string message) : base(message)
		{
			base.HResult = -2146233066;
		}

		// Token: 0x06001353 RID: 4947 RVA: 0x0004D630 File Offset: 0x0004B830
		public OverflowException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233066;
		}

		// Token: 0x06001354 RID: 4948 RVA: 0x0004D648 File Offset: 0x0004B848
		protected OverflowException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x04000594 RID: 1428
		private const int Result = -2146233066;
	}
}
