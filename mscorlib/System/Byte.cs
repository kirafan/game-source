﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000013 RID: 19
	[ComVisible(true)]
	[Serializable]
	public struct Byte : IFormattable, IConvertible, IComparable, IComparable<byte>, IEquatable<byte>
	{
		// Token: 0x060000F1 RID: 241 RVA: 0x0000525C File Offset: 0x0000345C
		object IConvertible.ToType(Type targetType, IFormatProvider provider)
		{
			if (targetType == null)
			{
				throw new ArgumentNullException("targetType");
			}
			return Convert.ToType(this, targetType, provider, false);
		}

		// Token: 0x060000F2 RID: 242 RVA: 0x0000528C File Offset: 0x0000348C
		bool IConvertible.ToBoolean(IFormatProvider provider)
		{
			return Convert.ToBoolean(this);
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x00005298 File Offset: 0x00003498
		byte IConvertible.ToByte(IFormatProvider provider)
		{
			return this;
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x0000529C File Offset: 0x0000349C
		char IConvertible.ToChar(IFormatProvider provider)
		{
			return Convert.ToChar(this);
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x000052A8 File Offset: 0x000034A8
		DateTime IConvertible.ToDateTime(IFormatProvider provider)
		{
			throw new InvalidCastException();
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x000052B0 File Offset: 0x000034B0
		decimal IConvertible.ToDecimal(IFormatProvider provider)
		{
			return Convert.ToDecimal(this);
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x000052BC File Offset: 0x000034BC
		double IConvertible.ToDouble(IFormatProvider provider)
		{
			return Convert.ToDouble(this);
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x000052C8 File Offset: 0x000034C8
		short IConvertible.ToInt16(IFormatProvider provider)
		{
			return Convert.ToInt16(this);
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x000052D4 File Offset: 0x000034D4
		int IConvertible.ToInt32(IFormatProvider provider)
		{
			return Convert.ToInt32(this);
		}

		// Token: 0x060000FA RID: 250 RVA: 0x000052E0 File Offset: 0x000034E0
		long IConvertible.ToInt64(IFormatProvider provider)
		{
			return Convert.ToInt64(this);
		}

		// Token: 0x060000FB RID: 251 RVA: 0x000052EC File Offset: 0x000034EC
		sbyte IConvertible.ToSByte(IFormatProvider provider)
		{
			return Convert.ToSByte(this);
		}

		// Token: 0x060000FC RID: 252 RVA: 0x000052F8 File Offset: 0x000034F8
		float IConvertible.ToSingle(IFormatProvider provider)
		{
			return Convert.ToSingle(this);
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00005304 File Offset: 0x00003504
		ushort IConvertible.ToUInt16(IFormatProvider provider)
		{
			return Convert.ToUInt16(this);
		}

		// Token: 0x060000FE RID: 254 RVA: 0x00005310 File Offset: 0x00003510
		uint IConvertible.ToUInt32(IFormatProvider provider)
		{
			return Convert.ToUInt32(this);
		}

		// Token: 0x060000FF RID: 255 RVA: 0x0000531C File Offset: 0x0000351C
		ulong IConvertible.ToUInt64(IFormatProvider provider)
		{
			return Convert.ToUInt64(this);
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00005328 File Offset: 0x00003528
		public int CompareTo(object value)
		{
			if (value == null)
			{
				return 1;
			}
			if (!(value is byte))
			{
				throw new ArgumentException(Locale.GetText("Value is not a System.Byte."));
			}
			byte b = (byte)value;
			if (this == b)
			{
				return 0;
			}
			if (this > b)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06000101 RID: 257 RVA: 0x00005374 File Offset: 0x00003574
		public override bool Equals(object obj)
		{
			return obj is byte && (byte)obj == this;
		}

		// Token: 0x06000102 RID: 258 RVA: 0x00005390 File Offset: 0x00003590
		public override int GetHashCode()
		{
			return (int)this;
		}

		// Token: 0x06000103 RID: 259 RVA: 0x00005394 File Offset: 0x00003594
		public int CompareTo(byte value)
		{
			if (this == value)
			{
				return 0;
			}
			if (this > value)
			{
				return 1;
			}
			return -1;
		}

		// Token: 0x06000104 RID: 260 RVA: 0x000053AC File Offset: 0x000035AC
		public bool Equals(byte obj)
		{
			return this == obj;
		}

		// Token: 0x06000105 RID: 261 RVA: 0x000053B4 File Offset: 0x000035B4
		public static byte Parse(string s, IFormatProvider provider)
		{
			return byte.Parse(s, NumberStyles.Integer, provider);
		}

		// Token: 0x06000106 RID: 262 RVA: 0x000053C0 File Offset: 0x000035C0
		public static byte Parse(string s, NumberStyles style)
		{
			return byte.Parse(s, style, null);
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000053CC File Offset: 0x000035CC
		public static byte Parse(string s, NumberStyles style, IFormatProvider provider)
		{
			uint num = uint.Parse(s, style, provider);
			if (num > 255U)
			{
				throw new OverflowException(Locale.GetText("Value too large."));
			}
			return (byte)num;
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00005400 File Offset: 0x00003600
		public static byte Parse(string s)
		{
			return byte.Parse(s, NumberStyles.Integer, null);
		}

		// Token: 0x06000109 RID: 265 RVA: 0x0000540C File Offset: 0x0000360C
		public static bool TryParse(string s, out byte result)
		{
			return byte.TryParse(s, NumberStyles.Integer, null, out result);
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00005418 File Offset: 0x00003618
		public static bool TryParse(string s, NumberStyles style, IFormatProvider provider, out byte result)
		{
			result = 0;
			uint num;
			if (!uint.TryParse(s, style, provider, out num))
			{
				return false;
			}
			if (num > 255U)
			{
				return false;
			}
			result = (byte)num;
			return true;
		}

		// Token: 0x0600010B RID: 267 RVA: 0x0000544C File Offset: 0x0000364C
		public override string ToString()
		{
			return NumberFormatter.NumberToString((int)this, null);
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00005458 File Offset: 0x00003658
		public string ToString(string format)
		{
			return this.ToString(format, null);
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00005464 File Offset: 0x00003664
		public string ToString(IFormatProvider provider)
		{
			return NumberFormatter.NumberToString((int)this, provider);
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00005470 File Offset: 0x00003670
		public string ToString(string format, IFormatProvider provider)
		{
			return NumberFormatter.NumberToString(format, this, provider);
		}

		// Token: 0x0600010F RID: 271 RVA: 0x0000547C File Offset: 0x0000367C
		public TypeCode GetTypeCode()
		{
			return TypeCode.Byte;
		}

		// Token: 0x04000012 RID: 18
		public const byte MinValue = 0;

		// Token: 0x04000013 RID: 19
		public const byte MaxValue = 255;

		// Token: 0x04000014 RID: 20
		internal byte m_value;
	}
}
