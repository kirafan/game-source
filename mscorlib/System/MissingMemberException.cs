﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000152 RID: 338
	[ComVisible(true)]
	[Serializable]
	public class MissingMemberException : MemberAccessException
	{
		// Token: 0x06001226 RID: 4646 RVA: 0x00047D68 File Offset: 0x00045F68
		public MissingMemberException() : base(Locale.GetText("Cannot find the requested class member."))
		{
			base.HResult = -2146233070;
		}

		// Token: 0x06001227 RID: 4647 RVA: 0x00047D88 File Offset: 0x00045F88
		public MissingMemberException(string message) : base(message)
		{
			base.HResult = -2146233070;
		}

		// Token: 0x06001228 RID: 4648 RVA: 0x00047D9C File Offset: 0x00045F9C
		public MissingMemberException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233070;
		}

		// Token: 0x06001229 RID: 4649 RVA: 0x00047DB4 File Offset: 0x00045FB4
		protected MissingMemberException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.ClassName = info.GetString("MMClassName");
			this.MemberName = info.GetString("MMMemberName");
			this.Signature = (byte[])info.GetValue("MMSignature", typeof(byte[]));
		}

		// Token: 0x0600122A RID: 4650 RVA: 0x00047E0C File Offset: 0x0004600C
		public MissingMemberException(string className, string memberName)
		{
			this.ClassName = className;
			this.MemberName = memberName;
			base.HResult = -2146233070;
		}

		// Token: 0x0600122B RID: 4651 RVA: 0x00047E30 File Offset: 0x00046030
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("MMClassName", this.ClassName);
			info.AddValue("MMMemberName", this.MemberName);
			info.AddValue("MMSignature", this.Signature);
		}

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x0600122C RID: 4652 RVA: 0x00047E78 File Offset: 0x00046078
		public override string Message
		{
			get
			{
				if (this.ClassName == null)
				{
					return base.Message;
				}
				string text = Locale.GetText("Member {0}.{1} not found.");
				return string.Format(text, this.ClassName, this.MemberName);
			}
		}

		// Token: 0x04000533 RID: 1331
		private const int Result = -2146233070;

		// Token: 0x04000534 RID: 1332
		protected string ClassName;

		// Token: 0x04000535 RID: 1333
		protected string MemberName;

		// Token: 0x04000536 RID: 1334
		protected byte[] Signature;
	}
}
