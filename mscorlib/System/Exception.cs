﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Text;

namespace System
{
	// Token: 0x02000039 RID: 57
	[ComDefaultInterface(typeof(_Exception))]
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	[Serializable]
	public class Exception : ISerializable, _Exception
	{
		// Token: 0x06000613 RID: 1555 RVA: 0x00013E48 File Offset: 0x00012048
		public Exception()
		{
		}

		// Token: 0x06000614 RID: 1556 RVA: 0x00013E5C File Offset: 0x0001205C
		public Exception(string message)
		{
			this.message = message;
		}

		// Token: 0x06000615 RID: 1557 RVA: 0x00013E78 File Offset: 0x00012078
		protected Exception(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			this.class_name = info.GetString("ClassName");
			this.message = info.GetString("Message");
			this.help_link = info.GetString("HelpURL");
			this.stack_trace = info.GetString("StackTraceString");
			this._remoteStackTraceString = info.GetString("RemoteStackTraceString");
			this.remote_stack_index = info.GetInt32("RemoteStackIndex");
			this.hresult = info.GetInt32("HResult");
			this.source = info.GetString("Source");
			this.inner_exception = (Exception)info.GetValue("InnerException", typeof(Exception));
			try
			{
				this._data = (IDictionary)info.GetValue("Data", typeof(IDictionary));
			}
			catch (SerializationException)
			{
			}
		}

		// Token: 0x06000616 RID: 1558 RVA: 0x00013F98 File Offset: 0x00012198
		public Exception(string message, Exception innerException)
		{
			this.inner_exception = innerException;
			this.message = message;
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x06000617 RID: 1559 RVA: 0x00013FBC File Offset: 0x000121BC
		public Exception InnerException
		{
			get
			{
				return this.inner_exception;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x06000618 RID: 1560 RVA: 0x00013FC4 File Offset: 0x000121C4
		// (set) Token: 0x06000619 RID: 1561 RVA: 0x00013FCC File Offset: 0x000121CC
		public virtual string HelpLink
		{
			get
			{
				return this.help_link;
			}
			set
			{
				this.help_link = value;
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x0600061A RID: 1562 RVA: 0x00013FD8 File Offset: 0x000121D8
		// (set) Token: 0x0600061B RID: 1563 RVA: 0x00013FE0 File Offset: 0x000121E0
		protected int HResult
		{
			get
			{
				return this.hresult;
			}
			set
			{
				this.hresult = value;
			}
		}

		// Token: 0x0600061C RID: 1564 RVA: 0x00013FEC File Offset: 0x000121EC
		internal void SetMessage(string s)
		{
			this.message = s;
		}

		// Token: 0x0600061D RID: 1565 RVA: 0x00013FF8 File Offset: 0x000121F8
		internal void SetStackTrace(string s)
		{
			this.stack_trace = s;
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x0600061E RID: 1566 RVA: 0x00014004 File Offset: 0x00012204
		private string ClassName
		{
			get
			{
				if (this.class_name == null)
				{
					this.class_name = this.GetType().ToString();
				}
				return this.class_name;
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x0600061F RID: 1567 RVA: 0x00014034 File Offset: 0x00012234
		public virtual string Message
		{
			get
			{
				if (this.message == null)
				{
					this.message = string.Format(Locale.GetText("Exception of type '{0}' was thrown."), this.ClassName);
				}
				return this.message;
			}
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x06000620 RID: 1568 RVA: 0x00014070 File Offset: 0x00012270
		// (set) Token: 0x06000621 RID: 1569 RVA: 0x000140D8 File Offset: 0x000122D8
		public virtual string Source
		{
			get
			{
				if (this.source == null)
				{
					StackTrace stackTrace = new StackTrace(this, true);
					if (stackTrace.FrameCount > 0)
					{
						StackFrame frame = stackTrace.GetFrame(0);
						if (stackTrace != null)
						{
							MethodBase method = frame.GetMethod();
							if (method != null)
							{
								this.source = method.DeclaringType.Assembly.UnprotectedGetName().Name;
							}
						}
					}
				}
				return this.source;
			}
			set
			{
				this.source = value;
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000622 RID: 1570 RVA: 0x000140E4 File Offset: 0x000122E4
		public virtual string StackTrace
		{
			get
			{
				if (this.stack_trace == null)
				{
					if (this.trace_ips == null)
					{
						return null;
					}
					StackTrace stackTrace = new StackTrace(this, 0, true, true);
					StringBuilder stringBuilder = new StringBuilder();
					string value = string.Format("{0}  {1} ", Environment.NewLine, Locale.GetText("at"));
					string text = Locale.GetText("<unknown method>");
					for (int i = 0; i < stackTrace.FrameCount; i++)
					{
						StackFrame frame = stackTrace.GetFrame(i);
						if (i == 0)
						{
							stringBuilder.AppendFormat("  {0} ", Locale.GetText("at"));
						}
						else
						{
							stringBuilder.Append(value);
						}
						if (frame.GetMethod() == null)
						{
							string internalMethodName = frame.GetInternalMethodName();
							if (internalMethodName != null)
							{
								stringBuilder.Append(internalMethodName);
							}
							else
							{
								stringBuilder.AppendFormat("<0x{0:x5}> {1}", frame.GetNativeOffset(), text);
							}
						}
						else
						{
							this.GetFullNameForStackTrace(stringBuilder, frame.GetMethod());
							if (frame.GetILOffset() == -1)
							{
								stringBuilder.AppendFormat(" <0x{0:x5}> ", frame.GetNativeOffset());
							}
							else
							{
								stringBuilder.AppendFormat(" [0x{0:x5}] ", frame.GetILOffset());
							}
							stringBuilder.AppendFormat("in {0}:{1} ", frame.GetSecureFileName(), frame.GetFileLineNumber());
						}
					}
					this.stack_trace = stringBuilder.ToString();
				}
				return this.stack_trace;
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000623 RID: 1571 RVA: 0x00014258 File Offset: 0x00012458
		public MethodBase TargetSite
		{
			get
			{
				StackTrace stackTrace = new StackTrace(this, true);
				if (stackTrace.FrameCount > 0)
				{
					return stackTrace.GetFrame(0).GetMethod();
				}
				return null;
			}
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x06000624 RID: 1572 RVA: 0x00014288 File Offset: 0x00012488
		public virtual IDictionary Data
		{
			get
			{
				if (this._data == null)
				{
					this._data = new Hashtable();
				}
				return this._data;
			}
		}

		// Token: 0x06000625 RID: 1573 RVA: 0x000142A8 File Offset: 0x000124A8
		public virtual Exception GetBaseException()
		{
			for (Exception innerException = this.inner_exception; innerException != null; innerException = innerException.InnerException)
			{
				if (innerException.InnerException == null)
				{
					return innerException;
				}
			}
			return this;
		}

		// Token: 0x06000626 RID: 1574 RVA: 0x000142E4 File Offset: 0x000124E4
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("ClassName", this.ClassName);
			info.AddValue("Message", this.message);
			info.AddValue("InnerException", this.inner_exception);
			info.AddValue("HelpURL", this.help_link);
			info.AddValue("StackTraceString", this.StackTrace);
			info.AddValue("RemoteStackTraceString", this._remoteStackTraceString);
			info.AddValue("RemoteStackIndex", this.remote_stack_index);
			info.AddValue("HResult", this.hresult);
			info.AddValue("Source", this.Source);
			info.AddValue("ExceptionMethod", null);
			info.AddValue("Data", this._data, typeof(IDictionary));
		}

		// Token: 0x06000627 RID: 1575 RVA: 0x000143C4 File Offset: 0x000125C4
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder(this.ClassName);
			stringBuilder.Append(": ").Append(this.Message);
			if (this._remoteStackTraceString != null)
			{
				stringBuilder.Append(this._remoteStackTraceString);
			}
			if (this.inner_exception != null)
			{
				stringBuilder.Append(" ---> ").Append(this.inner_exception.ToString());
				stringBuilder.Append(Environment.NewLine);
				stringBuilder.Append(Locale.GetText("  --- End of inner exception stack trace ---"));
			}
			if (this.StackTrace != null)
			{
				stringBuilder.Append(Environment.NewLine).Append(this.StackTrace);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000628 RID: 1576 RVA: 0x00014478 File Offset: 0x00012678
		internal Exception FixRemotingException()
		{
			string format = (this.remote_stack_index != 0) ? Locale.GetText("{1}{0}{0}Exception rethrown at [{2}]: {0}") : Locale.GetText("{0}{0}Server stack trace: {0}{1}{0}{0}Exception rethrown at [{2}]: {0}");
			string remoteStackTraceString = string.Format(format, Environment.NewLine, this.StackTrace, this.remote_stack_index);
			this._remoteStackTraceString = remoteStackTraceString;
			this.remote_stack_index++;
			this.stack_trace = null;
			return this;
		}

		// Token: 0x06000629 RID: 1577 RVA: 0x000144E4 File Offset: 0x000126E4
		internal void GetFullNameForStackTrace(StringBuilder sb, MethodBase mi)
		{
			ParameterInfo[] parameters = mi.GetParameters();
			sb.Append(mi.DeclaringType.ToString());
			sb.Append(".");
			sb.Append(mi.Name);
			if (mi.IsGenericMethod)
			{
				Type[] genericArguments = mi.GetGenericArguments();
				sb.Append("[");
				for (int i = 0; i < genericArguments.Length; i++)
				{
					if (i > 0)
					{
						sb.Append(",");
					}
					sb.Append(genericArguments[i].Name);
				}
				sb.Append("]");
			}
			sb.Append(" (");
			for (int j = 0; j < parameters.Length; j++)
			{
				if (j > 0)
				{
					sb.Append(", ");
				}
				Type parameterType = parameters[j].ParameterType;
				if (parameterType.IsClass && parameterType.Namespace != string.Empty)
				{
					sb.Append(parameterType.Namespace);
					sb.Append(".");
				}
				sb.Append(parameterType.Name);
				if (parameters[j].Name != null)
				{
					sb.Append(" ");
					sb.Append(parameters[j].Name);
				}
			}
			sb.Append(")");
		}

		// Token: 0x0600062A RID: 1578 RVA: 0x00014640 File Offset: 0x00012840
		public new Type GetType()
		{
			return base.GetType();
		}

		// Token: 0x04000076 RID: 118
		private IntPtr[] trace_ips;

		// Token: 0x04000077 RID: 119
		private Exception inner_exception;

		// Token: 0x04000078 RID: 120
		internal string message;

		// Token: 0x04000079 RID: 121
		private string help_link;

		// Token: 0x0400007A RID: 122
		private string class_name;

		// Token: 0x0400007B RID: 123
		private string stack_trace;

		// Token: 0x0400007C RID: 124
		private string _remoteStackTraceString;

		// Token: 0x0400007D RID: 125
		private int remote_stack_index;

		// Token: 0x0400007E RID: 126
		internal int hresult = -2146233088;

		// Token: 0x0400007F RID: 127
		private string source;

		// Token: 0x04000080 RID: 128
		private IDictionary _data;
	}
}
