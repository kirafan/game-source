﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000141 RID: 321
	[ComVisible(true)]
	public interface ICustomFormatter
	{
		// Token: 0x060011B0 RID: 4528
		string Format(string format, object arg, IFormatProvider formatProvider);
	}
}
