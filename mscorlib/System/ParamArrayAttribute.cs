﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200003D RID: 61
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Parameter)]
	public sealed class ParamArrayAttribute : Attribute
	{
	}
}
