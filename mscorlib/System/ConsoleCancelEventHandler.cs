﻿using System;

namespace System
{
	// Token: 0x020006E9 RID: 1769
	// (Invoke) Token: 0x06004398 RID: 17304
	public delegate void ConsoleCancelEventHandler(object sender, ConsoleCancelEventArgs e);
}
