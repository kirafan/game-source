﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000151 RID: 337
	[ComVisible(true)]
	[Serializable]
	public class MissingFieldException : MissingMemberException
	{
		// Token: 0x06001220 RID: 4640 RVA: 0x00047CBC File Offset: 0x00045EBC
		public MissingFieldException() : base(Locale.GetText("Cannot find requested field."))
		{
			base.HResult = -2146233071;
		}

		// Token: 0x06001221 RID: 4641 RVA: 0x00047CDC File Offset: 0x00045EDC
		public MissingFieldException(string message) : base(message)
		{
			base.HResult = -2146233071;
		}

		// Token: 0x06001222 RID: 4642 RVA: 0x00047CF0 File Offset: 0x00045EF0
		protected MissingFieldException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06001223 RID: 4643 RVA: 0x00047CFC File Offset: 0x00045EFC
		public MissingFieldException(string message, Exception inner) : base(message, inner)
		{
			base.HResult = -2146233071;
		}

		// Token: 0x06001224 RID: 4644 RVA: 0x00047D14 File Offset: 0x00045F14
		public MissingFieldException(string className, string fieldName) : base(className, fieldName)
		{
			base.HResult = -2146233071;
		}

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06001225 RID: 4645 RVA: 0x00047D2C File Offset: 0x00045F2C
		public override string Message
		{
			get
			{
				if (this.ClassName == null)
				{
					return base.Message;
				}
				string text = Locale.GetText("Field '{0}.{1}' not found.");
				return string.Format(text, this.ClassName, this.MemberName);
			}
		}

		// Token: 0x04000532 RID: 1330
		private const int Result = -2146233071;
	}
}
