﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System
{
	// Token: 0x02000121 RID: 289
	[Serializable]
	[StructLayout(LayoutKind.Auto)]
	public struct DateTimeOffset : IFormattable, IComparable, ISerializable, IComparable<DateTimeOffset>, IEquatable<DateTimeOffset>, IDeserializationCallback
	{
		// Token: 0x0600105F RID: 4191 RVA: 0x00042CB0 File Offset: 0x00040EB0
		public DateTimeOffset(DateTime dateTime)
		{
			this.dt = dateTime;
			if (dateTime.Kind == DateTimeKind.Utc)
			{
				this.utc_offset = TimeSpan.Zero;
			}
			else
			{
				this.utc_offset = TimeZone.CurrentTimeZone.GetUtcOffset(dateTime);
			}
			if (this.UtcDateTime < DateTime.MinValue || this.UtcDateTime > DateTime.MaxValue)
			{
				throw new ArgumentOutOfRangeException("The UTC date and time that results from applying the offset is earlier than MinValue or later than MaxValue.");
			}
		}

		// Token: 0x06001060 RID: 4192 RVA: 0x00042D28 File Offset: 0x00040F28
		public DateTimeOffset(DateTime dateTime, TimeSpan offset)
		{
			if (dateTime.Kind == DateTimeKind.Utc && offset != TimeSpan.Zero)
			{
				throw new ArgumentException("dateTime.Kind equals Utc and offset does not equal zero.");
			}
			if (dateTime.Kind == DateTimeKind.Local && offset != TimeZone.CurrentTimeZone.GetUtcOffset(dateTime))
			{
				throw new ArgumentException("dateTime.Kind equals Local and offset does not equal the offset of the system's local time zone.");
			}
			if (offset.Ticks % 600000000L != 0L)
			{
				throw new ArgumentException("offset is not specified in whole minutes.");
			}
			if (offset < new TimeSpan(-14, 0, 0) || offset > new TimeSpan(14, 0, 0))
			{
				throw new ArgumentOutOfRangeException("offset is less than -14 hours or greater than 14 hours.");
			}
			this.dt = dateTime;
			this.utc_offset = offset;
			if (this.UtcDateTime < DateTime.MinValue || this.UtcDateTime > DateTime.MaxValue)
			{
				throw new ArgumentOutOfRangeException("The UtcDateTime property is earlier than MinValue or later than MaxValue.");
			}
		}

		// Token: 0x06001061 RID: 4193 RVA: 0x00042E20 File Offset: 0x00041020
		public DateTimeOffset(long ticks, TimeSpan offset)
		{
			this = new DateTimeOffset(new DateTime(ticks), offset);
		}

		// Token: 0x06001062 RID: 4194 RVA: 0x00042E30 File Offset: 0x00041030
		public DateTimeOffset(int year, int month, int day, int hour, int minute, int second, TimeSpan offset)
		{
			this = new DateTimeOffset(new DateTime(year, month, day, hour, minute, second), offset);
		}

		// Token: 0x06001063 RID: 4195 RVA: 0x00042E48 File Offset: 0x00041048
		public DateTimeOffset(int year, int month, int day, int hour, int minute, int second, int millisecond, TimeSpan offset)
		{
			this = new DateTimeOffset(new DateTime(year, month, day, hour, minute, second, millisecond), offset);
		}

		// Token: 0x06001064 RID: 4196 RVA: 0x00042E70 File Offset: 0x00041070
		public DateTimeOffset(int year, int month, int day, int hour, int minute, int second, int millisecond, Calendar calendar, TimeSpan offset)
		{
			this = new DateTimeOffset(new DateTime(year, month, day, hour, minute, second, millisecond, calendar), offset);
		}

		// Token: 0x06001065 RID: 4197 RVA: 0x00042E98 File Offset: 0x00041098
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		private DateTimeOffset(SerializationInfo info, StreamingContext context)
		{
			DateTime dateTime = (DateTime)info.GetValue("DateTime", typeof(DateTime));
			short @int = info.GetInt16("OffsetMinutes");
			this.utc_offset = TimeSpan.FromMinutes((double)@int);
			this.dt = dateTime.Add(this.utc_offset);
		}

		// Token: 0x06001067 RID: 4199 RVA: 0x00042F24 File Offset: 0x00041124
		int IComparable.CompareTo(object obj)
		{
			return this.CompareTo((DateTimeOffset)obj);
		}

		// Token: 0x06001068 RID: 4200 RVA: 0x00042F34 File Offset: 0x00041134
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			DateTime dateTime = new DateTime(this.dt.Ticks);
			DateTime value = dateTime.Subtract(this.utc_offset);
			info.AddValue("DateTime", value);
			info.AddValue("OffsetMinutes", (short)this.utc_offset.TotalMinutes);
		}

		// Token: 0x06001069 RID: 4201 RVA: 0x00042F98 File Offset: 0x00041198
		[MonoTODO]
		void IDeserializationCallback.OnDeserialization(object sender)
		{
		}

		// Token: 0x0600106A RID: 4202 RVA: 0x00042F9C File Offset: 0x0004119C
		public DateTimeOffset Add(TimeSpan timeSpan)
		{
			return new DateTimeOffset(this.dt.Add(timeSpan), this.utc_offset);
		}

		// Token: 0x0600106B RID: 4203 RVA: 0x00042FB8 File Offset: 0x000411B8
		public DateTimeOffset AddDays(double days)
		{
			return new DateTimeOffset(this.dt.AddDays(days), this.utc_offset);
		}

		// Token: 0x0600106C RID: 4204 RVA: 0x00042FD4 File Offset: 0x000411D4
		public DateTimeOffset AddHours(double hours)
		{
			return new DateTimeOffset(this.dt.AddHours(hours), this.utc_offset);
		}

		// Token: 0x0600106D RID: 4205 RVA: 0x00042FF0 File Offset: 0x000411F0
		public DateTimeOffset AddMilliseconds(double milliseconds)
		{
			return new DateTimeOffset(this.dt.AddMilliseconds(milliseconds), this.utc_offset);
		}

		// Token: 0x0600106E RID: 4206 RVA: 0x0004300C File Offset: 0x0004120C
		public DateTimeOffset AddMinutes(double minutes)
		{
			return new DateTimeOffset(this.dt.AddMinutes(minutes), this.utc_offset);
		}

		// Token: 0x0600106F RID: 4207 RVA: 0x00043028 File Offset: 0x00041228
		public DateTimeOffset AddMonths(int months)
		{
			return new DateTimeOffset(this.dt.AddMonths(months), this.utc_offset);
		}

		// Token: 0x06001070 RID: 4208 RVA: 0x00043044 File Offset: 0x00041244
		public DateTimeOffset AddSeconds(double seconds)
		{
			return new DateTimeOffset(this.dt.AddSeconds(seconds), this.utc_offset);
		}

		// Token: 0x06001071 RID: 4209 RVA: 0x00043060 File Offset: 0x00041260
		public DateTimeOffset AddTicks(long ticks)
		{
			return new DateTimeOffset(this.dt.AddTicks(ticks), this.utc_offset);
		}

		// Token: 0x06001072 RID: 4210 RVA: 0x0004307C File Offset: 0x0004127C
		public DateTimeOffset AddYears(int years)
		{
			return new DateTimeOffset(this.dt.AddYears(years), this.utc_offset);
		}

		// Token: 0x06001073 RID: 4211 RVA: 0x00043098 File Offset: 0x00041298
		public static int Compare(DateTimeOffset first, DateTimeOffset second)
		{
			return first.CompareTo(second);
		}

		// Token: 0x06001074 RID: 4212 RVA: 0x000430A4 File Offset: 0x000412A4
		public int CompareTo(DateTimeOffset other)
		{
			return this.UtcDateTime.CompareTo(other.UtcDateTime);
		}

		// Token: 0x06001075 RID: 4213 RVA: 0x000430C8 File Offset: 0x000412C8
		public bool Equals(DateTimeOffset other)
		{
			return this.UtcDateTime == other.UtcDateTime;
		}

		// Token: 0x06001076 RID: 4214 RVA: 0x000430DC File Offset: 0x000412DC
		public override bool Equals(object obj)
		{
			return obj is DateTimeOffset && this.UtcDateTime == ((DateTimeOffset)obj).UtcDateTime;
		}

		// Token: 0x06001077 RID: 4215 RVA: 0x00043110 File Offset: 0x00041310
		public static bool Equals(DateTimeOffset first, DateTimeOffset second)
		{
			return first.Equals(second);
		}

		// Token: 0x06001078 RID: 4216 RVA: 0x0004311C File Offset: 0x0004131C
		public bool EqualsExact(DateTimeOffset other)
		{
			return this.dt == other.dt && this.utc_offset == other.utc_offset;
		}

		// Token: 0x06001079 RID: 4217 RVA: 0x00043158 File Offset: 0x00041358
		public static DateTimeOffset FromFileTime(long fileTime)
		{
			if (fileTime < 0L || fileTime > DateTimeOffset.MaxValue.Ticks)
			{
				throw new ArgumentOutOfRangeException("fileTime is less than zero or greater than DateTimeOffset.MaxValue.Ticks.");
			}
			return new DateTimeOffset(DateTime.FromFileTime(fileTime), TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.FromFileTime(fileTime)));
		}

		// Token: 0x0600107A RID: 4218 RVA: 0x000431A8 File Offset: 0x000413A8
		public override int GetHashCode()
		{
			return this.dt.GetHashCode() ^ this.utc_offset.GetHashCode();
		}

		// Token: 0x0600107B RID: 4219 RVA: 0x000431C4 File Offset: 0x000413C4
		public static DateTimeOffset Parse(string input)
		{
			return DateTimeOffset.Parse(input, null);
		}

		// Token: 0x0600107C RID: 4220 RVA: 0x000431D0 File Offset: 0x000413D0
		public static DateTimeOffset Parse(string input, IFormatProvider formatProvider)
		{
			return DateTimeOffset.Parse(input, formatProvider, DateTimeStyles.AllowWhiteSpaces);
		}

		// Token: 0x0600107D RID: 4221 RVA: 0x000431DC File Offset: 0x000413DC
		public static DateTimeOffset Parse(string input, IFormatProvider formatProvider, DateTimeStyles styles)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}
			Exception ex = null;
			DateTime dateTime;
			DateTimeOffset result;
			if (!DateTime.CoreParse(input, formatProvider, styles, out dateTime, out result, true, ref ex))
			{
				throw ex;
			}
			if (dateTime.Ticks != 0L && result.Ticks == 0L)
			{
				throw new ArgumentOutOfRangeException("The UTC representation falls outside the 1-9999 year range");
			}
			return result;
		}

		// Token: 0x0600107E RID: 4222 RVA: 0x00043238 File Offset: 0x00041438
		public static DateTimeOffset ParseExact(string input, string format, IFormatProvider formatProvider)
		{
			return DateTimeOffset.ParseExact(input, format, formatProvider, DateTimeStyles.AssumeLocal);
		}

		// Token: 0x0600107F RID: 4223 RVA: 0x00043244 File Offset: 0x00041444
		public static DateTimeOffset ParseExact(string input, string format, IFormatProvider formatProvider, DateTimeStyles styles)
		{
			if (format == null)
			{
				throw new ArgumentNullException("format");
			}
			if (format == string.Empty)
			{
				throw new FormatException("format is an empty string");
			}
			return DateTimeOffset.ParseExact(input, new string[]
			{
				format
			}, formatProvider, styles);
		}

		// Token: 0x06001080 RID: 4224 RVA: 0x00043290 File Offset: 0x00041490
		public static DateTimeOffset ParseExact(string input, string[] formats, IFormatProvider formatProvider, DateTimeStyles styles)
		{
			if (input == null)
			{
				throw new ArgumentNullException("input");
			}
			if (input == string.Empty)
			{
				throw new FormatException("input is an empty string");
			}
			if (formats == null)
			{
				throw new ArgumentNullException("formats");
			}
			if (formats.Length == 0)
			{
				throw new FormatException("Invalid format specifier");
			}
			if ((styles & DateTimeStyles.AssumeLocal) != DateTimeStyles.None && (styles & DateTimeStyles.AssumeUniversal) != DateTimeStyles.None)
			{
				throw new ArgumentException("styles parameter contains incompatible flags");
			}
			DateTimeOffset result;
			if (!DateTimeOffset.ParseExact(input, formats, DateTimeFormatInfo.GetInstance(formatProvider), styles, out result))
			{
				throw new FormatException("Invalid format string");
			}
			return result;
		}

		// Token: 0x06001081 RID: 4225 RVA: 0x0004332C File Offset: 0x0004152C
		private static bool ParseExact(string input, string[] formats, DateTimeFormatInfo dfi, DateTimeStyles styles, out DateTimeOffset ret)
		{
			foreach (string text in formats)
			{
				if (text == null || text == string.Empty)
				{
					throw new FormatException("Invalid format string");
				}
				DateTimeOffset dateTimeOffset;
				if (DateTimeOffset.DoParse(input, text, false, out dateTimeOffset, dfi, styles))
				{
					ret = dateTimeOffset;
					return true;
				}
			}
			ret = DateTimeOffset.MinValue;
			return false;
		}

		// Token: 0x06001082 RID: 4226 RVA: 0x0004339C File Offset: 0x0004159C
		private static bool DoParse(string input, string format, bool exact, out DateTimeOffset result, DateTimeFormatInfo dfi, DateTimeStyles styles)
		{
			if ((styles & DateTimeStyles.AllowLeadingWhite) != DateTimeStyles.None)
			{
				format = format.TrimStart(null);
				input = input.TrimStart(null);
			}
			if ((styles & DateTimeStyles.AllowTrailingWhite) != DateTimeStyles.None)
			{
				format = format.TrimEnd(null);
				input = input.TrimEnd(null);
			}
			bool allow_leading_white = false;
			if ((styles & DateTimeStyles.AllowInnerWhite) != DateTimeStyles.None)
			{
				allow_leading_white = true;
			}
			bool flag = false;
			bool flag2 = false;
			if (format.Length == 1)
			{
				format = DateTimeUtils.GetStandardPattern(format[0], dfi, out flag, out flag2, true);
			}
			int num = -1;
			int num2 = -1;
			int num3 = -1;
			int num4 = -1;
			int num5 = -1;
			int num6 = -1;
			int num7 = -1;
			double num8 = -1.0;
			int num9 = -1;
			TimeSpan timeSpan = TimeSpan.MinValue;
			result = DateTimeOffset.MinValue;
			int i = 0;
			int num10 = 0;
			while (i < format.Length)
			{
				char c = format[i];
				char c2 = c;
				int num11;
				switch (c2)
				{
				case 's':
					num11 = DateTimeUtils.CountRepeat(format, i, c);
					if (num7 != -1 || num11 > 2)
					{
						return false;
					}
					num10 += DateTimeOffset.ParseNumber(input, num10, 2, num11 == 2, allow_leading_white, out num7);
					break;
				case 't':
				{
					num11 = DateTimeUtils.CountRepeat(format, i, c);
					if (num5 != -1 || num11 > 2)
					{
						return false;
					}
					int num12 = num10;
					string input2 = input;
					int pos = num10;
					string[] enums;
					if (num11 == 1)
					{
						string[] array = new string[2];
						array[0] = new string(dfi.AMDesignator[0], 1);
						enums = array;
						array[1] = new string(dfi.PMDesignator[0], 0);
					}
					else
					{
						string[] array2 = new string[2];
						array2[0] = dfi.AMDesignator;
						enums = array2;
						array2[1] = dfi.PMDesignator;
					}
					num10 = num12 + DateTimeOffset.ParseEnum(input2, pos, enums, allow_leading_white, out num9);
					if (num9 == -1)
					{
						return false;
					}
					if (num4 == -1)
					{
						num4 = num9 * 12;
					}
					else
					{
						num5 = num4 + num9 * 12;
					}
					break;
				}
				default:
					switch (c2)
					{
					case 'd':
						num11 = DateTimeUtils.CountRepeat(format, i, c);
						if (num3 != -1 || num11 > 4)
						{
							return false;
						}
						if (num11 <= 2)
						{
							num10 += DateTimeOffset.ParseNumber(input, num10, 2, num11 == 2, allow_leading_white, out num3);
						}
						else
						{
							num10 += DateTimeOffset.ParseEnum(input, num10, (num11 != 3) ? dfi.DayNames : dfi.AbbreviatedDayNames, allow_leading_white, out num9);
						}
						break;
					default:
						switch (c2)
						{
						case 'F':
						{
							num11 = DateTimeUtils.CountRepeat(format, i, c);
							int num14;
							int num13 = DateTimeOffset.ParseNumber(input, num10, num11, true, allow_leading_white, out num9, out num14);
							if (num9 == -1)
							{
								num10 += DateTimeOffset.ParseNumber(input, num10, num14, true, allow_leading_white, out num9);
							}
							else
							{
								num10 += num13;
							}
							if (num8 >= 0.0 || num11 > 7 || num9 == -1)
							{
								return false;
							}
							num8 = (double)num9 / Math.Pow(10.0, (double)num14);
							break;
						}
						default:
							if (c2 != ' ')
							{
								if (c2 != '%')
								{
									if (c2 != '/')
									{
										if (c2 != ':')
										{
											if (c2 != 'M')
											{
												if (c2 != '\\')
												{
													if (c2 != 'm')
													{
														num11 = 1;
														num10 += DateTimeOffset.ParseChar(input, num10, format[i], allow_leading_white, out num9);
														if (num9 == -1)
														{
															return false;
														}
													}
													else
													{
														num11 = DateTimeUtils.CountRepeat(format, i, c);
														if (num6 != -1 || num11 > 2)
														{
															return false;
														}
														num10 += DateTimeOffset.ParseNumber(input, num10, 2, num11 == 2, allow_leading_white, out num6);
													}
												}
												else
												{
													num11 = 2;
													num10 += DateTimeOffset.ParseChar(input, num10, format[i + 1], allow_leading_white, out num9);
													if (num9 == -1)
													{
														return false;
													}
												}
											}
											else
											{
												num11 = DateTimeUtils.CountRepeat(format, i, c);
												if (num2 != -1 || num11 > 4)
												{
													return false;
												}
												if (num11 <= 2)
												{
													num10 += DateTimeOffset.ParseNumber(input, num10, 2, num11 == 2, allow_leading_white, out num2);
												}
												else
												{
													num10 += DateTimeOffset.ParseEnum(input, num10, (num11 != 3) ? dfi.MonthNames : dfi.AbbreviatedMonthNames, allow_leading_white, out num2);
													num2++;
												}
											}
										}
										else
										{
											num11 = 1;
											num10 += DateTimeOffset.ParseEnum(input, num10, new string[]
											{
												dfi.TimeSeparator
											}, false, out num9);
											if (num9 == -1)
											{
												return false;
											}
										}
									}
									else
									{
										num11 = 1;
										num10 += DateTimeOffset.ParseEnum(input, num10, new string[]
										{
											dfi.DateSeparator
										}, false, out num9);
										if (num9 == -1)
										{
											return false;
										}
									}
								}
								else
								{
									num11 = 1;
									if (i != 0)
									{
										return false;
									}
								}
							}
							else
							{
								num11 = 1;
								num10 += DateTimeOffset.ParseChar(input, num10, ' ', false, out num9);
								if (num9 == -1)
								{
									return false;
								}
							}
							break;
						case 'H':
							num11 = DateTimeUtils.CountRepeat(format, i, c);
							if (num5 != -1 || num11 > 2)
							{
								return false;
							}
							num10 += DateTimeOffset.ParseNumber(input, num10, 2, num11 == 2, allow_leading_white, out num5);
							break;
						}
						break;
					case 'f':
						num11 = DateTimeUtils.CountRepeat(format, i, c);
						num10 += DateTimeOffset.ParseNumber(input, num10, num11, true, allow_leading_white, out num9);
						if (num8 >= 0.0 || num11 > 7 || num9 == -1)
						{
							return false;
						}
						num8 = (double)num9 / Math.Pow(10.0, (double)num11);
						break;
					case 'h':
						num11 = DateTimeUtils.CountRepeat(format, i, c);
						if (num5 != -1 || num11 > 2)
						{
							return false;
						}
						num10 += DateTimeOffset.ParseNumber(input, num10, 2, num11 == 2, allow_leading_white, out num9);
						if (num9 == -1)
						{
							return false;
						}
						if (num4 == -1)
						{
							num4 = num9;
						}
						else
						{
							num5 = num4 + num9;
						}
						break;
					}
					break;
				case 'y':
					if (num != -1)
					{
						return false;
					}
					num11 = DateTimeUtils.CountRepeat(format, i, c);
					if (num11 <= 2)
					{
						num10 += DateTimeOffset.ParseNumber(input, num10, 2, num11 == 2, allow_leading_white, out num);
						if (num != -1)
						{
							num += DateTime.Now.Year - DateTime.Now.Year % 100;
						}
					}
					else if (num11 <= 4)
					{
						int num15;
						num10 += DateTimeOffset.ParseNumber(input, num10, 5, false, allow_leading_white, out num, out num15);
						if (num15 < num11 || (num15 > num11 && (double)num / Math.Pow(10.0, (double)(num15 - 1)) < 1.0))
						{
							return false;
						}
					}
					else
					{
						num10 += DateTimeOffset.ParseNumber(input, num10, num11, true, allow_leading_white, out num);
					}
					break;
				case 'z':
				{
					num11 = DateTimeUtils.CountRepeat(format, i, c);
					if (timeSpan != TimeSpan.MinValue || num11 > 3)
					{
						return false;
					}
					int num16 = 0;
					num9 = 0;
					int num17;
					num10 += DateTimeOffset.ParseEnum(input, num10, new string[]
					{
						"-",
						"+"
					}, allow_leading_white, out num17);
					int num18;
					num10 += DateTimeOffset.ParseNumber(input, num10, 2, num11 != 1, false, out num18);
					if (num11 == 3)
					{
						num10 += DateTimeOffset.ParseEnum(input, num10, new string[]
						{
							dfi.TimeSeparator
						}, false, out num9);
						num10 += DateTimeOffset.ParseNumber(input, num10, 2, true, false, out num16);
					}
					if (num18 == -1 || num16 == -1 || num17 == -1)
					{
						return false;
					}
					if (num17 == 0)
					{
						num17 = -1;
					}
					timeSpan = new TimeSpan(num17 * num18, num17 * num16, 0);
					break;
				}
				}
				i += num11;
			}
			if (timeSpan == TimeSpan.MinValue && (styles & DateTimeStyles.AssumeLocal) != DateTimeStyles.None)
			{
				timeSpan = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);
			}
			if (timeSpan == TimeSpan.MinValue && (styles & DateTimeStyles.AssumeUniversal) != DateTimeStyles.None)
			{
				timeSpan = TimeSpan.Zero;
			}
			if (num5 < 0)
			{
				num5 = 0;
			}
			if (num6 < 0)
			{
				num6 = 0;
			}
			if (num7 < 0)
			{
				num7 = 0;
			}
			if (num8 < 0.0)
			{
				num8 = 0.0;
			}
			if (num > 0 && num2 > 0 && num3 > 0)
			{
				result = new DateTimeOffset(num, num2, num3, num5, num6, num7, 0, timeSpan);
				result = result.AddSeconds(num8);
				if ((styles & DateTimeStyles.AdjustToUniversal) != DateTimeStyles.None)
				{
					result = result.ToUniversalTime();
				}
				return true;
			}
			return false;
		}

		// Token: 0x06001083 RID: 4227 RVA: 0x00043C18 File Offset: 0x00041E18
		private static int ParseNumber(string input, int pos, int digits, bool leading_zero, bool allow_leading_white, out int result)
		{
			int num;
			return DateTimeOffset.ParseNumber(input, pos, digits, leading_zero, allow_leading_white, out result, out num);
		}

		// Token: 0x06001084 RID: 4228 RVA: 0x00043C34 File Offset: 0x00041E34
		private static int ParseNumber(string input, int pos, int digits, bool leading_zero, bool allow_leading_white, out int result, out int digit_parsed)
		{
			int num = 0;
			digit_parsed = 0;
			result = 0;
			while (allow_leading_white && pos < input.Length && input[pos] == ' ')
			{
				num++;
				pos++;
			}
			while (pos < input.Length && char.IsDigit(input[pos]) && digits > 0)
			{
				result = 10 * result + (int)((byte)(input[pos] - '0'));
				pos++;
				num++;
				digit_parsed++;
				digits--;
			}
			if (leading_zero && digits > 0)
			{
				result = -1;
			}
			if (digit_parsed == 0)
			{
				result = -1;
			}
			return num;
		}

		// Token: 0x06001085 RID: 4229 RVA: 0x00043CEC File Offset: 0x00041EEC
		private static int ParseEnum(string input, int pos, string[] enums, bool allow_leading_white, out int result)
		{
			int num = 0;
			result = -1;
			while (allow_leading_white && pos < input.Length && input[pos] == ' ')
			{
				num++;
				pos++;
			}
			for (int i = 0; i < enums.Length; i++)
			{
				if (input.Substring(pos).StartsWith(enums[i]))
				{
					result = i;
					break;
				}
			}
			if (result >= 0)
			{
				num += enums[result].Length;
			}
			return num;
		}

		// Token: 0x06001086 RID: 4230 RVA: 0x00043D78 File Offset: 0x00041F78
		private static int ParseChar(string input, int pos, char c, bool allow_leading_white, out int result)
		{
			int num = 0;
			result = -1;
			while (allow_leading_white && pos < input.Length && input[pos] == ' ')
			{
				pos++;
				num++;
			}
			if (pos < input.Length && input[pos] == c)
			{
				result = (int)c;
				num++;
			}
			return num;
		}

		// Token: 0x06001087 RID: 4231 RVA: 0x00043DDC File Offset: 0x00041FDC
		public TimeSpan Subtract(DateTimeOffset value)
		{
			return this.UtcDateTime - value.UtcDateTime;
		}

		// Token: 0x06001088 RID: 4232 RVA: 0x00043DF0 File Offset: 0x00041FF0
		public DateTimeOffset Subtract(TimeSpan value)
		{
			return this.Add(-value);
		}

		// Token: 0x06001089 RID: 4233 RVA: 0x00043E00 File Offset: 0x00042000
		public long ToFileTime()
		{
			return this.UtcDateTime.ToFileTime();
		}

		// Token: 0x0600108A RID: 4234 RVA: 0x00043E1C File Offset: 0x0004201C
		public DateTimeOffset ToLocalTime()
		{
			return new DateTimeOffset(this.UtcDateTime.ToLocalTime(), TimeZone.CurrentTimeZone.GetUtcOffset(this.UtcDateTime.ToLocalTime()));
		}

		// Token: 0x0600108B RID: 4235 RVA: 0x00043E54 File Offset: 0x00042054
		public DateTimeOffset ToOffset(TimeSpan offset)
		{
			return new DateTimeOffset(this.dt - this.utc_offset + offset, offset);
		}

		// Token: 0x0600108C RID: 4236 RVA: 0x00043E74 File Offset: 0x00042074
		public override string ToString()
		{
			return this.ToString(null, null);
		}

		// Token: 0x0600108D RID: 4237 RVA: 0x00043E80 File Offset: 0x00042080
		public string ToString(IFormatProvider formatProvider)
		{
			return this.ToString(null, formatProvider);
		}

		// Token: 0x0600108E RID: 4238 RVA: 0x00043E8C File Offset: 0x0004208C
		public string ToString(string format)
		{
			return this.ToString(format, null);
		}

		// Token: 0x0600108F RID: 4239 RVA: 0x00043E98 File Offset: 0x00042098
		public string ToString(string format, IFormatProvider formatProvider)
		{
			DateTimeFormatInfo instance = DateTimeFormatInfo.GetInstance(formatProvider);
			if (format == null || format == string.Empty)
			{
				format = instance.ShortDatePattern + " " + instance.LongTimePattern + " zzz";
			}
			bool flag = false;
			bool flag2 = false;
			if (format.Length == 1)
			{
				char format2 = format[0];
				try
				{
					format = DateTimeUtils.GetStandardPattern(format2, instance, out flag, out flag2, true);
				}
				catch
				{
					format = null;
				}
				if (format == null)
				{
					throw new FormatException("format is not one of the format specifier characters defined for DateTimeFormatInfo");
				}
			}
			return (!flag) ? DateTimeUtils.ToString(this.DateTime, new TimeSpan?(this.Offset), format, instance) : DateTimeUtils.ToString(this.UtcDateTime, new TimeSpan?(TimeSpan.Zero), format, instance);
		}

		// Token: 0x06001090 RID: 4240 RVA: 0x00043F7C File Offset: 0x0004217C
		public DateTimeOffset ToUniversalTime()
		{
			return new DateTimeOffset(this.UtcDateTime, TimeSpan.Zero);
		}

		// Token: 0x06001091 RID: 4241 RVA: 0x00043F90 File Offset: 0x00042190
		public static bool TryParse(string input, out DateTimeOffset result)
		{
			bool result2;
			try
			{
				result = DateTimeOffset.Parse(input);
				result2 = true;
			}
			catch
			{
				result = DateTimeOffset.MinValue;
				result2 = false;
			}
			return result2;
		}

		// Token: 0x06001092 RID: 4242 RVA: 0x00043FEC File Offset: 0x000421EC
		public static bool TryParse(string input, IFormatProvider formatProvider, DateTimeStyles styles, out DateTimeOffset result)
		{
			bool result2;
			try
			{
				result = DateTimeOffset.Parse(input, formatProvider, styles);
				result2 = true;
			}
			catch
			{
				result = DateTimeOffset.MinValue;
				result2 = false;
			}
			return result2;
		}

		// Token: 0x06001093 RID: 4243 RVA: 0x00044048 File Offset: 0x00042248
		public static bool TryParseExact(string input, string format, IFormatProvider formatProvider, DateTimeStyles styles, out DateTimeOffset result)
		{
			bool result2;
			try
			{
				result = DateTimeOffset.ParseExact(input, format, formatProvider, styles);
				result2 = true;
			}
			catch
			{
				result = DateTimeOffset.MinValue;
				result2 = false;
			}
			return result2;
		}

		// Token: 0x06001094 RID: 4244 RVA: 0x000440A8 File Offset: 0x000422A8
		public static bool TryParseExact(string input, string[] formats, IFormatProvider formatProvider, DateTimeStyles styles, out DateTimeOffset result)
		{
			bool result2;
			try
			{
				result = DateTimeOffset.ParseExact(input, formats, formatProvider, styles);
				result2 = true;
			}
			catch
			{
				result = DateTimeOffset.MinValue;
				result2 = false;
			}
			return result2;
		}

		// Token: 0x17000242 RID: 578
		// (get) Token: 0x06001095 RID: 4245 RVA: 0x00044108 File Offset: 0x00042308
		public DateTime Date
		{
			get
			{
				return DateTime.SpecifyKind(this.dt.Date, DateTimeKind.Unspecified);
			}
		}

		// Token: 0x17000243 RID: 579
		// (get) Token: 0x06001096 RID: 4246 RVA: 0x0004411C File Offset: 0x0004231C
		public DateTime DateTime
		{
			get
			{
				return DateTime.SpecifyKind(this.dt, DateTimeKind.Unspecified);
			}
		}

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06001097 RID: 4247 RVA: 0x0004412C File Offset: 0x0004232C
		public int Day
		{
			get
			{
				return this.dt.Day;
			}
		}

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x06001098 RID: 4248 RVA: 0x0004413C File Offset: 0x0004233C
		public DayOfWeek DayOfWeek
		{
			get
			{
				return this.dt.DayOfWeek;
			}
		}

		// Token: 0x17000246 RID: 582
		// (get) Token: 0x06001099 RID: 4249 RVA: 0x0004414C File Offset: 0x0004234C
		public int DayOfYear
		{
			get
			{
				return this.dt.DayOfYear;
			}
		}

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x0600109A RID: 4250 RVA: 0x0004415C File Offset: 0x0004235C
		public int Hour
		{
			get
			{
				return this.dt.Hour;
			}
		}

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x0600109B RID: 4251 RVA: 0x0004416C File Offset: 0x0004236C
		public DateTime LocalDateTime
		{
			get
			{
				return this.UtcDateTime.ToLocalTime();
			}
		}

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x0600109C RID: 4252 RVA: 0x00044188 File Offset: 0x00042388
		public int Millisecond
		{
			get
			{
				return this.dt.Millisecond;
			}
		}

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x0600109D RID: 4253 RVA: 0x00044198 File Offset: 0x00042398
		public int Minute
		{
			get
			{
				return this.dt.Minute;
			}
		}

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x0600109E RID: 4254 RVA: 0x000441A8 File Offset: 0x000423A8
		public int Month
		{
			get
			{
				return this.dt.Month;
			}
		}

		// Token: 0x1700024C RID: 588
		// (get) Token: 0x0600109F RID: 4255 RVA: 0x000441B8 File Offset: 0x000423B8
		public static DateTimeOffset Now
		{
			get
			{
				return new DateTimeOffset(DateTime.Now);
			}
		}

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x060010A0 RID: 4256 RVA: 0x000441C4 File Offset: 0x000423C4
		public TimeSpan Offset
		{
			get
			{
				return this.utc_offset;
			}
		}

		// Token: 0x1700024E RID: 590
		// (get) Token: 0x060010A1 RID: 4257 RVA: 0x000441CC File Offset: 0x000423CC
		public int Second
		{
			get
			{
				return this.dt.Second;
			}
		}

		// Token: 0x1700024F RID: 591
		// (get) Token: 0x060010A2 RID: 4258 RVA: 0x000441DC File Offset: 0x000423DC
		public long Ticks
		{
			get
			{
				return this.dt.Ticks;
			}
		}

		// Token: 0x17000250 RID: 592
		// (get) Token: 0x060010A3 RID: 4259 RVA: 0x000441EC File Offset: 0x000423EC
		public TimeSpan TimeOfDay
		{
			get
			{
				return this.dt.TimeOfDay;
			}
		}

		// Token: 0x17000251 RID: 593
		// (get) Token: 0x060010A4 RID: 4260 RVA: 0x000441FC File Offset: 0x000423FC
		public DateTime UtcDateTime
		{
			get
			{
				return DateTime.SpecifyKind(this.dt - this.utc_offset, DateTimeKind.Utc);
			}
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x060010A5 RID: 4261 RVA: 0x00044218 File Offset: 0x00042418
		public static DateTimeOffset UtcNow
		{
			get
			{
				return new DateTimeOffset(DateTime.UtcNow);
			}
		}

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x060010A6 RID: 4262 RVA: 0x00044224 File Offset: 0x00042424
		public long UtcTicks
		{
			get
			{
				return this.UtcDateTime.Ticks;
			}
		}

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x060010A7 RID: 4263 RVA: 0x00044240 File Offset: 0x00042440
		public int Year
		{
			get
			{
				return this.dt.Year;
			}
		}

		// Token: 0x060010A8 RID: 4264 RVA: 0x00044250 File Offset: 0x00042450
		public static DateTimeOffset operator +(DateTimeOffset dateTimeTz, TimeSpan timeSpan)
		{
			return dateTimeTz.Add(timeSpan);
		}

		// Token: 0x060010A9 RID: 4265 RVA: 0x0004425C File Offset: 0x0004245C
		public static bool operator ==(DateTimeOffset left, DateTimeOffset right)
		{
			return left.Equals(right);
		}

		// Token: 0x060010AA RID: 4266 RVA: 0x00044268 File Offset: 0x00042468
		public static bool operator >(DateTimeOffset left, DateTimeOffset right)
		{
			return left.UtcDateTime > right.UtcDateTime;
		}

		// Token: 0x060010AB RID: 4267 RVA: 0x00044280 File Offset: 0x00042480
		public static bool operator >=(DateTimeOffset left, DateTimeOffset right)
		{
			return left.UtcDateTime >= right.UtcDateTime;
		}

		// Token: 0x060010AC RID: 4268 RVA: 0x00044298 File Offset: 0x00042498
		public static implicit operator DateTimeOffset(DateTime dateTime)
		{
			return new DateTimeOffset(dateTime);
		}

		// Token: 0x060010AD RID: 4269 RVA: 0x000442A0 File Offset: 0x000424A0
		public static bool operator !=(DateTimeOffset left, DateTimeOffset right)
		{
			return left.UtcDateTime != right.UtcDateTime;
		}

		// Token: 0x060010AE RID: 4270 RVA: 0x000442B8 File Offset: 0x000424B8
		public static bool operator <(DateTimeOffset left, DateTimeOffset right)
		{
			return left.UtcDateTime < right.UtcDateTime;
		}

		// Token: 0x060010AF RID: 4271 RVA: 0x000442D0 File Offset: 0x000424D0
		public static bool operator <=(DateTimeOffset left, DateTimeOffset right)
		{
			return left.UtcDateTime <= right.UtcDateTime;
		}

		// Token: 0x060010B0 RID: 4272 RVA: 0x000442E8 File Offset: 0x000424E8
		public static TimeSpan operator -(DateTimeOffset left, DateTimeOffset right)
		{
			return left.Subtract(right);
		}

		// Token: 0x060010B1 RID: 4273 RVA: 0x000442F4 File Offset: 0x000424F4
		public static DateTimeOffset operator -(DateTimeOffset dateTimeTz, TimeSpan timeSpan)
		{
			return dateTimeTz.Subtract(timeSpan);
		}

		// Token: 0x040004B6 RID: 1206
		public static readonly DateTimeOffset MaxValue = new DateTimeOffset(DateTime.MaxValue, TimeSpan.Zero);

		// Token: 0x040004B7 RID: 1207
		public static readonly DateTimeOffset MinValue = new DateTimeOffset(DateTime.MinValue, TimeSpan.Zero);

		// Token: 0x040004B8 RID: 1208
		private DateTime dt;

		// Token: 0x040004B9 RID: 1209
		private TimeSpan utc_offset;
	}
}
