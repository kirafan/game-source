﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Assemblies
{
	// Token: 0x020001D7 RID: 471
	[ComVisible(true)]
	[Serializable]
	public enum AssemblyVersionCompatibility
	{
		// Token: 0x040008D4 RID: 2260
		SameMachine = 1,
		// Token: 0x040008D5 RID: 2261
		SameProcess,
		// Token: 0x040008D6 RID: 2262
		SameDomain
	}
}
