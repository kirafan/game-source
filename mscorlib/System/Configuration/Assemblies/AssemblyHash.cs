﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Assemblies
{
	// Token: 0x020001D5 RID: 469
	[ComVisible(true)]
	[Obsolete]
	[Serializable]
	public struct AssemblyHash : ICloneable
	{
		// Token: 0x06001842 RID: 6210 RVA: 0x0005D470 File Offset: 0x0005B670
		[Obsolete]
		public AssemblyHash(AssemblyHashAlgorithm algorithm, byte[] value)
		{
			this._algorithm = algorithm;
			if (value != null)
			{
				this._value = (byte[])value.Clone();
			}
			else
			{
				this._value = null;
			}
		}

		// Token: 0x06001843 RID: 6211 RVA: 0x0005D4A8 File Offset: 0x0005B6A8
		[Obsolete]
		public AssemblyHash(byte[] value)
		{
			this = new AssemblyHash(AssemblyHashAlgorithm.SHA1, value);
		}

		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x06001845 RID: 6213 RVA: 0x0005D4C8 File Offset: 0x0005B6C8
		// (set) Token: 0x06001846 RID: 6214 RVA: 0x0005D4D0 File Offset: 0x0005B6D0
		[Obsolete]
		public AssemblyHashAlgorithm Algorithm
		{
			get
			{
				return this._algorithm;
			}
			set
			{
				this._algorithm = value;
			}
		}

		// Token: 0x06001847 RID: 6215 RVA: 0x0005D4DC File Offset: 0x0005B6DC
		[Obsolete]
		public object Clone()
		{
			return new AssemblyHash(this._algorithm, this._value);
		}

		// Token: 0x06001848 RID: 6216 RVA: 0x0005D4F4 File Offset: 0x0005B6F4
		[Obsolete]
		public byte[] GetValue()
		{
			return this._value;
		}

		// Token: 0x06001849 RID: 6217 RVA: 0x0005D4FC File Offset: 0x0005B6FC
		[Obsolete]
		public void SetValue(byte[] value)
		{
			this._value = value;
		}

		// Token: 0x040008CC RID: 2252
		private AssemblyHashAlgorithm _algorithm;

		// Token: 0x040008CD RID: 2253
		private byte[] _value;

		// Token: 0x040008CE RID: 2254
		[Obsolete]
		public static readonly AssemblyHash Empty = new AssemblyHash(AssemblyHashAlgorithm.None, null);
	}
}
