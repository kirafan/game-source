﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration.Assemblies
{
	// Token: 0x020001D6 RID: 470
	[ComVisible(true)]
	[Serializable]
	public enum AssemblyHashAlgorithm
	{
		// Token: 0x040008D0 RID: 2256
		None,
		// Token: 0x040008D1 RID: 2257
		MD5 = 32771,
		// Token: 0x040008D2 RID: 2258
		SHA1
	}
}
