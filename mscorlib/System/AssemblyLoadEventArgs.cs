﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000105 RID: 261
	[ComVisible(true)]
	public class AssemblyLoadEventArgs : EventArgs
	{
		// Token: 0x06000D8A RID: 3466 RVA: 0x0003AF4C File Offset: 0x0003914C
		public AssemblyLoadEventArgs(Assembly loadedAssembly)
		{
			this.m_loadedAssembly = loadedAssembly;
		}

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x06000D8B RID: 3467 RVA: 0x0003AF5C File Offset: 0x0003915C
		public Assembly LoadedAssembly
		{
			get
			{
				return this.m_loadedAssembly;
			}
		}

		// Token: 0x040003A5 RID: 933
		private Assembly m_loadedAssembly;
	}
}
