﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000138 RID: 312
	[AttributeUsage(AttributeTargets.Enum, Inherited = false)]
	[ComVisible(true)]
	[Serializable]
	public class FlagsAttribute : Attribute
	{
	}
}
