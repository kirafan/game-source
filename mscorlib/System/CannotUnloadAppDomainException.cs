﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x0200010B RID: 267
	[ComVisible(true)]
	[Serializable]
	public class CannotUnloadAppDomainException : SystemException
	{
		// Token: 0x06000DBF RID: 3519 RVA: 0x0003B954 File Offset: 0x00039B54
		public CannotUnloadAppDomainException() : base(Locale.GetText("Attempt to unload application domain failed."))
		{
			base.HResult = -2146234347;
		}

		// Token: 0x06000DC0 RID: 3520 RVA: 0x0003B974 File Offset: 0x00039B74
		public CannotUnloadAppDomainException(string message) : base(message)
		{
			base.HResult = -2146234347;
		}

		// Token: 0x06000DC1 RID: 3521 RVA: 0x0003B988 File Offset: 0x00039B88
		protected CannotUnloadAppDomainException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06000DC2 RID: 3522 RVA: 0x0003B994 File Offset: 0x00039B94
		public CannotUnloadAppDomainException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146234347;
		}

		// Token: 0x040003BF RID: 959
		private const int Result = -2146234347;
	}
}
