﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000011 RID: 17
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.All)]
	[Serializable]
	public sealed class CLSCompliantAttribute : Attribute
	{
		// Token: 0x060000CF RID: 207 RVA: 0x000049FC File Offset: 0x00002BFC
		public CLSCompliantAttribute(bool isCompliant)
		{
			this.is_compliant = isCompliant;
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x060000D0 RID: 208 RVA: 0x00004A0C File Offset: 0x00002C0C
		public bool IsCompliant
		{
			get
			{
				return this.is_compliant;
			}
		}

		// Token: 0x0400000E RID: 14
		private bool is_compliant;
	}
}
