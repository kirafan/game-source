﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x02000146 RID: 326
	[ComVisible(true)]
	[Serializable]
	public class InvalidOperationException : SystemException
	{
		// Token: 0x060011BE RID: 4542 RVA: 0x00047158 File Offset: 0x00045358
		public InvalidOperationException() : base(Locale.GetText("Operation is not valid due to the current state of the object"))
		{
			base.HResult = -2146233079;
		}

		// Token: 0x060011BF RID: 4543 RVA: 0x00047178 File Offset: 0x00045378
		public InvalidOperationException(string message) : base(message)
		{
			base.HResult = -2146233079;
		}

		// Token: 0x060011C0 RID: 4544 RVA: 0x0004718C File Offset: 0x0004538C
		public InvalidOperationException(string message, Exception innerException) : base(message, innerException)
		{
			base.HResult = -2146233079;
		}

		// Token: 0x060011C1 RID: 4545 RVA: 0x000471A4 File Offset: 0x000453A4
		protected InvalidOperationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0400051D RID: 1309
		private const int Result = -2146233079;
	}
}
