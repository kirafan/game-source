﻿using System;

namespace System
{
	// Token: 0x0200019B RID: 411
	internal struct CharInfo
	{
		// Token: 0x04000835 RID: 2101
		public char Character;

		// Token: 0x04000836 RID: 2102
		public short Attributes;
	}
}
