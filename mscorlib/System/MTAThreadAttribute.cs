﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200015A RID: 346
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Method)]
	public sealed class MTAThreadAttribute : Attribute
	{
	}
}
