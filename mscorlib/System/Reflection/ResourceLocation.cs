﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002B9 RID: 697
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum ResourceLocation
	{
		// Token: 0x04000D3D RID: 3389
		Embedded = 1,
		// Token: 0x04000D3E RID: 3390
		ContainedInAnotherAssembly = 2,
		// Token: 0x04000D3F RID: 3391
		ContainedInManifestFile = 4
	}
}
