﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x020002BC RID: 700
	[ComVisible(true)]
	[Serializable]
	public sealed class TargetInvocationException : ApplicationException
	{
		// Token: 0x0600234A RID: 9034 RVA: 0x0007E6B4 File Offset: 0x0007C8B4
		public TargetInvocationException(Exception inner) : base("Exception has been thrown by the target of an invocation.", inner)
		{
		}

		// Token: 0x0600234B RID: 9035 RVA: 0x0007E6C4 File Offset: 0x0007C8C4
		public TargetInvocationException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x0600234C RID: 9036 RVA: 0x0007E6D0 File Offset: 0x0007C8D0
		internal TargetInvocationException(SerializationInfo info, StreamingContext sc) : base(info, sc)
		{
		}
	}
}
