﻿using System;
using System.Globalization;

namespace System.Reflection
{
	// Token: 0x020002ED RID: 749
	internal class MonoArrayMethod : MethodInfo
	{
		// Token: 0x06002695 RID: 9877 RVA: 0x00088684 File Offset: 0x00086884
		internal MonoArrayMethod(Type arrayClass, string methodName, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			this.name = methodName;
			this.parent = arrayClass;
			this.ret = returnType;
			this.parameters = (Type[])parameterTypes.Clone();
			this.call_conv = callingConvention;
		}

		// Token: 0x06002696 RID: 9878 RVA: 0x000886BC File Offset: 0x000868BC
		[MonoTODO("Always returns this")]
		public override MethodInfo GetBaseDefinition()
		{
			return this;
		}

		// Token: 0x170006D7 RID: 1751
		// (get) Token: 0x06002697 RID: 9879 RVA: 0x000886C0 File Offset: 0x000868C0
		public override Type ReturnType
		{
			get
			{
				return this.ret;
			}
		}

		// Token: 0x170006D8 RID: 1752
		// (get) Token: 0x06002698 RID: 9880 RVA: 0x000886C8 File Offset: 0x000868C8
		[MonoTODO("Not implemented.  Always returns null")]
		public override ICustomAttributeProvider ReturnTypeCustomAttributes
		{
			get
			{
				return null;
			}
		}

		// Token: 0x06002699 RID: 9881 RVA: 0x000886CC File Offset: 0x000868CC
		[MonoTODO("Not implemented.  Always returns zero")]
		public override MethodImplAttributes GetMethodImplementationFlags()
		{
			return MethodImplAttributes.IL;
		}

		// Token: 0x0600269A RID: 9882 RVA: 0x000886D0 File Offset: 0x000868D0
		[MonoTODO("Not implemented.  Always returns an empty array")]
		public override ParameterInfo[] GetParameters()
		{
			return new ParameterInfo[0];
		}

		// Token: 0x0600269B RID: 9883 RVA: 0x000886D8 File Offset: 0x000868D8
		[MonoTODO("Not implemented")]
		public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		// Token: 0x170006D9 RID: 1753
		// (get) Token: 0x0600269C RID: 9884 RVA: 0x000886E0 File Offset: 0x000868E0
		public override RuntimeMethodHandle MethodHandle
		{
			get
			{
				return this.mhandle;
			}
		}

		// Token: 0x170006DA RID: 1754
		// (get) Token: 0x0600269D RID: 9885 RVA: 0x000886E8 File Offset: 0x000868E8
		[MonoTODO("Not implemented.  Always returns zero")]
		public override MethodAttributes Attributes
		{
			get
			{
				return MethodAttributes.PrivateScope;
			}
		}

		// Token: 0x170006DB RID: 1755
		// (get) Token: 0x0600269E RID: 9886 RVA: 0x000886EC File Offset: 0x000868EC
		public override Type ReflectedType
		{
			get
			{
				return this.parent;
			}
		}

		// Token: 0x170006DC RID: 1756
		// (get) Token: 0x0600269F RID: 9887 RVA: 0x000886F4 File Offset: 0x000868F4
		public override Type DeclaringType
		{
			get
			{
				return this.parent;
			}
		}

		// Token: 0x170006DD RID: 1757
		// (get) Token: 0x060026A0 RID: 9888 RVA: 0x000886FC File Offset: 0x000868FC
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x060026A1 RID: 9889 RVA: 0x00088704 File Offset: 0x00086904
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x060026A2 RID: 9890 RVA: 0x00088710 File Offset: 0x00086910
		public override object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x060026A3 RID: 9891 RVA: 0x0008871C File Offset: 0x0008691C
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x060026A4 RID: 9892 RVA: 0x00088728 File Offset: 0x00086928
		public override string ToString()
		{
			string text = string.Empty;
			ParameterInfo[] array = this.GetParameters();
			for (int i = 0; i < array.Length; i++)
			{
				if (i > 0)
				{
					text += ", ";
				}
				text += array[i].ParameterType.Name;
			}
			if (this.ReturnType != null)
			{
				return string.Concat(new string[]
				{
					this.ReturnType.Name,
					" ",
					this.Name,
					"(",
					text,
					")"
				});
			}
			return string.Concat(new string[]
			{
				"void ",
				this.Name,
				"(",
				text,
				")"
			});
		}

		// Token: 0x04000E7A RID: 3706
		internal RuntimeMethodHandle mhandle;

		// Token: 0x04000E7B RID: 3707
		internal Type parent;

		// Token: 0x04000E7C RID: 3708
		internal Type ret;

		// Token: 0x04000E7D RID: 3709
		internal Type[] parameters;

		// Token: 0x04000E7E RID: 3710
		internal string name;

		// Token: 0x04000E7F RID: 3711
		internal int table_idx;

		// Token: 0x04000E80 RID: 3712
		internal CallingConventions call_conv;
	}
}
