﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Cryptography.X509Certificates;
using System.Security.Permissions;

namespace System.Reflection
{
	// Token: 0x020002A0 RID: 672
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_Module))]
	[Serializable]
	public class Module : ISerializable, ICustomAttributeProvider, _Module
	{
		// Token: 0x060021EC RID: 8684 RVA: 0x0007AE28 File Offset: 0x00079028
		internal Module()
		{
		}

		// Token: 0x060021EE RID: 8686 RVA: 0x0007AE60 File Offset: 0x00079060
		void _Module.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021EF RID: 8687 RVA: 0x0007AE68 File Offset: 0x00079068
		void _Module.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021F0 RID: 8688 RVA: 0x0007AE70 File Offset: 0x00079070
		void _Module.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021F1 RID: 8689 RVA: 0x0007AE78 File Offset: 0x00079078
		void _Module.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x170005D8 RID: 1496
		// (get) Token: 0x060021F2 RID: 8690 RVA: 0x0007AE80 File Offset: 0x00079080
		public Assembly Assembly
		{
			get
			{
				return this.assembly;
			}
		}

		// Token: 0x170005D9 RID: 1497
		// (get) Token: 0x060021F3 RID: 8691 RVA: 0x0007AE88 File Offset: 0x00079088
		public virtual string FullyQualifiedName
		{
			get
			{
				if (SecurityManager.SecurityEnabled)
				{
					new FileIOPermission(FileIOPermissionAccess.PathDiscovery, this.fqname).Demand();
				}
				return this.fqname;
			}
		}

		// Token: 0x170005DA RID: 1498
		// (get) Token: 0x060021F4 RID: 8692 RVA: 0x0007AEAC File Offset: 0x000790AC
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170005DB RID: 1499
		// (get) Token: 0x060021F5 RID: 8693 RVA: 0x0007AEB4 File Offset: 0x000790B4
		public string ScopeName
		{
			get
			{
				return this.scopename;
			}
		}

		// Token: 0x170005DC RID: 1500
		// (get) Token: 0x060021F6 RID: 8694 RVA: 0x0007AEBC File Offset: 0x000790BC
		public ModuleHandle ModuleHandle
		{
			get
			{
				return new ModuleHandle(this._impl);
			}
		}

		// Token: 0x170005DD RID: 1501
		// (get) Token: 0x060021F7 RID: 8695
		public extern int MetadataToken { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x170005DE RID: 1502
		// (get) Token: 0x060021F8 RID: 8696 RVA: 0x0007AECC File Offset: 0x000790CC
		public int MDStreamVersion
		{
			get
			{
				if (this._impl == IntPtr.Zero)
				{
					throw new NotSupportedException();
				}
				return Module.GetMDStreamVersion(this._impl);
			}
		}

		// Token: 0x060021F9 RID: 8697
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int GetMDStreamVersion(IntPtr module_handle);

		// Token: 0x060021FA RID: 8698 RVA: 0x0007AF00 File Offset: 0x00079100
		public virtual Type[] FindTypes(TypeFilter filter, object filterCriteria)
		{
			ArrayList arrayList = new ArrayList();
			Type[] types = this.GetTypes();
			foreach (Type type in types)
			{
				if (filter(type, filterCriteria))
				{
					arrayList.Add(type);
				}
			}
			return (Type[])arrayList.ToArray(typeof(Type));
		}

		// Token: 0x060021FB RID: 8699 RVA: 0x0007AF64 File Offset: 0x00079164
		public virtual object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x060021FC RID: 8700 RVA: 0x0007AF70 File Offset: 0x00079170
		public virtual object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x060021FD RID: 8701 RVA: 0x0007AF7C File Offset: 0x0007917C
		public FieldInfo GetField(string name)
		{
			if (this.IsResource())
			{
				return null;
			}
			Type globalType = this.GetGlobalType();
			return (globalType == null) ? null : globalType.GetField(name, BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x060021FE RID: 8702 RVA: 0x0007AFB4 File Offset: 0x000791B4
		public FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			if (this.IsResource())
			{
				return null;
			}
			Type globalType = this.GetGlobalType();
			return (globalType == null) ? null : globalType.GetField(name, bindingAttr);
		}

		// Token: 0x060021FF RID: 8703 RVA: 0x0007AFEC File Offset: 0x000791EC
		public FieldInfo[] GetFields()
		{
			if (this.IsResource())
			{
				return new FieldInfo[0];
			}
			Type globalType = this.GetGlobalType();
			return (globalType == null) ? new FieldInfo[0] : globalType.GetFields(BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x06002200 RID: 8704 RVA: 0x0007B02C File Offset: 0x0007922C
		public MethodInfo GetMethod(string name)
		{
			if (this.IsResource())
			{
				return null;
			}
			Type globalType = this.GetGlobalType();
			return (globalType == null) ? null : globalType.GetMethod(name);
		}

		// Token: 0x06002201 RID: 8705 RVA: 0x0007B060 File Offset: 0x00079260
		public MethodInfo GetMethod(string name, Type[] types)
		{
			return this.GetMethodImpl(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public, null, CallingConventions.Any, types, null);
		}

		// Token: 0x06002202 RID: 8706 RVA: 0x0007B070 File Offset: 0x00079270
		public MethodInfo GetMethod(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			return this.GetMethodImpl(name, bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x06002203 RID: 8707 RVA: 0x0007B084 File Offset: 0x00079284
		protected virtual MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			if (this.IsResource())
			{
				return null;
			}
			Type globalType = this.GetGlobalType();
			return (globalType == null) ? null : globalType.GetMethod(name, bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x06002204 RID: 8708 RVA: 0x0007B0C0 File Offset: 0x000792C0
		public MethodInfo[] GetMethods()
		{
			if (this.IsResource())
			{
				return new MethodInfo[0];
			}
			Type globalType = this.GetGlobalType();
			return (globalType == null) ? new MethodInfo[0] : globalType.GetMethods();
		}

		// Token: 0x06002205 RID: 8709 RVA: 0x0007B100 File Offset: 0x00079300
		public MethodInfo[] GetMethods(BindingFlags bindingFlags)
		{
			if (this.IsResource())
			{
				return new MethodInfo[0];
			}
			Type globalType = this.GetGlobalType();
			return (globalType == null) ? new MethodInfo[0] : globalType.GetMethods(bindingFlags);
		}

		// Token: 0x06002206 RID: 8710 RVA: 0x0007B140 File Offset: 0x00079340
		public FieldInfo[] GetFields(BindingFlags bindingFlags)
		{
			if (this.IsResource())
			{
				return new FieldInfo[0];
			}
			Type globalType = this.GetGlobalType();
			return (globalType == null) ? new FieldInfo[0] : globalType.GetFields(bindingFlags);
		}

		// Token: 0x06002207 RID: 8711 RVA: 0x0007B180 File Offset: 0x00079380
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			UnitySerializationHolder.GetModuleData(this, info, context);
		}

		// Token: 0x06002208 RID: 8712 RVA: 0x0007B19C File Offset: 0x0007939C
		public X509Certificate GetSignerCertificate()
		{
			X509Certificate result;
			try
			{
				result = X509Certificate.CreateFromSignedFile(this.assembly.Location);
			}
			catch
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06002209 RID: 8713 RVA: 0x0007B1F0 File Offset: 0x000793F0
		[ComVisible(true)]
		public virtual Type GetType(string className)
		{
			return this.GetType(className, false, false);
		}

		// Token: 0x0600220A RID: 8714 RVA: 0x0007B1FC File Offset: 0x000793FC
		[ComVisible(true)]
		public virtual Type GetType(string className, bool ignoreCase)
		{
			return this.GetType(className, false, ignoreCase);
		}

		// Token: 0x0600220B RID: 8715 RVA: 0x0007B208 File Offset: 0x00079408
		[ComVisible(true)]
		public virtual Type GetType(string className, bool throwOnError, bool ignoreCase)
		{
			if (className == null)
			{
				throw new ArgumentNullException("className");
			}
			if (className == string.Empty)
			{
				throw new ArgumentException("Type name can't be empty");
			}
			return this.assembly.InternalGetType(this, className, throwOnError, ignoreCase);
		}

		// Token: 0x0600220C RID: 8716
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type[] InternalGetTypes();

		// Token: 0x0600220D RID: 8717 RVA: 0x0007B248 File Offset: 0x00079448
		public virtual Type[] GetTypes()
		{
			return this.InternalGetTypes();
		}

		// Token: 0x0600220E RID: 8718 RVA: 0x0007B250 File Offset: 0x00079450
		public virtual bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x0600220F RID: 8719 RVA: 0x0007B25C File Offset: 0x0007945C
		public bool IsResource()
		{
			return this.is_resource;
		}

		// Token: 0x06002210 RID: 8720 RVA: 0x0007B264 File Offset: 0x00079464
		public override string ToString()
		{
			return this.name;
		}

		// Token: 0x170005DF RID: 1503
		// (get) Token: 0x06002211 RID: 8721 RVA: 0x0007B26C File Offset: 0x0007946C
		internal Guid MvId
		{
			get
			{
				return this.GetModuleVersionId();
			}
		}

		// Token: 0x170005E0 RID: 1504
		// (get) Token: 0x06002212 RID: 8722 RVA: 0x0007B274 File Offset: 0x00079474
		public Guid ModuleVersionId
		{
			get
			{
				return this.GetModuleVersionId();
			}
		}

		// Token: 0x06002213 RID: 8723 RVA: 0x0007B27C File Offset: 0x0007947C
		public void GetPEKind(out PortableExecutableKinds peKind, out ImageFileMachine machine)
		{
			this.ModuleHandle.GetPEKind(out peKind, out machine);
		}

		// Token: 0x06002214 RID: 8724 RVA: 0x0007B29C File Offset: 0x0007949C
		private Exception resolve_token_exception(int metadataToken, ResolveTokenError error, string tokenType)
		{
			if (error == ResolveTokenError.OutOfRange)
			{
				return new ArgumentOutOfRangeException("metadataToken", string.Format("Token 0x{0:x} is not valid in the scope of module {1}", metadataToken, this.name));
			}
			return new ArgumentException(string.Format("Token 0x{0:x} is not a valid {1} token in the scope of module {2}", metadataToken, tokenType, this.name), "metadataToken");
		}

		// Token: 0x06002215 RID: 8725 RVA: 0x0007B2F4 File Offset: 0x000794F4
		private IntPtr[] ptrs_from_types(Type[] types)
		{
			if (types == null)
			{
				return null;
			}
			IntPtr[] array = new IntPtr[types.Length];
			for (int i = 0; i < types.Length; i++)
			{
				if (types[i] == null)
				{
					throw new ArgumentException();
				}
				array[i] = types[i].TypeHandle.Value;
			}
			return array;
		}

		// Token: 0x06002216 RID: 8726 RVA: 0x0007B354 File Offset: 0x00079554
		public FieldInfo ResolveField(int metadataToken)
		{
			return this.ResolveField(metadataToken, null, null);
		}

		// Token: 0x06002217 RID: 8727 RVA: 0x0007B360 File Offset: 0x00079560
		public FieldInfo ResolveField(int metadataToken, Type[] genericTypeArguments, Type[] genericMethodArguments)
		{
			ResolveTokenError error;
			IntPtr intPtr = Module.ResolveFieldToken(this._impl, metadataToken, this.ptrs_from_types(genericTypeArguments), this.ptrs_from_types(genericMethodArguments), out error);
			if (intPtr == IntPtr.Zero)
			{
				throw this.resolve_token_exception(metadataToken, error, "Field");
			}
			return FieldInfo.GetFieldFromHandle(new RuntimeFieldHandle(intPtr));
		}

		// Token: 0x06002218 RID: 8728 RVA: 0x0007B3B4 File Offset: 0x000795B4
		public MemberInfo ResolveMember(int metadataToken)
		{
			return this.ResolveMember(metadataToken, null, null);
		}

		// Token: 0x06002219 RID: 8729 RVA: 0x0007B3C0 File Offset: 0x000795C0
		public MemberInfo ResolveMember(int metadataToken, Type[] genericTypeArguments, Type[] genericMethodArguments)
		{
			ResolveTokenError error;
			MemberInfo memberInfo = Module.ResolveMemberToken(this._impl, metadataToken, this.ptrs_from_types(genericTypeArguments), this.ptrs_from_types(genericMethodArguments), out error);
			if (memberInfo == null)
			{
				throw this.resolve_token_exception(metadataToken, error, "MemberInfo");
			}
			return memberInfo;
		}

		// Token: 0x0600221A RID: 8730 RVA: 0x0007B400 File Offset: 0x00079600
		public MethodBase ResolveMethod(int metadataToken)
		{
			return this.ResolveMethod(metadataToken, null, null);
		}

		// Token: 0x0600221B RID: 8731 RVA: 0x0007B40C File Offset: 0x0007960C
		public MethodBase ResolveMethod(int metadataToken, Type[] genericTypeArguments, Type[] genericMethodArguments)
		{
			ResolveTokenError error;
			IntPtr intPtr = Module.ResolveMethodToken(this._impl, metadataToken, this.ptrs_from_types(genericTypeArguments), this.ptrs_from_types(genericMethodArguments), out error);
			if (intPtr == IntPtr.Zero)
			{
				throw this.resolve_token_exception(metadataToken, error, "MethodBase");
			}
			return MethodBase.GetMethodFromHandleNoGenericCheck(new RuntimeMethodHandle(intPtr));
		}

		// Token: 0x0600221C RID: 8732 RVA: 0x0007B460 File Offset: 0x00079660
		public string ResolveString(int metadataToken)
		{
			ResolveTokenError error;
			string text = Module.ResolveStringToken(this._impl, metadataToken, out error);
			if (text == null)
			{
				throw this.resolve_token_exception(metadataToken, error, "string");
			}
			return text;
		}

		// Token: 0x0600221D RID: 8733 RVA: 0x0007B494 File Offset: 0x00079694
		public Type ResolveType(int metadataToken)
		{
			return this.ResolveType(metadataToken, null, null);
		}

		// Token: 0x0600221E RID: 8734 RVA: 0x0007B4A0 File Offset: 0x000796A0
		public Type ResolveType(int metadataToken, Type[] genericTypeArguments, Type[] genericMethodArguments)
		{
			ResolveTokenError error;
			IntPtr intPtr = Module.ResolveTypeToken(this._impl, metadataToken, this.ptrs_from_types(genericTypeArguments), this.ptrs_from_types(genericMethodArguments), out error);
			if (intPtr == IntPtr.Zero)
			{
				throw this.resolve_token_exception(metadataToken, error, "Type");
			}
			return Type.GetTypeFromHandle(new RuntimeTypeHandle(intPtr));
		}

		// Token: 0x0600221F RID: 8735 RVA: 0x0007B4F4 File Offset: 0x000796F4
		public byte[] ResolveSignature(int metadataToken)
		{
			ResolveTokenError error;
			byte[] array = Module.ResolveSignature(this._impl, metadataToken, out error);
			if (array == null)
			{
				throw this.resolve_token_exception(metadataToken, error, "signature");
			}
			return array;
		}

		// Token: 0x06002220 RID: 8736 RVA: 0x0007B528 File Offset: 0x00079728
		internal static Type MonoDebugger_ResolveType(Module module, int token)
		{
			ResolveTokenError resolveTokenError;
			IntPtr intPtr = Module.ResolveTypeToken(module._impl, token, null, null, out resolveTokenError);
			if (intPtr == IntPtr.Zero)
			{
				return null;
			}
			return Type.GetTypeFromHandle(new RuntimeTypeHandle(intPtr));
		}

		// Token: 0x06002221 RID: 8737 RVA: 0x0007B564 File Offset: 0x00079764
		internal static Guid Mono_GetGuid(Module module)
		{
			return module.GetModuleVersionId();
		}

		// Token: 0x06002222 RID: 8738 RVA: 0x0007B56C File Offset: 0x0007976C
		internal virtual Guid GetModuleVersionId()
		{
			return new Guid(this.GetGuidInternal());
		}

		// Token: 0x06002223 RID: 8739 RVA: 0x0007B57C File Offset: 0x0007977C
		private static bool filter_by_type_name(Type m, object filterCriteria)
		{
			string text = (string)filterCriteria;
			if (text.EndsWith("*"))
			{
				return m.Name.StartsWith(text.Substring(0, text.Length - 1));
			}
			return m.Name == text;
		}

		// Token: 0x06002224 RID: 8740 RVA: 0x0007B5C8 File Offset: 0x000797C8
		private static bool filter_by_type_name_ignore_case(Type m, object filterCriteria)
		{
			string text = (string)filterCriteria;
			if (text.EndsWith("*"))
			{
				return m.Name.ToLower().StartsWith(text.Substring(0, text.Length - 1).ToLower());
			}
			return string.Compare(m.Name, text, true) == 0;
		}

		// Token: 0x06002225 RID: 8741
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern IntPtr GetHINSTANCE();

		// Token: 0x06002226 RID: 8742
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string GetGuidInternal();

		// Token: 0x06002227 RID: 8743
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type GetGlobalType();

		// Token: 0x06002228 RID: 8744
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern IntPtr ResolveTypeToken(IntPtr module, int token, IntPtr[] type_args, IntPtr[] method_args, out ResolveTokenError error);

		// Token: 0x06002229 RID: 8745
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern IntPtr ResolveMethodToken(IntPtr module, int token, IntPtr[] type_args, IntPtr[] method_args, out ResolveTokenError error);

		// Token: 0x0600222A RID: 8746
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern IntPtr ResolveFieldToken(IntPtr module, int token, IntPtr[] type_args, IntPtr[] method_args, out ResolveTokenError error);

		// Token: 0x0600222B RID: 8747
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern string ResolveStringToken(IntPtr module, int token, out ResolveTokenError error);

		// Token: 0x0600222C RID: 8748
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern MemberInfo ResolveMemberToken(IntPtr module, int token, IntPtr[] type_args, IntPtr[] method_args, out ResolveTokenError error);

		// Token: 0x0600222D RID: 8749
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern byte[] ResolveSignature(IntPtr module, int metadataToken, out ResolveTokenError error);

		// Token: 0x0600222E RID: 8750
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void GetPEKind(IntPtr module, out PortableExecutableKinds peKind, out ImageFileMachine machine);

		// Token: 0x04000CC9 RID: 3273
		private const BindingFlags defaultBindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public;

		// Token: 0x04000CCA RID: 3274
		public static readonly TypeFilter FilterTypeName = new TypeFilter(Module.filter_by_type_name);

		// Token: 0x04000CCB RID: 3275
		public static readonly TypeFilter FilterTypeNameIgnoreCase = new TypeFilter(Module.filter_by_type_name_ignore_case);

		// Token: 0x04000CCC RID: 3276
		private IntPtr _impl;

		// Token: 0x04000CCD RID: 3277
		internal Assembly assembly;

		// Token: 0x04000CCE RID: 3278
		internal string fqname;

		// Token: 0x04000CCF RID: 3279
		internal string name;

		// Token: 0x04000CD0 RID: 3280
		internal string scopename;

		// Token: 0x04000CD1 RID: 3281
		internal bool is_resource;

		// Token: 0x04000CD2 RID: 3282
		internal int token;
	}
}
