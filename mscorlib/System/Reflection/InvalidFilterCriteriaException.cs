﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x02000294 RID: 660
	[ComVisible(true)]
	[Serializable]
	public class InvalidFilterCriteriaException : ApplicationException
	{
		// Token: 0x06002195 RID: 8597 RVA: 0x0007A634 File Offset: 0x00078834
		public InvalidFilterCriteriaException() : base(Locale.GetText("Filter Criteria is not valid."))
		{
		}

		// Token: 0x06002196 RID: 8598 RVA: 0x0007A648 File Offset: 0x00078848
		public InvalidFilterCriteriaException(string message) : base(message)
		{
		}

		// Token: 0x06002197 RID: 8599 RVA: 0x0007A654 File Offset: 0x00078854
		public InvalidFilterCriteriaException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06002198 RID: 8600 RVA: 0x0007A660 File Offset: 0x00078860
		protected InvalidFilterCriteriaException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
