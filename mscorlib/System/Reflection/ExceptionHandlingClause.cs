﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200028D RID: 653
	[ComVisible(true)]
	public sealed class ExceptionHandlingClause
	{
		// Token: 0x06002166 RID: 8550 RVA: 0x0007A264 File Offset: 0x00078464
		internal ExceptionHandlingClause()
		{
		}

		// Token: 0x1700059B RID: 1435
		// (get) Token: 0x06002167 RID: 8551 RVA: 0x0007A26C File Offset: 0x0007846C
		public Type CatchType
		{
			get
			{
				return this.catch_type;
			}
		}

		// Token: 0x1700059C RID: 1436
		// (get) Token: 0x06002168 RID: 8552 RVA: 0x0007A274 File Offset: 0x00078474
		public int FilterOffset
		{
			get
			{
				return this.filter_offset;
			}
		}

		// Token: 0x1700059D RID: 1437
		// (get) Token: 0x06002169 RID: 8553 RVA: 0x0007A27C File Offset: 0x0007847C
		public ExceptionHandlingClauseOptions Flags
		{
			get
			{
				return this.flags;
			}
		}

		// Token: 0x1700059E RID: 1438
		// (get) Token: 0x0600216A RID: 8554 RVA: 0x0007A284 File Offset: 0x00078484
		public int HandlerLength
		{
			get
			{
				return this.handler_length;
			}
		}

		// Token: 0x1700059F RID: 1439
		// (get) Token: 0x0600216B RID: 8555 RVA: 0x0007A28C File Offset: 0x0007848C
		public int HandlerOffset
		{
			get
			{
				return this.handler_offset;
			}
		}

		// Token: 0x170005A0 RID: 1440
		// (get) Token: 0x0600216C RID: 8556 RVA: 0x0007A294 File Offset: 0x00078494
		public int TryLength
		{
			get
			{
				return this.try_length;
			}
		}

		// Token: 0x170005A1 RID: 1441
		// (get) Token: 0x0600216D RID: 8557 RVA: 0x0007A29C File Offset: 0x0007849C
		public int TryOffset
		{
			get
			{
				return this.try_offset;
			}
		}

		// Token: 0x0600216E RID: 8558 RVA: 0x0007A2A4 File Offset: 0x000784A4
		public override string ToString()
		{
			string text = string.Format("Flags={0}, TryOffset={1}, TryLength={2}, HandlerOffset={3}, HandlerLength={4}", new object[]
			{
				this.flags,
				this.try_offset,
				this.try_length,
				this.handler_offset,
				this.handler_length
			});
			if (this.catch_type != null)
			{
				text = string.Format("{0}, CatchType={1}", text, this.catch_type);
			}
			if (this.flags == ExceptionHandlingClauseOptions.Filter)
			{
				text = string.Format(CultureInfo.InvariantCulture, "{0}, FilterOffset={1}", new object[]
				{
					text,
					this.filter_offset
				});
			}
			return text;
		}

		// Token: 0x04000C4F RID: 3151
		internal Type catch_type;

		// Token: 0x04000C50 RID: 3152
		internal int filter_offset;

		// Token: 0x04000C51 RID: 3153
		internal ExceptionHandlingClauseOptions flags;

		// Token: 0x04000C52 RID: 3154
		internal int try_offset;

		// Token: 0x04000C53 RID: 3155
		internal int try_length;

		// Token: 0x04000C54 RID: 3156
		internal int handler_offset;

		// Token: 0x04000C55 RID: 3157
		internal int handler_length;
	}
}
