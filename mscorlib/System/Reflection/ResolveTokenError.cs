﻿using System;

namespace System.Reflection
{
	// Token: 0x0200029F RID: 671
	internal enum ResolveTokenError
	{
		// Token: 0x04000CC6 RID: 3270
		OutOfRange,
		// Token: 0x04000CC7 RID: 3271
		BadTable,
		// Token: 0x04000CC8 RID: 3272
		Other
	}
}
