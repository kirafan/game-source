﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000278 RID: 632
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyFlagsAttribute : Attribute
	{
		// Token: 0x060020C8 RID: 8392 RVA: 0x00077DD4 File Offset: 0x00075FD4
		[Obsolete("")]
		[CLSCompliant(false)]
		public AssemblyFlagsAttribute(uint flags)
		{
			this.flags = flags;
		}

		// Token: 0x060020C9 RID: 8393 RVA: 0x00077DE4 File Offset: 0x00075FE4
		[Obsolete("")]
		public AssemblyFlagsAttribute(int assemblyFlags)
		{
			this.flags = (uint)assemblyFlags;
		}

		// Token: 0x060020CA RID: 8394 RVA: 0x00077DF4 File Offset: 0x00075FF4
		public AssemblyFlagsAttribute(AssemblyNameFlags assemblyFlags)
		{
			this.flags = (uint)assemblyFlags;
		}

		// Token: 0x17000579 RID: 1401
		// (get) Token: 0x060020CB RID: 8395 RVA: 0x00077E04 File Offset: 0x00076004
		[CLSCompliant(false)]
		[Obsolete("")]
		public uint Flags
		{
			get
			{
				return this.flags;
			}
		}

		// Token: 0x1700057A RID: 1402
		// (get) Token: 0x060020CC RID: 8396 RVA: 0x00077E0C File Offset: 0x0007600C
		public int AssemblyFlags
		{
			get
			{
				return (int)this.flags;
			}
		}

		// Token: 0x04000C08 RID: 3080
		private uint flags;
	}
}
