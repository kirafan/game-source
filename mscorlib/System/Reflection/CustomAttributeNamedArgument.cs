﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000289 RID: 649
	[ComVisible(true)]
	[Serializable]
	public struct CustomAttributeNamedArgument
	{
		// Token: 0x0600213E RID: 8510 RVA: 0x00079D0C File Offset: 0x00077F0C
		internal CustomAttributeNamedArgument(MemberInfo memberInfo, object typedArgument)
		{
			this.memberInfo = memberInfo;
			this.typedArgument = (CustomAttributeTypedArgument)typedArgument;
		}

		// Token: 0x17000592 RID: 1426
		// (get) Token: 0x0600213F RID: 8511 RVA: 0x00079D24 File Offset: 0x00077F24
		public MemberInfo MemberInfo
		{
			get
			{
				return this.memberInfo;
			}
		}

		// Token: 0x17000593 RID: 1427
		// (get) Token: 0x06002140 RID: 8512 RVA: 0x00079D2C File Offset: 0x00077F2C
		public CustomAttributeTypedArgument TypedValue
		{
			get
			{
				return this.typedArgument;
			}
		}

		// Token: 0x06002141 RID: 8513 RVA: 0x00079D34 File Offset: 0x00077F34
		public override string ToString()
		{
			return this.memberInfo.Name + " = " + this.typedArgument.ToString();
		}

		// Token: 0x06002142 RID: 8514 RVA: 0x00079D64 File Offset: 0x00077F64
		public override bool Equals(object obj)
		{
			if (!(obj is CustomAttributeNamedArgument))
			{
				return false;
			}
			CustomAttributeNamedArgument customAttributeNamedArgument = (CustomAttributeNamedArgument)obj;
			return customAttributeNamedArgument.memberInfo == this.memberInfo && this.typedArgument.Equals(customAttributeNamedArgument.typedArgument);
		}

		// Token: 0x06002143 RID: 8515 RVA: 0x00079DB4 File Offset: 0x00077FB4
		public override int GetHashCode()
		{
			return (this.memberInfo.GetHashCode() << 16) + this.typedArgument.GetHashCode();
		}

		// Token: 0x06002144 RID: 8516 RVA: 0x00079DD0 File Offset: 0x00077FD0
		public static bool operator ==(CustomAttributeNamedArgument left, CustomAttributeNamedArgument right)
		{
			return left.Equals(right);
		}

		// Token: 0x06002145 RID: 8517 RVA: 0x00079DE0 File Offset: 0x00077FE0
		public static bool operator !=(CustomAttributeNamedArgument left, CustomAttributeNamedArgument right)
		{
			return !left.Equals(right);
		}

		// Token: 0x04000C45 RID: 3141
		private CustomAttributeTypedArgument typedArgument;

		// Token: 0x04000C46 RID: 3142
		private MemberInfo memberInfo;
	}
}
