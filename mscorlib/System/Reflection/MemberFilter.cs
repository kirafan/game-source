﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020006EF RID: 1775
	// (Invoke) Token: 0x060043B0 RID: 17328
	[ComVisible(true)]
	[Serializable]
	public delegate bool MemberFilter(MemberInfo m, object filterCriteria);
}
