﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020006F3 RID: 1779
	// (Invoke) Token: 0x060043C0 RID: 17344
	[ComVisible(true)]
	[Serializable]
	public delegate bool TypeFilter(Type m, object filterCriteria);
}
