﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000271 RID: 625
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyCompanyAttribute : Attribute
	{
		// Token: 0x060020BA RID: 8378 RVA: 0x00077D1C File Offset: 0x00075F1C
		public AssemblyCompanyAttribute(string company)
		{
			this.name = company;
		}

		// Token: 0x17000572 RID: 1394
		// (get) Token: 0x060020BB RID: 8379 RVA: 0x00077D2C File Offset: 0x00075F2C
		public string Company
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C01 RID: 3073
		private string name;
	}
}
