﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200028B RID: 651
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum EventAttributes
	{
		// Token: 0x04000C4A RID: 3146
		None = 0,
		// Token: 0x04000C4B RID: 3147
		SpecialName = 512,
		// Token: 0x04000C4C RID: 3148
		ReservedMask = 1024,
		// Token: 0x04000C4D RID: 3149
		RTSpecialName = 1024
	}
}
