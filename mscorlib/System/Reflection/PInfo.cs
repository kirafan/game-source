﻿using System;

namespace System.Reflection
{
	// Token: 0x020002AB RID: 683
	[Flags]
	internal enum PInfo
	{
		// Token: 0x04000CFB RID: 3323
		Attributes = 1,
		// Token: 0x04000CFC RID: 3324
		GetMethod = 2,
		// Token: 0x04000CFD RID: 3325
		SetMethod = 4,
		// Token: 0x04000CFE RID: 3326
		ReflectedType = 8,
		// Token: 0x04000CFF RID: 3327
		DeclaringType = 16,
		// Token: 0x04000D00 RID: 3328
		Name = 32
	}
}
