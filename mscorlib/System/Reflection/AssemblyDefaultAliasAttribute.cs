﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000274 RID: 628
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyDefaultAliasAttribute : Attribute
	{
		// Token: 0x060020C0 RID: 8384 RVA: 0x00077D64 File Offset: 0x00075F64
		public AssemblyDefaultAliasAttribute(string defaultAlias)
		{
			this.name = defaultAlias;
		}

		// Token: 0x17000575 RID: 1397
		// (get) Token: 0x060020C1 RID: 8385 RVA: 0x00077D74 File Offset: 0x00075F74
		public string DefaultAlias
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C04 RID: 3076
		private string name;
	}
}
