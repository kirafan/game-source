﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000298 RID: 664
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum MemberTypes
	{
		// Token: 0x04000C8D RID: 3213
		Constructor = 1,
		// Token: 0x04000C8E RID: 3214
		Event = 2,
		// Token: 0x04000C8F RID: 3215
		Field = 4,
		// Token: 0x04000C90 RID: 3216
		Method = 8,
		// Token: 0x04000C91 RID: 3217
		Property = 16,
		// Token: 0x04000C92 RID: 3218
		TypeInfo = 32,
		// Token: 0x04000C93 RID: 3219
		Custom = 64,
		// Token: 0x04000C94 RID: 3220
		NestedType = 128,
		// Token: 0x04000C95 RID: 3221
		All = 191
	}
}
