﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000280 RID: 640
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyTitleAttribute : Attribute
	{
		// Token: 0x06002100 RID: 8448 RVA: 0x00078798 File Offset: 0x00076998
		public AssemblyTitleAttribute(string title)
		{
			this.name = title;
		}

		// Token: 0x1700058B RID: 1419
		// (get) Token: 0x06002101 RID: 8449 RVA: 0x000787A8 File Offset: 0x000769A8
		public string Title
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C22 RID: 3106
		private string name;
	}
}
