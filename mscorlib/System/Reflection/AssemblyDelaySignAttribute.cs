﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000275 RID: 629
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyDelaySignAttribute : Attribute
	{
		// Token: 0x060020C2 RID: 8386 RVA: 0x00077D7C File Offset: 0x00075F7C
		public AssemblyDelaySignAttribute(bool delaySign)
		{
			this.delay = delaySign;
		}

		// Token: 0x17000576 RID: 1398
		// (get) Token: 0x060020C3 RID: 8387 RVA: 0x00077D8C File Offset: 0x00075F8C
		public bool DelaySign
		{
			get
			{
				return this.delay;
			}
		}

		// Token: 0x04000C05 RID: 3077
		private bool delay;
	}
}
