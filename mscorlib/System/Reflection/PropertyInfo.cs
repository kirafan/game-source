﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002B6 RID: 694
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_PropertyInfo))]
	[Serializable]
	public abstract class PropertyInfo : MemberInfo, _PropertyInfo
	{
		// Token: 0x0600231C RID: 8988 RVA: 0x0007E13C File Offset: 0x0007C33C
		void _PropertyInfo.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600231D RID: 8989 RVA: 0x0007E144 File Offset: 0x0007C344
		void _PropertyInfo.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600231E RID: 8990 RVA: 0x0007E14C File Offset: 0x0007C34C
		void _PropertyInfo.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600231F RID: 8991 RVA: 0x0007E154 File Offset: 0x0007C354
		void _PropertyInfo.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000621 RID: 1569
		// (get) Token: 0x06002320 RID: 8992
		public abstract PropertyAttributes Attributes { get; }

		// Token: 0x17000622 RID: 1570
		// (get) Token: 0x06002321 RID: 8993
		public abstract bool CanRead { get; }

		// Token: 0x17000623 RID: 1571
		// (get) Token: 0x06002322 RID: 8994
		public abstract bool CanWrite { get; }

		// Token: 0x17000624 RID: 1572
		// (get) Token: 0x06002323 RID: 8995 RVA: 0x0007E15C File Offset: 0x0007C35C
		public bool IsSpecialName
		{
			get
			{
				return (this.Attributes & PropertyAttributes.SpecialName) != PropertyAttributes.None;
			}
		}

		// Token: 0x17000625 RID: 1573
		// (get) Token: 0x06002324 RID: 8996 RVA: 0x0007E170 File Offset: 0x0007C370
		public override MemberTypes MemberType
		{
			get
			{
				return MemberTypes.Property;
			}
		}

		// Token: 0x17000626 RID: 1574
		// (get) Token: 0x06002325 RID: 8997
		public abstract Type PropertyType { get; }

		// Token: 0x06002326 RID: 8998 RVA: 0x0007E174 File Offset: 0x0007C374
		public MethodInfo[] GetAccessors()
		{
			return this.GetAccessors(false);
		}

		// Token: 0x06002327 RID: 8999
		public abstract MethodInfo[] GetAccessors(bool nonPublic);

		// Token: 0x06002328 RID: 9000 RVA: 0x0007E180 File Offset: 0x0007C380
		public MethodInfo GetGetMethod()
		{
			return this.GetGetMethod(false);
		}

		// Token: 0x06002329 RID: 9001
		public abstract MethodInfo GetGetMethod(bool nonPublic);

		// Token: 0x0600232A RID: 9002
		public abstract ParameterInfo[] GetIndexParameters();

		// Token: 0x0600232B RID: 9003 RVA: 0x0007E18C File Offset: 0x0007C38C
		public MethodInfo GetSetMethod()
		{
			return this.GetSetMethod(false);
		}

		// Token: 0x0600232C RID: 9004
		public abstract MethodInfo GetSetMethod(bool nonPublic);

		// Token: 0x0600232D RID: 9005 RVA: 0x0007E198 File Offset: 0x0007C398
		[DebuggerStepThrough]
		[DebuggerHidden]
		public virtual object GetValue(object obj, object[] index)
		{
			return this.GetValue(obj, BindingFlags.Default, null, index, null);
		}

		// Token: 0x0600232E RID: 9006
		public abstract object GetValue(object obj, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture);

		// Token: 0x0600232F RID: 9007 RVA: 0x0007E1A8 File Offset: 0x0007C3A8
		[DebuggerStepThrough]
		[DebuggerHidden]
		public virtual void SetValue(object obj, object value, object[] index)
		{
			this.SetValue(obj, value, BindingFlags.Default, null, index, null);
		}

		// Token: 0x06002330 RID: 9008
		public abstract void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture);

		// Token: 0x06002331 RID: 9009 RVA: 0x0007E1B8 File Offset: 0x0007C3B8
		public virtual Type[] GetOptionalCustomModifiers()
		{
			return Type.EmptyTypes;
		}

		// Token: 0x06002332 RID: 9010 RVA: 0x0007E1C0 File Offset: 0x0007C3C0
		public virtual Type[] GetRequiredCustomModifiers()
		{
			return Type.EmptyTypes;
		}

		// Token: 0x06002333 RID: 9011 RVA: 0x0007E1C8 File Offset: 0x0007C3C8
		[MonoTODO("Not implemented")]
		public virtual object GetConstantValue()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002334 RID: 9012 RVA: 0x0007E1D0 File Offset: 0x0007C3D0
		[MonoTODO("Not implemented")]
		public virtual object GetRawConstantValue()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002335 RID: 9013 RVA: 0x0007E1D8 File Offset: 0x0007C3D8
		virtual Type GetType()
		{
			return base.GetType();
		}
	}
}
