﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002B4 RID: 692
	[ComVisible(true)]
	[Serializable]
	public enum ProcessorArchitecture
	{
		// Token: 0x04000D29 RID: 3369
		None,
		// Token: 0x04000D2A RID: 3370
		MSIL,
		// Token: 0x04000D2B RID: 3371
		X86,
		// Token: 0x04000D2C RID: 3372
		IA64,
		// Token: 0x04000D2D RID: 3373
		Amd64
	}
}
