﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200027D RID: 637
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum AssemblyNameFlags
	{
		// Token: 0x04000C1C RID: 3100
		None = 0,
		// Token: 0x04000C1D RID: 3101
		PublicKey = 1,
		// Token: 0x04000C1E RID: 3102
		Retargetable = 256,
		// Token: 0x04000C1F RID: 3103
		EnableJITcompileOptimizer = 16384,
		// Token: 0x04000C20 RID: 3104
		EnableJITcompileTracking = 32768
	}
}
