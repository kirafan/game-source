﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200027A RID: 634
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyKeyFileAttribute : Attribute
	{
		// Token: 0x060020CF RID: 8399 RVA: 0x00077E2C File Offset: 0x0007602C
		public AssemblyKeyFileAttribute(string keyFile)
		{
			this.name = keyFile;
		}

		// Token: 0x1700057C RID: 1404
		// (get) Token: 0x060020D0 RID: 8400 RVA: 0x00077E3C File Offset: 0x0007603C
		public string KeyFile
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C0A RID: 3082
		private string name;
	}
}
