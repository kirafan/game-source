﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020006F0 RID: 1776
	// (Invoke) Token: 0x060043B4 RID: 17332
	[ComVisible(true)]
	[Serializable]
	public delegate Module ModuleResolveEventHandler(object sender, ResolveEventArgs e);
}
