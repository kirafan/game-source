﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200028E RID: 654
	[Flags]
	[ComVisible(true)]
	public enum ExceptionHandlingClauseOptions
	{
		// Token: 0x04000C57 RID: 3159
		Clause = 0,
		// Token: 0x04000C58 RID: 3160
		Filter = 1,
		// Token: 0x04000C59 RID: 3161
		Finally = 2,
		// Token: 0x04000C5A RID: 3162
		Fault = 4
	}
}
