﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000037 RID: 55
	[Guid("AFBF15E5-C37C-11d2-B88E-00A0C9B471B8")]
	[ComVisible(true)]
	public interface IReflect
	{
		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000597 RID: 1431
		Type UnderlyingSystemType { get; }

		// Token: 0x06000598 RID: 1432
		FieldInfo GetField(string name, BindingFlags bindingAttr);

		// Token: 0x06000599 RID: 1433
		FieldInfo[] GetFields(BindingFlags bindingAttr);

		// Token: 0x0600059A RID: 1434
		MemberInfo[] GetMember(string name, BindingFlags bindingAttr);

		// Token: 0x0600059B RID: 1435
		MemberInfo[] GetMembers(BindingFlags bindingAttr);

		// Token: 0x0600059C RID: 1436
		MethodInfo GetMethod(string name, BindingFlags bindingAttr);

		// Token: 0x0600059D RID: 1437
		MethodInfo GetMethod(string name, BindingFlags bindingAttr, Binder binder, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x0600059E RID: 1438
		MethodInfo[] GetMethods(BindingFlags bindingAttr);

		// Token: 0x0600059F RID: 1439
		PropertyInfo[] GetProperties(BindingFlags bindingAttr);

		// Token: 0x060005A0 RID: 1440
		PropertyInfo GetProperty(string name, BindingFlags bindingAttr);

		// Token: 0x060005A1 RID: 1441
		PropertyInfo GetProperty(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x060005A2 RID: 1442
		object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters);
	}
}
