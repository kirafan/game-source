﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x020002B7 RID: 695
	[ComVisible(true)]
	[Serializable]
	public sealed class ReflectionTypeLoadException : SystemException
	{
		// Token: 0x06002336 RID: 9014 RVA: 0x0007E1E0 File Offset: 0x0007C3E0
		public ReflectionTypeLoadException(Type[] classes, Exception[] exceptions) : base(Locale.GetText("The classes in the module cannot be loaded."))
		{
			this.loaderExceptions = exceptions;
			this.types = classes;
		}

		// Token: 0x06002337 RID: 9015 RVA: 0x0007E200 File Offset: 0x0007C400
		public ReflectionTypeLoadException(Type[] classes, Exception[] exceptions, string message) : base(message)
		{
			this.loaderExceptions = exceptions;
			this.types = classes;
		}

		// Token: 0x06002338 RID: 9016 RVA: 0x0007E218 File Offset: 0x0007C418
		private ReflectionTypeLoadException(SerializationInfo info, StreamingContext sc) : base(info, sc)
		{
			this.types = (Type[])info.GetValue("Types", typeof(Type[]));
			this.loaderExceptions = (Exception[])info.GetValue("Exceptions", typeof(Exception[]));
		}

		// Token: 0x17000627 RID: 1575
		// (get) Token: 0x06002339 RID: 9017 RVA: 0x0007E270 File Offset: 0x0007C470
		public Type[] Types
		{
			get
			{
				return this.types;
			}
		}

		// Token: 0x17000628 RID: 1576
		// (get) Token: 0x0600233A RID: 9018 RVA: 0x0007E278 File Offset: 0x0007C478
		public Exception[] LoaderExceptions
		{
			get
			{
				return this.loaderExceptions;
			}
		}

		// Token: 0x0600233B RID: 9019 RVA: 0x0007E280 File Offset: 0x0007C480
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("Types", this.types);
			info.AddValue("Exceptions", this.loaderExceptions);
		}

		// Token: 0x04000D37 RID: 3383
		private Exception[] loaderExceptions;

		// Token: 0x04000D38 RID: 3384
		private Type[] types;
	}
}
