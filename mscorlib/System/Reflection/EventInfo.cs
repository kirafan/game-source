﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200028C RID: 652
	[ComDefaultInterface(typeof(_EventInfo))]
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[Serializable]
	public abstract class EventInfo : MemberInfo, _EventInfo
	{
		// Token: 0x0600214F RID: 8527 RVA: 0x00079FE8 File Offset: 0x000781E8
		void _EventInfo.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002150 RID: 8528 RVA: 0x00079FF0 File Offset: 0x000781F0
		void _EventInfo.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002151 RID: 8529 RVA: 0x00079FF8 File Offset: 0x000781F8
		void _EventInfo.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002152 RID: 8530 RVA: 0x0007A000 File Offset: 0x00078200
		void _EventInfo.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000596 RID: 1430
		// (get) Token: 0x06002153 RID: 8531
		public abstract EventAttributes Attributes { get; }

		// Token: 0x17000597 RID: 1431
		// (get) Token: 0x06002154 RID: 8532 RVA: 0x0007A008 File Offset: 0x00078208
		public Type EventHandlerType
		{
			get
			{
				MethodInfo addMethod = this.GetAddMethod(true);
				ParameterInfo[] parameters = addMethod.GetParameters();
				if (parameters.Length > 0)
				{
					return parameters[0].ParameterType;
				}
				return null;
			}
		}

		// Token: 0x17000598 RID: 1432
		// (get) Token: 0x06002155 RID: 8533 RVA: 0x0007A03C File Offset: 0x0007823C
		public bool IsMulticast
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000599 RID: 1433
		// (get) Token: 0x06002156 RID: 8534 RVA: 0x0007A040 File Offset: 0x00078240
		public bool IsSpecialName
		{
			get
			{
				return (this.Attributes & EventAttributes.SpecialName) != EventAttributes.None;
			}
		}

		// Token: 0x1700059A RID: 1434
		// (get) Token: 0x06002157 RID: 8535 RVA: 0x0007A054 File Offset: 0x00078254
		public override MemberTypes MemberType
		{
			get
			{
				return MemberTypes.Event;
			}
		}

		// Token: 0x06002158 RID: 8536 RVA: 0x0007A058 File Offset: 0x00078258
		[DebuggerStepThrough]
		[DebuggerHidden]
		public void AddEventHandler(object target, Delegate handler)
		{
			if (this.cached_add_event == null)
			{
				MethodInfo addMethod = this.GetAddMethod();
				if (addMethod == null)
				{
					throw new InvalidOperationException("Cannot add a handler to an event that doesn't have a visible add method");
				}
				if (addMethod.DeclaringType.IsValueType)
				{
					if (target == null && !addMethod.IsStatic)
					{
						throw new TargetException("Cannot add a handler to a non static event with a null target");
					}
					addMethod.Invoke(target, new object[]
					{
						handler
					});
					return;
				}
				else
				{
					this.cached_add_event = EventInfo.CreateAddEventDelegate(addMethod);
				}
			}
			this.cached_add_event(target, handler);
		}

		// Token: 0x06002159 RID: 8537 RVA: 0x0007A0E0 File Offset: 0x000782E0
		public MethodInfo GetAddMethod()
		{
			return this.GetAddMethod(false);
		}

		// Token: 0x0600215A RID: 8538
		public abstract MethodInfo GetAddMethod(bool nonPublic);

		// Token: 0x0600215B RID: 8539 RVA: 0x0007A0EC File Offset: 0x000782EC
		public MethodInfo GetRaiseMethod()
		{
			return this.GetRaiseMethod(false);
		}

		// Token: 0x0600215C RID: 8540
		public abstract MethodInfo GetRaiseMethod(bool nonPublic);

		// Token: 0x0600215D RID: 8541 RVA: 0x0007A0F8 File Offset: 0x000782F8
		public MethodInfo GetRemoveMethod()
		{
			return this.GetRemoveMethod(false);
		}

		// Token: 0x0600215E RID: 8542
		public abstract MethodInfo GetRemoveMethod(bool nonPublic);

		// Token: 0x0600215F RID: 8543 RVA: 0x0007A104 File Offset: 0x00078304
		public virtual MethodInfo[] GetOtherMethods(bool nonPublic)
		{
			return new MethodInfo[0];
		}

		// Token: 0x06002160 RID: 8544 RVA: 0x0007A10C File Offset: 0x0007830C
		public MethodInfo[] GetOtherMethods()
		{
			return this.GetOtherMethods(false);
		}

		// Token: 0x06002161 RID: 8545 RVA: 0x0007A118 File Offset: 0x00078318
		[DebuggerStepThrough]
		[DebuggerHidden]
		public void RemoveEventHandler(object target, Delegate handler)
		{
			MethodInfo removeMethod = this.GetRemoveMethod();
			if (removeMethod == null)
			{
				throw new InvalidOperationException("Cannot remove a handler to an event that doesn't have a visible remove method");
			}
			removeMethod.Invoke(target, new object[]
			{
				handler
			});
		}

		// Token: 0x06002162 RID: 8546 RVA: 0x0007A150 File Offset: 0x00078350
		private static void AddEventFrame<T, D>(EventInfo.AddEvent<T, D> addEvent, object obj, object dele)
		{
			if (obj == null)
			{
				throw new TargetException("Cannot add a handler to a non static event with a null target");
			}
			if (!(obj is T))
			{
				throw new TargetException("Object doesn't match target");
			}
			addEvent((T)((object)obj), (D)((object)dele));
		}

		// Token: 0x06002163 RID: 8547 RVA: 0x0007A18C File Offset: 0x0007838C
		private static void StaticAddEventAdapterFrame<D>(EventInfo.StaticAddEvent<D> addEvent, object obj, object dele)
		{
			addEvent((D)((object)dele));
		}

		// Token: 0x06002164 RID: 8548 RVA: 0x0007A19C File Offset: 0x0007839C
		private static EventInfo.AddEventAdapter CreateAddEventDelegate(MethodInfo method)
		{
			Type[] typeArguments;
			Type typeFromHandle;
			string name;
			if (method.IsStatic)
			{
				typeArguments = new Type[]
				{
					method.GetParameters()[0].ParameterType
				};
				typeFromHandle = typeof(EventInfo.StaticAddEvent<>);
				name = "StaticAddEventAdapterFrame";
			}
			else
			{
				typeArguments = new Type[]
				{
					method.DeclaringType,
					method.GetParameters()[0].ParameterType
				};
				typeFromHandle = typeof(EventInfo.AddEvent<, >);
				name = "AddEventFrame";
			}
			Type type = typeFromHandle.MakeGenericType(typeArguments);
			object firstArgument = Delegate.CreateDelegate(type, method);
			MethodInfo methodInfo = typeof(EventInfo).GetMethod(name, BindingFlags.Static | BindingFlags.NonPublic);
			methodInfo = methodInfo.MakeGenericMethod(typeArguments);
			return (EventInfo.AddEventAdapter)Delegate.CreateDelegate(typeof(EventInfo.AddEventAdapter), firstArgument, methodInfo, true);
		}

		// Token: 0x06002165 RID: 8549 RVA: 0x0007A25C File Offset: 0x0007845C
		virtual Type GetType()
		{
			return base.GetType();
		}

		// Token: 0x04000C4E RID: 3150
		private EventInfo.AddEventAdapter cached_add_event;

		// Token: 0x020006DC RID: 1756
		// (Invoke) Token: 0x06004364 RID: 17252
		private delegate void AddEventAdapter(object _this, Delegate dele);

		// Token: 0x020006DD RID: 1757
		// (Invoke) Token: 0x06004368 RID: 17256
		private delegate void AddEvent<T, D>(T _this, D dele);

		// Token: 0x020006DE RID: 1758
		// (Invoke) Token: 0x0600436C RID: 17260
		private delegate void StaticAddEvent<D>(D dele);
	}
}
