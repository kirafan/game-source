﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x0200026C RID: 620
	[ComVisible(true)]
	[Serializable]
	public sealed class AmbiguousMatchException : SystemException
	{
		// Token: 0x0600204A RID: 8266 RVA: 0x00077050 File Offset: 0x00075250
		public AmbiguousMatchException() : base("Ambiguous matching in method resolution")
		{
		}

		// Token: 0x0600204B RID: 8267 RVA: 0x00077060 File Offset: 0x00075260
		public AmbiguousMatchException(string message) : base(message)
		{
		}

		// Token: 0x0600204C RID: 8268 RVA: 0x0007706C File Offset: 0x0007526C
		public AmbiguousMatchException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x0600204D RID: 8269 RVA: 0x00077078 File Offset: 0x00075278
		internal AmbiguousMatchException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
