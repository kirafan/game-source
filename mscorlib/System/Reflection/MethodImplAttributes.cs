﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200029C RID: 668
	[ComVisible(true)]
	[Serializable]
	public enum MethodImplAttributes
	{
		// Token: 0x04000CB6 RID: 3254
		CodeTypeMask = 3,
		// Token: 0x04000CB7 RID: 3255
		IL = 0,
		// Token: 0x04000CB8 RID: 3256
		Native,
		// Token: 0x04000CB9 RID: 3257
		OPTIL,
		// Token: 0x04000CBA RID: 3258
		Runtime,
		// Token: 0x04000CBB RID: 3259
		ManagedMask,
		// Token: 0x04000CBC RID: 3260
		Unmanaged = 4,
		// Token: 0x04000CBD RID: 3261
		Managed = 0,
		// Token: 0x04000CBE RID: 3262
		ForwardRef = 16,
		// Token: 0x04000CBF RID: 3263
		PreserveSig = 128,
		// Token: 0x04000CC0 RID: 3264
		InternalCall = 4096,
		// Token: 0x04000CC1 RID: 3265
		Synchronized = 32,
		// Token: 0x04000CC2 RID: 3266
		NoInlining = 8,
		// Token: 0x04000CC3 RID: 3267
		MaxMethodImplVal = 65535
	}
}
