﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x0200029E RID: 670
	[ComVisible(true)]
	[Serializable]
	public sealed class Missing : ISerializable
	{
		// Token: 0x060021E9 RID: 8681 RVA: 0x0007AE10 File Offset: 0x00079010
		internal Missing()
		{
		}

		// Token: 0x060021EB RID: 8683 RVA: 0x0007AE24 File Offset: 0x00079024
		[MonoTODO]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
		}

		// Token: 0x04000CC4 RID: 3268
		public static readonly Missing Value = new Missing();
	}
}
