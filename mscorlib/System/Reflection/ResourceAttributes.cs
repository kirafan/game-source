﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002B8 RID: 696
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum ResourceAttributes
	{
		// Token: 0x04000D3A RID: 3386
		Public = 1,
		// Token: 0x04000D3B RID: 3387
		Private = 2
	}
}
