﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002EF RID: 751
	[ComVisible(true)]
	public struct OpCode
	{
		// Token: 0x060026A7 RID: 9895 RVA: 0x00089134 File Offset: 0x00087334
		internal OpCode(int p, int q)
		{
			this.op1 = (byte)(p & 255);
			this.op2 = (byte)(p >> 8 & 255);
			this.push = (byte)(p >> 16 & 255);
			this.pop = (byte)(p >> 24 & 255);
			this.size = (byte)(q & 255);
			this.type = (byte)(q >> 8 & 255);
			this.args = (byte)(q >> 16 & 255);
			this.flow = (byte)(q >> 24 & 255);
		}

		// Token: 0x060026A8 RID: 9896 RVA: 0x000891C4 File Offset: 0x000873C4
		public override int GetHashCode()
		{
			return this.Name.GetHashCode();
		}

		// Token: 0x060026A9 RID: 9897 RVA: 0x000891D4 File Offset: 0x000873D4
		public override bool Equals(object obj)
		{
			if (obj == null || !(obj is OpCode))
			{
				return false;
			}
			OpCode opCode = (OpCode)obj;
			return opCode.op1 == this.op1 && opCode.op2 == this.op2;
		}

		// Token: 0x060026AA RID: 9898 RVA: 0x00089220 File Offset: 0x00087420
		public bool Equals(OpCode obj)
		{
			return obj.op1 == this.op1 && obj.op2 == this.op2;
		}

		// Token: 0x060026AB RID: 9899 RVA: 0x00089254 File Offset: 0x00087454
		public override string ToString()
		{
			return this.Name;
		}

		// Token: 0x170006DE RID: 1758
		// (get) Token: 0x060026AC RID: 9900 RVA: 0x0008925C File Offset: 0x0008745C
		public string Name
		{
			get
			{
				if (this.op1 == 255)
				{
					return OpCodeNames.names[(int)this.op2];
				}
				return OpCodeNames.names[256 + (int)this.op2];
			}
		}

		// Token: 0x170006DF RID: 1759
		// (get) Token: 0x060026AD RID: 9901 RVA: 0x00089290 File Offset: 0x00087490
		public int Size
		{
			get
			{
				return (int)this.size;
			}
		}

		// Token: 0x170006E0 RID: 1760
		// (get) Token: 0x060026AE RID: 9902 RVA: 0x00089298 File Offset: 0x00087498
		public OpCodeType OpCodeType
		{
			get
			{
				return (OpCodeType)this.type;
			}
		}

		// Token: 0x170006E1 RID: 1761
		// (get) Token: 0x060026AF RID: 9903 RVA: 0x000892A0 File Offset: 0x000874A0
		public OperandType OperandType
		{
			get
			{
				return (OperandType)this.args;
			}
		}

		// Token: 0x170006E2 RID: 1762
		// (get) Token: 0x060026B0 RID: 9904 RVA: 0x000892A8 File Offset: 0x000874A8
		public FlowControl FlowControl
		{
			get
			{
				return (FlowControl)this.flow;
			}
		}

		// Token: 0x170006E3 RID: 1763
		// (get) Token: 0x060026B1 RID: 9905 RVA: 0x000892B0 File Offset: 0x000874B0
		public StackBehaviour StackBehaviourPop
		{
			get
			{
				return (StackBehaviour)this.pop;
			}
		}

		// Token: 0x170006E4 RID: 1764
		// (get) Token: 0x060026B2 RID: 9906 RVA: 0x000892B8 File Offset: 0x000874B8
		public StackBehaviour StackBehaviourPush
		{
			get
			{
				return (StackBehaviour)this.push;
			}
		}

		// Token: 0x170006E5 RID: 1765
		// (get) Token: 0x060026B3 RID: 9907 RVA: 0x000892C0 File Offset: 0x000874C0
		public short Value
		{
			get
			{
				if (this.size == 1)
				{
					return (short)this.op2;
				}
				return (short)((int)this.op1 << 8 | (int)this.op2);
			}
		}

		// Token: 0x060026B4 RID: 9908 RVA: 0x000892E8 File Offset: 0x000874E8
		public static bool operator ==(OpCode a, OpCode b)
		{
			return a.op1 == b.op1 && a.op2 == b.op2;
		}

		// Token: 0x060026B5 RID: 9909 RVA: 0x0008931C File Offset: 0x0008751C
		public static bool operator !=(OpCode a, OpCode b)
		{
			return a.op1 != b.op1 || a.op2 != b.op2;
		}

		// Token: 0x04000E82 RID: 3714
		internal byte op1;

		// Token: 0x04000E83 RID: 3715
		internal byte op2;

		// Token: 0x04000E84 RID: 3716
		private byte push;

		// Token: 0x04000E85 RID: 3717
		private byte pop;

		// Token: 0x04000E86 RID: 3718
		private byte size;

		// Token: 0x04000E87 RID: 3719
		private byte type;

		// Token: 0x04000E88 RID: 3720
		private byte args;

		// Token: 0x04000E89 RID: 3721
		private byte flow;
	}
}
