﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002F9 RID: 761
	[ComVisible(true)]
	[Serializable]
	public struct PropertyToken
	{
		// Token: 0x06002703 RID: 9987 RVA: 0x0008AC3C File Offset: 0x00088E3C
		internal PropertyToken(int val)
		{
			this.tokValue = val;
		}

		// Token: 0x06002705 RID: 9989 RVA: 0x0008AC64 File Offset: 0x00088E64
		public override bool Equals(object obj)
		{
			bool flag = obj is PropertyToken;
			if (flag)
			{
				PropertyToken propertyToken = (PropertyToken)obj;
				flag = (this.tokValue == propertyToken.tokValue);
			}
			return flag;
		}

		// Token: 0x06002706 RID: 9990 RVA: 0x0008AC9C File Offset: 0x00088E9C
		public bool Equals(PropertyToken obj)
		{
			return this.tokValue == obj.tokValue;
		}

		// Token: 0x06002707 RID: 9991 RVA: 0x0008ACB0 File Offset: 0x00088EB0
		public override int GetHashCode()
		{
			return this.tokValue;
		}

		// Token: 0x170006FD RID: 1789
		// (get) Token: 0x06002708 RID: 9992 RVA: 0x0008ACB8 File Offset: 0x00088EB8
		public int Token
		{
			get
			{
				return this.tokValue;
			}
		}

		// Token: 0x06002709 RID: 9993 RVA: 0x0008ACC0 File Offset: 0x00088EC0
		public static bool operator ==(PropertyToken a, PropertyToken b)
		{
			return object.Equals(a, b);
		}

		// Token: 0x0600270A RID: 9994 RVA: 0x0008ACD4 File Offset: 0x00088ED4
		public static bool operator !=(PropertyToken a, PropertyToken b)
		{
			return !object.Equals(a, b);
		}

		// Token: 0x04000FAE RID: 4014
		internal int tokValue;

		// Token: 0x04000FAF RID: 4015
		public static readonly PropertyToken Empty = default(PropertyToken);
	}
}
