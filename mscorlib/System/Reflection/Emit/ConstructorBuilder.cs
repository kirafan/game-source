﻿using System;
using System.Diagnostics.SymbolStore;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;

namespace System.Reflection.Emit
{
	// Token: 0x020002C6 RID: 710
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_ConstructorBuilder))]
	[ClassInterface(ClassInterfaceType.None)]
	public sealed class ConstructorBuilder : ConstructorInfo, _ConstructorBuilder
	{
		// Token: 0x060023C0 RID: 9152 RVA: 0x000805A8 File Offset: 0x0007E7A8
		internal ConstructorBuilder(TypeBuilder tb, MethodAttributes attributes, CallingConventions callingConvention, Type[] parameterTypes, Type[][] paramModReq, Type[][] paramModOpt)
		{
			this.attrs = (attributes | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName);
			this.call_conv = callingConvention;
			if (parameterTypes != null)
			{
				for (int i = 0; i < parameterTypes.Length; i++)
				{
					if (parameterTypes[i] == null)
					{
						throw new ArgumentException("Elements of the parameterTypes array cannot be null", "parameterTypes");
					}
				}
				this.parameters = new Type[parameterTypes.Length];
				Array.Copy(parameterTypes, this.parameters, parameterTypes.Length);
			}
			this.type = tb;
			this.paramModReq = paramModReq;
			this.paramModOpt = paramModOpt;
			this.table_idx = this.get_next_table_index(this, 6, true);
			((ModuleBuilder)tb.Module).RegisterToken(this, this.GetToken().Token);
		}

		// Token: 0x060023C1 RID: 9153 RVA: 0x00080678 File Offset: 0x0007E878
		void _ConstructorBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060023C2 RID: 9154 RVA: 0x00080680 File Offset: 0x0007E880
		void _ConstructorBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060023C3 RID: 9155 RVA: 0x00080688 File Offset: 0x0007E888
		void _ConstructorBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060023C4 RID: 9156 RVA: 0x00080690 File Offset: 0x0007E890
		void _ConstructorBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x1700063F RID: 1599
		// (get) Token: 0x060023C5 RID: 9157 RVA: 0x00080698 File Offset: 0x0007E898
		[MonoTODO]
		public override CallingConventions CallingConvention
		{
			get
			{
				return this.call_conv;
			}
		}

		// Token: 0x17000640 RID: 1600
		// (get) Token: 0x060023C6 RID: 9158 RVA: 0x000806A0 File Offset: 0x0007E8A0
		// (set) Token: 0x060023C7 RID: 9159 RVA: 0x000806A8 File Offset: 0x0007E8A8
		public bool InitLocals
		{
			get
			{
				return this.init_locals;
			}
			set
			{
				this.init_locals = value;
			}
		}

		// Token: 0x17000641 RID: 1601
		// (get) Token: 0x060023C8 RID: 9160 RVA: 0x000806B4 File Offset: 0x0007E8B4
		internal TypeBuilder TypeBuilder
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x060023C9 RID: 9161 RVA: 0x000806BC File Offset: 0x0007E8BC
		public override MethodImplAttributes GetMethodImplementationFlags()
		{
			return this.iattrs;
		}

		// Token: 0x060023CA RID: 9162 RVA: 0x000806C4 File Offset: 0x0007E8C4
		public override ParameterInfo[] GetParameters()
		{
			if (!this.type.is_created && !this.IsCompilerContext)
			{
				throw this.not_created();
			}
			return this.GetParametersInternal();
		}

		// Token: 0x060023CB RID: 9163 RVA: 0x000806FC File Offset: 0x0007E8FC
		internal ParameterInfo[] GetParametersInternal()
		{
			if (this.parameters == null)
			{
				return new ParameterInfo[0];
			}
			ParameterInfo[] array = new ParameterInfo[this.parameters.Length];
			for (int i = 0; i < this.parameters.Length; i++)
			{
				array[i] = new ParameterInfo((this.pinfo != null) ? this.pinfo[i + 1] : null, this.parameters[i], this, i + 1);
			}
			return array;
		}

		// Token: 0x060023CC RID: 9164 RVA: 0x00080774 File Offset: 0x0007E974
		internal override int GetParameterCount()
		{
			if (this.parameters == null)
			{
				return 0;
			}
			return this.parameters.Length;
		}

		// Token: 0x060023CD RID: 9165 RVA: 0x0008078C File Offset: 0x0007E98C
		public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			throw this.not_supported();
		}

		// Token: 0x060023CE RID: 9166 RVA: 0x00080794 File Offset: 0x0007E994
		public override object Invoke(BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			throw this.not_supported();
		}

		// Token: 0x17000642 RID: 1602
		// (get) Token: 0x060023CF RID: 9167 RVA: 0x0008079C File Offset: 0x0007E99C
		public override RuntimeMethodHandle MethodHandle
		{
			get
			{
				throw this.not_supported();
			}
		}

		// Token: 0x17000643 RID: 1603
		// (get) Token: 0x060023D0 RID: 9168 RVA: 0x000807A4 File Offset: 0x0007E9A4
		public override MethodAttributes Attributes
		{
			get
			{
				return this.attrs;
			}
		}

		// Token: 0x17000644 RID: 1604
		// (get) Token: 0x060023D1 RID: 9169 RVA: 0x000807AC File Offset: 0x0007E9AC
		public override Type ReflectedType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x17000645 RID: 1605
		// (get) Token: 0x060023D2 RID: 9170 RVA: 0x000807B4 File Offset: 0x0007E9B4
		public override Type DeclaringType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x17000646 RID: 1606
		// (get) Token: 0x060023D3 RID: 9171 RVA: 0x000807BC File Offset: 0x0007E9BC
		public Type ReturnType
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000647 RID: 1607
		// (get) Token: 0x060023D4 RID: 9172 RVA: 0x000807C0 File Offset: 0x0007E9C0
		public override string Name
		{
			get
			{
				return ((this.attrs & MethodAttributes.Static) == MethodAttributes.PrivateScope) ? ConstructorInfo.ConstructorName : ConstructorInfo.TypeConstructorName;
			}
		}

		// Token: 0x17000648 RID: 1608
		// (get) Token: 0x060023D5 RID: 9173 RVA: 0x000807E0 File Offset: 0x0007E9E0
		public string Signature
		{
			get
			{
				return "constructor signature";
			}
		}

		// Token: 0x060023D6 RID: 9174 RVA: 0x000807E8 File Offset: 0x0007E9E8
		public void AddDeclarativeSecurity(SecurityAction action, PermissionSet pset)
		{
			if (pset == null)
			{
				throw new ArgumentNullException("pset");
			}
			if (action == SecurityAction.RequestMinimum || action == SecurityAction.RequestOptional || action == SecurityAction.RequestRefuse)
			{
				throw new ArgumentOutOfRangeException("action", "Request* values are not permitted");
			}
			this.RejectIfCreated();
			if (this.permissions != null)
			{
				foreach (RefEmitPermissionSet refEmitPermissionSet in this.permissions)
				{
					if (refEmitPermissionSet.action == action)
					{
						throw new InvalidOperationException("Multiple permission sets specified with the same SecurityAction.");
					}
				}
				RefEmitPermissionSet[] array2 = new RefEmitPermissionSet[this.permissions.Length + 1];
				this.permissions.CopyTo(array2, 0);
				this.permissions = array2;
			}
			else
			{
				this.permissions = new RefEmitPermissionSet[1];
			}
			this.permissions[this.permissions.Length - 1] = new RefEmitPermissionSet(action, pset.ToXml().ToString());
			this.attrs |= MethodAttributes.HasSecurity;
		}

		// Token: 0x060023D7 RID: 9175 RVA: 0x000808F0 File Offset: 0x0007EAF0
		public ParameterBuilder DefineParameter(int iSequence, ParameterAttributes attributes, string strParamName)
		{
			if (iSequence < 1 || iSequence > this.GetParameterCount())
			{
				throw new ArgumentOutOfRangeException("iSequence");
			}
			if (this.type.is_created)
			{
				throw this.not_after_created();
			}
			ParameterBuilder parameterBuilder = new ParameterBuilder(this, iSequence, attributes, strParamName);
			if (this.pinfo == null)
			{
				this.pinfo = new ParameterBuilder[this.parameters.Length + 1];
			}
			this.pinfo[iSequence] = parameterBuilder;
			return parameterBuilder;
		}

		// Token: 0x060023D8 RID: 9176 RVA: 0x00080968 File Offset: 0x0007EB68
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			throw this.not_supported();
		}

		// Token: 0x060023D9 RID: 9177 RVA: 0x00080970 File Offset: 0x0007EB70
		public override object[] GetCustomAttributes(bool inherit)
		{
			if (this.type.is_created && this.IsCompilerContext)
			{
				return MonoCustomAttrs.GetCustomAttributes(this, inherit);
			}
			throw this.not_supported();
		}

		// Token: 0x060023DA RID: 9178 RVA: 0x000809A8 File Offset: 0x0007EBA8
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			if (this.type.is_created && this.IsCompilerContext)
			{
				return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
			}
			throw this.not_supported();
		}

		// Token: 0x060023DB RID: 9179 RVA: 0x000809E0 File Offset: 0x0007EBE0
		public ILGenerator GetILGenerator()
		{
			return this.GetILGenerator(64);
		}

		// Token: 0x060023DC RID: 9180 RVA: 0x000809EC File Offset: 0x0007EBEC
		public ILGenerator GetILGenerator(int streamSize)
		{
			if (this.ilgen != null)
			{
				return this.ilgen;
			}
			this.ilgen = new ILGenerator(this.type.Module, ((ModuleBuilder)this.type.Module).GetTokenGenerator(), streamSize);
			return this.ilgen;
		}

		// Token: 0x060023DD RID: 9181 RVA: 0x00080A40 File Offset: 0x0007EC40
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			if (customBuilder == null)
			{
				throw new ArgumentNullException("customBuilder");
			}
			string fullName = customBuilder.Ctor.ReflectedType.FullName;
			if (fullName == "System.Runtime.CompilerServices.MethodImplAttribute")
			{
				byte[] data = customBuilder.Data;
				int num = (int)data[2];
				num |= (int)data[3] << 8;
				this.SetImplementationFlags((MethodImplAttributes)num);
				return;
			}
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x060023DE RID: 9182 RVA: 0x00080AF0 File Offset: 0x0007ECF0
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			if (con == null)
			{
				throw new ArgumentNullException("con");
			}
			if (binaryAttribute == null)
			{
				throw new ArgumentNullException("binaryAttribute");
			}
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x060023DF RID: 9183 RVA: 0x00080B24 File Offset: 0x0007ED24
		public void SetImplementationFlags(MethodImplAttributes attributes)
		{
			if (this.type.is_created)
			{
				throw this.not_after_created();
			}
			this.iattrs = attributes;
		}

		// Token: 0x060023E0 RID: 9184 RVA: 0x00080B44 File Offset: 0x0007ED44
		public Module GetModule()
		{
			return this.type.Module;
		}

		// Token: 0x060023E1 RID: 9185 RVA: 0x00080B54 File Offset: 0x0007ED54
		public MethodToken GetToken()
		{
			return new MethodToken(100663296 | this.table_idx);
		}

		// Token: 0x060023E2 RID: 9186 RVA: 0x00080B68 File Offset: 0x0007ED68
		[MonoTODO]
		public void SetSymCustomAttribute(string name, byte[] data)
		{
			if (this.type.is_created)
			{
				throw this.not_after_created();
			}
		}

		// Token: 0x17000649 RID: 1609
		// (get) Token: 0x060023E3 RID: 9187 RVA: 0x00080B84 File Offset: 0x0007ED84
		public override Module Module
		{
			get
			{
				return base.Module;
			}
		}

		// Token: 0x060023E4 RID: 9188 RVA: 0x00080B8C File Offset: 0x0007ED8C
		public override string ToString()
		{
			return "ConstructorBuilder ['" + this.type.Name + "']";
		}

		// Token: 0x060023E5 RID: 9189 RVA: 0x00080BA8 File Offset: 0x0007EDA8
		internal void fixup()
		{
			if ((this.attrs & (MethodAttributes.Abstract | MethodAttributes.PinvokeImpl)) == MethodAttributes.PrivateScope && (this.iattrs & (MethodImplAttributes)4099) == MethodImplAttributes.IL && (this.ilgen == null || ILGenerator.Mono_GetCurrentOffset(this.ilgen) == 0))
			{
				throw new InvalidOperationException("Method '" + this.Name + "' does not have a method body.");
			}
			if (this.ilgen != null)
			{
				this.ilgen.label_fixup();
			}
		}

		// Token: 0x060023E6 RID: 9190 RVA: 0x00080C24 File Offset: 0x0007EE24
		internal void GenerateDebugInfo(ISymbolWriter symbolWriter)
		{
			if (this.ilgen != null && this.ilgen.HasDebugInfo)
			{
				SymbolToken symbolToken = new SymbolToken(this.GetToken().Token);
				symbolWriter.OpenMethod(symbolToken);
				symbolWriter.SetSymAttribute(symbolToken, "__name", Encoding.UTF8.GetBytes(this.Name));
				this.ilgen.GenerateDebugInfo(symbolWriter);
				symbolWriter.CloseMethod();
			}
		}

		// Token: 0x060023E7 RID: 9191 RVA: 0x00080C98 File Offset: 0x0007EE98
		internal override int get_next_table_index(object obj, int table, bool inc)
		{
			return this.type.get_next_table_index(obj, table, inc);
		}

		// Token: 0x1700064A RID: 1610
		// (get) Token: 0x060023E8 RID: 9192 RVA: 0x00080CA8 File Offset: 0x0007EEA8
		private bool IsCompilerContext
		{
			get
			{
				ModuleBuilder moduleBuilder = (ModuleBuilder)this.TypeBuilder.Module;
				AssemblyBuilder assemblyBuilder = (AssemblyBuilder)moduleBuilder.Assembly;
				return assemblyBuilder.IsCompilerContext;
			}
		}

		// Token: 0x060023E9 RID: 9193 RVA: 0x00080CD8 File Offset: 0x0007EED8
		private void RejectIfCreated()
		{
			if (this.type.is_created)
			{
				throw new InvalidOperationException("Type definition of the method is complete.");
			}
		}

		// Token: 0x060023EA RID: 9194 RVA: 0x00080CF8 File Offset: 0x0007EEF8
		private Exception not_supported()
		{
			return new NotSupportedException("The invoked member is not supported in a dynamic module.");
		}

		// Token: 0x060023EB RID: 9195 RVA: 0x00080D04 File Offset: 0x0007EF04
		private Exception not_after_created()
		{
			return new InvalidOperationException("Unable to change after type has been created.");
		}

		// Token: 0x060023EC RID: 9196 RVA: 0x00080D10 File Offset: 0x0007EF10
		private Exception not_created()
		{
			return new NotSupportedException("The type is not yet created.");
		}

		// Token: 0x04000DA3 RID: 3491
		private RuntimeMethodHandle mhandle;

		// Token: 0x04000DA4 RID: 3492
		private ILGenerator ilgen;

		// Token: 0x04000DA5 RID: 3493
		internal Type[] parameters;

		// Token: 0x04000DA6 RID: 3494
		private MethodAttributes attrs;

		// Token: 0x04000DA7 RID: 3495
		private MethodImplAttributes iattrs;

		// Token: 0x04000DA8 RID: 3496
		private int table_idx;

		// Token: 0x04000DA9 RID: 3497
		private CallingConventions call_conv;

		// Token: 0x04000DAA RID: 3498
		private TypeBuilder type;

		// Token: 0x04000DAB RID: 3499
		internal ParameterBuilder[] pinfo;

		// Token: 0x04000DAC RID: 3500
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000DAD RID: 3501
		private bool init_locals = true;

		// Token: 0x04000DAE RID: 3502
		private Type[][] paramModReq;

		// Token: 0x04000DAF RID: 3503
		private Type[][] paramModOpt;

		// Token: 0x04000DB0 RID: 3504
		private RefEmitPermissionSet[] permissions;
	}
}
