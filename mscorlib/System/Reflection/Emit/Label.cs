﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002E5 RID: 741
	[ComVisible(true)]
	[Serializable]
	public struct Label
	{
		// Token: 0x060025C7 RID: 9671 RVA: 0x00085F04 File Offset: 0x00084104
		internal Label(int val)
		{
			this.label = val;
		}

		// Token: 0x060025C8 RID: 9672 RVA: 0x00085F10 File Offset: 0x00084110
		public override bool Equals(object obj)
		{
			bool flag = obj is Label;
			if (flag)
			{
				Label label = (Label)obj;
				flag = (this.label == label.label);
			}
			return flag;
		}

		// Token: 0x060025C9 RID: 9673 RVA: 0x00085F48 File Offset: 0x00084148
		public bool Equals(Label obj)
		{
			return this.label == obj.label;
		}

		// Token: 0x060025CA RID: 9674 RVA: 0x00085F5C File Offset: 0x0008415C
		public override int GetHashCode()
		{
			return this.label.GetHashCode();
		}

		// Token: 0x060025CB RID: 9675 RVA: 0x00085F6C File Offset: 0x0008416C
		public static bool operator ==(Label a, Label b)
		{
			return a.Equals(b);
		}

		// Token: 0x060025CC RID: 9676 RVA: 0x00085F78 File Offset: 0x00084178
		public static bool operator !=(Label a, Label b)
		{
			return !(a == b);
		}

		// Token: 0x04000E3B RID: 3643
		internal int label;
	}
}
