﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x02000301 RID: 769
	[Obsolete("An alternate API is available: Emit the MarshalAs custom attribute instead.")]
	[ComVisible(true)]
	[Serializable]
	public sealed class UnmanagedMarshal
	{
		// Token: 0x060027C7 RID: 10183 RVA: 0x0008DDD0 File Offset: 0x0008BFD0
		private UnmanagedMarshal(UnmanagedType maint, int cnt)
		{
			this.count = cnt;
			this.t = maint;
			this.tbase = maint;
		}

		// Token: 0x060027C8 RID: 10184 RVA: 0x0008DDF0 File Offset: 0x0008BFF0
		private UnmanagedMarshal(UnmanagedType maint, UnmanagedType elemt)
		{
			this.count = 0;
			this.t = maint;
			this.tbase = elemt;
		}

		// Token: 0x17000719 RID: 1817
		// (get) Token: 0x060027C9 RID: 10185 RVA: 0x0008DE10 File Offset: 0x0008C010
		public UnmanagedType BaseType
		{
			get
			{
				if (this.t == UnmanagedType.LPArray || this.t == UnmanagedType.SafeArray)
				{
					throw new ArgumentException();
				}
				return this.tbase;
			}
		}

		// Token: 0x1700071A RID: 1818
		// (get) Token: 0x060027CA RID: 10186 RVA: 0x0008DE44 File Offset: 0x0008C044
		public int ElementCount
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x1700071B RID: 1819
		// (get) Token: 0x060027CB RID: 10187 RVA: 0x0008DE4C File Offset: 0x0008C04C
		public UnmanagedType GetUnmanagedType
		{
			get
			{
				return this.t;
			}
		}

		// Token: 0x1700071C RID: 1820
		// (get) Token: 0x060027CC RID: 10188 RVA: 0x0008DE54 File Offset: 0x0008C054
		public Guid IIDGuid
		{
			get
			{
				return new Guid(this.guid);
			}
		}

		// Token: 0x060027CD RID: 10189 RVA: 0x0008DE64 File Offset: 0x0008C064
		public static UnmanagedMarshal DefineByValArray(int elemCount)
		{
			return new UnmanagedMarshal(UnmanagedType.ByValArray, elemCount);
		}

		// Token: 0x060027CE RID: 10190 RVA: 0x0008DE70 File Offset: 0x0008C070
		public static UnmanagedMarshal DefineByValTStr(int elemCount)
		{
			return new UnmanagedMarshal(UnmanagedType.ByValTStr, elemCount);
		}

		// Token: 0x060027CF RID: 10191 RVA: 0x0008DE7C File Offset: 0x0008C07C
		public static UnmanagedMarshal DefineLPArray(UnmanagedType elemType)
		{
			return new UnmanagedMarshal(UnmanagedType.LPArray, elemType);
		}

		// Token: 0x060027D0 RID: 10192 RVA: 0x0008DE88 File Offset: 0x0008C088
		public static UnmanagedMarshal DefineSafeArray(UnmanagedType elemType)
		{
			return new UnmanagedMarshal(UnmanagedType.SafeArray, elemType);
		}

		// Token: 0x060027D1 RID: 10193 RVA: 0x0008DE94 File Offset: 0x0008C094
		public static UnmanagedMarshal DefineUnmanagedMarshal(UnmanagedType unmanagedType)
		{
			return new UnmanagedMarshal(unmanagedType, unmanagedType);
		}

		// Token: 0x060027D2 RID: 10194 RVA: 0x0008DEA0 File Offset: 0x0008C0A0
		public static UnmanagedMarshal DefineCustom(Type typeref, string cookie, string mtype, Guid id)
		{
			UnmanagedMarshal unmanagedMarshal = new UnmanagedMarshal(UnmanagedType.CustomMarshaler, UnmanagedType.CustomMarshaler);
			unmanagedMarshal.mcookie = cookie;
			unmanagedMarshal.marshaltype = mtype;
			unmanagedMarshal.marshaltyperef = typeref;
			if (id == Guid.Empty)
			{
				unmanagedMarshal.guid = string.Empty;
			}
			else
			{
				unmanagedMarshal.guid = id.ToString();
			}
			return unmanagedMarshal;
		}

		// Token: 0x060027D3 RID: 10195 RVA: 0x0008DEFC File Offset: 0x0008C0FC
		internal static UnmanagedMarshal DefineLPArrayInternal(UnmanagedType elemType, int sizeConst, int sizeParamIndex)
		{
			return new UnmanagedMarshal(UnmanagedType.LPArray, elemType)
			{
				count = sizeConst,
				param_num = sizeParamIndex,
				has_size = true
			};
		}

		// Token: 0x060027D4 RID: 10196 RVA: 0x0008DF28 File Offset: 0x0008C128
		internal MarshalAsAttribute ToMarshalAsAttribute()
		{
			MarshalAsAttribute marshalAsAttribute = new MarshalAsAttribute(this.t);
			marshalAsAttribute.ArraySubType = this.tbase;
			marshalAsAttribute.MarshalCookie = this.mcookie;
			marshalAsAttribute.MarshalType = this.marshaltype;
			marshalAsAttribute.MarshalTypeRef = this.marshaltyperef;
			if (this.count == -1)
			{
				marshalAsAttribute.SizeConst = 0;
			}
			else
			{
				marshalAsAttribute.SizeConst = this.count;
			}
			if (this.param_num == -1)
			{
				marshalAsAttribute.SizeParamIndex = 0;
			}
			else
			{
				marshalAsAttribute.SizeParamIndex = (short)this.param_num;
			}
			return marshalAsAttribute;
		}

		// Token: 0x04000FFC RID: 4092
		private int count;

		// Token: 0x04000FFD RID: 4093
		private UnmanagedType t;

		// Token: 0x04000FFE RID: 4094
		private UnmanagedType tbase;

		// Token: 0x04000FFF RID: 4095
		private string guid;

		// Token: 0x04001000 RID: 4096
		private string mcookie;

		// Token: 0x04001001 RID: 4097
		private string marshaltype;

		// Token: 0x04001002 RID: 4098
		private Type marshaltyperef;

		// Token: 0x04001003 RID: 4099
		private int param_num;

		// Token: 0x04001004 RID: 4100
		private bool has_size;
	}
}
