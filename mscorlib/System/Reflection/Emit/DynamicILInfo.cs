﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002CF RID: 719
	[ComVisible(true)]
	public class DynamicILInfo
	{
		// Token: 0x0600245C RID: 9308 RVA: 0x00082170 File Offset: 0x00080370
		internal DynamicILInfo()
		{
		}

		// Token: 0x17000666 RID: 1638
		// (get) Token: 0x0600245D RID: 9309 RVA: 0x00082178 File Offset: 0x00080378
		[MonoTODO]
		public DynamicMethod DynamicMethod
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0600245E RID: 9310 RVA: 0x00082180 File Offset: 0x00080380
		[MonoTODO]
		public int GetTokenFor(byte[] signature)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600245F RID: 9311 RVA: 0x00082188 File Offset: 0x00080388
		[MonoTODO]
		public int GetTokenFor(DynamicMethod method)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002460 RID: 9312 RVA: 0x00082190 File Offset: 0x00080390
		[MonoTODO]
		public int GetTokenFor(RuntimeFieldHandle field)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002461 RID: 9313 RVA: 0x00082198 File Offset: 0x00080398
		[MonoTODO]
		public int GetTokenFor(RuntimeMethodHandle method)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002462 RID: 9314 RVA: 0x000821A0 File Offset: 0x000803A0
		[MonoTODO]
		public int GetTokenFor(RuntimeTypeHandle type)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002463 RID: 9315 RVA: 0x000821A8 File Offset: 0x000803A8
		[MonoTODO]
		public int GetTokenFor(string literal)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002464 RID: 9316 RVA: 0x000821B0 File Offset: 0x000803B0
		[MonoTODO]
		public int GetTokenFor(RuntimeMethodHandle method, RuntimeTypeHandle contextType)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002465 RID: 9317 RVA: 0x000821B8 File Offset: 0x000803B8
		[MonoTODO]
		public void SetCode(byte[] code, int maxStackSize)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002466 RID: 9318 RVA: 0x000821C0 File Offset: 0x000803C0
		[CLSCompliant(false)]
		[MonoTODO]
		public unsafe void SetCode(byte* code, int codeSize, int maxStackSize)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002467 RID: 9319 RVA: 0x000821C8 File Offset: 0x000803C8
		[MonoTODO]
		public void SetExceptions(byte[] exceptions)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002468 RID: 9320 RVA: 0x000821D0 File Offset: 0x000803D0
		[CLSCompliant(false)]
		[MonoTODO]
		public unsafe void SetExceptions(byte* exceptions, int exceptionsSize)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002469 RID: 9321 RVA: 0x000821D8 File Offset: 0x000803D8
		[MonoTODO]
		public void SetLocalSignature(byte[] localSignature)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600246A RID: 9322 RVA: 0x000821E0 File Offset: 0x000803E0
		[MonoTODO]
		[CLSCompliant(false)]
		public unsafe void SetLocalSignature(byte* localSignature, int signatureSize)
		{
			throw new NotImplementedException();
		}
	}
}
