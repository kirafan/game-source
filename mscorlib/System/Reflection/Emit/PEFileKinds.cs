﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002F6 RID: 758
	[ComVisible(true)]
	[Serializable]
	public enum PEFileKinds
	{
		// Token: 0x04000F9B RID: 3995
		Dll = 1,
		// Token: 0x04000F9C RID: 3996
		ConsoleApplication,
		// Token: 0x04000F9D RID: 3997
		WindowApplication
	}
}
