﻿using System;

namespace System.Reflection.Emit
{
	// Token: 0x020002D2 RID: 722
	internal class DynamicMethodTokenGenerator : TokenGenerator
	{
		// Token: 0x06002496 RID: 9366 RVA: 0x00082944 File Offset: 0x00080B44
		public DynamicMethodTokenGenerator(DynamicMethod m)
		{
			this.m = m;
		}

		// Token: 0x06002497 RID: 9367 RVA: 0x00082954 File Offset: 0x00080B54
		public int GetToken(string str)
		{
			return this.m.AddRef(str);
		}

		// Token: 0x06002498 RID: 9368 RVA: 0x00082964 File Offset: 0x00080B64
		public int GetToken(MethodInfo method, Type[] opt_param_types)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x06002499 RID: 9369 RVA: 0x0008296C File Offset: 0x00080B6C
		public int GetToken(MemberInfo member)
		{
			return this.m.AddRef(member);
		}

		// Token: 0x0600249A RID: 9370 RVA: 0x0008297C File Offset: 0x00080B7C
		public int GetToken(SignatureHelper helper)
		{
			return this.m.AddRef(helper);
		}

		// Token: 0x04000DD2 RID: 3538
		private DynamicMethod m;
	}
}
