﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002F3 RID: 755
	[ComVisible(true)]
	[Serializable]
	public enum PackingSize
	{
		// Token: 0x04000F87 RID: 3975
		Unspecified,
		// Token: 0x04000F88 RID: 3976
		Size1,
		// Token: 0x04000F89 RID: 3977
		Size2,
		// Token: 0x04000F8A RID: 3978
		Size4 = 4,
		// Token: 0x04000F8B RID: 3979
		Size8 = 8,
		// Token: 0x04000F8C RID: 3980
		Size16 = 16,
		// Token: 0x04000F8D RID: 3981
		Size32 = 32,
		// Token: 0x04000F8E RID: 3982
		Size64 = 64,
		// Token: 0x04000F8F RID: 3983
		Size128 = 128
	}
}
