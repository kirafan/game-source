﻿using System;
using System.Collections;
using System.Diagnostics.SymbolStore;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002EB RID: 747
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_ModuleBuilder))]
	[ClassInterface(ClassInterfaceType.None)]
	public class ModuleBuilder : Module, _ModuleBuilder
	{
		// Token: 0x06002642 RID: 9794 RVA: 0x00087360 File Offset: 0x00085560
		internal ModuleBuilder(AssemblyBuilder assb, string name, string fullyqname, bool emitSymbolInfo, bool transient)
		{
			this.scopename = name;
			this.name = name;
			this.fqname = fullyqname;
			this.assemblyb = assb;
			this.assembly = assb;
			this.transient = transient;
			this.guid = Guid.FastNewGuidArray();
			this.table_idx = this.get_next_table_index(this, 0, true);
			this.name_cache = new Hashtable();
			ModuleBuilder.basic_init(this);
			this.CreateGlobalType();
			if (assb.IsRun)
			{
				TypeBuilder typeBuilder = new TypeBuilder(this, TypeAttributes.Abstract, 16777215);
				Type ab = typeBuilder.CreateType();
				ModuleBuilder.set_wrappers_type(this, ab);
			}
			if (emitSymbolInfo)
			{
				Assembly assembly = Assembly.LoadWithPartialName("Mono.CompilerServices.SymbolWriter");
				if (assembly == null)
				{
					throw new ExecutionEngineException("The assembly for default symbol writer cannot be loaded");
				}
				Type type = assembly.GetType("Mono.CompilerServices.SymbolWriter.SymbolWriterImpl");
				if (type == null)
				{
					throw new ExecutionEngineException("The type that implements the default symbol writer interface cannot be found");
				}
				this.symbolWriter = (ISymbolWriter)Activator.CreateInstance(type, new object[]
				{
					this
				});
				string text = this.fqname;
				if (this.assemblyb.AssemblyDir != null)
				{
					text = Path.Combine(this.assemblyb.AssemblyDir, text);
				}
				this.symbolWriter.Initialize(IntPtr.Zero, text, true);
			}
		}

		// Token: 0x06002644 RID: 9796 RVA: 0x000874C4 File Offset: 0x000856C4
		void _ModuleBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002645 RID: 9797 RVA: 0x000874CC File Offset: 0x000856CC
		void _ModuleBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002646 RID: 9798 RVA: 0x000874D4 File Offset: 0x000856D4
		void _ModuleBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002647 RID: 9799 RVA: 0x000874DC File Offset: 0x000856DC
		void _ModuleBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002648 RID: 9800
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void basic_init(ModuleBuilder ab);

		// Token: 0x06002649 RID: 9801
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void set_wrappers_type(ModuleBuilder mb, Type ab);

		// Token: 0x170006D4 RID: 1748
		// (get) Token: 0x0600264A RID: 9802 RVA: 0x000874E4 File Offset: 0x000856E4
		public override string FullyQualifiedName
		{
			get
			{
				return this.fqname;
			}
		}

		// Token: 0x0600264B RID: 9803 RVA: 0x000874EC File Offset: 0x000856EC
		public bool IsTransient()
		{
			return this.transient;
		}

		// Token: 0x0600264C RID: 9804 RVA: 0x000874F4 File Offset: 0x000856F4
		public void CreateGlobalFunctions()
		{
			if (this.global_type_created != null)
			{
				throw new InvalidOperationException("global methods already created");
			}
			if (this.global_type != null)
			{
				this.global_type_created = this.global_type.CreateType();
			}
		}

		// Token: 0x0600264D RID: 9805 RVA: 0x00087534 File Offset: 0x00085734
		public FieldBuilder DefineInitializedData(string name, byte[] data, FieldAttributes attributes)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			FieldBuilder fieldBuilder = this.DefineUninitializedData(name, data.Length, attributes | FieldAttributes.HasFieldRVA);
			fieldBuilder.SetRVAData(data);
			return fieldBuilder;
		}

		// Token: 0x0600264E RID: 9806 RVA: 0x0008756C File Offset: 0x0008576C
		public FieldBuilder DefineUninitializedData(string name, int size, FieldAttributes attributes)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (this.global_type_created != null)
			{
				throw new InvalidOperationException("global fields already created");
			}
			if (size <= 0 || size > 4128768)
			{
				throw new ArgumentException("size", "Data size must be > 0 and < 0x3f0000");
			}
			this.CreateGlobalType();
			string text = "$ArrayType$" + size;
			Type type = this.GetType(text, false, false);
			if (type == null)
			{
				TypeBuilder typeBuilder = this.DefineType(text, TypeAttributes.Public | TypeAttributes.ExplicitLayout | TypeAttributes.Sealed, this.assemblyb.corlib_value_type, null, PackingSize.Size1, size);
				typeBuilder.CreateType();
				type = typeBuilder;
			}
			FieldBuilder fieldBuilder = this.global_type.DefineField(name, type, attributes | FieldAttributes.Static);
			if (this.global_fields != null)
			{
				FieldBuilder[] array = new FieldBuilder[this.global_fields.Length + 1];
				Array.Copy(this.global_fields, array, this.global_fields.Length);
				array[this.global_fields.Length] = fieldBuilder;
				this.global_fields = array;
			}
			else
			{
				this.global_fields = new FieldBuilder[1];
				this.global_fields[0] = fieldBuilder;
			}
			return fieldBuilder;
		}

		// Token: 0x0600264F RID: 9807 RVA: 0x00087680 File Offset: 0x00085880
		private void addGlobalMethod(MethodBuilder mb)
		{
			if (this.global_methods != null)
			{
				MethodBuilder[] array = new MethodBuilder[this.global_methods.Length + 1];
				Array.Copy(this.global_methods, array, this.global_methods.Length);
				array[this.global_methods.Length] = mb;
				this.global_methods = array;
			}
			else
			{
				this.global_methods = new MethodBuilder[1];
				this.global_methods[0] = mb;
			}
		}

		// Token: 0x06002650 RID: 9808 RVA: 0x000876E8 File Offset: 0x000858E8
		public MethodBuilder DefineGlobalMethod(string name, MethodAttributes attributes, Type returnType, Type[] parameterTypes)
		{
			return this.DefineGlobalMethod(name, attributes, CallingConventions.Standard, returnType, parameterTypes);
		}

		// Token: 0x06002651 RID: 9809 RVA: 0x000876F8 File Offset: 0x000858F8
		public MethodBuilder DefineGlobalMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			return this.DefineGlobalMethod(name, attributes, callingConvention, returnType, null, null, parameterTypes, null, null);
		}

		// Token: 0x06002652 RID: 9810 RVA: 0x00087718 File Offset: 0x00085918
		public MethodBuilder DefineGlobalMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] requiredReturnTypeCustomModifiers, Type[] optionalReturnTypeCustomModifiers, Type[] parameterTypes, Type[][] requiredParameterTypeCustomModifiers, Type[][] optionalParameterTypeCustomModifiers)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if ((attributes & MethodAttributes.Static) == MethodAttributes.PrivateScope)
			{
				throw new ArgumentException("global methods must be static");
			}
			if (this.global_type_created != null)
			{
				throw new InvalidOperationException("global methods already created");
			}
			this.CreateGlobalType();
			MethodBuilder methodBuilder = this.global_type.DefineMethod(name, attributes, callingConvention, returnType, requiredReturnTypeCustomModifiers, optionalReturnTypeCustomModifiers, parameterTypes, requiredParameterTypeCustomModifiers, optionalParameterTypeCustomModifiers);
			this.addGlobalMethod(methodBuilder);
			return methodBuilder;
		}

		// Token: 0x06002653 RID: 9811 RVA: 0x0008778C File Offset: 0x0008598C
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			return this.DefinePInvokeMethod(name, dllName, name, attributes, callingConvention, returnType, parameterTypes, nativeCallConv, nativeCharSet);
		}

		// Token: 0x06002654 RID: 9812 RVA: 0x000877B0 File Offset: 0x000859B0
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, string entryName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if ((attributes & MethodAttributes.Static) == MethodAttributes.PrivateScope)
			{
				throw new ArgumentException("global methods must be static");
			}
			if (this.global_type_created != null)
			{
				throw new InvalidOperationException("global methods already created");
			}
			this.CreateGlobalType();
			MethodBuilder methodBuilder = this.global_type.DefinePInvokeMethod(name, dllName, entryName, attributes, callingConvention, returnType, parameterTypes, nativeCallConv, nativeCharSet);
			this.addGlobalMethod(methodBuilder);
			return methodBuilder;
		}

		// Token: 0x06002655 RID: 9813 RVA: 0x00087824 File Offset: 0x00085A24
		public TypeBuilder DefineType(string name)
		{
			return this.DefineType(name, TypeAttributes.NotPublic);
		}

		// Token: 0x06002656 RID: 9814 RVA: 0x00087830 File Offset: 0x00085A30
		public TypeBuilder DefineType(string name, TypeAttributes attr)
		{
			if ((attr & TypeAttributes.ClassSemanticsMask) != TypeAttributes.NotPublic)
			{
				return this.DefineType(name, attr, null, null);
			}
			return this.DefineType(name, attr, typeof(object), null);
		}

		// Token: 0x06002657 RID: 9815 RVA: 0x00087864 File Offset: 0x00085A64
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent)
		{
			return this.DefineType(name, attr, parent, null);
		}

		// Token: 0x06002658 RID: 9816 RVA: 0x00087870 File Offset: 0x00085A70
		private void AddType(TypeBuilder tb)
		{
			if (this.types != null)
			{
				if (this.types.Length == this.num_types)
				{
					TypeBuilder[] destinationArray = new TypeBuilder[this.types.Length * 2];
					Array.Copy(this.types, destinationArray, this.num_types);
					this.types = destinationArray;
				}
			}
			else
			{
				this.types = new TypeBuilder[1];
			}
			this.types[this.num_types] = tb;
			this.num_types++;
		}

		// Token: 0x06002659 RID: 9817 RVA: 0x000878F4 File Offset: 0x00085AF4
		private TypeBuilder DefineType(string name, TypeAttributes attr, Type parent, Type[] interfaces, PackingSize packingSize, int typesize)
		{
			if (this.name_cache.ContainsKey(name))
			{
				throw new ArgumentException("Duplicate type name within an assembly.");
			}
			TypeBuilder typeBuilder = new TypeBuilder(this, name, attr, parent, interfaces, packingSize, typesize, null);
			this.AddType(typeBuilder);
			this.name_cache.Add(name, typeBuilder);
			return typeBuilder;
		}

		// Token: 0x0600265A RID: 9818 RVA: 0x00087944 File Offset: 0x00085B44
		internal void RegisterTypeName(TypeBuilder tb, string name)
		{
			this.name_cache.Add(name, tb);
		}

		// Token: 0x0600265B RID: 9819 RVA: 0x00087954 File Offset: 0x00085B54
		internal TypeBuilder GetRegisteredType(string name)
		{
			return (TypeBuilder)this.name_cache[name];
		}

		// Token: 0x0600265C RID: 9820 RVA: 0x00087968 File Offset: 0x00085B68
		[ComVisible(true)]
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent, Type[] interfaces)
		{
			return this.DefineType(name, attr, parent, interfaces, PackingSize.Unspecified, 0);
		}

		// Token: 0x0600265D RID: 9821 RVA: 0x00087978 File Offset: 0x00085B78
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent, int typesize)
		{
			return this.DefineType(name, attr, parent, null, PackingSize.Unspecified, 0);
		}

		// Token: 0x0600265E RID: 9822 RVA: 0x00087988 File Offset: 0x00085B88
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent, PackingSize packsize)
		{
			return this.DefineType(name, attr, parent, null, packsize, 0);
		}

		// Token: 0x0600265F RID: 9823 RVA: 0x00087998 File Offset: 0x00085B98
		public TypeBuilder DefineType(string name, TypeAttributes attr, Type parent, PackingSize packingSize, int typesize)
		{
			return this.DefineType(name, attr, parent, null, packingSize, typesize);
		}

		// Token: 0x06002660 RID: 9824 RVA: 0x000879A8 File Offset: 0x00085BA8
		public MethodInfo GetArrayMethod(Type arrayClass, string methodName, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			return new MonoArrayMethod(arrayClass, methodName, callingConvention, returnType, parameterTypes);
		}

		// Token: 0x06002661 RID: 9825 RVA: 0x000879B8 File Offset: 0x00085BB8
		public EnumBuilder DefineEnum(string name, TypeAttributes visibility, Type underlyingType)
		{
			if (this.name_cache.Contains(name))
			{
				throw new ArgumentException("Duplicate type name within an assembly.");
			}
			EnumBuilder enumBuilder = new EnumBuilder(this, name, visibility, underlyingType);
			TypeBuilder typeBuilder = enumBuilder.GetTypeBuilder();
			this.AddType(typeBuilder);
			this.name_cache.Add(name, typeBuilder);
			return enumBuilder;
		}

		// Token: 0x06002662 RID: 9826 RVA: 0x00087A08 File Offset: 0x00085C08
		[ComVisible(true)]
		public override Type GetType(string className)
		{
			return this.GetType(className, false, false);
		}

		// Token: 0x06002663 RID: 9827 RVA: 0x00087A14 File Offset: 0x00085C14
		[ComVisible(true)]
		public override Type GetType(string className, bool ignoreCase)
		{
			return this.GetType(className, false, ignoreCase);
		}

		// Token: 0x06002664 RID: 9828 RVA: 0x00087A20 File Offset: 0x00085C20
		private TypeBuilder search_in_array(TypeBuilder[] arr, int validElementsInArray, string className)
		{
			for (int i = 0; i < validElementsInArray; i++)
			{
				if (string.Compare(className, arr[i].FullName, true, CultureInfo.InvariantCulture) == 0)
				{
					return arr[i];
				}
			}
			return null;
		}

		// Token: 0x06002665 RID: 9829 RVA: 0x00087A60 File Offset: 0x00085C60
		private TypeBuilder search_nested_in_array(TypeBuilder[] arr, int validElementsInArray, string className)
		{
			for (int i = 0; i < validElementsInArray; i++)
			{
				if (string.Compare(className, arr[i].Name, true, CultureInfo.InvariantCulture) == 0)
				{
					return arr[i];
				}
			}
			return null;
		}

		// Token: 0x06002666 RID: 9830
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Type create_modified_type(TypeBuilder tb, string modifiers);

		// Token: 0x06002667 RID: 9831 RVA: 0x00087AA0 File Offset: 0x00085CA0
		private TypeBuilder GetMaybeNested(TypeBuilder t, string className)
		{
			int num = className.IndexOf('+');
			if (num >= 0)
			{
				if (t.subtypes != null)
				{
					string className2 = className.Substring(0, num);
					string className3 = className.Substring(num + 1);
					TypeBuilder typeBuilder = this.search_nested_in_array(t.subtypes, t.subtypes.Length, className2);
					if (typeBuilder != null)
					{
						return this.GetMaybeNested(typeBuilder, className3);
					}
				}
				return null;
			}
			if (t.subtypes != null)
			{
				return this.search_nested_in_array(t.subtypes, t.subtypes.Length, className);
			}
			return null;
		}

		// Token: 0x06002668 RID: 9832 RVA: 0x00087B24 File Offset: 0x00085D24
		[ComVisible(true)]
		public override Type GetType(string className, bool throwOnError, bool ignoreCase)
		{
			if (className == null)
			{
				throw new ArgumentNullException("className");
			}
			if (className.Length == 0)
			{
				throw new ArgumentException("className");
			}
			string message = className;
			TypeBuilder typeBuilder = null;
			if (this.types == null && throwOnError)
			{
				throw new TypeLoadException(className);
			}
			int num = className.IndexOfAny(ModuleBuilder.type_modifiers);
			string text;
			if (num >= 0)
			{
				text = className.Substring(num);
				className = className.Substring(0, num);
			}
			else
			{
				text = null;
			}
			if (!ignoreCase)
			{
				typeBuilder = (this.name_cache[className] as TypeBuilder);
			}
			else
			{
				num = className.IndexOf('+');
				if (num < 0)
				{
					if (this.types != null)
					{
						typeBuilder = this.search_in_array(this.types, this.num_types, className);
					}
				}
				else
				{
					string className2 = className.Substring(0, num);
					string className3 = className.Substring(num + 1);
					typeBuilder = this.search_in_array(this.types, this.num_types, className2);
					if (typeBuilder != null)
					{
						typeBuilder = this.GetMaybeNested(typeBuilder, className3);
					}
				}
			}
			if (typeBuilder == null && throwOnError)
			{
				throw new TypeLoadException(message);
			}
			if (typeBuilder != null && text != null)
			{
				Type type = ModuleBuilder.create_modified_type(typeBuilder, text);
				typeBuilder = (type as TypeBuilder);
				if (typeBuilder == null)
				{
					return type;
				}
			}
			if (typeBuilder != null && typeBuilder.is_created)
			{
				return typeBuilder.CreateType();
			}
			return typeBuilder;
		}

		// Token: 0x06002669 RID: 9833 RVA: 0x00087C80 File Offset: 0x00085E80
		internal int get_next_table_index(object obj, int table, bool inc)
		{
			if (this.table_indexes == null)
			{
				this.table_indexes = new int[64];
				for (int i = 0; i < 64; i++)
				{
					this.table_indexes[i] = 1;
				}
				this.table_indexes[2] = 2;
			}
			if (inc)
			{
				return this.table_indexes[table]++;
			}
			return this.table_indexes[table];
		}

		// Token: 0x0600266A RID: 9834 RVA: 0x00087CF0 File Offset: 0x00085EF0
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x0600266B RID: 9835 RVA: 0x00087D54 File Offset: 0x00085F54
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x0600266C RID: 9836 RVA: 0x00087D64 File Offset: 0x00085F64
		public ISymbolWriter GetSymWriter()
		{
			return this.symbolWriter;
		}

		// Token: 0x0600266D RID: 9837 RVA: 0x00087D6C File Offset: 0x00085F6C
		public ISymbolDocumentWriter DefineDocument(string url, Guid language, Guid languageVendor, Guid documentType)
		{
			if (this.symbolWriter != null)
			{
				return this.symbolWriter.DefineDocument(url, language, languageVendor, documentType);
			}
			return null;
		}

		// Token: 0x0600266E RID: 9838 RVA: 0x00087D8C File Offset: 0x00085F8C
		public override Type[] GetTypes()
		{
			if (this.types == null)
			{
				return Type.EmptyTypes;
			}
			int num = this.num_types;
			Type[] array = new Type[num];
			Array.Copy(this.types, array, num);
			for (int i = 0; i < array.Length; i++)
			{
				if (this.types[i].is_created)
				{
					array[i] = this.types[i].CreateType();
				}
			}
			return array;
		}

		// Token: 0x0600266F RID: 9839 RVA: 0x00087DFC File Offset: 0x00085FFC
		public IResourceWriter DefineResource(string name, string description, ResourceAttributes attribute)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name == string.Empty)
			{
				throw new ArgumentException("name cannot be empty");
			}
			if (this.transient)
			{
				throw new InvalidOperationException("The module is transient");
			}
			if (!this.assemblyb.IsSave)
			{
				throw new InvalidOperationException("The assembly is transient");
			}
			ResourceWriter resourceWriter = new ResourceWriter(new MemoryStream());
			if (this.resource_writers == null)
			{
				this.resource_writers = new Hashtable();
			}
			this.resource_writers[name] = resourceWriter;
			if (this.resources != null)
			{
				MonoResource[] destinationArray = new MonoResource[this.resources.Length + 1];
				Array.Copy(this.resources, destinationArray, this.resources.Length);
				this.resources = destinationArray;
			}
			else
			{
				this.resources = new MonoResource[1];
			}
			int num = this.resources.Length - 1;
			this.resources[num].name = name;
			this.resources[num].attrs = attribute;
			return resourceWriter;
		}

		// Token: 0x06002670 RID: 9840 RVA: 0x00087F0C File Offset: 0x0008610C
		public IResourceWriter DefineResource(string name, string description)
		{
			return this.DefineResource(name, description, ResourceAttributes.Public);
		}

		// Token: 0x06002671 RID: 9841 RVA: 0x00087F18 File Offset: 0x00086118
		[MonoTODO]
		public void DefineUnmanagedResource(byte[] resource)
		{
			if (resource == null)
			{
				throw new ArgumentNullException("resource");
			}
			throw new NotImplementedException();
		}

		// Token: 0x06002672 RID: 9842 RVA: 0x00087F30 File Offset: 0x00086130
		[MonoTODO]
		public void DefineUnmanagedResource(string resourceFileName)
		{
			if (resourceFileName == null)
			{
				throw new ArgumentNullException("resourceFileName");
			}
			if (resourceFileName == string.Empty)
			{
				throw new ArgumentException("resourceFileName");
			}
			if (!File.Exists(resourceFileName) || Directory.Exists(resourceFileName))
			{
				throw new FileNotFoundException("File '" + resourceFileName + "' does not exists or is a directory.");
			}
			throw new NotImplementedException();
		}

		// Token: 0x06002673 RID: 9843 RVA: 0x00087F9C File Offset: 0x0008619C
		public void DefineManifestResource(string name, Stream stream, ResourceAttributes attribute)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name == string.Empty)
			{
				throw new ArgumentException("name cannot be empty");
			}
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (this.transient)
			{
				throw new InvalidOperationException("The module is transient");
			}
			if (!this.assemblyb.IsSave)
			{
				throw new InvalidOperationException("The assembly is transient");
			}
			if (this.resources != null)
			{
				MonoResource[] destinationArray = new MonoResource[this.resources.Length + 1];
				Array.Copy(this.resources, destinationArray, this.resources.Length);
				this.resources = destinationArray;
			}
			else
			{
				this.resources = new MonoResource[1];
			}
			int num = this.resources.Length - 1;
			this.resources[num].name = name;
			this.resources[num].attrs = attribute;
			this.resources[num].stream = stream;
		}

		// Token: 0x06002674 RID: 9844 RVA: 0x000880A0 File Offset: 0x000862A0
		[MonoTODO]
		public void SetSymCustomAttribute(string name, byte[] data)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002675 RID: 9845 RVA: 0x000880A8 File Offset: 0x000862A8
		[MonoTODO]
		public void SetUserEntryPoint(MethodInfo entryPoint)
		{
			if (entryPoint == null)
			{
				throw new ArgumentNullException("entryPoint");
			}
			if (entryPoint.DeclaringType.Module != this)
			{
				throw new InvalidOperationException("entryPoint is not contained in this module");
			}
			throw new NotImplementedException();
		}

		// Token: 0x06002676 RID: 9846 RVA: 0x000880E8 File Offset: 0x000862E8
		public MethodToken GetMethodToken(MethodInfo method)
		{
			if (method == null)
			{
				throw new ArgumentNullException("method");
			}
			if (method.DeclaringType.Module != this)
			{
				throw new InvalidOperationException("The method is not in this module");
			}
			return new MethodToken(this.GetToken(method));
		}

		// Token: 0x06002677 RID: 9847 RVA: 0x00088124 File Offset: 0x00086324
		public MethodToken GetArrayMethodToken(Type arrayClass, string methodName, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			return this.GetMethodToken(this.GetArrayMethod(arrayClass, methodName, callingConvention, returnType, parameterTypes));
		}

		// Token: 0x06002678 RID: 9848 RVA: 0x0008813C File Offset: 0x0008633C
		[ComVisible(true)]
		public MethodToken GetConstructorToken(ConstructorInfo con)
		{
			if (con == null)
			{
				throw new ArgumentNullException("con");
			}
			return new MethodToken(this.GetToken(con));
		}

		// Token: 0x06002679 RID: 9849 RVA: 0x0008815C File Offset: 0x0008635C
		public FieldToken GetFieldToken(FieldInfo field)
		{
			if (field == null)
			{
				throw new ArgumentNullException("field");
			}
			if (field.DeclaringType.Module != this)
			{
				throw new InvalidOperationException("The method is not in this module");
			}
			return new FieldToken(this.GetToken(field));
		}

		// Token: 0x0600267A RID: 9850 RVA: 0x00088198 File Offset: 0x00086398
		[MonoTODO]
		public SignatureToken GetSignatureToken(byte[] sigBytes, int sigLength)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600267B RID: 9851 RVA: 0x000881A0 File Offset: 0x000863A0
		public SignatureToken GetSignatureToken(SignatureHelper sigHelper)
		{
			if (sigHelper == null)
			{
				throw new ArgumentNullException("sigHelper");
			}
			return new SignatureToken(this.GetToken(sigHelper));
		}

		// Token: 0x0600267C RID: 9852 RVA: 0x000881C0 File Offset: 0x000863C0
		public StringToken GetStringConstant(string str)
		{
			if (str == null)
			{
				throw new ArgumentNullException("str");
			}
			return new StringToken(this.GetToken(str));
		}

		// Token: 0x0600267D RID: 9853 RVA: 0x000881E0 File Offset: 0x000863E0
		public TypeToken GetTypeToken(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (type.IsByRef)
			{
				throw new ArgumentException("type can't be a byref type", "type");
			}
			if (!this.IsTransient() && type.Module is ModuleBuilder && ((ModuleBuilder)type.Module).IsTransient())
			{
				throw new InvalidOperationException("a non-transient module can't reference a transient module");
			}
			return new TypeToken(this.GetToken(type));
		}

		// Token: 0x0600267E RID: 9854 RVA: 0x00088260 File Offset: 0x00086460
		public TypeToken GetTypeToken(string name)
		{
			return this.GetTypeToken(this.GetType(name));
		}

		// Token: 0x0600267F RID: 9855
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int getUSIndex(ModuleBuilder mb, string str);

		// Token: 0x06002680 RID: 9856
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int getToken(ModuleBuilder mb, object obj);

		// Token: 0x06002681 RID: 9857
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern int getMethodToken(ModuleBuilder mb, MethodInfo method, Type[] opt_param_types);

		// Token: 0x06002682 RID: 9858 RVA: 0x00088270 File Offset: 0x00086470
		internal int GetToken(string str)
		{
			if (this.us_string_cache.Contains(str))
			{
				return (int)this.us_string_cache[str];
			}
			int usindex = ModuleBuilder.getUSIndex(this, str);
			this.us_string_cache[str] = usindex;
			return usindex;
		}

		// Token: 0x06002683 RID: 9859 RVA: 0x000882BC File Offset: 0x000864BC
		internal int GetToken(MemberInfo member)
		{
			return ModuleBuilder.getToken(this, member);
		}

		// Token: 0x06002684 RID: 9860 RVA: 0x000882C8 File Offset: 0x000864C8
		internal int GetToken(MethodInfo method, Type[] opt_param_types)
		{
			return ModuleBuilder.getMethodToken(this, method, opt_param_types);
		}

		// Token: 0x06002685 RID: 9861 RVA: 0x000882D4 File Offset: 0x000864D4
		internal int GetToken(SignatureHelper helper)
		{
			return ModuleBuilder.getToken(this, helper);
		}

		// Token: 0x06002686 RID: 9862
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern void RegisterToken(object obj, int token);

		// Token: 0x06002687 RID: 9863 RVA: 0x000882E0 File Offset: 0x000864E0
		internal TokenGenerator GetTokenGenerator()
		{
			if (this.token_gen == null)
			{
				this.token_gen = new ModuleBuilderTokenGenerator(this);
			}
			return this.token_gen;
		}

		// Token: 0x06002688 RID: 9864
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void build_metadata(ModuleBuilder mb);

		// Token: 0x06002689 RID: 9865
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void WriteToFile(IntPtr handle);

		// Token: 0x0600268A RID: 9866 RVA: 0x00088300 File Offset: 0x00086500
		internal void Save()
		{
			if (this.transient && !this.is_main)
			{
				return;
			}
			if (this.types != null)
			{
				for (int i = 0; i < this.num_types; i++)
				{
					if (!this.types[i].is_created)
					{
						throw new NotSupportedException("Type '" + this.types[i].FullName + "' was not completed.");
					}
				}
			}
			if (this.global_type != null && this.global_type_created == null)
			{
				this.global_type_created = this.global_type.CreateType();
			}
			if (this.resources != null)
			{
				for (int j = 0; j < this.resources.Length; j++)
				{
					IResourceWriter resourceWriter;
					if (this.resource_writers != null && (resourceWriter = (this.resource_writers[this.resources[j].name] as IResourceWriter)) != null)
					{
						ResourceWriter resourceWriter2 = (ResourceWriter)resourceWriter;
						resourceWriter2.Generate();
						MemoryStream memoryStream = (MemoryStream)resourceWriter2.Stream;
						this.resources[j].data = new byte[memoryStream.Length];
						memoryStream.Seek(0L, SeekOrigin.Begin);
						memoryStream.Read(this.resources[j].data, 0, (int)memoryStream.Length);
					}
					else
					{
						Stream stream = this.resources[j].stream;
						if (stream != null)
						{
							try
							{
								long length = stream.Length;
								this.resources[j].data = new byte[length];
								stream.Seek(0L, SeekOrigin.Begin);
								stream.Read(this.resources[j].data, 0, (int)length);
							}
							catch
							{
							}
						}
					}
				}
			}
			ModuleBuilder.build_metadata(this);
			string text = this.fqname;
			if (this.assemblyb.AssemblyDir != null)
			{
				text = Path.Combine(this.assemblyb.AssemblyDir, text);
			}
			try
			{
				File.Delete(text);
			}
			catch
			{
			}
			using (FileStream fileStream = new FileStream(text, FileMode.Create, FileAccess.Write))
			{
				this.WriteToFile(fileStream.Handle);
			}
			File.SetAttributes(text, (FileAttributes)(-2147483648));
			if (this.types != null && this.symbolWriter != null)
			{
				for (int k = 0; k < this.num_types; k++)
				{
					this.types[k].GenerateDebugInfo(this.symbolWriter);
				}
				this.symbolWriter.Close();
			}
		}

		// Token: 0x170006D5 RID: 1749
		// (get) Token: 0x0600268B RID: 9867 RVA: 0x000885EC File Offset: 0x000867EC
		internal string FileName
		{
			get
			{
				return this.fqname;
			}
		}

		// Token: 0x170006D6 RID: 1750
		// (set) Token: 0x0600268C RID: 9868 RVA: 0x000885F4 File Offset: 0x000867F4
		internal bool IsMain
		{
			set
			{
				this.is_main = value;
			}
		}

		// Token: 0x0600268D RID: 9869 RVA: 0x00088600 File Offset: 0x00086800
		internal void CreateGlobalType()
		{
			if (this.global_type == null)
			{
				this.global_type = new TypeBuilder(this, TypeAttributes.NotPublic, 1);
			}
		}

		// Token: 0x0600268E RID: 9870 RVA: 0x0008861C File Offset: 0x0008681C
		internal override Guid GetModuleVersionId()
		{
			return new Guid(this.guid);
		}

		// Token: 0x0600268F RID: 9871 RVA: 0x0008862C File Offset: 0x0008682C
		internal static Guid Mono_GetGuid(ModuleBuilder mb)
		{
			return mb.GetModuleVersionId();
		}

		// Token: 0x04000E64 RID: 3684
		private UIntPtr dynamic_image;

		// Token: 0x04000E65 RID: 3685
		private int num_types;

		// Token: 0x04000E66 RID: 3686
		private TypeBuilder[] types;

		// Token: 0x04000E67 RID: 3687
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000E68 RID: 3688
		private byte[] guid;

		// Token: 0x04000E69 RID: 3689
		private int table_idx;

		// Token: 0x04000E6A RID: 3690
		internal AssemblyBuilder assemblyb;

		// Token: 0x04000E6B RID: 3691
		private MethodBuilder[] global_methods;

		// Token: 0x04000E6C RID: 3692
		private FieldBuilder[] global_fields;

		// Token: 0x04000E6D RID: 3693
		private bool is_main;

		// Token: 0x04000E6E RID: 3694
		private MonoResource[] resources;

		// Token: 0x04000E6F RID: 3695
		private TypeBuilder global_type;

		// Token: 0x04000E70 RID: 3696
		private Type global_type_created;

		// Token: 0x04000E71 RID: 3697
		private Hashtable name_cache;

		// Token: 0x04000E72 RID: 3698
		private Hashtable us_string_cache = new Hashtable();

		// Token: 0x04000E73 RID: 3699
		private int[] table_indexes;

		// Token: 0x04000E74 RID: 3700
		private bool transient;

		// Token: 0x04000E75 RID: 3701
		private ModuleBuilderTokenGenerator token_gen;

		// Token: 0x04000E76 RID: 3702
		private Hashtable resource_writers;

		// Token: 0x04000E77 RID: 3703
		private ISymbolWriter symbolWriter;

		// Token: 0x04000E78 RID: 3704
		private static readonly char[] type_modifiers = new char[]
		{
			'&',
			'[',
			'*'
		};
	}
}
