﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002FC RID: 764
	[ComVisible(true)]
	public struct SignatureToken
	{
		// Token: 0x0600272C RID: 10028 RVA: 0x0008B380 File Offset: 0x00089580
		internal SignatureToken(int val)
		{
			this.tokValue = val;
		}

		// Token: 0x0600272E RID: 10030 RVA: 0x0008B3A8 File Offset: 0x000895A8
		public override bool Equals(object obj)
		{
			bool flag = obj is SignatureToken;
			if (flag)
			{
				SignatureToken signatureToken = (SignatureToken)obj;
				flag = (this.tokValue == signatureToken.tokValue);
			}
			return flag;
		}

		// Token: 0x0600272F RID: 10031 RVA: 0x0008B3E0 File Offset: 0x000895E0
		public bool Equals(SignatureToken obj)
		{
			return this.tokValue == obj.tokValue;
		}

		// Token: 0x06002730 RID: 10032 RVA: 0x0008B3F4 File Offset: 0x000895F4
		public override int GetHashCode()
		{
			return this.tokValue;
		}

		// Token: 0x170006FE RID: 1790
		// (get) Token: 0x06002731 RID: 10033 RVA: 0x0008B3FC File Offset: 0x000895FC
		public int Token
		{
			get
			{
				return this.tokValue;
			}
		}

		// Token: 0x06002732 RID: 10034 RVA: 0x0008B404 File Offset: 0x00089604
		public static bool operator ==(SignatureToken a, SignatureToken b)
		{
			return object.Equals(a, b);
		}

		// Token: 0x06002733 RID: 10035 RVA: 0x0008B418 File Offset: 0x00089618
		public static bool operator !=(SignatureToken a, SignatureToken b)
		{
			return !object.Equals(a, b);
		}

		// Token: 0x04000FBD RID: 4029
		internal int tokValue;

		// Token: 0x04000FBE RID: 4030
		public static readonly SignatureToken Empty = default(SignatureToken);
	}
}
