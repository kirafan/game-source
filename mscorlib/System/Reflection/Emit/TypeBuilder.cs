﻿using System;
using System.Collections;
using System.Diagnostics.SymbolStore;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

namespace System.Reflection.Emit
{
	// Token: 0x020002FF RID: 767
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_TypeBuilder))]
	[ClassInterface(ClassInterfaceType.None)]
	public sealed class TypeBuilder : Type, _TypeBuilder
	{
		// Token: 0x0600273C RID: 10044 RVA: 0x0008B4C8 File Offset: 0x000896C8
		internal TypeBuilder(ModuleBuilder mb, TypeAttributes attr, int table_idx)
		{
			this.parent = null;
			this.attrs = attr;
			this.class_size = 0;
			this.table_idx = table_idx;
			this.fullname = (this.tname = ((table_idx != 1) ? ("type_" + table_idx) : "<Module>"));
			this.nspace = string.Empty;
			this.pmodule = mb;
			this.setup_internal_class(this);
		}

		// Token: 0x0600273D RID: 10045 RVA: 0x0008B540 File Offset: 0x00089740
		internal TypeBuilder(ModuleBuilder mb, string name, TypeAttributes attr, Type parent, Type[] interfaces, PackingSize packing_size, int type_size, Type nesting_type)
		{
			this.parent = parent;
			this.attrs = attr;
			this.class_size = type_size;
			this.packing_size = packing_size;
			this.nesting_type = nesting_type;
			this.check_name("fullname", name);
			if (parent == null && (attr & TypeAttributes.ClassSemanticsMask) != TypeAttributes.NotPublic && (attr & TypeAttributes.Abstract) == TypeAttributes.NotPublic)
			{
				throw new InvalidOperationException("Interface must be declared abstract.");
			}
			int num = name.LastIndexOf('.');
			if (num != -1)
			{
				this.tname = name.Substring(num + 1);
				this.nspace = name.Substring(0, num);
			}
			else
			{
				this.tname = name;
				this.nspace = string.Empty;
			}
			if (interfaces != null)
			{
				this.interfaces = new Type[interfaces.Length];
				Array.Copy(interfaces, this.interfaces, interfaces.Length);
			}
			this.pmodule = mb;
			if ((attr & TypeAttributes.ClassSemanticsMask) == TypeAttributes.NotPublic && parent == null && !this.IsCompilerContext)
			{
				this.parent = typeof(object);
			}
			this.table_idx = mb.get_next_table_index(this, 2, true);
			this.setup_internal_class(this);
			this.fullname = this.GetFullName();
		}

		// Token: 0x0600273E RID: 10046 RVA: 0x0008B66C File Offset: 0x0008986C
		void _TypeBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600273F RID: 10047 RVA: 0x0008B674 File Offset: 0x00089874
		void _TypeBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002740 RID: 10048 RVA: 0x0008B67C File Offset: 0x0008987C
		void _TypeBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002741 RID: 10049 RVA: 0x0008B684 File Offset: 0x00089884
		void _TypeBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002742 RID: 10050 RVA: 0x0008B68C File Offset: 0x0008988C
		protected override TypeAttributes GetAttributeFlagsImpl()
		{
			return this.attrs;
		}

		// Token: 0x06002743 RID: 10051
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void setup_internal_class(TypeBuilder tb);

		// Token: 0x06002744 RID: 10052
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void create_internal_class(TypeBuilder tb);

		// Token: 0x06002745 RID: 10053
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void setup_generic_class();

		// Token: 0x06002746 RID: 10054
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void create_generic_class();

		// Token: 0x06002747 RID: 10055
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern EventInfo get_event_info(EventBuilder eb);

		// Token: 0x17000700 RID: 1792
		// (get) Token: 0x06002748 RID: 10056 RVA: 0x0008B694 File Offset: 0x00089894
		public override Assembly Assembly
		{
			get
			{
				return this.pmodule.Assembly;
			}
		}

		// Token: 0x17000701 RID: 1793
		// (get) Token: 0x06002749 RID: 10057 RVA: 0x0008B6A4 File Offset: 0x000898A4
		public override string AssemblyQualifiedName
		{
			get
			{
				return this.fullname + ", " + this.Assembly.FullName;
			}
		}

		// Token: 0x17000702 RID: 1794
		// (get) Token: 0x0600274A RID: 10058 RVA: 0x0008B6C4 File Offset: 0x000898C4
		public override Type BaseType
		{
			get
			{
				return this.parent;
			}
		}

		// Token: 0x17000703 RID: 1795
		// (get) Token: 0x0600274B RID: 10059 RVA: 0x0008B6CC File Offset: 0x000898CC
		public override Type DeclaringType
		{
			get
			{
				return this.nesting_type;
			}
		}

		// Token: 0x17000704 RID: 1796
		// (get) Token: 0x0600274C RID: 10060 RVA: 0x0008B6D4 File Offset: 0x000898D4
		public override Type UnderlyingSystemType
		{
			get
			{
				if (this.is_created)
				{
					return this.created.UnderlyingSystemType;
				}
				if (!this.IsEnum || this.IsCompilerContext)
				{
					return this;
				}
				if (this.underlying_type != null)
				{
					return this.underlying_type;
				}
				throw new InvalidOperationException("Enumeration type is not defined.");
			}
		}

		// Token: 0x0600274D RID: 10061 RVA: 0x0008B72C File Offset: 0x0008992C
		private string GetFullName()
		{
			if (this.nesting_type != null)
			{
				return this.nesting_type.FullName + "+" + this.tname;
			}
			if (this.nspace != null && this.nspace.Length > 0)
			{
				return this.nspace + "." + this.tname;
			}
			return this.tname;
		}

		// Token: 0x17000705 RID: 1797
		// (get) Token: 0x0600274E RID: 10062 RVA: 0x0008B79C File Offset: 0x0008999C
		public override string FullName
		{
			get
			{
				return this.fullname;
			}
		}

		// Token: 0x17000706 RID: 1798
		// (get) Token: 0x0600274F RID: 10063 RVA: 0x0008B7A4 File Offset: 0x000899A4
		public override Guid GUID
		{
			get
			{
				this.check_created();
				return this.created.GUID;
			}
		}

		// Token: 0x17000707 RID: 1799
		// (get) Token: 0x06002750 RID: 10064 RVA: 0x0008B7B8 File Offset: 0x000899B8
		public override Module Module
		{
			get
			{
				return this.pmodule;
			}
		}

		// Token: 0x17000708 RID: 1800
		// (get) Token: 0x06002751 RID: 10065 RVA: 0x0008B7C0 File Offset: 0x000899C0
		public override string Name
		{
			get
			{
				return this.tname;
			}
		}

		// Token: 0x17000709 RID: 1801
		// (get) Token: 0x06002752 RID: 10066 RVA: 0x0008B7C8 File Offset: 0x000899C8
		public override string Namespace
		{
			get
			{
				return this.nspace;
			}
		}

		// Token: 0x1700070A RID: 1802
		// (get) Token: 0x06002753 RID: 10067 RVA: 0x0008B7D0 File Offset: 0x000899D0
		public PackingSize PackingSize
		{
			get
			{
				return this.packing_size;
			}
		}

		// Token: 0x1700070B RID: 1803
		// (get) Token: 0x06002754 RID: 10068 RVA: 0x0008B7D8 File Offset: 0x000899D8
		public int Size
		{
			get
			{
				return this.class_size;
			}
		}

		// Token: 0x1700070C RID: 1804
		// (get) Token: 0x06002755 RID: 10069 RVA: 0x0008B7E0 File Offset: 0x000899E0
		public override Type ReflectedType
		{
			get
			{
				return this.nesting_type;
			}
		}

		// Token: 0x06002756 RID: 10070 RVA: 0x0008B7E8 File Offset: 0x000899E8
		public void AddDeclarativeSecurity(SecurityAction action, PermissionSet pset)
		{
			if (pset == null)
			{
				throw new ArgumentNullException("pset");
			}
			if (action == SecurityAction.RequestMinimum || action == SecurityAction.RequestOptional || action == SecurityAction.RequestRefuse)
			{
				throw new ArgumentOutOfRangeException("Request* values are not permitted", "action");
			}
			this.check_not_created();
			if (this.permissions != null)
			{
				foreach (RefEmitPermissionSet refEmitPermissionSet in this.permissions)
				{
					if (refEmitPermissionSet.action == action)
					{
						throw new InvalidOperationException("Multiple permission sets specified with the same SecurityAction.");
					}
				}
				RefEmitPermissionSet[] array2 = new RefEmitPermissionSet[this.permissions.Length + 1];
				this.permissions.CopyTo(array2, 0);
				this.permissions = array2;
			}
			else
			{
				this.permissions = new RefEmitPermissionSet[1];
			}
			this.permissions[this.permissions.Length - 1] = new RefEmitPermissionSet(action, pset.ToXml().ToString());
			this.attrs |= TypeAttributes.HasSecurity;
		}

		// Token: 0x06002757 RID: 10071 RVA: 0x0008B8F0 File Offset: 0x00089AF0
		[ComVisible(true)]
		public void AddInterfaceImplementation(Type interfaceType)
		{
			if (interfaceType == null)
			{
				throw new ArgumentNullException("interfaceType");
			}
			this.check_not_created();
			if (this.interfaces != null)
			{
				foreach (Type type in this.interfaces)
				{
					if (type == interfaceType)
					{
						return;
					}
				}
				Type[] array2 = new Type[this.interfaces.Length + 1];
				this.interfaces.CopyTo(array2, 0);
				array2[this.interfaces.Length] = interfaceType;
				this.interfaces = array2;
			}
			else
			{
				this.interfaces = new Type[1];
				this.interfaces[0] = interfaceType;
			}
		}

		// Token: 0x06002758 RID: 10072 RVA: 0x0008B990 File Offset: 0x00089B90
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			this.check_created();
			if (this.created != typeof(object))
			{
				return this.created.GetConstructor(bindingAttr, binder, callConvention, types, modifiers);
			}
			if (this.ctors == null)
			{
				return null;
			}
			ConstructorBuilder constructorBuilder = null;
			int num = 0;
			foreach (ConstructorBuilder constructorBuilder2 in this.ctors)
			{
				if (callConvention == CallingConventions.Any || constructorBuilder2.CallingConvention == callConvention)
				{
					constructorBuilder = constructorBuilder2;
					num++;
				}
			}
			if (num == 0)
			{
				return null;
			}
			if (types != null)
			{
				MethodBase[] array2 = new MethodBase[num];
				if (num == 1)
				{
					array2[0] = constructorBuilder;
				}
				else
				{
					num = 0;
					foreach (ConstructorBuilder constructorInfo in this.ctors)
					{
						if (callConvention == CallingConventions.Any || constructorInfo.CallingConvention == callConvention)
						{
							array2[num++] = constructorInfo;
						}
					}
				}
				if (binder == null)
				{
					binder = Binder.DefaultBinder;
				}
				return (ConstructorInfo)binder.SelectMethod(bindingAttr, array2, types, modifiers);
			}
			if (num > 1)
			{
				throw new AmbiguousMatchException();
			}
			return constructorBuilder;
		}

		// Token: 0x06002759 RID: 10073 RVA: 0x0008BAC4 File Offset: 0x00089CC4
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			if (!this.is_created && !this.IsCompilerContext)
			{
				throw new NotSupportedException();
			}
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x0600275A RID: 10074 RVA: 0x0008BAF8 File Offset: 0x00089CF8
		public override object[] GetCustomAttributes(bool inherit)
		{
			this.check_created();
			return this.created.GetCustomAttributes(inherit);
		}

		// Token: 0x0600275B RID: 10075 RVA: 0x0008BB0C File Offset: 0x00089D0C
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			this.check_created();
			return this.created.GetCustomAttributes(attributeType, inherit);
		}

		// Token: 0x0600275C RID: 10076 RVA: 0x0008BB24 File Offset: 0x00089D24
		public TypeBuilder DefineNestedType(string name)
		{
			return this.DefineNestedType(name, TypeAttributes.NestedPrivate, this.pmodule.assemblyb.corlib_object_type, null);
		}

		// Token: 0x0600275D RID: 10077 RVA: 0x0008BB40 File Offset: 0x00089D40
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr)
		{
			return this.DefineNestedType(name, attr, this.pmodule.assemblyb.corlib_object_type, null);
		}

		// Token: 0x0600275E RID: 10078 RVA: 0x0008BB5C File Offset: 0x00089D5C
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent)
		{
			return this.DefineNestedType(name, attr, parent, null);
		}

		// Token: 0x0600275F RID: 10079 RVA: 0x0008BB68 File Offset: 0x00089D68
		private TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent, Type[] interfaces, PackingSize packSize, int typeSize)
		{
			if (interfaces != null)
			{
				for (int i = 0; i < interfaces.Length; i++)
				{
					if (interfaces[i] == null)
					{
						throw new ArgumentNullException("interfaces");
					}
				}
			}
			TypeBuilder typeBuilder = new TypeBuilder(this.pmodule, name, attr, parent, interfaces, packSize, typeSize, this);
			typeBuilder.fullname = typeBuilder.GetFullName();
			this.pmodule.RegisterTypeName(typeBuilder, typeBuilder.fullname);
			if (this.subtypes != null)
			{
				TypeBuilder[] array = new TypeBuilder[this.subtypes.Length + 1];
				Array.Copy(this.subtypes, array, this.subtypes.Length);
				array[this.subtypes.Length] = typeBuilder;
				this.subtypes = array;
			}
			else
			{
				this.subtypes = new TypeBuilder[1];
				this.subtypes[0] = typeBuilder;
			}
			return typeBuilder;
		}

		// Token: 0x06002760 RID: 10080 RVA: 0x0008BC3C File Offset: 0x00089E3C
		[ComVisible(true)]
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent, Type[] interfaces)
		{
			return this.DefineNestedType(name, attr, parent, interfaces, PackingSize.Unspecified, 0);
		}

		// Token: 0x06002761 RID: 10081 RVA: 0x0008BC4C File Offset: 0x00089E4C
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent, int typeSize)
		{
			return this.DefineNestedType(name, attr, parent, null, PackingSize.Unspecified, typeSize);
		}

		// Token: 0x06002762 RID: 10082 RVA: 0x0008BC5C File Offset: 0x00089E5C
		public TypeBuilder DefineNestedType(string name, TypeAttributes attr, Type parent, PackingSize packSize)
		{
			return this.DefineNestedType(name, attr, parent, null, packSize, 0);
		}

		// Token: 0x06002763 RID: 10083 RVA: 0x0008BC6C File Offset: 0x00089E6C
		[ComVisible(true)]
		public ConstructorBuilder DefineConstructor(MethodAttributes attributes, CallingConventions callingConvention, Type[] parameterTypes)
		{
			return this.DefineConstructor(attributes, callingConvention, parameterTypes, null, null);
		}

		// Token: 0x06002764 RID: 10084 RVA: 0x0008BC7C File Offset: 0x00089E7C
		[ComVisible(true)]
		public ConstructorBuilder DefineConstructor(MethodAttributes attributes, CallingConventions callingConvention, Type[] parameterTypes, Type[][] requiredCustomModifiers, Type[][] optionalCustomModifiers)
		{
			this.check_not_created();
			ConstructorBuilder constructorBuilder = new ConstructorBuilder(this, attributes, callingConvention, parameterTypes, requiredCustomModifiers, optionalCustomModifiers);
			if (this.ctors != null)
			{
				ConstructorBuilder[] array = new ConstructorBuilder[this.ctors.Length + 1];
				Array.Copy(this.ctors, array, this.ctors.Length);
				array[this.ctors.Length] = constructorBuilder;
				this.ctors = array;
			}
			else
			{
				this.ctors = new ConstructorBuilder[1];
				this.ctors[0] = constructorBuilder;
			}
			return constructorBuilder;
		}

		// Token: 0x06002765 RID: 10085 RVA: 0x0008BCFC File Offset: 0x00089EFC
		[ComVisible(true)]
		public ConstructorBuilder DefineDefaultConstructor(MethodAttributes attributes)
		{
			Type corlib_object_type;
			if (this.parent != null)
			{
				corlib_object_type = this.parent;
			}
			else
			{
				corlib_object_type = this.pmodule.assemblyb.corlib_object_type;
			}
			ConstructorInfo constructor = corlib_object_type.GetConstructor(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic, null, Type.EmptyTypes, null);
			if (constructor == null)
			{
				throw new NotSupportedException("Parent does not have a default constructor. The default constructor must be explicitly defined.");
			}
			ConstructorBuilder constructorBuilder = this.DefineConstructor(attributes, CallingConventions.Standard, Type.EmptyTypes);
			ILGenerator ilgenerator = constructorBuilder.GetILGenerator();
			ilgenerator.Emit(OpCodes.Ldarg_0);
			ilgenerator.Emit(OpCodes.Call, constructor);
			ilgenerator.Emit(OpCodes.Ret);
			return constructorBuilder;
		}

		// Token: 0x06002766 RID: 10086 RVA: 0x0008BD8C File Offset: 0x00089F8C
		private void append_method(MethodBuilder mb)
		{
			if (this.methods != null)
			{
				if (this.methods.Length == this.num_methods)
				{
					MethodBuilder[] destinationArray = new MethodBuilder[this.methods.Length * 2];
					Array.Copy(this.methods, destinationArray, this.num_methods);
					this.methods = destinationArray;
				}
			}
			else
			{
				this.methods = new MethodBuilder[1];
			}
			this.methods[this.num_methods] = mb;
			this.num_methods++;
		}

		// Token: 0x06002767 RID: 10087 RVA: 0x0008BE10 File Offset: 0x0008A010
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes, Type returnType, Type[] parameterTypes)
		{
			return this.DefineMethod(name, attributes, CallingConventions.Standard, returnType, parameterTypes);
		}

		// Token: 0x06002768 RID: 10088 RVA: 0x0008BE20 File Offset: 0x0008A020
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes)
		{
			return this.DefineMethod(name, attributes, callingConvention, returnType, null, null, parameterTypes, null, null);
		}

		// Token: 0x06002769 RID: 10089 RVA: 0x0008BE40 File Offset: 0x0008A040
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] returnTypeRequiredCustomModifiers, Type[] returnTypeOptionalCustomModifiers, Type[] parameterTypes, Type[][] parameterTypeRequiredCustomModifiers, Type[][] parameterTypeOptionalCustomModifiers)
		{
			this.check_name("name", name);
			this.check_not_created();
			if (this.IsInterface && ((attributes & MethodAttributes.Abstract) == MethodAttributes.PrivateScope || (attributes & MethodAttributes.Virtual) == MethodAttributes.PrivateScope) && (attributes & MethodAttributes.Static) == MethodAttributes.PrivateScope)
			{
				throw new ArgumentException("Interface method must be abstract and virtual.");
			}
			if (returnType == null)
			{
				returnType = this.pmodule.assemblyb.corlib_void_type;
			}
			MethodBuilder methodBuilder = new MethodBuilder(this, name, attributes, callingConvention, returnType, returnTypeRequiredCustomModifiers, returnTypeOptionalCustomModifiers, parameterTypes, parameterTypeRequiredCustomModifiers, parameterTypeOptionalCustomModifiers);
			this.append_method(methodBuilder);
			return methodBuilder;
		}

		// Token: 0x0600276A RID: 10090 RVA: 0x0008BECC File Offset: 0x0008A0CC
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, string entryName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			return this.DefinePInvokeMethod(name, dllName, entryName, attributes, callingConvention, returnType, null, null, parameterTypes, null, null, nativeCallConv, nativeCharSet);
		}

		// Token: 0x0600276B RID: 10091 RVA: 0x0008BEF4 File Offset: 0x0008A0F4
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, string entryName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] returnTypeRequiredCustomModifiers, Type[] returnTypeOptionalCustomModifiers, Type[] parameterTypes, Type[][] parameterTypeRequiredCustomModifiers, Type[][] parameterTypeOptionalCustomModifiers, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			this.check_name("name", name);
			this.check_name("dllName", dllName);
			this.check_name("entryName", entryName);
			if ((attributes & MethodAttributes.Abstract) != MethodAttributes.PrivateScope)
			{
				throw new ArgumentException("PInvoke methods must be static and native and cannot be abstract.");
			}
			if (this.IsInterface)
			{
				throw new ArgumentException("PInvoke methods cannot exist on interfaces.");
			}
			this.check_not_created();
			MethodBuilder methodBuilder = new MethodBuilder(this, name, attributes, callingConvention, returnType, returnTypeRequiredCustomModifiers, returnTypeOptionalCustomModifiers, parameterTypes, parameterTypeRequiredCustomModifiers, parameterTypeOptionalCustomModifiers, dllName, entryName, nativeCallConv, nativeCharSet);
			this.append_method(methodBuilder);
			return methodBuilder;
		}

		// Token: 0x0600276C RID: 10092 RVA: 0x0008BF80 File Offset: 0x0008A180
		public MethodBuilder DefinePInvokeMethod(string name, string dllName, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, CallingConvention nativeCallConv, CharSet nativeCharSet)
		{
			return this.DefinePInvokeMethod(name, dllName, name, attributes, callingConvention, returnType, parameterTypes, nativeCallConv, nativeCharSet);
		}

		// Token: 0x0600276D RID: 10093 RVA: 0x0008BFA4 File Offset: 0x0008A1A4
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes)
		{
			return this.DefineMethod(name, attributes, CallingConventions.Standard);
		}

		// Token: 0x0600276E RID: 10094 RVA: 0x0008BFB0 File Offset: 0x0008A1B0
		public MethodBuilder DefineMethod(string name, MethodAttributes attributes, CallingConventions callingConvention)
		{
			return this.DefineMethod(name, attributes, callingConvention, null, null);
		}

		// Token: 0x0600276F RID: 10095 RVA: 0x0008BFC0 File Offset: 0x0008A1C0
		public void DefineMethodOverride(MethodInfo methodInfoBody, MethodInfo methodInfoDeclaration)
		{
			if (methodInfoBody == null)
			{
				throw new ArgumentNullException("methodInfoBody");
			}
			if (methodInfoDeclaration == null)
			{
				throw new ArgumentNullException("methodInfoDeclaration");
			}
			this.check_not_created();
			if (methodInfoBody.DeclaringType != this)
			{
				throw new ArgumentException("method body must belong to this type");
			}
			if (methodInfoBody is MethodBuilder)
			{
				MethodBuilder methodBuilder = (MethodBuilder)methodInfoBody;
				methodBuilder.set_override(methodInfoDeclaration);
			}
		}

		// Token: 0x06002770 RID: 10096 RVA: 0x0008C028 File Offset: 0x0008A228
		public FieldBuilder DefineField(string fieldName, Type type, FieldAttributes attributes)
		{
			return this.DefineField(fieldName, type, null, null, attributes);
		}

		// Token: 0x06002771 RID: 10097 RVA: 0x0008C038 File Offset: 0x0008A238
		public FieldBuilder DefineField(string fieldName, Type type, Type[] requiredCustomModifiers, Type[] optionalCustomModifiers, FieldAttributes attributes)
		{
			this.check_name("fieldName", fieldName);
			if (type == typeof(void))
			{
				throw new ArgumentException("Bad field type in defining field.");
			}
			this.check_not_created();
			FieldBuilder fieldBuilder = new FieldBuilder(this, fieldName, type, attributes, requiredCustomModifiers, optionalCustomModifiers);
			if (this.fields != null)
			{
				if (this.fields.Length == this.num_fields)
				{
					FieldBuilder[] destinationArray = new FieldBuilder[this.fields.Length * 2];
					Array.Copy(this.fields, destinationArray, this.num_fields);
					this.fields = destinationArray;
				}
				this.fields[this.num_fields] = fieldBuilder;
				this.num_fields++;
			}
			else
			{
				this.fields = new FieldBuilder[1];
				this.fields[0] = fieldBuilder;
				this.num_fields++;
				this.create_internal_class(this);
			}
			if (this.IsEnum && !this.IsCompilerContext && this.underlying_type == null && (attributes & FieldAttributes.Static) == FieldAttributes.PrivateScope)
			{
				this.underlying_type = type;
			}
			return fieldBuilder;
		}

		// Token: 0x06002772 RID: 10098 RVA: 0x0008C148 File Offset: 0x0008A348
		public PropertyBuilder DefineProperty(string name, PropertyAttributes attributes, Type returnType, Type[] parameterTypes)
		{
			return this.DefineProperty(name, attributes, returnType, null, null, parameterTypes, null, null);
		}

		// Token: 0x06002773 RID: 10099 RVA: 0x0008C164 File Offset: 0x0008A364
		public PropertyBuilder DefineProperty(string name, PropertyAttributes attributes, Type returnType, Type[] returnTypeRequiredCustomModifiers, Type[] returnTypeOptionalCustomModifiers, Type[] parameterTypes, Type[][] parameterTypeRequiredCustomModifiers, Type[][] parameterTypeOptionalCustomModifiers)
		{
			this.check_name("name", name);
			if (parameterTypes != null)
			{
				for (int i = 0; i < parameterTypes.Length; i++)
				{
					if (parameterTypes[i] == null)
					{
						throw new ArgumentNullException("parameterTypes");
					}
				}
			}
			this.check_not_created();
			PropertyBuilder propertyBuilder = new PropertyBuilder(this, name, attributes, returnType, returnTypeRequiredCustomModifiers, returnTypeOptionalCustomModifiers, parameterTypes, parameterTypeRequiredCustomModifiers, parameterTypeOptionalCustomModifiers);
			if (this.properties != null)
			{
				PropertyBuilder[] array = new PropertyBuilder[this.properties.Length + 1];
				Array.Copy(this.properties, array, this.properties.Length);
				array[this.properties.Length] = propertyBuilder;
				this.properties = array;
			}
			else
			{
				this.properties = new PropertyBuilder[1];
				this.properties[0] = propertyBuilder;
			}
			return propertyBuilder;
		}

		// Token: 0x06002774 RID: 10100 RVA: 0x0008C22C File Offset: 0x0008A42C
		[ComVisible(true)]
		public ConstructorBuilder DefineTypeInitializer()
		{
			return this.DefineConstructor(MethodAttributes.FamANDAssem | MethodAttributes.Family | MethodAttributes.Static | MethodAttributes.SpecialName | MethodAttributes.RTSpecialName, CallingConventions.Standard, null);
		}

		// Token: 0x06002775 RID: 10101
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type create_runtime_class(TypeBuilder tb);

		// Token: 0x06002776 RID: 10102 RVA: 0x0008C23C File Offset: 0x0008A43C
		private bool is_nested_in(Type t)
		{
			while (t != null)
			{
				if (t == this)
				{
					return true;
				}
				t = t.DeclaringType;
			}
			return false;
		}

		// Token: 0x06002777 RID: 10103 RVA: 0x0008C25C File Offset: 0x0008A45C
		private bool has_ctor_method()
		{
			MethodAttributes methodAttributes = MethodAttributes.SpecialName | MethodAttributes.RTSpecialName;
			for (int i = 0; i < this.num_methods; i++)
			{
				MethodBuilder methodBuilder = this.methods[i];
				if (methodBuilder.Name == ConstructorInfo.ConstructorName && (methodBuilder.Attributes & methodAttributes) == methodAttributes)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002778 RID: 10104 RVA: 0x0008C2B8 File Offset: 0x0008A4B8
		public Type CreateType()
		{
			if (this.createTypeCalled)
			{
				return this.created;
			}
			if (!this.IsInterface && this.parent == null && this != this.pmodule.assemblyb.corlib_object_type && this.FullName != "<Module>")
			{
				this.SetParent(this.pmodule.assemblyb.corlib_object_type);
			}
			this.create_generic_class();
			if (this.fields != null)
			{
				foreach (FieldBuilder fieldBuilder in this.fields)
				{
					if (fieldBuilder != null)
					{
						Type fieldType = fieldBuilder.FieldType;
						if (!fieldBuilder.IsStatic && fieldType is TypeBuilder && fieldType.IsValueType && fieldType != this && this.is_nested_in(fieldType))
						{
							TypeBuilder typeBuilder = (TypeBuilder)fieldType;
							if (!typeBuilder.is_created)
							{
								AppDomain.CurrentDomain.DoTypeResolve(typeBuilder);
								if (!typeBuilder.is_created)
								{
								}
							}
						}
					}
				}
			}
			if (this.parent != null && this.parent.IsSealed)
			{
				throw new TypeLoadException(string.Concat(new object[]
				{
					"Could not load type '",
					this.FullName,
					"' from assembly '",
					this.Assembly,
					"' because the parent type is sealed."
				}));
			}
			if (this.parent == this.pmodule.assemblyb.corlib_enum_type && this.methods != null)
			{
				throw new TypeLoadException(string.Concat(new object[]
				{
					"Could not load type '",
					this.FullName,
					"' from assembly '",
					this.Assembly,
					"' because it is an enum with methods."
				}));
			}
			if (this.methods != null)
			{
				bool flag = !this.IsAbstract;
				for (int j = 0; j < this.num_methods; j++)
				{
					MethodBuilder methodBuilder = this.methods[j];
					if (flag && methodBuilder.IsAbstract)
					{
						throw new InvalidOperationException("Type is concrete but has abstract method " + methodBuilder);
					}
					methodBuilder.check_override();
					methodBuilder.fixup();
				}
			}
			if (!this.IsInterface && !this.IsValueType && this.ctors == null && this.tname != "<Module>" && ((this.GetAttributeFlagsImpl() & TypeAttributes.Abstract) | TypeAttributes.Sealed) != (TypeAttributes.Abstract | TypeAttributes.Sealed) && !this.has_ctor_method())
			{
				this.DefineDefaultConstructor(MethodAttributes.Public);
			}
			if (this.ctors != null)
			{
				foreach (ConstructorBuilder constructorBuilder in this.ctors)
				{
					constructorBuilder.fixup();
				}
			}
			this.createTypeCalled = true;
			this.created = this.create_runtime_class(this);
			if (this.created != null)
			{
				return this.created;
			}
			return this;
		}

		// Token: 0x06002779 RID: 10105 RVA: 0x0008C5C0 File Offset: 0x0008A7C0
		internal void GenerateDebugInfo(ISymbolWriter symbolWriter)
		{
			symbolWriter.OpenNamespace(this.Namespace);
			if (this.methods != null)
			{
				for (int i = 0; i < this.num_methods; i++)
				{
					MethodBuilder methodBuilder = this.methods[i];
					methodBuilder.GenerateDebugInfo(symbolWriter);
				}
			}
			if (this.ctors != null)
			{
				foreach (ConstructorBuilder constructorBuilder in this.ctors)
				{
					constructorBuilder.GenerateDebugInfo(symbolWriter);
				}
			}
			symbolWriter.CloseNamespace();
			if (this.subtypes != null)
			{
				for (int k = 0; k < this.subtypes.Length; k++)
				{
					this.subtypes[k].GenerateDebugInfo(symbolWriter);
				}
			}
		}

		// Token: 0x0600277A RID: 10106 RVA: 0x0008C680 File Offset: 0x0008A880
		[ComVisible(true)]
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
		{
			if (this.is_created)
			{
				return this.created.GetConstructors(bindingAttr);
			}
			if (!this.IsCompilerContext)
			{
				throw new NotSupportedException();
			}
			return this.GetConstructorsInternal(bindingAttr);
		}

		// Token: 0x0600277B RID: 10107 RVA: 0x0008C6C0 File Offset: 0x0008A8C0
		internal ConstructorInfo[] GetConstructorsInternal(BindingFlags bindingAttr)
		{
			if (this.ctors == null)
			{
				return new ConstructorInfo[0];
			}
			ArrayList arrayList = new ArrayList();
			foreach (ConstructorBuilder constructorBuilder in this.ctors)
			{
				bool flag = false;
				MethodAttributes attributes = constructorBuilder.Attributes;
				if ((attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.Public)
				{
					if ((bindingAttr & BindingFlags.Public) != BindingFlags.Default)
					{
						flag = true;
					}
				}
				else if ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default)
				{
					flag = true;
				}
				if (flag)
				{
					flag = false;
					if ((attributes & MethodAttributes.Static) != MethodAttributes.PrivateScope)
					{
						if ((bindingAttr & BindingFlags.Static) != BindingFlags.Default)
						{
							flag = true;
						}
					}
					else if ((bindingAttr & BindingFlags.Instance) != BindingFlags.Default)
					{
						flag = true;
					}
					if (flag)
					{
						arrayList.Add(constructorBuilder);
					}
				}
			}
			ConstructorInfo[] array2 = new ConstructorInfo[arrayList.Count];
			arrayList.CopyTo(array2);
			return array2;
		}

		// Token: 0x0600277C RID: 10108 RVA: 0x0008C794 File Offset: 0x0008A994
		public override Type GetElementType()
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600277D RID: 10109 RVA: 0x0008C79C File Offset: 0x0008A99C
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
		{
			this.check_created();
			return this.created.GetEvent(name, bindingAttr);
		}

		// Token: 0x0600277E RID: 10110 RVA: 0x0008C7B4 File Offset: 0x0008A9B4
		public override EventInfo[] GetEvents()
		{
			return this.GetEvents(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x0600277F RID: 10111 RVA: 0x0008C7C0 File Offset: 0x0008A9C0
		public override EventInfo[] GetEvents(BindingFlags bindingAttr)
		{
			if (this.is_created)
			{
				return this.created.GetEvents(bindingAttr);
			}
			if (!this.IsCompilerContext)
			{
				throw new NotSupportedException();
			}
			return new EventInfo[0];
		}

		// Token: 0x06002780 RID: 10112 RVA: 0x0008C7FC File Offset: 0x0008A9FC
		internal EventInfo[] GetEvents_internal(BindingFlags bindingAttr)
		{
			if (this.events == null)
			{
				return new EventInfo[0];
			}
			ArrayList arrayList = new ArrayList();
			foreach (EventBuilder eventBuilder in this.events)
			{
				if (eventBuilder != null)
				{
					EventInfo eventInfo = this.get_event_info(eventBuilder);
					bool flag = false;
					MethodInfo methodInfo = eventInfo.GetAddMethod(true);
					if (methodInfo == null)
					{
						methodInfo = eventInfo.GetRemoveMethod(true);
					}
					if (methodInfo != null)
					{
						MethodAttributes attributes = methodInfo.Attributes;
						if ((attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.Public)
						{
							if ((bindingAttr & BindingFlags.Public) != BindingFlags.Default)
							{
								flag = true;
							}
						}
						else if ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default)
						{
							flag = true;
						}
						if (flag)
						{
							flag = false;
							if ((attributes & MethodAttributes.Static) != MethodAttributes.PrivateScope)
							{
								if ((bindingAttr & BindingFlags.Static) != BindingFlags.Default)
								{
									flag = true;
								}
							}
							else if ((bindingAttr & BindingFlags.Instance) != BindingFlags.Default)
							{
								flag = true;
							}
							if (flag)
							{
								arrayList.Add(eventInfo);
							}
						}
					}
				}
			}
			EventInfo[] array2 = new EventInfo[arrayList.Count];
			arrayList.CopyTo(array2);
			return array2;
		}

		// Token: 0x06002781 RID: 10113 RVA: 0x0008C90C File Offset: 0x0008AB0C
		public override FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			if (this.created != null)
			{
				return this.created.GetField(name, bindingAttr);
			}
			if (this.fields == null)
			{
				return null;
			}
			foreach (FieldBuilder fieldInfo in this.fields)
			{
				if (fieldInfo != null)
				{
					if (!(fieldInfo.Name != name))
					{
						bool flag = false;
						FieldAttributes attributes = fieldInfo.Attributes;
						if ((attributes & FieldAttributes.FieldAccessMask) == FieldAttributes.Public)
						{
							if ((bindingAttr & BindingFlags.Public) != BindingFlags.Default)
							{
								flag = true;
							}
						}
						else if ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default)
						{
							flag = true;
						}
						if (flag)
						{
							flag = false;
							if ((attributes & FieldAttributes.Static) != FieldAttributes.PrivateScope)
							{
								if ((bindingAttr & BindingFlags.Static) != BindingFlags.Default)
								{
									flag = true;
								}
							}
							else if ((bindingAttr & BindingFlags.Instance) != BindingFlags.Default)
							{
								flag = true;
							}
							if (flag)
							{
								return fieldInfo;
							}
						}
					}
				}
			}
			return null;
		}

		// Token: 0x06002782 RID: 10114 RVA: 0x0008C9F0 File Offset: 0x0008ABF0
		public override FieldInfo[] GetFields(BindingFlags bindingAttr)
		{
			if (this.created != null)
			{
				return this.created.GetFields(bindingAttr);
			}
			if (this.fields == null)
			{
				return new FieldInfo[0];
			}
			ArrayList arrayList = new ArrayList();
			foreach (FieldBuilder fieldInfo in this.fields)
			{
				if (fieldInfo != null)
				{
					bool flag = false;
					FieldAttributes attributes = fieldInfo.Attributes;
					if ((attributes & FieldAttributes.FieldAccessMask) == FieldAttributes.Public)
					{
						if ((bindingAttr & BindingFlags.Public) != BindingFlags.Default)
						{
							flag = true;
						}
					}
					else if ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default)
					{
						flag = true;
					}
					if (flag)
					{
						flag = false;
						if ((attributes & FieldAttributes.Static) != FieldAttributes.PrivateScope)
						{
							if ((bindingAttr & BindingFlags.Static) != BindingFlags.Default)
							{
								flag = true;
							}
						}
						else if ((bindingAttr & BindingFlags.Instance) != BindingFlags.Default)
						{
							flag = true;
						}
						if (flag)
						{
							arrayList.Add(fieldInfo);
						}
					}
				}
			}
			FieldInfo[] array2 = new FieldInfo[arrayList.Count];
			arrayList.CopyTo(array2);
			return array2;
		}

		// Token: 0x06002783 RID: 10115 RVA: 0x0008CAE8 File Offset: 0x0008ACE8
		public override Type GetInterface(string name, bool ignoreCase)
		{
			this.check_created();
			return this.created.GetInterface(name, ignoreCase);
		}

		// Token: 0x06002784 RID: 10116 RVA: 0x0008CB00 File Offset: 0x0008AD00
		public override Type[] GetInterfaces()
		{
			if (this.is_created)
			{
				return this.created.GetInterfaces();
			}
			if (this.interfaces != null)
			{
				Type[] array = new Type[this.interfaces.Length];
				this.interfaces.CopyTo(array, 0);
				return array;
			}
			return Type.EmptyTypes;
		}

		// Token: 0x06002785 RID: 10117 RVA: 0x0008CB54 File Offset: 0x0008AD54
		public override MemberInfo[] GetMember(string name, MemberTypes type, BindingFlags bindingAttr)
		{
			this.check_created();
			return this.created.GetMember(name, type, bindingAttr);
		}

		// Token: 0x06002786 RID: 10118 RVA: 0x0008CB6C File Offset: 0x0008AD6C
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
		{
			this.check_created();
			return this.created.GetMembers(bindingAttr);
		}

		// Token: 0x06002787 RID: 10119 RVA: 0x0008CB80 File Offset: 0x0008AD80
		private MethodInfo[] GetMethodsByName(string name, BindingFlags bindingAttr, bool ignoreCase, Type reflected_type)
		{
			MethodInfo[] array2;
			if ((bindingAttr & BindingFlags.DeclaredOnly) == BindingFlags.Default && this.parent != null)
			{
				MethodInfo[] array = this.parent.GetMethods(bindingAttr);
				ArrayList arrayList = new ArrayList(array.Length);
				bool flag = (bindingAttr & BindingFlags.FlattenHierarchy) != BindingFlags.Default;
				foreach (MethodInfo methodInfo in array)
				{
					MethodAttributes attributes = methodInfo.Attributes;
					if (!methodInfo.IsStatic || flag)
					{
						bool flag2;
						switch (attributes & MethodAttributes.MemberAccessMask)
						{
						case MethodAttributes.Private:
							flag2 = false;
							break;
						case MethodAttributes.FamANDAssem:
						case MethodAttributes.Family:
						case MethodAttributes.FamORAssem:
							goto IL_B6;
						case MethodAttributes.Assembly:
							flag2 = ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default);
							break;
						case MethodAttributes.Public:
							flag2 = ((bindingAttr & BindingFlags.Public) != BindingFlags.Default);
							break;
						default:
							goto IL_B6;
						}
						IL_C6:
						if (flag2)
						{
							arrayList.Add(methodInfo);
							goto IL_D6;
						}
						goto IL_D6;
						IL_B6:
						flag2 = ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default);
						goto IL_C6;
					}
					IL_D6:;
				}
				if (this.methods == null)
				{
					array2 = new MethodInfo[arrayList.Count];
					arrayList.CopyTo(array2);
				}
				else
				{
					array2 = new MethodInfo[this.methods.Length + arrayList.Count];
					arrayList.CopyTo(array2, 0);
					this.methods.CopyTo(array2, arrayList.Count);
				}
			}
			else
			{
				array2 = this.methods;
			}
			if (array2 == null)
			{
				return new MethodInfo[0];
			}
			ArrayList arrayList2 = new ArrayList();
			foreach (MethodInfo methodInfo2 in array2)
			{
				if (methodInfo2 != null)
				{
					if (name == null || string.Compare(methodInfo2.Name, name, ignoreCase) == 0)
					{
						bool flag2 = false;
						MethodAttributes attributes = methodInfo2.Attributes;
						if ((attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.Public)
						{
							if ((bindingAttr & BindingFlags.Public) != BindingFlags.Default)
							{
								flag2 = true;
							}
						}
						else if ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default)
						{
							flag2 = true;
						}
						if (flag2)
						{
							flag2 = false;
							if ((attributes & MethodAttributes.Static) != MethodAttributes.PrivateScope)
							{
								if ((bindingAttr & BindingFlags.Static) != BindingFlags.Default)
								{
									flag2 = true;
								}
							}
							else if ((bindingAttr & BindingFlags.Instance) != BindingFlags.Default)
							{
								flag2 = true;
							}
							if (flag2)
							{
								arrayList2.Add(methodInfo2);
							}
						}
					}
				}
			}
			MethodInfo[] array4 = new MethodInfo[arrayList2.Count];
			arrayList2.CopyTo(array4);
			return array4;
		}

		// Token: 0x06002788 RID: 10120 RVA: 0x0008CDC4 File Offset: 0x0008AFC4
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
		{
			return this.GetMethodsByName(null, bindingAttr, false, this);
		}

		// Token: 0x06002789 RID: 10121 RVA: 0x0008CDD0 File Offset: 0x0008AFD0
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			this.check_created();
			bool ignoreCase = (bindingAttr & BindingFlags.IgnoreCase) != BindingFlags.Default;
			MethodInfo[] methodsByName = this.GetMethodsByName(name, bindingAttr, ignoreCase, this);
			MethodInfo methodInfo = null;
			int num = (types == null) ? 0 : types.Length;
			int num2 = 0;
			foreach (MethodInfo methodInfo2 in methodsByName)
			{
				if (callConvention == CallingConventions.Any || (methodInfo2.CallingConvention & callConvention) == callConvention)
				{
					methodInfo = methodInfo2;
					num2++;
				}
			}
			if (num2 == 0)
			{
				return null;
			}
			if (num2 == 1 && num == 0)
			{
				return methodInfo;
			}
			MethodBase[] array2 = new MethodBase[num2];
			if (num2 == 1)
			{
				array2[0] = methodInfo;
			}
			else
			{
				num2 = 0;
				foreach (MethodInfo methodInfo3 in methodsByName)
				{
					if (callConvention == CallingConventions.Any || (methodInfo3.CallingConvention & callConvention) == callConvention)
					{
						array2[num2++] = methodInfo3;
					}
				}
			}
			if (types == null)
			{
				return (MethodInfo)Binder.FindMostDerivedMatch(array2);
			}
			if (binder == null)
			{
				binder = Binder.DefaultBinder;
			}
			return (MethodInfo)binder.SelectMethod(bindingAttr, array2, types, modifiers);
		}

		// Token: 0x0600278A RID: 10122 RVA: 0x0008CF10 File Offset: 0x0008B110
		public override Type GetNestedType(string name, BindingFlags bindingAttr)
		{
			this.check_created();
			if (this.subtypes == null)
			{
				return null;
			}
			foreach (TypeBuilder typeBuilder in this.subtypes)
			{
				if (typeBuilder.is_created)
				{
					if ((typeBuilder.attrs & TypeAttributes.VisibilityMask) == TypeAttributes.NestedPublic)
					{
						if ((bindingAttr & BindingFlags.Public) == BindingFlags.Default)
						{
							goto IL_7C;
						}
					}
					else if ((bindingAttr & BindingFlags.NonPublic) == BindingFlags.Default)
					{
						goto IL_7C;
					}
					if (typeBuilder.Name == name)
					{
						return typeBuilder.created;
					}
				}
				IL_7C:;
			}
			return null;
		}

		// Token: 0x0600278B RID: 10123 RVA: 0x0008CFA8 File Offset: 0x0008B1A8
		public override Type[] GetNestedTypes(BindingFlags bindingAttr)
		{
			if (!this.is_created && !this.IsCompilerContext)
			{
				throw new NotSupportedException();
			}
			ArrayList arrayList = new ArrayList();
			if (this.subtypes == null)
			{
				return Type.EmptyTypes;
			}
			foreach (TypeBuilder typeBuilder in this.subtypes)
			{
				bool flag = false;
				if ((typeBuilder.attrs & TypeAttributes.VisibilityMask) == TypeAttributes.NestedPublic)
				{
					if ((bindingAttr & BindingFlags.Public) != BindingFlags.Default)
					{
						flag = true;
					}
				}
				else if ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default)
				{
					flag = true;
				}
				if (flag)
				{
					arrayList.Add(typeBuilder);
				}
			}
			Type[] array2 = new Type[arrayList.Count];
			arrayList.CopyTo(array2);
			return array2;
		}

		// Token: 0x0600278C RID: 10124 RVA: 0x0008D064 File Offset: 0x0008B264
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
		{
			if (this.is_created)
			{
				return this.created.GetProperties(bindingAttr);
			}
			if (this.properties == null)
			{
				return new PropertyInfo[0];
			}
			ArrayList arrayList = new ArrayList();
			foreach (PropertyBuilder propertyInfo in this.properties)
			{
				bool flag = false;
				MethodInfo methodInfo = propertyInfo.GetGetMethod(true);
				if (methodInfo == null)
				{
					methodInfo = propertyInfo.GetSetMethod(true);
				}
				if (methodInfo != null)
				{
					MethodAttributes attributes = methodInfo.Attributes;
					if ((attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.Public)
					{
						if ((bindingAttr & BindingFlags.Public) != BindingFlags.Default)
						{
							flag = true;
						}
					}
					else if ((bindingAttr & BindingFlags.NonPublic) != BindingFlags.Default)
					{
						flag = true;
					}
					if (flag)
					{
						flag = false;
						if ((attributes & MethodAttributes.Static) != MethodAttributes.PrivateScope)
						{
							if ((bindingAttr & BindingFlags.Static) != BindingFlags.Default)
							{
								flag = true;
							}
						}
						else if ((bindingAttr & BindingFlags.Instance) != BindingFlags.Default)
						{
							flag = true;
						}
						if (flag)
						{
							arrayList.Add(propertyInfo);
						}
					}
				}
			}
			PropertyInfo[] array2 = new PropertyInfo[arrayList.Count];
			arrayList.CopyTo(array2);
			return array2;
		}

		// Token: 0x0600278D RID: 10125 RVA: 0x0008D174 File Offset: 0x0008B374
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			throw this.not_supported();
		}

		// Token: 0x0600278E RID: 10126 RVA: 0x0008D17C File Offset: 0x0008B37C
		protected override bool HasElementTypeImpl()
		{
			return this.is_created && this.created.HasElementType;
		}

		// Token: 0x0600278F RID: 10127 RVA: 0x0008D198 File Offset: 0x0008B398
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
		{
			this.check_created();
			return this.created.InvokeMember(name, invokeAttr, binder, target, args, modifiers, culture, namedParameters);
		}

		// Token: 0x06002790 RID: 10128 RVA: 0x0008D1C4 File Offset: 0x0008B3C4
		protected override bool IsArrayImpl()
		{
			return false;
		}

		// Token: 0x06002791 RID: 10129 RVA: 0x0008D1C8 File Offset: 0x0008B3C8
		protected override bool IsByRefImpl()
		{
			return false;
		}

		// Token: 0x06002792 RID: 10130 RVA: 0x0008D1CC File Offset: 0x0008B3CC
		protected override bool IsCOMObjectImpl()
		{
			return (this.GetAttributeFlagsImpl() & TypeAttributes.Import) != TypeAttributes.NotPublic;
		}

		// Token: 0x06002793 RID: 10131 RVA: 0x0008D1E0 File Offset: 0x0008B3E0
		protected override bool IsPointerImpl()
		{
			return false;
		}

		// Token: 0x06002794 RID: 10132 RVA: 0x0008D1E4 File Offset: 0x0008B3E4
		protected override bool IsPrimitiveImpl()
		{
			return false;
		}

		// Token: 0x06002795 RID: 10133 RVA: 0x0008D1E8 File Offset: 0x0008B3E8
		protected override bool IsValueTypeImpl()
		{
			return (Type.type_is_subtype_of(this, this.pmodule.assemblyb.corlib_value_type, false) || Type.type_is_subtype_of(this, typeof(ValueType), false)) && this != this.pmodule.assemblyb.corlib_value_type && this != this.pmodule.assemblyb.corlib_enum_type;
		}

		// Token: 0x06002796 RID: 10134 RVA: 0x0008D258 File Offset: 0x0008B458
		public override Type MakeArrayType()
		{
			return new ArrayType(this, 0);
		}

		// Token: 0x06002797 RID: 10135 RVA: 0x0008D264 File Offset: 0x0008B464
		public override Type MakeArrayType(int rank)
		{
			if (rank < 1)
			{
				throw new IndexOutOfRangeException();
			}
			return new ArrayType(this, rank);
		}

		// Token: 0x06002798 RID: 10136 RVA: 0x0008D27C File Offset: 0x0008B47C
		public override Type MakeByRefType()
		{
			return new ByRefType(this);
		}

		// Token: 0x06002799 RID: 10137 RVA: 0x0008D284 File Offset: 0x0008B484
		[MonoTODO]
		public override Type MakeGenericType(params Type[] typeArguments)
		{
			return base.MakeGenericType(typeArguments);
		}

		// Token: 0x0600279A RID: 10138 RVA: 0x0008D290 File Offset: 0x0008B490
		public override Type MakePointerType()
		{
			return new PointerType(this);
		}

		// Token: 0x1700070D RID: 1805
		// (get) Token: 0x0600279B RID: 10139 RVA: 0x0008D298 File Offset: 0x0008B498
		public override RuntimeTypeHandle TypeHandle
		{
			get
			{
				this.check_created();
				return this.created.TypeHandle;
			}
		}

		// Token: 0x0600279C RID: 10140 RVA: 0x0008D2AC File Offset: 0x0008B4AC
		internal void SetCharSet(TypeAttributes ta)
		{
			this.attrs = ta;
		}

		// Token: 0x0600279D RID: 10141 RVA: 0x0008D2B8 File Offset: 0x0008B4B8
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			if (customBuilder == null)
			{
				throw new ArgumentNullException("customBuilder");
			}
			string fullName = customBuilder.Ctor.ReflectedType.FullName;
			if (fullName == "System.Runtime.InteropServices.StructLayoutAttribute")
			{
				byte[] data = customBuilder.Data;
				int num = (int)data[2];
				num |= (int)data[3] << 8;
				this.attrs &= ~TypeAttributes.LayoutMask;
				switch (num)
				{
				case 0:
					this.attrs |= TypeAttributes.SequentialLayout;
					goto IL_B8;
				case 2:
					this.attrs |= TypeAttributes.ExplicitLayout;
					goto IL_B8;
				case 3:
					this.attrs |= TypeAttributes.NotPublic;
					goto IL_B8;
				}
				throw new Exception("Error in customattr");
				IL_B8:
				string fullName2 = customBuilder.Ctor.GetParameters()[0].ParameterType.FullName;
				int num2 = 6;
				if (fullName2 == "System.Int16")
				{
					num2 = 4;
				}
				int num3 = (int)data[num2++];
				num3 |= (int)data[num2++] << 8;
				int i = 0;
				while (i < num3)
				{
					num2++;
					byte b = data[num2++];
					int num4;
					if (b == 85)
					{
						num4 = CustomAttributeBuilder.decode_len(data, num2, out num2);
						CustomAttributeBuilder.string_from_bytes(data, num2, num4);
						num2 += num4;
					}
					num4 = CustomAttributeBuilder.decode_len(data, num2, out num2);
					string text = CustomAttributeBuilder.string_from_bytes(data, num2, num4);
					num2 += num4;
					int num5 = (int)data[num2++];
					num5 |= (int)data[num2++] << 8;
					num5 |= (int)data[num2++] << 16;
					num5 |= (int)data[num2++] << 24;
					string text2 = text;
					switch (text2)
					{
					case "CharSet":
						switch (num5)
						{
						case 1:
						case 2:
							this.attrs &= ~TypeAttributes.StringFormatMask;
							break;
						case 3:
							this.attrs &= ~TypeAttributes.AutoClass;
							this.attrs |= TypeAttributes.UnicodeClass;
							break;
						case 4:
							this.attrs &= ~TypeAttributes.UnicodeClass;
							this.attrs |= TypeAttributes.AutoClass;
							break;
						}
						break;
					case "Pack":
						this.packing_size = (PackingSize)num5;
						break;
					case "Size":
						this.class_size = num5;
						break;
					}
					IL_2C7:
					i++;
					continue;
					goto IL_2C7;
				}
				return;
			}
			if (fullName == "System.Runtime.CompilerServices.SpecialNameAttribute")
			{
				this.attrs |= TypeAttributes.SpecialName;
				return;
			}
			if (fullName == "System.SerializableAttribute")
			{
				this.attrs |= TypeAttributes.Serializable;
				return;
			}
			if (fullName == "System.Runtime.InteropServices.ComImportAttribute")
			{
				this.attrs |= TypeAttributes.Import;
				return;
			}
			if (fullName == "System.Security.SuppressUnmanagedCodeSecurityAttribute")
			{
				this.attrs |= TypeAttributes.HasSecurity;
			}
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x0600279E RID: 10142 RVA: 0x0008D680 File Offset: 0x0008B880
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x0600279F RID: 10143 RVA: 0x0008D690 File Offset: 0x0008B890
		public EventBuilder DefineEvent(string name, EventAttributes attributes, Type eventtype)
		{
			this.check_name("name", name);
			if (eventtype == null)
			{
				throw new ArgumentNullException("type");
			}
			this.check_not_created();
			EventBuilder eventBuilder = new EventBuilder(this, name, attributes, eventtype);
			if (this.events != null)
			{
				EventBuilder[] array = new EventBuilder[this.events.Length + 1];
				Array.Copy(this.events, array, this.events.Length);
				array[this.events.Length] = eventBuilder;
				this.events = array;
			}
			else
			{
				this.events = new EventBuilder[1];
				this.events[0] = eventBuilder;
			}
			return eventBuilder;
		}

		// Token: 0x060027A0 RID: 10144 RVA: 0x0008D728 File Offset: 0x0008B928
		public FieldBuilder DefineInitializedData(string name, byte[] data, FieldAttributes attributes)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			FieldBuilder fieldBuilder = this.DefineUninitializedData(name, data.Length, attributes);
			fieldBuilder.SetRVAData(data);
			return fieldBuilder;
		}

		// Token: 0x060027A1 RID: 10145 RVA: 0x0008D75C File Offset: 0x0008B95C
		public FieldBuilder DefineUninitializedData(string name, int size, FieldAttributes attributes)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Empty name is not legal", "name");
			}
			if (size <= 0 || size > 4128768)
			{
				throw new ArgumentException("Data size must be > 0 and < 0x3f0000");
			}
			this.check_not_created();
			string text = "$ArrayType$" + size;
			Type type = this.pmodule.GetRegisteredType(this.fullname + "+" + text);
			if (type == null)
			{
				TypeBuilder typeBuilder = this.DefineNestedType(text, TypeAttributes.Public | TypeAttributes.NestedPublic | TypeAttributes.ExplicitLayout | TypeAttributes.Sealed, this.pmodule.assemblyb.corlib_value_type, null, PackingSize.Size1, size);
				typeBuilder.CreateType();
				type = typeBuilder;
			}
			return this.DefineField(name, type, attributes | FieldAttributes.Static | FieldAttributes.HasFieldRVA);
		}

		// Token: 0x1700070E RID: 1806
		// (get) Token: 0x060027A2 RID: 10146 RVA: 0x0008D828 File Offset: 0x0008BA28
		public TypeToken TypeToken
		{
			get
			{
				return new TypeToken(33554432 | this.table_idx);
			}
		}

		// Token: 0x060027A3 RID: 10147 RVA: 0x0008D83C File Offset: 0x0008BA3C
		public void SetParent(Type parent)
		{
			this.check_not_created();
			if (parent == null)
			{
				if ((this.attrs & TypeAttributes.ClassSemanticsMask) != TypeAttributes.NotPublic)
				{
					if ((this.attrs & TypeAttributes.Abstract) == TypeAttributes.NotPublic)
					{
						throw new InvalidOperationException("Interface must be declared abstract.");
					}
					this.parent = null;
				}
				else
				{
					this.parent = typeof(object);
				}
			}
			else
			{
				this.parent = parent;
			}
			this.setup_internal_class(this);
		}

		// Token: 0x060027A4 RID: 10148 RVA: 0x0008D8B0 File Offset: 0x0008BAB0
		internal int get_next_table_index(object obj, int table, bool inc)
		{
			return this.pmodule.get_next_table_index(obj, table, inc);
		}

		// Token: 0x060027A5 RID: 10149 RVA: 0x0008D8C0 File Offset: 0x0008BAC0
		[ComVisible(true)]
		public override InterfaceMapping GetInterfaceMap(Type interfaceType)
		{
			if (this.created == null)
			{
				throw new NotSupportedException("This method is not implemented for incomplete types.");
			}
			return this.created.GetInterfaceMap(interfaceType);
		}

		// Token: 0x1700070F RID: 1807
		// (get) Token: 0x060027A6 RID: 10150 RVA: 0x0008D8F0 File Offset: 0x0008BAF0
		internal bool IsCompilerContext
		{
			get
			{
				return this.pmodule.assemblyb.IsCompilerContext;
			}
		}

		// Token: 0x17000710 RID: 1808
		// (get) Token: 0x060027A7 RID: 10151 RVA: 0x0008D904 File Offset: 0x0008BB04
		internal bool is_created
		{
			get
			{
				return this.created != null;
			}
		}

		// Token: 0x060027A8 RID: 10152 RVA: 0x0008D914 File Offset: 0x0008BB14
		private Exception not_supported()
		{
			return new NotSupportedException("The invoked member is not supported in a dynamic module.");
		}

		// Token: 0x060027A9 RID: 10153 RVA: 0x0008D920 File Offset: 0x0008BB20
		private void check_not_created()
		{
			if (this.is_created)
			{
				throw new InvalidOperationException("Unable to change after type has been created.");
			}
		}

		// Token: 0x060027AA RID: 10154 RVA: 0x0008D938 File Offset: 0x0008BB38
		private void check_created()
		{
			if (!this.is_created)
			{
				throw this.not_supported();
			}
		}

		// Token: 0x060027AB RID: 10155 RVA: 0x0008D94C File Offset: 0x0008BB4C
		private void check_name(string argName, string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException(argName);
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Empty name is not legal", argName);
			}
			if (name[0] == '\0')
			{
				throw new ArgumentException("Illegal name", argName);
			}
		}

		// Token: 0x060027AC RID: 10156 RVA: 0x0008D998 File Offset: 0x0008BB98
		public override string ToString()
		{
			return this.FullName;
		}

		// Token: 0x060027AD RID: 10157 RVA: 0x0008D9A0 File Offset: 0x0008BBA0
		[MonoTODO]
		public override bool IsAssignableFrom(Type c)
		{
			return base.IsAssignableFrom(c);
		}

		// Token: 0x060027AE RID: 10158 RVA: 0x0008D9AC File Offset: 0x0008BBAC
		[ComVisible(true)]
		[MonoTODO]
		public override bool IsSubclassOf(Type c)
		{
			return base.IsSubclassOf(c);
		}

		// Token: 0x060027AF RID: 10159 RVA: 0x0008D9B8 File Offset: 0x0008BBB8
		[MonoTODO("arrays")]
		internal bool IsAssignableTo(Type c)
		{
			if (c == this)
			{
				return true;
			}
			if (c.IsInterface)
			{
				if (this.parent != null && this.is_created && c.IsAssignableFrom(this.parent))
				{
					return true;
				}
				if (this.interfaces == null)
				{
					return false;
				}
				foreach (Type c2 in this.interfaces)
				{
					if (c.IsAssignableFrom(c2))
					{
						return true;
					}
				}
				if (!this.is_created)
				{
					return false;
				}
			}
			if (this.parent == null)
			{
				return c == typeof(object);
			}
			return c.IsAssignableFrom(this.parent);
		}

		// Token: 0x060027B0 RID: 10160 RVA: 0x0008DA70 File Offset: 0x0008BC70
		public bool IsCreated()
		{
			return this.is_created;
		}

		// Token: 0x060027B1 RID: 10161 RVA: 0x0008DA78 File Offset: 0x0008BC78
		public override Type[] GetGenericArguments()
		{
			if (this.generic_params == null)
			{
				return null;
			}
			Type[] array = new Type[this.generic_params.Length];
			this.generic_params.CopyTo(array, 0);
			return array;
		}

		// Token: 0x060027B2 RID: 10162 RVA: 0x0008DAB0 File Offset: 0x0008BCB0
		public override Type GetGenericTypeDefinition()
		{
			if (this.generic_params == null)
			{
				throw new InvalidOperationException("Type is not generic");
			}
			return this;
		}

		// Token: 0x17000711 RID: 1809
		// (get) Token: 0x060027B3 RID: 10163 RVA: 0x0008DACC File Offset: 0x0008BCCC
		public override bool ContainsGenericParameters
		{
			get
			{
				return this.generic_params != null;
			}
		}

		// Token: 0x17000712 RID: 1810
		// (get) Token: 0x060027B4 RID: 10164
		public override extern bool IsGenericParameter { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000713 RID: 1811
		// (get) Token: 0x060027B5 RID: 10165 RVA: 0x0008DADC File Offset: 0x0008BCDC
		public override GenericParameterAttributes GenericParameterAttributes
		{
			get
			{
				return GenericParameterAttributes.None;
			}
		}

		// Token: 0x17000714 RID: 1812
		// (get) Token: 0x060027B6 RID: 10166 RVA: 0x0008DAE0 File Offset: 0x0008BCE0
		public override bool IsGenericTypeDefinition
		{
			get
			{
				return this.generic_params != null;
			}
		}

		// Token: 0x17000715 RID: 1813
		// (get) Token: 0x060027B7 RID: 10167 RVA: 0x0008DAF0 File Offset: 0x0008BCF0
		public override bool IsGenericType
		{
			get
			{
				return this.IsGenericTypeDefinition;
			}
		}

		// Token: 0x17000716 RID: 1814
		// (get) Token: 0x060027B8 RID: 10168 RVA: 0x0008DAF8 File Offset: 0x0008BCF8
		[MonoTODO]
		public override int GenericParameterPosition
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x17000717 RID: 1815
		// (get) Token: 0x060027B9 RID: 10169 RVA: 0x0008DAFC File Offset: 0x0008BCFC
		public override MethodBase DeclaringMethod
		{
			get
			{
				return null;
			}
		}

		// Token: 0x060027BA RID: 10170 RVA: 0x0008DB00 File Offset: 0x0008BD00
		public GenericTypeParameterBuilder[] DefineGenericParameters(params string[] names)
		{
			if (names == null)
			{
				throw new ArgumentNullException("names");
			}
			if (names.Length == 0)
			{
				throw new ArgumentException("names");
			}
			this.setup_generic_class();
			this.generic_params = new GenericTypeParameterBuilder[names.Length];
			for (int i = 0; i < names.Length; i++)
			{
				string text = names[i];
				if (text == null)
				{
					throw new ArgumentNullException("names");
				}
				this.generic_params[i] = new GenericTypeParameterBuilder(this, null, text, i);
			}
			return this.generic_params;
		}

		// Token: 0x060027BB RID: 10171 RVA: 0x0008DB88 File Offset: 0x0008BD88
		public static ConstructorInfo GetConstructor(Type type, ConstructorInfo constructor)
		{
			if (type == null)
			{
				throw new ArgumentException("Type is not generic", "type");
			}
			ConstructorInfo constructor2 = type.GetConstructor(constructor);
			if (constructor2 == null)
			{
				throw new ArgumentException("constructor not found");
			}
			return constructor2;
		}

		// Token: 0x060027BC RID: 10172 RVA: 0x0008DBC8 File Offset: 0x0008BDC8
		private static bool IsValidGetMethodType(Type type)
		{
			if (type is TypeBuilder || type is MonoGenericClass)
			{
				return true;
			}
			if (type.Module is ModuleBuilder)
			{
				return true;
			}
			if (type.IsGenericParameter)
			{
				return false;
			}
			Type[] genericArguments = type.GetGenericArguments();
			if (genericArguments == null)
			{
				return false;
			}
			for (int i = 0; i < genericArguments.Length; i++)
			{
				if (TypeBuilder.IsValidGetMethodType(genericArguments[i]))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060027BD RID: 10173 RVA: 0x0008DC40 File Offset: 0x0008BE40
		public static MethodInfo GetMethod(Type type, MethodInfo method)
		{
			if (!TypeBuilder.IsValidGetMethodType(type))
			{
				throw new ArgumentException("type is not TypeBuilder but " + type.GetType(), "type");
			}
			if (!type.IsGenericType)
			{
				throw new ArgumentException("type is not a generic type", "type");
			}
			if (!method.DeclaringType.IsGenericTypeDefinition)
			{
				throw new ArgumentException("method declaring type is not a generic type definition", "method");
			}
			if (method.DeclaringType != type.GetGenericTypeDefinition())
			{
				throw new ArgumentException("method declaring type is not the generic type definition of type", "method");
			}
			MethodInfo method2 = type.GetMethod(method);
			if (method2 == null)
			{
				throw new ArgumentException(string.Format("method {0} not found in type {1}", method.Name, type));
			}
			return method2;
		}

		// Token: 0x060027BE RID: 10174 RVA: 0x0008DCF8 File Offset: 0x0008BEF8
		public static FieldInfo GetField(Type type, FieldInfo field)
		{
			FieldInfo field2 = type.GetField(field);
			if (field2 == null)
			{
				throw new Exception("field not found");
			}
			return field2;
		}

		// Token: 0x04000FDE RID: 4062
		public const int UnspecifiedTypeSize = 0;

		// Token: 0x04000FDF RID: 4063
		private string tname;

		// Token: 0x04000FE0 RID: 4064
		private string nspace;

		// Token: 0x04000FE1 RID: 4065
		private Type parent;

		// Token: 0x04000FE2 RID: 4066
		private Type nesting_type;

		// Token: 0x04000FE3 RID: 4067
		internal Type[] interfaces;

		// Token: 0x04000FE4 RID: 4068
		internal int num_methods;

		// Token: 0x04000FE5 RID: 4069
		internal MethodBuilder[] methods;

		// Token: 0x04000FE6 RID: 4070
		internal ConstructorBuilder[] ctors;

		// Token: 0x04000FE7 RID: 4071
		internal PropertyBuilder[] properties;

		// Token: 0x04000FE8 RID: 4072
		internal int num_fields;

		// Token: 0x04000FE9 RID: 4073
		internal FieldBuilder[] fields;

		// Token: 0x04000FEA RID: 4074
		internal EventBuilder[] events;

		// Token: 0x04000FEB RID: 4075
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000FEC RID: 4076
		internal TypeBuilder[] subtypes;

		// Token: 0x04000FED RID: 4077
		internal TypeAttributes attrs;

		// Token: 0x04000FEE RID: 4078
		private int table_idx;

		// Token: 0x04000FEF RID: 4079
		private ModuleBuilder pmodule;

		// Token: 0x04000FF0 RID: 4080
		private int class_size;

		// Token: 0x04000FF1 RID: 4081
		private PackingSize packing_size;

		// Token: 0x04000FF2 RID: 4082
		private IntPtr generic_container;

		// Token: 0x04000FF3 RID: 4083
		private GenericTypeParameterBuilder[] generic_params;

		// Token: 0x04000FF4 RID: 4084
		private RefEmitPermissionSet[] permissions;

		// Token: 0x04000FF5 RID: 4085
		private Type created;

		// Token: 0x04000FF6 RID: 4086
		private string fullname;

		// Token: 0x04000FF7 RID: 4087
		private bool createTypeCalled;

		// Token: 0x04000FF8 RID: 4088
		private Type underlying_type;
	}
}
