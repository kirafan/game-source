﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002CB RID: 715
	internal abstract class DerivedType : Type
	{
		// Token: 0x06002417 RID: 9239 RVA: 0x00081DC0 File Offset: 0x0007FFC0
		internal DerivedType(Type elementType)
		{
			this.elementType = elementType;
		}

		// Token: 0x06002418 RID: 9240
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void create_unmanaged_type(Type type);

		// Token: 0x06002419 RID: 9241
		internal abstract string FormatName(string elementName);

		// Token: 0x0600241A RID: 9242 RVA: 0x00081DD0 File Offset: 0x0007FFD0
		public override Type GetInterface(string name, bool ignoreCase)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600241B RID: 9243 RVA: 0x00081DD8 File Offset: 0x0007FFD8
		public override Type[] GetInterfaces()
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600241C RID: 9244 RVA: 0x00081DE0 File Offset: 0x0007FFE0
		public override Type GetElementType()
		{
			return this.elementType;
		}

		// Token: 0x0600241D RID: 9245 RVA: 0x00081DE8 File Offset: 0x0007FFE8
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600241E RID: 9246 RVA: 0x00081DF0 File Offset: 0x0007FFF0
		public override EventInfo[] GetEvents(BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x0600241F RID: 9247 RVA: 0x00081DF8 File Offset: 0x0007FFF8
		public override FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002420 RID: 9248 RVA: 0x00081E00 File Offset: 0x00080000
		public override FieldInfo[] GetFields(BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002421 RID: 9249 RVA: 0x00081E08 File Offset: 0x00080008
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002422 RID: 9250 RVA: 0x00081E10 File Offset: 0x00080010
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002423 RID: 9251 RVA: 0x00081E18 File Offset: 0x00080018
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002424 RID: 9252 RVA: 0x00081E20 File Offset: 0x00080020
		public override Type GetNestedType(string name, BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002425 RID: 9253 RVA: 0x00081E28 File Offset: 0x00080028
		public override Type[] GetNestedTypes(BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002426 RID: 9254 RVA: 0x00081E30 File Offset: 0x00080030
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002427 RID: 9255 RVA: 0x00081E38 File Offset: 0x00080038
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002428 RID: 9256 RVA: 0x00081E40 File Offset: 0x00080040
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002429 RID: 9257 RVA: 0x00081E48 File Offset: 0x00080048
		protected override TypeAttributes GetAttributeFlagsImpl()
		{
			return this.elementType.Attributes;
		}

		// Token: 0x0600242A RID: 9258 RVA: 0x00081E58 File Offset: 0x00080058
		protected override bool HasElementTypeImpl()
		{
			return true;
		}

		// Token: 0x0600242B RID: 9259 RVA: 0x00081E5C File Offset: 0x0008005C
		protected override bool IsArrayImpl()
		{
			return false;
		}

		// Token: 0x0600242C RID: 9260 RVA: 0x00081E60 File Offset: 0x00080060
		protected override bool IsByRefImpl()
		{
			return false;
		}

		// Token: 0x0600242D RID: 9261 RVA: 0x00081E64 File Offset: 0x00080064
		protected override bool IsCOMObjectImpl()
		{
			return false;
		}

		// Token: 0x0600242E RID: 9262 RVA: 0x00081E68 File Offset: 0x00080068
		protected override bool IsPointerImpl()
		{
			return false;
		}

		// Token: 0x0600242F RID: 9263 RVA: 0x00081E6C File Offset: 0x0008006C
		protected override bool IsPrimitiveImpl()
		{
			return false;
		}

		// Token: 0x06002430 RID: 9264 RVA: 0x00081E70 File Offset: 0x00080070
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002431 RID: 9265 RVA: 0x00081E78 File Offset: 0x00080078
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002432 RID: 9266 RVA: 0x00081E80 File Offset: 0x00080080
		public override InterfaceMapping GetInterfaceMap(Type interfaceType)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002433 RID: 9267 RVA: 0x00081E88 File Offset: 0x00080088
		public override bool IsInstanceOfType(object o)
		{
			return false;
		}

		// Token: 0x06002434 RID: 9268 RVA: 0x00081E8C File Offset: 0x0008008C
		public override bool IsAssignableFrom(Type c)
		{
			return false;
		}

		// Token: 0x17000657 RID: 1623
		// (get) Token: 0x06002435 RID: 9269 RVA: 0x00081E90 File Offset: 0x00080090
		public override bool ContainsGenericParameters
		{
			get
			{
				return this.elementType.ContainsGenericParameters;
			}
		}

		// Token: 0x06002436 RID: 9270 RVA: 0x00081EA0 File Offset: 0x000800A0
		public override Type MakeGenericType(params Type[] typeArguments)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002437 RID: 9271 RVA: 0x00081EA8 File Offset: 0x000800A8
		public override Type MakeArrayType()
		{
			return new ArrayType(this, 0);
		}

		// Token: 0x06002438 RID: 9272 RVA: 0x00081EB4 File Offset: 0x000800B4
		public override Type MakeArrayType(int rank)
		{
			if (rank < 1)
			{
				throw new IndexOutOfRangeException();
			}
			return new ArrayType(this, rank);
		}

		// Token: 0x06002439 RID: 9273 RVA: 0x00081ECC File Offset: 0x000800CC
		public override Type MakeByRefType()
		{
			return new ByRefType(this);
		}

		// Token: 0x0600243A RID: 9274 RVA: 0x00081ED4 File Offset: 0x000800D4
		public override Type MakePointerType()
		{
			return new PointerType(this);
		}

		// Token: 0x0600243B RID: 9275 RVA: 0x00081EDC File Offset: 0x000800DC
		public override string ToString()
		{
			return this.FormatName(this.elementType.ToString());
		}

		// Token: 0x17000658 RID: 1624
		// (get) Token: 0x0600243C RID: 9276 RVA: 0x00081EF0 File Offset: 0x000800F0
		public override GenericParameterAttributes GenericParameterAttributes
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000659 RID: 1625
		// (get) Token: 0x0600243D RID: 9277 RVA: 0x00081EF8 File Offset: 0x000800F8
		public override StructLayoutAttribute StructLayoutAttribute
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x1700065A RID: 1626
		// (get) Token: 0x0600243E RID: 9278 RVA: 0x00081F00 File Offset: 0x00080100
		public override Assembly Assembly
		{
			get
			{
				return this.elementType.Assembly;
			}
		}

		// Token: 0x1700065B RID: 1627
		// (get) Token: 0x0600243F RID: 9279 RVA: 0x00081F10 File Offset: 0x00080110
		public override string AssemblyQualifiedName
		{
			get
			{
				string text = this.FormatName(this.elementType.FullName);
				if (text == null)
				{
					return null;
				}
				return text + ", " + this.elementType.Assembly.FullName;
			}
		}

		// Token: 0x1700065C RID: 1628
		// (get) Token: 0x06002440 RID: 9280 RVA: 0x00081F54 File Offset: 0x00080154
		public override string FullName
		{
			get
			{
				return this.FormatName(this.elementType.FullName);
			}
		}

		// Token: 0x1700065D RID: 1629
		// (get) Token: 0x06002441 RID: 9281 RVA: 0x00081F68 File Offset: 0x00080168
		public override string Name
		{
			get
			{
				return this.FormatName(this.elementType.Name);
			}
		}

		// Token: 0x1700065E RID: 1630
		// (get) Token: 0x06002442 RID: 9282 RVA: 0x00081F7C File Offset: 0x0008017C
		public override Guid GUID
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x1700065F RID: 1631
		// (get) Token: 0x06002443 RID: 9283 RVA: 0x00081F84 File Offset: 0x00080184
		public override Module Module
		{
			get
			{
				return this.elementType.Module;
			}
		}

		// Token: 0x17000660 RID: 1632
		// (get) Token: 0x06002444 RID: 9284 RVA: 0x00081F94 File Offset: 0x00080194
		public override string Namespace
		{
			get
			{
				return this.elementType.Namespace;
			}
		}

		// Token: 0x17000661 RID: 1633
		// (get) Token: 0x06002445 RID: 9285 RVA: 0x00081FA4 File Offset: 0x000801A4
		public override RuntimeTypeHandle TypeHandle
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000662 RID: 1634
		// (get) Token: 0x06002446 RID: 9286 RVA: 0x00081FAC File Offset: 0x000801AC
		public override Type UnderlyingSystemType
		{
			get
			{
				DerivedType.create_unmanaged_type(this);
				return this;
			}
		}

		// Token: 0x06002447 RID: 9287 RVA: 0x00081FB8 File Offset: 0x000801B8
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002448 RID: 9288 RVA: 0x00081FC0 File Offset: 0x000801C0
		public override object[] GetCustomAttributes(bool inherit)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06002449 RID: 9289 RVA: 0x00081FC8 File Offset: 0x000801C8
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			throw new NotSupportedException();
		}

		// Token: 0x04000DBD RID: 3517
		internal Type elementType;
	}
}
