﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002D9 RID: 729
	[ComVisible(true)]
	[Serializable]
	public struct FieldToken
	{
		// Token: 0x06002524 RID: 9508 RVA: 0x000836E0 File Offset: 0x000818E0
		internal FieldToken(int val)
		{
			this.tokValue = val;
		}

		// Token: 0x06002526 RID: 9510 RVA: 0x00083708 File Offset: 0x00081908
		public override bool Equals(object obj)
		{
			bool flag = obj is FieldToken;
			if (flag)
			{
				FieldToken fieldToken = (FieldToken)obj;
				flag = (this.tokValue == fieldToken.tokValue);
			}
			return flag;
		}

		// Token: 0x06002527 RID: 9511 RVA: 0x00083740 File Offset: 0x00081940
		public bool Equals(FieldToken obj)
		{
			return this.tokValue == obj.tokValue;
		}

		// Token: 0x06002528 RID: 9512 RVA: 0x00083754 File Offset: 0x00081954
		public override int GetHashCode()
		{
			return this.tokValue;
		}

		// Token: 0x17000694 RID: 1684
		// (get) Token: 0x06002529 RID: 9513 RVA: 0x0008375C File Offset: 0x0008195C
		public int Token
		{
			get
			{
				return this.tokValue;
			}
		}

		// Token: 0x0600252A RID: 9514 RVA: 0x00083764 File Offset: 0x00081964
		public static bool operator ==(FieldToken a, FieldToken b)
		{
			return object.Equals(a, b);
		}

		// Token: 0x0600252B RID: 9515 RVA: 0x00083778 File Offset: 0x00081978
		public static bool operator !=(FieldToken a, FieldToken b)
		{
			return !object.Equals(a, b);
		}

		// Token: 0x04000DF3 RID: 3571
		internal int tokValue;

		// Token: 0x04000DF4 RID: 3572
		public static readonly FieldToken Empty = default(FieldToken);
	}
}
