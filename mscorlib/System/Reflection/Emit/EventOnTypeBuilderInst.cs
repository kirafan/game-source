﻿using System;
using System.Collections;

namespace System.Reflection.Emit
{
	// Token: 0x020002D5 RID: 725
	internal class EventOnTypeBuilderInst : EventInfo
	{
		// Token: 0x060024E6 RID: 9446 RVA: 0x00082FC8 File Offset: 0x000811C8
		internal EventOnTypeBuilderInst(MonoGenericClass instantiation, EventBuilder evt)
		{
			this.instantiation = instantiation;
			this.evt = evt;
		}

		// Token: 0x17000680 RID: 1664
		// (get) Token: 0x060024E7 RID: 9447 RVA: 0x00082FE0 File Offset: 0x000811E0
		public override EventAttributes Attributes
		{
			get
			{
				return this.evt.attrs;
			}
		}

		// Token: 0x060024E8 RID: 9448 RVA: 0x00082FF0 File Offset: 0x000811F0
		public override MethodInfo GetAddMethod(bool nonPublic)
		{
			if (this.evt.add_method == null || (!nonPublic && !this.evt.add_method.IsPublic))
			{
				return null;
			}
			return TypeBuilder.GetMethod(this.instantiation, this.evt.add_method);
		}

		// Token: 0x060024E9 RID: 9449 RVA: 0x00083040 File Offset: 0x00081240
		public override MethodInfo GetRaiseMethod(bool nonPublic)
		{
			if (this.evt.raise_method == null || (!nonPublic && !this.evt.raise_method.IsPublic))
			{
				return null;
			}
			return TypeBuilder.GetMethod(this.instantiation, this.evt.raise_method);
		}

		// Token: 0x060024EA RID: 9450 RVA: 0x00083090 File Offset: 0x00081290
		public override MethodInfo GetRemoveMethod(bool nonPublic)
		{
			if (this.evt.remove_method == null || (!nonPublic && !this.evt.remove_method.IsPublic))
			{
				return null;
			}
			return TypeBuilder.GetMethod(this.instantiation, this.evt.remove_method);
		}

		// Token: 0x060024EB RID: 9451 RVA: 0x000830E0 File Offset: 0x000812E0
		public override MethodInfo[] GetOtherMethods(bool nonPublic)
		{
			if (this.evt.other_methods == null)
			{
				return new MethodInfo[0];
			}
			ArrayList arrayList = new ArrayList();
			foreach (MethodBuilder methodInfo in this.evt.other_methods)
			{
				if (nonPublic || methodInfo.IsPublic)
				{
					arrayList.Add(TypeBuilder.GetMethod(this.instantiation, methodInfo));
				}
			}
			MethodInfo[] array = new MethodInfo[arrayList.Count];
			arrayList.CopyTo(array, 0);
			return array;
		}

		// Token: 0x17000681 RID: 1665
		// (get) Token: 0x060024EC RID: 9452 RVA: 0x0008316C File Offset: 0x0008136C
		public override Type DeclaringType
		{
			get
			{
				return this.instantiation;
			}
		}

		// Token: 0x17000682 RID: 1666
		// (get) Token: 0x060024ED RID: 9453 RVA: 0x00083174 File Offset: 0x00081374
		public override string Name
		{
			get
			{
				return this.evt.name;
			}
		}

		// Token: 0x17000683 RID: 1667
		// (get) Token: 0x060024EE RID: 9454 RVA: 0x00083184 File Offset: 0x00081384
		public override Type ReflectedType
		{
			get
			{
				return this.instantiation;
			}
		}

		// Token: 0x060024EF RID: 9455 RVA: 0x0008318C File Offset: 0x0008138C
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060024F0 RID: 9456 RVA: 0x00083194 File Offset: 0x00081394
		public override object[] GetCustomAttributes(bool inherit)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060024F1 RID: 9457 RVA: 0x0008319C File Offset: 0x0008139C
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			throw new NotSupportedException();
		}

		// Token: 0x04000DE0 RID: 3552
		private MonoGenericClass instantiation;

		// Token: 0x04000DE1 RID: 3553
		private EventBuilder evt;
	}
}
