﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002FE RID: 766
	[ComVisible(true)]
	[Serializable]
	public struct StringToken
	{
		// Token: 0x06002734 RID: 10036 RVA: 0x0008B430 File Offset: 0x00089630
		internal StringToken(int val)
		{
			this.tokValue = val;
		}

		// Token: 0x06002736 RID: 10038 RVA: 0x0008B440 File Offset: 0x00089640
		public override bool Equals(object obj)
		{
			bool flag = obj is StringToken;
			if (flag)
			{
				StringToken stringToken = (StringToken)obj;
				flag = (this.tokValue == stringToken.tokValue);
			}
			return flag;
		}

		// Token: 0x06002737 RID: 10039 RVA: 0x0008B478 File Offset: 0x00089678
		public bool Equals(StringToken obj)
		{
			return this.tokValue == obj.tokValue;
		}

		// Token: 0x06002738 RID: 10040 RVA: 0x0008B48C File Offset: 0x0008968C
		public override int GetHashCode()
		{
			return this.tokValue;
		}

		// Token: 0x170006FF RID: 1791
		// (get) Token: 0x06002739 RID: 10041 RVA: 0x0008B494 File Offset: 0x00089694
		public int Token
		{
			get
			{
				return this.tokValue;
			}
		}

		// Token: 0x0600273A RID: 10042 RVA: 0x0008B49C File Offset: 0x0008969C
		public static bool operator ==(StringToken a, StringToken b)
		{
			return object.Equals(a, b);
		}

		// Token: 0x0600273B RID: 10043 RVA: 0x0008B4B0 File Offset: 0x000896B0
		public static bool operator !=(StringToken a, StringToken b)
		{
			return !object.Equals(a, b);
		}

		// Token: 0x04000FDD RID: 4061
		internal int tokValue;
	}
}
