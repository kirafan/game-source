﻿using System;
using System.Collections;
using System.Diagnostics.SymbolStore;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002E0 RID: 736
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_ILGenerator))]
	[ClassInterface(ClassInterfaceType.None)]
	public class ILGenerator : _ILGenerator
	{
		// Token: 0x06002583 RID: 9603 RVA: 0x00083EB4 File Offset: 0x000820B4
		internal ILGenerator(Module m, TokenGenerator token_gen, int size)
		{
			if (size < 0)
			{
				size = 128;
			}
			this.code = new byte[size];
			this.token_fixups = new ILTokenInfo[8];
			this.module = m;
			this.token_gen = token_gen;
		}

		// Token: 0x06002585 RID: 9605 RVA: 0x00083F10 File Offset: 0x00082110
		void _ILGenerator.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002586 RID: 9606 RVA: 0x00083F18 File Offset: 0x00082118
		void _ILGenerator.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002587 RID: 9607 RVA: 0x00083F20 File Offset: 0x00082120
		void _ILGenerator.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002588 RID: 9608 RVA: 0x00083F28 File Offset: 0x00082128
		void _ILGenerator.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002589 RID: 9609 RVA: 0x00083F30 File Offset: 0x00082130
		private void add_token_fixup(MemberInfo mi)
		{
			if (this.num_token_fixups == this.token_fixups.Length)
			{
				ILTokenInfo[] array = new ILTokenInfo[this.num_token_fixups * 2];
				this.token_fixups.CopyTo(array, 0);
				this.token_fixups = array;
			}
			this.token_fixups[this.num_token_fixups].member = mi;
			this.token_fixups[this.num_token_fixups++].code_pos = this.code_len;
		}

		// Token: 0x0600258A RID: 9610 RVA: 0x00083FB0 File Offset: 0x000821B0
		private void make_room(int nbytes)
		{
			if (this.code_len + nbytes < this.code.Length)
			{
				return;
			}
			byte[] destinationArray = new byte[(this.code_len + nbytes) * 2 + 128];
			Array.Copy(this.code, 0, destinationArray, 0, this.code.Length);
			this.code = destinationArray;
		}

		// Token: 0x0600258B RID: 9611 RVA: 0x00084008 File Offset: 0x00082208
		private void emit_int(int val)
		{
			this.code[this.code_len++] = (byte)(val & 255);
			this.code[this.code_len++] = (byte)(val >> 8 & 255);
			this.code[this.code_len++] = (byte)(val >> 16 & 255);
			this.code[this.code_len++] = (byte)(val >> 24 & 255);
		}

		// Token: 0x0600258C RID: 9612 RVA: 0x000840A0 File Offset: 0x000822A0
		private void ll_emit(OpCode opcode)
		{
			if (opcode.Size == 2)
			{
				this.code[this.code_len++] = opcode.op1;
			}
			this.code[this.code_len++] = opcode.op2;
			switch (opcode.StackBehaviourPush)
			{
			case StackBehaviour.Push1:
			case StackBehaviour.Pushi:
			case StackBehaviour.Pushi8:
			case StackBehaviour.Pushr4:
			case StackBehaviour.Pushr8:
			case StackBehaviour.Pushref:
			case StackBehaviour.Varpush:
				this.cur_stack++;
				break;
			case StackBehaviour.Push1_push1:
				this.cur_stack += 2;
				break;
			}
			if (this.max_stack < this.cur_stack)
			{
				this.max_stack = this.cur_stack;
			}
			switch (opcode.StackBehaviourPop)
			{
			case StackBehaviour.Pop1:
			case StackBehaviour.Popi:
			case StackBehaviour.Popref:
				this.cur_stack--;
				break;
			case StackBehaviour.Pop1_pop1:
			case StackBehaviour.Popi_pop1:
			case StackBehaviour.Popi_popi:
			case StackBehaviour.Popi_popi8:
			case StackBehaviour.Popi_popr4:
			case StackBehaviour.Popi_popr8:
			case StackBehaviour.Popref_pop1:
			case StackBehaviour.Popref_popi:
				this.cur_stack -= 2;
				break;
			case StackBehaviour.Popi_popi_popi:
			case StackBehaviour.Popref_popi_popi:
			case StackBehaviour.Popref_popi_popi8:
			case StackBehaviour.Popref_popi_popr4:
			case StackBehaviour.Popref_popi_popr8:
			case StackBehaviour.Popref_popi_popref:
				this.cur_stack -= 3;
				break;
			}
		}

		// Token: 0x0600258D RID: 9613 RVA: 0x00084230 File Offset: 0x00082430
		private static int target_len(OpCode opcode)
		{
			if (opcode.OperandType == OperandType.InlineBrTarget)
			{
				return 4;
			}
			return 1;
		}

		// Token: 0x0600258E RID: 9614 RVA: 0x00084244 File Offset: 0x00082444
		private void InternalEndClause()
		{
			int num = this.ex_handlers[this.cur_block].LastClauseType();
			switch (num + 1)
			{
			case 0:
			case 1:
			case 2:
				this.Emit(OpCodes.Leave, this.ex_handlers[this.cur_block].end);
				break;
			case 3:
			case 5:
				this.Emit(OpCodes.Endfinally);
				break;
			}
		}

		// Token: 0x0600258F RID: 9615 RVA: 0x000842C4 File Offset: 0x000824C4
		public virtual void BeginCatchBlock(Type exceptionType)
		{
			if (this.open_blocks == null)
			{
				this.open_blocks = new Stack(2);
			}
			if (this.open_blocks.Count <= 0)
			{
				throw new NotSupportedException("Not in an exception block");
			}
			if (exceptionType != null && exceptionType.IsUserType)
			{
				throw new NotSupportedException("User defined subclasses of System.Type are not yet supported.");
			}
			if (this.ex_handlers[this.cur_block].LastClauseType() == -1)
			{
				if (exceptionType != null)
				{
					throw new ArgumentException("Do not supply an exception type for filter clause");
				}
				this.Emit(OpCodes.Endfilter);
				this.ex_handlers[this.cur_block].PatchFilterClause(this.code_len);
			}
			else
			{
				this.InternalEndClause();
				this.ex_handlers[this.cur_block].AddCatch(exceptionType, this.code_len);
			}
			this.cur_stack = 1;
			if (this.max_stack < this.cur_stack)
			{
				this.max_stack = this.cur_stack;
			}
		}

		// Token: 0x06002590 RID: 9616 RVA: 0x000843C0 File Offset: 0x000825C0
		public virtual void BeginExceptFilterBlock()
		{
			if (this.open_blocks == null)
			{
				this.open_blocks = new Stack(2);
			}
			if (this.open_blocks.Count <= 0)
			{
				throw new NotSupportedException("Not in an exception block");
			}
			this.InternalEndClause();
			this.ex_handlers[this.cur_block].AddFilter(this.code_len);
		}

		// Token: 0x06002591 RID: 9617 RVA: 0x00084424 File Offset: 0x00082624
		public virtual Label BeginExceptionBlock()
		{
			if (this.open_blocks == null)
			{
				this.open_blocks = new Stack(2);
			}
			if (this.ex_handlers != null)
			{
				this.cur_block = this.ex_handlers.Length;
				ILExceptionInfo[] destinationArray = new ILExceptionInfo[this.cur_block + 1];
				Array.Copy(this.ex_handlers, destinationArray, this.cur_block);
				this.ex_handlers = destinationArray;
			}
			else
			{
				this.ex_handlers = new ILExceptionInfo[1];
				this.cur_block = 0;
			}
			this.open_blocks.Push(this.cur_block);
			this.ex_handlers[this.cur_block].start = this.code_len;
			return this.ex_handlers[this.cur_block].end = this.DefineLabel();
		}

		// Token: 0x06002592 RID: 9618 RVA: 0x000844F4 File Offset: 0x000826F4
		public virtual void BeginFaultBlock()
		{
			if (this.open_blocks == null)
			{
				this.open_blocks = new Stack(2);
			}
			if (this.open_blocks.Count <= 0)
			{
				throw new NotSupportedException("Not in an exception block");
			}
			if (this.ex_handlers[this.cur_block].LastClauseType() == -1)
			{
				this.Emit(OpCodes.Leave, this.ex_handlers[this.cur_block].end);
				this.ex_handlers[this.cur_block].PatchFilterClause(this.code_len);
			}
			this.InternalEndClause();
			this.ex_handlers[this.cur_block].AddFault(this.code_len);
		}

		// Token: 0x06002593 RID: 9619 RVA: 0x000845B0 File Offset: 0x000827B0
		public virtual void BeginFinallyBlock()
		{
			if (this.open_blocks == null)
			{
				this.open_blocks = new Stack(2);
			}
			if (this.open_blocks.Count <= 0)
			{
				throw new NotSupportedException("Not in an exception block");
			}
			this.InternalEndClause();
			if (this.ex_handlers[this.cur_block].LastClauseType() == -1)
			{
				this.Emit(OpCodes.Leave, this.ex_handlers[this.cur_block].end);
				this.ex_handlers[this.cur_block].PatchFilterClause(this.code_len);
			}
			this.ex_handlers[this.cur_block].AddFinally(this.code_len);
		}

		// Token: 0x06002594 RID: 9620 RVA: 0x0008466C File Offset: 0x0008286C
		public virtual void BeginScope()
		{
		}

		// Token: 0x06002595 RID: 9621 RVA: 0x00084670 File Offset: 0x00082870
		public virtual LocalBuilder DeclareLocal(Type localType)
		{
			return this.DeclareLocal(localType, false);
		}

		// Token: 0x06002596 RID: 9622 RVA: 0x0008467C File Offset: 0x0008287C
		public virtual LocalBuilder DeclareLocal(Type localType, bool pinned)
		{
			if (localType == null)
			{
				throw new ArgumentNullException("localType");
			}
			if (localType.IsUserType)
			{
				throw new NotSupportedException("User defined subclasses of System.Type are not yet supported.");
			}
			LocalBuilder localBuilder = new LocalBuilder(localType, this);
			localBuilder.is_pinned = pinned;
			if (this.locals != null)
			{
				LocalBuilder[] array = new LocalBuilder[this.locals.Length + 1];
				Array.Copy(this.locals, array, this.locals.Length);
				array[this.locals.Length] = localBuilder;
				this.locals = array;
			}
			else
			{
				this.locals = new LocalBuilder[1];
				this.locals[0] = localBuilder;
			}
			localBuilder.position = (ushort)(this.locals.Length - 1);
			return localBuilder;
		}

		// Token: 0x06002597 RID: 9623 RVA: 0x0008472C File Offset: 0x0008292C
		public virtual Label DefineLabel()
		{
			if (this.labels == null)
			{
				this.labels = new ILGenerator.LabelData[4];
			}
			else if (this.num_labels >= this.labels.Length)
			{
				ILGenerator.LabelData[] destinationArray = new ILGenerator.LabelData[this.labels.Length * 2];
				Array.Copy(this.labels, destinationArray, this.labels.Length);
				this.labels = destinationArray;
			}
			this.labels[this.num_labels] = new ILGenerator.LabelData(-1, 0);
			return new Label(this.num_labels++);
		}

		// Token: 0x06002598 RID: 9624 RVA: 0x000847C8 File Offset: 0x000829C8
		public virtual void Emit(OpCode opcode)
		{
			this.make_room(2);
			this.ll_emit(opcode);
		}

		// Token: 0x06002599 RID: 9625 RVA: 0x000847D8 File Offset: 0x000829D8
		public virtual void Emit(OpCode opcode, byte arg)
		{
			this.make_room(3);
			this.ll_emit(opcode);
			this.code[this.code_len++] = arg;
		}

		// Token: 0x0600259A RID: 9626 RVA: 0x0008480C File Offset: 0x00082A0C
		[ComVisible(true)]
		public virtual void Emit(OpCode opcode, ConstructorInfo con)
		{
			int token = this.token_gen.GetToken(con);
			this.make_room(6);
			this.ll_emit(opcode);
			if (con.DeclaringType.Module == this.module)
			{
				this.add_token_fixup(con);
			}
			this.emit_int(token);
			if (opcode.StackBehaviourPop == StackBehaviour.Varpop)
			{
				this.cur_stack -= con.GetParameterCount();
			}
		}

		// Token: 0x0600259B RID: 9627 RVA: 0x0008487C File Offset: 0x00082A7C
		public virtual void Emit(OpCode opcode, double arg)
		{
			byte[] bytes = BitConverter.GetBytes(arg);
			this.make_room(10);
			this.ll_emit(opcode);
			if (BitConverter.IsLittleEndian)
			{
				Array.Copy(bytes, 0, this.code, this.code_len, 8);
				this.code_len += 8;
			}
			else
			{
				this.code[this.code_len++] = bytes[7];
				this.code[this.code_len++] = bytes[6];
				this.code[this.code_len++] = bytes[5];
				this.code[this.code_len++] = bytes[4];
				this.code[this.code_len++] = bytes[3];
				this.code[this.code_len++] = bytes[2];
				this.code[this.code_len++] = bytes[1];
				this.code[this.code_len++] = bytes[0];
			}
		}

		// Token: 0x0600259C RID: 9628 RVA: 0x000849A8 File Offset: 0x00082BA8
		public virtual void Emit(OpCode opcode, FieldInfo field)
		{
			int token = this.token_gen.GetToken(field);
			this.make_room(6);
			this.ll_emit(opcode);
			if (field.DeclaringType.Module == this.module)
			{
				this.add_token_fixup(field);
			}
			this.emit_int(token);
		}

		// Token: 0x0600259D RID: 9629 RVA: 0x000849F4 File Offset: 0x00082BF4
		public virtual void Emit(OpCode opcode, short arg)
		{
			this.make_room(4);
			this.ll_emit(opcode);
			this.code[this.code_len++] = (byte)(arg & 255);
			this.code[this.code_len++] = (byte)(arg >> 8 & 255);
		}

		// Token: 0x0600259E RID: 9630 RVA: 0x00084A54 File Offset: 0x00082C54
		public virtual void Emit(OpCode opcode, int arg)
		{
			this.make_room(6);
			this.ll_emit(opcode);
			this.emit_int(arg);
		}

		// Token: 0x0600259F RID: 9631 RVA: 0x00084A78 File Offset: 0x00082C78
		public virtual void Emit(OpCode opcode, long arg)
		{
			this.make_room(10);
			this.ll_emit(opcode);
			this.code[this.code_len++] = (byte)(arg & 255L);
			this.code[this.code_len++] = (byte)(arg >> 8 & 255L);
			this.code[this.code_len++] = (byte)(arg >> 16 & 255L);
			this.code[this.code_len++] = (byte)(arg >> 24 & 255L);
			this.code[this.code_len++] = (byte)(arg >> 32 & 255L);
			this.code[this.code_len++] = (byte)(arg >> 40 & 255L);
			this.code[this.code_len++] = (byte)(arg >> 48 & 255L);
			this.code[this.code_len++] = (byte)(arg >> 56 & 255L);
		}

		// Token: 0x060025A0 RID: 9632 RVA: 0x00084BB0 File Offset: 0x00082DB0
		public virtual void Emit(OpCode opcode, Label label)
		{
			int num = ILGenerator.target_len(opcode);
			this.make_room(6);
			this.ll_emit(opcode);
			if (this.cur_stack > this.labels[label.label].maxStack)
			{
				this.labels[label.label].maxStack = this.cur_stack;
			}
			if (this.fixups == null)
			{
				this.fixups = new ILGenerator.LabelFixup[4];
			}
			else if (this.num_fixups >= this.fixups.Length)
			{
				ILGenerator.LabelFixup[] destinationArray = new ILGenerator.LabelFixup[this.fixups.Length * 2];
				Array.Copy(this.fixups, destinationArray, this.fixups.Length);
				this.fixups = destinationArray;
			}
			this.fixups[this.num_fixups].offset = num;
			this.fixups[this.num_fixups].pos = this.code_len;
			this.fixups[this.num_fixups].label_idx = label.label;
			this.num_fixups++;
			this.code_len += num;
		}

		// Token: 0x060025A1 RID: 9633 RVA: 0x00084CD8 File Offset: 0x00082ED8
		public virtual void Emit(OpCode opcode, Label[] labels)
		{
			if (labels == null)
			{
				throw new ArgumentNullException("labels");
			}
			int num = labels.Length;
			this.make_room(6 + num * 4);
			this.ll_emit(opcode);
			for (int i = 0; i < num; i++)
			{
				if (this.cur_stack > this.labels[labels[i].label].maxStack)
				{
					this.labels[labels[i].label].maxStack = this.cur_stack;
				}
			}
			this.emit_int(num);
			if (this.fixups == null)
			{
				this.fixups = new ILGenerator.LabelFixup[4 + num];
			}
			else if (this.num_fixups + num >= this.fixups.Length)
			{
				ILGenerator.LabelFixup[] destinationArray = new ILGenerator.LabelFixup[num + this.fixups.Length * 2];
				Array.Copy(this.fixups, destinationArray, this.fixups.Length);
				this.fixups = destinationArray;
			}
			int j = 0;
			int num2 = num * 4;
			while (j < num)
			{
				this.fixups[this.num_fixups].offset = num2;
				this.fixups[this.num_fixups].pos = this.code_len;
				this.fixups[this.num_fixups].label_idx = labels[j].label;
				this.num_fixups++;
				this.code_len += 4;
				j++;
				num2 -= 4;
			}
		}

		// Token: 0x060025A2 RID: 9634 RVA: 0x00084E60 File Offset: 0x00083060
		public virtual void Emit(OpCode opcode, LocalBuilder local)
		{
			if (local == null)
			{
				throw new ArgumentNullException("local");
			}
			uint position = (uint)local.position;
			bool flag = false;
			bool flag2 = false;
			this.make_room(6);
			if (local.ilgen != this)
			{
				throw new ArgumentException("Trying to emit a local from a different ILGenerator.");
			}
			if (opcode.StackBehaviourPop == StackBehaviour.Pop1)
			{
				this.cur_stack--;
				flag2 = true;
			}
			else
			{
				this.cur_stack++;
				if (this.cur_stack > this.max_stack)
				{
					this.max_stack = this.cur_stack;
				}
				flag = (opcode.StackBehaviourPush == StackBehaviour.Pushi);
			}
			if (flag)
			{
				if (position < 256U)
				{
					this.code[this.code_len++] = 18;
					this.code[this.code_len++] = (byte)position;
				}
				else
				{
					this.code[this.code_len++] = 254;
					this.code[this.code_len++] = 13;
					this.code[this.code_len++] = (byte)(position & 255U);
					this.code[this.code_len++] = (byte)(position >> 8 & 255U);
				}
			}
			else if (flag2)
			{
				if (position < 4U)
				{
					this.code[this.code_len++] = (byte)(10U + position);
				}
				else if (position < 256U)
				{
					this.code[this.code_len++] = 19;
					this.code[this.code_len++] = (byte)position;
				}
				else
				{
					this.code[this.code_len++] = 254;
					this.code[this.code_len++] = 14;
					this.code[this.code_len++] = (byte)(position & 255U);
					this.code[this.code_len++] = (byte)(position >> 8 & 255U);
				}
			}
			else if (position < 4U)
			{
				this.code[this.code_len++] = (byte)(6U + position);
			}
			else if (position < 256U)
			{
				this.code[this.code_len++] = 17;
				this.code[this.code_len++] = (byte)position;
			}
			else
			{
				this.code[this.code_len++] = 254;
				this.code[this.code_len++] = 12;
				this.code[this.code_len++] = (byte)(position & 255U);
				this.code[this.code_len++] = (byte)(position >> 8 & 255U);
			}
		}

		// Token: 0x060025A3 RID: 9635 RVA: 0x000851A0 File Offset: 0x000833A0
		public virtual void Emit(OpCode opcode, MethodInfo meth)
		{
			if (meth == null)
			{
				throw new ArgumentNullException("meth");
			}
			if (meth is DynamicMethod && (opcode == OpCodes.Ldftn || opcode == OpCodes.Ldvirtftn || opcode == OpCodes.Ldtoken))
			{
				throw new ArgumentException("Ldtoken, Ldftn and Ldvirtftn OpCodes cannot target DynamicMethods.");
			}
			int token = this.token_gen.GetToken(meth);
			this.make_room(6);
			this.ll_emit(opcode);
			Type declaringType = meth.DeclaringType;
			if (declaringType != null && declaringType.Module == this.module)
			{
				this.add_token_fixup(meth);
			}
			this.emit_int(token);
			if (meth.ReturnType != ILGenerator.void_type)
			{
				this.cur_stack++;
			}
			if (opcode.StackBehaviourPop == StackBehaviour.Varpop)
			{
				this.cur_stack -= meth.GetParameterCount();
			}
		}

		// Token: 0x060025A4 RID: 9636 RVA: 0x0008528C File Offset: 0x0008348C
		private void Emit(OpCode opcode, MethodInfo method, int token)
		{
			this.make_room(6);
			this.ll_emit(opcode);
			Type declaringType = method.DeclaringType;
			if (declaringType != null && declaringType.Module == this.module)
			{
				this.add_token_fixup(method);
			}
			this.emit_int(token);
			if (method.ReturnType != ILGenerator.void_type)
			{
				this.cur_stack++;
			}
			if (opcode.StackBehaviourPop == StackBehaviour.Varpop)
			{
				this.cur_stack -= method.GetParameterCount();
			}
		}

		// Token: 0x060025A5 RID: 9637 RVA: 0x00085314 File Offset: 0x00083514
		[CLSCompliant(false)]
		public void Emit(OpCode opcode, sbyte arg)
		{
			this.make_room(3);
			this.ll_emit(opcode);
			this.code[this.code_len++] = (byte)arg;
		}

		// Token: 0x060025A6 RID: 9638 RVA: 0x0008534C File Offset: 0x0008354C
		public virtual void Emit(OpCode opcode, SignatureHelper signature)
		{
			int token = this.token_gen.GetToken(signature);
			this.make_room(6);
			this.ll_emit(opcode);
			this.emit_int(token);
		}

		// Token: 0x060025A7 RID: 9639 RVA: 0x0008537C File Offset: 0x0008357C
		public virtual void Emit(OpCode opcode, float arg)
		{
			byte[] bytes = BitConverter.GetBytes(arg);
			this.make_room(6);
			this.ll_emit(opcode);
			if (BitConverter.IsLittleEndian)
			{
				Array.Copy(bytes, 0, this.code, this.code_len, 4);
				this.code_len += 4;
			}
			else
			{
				this.code[this.code_len++] = bytes[3];
				this.code[this.code_len++] = bytes[2];
				this.code[this.code_len++] = bytes[1];
				this.code[this.code_len++] = bytes[0];
			}
		}

		// Token: 0x060025A8 RID: 9640 RVA: 0x0008543C File Offset: 0x0008363C
		public virtual void Emit(OpCode opcode, string str)
		{
			int token = this.token_gen.GetToken(str);
			this.make_room(6);
			this.ll_emit(opcode);
			this.emit_int(token);
		}

		// Token: 0x060025A9 RID: 9641 RVA: 0x0008546C File Offset: 0x0008366C
		public virtual void Emit(OpCode opcode, Type cls)
		{
			this.make_room(6);
			this.ll_emit(opcode);
			this.emit_int(this.token_gen.GetToken(cls));
		}

		// Token: 0x060025AA RID: 9642 RVA: 0x0008549C File Offset: 0x0008369C
		[MonoLimitation("vararg methods are not supported")]
		public virtual void EmitCall(OpCode opcode, MethodInfo methodInfo, Type[] optionalParameterTypes)
		{
			if (methodInfo == null)
			{
				throw new ArgumentNullException("methodInfo");
			}
			short value = opcode.Value;
			if (value != OpCodes.Call.Value && value != OpCodes.Callvirt.Value)
			{
				throw new NotSupportedException("Only Call and CallVirt are allowed");
			}
			if ((methodInfo.CallingConvention & CallingConventions.VarArgs) == (CallingConventions)0)
			{
				optionalParameterTypes = null;
			}
			if (optionalParameterTypes == null)
			{
				this.Emit(opcode, methodInfo);
				return;
			}
			if ((methodInfo.CallingConvention & CallingConventions.VarArgs) == (CallingConventions)0)
			{
				throw new InvalidOperationException("Method is not VarArgs method and optional types were passed");
			}
			int token = this.token_gen.GetToken(methodInfo, optionalParameterTypes);
			this.Emit(opcode, methodInfo, token);
		}

		// Token: 0x060025AB RID: 9643 RVA: 0x00085544 File Offset: 0x00083744
		public virtual void EmitCalli(OpCode opcode, CallingConvention unmanagedCallConv, Type returnType, Type[] parameterTypes)
		{
			SignatureHelper methodSigHelper = SignatureHelper.GetMethodSigHelper(this.module, (CallingConventions)0, unmanagedCallConv, returnType, parameterTypes);
			this.Emit(opcode, methodSigHelper);
		}

		// Token: 0x060025AC RID: 9644 RVA: 0x0008556C File Offset: 0x0008376C
		public virtual void EmitCalli(OpCode opcode, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type[] optionalParameterTypes)
		{
			if (optionalParameterTypes != null)
			{
				throw new NotImplementedException();
			}
			SignatureHelper methodSigHelper = SignatureHelper.GetMethodSigHelper(this.module, callingConvention, (CallingConvention)0, returnType, parameterTypes);
			this.Emit(opcode, methodSigHelper);
		}

		// Token: 0x060025AD RID: 9645 RVA: 0x000855A0 File Offset: 0x000837A0
		public virtual void EmitWriteLine(FieldInfo fld)
		{
			if (fld == null)
			{
				throw new ArgumentNullException("fld");
			}
			if (fld.IsStatic)
			{
				this.Emit(OpCodes.Ldsfld, fld);
			}
			else
			{
				this.Emit(OpCodes.Ldarg_0);
				this.Emit(OpCodes.Ldfld, fld);
			}
			this.Emit(OpCodes.Call, typeof(Console).GetMethod("WriteLine", new Type[]
			{
				fld.FieldType
			}));
		}

		// Token: 0x060025AE RID: 9646 RVA: 0x00085620 File Offset: 0x00083820
		public virtual void EmitWriteLine(LocalBuilder localBuilder)
		{
			if (localBuilder == null)
			{
				throw new ArgumentNullException("localBuilder");
			}
			if (localBuilder.LocalType is TypeBuilder)
			{
				throw new ArgumentException("Output streams do not support TypeBuilders.");
			}
			this.Emit(OpCodes.Ldloc, localBuilder);
			this.Emit(OpCodes.Call, typeof(Console).GetMethod("WriteLine", new Type[]
			{
				localBuilder.LocalType
			}));
		}

		// Token: 0x060025AF RID: 9647 RVA: 0x00085694 File Offset: 0x00083894
		public virtual void EmitWriteLine(string value)
		{
			this.Emit(OpCodes.Ldstr, value);
			this.Emit(OpCodes.Call, typeof(Console).GetMethod("WriteLine", new Type[]
			{
				typeof(string)
			}));
		}

		// Token: 0x060025B0 RID: 9648 RVA: 0x000856E0 File Offset: 0x000838E0
		public virtual void EndExceptionBlock()
		{
			if (this.open_blocks == null)
			{
				this.open_blocks = new Stack(2);
			}
			if (this.open_blocks.Count <= 0)
			{
				throw new NotSupportedException("Not in an exception block");
			}
			if (this.ex_handlers[this.cur_block].LastClauseType() == -1)
			{
				throw new InvalidOperationException("Incorrect code generation for exception block.");
			}
			this.InternalEndClause();
			this.MarkLabel(this.ex_handlers[this.cur_block].end);
			this.ex_handlers[this.cur_block].End(this.code_len);
			this.ex_handlers[this.cur_block].Debug(this.cur_block);
			this.open_blocks.Pop();
			if (this.open_blocks.Count > 0)
			{
				this.cur_block = (int)this.open_blocks.Peek();
			}
		}

		// Token: 0x060025B1 RID: 9649 RVA: 0x000857D4 File Offset: 0x000839D4
		public virtual void EndScope()
		{
		}

		// Token: 0x060025B2 RID: 9650 RVA: 0x000857D8 File Offset: 0x000839D8
		public virtual void MarkLabel(Label loc)
		{
			if (loc.label < 0 || loc.label >= this.num_labels)
			{
				throw new ArgumentException("The label is not valid");
			}
			if (this.labels[loc.label].addr >= 0)
			{
				throw new ArgumentException("The label was already defined");
			}
			this.labels[loc.label].addr = this.code_len;
			if (this.labels[loc.label].maxStack > this.cur_stack)
			{
				this.cur_stack = this.labels[loc.label].maxStack;
			}
		}

		// Token: 0x060025B3 RID: 9651 RVA: 0x00085894 File Offset: 0x00083A94
		public virtual void MarkSequencePoint(ISymbolDocumentWriter document, int startLine, int startColumn, int endLine, int endColumn)
		{
			if (this.currentSequence == null || this.currentSequence.Document != document)
			{
				if (this.sequencePointLists == null)
				{
					this.sequencePointLists = new ArrayList();
				}
				this.currentSequence = new SequencePointList(document);
				this.sequencePointLists.Add(this.currentSequence);
			}
			this.currentSequence.AddSequencePoint(this.code_len, startLine, startColumn, endLine, endColumn);
		}

		// Token: 0x060025B4 RID: 9652 RVA: 0x00085908 File Offset: 0x00083B08
		internal void GenerateDebugInfo(ISymbolWriter symbolWriter)
		{
			if (this.sequencePointLists != null)
			{
				SequencePointList sequencePointList = (SequencePointList)this.sequencePointLists[0];
				SequencePointList sequencePointList2 = (SequencePointList)this.sequencePointLists[this.sequencePointLists.Count - 1];
				symbolWriter.SetMethodSourceRange(sequencePointList.Document, sequencePointList.StartLine, sequencePointList.StartColumn, sequencePointList2.Document, sequencePointList2.EndLine, sequencePointList2.EndColumn);
				foreach (object obj in this.sequencePointLists)
				{
					SequencePointList sequencePointList3 = (SequencePointList)obj;
					symbolWriter.DefineSequencePoints(sequencePointList3.Document, sequencePointList3.GetOffsets(), sequencePointList3.GetLines(), sequencePointList3.GetColumns(), sequencePointList3.GetEndLines(), sequencePointList3.GetEndColumns());
				}
				if (this.locals != null)
				{
					foreach (LocalBuilder localBuilder in this.locals)
					{
						if (localBuilder.Name != null && localBuilder.Name.Length > 0)
						{
							SignatureHelper localVarSigHelper = SignatureHelper.GetLocalVarSigHelper(this.module);
							localVarSigHelper.AddArgument(localBuilder.LocalType);
							byte[] signature = localVarSigHelper.GetSignature();
							symbolWriter.DefineLocalVariable(localBuilder.Name, FieldAttributes.Public, signature, SymAddressKind.ILOffset, (int)localBuilder.position, 0, 0, localBuilder.StartOffset, localBuilder.EndOffset);
						}
					}
				}
				this.sequencePointLists = null;
			}
		}

		// Token: 0x170006A8 RID: 1704
		// (get) Token: 0x060025B5 RID: 9653 RVA: 0x00085AA8 File Offset: 0x00083CA8
		internal bool HasDebugInfo
		{
			get
			{
				return this.sequencePointLists != null;
			}
		}

		// Token: 0x060025B6 RID: 9654 RVA: 0x00085AB8 File Offset: 0x00083CB8
		public virtual void ThrowException(Type excType)
		{
			if (excType == null)
			{
				throw new ArgumentNullException("excType");
			}
			if (excType != typeof(Exception) && !excType.IsSubclassOf(typeof(Exception)))
			{
				throw new ArgumentException("Type should be an exception type", "excType");
			}
			ConstructorInfo constructor = excType.GetConstructor(Type.EmptyTypes);
			if (constructor == null)
			{
				throw new ArgumentException("Type should have a default constructor", "excType");
			}
			this.Emit(OpCodes.Newobj, constructor);
			this.Emit(OpCodes.Throw);
		}

		// Token: 0x060025B7 RID: 9655 RVA: 0x00085B44 File Offset: 0x00083D44
		[MonoTODO("Not implemented")]
		public virtual void UsingNamespace(string usingNamespace)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060025B8 RID: 9656 RVA: 0x00085B4C File Offset: 0x00083D4C
		internal void label_fixup()
		{
			for (int i = 0; i < this.num_fixups; i++)
			{
				if (this.labels[this.fixups[i].label_idx].addr < 0)
				{
					throw new ArgumentException("Label not marked");
				}
				int num = this.labels[this.fixups[i].label_idx].addr - (this.fixups[i].pos + this.fixups[i].offset);
				if (this.fixups[i].offset == 1)
				{
					this.code[this.fixups[i].pos] = (byte)((sbyte)num);
				}
				else
				{
					int num2 = this.code_len;
					this.code_len = this.fixups[i].pos;
					this.emit_int(num);
					this.code_len = num2;
				}
			}
		}

		// Token: 0x060025B9 RID: 9657 RVA: 0x00085C4C File Offset: 0x00083E4C
		[Obsolete("Use ILOffset")]
		internal static int Mono_GetCurrentOffset(ILGenerator ig)
		{
			return ig.code_len;
		}

		// Token: 0x04000E17 RID: 3607
		private const int defaultFixupSize = 4;

		// Token: 0x04000E18 RID: 3608
		private const int defaultLabelsSize = 4;

		// Token: 0x04000E19 RID: 3609
		private const int defaultExceptionStackSize = 2;

		// Token: 0x04000E1A RID: 3610
		private static readonly Type void_type = typeof(void);

		// Token: 0x04000E1B RID: 3611
		private byte[] code;

		// Token: 0x04000E1C RID: 3612
		private int code_len;

		// Token: 0x04000E1D RID: 3613
		private int max_stack;

		// Token: 0x04000E1E RID: 3614
		private int cur_stack;

		// Token: 0x04000E1F RID: 3615
		private LocalBuilder[] locals;

		// Token: 0x04000E20 RID: 3616
		private ILExceptionInfo[] ex_handlers;

		// Token: 0x04000E21 RID: 3617
		private int num_token_fixups;

		// Token: 0x04000E22 RID: 3618
		private ILTokenInfo[] token_fixups;

		// Token: 0x04000E23 RID: 3619
		private ILGenerator.LabelData[] labels;

		// Token: 0x04000E24 RID: 3620
		private int num_labels;

		// Token: 0x04000E25 RID: 3621
		private ILGenerator.LabelFixup[] fixups;

		// Token: 0x04000E26 RID: 3622
		private int num_fixups;

		// Token: 0x04000E27 RID: 3623
		internal Module module;

		// Token: 0x04000E28 RID: 3624
		private int cur_block;

		// Token: 0x04000E29 RID: 3625
		private Stack open_blocks;

		// Token: 0x04000E2A RID: 3626
		private TokenGenerator token_gen;

		// Token: 0x04000E2B RID: 3627
		private ArrayList sequencePointLists;

		// Token: 0x04000E2C RID: 3628
		private SequencePointList currentSequence;

		// Token: 0x020002E1 RID: 737
		private struct LabelFixup
		{
			// Token: 0x04000E2D RID: 3629
			public int offset;

			// Token: 0x04000E2E RID: 3630
			public int pos;

			// Token: 0x04000E2F RID: 3631
			public int label_idx;
		}

		// Token: 0x020002E2 RID: 738
		private struct LabelData
		{
			// Token: 0x060025BA RID: 9658 RVA: 0x00085C54 File Offset: 0x00083E54
			public LabelData(int addr, int maxStack)
			{
				this.addr = addr;
				this.maxStack = maxStack;
			}

			// Token: 0x04000E30 RID: 3632
			public int addr;

			// Token: 0x04000E31 RID: 3633
			public int maxStack;
		}
	}
}
