﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002C5 RID: 709
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum AssemblyBuilderAccess
	{
		// Token: 0x04000D9F RID: 3487
		Run = 1,
		// Token: 0x04000DA0 RID: 3488
		Save = 2,
		// Token: 0x04000DA1 RID: 3489
		RunAndSave = 3,
		// Token: 0x04000DA2 RID: 3490
		ReflectionOnly = 6
	}
}
