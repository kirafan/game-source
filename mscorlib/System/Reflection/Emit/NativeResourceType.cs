﻿using System;

namespace System.Reflection.Emit
{
	// Token: 0x020002C0 RID: 704
	internal enum NativeResourceType
	{
		// Token: 0x04000D67 RID: 3431
		None,
		// Token: 0x04000D68 RID: 3432
		Unmanaged,
		// Token: 0x04000D69 RID: 3433
		Assembly,
		// Token: 0x04000D6A RID: 3434
		Explicit
	}
}
