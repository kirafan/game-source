﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002E6 RID: 742
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_LocalBuilder))]
	public sealed class LocalBuilder : LocalVariableInfo, _LocalBuilder
	{
		// Token: 0x060025CD RID: 9677 RVA: 0x00085F84 File Offset: 0x00084184
		internal LocalBuilder(Type t, ILGenerator ilgen)
		{
			this.type = t;
			this.ilgen = ilgen;
		}

		// Token: 0x060025CE RID: 9678 RVA: 0x00085F9C File Offset: 0x0008419C
		void _LocalBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060025CF RID: 9679 RVA: 0x00085FA4 File Offset: 0x000841A4
		void _LocalBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060025D0 RID: 9680 RVA: 0x00085FAC File Offset: 0x000841AC
		void _LocalBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060025D1 RID: 9681 RVA: 0x00085FB4 File Offset: 0x000841B4
		void _LocalBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060025D2 RID: 9682 RVA: 0x00085FBC File Offset: 0x000841BC
		public void SetLocalSymInfo(string name, int startOffset, int endOffset)
		{
			this.name = name;
			this.startOffset = startOffset;
			this.endOffset = endOffset;
		}

		// Token: 0x060025D3 RID: 9683 RVA: 0x00085FD4 File Offset: 0x000841D4
		public void SetLocalSymInfo(string name)
		{
			this.SetLocalSymInfo(name, 0, 0);
		}

		// Token: 0x170006AE RID: 1710
		// (get) Token: 0x060025D4 RID: 9684 RVA: 0x00085FE0 File Offset: 0x000841E0
		public override Type LocalType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x170006AF RID: 1711
		// (get) Token: 0x060025D5 RID: 9685 RVA: 0x00085FE8 File Offset: 0x000841E8
		public override bool IsPinned
		{
			get
			{
				return this.is_pinned;
			}
		}

		// Token: 0x170006B0 RID: 1712
		// (get) Token: 0x060025D6 RID: 9686 RVA: 0x00085FF0 File Offset: 0x000841F0
		public override int LocalIndex
		{
			get
			{
				return (int)this.position;
			}
		}

		// Token: 0x060025D7 RID: 9687 RVA: 0x00085FF8 File Offset: 0x000841F8
		internal static int Mono_GetLocalIndex(LocalBuilder builder)
		{
			return (int)builder.position;
		}

		// Token: 0x170006B1 RID: 1713
		// (get) Token: 0x060025D8 RID: 9688 RVA: 0x00086000 File Offset: 0x00084200
		internal string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170006B2 RID: 1714
		// (get) Token: 0x060025D9 RID: 9689 RVA: 0x00086008 File Offset: 0x00084208
		internal int StartOffset
		{
			get
			{
				return this.startOffset;
			}
		}

		// Token: 0x170006B3 RID: 1715
		// (get) Token: 0x060025DA RID: 9690 RVA: 0x00086010 File Offset: 0x00084210
		internal int EndOffset
		{
			get
			{
				return this.endOffset;
			}
		}

		// Token: 0x04000E3C RID: 3644
		private string name;

		// Token: 0x04000E3D RID: 3645
		internal ILGenerator ilgen;

		// Token: 0x04000E3E RID: 3646
		private int startOffset;

		// Token: 0x04000E3F RID: 3647
		private int endOffset;
	}
}
