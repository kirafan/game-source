﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002D6 RID: 726
	[ComVisible(true)]
	[Serializable]
	public struct EventToken
	{
		// Token: 0x060024F2 RID: 9458 RVA: 0x000831A4 File Offset: 0x000813A4
		internal EventToken(int val)
		{
			this.tokValue = val;
		}

		// Token: 0x060024F4 RID: 9460 RVA: 0x000831CC File Offset: 0x000813CC
		public override bool Equals(object obj)
		{
			bool flag = obj is EventToken;
			if (flag)
			{
				EventToken eventToken = (EventToken)obj;
				flag = (this.tokValue == eventToken.tokValue);
			}
			return flag;
		}

		// Token: 0x060024F5 RID: 9461 RVA: 0x00083204 File Offset: 0x00081404
		public bool Equals(EventToken obj)
		{
			return this.tokValue == obj.tokValue;
		}

		// Token: 0x060024F6 RID: 9462 RVA: 0x00083218 File Offset: 0x00081418
		public override int GetHashCode()
		{
			return this.tokValue;
		}

		// Token: 0x17000684 RID: 1668
		// (get) Token: 0x060024F7 RID: 9463 RVA: 0x00083220 File Offset: 0x00081420
		public int Token
		{
			get
			{
				return this.tokValue;
			}
		}

		// Token: 0x060024F8 RID: 9464 RVA: 0x00083228 File Offset: 0x00081428
		public static bool operator ==(EventToken a, EventToken b)
		{
			return object.Equals(a, b);
		}

		// Token: 0x060024F9 RID: 9465 RVA: 0x0008323C File Offset: 0x0008143C
		public static bool operator !=(EventToken a, EventToken b)
		{
			return !object.Equals(a, b);
		}

		// Token: 0x04000DE2 RID: 3554
		internal int tokValue;

		// Token: 0x04000DE3 RID: 3555
		public static readonly EventToken Empty = default(EventToken);
	}
}
