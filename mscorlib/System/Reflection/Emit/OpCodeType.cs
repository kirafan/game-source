﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002F1 RID: 753
	[ComVisible(true)]
	[Serializable]
	public enum OpCodeType
	{
		// Token: 0x04000F6D RID: 3949
		[Obsolete("This API has been deprecated.")]
		Annotation,
		// Token: 0x04000F6E RID: 3950
		Macro,
		// Token: 0x04000F6F RID: 3951
		Nternal,
		// Token: 0x04000F70 RID: 3952
		Objmodel,
		// Token: 0x04000F71 RID: 3953
		Prefix,
		// Token: 0x04000F72 RID: 3954
		Primitive
	}
}
