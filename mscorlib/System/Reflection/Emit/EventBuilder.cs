﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002D4 RID: 724
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_EventBuilder))]
	public sealed class EventBuilder : _EventBuilder
	{
		// Token: 0x060024D8 RID: 9432 RVA: 0x00082D74 File Offset: 0x00080F74
		internal EventBuilder(TypeBuilder tb, string eventName, EventAttributes eventAttrs, Type eventType)
		{
			this.name = eventName;
			this.attrs = eventAttrs;
			this.type = eventType;
			this.typeb = tb;
			this.table_idx = this.get_next_table_index(this, 20, true);
		}

		// Token: 0x060024D9 RID: 9433 RVA: 0x00082DAC File Offset: 0x00080FAC
		void _EventBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060024DA RID: 9434 RVA: 0x00082DB4 File Offset: 0x00080FB4
		void _EventBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060024DB RID: 9435 RVA: 0x00082DBC File Offset: 0x00080FBC
		void _EventBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060024DC RID: 9436 RVA: 0x00082DC4 File Offset: 0x00080FC4
		void _EventBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060024DD RID: 9437 RVA: 0x00082DCC File Offset: 0x00080FCC
		internal int get_next_table_index(object obj, int table, bool inc)
		{
			return this.typeb.get_next_table_index(obj, table, inc);
		}

		// Token: 0x060024DE RID: 9438 RVA: 0x00082DDC File Offset: 0x00080FDC
		public void AddOtherMethod(MethodBuilder mdBuilder)
		{
			if (mdBuilder == null)
			{
				throw new ArgumentNullException("mdBuilder");
			}
			this.RejectIfCreated();
			if (this.other_methods != null)
			{
				MethodBuilder[] array = new MethodBuilder[this.other_methods.Length + 1];
				this.other_methods.CopyTo(array, 0);
				this.other_methods = array;
			}
			else
			{
				this.other_methods = new MethodBuilder[1];
			}
			this.other_methods[this.other_methods.Length - 1] = mdBuilder;
		}

		// Token: 0x060024DF RID: 9439 RVA: 0x00082E54 File Offset: 0x00081054
		public EventToken GetEventToken()
		{
			return new EventToken(335544320 | this.table_idx);
		}

		// Token: 0x060024E0 RID: 9440 RVA: 0x00082E68 File Offset: 0x00081068
		public void SetAddOnMethod(MethodBuilder mdBuilder)
		{
			if (mdBuilder == null)
			{
				throw new ArgumentNullException("mdBuilder");
			}
			this.RejectIfCreated();
			this.add_method = mdBuilder;
		}

		// Token: 0x060024E1 RID: 9441 RVA: 0x00082E88 File Offset: 0x00081088
		public void SetRaiseMethod(MethodBuilder mdBuilder)
		{
			if (mdBuilder == null)
			{
				throw new ArgumentNullException("mdBuilder");
			}
			this.RejectIfCreated();
			this.raise_method = mdBuilder;
		}

		// Token: 0x060024E2 RID: 9442 RVA: 0x00082EA8 File Offset: 0x000810A8
		public void SetRemoveOnMethod(MethodBuilder mdBuilder)
		{
			if (mdBuilder == null)
			{
				throw new ArgumentNullException("mdBuilder");
			}
			this.RejectIfCreated();
			this.remove_method = mdBuilder;
		}

		// Token: 0x060024E3 RID: 9443 RVA: 0x00082EC8 File Offset: 0x000810C8
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			if (customBuilder == null)
			{
				throw new ArgumentNullException("customBuilder");
			}
			this.RejectIfCreated();
			string fullName = customBuilder.Ctor.ReflectedType.FullName;
			if (fullName == "System.Runtime.CompilerServices.SpecialNameAttribute")
			{
				this.attrs |= EventAttributes.SpecialName;
				return;
			}
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x060024E4 RID: 9444 RVA: 0x00082F74 File Offset: 0x00081174
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			if (con == null)
			{
				throw new ArgumentNullException("con");
			}
			if (binaryAttribute == null)
			{
				throw new ArgumentNullException("binaryAttribute");
			}
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x060024E5 RID: 9445 RVA: 0x00082FA8 File Offset: 0x000811A8
		private void RejectIfCreated()
		{
			if (this.typeb.is_created)
			{
				throw new InvalidOperationException("Type definition of the method is complete.");
			}
		}

		// Token: 0x04000DD6 RID: 3542
		internal string name;

		// Token: 0x04000DD7 RID: 3543
		private Type type;

		// Token: 0x04000DD8 RID: 3544
		private TypeBuilder typeb;

		// Token: 0x04000DD9 RID: 3545
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000DDA RID: 3546
		internal MethodBuilder add_method;

		// Token: 0x04000DDB RID: 3547
		internal MethodBuilder remove_method;

		// Token: 0x04000DDC RID: 3548
		internal MethodBuilder raise_method;

		// Token: 0x04000DDD RID: 3549
		internal MethodBuilder[] other_methods;

		// Token: 0x04000DDE RID: 3550
		internal EventAttributes attrs;

		// Token: 0x04000DDF RID: 3551
		private int table_idx;
	}
}
