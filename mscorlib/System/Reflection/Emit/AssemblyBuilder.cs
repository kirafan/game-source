﻿using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Resources;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using Mono.Security;

namespace System.Reflection.Emit
{
	// Token: 0x020002C4 RID: 708
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_AssemblyBuilder))]
	public sealed class AssemblyBuilder : Assembly, _AssemblyBuilder
	{
		// Token: 0x0600237F RID: 9087 RVA: 0x0007EA34 File Offset: 0x0007CC34
		internal AssemblyBuilder(AssemblyName n, string directory, AssemblyBuilderAccess access, bool corlib_internal)
		{
			this.is_compiler_context = ((access & (AssemblyBuilderAccess)2048) != (AssemblyBuilderAccess)0);
			access &= (AssemblyBuilderAccess)(-2049);
			if (!Enum.IsDefined(typeof(AssemblyBuilderAccess), access))
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Argument value {0} is not valid.", new object[]
				{
					(int)access
				}), "access");
			}
			this.name = n.Name;
			this.access = (uint)access;
			this.flags = (uint)n.Flags;
			if (this.IsSave && (directory == null || directory.Length == 0))
			{
				this.dir = Directory.GetCurrentDirectory();
			}
			else
			{
				this.dir = directory;
			}
			if (n.CultureInfo != null)
			{
				this.culture = n.CultureInfo.Name;
				this.versioninfo_culture = n.CultureInfo.Name;
			}
			Version version = n.Version;
			if (version != null)
			{
				this.version = version.ToString();
			}
			if (n.KeyPair != null)
			{
				this.sn = n.KeyPair.StrongName();
			}
			else
			{
				byte[] publicKey = n.GetPublicKey();
				if (publicKey != null && publicKey.Length > 0)
				{
					this.sn = new StrongName(publicKey);
				}
			}
			if (this.sn != null)
			{
				this.flags |= 1U;
			}
			this.corlib_internal = corlib_internal;
			if (this.sn != null)
			{
				this.pktoken = new byte[this.sn.PublicKeyToken.Length * 2];
				int num = 0;
				foreach (byte b in this.sn.PublicKeyToken)
				{
					string text = b.ToString("x2");
					this.pktoken[num++] = (byte)text[0];
					this.pktoken[num++] = (byte)text[1];
				}
			}
			AssemblyBuilder.basic_init(this);
		}

		// Token: 0x06002380 RID: 9088 RVA: 0x0007EC80 File Offset: 0x0007CE80
		void _AssemblyBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002381 RID: 9089 RVA: 0x0007EC88 File Offset: 0x0007CE88
		void _AssemblyBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002382 RID: 9090 RVA: 0x0007EC90 File Offset: 0x0007CE90
		void _AssemblyBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002383 RID: 9091 RVA: 0x0007EC98 File Offset: 0x0007CE98
		void _AssemblyBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002384 RID: 9092
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void basic_init(AssemblyBuilder ab);

		// Token: 0x17000635 RID: 1589
		// (get) Token: 0x06002385 RID: 9093 RVA: 0x0007ECA0 File Offset: 0x0007CEA0
		public override string CodeBase
		{
			get
			{
				throw this.not_supported();
			}
		}

		// Token: 0x17000636 RID: 1590
		// (get) Token: 0x06002386 RID: 9094 RVA: 0x0007ECA8 File Offset: 0x0007CEA8
		public override MethodInfo EntryPoint
		{
			get
			{
				return this.entry_point;
			}
		}

		// Token: 0x17000637 RID: 1591
		// (get) Token: 0x06002387 RID: 9095 RVA: 0x0007ECB0 File Offset: 0x0007CEB0
		public override string Location
		{
			get
			{
				throw this.not_supported();
			}
		}

		// Token: 0x17000638 RID: 1592
		// (get) Token: 0x06002388 RID: 9096 RVA: 0x0007ECB8 File Offset: 0x0007CEB8
		public override string ImageRuntimeVersion
		{
			get
			{
				return base.ImageRuntimeVersion;
			}
		}

		// Token: 0x17000639 RID: 1593
		// (get) Token: 0x06002389 RID: 9097 RVA: 0x0007ECC0 File Offset: 0x0007CEC0
		[MonoTODO]
		public override bool ReflectionOnly
		{
			get
			{
				return base.ReflectionOnly;
			}
		}

		// Token: 0x0600238A RID: 9098 RVA: 0x0007ECC8 File Offset: 0x0007CEC8
		public void AddResourceFile(string name, string fileName)
		{
			this.AddResourceFile(name, fileName, ResourceAttributes.Public);
		}

		// Token: 0x0600238B RID: 9099 RVA: 0x0007ECD4 File Offset: 0x0007CED4
		public void AddResourceFile(string name, string fileName, ResourceAttributes attribute)
		{
			this.AddResourceFile(name, fileName, attribute, true);
		}

		// Token: 0x0600238C RID: 9100 RVA: 0x0007ECE0 File Offset: 0x0007CEE0
		private void AddResourceFile(string name, string fileName, ResourceAttributes attribute, bool fileNeedsToExists)
		{
			this.check_name_and_filename(name, fileName, fileNeedsToExists);
			if (this.dir != null)
			{
				fileName = Path.Combine(this.dir, fileName);
			}
			if (this.resources != null)
			{
				MonoResource[] destinationArray = new MonoResource[this.resources.Length + 1];
				Array.Copy(this.resources, destinationArray, this.resources.Length);
				this.resources = destinationArray;
			}
			else
			{
				this.resources = new MonoResource[1];
			}
			int num = this.resources.Length - 1;
			this.resources[num].name = name;
			this.resources[num].filename = fileName;
			this.resources[num].attrs = attribute;
		}

		// Token: 0x0600238D RID: 9101 RVA: 0x0007ED98 File Offset: 0x0007CF98
		internal void AddPermissionRequests(PermissionSet required, PermissionSet optional, PermissionSet refused)
		{
			if (this.created)
			{
				throw new InvalidOperationException("Assembly was already saved.");
			}
			this._minimum = required;
			this._optional = optional;
			this._refuse = refused;
			if (required != null)
			{
				this.permissions_minimum = new RefEmitPermissionSet[1];
				this.permissions_minimum[0] = new RefEmitPermissionSet(SecurityAction.RequestMinimum, required.ToXml().ToString());
			}
			if (optional != null)
			{
				this.permissions_optional = new RefEmitPermissionSet[1];
				this.permissions_optional[0] = new RefEmitPermissionSet(SecurityAction.RequestOptional, optional.ToXml().ToString());
			}
			if (refused != null)
			{
				this.permissions_refused = new RefEmitPermissionSet[1];
				this.permissions_refused[0] = new RefEmitPermissionSet(SecurityAction.RequestRefuse, refused.ToXml().ToString());
			}
		}

		// Token: 0x0600238E RID: 9102 RVA: 0x0007EE70 File Offset: 0x0007D070
		internal void EmbedResourceFile(string name, string fileName)
		{
			this.EmbedResourceFile(name, fileName, ResourceAttributes.Public);
		}

		// Token: 0x0600238F RID: 9103 RVA: 0x0007EE7C File Offset: 0x0007D07C
		internal void EmbedResourceFile(string name, string fileName, ResourceAttributes attribute)
		{
			if (this.resources != null)
			{
				MonoResource[] destinationArray = new MonoResource[this.resources.Length + 1];
				Array.Copy(this.resources, destinationArray, this.resources.Length);
				this.resources = destinationArray;
			}
			else
			{
				this.resources = new MonoResource[1];
			}
			int num = this.resources.Length - 1;
			this.resources[num].name = name;
			this.resources[num].attrs = attribute;
			try
			{
				FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
				long length = fileStream.Length;
				this.resources[num].data = new byte[length];
				fileStream.Read(this.resources[num].data, 0, (int)length);
				fileStream.Close();
			}
			catch
			{
			}
		}

		// Token: 0x06002390 RID: 9104 RVA: 0x0007EF70 File Offset: 0x0007D170
		internal void EmbedResource(string name, byte[] blob, ResourceAttributes attribute)
		{
			if (this.resources != null)
			{
				MonoResource[] destinationArray = new MonoResource[this.resources.Length + 1];
				Array.Copy(this.resources, destinationArray, this.resources.Length);
				this.resources = destinationArray;
			}
			else
			{
				this.resources = new MonoResource[1];
			}
			int num = this.resources.Length - 1;
			this.resources[num].name = name;
			this.resources[num].attrs = attribute;
			this.resources[num].data = blob;
		}

		// Token: 0x06002391 RID: 9105 RVA: 0x0007F008 File Offset: 0x0007D208
		internal void AddTypeForwarder(Type t)
		{
			if (t == null)
			{
				throw new ArgumentNullException("t");
			}
			if (this.type_forwarders == null)
			{
				this.type_forwarders = new Type[]
				{
					t
				};
			}
			else
			{
				Type[] array = new Type[this.type_forwarders.Length + 1];
				Array.Copy(this.type_forwarders, array, this.type_forwarders.Length);
				array[this.type_forwarders.Length] = t;
				this.type_forwarders = array;
			}
		}

		// Token: 0x06002392 RID: 9106 RVA: 0x0007F07C File Offset: 0x0007D27C
		public ModuleBuilder DefineDynamicModule(string name)
		{
			return this.DefineDynamicModule(name, name, false, true);
		}

		// Token: 0x06002393 RID: 9107 RVA: 0x0007F088 File Offset: 0x0007D288
		public ModuleBuilder DefineDynamicModule(string name, bool emitSymbolInfo)
		{
			return this.DefineDynamicModule(name, name, emitSymbolInfo, true);
		}

		// Token: 0x06002394 RID: 9108 RVA: 0x0007F094 File Offset: 0x0007D294
		public ModuleBuilder DefineDynamicModule(string name, string fileName)
		{
			return this.DefineDynamicModule(name, fileName, false, false);
		}

		// Token: 0x06002395 RID: 9109 RVA: 0x0007F0A0 File Offset: 0x0007D2A0
		public ModuleBuilder DefineDynamicModule(string name, string fileName, bool emitSymbolInfo)
		{
			return this.DefineDynamicModule(name, fileName, emitSymbolInfo, false);
		}

		// Token: 0x06002396 RID: 9110 RVA: 0x0007F0AC File Offset: 0x0007D2AC
		private ModuleBuilder DefineDynamicModule(string name, string fileName, bool emitSymbolInfo, bool transient)
		{
			this.check_name_and_filename(name, fileName, false);
			if (!transient)
			{
				if (Path.GetExtension(fileName) == string.Empty)
				{
					throw new ArgumentException("Module file name '" + fileName + "' must have file extension.");
				}
				if (!this.IsSave)
				{
					throw new NotSupportedException("Persistable modules are not supported in a dynamic assembly created with AssemblyBuilderAccess.Run");
				}
				if (this.created)
				{
					throw new InvalidOperationException("Assembly was already saved.");
				}
			}
			ModuleBuilder moduleBuilder = new ModuleBuilder(this, name, fileName, emitSymbolInfo, transient);
			if (this.modules != null && this.is_module_only)
			{
				throw new InvalidOperationException("A module-only assembly can only contain one module.");
			}
			if (this.modules != null)
			{
				ModuleBuilder[] destinationArray = new ModuleBuilder[this.modules.Length + 1];
				Array.Copy(this.modules, destinationArray, this.modules.Length);
				this.modules = destinationArray;
			}
			else
			{
				this.modules = new ModuleBuilder[1];
			}
			this.modules[this.modules.Length - 1] = moduleBuilder;
			return moduleBuilder;
		}

		// Token: 0x06002397 RID: 9111
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Module InternalAddModule(string fileName);

		// Token: 0x06002398 RID: 9112 RVA: 0x0007F1A8 File Offset: 0x0007D3A8
		internal Module AddModule(string fileName)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException(fileName);
			}
			Module module = this.InternalAddModule(fileName);
			if (this.loaded_modules != null)
			{
				Module[] destinationArray = new Module[this.loaded_modules.Length + 1];
				Array.Copy(this.loaded_modules, destinationArray, this.loaded_modules.Length);
				this.loaded_modules = destinationArray;
			}
			else
			{
				this.loaded_modules = new Module[1];
			}
			this.loaded_modules[this.loaded_modules.Length - 1] = module;
			return module;
		}

		// Token: 0x06002399 RID: 9113 RVA: 0x0007F224 File Offset: 0x0007D424
		public IResourceWriter DefineResource(string name, string description, string fileName)
		{
			return this.DefineResource(name, description, fileName, ResourceAttributes.Public);
		}

		// Token: 0x0600239A RID: 9114 RVA: 0x0007F230 File Offset: 0x0007D430
		public IResourceWriter DefineResource(string name, string description, string fileName, ResourceAttributes attribute)
		{
			this.AddResourceFile(name, fileName, attribute, false);
			IResourceWriter resourceWriter = new ResourceWriter(fileName);
			if (this.resource_writers == null)
			{
				this.resource_writers = new ArrayList();
			}
			this.resource_writers.Add(resourceWriter);
			return resourceWriter;
		}

		// Token: 0x0600239B RID: 9115 RVA: 0x0007F274 File Offset: 0x0007D474
		private void AddUnmanagedResource(Win32Resource res)
		{
			MemoryStream memoryStream = new MemoryStream();
			res.WriteTo(memoryStream);
			if (this.win32_resources != null)
			{
				MonoWin32Resource[] destinationArray = new MonoWin32Resource[this.win32_resources.Length + 1];
				Array.Copy(this.win32_resources, destinationArray, this.win32_resources.Length);
				this.win32_resources = destinationArray;
			}
			else
			{
				this.win32_resources = new MonoWin32Resource[1];
			}
			this.win32_resources[this.win32_resources.Length - 1] = new MonoWin32Resource(res.Type.Id, res.Name.Id, res.Language, memoryStream.ToArray());
		}

		// Token: 0x0600239C RID: 9116 RVA: 0x0007F318 File Offset: 0x0007D518
		[MonoTODO("Not currently implemenented")]
		public void DefineUnmanagedResource(byte[] resource)
		{
			if (resource == null)
			{
				throw new ArgumentNullException("resource");
			}
			if (this.native_resource != NativeResourceType.None)
			{
				throw new ArgumentException("Native resource has already been defined.");
			}
			this.native_resource = NativeResourceType.Unmanaged;
			throw new NotImplementedException();
		}

		// Token: 0x0600239D RID: 9117 RVA: 0x0007F350 File Offset: 0x0007D550
		public void DefineUnmanagedResource(string resourceFileName)
		{
			if (resourceFileName == null)
			{
				throw new ArgumentNullException("resourceFileName");
			}
			if (resourceFileName.Length == 0)
			{
				throw new ArgumentException("resourceFileName");
			}
			if (!File.Exists(resourceFileName) || Directory.Exists(resourceFileName))
			{
				throw new FileNotFoundException("File '" + resourceFileName + "' does not exists or is a directory.");
			}
			if (this.native_resource != NativeResourceType.None)
			{
				throw new ArgumentException("Native resource has already been defined.");
			}
			this.native_resource = NativeResourceType.Unmanaged;
			using (FileStream fileStream = new FileStream(resourceFileName, FileMode.Open, FileAccess.Read))
			{
				Win32ResFileReader win32ResFileReader = new Win32ResFileReader(fileStream);
				foreach (object obj in win32ResFileReader.ReadResources())
				{
					Win32EncodedResource win32EncodedResource = (Win32EncodedResource)obj;
					if (win32EncodedResource.Name.IsName || win32EncodedResource.Type.IsName)
					{
						throw new InvalidOperationException("resource files with named resources or non-default resource types are not supported.");
					}
					this.AddUnmanagedResource(win32EncodedResource);
				}
			}
		}

		// Token: 0x0600239E RID: 9118 RVA: 0x0007F498 File Offset: 0x0007D698
		public void DefineVersionInfoResource()
		{
			if (this.native_resource != NativeResourceType.None)
			{
				throw new ArgumentException("Native resource has already been defined.");
			}
			this.native_resource = NativeResourceType.Assembly;
			this.version_res = new Win32VersionResource(1, 0, this.IsCompilerContext);
		}

		// Token: 0x0600239F RID: 9119 RVA: 0x0007F4D8 File Offset: 0x0007D6D8
		public void DefineVersionInfoResource(string product, string productVersion, string company, string copyright, string trademark)
		{
			if (this.native_resource != NativeResourceType.None)
			{
				throw new ArgumentException("Native resource has already been defined.");
			}
			this.native_resource = NativeResourceType.Explicit;
			this.version_res = new Win32VersionResource(1, 0, false);
			this.version_res.ProductName = ((product == null) ? " " : product);
			this.version_res.ProductVersion = ((productVersion == null) ? " " : productVersion);
			this.version_res.CompanyName = ((company == null) ? " " : company);
			this.version_res.LegalCopyright = ((copyright == null) ? " " : copyright);
			this.version_res.LegalTrademarks = ((trademark == null) ? " " : trademark);
		}

		// Token: 0x060023A0 RID: 9120 RVA: 0x0007F5A0 File Offset: 0x0007D7A0
		internal void DefineIconResource(string iconFileName)
		{
			if (iconFileName == null)
			{
				throw new ArgumentNullException("iconFileName");
			}
			if (iconFileName.Length == 0)
			{
				throw new ArgumentException("iconFileName");
			}
			if (!File.Exists(iconFileName) || Directory.Exists(iconFileName))
			{
				throw new FileNotFoundException("File '" + iconFileName + "' does not exists or is a directory.");
			}
			using (FileStream fileStream = new FileStream(iconFileName, FileMode.Open, FileAccess.Read))
			{
				Win32IconFileReader win32IconFileReader = new Win32IconFileReader(fileStream);
				ICONDIRENTRY[] array = win32IconFileReader.ReadIcons();
				Win32IconResource[] array2 = new Win32IconResource[array.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array2[i] = new Win32IconResource(i + 1, 0, array[i]);
					this.AddUnmanagedResource(array2[i]);
				}
				Win32GroupIconResource res = new Win32GroupIconResource(1, 0, array2);
				this.AddUnmanagedResource(res);
			}
		}

		// Token: 0x060023A1 RID: 9121 RVA: 0x0007F694 File Offset: 0x0007D894
		private void DefineVersionInfoResourceImpl(string fileName)
		{
			if (this.versioninfo_culture != null)
			{
				this.version_res.FileLanguage = new CultureInfo(this.versioninfo_culture).LCID;
			}
			this.version_res.Version = ((this.version != null) ? this.version : "0.0.0.0");
			if (this.cattrs != null)
			{
				NativeResourceType nativeResourceType = this.native_resource;
				if (nativeResourceType != NativeResourceType.Assembly)
				{
					if (nativeResourceType == NativeResourceType.Explicit)
					{
						foreach (CustomAttributeBuilder customAttributeBuilder in this.cattrs)
						{
							string fullName = customAttributeBuilder.Ctor.ReflectedType.FullName;
							if (fullName == "System.Reflection.AssemblyCultureAttribute")
							{
								if (!this.IsCompilerContext)
								{
									this.version_res.FileLanguage = new CultureInfo(customAttributeBuilder.string_arg()).LCID;
								}
							}
							else if (fullName == "System.Reflection.AssemblyDescriptionAttribute")
							{
								this.version_res.Comments = customAttributeBuilder.string_arg();
							}
						}
					}
				}
				else
				{
					foreach (CustomAttributeBuilder customAttributeBuilder2 in this.cattrs)
					{
						string fullName2 = customAttributeBuilder2.Ctor.ReflectedType.FullName;
						if (fullName2 == "System.Reflection.AssemblyProductAttribute")
						{
							this.version_res.ProductName = customAttributeBuilder2.string_arg();
						}
						else if (fullName2 == "System.Reflection.AssemblyCompanyAttribute")
						{
							this.version_res.CompanyName = customAttributeBuilder2.string_arg();
						}
						else if (fullName2 == "System.Reflection.AssemblyCopyrightAttribute")
						{
							this.version_res.LegalCopyright = customAttributeBuilder2.string_arg();
						}
						else if (fullName2 == "System.Reflection.AssemblyTrademarkAttribute")
						{
							this.version_res.LegalTrademarks = customAttributeBuilder2.string_arg();
						}
						else if (fullName2 == "System.Reflection.AssemblyCultureAttribute")
						{
							if (!this.IsCompilerContext)
							{
								this.version_res.FileLanguage = new CultureInfo(customAttributeBuilder2.string_arg()).LCID;
							}
						}
						else if (fullName2 == "System.Reflection.AssemblyFileVersionAttribute")
						{
							string text = customAttributeBuilder2.string_arg();
							if (!this.IsCompilerContext || (text != null && text.Length != 0))
							{
								this.version_res.FileVersion = text;
							}
						}
						else if (fullName2 == "System.Reflection.AssemblyInformationalVersionAttribute")
						{
							this.version_res.ProductVersion = customAttributeBuilder2.string_arg();
						}
						else if (fullName2 == "System.Reflection.AssemblyTitleAttribute")
						{
							this.version_res.FileDescription = customAttributeBuilder2.string_arg();
						}
						else if (fullName2 == "System.Reflection.AssemblyDescriptionAttribute")
						{
							this.version_res.Comments = customAttributeBuilder2.string_arg();
						}
					}
				}
			}
			this.version_res.OriginalFilename = fileName;
			if (this.IsCompilerContext)
			{
				this.version_res.InternalName = fileName;
				if (this.version_res.ProductVersion.Trim().Length == 0)
				{
					this.version_res.ProductVersion = this.version_res.FileVersion;
				}
			}
			else
			{
				this.version_res.InternalName = Path.GetFileNameWithoutExtension(fileName);
			}
			this.AddUnmanagedResource(this.version_res);
		}

		// Token: 0x060023A2 RID: 9122 RVA: 0x0007F9E8 File Offset: 0x0007DBE8
		public ModuleBuilder GetDynamicModule(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Empty name is not legal.", "name");
			}
			if (this.modules != null)
			{
				for (int i = 0; i < this.modules.Length; i++)
				{
					if (this.modules[i].name == name)
					{
						return this.modules[i];
					}
				}
			}
			return null;
		}

		// Token: 0x060023A3 RID: 9123 RVA: 0x0007FA68 File Offset: 0x0007DC68
		public override Type[] GetExportedTypes()
		{
			throw this.not_supported();
		}

		// Token: 0x060023A4 RID: 9124 RVA: 0x0007FA70 File Offset: 0x0007DC70
		public override FileStream GetFile(string name)
		{
			throw this.not_supported();
		}

		// Token: 0x060023A5 RID: 9125 RVA: 0x0007FA78 File Offset: 0x0007DC78
		public override FileStream[] GetFiles(bool getResourceModules)
		{
			throw this.not_supported();
		}

		// Token: 0x060023A6 RID: 9126 RVA: 0x0007FA80 File Offset: 0x0007DC80
		internal override Module[] GetModulesInternal()
		{
			if (this.modules == null)
			{
				return new Module[0];
			}
			return (Module[])this.modules.Clone();
		}

		// Token: 0x060023A7 RID: 9127 RVA: 0x0007FAB0 File Offset: 0x0007DCB0
		internal override Type[] GetTypes(bool exportedOnly)
		{
			Type[] array = null;
			if (this.modules != null)
			{
				for (int i = 0; i < this.modules.Length; i++)
				{
					Type[] types = this.modules[i].GetTypes();
					if (array == null)
					{
						array = types;
					}
					else
					{
						Type[] destinationArray = new Type[array.Length + types.Length];
						Array.Copy(array, 0, destinationArray, 0, array.Length);
						Array.Copy(types, 0, destinationArray, array.Length, types.Length);
					}
				}
			}
			if (this.loaded_modules != null)
			{
				for (int j = 0; j < this.loaded_modules.Length; j++)
				{
					Type[] types2 = this.loaded_modules[j].GetTypes();
					if (array == null)
					{
						array = types2;
					}
					else
					{
						Type[] destinationArray2 = new Type[array.Length + types2.Length];
						Array.Copy(array, 0, destinationArray2, 0, array.Length);
						Array.Copy(types2, 0, destinationArray2, array.Length, types2.Length);
					}
				}
			}
			return (array != null) ? array : Type.EmptyTypes;
		}

		// Token: 0x060023A8 RID: 9128 RVA: 0x0007FBAC File Offset: 0x0007DDAC
		public override ManifestResourceInfo GetManifestResourceInfo(string resourceName)
		{
			throw this.not_supported();
		}

		// Token: 0x060023A9 RID: 9129 RVA: 0x0007FBB4 File Offset: 0x0007DDB4
		public override string[] GetManifestResourceNames()
		{
			throw this.not_supported();
		}

		// Token: 0x060023AA RID: 9130 RVA: 0x0007FBBC File Offset: 0x0007DDBC
		public override Stream GetManifestResourceStream(string name)
		{
			throw this.not_supported();
		}

		// Token: 0x060023AB RID: 9131 RVA: 0x0007FBC4 File Offset: 0x0007DDC4
		public override Stream GetManifestResourceStream(Type type, string name)
		{
			throw this.not_supported();
		}

		// Token: 0x1700063A RID: 1594
		// (get) Token: 0x060023AC RID: 9132 RVA: 0x0007FBCC File Offset: 0x0007DDCC
		internal bool IsCompilerContext
		{
			get
			{
				return this.is_compiler_context;
			}
		}

		// Token: 0x1700063B RID: 1595
		// (get) Token: 0x060023AD RID: 9133 RVA: 0x0007FBD4 File Offset: 0x0007DDD4
		internal bool IsSave
		{
			get
			{
				return this.access != 1U;
			}
		}

		// Token: 0x1700063C RID: 1596
		// (get) Token: 0x060023AE RID: 9134 RVA: 0x0007FBE4 File Offset: 0x0007DDE4
		internal bool IsRun
		{
			get
			{
				return this.access == 1U || this.access == 3U;
			}
		}

		// Token: 0x1700063D RID: 1597
		// (get) Token: 0x060023AF RID: 9135 RVA: 0x0007FC00 File Offset: 0x0007DE00
		internal string AssemblyDir
		{
			get
			{
				return this.dir;
			}
		}

		// Token: 0x1700063E RID: 1598
		// (get) Token: 0x060023B0 RID: 9136 RVA: 0x0007FC08 File Offset: 0x0007DE08
		// (set) Token: 0x060023B1 RID: 9137 RVA: 0x0007FC10 File Offset: 0x0007DE10
		internal bool IsModuleOnly
		{
			get
			{
				return this.is_module_only;
			}
			set
			{
				this.is_module_only = value;
			}
		}

		// Token: 0x060023B2 RID: 9138 RVA: 0x0007FC1C File Offset: 0x0007DE1C
		internal override Module GetManifestModule()
		{
			if (this.manifest_module == null)
			{
				this.manifest_module = this.DefineDynamicModule("Default Dynamic Module");
			}
			return this.manifest_module;
		}

		// Token: 0x060023B3 RID: 9139 RVA: 0x0007FC4C File Offset: 0x0007DE4C
		[MonoLimitation("No support for PE32+ assemblies for AMD64 and IA64")]
		public void Save(string assemblyFileName, PortableExecutableKinds portableExecutableKind, ImageFileMachine imageFileMachine)
		{
			this.peKind = portableExecutableKind;
			this.machine = imageFileMachine;
			if ((this.peKind & PortableExecutableKinds.PE32Plus) != PortableExecutableKinds.NotAPortableExecutableImage || (this.peKind & PortableExecutableKinds.Unmanaged32Bit) != PortableExecutableKinds.NotAPortableExecutableImage)
			{
				throw new NotImplementedException(this.peKind.ToString());
			}
			if (this.machine == ImageFileMachine.IA64 || this.machine == ImageFileMachine.AMD64)
			{
				throw new NotImplementedException(this.machine.ToString());
			}
			if (this.resource_writers != null)
			{
				foreach (object obj in this.resource_writers)
				{
					IResourceWriter resourceWriter = (IResourceWriter)obj;
					resourceWriter.Generate();
					resourceWriter.Close();
				}
			}
			ModuleBuilder moduleBuilder = null;
			if (this.modules != null)
			{
				foreach (ModuleBuilder moduleBuilder2 in this.modules)
				{
					if (moduleBuilder2.FullyQualifiedName == assemblyFileName)
					{
						moduleBuilder = moduleBuilder2;
					}
				}
			}
			if (moduleBuilder == null)
			{
				moduleBuilder = this.DefineDynamicModule("RefEmit_OnDiskManifestModule", assemblyFileName);
			}
			if (!this.is_module_only)
			{
				moduleBuilder.IsMain = true;
			}
			if (this.entry_point != null && this.entry_point.DeclaringType.Module != moduleBuilder)
			{
				Type[] array2;
				if (this.entry_point.GetParameters().Length == 1)
				{
					array2 = new Type[]
					{
						typeof(string)
					};
				}
				else
				{
					array2 = Type.EmptyTypes;
				}
				MethodBuilder methodBuilder = moduleBuilder.DefineGlobalMethod("__EntryPoint$", MethodAttributes.Static, this.entry_point.ReturnType, array2);
				ILGenerator ilgenerator = methodBuilder.GetILGenerator();
				if (array2.Length == 1)
				{
					ilgenerator.Emit(OpCodes.Ldarg_0);
				}
				ilgenerator.Emit(OpCodes.Tailcall);
				ilgenerator.Emit(OpCodes.Call, this.entry_point);
				ilgenerator.Emit(OpCodes.Ret);
				this.entry_point = methodBuilder;
			}
			if (this.version_res != null)
			{
				this.DefineVersionInfoResourceImpl(assemblyFileName);
			}
			if (this.sn != null)
			{
				this.public_key = this.sn.PublicKey;
			}
			foreach (ModuleBuilder moduleBuilder3 in this.modules)
			{
				if (moduleBuilder3 != moduleBuilder)
				{
					moduleBuilder3.Save();
				}
			}
			moduleBuilder.Save();
			if (this.sn != null && this.sn.CanSign)
			{
				this.sn.Sign(Path.Combine(this.AssemblyDir, assemblyFileName));
			}
			this.created = true;
		}

		// Token: 0x060023B4 RID: 9140 RVA: 0x0007FF18 File Offset: 0x0007E118
		public void Save(string assemblyFileName)
		{
			this.Save(assemblyFileName, PortableExecutableKinds.ILOnly, ImageFileMachine.I386);
		}

		// Token: 0x060023B5 RID: 9141 RVA: 0x0007FF28 File Offset: 0x0007E128
		public void SetEntryPoint(MethodInfo entryMethod)
		{
			this.SetEntryPoint(entryMethod, PEFileKinds.ConsoleApplication);
		}

		// Token: 0x060023B6 RID: 9142 RVA: 0x0007FF34 File Offset: 0x0007E134
		public void SetEntryPoint(MethodInfo entryMethod, PEFileKinds fileKind)
		{
			if (entryMethod == null)
			{
				throw new ArgumentNullException("entryMethod");
			}
			if (entryMethod.DeclaringType.Assembly != this)
			{
				throw new InvalidOperationException("Entry method is not defined in the same assembly.");
			}
			this.entry_point = entryMethod;
			this.pekind = fileKind;
		}

		// Token: 0x060023B7 RID: 9143 RVA: 0x0007FF74 File Offset: 0x0007E174
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			if (customBuilder == null)
			{
				throw new ArgumentNullException("customBuilder");
			}
			if (this.IsCompilerContext)
			{
				string fullName = customBuilder.Ctor.ReflectedType.FullName;
				if (fullName == "System.Reflection.AssemblyVersionAttribute")
				{
					this.version = this.create_assembly_version(customBuilder.string_arg());
					return;
				}
				if (fullName == "System.Reflection.AssemblyCultureAttribute")
				{
					this.culture = this.GetCultureString(customBuilder.string_arg());
				}
				else if (fullName == "System.Reflection.AssemblyAlgorithmIdAttribute")
				{
					byte[] data = customBuilder.Data;
					int num = 2;
					this.algid = (uint)data[num];
					this.algid |= (uint)((uint)data[num + 1] << 8);
					this.algid |= (uint)((uint)data[num + 2] << 16);
					this.algid |= (uint)((uint)data[num + 3] << 24);
				}
				else if (fullName == "System.Reflection.AssemblyFlagsAttribute")
				{
					byte[] data = customBuilder.Data;
					int num = 2;
					this.flags |= (uint)data[num];
					this.flags |= (uint)((uint)data[num + 1] << 8);
					this.flags |= (uint)((uint)data[num + 2] << 16);
					this.flags |= (uint)((uint)data[num + 3] << 24);
					if (this.sn == null)
					{
						this.flags &= 4294967294U;
					}
				}
			}
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x060023B8 RID: 9144 RVA: 0x00080134 File Offset: 0x0007E334
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			if (con == null)
			{
				throw new ArgumentNullException("con");
			}
			if (binaryAttribute == null)
			{
				throw new ArgumentNullException("binaryAttribute");
			}
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x060023B9 RID: 9145 RVA: 0x00080168 File Offset: 0x0007E368
		internal void SetCorlibTypeBuilders(Type corlib_object_type, Type corlib_value_type, Type corlib_enum_type)
		{
			this.corlib_object_type = corlib_object_type;
			this.corlib_value_type = corlib_value_type;
			this.corlib_enum_type = corlib_enum_type;
		}

		// Token: 0x060023BA RID: 9146 RVA: 0x00080180 File Offset: 0x0007E380
		internal void SetCorlibTypeBuilders(Type corlib_object_type, Type corlib_value_type, Type corlib_enum_type, Type corlib_void_type)
		{
			this.SetCorlibTypeBuilders(corlib_object_type, corlib_value_type, corlib_enum_type);
			this.corlib_void_type = corlib_void_type;
		}

		// Token: 0x060023BB RID: 9147 RVA: 0x00080194 File Offset: 0x0007E394
		private Exception not_supported()
		{
			return new NotSupportedException("The invoked member is not supported in a dynamic module.");
		}

		// Token: 0x060023BC RID: 9148 RVA: 0x000801A0 File Offset: 0x0007E3A0
		private void check_name_and_filename(string name, string fileName, bool fileNeedsToExists)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Empty name is not legal.", "name");
			}
			if (fileName.Length == 0)
			{
				throw new ArgumentException("Empty file name is not legal.", "fileName");
			}
			if (Path.GetFileName(fileName) != fileName)
			{
				throw new ArgumentException("fileName '" + fileName + "' must not include a path.", "fileName");
			}
			string text = fileName;
			if (this.dir != null)
			{
				text = Path.Combine(this.dir, fileName);
			}
			if (fileNeedsToExists && !File.Exists(text))
			{
				throw new FileNotFoundException("Could not find file '" + fileName + "'");
			}
			if (this.resources != null)
			{
				for (int i = 0; i < this.resources.Length; i++)
				{
					if (this.resources[i].filename == text)
					{
						throw new ArgumentException("Duplicate file name '" + fileName + "'");
					}
					if (this.resources[i].name == name)
					{
						throw new ArgumentException("Duplicate name '" + name + "'");
					}
				}
			}
			if (this.modules != null)
			{
				for (int j = 0; j < this.modules.Length; j++)
				{
					if (!this.modules[j].IsTransient() && this.modules[j].FileName == fileName)
					{
						throw new ArgumentException("Duplicate file name '" + fileName + "'");
					}
					if (this.modules[j].Name == name)
					{
						throw new ArgumentException("Duplicate name '" + name + "'");
					}
				}
			}
		}

		// Token: 0x060023BD RID: 9149 RVA: 0x0008038C File Offset: 0x0007E58C
		private string create_assembly_version(string version)
		{
			string[] array = version.Split(new char[]
			{
				'.'
			});
			int[] array2 = new int[4];
			if (array.Length < 0 || array.Length > 4)
			{
				throw new ArgumentException("The version specified '" + version + "' is invalid");
			}
			for (int i = 0; i < array.Length; i++)
			{
				if (array[i] == "*")
				{
					DateTime now = DateTime.Now;
					if (i == 2)
					{
						array2[2] = (now - new DateTime(2000, 1, 1)).Days;
						if (array.Length == 3)
						{
							array2[3] = (now.Second + now.Minute * 60 + now.Hour * 3600) / 2;
						}
					}
					else
					{
						if (i != 3)
						{
							throw new ArgumentException("The version specified '" + version + "' is invalid");
						}
						array2[3] = (now.Second + now.Minute * 60 + now.Hour * 3600) / 2;
					}
				}
				else
				{
					try
					{
						array2[i] = int.Parse(array[i]);
					}
					catch (FormatException)
					{
						throw new ArgumentException("The version specified '" + version + "' is invalid");
					}
				}
			}
			return string.Concat(new object[]
			{
				array2[0],
				".",
				array2[1],
				".",
				array2[2],
				".",
				array2[3]
			});
		}

		// Token: 0x060023BE RID: 9150 RVA: 0x00080544 File Offset: 0x0007E744
		private string GetCultureString(string str)
		{
			return (!(str == "neutral")) ? str : string.Empty;
		}

		// Token: 0x060023BF RID: 9151 RVA: 0x00080564 File Offset: 0x0007E764
		internal override AssemblyName UnprotectedGetName()
		{
			AssemblyName assemblyName = base.UnprotectedGetName();
			if (this.sn != null)
			{
				assemblyName.SetPublicKey(this.sn.PublicKey);
				assemblyName.SetPublicKeyToken(this.sn.PublicKeyToken);
			}
			return assemblyName;
		}

		// Token: 0x04000D77 RID: 3447
		private const AssemblyBuilderAccess COMPILER_ACCESS = (AssemblyBuilderAccess)2048;

		// Token: 0x04000D78 RID: 3448
		private UIntPtr dynamic_assembly;

		// Token: 0x04000D79 RID: 3449
		private MethodInfo entry_point;

		// Token: 0x04000D7A RID: 3450
		private ModuleBuilder[] modules;

		// Token: 0x04000D7B RID: 3451
		private string name;

		// Token: 0x04000D7C RID: 3452
		private string dir;

		// Token: 0x04000D7D RID: 3453
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000D7E RID: 3454
		private MonoResource[] resources;

		// Token: 0x04000D7F RID: 3455
		private byte[] public_key;

		// Token: 0x04000D80 RID: 3456
		private string version;

		// Token: 0x04000D81 RID: 3457
		private string culture;

		// Token: 0x04000D82 RID: 3458
		private uint algid;

		// Token: 0x04000D83 RID: 3459
		private uint flags;

		// Token: 0x04000D84 RID: 3460
		private PEFileKinds pekind = PEFileKinds.Dll;

		// Token: 0x04000D85 RID: 3461
		private bool delay_sign;

		// Token: 0x04000D86 RID: 3462
		private uint access;

		// Token: 0x04000D87 RID: 3463
		private Module[] loaded_modules;

		// Token: 0x04000D88 RID: 3464
		private MonoWin32Resource[] win32_resources;

		// Token: 0x04000D89 RID: 3465
		private RefEmitPermissionSet[] permissions_minimum;

		// Token: 0x04000D8A RID: 3466
		private RefEmitPermissionSet[] permissions_optional;

		// Token: 0x04000D8B RID: 3467
		private RefEmitPermissionSet[] permissions_refused;

		// Token: 0x04000D8C RID: 3468
		private PortableExecutableKinds peKind;

		// Token: 0x04000D8D RID: 3469
		private ImageFileMachine machine;

		// Token: 0x04000D8E RID: 3470
		private bool corlib_internal;

		// Token: 0x04000D8F RID: 3471
		private Type[] type_forwarders;

		// Token: 0x04000D90 RID: 3472
		private byte[] pktoken;

		// Token: 0x04000D91 RID: 3473
		internal Type corlib_object_type = typeof(object);

		// Token: 0x04000D92 RID: 3474
		internal Type corlib_value_type = typeof(ValueType);

		// Token: 0x04000D93 RID: 3475
		internal Type corlib_enum_type = typeof(Enum);

		// Token: 0x04000D94 RID: 3476
		internal Type corlib_void_type = typeof(void);

		// Token: 0x04000D95 RID: 3477
		private ArrayList resource_writers;

		// Token: 0x04000D96 RID: 3478
		private Win32VersionResource version_res;

		// Token: 0x04000D97 RID: 3479
		private bool created;

		// Token: 0x04000D98 RID: 3480
		private bool is_module_only;

		// Token: 0x04000D99 RID: 3481
		private StrongName sn;

		// Token: 0x04000D9A RID: 3482
		private NativeResourceType native_resource;

		// Token: 0x04000D9B RID: 3483
		private readonly bool is_compiler_context;

		// Token: 0x04000D9C RID: 3484
		private string versioninfo_culture;

		// Token: 0x04000D9D RID: 3485
		private ModuleBuilder manifest_module;
	}
}
