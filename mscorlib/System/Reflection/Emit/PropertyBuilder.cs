﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002F7 RID: 759
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_PropertyBuilder))]
	public sealed class PropertyBuilder : PropertyInfo, _PropertyBuilder
	{
		// Token: 0x060026D1 RID: 9937 RVA: 0x0008A834 File Offset: 0x00088A34
		internal PropertyBuilder(TypeBuilder tb, string name, PropertyAttributes attributes, Type returnType, Type[] returnModReq, Type[] returnModOpt, Type[] parameterTypes, Type[][] paramModReq, Type[][] paramModOpt)
		{
			this.name = name;
			this.attrs = attributes;
			this.type = returnType;
			this.returnModReq = returnModReq;
			this.returnModOpt = returnModOpt;
			this.paramModReq = paramModReq;
			this.paramModOpt = paramModOpt;
			if (parameterTypes != null)
			{
				this.parameters = new Type[parameterTypes.Length];
				Array.Copy(parameterTypes, this.parameters, this.parameters.Length);
			}
			this.typeb = tb;
			this.table_idx = tb.get_next_table_index(this, 23, true);
		}

		// Token: 0x060026D2 RID: 9938 RVA: 0x0008A8C0 File Offset: 0x00088AC0
		void _PropertyBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060026D3 RID: 9939 RVA: 0x0008A8C8 File Offset: 0x00088AC8
		void _PropertyBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060026D4 RID: 9940 RVA: 0x0008A8D0 File Offset: 0x00088AD0
		void _PropertyBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060026D5 RID: 9941 RVA: 0x0008A8D8 File Offset: 0x00088AD8
		void _PropertyBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x170006ED RID: 1773
		// (get) Token: 0x060026D6 RID: 9942 RVA: 0x0008A8E0 File Offset: 0x00088AE0
		public override PropertyAttributes Attributes
		{
			get
			{
				return this.attrs;
			}
		}

		// Token: 0x170006EE RID: 1774
		// (get) Token: 0x060026D7 RID: 9943 RVA: 0x0008A8E8 File Offset: 0x00088AE8
		public override bool CanRead
		{
			get
			{
				return this.get_method != null;
			}
		}

		// Token: 0x170006EF RID: 1775
		// (get) Token: 0x060026D8 RID: 9944 RVA: 0x0008A8F8 File Offset: 0x00088AF8
		public override bool CanWrite
		{
			get
			{
				return this.set_method != null;
			}
		}

		// Token: 0x170006F0 RID: 1776
		// (get) Token: 0x060026D9 RID: 9945 RVA: 0x0008A908 File Offset: 0x00088B08
		public override Type DeclaringType
		{
			get
			{
				return this.typeb;
			}
		}

		// Token: 0x170006F1 RID: 1777
		// (get) Token: 0x060026DA RID: 9946 RVA: 0x0008A910 File Offset: 0x00088B10
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170006F2 RID: 1778
		// (get) Token: 0x060026DB RID: 9947 RVA: 0x0008A918 File Offset: 0x00088B18
		public PropertyToken PropertyToken
		{
			get
			{
				return default(PropertyToken);
			}
		}

		// Token: 0x170006F3 RID: 1779
		// (get) Token: 0x060026DC RID: 9948 RVA: 0x0008A930 File Offset: 0x00088B30
		public override Type PropertyType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x170006F4 RID: 1780
		// (get) Token: 0x060026DD RID: 9949 RVA: 0x0008A938 File Offset: 0x00088B38
		public override Type ReflectedType
		{
			get
			{
				return this.typeb;
			}
		}

		// Token: 0x060026DE RID: 9950 RVA: 0x0008A940 File Offset: 0x00088B40
		public void AddOtherMethod(MethodBuilder mdBuilder)
		{
		}

		// Token: 0x060026DF RID: 9951 RVA: 0x0008A944 File Offset: 0x00088B44
		public override MethodInfo[] GetAccessors(bool nonPublic)
		{
			return null;
		}

		// Token: 0x060026E0 RID: 9952 RVA: 0x0008A948 File Offset: 0x00088B48
		public override object[] GetCustomAttributes(bool inherit)
		{
			throw this.not_supported();
		}

		// Token: 0x060026E1 RID: 9953 RVA: 0x0008A950 File Offset: 0x00088B50
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			throw this.not_supported();
		}

		// Token: 0x060026E2 RID: 9954 RVA: 0x0008A958 File Offset: 0x00088B58
		public override MethodInfo GetGetMethod(bool nonPublic)
		{
			return this.get_method;
		}

		// Token: 0x060026E3 RID: 9955 RVA: 0x0008A960 File Offset: 0x00088B60
		public override ParameterInfo[] GetIndexParameters()
		{
			throw this.not_supported();
		}

		// Token: 0x060026E4 RID: 9956 RVA: 0x0008A968 File Offset: 0x00088B68
		public override MethodInfo GetSetMethod(bool nonPublic)
		{
			return this.set_method;
		}

		// Token: 0x060026E5 RID: 9957 RVA: 0x0008A970 File Offset: 0x00088B70
		public override object GetValue(object obj, object[] index)
		{
			return null;
		}

		// Token: 0x060026E6 RID: 9958 RVA: 0x0008A974 File Offset: 0x00088B74
		public override object GetValue(object obj, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
		{
			throw this.not_supported();
		}

		// Token: 0x060026E7 RID: 9959 RVA: 0x0008A97C File Offset: 0x00088B7C
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			throw this.not_supported();
		}

		// Token: 0x060026E8 RID: 9960 RVA: 0x0008A984 File Offset: 0x00088B84
		public void SetConstant(object defaultValue)
		{
			this.def_value = defaultValue;
		}

		// Token: 0x060026E9 RID: 9961 RVA: 0x0008A990 File Offset: 0x00088B90
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			string fullName = customBuilder.Ctor.ReflectedType.FullName;
			if (fullName == "System.Runtime.CompilerServices.SpecialNameAttribute")
			{
				this.attrs |= PropertyAttributes.SpecialName;
				return;
			}
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x060026EA RID: 9962 RVA: 0x0008AA28 File Offset: 0x00088C28
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x060026EB RID: 9963 RVA: 0x0008AA38 File Offset: 0x00088C38
		public void SetGetMethod(MethodBuilder mdBuilder)
		{
			this.get_method = mdBuilder;
		}

		// Token: 0x060026EC RID: 9964 RVA: 0x0008AA44 File Offset: 0x00088C44
		public void SetSetMethod(MethodBuilder mdBuilder)
		{
			this.set_method = mdBuilder;
		}

		// Token: 0x060026ED RID: 9965 RVA: 0x0008AA50 File Offset: 0x00088C50
		public override void SetValue(object obj, object value, object[] index)
		{
		}

		// Token: 0x060026EE RID: 9966 RVA: 0x0008AA54 File Offset: 0x00088C54
		public override void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, object[] index, CultureInfo culture)
		{
		}

		// Token: 0x170006F5 RID: 1781
		// (get) Token: 0x060026EF RID: 9967 RVA: 0x0008AA58 File Offset: 0x00088C58
		public override Module Module
		{
			get
			{
				return base.Module;
			}
		}

		// Token: 0x060026F0 RID: 9968 RVA: 0x0008AA60 File Offset: 0x00088C60
		private Exception not_supported()
		{
			return new NotSupportedException("The invoked member is not supported in a dynamic module.");
		}

		// Token: 0x04000F9E RID: 3998
		private PropertyAttributes attrs;

		// Token: 0x04000F9F RID: 3999
		private string name;

		// Token: 0x04000FA0 RID: 4000
		private Type type;

		// Token: 0x04000FA1 RID: 4001
		private Type[] parameters;

		// Token: 0x04000FA2 RID: 4002
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000FA3 RID: 4003
		private object def_value;

		// Token: 0x04000FA4 RID: 4004
		private MethodBuilder set_method;

		// Token: 0x04000FA5 RID: 4005
		private MethodBuilder get_method;

		// Token: 0x04000FA6 RID: 4006
		private int table_idx;

		// Token: 0x04000FA7 RID: 4007
		internal TypeBuilder typeb;

		// Token: 0x04000FA8 RID: 4008
		private Type[] returnModReq;

		// Token: 0x04000FA9 RID: 4009
		private Type[] returnModOpt;

		// Token: 0x04000FAA RID: 4010
		private Type[][] paramModReq;

		// Token: 0x04000FAB RID: 4011
		private Type[][] paramModOpt;
	}
}
