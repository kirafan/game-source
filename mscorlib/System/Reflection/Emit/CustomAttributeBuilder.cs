﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Reflection.Emit
{
	// Token: 0x020002C8 RID: 712
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_CustomAttributeBuilder))]
	[ComVisible(true)]
	public class CustomAttributeBuilder : _CustomAttributeBuilder
	{
		// Token: 0x06002401 RID: 9217 RVA: 0x00080F10 File Offset: 0x0007F110
		internal CustomAttributeBuilder(ConstructorInfo con, byte[] cdata)
		{
			this.ctor = con;
			this.data = (byte[])cdata.Clone();
		}

		// Token: 0x06002402 RID: 9218 RVA: 0x00080F30 File Offset: 0x0007F130
		public CustomAttributeBuilder(ConstructorInfo con, object[] constructorArgs)
		{
			this.Initialize(con, constructorArgs, new PropertyInfo[0], new object[0], new FieldInfo[0], new object[0]);
		}

		// Token: 0x06002403 RID: 9219 RVA: 0x00080F64 File Offset: 0x0007F164
		public CustomAttributeBuilder(ConstructorInfo con, object[] constructorArgs, FieldInfo[] namedFields, object[] fieldValues)
		{
			this.Initialize(con, constructorArgs, new PropertyInfo[0], new object[0], namedFields, fieldValues);
		}

		// Token: 0x06002404 RID: 9220 RVA: 0x00080F90 File Offset: 0x0007F190
		public CustomAttributeBuilder(ConstructorInfo con, object[] constructorArgs, PropertyInfo[] namedProperties, object[] propertyValues)
		{
			this.Initialize(con, constructorArgs, namedProperties, propertyValues, new FieldInfo[0], new object[0]);
		}

		// Token: 0x06002405 RID: 9221 RVA: 0x00080FBC File Offset: 0x0007F1BC
		public CustomAttributeBuilder(ConstructorInfo con, object[] constructorArgs, PropertyInfo[] namedProperties, object[] propertyValues, FieldInfo[] namedFields, object[] fieldValues)
		{
			this.Initialize(con, constructorArgs, namedProperties, propertyValues, namedFields, fieldValues);
		}

		// Token: 0x06002406 RID: 9222 RVA: 0x00080FE0 File Offset: 0x0007F1E0
		void _CustomAttributeBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002407 RID: 9223 RVA: 0x00080FE8 File Offset: 0x0007F1E8
		void _CustomAttributeBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002408 RID: 9224 RVA: 0x00080FF0 File Offset: 0x0007F1F0
		void _CustomAttributeBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002409 RID: 9225 RVA: 0x00080FF8 File Offset: 0x0007F1F8
		void _CustomAttributeBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000655 RID: 1621
		// (get) Token: 0x0600240A RID: 9226 RVA: 0x00081000 File Offset: 0x0007F200
		internal ConstructorInfo Ctor
		{
			get
			{
				return this.ctor;
			}
		}

		// Token: 0x17000656 RID: 1622
		// (get) Token: 0x0600240B RID: 9227 RVA: 0x00081008 File Offset: 0x0007F208
		internal byte[] Data
		{
			get
			{
				return this.data;
			}
		}

		// Token: 0x0600240C RID: 9228
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern byte[] GetBlob(Assembly asmb, ConstructorInfo con, object[] constructorArgs, PropertyInfo[] namedProperties, object[] propertyValues, FieldInfo[] namedFields, object[] fieldValues);

		// Token: 0x0600240D RID: 9229 RVA: 0x00081010 File Offset: 0x0007F210
		private bool IsValidType(Type t)
		{
			if (t.IsArray && t.GetArrayRank() > 1)
			{
				return false;
			}
			if (t is TypeBuilder && t.IsEnum)
			{
				Enum.GetUnderlyingType(t);
			}
			return true;
		}

		// Token: 0x0600240E RID: 9230 RVA: 0x00081054 File Offset: 0x0007F254
		private void Initialize(ConstructorInfo con, object[] constructorArgs, PropertyInfo[] namedProperties, object[] propertyValues, FieldInfo[] namedFields, object[] fieldValues)
		{
			this.ctor = con;
			if (con == null)
			{
				throw new ArgumentNullException("con");
			}
			if (constructorArgs == null)
			{
				throw new ArgumentNullException("constructorArgs");
			}
			if (namedProperties == null)
			{
				throw new ArgumentNullException("namedProperties");
			}
			if (propertyValues == null)
			{
				throw new ArgumentNullException("propertyValues");
			}
			if (namedFields == null)
			{
				throw new ArgumentNullException("namedFields");
			}
			if (fieldValues == null)
			{
				throw new ArgumentNullException("fieldValues");
			}
			if (con.GetParameterCount() != constructorArgs.Length)
			{
				throw new ArgumentException("Parameter count does not match passed in argument value count.");
			}
			if (namedProperties.Length != propertyValues.Length)
			{
				throw new ArgumentException("Array lengths must be the same.", "namedProperties, propertyValues");
			}
			if (namedFields.Length != fieldValues.Length)
			{
				throw new ArgumentException("Array lengths must be the same.", "namedFields, fieldValues");
			}
			if ((con.Attributes & MethodAttributes.Static) == MethodAttributes.Static || (con.Attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.Private)
			{
				throw new ArgumentException("Cannot have private or static constructor.");
			}
			Type declaringType = this.ctor.DeclaringType;
			int num = 0;
			foreach (FieldInfo fieldInfo in namedFields)
			{
				Type declaringType2 = fieldInfo.DeclaringType;
				if (!this.IsValidType(declaringType2))
				{
					throw new ArgumentException("Field '" + fieldInfo.Name + "' does not have a valid type.");
				}
				if (declaringType != declaringType2 && !declaringType2.IsSubclassOf(declaringType) && !declaringType.IsSubclassOf(declaringType2))
				{
					throw new ArgumentException("Field '" + fieldInfo.Name + "' does not belong to the same class as the constructor");
				}
				if (fieldValues[num] != null && !(fieldInfo.FieldType is TypeBuilder) && !fieldInfo.FieldType.IsEnum && !fieldInfo.FieldType.IsInstanceOfType(fieldValues[num]) && !fieldInfo.FieldType.IsArray)
				{
					throw new ArgumentException(string.Concat(new object[]
					{
						"Value of field '",
						fieldInfo.Name,
						"' does not match field type: ",
						fieldInfo.FieldType
					}));
				}
				num++;
			}
			num = 0;
			foreach (PropertyInfo propertyInfo in namedProperties)
			{
				if (!propertyInfo.CanWrite)
				{
					throw new ArgumentException("Property '" + propertyInfo.Name + "' does not have a setter.");
				}
				Type declaringType3 = propertyInfo.DeclaringType;
				if (!this.IsValidType(declaringType3))
				{
					throw new ArgumentException("Property '" + propertyInfo.Name + "' does not have a valid type.");
				}
				if (declaringType != declaringType3 && !declaringType3.IsSubclassOf(declaringType) && !declaringType.IsSubclassOf(declaringType3))
				{
					throw new ArgumentException("Property '" + propertyInfo.Name + "' does not belong to the same class as the constructor");
				}
				if (propertyValues[num] != null && !(propertyInfo.PropertyType is TypeBuilder) && !propertyInfo.PropertyType.IsEnum && !propertyInfo.PropertyType.IsInstanceOfType(propertyValues[num]) && !propertyInfo.PropertyType.IsArray)
				{
					throw new ArgumentException(string.Concat(new object[]
					{
						"Value of property '",
						propertyInfo.Name,
						"' does not match property type: ",
						propertyInfo.PropertyType,
						" -> ",
						propertyValues[num]
					}));
				}
				num++;
			}
			num = 0;
			foreach (ParameterInfo parameterInfo in CustomAttributeBuilder.GetParameters(con))
			{
				if (parameterInfo != null)
				{
					Type parameterType = parameterInfo.ParameterType;
					if (!this.IsValidType(parameterType))
					{
						throw new ArgumentException("Argument " + num + " does not have a valid type.");
					}
					if (constructorArgs[num] != null && !(parameterType is TypeBuilder) && !parameterType.IsEnum && !parameterType.IsInstanceOfType(constructorArgs[num]) && !parameterType.IsArray)
					{
						throw new ArgumentException(string.Concat(new object[]
						{
							"Value of argument ",
							num,
							" does not match parameter type: ",
							parameterType,
							" -> ",
							constructorArgs[num]
						}));
					}
				}
				num++;
			}
			this.data = CustomAttributeBuilder.GetBlob(declaringType.Assembly, con, constructorArgs, namedProperties, propertyValues, namedFields, fieldValues);
		}

		// Token: 0x0600240F RID: 9231 RVA: 0x000814BC File Offset: 0x0007F6BC
		internal static int decode_len(byte[] data, int pos, out int rpos)
		{
			int result;
			if ((data[pos] & 128) == 0)
			{
				result = (int)(data[pos++] & 127);
			}
			else if ((data[pos] & 64) == 0)
			{
				result = ((int)(data[pos] & 63) << 8) + (int)data[pos + 1];
				pos += 2;
			}
			else
			{
				result = ((int)(data[pos] & 31) << 24) + ((int)data[pos + 1] << 16) + ((int)data[pos + 2] << 8) + (int)data[pos + 3];
				pos += 4;
			}
			rpos = pos;
			return result;
		}

		// Token: 0x06002410 RID: 9232 RVA: 0x00081538 File Offset: 0x0007F738
		internal static string string_from_bytes(byte[] data, int pos, int len)
		{
			return Encoding.UTF8.GetString(data, pos, len);
		}

		// Token: 0x06002411 RID: 9233 RVA: 0x00081548 File Offset: 0x0007F748
		internal string string_arg()
		{
			int pos = 2;
			int len = CustomAttributeBuilder.decode_len(this.data, pos, out pos);
			return CustomAttributeBuilder.string_from_bytes(this.data, pos, len);
		}

		// Token: 0x06002412 RID: 9234 RVA: 0x00081574 File Offset: 0x0007F774
		internal static UnmanagedMarshal get_umarshal(CustomAttributeBuilder customBuilder, bool is_field)
		{
			byte[] array = customBuilder.Data;
			UnmanagedType elemType = (UnmanagedType)80;
			int num = -1;
			int sizeParamIndex = -1;
			bool flag = false;
			string text = null;
			Type typeref = null;
			string cookie = string.Empty;
			int num2 = (int)array[2];
			num2 |= (int)array[3] << 8;
			string fullName = CustomAttributeBuilder.GetParameters(customBuilder.Ctor)[0].ParameterType.FullName;
			int num3 = 6;
			if (fullName == "System.Int16")
			{
				num3 = 4;
			}
			int num4 = (int)array[num3++];
			num4 |= (int)array[num3++] << 8;
			int i = 0;
			while (i < num4)
			{
				num3++;
				int num5 = (int)array[num3++];
				if (num5 == 85)
				{
					int num6 = CustomAttributeBuilder.decode_len(array, num3, out num3);
					CustomAttributeBuilder.string_from_bytes(array, num3, num6);
					num3 += num6;
				}
				int num7 = CustomAttributeBuilder.decode_len(array, num3, out num3);
				string text2 = CustomAttributeBuilder.string_from_bytes(array, num3, num7);
				num3 += num7;
				string text3 = text2;
				if (text3 != null)
				{
					if (CustomAttributeBuilder.<>f__switch$map1E == null)
					{
						CustomAttributeBuilder.<>f__switch$map1E = new Dictionary<string, int>(9)
						{
							{
								"ArraySubType",
								0
							},
							{
								"SizeConst",
								1
							},
							{
								"SafeArraySubType",
								2
							},
							{
								"IidParameterIndex",
								3
							},
							{
								"SafeArrayUserDefinedSubType",
								4
							},
							{
								"SizeParamIndex",
								5
							},
							{
								"MarshalType",
								6
							},
							{
								"MarshalTypeRef",
								7
							},
							{
								"MarshalCookie",
								8
							}
						};
					}
					int num8;
					if (CustomAttributeBuilder.<>f__switch$map1E.TryGetValue(text3, out num8))
					{
						switch (num8)
						{
						case 0:
						{
							int num9 = (int)array[num3++];
							num9 |= (int)array[num3++] << 8;
							num9 |= (int)array[num3++] << 16;
							num9 |= (int)array[num3++] << 24;
							elemType = (UnmanagedType)num9;
							break;
						}
						case 1:
						{
							int num9 = (int)array[num3++];
							num9 |= (int)array[num3++] << 8;
							num9 |= (int)array[num3++] << 16;
							num9 |= (int)array[num3++] << 24;
							num = num9;
							flag = true;
							break;
						}
						case 2:
						{
							int num9 = (int)array[num3++];
							num9 |= (int)array[num3++] << 8;
							num9 |= (int)array[num3++] << 16;
							num9 |= (int)array[num3++] << 24;
							elemType = (UnmanagedType)num9;
							break;
						}
						case 3:
							num3 += 4;
							break;
						case 4:
							num7 = CustomAttributeBuilder.decode_len(array, num3, out num3);
							CustomAttributeBuilder.string_from_bytes(array, num3, num7);
							num3 += num7;
							break;
						case 5:
						{
							int num9 = (int)array[num3++];
							num9 |= (int)array[num3++] << 8;
							sizeParamIndex = num9;
							flag = true;
							break;
						}
						case 6:
							num7 = CustomAttributeBuilder.decode_len(array, num3, out num3);
							text = CustomAttributeBuilder.string_from_bytes(array, num3, num7);
							num3 += num7;
							break;
						case 7:
							num7 = CustomAttributeBuilder.decode_len(array, num3, out num3);
							text = CustomAttributeBuilder.string_from_bytes(array, num3, num7);
							typeref = Type.GetType(text);
							num3 += num7;
							break;
						case 8:
							num7 = CustomAttributeBuilder.decode_len(array, num3, out num3);
							cookie = CustomAttributeBuilder.string_from_bytes(array, num3, num7);
							num3 += num7;
							break;
						default:
							goto IL_34F;
						}
						i++;
						continue;
					}
				}
				IL_34F:
				throw new Exception("Unknown MarshalAsAttribute field: " + text2);
			}
			UnmanagedType unmanagedType = (UnmanagedType)num2;
			switch (unmanagedType)
			{
			case UnmanagedType.LPArray:
				if (flag)
				{
					return UnmanagedMarshal.DefineLPArrayInternal(elemType, num, sizeParamIndex);
				}
				return UnmanagedMarshal.DefineLPArray(elemType);
			default:
				if (unmanagedType == UnmanagedType.SafeArray)
				{
					return UnmanagedMarshal.DefineSafeArray(elemType);
				}
				if (unmanagedType != UnmanagedType.ByValArray)
				{
					if (unmanagedType != UnmanagedType.ByValTStr)
					{
						return UnmanagedMarshal.DefineUnmanagedMarshal((UnmanagedType)num2);
					}
					return UnmanagedMarshal.DefineByValTStr(num);
				}
				else
				{
					if (!is_field)
					{
						throw new ArgumentException("Specified unmanaged type is only valid on fields");
					}
					return UnmanagedMarshal.DefineByValArray(num);
				}
				break;
			case UnmanagedType.CustomMarshaler:
				return UnmanagedMarshal.DefineCustom(typeref, cookie, text, Guid.Empty);
			}
		}

		// Token: 0x06002413 RID: 9235 RVA: 0x00081980 File Offset: 0x0007FB80
		private static Type elementTypeToType(int elementType)
		{
			switch (elementType)
			{
			case 2:
				return typeof(bool);
			case 3:
				return typeof(char);
			case 4:
				return typeof(sbyte);
			case 5:
				return typeof(byte);
			case 6:
				return typeof(short);
			case 7:
				return typeof(ushort);
			case 8:
				return typeof(int);
			case 9:
				return typeof(uint);
			case 10:
				return typeof(long);
			case 11:
				return typeof(ulong);
			case 12:
				return typeof(float);
			case 13:
				return typeof(double);
			case 14:
				return typeof(string);
			default:
				throw new Exception("Unknown element type '" + elementType + "'");
			}
		}

		// Token: 0x06002414 RID: 9236 RVA: 0x00081A7C File Offset: 0x0007FC7C
		private static object decode_cattr_value(Type t, byte[] data, int pos, out int rpos)
		{
			TypeCode typeCode = Type.GetTypeCode(t);
			switch (typeCode)
			{
			case TypeCode.Object:
			{
				int num = (int)data[pos];
				pos++;
				if (num >= 2 && num <= 14)
				{
					return CustomAttributeBuilder.decode_cattr_value(CustomAttributeBuilder.elementTypeToType(num), data, pos, out rpos);
				}
				throw new Exception("Subtype '" + num + "' of type object not yet handled in decode_cattr_value");
			}
			default:
			{
				if (typeCode == TypeCode.Int32)
				{
					rpos = pos + 4;
					return (int)data[pos] + ((int)data[pos + 1] << 8) + ((int)data[pos + 2] << 16) + ((int)data[pos + 3] << 24);
				}
				if (typeCode != TypeCode.String)
				{
					throw new Exception("FIXME: Type " + t + " not yet handled in decode_cattr_value.");
				}
				if (data[pos] == 255)
				{
					rpos = pos + 1;
					return null;
				}
				int num2 = CustomAttributeBuilder.decode_len(data, pos, out pos);
				rpos = pos + num2;
				return CustomAttributeBuilder.string_from_bytes(data, pos, num2);
			}
			case TypeCode.Boolean:
				rpos = pos + 1;
				return data[pos] != 0;
			}
		}

		// Token: 0x06002415 RID: 9237 RVA: 0x00081B80 File Offset: 0x0007FD80
		internal static CustomAttributeBuilder.CustomAttributeInfo decode_cattr(CustomAttributeBuilder customBuilder)
		{
			byte[] array = customBuilder.Data;
			ConstructorInfo constructorInfo = customBuilder.Ctor;
			int num = 0;
			CustomAttributeBuilder.CustomAttributeInfo result = default(CustomAttributeBuilder.CustomAttributeInfo);
			if (array.Length < 2)
			{
				throw new Exception("Custom attr length is only '" + array.Length + "'");
			}
			if (array[0] != 1 || array[1] != 0)
			{
				throw new Exception("Prolog invalid");
			}
			num = 2;
			ParameterInfo[] parameters = CustomAttributeBuilder.GetParameters(constructorInfo);
			result.ctor = constructorInfo;
			result.ctorArgs = new object[parameters.Length];
			for (int i = 0; i < parameters.Length; i++)
			{
				result.ctorArgs[i] = CustomAttributeBuilder.decode_cattr_value(parameters[i].ParameterType, array, num, out num);
			}
			int num2 = (int)array[num] + (int)array[num + 1] * 256;
			num += 2;
			result.namedParamNames = new string[num2];
			result.namedParamValues = new object[num2];
			for (int j = 0; j < num2; j++)
			{
				int num3 = (int)array[num++];
				int num4 = (int)array[num++];
				string text = null;
				if (num4 == 85)
				{
					int num5 = CustomAttributeBuilder.decode_len(array, num, out num);
					text = CustomAttributeBuilder.string_from_bytes(array, num, num5);
					num += num5;
				}
				int num6 = CustomAttributeBuilder.decode_len(array, num, out num);
				string text2 = CustomAttributeBuilder.string_from_bytes(array, num, num6);
				result.namedParamNames[j] = text2;
				num += num6;
				if (num3 != 83)
				{
					throw new Exception("Unknown named type: " + num3);
				}
				FieldInfo field = constructorInfo.DeclaringType.GetField(text2, BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
				if (field == null)
				{
					throw new Exception(string.Concat(new object[]
					{
						"Custom attribute type '",
						constructorInfo.DeclaringType,
						"' doesn't contain a field named '",
						text2,
						"'"
					}));
				}
				object obj = CustomAttributeBuilder.decode_cattr_value(field.FieldType, array, num, out num);
				if (text != null)
				{
					Type type = Type.GetType(text);
					obj = Enum.ToObject(type, obj);
				}
				result.namedParamValues[j] = obj;
			}
			return result;
		}

		// Token: 0x06002416 RID: 9238 RVA: 0x00081D98 File Offset: 0x0007FF98
		private static ParameterInfo[] GetParameters(ConstructorInfo ctor)
		{
			ConstructorBuilder constructorBuilder = ctor as ConstructorBuilder;
			if (constructorBuilder != null)
			{
				return constructorBuilder.GetParametersInternal();
			}
			return ctor.GetParameters();
		}

		// Token: 0x04000DB3 RID: 3507
		private ConstructorInfo ctor;

		// Token: 0x04000DB4 RID: 3508
		private byte[] data;

		// Token: 0x020002C9 RID: 713
		internal struct CustomAttributeInfo
		{
			// Token: 0x04000DB6 RID: 3510
			public ConstructorInfo ctor;

			// Token: 0x04000DB7 RID: 3511
			public object[] ctorArgs;

			// Token: 0x04000DB8 RID: 3512
			public string[] namedParamNames;

			// Token: 0x04000DB9 RID: 3513
			public object[] namedParamValues;
		}
	}
}
