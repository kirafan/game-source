﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002FD RID: 765
	[ComVisible(true)]
	[Serializable]
	public enum StackBehaviour
	{
		// Token: 0x04000FC0 RID: 4032
		Pop0,
		// Token: 0x04000FC1 RID: 4033
		Pop1,
		// Token: 0x04000FC2 RID: 4034
		Pop1_pop1,
		// Token: 0x04000FC3 RID: 4035
		Popi,
		// Token: 0x04000FC4 RID: 4036
		Popi_pop1,
		// Token: 0x04000FC5 RID: 4037
		Popi_popi,
		// Token: 0x04000FC6 RID: 4038
		Popi_popi8,
		// Token: 0x04000FC7 RID: 4039
		Popi_popi_popi,
		// Token: 0x04000FC8 RID: 4040
		Popi_popr4,
		// Token: 0x04000FC9 RID: 4041
		Popi_popr8,
		// Token: 0x04000FCA RID: 4042
		Popref,
		// Token: 0x04000FCB RID: 4043
		Popref_pop1,
		// Token: 0x04000FCC RID: 4044
		Popref_popi,
		// Token: 0x04000FCD RID: 4045
		Popref_popi_popi,
		// Token: 0x04000FCE RID: 4046
		Popref_popi_popi8,
		// Token: 0x04000FCF RID: 4047
		Popref_popi_popr4,
		// Token: 0x04000FD0 RID: 4048
		Popref_popi_popr8,
		// Token: 0x04000FD1 RID: 4049
		Popref_popi_popref,
		// Token: 0x04000FD2 RID: 4050
		Push0,
		// Token: 0x04000FD3 RID: 4051
		Push1,
		// Token: 0x04000FD4 RID: 4052
		Push1_push1,
		// Token: 0x04000FD5 RID: 4053
		Pushi,
		// Token: 0x04000FD6 RID: 4054
		Pushi8,
		// Token: 0x04000FD7 RID: 4055
		Pushr4,
		// Token: 0x04000FD8 RID: 4056
		Pushr8,
		// Token: 0x04000FD9 RID: 4057
		Pushref,
		// Token: 0x04000FDA RID: 4058
		Varpop,
		// Token: 0x04000FDB RID: 4059
		Varpush,
		// Token: 0x04000FDC RID: 4060
		Popref_popi_pop1
	}
}
