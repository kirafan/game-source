﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002D0 RID: 720
	[ComVisible(true)]
	public sealed class DynamicMethod : MethodInfo
	{
		// Token: 0x0600246B RID: 9323 RVA: 0x000821E8 File Offset: 0x000803E8
		public DynamicMethod(string name, Type returnType, Type[] parameterTypes, Module m) : this(name, returnType, parameterTypes, m, false)
		{
		}

		// Token: 0x0600246C RID: 9324 RVA: 0x000821F8 File Offset: 0x000803F8
		public DynamicMethod(string name, Type returnType, Type[] parameterTypes, Type owner) : this(name, returnType, parameterTypes, owner, false)
		{
		}

		// Token: 0x0600246D RID: 9325 RVA: 0x00082208 File Offset: 0x00080408
		public DynamicMethod(string name, Type returnType, Type[] parameterTypes, Module m, bool skipVisibility) : this(name, MethodAttributes.FamANDAssem | MethodAttributes.Family | MethodAttributes.Static, CallingConventions.Standard, returnType, parameterTypes, m, skipVisibility)
		{
		}

		// Token: 0x0600246E RID: 9326 RVA: 0x00082228 File Offset: 0x00080428
		public DynamicMethod(string name, Type returnType, Type[] parameterTypes, Type owner, bool skipVisibility) : this(name, MethodAttributes.FamANDAssem | MethodAttributes.Family | MethodAttributes.Static, CallingConventions.Standard, returnType, parameterTypes, owner, skipVisibility)
		{
		}

		// Token: 0x0600246F RID: 9327 RVA: 0x00082248 File Offset: 0x00080448
		public DynamicMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type owner, bool skipVisibility) : this(name, attributes, callingConvention, returnType, parameterTypes, owner, owner.Module, skipVisibility, false)
		{
		}

		// Token: 0x06002470 RID: 9328 RVA: 0x00082270 File Offset: 0x00080470
		public DynamicMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Module m, bool skipVisibility) : this(name, attributes, callingConvention, returnType, parameterTypes, null, m, skipVisibility, false)
		{
		}

		// Token: 0x06002471 RID: 9329 RVA: 0x00082290 File Offset: 0x00080490
		public DynamicMethod(string name, Type returnType, Type[] parameterTypes) : this(name, returnType, parameterTypes, false)
		{
		}

		// Token: 0x06002472 RID: 9330 RVA: 0x0008229C File Offset: 0x0008049C
		[MonoTODO("Visibility is not restricted")]
		public DynamicMethod(string name, Type returnType, Type[] parameterTypes, bool restrictedSkipVisibility) : this(name, MethodAttributes.FamANDAssem | MethodAttributes.Family | MethodAttributes.Static, CallingConventions.Standard, returnType, parameterTypes, null, null, restrictedSkipVisibility, true)
		{
		}

		// Token: 0x06002473 RID: 9331 RVA: 0x000822BC File Offset: 0x000804BC
		private DynamicMethod(string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] parameterTypes, Type owner, Module m, bool skipVisibility, bool anonHosted)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (returnType == null)
			{
				returnType = typeof(void);
			}
			if (m == null && !anonHosted)
			{
				throw new ArgumentNullException("m");
			}
			if (returnType.IsByRef)
			{
				throw new ArgumentException("Return type can't be a byref type", "returnType");
			}
			if (parameterTypes != null)
			{
				for (int i = 0; i < parameterTypes.Length; i++)
				{
					if (parameterTypes[i] == null)
					{
						throw new ArgumentException("Parameter " + i + " is null", "parameterTypes");
					}
				}
			}
			if (m == null)
			{
				m = DynamicMethod.AnonHostModuleHolder.anon_host_module;
			}
			this.name = name;
			this.attributes = (attributes | MethodAttributes.Static);
			this.callingConvention = callingConvention;
			this.returnType = returnType;
			this.parameters = parameterTypes;
			this.owner = owner;
			this.module = m;
			this.skipVisibility = skipVisibility;
		}

		// Token: 0x06002474 RID: 9332
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void create_dynamic_method(DynamicMethod m);

		// Token: 0x06002475 RID: 9333
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void destroy_dynamic_method(DynamicMethod m);

		// Token: 0x06002476 RID: 9334 RVA: 0x000823C4 File Offset: 0x000805C4
		private void CreateDynMethod()
		{
			if (this.mhandle.Value == IntPtr.Zero)
			{
				if (this.ilgen == null || ILGenerator.Mono_GetCurrentOffset(this.ilgen) == 0)
				{
					throw new InvalidOperationException("Method '" + this.name + "' does not have a method body.");
				}
				this.ilgen.label_fixup();
				try
				{
					this.creating = true;
					if (this.refs != null)
					{
						for (int i = 0; i < this.refs.Length; i++)
						{
							if (this.refs[i] is DynamicMethod)
							{
								DynamicMethod dynamicMethod = (DynamicMethod)this.refs[i];
								if (!dynamicMethod.creating)
								{
									dynamicMethod.CreateDynMethod();
								}
							}
						}
					}
				}
				finally
				{
					this.creating = false;
				}
				this.create_dynamic_method(this);
			}
		}

		// Token: 0x06002477 RID: 9335 RVA: 0x000824B8 File Offset: 0x000806B8
		~DynamicMethod()
		{
			this.destroy_dynamic_method(this);
		}

		// Token: 0x06002478 RID: 9336 RVA: 0x000824F4 File Offset: 0x000806F4
		[ComVisible(true)]
		public Delegate CreateDelegate(Type delegateType)
		{
			if (delegateType == null)
			{
				throw new ArgumentNullException("delegateType");
			}
			if (this.deleg != null)
			{
				return this.deleg;
			}
			this.CreateDynMethod();
			this.deleg = Delegate.CreateDelegate(delegateType, this);
			return this.deleg;
		}

		// Token: 0x06002479 RID: 9337 RVA: 0x00082540 File Offset: 0x00080740
		[ComVisible(true)]
		public Delegate CreateDelegate(Type delegateType, object target)
		{
			if (delegateType == null)
			{
				throw new ArgumentNullException("delegateType");
			}
			this.CreateDynMethod();
			return Delegate.CreateDelegate(delegateType, target, this);
		}

		// Token: 0x0600247A RID: 9338 RVA: 0x00082564 File Offset: 0x00080764
		public ParameterBuilder DefineParameter(int position, ParameterAttributes attributes, string parameterName)
		{
			if (position < 0 || position > this.parameters.Length)
			{
				throw new ArgumentOutOfRangeException("position");
			}
			this.RejectIfCreated();
			ParameterBuilder parameterBuilder = new ParameterBuilder(this, position, attributes, parameterName);
			if (this.pinfo == null)
			{
				this.pinfo = new ParameterBuilder[this.parameters.Length + 1];
			}
			this.pinfo[position] = parameterBuilder;
			return parameterBuilder;
		}

		// Token: 0x0600247B RID: 9339 RVA: 0x000825CC File Offset: 0x000807CC
		public override MethodInfo GetBaseDefinition()
		{
			return this;
		}

		// Token: 0x0600247C RID: 9340 RVA: 0x000825D0 File Offset: 0x000807D0
		[MonoTODO("Not implemented")]
		public override object[] GetCustomAttributes(bool inherit)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600247D RID: 9341 RVA: 0x000825D8 File Offset: 0x000807D8
		[MonoTODO("Not implemented")]
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600247E RID: 9342 RVA: 0x000825E0 File Offset: 0x000807E0
		[MonoTODO("Not implemented")]
		public DynamicILInfo GetDynamicILInfo()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600247F RID: 9343 RVA: 0x000825E8 File Offset: 0x000807E8
		public ILGenerator GetILGenerator()
		{
			return this.GetILGenerator(64);
		}

		// Token: 0x06002480 RID: 9344 RVA: 0x000825F4 File Offset: 0x000807F4
		public ILGenerator GetILGenerator(int streamSize)
		{
			if ((this.GetMethodImplementationFlags() & MethodImplAttributes.CodeTypeMask) != MethodImplAttributes.IL || (this.GetMethodImplementationFlags() & MethodImplAttributes.ManagedMask) != MethodImplAttributes.IL)
			{
				throw new InvalidOperationException("Method body should not exist.");
			}
			if (this.ilgen != null)
			{
				return this.ilgen;
			}
			this.ilgen = new ILGenerator(this.Module, new DynamicMethodTokenGenerator(this), streamSize);
			return this.ilgen;
		}

		// Token: 0x06002481 RID: 9345 RVA: 0x00082658 File Offset: 0x00080858
		public override MethodImplAttributes GetMethodImplementationFlags()
		{
			return MethodImplAttributes.IL;
		}

		// Token: 0x06002482 RID: 9346 RVA: 0x0008265C File Offset: 0x0008085C
		public override ParameterInfo[] GetParameters()
		{
			if (this.parameters == null)
			{
				return new ParameterInfo[0];
			}
			ParameterInfo[] array = new ParameterInfo[this.parameters.Length];
			for (int i = 0; i < this.parameters.Length; i++)
			{
				array[i] = new ParameterInfo((this.pinfo != null) ? this.pinfo[i + 1] : null, this.parameters[i], this, i + 1);
			}
			return array;
		}

		// Token: 0x06002483 RID: 9347 RVA: 0x000826D4 File Offset: 0x000808D4
		public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			object result;
			try
			{
				this.CreateDynMethod();
				if (this.method == null)
				{
					this.method = new MonoMethod(this.mhandle);
				}
				result = this.method.Invoke(obj, parameters);
			}
			catch (MethodAccessException inner)
			{
				throw new TargetInvocationException("Method cannot be invoked.", inner);
			}
			return result;
		}

		// Token: 0x06002484 RID: 9348 RVA: 0x0008274C File Offset: 0x0008094C
		[MonoTODO("Not implemented")]
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002485 RID: 9349 RVA: 0x00082754 File Offset: 0x00080954
		public override string ToString()
		{
			string text = string.Empty;
			ParameterInfo[] array = this.GetParameters();
			for (int i = 0; i < array.Length; i++)
			{
				if (i > 0)
				{
					text += ", ";
				}
				text += array[i].ParameterType.Name;
			}
			return string.Concat(new string[]
			{
				this.ReturnType.Name,
				" ",
				this.Name,
				"(",
				text,
				")"
			});
		}

		// Token: 0x17000667 RID: 1639
		// (get) Token: 0x06002486 RID: 9350 RVA: 0x000827E8 File Offset: 0x000809E8
		public override MethodAttributes Attributes
		{
			get
			{
				return this.attributes;
			}
		}

		// Token: 0x17000668 RID: 1640
		// (get) Token: 0x06002487 RID: 9351 RVA: 0x000827F0 File Offset: 0x000809F0
		public override CallingConventions CallingConvention
		{
			get
			{
				return this.callingConvention;
			}
		}

		// Token: 0x17000669 RID: 1641
		// (get) Token: 0x06002488 RID: 9352 RVA: 0x000827F8 File Offset: 0x000809F8
		public override Type DeclaringType
		{
			get
			{
				return null;
			}
		}

		// Token: 0x1700066A RID: 1642
		// (get) Token: 0x06002489 RID: 9353 RVA: 0x000827FC File Offset: 0x000809FC
		// (set) Token: 0x0600248A RID: 9354 RVA: 0x00082804 File Offset: 0x00080A04
		public bool InitLocals
		{
			get
			{
				return this.init_locals;
			}
			set
			{
				this.init_locals = value;
			}
		}

		// Token: 0x1700066B RID: 1643
		// (get) Token: 0x0600248B RID: 9355 RVA: 0x00082810 File Offset: 0x00080A10
		public override RuntimeMethodHandle MethodHandle
		{
			get
			{
				return this.mhandle;
			}
		}

		// Token: 0x1700066C RID: 1644
		// (get) Token: 0x0600248C RID: 9356 RVA: 0x00082818 File Offset: 0x00080A18
		public override Module Module
		{
			get
			{
				return this.module;
			}
		}

		// Token: 0x1700066D RID: 1645
		// (get) Token: 0x0600248D RID: 9357 RVA: 0x00082820 File Offset: 0x00080A20
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x1700066E RID: 1646
		// (get) Token: 0x0600248E RID: 9358 RVA: 0x00082828 File Offset: 0x00080A28
		public override Type ReflectedType
		{
			get
			{
				return null;
			}
		}

		// Token: 0x1700066F RID: 1647
		// (get) Token: 0x0600248F RID: 9359 RVA: 0x0008282C File Offset: 0x00080A2C
		[MonoTODO("Not implemented")]
		public override ParameterInfo ReturnParameter
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000670 RID: 1648
		// (get) Token: 0x06002490 RID: 9360 RVA: 0x00082834 File Offset: 0x00080A34
		public override Type ReturnType
		{
			get
			{
				return this.returnType;
			}
		}

		// Token: 0x17000671 RID: 1649
		// (get) Token: 0x06002491 RID: 9361 RVA: 0x0008283C File Offset: 0x00080A3C
		[MonoTODO("Not implemented")]
		public override ICustomAttributeProvider ReturnTypeCustomAttributes
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x06002492 RID: 9362 RVA: 0x00082844 File Offset: 0x00080A44
		private void RejectIfCreated()
		{
			if (this.mhandle.Value != IntPtr.Zero)
			{
				throw new InvalidOperationException("Type definition of the method is complete.");
			}
		}

		// Token: 0x06002493 RID: 9363 RVA: 0x0008286C File Offset: 0x00080A6C
		internal int AddRef(object reference)
		{
			if (this.refs == null)
			{
				this.refs = new object[4];
			}
			if (this.nrefs >= this.refs.Length - 1)
			{
				object[] destinationArray = new object[this.refs.Length * 2];
				Array.Copy(this.refs, destinationArray, this.refs.Length);
				this.refs = destinationArray;
			}
			this.refs[this.nrefs] = reference;
			this.refs[this.nrefs + 1] = null;
			this.nrefs += 2;
			return this.nrefs - 1;
		}

		// Token: 0x04000DBF RID: 3519
		private RuntimeMethodHandle mhandle;

		// Token: 0x04000DC0 RID: 3520
		private string name;

		// Token: 0x04000DC1 RID: 3521
		private Type returnType;

		// Token: 0x04000DC2 RID: 3522
		private Type[] parameters;

		// Token: 0x04000DC3 RID: 3523
		private MethodAttributes attributes;

		// Token: 0x04000DC4 RID: 3524
		private CallingConventions callingConvention;

		// Token: 0x04000DC5 RID: 3525
		private Module module;

		// Token: 0x04000DC6 RID: 3526
		private bool skipVisibility;

		// Token: 0x04000DC7 RID: 3527
		private bool init_locals = true;

		// Token: 0x04000DC8 RID: 3528
		private ILGenerator ilgen;

		// Token: 0x04000DC9 RID: 3529
		private int nrefs;

		// Token: 0x04000DCA RID: 3530
		private object[] refs;

		// Token: 0x04000DCB RID: 3531
		private IntPtr referenced_by;

		// Token: 0x04000DCC RID: 3532
		private Type owner;

		// Token: 0x04000DCD RID: 3533
		private Delegate deleg;

		// Token: 0x04000DCE RID: 3534
		private MonoMethod method;

		// Token: 0x04000DCF RID: 3535
		private ParameterBuilder[] pinfo;

		// Token: 0x04000DD0 RID: 3536
		internal bool creating;

		// Token: 0x020002D1 RID: 721
		private class AnonHostModuleHolder
		{
			// Token: 0x06002495 RID: 9365 RVA: 0x0008290C File Offset: 0x00080B0C
			static AnonHostModuleHolder()
			{
				AssemblyName assemblyName = new AssemblyName();
				assemblyName.Name = "Anonymously Hosted DynamicMethods Assembly";
				AssemblyBuilder assemblyBuilder = AppDomain.CurrentDomain.DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run);
				DynamicMethod.AnonHostModuleHolder.anon_host_module = assemblyBuilder.GetManifestModule();
			}

			// Token: 0x04000DD1 RID: 3537
			public static Module anon_host_module;
		}
	}
}
