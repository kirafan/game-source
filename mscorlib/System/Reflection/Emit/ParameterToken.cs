﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002F5 RID: 757
	[ComVisible(true)]
	[Serializable]
	public struct ParameterToken
	{
		// Token: 0x060026C9 RID: 9929 RVA: 0x0008A784 File Offset: 0x00088984
		internal ParameterToken(int val)
		{
			this.tokValue = val;
		}

		// Token: 0x060026CB RID: 9931 RVA: 0x0008A7AC File Offset: 0x000889AC
		public override bool Equals(object obj)
		{
			bool flag = obj is ParameterToken;
			if (flag)
			{
				ParameterToken parameterToken = (ParameterToken)obj;
				flag = (this.tokValue == parameterToken.tokValue);
			}
			return flag;
		}

		// Token: 0x060026CC RID: 9932 RVA: 0x0008A7E4 File Offset: 0x000889E4
		public bool Equals(ParameterToken obj)
		{
			return this.tokValue == obj.tokValue;
		}

		// Token: 0x060026CD RID: 9933 RVA: 0x0008A7F8 File Offset: 0x000889F8
		public override int GetHashCode()
		{
			return this.tokValue;
		}

		// Token: 0x170006EC RID: 1772
		// (get) Token: 0x060026CE RID: 9934 RVA: 0x0008A800 File Offset: 0x00088A00
		public int Token
		{
			get
			{
				return this.tokValue;
			}
		}

		// Token: 0x060026CF RID: 9935 RVA: 0x0008A808 File Offset: 0x00088A08
		public static bool operator ==(ParameterToken a, ParameterToken b)
		{
			return object.Equals(a, b);
		}

		// Token: 0x060026D0 RID: 9936 RVA: 0x0008A81C File Offset: 0x00088A1C
		public static bool operator !=(ParameterToken a, ParameterToken b)
		{
			return !object.Equals(a, b);
		}

		// Token: 0x04000F98 RID: 3992
		internal int tokValue;

		// Token: 0x04000F99 RID: 3993
		public static readonly ParameterToken Empty = default(ParameterToken);
	}
}
