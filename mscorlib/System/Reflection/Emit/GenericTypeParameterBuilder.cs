﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002DB RID: 731
	[ComVisible(true)]
	public sealed class GenericTypeParameterBuilder : Type
	{
		// Token: 0x0600252C RID: 9516 RVA: 0x00083790 File Offset: 0x00081990
		internal GenericTypeParameterBuilder(TypeBuilder tbuilder, MethodBuilder mbuilder, string name, int index)
		{
			this.tbuilder = tbuilder;
			this.mbuilder = mbuilder;
			this.name = name;
			this.index = index;
			this.initialize();
		}

		// Token: 0x0600252D RID: 9517 RVA: 0x000837BC File Offset: 0x000819BC
		public void SetBaseTypeConstraint(Type baseTypeConstraint)
		{
			this.base_type = (baseTypeConstraint ?? typeof(object));
		}

		// Token: 0x0600252E RID: 9518 RVA: 0x000837D8 File Offset: 0x000819D8
		[ComVisible(true)]
		public void SetInterfaceConstraints(params Type[] interfaceConstraints)
		{
			this.iface_constraints = interfaceConstraints;
		}

		// Token: 0x0600252F RID: 9519 RVA: 0x000837E4 File Offset: 0x000819E4
		public void SetGenericParameterAttributes(GenericParameterAttributes genericParameterAttributes)
		{
			this.attrs = genericParameterAttributes;
		}

		// Token: 0x06002530 RID: 9520
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void initialize();

		// Token: 0x06002531 RID: 9521 RVA: 0x000837F0 File Offset: 0x000819F0
		[ComVisible(true)]
		public override bool IsSubclassOf(Type c)
		{
			if (!((ModuleBuilder)this.tbuilder.Module).assemblyb.IsCompilerContext)
			{
				throw this.not_supported();
			}
			return this.BaseType != null && (this.BaseType == c || this.BaseType.IsSubclassOf(c));
		}

		// Token: 0x06002532 RID: 9522 RVA: 0x0008384C File Offset: 0x00081A4C
		protected override TypeAttributes GetAttributeFlagsImpl()
		{
			if (((ModuleBuilder)this.tbuilder.Module).assemblyb.IsCompilerContext)
			{
				return TypeAttributes.Public;
			}
			throw this.not_supported();
		}

		// Token: 0x06002533 RID: 9523 RVA: 0x00083878 File Offset: 0x00081A78
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			throw this.not_supported();
		}

		// Token: 0x06002534 RID: 9524 RVA: 0x00083880 File Offset: 0x00081A80
		[ComVisible(true)]
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x06002535 RID: 9525 RVA: 0x00083888 File Offset: 0x00081A88
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x06002536 RID: 9526 RVA: 0x00083890 File Offset: 0x00081A90
		public override EventInfo[] GetEvents()
		{
			throw this.not_supported();
		}

		// Token: 0x06002537 RID: 9527 RVA: 0x00083898 File Offset: 0x00081A98
		public override EventInfo[] GetEvents(BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x06002538 RID: 9528 RVA: 0x000838A0 File Offset: 0x00081AA0
		public override FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x06002539 RID: 9529 RVA: 0x000838A8 File Offset: 0x00081AA8
		public override FieldInfo[] GetFields(BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x0600253A RID: 9530 RVA: 0x000838B0 File Offset: 0x00081AB0
		public override Type GetInterface(string name, bool ignoreCase)
		{
			throw this.not_supported();
		}

		// Token: 0x0600253B RID: 9531 RVA: 0x000838B8 File Offset: 0x00081AB8
		public override Type[] GetInterfaces()
		{
			throw this.not_supported();
		}

		// Token: 0x0600253C RID: 9532 RVA: 0x000838C0 File Offset: 0x00081AC0
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x0600253D RID: 9533 RVA: 0x000838C8 File Offset: 0x00081AC8
		public override MemberInfo[] GetMember(string name, MemberTypes type, BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x0600253E RID: 9534 RVA: 0x000838D0 File Offset: 0x00081AD0
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x0600253F RID: 9535 RVA: 0x000838D8 File Offset: 0x00081AD8
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			throw this.not_supported();
		}

		// Token: 0x06002540 RID: 9536 RVA: 0x000838E0 File Offset: 0x00081AE0
		public override Type GetNestedType(string name, BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x06002541 RID: 9537 RVA: 0x000838E8 File Offset: 0x00081AE8
		public override Type[] GetNestedTypes(BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x06002542 RID: 9538 RVA: 0x000838F0 File Offset: 0x00081AF0
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
		{
			throw this.not_supported();
		}

		// Token: 0x06002543 RID: 9539 RVA: 0x000838F8 File Offset: 0x00081AF8
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			throw this.not_supported();
		}

		// Token: 0x06002544 RID: 9540 RVA: 0x00083900 File Offset: 0x00081B00
		protected override bool HasElementTypeImpl()
		{
			return false;
		}

		// Token: 0x06002545 RID: 9541 RVA: 0x00083904 File Offset: 0x00081B04
		public override bool IsAssignableFrom(Type c)
		{
			throw this.not_supported();
		}

		// Token: 0x06002546 RID: 9542 RVA: 0x0008390C File Offset: 0x00081B0C
		public override bool IsInstanceOfType(object o)
		{
			throw this.not_supported();
		}

		// Token: 0x06002547 RID: 9543 RVA: 0x00083914 File Offset: 0x00081B14
		protected override bool IsArrayImpl()
		{
			return false;
		}

		// Token: 0x06002548 RID: 9544 RVA: 0x00083918 File Offset: 0x00081B18
		protected override bool IsByRefImpl()
		{
			return false;
		}

		// Token: 0x06002549 RID: 9545 RVA: 0x0008391C File Offset: 0x00081B1C
		protected override bool IsCOMObjectImpl()
		{
			return false;
		}

		// Token: 0x0600254A RID: 9546 RVA: 0x00083920 File Offset: 0x00081B20
		protected override bool IsPointerImpl()
		{
			return false;
		}

		// Token: 0x0600254B RID: 9547 RVA: 0x00083924 File Offset: 0x00081B24
		protected override bool IsPrimitiveImpl()
		{
			return false;
		}

		// Token: 0x0600254C RID: 9548 RVA: 0x00083928 File Offset: 0x00081B28
		protected override bool IsValueTypeImpl()
		{
			return this.base_type != null && this.base_type.IsValueType;
		}

		// Token: 0x0600254D RID: 9549 RVA: 0x00083948 File Offset: 0x00081B48
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
		{
			throw this.not_supported();
		}

		// Token: 0x0600254E RID: 9550 RVA: 0x00083950 File Offset: 0x00081B50
		public override Type GetElementType()
		{
			throw this.not_supported();
		}

		// Token: 0x17000695 RID: 1685
		// (get) Token: 0x0600254F RID: 9551 RVA: 0x00083958 File Offset: 0x00081B58
		public override Type UnderlyingSystemType
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000696 RID: 1686
		// (get) Token: 0x06002550 RID: 9552 RVA: 0x0008395C File Offset: 0x00081B5C
		public override Assembly Assembly
		{
			get
			{
				return this.tbuilder.Assembly;
			}
		}

		// Token: 0x17000697 RID: 1687
		// (get) Token: 0x06002551 RID: 9553 RVA: 0x0008396C File Offset: 0x00081B6C
		public override string AssemblyQualifiedName
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000698 RID: 1688
		// (get) Token: 0x06002552 RID: 9554 RVA: 0x00083970 File Offset: 0x00081B70
		public override Type BaseType
		{
			get
			{
				return this.base_type;
			}
		}

		// Token: 0x17000699 RID: 1689
		// (get) Token: 0x06002553 RID: 9555 RVA: 0x00083978 File Offset: 0x00081B78
		public override string FullName
		{
			get
			{
				return null;
			}
		}

		// Token: 0x1700069A RID: 1690
		// (get) Token: 0x06002554 RID: 9556 RVA: 0x0008397C File Offset: 0x00081B7C
		public override Guid GUID
		{
			get
			{
				throw this.not_supported();
			}
		}

		// Token: 0x06002555 RID: 9557 RVA: 0x00083984 File Offset: 0x00081B84
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			throw this.not_supported();
		}

		// Token: 0x06002556 RID: 9558 RVA: 0x0008398C File Offset: 0x00081B8C
		public override object[] GetCustomAttributes(bool inherit)
		{
			throw this.not_supported();
		}

		// Token: 0x06002557 RID: 9559 RVA: 0x00083994 File Offset: 0x00081B94
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			throw this.not_supported();
		}

		// Token: 0x06002558 RID: 9560 RVA: 0x0008399C File Offset: 0x00081B9C
		[ComVisible(true)]
		public override InterfaceMapping GetInterfaceMap(Type interfaceType)
		{
			throw this.not_supported();
		}

		// Token: 0x1700069B RID: 1691
		// (get) Token: 0x06002559 RID: 9561 RVA: 0x000839A4 File Offset: 0x00081BA4
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x1700069C RID: 1692
		// (get) Token: 0x0600255A RID: 9562 RVA: 0x000839AC File Offset: 0x00081BAC
		public override string Namespace
		{
			get
			{
				return null;
			}
		}

		// Token: 0x1700069D RID: 1693
		// (get) Token: 0x0600255B RID: 9563 RVA: 0x000839B0 File Offset: 0x00081BB0
		public override Module Module
		{
			get
			{
				return this.tbuilder.Module;
			}
		}

		// Token: 0x1700069E RID: 1694
		// (get) Token: 0x0600255C RID: 9564 RVA: 0x000839C0 File Offset: 0x00081BC0
		public override Type DeclaringType
		{
			get
			{
				return (this.mbuilder == null) ? this.tbuilder : this.mbuilder.DeclaringType;
			}
		}

		// Token: 0x1700069F RID: 1695
		// (get) Token: 0x0600255D RID: 9565 RVA: 0x000839E4 File Offset: 0x00081BE4
		public override Type ReflectedType
		{
			get
			{
				return this.DeclaringType;
			}
		}

		// Token: 0x170006A0 RID: 1696
		// (get) Token: 0x0600255E RID: 9566 RVA: 0x000839EC File Offset: 0x00081BEC
		public override RuntimeTypeHandle TypeHandle
		{
			get
			{
				throw this.not_supported();
			}
		}

		// Token: 0x0600255F RID: 9567 RVA: 0x000839F4 File Offset: 0x00081BF4
		public override Type[] GetGenericArguments()
		{
			throw new InvalidOperationException();
		}

		// Token: 0x06002560 RID: 9568 RVA: 0x000839FC File Offset: 0x00081BFC
		public override Type GetGenericTypeDefinition()
		{
			throw new InvalidOperationException();
		}

		// Token: 0x170006A1 RID: 1697
		// (get) Token: 0x06002561 RID: 9569 RVA: 0x00083A04 File Offset: 0x00081C04
		public override bool ContainsGenericParameters
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170006A2 RID: 1698
		// (get) Token: 0x06002562 RID: 9570 RVA: 0x00083A08 File Offset: 0x00081C08
		public override bool IsGenericParameter
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170006A3 RID: 1699
		// (get) Token: 0x06002563 RID: 9571 RVA: 0x00083A0C File Offset: 0x00081C0C
		public override bool IsGenericType
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170006A4 RID: 1700
		// (get) Token: 0x06002564 RID: 9572 RVA: 0x00083A10 File Offset: 0x00081C10
		public override bool IsGenericTypeDefinition
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170006A5 RID: 1701
		// (get) Token: 0x06002565 RID: 9573 RVA: 0x00083A14 File Offset: 0x00081C14
		public override GenericParameterAttributes GenericParameterAttributes
		{
			get
			{
				if (((ModuleBuilder)this.tbuilder.Module).assemblyb.IsCompilerContext)
				{
					return this.attrs;
				}
				throw new NotSupportedException();
			}
		}

		// Token: 0x170006A6 RID: 1702
		// (get) Token: 0x06002566 RID: 9574 RVA: 0x00083A44 File Offset: 0x00081C44
		public override int GenericParameterPosition
		{
			get
			{
				return this.index;
			}
		}

		// Token: 0x06002567 RID: 9575 RVA: 0x00083A4C File Offset: 0x00081C4C
		public override Type[] GetGenericParameterConstraints()
		{
			if (!((ModuleBuilder)this.tbuilder.Module).assemblyb.IsCompilerContext)
			{
				throw new InvalidOperationException();
			}
			if (this.base_type == null)
			{
				if (this.iface_constraints != null)
				{
					return this.iface_constraints;
				}
				return Type.EmptyTypes;
			}
			else
			{
				if (this.iface_constraints == null)
				{
					return new Type[]
					{
						this.base_type
					};
				}
				Type[] array = new Type[this.iface_constraints.Length + 1];
				array[0] = this.base_type;
				this.iface_constraints.CopyTo(array, 1);
				return array;
			}
		}

		// Token: 0x170006A7 RID: 1703
		// (get) Token: 0x06002568 RID: 9576 RVA: 0x00083AE4 File Offset: 0x00081CE4
		public override MethodBase DeclaringMethod
		{
			get
			{
				return this.mbuilder;
			}
		}

		// Token: 0x06002569 RID: 9577 RVA: 0x00083AEC File Offset: 0x00081CEC
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			if (customBuilder == null)
			{
				throw new ArgumentNullException("customBuilder");
			}
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x0600256A RID: 9578 RVA: 0x00083B60 File Offset: 0x00081D60
		[MonoTODO("unverified implementation")]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x0600256B RID: 9579 RVA: 0x00083B70 File Offset: 0x00081D70
		private Exception not_supported()
		{
			return new NotSupportedException();
		}

		// Token: 0x0600256C RID: 9580 RVA: 0x00083B78 File Offset: 0x00081D78
		public override string ToString()
		{
			return this.name;
		}

		// Token: 0x0600256D RID: 9581 RVA: 0x00083B80 File Offset: 0x00081D80
		[MonoTODO]
		public override bool Equals(object o)
		{
			return base.Equals(o);
		}

		// Token: 0x0600256E RID: 9582 RVA: 0x00083B8C File Offset: 0x00081D8C
		[MonoTODO]
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}

		// Token: 0x0600256F RID: 9583 RVA: 0x00083B94 File Offset: 0x00081D94
		public override Type MakeArrayType()
		{
			return new ArrayType(this, 0);
		}

		// Token: 0x06002570 RID: 9584 RVA: 0x00083BA0 File Offset: 0x00081DA0
		public override Type MakeArrayType(int rank)
		{
			if (rank < 1)
			{
				throw new IndexOutOfRangeException();
			}
			return new ArrayType(this, rank);
		}

		// Token: 0x06002571 RID: 9585 RVA: 0x00083BB8 File Offset: 0x00081DB8
		public override Type MakeByRefType()
		{
			return new ByRefType(this);
		}

		// Token: 0x06002572 RID: 9586 RVA: 0x00083BC0 File Offset: 0x00081DC0
		[MonoTODO]
		public override Type MakeGenericType(params Type[] typeArguments)
		{
			return base.MakeGenericType(typeArguments);
		}

		// Token: 0x06002573 RID: 9587 RVA: 0x00083BCC File Offset: 0x00081DCC
		public override Type MakePointerType()
		{
			return new PointerType(this);
		}

		// Token: 0x04000DFF RID: 3583
		private TypeBuilder tbuilder;

		// Token: 0x04000E00 RID: 3584
		private MethodBuilder mbuilder;

		// Token: 0x04000E01 RID: 3585
		private string name;

		// Token: 0x04000E02 RID: 3586
		private int index;

		// Token: 0x04000E03 RID: 3587
		private Type base_type;

		// Token: 0x04000E04 RID: 3588
		private Type[] iface_constraints;

		// Token: 0x04000E05 RID: 3589
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000E06 RID: 3590
		private GenericParameterAttributes attrs;
	}
}
