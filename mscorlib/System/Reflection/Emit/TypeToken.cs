﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x02000300 RID: 768
	[ComVisible(true)]
	[Serializable]
	public struct TypeToken
	{
		// Token: 0x060027BF RID: 10175 RVA: 0x0008DD20 File Offset: 0x0008BF20
		internal TypeToken(int val)
		{
			this.tokValue = val;
		}

		// Token: 0x060027C1 RID: 10177 RVA: 0x0008DD48 File Offset: 0x0008BF48
		public override bool Equals(object obj)
		{
			bool flag = obj is TypeToken;
			if (flag)
			{
				TypeToken typeToken = (TypeToken)obj;
				flag = (this.tokValue == typeToken.tokValue);
			}
			return flag;
		}

		// Token: 0x060027C2 RID: 10178 RVA: 0x0008DD80 File Offset: 0x0008BF80
		public bool Equals(TypeToken obj)
		{
			return this.tokValue == obj.tokValue;
		}

		// Token: 0x060027C3 RID: 10179 RVA: 0x0008DD94 File Offset: 0x0008BF94
		public override int GetHashCode()
		{
			return this.tokValue;
		}

		// Token: 0x17000718 RID: 1816
		// (get) Token: 0x060027C4 RID: 10180 RVA: 0x0008DD9C File Offset: 0x0008BF9C
		public int Token
		{
			get
			{
				return this.tokValue;
			}
		}

		// Token: 0x060027C5 RID: 10181 RVA: 0x0008DDA4 File Offset: 0x0008BFA4
		public static bool operator ==(TypeToken a, TypeToken b)
		{
			return object.Equals(a, b);
		}

		// Token: 0x060027C6 RID: 10182 RVA: 0x0008DDB8 File Offset: 0x0008BFB8
		public static bool operator !=(TypeToken a, TypeToken b)
		{
			return !object.Equals(a, b);
		}

		// Token: 0x04000FFA RID: 4090
		internal int tokValue;

		// Token: 0x04000FFB RID: 4091
		public static readonly TypeToken Empty = default(TypeToken);
	}
}
