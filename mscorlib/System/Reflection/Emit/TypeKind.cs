﻿using System;

namespace System.Reflection.Emit
{
	// Token: 0x020002CA RID: 714
	internal enum TypeKind
	{
		// Token: 0x04000DBB RID: 3515
		SZARRAY = 29,
		// Token: 0x04000DBC RID: 3516
		ARRAY = 20
	}
}
