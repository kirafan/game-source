﻿using System;
using System.Globalization;

namespace System.Reflection.Emit
{
	// Token: 0x020002C7 RID: 711
	internal class ConstructorOnTypeBuilderInst : ConstructorInfo
	{
		// Token: 0x060023ED RID: 9197 RVA: 0x00080D1C File Offset: 0x0007EF1C
		public ConstructorOnTypeBuilderInst(MonoGenericClass instantiation, ConstructorBuilder cb)
		{
			this.instantiation = instantiation;
			this.cb = cb;
		}

		// Token: 0x1700064B RID: 1611
		// (get) Token: 0x060023EE RID: 9198 RVA: 0x00080D34 File Offset: 0x0007EF34
		public override Type DeclaringType
		{
			get
			{
				return this.instantiation;
			}
		}

		// Token: 0x1700064C RID: 1612
		// (get) Token: 0x060023EF RID: 9199 RVA: 0x00080D3C File Offset: 0x0007EF3C
		public override string Name
		{
			get
			{
				return this.cb.Name;
			}
		}

		// Token: 0x1700064D RID: 1613
		// (get) Token: 0x060023F0 RID: 9200 RVA: 0x00080D4C File Offset: 0x0007EF4C
		public override Type ReflectedType
		{
			get
			{
				return this.instantiation;
			}
		}

		// Token: 0x060023F1 RID: 9201 RVA: 0x00080D54 File Offset: 0x0007EF54
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return this.cb.IsDefined(attributeType, inherit);
		}

		// Token: 0x060023F2 RID: 9202 RVA: 0x00080D64 File Offset: 0x0007EF64
		public override object[] GetCustomAttributes(bool inherit)
		{
			return this.cb.GetCustomAttributes(inherit);
		}

		// Token: 0x060023F3 RID: 9203 RVA: 0x00080D74 File Offset: 0x0007EF74
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return this.cb.GetCustomAttributes(attributeType, inherit);
		}

		// Token: 0x060023F4 RID: 9204 RVA: 0x00080D84 File Offset: 0x0007EF84
		public override MethodImplAttributes GetMethodImplementationFlags()
		{
			return this.cb.GetMethodImplementationFlags();
		}

		// Token: 0x060023F5 RID: 9205 RVA: 0x00080D94 File Offset: 0x0007EF94
		public override ParameterInfo[] GetParameters()
		{
			if (!((ModuleBuilder)this.cb.Module).assemblyb.IsCompilerContext && !this.instantiation.generic_type.is_created)
			{
				throw new NotSupportedException();
			}
			ParameterInfo[] array = new ParameterInfo[this.cb.parameters.Length];
			for (int i = 0; i < this.cb.parameters.Length; i++)
			{
				Type type = this.instantiation.InflateType(this.cb.parameters[i]);
				array[i] = new ParameterInfo((this.cb.pinfo != null) ? this.cb.pinfo[i] : null, type, this, i + 1);
			}
			return array;
		}

		// Token: 0x1700064E RID: 1614
		// (get) Token: 0x060023F6 RID: 9206 RVA: 0x00080E58 File Offset: 0x0007F058
		public override int MetadataToken
		{
			get
			{
				if (!((ModuleBuilder)this.cb.Module).assemblyb.IsCompilerContext)
				{
					return base.MetadataToken;
				}
				return this.cb.MetadataToken;
			}
		}

		// Token: 0x060023F7 RID: 9207 RVA: 0x00080E98 File Offset: 0x0007F098
		internal override int GetParameterCount()
		{
			return this.cb.GetParameterCount();
		}

		// Token: 0x060023F8 RID: 9208 RVA: 0x00080EA8 File Offset: 0x0007F0A8
		public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			return this.cb.Invoke(obj, invokeAttr, binder, parameters, culture);
		}

		// Token: 0x1700064F RID: 1615
		// (get) Token: 0x060023F9 RID: 9209 RVA: 0x00080EBC File Offset: 0x0007F0BC
		public override RuntimeMethodHandle MethodHandle
		{
			get
			{
				return this.cb.MethodHandle;
			}
		}

		// Token: 0x17000650 RID: 1616
		// (get) Token: 0x060023FA RID: 9210 RVA: 0x00080ECC File Offset: 0x0007F0CC
		public override MethodAttributes Attributes
		{
			get
			{
				return this.cb.Attributes;
			}
		}

		// Token: 0x17000651 RID: 1617
		// (get) Token: 0x060023FB RID: 9211 RVA: 0x00080EDC File Offset: 0x0007F0DC
		public override CallingConventions CallingConvention
		{
			get
			{
				return this.cb.CallingConvention;
			}
		}

		// Token: 0x060023FC RID: 9212 RVA: 0x00080EEC File Offset: 0x0007F0EC
		public override Type[] GetGenericArguments()
		{
			return this.cb.GetGenericArguments();
		}

		// Token: 0x17000652 RID: 1618
		// (get) Token: 0x060023FD RID: 9213 RVA: 0x00080EFC File Offset: 0x0007F0FC
		public override bool ContainsGenericParameters
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000653 RID: 1619
		// (get) Token: 0x060023FE RID: 9214 RVA: 0x00080F00 File Offset: 0x0007F100
		public override bool IsGenericMethodDefinition
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000654 RID: 1620
		// (get) Token: 0x060023FF RID: 9215 RVA: 0x00080F04 File Offset: 0x0007F104
		public override bool IsGenericMethod
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06002400 RID: 9216 RVA: 0x00080F08 File Offset: 0x0007F108
		public override object Invoke(BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			throw new InvalidOperationException();
		}

		// Token: 0x04000DB1 RID: 3505
		private MonoGenericClass instantiation;

		// Token: 0x04000DB2 RID: 3506
		private ConstructorBuilder cb;
	}
}
