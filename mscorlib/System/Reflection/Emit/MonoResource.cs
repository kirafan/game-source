﻿using System;
using System.IO;

namespace System.Reflection.Emit
{
	// Token: 0x020002C2 RID: 706
	internal struct MonoResource
	{
		// Token: 0x04000D6D RID: 3437
		public byte[] data;

		// Token: 0x04000D6E RID: 3438
		public string name;

		// Token: 0x04000D6F RID: 3439
		public string filename;

		// Token: 0x04000D70 RID: 3440
		public ResourceAttributes attrs;

		// Token: 0x04000D71 RID: 3441
		public int offset;

		// Token: 0x04000D72 RID: 3442
		public Stream stream;
	}
}
