﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002E9 RID: 745
	[ComVisible(true)]
	[Serializable]
	public struct MethodToken
	{
		// Token: 0x06002634 RID: 9780 RVA: 0x00087210 File Offset: 0x00085410
		internal MethodToken(int val)
		{
			this.tokValue = val;
		}

		// Token: 0x06002636 RID: 9782 RVA: 0x00087238 File Offset: 0x00085438
		public override bool Equals(object obj)
		{
			bool flag = obj is MethodToken;
			if (flag)
			{
				MethodToken methodToken = (MethodToken)obj;
				flag = (this.tokValue == methodToken.tokValue);
			}
			return flag;
		}

		// Token: 0x06002637 RID: 9783 RVA: 0x00087270 File Offset: 0x00085470
		public bool Equals(MethodToken obj)
		{
			return this.tokValue == obj.tokValue;
		}

		// Token: 0x06002638 RID: 9784 RVA: 0x00087284 File Offset: 0x00085484
		public override int GetHashCode()
		{
			return this.tokValue;
		}

		// Token: 0x170006D3 RID: 1747
		// (get) Token: 0x06002639 RID: 9785 RVA: 0x0008728C File Offset: 0x0008548C
		public int Token
		{
			get
			{
				return this.tokValue;
			}
		}

		// Token: 0x0600263A RID: 9786 RVA: 0x00087294 File Offset: 0x00085494
		public static bool operator ==(MethodToken a, MethodToken b)
		{
			return object.Equals(a, b);
		}

		// Token: 0x0600263B RID: 9787 RVA: 0x000872A8 File Offset: 0x000854A8
		public static bool operator !=(MethodToken a, MethodToken b)
		{
			return !object.Equals(a, b);
		}

		// Token: 0x04000E60 RID: 3680
		internal int tokValue;

		// Token: 0x04000E61 RID: 3681
		public static readonly MethodToken Empty = default(MethodToken);
	}
}
