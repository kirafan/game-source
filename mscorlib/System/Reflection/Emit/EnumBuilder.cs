﻿using System;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002D3 RID: 723
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	[ComDefaultInterface(typeof(_EnumBuilder))]
	public sealed class EnumBuilder : Type, _EnumBuilder
	{
		// Token: 0x0600249B RID: 9371 RVA: 0x0008298C File Offset: 0x00080B8C
		internal EnumBuilder(ModuleBuilder mb, string name, TypeAttributes visibility, Type underlyingType)
		{
			this._tb = new TypeBuilder(mb, name, visibility | TypeAttributes.Sealed, typeof(Enum), null, PackingSize.Unspecified, 0, null);
			this._underlyingType = underlyingType;
			this._underlyingField = this._tb.DefineField("value__", underlyingType, FieldAttributes.Private | FieldAttributes.SpecialName | FieldAttributes.RTSpecialName);
			this.setup_enum_type(this._tb);
		}

		// Token: 0x0600249C RID: 9372 RVA: 0x000829F4 File Offset: 0x00080BF4
		void _EnumBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600249D RID: 9373 RVA: 0x000829FC File Offset: 0x00080BFC
		void _EnumBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600249E RID: 9374 RVA: 0x00082A04 File Offset: 0x00080C04
		void _EnumBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600249F RID: 9375 RVA: 0x00082A0C File Offset: 0x00080C0C
		void _EnumBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060024A0 RID: 9376 RVA: 0x00082A14 File Offset: 0x00080C14
		internal TypeBuilder GetTypeBuilder()
		{
			return this._tb;
		}

		// Token: 0x17000672 RID: 1650
		// (get) Token: 0x060024A1 RID: 9377 RVA: 0x00082A1C File Offset: 0x00080C1C
		public override Assembly Assembly
		{
			get
			{
				return this._tb.Assembly;
			}
		}

		// Token: 0x17000673 RID: 1651
		// (get) Token: 0x060024A2 RID: 9378 RVA: 0x00082A2C File Offset: 0x00080C2C
		public override string AssemblyQualifiedName
		{
			get
			{
				return this._tb.AssemblyQualifiedName;
			}
		}

		// Token: 0x17000674 RID: 1652
		// (get) Token: 0x060024A3 RID: 9379 RVA: 0x00082A3C File Offset: 0x00080C3C
		public override Type BaseType
		{
			get
			{
				return this._tb.BaseType;
			}
		}

		// Token: 0x17000675 RID: 1653
		// (get) Token: 0x060024A4 RID: 9380 RVA: 0x00082A4C File Offset: 0x00080C4C
		public override Type DeclaringType
		{
			get
			{
				return this._tb.DeclaringType;
			}
		}

		// Token: 0x17000676 RID: 1654
		// (get) Token: 0x060024A5 RID: 9381 RVA: 0x00082A5C File Offset: 0x00080C5C
		public override string FullName
		{
			get
			{
				return this._tb.FullName;
			}
		}

		// Token: 0x17000677 RID: 1655
		// (get) Token: 0x060024A6 RID: 9382 RVA: 0x00082A6C File Offset: 0x00080C6C
		public override Guid GUID
		{
			get
			{
				return this._tb.GUID;
			}
		}

		// Token: 0x17000678 RID: 1656
		// (get) Token: 0x060024A7 RID: 9383 RVA: 0x00082A7C File Offset: 0x00080C7C
		public override Module Module
		{
			get
			{
				return this._tb.Module;
			}
		}

		// Token: 0x17000679 RID: 1657
		// (get) Token: 0x060024A8 RID: 9384 RVA: 0x00082A8C File Offset: 0x00080C8C
		public override string Name
		{
			get
			{
				return this._tb.Name;
			}
		}

		// Token: 0x1700067A RID: 1658
		// (get) Token: 0x060024A9 RID: 9385 RVA: 0x00082A9C File Offset: 0x00080C9C
		public override string Namespace
		{
			get
			{
				return this._tb.Namespace;
			}
		}

		// Token: 0x1700067B RID: 1659
		// (get) Token: 0x060024AA RID: 9386 RVA: 0x00082AAC File Offset: 0x00080CAC
		public override Type ReflectedType
		{
			get
			{
				return this._tb.ReflectedType;
			}
		}

		// Token: 0x1700067C RID: 1660
		// (get) Token: 0x060024AB RID: 9387 RVA: 0x00082ABC File Offset: 0x00080CBC
		public override RuntimeTypeHandle TypeHandle
		{
			get
			{
				return this._tb.TypeHandle;
			}
		}

		// Token: 0x1700067D RID: 1661
		// (get) Token: 0x060024AC RID: 9388 RVA: 0x00082ACC File Offset: 0x00080CCC
		public TypeToken TypeToken
		{
			get
			{
				return this._tb.TypeToken;
			}
		}

		// Token: 0x1700067E RID: 1662
		// (get) Token: 0x060024AD RID: 9389 RVA: 0x00082ADC File Offset: 0x00080CDC
		public FieldBuilder UnderlyingField
		{
			get
			{
				return this._underlyingField;
			}
		}

		// Token: 0x1700067F RID: 1663
		// (get) Token: 0x060024AE RID: 9390 RVA: 0x00082AE4 File Offset: 0x00080CE4
		public override Type UnderlyingSystemType
		{
			get
			{
				return this._underlyingType;
			}
		}

		// Token: 0x060024AF RID: 9391 RVA: 0x00082AEC File Offset: 0x00080CEC
		public Type CreateType()
		{
			return this._tb.CreateType();
		}

		// Token: 0x060024B0 RID: 9392
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void setup_enum_type(Type t);

		// Token: 0x060024B1 RID: 9393 RVA: 0x00082B08 File Offset: 0x00080D08
		public FieldBuilder DefineLiteral(string literalName, object literalValue)
		{
			FieldBuilder fieldBuilder = this._tb.DefineField(literalName, this, FieldAttributes.FamANDAssem | FieldAttributes.Family | FieldAttributes.Static | FieldAttributes.Literal);
			fieldBuilder.SetConstant(literalValue);
			return fieldBuilder;
		}

		// Token: 0x060024B2 RID: 9394 RVA: 0x00082B30 File Offset: 0x00080D30
		protected override TypeAttributes GetAttributeFlagsImpl()
		{
			return this._tb.attrs;
		}

		// Token: 0x060024B3 RID: 9395 RVA: 0x00082B40 File Offset: 0x00080D40
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			return this._tb.GetConstructor(bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x060024B4 RID: 9396 RVA: 0x00082B54 File Offset: 0x00080D54
		[ComVisible(true)]
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
		{
			return this._tb.GetConstructors(bindingAttr);
		}

		// Token: 0x060024B5 RID: 9397 RVA: 0x00082B64 File Offset: 0x00080D64
		public override object[] GetCustomAttributes(bool inherit)
		{
			return this._tb.GetCustomAttributes(inherit);
		}

		// Token: 0x060024B6 RID: 9398 RVA: 0x00082B74 File Offset: 0x00080D74
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return this._tb.GetCustomAttributes(attributeType, inherit);
		}

		// Token: 0x060024B7 RID: 9399 RVA: 0x00082B84 File Offset: 0x00080D84
		public override Type GetElementType()
		{
			return this._tb.GetElementType();
		}

		// Token: 0x060024B8 RID: 9400 RVA: 0x00082B94 File Offset: 0x00080D94
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
		{
			return this._tb.GetEvent(name, bindingAttr);
		}

		// Token: 0x060024B9 RID: 9401 RVA: 0x00082BA4 File Offset: 0x00080DA4
		public override EventInfo[] GetEvents()
		{
			return this._tb.GetEvents();
		}

		// Token: 0x060024BA RID: 9402 RVA: 0x00082BB4 File Offset: 0x00080DB4
		public override EventInfo[] GetEvents(BindingFlags bindingAttr)
		{
			return this._tb.GetEvents(bindingAttr);
		}

		// Token: 0x060024BB RID: 9403 RVA: 0x00082BC4 File Offset: 0x00080DC4
		public override FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			return this._tb.GetField(name, bindingAttr);
		}

		// Token: 0x060024BC RID: 9404 RVA: 0x00082BD4 File Offset: 0x00080DD4
		public override FieldInfo[] GetFields(BindingFlags bindingAttr)
		{
			return this._tb.GetFields(bindingAttr);
		}

		// Token: 0x060024BD RID: 9405 RVA: 0x00082BE4 File Offset: 0x00080DE4
		public override Type GetInterface(string name, bool ignoreCase)
		{
			return this._tb.GetInterface(name, ignoreCase);
		}

		// Token: 0x060024BE RID: 9406 RVA: 0x00082BF4 File Offset: 0x00080DF4
		[ComVisible(true)]
		public override InterfaceMapping GetInterfaceMap(Type interfaceType)
		{
			return this._tb.GetInterfaceMap(interfaceType);
		}

		// Token: 0x060024BF RID: 9407 RVA: 0x00082C04 File Offset: 0x00080E04
		public override Type[] GetInterfaces()
		{
			return this._tb.GetInterfaces();
		}

		// Token: 0x060024C0 RID: 9408 RVA: 0x00082C14 File Offset: 0x00080E14
		public override MemberInfo[] GetMember(string name, MemberTypes type, BindingFlags bindingAttr)
		{
			return this._tb.GetMember(name, type, bindingAttr);
		}

		// Token: 0x060024C1 RID: 9409 RVA: 0x00082C24 File Offset: 0x00080E24
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
		{
			return this._tb.GetMembers(bindingAttr);
		}

		// Token: 0x060024C2 RID: 9410 RVA: 0x00082C34 File Offset: 0x00080E34
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			if (types == null)
			{
				return this._tb.GetMethod(name, bindingAttr);
			}
			return this._tb.GetMethod(name, bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x060024C3 RID: 9411 RVA: 0x00082C6C File Offset: 0x00080E6C
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
		{
			return this._tb.GetMethods(bindingAttr);
		}

		// Token: 0x060024C4 RID: 9412 RVA: 0x00082C7C File Offset: 0x00080E7C
		public override Type GetNestedType(string name, BindingFlags bindingAttr)
		{
			return this._tb.GetNestedType(name, bindingAttr);
		}

		// Token: 0x060024C5 RID: 9413 RVA: 0x00082C8C File Offset: 0x00080E8C
		public override Type[] GetNestedTypes(BindingFlags bindingAttr)
		{
			return this._tb.GetNestedTypes(bindingAttr);
		}

		// Token: 0x060024C6 RID: 9414 RVA: 0x00082C9C File Offset: 0x00080E9C
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
		{
			return this._tb.GetProperties(bindingAttr);
		}

		// Token: 0x060024C7 RID: 9415 RVA: 0x00082CAC File Offset: 0x00080EAC
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			throw this.CreateNotSupportedException();
		}

		// Token: 0x060024C8 RID: 9416 RVA: 0x00082CB4 File Offset: 0x00080EB4
		protected override bool HasElementTypeImpl()
		{
			return this._tb.HasElementType;
		}

		// Token: 0x060024C9 RID: 9417 RVA: 0x00082CC4 File Offset: 0x00080EC4
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
		{
			return this._tb.InvokeMember(name, invokeAttr, binder, target, args, modifiers, culture, namedParameters);
		}

		// Token: 0x060024CA RID: 9418 RVA: 0x00082CEC File Offset: 0x00080EEC
		protected override bool IsArrayImpl()
		{
			return false;
		}

		// Token: 0x060024CB RID: 9419 RVA: 0x00082CF0 File Offset: 0x00080EF0
		protected override bool IsByRefImpl()
		{
			return false;
		}

		// Token: 0x060024CC RID: 9420 RVA: 0x00082CF4 File Offset: 0x00080EF4
		protected override bool IsCOMObjectImpl()
		{
			return false;
		}

		// Token: 0x060024CD RID: 9421 RVA: 0x00082CF8 File Offset: 0x00080EF8
		protected override bool IsPointerImpl()
		{
			return false;
		}

		// Token: 0x060024CE RID: 9422 RVA: 0x00082CFC File Offset: 0x00080EFC
		protected override bool IsPrimitiveImpl()
		{
			return false;
		}

		// Token: 0x060024CF RID: 9423 RVA: 0x00082D00 File Offset: 0x00080F00
		protected override bool IsValueTypeImpl()
		{
			return true;
		}

		// Token: 0x060024D0 RID: 9424 RVA: 0x00082D04 File Offset: 0x00080F04
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return this._tb.IsDefined(attributeType, inherit);
		}

		// Token: 0x060024D1 RID: 9425 RVA: 0x00082D14 File Offset: 0x00080F14
		public override Type MakeArrayType()
		{
			return new ArrayType(this, 0);
		}

		// Token: 0x060024D2 RID: 9426 RVA: 0x00082D20 File Offset: 0x00080F20
		public override Type MakeArrayType(int rank)
		{
			if (rank < 1)
			{
				throw new IndexOutOfRangeException();
			}
			return new ArrayType(this, rank);
		}

		// Token: 0x060024D3 RID: 9427 RVA: 0x00082D38 File Offset: 0x00080F38
		public override Type MakeByRefType()
		{
			return new ByRefType(this);
		}

		// Token: 0x060024D4 RID: 9428 RVA: 0x00082D40 File Offset: 0x00080F40
		public override Type MakePointerType()
		{
			return new PointerType(this);
		}

		// Token: 0x060024D5 RID: 9429 RVA: 0x00082D48 File Offset: 0x00080F48
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			this._tb.SetCustomAttribute(customBuilder);
		}

		// Token: 0x060024D6 RID: 9430 RVA: 0x00082D58 File Offset: 0x00080F58
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x060024D7 RID: 9431 RVA: 0x00082D68 File Offset: 0x00080F68
		private Exception CreateNotSupportedException()
		{
			return new NotSupportedException("The invoked member is not supported in a dynamic module.");
		}

		// Token: 0x04000DD3 RID: 3539
		private TypeBuilder _tb;

		// Token: 0x04000DD4 RID: 3540
		private FieldBuilder _underlyingField;

		// Token: 0x04000DD5 RID: 3541
		private Type _underlyingType;
	}
}
