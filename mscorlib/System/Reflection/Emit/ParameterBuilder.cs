﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002F4 RID: 756
	[ComDefaultInterface(typeof(_ParameterBuilder))]
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	public class ParameterBuilder : _ParameterBuilder
	{
		// Token: 0x060026B9 RID: 9913 RVA: 0x0008A540 File Offset: 0x00088740
		internal ParameterBuilder(MethodBase mb, int pos, ParameterAttributes attributes, string strParamName)
		{
			this.name = strParamName;
			this.position = pos;
			this.attrs = attributes;
			this.methodb = mb;
			if (mb is DynamicMethod)
			{
				this.table_idx = 0;
			}
			else
			{
				this.table_idx = mb.get_next_table_index(this, 8, true);
			}
		}

		// Token: 0x060026BA RID: 9914 RVA: 0x0008A598 File Offset: 0x00088798
		void _ParameterBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060026BB RID: 9915 RVA: 0x0008A5A0 File Offset: 0x000887A0
		void _ParameterBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060026BC RID: 9916 RVA: 0x0008A5A8 File Offset: 0x000887A8
		void _ParameterBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060026BD RID: 9917 RVA: 0x0008A5B0 File Offset: 0x000887B0
		void _ParameterBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x170006E6 RID: 1766
		// (get) Token: 0x060026BE RID: 9918 RVA: 0x0008A5B8 File Offset: 0x000887B8
		public virtual int Attributes
		{
			get
			{
				return (int)this.attrs;
			}
		}

		// Token: 0x170006E7 RID: 1767
		// (get) Token: 0x060026BF RID: 9919 RVA: 0x0008A5C0 File Offset: 0x000887C0
		public bool IsIn
		{
			get
			{
				return (this.attrs & ParameterAttributes.In) != ParameterAttributes.None;
			}
		}

		// Token: 0x170006E8 RID: 1768
		// (get) Token: 0x060026C0 RID: 9920 RVA: 0x0008A5D0 File Offset: 0x000887D0
		public bool IsOut
		{
			get
			{
				return (this.attrs & ParameterAttributes.Out) != ParameterAttributes.None;
			}
		}

		// Token: 0x170006E9 RID: 1769
		// (get) Token: 0x060026C1 RID: 9921 RVA: 0x0008A5E0 File Offset: 0x000887E0
		public bool IsOptional
		{
			get
			{
				return (this.attrs & ParameterAttributes.Optional) != ParameterAttributes.None;
			}
		}

		// Token: 0x170006EA RID: 1770
		// (get) Token: 0x060026C2 RID: 9922 RVA: 0x0008A5F4 File Offset: 0x000887F4
		public virtual string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170006EB RID: 1771
		// (get) Token: 0x060026C3 RID: 9923 RVA: 0x0008A5FC File Offset: 0x000887FC
		public virtual int Position
		{
			get
			{
				return this.position;
			}
		}

		// Token: 0x060026C4 RID: 9924 RVA: 0x0008A604 File Offset: 0x00088804
		public virtual ParameterToken GetToken()
		{
			return new ParameterToken(8 | this.table_idx);
		}

		// Token: 0x060026C5 RID: 9925 RVA: 0x0008A614 File Offset: 0x00088814
		public virtual void SetConstant(object defaultValue)
		{
			this.def_value = defaultValue;
			this.attrs |= ParameterAttributes.HasDefault;
		}

		// Token: 0x060026C6 RID: 9926 RVA: 0x0008A630 File Offset: 0x00088830
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			string fullName = customBuilder.Ctor.ReflectedType.FullName;
			if (fullName == "System.Runtime.InteropServices.InAttribute")
			{
				this.attrs |= ParameterAttributes.In;
				return;
			}
			if (fullName == "System.Runtime.InteropServices.OutAttribute")
			{
				this.attrs |= ParameterAttributes.Out;
				return;
			}
			if (fullName == "System.Runtime.InteropServices.OptionalAttribute")
			{
				this.attrs |= ParameterAttributes.Optional;
				return;
			}
			if (fullName == "System.Runtime.InteropServices.MarshalAsAttribute")
			{
				this.attrs |= ParameterAttributes.HasFieldMarshal;
				this.marshal_info = CustomAttributeBuilder.get_umarshal(customBuilder, false);
				return;
			}
			if (fullName == "System.Runtime.InteropServices.DefaultParameterValueAttribute")
			{
				this.SetConstant(CustomAttributeBuilder.decode_cattr(customBuilder).ctorArgs[0]);
				return;
			}
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x060026C7 RID: 9927 RVA: 0x0008A758 File Offset: 0x00088958
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x060026C8 RID: 9928 RVA: 0x0008A768 File Offset: 0x00088968
		[Obsolete("An alternate API is available: Emit the MarshalAs custom attribute instead.")]
		public virtual void SetMarshal(UnmanagedMarshal unmanagedMarshal)
		{
			this.marshal_info = unmanagedMarshal;
			this.attrs |= ParameterAttributes.HasFieldMarshal;
		}

		// Token: 0x04000F90 RID: 3984
		private MethodBase methodb;

		// Token: 0x04000F91 RID: 3985
		private string name;

		// Token: 0x04000F92 RID: 3986
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000F93 RID: 3987
		private UnmanagedMarshal marshal_info;

		// Token: 0x04000F94 RID: 3988
		private ParameterAttributes attrs;

		// Token: 0x04000F95 RID: 3989
		private int position;

		// Token: 0x04000F96 RID: 3990
		private int table_idx;

		// Token: 0x04000F97 RID: 3991
		private object def_value;
	}
}
