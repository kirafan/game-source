﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Reflection.Emit
{
	// Token: 0x020002EA RID: 746
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_MethodRental))]
	[ClassInterface(ClassInterfaceType.None)]
	public sealed class MethodRental : _MethodRental
	{
		// Token: 0x0600263C RID: 9788 RVA: 0x000872C0 File Offset: 0x000854C0
		private MethodRental()
		{
		}

		// Token: 0x0600263D RID: 9789 RVA: 0x000872C8 File Offset: 0x000854C8
		void _MethodRental.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600263E RID: 9790 RVA: 0x000872D0 File Offset: 0x000854D0
		void _MethodRental.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600263F RID: 9791 RVA: 0x000872D8 File Offset: 0x000854D8
		void _MethodRental.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002640 RID: 9792 RVA: 0x000872E0 File Offset: 0x000854E0
		void _MethodRental.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002641 RID: 9793 RVA: 0x000872E8 File Offset: 0x000854E8
		[MonoTODO]
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public static void SwapMethodBody(Type cls, int methodtoken, IntPtr rgIL, int methodSize, int flags)
		{
			if (methodSize <= 0 || methodSize >= 4128768)
			{
				throw new ArgumentException("Data size must be > 0 and < 0x3f0000", "methodSize");
			}
			if (cls == null)
			{
				throw new ArgumentNullException("cls");
			}
			if (cls is TypeBuilder && !((TypeBuilder)cls).is_created)
			{
				throw new NotSupportedException("Type '" + cls + "' is not yet created.");
			}
			throw new NotImplementedException();
		}

		// Token: 0x04000E62 RID: 3682
		public const int JitImmediate = 1;

		// Token: 0x04000E63 RID: 3683
		public const int JitOnDemand = 0;
	}
}
