﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002DA RID: 730
	[ComVisible(true)]
	[Serializable]
	public enum FlowControl
	{
		// Token: 0x04000DF6 RID: 3574
		Branch,
		// Token: 0x04000DF7 RID: 3575
		Break,
		// Token: 0x04000DF8 RID: 3576
		Call,
		// Token: 0x04000DF9 RID: 3577
		Cond_Branch,
		// Token: 0x04000DFA RID: 3578
		Meta,
		// Token: 0x04000DFB RID: 3579
		Next,
		// Token: 0x04000DFC RID: 3580
		[Obsolete("This API has been deprecated.")]
		Phi,
		// Token: 0x04000DFD RID: 3581
		Return,
		// Token: 0x04000DFE RID: 3582
		Throw
	}
}
