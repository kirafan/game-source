﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002F2 RID: 754
	[ComVisible(true)]
	[Serializable]
	public enum OperandType
	{
		// Token: 0x04000F74 RID: 3956
		InlineBrTarget,
		// Token: 0x04000F75 RID: 3957
		InlineField,
		// Token: 0x04000F76 RID: 3958
		InlineI,
		// Token: 0x04000F77 RID: 3959
		InlineI8,
		// Token: 0x04000F78 RID: 3960
		InlineMethod,
		// Token: 0x04000F79 RID: 3961
		InlineNone,
		// Token: 0x04000F7A RID: 3962
		[Obsolete("This API has been deprecated.")]
		InlinePhi,
		// Token: 0x04000F7B RID: 3963
		InlineR,
		// Token: 0x04000F7C RID: 3964
		InlineSig = 9,
		// Token: 0x04000F7D RID: 3965
		InlineString,
		// Token: 0x04000F7E RID: 3966
		InlineSwitch,
		// Token: 0x04000F7F RID: 3967
		InlineTok,
		// Token: 0x04000F80 RID: 3968
		InlineType,
		// Token: 0x04000F81 RID: 3969
		InlineVar,
		// Token: 0x04000F82 RID: 3970
		ShortInlineBrTarget,
		// Token: 0x04000F83 RID: 3971
		ShortInlineI,
		// Token: 0x04000F84 RID: 3972
		ShortInlineR,
		// Token: 0x04000F85 RID: 3973
		ShortInlineVar
	}
}
