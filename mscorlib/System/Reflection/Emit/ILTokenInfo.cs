﻿using System;

namespace System.Reflection.Emit
{
	// Token: 0x020002DE RID: 734
	internal struct ILTokenInfo
	{
		// Token: 0x04000E15 RID: 3605
		public MemberInfo member;

		// Token: 0x04000E16 RID: 3606
		public int code_pos;
	}
}
