﻿using System;

namespace System.Reflection.Emit
{
	// Token: 0x020002E4 RID: 740
	internal struct SequencePoint
	{
		// Token: 0x04000E36 RID: 3638
		public int Offset;

		// Token: 0x04000E37 RID: 3639
		public int Line;

		// Token: 0x04000E38 RID: 3640
		public int Col;

		// Token: 0x04000E39 RID: 3641
		public int EndLine;

		// Token: 0x04000E3A RID: 3642
		public int EndCol;
	}
}
