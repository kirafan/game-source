﻿using System;
using System.Diagnostics.SymbolStore;
using System.Globalization;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;
using System.Text;

namespace System.Reflection.Emit
{
	// Token: 0x020002E7 RID: 743
	[ComDefaultInterface(typeof(_MethodBuilder))]
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	public sealed class MethodBuilder : MethodInfo, _MethodBuilder
	{
		// Token: 0x060025DB RID: 9691 RVA: 0x00086018 File Offset: 0x00084218
		internal MethodBuilder(TypeBuilder tb, string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] returnModReq, Type[] returnModOpt, Type[] parameterTypes, Type[][] paramModReq, Type[][] paramModOpt)
		{
			this.name = name;
			this.attrs = attributes;
			this.call_conv = callingConvention;
			this.rtype = returnType;
			this.returnModReq = returnModReq;
			this.returnModOpt = returnModOpt;
			this.paramModReq = paramModReq;
			this.paramModOpt = paramModOpt;
			if ((attributes & MethodAttributes.Static) == MethodAttributes.PrivateScope)
			{
				this.call_conv |= CallingConventions.HasThis;
			}
			if (parameterTypes != null)
			{
				for (int i = 0; i < parameterTypes.Length; i++)
				{
					if (parameterTypes[i] == null)
					{
						throw new ArgumentException("Elements of the parameterTypes array cannot be null", "parameterTypes");
					}
				}
				this.parameters = new Type[parameterTypes.Length];
				Array.Copy(parameterTypes, this.parameters, parameterTypes.Length);
			}
			this.type = tb;
			this.table_idx = this.get_next_table_index(this, 6, true);
			((ModuleBuilder)tb.Module).RegisterToken(this, this.GetToken().Token);
		}

		// Token: 0x060025DC RID: 9692 RVA: 0x00086114 File Offset: 0x00084314
		internal MethodBuilder(TypeBuilder tb, string name, MethodAttributes attributes, CallingConventions callingConvention, Type returnType, Type[] returnModReq, Type[] returnModOpt, Type[] parameterTypes, Type[][] paramModReq, Type[][] paramModOpt, string dllName, string entryName, CallingConvention nativeCConv, CharSet nativeCharset) : this(tb, name, attributes, callingConvention, returnType, returnModReq, returnModOpt, parameterTypes, paramModReq, paramModOpt)
		{
			this.pi_dll = dllName;
			this.pi_entry = entryName;
			this.native_cc = nativeCConv;
			this.charset = nativeCharset;
		}

		// Token: 0x060025DD RID: 9693 RVA: 0x00086158 File Offset: 0x00084358
		void _MethodBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060025DE RID: 9694 RVA: 0x00086160 File Offset: 0x00084360
		void _MethodBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060025DF RID: 9695 RVA: 0x00086168 File Offset: 0x00084368
		void _MethodBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060025E0 RID: 9696 RVA: 0x00086170 File Offset: 0x00084370
		void _MethodBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x170006B4 RID: 1716
		// (get) Token: 0x060025E1 RID: 9697 RVA: 0x00086178 File Offset: 0x00084378
		public override bool ContainsGenericParameters
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x170006B5 RID: 1717
		// (get) Token: 0x060025E2 RID: 9698 RVA: 0x00086180 File Offset: 0x00084380
		// (set) Token: 0x060025E3 RID: 9699 RVA: 0x00086188 File Offset: 0x00084388
		public bool InitLocals
		{
			get
			{
				return this.init_locals;
			}
			set
			{
				this.init_locals = value;
			}
		}

		// Token: 0x170006B6 RID: 1718
		// (get) Token: 0x060025E4 RID: 9700 RVA: 0x00086194 File Offset: 0x00084394
		internal TypeBuilder TypeBuilder
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x170006B7 RID: 1719
		// (get) Token: 0x060025E5 RID: 9701 RVA: 0x0008619C File Offset: 0x0008439C
		public override RuntimeMethodHandle MethodHandle
		{
			get
			{
				throw this.NotSupported();
			}
		}

		// Token: 0x170006B8 RID: 1720
		// (get) Token: 0x060025E6 RID: 9702 RVA: 0x000861A4 File Offset: 0x000843A4
		public override Type ReturnType
		{
			get
			{
				return this.rtype;
			}
		}

		// Token: 0x170006B9 RID: 1721
		// (get) Token: 0x060025E7 RID: 9703 RVA: 0x000861AC File Offset: 0x000843AC
		public override Type ReflectedType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x170006BA RID: 1722
		// (get) Token: 0x060025E8 RID: 9704 RVA: 0x000861B4 File Offset: 0x000843B4
		public override Type DeclaringType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x170006BB RID: 1723
		// (get) Token: 0x060025E9 RID: 9705 RVA: 0x000861BC File Offset: 0x000843BC
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x170006BC RID: 1724
		// (get) Token: 0x060025EA RID: 9706 RVA: 0x000861C4 File Offset: 0x000843C4
		public override MethodAttributes Attributes
		{
			get
			{
				return this.attrs;
			}
		}

		// Token: 0x170006BD RID: 1725
		// (get) Token: 0x060025EB RID: 9707 RVA: 0x000861CC File Offset: 0x000843CC
		public override ICustomAttributeProvider ReturnTypeCustomAttributes
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170006BE RID: 1726
		// (get) Token: 0x060025EC RID: 9708 RVA: 0x000861D0 File Offset: 0x000843D0
		public override CallingConventions CallingConvention
		{
			get
			{
				return this.call_conv;
			}
		}

		// Token: 0x170006BF RID: 1727
		// (get) Token: 0x060025ED RID: 9709 RVA: 0x000861D8 File Offset: 0x000843D8
		[MonoTODO("Not implemented")]
		public string Signature
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170006C0 RID: 1728
		// (set) Token: 0x060025EE RID: 9710 RVA: 0x000861E0 File Offset: 0x000843E0
		internal bool BestFitMapping
		{
			set
			{
				this.extra_flags = (uint)(((ulong)this.extra_flags & 18446744073709551567UL) | ((!value) ? 32UL : 16UL));
			}
		}

		// Token: 0x170006C1 RID: 1729
		// (set) Token: 0x060025EF RID: 9711 RVA: 0x00086208 File Offset: 0x00084408
		internal bool ThrowOnUnmappableChar
		{
			set
			{
				this.extra_flags = (uint)(((ulong)this.extra_flags & 18446744073709539327UL) | ((!value) ? 8192UL : 4096UL));
			}
		}

		// Token: 0x170006C2 RID: 1730
		// (set) Token: 0x060025F0 RID: 9712 RVA: 0x00086244 File Offset: 0x00084444
		internal bool ExactSpelling
		{
			set
			{
				this.extra_flags = (uint)(((ulong)this.extra_flags & 18446744073709551614UL) | ((!value) ? 0UL : 1UL));
			}
		}

		// Token: 0x170006C3 RID: 1731
		// (set) Token: 0x060025F1 RID: 9713 RVA: 0x00086268 File Offset: 0x00084468
		internal bool SetLastError
		{
			set
			{
				this.extra_flags = (uint)(((ulong)this.extra_flags & 18446744073709551551UL) | ((!value) ? 0UL : 64UL));
			}
		}

		// Token: 0x060025F2 RID: 9714 RVA: 0x00086298 File Offset: 0x00084498
		public MethodToken GetToken()
		{
			return new MethodToken(100663296 | this.table_idx);
		}

		// Token: 0x060025F3 RID: 9715 RVA: 0x000862AC File Offset: 0x000844AC
		public override MethodInfo GetBaseDefinition()
		{
			return this;
		}

		// Token: 0x060025F4 RID: 9716 RVA: 0x000862B0 File Offset: 0x000844B0
		public override MethodImplAttributes GetMethodImplementationFlags()
		{
			return this.iattrs;
		}

		// Token: 0x060025F5 RID: 9717 RVA: 0x000862B8 File Offset: 0x000844B8
		public override ParameterInfo[] GetParameters()
		{
			if (!this.type.is_created)
			{
				throw this.NotSupported();
			}
			if (this.parameters == null)
			{
				return null;
			}
			ParameterInfo[] array = new ParameterInfo[this.parameters.Length];
			for (int i = 0; i < this.parameters.Length; i++)
			{
				array[i] = new ParameterInfo((this.pinfo != null) ? this.pinfo[i + 1] : null, this.parameters[i], this, i + 1);
			}
			return array;
		}

		// Token: 0x060025F6 RID: 9718 RVA: 0x00086340 File Offset: 0x00084540
		internal override int GetParameterCount()
		{
			if (this.parameters == null)
			{
				return 0;
			}
			return this.parameters.Length;
		}

		// Token: 0x060025F7 RID: 9719 RVA: 0x00086358 File Offset: 0x00084558
		public Module GetModule()
		{
			return this.type.Module;
		}

		// Token: 0x060025F8 RID: 9720 RVA: 0x00086368 File Offset: 0x00084568
		public void CreateMethodBody(byte[] il, int count)
		{
			if (il != null && (count < 0 || count > il.Length))
			{
				throw new ArgumentOutOfRangeException("Index was out of range.  Must be non-negative and less than the size of the collection.");
			}
			if (this.code != null || this.type.is_created)
			{
				throw new InvalidOperationException("Type definition of the method is complete.");
			}
			if (il == null)
			{
				this.code = null;
			}
			else
			{
				this.code = new byte[count];
				Array.Copy(il, this.code, count);
			}
		}

		// Token: 0x060025F9 RID: 9721 RVA: 0x000863E8 File Offset: 0x000845E8
		public override object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			throw this.NotSupported();
		}

		// Token: 0x060025FA RID: 9722 RVA: 0x000863F0 File Offset: 0x000845F0
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			throw this.NotSupported();
		}

		// Token: 0x060025FB RID: 9723 RVA: 0x000863F8 File Offset: 0x000845F8
		public override object[] GetCustomAttributes(bool inherit)
		{
			if (this.type.is_created)
			{
				return MonoCustomAttrs.GetCustomAttributes(this, inherit);
			}
			throw this.NotSupported();
		}

		// Token: 0x060025FC RID: 9724 RVA: 0x00086418 File Offset: 0x00084618
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			if (this.type.is_created)
			{
				return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
			}
			throw this.NotSupported();
		}

		// Token: 0x060025FD RID: 9725 RVA: 0x0008643C File Offset: 0x0008463C
		public ILGenerator GetILGenerator()
		{
			return this.GetILGenerator(64);
		}

		// Token: 0x060025FE RID: 9726 RVA: 0x00086448 File Offset: 0x00084648
		public ILGenerator GetILGenerator(int size)
		{
			if ((this.iattrs & MethodImplAttributes.CodeTypeMask) != MethodImplAttributes.IL || (this.iattrs & MethodImplAttributes.ManagedMask) != MethodImplAttributes.IL)
			{
				throw new InvalidOperationException("Method body should not exist.");
			}
			if (this.ilgen != null)
			{
				return this.ilgen;
			}
			this.ilgen = new ILGenerator(this.type.Module, ((ModuleBuilder)this.type.Module).GetTokenGenerator(), size);
			return this.ilgen;
		}

		// Token: 0x060025FF RID: 9727 RVA: 0x000864C0 File Offset: 0x000846C0
		public ParameterBuilder DefineParameter(int position, ParameterAttributes attributes, string strParamName)
		{
			this.RejectIfCreated();
			if (position < 0 || position > this.parameters.Length)
			{
				throw new ArgumentOutOfRangeException("position");
			}
			ParameterBuilder parameterBuilder = new ParameterBuilder(this, position, attributes, strParamName);
			if (this.pinfo == null)
			{
				this.pinfo = new ParameterBuilder[this.parameters.Length + 1];
			}
			this.pinfo[position] = parameterBuilder;
			return parameterBuilder;
		}

		// Token: 0x06002600 RID: 9728 RVA: 0x00086528 File Offset: 0x00084728
		internal void check_override()
		{
			if (this.override_method != null && this.override_method.IsVirtual && !this.IsVirtual)
			{
				throw new TypeLoadException(string.Format("Method '{0}' override '{1}' but it is not virtual", this.name, this.override_method));
			}
		}

		// Token: 0x06002601 RID: 9729 RVA: 0x00086578 File Offset: 0x00084778
		internal void fixup()
		{
			if ((this.attrs & (MethodAttributes.Abstract | MethodAttributes.PinvokeImpl)) == MethodAttributes.PrivateScope && (this.iattrs & (MethodImplAttributes)4099) == MethodImplAttributes.IL && (this.ilgen == null || ILGenerator.Mono_GetCurrentOffset(this.ilgen) == 0) && (this.code == null || this.code.Length == 0))
			{
				throw new InvalidOperationException(string.Format("Method '{0}.{1}' does not have a method body.", this.DeclaringType.FullName, this.Name));
			}
			if (this.ilgen != null)
			{
				this.ilgen.label_fixup();
			}
		}

		// Token: 0x06002602 RID: 9730 RVA: 0x00086614 File Offset: 0x00084814
		internal void GenerateDebugInfo(ISymbolWriter symbolWriter)
		{
			if (this.ilgen != null && this.ilgen.HasDebugInfo)
			{
				SymbolToken symbolToken = new SymbolToken(this.GetToken().Token);
				symbolWriter.OpenMethod(symbolToken);
				symbolWriter.SetSymAttribute(symbolToken, "__name", Encoding.UTF8.GetBytes(this.Name));
				this.ilgen.GenerateDebugInfo(symbolWriter);
				symbolWriter.CloseMethod();
			}
		}

		// Token: 0x06002603 RID: 9731 RVA: 0x00086688 File Offset: 0x00084888
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			if (customBuilder == null)
			{
				throw new ArgumentNullException("customBuilder");
			}
			string fullName = customBuilder.Ctor.ReflectedType.FullName;
			switch (fullName)
			{
			case "System.Runtime.CompilerServices.MethodImplAttribute":
			{
				byte[] data = customBuilder.Data;
				int num2 = (int)data[2];
				num2 |= (int)data[3] << 8;
				this.iattrs |= (MethodImplAttributes)num2;
				return;
			}
			case "System.Runtime.InteropServices.DllImportAttribute":
			{
				CustomAttributeBuilder.CustomAttributeInfo customAttributeInfo = CustomAttributeBuilder.decode_cattr(customBuilder);
				bool flag = true;
				this.pi_dll = (string)customAttributeInfo.ctorArgs[0];
				if (this.pi_dll == null || this.pi_dll.Length == 0)
				{
					throw new ArgumentException("DllName cannot be empty");
				}
				this.native_cc = System.Runtime.InteropServices.CallingConvention.Winapi;
				for (int i = 0; i < customAttributeInfo.namedParamNames.Length; i++)
				{
					string a = customAttributeInfo.namedParamNames[i];
					object obj = customAttributeInfo.namedParamValues[i];
					if (a == "CallingConvention")
					{
						this.native_cc = (CallingConvention)((int)obj);
					}
					else if (a == "CharSet")
					{
						this.charset = (CharSet)((int)obj);
					}
					else if (a == "EntryPoint")
					{
						this.pi_entry = (string)obj;
					}
					else if (a == "ExactSpelling")
					{
						this.ExactSpelling = (bool)obj;
					}
					else if (a == "SetLastError")
					{
						this.SetLastError = (bool)obj;
					}
					else if (a == "PreserveSig")
					{
						flag = (bool)obj;
					}
					else if (a == "BestFitMapping")
					{
						this.BestFitMapping = (bool)obj;
					}
					else if (a == "ThrowOnUnmappableChar")
					{
						this.ThrowOnUnmappableChar = (bool)obj;
					}
				}
				this.attrs |= MethodAttributes.PinvokeImpl;
				if (flag)
				{
					this.iattrs |= MethodImplAttributes.PreserveSig;
				}
				return;
			}
			case "System.Runtime.InteropServices.PreserveSigAttribute":
				this.iattrs |= MethodImplAttributes.PreserveSig;
				return;
			case "System.Runtime.CompilerServices.SpecialNameAttribute":
				this.attrs |= MethodAttributes.SpecialName;
				return;
			case "System.Security.SuppressUnmanagedCodeSecurityAttribute":
				this.attrs |= MethodAttributes.HasSecurity;
				break;
			}
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x06002604 RID: 9732 RVA: 0x000869BC File Offset: 0x00084BBC
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			if (con == null)
			{
				throw new ArgumentNullException("con");
			}
			if (binaryAttribute == null)
			{
				throw new ArgumentNullException("binaryAttribute");
			}
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x06002605 RID: 9733 RVA: 0x000869F0 File Offset: 0x00084BF0
		public void SetImplementationFlags(MethodImplAttributes attributes)
		{
			this.RejectIfCreated();
			this.iattrs = attributes;
		}

		// Token: 0x06002606 RID: 9734 RVA: 0x00086A00 File Offset: 0x00084C00
		public void AddDeclarativeSecurity(SecurityAction action, PermissionSet pset)
		{
			if (pset == null)
			{
				throw new ArgumentNullException("pset");
			}
			if (action == SecurityAction.RequestMinimum || action == SecurityAction.RequestOptional || action == SecurityAction.RequestRefuse)
			{
				throw new ArgumentOutOfRangeException("Request* values are not permitted", "action");
			}
			this.RejectIfCreated();
			if (this.permissions != null)
			{
				foreach (RefEmitPermissionSet refEmitPermissionSet in this.permissions)
				{
					if (refEmitPermissionSet.action == action)
					{
						throw new InvalidOperationException("Multiple permission sets specified with the same SecurityAction.");
					}
				}
				RefEmitPermissionSet[] array2 = new RefEmitPermissionSet[this.permissions.Length + 1];
				this.permissions.CopyTo(array2, 0);
				this.permissions = array2;
			}
			else
			{
				this.permissions = new RefEmitPermissionSet[1];
			}
			this.permissions[this.permissions.Length - 1] = new RefEmitPermissionSet(action, pset.ToXml().ToString());
			this.attrs |= MethodAttributes.HasSecurity;
		}

		// Token: 0x06002607 RID: 9735 RVA: 0x00086B08 File Offset: 0x00084D08
		[Obsolete("An alternate API is available: Emit the MarshalAs custom attribute instead.")]
		public void SetMarshal(UnmanagedMarshal unmanagedMarshal)
		{
			this.RejectIfCreated();
			throw new NotImplementedException();
		}

		// Token: 0x06002608 RID: 9736 RVA: 0x00086B18 File Offset: 0x00084D18
		[MonoTODO]
		public void SetSymCustomAttribute(string name, byte[] data)
		{
			this.RejectIfCreated();
			throw new NotImplementedException();
		}

		// Token: 0x06002609 RID: 9737 RVA: 0x00086B28 File Offset: 0x00084D28
		public override string ToString()
		{
			return string.Concat(new string[]
			{
				"MethodBuilder [",
				this.type.Name,
				"::",
				this.name,
				"]"
			});
		}

		// Token: 0x0600260A RID: 9738 RVA: 0x00086B70 File Offset: 0x00084D70
		[MonoTODO]
		public override bool Equals(object obj)
		{
			return base.Equals(obj);
		}

		// Token: 0x0600260B RID: 9739 RVA: 0x00086B7C File Offset: 0x00084D7C
		public override int GetHashCode()
		{
			return this.name.GetHashCode();
		}

		// Token: 0x0600260C RID: 9740 RVA: 0x00086B8C File Offset: 0x00084D8C
		internal override int get_next_table_index(object obj, int table, bool inc)
		{
			return this.type.get_next_table_index(obj, table, inc);
		}

		// Token: 0x0600260D RID: 9741 RVA: 0x00086B9C File Offset: 0x00084D9C
		internal void set_override(MethodInfo mdecl)
		{
			this.override_method = mdecl;
		}

		// Token: 0x0600260E RID: 9742 RVA: 0x00086BA8 File Offset: 0x00084DA8
		private void RejectIfCreated()
		{
			if (this.type.is_created)
			{
				throw new InvalidOperationException("Type definition of the method is complete.");
			}
		}

		// Token: 0x0600260F RID: 9743 RVA: 0x00086BC8 File Offset: 0x00084DC8
		private Exception NotSupported()
		{
			return new NotSupportedException("The invoked member is not supported in a dynamic module.");
		}

		// Token: 0x06002610 RID: 9744
		[MethodImpl(MethodImplOptions.InternalCall)]
		public override extern MethodInfo MakeGenericMethod(params Type[] typeArguments);

		// Token: 0x170006C4 RID: 1732
		// (get) Token: 0x06002611 RID: 9745 RVA: 0x00086BD4 File Offset: 0x00084DD4
		public override bool IsGenericMethodDefinition
		{
			get
			{
				return this.generic_params != null;
			}
		}

		// Token: 0x170006C5 RID: 1733
		// (get) Token: 0x06002612 RID: 9746 RVA: 0x00086BE4 File Offset: 0x00084DE4
		public override bool IsGenericMethod
		{
			get
			{
				return this.generic_params != null;
			}
		}

		// Token: 0x06002613 RID: 9747 RVA: 0x00086BF4 File Offset: 0x00084DF4
		public override MethodInfo GetGenericMethodDefinition()
		{
			if (!this.IsGenericMethodDefinition)
			{
				throw new InvalidOperationException();
			}
			return this;
		}

		// Token: 0x06002614 RID: 9748 RVA: 0x00086C08 File Offset: 0x00084E08
		public override Type[] GetGenericArguments()
		{
			if (this.generic_params == null)
			{
				return Type.EmptyTypes;
			}
			Type[] array = new Type[this.generic_params.Length];
			for (int i = 0; i < this.generic_params.Length; i++)
			{
				array[i] = this.generic_params[i];
			}
			return array;
		}

		// Token: 0x06002615 RID: 9749 RVA: 0x00086C5C File Offset: 0x00084E5C
		public GenericTypeParameterBuilder[] DefineGenericParameters(params string[] names)
		{
			if (names == null)
			{
				throw new ArgumentNullException("names");
			}
			if (names.Length == 0)
			{
				throw new ArgumentException("names");
			}
			this.generic_params = new GenericTypeParameterBuilder[names.Length];
			for (int i = 0; i < names.Length; i++)
			{
				string text = names[i];
				if (text == null)
				{
					throw new ArgumentNullException("names");
				}
				this.generic_params[i] = new GenericTypeParameterBuilder(this.type, this, text, i);
			}
			return this.generic_params;
		}

		// Token: 0x06002616 RID: 9750 RVA: 0x00086CE0 File Offset: 0x00084EE0
		public void SetReturnType(Type returnType)
		{
			this.rtype = returnType;
		}

		// Token: 0x06002617 RID: 9751 RVA: 0x00086CEC File Offset: 0x00084EEC
		public void SetParameters(params Type[] parameterTypes)
		{
			if (parameterTypes != null)
			{
				for (int i = 0; i < parameterTypes.Length; i++)
				{
					if (parameterTypes[i] == null)
					{
						throw new ArgumentException("Elements of the parameterTypes array cannot be null", "parameterTypes");
					}
				}
				this.parameters = new Type[parameterTypes.Length];
				Array.Copy(parameterTypes, this.parameters, parameterTypes.Length);
			}
		}

		// Token: 0x06002618 RID: 9752 RVA: 0x00086D48 File Offset: 0x00084F48
		public void SetSignature(Type returnType, Type[] returnTypeRequiredCustomModifiers, Type[] returnTypeOptionalCustomModifiers, Type[] parameterTypes, Type[][] parameterTypeRequiredCustomModifiers, Type[][] parameterTypeOptionalCustomModifiers)
		{
			this.SetReturnType(returnType);
			this.SetParameters(parameterTypes);
			this.returnModReq = returnTypeRequiredCustomModifiers;
			this.returnModOpt = returnTypeOptionalCustomModifiers;
			this.paramModReq = parameterTypeRequiredCustomModifiers;
			this.paramModOpt = parameterTypeOptionalCustomModifiers;
		}

		// Token: 0x170006C6 RID: 1734
		// (get) Token: 0x06002619 RID: 9753 RVA: 0x00086D78 File Offset: 0x00084F78
		public override Module Module
		{
			get
			{
				return base.Module;
			}
		}

		// Token: 0x04000E40 RID: 3648
		private RuntimeMethodHandle mhandle;

		// Token: 0x04000E41 RID: 3649
		private Type rtype;

		// Token: 0x04000E42 RID: 3650
		internal Type[] parameters;

		// Token: 0x04000E43 RID: 3651
		private MethodAttributes attrs;

		// Token: 0x04000E44 RID: 3652
		private MethodImplAttributes iattrs;

		// Token: 0x04000E45 RID: 3653
		private string name;

		// Token: 0x04000E46 RID: 3654
		private int table_idx;

		// Token: 0x04000E47 RID: 3655
		private byte[] code;

		// Token: 0x04000E48 RID: 3656
		private ILGenerator ilgen;

		// Token: 0x04000E49 RID: 3657
		private TypeBuilder type;

		// Token: 0x04000E4A RID: 3658
		internal ParameterBuilder[] pinfo;

		// Token: 0x04000E4B RID: 3659
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000E4C RID: 3660
		private MethodInfo override_method;

		// Token: 0x04000E4D RID: 3661
		private string pi_dll;

		// Token: 0x04000E4E RID: 3662
		private string pi_entry;

		// Token: 0x04000E4F RID: 3663
		private CharSet charset;

		// Token: 0x04000E50 RID: 3664
		private uint extra_flags;

		// Token: 0x04000E51 RID: 3665
		private CallingConvention native_cc;

		// Token: 0x04000E52 RID: 3666
		private CallingConventions call_conv;

		// Token: 0x04000E53 RID: 3667
		private bool init_locals = true;

		// Token: 0x04000E54 RID: 3668
		private IntPtr generic_container;

		// Token: 0x04000E55 RID: 3669
		internal GenericTypeParameterBuilder[] generic_params;

		// Token: 0x04000E56 RID: 3670
		private Type[] returnModReq;

		// Token: 0x04000E57 RID: 3671
		private Type[] returnModOpt;

		// Token: 0x04000E58 RID: 3672
		private Type[][] paramModReq;

		// Token: 0x04000E59 RID: 3673
		private Type[][] paramModOpt;

		// Token: 0x04000E5A RID: 3674
		private RefEmitPermissionSet[] permissions;
	}
}
