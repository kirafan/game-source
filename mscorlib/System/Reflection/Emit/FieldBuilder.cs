﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Reflection.Emit
{
	// Token: 0x020002D7 RID: 727
	[ComDefaultInterface(typeof(_FieldBuilder))]
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	public sealed class FieldBuilder : FieldInfo, _FieldBuilder
	{
		// Token: 0x060024FA RID: 9466 RVA: 0x00083254 File Offset: 0x00081454
		internal FieldBuilder(TypeBuilder tb, string fieldName, Type type, FieldAttributes attributes, Type[] modReq, Type[] modOpt)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			this.attrs = attributes;
			this.name = fieldName;
			this.type = type;
			this.modReq = modReq;
			this.modOpt = modOpt;
			this.offset = -1;
			this.typeb = tb;
			this.table_idx = tb.get_next_table_index(this, 4, true);
		}

		// Token: 0x060024FB RID: 9467 RVA: 0x000832BC File Offset: 0x000814BC
		void _FieldBuilder.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060024FC RID: 9468 RVA: 0x000832C4 File Offset: 0x000814C4
		void _FieldBuilder.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060024FD RID: 9469 RVA: 0x000832CC File Offset: 0x000814CC
		void _FieldBuilder.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060024FE RID: 9470 RVA: 0x000832D4 File Offset: 0x000814D4
		void _FieldBuilder.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000685 RID: 1669
		// (get) Token: 0x060024FF RID: 9471 RVA: 0x000832DC File Offset: 0x000814DC
		public override FieldAttributes Attributes
		{
			get
			{
				return this.attrs;
			}
		}

		// Token: 0x17000686 RID: 1670
		// (get) Token: 0x06002500 RID: 9472 RVA: 0x000832E4 File Offset: 0x000814E4
		public override Type DeclaringType
		{
			get
			{
				return this.typeb;
			}
		}

		// Token: 0x17000687 RID: 1671
		// (get) Token: 0x06002501 RID: 9473 RVA: 0x000832EC File Offset: 0x000814EC
		public override RuntimeFieldHandle FieldHandle
		{
			get
			{
				throw this.CreateNotSupportedException();
			}
		}

		// Token: 0x17000688 RID: 1672
		// (get) Token: 0x06002502 RID: 9474 RVA: 0x000832F4 File Offset: 0x000814F4
		public override Type FieldType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x17000689 RID: 1673
		// (get) Token: 0x06002503 RID: 9475 RVA: 0x000832FC File Offset: 0x000814FC
		public override string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x1700068A RID: 1674
		// (get) Token: 0x06002504 RID: 9476 RVA: 0x00083304 File Offset: 0x00081504
		public override Type ReflectedType
		{
			get
			{
				return this.typeb;
			}
		}

		// Token: 0x06002505 RID: 9477 RVA: 0x0008330C File Offset: 0x0008150C
		public override object[] GetCustomAttributes(bool inherit)
		{
			if (this.typeb.is_created)
			{
				return MonoCustomAttrs.GetCustomAttributes(this, inherit);
			}
			throw this.CreateNotSupportedException();
		}

		// Token: 0x06002506 RID: 9478 RVA: 0x0008332C File Offset: 0x0008152C
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			if (this.typeb.is_created)
			{
				return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
			}
			throw this.CreateNotSupportedException();
		}

		// Token: 0x06002507 RID: 9479 RVA: 0x00083350 File Offset: 0x00081550
		public FieldToken GetToken()
		{
			return new FieldToken(this.MetadataToken);
		}

		// Token: 0x06002508 RID: 9480 RVA: 0x00083360 File Offset: 0x00081560
		public override object GetValue(object obj)
		{
			throw this.CreateNotSupportedException();
		}

		// Token: 0x06002509 RID: 9481 RVA: 0x00083368 File Offset: 0x00081568
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			throw this.CreateNotSupportedException();
		}

		// Token: 0x0600250A RID: 9482 RVA: 0x00083370 File Offset: 0x00081570
		internal override int GetFieldOffset()
		{
			return 0;
		}

		// Token: 0x0600250B RID: 9483 RVA: 0x00083374 File Offset: 0x00081574
		internal void SetRVAData(byte[] data)
		{
			this.rva_data = (byte[])data.Clone();
		}

		// Token: 0x0600250C RID: 9484 RVA: 0x00083388 File Offset: 0x00081588
		public void SetConstant(object defaultValue)
		{
			this.RejectIfCreated();
			this.def_value = defaultValue;
		}

		// Token: 0x0600250D RID: 9485 RVA: 0x00083398 File Offset: 0x00081598
		public void SetCustomAttribute(CustomAttributeBuilder customBuilder)
		{
			this.RejectIfCreated();
			string fullName = customBuilder.Ctor.ReflectedType.FullName;
			if (fullName == "System.Runtime.InteropServices.FieldOffsetAttribute")
			{
				byte[] data = customBuilder.Data;
				this.offset = (int)data[2];
				this.offset |= (int)data[3] << 8;
				this.offset |= (int)data[4] << 16;
				this.offset |= (int)data[5] << 24;
				return;
			}
			if (fullName == "System.NonSerializedAttribute")
			{
				this.attrs |= FieldAttributes.NotSerialized;
				return;
			}
			if (fullName == "System.Runtime.CompilerServices.SpecialNameAttribute")
			{
				this.attrs |= FieldAttributes.SpecialName;
				return;
			}
			if (fullName == "System.Runtime.InteropServices.MarshalAsAttribute")
			{
				this.attrs |= FieldAttributes.HasFieldMarshal;
				this.marshal_info = CustomAttributeBuilder.get_umarshal(customBuilder, true);
				return;
			}
			if (this.cattrs != null)
			{
				CustomAttributeBuilder[] array = new CustomAttributeBuilder[this.cattrs.Length + 1];
				this.cattrs.CopyTo(array, 0);
				array[this.cattrs.Length] = customBuilder;
				this.cattrs = array;
			}
			else
			{
				this.cattrs = new CustomAttributeBuilder[1];
				this.cattrs[0] = customBuilder;
			}
		}

		// Token: 0x0600250E RID: 9486 RVA: 0x000834E0 File Offset: 0x000816E0
		[ComVisible(true)]
		public void SetCustomAttribute(ConstructorInfo con, byte[] binaryAttribute)
		{
			this.RejectIfCreated();
			this.SetCustomAttribute(new CustomAttributeBuilder(con, binaryAttribute));
		}

		// Token: 0x0600250F RID: 9487 RVA: 0x000834F8 File Offset: 0x000816F8
		[Obsolete("An alternate API is available: Emit the MarshalAs custom attribute instead.")]
		public void SetMarshal(UnmanagedMarshal unmanagedMarshal)
		{
			this.RejectIfCreated();
			this.marshal_info = unmanagedMarshal;
			this.attrs |= FieldAttributes.HasFieldMarshal;
		}

		// Token: 0x06002510 RID: 9488 RVA: 0x0008351C File Offset: 0x0008171C
		public void SetOffset(int iOffset)
		{
			this.RejectIfCreated();
			this.offset = iOffset;
		}

		// Token: 0x06002511 RID: 9489 RVA: 0x0008352C File Offset: 0x0008172C
		public override void SetValue(object obj, object val, BindingFlags invokeAttr, Binder binder, CultureInfo culture)
		{
			throw this.CreateNotSupportedException();
		}

		// Token: 0x1700068B RID: 1675
		// (get) Token: 0x06002512 RID: 9490 RVA: 0x00083534 File Offset: 0x00081734
		internal override UnmanagedMarshal UMarshal
		{
			get
			{
				return this.marshal_info;
			}
		}

		// Token: 0x06002513 RID: 9491 RVA: 0x0008353C File Offset: 0x0008173C
		private Exception CreateNotSupportedException()
		{
			return new NotSupportedException("The invoked member is not supported in a dynamic module.");
		}

		// Token: 0x06002514 RID: 9492 RVA: 0x00083548 File Offset: 0x00081748
		private void RejectIfCreated()
		{
			if (this.typeb.is_created)
			{
				throw new InvalidOperationException("Unable to change after type has been created.");
			}
		}

		// Token: 0x1700068C RID: 1676
		// (get) Token: 0x06002515 RID: 9493 RVA: 0x00083568 File Offset: 0x00081768
		public override Module Module
		{
			get
			{
				return base.Module;
			}
		}

		// Token: 0x04000DE4 RID: 3556
		private FieldAttributes attrs;

		// Token: 0x04000DE5 RID: 3557
		private Type type;

		// Token: 0x04000DE6 RID: 3558
		private string name;

		// Token: 0x04000DE7 RID: 3559
		private object def_value;

		// Token: 0x04000DE8 RID: 3560
		private int offset;

		// Token: 0x04000DE9 RID: 3561
		private int table_idx;

		// Token: 0x04000DEA RID: 3562
		internal TypeBuilder typeb;

		// Token: 0x04000DEB RID: 3563
		private byte[] rva_data;

		// Token: 0x04000DEC RID: 3564
		private CustomAttributeBuilder[] cattrs;

		// Token: 0x04000DED RID: 3565
		private UnmanagedMarshal marshal_info;

		// Token: 0x04000DEE RID: 3566
		private RuntimeFieldHandle handle;

		// Token: 0x04000DEF RID: 3567
		private Type[] modReq;

		// Token: 0x04000DF0 RID: 3568
		private Type[] modOpt;
	}
}
