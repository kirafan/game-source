﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x020002BD RID: 701
	[ComVisible(true)]
	[Serializable]
	public sealed class TargetParameterCountException : ApplicationException
	{
		// Token: 0x0600234D RID: 9037 RVA: 0x0007E6DC File Offset: 0x0007C8DC
		public TargetParameterCountException() : base(Locale.GetText("Number of parameter does not match expected count."))
		{
		}

		// Token: 0x0600234E RID: 9038 RVA: 0x0007E6F0 File Offset: 0x0007C8F0
		public TargetParameterCountException(string message) : base(message)
		{
		}

		// Token: 0x0600234F RID: 9039 RVA: 0x0007E6FC File Offset: 0x0007C8FC
		public TargetParameterCountException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06002350 RID: 9040 RVA: 0x0007E708 File Offset: 0x0007C908
		internal TargetParameterCountException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
