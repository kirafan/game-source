﻿using System;
using System.Collections.ObjectModel;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200028A RID: 650
	[ComVisible(true)]
	[Serializable]
	public struct CustomAttributeTypedArgument
	{
		// Token: 0x06002146 RID: 8518 RVA: 0x00079DF4 File Offset: 0x00077FF4
		internal CustomAttributeTypedArgument(Type argumentType, object value)
		{
			this.argumentType = argumentType;
			this.value = value;
			if (value is Array)
			{
				Array array = (Array)value;
				Type elementType = array.GetType().GetElementType();
				CustomAttributeTypedArgument[] array2 = new CustomAttributeTypedArgument[array.GetLength(0)];
				for (int i = 0; i < array2.Length; i++)
				{
					array2[i] = new CustomAttributeTypedArgument(elementType, array.GetValue(i));
				}
				this.value = new ReadOnlyCollection<CustomAttributeTypedArgument>(array2);
			}
		}

		// Token: 0x17000594 RID: 1428
		// (get) Token: 0x06002147 RID: 8519 RVA: 0x00079E74 File Offset: 0x00078074
		public Type ArgumentType
		{
			get
			{
				return this.argumentType;
			}
		}

		// Token: 0x17000595 RID: 1429
		// (get) Token: 0x06002148 RID: 8520 RVA: 0x00079E7C File Offset: 0x0007807C
		public object Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x06002149 RID: 8521 RVA: 0x00079E84 File Offset: 0x00078084
		public override string ToString()
		{
			string text = (this.value == null) ? string.Empty : this.value.ToString();
			if (this.argumentType == typeof(string))
			{
				return "\"" + text + "\"";
			}
			if (this.argumentType == typeof(Type))
			{
				return "typeof (" + text + ")";
			}
			if (this.argumentType.IsEnum)
			{
				return "(" + this.argumentType.Name + ")" + text;
			}
			return text;
		}

		// Token: 0x0600214A RID: 8522 RVA: 0x00079F2C File Offset: 0x0007812C
		public override bool Equals(object obj)
		{
			if (!(obj is CustomAttributeTypedArgument))
			{
				return false;
			}
			CustomAttributeTypedArgument customAttributeTypedArgument = (CustomAttributeTypedArgument)obj;
			return (customAttributeTypedArgument.argumentType != this.argumentType || this.value == null) ? (customAttributeTypedArgument.value == null) : this.value.Equals(customAttributeTypedArgument.value);
		}

		// Token: 0x0600214B RID: 8523 RVA: 0x00079F8C File Offset: 0x0007818C
		public override int GetHashCode()
		{
			return (this.argumentType.GetHashCode() << 16) + ((this.value == null) ? 0 : this.value.GetHashCode());
		}

		// Token: 0x0600214C RID: 8524 RVA: 0x00079FBC File Offset: 0x000781BC
		public static bool operator ==(CustomAttributeTypedArgument left, CustomAttributeTypedArgument right)
		{
			return left.Equals(right);
		}

		// Token: 0x0600214D RID: 8525 RVA: 0x00079FCC File Offset: 0x000781CC
		public static bool operator !=(CustomAttributeTypedArgument left, CustomAttributeTypedArgument right)
		{
			return !left.Equals(right);
		}

		// Token: 0x04000C47 RID: 3143
		private Type argumentType;

		// Token: 0x04000C48 RID: 3144
		private object value;
	}
}
