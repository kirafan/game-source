﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000049 RID: 73
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyCultureAttribute : Attribute
	{
		// Token: 0x0600066C RID: 1644 RVA: 0x00014B5C File Offset: 0x00012D5C
		public AssemblyCultureAttribute(string culture)
		{
			this.name = culture;
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x0600066D RID: 1645 RVA: 0x00014B6C File Offset: 0x00012D6C
		public string Culture
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x0400009E RID: 158
		private string name;
	}
}
