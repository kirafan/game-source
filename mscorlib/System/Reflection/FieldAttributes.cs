﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200028F RID: 655
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum FieldAttributes
	{
		// Token: 0x04000C5C RID: 3164
		FieldAccessMask = 7,
		// Token: 0x04000C5D RID: 3165
		PrivateScope = 0,
		// Token: 0x04000C5E RID: 3166
		Private = 1,
		// Token: 0x04000C5F RID: 3167
		FamANDAssem = 2,
		// Token: 0x04000C60 RID: 3168
		Assembly = 3,
		// Token: 0x04000C61 RID: 3169
		Family = 4,
		// Token: 0x04000C62 RID: 3170
		FamORAssem = 5,
		// Token: 0x04000C63 RID: 3171
		Public = 6,
		// Token: 0x04000C64 RID: 3172
		Static = 16,
		// Token: 0x04000C65 RID: 3173
		InitOnly = 32,
		// Token: 0x04000C66 RID: 3174
		Literal = 64,
		// Token: 0x04000C67 RID: 3175
		NotSerialized = 128,
		// Token: 0x04000C68 RID: 3176
		HasFieldRVA = 256,
		// Token: 0x04000C69 RID: 3177
		SpecialName = 512,
		// Token: 0x04000C6A RID: 3178
		RTSpecialName = 1024,
		// Token: 0x04000C6B RID: 3179
		HasFieldMarshal = 4096,
		// Token: 0x04000C6C RID: 3180
		PinvokeImpl = 8192,
		// Token: 0x04000C6D RID: 3181
		HasDefault = 32768,
		// Token: 0x04000C6E RID: 3182
		ReservedMask = 38144
	}
}
