﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000273 RID: 627
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyCopyrightAttribute : Attribute
	{
		// Token: 0x060020BE RID: 8382 RVA: 0x00077D4C File Offset: 0x00075F4C
		public AssemblyCopyrightAttribute(string copyright)
		{
			this.name = copyright;
		}

		// Token: 0x17000574 RID: 1396
		// (get) Token: 0x060020BF RID: 8383 RVA: 0x00077D5C File Offset: 0x00075F5C
		public string Copyright
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C03 RID: 3075
		private string name;
	}
}
