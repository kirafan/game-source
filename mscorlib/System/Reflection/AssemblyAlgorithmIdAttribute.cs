﻿using System;
using System.Configuration.Assemblies;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000270 RID: 624
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyAlgorithmIdAttribute : Attribute
	{
		// Token: 0x060020B7 RID: 8375 RVA: 0x00077CF4 File Offset: 0x00075EF4
		public AssemblyAlgorithmIdAttribute(AssemblyHashAlgorithm algorithmId)
		{
			this.id = (uint)algorithmId;
		}

		// Token: 0x060020B8 RID: 8376 RVA: 0x00077D04 File Offset: 0x00075F04
		[CLSCompliant(false)]
		public AssemblyAlgorithmIdAttribute(uint algorithmId)
		{
			this.id = algorithmId;
		}

		// Token: 0x17000571 RID: 1393
		// (get) Token: 0x060020B9 RID: 8377 RVA: 0x00077D14 File Offset: 0x00075F14
		[CLSCompliant(false)]
		public uint AlgorithmId
		{
			get
			{
				return this.id;
			}
		}

		// Token: 0x04000C00 RID: 3072
		private uint id;
	}
}
