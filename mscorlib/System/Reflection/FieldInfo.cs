﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000290 RID: 656
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_FieldInfo))]
	[ClassInterface(ClassInterfaceType.None)]
	[Serializable]
	public abstract class FieldInfo : MemberInfo, _FieldInfo
	{
		// Token: 0x06002170 RID: 8560 RVA: 0x0007A364 File Offset: 0x00078564
		void _FieldInfo.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002171 RID: 8561 RVA: 0x0007A36C File Offset: 0x0007856C
		void _FieldInfo.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002172 RID: 8562 RVA: 0x0007A374 File Offset: 0x00078574
		void _FieldInfo.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002173 RID: 8563 RVA: 0x0007A37C File Offset: 0x0007857C
		void _FieldInfo.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x170005A2 RID: 1442
		// (get) Token: 0x06002174 RID: 8564
		public abstract FieldAttributes Attributes { get; }

		// Token: 0x170005A3 RID: 1443
		// (get) Token: 0x06002175 RID: 8565
		public abstract RuntimeFieldHandle FieldHandle { get; }

		// Token: 0x170005A4 RID: 1444
		// (get) Token: 0x06002176 RID: 8566
		public abstract Type FieldType { get; }

		// Token: 0x06002177 RID: 8567
		public abstract object GetValue(object obj);

		// Token: 0x170005A5 RID: 1445
		// (get) Token: 0x06002178 RID: 8568 RVA: 0x0007A384 File Offset: 0x00078584
		public override MemberTypes MemberType
		{
			get
			{
				return MemberTypes.Field;
			}
		}

		// Token: 0x170005A6 RID: 1446
		// (get) Token: 0x06002179 RID: 8569 RVA: 0x0007A388 File Offset: 0x00078588
		public bool IsLiteral
		{
			get
			{
				return (this.Attributes & FieldAttributes.Literal) != FieldAttributes.PrivateScope;
			}
		}

		// Token: 0x170005A7 RID: 1447
		// (get) Token: 0x0600217A RID: 8570 RVA: 0x0007A39C File Offset: 0x0007859C
		public bool IsStatic
		{
			get
			{
				return (this.Attributes & FieldAttributes.Static) != FieldAttributes.PrivateScope;
			}
		}

		// Token: 0x170005A8 RID: 1448
		// (get) Token: 0x0600217B RID: 8571 RVA: 0x0007A3B0 File Offset: 0x000785B0
		public bool IsInitOnly
		{
			get
			{
				return (this.Attributes & FieldAttributes.InitOnly) != FieldAttributes.PrivateScope;
			}
		}

		// Token: 0x170005A9 RID: 1449
		// (get) Token: 0x0600217C RID: 8572 RVA: 0x0007A3C4 File Offset: 0x000785C4
		public bool IsPublic
		{
			get
			{
				return (this.Attributes & FieldAttributes.FieldAccessMask) == FieldAttributes.Public;
			}
		}

		// Token: 0x170005AA RID: 1450
		// (get) Token: 0x0600217D RID: 8573 RVA: 0x0007A3D4 File Offset: 0x000785D4
		public bool IsPrivate
		{
			get
			{
				return (this.Attributes & FieldAttributes.FieldAccessMask) == FieldAttributes.Private;
			}
		}

		// Token: 0x170005AB RID: 1451
		// (get) Token: 0x0600217E RID: 8574 RVA: 0x0007A3E4 File Offset: 0x000785E4
		public bool IsFamily
		{
			get
			{
				return (this.Attributes & FieldAttributes.FieldAccessMask) == FieldAttributes.Family;
			}
		}

		// Token: 0x170005AC RID: 1452
		// (get) Token: 0x0600217F RID: 8575 RVA: 0x0007A3F4 File Offset: 0x000785F4
		public bool IsAssembly
		{
			get
			{
				return (this.Attributes & FieldAttributes.FieldAccessMask) == FieldAttributes.Assembly;
			}
		}

		// Token: 0x170005AD RID: 1453
		// (get) Token: 0x06002180 RID: 8576 RVA: 0x0007A404 File Offset: 0x00078604
		public bool IsFamilyAndAssembly
		{
			get
			{
				return (this.Attributes & FieldAttributes.FieldAccessMask) == FieldAttributes.FamANDAssem;
			}
		}

		// Token: 0x170005AE RID: 1454
		// (get) Token: 0x06002181 RID: 8577 RVA: 0x0007A414 File Offset: 0x00078614
		public bool IsFamilyOrAssembly
		{
			get
			{
				return (this.Attributes & FieldAttributes.FieldAccessMask) == FieldAttributes.FamORAssem;
			}
		}

		// Token: 0x170005AF RID: 1455
		// (get) Token: 0x06002182 RID: 8578 RVA: 0x0007A424 File Offset: 0x00078624
		public bool IsPinvokeImpl
		{
			get
			{
				return (this.Attributes & FieldAttributes.PinvokeImpl) == FieldAttributes.PinvokeImpl;
			}
		}

		// Token: 0x170005B0 RID: 1456
		// (get) Token: 0x06002183 RID: 8579 RVA: 0x0007A43C File Offset: 0x0007863C
		public bool IsSpecialName
		{
			get
			{
				return (this.Attributes & FieldAttributes.SpecialName) == FieldAttributes.SpecialName;
			}
		}

		// Token: 0x170005B1 RID: 1457
		// (get) Token: 0x06002184 RID: 8580 RVA: 0x0007A454 File Offset: 0x00078654
		public bool IsNotSerialized
		{
			get
			{
				return (this.Attributes & FieldAttributes.NotSerialized) == FieldAttributes.NotSerialized;
			}
		}

		// Token: 0x06002185 RID: 8581
		public abstract void SetValue(object obj, object value, BindingFlags invokeAttr, Binder binder, CultureInfo culture);

		// Token: 0x06002186 RID: 8582 RVA: 0x0007A46C File Offset: 0x0007866C
		[DebuggerHidden]
		[DebuggerStepThrough]
		public void SetValue(object obj, object value)
		{
			this.SetValue(obj, value, BindingFlags.Default, null, null);
		}

		// Token: 0x06002187 RID: 8583
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern FieldInfo internal_from_handle_type(IntPtr field_handle, IntPtr type_handle);

		// Token: 0x06002188 RID: 8584 RVA: 0x0007A47C File Offset: 0x0007867C
		public static FieldInfo GetFieldFromHandle(RuntimeFieldHandle handle)
		{
			if (handle.Value == IntPtr.Zero)
			{
				throw new ArgumentException("The handle is invalid.");
			}
			return FieldInfo.internal_from_handle_type(handle.Value, IntPtr.Zero);
		}

		// Token: 0x06002189 RID: 8585 RVA: 0x0007A4BC File Offset: 0x000786BC
		[ComVisible(false)]
		public static FieldInfo GetFieldFromHandle(RuntimeFieldHandle handle, RuntimeTypeHandle declaringType)
		{
			if (handle.Value == IntPtr.Zero)
			{
				throw new ArgumentException("The handle is invalid.");
			}
			FieldInfo fieldInfo = FieldInfo.internal_from_handle_type(handle.Value, declaringType.Value);
			if (fieldInfo == null)
			{
				throw new ArgumentException("The field handle and the type handle are incompatible.");
			}
			return fieldInfo;
		}

		// Token: 0x0600218A RID: 8586 RVA: 0x0007A510 File Offset: 0x00078710
		internal virtual int GetFieldOffset()
		{
			throw new SystemException("This method should not be called");
		}

		// Token: 0x0600218B RID: 8587 RVA: 0x0007A51C File Offset: 0x0007871C
		[CLSCompliant(false)]
		[MonoTODO("Not implemented")]
		public virtual object GetValueDirect(TypedReference obj)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600218C RID: 8588 RVA: 0x0007A524 File Offset: 0x00078724
		[CLSCompliant(false)]
		[MonoTODO("Not implemented")]
		public virtual void SetValueDirect(TypedReference obj, object value)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600218D RID: 8589
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern UnmanagedMarshal GetUnmanagedMarshal();

		// Token: 0x170005B2 RID: 1458
		// (get) Token: 0x0600218E RID: 8590 RVA: 0x0007A52C File Offset: 0x0007872C
		internal virtual UnmanagedMarshal UMarshal
		{
			get
			{
				return this.GetUnmanagedMarshal();
			}
		}

		// Token: 0x0600218F RID: 8591 RVA: 0x0007A534 File Offset: 0x00078734
		internal object[] GetPseudoCustomAttributes()
		{
			int num = 0;
			if (this.IsNotSerialized)
			{
				num++;
			}
			if (this.DeclaringType.IsExplicitLayout)
			{
				num++;
			}
			UnmanagedMarshal umarshal = this.UMarshal;
			if (umarshal != null)
			{
				num++;
			}
			if (num == 0)
			{
				return null;
			}
			object[] array = new object[num];
			num = 0;
			if (this.IsNotSerialized)
			{
				array[num++] = new NonSerializedAttribute();
			}
			if (this.DeclaringType.IsExplicitLayout)
			{
				array[num++] = new FieldOffsetAttribute(this.GetFieldOffset());
			}
			if (umarshal != null)
			{
				array[num++] = umarshal.ToMarshalAsAttribute();
			}
			return array;
		}

		// Token: 0x06002190 RID: 8592
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type[] GetTypeModifiers(bool optional);

		// Token: 0x06002191 RID: 8593 RVA: 0x0007A5D8 File Offset: 0x000787D8
		public virtual Type[] GetOptionalCustomModifiers()
		{
			Type[] typeModifiers = this.GetTypeModifiers(true);
			if (typeModifiers == null)
			{
				return Type.EmptyTypes;
			}
			return typeModifiers;
		}

		// Token: 0x06002192 RID: 8594 RVA: 0x0007A5FC File Offset: 0x000787FC
		public virtual Type[] GetRequiredCustomModifiers()
		{
			Type[] typeModifiers = this.GetTypeModifiers(false);
			if (typeModifiers == null)
			{
				return Type.EmptyTypes;
			}
			return typeModifiers;
		}

		// Token: 0x06002193 RID: 8595 RVA: 0x0007A620 File Offset: 0x00078820
		public virtual object GetRawConstantValue()
		{
			throw new NotSupportedException("This non-CLS method is not implemented.");
		}

		// Token: 0x06002194 RID: 8596 RVA: 0x0007A62C File Offset: 0x0007882C
		virtual Type GetType()
		{
			return base.GetType();
		}
	}
}
