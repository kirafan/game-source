﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200027F RID: 639
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyProductAttribute : Attribute
	{
		// Token: 0x060020FE RID: 8446 RVA: 0x00078780 File Offset: 0x00076980
		public AssemblyProductAttribute(string product)
		{
			this.name = product;
		}

		// Token: 0x1700058A RID: 1418
		// (get) Token: 0x060020FF RID: 8447 RVA: 0x00078790 File Offset: 0x00076990
		public string Product
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C21 RID: 3105
		private string name;
	}
}
