﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000272 RID: 626
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyConfigurationAttribute : Attribute
	{
		// Token: 0x060020BC RID: 8380 RVA: 0x00077D34 File Offset: 0x00075F34
		public AssemblyConfigurationAttribute(string configuration)
		{
			this.name = configuration;
		}

		// Token: 0x17000573 RID: 1395
		// (get) Token: 0x060020BD RID: 8381 RVA: 0x00077D44 File Offset: 0x00075F44
		public string Configuration
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C02 RID: 3074
		private string name;
	}
}
