﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000292 RID: 658
	[ComVisible(true)]
	[Serializable]
	public enum ImageFileMachine
	{
		// Token: 0x04000C79 RID: 3193
		I386 = 332,
		// Token: 0x04000C7A RID: 3194
		IA64 = 512,
		// Token: 0x04000C7B RID: 3195
		AMD64 = 34404
	}
}
