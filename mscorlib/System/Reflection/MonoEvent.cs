﻿using System;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x020002A5 RID: 677
	[Serializable]
	internal sealed class MonoEvent : EventInfo, ISerializable
	{
		// Token: 0x170005EA RID: 1514
		// (get) Token: 0x0600226B RID: 8811 RVA: 0x0007C658 File Offset: 0x0007A858
		public override EventAttributes Attributes
		{
			get
			{
				return MonoEventInfo.GetEventInfo(this).attrs;
			}
		}

		// Token: 0x0600226C RID: 8812 RVA: 0x0007C674 File Offset: 0x0007A874
		public override MethodInfo GetAddMethod(bool nonPublic)
		{
			MonoEventInfo eventInfo = MonoEventInfo.GetEventInfo(this);
			if (nonPublic || (eventInfo.add_method != null && eventInfo.add_method.IsPublic))
			{
				return eventInfo.add_method;
			}
			return null;
		}

		// Token: 0x0600226D RID: 8813 RVA: 0x0007C6B4 File Offset: 0x0007A8B4
		public override MethodInfo GetRaiseMethod(bool nonPublic)
		{
			MonoEventInfo eventInfo = MonoEventInfo.GetEventInfo(this);
			if (nonPublic || (eventInfo.raise_method != null && eventInfo.raise_method.IsPublic))
			{
				return eventInfo.raise_method;
			}
			return null;
		}

		// Token: 0x0600226E RID: 8814 RVA: 0x0007C6F4 File Offset: 0x0007A8F4
		public override MethodInfo GetRemoveMethod(bool nonPublic)
		{
			MonoEventInfo eventInfo = MonoEventInfo.GetEventInfo(this);
			if (nonPublic || (eventInfo.remove_method != null && eventInfo.remove_method.IsPublic))
			{
				return eventInfo.remove_method;
			}
			return null;
		}

		// Token: 0x0600226F RID: 8815 RVA: 0x0007C734 File Offset: 0x0007A934
		public override MethodInfo[] GetOtherMethods(bool nonPublic)
		{
			MonoEventInfo eventInfo = MonoEventInfo.GetEventInfo(this);
			if (nonPublic)
			{
				return eventInfo.other_methods;
			}
			int num = 0;
			foreach (MethodInfo methodInfo in eventInfo.other_methods)
			{
				if (methodInfo.IsPublic)
				{
					num++;
				}
			}
			if (num == eventInfo.other_methods.Length)
			{
				return eventInfo.other_methods;
			}
			MethodInfo[] array = new MethodInfo[num];
			num = 0;
			foreach (MethodInfo methodInfo2 in eventInfo.other_methods)
			{
				if (methodInfo2.IsPublic)
				{
					array[num++] = methodInfo2;
				}
			}
			return array;
		}

		// Token: 0x170005EB RID: 1515
		// (get) Token: 0x06002270 RID: 8816 RVA: 0x0007C7F0 File Offset: 0x0007A9F0
		public override Type DeclaringType
		{
			get
			{
				return MonoEventInfo.GetEventInfo(this).declaring_type;
			}
		}

		// Token: 0x170005EC RID: 1516
		// (get) Token: 0x06002271 RID: 8817 RVA: 0x0007C80C File Offset: 0x0007AA0C
		public override Type ReflectedType
		{
			get
			{
				return MonoEventInfo.GetEventInfo(this).reflected_type;
			}
		}

		// Token: 0x170005ED RID: 1517
		// (get) Token: 0x06002272 RID: 8818 RVA: 0x0007C828 File Offset: 0x0007AA28
		public override string Name
		{
			get
			{
				return MonoEventInfo.GetEventInfo(this).name;
			}
		}

		// Token: 0x06002273 RID: 8819 RVA: 0x0007C844 File Offset: 0x0007AA44
		public override string ToString()
		{
			return this.EventHandlerType + " " + this.Name;
		}

		// Token: 0x06002274 RID: 8820 RVA: 0x0007C85C File Offset: 0x0007AA5C
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x06002275 RID: 8821 RVA: 0x0007C868 File Offset: 0x0007AA68
		public override object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x06002276 RID: 8822 RVA: 0x0007C874 File Offset: 0x0007AA74
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x06002277 RID: 8823 RVA: 0x0007C880 File Offset: 0x0007AA80
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			MemberInfoSerializationHolder.Serialize(info, this.Name, this.ReflectedType, this.ToString(), MemberTypes.Event);
		}

		// Token: 0x04000CE3 RID: 3299
		private IntPtr klass;

		// Token: 0x04000CE4 RID: 3300
		private IntPtr handle;
	}
}
