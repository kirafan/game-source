﻿using System;
using System.Collections;
using System.Configuration.Assemblies;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;

namespace System.Reflection
{
	// Token: 0x0200026D RID: 621
	[ComDefaultInterface(typeof(_Assembly))]
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[Serializable]
	public class Assembly : ISerializable, ICustomAttributeProvider, _Assembly, IEvidenceFactory
	{
		// Token: 0x0600204E RID: 8270 RVA: 0x00077084 File Offset: 0x00075284
		internal Assembly()
		{
			this.resolve_event_holder = new Assembly.ResolveEventHolder();
		}

		// Token: 0x14000014 RID: 20
		// (add) Token: 0x0600204F RID: 8271 RVA: 0x00077098 File Offset: 0x00075298
		// (remove) Token: 0x06002050 RID: 8272 RVA: 0x000770A8 File Offset: 0x000752A8
		public event ModuleResolveEventHandler ModuleResolve
		{
			[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
			add
			{
				this.resolve_event_holder.ModuleResolve += value;
			}
			[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlAppDomain\"/>\n</PermissionSet>\n")]
			remove
			{
				this.resolve_event_holder.ModuleResolve -= value;
			}
		}

		// Token: 0x06002051 RID: 8273
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string get_code_base(bool escaped);

		// Token: 0x06002052 RID: 8274
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string get_fullname();

		// Token: 0x06002053 RID: 8275
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string get_location();

		// Token: 0x06002054 RID: 8276
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern string InternalImageRuntimeVersion();

		// Token: 0x06002055 RID: 8277 RVA: 0x000770B8 File Offset: 0x000752B8
		private string GetCodeBase(bool escaped)
		{
			string text = this.get_code_base(escaped);
			if (SecurityManager.SecurityEnabled && string.Compare("FILE://", 0, text, 0, 7, true, CultureInfo.InvariantCulture) == 0)
			{
				string path = text.Substring(7);
				new FileIOPermission(FileIOPermissionAccess.PathDiscovery, path).Demand();
			}
			return text;
		}

		// Token: 0x17000563 RID: 1379
		// (get) Token: 0x06002056 RID: 8278 RVA: 0x00077108 File Offset: 0x00075308
		public virtual string CodeBase
		{
			get
			{
				return this.GetCodeBase(false);
			}
		}

		// Token: 0x17000564 RID: 1380
		// (get) Token: 0x06002057 RID: 8279 RVA: 0x00077114 File Offset: 0x00075314
		public virtual string EscapedCodeBase
		{
			get
			{
				return this.GetCodeBase(true);
			}
		}

		// Token: 0x17000565 RID: 1381
		// (get) Token: 0x06002058 RID: 8280 RVA: 0x00077120 File Offset: 0x00075320
		public virtual string FullName
		{
			get
			{
				return this.ToString();
			}
		}

		// Token: 0x17000566 RID: 1382
		// (get) Token: 0x06002059 RID: 8281
		public virtual extern MethodInfo EntryPoint { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x17000567 RID: 1383
		// (get) Token: 0x0600205A RID: 8282 RVA: 0x00077128 File Offset: 0x00075328
		public virtual Evidence Evidence
		{
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"ControlEvidence\"/>\n</PermissionSet>\n")]
			get
			{
				return this.UnprotectedGetEvidence();
			}
		}

		// Token: 0x0600205B RID: 8283 RVA: 0x00077130 File Offset: 0x00075330
		internal Evidence UnprotectedGetEvidence()
		{
			if (this._evidence == null)
			{
				lock (this)
				{
					this._evidence = Evidence.GetDefaultHostEvidence(this);
				}
			}
			return this._evidence;
		}

		// Token: 0x0600205C RID: 8284
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool get_global_assembly_cache();

		// Token: 0x17000568 RID: 1384
		// (get) Token: 0x0600205D RID: 8285 RVA: 0x0007718C File Offset: 0x0007538C
		public bool GlobalAssemblyCache
		{
			get
			{
				return this.get_global_assembly_cache();
			}
		}

		// Token: 0x17000569 RID: 1385
		// (set) Token: 0x0600205E RID: 8286 RVA: 0x00077194 File Offset: 0x00075394
		internal bool FromByteArray
		{
			set
			{
				this.fromByteArray = value;
			}
		}

		// Token: 0x1700056A RID: 1386
		// (get) Token: 0x0600205F RID: 8287 RVA: 0x000771A0 File Offset: 0x000753A0
		public virtual string Location
		{
			get
			{
				if (this.fromByteArray)
				{
					return string.Empty;
				}
				string location = this.get_location();
				if (location != string.Empty && SecurityManager.SecurityEnabled)
				{
					new FileIOPermission(FileIOPermissionAccess.PathDiscovery, location).Demand();
				}
				return location;
			}
		}

		// Token: 0x1700056B RID: 1387
		// (get) Token: 0x06002060 RID: 8288 RVA: 0x000771EC File Offset: 0x000753EC
		[ComVisible(false)]
		public virtual string ImageRuntimeVersion
		{
			get
			{
				return this.InternalImageRuntimeVersion();
			}
		}

		// Token: 0x06002061 RID: 8289 RVA: 0x000771F4 File Offset: 0x000753F4
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			UnitySerializationHolder.GetAssemblyData(this, info, context);
		}

		// Token: 0x06002062 RID: 8290 RVA: 0x00077210 File Offset: 0x00075410
		public virtual bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x06002063 RID: 8291 RVA: 0x0007721C File Offset: 0x0007541C
		public virtual object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x06002064 RID: 8292 RVA: 0x00077228 File Offset: 0x00075428
		public virtual object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x06002065 RID: 8293
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern object GetFilesInternal(string name, bool getResourceModules);

		// Token: 0x06002066 RID: 8294 RVA: 0x00077234 File Offset: 0x00075434
		public virtual FileStream[] GetFiles()
		{
			return this.GetFiles(false);
		}

		// Token: 0x06002067 RID: 8295 RVA: 0x00077240 File Offset: 0x00075440
		public virtual FileStream[] GetFiles(bool getResourceModules)
		{
			string[] array = (string[])this.GetFilesInternal(null, getResourceModules);
			if (array == null)
			{
				return new FileStream[0];
			}
			string location = this.Location;
			FileStream[] array2;
			if (location != string.Empty)
			{
				array2 = new FileStream[array.Length + 1];
				array2[0] = new FileStream(location, FileMode.Open, FileAccess.Read);
				for (int i = 0; i < array.Length; i++)
				{
					array2[i + 1] = new FileStream(array[i], FileMode.Open, FileAccess.Read);
				}
			}
			else
			{
				array2 = new FileStream[array.Length];
				for (int j = 0; j < array.Length; j++)
				{
					array2[j] = new FileStream(array[j], FileMode.Open, FileAccess.Read);
				}
			}
			return array2;
		}

		// Token: 0x06002068 RID: 8296 RVA: 0x000772F0 File Offset: 0x000754F0
		public virtual FileStream GetFile(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException(null, "Name cannot be null.");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Empty name is not valid");
			}
			string text = (string)this.GetFilesInternal(name, true);
			if (text != null)
			{
				return new FileStream(text, FileMode.Open, FileAccess.Read);
			}
			return null;
		}

		// Token: 0x06002069 RID: 8297
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern IntPtr GetManifestResourceInternal(string name, out int size, out Module module);

		// Token: 0x0600206A RID: 8298 RVA: 0x00077344 File Offset: 0x00075544
		public unsafe virtual Stream GetManifestResourceStream(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("String cannot have zero length.", "name");
			}
			ManifestResourceInfo manifestResourceInfo = this.GetManifestResourceInfo(name);
			if (manifestResourceInfo == null)
			{
				return null;
			}
			if (manifestResourceInfo.ReferencedAssembly != null)
			{
				return manifestResourceInfo.ReferencedAssembly.GetManifestResourceStream(name);
			}
			if (manifestResourceInfo.FileName != null && manifestResourceInfo.ResourceLocation == (ResourceLocation)0)
			{
				if (this.fromByteArray)
				{
					throw new FileNotFoundException(manifestResourceInfo.FileName);
				}
				string directoryName = Path.GetDirectoryName(this.Location);
				string path = Path.Combine(directoryName, manifestResourceInfo.FileName);
				return new FileStream(path, FileMode.Open, FileAccess.Read);
			}
			else
			{
				int num;
				Module module;
				IntPtr manifestResourceInternal = this.GetManifestResourceInternal(name, out num, out module);
				if (manifestResourceInternal == (IntPtr)0)
				{
					return null;
				}
				UnmanagedMemoryStream unmanagedMemoryStream = new UnmanagedMemoryStream((byte*)((void*)manifestResourceInternal), (long)num);
				unmanagedMemoryStream.Closed += new Assembly.ResourceCloseHandler(module).OnClose;
				return unmanagedMemoryStream;
			}
		}

		// Token: 0x0600206B RID: 8299 RVA: 0x00077440 File Offset: 0x00075640
		public virtual Stream GetManifestResourceStream(Type type, string name)
		{
			string text;
			if (type != null)
			{
				text = type.Namespace;
			}
			else
			{
				if (name == null)
				{
					throw new ArgumentNullException("type");
				}
				text = null;
			}
			if (text == null || text.Length == 0)
			{
				return this.GetManifestResourceStream(name);
			}
			return this.GetManifestResourceStream(text + "." + name);
		}

		// Token: 0x0600206C RID: 8300
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal virtual extern Type[] GetTypes(bool exportedOnly);

		// Token: 0x0600206D RID: 8301 RVA: 0x000774A0 File Offset: 0x000756A0
		public virtual Type[] GetTypes()
		{
			return this.GetTypes(false);
		}

		// Token: 0x0600206E RID: 8302 RVA: 0x000774AC File Offset: 0x000756AC
		public virtual Type[] GetExportedTypes()
		{
			return this.GetTypes(true);
		}

		// Token: 0x0600206F RID: 8303 RVA: 0x000774B8 File Offset: 0x000756B8
		public virtual Type GetType(string name, bool throwOnError)
		{
			return this.GetType(name, throwOnError, false);
		}

		// Token: 0x06002070 RID: 8304 RVA: 0x000774C4 File Offset: 0x000756C4
		public virtual Type GetType(string name)
		{
			return this.GetType(name, false, false);
		}

		// Token: 0x06002071 RID: 8305
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Type InternalGetType(Module module, string name, bool throwOnError, bool ignoreCase);

		// Token: 0x06002072 RID: 8306 RVA: 0x000774D0 File Offset: 0x000756D0
		public Type GetType(string name, bool throwOnError, bool ignoreCase)
		{
			if (name == null)
			{
				throw new ArgumentNullException(name);
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("name", "Name cannot be empty");
			}
			return this.InternalGetType(null, name, throwOnError, ignoreCase);
		}

		// Token: 0x06002073 RID: 8307
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void InternalGetAssemblyName(string assemblyFile, AssemblyName aname);

		// Token: 0x06002074 RID: 8308
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern void FillName(Assembly ass, AssemblyName aname);

		// Token: 0x06002075 RID: 8309 RVA: 0x00077510 File Offset: 0x00075710
		[MonoTODO("copiedName == true is not supported")]
		public virtual AssemblyName GetName(bool copiedName)
		{
			if (SecurityManager.SecurityEnabled)
			{
				this.GetCodeBase(true);
			}
			return this.UnprotectedGetName();
		}

		// Token: 0x06002076 RID: 8310 RVA: 0x0007752C File Offset: 0x0007572C
		public virtual AssemblyName GetName()
		{
			return this.GetName(false);
		}

		// Token: 0x06002077 RID: 8311 RVA: 0x00077538 File Offset: 0x00075738
		internal virtual AssemblyName UnprotectedGetName()
		{
			AssemblyName assemblyName = new AssemblyName();
			Assembly.FillName(this, assemblyName);
			return assemblyName;
		}

		// Token: 0x06002078 RID: 8312 RVA: 0x00077554 File Offset: 0x00075754
		public override string ToString()
		{
			if (this.assemblyName != null)
			{
				return this.assemblyName;
			}
			this.assemblyName = this.get_fullname();
			return this.assemblyName;
		}

		// Token: 0x06002079 RID: 8313 RVA: 0x00077588 File Offset: 0x00075788
		public static string CreateQualifiedName(string assemblyName, string typeName)
		{
			return typeName + ", " + assemblyName;
		}

		// Token: 0x0600207A RID: 8314 RVA: 0x00077598 File Offset: 0x00075798
		public static Assembly GetAssembly(Type type)
		{
			if (type != null)
			{
				return type.Assembly;
			}
			throw new ArgumentNullException("type");
		}

		// Token: 0x0600207B RID: 8315
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Assembly GetEntryAssembly();

		// Token: 0x0600207C RID: 8316 RVA: 0x000775B4 File Offset: 0x000757B4
		public Assembly GetSatelliteAssembly(CultureInfo culture)
		{
			return this.GetSatelliteAssembly(culture, null, true);
		}

		// Token: 0x0600207D RID: 8317 RVA: 0x000775C0 File Offset: 0x000757C0
		public Assembly GetSatelliteAssembly(CultureInfo culture, Version version)
		{
			return this.GetSatelliteAssembly(culture, version, true);
		}

		// Token: 0x0600207E RID: 8318 RVA: 0x000775CC File Offset: 0x000757CC
		internal Assembly GetSatelliteAssemblyNoThrow(CultureInfo culture, Version version)
		{
			return this.GetSatelliteAssembly(culture, version, false);
		}

		// Token: 0x0600207F RID: 8319 RVA: 0x000775D8 File Offset: 0x000757D8
		private Assembly GetSatelliteAssembly(CultureInfo culture, Version version, bool throwOnError)
		{
			if (culture == null)
			{
				throw new ArgumentException("culture");
			}
			AssemblyName name = this.GetName(true);
			if (version != null)
			{
				name.Version = version;
			}
			name.CultureInfo = culture;
			name.Name += ".resources";
			try
			{
				Assembly assembly = AppDomain.CurrentDomain.LoadSatellite(name, false);
				if (assembly != null)
				{
					return assembly;
				}
			}
			catch (FileNotFoundException)
			{
			}
			string directoryName = Path.GetDirectoryName(this.Location);
			string text = Path.Combine(directoryName, Path.Combine(culture.Name, name.Name + ".dll"));
			if (!throwOnError && !File.Exists(text))
			{
				return null;
			}
			return Assembly.LoadFrom(text);
		}

		// Token: 0x06002080 RID: 8320
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Assembly LoadFrom(string assemblyFile, bool refonly);

		// Token: 0x06002081 RID: 8321 RVA: 0x000776BC File Offset: 0x000758BC
		public static Assembly LoadFrom(string assemblyFile)
		{
			return Assembly.LoadFrom(assemblyFile, false);
		}

		// Token: 0x06002082 RID: 8322 RVA: 0x000776C8 File Offset: 0x000758C8
		public static Assembly LoadFrom(string assemblyFile, Evidence securityEvidence)
		{
			Assembly assembly = Assembly.LoadFrom(assemblyFile, false);
			if (assembly != null && securityEvidence != null)
			{
				assembly.Evidence.Merge(securityEvidence);
			}
			return assembly;
		}

		// Token: 0x06002083 RID: 8323 RVA: 0x000776F8 File Offset: 0x000758F8
		[MonoTODO("This overload is not currently implemented")]
		public static Assembly LoadFrom(string assemblyFile, Evidence securityEvidence, byte[] hashValue, AssemblyHashAlgorithm hashAlgorithm)
		{
			if (assemblyFile == null)
			{
				throw new ArgumentNullException("assemblyFile");
			}
			if (assemblyFile == string.Empty)
			{
				throw new ArgumentException("Name can't be the empty string", "assemblyFile");
			}
			throw new NotImplementedException();
		}

		// Token: 0x06002084 RID: 8324 RVA: 0x0007773C File Offset: 0x0007593C
		public static Assembly LoadFile(string path, Evidence securityEvidence)
		{
			if (path == null)
			{
				throw new ArgumentNullException("path");
			}
			if (path == string.Empty)
			{
				throw new ArgumentException("Path can't be empty", "path");
			}
			return Assembly.LoadFrom(path, securityEvidence);
		}

		// Token: 0x06002085 RID: 8325 RVA: 0x00077784 File Offset: 0x00075984
		public static Assembly LoadFile(string path)
		{
			return Assembly.LoadFile(path, null);
		}

		// Token: 0x06002086 RID: 8326 RVA: 0x00077790 File Offset: 0x00075990
		public static Assembly Load(string assemblyString)
		{
			return AppDomain.CurrentDomain.Load(assemblyString);
		}

		// Token: 0x06002087 RID: 8327 RVA: 0x000777A0 File Offset: 0x000759A0
		public static Assembly Load(string assemblyString, Evidence assemblySecurity)
		{
			return AppDomain.CurrentDomain.Load(assemblyString, assemblySecurity);
		}

		// Token: 0x06002088 RID: 8328 RVA: 0x000777B0 File Offset: 0x000759B0
		public static Assembly Load(AssemblyName assemblyRef)
		{
			return AppDomain.CurrentDomain.Load(assemblyRef);
		}

		// Token: 0x06002089 RID: 8329 RVA: 0x000777C0 File Offset: 0x000759C0
		public static Assembly Load(AssemblyName assemblyRef, Evidence assemblySecurity)
		{
			return AppDomain.CurrentDomain.Load(assemblyRef, assemblySecurity);
		}

		// Token: 0x0600208A RID: 8330 RVA: 0x000777D0 File Offset: 0x000759D0
		public static Assembly Load(byte[] rawAssembly)
		{
			return AppDomain.CurrentDomain.Load(rawAssembly);
		}

		// Token: 0x0600208B RID: 8331 RVA: 0x000777E0 File Offset: 0x000759E0
		public static Assembly Load(byte[] rawAssembly, byte[] rawSymbolStore)
		{
			return AppDomain.CurrentDomain.Load(rawAssembly, rawSymbolStore);
		}

		// Token: 0x0600208C RID: 8332 RVA: 0x000777F0 File Offset: 0x000759F0
		public static Assembly Load(byte[] rawAssembly, byte[] rawSymbolStore, Evidence securityEvidence)
		{
			return AppDomain.CurrentDomain.Load(rawAssembly, rawSymbolStore, securityEvidence);
		}

		// Token: 0x0600208D RID: 8333 RVA: 0x00077800 File Offset: 0x00075A00
		public static Assembly ReflectionOnlyLoad(byte[] rawAssembly)
		{
			return AppDomain.CurrentDomain.Load(rawAssembly, null, null, true);
		}

		// Token: 0x0600208E RID: 8334 RVA: 0x00077810 File Offset: 0x00075A10
		public static Assembly ReflectionOnlyLoad(string assemblyString)
		{
			return AppDomain.CurrentDomain.Load(assemblyString, null, true);
		}

		// Token: 0x0600208F RID: 8335 RVA: 0x00077820 File Offset: 0x00075A20
		public static Assembly ReflectionOnlyLoadFrom(string assemblyFile)
		{
			if (assemblyFile == null)
			{
				throw new ArgumentNullException("assemblyFile");
			}
			return Assembly.LoadFrom(assemblyFile, true);
		}

		// Token: 0x06002090 RID: 8336 RVA: 0x0007783C File Offset: 0x00075A3C
		[Obsolete("")]
		public static Assembly LoadWithPartialName(string partialName)
		{
			return Assembly.LoadWithPartialName(partialName, null);
		}

		// Token: 0x06002091 RID: 8337 RVA: 0x00077848 File Offset: 0x00075A48
		[MonoTODO("Not implemented")]
		public Module LoadModule(string moduleName, byte[] rawModule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002092 RID: 8338 RVA: 0x00077850 File Offset: 0x00075A50
		[MonoTODO("Not implemented")]
		public Module LoadModule(string moduleName, byte[] rawModule, byte[] rawSymbolStore)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002093 RID: 8339
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Assembly load_with_partial_name(string name, Evidence e);

		// Token: 0x06002094 RID: 8340 RVA: 0x00077858 File Offset: 0x00075A58
		[Obsolete("")]
		public static Assembly LoadWithPartialName(string partialName, Evidence securityEvidence)
		{
			return Assembly.LoadWithPartialName(partialName, securityEvidence, true);
		}

		// Token: 0x06002095 RID: 8341 RVA: 0x00077864 File Offset: 0x00075A64
		internal static Assembly LoadWithPartialName(string partialName, Evidence securityEvidence, bool oldBehavior)
		{
			if (!oldBehavior)
			{
				throw new NotImplementedException();
			}
			if (partialName == null)
			{
				throw new NullReferenceException();
			}
			return Assembly.load_with_partial_name(partialName, securityEvidence);
		}

		// Token: 0x06002096 RID: 8342 RVA: 0x00077888 File Offset: 0x00075A88
		public object CreateInstance(string typeName)
		{
			return this.CreateInstance(typeName, false);
		}

		// Token: 0x06002097 RID: 8343 RVA: 0x00077894 File Offset: 0x00075A94
		public object CreateInstance(string typeName, bool ignoreCase)
		{
			Type type = this.GetType(typeName, false, ignoreCase);
			if (type == null)
			{
				return null;
			}
			object result;
			try
			{
				result = Activator.CreateInstance(type);
			}
			catch (InvalidOperationException)
			{
				throw new ArgumentException("It is illegal to invoke a method on a Type loaded via ReflectionOnly methods.");
			}
			return result;
		}

		// Token: 0x06002098 RID: 8344 RVA: 0x000778F4 File Offset: 0x00075AF4
		public object CreateInstance(string typeName, bool ignoreCase, BindingFlags bindingAttr, Binder binder, object[] args, CultureInfo culture, object[] activationAttributes)
		{
			Type type = this.GetType(typeName, false, ignoreCase);
			if (type == null)
			{
				return null;
			}
			object result;
			try
			{
				result = Activator.CreateInstance(type, bindingAttr, binder, args, culture, activationAttributes);
			}
			catch (InvalidOperationException)
			{
				throw new ArgumentException("It is illegal to invoke a method on a Type loaded via ReflectionOnly methods.");
			}
			return result;
		}

		// Token: 0x06002099 RID: 8345 RVA: 0x0007795C File Offset: 0x00075B5C
		public Module[] GetLoadedModules()
		{
			return this.GetLoadedModules(false);
		}

		// Token: 0x0600209A RID: 8346 RVA: 0x00077968 File Offset: 0x00075B68
		public Module[] GetLoadedModules(bool getResourceModules)
		{
			return this.GetModules(getResourceModules);
		}

		// Token: 0x0600209B RID: 8347 RVA: 0x00077974 File Offset: 0x00075B74
		public Module[] GetModules()
		{
			return this.GetModules(false);
		}

		// Token: 0x0600209C RID: 8348 RVA: 0x00077980 File Offset: 0x00075B80
		public Module GetModule(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (name.Length == 0)
			{
				throw new ArgumentException("Name can't be empty");
			}
			Module[] modules = this.GetModules(true);
			foreach (Module module in modules)
			{
				if (module.ScopeName == name)
				{
					return module;
				}
			}
			return null;
		}

		// Token: 0x0600209D RID: 8349
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal virtual extern Module[] GetModulesInternal();

		// Token: 0x0600209E RID: 8350 RVA: 0x000779EC File Offset: 0x00075BEC
		public Module[] GetModules(bool getResourceModules)
		{
			Module[] modulesInternal = this.GetModulesInternal();
			if (!getResourceModules)
			{
				ArrayList arrayList = new ArrayList(modulesInternal.Length);
				foreach (Module module in modulesInternal)
				{
					if (!module.IsResource())
					{
						arrayList.Add(module);
					}
				}
				return (Module[])arrayList.ToArray(typeof(Module));
			}
			return modulesInternal;
		}

		// Token: 0x0600209F RID: 8351
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern string[] GetNamespaces();

		// Token: 0x060020A0 RID: 8352
		[MethodImpl(MethodImplOptions.InternalCall)]
		public virtual extern string[] GetManifestResourceNames();

		// Token: 0x060020A1 RID: 8353
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Assembly GetExecutingAssembly();

		// Token: 0x060020A2 RID: 8354
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern Assembly GetCallingAssembly();

		// Token: 0x060020A3 RID: 8355
		[MethodImpl(MethodImplOptions.InternalCall)]
		public extern AssemblyName[] GetReferencedAssemblies();

		// Token: 0x060020A4 RID: 8356
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool GetManifestResourceInfoInternal(string name, ManifestResourceInfo info);

		// Token: 0x060020A5 RID: 8357 RVA: 0x00077A58 File Offset: 0x00075C58
		public virtual ManifestResourceInfo GetManifestResourceInfo(string resourceName)
		{
			if (resourceName == null)
			{
				throw new ArgumentNullException("resourceName");
			}
			if (resourceName.Length == 0)
			{
				throw new ArgumentException("String cannot have zero length.");
			}
			ManifestResourceInfo manifestResourceInfo = new ManifestResourceInfo();
			bool manifestResourceInfoInternal = this.GetManifestResourceInfoInternal(resourceName, manifestResourceInfo);
			if (manifestResourceInfoInternal)
			{
				return manifestResourceInfo;
			}
			return null;
		}

		// Token: 0x060020A6 RID: 8358
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern int MonoDebugger_GetMethodToken(MethodBase method);

		// Token: 0x1700056C RID: 1388
		// (get) Token: 0x060020A7 RID: 8359 RVA: 0x00077AA4 File Offset: 0x00075CA4
		[ComVisible(false)]
		[MonoTODO("Always returns zero")]
		public long HostContext
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x1700056D RID: 1389
		// (get) Token: 0x060020A8 RID: 8360 RVA: 0x00077AA8 File Offset: 0x00075CA8
		[ComVisible(false)]
		public Module ManifestModule
		{
			get
			{
				return this.GetManifestModule();
			}
		}

		// Token: 0x060020A9 RID: 8361 RVA: 0x00077AB0 File Offset: 0x00075CB0
		internal virtual Module GetManifestModule()
		{
			return this.GetManifestModuleInternal();
		}

		// Token: 0x060020AA RID: 8362
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Module GetManifestModuleInternal();

		// Token: 0x1700056E RID: 1390
		// (get) Token: 0x060020AB RID: 8363
		[ComVisible(false)]
		public virtual extern bool ReflectionOnly { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x060020AC RID: 8364 RVA: 0x00077AB8 File Offset: 0x00075CB8
		internal void Resolve()
		{
			lock (this)
			{
				this.LoadAssemblyPermissions();
				Evidence evidence = new Evidence(this.UnprotectedGetEvidence());
				evidence.AddHost(new PermissionRequestEvidence(this._minimum, this._optional, this._refuse));
				this._granted = SecurityManager.ResolvePolicy(evidence, this._minimum, this._optional, this._refuse, out this._denied);
			}
		}

		// Token: 0x1700056F RID: 1391
		// (get) Token: 0x060020AD RID: 8365 RVA: 0x00077B48 File Offset: 0x00075D48
		internal PermissionSet GrantedPermissionSet
		{
			get
			{
				if (this._granted == null)
				{
					if (SecurityManager.ResolvingPolicyLevel != null)
					{
						if (SecurityManager.ResolvingPolicyLevel.IsFullTrustAssembly(this))
						{
							return DefaultPolicies.FullTrust;
						}
						return null;
					}
					else
					{
						this.Resolve();
					}
				}
				return this._granted;
			}
		}

		// Token: 0x17000570 RID: 1392
		// (get) Token: 0x060020AE RID: 8366 RVA: 0x00077B90 File Offset: 0x00075D90
		internal PermissionSet DeniedPermissionSet
		{
			get
			{
				if (this._granted == null)
				{
					if (SecurityManager.ResolvingPolicyLevel != null)
					{
						if (SecurityManager.ResolvingPolicyLevel.IsFullTrustAssembly(this))
						{
							return null;
						}
						return DefaultPolicies.FullTrust;
					}
					else
					{
						this.Resolve();
					}
				}
				return this._denied;
			}
		}

		// Token: 0x060020AF RID: 8367
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool LoadPermissions(Assembly a, ref IntPtr minimum, ref int minLength, ref IntPtr optional, ref int optLength, ref IntPtr refused, ref int refLength);

		// Token: 0x060020B0 RID: 8368 RVA: 0x00077BD8 File Offset: 0x00075DD8
		private void LoadAssemblyPermissions()
		{
			IntPtr zero = IntPtr.Zero;
			IntPtr zero2 = IntPtr.Zero;
			IntPtr zero3 = IntPtr.Zero;
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			if (Assembly.LoadPermissions(this, ref zero, ref num, ref zero2, ref num2, ref zero3, ref num3))
			{
				if (num > 0)
				{
					byte[] array = new byte[num];
					Marshal.Copy(zero, array, 0, num);
					this._minimum = SecurityManager.Decode(array);
				}
				if (num2 > 0)
				{
					byte[] array2 = new byte[num2];
					Marshal.Copy(zero2, array2, 0, num2);
					this._optional = SecurityManager.Decode(array2);
				}
				if (num3 > 0)
				{
					byte[] array3 = new byte[num3];
					Marshal.Copy(zero3, array3, 0, num3);
					this._refuse = SecurityManager.Decode(array3);
				}
			}
		}

		// Token: 0x060020B1 RID: 8369 RVA: 0x00077C90 File Offset: 0x00075E90
		virtual Type GetType()
		{
			return base.GetType();
		}

		// Token: 0x04000BF4 RID: 3060
		private IntPtr _mono_assembly;

		// Token: 0x04000BF5 RID: 3061
		private Assembly.ResolveEventHolder resolve_event_holder;

		// Token: 0x04000BF6 RID: 3062
		private Evidence _evidence;

		// Token: 0x04000BF7 RID: 3063
		internal PermissionSet _minimum;

		// Token: 0x04000BF8 RID: 3064
		internal PermissionSet _optional;

		// Token: 0x04000BF9 RID: 3065
		internal PermissionSet _refuse;

		// Token: 0x04000BFA RID: 3066
		private PermissionSet _granted;

		// Token: 0x04000BFB RID: 3067
		private PermissionSet _denied;

		// Token: 0x04000BFC RID: 3068
		private bool fromByteArray;

		// Token: 0x04000BFD RID: 3069
		private string assemblyName;

		// Token: 0x0200026E RID: 622
		internal class ResolveEventHolder
		{
			// Token: 0x14000015 RID: 21
			// (add) Token: 0x060020B3 RID: 8371 RVA: 0x00077CA0 File Offset: 0x00075EA0
			// (remove) Token: 0x060020B4 RID: 8372 RVA: 0x00077CBC File Offset: 0x00075EBC
			public event ModuleResolveEventHandler ModuleResolve;
		}

		// Token: 0x0200026F RID: 623
		private class ResourceCloseHandler
		{
			// Token: 0x060020B5 RID: 8373 RVA: 0x00077CD8 File Offset: 0x00075ED8
			public ResourceCloseHandler(Module module)
			{
				this.module = module;
			}

			// Token: 0x060020B6 RID: 8374 RVA: 0x00077CE8 File Offset: 0x00075EE8
			public void OnClose(object sender, EventArgs e)
			{
				this.module = null;
			}

			// Token: 0x04000BFF RID: 3071
			private Module module;
		}
	}
}
