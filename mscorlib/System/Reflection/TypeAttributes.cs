﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002BE RID: 702
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum TypeAttributes
	{
		// Token: 0x04000D46 RID: 3398
		VisibilityMask = 7,
		// Token: 0x04000D47 RID: 3399
		NotPublic = 0,
		// Token: 0x04000D48 RID: 3400
		Public = 1,
		// Token: 0x04000D49 RID: 3401
		NestedPublic = 2,
		// Token: 0x04000D4A RID: 3402
		NestedPrivate = 3,
		// Token: 0x04000D4B RID: 3403
		NestedFamily = 4,
		// Token: 0x04000D4C RID: 3404
		NestedAssembly = 5,
		// Token: 0x04000D4D RID: 3405
		NestedFamANDAssem = 6,
		// Token: 0x04000D4E RID: 3406
		NestedFamORAssem = 7,
		// Token: 0x04000D4F RID: 3407
		LayoutMask = 24,
		// Token: 0x04000D50 RID: 3408
		AutoLayout = 0,
		// Token: 0x04000D51 RID: 3409
		SequentialLayout = 8,
		// Token: 0x04000D52 RID: 3410
		ExplicitLayout = 16,
		// Token: 0x04000D53 RID: 3411
		ClassSemanticsMask = 32,
		// Token: 0x04000D54 RID: 3412
		Class = 0,
		// Token: 0x04000D55 RID: 3413
		Interface = 32,
		// Token: 0x04000D56 RID: 3414
		Abstract = 128,
		// Token: 0x04000D57 RID: 3415
		Sealed = 256,
		// Token: 0x04000D58 RID: 3416
		SpecialName = 1024,
		// Token: 0x04000D59 RID: 3417
		Import = 4096,
		// Token: 0x04000D5A RID: 3418
		Serializable = 8192,
		// Token: 0x04000D5B RID: 3419
		StringFormatMask = 196608,
		// Token: 0x04000D5C RID: 3420
		AnsiClass = 0,
		// Token: 0x04000D5D RID: 3421
		UnicodeClass = 65536,
		// Token: 0x04000D5E RID: 3422
		AutoClass = 131072,
		// Token: 0x04000D5F RID: 3423
		BeforeFieldInit = 1048576,
		// Token: 0x04000D60 RID: 3424
		ReservedMask = 264192,
		// Token: 0x04000D61 RID: 3425
		RTSpecialName = 2048,
		// Token: 0x04000D62 RID: 3426
		HasSecurity = 262144,
		// Token: 0x04000D63 RID: 3427
		CustomFormatClass = 196608,
		// Token: 0x04000D64 RID: 3428
		CustomFormatMask = 12582912
	}
}
