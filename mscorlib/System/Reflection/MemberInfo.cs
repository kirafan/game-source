﻿using System;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace System.Reflection
{
	// Token: 0x02000034 RID: 52
	[ComDefaultInterface(typeof(_MemberInfo))]
	[ComVisible(true)]
	[ClassInterface(ClassInterfaceType.None)]
	[PermissionSet((SecurityAction)15, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\"\n               Unrestricted=\"true\"/>\n")]
	[Serializable]
	public abstract class MemberInfo : ICustomAttributeProvider, _MemberInfo
	{
		// Token: 0x06000577 RID: 1399 RVA: 0x00013E10 File Offset: 0x00012010
		void _MemberInfo.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000578 RID: 1400 RVA: 0x00013E18 File Offset: 0x00012018
		void _MemberInfo.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000579 RID: 1401 RVA: 0x00013E20 File Offset: 0x00012020
		void _MemberInfo.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600057A RID: 1402 RVA: 0x00013E28 File Offset: 0x00012028
		void _MemberInfo.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x0600057B RID: 1403
		public abstract Type DeclaringType { get; }

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x0600057C RID: 1404
		public abstract MemberTypes MemberType { get; }

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x0600057D RID: 1405
		public abstract string Name { get; }

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x0600057E RID: 1406
		public abstract Type ReflectedType { get; }

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x0600057F RID: 1407 RVA: 0x00013E30 File Offset: 0x00012030
		public virtual Module Module
		{
			get
			{
				return this.DeclaringType.Module;
			}
		}

		// Token: 0x06000580 RID: 1408
		public abstract bool IsDefined(Type attributeType, bool inherit);

		// Token: 0x06000581 RID: 1409
		public abstract object[] GetCustomAttributes(bool inherit);

		// Token: 0x06000582 RID: 1410
		public abstract object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x06000583 RID: 1411
		public virtual extern int MetadataToken { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x06000584 RID: 1412 RVA: 0x00013E40 File Offset: 0x00012040
		virtual Type GetType()
		{
			return base.GetType();
		}
	}
}
