﻿using System;
using System.Configuration.Assemblies;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Cryptography;
using System.Security.Permissions;
using System.Text;
using Mono.Security;
using Mono.Security.Cryptography;

namespace System.Reflection
{
	// Token: 0x0200027C RID: 636
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_AssemblyName))]
	[ClassInterface(ClassInterfaceType.None)]
	[Serializable]
	public sealed class AssemblyName : ICloneable, ISerializable, _AssemblyName, IDeserializationCallback
	{
		// Token: 0x060020D3 RID: 8403 RVA: 0x00077E5C File Offset: 0x0007605C
		public AssemblyName()
		{
			this.versioncompat = AssemblyVersionCompatibility.SameMachine;
		}

		// Token: 0x060020D4 RID: 8404 RVA: 0x00077E6C File Offset: 0x0007606C
		public AssemblyName(string assemblyName)
		{
			if (assemblyName == null)
			{
				throw new ArgumentNullException("assemblyName");
			}
			if (assemblyName.Length < 1)
			{
				throw new ArgumentException("assemblyName cannot have zero length.");
			}
			if (!AssemblyName.ParseName(this, assemblyName))
			{
				throw new FileLoadException("The assembly name is invalid.");
			}
		}

		// Token: 0x060020D5 RID: 8405 RVA: 0x00077EC0 File Offset: 0x000760C0
		internal AssemblyName(SerializationInfo si, StreamingContext sc)
		{
			this.name = si.GetString("_Name");
			this.codebase = si.GetString("_CodeBase");
			this.version = (Version)si.GetValue("_Version", typeof(Version));
			this.publicKey = (byte[])si.GetValue("_PublicKey", typeof(byte[]));
			this.keyToken = (byte[])si.GetValue("_PublicKeyToken", typeof(byte[]));
			this.hashalg = (AssemblyHashAlgorithm)((int)si.GetValue("_HashAlgorithm", typeof(AssemblyHashAlgorithm)));
			this.keypair = (StrongNameKeyPair)si.GetValue("_StrongNameKeyPair", typeof(StrongNameKeyPair));
			this.versioncompat = (AssemblyVersionCompatibility)((int)si.GetValue("_VersionCompatibility", typeof(AssemblyVersionCompatibility)));
			this.flags = (AssemblyNameFlags)((int)si.GetValue("_Flags", typeof(AssemblyNameFlags)));
			int @int = si.GetInt32("_CultureInfo");
			if (@int != -1)
			{
				this.cultureinfo = new CultureInfo(@int);
			}
		}

		// Token: 0x060020D6 RID: 8406 RVA: 0x00077FF4 File Offset: 0x000761F4
		void _AssemblyName.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060020D7 RID: 8407 RVA: 0x00077FFC File Offset: 0x000761FC
		void _AssemblyName.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060020D8 RID: 8408 RVA: 0x00078004 File Offset: 0x00076204
		void _AssemblyName.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060020D9 RID: 8409 RVA: 0x0007800C File Offset: 0x0007620C
		void _AssemblyName.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060020DA RID: 8410
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern bool ParseName(AssemblyName aname, string assemblyName);

		// Token: 0x1700057E RID: 1406
		// (get) Token: 0x060020DB RID: 8411 RVA: 0x00078014 File Offset: 0x00076214
		// (set) Token: 0x060020DC RID: 8412 RVA: 0x0007801C File Offset: 0x0007621C
		[MonoTODO("Not used, as the values are too limited;  Mono supports more")]
		public ProcessorArchitecture ProcessorArchitecture
		{
			get
			{
				return this.processor_architecture;
			}
			set
			{
				this.processor_architecture = value;
			}
		}

		// Token: 0x1700057F RID: 1407
		// (get) Token: 0x060020DD RID: 8413 RVA: 0x00078028 File Offset: 0x00076228
		// (set) Token: 0x060020DE RID: 8414 RVA: 0x00078030 File Offset: 0x00076230
		public string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x17000580 RID: 1408
		// (get) Token: 0x060020DF RID: 8415 RVA: 0x0007803C File Offset: 0x0007623C
		// (set) Token: 0x060020E0 RID: 8416 RVA: 0x00078044 File Offset: 0x00076244
		public string CodeBase
		{
			get
			{
				return this.codebase;
			}
			set
			{
				this.codebase = value;
			}
		}

		// Token: 0x17000581 RID: 1409
		// (get) Token: 0x060020E1 RID: 8417 RVA: 0x00078050 File Offset: 0x00076250
		public string EscapedCodeBase
		{
			get
			{
				if (this.codebase == null)
				{
					return null;
				}
				return Uri.EscapeString(this.codebase, false, true, true);
			}
		}

		// Token: 0x17000582 RID: 1410
		// (get) Token: 0x060020E2 RID: 8418 RVA: 0x00078070 File Offset: 0x00076270
		// (set) Token: 0x060020E3 RID: 8419 RVA: 0x00078078 File Offset: 0x00076278
		public CultureInfo CultureInfo
		{
			get
			{
				return this.cultureinfo;
			}
			set
			{
				this.cultureinfo = value;
			}
		}

		// Token: 0x17000583 RID: 1411
		// (get) Token: 0x060020E4 RID: 8420 RVA: 0x00078084 File Offset: 0x00076284
		// (set) Token: 0x060020E5 RID: 8421 RVA: 0x0007808C File Offset: 0x0007628C
		public AssemblyNameFlags Flags
		{
			get
			{
				return this.flags;
			}
			set
			{
				this.flags = value;
			}
		}

		// Token: 0x17000584 RID: 1412
		// (get) Token: 0x060020E6 RID: 8422 RVA: 0x00078098 File Offset: 0x00076298
		public string FullName
		{
			get
			{
				if (this.name == null)
				{
					return string.Empty;
				}
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append(this.name);
				if (this.Version != null)
				{
					stringBuilder.Append(", Version=");
					stringBuilder.Append(this.Version.ToString());
				}
				if (this.cultureinfo != null)
				{
					stringBuilder.Append(", Culture=");
					if (this.cultureinfo.LCID == CultureInfo.InvariantCulture.LCID)
					{
						stringBuilder.Append("neutral");
					}
					else
					{
						stringBuilder.Append(this.cultureinfo.Name);
					}
				}
				byte[] array = this.InternalGetPublicKeyToken();
				if (array != null)
				{
					if (array.Length == 0)
					{
						stringBuilder.Append(", PublicKeyToken=null");
					}
					else
					{
						stringBuilder.Append(", PublicKeyToken=");
						for (int i = 0; i < array.Length; i++)
						{
							stringBuilder.Append(array[i].ToString("x2"));
						}
					}
				}
				if ((this.Flags & AssemblyNameFlags.Retargetable) != AssemblyNameFlags.None)
				{
					stringBuilder.Append(", Retargetable=Yes");
				}
				return stringBuilder.ToString();
			}
		}

		// Token: 0x17000585 RID: 1413
		// (get) Token: 0x060020E7 RID: 8423 RVA: 0x000781D0 File Offset: 0x000763D0
		// (set) Token: 0x060020E8 RID: 8424 RVA: 0x000781D8 File Offset: 0x000763D8
		public AssemblyHashAlgorithm HashAlgorithm
		{
			get
			{
				return this.hashalg;
			}
			set
			{
				this.hashalg = value;
			}
		}

		// Token: 0x17000586 RID: 1414
		// (get) Token: 0x060020E9 RID: 8425 RVA: 0x000781E4 File Offset: 0x000763E4
		// (set) Token: 0x060020EA RID: 8426 RVA: 0x000781EC File Offset: 0x000763EC
		public StrongNameKeyPair KeyPair
		{
			get
			{
				return this.keypair;
			}
			set
			{
				this.keypair = value;
			}
		}

		// Token: 0x17000587 RID: 1415
		// (get) Token: 0x060020EB RID: 8427 RVA: 0x000781F8 File Offset: 0x000763F8
		// (set) Token: 0x060020EC RID: 8428 RVA: 0x00078200 File Offset: 0x00076400
		public Version Version
		{
			get
			{
				return this.version;
			}
			set
			{
				this.version = value;
				if (value == null)
				{
					this.major = (this.minor = (this.build = (this.revision = 0)));
				}
				else
				{
					this.major = value.Major;
					this.minor = value.Minor;
					this.build = value.Build;
					this.revision = value.Revision;
				}
			}
		}

		// Token: 0x17000588 RID: 1416
		// (get) Token: 0x060020ED RID: 8429 RVA: 0x00078278 File Offset: 0x00076478
		// (set) Token: 0x060020EE RID: 8430 RVA: 0x00078280 File Offset: 0x00076480
		public AssemblyVersionCompatibility VersionCompatibility
		{
			get
			{
				return this.versioncompat;
			}
			set
			{
				this.versioncompat = value;
			}
		}

		// Token: 0x060020EF RID: 8431 RVA: 0x0007828C File Offset: 0x0007648C
		public override string ToString()
		{
			string fullName = this.FullName;
			return (fullName == null) ? base.ToString() : fullName;
		}

		// Token: 0x060020F0 RID: 8432 RVA: 0x000782B4 File Offset: 0x000764B4
		public byte[] GetPublicKey()
		{
			return this.publicKey;
		}

		// Token: 0x060020F1 RID: 8433 RVA: 0x000782BC File Offset: 0x000764BC
		public byte[] GetPublicKeyToken()
		{
			if (this.keyToken != null)
			{
				return this.keyToken;
			}
			if (this.publicKey == null)
			{
				return null;
			}
			if (this.publicKey.Length == 0)
			{
				return new byte[0];
			}
			if (!this.IsPublicKeyValid)
			{
				throw new SecurityException("The public key is not valid.");
			}
			this.keyToken = this.ComputePublicKeyToken();
			return this.keyToken;
		}

		// Token: 0x17000589 RID: 1417
		// (get) Token: 0x060020F2 RID: 8434 RVA: 0x00078324 File Offset: 0x00076524
		private bool IsPublicKeyValid
		{
			get
			{
				if (this.publicKey.Length == 16)
				{
					int i = 0;
					int num = 0;
					while (i < this.publicKey.Length)
					{
						num += (int)this.publicKey[i++];
					}
					if (num == 4)
					{
						return true;
					}
				}
				byte b = this.publicKey[0];
				if (b != 6)
				{
					if (b != 7)
					{
						if (b == 0)
						{
							if (this.publicKey.Length > 12 && this.publicKey[12] == 6)
							{
								try
								{
									CryptoConvert.FromCapiPublicKeyBlob(this.publicKey, 12);
									return true;
								}
								catch (CryptographicException)
								{
								}
							}
						}
					}
				}
				else
				{
					try
					{
						CryptoConvert.FromCapiPublicKeyBlob(this.publicKey);
						return true;
					}
					catch (CryptographicException)
					{
					}
				}
				return false;
			}
		}

		// Token: 0x060020F3 RID: 8435 RVA: 0x00078434 File Offset: 0x00076634
		private byte[] InternalGetPublicKeyToken()
		{
			if (this.keyToken != null)
			{
				return this.keyToken;
			}
			if (this.publicKey == null)
			{
				return null;
			}
			if (this.publicKey.Length == 0)
			{
				return new byte[0];
			}
			if (!this.IsPublicKeyValid)
			{
				throw new SecurityException("The public key is not valid.");
			}
			return this.ComputePublicKeyToken();
		}

		// Token: 0x060020F4 RID: 8436 RVA: 0x00078490 File Offset: 0x00076690
		private byte[] ComputePublicKeyToken()
		{
			HashAlgorithm hashAlgorithm = SHA1.Create();
			byte[] array = hashAlgorithm.ComputeHash(this.publicKey);
			byte[] array2 = new byte[8];
			Array.Copy(array, array.Length - 8, array2, 0, 8);
			Array.Reverse(array2, 0, 8);
			return array2;
		}

		// Token: 0x060020F5 RID: 8437 RVA: 0x000784D0 File Offset: 0x000766D0
		[MonoTODO]
		public static bool ReferenceMatchesDefinition(AssemblyName reference, AssemblyName definition)
		{
			if (reference == null)
			{
				throw new ArgumentNullException("reference");
			}
			if (definition == null)
			{
				throw new ArgumentNullException("definition");
			}
			if (reference.Name != definition.Name)
			{
				return false;
			}
			throw new NotImplementedException();
		}

		// Token: 0x060020F6 RID: 8438 RVA: 0x0007851C File Offset: 0x0007671C
		public void SetPublicKey(byte[] publicKey)
		{
			if (publicKey == null)
			{
				this.flags ^= AssemblyNameFlags.PublicKey;
			}
			else
			{
				this.flags |= AssemblyNameFlags.PublicKey;
			}
			this.publicKey = publicKey;
		}

		// Token: 0x060020F7 RID: 8439 RVA: 0x00078558 File Offset: 0x00076758
		public void SetPublicKeyToken(byte[] publicKeyToken)
		{
			this.keyToken = publicKeyToken;
		}

		// Token: 0x060020F8 RID: 8440 RVA: 0x00078564 File Offset: 0x00076764
		[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\n               version=\"1\">\n   <IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\n                version=\"1\"\n                Flags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("_Name", this.name);
			info.AddValue("_PublicKey", this.publicKey);
			info.AddValue("_PublicKeyToken", this.keyToken);
			info.AddValue("_CultureInfo", (this.cultureinfo == null) ? -1 : this.cultureinfo.LCID);
			info.AddValue("_CodeBase", this.codebase);
			info.AddValue("_Version", this.Version);
			info.AddValue("_HashAlgorithm", this.hashalg);
			info.AddValue("_HashAlgorithmForControl", AssemblyHashAlgorithm.None);
			info.AddValue("_StrongNameKeyPair", this.keypair);
			info.AddValue("_VersionCompatibility", this.versioncompat);
			info.AddValue("_Flags", this.flags);
			info.AddValue("_HashForControl", null);
		}

		// Token: 0x060020F9 RID: 8441 RVA: 0x00078670 File Offset: 0x00076870
		public object Clone()
		{
			return new AssemblyName
			{
				name = this.name,
				codebase = this.codebase,
				major = this.major,
				minor = this.minor,
				build = this.build,
				revision = this.revision,
				version = this.version,
				cultureinfo = this.cultureinfo,
				flags = this.flags,
				hashalg = this.hashalg,
				keypair = this.keypair,
				publicKey = this.publicKey,
				keyToken = this.keyToken,
				versioncompat = this.versioncompat
			};
		}

		// Token: 0x060020FA RID: 8442 RVA: 0x0007872C File Offset: 0x0007692C
		public void OnDeserialization(object sender)
		{
			this.Version = this.version;
		}

		// Token: 0x060020FB RID: 8443 RVA: 0x0007873C File Offset: 0x0007693C
		public static AssemblyName GetAssemblyName(string assemblyFile)
		{
			if (assemblyFile == null)
			{
				throw new ArgumentNullException("assemblyFile");
			}
			AssemblyName assemblyName = new AssemblyName();
			Assembly.InternalGetAssemblyName(Path.GetFullPath(assemblyFile), assemblyName);
			return assemblyName;
		}

		// Token: 0x04000C0C RID: 3084
		private string name;

		// Token: 0x04000C0D RID: 3085
		private string codebase;

		// Token: 0x04000C0E RID: 3086
		private int major;

		// Token: 0x04000C0F RID: 3087
		private int minor;

		// Token: 0x04000C10 RID: 3088
		private int build;

		// Token: 0x04000C11 RID: 3089
		private int revision;

		// Token: 0x04000C12 RID: 3090
		private CultureInfo cultureinfo;

		// Token: 0x04000C13 RID: 3091
		private AssemblyNameFlags flags;

		// Token: 0x04000C14 RID: 3092
		private AssemblyHashAlgorithm hashalg;

		// Token: 0x04000C15 RID: 3093
		private StrongNameKeyPair keypair;

		// Token: 0x04000C16 RID: 3094
		private byte[] publicKey;

		// Token: 0x04000C17 RID: 3095
		private byte[] keyToken;

		// Token: 0x04000C18 RID: 3096
		private AssemblyVersionCompatibility versioncompat;

		// Token: 0x04000C19 RID: 3097
		private Version version;

		// Token: 0x04000C1A RID: 3098
		private ProcessorArchitecture processor_architecture;
	}
}
