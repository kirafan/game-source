﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002B1 RID: 689
	[ComVisible(true)]
	[Serializable]
	public struct ParameterModifier
	{
		// Token: 0x06002314 RID: 8980 RVA: 0x0007E070 File Offset: 0x0007C270
		public ParameterModifier(int parameterCount)
		{
			if (parameterCount <= 0)
			{
				throw new ArgumentException("Must specify one or more parameters.");
			}
			this._byref = new bool[parameterCount];
		}

		// Token: 0x17000620 RID: 1568
		public bool this[int index]
		{
			get
			{
				return this._byref[index];
			}
			set
			{
				this._byref[index] = value;
			}
		}

		// Token: 0x04000D1F RID: 3359
		private bool[] _byref;
	}
}
