﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000299 RID: 665
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum MethodAttributes
	{
		// Token: 0x04000C97 RID: 3223
		MemberAccessMask = 7,
		// Token: 0x04000C98 RID: 3224
		PrivateScope = 0,
		// Token: 0x04000C99 RID: 3225
		Private = 1,
		// Token: 0x04000C9A RID: 3226
		FamANDAssem = 2,
		// Token: 0x04000C9B RID: 3227
		Assembly = 3,
		// Token: 0x04000C9C RID: 3228
		Family = 4,
		// Token: 0x04000C9D RID: 3229
		FamORAssem = 5,
		// Token: 0x04000C9E RID: 3230
		Public = 6,
		// Token: 0x04000C9F RID: 3231
		Static = 16,
		// Token: 0x04000CA0 RID: 3232
		Final = 32,
		// Token: 0x04000CA1 RID: 3233
		Virtual = 64,
		// Token: 0x04000CA2 RID: 3234
		HideBySig = 128,
		// Token: 0x04000CA3 RID: 3235
		VtableLayoutMask = 256,
		// Token: 0x04000CA4 RID: 3236
		CheckAccessOnOverride = 512,
		// Token: 0x04000CA5 RID: 3237
		ReuseSlot = 0,
		// Token: 0x04000CA6 RID: 3238
		NewSlot = 256,
		// Token: 0x04000CA7 RID: 3239
		Abstract = 1024,
		// Token: 0x04000CA8 RID: 3240
		SpecialName = 2048,
		// Token: 0x04000CA9 RID: 3241
		PinvokeImpl = 8192,
		// Token: 0x04000CAA RID: 3242
		UnmanagedExport = 8,
		// Token: 0x04000CAB RID: 3243
		RTSpecialName = 4096,
		// Token: 0x04000CAC RID: 3244
		ReservedMask = 53248,
		// Token: 0x04000CAD RID: 3245
		HasSecurity = 16384,
		// Token: 0x04000CAE RID: 3246
		RequireSecObject = 32768
	}
}
