﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002B3 RID: 691
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum PortableExecutableKinds
	{
		// Token: 0x04000D23 RID: 3363
		NotAPortableExecutableImage = 0,
		// Token: 0x04000D24 RID: 3364
		ILOnly = 1,
		// Token: 0x04000D25 RID: 3365
		Required32Bit = 2,
		// Token: 0x04000D26 RID: 3366
		PE32Plus = 4,
		// Token: 0x04000D27 RID: 3367
		Unmanaged32Bit = 8
	}
}
