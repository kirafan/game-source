﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000296 RID: 662
	[ComVisible(true)]
	public class ManifestResourceInfo
	{
		// Token: 0x0600219E RID: 8606 RVA: 0x0007A6DC File Offset: 0x000788DC
		internal ManifestResourceInfo()
		{
		}

		// Token: 0x0600219F RID: 8607 RVA: 0x0007A6E4 File Offset: 0x000788E4
		internal ManifestResourceInfo(Assembly assembly, string filename, ResourceLocation location)
		{
			this._assembly = assembly;
			this._filename = filename;
			this._location = location;
		}

		// Token: 0x170005B6 RID: 1462
		// (get) Token: 0x060021A0 RID: 8608 RVA: 0x0007A704 File Offset: 0x00078904
		public virtual string FileName
		{
			get
			{
				return this._filename;
			}
		}

		// Token: 0x170005B7 RID: 1463
		// (get) Token: 0x060021A1 RID: 8609 RVA: 0x0007A70C File Offset: 0x0007890C
		public virtual Assembly ReferencedAssembly
		{
			get
			{
				return this._assembly;
			}
		}

		// Token: 0x170005B8 RID: 1464
		// (get) Token: 0x060021A2 RID: 8610 RVA: 0x0007A714 File Offset: 0x00078914
		public virtual ResourceLocation ResourceLocation
		{
			get
			{
				return this._location;
			}
		}

		// Token: 0x04000C83 RID: 3203
		private Assembly _assembly;

		// Token: 0x04000C84 RID: 3204
		private string _filename;

		// Token: 0x04000C85 RID: 3205
		private ResourceLocation _location;
	}
}
