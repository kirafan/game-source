﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000276 RID: 630
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyDescriptionAttribute : Attribute
	{
		// Token: 0x060020C4 RID: 8388 RVA: 0x00077D94 File Offset: 0x00075F94
		public AssemblyDescriptionAttribute(string description)
		{
			this.name = description;
		}

		// Token: 0x17000577 RID: 1399
		// (get) Token: 0x060020C5 RID: 8389 RVA: 0x00077DA4 File Offset: 0x00075FA4
		public string Description
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C06 RID: 3078
		private string name;
	}
}
