﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x020002B2 RID: 690
	[ComVisible(true)]
	[CLSCompliant(false)]
	[Serializable]
	public sealed class Pointer : ISerializable
	{
		// Token: 0x06002317 RID: 8983 RVA: 0x0007E0A8 File Offset: 0x0007C2A8
		private Pointer()
		{
		}

		// Token: 0x06002318 RID: 8984 RVA: 0x0007E0B0 File Offset: 0x0007C2B0
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotSupportedException("Pointer deserializatioon not supported.");
		}

		// Token: 0x06002319 RID: 8985 RVA: 0x0007E0BC File Offset: 0x0007C2BC
		public unsafe static object Box(void* ptr, Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			if (!type.IsPointer)
			{
				throw new ArgumentException("type");
			}
			return new Pointer
			{
				data = ptr,
				type = type
			};
		}

		// Token: 0x0600231A RID: 8986 RVA: 0x0007E108 File Offset: 0x0007C308
		public unsafe static void* Unbox(object ptr)
		{
			Pointer pointer = ptr as Pointer;
			if (pointer == null)
			{
				throw new ArgumentException("ptr");
			}
			return pointer.data;
		}

		// Token: 0x04000D20 RID: 3360
		private unsafe void* data;

		// Token: 0x04000D21 RID: 3361
		private Type type;
	}
}
