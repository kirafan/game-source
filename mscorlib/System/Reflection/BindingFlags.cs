﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000284 RID: 644
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum BindingFlags
	{
		// Token: 0x04000C26 RID: 3110
		Default = 0,
		// Token: 0x04000C27 RID: 3111
		IgnoreCase = 1,
		// Token: 0x04000C28 RID: 3112
		DeclaredOnly = 2,
		// Token: 0x04000C29 RID: 3113
		Instance = 4,
		// Token: 0x04000C2A RID: 3114
		Static = 8,
		// Token: 0x04000C2B RID: 3115
		Public = 16,
		// Token: 0x04000C2C RID: 3116
		NonPublic = 32,
		// Token: 0x04000C2D RID: 3117
		FlattenHierarchy = 64,
		// Token: 0x04000C2E RID: 3118
		InvokeMethod = 256,
		// Token: 0x04000C2F RID: 3119
		CreateInstance = 512,
		// Token: 0x04000C30 RID: 3120
		GetField = 1024,
		// Token: 0x04000C31 RID: 3121
		SetField = 2048,
		// Token: 0x04000C32 RID: 3122
		GetProperty = 4096,
		// Token: 0x04000C33 RID: 3123
		SetProperty = 8192,
		// Token: 0x04000C34 RID: 3124
		PutDispProperty = 16384,
		// Token: 0x04000C35 RID: 3125
		PutRefDispProperty = 32768,
		// Token: 0x04000C36 RID: 3126
		ExactBinding = 65536,
		// Token: 0x04000C37 RID: 3127
		SuppressChangeType = 131072,
		// Token: 0x04000C38 RID: 3128
		OptionalParamBinding = 262144,
		// Token: 0x04000C39 RID: 3129
		IgnoreReturn = 16777216
	}
}
