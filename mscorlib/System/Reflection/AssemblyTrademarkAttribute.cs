﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000281 RID: 641
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyTrademarkAttribute : Attribute
	{
		// Token: 0x06002102 RID: 8450 RVA: 0x000787B0 File Offset: 0x000769B0
		public AssemblyTrademarkAttribute(string trademark)
		{
			this.name = trademark;
		}

		// Token: 0x1700058C RID: 1420
		// (get) Token: 0x06002103 RID: 8451 RVA: 0x000787C0 File Offset: 0x000769C0
		public string Trademark
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C23 RID: 3107
		private string name;
	}
}
