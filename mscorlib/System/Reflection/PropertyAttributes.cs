﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002B5 RID: 693
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum PropertyAttributes
	{
		// Token: 0x04000D2F RID: 3375
		None = 0,
		// Token: 0x04000D30 RID: 3376
		SpecialName = 512,
		// Token: 0x04000D31 RID: 3377
		ReservedMask = 62464,
		// Token: 0x04000D32 RID: 3378
		RTSpecialName = 1024,
		// Token: 0x04000D33 RID: 3379
		HasDefault = 4096,
		// Token: 0x04000D34 RID: 3380
		Reserved2 = 8192,
		// Token: 0x04000D35 RID: 3381
		Reserved3 = 16384,
		// Token: 0x04000D36 RID: 3382
		Reserved4 = 32768
	}
}
