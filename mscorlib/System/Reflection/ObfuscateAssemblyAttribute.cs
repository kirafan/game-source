﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002AD RID: 685
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class ObfuscateAssemblyAttribute : Attribute
	{
		// Token: 0x060022E9 RID: 8937 RVA: 0x0007DB04 File Offset: 0x0007BD04
		public ObfuscateAssemblyAttribute(bool assemblyIsPrivate)
		{
			this.strip = true;
			this.is_private = assemblyIsPrivate;
		}

		// Token: 0x1700060D RID: 1549
		// (get) Token: 0x060022EA RID: 8938 RVA: 0x0007DB1C File Offset: 0x0007BD1C
		public bool AssemblyIsPrivate
		{
			get
			{
				return this.is_private;
			}
		}

		// Token: 0x1700060E RID: 1550
		// (get) Token: 0x060022EB RID: 8939 RVA: 0x0007DB24 File Offset: 0x0007BD24
		// (set) Token: 0x060022EC RID: 8940 RVA: 0x0007DB2C File Offset: 0x0007BD2C
		public bool StripAfterObfuscation
		{
			get
			{
				return this.strip;
			}
			set
			{
				this.strip = value;
			}
		}

		// Token: 0x04000D06 RID: 3334
		private bool is_private;

		// Token: 0x04000D07 RID: 3335
		private bool strip;
	}
}
