﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000277 RID: 631
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	public sealed class AssemblyFileVersionAttribute : Attribute
	{
		// Token: 0x060020C6 RID: 8390 RVA: 0x00077DAC File Offset: 0x00075FAC
		public AssemblyFileVersionAttribute(string version)
		{
			if (version == null)
			{
				throw new ArgumentNullException("version");
			}
			this.name = version;
		}

		// Token: 0x17000578 RID: 1400
		// (get) Token: 0x060020C7 RID: 8391 RVA: 0x00077DCC File Offset: 0x00075FCC
		public string Version
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C07 RID: 3079
		private string name;
	}
}
