﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000286 RID: 646
	[ComDefaultInterface(typeof(_ConstructorInfo))]
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[Serializable]
	public abstract class ConstructorInfo : MethodBase, _ConstructorInfo
	{
		// Token: 0x06002122 RID: 8482 RVA: 0x000798A8 File Offset: 0x00077AA8
		void _ConstructorInfo.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002123 RID: 8483 RVA: 0x000798B0 File Offset: 0x00077AB0
		void _ConstructorInfo.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002124 RID: 8484 RVA: 0x000798B8 File Offset: 0x00077AB8
		void _ConstructorInfo.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002125 RID: 8485 RVA: 0x000798C0 File Offset: 0x00077AC0
		void _ConstructorInfo.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002126 RID: 8486 RVA: 0x000798C8 File Offset: 0x00077AC8
		object _ConstructorInfo.Invoke_2(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			return this.Invoke(obj, invokeAttr, binder, parameters, culture);
		}

		// Token: 0x06002127 RID: 8487 RVA: 0x000798D8 File Offset: 0x00077AD8
		object _ConstructorInfo.Invoke_3(object obj, object[] parameters)
		{
			return base.Invoke(obj, parameters);
		}

		// Token: 0x06002128 RID: 8488 RVA: 0x000798E4 File Offset: 0x00077AE4
		object _ConstructorInfo.Invoke_4(BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture)
		{
			return this.Invoke(invokeAttr, binder, parameters, culture);
		}

		// Token: 0x06002129 RID: 8489 RVA: 0x000798F4 File Offset: 0x00077AF4
		object _ConstructorInfo.Invoke_5(object[] parameters)
		{
			return this.Invoke(parameters);
		}

		// Token: 0x1700058E RID: 1422
		// (get) Token: 0x0600212A RID: 8490 RVA: 0x00079900 File Offset: 0x00077B00
		[ComVisible(true)]
		public override MemberTypes MemberType
		{
			get
			{
				return MemberTypes.Constructor;
			}
		}

		// Token: 0x0600212B RID: 8491 RVA: 0x00079904 File Offset: 0x00077B04
		[DebuggerStepThrough]
		[DebuggerHidden]
		public object Invoke(object[] parameters)
		{
			if (parameters == null)
			{
				parameters = new object[0];
			}
			return this.Invoke(BindingFlags.CreateInstance, null, parameters, null);
		}

		// Token: 0x0600212C RID: 8492
		public abstract object Invoke(BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture);

		// Token: 0x0600212D RID: 8493 RVA: 0x00079924 File Offset: 0x00077B24
		virtual Type GetType()
		{
			return base.GetType();
		}

		// Token: 0x04000C40 RID: 3136
		[ComVisible(true)]
		public static readonly string ConstructorName = ".ctor";

		// Token: 0x04000C41 RID: 3137
		[ComVisible(true)]
		public static readonly string TypeConstructorName = ".cctor";
	}
}
