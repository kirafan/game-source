﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace System.Reflection
{
	// Token: 0x02000287 RID: 647
	[ComVisible(true)]
	[Serializable]
	public sealed class CustomAttributeData
	{
		// Token: 0x0600212E RID: 8494 RVA: 0x0007992C File Offset: 0x00077B2C
		internal CustomAttributeData(ConstructorInfo ctorInfo, object[] ctorArgs, object[] namedArgs)
		{
			this.ctorInfo = ctorInfo;
			this.ctorArgs = Array.AsReadOnly<CustomAttributeTypedArgument>((ctorArgs == null) ? new CustomAttributeTypedArgument[0] : CustomAttributeData.UnboxValues<CustomAttributeTypedArgument>(ctorArgs));
			this.namedArgs = Array.AsReadOnly<CustomAttributeNamedArgument>((namedArgs == null) ? new CustomAttributeNamedArgument[0] : CustomAttributeData.UnboxValues<CustomAttributeNamedArgument>(namedArgs));
		}

		// Token: 0x1700058F RID: 1423
		// (get) Token: 0x0600212F RID: 8495 RVA: 0x0007998C File Offset: 0x00077B8C
		[ComVisible(true)]
		public ConstructorInfo Constructor
		{
			get
			{
				return this.ctorInfo;
			}
		}

		// Token: 0x17000590 RID: 1424
		// (get) Token: 0x06002130 RID: 8496 RVA: 0x00079994 File Offset: 0x00077B94
		[ComVisible(true)]
		public IList<CustomAttributeTypedArgument> ConstructorArguments
		{
			get
			{
				return this.ctorArgs;
			}
		}

		// Token: 0x17000591 RID: 1425
		// (get) Token: 0x06002131 RID: 8497 RVA: 0x0007999C File Offset: 0x00077B9C
		public IList<CustomAttributeNamedArgument> NamedArguments
		{
			get
			{
				return this.namedArgs;
			}
		}

		// Token: 0x06002132 RID: 8498 RVA: 0x000799A4 File Offset: 0x00077BA4
		public static IList<CustomAttributeData> GetCustomAttributes(Assembly target)
		{
			return MonoCustomAttrs.GetCustomAttributesData(target);
		}

		// Token: 0x06002133 RID: 8499 RVA: 0x000799AC File Offset: 0x00077BAC
		public static IList<CustomAttributeData> GetCustomAttributes(MemberInfo target)
		{
			return MonoCustomAttrs.GetCustomAttributesData(target);
		}

		// Token: 0x06002134 RID: 8500 RVA: 0x000799B4 File Offset: 0x00077BB4
		public static IList<CustomAttributeData> GetCustomAttributes(Module target)
		{
			return MonoCustomAttrs.GetCustomAttributesData(target);
		}

		// Token: 0x06002135 RID: 8501 RVA: 0x000799BC File Offset: 0x00077BBC
		public static IList<CustomAttributeData> GetCustomAttributes(ParameterInfo target)
		{
			return MonoCustomAttrs.GetCustomAttributesData(target);
		}

		// Token: 0x06002136 RID: 8502 RVA: 0x000799C4 File Offset: 0x00077BC4
		public override string ToString()
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("[" + this.ctorInfo.DeclaringType.FullName + "(");
			for (int i = 0; i < this.ctorArgs.Count; i++)
			{
				stringBuilder.Append(this.ctorArgs[i].ToString());
				if (i + 1 < this.ctorArgs.Count)
				{
					stringBuilder.Append(", ");
				}
			}
			if (this.namedArgs.Count > 0)
			{
				stringBuilder.Append(", ");
			}
			for (int j = 0; j < this.namedArgs.Count; j++)
			{
				stringBuilder.Append(this.namedArgs[j].ToString());
				if (j + 1 < this.namedArgs.Count)
				{
					stringBuilder.Append(", ");
				}
			}
			stringBuilder.AppendFormat(")]", new object[0]);
			return stringBuilder.ToString();
		}

		// Token: 0x06002137 RID: 8503 RVA: 0x00079AE0 File Offset: 0x00077CE0
		private static T[] UnboxValues<T>(object[] values)
		{
			T[] array = new T[values.Length];
			for (int i = 0; i < values.Length; i++)
			{
				array[i] = (T)((object)values[i]);
			}
			return array;
		}

		// Token: 0x06002138 RID: 8504 RVA: 0x00079B1C File Offset: 0x00077D1C
		public override bool Equals(object obj)
		{
			CustomAttributeData customAttributeData = obj as CustomAttributeData;
			if (customAttributeData == null || customAttributeData.ctorInfo != this.ctorInfo || customAttributeData.ctorArgs.Count != this.ctorArgs.Count || customAttributeData.namedArgs.Count != this.namedArgs.Count)
			{
				return false;
			}
			for (int i = 0; i < this.ctorArgs.Count; i++)
			{
				if (this.ctorArgs[i].Equals(customAttributeData.ctorArgs[i]))
				{
					return false;
				}
			}
			for (int j = 0; j < this.namedArgs.Count; j++)
			{
				bool flag = false;
				for (int k = 0; k < customAttributeData.namedArgs.Count; k++)
				{
					if (this.namedArgs[j].Equals(customAttributeData.namedArgs[k]))
					{
						flag = true;
						break;
					}
				}
				if (!flag)
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002139 RID: 8505 RVA: 0x00079C44 File Offset: 0x00077E44
		public override int GetHashCode()
		{
			int num = this.ctorInfo.GetHashCode() << 16;
			for (int i = 0; i < this.ctorArgs.Count; i++)
			{
				num += (num ^ 7 + this.ctorArgs[i].GetHashCode() << i * 4);
			}
			for (int j = 0; j < this.namedArgs.Count; j++)
			{
				num += this.namedArgs[j].GetHashCode() << 5;
			}
			return num;
		}

		// Token: 0x04000C42 RID: 3138
		private ConstructorInfo ctorInfo;

		// Token: 0x04000C43 RID: 3139
		private IList<CustomAttributeTypedArgument> ctorArgs;

		// Token: 0x04000C44 RID: 3140
		private IList<CustomAttributeNamedArgument> namedArgs;
	}
}
