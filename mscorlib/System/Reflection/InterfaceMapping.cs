﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000293 RID: 659
	[ComVisible(true)]
	public struct InterfaceMapping
	{
		// Token: 0x04000C7C RID: 3196
		[ComVisible(true)]
		public MethodInfo[] InterfaceMethods;

		// Token: 0x04000C7D RID: 3197
		[ComVisible(true)]
		public Type InterfaceType;

		// Token: 0x04000C7E RID: 3198
		[ComVisible(true)]
		public MethodInfo[] TargetMethods;

		// Token: 0x04000C7F RID: 3199
		[ComVisible(true)]
		public Type TargetType;
	}
}
