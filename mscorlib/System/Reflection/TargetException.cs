﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x020002BB RID: 699
	[ComVisible(true)]
	[Serializable]
	public class TargetException : ApplicationException
	{
		// Token: 0x06002346 RID: 9030 RVA: 0x0007E67C File Offset: 0x0007C87C
		public TargetException() : base(Locale.GetText("Unable to invoke an invalid target."))
		{
		}

		// Token: 0x06002347 RID: 9031 RVA: 0x0007E690 File Offset: 0x0007C890
		public TargetException(string message) : base(message)
		{
		}

		// Token: 0x06002348 RID: 9032 RVA: 0x0007E69C File Offset: 0x0007C89C
		public TargetException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06002349 RID: 9033 RVA: 0x0007E6A8 File Offset: 0x0007C8A8
		protected TargetException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
