﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000056 RID: 86
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface)]
	[Serializable]
	public sealed class DefaultMemberAttribute : Attribute
	{
		// Token: 0x06000685 RID: 1669 RVA: 0x00014C74 File Offset: 0x00012E74
		public DefaultMemberAttribute(string memberName)
		{
			this.member_name = memberName;
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x06000686 RID: 1670 RVA: 0x00014C84 File Offset: 0x00012E84
		public string MemberName
		{
			get
			{
				return this.member_name;
			}
		}

		// Token: 0x040000A8 RID: 168
		private string member_name;
	}
}
