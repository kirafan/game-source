﻿using System;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002B0 RID: 688
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_ParameterInfo))]
	[Serializable]
	public class ParameterInfo : ICustomAttributeProvider, _ParameterInfo
	{
		// Token: 0x060022F6 RID: 8950 RVA: 0x0007DBBC File Offset: 0x0007BDBC
		protected ParameterInfo()
		{
		}

		// Token: 0x060022F7 RID: 8951 RVA: 0x0007DBC4 File Offset: 0x0007BDC4
		internal ParameterInfo(ParameterBuilder pb, Type type, MemberInfo member, int position)
		{
			this.ClassImpl = type;
			this.MemberImpl = member;
			if (pb != null)
			{
				this.NameImpl = pb.Name;
				this.PositionImpl = pb.Position - 1;
				this.AttrsImpl = (ParameterAttributes)pb.Attributes;
			}
			else
			{
				this.NameImpl = null;
				this.PositionImpl = position - 1;
				this.AttrsImpl = ParameterAttributes.None;
			}
		}

		// Token: 0x060022F8 RID: 8952 RVA: 0x0007DC30 File Offset: 0x0007BE30
		internal ParameterInfo(ParameterInfo pinfo, MemberInfo member)
		{
			this.ClassImpl = pinfo.ParameterType;
			this.MemberImpl = member;
			this.NameImpl = pinfo.Name;
			this.PositionImpl = pinfo.Position;
			this.AttrsImpl = pinfo.Attributes;
		}

		// Token: 0x060022F9 RID: 8953 RVA: 0x0007DC7C File Offset: 0x0007BE7C
		internal ParameterInfo(Type type, MemberInfo member, UnmanagedMarshal marshalAs)
		{
			this.ClassImpl = type;
			this.MemberImpl = member;
			this.NameImpl = string.Empty;
			this.PositionImpl = -1;
			this.AttrsImpl = ParameterAttributes.Retval;
			this.marshalAs = marshalAs;
		}

		// Token: 0x060022FA RID: 8954 RVA: 0x0007DCC0 File Offset: 0x0007BEC0
		void _ParameterInfo.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060022FB RID: 8955 RVA: 0x0007DCC8 File Offset: 0x0007BEC8
		void _ParameterInfo.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060022FC RID: 8956 RVA: 0x0007DCD0 File Offset: 0x0007BED0
		void _ParameterInfo.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060022FD RID: 8957 RVA: 0x0007DCD8 File Offset: 0x0007BED8
		void _ParameterInfo.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060022FE RID: 8958 RVA: 0x0007DCE0 File Offset: 0x0007BEE0
		public override string ToString()
		{
			Type type = this.ClassImpl;
			while (type.HasElementType)
			{
				type = type.GetElementType();
			}
			bool flag = type.IsPrimitive || this.ClassImpl == typeof(void) || this.ClassImpl.Namespace == this.MemberImpl.DeclaringType.Namespace;
			string text = (!flag) ? this.ClassImpl.FullName : this.ClassImpl.Name;
			if (!this.IsRetval)
			{
				text += ' ';
				text += this.NameImpl;
			}
			return text;
		}

		// Token: 0x17000613 RID: 1555
		// (get) Token: 0x060022FF RID: 8959 RVA: 0x0007DD98 File Offset: 0x0007BF98
		public virtual Type ParameterType
		{
			get
			{
				return this.ClassImpl;
			}
		}

		// Token: 0x17000614 RID: 1556
		// (get) Token: 0x06002300 RID: 8960 RVA: 0x0007DDA0 File Offset: 0x0007BFA0
		public virtual ParameterAttributes Attributes
		{
			get
			{
				return this.AttrsImpl;
			}
		}

		// Token: 0x17000615 RID: 1557
		// (get) Token: 0x06002301 RID: 8961 RVA: 0x0007DDA8 File Offset: 0x0007BFA8
		public virtual object DefaultValue
		{
			get
			{
				if (this.ClassImpl == typeof(decimal))
				{
					DecimalConstantAttribute[] array = (DecimalConstantAttribute[])this.GetCustomAttributes(typeof(DecimalConstantAttribute), false);
					if (array.Length > 0)
					{
						return array[0].Value;
					}
				}
				else if (this.ClassImpl == typeof(DateTime))
				{
					DateTimeConstantAttribute[] array2 = (DateTimeConstantAttribute[])this.GetCustomAttributes(typeof(DateTimeConstantAttribute), false);
					if (array2.Length > 0)
					{
						return new DateTime(array2[0].Ticks);
					}
				}
				return this.DefaultValueImpl;
			}
		}

		// Token: 0x17000616 RID: 1558
		// (get) Token: 0x06002302 RID: 8962 RVA: 0x0007DE4C File Offset: 0x0007C04C
		public bool IsIn
		{
			get
			{
				return (this.Attributes & ParameterAttributes.In) != ParameterAttributes.None;
			}
		}

		// Token: 0x17000617 RID: 1559
		// (get) Token: 0x06002303 RID: 8963 RVA: 0x0007DE5C File Offset: 0x0007C05C
		public bool IsLcid
		{
			get
			{
				return (this.Attributes & ParameterAttributes.Lcid) != ParameterAttributes.None;
			}
		}

		// Token: 0x17000618 RID: 1560
		// (get) Token: 0x06002304 RID: 8964 RVA: 0x0007DE6C File Offset: 0x0007C06C
		public bool IsOptional
		{
			get
			{
				return (this.Attributes & ParameterAttributes.Optional) != ParameterAttributes.None;
			}
		}

		// Token: 0x17000619 RID: 1561
		// (get) Token: 0x06002305 RID: 8965 RVA: 0x0007DE80 File Offset: 0x0007C080
		public bool IsOut
		{
			get
			{
				return (this.Attributes & ParameterAttributes.Out) != ParameterAttributes.None;
			}
		}

		// Token: 0x1700061A RID: 1562
		// (get) Token: 0x06002306 RID: 8966 RVA: 0x0007DE90 File Offset: 0x0007C090
		public bool IsRetval
		{
			get
			{
				return (this.Attributes & ParameterAttributes.Retval) != ParameterAttributes.None;
			}
		}

		// Token: 0x1700061B RID: 1563
		// (get) Token: 0x06002307 RID: 8967 RVA: 0x0007DEA0 File Offset: 0x0007C0A0
		public virtual MemberInfo Member
		{
			get
			{
				return this.MemberImpl;
			}
		}

		// Token: 0x1700061C RID: 1564
		// (get) Token: 0x06002308 RID: 8968 RVA: 0x0007DEA8 File Offset: 0x0007C0A8
		public virtual string Name
		{
			get
			{
				return this.NameImpl;
			}
		}

		// Token: 0x1700061D RID: 1565
		// (get) Token: 0x06002309 RID: 8969 RVA: 0x0007DEB0 File Offset: 0x0007C0B0
		public virtual int Position
		{
			get
			{
				return this.PositionImpl;
			}
		}

		// Token: 0x0600230A RID: 8970
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetMetadataToken();

		// Token: 0x1700061E RID: 1566
		// (get) Token: 0x0600230B RID: 8971 RVA: 0x0007DEB8 File Offset: 0x0007C0B8
		public int MetadataToken
		{
			get
			{
				if (this.MemberImpl is PropertyInfo)
				{
					PropertyInfo propertyInfo = (PropertyInfo)this.MemberImpl;
					MethodInfo methodInfo = propertyInfo.GetGetMethod(true);
					if (methodInfo == null)
					{
						methodInfo = propertyInfo.GetSetMethod(true);
					}
					return methodInfo.GetParameters()[this.PositionImpl].MetadataToken;
				}
				if (this.MemberImpl is MethodBase)
				{
					return this.GetMetadataToken();
				}
				throw new ArgumentException("Can't produce MetadataToken for member of type " + this.MemberImpl.GetType());
			}
		}

		// Token: 0x0600230C RID: 8972 RVA: 0x0007DF3C File Offset: 0x0007C13C
		public virtual object[] GetCustomAttributes(bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, inherit);
		}

		// Token: 0x0600230D RID: 8973 RVA: 0x0007DF48 File Offset: 0x0007C148
		public virtual object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.GetCustomAttributes(this, attributeType, inherit);
		}

		// Token: 0x0600230E RID: 8974 RVA: 0x0007DF54 File Offset: 0x0007C154
		public virtual bool IsDefined(Type attributeType, bool inherit)
		{
			return MonoCustomAttrs.IsDefined(this, attributeType, inherit);
		}

		// Token: 0x0600230F RID: 8975 RVA: 0x0007DF60 File Offset: 0x0007C160
		internal object[] GetPseudoCustomAttributes()
		{
			int num = 0;
			if (this.IsIn)
			{
				num++;
			}
			if (this.IsOut)
			{
				num++;
			}
			if (this.IsOptional)
			{
				num++;
			}
			if (this.marshalAs != null)
			{
				num++;
			}
			if (num == 0)
			{
				return null;
			}
			object[] array = new object[num];
			num = 0;
			if (this.IsIn)
			{
				array[num++] = new InAttribute();
			}
			if (this.IsOptional)
			{
				array[num++] = new OptionalAttribute();
			}
			if (this.IsOut)
			{
				array[num++] = new OutAttribute();
			}
			if (this.marshalAs != null)
			{
				array[num++] = this.marshalAs.ToMarshalAsAttribute();
			}
			return array;
		}

		// Token: 0x06002310 RID: 8976
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type[] GetTypeModifiers(bool optional);

		// Token: 0x06002311 RID: 8977 RVA: 0x0007E020 File Offset: 0x0007C220
		public virtual Type[] GetOptionalCustomModifiers()
		{
			Type[] typeModifiers = this.GetTypeModifiers(true);
			if (typeModifiers == null)
			{
				return Type.EmptyTypes;
			}
			return typeModifiers;
		}

		// Token: 0x06002312 RID: 8978 RVA: 0x0007E044 File Offset: 0x0007C244
		public virtual Type[] GetRequiredCustomModifiers()
		{
			Type[] typeModifiers = this.GetTypeModifiers(false);
			if (typeModifiers == null)
			{
				return Type.EmptyTypes;
			}
			return typeModifiers;
		}

		// Token: 0x1700061F RID: 1567
		// (get) Token: 0x06002313 RID: 8979 RVA: 0x0007E068 File Offset: 0x0007C268
		public virtual object RawDefaultValue
		{
			get
			{
				return this.DefaultValue;
			}
		}

		// Token: 0x04000D18 RID: 3352
		protected Type ClassImpl;

		// Token: 0x04000D19 RID: 3353
		protected object DefaultValueImpl;

		// Token: 0x04000D1A RID: 3354
		protected MemberInfo MemberImpl;

		// Token: 0x04000D1B RID: 3355
		protected string NameImpl;

		// Token: 0x04000D1C RID: 3356
		protected int PositionImpl;

		// Token: 0x04000D1D RID: 3357
		protected ParameterAttributes AttrsImpl;

		// Token: 0x04000D1E RID: 3358
		private UnmanagedMarshal marshalAs;
	}
}
