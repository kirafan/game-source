﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200029D RID: 669
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_MethodInfo))]
	[ClassInterface(ClassInterfaceType.None)]
	[Serializable]
	public abstract class MethodInfo : MethodBase, _MethodInfo
	{
		// Token: 0x060021D9 RID: 8665 RVA: 0x0007ADA8 File Offset: 0x00078FA8
		void _MethodInfo.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021DA RID: 8666 RVA: 0x0007ADB0 File Offset: 0x00078FB0
		void _MethodInfo.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021DB RID: 8667 RVA: 0x0007ADB8 File Offset: 0x00078FB8
		void _MethodInfo.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021DC RID: 8668 RVA: 0x0007ADC0 File Offset: 0x00078FC0
		void _MethodInfo.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021DD RID: 8669
		public abstract MethodInfo GetBaseDefinition();

		// Token: 0x170005D1 RID: 1489
		// (get) Token: 0x060021DE RID: 8670 RVA: 0x0007ADC8 File Offset: 0x00078FC8
		public override MemberTypes MemberType
		{
			get
			{
				return MemberTypes.Method;
			}
		}

		// Token: 0x170005D2 RID: 1490
		// (get) Token: 0x060021DF RID: 8671 RVA: 0x0007ADCC File Offset: 0x00078FCC
		public virtual Type ReturnType
		{
			get
			{
				return null;
			}
		}

		// Token: 0x170005D3 RID: 1491
		// (get) Token: 0x060021E0 RID: 8672
		public abstract ICustomAttributeProvider ReturnTypeCustomAttributes { get; }

		// Token: 0x060021E1 RID: 8673 RVA: 0x0007ADD0 File Offset: 0x00078FD0
		[ComVisible(true)]
		public virtual MethodInfo GetGenericMethodDefinition()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060021E2 RID: 8674 RVA: 0x0007ADD8 File Offset: 0x00078FD8
		public virtual MethodInfo MakeGenericMethod(params Type[] typeArguments)
		{
			throw new NotSupportedException(base.GetType().ToString());
		}

		// Token: 0x060021E3 RID: 8675 RVA: 0x0007ADEC File Offset: 0x00078FEC
		[ComVisible(true)]
		public override Type[] GetGenericArguments()
		{
			return Type.EmptyTypes;
		}

		// Token: 0x170005D4 RID: 1492
		// (get) Token: 0x060021E4 RID: 8676 RVA: 0x0007ADF4 File Offset: 0x00078FF4
		public override bool IsGenericMethod
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170005D5 RID: 1493
		// (get) Token: 0x060021E5 RID: 8677 RVA: 0x0007ADF8 File Offset: 0x00078FF8
		public override bool IsGenericMethodDefinition
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170005D6 RID: 1494
		// (get) Token: 0x060021E6 RID: 8678 RVA: 0x0007ADFC File Offset: 0x00078FFC
		public override bool ContainsGenericParameters
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170005D7 RID: 1495
		// (get) Token: 0x060021E7 RID: 8679 RVA: 0x0007AE00 File Offset: 0x00079000
		public virtual ParameterInfo ReturnParameter
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x060021E8 RID: 8680 RVA: 0x0007AE08 File Offset: 0x00079008
		virtual Type GetType()
		{
			return base.GetType();
		}
	}
}
