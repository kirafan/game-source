﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200029A RID: 666
	[ComDefaultInterface(typeof(_MethodBase))]
	[ClassInterface(ClassInterfaceType.None)]
	[ComVisible(true)]
	[Serializable]
	public abstract class MethodBase : MemberInfo, _MethodBase
	{
		// Token: 0x060021A9 RID: 8617 RVA: 0x0007AAB4 File Offset: 0x00078CB4
		void _MethodBase.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021AA RID: 8618 RVA: 0x0007AABC File Offset: 0x00078CBC
		void _MethodBase.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021AB RID: 8619 RVA: 0x0007AAC4 File Offset: 0x00078CC4
		void _MethodBase.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021AC RID: 8620 RVA: 0x0007AACC File Offset: 0x00078CCC
		void _MethodBase.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060021AD RID: 8621
		[MethodImpl(MethodImplOptions.InternalCall)]
		public static extern MethodBase GetCurrentMethod();

		// Token: 0x060021AE RID: 8622 RVA: 0x0007AAD4 File Offset: 0x00078CD4
		internal static MethodBase GetMethodFromHandleNoGenericCheck(RuntimeMethodHandle handle)
		{
			return MethodBase.GetMethodFromIntPtr(handle.Value, IntPtr.Zero);
		}

		// Token: 0x060021AF RID: 8623 RVA: 0x0007AAE8 File Offset: 0x00078CE8
		private static MethodBase GetMethodFromIntPtr(IntPtr handle, IntPtr declaringType)
		{
			if (handle == IntPtr.Zero)
			{
				throw new ArgumentException("The handle is invalid.");
			}
			MethodBase methodFromHandleInternalType = MethodBase.GetMethodFromHandleInternalType(handle, declaringType);
			if (methodFromHandleInternalType == null)
			{
				throw new ArgumentException("The handle is invalid.");
			}
			return methodFromHandleInternalType;
		}

		// Token: 0x060021B0 RID: 8624 RVA: 0x0007AB2C File Offset: 0x00078D2C
		public static MethodBase GetMethodFromHandle(RuntimeMethodHandle handle)
		{
			MethodBase methodFromIntPtr = MethodBase.GetMethodFromIntPtr(handle.Value, IntPtr.Zero);
			Type declaringType = methodFromIntPtr.DeclaringType;
			if (declaringType.IsGenericType || declaringType.IsGenericTypeDefinition)
			{
				throw new ArgumentException("Cannot resolve method because it's declared in a generic class.");
			}
			return methodFromIntPtr;
		}

		// Token: 0x060021B1 RID: 8625
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern MethodBase GetMethodFromHandleInternalType(IntPtr method_handle, IntPtr type_handle);

		// Token: 0x060021B2 RID: 8626 RVA: 0x0007AB74 File Offset: 0x00078D74
		[ComVisible(false)]
		public static MethodBase GetMethodFromHandle(RuntimeMethodHandle handle, RuntimeTypeHandle declaringType)
		{
			return MethodBase.GetMethodFromIntPtr(handle.Value, declaringType.Value);
		}

		// Token: 0x060021B3 RID: 8627
		public abstract MethodImplAttributes GetMethodImplementationFlags();

		// Token: 0x060021B4 RID: 8628
		public abstract ParameterInfo[] GetParameters();

		// Token: 0x060021B5 RID: 8629 RVA: 0x0007AB8C File Offset: 0x00078D8C
		internal virtual int GetParameterCount()
		{
			ParameterInfo[] parameters = this.GetParameters();
			if (parameters == null)
			{
				return 0;
			}
			return parameters.Length;
		}

		// Token: 0x060021B6 RID: 8630 RVA: 0x0007ABAC File Offset: 0x00078DAC
		[DebuggerHidden]
		[DebuggerStepThrough]
		public object Invoke(object obj, object[] parameters)
		{
			return this.Invoke(obj, BindingFlags.Default, null, parameters, null);
		}

		// Token: 0x060021B7 RID: 8631
		public abstract object Invoke(object obj, BindingFlags invokeAttr, Binder binder, object[] parameters, CultureInfo culture);

		// Token: 0x170005B9 RID: 1465
		// (get) Token: 0x060021B8 RID: 8632
		public abstract RuntimeMethodHandle MethodHandle { get; }

		// Token: 0x170005BA RID: 1466
		// (get) Token: 0x060021B9 RID: 8633
		public abstract MethodAttributes Attributes { get; }

		// Token: 0x170005BB RID: 1467
		// (get) Token: 0x060021BA RID: 8634 RVA: 0x0007ABBC File Offset: 0x00078DBC
		public virtual CallingConventions CallingConvention
		{
			get
			{
				return CallingConventions.Standard;
			}
		}

		// Token: 0x170005BC RID: 1468
		// (get) Token: 0x060021BB RID: 8635 RVA: 0x0007ABC0 File Offset: 0x00078DC0
		public bool IsPublic
		{
			get
			{
				return (this.Attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.Public;
			}
		}

		// Token: 0x170005BD RID: 1469
		// (get) Token: 0x060021BC RID: 8636 RVA: 0x0007ABD0 File Offset: 0x00078DD0
		public bool IsPrivate
		{
			get
			{
				return (this.Attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.Private;
			}
		}

		// Token: 0x170005BE RID: 1470
		// (get) Token: 0x060021BD RID: 8637 RVA: 0x0007ABE0 File Offset: 0x00078DE0
		public bool IsFamily
		{
			get
			{
				return (this.Attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.Family;
			}
		}

		// Token: 0x170005BF RID: 1471
		// (get) Token: 0x060021BE RID: 8638 RVA: 0x0007ABF0 File Offset: 0x00078DF0
		public bool IsAssembly
		{
			get
			{
				return (this.Attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.Assembly;
			}
		}

		// Token: 0x170005C0 RID: 1472
		// (get) Token: 0x060021BF RID: 8639 RVA: 0x0007AC00 File Offset: 0x00078E00
		public bool IsFamilyAndAssembly
		{
			get
			{
				return (this.Attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.FamANDAssem;
			}
		}

		// Token: 0x170005C1 RID: 1473
		// (get) Token: 0x060021C0 RID: 8640 RVA: 0x0007AC10 File Offset: 0x00078E10
		public bool IsFamilyOrAssembly
		{
			get
			{
				return (this.Attributes & MethodAttributes.MemberAccessMask) == MethodAttributes.FamORAssem;
			}
		}

		// Token: 0x170005C2 RID: 1474
		// (get) Token: 0x060021C1 RID: 8641 RVA: 0x0007AC20 File Offset: 0x00078E20
		public bool IsStatic
		{
			get
			{
				return (this.Attributes & MethodAttributes.Static) != MethodAttributes.PrivateScope;
			}
		}

		// Token: 0x170005C3 RID: 1475
		// (get) Token: 0x060021C2 RID: 8642 RVA: 0x0007AC34 File Offset: 0x00078E34
		public bool IsFinal
		{
			get
			{
				return (this.Attributes & MethodAttributes.Final) != MethodAttributes.PrivateScope;
			}
		}

		// Token: 0x170005C4 RID: 1476
		// (get) Token: 0x060021C3 RID: 8643 RVA: 0x0007AC48 File Offset: 0x00078E48
		public bool IsVirtual
		{
			get
			{
				return (this.Attributes & MethodAttributes.Virtual) != MethodAttributes.PrivateScope;
			}
		}

		// Token: 0x170005C5 RID: 1477
		// (get) Token: 0x060021C4 RID: 8644 RVA: 0x0007AC5C File Offset: 0x00078E5C
		public bool IsHideBySig
		{
			get
			{
				return (this.Attributes & MethodAttributes.HideBySig) != MethodAttributes.PrivateScope;
			}
		}

		// Token: 0x170005C6 RID: 1478
		// (get) Token: 0x060021C5 RID: 8645 RVA: 0x0007AC70 File Offset: 0x00078E70
		public bool IsAbstract
		{
			get
			{
				return (this.Attributes & MethodAttributes.Abstract) != MethodAttributes.PrivateScope;
			}
		}

		// Token: 0x170005C7 RID: 1479
		// (get) Token: 0x060021C6 RID: 8646 RVA: 0x0007AC84 File Offset: 0x00078E84
		public bool IsSpecialName
		{
			get
			{
				int attributes = (int)this.Attributes;
				return (attributes & 2048) != 0;
			}
		}

		// Token: 0x170005C8 RID: 1480
		// (get) Token: 0x060021C7 RID: 8647 RVA: 0x0007ACA8 File Offset: 0x00078EA8
		[ComVisible(true)]
		public bool IsConstructor
		{
			get
			{
				int attributes = (int)this.Attributes;
				return (attributes & 4096) != 0 && this.Name == ".ctor";
			}
		}

		// Token: 0x060021C8 RID: 8648 RVA: 0x0007ACDC File Offset: 0x00078EDC
		internal virtual int get_next_table_index(object obj, int table, bool inc)
		{
			if (this is MethodBuilder)
			{
				MethodBuilder methodBuilder = (MethodBuilder)this;
				return methodBuilder.get_next_table_index(obj, table, inc);
			}
			if (this is ConstructorBuilder)
			{
				ConstructorBuilder constructorBuilder = (ConstructorBuilder)this;
				return constructorBuilder.get_next_table_index(obj, table, inc);
			}
			throw new Exception("Method is not a builder method");
		}

		// Token: 0x060021C9 RID: 8649 RVA: 0x0007AD2C File Offset: 0x00078F2C
		[ComVisible(true)]
		public virtual Type[] GetGenericArguments()
		{
			throw new NotSupportedException();
		}

		// Token: 0x170005C9 RID: 1481
		// (get) Token: 0x060021CA RID: 8650 RVA: 0x0007AD34 File Offset: 0x00078F34
		public virtual bool ContainsGenericParameters
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170005CA RID: 1482
		// (get) Token: 0x060021CB RID: 8651 RVA: 0x0007AD38 File Offset: 0x00078F38
		public virtual bool IsGenericMethodDefinition
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170005CB RID: 1483
		// (get) Token: 0x060021CC RID: 8652 RVA: 0x0007AD3C File Offset: 0x00078F3C
		public virtual bool IsGenericMethod
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060021CD RID: 8653
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern MethodBody GetMethodBodyInternal(IntPtr handle);

		// Token: 0x060021CE RID: 8654 RVA: 0x0007AD40 File Offset: 0x00078F40
		internal static MethodBody GetMethodBody(IntPtr handle)
		{
			return MethodBase.GetMethodBodyInternal(handle);
		}

		// Token: 0x060021CF RID: 8655 RVA: 0x0007AD48 File Offset: 0x00078F48
		public virtual MethodBody GetMethodBody()
		{
			throw new NotSupportedException();
		}

		// Token: 0x060021D0 RID: 8656 RVA: 0x0007AD50 File Offset: 0x00078F50
		virtual Type GetType()
		{
			return base.GetType();
		}
	}
}
