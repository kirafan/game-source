﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200029B RID: 667
	[ComVisible(true)]
	public sealed class MethodBody
	{
		// Token: 0x060021D1 RID: 8657 RVA: 0x0007AD58 File Offset: 0x00078F58
		internal MethodBody()
		{
		}

		// Token: 0x170005CC RID: 1484
		// (get) Token: 0x060021D2 RID: 8658 RVA: 0x0007AD60 File Offset: 0x00078F60
		public IList<ExceptionHandlingClause> ExceptionHandlingClauses
		{
			get
			{
				return Array.AsReadOnly<ExceptionHandlingClause>(this.clauses);
			}
		}

		// Token: 0x170005CD RID: 1485
		// (get) Token: 0x060021D3 RID: 8659 RVA: 0x0007AD70 File Offset: 0x00078F70
		public IList<LocalVariableInfo> LocalVariables
		{
			get
			{
				return Array.AsReadOnly<LocalVariableInfo>(this.locals);
			}
		}

		// Token: 0x170005CE RID: 1486
		// (get) Token: 0x060021D4 RID: 8660 RVA: 0x0007AD80 File Offset: 0x00078F80
		public bool InitLocals
		{
			get
			{
				return this.init_locals;
			}
		}

		// Token: 0x170005CF RID: 1487
		// (get) Token: 0x060021D5 RID: 8661 RVA: 0x0007AD88 File Offset: 0x00078F88
		public int LocalSignatureMetadataToken
		{
			get
			{
				return this.sig_token;
			}
		}

		// Token: 0x170005D0 RID: 1488
		// (get) Token: 0x060021D6 RID: 8662 RVA: 0x0007AD90 File Offset: 0x00078F90
		public int MaxStackSize
		{
			get
			{
				return this.max_stack;
			}
		}

		// Token: 0x060021D7 RID: 8663 RVA: 0x0007AD98 File Offset: 0x00078F98
		public byte[] GetILAsByteArray()
		{
			return this.il;
		}

		// Token: 0x04000CAF RID: 3247
		private ExceptionHandlingClause[] clauses;

		// Token: 0x04000CB0 RID: 3248
		private LocalVariableInfo[] locals;

		// Token: 0x04000CB1 RID: 3249
		private byte[] il;

		// Token: 0x04000CB2 RID: 3250
		private bool init_locals;

		// Token: 0x04000CB3 RID: 3251
		private int sig_token;

		// Token: 0x04000CB4 RID: 3252
		private int max_stack;
	}
}
