﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000295 RID: 661
	[ComVisible(true)]
	public class LocalVariableInfo
	{
		// Token: 0x06002199 RID: 8601 RVA: 0x0007A66C File Offset: 0x0007886C
		internal LocalVariableInfo()
		{
		}

		// Token: 0x170005B3 RID: 1459
		// (get) Token: 0x0600219A RID: 8602 RVA: 0x0007A674 File Offset: 0x00078874
		public virtual bool IsPinned
		{
			get
			{
				return this.is_pinned;
			}
		}

		// Token: 0x170005B4 RID: 1460
		// (get) Token: 0x0600219B RID: 8603 RVA: 0x0007A67C File Offset: 0x0007887C
		public virtual int LocalIndex
		{
			get
			{
				return (int)this.position;
			}
		}

		// Token: 0x170005B5 RID: 1461
		// (get) Token: 0x0600219C RID: 8604 RVA: 0x0007A684 File Offset: 0x00078884
		public virtual Type LocalType
		{
			get
			{
				return this.type;
			}
		}

		// Token: 0x0600219D RID: 8605 RVA: 0x0007A68C File Offset: 0x0007888C
		public override string ToString()
		{
			if (this.is_pinned)
			{
				return string.Format("{0} ({1}) (pinned)", this.type, this.position);
			}
			return string.Format("{0} ({1})", this.type, this.position);
		}

		// Token: 0x04000C80 RID: 3200
		internal Type type;

		// Token: 0x04000C81 RID: 3201
		internal bool is_pinned;

		// Token: 0x04000C82 RID: 3202
		internal ushort position;
	}
}
