﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200004A RID: 74
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyVersionAttribute : Attribute
	{
		// Token: 0x0600066E RID: 1646 RVA: 0x00014B74 File Offset: 0x00012D74
		public AssemblyVersionAttribute(string version)
		{
			this.name = version;
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x0600066F RID: 1647 RVA: 0x00014B84 File Offset: 0x00012D84
		public string Version
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x0400009F RID: 159
		private string name;
	}
}
