﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System.Reflection
{
	// Token: 0x02000288 RID: 648
	[ComVisible(true)]
	[Serializable]
	public class CustomAttributeFormatException : FormatException
	{
		// Token: 0x0600213A RID: 8506 RVA: 0x00079CD8 File Offset: 0x00077ED8
		public CustomAttributeFormatException() : base(Locale.GetText("The Binary format of the custom attribute is invalid."))
		{
		}

		// Token: 0x0600213B RID: 8507 RVA: 0x00079CEC File Offset: 0x00077EEC
		public CustomAttributeFormatException(string message) : base(message)
		{
		}

		// Token: 0x0600213C RID: 8508 RVA: 0x00079CF8 File Offset: 0x00077EF8
		public CustomAttributeFormatException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x0600213D RID: 8509 RVA: 0x00079D04 File Offset: 0x00077F04
		protected CustomAttributeFormatException(SerializationInfo info, StreamingContext context)
		{
		}
	}
}
