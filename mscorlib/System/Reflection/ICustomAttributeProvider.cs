﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000035 RID: 53
	[ComVisible(true)]
	public interface ICustomAttributeProvider
	{
		// Token: 0x06000585 RID: 1413
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x06000586 RID: 1414
		object[] GetCustomAttributes(Type attributeType, bool inherit);

		// Token: 0x06000587 RID: 1415
		bool IsDefined(Type attributeType, bool inherit);
	}
}
