﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x0200027B RID: 635
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyKeyNameAttribute : Attribute
	{
		// Token: 0x060020D1 RID: 8401 RVA: 0x00077E44 File Offset: 0x00076044
		public AssemblyKeyNameAttribute(string keyName)
		{
			this.name = keyName;
		}

		// Token: 0x1700057D RID: 1405
		// (get) Token: 0x060020D2 RID: 8402 RVA: 0x00077E54 File Offset: 0x00076054
		public string KeyName
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C0B RID: 3083
		private string name;
	}
}
