﻿using System;
using System.Globalization;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002BF RID: 703
	[ComVisible(true)]
	[Serializable]
	public class TypeDelegator : Type
	{
		// Token: 0x06002351 RID: 9041 RVA: 0x0007E714 File Offset: 0x0007C914
		protected TypeDelegator()
		{
		}

		// Token: 0x06002352 RID: 9042 RVA: 0x0007E71C File Offset: 0x0007C91C
		public TypeDelegator(Type delegatingType)
		{
			if (delegatingType == null)
			{
				throw new ArgumentNullException("delegatingType must be non-null");
			}
			this.typeImpl = delegatingType;
		}

		// Token: 0x1700062A RID: 1578
		// (get) Token: 0x06002353 RID: 9043 RVA: 0x0007E73C File Offset: 0x0007C93C
		public override Assembly Assembly
		{
			get
			{
				return this.typeImpl.Assembly;
			}
		}

		// Token: 0x1700062B RID: 1579
		// (get) Token: 0x06002354 RID: 9044 RVA: 0x0007E74C File Offset: 0x0007C94C
		public override string AssemblyQualifiedName
		{
			get
			{
				return this.typeImpl.AssemblyQualifiedName;
			}
		}

		// Token: 0x1700062C RID: 1580
		// (get) Token: 0x06002355 RID: 9045 RVA: 0x0007E75C File Offset: 0x0007C95C
		public override Type BaseType
		{
			get
			{
				return this.typeImpl.BaseType;
			}
		}

		// Token: 0x1700062D RID: 1581
		// (get) Token: 0x06002356 RID: 9046 RVA: 0x0007E76C File Offset: 0x0007C96C
		public override string FullName
		{
			get
			{
				return this.typeImpl.FullName;
			}
		}

		// Token: 0x1700062E RID: 1582
		// (get) Token: 0x06002357 RID: 9047 RVA: 0x0007E77C File Offset: 0x0007C97C
		public override Guid GUID
		{
			get
			{
				return this.typeImpl.GUID;
			}
		}

		// Token: 0x1700062F RID: 1583
		// (get) Token: 0x06002358 RID: 9048 RVA: 0x0007E78C File Offset: 0x0007C98C
		public override Module Module
		{
			get
			{
				return this.typeImpl.Module;
			}
		}

		// Token: 0x17000630 RID: 1584
		// (get) Token: 0x06002359 RID: 9049 RVA: 0x0007E79C File Offset: 0x0007C99C
		public override string Name
		{
			get
			{
				return this.typeImpl.Name;
			}
		}

		// Token: 0x17000631 RID: 1585
		// (get) Token: 0x0600235A RID: 9050 RVA: 0x0007E7AC File Offset: 0x0007C9AC
		public override string Namespace
		{
			get
			{
				return this.typeImpl.Namespace;
			}
		}

		// Token: 0x17000632 RID: 1586
		// (get) Token: 0x0600235B RID: 9051 RVA: 0x0007E7BC File Offset: 0x0007C9BC
		public override RuntimeTypeHandle TypeHandle
		{
			get
			{
				return this.typeImpl.TypeHandle;
			}
		}

		// Token: 0x17000633 RID: 1587
		// (get) Token: 0x0600235C RID: 9052 RVA: 0x0007E7CC File Offset: 0x0007C9CC
		public override Type UnderlyingSystemType
		{
			get
			{
				return this.typeImpl.UnderlyingSystemType;
			}
		}

		// Token: 0x0600235D RID: 9053 RVA: 0x0007E7DC File Offset: 0x0007C9DC
		protected override TypeAttributes GetAttributeFlagsImpl()
		{
			return this.typeImpl.Attributes;
		}

		// Token: 0x0600235E RID: 9054 RVA: 0x0007E7EC File Offset: 0x0007C9EC
		protected override ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			return this.typeImpl.GetConstructor(bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x0600235F RID: 9055 RVA: 0x0007E800 File Offset: 0x0007CA00
		[ComVisible(true)]
		public override ConstructorInfo[] GetConstructors(BindingFlags bindingAttr)
		{
			return this.typeImpl.GetConstructors(bindingAttr);
		}

		// Token: 0x06002360 RID: 9056 RVA: 0x0007E810 File Offset: 0x0007CA10
		public override object[] GetCustomAttributes(bool inherit)
		{
			return this.typeImpl.GetCustomAttributes(inherit);
		}

		// Token: 0x06002361 RID: 9057 RVA: 0x0007E820 File Offset: 0x0007CA20
		public override object[] GetCustomAttributes(Type attributeType, bool inherit)
		{
			return this.typeImpl.GetCustomAttributes(attributeType, inherit);
		}

		// Token: 0x06002362 RID: 9058 RVA: 0x0007E830 File Offset: 0x0007CA30
		public override Type GetElementType()
		{
			return this.typeImpl.GetElementType();
		}

		// Token: 0x06002363 RID: 9059 RVA: 0x0007E840 File Offset: 0x0007CA40
		public override EventInfo GetEvent(string name, BindingFlags bindingAttr)
		{
			return this.typeImpl.GetEvent(name, bindingAttr);
		}

		// Token: 0x06002364 RID: 9060 RVA: 0x0007E850 File Offset: 0x0007CA50
		public override EventInfo[] GetEvents()
		{
			return this.GetEvents(BindingFlags.Public);
		}

		// Token: 0x06002365 RID: 9061 RVA: 0x0007E85C File Offset: 0x0007CA5C
		public override EventInfo[] GetEvents(BindingFlags bindingAttr)
		{
			return this.typeImpl.GetEvents(bindingAttr);
		}

		// Token: 0x06002366 RID: 9062 RVA: 0x0007E86C File Offset: 0x0007CA6C
		public override FieldInfo GetField(string name, BindingFlags bindingAttr)
		{
			return this.typeImpl.GetField(name, bindingAttr);
		}

		// Token: 0x06002367 RID: 9063 RVA: 0x0007E87C File Offset: 0x0007CA7C
		public override FieldInfo[] GetFields(BindingFlags bindingAttr)
		{
			return this.typeImpl.GetFields(bindingAttr);
		}

		// Token: 0x06002368 RID: 9064 RVA: 0x0007E88C File Offset: 0x0007CA8C
		public override Type GetInterface(string name, bool ignoreCase)
		{
			return this.typeImpl.GetInterface(name, ignoreCase);
		}

		// Token: 0x06002369 RID: 9065 RVA: 0x0007E89C File Offset: 0x0007CA9C
		[ComVisible(true)]
		public override InterfaceMapping GetInterfaceMap(Type interfaceType)
		{
			return this.typeImpl.GetInterfaceMap(interfaceType);
		}

		// Token: 0x0600236A RID: 9066 RVA: 0x0007E8AC File Offset: 0x0007CAAC
		public override Type[] GetInterfaces()
		{
			return this.typeImpl.GetInterfaces();
		}

		// Token: 0x0600236B RID: 9067 RVA: 0x0007E8BC File Offset: 0x0007CABC
		public override MemberInfo[] GetMember(string name, MemberTypes type, BindingFlags bindingAttr)
		{
			return this.typeImpl.GetMember(name, type, bindingAttr);
		}

		// Token: 0x0600236C RID: 9068 RVA: 0x0007E8CC File Offset: 0x0007CACC
		public override MemberInfo[] GetMembers(BindingFlags bindingAttr)
		{
			return this.typeImpl.GetMembers(bindingAttr);
		}

		// Token: 0x0600236D RID: 9069 RVA: 0x0007E8DC File Offset: 0x0007CADC
		protected override MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			return this.typeImpl.GetMethodImplInternal(name, bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x0600236E RID: 9070 RVA: 0x0007E8F4 File Offset: 0x0007CAF4
		public override MethodInfo[] GetMethods(BindingFlags bindingAttr)
		{
			return this.typeImpl.GetMethods(bindingAttr);
		}

		// Token: 0x0600236F RID: 9071 RVA: 0x0007E904 File Offset: 0x0007CB04
		public override Type GetNestedType(string name, BindingFlags bindingAttr)
		{
			return this.typeImpl.GetNestedType(name, bindingAttr);
		}

		// Token: 0x06002370 RID: 9072 RVA: 0x0007E914 File Offset: 0x0007CB14
		public override Type[] GetNestedTypes(BindingFlags bindingAttr)
		{
			return this.typeImpl.GetNestedTypes(bindingAttr);
		}

		// Token: 0x06002371 RID: 9073 RVA: 0x0007E924 File Offset: 0x0007CB24
		public override PropertyInfo[] GetProperties(BindingFlags bindingAttr)
		{
			return this.typeImpl.GetProperties(bindingAttr);
		}

		// Token: 0x06002372 RID: 9074 RVA: 0x0007E934 File Offset: 0x0007CB34
		protected override PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			return this.typeImpl.GetPropertyImplInternal(name, bindingAttr, binder, returnType, types, modifiers);
		}

		// Token: 0x06002373 RID: 9075 RVA: 0x0007E94C File Offset: 0x0007CB4C
		protected override bool HasElementTypeImpl()
		{
			return this.typeImpl.HasElementType;
		}

		// Token: 0x06002374 RID: 9076 RVA: 0x0007E95C File Offset: 0x0007CB5C
		public override object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters)
		{
			return this.typeImpl.InvokeMember(name, invokeAttr, binder, target, args, modifiers, culture, namedParameters);
		}

		// Token: 0x06002375 RID: 9077 RVA: 0x0007E984 File Offset: 0x0007CB84
		protected override bool IsArrayImpl()
		{
			return this.typeImpl.IsArray;
		}

		// Token: 0x06002376 RID: 9078 RVA: 0x0007E994 File Offset: 0x0007CB94
		protected override bool IsByRefImpl()
		{
			return this.typeImpl.IsByRef;
		}

		// Token: 0x06002377 RID: 9079 RVA: 0x0007E9A4 File Offset: 0x0007CBA4
		protected override bool IsCOMObjectImpl()
		{
			return this.typeImpl.IsCOMObject;
		}

		// Token: 0x06002378 RID: 9080 RVA: 0x0007E9B4 File Offset: 0x0007CBB4
		public override bool IsDefined(Type attributeType, bool inherit)
		{
			return this.typeImpl.IsDefined(attributeType, inherit);
		}

		// Token: 0x06002379 RID: 9081 RVA: 0x0007E9C4 File Offset: 0x0007CBC4
		protected override bool IsPointerImpl()
		{
			return this.typeImpl.IsPointer;
		}

		// Token: 0x0600237A RID: 9082 RVA: 0x0007E9D4 File Offset: 0x0007CBD4
		protected override bool IsPrimitiveImpl()
		{
			return this.typeImpl.IsPrimitive;
		}

		// Token: 0x0600237B RID: 9083 RVA: 0x0007E9E4 File Offset: 0x0007CBE4
		protected override bool IsValueTypeImpl()
		{
			return this.typeImpl.IsValueType;
		}

		// Token: 0x17000634 RID: 1588
		// (get) Token: 0x0600237C RID: 9084 RVA: 0x0007E9F4 File Offset: 0x0007CBF4
		public override int MetadataToken
		{
			get
			{
				return this.typeImpl.MetadataToken;
			}
		}

		// Token: 0x04000D65 RID: 3429
		protected Type typeImpl;
	}
}
