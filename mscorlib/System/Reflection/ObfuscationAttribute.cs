﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002AE RID: 686
	[ComVisible(true)]
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Enum | AttributeTargets.Method | AttributeTargets.Property | AttributeTargets.Field | AttributeTargets.Event | AttributeTargets.Interface | AttributeTargets.Parameter | AttributeTargets.Delegate, AllowMultiple = true, Inherited = false)]
	public sealed class ObfuscationAttribute : Attribute
	{
		// Token: 0x060022ED RID: 8941 RVA: 0x0007DB38 File Offset: 0x0007BD38
		public ObfuscationAttribute()
		{
			this.exclude = true;
			this.strip = true;
			this.applyToMembers = true;
			this.feature = "all";
		}

		// Token: 0x1700060F RID: 1551
		// (get) Token: 0x060022EE RID: 8942 RVA: 0x0007DB6C File Offset: 0x0007BD6C
		// (set) Token: 0x060022EF RID: 8943 RVA: 0x0007DB74 File Offset: 0x0007BD74
		public bool Exclude
		{
			get
			{
				return this.exclude;
			}
			set
			{
				this.exclude = value;
			}
		}

		// Token: 0x17000610 RID: 1552
		// (get) Token: 0x060022F0 RID: 8944 RVA: 0x0007DB80 File Offset: 0x0007BD80
		// (set) Token: 0x060022F1 RID: 8945 RVA: 0x0007DB88 File Offset: 0x0007BD88
		public bool StripAfterObfuscation
		{
			get
			{
				return this.strip;
			}
			set
			{
				this.strip = value;
			}
		}

		// Token: 0x17000611 RID: 1553
		// (get) Token: 0x060022F2 RID: 8946 RVA: 0x0007DB94 File Offset: 0x0007BD94
		// (set) Token: 0x060022F3 RID: 8947 RVA: 0x0007DB9C File Offset: 0x0007BD9C
		public bool ApplyToMembers
		{
			get
			{
				return this.applyToMembers;
			}
			set
			{
				this.applyToMembers = value;
			}
		}

		// Token: 0x17000612 RID: 1554
		// (get) Token: 0x060022F4 RID: 8948 RVA: 0x0007DBA8 File Offset: 0x0007BDA8
		// (set) Token: 0x060022F5 RID: 8949 RVA: 0x0007DBB0 File Offset: 0x0007BDB0
		public string Feature
		{
			get
			{
				return this.feature;
			}
			set
			{
				this.feature = value;
			}
		}

		// Token: 0x04000D08 RID: 3336
		private bool exclude;

		// Token: 0x04000D09 RID: 3337
		private bool strip;

		// Token: 0x04000D0A RID: 3338
		private bool applyToMembers;

		// Token: 0x04000D0B RID: 3339
		private string feature;
	}
}
