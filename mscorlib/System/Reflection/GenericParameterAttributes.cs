﻿using System;

namespace System.Reflection
{
	// Token: 0x02000291 RID: 657
	[Flags]
	public enum GenericParameterAttributes
	{
		// Token: 0x04000C70 RID: 3184
		Covariant = 1,
		// Token: 0x04000C71 RID: 3185
		Contravariant = 2,
		// Token: 0x04000C72 RID: 3186
		VarianceMask = 3,
		// Token: 0x04000C73 RID: 3187
		None = 0,
		// Token: 0x04000C74 RID: 3188
		ReferenceTypeConstraint = 4,
		// Token: 0x04000C75 RID: 3189
		NotNullableValueTypeConstraint = 8,
		// Token: 0x04000C76 RID: 3190
		DefaultConstructorConstraint = 16,
		// Token: 0x04000C77 RID: 3191
		SpecialConstraintMask = 28
	}
}
