﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x020002AF RID: 687
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum ParameterAttributes
	{
		// Token: 0x04000D0D RID: 3341
		None = 0,
		// Token: 0x04000D0E RID: 3342
		In = 1,
		// Token: 0x04000D0F RID: 3343
		Out = 2,
		// Token: 0x04000D10 RID: 3344
		Lcid = 4,
		// Token: 0x04000D11 RID: 3345
		Retval = 8,
		// Token: 0x04000D12 RID: 3346
		Optional = 16,
		// Token: 0x04000D13 RID: 3347
		ReservedMask = 61440,
		// Token: 0x04000D14 RID: 3348
		HasDefault = 4096,
		// Token: 0x04000D15 RID: 3349
		HasFieldMarshal = 8192,
		// Token: 0x04000D16 RID: 3350
		Reserved3 = 16384,
		// Token: 0x04000D17 RID: 3351
		Reserved4 = 32768
	}
}
