﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000285 RID: 645
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum CallingConventions
	{
		// Token: 0x04000C3B RID: 3131
		Standard = 1,
		// Token: 0x04000C3C RID: 3132
		VarArgs = 2,
		// Token: 0x04000C3D RID: 3133
		Any = 3,
		// Token: 0x04000C3E RID: 3134
		HasThis = 32,
		// Token: 0x04000C3F RID: 3135
		ExplicitThis = 64
	}
}
