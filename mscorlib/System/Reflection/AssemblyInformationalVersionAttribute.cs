﻿using System;
using System.Runtime.InteropServices;

namespace System.Reflection
{
	// Token: 0x02000279 RID: 633
	[AttributeUsage(AttributeTargets.Assembly, Inherited = false)]
	[ComVisible(true)]
	public sealed class AssemblyInformationalVersionAttribute : Attribute
	{
		// Token: 0x060020CD RID: 8397 RVA: 0x00077E14 File Offset: 0x00076014
		public AssemblyInformationalVersionAttribute(string informationalVersion)
		{
			this.name = informationalVersion;
		}

		// Token: 0x1700057B RID: 1403
		// (get) Token: 0x060020CE RID: 8398 RVA: 0x00077E24 File Offset: 0x00076024
		public string InformationalVersion
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x04000C09 RID: 3081
		private string name;
	}
}
