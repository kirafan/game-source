﻿using System;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x020000F4 RID: 244
	[ComVisible(false)]
	[Serializable]
	public sealed class ActivationContext : IDisposable, ISerializable
	{
		// Token: 0x06000C5E RID: 3166 RVA: 0x00038504 File Offset: 0x00036704
		private ActivationContext(ApplicationIdentity identity)
		{
			this._appid = identity;
		}

		// Token: 0x06000C5F RID: 3167 RVA: 0x00038514 File Offset: 0x00036714
		[MonoTODO("Missing serialization support")]
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
		}

		// Token: 0x06000C60 RID: 3168 RVA: 0x00038528 File Offset: 0x00036728
		~ActivationContext()
		{
			this.Dispose(false);
		}

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x06000C61 RID: 3169 RVA: 0x00038564 File Offset: 0x00036764
		public ActivationContext.ContextForm Form
		{
			get
			{
				return this._form;
			}
		}

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x06000C62 RID: 3170 RVA: 0x0003856C File Offset: 0x0003676C
		public ApplicationIdentity Identity
		{
			get
			{
				return this._appid;
			}
		}

		// Token: 0x06000C63 RID: 3171 RVA: 0x00038574 File Offset: 0x00036774
		[MonoTODO("Missing validation")]
		public static ActivationContext CreatePartialActivationContext(ApplicationIdentity identity)
		{
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			return new ActivationContext(identity);
		}

		// Token: 0x06000C64 RID: 3172 RVA: 0x00038590 File Offset: 0x00036790
		[MonoTODO("Missing validation")]
		public static ActivationContext CreatePartialActivationContext(ApplicationIdentity identity, string[] manifestPaths)
		{
			if (identity == null)
			{
				throw new ArgumentNullException("identity");
			}
			if (manifestPaths == null)
			{
				throw new ArgumentNullException("manifestPaths");
			}
			return new ActivationContext(identity);
		}

		// Token: 0x06000C65 RID: 3173 RVA: 0x000385C8 File Offset: 0x000367C8
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000C66 RID: 3174 RVA: 0x000385D8 File Offset: 0x000367D8
		private void Dispose(bool disposing)
		{
			if (this._disposed)
			{
				if (disposing)
				{
				}
				this._disposed = true;
			}
		}

		// Token: 0x0400035D RID: 861
		private ActivationContext.ContextForm _form;

		// Token: 0x0400035E RID: 862
		private ApplicationIdentity _appid;

		// Token: 0x0400035F RID: 863
		private bool _disposed;

		// Token: 0x020000F5 RID: 245
		public enum ContextForm
		{
			// Token: 0x04000361 RID: 865
			Loose,
			// Token: 0x04000362 RID: 866
			StoreBounded
		}
	}
}
