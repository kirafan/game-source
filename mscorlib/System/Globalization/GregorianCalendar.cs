﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000216 RID: 534
	[MonoTODO("Serialization format not compatible with .NET")]
	[ComVisible(true)]
	[Serializable]
	public class GregorianCalendar : Calendar
	{
		// Token: 0x06001AC7 RID: 6855 RVA: 0x00063D20 File Offset: 0x00061F20
		public GregorianCalendar(GregorianCalendarTypes type)
		{
			this.CalendarType = type;
			this.M_AbbrEraNames = new string[]
			{
				"AD"
			};
			this.M_EraNames = new string[]
			{
				"A.D."
			};
			if (this.twoDigitYearMax == 99)
			{
				this.twoDigitYearMax = 2029;
			}
		}

		// Token: 0x06001AC8 RID: 6856 RVA: 0x00063D7C File Offset: 0x00061F7C
		public GregorianCalendar() : this(GregorianCalendarTypes.Localized)
		{
		}

		// Token: 0x1700045D RID: 1117
		// (get) Token: 0x06001AC9 RID: 6857 RVA: 0x00063D88 File Offset: 0x00061F88
		public override int[] Eras
		{
			get
			{
				return new int[]
				{
					1
				};
			}
		}

		// Token: 0x1700045E RID: 1118
		// (get) Token: 0x06001ACA RID: 6858 RVA: 0x00063D94 File Offset: 0x00061F94
		// (set) Token: 0x06001ACB RID: 6859 RVA: 0x00063D9C File Offset: 0x00061F9C
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x1700045F RID: 1119
		// (get) Token: 0x06001ACC RID: 6860 RVA: 0x00063DCC File Offset: 0x00061FCC
		// (set) Token: 0x06001ACD RID: 6861 RVA: 0x00063DD4 File Offset: 0x00061FD4
		public virtual GregorianCalendarTypes CalendarType
		{
			get
			{
				return this.m_type;
			}
			set
			{
				base.CheckReadOnly();
				this.m_type = value;
			}
		}

		// Token: 0x06001ACE RID: 6862 RVA: 0x00063DE4 File Offset: 0x00061FE4
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = 1;
			}
			if (era != 1)
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001ACF RID: 6863 RVA: 0x00063E04 File Offset: 0x00062004
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			base.M_ArgumentInRange("year", year, 1, 9999);
		}

		// Token: 0x06001AD0 RID: 6864 RVA: 0x00063E2C File Offset: 0x0006202C
		internal void M_CheckYME(int year, int month, ref int era)
		{
			this.M_CheckYE(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
		}

		// Token: 0x06001AD1 RID: 6865 RVA: 0x00063E58 File Offset: 0x00062058
		internal void M_CheckYMDE(int year, int month, int day, ref int era)
		{
			this.M_CheckYME(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, era));
		}

		// Token: 0x06001AD2 RID: 6866 RVA: 0x00063E88 File Offset: 0x00062088
		public override DateTime AddMonths(DateTime time, int months)
		{
			return CCGregorianCalendar.AddMonths(time, months);
		}

		// Token: 0x06001AD3 RID: 6867 RVA: 0x00063E94 File Offset: 0x00062094
		public override DateTime AddYears(DateTime time, int years)
		{
			return CCGregorianCalendar.AddYears(time, years);
		}

		// Token: 0x06001AD4 RID: 6868 RVA: 0x00063EA0 File Offset: 0x000620A0
		public override int GetDayOfMonth(DateTime time)
		{
			return CCGregorianCalendar.GetDayOfMonth(time);
		}

		// Token: 0x06001AD5 RID: 6869 RVA: 0x00063EA8 File Offset: 0x000620A8
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			return CCFixed.day_of_week(date);
		}

		// Token: 0x06001AD6 RID: 6870 RVA: 0x00063EC4 File Offset: 0x000620C4
		public override int GetDayOfYear(DateTime time)
		{
			return CCGregorianCalendar.GetDayOfYear(time);
		}

		// Token: 0x06001AD7 RID: 6871 RVA: 0x00063ECC File Offset: 0x000620CC
		public override int GetDaysInMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			return CCGregorianCalendar.GetDaysInMonth(year, month);
		}

		// Token: 0x06001AD8 RID: 6872 RVA: 0x00063EE0 File Offset: 0x000620E0
		public override int GetDaysInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return CCGregorianCalendar.GetDaysInYear(year);
		}

		// Token: 0x06001AD9 RID: 6873 RVA: 0x00063EF4 File Offset: 0x000620F4
		public override int GetEra(DateTime time)
		{
			return 1;
		}

		// Token: 0x06001ADA RID: 6874 RVA: 0x00063EF8 File Offset: 0x000620F8
		[ComVisible(false)]
		public override int GetLeapMonth(int year, int era)
		{
			return 0;
		}

		// Token: 0x06001ADB RID: 6875 RVA: 0x00063EFC File Offset: 0x000620FC
		public override int GetMonth(DateTime time)
		{
			return CCGregorianCalendar.GetMonth(time);
		}

		// Token: 0x06001ADC RID: 6876 RVA: 0x00063F04 File Offset: 0x00062104
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return 12;
		}

		// Token: 0x06001ADD RID: 6877 RVA: 0x00063F14 File Offset: 0x00062114
		[ComVisible(false)]
		public override int GetWeekOfYear(DateTime time, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
		{
			return base.GetWeekOfYear(time, rule, firstDayOfWeek);
		}

		// Token: 0x06001ADE RID: 6878 RVA: 0x00063F20 File Offset: 0x00062120
		public override int GetYear(DateTime time)
		{
			return CCGregorianCalendar.GetYear(time);
		}

		// Token: 0x06001ADF RID: 6879 RVA: 0x00063F28 File Offset: 0x00062128
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			return CCGregorianCalendar.IsLeapDay(year, month, day);
		}

		// Token: 0x06001AE0 RID: 6880 RVA: 0x00063F40 File Offset: 0x00062140
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			return false;
		}

		// Token: 0x06001AE1 RID: 6881 RVA: 0x00063F50 File Offset: 0x00062150
		public override bool IsLeapYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return CCGregorianCalendar.is_leap_year(year);
		}

		// Token: 0x06001AE2 RID: 6882 RVA: 0x00063F64 File Offset: 0x00062164
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			return CCGregorianCalendar.ToDateTime(year, month, day, hour, minute, second, millisecond);
		}

		// Token: 0x06001AE3 RID: 6883 RVA: 0x00063F9C File Offset: 0x0006219C
		public override int ToFourDigitYear(int year)
		{
			return base.ToFourDigitYear(year);
		}

		// Token: 0x17000460 RID: 1120
		// (get) Token: 0x06001AE4 RID: 6884 RVA: 0x00063FA8 File Offset: 0x000621A8
		[ComVisible(false)]
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.SolarCalendar;
			}
		}

		// Token: 0x17000461 RID: 1121
		// (get) Token: 0x06001AE5 RID: 6885 RVA: 0x00063FAC File Offset: 0x000621AC
		[ComVisible(false)]
		public override DateTime MinSupportedDateTime
		{
			get
			{
				DateTime? min = GregorianCalendar.Min;
				if (min == null)
				{
					GregorianCalendar.Min = new DateTime?(new DateTime(1, 1, 1, 0, 0, 0));
				}
				return GregorianCalendar.Min.Value;
			}
		}

		// Token: 0x17000462 RID: 1122
		// (get) Token: 0x06001AE6 RID: 6886 RVA: 0x00063FF0 File Offset: 0x000621F0
		[ComVisible(false)]
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				DateTime? max = GregorianCalendar.Max;
				if (max == null)
				{
					GregorianCalendar.Max = new DateTime?(new DateTime(9999, 12, 31, 11, 59, 59));
				}
				return GregorianCalendar.Max.Value;
			}
		}

		// Token: 0x04000A1A RID: 2586
		public const int ADEra = 1;

		// Token: 0x04000A1B RID: 2587
		[NonSerialized]
		internal GregorianCalendarTypes m_type;

		// Token: 0x04000A1C RID: 2588
		private static DateTime? Min;

		// Token: 0x04000A1D RID: 2589
		private static DateTime? Max;
	}
}
