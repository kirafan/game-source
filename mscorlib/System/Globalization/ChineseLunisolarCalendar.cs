﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x0200020A RID: 522
	[Serializable]
	public class ChineseLunisolarCalendar : EastAsianLunisolarCalendar
	{
		// Token: 0x060019C3 RID: 6595 RVA: 0x000605C8 File Offset: 0x0005E7C8
		[MonoTODO]
		public ChineseLunisolarCalendar() : base(ChineseLunisolarCalendar.era_handler)
		{
		}

		// Token: 0x060019C4 RID: 6596 RVA: 0x000605D8 File Offset: 0x0005E7D8
		static ChineseLunisolarCalendar()
		{
			ChineseLunisolarCalendar.era_handler = new CCEastAsianLunisolarEraHandler();
			ChineseLunisolarCalendar.era_handler.appendEra(1, CCFixed.FromDateTime(new DateTime(1, 1, 1)));
		}

		// Token: 0x17000415 RID: 1045
		// (get) Token: 0x060019C5 RID: 6597 RVA: 0x00060638 File Offset: 0x0005E838
		[ComVisible(false)]
		public override int[] Eras
		{
			get
			{
				return (int[])ChineseLunisolarCalendar.era_handler.Eras.Clone();
			}
		}

		// Token: 0x060019C6 RID: 6598 RVA: 0x00060650 File Offset: 0x0005E850
		[ComVisible(false)]
		public override int GetEra(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int result;
			ChineseLunisolarCalendar.era_handler.EraYear(out result, date);
			return result;
		}

		// Token: 0x17000416 RID: 1046
		// (get) Token: 0x060019C7 RID: 6599 RVA: 0x00060674 File Offset: 0x0005E874
		[ComVisible(false)]
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return ChineseLunisolarCalendar.ChineseMin;
			}
		}

		// Token: 0x17000417 RID: 1047
		// (get) Token: 0x060019C8 RID: 6600 RVA: 0x0006067C File Offset: 0x0005E87C
		[ComVisible(false)]
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return ChineseLunisolarCalendar.ChineseMax;
			}
		}

		// Token: 0x04000974 RID: 2420
		public const int ChineseEra = 1;

		// Token: 0x04000975 RID: 2421
		internal static readonly CCEastAsianLunisolarEraHandler era_handler;

		// Token: 0x04000976 RID: 2422
		private static DateTime ChineseMin = new DateTime(1901, 2, 19);

		// Token: 0x04000977 RID: 2423
		private static DateTime ChineseMax = new DateTime(2101, 1, 28, 23, 59, 59, 999);
	}
}
