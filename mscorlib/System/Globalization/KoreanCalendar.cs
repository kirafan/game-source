﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000220 RID: 544
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with .NET")]
	[Serializable]
	public class KoreanCalendar : Calendar
	{
		// Token: 0x06001B8D RID: 7053 RVA: 0x000662C4 File Offset: 0x000644C4
		public KoreanCalendar()
		{
			this.M_AbbrEraNames = new string[]
			{
				"K.C.E."
			};
			this.M_EraNames = new string[]
			{
				"Korean Current Era"
			};
			if (this.twoDigitYearMax == 99)
			{
				this.twoDigitYearMax = 4362;
			}
		}

		// Token: 0x06001B8E RID: 7054 RVA: 0x00066318 File Offset: 0x00064518
		static KoreanCalendar()
		{
			KoreanCalendar.M_EraHandler = new CCGregorianEraHandler();
			KoreanCalendar.M_EraHandler.appendEra(1, CCGregorianCalendar.fixed_from_dmy(1, 1, -2332));
		}

		// Token: 0x17000480 RID: 1152
		// (get) Token: 0x06001B8F RID: 7055 RVA: 0x00066370 File Offset: 0x00064570
		public override int[] Eras
		{
			get
			{
				return (int[])KoreanCalendar.M_EraHandler.Eras.Clone();
			}
		}

		// Token: 0x17000481 RID: 1153
		// (get) Token: 0x06001B90 RID: 7056 RVA: 0x00066388 File Offset: 0x00064588
		// (set) Token: 0x06001B91 RID: 7057 RVA: 0x00066390 File Offset: 0x00064590
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x06001B92 RID: 7058 RVA: 0x000663C0 File Offset: 0x000645C0
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = 1;
			}
			if (!KoreanCalendar.M_EraHandler.ValidEra(era))
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001B93 RID: 7059 RVA: 0x000663F4 File Offset: 0x000645F4
		internal int M_CheckYEG(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			return KoreanCalendar.M_EraHandler.GregorianYear(year, era);
		}

		// Token: 0x06001B94 RID: 7060 RVA: 0x0006640C File Offset: 0x0006460C
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckYEG(year, ref era);
		}

		// Token: 0x06001B95 RID: 7061 RVA: 0x00066418 File Offset: 0x00064618
		internal int M_CheckYMEG(int year, int month, ref int era)
		{
			int result = this.M_CheckYEG(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
			return result;
		}

		// Token: 0x06001B96 RID: 7062 RVA: 0x00066450 File Offset: 0x00064650
		internal int M_CheckYMDEG(int year, int month, int day, ref int era)
		{
			int result = this.M_CheckYMEG(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, era));
			return result;
		}

		// Token: 0x06001B97 RID: 7063 RVA: 0x00066484 File Offset: 0x00064684
		public override DateTime AddMonths(DateTime time, int months)
		{
			return CCGregorianCalendar.AddMonths(time, months);
		}

		// Token: 0x06001B98 RID: 7064 RVA: 0x00066490 File Offset: 0x00064690
		public override DateTime AddYears(DateTime time, int years)
		{
			return CCGregorianCalendar.AddYears(time, years);
		}

		// Token: 0x06001B99 RID: 7065 RVA: 0x0006649C File Offset: 0x0006469C
		public override int GetDayOfMonth(DateTime time)
		{
			return CCGregorianCalendar.GetDayOfMonth(time);
		}

		// Token: 0x06001B9A RID: 7066 RVA: 0x000664A4 File Offset: 0x000646A4
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			return CCFixed.day_of_week(date);
		}

		// Token: 0x06001B9B RID: 7067 RVA: 0x000664C0 File Offset: 0x000646C0
		public override int GetDayOfYear(DateTime time)
		{
			return CCGregorianCalendar.GetDayOfYear(time);
		}

		// Token: 0x06001B9C RID: 7068 RVA: 0x000664C8 File Offset: 0x000646C8
		public override int GetDaysInMonth(int year, int month, int era)
		{
			int year2 = this.M_CheckYMEG(year, month, ref era);
			return CCGregorianCalendar.GetDaysInMonth(year2, month);
		}

		// Token: 0x06001B9D RID: 7069 RVA: 0x000664E8 File Offset: 0x000646E8
		public override int GetDaysInYear(int year, int era)
		{
			int year2 = this.M_CheckYEG(year, ref era);
			return CCGregorianCalendar.GetDaysInYear(year2);
		}

		// Token: 0x06001B9E RID: 7070 RVA: 0x00066508 File Offset: 0x00064708
		public override int GetEra(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int result;
			KoreanCalendar.M_EraHandler.EraYear(out result, date);
			return result;
		}

		// Token: 0x06001B9F RID: 7071 RVA: 0x0006652C File Offset: 0x0006472C
		[ComVisible(false)]
		public override int GetLeapMonth(int year, int era)
		{
			return 0;
		}

		// Token: 0x06001BA0 RID: 7072 RVA: 0x00066530 File Offset: 0x00064730
		public override int GetMonth(DateTime time)
		{
			return CCGregorianCalendar.GetMonth(time);
		}

		// Token: 0x06001BA1 RID: 7073 RVA: 0x00066538 File Offset: 0x00064738
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYEG(year, ref era);
			return 12;
		}

		// Token: 0x06001BA2 RID: 7074 RVA: 0x00066548 File Offset: 0x00064748
		[ComVisible(false)]
		public override int GetWeekOfYear(DateTime time, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
		{
			return base.GetWeekOfYear(time, rule, firstDayOfWeek);
		}

		// Token: 0x06001BA3 RID: 7075 RVA: 0x00066554 File Offset: 0x00064754
		public override int GetYear(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int num;
			return KoreanCalendar.M_EraHandler.EraYear(out num, date);
		}

		// Token: 0x06001BA4 RID: 7076 RVA: 0x00066578 File Offset: 0x00064778
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			int year2 = this.M_CheckYMDEG(year, month, day, ref era);
			return CCGregorianCalendar.IsLeapDay(year2, month, day);
		}

		// Token: 0x06001BA5 RID: 7077 RVA: 0x0006659C File Offset: 0x0006479C
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYMEG(year, month, ref era);
			return false;
		}

		// Token: 0x06001BA6 RID: 7078 RVA: 0x000665AC File Offset: 0x000647AC
		public override bool IsLeapYear(int year, int era)
		{
			int year2 = this.M_CheckYEG(year, ref era);
			return CCGregorianCalendar.is_leap_year(year2);
		}

		// Token: 0x06001BA7 RID: 7079 RVA: 0x000665CC File Offset: 0x000647CC
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			int year2 = this.M_CheckYMDEG(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			return CCGregorianCalendar.ToDateTime(year2, month, day, hour, minute, second, millisecond);
		}

		// Token: 0x06001BA8 RID: 7080 RVA: 0x00066604 File Offset: 0x00064804
		public override int ToFourDigitYear(int year)
		{
			return base.ToFourDigitYear(year);
		}

		// Token: 0x17000482 RID: 1154
		// (get) Token: 0x06001BA9 RID: 7081 RVA: 0x00066610 File Offset: 0x00064810
		[ComVisible(false)]
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.SolarCalendar;
			}
		}

		// Token: 0x17000483 RID: 1155
		// (get) Token: 0x06001BAA RID: 7082 RVA: 0x00066614 File Offset: 0x00064814
		[ComVisible(false)]
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return KoreanCalendar.KoreanMin;
			}
		}

		// Token: 0x17000484 RID: 1156
		// (get) Token: 0x06001BAB RID: 7083 RVA: 0x0006661C File Offset: 0x0006481C
		[ComVisible(false)]
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return KoreanCalendar.KoreanMax;
			}
		}

		// Token: 0x04000A46 RID: 2630
		public const int KoreanEra = 1;

		// Token: 0x04000A47 RID: 2631
		internal static readonly CCGregorianEraHandler M_EraHandler;

		// Token: 0x04000A48 RID: 2632
		private static DateTime KoreanMin = new DateTime(1, 1, 1, 0, 0, 0);

		// Token: 0x04000A49 RID: 2633
		private static DateTime KoreanMax = new DateTime(9999, 12, 31, 11, 59, 59);
	}
}
