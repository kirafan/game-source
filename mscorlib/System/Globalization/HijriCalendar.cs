﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000219 RID: 537
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with .NET")]
	[Serializable]
	public class HijriCalendar : Calendar
	{
		// Token: 0x06001B08 RID: 6920 RVA: 0x00064664 File Offset: 0x00062864
		public HijriCalendar()
		{
			this.M_AbbrEraNames = new string[]
			{
				"A.H."
			};
			this.M_EraNames = new string[]
			{
				"Anno Hegirae"
			};
			if (this.twoDigitYearMax == 99)
			{
				this.twoDigitYearMax = 1451;
			}
		}

		// Token: 0x17000469 RID: 1129
		// (get) Token: 0x06001B0A RID: 6922 RVA: 0x0006471C File Offset: 0x0006291C
		public override int[] Eras
		{
			get
			{
				return new int[]
				{
					HijriCalendar.HijriEra
				};
			}
		}

		// Token: 0x1700046A RID: 1130
		// (get) Token: 0x06001B0B RID: 6923 RVA: 0x0006472C File Offset: 0x0006292C
		// (set) Token: 0x06001B0C RID: 6924 RVA: 0x00064734 File Offset: 0x00062934
		[MonoTODO("Not supported")]
		public int HijriAdjustment
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x1700046B RID: 1131
		// (get) Token: 0x06001B0D RID: 6925 RVA: 0x0006473C File Offset: 0x0006293C
		// (set) Token: 0x06001B0E RID: 6926 RVA: 0x00064744 File Offset: 0x00062944
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x1700046C RID: 1132
		// (get) Token: 0x06001B0F RID: 6927 RVA: 0x00064774 File Offset: 0x00062974
		// (set) Token: 0x06001B10 RID: 6928 RVA: 0x0006477C File Offset: 0x0006297C
		internal virtual int AddHijriDate
		{
			get
			{
				return this.M_AddHijriDate;
			}
			set
			{
				base.CheckReadOnly();
				if (value < -3 && value > 3)
				{
					throw new ArgumentOutOfRangeException("AddHijriDate", "Value should be between -3 and 3.");
				}
				this.M_AddHijriDate = value;
			}
		}

		// Token: 0x06001B11 RID: 6929 RVA: 0x000647B8 File Offset: 0x000629B8
		internal void M_CheckFixedHijri(string param, int rdHijri)
		{
			if (rdHijri < HijriCalendar.M_MinFixed || rdHijri > HijriCalendar.M_MaxFixed - this.AddHijriDate)
			{
				StringWriter stringWriter = new StringWriter();
				int num;
				int num2;
				int num3;
				CCHijriCalendar.dmy_from_fixed(out num, out num2, out num3, HijriCalendar.M_MaxFixed - this.AddHijriDate);
				if (this.AddHijriDate != 0)
				{
					stringWriter.Write("This HijriCalendar (AddHijriDate {0}) allows dates from 1. 1. 1 to {1}. {2}. {3}.", new object[]
					{
						this.AddHijriDate,
						num,
						num2,
						num3
					});
				}
				else
				{
					stringWriter.Write("HijriCalendar allows dates from 1.1.1 to {0}.{1}.{2}.", num, num2, num3);
				}
				throw new ArgumentOutOfRangeException(param, stringWriter.ToString());
			}
		}

		// Token: 0x06001B12 RID: 6930 RVA: 0x00064874 File Offset: 0x00062A74
		internal void M_CheckDateTime(DateTime time)
		{
			int rdHijri = CCFixed.FromDateTime(time) - this.AddHijriDate;
			this.M_CheckFixedHijri("time", rdHijri);
		}

		// Token: 0x06001B13 RID: 6931 RVA: 0x0006489C File Offset: 0x00062A9C
		internal int M_FromDateTime(DateTime time)
		{
			return CCFixed.FromDateTime(time) - this.AddHijriDate;
		}

		// Token: 0x06001B14 RID: 6932 RVA: 0x000648AC File Offset: 0x00062AAC
		internal DateTime M_ToDateTime(int rd)
		{
			return CCFixed.ToDateTime(rd + this.AddHijriDate);
		}

		// Token: 0x06001B15 RID: 6933 RVA: 0x000648BC File Offset: 0x00062ABC
		internal DateTime M_ToDateTime(int date, int hour, int minute, int second, int milliseconds)
		{
			return CCFixed.ToDateTime(date + this.AddHijriDate, hour, minute, second, (double)milliseconds);
		}

		// Token: 0x06001B16 RID: 6934 RVA: 0x000648D4 File Offset: 0x00062AD4
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = HijriCalendar.HijriEra;
			}
			if (era != HijriCalendar.HijriEra)
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001B17 RID: 6935 RVA: 0x000648FC File Offset: 0x00062AFC
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			base.M_ArgumentInRange("year", year, 1, 9666);
		}

		// Token: 0x06001B18 RID: 6936 RVA: 0x00064924 File Offset: 0x00062B24
		internal void M_CheckYME(int year, int month, ref int era)
		{
			this.M_CheckYE(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
			if (year == 9666)
			{
				int rdHijri = CCHijriCalendar.fixed_from_dmy(1, month, year);
				this.M_CheckFixedHijri("month", rdHijri);
			}
		}

		// Token: 0x06001B19 RID: 6937 RVA: 0x00064978 File Offset: 0x00062B78
		internal void M_CheckYMDE(int year, int month, int day, ref int era)
		{
			this.M_CheckYME(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, HijriCalendar.HijriEra));
			if (year == 9666)
			{
				int rdHijri = CCHijriCalendar.fixed_from_dmy(day, month, year);
				this.M_CheckFixedHijri("day", rdHijri);
			}
		}

		// Token: 0x06001B1A RID: 6938 RVA: 0x000649CC File Offset: 0x00062BCC
		public override DateTime AddMonths(DateTime time, int months)
		{
			int num = this.M_FromDateTime(time);
			int day;
			int num2;
			int num3;
			CCHijriCalendar.dmy_from_fixed(out day, out num2, out num3, num);
			num2 += months;
			num3 += CCMath.div_mod(out num2, num2, 12);
			num = CCHijriCalendar.fixed_from_dmy(day, num2, num3);
			this.M_CheckFixedHijri("time", num);
			return this.M_ToDateTime(num).Add(time.TimeOfDay);
		}

		// Token: 0x06001B1B RID: 6939 RVA: 0x00064A2C File Offset: 0x00062C2C
		public override DateTime AddYears(DateTime time, int years)
		{
			int num = this.M_FromDateTime(time);
			int day;
			int month;
			int num2;
			CCHijriCalendar.dmy_from_fixed(out day, out month, out num2, num);
			num2 += years;
			num = CCHijriCalendar.fixed_from_dmy(day, month, num2);
			this.M_CheckFixedHijri("time", num);
			return this.M_ToDateTime(num).Add(time.TimeOfDay);
		}

		// Token: 0x06001B1C RID: 6940 RVA: 0x00064A80 File Offset: 0x00062C80
		public override int GetDayOfMonth(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			return CCHijriCalendar.day_from_fixed(num);
		}

		// Token: 0x06001B1D RID: 6941 RVA: 0x00064AA8 File Offset: 0x00062CA8
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			return CCFixed.day_of_week(num);
		}

		// Token: 0x06001B1E RID: 6942 RVA: 0x00064AD0 File Offset: 0x00062CD0
		public override int GetDayOfYear(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			int year = CCHijriCalendar.year_from_fixed(num);
			int num2 = CCHijriCalendar.fixed_from_dmy(1, 1, year);
			return num - num2 + 1;
		}

		// Token: 0x06001B1F RID: 6943 RVA: 0x00064B08 File Offset: 0x00062D08
		public override int GetDaysInMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			int num = CCHijriCalendar.fixed_from_dmy(1, month, year);
			int num2 = CCHijriCalendar.fixed_from_dmy(1, month + 1, year);
			return num2 - num;
		}

		// Token: 0x06001B20 RID: 6944 RVA: 0x00064B38 File Offset: 0x00062D38
		public override int GetDaysInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			int num = CCHijriCalendar.fixed_from_dmy(1, 1, year);
			int num2 = CCHijriCalendar.fixed_from_dmy(1, 1, year + 1);
			return num2 - num;
		}

		// Token: 0x06001B21 RID: 6945 RVA: 0x00064B68 File Offset: 0x00062D68
		public override int GetEra(DateTime time)
		{
			this.M_CheckDateTime(time);
			return HijriCalendar.HijriEra;
		}

		// Token: 0x06001B22 RID: 6946 RVA: 0x00064B78 File Offset: 0x00062D78
		[ComVisible(false)]
		public override int GetLeapMonth(int year, int era)
		{
			return 0;
		}

		// Token: 0x06001B23 RID: 6947 RVA: 0x00064B7C File Offset: 0x00062D7C
		public override int GetMonth(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			return CCHijriCalendar.month_from_fixed(num);
		}

		// Token: 0x06001B24 RID: 6948 RVA: 0x00064BA4 File Offset: 0x00062DA4
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return 12;
		}

		// Token: 0x06001B25 RID: 6949 RVA: 0x00064BB4 File Offset: 0x00062DB4
		public override int GetYear(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			return CCHijriCalendar.year_from_fixed(num);
		}

		// Token: 0x06001B26 RID: 6950 RVA: 0x00064BDC File Offset: 0x00062DDC
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			return this.IsLeapYear(year) && month == 12 && day == 30;
		}

		// Token: 0x06001B27 RID: 6951 RVA: 0x00064C08 File Offset: 0x00062E08
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			return false;
		}

		// Token: 0x06001B28 RID: 6952 RVA: 0x00064C18 File Offset: 0x00062E18
		public override bool IsLeapYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return CCHijriCalendar.is_leap_year(year);
		}

		// Token: 0x06001B29 RID: 6953 RVA: 0x00064C2C File Offset: 0x00062E2C
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			int date = CCHijriCalendar.fixed_from_dmy(day, month, year);
			return this.M_ToDateTime(date, hour, minute, second, millisecond);
		}

		// Token: 0x06001B2A RID: 6954 RVA: 0x00064C6C File Offset: 0x00062E6C
		public override int ToFourDigitYear(int year)
		{
			return base.ToFourDigitYear(year);
		}

		// Token: 0x1700046D RID: 1133
		// (get) Token: 0x06001B2B RID: 6955 RVA: 0x00064C78 File Offset: 0x00062E78
		[ComVisible(false)]
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.LunarCalendar;
			}
		}

		// Token: 0x1700046E RID: 1134
		// (get) Token: 0x06001B2C RID: 6956 RVA: 0x00064C7C File Offset: 0x00062E7C
		[ComVisible(false)]
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return HijriCalendar.Min;
			}
		}

		// Token: 0x1700046F RID: 1135
		// (get) Token: 0x06001B2D RID: 6957 RVA: 0x00064C84 File Offset: 0x00062E84
		[ComVisible(false)]
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return HijriCalendar.Max;
			}
		}

		// Token: 0x04000A2B RID: 2603
		public static readonly int HijriEra = 1;

		// Token: 0x04000A2C RID: 2604
		internal static readonly int M_MinFixed = CCHijriCalendar.fixed_from_dmy(1, 1, 1);

		// Token: 0x04000A2D RID: 2605
		internal static readonly int M_MaxFixed = CCGregorianCalendar.fixed_from_dmy(31, 12, 9999);

		// Token: 0x04000A2E RID: 2606
		internal int M_AddHijriDate;

		// Token: 0x04000A2F RID: 2607
		private static DateTime Min = new DateTime(622, 7, 18, 0, 0, 0);

		// Token: 0x04000A30 RID: 2608
		private static DateTime Max = new DateTime(9999, 12, 31, 11, 59, 59);
	}
}
