﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000227 RID: 551
	[MonoTODO("Serialization format not compatible with.NET")]
	[ComVisible(true)]
	[Serializable]
	public class TaiwanCalendar : Calendar
	{
		// Token: 0x06001C41 RID: 7233 RVA: 0x00068708 File Offset: 0x00066908
		public TaiwanCalendar()
		{
			this.M_AbbrEraNames = new string[]
			{
				"T.C.E."
			};
			this.M_EraNames = new string[]
			{
				"Taiwan current era"
			};
		}

		// Token: 0x06001C42 RID: 7234 RVA: 0x00068744 File Offset: 0x00066944
		static TaiwanCalendar()
		{
			TaiwanCalendar.M_EraHandler = new CCGregorianEraHandler();
			TaiwanCalendar.M_EraHandler.appendEra(1, CCGregorianCalendar.fixed_from_dmy(1, 1, 1912));
		}

		// Token: 0x170004BE RID: 1214
		// (get) Token: 0x06001C43 RID: 7235 RVA: 0x000687A0 File Offset: 0x000669A0
		public override int[] Eras
		{
			get
			{
				return (int[])TaiwanCalendar.M_EraHandler.Eras.Clone();
			}
		}

		// Token: 0x170004BF RID: 1215
		// (get) Token: 0x06001C44 RID: 7236 RVA: 0x000687B8 File Offset: 0x000669B8
		// (set) Token: 0x06001C45 RID: 7237 RVA: 0x000687C0 File Offset: 0x000669C0
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x06001C46 RID: 7238 RVA: 0x000687F0 File Offset: 0x000669F0
		internal void M_CheckDateTime(DateTime time)
		{
			TaiwanCalendar.M_EraHandler.CheckDateTime(time);
		}

		// Token: 0x06001C47 RID: 7239 RVA: 0x00068800 File Offset: 0x00066A00
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = 1;
			}
			if (!TaiwanCalendar.M_EraHandler.ValidEra(era))
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001C48 RID: 7240 RVA: 0x00068834 File Offset: 0x00066A34
		internal int M_CheckYEG(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			return TaiwanCalendar.M_EraHandler.GregorianYear(year, era);
		}

		// Token: 0x06001C49 RID: 7241 RVA: 0x0006884C File Offset: 0x00066A4C
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckYEG(year, ref era);
		}

		// Token: 0x06001C4A RID: 7242 RVA: 0x00068858 File Offset: 0x00066A58
		internal int M_CheckYMEG(int year, int month, ref int era)
		{
			int result = this.M_CheckYEG(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
			return result;
		}

		// Token: 0x06001C4B RID: 7243 RVA: 0x00068890 File Offset: 0x00066A90
		internal int M_CheckYMDEG(int year, int month, int day, ref int era)
		{
			int result = this.M_CheckYMEG(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, era));
			return result;
		}

		// Token: 0x06001C4C RID: 7244 RVA: 0x000688C4 File Offset: 0x00066AC4
		public override DateTime AddMonths(DateTime time, int months)
		{
			DateTime dateTime = CCGregorianCalendar.AddMonths(time, months);
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001C4D RID: 7245 RVA: 0x000688E4 File Offset: 0x00066AE4
		public override DateTime AddYears(DateTime time, int years)
		{
			DateTime dateTime = CCGregorianCalendar.AddYears(time, years);
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001C4E RID: 7246 RVA: 0x00068904 File Offset: 0x00066B04
		public override int GetDayOfMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			return CCGregorianCalendar.GetDayOfMonth(time);
		}

		// Token: 0x06001C4F RID: 7247 RVA: 0x00068914 File Offset: 0x00066B14
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			return CCFixed.day_of_week(date);
		}

		// Token: 0x06001C50 RID: 7248 RVA: 0x00068938 File Offset: 0x00066B38
		public override int GetDayOfYear(DateTime time)
		{
			this.M_CheckDateTime(time);
			return CCGregorianCalendar.GetDayOfYear(time);
		}

		// Token: 0x06001C51 RID: 7249 RVA: 0x00068948 File Offset: 0x00066B48
		public override int GetDaysInMonth(int year, int month, int era)
		{
			int year2 = this.M_CheckYMEG(year, month, ref era);
			return CCGregorianCalendar.GetDaysInMonth(year2, month);
		}

		// Token: 0x06001C52 RID: 7250 RVA: 0x00068968 File Offset: 0x00066B68
		public override int GetDaysInYear(int year, int era)
		{
			int year2 = this.M_CheckYEG(year, ref era);
			return CCGregorianCalendar.GetDaysInYear(year2);
		}

		// Token: 0x06001C53 RID: 7251 RVA: 0x00068988 File Offset: 0x00066B88
		public override int GetEra(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int result;
			TaiwanCalendar.M_EraHandler.EraYear(out result, date);
			return result;
		}

		// Token: 0x06001C54 RID: 7252 RVA: 0x000689AC File Offset: 0x00066BAC
		[ComVisible(false)]
		public override int GetLeapMonth(int year, int era)
		{
			return 0;
		}

		// Token: 0x06001C55 RID: 7253 RVA: 0x000689B0 File Offset: 0x00066BB0
		public override int GetMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			return CCGregorianCalendar.GetMonth(time);
		}

		// Token: 0x06001C56 RID: 7254 RVA: 0x000689C0 File Offset: 0x00066BC0
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYEG(year, ref era);
			return 12;
		}

		// Token: 0x06001C57 RID: 7255 RVA: 0x000689D0 File Offset: 0x00066BD0
		[ComVisible(false)]
		public override int GetWeekOfYear(DateTime time, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
		{
			return base.GetWeekOfYear(time, rule, firstDayOfWeek);
		}

		// Token: 0x06001C58 RID: 7256 RVA: 0x000689DC File Offset: 0x00066BDC
		public override int GetYear(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int num;
			return TaiwanCalendar.M_EraHandler.EraYear(out num, date);
		}

		// Token: 0x06001C59 RID: 7257 RVA: 0x00068A00 File Offset: 0x00066C00
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			int year2 = this.M_CheckYMDEG(year, month, day, ref era);
			return CCGregorianCalendar.IsLeapDay(year2, month, day);
		}

		// Token: 0x06001C5A RID: 7258 RVA: 0x00068A24 File Offset: 0x00066C24
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYMEG(year, month, ref era);
			return false;
		}

		// Token: 0x06001C5B RID: 7259 RVA: 0x00068A34 File Offset: 0x00066C34
		public override bool IsLeapYear(int year, int era)
		{
			int year2 = this.M_CheckYEG(year, ref era);
			return CCGregorianCalendar.is_leap_year(year2);
		}

		// Token: 0x06001C5C RID: 7260 RVA: 0x00068A54 File Offset: 0x00066C54
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			int year2 = this.M_CheckYMDEG(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			return CCGregorianCalendar.ToDateTime(year2, month, day, hour, minute, second, millisecond);
		}

		// Token: 0x06001C5D RID: 7261 RVA: 0x00068A8C File Offset: 0x00066C8C
		public override int ToFourDigitYear(int year)
		{
			if (year < 0)
			{
				throw new ArgumentOutOfRangeException("year", "Non-negative number required.");
			}
			int num = 0;
			this.M_CheckYE(year, ref num);
			return year;
		}

		// Token: 0x170004C0 RID: 1216
		// (get) Token: 0x06001C5E RID: 7262 RVA: 0x00068ABC File Offset: 0x00066CBC
		[ComVisible(false)]
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.SolarCalendar;
			}
		}

		// Token: 0x170004C1 RID: 1217
		// (get) Token: 0x06001C5F RID: 7263 RVA: 0x00068AC0 File Offset: 0x00066CC0
		[ComVisible(false)]
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return TaiwanCalendar.TaiwanMin;
			}
		}

		// Token: 0x170004C2 RID: 1218
		// (get) Token: 0x06001C60 RID: 7264 RVA: 0x00068AC8 File Offset: 0x00066CC8
		[ComVisible(false)]
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return TaiwanCalendar.TaiwanMax;
			}
		}

		// Token: 0x04000A9A RID: 2714
		internal static readonly CCGregorianEraHandler M_EraHandler;

		// Token: 0x04000A9B RID: 2715
		private static DateTime TaiwanMin = new DateTime(1912, 1, 1, 0, 0, 0);

		// Token: 0x04000A9C RID: 2716
		private static DateTime TaiwanMax = new DateTime(9999, 12, 31, 11, 59, 59);
	}
}
