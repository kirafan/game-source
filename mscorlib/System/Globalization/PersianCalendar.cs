﻿using System;

namespace System.Globalization
{
	// Token: 0x02000224 RID: 548
	[Serializable]
	public class PersianCalendar : Calendar
	{
		// Token: 0x06001BFA RID: 7162 RVA: 0x00067A18 File Offset: 0x00065C18
		public PersianCalendar()
		{
			this.M_AbbrEraNames = new string[]
			{
				"A.P."
			};
			this.M_EraNames = new string[]
			{
				"Anno Persico"
			};
			if (this.twoDigitYearMax == 99)
			{
				this.twoDigitYearMax = 1410;
			}
		}

		// Token: 0x170004A9 RID: 1193
		// (get) Token: 0x06001BFC RID: 7164 RVA: 0x00067AB0 File Offset: 0x00065CB0
		public override int[] Eras
		{
			get
			{
				return new int[]
				{
					PersianCalendar.PersianEra
				};
			}
		}

		// Token: 0x170004AA RID: 1194
		// (get) Token: 0x06001BFD RID: 7165 RVA: 0x00067AC0 File Offset: 0x00065CC0
		// (set) Token: 0x06001BFE RID: 7166 RVA: 0x00067AC8 File Offset: 0x00065CC8
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x06001BFF RID: 7167 RVA: 0x00067AF8 File Offset: 0x00065CF8
		internal void M_CheckDateTime(DateTime time)
		{
			if (time.Ticks < 196036416000000000L)
			{
				throw new ArgumentOutOfRangeException("time", "Only positive Persian years are supported.");
			}
		}

		// Token: 0x06001C00 RID: 7168 RVA: 0x00067B20 File Offset: 0x00065D20
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = PersianCalendar.PersianEra;
			}
			if (era != PersianCalendar.PersianEra)
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001C01 RID: 7169 RVA: 0x00067B48 File Offset: 0x00065D48
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			if (year < 1 || year > this.M_MaxYear)
			{
				throw new ArgumentOutOfRangeException("year", "Only Persian years between 1 and 9378, inclusive, are supported.");
			}
		}

		// Token: 0x06001C02 RID: 7170 RVA: 0x00067B80 File Offset: 0x00065D80
		internal void M_CheckYME(int year, int month, ref int era)
		{
			this.M_CheckYE(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
			if (year == this.M_MaxYear && month > 10)
			{
				throw new ArgumentOutOfRangeException("month", "Months in year 9378 must be between one and ten.");
			}
		}

		// Token: 0x06001C03 RID: 7171 RVA: 0x00067BD8 File Offset: 0x00065DD8
		internal void M_CheckYMDE(int year, int month, int day, ref int era)
		{
			this.M_CheckYME(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, era));
			if (year == this.M_MaxYear && month == 10 && day > 10)
			{
				throw new ArgumentOutOfRangeException("day", "Days in month 10 of year 9378 must be between one and ten.");
			}
		}

		// Token: 0x06001C04 RID: 7172 RVA: 0x00067C34 File Offset: 0x00065E34
		internal int fixed_from_dmy(int day, int month, int year)
		{
			int num = 226894;
			num += 365 * (year - 1);
			num += (8 * year + 21) / 33;
			if (month <= 7)
			{
				num += 31 * (month - 1);
			}
			else
			{
				num += 30 * (month - 1) + 6;
			}
			return num + day;
		}

		// Token: 0x06001C05 RID: 7173 RVA: 0x00067C84 File Offset: 0x00065E84
		internal int year_from_fixed(int date)
		{
			return (33 * (date - 226895) + 3) / 12053 + 1;
		}

		// Token: 0x06001C06 RID: 7174 RVA: 0x00067C9C File Offset: 0x00065E9C
		internal void my_from_fixed(out int month, out int year, int date)
		{
			year = this.year_from_fixed(date);
			int num = date - this.fixed_from_dmy(1, 1, year);
			if (num < 216)
			{
				month = num / 31 + 1;
			}
			else
			{
				month = (num - 6) / 30 + 1;
			}
		}

		// Token: 0x06001C07 RID: 7175 RVA: 0x00067CE4 File Offset: 0x00065EE4
		internal void dmy_from_fixed(out int day, out int month, out int year, int date)
		{
			year = this.year_from_fixed(date);
			day = date - this.fixed_from_dmy(1, 1, year);
			if (day < 216)
			{
				month = day / 31 + 1;
				day = day % 31 + 1;
			}
			else
			{
				month = (day - 6) / 30 + 1;
				day = (day - 6) % 30 + 1;
			}
		}

		// Token: 0x06001C08 RID: 7176 RVA: 0x00067D44 File Offset: 0x00065F44
		internal bool is_leap_year(int year)
		{
			return (25 * year + 11) % 33 < 8;
		}

		// Token: 0x06001C09 RID: 7177 RVA: 0x00067D54 File Offset: 0x00065F54
		public override DateTime AddMonths(DateTime time, int months)
		{
			int date = CCFixed.FromDateTime(time);
			int day;
			int num;
			int num2;
			this.dmy_from_fixed(out day, out num, out num2, date);
			num += months;
			num2 += CCMath.div_mod(out num, num, 12);
			date = this.fixed_from_dmy(day, num, num2);
			DateTime dateTime = CCFixed.ToDateTime(date).Add(time.TimeOfDay);
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001C0A RID: 7178 RVA: 0x00067DB4 File Offset: 0x00065FB4
		public override DateTime AddYears(DateTime time, int years)
		{
			int date = CCFixed.FromDateTime(time);
			int day;
			int month;
			int num;
			this.dmy_from_fixed(out day, out month, out num, date);
			num += years;
			date = this.fixed_from_dmy(day, month, num);
			DateTime dateTime = CCFixed.ToDateTime(date).Add(time.TimeOfDay);
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001C0B RID: 7179 RVA: 0x00067E08 File Offset: 0x00066008
		public override int GetDayOfMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			int result;
			int num;
			int num2;
			this.dmy_from_fixed(out result, out num, out num2, date);
			return result;
		}

		// Token: 0x06001C0C RID: 7180 RVA: 0x00067E34 File Offset: 0x00066034
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			return CCFixed.day_of_week(date);
		}

		// Token: 0x06001C0D RID: 7181 RVA: 0x00067E58 File Offset: 0x00066058
		public override int GetDayOfYear(DateTime time)
		{
			this.M_CheckDateTime(time);
			int num = CCFixed.FromDateTime(time);
			int year = this.year_from_fixed(num);
			int num2 = this.fixed_from_dmy(1, 1, year);
			return num - num2 + 1;
		}

		// Token: 0x06001C0E RID: 7182 RVA: 0x00067E8C File Offset: 0x0006608C
		public override int GetDaysInMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			if (month <= 6)
			{
				return 31;
			}
			if (month == 12 && !this.is_leap_year(year))
			{
				return 29;
			}
			return 30;
		}

		// Token: 0x06001C0F RID: 7183 RVA: 0x00067EBC File Offset: 0x000660BC
		public override int GetDaysInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return (!this.is_leap_year(year)) ? 365 : 366;
		}

		// Token: 0x06001C10 RID: 7184 RVA: 0x00067EF0 File Offset: 0x000660F0
		public override int GetEra(DateTime time)
		{
			this.M_CheckDateTime(time);
			return PersianCalendar.PersianEra;
		}

		// Token: 0x06001C11 RID: 7185 RVA: 0x00067F00 File Offset: 0x00066100
		public override int GetLeapMonth(int year, int era)
		{
			return 0;
		}

		// Token: 0x06001C12 RID: 7186 RVA: 0x00067F04 File Offset: 0x00066104
		public override int GetMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			int result;
			int num;
			this.my_from_fixed(out result, out num, date);
			return result;
		}

		// Token: 0x06001C13 RID: 7187 RVA: 0x00067F2C File Offset: 0x0006612C
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return 12;
		}

		// Token: 0x06001C14 RID: 7188 RVA: 0x00067F3C File Offset: 0x0006613C
		public override int GetYear(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			return this.year_from_fixed(date);
		}

		// Token: 0x06001C15 RID: 7189 RVA: 0x00067F60 File Offset: 0x00066160
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			return this.is_leap_year(year) && month == 12 && day == 30;
		}

		// Token: 0x06001C16 RID: 7190 RVA: 0x00067F8C File Offset: 0x0006618C
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			return false;
		}

		// Token: 0x06001C17 RID: 7191 RVA: 0x00067F9C File Offset: 0x0006619C
		public override bool IsLeapYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return this.is_leap_year(year);
		}

		// Token: 0x06001C18 RID: 7192 RVA: 0x00067FB0 File Offset: 0x000661B0
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			int date = this.fixed_from_dmy(day, month, year);
			return CCFixed.ToDateTime(date, hour, minute, second, (double)millisecond);
		}

		// Token: 0x06001C19 RID: 7193 RVA: 0x00067FF0 File Offset: 0x000661F0
		public override int ToFourDigitYear(int year)
		{
			base.M_ArgumentInRange("year", year, 0, 99);
			int num = this.twoDigitYearMax % 100;
			int num2 = this.twoDigitYearMax - num;
			if (year <= num)
			{
				return num2 + year;
			}
			return num2 + year - 100;
		}

		// Token: 0x170004AB RID: 1195
		// (get) Token: 0x06001C1A RID: 7194 RVA: 0x00068030 File Offset: 0x00066230
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.SolarCalendar;
			}
		}

		// Token: 0x170004AC RID: 1196
		// (get) Token: 0x06001C1B RID: 7195 RVA: 0x00068034 File Offset: 0x00066234
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return PersianCalendar.PersianMin;
			}
		}

		// Token: 0x170004AD RID: 1197
		// (get) Token: 0x06001C1C RID: 7196 RVA: 0x0006803C File Offset: 0x0006623C
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return PersianCalendar.PersianMax;
			}
		}

		// Token: 0x04000A87 RID: 2695
		internal const long M_MinTicks = 196036416000000000L;

		// Token: 0x04000A88 RID: 2696
		internal const int M_MinYear = 1;

		// Token: 0x04000A89 RID: 2697
		internal const int epoch = 226895;

		// Token: 0x04000A8A RID: 2698
		public static readonly int PersianEra = 1;

		// Token: 0x04000A8B RID: 2699
		private static DateTime PersianMin = new DateTime(622, 3, 21, 0, 0, 0);

		// Token: 0x04000A8C RID: 2700
		private static DateTime PersianMax = new DateTime(9999, 12, 31, 11, 59, 59);
	}
}
