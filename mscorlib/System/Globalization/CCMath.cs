﻿using System;

namespace System.Globalization
{
	// Token: 0x020001FA RID: 506
	internal class CCMath
	{
		// Token: 0x0600193F RID: 6463 RVA: 0x0005E85C File Offset: 0x0005CA5C
		public static double round(double x)
		{
			return Math.Floor(x + 0.5);
		}

		// Token: 0x06001940 RID: 6464 RVA: 0x0005E870 File Offset: 0x0005CA70
		public static double mod(double x, double y)
		{
			return x - y * Math.Floor(x / y);
		}

		// Token: 0x06001941 RID: 6465 RVA: 0x0005E880 File Offset: 0x0005CA80
		public static int div(int x, int y)
		{
			return (int)Math.Floor((double)x / (double)y);
		}

		// Token: 0x06001942 RID: 6466 RVA: 0x0005E890 File Offset: 0x0005CA90
		public static int mod(int x, int y)
		{
			return x - y * CCMath.div(x, y);
		}

		// Token: 0x06001943 RID: 6467 RVA: 0x0005E8A0 File Offset: 0x0005CAA0
		public static int div_mod(out int remainder, int x, int y)
		{
			int num = CCMath.div(x, y);
			remainder = x - y * num;
			return num;
		}

		// Token: 0x06001944 RID: 6468 RVA: 0x0005E8C0 File Offset: 0x0005CAC0
		public static int signum(double x)
		{
			if (x < 0.0)
			{
				return -1;
			}
			if (x == 0.0)
			{
				return 0;
			}
			return 1;
		}

		// Token: 0x06001945 RID: 6469 RVA: 0x0005E8E8 File Offset: 0x0005CAE8
		public static int signum(int x)
		{
			if (x < 0)
			{
				return -1;
			}
			if (x == 0)
			{
				return 0;
			}
			return 1;
		}

		// Token: 0x06001946 RID: 6470 RVA: 0x0005E8FC File Offset: 0x0005CAFC
		public static double amod(double x, double y)
		{
			double num = CCMath.mod(x, y);
			return (num != 0.0) ? num : y;
		}

		// Token: 0x06001947 RID: 6471 RVA: 0x0005E928 File Offset: 0x0005CB28
		public static int amod(int x, int y)
		{
			int num = CCMath.mod(x, y);
			return (num != 0) ? num : y;
		}
	}
}
