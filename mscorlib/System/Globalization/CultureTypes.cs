﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x0200020F RID: 527
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum CultureTypes
	{
		// Token: 0x040009BF RID: 2495
		NeutralCultures = 1,
		// Token: 0x040009C0 RID: 2496
		SpecificCultures = 2,
		// Token: 0x040009C1 RID: 2497
		InstalledWin32Cultures = 4,
		// Token: 0x040009C2 RID: 2498
		AllCultures = 7,
		// Token: 0x040009C3 RID: 2499
		UserCustomCulture = 8,
		// Token: 0x040009C4 RID: 2500
		ReplacementCultures = 16,
		// Token: 0x040009C5 RID: 2501
		WindowsOnlyCultures = 32,
		// Token: 0x040009C6 RID: 2502
		FrameworkCultures = 64
	}
}
