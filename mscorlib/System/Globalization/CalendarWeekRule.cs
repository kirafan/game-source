﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x020001F9 RID: 505
	[ComVisible(true)]
	[Serializable]
	public enum CalendarWeekRule
	{
		// Token: 0x0400092A RID: 2346
		FirstDay,
		// Token: 0x0400092B RID: 2347
		FirstFullWeek,
		// Token: 0x0400092C RID: 2348
		FirstFourDayWeek
	}
}
