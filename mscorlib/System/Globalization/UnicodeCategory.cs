﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x0200022E RID: 558
	[ComVisible(true)]
	[Serializable]
	public enum UnicodeCategory
	{
		// Token: 0x04000ABF RID: 2751
		UppercaseLetter,
		// Token: 0x04000AC0 RID: 2752
		LowercaseLetter,
		// Token: 0x04000AC1 RID: 2753
		TitlecaseLetter,
		// Token: 0x04000AC2 RID: 2754
		ModifierLetter,
		// Token: 0x04000AC3 RID: 2755
		OtherLetter,
		// Token: 0x04000AC4 RID: 2756
		NonSpacingMark,
		// Token: 0x04000AC5 RID: 2757
		SpacingCombiningMark,
		// Token: 0x04000AC6 RID: 2758
		EnclosingMark,
		// Token: 0x04000AC7 RID: 2759
		DecimalDigitNumber,
		// Token: 0x04000AC8 RID: 2760
		LetterNumber,
		// Token: 0x04000AC9 RID: 2761
		OtherNumber,
		// Token: 0x04000ACA RID: 2762
		SpaceSeparator,
		// Token: 0x04000ACB RID: 2763
		LineSeparator,
		// Token: 0x04000ACC RID: 2764
		ParagraphSeparator,
		// Token: 0x04000ACD RID: 2765
		Control,
		// Token: 0x04000ACE RID: 2766
		Format,
		// Token: 0x04000ACF RID: 2767
		Surrogate,
		// Token: 0x04000AD0 RID: 2768
		PrivateUse,
		// Token: 0x04000AD1 RID: 2769
		ConnectorPunctuation,
		// Token: 0x04000AD2 RID: 2770
		DashPunctuation,
		// Token: 0x04000AD3 RID: 2771
		OpenPunctuation,
		// Token: 0x04000AD4 RID: 2772
		ClosePunctuation,
		// Token: 0x04000AD5 RID: 2773
		InitialQuotePunctuation,
		// Token: 0x04000AD6 RID: 2774
		FinalQuotePunctuation,
		// Token: 0x04000AD7 RID: 2775
		OtherPunctuation,
		// Token: 0x04000AD8 RID: 2776
		MathSymbol,
		// Token: 0x04000AD9 RID: 2777
		CurrencySymbol,
		// Token: 0x04000ADA RID: 2778
		ModifierSymbol,
		// Token: 0x04000ADB RID: 2779
		OtherSymbol,
		// Token: 0x04000ADC RID: 2780
		OtherNotAssigned
	}
}
