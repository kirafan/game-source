﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000223 RID: 547
	[ComVisible(true)]
	[Flags]
	[Serializable]
	public enum NumberStyles
	{
		// Token: 0x04000A76 RID: 2678
		None = 0,
		// Token: 0x04000A77 RID: 2679
		AllowLeadingWhite = 1,
		// Token: 0x04000A78 RID: 2680
		AllowTrailingWhite = 2,
		// Token: 0x04000A79 RID: 2681
		AllowLeadingSign = 4,
		// Token: 0x04000A7A RID: 2682
		AllowTrailingSign = 8,
		// Token: 0x04000A7B RID: 2683
		AllowParentheses = 16,
		// Token: 0x04000A7C RID: 2684
		AllowDecimalPoint = 32,
		// Token: 0x04000A7D RID: 2685
		AllowThousands = 64,
		// Token: 0x04000A7E RID: 2686
		AllowExponent = 128,
		// Token: 0x04000A7F RID: 2687
		AllowCurrencySymbol = 256,
		// Token: 0x04000A80 RID: 2688
		AllowHexSpecifier = 512,
		// Token: 0x04000A81 RID: 2689
		Integer = 7,
		// Token: 0x04000A82 RID: 2690
		HexNumber = 515,
		// Token: 0x04000A83 RID: 2691
		Number = 111,
		// Token: 0x04000A84 RID: 2692
		Float = 167,
		// Token: 0x04000A85 RID: 2693
		Currency = 383,
		// Token: 0x04000A86 RID: 2694
		Any = 511
	}
}
