﻿using System;
using System.IO;

namespace System.Globalization
{
	// Token: 0x0200022D RID: 557
	[MonoTODO("Serialization format not compatible with .NET")]
	[Serializable]
	public class UmAlQuraCalendar : Calendar
	{
		// Token: 0x06001CA4 RID: 7332 RVA: 0x00069A40 File Offset: 0x00067C40
		public UmAlQuraCalendar()
		{
			this.M_AbbrEraNames = new string[]
			{
				"A.H."
			};
			this.M_EraNames = new string[]
			{
				"Anno Hegirae"
			};
			if (this.twoDigitYearMax == 99)
			{
				this.twoDigitYearMax = 1451;
			}
		}

		// Token: 0x170004D6 RID: 1238
		// (get) Token: 0x06001CA6 RID: 7334 RVA: 0x00069AF0 File Offset: 0x00067CF0
		public override int[] Eras
		{
			get
			{
				return new int[]
				{
					1
				};
			}
		}

		// Token: 0x170004D7 RID: 1239
		// (get) Token: 0x06001CA7 RID: 7335 RVA: 0x00069AFC File Offset: 0x00067CFC
		// (set) Token: 0x06001CA8 RID: 7336 RVA: 0x00069B04 File Offset: 0x00067D04
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x170004D8 RID: 1240
		// (get) Token: 0x06001CA9 RID: 7337 RVA: 0x00069B34 File Offset: 0x00067D34
		// (set) Token: 0x06001CAA RID: 7338 RVA: 0x00069B3C File Offset: 0x00067D3C
		internal virtual int AddHijriDate
		{
			get
			{
				return this.M_AddHijriDate;
			}
			set
			{
				base.CheckReadOnly();
				if (value < -3 && value > 3)
				{
					throw new ArgumentOutOfRangeException("AddHijriDate", "Value should be between -3 and 3.");
				}
				this.M_AddHijriDate = value;
			}
		}

		// Token: 0x06001CAB RID: 7339 RVA: 0x00069B78 File Offset: 0x00067D78
		internal void M_CheckFixedHijri(string param, int rdHijri)
		{
			if (rdHijri < UmAlQuraCalendar.M_MinFixed || rdHijri > UmAlQuraCalendar.M_MaxFixed - this.AddHijriDate)
			{
				StringWriter stringWriter = new StringWriter();
				int num;
				int num2;
				int num3;
				CCHijriCalendar.dmy_from_fixed(out num, out num2, out num3, UmAlQuraCalendar.M_MaxFixed - this.AddHijriDate);
				if (this.AddHijriDate != 0)
				{
					stringWriter.Write("This HijriCalendar (AddHijriDate {0}) allows dates from 1. 1. 1 to {1}. {2}. {3}.", new object[]
					{
						this.AddHijriDate,
						num,
						num2,
						num3
					});
				}
				else
				{
					stringWriter.Write("HijriCalendar allows dates from 1.1.1 to {0}.{1}.{2}.", num, num2, num3);
				}
				throw new ArgumentOutOfRangeException(param, stringWriter.ToString());
			}
		}

		// Token: 0x06001CAC RID: 7340 RVA: 0x00069C34 File Offset: 0x00067E34
		internal void M_CheckDateTime(DateTime time)
		{
			int rdHijri = CCFixed.FromDateTime(time) - this.AddHijriDate;
			this.M_CheckFixedHijri("time", rdHijri);
		}

		// Token: 0x06001CAD RID: 7341 RVA: 0x00069C5C File Offset: 0x00067E5C
		internal int M_FromDateTime(DateTime time)
		{
			return CCFixed.FromDateTime(time) - this.AddHijriDate;
		}

		// Token: 0x06001CAE RID: 7342 RVA: 0x00069C6C File Offset: 0x00067E6C
		internal DateTime M_ToDateTime(int rd)
		{
			return CCFixed.ToDateTime(rd + this.AddHijriDate);
		}

		// Token: 0x06001CAF RID: 7343 RVA: 0x00069C7C File Offset: 0x00067E7C
		internal DateTime M_ToDateTime(int date, int hour, int minute, int second, int milliseconds)
		{
			return CCFixed.ToDateTime(date + this.AddHijriDate, hour, minute, second, (double)milliseconds);
		}

		// Token: 0x06001CB0 RID: 7344 RVA: 0x00069C94 File Offset: 0x00067E94
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = 1;
			}
			if (era != 1)
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001CB1 RID: 7345 RVA: 0x00069CB4 File Offset: 0x00067EB4
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			base.M_ArgumentInRange("year", year, 1, 9666);
		}

		// Token: 0x06001CB2 RID: 7346 RVA: 0x00069CDC File Offset: 0x00067EDC
		internal void M_CheckYME(int year, int month, ref int era)
		{
			this.M_CheckYE(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
			if (year == 9666)
			{
				int rdHijri = CCHijriCalendar.fixed_from_dmy(1, month, year);
				this.M_CheckFixedHijri("month", rdHijri);
			}
		}

		// Token: 0x06001CB3 RID: 7347 RVA: 0x00069D30 File Offset: 0x00067F30
		internal void M_CheckYMDE(int year, int month, int day, ref int era)
		{
			this.M_CheckYME(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, 1));
			if (year == 9666)
			{
				int rdHijri = CCHijriCalendar.fixed_from_dmy(day, month, year);
				this.M_CheckFixedHijri("day", rdHijri);
			}
		}

		// Token: 0x06001CB4 RID: 7348 RVA: 0x00069D80 File Offset: 0x00067F80
		public override DateTime AddMonths(DateTime time, int months)
		{
			int num = this.M_FromDateTime(time);
			int day;
			int num2;
			int num3;
			CCHijriCalendar.dmy_from_fixed(out day, out num2, out num3, num);
			num2 += months;
			num3 += CCMath.div_mod(out num2, num2, 12);
			num = CCHijriCalendar.fixed_from_dmy(day, num2, num3);
			this.M_CheckFixedHijri("time", num);
			return this.M_ToDateTime(num).Add(time.TimeOfDay);
		}

		// Token: 0x06001CB5 RID: 7349 RVA: 0x00069DE0 File Offset: 0x00067FE0
		public override DateTime AddYears(DateTime time, int years)
		{
			int num = this.M_FromDateTime(time);
			int day;
			int month;
			int num2;
			CCHijriCalendar.dmy_from_fixed(out day, out month, out num2, num);
			num2 += years;
			num = CCHijriCalendar.fixed_from_dmy(day, month, num2);
			this.M_CheckFixedHijri("time", num);
			return this.M_ToDateTime(num).Add(time.TimeOfDay);
		}

		// Token: 0x06001CB6 RID: 7350 RVA: 0x00069E34 File Offset: 0x00068034
		public override int GetDayOfMonth(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			return CCHijriCalendar.day_from_fixed(num);
		}

		// Token: 0x06001CB7 RID: 7351 RVA: 0x00069E5C File Offset: 0x0006805C
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			return CCFixed.day_of_week(num);
		}

		// Token: 0x06001CB8 RID: 7352 RVA: 0x00069E84 File Offset: 0x00068084
		public override int GetDayOfYear(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			int year = CCHijriCalendar.year_from_fixed(num);
			int num2 = CCHijriCalendar.fixed_from_dmy(1, 1, year);
			return num - num2 + 1;
		}

		// Token: 0x06001CB9 RID: 7353 RVA: 0x00069EBC File Offset: 0x000680BC
		public override int GetDaysInMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			int num = CCHijriCalendar.fixed_from_dmy(1, month, year);
			int num2 = CCHijriCalendar.fixed_from_dmy(1, month + 1, year);
			return num2 - num;
		}

		// Token: 0x06001CBA RID: 7354 RVA: 0x00069EEC File Offset: 0x000680EC
		public override int GetDaysInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			int num = CCHijriCalendar.fixed_from_dmy(1, 1, year);
			int num2 = CCHijriCalendar.fixed_from_dmy(1, 1, year + 1);
			return num2 - num;
		}

		// Token: 0x06001CBB RID: 7355 RVA: 0x00069F1C File Offset: 0x0006811C
		public override int GetEra(DateTime time)
		{
			this.M_CheckDateTime(time);
			return 1;
		}

		// Token: 0x06001CBC RID: 7356 RVA: 0x00069F28 File Offset: 0x00068128
		public override int GetLeapMonth(int year, int era)
		{
			return 0;
		}

		// Token: 0x06001CBD RID: 7357 RVA: 0x00069F2C File Offset: 0x0006812C
		public override int GetMonth(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			return CCHijriCalendar.month_from_fixed(num);
		}

		// Token: 0x06001CBE RID: 7358 RVA: 0x00069F54 File Offset: 0x00068154
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return 12;
		}

		// Token: 0x06001CBF RID: 7359 RVA: 0x00069F64 File Offset: 0x00068164
		public override int GetYear(DateTime time)
		{
			int num = this.M_FromDateTime(time);
			this.M_CheckFixedHijri("time", num);
			return CCHijriCalendar.year_from_fixed(num);
		}

		// Token: 0x06001CC0 RID: 7360 RVA: 0x00069F8C File Offset: 0x0006818C
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			return this.IsLeapYear(year) && month == 12 && day == 30;
		}

		// Token: 0x06001CC1 RID: 7361 RVA: 0x00069FB8 File Offset: 0x000681B8
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			return false;
		}

		// Token: 0x06001CC2 RID: 7362 RVA: 0x00069FC8 File Offset: 0x000681C8
		public override bool IsLeapYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return CCHijriCalendar.is_leap_year(year);
		}

		// Token: 0x06001CC3 RID: 7363 RVA: 0x00069FDC File Offset: 0x000681DC
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			int date = CCHijriCalendar.fixed_from_dmy(day, month, year);
			return this.M_ToDateTime(date, hour, minute, second, millisecond);
		}

		// Token: 0x06001CC4 RID: 7364 RVA: 0x0006A01C File Offset: 0x0006821C
		public override int ToFourDigitYear(int year)
		{
			return base.ToFourDigitYear(year);
		}

		// Token: 0x170004D9 RID: 1241
		// (get) Token: 0x06001CC5 RID: 7365 RVA: 0x0006A028 File Offset: 0x00068228
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.LunarCalendar;
			}
		}

		// Token: 0x170004DA RID: 1242
		// (get) Token: 0x06001CC6 RID: 7366 RVA: 0x0006A02C File Offset: 0x0006822C
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return UmAlQuraCalendar.Min;
			}
		}

		// Token: 0x170004DB RID: 1243
		// (get) Token: 0x06001CC7 RID: 7367 RVA: 0x0006A034 File Offset: 0x00068234
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return UmAlQuraCalendar.Max;
			}
		}

		// Token: 0x04000AB8 RID: 2744
		public const int UmAlQuraEra = 1;

		// Token: 0x04000AB9 RID: 2745
		internal static readonly int M_MinFixed = CCHijriCalendar.fixed_from_dmy(1, 1, 1);

		// Token: 0x04000ABA RID: 2746
		internal static readonly int M_MaxFixed = CCGregorianCalendar.fixed_from_dmy(31, 12, 9999);

		// Token: 0x04000ABB RID: 2747
		internal int M_AddHijriDate;

		// Token: 0x04000ABC RID: 2748
		private static DateTime Min = new DateTime(622, 7, 18, 0, 0, 0);

		// Token: 0x04000ABD RID: 2749
		private static DateTime Max = new DateTime(9999, 12, 31, 11, 59, 59);
	}
}
