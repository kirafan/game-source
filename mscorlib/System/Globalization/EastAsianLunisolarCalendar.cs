﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000215 RID: 533
	[ComVisible(true)]
	[Serializable]
	public abstract class EastAsianLunisolarCalendar : Calendar
	{
		// Token: 0x06001AA9 RID: 6825 RVA: 0x00063990 File Offset: 0x00061B90
		internal EastAsianLunisolarCalendar(CCEastAsianLunisolarEraHandler eraHandler)
		{
			this.M_EraHandler = eraHandler;
		}

		// Token: 0x1700045A RID: 1114
		// (get) Token: 0x06001AAA RID: 6826 RVA: 0x000639A0 File Offset: 0x00061BA0
		// (set) Token: 0x06001AAB RID: 6827 RVA: 0x000639A8 File Offset: 0x00061BA8
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x06001AAC RID: 6828 RVA: 0x000639D8 File Offset: 0x00061BD8
		internal void M_CheckDateTime(DateTime time)
		{
			this.M_EraHandler.CheckDateTime(time);
		}

		// Token: 0x1700045B RID: 1115
		// (get) Token: 0x06001AAD RID: 6829 RVA: 0x000639E8 File Offset: 0x00061BE8
		internal virtual int ActualCurrentEra
		{
			get
			{
				return 1;
			}
		}

		// Token: 0x06001AAE RID: 6830 RVA: 0x000639EC File Offset: 0x00061BEC
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = this.ActualCurrentEra;
			}
			if (!this.M_EraHandler.ValidEra(era))
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001AAF RID: 6831 RVA: 0x00063A28 File Offset: 0x00061C28
		internal int M_CheckYEG(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			return this.M_EraHandler.GregorianYear(year, era);
		}

		// Token: 0x06001AB0 RID: 6832 RVA: 0x00063A40 File Offset: 0x00061C40
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckYEG(year, ref era);
		}

		// Token: 0x06001AB1 RID: 6833 RVA: 0x00063A4C File Offset: 0x00061C4C
		internal int M_CheckYMEG(int year, int month, ref int era)
		{
			int result = this.M_CheckYEG(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
			return result;
		}

		// Token: 0x06001AB2 RID: 6834 RVA: 0x00063A84 File Offset: 0x00061C84
		internal int M_CheckYMDEG(int year, int month, int day, ref int era)
		{
			int result = this.M_CheckYMEG(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, era));
			return result;
		}

		// Token: 0x06001AB3 RID: 6835 RVA: 0x00063AB8 File Offset: 0x00061CB8
		[MonoTODO]
		public override DateTime AddMonths(DateTime time, int months)
		{
			DateTime dateTime = CCEastAsianLunisolarCalendar.AddMonths(time, months);
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001AB4 RID: 6836 RVA: 0x00063AD8 File Offset: 0x00061CD8
		[MonoTODO]
		public override DateTime AddYears(DateTime time, int years)
		{
			DateTime dateTime = CCEastAsianLunisolarCalendar.AddYears(time, years);
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001AB5 RID: 6837 RVA: 0x00063AF8 File Offset: 0x00061CF8
		[MonoTODO]
		public override int GetDayOfMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			return CCEastAsianLunisolarCalendar.GetDayOfMonth(time);
		}

		// Token: 0x06001AB6 RID: 6838 RVA: 0x00063B08 File Offset: 0x00061D08
		[MonoTODO]
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			return CCFixed.day_of_week(date);
		}

		// Token: 0x06001AB7 RID: 6839 RVA: 0x00063B2C File Offset: 0x00061D2C
		[MonoTODO]
		public override int GetDayOfYear(DateTime time)
		{
			this.M_CheckDateTime(time);
			return CCEastAsianLunisolarCalendar.GetDayOfYear(time);
		}

		// Token: 0x06001AB8 RID: 6840 RVA: 0x00063B3C File Offset: 0x00061D3C
		[MonoTODO]
		public override int GetDaysInMonth(int year, int month, int era)
		{
			int gyear = this.M_CheckYMEG(year, month, ref era);
			return CCEastAsianLunisolarCalendar.GetDaysInMonth(gyear, month);
		}

		// Token: 0x06001AB9 RID: 6841 RVA: 0x00063B5C File Offset: 0x00061D5C
		[MonoTODO]
		public override int GetDaysInYear(int year, int era)
		{
			int year2 = this.M_CheckYEG(year, ref era);
			return CCEastAsianLunisolarCalendar.GetDaysInYear(year2);
		}

		// Token: 0x06001ABA RID: 6842 RVA: 0x00063B7C File Offset: 0x00061D7C
		[MonoTODO]
		public override int GetLeapMonth(int year, int era)
		{
			return base.GetLeapMonth(year, era);
		}

		// Token: 0x06001ABB RID: 6843 RVA: 0x00063B88 File Offset: 0x00061D88
		[MonoTODO]
		public override int GetMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			return CCEastAsianLunisolarCalendar.GetMonth(time);
		}

		// Token: 0x06001ABC RID: 6844 RVA: 0x00063B98 File Offset: 0x00061D98
		[MonoTODO]
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return (!this.IsLeapYear(year, era)) ? 12 : 13;
		}

		// Token: 0x06001ABD RID: 6845 RVA: 0x00063BBC File Offset: 0x00061DBC
		public override int GetYear(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int num;
			return this.M_EraHandler.EraYear(out num, date);
		}

		// Token: 0x06001ABE RID: 6846 RVA: 0x00063BE0 File Offset: 0x00061DE0
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			int gyear = this.M_CheckYMDEG(year, month, day, ref era);
			return CCEastAsianLunisolarCalendar.IsLeapMonth(gyear, month);
		}

		// Token: 0x06001ABF RID: 6847 RVA: 0x00063C00 File Offset: 0x00061E00
		[MonoTODO]
		public override bool IsLeapMonth(int year, int month, int era)
		{
			int gyear = this.M_CheckYMEG(year, month, ref era);
			return CCEastAsianLunisolarCalendar.IsLeapMonth(gyear, month);
		}

		// Token: 0x06001AC0 RID: 6848 RVA: 0x00063C20 File Offset: 0x00061E20
		public override bool IsLeapYear(int year, int era)
		{
			int gyear = this.M_CheckYEG(year, ref era);
			return CCEastAsianLunisolarCalendar.IsLeapYear(gyear);
		}

		// Token: 0x06001AC1 RID: 6849 RVA: 0x00063C40 File Offset: 0x00061E40
		[MonoTODO]
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			int year2 = this.M_CheckYMDEG(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			return CCGregorianCalendar.ToDateTime(year2, month, day, hour, minute, second, millisecond);
		}

		// Token: 0x06001AC2 RID: 6850 RVA: 0x00063C78 File Offset: 0x00061E78
		[MonoTODO]
		public override int ToFourDigitYear(int year)
		{
			if (year < 0)
			{
				throw new ArgumentOutOfRangeException("year", "Non-negative number required.");
			}
			int num = 0;
			this.M_CheckYE(year, ref num);
			return year;
		}

		// Token: 0x1700045C RID: 1116
		// (get) Token: 0x06001AC3 RID: 6851 RVA: 0x00063CA8 File Offset: 0x00061EA8
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.LunisolarCalendar;
			}
		}

		// Token: 0x06001AC4 RID: 6852 RVA: 0x00063CAC File Offset: 0x00061EAC
		public int GetCelestialStem(int sexagenaryYear)
		{
			if (sexagenaryYear < 1 || 60 < sexagenaryYear)
			{
				throw new ArgumentOutOfRangeException("sexagendaryYear is less than 0 or greater than 60");
			}
			return (sexagenaryYear - 1) % 10 + 1;
		}

		// Token: 0x06001AC5 RID: 6853 RVA: 0x00063CDC File Offset: 0x00061EDC
		public virtual int GetSexagenaryYear(DateTime time)
		{
			return (this.GetYear(time) - 1900) % 60;
		}

		// Token: 0x06001AC6 RID: 6854 RVA: 0x00063CF0 File Offset: 0x00061EF0
		public int GetTerrestrialBranch(int sexagenaryYear)
		{
			if (sexagenaryYear < 1 || 60 < sexagenaryYear)
			{
				throw new ArgumentOutOfRangeException("sexagendaryYear is less than 0 or greater than 60");
			}
			return (sexagenaryYear - 1) % 12 + 1;
		}

		// Token: 0x04000A19 RID: 2585
		internal readonly CCEastAsianLunisolarEraHandler M_EraHandler;
	}
}
