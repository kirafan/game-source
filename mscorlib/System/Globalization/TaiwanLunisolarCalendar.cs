﻿using System;

namespace System.Globalization
{
	// Token: 0x02000228 RID: 552
	[Serializable]
	public class TaiwanLunisolarCalendar : EastAsianLunisolarCalendar
	{
		// Token: 0x06001C61 RID: 7265 RVA: 0x00068AD0 File Offset: 0x00066CD0
		[MonoTODO]
		public TaiwanLunisolarCalendar() : base(TaiwanLunisolarCalendar.era_handler)
		{
		}

		// Token: 0x06001C62 RID: 7266 RVA: 0x00068AE0 File Offset: 0x00066CE0
		static TaiwanLunisolarCalendar()
		{
			TaiwanLunisolarCalendar.era_handler = new CCEastAsianLunisolarEraHandler();
			TaiwanLunisolarCalendar.era_handler.appendEra(1, CCFixed.FromDateTime(TaiwanLunisolarCalendar.TaiwanMin), CCFixed.FromDateTime(TaiwanLunisolarCalendar.TaiwanMax));
		}

		// Token: 0x170004C3 RID: 1219
		// (get) Token: 0x06001C63 RID: 7267 RVA: 0x00068B48 File Offset: 0x00066D48
		public override int[] Eras
		{
			get
			{
				return (int[])TaiwanLunisolarCalendar.era_handler.Eras.Clone();
			}
		}

		// Token: 0x06001C64 RID: 7268 RVA: 0x00068B60 File Offset: 0x00066D60
		public override int GetEra(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int result;
			TaiwanLunisolarCalendar.era_handler.EraYear(out result, date);
			return result;
		}

		// Token: 0x170004C4 RID: 1220
		// (get) Token: 0x06001C65 RID: 7269 RVA: 0x00068B84 File Offset: 0x00066D84
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return TaiwanLunisolarCalendar.TaiwanMin;
			}
		}

		// Token: 0x170004C5 RID: 1221
		// (get) Token: 0x06001C66 RID: 7270 RVA: 0x00068B8C File Offset: 0x00066D8C
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return TaiwanLunisolarCalendar.TaiwanMax;
			}
		}

		// Token: 0x04000A9D RID: 2717
		private const int TaiwanEra = 1;

		// Token: 0x04000A9E RID: 2718
		internal static readonly CCEastAsianLunisolarEraHandler era_handler;

		// Token: 0x04000A9F RID: 2719
		private static DateTime TaiwanMin = new DateTime(1912, 2, 18);

		// Token: 0x04000AA0 RID: 2720
		private static DateTime TaiwanMax = new DateTime(2051, 2, 10, 23, 59, 59, 999);
	}
}
