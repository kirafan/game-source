﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000213 RID: 531
	[ComVisible(true)]
	[Serializable]
	public class DaylightTime
	{
		// Token: 0x06001AA5 RID: 6821 RVA: 0x00063958 File Offset: 0x00061B58
		public DaylightTime(DateTime start, DateTime end, TimeSpan delta)
		{
			this.m_start = start;
			this.m_end = end;
			this.m_delta = delta;
		}

		// Token: 0x17000457 RID: 1111
		// (get) Token: 0x06001AA6 RID: 6822 RVA: 0x00063978 File Offset: 0x00061B78
		public DateTime Start
		{
			get
			{
				return this.m_start;
			}
		}

		// Token: 0x17000458 RID: 1112
		// (get) Token: 0x06001AA7 RID: 6823 RVA: 0x00063980 File Offset: 0x00061B80
		public DateTime End
		{
			get
			{
				return this.m_end;
			}
		}

		// Token: 0x17000459 RID: 1113
		// (get) Token: 0x06001AA8 RID: 6824 RVA: 0x00063988 File Offset: 0x00061B88
		public TimeSpan Delta
		{
			get
			{
				return this.m_delta;
			}
		}

		// Token: 0x04000A12 RID: 2578
		private DateTime m_start;

		// Token: 0x04000A13 RID: 2579
		private DateTime m_end;

		// Token: 0x04000A14 RID: 2580
		private TimeSpan m_delta;
	}
}
