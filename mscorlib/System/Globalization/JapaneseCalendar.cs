﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x0200021D RID: 541
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with .NET")]
	[Serializable]
	public class JapaneseCalendar : Calendar
	{
		// Token: 0x06001B49 RID: 6985 RVA: 0x0006596C File Offset: 0x00063B6C
		public JapaneseCalendar()
		{
			this.M_AbbrEraNames = new string[]
			{
				"M",
				"T",
				"S",
				"H"
			};
			this.M_EraNames = new string[]
			{
				"Meiji",
				"Taisho",
				"Showa",
				"Heisei"
			};
		}

		// Token: 0x06001B4A RID: 6986 RVA: 0x000659D8 File Offset: 0x00063BD8
		static JapaneseCalendar()
		{
			JapaneseCalendar.M_EraHandler = new CCGregorianEraHandler();
			JapaneseCalendar.M_EraHandler.appendEra(1, CCGregorianCalendar.fixed_from_dmy(8, 9, 1868), CCGregorianCalendar.fixed_from_dmy(29, 7, 1912));
			JapaneseCalendar.M_EraHandler.appendEra(2, CCGregorianCalendar.fixed_from_dmy(30, 7, 1912), CCGregorianCalendar.fixed_from_dmy(24, 12, 1926));
			JapaneseCalendar.M_EraHandler.appendEra(3, CCGregorianCalendar.fixed_from_dmy(25, 12, 1926), CCGregorianCalendar.fixed_from_dmy(7, 1, 1989));
			JapaneseCalendar.M_EraHandler.appendEra(4, CCGregorianCalendar.fixed_from_dmy(8, 1, 1989));
		}

		// Token: 0x17000472 RID: 1138
		// (get) Token: 0x06001B4B RID: 6987 RVA: 0x00065AA4 File Offset: 0x00063CA4
		public override int[] Eras
		{
			get
			{
				return (int[])JapaneseCalendar.M_EraHandler.Eras.Clone();
			}
		}

		// Token: 0x17000473 RID: 1139
		// (get) Token: 0x06001B4C RID: 6988 RVA: 0x00065ABC File Offset: 0x00063CBC
		// (set) Token: 0x06001B4D RID: 6989 RVA: 0x00065AC4 File Offset: 0x00063CC4
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x06001B4E RID: 6990 RVA: 0x00065AF4 File Offset: 0x00063CF4
		internal void M_CheckDateTime(DateTime time)
		{
			JapaneseCalendar.M_EraHandler.CheckDateTime(time);
		}

		// Token: 0x06001B4F RID: 6991 RVA: 0x00065B04 File Offset: 0x00063D04
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = 4;
			}
			if (!JapaneseCalendar.M_EraHandler.ValidEra(era))
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001B50 RID: 6992 RVA: 0x00065B38 File Offset: 0x00063D38
		internal int M_CheckYEG(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			return JapaneseCalendar.M_EraHandler.GregorianYear(year, era);
		}

		// Token: 0x06001B51 RID: 6993 RVA: 0x00065B50 File Offset: 0x00063D50
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckYEG(year, ref era);
		}

		// Token: 0x06001B52 RID: 6994 RVA: 0x00065B5C File Offset: 0x00063D5C
		internal int M_CheckYMEG(int year, int month, ref int era)
		{
			int result = this.M_CheckYEG(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
			return result;
		}

		// Token: 0x06001B53 RID: 6995 RVA: 0x00065B94 File Offset: 0x00063D94
		internal int M_CheckYMDEG(int year, int month, int day, ref int era)
		{
			int result = this.M_CheckYMEG(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, era));
			return result;
		}

		// Token: 0x06001B54 RID: 6996 RVA: 0x00065BC8 File Offset: 0x00063DC8
		public override DateTime AddMonths(DateTime time, int months)
		{
			DateTime dateTime = CCGregorianCalendar.AddMonths(time, months);
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001B55 RID: 6997 RVA: 0x00065BE8 File Offset: 0x00063DE8
		public override DateTime AddYears(DateTime time, int years)
		{
			DateTime dateTime = CCGregorianCalendar.AddYears(time, years);
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001B56 RID: 6998 RVA: 0x00065C08 File Offset: 0x00063E08
		public override int GetDayOfMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			return CCGregorianCalendar.GetDayOfMonth(time);
		}

		// Token: 0x06001B57 RID: 6999 RVA: 0x00065C18 File Offset: 0x00063E18
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			return CCFixed.day_of_week(date);
		}

		// Token: 0x06001B58 RID: 7000 RVA: 0x00065C3C File Offset: 0x00063E3C
		public override int GetDayOfYear(DateTime time)
		{
			this.M_CheckDateTime(time);
			return CCGregorianCalendar.GetDayOfYear(time);
		}

		// Token: 0x06001B59 RID: 7001 RVA: 0x00065C4C File Offset: 0x00063E4C
		public override int GetDaysInMonth(int year, int month, int era)
		{
			int year2 = this.M_CheckYMEG(year, month, ref era);
			return CCGregorianCalendar.GetDaysInMonth(year2, month);
		}

		// Token: 0x06001B5A RID: 7002 RVA: 0x00065C6C File Offset: 0x00063E6C
		public override int GetDaysInYear(int year, int era)
		{
			int year2 = this.M_CheckYEG(year, ref era);
			return CCGregorianCalendar.GetDaysInYear(year2);
		}

		// Token: 0x06001B5B RID: 7003 RVA: 0x00065C8C File Offset: 0x00063E8C
		public override int GetEra(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int result;
			JapaneseCalendar.M_EraHandler.EraYear(out result, date);
			return result;
		}

		// Token: 0x06001B5C RID: 7004 RVA: 0x00065CB0 File Offset: 0x00063EB0
		[ComVisible(false)]
		public override int GetLeapMonth(int year, int era)
		{
			return 0;
		}

		// Token: 0x06001B5D RID: 7005 RVA: 0x00065CB4 File Offset: 0x00063EB4
		public override int GetMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			return CCGregorianCalendar.GetMonth(time);
		}

		// Token: 0x06001B5E RID: 7006 RVA: 0x00065CC4 File Offset: 0x00063EC4
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return 12;
		}

		// Token: 0x06001B5F RID: 7007 RVA: 0x00065CD4 File Offset: 0x00063ED4
		[ComVisible(false)]
		public override int GetWeekOfYear(DateTime time, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
		{
			return base.GetWeekOfYear(time, rule, firstDayOfWeek);
		}

		// Token: 0x06001B60 RID: 7008 RVA: 0x00065CE0 File Offset: 0x00063EE0
		public override int GetYear(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int num;
			return JapaneseCalendar.M_EraHandler.EraYear(out num, date);
		}

		// Token: 0x06001B61 RID: 7009 RVA: 0x00065D04 File Offset: 0x00063F04
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			int year2 = this.M_CheckYMDEG(year, month, day, ref era);
			return CCGregorianCalendar.IsLeapDay(year2, month, day);
		}

		// Token: 0x06001B62 RID: 7010 RVA: 0x00065D28 File Offset: 0x00063F28
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYMEG(year, month, ref era);
			return false;
		}

		// Token: 0x06001B63 RID: 7011 RVA: 0x00065D38 File Offset: 0x00063F38
		public override bool IsLeapYear(int year, int era)
		{
			int year2 = this.M_CheckYEG(year, ref era);
			return CCGregorianCalendar.is_leap_year(year2);
		}

		// Token: 0x06001B64 RID: 7012 RVA: 0x00065D58 File Offset: 0x00063F58
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			int year2 = this.M_CheckYMDEG(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			return CCGregorianCalendar.ToDateTime(year2, month, day, hour, minute, second, millisecond);
		}

		// Token: 0x06001B65 RID: 7013 RVA: 0x00065D90 File Offset: 0x00063F90
		public override int ToFourDigitYear(int year)
		{
			if (year < 0)
			{
				throw new ArgumentOutOfRangeException("year", "Non-negative number required.");
			}
			int num = 0;
			this.M_CheckYE(year, ref num);
			return year;
		}

		// Token: 0x17000474 RID: 1140
		// (get) Token: 0x06001B66 RID: 7014 RVA: 0x00065DC0 File Offset: 0x00063FC0
		[ComVisible(false)]
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.SolarCalendar;
			}
		}

		// Token: 0x17000475 RID: 1141
		// (get) Token: 0x06001B67 RID: 7015 RVA: 0x00065DC4 File Offset: 0x00063FC4
		[ComVisible(false)]
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return JapaneseCalendar.JapanMin;
			}
		}

		// Token: 0x17000476 RID: 1142
		// (get) Token: 0x06001B68 RID: 7016 RVA: 0x00065DCC File Offset: 0x00063FCC
		[ComVisible(false)]
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return JapaneseCalendar.JapanMax;
			}
		}

		// Token: 0x04000A3C RID: 2620
		internal static readonly CCGregorianEraHandler M_EraHandler;

		// Token: 0x04000A3D RID: 2621
		private static DateTime JapanMin = new DateTime(1868, 9, 8, 0, 0, 0);

		// Token: 0x04000A3E RID: 2622
		private static DateTime JapanMax = new DateTime(9999, 12, 31, 11, 59, 59);
	}
}
