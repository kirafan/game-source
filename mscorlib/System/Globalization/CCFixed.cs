﻿using System;

namespace System.Globalization
{
	// Token: 0x020001FB RID: 507
	internal class CCFixed
	{
		// Token: 0x06001949 RID: 6473 RVA: 0x0005E954 File Offset: 0x0005CB54
		public static DateTime ToDateTime(int date)
		{
			long ticks = (long)(date - 1) * 864000000000L;
			return new DateTime(ticks);
		}

		// Token: 0x0600194A RID: 6474 RVA: 0x0005E978 File Offset: 0x0005CB78
		public static DateTime ToDateTime(int date, int hour, int minute, int second, double milliseconds)
		{
			return CCFixed.ToDateTime(date).AddHours((double)hour).AddMinutes((double)minute).AddSeconds((double)second).AddMilliseconds(milliseconds);
		}

		// Token: 0x0600194B RID: 6475 RVA: 0x0005E9B4 File Offset: 0x0005CBB4
		public static int FromDateTime(DateTime time)
		{
			return 1 + (int)(time.Ticks / 864000000000L);
		}

		// Token: 0x0600194C RID: 6476 RVA: 0x0005E9CC File Offset: 0x0005CBCC
		public static DayOfWeek day_of_week(int date)
		{
			return (DayOfWeek)CCMath.mod(date, 7);
		}

		// Token: 0x0600194D RID: 6477 RVA: 0x0005E9D8 File Offset: 0x0005CBD8
		public static int kday_on_or_before(int date, int k)
		{
			return date - (int)CCFixed.day_of_week(date - k);
		}

		// Token: 0x0600194E RID: 6478 RVA: 0x0005E9E4 File Offset: 0x0005CBE4
		public static int kday_on_or_after(int date, int k)
		{
			return CCFixed.kday_on_or_before(date + 6, k);
		}

		// Token: 0x0600194F RID: 6479 RVA: 0x0005E9F0 File Offset: 0x0005CBF0
		public static int kd_nearest(int date, int k)
		{
			return CCFixed.kday_on_or_before(date + 3, k);
		}

		// Token: 0x06001950 RID: 6480 RVA: 0x0005E9FC File Offset: 0x0005CBFC
		public static int kday_after(int date, int k)
		{
			return CCFixed.kday_on_or_before(date + 7, k);
		}

		// Token: 0x06001951 RID: 6481 RVA: 0x0005EA08 File Offset: 0x0005CC08
		public static int kday_before(int date, int k)
		{
			return CCFixed.kday_on_or_before(date - 1, k);
		}
	}
}
