﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000218 RID: 536
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with.NET")]
	[Serializable]
	public class HebrewCalendar : Calendar
	{
		// Token: 0x06001AE7 RID: 6887 RVA: 0x0006403C File Offset: 0x0006223C
		public HebrewCalendar()
		{
			this.M_AbbrEraNames = new string[]
			{
				"A.M."
			};
			this.M_EraNames = new string[]
			{
				"Anno Mundi"
			};
			if (this.twoDigitYearMax == 99)
			{
				this.twoDigitYearMax = 5790;
			}
		}

		// Token: 0x17000463 RID: 1123
		// (get) Token: 0x06001AE9 RID: 6889 RVA: 0x000640C8 File Offset: 0x000622C8
		internal override int M_MaxYear
		{
			get
			{
				return 6000;
			}
		}

		// Token: 0x17000464 RID: 1124
		// (get) Token: 0x06001AEA RID: 6890 RVA: 0x000640D0 File Offset: 0x000622D0
		public override int[] Eras
		{
			get
			{
				return new int[]
				{
					HebrewCalendar.HebrewEra
				};
			}
		}

		// Token: 0x17000465 RID: 1125
		// (get) Token: 0x06001AEB RID: 6891 RVA: 0x000640E0 File Offset: 0x000622E0
		// (set) Token: 0x06001AEC RID: 6892 RVA: 0x000640E8 File Offset: 0x000622E8
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 5343, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x06001AED RID: 6893 RVA: 0x0006411C File Offset: 0x0006231C
		internal void M_CheckDateTime(DateTime time)
		{
			if (time.Ticks < 499147488000000000L || time.Ticks > 706783967999999999L)
			{
				throw new ArgumentOutOfRangeException("time", "Only hebrew years between 5343 and 6000, inclusive, are supported.");
			}
		}

		// Token: 0x06001AEE RID: 6894 RVA: 0x00064164 File Offset: 0x00062364
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = HebrewCalendar.HebrewEra;
			}
			if (era != HebrewCalendar.HebrewEra)
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001AEF RID: 6895 RVA: 0x0006418C File Offset: 0x0006238C
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			if (year < 5343 || year > this.M_MaxYear)
			{
				throw new ArgumentOutOfRangeException("year", "Only hebrew years between 5343 and 6000, inclusive, are supported.");
			}
		}

		// Token: 0x06001AF0 RID: 6896 RVA: 0x000641C8 File Offset: 0x000623C8
		internal void M_CheckYME(int year, int month, ref int era)
		{
			this.M_CheckYE(year, ref era);
			int num = CCHebrewCalendar.last_month_of_year(year);
			if (month < 1 || month > num)
			{
				StringWriter stringWriter = new StringWriter();
				stringWriter.Write("Month must be between 1 and {0}.", num);
				throw new ArgumentOutOfRangeException("month", stringWriter.ToString());
			}
		}

		// Token: 0x06001AF1 RID: 6897 RVA: 0x0006421C File Offset: 0x0006241C
		internal void M_CheckYMDE(int year, int month, int day, ref int era)
		{
			this.M_CheckYME(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, era));
		}

		// Token: 0x06001AF2 RID: 6898 RVA: 0x0006424C File Offset: 0x0006244C
		public override DateTime AddMonths(DateTime time, int months)
		{
			DateTime dateTime;
			if (months == 0)
			{
				dateTime = time;
			}
			else
			{
				int date = CCFixed.FromDateTime(time);
				int day;
				int num;
				int num2;
				CCHebrewCalendar.dmy_from_fixed(out day, out num, out num2, date);
				num = this.M_Month(num, num2);
				if (months < 0)
				{
					while (months < 0)
					{
						if (num + months > 0)
						{
							num += months;
							months = 0;
						}
						else
						{
							months += num;
							num2--;
							num = this.GetMonthsInYear(num2);
						}
					}
				}
				else
				{
					while (months > 0)
					{
						int monthsInYear = this.GetMonthsInYear(num2);
						if (num + months <= monthsInYear)
						{
							num += months;
							months = 0;
						}
						else
						{
							months -= monthsInYear - num + 1;
							num = 1;
							num2++;
						}
					}
				}
				dateTime = this.ToDateTime(num2, num, day, 0, 0, 0, 0).Add(time.TimeOfDay);
			}
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001AF3 RID: 6899 RVA: 0x00064324 File Offset: 0x00062524
		public override DateTime AddYears(DateTime time, int years)
		{
			int date = CCFixed.FromDateTime(time);
			int day;
			int month;
			int num;
			CCHebrewCalendar.dmy_from_fixed(out day, out month, out num, date);
			num += years;
			date = CCHebrewCalendar.fixed_from_dmy(day, month, num);
			DateTime dateTime = CCFixed.ToDateTime(date).Add(time.TimeOfDay);
			this.M_CheckDateTime(dateTime);
			return dateTime;
		}

		// Token: 0x06001AF4 RID: 6900 RVA: 0x00064374 File Offset: 0x00062574
		public override int GetDayOfMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			return CCHebrewCalendar.day_from_fixed(date);
		}

		// Token: 0x06001AF5 RID: 6901 RVA: 0x00064398 File Offset: 0x00062598
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			return CCFixed.day_of_week(date);
		}

		// Token: 0x06001AF6 RID: 6902 RVA: 0x000643BC File Offset: 0x000625BC
		public override int GetDayOfYear(DateTime time)
		{
			this.M_CheckDateTime(time);
			int num = CCFixed.FromDateTime(time);
			int year = CCHebrewCalendar.year_from_fixed(num);
			int num2 = CCHebrewCalendar.fixed_from_dmy(1, 7, year);
			return num - num2 + 1;
		}

		// Token: 0x06001AF7 RID: 6903 RVA: 0x000643EC File Offset: 0x000625EC
		internal int M_CCMonth(int month, int year)
		{
			if (month <= 6)
			{
				return 6 + month;
			}
			int num = CCHebrewCalendar.last_month_of_year(year);
			if (num == 12)
			{
				return month - 6;
			}
			return (month > 7) ? (month - 7) : (6 + month);
		}

		// Token: 0x06001AF8 RID: 6904 RVA: 0x0006442C File Offset: 0x0006262C
		internal int M_Month(int ccmonth, int year)
		{
			if (ccmonth >= 7)
			{
				return ccmonth - 6;
			}
			int num = CCHebrewCalendar.last_month_of_year(year);
			return ccmonth + ((num != 12) ? 7 : 6);
		}

		// Token: 0x06001AF9 RID: 6905 RVA: 0x0006445C File Offset: 0x0006265C
		public override int GetDaysInMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			int month2 = this.M_CCMonth(month, year);
			return CCHebrewCalendar.last_day_of_month(month2, year);
		}

		// Token: 0x06001AFA RID: 6906 RVA: 0x00064484 File Offset: 0x00062684
		public override int GetDaysInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			int num = CCHebrewCalendar.fixed_from_dmy(1, 7, year);
			int num2 = CCHebrewCalendar.fixed_from_dmy(1, 7, year + 1);
			return num2 - num;
		}

		// Token: 0x06001AFB RID: 6907 RVA: 0x000644B4 File Offset: 0x000626B4
		public override int GetEra(DateTime time)
		{
			this.M_CheckDateTime(time);
			return HebrewCalendar.HebrewEra;
		}

		// Token: 0x06001AFC RID: 6908 RVA: 0x000644C4 File Offset: 0x000626C4
		public override int GetLeapMonth(int year, int era)
		{
			return (!this.IsLeapMonth(year, 7, era)) ? 0 : 7;
		}

		// Token: 0x06001AFD RID: 6909 RVA: 0x000644DC File Offset: 0x000626DC
		public override int GetMonth(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			int ccmonth;
			int year;
			CCHebrewCalendar.my_from_fixed(out ccmonth, out year, date);
			return this.M_Month(ccmonth, year);
		}

		// Token: 0x06001AFE RID: 6910 RVA: 0x0006450C File Offset: 0x0006270C
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return CCHebrewCalendar.last_month_of_year(year);
		}

		// Token: 0x06001AFF RID: 6911 RVA: 0x00064520 File Offset: 0x00062720
		public override int GetYear(DateTime time)
		{
			this.M_CheckDateTime(time);
			int date = CCFixed.FromDateTime(time);
			return CCHebrewCalendar.year_from_fixed(date);
		}

		// Token: 0x06001B00 RID: 6912 RVA: 0x00064544 File Offset: 0x00062744
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			return this.IsLeapYear(year) && (month == 7 || (month == 6 && day == 30));
		}

		// Token: 0x06001B01 RID: 6913 RVA: 0x00064584 File Offset: 0x00062784
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			return this.IsLeapYear(year) && month == 7;
		}

		// Token: 0x06001B02 RID: 6914 RVA: 0x000645A4 File Offset: 0x000627A4
		public override bool IsLeapYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return CCHebrewCalendar.is_leap_year(year);
		}

		// Token: 0x06001B03 RID: 6915 RVA: 0x000645B8 File Offset: 0x000627B8
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			int month2 = this.M_CCMonth(month, year);
			int date = CCHebrewCalendar.fixed_from_dmy(day, month2, year);
			return CCFixed.ToDateTime(date, hour, minute, second, (double)millisecond);
		}

		// Token: 0x06001B04 RID: 6916 RVA: 0x00064600 File Offset: 0x00062800
		public override int ToFourDigitYear(int year)
		{
			base.M_ArgumentInRange("year", year, 0, this.M_MaxYear - 1);
			int num = this.twoDigitYearMax % 100;
			int num2 = this.twoDigitYearMax - num;
			if (year >= 100)
			{
				return year;
			}
			if (year <= num)
			{
				return num2 + year;
			}
			return num2 + year - 100;
		}

		// Token: 0x17000466 RID: 1126
		// (get) Token: 0x06001B05 RID: 6917 RVA: 0x00064650 File Offset: 0x00062850
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.LunisolarCalendar;
			}
		}

		// Token: 0x17000467 RID: 1127
		// (get) Token: 0x06001B06 RID: 6918 RVA: 0x00064654 File Offset: 0x00062854
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return HebrewCalendar.Min;
			}
		}

		// Token: 0x17000468 RID: 1128
		// (get) Token: 0x06001B07 RID: 6919 RVA: 0x0006465C File Offset: 0x0006285C
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return HebrewCalendar.Max;
			}
		}

		// Token: 0x04000A25 RID: 2597
		internal const long M_MinTicks = 499147488000000000L;

		// Token: 0x04000A26 RID: 2598
		internal const long M_MaxTicks = 706783967999999999L;

		// Token: 0x04000A27 RID: 2599
		internal const int M_MinYear = 5343;

		// Token: 0x04000A28 RID: 2600
		public static readonly int HebrewEra = 1;

		// Token: 0x04000A29 RID: 2601
		private static DateTime Min = new DateTime(1583, 1, 1, 0, 0, 0);

		// Token: 0x04000A2A RID: 2602
		private static DateTime Max = new DateTime(2239, 9, 29, 11, 59, 59);
	}
}
