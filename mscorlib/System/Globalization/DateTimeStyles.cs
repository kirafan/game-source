﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000212 RID: 530
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum DateTimeStyles
	{
		// Token: 0x04000A08 RID: 2568
		None = 0,
		// Token: 0x04000A09 RID: 2569
		AllowLeadingWhite = 1,
		// Token: 0x04000A0A RID: 2570
		AllowTrailingWhite = 2,
		// Token: 0x04000A0B RID: 2571
		AllowInnerWhite = 4,
		// Token: 0x04000A0C RID: 2572
		AllowWhiteSpaces = 7,
		// Token: 0x04000A0D RID: 2573
		NoCurrentDateDefault = 8,
		// Token: 0x04000A0E RID: 2574
		AdjustToUniversal = 16,
		// Token: 0x04000A0F RID: 2575
		AssumeLocal = 32,
		// Token: 0x04000A10 RID: 2576
		AssumeUniversal = 64,
		// Token: 0x04000A11 RID: 2577
		RoundtripKind = 128
	}
}
