﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x0200020D RID: 525
	[Flags]
	[ComVisible(true)]
	[Serializable]
	public enum CompareOptions
	{
		// Token: 0x0400098B RID: 2443
		None = 0,
		// Token: 0x0400098C RID: 2444
		IgnoreCase = 1,
		// Token: 0x0400098D RID: 2445
		IgnoreNonSpace = 2,
		// Token: 0x0400098E RID: 2446
		IgnoreSymbols = 4,
		// Token: 0x0400098F RID: 2447
		IgnoreKanaType = 8,
		// Token: 0x04000990 RID: 2448
		IgnoreWidth = 16,
		// Token: 0x04000991 RID: 2449
		StringSort = 536870912,
		// Token: 0x04000992 RID: 2450
		Ordinal = 1073741824,
		// Token: 0x04000993 RID: 2451
		OrdinalIgnoreCase = 268435456
	}
}
