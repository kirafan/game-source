﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000217 RID: 535
	[ComVisible(true)]
	[Serializable]
	public enum GregorianCalendarTypes
	{
		// Token: 0x04000A1F RID: 2591
		Localized = 1,
		// Token: 0x04000A20 RID: 2592
		USEnglish,
		// Token: 0x04000A21 RID: 2593
		MiddleEastFrench = 9,
		// Token: 0x04000A22 RID: 2594
		Arabic,
		// Token: 0x04000A23 RID: 2595
		TransliteratedEnglish,
		// Token: 0x04000A24 RID: 2596
		TransliteratedFrench
	}
}
