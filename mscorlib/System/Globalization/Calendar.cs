﻿using System;
using System.IO;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x020001F7 RID: 503
	[ComVisible(true)]
	[Serializable]
	public abstract class Calendar : ICloneable
	{
		// Token: 0x06001903 RID: 6403 RVA: 0x0005E1D4 File Offset: 0x0005C3D4
		protected Calendar()
		{
			this.twoDigitYearMax = 99;
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x06001904 RID: 6404 RVA: 0x0005E1E4 File Offset: 0x0005C3E4
		internal virtual int M_DaysInWeek
		{
			get
			{
				return 7;
			}
		}

		// Token: 0x06001905 RID: 6405 RVA: 0x0005E1E8 File Offset: 0x0005C3E8
		internal string M_ValidValues(object a, object b)
		{
			StringWriter stringWriter = new StringWriter();
			stringWriter.Write("Valid values are between {0} and {1}, inclusive.", a, b);
			return stringWriter.ToString();
		}

		// Token: 0x06001906 RID: 6406 RVA: 0x0005E210 File Offset: 0x0005C410
		internal void M_ArgumentInRange(string param, int arg, int a, int b)
		{
			if (a <= arg && arg <= b)
			{
				return;
			}
			throw new ArgumentOutOfRangeException(param, this.M_ValidValues(a, b));
		}

		// Token: 0x06001907 RID: 6407 RVA: 0x0005E23C File Offset: 0x0005C43C
		internal void M_CheckHMSM(int hour, int minute, int second, int milliseconds)
		{
			this.M_ArgumentInRange("hour", hour, 0, 23);
			this.M_ArgumentInRange("minute", minute, 0, 59);
			this.M_ArgumentInRange("second", second, 0, 59);
			this.M_ArgumentInRange("milliseconds", milliseconds, 0, 999999);
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x06001908 RID: 6408
		public abstract int[] Eras { get; }

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x06001909 RID: 6409 RVA: 0x0005E28C File Offset: 0x0005C48C
		[ComVisible(false)]
		public virtual CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.Unknown;
			}
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x0600190A RID: 6410 RVA: 0x0005E290 File Offset: 0x0005C490
		[ComVisible(false)]
		public virtual DateTime MaxSupportedDateTime
		{
			get
			{
				return DateTime.MaxValue;
			}
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x0600190B RID: 6411 RVA: 0x0005E298 File Offset: 0x0005C498
		[ComVisible(false)]
		public virtual DateTime MinSupportedDateTime
		{
			get
			{
				return DateTime.MinValue;
			}
		}

		// Token: 0x0600190C RID: 6412 RVA: 0x0005E2A0 File Offset: 0x0005C4A0
		[ComVisible(false)]
		public virtual object Clone()
		{
			Calendar calendar = (Calendar)base.MemberwiseClone();
			calendar.m_isReadOnly = false;
			return calendar;
		}

		// Token: 0x0600190D RID: 6413 RVA: 0x0005E2C4 File Offset: 0x0005C4C4
		[ComVisible(false)]
		public virtual int GetLeapMonth(int year)
		{
			return this.GetLeapMonth(year, this.GetEra(this.ToDateTime(year, 1, 1, 0, 0, 0, 0)));
		}

		// Token: 0x0600190E RID: 6414 RVA: 0x0005E2EC File Offset: 0x0005C4EC
		[ComVisible(false)]
		public virtual int GetLeapMonth(int year, int era)
		{
			int monthsInYear = this.GetMonthsInYear(year, era);
			for (int i = 1; i <= monthsInYear; i++)
			{
				if (this.IsLeapMonth(year, i, era))
				{
					return i;
				}
			}
			return 0;
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x0600190F RID: 6415 RVA: 0x0005E328 File Offset: 0x0005C528
		[ComVisible(false)]
		public bool IsReadOnly
		{
			get
			{
				return this.m_isReadOnly;
			}
		}

		// Token: 0x06001910 RID: 6416 RVA: 0x0005E330 File Offset: 0x0005C530
		[ComVisible(false)]
		public static Calendar ReadOnly(Calendar calendar)
		{
			if (calendar.m_isReadOnly)
			{
				return calendar;
			}
			Calendar calendar2 = (Calendar)calendar.Clone();
			calendar2.m_isReadOnly = true;
			return calendar2;
		}

		// Token: 0x06001911 RID: 6417 RVA: 0x0005E360 File Offset: 0x0005C560
		internal void CheckReadOnly()
		{
			if (this.m_isReadOnly)
			{
				throw new InvalidOperationException("This Calendar is read-only.");
			}
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x06001912 RID: 6418 RVA: 0x0005E378 File Offset: 0x0005C578
		internal virtual int M_MaxYear
		{
			get
			{
				if (this.M_MaxYearValue == 0)
				{
					this.M_MaxYearValue = this.GetYear(DateTime.MaxValue);
				}
				return this.M_MaxYearValue;
			}
		}

		// Token: 0x06001913 RID: 6419 RVA: 0x0005E3A8 File Offset: 0x0005C5A8
		internal virtual void M_CheckYE(int year, ref int era)
		{
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x06001914 RID: 6420 RVA: 0x0005E3AC File Offset: 0x0005C5AC
		// (set) Token: 0x06001915 RID: 6421 RVA: 0x0005E3B4 File Offset: 0x0005C5B4
		public virtual int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				this.CheckReadOnly();
				this.M_ArgumentInRange("year", value, 100, this.M_MaxYear);
				int num = 0;
				this.M_CheckYE(value, ref num);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x06001916 RID: 6422 RVA: 0x0005E3F0 File Offset: 0x0005C5F0
		public virtual DateTime AddDays(DateTime time, int days)
		{
			return time.Add(TimeSpan.FromDays((double)days));
		}

		// Token: 0x06001917 RID: 6423 RVA: 0x0005E400 File Offset: 0x0005C600
		public virtual DateTime AddHours(DateTime time, int hours)
		{
			return time.Add(TimeSpan.FromHours((double)hours));
		}

		// Token: 0x06001918 RID: 6424 RVA: 0x0005E410 File Offset: 0x0005C610
		public virtual DateTime AddMilliseconds(DateTime time, double milliseconds)
		{
			return time.Add(TimeSpan.FromMilliseconds(milliseconds));
		}

		// Token: 0x06001919 RID: 6425 RVA: 0x0005E420 File Offset: 0x0005C620
		public virtual DateTime AddMinutes(DateTime time, int minutes)
		{
			return time.Add(TimeSpan.FromMinutes((double)minutes));
		}

		// Token: 0x0600191A RID: 6426
		public abstract DateTime AddMonths(DateTime time, int months);

		// Token: 0x0600191B RID: 6427 RVA: 0x0005E430 File Offset: 0x0005C630
		public virtual DateTime AddSeconds(DateTime time, int seconds)
		{
			return time.Add(TimeSpan.FromSeconds((double)seconds));
		}

		// Token: 0x0600191C RID: 6428 RVA: 0x0005E440 File Offset: 0x0005C640
		public virtual DateTime AddWeeks(DateTime time, int weeks)
		{
			return time.AddDays((double)(weeks * this.M_DaysInWeek));
		}

		// Token: 0x0600191D RID: 6429
		public abstract DateTime AddYears(DateTime time, int years);

		// Token: 0x0600191E RID: 6430
		public abstract int GetDayOfMonth(DateTime time);

		// Token: 0x0600191F RID: 6431
		public abstract DayOfWeek GetDayOfWeek(DateTime time);

		// Token: 0x06001920 RID: 6432
		public abstract int GetDayOfYear(DateTime time);

		// Token: 0x06001921 RID: 6433 RVA: 0x0005E454 File Offset: 0x0005C654
		public virtual int GetDaysInMonth(int year, int month)
		{
			return this.GetDaysInMonth(year, month, 0);
		}

		// Token: 0x06001922 RID: 6434
		public abstract int GetDaysInMonth(int year, int month, int era);

		// Token: 0x06001923 RID: 6435 RVA: 0x0005E460 File Offset: 0x0005C660
		public virtual int GetDaysInYear(int year)
		{
			return this.GetDaysInYear(year, 0);
		}

		// Token: 0x06001924 RID: 6436
		public abstract int GetDaysInYear(int year, int era);

		// Token: 0x06001925 RID: 6437
		public abstract int GetEra(DateTime time);

		// Token: 0x06001926 RID: 6438 RVA: 0x0005E46C File Offset: 0x0005C66C
		public virtual int GetHour(DateTime time)
		{
			return time.TimeOfDay.Hours;
		}

		// Token: 0x06001927 RID: 6439 RVA: 0x0005E488 File Offset: 0x0005C688
		public virtual double GetMilliseconds(DateTime time)
		{
			return (double)time.TimeOfDay.Milliseconds;
		}

		// Token: 0x06001928 RID: 6440 RVA: 0x0005E4A8 File Offset: 0x0005C6A8
		public virtual int GetMinute(DateTime time)
		{
			return time.TimeOfDay.Minutes;
		}

		// Token: 0x06001929 RID: 6441
		public abstract int GetMonth(DateTime time);

		// Token: 0x0600192A RID: 6442 RVA: 0x0005E4C4 File Offset: 0x0005C6C4
		public virtual int GetMonthsInYear(int year)
		{
			return this.GetMonthsInYear(year, 0);
		}

		// Token: 0x0600192B RID: 6443
		public abstract int GetMonthsInYear(int year, int era);

		// Token: 0x0600192C RID: 6444 RVA: 0x0005E4D0 File Offset: 0x0005C6D0
		public virtual int GetSecond(DateTime time)
		{
			return time.TimeOfDay.Seconds;
		}

		// Token: 0x0600192D RID: 6445 RVA: 0x0005E4EC File Offset: 0x0005C6EC
		internal int M_DiffDays(DateTime timeA, DateTime timeB)
		{
			long num = timeA.Ticks - timeB.Ticks;
			if (num >= 0L)
			{
				return (int)(num / 864000000000L);
			}
			num += 1L;
			return -1 + (int)(num / 864000000000L);
		}

		// Token: 0x0600192E RID: 6446 RVA: 0x0005E534 File Offset: 0x0005C734
		internal DateTime M_GetFirstDayOfSecondWeekOfYear(int year, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
		{
			DateTime time = this.ToDateTime(year, 1, 1, 0, 0, 0, 0);
			int dayOfWeek = (int)this.GetDayOfWeek(time);
			int num = 0;
			switch (rule)
			{
			case CalendarWeekRule.FirstDay:
				if (firstDayOfWeek > (DayOfWeek)dayOfWeek)
				{
					num += firstDayOfWeek - (DayOfWeek)dayOfWeek;
				}
				else
				{
					num += firstDayOfWeek + this.M_DaysInWeek - (DayOfWeek)dayOfWeek;
				}
				break;
			case CalendarWeekRule.FirstFullWeek:
				num = this.M_DaysInWeek;
				if (firstDayOfWeek >= (DayOfWeek)dayOfWeek)
				{
					num += firstDayOfWeek - (DayOfWeek)dayOfWeek;
				}
				else
				{
					num += firstDayOfWeek + this.M_DaysInWeek - (DayOfWeek)dayOfWeek;
				}
				break;
			case CalendarWeekRule.FirstFourDayWeek:
			{
				int num2 = (dayOfWeek + 3) % this.M_DaysInWeek;
				num = 3;
				if (firstDayOfWeek > (DayOfWeek)num2)
				{
					num += firstDayOfWeek - (DayOfWeek)num2;
				}
				else
				{
					num += firstDayOfWeek + this.M_DaysInWeek - (DayOfWeek)num2;
				}
				break;
			}
			}
			return this.AddDays(time, num);
		}

		// Token: 0x0600192F RID: 6447 RVA: 0x0005E604 File Offset: 0x0005C804
		public virtual int GetWeekOfYear(DateTime time, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
		{
			if (firstDayOfWeek < DayOfWeek.Sunday || DayOfWeek.Saturday < firstDayOfWeek)
			{
				throw new ArgumentOutOfRangeException("firstDayOfWeek", "Value is not a valid day of week.");
			}
			int num = this.GetYear(time);
			int num2;
			for (;;)
			{
				DateTime timeB = this.M_GetFirstDayOfSecondWeekOfYear(num, rule, firstDayOfWeek);
				num2 = this.M_DiffDays(time, timeB) + this.M_DaysInWeek;
				if (num2 >= 0)
				{
					break;
				}
				num--;
			}
			return 1 + num2 / this.M_DaysInWeek;
		}

		// Token: 0x06001930 RID: 6448
		public abstract int GetYear(DateTime time);

		// Token: 0x06001931 RID: 6449 RVA: 0x0005E670 File Offset: 0x0005C870
		public virtual bool IsLeapDay(int year, int month, int day)
		{
			return this.IsLeapDay(year, month, day, 0);
		}

		// Token: 0x06001932 RID: 6450
		public abstract bool IsLeapDay(int year, int month, int day, int era);

		// Token: 0x06001933 RID: 6451 RVA: 0x0005E67C File Offset: 0x0005C87C
		public virtual bool IsLeapMonth(int year, int month)
		{
			return this.IsLeapMonth(year, month, 0);
		}

		// Token: 0x06001934 RID: 6452
		public abstract bool IsLeapMonth(int year, int month, int era);

		// Token: 0x06001935 RID: 6453 RVA: 0x0005E688 File Offset: 0x0005C888
		public virtual bool IsLeapYear(int year)
		{
			return this.IsLeapYear(year, 0);
		}

		// Token: 0x06001936 RID: 6454
		public abstract bool IsLeapYear(int year, int era);

		// Token: 0x06001937 RID: 6455 RVA: 0x0005E694 File Offset: 0x0005C894
		public virtual DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond)
		{
			return this.ToDateTime(year, month, day, hour, minute, second, millisecond, 0);
		}

		// Token: 0x06001938 RID: 6456
		public abstract DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era);

		// Token: 0x06001939 RID: 6457 RVA: 0x0005E6B4 File Offset: 0x0005C8B4
		public virtual int ToFourDigitYear(int year)
		{
			if (year < 0)
			{
				throw new ArgumentOutOfRangeException("year", "Non-negative number required.");
			}
			if (year <= 99)
			{
				int num = this.TwoDigitYearMax % 100;
				int num2 = year - num;
				year = this.TwoDigitYearMax + num2 + ((num2 > 0) ? -100 : 0);
			}
			int num3 = 0;
			this.M_CheckYE(year, ref num3);
			return year;
		}

		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x0600193A RID: 6458 RVA: 0x0005E714 File Offset: 0x0005C914
		// (set) Token: 0x0600193B RID: 6459 RVA: 0x0005E754 File Offset: 0x0005C954
		internal string[] AbbreviatedEraNames
		{
			get
			{
				if (this.M_AbbrEraNames == null || this.M_AbbrEraNames.Length != this.Eras.Length)
				{
					throw new Exception("Internal: M_AbbrEraNames wrong initialized!");
				}
				return (string[])this.M_AbbrEraNames.Clone();
			}
			set
			{
				this.CheckReadOnly();
				if (value.Length != this.Eras.Length)
				{
					StringWriter stringWriter = new StringWriter();
					stringWriter.Write("Array length must be equal Eras length {0}.", this.Eras.Length);
					throw new ArgumentException(stringWriter.ToString());
				}
				this.M_AbbrEraNames = (string[])value.Clone();
			}
		}

		// Token: 0x17000410 RID: 1040
		// (get) Token: 0x0600193C RID: 6460 RVA: 0x0005E7B4 File Offset: 0x0005C9B4
		// (set) Token: 0x0600193D RID: 6461 RVA: 0x0005E7F4 File Offset: 0x0005C9F4
		internal string[] EraNames
		{
			get
			{
				if (this.M_EraNames == null || this.M_EraNames.Length != this.Eras.Length)
				{
					throw new Exception("Internal: M_EraNames not initialized!");
				}
				return (string[])this.M_EraNames.Clone();
			}
			set
			{
				this.CheckReadOnly();
				if (value.Length != this.Eras.Length)
				{
					StringWriter stringWriter = new StringWriter();
					stringWriter.Write("Array length must be equal Eras length {0}.", this.Eras.Length);
					throw new ArgumentException(stringWriter.ToString());
				}
				this.M_EraNames = (string[])value.Clone();
			}
		}

		// Token: 0x0400091D RID: 2333
		public const int CurrentEra = 0;

		// Token: 0x0400091E RID: 2334
		[NonSerialized]
		private bool m_isReadOnly;

		// Token: 0x0400091F RID: 2335
		[NonSerialized]
		internal int twoDigitYearMax;

		// Token: 0x04000920 RID: 2336
		[NonSerialized]
		private int M_MaxYearValue;

		// Token: 0x04000921 RID: 2337
		[NonSerialized]
		internal string[] M_AbbrEraNames;

		// Token: 0x04000922 RID: 2338
		[NonSerialized]
		internal string[] M_EraNames;

		// Token: 0x04000923 RID: 2339
		internal int m_currentEraValue;
	}
}
