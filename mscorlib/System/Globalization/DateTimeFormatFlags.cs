﻿using System;

namespace System.Globalization
{
	// Token: 0x02000210 RID: 528
	[Flags]
	internal enum DateTimeFormatFlags
	{
		// Token: 0x040009C8 RID: 2504
		Unused = 0,
		// Token: 0x040009C9 RID: 2505
		But = 1,
		// Token: 0x040009CA RID: 2506
		Serialized = 2,
		// Token: 0x040009CB RID: 2507
		By = 3,
		// Token: 0x040009CC RID: 2508
		Microsoft = 4
	}
}
