﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x020001F8 RID: 504
	[ComVisible(true)]
	public enum CalendarAlgorithmType
	{
		// Token: 0x04000925 RID: 2341
		Unknown,
		// Token: 0x04000926 RID: 2342
		SolarCalendar,
		// Token: 0x04000927 RID: 2343
		LunarCalendar,
		// Token: 0x04000928 RID: 2344
		LunisolarCalendar
	}
}
