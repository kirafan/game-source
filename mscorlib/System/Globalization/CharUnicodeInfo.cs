﻿using System;

namespace System.Globalization
{
	// Token: 0x02000209 RID: 521
	public sealed class CharUnicodeInfo
	{
		// Token: 0x060019BA RID: 6586 RVA: 0x0005FD68 File Offset: 0x0005DF68
		private CharUnicodeInfo()
		{
		}

		// Token: 0x060019BB RID: 6587 RVA: 0x0005FD70 File Offset: 0x0005DF70
		public static int GetDecimalDigitValue(char ch)
		{
			if (ch == '²')
			{
				return 2;
			}
			if (ch == '³')
			{
				return 3;
			}
			if (ch == '¹')
			{
				return 1;
			}
			if (ch == '⁰')
			{
				return 0;
			}
			if ('⁴' <= ch && ch < '⁺')
			{
				return (int)(ch - '⁰');
			}
			if ('₀' <= ch && ch < '₊')
			{
				return (int)(ch - '₀');
			}
			if (!char.IsDigit(ch))
			{
				return -1;
			}
			if (ch < ':')
			{
				return (int)(ch - '0');
			}
			if (ch < '٪')
			{
				return (int)(ch - '٠');
			}
			if (ch < 'ۺ')
			{
				return (int)(ch - '۰');
			}
			if (ch < '॰')
			{
				return (int)(ch - '०');
			}
			if (ch < 'ৰ')
			{
				return (int)(ch - '০');
			}
			if (ch < 'ੰ')
			{
				return (int)(ch - '੦');
			}
			if (ch < '૰')
			{
				return (int)(ch - '૦');
			}
			if (ch < '୰')
			{
				return (int)(ch - '୦');
			}
			if (ch < '௰')
			{
				return (int)(ch - '௦');
			}
			if (ch < '౰')
			{
				return (int)(ch - '౦');
			}
			if (ch < '೰')
			{
				return (int)(ch - '೦');
			}
			if (ch < '൰')
			{
				return (int)(ch - '൦');
			}
			if (ch < '๚')
			{
				return (int)(ch - '๐');
			}
			if (ch < '໚')
			{
				return (int)(ch - '໐');
			}
			if (ch < '༪')
			{
				return (int)(ch - '༠');
			}
			if (ch < '၊')
			{
				return (int)(ch - '၀');
			}
			if (ch < '፲')
			{
				return (int)(ch - '፨');
			}
			if (ch < '៪')
			{
				return (int)(ch - '០');
			}
			if (ch < '᠚')
			{
				return (int)(ch - '᠐');
			}
			if (ch < '⁺')
			{
				return (int)(ch - '⁰');
			}
			if (ch < '₊')
			{
				return (int)(ch - '₀');
			}
			if (ch < '０')
			{
				return -1;
			}
			if (ch < '：')
			{
				return (int)(ch - '０');
			}
			return -1;
		}

		// Token: 0x060019BC RID: 6588 RVA: 0x0005FFB0 File Offset: 0x0005E1B0
		public static int GetDecimalDigitValue(string s, int index)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			return CharUnicodeInfo.GetDecimalDigitValue(s[index]);
		}

		// Token: 0x060019BD RID: 6589 RVA: 0x0005FFD0 File Offset: 0x0005E1D0
		public static int GetDigitValue(char ch)
		{
			int decimalDigitValue = CharUnicodeInfo.GetDecimalDigitValue(ch);
			if (decimalDigitValue >= 0)
			{
				return decimalDigitValue;
			}
			if (ch == '⓪')
			{
				return 0;
			}
			if (ch >= '①' && ch < '⑩')
			{
				return (int)(ch - '⑟');
			}
			if (ch >= '⑴' && ch < '⑽')
			{
				return (int)(ch - '⑳');
			}
			if (ch >= '⒈' && ch < '⒑')
			{
				return (int)(ch - '⒇');
			}
			if (ch >= '⓵' && ch < '⓾')
			{
				return (int)(ch - '⓴');
			}
			if (ch >= '❶' && ch < '❿')
			{
				return (int)(ch - '❵');
			}
			if (ch >= '➀' && ch < '➉')
			{
				return (int)(ch - '❿');
			}
			if (ch >= '➊' && ch < '➓')
			{
				return (int)(ch - '➉');
			}
			return -1;
		}

		// Token: 0x060019BE RID: 6590 RVA: 0x000600D0 File Offset: 0x0005E2D0
		public static int GetDigitValue(string s, int index)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			return CharUnicodeInfo.GetDigitValue(s[index]);
		}

		// Token: 0x060019BF RID: 6591 RVA: 0x000600F0 File Offset: 0x0005E2F0
		public static double GetNumericValue(char ch)
		{
			int digitValue = CharUnicodeInfo.GetDigitValue(ch);
			if (digitValue >= 0)
			{
				return (double)digitValue;
			}
			switch (ch)
			{
			case '⅓':
				return 0.3333333333333333;
			case '⅔':
				return 0.6666666666666666;
			default:
				switch (ch)
				{
				case 'ⅼ':
					return 50.0;
				case 'ⅽ':
					return 100.0;
				case 'ⅾ':
					return 500.0;
				case 'ⅿ':
					return 1000.0;
				case 'ↀ':
					return 1000.0;
				case 'ↁ':
					return 5000.0;
				case 'ↂ':
					return 10000.0;
				default:
					switch (ch)
					{
					case '৴':
						return 1.0;
					case '৵':
						return 2.0;
					case '৶':
						return 3.0;
					case '৷':
						return 4.0;
					default:
						switch (ch)
						{
						case 'Ⅼ':
							return 50.0;
						case 'Ⅽ':
							return 100.0;
						case 'Ⅾ':
							return 500.0;
						case 'Ⅿ':
							return 1000.0;
						default:
							switch (ch)
							{
							case '¼':
								return 0.25;
							case '½':
								return 0.5;
							case '¾':
								return 0.75;
							default:
								switch (ch)
								{
								case '௰':
									return 10.0;
								case '௱':
									return 100.0;
								case '௲':
									return 1000.0;
								default:
									switch (ch)
									{
									case 'ᛮ':
										return 17.0;
									case 'ᛯ':
										return 18.0;
									case 'ᛰ':
										return 19.0;
									default:
										switch (ch)
										{
										case '〸':
											return 10.0;
										case '〹':
											return 20.0;
										case '〺':
											return 30.0;
										default:
											if (ch == '፼')
											{
												return 10000.0;
											}
											if (ch == '⓾' || ch == '❿' || ch == '➉' || ch == '➓')
											{
												return 10.0;
											}
											if (ch == '〇')
											{
												return 0.0;
											}
											if ('⓫' <= ch && ch < '⓵')
											{
												return (double)(ch - 'ⓠ');
											}
											if ('〡' <= ch && ch < '〪')
											{
												return (double)(ch - '〠');
											}
											if ('㉑' <= ch && ch < '㉠')
											{
												return (double)(ch - '㈼');
											}
											if ('㊱' <= ch && ch < '㋀')
											{
												return (double)(ch - '㊍');
											}
											if (!char.IsNumber(ch))
											{
												return -1.0;
											}
											if (ch < '༳')
											{
												return 0.5 + (double)ch - 3882.0;
											}
											if (ch < '፼')
											{
												return (double)((ch - '፱') * '\n');
											}
											if (ch < '⅙')
											{
												return 0.2 * (double)(ch - '⅔');
											}
											if (ch < 'Ⅼ')
											{
												return (double)(ch - '⅟');
											}
											if (ch < 'ⅼ')
											{
												return (double)(ch - 'Ⅿ');
											}
											if (ch < '⑴')
											{
												return (double)(ch - '⑟');
											}
											if (ch < '⒈')
											{
												return (double)(ch - '⑳');
											}
											if (ch < '⒜')
											{
												return (double)(ch - '⒇');
											}
											if (ch < '㆖')
											{
												return (double)(ch - '㆑');
											}
											if (ch < '㈪')
											{
												return (double)(ch - '㈟');
											}
											if (ch < '㊊')
											{
												return (double)(ch - '㉿');
											}
											return -1.0;
										}
										break;
									}
									break;
								}
								break;
							}
							break;
						}
						break;
					case '৹':
						return 16.0;
					}
					break;
				}
				break;
			case '⅙':
				return 0.16666666666666666;
			case '⅚':
				return 0.8333333333333334;
			case '⅛':
				return 0.125;
			case '⅜':
				return 0.375;
			case '⅝':
				return 0.625;
			case '⅞':
				return 0.875;
			case '⅟':
				return 1.0;
			}
		}

		// Token: 0x060019C0 RID: 6592 RVA: 0x00060584 File Offset: 0x0005E784
		public static double GetNumericValue(string s, int index)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			return CharUnicodeInfo.GetNumericValue(s[index]);
		}

		// Token: 0x060019C1 RID: 6593 RVA: 0x000605A4 File Offset: 0x0005E7A4
		public static UnicodeCategory GetUnicodeCategory(char ch)
		{
			return char.GetUnicodeCategory(ch);
		}

		// Token: 0x060019C2 RID: 6594 RVA: 0x000605AC File Offset: 0x0005E7AC
		public static UnicodeCategory GetUnicodeCategory(string s, int index)
		{
			if (s == null)
			{
				throw new ArgumentNullException("s");
			}
			return char.GetUnicodeCategory(s, index);
		}
	}
}
