﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000229 RID: 553
	[ComVisible(true)]
	[Serializable]
	public class TextElementEnumerator : IEnumerator
	{
		// Token: 0x06001C67 RID: 7271 RVA: 0x00068B94 File Offset: 0x00066D94
		internal TextElementEnumerator(string str, int startpos)
		{
			this.index = -1;
			this.startpos = startpos;
			this.str = str.Substring(startpos);
			this.element = null;
		}

		// Token: 0x170004C6 RID: 1222
		// (get) Token: 0x06001C68 RID: 7272 RVA: 0x00068BCC File Offset: 0x00066DCC
		public object Current
		{
			get
			{
				if (this.element == null)
				{
					throw new InvalidOperationException();
				}
				return this.element;
			}
		}

		// Token: 0x170004C7 RID: 1223
		// (get) Token: 0x06001C69 RID: 7273 RVA: 0x00068BE8 File Offset: 0x00066DE8
		public int ElementIndex
		{
			get
			{
				if (this.element == null)
				{
					throw new InvalidOperationException();
				}
				return this.elementindex + this.startpos;
			}
		}

		// Token: 0x06001C6A RID: 7274 RVA: 0x00068C08 File Offset: 0x00066E08
		public string GetTextElement()
		{
			if (this.element == null)
			{
				throw new InvalidOperationException();
			}
			return this.element;
		}

		// Token: 0x06001C6B RID: 7275 RVA: 0x00068C24 File Offset: 0x00066E24
		public bool MoveNext()
		{
			this.elementindex = this.index + 1;
			if (this.elementindex < this.str.Length)
			{
				this.element = StringInfo.GetNextTextElement(this.str, this.elementindex);
				this.index += this.element.Length;
				return true;
			}
			this.element = null;
			return false;
		}

		// Token: 0x06001C6C RID: 7276 RVA: 0x00068C90 File Offset: 0x00066E90
		public void Reset()
		{
			this.element = null;
			this.index = -1;
		}

		// Token: 0x04000AA1 RID: 2721
		private int index;

		// Token: 0x04000AA2 RID: 2722
		private int elementindex;

		// Token: 0x04000AA3 RID: 2723
		private int startpos;

		// Token: 0x04000AA4 RID: 2724
		private string str;

		// Token: 0x04000AA5 RID: 2725
		private string element;
	}
}
