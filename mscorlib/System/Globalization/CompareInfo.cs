﻿using System;
using System.Collections;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using Mono.Globalization.Unicode;

namespace System.Globalization
{
	// Token: 0x0200020C RID: 524
	[ComVisible(true)]
	[Serializable]
	public class CompareInfo : IDeserializationCallback
	{
		// Token: 0x060019CA RID: 6602 RVA: 0x0006068C File Offset: 0x0005E88C
		private CompareInfo()
		{
		}

		// Token: 0x060019CB RID: 6603 RVA: 0x00060694 File Offset: 0x0005E894
		internal CompareInfo(CultureInfo ci)
		{
			this.culture = ci.LCID;
			if (CompareInfo.UseManagedCollation)
			{
				object obj = CompareInfo.monitor;
				lock (obj)
				{
					if (CompareInfo.collators == null)
					{
						CompareInfo.collators = new Hashtable();
					}
					this.collator = (SimpleCollator)CompareInfo.collators[ci.LCID];
					if (this.collator == null)
					{
						this.collator = new SimpleCollator(ci);
						CompareInfo.collators[ci.LCID] = this.collator;
					}
				}
			}
			else
			{
				this.icu_name = ci.IcuName;
				this.construct_compareinfo(this.icu_name);
			}
		}

		// Token: 0x060019CD RID: 6605 RVA: 0x000607B4 File Offset: 0x0005E9B4
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			if (CompareInfo.UseManagedCollation)
			{
				this.collator = new SimpleCollator(new CultureInfo(this.culture));
			}
			else
			{
				try
				{
					this.construct_compareinfo(this.icu_name);
				}
				catch
				{
				}
			}
		}

		// Token: 0x17000418 RID: 1048
		// (get) Token: 0x060019CE RID: 6606 RVA: 0x0006081C File Offset: 0x0005EA1C
		internal static bool UseManagedCollation
		{
			get
			{
				return CompareInfo.useManagedCollation;
			}
		}

		// Token: 0x060019CF RID: 6607
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void construct_compareinfo(string locale);

		// Token: 0x060019D0 RID: 6608
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void free_internal_collator();

		// Token: 0x060019D1 RID: 6609
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int internal_compare(string str1, int offset1, int length1, string str2, int offset2, int length2, CompareOptions options);

		// Token: 0x060019D2 RID: 6610
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void assign_sortkey(object key, string source, CompareOptions options);

		// Token: 0x060019D3 RID: 6611
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int internal_index(string source, int sindex, int count, char value, CompareOptions options, bool first);

		// Token: 0x060019D4 RID: 6612
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int internal_index(string source, int sindex, int count, string value, CompareOptions options, bool first);

		// Token: 0x060019D5 RID: 6613 RVA: 0x00060824 File Offset: 0x0005EA24
		~CompareInfo()
		{
			this.free_internal_collator();
		}

		// Token: 0x060019D6 RID: 6614 RVA: 0x00060860 File Offset: 0x0005EA60
		private int internal_compare_managed(string str1, int offset1, int length1, string str2, int offset2, int length2, CompareOptions options)
		{
			return this.collator.Compare(str1, offset1, length1, str2, offset2, length2, options);
		}

		// Token: 0x060019D7 RID: 6615 RVA: 0x00060884 File Offset: 0x0005EA84
		private int internal_compare_switch(string str1, int offset1, int length1, string str2, int offset2, int length2, CompareOptions options)
		{
			return (!CompareInfo.UseManagedCollation) ? this.internal_compare(str1, offset1, length1, str2, offset2, length2, options) : this.internal_compare_managed(str1, offset1, length1, str2, offset2, length2, options);
		}

		// Token: 0x060019D8 RID: 6616 RVA: 0x000608C4 File Offset: 0x0005EAC4
		public virtual int Compare(string string1, string string2)
		{
			return this.Compare(string1, string2, CompareOptions.None);
		}

		// Token: 0x060019D9 RID: 6617 RVA: 0x000608D0 File Offset: 0x0005EAD0
		public virtual int Compare(string string1, string string2, CompareOptions options)
		{
			if ((options & (CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreSymbols | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.StringSort | CompareOptions.Ordinal | CompareOptions.OrdinalIgnoreCase)) != options)
			{
				throw new ArgumentException("options");
			}
			if (string1 == null)
			{
				if (string2 == null)
				{
					return 0;
				}
				return -1;
			}
			else
			{
				if (string2 == null)
				{
					return 1;
				}
				if (string1.Length == 0 && string2.Length == 0)
				{
					return 0;
				}
				return this.internal_compare_switch(string1, 0, string1.Length, string2, 0, string2.Length, options);
			}
		}

		// Token: 0x060019DA RID: 6618 RVA: 0x0006093C File Offset: 0x0005EB3C
		public virtual int Compare(string string1, int offset1, string string2, int offset2)
		{
			return this.Compare(string1, offset1, string2, offset2, CompareOptions.None);
		}

		// Token: 0x060019DB RID: 6619 RVA: 0x0006094C File Offset: 0x0005EB4C
		public virtual int Compare(string string1, int offset1, string string2, int offset2, CompareOptions options)
		{
			if ((options & (CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreSymbols | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.StringSort | CompareOptions.Ordinal | CompareOptions.OrdinalIgnoreCase)) != options)
			{
				throw new ArgumentException("options");
			}
			if (string1 == null)
			{
				if (string2 == null)
				{
					return 0;
				}
				return -1;
			}
			else
			{
				if (string2 == null)
				{
					return 1;
				}
				if ((string1.Length == 0 || offset1 == string1.Length) && (string2.Length == 0 || offset2 == string2.Length))
				{
					return 0;
				}
				if (offset1 < 0 || offset2 < 0)
				{
					throw new ArgumentOutOfRangeException("Offsets must not be less than zero");
				}
				if (offset1 > string1.Length)
				{
					throw new ArgumentOutOfRangeException("Offset1 is greater than or equal to the length of string1");
				}
				if (offset2 > string2.Length)
				{
					throw new ArgumentOutOfRangeException("Offset2 is greater than or equal to the length of string2");
				}
				return this.internal_compare_switch(string1, offset1, string1.Length - offset1, string2, offset2, string2.Length - offset2, options);
			}
		}

		// Token: 0x060019DC RID: 6620 RVA: 0x00060A24 File Offset: 0x0005EC24
		public virtual int Compare(string string1, int offset1, int length1, string string2, int offset2, int length2)
		{
			return this.Compare(string1, offset1, length1, string2, offset2, length2, CompareOptions.None);
		}

		// Token: 0x060019DD RID: 6621 RVA: 0x00060A44 File Offset: 0x0005EC44
		public virtual int Compare(string string1, int offset1, int length1, string string2, int offset2, int length2, CompareOptions options)
		{
			if ((options & (CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreSymbols | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.StringSort | CompareOptions.Ordinal | CompareOptions.OrdinalIgnoreCase)) != options)
			{
				throw new ArgumentException("options");
			}
			if (string1 == null)
			{
				if (string2 == null)
				{
					return 0;
				}
				return -1;
			}
			else
			{
				if (string2 == null)
				{
					return 1;
				}
				if ((string1.Length == 0 || offset1 == string1.Length || length1 == 0) && (string2.Length == 0 || offset2 == string2.Length || length2 == 0))
				{
					return 0;
				}
				if (offset1 < 0 || length1 < 0 || offset2 < 0 || length2 < 0)
				{
					throw new ArgumentOutOfRangeException("Offsets and lengths must not be less than zero");
				}
				if (offset1 > string1.Length)
				{
					throw new ArgumentOutOfRangeException("Offset1 is greater than or equal to the length of string1");
				}
				if (offset2 > string2.Length)
				{
					throw new ArgumentOutOfRangeException("Offset2 is greater than or equal to the length of string2");
				}
				if (length1 > string1.Length - offset1)
				{
					throw new ArgumentOutOfRangeException("Length1 is greater than the number of characters from offset1 to the end of string1");
				}
				if (length2 > string2.Length - offset2)
				{
					throw new ArgumentOutOfRangeException("Length2 is greater than the number of characters from offset2 to the end of string2");
				}
				return this.internal_compare_switch(string1, offset1, length1, string2, offset2, length2, options);
			}
		}

		// Token: 0x060019DE RID: 6622 RVA: 0x00060B64 File Offset: 0x0005ED64
		public override bool Equals(object value)
		{
			CompareInfo compareInfo = value as CompareInfo;
			return compareInfo != null && compareInfo.culture == this.culture;
		}

		// Token: 0x060019DF RID: 6623 RVA: 0x00060B90 File Offset: 0x0005ED90
		public static CompareInfo GetCompareInfo(int culture)
		{
			return new CultureInfo(culture).CompareInfo;
		}

		// Token: 0x060019E0 RID: 6624 RVA: 0x00060BA0 File Offset: 0x0005EDA0
		public static CompareInfo GetCompareInfo(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			return new CultureInfo(name).CompareInfo;
		}

		// Token: 0x060019E1 RID: 6625 RVA: 0x00060BC0 File Offset: 0x0005EDC0
		public static CompareInfo GetCompareInfo(int culture, Assembly assembly)
		{
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			if (assembly != typeof(object).Module.Assembly)
			{
				throw new ArgumentException("Assembly is an invalid type");
			}
			return CompareInfo.GetCompareInfo(culture);
		}

		// Token: 0x060019E2 RID: 6626 RVA: 0x00060C0C File Offset: 0x0005EE0C
		public static CompareInfo GetCompareInfo(string name, Assembly assembly)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (assembly == null)
			{
				throw new ArgumentNullException("assembly");
			}
			if (assembly != typeof(object).Module.Assembly)
			{
				throw new ArgumentException("Assembly is an invalid type");
			}
			return CompareInfo.GetCompareInfo(name);
		}

		// Token: 0x060019E3 RID: 6627 RVA: 0x00060C68 File Offset: 0x0005EE68
		public override int GetHashCode()
		{
			return this.LCID;
		}

		// Token: 0x060019E4 RID: 6628 RVA: 0x00060C70 File Offset: 0x0005EE70
		public virtual SortKey GetSortKey(string source)
		{
			return this.GetSortKey(source, CompareOptions.None);
		}

		// Token: 0x060019E5 RID: 6629 RVA: 0x00060C7C File Offset: 0x0005EE7C
		public virtual SortKey GetSortKey(string source, CompareOptions options)
		{
			if (options == CompareOptions.OrdinalIgnoreCase || options == CompareOptions.Ordinal)
			{
				throw new ArgumentException("Now allowed CompareOptions.", "options");
			}
			if (CompareInfo.UseManagedCollation)
			{
				return this.collator.GetSortKey(source, options);
			}
			SortKey sortKey = new SortKey(this.culture, source, options);
			this.assign_sortkey(sortKey, source, options);
			return sortKey;
		}

		// Token: 0x060019E6 RID: 6630 RVA: 0x00060CE8 File Offset: 0x0005EEE8
		public virtual int IndexOf(string source, char value)
		{
			return this.IndexOf(source, value, 0, source.Length, CompareOptions.None);
		}

		// Token: 0x060019E7 RID: 6631 RVA: 0x00060D08 File Offset: 0x0005EF08
		public virtual int IndexOf(string source, string value)
		{
			return this.IndexOf(source, value, 0, source.Length, CompareOptions.None);
		}

		// Token: 0x060019E8 RID: 6632 RVA: 0x00060D28 File Offset: 0x0005EF28
		public virtual int IndexOf(string source, char value, CompareOptions options)
		{
			return this.IndexOf(source, value, 0, source.Length, options);
		}

		// Token: 0x060019E9 RID: 6633 RVA: 0x00060D48 File Offset: 0x0005EF48
		public virtual int IndexOf(string source, char value, int startIndex)
		{
			return this.IndexOf(source, value, startIndex, source.Length - startIndex, CompareOptions.None);
		}

		// Token: 0x060019EA RID: 6634 RVA: 0x00060D68 File Offset: 0x0005EF68
		public virtual int IndexOf(string source, string value, CompareOptions options)
		{
			return this.IndexOf(source, value, 0, source.Length, options);
		}

		// Token: 0x060019EB RID: 6635 RVA: 0x00060D88 File Offset: 0x0005EF88
		public virtual int IndexOf(string source, string value, int startIndex)
		{
			return this.IndexOf(source, value, startIndex, source.Length - startIndex, CompareOptions.None);
		}

		// Token: 0x060019EC RID: 6636 RVA: 0x00060DA8 File Offset: 0x0005EFA8
		public virtual int IndexOf(string source, char value, int startIndex, CompareOptions options)
		{
			return this.IndexOf(source, value, startIndex, source.Length - startIndex, options);
		}

		// Token: 0x060019ED RID: 6637 RVA: 0x00060DC8 File Offset: 0x0005EFC8
		public virtual int IndexOf(string source, char value, int startIndex, int count)
		{
			return this.IndexOf(source, value, startIndex, count, CompareOptions.None);
		}

		// Token: 0x060019EE RID: 6638 RVA: 0x00060DD8 File Offset: 0x0005EFD8
		public virtual int IndexOf(string source, string value, int startIndex, CompareOptions options)
		{
			return this.IndexOf(source, value, startIndex, source.Length - startIndex, options);
		}

		// Token: 0x060019EF RID: 6639 RVA: 0x00060DF8 File Offset: 0x0005EFF8
		public virtual int IndexOf(string source, string value, int startIndex, int count)
		{
			return this.IndexOf(source, value, startIndex, count, CompareOptions.None);
		}

		// Token: 0x060019F0 RID: 6640 RVA: 0x00060E08 File Offset: 0x0005F008
		private int internal_index_managed(string s, int sindex, int count, char c, CompareOptions opt, bool first)
		{
			return (!first) ? this.collator.LastIndexOf(s, c, sindex, count, opt) : this.collator.IndexOf(s, c, sindex, count, opt);
		}

		// Token: 0x060019F1 RID: 6641 RVA: 0x00060E48 File Offset: 0x0005F048
		private int internal_index_switch(string s, int sindex, int count, char c, CompareOptions opt, bool first)
		{
			return (!CompareInfo.UseManagedCollation || (first && opt == CompareOptions.Ordinal)) ? this.internal_index(s, sindex, count, c, opt, first) : this.internal_index_managed(s, sindex, count, c, opt, first);
		}

		// Token: 0x060019F2 RID: 6642 RVA: 0x00060E98 File Offset: 0x0005F098
		public virtual int IndexOf(string source, char value, int startIndex, int count, CompareOptions options)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (startIndex < 0)
			{
				throw new ArgumentOutOfRangeException("startIndex");
			}
			if (count < 0 || source.Length - startIndex < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if ((options & (CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreSymbols | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.Ordinal | CompareOptions.OrdinalIgnoreCase)) != options)
			{
				throw new ArgumentException("options");
			}
			if (count == 0)
			{
				return -1;
			}
			if ((options & CompareOptions.Ordinal) != CompareOptions.None)
			{
				for (int i = startIndex; i < startIndex + count; i++)
				{
					if (source[i] == value)
					{
						return i;
					}
				}
				return -1;
			}
			return this.internal_index_switch(source, startIndex, count, value, options, true);
		}

		// Token: 0x060019F3 RID: 6643 RVA: 0x00060F50 File Offset: 0x0005F150
		private int internal_index_managed(string s1, int sindex, int count, string s2, CompareOptions opt, bool first)
		{
			return (!first) ? this.collator.LastIndexOf(s1, s2, sindex, count, opt) : this.collator.IndexOf(s1, s2, sindex, count, opt);
		}

		// Token: 0x060019F4 RID: 6644 RVA: 0x00060F90 File Offset: 0x0005F190
		private int internal_index_switch(string s1, int sindex, int count, string s2, CompareOptions opt, bool first)
		{
			return (!CompareInfo.UseManagedCollation || (first && opt == CompareOptions.Ordinal)) ? this.internal_index(s1, sindex, count, s2, opt, first) : this.internal_index_managed(s1, sindex, count, s2, opt, first);
		}

		// Token: 0x060019F5 RID: 6645 RVA: 0x00060FE0 File Offset: 0x0005F1E0
		public virtual int IndexOf(string source, string value, int startIndex, int count, CompareOptions options)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (startIndex < 0)
			{
				throw new ArgumentOutOfRangeException("startIndex");
			}
			if (count < 0 || source.Length - startIndex < count)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if ((options & (CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreSymbols | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.Ordinal | CompareOptions.OrdinalIgnoreCase)) != options)
			{
				throw new ArgumentException("options");
			}
			if (value.Length == 0)
			{
				return startIndex;
			}
			if (count == 0)
			{
				return -1;
			}
			return this.internal_index_switch(source, startIndex, count, value, options, true);
		}

		// Token: 0x060019F6 RID: 6646 RVA: 0x00061084 File Offset: 0x0005F284
		public virtual bool IsPrefix(string source, string prefix)
		{
			return this.IsPrefix(source, prefix, CompareOptions.None);
		}

		// Token: 0x060019F7 RID: 6647 RVA: 0x00061090 File Offset: 0x0005F290
		public virtual bool IsPrefix(string source, string prefix, CompareOptions options)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (prefix == null)
			{
				throw new ArgumentNullException("prefix");
			}
			if (CompareInfo.UseManagedCollation)
			{
				return this.collator.IsPrefix(source, prefix, options);
			}
			return source.Length >= prefix.Length && this.Compare(source, 0, prefix.Length, prefix, 0, prefix.Length, options) == 0;
		}

		// Token: 0x060019F8 RID: 6648 RVA: 0x00061108 File Offset: 0x0005F308
		public virtual bool IsSuffix(string source, string suffix)
		{
			return this.IsSuffix(source, suffix, CompareOptions.None);
		}

		// Token: 0x060019F9 RID: 6649 RVA: 0x00061114 File Offset: 0x0005F314
		public virtual bool IsSuffix(string source, string suffix, CompareOptions options)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (suffix == null)
			{
				throw new ArgumentNullException("suffix");
			}
			if (CompareInfo.UseManagedCollation)
			{
				return this.collator.IsSuffix(source, suffix, options);
			}
			return source.Length >= suffix.Length && this.Compare(source, source.Length - suffix.Length, suffix.Length, suffix, 0, suffix.Length, options) == 0;
		}

		// Token: 0x060019FA RID: 6650 RVA: 0x00061198 File Offset: 0x0005F398
		public virtual int LastIndexOf(string source, char value)
		{
			return this.LastIndexOf(source, value, source.Length - 1, source.Length, CompareOptions.None);
		}

		// Token: 0x060019FB RID: 6651 RVA: 0x000611BC File Offset: 0x0005F3BC
		public virtual int LastIndexOf(string source, string value)
		{
			return this.LastIndexOf(source, value, source.Length - 1, source.Length, CompareOptions.None);
		}

		// Token: 0x060019FC RID: 6652 RVA: 0x000611E0 File Offset: 0x0005F3E0
		public virtual int LastIndexOf(string source, char value, CompareOptions options)
		{
			return this.LastIndexOf(source, value, source.Length - 1, source.Length, options);
		}

		// Token: 0x060019FD RID: 6653 RVA: 0x00061204 File Offset: 0x0005F404
		public virtual int LastIndexOf(string source, char value, int startIndex)
		{
			return this.LastIndexOf(source, value, startIndex, startIndex + 1, CompareOptions.None);
		}

		// Token: 0x060019FE RID: 6654 RVA: 0x00061214 File Offset: 0x0005F414
		public virtual int LastIndexOf(string source, string value, CompareOptions options)
		{
			return this.LastIndexOf(source, value, source.Length - 1, source.Length, options);
		}

		// Token: 0x060019FF RID: 6655 RVA: 0x00061238 File Offset: 0x0005F438
		public virtual int LastIndexOf(string source, string value, int startIndex)
		{
			return this.LastIndexOf(source, value, startIndex, startIndex + 1, CompareOptions.None);
		}

		// Token: 0x06001A00 RID: 6656 RVA: 0x00061248 File Offset: 0x0005F448
		public virtual int LastIndexOf(string source, char value, int startIndex, CompareOptions options)
		{
			return this.LastIndexOf(source, value, startIndex, startIndex + 1, options);
		}

		// Token: 0x06001A01 RID: 6657 RVA: 0x00061258 File Offset: 0x0005F458
		public virtual int LastIndexOf(string source, char value, int startIndex, int count)
		{
			return this.LastIndexOf(source, value, startIndex, count, CompareOptions.None);
		}

		// Token: 0x06001A02 RID: 6658 RVA: 0x00061268 File Offset: 0x0005F468
		public virtual int LastIndexOf(string source, string value, int startIndex, CompareOptions options)
		{
			return this.LastIndexOf(source, value, startIndex, startIndex + 1, options);
		}

		// Token: 0x06001A03 RID: 6659 RVA: 0x00061278 File Offset: 0x0005F478
		public virtual int LastIndexOf(string source, string value, int startIndex, int count)
		{
			return this.LastIndexOf(source, value, startIndex, count, CompareOptions.None);
		}

		// Token: 0x06001A04 RID: 6660 RVA: 0x00061288 File Offset: 0x0005F488
		public virtual int LastIndexOf(string source, char value, int startIndex, int count, CompareOptions options)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (startIndex < 0)
			{
				throw new ArgumentOutOfRangeException("startIndex");
			}
			if (count < 0 || startIndex - count < -1)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if ((options & (CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreSymbols | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.Ordinal | CompareOptions.OrdinalIgnoreCase)) != options)
			{
				throw new ArgumentException("options");
			}
			if (count == 0)
			{
				return -1;
			}
			if ((options & CompareOptions.Ordinal) != CompareOptions.None)
			{
				for (int i = startIndex; i > startIndex - count; i--)
				{
					if (source[i] == value)
					{
						return i;
					}
				}
				return -1;
			}
			return this.internal_index_switch(source, startIndex, count, value, options, false);
		}

		// Token: 0x06001A05 RID: 6661 RVA: 0x0006133C File Offset: 0x0005F53C
		public virtual int LastIndexOf(string source, string value, int startIndex, int count, CompareOptions options)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (startIndex < 0)
			{
				throw new ArgumentOutOfRangeException("startIndex");
			}
			if (count < 0 || startIndex - count < -1)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			if ((options & (CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreSymbols | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.Ordinal | CompareOptions.OrdinalIgnoreCase)) != options)
			{
				throw new ArgumentException("options");
			}
			if (count == 0)
			{
				return -1;
			}
			if (value.Length == 0)
			{
				return 0;
			}
			return this.internal_index_switch(source, startIndex, count, value, options, false);
		}

		// Token: 0x06001A06 RID: 6662 RVA: 0x000613DC File Offset: 0x0005F5DC
		[ComVisible(false)]
		public static bool IsSortable(char ch)
		{
			return MSCompatUnicodeTable.IsSortable((int)ch);
		}

		// Token: 0x06001A07 RID: 6663 RVA: 0x000613E4 File Offset: 0x0005F5E4
		[ComVisible(false)]
		public static bool IsSortable(string text)
		{
			return MSCompatUnicodeTable.IsSortable(text);
		}

		// Token: 0x06001A08 RID: 6664 RVA: 0x000613EC File Offset: 0x0005F5EC
		public override string ToString()
		{
			return "CompareInfo - " + this.culture;
		}

		// Token: 0x17000419 RID: 1049
		// (get) Token: 0x06001A09 RID: 6665 RVA: 0x00061404 File Offset: 0x0005F604
		public int LCID
		{
			get
			{
				return this.culture;
			}
		}

		// Token: 0x1700041A RID: 1050
		// (get) Token: 0x06001A0A RID: 6666 RVA: 0x0006140C File Offset: 0x0005F60C
		[ComVisible(false)]
		public virtual string Name
		{
			get
			{
				return this.icu_name;
			}
		}

		// Token: 0x04000980 RID: 2432
		private const CompareOptions ValidCompareOptions_NoStringSort = CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreSymbols | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.Ordinal | CompareOptions.OrdinalIgnoreCase;

		// Token: 0x04000981 RID: 2433
		private const CompareOptions ValidCompareOptions = CompareOptions.IgnoreCase | CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreSymbols | CompareOptions.IgnoreKanaType | CompareOptions.IgnoreWidth | CompareOptions.StringSort | CompareOptions.Ordinal | CompareOptions.OrdinalIgnoreCase;

		// Token: 0x04000982 RID: 2434
		private static readonly bool useManagedCollation = Environment.internalGetEnvironmentVariable("MONO_DISABLE_MANAGED_COLLATION") != "yes" && MSCompatUnicodeTable.IsReady;

		// Token: 0x04000983 RID: 2435
		private int culture;

		// Token: 0x04000984 RID: 2436
		[NonSerialized]
		private string icu_name;

		// Token: 0x04000985 RID: 2437
		private int win32LCID;

		// Token: 0x04000986 RID: 2438
		private string m_name;

		// Token: 0x04000987 RID: 2439
		[NonSerialized]
		private SimpleCollator collator;

		// Token: 0x04000988 RID: 2440
		private static Hashtable collators;

		// Token: 0x04000989 RID: 2441
		[NonSerialized]
		private static object monitor = new object();
	}
}
