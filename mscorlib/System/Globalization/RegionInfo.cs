﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000225 RID: 549
	[ComVisible(true)]
	[Serializable]
	public class RegionInfo
	{
		// Token: 0x06001C1D RID: 7197 RVA: 0x00068044 File Offset: 0x00066244
		public RegionInfo(int culture)
		{
			if (!this.GetByTerritory(CultureInfo.GetCultureInfo(culture)))
			{
				throw new ArgumentException(string.Format("Region ID {0} (0x{0:X4}) is not a supported region.", culture), "culture");
			}
		}

		// Token: 0x06001C1E RID: 7198 RVA: 0x00068084 File Offset: 0x00066284
		public RegionInfo(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException();
			}
			if (this.construct_internal_region_from_name(name.ToUpperInvariant()))
			{
				this.lcid = name.GetHashCode();
				return;
			}
			if (!this.GetByTerritory(CultureInfo.GetCultureInfo(name)))
			{
				throw new ArgumentException(string.Format("Region name {0} is not supported.", name), "name");
			}
		}

		// Token: 0x170004AE RID: 1198
		// (get) Token: 0x06001C1F RID: 7199 RVA: 0x000680E8 File Offset: 0x000662E8
		public static RegionInfo CurrentRegion
		{
			get
			{
				if (RegionInfo.currentRegion == null)
				{
					CultureInfo currentCulture = CultureInfo.CurrentCulture;
					if (currentCulture == null || CultureInfo.BootstrapCultureID == 127)
					{
						return null;
					}
					RegionInfo.currentRegion = new RegionInfo(CultureInfo.BootstrapCultureID);
				}
				return RegionInfo.currentRegion;
			}
		}

		// Token: 0x06001C20 RID: 7200 RVA: 0x00068130 File Offset: 0x00066330
		private bool GetByTerritory(CultureInfo ci)
		{
			if (ci == null)
			{
				throw new Exception("INTERNAL ERROR: should not happen.");
			}
			if (ci.IsNeutralCulture || ci.Territory == null)
			{
				return false;
			}
			this.lcid = ci.LCID;
			return this.construct_internal_region_from_name(ci.Territory.ToUpperInvariant());
		}

		// Token: 0x06001C21 RID: 7201
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern bool construct_internal_region_from_name(string name);

		// Token: 0x170004AF RID: 1199
		// (get) Token: 0x06001C22 RID: 7202 RVA: 0x00068184 File Offset: 0x00066384
		[ComVisible(false)]
		public virtual string CurrencyEnglishName
		{
			get
			{
				return this.currencyEnglishName;
			}
		}

		// Token: 0x170004B0 RID: 1200
		// (get) Token: 0x06001C23 RID: 7203 RVA: 0x0006818C File Offset: 0x0006638C
		public virtual string CurrencySymbol
		{
			get
			{
				return this.currencySymbol;
			}
		}

		// Token: 0x170004B1 RID: 1201
		// (get) Token: 0x06001C24 RID: 7204 RVA: 0x00068194 File Offset: 0x00066394
		[MonoTODO("DisplayName currently only returns the EnglishName")]
		public virtual string DisplayName
		{
			get
			{
				return this.englishName;
			}
		}

		// Token: 0x170004B2 RID: 1202
		// (get) Token: 0x06001C25 RID: 7205 RVA: 0x0006819C File Offset: 0x0006639C
		public virtual string EnglishName
		{
			get
			{
				return this.englishName;
			}
		}

		// Token: 0x170004B3 RID: 1203
		// (get) Token: 0x06001C26 RID: 7206 RVA: 0x000681A4 File Offset: 0x000663A4
		[ComVisible(false)]
		public virtual int GeoId
		{
			get
			{
				return this.regionId;
			}
		}

		// Token: 0x170004B4 RID: 1204
		// (get) Token: 0x06001C27 RID: 7207 RVA: 0x000681AC File Offset: 0x000663AC
		public virtual bool IsMetric
		{
			get
			{
				string text = this.iso2Name;
				if (text != null)
				{
					if (RegionInfo.<>f__switch$map1D == null)
					{
						RegionInfo.<>f__switch$map1D = new Dictionary<string, int>(2)
						{
							{
								"US",
								0
							},
							{
								"UK",
								0
							}
						};
					}
					int num;
					if (RegionInfo.<>f__switch$map1D.TryGetValue(text, out num))
					{
						if (num == 0)
						{
							return false;
						}
					}
				}
				return true;
			}
		}

		// Token: 0x170004B5 RID: 1205
		// (get) Token: 0x06001C28 RID: 7208 RVA: 0x00068218 File Offset: 0x00066418
		public virtual string ISOCurrencySymbol
		{
			get
			{
				return this.isoCurrencySymbol;
			}
		}

		// Token: 0x170004B6 RID: 1206
		// (get) Token: 0x06001C29 RID: 7209 RVA: 0x00068220 File Offset: 0x00066420
		[ComVisible(false)]
		public virtual string NativeName
		{
			get
			{
				return this.DisplayName;
			}
		}

		// Token: 0x170004B7 RID: 1207
		// (get) Token: 0x06001C2A RID: 7210 RVA: 0x00068228 File Offset: 0x00066428
		[MonoTODO("Not implemented")]
		[ComVisible(false)]
		public virtual string CurrencyNativeName
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170004B8 RID: 1208
		// (get) Token: 0x06001C2B RID: 7211 RVA: 0x00068230 File Offset: 0x00066430
		public virtual string Name
		{
			get
			{
				return this.iso2Name;
			}
		}

		// Token: 0x170004B9 RID: 1209
		// (get) Token: 0x06001C2C RID: 7212 RVA: 0x00068238 File Offset: 0x00066438
		public virtual string ThreeLetterISORegionName
		{
			get
			{
				return this.iso3Name;
			}
		}

		// Token: 0x170004BA RID: 1210
		// (get) Token: 0x06001C2D RID: 7213 RVA: 0x00068240 File Offset: 0x00066440
		public virtual string ThreeLetterWindowsRegionName
		{
			get
			{
				return this.win3Name;
			}
		}

		// Token: 0x170004BB RID: 1211
		// (get) Token: 0x06001C2E RID: 7214 RVA: 0x00068248 File Offset: 0x00066448
		public virtual string TwoLetterISORegionName
		{
			get
			{
				return this.iso2Name;
			}
		}

		// Token: 0x06001C2F RID: 7215 RVA: 0x00068250 File Offset: 0x00066450
		public override bool Equals(object value)
		{
			RegionInfo regionInfo = value as RegionInfo;
			return regionInfo != null && this.lcid == regionInfo.lcid;
		}

		// Token: 0x06001C30 RID: 7216 RVA: 0x0006827C File Offset: 0x0006647C
		public override int GetHashCode()
		{
			return (int)((ulong)int.MinValue + (ulong)((long)((long)this.regionId << 3)) + (ulong)((long)this.regionId));
		}

		// Token: 0x06001C31 RID: 7217 RVA: 0x00068298 File Offset: 0x00066498
		public override string ToString()
		{
			return this.Name;
		}

		// Token: 0x04000A8D RID: 2701
		private static RegionInfo currentRegion;

		// Token: 0x04000A8E RID: 2702
		private int lcid;

		// Token: 0x04000A8F RID: 2703
		private int regionId;

		// Token: 0x04000A90 RID: 2704
		private string iso2Name;

		// Token: 0x04000A91 RID: 2705
		private string iso3Name;

		// Token: 0x04000A92 RID: 2706
		private string win3Name;

		// Token: 0x04000A93 RID: 2707
		private string englishName;

		// Token: 0x04000A94 RID: 2708
		private string currencySymbol;

		// Token: 0x04000A95 RID: 2709
		private string isoCurrencySymbol;

		// Token: 0x04000A96 RID: 2710
		private string currencyEnglishName;
	}
}
