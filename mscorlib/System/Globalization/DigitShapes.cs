﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000214 RID: 532
	[ComVisible(true)]
	[Serializable]
	public enum DigitShapes
	{
		// Token: 0x04000A16 RID: 2582
		Context,
		// Token: 0x04000A17 RID: 2583
		None,
		// Token: 0x04000A18 RID: 2584
		NativeNational
	}
}
