﻿using System;

namespace System.Globalization
{
	// Token: 0x0200021C RID: 540
	internal class Punycode : Bootstring
	{
		// Token: 0x06001B48 RID: 6984 RVA: 0x00065944 File Offset: 0x00063B44
		public Punycode() : base('-', 36, 1, 26, 38, 700, 72, 128)
		{
		}
	}
}
