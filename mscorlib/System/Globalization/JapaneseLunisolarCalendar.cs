﻿using System;

namespace System.Globalization
{
	// Token: 0x0200021E RID: 542
	[Serializable]
	public class JapaneseLunisolarCalendar : EastAsianLunisolarCalendar
	{
		// Token: 0x06001B69 RID: 7017 RVA: 0x00065DD4 File Offset: 0x00063FD4
		[MonoTODO]
		public JapaneseLunisolarCalendar() : base(JapaneseLunisolarCalendar.era_handler)
		{
		}

		// Token: 0x06001B6A RID: 7018 RVA: 0x00065DE4 File Offset: 0x00063FE4
		static JapaneseLunisolarCalendar()
		{
			JapaneseLunisolarCalendar.era_handler = new CCEastAsianLunisolarEraHandler();
			JapaneseLunisolarCalendar.era_handler.appendEra(3, CCGregorianCalendar.fixed_from_dmy(25, 12, 1926), CCGregorianCalendar.fixed_from_dmy(7, 1, 1989));
			JapaneseLunisolarCalendar.era_handler.appendEra(4, CCGregorianCalendar.fixed_from_dmy(8, 1, 1989));
		}

		// Token: 0x17000477 RID: 1143
		// (get) Token: 0x06001B6B RID: 7019 RVA: 0x00065E64 File Offset: 0x00064064
		internal override int ActualCurrentEra
		{
			get
			{
				return 4;
			}
		}

		// Token: 0x17000478 RID: 1144
		// (get) Token: 0x06001B6C RID: 7020 RVA: 0x00065E68 File Offset: 0x00064068
		public override int[] Eras
		{
			get
			{
				return (int[])JapaneseLunisolarCalendar.era_handler.Eras.Clone();
			}
		}

		// Token: 0x06001B6D RID: 7021 RVA: 0x00065E80 File Offset: 0x00064080
		public override int GetEra(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int result;
			JapaneseLunisolarCalendar.era_handler.EraYear(out result, date);
			return result;
		}

		// Token: 0x17000479 RID: 1145
		// (get) Token: 0x06001B6E RID: 7022 RVA: 0x00065EA4 File Offset: 0x000640A4
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return JapaneseLunisolarCalendar.JapanMin;
			}
		}

		// Token: 0x1700047A RID: 1146
		// (get) Token: 0x06001B6F RID: 7023 RVA: 0x00065EAC File Offset: 0x000640AC
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return JapaneseLunisolarCalendar.JapanMax;
			}
		}

		// Token: 0x04000A3F RID: 2623
		public const int JapaneseEra = 1;

		// Token: 0x04000A40 RID: 2624
		internal static readonly CCEastAsianLunisolarEraHandler era_handler;

		// Token: 0x04000A41 RID: 2625
		private static DateTime JapanMin = new DateTime(1960, 1, 28, 0, 0, 0);

		// Token: 0x04000A42 RID: 2626
		private static DateTime JapanMax = new DateTime(2050, 1, 22, 23, 59, 59);
	}
}
