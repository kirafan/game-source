﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000086 RID: 134
	[ComVisible(true)]
	[Serializable]
	public class SortKey
	{
		// Token: 0x060007F2 RID: 2034 RVA: 0x0001CA18 File Offset: 0x0001AC18
		internal SortKey(int lcid, string source, CompareOptions opt)
		{
			this.lcid = lcid;
			this.source = source;
			this.options = opt;
		}

		// Token: 0x060007F3 RID: 2035 RVA: 0x0001CA38 File Offset: 0x0001AC38
		internal SortKey(int lcid, string source, byte[] buffer, CompareOptions opt, int lv1Length, int lv2Length, int lv3Length, int kanaSmallLength, int markTypeLength, int katakanaLength, int kanaWidthLength, int identLength)
		{
			this.lcid = lcid;
			this.source = source;
			this.key = buffer;
			this.options = opt;
		}

		// Token: 0x060007F4 RID: 2036 RVA: 0x0001CA60 File Offset: 0x0001AC60
		public static int Compare(SortKey sortkey1, SortKey sortkey2)
		{
			if (sortkey1 == null)
			{
				throw new ArgumentNullException("sortkey1");
			}
			if (sortkey2 == null)
			{
				throw new ArgumentNullException("sortkey2");
			}
			if (object.ReferenceEquals(sortkey1, sortkey2) || object.ReferenceEquals(sortkey1.OriginalString, sortkey2.OriginalString))
			{
				return 0;
			}
			byte[] keyData = sortkey1.KeyData;
			byte[] keyData2 = sortkey2.KeyData;
			int num = (keyData.Length <= keyData2.Length) ? keyData.Length : keyData2.Length;
			for (int i = 0; i < num; i++)
			{
				if (keyData[i] != keyData2[i])
				{
					return (keyData[i] >= keyData2[i]) ? 1 : -1;
				}
			}
			return (keyData.Length != keyData2.Length) ? ((keyData.Length >= keyData2.Length) ? 1 : -1) : 0;
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x060007F5 RID: 2037 RVA: 0x0001CB2C File Offset: 0x0001AD2C
		public virtual string OriginalString
		{
			get
			{
				return this.source;
			}
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x060007F6 RID: 2038 RVA: 0x0001CB34 File Offset: 0x0001AD34
		public virtual byte[] KeyData
		{
			get
			{
				return this.key;
			}
		}

		// Token: 0x060007F7 RID: 2039 RVA: 0x0001CB3C File Offset: 0x0001AD3C
		public override bool Equals(object value)
		{
			SortKey sortKey = value as SortKey;
			return sortKey != null && this.lcid == sortKey.lcid && this.options == sortKey.options && SortKey.Compare(this, sortKey) == 0;
		}

		// Token: 0x060007F8 RID: 2040 RVA: 0x0001CB88 File Offset: 0x0001AD88
		public override int GetHashCode()
		{
			if (this.key.Length == 0)
			{
				return 0;
			}
			int num = (int)this.key[0];
			for (int i = 1; i < this.key.Length; i++)
			{
				num ^= (int)this.key[i] << (i & 3);
			}
			return num;
		}

		// Token: 0x060007F9 RID: 2041 RVA: 0x0001CBDC File Offset: 0x0001ADDC
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"SortKey - ",
				this.lcid,
				", ",
				this.options,
				", ",
				this.source
			});
		}

		// Token: 0x0400016B RID: 363
		private readonly string source;

		// Token: 0x0400016C RID: 364
		private readonly CompareOptions options;

		// Token: 0x0400016D RID: 365
		private readonly byte[] key;

		// Token: 0x0400016E RID: 366
		private readonly int lcid;
	}
}
