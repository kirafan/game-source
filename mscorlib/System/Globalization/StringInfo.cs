﻿using System;
using System.Collections;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x02000226 RID: 550
	[ComVisible(true)]
	[Serializable]
	public class StringInfo
	{
		// Token: 0x06001C32 RID: 7218 RVA: 0x000682A0 File Offset: 0x000664A0
		public StringInfo()
		{
		}

		// Token: 0x06001C33 RID: 7219 RVA: 0x000682A8 File Offset: 0x000664A8
		public StringInfo(string value)
		{
			this.String = value;
		}

		// Token: 0x06001C34 RID: 7220 RVA: 0x000682B8 File Offset: 0x000664B8
		[ComVisible(false)]
		public override bool Equals(object value)
		{
			StringInfo stringInfo = value as StringInfo;
			return stringInfo != null && this.s == stringInfo.s;
		}

		// Token: 0x06001C35 RID: 7221 RVA: 0x000682E8 File Offset: 0x000664E8
		[ComVisible(false)]
		public override int GetHashCode()
		{
			return this.s.GetHashCode();
		}

		// Token: 0x170004BC RID: 1212
		// (get) Token: 0x06001C36 RID: 7222 RVA: 0x000682F8 File Offset: 0x000664F8
		public int LengthInTextElements
		{
			get
			{
				if (this.length < 0)
				{
					this.length = 0;
					int i = 0;
					while (i < this.s.Length)
					{
						i += StringInfo.GetNextTextElementLength(this.s, i);
						this.length++;
					}
				}
				return this.length;
			}
		}

		// Token: 0x170004BD RID: 1213
		// (get) Token: 0x06001C37 RID: 7223 RVA: 0x00068354 File Offset: 0x00066554
		// (set) Token: 0x06001C38 RID: 7224 RVA: 0x0006835C File Offset: 0x0006655C
		public string String
		{
			get
			{
				return this.s;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				this.length = -1;
				this.s = value;
			}
		}

		// Token: 0x06001C39 RID: 7225 RVA: 0x00068380 File Offset: 0x00066580
		public string SubstringByTextElements(int startingTextElement)
		{
			if (startingTextElement < 0 || this.s.Length == 0)
			{
				throw new ArgumentOutOfRangeException("startingTextElement");
			}
			int num = 0;
			for (int i = 0; i < startingTextElement; i++)
			{
				if (num >= this.s.Length)
				{
					throw new ArgumentOutOfRangeException("startingTextElement");
				}
				num += StringInfo.GetNextTextElementLength(this.s, num);
			}
			return this.s.Substring(num);
		}

		// Token: 0x06001C3A RID: 7226 RVA: 0x000683FC File Offset: 0x000665FC
		public string SubstringByTextElements(int startingTextElement, int lengthInTextElements)
		{
			if (startingTextElement < 0 || this.s.Length == 0)
			{
				throw new ArgumentOutOfRangeException("startingTextElement");
			}
			if (lengthInTextElements < 0)
			{
				throw new ArgumentOutOfRangeException("lengthInTextElements");
			}
			int num = 0;
			for (int i = 0; i < startingTextElement; i++)
			{
				if (num >= this.s.Length)
				{
					throw new ArgumentOutOfRangeException("startingTextElement");
				}
				num += StringInfo.GetNextTextElementLength(this.s, num);
			}
			int num2 = num;
			for (int j = 0; j < lengthInTextElements; j++)
			{
				if (num >= this.s.Length)
				{
					throw new ArgumentOutOfRangeException("lengthInTextElements");
				}
				num += StringInfo.GetNextTextElementLength(this.s, num);
			}
			return this.s.Substring(num2, num - num2);
		}

		// Token: 0x06001C3B RID: 7227 RVA: 0x000684CC File Offset: 0x000666CC
		public static string GetNextTextElement(string str)
		{
			if (str == null || str.Length == 0)
			{
				throw new ArgumentNullException("string is null");
			}
			return StringInfo.GetNextTextElement(str, 0);
		}

		// Token: 0x06001C3C RID: 7228 RVA: 0x000684F4 File Offset: 0x000666F4
		public static string GetNextTextElement(string str, int index)
		{
			int nextTextElementLength = StringInfo.GetNextTextElementLength(str, index);
			return (nextTextElementLength == 1) ? new string(str[index], 1) : str.Substring(index, nextTextElementLength);
		}

		// Token: 0x06001C3D RID: 7229 RVA: 0x0006852C File Offset: 0x0006672C
		private static int GetNextTextElementLength(string str, int index)
		{
			if (str == null)
			{
				throw new ArgumentNullException("string is null");
			}
			if (index >= str.Length)
			{
				return 0;
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("Index is not valid");
			}
			char c = str[index];
			UnicodeCategory unicodeCategory = char.GetUnicodeCategory(c);
			if (unicodeCategory == UnicodeCategory.Surrogate)
			{
				if (c < '\ud800' || c > '\udbff')
				{
					return 1;
				}
				if (index + 1 < str.Length && str[index + 1] >= '\udc00' && str[index + 1] <= '\udfff')
				{
					return 2;
				}
				return 1;
			}
			else
			{
				if (unicodeCategory == UnicodeCategory.NonSpacingMark || unicodeCategory == UnicodeCategory.SpacingCombiningMark || unicodeCategory == UnicodeCategory.EnclosingMark)
				{
					return 1;
				}
				int num = 1;
				while (index + num < str.Length)
				{
					unicodeCategory = char.GetUnicodeCategory(str[index + num]);
					if (unicodeCategory != UnicodeCategory.NonSpacingMark && unicodeCategory != UnicodeCategory.SpacingCombiningMark && unicodeCategory != UnicodeCategory.EnclosingMark)
					{
						break;
					}
					num++;
				}
				return num;
			}
		}

		// Token: 0x06001C3E RID: 7230 RVA: 0x0006862C File Offset: 0x0006682C
		public static TextElementEnumerator GetTextElementEnumerator(string str)
		{
			if (str == null || str.Length == 0)
			{
				throw new ArgumentNullException("string is null");
			}
			return new TextElementEnumerator(str, 0);
		}

		// Token: 0x06001C3F RID: 7231 RVA: 0x00068654 File Offset: 0x00066854
		public static TextElementEnumerator GetTextElementEnumerator(string str, int index)
		{
			if (str == null)
			{
				throw new ArgumentNullException("string is null");
			}
			if (index < 0 || index >= str.Length)
			{
				throw new ArgumentOutOfRangeException("Index is not valid");
			}
			return new TextElementEnumerator(str, index);
		}

		// Token: 0x06001C40 RID: 7232 RVA: 0x00068698 File Offset: 0x00066898
		public static int[] ParseCombiningCharacters(string str)
		{
			if (str == null)
			{
				throw new ArgumentNullException("string is null");
			}
			ArrayList arrayList = new ArrayList(str.Length);
			TextElementEnumerator textElementEnumerator = StringInfo.GetTextElementEnumerator(str);
			textElementEnumerator.Reset();
			while (textElementEnumerator.MoveNext())
			{
				arrayList.Add(textElementEnumerator.ElementIndex);
			}
			return (int[])arrayList.ToArray(typeof(int));
		}

		// Token: 0x04000A98 RID: 2712
		private string s;

		// Token: 0x04000A99 RID: 2713
		private int length;
	}
}
