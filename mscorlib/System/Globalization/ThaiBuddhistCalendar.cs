﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x0200022C RID: 556
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with.NET")]
	[Serializable]
	public class ThaiBuddhistCalendar : Calendar
	{
		// Token: 0x06001C85 RID: 7301 RVA: 0x000696E0 File Offset: 0x000678E0
		public ThaiBuddhistCalendar()
		{
			this.M_AbbrEraNames = new string[]
			{
				"T.B.C.E."
			};
			this.M_EraNames = new string[]
			{
				"ThaiBuddhist current era"
			};
			if (this.twoDigitYearMax == 99)
			{
				this.twoDigitYearMax = 2572;
			}
		}

		// Token: 0x06001C86 RID: 7302 RVA: 0x00069734 File Offset: 0x00067934
		static ThaiBuddhistCalendar()
		{
			ThaiBuddhistCalendar.M_EraHandler = new CCGregorianEraHandler();
			ThaiBuddhistCalendar.M_EraHandler.appendEra(1, CCGregorianCalendar.fixed_from_dmy(1, 1, -542));
		}

		// Token: 0x170004D1 RID: 1233
		// (get) Token: 0x06001C87 RID: 7303 RVA: 0x0006978C File Offset: 0x0006798C
		public override int[] Eras
		{
			get
			{
				return (int[])ThaiBuddhistCalendar.M_EraHandler.Eras.Clone();
			}
		}

		// Token: 0x170004D2 RID: 1234
		// (get) Token: 0x06001C88 RID: 7304 RVA: 0x000697A4 File Offset: 0x000679A4
		// (set) Token: 0x06001C89 RID: 7305 RVA: 0x000697AC File Offset: 0x000679AC
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x06001C8A RID: 7306 RVA: 0x000697DC File Offset: 0x000679DC
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = 1;
			}
			if (!ThaiBuddhistCalendar.M_EraHandler.ValidEra(era))
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001C8B RID: 7307 RVA: 0x00069810 File Offset: 0x00067A10
		internal int M_CheckYEG(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			return ThaiBuddhistCalendar.M_EraHandler.GregorianYear(year, era);
		}

		// Token: 0x06001C8C RID: 7308 RVA: 0x00069828 File Offset: 0x00067A28
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckYEG(year, ref era);
		}

		// Token: 0x06001C8D RID: 7309 RVA: 0x00069834 File Offset: 0x00067A34
		internal int M_CheckYMEG(int year, int month, ref int era)
		{
			int result = this.M_CheckYEG(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
			return result;
		}

		// Token: 0x06001C8E RID: 7310 RVA: 0x0006986C File Offset: 0x00067A6C
		internal int M_CheckYMDEG(int year, int month, int day, ref int era)
		{
			int result = this.M_CheckYMEG(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, era));
			return result;
		}

		// Token: 0x06001C8F RID: 7311 RVA: 0x000698A0 File Offset: 0x00067AA0
		public override DateTime AddMonths(DateTime time, int months)
		{
			return CCGregorianCalendar.AddMonths(time, months);
		}

		// Token: 0x06001C90 RID: 7312 RVA: 0x000698AC File Offset: 0x00067AAC
		public override DateTime AddYears(DateTime time, int years)
		{
			return CCGregorianCalendar.AddYears(time, years);
		}

		// Token: 0x06001C91 RID: 7313 RVA: 0x000698B8 File Offset: 0x00067AB8
		public override int GetDayOfMonth(DateTime time)
		{
			return CCGregorianCalendar.GetDayOfMonth(time);
		}

		// Token: 0x06001C92 RID: 7314 RVA: 0x000698C0 File Offset: 0x00067AC0
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			return CCFixed.day_of_week(date);
		}

		// Token: 0x06001C93 RID: 7315 RVA: 0x000698DC File Offset: 0x00067ADC
		public override int GetDayOfYear(DateTime time)
		{
			return CCGregorianCalendar.GetDayOfYear(time);
		}

		// Token: 0x06001C94 RID: 7316 RVA: 0x000698E4 File Offset: 0x00067AE4
		public override int GetDaysInMonth(int year, int month, int era)
		{
			int year2 = this.M_CheckYMEG(year, month, ref era);
			return CCGregorianCalendar.GetDaysInMonth(year2, month);
		}

		// Token: 0x06001C95 RID: 7317 RVA: 0x00069904 File Offset: 0x00067B04
		public override int GetDaysInYear(int year, int era)
		{
			int year2 = this.M_CheckYEG(year, ref era);
			return CCGregorianCalendar.GetDaysInYear(year2);
		}

		// Token: 0x06001C96 RID: 7318 RVA: 0x00069924 File Offset: 0x00067B24
		public override int GetEra(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int result;
			ThaiBuddhistCalendar.M_EraHandler.EraYear(out result, date);
			return result;
		}

		// Token: 0x06001C97 RID: 7319 RVA: 0x00069948 File Offset: 0x00067B48
		[ComVisible(false)]
		public override int GetLeapMonth(int year, int era)
		{
			return 0;
		}

		// Token: 0x06001C98 RID: 7320 RVA: 0x0006994C File Offset: 0x00067B4C
		public override int GetMonth(DateTime time)
		{
			return CCGregorianCalendar.GetMonth(time);
		}

		// Token: 0x06001C99 RID: 7321 RVA: 0x00069954 File Offset: 0x00067B54
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return 12;
		}

		// Token: 0x06001C9A RID: 7322 RVA: 0x00069964 File Offset: 0x00067B64
		[ComVisible(false)]
		public override int GetWeekOfYear(DateTime time, CalendarWeekRule rule, DayOfWeek firstDayOfWeek)
		{
			return base.GetWeekOfYear(time, rule, firstDayOfWeek);
		}

		// Token: 0x06001C9B RID: 7323 RVA: 0x00069970 File Offset: 0x00067B70
		public override int GetYear(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int num;
			return ThaiBuddhistCalendar.M_EraHandler.EraYear(out num, date);
		}

		// Token: 0x06001C9C RID: 7324 RVA: 0x00069994 File Offset: 0x00067B94
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			int year2 = this.M_CheckYMDEG(year, month, day, ref era);
			return CCGregorianCalendar.IsLeapDay(year2, month, day);
		}

		// Token: 0x06001C9D RID: 7325 RVA: 0x000699B8 File Offset: 0x00067BB8
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYMEG(year, month, ref era);
			return false;
		}

		// Token: 0x06001C9E RID: 7326 RVA: 0x000699C8 File Offset: 0x00067BC8
		public override bool IsLeapYear(int year, int era)
		{
			int year2 = this.M_CheckYEG(year, ref era);
			return CCGregorianCalendar.is_leap_year(year2);
		}

		// Token: 0x06001C9F RID: 7327 RVA: 0x000699E8 File Offset: 0x00067BE8
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			int year2 = this.M_CheckYMDEG(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			return CCGregorianCalendar.ToDateTime(year2, month, day, hour, minute, second, millisecond);
		}

		// Token: 0x06001CA0 RID: 7328 RVA: 0x00069A20 File Offset: 0x00067C20
		public override int ToFourDigitYear(int year)
		{
			return base.ToFourDigitYear(year);
		}

		// Token: 0x170004D3 RID: 1235
		// (get) Token: 0x06001CA1 RID: 7329 RVA: 0x00069A2C File Offset: 0x00067C2C
		[ComVisible(false)]
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.SolarCalendar;
			}
		}

		// Token: 0x170004D4 RID: 1236
		// (get) Token: 0x06001CA2 RID: 7330 RVA: 0x00069A30 File Offset: 0x00067C30
		[ComVisible(false)]
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return ThaiBuddhistCalendar.ThaiMin;
			}
		}

		// Token: 0x170004D5 RID: 1237
		// (get) Token: 0x06001CA3 RID: 7331 RVA: 0x00069A38 File Offset: 0x00067C38
		[ComVisible(false)]
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return ThaiBuddhistCalendar.ThaiMax;
			}
		}

		// Token: 0x04000AB4 RID: 2740
		public const int ThaiBuddhistEra = 1;

		// Token: 0x04000AB5 RID: 2741
		internal static readonly CCGregorianEraHandler M_EraHandler;

		// Token: 0x04000AB6 RID: 2742
		private static DateTime ThaiMin = new DateTime(1, 1, 1, 0, 0, 0);

		// Token: 0x04000AB7 RID: 2743
		private static DateTime ThaiMax = new DateTime(9999, 12, 31, 11, 59, 59);
	}
}
