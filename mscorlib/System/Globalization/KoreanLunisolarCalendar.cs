﻿using System;

namespace System.Globalization
{
	// Token: 0x02000221 RID: 545
	[Serializable]
	public class KoreanLunisolarCalendar : EastAsianLunisolarCalendar
	{
		// Token: 0x06001BAC RID: 7084 RVA: 0x00066624 File Offset: 0x00064824
		[MonoTODO]
		public KoreanLunisolarCalendar() : base(KoreanLunisolarCalendar.era_handler)
		{
		}

		// Token: 0x06001BAD RID: 7085 RVA: 0x00066634 File Offset: 0x00064834
		static KoreanLunisolarCalendar()
		{
			KoreanLunisolarCalendar.era_handler = new CCEastAsianLunisolarEraHandler();
			KoreanLunisolarCalendar.era_handler.appendEra(1, CCFixed.FromDateTime(new DateTime(1, 1, 1)));
		}

		// Token: 0x17000485 RID: 1157
		// (get) Token: 0x06001BAE RID: 7086 RVA: 0x00066690 File Offset: 0x00064890
		public override int[] Eras
		{
			get
			{
				return (int[])KoreanLunisolarCalendar.era_handler.Eras.Clone();
			}
		}

		// Token: 0x06001BAF RID: 7087 RVA: 0x000666A8 File Offset: 0x000648A8
		public override int GetEra(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			int result;
			KoreanLunisolarCalendar.era_handler.EraYear(out result, date);
			return result;
		}

		// Token: 0x17000486 RID: 1158
		// (get) Token: 0x06001BB0 RID: 7088 RVA: 0x000666CC File Offset: 0x000648CC
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return KoreanLunisolarCalendar.KoreanMin;
			}
		}

		// Token: 0x17000487 RID: 1159
		// (get) Token: 0x06001BB1 RID: 7089 RVA: 0x000666D4 File Offset: 0x000648D4
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return KoreanLunisolarCalendar.KoreanMax;
			}
		}

		// Token: 0x04000A4A RID: 2634
		public const int GregorianEra = 1;

		// Token: 0x04000A4B RID: 2635
		internal static readonly CCEastAsianLunisolarEraHandler era_handler;

		// Token: 0x04000A4C RID: 2636
		private static DateTime KoreanMin = new DateTime(918, 2, 14, 0, 0, 0);

		// Token: 0x04000A4D RID: 2637
		private static DateTime KoreanMax = new DateTime(2051, 2, 10, 23, 59, 59);
	}
}
