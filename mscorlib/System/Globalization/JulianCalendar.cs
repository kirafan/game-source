﻿using System;
using System.Runtime.InteropServices;

namespace System.Globalization
{
	// Token: 0x0200021F RID: 543
	[ComVisible(true)]
	[MonoTODO("Serialization format not compatible with .NET")]
	[Serializable]
	public class JulianCalendar : Calendar
	{
		// Token: 0x06001B70 RID: 7024 RVA: 0x00065EB4 File Offset: 0x000640B4
		public JulianCalendar()
		{
			this.M_AbbrEraNames = new string[]
			{
				"C.E."
			};
			this.M_EraNames = new string[]
			{
				"Common Era"
			};
			if (this.twoDigitYearMax == 99)
			{
				this.twoDigitYearMax = 2029;
			}
		}

		// Token: 0x1700047B RID: 1147
		// (get) Token: 0x06001B72 RID: 7026 RVA: 0x00065F3C File Offset: 0x0006413C
		public override int[] Eras
		{
			get
			{
				return new int[]
				{
					JulianCalendar.JulianEra
				};
			}
		}

		// Token: 0x1700047C RID: 1148
		// (get) Token: 0x06001B73 RID: 7027 RVA: 0x00065F4C File Offset: 0x0006414C
		// (set) Token: 0x06001B74 RID: 7028 RVA: 0x00065F54 File Offset: 0x00064154
		public override int TwoDigitYearMax
		{
			get
			{
				return this.twoDigitYearMax;
			}
			set
			{
				base.CheckReadOnly();
				base.M_ArgumentInRange("value", value, 100, this.M_MaxYear);
				this.twoDigitYearMax = value;
			}
		}

		// Token: 0x06001B75 RID: 7029 RVA: 0x00065F84 File Offset: 0x00064184
		internal void M_CheckEra(ref int era)
		{
			if (era == 0)
			{
				era = JulianCalendar.JulianEra;
			}
			if (era != JulianCalendar.JulianEra)
			{
				throw new ArgumentException("Era value was not valid.");
			}
		}

		// Token: 0x06001B76 RID: 7030 RVA: 0x00065FAC File Offset: 0x000641AC
		internal override void M_CheckYE(int year, ref int era)
		{
			this.M_CheckEra(ref era);
			base.M_ArgumentInRange("year", year, 1, 9999);
		}

		// Token: 0x06001B77 RID: 7031 RVA: 0x00065FD4 File Offset: 0x000641D4
		internal void M_CheckYME(int year, int month, ref int era)
		{
			this.M_CheckYE(year, ref era);
			if (month < 1 || month > 12)
			{
				throw new ArgumentOutOfRangeException("month", "Month must be between one and twelve.");
			}
		}

		// Token: 0x06001B78 RID: 7032 RVA: 0x00066000 File Offset: 0x00064200
		internal void M_CheckYMDE(int year, int month, int day, ref int era)
		{
			this.M_CheckYME(year, month, ref era);
			base.M_ArgumentInRange("day", day, 1, this.GetDaysInMonth(year, month, era));
			if (year == 9999 && ((month == 10 && day > 19) || month > 10))
			{
				throw new ArgumentOutOfRangeException("The maximum Julian date is 19. 10. 9999.");
			}
		}

		// Token: 0x06001B79 RID: 7033 RVA: 0x00066060 File Offset: 0x00064260
		public override DateTime AddMonths(DateTime time, int months)
		{
			int date = CCFixed.FromDateTime(time);
			int day;
			int num;
			int num2;
			CCJulianCalendar.dmy_from_fixed(out day, out num, out num2, date);
			num += months;
			num2 += CCMath.div_mod(out num, num, 12);
			date = CCJulianCalendar.fixed_from_dmy(day, num, num2);
			return CCFixed.ToDateTime(date).Add(time.TimeOfDay);
		}

		// Token: 0x06001B7A RID: 7034 RVA: 0x000660B0 File Offset: 0x000642B0
		public override DateTime AddYears(DateTime time, int years)
		{
			int date = CCFixed.FromDateTime(time);
			int day;
			int month;
			int num;
			CCJulianCalendar.dmy_from_fixed(out day, out month, out num, date);
			num += years;
			date = CCJulianCalendar.fixed_from_dmy(day, month, num);
			return CCFixed.ToDateTime(date).Add(time.TimeOfDay);
		}

		// Token: 0x06001B7B RID: 7035 RVA: 0x000660F4 File Offset: 0x000642F4
		public override int GetDayOfMonth(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			return CCJulianCalendar.day_from_fixed(date);
		}

		// Token: 0x06001B7C RID: 7036 RVA: 0x00066110 File Offset: 0x00064310
		public override DayOfWeek GetDayOfWeek(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			return CCFixed.day_of_week(date);
		}

		// Token: 0x06001B7D RID: 7037 RVA: 0x0006612C File Offset: 0x0006432C
		public override int GetDayOfYear(DateTime time)
		{
			int num = CCFixed.FromDateTime(time);
			int year = CCJulianCalendar.year_from_fixed(num);
			int num2 = CCJulianCalendar.fixed_from_dmy(1, 1, year);
			return num - num2 + 1;
		}

		// Token: 0x06001B7E RID: 7038 RVA: 0x00066158 File Offset: 0x00064358
		public override int GetDaysInMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			int num = CCJulianCalendar.fixed_from_dmy(1, month, year);
			int num2 = CCJulianCalendar.fixed_from_dmy(1, month + 1, year);
			return num2 - num;
		}

		// Token: 0x06001B7F RID: 7039 RVA: 0x00066188 File Offset: 0x00064388
		public override int GetDaysInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			int num = CCJulianCalendar.fixed_from_dmy(1, 1, year);
			int num2 = CCJulianCalendar.fixed_from_dmy(1, 1, year + 1);
			return num2 - num;
		}

		// Token: 0x06001B80 RID: 7040 RVA: 0x000661B8 File Offset: 0x000643B8
		public override int GetEra(DateTime time)
		{
			return JulianCalendar.JulianEra;
		}

		// Token: 0x06001B81 RID: 7041 RVA: 0x000661C0 File Offset: 0x000643C0
		[ComVisible(false)]
		public override int GetLeapMonth(int year, int era)
		{
			return 0;
		}

		// Token: 0x06001B82 RID: 7042 RVA: 0x000661C4 File Offset: 0x000643C4
		public override int GetMonth(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			return CCJulianCalendar.month_from_fixed(date);
		}

		// Token: 0x06001B83 RID: 7043 RVA: 0x000661E0 File Offset: 0x000643E0
		public override int GetMonthsInYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return 12;
		}

		// Token: 0x06001B84 RID: 7044 RVA: 0x000661F0 File Offset: 0x000643F0
		public override int GetYear(DateTime time)
		{
			int date = CCFixed.FromDateTime(time);
			return CCJulianCalendar.year_from_fixed(date);
		}

		// Token: 0x06001B85 RID: 7045 RVA: 0x0006620C File Offset: 0x0006440C
		public override bool IsLeapDay(int year, int month, int day, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			return this.IsLeapYear(year) && month == 2 && day == 29;
		}

		// Token: 0x06001B86 RID: 7046 RVA: 0x00066240 File Offset: 0x00064440
		public override bool IsLeapMonth(int year, int month, int era)
		{
			this.M_CheckYME(year, month, ref era);
			return false;
		}

		// Token: 0x06001B87 RID: 7047 RVA: 0x00066250 File Offset: 0x00064450
		public override bool IsLeapYear(int year, int era)
		{
			this.M_CheckYE(year, ref era);
			return CCJulianCalendar.is_leap_year(year);
		}

		// Token: 0x06001B88 RID: 7048 RVA: 0x00066264 File Offset: 0x00064464
		public override DateTime ToDateTime(int year, int month, int day, int hour, int minute, int second, int millisecond, int era)
		{
			this.M_CheckYMDE(year, month, day, ref era);
			base.M_CheckHMSM(hour, minute, second, millisecond);
			int date = CCJulianCalendar.fixed_from_dmy(day, month, year);
			return CCFixed.ToDateTime(date, hour, minute, second, (double)millisecond);
		}

		// Token: 0x06001B89 RID: 7049 RVA: 0x000662A4 File Offset: 0x000644A4
		public override int ToFourDigitYear(int year)
		{
			return base.ToFourDigitYear(year);
		}

		// Token: 0x1700047D RID: 1149
		// (get) Token: 0x06001B8A RID: 7050 RVA: 0x000662B0 File Offset: 0x000644B0
		[ComVisible(false)]
		public override CalendarAlgorithmType AlgorithmType
		{
			get
			{
				return CalendarAlgorithmType.SolarCalendar;
			}
		}

		// Token: 0x1700047E RID: 1150
		// (get) Token: 0x06001B8B RID: 7051 RVA: 0x000662B4 File Offset: 0x000644B4
		[ComVisible(false)]
		public override DateTime MinSupportedDateTime
		{
			get
			{
				return JulianCalendar.JulianMin;
			}
		}

		// Token: 0x1700047F RID: 1151
		// (get) Token: 0x06001B8C RID: 7052 RVA: 0x000662BC File Offset: 0x000644BC
		[ComVisible(false)]
		public override DateTime MaxSupportedDateTime
		{
			get
			{
				return JulianCalendar.JulianMax;
			}
		}

		// Token: 0x04000A43 RID: 2627
		public static readonly int JulianEra = 1;

		// Token: 0x04000A44 RID: 2628
		private static DateTime JulianMin = new DateTime(1, 1, 1, 0, 0, 0);

		// Token: 0x04000A45 RID: 2629
		private static DateTime JulianMax = new DateTime(9999, 12, 31, 11, 59, 59);
	}
}
