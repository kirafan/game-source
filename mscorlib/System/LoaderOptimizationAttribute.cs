﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200014B RID: 331
	[AttributeUsage(AttributeTargets.Method)]
	[ComVisible(true)]
	public sealed class LoaderOptimizationAttribute : Attribute
	{
		// Token: 0x060011CA RID: 4554 RVA: 0x00047230 File Offset: 0x00045430
		public LoaderOptimizationAttribute(byte value)
		{
			this.lo = (LoaderOptimization)value;
		}

		// Token: 0x060011CB RID: 4555 RVA: 0x00047240 File Offset: 0x00045440
		public LoaderOptimizationAttribute(LoaderOptimization value)
		{
			this.lo = value;
		}

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x060011CC RID: 4556 RVA: 0x00047250 File Offset: 0x00045450
		public LoaderOptimization Value
		{
			get
			{
				return this.lo;
			}
		}

		// Token: 0x04000525 RID: 1317
		private LoaderOptimization lo;
	}
}
