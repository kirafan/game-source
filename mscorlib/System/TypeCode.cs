﻿using System;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x0200018D RID: 397
	[ComVisible(true)]
	[Serializable]
	public enum TypeCode
	{
		// Token: 0x040007EC RID: 2028
		Empty,
		// Token: 0x040007ED RID: 2029
		Object,
		// Token: 0x040007EE RID: 2030
		DBNull,
		// Token: 0x040007EF RID: 2031
		Boolean,
		// Token: 0x040007F0 RID: 2032
		Char,
		// Token: 0x040007F1 RID: 2033
		SByte,
		// Token: 0x040007F2 RID: 2034
		Byte,
		// Token: 0x040007F3 RID: 2035
		Int16,
		// Token: 0x040007F4 RID: 2036
		UInt16,
		// Token: 0x040007F5 RID: 2037
		Int32,
		// Token: 0x040007F6 RID: 2038
		UInt32,
		// Token: 0x040007F7 RID: 2039
		Int64,
		// Token: 0x040007F8 RID: 2040
		UInt64,
		// Token: 0x040007F9 RID: 2041
		Single,
		// Token: 0x040007FA RID: 2042
		Double,
		// Token: 0x040007FB RID: 2043
		Decimal,
		// Token: 0x040007FC RID: 2044
		DateTime,
		// Token: 0x040007FD RID: 2045
		String = 18
	}
}
