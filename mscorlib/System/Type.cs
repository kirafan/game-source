﻿using System;
using System.Collections;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace System
{
	// Token: 0x02000033 RID: 51
	[ComVisible(true)]
	[ComDefaultInterface(typeof(_Type))]
	[ClassInterface(ClassInterfaceType.None)]
	[Serializable]
	public abstract class Type : MemberInfo, IReflect, _Type
	{
		// Token: 0x060004BF RID: 1215 RVA: 0x00012980 File Offset: 0x00010B80
		void _Type.GetIDsOfNames([In] ref Guid riid, IntPtr rgszNames, uint cNames, uint lcid, IntPtr rgDispId)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060004C0 RID: 1216 RVA: 0x00012988 File Offset: 0x00010B88
		void _Type.GetTypeInfo(uint iTInfo, uint lcid, IntPtr ppTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060004C1 RID: 1217 RVA: 0x00012990 File Offset: 0x00010B90
		void _Type.GetTypeInfoCount(out uint pcTInfo)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060004C2 RID: 1218 RVA: 0x00012998 File Offset: 0x00010B98
		void _Type.Invoke(uint dispIdMember, [In] ref Guid riid, uint lcid, short wFlags, IntPtr pDispParams, IntPtr pVarResult, IntPtr pExcepInfo, IntPtr puArgErr)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060004C3 RID: 1219 RVA: 0x000129A0 File Offset: 0x00010BA0
		private static bool FilterName_impl(MemberInfo m, object filterCriteria)
		{
			string text = (string)filterCriteria;
			if (text == null || text.Length == 0)
			{
				return false;
			}
			if (text[text.Length - 1] == '*')
			{
				return string.Compare(text, 0, m.Name, 0, text.Length - 1, false, CultureInfo.InvariantCulture) == 0;
			}
			return text.Equals(m.Name);
		}

		// Token: 0x060004C4 RID: 1220 RVA: 0x00012A08 File Offset: 0x00010C08
		private static bool FilterNameIgnoreCase_impl(MemberInfo m, object filterCriteria)
		{
			string text = (string)filterCriteria;
			if (text == null || text.Length == 0)
			{
				return false;
			}
			if (text[text.Length - 1] == '*')
			{
				return string.Compare(text, 0, m.Name, 0, text.Length - 1, true, CultureInfo.InvariantCulture) == 0;
			}
			return string.Compare(text, m.Name, true, CultureInfo.InvariantCulture) == 0;
		}

		// Token: 0x060004C5 RID: 1221 RVA: 0x00012A7C File Offset: 0x00010C7C
		private static bool FilterAttribute_impl(MemberInfo m, object filterCriteria)
		{
			int num = ((IConvertible)filterCriteria).ToInt32(null);
			if (m is MethodInfo)
			{
				return (((MethodInfo)m).Attributes & (MethodAttributes)num) != MethodAttributes.PrivateScope;
			}
			if (m is FieldInfo)
			{
				return (((FieldInfo)m).Attributes & (FieldAttributes)num) != FieldAttributes.PrivateScope;
			}
			if (m is PropertyInfo)
			{
				return (((PropertyInfo)m).Attributes & (PropertyAttributes)num) != PropertyAttributes.None;
			}
			return m is EventInfo && (((EventInfo)m).Attributes & (EventAttributes)num) != EventAttributes.None;
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060004C6 RID: 1222
		public abstract Assembly Assembly { get; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060004C7 RID: 1223
		public abstract string AssemblyQualifiedName { get; }

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060004C8 RID: 1224 RVA: 0x00012B14 File Offset: 0x00010D14
		public TypeAttributes Attributes
		{
			get
			{
				return this.GetAttributeFlagsImpl();
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060004C9 RID: 1225
		public abstract Type BaseType { get; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060004CA RID: 1226 RVA: 0x00012B1C File Offset: 0x00010D1C
		public override Type DeclaringType
		{
			get
			{
				return null;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060004CB RID: 1227 RVA: 0x00012B20 File Offset: 0x00010D20
		public static Binder DefaultBinder
		{
			get
			{
				return Binder.DefaultBinder;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060004CC RID: 1228
		public abstract string FullName { get; }

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060004CD RID: 1229
		public abstract Guid GUID { get; }

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x060004CE RID: 1230 RVA: 0x00012B28 File Offset: 0x00010D28
		public bool HasElementType
		{
			get
			{
				return this.HasElementTypeImpl();
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x060004CF RID: 1231 RVA: 0x00012B30 File Offset: 0x00010D30
		public bool IsAbstract
		{
			get
			{
				return (this.Attributes & TypeAttributes.Abstract) != TypeAttributes.NotPublic;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x060004D0 RID: 1232 RVA: 0x00012B44 File Offset: 0x00010D44
		public bool IsAnsiClass
		{
			get
			{
				return (this.Attributes & TypeAttributes.StringFormatMask) == TypeAttributes.NotPublic;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x060004D1 RID: 1233 RVA: 0x00012B58 File Offset: 0x00010D58
		public bool IsArray
		{
			get
			{
				return this.IsArrayImpl();
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x060004D2 RID: 1234 RVA: 0x00012B60 File Offset: 0x00010D60
		public bool IsAutoClass
		{
			get
			{
				return (this.Attributes & TypeAttributes.StringFormatMask) == TypeAttributes.AutoClass;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x060004D3 RID: 1235 RVA: 0x00012B78 File Offset: 0x00010D78
		public bool IsAutoLayout
		{
			get
			{
				return (this.Attributes & TypeAttributes.LayoutMask) == TypeAttributes.NotPublic;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x060004D4 RID: 1236 RVA: 0x00012B88 File Offset: 0x00010D88
		public bool IsByRef
		{
			get
			{
				return this.IsByRefImpl();
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060004D5 RID: 1237 RVA: 0x00012B90 File Offset: 0x00010D90
		public bool IsClass
		{
			get
			{
				return !this.IsInterface && !this.IsValueType;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060004D6 RID: 1238 RVA: 0x00012BA8 File Offset: 0x00010DA8
		public bool IsCOMObject
		{
			get
			{
				return this.IsCOMObjectImpl();
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060004D7 RID: 1239 RVA: 0x00012BB0 File Offset: 0x00010DB0
		public bool IsContextful
		{
			get
			{
				return this.IsContextfulImpl();
			}
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060004D8 RID: 1240 RVA: 0x00012BB8 File Offset: 0x00010DB8
		public bool IsEnum
		{
			get
			{
				return this.IsSubclassOf(typeof(Enum));
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060004D9 RID: 1241 RVA: 0x00012BCC File Offset: 0x00010DCC
		public bool IsExplicitLayout
		{
			get
			{
				return (this.Attributes & TypeAttributes.LayoutMask) == TypeAttributes.ExplicitLayout;
			}
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060004DA RID: 1242 RVA: 0x00012BDC File Offset: 0x00010DDC
		public bool IsImport
		{
			get
			{
				return (this.Attributes & TypeAttributes.Import) != TypeAttributes.NotPublic;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060004DB RID: 1243 RVA: 0x00012BF0 File Offset: 0x00010DF0
		public bool IsInterface
		{
			get
			{
				return (this.Attributes & TypeAttributes.ClassSemanticsMask) == TypeAttributes.ClassSemanticsMask;
			}
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x060004DC RID: 1244 RVA: 0x00012C00 File Offset: 0x00010E00
		public bool IsLayoutSequential
		{
			get
			{
				return (this.Attributes & TypeAttributes.LayoutMask) == TypeAttributes.SequentialLayout;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060004DD RID: 1245 RVA: 0x00012C10 File Offset: 0x00010E10
		public bool IsMarshalByRef
		{
			get
			{
				return this.IsMarshalByRefImpl();
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060004DE RID: 1246 RVA: 0x00012C18 File Offset: 0x00010E18
		public bool IsNestedAssembly
		{
			get
			{
				return (this.Attributes & TypeAttributes.VisibilityMask) == TypeAttributes.NestedAssembly;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060004DF RID: 1247 RVA: 0x00012C28 File Offset: 0x00010E28
		public bool IsNestedFamANDAssem
		{
			get
			{
				return (this.Attributes & TypeAttributes.VisibilityMask) == TypeAttributes.NestedFamANDAssem;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x060004E0 RID: 1248 RVA: 0x00012C38 File Offset: 0x00010E38
		public bool IsNestedFamily
		{
			get
			{
				return (this.Attributes & TypeAttributes.VisibilityMask) == TypeAttributes.NestedFamily;
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x060004E1 RID: 1249 RVA: 0x00012C48 File Offset: 0x00010E48
		public bool IsNestedFamORAssem
		{
			get
			{
				return (this.Attributes & TypeAttributes.VisibilityMask) == TypeAttributes.VisibilityMask;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x060004E2 RID: 1250 RVA: 0x00012C58 File Offset: 0x00010E58
		public bool IsNestedPrivate
		{
			get
			{
				return (this.Attributes & TypeAttributes.VisibilityMask) == TypeAttributes.NestedPrivate;
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x060004E3 RID: 1251 RVA: 0x00012C68 File Offset: 0x00010E68
		public bool IsNestedPublic
		{
			get
			{
				return (this.Attributes & TypeAttributes.VisibilityMask) == TypeAttributes.NestedPublic;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x060004E4 RID: 1252 RVA: 0x00012C78 File Offset: 0x00010E78
		public bool IsNotPublic
		{
			get
			{
				return (this.Attributes & TypeAttributes.VisibilityMask) == TypeAttributes.NotPublic;
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x060004E5 RID: 1253 RVA: 0x00012C88 File Offset: 0x00010E88
		public bool IsPointer
		{
			get
			{
				return this.IsPointerImpl();
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x060004E6 RID: 1254 RVA: 0x00012C90 File Offset: 0x00010E90
		public bool IsPrimitive
		{
			get
			{
				return this.IsPrimitiveImpl();
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x060004E7 RID: 1255 RVA: 0x00012C98 File Offset: 0x00010E98
		public bool IsPublic
		{
			get
			{
				return (this.Attributes & TypeAttributes.VisibilityMask) == TypeAttributes.Public;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x060004E8 RID: 1256 RVA: 0x00012CA8 File Offset: 0x00010EA8
		public bool IsSealed
		{
			get
			{
				return (this.Attributes & TypeAttributes.Sealed) != TypeAttributes.NotPublic;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x060004E9 RID: 1257 RVA: 0x00012CBC File Offset: 0x00010EBC
		public bool IsSerializable
		{
			get
			{
				if ((this.Attributes & TypeAttributes.Serializable) != TypeAttributes.NotPublic)
				{
					return true;
				}
				Type type = this.UnderlyingSystemType;
				if (type == null)
				{
					return false;
				}
				if (type.IsSystemType)
				{
					return Type.type_is_subtype_of(type, typeof(Enum), false) || Type.type_is_subtype_of(type, typeof(Delegate), false);
				}
				while (type != typeof(Enum) && type != typeof(Delegate))
				{
					type = type.BaseType;
					if (type == null)
					{
						return false;
					}
				}
				return true;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x060004EA RID: 1258 RVA: 0x00012D54 File Offset: 0x00010F54
		public bool IsSpecialName
		{
			get
			{
				return (this.Attributes & TypeAttributes.SpecialName) != TypeAttributes.NotPublic;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x060004EB RID: 1259 RVA: 0x00012D68 File Offset: 0x00010F68
		public bool IsUnicodeClass
		{
			get
			{
				return (this.Attributes & TypeAttributes.StringFormatMask) == TypeAttributes.UnicodeClass;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x060004EC RID: 1260 RVA: 0x00012D80 File Offset: 0x00010F80
		public bool IsValueType
		{
			get
			{
				return this.IsValueTypeImpl();
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x060004ED RID: 1261 RVA: 0x00012D88 File Offset: 0x00010F88
		public override MemberTypes MemberType
		{
			get
			{
				return MemberTypes.TypeInfo;
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x060004EE RID: 1262
		public abstract override Module Module { get; }

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x060004EF RID: 1263
		public abstract string Namespace { get; }

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x060004F0 RID: 1264 RVA: 0x00012D8C File Offset: 0x00010F8C
		public override Type ReflectedType
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x060004F1 RID: 1265 RVA: 0x00012D90 File Offset: 0x00010F90
		public virtual RuntimeTypeHandle TypeHandle
		{
			get
			{
				return default(RuntimeTypeHandle);
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060004F2 RID: 1266 RVA: 0x00012DA8 File Offset: 0x00010FA8
		[ComVisible(true)]
		public ConstructorInfo TypeInitializer
		{
			get
			{
				return this.GetConstructorImpl(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic, null, CallingConventions.Any, Type.EmptyTypes, null);
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060004F3 RID: 1267
		public abstract Type UnderlyingSystemType { get; }

		// Token: 0x060004F4 RID: 1268 RVA: 0x00012DBC File Offset: 0x00010FBC
		public override bool Equals(object o)
		{
			if (o == null)
			{
				return false;
			}
			Type type = o as Type;
			return type != null && this.Equals(type);
		}

		// Token: 0x060004F5 RID: 1269 RVA: 0x00012DE8 File Offset: 0x00010FE8
		public bool Equals(Type o)
		{
			return o != null && this.UnderlyingSystemType.EqualsInternal(o.UnderlyingSystemType);
		}

		// Token: 0x060004F6 RID: 1270
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern bool EqualsInternal(Type type);

		// Token: 0x060004F7 RID: 1271
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Type internal_from_handle(IntPtr handle);

		// Token: 0x060004F8 RID: 1272
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Type internal_from_name(string name, bool throwOnError, bool ignoreCase);

		// Token: 0x060004F9 RID: 1273 RVA: 0x00012E04 File Offset: 0x00011004
		public static Type GetType(string typeName)
		{
			if (typeName == null)
			{
				throw new ArgumentNullException("TypeName");
			}
			return Type.internal_from_name(typeName, false, false);
		}

		// Token: 0x060004FA RID: 1274 RVA: 0x00012E20 File Offset: 0x00011020
		public static Type GetType(string typeName, bool throwOnError)
		{
			if (typeName == null)
			{
				throw new ArgumentNullException("TypeName");
			}
			Type type = Type.internal_from_name(typeName, throwOnError, false);
			if (throwOnError && type == null)
			{
				throw new TypeLoadException("Error loading '" + typeName + "'");
			}
			return type;
		}

		// Token: 0x060004FB RID: 1275 RVA: 0x00012E6C File Offset: 0x0001106C
		public static Type GetType(string typeName, bool throwOnError, bool ignoreCase)
		{
			if (typeName == null)
			{
				throw new ArgumentNullException("TypeName");
			}
			Type type = Type.internal_from_name(typeName, throwOnError, ignoreCase);
			if (throwOnError && type == null)
			{
				throw new TypeLoadException("Error loading '" + typeName + "'");
			}
			return type;
		}

		// Token: 0x060004FC RID: 1276 RVA: 0x00012EB8 File Offset: 0x000110B8
		public static Type[] GetTypeArray(object[] args)
		{
			if (args == null)
			{
				throw new ArgumentNullException("args");
			}
			Type[] array = new Type[args.Length];
			for (int i = 0; i < args.Length; i++)
			{
				array[i] = args[i].GetType();
			}
			return array;
		}

		// Token: 0x060004FD RID: 1277
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern TypeCode GetTypeCodeInternal(Type type);

		// Token: 0x060004FE RID: 1278 RVA: 0x00012F00 File Offset: 0x00011100
		public static TypeCode GetTypeCode(Type type)
		{
			if (type is MonoType)
			{
				return Type.GetTypeCodeInternal(type);
			}
			if (type == null)
			{
				return TypeCode.Empty;
			}
			type = type.UnderlyingSystemType;
			if (!type.IsSystemType)
			{
				return TypeCode.Object;
			}
			return Type.GetTypeCodeInternal(type);
		}

		// Token: 0x060004FF RID: 1279 RVA: 0x00012F44 File Offset: 0x00011144
		[MonoTODO("This operation is currently not supported by Mono")]
		public static Type GetTypeFromCLSID(Guid clsid)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000500 RID: 1280 RVA: 0x00012F4C File Offset: 0x0001114C
		[MonoTODO("This operation is currently not supported by Mono")]
		public static Type GetTypeFromCLSID(Guid clsid, bool throwOnError)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000501 RID: 1281 RVA: 0x00012F54 File Offset: 0x00011154
		[MonoTODO("This operation is currently not supported by Mono")]
		public static Type GetTypeFromCLSID(Guid clsid, string server)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000502 RID: 1282 RVA: 0x00012F5C File Offset: 0x0001115C
		[MonoTODO("This operation is currently not supported by Mono")]
		public static Type GetTypeFromCLSID(Guid clsid, string server, bool throwOnError)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000503 RID: 1283 RVA: 0x00012F64 File Offset: 0x00011164
		public static Type GetTypeFromHandle(RuntimeTypeHandle handle)
		{
			if (handle.Value == IntPtr.Zero)
			{
				return null;
			}
			return Type.internal_from_handle(handle.Value);
		}

		// Token: 0x06000504 RID: 1284 RVA: 0x00012F98 File Offset: 0x00011198
		[MonoTODO("Mono does not support COM")]
		public static Type GetTypeFromProgID(string progID)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000505 RID: 1285 RVA: 0x00012FA0 File Offset: 0x000111A0
		[MonoTODO("Mono does not support COM")]
		public static Type GetTypeFromProgID(string progID, bool throwOnError)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000506 RID: 1286 RVA: 0x00012FA8 File Offset: 0x000111A8
		[MonoTODO("Mono does not support COM")]
		public static Type GetTypeFromProgID(string progID, string server)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000507 RID: 1287 RVA: 0x00012FB0 File Offset: 0x000111B0
		[MonoTODO("Mono does not support COM")]
		public static Type GetTypeFromProgID(string progID, string server, bool throwOnError)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000508 RID: 1288 RVA: 0x00012FB8 File Offset: 0x000111B8
		public static RuntimeTypeHandle GetTypeHandle(object o)
		{
			if (o == null)
			{
				throw new ArgumentNullException();
			}
			return o.GetType().TypeHandle;
		}

		// Token: 0x06000509 RID: 1289
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool type_is_subtype_of(Type a, Type b, bool check_interfaces);

		// Token: 0x0600050A RID: 1290
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool type_is_assignable_from(Type a, Type b);

		// Token: 0x0600050B RID: 1291 RVA: 0x00012FD4 File Offset: 0x000111D4
		public new Type GetType()
		{
			return base.GetType();
		}

		// Token: 0x0600050C RID: 1292 RVA: 0x00012FDC File Offset: 0x000111DC
		[ComVisible(true)]
		public virtual bool IsSubclassOf(Type c)
		{
			if (c == null || c == this)
			{
				return false;
			}
			if (this.IsSystemType)
			{
				return c.IsSystemType && Type.type_is_subtype_of(this, c, false);
			}
			for (Type baseType = this.BaseType; baseType != null; baseType = baseType.BaseType)
			{
				if (baseType == c)
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600050D RID: 1293 RVA: 0x00013040 File Offset: 0x00011240
		public virtual Type[] FindInterfaces(TypeFilter filter, object filterCriteria)
		{
			if (filter == null)
			{
				throw new ArgumentNullException("filter");
			}
			ArrayList arrayList = new ArrayList();
			foreach (Type type in this.GetInterfaces())
			{
				if (filter(type, filterCriteria))
				{
					arrayList.Add(type);
				}
			}
			return (Type[])arrayList.ToArray(typeof(Type));
		}

		// Token: 0x0600050E RID: 1294 RVA: 0x000130B0 File Offset: 0x000112B0
		public Type GetInterface(string name)
		{
			return this.GetInterface(name, false);
		}

		// Token: 0x0600050F RID: 1295
		public abstract Type GetInterface(string name, bool ignoreCase);

		// Token: 0x06000510 RID: 1296
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern void GetInterfaceMapData(Type t, Type iface, out MethodInfo[] targets, out MethodInfo[] methods);

		// Token: 0x06000511 RID: 1297 RVA: 0x000130BC File Offset: 0x000112BC
		[ComVisible(true)]
		public virtual InterfaceMapping GetInterfaceMap(Type interfaceType)
		{
			if (interfaceType == null)
			{
				throw new ArgumentNullException("interfaceType");
			}
			if (!interfaceType.IsInterface)
			{
				throw new ArgumentException(Locale.GetText("Argument must be an interface."), "interfaceType");
			}
			if (this.IsInterface)
			{
				throw new ArgumentException("'this' type cannot be an interface itself");
			}
			InterfaceMapping result;
			result.TargetType = this;
			result.InterfaceType = interfaceType;
			Type.GetInterfaceMapData(this, interfaceType, out result.TargetMethods, out result.InterfaceMethods);
			if (result.TargetMethods == null)
			{
				throw new ArgumentException(Locale.GetText("Interface not found"), "interfaceType");
			}
			return result;
		}

		// Token: 0x06000512 RID: 1298
		public abstract Type[] GetInterfaces();

		// Token: 0x06000513 RID: 1299 RVA: 0x00013158 File Offset: 0x00011358
		public virtual bool IsAssignableFrom(Type c)
		{
			if (c == null)
			{
				return false;
			}
			if (this.Equals(c))
			{
				return true;
			}
			if (c is TypeBuilder)
			{
				return ((TypeBuilder)c).IsAssignableTo(this);
			}
			if (!this.IsSystemType)
			{
				Type underlyingSystemType = this.UnderlyingSystemType;
				return underlyingSystemType.IsSystemType && underlyingSystemType.IsAssignableFrom(c);
			}
			if (!c.IsSystemType)
			{
				Type underlyingSystemType2 = c.UnderlyingSystemType;
				return underlyingSystemType2.IsSystemType && this.IsAssignableFrom(underlyingSystemType2);
			}
			return Type.type_is_assignable_from(this, c);
		}

		// Token: 0x06000514 RID: 1300
		[MethodImpl(MethodImplOptions.InternalCall)]
		public virtual extern bool IsInstanceOfType(object o);

		// Token: 0x06000515 RID: 1301 RVA: 0x000131E8 File Offset: 0x000113E8
		public virtual int GetArrayRank()
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000516 RID: 1302
		public abstract Type GetElementType();

		// Token: 0x06000517 RID: 1303 RVA: 0x000131F0 File Offset: 0x000113F0
		public EventInfo GetEvent(string name)
		{
			return this.GetEvent(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x06000518 RID: 1304
		public abstract EventInfo GetEvent(string name, BindingFlags bindingAttr);

		// Token: 0x06000519 RID: 1305 RVA: 0x000131FC File Offset: 0x000113FC
		public virtual EventInfo[] GetEvents()
		{
			return this.GetEvents(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x0600051A RID: 1306
		public abstract EventInfo[] GetEvents(BindingFlags bindingAttr);

		// Token: 0x0600051B RID: 1307 RVA: 0x00013208 File Offset: 0x00011408
		public FieldInfo GetField(string name)
		{
			return this.GetField(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x0600051C RID: 1308
		public abstract FieldInfo GetField(string name, BindingFlags bindingAttr);

		// Token: 0x0600051D RID: 1309 RVA: 0x00013214 File Offset: 0x00011414
		public FieldInfo[] GetFields()
		{
			return this.GetFields(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x0600051E RID: 1310
		public abstract FieldInfo[] GetFields(BindingFlags bindingAttr);

		// Token: 0x0600051F RID: 1311 RVA: 0x00013220 File Offset: 0x00011420
		public override int GetHashCode()
		{
			Type underlyingSystemType = this.UnderlyingSystemType;
			if (underlyingSystemType != null && underlyingSystemType != this)
			{
				return underlyingSystemType.GetHashCode();
			}
			return (int)this._impl.Value;
		}

		// Token: 0x06000520 RID: 1312 RVA: 0x00013258 File Offset: 0x00011458
		public MemberInfo[] GetMember(string name)
		{
			return this.GetMember(name, MemberTypes.All, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x00013268 File Offset: 0x00011468
		public virtual MemberInfo[] GetMember(string name, BindingFlags bindingAttr)
		{
			return this.GetMember(name, MemberTypes.All, bindingAttr);
		}

		// Token: 0x06000522 RID: 1314 RVA: 0x00013278 File Offset: 0x00011478
		public virtual MemberInfo[] GetMember(string name, MemberTypes type, BindingFlags bindingAttr)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if ((bindingAttr & BindingFlags.IgnoreCase) != BindingFlags.Default)
			{
				return this.FindMembers(type, bindingAttr, Type.FilterNameIgnoreCase, name);
			}
			return this.FindMembers(type, bindingAttr, Type.FilterName, name);
		}

		// Token: 0x06000523 RID: 1315 RVA: 0x000132BC File Offset: 0x000114BC
		public MemberInfo[] GetMembers()
		{
			return this.GetMembers(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x06000524 RID: 1316
		public abstract MemberInfo[] GetMembers(BindingFlags bindingAttr);

		// Token: 0x06000525 RID: 1317 RVA: 0x000132C8 File Offset: 0x000114C8
		public MethodInfo GetMethod(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			return this.GetMethodImpl(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public, null, CallingConventions.Any, null, null);
		}

		// Token: 0x06000526 RID: 1318 RVA: 0x000132E8 File Offset: 0x000114E8
		public MethodInfo GetMethod(string name, BindingFlags bindingAttr)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			return this.GetMethodImpl(name, bindingAttr, null, CallingConventions.Any, null, null);
		}

		// Token: 0x06000527 RID: 1319 RVA: 0x00013308 File Offset: 0x00011508
		public MethodInfo GetMethod(string name, Type[] types)
		{
			return this.GetMethod(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public, null, CallingConventions.Any, types, null);
		}

		// Token: 0x06000528 RID: 1320 RVA: 0x00013318 File Offset: 0x00011518
		public MethodInfo GetMethod(string name, Type[] types, ParameterModifier[] modifiers)
		{
			return this.GetMethod(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public, null, CallingConventions.Any, types, modifiers);
		}

		// Token: 0x06000529 RID: 1321 RVA: 0x00013328 File Offset: 0x00011528
		public MethodInfo GetMethod(string name, BindingFlags bindingAttr, Binder binder, Type[] types, ParameterModifier[] modifiers)
		{
			return this.GetMethod(name, bindingAttr, binder, CallingConventions.Any, types, modifiers);
		}

		// Token: 0x0600052A RID: 1322 RVA: 0x00013338 File Offset: 0x00011538
		public MethodInfo GetMethod(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (types == null)
			{
				throw new ArgumentNullException("types");
			}
			for (int i = 0; i < types.Length; i++)
			{
				if (types[i] == null)
				{
					throw new ArgumentNullException("types");
				}
			}
			return this.GetMethodImpl(name, bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x0600052B RID: 1323
		protected abstract MethodInfo GetMethodImpl(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x0600052C RID: 1324 RVA: 0x000133A0 File Offset: 0x000115A0
		internal MethodInfo GetMethodImplInternal(string name, BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			return this.GetMethodImpl(name, bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x0600052D RID: 1325 RVA: 0x000133B4 File Offset: 0x000115B4
		internal virtual MethodInfo GetMethod(MethodInfo fromNoninstanciated)
		{
			throw new InvalidOperationException("can only be called in generic type");
		}

		// Token: 0x0600052E RID: 1326 RVA: 0x000133C0 File Offset: 0x000115C0
		internal virtual ConstructorInfo GetConstructor(ConstructorInfo fromNoninstanciated)
		{
			throw new InvalidOperationException("can only be called in generic type");
		}

		// Token: 0x0600052F RID: 1327 RVA: 0x000133CC File Offset: 0x000115CC
		internal virtual FieldInfo GetField(FieldInfo fromNoninstanciated)
		{
			throw new InvalidOperationException("can only be called in generic type");
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x000133D8 File Offset: 0x000115D8
		public MethodInfo[] GetMethods()
		{
			return this.GetMethods(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x06000531 RID: 1329
		public abstract MethodInfo[] GetMethods(BindingFlags bindingAttr);

		// Token: 0x06000532 RID: 1330 RVA: 0x000133E4 File Offset: 0x000115E4
		public Type GetNestedType(string name)
		{
			return this.GetNestedType(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x06000533 RID: 1331
		public abstract Type GetNestedType(string name, BindingFlags bindingAttr);

		// Token: 0x06000534 RID: 1332 RVA: 0x000133F0 File Offset: 0x000115F0
		public Type[] GetNestedTypes()
		{
			return this.GetNestedTypes(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x06000535 RID: 1333
		public abstract Type[] GetNestedTypes(BindingFlags bindingAttr);

		// Token: 0x06000536 RID: 1334 RVA: 0x000133FC File Offset: 0x000115FC
		public PropertyInfo[] GetProperties()
		{
			return this.GetProperties(BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public);
		}

		// Token: 0x06000537 RID: 1335
		public abstract PropertyInfo[] GetProperties(BindingFlags bindingAttr);

		// Token: 0x06000538 RID: 1336 RVA: 0x00013408 File Offset: 0x00011608
		public PropertyInfo GetProperty(string name)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			return this.GetPropertyImpl(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public, null, null, null, null);
		}

		// Token: 0x06000539 RID: 1337 RVA: 0x00013428 File Offset: 0x00011628
		public PropertyInfo GetProperty(string name, BindingFlags bindingAttr)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			return this.GetPropertyImpl(name, bindingAttr, null, null, null, null);
		}

		// Token: 0x0600053A RID: 1338 RVA: 0x00013448 File Offset: 0x00011648
		public PropertyInfo GetProperty(string name, Type returnType)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			return this.GetPropertyImpl(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public, null, returnType, null, null);
		}

		// Token: 0x0600053B RID: 1339 RVA: 0x00013468 File Offset: 0x00011668
		public PropertyInfo GetProperty(string name, Type[] types)
		{
			return this.GetProperty(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public, null, null, types, null);
		}

		// Token: 0x0600053C RID: 1340 RVA: 0x00013478 File Offset: 0x00011678
		public PropertyInfo GetProperty(string name, Type returnType, Type[] types)
		{
			return this.GetProperty(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public, null, returnType, types, null);
		}

		// Token: 0x0600053D RID: 1341 RVA: 0x00013488 File Offset: 0x00011688
		public PropertyInfo GetProperty(string name, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			return this.GetProperty(name, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public, null, returnType, types, modifiers);
		}

		// Token: 0x0600053E RID: 1342 RVA: 0x00013498 File Offset: 0x00011698
		public PropertyInfo GetProperty(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			if (name == null)
			{
				throw new ArgumentNullException("name");
			}
			if (types == null)
			{
				throw new ArgumentNullException("types");
			}
			for (int i = 0; i < types.Length; i++)
			{
				if (types[i] == null)
				{
					throw new ArgumentNullException("types");
				}
			}
			return this.GetPropertyImpl(name, bindingAttr, binder, returnType, types, modifiers);
		}

		// Token: 0x0600053F RID: 1343
		protected abstract PropertyInfo GetPropertyImpl(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x06000540 RID: 1344 RVA: 0x00013504 File Offset: 0x00011704
		internal PropertyInfo GetPropertyImplInternal(string name, BindingFlags bindingAttr, Binder binder, Type returnType, Type[] types, ParameterModifier[] modifiers)
		{
			return this.GetPropertyImpl(name, bindingAttr, binder, returnType, types, modifiers);
		}

		// Token: 0x06000541 RID: 1345
		protected abstract ConstructorInfo GetConstructorImpl(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers);

		// Token: 0x06000542 RID: 1346
		protected abstract TypeAttributes GetAttributeFlagsImpl();

		// Token: 0x06000543 RID: 1347
		protected abstract bool HasElementTypeImpl();

		// Token: 0x06000544 RID: 1348
		protected abstract bool IsArrayImpl();

		// Token: 0x06000545 RID: 1349
		protected abstract bool IsByRefImpl();

		// Token: 0x06000546 RID: 1350
		protected abstract bool IsCOMObjectImpl();

		// Token: 0x06000547 RID: 1351
		protected abstract bool IsPointerImpl();

		// Token: 0x06000548 RID: 1352
		protected abstract bool IsPrimitiveImpl();

		// Token: 0x06000549 RID: 1353
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal static extern bool IsArrayImpl(Type type);

		// Token: 0x0600054A RID: 1354 RVA: 0x00013518 File Offset: 0x00011718
		protected virtual bool IsValueTypeImpl()
		{
			return this != typeof(ValueType) && this != typeof(Enum) && this.IsSubclassOf(typeof(ValueType));
		}

		// Token: 0x0600054B RID: 1355 RVA: 0x00013558 File Offset: 0x00011758
		protected virtual bool IsContextfulImpl()
		{
			return typeof(ContextBoundObject).IsAssignableFrom(this);
		}

		// Token: 0x0600054C RID: 1356 RVA: 0x0001356C File Offset: 0x0001176C
		protected virtual bool IsMarshalByRefImpl()
		{
			return typeof(MarshalByRefObject).IsAssignableFrom(this);
		}

		// Token: 0x0600054D RID: 1357 RVA: 0x00013580 File Offset: 0x00011780
		[ComVisible(true)]
		public ConstructorInfo GetConstructor(Type[] types)
		{
			return this.GetConstructor(BindingFlags.Instance | BindingFlags.Public, null, CallingConventions.Any, types, null);
		}

		// Token: 0x0600054E RID: 1358 RVA: 0x00013590 File Offset: 0x00011790
		[ComVisible(true)]
		public ConstructorInfo GetConstructor(BindingFlags bindingAttr, Binder binder, Type[] types, ParameterModifier[] modifiers)
		{
			return this.GetConstructor(bindingAttr, binder, CallingConventions.Any, types, modifiers);
		}

		// Token: 0x0600054F RID: 1359 RVA: 0x000135A0 File Offset: 0x000117A0
		[ComVisible(true)]
		public ConstructorInfo GetConstructor(BindingFlags bindingAttr, Binder binder, CallingConventions callConvention, Type[] types, ParameterModifier[] modifiers)
		{
			if (types == null)
			{
				throw new ArgumentNullException("types");
			}
			for (int i = 0; i < types.Length; i++)
			{
				if (types[i] == null)
				{
					throw new ArgumentNullException("types");
				}
			}
			return this.GetConstructorImpl(bindingAttr, binder, callConvention, types, modifiers);
		}

		// Token: 0x06000550 RID: 1360 RVA: 0x000135F8 File Offset: 0x000117F8
		[ComVisible(true)]
		public ConstructorInfo[] GetConstructors()
		{
			return this.GetConstructors(BindingFlags.Instance | BindingFlags.Public);
		}

		// Token: 0x06000551 RID: 1361
		[ComVisible(true)]
		public abstract ConstructorInfo[] GetConstructors(BindingFlags bindingAttr);

		// Token: 0x06000552 RID: 1362 RVA: 0x00013604 File Offset: 0x00011804
		public virtual MemberInfo[] GetDefaultMembers()
		{
			object[] customAttributes = this.GetCustomAttributes(typeof(DefaultMemberAttribute), true);
			if (customAttributes.Length == 0)
			{
				return new MemberInfo[0];
			}
			MemberInfo[] member = this.GetMember(((DefaultMemberAttribute)customAttributes[0]).MemberName);
			return (member == null) ? new MemberInfo[0] : member;
		}

		// Token: 0x06000553 RID: 1363 RVA: 0x00013658 File Offset: 0x00011858
		public virtual MemberInfo[] FindMembers(MemberTypes memberType, BindingFlags bindingAttr, MemberFilter filter, object filterCriteria)
		{
			ArrayList arrayList = new ArrayList();
			if ((memberType & MemberTypes.Method) != (MemberTypes)0)
			{
				MethodInfo[] methods = this.GetMethods(bindingAttr);
				if (filter != null)
				{
					foreach (MethodInfo memberInfo in methods)
					{
						if (filter(memberInfo, filterCriteria))
						{
							arrayList.Add(memberInfo);
						}
					}
				}
				else
				{
					arrayList.AddRange(methods);
				}
			}
			if ((memberType & MemberTypes.Constructor) != (MemberTypes)0)
			{
				ConstructorInfo[] constructors = this.GetConstructors(bindingAttr);
				if (filter != null)
				{
					foreach (ConstructorInfo memberInfo2 in constructors)
					{
						if (filter(memberInfo2, filterCriteria))
						{
							arrayList.Add(memberInfo2);
						}
					}
				}
				else
				{
					arrayList.AddRange(constructors);
				}
			}
			if ((memberType & MemberTypes.Property) != (MemberTypes)0)
			{
				int count = arrayList.Count;
				if (filter != null)
				{
					Type type = this;
					while (arrayList.Count == count && type != null)
					{
						PropertyInfo[] properties = type.GetProperties(bindingAttr);
						foreach (PropertyInfo memberInfo3 in properties)
						{
							if (filter(memberInfo3, filterCriteria))
							{
								arrayList.Add(memberInfo3);
							}
						}
						type = type.BaseType;
					}
				}
				else
				{
					PropertyInfo[] properties = this.GetProperties(bindingAttr);
					arrayList.AddRange(properties);
				}
			}
			if ((memberType & MemberTypes.Event) != (MemberTypes)0)
			{
				EventInfo[] events = this.GetEvents(bindingAttr);
				if (filter != null)
				{
					foreach (EventInfo memberInfo4 in events)
					{
						if (filter(memberInfo4, filterCriteria))
						{
							arrayList.Add(memberInfo4);
						}
					}
				}
				else
				{
					arrayList.AddRange(events);
				}
			}
			if ((memberType & MemberTypes.Field) != (MemberTypes)0)
			{
				FieldInfo[] fields = this.GetFields(bindingAttr);
				if (filter != null)
				{
					foreach (FieldInfo memberInfo5 in fields)
					{
						if (filter(memberInfo5, filterCriteria))
						{
							arrayList.Add(memberInfo5);
						}
					}
				}
				else
				{
					arrayList.AddRange(fields);
				}
			}
			if ((memberType & MemberTypes.NestedType) != (MemberTypes)0)
			{
				Type[] nestedTypes = this.GetNestedTypes(bindingAttr);
				if (filter != null)
				{
					foreach (Type memberInfo6 in nestedTypes)
					{
						if (filter(memberInfo6, filterCriteria))
						{
							arrayList.Add(memberInfo6);
						}
					}
				}
				else
				{
					arrayList.AddRange(nestedTypes);
				}
			}
			MemberInfo[] array7;
			switch (memberType)
			{
			case MemberTypes.Constructor:
				array7 = new ConstructorInfo[arrayList.Count];
				break;
			case MemberTypes.Event:
				array7 = new EventInfo[arrayList.Count];
				break;
			default:
				if (memberType != MemberTypes.Property)
				{
					if (memberType != MemberTypes.TypeInfo && memberType != MemberTypes.NestedType)
					{
						array7 = new MemberInfo[arrayList.Count];
					}
					else
					{
						array7 = new Type[arrayList.Count];
					}
				}
				else
				{
					array7 = new PropertyInfo[arrayList.Count];
				}
				break;
			case MemberTypes.Field:
				array7 = new FieldInfo[arrayList.Count];
				break;
			case MemberTypes.Method:
				array7 = new MethodInfo[arrayList.Count];
				break;
			}
			arrayList.CopyTo(array7);
			return array7;
		}

		// Token: 0x06000554 RID: 1364 RVA: 0x000139B0 File Offset: 0x00011BB0
		[DebuggerStepThrough]
		[DebuggerHidden]
		public object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args)
		{
			return this.InvokeMember(name, invokeAttr, binder, target, args, null, null, null);
		}

		// Token: 0x06000555 RID: 1365 RVA: 0x000139D0 File Offset: 0x00011BD0
		[DebuggerStepThrough]
		[DebuggerHidden]
		public object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, CultureInfo culture)
		{
			return this.InvokeMember(name, invokeAttr, binder, target, args, null, culture, null);
		}

		// Token: 0x06000556 RID: 1366
		public abstract object InvokeMember(string name, BindingFlags invokeAttr, Binder binder, object target, object[] args, ParameterModifier[] modifiers, CultureInfo culture, string[] namedParameters);

		// Token: 0x06000557 RID: 1367 RVA: 0x000139F0 File Offset: 0x00011BF0
		public override string ToString()
		{
			return this.FullName;
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000558 RID: 1368 RVA: 0x000139F8 File Offset: 0x00011BF8
		internal bool IsSystemType
		{
			get
			{
				return this._impl.Value != IntPtr.Zero;
			}
		}

		// Token: 0x06000559 RID: 1369 RVA: 0x00013A10 File Offset: 0x00011C10
		public virtual Type[] GetGenericArguments()
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600055A RID: 1370 RVA: 0x00013A18 File Offset: 0x00011C18
		public virtual bool ContainsGenericParameters
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600055B RID: 1371
		public virtual extern bool IsGenericTypeDefinition { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600055C RID: 1372
		[MethodImpl(MethodImplOptions.InternalCall)]
		internal extern Type GetGenericTypeDefinition_impl();

		// Token: 0x0600055D RID: 1373 RVA: 0x00013A1C File Offset: 0x00011C1C
		public virtual Type GetGenericTypeDefinition()
		{
			throw new NotSupportedException("Derived classes must provide an implementation.");
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600055E RID: 1374
		public virtual extern bool IsGenericType { [MethodImpl(MethodImplOptions.InternalCall)] get; }

		// Token: 0x0600055F RID: 1375
		[MethodImpl(MethodImplOptions.InternalCall)]
		private static extern Type MakeGenericType(Type gt, Type[] types);

		// Token: 0x06000560 RID: 1376 RVA: 0x00013A28 File Offset: 0x00011C28
		public virtual Type MakeGenericType(params Type[] typeArguments)
		{
			if (!this.IsGenericTypeDefinition)
			{
				throw new InvalidOperationException("not a generic type definition");
			}
			if (typeArguments == null)
			{
				throw new ArgumentNullException("typeArguments");
			}
			if (this.GetGenericArguments().Length != typeArguments.Length)
			{
				throw new ArgumentException(string.Format("The type or method has {0} generic parameter(s) but {1} generic argument(s) where provided. A generic argument must be provided for each generic parameter.", this.GetGenericArguments().Length, typeArguments.Length), "typeArguments");
			}
			Type[] array = new Type[typeArguments.Length];
			for (int i = 0; i < typeArguments.Length; i++)
			{
				Type type = typeArguments[i];
				if (type == null)
				{
					throw new ArgumentNullException("typeArguments");
				}
				if (!(type is EnumBuilder) && !(type is TypeBuilder))
				{
					type = type.UnderlyingSystemType;
				}
				if (type == null || !type.IsSystemType)
				{
					throw new ArgumentNullException("typeArguments");
				}
				array[i] = type;
			}
			Type type2 = Type.MakeGenericType(this, array);
			if (type2 == null)
			{
				throw new TypeLoadException();
			}
			return type2;
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000561 RID: 1377 RVA: 0x00013B1C File Offset: 0x00011D1C
		public virtual bool IsGenericParameter
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000562 RID: 1378 RVA: 0x00013B20 File Offset: 0x00011D20
		public bool IsNested
		{
			get
			{
				return this.DeclaringType != null;
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000563 RID: 1379 RVA: 0x00013B30 File Offset: 0x00011D30
		public bool IsVisible
		{
			get
			{
				if (this.IsNestedPublic)
				{
					return this.DeclaringType.IsVisible;
				}
				return this.IsPublic;
			}
		}

		// Token: 0x06000564 RID: 1380
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern int GetGenericParameterPosition();

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000565 RID: 1381 RVA: 0x00013B5C File Offset: 0x00011D5C
		public virtual int GenericParameterPosition
		{
			get
			{
				int genericParameterPosition = this.GetGenericParameterPosition();
				if (genericParameterPosition < 0)
				{
					throw new InvalidOperationException();
				}
				return genericParameterPosition;
			}
		}

		// Token: 0x06000566 RID: 1382
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern GenericParameterAttributes GetGenericParameterAttributes();

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000567 RID: 1383 RVA: 0x00013B80 File Offset: 0x00011D80
		public virtual GenericParameterAttributes GenericParameterAttributes
		{
			get
			{
				if (!this.IsGenericParameter)
				{
					throw new InvalidOperationException();
				}
				return this.GetGenericParameterAttributes();
			}
		}

		// Token: 0x06000568 RID: 1384
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type[] GetGenericParameterConstraints_impl();

		// Token: 0x06000569 RID: 1385 RVA: 0x00013B9C File Offset: 0x00011D9C
		public virtual Type[] GetGenericParameterConstraints()
		{
			if (!this.IsGenericParameter)
			{
				throw new InvalidOperationException();
			}
			return this.GetGenericParameterConstraints_impl();
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600056A RID: 1386 RVA: 0x00013BB8 File Offset: 0x00011DB8
		public virtual MethodBase DeclaringMethod
		{
			get
			{
				return null;
			}
		}

		// Token: 0x0600056B RID: 1387
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type make_array_type(int rank);

		// Token: 0x0600056C RID: 1388 RVA: 0x00013BBC File Offset: 0x00011DBC
		public virtual Type MakeArrayType()
		{
			return this.make_array_type(0);
		}

		// Token: 0x0600056D RID: 1389 RVA: 0x00013BC8 File Offset: 0x00011DC8
		public virtual Type MakeArrayType(int rank)
		{
			if (rank < 1)
			{
				throw new IndexOutOfRangeException();
			}
			return this.make_array_type(rank);
		}

		// Token: 0x0600056E RID: 1390
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern Type make_byref_type();

		// Token: 0x0600056F RID: 1391 RVA: 0x00013BE0 File Offset: 0x00011DE0
		public virtual Type MakeByRefType()
		{
			return this.make_byref_type();
		}

		// Token: 0x06000570 RID: 1392
		[MethodImpl(MethodImplOptions.InternalCall)]
		public virtual extern Type MakePointerType();

		// Token: 0x06000571 RID: 1393 RVA: 0x00013BE8 File Offset: 0x00011DE8
		public static Type ReflectionOnlyGetType(string typeName, bool throwIfNotFound, bool ignoreCase)
		{
			if (typeName == null)
			{
				throw new ArgumentNullException("typeName");
			}
			int num = typeName.IndexOf(',');
			if (num < 0 || num == 0 || num == typeName.Length - 1)
			{
				throw new ArgumentException("Assembly qualifed type name is required", "typeName");
			}
			string assemblyString = typeName.Substring(num + 1);
			Assembly assembly;
			try
			{
				assembly = Assembly.ReflectionOnlyLoad(assemblyString);
			}
			catch
			{
				if (throwIfNotFound)
				{
					throw;
				}
				return null;
			}
			return assembly.GetType(typeName.Substring(0, num), throwIfNotFound, ignoreCase);
		}

		// Token: 0x06000572 RID: 1394
		[MethodImpl(MethodImplOptions.InternalCall)]
		private extern void GetPacking(out int packing, out int size);

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000573 RID: 1395 RVA: 0x00013C94 File Offset: 0x00011E94
		public virtual StructLayoutAttribute StructLayoutAttribute
		{
			get
			{
				LayoutKind layoutKind;
				if (this.IsLayoutSequential)
				{
					layoutKind = LayoutKind.Sequential;
				}
				else if (this.IsExplicitLayout)
				{
					layoutKind = LayoutKind.Explicit;
				}
				else
				{
					layoutKind = LayoutKind.Auto;
				}
				StructLayoutAttribute structLayoutAttribute = new StructLayoutAttribute(layoutKind);
				if (this.IsUnicodeClass)
				{
					structLayoutAttribute.CharSet = CharSet.Unicode;
				}
				else if (this.IsAnsiClass)
				{
					structLayoutAttribute.CharSet = CharSet.Ansi;
				}
				else
				{
					structLayoutAttribute.CharSet = CharSet.Auto;
				}
				if (layoutKind != LayoutKind.Auto)
				{
					this.GetPacking(out structLayoutAttribute.Pack, out structLayoutAttribute.Size);
				}
				return structLayoutAttribute;
			}
		}

		// Token: 0x06000574 RID: 1396 RVA: 0x00013D20 File Offset: 0x00011F20
		internal object[] GetPseudoCustomAttributes()
		{
			int num = 0;
			if ((this.Attributes & TypeAttributes.Serializable) != TypeAttributes.NotPublic)
			{
				num++;
			}
			if ((this.Attributes & TypeAttributes.Import) != TypeAttributes.NotPublic)
			{
				num++;
			}
			if (num == 0)
			{
				return null;
			}
			object[] array = new object[num];
			num = 0;
			if ((this.Attributes & TypeAttributes.Serializable) != TypeAttributes.NotPublic)
			{
				array[num++] = new SerializableAttribute();
			}
			if ((this.Attributes & TypeAttributes.Import) != TypeAttributes.NotPublic)
			{
				array[num++] = new ComImportAttribute();
			}
			return array;
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000575 RID: 1397 RVA: 0x00013DA8 File Offset: 0x00011FA8
		internal bool IsUserType
		{
			get
			{
				return this._impl.Value == IntPtr.Zero && (this.GetType().Assembly != typeof(Type).Assembly || this.GetType() == typeof(TypeDelegator));
			}
		}

		// Token: 0x0400006E RID: 110
		internal const BindingFlags DefaultBindingFlags = BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public;

		// Token: 0x0400006F RID: 111
		internal RuntimeTypeHandle _impl;

		// Token: 0x04000070 RID: 112
		public static readonly char Delimiter = '.';

		// Token: 0x04000071 RID: 113
		public static readonly Type[] EmptyTypes = new Type[0];

		// Token: 0x04000072 RID: 114
		public static readonly MemberFilter FilterAttribute = new MemberFilter(Type.FilterAttribute_impl);

		// Token: 0x04000073 RID: 115
		public static readonly MemberFilter FilterName = new MemberFilter(Type.FilterName_impl);

		// Token: 0x04000074 RID: 116
		public static readonly MemberFilter FilterNameIgnoreCase = new MemberFilter(Type.FilterNameIgnoreCase_impl);

		// Token: 0x04000075 RID: 117
		public static readonly object Missing = System.Reflection.Missing.Value;
	}
}
