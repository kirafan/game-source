﻿using System;
using System.Text;

namespace System
{
	// Token: 0x02000109 RID: 265
	public static class BitConverter
	{
		// Token: 0x06000D98 RID: 3480 RVA: 0x0003B174 File Offset: 0x00039374
		private static bool AmILittleEndian()
		{
			double num = 1.0;
			return num == (double)0;
		}

		// Token: 0x06000D99 RID: 3481 RVA: 0x0003B194 File Offset: 0x00039394
		private unsafe static bool DoubleWordsAreSwapped()
		{
			double num = 1.0;
			return *(ref num + 2) == 240;
		}

		// Token: 0x06000D9A RID: 3482 RVA: 0x0003B1BC File Offset: 0x000393BC
		public static long DoubleToInt64Bits(double value)
		{
			return BitConverter.ToInt64(BitConverter.GetBytes(value), 0);
		}

		// Token: 0x06000D9B RID: 3483 RVA: 0x0003B1CC File Offset: 0x000393CC
		public static double Int64BitsToDouble(long value)
		{
			return BitConverter.ToDouble(BitConverter.GetBytes(value), 0);
		}

		// Token: 0x06000D9C RID: 3484 RVA: 0x0003B1DC File Offset: 0x000393DC
		internal static double InternalInt64BitsToDouble(long value)
		{
			return BitConverter.SwappableToDouble(BitConverter.GetBytes(value), 0);
		}

		// Token: 0x06000D9D RID: 3485 RVA: 0x0003B1EC File Offset: 0x000393EC
		private unsafe static byte[] GetBytes(byte* ptr, int count)
		{
			byte[] array = new byte[count];
			for (int i = 0; i < count; i++)
			{
				array[i] = ptr[i];
			}
			return array;
		}

		// Token: 0x06000D9E RID: 3486 RVA: 0x0003B21C File Offset: 0x0003941C
		public unsafe static byte[] GetBytes(bool value)
		{
			return BitConverter.GetBytes((byte*)(&value), 1);
		}

		// Token: 0x06000D9F RID: 3487 RVA: 0x0003B228 File Offset: 0x00039428
		public unsafe static byte[] GetBytes(char value)
		{
			return BitConverter.GetBytes((byte*)(&value), 2);
		}

		// Token: 0x06000DA0 RID: 3488 RVA: 0x0003B234 File Offset: 0x00039434
		public unsafe static byte[] GetBytes(short value)
		{
			return BitConverter.GetBytes((byte*)(&value), 2);
		}

		// Token: 0x06000DA1 RID: 3489 RVA: 0x0003B240 File Offset: 0x00039440
		public unsafe static byte[] GetBytes(int value)
		{
			return BitConverter.GetBytes((byte*)(&value), 4);
		}

		// Token: 0x06000DA2 RID: 3490 RVA: 0x0003B24C File Offset: 0x0003944C
		public unsafe static byte[] GetBytes(long value)
		{
			return BitConverter.GetBytes((byte*)(&value), 8);
		}

		// Token: 0x06000DA3 RID: 3491 RVA: 0x0003B258 File Offset: 0x00039458
		[CLSCompliant(false)]
		public unsafe static byte[] GetBytes(ushort value)
		{
			return BitConverter.GetBytes((byte*)(&value), 2);
		}

		// Token: 0x06000DA4 RID: 3492 RVA: 0x0003B264 File Offset: 0x00039464
		[CLSCompliant(false)]
		public unsafe static byte[] GetBytes(uint value)
		{
			return BitConverter.GetBytes((byte*)(&value), 4);
		}

		// Token: 0x06000DA5 RID: 3493 RVA: 0x0003B270 File Offset: 0x00039470
		[CLSCompliant(false)]
		public unsafe static byte[] GetBytes(ulong value)
		{
			return BitConverter.GetBytes((byte*)(&value), 8);
		}

		// Token: 0x06000DA6 RID: 3494 RVA: 0x0003B27C File Offset: 0x0003947C
		public unsafe static byte[] GetBytes(float value)
		{
			return BitConverter.GetBytes((byte*)(&value), 4);
		}

		// Token: 0x06000DA7 RID: 3495 RVA: 0x0003B288 File Offset: 0x00039488
		public unsafe static byte[] GetBytes(double value)
		{
			if (BitConverter.SwappedWordsInDouble)
			{
				return new byte[]
				{
					*(ref value + 4),
					*(ref value + 5),
					*(ref value + 6),
					*(ref value + 7),
					(byte)value,
					*(ref value + 1),
					*(ref value + 2),
					*(ref value + 3)
				};
			}
			return BitConverter.GetBytes((byte*)(&value), 8);
		}

		// Token: 0x06000DA8 RID: 3496 RVA: 0x0003B2EC File Offset: 0x000394EC
		private unsafe static void PutBytes(byte* dst, byte[] src, int start_index, int count)
		{
			if (src == null)
			{
				throw new ArgumentNullException("value");
			}
			if (start_index < 0 || start_index > src.Length - 1)
			{
				throw new ArgumentOutOfRangeException("startIndex", "Index was out of range. Must be non-negative and less than the size of the collection.");
			}
			if (src.Length - count < start_index)
			{
				throw new ArgumentException("Destination array is not long enough to copy all the items in the collection. Check array index and length.");
			}
			for (int i = 0; i < count; i++)
			{
				dst[i] = src[i + start_index];
			}
		}

		// Token: 0x06000DA9 RID: 3497 RVA: 0x0003B360 File Offset: 0x00039560
		public static bool ToBoolean(byte[] value, int startIndex)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (startIndex < 0 || startIndex > value.Length - 1)
			{
				throw new ArgumentOutOfRangeException("startIndex", "Index was out of range. Must be non-negative and less than the size of the collection.");
			}
			return value[startIndex] != 0;
		}

		// Token: 0x06000DAA RID: 3498 RVA: 0x0003B3AC File Offset: 0x000395AC
		public unsafe static char ToChar(byte[] value, int startIndex)
		{
			char result;
			BitConverter.PutBytes((byte*)(&result), value, startIndex, 2);
			return result;
		}

		// Token: 0x06000DAB RID: 3499 RVA: 0x0003B3C4 File Offset: 0x000395C4
		public unsafe static short ToInt16(byte[] value, int startIndex)
		{
			short result;
			BitConverter.PutBytes((byte*)(&result), value, startIndex, 2);
			return result;
		}

		// Token: 0x06000DAC RID: 3500 RVA: 0x0003B3DC File Offset: 0x000395DC
		public unsafe static int ToInt32(byte[] value, int startIndex)
		{
			int result;
			BitConverter.PutBytes((byte*)(&result), value, startIndex, 4);
			return result;
		}

		// Token: 0x06000DAD RID: 3501 RVA: 0x0003B3F4 File Offset: 0x000395F4
		public unsafe static long ToInt64(byte[] value, int startIndex)
		{
			long result;
			BitConverter.PutBytes((byte*)(&result), value, startIndex, 8);
			return result;
		}

		// Token: 0x06000DAE RID: 3502 RVA: 0x0003B40C File Offset: 0x0003960C
		[CLSCompliant(false)]
		public unsafe static ushort ToUInt16(byte[] value, int startIndex)
		{
			ushort result;
			BitConverter.PutBytes((byte*)(&result), value, startIndex, 2);
			return result;
		}

		// Token: 0x06000DAF RID: 3503 RVA: 0x0003B424 File Offset: 0x00039624
		[CLSCompliant(false)]
		public unsafe static uint ToUInt32(byte[] value, int startIndex)
		{
			uint result;
			BitConverter.PutBytes((byte*)(&result), value, startIndex, 4);
			return result;
		}

		// Token: 0x06000DB0 RID: 3504 RVA: 0x0003B43C File Offset: 0x0003963C
		[CLSCompliant(false)]
		public unsafe static ulong ToUInt64(byte[] value, int startIndex)
		{
			ulong result;
			BitConverter.PutBytes((byte*)(&result), value, startIndex, 8);
			return result;
		}

		// Token: 0x06000DB1 RID: 3505 RVA: 0x0003B454 File Offset: 0x00039654
		public unsafe static float ToSingle(byte[] value, int startIndex)
		{
			float result;
			BitConverter.PutBytes((byte*)(&result), value, startIndex, 4);
			return result;
		}

		// Token: 0x06000DB2 RID: 3506 RVA: 0x0003B46C File Offset: 0x0003966C
		public unsafe static double ToDouble(byte[] value, int startIndex)
		{
			double result;
			if (!BitConverter.SwappedWordsInDouble)
			{
				BitConverter.PutBytes((byte*)(&result), value, startIndex, 8);
				return result;
			}
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (startIndex < 0 || startIndex > value.Length - 1)
			{
				throw new ArgumentOutOfRangeException("startIndex", "Index was out of range. Must be non-negative and less than the size of the collection.");
			}
			if (value.Length - 8 < startIndex)
			{
				throw new ArgumentException("Destination array is not long enough to copy all the items in the collection. Check array index and length.");
			}
			result = (double)value[startIndex + 4];
			*(ref result + 1) = value[startIndex + 5];
			*(ref result + 2) = value[startIndex + 6];
			*(ref result + 3) = value[startIndex + 7];
			*(ref result + 4) = value[startIndex];
			*(ref result + 5) = value[startIndex + 1];
			*(ref result + 6) = value[startIndex + 2];
			*(ref result + 7) = value[startIndex + 3];
			return result;
		}

		// Token: 0x06000DB3 RID: 3507 RVA: 0x0003B520 File Offset: 0x00039720
		internal unsafe static double SwappableToDouble(byte[] value, int startIndex)
		{
			if (BitConverter.SwappedWordsInDouble)
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (startIndex < 0 || startIndex > value.Length - 1)
				{
					throw new ArgumentOutOfRangeException("startIndex", "Index was out of range. Must be non-negative and less than the size of the collection.");
				}
				if (value.Length - 8 < startIndex)
				{
					throw new ArgumentException("Destination array is not long enough to copy all the items in the collection. Check array index and length.");
				}
				double result = (double)value[startIndex + 4];
				*(ref result + 1) = value[startIndex + 5];
				*(ref result + 2) = value[startIndex + 6];
				*(ref result + 3) = value[startIndex + 7];
				*(ref result + 4) = value[startIndex];
				*(ref result + 5) = value[startIndex + 1];
				*(ref result + 6) = value[startIndex + 2];
				*(ref result + 7) = value[startIndex + 3];
				return result;
			}
			else
			{
				double result;
				if (BitConverter.IsLittleEndian)
				{
					BitConverter.PutBytes((byte*)(&result), value, startIndex, 8);
					return result;
				}
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				if (startIndex < 0 || startIndex > value.Length - 1)
				{
					throw new ArgumentOutOfRangeException("startIndex", "Index was out of range. Must be non-negative and less than the size of the collection.");
				}
				if (value.Length - 8 < startIndex)
				{
					throw new ArgumentException("Destination array is not long enough to copy all the items in the collection. Check array index and length.");
				}
				result = (double)value[startIndex + 7];
				*(ref result + 1) = value[startIndex + 6];
				*(ref result + 2) = value[startIndex + 5];
				*(ref result + 3) = value[startIndex + 4];
				*(ref result + 4) = value[startIndex + 3];
				*(ref result + 5) = value[startIndex + 2];
				*(ref result + 6) = value[startIndex + 1];
				*(ref result + 7) = value[startIndex];
				return result;
			}
		}

		// Token: 0x06000DB4 RID: 3508 RVA: 0x0003B670 File Offset: 0x00039870
		public static string ToString(byte[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			return BitConverter.ToString(value, 0, value.Length);
		}

		// Token: 0x06000DB5 RID: 3509 RVA: 0x0003B690 File Offset: 0x00039890
		public static string ToString(byte[] value, int startIndex)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			return BitConverter.ToString(value, startIndex, value.Length - startIndex);
		}

		// Token: 0x06000DB6 RID: 3510 RVA: 0x0003B6B0 File Offset: 0x000398B0
		public static string ToString(byte[] value, int startIndex, int length)
		{
			if (value == null)
			{
				throw new ArgumentNullException("byteArray");
			}
			if (startIndex < 0 || startIndex >= value.Length)
			{
				if (startIndex == 0 && value.Length == 0)
				{
					return string.Empty;
				}
				throw new ArgumentOutOfRangeException("startIndex", "Index was out of range. Must be non-negative and less than the size of the collection.");
			}
			else
			{
				if (length < 0)
				{
					throw new ArgumentOutOfRangeException("length", "Value must be positive.");
				}
				if (startIndex > value.Length - length)
				{
					throw new ArgumentException("startIndex + length > value.Length");
				}
				if (length == 0)
				{
					return string.Empty;
				}
				StringBuilder stringBuilder = new StringBuilder(length * 3 - 1);
				int num = startIndex + length;
				for (int i = startIndex; i < num; i++)
				{
					if (i > startIndex)
					{
						stringBuilder.Append('-');
					}
					char c = (char)(value[i] >> 4 & 15);
					char c2 = (char)(value[i] & 15);
					if (c < '\n')
					{
						c += '0';
					}
					else
					{
						c -= '\n';
						c += 'A';
					}
					if (c2 < '\n')
					{
						c2 += '0';
					}
					else
					{
						c2 -= '\n';
						c2 += 'A';
					}
					stringBuilder.Append(c);
					stringBuilder.Append(c2);
				}
				return stringBuilder.ToString();
			}
		}

		// Token: 0x040003BD RID: 957
		private static readonly bool SwappedWordsInDouble = BitConverter.DoubleWordsAreSwapped();

		// Token: 0x040003BE RID: 958
		public static readonly bool IsLittleEndian = BitConverter.AmILittleEndian();
	}
}
