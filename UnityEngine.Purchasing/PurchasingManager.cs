﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using SimpleJson;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000019 RID: 25
	internal class PurchasingManager : IStoreCallback, IStoreController
	{
		// Token: 0x0600007E RID: 126 RVA: 0x00003350 File Offset: 0x00001550
		internal PurchasingManager(TransactionLog tDb, ILogger logger, IStore store, string storeName)
		{
			this.m_TransactionLog = tDb;
			this.m_Store = store;
			this.m_Logger = logger;
			this.m_StoreName = storeName;
			this.useTransactionLog = true;
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600007F RID: 127 RVA: 0x00003380 File Offset: 0x00001580
		// (set) Token: 0x06000080 RID: 128 RVA: 0x0000339A File Offset: 0x0000159A
		public bool useTransactionLog { get; set; }

		// Token: 0x06000081 RID: 129 RVA: 0x000033A3 File Offset: 0x000015A3
		public void InitiatePurchase(Product product)
		{
			this.InitiatePurchase(product, string.Empty);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x000033B2 File Offset: 0x000015B2
		public void InitiatePurchase(string productId)
		{
			this.InitiatePurchase(productId, string.Empty);
		}

		// Token: 0x06000083 RID: 131 RVA: 0x000033C4 File Offset: 0x000015C4
		public void InitiatePurchase(Product product, string developerPayload)
		{
			if (product == null)
			{
				this.m_Logger.Log("Trying to purchase null Product");
			}
			else if (!product.availableToPurchase)
			{
				this.m_Listener.OnPurchaseFailed(product, PurchaseFailureReason.ProductUnavailable);
			}
			else
			{
				this.m_Store.Purchase(product.definition, developerPayload);
				this.m_Logger.Log("purchase({0})", product.definition.id);
			}
		}

		// Token: 0x06000084 RID: 132 RVA: 0x0000343C File Offset: 0x0000163C
		public void InitiatePurchase(string purchasableId, string developerPayload)
		{
			Product product = this.products.WithID(purchasableId);
			if (product == null)
			{
				this.m_Logger.LogWarning("Unable to purchase unknown product with id: {0}", purchasableId);
			}
			this.InitiatePurchase(product, developerPayload);
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00003478 File Offset: 0x00001678
		public void ConfirmPendingPurchase(Product product)
		{
			if (product == null)
			{
				this.m_Logger.Log("Unable to confirm purchase with null Product");
			}
			else if (string.IsNullOrEmpty(product.transactionID))
			{
				this.m_Logger.Log("Unable to confirm purchase; Product has missing or empty transactionID");
			}
			else
			{
				if (this.useTransactionLog)
				{
					this.m_TransactionLog.Record(product.transactionID);
				}
				this.m_Store.FinishTransaction(product.definition, product.transactionID);
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000086 RID: 134 RVA: 0x000034FC File Offset: 0x000016FC
		// (set) Token: 0x06000087 RID: 135 RVA: 0x00003516 File Offset: 0x00001716
		public ProductCollection products { get; private set; }

		// Token: 0x06000088 RID: 136 RVA: 0x00003520 File Offset: 0x00001720
		public void OnPurchaseSucceeded(string id, string receipt, string transactionId)
		{
			Product product = this.products.WithStoreSpecificID(id);
			if (product == null)
			{
				ProductDefinition definition = new ProductDefinition(id, ProductType.NonConsumable);
				product = new Product(definition, new ProductMetadata());
			}
			string receipt2 = this.FormatUnifiedReceipt(receipt, transactionId);
			product.receipt = receipt2;
			product.transactionID = transactionId;
			this.ProcessPurchaseIfNew(product);
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00003575 File Offset: 0x00001775
		public void OnSetupFailed(InitializationFailureReason reason)
		{
			if (this.initialized)
			{
				if (this.m_AdditionalProductsFailCallback != null)
				{
					this.m_AdditionalProductsFailCallback(reason);
				}
			}
			else
			{
				this.m_Listener.OnInitializeFailed(reason);
			}
		}

		// Token: 0x0600008A RID: 138 RVA: 0x000035B0 File Offset: 0x000017B0
		public void OnPurchaseFailed(PurchaseFailureDescription description)
		{
			Product product = this.products.WithStoreSpecificID(description.productId);
			if (product == null)
			{
				this.m_Logger.LogError("Failed to purchase unknown product {0}", description.productId);
			}
			else
			{
				this.m_Logger.Log("onPurchaseFailedEvent({0})", product.definition.id);
				this.m_Listener.OnPurchaseFailed(product, description.reason);
			}
		}

		// Token: 0x0600008B RID: 139 RVA: 0x00003620 File Offset: 0x00001820
		public void OnProductsRetrieved(List<ProductDescription> products)
		{
			HashSet<Product> hashSet = new HashSet<Product>();
			foreach (ProductDescription productDescription in products)
			{
				Product product = this.products.WithStoreSpecificID(productDescription.storeSpecificId);
				if (product == null)
				{
					ProductDefinition definition = new ProductDefinition(productDescription.storeSpecificId, productDescription.storeSpecificId, productDescription.type);
					product = new Product(definition, productDescription.metadata);
					hashSet.Add(product);
				}
				product.availableToPurchase = true;
				product.metadata = productDescription.metadata;
				product.transactionID = productDescription.transactionId;
				if (!string.IsNullOrEmpty(productDescription.receipt))
				{
					product.receipt = this.FormatUnifiedReceipt(productDescription.receipt, productDescription.transactionId);
				}
			}
			if (hashSet.Count > 0)
			{
				this.products.AddProducts(hashSet);
			}
			this.CheckForInitialization();
			foreach (Product product2 in this.products.set)
			{
				if (!string.IsNullOrEmpty(product2.receipt) && !string.IsNullOrEmpty(product2.transactionID))
				{
					this.ProcessPurchaseIfNew(product2);
				}
			}
		}

		// Token: 0x0600008C RID: 140 RVA: 0x000037A4 File Offset: 0x000019A4
		public void FetchAdditionalProducts(HashSet<ProductDefinition> products, Action successCallback, Action<InitializationFailureReason> failCallback)
		{
			this.m_AdditionalProductsCallback = successCallback;
			this.m_AdditionalProductsFailCallback = failCallback;
			this.products.AddProducts(from x in products
			select new Product(x, new ProductMetadata()));
			this.m_Store.RetrieveProducts(new ReadOnlyCollection<ProductDefinition>(products.ToList<ProductDefinition>()));
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00003804 File Offset: 0x00001A04
		private void ProcessPurchaseIfNew(Product product)
		{
			if (this.useTransactionLog && this.m_TransactionLog.HasRecordOf(product.transactionID))
			{
				this.m_Logger.Log("Already recorded transaction " + product.transactionID);
				this.m_Store.FinishTransaction(product.definition, product.transactionID);
			}
			else
			{
				PurchaseEventArgs e = new PurchaseEventArgs(product);
				if (this.m_Listener.ProcessPurchase(e) == PurchaseProcessingResult.Complete)
				{
					this.ConfirmPendingPurchase(product);
				}
			}
		}

		// Token: 0x0600008E RID: 142 RVA: 0x0000388C File Offset: 0x00001A8C
		private void CheckForInitialization()
		{
			if (!this.initialized)
			{
				bool flag = false;
				foreach (Product product in this.products.set)
				{
					if (!product.availableToPurchase)
					{
						this.m_Logger.LogFormat(LogType.Warning, "Unavailable product {0} -{1}", new object[]
						{
							product.definition.id,
							product.definition.storeSpecificId
						});
					}
					else
					{
						flag = true;
					}
				}
				if (flag)
				{
					this.m_Listener.OnInitialized(this);
				}
				else
				{
					this.OnSetupFailed(InitializationFailureReason.NoProductsAvailable);
				}
				this.initialized = true;
			}
			else if (this.m_AdditionalProductsCallback != null)
			{
				this.m_AdditionalProductsCallback();
			}
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00003980 File Offset: 0x00001B80
		public void Initialize(IInternalStoreListener listener, HashSet<ProductDefinition> products)
		{
			this.m_Listener = listener;
			this.m_Store.Initialize(this);
			Product[] products2 = (from x in products
			select new Product(x, new ProductMetadata())).ToArray<Product>();
			this.products = new ProductCollection(products2);
			ReadOnlyCollection<ProductDefinition> products3 = new ReadOnlyCollection<ProductDefinition>(products.ToList<ProductDefinition>());
			this.m_Store.RetrieveProducts(products3);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x000039F0 File Offset: 0x00001BF0
		private string FormatUnifiedReceipt(string platformReceipt, string transactionId)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["Store"] = this.m_StoreName;
			dictionary["TransactionID"] = (transactionId ?? string.Empty);
			dictionary["Payload"] = (platformReceipt ?? string.Empty);
			return SimpleJson.SerializeObject(dictionary);
		}

		// Token: 0x04000040 RID: 64
		private IStore m_Store;

		// Token: 0x04000041 RID: 65
		private IInternalStoreListener m_Listener;

		// Token: 0x04000042 RID: 66
		private ILogger m_Logger;

		// Token: 0x04000043 RID: 67
		private TransactionLog m_TransactionLog;

		// Token: 0x04000044 RID: 68
		private string m_StoreName;

		// Token: 0x04000045 RID: 69
		private Action m_AdditionalProductsCallback;

		// Token: 0x04000046 RID: 70
		private Action<InitializationFailureReason> m_AdditionalProductsFailCallback;

		// Token: 0x04000049 RID: 73
		private bool initialized;
	}
}
