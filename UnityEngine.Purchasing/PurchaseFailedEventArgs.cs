﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000014 RID: 20
	public class PurchaseFailedEventArgs
	{
		// Token: 0x06000066 RID: 102 RVA: 0x0000303B File Offset: 0x0000123B
		internal PurchaseFailedEventArgs(Product purchasedProduct, PurchaseFailureReason reason, string message)
		{
			this.purchasedProduct = purchasedProduct;
			this.reason = reason;
			this.message = message;
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000067 RID: 103 RVA: 0x0000305C File Offset: 0x0000125C
		// (set) Token: 0x06000068 RID: 104 RVA: 0x00003076 File Offset: 0x00001276
		public Product purchasedProduct { get; private set; }

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000069 RID: 105 RVA: 0x00003080 File Offset: 0x00001280
		// (set) Token: 0x0600006A RID: 106 RVA: 0x0000309A File Offset: 0x0000129A
		public PurchaseFailureReason reason { get; private set; }

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x0600006B RID: 107 RVA: 0x000030A4 File Offset: 0x000012A4
		// (set) Token: 0x0600006C RID: 108 RVA: 0x000030BE File Offset: 0x000012BE
		public string message { get; private set; }
	}
}
