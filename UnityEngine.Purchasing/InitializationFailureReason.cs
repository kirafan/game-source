﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000A RID: 10
	public enum InitializationFailureReason
	{
		// Token: 0x0400000F RID: 15
		PurchasingUnavailable,
		// Token: 0x04000010 RID: 16
		NoProductsAvailable,
		// Token: 0x04000011 RID: 17
		AppNotKnown
	}
}
