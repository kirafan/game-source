﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000016 RID: 22
	public enum PurchaseFailureReason
	{
		// Token: 0x04000031 RID: 49
		PurchasingUnavailable,
		// Token: 0x04000032 RID: 50
		ExistingPurchasePending,
		// Token: 0x04000033 RID: 51
		ProductUnavailable,
		// Token: 0x04000034 RID: 52
		SignatureInvalid,
		// Token: 0x04000035 RID: 53
		UserCancelled,
		// Token: 0x04000036 RID: 54
		PaymentDeclined,
		// Token: 0x04000037 RID: 55
		DuplicateTransaction,
		// Token: 0x04000038 RID: 56
		Unknown
	}
}
