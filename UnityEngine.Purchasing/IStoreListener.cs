﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000C RID: 12
	public interface IStoreListener
	{
		// Token: 0x0600002F RID: 47
		void OnInitializeFailed(InitializationFailureReason error);

		// Token: 0x06000030 RID: 48
		PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e);

		// Token: 0x06000031 RID: 49
		void OnPurchaseFailed(Product i, PurchaseFailureReason p);

		// Token: 0x06000032 RID: 50
		void OnInitialized(IStoreController controller, IExtensionProvider extensions);
	}
}
