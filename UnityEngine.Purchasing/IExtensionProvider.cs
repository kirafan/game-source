﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000008 RID: 8
	public interface IExtensionProvider
	{
		// Token: 0x06000023 RID: 35
		T GetExtension<T>() where T : IStoreExtension;
	}
}
