﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using SimpleJson;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000004 RID: 4
	internal class CloudCatalogManager
	{
		// Token: 0x06000009 RID: 9 RVA: 0x0000238D File Offset: 0x0000058D
		internal CloudCatalogManager(IAsyncUtil util, string cacheFile, ILogger logger, string catalogURL, string storeName)
		{
			this.m_AsyncUtil = util;
			this.m_CacheFileName = cacheFile;
			this.m_Logger = logger;
			this.m_CatalogURL = catalogURL;
			this.m_StoreName = storeName;
		}

		// Token: 0x0600000A RID: 10 RVA: 0x000023BC File Offset: 0x000005BC
		internal void FetchProducts(Action<HashSet<ProductDefinition>> callback, int delayInSeconds)
		{
			this.m_Logger.Log("Fetching IAP cloud catalog...");
			this.m_AsyncUtil.Get(this.m_CatalogURL, delegate(string response)
			{
				this.m_Logger.Log("Fetched catalog");
				try
				{
					HashSet<ProductDefinition> obj = CloudCatalogManager.ParseProductsFromJSON(response, this.m_StoreName);
					this.TryPersistCatalog(response);
					callback(obj);
				}
				catch (SerializationException message)
				{
					this.m_Logger.LogError("Error parsing IAP catalog", message);
					this.m_Logger.Log(response);
					callback(this.TryLoadCachedCatalog());
				}
			}, delegate(string error)
			{
				HashSet<ProductDefinition> hashSet = this.TryLoadCachedCatalog();
				if (hashSet.Count > 0)
				{
					this.m_Logger.Log("Failed to fetch IAP catalog, using cache.");
					callback(hashSet);
				}
				else
				{
					delayInSeconds = Math.Max(5, delayInSeconds * 2);
					delayInSeconds = Math.Min(300, delayInSeconds);
					this.m_AsyncUtil.Schedule(delegate
					{
						this.FetchProducts(callback, delayInSeconds);
					}, delayInSeconds);
				}
			});
		}

		// Token: 0x0600000B RID: 11 RVA: 0x00002420 File Offset: 0x00000620
		internal static HashSet<ProductDefinition> ParseProductsFromJSON(string json, string storeName)
		{
			HashSet<ProductDefinition> hashSet = new HashSet<ProductDefinition>();
			HashSet<ProductDefinition> result;
			try
			{
				JsonObject jsonObject = (JsonObject)SimpleJson.DeserializeObject(json);
				object obj;
				jsonObject.TryGetValue("products", out obj);
				string key = CloudCatalogManager.CamelCaseToSnakeCase(storeName);
				foreach (object obj2 in ((JsonArray)obj))
				{
					JsonObject jsonObject2 = (JsonObject)obj2;
					object obj3;
					jsonObject2.TryGetValue("id", out obj3);
					object obj4;
					jsonObject2.TryGetValue("store_ids", out obj4);
					object obj5;
					jsonObject2.TryGetValue("type", out obj5);
					JsonObject jsonObject3 = (JsonObject)obj4;
					string storeSpecificId = (string)obj3;
					if (jsonObject3 != null && jsonObject3.ContainsKey(key))
					{
						object obj6 = null;
						jsonObject3.TryGetValue(key, out obj6);
						if (obj6 != null)
						{
							storeSpecificId = (string)obj6;
						}
					}
					ProductType type = (ProductType)Enum.Parse(typeof(ProductType), (string)obj5);
					hashSet.Add(new ProductDefinition((string)obj3, storeSpecificId, type));
				}
				result = hashSet;
			}
			catch (Exception innerException)
			{
				throw new SerializationException("Error parsing JSON", innerException);
			}
			return result;
		}

		// Token: 0x0600000C RID: 12 RVA: 0x00002590 File Offset: 0x00000790
		internal static string CamelCaseToSnakeCase(string s)
		{
			IEnumerable<string> source = s.Select((char a, int b) => (!char.IsUpper(a) || b <= 0) ? char.ToLower(a).ToString() : ("_" + char.ToLower(a)));
			return source.Aggregate((string a, string b) => a + b);
		}

		// Token: 0x0600000D RID: 13 RVA: 0x000025EC File Offset: 0x000007EC
		private void TryPersistCatalog(string response)
		{
			if (this.m_CacheFileName != null)
			{
				try
				{
					File.WriteAllText(this.m_CacheFileName, response);
				}
				catch (Exception message)
				{
					this.m_Logger.LogError("Failed persisting IAP catalog", message);
				}
			}
		}

		// Token: 0x0600000E RID: 14 RVA: 0x00002648 File Offset: 0x00000848
		private HashSet<ProductDefinition> TryLoadCachedCatalog()
		{
			if (this.m_CacheFileName != null && File.Exists(this.m_CacheFileName))
			{
				try
				{
					string json = File.ReadAllText(this.m_CacheFileName);
					return CloudCatalogManager.ParseProductsFromJSON(json, this.m_StoreName);
				}
				catch (Exception message)
				{
					this.m_Logger.LogError("Error loading cached catalog", message);
				}
			}
			return new HashSet<ProductDefinition>();
		}

		// Token: 0x04000002 RID: 2
		private IAsyncUtil m_AsyncUtil;

		// Token: 0x04000003 RID: 3
		private string m_CacheFileName;

		// Token: 0x04000004 RID: 4
		private ILogger m_Logger;

		// Token: 0x04000005 RID: 5
		private string m_CatalogURL;

		// Token: 0x04000006 RID: 6
		private string m_StoreName;

		// Token: 0x04000007 RID: 7
		private const int kMaxRetryDelayInSeconds = 300;
	}
}
