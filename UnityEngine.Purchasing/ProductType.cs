﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000012 RID: 18
	public enum ProductType
	{
		// Token: 0x04000026 RID: 38
		Consumable,
		// Token: 0x04000027 RID: 39
		NonConsumable,
		// Token: 0x04000028 RID: 40
		Subscription
	}
}
