﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000013 RID: 19
	public class PurchaseEventArgs
	{
		// Token: 0x06000063 RID: 99 RVA: 0x00003007 File Offset: 0x00001207
		internal PurchaseEventArgs(Product purchasedProduct)
		{
			this.purchasedProduct = purchasedProduct;
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000064 RID: 100 RVA: 0x00003018 File Offset: 0x00001218
		// (set) Token: 0x06000065 RID: 101 RVA: 0x00003032 File Offset: 0x00001232
		public Product purchasedProduct { get; private set; }
	}
}
