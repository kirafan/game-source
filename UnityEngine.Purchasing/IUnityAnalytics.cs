﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000D RID: 13
	internal interface IUnityAnalytics
	{
		// Token: 0x06000033 RID: 51
		void Transaction(string productId, decimal price, string currency, string receipt, string signature);

		// Token: 0x06000034 RID: 52
		void CustomEvent(string name, Dictionary<string, object> data);
	}
}
