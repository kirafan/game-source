﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000B RID: 11
	public interface IStoreController
	{
		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000028 RID: 40
		ProductCollection products { get; }

		// Token: 0x06000029 RID: 41
		void InitiatePurchase(Product product, string payload);

		// Token: 0x0600002A RID: 42
		void InitiatePurchase(string productId, string payload);

		// Token: 0x0600002B RID: 43
		void InitiatePurchase(Product product);

		// Token: 0x0600002C RID: 44
		void InitiatePurchase(string productId);

		// Token: 0x0600002D RID: 45
		void FetchAdditionalProducts(HashSet<ProductDefinition> products, Action successCallback, Action<InitializationFailureReason> failCallback);

		// Token: 0x0600002E RID: 46
		void ConfirmPendingPurchase(Product product);
	}
}
