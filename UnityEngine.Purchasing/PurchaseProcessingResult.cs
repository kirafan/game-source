﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000017 RID: 23
	public enum PurchaseProcessingResult
	{
		// Token: 0x0400003A RID: 58
		Complete,
		// Token: 0x0400003B RID: 59
		Pending
	}
}
