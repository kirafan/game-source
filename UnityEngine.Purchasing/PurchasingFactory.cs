﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000018 RID: 24
	internal class PurchasingFactory : IPurchasingBinder, IExtensionProvider
	{
		// Token: 0x06000074 RID: 116 RVA: 0x00003154 File Offset: 0x00001354
		public PurchasingFactory(IPurchasingModule first, params IPurchasingModule[] remainingModules)
		{
			first.Configure(this);
			foreach (IPurchasingModule purchasingModule in remainingModules)
			{
				purchasingModule.Configure(this);
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x06000075 RID: 117 RVA: 0x000031A8 File Offset: 0x000013A8
		// (set) Token: 0x06000076 RID: 118 RVA: 0x000031C2 File Offset: 0x000013C2
		public string storeName { get; private set; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000077 RID: 119 RVA: 0x000031CC File Offset: 0x000013CC
		// (set) Token: 0x06000078 RID: 120 RVA: 0x000031FD File Offset: 0x000013FD
		public IStore service
		{
			get
			{
				if (this.m_Store != null)
				{
					return this.m_Store;
				}
				throw new InvalidOperationException("No impl available!");
			}
			set
			{
				this.m_Store = value;
			}
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00003207 File Offset: 0x00001407
		public void RegisterStore(string name, IStore s)
		{
			if (this.m_Store == null && s != null)
			{
				this.storeName = name;
				this.service = s;
			}
		}

		// Token: 0x0600007A RID: 122 RVA: 0x0000322B File Offset: 0x0000142B
		public void RegisterExtension<T>(T instance) where T : IStoreExtension
		{
			this.m_ExtensionMap[typeof(T)] = instance;
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00003249 File Offset: 0x00001449
		public void RegisterConfiguration<T>(T instance) where T : IStoreConfiguration
		{
			this.m_ConfigMap[typeof(T)] = instance;
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00003268 File Offset: 0x00001468
		public T GetConfig<T>() where T : IStoreConfiguration
		{
			T result;
			if (this.service is T)
			{
				result = (T)((object)this.service);
			}
			else
			{
				Type typeFromHandle = typeof(T);
				if (!this.m_ConfigMap.ContainsKey(typeFromHandle))
				{
					throw new ArgumentException("No binding for config type " + typeFromHandle);
				}
				result = (T)((object)this.m_ConfigMap[typeFromHandle]);
			}
			return result;
		}

		// Token: 0x0600007D RID: 125 RVA: 0x000032DC File Offset: 0x000014DC
		public T GetExtension<T>() where T : IStoreExtension
		{
			T result;
			if (this.service is T)
			{
				result = (T)((object)this.service);
			}
			else
			{
				Type typeFromHandle = typeof(T);
				if (!this.m_ExtensionMap.ContainsKey(typeFromHandle))
				{
					throw new ArgumentException("No binding for type " + typeFromHandle);
				}
				result = (T)((object)this.m_ExtensionMap[typeFromHandle]);
			}
			return result;
		}

		// Token: 0x0400003C RID: 60
		private Dictionary<Type, IStoreConfiguration> m_ConfigMap = new Dictionary<Type, IStoreConfiguration>();

		// Token: 0x0400003D RID: 61
		private Dictionary<Type, IStoreExtension> m_ExtensionMap = new Dictionary<Type, IStoreExtension>();

		// Token: 0x0400003E RID: 62
		private IStore m_Store;
	}
}
