﻿using System;
using System.Collections.Generic;
using System.IO;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001D RID: 29
	public abstract class UnityPurchasing
	{
		// Token: 0x060000A2 RID: 162 RVA: 0x00003D0F File Offset: 0x00001F0F
		public static void Initialize(IStoreListener listener, ConfigurationBuilder builder)
		{
			UnityPurchasing.Initialize(listener, builder, Debug.logger, Application.persistentDataPath, new UnityAnalytics(), UnityPurchasing.InstantiateCatalog(builder.factory.storeName));
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x00003D38 File Offset: 0x00001F38
		public static void ClearTransactionLog()
		{
			TransactionLog transactionLog = new TransactionLog(Debug.logger, Application.persistentDataPath);
			transactionLog.Clear();
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x00003D5C File Offset: 0x00001F5C
		internal static void Initialize(IStoreListener listener, ConfigurationBuilder builder, ILogger logger, string persistentDatapath, IUnityAnalytics analytics, CloudCatalogManager catalog)
		{
			TransactionLog tDb = new TransactionLog(logger, persistentDatapath);
			PurchasingManager manager = new PurchasingManager(tDb, logger, builder.factory.service, builder.factory.storeName);
			AnalyticsReporter analytics2 = new AnalyticsReporter(analytics);
			StoreListenerProxy proxy = new StoreListenerProxy(listener, analytics2, builder.factory);
			UnityPurchasing.FetchAndMergeProducts(builder.useCloudCatalog, builder.products, catalog, delegate(HashSet<ProductDefinition> response)
			{
				manager.Initialize(proxy, response);
			});
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x00003DD8 File Offset: 0x00001FD8
		internal static void FetchAndMergeProducts(bool useCatalog, HashSet<ProductDefinition> applicationProducts, CloudCatalogManager catalog, Action<HashSet<ProductDefinition>> callback)
		{
			if (useCatalog)
			{
				catalog.FetchProducts(delegate(HashSet<ProductDefinition> response)
				{
					response.UnionWith(applicationProducts);
					callback(response);
				}, 0);
			}
			else
			{
				callback(applicationProducts);
			}
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x00003E30 File Offset: 0x00002030
		private static CloudCatalogManager InstantiateCatalog(string storeName)
		{
			GameObject gameObject = new GameObject();
			Object.DontDestroyOnLoad(gameObject);
			gameObject.name = "Unity IAP";
			gameObject.hideFlags = (HideFlags.HideInHierarchy | HideFlags.HideInInspector);
			AsyncUtil util = gameObject.AddComponent<AsyncUtil>();
			string text = Path.Combine(Path.Combine(Application.persistentDataPath, "Unity"), Path.Combine(Application.cloudProjectId, "IAP"));
			string cacheFile = null;
			try
			{
				Directory.CreateDirectory(text);
				cacheFile = Path.Combine(text, "catalog.json");
			}
			catch (Exception message)
			{
				Debug.logger.LogError("Unable to cache IAP catalog", message);
			}
			string catalogURL = string.Format("{0}/{1}", "https://catalog.iap.cloud.unity3d.com", Application.cloudProjectId);
			return new CloudCatalogManager(util, cacheFile, Debug.logger, catalogURL, storeName);
		}

		// Token: 0x04000051 RID: 81
		private const string kCatalogURL = "https://catalog.iap.cloud.unity3d.com";
	}
}
