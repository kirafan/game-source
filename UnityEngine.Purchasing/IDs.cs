﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000005 RID: 5
	public class IDs : IEnumerable<KeyValuePair<string, string>>, IEnumerable
	{
		// Token: 0x06000012 RID: 18 RVA: 0x000028B4 File Offset: 0x00000AB4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.m_Dic.GetEnumerator();
		}

		// Token: 0x06000013 RID: 19 RVA: 0x000028DC File Offset: 0x00000ADC
		public void Add(string id, params string[] stores)
		{
			foreach (string key in stores)
			{
				this.m_Dic[key] = id;
			}
		}

		// Token: 0x06000014 RID: 20 RVA: 0x00002914 File Offset: 0x00000B14
		public void Add(string id, params object[] stores)
		{
			foreach (object obj in stores)
			{
				this.m_Dic[obj.ToString()] = id;
			}
		}

		// Token: 0x06000015 RID: 21 RVA: 0x00002950 File Offset: 0x00000B50
		internal string SpecificIDForStore(string store, string defaultValue)
		{
			string result;
			if (this.m_Dic.ContainsKey(store))
			{
				result = this.m_Dic[store];
			}
			else
			{
				result = defaultValue;
			}
			return result;
		}

		// Token: 0x06000016 RID: 22 RVA: 0x0000298C File Offset: 0x00000B8C
		public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
		{
			return this.m_Dic.GetEnumerator();
		}

		// Token: 0x0400000A RID: 10
		private Dictionary<string, string> m_Dic = new Dictionary<string, string>();
	}
}
