﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000007 RID: 7
	internal interface IAsyncUtil
	{
		// Token: 0x06000021 RID: 33
		void Get(string url, Action<string> responseHandler, Action<string> errorHandler);

		// Token: 0x06000022 RID: 34
		void Schedule(Action a, int delayInSeconds);
	}
}
