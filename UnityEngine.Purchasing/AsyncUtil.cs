﻿using System;
using System.Collections;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000003 RID: 3
	internal class AsyncUtil : MonoBehaviour, IAsyncUtil
	{
		// Token: 0x06000005 RID: 5 RVA: 0x00002140 File Offset: 0x00000340
		public void Get(string url, Action<string> responseHandler, Action<string> errorHandler)
		{
			WWW request = new WWW(url);
			base.StartCoroutine(this.Process(request, responseHandler, errorHandler));
		}

		// Token: 0x06000006 RID: 6 RVA: 0x00002165 File Offset: 0x00000365
		public void Schedule(Action a, int delayInSeconds)
		{
			base.StartCoroutine(this.DoInvoke(a, delayInSeconds));
		}

		// Token: 0x06000007 RID: 7 RVA: 0x00002178 File Offset: 0x00000378
		private IEnumerator DoInvoke(Action a, int delayInSeconds)
		{
			yield return new WaitForSeconds((float)delayInSeconds);
			a();
			yield break;
		}

		// Token: 0x06000008 RID: 8 RVA: 0x000021A4 File Offset: 0x000003A4
		private IEnumerator Process(WWW request, Action<string> responseHandler, Action<string> errorHandler)
		{
			yield return request;
			if (request.error != null)
			{
				errorHandler(request.error);
			}
			else
			{
				responseHandler(request.text);
			}
			yield break;
		}
	}
}
