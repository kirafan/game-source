﻿using System;
using System.Collections.ObjectModel;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x0200001F RID: 31
	public abstract class AbstractStore : IStore
	{
		// Token: 0x060000AE RID: 174
		public abstract void Initialize(IStoreCallback callback);

		// Token: 0x060000AF RID: 175
		public abstract void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products);

		// Token: 0x060000B0 RID: 176
		public abstract void Purchase(ProductDefinition product, string developerPayload);

		// Token: 0x060000B1 RID: 177
		public abstract void FinishTransaction(ProductDefinition product, string transactionId);
	}
}
