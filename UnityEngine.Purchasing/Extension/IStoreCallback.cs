﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000023 RID: 35
	public interface IStoreCallback
	{
		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000BA RID: 186
		ProductCollection products { get; }

		// Token: 0x060000BB RID: 187
		void OnSetupFailed(InitializationFailureReason reason);

		// Token: 0x060000BC RID: 188
		void OnProductsRetrieved(List<ProductDescription> products);

		// Token: 0x060000BD RID: 189
		void OnPurchaseSucceeded(string storeSpecificId, string receipt, string transactionIdentifier);

		// Token: 0x060000BE RID: 190
		void OnPurchaseFailed(PurchaseFailureDescription desc);

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000BF RID: 191
		// (set) Token: 0x060000C0 RID: 192
		bool useTransactionLog { get; set; }
	}
}
