﻿using System;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x0200001E RID: 30
	public abstract class AbstractPurchasingModule : IPurchasingModule
	{
		// Token: 0x060000A8 RID: 168 RVA: 0x00003F44 File Offset: 0x00002144
		public void Configure(IPurchasingBinder binder)
		{
			this.m_Binder = binder;
			this.Configure();
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00003F54 File Offset: 0x00002154
		protected void RegisterStore(string name, IStore a)
		{
			this.m_Binder.RegisterStore(name, a);
		}

		// Token: 0x060000AA RID: 170 RVA: 0x00003F64 File Offset: 0x00002164
		protected void BindExtension<T>(T instance) where T : IStoreExtension
		{
			this.m_Binder.RegisterExtension<T>(instance);
		}

		// Token: 0x060000AB RID: 171 RVA: 0x00003F73 File Offset: 0x00002173
		protected void BindConfiguration<T>(T instance) where T : IStoreConfiguration
		{
			this.m_Binder.RegisterConfiguration<T>(instance);
		}

		// Token: 0x060000AC RID: 172
		public abstract void Configure();

		// Token: 0x04000052 RID: 82
		protected IPurchasingBinder m_Binder;
	}
}
