﻿using System;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000015 RID: 21
	public class PurchaseFailureDescription
	{
		// Token: 0x0600006D RID: 109 RVA: 0x000030C7 File Offset: 0x000012C7
		public PurchaseFailureDescription(string productId, PurchaseFailureReason reason, string message)
		{
			this.productId = productId;
			this.reason = reason;
			this.message = message;
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x0600006E RID: 110 RVA: 0x000030E8 File Offset: 0x000012E8
		// (set) Token: 0x0600006F RID: 111 RVA: 0x00003102 File Offset: 0x00001302
		public string productId { get; private set; }

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000070 RID: 112 RVA: 0x0000310C File Offset: 0x0000130C
		// (set) Token: 0x06000071 RID: 113 RVA: 0x00003126 File Offset: 0x00001326
		public PurchaseFailureReason reason { get; private set; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00003130 File Offset: 0x00001330
		// (set) Token: 0x06000073 RID: 115 RVA: 0x0000314A File Offset: 0x0000134A
		public string message { get; private set; }
	}
}
