﻿using System;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000021 RID: 33
	public interface IPurchasingModule
	{
		// Token: 0x060000B5 RID: 181
		void Configure(IPurchasingBinder binder);
	}
}
