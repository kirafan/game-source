﻿using System;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000020 RID: 32
	public interface IPurchasingBinder
	{
		// Token: 0x060000B2 RID: 178
		void RegisterStore(string name, IStore a);

		// Token: 0x060000B3 RID: 179
		void RegisterExtension<T>(T instance) where T : IStoreExtension;

		// Token: 0x060000B4 RID: 180
		void RegisterConfiguration<T>(T instance) where T : IStoreConfiguration;
	}
}
