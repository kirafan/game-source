﻿using System;
using System.Collections.ObjectModel;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000022 RID: 34
	public interface IStore
	{
		// Token: 0x060000B6 RID: 182
		void Initialize(IStoreCallback callback);

		// Token: 0x060000B7 RID: 183
		void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products);

		// Token: 0x060000B8 RID: 184
		void Purchase(ProductDefinition product, string developerPayload);

		// Token: 0x060000B9 RID: 185
		void FinishTransaction(ProductDefinition product, string transactionId);
	}
}
