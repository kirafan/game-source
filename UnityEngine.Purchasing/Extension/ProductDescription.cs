﻿using System;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x02000026 RID: 38
	public class ProductDescription
	{
		// Token: 0x060000C1 RID: 193 RVA: 0x00003F8A File Offset: 0x0000218A
		public ProductDescription(string id, ProductMetadata metadata, string receipt, string transactionId)
		{
			this.storeSpecificId = id;
			this.metadata = metadata;
			this.receipt = receipt;
			this.transactionId = transactionId;
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x00003FB0 File Offset: 0x000021B0
		public ProductDescription(string id, ProductMetadata metadata, string receipt, string transactionId, ProductType type) : this(id, metadata, receipt, transactionId)
		{
			this.type = type;
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x00003FC6 File Offset: 0x000021C6
		public ProductDescription(string id, ProductMetadata metadata) : this(id, metadata, null, null)
		{
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000C4 RID: 196 RVA: 0x00003FD4 File Offset: 0x000021D4
		// (set) Token: 0x060000C5 RID: 197 RVA: 0x00003FEE File Offset: 0x000021EE
		public string storeSpecificId { get; private set; }

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000C6 RID: 198 RVA: 0x00003FF8 File Offset: 0x000021F8
		// (set) Token: 0x060000C7 RID: 199 RVA: 0x00004012 File Offset: 0x00002212
		public ProductMetadata metadata { get; private set; }

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000C8 RID: 200 RVA: 0x0000401C File Offset: 0x0000221C
		// (set) Token: 0x060000C9 RID: 201 RVA: 0x00004036 File Offset: 0x00002236
		public string receipt { get; private set; }

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00004040 File Offset: 0x00002240
		// (set) Token: 0x060000CB RID: 203 RVA: 0x0000405A File Offset: 0x0000225A
		public string transactionId { get; set; }

		// Token: 0x04000054 RID: 84
		public ProductType type;
	}
}
