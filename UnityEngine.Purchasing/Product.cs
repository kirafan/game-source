﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000E RID: 14
	public class Product
	{
		// Token: 0x06000035 RID: 53 RVA: 0x00002B3C File Offset: 0x00000D3C
		internal Product(ProductDefinition definition, ProductMetadata metadata, string receipt)
		{
			this.definition = definition;
			this.metadata = metadata;
			this.receipt = receipt;
		}

		// Token: 0x06000036 RID: 54 RVA: 0x00002B5A File Offset: 0x00000D5A
		internal Product(ProductDefinition definition, ProductMetadata metadata) : this(definition, metadata, null)
		{
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000037 RID: 55 RVA: 0x00002B68 File Offset: 0x00000D68
		// (set) Token: 0x06000038 RID: 56 RVA: 0x00002B82 File Offset: 0x00000D82
		public ProductDefinition definition { get; private set; }

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000039 RID: 57 RVA: 0x00002B8C File Offset: 0x00000D8C
		// (set) Token: 0x0600003A RID: 58 RVA: 0x00002BA6 File Offset: 0x00000DA6
		public ProductMetadata metadata { get; internal set; }

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600003B RID: 59 RVA: 0x00002BB0 File Offset: 0x00000DB0
		// (set) Token: 0x0600003C RID: 60 RVA: 0x00002BCA File Offset: 0x00000DCA
		public bool availableToPurchase { get; internal set; }

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600003D RID: 61 RVA: 0x00002BD4 File Offset: 0x00000DD4
		// (set) Token: 0x0600003E RID: 62 RVA: 0x00002BEE File Offset: 0x00000DEE
		public string transactionID { get; internal set; }

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600003F RID: 63 RVA: 0x00002BF8 File Offset: 0x00000DF8
		public bool hasReceipt
		{
			get
			{
				return !string.IsNullOrEmpty(this.receipt);
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000040 RID: 64 RVA: 0x00002C1C File Offset: 0x00000E1C
		// (set) Token: 0x06000041 RID: 65 RVA: 0x00002C36 File Offset: 0x00000E36
		public string receipt { get; internal set; }

		// Token: 0x06000042 RID: 66 RVA: 0x00002C40 File Offset: 0x00000E40
		public override bool Equals(object obj)
		{
			bool result;
			if (obj == null)
			{
				result = false;
			}
			else
			{
				Product product = obj as Product;
				result = (product != null && this.definition.Equals(product.definition));
			}
			return result;
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00002C88 File Offset: 0x00000E88
		public override int GetHashCode()
		{
			return this.definition.GetHashCode();
		}
	}
}
