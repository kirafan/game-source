﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000010 RID: 16
	public class ProductDefinition
	{
		// Token: 0x0600004C RID: 76 RVA: 0x00002E13 File Offset: 0x00001013
		private ProductDefinition()
		{
		}

		// Token: 0x0600004D RID: 77 RVA: 0x00002E1C File Offset: 0x0000101C
		public ProductDefinition(string id, string storeSpecificId, ProductType type)
		{
			this.id = id;
			this.storeSpecificId = storeSpecificId;
			this.type = type;
		}

		// Token: 0x0600004E RID: 78 RVA: 0x00002E3A File Offset: 0x0000103A
		public ProductDefinition(string id, ProductType type) : this(id, id, type)
		{
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600004F RID: 79 RVA: 0x00002E48 File Offset: 0x00001048
		// (set) Token: 0x06000050 RID: 80 RVA: 0x00002E62 File Offset: 0x00001062
		public string id { get; private set; }

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000051 RID: 81 RVA: 0x00002E6C File Offset: 0x0000106C
		// (set) Token: 0x06000052 RID: 82 RVA: 0x00002E86 File Offset: 0x00001086
		public string storeSpecificId { get; private set; }

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000053 RID: 83 RVA: 0x00002E90 File Offset: 0x00001090
		// (set) Token: 0x06000054 RID: 84 RVA: 0x00002EAA File Offset: 0x000010AA
		public ProductType type { get; private set; }

		// Token: 0x06000055 RID: 85 RVA: 0x00002EB4 File Offset: 0x000010B4
		public override bool Equals(object obj)
		{
			bool result;
			if (obj == null)
			{
				result = false;
			}
			else
			{
				ProductDefinition productDefinition = obj as ProductDefinition;
				result = (productDefinition != null && this.id == productDefinition.id);
			}
			return result;
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00002EFC File Offset: 0x000010FC
		public override int GetHashCode()
		{
			return this.id.GetHashCode();
		}
	}
}
