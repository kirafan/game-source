﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000006 RID: 6
	public class ConfigurationBuilder
	{
		// Token: 0x06000017 RID: 23 RVA: 0x000029B1 File Offset: 0x00000BB1
		internal ConfigurationBuilder(PurchasingFactory factory)
		{
			this.m_Factory = factory;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000018 RID: 24 RVA: 0x000029CC File Offset: 0x00000BCC
		// (set) Token: 0x06000019 RID: 25 RVA: 0x000029E6 File Offset: 0x00000BE6
		public bool useCloudCatalog { get; set; }

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600001A RID: 26 RVA: 0x000029F0 File Offset: 0x00000BF0
		public HashSet<ProductDefinition> products
		{
			get
			{
				return this.m_Products;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600001B RID: 27 RVA: 0x00002A0C File Offset: 0x00000C0C
		internal PurchasingFactory factory
		{
			get
			{
				return this.m_Factory;
			}
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002A28 File Offset: 0x00000C28
		public T Configure<T>() where T : IStoreConfiguration
		{
			return this.m_Factory.GetConfig<T>();
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002A48 File Offset: 0x00000C48
		public static ConfigurationBuilder Instance(IPurchasingModule first, params IPurchasingModule[] rest)
		{
			PurchasingFactory factory = new PurchasingFactory(first, rest);
			return new ConfigurationBuilder(factory);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002A6C File Offset: 0x00000C6C
		public ConfigurationBuilder AddProduct(string id, ProductType type)
		{
			return this.AddProduct(id, type, null);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002A8C File Offset: 0x00000C8C
		public ConfigurationBuilder AddProduct(string id, ProductType type, IDs storeIDs)
		{
			string storeSpecificId = id;
			if (storeIDs != null)
			{
				storeSpecificId = storeIDs.SpecificIDForStore(this.factory.storeName, id);
			}
			ProductDefinition item = new ProductDefinition(id, storeSpecificId, type);
			this.m_Products.Add(item);
			return this;
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002AD4 File Offset: 0x00000CD4
		public ConfigurationBuilder AddProducts(IEnumerable<ProductDefinition> products)
		{
			foreach (ProductDefinition item in products)
			{
				this.m_Products.Add(item);
			}
			return this;
		}

		// Token: 0x0400000B RID: 11
		private PurchasingFactory m_Factory;

		// Token: 0x0400000C RID: 12
		private HashSet<ProductDefinition> m_Products = new HashSet<ProductDefinition>();
	}
}
