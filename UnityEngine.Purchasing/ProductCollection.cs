﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000F RID: 15
	public class ProductCollection
	{
		// Token: 0x06000044 RID: 68 RVA: 0x00002CA8 File Offset: 0x00000EA8
		internal ProductCollection(Product[] products)
		{
			this.AddProducts(products);
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00002CC4 File Offset: 0x00000EC4
		internal void AddProducts(IEnumerable<Product> products)
		{
			this.m_ProductSet.UnionWith(products);
			this.m_Products = this.m_ProductSet.ToArray<Product>();
			this.m_IdToProduct = this.m_Products.ToDictionary((Product x) => x.definition.id);
			this.m_StoreSpecificIdToProduct = this.m_Products.ToDictionary((Product x) => x.definition.storeSpecificId);
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000046 RID: 70 RVA: 0x00002D4C File Offset: 0x00000F4C
		public HashSet<Product> set
		{
			get
			{
				return this.m_ProductSet;
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000047 RID: 71 RVA: 0x00002D68 File Offset: 0x00000F68
		public Product[] all
		{
			get
			{
				return this.m_Products;
			}
		}

		// Token: 0x06000048 RID: 72 RVA: 0x00002D84 File Offset: 0x00000F84
		public Product WithID(string id)
		{
			Product result = null;
			this.m_IdToProduct.TryGetValue(id, out result);
			return result;
		}

		// Token: 0x06000049 RID: 73 RVA: 0x00002DAC File Offset: 0x00000FAC
		public Product WithStoreSpecificID(string id)
		{
			Product result = null;
			this.m_StoreSpecificIdToProduct.TryGetValue(id, out result);
			return result;
		}

		// Token: 0x04000017 RID: 23
		private Dictionary<string, Product> m_IdToProduct;

		// Token: 0x04000018 RID: 24
		private Dictionary<string, Product> m_StoreSpecificIdToProduct;

		// Token: 0x04000019 RID: 25
		private Product[] m_Products;

		// Token: 0x0400001A RID: 26
		private HashSet<Product> m_ProductSet = new HashSet<Product>();
	}
}
