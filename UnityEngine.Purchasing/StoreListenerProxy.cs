﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001A RID: 26
	internal class StoreListenerProxy : IInternalStoreListener
	{
		// Token: 0x06000093 RID: 147 RVA: 0x00003A93 File Offset: 0x00001C93
		public StoreListenerProxy(IStoreListener forwardTo, AnalyticsReporter analytics, IExtensionProvider extensions)
		{
			this.m_ForwardTo = forwardTo;
			this.m_Analytics = analytics;
			this.m_Extensions = extensions;
		}

		// Token: 0x06000094 RID: 148 RVA: 0x00003AB1 File Offset: 0x00001CB1
		public void OnInitialized(IStoreController controller)
		{
			this.m_ForwardTo.OnInitialized(controller, this.m_Extensions);
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00003AC6 File Offset: 0x00001CC6
		public void OnInitializeFailed(InitializationFailureReason error)
		{
			this.m_ForwardTo.OnInitializeFailed(error);
		}

		// Token: 0x06000096 RID: 150 RVA: 0x00003AD8 File Offset: 0x00001CD8
		public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
		{
			this.m_Analytics.OnPurchaseSucceeded(e.purchasedProduct);
			return this.m_ForwardTo.ProcessPurchase(e);
		}

		// Token: 0x06000097 RID: 151 RVA: 0x00003B0A File Offset: 0x00001D0A
		public void OnPurchaseFailed(Product i, PurchaseFailureReason p)
		{
			this.m_Analytics.OnPurchaseFailed(i, p);
			this.m_ForwardTo.OnPurchaseFailed(i, p);
		}

		// Token: 0x0400004C RID: 76
		private AnalyticsReporter m_Analytics;

		// Token: 0x0400004D RID: 77
		private IStoreListener m_ForwardTo;

		// Token: 0x0400004E RID: 78
		private IExtensionProvider m_Extensions;
	}
}
