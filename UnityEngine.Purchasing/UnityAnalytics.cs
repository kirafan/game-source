﻿using System;
using System.Collections.Generic;
using UnityEngine.Analytics;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001C RID: 28
	internal class UnityAnalytics : IUnityAnalytics
	{
		// Token: 0x0600009F RID: 159 RVA: 0x00003CEB File Offset: 0x00001EEB
		public void Transaction(string productId, decimal price, string currency, string receipt, string signature)
		{
			Analytics.Transaction(productId, price, currency, receipt, signature, true);
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x00003CFC File Offset: 0x00001EFC
		public void CustomEvent(string name, Dictionary<string, object> data)
		{
			Analytics.CustomEvent(name, data);
		}
	}
}
