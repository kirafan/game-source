﻿using System;
using System.IO;
using System.Text;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001B RID: 27
	internal class TransactionLog
	{
		// Token: 0x06000098 RID: 152 RVA: 0x00003B27 File Offset: 0x00001D27
		public TransactionLog(ILogger logger, string persistentDataPath)
		{
			this.logger = logger;
			if (!string.IsNullOrEmpty(persistentDataPath))
			{
				this.persistentDataPath = Path.Combine(Path.Combine(persistentDataPath, "Unity"), "UnityPurchasing");
			}
		}

		// Token: 0x06000099 RID: 153 RVA: 0x00003B5F File Offset: 0x00001D5F
		public void Clear()
		{
			Directory.Delete(this.persistentDataPath, true);
		}

		// Token: 0x0600009A RID: 154 RVA: 0x00003B70 File Offset: 0x00001D70
		public bool HasRecordOf(string transactionID)
		{
			return !string.IsNullOrEmpty(transactionID) && !string.IsNullOrEmpty(this.persistentDataPath) && Directory.Exists(this.GetRecordPath(transactionID));
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00003BB4 File Offset: 0x00001DB4
		public void Record(string transactionID)
		{
			if (!string.IsNullOrEmpty(transactionID) && !string.IsNullOrEmpty(this.persistentDataPath))
			{
				string recordPath = this.GetRecordPath(transactionID);
				try
				{
					Directory.CreateDirectory(recordPath);
				}
				catch (Exception ex)
				{
					this.logger.Log(ex.Message);
				}
			}
		}

		// Token: 0x0600009C RID: 156 RVA: 0x00003C20 File Offset: 0x00001E20
		private string GetRecordPath(string transactionID)
		{
			return Path.Combine(this.persistentDataPath, TransactionLog.ComputeHash(transactionID));
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00003C48 File Offset: 0x00001E48
		internal static string ComputeHash(string transactionID)
		{
			ulong num = 3074457345618258791UL;
			for (int i = 0; i < transactionID.Length; i++)
			{
				num += (ulong)transactionID[i];
				num *= 3074457345618258799UL;
			}
			StringBuilder stringBuilder = new StringBuilder(16);
			foreach (byte b in BitConverter.GetBytes(num))
			{
				stringBuilder.AppendFormat("{0:X2}", b);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0400004F RID: 79
		private readonly ILogger logger;

		// Token: 0x04000050 RID: 80
		private readonly string persistentDataPath;
	}
}
