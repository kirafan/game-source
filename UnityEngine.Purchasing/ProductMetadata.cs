﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000011 RID: 17
	public class ProductMetadata
	{
		// Token: 0x06000057 RID: 87 RVA: 0x00002F1C File Offset: 0x0000111C
		public ProductMetadata(string priceString, string title, string description, string currencyCode, decimal localizedPrice)
		{
			this.localizedPriceString = priceString;
			this.localizedTitle = title;
			this.localizedDescription = description;
			this.isoCurrencyCode = currencyCode;
			this.localizedPrice = localizedPrice;
		}

		// Token: 0x06000058 RID: 88 RVA: 0x00002F4A File Offset: 0x0000114A
		public ProductMetadata()
		{
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000059 RID: 89 RVA: 0x00002F54 File Offset: 0x00001154
		// (set) Token: 0x0600005A RID: 90 RVA: 0x00002F6E File Offset: 0x0000116E
		public string localizedPriceString { get; internal set; }

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x0600005B RID: 91 RVA: 0x00002F78 File Offset: 0x00001178
		// (set) Token: 0x0600005C RID: 92 RVA: 0x00002F92 File Offset: 0x00001192
		public string localizedTitle { get; internal set; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x0600005D RID: 93 RVA: 0x00002F9C File Offset: 0x0000119C
		// (set) Token: 0x0600005E RID: 94 RVA: 0x00002FB6 File Offset: 0x000011B6
		public string localizedDescription { get; internal set; }

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x0600005F RID: 95 RVA: 0x00002FC0 File Offset: 0x000011C0
		// (set) Token: 0x06000060 RID: 96 RVA: 0x00002FDA File Offset: 0x000011DA
		public string isoCurrencyCode { get; internal set; }

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000061 RID: 97 RVA: 0x00002FE4 File Offset: 0x000011E4
		// (set) Token: 0x06000062 RID: 98 RVA: 0x00002FFE File Offset: 0x000011FE
		public decimal localizedPrice { get; internal set; }
	}
}
