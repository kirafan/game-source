﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200005A RID: 90
	public enum JsonToken
	{
		// Token: 0x04000107 RID: 263
		None,
		// Token: 0x04000108 RID: 264
		ObjectStart,
		// Token: 0x04000109 RID: 265
		PropertyName,
		// Token: 0x0400010A RID: 266
		ObjectEnd,
		// Token: 0x0400010B RID: 267
		ArrayStart,
		// Token: 0x0400010C RID: 268
		ArrayEnd,
		// Token: 0x0400010D RID: 269
		Int,
		// Token: 0x0400010E RID: 270
		UInt,
		// Token: 0x0400010F RID: 271
		Long,
		// Token: 0x04000110 RID: 272
		ULong,
		// Token: 0x04000111 RID: 273
		Double,
		// Token: 0x04000112 RID: 274
		String,
		// Token: 0x04000113 RID: 275
		Boolean,
		// Token: 0x04000114 RID: 276
		Null
	}
}
