﻿using System;
using System.Collections.Generic;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x02000053 RID: 83
	internal struct ObjectMetadata
	{
		// Token: 0x17000138 RID: 312
		// (get) Token: 0x060003EF RID: 1007 RVA: 0x00008D1E File Offset: 0x00006F1E
		// (set) Token: 0x060003F0 RID: 1008 RVA: 0x00008D39 File Offset: 0x00006F39
		public Type ElementType
		{
			get
			{
				if (this.element_type == null)
				{
					return typeof(JsonData);
				}
				return this.element_type;
			}
			set
			{
				this.element_type = value;
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x060003F1 RID: 1009 RVA: 0x00008D42 File Offset: 0x00006F42
		// (set) Token: 0x060003F2 RID: 1010 RVA: 0x00008D4A File Offset: 0x00006F4A
		public bool IsDictionary
		{
			get
			{
				return this.is_dictionary;
			}
			set
			{
				this.is_dictionary = value;
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x060003F3 RID: 1011 RVA: 0x00008D53 File Offset: 0x00006F53
		// (set) Token: 0x060003F4 RID: 1012 RVA: 0x00008D5B File Offset: 0x00006F5B
		public IDictionary<string, PropertyMetadata> Properties
		{
			get
			{
				return this.properties;
			}
			set
			{
				this.properties = value;
			}
		}

		// Token: 0x040000F2 RID: 242
		private Type element_type;

		// Token: 0x040000F3 RID: 243
		private bool is_dictionary;

		// Token: 0x040000F4 RID: 244
		private IDictionary<string, PropertyMetadata> properties;
	}
}
