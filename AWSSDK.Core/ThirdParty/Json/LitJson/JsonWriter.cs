﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Util;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200005E RID: 94
	public class JsonWriter
	{
		// Token: 0x17000141 RID: 321
		// (get) Token: 0x06000434 RID: 1076 RVA: 0x0000AA34 File Offset: 0x00008C34
		// (set) Token: 0x06000435 RID: 1077 RVA: 0x0000AA3C File Offset: 0x00008C3C
		public int IndentValue
		{
			get
			{
				return this.indent_value;
			}
			set
			{
				this.indentation = this.indentation / this.indent_value * value;
				this.indent_value = value;
			}
		}

		// Token: 0x17000142 RID: 322
		// (get) Token: 0x06000436 RID: 1078 RVA: 0x0000AA5A File Offset: 0x00008C5A
		// (set) Token: 0x06000437 RID: 1079 RVA: 0x0000AA62 File Offset: 0x00008C62
		public bool PrettyPrint
		{
			get
			{
				return this.pretty_print;
			}
			set
			{
				this.pretty_print = value;
			}
		}

		// Token: 0x17000143 RID: 323
		// (get) Token: 0x06000438 RID: 1080 RVA: 0x0000AA6B File Offset: 0x00008C6B
		public TextWriter TextWriter
		{
			get
			{
				return this.writer;
			}
		}

		// Token: 0x17000144 RID: 324
		// (get) Token: 0x06000439 RID: 1081 RVA: 0x0000AA73 File Offset: 0x00008C73
		// (set) Token: 0x0600043A RID: 1082 RVA: 0x0000AA7B File Offset: 0x00008C7B
		public bool Validate
		{
			get
			{
				return this.validate;
			}
			set
			{
				this.validate = value;
			}
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x0000AA90 File Offset: 0x00008C90
		public JsonWriter()
		{
			this.inst_string_builder = new StringBuilder();
			this.writer = new StringWriter(this.inst_string_builder);
			this.Init();
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x0000AABA File Offset: 0x00008CBA
		public JsonWriter(StringBuilder sb) : this(new StringWriter(sb))
		{
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x0000AAC8 File Offset: 0x00008CC8
		public JsonWriter(TextWriter writer)
		{
			if (writer == null)
			{
				throw new ArgumentNullException("writer");
			}
			this.writer = writer;
			this.Init();
		}

		// Token: 0x0600043F RID: 1087 RVA: 0x0000AAEC File Offset: 0x00008CEC
		private void DoValidation(Condition cond)
		{
			if (!this.context.ExpectingValue)
			{
				this.context.Count++;
			}
			if (!this.validate)
			{
				return;
			}
			if (this.has_reached_end)
			{
				throw new JsonException("A complete JSON symbol has already been written");
			}
			switch (cond)
			{
			case Condition.InArray:
				if (!this.context.InArray)
				{
					throw new JsonException("Can't close an array here");
				}
				break;
			case Condition.InObject:
				if (!this.context.InObject || this.context.ExpectingValue)
				{
					throw new JsonException("Can't close an object here");
				}
				break;
			case Condition.NotAProperty:
				if (this.context.InObject && !this.context.ExpectingValue)
				{
					throw new JsonException("Expected a property");
				}
				break;
			case Condition.Property:
				if (!this.context.InObject || this.context.ExpectingValue)
				{
					throw new JsonException("Can't add a property here");
				}
				break;
			case Condition.Value:
				if (!this.context.InArray && (!this.context.InObject || !this.context.ExpectingValue))
				{
					throw new JsonException("Can't add a value here");
				}
				break;
			default:
				return;
			}
		}

		// Token: 0x06000440 RID: 1088 RVA: 0x0000AC10 File Offset: 0x00008E10
		private void Init()
		{
			this.has_reached_end = false;
			this.hex_seq = new char[4];
			this.indentation = 0;
			this.indent_value = 4;
			this.pretty_print = false;
			this.validate = true;
			this.ctx_stack = new Stack<WriterContext>();
			this.context = new WriterContext();
			this.ctx_stack.Push(this.context);
		}

		// Token: 0x06000441 RID: 1089 RVA: 0x0000AC74 File Offset: 0x00008E74
		private static void IntToHex(int n, char[] hex)
		{
			for (int i = 0; i < 4; i++)
			{
				int num = n % 16;
				if (num < 10)
				{
					hex[3 - i] = (char)(48 + num);
				}
				else
				{
					hex[3 - i] = (char)(65 + (num - 10));
				}
				n >>= 4;
			}
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x0000ACB5 File Offset: 0x00008EB5
		private void Indent()
		{
			if (this.pretty_print)
			{
				this.indentation += this.indent_value;
			}
		}

		// Token: 0x06000443 RID: 1091 RVA: 0x0000ACD4 File Offset: 0x00008ED4
		private void Put(string str)
		{
			if (this.pretty_print && !this.context.ExpectingValue)
			{
				for (int i = 0; i < this.indentation; i++)
				{
					this.writer.Write(' ');
				}
			}
			this.writer.Write(str);
		}

		// Token: 0x06000444 RID: 1092 RVA: 0x0000AD20 File Offset: 0x00008F20
		private void PutNewline()
		{
			this.PutNewline(true);
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x0000AD2C File Offset: 0x00008F2C
		private void PutNewline(bool add_comma)
		{
			if (add_comma && !this.context.ExpectingValue && this.context.Count > 1)
			{
				this.writer.Write(',');
			}
			if (this.pretty_print && !this.context.ExpectingValue)
			{
				this.writer.Write("\r\n");
			}
		}

		// Token: 0x06000446 RID: 1094 RVA: 0x0000AD8C File Offset: 0x00008F8C
		private void PutString(string str)
		{
			this.Put(string.Empty);
			this.writer.Write('"');
			int length = str.Length;
			int i = 0;
			while (i < length)
			{
				char c = str[i];
				switch (c)
				{
				case '\b':
					this.writer.Write("\\b");
					break;
				case '\t':
					this.writer.Write("\\t");
					break;
				case '\n':
					this.writer.Write("\\n");
					break;
				case '\v':
					goto IL_DE;
				case '\f':
					this.writer.Write("\\f");
					break;
				case '\r':
					this.writer.Write("\\r");
					break;
				default:
					if (c != '"' && c != '\\')
					{
						goto IL_DE;
					}
					this.writer.Write('\\');
					this.writer.Write(c);
					break;
				}
				IL_146:
				i++;
				continue;
				IL_DE:
				if (c >= ' ' && c <= '~')
				{
					this.writer.Write(c);
					goto IL_146;
				}
				if (c < ' ' || (c >= '\u0080' && c < '\u00a0'))
				{
					JsonWriter.IntToHex((int)c, this.hex_seq);
					this.writer.Write("\\u");
					this.writer.Write(this.hex_seq);
					goto IL_146;
				}
				this.writer.Write(c);
				goto IL_146;
			}
			this.writer.Write('"');
		}

		// Token: 0x06000447 RID: 1095 RVA: 0x0000AEF7 File Offset: 0x000090F7
		private void Unindent()
		{
			if (this.pretty_print)
			{
				this.indentation -= this.indent_value;
			}
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x0000AF14 File Offset: 0x00009114
		public override string ToString()
		{
			if (this.inst_string_builder == null)
			{
				return string.Empty;
			}
			return this.inst_string_builder.ToString();
		}

		// Token: 0x06000449 RID: 1097 RVA: 0x0000AF30 File Offset: 0x00009130
		public void Reset()
		{
			this.has_reached_end = false;
			this.ctx_stack.Clear();
			this.context = new WriterContext();
			this.ctx_stack.Push(this.context);
			if (this.inst_string_builder != null)
			{
				this.inst_string_builder.Remove(0, this.inst_string_builder.Length);
			}
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x0000AF8B File Offset: 0x0000918B
		public void Write(bool boolean)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			this.Put(boolean ? "true" : "false");
			this.context.ExpectingValue = false;
		}

		// Token: 0x0600044B RID: 1099 RVA: 0x0000AFBB File Offset: 0x000091BB
		public void Write(decimal number)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			this.Put(Convert.ToString(number, JsonWriter.number_format));
			this.context.ExpectingValue = false;
		}

		// Token: 0x0600044C RID: 1100 RVA: 0x0000AFE8 File Offset: 0x000091E8
		public void Write(double number)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			string text = number.ToString("R", CultureInfo.InvariantCulture);
			this.Put(text);
			if (text.IndexOf('.') == -1 && text.IndexOf('E') == -1)
			{
				this.writer.Write(".0");
			}
			this.context.ExpectingValue = false;
		}

		// Token: 0x0600044D RID: 1101 RVA: 0x0000B04D File Offset: 0x0000924D
		public void Write(int number)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			this.Put(Convert.ToString(number, JsonWriter.number_format));
			this.context.ExpectingValue = false;
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x0000B079 File Offset: 0x00009279
		public void Write(uint number)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			this.Put(Convert.ToString(number, JsonWriter.number_format));
			this.context.ExpectingValue = false;
		}

		// Token: 0x0600044F RID: 1103 RVA: 0x0000B0A5 File Offset: 0x000092A5
		public void Write(long number)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			this.Put(Convert.ToString(number, JsonWriter.number_format));
			this.context.ExpectingValue = false;
		}

		// Token: 0x06000450 RID: 1104 RVA: 0x0000B0D1 File Offset: 0x000092D1
		public void Write(string str)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			if (str == null)
			{
				this.Put("null");
			}
			else
			{
				this.PutString(str);
			}
			this.context.ExpectingValue = false;
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x0000B103 File Offset: 0x00009303
		public void WriteRaw(string str)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			if (str == null)
			{
				this.Put("null");
			}
			else
			{
				this.Put(str);
			}
			this.context.ExpectingValue = false;
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x0000B135 File Offset: 0x00009335
		[CLSCompliant(false)]
		public void Write(ulong number)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			this.Put(Convert.ToString(number, JsonWriter.number_format));
			this.context.ExpectingValue = false;
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x0000B164 File Offset: 0x00009364
		public void Write(DateTime date)
		{
			this.DoValidation(Condition.Value);
			this.PutNewline();
			this.Put(AWSSDKUtils.ConvertToUnixEpochMilliSeconds(date).ToString(CultureInfo.InvariantCulture));
			this.context.ExpectingValue = false;
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x0000B1A4 File Offset: 0x000093A4
		public void WriteArrayEnd()
		{
			this.DoValidation(Condition.InArray);
			this.PutNewline(false);
			this.ctx_stack.Pop();
			if (this.ctx_stack.Count == 1)
			{
				this.has_reached_end = true;
			}
			else
			{
				this.context = this.ctx_stack.Peek();
				this.context.ExpectingValue = false;
			}
			this.Unindent();
			this.Put("]");
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x0000B210 File Offset: 0x00009410
		public void WriteArrayStart()
		{
			this.DoValidation(Condition.NotAProperty);
			this.PutNewline();
			this.Put("[");
			this.context = new WriterContext();
			this.context.InArray = true;
			this.ctx_stack.Push(this.context);
			this.Indent();
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x0000B264 File Offset: 0x00009464
		public void WriteObjectEnd()
		{
			this.DoValidation(Condition.InObject);
			this.PutNewline(false);
			this.ctx_stack.Pop();
			if (this.ctx_stack.Count == 1)
			{
				this.has_reached_end = true;
			}
			else
			{
				this.context = this.ctx_stack.Peek();
				this.context.ExpectingValue = false;
			}
			this.Unindent();
			this.Put("}");
		}

		// Token: 0x06000457 RID: 1111 RVA: 0x0000B2D0 File Offset: 0x000094D0
		public void WriteObjectStart()
		{
			this.DoValidation(Condition.NotAProperty);
			this.PutNewline();
			this.Put("{");
			this.context = new WriterContext();
			this.context.InObject = true;
			this.ctx_stack.Push(this.context);
			this.Indent();
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x0000B324 File Offset: 0x00009524
		public void WritePropertyName(string property_name)
		{
			this.DoValidation(Condition.Property);
			this.PutNewline();
			this.PutString(property_name);
			if (this.pretty_print)
			{
				if (property_name.Length > this.context.Padding)
				{
					this.context.Padding = property_name.Length;
				}
				for (int i = this.context.Padding - property_name.Length; i >= 0; i--)
				{
					this.writer.Write(' ');
				}
				this.writer.Write(": ");
			}
			else
			{
				this.writer.Write(':');
			}
			this.context.ExpectingValue = true;
		}

		// Token: 0x0400012D RID: 301
		private static NumberFormatInfo number_format = NumberFormatInfo.InvariantInfo;

		// Token: 0x0400012E RID: 302
		private WriterContext context;

		// Token: 0x0400012F RID: 303
		private Stack<WriterContext> ctx_stack;

		// Token: 0x04000130 RID: 304
		private bool has_reached_end;

		// Token: 0x04000131 RID: 305
		private char[] hex_seq;

		// Token: 0x04000132 RID: 306
		private int indentation;

		// Token: 0x04000133 RID: 307
		private int indent_value;

		// Token: 0x04000134 RID: 308
		private StringBuilder inst_string_builder;

		// Token: 0x04000135 RID: 309
		private bool pretty_print;

		// Token: 0x04000136 RID: 310
		private bool validate;

		// Token: 0x04000137 RID: 311
		private TextWriter writer;
	}
}
