﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x02000050 RID: 80
	[Serializable]
	public class JsonException : Exception
	{
		// Token: 0x060003E2 RID: 994 RVA: 0x00008C59 File Offset: 0x00006E59
		public JsonException()
		{
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x00008C61 File Offset: 0x00006E61
		internal JsonException(ParserToken token) : base(string.Format("Invalid token '{0}' in input string", token))
		{
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x00008C79 File Offset: 0x00006E79
		internal JsonException(ParserToken token, Exception inner_exception) : base(string.Format("Invalid token '{0}' in input string", token), inner_exception)
		{
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x00008C92 File Offset: 0x00006E92
		internal JsonException(int c) : base(string.Format("Invalid character '{0}' in input string", (char)c))
		{
		}

		// Token: 0x060003E6 RID: 998 RVA: 0x00008CAB File Offset: 0x00006EAB
		internal JsonException(int c, Exception inner_exception) : base(string.Format("Invalid character '{0}' in input string", (char)c), inner_exception)
		{
		}

		// Token: 0x060003E7 RID: 999 RVA: 0x00008CC5 File Offset: 0x00006EC5
		public JsonException(string message) : base(message)
		{
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x00008CCE File Offset: 0x00006ECE
		public JsonException(string message, Exception inner_exception) : base(message, inner_exception)
		{
		}
	}
}
