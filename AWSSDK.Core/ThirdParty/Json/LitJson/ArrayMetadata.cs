﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x02000052 RID: 82
	internal struct ArrayMetadata
	{
		// Token: 0x17000135 RID: 309
		// (get) Token: 0x060003E9 RID: 1001 RVA: 0x00008CD8 File Offset: 0x00006ED8
		// (set) Token: 0x060003EA RID: 1002 RVA: 0x00008CF3 File Offset: 0x00006EF3
		public Type ElementType
		{
			get
			{
				if (this.element_type == null)
				{
					return typeof(JsonData);
				}
				return this.element_type;
			}
			set
			{
				this.element_type = value;
			}
		}

		// Token: 0x17000136 RID: 310
		// (get) Token: 0x060003EB RID: 1003 RVA: 0x00008CFC File Offset: 0x00006EFC
		// (set) Token: 0x060003EC RID: 1004 RVA: 0x00008D04 File Offset: 0x00006F04
		public bool IsArray
		{
			get
			{
				return this.is_array;
			}
			set
			{
				this.is_array = value;
			}
		}

		// Token: 0x17000137 RID: 311
		// (get) Token: 0x060003ED RID: 1005 RVA: 0x00008D0D File Offset: 0x00006F0D
		// (set) Token: 0x060003EE RID: 1006 RVA: 0x00008D15 File Offset: 0x00006F15
		public bool IsList
		{
			get
			{
				return this.is_list;
			}
			set
			{
				this.is_list = value;
			}
		}

		// Token: 0x040000EF RID: 239
		private Type element_type;

		// Token: 0x040000F0 RID: 240
		private bool is_array;

		// Token: 0x040000F1 RID: 241
		private bool is_list;
	}
}
