﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200005D RID: 93
	internal class WriterContext
	{
		// Token: 0x04000128 RID: 296
		public int Count;

		// Token: 0x04000129 RID: 297
		public bool InArray;

		// Token: 0x0400012A RID: 298
		public bool InObject;

		// Token: 0x0400012B RID: 299
		public bool ExpectingValue;

		// Token: 0x0400012C RID: 300
		public int Padding;
	}
}
