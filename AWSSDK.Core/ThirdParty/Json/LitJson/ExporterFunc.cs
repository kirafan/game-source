﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x02000054 RID: 84
	// (Invoke) Token: 0x060003F6 RID: 1014
	internal delegate void ExporterFunc(object obj, JsonWriter writer);
}
