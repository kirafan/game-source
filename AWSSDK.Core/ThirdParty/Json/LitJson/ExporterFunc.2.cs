﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x02000055 RID: 85
	// (Invoke) Token: 0x060003FA RID: 1018
	public delegate void ExporterFunc<T>(T obj, JsonWriter writer);
}
