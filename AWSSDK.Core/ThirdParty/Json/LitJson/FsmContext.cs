﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200005F RID: 95
	internal class FsmContext
	{
		// Token: 0x04000138 RID: 312
		public bool Return;

		// Token: 0x04000139 RID: 313
		public int NextState;

		// Token: 0x0400013A RID: 314
		public Lexer L;

		// Token: 0x0400013B RID: 315
		public int StateStack;
	}
}
