﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200005C RID: 92
	internal enum Condition
	{
		// Token: 0x04000123 RID: 291
		InArray,
		// Token: 0x04000124 RID: 292
		InObject,
		// Token: 0x04000125 RID: 293
		NotAProperty,
		// Token: 0x04000126 RID: 294
		Property,
		// Token: 0x04000127 RID: 295
		Value
	}
}
