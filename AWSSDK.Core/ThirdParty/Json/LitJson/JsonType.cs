﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200004C RID: 76
	public enum JsonType
	{
		// Token: 0x040000D8 RID: 216
		None,
		// Token: 0x040000D9 RID: 217
		Object,
		// Token: 0x040000DA RID: 218
		Array,
		// Token: 0x040000DB RID: 219
		String,
		// Token: 0x040000DC RID: 220
		Int,
		// Token: 0x040000DD RID: 221
		UInt,
		// Token: 0x040000DE RID: 222
		Long,
		// Token: 0x040000DF RID: 223
		ULong,
		// Token: 0x040000E0 RID: 224
		Double,
		// Token: 0x040000E1 RID: 225
		Boolean
	}
}
