﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x02000057 RID: 87
	// (Invoke) Token: 0x06000402 RID: 1026
	public delegate TValue ImporterFunc<TJson, TValue>(TJson input);
}
