﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x02000056 RID: 86
	// (Invoke) Token: 0x060003FE RID: 1022
	internal delegate object ImporterFunc(object input);
}
