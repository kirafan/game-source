﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200004F RID: 79
	internal class OrderedDictionaryEnumerator : IDictionaryEnumerator, IEnumerator
	{
		// Token: 0x17000131 RID: 305
		// (get) Token: 0x060003DB RID: 987 RVA: 0x00008BB5 File Offset: 0x00006DB5
		public object Current
		{
			get
			{
				return this.Entry;
			}
		}

		// Token: 0x17000132 RID: 306
		// (get) Token: 0x060003DC RID: 988 RVA: 0x00008BC4 File Offset: 0x00006DC4
		public DictionaryEntry Entry
		{
			get
			{
				KeyValuePair<string, JsonData> keyValuePair = this.list_enumerator.Current;
				return new DictionaryEntry(keyValuePair.Key, keyValuePair.Value);
			}
		}

		// Token: 0x17000133 RID: 307
		// (get) Token: 0x060003DD RID: 989 RVA: 0x00008BF0 File Offset: 0x00006DF0
		public object Key
		{
			get
			{
				KeyValuePair<string, JsonData> keyValuePair = this.list_enumerator.Current;
				return keyValuePair.Key;
			}
		}

		// Token: 0x17000134 RID: 308
		// (get) Token: 0x060003DE RID: 990 RVA: 0x00008C10 File Offset: 0x00006E10
		public object Value
		{
			get
			{
				KeyValuePair<string, JsonData> keyValuePair = this.list_enumerator.Current;
				return keyValuePair.Value;
			}
		}

		// Token: 0x060003DF RID: 991 RVA: 0x00008C30 File Offset: 0x00006E30
		public OrderedDictionaryEnumerator(IEnumerator<KeyValuePair<string, JsonData>> enumerator)
		{
			this.list_enumerator = enumerator;
		}

		// Token: 0x060003E0 RID: 992 RVA: 0x00008C3F File Offset: 0x00006E3F
		public bool MoveNext()
		{
			return this.list_enumerator.MoveNext();
		}

		// Token: 0x060003E1 RID: 993 RVA: 0x00008C4C File Offset: 0x00006E4C
		public void Reset()
		{
			this.list_enumerator.Reset();
		}

		// Token: 0x040000EB RID: 235
		private IEnumerator<KeyValuePair<string, JsonData>> list_enumerator;
	}
}
