﻿using System;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x02000061 RID: 97
	internal enum ParserToken
	{
		// Token: 0x0400014B RID: 331
		None = 65536,
		// Token: 0x0400014C RID: 332
		Number,
		// Token: 0x0400014D RID: 333
		True,
		// Token: 0x0400014E RID: 334
		False,
		// Token: 0x0400014F RID: 335
		Null,
		// Token: 0x04000150 RID: 336
		CharSeq,
		// Token: 0x04000151 RID: 337
		Char,
		// Token: 0x04000152 RID: 338
		Text,
		// Token: 0x04000153 RID: 339
		Object,
		// Token: 0x04000154 RID: 340
		ObjectPrime,
		// Token: 0x04000155 RID: 341
		Pair,
		// Token: 0x04000156 RID: 342
		PairRest,
		// Token: 0x04000157 RID: 343
		Array,
		// Token: 0x04000158 RID: 344
		ArrayPrime,
		// Token: 0x04000159 RID: 345
		Value,
		// Token: 0x0400015A RID: 346
		ValueRest,
		// Token: 0x0400015B RID: 347
		String,
		// Token: 0x0400015C RID: 348
		End,
		// Token: 0x0400015D RID: 349
		Epsilon
	}
}
