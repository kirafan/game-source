﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200004E RID: 78
	public class JsonData : IJsonWrapper, IList, ICollection, IEnumerable, IOrderedDictionary, IDictionary, IEquatable<JsonData>
	{
		// Token: 0x17000111 RID: 273
		// (get) Token: 0x06000373 RID: 883 RVA: 0x00007B8E File Offset: 0x00005D8E
		public int Count
		{
			get
			{
				return this.EnsureCollection().Count;
			}
		}

		// Token: 0x17000112 RID: 274
		// (get) Token: 0x06000374 RID: 884 RVA: 0x00007B9B File Offset: 0x00005D9B
		public bool IsArray
		{
			get
			{
				return this.type == JsonType.Array;
			}
		}

		// Token: 0x17000113 RID: 275
		// (get) Token: 0x06000375 RID: 885 RVA: 0x00007BA6 File Offset: 0x00005DA6
		public bool IsBoolean
		{
			get
			{
				return this.type == JsonType.Boolean;
			}
		}

		// Token: 0x17000114 RID: 276
		// (get) Token: 0x06000376 RID: 886 RVA: 0x00007BB2 File Offset: 0x00005DB2
		public bool IsDouble
		{
			get
			{
				return this.type == JsonType.Double;
			}
		}

		// Token: 0x17000115 RID: 277
		// (get) Token: 0x06000377 RID: 887 RVA: 0x00007BBD File Offset: 0x00005DBD
		public bool IsInt
		{
			get
			{
				return this.type == JsonType.Int;
			}
		}

		// Token: 0x17000116 RID: 278
		// (get) Token: 0x06000378 RID: 888 RVA: 0x00007BC8 File Offset: 0x00005DC8
		public bool IsUInt
		{
			get
			{
				return this.type == JsonType.UInt;
			}
		}

		// Token: 0x17000117 RID: 279
		// (get) Token: 0x06000379 RID: 889 RVA: 0x00007BD3 File Offset: 0x00005DD3
		public bool IsLong
		{
			get
			{
				return this.type == JsonType.Long;
			}
		}

		// Token: 0x17000118 RID: 280
		// (get) Token: 0x0600037A RID: 890 RVA: 0x00007BDE File Offset: 0x00005DDE
		public bool IsULong
		{
			get
			{
				return this.type == JsonType.ULong;
			}
		}

		// Token: 0x17000119 RID: 281
		// (get) Token: 0x0600037B RID: 891 RVA: 0x00007BE9 File Offset: 0x00005DE9
		public bool IsObject
		{
			get
			{
				return this.type == JsonType.Object;
			}
		}

		// Token: 0x1700011A RID: 282
		// (get) Token: 0x0600037C RID: 892 RVA: 0x00007BF4 File Offset: 0x00005DF4
		public bool IsString
		{
			get
			{
				return this.type == JsonType.String;
			}
		}

		// Token: 0x1700011B RID: 283
		// (get) Token: 0x0600037D RID: 893 RVA: 0x00007BFF File Offset: 0x00005DFF
		int ICollection.Count
		{
			get
			{
				return this.Count;
			}
		}

		// Token: 0x1700011C RID: 284
		// (get) Token: 0x0600037E RID: 894 RVA: 0x00007C07 File Offset: 0x00005E07
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.EnsureCollection().IsSynchronized;
			}
		}

		// Token: 0x1700011D RID: 285
		// (get) Token: 0x0600037F RID: 895 RVA: 0x00007C14 File Offset: 0x00005E14
		object ICollection.SyncRoot
		{
			get
			{
				return this.EnsureCollection().SyncRoot;
			}
		}

		// Token: 0x1700011E RID: 286
		// (get) Token: 0x06000380 RID: 896 RVA: 0x00007C21 File Offset: 0x00005E21
		bool IDictionary.IsFixedSize
		{
			get
			{
				return this.EnsureDictionary().IsFixedSize;
			}
		}

		// Token: 0x1700011F RID: 287
		// (get) Token: 0x06000381 RID: 897 RVA: 0x00007C2E File Offset: 0x00005E2E
		bool IDictionary.IsReadOnly
		{
			get
			{
				return this.EnsureDictionary().IsReadOnly;
			}
		}

		// Token: 0x17000120 RID: 288
		// (get) Token: 0x06000382 RID: 898 RVA: 0x00007C3C File Offset: 0x00005E3C
		ICollection IDictionary.Keys
		{
			get
			{
				this.EnsureDictionary();
				IList<string> list = new List<string>();
				foreach (KeyValuePair<string, JsonData> keyValuePair in this.object_list)
				{
					list.Add(keyValuePair.Key);
				}
				return (ICollection)list;
			}
		}

		// Token: 0x17000121 RID: 289
		// (get) Token: 0x06000383 RID: 899 RVA: 0x00007CA4 File Offset: 0x00005EA4
		ICollection IDictionary.Values
		{
			get
			{
				this.EnsureDictionary();
				IList<JsonData> list = new List<JsonData>();
				foreach (KeyValuePair<string, JsonData> keyValuePair in this.object_list)
				{
					list.Add(keyValuePair.Value);
				}
				return (ICollection)list;
			}
		}

		// Token: 0x17000122 RID: 290
		// (get) Token: 0x06000384 RID: 900 RVA: 0x00007D0C File Offset: 0x00005F0C
		bool IJsonWrapper.IsArray
		{
			get
			{
				return this.IsArray;
			}
		}

		// Token: 0x17000123 RID: 291
		// (get) Token: 0x06000385 RID: 901 RVA: 0x00007D14 File Offset: 0x00005F14
		bool IJsonWrapper.IsBoolean
		{
			get
			{
				return this.IsBoolean;
			}
		}

		// Token: 0x17000124 RID: 292
		// (get) Token: 0x06000386 RID: 902 RVA: 0x00007D1C File Offset: 0x00005F1C
		bool IJsonWrapper.IsDouble
		{
			get
			{
				return this.IsDouble;
			}
		}

		// Token: 0x17000125 RID: 293
		// (get) Token: 0x06000387 RID: 903 RVA: 0x00007D24 File Offset: 0x00005F24
		bool IJsonWrapper.IsInt
		{
			get
			{
				return this.IsInt;
			}
		}

		// Token: 0x17000126 RID: 294
		// (get) Token: 0x06000388 RID: 904 RVA: 0x00007D2C File Offset: 0x00005F2C
		bool IJsonWrapper.IsLong
		{
			get
			{
				return this.IsLong;
			}
		}

		// Token: 0x17000127 RID: 295
		// (get) Token: 0x06000389 RID: 905 RVA: 0x00007D34 File Offset: 0x00005F34
		bool IJsonWrapper.IsObject
		{
			get
			{
				return this.IsObject;
			}
		}

		// Token: 0x17000128 RID: 296
		// (get) Token: 0x0600038A RID: 906 RVA: 0x00007D3C File Offset: 0x00005F3C
		bool IJsonWrapper.IsString
		{
			get
			{
				return this.IsString;
			}
		}

		// Token: 0x17000129 RID: 297
		// (get) Token: 0x0600038B RID: 907 RVA: 0x00007D44 File Offset: 0x00005F44
		bool IList.IsFixedSize
		{
			get
			{
				return this.EnsureList().IsFixedSize;
			}
		}

		// Token: 0x1700012A RID: 298
		// (get) Token: 0x0600038C RID: 908 RVA: 0x00007D51 File Offset: 0x00005F51
		bool IList.IsReadOnly
		{
			get
			{
				return this.EnsureList().IsReadOnly;
			}
		}

		// Token: 0x1700012B RID: 299
		object IDictionary.this[object key]
		{
			get
			{
				return this.EnsureDictionary()[key];
			}
			set
			{
				if (!(key is string))
				{
					throw new ArgumentException("The key has to be a string");
				}
				JsonData value2 = this.ToJsonData(value);
				this[(string)key] = value2;
			}
		}

		// Token: 0x1700012C RID: 300
		object IOrderedDictionary.this[int idx]
		{
			get
			{
				this.EnsureDictionary();
				return this.object_list[idx].Value;
			}
			set
			{
				this.EnsureDictionary();
				JsonData value2 = this.ToJsonData(value);
				KeyValuePair<string, JsonData> keyValuePair = this.object_list[idx];
				this.inst_object[keyValuePair.Key] = value2;
				KeyValuePair<string, JsonData> value3 = new KeyValuePair<string, JsonData>(keyValuePair.Key, value2);
				this.object_list[idx] = value3;
			}
		}

		// Token: 0x1700012D RID: 301
		object IList.this[int index]
		{
			get
			{
				return this.EnsureList()[index];
			}
			set
			{
				this.EnsureList();
				JsonData value2 = this.ToJsonData(value);
				this[index] = value2;
			}
		}

		// Token: 0x1700012E RID: 302
		// (get) Token: 0x06000393 RID: 915 RVA: 0x00007E58 File Offset: 0x00006058
		public IEnumerable<string> PropertyNames
		{
			get
			{
				this.EnsureDictionary();
				return this.inst_object.Keys;
			}
		}

		// Token: 0x1700012F RID: 303
		public JsonData this[string prop_name]
		{
			get
			{
				this.EnsureDictionary();
				JsonData result = null;
				this.inst_object.TryGetValue(prop_name, out result);
				return result;
			}
			set
			{
				this.EnsureDictionary();
				KeyValuePair<string, JsonData> keyValuePair = new KeyValuePair<string, JsonData>(prop_name, value);
				if (this.inst_object.ContainsKey(prop_name))
				{
					for (int i = 0; i < this.object_list.Count; i++)
					{
						if (this.object_list[i].Key == prop_name)
						{
							this.object_list[i] = keyValuePair;
							break;
						}
					}
				}
				else
				{
					this.object_list.Add(keyValuePair);
				}
				this.inst_object[prop_name] = value;
				this.json = null;
			}
		}

		// Token: 0x17000130 RID: 304
		public JsonData this[int index]
		{
			get
			{
				this.EnsureCollection();
				if (this.type == JsonType.Array)
				{
					return this.inst_array[index];
				}
				return this.object_list[index].Value;
			}
			set
			{
				this.EnsureCollection();
				if (this.type == JsonType.Array)
				{
					this.inst_array[index] = value;
				}
				else
				{
					KeyValuePair<string, JsonData> keyValuePair = this.object_list[index];
					KeyValuePair<string, JsonData> value2 = new KeyValuePair<string, JsonData>(keyValuePair.Key, value);
					this.object_list[index] = value2;
					this.inst_object[keyValuePair.Key] = value;
				}
				this.json = null;
			}
		}

		// Token: 0x06000398 RID: 920 RVA: 0x0000584A File Offset: 0x00003A4A
		public JsonData()
		{
		}

		// Token: 0x06000399 RID: 921 RVA: 0x00007FD3 File Offset: 0x000061D3
		public JsonData(bool boolean)
		{
			this.type = JsonType.Boolean;
			this.inst_boolean = boolean;
		}

		// Token: 0x0600039A RID: 922 RVA: 0x00007FEA File Offset: 0x000061EA
		public JsonData(double number)
		{
			this.type = JsonType.Double;
			this.inst_double = number;
		}

		// Token: 0x0600039B RID: 923 RVA: 0x00008000 File Offset: 0x00006200
		public JsonData(int number)
		{
			this.type = JsonType.Int;
			this.inst_number = (ulong)((long)number);
		}

		// Token: 0x0600039C RID: 924 RVA: 0x00008017 File Offset: 0x00006217
		public JsonData(uint number)
		{
			this.type = JsonType.UInt;
			this.inst_number = (ulong)number;
		}

		// Token: 0x0600039D RID: 925 RVA: 0x0000802E File Offset: 0x0000622E
		public JsonData(long number)
		{
			this.type = JsonType.Long;
			this.inst_number = (ulong)number;
		}

		// Token: 0x0600039E RID: 926 RVA: 0x00008044 File Offset: 0x00006244
		public JsonData(ulong number)
		{
			this.type = JsonType.ULong;
			this.inst_number = number;
		}

		// Token: 0x0600039F RID: 927 RVA: 0x0000805C File Offset: 0x0000625C
		public JsonData(object obj)
		{
			if (obj is bool)
			{
				this.type = JsonType.Boolean;
				this.inst_boolean = (bool)obj;
				return;
			}
			if (obj is double)
			{
				this.type = JsonType.Double;
				this.inst_double = (double)obj;
				return;
			}
			if (obj is int)
			{
				this.type = JsonType.Int;
				this.inst_number = (ulong)obj;
				return;
			}
			if (obj is uint)
			{
				this.type = JsonType.UInt;
				this.inst_number = (ulong)obj;
				return;
			}
			if (obj is long)
			{
				this.type = JsonType.Long;
				this.inst_number = (ulong)obj;
				return;
			}
			if (obj is ulong)
			{
				this.type = JsonType.ULong;
				this.inst_number = (ulong)obj;
				return;
			}
			if (obj is string)
			{
				this.type = JsonType.String;
				this.inst_string = (string)obj;
				return;
			}
			throw new ArgumentException("Unable to wrap the given object with JsonData");
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x0000813E File Offset: 0x0000633E
		public JsonData(string str)
		{
			this.type = JsonType.String;
			this.inst_string = str;
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x00008154 File Offset: 0x00006354
		public static implicit operator JsonData(bool data)
		{
			return new JsonData(data);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x0000815C File Offset: 0x0000635C
		public static implicit operator JsonData(double data)
		{
			return new JsonData(data);
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00008164 File Offset: 0x00006364
		public static implicit operator JsonData(int data)
		{
			return new JsonData(data);
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x0000816C File Offset: 0x0000636C
		public static implicit operator JsonData(long data)
		{
			return new JsonData(data);
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x00008174 File Offset: 0x00006374
		public static implicit operator JsonData(string data)
		{
			return new JsonData(data);
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x0000817C File Offset: 0x0000637C
		public static explicit operator bool(JsonData data)
		{
			if (data.type != JsonType.Boolean)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold a double");
			}
			return data.inst_boolean;
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x00008199 File Offset: 0x00006399
		public static explicit operator double(JsonData data)
		{
			if (data.type != JsonType.Double)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold a double");
			}
			return data.inst_double;
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x000081B5 File Offset: 0x000063B5
		public static explicit operator int(JsonData data)
		{
			if (data.type != JsonType.Int)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold an int");
			}
			return (int)data.inst_number;
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x000081D2 File Offset: 0x000063D2
		public static explicit operator uint(JsonData data)
		{
			if (data.type != JsonType.UInt)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold an int");
			}
			return (uint)data.inst_number;
		}

		// Token: 0x060003AA RID: 938 RVA: 0x000081EF File Offset: 0x000063EF
		public static explicit operator long(JsonData data)
		{
			if (data.type != JsonType.Int && data.type != JsonType.Long)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold an long");
			}
			return (long)data.inst_number;
		}

		// Token: 0x060003AB RID: 939 RVA: 0x00008214 File Offset: 0x00006414
		[CLSCompliant(false)]
		public static explicit operator ulong(JsonData data)
		{
			if (data.type != JsonType.UInt && data.type != JsonType.ULong)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold an long");
			}
			return data.inst_number;
		}

		// Token: 0x060003AC RID: 940 RVA: 0x00008239 File Offset: 0x00006439
		public static explicit operator string(JsonData data)
		{
			if (data.type != JsonType.String)
			{
				throw new InvalidCastException("Instance of JsonData doesn't hold a string");
			}
			return data.inst_string;
		}

		// Token: 0x060003AD RID: 941 RVA: 0x00008255 File Offset: 0x00006455
		void ICollection.CopyTo(Array array, int index)
		{
			this.EnsureCollection().CopyTo(array, index);
		}

		// Token: 0x060003AE RID: 942 RVA: 0x00008264 File Offset: 0x00006464
		void IDictionary.Add(object key, object value)
		{
			JsonData value2 = this.ToJsonData(value);
			this.EnsureDictionary().Add(key, value2);
			KeyValuePair<string, JsonData> item = new KeyValuePair<string, JsonData>((string)key, value2);
			this.object_list.Add(item);
			this.json = null;
		}

		// Token: 0x060003AF RID: 943 RVA: 0x000082A7 File Offset: 0x000064A7
		void IDictionary.Clear()
		{
			this.EnsureDictionary().Clear();
			this.object_list.Clear();
			this.json = null;
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x000082C6 File Offset: 0x000064C6
		bool IDictionary.Contains(object key)
		{
			return this.EnsureDictionary().Contains(key);
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x000082D4 File Offset: 0x000064D4
		IDictionaryEnumerator IDictionary.GetEnumerator()
		{
			return ((IOrderedDictionary)this).GetEnumerator();
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x000082DC File Offset: 0x000064DC
		void IDictionary.Remove(object key)
		{
			this.EnsureDictionary().Remove(key);
			for (int i = 0; i < this.object_list.Count; i++)
			{
				if (this.object_list[i].Key == (string)key)
				{
					this.object_list.RemoveAt(i);
					break;
				}
			}
			this.json = null;
		}

		// Token: 0x060003B3 RID: 947 RVA: 0x00008341 File Offset: 0x00006541
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.EnsureCollection().GetEnumerator();
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x0000834E File Offset: 0x0000654E
		bool IJsonWrapper.GetBoolean()
		{
			if (this.type != JsonType.Boolean)
			{
				throw new InvalidOperationException("JsonData instance doesn't hold a boolean");
			}
			return this.inst_boolean;
		}

		// Token: 0x060003B5 RID: 949 RVA: 0x0000836B File Offset: 0x0000656B
		double IJsonWrapper.GetDouble()
		{
			if (this.type != JsonType.Double)
			{
				throw new InvalidOperationException("JsonData instance doesn't hold a double");
			}
			return this.inst_double;
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x00008387 File Offset: 0x00006587
		int IJsonWrapper.GetInt()
		{
			if (this.type != JsonType.Int)
			{
				throw new InvalidOperationException("JsonData instance doesn't hold an int");
			}
			return (int)this.inst_number;
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x000083A4 File Offset: 0x000065A4
		uint IJsonWrapper.GetUInt()
		{
			if (this.type != JsonType.UInt)
			{
				throw new InvalidOperationException("JsonData instance doesn't hold an int");
			}
			return (uint)this.inst_number;
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x000083C1 File Offset: 0x000065C1
		long IJsonWrapper.GetLong()
		{
			if (this.type != JsonType.Long)
			{
				throw new InvalidOperationException("JsonData instance doesn't hold a long");
			}
			return (long)this.inst_number;
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x000083DD File Offset: 0x000065DD
		ulong IJsonWrapper.GetULong()
		{
			if (this.type != JsonType.ULong)
			{
				throw new InvalidOperationException("JsonData instance doesn't hold a long");
			}
			return this.inst_number;
		}

		// Token: 0x060003BA RID: 954 RVA: 0x000083F9 File Offset: 0x000065F9
		string IJsonWrapper.GetString()
		{
			if (this.type != JsonType.String)
			{
				throw new InvalidOperationException("JsonData instance doesn't hold a string");
			}
			return this.inst_string;
		}

		// Token: 0x060003BB RID: 955 RVA: 0x00008415 File Offset: 0x00006615
		void IJsonWrapper.SetBoolean(bool val)
		{
			this.type = JsonType.Boolean;
			this.inst_boolean = val;
			this.json = null;
		}

		// Token: 0x060003BC RID: 956 RVA: 0x0000842D File Offset: 0x0000662D
		void IJsonWrapper.SetDouble(double val)
		{
			this.type = JsonType.Double;
			this.inst_double = val;
			this.json = null;
		}

		// Token: 0x060003BD RID: 957 RVA: 0x00008444 File Offset: 0x00006644
		void IJsonWrapper.SetInt(int val)
		{
			this.type = JsonType.Int;
			this.inst_number = (ulong)((long)val);
			this.json = null;
		}

		// Token: 0x060003BE RID: 958 RVA: 0x0000845C File Offset: 0x0000665C
		void IJsonWrapper.SetUInt(uint val)
		{
			this.type = JsonType.UInt;
			this.inst_number = (ulong)val;
			this.json = null;
		}

		// Token: 0x060003BF RID: 959 RVA: 0x00008474 File Offset: 0x00006674
		void IJsonWrapper.SetLong(long val)
		{
			this.type = JsonType.Long;
			this.inst_number = (ulong)val;
			this.json = null;
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x0000848B File Offset: 0x0000668B
		void IJsonWrapper.SetULong(ulong val)
		{
			this.type = JsonType.ULong;
			this.inst_number = val;
			this.json = null;
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x000084A2 File Offset: 0x000066A2
		void IJsonWrapper.SetString(string val)
		{
			this.type = JsonType.String;
			this.inst_string = val;
			this.json = null;
		}

		// Token: 0x060003C2 RID: 962 RVA: 0x000084B9 File Offset: 0x000066B9
		string IJsonWrapper.ToJson()
		{
			return this.ToJson();
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x000084C1 File Offset: 0x000066C1
		void IJsonWrapper.ToJson(JsonWriter writer)
		{
			this.ToJson(writer);
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x000084CA File Offset: 0x000066CA
		int IList.Add(object value)
		{
			return this.Add(value);
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x000084D3 File Offset: 0x000066D3
		void IList.Clear()
		{
			this.EnsureList().Clear();
			this.json = null;
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x000084E7 File Offset: 0x000066E7
		bool IList.Contains(object value)
		{
			return this.EnsureList().Contains(value);
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x000084F5 File Offset: 0x000066F5
		int IList.IndexOf(object value)
		{
			return this.EnsureList().IndexOf(value);
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x00008503 File Offset: 0x00006703
		void IList.Insert(int index, object value)
		{
			this.EnsureList().Insert(index, value);
			this.json = null;
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x00008519 File Offset: 0x00006719
		void IList.Remove(object value)
		{
			this.EnsureList().Remove(value);
			this.json = null;
		}

		// Token: 0x060003CA RID: 970 RVA: 0x0000852E File Offset: 0x0000672E
		void IList.RemoveAt(int index)
		{
			this.EnsureList().RemoveAt(index);
			this.json = null;
		}

		// Token: 0x060003CB RID: 971 RVA: 0x00008543 File Offset: 0x00006743
		IDictionaryEnumerator IOrderedDictionary.GetEnumerator()
		{
			this.EnsureDictionary();
			return new OrderedDictionaryEnumerator(this.object_list.GetEnumerator());
		}

		// Token: 0x060003CC RID: 972 RVA: 0x0000855C File Offset: 0x0000675C
		void IOrderedDictionary.Insert(int idx, object key, object value)
		{
			string text = (string)key;
			JsonData value2 = this.ToJsonData(value);
			this[text] = value2;
			KeyValuePair<string, JsonData> item = new KeyValuePair<string, JsonData>(text, value2);
			this.object_list.Insert(idx, item);
		}

		// Token: 0x060003CD RID: 973 RVA: 0x00008598 File Offset: 0x00006798
		void IOrderedDictionary.RemoveAt(int idx)
		{
			this.EnsureDictionary();
			this.inst_object.Remove(this.object_list[idx].Key);
			this.object_list.RemoveAt(idx);
		}

		// Token: 0x060003CE RID: 974 RVA: 0x000085D8 File Offset: 0x000067D8
		private ICollection EnsureCollection()
		{
			if (this.type == JsonType.Array)
			{
				return (ICollection)this.inst_array;
			}
			if (this.type == JsonType.Object)
			{
				return (ICollection)this.inst_object;
			}
			throw new InvalidOperationException("The JsonData instance has to be initialized first");
		}

		// Token: 0x060003CF RID: 975 RVA: 0x00008610 File Offset: 0x00006810
		private IDictionary EnsureDictionary()
		{
			if (this.type == JsonType.Object)
			{
				return (IDictionary)this.inst_object;
			}
			if (this.type != JsonType.None)
			{
				throw new InvalidOperationException("Instance of JsonData is not a dictionary");
			}
			this.type = JsonType.Object;
			this.inst_object = new Dictionary<string, JsonData>();
			this.object_list = new List<KeyValuePair<string, JsonData>>();
			return (IDictionary)this.inst_object;
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x00008670 File Offset: 0x00006870
		private IList EnsureList()
		{
			if (this.type == JsonType.Array)
			{
				return (IList)this.inst_array;
			}
			if (this.type != JsonType.None)
			{
				throw new InvalidOperationException("Instance of JsonData is not a list");
			}
			this.type = JsonType.Array;
			this.inst_array = new List<JsonData>();
			return (IList)this.inst_array;
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x000086C2 File Offset: 0x000068C2
		private JsonData ToJsonData(object obj)
		{
			if (obj == null)
			{
				return null;
			}
			if (obj is JsonData)
			{
				return (JsonData)obj;
			}
			return new JsonData(obj);
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x000086E0 File Offset: 0x000068E0
		private static void WriteJson(IJsonWrapper obj, JsonWriter writer)
		{
			if (obj == null)
			{
				writer.Write(null);
				return;
			}
			if (obj.IsString)
			{
				writer.Write(obj.GetString());
				return;
			}
			if (obj.IsBoolean)
			{
				writer.Write(obj.GetBoolean());
				return;
			}
			if (obj.IsDouble)
			{
				writer.Write(obj.GetDouble());
				return;
			}
			if (obj.IsInt)
			{
				writer.Write(obj.GetInt());
				return;
			}
			if (obj.IsUInt)
			{
				writer.Write(obj.GetUInt());
				return;
			}
			if (obj.IsLong)
			{
				writer.Write(obj.GetLong());
				return;
			}
			if (obj.IsULong)
			{
				writer.Write(obj.GetULong());
				return;
			}
			if (obj.IsArray)
			{
				writer.WriteArrayStart();
				foreach (object obj2 in obj)
				{
					JsonData.WriteJson((JsonData)obj2, writer);
				}
				writer.WriteArrayEnd();
				return;
			}
			if (obj.IsObject)
			{
				writer.WriteObjectStart();
				foreach (object obj3 in obj)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)obj3;
					writer.WritePropertyName((string)dictionaryEntry.Key);
					JsonData.WriteJson((JsonData)dictionaryEntry.Value, writer);
				}
				writer.WriteObjectEnd();
				return;
			}
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x0000885C File Offset: 0x00006A5C
		public int Add(object value)
		{
			JsonData value2 = this.ToJsonData(value);
			this.json = null;
			return this.EnsureList().Add(value2);
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x00008884 File Offset: 0x00006A84
		public void Clear()
		{
			if (this.IsObject)
			{
				((IDictionary)this).Clear();
				return;
			}
			if (this.IsArray)
			{
				((IList)this).Clear();
				return;
			}
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x000088A4 File Offset: 0x00006AA4
		public bool Equals(JsonData x)
		{
			if (x == null)
			{
				return false;
			}
			if (x.type != this.type)
			{
				bool flag = this.type == JsonType.Int || this.type == JsonType.Long;
				bool flag2 = this.type == JsonType.UInt || this.type == JsonType.ULong;
				bool flag3 = x.type == JsonType.Int || x.type == JsonType.Long;
				bool flag4 = x.type == JsonType.UInt || x.type == JsonType.ULong;
				if (flag != flag3 && flag2 != flag4)
				{
					return false;
				}
			}
			switch (this.type)
			{
			case JsonType.None:
				return true;
			case JsonType.Object:
				return this.inst_object.Equals(x.inst_object);
			case JsonType.Array:
				return this.inst_array.Equals(x.inst_array);
			case JsonType.String:
				return this.inst_string.Equals(x.inst_string);
			case JsonType.Int:
			case JsonType.UInt:
			case JsonType.Long:
			case JsonType.ULong:
				return this.inst_number.Equals(x.inst_number);
			case JsonType.Double:
				return this.inst_double.Equals(x.inst_double);
			case JsonType.Boolean:
				return this.inst_boolean.Equals(x.inst_boolean);
			default:
				return false;
			}
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x000089CA File Offset: 0x00006BCA
		public JsonType GetJsonType()
		{
			return this.type;
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x000089D4 File Offset: 0x00006BD4
		public void SetJsonType(JsonType type)
		{
			if (this.type == type)
			{
				return;
			}
			switch (type)
			{
			case JsonType.Object:
				this.inst_object = new Dictionary<string, JsonData>();
				this.object_list = new List<KeyValuePair<string, JsonData>>();
				break;
			case JsonType.Array:
				this.inst_array = new List<JsonData>();
				break;
			case JsonType.String:
				this.inst_string = null;
				break;
			case JsonType.Int:
				this.inst_number = 0UL;
				break;
			case JsonType.UInt:
				this.inst_number = 0UL;
				break;
			case JsonType.Long:
				this.inst_number = 0UL;
				break;
			case JsonType.ULong:
				this.inst_number = 0UL;
				break;
			case JsonType.Double:
				this.inst_double = 0.0;
				break;
			case JsonType.Boolean:
				this.inst_boolean = false;
				break;
			}
			this.type = type;
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x00008A90 File Offset: 0x00006C90
		public string ToJson()
		{
			if (this.json != null)
			{
				return this.json;
			}
			StringWriter stringWriter = new StringWriter();
			JsonData.WriteJson(this, new JsonWriter(stringWriter)
			{
				Validate = false
			});
			this.json = stringWriter.ToString();
			return this.json;
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x00008ADC File Offset: 0x00006CDC
		public void ToJson(JsonWriter writer)
		{
			bool validate = writer.Validate;
			writer.Validate = false;
			JsonData.WriteJson(this, writer);
			writer.Validate = validate;
		}

		// Token: 0x060003DA RID: 986 RVA: 0x00008B08 File Offset: 0x00006D08
		public override string ToString()
		{
			switch (this.type)
			{
			case JsonType.Object:
				return "JsonData object";
			case JsonType.Array:
				return "JsonData array";
			case JsonType.String:
				return this.inst_string;
			case JsonType.Int:
				return ((int)this.inst_number).ToString();
			case JsonType.UInt:
				return ((uint)this.inst_number).ToString();
			case JsonType.Long:
			{
				long num = (long)this.inst_number;
				return num.ToString();
			}
			case JsonType.ULong:
				return this.inst_number.ToString();
			case JsonType.Double:
				return this.inst_double.ToString();
			case JsonType.Boolean:
				return this.inst_boolean.ToString();
			default:
				return "Uninitialized JsonData";
			}
		}

		// Token: 0x040000E2 RID: 226
		private IList<JsonData> inst_array;

		// Token: 0x040000E3 RID: 227
		private bool inst_boolean;

		// Token: 0x040000E4 RID: 228
		private double inst_double;

		// Token: 0x040000E5 RID: 229
		private ulong inst_number;

		// Token: 0x040000E6 RID: 230
		private IDictionary<string, JsonData> inst_object;

		// Token: 0x040000E7 RID: 231
		private string inst_string;

		// Token: 0x040000E8 RID: 232
		private string json;

		// Token: 0x040000E9 RID: 233
		private JsonType type;

		// Token: 0x040000EA RID: 234
		private IList<KeyValuePair<string, JsonData>> object_list;
	}
}
