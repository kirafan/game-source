﻿using System;
using System.Reflection;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x02000051 RID: 81
	internal struct PropertyMetadata
	{
		// Token: 0x040000EC RID: 236
		public MemberInfo Info;

		// Token: 0x040000ED RID: 237
		public bool IsField;

		// Token: 0x040000EE RID: 238
		public Type Type;
	}
}
