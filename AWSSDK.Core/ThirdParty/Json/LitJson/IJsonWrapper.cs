﻿using System;
using System.Collections;
using System.Collections.Specialized;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200004D RID: 77
	public interface IJsonWrapper : IList, ICollection, IEnumerable, IOrderedDictionary, IDictionary
	{
		// Token: 0x17000108 RID: 264
		// (get) Token: 0x06000358 RID: 856
		bool IsArray { get; }

		// Token: 0x17000109 RID: 265
		// (get) Token: 0x06000359 RID: 857
		bool IsBoolean { get; }

		// Token: 0x1700010A RID: 266
		// (get) Token: 0x0600035A RID: 858
		bool IsDouble { get; }

		// Token: 0x1700010B RID: 267
		// (get) Token: 0x0600035B RID: 859
		bool IsInt { get; }

		// Token: 0x1700010C RID: 268
		// (get) Token: 0x0600035C RID: 860
		bool IsUInt { get; }

		// Token: 0x1700010D RID: 269
		// (get) Token: 0x0600035D RID: 861
		bool IsLong { get; }

		// Token: 0x1700010E RID: 270
		// (get) Token: 0x0600035E RID: 862
		bool IsULong { get; }

		// Token: 0x1700010F RID: 271
		// (get) Token: 0x0600035F RID: 863
		bool IsObject { get; }

		// Token: 0x17000110 RID: 272
		// (get) Token: 0x06000360 RID: 864
		bool IsString { get; }

		// Token: 0x06000361 RID: 865
		bool GetBoolean();

		// Token: 0x06000362 RID: 866
		double GetDouble();

		// Token: 0x06000363 RID: 867
		int GetInt();

		// Token: 0x06000364 RID: 868
		uint GetUInt();

		// Token: 0x06000365 RID: 869
		JsonType GetJsonType();

		// Token: 0x06000366 RID: 870
		long GetLong();

		// Token: 0x06000367 RID: 871
		ulong GetULong();

		// Token: 0x06000368 RID: 872
		string GetString();

		// Token: 0x06000369 RID: 873
		void SetBoolean(bool val);

		// Token: 0x0600036A RID: 874
		void SetDouble(double val);

		// Token: 0x0600036B RID: 875
		void SetInt(int val);

		// Token: 0x0600036C RID: 876
		void SetUInt(uint val);

		// Token: 0x0600036D RID: 877
		void SetJsonType(JsonType type);

		// Token: 0x0600036E RID: 878
		void SetLong(long val);

		// Token: 0x0600036F RID: 879
		void SetULong(ulong val);

		// Token: 0x06000370 RID: 880
		void SetString(string val);

		// Token: 0x06000371 RID: 881
		string ToJson();

		// Token: 0x06000372 RID: 882
		void ToJson(JsonWriter writer);
	}
}
