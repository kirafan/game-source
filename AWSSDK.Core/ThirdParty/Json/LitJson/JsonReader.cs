﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ThirdParty.Json.LitJson
{
	// Token: 0x0200005B RID: 91
	public class JsonReader
	{
		// Token: 0x1700013B RID: 315
		// (get) Token: 0x06000423 RID: 1059 RVA: 0x0000A3F0 File Offset: 0x000085F0
		// (set) Token: 0x06000424 RID: 1060 RVA: 0x0000A3FD File Offset: 0x000085FD
		public bool AllowComments
		{
			get
			{
				return this.lexer.AllowComments;
			}
			set
			{
				this.lexer.AllowComments = value;
			}
		}

		// Token: 0x1700013C RID: 316
		// (get) Token: 0x06000425 RID: 1061 RVA: 0x0000A40B File Offset: 0x0000860B
		// (set) Token: 0x06000426 RID: 1062 RVA: 0x0000A418 File Offset: 0x00008618
		public bool AllowSingleQuotedStrings
		{
			get
			{
				return this.lexer.AllowSingleQuotedStrings;
			}
			set
			{
				this.lexer.AllowSingleQuotedStrings = value;
			}
		}

		// Token: 0x1700013D RID: 317
		// (get) Token: 0x06000427 RID: 1063 RVA: 0x0000A426 File Offset: 0x00008626
		public bool EndOfInput
		{
			get
			{
				return this.end_of_input;
			}
		}

		// Token: 0x1700013E RID: 318
		// (get) Token: 0x06000428 RID: 1064 RVA: 0x0000A42E File Offset: 0x0000862E
		public bool EndOfJson
		{
			get
			{
				return this.end_of_json;
			}
		}

		// Token: 0x1700013F RID: 319
		// (get) Token: 0x06000429 RID: 1065 RVA: 0x0000A436 File Offset: 0x00008636
		public JsonToken Token
		{
			get
			{
				return this.token;
			}
		}

		// Token: 0x17000140 RID: 320
		// (get) Token: 0x0600042A RID: 1066 RVA: 0x0000A43E File Offset: 0x0000863E
		public object Value
		{
			get
			{
				return this.token_value;
			}
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x0000A446 File Offset: 0x00008646
		public JsonReader(string json_text) : this(new StringReader(json_text), true)
		{
		}

		// Token: 0x0600042C RID: 1068 RVA: 0x0000A455 File Offset: 0x00008655
		public JsonReader(TextReader reader) : this(reader, false)
		{
		}

		// Token: 0x0600042D RID: 1069 RVA: 0x0000A460 File Offset: 0x00008660
		private JsonReader(TextReader reader, bool owned)
		{
			if (reader == null)
			{
				throw new ArgumentNullException("reader");
			}
			this.parser_in_string = false;
			this.parser_return = false;
			this.read_started = false;
			this.lexer = new Lexer(reader);
			this.end_of_input = false;
			this.end_of_json = false;
			this.reader = reader;
			this.reader_is_owned = owned;
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x0000A4CC File Offset: 0x000086CC
		private void ProcessNumber(string number)
		{
			double num;
			if ((number.IndexOf('.') != -1 || number.IndexOf('e') != -1 || number.IndexOf('E') != -1) && double.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out num))
			{
				this.token = JsonToken.Double;
				this.token_value = num;
				return;
			}
			int num2;
			if (int.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out num2))
			{
				this.token = JsonToken.Int;
				this.token_value = num2;
				return;
			}
			uint num3;
			if (uint.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out num3))
			{
				this.token = JsonToken.UInt;
				this.token_value = num3;
				return;
			}
			long num4;
			if (long.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out num4))
			{
				this.token = JsonToken.Long;
				this.token_value = num4;
				return;
			}
			ulong num5;
			if (ulong.TryParse(number, NumberStyles.Any, CultureInfo.InvariantCulture, out num5))
			{
				this.token = JsonToken.ULong;
				this.token_value = num5;
				return;
			}
			this.token = JsonToken.ULong;
			this.token_value = 0UL;
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x0000A5DC File Offset: 0x000087DC
		private void ProcessSymbol()
		{
			if (this.current_symbol == 91)
			{
				this.token = JsonToken.ArrayStart;
				this.parser_return = true;
				return;
			}
			if (this.current_symbol == 93)
			{
				this.token = JsonToken.ArrayEnd;
				this.parser_return = true;
				return;
			}
			if (this.current_symbol == 123)
			{
				this.token = JsonToken.ObjectStart;
				this.parser_return = true;
				return;
			}
			if (this.current_symbol == 125)
			{
				this.token = JsonToken.ObjectEnd;
				this.parser_return = true;
				return;
			}
			if (this.current_symbol == 34)
			{
				if (this.parser_in_string)
				{
					this.parser_in_string = false;
					this.parser_return = true;
					return;
				}
				if (this.token == JsonToken.None)
				{
					this.token = JsonToken.String;
				}
				this.parser_in_string = true;
				return;
			}
			else
			{
				if (this.current_symbol == 65541)
				{
					this.token_value = this.lexer.StringValue;
					return;
				}
				if (this.current_symbol == 65539)
				{
					this.token = JsonToken.Boolean;
					this.token_value = false;
					this.parser_return = true;
					return;
				}
				if (this.current_symbol == 65540)
				{
					this.token = JsonToken.Null;
					this.parser_return = true;
					return;
				}
				if (this.current_symbol == 65537)
				{
					this.ProcessNumber(this.lexer.StringValue);
					this.parser_return = true;
					return;
				}
				if (this.current_symbol == 65546)
				{
					this.token = JsonToken.PropertyName;
					return;
				}
				if (this.current_symbol == 65538)
				{
					this.token = JsonToken.Boolean;
					this.token_value = true;
					this.parser_return = true;
				}
				return;
			}
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x0000A74E File Offset: 0x0000894E
		private bool ReadToken()
		{
			if (this.end_of_input)
			{
				return false;
			}
			this.lexer.NextToken();
			if (this.lexer.EndOfInput)
			{
				this.Close();
				return false;
			}
			this.current_input = this.lexer.Token;
			return true;
		}

		// Token: 0x06000431 RID: 1073 RVA: 0x0000A78D File Offset: 0x0000898D
		public void Close()
		{
			if (this.end_of_input)
			{
				return;
			}
			this.end_of_input = true;
			this.end_of_json = true;
			this.reader = null;
		}

		// Token: 0x06000432 RID: 1074 RVA: 0x0000A7B0 File Offset: 0x000089B0
		public bool Read()
		{
			if (this.end_of_input)
			{
				return false;
			}
			if (this.end_of_json)
			{
				this.end_of_json = false;
			}
			this.token = JsonToken.None;
			this.parser_in_string = false;
			this.parser_return = false;
			if (!this.read_started)
			{
				this.read_started = true;
				if (!this.ReadToken())
				{
					return false;
				}
			}
			for (;;)
			{
				this.current_symbol = this.current_input;
				this.ProcessSymbol();
				if (this.parser_return)
				{
					break;
				}
				if (!this.ReadToken())
				{
					goto Block_34;
				}
			}
			if (this.token == JsonToken.ObjectStart || this.token == JsonToken.ArrayStart)
			{
				this.depth.Push(this.token);
			}
			else if (this.token == JsonToken.ObjectEnd || this.token == JsonToken.ArrayEnd)
			{
				if (this.depth.Peek() == JsonToken.PropertyName)
				{
					this.depth.Pop();
				}
				JsonToken jsonToken = this.depth.Pop();
				if (this.token == JsonToken.ObjectEnd && jsonToken != JsonToken.ObjectStart)
				{
					throw new JsonException("Error: Current token is ObjectEnd which does not match the opening " + jsonToken.ToString());
				}
				if (this.token == JsonToken.ArrayEnd && jsonToken != JsonToken.ArrayStart)
				{
					throw new JsonException("Error: Current token is ArrayEnd which does not match the opening " + jsonToken.ToString());
				}
				if (this.depth.Count == 0)
				{
					this.end_of_json = true;
				}
			}
			else if (this.depth.Count > 0 && this.depth.Peek() != JsonToken.PropertyName && this.token == JsonToken.String && this.depth.Peek() == JsonToken.ObjectStart)
			{
				this.token = JsonToken.PropertyName;
				this.depth.Push(this.token);
			}
			if (this.token == JsonToken.ObjectEnd || this.token == JsonToken.ArrayEnd || this.token == JsonToken.String || this.token == JsonToken.Boolean || this.token == JsonToken.Double || this.token == JsonToken.Int || this.token == JsonToken.UInt || this.token == JsonToken.Long || this.token == JsonToken.ULong || this.token == JsonToken.Null || this.token == JsonToken.String)
			{
				if (this.depth.Count == 0)
				{
					if (this.token != JsonToken.ArrayEnd && this.token != JsonToken.ObjectEnd)
					{
						throw new JsonException("Value without enclosing object or array");
					}
				}
				else if (this.depth.Peek() == JsonToken.PropertyName)
				{
					this.depth.Pop();
				}
			}
			if (!this.ReadToken() && this.depth.Count != 0)
			{
				throw new JsonException("Incomplete JSON Document");
			}
			return true;
			Block_34:
			if (this.depth.Count != 0)
			{
				throw new JsonException("Incomplete JSON Document");
			}
			this.end_of_input = true;
			return false;
		}

		// Token: 0x04000115 RID: 277
		private Stack<JsonToken> depth = new Stack<JsonToken>();

		// Token: 0x04000116 RID: 278
		private int current_input;

		// Token: 0x04000117 RID: 279
		private int current_symbol;

		// Token: 0x04000118 RID: 280
		private bool end_of_json;

		// Token: 0x04000119 RID: 281
		private bool end_of_input;

		// Token: 0x0400011A RID: 282
		private Lexer lexer;

		// Token: 0x0400011B RID: 283
		private bool parser_in_string;

		// Token: 0x0400011C RID: 284
		private bool parser_return;

		// Token: 0x0400011D RID: 285
		private bool read_started;

		// Token: 0x0400011E RID: 286
		private TextReader reader;

		// Token: 0x0400011F RID: 287
		private bool reader_is_owned;

		// Token: 0x04000120 RID: 288
		private object token_value;

		// Token: 0x04000121 RID: 289
		private JsonToken token;
	}
}
