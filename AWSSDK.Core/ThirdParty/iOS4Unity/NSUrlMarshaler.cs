﻿using System;
using System.Runtime.InteropServices;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000029 RID: 41
	public class NSUrlMarshaler : ICustomMarshaler
	{
		// Token: 0x06000201 RID: 513 RVA: 0x000057FE File Offset: 0x000039FE
		public static ICustomMarshaler GetInstance(string cookie)
		{
			return NSUrlMarshaler._instance;
		}

		// Token: 0x06000202 RID: 514 RVA: 0x00005805 File Offset: 0x00003A05
		public void CleanUpManagedData(object managedObj)
		{
		}

		// Token: 0x06000203 RID: 515 RVA: 0x00005805 File Offset: 0x00003A05
		public void CleanUpNativeData(IntPtr pNativeData)
		{
		}

		// Token: 0x06000204 RID: 516 RVA: 0x00005807 File Offset: 0x00003A07
		public int GetNativeDataSize()
		{
			return IntPtr.Size;
		}

		// Token: 0x06000205 RID: 517 RVA: 0x00005810 File Offset: 0x00003A10
		public IntPtr MarshalManagedToNative(object managedObj)
		{
			string text = managedObj as string;
			if (text == null)
			{
				return IntPtr.Zero;
			}
			return ObjC.ToNSUrl(text);
		}

		// Token: 0x06000206 RID: 518 RVA: 0x00005833 File Offset: 0x00003A33
		public object MarshalNativeToManaged(IntPtr pNativeData)
		{
			if (pNativeData == IntPtr.Zero)
			{
				return null;
			}
			return ObjC.FromNSUrl(pNativeData);
		}

		// Token: 0x04000081 RID: 129
		private static readonly NSUrlMarshaler _instance = new NSUrlMarshaler();
	}
}
