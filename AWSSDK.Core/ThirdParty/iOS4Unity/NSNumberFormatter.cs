﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000019 RID: 25
	public class NSNumberFormatter : NSObject
	{
		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000148 RID: 328 RVA: 0x000045C2 File Offset: 0x000027C2
		public override IntPtr ClassHandle
		{
			get
			{
				return NSNumberFormatter._classHandle;
			}
		}

		// Token: 0x06000149 RID: 329 RVA: 0x000045C9 File Offset: 0x000027C9
		public NSNumberFormatter()
		{
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00003A32 File Offset: 0x00001C32
		internal NSNumberFormatter(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x0600014B RID: 331 RVA: 0x000045D1 File Offset: 0x000027D1
		// (set) Token: 0x0600014C RID: 332 RVA: 0x000045E8 File Offset: 0x000027E8
		public NSNumberFormatterBehavior FormatterBehavior
		{
			get
			{
				return (NSNumberFormatterBehavior)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("formatterBehavior"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setFormatterBehavior:"), (int)value);
			}
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x0600014D RID: 333 RVA: 0x00004600 File Offset: 0x00002800
		// (set) Token: 0x0600014E RID: 334 RVA: 0x00004617 File Offset: 0x00002817
		public bool AllowsFloats
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("allowsFloats"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setAllowsFloats:"), value);
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x0600014F RID: 335 RVA: 0x0000462F File Offset: 0x0000282F
		// (set) Token: 0x06000150 RID: 336 RVA: 0x00004646 File Offset: 0x00002846
		public string CurrencyCode
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("currencyCode"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setCurrencyCode:"), value);
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000151 RID: 337 RVA: 0x0000465E File Offset: 0x0000285E
		// (set) Token: 0x06000152 RID: 338 RVA: 0x00004675 File Offset: 0x00002875
		public string CurrencyDecimalSeparator
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("currencyDecimalSeparator"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setCurrencyDecimalSeparator:"), value);
			}
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000153 RID: 339 RVA: 0x0000468D File Offset: 0x0000288D
		// (set) Token: 0x06000154 RID: 340 RVA: 0x000046A4 File Offset: 0x000028A4
		public string CurrencyGroupingSeparator
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("currencyGroupingSeparator"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setCurrencyGroupingSeparator:"), value);
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x06000155 RID: 341 RVA: 0x000046BC File Offset: 0x000028BC
		// (set) Token: 0x06000156 RID: 342 RVA: 0x000046D3 File Offset: 0x000028D3
		public string CurrencySymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("currencySymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setCurrencySymbol:"), value);
			}
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000157 RID: 343 RVA: 0x000046EB File Offset: 0x000028EB
		// (set) Token: 0x06000158 RID: 344 RVA: 0x00004702 File Offset: 0x00002902
		public string DecimalSeparator
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("decimalSeparator"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setDecimalSeparator:"), value);
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000159 RID: 345 RVA: 0x0000471A File Offset: 0x0000291A
		// (set) Token: 0x0600015A RID: 346 RVA: 0x00004730 File Offset: 0x00002930
		public static NSNumberFormatterBehavior DefaultFormatterBehavior
		{
			get
			{
				return (NSNumberFormatterBehavior)ObjC.MessageSendInt(NSNumberFormatter._classHandle, Selector.GetHandle("defaultFormatterBehavior"));
			}
			set
			{
				ObjC.MessageSend(NSNumberFormatter._classHandle, Selector.GetHandle("setDefaultFormatterBehavior:"), (int)value);
			}
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x0600015B RID: 347 RVA: 0x00004747 File Offset: 0x00002947
		// (set) Token: 0x0600015C RID: 348 RVA: 0x0000475E File Offset: 0x0000295E
		public string ExponentSymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("exponentSymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setExponentSymbol:"), value);
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x0600015D RID: 349 RVA: 0x00004776 File Offset: 0x00002976
		// (set) Token: 0x0600015E RID: 350 RVA: 0x0000478D File Offset: 0x0000298D
		[CLSCompliant(false)]
		public uint FormatWidth
		{
			get
			{
				return ObjC.MessageSendUInt(this.Handle, Selector.GetHandle("formatWidth"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setFormatWidth:"), value);
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600015F RID: 351 RVA: 0x000047A7 File Offset: 0x000029A7
		// (set) Token: 0x06000160 RID: 352 RVA: 0x000047BE File Offset: 0x000029BE
		public bool GeneratesDecimalNumbers
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("generatesDecimalNumbers"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setGeneratesDecimalNumbers:"), value);
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000161 RID: 353 RVA: 0x000047D6 File Offset: 0x000029D6
		// (set) Token: 0x06000162 RID: 354 RVA: 0x000047ED File Offset: 0x000029ED
		public string GroupingSeparator
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("groupingSeparator"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setGroupingSeparator:"), value);
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000163 RID: 355 RVA: 0x00004805 File Offset: 0x00002A05
		// (set) Token: 0x06000164 RID: 356 RVA: 0x0000481C File Offset: 0x00002A1C
		public uint GroupingSize
		{
			get
			{
				return ObjC.MessageSendUInt(this.Handle, Selector.GetHandle("groupingSize"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setGroupingSize:"), value);
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000165 RID: 357 RVA: 0x00004836 File Offset: 0x00002A36
		// (set) Token: 0x06000166 RID: 358 RVA: 0x0000484D File Offset: 0x00002A4D
		public string InternationalCurrencySymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("internationalCurrencySymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setInternationalCurrencySymbol:"), value);
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000167 RID: 359 RVA: 0x00004865 File Offset: 0x00002A65
		// (set) Token: 0x06000168 RID: 360 RVA: 0x0000487C File Offset: 0x00002A7C
		public bool Lenient
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isLenient"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setLenient:"), value);
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000169 RID: 361 RVA: 0x00004894 File Offset: 0x00002A94
		// (set) Token: 0x0600016A RID: 362 RVA: 0x000048B0 File Offset: 0x00002AB0
		public NSLocale Locale
		{
			get
			{
				return Runtime.GetNSObject<NSLocale>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("locale")));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setLocale:"), value.Handle);
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600016B RID: 363 RVA: 0x000048CD File Offset: 0x00002ACD
		// (set) Token: 0x0600016C RID: 364 RVA: 0x000048E4 File Offset: 0x00002AE4
		public int MaximumFractionDigits
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("maximumFractionDigits"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setMaximumFractionDigits:"), value);
			}
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600016D RID: 365 RVA: 0x000048FC File Offset: 0x00002AFC
		// (set) Token: 0x0600016E RID: 366 RVA: 0x00004913 File Offset: 0x00002B13
		public int MaximumIntegerDigits
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("maximumIntegerDigits"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setMaximumIntegerDigits:"), value);
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x0600016F RID: 367 RVA: 0x0000492B File Offset: 0x00002B2B
		// (set) Token: 0x06000170 RID: 368 RVA: 0x00004942 File Offset: 0x00002B42
		public uint MaximumSignificantDigits
		{
			get
			{
				return ObjC.MessageSendUInt(this.Handle, Selector.GetHandle("maximumSignificantDigits"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setMaximumSignificantDigits:"), value);
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000171 RID: 369 RVA: 0x0000495C File Offset: 0x00002B5C
		// (set) Token: 0x06000172 RID: 370 RVA: 0x00004973 File Offset: 0x00002B73
		public int MinimumFractionDigits
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("minimumFractionDigits"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setMinimumFractionDigits:"), value);
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000173 RID: 371 RVA: 0x0000498B File Offset: 0x00002B8B
		// (set) Token: 0x06000174 RID: 372 RVA: 0x000049A2 File Offset: 0x00002BA2
		public int MinimumIntegerDigits
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("minimumIntegerDigits"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setMinimumIntegerDigits:"), value);
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x06000175 RID: 373 RVA: 0x000049BA File Offset: 0x00002BBA
		// (set) Token: 0x06000176 RID: 374 RVA: 0x000049D1 File Offset: 0x00002BD1
		public uint MinimumSignificantDigits
		{
			get
			{
				return ObjC.MessageSendUInt(this.Handle, Selector.GetHandle("minimumSignificantDigits"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setMinimumSignificantDigits:"), value);
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x06000177 RID: 375 RVA: 0x000049EB File Offset: 0x00002BEB
		// (set) Token: 0x06000178 RID: 376 RVA: 0x00004A02 File Offset: 0x00002C02
		public string MinusSign
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("minusSign"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setMinusSign:"), value);
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000179 RID: 377 RVA: 0x00004A1A File Offset: 0x00002C1A
		// (set) Token: 0x0600017A RID: 378 RVA: 0x00004A31 File Offset: 0x00002C31
		public string NegativeFormat
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("negativeFormat"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setNegativeFormat:"), value);
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x0600017B RID: 379 RVA: 0x00004A49 File Offset: 0x00002C49
		// (set) Token: 0x0600017C RID: 380 RVA: 0x00004A60 File Offset: 0x00002C60
		public string NegativeInfinitySymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("negativeInfinitySymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setNegativeInfinitySymbol:"), value);
			}
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600017D RID: 381 RVA: 0x00004A78 File Offset: 0x00002C78
		// (set) Token: 0x0600017E RID: 382 RVA: 0x00004A8F File Offset: 0x00002C8F
		public string NegativePrefix
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("negativePrefix"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setNegativePrefix:"), value);
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600017F RID: 383 RVA: 0x00004AA7 File Offset: 0x00002CA7
		// (set) Token: 0x06000180 RID: 384 RVA: 0x00004ABE File Offset: 0x00002CBE
		public string NegativeSuffix
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("negativeSuffix"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setNegativeSuffix:"), value);
			}
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x06000181 RID: 385 RVA: 0x00004AD6 File Offset: 0x00002CD6
		// (set) Token: 0x06000182 RID: 386 RVA: 0x00004AED File Offset: 0x00002CED
		public string NilSymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("nilSymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setNilSymbol:"), value);
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00004B05 File Offset: 0x00002D05
		// (set) Token: 0x06000184 RID: 388 RVA: 0x00004B1C File Offset: 0x00002D1C
		public string NotANumberSymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("notANumberSymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setNotANumberSymbol:"), value);
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x06000185 RID: 389 RVA: 0x00004B34 File Offset: 0x00002D34
		// (set) Token: 0x06000186 RID: 390 RVA: 0x00004B4B File Offset: 0x00002D4B
		public NSNumberFormatterStyle NumberStyle
		{
			get
			{
				return (NSNumberFormatterStyle)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("numberStyle"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setNumberStyle:"), (int)value);
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000187 RID: 391 RVA: 0x00004B63 File Offset: 0x00002D63
		// (set) Token: 0x06000188 RID: 392 RVA: 0x00004B7A File Offset: 0x00002D7A
		public string PaddingCharacter
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("paddingCharacter"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPaddingCharacter:"), value);
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x06000189 RID: 393 RVA: 0x00004B92 File Offset: 0x00002D92
		// (set) Token: 0x0600018A RID: 394 RVA: 0x00004BA9 File Offset: 0x00002DA9
		public NSNumberFormatterPadPosition PaddingPosition
		{
			get
			{
				return (NSNumberFormatterPadPosition)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("paddingPosition"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPaddingPosition:"), (int)value);
			}
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x0600018B RID: 395 RVA: 0x00004BC1 File Offset: 0x00002DC1
		// (set) Token: 0x0600018C RID: 396 RVA: 0x00004BD8 File Offset: 0x00002DD8
		public bool PartialStringValidationEnabled
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isPartialStringValidationEnabled"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPartialStringValidationEnabled:"), value);
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x0600018D RID: 397 RVA: 0x00004BF0 File Offset: 0x00002DF0
		// (set) Token: 0x0600018E RID: 398 RVA: 0x00004C07 File Offset: 0x00002E07
		public string PercentSymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("percentSymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPercentSymbol:"), value);
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x0600018F RID: 399 RVA: 0x00004C1F File Offset: 0x00002E1F
		// (set) Token: 0x06000190 RID: 400 RVA: 0x00004C36 File Offset: 0x00002E36
		public string PerMillSymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("perMillSymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPerMillSymbol:"), value);
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000191 RID: 401 RVA: 0x00004C4E File Offset: 0x00002E4E
		// (set) Token: 0x06000192 RID: 402 RVA: 0x00004C65 File Offset: 0x00002E65
		public string PlusSign
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("plusSign"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPlusSign:"), value);
			}
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x06000193 RID: 403 RVA: 0x00004C7D File Offset: 0x00002E7D
		// (set) Token: 0x06000194 RID: 404 RVA: 0x00004C94 File Offset: 0x00002E94
		public string PositiveFormat
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("positiveFormat"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPositiveFormat:"), value);
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x06000195 RID: 405 RVA: 0x00004CAC File Offset: 0x00002EAC
		// (set) Token: 0x06000196 RID: 406 RVA: 0x00004CC3 File Offset: 0x00002EC3
		public string PositiveInfinitySymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("positiveInfinitySymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPositiveInfinitySymbol:"), value);
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x06000197 RID: 407 RVA: 0x00004CDB File Offset: 0x00002EDB
		// (set) Token: 0x06000198 RID: 408 RVA: 0x00004CF2 File Offset: 0x00002EF2
		public string PositivePrefix
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("positivePrefix"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPositivePrefix:"), value);
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x06000199 RID: 409 RVA: 0x00004D0A File Offset: 0x00002F0A
		// (set) Token: 0x0600019A RID: 410 RVA: 0x00004D21 File Offset: 0x00002F21
		public string PositiveSuffix
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("positiveSuffix"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPositiveSuffix:"), value);
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x0600019B RID: 411 RVA: 0x00004D39 File Offset: 0x00002F39
		// (set) Token: 0x0600019C RID: 412 RVA: 0x00004D50 File Offset: 0x00002F50
		public NSNumberFormatterRoundingMode RoundingMode
		{
			get
			{
				return (NSNumberFormatterRoundingMode)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("roundingMode"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setRoundingMode:"), (int)value);
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x0600019D RID: 413 RVA: 0x00004D68 File Offset: 0x00002F68
		// (set) Token: 0x0600019E RID: 414 RVA: 0x00004D7F File Offset: 0x00002F7F
		public uint SecondaryGroupingSize
		{
			get
			{
				return ObjC.MessageSendUInt(this.Handle, Selector.GetHandle("secondaryGroupingSize"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setSecondaryGroupingSize:"), value);
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x0600019F RID: 415 RVA: 0x00004D99 File Offset: 0x00002F99
		// (set) Token: 0x060001A0 RID: 416 RVA: 0x00004DB0 File Offset: 0x00002FB0
		public bool UsesGroupingSeparator
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("usesGroupingSeparator"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setUsesGroupingSeparator:"), value);
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060001A1 RID: 417 RVA: 0x00004DC8 File Offset: 0x00002FC8
		// (set) Token: 0x060001A2 RID: 418 RVA: 0x00004DDF File Offset: 0x00002FDF
		public bool UsesSignificantDigits
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("usesSignificantDigits"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setUsesSignificantDigits:"), value);
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060001A3 RID: 419 RVA: 0x00004DF7 File Offset: 0x00002FF7
		// (set) Token: 0x060001A4 RID: 420 RVA: 0x00004E0E File Offset: 0x0000300E
		public string ZeroSymbol
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("zeroSymbol"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setZeroSymbol:"), value);
			}
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x00004E26 File Offset: 0x00003026
		public double NumberFromString(string text)
		{
			return ObjC.FromNSNumber(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("numberFromString:"), text));
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x00004E44 File Offset: 0x00003044
		public string StringFromNumber(double number)
		{
			IntPtr intPtr = ObjC.ToNSNumber(number);
			string result = ObjC.MessageSendString(this.Handle, Selector.GetHandle("stringFromNumber:"), intPtr);
			ObjC.MessageSend(intPtr, Selector.ReleaseHandle);
			return result;
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x00004E7C File Offset: 0x0000307C
		public static string LocalizedStringFromNumber(double number, NSNumberFormatterStyle style)
		{
			IntPtr intPtr = ObjC.ToNSNumber(number);
			string result = ObjC.MessageSendString(NSNumberFormatter._classHandle, Selector.GetHandle("localizedStringFromNumber:numberStyle:"), intPtr, (int)style);
			ObjC.MessageSend(intPtr, Selector.ReleaseHandle);
			return result;
		}

		// Token: 0x04000052 RID: 82
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSNumberFormatter");
	}
}
