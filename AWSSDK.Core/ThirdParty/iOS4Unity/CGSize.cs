﻿using System;
using System.Globalization;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000008 RID: 8
	[Serializable]
	public struct CGSize
	{
		// Token: 0x17000009 RID: 9
		// (get) Token: 0x060000B2 RID: 178 RVA: 0x000038B4 File Offset: 0x00001AB4
		public bool IsEmpty
		{
			get
			{
				return (double)this.Width == 0.0 && (double)this.Height == 0.0;
			}
		}

		// Token: 0x060000B3 RID: 179 RVA: 0x000038DC File Offset: 0x00001ADC
		public CGSize(CGPoint pt)
		{
			this.Width = pt.X;
			this.Height = pt.Y;
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000038F6 File Offset: 0x00001AF6
		public CGSize(CGSize size)
		{
			this.Width = size.Width;
			this.Height = size.Height;
		}

		// Token: 0x060000B5 RID: 181 RVA: 0x00003910 File Offset: 0x00001B10
		public CGSize(float width, float height)
		{
			this.Width = width;
			this.Height = height;
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00003920 File Offset: 0x00001B20
		public static CGSize Add(CGSize sz1, CGSize sz2)
		{
			return new CGSize(sz1.Width + sz2.Width, sz1.Height + sz2.Height);
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00003941 File Offset: 0x00001B41
		public static CGSize Subtract(CGSize sz1, CGSize sz2)
		{
			return new CGSize(sz1.Width - sz2.Width, sz1.Height - sz2.Height);
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00003962 File Offset: 0x00001B62
		public override bool Equals(object obj)
		{
			return obj is CGSize && this == (CGSize)obj;
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x0000397F File Offset: 0x00001B7F
		public override int GetHashCode()
		{
			return (int)this.Width ^ (int)this.Height;
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00003990 File Offset: 0x00001B90
		public CGPoint ToCGPoint()
		{
			return new CGPoint(this.Width, this.Height);
		}

		// Token: 0x060000BB RID: 187 RVA: 0x000039A3 File Offset: 0x00001BA3
		public override string ToString()
		{
			return string.Format("{{Width={0}, Height={1}}}", this.Width.ToString(CultureInfo.CurrentCulture), this.Height.ToString(CultureInfo.CurrentCulture));
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00003920 File Offset: 0x00001B20
		public static CGSize operator +(CGSize sz1, CGSize sz2)
		{
			return new CGSize(sz1.Width + sz2.Width, sz1.Height + sz2.Height);
		}

		// Token: 0x060000BD RID: 189 RVA: 0x000039CF File Offset: 0x00001BCF
		public static bool operator ==(CGSize sz1, CGSize sz2)
		{
			return sz1.Width == sz2.Width && sz1.Height == sz2.Height;
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00003990 File Offset: 0x00001B90
		public static explicit operator CGPoint(CGSize size)
		{
			return new CGPoint(size.Width, size.Height);
		}

		// Token: 0x060000BF RID: 191 RVA: 0x000039EF File Offset: 0x00001BEF
		public static bool operator !=(CGSize sz1, CGSize sz2)
		{
			return sz1.Width != sz2.Width || sz1.Height != sz2.Height;
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00003941 File Offset: 0x00001B41
		public static CGSize operator -(CGSize sz1, CGSize sz2)
		{
			return new CGSize(sz1.Width - sz2.Width, sz1.Height - sz2.Height);
		}

		// Token: 0x0400003F RID: 63
		public static readonly CGSize Empty;

		// Token: 0x04000040 RID: 64
		public float Width;

		// Token: 0x04000041 RID: 65
		public float Height;
	}
}
