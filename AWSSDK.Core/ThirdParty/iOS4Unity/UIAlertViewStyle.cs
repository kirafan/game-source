﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200002F RID: 47
	public enum UIAlertViewStyle
	{
		// Token: 0x0400008B RID: 139
		Default,
		// Token: 0x0400008C RID: 140
		SecureTextInput,
		// Token: 0x0400008D RID: 141
		PlainTextInput,
		// Token: 0x0400008E RID: 142
		LoginAndPasswordInput
	}
}
