﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000010 RID: 16
	public class NSBundle : NSObject
	{
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x060000D8 RID: 216 RVA: 0x00003A2B File Offset: 0x00001C2B
		public override IntPtr ClassHandle
		{
			get
			{
				return NSBundle._classHandle;
			}
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00003A32 File Offset: 0x00001C32
		internal NSBundle(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x060000DA RID: 218 RVA: 0x00003A3B File Offset: 0x00001C3B
		public string BundleIdentifier
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("bundleIdentifier"));
			}
		}

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x060000DB RID: 219 RVA: 0x00003A52 File Offset: 0x00001C52
		public string BundlePath
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("bundlePath"));
			}
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00003A69 File Offset: 0x00001C69
		public static NSBundle FromIdentifier(string str)
		{
			return Runtime.GetNSObject<NSBundle>(ObjC.MessageSendIntPtr(NSBundle._classHandle, Selector.GetHandle("bundleWithIdentifier:"), str));
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00003A85 File Offset: 0x00001C85
		public static NSBundle FromPath(string path)
		{
			return Runtime.GetNSObject<NSBundle>(ObjC.MessageSendIntPtr(NSBundle._classHandle, Selector.GetHandle("bundleWithPath:"), path));
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x060000DE RID: 222 RVA: 0x00003AA1 File Offset: 0x00001CA1
		public static NSBundle MainBundle
		{
			get
			{
				return Runtime.GetNSObject<NSBundle>(ObjC.MessageSendIntPtr(NSBundle._classHandle, Selector.GetHandle("mainBundle")));
			}
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00003ABC File Offset: 0x00001CBC
		public string LocalizedString(string key, string value = "", string table = "")
		{
			return ObjC.MessageSendString(this.Handle, Selector.GetHandle("localizedStringForKey:value:table:"), key, value, table);
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x060000E0 RID: 224 RVA: 0x00003AD6 File Offset: 0x00001CD6
		public string ResourcePath
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("resourcePath"));
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x060000E1 RID: 225 RVA: 0x00003AED File Offset: 0x00001CED
		public NSDictionary InfoDictionary
		{
			get
			{
				return Runtime.GetNSObject<NSDictionary>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("infoDictionary")));
			}
		}

		// Token: 0x04000044 RID: 68
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSBundle");
	}
}
