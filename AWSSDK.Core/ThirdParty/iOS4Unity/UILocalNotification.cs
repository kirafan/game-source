﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000039 RID: 57
	public class UILocalNotification : NSObject
	{
		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060002A4 RID: 676 RVA: 0x000065EE File Offset: 0x000047EE
		public override IntPtr ClassHandle
		{
			get
			{
				return UILocalNotification._classHandle;
			}
		}

		// Token: 0x060002A5 RID: 677 RVA: 0x00004FC2 File Offset: 0x000031C2
		public UILocalNotification()
		{
			ObjC.MessageSendIntPtr(this.Handle, Selector.Init);
		}

		// Token: 0x060002A6 RID: 678 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UILocalNotification(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x060002A7 RID: 679 RVA: 0x000065F5 File Offset: 0x000047F5
		// (set) Token: 0x060002A8 RID: 680 RVA: 0x0000660C File Offset: 0x0000480C
		public string AlertAction
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("alertAction"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setAlertAction:"), value);
			}
		}

		// Token: 0x170000CB RID: 203
		// (get) Token: 0x060002A9 RID: 681 RVA: 0x00006624 File Offset: 0x00004824
		// (set) Token: 0x060002AA RID: 682 RVA: 0x0000663B File Offset: 0x0000483B
		public string AlertBody
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("alertBody"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setAlertBody:"), value);
			}
		}

		// Token: 0x170000CC RID: 204
		// (get) Token: 0x060002AB RID: 683 RVA: 0x00006653 File Offset: 0x00004853
		// (set) Token: 0x060002AC RID: 684 RVA: 0x0000666A File Offset: 0x0000486A
		public string AlertLaunchImage
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("alertLaunchImage"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setAlertLaunchImage:"), value);
			}
		}

		// Token: 0x170000CD RID: 205
		// (get) Token: 0x060002AD RID: 685 RVA: 0x00006682 File Offset: 0x00004882
		// (set) Token: 0x060002AE RID: 686 RVA: 0x00006699 File Offset: 0x00004899
		public string AlertTitle
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("alertTitle"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setAlertTitle:"), value);
			}
		}

		// Token: 0x170000CE RID: 206
		// (get) Token: 0x060002AF RID: 687 RVA: 0x00005FAB File Offset: 0x000041AB
		// (set) Token: 0x060002B0 RID: 688 RVA: 0x00005FC2 File Offset: 0x000041C2
		public int ApplicationIconBadgeNumber
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("applicationIconBadgeNumber"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setApplicationIconBadgeNumber:"), value);
			}
		}

		// Token: 0x170000CF RID: 207
		// (get) Token: 0x060002B1 RID: 689 RVA: 0x000066B1 File Offset: 0x000048B1
		// (set) Token: 0x060002B2 RID: 690 RVA: 0x000066C8 File Offset: 0x000048C8
		public string Category
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("category"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setCategory:"), value);
			}
		}

		// Token: 0x170000D0 RID: 208
		// (get) Token: 0x060002B3 RID: 691 RVA: 0x000066E0 File Offset: 0x000048E0
		// (set) Token: 0x060002B4 RID: 692 RVA: 0x000066FC File Offset: 0x000048FC
		public DateTime FireDate
		{
			get
			{
				return (DateTime)ObjC.MessageSendDate(this.Handle, Selector.GetHandle("fireDate"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setFireDate:"), value);
			}
		}

		// Token: 0x170000D1 RID: 209
		// (get) Token: 0x060002B5 RID: 693 RVA: 0x00006719 File Offset: 0x00004919
		// (set) Token: 0x060002B6 RID: 694 RVA: 0x00006730 File Offset: 0x00004930
		public bool HasAction
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("hasAction"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setHasAction:"), value);
			}
		}

		// Token: 0x170000D2 RID: 210
		// (get) Token: 0x060002B7 RID: 695 RVA: 0x00006748 File Offset: 0x00004948
		// (set) Token: 0x060002B8 RID: 696 RVA: 0x0000675F File Offset: 0x0000495F
		public string SoundName
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("soundName"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setSoundName:"), value);
			}
		}

		// Token: 0x170000D3 RID: 211
		// (get) Token: 0x060002B9 RID: 697 RVA: 0x00006777 File Offset: 0x00004977
		// (set) Token: 0x060002BA RID: 698 RVA: 0x00006793 File Offset: 0x00004993
		public NSDictionary UserInfo
		{
			get
			{
				return Runtime.GetNSObject<NSDictionary>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("userInfo")));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setUserInfo:"), (value == null) ? IntPtr.Zero : value.Handle);
			}
		}

		// Token: 0x040000B6 RID: 182
		private static readonly IntPtr _classHandle = ObjC.GetClass("UILocalNotification");
	}
}
