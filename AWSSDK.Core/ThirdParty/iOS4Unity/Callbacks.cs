﻿using System;
using System.Collections.Generic;
using AOT;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000005 RID: 5
	public static class Callbacks
	{
		// Token: 0x06000079 RID: 121 RVA: 0x00002DF0 File Offset: 0x00000FF0
		private static Callbacks.Methods GetMethods(NSObject obj, string selector)
		{
			Dictionary<IntPtr, Callbacks.Methods> dictionary;
			if (!Callbacks._callbacks.TryGetValue(obj.Handle, out dictionary))
			{
				dictionary = (Callbacks._callbacks[obj.Handle] = new Dictionary<IntPtr, Callbacks.Methods>());
			}
			IntPtr selector2 = ObjC.GetSelector(selector);
			Callbacks.Methods result;
			if (!dictionary.TryGetValue(selector2, out result))
			{
				result = (dictionary[selector2] = new Callbacks.Methods(obj));
			}
			return result;
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00002E4C File Offset: 0x0000104C
		public static void Subscribe(NSObject obj, string selector, IntPtrHandler callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.ActionIntPtr = (IntPtrHandler)Delegate.Combine(methods.ActionIntPtr, callback);
			if (!Callbacks._delegates.ContainsKey(selector))
			{
				IntPtrHandler3 intPtrHandler = new IntPtrHandler3(Callbacks.OnCallback);
				if (!ObjC.AddMethod(obj.ClassHandle, Selector.GetHandle(selector), intPtrHandler, "v@:@"))
				{
					throw new InvalidOperationException("AddMethod failed for selector " + selector);
				}
				Callbacks._delegates[selector] = intPtrHandler;
			}
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00002EC8 File Offset: 0x000010C8
		public static void Subscribe(NSObject obj, string selector, EventHandler callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.EventHandler = (EventHandler)Delegate.Combine(methods.EventHandler, callback);
			if (!Callbacks._delegates.ContainsKey(selector))
			{
				IntPtrHandler3 intPtrHandler = new IntPtrHandler3(Callbacks.OnCallback);
				if (!ObjC.AddMethod(obj.ClassHandle, Selector.GetHandle(selector), intPtrHandler, "v@:@"))
				{
					throw new InvalidOperationException("AddMethod failed for selector " + selector);
				}
				Callbacks._delegates[selector] = intPtrHandler;
			}
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00002F44 File Offset: 0x00001144
		public static void Subscribe(NSObject obj, string selector, EventHandler<ButtonEventArgs> callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.EventHandlerInt = (EventHandler<ButtonEventArgs>)Delegate.Combine(methods.EventHandlerInt, callback);
			if (!Callbacks._delegates.ContainsKey(selector))
			{
				Action<IntPtr, IntPtr, IntPtr, int> action = new Action<IntPtr, IntPtr, IntPtr, int>(Callbacks.OnCallbackInt);
				if (!ObjC.AddMethod(obj.ClassHandle, Selector.GetHandle(selector), action, "v@:@l"))
				{
					throw new InvalidOperationException("AddMethod failed for selector " + selector);
				}
				Callbacks._delegates[selector] = action;
			}
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00002FC0 File Offset: 0x000011C0
		public static void Subscribe(NSObject obj, string selector, IntPtrHandler2 callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.ActionIntPtrIntPtr = (IntPtrHandler2)Delegate.Combine(methods.ActionIntPtrIntPtr, callback);
			if (!Callbacks._delegates.ContainsKey(selector))
			{
				IntPtrHandler4 intPtrHandler = new IntPtrHandler4(Callbacks.OnCallbackIntPtrIntPtr);
				if (!ObjC.AddMethod(obj.ClassHandle, Selector.GetHandle(selector), intPtrHandler, "v@:@@"))
				{
					throw new InvalidOperationException("AddMethod failed for selector " + selector);
				}
				Callbacks._delegates[selector] = intPtrHandler;
			}
		}

		// Token: 0x0600007E RID: 126 RVA: 0x0000303C File Offset: 0x0000123C
		public static void Subscribe(NSObject obj, string selector, IntPtrHandler3 callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.ActionIntPtrIntPtrIntPtr = (IntPtrHandler3)Delegate.Combine(methods.ActionIntPtrIntPtrIntPtr, callback);
			if (!Callbacks._delegates.ContainsKey(selector))
			{
				IntPtrHandler5 intPtrHandler = new IntPtrHandler5(Callbacks.OnCallbackIntPtrIntPtrIntPtr);
				if (!ObjC.AddMethod(obj.ClassHandle, Selector.GetHandle(selector), intPtrHandler, "v@:@@@"))
				{
					throw new InvalidOperationException("AddMethod failed for selector " + selector);
				}
				Callbacks._delegates[selector] = intPtrHandler;
			}
		}

		// Token: 0x0600007F RID: 127 RVA: 0x000030B6 File Offset: 0x000012B6
		public static void Unsubscribe(NSObject obj, string selector, Action callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.Action = (Action)Delegate.Remove(methods.Action, callback);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x000030D5 File Offset: 0x000012D5
		public static void Unsubscribe(NSObject obj, string selector, IntPtrHandler callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.ActionIntPtr = (IntPtrHandler)Delegate.Remove(methods.ActionIntPtr, callback);
		}

		// Token: 0x06000081 RID: 129 RVA: 0x000030F4 File Offset: 0x000012F4
		public static void Unsubscribe(NSObject obj, string selector, IntPtrHandler2 callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.ActionIntPtrIntPtr = (IntPtrHandler2)Delegate.Remove(methods.ActionIntPtrIntPtr, callback);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x00003113 File Offset: 0x00001313
		public static void Unsubscribe(NSObject obj, string selector, IntPtrHandler3 callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.ActionIntPtrIntPtrIntPtr = (IntPtrHandler3)Delegate.Remove(methods.ActionIntPtrIntPtrIntPtr, callback);
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00003132 File Offset: 0x00001332
		public static void Unsubscribe(NSObject obj, string selector, EventHandler callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.EventHandler = (EventHandler)Delegate.Remove(methods.EventHandler, callback);
		}

		// Token: 0x06000084 RID: 132 RVA: 0x00003151 File Offset: 0x00001351
		public static void Unsubscribe(NSObject obj, string selector, EventHandler<ButtonEventArgs> callback)
		{
			Callbacks.Methods methods = Callbacks.GetMethods(obj, selector);
			methods.EventHandlerInt = (EventHandler<ButtonEventArgs>)Delegate.Remove(methods.EventHandlerInt, callback);
		}

		// Token: 0x06000085 RID: 133 RVA: 0x00003170 File Offset: 0x00001370
		public static void UnsubscribeAll(NSObject obj)
		{
			Callbacks._callbacks.Remove(obj.Handle);
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00003184 File Offset: 0x00001384
		[MonoPInvokeCallback(typeof(IntPtrHandler3))]
		private static void OnCallback(IntPtr @this, IntPtr selector, IntPtr arg1)
		{
			Dictionary<IntPtr, Callbacks.Methods> dictionary;
			Callbacks.Methods methods;
			if (Callbacks._callbacks.TryGetValue(@this, out dictionary) && dictionary.TryGetValue(selector, out methods))
			{
				Action action = methods.Action;
				if (action != null)
				{
					action();
				}
				IntPtrHandler actionIntPtr = methods.ActionIntPtr;
				if (actionIntPtr != null)
				{
					actionIntPtr(arg1);
				}
				EventHandler eventHandler = methods.EventHandler;
				if (eventHandler != null)
				{
					eventHandler(methods.Object, EventArgs.Empty);
				}
			}
		}

		// Token: 0x06000087 RID: 135 RVA: 0x000031EC File Offset: 0x000013EC
		[MonoPInvokeCallback(typeof(Action<IntPtr, IntPtr, IntPtr, int>))]
		private static void OnCallbackInt(IntPtr @this, IntPtr selector, IntPtr arg1, int arg2)
		{
			Dictionary<IntPtr, Callbacks.Methods> dictionary;
			Callbacks.Methods methods;
			if (Callbacks._callbacks.TryGetValue(@this, out dictionary) && dictionary.TryGetValue(selector, out methods))
			{
				EventHandler<ButtonEventArgs> eventHandlerInt = methods.EventHandlerInt;
				if (eventHandlerInt != null)
				{
					eventHandlerInt(methods.Object, new ButtonEventArgs
					{
						Index = arg2
					});
				}
			}
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00003238 File Offset: 0x00001438
		[MonoPInvokeCallback(typeof(IntPtrHandler4))]
		private static void OnCallbackIntPtrIntPtr(IntPtr @this, IntPtr selector, IntPtr arg1, IntPtr arg2)
		{
			Dictionary<IntPtr, Callbacks.Methods> dictionary;
			Callbacks.Methods methods;
			if (Callbacks._callbacks.TryGetValue(@this, out dictionary) && dictionary.TryGetValue(selector, out methods))
			{
				IntPtrHandler2 actionIntPtrIntPtr = methods.ActionIntPtrIntPtr;
				if (actionIntPtrIntPtr != null)
				{
					actionIntPtrIntPtr(arg1, arg2);
				}
			}
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00003274 File Offset: 0x00001474
		[MonoPInvokeCallback(typeof(IntPtrHandler5))]
		private static void OnCallbackIntPtrIntPtrIntPtr(IntPtr @this, IntPtr selector, IntPtr arg1, IntPtr arg2, IntPtr arg3)
		{
			Dictionary<IntPtr, Callbacks.Methods> dictionary;
			Callbacks.Methods methods;
			if (Callbacks._callbacks.TryGetValue(@this, out dictionary) && dictionary.TryGetValue(selector, out methods))
			{
				IntPtrHandler3 actionIntPtrIntPtrIntPtr = methods.ActionIntPtrIntPtrIntPtr;
				if (actionIntPtrIntPtrIntPtr != null)
				{
					actionIntPtrIntPtrIntPtr(arg1, arg2, arg3);
				}
			}
		}

		// Token: 0x04000035 RID: 53
		private static readonly Dictionary<string, Delegate> _delegates = new Dictionary<string, Delegate>();

		// Token: 0x04000036 RID: 54
		private static readonly Dictionary<IntPtr, Dictionary<IntPtr, Callbacks.Methods>> _callbacks = new Dictionary<IntPtr, Dictionary<IntPtr, Callbacks.Methods>>();

		// Token: 0x020001A9 RID: 425
		private class Methods
		{
			// Token: 0x06000E5B RID: 3675 RVA: 0x000261F2 File Offset: 0x000243F2
			public Methods(object obj)
			{
				this.Object = obj;
			}

			// Token: 0x04000922 RID: 2338
			public Action Action;

			// Token: 0x04000923 RID: 2339
			public IntPtrHandler ActionIntPtr;

			// Token: 0x04000924 RID: 2340
			public IntPtrHandler2 ActionIntPtrIntPtr;

			// Token: 0x04000925 RID: 2341
			public IntPtrHandler3 ActionIntPtrIntPtrIntPtr;

			// Token: 0x04000926 RID: 2342
			public EventHandler EventHandler;

			// Token: 0x04000927 RID: 2343
			public EventHandler<ButtonEventArgs> EventHandlerInt;

			// Token: 0x04000928 RID: 2344
			public readonly object Object;
		}
	}
}
