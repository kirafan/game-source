﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000015 RID: 21
	public class NSLocale : NSObject
	{
		// Token: 0x1700001D RID: 29
		// (get) Token: 0x06000110 RID: 272 RVA: 0x0000402A File Offset: 0x0000222A
		public override IntPtr ClassHandle
		{
			get
			{
				return NSLocale._classHandle;
			}
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00003A32 File Offset: 0x00001C32
		internal NSLocale(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x06000112 RID: 274 RVA: 0x00004031 File Offset: 0x00002231
		public string AlternateQuotationBeginDelimiter
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleAlternateQuotationBeginDelimiterKey"));
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000113 RID: 275 RVA: 0x00004048 File Offset: 0x00002248
		public string AlternateQuotationEndDelimiter
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleAlternateQuotationEndDelimiterKey"));
			}
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x06000114 RID: 276 RVA: 0x0000405F File Offset: 0x0000225F
		public static NSLocale AutoUpdatingCurrentLocale
		{
			get
			{
				return Runtime.GetNSObject<NSLocale>(ObjC.MessageSendIntPtr(NSLocale._classHandle, Selector.GetHandle("autoupdatingCurrentLocale")));
			}
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000115 RID: 277 RVA: 0x0000407A File Offset: 0x0000227A
		public static string[] AvailableLocaleIdentifiers
		{
			get
			{
				return ObjC.FromNSArray(ObjC.MessageSendIntPtr(NSLocale._classHandle, Selector.GetHandle("availableLocaleIdentifiers")));
			}
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000116 RID: 278 RVA: 0x00004095 File Offset: 0x00002295
		public string CollationIdentifier
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleCollationIdentifier"));
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000117 RID: 279 RVA: 0x000040AC File Offset: 0x000022AC
		public string CollatorIdentifier
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleCollatorIdentifier"));
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000118 RID: 280 RVA: 0x000040C3 File Offset: 0x000022C3
		public static string[] CommonISOCurrencyCodes
		{
			get
			{
				return ObjC.FromNSArray(ObjC.MessageSendIntPtr(NSLocale._classHandle, Selector.GetHandle("commonISOCurrencyCodes")));
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000119 RID: 281 RVA: 0x000040DE File Offset: 0x000022DE
		public string CountryCode
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleCountryCode"));
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x0600011A RID: 282 RVA: 0x000040F5 File Offset: 0x000022F5
		public string CurrencyCode
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleCurrencyCode"));
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x0600011B RID: 283 RVA: 0x0000410C File Offset: 0x0000230C
		public string CurrencySymbol
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleCurrencySymbol"));
			}
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x0600011C RID: 284 RVA: 0x00004123 File Offset: 0x00002323
		public static NSLocale CurrentLocale
		{
			get
			{
				return Runtime.GetNSObject<NSLocale>(ObjC.MessageSendIntPtr(NSLocale._classHandle, Selector.GetHandle("currentLocale")));
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600011D RID: 285 RVA: 0x0000413E File Offset: 0x0000233E
		public static string CurrentLocaleDidChangeNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSCurrentLocaleDidChangeNotification");
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600011E RID: 286 RVA: 0x0000414F File Offset: 0x0000234F
		public string DecimalSeparator
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleDecimalSeparator"));
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600011F RID: 287 RVA: 0x00004166 File Offset: 0x00002366
		public string GroupingSeparator
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleGroupingSeparator"));
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000120 RID: 288 RVA: 0x0000417D File Offset: 0x0000237D
		public string Identifier
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleIdentifier"));
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000121 RID: 289 RVA: 0x00004194 File Offset: 0x00002394
		public static string[] ISOCountryCodes
		{
			get
			{
				return ObjC.FromNSArray(ObjC.MessageSendIntPtr(NSLocale._classHandle, Selector.GetHandle("ISOCountryCodes")));
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000122 RID: 290 RVA: 0x000041AF File Offset: 0x000023AF
		public static string[] ISOCurrencyCodes
		{
			get
			{
				return ObjC.FromNSArray(ObjC.MessageSendIntPtr(NSLocale._classHandle, Selector.GetHandle("ISOCurrencyCodes")));
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000123 RID: 291 RVA: 0x000041CA File Offset: 0x000023CA
		public static string[] ISOLanguageCodes
		{
			get
			{
				return ObjC.FromNSArray(ObjC.MessageSendIntPtr(NSLocale._classHandle, Selector.GetHandle("ISOLanguageCodes")));
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000124 RID: 292 RVA: 0x000041E5 File Offset: 0x000023E5
		public string LanguageCode
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleLanguageCode"));
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000125 RID: 293 RVA: 0x000041FC File Offset: 0x000023FC
		public string LocaleIdentifier
		{
			get
			{
				return ObjC.GetStringConstant(this.Handle, "localeIdentifier");
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000126 RID: 294 RVA: 0x0000420E File Offset: 0x0000240E
		public string MeasurementSystem
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleMeasurementSystem"));
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000127 RID: 295 RVA: 0x00004225 File Offset: 0x00002425
		public static string[] PreferredLanguages
		{
			get
			{
				return ObjC.FromNSArray(ObjC.MessageSendIntPtr(NSLocale._classHandle, Selector.GetHandle("preferredLanguages")));
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000128 RID: 296 RVA: 0x00004240 File Offset: 0x00002440
		public string ScriptCode
		{
			get
			{
				return this.ObjectForKey(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleScriptCode"));
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000129 RID: 297 RVA: 0x00004257 File Offset: 0x00002457
		public static NSLocale SystemLocale
		{
			get
			{
				return Runtime.GetNSObject<NSLocale>(ObjC.MessageSendIntPtr(NSLocale._classHandle, Selector.GetHandle("systemLocale")));
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600012A RID: 298 RVA: 0x00004272 File Offset: 0x00002472
		public bool UsesMetricSystem
		{
			get
			{
				return this.ObjectForKeyBool(ObjC.GetStringConstant(ObjC.Libraries.Foundation, "NSLocaleUsesMetricSystem"));
			}
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00004289 File Offset: 0x00002489
		private string ObjectForKey(string key)
		{
			return ObjC.FromNSString(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("objectForKey:"), ObjC.ToNSString(key)));
		}

		// Token: 0x0600012C RID: 300 RVA: 0x000042AB File Offset: 0x000024AB
		private bool ObjectForKeyBool(string key)
		{
			return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("objectForKey:"), ObjC.ToNSString(key));
		}

		// Token: 0x0400004D RID: 77
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSLocale");
	}
}
