﻿using System;
using System.Collections.Generic;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000026 RID: 38
	public class SKProductsRequest : NSObject
	{
		// Token: 0x1700008D RID: 141
		// (get) Token: 0x060001F0 RID: 496 RVA: 0x000055DD File Offset: 0x000037DD
		public override IntPtr ClassHandle
		{
			get
			{
				return SKProductsRequest._classHandle;
			}
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x000055E4 File Offset: 0x000037E4
		public SKProductsRequest(params string[] productIds)
		{
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("initWithProductIdentifiers:"), ObjC.ToNSSet(productIds));
			ObjC.MessageSend(this.Handle, Selector.GetHandle("setDelegate:"), this.Handle);
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x00003A32 File Offset: 0x00001C32
		internal SKProductsRequest(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x00005623 File Offset: 0x00003823
		public void Cancel()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("cancel"));
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x0000563A File Offset: 0x0000383A
		public void Start()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("start"));
		}

		// Token: 0x14000004 RID: 4
		// (add) Token: 0x060001F5 RID: 501 RVA: 0x00005654 File Offset: 0x00003854
		// (remove) Token: 0x060001F6 RID: 502 RVA: 0x000056B4 File Offset: 0x000038B4
		public event EventHandler<SKProductsResponseEventArgs> ReceivedResponse
		{
			add
			{
				if (this._receivedResponse == null)
				{
					this._receivedResponse = new Dictionary<object, IntPtrHandler2>();
				}
				IntPtrHandler2 intPtrHandler = delegate(IntPtr _, IntPtr i)
				{
					value(this, new SKProductsResponseEventArgs
					{
						Response = Runtime.GetNSObject<SKProductsResponse>(i)
					});
				};
				this._receivedResponse[value] = intPtrHandler;
				Callbacks.Subscribe(this, "productsRequest:didReceiveResponse:", intPtrHandler);
			}
			remove
			{
				IntPtrHandler2 callback;
				if (this._receivedResponse != null && this._receivedResponse.TryGetValue(value, out callback))
				{
					this._receivedResponse.Remove(value);
					Callbacks.Unsubscribe(this, "productsRequest:didReceiveResponse:", callback);
				}
			}
		}

		// Token: 0x14000005 RID: 5
		// (add) Token: 0x060001F7 RID: 503 RVA: 0x000056F4 File Offset: 0x000038F4
		// (remove) Token: 0x060001F8 RID: 504 RVA: 0x00005754 File Offset: 0x00003954
		public event EventHandler<NSErrorEventArgs> Failed
		{
			add
			{
				if (this._failed == null)
				{
					this._failed = new Dictionary<object, IntPtrHandler2>();
				}
				IntPtrHandler2 intPtrHandler = delegate(IntPtr _, IntPtr i)
				{
					value(this, new NSErrorEventArgs
					{
						Error = Runtime.GetNSObject<NSError>(i)
					});
				};
				this._failed[value] = intPtrHandler;
				Callbacks.Subscribe(this, "request:didFailWithError:", intPtrHandler);
			}
			remove
			{
				IntPtrHandler2 callback;
				if (this._failed != null && this._failed.TryGetValue(value, out callback))
				{
					this._failed.Remove(value);
					Callbacks.Unsubscribe(this, "request:didFailWithError:", callback);
				}
			}
		}

		// Token: 0x14000006 RID: 6
		// (add) Token: 0x060001F9 RID: 505 RVA: 0x00005792 File Offset: 0x00003992
		// (remove) Token: 0x060001FA RID: 506 RVA: 0x000057A0 File Offset: 0x000039A0
		public event EventHandler Finished
		{
			add
			{
				Callbacks.Subscribe(this, "requestDidFinish:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "requestDidFinish:", value);
			}
		}

		// Token: 0x0400007C RID: 124
		private static readonly IntPtr _classHandle = ObjC.GetClass("SKProductsRequest");

		// Token: 0x0400007D RID: 125
		private Dictionary<object, IntPtrHandler2> _receivedResponse;

		// Token: 0x0400007E RID: 126
		private Dictionary<object, IntPtrHandler2> _failed;
	}
}
