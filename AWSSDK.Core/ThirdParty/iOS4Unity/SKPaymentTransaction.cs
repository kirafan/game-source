﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000023 RID: 35
	public class SKPaymentTransaction : NSObject
	{
		// Token: 0x17000080 RID: 128
		// (get) Token: 0x060001DF RID: 479 RVA: 0x0000548B File Offset: 0x0000368B
		public override IntPtr ClassHandle
		{
			get
			{
				return SKPaymentTransaction._classHandle;
			}
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x00003A32 File Offset: 0x00001C32
		internal SKPaymentTransaction(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x060001E1 RID: 481 RVA: 0x00005494 File Offset: 0x00003694
		public NSError Error
		{
			get
			{
				IntPtr intPtr = ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("error"));
				if (!(intPtr == IntPtr.Zero))
				{
					return Runtime.GetNSObject<NSError>(intPtr);
				}
				return null;
			}
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x000054CC File Offset: 0x000036CC
		public SKPaymentTransaction OriginalTransaction
		{
			get
			{
				IntPtr intPtr = ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("originalTransaction"));
				if (!(intPtr == IntPtr.Zero))
				{
					return Runtime.GetNSObject<SKPaymentTransaction>(intPtr);
				}
				return null;
			}
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x060001E3 RID: 483 RVA: 0x00005504 File Offset: 0x00003704
		public DateTime TransactionDate
		{
			get
			{
				return (DateTime)ObjC.MessageSendDate(this.Handle, Selector.GetHandle("transactionDate"));
			}
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x060001E4 RID: 484 RVA: 0x00005520 File Offset: 0x00003720
		public string TransactionIdentifier
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("transactionIdentifier"));
			}
		}

		// Token: 0x04000074 RID: 116
		private static readonly IntPtr _classHandle = ObjC.GetClass("SKPaymentTransaction");
	}
}
