﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200001C RID: 28
	public enum NSNumberFormatterPadPosition
	{
		// Token: 0x0400005F RID: 95
		BeforePrefix,
		// Token: 0x04000060 RID: 96
		AfterPrefix,
		// Token: 0x04000061 RID: 97
		BeforeSuffix,
		// Token: 0x04000062 RID: 98
		AfterSuffix
	}
}
