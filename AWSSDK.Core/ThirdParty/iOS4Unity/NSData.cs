﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000011 RID: 17
	public class NSData : NSObject
	{
		// Token: 0x17000010 RID: 16
		// (get) Token: 0x060000E3 RID: 227 RVA: 0x00003B1A File Offset: 0x00001D1A
		public override IntPtr ClassHandle
		{
			get
			{
				return NSData._classHandle;
			}
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x00003A32 File Offset: 0x00001C32
		internal NSData(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x060000E5 RID: 229 RVA: 0x00003B24 File Offset: 0x00001D24
		public unsafe static NSData FromArray(byte[] buffer)
		{
			if (buffer.Length == 0)
			{
				return NSData.FromBytes(IntPtr.Zero, 0U);
			}
			fixed (IntPtr* ptr = (IntPtr*)(&buffer[0]))
			{
				return NSData.FromBytes((IntPtr)((void*)ptr), (uint)buffer.Length);
			}
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x00003B58 File Offset: 0x00001D58
		public static NSData FromBytes(IntPtr bytes, uint size)
		{
			return Runtime.GetNSObject<NSData>(ObjC.MessageSendIntPtr(NSData._classHandle, Selector.GetHandle("dataWithBytes:length:"), bytes, size));
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x00003B75 File Offset: 0x00001D75
		public static NSData FromData(NSData source)
		{
			return Runtime.GetNSObject<NSData>(ObjC.MessageSendIntPtr(NSData._classHandle, Selector.GetHandle("dataWithData:"), source.Handle));
		}

		// Token: 0x060000E8 RID: 232 RVA: 0x00003B96 File Offset: 0x00001D96
		public static NSData FromBytesNoCopy(IntPtr bytes, uint size)
		{
			return Runtime.GetNSObject<NSData>(ObjC.MessageSendIntPtr(NSData._classHandle, Selector.GetHandle("dataWithBytesNoCopy:length:"), bytes, size));
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00003BB3 File Offset: 0x00001DB3
		public static NSData FromBytesNoCopy(IntPtr bytes, uint size, bool freeWhenDone)
		{
			return Runtime.GetNSObject<NSData>(ObjC.MessageSendIntPtr(NSData._classHandle, Selector.GetHandle("dataWithBytesNoCopy:length:freeWhenDone:"), bytes, size, freeWhenDone));
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00003BD4 File Offset: 0x00001DD4
		public static NSData FromFile(string path, NSDataReadingOptions mask, out NSError error)
		{
			path = Path.Combine(Application.streamingAssetsPath, path);
			IntPtr intPtr;
			NSData nsobject = Runtime.GetNSObject<NSData>(ObjC.MessageSendIntPtr(NSData._classHandle, Selector.GetHandle("dataWithContentsOfFile:options:error:"), path, (uint)mask, out intPtr));
			error = ((intPtr == IntPtr.Zero) ? null : Runtime.GetNSObject<NSError>(intPtr));
			return nsobject;
		}

		// Token: 0x060000EB RID: 235 RVA: 0x00003C23 File Offset: 0x00001E23
		public static NSData FromFile(string path)
		{
			path = Path.Combine(Application.streamingAssetsPath, path);
			return Runtime.GetNSObject<NSData>(ObjC.MessageSendIntPtr(NSData._classHandle, Selector.GetHandle("dataWithContentsOfFile:"), path));
		}

		// Token: 0x060000EC RID: 236 RVA: 0x00003C4C File Offset: 0x00001E4C
		public static NSData FromUrl(string url)
		{
			return Runtime.GetNSObject<NSData>(ObjC.MessageSendIntPtr_NSUrl(NSData._classHandle, Selector.GetHandle("dataWithContentsOfURL:"), url));
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00003C68 File Offset: 0x00001E68
		public static NSData FromUrl(string url, NSDataReadingOptions mask, out NSError error)
		{
			IntPtr intPtr;
			NSData nsobject = Runtime.GetNSObject<NSData>(ObjC.MessageSendIntPtr_NSUrl(NSData._classHandle, Selector.GetHandle("dataWithContentsOfURL:options:error:"), url, (uint)mask, out intPtr));
			error = ((intPtr == IntPtr.Zero) ? null : Runtime.GetNSObject<NSError>(intPtr));
			return nsobject;
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x060000EE RID: 238 RVA: 0x00003CAA File Offset: 0x00001EAA
		public IntPtr Bytes
		{
			get
			{
				return ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("bytes"));
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x060000EF RID: 239 RVA: 0x00003CC1 File Offset: 0x00001EC1
		// (set) Token: 0x060000F0 RID: 240 RVA: 0x00003CD8 File Offset: 0x00001ED8
		public uint Length
		{
			get
			{
				return ObjC.MessageSendUInt(this.Handle, Selector.GetHandle("length"));
			}
			set
			{
				throw new NotImplementedException("Not available on NSData, only available on NSMutableData");
			}
		}

		// Token: 0x17000013 RID: 19
		public byte this[int index]
		{
			get
			{
				if (index < 0 || (long)index > (long)((ulong)this.Length))
				{
					throw new ArgumentException("idx");
				}
				return Marshal.ReadByte(new IntPtr(this.Bytes.ToInt64() + (long)index));
			}
			set
			{
				throw new NotImplementedException("NSData arrays can not be modified, use an NSMutableData instead");
			}
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x00003D32 File Offset: 0x00001F32
		public bool Save(string path, bool atomically = true)
		{
			return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("writeToFile:atomically:"), path, atomically);
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x00003D4B File Offset: 0x00001F4B
		public Stream AsStream()
		{
			return new NSData.UnmanagedMemoryStreamWithRef(this);
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x00003D54 File Offset: 0x00001F54
		public byte[] ToArray()
		{
			byte[] array = new byte[this.Length];
			Marshal.Copy(this.Bytes, array, 0, array.Length);
			return array;
		}

		// Token: 0x04000045 RID: 69
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSData");

		// Token: 0x020001AA RID: 426
		private class UnmanagedMemoryStreamWithRef : UnmanagedMemoryStream
		{
			// Token: 0x06000E5C RID: 3676 RVA: 0x00026201 File Offset: 0x00024401
			public unsafe UnmanagedMemoryStreamWithRef(NSData source) : base((byte*)((void*)source.Bytes), (long)((ulong)source.Length))
			{
				this._data = source;
			}

			// Token: 0x06000E5D RID: 3677 RVA: 0x00026222 File Offset: 0x00024422
			protected override void Dispose(bool disposing)
			{
				this._data = null;
				base.Dispose(disposing);
			}

			// Token: 0x04000929 RID: 2345
			private NSData _data;
		}
	}
}
