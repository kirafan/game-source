﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000041 RID: 65
	public class UIViewController : NSObject
	{
		// Token: 0x170000F9 RID: 249
		// (get) Token: 0x06000304 RID: 772 RVA: 0x00006D84 File Offset: 0x00004F84
		public override IntPtr ClassHandle
		{
			get
			{
				return UIViewController._classHandle;
			}
		}

		// Token: 0x06000305 RID: 773 RVA: 0x000045C9 File Offset: 0x000027C9
		public UIViewController()
		{
		}

		// Token: 0x06000306 RID: 774 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIViewController(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x170000FA RID: 250
		// (get) Token: 0x06000307 RID: 775 RVA: 0x00005BF7 File Offset: 0x00003DF7
		// (set) Token: 0x06000308 RID: 776 RVA: 0x00005C0E File Offset: 0x00003E0E
		public string Title
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("title"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setTitle:"), value);
			}
		}

		// Token: 0x170000FB RID: 251
		// (get) Token: 0x06000309 RID: 777 RVA: 0x00006D8B File Offset: 0x00004F8B
		// (set) Token: 0x0600030A RID: 778 RVA: 0x00006DA7 File Offset: 0x00004FA7
		public UIView View
		{
			get
			{
				return Runtime.GetNSObject<UIView>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("view")));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setView:"), value.Handle);
			}
		}

		// Token: 0x170000FC RID: 252
		// (get) Token: 0x0600030B RID: 779 RVA: 0x00006DC4 File Offset: 0x00004FC4
		public bool IsViewLoaded
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isViewLoaded"));
			}
		}

		// Token: 0x0600030C RID: 780 RVA: 0x00006DDB File Offset: 0x00004FDB
		public void LoadView()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("loadView"));
		}

		// Token: 0x170000FD RID: 253
		// (get) Token: 0x0600030D RID: 781 RVA: 0x00006DF2 File Offset: 0x00004FF2
		public UIViewController ParentViewController
		{
			get
			{
				return Runtime.GetNSObject<UIViewController>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("parentViewController")));
			}
		}

		// Token: 0x170000FE RID: 254
		// (get) Token: 0x0600030E RID: 782 RVA: 0x00006E0E File Offset: 0x0000500E
		public UIViewController PresentedViewController
		{
			get
			{
				return Runtime.GetNSObject<UIViewController>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("presentedViewController")));
			}
		}

		// Token: 0x170000FF RID: 255
		// (get) Token: 0x0600030F RID: 783 RVA: 0x00006E2A File Offset: 0x0000502A
		public UIViewController PresentingViewController
		{
			get
			{
				return Runtime.GetNSObject<UIViewController>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("presentingViewController")));
			}
		}

		// Token: 0x06000310 RID: 784 RVA: 0x00006E46 File Offset: 0x00005046
		public void PresentViewController(UIViewController controller, bool animated = true)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("presentViewController:animated:completion:"), controller.Handle, animated, IntPtr.Zero);
		}

		// Token: 0x06000311 RID: 785 RVA: 0x00006E69 File Offset: 0x00005069
		public void DismissViewController(bool animated = true)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("dismissViewControllerAnimated:completion:"), animated, IntPtr.Zero);
		}

		// Token: 0x040000C8 RID: 200
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIViewController");
	}
}
