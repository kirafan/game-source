﻿using System;
using System.Collections.Generic;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000030 RID: 48
	public class UIApplication : NSObject
	{
		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x0600025B RID: 603 RVA: 0x00005EEB File Offset: 0x000040EB
		public override IntPtr ClassHandle
		{
			get
			{
				return UIApplication._classHandle;
			}
		}

		// Token: 0x0600025C RID: 604 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIApplication(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x0600025D RID: 605 RVA: 0x00005EF2 File Offset: 0x000040F2
		public static UIApplication SharedApplication
		{
			get
			{
				return Runtime.GetNSObject<UIApplication>(ObjC.MessageSendIntPtr(UIApplication._classHandle, Selector.GetHandle("sharedApplication")));
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x0600025E RID: 606 RVA: 0x00005F0D File Offset: 0x0000410D
		public static string DidBecomeActiveNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIApplicationDidBecomeActiveNotification");
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x0600025F RID: 607 RVA: 0x00005F1E File Offset: 0x0000411E
		public static string DidEnterBackgroundNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIApplicationDidEnterBackgroundNotification");
			}
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x06000260 RID: 608 RVA: 0x00005F2F File Offset: 0x0000412F
		public static string DidFinishLaunchingNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIApplicationDidFinishLaunchingNotification");
			}
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000261 RID: 609 RVA: 0x00005F40 File Offset: 0x00004140
		public static string DidReceiveMemoryWarningNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIApplicationDidReceiveMemoryWarningNotification");
			}
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x06000262 RID: 610 RVA: 0x00005F51 File Offset: 0x00004151
		public static string WillEnterForegroundNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIApplicationWillEnterForegroundNotification");
			}
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x06000263 RID: 611 RVA: 0x00005F62 File Offset: 0x00004162
		public static string WillResignActiveNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIApplicationWillResignActiveNotification");
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000264 RID: 612 RVA: 0x00005F73 File Offset: 0x00004173
		public UIWindow KeyWindow
		{
			get
			{
				return Runtime.GetNSObject<UIWindow>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("keyWindow")));
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000265 RID: 613 RVA: 0x00005F8F File Offset: 0x0000418F
		public UIWindow[] Windows
		{
			get
			{
				return ObjC.FromNSArray<UIWindow>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("windows")));
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x06000266 RID: 614 RVA: 0x00005FAB File Offset: 0x000041AB
		// (set) Token: 0x06000267 RID: 615 RVA: 0x00005FC2 File Offset: 0x000041C2
		public int ApplicationIconBadgeNumber
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("applicationIconBadgeNumber"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setApplicationIconBadgeNumber:"), value);
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x06000268 RID: 616 RVA: 0x00005FDA File Offset: 0x000041DA
		public UIApplicationState ApplicationState
		{
			get
			{
				return (UIApplicationState)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("applicationState"));
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000269 RID: 617 RVA: 0x00005FF1 File Offset: 0x000041F1
		// (set) Token: 0x0600026A RID: 618 RVA: 0x00006008 File Offset: 0x00004208
		public UIStatusBarStyle StatusBarStyle
		{
			get
			{
				return (UIStatusBarStyle)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("statusBarStyle"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setStatusBarStyle:"), (int)value);
			}
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x0600026B RID: 619 RVA: 0x00006020 File Offset: 0x00004220
		// (set) Token: 0x0600026C RID: 620 RVA: 0x00006037 File Offset: 0x00004237
		public bool StatusBarHidden
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isStatusBarHidden"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setStatusBarHidden:"), value);
			}
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x0600026D RID: 621 RVA: 0x0000604F File Offset: 0x0000424F
		// (set) Token: 0x0600026E RID: 622 RVA: 0x00006066 File Offset: 0x00004266
		public bool NetworkActivityIndicatorVisible
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isNetworkActivityIndicatorVisible"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setNetworkActivityIndicatorVisible:"), value);
			}
		}

		// Token: 0x170000B2 RID: 178
		// (get) Token: 0x0600026F RID: 623 RVA: 0x0000607E File Offset: 0x0000427E
		// (set) Token: 0x06000270 RID: 624 RVA: 0x00006095 File Offset: 0x00004295
		public bool IdleTimerDisabled
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isIdleTimerDisabled"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setIdleTimerDisabled:"), value);
			}
		}

		// Token: 0x06000271 RID: 625 RVA: 0x000060AD File Offset: 0x000042AD
		public void SetStatusBarHidden(bool hidden, bool animated = true)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("setStatusBarHidden:animated:"), hidden, animated);
		}

		// Token: 0x06000272 RID: 626 RVA: 0x000060C6 File Offset: 0x000042C6
		public bool CanOpenUrl(string url)
		{
			return ObjC.MessageSendBool_NSUrl(this.Handle, Selector.GetHandle("canOpenURL:"), url);
		}

		// Token: 0x06000273 RID: 627 RVA: 0x000060DE File Offset: 0x000042DE
		public bool OpenUrl(string url)
		{
			return ObjC.MessageSendBool_NSUrl(this.Handle, Selector.GetHandle("openURL:"), url);
		}

		// Token: 0x06000274 RID: 628 RVA: 0x000060F6 File Offset: 0x000042F6
		public void RegisterForRemoteNotificationTypes(UIRemoteNotificationType types)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("registerForRemoteNotificationTypes:"), (int)types);
		}

		// Token: 0x06000275 RID: 629 RVA: 0x0000610E File Offset: 0x0000430E
		public void UnregisterForRemoteNotifications()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("unregisterForRemoteNotifications"));
		}

		// Token: 0x06000276 RID: 630 RVA: 0x00006125 File Offset: 0x00004325
		public void RegisterUserNotificationSettings(UIUserNotificationSettings notificationSettings)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("registerUserNotificationSettings:"), notificationSettings.Handle);
		}

		// Token: 0x06000277 RID: 631 RVA: 0x00006142 File Offset: 0x00004342
		public void PresentLocationNotificationNow(UILocalNotification notification)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("presentLocalNotificationNow:"), notification.Handle);
		}

		// Token: 0x06000278 RID: 632 RVA: 0x0000615F File Offset: 0x0000435F
		public void ScheduleLocalNotification(UILocalNotification notification)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("scheduleLocalNotification:"), notification.Handle);
		}

		// Token: 0x06000279 RID: 633 RVA: 0x0000617C File Offset: 0x0000437C
		public void CancelAllLocalNotifications()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("cancelAllLocalNotifications"));
		}

		// Token: 0x0600027A RID: 634 RVA: 0x00006193 File Offset: 0x00004393
		public void CancelLocalNotification(UILocalNotification notification)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("cancelLocalNotification:"), notification.Handle);
		}

		// Token: 0x0400008F RID: 143
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIApplication");

		// Token: 0x04000090 RID: 144
		private Dictionary<object, IntPtrHandler2> _failed;
	}
}
