﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000017 RID: 23
	public class NSNotification : NSObject
	{
		// Token: 0x17000039 RID: 57
		// (get) Token: 0x0600013B RID: 315 RVA: 0x0000445C File Offset: 0x0000265C
		public override IntPtr ClassHandle
		{
			get
			{
				return NSNotification._classHandle;
			}
		}

		// Token: 0x0600013C RID: 316 RVA: 0x00004463 File Offset: 0x00002663
		public static NSNotification FromName(string name, NSObject obj = null)
		{
			return Runtime.GetNSObject<NSNotification>(ObjC.MessageSendIntPtr(NSNotification._classHandle, Selector.GetHandle("notificationWithName:object:"), name, (obj == null) ? IntPtr.Zero : obj.Handle));
		}

		// Token: 0x0600013D RID: 317 RVA: 0x00003A32 File Offset: 0x00001C32
		internal NSNotification(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x0600013E RID: 318 RVA: 0x0000448F File Offset: 0x0000268F
		public string Name
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("name"));
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600013F RID: 319 RVA: 0x000044A6 File Offset: 0x000026A6
		public NSObject Object
		{
			get
			{
				return Runtime.GetNSObject<NSObject>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("object")));
			}
		}

		// Token: 0x0400004F RID: 79
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSNotification");
	}
}
