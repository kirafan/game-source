﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200003E RID: 62
	public class UIUserNotificationSettings : NSObject
	{
		// Token: 0x170000EE RID: 238
		// (get) Token: 0x060002E8 RID: 744 RVA: 0x00006B6C File Offset: 0x00004D6C
		public override IntPtr ClassHandle
		{
			get
			{
				return UIUserNotificationSettings._classHandle;
			}
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x00004FC2 File Offset: 0x000031C2
		public UIUserNotificationSettings()
		{
			ObjC.MessageSendIntPtr(this.Handle, Selector.Init);
		}

		// Token: 0x060002EA RID: 746 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIUserNotificationSettings(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x060002EB RID: 747 RVA: 0x00006B73 File Offset: 0x00004D73
		public static UIUserNotificationSettings GetSettingsForTypes(UIUserNotificationType types)
		{
			return Runtime.GetNSObject<UIUserNotificationSettings>(ObjC.MessageSendIntPtr(UIUserNotificationSettings._classHandle, Selector.GetHandle("settingsForTypes:categories:"), (uint)types, IntPtr.Zero));
		}

		// Token: 0x170000EF RID: 239
		// (get) Token: 0x060002EC RID: 748 RVA: 0x00006B94 File Offset: 0x00004D94
		public UIUserNotificationType Types
		{
			get
			{
				return (UIUserNotificationType)ObjC.MessageSendUInt(this.Handle, Selector.GetHandle("types"));
			}
		}

		// Token: 0x040000C1 RID: 193
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIUserNotificationSettings");
	}
}
