﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000012 RID: 18
	[Flags]
	[CLSCompliant(false)]
	public enum NSDataReadingOptions : uint
	{
		// Token: 0x04000047 RID: 71
		Mapped = 1U,
		// Token: 0x04000048 RID: 72
		Uncached = 2U,
		// Token: 0x04000049 RID: 73
		Coordinated = 4U,
		// Token: 0x0400004A RID: 74
		MappedAlways = 8U
	}
}
