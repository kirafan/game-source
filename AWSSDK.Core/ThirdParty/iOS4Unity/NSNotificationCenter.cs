﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000018 RID: 24
	public sealed class NSNotificationCenter : NSObject
	{
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000141 RID: 321 RVA: 0x000044D3 File Offset: 0x000026D3
		public override IntPtr ClassHandle
		{
			get
			{
				return NSNotificationCenter._classHandle;
			}
		}

		// Token: 0x06000142 RID: 322 RVA: 0x00003A32 File Offset: 0x00001C32
		internal NSNotificationCenter(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000143 RID: 323 RVA: 0x000044DA File Offset: 0x000026DA
		public static NSNotificationCenter DefaultCenter
		{
			get
			{
				return Runtime.GetNSObject<NSNotificationCenter>(ObjC.MessageSendIntPtr(NSNotificationCenter._classHandle, Selector.GetHandle("defaultCenter")));
			}
		}

		// Token: 0x06000144 RID: 324 RVA: 0x000044F8 File Offset: 0x000026F8
		public NSObject AddObserver(string name, Action<NSNotification> action, NSObject fromObject = null)
		{
			NSNotificationCenter.Observer observer = new NSNotificationCenter.Observer(action);
			Callbacks.Subscribe(observer, "__onNotification:", delegate(IntPtr n)
			{
				action(Runtime.GetNSObject<NSNotification>(n));
			});
			ObjC.MessageSend(this.Handle, Selector.GetHandle("addObserver:selector:name:object:"), observer.Handle, Selector.GetHandle("__onNotification:"), name, (fromObject == null) ? IntPtr.Zero : fromObject.Handle);
			return observer;
		}

		// Token: 0x06000145 RID: 325 RVA: 0x0000456C File Offset: 0x0000276C
		public void PostNotificationName(string name, NSObject obj = null)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("postNotificationName:object:"), name, (obj == null) ? IntPtr.Zero : obj.Handle);
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00004594 File Offset: 0x00002794
		public void RemoveObserver(NSObject observer)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("removeObserver:"), observer.Handle);
		}

		// Token: 0x04000050 RID: 80
		private const string SelectorName = "__onNotification:";

		// Token: 0x04000051 RID: 81
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSNotificationCenter");

		// Token: 0x020001AB RID: 427
		private class Observer : NSObject
		{
			// Token: 0x17000410 RID: 1040
			// (get) Token: 0x06000E5F RID: 3679 RVA: 0x0002624E File Offset: 0x0002444E
			public override IntPtr ClassHandle
			{
				get
				{
					return NSNotificationCenter.Observer._classHandle;
				}
			}

			// Token: 0x06000E60 RID: 3680 RVA: 0x00026255 File Offset: 0x00024455
			public Observer(Action<NSNotification> action)
			{
				this.Action = action;
			}

			// Token: 0x06000E61 RID: 3681 RVA: 0x00026264 File Offset: 0x00024464
			public override void Dispose()
			{
				NSNotificationCenter.DefaultCenter.RemoveObserver(this);
				base.Dispose();
			}

			// Token: 0x0400092A RID: 2346
			private static readonly IntPtr _classHandle = ObjC.AllocateClassPair(ObjC.GetClass("NSObject"), "__Observer", 0);

			// Token: 0x0400092B RID: 2347
			public readonly Action<NSNotification> Action;
		}
	}
}
