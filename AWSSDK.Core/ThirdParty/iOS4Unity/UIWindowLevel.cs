﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000043 RID: 67
	public static class UIWindowLevel
	{
		// Token: 0x17000105 RID: 261
		// (get) Token: 0x06000322 RID: 802 RVA: 0x00006FC3 File Offset: 0x000051C3
		public static float Alert
		{
			get
			{
				return ObjC.GetFloatConstant(ObjC.Libraries.UIKit, "UIWindowLevelAlert");
			}
		}

		// Token: 0x17000106 RID: 262
		// (get) Token: 0x06000323 RID: 803 RVA: 0x00006FD4 File Offset: 0x000051D4
		public static float Normal
		{
			get
			{
				return ObjC.GetFloatConstant(ObjC.Libraries.UIKit, "UIWindowLevelNormal");
			}
		}

		// Token: 0x17000107 RID: 263
		// (get) Token: 0x06000324 RID: 804 RVA: 0x00006FE5 File Offset: 0x000051E5
		public static float StatusBar
		{
			get
			{
				return ObjC.GetFloatConstant(ObjC.Libraries.UIKit, "UIWindowLevelStatusBar");
			}
		}
	}
}
