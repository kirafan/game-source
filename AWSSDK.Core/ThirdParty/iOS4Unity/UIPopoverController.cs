﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200003A RID: 58
	public class UIPopoverController : NSObject
	{
		// Token: 0x170000D4 RID: 212
		// (get) Token: 0x060002BC RID: 700 RVA: 0x000067CB File Offset: 0x000049CB
		public override IntPtr ClassHandle
		{
			get
			{
				return UIPopoverController._classHandle;
			}
		}

		// Token: 0x060002BD RID: 701 RVA: 0x000067D4 File Offset: 0x000049D4
		public UIPopoverController(UIViewController controller)
		{
			this.Handle = ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("initWithContentViewController:"), controller.Handle);
			ObjC.MessageSend(this.Handle, Selector.GetHandle("setDelegate:"), this.Handle);
		}

		// Token: 0x060002BE RID: 702 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIPopoverController(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x060002BF RID: 703 RVA: 0x00006823 File Offset: 0x00004A23
		public void PresentFromRect(CGRect rect, UIView view, UIPopoverArrowDirection arrowDirections, bool animated)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("presentPopoverFromRect:inView:permittedArrowDirections:animated:"), rect, view.Handle, (uint)arrowDirections, animated);
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x00006844 File Offset: 0x00004A44
		public void SetContentViewController(UIViewController viewController, bool animated)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("setContentViewController:animated:"), viewController.Handle, animated);
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x00006862 File Offset: 0x00004A62
		public void SetPopoverContentSize(CGSize size, bool animated)
		{
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("setPopoverContentSize:animated:"), size, animated);
		}

		// Token: 0x060002C2 RID: 706 RVA: 0x0000687C File Offset: 0x00004A7C
		public void Dismiss(bool animated)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("dismissPopoverAnimated:"), animated);
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x060002C3 RID: 707 RVA: 0x00006894 File Offset: 0x00004A94
		// (set) Token: 0x060002C4 RID: 708 RVA: 0x000068B0 File Offset: 0x00004AB0
		public UIViewController ContentViewController
		{
			get
			{
				return Runtime.GetNSObject<UIViewController>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("contentViewController")));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setContentViewController:"), value.Handle);
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x060002C5 RID: 709 RVA: 0x000068CD File Offset: 0x00004ACD
		public UIPopoverArrowDirection PopoverArrowDirection
		{
			get
			{
				return (UIPopoverArrowDirection)ObjC.MessageSendUInt(this.Handle, Selector.GetHandle("popoverArrowDirection"));
			}
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x060002C6 RID: 710 RVA: 0x000068E4 File Offset: 0x00004AE4
		// (set) Token: 0x060002C7 RID: 711 RVA: 0x000068F6 File Offset: 0x00004AF6
		public CGSize PopoverContentSize
		{
			get
			{
				return ObjC.MessageSendCGSize(this.Handle, "popoverContentSize");
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setPopoverContentSize:"), value);
			}
		}

		// Token: 0x170000D8 RID: 216
		// (get) Token: 0x060002C8 RID: 712 RVA: 0x0000690E File Offset: 0x00004B0E
		public bool PopoverVisible
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isPopoverVisible"));
			}
		}

		// Token: 0x14000017 RID: 23
		// (add) Token: 0x060002C9 RID: 713 RVA: 0x00006925 File Offset: 0x00004B25
		// (remove) Token: 0x060002CA RID: 714 RVA: 0x00006933 File Offset: 0x00004B33
		public event EventHandler Dismissed
		{
			add
			{
				Callbacks.Subscribe(this, "popoverControllerDidDismissPopover:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "popoverControllerDidDismissPopover:", value);
			}
		}

		// Token: 0x040000B7 RID: 183
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIPopoverController");
	}
}
