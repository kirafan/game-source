﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200002C RID: 44
	public class UIActionSheet : UIView
	{
		// Token: 0x17000094 RID: 148
		// (get) Token: 0x0600021B RID: 539 RVA: 0x00005A4A File Offset: 0x00003C4A
		public override IntPtr ClassHandle
		{
			get
			{
				return UIActionSheet._classHandle;
			}
		}

		// Token: 0x0600021C RID: 540 RVA: 0x00005A51 File Offset: 0x00003C51
		public UIActionSheet()
		{
			this.Handle = ObjC.MessageSendIntPtr(this.Handle, Selector.Init);
			ObjC.MessageSend(this.Handle, Selector.GetHandle("setDelegate:"), this.Handle);
		}

		// Token: 0x0600021D RID: 541 RVA: 0x000058F7 File Offset: 0x00003AF7
		internal UIActionSheet(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x0600021E RID: 542 RVA: 0x00005A8A File Offset: 0x00003C8A
		public int AddButton(string title)
		{
			return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("addButtonWithTitle:"), title);
		}

		// Token: 0x1400000B RID: 11
		// (add) Token: 0x0600021F RID: 543 RVA: 0x00005AA2 File Offset: 0x00003CA2
		// (remove) Token: 0x06000220 RID: 544 RVA: 0x00005AB0 File Offset: 0x00003CB0
		public event EventHandler<ButtonEventArgs> Clicked
		{
			add
			{
				Callbacks.Subscribe(this, "actionSheet:clickedButtonAtIndex:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "actionSheet:clickedButtonAtIndex:", value);
			}
		}

		// Token: 0x1400000C RID: 12
		// (add) Token: 0x06000221 RID: 545 RVA: 0x00005ABE File Offset: 0x00003CBE
		// (remove) Token: 0x06000222 RID: 546 RVA: 0x00005ACC File Offset: 0x00003CCC
		public event EventHandler<ButtonEventArgs> Dismissed
		{
			add
			{
				Callbacks.Subscribe(this, "actionSheet:didDismissWithButtonIndex:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "actionSheet:didDismissWithButtonIndex:", value);
			}
		}

		// Token: 0x1400000D RID: 13
		// (add) Token: 0x06000223 RID: 547 RVA: 0x00005ADA File Offset: 0x00003CDA
		// (remove) Token: 0x06000224 RID: 548 RVA: 0x00005AE8 File Offset: 0x00003CE8
		public event EventHandler<ButtonEventArgs> WillDismiss
		{
			add
			{
				Callbacks.Subscribe(this, "actionSheet:willDismissWithButtonIndex:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "actionSheet:willDismissWithButtonIndex:", value);
			}
		}

		// Token: 0x1400000E RID: 14
		// (add) Token: 0x06000225 RID: 549 RVA: 0x00005AF6 File Offset: 0x00003CF6
		// (remove) Token: 0x06000226 RID: 550 RVA: 0x00005B04 File Offset: 0x00003D04
		public event EventHandler Canceled
		{
			add
			{
				Callbacks.Subscribe(this, "actionSheetCancel:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "actionSheetCancel:", value);
			}
		}

		// Token: 0x1400000F RID: 15
		// (add) Token: 0x06000227 RID: 551 RVA: 0x00005B12 File Offset: 0x00003D12
		// (remove) Token: 0x06000228 RID: 552 RVA: 0x00005B20 File Offset: 0x00003D20
		public event EventHandler Presented
		{
			add
			{
				Callbacks.Subscribe(this, "didPresentActionSheet:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "didPresentActionSheet:", value);
			}
		}

		// Token: 0x14000010 RID: 16
		// (add) Token: 0x06000229 RID: 553 RVA: 0x00005B2E File Offset: 0x00003D2E
		// (remove) Token: 0x0600022A RID: 554 RVA: 0x00005B3C File Offset: 0x00003D3C
		public event EventHandler WillPresent
		{
			add
			{
				Callbacks.Subscribe(this, "willPresentActionSheet:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "willPresentActionSheet:", value);
			}
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x0600022B RID: 555 RVA: 0x00005B4A File Offset: 0x00003D4A
		public int ButtonCount
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("numberOfButtons"));
			}
		}

		// Token: 0x0600022C RID: 556 RVA: 0x00005B61 File Offset: 0x00003D61
		public string ButtonTitle(int index)
		{
			return ObjC.MessageSendString(this.Handle, Selector.GetHandle("buttonTitleAtIndex:"), index);
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x0600022D RID: 557 RVA: 0x00005B79 File Offset: 0x00003D79
		// (set) Token: 0x0600022E RID: 558 RVA: 0x00005B90 File Offset: 0x00003D90
		public int CancelButtonIndex
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("cancelButtonIndex"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setCancelButtonIndex:"), value);
			}
		}

		// Token: 0x0600022F RID: 559 RVA: 0x00005BA8 File Offset: 0x00003DA8
		public void DismissWithClickedButtonIndex(int buttonIndex, bool animated)
		{
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("dismissWithClickedButtonIndex:animated:"), buttonIndex, animated);
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000230 RID: 560 RVA: 0x00005BC2 File Offset: 0x00003DC2
		public int FirstOtherButtonIndex
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("firstOtherButtonIndex"));
			}
		}

		// Token: 0x06000231 RID: 561 RVA: 0x00005BD9 File Offset: 0x00003DD9
		public void ShowInView(UIView view)
		{
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("showInView:"), view.Handle);
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000232 RID: 562 RVA: 0x00005BF7 File Offset: 0x00003DF7
		// (set) Token: 0x06000233 RID: 563 RVA: 0x00005C0E File Offset: 0x00003E0E
		public string Title
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("title"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setTitle:"), value);
			}
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x06000234 RID: 564 RVA: 0x00005C26 File Offset: 0x00003E26
		public bool Visible
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isVisible"));
			}
		}

		// Token: 0x04000087 RID: 135
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIActionSheet");
	}
}
