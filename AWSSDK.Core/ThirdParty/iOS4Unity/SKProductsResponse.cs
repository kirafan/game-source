﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000028 RID: 40
	public sealed class SKProductsResponse : NSObject
	{
		// Token: 0x1700008E RID: 142
		// (get) Token: 0x060001FD RID: 509 RVA: 0x000057BF File Offset: 0x000039BF
		public override IntPtr ClassHandle
		{
			get
			{
				return SKProductsResponse._classHandle;
			}
		}

		// Token: 0x060001FE RID: 510 RVA: 0x00003A32 File Offset: 0x00001C32
		internal SKProductsResponse(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060001FF RID: 511 RVA: 0x000057C6 File Offset: 0x000039C6
		public string[] InvalidProducts
		{
			get
			{
				return ObjC.FromNSArray(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("invalidProductIdentifiers")));
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x06000200 RID: 512 RVA: 0x000057E2 File Offset: 0x000039E2
		public SKProduct[] Products
		{
			get
			{
				return ObjC.FromNSArray<SKProduct>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("products")));
			}
		}

		// Token: 0x04000080 RID: 128
		private static readonly IntPtr _classHandle = ObjC.GetClass("SKProductsResponse");
	}
}
