﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000031 RID: 49
	public enum UIApplicationState
	{
		// Token: 0x04000092 RID: 146
		Active,
		// Token: 0x04000093 RID: 147
		Inactive,
		// Token: 0x04000094 RID: 148
		Background
	}
}
