﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000024 RID: 36
	public class SKProduct : NSObject
	{
		// Token: 0x17000085 RID: 133
		// (get) Token: 0x060001E6 RID: 486 RVA: 0x00005548 File Offset: 0x00003748
		public override IntPtr ClassHandle
		{
			get
			{
				return SKProduct._classHandle;
			}
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x00003A32 File Offset: 0x00001C32
		internal SKProduct(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060001E8 RID: 488 RVA: 0x0000554F File Offset: 0x0000374F
		public bool Downloadable
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isDownloadable"));
			}
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x00003FFA File Offset: 0x000021FA
		public string LocalizedDescription
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("localizedDescription"));
			}
		}

		// Token: 0x17000088 RID: 136
		// (get) Token: 0x060001EA RID: 490 RVA: 0x00005566 File Offset: 0x00003766
		public string LocalizedTitle
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("localizedTitle"));
			}
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x060001EB RID: 491 RVA: 0x0000557D File Offset: 0x0000377D
		public double Price
		{
			get
			{
				return ObjC.FromNSNumber(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("price")));
			}
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x060001EC RID: 492 RVA: 0x00005599 File Offset: 0x00003799
		public NSLocale PriceLocale
		{
			get
			{
				return Runtime.GetNSObject<NSLocale>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("priceLocale")));
			}
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x060001ED RID: 493 RVA: 0x000051F2 File Offset: 0x000033F2
		public string ProductIdentifier
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("productIdentifier"));
			}
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x060001EE RID: 494 RVA: 0x000055B5 File Offset: 0x000037B5
		public SKPaymentTransactionState TransactionState
		{
			get
			{
				return (SKPaymentTransactionState)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("transactionState"));
			}
		}

		// Token: 0x04000075 RID: 117
		private static readonly IntPtr _classHandle = ObjC.GetClass("SKProduct");
	}
}
