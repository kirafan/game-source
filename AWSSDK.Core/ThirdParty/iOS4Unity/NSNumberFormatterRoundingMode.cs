﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200001D RID: 29
	public enum NSNumberFormatterRoundingMode
	{
		// Token: 0x04000064 RID: 100
		Ceiling,
		// Token: 0x04000065 RID: 101
		Floor,
		// Token: 0x04000066 RID: 102
		Down,
		// Token: 0x04000067 RID: 103
		Up,
		// Token: 0x04000068 RID: 104
		HalfEven,
		// Token: 0x04000069 RID: 105
		HalfDown,
		// Token: 0x0400006A RID: 106
		HalfUp
	}
}
