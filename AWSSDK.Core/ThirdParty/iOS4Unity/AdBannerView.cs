﻿using System;
using System.Collections.Generic;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200002A RID: 42
	public class AdBannerView : UIView
	{
		// Token: 0x17000091 RID: 145
		// (get) Token: 0x0600020A RID: 522 RVA: 0x0000586F File Offset: 0x00003A6F
		public override IntPtr ClassHandle
		{
			get
			{
				return AdBannerView._classHandle;
			}
		}

		// Token: 0x0600020B RID: 523 RVA: 0x00005876 File Offset: 0x00003A76
		public AdBannerView()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("setDelegate:"), this.Handle);
		}

		// Token: 0x0600020C RID: 524 RVA: 0x00005899 File Offset: 0x00003A99
		public AdBannerView(CGRect frame) : base(frame)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("setDelegate:"), this.Handle);
		}

		// Token: 0x0600020D RID: 525 RVA: 0x000058BD File Offset: 0x00003ABD
		public AdBannerView(AdType type)
		{
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("initWithAdType:"), (int)type);
			ObjC.MessageSend(this.Handle, Selector.GetHandle("setDelegate:"), this.Handle);
		}

		// Token: 0x0600020E RID: 526 RVA: 0x000058F7 File Offset: 0x00003AF7
		internal AdBannerView(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x0600020F RID: 527 RVA: 0x00005900 File Offset: 0x00003B00
		public AdType AdType
		{
			get
			{
				return (AdType)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("adType"));
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x06000210 RID: 528 RVA: 0x00005917 File Offset: 0x00003B17
		public bool BannerLoaded
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isBannerLoaded"));
			}
		}

		// Token: 0x14000007 RID: 7
		// (add) Token: 0x06000211 RID: 529 RVA: 0x0000592E File Offset: 0x00003B2E
		// (remove) Token: 0x06000212 RID: 530 RVA: 0x0000593C File Offset: 0x00003B3C
		public event EventHandler AdLoaded
		{
			add
			{
				Callbacks.Subscribe(this, "bannerViewDidLoadAd:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "bannerViewDidLoadAd:", value);
			}
		}

		// Token: 0x14000008 RID: 8
		// (add) Token: 0x06000213 RID: 531 RVA: 0x0000594A File Offset: 0x00003B4A
		// (remove) Token: 0x06000214 RID: 532 RVA: 0x00005958 File Offset: 0x00003B58
		public event EventHandler ActionFinished
		{
			add
			{
				Callbacks.Subscribe(this, "bannerViewActionDidFinish:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "bannerViewActionDidFinish:", value);
			}
		}

		// Token: 0x14000009 RID: 9
		// (add) Token: 0x06000215 RID: 533 RVA: 0x00005968 File Offset: 0x00003B68
		// (remove) Token: 0x06000216 RID: 534 RVA: 0x000059C8 File Offset: 0x00003BC8
		public event EventHandler<NSErrorEventArgs> FailedToReceiveAd
		{
			add
			{
				if (this._failedToReceiveAd == null)
				{
					this._failedToReceiveAd = new Dictionary<object, IntPtrHandler2>();
				}
				IntPtrHandler2 intPtrHandler = delegate(IntPtr _, IntPtr i)
				{
					value(this, new NSErrorEventArgs
					{
						Error = Runtime.GetNSObject<NSError>(i)
					});
				};
				this._failedToReceiveAd[value] = intPtrHandler;
				Callbacks.Subscribe(this, "bannerView:didFailToReceiveAdWithError:", intPtrHandler);
			}
			remove
			{
				IntPtrHandler2 callback;
				if (this._failedToReceiveAd != null && this._failedToReceiveAd.TryGetValue(value, out callback))
				{
					this._failedToReceiveAd.Remove(value);
					Callbacks.Unsubscribe(this, "bannerView:didFailToReceiveAdWithError:", callback);
				}
			}
		}

		// Token: 0x1400000A RID: 10
		// (add) Token: 0x06000217 RID: 535 RVA: 0x00005A06 File Offset: 0x00003C06
		// (remove) Token: 0x06000218 RID: 536 RVA: 0x00005A14 File Offset: 0x00003C14
		public event EventHandler WillLoad
		{
			add
			{
				Callbacks.Subscribe(this, "bannerViewWillLoadAd:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "bannerViewWillLoadAd:", value);
			}
		}

		// Token: 0x06000219 RID: 537 RVA: 0x00005A22 File Offset: 0x00003C22
		public void CancelBannerViewAction()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("cancelBannerViewAction"));
		}

		// Token: 0x04000082 RID: 130
		private static readonly IntPtr _classHandle = ObjC.GetClass("ADBannerView");

		// Token: 0x04000083 RID: 131
		private Dictionary<object, IntPtrHandler2> _failedToReceiveAd;
	}
}
