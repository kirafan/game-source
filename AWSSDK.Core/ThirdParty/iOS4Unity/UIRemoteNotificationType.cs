﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000032 RID: 50
	[Flags]
	public enum UIRemoteNotificationType
	{
		// Token: 0x04000096 RID: 150
		None = 0,
		// Token: 0x04000097 RID: 151
		Badge = 1,
		// Token: 0x04000098 RID: 152
		Sound = 2,
		// Token: 0x04000099 RID: 153
		Alert = 4,
		// Token: 0x0400009A RID: 154
		NewsstandContentAvailability = 8
	}
}
