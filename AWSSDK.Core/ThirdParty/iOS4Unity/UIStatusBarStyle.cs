﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000033 RID: 51
	public enum UIStatusBarStyle
	{
		// Token: 0x0400009C RID: 156
		Default,
		// Token: 0x0400009D RID: 157
		BlackTranslucent,
		// Token: 0x0400009E RID: 158
		LightContent = 1,
		// Token: 0x0400009F RID: 159
		BlackOpaque
	}
}
