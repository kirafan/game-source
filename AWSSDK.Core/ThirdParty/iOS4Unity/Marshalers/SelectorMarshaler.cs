﻿using System;
using System.Runtime.InteropServices;

namespace ThirdParty.iOS4Unity.Marshalers
{
	// Token: 0x02000048 RID: 72
	public class SelectorMarshaler : ICustomMarshaler
	{
		// Token: 0x0600033D RID: 829 RVA: 0x000070E9 File Offset: 0x000052E9
		public static ICustomMarshaler GetInstance(string cookie)
		{
			return SelectorMarshaler._instance;
		}

		// Token: 0x0600033E RID: 830 RVA: 0x00005805 File Offset: 0x00003A05
		public void CleanUpManagedData(object managedObj)
		{
		}

		// Token: 0x0600033F RID: 831 RVA: 0x00005805 File Offset: 0x00003A05
		public void CleanUpNativeData(IntPtr pNativeData)
		{
		}

		// Token: 0x06000340 RID: 832 RVA: 0x00005807 File Offset: 0x00003A07
		public int GetNativeDataSize()
		{
			return IntPtr.Size;
		}

		// Token: 0x06000341 RID: 833 RVA: 0x000070F0 File Offset: 0x000052F0
		public IntPtr MarshalManagedToNative(object managedObj)
		{
			string text = managedObj as string;
			if (string.IsNullOrEmpty(text))
			{
				return IntPtr.Zero;
			}
			return ObjC.GetSelector(text);
		}

		// Token: 0x06000342 RID: 834 RVA: 0x00007118 File Offset: 0x00005318
		public object MarshalNativeToManaged(IntPtr pNativeData)
		{
			return ObjC.GetSelectorName(pNativeData);
		}

		// Token: 0x040000CE RID: 206
		private static readonly SelectorMarshaler _instance = new SelectorMarshaler();
	}
}
