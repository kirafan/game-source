﻿using System;
using System.Runtime.InteropServices;

namespace ThirdParty.iOS4Unity.Marshalers
{
	// Token: 0x02000047 RID: 71
	public class NSStringReleaseMarshaler : NSStringMarshaler
	{
		// Token: 0x06000339 RID: 825 RVA: 0x000070CE File Offset: 0x000052CE
		public new static ICustomMarshaler GetInstance(string cookie)
		{
			return NSStringReleaseMarshaler._instance;
		}

		// Token: 0x0600033A RID: 826 RVA: 0x00007053 File Offset: 0x00005253
		public override void CleanUpNativeData(IntPtr pNativeData)
		{
			if (pNativeData != IntPtr.Zero)
			{
				ObjC.MessageSend(pNativeData, Selector.ReleaseHandle);
			}
		}

		// Token: 0x040000CD RID: 205
		private static readonly NSStringReleaseMarshaler _instance = new NSStringReleaseMarshaler();
	}
}
