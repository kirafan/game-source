﻿using System;
using System.Runtime.InteropServices;

namespace ThirdParty.iOS4Unity.Marshalers
{
	// Token: 0x02000046 RID: 70
	public class NSStringMarshaler : ICustomMarshaler
	{
		// Token: 0x06000331 RID: 817 RVA: 0x00007081 File Offset: 0x00005281
		public static ICustomMarshaler GetInstance(string cookie)
		{
			return NSStringMarshaler._instance;
		}

		// Token: 0x06000332 RID: 818 RVA: 0x00005805 File Offset: 0x00003A05
		public void CleanUpManagedData(object managedObj)
		{
		}

		// Token: 0x06000333 RID: 819 RVA: 0x00005805 File Offset: 0x00003A05
		public virtual void CleanUpNativeData(IntPtr pNativeData)
		{
		}

		// Token: 0x06000334 RID: 820 RVA: 0x00005807 File Offset: 0x00003A07
		public int GetNativeDataSize()
		{
			return IntPtr.Size;
		}

		// Token: 0x06000335 RID: 821 RVA: 0x00007088 File Offset: 0x00005288
		public IntPtr MarshalManagedToNative(object managedObj)
		{
			string text = managedObj as string;
			if (text == null)
			{
				return IntPtr.Zero;
			}
			return ObjC.ToNSString(text);
		}

		// Token: 0x06000336 RID: 822 RVA: 0x000070AB File Offset: 0x000052AB
		public object MarshalNativeToManaged(IntPtr pNativeData)
		{
			if (pNativeData == IntPtr.Zero)
			{
				return null;
			}
			return ObjC.FromNSString(pNativeData);
		}

		// Token: 0x040000CC RID: 204
		private static readonly NSStringMarshaler _instance = new NSStringMarshaler();
	}
}
