﻿using System;
using System.Runtime.InteropServices;

namespace ThirdParty.iOS4Unity.Marshalers
{
	// Token: 0x02000044 RID: 68
	public class NSDateMarshaler : ICustomMarshaler
	{
		// Token: 0x06000325 RID: 805 RVA: 0x00006FF6 File Offset: 0x000051F6
		public static ICustomMarshaler GetInstance(string cookie)
		{
			return NSDateMarshaler._instance;
		}

		// Token: 0x06000326 RID: 806 RVA: 0x00005805 File Offset: 0x00003A05
		public void CleanUpManagedData(object ManagedObj)
		{
		}

		// Token: 0x06000327 RID: 807 RVA: 0x00005805 File Offset: 0x00003A05
		public virtual void CleanUpNativeData(IntPtr pNativeData)
		{
		}

		// Token: 0x06000328 RID: 808 RVA: 0x00005807 File Offset: 0x00003A07
		public int GetNativeDataSize()
		{
			return IntPtr.Size;
		}

		// Token: 0x06000329 RID: 809 RVA: 0x00006FFD File Offset: 0x000051FD
		public IntPtr MarshalManagedToNative(object managedObj)
		{
			return ObjC.ToNSDate((DateTime)managedObj);
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000700C File Offset: 0x0000520C
		public object MarshalNativeToManaged(IntPtr pNativeData)
		{
			if (pNativeData == IntPtr.Zero)
			{
				return default(DateTime);
			}
			return ObjC.FromNSDate(pNativeData);
		}

		// Token: 0x040000CA RID: 202
		private static readonly NSDateMarshaler _instance = new NSDateMarshaler();
	}
}
