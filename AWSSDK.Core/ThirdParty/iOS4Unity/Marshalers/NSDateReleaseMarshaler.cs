﻿using System;
using System.Runtime.InteropServices;

namespace ThirdParty.iOS4Unity.Marshalers
{
	// Token: 0x02000045 RID: 69
	public class NSDateReleaseMarshaler : NSDateMarshaler
	{
		// Token: 0x0600032D RID: 813 RVA: 0x0000704C File Offset: 0x0000524C
		public new static ICustomMarshaler GetInstance(string cookie)
		{
			return NSDateReleaseMarshaler._instance;
		}

		// Token: 0x0600032E RID: 814 RVA: 0x00007053 File Offset: 0x00005253
		public override void CleanUpNativeData(IntPtr pNativeData)
		{
			if (pNativeData != IntPtr.Zero)
			{
				ObjC.MessageSend(pNativeData, Selector.ReleaseHandle);
			}
		}

		// Token: 0x040000CB RID: 203
		private static readonly NSDateReleaseMarshaler _instance = new NSDateReleaseMarshaler();
	}
}
