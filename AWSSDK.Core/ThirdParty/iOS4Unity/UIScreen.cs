﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200003C RID: 60
	public class UIScreen : NSObject
	{
		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x060002CC RID: 716 RVA: 0x00006952 File Offset: 0x00004B52
		public override IntPtr ClassHandle
		{
			get
			{
				return UIScreen._classHandle;
			}
		}

		// Token: 0x060002CD RID: 717 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIScreen(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x060002CE RID: 718 RVA: 0x00006959 File Offset: 0x00004B59
		public CGRect ApplicationFrame
		{
			get
			{
				return ObjC.MessageSendCGRect(this.Handle, Selector.GetHandle("applicationFrame"));
			}
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x060002CF RID: 719 RVA: 0x00006970 File Offset: 0x00004B70
		public UIScreenMode[] AvailableModes
		{
			get
			{
				return ObjC.FromNSArray<UIScreenMode>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("availableModes")));
			}
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x060002D0 RID: 720 RVA: 0x0000698C File Offset: 0x00004B8C
		public CGRect Bounds
		{
			get
			{
				return ObjC.MessageSendCGRect(this.Handle, Selector.GetHandle("bounds"));
			}
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x060002D1 RID: 721 RVA: 0x000069A3 File Offset: 0x00004BA3
		// (set) Token: 0x060002D2 RID: 722 RVA: 0x000069BA File Offset: 0x00004BBA
		public float Brightness
		{
			get
			{
				return ObjC.MessageSendFloat(this.Handle, Selector.GetHandle("brightness"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setBrightness"), value);
			}
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x060002D3 RID: 723 RVA: 0x000069D2 File Offset: 0x00004BD2
		public static string BrightnessDidChangeNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIScreenBrightnessDidChangeNotification");
			}
		}

		// Token: 0x170000DF RID: 223
		// (get) Token: 0x060002D4 RID: 724 RVA: 0x000069E3 File Offset: 0x00004BE3
		// (set) Token: 0x060002D5 RID: 725 RVA: 0x000069FF File Offset: 0x00004BFF
		public UIScreenMode CurrentMode
		{
			get
			{
				return Runtime.GetNSObject<UIScreenMode>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("currentMode")));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setCurrentMode"), value.Handle);
			}
		}

		// Token: 0x170000E0 RID: 224
		// (get) Token: 0x060002D6 RID: 726 RVA: 0x00006A1C File Offset: 0x00004C1C
		public static string DidConnectNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIScreenDidConnectNotification");
			}
		}

		// Token: 0x170000E1 RID: 225
		// (get) Token: 0x060002D7 RID: 727 RVA: 0x00006A2D File Offset: 0x00004C2D
		public static string DidDisconnectNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIScreenDidDisconnectNotification");
			}
		}

		// Token: 0x170000E2 RID: 226
		// (get) Token: 0x060002D8 RID: 728 RVA: 0x00006A3E File Offset: 0x00004C3E
		public static UIScreen MainScreen
		{
			get
			{
				return Runtime.GetNSObject<UIScreen>(ObjC.MessageSendIntPtr(UIScreen._classHandle, Selector.GetHandle("mainScreen")));
			}
		}

		// Token: 0x170000E3 RID: 227
		// (get) Token: 0x060002D9 RID: 729 RVA: 0x00006A59 File Offset: 0x00004C59
		public UIScreen MirroredScreen
		{
			get
			{
				return Runtime.GetNSObject<UIScreen>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("mirroredScreen")));
			}
		}

		// Token: 0x170000E4 RID: 228
		// (get) Token: 0x060002DA RID: 730 RVA: 0x00006A75 File Offset: 0x00004C75
		public static string ModeDidChangeNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIScreenModeDidChangeNotification");
			}
		}

		// Token: 0x170000E5 RID: 229
		// (get) Token: 0x060002DB RID: 731 RVA: 0x00006A86 File Offset: 0x00004C86
		public CGRect NativeBounds
		{
			get
			{
				return ObjC.MessageSendCGRect(this.Handle, Selector.GetHandle("nativeBounds"));
			}
		}

		// Token: 0x170000E6 RID: 230
		// (get) Token: 0x060002DC RID: 732 RVA: 0x00006A9D File Offset: 0x00004C9D
		public float NativeScale
		{
			get
			{
				return ObjC.MessageSendFloat(this.Handle, Selector.GetHandle("nativeScale"));
			}
		}

		// Token: 0x170000E7 RID: 231
		// (get) Token: 0x060002DD RID: 733 RVA: 0x00006AB4 File Offset: 0x00004CB4
		public UIScreenMode PreferredMode
		{
			get
			{
				return Runtime.GetNSObject<UIScreenMode>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("preferredMode")));
			}
		}

		// Token: 0x170000E8 RID: 232
		// (get) Token: 0x060002DE RID: 734 RVA: 0x00006483 File Offset: 0x00004683
		public float Scale
		{
			get
			{
				return ObjC.MessageSendFloat(this.Handle, Selector.GetHandle("scale"));
			}
		}

		// Token: 0x170000E9 RID: 233
		// (get) Token: 0x060002DF RID: 735 RVA: 0x00006AD0 File Offset: 0x00004CD0
		public static UIScreen[] Screens
		{
			get
			{
				return ObjC.FromNSArray<UIScreen>(ObjC.MessageSendIntPtr(UIScreen._classHandle, Selector.GetHandle("screens")));
			}
		}

		// Token: 0x170000EA RID: 234
		// (get) Token: 0x060002E0 RID: 736 RVA: 0x00006AEB File Offset: 0x00004CEB
		// (set) Token: 0x060002E1 RID: 737 RVA: 0x00006B02 File Offset: 0x00004D02
		public bool WantsSoftwareDimming
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("wantsSoftwareDimming"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setWantsSoftwareDimming"), value);
			}
		}

		// Token: 0x040000BF RID: 191
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIScreen");
	}
}
