﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200002D RID: 45
	public class UIActivityViewController : UIViewController
	{
		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000236 RID: 566 RVA: 0x00005C4E File Offset: 0x00003E4E
		public override IntPtr ClassHandle
		{
			get
			{
				return UIActivityViewController._classHandle;
			}
		}

		// Token: 0x06000237 RID: 567 RVA: 0x00005C58 File Offset: 0x00003E58
		public UIActivityViewController(string text)
		{
			IntPtr intPtr = ObjC.ToNSString(text);
			IntPtr arg = ObjC.ToNSArray(new IntPtr[]
			{
				intPtr
			});
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("initWithActivityItems:applicationActivities:"), arg, IntPtr.Zero);
			ObjC.MessageSend(intPtr, Selector.ReleaseHandle);
		}

		// Token: 0x06000238 RID: 568 RVA: 0x00005CAC File Offset: 0x00003EAC
		public UIActivityViewController(UIImage image)
		{
			IntPtr arg = ObjC.ToNSArray(new IntPtr[]
			{
				image.Handle
			});
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("initWithActivityItems:applicationActivities:"), arg, IntPtr.Zero);
		}

		// Token: 0x06000239 RID: 569 RVA: 0x00005CF0 File Offset: 0x00003EF0
		public UIActivityViewController(string text, UIImage image)
		{
			IntPtr intPtr = ObjC.ToNSString(text);
			IntPtr arg = ObjC.ToNSArray(new IntPtr[]
			{
				intPtr,
				image.Handle
			});
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("initWithActivityItems:applicationActivities:"), arg, IntPtr.Zero);
			ObjC.MessageSend(intPtr, Selector.ReleaseHandle);
		}

		// Token: 0x0600023A RID: 570 RVA: 0x00005D4A File Offset: 0x00003F4A
		internal UIActivityViewController(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x04000088 RID: 136
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIActivityViewController");
	}
}
