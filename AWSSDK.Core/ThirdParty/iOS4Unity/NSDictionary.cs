﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000013 RID: 19
	public class NSDictionary : NSObject
	{
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x060000F7 RID: 247 RVA: 0x00003D8F File Offset: 0x00001F8F
		public override IntPtr ClassHandle
		{
			get
			{
				return NSDictionary._classHandle;
			}
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x00003A32 File Offset: 0x00001C32
		internal NSDictionary(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x00003D96 File Offset: 0x00001F96
		public NSDictionary()
		{
			this.Handle = ObjC.MessageSendIntPtr(this.Handle, Selector.Init);
		}

		// Token: 0x060000FA RID: 250 RVA: 0x00003DB4 File Offset: 0x00001FB4
		public static NSDictionary FromDictionary(NSDictionary dictionary)
		{
			return NSDictionary.FromObjectsAndKeys(dictionary.Values, dictionary.Keys);
		}

		// Token: 0x060000FB RID: 251 RVA: 0x00003DC8 File Offset: 0x00001FC8
		public static NSDictionary FromObjectAndKey(NSObject obj, string key)
		{
			IntPtr intPtr = ObjC.ToNSString(key);
			NSDictionary nsobject = Runtime.GetNSObject<NSDictionary>(ObjC.MessageSendIntPtr(NSDictionary._classHandle, Selector.GetHandle("dictionaryWithObject:forKey:"), obj.Handle, intPtr));
			ObjC.MessageSend(intPtr, Selector.ReleaseHandle);
			return nsobject;
		}

		// Token: 0x060000FC RID: 252 RVA: 0x00003E08 File Offset: 0x00002008
		public static NSDictionary FromObjectsAndKeys(NSObject[] objs, string[] keys)
		{
			IntPtr arg = ObjC.ToNSArray(objs);
			IntPtr intPtr = ObjC.ToNSArray(keys);
			NSDictionary nsobject = Runtime.GetNSObject<NSDictionary>(ObjC.MessageSendIntPtr(NSDictionary._classHandle, Selector.GetHandle("dictionaryWithObjects:forKeys:"), arg, intPtr));
			ObjC.ReleaseNSArrayItems(intPtr);
			return nsobject;
		}

		// Token: 0x060000FD RID: 253 RVA: 0x00003E44 File Offset: 0x00002044
		public NSObject ObjectForKey(string key)
		{
			IntPtr intPtr = ObjC.ToNSString(key);
			NSObject nsobject = Runtime.GetNSObject<NSObject>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("objectForKey:"), intPtr));
			ObjC.MessageSend(intPtr, Selector.ReleaseHandle);
			return nsobject;
		}

		// Token: 0x060000FE RID: 254 RVA: 0x00003E80 File Offset: 0x00002080
		public NSObject[] ObjectsForKeys(string[] keys)
		{
			IntPtr intPtr = ObjC.ToNSArray(keys);
			NSObject[] result = ObjC.FromNSArray<NSObject>(Runtime.GetNSObject<NSObject>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("objectsForKeys:notFoundMarker:"), intPtr, new NSObject().Handle)).Handle);
			ObjC.ReleaseNSArrayItems(intPtr);
			return result;
		}

		// Token: 0x060000FF RID: 255 RVA: 0x00003EC9 File Offset: 0x000020C9
		public string[] KeysForObject(NSObject obj)
		{
			return ObjC.FromNSArray(Runtime.GetNSObject<NSObject>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("allKeysForObject:"), obj.Handle)).Handle);
		}

		// Token: 0x17000015 RID: 21
		public virtual NSObject this[string key]
		{
			get
			{
				return this.ObjectForKey(key);
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000102 RID: 258 RVA: 0x00003F05 File Offset: 0x00002105
		public uint Count
		{
			get
			{
				return ObjC.MessageSendUInt(this.Handle, Selector.GetHandle("count"));
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000103 RID: 259 RVA: 0x00003F1C File Offset: 0x0000211C
		public string[] Keys
		{
			get
			{
				return ObjC.FromNSArray(Runtime.GetNSObject<NSObject>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("allKeys"))).Handle);
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000104 RID: 260 RVA: 0x00003F42 File Offset: 0x00002142
		public NSObject[] Values
		{
			get
			{
				return ObjC.FromNSArray<NSObject>(Runtime.GetNSObject<NSObject>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("allValues"))).Handle);
			}
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00003F68 File Offset: 0x00002168
		public static NSDictionary FromFile(string path)
		{
			return Runtime.GetNSObject<NSDictionary>(ObjC.MessageSendIntPtr(NSDictionary._classHandle, Selector.GetHandle("dictionaryWithContentsOfFile:"), path));
		}

		// Token: 0x06000106 RID: 262 RVA: 0x00003F84 File Offset: 0x00002184
		public bool ContainsKey(string key)
		{
			return this.ObjectForKey(key) != null;
		}

		// Token: 0x0400004B RID: 75
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSDictionary");
	}
}
