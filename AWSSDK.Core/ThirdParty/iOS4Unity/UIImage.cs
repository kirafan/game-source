﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using UnityEngine;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000038 RID: 56
	public class UIImage : NSObject
	{
		// Token: 0x06000294 RID: 660
		[DllImport("/System/Library/Frameworks/UIKit.framework/UIKit")]
		private static extern IntPtr UIImageJPEGRepresentation(IntPtr image, float compressionQuality);

		// Token: 0x06000295 RID: 661
		[DllImport("/System/Library/Frameworks/UIKit.framework/UIKit")]
		private static extern IntPtr UIImagePNGRepresentation(IntPtr image);

		// Token: 0x06000296 RID: 662
		[DllImport("/System/Library/Frameworks/UIKit.framework/UIKit")]
		private static extern void UIImageWriteToSavedPhotosAlbum(IntPtr image, IntPtr obj, IntPtr selector, IntPtr ctx);

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x06000298 RID: 664 RVA: 0x00006457 File Offset: 0x00004657
		public override IntPtr ClassHandle
		{
			get
			{
				return UIImage._classHandle;
			}
		}

		// Token: 0x06000299 RID: 665 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIImage(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x0600029A RID: 666 RVA: 0x0000645E File Offset: 0x0000465E
		public NSData AsJPEG(float compressionQuality = 1f)
		{
			return Runtime.GetNSObject<NSData>(UIImage.UIImageJPEGRepresentation(this.Handle, compressionQuality));
		}

		// Token: 0x0600029B RID: 667 RVA: 0x00006471 File Offset: 0x00004671
		public NSData AsPNG()
		{
			return Runtime.GetNSObject<NSData>(UIImage.UIImagePNGRepresentation(this.Handle));
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x0600029C RID: 668 RVA: 0x00006483 File Offset: 0x00004683
		public float CurrentScale
		{
			get
			{
				return ObjC.MessageSendFloat(this.Handle, Selector.GetHandle("scale"));
			}
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000649A File Offset: 0x0000469A
		public static UIImage FromBundle(string name)
		{
			return Runtime.GetNSObject<UIImage>(ObjC.MessageSendIntPtr(UIImage._classHandle, Selector.GetHandle("imageNamed:"), name));
		}

		// Token: 0x0600029E RID: 670 RVA: 0x000064B6 File Offset: 0x000046B6
		public static UIImage FromFile(string filename)
		{
			filename = Path.Combine(Application.streamingAssetsPath, filename);
			return Runtime.GetNSObject<UIImage>(ObjC.MessageSendIntPtr(UIImage._classHandle, Selector.GetHandle("imageWithContentsOfFile:"), filename));
		}

		// Token: 0x0600029F RID: 671 RVA: 0x000064DF File Offset: 0x000046DF
		public static UIImage LoadFromData(NSData data)
		{
			return Runtime.GetNSObject<UIImage>(ObjC.MessageSendIntPtr(UIImage._classHandle, Selector.GetHandle("imageWithData:"), data.Handle));
		}

		// Token: 0x060002A0 RID: 672 RVA: 0x00006500 File Offset: 0x00004700
		public static UIImage LoadFromData(NSData data, float scale)
		{
			return Runtime.GetNSObject<UIImage>(ObjC.MessageSendIntPtr(UIImage._classHandle, Selector.GetHandle("imageWithData:scale:"), data.Handle, scale));
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x060002A1 RID: 673 RVA: 0x00006522 File Offset: 0x00004722
		public CGSize Size
		{
			get
			{
				return new CGSize(ObjC.MessageSendCGSize(this.Handle, "size"));
			}
		}

		// Token: 0x060002A2 RID: 674 RVA: 0x0000653C File Offset: 0x0000473C
		public void SaveToPhotosAlbum(Action<NSError> callback = null)
		{
			UIImage.<>c__DisplayClass20_0 CS$<>8__locals1 = new UIImage.<>c__DisplayClass20_0();
			CS$<>8__locals1.callback = callback;
			if (CS$<>8__locals1.callback == null)
			{
				UIImage.UIImageWriteToSavedPhotosAlbum(this.Handle, IntPtr.Zero, IntPtr.Zero, IntPtr.Zero);
				return;
			}
			UIImage.UIImageDispatcher dispatcher = new UIImage.UIImageDispatcher(CS$<>8__locals1.callback);
			Callbacks.Subscribe(dispatcher, "__onSaveToPhotoAlbum:", delegate(IntPtr obj, IntPtr e, IntPtr ctx)
			{
				CS$<>8__locals1.callback((e == IntPtr.Zero) ? null : Runtime.GetNSObject<NSError>(e));
				dispatcher.Dispose();
			});
			UIImage.UIImageWriteToSavedPhotosAlbum(this.Handle, dispatcher.Handle, ObjC.GetSelector("__onSaveToPhotoAlbum:"), IntPtr.Zero);
		}

		// Token: 0x040000B4 RID: 180
		private const string SelectorName = "__onSaveToPhotoAlbum:";

		// Token: 0x040000B5 RID: 181
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIImage");

		// Token: 0x020001B2 RID: 434
		private class UIImageDispatcher : NSObject
		{
			// Token: 0x17000411 RID: 1041
			// (get) Token: 0x06000E6F RID: 3695 RVA: 0x0002635A File Offset: 0x0002455A
			public override IntPtr ClassHandle
			{
				get
				{
					return UIImage.UIImageDispatcher._classHandle;
				}
			}

			// Token: 0x06000E70 RID: 3696 RVA: 0x00026361 File Offset: 0x00024561
			public UIImageDispatcher(Action<NSError> action)
			{
				this.Action = action;
			}

			// Token: 0x04000937 RID: 2359
			private static readonly IntPtr _classHandle = ObjC.AllocateClassPair(ObjC.GetClass("NSObject"), "__UIImageDispatcher", 0);

			// Token: 0x04000938 RID: 2360
			public readonly Action<NSError> Action;
		}
	}
}
