﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000036 RID: 54
	public enum UIDeviceOrientation
	{
		// Token: 0x040000A9 RID: 169
		Unknown,
		// Token: 0x040000AA RID: 170
		Portrait,
		// Token: 0x040000AB RID: 171
		PortraitUpsideDown,
		// Token: 0x040000AC RID: 172
		LandscapeLeft,
		// Token: 0x040000AD RID: 173
		LandscapeRight,
		// Token: 0x040000AE RID: 174
		FaceUp,
		// Token: 0x040000AF RID: 175
		FaceDown
	}
}
