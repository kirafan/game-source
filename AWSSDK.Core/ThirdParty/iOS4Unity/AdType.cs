﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200002B RID: 43
	public enum AdType
	{
		// Token: 0x04000085 RID: 133
		Banner,
		// Token: 0x04000086 RID: 134
		MediumRectangle
	}
}
