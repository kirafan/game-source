﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200003D RID: 61
	public class UIScreenMode : NSObject
	{
		// Token: 0x170000EB RID: 235
		// (get) Token: 0x060002E3 RID: 739 RVA: 0x00006B2B File Offset: 0x00004D2B
		public override IntPtr ClassHandle
		{
			get
			{
				return UIScreenMode._classHandle;
			}
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIScreenMode(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x170000EC RID: 236
		// (get) Token: 0x060002E5 RID: 741 RVA: 0x00006B32 File Offset: 0x00004D32
		public float PixelAspectRatio
		{
			get
			{
				return ObjC.MessageSendFloat(this.Handle, Selector.GetHandle("pixelAspectRatio"));
			}
		}

		// Token: 0x170000ED RID: 237
		// (get) Token: 0x060002E6 RID: 742 RVA: 0x00006B49 File Offset: 0x00004D49
		public CGSize Size
		{
			get
			{
				return ObjC.MessageSendCGSize(this.Handle, "size");
			}
		}

		// Token: 0x040000C0 RID: 192
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIScreenMode");
	}
}
