﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000027 RID: 39
	public class SKProductsResponseEventArgs : EventArgs
	{
		// Token: 0x0400007F RID: 127
		public SKProductsResponse Response;
	}
}
