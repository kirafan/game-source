﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000042 RID: 66
	public sealed class UIWindow : UIView
	{
		// Token: 0x17000100 RID: 256
		// (get) Token: 0x06000313 RID: 787 RVA: 0x00006E97 File Offset: 0x00005097
		public override IntPtr ClassHandle
		{
			get
			{
				return UIWindow._classHandle;
			}
		}

		// Token: 0x06000314 RID: 788 RVA: 0x00006E9E File Offset: 0x0000509E
		public UIWindow()
		{
		}

		// Token: 0x06000315 RID: 789 RVA: 0x00006EA6 File Offset: 0x000050A6
		public UIWindow(CGRect frame) : base(frame)
		{
		}

		// Token: 0x06000316 RID: 790 RVA: 0x000058F7 File Offset: 0x00003AF7
		internal UIWindow(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x17000101 RID: 257
		// (get) Token: 0x06000317 RID: 791 RVA: 0x00006EAF File Offset: 0x000050AF
		public bool IsKeyWindow
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isKeyWindow"));
			}
		}

		// Token: 0x06000318 RID: 792 RVA: 0x00006EC6 File Offset: 0x000050C6
		public void BecomeKeyWindow()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("becomeKeyWindow"));
		}

		// Token: 0x06000319 RID: 793 RVA: 0x00006EDD File Offset: 0x000050DD
		public void MakeKeyAndVisible()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("makeKeyAndVisible"));
		}

		// Token: 0x0600031A RID: 794 RVA: 0x00006EF4 File Offset: 0x000050F4
		public void MakeKeyWindow()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("makeKeyWindow"));
		}

		// Token: 0x0600031B RID: 795 RVA: 0x00006F0B File Offset: 0x0000510B
		public void ResignKeyWindow()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("resignKeyWindow"));
		}

		// Token: 0x17000102 RID: 258
		// (get) Token: 0x0600031C RID: 796 RVA: 0x00006F22 File Offset: 0x00005122
		// (set) Token: 0x0600031D RID: 797 RVA: 0x00006F3E File Offset: 0x0000513E
		public UIScreen Screen
		{
			get
			{
				return Runtime.GetNSObject<UIScreen>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("screen")));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setScreen:"), value.Handle);
			}
		}

		// Token: 0x17000103 RID: 259
		// (get) Token: 0x0600031E RID: 798 RVA: 0x00006F5B File Offset: 0x0000515B
		// (set) Token: 0x0600031F RID: 799 RVA: 0x00006F72 File Offset: 0x00005172
		public float WindowLevel
		{
			get
			{
				return ObjC.MessageSendFloat(this.Handle, Selector.GetHandle("windowLevel"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setWindowLevel:"), value);
			}
		}

		// Token: 0x17000104 RID: 260
		// (get) Token: 0x06000320 RID: 800 RVA: 0x00006F8A File Offset: 0x0000518A
		// (set) Token: 0x06000321 RID: 801 RVA: 0x00006FA6 File Offset: 0x000051A6
		public UIViewController RootViewController
		{
			get
			{
				return Runtime.GetNSObject<UIViewController>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("rootViewController")));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setRootViewController:"), value.Handle);
			}
		}

		// Token: 0x040000C9 RID: 201
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIWindow");
	}
}
