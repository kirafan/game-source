﻿using System;
using System.Collections.Generic;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000021 RID: 33
	public class SKPaymentQueue : NSObject
	{
		// Token: 0x1700007C RID: 124
		// (get) Token: 0x060001CE RID: 462 RVA: 0x00005231 File Offset: 0x00003431
		public override IntPtr ClassHandle
		{
			get
			{
				return SKPaymentQueue._classHandle;
			}
		}

		// Token: 0x060001CF RID: 463 RVA: 0x00005238 File Offset: 0x00003438
		internal SKPaymentQueue(IntPtr handle) : base(handle)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("addTransactionObserver:"), this.Handle);
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x060001D0 RID: 464 RVA: 0x0000525C File Offset: 0x0000345C
		public static bool CanMakePayments
		{
			get
			{
				return ObjC.MessageSendBool(SKPaymentQueue._classHandle, Selector.GetHandle("canMakePayments"));
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x060001D1 RID: 465 RVA: 0x00005272 File Offset: 0x00003472
		public SKPaymentTransaction[] Transactions
		{
			get
			{
				return ObjC.FromNSArray<SKPaymentTransaction>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("transactions")));
			}
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x060001D2 RID: 466 RVA: 0x0000528E File Offset: 0x0000348E
		public static SKPaymentQueue DefaultQueue
		{
			get
			{
				return Runtime.GetNSObject<SKPaymentQueue>(ObjC.MessageSendIntPtr(SKPaymentQueue._classHandle, Selector.GetHandle("defaultQueue")));
			}
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x060001D3 RID: 467 RVA: 0x000052A9 File Offset: 0x000034A9
		// (remove) Token: 0x060001D4 RID: 468 RVA: 0x000052B7 File Offset: 0x000034B7
		public event EventHandler RestoreCompleted
		{
			add
			{
				Callbacks.Subscribe(this, "paymentQueueRestoreCompletedTransactionsFinished:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "paymentQueueRestoreCompletedTransactionsFinished:", value);
			}
		}

		// Token: 0x14000002 RID: 2
		// (add) Token: 0x060001D5 RID: 469 RVA: 0x000052C8 File Offset: 0x000034C8
		// (remove) Token: 0x060001D6 RID: 470 RVA: 0x00005328 File Offset: 0x00003528
		public event EventHandler<NSErrorEventArgs> RestoreFailed
		{
			add
			{
				if (this._restoreFailed == null)
				{
					this._restoreFailed = new Dictionary<object, IntPtrHandler2>();
				}
				IntPtrHandler2 intPtrHandler = delegate(IntPtr _, IntPtr i)
				{
					value(this, new NSErrorEventArgs
					{
						Error = Runtime.GetNSObject<NSError>(i)
					});
				};
				this._restoreFailed[value] = intPtrHandler;
				Callbacks.Subscribe(this, "paymentQueue:restoreCompletedTransactionsFailedWithError:", intPtrHandler);
			}
			remove
			{
				IntPtrHandler2 callback;
				if (this._restoreFailed != null && this._restoreFailed.TryGetValue(value, out callback))
				{
					this._restoreFailed.Remove(value);
					Callbacks.Unsubscribe(this, "paymentQueue:restoreCompletedTransactionsFailedWithError:", callback);
				}
			}
		}

		// Token: 0x14000003 RID: 3
		// (add) Token: 0x060001D7 RID: 471 RVA: 0x00005368 File Offset: 0x00003568
		// (remove) Token: 0x060001D8 RID: 472 RVA: 0x000053C8 File Offset: 0x000035C8
		public event EventHandler<SKPaymentTransactionEventArgs> UpdatedTransactions
		{
			add
			{
				if (this._updatedTransactions == null)
				{
					this._updatedTransactions = new Dictionary<object, IntPtrHandler2>();
				}
				IntPtrHandler2 intPtrHandler = delegate(IntPtr _, IntPtr i)
				{
					value(this, new SKPaymentTransactionEventArgs
					{
						Transactions = ObjC.FromNSArray<SKPaymentTransaction>(i)
					});
				};
				this._updatedTransactions[value] = intPtrHandler;
				Callbacks.Subscribe(this, "paymentQueue:updatedTransactions:", intPtrHandler);
			}
			remove
			{
				IntPtrHandler2 callback;
				if (this._updatedTransactions != null && this._updatedTransactions.TryGetValue(value, out callback))
				{
					this._updatedTransactions.Remove(value);
					Callbacks.Unsubscribe(this, "paymentQueue:updatedTransactions:", callback);
				}
			}
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x00005406 File Offset: 0x00003606
		public void AddPayment(SKPayment payment)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("addPayment:"), payment.Handle);
		}

		// Token: 0x060001DA RID: 474 RVA: 0x00005423 File Offset: 0x00003623
		public void RestoreCompletedTransactions()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("restoreCompletedTransactions"));
		}

		// Token: 0x060001DB RID: 475 RVA: 0x0000543A File Offset: 0x0000363A
		public void FinishTransaction(SKPaymentTransaction transaction)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("finishTransaction:"), transaction.Handle);
		}

		// Token: 0x060001DC RID: 476 RVA: 0x00005457 File Offset: 0x00003657
		public override void Dispose()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("removeTransactionObserver:"), this.Handle);
			base.Dispose();
		}

		// Token: 0x04000070 RID: 112
		private static readonly IntPtr _classHandle = ObjC.GetClass("SKPaymentQueue");

		// Token: 0x04000071 RID: 113
		private Dictionary<object, IntPtrHandler2> _restoreFailed;

		// Token: 0x04000072 RID: 114
		private Dictionary<object, IntPtrHandler2> _updatedTransactions;
	}
}
