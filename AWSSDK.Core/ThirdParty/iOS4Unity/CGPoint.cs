﻿using System;
using System.Globalization;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000006 RID: 6
	[Serializable]
	public struct CGPoint
	{
		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600008B RID: 139 RVA: 0x000032C5 File Offset: 0x000014C5
		public bool IsEmpty
		{
			get
			{
				return (double)this.X == 0.0 && (double)this.Y == 0.0;
			}
		}

		// Token: 0x0600008C RID: 140 RVA: 0x000032ED File Offset: 0x000014ED
		public CGPoint(float x, float y)
		{
			this.X = x;
			this.Y = y;
		}

		// Token: 0x0600008D RID: 141 RVA: 0x000032FD File Offset: 0x000014FD
		public static CGPoint Add(CGPoint pt, CGSize sz)
		{
			return new CGPoint(pt.X + sz.Width, pt.Y + sz.Height);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x0000331E File Offset: 0x0000151E
		public static CGPoint Subtract(CGPoint pt, CGSize sz)
		{
			return new CGPoint(pt.X - sz.Width, pt.Y - sz.Height);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x0000333F File Offset: 0x0000153F
		public override bool Equals(object obj)
		{
			return obj is CGPoint && this == (CGPoint)obj;
		}

		// Token: 0x06000090 RID: 144 RVA: 0x0000335C File Offset: 0x0000155C
		public override int GetHashCode()
		{
			return (int)this.X ^ (int)this.Y;
		}

		// Token: 0x06000091 RID: 145 RVA: 0x0000336D File Offset: 0x0000156D
		public override string ToString()
		{
			return string.Format("{{X={0}, Y={1}}}", this.X.ToString(CultureInfo.CurrentCulture), this.Y.ToString(CultureInfo.CurrentCulture));
		}

		// Token: 0x06000092 RID: 146 RVA: 0x000032FD File Offset: 0x000014FD
		public static CGPoint operator +(CGPoint pt, CGSize sz)
		{
			return new CGPoint(pt.X + sz.Width, pt.Y + sz.Height);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x00003399 File Offset: 0x00001599
		public static bool operator ==(CGPoint left, CGPoint right)
		{
			return left.X == right.X && left.Y == right.Y;
		}

		// Token: 0x06000094 RID: 148 RVA: 0x000033B9 File Offset: 0x000015B9
		public static bool operator !=(CGPoint left, CGPoint right)
		{
			return left.X != right.X || left.Y != right.Y;
		}

		// Token: 0x06000095 RID: 149 RVA: 0x0000331E File Offset: 0x0000151E
		public static CGPoint operator -(CGPoint pt, CGSize sz)
		{
			return new CGPoint(pt.X - sz.Width, pt.Y - sz.Height);
		}

		// Token: 0x04000037 RID: 55
		public static readonly CGPoint Empty;

		// Token: 0x04000038 RID: 56
		public float X;

		// Token: 0x04000039 RID: 57
		public float Y;
	}
}
