﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200000C RID: 12
	// (Invoke) Token: 0x060000CE RID: 206
	public delegate void IntPtrHandler4(IntPtr arg1, IntPtr arg2, IntPtr arg3, IntPtr arg4);
}
