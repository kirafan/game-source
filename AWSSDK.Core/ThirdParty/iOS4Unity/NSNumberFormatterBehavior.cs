﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200001A RID: 26
	public enum NSNumberFormatterBehavior
	{
		// Token: 0x04000054 RID: 84
		Default,
		// Token: 0x04000055 RID: 85
		Version_10_0 = 1000,
		// Token: 0x04000056 RID: 86
		Version_10_4 = 1040
	}
}
