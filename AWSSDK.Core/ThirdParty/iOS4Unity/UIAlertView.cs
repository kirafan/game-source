﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200002E RID: 46
	public class UIAlertView : NSObject
	{
		// Token: 0x1700009B RID: 155
		// (get) Token: 0x0600023C RID: 572 RVA: 0x00005D64 File Offset: 0x00003F64
		public override IntPtr ClassHandle
		{
			get
			{
				return UIAlertView._classHandle;
			}
		}

		// Token: 0x0600023D RID: 573 RVA: 0x00005D6B File Offset: 0x00003F6B
		public UIAlertView()
		{
			this.Handle = ObjC.MessageSendIntPtr(this.Handle, Selector.Init);
			ObjC.MessageSend(this.Handle, Selector.GetHandle("setDelegate:"), this.Handle);
		}

		// Token: 0x0600023E RID: 574 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIAlertView(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x14000011 RID: 17
		// (add) Token: 0x0600023F RID: 575 RVA: 0x00005DA4 File Offset: 0x00003FA4
		// (remove) Token: 0x06000240 RID: 576 RVA: 0x00005DB2 File Offset: 0x00003FB2
		public event EventHandler<ButtonEventArgs> Clicked
		{
			add
			{
				Callbacks.Subscribe(this, "alertView:clickedButtonAtIndex:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "alertView:clickedButtonAtIndex:", value);
			}
		}

		// Token: 0x14000012 RID: 18
		// (add) Token: 0x06000241 RID: 577 RVA: 0x00005DC0 File Offset: 0x00003FC0
		// (remove) Token: 0x06000242 RID: 578 RVA: 0x00005DCE File Offset: 0x00003FCE
		public event EventHandler<ButtonEventArgs> Dismissed
		{
			add
			{
				Callbacks.Subscribe(this, "alertView:didDismissWithButtonIndex:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "alertView:didDismissWithButtonIndex:", value);
			}
		}

		// Token: 0x14000013 RID: 19
		// (add) Token: 0x06000243 RID: 579 RVA: 0x00005DDC File Offset: 0x00003FDC
		// (remove) Token: 0x06000244 RID: 580 RVA: 0x00005DEA File Offset: 0x00003FEA
		public event EventHandler<ButtonEventArgs> WillDismiss
		{
			add
			{
				Callbacks.Subscribe(this, "alertView:willDismissWithButtonIndex:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "alertView:willDismissWithButtonIndex:", value);
			}
		}

		// Token: 0x14000014 RID: 20
		// (add) Token: 0x06000245 RID: 581 RVA: 0x00005DF8 File Offset: 0x00003FF8
		// (remove) Token: 0x06000246 RID: 582 RVA: 0x00005E06 File Offset: 0x00004006
		public event EventHandler Canceled
		{
			add
			{
				Callbacks.Subscribe(this, "alertViewCancel:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "alertViewCancel:", value);
			}
		}

		// Token: 0x14000015 RID: 21
		// (add) Token: 0x06000247 RID: 583 RVA: 0x00005E14 File Offset: 0x00004014
		// (remove) Token: 0x06000248 RID: 584 RVA: 0x00005E22 File Offset: 0x00004022
		public event EventHandler Presented
		{
			add
			{
				Callbacks.Subscribe(this, "didPresentAlertView:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "didPresentAlertView:", value);
			}
		}

		// Token: 0x14000016 RID: 22
		// (add) Token: 0x06000249 RID: 585 RVA: 0x00005E30 File Offset: 0x00004030
		// (remove) Token: 0x0600024A RID: 586 RVA: 0x00005E3E File Offset: 0x0000403E
		public event EventHandler WillPresent
		{
			add
			{
				Callbacks.Subscribe(this, "willPresentAlertView:", value);
			}
			remove
			{
				Callbacks.Unsubscribe(this, "willPresentAlertView:", value);
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x0600024B RID: 587 RVA: 0x00005E4C File Offset: 0x0000404C
		// (set) Token: 0x0600024C RID: 588 RVA: 0x00005E63 File Offset: 0x00004063
		public UIAlertViewStyle AlertViewStyle
		{
			get
			{
				return (UIAlertViewStyle)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("alertViewStyle"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setAlertViewStyle:"), (int)value);
			}
		}

		// Token: 0x0600024D RID: 589 RVA: 0x00005A8A File Offset: 0x00003C8A
		public int AddButton(string title)
		{
			return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("addButtonWithTitle:"), title);
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x0600024E RID: 590 RVA: 0x00005B4A File Offset: 0x00003D4A
		public int ButtonCount
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("numberOfButtons"));
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x0600024F RID: 591 RVA: 0x00005B79 File Offset: 0x00003D79
		// (set) Token: 0x06000250 RID: 592 RVA: 0x00005B90 File Offset: 0x00003D90
		public int CancelButtonIndex
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("cancelButtonIndex"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setCancelButtonIndex:"), value);
			}
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x06000251 RID: 593 RVA: 0x00005BC2 File Offset: 0x00003DC2
		public int FirstOtherButtonIndex
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("firstOtherButtonIndex"));
			}
		}

		// Token: 0x06000252 RID: 594 RVA: 0x00005B61 File Offset: 0x00003D61
		public string ButtonTitle(int index)
		{
			return ObjC.MessageSendString(this.Handle, Selector.GetHandle("buttonTitleAtIndex:"), index);
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x06000253 RID: 595 RVA: 0x00005C26 File Offset: 0x00003E26
		public bool Visible
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isVisible"));
			}
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x06000254 RID: 596 RVA: 0x00005E7B File Offset: 0x0000407B
		// (set) Token: 0x06000255 RID: 597 RVA: 0x00005E92 File Offset: 0x00004092
		public string Message
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("message"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setMessage:"), value);
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000256 RID: 598 RVA: 0x00005BF7 File Offset: 0x00003DF7
		// (set) Token: 0x06000257 RID: 599 RVA: 0x00005C0E File Offset: 0x00003E0E
		public string Title
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("title"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setTitle:"), value);
			}
		}

		// Token: 0x06000258 RID: 600 RVA: 0x00005EAA File Offset: 0x000040AA
		public void Show()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("show"));
		}

		// Token: 0x06000259 RID: 601 RVA: 0x00005EC1 File Offset: 0x000040C1
		public void Dismiss(int buttonIndex, bool animated = true)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("dismissWithClickedButtonIndex:animated:"), buttonIndex, animated);
		}

		// Token: 0x04000089 RID: 137
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIAlertView");
	}
}
