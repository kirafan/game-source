﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000025 RID: 37
	public enum SKPaymentTransactionState
	{
		// Token: 0x04000077 RID: 119
		Purchasing,
		// Token: 0x04000078 RID: 120
		Purchased,
		// Token: 0x04000079 RID: 121
		Failed,
		// Token: 0x0400007A RID: 122
		Restored,
		// Token: 0x0400007B RID: 123
		Deferred
	}
}
