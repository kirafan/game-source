﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200000F RID: 15
	public class NSErrorEventArgs : EventArgs
	{
		// Token: 0x04000043 RID: 67
		public NSError Error;
	}
}
