﻿using System;
using System.Runtime.InteropServices;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000004 RID: 4
	public class Selector
	{
		// Token: 0x0600006D RID: 109 RVA: 0x00002C7D File Offset: 0x00000E7D
		public Selector(IntPtr sel) : this(sel, true)
		{
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00002C87 File Offset: 0x00000E87
		internal Selector(IntPtr sel, bool check)
		{
			if (check && !Selector.sel_isMapped(sel))
			{
				throw new ArgumentException("sel is not a selector handle.");
			}
			this.handle = sel;
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00002CAC File Offset: 0x00000EAC
		public Selector(string name, bool alloc)
		{
			this.handle = Selector.GetHandle(name);
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00002CC0 File Offset: 0x00000EC0
		public Selector(string name) : this(name, false)
		{
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000071 RID: 113 RVA: 0x00002CCA File Offset: 0x00000ECA
		public IntPtr Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000072 RID: 114 RVA: 0x00002CD2 File Offset: 0x00000ED2
		public string Name
		{
			get
			{
				return Marshal.PtrToStringAuto(Selector.sel_getName(this.handle));
			}
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00002CE4 File Offset: 0x00000EE4
		public static Selector Register(IntPtr handle)
		{
			return new Selector(handle);
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00002CEC File Offset: 0x00000EEC
		public static Selector FromHandle(IntPtr sel)
		{
			if (!Selector.sel_isMapped(sel))
			{
				return null;
			}
			return new Selector(sel, false);
		}

		// Token: 0x06000075 RID: 117
		[DllImport("/usr/lib/libobjc.dylib")]
		private static extern IntPtr sel_getName(IntPtr sel);

		// Token: 0x06000076 RID: 118
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "sel_registerName")]
		public static extern IntPtr GetHandle(string name);

		// Token: 0x06000077 RID: 119
		[DllImport("/usr/lib/libobjc.dylib")]
		private static extern bool sel_isMapped(IntPtr sel);

		// Token: 0x0400001D RID: 29
		public static readonly IntPtr Init = Selector.GetHandle("init");

		// Token: 0x0400001E RID: 30
		public static readonly IntPtr InitWithCoder = Selector.GetHandle("initWithCoder:");

		// Token: 0x0400001F RID: 31
		public static readonly IntPtr InitWithName = Selector.GetHandle("initWithName:");

		// Token: 0x04000020 RID: 32
		public static readonly IntPtr InitWithFrame = Selector.GetHandle("initWithFrame:");

		// Token: 0x04000021 RID: 33
		private static IntPtr MethodSignatureForSelector = Selector.GetHandle("methodSignatureForSelector:");

		// Token: 0x04000022 RID: 34
		private static IntPtr FrameLength = Selector.GetHandle("frameLength");

		// Token: 0x04000023 RID: 35
		internal static IntPtr RetainCount = Selector.GetHandle("retainCount");

		// Token: 0x04000024 RID: 36
		internal const string Alloc = "alloc";

		// Token: 0x04000025 RID: 37
		internal const string Release = "release";

		// Token: 0x04000026 RID: 38
		internal const string Retain = "retain";

		// Token: 0x04000027 RID: 39
		internal const string Autorelease = "autorelease";

		// Token: 0x04000028 RID: 40
		internal const string DoesNotRecognizeSelector = "doesNotRecognizeSelector:";

		// Token: 0x04000029 RID: 41
		internal const string PerformSelectorOnMainThreadWithObjectWaitUntilDone = "performSelectorOnMainThread:withObject:waitUntilDone:";

		// Token: 0x0400002A RID: 42
		internal const string PerformSelectorWithObjectAfterDelay = "performSelector:withObject:afterDelay:";

		// Token: 0x0400002B RID: 43
		internal const string UTF8String = "UTF8String";

		// Token: 0x0400002C RID: 44
		internal static IntPtr AllocHandle = Selector.GetHandle("alloc");

		// Token: 0x0400002D RID: 45
		internal static IntPtr ReleaseHandle = Selector.GetHandle("release");

		// Token: 0x0400002E RID: 46
		internal static IntPtr RetainHandle = Selector.GetHandle("retain");

		// Token: 0x0400002F RID: 47
		internal static IntPtr AutoreleaseHandle = Selector.GetHandle("autorelease");

		// Token: 0x04000030 RID: 48
		internal static IntPtr DoesNotRecognizeSelectorHandle = Selector.GetHandle("doesNotRecognizeSelector:");

		// Token: 0x04000031 RID: 49
		internal static IntPtr PerformSelectorOnMainThreadWithObjectWaitUntilDoneHandle = Selector.GetHandle("performSelectorOnMainThread:withObject:waitUntilDone:");

		// Token: 0x04000032 RID: 50
		internal static IntPtr PerformSelectorWithObjectAfterDelayHandle = Selector.GetHandle("performSelector:withObject:afterDelay:");

		// Token: 0x04000033 RID: 51
		internal static IntPtr UTF8StringHandle = Selector.GetHandle("UTF8String");

		// Token: 0x04000034 RID: 52
		internal IntPtr handle;
	}
}
