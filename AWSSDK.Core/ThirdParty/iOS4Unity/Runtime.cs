﻿using System;
using System.Collections.Generic;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000003 RID: 3
	public static class Runtime
	{
		// Token: 0x06000067 RID: 103 RVA: 0x0000269C File Offset: 0x0000089C
		public static void RegisterType<T>(Func<IntPtr, object> constructor) where T : NSObject
		{
			object contructorLock = Runtime._contructorLock;
			lock (contructorLock)
			{
				Runtime._constructors[typeof(T)] = constructor;
			}
		}

		// Token: 0x06000068 RID: 104 RVA: 0x000026E4 File Offset: 0x000008E4
		public static T GetNSObject<T>(IntPtr handle) where T : NSObject
		{
			if (handle == IntPtr.Zero)
			{
				return default(T);
			}
			object objectLock = Runtime._objectLock;
			lock (objectLock)
			{
				object obj;
				if (Runtime._objects.TryGetValue(handle, out obj))
				{
					return (T)((object)obj);
				}
			}
			return Runtime.ConstructNSObject<T>(handle);
		}

		// Token: 0x06000069 RID: 105 RVA: 0x00002750 File Offset: 0x00000950
		private static T ConstructNSObject<T>(IntPtr handle) where T : NSObject
		{
			object contructorLock = Runtime._contructorLock;
			Func<IntPtr, object> func;
			lock (contructorLock)
			{
				if (!Runtime._constructors.TryGetValue(typeof(T), out func))
				{
					throw new NotImplementedException("Type's constructor not registered: " + typeof(T));
				}
			}
			return (T)((object)func(handle));
		}

		// Token: 0x0600006A RID: 106 RVA: 0x000027C0 File Offset: 0x000009C0
		public static void RegisterNSObject(NSObject obj)
		{
			if (obj == null || obj.Handle == IntPtr.Zero)
			{
				return;
			}
			object objectLock = Runtime._objectLock;
			lock (objectLock)
			{
				Runtime._objects[obj.Handle] = obj;
			}
		}

		// Token: 0x0600006B RID: 107 RVA: 0x0000281C File Offset: 0x00000A1C
		public static void UnregisterNSObject(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return;
			}
			object objectLock = Runtime._objectLock;
			lock (objectLock)
			{
				Runtime._objects.Remove(handle);
			}
		}

		// Token: 0x04000019 RID: 25
		private static object _contructorLock = new object();

		// Token: 0x0400001A RID: 26
		private static object _objectLock = new object();

		// Token: 0x0400001B RID: 27
		private static readonly Dictionary<Type, Func<IntPtr, object>> _constructors = new Dictionary<Type, Func<IntPtr, object>>
		{
			{
				typeof(AdBannerView),
				(IntPtr h) => new AdBannerView(h)
			},
			{
				typeof(NSBundle),
				(IntPtr h) => new NSBundle(h)
			},
			{
				typeof(NSData),
				(IntPtr h) => new NSData(h)
			},
			{
				typeof(NSDictionary),
				(IntPtr h) => new NSDictionary(h)
			},
			{
				typeof(NSMutableDictionary),
				(IntPtr h) => new NSMutableDictionary(h)
			},
			{
				typeof(NSError),
				(IntPtr h) => new NSError(h)
			},
			{
				typeof(NSLocale),
				(IntPtr h) => new NSLocale(h)
			},
			{
				typeof(NSNotification),
				(IntPtr h) => new NSNotification(h)
			},
			{
				typeof(NSNotificationCenter),
				(IntPtr h) => new NSNotificationCenter(h)
			},
			{
				typeof(NSNumberFormatter),
				(IntPtr h) => new NSNumberFormatter(h)
			},
			{
				typeof(NSObject),
				(IntPtr h) => new NSObject(h)
			},
			{
				typeof(NSTimeZone),
				(IntPtr h) => new NSTimeZone(h)
			},
			{
				typeof(SKPayment),
				(IntPtr h) => new SKPayment(h)
			},
			{
				typeof(SKPaymentQueue),
				(IntPtr h) => new SKPaymentQueue(h)
			},
			{
				typeof(SKPaymentTransaction),
				(IntPtr h) => new SKPaymentTransaction(h)
			},
			{
				typeof(SKProduct),
				(IntPtr h) => new SKProduct(h)
			},
			{
				typeof(SKProductsRequest),
				(IntPtr h) => new SKProductsRequest(h)
			},
			{
				typeof(SKProductsResponse),
				(IntPtr h) => new SKProductsResponse(h)
			},
			{
				typeof(UIActionSheet),
				(IntPtr h) => new UIActionSheet(h)
			},
			{
				typeof(UIActivityViewController),
				(IntPtr h) => new UIActivityViewController(h)
			},
			{
				typeof(UIAlertView),
				(IntPtr h) => new UIAlertView(h)
			},
			{
				typeof(UIApplication),
				(IntPtr h) => new UIApplication(h)
			},
			{
				typeof(UIDevice),
				(IntPtr h) => new UIDevice(h)
			},
			{
				typeof(UIImage),
				(IntPtr h) => new UIImage(h)
			},
			{
				typeof(UILocalNotification),
				(IntPtr h) => new UILocalNotification(h)
			},
			{
				typeof(UIScreen),
				(IntPtr h) => new UIScreen(h)
			},
			{
				typeof(UIScreenMode),
				(IntPtr h) => new UIScreenMode(h)
			},
			{
				typeof(UIUserNotificationSettings),
				(IntPtr h) => new UIUserNotificationSettings(h)
			},
			{
				typeof(UIView),
				(IntPtr h) => new UIView(h)
			},
			{
				typeof(UIViewController),
				(IntPtr h) => new UIViewController(h)
			},
			{
				typeof(UIWindow),
				(IntPtr h) => new UIWindow(h)
			}
		};

		// Token: 0x0400001C RID: 28
		private static readonly Dictionary<IntPtr, object> _objects = new Dictionary<IntPtr, object>();
	}
}
