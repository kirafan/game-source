﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000014 RID: 20
	public class NSError : NSObject
	{
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000108 RID: 264 RVA: 0x00003FA1 File Offset: 0x000021A1
		public override IntPtr ClassHandle
		{
			get
			{
				return NSError._classHandle;
			}
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00003FA8 File Offset: 0x000021A8
		public NSError(string domain, int code)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("initWithDomain:code:userInfo:"), domain, code, IntPtr.Zero);
		}

		// Token: 0x0600010A RID: 266 RVA: 0x00003A32 File Offset: 0x00001C32
		internal NSError(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600010B RID: 267 RVA: 0x00003FCC File Offset: 0x000021CC
		public int Code
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("code"));
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600010C RID: 268 RVA: 0x00003FE3 File Offset: 0x000021E3
		public string Domain
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("domain"));
			}
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600010D RID: 269 RVA: 0x00003FFA File Offset: 0x000021FA
		public string LocalizedDescription
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("localizedDescription"));
			}
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00004011 File Offset: 0x00002211
		public override string ToString()
		{
			return this.LocalizedDescription;
		}

		// Token: 0x0400004C RID: 76
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSError");
	}
}
