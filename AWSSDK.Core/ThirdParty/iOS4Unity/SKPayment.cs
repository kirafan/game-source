﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000020 RID: 32
	public class SKPayment : NSObject
	{
		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060001C7 RID: 455 RVA: 0x000051B3 File Offset: 0x000033B3
		public override IntPtr ClassHandle
		{
			get
			{
				return SKPayment._classHandle;
			}
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x00003A32 File Offset: 0x00001C32
		internal SKPayment(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x000051BA File Offset: 0x000033BA
		public static SKPayment PaymentWithProduct(SKProduct product)
		{
			return Runtime.GetNSObject<SKPayment>(ObjC.MessageSendIntPtr(SKPayment._classHandle, Selector.GetHandle("paymentWithProduct:"), product.Handle));
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060001CA RID: 458 RVA: 0x000051DB File Offset: 0x000033DB
		public string ApplicationUsername
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("applicationUsername"));
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060001CB RID: 459 RVA: 0x000051F2 File Offset: 0x000033F2
		public string ProductIdentifier
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("productIdentifier"));
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x060001CC RID: 460 RVA: 0x00005209 File Offset: 0x00003409
		public int Quantity
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("quantity"));
			}
		}

		// Token: 0x0400006F RID: 111
		private static readonly IntPtr _classHandle = ObjC.GetClass("SKPayment");
	}
}
