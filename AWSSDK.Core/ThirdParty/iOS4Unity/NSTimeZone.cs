﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200001F RID: 31
	public class NSTimeZone : NSObject
	{
		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060001B1 RID: 433 RVA: 0x00004FBB File Offset: 0x000031BB
		public override IntPtr ClassHandle
		{
			get
			{
				return NSTimeZone._classHandle;
			}
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x00004FC2 File Offset: 0x000031C2
		public NSTimeZone()
		{
			ObjC.MessageSendIntPtr(this.Handle, Selector.Init);
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x00004FDB File Offset: 0x000031DB
		public NSTimeZone(string name)
		{
			this.Handle = ObjC.MessageSendIntPtr(this.Handle, Selector.InitWithName, name);
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x00003A32 File Offset: 0x00001C32
		internal NSTimeZone(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x00004FFA File Offset: 0x000031FA
		public string Abbreviation(DateTime date)
		{
			return ObjC.MessageSendString(this.Handle, Selector.GetHandle("abbreviationForDate:"), date);
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00005017 File Offset: 0x00003217
		public virtual string Abbreviation()
		{
			return ObjC.MessageSendString(this.Handle, Selector.GetHandle("abbreviation"));
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x0000502E File Offset: 0x0000322E
		public static NSTimeZone FromAbbreviation(string abbreviation)
		{
			return Runtime.GetNSObject<NSTimeZone>(ObjC.MessageSendIntPtr(NSTimeZone._classHandle, Selector.GetHandle("timeZoneWithAbbreviation:"), abbreviation));
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x0000504A File Offset: 0x0000324A
		public static NSTimeZone FromName(string name, NSData data)
		{
			return Runtime.GetNSObject<NSTimeZone>(ObjC.MessageSendIntPtr(NSTimeZone._classHandle, Selector.GetHandle("timeZoneWithName:data:"), name, data.Handle));
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x0000506C File Offset: 0x0000326C
		public static NSTimeZone FromName(string name)
		{
			return Runtime.GetNSObject<NSTimeZone>(ObjC.MessageSendIntPtr(NSTimeZone._classHandle, Selector.GetHandle("timeZoneWithName:"), name));
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060001BA RID: 442 RVA: 0x00005088 File Offset: 0x00003288
		public static string[] KnownTimeZoneNames
		{
			get
			{
				return ObjC.FromNSArray(ObjC.MessageSendIntPtr(NSTimeZone._classHandle, Selector.GetHandle("knownTimeZoneNames")));
			}
		}

		// Token: 0x060001BB RID: 443 RVA: 0x000050A3 File Offset: 0x000032A3
		public static void ResetSystemTimeZone()
		{
			ObjC.MessageSend(NSTimeZone._classHandle, Selector.GetHandle("resetSystemTimeZone"));
		}

		// Token: 0x060001BC RID: 444 RVA: 0x000050B9 File Offset: 0x000032B9
		public int SecondsFromGMT(DateTime date)
		{
			return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("secondsFromGMTForDate:"), date);
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060001BD RID: 445 RVA: 0x000050D6 File Offset: 0x000032D6
		public static NSDictionary Abbreviations
		{
			get
			{
				return Runtime.GetNSObject<NSDictionary>(ObjC.MessageSendIntPtr(NSTimeZone._classHandle, Selector.GetHandle("abbreviationDictionary")));
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060001BE RID: 446 RVA: 0x000050F1 File Offset: 0x000032F1
		public NSData Data
		{
			get
			{
				return Runtime.GetNSObject<NSData>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("data")));
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060001BF RID: 447 RVA: 0x0000510D File Offset: 0x0000330D
		public static string DataVersion
		{
			get
			{
				return ObjC.MessageSendString(NSTimeZone._classHandle, Selector.GetHandle("timeZoneDataVersion"));
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060001C0 RID: 448 RVA: 0x00005123 File Offset: 0x00003323
		// (set) Token: 0x060001C1 RID: 449 RVA: 0x0000513E File Offset: 0x0000333E
		public static NSTimeZone DefaultTimeZone
		{
			get
			{
				return Runtime.GetNSObject<NSTimeZone>(ObjC.MessageSendIntPtr(NSTimeZone._classHandle, Selector.GetHandle("defaultTimeZone")));
			}
			set
			{
				ObjC.MessageSend(NSTimeZone._classHandle, Selector.GetHandle("setDefaultTimeZone:"), value);
			}
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060001C2 RID: 450 RVA: 0x00005155 File Offset: 0x00003355
		public int GetSecondsFromGMT
		{
			get
			{
				return ObjC.MessageSendInt(this.Handle, Selector.GetHandle("secondsFromGMT"));
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060001C3 RID: 451 RVA: 0x0000516C File Offset: 0x0000336C
		public static NSTimeZone LocalTimeZone
		{
			get
			{
				return Runtime.GetNSObject<NSTimeZone>(ObjC.MessageSendIntPtr(NSTimeZone._classHandle, Selector.GetHandle("localTimeZone")));
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x060001C4 RID: 452 RVA: 0x0000448F File Offset: 0x0000268F
		public string Name
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("name"));
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060001C5 RID: 453 RVA: 0x00005187 File Offset: 0x00003387
		public static NSTimeZone SystemTimeZone
		{
			get
			{
				return Runtime.GetNSObject<NSTimeZone>(ObjC.MessageSendIntPtr(NSTimeZone._classHandle, Selector.GetHandle("systemTimeZone")));
			}
		}

		// Token: 0x0400006E RID: 110
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSTimeZone");
	}
}
