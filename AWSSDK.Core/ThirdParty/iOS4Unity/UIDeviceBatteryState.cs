﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000035 RID: 53
	public enum UIDeviceBatteryState
	{
		// Token: 0x040000A4 RID: 164
		Unknown,
		// Token: 0x040000A5 RID: 165
		Unplugged,
		// Token: 0x040000A6 RID: 166
		Charging,
		// Token: 0x040000A7 RID: 167
		Full
	}
}
