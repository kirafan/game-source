﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200001E RID: 30
	public class NSObject : IDisposable
	{
		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060001A9 RID: 425 RVA: 0x00004EC2 File Offset: 0x000030C2
		public virtual IntPtr ClassHandle
		{
			get
			{
				return NSObject._classHandle;
			}
		}

		// Token: 0x060001AA RID: 426 RVA: 0x00004ECC File Offset: 0x000030CC
		~NSObject()
		{
			this.Dispose();
		}

		// Token: 0x060001AB RID: 427 RVA: 0x00004EF8 File Offset: 0x000030F8
		public NSObject(IntPtr handle)
		{
			this.Handle = handle;
			Runtime.RegisterNSObject(this);
		}

		// Token: 0x060001AC RID: 428 RVA: 0x00004F0D File Offset: 0x0000310D
		public NSObject()
		{
			this.Handle = ObjC.MessageSendIntPtr(this.ClassHandle, Selector.AllocHandle);
			Runtime.RegisterNSObject(this);
			this._shouldRelease = true;
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060001AD RID: 429 RVA: 0x00004F38 File Offset: 0x00003138
		public string Description
		{
			get
			{
				return ObjC.FromNSString(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("description")));
			}
		}

		// Token: 0x060001AE RID: 430 RVA: 0x00004F54 File Offset: 0x00003154
		public override string ToString()
		{
			return this.Description;
		}

		// Token: 0x060001AF RID: 431 RVA: 0x00004F5C File Offset: 0x0000315C
		public virtual void Dispose()
		{
			GC.SuppressFinalize(this);
			if (this.Handle != IntPtr.Zero)
			{
				Runtime.UnregisterNSObject(this.Handle);
				Callbacks.UnsubscribeAll(this);
				if (this._shouldRelease)
				{
					ObjC.MessageSend(this.Handle, Selector.ReleaseHandle);
				}
			}
		}

		// Token: 0x0400006B RID: 107
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSObject");

		// Token: 0x0400006C RID: 108
		public IntPtr Handle;

		// Token: 0x0400006D RID: 109
		private readonly bool _shouldRelease;
	}
}
