﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000040 RID: 64
	public class UIView : NSObject
	{
		// Token: 0x170000F0 RID: 240
		// (get) Token: 0x060002EE RID: 750 RVA: 0x00006BBC File Offset: 0x00004DBC
		public override IntPtr ClassHandle
		{
			get
			{
				return UIView._classHandle;
			}
		}

		// Token: 0x060002EF RID: 751 RVA: 0x00004FC2 File Offset: 0x000031C2
		public UIView()
		{
			ObjC.MessageSendIntPtr(this.Handle, Selector.Init);
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x00006BC3 File Offset: 0x00004DC3
		public UIView(CGRect frame)
		{
			this.Handle = ObjC.MessageSendIntPtr(this.Handle, Selector.InitWithFrame, frame);
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIView(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x170000F1 RID: 241
		// (get) Token: 0x060002F2 RID: 754 RVA: 0x00006BE2 File Offset: 0x00004DE2
		// (set) Token: 0x060002F3 RID: 755 RVA: 0x00006BF9 File Offset: 0x00004DF9
		public CGRect Frame
		{
			get
			{
				return ObjC.MessageSendCGRect(this.Handle, Selector.GetHandle("frame"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setFrame:"), value);
			}
		}

		// Token: 0x170000F2 RID: 242
		// (get) Token: 0x060002F4 RID: 756 RVA: 0x0000698C File Offset: 0x00004B8C
		// (set) Token: 0x060002F5 RID: 757 RVA: 0x00006C11 File Offset: 0x00004E11
		public CGRect Bounds
		{
			get
			{
				return ObjC.MessageSendCGRect(this.Handle, Selector.GetHandle("bounds"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setBounds:"), value);
			}
		}

		// Token: 0x170000F3 RID: 243
		// (get) Token: 0x060002F6 RID: 758 RVA: 0x00006C29 File Offset: 0x00004E29
		// (set) Token: 0x060002F7 RID: 759 RVA: 0x00006C3B File Offset: 0x00004E3B
		public CGPoint Center
		{
			get
			{
				return ObjC.MessageSendCGPoint(this.Handle, "center");
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setCenter:"), value);
			}
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x00006C53 File Offset: 0x00004E53
		public void AddSubview(UIView view)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("addSubview:"), view.Handle);
		}

		// Token: 0x170000F4 RID: 244
		// (get) Token: 0x060002F9 RID: 761 RVA: 0x00006C70 File Offset: 0x00004E70
		public UIView[] Subviews
		{
			get
			{
				return ObjC.FromNSArray<UIView>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("subviews")));
			}
		}

		// Token: 0x170000F5 RID: 245
		// (get) Token: 0x060002FA RID: 762 RVA: 0x00006C8C File Offset: 0x00004E8C
		public UIView Superview
		{
			get
			{
				return Runtime.GetNSObject<UIView>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("superview")));
			}
		}

		// Token: 0x060002FB RID: 763 RVA: 0x00006CA8 File Offset: 0x00004EA8
		public void BringSubviewToFront(UIView view)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("bringSubviewToFront:"), view.Handle);
		}

		// Token: 0x060002FC RID: 764 RVA: 0x00006CC5 File Offset: 0x00004EC5
		public void SendSubviewToBack(UIView view)
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("sendSubviewToBack:"), view.Handle);
		}

		// Token: 0x170000F6 RID: 246
		// (get) Token: 0x060002FD RID: 765 RVA: 0x00006CE2 File Offset: 0x00004EE2
		// (set) Token: 0x060002FE RID: 766 RVA: 0x00006CF9 File Offset: 0x00004EF9
		public bool Hidden
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isHidden"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setHidden:"), value);
			}
		}

		// Token: 0x170000F7 RID: 247
		// (get) Token: 0x060002FF RID: 767 RVA: 0x00006D11 File Offset: 0x00004F11
		// (set) Token: 0x06000300 RID: 768 RVA: 0x00006D28 File Offset: 0x00004F28
		public float Alpha
		{
			get
			{
				return ObjC.MessageSendFloat(this.Handle, Selector.GetHandle("alpha"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setAlpha:"), value);
			}
		}

		// Token: 0x170000F8 RID: 248
		// (get) Token: 0x06000301 RID: 769 RVA: 0x00006D40 File Offset: 0x00004F40
		public UIWindow Window
		{
			get
			{
				return Runtime.GetNSObject<UIWindow>(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("window")));
			}
		}

		// Token: 0x06000302 RID: 770 RVA: 0x00006D5C File Offset: 0x00004F5C
		public void RemoveFromSuperview()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("removeFromSuperview"));
		}

		// Token: 0x040000C7 RID: 199
		private static readonly IntPtr _classHandle = ObjC.GetClass("UIView");
	}
}
