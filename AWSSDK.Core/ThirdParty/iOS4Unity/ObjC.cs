﻿using System;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000002 RID: 2
	public static class ObjC
	{
		// Token: 0x06000001 RID: 1
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "sel_registerName")]
		public static extern IntPtr GetSelector(string name);

		// Token: 0x06000002 RID: 2
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "sel_getName")]
		public static extern string GetSelectorName(IntPtr selector);

		// Token: 0x06000003 RID: 3
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_getClass")]
		public static extern IntPtr GetClass(string name);

		// Token: 0x06000004 RID: 4
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_allocateClassPair")]
		public static extern IntPtr AllocateClassPair(IntPtr superclass, string name, int extraBytes);

		// Token: 0x06000005 RID: 5
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "class_addMethod")]
		public static extern bool AddMethod(IntPtr cls, IntPtr selector, Delegate imp, string types);

		// Token: 0x06000006 RID: 6
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = ThirdParty.iOS4Unity.Marshalers.NSDateReleaseMarshaler)] object arg1);

		// Token: 0x06000007 RID: 7
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector);

		// Token: 0x06000008 RID: 8
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, bool arg1);

		// Token: 0x06000009 RID: 9
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, CGSize arg1);

		// Token: 0x0600000A RID: 10
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, IntPtr arg1);

		// Token: 0x0600000B RID: 11
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, CGRect arg1);

		// Token: 0x0600000C RID: 12
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, CGPoint arg1);

		// Token: 0x0600000D RID: 13
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, int arg1);

		// Token: 0x0600000E RID: 14
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, float arg1);

		// Token: 0x0600000F RID: 15
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1);

		// Token: 0x06000010 RID: 16
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1, IntPtr arg2);

		// Token: 0x06000011 RID: 17
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1, int arg2);

		// Token: 0x06000012 RID: 18
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1, int arg2, IntPtr arg3);

		// Token: 0x06000013 RID: 19
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, IntPtr arg1, IntPtr arg2, double arg3);

		// Token: 0x06000014 RID: 20
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, IntPtr arg1, bool arg2, IntPtr arg3);

		// Token: 0x06000015 RID: 21
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, IntPtr arg1, IntPtr arg2, [MarshalAs(UnmanagedType.LPStr)] string arg3, IntPtr arg4);

		// Token: 0x06000016 RID: 22
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, CGRect arg1, IntPtr arg2, uint arg3, bool arg4);

		// Token: 0x06000017 RID: 23
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, IntPtr arg1, bool arg2);

		// Token: 0x06000018 RID: 24
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, int arg1, bool arg2);

		// Token: 0x06000019 RID: 25
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, bool arg1, bool arg2);

		// Token: 0x0600001A RID: 26
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern void MessageSend(IntPtr receiver, IntPtr selector, bool arg1, IntPtr arg2);

		// Token: 0x0600001B RID: 27
		[CLSCompliant(false)]
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern uint MessageSendUInt(IntPtr receiver, IntPtr selector);

		// Token: 0x0600001C RID: 28
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern int MessageSendInt(IntPtr receiver, IntPtr selector);

		// Token: 0x0600001D RID: 29
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern int MessageSendInt(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1);

		// Token: 0x0600001E RID: 30
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern int MessageSendInt(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] object arg1);

		// Token: 0x0600001F RID: 31
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector);

		// Token: 0x06000020 RID: 32
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1);

		// Token: 0x06000021 RID: 33
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, CGRect arg1);

		// Token: 0x06000022 RID: 34
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, CGSize arg1, bool arg2);

		// Token: 0x06000023 RID: 35
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, double arg1);

		// Token: 0x06000024 RID: 36
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, int arg1);

		// Token: 0x06000025 RID: 37
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, int arg1, bool arg2);

		// Token: 0x06000026 RID: 38
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, double arg1, int arg2);

		// Token: 0x06000027 RID: 39
		[CLSCompliant(false)]
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, uint arg1);

		// Token: 0x06000028 RID: 40
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, IntPtr arg1);

		// Token: 0x06000029 RID: 41
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, IntPtr arg1, IntPtr arg2);

		// Token: 0x0600002A RID: 42
		[CLSCompliant(false)]
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, uint arg1, IntPtr arg2);

		// Token: 0x0600002B RID: 43
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, IntPtr arg1, int arg2);

		// Token: 0x0600002C RID: 44
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, IntPtr arg1, float arg2);

		// Token: 0x0600002D RID: 45
		[CLSCompliant(false)]
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, IntPtr arg1, uint arg2);

		// Token: 0x0600002E RID: 46
		[CLSCompliant(false)]
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, IntPtr arg1, uint arg2, bool arg3);

		// Token: 0x0600002F RID: 47
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1, IntPtr arg2);

		// Token: 0x06000030 RID: 48
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1, uint arg2, out IntPtr arg3);

		// Token: 0x06000031 RID: 49
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern bool MessageSendBool(IntPtr receiver, IntPtr selector);

		// Token: 0x06000032 RID: 50
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern bool MessageSendBool(IntPtr receiver, IntPtr selector, bool arg1);

		// Token: 0x06000033 RID: 51
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern bool MessageSendBool(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = ThirdParty.iOS4Unity.Marshalers.NSDateReleaseMarshaler)] object arg1);

		// Token: 0x06000034 RID: 52
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern bool MessageSendBool(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1);

		// Token: 0x06000035 RID: 53
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern bool MessageSendBool(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] string arg1, bool arg2);

		// Token: 0x06000036 RID: 54
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern double MessageSendDouble(IntPtr receiver, IntPtr selector);

		// Token: 0x06000037 RID: 55
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern double MessageSendDouble(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.LPStr)] object arg1);

		// Token: 0x06000038 RID: 56
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern float MessageSendFloat(IntPtr receiver, IntPtr selector);

		// Token: 0x06000039 RID: 57
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern float MessageSendFloat(IntPtr receiver, IntPtr selector, float arg1);

		// Token: 0x0600003A RID: 58
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = ThirdParty.iOS4Unity.Marshalers.NSDateMarshaler)]
		public static extern object MessageSendDate(IntPtr receiver, IntPtr selector);

		// Token: 0x0600003B RID: 59
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = ThirdParty.iOS4Unity.Marshalers.NSDateMarshaler)]
		public static extern object MessageSendDate(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = ThirdParty.iOS4Unity.Marshalers.NSDateReleaseMarshaler)] object arg1);

		// Token: 0x0600003C RID: 60
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.BStr)]
		public static extern string MessageSendString(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = ThirdParty.iOS4Unity.Marshalers.NSDateReleaseMarshaler)] object arg1);

		// Token: 0x0600003D RID: 61
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.BStr)]
		public static extern string MessageSendString(IntPtr receiver, IntPtr selector);

		// Token: 0x0600003E RID: 62
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.BStr)]
		public static extern string MessageSendString(IntPtr receiver, IntPtr selector, IntPtr arg1);

		// Token: 0x0600003F RID: 63
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.BStr)]
		public static extern string MessageSendString(IntPtr receiver, IntPtr selector, int arg1);

		// Token: 0x06000040 RID: 64
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.BStr)]
		public static extern string MessageSendString(IntPtr receiver, IntPtr selector, double arg1);

		// Token: 0x06000041 RID: 65
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.BStr)]
		public static extern string MessageSendString(IntPtr receiver, IntPtr selector, IntPtr arg1, IntPtr arg2);

		// Token: 0x06000042 RID: 66
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.BStr)]
		public static extern string MessageSendString(IntPtr receiver, IntPtr selector, IntPtr arg1, int arg2);

		// Token: 0x06000043 RID: 67
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		[return: MarshalAs(UnmanagedType.BStr)]
		public static extern string MessageSendString(IntPtr receiver, IntPtr selector, IntPtr arg1, IntPtr arg2, IntPtr arg3);

		// Token: 0x06000044 RID: 68 RVA: 0x00002050 File Offset: 0x00000250
		public static string MessageSendString(IntPtr receiver, IntPtr selector, string arg1)
		{
			return ObjC.MessageSendString(receiver, selector, ObjC.ToNSString(arg1));
		}

		// Token: 0x06000045 RID: 69 RVA: 0x0000205F File Offset: 0x0000025F
		public static string MessageSendString(IntPtr receiver, IntPtr selector, string arg1, string arg2, string arg3)
		{
			return ObjC.MessageSendString(receiver, selector, ObjC.ToNSString(arg1), ObjC.ToNSString(arg2), ObjC.ToNSString(arg3));
		}

		// Token: 0x06000046 RID: 70
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr_NSUrl(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = ThirdParty.iOS4Unity.NSUrlMarshaler)] string arg1);

		// Token: 0x06000047 RID: 71
		[CLSCompliant(false)]
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern IntPtr MessageSendIntPtr_NSUrl(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = ThirdParty.iOS4Unity.NSUrlMarshaler)] string arg1, uint arg2, out IntPtr arg3);

		// Token: 0x06000048 RID: 72
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend")]
		public static extern bool MessageSendBool_NSUrl(IntPtr receiver, IntPtr selector, [MarshalAs(UnmanagedType.CustomMarshaler, MarshalTypeRef = ThirdParty.iOS4Unity.NSUrlMarshaler)] string arg1);

		// Token: 0x06000049 RID: 73
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend_stret")]
		public static extern CGRect MessageSendCGRect(IntPtr receiver, IntPtr selector);

		// Token: 0x0600004A RID: 74
		[CLSCompliant(false)]
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend_stret")]
		public static extern CGPoint _MessageSendStretCGPoint(IntPtr receiver, IntPtr selector);

		// Token: 0x0600004B RID: 75
		[CLSCompliant(false)]
		[DllImport("/usr/lib/libobjc.dylib", EntryPoint = "objc_msgSend_stret")]
		public static extern CGSize _MessageSendStretCGSize(IntPtr receiver, IntPtr selector);

		// Token: 0x0600004C RID: 76 RVA: 0x0000207C File Offset: 0x0000027C
		public static CGSize MessageSendCGSize(IntPtr receiver, string selector)
		{
			Selector selector2 = new Selector(selector);
			return ObjC._MessageSendStretCGSize(receiver, selector2.Handle);
		}

		// Token: 0x0600004D RID: 77 RVA: 0x0000209C File Offset: 0x0000029C
		public static CGPoint MessageSendCGPoint(IntPtr receiver, string selector)
		{
			Selector selector2 = new Selector(selector);
			return ObjC._MessageSendStretCGPoint(receiver, selector2.Handle);
		}

		// Token: 0x0600004E RID: 78
		[DllImport("/usr/lib/libSystem.dylib")]
		private static extern IntPtr dlsym(IntPtr handle, string symbol);

		// Token: 0x0600004F RID: 79
		[DllImport("/usr/lib/libSystem.dylib")]
		private static extern IntPtr dlopen(string path, int mode);

		// Token: 0x06000050 RID: 80 RVA: 0x000020BC File Offset: 0x000002BC
		public static string FromNSString(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return null;
			}
			return Marshal.PtrToStringAuto(ObjC.MessageSendIntPtr(handle, Selector.UTF8StringHandle));
		}

		// Token: 0x06000051 RID: 81 RVA: 0x000020E0 File Offset: 0x000002E0
		public static DateTime FromNSDate(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return default(DateTime);
			}
			double num = ObjC.MessageSendDouble(handle, ObjC.TimeIntervalSinceReferenceDateSelector.Handle);
			if (num < -63113904000.0)
			{
				return DateTime.MinValue;
			}
			if (num > 252423993599.0)
			{
				return DateTime.MaxValue;
			}
			return new DateTime((long)(num * 10000000.0 + 6.3113904E+17), DateTimeKind.Utc);
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00002155 File Offset: 0x00000355
		public static string FromNSUrl(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return null;
			}
			return ObjC.FromNSString(ObjC.MessageSendIntPtr(handle, ObjC.AbsoluteStringSelector.Handle));
		}

		// Token: 0x06000053 RID: 83 RVA: 0x0000217B File Offset: 0x0000037B
		public static double FromNSNumber(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return 0.0;
			}
			return ObjC.MessageSendDouble(handle, ObjC.DoubleValueSelector.Handle);
		}

		// Token: 0x06000054 RID: 84 RVA: 0x000021A4 File Offset: 0x000003A4
		public static string[] FromNSArray(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return null;
			}
			uint num = ObjC.MessageSendUInt(handle, ObjC.CountSelector.Handle);
			string[] array = new string[num];
			for (uint num2 = 0U; num2 < num; num2 += 1U)
			{
				IntPtr handle2 = ObjC.MessageSendIntPtr(handle, ObjC.ObjectAtIndexSelector.Handle, num2);
				array[(int)num2] = ObjC.FromNSString(handle2);
			}
			return array;
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00002200 File Offset: 0x00000400
		public static string[] FromNSSet(IntPtr handle)
		{
			if (handle == IntPtr.Zero)
			{
				return null;
			}
			return ObjC.FromNSArray(ObjC.MessageSendIntPtr(handle, ObjC.AllObjectsSelector.Handle));
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00002228 File Offset: 0x00000428
		public static T[] FromNSArray<T>(IntPtr handle) where T : NSObject
		{
			if (handle == IntPtr.Zero)
			{
				return null;
			}
			uint num = ObjC.MessageSendUInt(handle, ObjC.CountSelector.Handle);
			T[] array = new T[num];
			for (uint num2 = 0U; num2 < num; num2 += 1U)
			{
				IntPtr handle2 = ObjC.MessageSendIntPtr(handle, ObjC.ObjectAtIndexSelector.Handle, num2);
				array[(int)num2] = Runtime.GetNSObject<T>(handle2);
			}
			return array;
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00002288 File Offset: 0x00000488
		public static void ReleaseNSArrayItems(IntPtr handle)
		{
			uint num = ObjC.MessageSendUInt(handle, ObjC.CountSelector.Handle);
			for (uint num2 = 0U; num2 < num; num2 += 1U)
			{
				ObjC.MessageSend(ObjC.MessageSendIntPtr(handle, ObjC.ObjectAtIndexSelector.Handle, num2), Selector.ReleaseHandle);
			}
		}

		// Token: 0x06000058 RID: 88 RVA: 0x000022CD File Offset: 0x000004CD
		public static T[] FromNSSet<T>(IntPtr handle) where T : NSObject
		{
			if (handle == IntPtr.Zero)
			{
				return null;
			}
			return ObjC.FromNSArray<T>(ObjC.MessageSendIntPtr(handle, ObjC.AllObjectsSelector.Handle));
		}

		// Token: 0x06000059 RID: 89 RVA: 0x000022F4 File Offset: 0x000004F4
		public static string GetStringConstant(IntPtr handle, string symbol)
		{
			IntPtr intPtr = ObjC.dlsym(handle, symbol);
			if (intPtr == IntPtr.Zero)
			{
				return null;
			}
			intPtr = Marshal.ReadIntPtr(intPtr);
			if (intPtr == IntPtr.Zero)
			{
				return null;
			}
			return ObjC.FromNSString(intPtr);
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00002334 File Offset: 0x00000534
		public unsafe static float GetFloatConstant(IntPtr handle, string symbol)
		{
			IntPtr intPtr = ObjC.dlsym(handle, symbol);
			if (intPtr == IntPtr.Zero)
			{
				return 0f;
			}
			float* ptr = (float*)((void*)intPtr);
			return *ptr;
		}

		// Token: 0x0600005B RID: 91 RVA: 0x00002368 File Offset: 0x00000568
		public static IntPtr ToNSArray(IntPtr[] items)
		{
			IntPtr intPtr = Marshal.AllocHGlobal((IntPtr)(items.Length * IntPtr.Size));
			for (int i = 0; i < items.Length; i++)
			{
				Marshal.WriteIntPtr(intPtr, i * IntPtr.Size, items[i]);
			}
			IntPtr result = ObjC.MessageSendIntPtr(ObjC.GetClass("NSArray"), ObjC.ArrayWithObjects_CountSelector.Handle, intPtr, items.Length);
			Marshal.FreeHGlobal(intPtr);
			return result;
		}

		// Token: 0x0600005C RID: 92 RVA: 0x000023CA File Offset: 0x000005CA
		public static IntPtr ToNSArray(NSObject[] items)
		{
			return ObjC.ToNSArray((from i in items
			select i.Handle).ToArray<IntPtr>());
		}

		// Token: 0x0600005D RID: 93 RVA: 0x000023FB File Offset: 0x000005FB
		public static IntPtr ToNSArray(string[] items)
		{
			return ObjC.ToNSArray((from s in items
			select ObjC.ToNSString(s)).ToArray<IntPtr>());
		}

		// Token: 0x0600005E RID: 94 RVA: 0x0000242C File Offset: 0x0000062C
		public static IntPtr ToNSSet(IntPtr[] items)
		{
			IntPtr arg = ObjC.ToNSArray(items);
			return ObjC.MessageSendIntPtr(ObjC.GetClass("NSSet"), ObjC.SetWithArraySelector.Handle, arg);
		}

		// Token: 0x0600005F RID: 95 RVA: 0x0000245C File Offset: 0x0000065C
		public static IntPtr ToNSSet(string[] items)
		{
			IntPtr[] array = new IntPtr[items.Length];
			for (int i = 0; i < items.Length; i++)
			{
				array[i] = ObjC.ToNSString(items[i]);
			}
			IntPtr arg = ObjC.ToNSArray(array);
			IntPtr result = ObjC.MessageSendIntPtr(ObjC.GetClass("NSSet"), ObjC.SetWithArraySelector.Handle, arg);
			for (int j = 0; j < array.Length; j++)
			{
				ObjC.MessageSend(array[j], Selector.ReleaseHandle);
			}
			return result;
		}

		// Token: 0x06000060 RID: 96 RVA: 0x000024D0 File Offset: 0x000006D0
		public unsafe static IntPtr ToNSString(string str)
		{
			IntPtr receiver = ObjC.MessageSendIntPtr(ObjC.GetClass("NSString"), Selector.AllocHandle);
			fixed (string text = str + ((IntPtr)(RuntimeHelpers.OffsetToStringData / 2)).ToString())
			{
				char* ptr = text;
				if (ptr != null)
				{
					ptr += RuntimeHelpers.OffsetToStringData / 2;
				}
				return ObjC.MessageSendIntPtr(receiver, ObjC.InitWithCharacters_lengthSelector.Handle, (IntPtr)((void*)ptr), str.Length);
			}
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00002535 File Offset: 0x00000735
		public static IntPtr ToNSUrl(string str)
		{
			return ObjC.MessageSendIntPtr(ObjC.GetClass("NSURL"), ObjC.URLWithStringSelector.Handle, str);
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00002551 File Offset: 0x00000751
		public static IntPtr ToNSDate(DateTime date)
		{
			return ObjC.MessageSendIntPtr(ObjC.GetClass("NSDate"), ObjC.DateWithTimeIntervalSinceReferenceDateSelector.Handle, (double)((date.Ticks - 631139040000000000L) / 10000000L));
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002585 File Offset: 0x00000785
		public static IntPtr ToNSNumber(double value)
		{
			return ObjC.MessageSendIntPtr(ObjC.MessageSendIntPtr(ObjC.GetClass("NSDecimalNumber"), Selector.AllocHandle), ObjC.InitWithDoubleSelector.Handle, value);
		}

		// Token: 0x06000064 RID: 100
		[DllImport("/System/Library/Frameworks/Foundation.framework/Foundation")]
		public static extern void NSLog(IntPtr format, IntPtr args);

		// Token: 0x06000065 RID: 101 RVA: 0x000025AC File Offset: 0x000007AC
		public static void NSLog(string format, params object[] args)
		{
			IntPtr format2 = ObjC.ToNSString("%@");
			IntPtr args2 = ObjC.ToNSString(string.Format(format, args));
			ObjC.NSLog(format2, args2);
		}

		// Token: 0x04000001 RID: 1
		private const string timeIntervalSinceReferenceDate = "timeIntervalSinceReferenceDate";

		// Token: 0x04000002 RID: 2
		internal static readonly Selector TimeIntervalSinceReferenceDateSelector = new Selector("timeIntervalSinceReferenceDate");

		// Token: 0x04000003 RID: 3
		private const string absoluteString = "absoluteString";

		// Token: 0x04000004 RID: 4
		internal static readonly Selector AbsoluteStringSelector = new Selector("absoluteString");

		// Token: 0x04000005 RID: 5
		private const string doubleValue = "doubleValue";

		// Token: 0x04000006 RID: 6
		internal static readonly Selector DoubleValueSelector = new Selector("doubleValue");

		// Token: 0x04000007 RID: 7
		private const string count = "count";

		// Token: 0x04000008 RID: 8
		internal static readonly Selector CountSelector = new Selector("count");

		// Token: 0x04000009 RID: 9
		private const string objectAtIndex = "objectAtIndex:";

		// Token: 0x0400000A RID: 10
		internal static readonly Selector ObjectAtIndexSelector = new Selector("objectAtIndex:");

		// Token: 0x0400000B RID: 11
		private const string allObjects = "allObjects";

		// Token: 0x0400000C RID: 12
		internal static readonly Selector AllObjectsSelector = new Selector("allObjects");

		// Token: 0x0400000D RID: 13
		private const string arrayWithObjects_count = "arrayWithObjects:count:";

		// Token: 0x0400000E RID: 14
		internal static readonly Selector ArrayWithObjects_CountSelector = new Selector("arrayWithObjects:count:");

		// Token: 0x0400000F RID: 15
		private const string setWithArray = "setWithArray";

		// Token: 0x04000010 RID: 16
		internal static readonly Selector SetWithArraySelector = new Selector("setWithArray");

		// Token: 0x04000011 RID: 17
		private const string initWithCharacters_length = "initWithCharacters:length:";

		// Token: 0x04000012 RID: 18
		internal static readonly Selector InitWithCharacters_lengthSelector = new Selector("initWithCharacters:length:");

		// Token: 0x04000013 RID: 19
		private const string URLWithString = "URLWithString:";

		// Token: 0x04000014 RID: 20
		internal static readonly Selector URLWithStringSelector = new Selector("URLWithString:");

		// Token: 0x04000015 RID: 21
		private const string dateWithTimeIntervalSinceReferenceDate = "dateWithTimeIntervalSinceReferenceDate:";

		// Token: 0x04000016 RID: 22
		internal static readonly Selector DateWithTimeIntervalSinceReferenceDateSelector = new Selector("dateWithTimeIntervalSinceReferenceDate:");

		// Token: 0x04000017 RID: 23
		private const string initWithDouble = "initWithDouble:";

		// Token: 0x04000018 RID: 24
		internal static readonly Selector InitWithDoubleSelector = new Selector("initWithDouble:");

		// Token: 0x020001A6 RID: 422
		public static class Libraries
		{
			// Token: 0x0400091C RID: 2332
			public static readonly IntPtr Foundation = ObjC.dlopen("/System/Library/Frameworks/Foundation.framework/Foundation", 0);

			// Token: 0x0400091D RID: 2333
			public static readonly IntPtr UIKit = ObjC.dlopen("/System/Library/Frameworks/UIKit.framework/UIKit", 0);
		}
	}
}
