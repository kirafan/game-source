﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200001B RID: 27
	public enum NSNumberFormatterStyle
	{
		// Token: 0x04000058 RID: 88
		None,
		// Token: 0x04000059 RID: 89
		Decimal,
		// Token: 0x0400005A RID: 90
		Currency,
		// Token: 0x0400005B RID: 91
		Percent,
		// Token: 0x0400005C RID: 92
		Scientific,
		// Token: 0x0400005D RID: 93
		SpellOut
	}
}
