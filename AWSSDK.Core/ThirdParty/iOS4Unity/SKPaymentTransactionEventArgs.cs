﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000022 RID: 34
	public class SKPaymentTransactionEventArgs : EventArgs
	{
		// Token: 0x04000073 RID: 115
		public SKPaymentTransaction[] Transactions;
	}
}
