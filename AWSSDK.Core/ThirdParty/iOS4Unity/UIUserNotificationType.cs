﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200003F RID: 63
	[CLSCompliant(false)]
	public enum UIUserNotificationType : uint
	{
		// Token: 0x040000C3 RID: 195
		None,
		// Token: 0x040000C4 RID: 196
		Badge,
		// Token: 0x040000C5 RID: 197
		Sound,
		// Token: 0x040000C6 RID: 198
		Alert = 4U
	}
}
