﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000016 RID: 22
	public class NSMutableDictionary : NSDictionary
	{
		// Token: 0x17000037 RID: 55
		// (get) Token: 0x0600012E RID: 302 RVA: 0x000042DE File Offset: 0x000024DE
		public override IntPtr ClassHandle
		{
			get
			{
				return NSMutableDictionary._classHandle;
			}
		}

		// Token: 0x0600012F RID: 303 RVA: 0x000042E5 File Offset: 0x000024E5
		internal NSMutableDictionary(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x06000130 RID: 304 RVA: 0x000042EE File Offset: 0x000024EE
		public NSMutableDictionary()
		{
			this.Handle = ObjC.MessageSendIntPtr(this.Handle, Selector.Init);
		}

		// Token: 0x06000131 RID: 305 RVA: 0x0000430C File Offset: 0x0000250C
		public new static NSMutableDictionary FromDictionary(NSDictionary dictionary)
		{
			return NSMutableDictionary.FromObjectsAndKeys(dictionary.Values, dictionary.Keys);
		}

		// Token: 0x06000132 RID: 306 RVA: 0x00004320 File Offset: 0x00002520
		public new static NSMutableDictionary FromObjectAndKey(NSObject obj, string key)
		{
			IntPtr intPtr = ObjC.ToNSString(key);
			NSMutableDictionary nsobject = Runtime.GetNSObject<NSMutableDictionary>(ObjC.MessageSendIntPtr(NSMutableDictionary._classHandle, Selector.GetHandle("dictionaryWithObject:forKey:"), obj.Handle, intPtr));
			ObjC.MessageSend(intPtr, Selector.ReleaseHandle);
			return nsobject;
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00004360 File Offset: 0x00002560
		public new static NSMutableDictionary FromObjectsAndKeys(NSObject[] objects, string[] keys)
		{
			IntPtr arg = ObjC.ToNSArray(objects);
			IntPtr intPtr = ObjC.ToNSArray(keys);
			NSMutableDictionary nsobject = Runtime.GetNSObject<NSMutableDictionary>(ObjC.MessageSendIntPtr(NSMutableDictionary._classHandle, Selector.GetHandle("dictionaryWithObjects:forKeys:"), arg, intPtr));
			ObjC.ReleaseNSArrayItems(intPtr);
			return nsobject;
		}

		// Token: 0x06000134 RID: 308 RVA: 0x0000439C File Offset: 0x0000259C
		public void SetObjectForKey(NSObject obj, string key)
		{
			IntPtr intPtr = ObjC.ToNSString(key);
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("setObject:forKey:"), obj.Handle, intPtr);
			ObjC.MessageSend(intPtr, Selector.ReleaseHandle);
		}

		// Token: 0x06000135 RID: 309 RVA: 0x000043D8 File Offset: 0x000025D8
		public void RemoveObjectForKey(string key)
		{
			IntPtr intPtr = ObjC.ToNSString(key);
			ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("removeObjectForKey:"), intPtr);
			ObjC.MessageSend(intPtr, Selector.ReleaseHandle);
		}

		// Token: 0x06000136 RID: 310 RVA: 0x0000440E File Offset: 0x0000260E
		public new static NSMutableDictionary FromFile(string path)
		{
			return Runtime.GetNSObject<NSMutableDictionary>(ObjC.MessageSendIntPtr(NSMutableDictionary._classHandle, Selector.GetHandle("dictionaryWithContentsOfFile:"), path));
		}

		// Token: 0x06000137 RID: 311 RVA: 0x0000442A File Offset: 0x0000262A
		public void Clear()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("removeAllObjects"));
		}

		// Token: 0x17000038 RID: 56
		public override NSObject this[string key]
		{
			get
			{
				return base.ObjectForKey(key);
			}
			set
			{
				this.SetObjectForKey(value, key);
			}
		}

		// Token: 0x0400004E RID: 78
		private static readonly IntPtr _classHandle = ObjC.GetClass("NSMutableDictionary");
	}
}
