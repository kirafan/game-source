﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x0200003B RID: 59
	[CLSCompliant(false)]
	public enum UIPopoverArrowDirection : uint
	{
		// Token: 0x040000B9 RID: 185
		Up = 1U,
		// Token: 0x040000BA RID: 186
		Down,
		// Token: 0x040000BB RID: 187
		Left = 4U,
		// Token: 0x040000BC RID: 188
		Right = 8U,
		// Token: 0x040000BD RID: 189
		Any = 15U,
		// Token: 0x040000BE RID: 190
		Unknown = 4294967295U
	}
}
