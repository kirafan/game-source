﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000037 RID: 55
	public enum UIUserInterfaceIdiom
	{
		// Token: 0x040000B1 RID: 177
		Unspecified = -1,
		// Token: 0x040000B2 RID: 178
		Phone,
		// Token: 0x040000B3 RID: 179
		Pad
	}
}
