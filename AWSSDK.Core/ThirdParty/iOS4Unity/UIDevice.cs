﻿using System;
using System.Globalization;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000034 RID: 52
	public sealed class UIDevice : NSObject
	{
		// Token: 0x0600027B RID: 635 RVA: 0x000061B0 File Offset: 0x000043B0
		static UIDevice()
		{
			UIDevice._classHandle = ObjC.GetClass("UIDevice");
		}

		// Token: 0x170000B3 RID: 179
		// (get) Token: 0x0600027C RID: 636 RVA: 0x000061CD File Offset: 0x000043CD
		public override IntPtr ClassHandle
		{
			get
			{
				return UIDevice._classHandle;
			}
		}

		// Token: 0x170000B4 RID: 180
		// (get) Token: 0x0600027D RID: 637 RVA: 0x000061D4 File Offset: 0x000043D4
		public static string BatteryLevelDidChangeNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIDeviceBatteryLevelDidChangeNotification");
			}
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x0600027E RID: 638 RVA: 0x000061E5 File Offset: 0x000043E5
		public static string BatteryStateDidChangeNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIDeviceBatteryStateDidChangeNotification");
			}
		}

		// Token: 0x170000B6 RID: 182
		// (get) Token: 0x0600027F RID: 639 RVA: 0x000061F6 File Offset: 0x000043F6
		public static string OrientationDidChangeNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIDeviceOrientationDidChangeNotification");
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x06000280 RID: 640 RVA: 0x00006207 File Offset: 0x00004407
		public static string ProximityStateDidChangeNotification
		{
			get
			{
				return ObjC.GetStringConstant(ObjC.Libraries.UIKit, "UIDeviceProximityStateDidChangeNotification");
			}
		}

		// Token: 0x06000281 RID: 641 RVA: 0x00003A32 File Offset: 0x00001C32
		internal UIDevice(IntPtr handle) : base(handle)
		{
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x06000282 RID: 642 RVA: 0x00006218 File Offset: 0x00004418
		public static UIDevice CurrentDevice
		{
			get
			{
				return Runtime.GetNSObject<UIDevice>(ObjC.MessageSendIntPtr(UIDevice._classHandle, Selector.GetHandle("currentDevice")));
			}
		}

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x06000283 RID: 643 RVA: 0x00006233 File Offset: 0x00004433
		public float BatteryLevel
		{
			get
			{
				return ObjC.MessageSendFloat(this.Handle, Selector.GetHandle("batteryLevel"));
			}
		}

		// Token: 0x170000BA RID: 186
		// (get) Token: 0x06000284 RID: 644 RVA: 0x0000624A File Offset: 0x0000444A
		// (set) Token: 0x06000285 RID: 645 RVA: 0x00006261 File Offset: 0x00004461
		public bool BatteryMonitoringEnabled
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isBatteryMonitoringEnabled"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setBatteryMonitoringEnabled:"), value);
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x06000286 RID: 646 RVA: 0x00006279 File Offset: 0x00004479
		public UIDeviceBatteryState BatteryState
		{
			get
			{
				return (UIDeviceBatteryState)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("batteryState"));
			}
		}

		// Token: 0x06000287 RID: 647 RVA: 0x00006290 File Offset: 0x00004490
		public bool CheckSystemVersion(int major, int minor)
		{
			if (UIDevice._majorVersion == -1)
			{
				string[] array = this.SystemVersion.Split(new char[]
				{
					'.'
				});
				if (array.Length < 1 || !int.TryParse(array[0], NumberStyles.Integer, CultureInfo.InvariantCulture, out UIDevice._majorVersion))
				{
					UIDevice._majorVersion = 2;
				}
				if (array.Length < 2 || !int.TryParse(array[1], NumberStyles.Integer, CultureInfo.InvariantCulture, out UIDevice._minorVersion))
				{
					UIDevice._minorVersion = 0;
				}
			}
			return UIDevice._majorVersion > major || (UIDevice._majorVersion == major && UIDevice._minorVersion >= minor);
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x06000288 RID: 648 RVA: 0x0000631D File Offset: 0x0000451D
		public bool GeneratesDeviceOrientationNotifications
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isGeneratingDeviceOrientationNotifications"));
			}
		}

		// Token: 0x170000BD RID: 189
		// (get) Token: 0x06000289 RID: 649 RVA: 0x00006334 File Offset: 0x00004534
		public string LocalizedModel
		{
			get
			{
				return ObjC.MessageSendString(this.Handle, Selector.GetHandle("localizedModel"));
			}
		}

		// Token: 0x170000BE RID: 190
		// (get) Token: 0x0600028A RID: 650 RVA: 0x0000634B File Offset: 0x0000454B
		public string Model
		{
			get
			{
				return ObjC.FromNSString(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("model")));
			}
		}

		// Token: 0x170000BF RID: 191
		// (get) Token: 0x0600028B RID: 651 RVA: 0x00006367 File Offset: 0x00004567
		public string Name
		{
			get
			{
				return ObjC.FromNSString(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("name")));
			}
		}

		// Token: 0x170000C0 RID: 192
		// (get) Token: 0x0600028C RID: 652 RVA: 0x00006383 File Offset: 0x00004583
		public UIDeviceOrientation Orientation
		{
			get
			{
				return (UIDeviceOrientation)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("orientation"));
			}
		}

		// Token: 0x0600028D RID: 653 RVA: 0x0000639A File Offset: 0x0000459A
		public void PlayInputClick()
		{
			ObjC.MessageSend(this.Handle, Selector.GetHandle("playInputClick"));
		}

		// Token: 0x170000C1 RID: 193
		// (get) Token: 0x0600028E RID: 654 RVA: 0x000063B1 File Offset: 0x000045B1
		// (set) Token: 0x0600028F RID: 655 RVA: 0x000063C8 File Offset: 0x000045C8
		public bool ProximityMonitoringEnabled
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("isProximityMonitoringEnabled"));
			}
			set
			{
				ObjC.MessageSend(this.Handle, Selector.GetHandle("setProximityMonitoringEnabled:"), value);
			}
		}

		// Token: 0x170000C2 RID: 194
		// (get) Token: 0x06000290 RID: 656 RVA: 0x000063E0 File Offset: 0x000045E0
		public bool ProximityState
		{
			get
			{
				return ObjC.MessageSendBool(this.Handle, Selector.GetHandle("proximityState"));
			}
		}

		// Token: 0x170000C3 RID: 195
		// (get) Token: 0x06000291 RID: 657 RVA: 0x000063F7 File Offset: 0x000045F7
		public string SystemName
		{
			get
			{
				return ObjC.FromNSString(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("systemName")));
			}
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x06000292 RID: 658 RVA: 0x00006413 File Offset: 0x00004613
		public string SystemVersion
		{
			get
			{
				return ObjC.FromNSString(ObjC.MessageSendIntPtr(this.Handle, Selector.GetHandle("systemVersion")));
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x06000293 RID: 659 RVA: 0x0000642F File Offset: 0x0000462F
		public UIUserInterfaceIdiom UserInterfaceIdiom
		{
			get
			{
				return (UIUserInterfaceIdiom)ObjC.MessageSendInt(this.Handle, Selector.GetHandle("userInterfaceIdiom"));
			}
		}

		// Token: 0x040000A0 RID: 160
		private static readonly IntPtr _classHandle;

		// Token: 0x040000A1 RID: 161
		private static int _majorVersion = -1;

		// Token: 0x040000A2 RID: 162
		private static int _minorVersion = -1;
	}
}
