﻿using System;

namespace ThirdParty.iOS4Unity
{
	// Token: 0x02000007 RID: 7
	[Serializable]
	public struct CGRect
	{
		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000096 RID: 150 RVA: 0x000033DC File Offset: 0x000015DC
		public float Bottom
		{
			get
			{
				return this.Y + this.Height;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000097 RID: 151 RVA: 0x000033EB File Offset: 0x000015EB
		public bool IsEmpty
		{
			get
			{
				return this.Width <= 0f || this.Height <= 0f;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000098 RID: 152 RVA: 0x0000340C File Offset: 0x0000160C
		// (set) Token: 0x06000099 RID: 153 RVA: 0x0000341F File Offset: 0x0000161F
		public CGPoint Location
		{
			get
			{
				return new CGPoint(this.X, this.Y);
			}
			set
			{
				this.X = value.X;
				this.Y = value.Y;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600009A RID: 154 RVA: 0x00003439 File Offset: 0x00001639
		public float Right
		{
			get
			{
				return this.X + this.Width;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600009B RID: 155 RVA: 0x00003448 File Offset: 0x00001648
		// (set) Token: 0x0600009C RID: 156 RVA: 0x0000345B File Offset: 0x0000165B
		public CGSize Size
		{
			get
			{
				return new CGSize(this.Width, this.Height);
			}
			set
			{
				this.Width = value.Width;
				this.Height = value.Height;
			}
		}

		// Token: 0x0600009D RID: 157 RVA: 0x00003475 File Offset: 0x00001675
		public CGRect(CGPoint location, CGSize size)
		{
			this.X = location.X;
			this.Y = location.Y;
			this.Width = size.Width;
			this.Height = size.Height;
		}

		// Token: 0x0600009E RID: 158 RVA: 0x000034A7 File Offset: 0x000016A7
		public CGRect(float x, float y, float width, float height)
		{
			this.X = x;
			this.Y = y;
			this.Width = width;
			this.Height = height;
		}

		// Token: 0x0600009F RID: 159 RVA: 0x000034C6 File Offset: 0x000016C6
		public static CGRect FromLTRB(float left, float top, float right, float bottom)
		{
			return new CGRect(left, top, right - left, bottom - top);
		}

		// Token: 0x060000A0 RID: 160 RVA: 0x000034D8 File Offset: 0x000016D8
		public static CGRect Inflate(CGRect rect, float x, float y)
		{
			CGRect result = new CGRect(rect.X, rect.Y, rect.Width, rect.Height);
			result.Inflate(x, y);
			return result;
		}

		// Token: 0x060000A1 RID: 161 RVA: 0x00003510 File Offset: 0x00001710
		public static CGRect Intersect(CGRect a, CGRect b)
		{
			if (!a.IntersectsWithInclusive(b))
			{
				return CGRect.Empty;
			}
			return CGRect.FromLTRB(Math.Max(a.X, b.X), Math.Max(a.Y, b.Y), Math.Min(a.Right, b.Right), Math.Min(a.Bottom, b.Bottom));
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x0000357C File Offset: 0x0000177C
		public static CGRect Union(CGRect a, CGRect b)
		{
			return CGRect.FromLTRB(Math.Min(a.X, b.X), Math.Min(a.Y, b.Y), Math.Max(a.Right, b.Right), Math.Max(a.Bottom, b.Bottom));
		}

		// Token: 0x060000A3 RID: 163 RVA: 0x000035D6 File Offset: 0x000017D6
		public bool Contains(float x, float y)
		{
			return x >= this.X && x < this.Right && y >= this.Y && y < this.Bottom;
		}

		// Token: 0x060000A4 RID: 164 RVA: 0x00003600 File Offset: 0x00001800
		public bool Contains(CGRect rect)
		{
			return this.X <= rect.X && this.Right >= rect.Right && this.Y <= rect.Y && this.Bottom >= rect.Bottom;
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x0000364C File Offset: 0x0000184C
		public bool Contains(CGPoint pt)
		{
			return this.Contains(pt.X, pt.Y);
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x00003660 File Offset: 0x00001860
		public override bool Equals(object obj)
		{
			return obj is CGRect && this == (CGRect)obj;
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x0000367D File Offset: 0x0000187D
		public override int GetHashCode()
		{
			return (int)(this.X + this.Y + this.Width + this.Height);
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x0000369C File Offset: 0x0000189C
		public void Inflate(CGSize size)
		{
			this.X -= size.Width;
			this.Y -= size.Height;
			this.Width += size.Width * 2f;
			this.Height += size.Height * 2f;
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x00003701 File Offset: 0x00001901
		public void Inflate(float x, float y)
		{
			this.Inflate(new CGSize(x, y));
		}

		// Token: 0x060000AA RID: 170 RVA: 0x00003710 File Offset: 0x00001910
		public void Intersect(CGRect rect)
		{
			this = CGRect.Intersect(this, rect);
		}

		// Token: 0x060000AB RID: 171 RVA: 0x00003724 File Offset: 0x00001924
		public bool IntersectsWith(CGRect rect)
		{
			return this.X < rect.Right && this.Right > rect.X && this.Y < rect.Bottom && this.Bottom > rect.Y;
		}

		// Token: 0x060000AC RID: 172 RVA: 0x00003764 File Offset: 0x00001964
		private bool IntersectsWithInclusive(CGRect r)
		{
			return this.X <= r.Right && this.Right >= r.X && this.Y <= r.Bottom && this.Bottom >= r.Y;
		}

		// Token: 0x060000AD RID: 173 RVA: 0x000037B0 File Offset: 0x000019B0
		public void Offset(float x, float y)
		{
			this.X += x;
			this.Y += y;
		}

		// Token: 0x060000AE RID: 174 RVA: 0x000037CE File Offset: 0x000019CE
		public void Offset(CGPoint pos)
		{
			this.Offset(pos.X, pos.Y);
		}

		// Token: 0x060000AF RID: 175 RVA: 0x000037E4 File Offset: 0x000019E4
		public override string ToString()
		{
			return string.Format("{{X={0},Y={1},Width={2},Height={3}}}", new object[]
			{
				this.X,
				this.Y,
				this.Width,
				this.Height
			});
		}

		// Token: 0x060000B0 RID: 176 RVA: 0x00003839 File Offset: 0x00001A39
		public static bool operator ==(CGRect left, CGRect right)
		{
			return left.X == right.X && left.Y == right.Y && left.Width == right.Width && left.Height == right.Height;
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x00003875 File Offset: 0x00001A75
		public static bool operator !=(CGRect left, CGRect right)
		{
			return left.X != right.X || left.Y != right.Y || left.Width != right.Width || left.Height != right.Height;
		}

		// Token: 0x0400003A RID: 58
		public static readonly CGRect Empty;

		// Token: 0x0400003B RID: 59
		public float X;

		// Token: 0x0400003C RID: 60
		public float Height;

		// Token: 0x0400003D RID: 61
		public float Width;

		// Token: 0x0400003E RID: 62
		public float Y;
	}
}
