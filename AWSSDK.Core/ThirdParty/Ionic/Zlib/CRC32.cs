﻿using System;
using System.IO;

namespace ThirdParty.Ionic.Zlib
{
	// Token: 0x02000062 RID: 98
	internal class CRC32
	{
		// Token: 0x1700014A RID: 330
		// (get) Token: 0x06000486 RID: 1158 RVA: 0x0000C63B File Offset: 0x0000A83B
		public long TotalBytesRead
		{
			get
			{
				return this._TotalBytesRead;
			}
		}

		// Token: 0x1700014B RID: 331
		// (get) Token: 0x06000487 RID: 1159 RVA: 0x0000C643 File Offset: 0x0000A843
		public int Crc32Result
		{
			get
			{
				return (int)(~(int)this._RunningCrc32Result);
			}
		}

		// Token: 0x06000488 RID: 1160 RVA: 0x0000C64C File Offset: 0x0000A84C
		public int GetCrc32(Stream input)
		{
			return this.GetCrc32AndCopy(input, null);
		}

		// Token: 0x06000489 RID: 1161 RVA: 0x0000C658 File Offset: 0x0000A858
		public int GetCrc32AndCopy(Stream input, Stream output)
		{
			byte[] array = new byte[8192];
			int count = 8192;
			this._TotalBytesRead = 0L;
			int i = input.Read(array, 0, count);
			if (output != null)
			{
				output.Write(array, 0, i);
			}
			this._TotalBytesRead += (long)i;
			while (i > 0)
			{
				this.SlurpBlock(array, 0, i);
				i = input.Read(array, 0, count);
				if (output != null)
				{
					output.Write(array, 0, i);
				}
				this._TotalBytesRead += (long)i;
			}
			return (int)(~(int)this._RunningCrc32Result);
		}

		// Token: 0x0600048A RID: 1162 RVA: 0x0000C6DE File Offset: 0x0000A8DE
		public int ComputeCrc32(int W, byte B)
		{
			return this._InternalComputeCrc32((uint)W, B);
		}

		// Token: 0x0600048B RID: 1163 RVA: 0x0000C6E8 File Offset: 0x0000A8E8
		internal int _InternalComputeCrc32(uint W, byte B)
		{
			return (int)(CRC32.crc32Table[(int)((W ^ (uint)B) & 255U)] ^ W >> 8);
		}

		// Token: 0x0600048C RID: 1164 RVA: 0x0000C700 File Offset: 0x0000A900
		public void SlurpBlock(byte[] block, int offset, int count)
		{
			for (int i = 0; i < count; i++)
			{
				int num = offset + i;
				this._RunningCrc32Result = (this._RunningCrc32Result >> 8 ^ CRC32.crc32Table[(int)((uint)block[num] ^ (this._RunningCrc32Result & 255U))]);
			}
			this._TotalBytesRead += (long)count;
		}

		// Token: 0x0600048D RID: 1165 RVA: 0x0000C754 File Offset: 0x0000A954
		static CRC32()
		{
			uint num = 3988292384U;
			CRC32.crc32Table = new uint[256];
			for (uint num2 = 0U; num2 < 256U; num2 += 1U)
			{
				uint num3 = num2;
				for (uint num4 = 8U; num4 > 0U; num4 -= 1U)
				{
					if ((num3 & 1U) == 1U)
					{
						num3 = (num3 >> 1 ^ num);
					}
					else
					{
						num3 >>= 1;
					}
				}
				CRC32.crc32Table[(int)num2] = num3;
			}
		}

		// Token: 0x0400015E RID: 350
		private long _TotalBytesRead;

		// Token: 0x0400015F RID: 351
		private static uint[] crc32Table;

		// Token: 0x04000160 RID: 352
		private const int BUFFER_SIZE = 8192;

		// Token: 0x04000161 RID: 353
		private uint _RunningCrc32Result = uint.MaxValue;
	}
}
