﻿using System;
using System.IO;

namespace ThirdParty.Ionic.Zlib
{
	// Token: 0x02000063 RID: 99
	public class CrcCalculatorStream : Stream
	{
		// Token: 0x1700014C RID: 332
		// (get) Token: 0x0600048F RID: 1167 RVA: 0x0000C7BD File Offset: 0x0000A9BD
		public long TotalBytesSlurped
		{
			get
			{
				return this._Crc32.TotalBytesRead;
			}
		}

		// Token: 0x06000490 RID: 1168 RVA: 0x0000C7CA File Offset: 0x0000A9CA
		public CrcCalculatorStream(Stream stream)
		{
			this._InnerStream = stream;
			this._Crc32 = new CRC32();
		}

		// Token: 0x06000491 RID: 1169 RVA: 0x0000C7E4 File Offset: 0x0000A9E4
		public CrcCalculatorStream(Stream stream, long length)
		{
			this._InnerStream = stream;
			this._Crc32 = new CRC32();
			this._length = length;
		}

		// Token: 0x1700014D RID: 333
		// (get) Token: 0x06000492 RID: 1170 RVA: 0x0000C805 File Offset: 0x0000AA05
		public int Crc32
		{
			get
			{
				return this._Crc32.Crc32Result;
			}
		}

		// Token: 0x06000493 RID: 1171 RVA: 0x0000C814 File Offset: 0x0000AA14
		public override int Read(byte[] buffer, int offset, int count)
		{
			int count2 = count;
			if (this._length != 0L)
			{
				if (this._Crc32.TotalBytesRead >= this._length)
				{
					return 0;
				}
				long num = this._length - this._Crc32.TotalBytesRead;
				if (num < (long)count)
				{
					count2 = (int)num;
				}
			}
			int num2 = this._InnerStream.Read(buffer, offset, count2);
			if (num2 > 0)
			{
				this._Crc32.SlurpBlock(buffer, offset, num2);
			}
			return num2;
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x0000C87D File Offset: 0x0000AA7D
		public override void Write(byte[] buffer, int offset, int count)
		{
			if (count > 0)
			{
				this._Crc32.SlurpBlock(buffer, offset, count);
			}
			this._InnerStream.Write(buffer, offset, count);
		}

		// Token: 0x1700014E RID: 334
		// (get) Token: 0x06000495 RID: 1173 RVA: 0x0000C89F File Offset: 0x0000AA9F
		public override bool CanRead
		{
			get
			{
				return this._InnerStream.CanRead;
			}
		}

		// Token: 0x1700014F RID: 335
		// (get) Token: 0x06000496 RID: 1174 RVA: 0x0000C8AC File Offset: 0x0000AAAC
		public override bool CanSeek
		{
			get
			{
				return this._InnerStream.CanSeek;
			}
		}

		// Token: 0x17000150 RID: 336
		// (get) Token: 0x06000497 RID: 1175 RVA: 0x0000C8B9 File Offset: 0x0000AAB9
		public override bool CanWrite
		{
			get
			{
				return this._InnerStream.CanWrite;
			}
		}

		// Token: 0x06000498 RID: 1176 RVA: 0x0000C8C6 File Offset: 0x0000AAC6
		public override void Flush()
		{
			this._InnerStream.Flush();
		}

		// Token: 0x17000151 RID: 337
		// (get) Token: 0x06000499 RID: 1177 RVA: 0x0000C8D3 File Offset: 0x0000AAD3
		public override long Length
		{
			get
			{
				if (this._length == 0L)
				{
					throw new NotImplementedException();
				}
				return this._length;
			}
		}

		// Token: 0x17000152 RID: 338
		// (get) Token: 0x0600049A RID: 1178 RVA: 0x0000C7BD File Offset: 0x0000A9BD
		// (set) Token: 0x0600049B RID: 1179 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		public override long Position
		{
			get
			{
				return this._Crc32.TotalBytesRead;
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x0600049C RID: 1180 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600049D RID: 1181 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		public override void SetLength(long value)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000162 RID: 354
		private Stream _InnerStream;

		// Token: 0x04000163 RID: 355
		private CRC32 _Crc32;

		// Token: 0x04000164 RID: 356
		private long _length;
	}
}
