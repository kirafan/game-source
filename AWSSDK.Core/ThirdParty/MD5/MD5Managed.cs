﻿using System;
using System.Security.Cryptography;

namespace ThirdParty.MD5
{
	// Token: 0x0200004B RID: 75
	public class MD5Managed : HashAlgorithm
	{
		// Token: 0x06000354 RID: 852 RVA: 0x000079FC File Offset: 0x00005BFC
		public MD5Managed()
		{
			this.HashSizeValue = 128;
			this.Initialize();
		}

		// Token: 0x06000355 RID: 853 RVA: 0x00007A18 File Offset: 0x00005C18
		public override void Initialize()
		{
			this._data = new byte[64];
			this._dataSize = 0;
			this._totalLength = 0L;
			this._abcd = default(ABCDStruct);
			this._abcd.A = 1732584193U;
			this._abcd.B = 4023233417U;
			this._abcd.C = 2562383102U;
			this._abcd.D = 271733878U;
		}

		// Token: 0x06000356 RID: 854 RVA: 0x00007A90 File Offset: 0x00005C90
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			int i = this._dataSize + cbSize;
			if (i >= 64)
			{
				Array.Copy(array, ibStart, this._data, this._dataSize, 64 - this._dataSize);
				MD5Core.GetHashBlock(this._data, ref this._abcd, 0);
				int num = ibStart + (64 - this._dataSize);
				i -= 64;
				while (i >= 64)
				{
					Array.Copy(array, num, this._data, 0, 64);
					MD5Core.GetHashBlock(array, ref this._abcd, num);
					i -= 64;
					num += 64;
				}
				this._dataSize = i;
				Array.Copy(array, num, this._data, 0, i);
			}
			else
			{
				Array.Copy(array, ibStart, this._data, this._dataSize, cbSize);
				this._dataSize = i;
			}
			this._totalLength += (long)cbSize;
		}

		// Token: 0x06000357 RID: 855 RVA: 0x00007B5F File Offset: 0x00005D5F
		protected override byte[] HashFinal()
		{
			this.HashValue = MD5Core.GetHashFinalBlock(this._data, 0, this._dataSize, this._abcd, this._totalLength * 8L);
			return this.HashValue;
		}

		// Token: 0x040000D3 RID: 211
		private byte[] _data;

		// Token: 0x040000D4 RID: 212
		private ABCDStruct _abcd;

		// Token: 0x040000D5 RID: 213
		private long _totalLength;

		// Token: 0x040000D6 RID: 214
		private int _dataSize;
	}
}
