﻿using System;

namespace ThirdParty.MD5
{
	// Token: 0x02000049 RID: 73
	internal struct ABCDStruct
	{
		// Token: 0x040000CF RID: 207
		public uint A;

		// Token: 0x040000D0 RID: 208
		public uint B;

		// Token: 0x040000D1 RID: 209
		public uint C;

		// Token: 0x040000D2 RID: 210
		public uint D;
	}
}
