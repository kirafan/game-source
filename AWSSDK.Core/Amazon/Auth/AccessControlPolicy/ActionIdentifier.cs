﻿using System;

namespace Amazon.Auth.AccessControlPolicy
{
	// Token: 0x02000172 RID: 370
	public class ActionIdentifier
	{
		// Token: 0x06000DB4 RID: 3508 RVA: 0x00020FC0 File Offset: 0x0001F1C0
		public ActionIdentifier(string actionName)
		{
			this.actionName = actionName;
		}

		// Token: 0x17000400 RID: 1024
		// (get) Token: 0x06000DB5 RID: 3509 RVA: 0x00020FCF File Offset: 0x0001F1CF
		// (set) Token: 0x06000DB6 RID: 3510 RVA: 0x00020FD7 File Offset: 0x0001F1D7
		public string ActionName
		{
			get
			{
				return this.actionName;
			}
			set
			{
				this.actionName = value;
			}
		}

		// Token: 0x0400050E RID: 1294
		private string actionName;
	}
}
