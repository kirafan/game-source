﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200018E RID: 398
	public static class ElasticMapReduceActionIdentifiers
	{
		// Token: 0x040006EF RID: 1775
		public static readonly ActionIdentifier AllElasticMapReduceActions = new ActionIdentifier("elasticmapreduce:*");

		// Token: 0x040006F0 RID: 1776
		public static readonly ActionIdentifier AddInstanceGroups = new ActionIdentifier("elasticmapreduce:AddInstanceGroups");

		// Token: 0x040006F1 RID: 1777
		public static readonly ActionIdentifier AddTags = new ActionIdentifier("elasticmapreduce:AddTags");

		// Token: 0x040006F2 RID: 1778
		public static readonly ActionIdentifier AddJobFlowSteps = new ActionIdentifier("elasticmapreduce:AddJobFlowSteps");

		// Token: 0x040006F3 RID: 1779
		public static readonly ActionIdentifier DescribeCluster = new ActionIdentifier("elasticmapreduce:DescribeCluster");

		// Token: 0x040006F4 RID: 1780
		public static readonly ActionIdentifier DescribeJobFlows = new ActionIdentifier("elasticmapreduce:DescribeJobFlows");

		// Token: 0x040006F5 RID: 1781
		public static readonly ActionIdentifier DescribeStep = new ActionIdentifier("elasticmapreduce:DescribeStep");

		// Token: 0x040006F6 RID: 1782
		public static readonly ActionIdentifier ListBootstrapActions = new ActionIdentifier("elasticmapreduce:ListBootstrapActions");

		// Token: 0x040006F7 RID: 1783
		public static readonly ActionIdentifier ListClusters = new ActionIdentifier("elasticmapreduce:ListClusters");

		// Token: 0x040006F8 RID: 1784
		public static readonly ActionIdentifier ListInstanceGroups = new ActionIdentifier("elasticmapreduce:ListInstanceGroups");

		// Token: 0x040006F9 RID: 1785
		public static readonly ActionIdentifier ListInstances = new ActionIdentifier("elasticmapreduce:ListInstances");

		// Token: 0x040006FA RID: 1786
		public static readonly ActionIdentifier ListSteps = new ActionIdentifier("elasticmapreduce:ListSteps");

		// Token: 0x040006FB RID: 1787
		public static readonly ActionIdentifier ModifyInstanceGroups = new ActionIdentifier("elasticmapreduce:ModifyInstanceGroups");

		// Token: 0x040006FC RID: 1788
		public static readonly ActionIdentifier RemoveTags = new ActionIdentifier("elasticmapreduce:RemoveTags");

		// Token: 0x040006FD RID: 1789
		public static readonly ActionIdentifier RunJobFlow = new ActionIdentifier("elasticmapreduce:RunJobFlow");

		// Token: 0x040006FE RID: 1790
		public static readonly ActionIdentifier SetTerminationProtection = new ActionIdentifier("elasticmapreduce:SetTerminationProtection");

		// Token: 0x040006FF RID: 1791
		public static readonly ActionIdentifier TerminateJobFlows = new ActionIdentifier("elasticmapreduce:TerminateJobFlows");
	}
}
