﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000199 RID: 409
	public static class RedshiftActionIdentifiers
	{
		// Token: 0x040007FC RID: 2044
		public static readonly ActionIdentifier AllRedshiftActions = new ActionIdentifier("redshift:*");

		// Token: 0x040007FD RID: 2045
		public static readonly ActionIdentifier AuthorizeClusterSecurityGroupIngress = new ActionIdentifier("redshift:AuthorizeClusterSecurityGroupIngress");

		// Token: 0x040007FE RID: 2046
		public static readonly ActionIdentifier AuthorizeSnapshotAccess = new ActionIdentifier("redshift:AuthorizeSnapshotAccess");

		// Token: 0x040007FF RID: 2047
		public static readonly ActionIdentifier CopyClusterSnapshot = new ActionIdentifier("redshift:CopyClusterSnapshot");

		// Token: 0x04000800 RID: 2048
		public static readonly ActionIdentifier CreateCluster = new ActionIdentifier("redshift:CreateCluster");

		// Token: 0x04000801 RID: 2049
		public static readonly ActionIdentifier CreateClusterParameterGroup = new ActionIdentifier("redshift:CreateClusterParameterGroup");

		// Token: 0x04000802 RID: 2050
		public static readonly ActionIdentifier CreateClusterSecurityGroup = new ActionIdentifier("redshift:CreateClusterSecurityGroup");

		// Token: 0x04000803 RID: 2051
		public static readonly ActionIdentifier CreateClusterSnapshot = new ActionIdentifier("redshift:CreateClusterSnapshot");

		// Token: 0x04000804 RID: 2052
		public static readonly ActionIdentifier CreateClusterSubnetGroup = new ActionIdentifier("redshift:CreateClusterSubnetGroup");

		// Token: 0x04000805 RID: 2053
		public static readonly ActionIdentifier CreateEventSubscription = new ActionIdentifier("redshift:CreateEventSubscription");

		// Token: 0x04000806 RID: 2054
		public static readonly ActionIdentifier CreateHsmClientCertificate = new ActionIdentifier("redshift:CreateHsmClientCertificate");

		// Token: 0x04000807 RID: 2055
		public static readonly ActionIdentifier CreateHsmConfiguration = new ActionIdentifier("redshift:CreateHsmConfiguration");

		// Token: 0x04000808 RID: 2056
		public static readonly ActionIdentifier DeleteCluster = new ActionIdentifier("redshift:DeleteCluster");

		// Token: 0x04000809 RID: 2057
		public static readonly ActionIdentifier DeleteClusterParameterGroup = new ActionIdentifier("redshift:DeleteClusterParameterGroup");

		// Token: 0x0400080A RID: 2058
		public static readonly ActionIdentifier DeleteClusterSecurityGroup = new ActionIdentifier("redshift:DeleteClusterSecurityGroup");

		// Token: 0x0400080B RID: 2059
		public static readonly ActionIdentifier DeleteClusterSnapshot = new ActionIdentifier("redshift:DeleteClusterSnapshot");

		// Token: 0x0400080C RID: 2060
		public static readonly ActionIdentifier DeleteClusterSubnetGroup = new ActionIdentifier("redshift:DeleteClusterSubnetGroup");

		// Token: 0x0400080D RID: 2061
		public static readonly ActionIdentifier DeleteEventSubscription = new ActionIdentifier("redshift:DeleteEventSubscription");

		// Token: 0x0400080E RID: 2062
		public static readonly ActionIdentifier DeleteHsmClientCertificate = new ActionIdentifier("redshift:DeleteHsmClientCertificate");

		// Token: 0x0400080F RID: 2063
		public static readonly ActionIdentifier DeleteHsmConfiguration = new ActionIdentifier("redshift:DeleteHsmConfiguration");

		// Token: 0x04000810 RID: 2064
		public static readonly ActionIdentifier DescribeClusterParameterGroups = new ActionIdentifier("redshift:DescribeClusterParameterGroups");

		// Token: 0x04000811 RID: 2065
		public static readonly ActionIdentifier DescribeClusterParameters = new ActionIdentifier("redshift:DescribeClusterParameters");

		// Token: 0x04000812 RID: 2066
		public static readonly ActionIdentifier DescribeClusterSecurityGroups = new ActionIdentifier("redshift:DescribeClusterSecurityGroups");

		// Token: 0x04000813 RID: 2067
		public static readonly ActionIdentifier DescribeClusterSnapshots = new ActionIdentifier("redshift:DescribeClusterSnapshots");

		// Token: 0x04000814 RID: 2068
		public static readonly ActionIdentifier DescribeClusterSubnetGroups = new ActionIdentifier("redshift:DescribeClusterSubnetGroups");

		// Token: 0x04000815 RID: 2069
		public static readonly ActionIdentifier DescribeClusterVersions = new ActionIdentifier("redshift:DescribeClusterVersions");

		// Token: 0x04000816 RID: 2070
		public static readonly ActionIdentifier DescribeClusters = new ActionIdentifier("redshift:DescribeClusters");

		// Token: 0x04000817 RID: 2071
		public static readonly ActionIdentifier DescribeDefaultClusterParameters = new ActionIdentifier("redshift:DescribeDefaultClusterParameters");

		// Token: 0x04000818 RID: 2072
		public static readonly ActionIdentifier DescribeEventCategories = new ActionIdentifier("redshift:DescribeEventCategories");

		// Token: 0x04000819 RID: 2073
		public static readonly ActionIdentifier DescribeEventSubscriptions = new ActionIdentifier("redshift:DescribeEventSubscriptions");

		// Token: 0x0400081A RID: 2074
		public static readonly ActionIdentifier DescribeEvents = new ActionIdentifier("redshift:DescribeEvents");

		// Token: 0x0400081B RID: 2075
		public static readonly ActionIdentifier DescribeHsmClientCertificates = new ActionIdentifier("redshift:DescribeHsmClientCertificates");

		// Token: 0x0400081C RID: 2076
		public static readonly ActionIdentifier DescribeHsmConfigurations = new ActionIdentifier("redshift:DescribeHsmConfigurations");

		// Token: 0x0400081D RID: 2077
		public static readonly ActionIdentifier DescribeLoggingStatus = new ActionIdentifier("redshift:DescribeLoggingStatus");

		// Token: 0x0400081E RID: 2078
		public static readonly ActionIdentifier DescribeOrderableClusterOptions = new ActionIdentifier("redshift:DescribeOrderableClusterOptions");

		// Token: 0x0400081F RID: 2079
		public static readonly ActionIdentifier DescribeReservedNodeOfferings = new ActionIdentifier("redshift:DescribeReservedNodeOfferings");

		// Token: 0x04000820 RID: 2080
		public static readonly ActionIdentifier DescribeReservedNodes = new ActionIdentifier("redshift:DescribeReservedNodes");

		// Token: 0x04000821 RID: 2081
		public static readonly ActionIdentifier DescribeResize = new ActionIdentifier("redshift:DescribeResize");

		// Token: 0x04000822 RID: 2082
		public static readonly ActionIdentifier DisableLogging = new ActionIdentifier("redshift:DisableLogging");

		// Token: 0x04000823 RID: 2083
		public static readonly ActionIdentifier DisableSnapshotCopy = new ActionIdentifier("redshift:DisableSnapshotCopy");

		// Token: 0x04000824 RID: 2084
		public static readonly ActionIdentifier EnableLogging = new ActionIdentifier("redshift:EnableLogging");

		// Token: 0x04000825 RID: 2085
		public static readonly ActionIdentifier EnableSnapshotCopy = new ActionIdentifier("redshift:EnableSnapshotCopy");

		// Token: 0x04000826 RID: 2086
		public static readonly ActionIdentifier ModifyCluster = new ActionIdentifier("redshift:ModifyCluster");

		// Token: 0x04000827 RID: 2087
		public static readonly ActionIdentifier ModifyClusterParameterGroup = new ActionIdentifier("redshift:ModifyClusterParameterGroup");

		// Token: 0x04000828 RID: 2088
		public static readonly ActionIdentifier ModifyClusterSubnetGroup = new ActionIdentifier("redshift:ModifyClusterSubnetGroup");

		// Token: 0x04000829 RID: 2089
		public static readonly ActionIdentifier ModifyEventSubscription = new ActionIdentifier("redshift:ModifyEventSubscription");

		// Token: 0x0400082A RID: 2090
		public static readonly ActionIdentifier ModifySnapshotCopyRetentionPeriod = new ActionIdentifier("redshift:ModifySnapshotCopyRetentionPeriod");

		// Token: 0x0400082B RID: 2091
		public static readonly ActionIdentifier PurchaseReservedNodeOffering = new ActionIdentifier("redshift:PurchaseReservedNodeOffering");

		// Token: 0x0400082C RID: 2092
		public static readonly ActionIdentifier RebootCluster = new ActionIdentifier("redshift:RebootCluster");

		// Token: 0x0400082D RID: 2093
		public static readonly ActionIdentifier ResetClusterParameterGroup = new ActionIdentifier("redshift:ResetClusterParameterGroup");

		// Token: 0x0400082E RID: 2094
		public static readonly ActionIdentifier RestoreFromClusterSnapshot = new ActionIdentifier("redshift:RestoreFromClusterSnapshot");

		// Token: 0x0400082F RID: 2095
		public static readonly ActionIdentifier RevokeClusterSecurityGroupIngress = new ActionIdentifier("redshift:RevokeClusterSecurityGroupIngress");

		// Token: 0x04000830 RID: 2096
		public static readonly ActionIdentifier RevokeSnapshotAccess = new ActionIdentifier("redshift:RevokeSnapshotAccess");

		// Token: 0x04000831 RID: 2097
		public static readonly ActionIdentifier RotateEncryptionKey = new ActionIdentifier("redshift:RotateEncryptionKey");

		// Token: 0x04000832 RID: 2098
		public static readonly ActionIdentifier ViewQueriesInConsole = new ActionIdentifier("redshift:ViewQueriesInConsole");
	}
}
