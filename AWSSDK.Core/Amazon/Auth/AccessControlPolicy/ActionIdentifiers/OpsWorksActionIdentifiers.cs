﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000197 RID: 407
	public static class OpsWorksActionIdentifiers
	{
		// Token: 0x04000791 RID: 1937
		public static readonly ActionIdentifier AllOpsWorksActions = new ActionIdentifier("opsworks:*");

		// Token: 0x04000792 RID: 1938
		public static readonly ActionIdentifier AssignVolume = new ActionIdentifier("opsworks:AssignVolume");

		// Token: 0x04000793 RID: 1939
		public static readonly ActionIdentifier AssociateElasticIp = new ActionIdentifier("opsworks:AssociateElasticIp");

		// Token: 0x04000794 RID: 1940
		public static readonly ActionIdentifier AttachElasticLoadBalancer = new ActionIdentifier("opsworks:AttachElasticLoadBalancer");

		// Token: 0x04000795 RID: 1941
		public static readonly ActionIdentifier CloneStack = new ActionIdentifier("opsworks:CloneStack");

		// Token: 0x04000796 RID: 1942
		public static readonly ActionIdentifier CreateApp = new ActionIdentifier("opsworks:CreateApp");

		// Token: 0x04000797 RID: 1943
		public static readonly ActionIdentifier CreateDeployment = new ActionIdentifier("opsworks:CreateDeployment");

		// Token: 0x04000798 RID: 1944
		public static readonly ActionIdentifier CreateInstance = new ActionIdentifier("opsworks:CreateInstance");

		// Token: 0x04000799 RID: 1945
		public static readonly ActionIdentifier CreateLayer = new ActionIdentifier("opsworks:CreateLayer");

		// Token: 0x0400079A RID: 1946
		public static readonly ActionIdentifier CreateStack = new ActionIdentifier("opsworks:CreateStack");

		// Token: 0x0400079B RID: 1947
		public static readonly ActionIdentifier CreateUserProfile = new ActionIdentifier("opsworks:CreateUserProfile");

		// Token: 0x0400079C RID: 1948
		public static readonly ActionIdentifier DeleteApp = new ActionIdentifier("opsworks:DeleteApp");

		// Token: 0x0400079D RID: 1949
		public static readonly ActionIdentifier DeleteInstance = new ActionIdentifier("opsworks:DeleteInstance");

		// Token: 0x0400079E RID: 1950
		public static readonly ActionIdentifier DeleteLayer = new ActionIdentifier("opsworks:DeleteLayer");

		// Token: 0x0400079F RID: 1951
		public static readonly ActionIdentifier DeleteStack = new ActionIdentifier("opsworks:DeleteStack");

		// Token: 0x040007A0 RID: 1952
		public static readonly ActionIdentifier DeleteUserProfile = new ActionIdentifier("opsworks:DeleteUserProfile");

		// Token: 0x040007A1 RID: 1953
		public static readonly ActionIdentifier DeregisterElasticIp = new ActionIdentifier("opsworks:DeregisterElasticIp");

		// Token: 0x040007A2 RID: 1954
		public static readonly ActionIdentifier DeregisterVolume = new ActionIdentifier("opsworks:DeregisterVolume");

		// Token: 0x040007A3 RID: 1955
		public static readonly ActionIdentifier DescribeApps = new ActionIdentifier("opsworks:DescribeApps");

		// Token: 0x040007A4 RID: 1956
		public static readonly ActionIdentifier DescribeCommands = new ActionIdentifier("opsworks:DescribeCommands");

		// Token: 0x040007A5 RID: 1957
		public static readonly ActionIdentifier DescribeDeployments = new ActionIdentifier("opsworks:DescribeDeployments");

		// Token: 0x040007A6 RID: 1958
		public static readonly ActionIdentifier DescribeElasticIps = new ActionIdentifier("opsworks:DescribeElasticIps");

		// Token: 0x040007A7 RID: 1959
		public static readonly ActionIdentifier DescribeElasticLoadBalancers = new ActionIdentifier("opsworks:DescribeElasticLoadBalancers");

		// Token: 0x040007A8 RID: 1960
		public static readonly ActionIdentifier DescribeInstances = new ActionIdentifier("opsworks:DescribeInstances");

		// Token: 0x040007A9 RID: 1961
		public static readonly ActionIdentifier DescribeLayers = new ActionIdentifier("opsworks:DescribeLayers");

		// Token: 0x040007AA RID: 1962
		public static readonly ActionIdentifier DescribeLoadBasedAutoScaling = new ActionIdentifier("opsworks:DescribeLoadBasedAutoScaling");

		// Token: 0x040007AB RID: 1963
		public static readonly ActionIdentifier DescribePermissions = new ActionIdentifier("opsworks:DescribePermissions");

		// Token: 0x040007AC RID: 1964
		public static readonly ActionIdentifier DescribeRaidArrays = new ActionIdentifier("opsworks:DescribeRaidArrays");

		// Token: 0x040007AD RID: 1965
		public static readonly ActionIdentifier DescribeServiceErrors = new ActionIdentifier("opsworks:DescribeServiceErrors");

		// Token: 0x040007AE RID: 1966
		public static readonly ActionIdentifier DescribeStacks = new ActionIdentifier("opsworks:DescribeStacks");

		// Token: 0x040007AF RID: 1967
		public static readonly ActionIdentifier DescribeTimeBasedAutoScaling = new ActionIdentifier("opsworks:DescribeTimeBasedAutoScaling");

		// Token: 0x040007B0 RID: 1968
		public static readonly ActionIdentifier DescribeUserProfiles = new ActionIdentifier("opsworks:DescribeUserProfiles");

		// Token: 0x040007B1 RID: 1969
		public static readonly ActionIdentifier DescribeVolumes = new ActionIdentifier("opsworks:DescribeVolumes");

		// Token: 0x040007B2 RID: 1970
		public static readonly ActionIdentifier DetachElasticLoadBalancer = new ActionIdentifier("opsworks:DetachElasticLoadBalancer");

		// Token: 0x040007B3 RID: 1971
		public static readonly ActionIdentifier DisassociateElasticIp = new ActionIdentifier("opsworks:DisassociateElasticIp");

		// Token: 0x040007B4 RID: 1972
		public static readonly ActionIdentifier GetHostnameSuggestion = new ActionIdentifier("opsworks:GetHostnameSuggestion");

		// Token: 0x040007B5 RID: 1973
		public static readonly ActionIdentifier RebootInstance = new ActionIdentifier("opsworks:RebootInstance");

		// Token: 0x040007B6 RID: 1974
		public static readonly ActionIdentifier RegisterElasticIp = new ActionIdentifier("opsworks:RegisterElasticIp");

		// Token: 0x040007B7 RID: 1975
		public static readonly ActionIdentifier RegisterVolume = new ActionIdentifier("opsworks:RegisterVolume");

		// Token: 0x040007B8 RID: 1976
		public static readonly ActionIdentifier SetLoadBasedAutoScaling = new ActionIdentifier("opsworks:SetLoadBasedAutoScaling");

		// Token: 0x040007B9 RID: 1977
		public static readonly ActionIdentifier SetPermission = new ActionIdentifier("opsworks:SetPermission");

		// Token: 0x040007BA RID: 1978
		public static readonly ActionIdentifier SetTimeBasedAutoScaling = new ActionIdentifier("opsworks:SetTimeBasedAutoScaling");

		// Token: 0x040007BB RID: 1979
		public static readonly ActionIdentifier StartInstance = new ActionIdentifier("opsworks:StartInstance");

		// Token: 0x040007BC RID: 1980
		public static readonly ActionIdentifier StartStack = new ActionIdentifier("opsworks:StartStack");

		// Token: 0x040007BD RID: 1981
		public static readonly ActionIdentifier StopInstance = new ActionIdentifier("opsworks:StopInstance");

		// Token: 0x040007BE RID: 1982
		public static readonly ActionIdentifier StopStack = new ActionIdentifier("opsworks:StopStack");

		// Token: 0x040007BF RID: 1983
		public static readonly ActionIdentifier UnassignVolume = new ActionIdentifier("opsworks:UnassignVolume");

		// Token: 0x040007C0 RID: 1984
		public static readonly ActionIdentifier UpdateApp = new ActionIdentifier("opsworks:UpdateApp");

		// Token: 0x040007C1 RID: 1985
		public static readonly ActionIdentifier UpdateElasticIp = new ActionIdentifier("opsworks:UpdateElasticIp");

		// Token: 0x040007C2 RID: 1986
		public static readonly ActionIdentifier UpdateInstance = new ActionIdentifier("opsworks:UpdateInstance");

		// Token: 0x040007C3 RID: 1987
		public static readonly ActionIdentifier UpdateLayer = new ActionIdentifier("opsworks:UpdateLayer");

		// Token: 0x040007C4 RID: 1988
		public static readonly ActionIdentifier UpdateStack = new ActionIdentifier("opsworks:UpdateStack");

		// Token: 0x040007C5 RID: 1989
		public static readonly ActionIdentifier UpdateUserProfile = new ActionIdentifier("opsworks:UpdateUserProfile");

		// Token: 0x040007C6 RID: 1990
		public static readonly ActionIdentifier UpdateVolume = new ActionIdentifier("opsworks:UpdateVolume");
	}
}
