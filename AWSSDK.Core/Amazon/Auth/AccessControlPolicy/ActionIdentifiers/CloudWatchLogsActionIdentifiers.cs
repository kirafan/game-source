﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000185 RID: 389
	public static class CloudWatchLogsActionIdentifiers
	{
		// Token: 0x040005CE RID: 1486
		public static readonly ActionIdentifier AllCloudWatchLogsActions = new ActionIdentifier("logs:*");

		// Token: 0x040005CF RID: 1487
		public static readonly ActionIdentifier CreateLogGroup = new ActionIdentifier("logs:CreateLogGroup");

		// Token: 0x040005D0 RID: 1488
		public static readonly ActionIdentifier CreateLogStream = new ActionIdentifier("logs:CreateLogStream");

		// Token: 0x040005D1 RID: 1489
		public static readonly ActionIdentifier DeleteLogGroup = new ActionIdentifier("logs:DeleteLogGroup");

		// Token: 0x040005D2 RID: 1490
		public static readonly ActionIdentifier DeleteLogStream = new ActionIdentifier("logs:DeleteLogStream");

		// Token: 0x040005D3 RID: 1491
		public static readonly ActionIdentifier DeleteMetricFilter = new ActionIdentifier("logs:DeleteMetricFilter");

		// Token: 0x040005D4 RID: 1492
		public static readonly ActionIdentifier DeleteRetentionPolicy = new ActionIdentifier("logs:DeleteRetentionPolicy");

		// Token: 0x040005D5 RID: 1493
		public static readonly ActionIdentifier DescribeLogGroups = new ActionIdentifier("logs:DescribeLogGroups");

		// Token: 0x040005D6 RID: 1494
		public static readonly ActionIdentifier DescribeLogStreams = new ActionIdentifier("logs:DescribeLogStreams");

		// Token: 0x040005D7 RID: 1495
		public static readonly ActionIdentifier DescribeMetricFilters = new ActionIdentifier("logs:DescribeMetricFilters");

		// Token: 0x040005D8 RID: 1496
		public static readonly ActionIdentifier GetLogEvents = new ActionIdentifier("logs:GetLogEvents");

		// Token: 0x040005D9 RID: 1497
		public static readonly ActionIdentifier PutLogEvents = new ActionIdentifier("logs:PutLogEvents");

		// Token: 0x040005DA RID: 1498
		public static readonly ActionIdentifier PutMetricFilter = new ActionIdentifier("logs:PutMetricFilter");

		// Token: 0x040005DB RID: 1499
		public static readonly ActionIdentifier PutRetentionPolicy = new ActionIdentifier("logs:PutRetentionPolicy");

		// Token: 0x040005DC RID: 1500
		public static readonly ActionIdentifier TestMetricFilter = new ActionIdentifier("logs:TestMetricFilter");
	}
}
