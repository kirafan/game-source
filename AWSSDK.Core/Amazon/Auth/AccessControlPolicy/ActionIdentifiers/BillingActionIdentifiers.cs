﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200017F RID: 383
	public static class BillingActionIdentifiers
	{
		// Token: 0x04000576 RID: 1398
		public static readonly ActionIdentifier AllBillingActions = new ActionIdentifier("aws-portal:*");

		// Token: 0x04000577 RID: 1399
		public static readonly ActionIdentifier ModifyAccount = new ActionIdentifier("aws-portal:ModifyAccount");

		// Token: 0x04000578 RID: 1400
		public static readonly ActionIdentifier ModifyBilling = new ActionIdentifier("aws-portal:ModifyBilling");

		// Token: 0x04000579 RID: 1401
		public static readonly ActionIdentifier ModifyPaymentMethods = new ActionIdentifier("aws-portal:ModifyPaymentMethods");

		// Token: 0x0400057A RID: 1402
		public static readonly ActionIdentifier ViewAccount = new ActionIdentifier("aws-portal:ViewAccount");

		// Token: 0x0400057B RID: 1403
		public static readonly ActionIdentifier ViewBilling = new ActionIdentifier("aws-portal:ViewBilling");

		// Token: 0x0400057C RID: 1404
		public static readonly ActionIdentifier ViewPaymentMethods = new ActionIdentifier("aws-portal:ViewPaymentMethods");

		// Token: 0x0400057D RID: 1405
		public static readonly ActionIdentifier ViewUsage = new ActionIdentifier("aws-portal:ViewUsage");
	}
}
