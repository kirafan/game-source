﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000191 RID: 401
	public static class IdentityandAccessManagementActionIdentifiers
	{
		// Token: 0x04000726 RID: 1830
		public static readonly ActionIdentifier AllIdentityandAccessManagementActions = new ActionIdentifier("iam:*");

		// Token: 0x04000727 RID: 1831
		public static readonly ActionIdentifier AddRoleToInstanceProfile = new ActionIdentifier("iam:AddRoleToInstanceProfile");

		// Token: 0x04000728 RID: 1832
		public static readonly ActionIdentifier AddUserToGroup = new ActionIdentifier("iam:AddUserToGroup");

		// Token: 0x04000729 RID: 1833
		public static readonly ActionIdentifier ChangePassword = new ActionIdentifier("iam:ChangePassword");

		// Token: 0x0400072A RID: 1834
		public static readonly ActionIdentifier CreateAccessKey = new ActionIdentifier("iam:CreateAccessKey");

		// Token: 0x0400072B RID: 1835
		public static readonly ActionIdentifier CreateAccountAlias = new ActionIdentifier("iam:CreateAccountAlias");

		// Token: 0x0400072C RID: 1836
		public static readonly ActionIdentifier CreateGroup = new ActionIdentifier("iam:CreateGroup");

		// Token: 0x0400072D RID: 1837
		public static readonly ActionIdentifier CreateInstanceProfile = new ActionIdentifier("iam:CreateInstanceProfile");

		// Token: 0x0400072E RID: 1838
		public static readonly ActionIdentifier CreateLoginProfile = new ActionIdentifier("iam:CreateLoginProfile");

		// Token: 0x0400072F RID: 1839
		public static readonly ActionIdentifier CreateRole = new ActionIdentifier("iam:CreateRole");

		// Token: 0x04000730 RID: 1840
		public static readonly ActionIdentifier CreateSAMLProvider = new ActionIdentifier("iam:CreateSAMLProvider");

		// Token: 0x04000731 RID: 1841
		public static readonly ActionIdentifier CreateUser = new ActionIdentifier("iam:CreateUser");

		// Token: 0x04000732 RID: 1842
		public static readonly ActionIdentifier CreateVirtualMFADevice = new ActionIdentifier("iam:CreateVirtualMFADevice");

		// Token: 0x04000733 RID: 1843
		public static readonly ActionIdentifier DeactivateMFADevice = new ActionIdentifier("iam:DeactivateMFADevice");

		// Token: 0x04000734 RID: 1844
		public static readonly ActionIdentifier DeleteAccessKey = new ActionIdentifier("iam:DeleteAccessKey");

		// Token: 0x04000735 RID: 1845
		public static readonly ActionIdentifier DeleteAccountAlias = new ActionIdentifier("iam:DeleteAccountAlias");

		// Token: 0x04000736 RID: 1846
		public static readonly ActionIdentifier DeleteAccountPasswordPolicy = new ActionIdentifier("iam:DeleteAccountPasswordPolicy");

		// Token: 0x04000737 RID: 1847
		public static readonly ActionIdentifier DeleteGroup = new ActionIdentifier("iam:DeleteGroup");

		// Token: 0x04000738 RID: 1848
		public static readonly ActionIdentifier DeleteGroupPolicy = new ActionIdentifier("iam:DeleteGroupPolicy");

		// Token: 0x04000739 RID: 1849
		public static readonly ActionIdentifier DeleteInstanceProfile = new ActionIdentifier("iam:DeleteInstanceProfile");

		// Token: 0x0400073A RID: 1850
		public static readonly ActionIdentifier DeleteLoginProfile = new ActionIdentifier("iam:DeleteLoginProfile");

		// Token: 0x0400073B RID: 1851
		public static readonly ActionIdentifier DeleteRole = new ActionIdentifier("iam:DeleteRole");

		// Token: 0x0400073C RID: 1852
		public static readonly ActionIdentifier DeleteRolePolicy = new ActionIdentifier("iam:DeleteRolePolicy");

		// Token: 0x0400073D RID: 1853
		public static readonly ActionIdentifier DeleteSAMLProvider = new ActionIdentifier("iam:DeleteSAMLProvider");

		// Token: 0x0400073E RID: 1854
		public static readonly ActionIdentifier DeleteServerCertificate = new ActionIdentifier("iam:DeleteServerCertificate");

		// Token: 0x0400073F RID: 1855
		public static readonly ActionIdentifier DeleteSigningCertificate = new ActionIdentifier("iam:DeleteSigningCertificate");

		// Token: 0x04000740 RID: 1856
		public static readonly ActionIdentifier DeleteUser = new ActionIdentifier("iam:DeleteUser");

		// Token: 0x04000741 RID: 1857
		public static readonly ActionIdentifier DeleteUserPolicy = new ActionIdentifier("iam:DeleteUserPolicy");

		// Token: 0x04000742 RID: 1858
		public static readonly ActionIdentifier DeleteVirtualMFADevice = new ActionIdentifier("iam:DeleteVirtualMFADevice");

		// Token: 0x04000743 RID: 1859
		public static readonly ActionIdentifier EnableMFADevice = new ActionIdentifier("iam:EnableMFADevice");

		// Token: 0x04000744 RID: 1860
		public static readonly ActionIdentifier GenerateCredentialReport = new ActionIdentifier("iam:GenerateCredentialReport");

		// Token: 0x04000745 RID: 1861
		public static readonly ActionIdentifier GetAccountPasswordPolicy = new ActionIdentifier("iam:GetAccountPasswordPolicy");

		// Token: 0x04000746 RID: 1862
		public static readonly ActionIdentifier GetAccountSummary = new ActionIdentifier("iam:GetAccountSummary");

		// Token: 0x04000747 RID: 1863
		public static readonly ActionIdentifier GetCredentialReport = new ActionIdentifier("iam:GetCredentialReport");

		// Token: 0x04000748 RID: 1864
		public static readonly ActionIdentifier GetGroup = new ActionIdentifier("iam:GetGroup");

		// Token: 0x04000749 RID: 1865
		public static readonly ActionIdentifier GetGroupPolicy = new ActionIdentifier("iam:GetGroupPolicy");

		// Token: 0x0400074A RID: 1866
		public static readonly ActionIdentifier GetInstanceProfile = new ActionIdentifier("iam:GetInstanceProfile");

		// Token: 0x0400074B RID: 1867
		public static readonly ActionIdentifier GetLoginProfile = new ActionIdentifier("iam:GetLoginProfile");

		// Token: 0x0400074C RID: 1868
		public static readonly ActionIdentifier GetRole = new ActionIdentifier("iam:GetRole");

		// Token: 0x0400074D RID: 1869
		public static readonly ActionIdentifier GetRolePolicy = new ActionIdentifier("iam:GetRolePolicy");

		// Token: 0x0400074E RID: 1870
		public static readonly ActionIdentifier GetSAMLProvider = new ActionIdentifier("iam:GetSAMLProvider");

		// Token: 0x0400074F RID: 1871
		public static readonly ActionIdentifier GetServerCertificate = new ActionIdentifier("iam:GetServerCertificate");

		// Token: 0x04000750 RID: 1872
		public static readonly ActionIdentifier GetUser = new ActionIdentifier("iam:GetUser");

		// Token: 0x04000751 RID: 1873
		public static readonly ActionIdentifier GetUserPolicy = new ActionIdentifier("iam:GetUserPolicy");

		// Token: 0x04000752 RID: 1874
		public static readonly ActionIdentifier ListAccessKeys = new ActionIdentifier("iam:ListAccessKeys");

		// Token: 0x04000753 RID: 1875
		public static readonly ActionIdentifier ListAccountAliases = new ActionIdentifier("iam:ListAccountAliases");

		// Token: 0x04000754 RID: 1876
		public static readonly ActionIdentifier ListGroupPolicies = new ActionIdentifier("iam:ListGroupPolicies");

		// Token: 0x04000755 RID: 1877
		public static readonly ActionIdentifier ListGroups = new ActionIdentifier("iam:ListGroups");

		// Token: 0x04000756 RID: 1878
		public static readonly ActionIdentifier ListGroupsForUser = new ActionIdentifier("iam:ListGroupsForUser");

		// Token: 0x04000757 RID: 1879
		public static readonly ActionIdentifier ListInstanceProfiles = new ActionIdentifier("iam:ListInstanceProfiles");

		// Token: 0x04000758 RID: 1880
		public static readonly ActionIdentifier ListInstanceProfilesForRole = new ActionIdentifier("iam:ListInstanceProfilesForRole");

		// Token: 0x04000759 RID: 1881
		public static readonly ActionIdentifier ListMFADevices = new ActionIdentifier("iam:ListMFADevices");

		// Token: 0x0400075A RID: 1882
		public static readonly ActionIdentifier ListRolePolicies = new ActionIdentifier("iam:ListRolePolicies");

		// Token: 0x0400075B RID: 1883
		public static readonly ActionIdentifier ListRoles = new ActionIdentifier("iam:ListRoles");

		// Token: 0x0400075C RID: 1884
		public static readonly ActionIdentifier ListSAMLProviders = new ActionIdentifier("iam:ListSAMLProviders");

		// Token: 0x0400075D RID: 1885
		public static readonly ActionIdentifier ListServerCertificates = new ActionIdentifier("iam:ListServerCertificates");

		// Token: 0x0400075E RID: 1886
		public static readonly ActionIdentifier ListSigningCertificates = new ActionIdentifier("iam:ListSigningCertificates");

		// Token: 0x0400075F RID: 1887
		public static readonly ActionIdentifier ListUserPolicies = new ActionIdentifier("iam:ListUserPolicies");

		// Token: 0x04000760 RID: 1888
		public static readonly ActionIdentifier ListUsers = new ActionIdentifier("iam:ListUsers");

		// Token: 0x04000761 RID: 1889
		public static readonly ActionIdentifier ListVirtualMFADevices = new ActionIdentifier("iam:ListVirtualMFADevices");

		// Token: 0x04000762 RID: 1890
		public static readonly ActionIdentifier PassRole = new ActionIdentifier("iam:PassRole");

		// Token: 0x04000763 RID: 1891
		public static readonly ActionIdentifier PutGroupPolicy = new ActionIdentifier("iam:PutGroupPolicy");

		// Token: 0x04000764 RID: 1892
		public static readonly ActionIdentifier PutRolePolicy = new ActionIdentifier("iam:PutRolePolicy");

		// Token: 0x04000765 RID: 1893
		public static readonly ActionIdentifier PutUserPolicy = new ActionIdentifier("iam:PutUserPolicy");

		// Token: 0x04000766 RID: 1894
		public static readonly ActionIdentifier RemoveRoleFromInstanceProfile = new ActionIdentifier("iam:RemoveRoleFromInstanceProfile");

		// Token: 0x04000767 RID: 1895
		public static readonly ActionIdentifier RemoveUserFromGroup = new ActionIdentifier("iam:RemoveUserFromGroup");

		// Token: 0x04000768 RID: 1896
		public static readonly ActionIdentifier ResyncMFADevice = new ActionIdentifier("iam:ResyncMFADevice");

		// Token: 0x04000769 RID: 1897
		public static readonly ActionIdentifier UpdateAccessKey = new ActionIdentifier("iam:UpdateAccessKey");

		// Token: 0x0400076A RID: 1898
		public static readonly ActionIdentifier UpdateAccountPasswordPolicy = new ActionIdentifier("iam:UpdateAccountPasswordPolicy");

		// Token: 0x0400076B RID: 1899
		public static readonly ActionIdentifier UpdateAssumeRolePolicy = new ActionIdentifier("iam:UpdateAssumeRolePolicy");

		// Token: 0x0400076C RID: 1900
		public static readonly ActionIdentifier UpdateGroup = new ActionIdentifier("iam:UpdateGroup");

		// Token: 0x0400076D RID: 1901
		public static readonly ActionIdentifier UpdateLoginProfile = new ActionIdentifier("iam:UpdateLoginProfile");

		// Token: 0x0400076E RID: 1902
		public static readonly ActionIdentifier UpdateSAMLProvider = new ActionIdentifier("iam:UpdateSAMLProvider");

		// Token: 0x0400076F RID: 1903
		public static readonly ActionIdentifier UpdateServerCertificate = new ActionIdentifier("iam:UpdateServerCertificate");

		// Token: 0x04000770 RID: 1904
		public static readonly ActionIdentifier UpdateSigningCertificate = new ActionIdentifier("iam:UpdateSigningCertificate");

		// Token: 0x04000771 RID: 1905
		public static readonly ActionIdentifier UpdateUser = new ActionIdentifier("iam:UpdateUser");

		// Token: 0x04000772 RID: 1906
		public static readonly ActionIdentifier UploadServerCertificate = new ActionIdentifier("iam:UploadServerCertificate");

		// Token: 0x04000773 RID: 1907
		public static readonly ActionIdentifier UploadSigningCertificate = new ActionIdentifier("iam:UploadSigningCertificate");
	}
}
