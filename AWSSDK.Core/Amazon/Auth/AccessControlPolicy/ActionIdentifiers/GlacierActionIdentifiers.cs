﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000190 RID: 400
	public static class GlacierActionIdentifiers
	{
		// Token: 0x04000712 RID: 1810
		public static readonly ActionIdentifier AllGlacierActions = new ActionIdentifier("glacier:*");

		// Token: 0x04000713 RID: 1811
		public static readonly ActionIdentifier AbortMultipartUpload = new ActionIdentifier("glacier:AbortMultipartUpload");

		// Token: 0x04000714 RID: 1812
		public static readonly ActionIdentifier CompleteMultipartUpload = new ActionIdentifier("glacier:CompleteMultipartUpload");

		// Token: 0x04000715 RID: 1813
		public static readonly ActionIdentifier CreateVault = new ActionIdentifier("glacier:CreateVault");

		// Token: 0x04000716 RID: 1814
		public static readonly ActionIdentifier DeleteArchive = new ActionIdentifier("glacier:DeleteArchive");

		// Token: 0x04000717 RID: 1815
		public static readonly ActionIdentifier DeleteVault = new ActionIdentifier("glacier:DeleteVault");

		// Token: 0x04000718 RID: 1816
		public static readonly ActionIdentifier DeleteVaultNotifications = new ActionIdentifier("glacier:DeleteVaultNotifications");

		// Token: 0x04000719 RID: 1817
		public static readonly ActionIdentifier DescribeJob = new ActionIdentifier("glacier:DescribeJob");

		// Token: 0x0400071A RID: 1818
		public static readonly ActionIdentifier DescribeVault = new ActionIdentifier("glacier:DescribeVault");

		// Token: 0x0400071B RID: 1819
		public static readonly ActionIdentifier GetJobOutput = new ActionIdentifier("glacier:GetJobOutput");

		// Token: 0x0400071C RID: 1820
		public static readonly ActionIdentifier GetVaultNotifications = new ActionIdentifier("glacier:GetVaultNotifications");

		// Token: 0x0400071D RID: 1821
		public static readonly ActionIdentifier InitiateMultipartUpload = new ActionIdentifier("glacier:InitiateMultipartUpload");

		// Token: 0x0400071E RID: 1822
		public static readonly ActionIdentifier InitiateJob = new ActionIdentifier("glacier:InitiateJob");

		// Token: 0x0400071F RID: 1823
		public static readonly ActionIdentifier ListJobs = new ActionIdentifier("glacier:ListJobs");

		// Token: 0x04000720 RID: 1824
		public static readonly ActionIdentifier ListMultipartUploads = new ActionIdentifier("glacier:ListMultipartUploads");

		// Token: 0x04000721 RID: 1825
		public static readonly ActionIdentifier ListParts = new ActionIdentifier("glacier:ListParts");

		// Token: 0x04000722 RID: 1826
		public static readonly ActionIdentifier ListVaults = new ActionIdentifier("glacier:ListVaults");

		// Token: 0x04000723 RID: 1827
		public static readonly ActionIdentifier SetVaultNotifications = new ActionIdentifier("glacier:SetVaultNotifications");

		// Token: 0x04000724 RID: 1828
		public static readonly ActionIdentifier UploadArchive = new ActionIdentifier("glacier:UploadArchive");

		// Token: 0x04000725 RID: 1829
		public static readonly ActionIdentifier UploadMultipartPart = new ActionIdentifier("glacier:UploadMultipartPart");
	}
}
