﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200019E RID: 414
	public static class SimpleDBActionIdentifiers
	{
		// Token: 0x0400087D RID: 2173
		public static readonly ActionIdentifier AllSimpleDBActions = new ActionIdentifier("sdb:*");

		// Token: 0x0400087E RID: 2174
		public static readonly ActionIdentifier BatchDeleteAttributes = new ActionIdentifier("sdb:BatchDeleteAttributes");

		// Token: 0x0400087F RID: 2175
		public static readonly ActionIdentifier BatchPutAttributes = new ActionIdentifier("sdb:BatchPutAttributes");

		// Token: 0x04000880 RID: 2176
		public static readonly ActionIdentifier CreateDomain = new ActionIdentifier("sdb:CreateDomain");

		// Token: 0x04000881 RID: 2177
		public static readonly ActionIdentifier DeleteAttributes = new ActionIdentifier("sdb:DeleteAttributes");

		// Token: 0x04000882 RID: 2178
		public static readonly ActionIdentifier DeleteDomain = new ActionIdentifier("sdb:DeleteDomain");

		// Token: 0x04000883 RID: 2179
		public static readonly ActionIdentifier DomainMetadata = new ActionIdentifier("sdb:DomainMetadata");

		// Token: 0x04000884 RID: 2180
		public static readonly ActionIdentifier GetAttributes = new ActionIdentifier("sdb:GetAttributes");

		// Token: 0x04000885 RID: 2181
		public static readonly ActionIdentifier ListDomains = new ActionIdentifier("sdb:ListDomains");

		// Token: 0x04000886 RID: 2182
		public static readonly ActionIdentifier PutAttributes = new ActionIdentifier("sdb:PutAttributes");

		// Token: 0x04000887 RID: 2183
		public static readonly ActionIdentifier Select = new ActionIdentifier("sdb:Select");
	}
}
