﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200019A RID: 410
	public static class Route53ActionIdentifiers
	{
		// Token: 0x04000833 RID: 2099
		public static readonly ActionIdentifier AllRoute53Actions = new ActionIdentifier("route53:*");

		// Token: 0x04000834 RID: 2100
		public static readonly ActionIdentifier ChangeResourceRecordSets = new ActionIdentifier("route53:ChangeResourceRecordSets");

		// Token: 0x04000835 RID: 2101
		public static readonly ActionIdentifier CreateHostedZone = new ActionIdentifier("route53:CreateHostedZone");

		// Token: 0x04000836 RID: 2102
		public static readonly ActionIdentifier DeleteHostedZone = new ActionIdentifier("route53:DeleteHostedZone");

		// Token: 0x04000837 RID: 2103
		public static readonly ActionIdentifier GetChange = new ActionIdentifier("route53:GetChange");

		// Token: 0x04000838 RID: 2104
		public static readonly ActionIdentifier GetHostedZone = new ActionIdentifier("route53:GetHostedZone");

		// Token: 0x04000839 RID: 2105
		public static readonly ActionIdentifier ListHostedZones = new ActionIdentifier("route53:ListHostedZones");

		// Token: 0x0400083A RID: 2106
		public static readonly ActionIdentifier ListResourceRecordSets = new ActionIdentifier("route53:ListResourceRecordSets");
	}
}
