﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000198 RID: 408
	public static class RDSActionIdentifiers
	{
		// Token: 0x040007C7 RID: 1991
		public static readonly ActionIdentifier AllRDSActions = new ActionIdentifier("rds:*");

		// Token: 0x040007C8 RID: 1992
		public static readonly ActionIdentifier AuthorizeDBSecurityGroupIngress = new ActionIdentifier("rds:AuthorizeDBSecurityGroupIngress");

		// Token: 0x040007C9 RID: 1993
		public static readonly ActionIdentifier AddTagsToResource = new ActionIdentifier("rds:AddTagsToResource");

		// Token: 0x040007CA RID: 1994
		public static readonly ActionIdentifier AddSourceIdentifierToSubscription = new ActionIdentifier("rds:AddSourceIdentifierToSubscription");

		// Token: 0x040007CB RID: 1995
		public static readonly ActionIdentifier CopyDBSnapshot = new ActionIdentifier("rds:CopyDBSnapshot");

		// Token: 0x040007CC RID: 1996
		public static readonly ActionIdentifier CreateDBInstance = new ActionIdentifier("rds:CreateDBInstance");

		// Token: 0x040007CD RID: 1997
		public static readonly ActionIdentifier CreateDBInstanceReadReplica = new ActionIdentifier("rds:CreateDBInstanceReadReplica");

		// Token: 0x040007CE RID: 1998
		public static readonly ActionIdentifier CreateDBParameterGroup = new ActionIdentifier("rds:CreateDBParameterGroup");

		// Token: 0x040007CF RID: 1999
		public static readonly ActionIdentifier CreateDBSecurityGroup = new ActionIdentifier("rds:CreateDBSecurityGroup");

		// Token: 0x040007D0 RID: 2000
		public static readonly ActionIdentifier CreateDBSnapshot = new ActionIdentifier("rds:CreateDBSnapshot");

		// Token: 0x040007D1 RID: 2001
		public static readonly ActionIdentifier CreateDBSubnetGroup = new ActionIdentifier("rds:CreateDBSubnetGroup");

		// Token: 0x040007D2 RID: 2002
		public static readonly ActionIdentifier CreateEventSubscription = new ActionIdentifier("rds:CreateEventSubscription");

		// Token: 0x040007D3 RID: 2003
		public static readonly ActionIdentifier CreateOptionGroup = new ActionIdentifier("rds:CreateOptionGroup");

		// Token: 0x040007D4 RID: 2004
		public static readonly ActionIdentifier DeleteDBInstance = new ActionIdentifier("rds:DeleteDBInstance");

		// Token: 0x040007D5 RID: 2005
		public static readonly ActionIdentifier DeleteDBParameterGroup = new ActionIdentifier("rds:DeleteDBParameterGroup");

		// Token: 0x040007D6 RID: 2006
		public static readonly ActionIdentifier DeleteDBSecurityGroup = new ActionIdentifier("rds:DeleteDBSecurityGroup");

		// Token: 0x040007D7 RID: 2007
		public static readonly ActionIdentifier DeleteDBSnapshot = new ActionIdentifier("rds:DeleteDBSnapshot");

		// Token: 0x040007D8 RID: 2008
		public static readonly ActionIdentifier DeleteDBSubnetGroup = new ActionIdentifier("rds:DeleteDBSubnetGroup");

		// Token: 0x040007D9 RID: 2009
		public static readonly ActionIdentifier DeleteEventSubscription = new ActionIdentifier("rds:DeleteEventSubscription");

		// Token: 0x040007DA RID: 2010
		public static readonly ActionIdentifier DeleteOptionGroup = new ActionIdentifier("rds:DeleteOptionGroup");

		// Token: 0x040007DB RID: 2011
		public static readonly ActionIdentifier DescribeEngineDefaultParameters = new ActionIdentifier("rds:DescribeEngineDefaultParameters");

		// Token: 0x040007DC RID: 2012
		public static readonly ActionIdentifier DescribeDBInstances = new ActionIdentifier("rds:DescribeDBInstances");

		// Token: 0x040007DD RID: 2013
		public static readonly ActionIdentifier DescribeDBLogFiles = new ActionIdentifier("rds:DescribeDBLogFiles");

		// Token: 0x040007DE RID: 2014
		public static readonly ActionIdentifier DescribeDBParameterGroups = new ActionIdentifier("rds:DescribeDBParameterGroups");

		// Token: 0x040007DF RID: 2015
		public static readonly ActionIdentifier DescribeDBParameters = new ActionIdentifier("rds:DescribeDBParameters");

		// Token: 0x040007E0 RID: 2016
		public static readonly ActionIdentifier DescribeDBSecurityGroups = new ActionIdentifier("rds:DescribeDBSecurityGroups");

		// Token: 0x040007E1 RID: 2017
		public static readonly ActionIdentifier DescribeDBSnapshots = new ActionIdentifier("rds:DescribeDBSnapshots");

		// Token: 0x040007E2 RID: 2018
		public static readonly ActionIdentifier DescribeDBEngineVersions = new ActionIdentifier("rds:DescribeDBEngineVersions");

		// Token: 0x040007E3 RID: 2019
		public static readonly ActionIdentifier DescribeDBSubnetGroups = new ActionIdentifier("rds:DescribeDBSubnetGroups");

		// Token: 0x040007E4 RID: 2020
		public static readonly ActionIdentifier DescribeEventCategories = new ActionIdentifier("rds:DescribeEventCategories");

		// Token: 0x040007E5 RID: 2021
		public static readonly ActionIdentifier DescribeEvents = new ActionIdentifier("rds:DescribeEvents");

		// Token: 0x040007E6 RID: 2022
		public static readonly ActionIdentifier DescribeEventSubscriptions = new ActionIdentifier("rds:DescribeEventSubscriptions");

		// Token: 0x040007E7 RID: 2023
		public static readonly ActionIdentifier DescribeOptionGroups = new ActionIdentifier("rds:DescribeOptionGroups");

		// Token: 0x040007E8 RID: 2024
		public static readonly ActionIdentifier DescribeOptionGroupOptions = new ActionIdentifier("rds:DescribeOptionGroupOptions");

		// Token: 0x040007E9 RID: 2025
		public static readonly ActionIdentifier DescribeOrderableDBInstanceOptions = new ActionIdentifier("rds:DescribeOrderableDBInstanceOptions");

		// Token: 0x040007EA RID: 2026
		public static readonly ActionIdentifier DescribeReservedDBInstances = new ActionIdentifier("rds:DescribeReservedDBInstances");

		// Token: 0x040007EB RID: 2027
		public static readonly ActionIdentifier DescribeReservedDBInstancesOfferings = new ActionIdentifier("rds:DescribeReservedDBInstancesOfferings");

		// Token: 0x040007EC RID: 2028
		public static readonly ActionIdentifier DownloadDBLogFilePortion = new ActionIdentifier("rds:DownloadDBLogFilePortion");

		// Token: 0x040007ED RID: 2029
		public static readonly ActionIdentifier ListTagsForResource = new ActionIdentifier("rds:ListTagsForResource");

		// Token: 0x040007EE RID: 2030
		public static readonly ActionIdentifier ModifyDBInstance = new ActionIdentifier("rds:ModifyDBInstance");

		// Token: 0x040007EF RID: 2031
		public static readonly ActionIdentifier ModifyDBParameterGroup = new ActionIdentifier("rds:ModifyDBParameterGroup");

		// Token: 0x040007F0 RID: 2032
		public static readonly ActionIdentifier ModifyDBSubnetGroup = new ActionIdentifier("rds:ModifyDBSubnetGroup");

		// Token: 0x040007F1 RID: 2033
		public static readonly ActionIdentifier ModifyEventSubscription = new ActionIdentifier("rds:ModifyEventSubscription");

		// Token: 0x040007F2 RID: 2034
		public static readonly ActionIdentifier ModifyOptionGroup = new ActionIdentifier("rds:ModifyOptionGroup");

		// Token: 0x040007F3 RID: 2035
		public static readonly ActionIdentifier PromoteReadReplica = new ActionIdentifier("rds:PromoteReadReplica");

		// Token: 0x040007F4 RID: 2036
		public static readonly ActionIdentifier PurchaseReservedDBInstancesOffering = new ActionIdentifier("rds:PurchaseReservedDBInstancesOffering");

		// Token: 0x040007F5 RID: 2037
		public static readonly ActionIdentifier RebootDBInstance = new ActionIdentifier("rds:RebootDBInstance");

		// Token: 0x040007F6 RID: 2038
		public static readonly ActionIdentifier RemoveSourceIdentifierFromSubscription = new ActionIdentifier("rds:RemoveSourceIdentifierFromSubscription");

		// Token: 0x040007F7 RID: 2039
		public static readonly ActionIdentifier RemoveTagsFromResource = new ActionIdentifier("rds:RemoveTagsFromResource");

		// Token: 0x040007F8 RID: 2040
		public static readonly ActionIdentifier RestoreDBInstanceFromDBSnapshot = new ActionIdentifier("rds:RestoreDBInstanceFromDBSnapshot");

		// Token: 0x040007F9 RID: 2041
		public static readonly ActionIdentifier RestoreDBInstanceToPointInTime = new ActionIdentifier("rds:RestoreDBInstanceToPointInTime");

		// Token: 0x040007FA RID: 2042
		public static readonly ActionIdentifier ResetDBParameterGroup = new ActionIdentifier("rds:ResetDBParameterGroup");

		// Token: 0x040007FB RID: 2043
		public static readonly ActionIdentifier RevokeDBSecurityGroupIngress = new ActionIdentifier("rds:RevokeDBSecurityGroupIngress");
	}
}
