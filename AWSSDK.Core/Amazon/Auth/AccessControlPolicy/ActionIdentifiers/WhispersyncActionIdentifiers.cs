﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x020001A3 RID: 419
	public static class WhispersyncActionIdentifiers
	{
		// Token: 0x0400090B RID: 2315
		public static readonly ActionIdentifier AllWhispersyncActions = new ActionIdentifier("whispersync:*");

		// Token: 0x0400090C RID: 2316
		public static readonly ActionIdentifier GetDatamapUpdates = new ActionIdentifier("whispersync:GetDatamapUpdates");

		// Token: 0x0400090D RID: 2317
		public static readonly ActionIdentifier PatchDatamapUpdates = new ActionIdentifier("whispersync:PatchDatamapUpdates");
	}
}
