﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200019D RID: 413
	public static class SESActionIdentifiers
	{
		// Token: 0x0400086A RID: 2154
		public static readonly ActionIdentifier AllSESActions = new ActionIdentifier("ses:*");

		// Token: 0x0400086B RID: 2155
		public static readonly ActionIdentifier DeleteIdentity = new ActionIdentifier("ses:DeleteIdentity");

		// Token: 0x0400086C RID: 2156
		public static readonly ActionIdentifier DeleteVerifiedEmailAddress = new ActionIdentifier("ses:DeleteVerifiedEmailAddress");

		// Token: 0x0400086D RID: 2157
		public static readonly ActionIdentifier GetIdentityDkimAttributes = new ActionIdentifier("ses:GetIdentityDkimAttributes");

		// Token: 0x0400086E RID: 2158
		public static readonly ActionIdentifier GetIdentityNotificationAttributes = new ActionIdentifier("ses:GetIdentityNotificationAttributes");

		// Token: 0x0400086F RID: 2159
		public static readonly ActionIdentifier GetIdentityVerificationAttributes = new ActionIdentifier("ses:GetIdentityVerificationAttributes");

		// Token: 0x04000870 RID: 2160
		public static readonly ActionIdentifier GetSendQuota = new ActionIdentifier("ses:GetSendQuota");

		// Token: 0x04000871 RID: 2161
		public static readonly ActionIdentifier GetSendStatistics = new ActionIdentifier("ses:GetSendStatistics");

		// Token: 0x04000872 RID: 2162
		public static readonly ActionIdentifier ListIdentities = new ActionIdentifier("ses:ListIdentities");

		// Token: 0x04000873 RID: 2163
		public static readonly ActionIdentifier ListVerifiedEmailAddresses = new ActionIdentifier("ses:ListVerifiedEmailAddresses");

		// Token: 0x04000874 RID: 2164
		public static readonly ActionIdentifier SendEmail = new ActionIdentifier("ses:SendEmail");

		// Token: 0x04000875 RID: 2165
		public static readonly ActionIdentifier SendRawEmail = new ActionIdentifier("ses:SendRawEmail");

		// Token: 0x04000876 RID: 2166
		public static readonly ActionIdentifier SetIdentityDkimEnabled = new ActionIdentifier("ses:SetIdentityDkimEnabled");

		// Token: 0x04000877 RID: 2167
		public static readonly ActionIdentifier SetIdentityNotificationTopic = new ActionIdentifier("ses:SetIdentityNotificationTopic");

		// Token: 0x04000878 RID: 2168
		public static readonly ActionIdentifier SetIdentityFeedbackForwardingEnabled = new ActionIdentifier("ses:SetIdentityFeedbackForwardingEnabled");

		// Token: 0x04000879 RID: 2169
		public static readonly ActionIdentifier VerifyDomainDkim = new ActionIdentifier("ses:VerifyDomainDkim");

		// Token: 0x0400087A RID: 2170
		public static readonly ActionIdentifier VerifyDomainIdentity = new ActionIdentifier("ses:VerifyDomainIdentity");

		// Token: 0x0400087B RID: 2171
		public static readonly ActionIdentifier VerifyEmailAddress = new ActionIdentifier("ses:VerifyEmailAddress");

		// Token: 0x0400087C RID: 2172
		public static readonly ActionIdentifier VerifyEmailIdentity = new ActionIdentifier("ses:VerifyEmailIdentity");
	}
}
