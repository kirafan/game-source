﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x020001A1 RID: 417
	public static class SQSActionIdentifiers
	{
		// Token: 0x040008CE RID: 2254
		public static readonly ActionIdentifier AllSQSActions = new ActionIdentifier("sqs:*");

		// Token: 0x040008CF RID: 2255
		public static readonly ActionIdentifier AddPermission = new ActionIdentifier("sqs:AddPermission");

		// Token: 0x040008D0 RID: 2256
		public static readonly ActionIdentifier ChangeMessageVisibility = new ActionIdentifier("sqs:ChangeMessageVisibility");

		// Token: 0x040008D1 RID: 2257
		public static readonly ActionIdentifier CreateQueue = new ActionIdentifier("sqs:CreateQueue");

		// Token: 0x040008D2 RID: 2258
		public static readonly ActionIdentifier DeleteMessage = new ActionIdentifier("sqs:DeleteMessage");

		// Token: 0x040008D3 RID: 2259
		public static readonly ActionIdentifier DeleteQueue = new ActionIdentifier("sqs:DeleteQueue");

		// Token: 0x040008D4 RID: 2260
		public static readonly ActionIdentifier GetQueueAttributes = new ActionIdentifier("sqs:GetQueueAttributes");

		// Token: 0x040008D5 RID: 2261
		public static readonly ActionIdentifier GetQueueUrl = new ActionIdentifier("sqs:GetQueueUrl");

		// Token: 0x040008D6 RID: 2262
		public static readonly ActionIdentifier ListQueues = new ActionIdentifier("sqs:ListQueues");

		// Token: 0x040008D7 RID: 2263
		public static readonly ActionIdentifier ReceiveMessage = new ActionIdentifier("sqs:ReceiveMessage");

		// Token: 0x040008D8 RID: 2264
		public static readonly ActionIdentifier RemovePermission = new ActionIdentifier("sqs:RemovePermission");

		// Token: 0x040008D9 RID: 2265
		public static readonly ActionIdentifier SendMessage = new ActionIdentifier("sqs:SendMessage");

		// Token: 0x040008DA RID: 2266
		public static readonly ActionIdentifier SetQueueAttributes = new ActionIdentifier("sqs:SetQueueAttributes");
	}
}
