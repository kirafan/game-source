﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000188 RID: 392
	public static class DirectConnectActionIdentifiers
	{
		// Token: 0x040005ED RID: 1517
		public static readonly ActionIdentifier AllDirectConnectActions = new ActionIdentifier("directconnect:*");

		// Token: 0x040005EE RID: 1518
		public static readonly ActionIdentifier CreateConnection = new ActionIdentifier("directconnect:CreateConnection");

		// Token: 0x040005EF RID: 1519
		public static readonly ActionIdentifier CreatePrivateVirtualInterface = new ActionIdentifier("directconnect:CreatePrivateVirtualInterface");

		// Token: 0x040005F0 RID: 1520
		public static readonly ActionIdentifier CreatePublicVirtualInterface = new ActionIdentifier("directconnect:CreatePublicVirtualInterface");

		// Token: 0x040005F1 RID: 1521
		public static readonly ActionIdentifier DeleteConnection = new ActionIdentifier("directconnect:DeleteConnection");

		// Token: 0x040005F2 RID: 1522
		public static readonly ActionIdentifier DeleteVirtualInterface = new ActionIdentifier("directconnect:DeleteVirtualInterface");

		// Token: 0x040005F3 RID: 1523
		public static readonly ActionIdentifier DescribeConnectionDetail = new ActionIdentifier("directconnect:DescribeConnectionDetail");

		// Token: 0x040005F4 RID: 1524
		public static readonly ActionIdentifier DescribeConnections = new ActionIdentifier("directconnect:DescribeConnections");

		// Token: 0x040005F5 RID: 1525
		public static readonly ActionIdentifier DescribeOfferingDetail = new ActionIdentifier("directconnect:DescribeOfferingDetail");

		// Token: 0x040005F6 RID: 1526
		public static readonly ActionIdentifier DescribeOfferings = new ActionIdentifier("directconnect:DescribeOfferings");

		// Token: 0x040005F7 RID: 1527
		public static readonly ActionIdentifier DescribeVirtualGateways = new ActionIdentifier("directconnect:DescribeVirtualGateways");

		// Token: 0x040005F8 RID: 1528
		public static readonly ActionIdentifier DescribeVirtualInterfaces = new ActionIdentifier("directconnect:DescribeVirtualInterfaces");
	}
}
