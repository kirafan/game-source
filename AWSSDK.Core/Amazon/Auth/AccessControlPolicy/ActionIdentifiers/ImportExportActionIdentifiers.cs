﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000192 RID: 402
	public static class ImportExportActionIdentifiers
	{
		// Token: 0x04000774 RID: 1908
		public static readonly ActionIdentifier AllImportExportActions = new ActionIdentifier("importexport:*");

		// Token: 0x04000775 RID: 1909
		public static readonly ActionIdentifier CreateJob = new ActionIdentifier("importexport:CreateJob");

		// Token: 0x04000776 RID: 1910
		public static readonly ActionIdentifier UpdateJob = new ActionIdentifier("importexport:UpdateJob");

		// Token: 0x04000777 RID: 1911
		public static readonly ActionIdentifier CancelJob = new ActionIdentifier("importexport:CancelJob");

		// Token: 0x04000778 RID: 1912
		public static readonly ActionIdentifier ListJobs = new ActionIdentifier("importexport:ListJobs");

		// Token: 0x04000779 RID: 1913
		public static readonly ActionIdentifier GetStatus = new ActionIdentifier("importexport:GetStatus");
	}
}
