﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x020001A4 RID: 420
	public static class ZocaloActionIdentifiers
	{
		// Token: 0x0400090E RID: 2318
		public static readonly ActionIdentifier AllZocaloActions = new ActionIdentifier("zocalo:*");

		// Token: 0x0400090F RID: 2319
		public static readonly ActionIdentifier ActivateUser = new ActionIdentifier("zocalo:ActivateUser");

		// Token: 0x04000910 RID: 2320
		public static readonly ActionIdentifier AddUserToGroup = new ActionIdentifier("zocalo:AddUserToGroup");

		// Token: 0x04000911 RID: 2321
		public static readonly ActionIdentifier CheckAlias = new ActionIdentifier("zocalo:CheckAlias");

		// Token: 0x04000912 RID: 2322
		public static readonly ActionIdentifier CreateInstance = new ActionIdentifier("zocalo:CreateInstance");

		// Token: 0x04000913 RID: 2323
		public static readonly ActionIdentifier DeactivateUser = new ActionIdentifier("zocalo:DeactivateUser");

		// Token: 0x04000914 RID: 2324
		public static readonly ActionIdentifier DeleteInstance = new ActionIdentifier("zocalo:DeleteInstance");

		// Token: 0x04000915 RID: 2325
		public static readonly ActionIdentifier DeregisterDirectory = new ActionIdentifier("zocalo:DeregisterDirectory");

		// Token: 0x04000916 RID: 2326
		public static readonly ActionIdentifier DescribeAvailableDirectories = new ActionIdentifier("zocalo:DescribeAvailableDirectories");

		// Token: 0x04000917 RID: 2327
		public static readonly ActionIdentifier DescribeInstances = new ActionIdentifier("zocalo:DescribeInstances");

		// Token: 0x04000918 RID: 2328
		public static readonly ActionIdentifier RegisterDirectory = new ActionIdentifier("zocalo:RegisterDirectory");

		// Token: 0x04000919 RID: 2329
		public static readonly ActionIdentifier RemoveUserFromGroup = new ActionIdentifier("zocalo:RemoveUserFromGroup");

		// Token: 0x0400091A RID: 2330
		public static readonly ActionIdentifier UpdateInstanceAlias = new ActionIdentifier("zocalo:UpdateInstanceAlias");
	}
}
