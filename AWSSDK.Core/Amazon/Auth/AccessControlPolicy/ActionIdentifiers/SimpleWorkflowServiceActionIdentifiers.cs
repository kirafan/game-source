﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200019F RID: 415
	public static class SimpleWorkflowServiceActionIdentifiers
	{
		// Token: 0x04000888 RID: 2184
		public static readonly ActionIdentifier AllSimpleWorkflowServiceActions = new ActionIdentifier("swf:*");

		// Token: 0x04000889 RID: 2185
		public static readonly ActionIdentifier CancelTimer = new ActionIdentifier("swf:CancelTimer");

		// Token: 0x0400088A RID: 2186
		public static readonly ActionIdentifier CancelWorkflowExecution = new ActionIdentifier("swf:CancelWorkflowExecution");

		// Token: 0x0400088B RID: 2187
		public static readonly ActionIdentifier CompleteWorkflowExecution = new ActionIdentifier("swf:CompleteWorkflowExecution");

		// Token: 0x0400088C RID: 2188
		public static readonly ActionIdentifier ContinueAsNewWorkflowExecution = new ActionIdentifier("swf:ContinueAsNewWorkflowExecution");

		// Token: 0x0400088D RID: 2189
		public static readonly ActionIdentifier CountClosedWorkflowExecutions = new ActionIdentifier("swf:CountClosedWorkflowExecutions");

		// Token: 0x0400088E RID: 2190
		public static readonly ActionIdentifier CountOpenWorkflowExecutions = new ActionIdentifier("swf:CountOpenWorkflowExecutions");

		// Token: 0x0400088F RID: 2191
		public static readonly ActionIdentifier CountPendingActivityTasks = new ActionIdentifier("swf:CountPendingActivityTasks");

		// Token: 0x04000890 RID: 2192
		public static readonly ActionIdentifier CountPendingDecisionTasks = new ActionIdentifier("swf:CountPendingDecisionTasks");

		// Token: 0x04000891 RID: 2193
		public static readonly ActionIdentifier DeprecateActivityType = new ActionIdentifier("swf:DeprecateActivityType");

		// Token: 0x04000892 RID: 2194
		public static readonly ActionIdentifier DeprecateDomain = new ActionIdentifier("swf:DeprecateDomain");

		// Token: 0x04000893 RID: 2195
		public static readonly ActionIdentifier DeprecateWorkflowType = new ActionIdentifier("swf:DeprecateWorkflowType");

		// Token: 0x04000894 RID: 2196
		public static readonly ActionIdentifier DescribeActivityType = new ActionIdentifier("swf:DescribeActivityType");

		// Token: 0x04000895 RID: 2197
		public static readonly ActionIdentifier DescribeDomain = new ActionIdentifier("swf:DescribeDomain");

		// Token: 0x04000896 RID: 2198
		public static readonly ActionIdentifier DescribeWorkflowExecution = new ActionIdentifier("swf:DescribeWorkflowExecution");

		// Token: 0x04000897 RID: 2199
		public static readonly ActionIdentifier DescribeWorkflowType = new ActionIdentifier("swf:DescribeWorkflowType");

		// Token: 0x04000898 RID: 2200
		public static readonly ActionIdentifier FailWorkflowExecution = new ActionIdentifier("swf:FailWorkflowExecution");

		// Token: 0x04000899 RID: 2201
		public static readonly ActionIdentifier GetWorkflowExecutionHistory = new ActionIdentifier("swf:GetWorkflowExecutionHistory");

		// Token: 0x0400089A RID: 2202
		public static readonly ActionIdentifier ListActivityTypes = new ActionIdentifier("swf:ListActivityTypes");

		// Token: 0x0400089B RID: 2203
		public static readonly ActionIdentifier ListClosedWorkflowExecutions = new ActionIdentifier("swf:ListClosedWorkflowExecutions");

		// Token: 0x0400089C RID: 2204
		public static readonly ActionIdentifier ListDomains = new ActionIdentifier("swf:ListDomains");

		// Token: 0x0400089D RID: 2205
		public static readonly ActionIdentifier ListOpenWorkflowExecutions = new ActionIdentifier("swf:ListOpenWorkflowExecutions");

		// Token: 0x0400089E RID: 2206
		public static readonly ActionIdentifier ListWorkflowTypes = new ActionIdentifier("swf:ListWorkflowTypes");

		// Token: 0x0400089F RID: 2207
		public static readonly ActionIdentifier PollForActivityTask = new ActionIdentifier("swf:PollForActivityTask");

		// Token: 0x040008A0 RID: 2208
		public static readonly ActionIdentifier PollForDecisionTask = new ActionIdentifier("swf:PollForDecisionTask");

		// Token: 0x040008A1 RID: 2209
		public static readonly ActionIdentifier RecordActivityTaskHeartbeat = new ActionIdentifier("swf:RecordActivityTaskHeartbeat");

		// Token: 0x040008A2 RID: 2210
		public static readonly ActionIdentifier RecordMarker = new ActionIdentifier("swf:RecordMarker");

		// Token: 0x040008A3 RID: 2211
		public static readonly ActionIdentifier RegisterActivityType = new ActionIdentifier("swf:RegisterActivityType");

		// Token: 0x040008A4 RID: 2212
		public static readonly ActionIdentifier RegisterDomain = new ActionIdentifier("swf:RegisterDomain");

		// Token: 0x040008A5 RID: 2213
		public static readonly ActionIdentifier RegisterWorkflowType = new ActionIdentifier("swf:RegisterWorkflowType");

		// Token: 0x040008A6 RID: 2214
		public static readonly ActionIdentifier RequestCancelActivityTask = new ActionIdentifier("swf:RequestCancelActivityTask");

		// Token: 0x040008A7 RID: 2215
		public static readonly ActionIdentifier RequestCancelExternalWorkflowExecution = new ActionIdentifier("swf:RequestCancelExternalWorkflowExecution");

		// Token: 0x040008A8 RID: 2216
		public static readonly ActionIdentifier RequestCancelWorkflowExecution = new ActionIdentifier("swf:RequestCancelWorkflowExecution");

		// Token: 0x040008A9 RID: 2217
		public static readonly ActionIdentifier RespondActivityTaskCanceled = new ActionIdentifier("swf:RespondActivityTaskCanceled");

		// Token: 0x040008AA RID: 2218
		public static readonly ActionIdentifier RespondActivityTaskCompleted = new ActionIdentifier("swf:RespondActivityTaskCompleted");

		// Token: 0x040008AB RID: 2219
		public static readonly ActionIdentifier RespondActivityTaskFailed = new ActionIdentifier("swf:RespondActivityTaskFailed");

		// Token: 0x040008AC RID: 2220
		public static readonly ActionIdentifier RespondDecisionTaskCompleted = new ActionIdentifier("swf:RespondDecisionTaskCompleted");

		// Token: 0x040008AD RID: 2221
		public static readonly ActionIdentifier ScheduleActivityTask = new ActionIdentifier("swf:ScheduleActivityTask");

		// Token: 0x040008AE RID: 2222
		public static readonly ActionIdentifier SignalExternalWorkflowExecution = new ActionIdentifier("swf:SignalExternalWorkflowExecution");

		// Token: 0x040008AF RID: 2223
		public static readonly ActionIdentifier SignalWorkflowExecution = new ActionIdentifier("swf:SignalWorkflowExecution");

		// Token: 0x040008B0 RID: 2224
		public static readonly ActionIdentifier StartChildWorkflowExecution = new ActionIdentifier("swf:StartChildWorkflowExecution");

		// Token: 0x040008B1 RID: 2225
		public static readonly ActionIdentifier StartTimer = new ActionIdentifier("swf:StartTimer");

		// Token: 0x040008B2 RID: 2226
		public static readonly ActionIdentifier StartWorkflowExecution = new ActionIdentifier("swf:StartWorkflowExecution");

		// Token: 0x040008B3 RID: 2227
		public static readonly ActionIdentifier TerminateWorkflowExecution = new ActionIdentifier("swf:TerminateWorkflowExecution");
	}
}
