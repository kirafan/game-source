﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000181 RID: 385
	public static class CloudFrontActionIdentifiers
	{
		// Token: 0x0400058B RID: 1419
		public static readonly ActionIdentifier AllCloudFrontActions = new ActionIdentifier("cloudfront:*");

		// Token: 0x0400058C RID: 1420
		public static readonly ActionIdentifier CreateCloudFrontOriginAccessIdentity = new ActionIdentifier("cloudfront:CreateCloudFrontOriginAccessIdentity");

		// Token: 0x0400058D RID: 1421
		public static readonly ActionIdentifier CreateDistribution = new ActionIdentifier("cloudfront:CreateDistribution");

		// Token: 0x0400058E RID: 1422
		public static readonly ActionIdentifier CreateInvalidation = new ActionIdentifier("cloudfront:CreateInvalidation");

		// Token: 0x0400058F RID: 1423
		public static readonly ActionIdentifier CreateStreamingDistribution = new ActionIdentifier("cloudfront:CreateStreamingDistribution");

		// Token: 0x04000590 RID: 1424
		public static readonly ActionIdentifier DeleteCloudFrontOriginAccessIdentity = new ActionIdentifier("cloudfront:DeleteCloudFrontOriginAccessIdentity");

		// Token: 0x04000591 RID: 1425
		public static readonly ActionIdentifier DeleteDistribution = new ActionIdentifier("cloudfront:DeleteDistribution");

		// Token: 0x04000592 RID: 1426
		public static readonly ActionIdentifier DeleteStreamingDistribution = new ActionIdentifier("cloudfront:DeleteStreamingDistribution");

		// Token: 0x04000593 RID: 1427
		public static readonly ActionIdentifier GetCloudFrontOriginAccessIdentity = new ActionIdentifier("cloudfront:GetCloudFrontOriginAccessIdentity");

		// Token: 0x04000594 RID: 1428
		public static readonly ActionIdentifier GetCloudFrontOriginAccessIdentityConfig = new ActionIdentifier("cloudfront:GetCloudFrontOriginAccessIdentityConfig");

		// Token: 0x04000595 RID: 1429
		public static readonly ActionIdentifier GetDistribution = new ActionIdentifier("cloudfront:GetDistribution");

		// Token: 0x04000596 RID: 1430
		public static readonly ActionIdentifier GetDistributionConfig = new ActionIdentifier("cloudfront:GetDistributionConfig");

		// Token: 0x04000597 RID: 1431
		public static readonly ActionIdentifier GetInvalidation = new ActionIdentifier("cloudfront:GetInvalidation");

		// Token: 0x04000598 RID: 1432
		public static readonly ActionIdentifier GetStreamingDistribution = new ActionIdentifier("cloudfront:GetStreamingDistribution");

		// Token: 0x04000599 RID: 1433
		public static readonly ActionIdentifier GetStreamingDistributionConfig = new ActionIdentifier("cloudfront:GetStreamingDistributionConfig");

		// Token: 0x0400059A RID: 1434
		public static readonly ActionIdentifier ListCloudFrontOriginAccessIdentities = new ActionIdentifier("cloudfront:ListCloudFrontOriginAccessIdentities");

		// Token: 0x0400059B RID: 1435
		public static readonly ActionIdentifier ListDistributions = new ActionIdentifier("cloudfront:ListDistributions");

		// Token: 0x0400059C RID: 1436
		public static readonly ActionIdentifier ListInvalidations = new ActionIdentifier("cloudfront:ListInvalidations");

		// Token: 0x0400059D RID: 1437
		public static readonly ActionIdentifier ListStreamingDistributions = new ActionIdentifier("cloudfront:ListStreamingDistributions");

		// Token: 0x0400059E RID: 1438
		public static readonly ActionIdentifier UpdateCloudFrontOriginAccessIdentity = new ActionIdentifier("cloudfront:UpdateCloudFrontOriginAccessIdentity");

		// Token: 0x0400059F RID: 1439
		public static readonly ActionIdentifier UpdateDistribution = new ActionIdentifier("cloudfront:UpdateDistribution");

		// Token: 0x040005A0 RID: 1440
		public static readonly ActionIdentifier UpdateStreamingDistribution = new ActionIdentifier("cloudfront:UpdateStreamingDistribution");
	}
}
