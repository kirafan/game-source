﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200019C RID: 412
	public static class SecurityTokenServiceActionIdentifiers
	{
		// Token: 0x04000867 RID: 2151
		public static readonly ActionIdentifier AllSecurityTokenServiceActions = new ActionIdentifier("sts:*");

		// Token: 0x04000868 RID: 2152
		public static readonly ActionIdentifier GetFederationToken = new ActionIdentifier("sts:GetFederationToken");

		// Token: 0x04000869 RID: 2153
		public static readonly ActionIdentifier AssumeRole = new ActionIdentifier("sts:AssumeRole");
	}
}
