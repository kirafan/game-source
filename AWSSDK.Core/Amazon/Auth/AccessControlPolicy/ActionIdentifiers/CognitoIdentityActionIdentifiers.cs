﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000186 RID: 390
	public static class CognitoIdentityActionIdentifiers
	{
		// Token: 0x040005DD RID: 1501
		public static readonly ActionIdentifier AllCognitoIdentityActions = new ActionIdentifier("cognito-identity:*");

		// Token: 0x040005DE RID: 1502
		public static readonly ActionIdentifier CreateIdentityPool = new ActionIdentifier("cognito-identity:CreateIdentityPool");

		// Token: 0x040005DF RID: 1503
		public static readonly ActionIdentifier DeleteIdentityPool = new ActionIdentifier("cognito-identity:DeleteIdentityPool");

		// Token: 0x040005E0 RID: 1504
		public static readonly ActionIdentifier DescribeIdentityPool = new ActionIdentifier("cognito-identity:DescribeIdentityPool");

		// Token: 0x040005E1 RID: 1505
		public static readonly ActionIdentifier ListIdentities = new ActionIdentifier("cognito-identity:ListIdentities");

		// Token: 0x040005E2 RID: 1506
		public static readonly ActionIdentifier ListIdentityPools = new ActionIdentifier("cognito-identity:ListIdentityPools");

		// Token: 0x040005E3 RID: 1507
		public static readonly ActionIdentifier UpdateIdentityPool = new ActionIdentifier("cognito-identity:UpdateIdentityPool");
	}
}
