﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000183 RID: 387
	public static class CloudTrailActionIdentifiers
	{
		// Token: 0x040005BA RID: 1466
		public static readonly ActionIdentifier AllCloudTrailActions = new ActionIdentifier("cloudtrail:*");

		// Token: 0x040005BB RID: 1467
		public static readonly ActionIdentifier CreateTrail = new ActionIdentifier("cloudtrail:CreateTrail");

		// Token: 0x040005BC RID: 1468
		public static readonly ActionIdentifier DeleteTrail = new ActionIdentifier("cloudtrail:DeleteTrail");

		// Token: 0x040005BD RID: 1469
		public static readonly ActionIdentifier DescribeTrails = new ActionIdentifier("cloudtrail:DescribeTrails");

		// Token: 0x040005BE RID: 1470
		public static readonly ActionIdentifier GetTrailStatus = new ActionIdentifier("cloudtrail:GetTrailStatus");

		// Token: 0x040005BF RID: 1471
		public static readonly ActionIdentifier StartLogging = new ActionIdentifier("cloudtrail:StartLogging");

		// Token: 0x040005C0 RID: 1472
		public static readonly ActionIdentifier StopLogging = new ActionIdentifier("cloudtrail:StopLogging");

		// Token: 0x040005C1 RID: 1473
		public static readonly ActionIdentifier UpdateTrail = new ActionIdentifier("cloudtrail:UpdateTrail");
	}
}
