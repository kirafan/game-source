﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200018A RID: 394
	public static class EC2ActionIdentifiers
	{
		// Token: 0x04000607 RID: 1543
		public static readonly ActionIdentifier AllEC2Actions = new ActionIdentifier("ec2:*");

		// Token: 0x04000608 RID: 1544
		public static readonly ActionIdentifier AcceptVpcPeeringConnection = new ActionIdentifier("ec2:AcceptVpcPeeringConnection");

		// Token: 0x04000609 RID: 1545
		public static readonly ActionIdentifier ActivateLicense = new ActionIdentifier("ec2:ActivateLicense");

		// Token: 0x0400060A RID: 1546
		public static readonly ActionIdentifier AllocateAddress = new ActionIdentifier("ec2:AllocateAddress");

		// Token: 0x0400060B RID: 1547
		public static readonly ActionIdentifier AssignPrivateIpAddresses = new ActionIdentifier("ec2:AssignPrivateIpAddresses");

		// Token: 0x0400060C RID: 1548
		public static readonly ActionIdentifier AssociateAddress = new ActionIdentifier("ec2:AssociateAddress");

		// Token: 0x0400060D RID: 1549
		public static readonly ActionIdentifier AssociateDhcpOptions = new ActionIdentifier("ec2:AssociateDhcpOptions");

		// Token: 0x0400060E RID: 1550
		public static readonly ActionIdentifier AssociateRouteTable = new ActionIdentifier("ec2:AssociateRouteTable");

		// Token: 0x0400060F RID: 1551
		public static readonly ActionIdentifier AttachInternetGateway = new ActionIdentifier("ec2:AttachInternetGateway");

		// Token: 0x04000610 RID: 1552
		public static readonly ActionIdentifier AttachNetworkInterface = new ActionIdentifier("ec2:AttachNetworkInterface");

		// Token: 0x04000611 RID: 1553
		public static readonly ActionIdentifier AttachVolume = new ActionIdentifier("ec2:AttachVolume");

		// Token: 0x04000612 RID: 1554
		public static readonly ActionIdentifier AttachVpnGateway = new ActionIdentifier("ec2:AttachVpnGateway");

		// Token: 0x04000613 RID: 1555
		public static readonly ActionIdentifier AuthorizeSecurityGroupEgress = new ActionIdentifier("ec2:AuthorizeSecurityGroupEgress");

		// Token: 0x04000614 RID: 1556
		public static readonly ActionIdentifier AuthorizeSecurityGroupIngress = new ActionIdentifier("ec2:AuthorizeSecurityGroupIngress");

		// Token: 0x04000615 RID: 1557
		public static readonly ActionIdentifier BundleInstance = new ActionIdentifier("ec2:BundleInstance");

		// Token: 0x04000616 RID: 1558
		public static readonly ActionIdentifier CancelBundleTask = new ActionIdentifier("ec2:CancelBundleTask");

		// Token: 0x04000617 RID: 1559
		public static readonly ActionIdentifier CancelConversionTask = new ActionIdentifier("ec2:CancelConversionTask");

		// Token: 0x04000618 RID: 1560
		public static readonly ActionIdentifier CancelExportTask = new ActionIdentifier("ec2:CancelExportTask");

		// Token: 0x04000619 RID: 1561
		public static readonly ActionIdentifier CancelReservedInstancesListing = new ActionIdentifier("ec2:CancelReservedInstancesListing");

		// Token: 0x0400061A RID: 1562
		public static readonly ActionIdentifier CancelSpotInstanceRequests = new ActionIdentifier("ec2:CancelSpotInstanceRequests");

		// Token: 0x0400061B RID: 1563
		public static readonly ActionIdentifier ConfirmProductInstance = new ActionIdentifier("ec2:ConfirmProductInstance");

		// Token: 0x0400061C RID: 1564
		public static readonly ActionIdentifier CopyImage = new ActionIdentifier("ec2:CopyImage");

		// Token: 0x0400061D RID: 1565
		public static readonly ActionIdentifier CopySnapshot = new ActionIdentifier("ec2:CopySnapshot");

		// Token: 0x0400061E RID: 1566
		public static readonly ActionIdentifier CreateCustomerGateway = new ActionIdentifier("ec2:CreateCustomerGateway");

		// Token: 0x0400061F RID: 1567
		public static readonly ActionIdentifier CreateDhcpOptions = new ActionIdentifier("ec2:CreateDhcpOptions");

		// Token: 0x04000620 RID: 1568
		public static readonly ActionIdentifier CreateImage = new ActionIdentifier("ec2:CreateImage");

		// Token: 0x04000621 RID: 1569
		public static readonly ActionIdentifier CreateInstanceExportTask = new ActionIdentifier("ec2:CreateInstanceExportTask");

		// Token: 0x04000622 RID: 1570
		public static readonly ActionIdentifier CreateInternetGateway = new ActionIdentifier("ec2:CreateInternetGateway");

		// Token: 0x04000623 RID: 1571
		public static readonly ActionIdentifier CreateKeyPair = new ActionIdentifier("ec2:CreateKeyPair");

		// Token: 0x04000624 RID: 1572
		public static readonly ActionIdentifier CreateNetworkAcl = new ActionIdentifier("ec2:CreateNetworkAcl");

		// Token: 0x04000625 RID: 1573
		public static readonly ActionIdentifier CreateNetworkAclEntry = new ActionIdentifier("ec2:CreateNetworkAclEntry");

		// Token: 0x04000626 RID: 1574
		public static readonly ActionIdentifier CreateNetworkInterface = new ActionIdentifier("ec2:CreateNetworkInterface");

		// Token: 0x04000627 RID: 1575
		public static readonly ActionIdentifier CreatePlacementGroup = new ActionIdentifier("ec2:CreatePlacementGroup");

		// Token: 0x04000628 RID: 1576
		public static readonly ActionIdentifier CreateReservedInstancesListing = new ActionIdentifier("ec2:CreateReservedInstancesListing");

		// Token: 0x04000629 RID: 1577
		public static readonly ActionIdentifier CreateRoute = new ActionIdentifier("ec2:CreateRoute");

		// Token: 0x0400062A RID: 1578
		public static readonly ActionIdentifier CreateRouteTable = new ActionIdentifier("ec2:CreateRouteTable");

		// Token: 0x0400062B RID: 1579
		public static readonly ActionIdentifier CreateSecurityGroup = new ActionIdentifier("ec2:CreateSecurityGroup");

		// Token: 0x0400062C RID: 1580
		public static readonly ActionIdentifier CreateSnapshot = new ActionIdentifier("ec2:CreateSnapshot");

		// Token: 0x0400062D RID: 1581
		public static readonly ActionIdentifier CreateSpotDatafeedSubscription = new ActionIdentifier("ec2:CreateSpotDatafeedSubscription");

		// Token: 0x0400062E RID: 1582
		public static readonly ActionIdentifier CreateSubnet = new ActionIdentifier("ec2:CreateSubnet");

		// Token: 0x0400062F RID: 1583
		public static readonly ActionIdentifier CreateTags = new ActionIdentifier("ec2:CreateTags");

		// Token: 0x04000630 RID: 1584
		public static readonly ActionIdentifier CreateVolume = new ActionIdentifier("ec2:CreateVolume");

		// Token: 0x04000631 RID: 1585
		public static readonly ActionIdentifier CreateVpc = new ActionIdentifier("ec2:CreateVpc");

		// Token: 0x04000632 RID: 1586
		public static readonly ActionIdentifier CreateVpcPeeringConnection = new ActionIdentifier("ec2:CreateVpcPeeringConnection");

		// Token: 0x04000633 RID: 1587
		public static readonly ActionIdentifier CreateVpnConnection = new ActionIdentifier("ec2:CreateVpnConnection");

		// Token: 0x04000634 RID: 1588
		public static readonly ActionIdentifier CreateVpnConnectionRoute = new ActionIdentifier("ec2:CreateVpnConnectionRoute");

		// Token: 0x04000635 RID: 1589
		public static readonly ActionIdentifier CreateVpnGateway = new ActionIdentifier("ec2:CreateVpnGateway");

		// Token: 0x04000636 RID: 1590
		public static readonly ActionIdentifier DeactivateLicense = new ActionIdentifier("ec2:DeactivateLicense");

		// Token: 0x04000637 RID: 1591
		public static readonly ActionIdentifier DeleteCustomerGateway = new ActionIdentifier("ec2:DeleteCustomerGateway");

		// Token: 0x04000638 RID: 1592
		public static readonly ActionIdentifier DeleteDhcpOptions = new ActionIdentifier("ec2:DeleteDhcpOptions");

		// Token: 0x04000639 RID: 1593
		public static readonly ActionIdentifier DeleteInternetGateway = new ActionIdentifier("ec2:DeleteInternetGateway");

		// Token: 0x0400063A RID: 1594
		public static readonly ActionIdentifier DeleteKeyPair = new ActionIdentifier("ec2:DeleteKeyPair");

		// Token: 0x0400063B RID: 1595
		public static readonly ActionIdentifier DeleteNetworkAcl = new ActionIdentifier("ec2:DeleteNetworkAcl");

		// Token: 0x0400063C RID: 1596
		public static readonly ActionIdentifier DeleteNetworkAclEntry = new ActionIdentifier("ec2:DeleteNetworkAclEntry");

		// Token: 0x0400063D RID: 1597
		public static readonly ActionIdentifier DeleteNetworkInterface = new ActionIdentifier("ec2:DeleteNetworkInterface");

		// Token: 0x0400063E RID: 1598
		public static readonly ActionIdentifier DeletePlacementGroup = new ActionIdentifier("ec2:DeletePlacementGroup");

		// Token: 0x0400063F RID: 1599
		public static readonly ActionIdentifier DeleteRoute = new ActionIdentifier("ec2:DeleteRoute");

		// Token: 0x04000640 RID: 1600
		public static readonly ActionIdentifier DeleteRouteTable = new ActionIdentifier("ec2:DeleteRouteTable");

		// Token: 0x04000641 RID: 1601
		public static readonly ActionIdentifier DeleteSecurityGroup = new ActionIdentifier("ec2:DeleteSecurityGroup");

		// Token: 0x04000642 RID: 1602
		public static readonly ActionIdentifier DeleteSnapshot = new ActionIdentifier("ec2:DeleteSnapshot");

		// Token: 0x04000643 RID: 1603
		public static readonly ActionIdentifier DeleteSpotDatafeedSubscription = new ActionIdentifier("ec2:DeleteSpotDatafeedSubscription");

		// Token: 0x04000644 RID: 1604
		public static readonly ActionIdentifier DeleteSubnet = new ActionIdentifier("ec2:DeleteSubnet");

		// Token: 0x04000645 RID: 1605
		public static readonly ActionIdentifier DeleteTags = new ActionIdentifier("ec2:DeleteTags");

		// Token: 0x04000646 RID: 1606
		public static readonly ActionIdentifier DeleteVolume = new ActionIdentifier("ec2:DeleteVolume");

		// Token: 0x04000647 RID: 1607
		public static readonly ActionIdentifier DeleteVpc = new ActionIdentifier("ec2:DeleteVpc");

		// Token: 0x04000648 RID: 1608
		public static readonly ActionIdentifier DeleteVpcPeeringConnection = new ActionIdentifier("ec2:DeleteVpcPeeringConnection");

		// Token: 0x04000649 RID: 1609
		public static readonly ActionIdentifier DeleteVpnConnection = new ActionIdentifier("ec2:DeleteVpnConnection");

		// Token: 0x0400064A RID: 1610
		public static readonly ActionIdentifier DeleteVpnConnectionRoute = new ActionIdentifier("ec2:DeleteVpnConnectionRoute");

		// Token: 0x0400064B RID: 1611
		public static readonly ActionIdentifier DeleteVpnGateway = new ActionIdentifier("ec2:DeleteVpnGateway");

		// Token: 0x0400064C RID: 1612
		public static readonly ActionIdentifier DeregisterImage = new ActionIdentifier("ec2:DeregisterImage");

		// Token: 0x0400064D RID: 1613
		public static readonly ActionIdentifier DescribeAccountAttributes = new ActionIdentifier("ec2:DescribeAccountAttributes");

		// Token: 0x0400064E RID: 1614
		public static readonly ActionIdentifier DescribeAddresses = new ActionIdentifier("ec2:DescribeAddresses");

		// Token: 0x0400064F RID: 1615
		public static readonly ActionIdentifier DescribeAvailabilityZones = new ActionIdentifier("ec2:DescribeAvailabilityZones");

		// Token: 0x04000650 RID: 1616
		public static readonly ActionIdentifier DescribeBundleTasks = new ActionIdentifier("ec2:DescribeBundleTasks");

		// Token: 0x04000651 RID: 1617
		public static readonly ActionIdentifier DescribeConversionTasks = new ActionIdentifier("ec2:DescribeConversionTasks");

		// Token: 0x04000652 RID: 1618
		public static readonly ActionIdentifier DescribeCustomerGateways = new ActionIdentifier("ec2:DescribeCustomerGateways");

		// Token: 0x04000653 RID: 1619
		public static readonly ActionIdentifier DescribeDhcpOptions = new ActionIdentifier("ec2:DescribeDhcpOptions");

		// Token: 0x04000654 RID: 1620
		public static readonly ActionIdentifier DescribeExportTasks = new ActionIdentifier("ec2:DescribeExportTasks");

		// Token: 0x04000655 RID: 1621
		public static readonly ActionIdentifier DescribeImageAttribute = new ActionIdentifier("ec2:DescribeImageAttribute");

		// Token: 0x04000656 RID: 1622
		public static readonly ActionIdentifier DescribeImages = new ActionIdentifier("ec2:DescribeImages");

		// Token: 0x04000657 RID: 1623
		public static readonly ActionIdentifier DescribeInstanceAttribute = new ActionIdentifier("ec2:DescribeInstanceAttribute");

		// Token: 0x04000658 RID: 1624
		public static readonly ActionIdentifier DescribeInstanceStatus = new ActionIdentifier("ec2:DescribeInstanceStatus");

		// Token: 0x04000659 RID: 1625
		public static readonly ActionIdentifier DescribeInstances = new ActionIdentifier("ec2:DescribeInstances");

		// Token: 0x0400065A RID: 1626
		public static readonly ActionIdentifier DescribeInternetGateways = new ActionIdentifier("ec2:DescribeInternetGateways");

		// Token: 0x0400065B RID: 1627
		public static readonly ActionIdentifier DescribeKeyPairs = new ActionIdentifier("ec2:DescribeKeyPairs");

		// Token: 0x0400065C RID: 1628
		public static readonly ActionIdentifier DescribeLicenses = new ActionIdentifier("ec2:DescribeLicenses");

		// Token: 0x0400065D RID: 1629
		public static readonly ActionIdentifier DescribeNetworkAcls = new ActionIdentifier("ec2:DescribeNetworkAcls");

		// Token: 0x0400065E RID: 1630
		public static readonly ActionIdentifier DescribeNetworkInterfaceAttribute = new ActionIdentifier("ec2:DescribeNetworkInterfaceAttribute");

		// Token: 0x0400065F RID: 1631
		public static readonly ActionIdentifier DescribeNetworkInterfaces = new ActionIdentifier("ec2:DescribeNetworkInterfaces");

		// Token: 0x04000660 RID: 1632
		public static readonly ActionIdentifier DescribePlacementGroups = new ActionIdentifier("ec2:DescribePlacementGroups");

		// Token: 0x04000661 RID: 1633
		public static readonly ActionIdentifier DescribeRegions = new ActionIdentifier("ec2:DescribeRegions");

		// Token: 0x04000662 RID: 1634
		public static readonly ActionIdentifier DescribeReservedInstances = new ActionIdentifier("ec2:DescribeReservedInstances");

		// Token: 0x04000663 RID: 1635
		public static readonly ActionIdentifier DescribeReservedInstancesListings = new ActionIdentifier("ec2:DescribeReservedInstancesListings");

		// Token: 0x04000664 RID: 1636
		public static readonly ActionIdentifier DescribeReservedInstancesModifications = new ActionIdentifier("ec2:DescribeReservedInstancesModifications");

		// Token: 0x04000665 RID: 1637
		public static readonly ActionIdentifier DescribeReservedInstancesOfferings = new ActionIdentifier("ec2:DescribeReservedInstancesOfferings");

		// Token: 0x04000666 RID: 1638
		public static readonly ActionIdentifier DescribeRouteTables = new ActionIdentifier("ec2:DescribeRouteTables");

		// Token: 0x04000667 RID: 1639
		public static readonly ActionIdentifier DescribeSecurityGroups = new ActionIdentifier("ec2:DescribeSecurityGroups");

		// Token: 0x04000668 RID: 1640
		public static readonly ActionIdentifier DescribeSnapshotAttribute = new ActionIdentifier("ec2:DescribeSnapshotAttribute");

		// Token: 0x04000669 RID: 1641
		public static readonly ActionIdentifier DescribeSnapshots = new ActionIdentifier("ec2:DescribeSnapshots");

		// Token: 0x0400066A RID: 1642
		public static readonly ActionIdentifier DescribeSpotDatafeedSubscription = new ActionIdentifier("ec2:DescribeSpotDatafeedSubscription");

		// Token: 0x0400066B RID: 1643
		public static readonly ActionIdentifier DescribeSpotInstanceRequests = new ActionIdentifier("ec2:DescribeSpotInstanceRequests");

		// Token: 0x0400066C RID: 1644
		public static readonly ActionIdentifier DescribeSpotPriceHistory = new ActionIdentifier("ec2:DescribeSpotPriceHistory");

		// Token: 0x0400066D RID: 1645
		public static readonly ActionIdentifier DescribeSubnets = new ActionIdentifier("ec2:DescribeSubnets");

		// Token: 0x0400066E RID: 1646
		public static readonly ActionIdentifier DescribeTags = new ActionIdentifier("ec2:DescribeTags");

		// Token: 0x0400066F RID: 1647
		public static readonly ActionIdentifier DescribeVolumeAttribute = new ActionIdentifier("ec2:DescribeVolumeAttribute");

		// Token: 0x04000670 RID: 1648
		public static readonly ActionIdentifier DescribeVolumeStatus = new ActionIdentifier("ec2:DescribeVolumeStatus");

		// Token: 0x04000671 RID: 1649
		public static readonly ActionIdentifier DescribeVolumes = new ActionIdentifier("ec2:DescribeVolumes");

		// Token: 0x04000672 RID: 1650
		public static readonly ActionIdentifier DescribeVpcAttribute = new ActionIdentifier("ec2:DescribeVpcAttribute");

		// Token: 0x04000673 RID: 1651
		public static readonly ActionIdentifier DescribeVpcs = new ActionIdentifier("ec2:DescribeVpcs");

		// Token: 0x04000674 RID: 1652
		public static readonly ActionIdentifier DescribeVpcPeeringConnection = new ActionIdentifier("ec2:DescribeVpcPeeringConnection");

		// Token: 0x04000675 RID: 1653
		public static readonly ActionIdentifier DescribeVpnConnections = new ActionIdentifier("ec2:DescribeVpnConnections");

		// Token: 0x04000676 RID: 1654
		public static readonly ActionIdentifier DescribeVpnGateways = new ActionIdentifier("ec2:DescribeVpnGateways");

		// Token: 0x04000677 RID: 1655
		public static readonly ActionIdentifier DetachInternetGateway = new ActionIdentifier("ec2:DetachInternetGateway");

		// Token: 0x04000678 RID: 1656
		public static readonly ActionIdentifier DetachNetworkInterface = new ActionIdentifier("ec2:DetachNetworkInterface");

		// Token: 0x04000679 RID: 1657
		public static readonly ActionIdentifier DetachVolume = new ActionIdentifier("ec2:DetachVolume");

		// Token: 0x0400067A RID: 1658
		public static readonly ActionIdentifier DetachVpnGateway = new ActionIdentifier("ec2:DetachVpnGateway");

		// Token: 0x0400067B RID: 1659
		public static readonly ActionIdentifier DisableVgwRoutePropagation = new ActionIdentifier("ec2:DisableVgwRoutePropagation");

		// Token: 0x0400067C RID: 1660
		public static readonly ActionIdentifier DisassociateAddress = new ActionIdentifier("ec2:DisassociateAddress");

		// Token: 0x0400067D RID: 1661
		public static readonly ActionIdentifier DisassociateRouteTable = new ActionIdentifier("ec2:DisassociateRouteTable");

		// Token: 0x0400067E RID: 1662
		public static readonly ActionIdentifier EnableVgwRoutePropagation = new ActionIdentifier("ec2:EnableVgwRoutePropagation");

		// Token: 0x0400067F RID: 1663
		public static readonly ActionIdentifier EnableVolumeIO = new ActionIdentifier("ec2:EnableVolumeIO");

		// Token: 0x04000680 RID: 1664
		public static readonly ActionIdentifier GetConsoleOutput = new ActionIdentifier("ec2:GetConsoleOutput");

		// Token: 0x04000681 RID: 1665
		public static readonly ActionIdentifier GetPasswordData = new ActionIdentifier("ec2:GetPasswordData");

		// Token: 0x04000682 RID: 1666
		public static readonly ActionIdentifier ImportInstance = new ActionIdentifier("ec2:ImportInstance");

		// Token: 0x04000683 RID: 1667
		public static readonly ActionIdentifier ImportKeyPair = new ActionIdentifier("ec2:ImportKeyPair");

		// Token: 0x04000684 RID: 1668
		public static readonly ActionIdentifier ImportVolume = new ActionIdentifier("ec2:ImportVolume");

		// Token: 0x04000685 RID: 1669
		public static readonly ActionIdentifier ModifyImageAttribute = new ActionIdentifier("ec2:ModifyImageAttribute");

		// Token: 0x04000686 RID: 1670
		public static readonly ActionIdentifier ModifyInstanceAttribute = new ActionIdentifier("ec2:ModifyInstanceAttribute");

		// Token: 0x04000687 RID: 1671
		public static readonly ActionIdentifier ModifyNetworkInterfaceAttribute = new ActionIdentifier("ec2:ModifyNetworkInterfaceAttribute");

		// Token: 0x04000688 RID: 1672
		public static readonly ActionIdentifier ModifyReservedInstances = new ActionIdentifier("ec2:ModifyReservedInstances");

		// Token: 0x04000689 RID: 1673
		public static readonly ActionIdentifier ModifySnapshotAttribute = new ActionIdentifier("ec2:ModifySnapshotAttribute");

		// Token: 0x0400068A RID: 1674
		public static readonly ActionIdentifier ModifyVolumeAttribute = new ActionIdentifier("ec2:ModifyVolumeAttribute");

		// Token: 0x0400068B RID: 1675
		public static readonly ActionIdentifier ModifyVpcAttribute = new ActionIdentifier("ec2:ModifyVpcAttribute");

		// Token: 0x0400068C RID: 1676
		public static readonly ActionIdentifier MonitorInstances = new ActionIdentifier("ec2:MonitorInstances");

		// Token: 0x0400068D RID: 1677
		public static readonly ActionIdentifier PurchaseReservedInstancesOffering = new ActionIdentifier("ec2:PurchaseReservedInstancesOffering");

		// Token: 0x0400068E RID: 1678
		public static readonly ActionIdentifier RebootInstances = new ActionIdentifier("ec2:RebootInstances");

		// Token: 0x0400068F RID: 1679
		public static readonly ActionIdentifier RegisterImage = new ActionIdentifier("ec2:RegisterImage");

		// Token: 0x04000690 RID: 1680
		public static readonly ActionIdentifier RejectVpcPeeringConnection = new ActionIdentifier("ec2:RejectVpcPeeringConnection");

		// Token: 0x04000691 RID: 1681
		public static readonly ActionIdentifier ReleaseAddress = new ActionIdentifier("ec2:ReleaseAddress");

		// Token: 0x04000692 RID: 1682
		public static readonly ActionIdentifier ReplaceNetworkAclAssociation = new ActionIdentifier("ec2:ReplaceNetworkAclAssociation");

		// Token: 0x04000693 RID: 1683
		public static readonly ActionIdentifier ReplaceNetworkAclEntry = new ActionIdentifier("ec2:ReplaceNetworkAclEntry");

		// Token: 0x04000694 RID: 1684
		public static readonly ActionIdentifier ReplaceRoute = new ActionIdentifier("ec2:ReplaceRoute");

		// Token: 0x04000695 RID: 1685
		public static readonly ActionIdentifier ReplaceRouteTableAssociation = new ActionIdentifier("ec2:ReplaceRouteTableAssociation");

		// Token: 0x04000696 RID: 1686
		public static readonly ActionIdentifier ReportInstanceStatus = new ActionIdentifier("ec2:ReportInstanceStatus");

		// Token: 0x04000697 RID: 1687
		public static readonly ActionIdentifier RequestSpotInstances = new ActionIdentifier("ec2:RequestSpotInstances");

		// Token: 0x04000698 RID: 1688
		public static readonly ActionIdentifier ResetImageAttribute = new ActionIdentifier("ec2:ResetImageAttribute");

		// Token: 0x04000699 RID: 1689
		public static readonly ActionIdentifier ResetInstanceAttribute = new ActionIdentifier("ec2:ResetInstanceAttribute");

		// Token: 0x0400069A RID: 1690
		public static readonly ActionIdentifier ResetNetworkInterfaceAttribute = new ActionIdentifier("ec2:ResetNetworkInterfaceAttribute");

		// Token: 0x0400069B RID: 1691
		public static readonly ActionIdentifier ResetSnapshotAttribute = new ActionIdentifier("ec2:ResetSnapshotAttribute");

		// Token: 0x0400069C RID: 1692
		public static readonly ActionIdentifier RevokeSecurityGroupEgress = new ActionIdentifier("ec2:RevokeSecurityGroupEgress");

		// Token: 0x0400069D RID: 1693
		public static readonly ActionIdentifier RevokeSecurityGroupIngress = new ActionIdentifier("ec2:RevokeSecurityGroupIngress");

		// Token: 0x0400069E RID: 1694
		public static readonly ActionIdentifier RunInstances = new ActionIdentifier("ec2:RunInstances");

		// Token: 0x0400069F RID: 1695
		public static readonly ActionIdentifier StartInstances = new ActionIdentifier("ec2:StartInstances");

		// Token: 0x040006A0 RID: 1696
		public static readonly ActionIdentifier StopInstances = new ActionIdentifier("ec2:StopInstances");

		// Token: 0x040006A1 RID: 1697
		public static readonly ActionIdentifier TerminateInstances = new ActionIdentifier("ec2:TerminateInstances");

		// Token: 0x040006A2 RID: 1698
		public static readonly ActionIdentifier UnassignPrivateIpAddresses = new ActionIdentifier("ec2:UnassignPrivateIpAddresses");

		// Token: 0x040006A3 RID: 1699
		public static readonly ActionIdentifier UnmonitorInstances = new ActionIdentifier("ec2:UnmonitorInstances");
	}
}
