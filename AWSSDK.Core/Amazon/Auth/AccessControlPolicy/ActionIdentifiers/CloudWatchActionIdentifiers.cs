﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000184 RID: 388
	public static class CloudWatchActionIdentifiers
	{
		// Token: 0x040005C2 RID: 1474
		public static readonly ActionIdentifier AllCloudWatchActions = new ActionIdentifier("cloudwatch:*");

		// Token: 0x040005C3 RID: 1475
		public static readonly ActionIdentifier DeleteAlarms = new ActionIdentifier("cloudwatch:DeleteAlarms");

		// Token: 0x040005C4 RID: 1476
		public static readonly ActionIdentifier DescribeAlarmHistory = new ActionIdentifier("cloudwatch:DescribeAlarmHistory");

		// Token: 0x040005C5 RID: 1477
		public static readonly ActionIdentifier DescribeAlarms = new ActionIdentifier("cloudwatch:DescribeAlarms");

		// Token: 0x040005C6 RID: 1478
		public static readonly ActionIdentifier DescribeAlarmsForMetric = new ActionIdentifier("cloudwatch:DescribeAlarmsForMetric");

		// Token: 0x040005C7 RID: 1479
		public static readonly ActionIdentifier DisableAlarmActions = new ActionIdentifier("cloudwatch:DisableAlarmActions");

		// Token: 0x040005C8 RID: 1480
		public static readonly ActionIdentifier EnableAlarmActions = new ActionIdentifier("cloudwatch:EnableAlarmActions");

		// Token: 0x040005C9 RID: 1481
		public static readonly ActionIdentifier GetMetricStatistics = new ActionIdentifier("cloudwatch:GetMetricStatistics");

		// Token: 0x040005CA RID: 1482
		public static readonly ActionIdentifier ListMetrics = new ActionIdentifier("cloudwatch:ListMetrics");

		// Token: 0x040005CB RID: 1483
		public static readonly ActionIdentifier PutMetricAlarm = new ActionIdentifier("cloudwatch:PutMetricAlarm");

		// Token: 0x040005CC RID: 1484
		public static readonly ActionIdentifier PutMetricData = new ActionIdentifier("cloudwatch:PutMetricData");

		// Token: 0x040005CD RID: 1485
		public static readonly ActionIdentifier SetAlarmState = new ActionIdentifier("cloudwatch:SetAlarmState");
	}
}
