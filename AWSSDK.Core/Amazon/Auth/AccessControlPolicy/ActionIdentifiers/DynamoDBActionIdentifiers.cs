﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000189 RID: 393
	public static class DynamoDBActionIdentifiers
	{
		// Token: 0x040005F9 RID: 1529
		public static readonly ActionIdentifier AllDynamoDBActions = new ActionIdentifier("dynamodb:*");

		// Token: 0x040005FA RID: 1530
		public static readonly ActionIdentifier BatchGetItem = new ActionIdentifier("dynamodb:BatchGetItem");

		// Token: 0x040005FB RID: 1531
		public static readonly ActionIdentifier BatchWriteItem = new ActionIdentifier("dynamodb:BatchWriteItem");

		// Token: 0x040005FC RID: 1532
		public static readonly ActionIdentifier CreateTable = new ActionIdentifier("dynamodb:CreateTable");

		// Token: 0x040005FD RID: 1533
		public static readonly ActionIdentifier DeleteItem = new ActionIdentifier("dynamodb:DeleteItem");

		// Token: 0x040005FE RID: 1534
		public static readonly ActionIdentifier DeleteTable = new ActionIdentifier("dynamodb:DeleteTable");

		// Token: 0x040005FF RID: 1535
		public static readonly ActionIdentifier DescribeTable = new ActionIdentifier("dynamodb:DescribeTable");

		// Token: 0x04000600 RID: 1536
		public static readonly ActionIdentifier GetItem = new ActionIdentifier("dynamodb:GetItem");

		// Token: 0x04000601 RID: 1537
		public static readonly ActionIdentifier ListTables = new ActionIdentifier("dynamodb:ListTables");

		// Token: 0x04000602 RID: 1538
		public static readonly ActionIdentifier PutItem = new ActionIdentifier("dynamodb:PutItem");

		// Token: 0x04000603 RID: 1539
		public static readonly ActionIdentifier Query = new ActionIdentifier("dynamodb:Query");

		// Token: 0x04000604 RID: 1540
		public static readonly ActionIdentifier Scan = new ActionIdentifier("dynamodb:Scan");

		// Token: 0x04000605 RID: 1541
		public static readonly ActionIdentifier UpdateItem = new ActionIdentifier("dynamodb:UpdateItem");

		// Token: 0x04000606 RID: 1542
		public static readonly ActionIdentifier UpdateTable = new ActionIdentifier("dynamodb:UpdateTable");
	}
}
