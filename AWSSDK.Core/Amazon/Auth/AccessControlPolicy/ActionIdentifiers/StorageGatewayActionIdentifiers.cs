﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x020001A2 RID: 418
	public static class StorageGatewayActionIdentifiers
	{
		// Token: 0x040008DB RID: 2267
		public static readonly ActionIdentifier AllStorageGatewayActions = new ActionIdentifier("storagegateway:*");

		// Token: 0x040008DC RID: 2268
		public static readonly ActionIdentifier ActivateGateway = new ActionIdentifier("storagegateway:ActivateGateway");

		// Token: 0x040008DD RID: 2269
		public static readonly ActionIdentifier AddCache = new ActionIdentifier("storagegateway:AddCache");

		// Token: 0x040008DE RID: 2270
		public static readonly ActionIdentifier AddUploadBuffer = new ActionIdentifier("storagegateway:AddUploadBuffer");

		// Token: 0x040008DF RID: 2271
		public static readonly ActionIdentifier AddWorkingStorage = new ActionIdentifier("storagegateway:AddWorkingStorage");

		// Token: 0x040008E0 RID: 2272
		public static readonly ActionIdentifier CancelArchival = new ActionIdentifier("storagegateway:CancelArchival");

		// Token: 0x040008E1 RID: 2273
		public static readonly ActionIdentifier CancelRetrieval = new ActionIdentifier("storagegateway:CancelRetrieval");

		// Token: 0x040008E2 RID: 2274
		public static readonly ActionIdentifier CreateCachediSCSIVolume = new ActionIdentifier("storagegateway:CreateCachediSCSIVolume");

		// Token: 0x040008E3 RID: 2275
		public static readonly ActionIdentifier CreateSnapshot = new ActionIdentifier("storagegateway:CreateSnapshot");

		// Token: 0x040008E4 RID: 2276
		public static readonly ActionIdentifier CreateSnapshotFromVolumeRecoveryPoint = new ActionIdentifier("storagegateway:CreateSnapshotFromVolumeRecoveryPoint");

		// Token: 0x040008E5 RID: 2277
		public static readonly ActionIdentifier CreateStorediSCSIVolume = new ActionIdentifier("storagegateway:CreateStorediSCSIVolume");

		// Token: 0x040008E6 RID: 2278
		public static readonly ActionIdentifier CreateTapes = new ActionIdentifier("storagegateway:CreateTapes");

		// Token: 0x040008E7 RID: 2279
		public static readonly ActionIdentifier DeleteBandwidthRateLimit = new ActionIdentifier("storagegateway:DeleteBandwidthRateLimit");

		// Token: 0x040008E8 RID: 2280
		public static readonly ActionIdentifier DeleteChapCredentials = new ActionIdentifier("storagegateway:DeleteChapCredentials");

		// Token: 0x040008E9 RID: 2281
		public static readonly ActionIdentifier DeleteGateway = new ActionIdentifier("storagegateway:DeleteGateway");

		// Token: 0x040008EA RID: 2282
		public static readonly ActionIdentifier DeleteSnapshotSchedule = new ActionIdentifier("storagegateway:DeleteSnapshotSchedule");

		// Token: 0x040008EB RID: 2283
		public static readonly ActionIdentifier DeleteTape = new ActionIdentifier("storagegateway:DeleteTape");

		// Token: 0x040008EC RID: 2284
		public static readonly ActionIdentifier DeleteTapeArchive = new ActionIdentifier("storagegateway:DeleteTapeArchive");

		// Token: 0x040008ED RID: 2285
		public static readonly ActionIdentifier DeleteVolume = new ActionIdentifier("storagegateway:DeleteVolume");

		// Token: 0x040008EE RID: 2286
		public static readonly ActionIdentifier DescribeBandwidthRateLimit = new ActionIdentifier("storagegateway:DescribeBandwidthRateLimit");

		// Token: 0x040008EF RID: 2287
		public static readonly ActionIdentifier DescribeCache = new ActionIdentifier("storagegateway:DescribeCache");

		// Token: 0x040008F0 RID: 2288
		public static readonly ActionIdentifier DescribeCachediSCSIVolumes = new ActionIdentifier("storagegateway:DescribeCachediSCSIVolumes");

		// Token: 0x040008F1 RID: 2289
		public static readonly ActionIdentifier DescribeChapCredentials = new ActionIdentifier("storagegateway:DescribeChapCredentials");

		// Token: 0x040008F2 RID: 2290
		public static readonly ActionIdentifier DescribeGatewayInformation = new ActionIdentifier("storagegateway:DescribeGatewayInformation");

		// Token: 0x040008F3 RID: 2291
		public static readonly ActionIdentifier DescribeMaintenanceStartTime = new ActionIdentifier("storagegateway:DescribeMaintenanceStartTime");

		// Token: 0x040008F4 RID: 2292
		public static readonly ActionIdentifier DescribeSnapshotSchedule = new ActionIdentifier("storagegateway:DescribeSnapshotSchedule");

		// Token: 0x040008F5 RID: 2293
		public static readonly ActionIdentifier DescribeStorediSCSIVolumes = new ActionIdentifier("storagegateway:DescribeStorediSCSIVolumes");

		// Token: 0x040008F6 RID: 2294
		public static readonly ActionIdentifier DescribeTapeArchives = new ActionIdentifier("storagegateway:DescribeTapeArchives");

		// Token: 0x040008F7 RID: 2295
		public static readonly ActionIdentifier DescribeTapeRecoveryPoints = new ActionIdentifier("storagegateway:DescribeTapeRecoveryPoints");

		// Token: 0x040008F8 RID: 2296
		public static readonly ActionIdentifier DescribeTapes = new ActionIdentifier("storagegateway:DescribeTapes");

		// Token: 0x040008F9 RID: 2297
		public static readonly ActionIdentifier DescribeUploadBuffer = new ActionIdentifier("storagegateway:DescribeUploadBuffer");

		// Token: 0x040008FA RID: 2298
		public static readonly ActionIdentifier DescribeVTLDevices = new ActionIdentifier("storagegateway:DescribeVTLDevices");

		// Token: 0x040008FB RID: 2299
		public static readonly ActionIdentifier DescribeWorkingStorage = new ActionIdentifier("storagegateway:DescribeWorkingStorage");

		// Token: 0x040008FC RID: 2300
		public static readonly ActionIdentifier DisableGateway = new ActionIdentifier("storagegateway:DisableGateway");

		// Token: 0x040008FD RID: 2301
		public static readonly ActionIdentifier ListGateways = new ActionIdentifier("storagegateway:ListGateways");

		// Token: 0x040008FE RID: 2302
		public static readonly ActionIdentifier ListLocalDisks = new ActionIdentifier("storagegateway:ListLocalDisks");

		// Token: 0x040008FF RID: 2303
		public static readonly ActionIdentifier ListVolumeRecoveryPoints = new ActionIdentifier("storagegateway:ListVolumeRecoveryPoints");

		// Token: 0x04000900 RID: 2304
		public static readonly ActionIdentifier ListVolumes = new ActionIdentifier("storagegateway:ListVolumes");

		// Token: 0x04000901 RID: 2305
		public static readonly ActionIdentifier RetrieveTapeArchive = new ActionIdentifier("storagegateway:RetrieveTapeArchive");

		// Token: 0x04000902 RID: 2306
		public static readonly ActionIdentifier RetrieveTapeRecoveryPoint = new ActionIdentifier("storagegateway:RetrieveTapeRecoveryPoint");

		// Token: 0x04000903 RID: 2307
		public static readonly ActionIdentifier ShutdownGateway = new ActionIdentifier("storagegateway:ShutdownGateway");

		// Token: 0x04000904 RID: 2308
		public static readonly ActionIdentifier StartGateway = new ActionIdentifier("storagegateway:StartGateway");

		// Token: 0x04000905 RID: 2309
		public static readonly ActionIdentifier UpdateBandwidthRateLimit = new ActionIdentifier("storagegateway:UpdateBandwidthRateLimit");

		// Token: 0x04000906 RID: 2310
		public static readonly ActionIdentifier UpdateChapCredentials = new ActionIdentifier("storagegateway:UpdateChapCredentials");

		// Token: 0x04000907 RID: 2311
		public static readonly ActionIdentifier UpdateGatewayInformation = new ActionIdentifier("storagegateway:UpdateGatewayInformation");

		// Token: 0x04000908 RID: 2312
		public static readonly ActionIdentifier UpdateGatewaySoftwareNow = new ActionIdentifier("storagegateway:UpdateGatewaySoftwareNow");

		// Token: 0x04000909 RID: 2313
		public static readonly ActionIdentifier UpdateMaintenanceStartTime = new ActionIdentifier("storagegateway:UpdateMaintenanceStartTime");

		// Token: 0x0400090A RID: 2314
		public static readonly ActionIdentifier UpdateSnapshotSchedule = new ActionIdentifier("storagegateway:UpdateSnapshotSchedule");
	}
}
