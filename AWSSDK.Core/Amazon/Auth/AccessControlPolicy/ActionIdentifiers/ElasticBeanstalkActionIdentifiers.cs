﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200018C RID: 396
	public static class ElasticBeanstalkActionIdentifiers
	{
		// Token: 0x040006B7 RID: 1719
		public static readonly ActionIdentifier AllElasticBeanstalkActions = new ActionIdentifier("elasticbeanstalk:*");

		// Token: 0x040006B8 RID: 1720
		public static readonly ActionIdentifier CheckDNSAvailability = new ActionIdentifier("elasticbeanstalk:CheckDNSAvailability");

		// Token: 0x040006B9 RID: 1721
		public static readonly ActionIdentifier CreateApplication = new ActionIdentifier("elasticbeanstalk:CreateApplication");

		// Token: 0x040006BA RID: 1722
		public static readonly ActionIdentifier CreateApplicationVersion = new ActionIdentifier("elasticbeanstalk:CreateApplicationVersion");

		// Token: 0x040006BB RID: 1723
		public static readonly ActionIdentifier CreateConfigurationTemplate = new ActionIdentifier("elasticbeanstalk:CreateConfigurationTemplate");

		// Token: 0x040006BC RID: 1724
		public static readonly ActionIdentifier CreateEnvironment = new ActionIdentifier("elasticbeanstalk:CreateEnvironment");

		// Token: 0x040006BD RID: 1725
		public static readonly ActionIdentifier CreateStorageLocation = new ActionIdentifier("elasticbeanstalk:CreateStorageLocation");

		// Token: 0x040006BE RID: 1726
		public static readonly ActionIdentifier DeleteApplication = new ActionIdentifier("elasticbeanstalk:DeleteApplication");

		// Token: 0x040006BF RID: 1727
		public static readonly ActionIdentifier DeleteApplicationVersion = new ActionIdentifier("elasticbeanstalk:DeleteApplicationVersion");

		// Token: 0x040006C0 RID: 1728
		public static readonly ActionIdentifier DeleteConfigurationTemplate = new ActionIdentifier("elasticbeanstalk:DeleteConfigurationTemplate");

		// Token: 0x040006C1 RID: 1729
		public static readonly ActionIdentifier DeleteEnvironmentConfiguration = new ActionIdentifier("elasticbeanstalk:DeleteEnvironmentConfiguration");

		// Token: 0x040006C2 RID: 1730
		public static readonly ActionIdentifier DescribeApplicationVersions = new ActionIdentifier("elasticbeanstalk:DescribeApplicationVersions");

		// Token: 0x040006C3 RID: 1731
		public static readonly ActionIdentifier DescribeApplications = new ActionIdentifier("elasticbeanstalk:DescribeApplications");

		// Token: 0x040006C4 RID: 1732
		public static readonly ActionIdentifier DescribeConfigurationOptions = new ActionIdentifier("elasticbeanstalk:DescribeConfigurationOptions");

		// Token: 0x040006C5 RID: 1733
		public static readonly ActionIdentifier DescribeConfigurationSettings = new ActionIdentifier("elasticbeanstalk:DescribeConfigurationSettings");

		// Token: 0x040006C6 RID: 1734
		public static readonly ActionIdentifier DescribeEnvironmentResources = new ActionIdentifier("elasticbeanstalk:DescribeEnvironmentResources");

		// Token: 0x040006C7 RID: 1735
		public static readonly ActionIdentifier DescribeEnvironments = new ActionIdentifier("elasticbeanstalk:DescribeEnvironments");

		// Token: 0x040006C8 RID: 1736
		public static readonly ActionIdentifier DescribeEvents = new ActionIdentifier("elasticbeanstalk:DescribeEvents");

		// Token: 0x040006C9 RID: 1737
		public static readonly ActionIdentifier ListAvailableSolutionStacks = new ActionIdentifier("elasticbeanstalk:ListAvailableSolutionStacks");

		// Token: 0x040006CA RID: 1738
		public static readonly ActionIdentifier RebuildEnvironment = new ActionIdentifier("elasticbeanstalk:RebuildEnvironment");

		// Token: 0x040006CB RID: 1739
		public static readonly ActionIdentifier RequestEnvironmentInfo = new ActionIdentifier("elasticbeanstalk:RequestEnvironmentInfo");

		// Token: 0x040006CC RID: 1740
		public static readonly ActionIdentifier RestartAppServer = new ActionIdentifier("elasticbeanstalk:RestartAppServer");

		// Token: 0x040006CD RID: 1741
		public static readonly ActionIdentifier RetrieveEnvironmentInfo = new ActionIdentifier("elasticbeanstalk:RetrieveEnvironmentInfo");

		// Token: 0x040006CE RID: 1742
		public static readonly ActionIdentifier SwapEnvironmentCNAMEs = new ActionIdentifier("elasticbeanstalk:SwapEnvironmentCNAMEs");

		// Token: 0x040006CF RID: 1743
		public static readonly ActionIdentifier TerminateEnvironment = new ActionIdentifier("elasticbeanstalk:TerminateEnvironment");

		// Token: 0x040006D0 RID: 1744
		public static readonly ActionIdentifier UpdateApplication = new ActionIdentifier("elasticbeanstalk:UpdateApplication");

		// Token: 0x040006D1 RID: 1745
		public static readonly ActionIdentifier UpdateApplicationVersion = new ActionIdentifier("elasticbeanstalk:UpdateApplicationVersion");

		// Token: 0x040006D2 RID: 1746
		public static readonly ActionIdentifier UpdateConfigurationTemplate = new ActionIdentifier("elasticbeanstalk:UpdateConfigurationTemplate");

		// Token: 0x040006D3 RID: 1747
		public static readonly ActionIdentifier UpdateEnvironment = new ActionIdentifier("elasticbeanstalk:UpdateEnvironment");

		// Token: 0x040006D4 RID: 1748
		public static readonly ActionIdentifier ValidateConfigurationSettings = new ActionIdentifier("elasticbeanstalk:ValidateConfigurationSettings");
	}
}
