﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200018B RID: 395
	public static class ElastiCacheActionIdentifiers
	{
		// Token: 0x040006A4 RID: 1700
		public static readonly ActionIdentifier AllElastiCacheActions = new ActionIdentifier("elasticache:*");

		// Token: 0x040006A5 RID: 1701
		public static readonly ActionIdentifier AuthorizeCacheSecurityGroupIngress = new ActionIdentifier("elasticache:AuthorizeCacheSecurityGroupIngress");

		// Token: 0x040006A6 RID: 1702
		public static readonly ActionIdentifier CreateCacheCluster = new ActionIdentifier("elasticache:CreateCacheCluster");

		// Token: 0x040006A7 RID: 1703
		public static readonly ActionIdentifier CreateCacheParameterGroup = new ActionIdentifier("elasticache:CreateCacheParameterGroup");

		// Token: 0x040006A8 RID: 1704
		public static readonly ActionIdentifier CreateCacheSecurityGroup = new ActionIdentifier("elasticache:CreateCacheSecurityGroup");

		// Token: 0x040006A9 RID: 1705
		public static readonly ActionIdentifier DeleteCacheCluster = new ActionIdentifier("elasticache:DeleteCacheCluster");

		// Token: 0x040006AA RID: 1706
		public static readonly ActionIdentifier DeleteCacheParameterGroup = new ActionIdentifier("elasticache:DeleteCacheParameterGroup");

		// Token: 0x040006AB RID: 1707
		public static readonly ActionIdentifier DeleteCacheSecurityGroup = new ActionIdentifier("elasticache:DeleteCacheSecurityGroup");

		// Token: 0x040006AC RID: 1708
		public static readonly ActionIdentifier DescribeCacheClusters = new ActionIdentifier("elasticache:DescribeCacheClusters");

		// Token: 0x040006AD RID: 1709
		public static readonly ActionIdentifier DescribeCacheParameterGroups = new ActionIdentifier("elasticache:DescribeCacheParameterGroups");

		// Token: 0x040006AE RID: 1710
		public static readonly ActionIdentifier DescribeCacheParameters = new ActionIdentifier("elasticache:DescribeCacheParameters");

		// Token: 0x040006AF RID: 1711
		public static readonly ActionIdentifier DescribeCacheSecurityGroups = new ActionIdentifier("elasticache:DescribeCacheSecurityGroups");

		// Token: 0x040006B0 RID: 1712
		public static readonly ActionIdentifier DescribeEngineDefaultParameters = new ActionIdentifier("elasticache:DescribeEngineDefaultParameters");

		// Token: 0x040006B1 RID: 1713
		public static readonly ActionIdentifier DescribeEvents = new ActionIdentifier("elasticache:DescribeEvents");

		// Token: 0x040006B2 RID: 1714
		public static readonly ActionIdentifier ModifyCacheCluster = new ActionIdentifier("elasticache:ModifyCacheCluster");

		// Token: 0x040006B3 RID: 1715
		public static readonly ActionIdentifier ModifyCacheParameterGroup = new ActionIdentifier("elasticache:ModifyCacheParameterGroup");

		// Token: 0x040006B4 RID: 1716
		public static readonly ActionIdentifier RebootCacheCluster = new ActionIdentifier("elasticache:RebootCacheCluster");

		// Token: 0x040006B5 RID: 1717
		public static readonly ActionIdentifier ResetCacheParameterGroup = new ActionIdentifier("elasticache:ResetCacheParameterGroup");

		// Token: 0x040006B6 RID: 1718
		public static readonly ActionIdentifier RevokeCacheSecurityGroupIngress = new ActionIdentifier("elasticache:RevokeCacheSecurityGroupIngress");
	}
}
