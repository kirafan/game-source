﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000182 RID: 386
	public static class CloudSearchActionIdentifiers
	{
		// Token: 0x040005A1 RID: 1441
		public static readonly ActionIdentifier AllCloudSearchActions = new ActionIdentifier("cloudsearch:*");

		// Token: 0x040005A2 RID: 1442
		public static readonly ActionIdentifier BuildSuggesters = new ActionIdentifier("cloudsearch:BuildSuggesters");

		// Token: 0x040005A3 RID: 1443
		public static readonly ActionIdentifier CreateDomain = new ActionIdentifier("cloudsearch:CreateDomain");

		// Token: 0x040005A4 RID: 1444
		public static readonly ActionIdentifier DefineAnalysisScheme = new ActionIdentifier("cloudsearch:DefineAnalysisScheme");

		// Token: 0x040005A5 RID: 1445
		public static readonly ActionIdentifier DefineExpression = new ActionIdentifier("cloudsearch:DefineExpression");

		// Token: 0x040005A6 RID: 1446
		public static readonly ActionIdentifier DefineIndexField = new ActionIdentifier("cloudsearch:DefineIndexField");

		// Token: 0x040005A7 RID: 1447
		public static readonly ActionIdentifier DefineSuggester = new ActionIdentifier("cloudsearch:DefineSuggester");

		// Token: 0x040005A8 RID: 1448
		public static readonly ActionIdentifier DeleteAnalysisScheme = new ActionIdentifier("cloudsearch:DeleteAnalysisScheme");

		// Token: 0x040005A9 RID: 1449
		public static readonly ActionIdentifier DeleteDomain = new ActionIdentifier("cloudsearch:DeleteDomain");

		// Token: 0x040005AA RID: 1450
		public static readonly ActionIdentifier DeleteExpression = new ActionIdentifier("cloudsearch:DeleteExpression");

		// Token: 0x040005AB RID: 1451
		public static readonly ActionIdentifier DeleteIndexField = new ActionIdentifier("cloudsearch:DeleteIndexField");

		// Token: 0x040005AC RID: 1452
		public static readonly ActionIdentifier DeleteSuggester = new ActionIdentifier("cloudsearch:DeleteSuggester");

		// Token: 0x040005AD RID: 1453
		public static readonly ActionIdentifier DescribeAnalysisSchemes = new ActionIdentifier("cloudsearch:DescribeAnalysisSchemes");

		// Token: 0x040005AE RID: 1454
		public static readonly ActionIdentifier DescribeAvailabilityOptions = new ActionIdentifier("cloudsearch:DescribeAvailabilityOptions");

		// Token: 0x040005AF RID: 1455
		public static readonly ActionIdentifier DescribeDomains = new ActionIdentifier("cloudsearch:DescribeDomains");

		// Token: 0x040005B0 RID: 1456
		public static readonly ActionIdentifier DescribeExpressions = new ActionIdentifier("cloudsearch:DescribeExpressions");

		// Token: 0x040005B1 RID: 1457
		public static readonly ActionIdentifier DescribeIndexFields = new ActionIdentifier("cloudsearch:DescribeIndexFields");

		// Token: 0x040005B2 RID: 1458
		public static readonly ActionIdentifier DescribeScalingParameters = new ActionIdentifier("cloudsearch:DescribeScalingParameters");

		// Token: 0x040005B3 RID: 1459
		public static readonly ActionIdentifier DescribeServiceAccessPolicies = new ActionIdentifier("cloudsearch:DescribeServiceAccessPolicies");

		// Token: 0x040005B4 RID: 1460
		public static readonly ActionIdentifier DescribeSuggesters = new ActionIdentifier("cloudsearch:DescribeSuggesters");

		// Token: 0x040005B5 RID: 1461
		public static readonly ActionIdentifier IndexDocuments = new ActionIdentifier("cloudsearch:IndexDocuments");

		// Token: 0x040005B6 RID: 1462
		public static readonly ActionIdentifier ListDomainNames = new ActionIdentifier("cloudsearch:ListDomainNames");

		// Token: 0x040005B7 RID: 1463
		public static readonly ActionIdentifier UpdateAvailabilityOptions = new ActionIdentifier("cloudsearch:UpdateAvailabilityOptions");

		// Token: 0x040005B8 RID: 1464
		public static readonly ActionIdentifier UpdateScalingParameters = new ActionIdentifier("cloudsearch:UpdateScalingParameters");

		// Token: 0x040005B9 RID: 1465
		public static readonly ActionIdentifier UpdateServiceAccessPolicies = new ActionIdentifier("cloudsearch:UpdateServiceAccessPolicies");
	}
}
