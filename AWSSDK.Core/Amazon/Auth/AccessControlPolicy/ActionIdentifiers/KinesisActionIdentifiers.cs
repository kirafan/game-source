﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000193 RID: 403
	public static class KinesisActionIdentifiers
	{
		// Token: 0x0400077A RID: 1914
		public static readonly ActionIdentifier AllKinesisActions = new ActionIdentifier("kinesis:*");

		// Token: 0x0400077B RID: 1915
		public static readonly ActionIdentifier CreateStream = new ActionIdentifier("kinesis:CreateStream");

		// Token: 0x0400077C RID: 1916
		public static readonly ActionIdentifier DeleteStream = new ActionIdentifier("kinesis:DeleteStream");

		// Token: 0x0400077D RID: 1917
		public static readonly ActionIdentifier DescribeStream = new ActionIdentifier("kinesis:DescribeStream");

		// Token: 0x0400077E RID: 1918
		public static readonly ActionIdentifier ListStreams = new ActionIdentifier("kinesis:ListStreams");

		// Token: 0x0400077F RID: 1919
		public static readonly ActionIdentifier PutRecord = new ActionIdentifier("kinesis:PutRecord");

		// Token: 0x04000780 RID: 1920
		public static readonly ActionIdentifier GetShardIterator = new ActionIdentifier("kinesis:GetShardIterator");

		// Token: 0x04000781 RID: 1921
		public static readonly ActionIdentifier GetRecords = new ActionIdentifier("kinesis:GetRecords");

		// Token: 0x04000782 RID: 1922
		public static readonly ActionIdentifier MergeShards = new ActionIdentifier("kinesis:MergeShards");

		// Token: 0x04000783 RID: 1923
		public static readonly ActionIdentifier SplitShard = new ActionIdentifier("kinesis:SplitShard");
	}
}
