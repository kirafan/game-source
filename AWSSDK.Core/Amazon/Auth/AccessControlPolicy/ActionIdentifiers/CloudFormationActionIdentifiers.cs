﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000180 RID: 384
	public static class CloudFormationActionIdentifiers
	{
		// Token: 0x0400057E RID: 1406
		public static readonly ActionIdentifier AllCloudFormationActions = new ActionIdentifier("cloudformation:*");

		// Token: 0x0400057F RID: 1407
		public static readonly ActionIdentifier CreateStack = new ActionIdentifier("cloudformation:CreateStack");

		// Token: 0x04000580 RID: 1408
		public static readonly ActionIdentifier DeleteStack = new ActionIdentifier("cloudformation:DeleteStack");

		// Token: 0x04000581 RID: 1409
		public static readonly ActionIdentifier DescribeStackEvents = new ActionIdentifier("cloudformation:DescribeStackEvents");

		// Token: 0x04000582 RID: 1410
		public static readonly ActionIdentifier DescribeStackResource = new ActionIdentifier("cloudformation:DescribeStackResource");

		// Token: 0x04000583 RID: 1411
		public static readonly ActionIdentifier DescribeStackResources = new ActionIdentifier("cloudformation:DescribeStackResources");

		// Token: 0x04000584 RID: 1412
		public static readonly ActionIdentifier DescribeStacks = new ActionIdentifier("cloudformation:DescribeStacks");

		// Token: 0x04000585 RID: 1413
		public static readonly ActionIdentifier EstimateTemplateCost = new ActionIdentifier("cloudformation:EstimateTemplateCost");

		// Token: 0x04000586 RID: 1414
		public static readonly ActionIdentifier GetTemplate = new ActionIdentifier("cloudformation:GetTemplate");

		// Token: 0x04000587 RID: 1415
		public static readonly ActionIdentifier ListStacks = new ActionIdentifier("cloudformation:ListStacks");

		// Token: 0x04000588 RID: 1416
		public static readonly ActionIdentifier ListStackResources = new ActionIdentifier("cloudformation:ListStackResources");

		// Token: 0x04000589 RID: 1417
		public static readonly ActionIdentifier UpdateStack = new ActionIdentifier("cloudformation:UpdateStack");

		// Token: 0x0400058A RID: 1418
		public static readonly ActionIdentifier ValidateTemplate = new ActionIdentifier("cloudformation:ValidateTemplate");
	}
}
