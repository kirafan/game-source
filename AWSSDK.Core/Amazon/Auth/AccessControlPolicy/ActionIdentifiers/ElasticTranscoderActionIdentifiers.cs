﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200018F RID: 399
	public static class ElasticTranscoderActionIdentifiers
	{
		// Token: 0x04000700 RID: 1792
		public static readonly ActionIdentifier AllElasticTranscoderActions = new ActionIdentifier("elastictranscoder:*");

		// Token: 0x04000701 RID: 1793
		public static readonly ActionIdentifier CancelJob = new ActionIdentifier("elastictranscoder:CancelJob");

		// Token: 0x04000702 RID: 1794
		public static readonly ActionIdentifier CreateJob = new ActionIdentifier("elastictranscoder:CreateJob");

		// Token: 0x04000703 RID: 1795
		public static readonly ActionIdentifier CreatePipeline = new ActionIdentifier("elastictranscoder:CreatePipeline");

		// Token: 0x04000704 RID: 1796
		public static readonly ActionIdentifier CreatePreset = new ActionIdentifier("elastictranscoder:CreatePreset");

		// Token: 0x04000705 RID: 1797
		public static readonly ActionIdentifier DeletePipeline = new ActionIdentifier("elastictranscoder:DeletePipeline");

		// Token: 0x04000706 RID: 1798
		public static readonly ActionIdentifier DeletePreset = new ActionIdentifier("elastictranscoder:DeletePreset");

		// Token: 0x04000707 RID: 1799
		public static readonly ActionIdentifier ListJobsByPipeline = new ActionIdentifier("elastictranscoder:ListJobsByPipeline");

		// Token: 0x04000708 RID: 1800
		public static readonly ActionIdentifier ListJobsByStatus = new ActionIdentifier("elastictranscoder:ListJobsByStatus");

		// Token: 0x04000709 RID: 1801
		public static readonly ActionIdentifier ListPipelines = new ActionIdentifier("elastictranscoder:ListPipelines");

		// Token: 0x0400070A RID: 1802
		public static readonly ActionIdentifier ListPresets = new ActionIdentifier("elastictranscoder:ListPresets");

		// Token: 0x0400070B RID: 1803
		public static readonly ActionIdentifier ReadJob = new ActionIdentifier("elastictranscoder:ReadJob");

		// Token: 0x0400070C RID: 1804
		public static readonly ActionIdentifier ReadPipeline = new ActionIdentifier("elastictranscoder:ReadPipeline");

		// Token: 0x0400070D RID: 1805
		public static readonly ActionIdentifier ReadPreset = new ActionIdentifier("elastictranscoder:ReadPreset");

		// Token: 0x0400070E RID: 1806
		public static readonly ActionIdentifier TestRole = new ActionIdentifier("elastictranscoder:TestRole");

		// Token: 0x0400070F RID: 1807
		public static readonly ActionIdentifier UpdatePipeline = new ActionIdentifier("elastictranscoder:UpdatePipeline");

		// Token: 0x04000710 RID: 1808
		public static readonly ActionIdentifier UpdatePipelineNotifications = new ActionIdentifier("elastictranscoder:UpdatePipelineNotifications");

		// Token: 0x04000711 RID: 1809
		public static readonly ActionIdentifier UpdatePipelineStatus = new ActionIdentifier("elastictranscoder:UpdatePipelineStatus");
	}
}
