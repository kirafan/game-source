﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200018D RID: 397
	public static class ElasticLoadBalancingActionIdentifiers
	{
		// Token: 0x040006D5 RID: 1749
		public static readonly ActionIdentifier AllElasticLoadBalancingActions = new ActionIdentifier("elasticloadbalancing:*");

		// Token: 0x040006D6 RID: 1750
		public static readonly ActionIdentifier ApplySecurityGroupsToLoadBalancer = new ActionIdentifier("elasticloadbalancing:ApplySecurityGroupsToLoadBalancer");

		// Token: 0x040006D7 RID: 1751
		public static readonly ActionIdentifier AttachLoadBalancerToSubnets = new ActionIdentifier("elasticloadbalancing:AttachLoadBalancerToSubnets");

		// Token: 0x040006D8 RID: 1752
		public static readonly ActionIdentifier ConfigureHealthCheck = new ActionIdentifier("elasticloadbalancing:ConfigureHealthCheck");

		// Token: 0x040006D9 RID: 1753
		public static readonly ActionIdentifier CreateAppCookieStickinessPolicy = new ActionIdentifier("elasticloadbalancing:CreateAppCookieStickinessPolicy");

		// Token: 0x040006DA RID: 1754
		public static readonly ActionIdentifier CreateLBCookieStickinessPolicy = new ActionIdentifier("elasticloadbalancing:CreateLBCookieStickinessPolicy");

		// Token: 0x040006DB RID: 1755
		public static readonly ActionIdentifier CreateLoadBalancer = new ActionIdentifier("elasticloadbalancing:CreateLoadBalancer");

		// Token: 0x040006DC RID: 1756
		public static readonly ActionIdentifier CreateLoadBalancerListeners = new ActionIdentifier("elasticloadbalancing:CreateLoadBalancerListeners");

		// Token: 0x040006DD RID: 1757
		public static readonly ActionIdentifier CreateLoadBalancerPolicy = new ActionIdentifier("elasticloadbalancing:CreateLoadBalancerPolicy");

		// Token: 0x040006DE RID: 1758
		public static readonly ActionIdentifier DeleteLoadBalancer = new ActionIdentifier("elasticloadbalancing:DeleteLoadBalancer");

		// Token: 0x040006DF RID: 1759
		public static readonly ActionIdentifier DeleteLoadBalancerListeners = new ActionIdentifier("elasticloadbalancing:DeleteLoadBalancerListeners");

		// Token: 0x040006E0 RID: 1760
		public static readonly ActionIdentifier DeleteLoadBalancerPolicy = new ActionIdentifier("elasticloadbalancing:DeleteLoadBalancerPolicy");

		// Token: 0x040006E1 RID: 1761
		public static readonly ActionIdentifier DeregisterInstancesFromLoadBalancer = new ActionIdentifier("elasticloadbalancing:DeregisterInstancesFromLoadBalancer");

		// Token: 0x040006E2 RID: 1762
		public static readonly ActionIdentifier DescribeInstanceHealth = new ActionIdentifier("elasticloadbalancing:DescribeInstanceHealth");

		// Token: 0x040006E3 RID: 1763
		public static readonly ActionIdentifier DescribeLoadBalancerAttributes = new ActionIdentifier("elasticloadbalancing:DescribeLoadBalancerAttributes");

		// Token: 0x040006E4 RID: 1764
		public static readonly ActionIdentifier DescribeLoadBalancerPolicyTypes = new ActionIdentifier("elasticloadbalancing:DescribeLoadBalancerPolicyTypes");

		// Token: 0x040006E5 RID: 1765
		public static readonly ActionIdentifier DescribeLoadBalancerPolicies = new ActionIdentifier("elasticloadbalancing:DescribeLoadBalancerPolicies");

		// Token: 0x040006E6 RID: 1766
		public static readonly ActionIdentifier DescribeLoadBalancers = new ActionIdentifier("elasticloadbalancing:DescribeLoadBalancers");

		// Token: 0x040006E7 RID: 1767
		public static readonly ActionIdentifier DetachLoadBalancerFromSubnets = new ActionIdentifier("elasticloadbalancing:DetachLoadBalancerFromSubnets");

		// Token: 0x040006E8 RID: 1768
		public static readonly ActionIdentifier DisableAvailabilityZonesForLoadBalancer = new ActionIdentifier("elasticloadbalancing:DisableAvailabilityZonesForLoadBalancer");

		// Token: 0x040006E9 RID: 1769
		public static readonly ActionIdentifier EnableAvailabilityZonesForLoadBalancer = new ActionIdentifier("elasticloadbalancing:EnableAvailabilityZonesForLoadBalancer");

		// Token: 0x040006EA RID: 1770
		public static readonly ActionIdentifier ModifyLoadBalancerAttributes = new ActionIdentifier("elasticloadbalancing:ModifyLoadBalancerAttributes");

		// Token: 0x040006EB RID: 1771
		public static readonly ActionIdentifier RegisterInstancesWithLoadBalancer = new ActionIdentifier("elasticloadbalancing:RegisterInstancesWithLoadBalancer");

		// Token: 0x040006EC RID: 1772
		public static readonly ActionIdentifier SetLoadBalancerListenerSSLCertificate = new ActionIdentifier("elasticloadbalancing:SetLoadBalancerListenerSSLCertificate");

		// Token: 0x040006ED RID: 1773
		public static readonly ActionIdentifier SetLoadBalancerPoliciesForBackendServer = new ActionIdentifier("elasticloadbalancing:SetLoadBalancerPoliciesForBackendServer");

		// Token: 0x040006EE RID: 1774
		public static readonly ActionIdentifier SetLoadBalancerPoliciesOfListener = new ActionIdentifier("elasticloadbalancing:SetLoadBalancerPoliciesOfListener");
	}
}
