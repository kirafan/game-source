﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200019B RID: 411
	public static class S3ActionIdentifiers
	{
		// Token: 0x0400083B RID: 2107
		public static readonly ActionIdentifier AllS3Actions = new ActionIdentifier("s3:*");

		// Token: 0x0400083C RID: 2108
		public static readonly ActionIdentifier AbortMultipartUpload = new ActionIdentifier("s3:AbortMultipartUpload");

		// Token: 0x0400083D RID: 2109
		public static readonly ActionIdentifier CreateBucket = new ActionIdentifier("s3:CreateBucket");

		// Token: 0x0400083E RID: 2110
		public static readonly ActionIdentifier DeleteBucket = new ActionIdentifier("s3:DeleteBucket");

		// Token: 0x0400083F RID: 2111
		public static readonly ActionIdentifier DeleteBucketPolicy = new ActionIdentifier("s3:DeleteBucketPolicy");

		// Token: 0x04000840 RID: 2112
		public static readonly ActionIdentifier DeleteBucketWebsite = new ActionIdentifier("s3:DeleteBucketWebsite");

		// Token: 0x04000841 RID: 2113
		public static readonly ActionIdentifier DeleteObject = new ActionIdentifier("s3:DeleteObject");

		// Token: 0x04000842 RID: 2114
		public static readonly ActionIdentifier DeleteObjectVersion = new ActionIdentifier("s3:DeleteObjectVersion");

		// Token: 0x04000843 RID: 2115
		public static readonly ActionIdentifier GetBucketAcl = new ActionIdentifier("s3:GetBucketAcl");

		// Token: 0x04000844 RID: 2116
		public static readonly ActionIdentifier GetBucketCORS = new ActionIdentifier("s3:GetBucketCORS");

		// Token: 0x04000845 RID: 2117
		public static readonly ActionIdentifier GetBucketLocation = new ActionIdentifier("s3:GetBucketLocation");

		// Token: 0x04000846 RID: 2118
		public static readonly ActionIdentifier GetBucketLogging = new ActionIdentifier("s3:GetBucketLogging");

		// Token: 0x04000847 RID: 2119
		public static readonly ActionIdentifier GetBucketNotification = new ActionIdentifier("s3:GetBucketNotification");

		// Token: 0x04000848 RID: 2120
		public static readonly ActionIdentifier GetBucketPolicy = new ActionIdentifier("s3:GetBucketPolicy");

		// Token: 0x04000849 RID: 2121
		public static readonly ActionIdentifier GetBucketRequestPayment = new ActionIdentifier("s3:GetBucketRequestPayment");

		// Token: 0x0400084A RID: 2122
		public static readonly ActionIdentifier GetBucketTagging = new ActionIdentifier("s3:GetBucketTagging");

		// Token: 0x0400084B RID: 2123
		public static readonly ActionIdentifier GetBucketVersioning = new ActionIdentifier("s3:GetBucketVersioning");

		// Token: 0x0400084C RID: 2124
		public static readonly ActionIdentifier GetBucketWebsite = new ActionIdentifier("s3:GetBucketWebsite");

		// Token: 0x0400084D RID: 2125
		public static readonly ActionIdentifier GetLifecycleConfiguration = new ActionIdentifier("s3:GetLifecycleConfiguration");

		// Token: 0x0400084E RID: 2126
		public static readonly ActionIdentifier GetObject = new ActionIdentifier("s3:GetObject");

		// Token: 0x0400084F RID: 2127
		public static readonly ActionIdentifier GetObjectAcl = new ActionIdentifier("s3:GetObjectAcl");

		// Token: 0x04000850 RID: 2128
		public static readonly ActionIdentifier GetObjectTorrent = new ActionIdentifier("s3:GetObjectTorrent");

		// Token: 0x04000851 RID: 2129
		public static readonly ActionIdentifier GetObjectVersion = new ActionIdentifier("s3:GetObjectVersion");

		// Token: 0x04000852 RID: 2130
		public static readonly ActionIdentifier GetObjectVersionAcl = new ActionIdentifier("s3:GetObjectVersionAcl");

		// Token: 0x04000853 RID: 2131
		public static readonly ActionIdentifier GetObjectVersionTorrent = new ActionIdentifier("s3:GetObjectVersionTorrent");

		// Token: 0x04000854 RID: 2132
		public static readonly ActionIdentifier ListAllMyBuckets = new ActionIdentifier("s3:ListAllMyBuckets");

		// Token: 0x04000855 RID: 2133
		public static readonly ActionIdentifier ListBucket = new ActionIdentifier("s3:ListBucket");

		// Token: 0x04000856 RID: 2134
		public static readonly ActionIdentifier ListBucketMultipartUploads = new ActionIdentifier("s3:ListBucketMultipartUploads");

		// Token: 0x04000857 RID: 2135
		public static readonly ActionIdentifier ListBucketVersions = new ActionIdentifier("s3:ListBucketVersions");

		// Token: 0x04000858 RID: 2136
		public static readonly ActionIdentifier ListMultipartUploadParts = new ActionIdentifier("s3:ListMultipartUploadParts");

		// Token: 0x04000859 RID: 2137
		public static readonly ActionIdentifier PutBucketAcl = new ActionIdentifier("s3:PutBucketAcl");

		// Token: 0x0400085A RID: 2138
		public static readonly ActionIdentifier PutBucketCORS = new ActionIdentifier("s3:PutBucketCORS");

		// Token: 0x0400085B RID: 2139
		public static readonly ActionIdentifier PutBucketLogging = new ActionIdentifier("s3:PutBucketLogging");

		// Token: 0x0400085C RID: 2140
		public static readonly ActionIdentifier PutBucketNotification = new ActionIdentifier("s3:PutBucketNotification");

		// Token: 0x0400085D RID: 2141
		public static readonly ActionIdentifier PutBucketPolicy = new ActionIdentifier("s3:PutBucketPolicy");

		// Token: 0x0400085E RID: 2142
		public static readonly ActionIdentifier PutBucketRequestPayment = new ActionIdentifier("s3:PutBucketRequestPayment");

		// Token: 0x0400085F RID: 2143
		public static readonly ActionIdentifier PutBucketTagging = new ActionIdentifier("s3:PutBucketTagging");

		// Token: 0x04000860 RID: 2144
		public static readonly ActionIdentifier PutBucketVersioning = new ActionIdentifier("s3:PutBucketVersioning");

		// Token: 0x04000861 RID: 2145
		public static readonly ActionIdentifier PutBucketWebsite = new ActionIdentifier("s3:PutBucketWebsite");

		// Token: 0x04000862 RID: 2146
		public static readonly ActionIdentifier PutLifecycleConfiguration = new ActionIdentifier("s3:PutLifecycleConfiguration");

		// Token: 0x04000863 RID: 2147
		public static readonly ActionIdentifier PutObject = new ActionIdentifier("s3:PutObject");

		// Token: 0x04000864 RID: 2148
		public static readonly ActionIdentifier PutObjectAcl = new ActionIdentifier("s3:PutObjectAcl");

		// Token: 0x04000865 RID: 2149
		public static readonly ActionIdentifier PutObjectVersionAcl = new ActionIdentifier("s3:PutObjectVersionAcl");

		// Token: 0x04000866 RID: 2150
		public static readonly ActionIdentifier RestoreObject = new ActionIdentifier("s3:RestoreObject");
	}
}
