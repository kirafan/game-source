﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000194 RID: 404
	public static class MarketplaceActionIdentifiers
	{
		// Token: 0x04000784 RID: 1924
		public static readonly ActionIdentifier AllMarketplaceActions = new ActionIdentifier("aws-marketplace:*");

		// Token: 0x04000785 RID: 1925
		public static readonly ActionIdentifier Subscribe = new ActionIdentifier("aws-marketplace:Subscribe");

		// Token: 0x04000786 RID: 1926
		public static readonly ActionIdentifier Unsubscribe = new ActionIdentifier("aws-marketplace:Unsubscribe");

		// Token: 0x04000787 RID: 1927
		public static readonly ActionIdentifier ViewSubscriptions = new ActionIdentifier("aws-marketplace:ViewSubscriptions");
	}
}
