﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200017E RID: 382
	public static class AutoScalingActionIdentifiers
	{
		// Token: 0x04000551 RID: 1361
		public static readonly ActionIdentifier AllAutoScalingActions = new ActionIdentifier("autoscaling:*");

		// Token: 0x04000552 RID: 1362
		public static readonly ActionIdentifier CreateAutoScalingGroup = new ActionIdentifier("autoscaling:CreateAutoScalingGroup");

		// Token: 0x04000553 RID: 1363
		public static readonly ActionIdentifier CreateLaunchConfiguration = new ActionIdentifier("autoscaling:CreateLaunchConfiguration");

		// Token: 0x04000554 RID: 1364
		public static readonly ActionIdentifier CreateOrUpdateScalingTrigger = new ActionIdentifier("autoscaling:CreateOrUpdateScalingTrigger");

		// Token: 0x04000555 RID: 1365
		public static readonly ActionIdentifier CreateOrUpdateTags = new ActionIdentifier("autoscaling:CreateOrUpdateTags");

		// Token: 0x04000556 RID: 1366
		public static readonly ActionIdentifier DeleteAutoScalingGroup = new ActionIdentifier("autoscaling:DeleteAutoScalingGroup");

		// Token: 0x04000557 RID: 1367
		public static readonly ActionIdentifier DeleteLaunchConfiguration = new ActionIdentifier("autoscaling:DeleteLaunchConfiguration");

		// Token: 0x04000558 RID: 1368
		public static readonly ActionIdentifier DeleteNotificationConfiguration = new ActionIdentifier("autoscaling:DeleteNotificationConfiguration");

		// Token: 0x04000559 RID: 1369
		public static readonly ActionIdentifier DeletePolicy = new ActionIdentifier("autoscaling:DeletePolicy");

		// Token: 0x0400055A RID: 1370
		public static readonly ActionIdentifier DeleteScheduledAction = new ActionIdentifier("autoscaling:DeleteScheduledAction");

		// Token: 0x0400055B RID: 1371
		public static readonly ActionIdentifier DeleteTags = new ActionIdentifier("autoscaling:DeleteTags");

		// Token: 0x0400055C RID: 1372
		public static readonly ActionIdentifier DeleteTrigger = new ActionIdentifier("autoscaling:DeleteTrigger");

		// Token: 0x0400055D RID: 1373
		public static readonly ActionIdentifier DescribeAdjustmentTypes = new ActionIdentifier("autoscaling:DescribeAdjustmentTypes");

		// Token: 0x0400055E RID: 1374
		public static readonly ActionIdentifier DescribeAutoScalingGroups = new ActionIdentifier("autoscaling:DescribeAutoScalingGroups");

		// Token: 0x0400055F RID: 1375
		public static readonly ActionIdentifier DescribeAutoScalingInstances = new ActionIdentifier("autoscaling:DescribeAutoScalingInstances");

		// Token: 0x04000560 RID: 1376
		public static readonly ActionIdentifier DescribeAutoScalingNotificationTypes = new ActionIdentifier("autoscaling:DescribeAutoScalingNotificationTypes");

		// Token: 0x04000561 RID: 1377
		public static readonly ActionIdentifier DescribeLaunchConfigurations = new ActionIdentifier("autoscaling:DescribeLaunchConfigurations");

		// Token: 0x04000562 RID: 1378
		public static readonly ActionIdentifier DescribeMetricCollectionTypes = new ActionIdentifier("autoscaling:DescribeMetricCollectionTypes");

		// Token: 0x04000563 RID: 1379
		public static readonly ActionIdentifier DescribeNotificationConfigurations = new ActionIdentifier("autoscaling:DescribeNotificationConfigurations");

		// Token: 0x04000564 RID: 1380
		public static readonly ActionIdentifier DescribePolicies = new ActionIdentifier("autoscaling:DescribePolicies");

		// Token: 0x04000565 RID: 1381
		public static readonly ActionIdentifier DescribeScalingActivities = new ActionIdentifier("autoscaling:DescribeScalingActivities");

		// Token: 0x04000566 RID: 1382
		public static readonly ActionIdentifier DescribeScalingProcessTypes = new ActionIdentifier("autoscaling:DescribeScalingProcessTypes");

		// Token: 0x04000567 RID: 1383
		public static readonly ActionIdentifier DescribeScheduledActions = new ActionIdentifier("autoscaling:DescribeScheduledActions");

		// Token: 0x04000568 RID: 1384
		public static readonly ActionIdentifier DescribeTags = new ActionIdentifier("autoscaling:DescribeTags");

		// Token: 0x04000569 RID: 1385
		public static readonly ActionIdentifier DescribeTriggers = new ActionIdentifier("autoscaling:DescribeTriggers");

		// Token: 0x0400056A RID: 1386
		public static readonly ActionIdentifier DisableMetricsCollection = new ActionIdentifier("autoscaling:DisableMetricsCollection");

		// Token: 0x0400056B RID: 1387
		public static readonly ActionIdentifier EnableMetricsCollection = new ActionIdentifier("autoscaling:EnableMetricsCollection");

		// Token: 0x0400056C RID: 1388
		public static readonly ActionIdentifier ExecutePolicy = new ActionIdentifier("autoscaling:ExecutePolicy");

		// Token: 0x0400056D RID: 1389
		public static readonly ActionIdentifier PutNotificationConfiguration = new ActionIdentifier("autoscaling:PutNotificationConfiguration");

		// Token: 0x0400056E RID: 1390
		public static readonly ActionIdentifier PutScalingPolicy = new ActionIdentifier("autoscaling:PutScalingPolicy");

		// Token: 0x0400056F RID: 1391
		public static readonly ActionIdentifier PutScheduledUpdateGroupAction = new ActionIdentifier("autoscaling:PutScheduledUpdateGroupAction");

		// Token: 0x04000570 RID: 1392
		public static readonly ActionIdentifier ResumeProcesses = new ActionIdentifier("autoscaling:ResumeProcesses");

		// Token: 0x04000571 RID: 1393
		public static readonly ActionIdentifier SetDesiredCapacity = new ActionIdentifier("autoscaling:SetDesiredCapacity");

		// Token: 0x04000572 RID: 1394
		public static readonly ActionIdentifier SetInstanceHealth = new ActionIdentifier("autoscaling:SetInstanceHealth");

		// Token: 0x04000573 RID: 1395
		public static readonly ActionIdentifier SuspendProcesses = new ActionIdentifier("autoscaling:SuspendProcesses");

		// Token: 0x04000574 RID: 1396
		public static readonly ActionIdentifier TerminateInstanceInAutoScalingGroup = new ActionIdentifier("autoscaling:TerminateInstanceInAutoScalingGroup");

		// Token: 0x04000575 RID: 1397
		public static readonly ActionIdentifier UpdateAutoScalingGroup = new ActionIdentifier("autoscaling:UpdateAutoScalingGroup");
	}
}
