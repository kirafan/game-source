﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000196 RID: 406
	public static class MobileAnalyticsActionIdentifiers
	{
		// Token: 0x0400078D RID: 1933
		public static readonly ActionIdentifier AllMobileAnalyticsActions = new ActionIdentifier("mobileanalytics:*");

		// Token: 0x0400078E RID: 1934
		public static readonly ActionIdentifier PutEvents = new ActionIdentifier("mobileanalytics:PutEvents");

		// Token: 0x0400078F RID: 1935
		public static readonly ActionIdentifier GetReports = new ActionIdentifier("mobileanalytics:GetReports");

		// Token: 0x04000790 RID: 1936
		public static readonly ActionIdentifier GetFinancialReports = new ActionIdentifier("mobileanalytics:GetFinancialReports");
	}
}
