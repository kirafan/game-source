﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x020001A0 RID: 416
	public static class SNSActionIdentifiers
	{
		// Token: 0x040008B4 RID: 2228
		public static readonly ActionIdentifier AllSNSActions = new ActionIdentifier("sns:*");

		// Token: 0x040008B5 RID: 2229
		public static readonly ActionIdentifier AddPermission = new ActionIdentifier("sns:AddPermission");

		// Token: 0x040008B6 RID: 2230
		public static readonly ActionIdentifier ConfirmSubscription = new ActionIdentifier("sns:ConfirmSubscription");

		// Token: 0x040008B7 RID: 2231
		public static readonly ActionIdentifier CreatePlatformApplication = new ActionIdentifier("sns:CreatePlatformApplication");

		// Token: 0x040008B8 RID: 2232
		public static readonly ActionIdentifier CreatePlatformEndpoint = new ActionIdentifier("sns:CreatePlatformEndpoint");

		// Token: 0x040008B9 RID: 2233
		public static readonly ActionIdentifier CreateTopic = new ActionIdentifier("sns:CreateTopic");

		// Token: 0x040008BA RID: 2234
		public static readonly ActionIdentifier DeleteEndpoint = new ActionIdentifier("sns:DeleteEndpoint");

		// Token: 0x040008BB RID: 2235
		public static readonly ActionIdentifier DeletePlatformApplication = new ActionIdentifier("sns:DeletePlatformApplication");

		// Token: 0x040008BC RID: 2236
		public static readonly ActionIdentifier DeleteTopic = new ActionIdentifier("sns:DeleteTopic");

		// Token: 0x040008BD RID: 2237
		public static readonly ActionIdentifier GetEndpointAttributes = new ActionIdentifier("sns:GetEndpointAttributes");

		// Token: 0x040008BE RID: 2238
		public static readonly ActionIdentifier GetPlatformApplicationAttributes = new ActionIdentifier("sns:GetPlatformApplicationAttributes");

		// Token: 0x040008BF RID: 2239
		public static readonly ActionIdentifier GetSubscriptionAttributes = new ActionIdentifier("sns:GetSubscriptionAttributes");

		// Token: 0x040008C0 RID: 2240
		public static readonly ActionIdentifier GetTopicAttributes = new ActionIdentifier("sns:GetTopicAttributes");

		// Token: 0x040008C1 RID: 2241
		public static readonly ActionIdentifier ListEndpointsByPlatformApplication = new ActionIdentifier("sns:ListEndpointsByPlatformApplication");

		// Token: 0x040008C2 RID: 2242
		public static readonly ActionIdentifier ListPlatformApplications = new ActionIdentifier("sns:ListPlatformApplications");

		// Token: 0x040008C3 RID: 2243
		public static readonly ActionIdentifier ListSubscriptions = new ActionIdentifier("sns:ListSubscriptions");

		// Token: 0x040008C4 RID: 2244
		public static readonly ActionIdentifier ListSubscriptionsByTopic = new ActionIdentifier("sns:ListSubscriptionsByTopic");

		// Token: 0x040008C5 RID: 2245
		public static readonly ActionIdentifier ListTopics = new ActionIdentifier("sns:ListTopics");

		// Token: 0x040008C6 RID: 2246
		public static readonly ActionIdentifier Publish = new ActionIdentifier("sns:Publish");

		// Token: 0x040008C7 RID: 2247
		public static readonly ActionIdentifier RemovePermission = new ActionIdentifier("sns:RemovePermission");

		// Token: 0x040008C8 RID: 2248
		public static readonly ActionIdentifier SetEndpointAttributes = new ActionIdentifier("sns:SetEndpointAttributes");

		// Token: 0x040008C9 RID: 2249
		public static readonly ActionIdentifier SetPlatformApplicationAttributes = new ActionIdentifier("sns:SetPlatformApplicationAttributes");

		// Token: 0x040008CA RID: 2250
		public static readonly ActionIdentifier SetSubscriptionAttributes = new ActionIdentifier("sns:SetSubscriptionAttributes");

		// Token: 0x040008CB RID: 2251
		public static readonly ActionIdentifier SetTopicAttributes = new ActionIdentifier("sns:SetTopicAttributes");

		// Token: 0x040008CC RID: 2252
		public static readonly ActionIdentifier Subscribe = new ActionIdentifier("sns:Subscribe");

		// Token: 0x040008CD RID: 2253
		public static readonly ActionIdentifier Unsubscribe = new ActionIdentifier("sns:Unsubscribe");
	}
}
