﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000195 RID: 405
	public static class MarketplaceManagementPortalActionIdentifiers
	{
		// Token: 0x04000788 RID: 1928
		public static readonly ActionIdentifier AllMarketplaceManagementPortalActions = new ActionIdentifier("aws-marketplace-management:*");

		// Token: 0x04000789 RID: 1929
		public static readonly ActionIdentifier uploadFiles = new ActionIdentifier("aws-marketplace-management:uploadFiles");

		// Token: 0x0400078A RID: 1930
		public static readonly ActionIdentifier viewMarketing = new ActionIdentifier("aws-marketplace-management:viewMarketing");

		// Token: 0x0400078B RID: 1931
		public static readonly ActionIdentifier viewReports = new ActionIdentifier("aws-marketplace-management:viewReports");

		// Token: 0x0400078C RID: 1932
		public static readonly ActionIdentifier viewSupport = new ActionIdentifier("aws-marketplace-management:viewSupport");
	}
}
