﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x02000187 RID: 391
	public static class CognitoSyncActionIdentifiers
	{
		// Token: 0x040005E4 RID: 1508
		public static readonly ActionIdentifier AllCognitoSyncActions = new ActionIdentifier("cognito-sync:*");

		// Token: 0x040005E5 RID: 1509
		public static readonly ActionIdentifier DeleteDataset = new ActionIdentifier("cognito-sync:DeleteDataset");

		// Token: 0x040005E6 RID: 1510
		public static readonly ActionIdentifier DescribeDataset = new ActionIdentifier("cognito-sync:DescribeDataset");

		// Token: 0x040005E7 RID: 1511
		public static readonly ActionIdentifier DescribeIdentityUsage = new ActionIdentifier("cognito-sync:DescribeIdentityUsage");

		// Token: 0x040005E8 RID: 1512
		public static readonly ActionIdentifier DescribeIdentityPoolUsage = new ActionIdentifier("cognito-sync:DescribeIdentityPoolUsage");

		// Token: 0x040005E9 RID: 1513
		public static readonly ActionIdentifier ListDatasets = new ActionIdentifier("cognito-sync:ListDatasets");

		// Token: 0x040005EA RID: 1514
		public static readonly ActionIdentifier ListIdentityPoolUsage = new ActionIdentifier("cognito-sync:ListIdentityPoolUsage");

		// Token: 0x040005EB RID: 1515
		public static readonly ActionIdentifier ListRecords = new ActionIdentifier("cognito-sync:ListRecords");

		// Token: 0x040005EC RID: 1516
		public static readonly ActionIdentifier UpdateRecords = new ActionIdentifier("cognito-sync:UpdateRecords");
	}
}
