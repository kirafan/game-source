﻿using System;

namespace Amazon.Auth.AccessControlPolicy.ActionIdentifiers
{
	// Token: 0x0200017D RID: 381
	public static class AppStreamActionIdentifiers
	{
		// Token: 0x04000541 RID: 1345
		public static readonly ActionIdentifier AllAppStreamActions = new ActionIdentifier("appstream:*");

		// Token: 0x04000542 RID: 1346
		public static readonly ActionIdentifier CreateApplication = new ActionIdentifier("appstream:CreateApplication");

		// Token: 0x04000543 RID: 1347
		public static readonly ActionIdentifier CreateSession = new ActionIdentifier("appstream:CreateSession");

		// Token: 0x04000544 RID: 1348
		public static readonly ActionIdentifier DeleteApplication = new ActionIdentifier("appstream:DeleteApplication");

		// Token: 0x04000545 RID: 1349
		public static readonly ActionIdentifier GetApiRoot = new ActionIdentifier("appstream:GetApiRoot");

		// Token: 0x04000546 RID: 1350
		public static readonly ActionIdentifier GetApplication = new ActionIdentifier("appstream:GetApplication");

		// Token: 0x04000547 RID: 1351
		public static readonly ActionIdentifier GetApplications = new ActionIdentifier("appstream:GetApplications");

		// Token: 0x04000548 RID: 1352
		public static readonly ActionIdentifier GetApplicationError = new ActionIdentifier("appstream:GetApplicationError");

		// Token: 0x04000549 RID: 1353
		public static readonly ActionIdentifier GetApplicationErrors = new ActionIdentifier("appstream:GetApplicationErrors");

		// Token: 0x0400054A RID: 1354
		public static readonly ActionIdentifier GetApplicationStatus = new ActionIdentifier("appstream:GetApplicationStatus");

		// Token: 0x0400054B RID: 1355
		public static readonly ActionIdentifier GetSession = new ActionIdentifier("appstream:GetSession");

		// Token: 0x0400054C RID: 1356
		public static readonly ActionIdentifier GetSessions = new ActionIdentifier("appstream:GetSessions");

		// Token: 0x0400054D RID: 1357
		public static readonly ActionIdentifier GetSessionStatus = new ActionIdentifier("appstream:GetSessionStatus");

		// Token: 0x0400054E RID: 1358
		public static readonly ActionIdentifier UpdateApplication = new ActionIdentifier("appstream:UpdateApplication");

		// Token: 0x0400054F RID: 1359
		public static readonly ActionIdentifier UpdateApplicationState = new ActionIdentifier("appstream:UpdateApplicationState");

		// Token: 0x04000550 RID: 1360
		public static readonly ActionIdentifier UpdateSessionState = new ActionIdentifier("appstream:UpdateSessionState");
	}
}
