﻿using System;

namespace Amazon.Auth.AccessControlPolicy
{
	// Token: 0x02000177 RID: 375
	public class Resource
	{
		// Token: 0x06000DE5 RID: 3557 RVA: 0x00021545 File Offset: 0x0001F745
		public Resource(string resource)
		{
			this.resource = resource;
		}

		// Token: 0x17000409 RID: 1033
		// (get) Token: 0x06000DE6 RID: 3558 RVA: 0x00021554 File Offset: 0x0001F754
		public string Id
		{
			get
			{
				return this.resource;
			}
		}

		// Token: 0x04000530 RID: 1328
		private string resource;
	}
}
