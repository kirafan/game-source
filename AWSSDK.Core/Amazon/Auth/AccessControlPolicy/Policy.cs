﻿using System;
using System.Collections.Generic;
using System.Linq;
using Amazon.Auth.AccessControlPolicy.Internal;

namespace Amazon.Auth.AccessControlPolicy
{
	// Token: 0x02000175 RID: 373
	public class Policy
	{
		// Token: 0x06000DCB RID: 3531 RVA: 0x00021153 File Offset: 0x0001F353
		public Policy()
		{
		}

		// Token: 0x06000DCC RID: 3532 RVA: 0x00021171 File Offset: 0x0001F371
		public Policy(string id)
		{
			this.id = id;
		}

		// Token: 0x06000DCD RID: 3533 RVA: 0x00021196 File Offset: 0x0001F396
		public Policy(string id, IList<Statement> statements)
		{
			this.id = id;
			this.statements = statements;
		}

		// Token: 0x17000404 RID: 1028
		// (get) Token: 0x06000DCE RID: 3534 RVA: 0x000211C2 File Offset: 0x0001F3C2
		// (set) Token: 0x06000DCF RID: 3535 RVA: 0x000211CA File Offset: 0x0001F3CA
		public string Id
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}

		// Token: 0x06000DD0 RID: 3536 RVA: 0x000211D3 File Offset: 0x0001F3D3
		public Policy WithId(string id)
		{
			this.Id = id;
			return this;
		}

		// Token: 0x17000405 RID: 1029
		// (get) Token: 0x06000DD1 RID: 3537 RVA: 0x000211DD File Offset: 0x0001F3DD
		// (set) Token: 0x06000DD2 RID: 3538 RVA: 0x000211E5 File Offset: 0x0001F3E5
		public string Version
		{
			get
			{
				return this.version;
			}
			set
			{
				this.version = value;
			}
		}

		// Token: 0x17000406 RID: 1030
		// (get) Token: 0x06000DD3 RID: 3539 RVA: 0x000211EE File Offset: 0x0001F3EE
		// (set) Token: 0x06000DD4 RID: 3540 RVA: 0x000211F6 File Offset: 0x0001F3F6
		public IList<Statement> Statements
		{
			get
			{
				return this.statements;
			}
			set
			{
				this.statements = value;
			}
		}

		// Token: 0x06000DD5 RID: 3541 RVA: 0x00021200 File Offset: 0x0001F400
		public bool CheckIfStatementExists(Statement statement)
		{
			if (this.Statements == null)
			{
				return false;
			}
			foreach (Statement statement2 in this.Statements)
			{
				if (statement2.Effect == statement.Effect && Policy.StatementContainsResources(statement2, statement.Resources) && Policy.StatementContainsActions(statement2, statement.Actions) && Policy.StatementContainsConditions(statement2, statement.Conditions) && Policy.StatementContainsPrincipals(statement2, statement.Principals))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000DD6 RID: 3542 RVA: 0x000212A0 File Offset: 0x0001F4A0
		private static bool StatementContainsResources(Statement statement, IList<Resource> resources)
		{
			using (IEnumerator<Resource> enumerator = resources.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					Resource resource = enumerator.Current;
					if (statement.Resources.FirstOrDefault((Resource x) => string.Equals(x.Id, resource.Id)) == null)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06000DD7 RID: 3543 RVA: 0x0002130C File Offset: 0x0001F50C
		private static bool StatementContainsActions(Statement statement, IList<ActionIdentifier> actions)
		{
			using (IEnumerator<ActionIdentifier> enumerator = actions.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					ActionIdentifier action = enumerator.Current;
					if (statement.Actions.FirstOrDefault((ActionIdentifier x) => string.Equals(x.ActionName, action.ActionName)) == null)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06000DD8 RID: 3544 RVA: 0x00021378 File Offset: 0x0001F578
		private static bool StatementContainsConditions(Statement statement, IList<Condition> conditions)
		{
			using (IEnumerator<Condition> enumerator = conditions.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					Condition condition = enumerator.Current;
					if (statement.Conditions.FirstOrDefault((Condition x) => string.Equals(x.Type, condition.Type) && string.Equals(x.ConditionKey, condition.ConditionKey) && x.Values.Intersect(condition.Values).Count<string>() == condition.Values.Count<string>()) == null)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06000DD9 RID: 3545 RVA: 0x000213E4 File Offset: 0x0001F5E4
		private static bool StatementContainsPrincipals(Statement statement, IList<Principal> principals)
		{
			using (IEnumerator<Principal> enumerator = principals.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					Principal principal = enumerator.Current;
					if (statement.Principals.FirstOrDefault((Principal x) => string.Equals(x.Id, principal.Id) && string.Equals(x.Provider, principal.Provider)) == null)
					{
						return false;
					}
				}
			}
			return true;
		}

		// Token: 0x06000DDA RID: 3546 RVA: 0x00021450 File Offset: 0x0001F650
		public Policy WithStatements(params Statement[] statements)
		{
			if (this.Statements == null)
			{
				this.Statements = new List<Statement>();
			}
			foreach (Statement item in statements)
			{
				this.Statements.Add(item);
			}
			return this;
		}

		// Token: 0x06000DDB RID: 3547 RVA: 0x00021491 File Offset: 0x0001F691
		public string ToJson()
		{
			return this.ToJson(true);
		}

		// Token: 0x06000DDC RID: 3548 RVA: 0x0002149A File Offset: 0x0001F69A
		public string ToJson(bool prettyPrint)
		{
			return JsonPolicyWriter.WritePolicyToString(prettyPrint, this);
		}

		// Token: 0x06000DDD RID: 3549 RVA: 0x000214A3 File Offset: 0x0001F6A3
		public static Policy FromJson(string json)
		{
			return JsonPolicyReader.ReadJsonStringToPolicy(json);
		}

		// Token: 0x04000523 RID: 1315
		private const string DEFAULT_POLICY_VERSION = "2012-10-17";

		// Token: 0x04000524 RID: 1316
		private string id;

		// Token: 0x04000525 RID: 1317
		private string version = "2012-10-17";

		// Token: 0x04000526 RID: 1318
		private IList<Statement> statements = new List<Statement>();
	}
}
