﻿using System;
using System.Globalization;

namespace Amazon.Auth.AccessControlPolicy
{
	// Token: 0x02000174 RID: 372
	public static class ConditionFactory
	{
		// Token: 0x06000DBF RID: 3519 RVA: 0x00021030 File Offset: 0x0001F230
		public static Condition NewCondition(ConditionFactory.ArnComparisonType type, string key, string value)
		{
			return new Condition(type.ToString(), key, new string[]
			{
				value
			});
		}

		// Token: 0x06000DC0 RID: 3520 RVA: 0x0002104F File Offset: 0x0001F24F
		public static Condition NewCondition(string key, bool value)
		{
			return new Condition("Bool", key, new string[]
			{
				value.ToString().ToLowerInvariant()
			});
		}

		// Token: 0x06000DC1 RID: 3521 RVA: 0x00021071 File Offset: 0x0001F271
		public static Condition NewCondition(ConditionFactory.DateComparisonType type, DateTime date)
		{
			return new Condition(type.ToString(), "aws:CurrentTime", new string[]
			{
				date.ToString("yyyy-MM-dd\\THH:mm:ss.fff\\Z", CultureInfo.InvariantCulture)
			});
		}

		// Token: 0x06000DC2 RID: 3522 RVA: 0x000210A4 File Offset: 0x0001F2A4
		public static Condition NewIpAddressCondition(string ipAddressRange)
		{
			return ConditionFactory.NewCondition(ConditionFactory.IpAddressComparisonType.IpAddress, ipAddressRange);
		}

		// Token: 0x06000DC3 RID: 3523 RVA: 0x000210AD File Offset: 0x0001F2AD
		public static Condition NewCondition(ConditionFactory.IpAddressComparisonType type, string ipAddressRange)
		{
			return new Condition(type.ToString(), "aws:SourceIp", new string[]
			{
				ipAddressRange
			});
		}

		// Token: 0x06000DC4 RID: 3524 RVA: 0x000210D0 File Offset: 0x0001F2D0
		public static Condition NewCondition(ConditionFactory.NumericComparisonType type, string key, string value)
		{
			return new Condition(type.ToString(), key, new string[]
			{
				value
			});
		}

		// Token: 0x06000DC5 RID: 3525 RVA: 0x000210EF File Offset: 0x0001F2EF
		public static Condition NewCondition(ConditionFactory.StringComparisonType type, string key, string value)
		{
			return new Condition(type.ToString(), key, new string[]
			{
				value
			});
		}

		// Token: 0x06000DC6 RID: 3526 RVA: 0x0002110E File Offset: 0x0001F30E
		public static Condition NewSourceArnCondition(string arnPattern)
		{
			return ConditionFactory.NewCondition(ConditionFactory.ArnComparisonType.ArnLike, "aws:SourceArn", arnPattern);
		}

		// Token: 0x06000DC7 RID: 3527 RVA: 0x0002111C File Offset: 0x0001F31C
		public static Condition NewSecureTransportCondition()
		{
			return ConditionFactory.NewCondition("aws:SecureTransport", true);
		}

		// Token: 0x06000DC8 RID: 3528 RVA: 0x00021129 File Offset: 0x0001F329
		public static Condition NewCannedACLCondition(string cannedAcl)
		{
			return ConditionFactory.NewCondition(ConditionFactory.StringComparisonType.StringEquals, "s3:x-amz-acl", cannedAcl);
		}

		// Token: 0x06000DC9 RID: 3529 RVA: 0x00021137 File Offset: 0x0001F337
		public static Condition NewEndpointCondition(string endpointPattern)
		{
			return ConditionFactory.NewCondition(ConditionFactory.StringComparisonType.StringLike, "sns:Endpoint", endpointPattern);
		}

		// Token: 0x06000DCA RID: 3530 RVA: 0x00021145 File Offset: 0x0001F345
		public static Condition NewProtocolCondition(string protocol)
		{
			return ConditionFactory.NewCondition(ConditionFactory.StringComparisonType.StringEquals, "sns:Protocol", protocol);
		}

		// Token: 0x04000512 RID: 1298
		public const string CURRENT_TIME_CONDITION_KEY = "aws:CurrentTime";

		// Token: 0x04000513 RID: 1299
		public const string SECURE_TRANSPORT_CONDITION_KEY = "aws:SecureTransport";

		// Token: 0x04000514 RID: 1300
		public const string SOURCE_IP_CONDITION_KEY = "aws:SourceIp";

		// Token: 0x04000515 RID: 1301
		public const string USER_AGENT_CONDITION_KEY = "aws:UserAgent";

		// Token: 0x04000516 RID: 1302
		public const string EPOCH_TIME_CONDITION_KEY = "aws:EpochTime";

		// Token: 0x04000517 RID: 1303
		public const string REFERRER_CONDITION_KEY = "aws:Referer";

		// Token: 0x04000518 RID: 1304
		public const string SOURCE_ARN_CONDITION_KEY = "aws:SourceArn";

		// Token: 0x04000519 RID: 1305
		public const string S3_CANNED_ACL_CONDITION_KEY = "s3:x-amz-acl";

		// Token: 0x0400051A RID: 1306
		public const string S3_LOCATION_CONSTRAINT_CONDITION_KEY = "s3:LocationConstraint";

		// Token: 0x0400051B RID: 1307
		public const string S3_PREFIX_CONDITION_KEY = "s3:prefix";

		// Token: 0x0400051C RID: 1308
		public const string S3_DELIMITER_CONDITION_KEY = "s3:delimiter";

		// Token: 0x0400051D RID: 1309
		public const string S3_MAX_KEYS_CONDITION_KEY = "s3:max-keys";

		// Token: 0x0400051E RID: 1310
		public const string S3_COPY_SOURCE_CONDITION_KEY = "s3:x-amz-copy-source";

		// Token: 0x0400051F RID: 1311
		public const string S3_METADATA_DIRECTIVE_CONDITION_KEY = "s3:x-amz-metadata-directive";

		// Token: 0x04000520 RID: 1312
		public const string S3_VERSION_ID_CONDITION_KEY = "s3:VersionId";

		// Token: 0x04000521 RID: 1313
		public const string SNS_ENDPOINT_CONDITION_KEY = "sns:Endpoint";

		// Token: 0x04000522 RID: 1314
		public const string SNS_PROTOCOL_CONDITION_KEY = "sns:Protocol";

		// Token: 0x020001F5 RID: 501
		public enum ArnComparisonType
		{
			// Token: 0x04000A0F RID: 2575
			ArnEquals,
			// Token: 0x04000A10 RID: 2576
			ArnLike,
			// Token: 0x04000A11 RID: 2577
			ArnNotEquals,
			// Token: 0x04000A12 RID: 2578
			ArnNotLike
		}

		// Token: 0x020001F6 RID: 502
		public enum DateComparisonType
		{
			// Token: 0x04000A14 RID: 2580
			DateEquals,
			// Token: 0x04000A15 RID: 2581
			DateGreaterThan,
			// Token: 0x04000A16 RID: 2582
			DateGreaterThanEquals,
			// Token: 0x04000A17 RID: 2583
			DateLessThan,
			// Token: 0x04000A18 RID: 2584
			DateLessThanEquals,
			// Token: 0x04000A19 RID: 2585
			DateNotEquals
		}

		// Token: 0x020001F7 RID: 503
		public enum IpAddressComparisonType
		{
			// Token: 0x04000A1B RID: 2587
			IpAddress,
			// Token: 0x04000A1C RID: 2588
			NotIpAddress
		}

		// Token: 0x020001F8 RID: 504
		public enum NumericComparisonType
		{
			// Token: 0x04000A1E RID: 2590
			NumericEquals,
			// Token: 0x04000A1F RID: 2591
			NumericGreaterThan,
			// Token: 0x04000A20 RID: 2592
			NumericGreaterThanEquals,
			// Token: 0x04000A21 RID: 2593
			NumericLessThan,
			// Token: 0x04000A22 RID: 2594
			NumericLessThanEquals,
			// Token: 0x04000A23 RID: 2595
			NumericNotEquals
		}

		// Token: 0x020001F9 RID: 505
		public enum StringComparisonType
		{
			// Token: 0x04000A25 RID: 2597
			StringEquals,
			// Token: 0x04000A26 RID: 2598
			StringEqualsIgnoreCase,
			// Token: 0x04000A27 RID: 2599
			StringLike,
			// Token: 0x04000A28 RID: 2600
			StringNotEquals,
			// Token: 0x04000A29 RID: 2601
			StringNotEqualsIgnoreCase,
			// Token: 0x04000A2A RID: 2602
			StringNotLike
		}
	}
}
