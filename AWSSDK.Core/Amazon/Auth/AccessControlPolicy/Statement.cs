﻿using System;
using System.Collections.Generic;

namespace Amazon.Auth.AccessControlPolicy
{
	// Token: 0x02000179 RID: 377
	public class Statement
	{
		// Token: 0x06000DEB RID: 3563 RVA: 0x00021600 File Offset: 0x0001F800
		public Statement(Statement.StatementEffect effect)
		{
			this.effect = effect;
			this.id = Guid.NewGuid().ToString().Replace("-", "");
		}

		// Token: 0x1700040A RID: 1034
		// (get) Token: 0x06000DEC RID: 3564 RVA: 0x0002166E File Offset: 0x0001F86E
		// (set) Token: 0x06000DED RID: 3565 RVA: 0x00021676 File Offset: 0x0001F876
		public string Id
		{
			get
			{
				return this.id;
			}
			set
			{
				this.id = value;
			}
		}

		// Token: 0x06000DEE RID: 3566 RVA: 0x0002167F File Offset: 0x0001F87F
		public Statement WithId(string id)
		{
			this.Id = id;
			return this;
		}

		// Token: 0x1700040B RID: 1035
		// (get) Token: 0x06000DEF RID: 3567 RVA: 0x00021689 File Offset: 0x0001F889
		// (set) Token: 0x06000DF0 RID: 3568 RVA: 0x00021691 File Offset: 0x0001F891
		public Statement.StatementEffect Effect
		{
			get
			{
				return this.effect;
			}
			set
			{
				this.effect = value;
			}
		}

		// Token: 0x1700040C RID: 1036
		// (get) Token: 0x06000DF1 RID: 3569 RVA: 0x0002169A File Offset: 0x0001F89A
		// (set) Token: 0x06000DF2 RID: 3570 RVA: 0x000216A2 File Offset: 0x0001F8A2
		public IList<ActionIdentifier> Actions
		{
			get
			{
				return this.actions;
			}
			set
			{
				this.actions = value;
			}
		}

		// Token: 0x06000DF3 RID: 3571 RVA: 0x000216AC File Offset: 0x0001F8AC
		public Statement WithActionIdentifiers(params ActionIdentifier[] actions)
		{
			if (this.actions == null)
			{
				this.actions = new List<ActionIdentifier>();
			}
			foreach (ActionIdentifier item in actions)
			{
				this.actions.Add(item);
			}
			return this;
		}

		// Token: 0x1700040D RID: 1037
		// (get) Token: 0x06000DF4 RID: 3572 RVA: 0x000216ED File Offset: 0x0001F8ED
		// (set) Token: 0x06000DF5 RID: 3573 RVA: 0x000216F5 File Offset: 0x0001F8F5
		public IList<Resource> Resources
		{
			get
			{
				return this.resources;
			}
			set
			{
				this.resources = value;
			}
		}

		// Token: 0x06000DF6 RID: 3574 RVA: 0x00021700 File Offset: 0x0001F900
		public Statement WithResources(params Resource[] resources)
		{
			if (this.resources == null)
			{
				this.resources = new List<Resource>();
			}
			foreach (Resource item in resources)
			{
				this.resources.Add(item);
			}
			return this;
		}

		// Token: 0x1700040E RID: 1038
		// (get) Token: 0x06000DF7 RID: 3575 RVA: 0x00021741 File Offset: 0x0001F941
		// (set) Token: 0x06000DF8 RID: 3576 RVA: 0x00021749 File Offset: 0x0001F949
		public IList<Condition> Conditions
		{
			get
			{
				return this.conditions;
			}
			set
			{
				this.conditions = value;
			}
		}

		// Token: 0x06000DF9 RID: 3577 RVA: 0x00021754 File Offset: 0x0001F954
		public Statement WithConditions(params Condition[] conditions)
		{
			if (this.Conditions == null)
			{
				this.Conditions = new List<Condition>();
			}
			foreach (Condition item in conditions)
			{
				this.Conditions.Add(item);
			}
			return this;
		}

		// Token: 0x1700040F RID: 1039
		// (get) Token: 0x06000DFA RID: 3578 RVA: 0x00021795 File Offset: 0x0001F995
		// (set) Token: 0x06000DFB RID: 3579 RVA: 0x0002179D File Offset: 0x0001F99D
		public IList<Principal> Principals
		{
			get
			{
				return this.principals;
			}
			set
			{
				this.principals = value;
			}
		}

		// Token: 0x06000DFC RID: 3580 RVA: 0x000217A8 File Offset: 0x0001F9A8
		public Statement WithPrincipals(params Principal[] principals)
		{
			if (this.principals == null)
			{
				this.principals = new List<Principal>();
			}
			foreach (Principal item in principals)
			{
				this.principals.Add(item);
			}
			return this;
		}

		// Token: 0x04000531 RID: 1329
		private string id;

		// Token: 0x04000532 RID: 1330
		private Statement.StatementEffect effect;

		// Token: 0x04000533 RID: 1331
		private IList<Principal> principals = new List<Principal>();

		// Token: 0x04000534 RID: 1332
		private IList<ActionIdentifier> actions = new List<ActionIdentifier>();

		// Token: 0x04000535 RID: 1333
		private IList<Resource> resources = new List<Resource>();

		// Token: 0x04000536 RID: 1334
		private IList<Condition> conditions = new List<Condition>();

		// Token: 0x020001FE RID: 510
		public enum StatementEffect
		{
			// Token: 0x04000A30 RID: 2608
			Allow,
			// Token: 0x04000A31 RID: 2609
			Deny
		}
	}
}
