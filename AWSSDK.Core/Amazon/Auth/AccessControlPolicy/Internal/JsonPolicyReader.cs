﻿using System;
using System.Collections;
using System.Collections.Generic;
using ThirdParty.Json.LitJson;

namespace Amazon.Auth.AccessControlPolicy.Internal
{
	// Token: 0x0200017B RID: 379
	internal static class JsonPolicyReader
	{
		// Token: 0x06000DFD RID: 3581 RVA: 0x000217EC File Offset: 0x0001F9EC
		public static Policy ReadJsonStringToPolicy(string jsonString)
		{
			Policy policy = new Policy();
			JsonData jsonData = JsonMapper.ToObject(jsonString);
			if (jsonData["Id"] != null && jsonData["Id"].IsString)
			{
				policy.Id = (string)jsonData["Id"];
			}
			JsonData jsonData2 = jsonData["Statement"];
			if (jsonData2 != null && jsonData2.IsArray)
			{
				foreach (object obj in ((IEnumerable)jsonData2))
				{
					Statement statement = JsonPolicyReader.convertStatement((JsonData)obj);
					if (statement != null)
					{
						policy.Statements.Add(statement);
					}
				}
			}
			return policy;
		}

		// Token: 0x06000DFE RID: 3582 RVA: 0x000218B0 File Offset: 0x0001FAB0
		private static Statement convertStatement(JsonData jStatement)
		{
			if (jStatement["Effect"] == null || !jStatement["Effect"].IsString)
			{
				return null;
			}
			string value = (string)jStatement["Effect"];
			Statement.StatementEffect effect;
			if ("Allow".Equals(value))
			{
				effect = Statement.StatementEffect.Allow;
			}
			else
			{
				effect = Statement.StatementEffect.Deny;
			}
			Statement statement = new Statement(effect);
			if (jStatement["Sid"] != null && jStatement["Sid"].IsString)
			{
				statement.Id = (string)jStatement["Sid"];
			}
			JsonPolicyReader.convertActions(statement, jStatement);
			JsonPolicyReader.convertResources(statement, jStatement);
			JsonPolicyReader.convertCondition(statement, jStatement);
			JsonPolicyReader.convertPrincipals(statement, jStatement);
			return statement;
		}

		// Token: 0x06000DFF RID: 3583 RVA: 0x0002195C File Offset: 0x0001FB5C
		private static void convertPrincipals(Statement statement, JsonData jStatement)
		{
			JsonData jsonData = jStatement["Principal"];
			if (jsonData == null)
			{
				return;
			}
			if (jsonData.IsObject)
			{
				JsonPolicyReader.convertPrincipalRecord(statement, jsonData);
				return;
			}
			if (jsonData.IsArray)
			{
				using (IEnumerator enumerator = ((IEnumerable)jsonData).GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						JsonData jPrincipal = (JsonData)obj;
						JsonPolicyReader.convertPrincipalRecord(statement, jPrincipal);
					}
					return;
				}
			}
			if (jsonData.IsString && jsonData.Equals("*"))
			{
				statement.Principals.Add(Principal.Anonymous);
			}
		}

		// Token: 0x06000E00 RID: 3584 RVA: 0x00021A04 File Offset: 0x0001FC04
		private static void convertPrincipalRecord(Statement statement, JsonData jPrincipal)
		{
			foreach (object obj in ((IEnumerable)jPrincipal))
			{
				KeyValuePair<string, JsonData> keyValuePair = (KeyValuePair<string, JsonData>)obj;
				if (keyValuePair.Value != null)
				{
					if (keyValuePair.Value.IsArray)
					{
						using (IEnumerator enumerator2 = ((IEnumerable)keyValuePair.Value).GetEnumerator())
						{
							while (enumerator2.MoveNext())
							{
								object obj2 = enumerator2.Current;
								JsonData jsonData = (JsonData)obj2;
								if (jsonData.IsString)
								{
									Principal item = new Principal(keyValuePair.Key, (string)jsonData, false);
									statement.Principals.Add(item);
								}
							}
							continue;
						}
					}
					if (keyValuePair.Value.IsString)
					{
						Principal item2 = new Principal(keyValuePair.Key, (string)keyValuePair.Value, false);
						statement.Principals.Add(item2);
					}
				}
			}
		}

		// Token: 0x06000E01 RID: 3585 RVA: 0x00021B1C File Offset: 0x0001FD1C
		private static void convertActions(Statement statement, JsonData jStatement)
		{
			JsonData jsonData = jStatement["Action"];
			if (jsonData == null)
			{
				return;
			}
			if (jsonData.IsString)
			{
				statement.Actions.Add(new ActionIdentifier((string)jsonData));
				return;
			}
			if (jsonData.IsArray)
			{
				foreach (object obj in ((IEnumerable)jsonData))
				{
					JsonData jsonData2 = (JsonData)obj;
					if (jsonData2.IsString)
					{
						statement.Actions.Add(new ActionIdentifier((string)jsonData2));
					}
				}
			}
		}

		// Token: 0x06000E02 RID: 3586 RVA: 0x00021BC0 File Offset: 0x0001FDC0
		private static void convertResources(Statement statement, JsonData jStatement)
		{
			JsonData jsonData = jStatement["Resource"];
			if (jsonData == null)
			{
				return;
			}
			if (jsonData.IsString)
			{
				statement.Resources.Add(new Resource((string)jsonData));
				return;
			}
			if (jsonData.IsArray)
			{
				foreach (object obj in ((IEnumerable)jsonData))
				{
					JsonData jsonData2 = (JsonData)obj;
					if (jsonData2.IsString)
					{
						statement.Resources.Add(new Resource((string)jsonData2));
					}
				}
			}
		}

		// Token: 0x06000E03 RID: 3587 RVA: 0x00021C64 File Offset: 0x0001FE64
		private static void convertCondition(Statement statement, JsonData jStatement)
		{
			JsonData jsonData = jStatement["Condition"];
			if (jsonData == null)
			{
				return;
			}
			if (jsonData.IsObject)
			{
				JsonPolicyReader.convertConditionRecord(statement, jsonData);
				return;
			}
			if (jsonData.IsArray)
			{
				foreach (object obj in ((IEnumerable)jsonData))
				{
					JsonData jCondition = (JsonData)obj;
					JsonPolicyReader.convertConditionRecord(statement, jCondition);
				}
			}
		}

		// Token: 0x06000E04 RID: 3588 RVA: 0x00021CE0 File Offset: 0x0001FEE0
		private static void convertConditionRecord(Statement statement, JsonData jCondition)
		{
			foreach (object obj in ((IEnumerable)jCondition))
			{
				KeyValuePair<string, JsonData> keyValuePair = (KeyValuePair<string, JsonData>)obj;
				string key = keyValuePair.Key;
				foreach (object obj2 in ((IEnumerable)keyValuePair.Value))
				{
					KeyValuePair<string, JsonData> keyValuePair2 = (KeyValuePair<string, JsonData>)obj2;
					string key2 = keyValuePair2.Key;
					List<string> list = new List<string>();
					if (keyValuePair2.Value != null)
					{
						if (keyValuePair2.Value.IsString)
						{
							list.Add((string)keyValuePair2.Value);
						}
						else if (keyValuePair2.Value.IsArray)
						{
							foreach (object obj3 in ((IEnumerable)keyValuePair2.Value))
							{
								JsonData jsonData = (JsonData)obj3;
								if (jsonData.IsString)
								{
									list.Add((string)jsonData);
								}
							}
						}
					}
					Condition item = new Condition(key, key2, list.ToArray());
					statement.Conditions.Add(item);
				}
			}
		}
	}
}
