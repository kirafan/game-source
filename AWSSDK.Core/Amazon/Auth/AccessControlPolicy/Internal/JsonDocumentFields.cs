﻿using System;

namespace Amazon.Auth.AccessControlPolicy.Internal
{
	// Token: 0x0200017A RID: 378
	internal static class JsonDocumentFields
	{
		// Token: 0x04000537 RID: 1335
		internal const string VERSION = "Version";

		// Token: 0x04000538 RID: 1336
		internal const string POLICY_ID = "Id";

		// Token: 0x04000539 RID: 1337
		internal const string STATEMENT = "Statement";

		// Token: 0x0400053A RID: 1338
		internal const string STATEMENT_EFFECT = "Effect";

		// Token: 0x0400053B RID: 1339
		internal const string EFFECT_VALUE_ALLOW = "Allow";

		// Token: 0x0400053C RID: 1340
		internal const string STATEMENT_ID = "Sid";

		// Token: 0x0400053D RID: 1341
		internal const string PRINCIPAL = "Principal";

		// Token: 0x0400053E RID: 1342
		internal const string ACTION = "Action";

		// Token: 0x0400053F RID: 1343
		internal const string RESOURCE = "Resource";

		// Token: 0x04000540 RID: 1344
		internal const string CONDITION = "Condition";
	}
}
