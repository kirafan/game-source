﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using ThirdParty.Json.LitJson;

namespace Amazon.Auth.AccessControlPolicy.Internal
{
	// Token: 0x0200017C RID: 380
	internal static class JsonPolicyWriter
	{
		// Token: 0x06000E05 RID: 3589 RVA: 0x00021E7C File Offset: 0x0002007C
		public static string WritePolicyToString(bool prettyPrint, Policy policy)
		{
			if (policy == null)
			{
				throw new ArgumentNullException("policy");
			}
			StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture);
			string result;
			try
			{
				JsonPolicyWriter.writePolicy(policy, new JsonWriter(stringWriter)
				{
					IndentValue = 4,
					PrettyPrint = prettyPrint
				});
				result = stringWriter.ToString().Trim();
			}
			catch (Exception ex)
			{
				throw new ArgumentException("Unable to serialize policy to JSON string: " + ex.Message, ex);
			}
			return result;
		}

		// Token: 0x06000E06 RID: 3590 RVA: 0x00021EF8 File Offset: 0x000200F8
		private static void writePolicy(Policy policy, JsonWriter generator)
		{
			generator.WriteObjectStart();
			JsonPolicyWriter.writePropertyValue(generator, "Version", policy.Version);
			if (policy.Id != null)
			{
				JsonPolicyWriter.writePropertyValue(generator, "Id", policy.Id);
			}
			generator.WritePropertyName("Statement");
			generator.WriteArrayStart();
			foreach (Statement statement in policy.Statements)
			{
				generator.WriteObjectStart();
				if (statement.Id != null)
				{
					JsonPolicyWriter.writePropertyValue(generator, "Sid", statement.Id);
				}
				JsonPolicyWriter.writePropertyValue(generator, "Effect", statement.Effect.ToString());
				JsonPolicyWriter.writePrincipals(statement, generator);
				JsonPolicyWriter.writeActions(statement, generator);
				JsonPolicyWriter.writeResources(statement, generator);
				JsonPolicyWriter.writeConditions(statement, generator);
				generator.WriteObjectEnd();
			}
			generator.WriteArrayEnd();
			generator.WriteObjectEnd();
		}

		// Token: 0x06000E07 RID: 3591 RVA: 0x00021FEC File Offset: 0x000201EC
		private static void writePrincipals(Statement statement, JsonWriter generator)
		{
			IList<Principal> principals = statement.Principals;
			if (principals == null || principals.Count == 0)
			{
				return;
			}
			generator.WritePropertyName("Principal");
			if (principals.Count == 1 && principals[0] != null && principals[0].Provider.Equals("__ANONYMOUS__", StringComparison.Ordinal))
			{
				generator.Write("*");
				return;
			}
			generator.WriteObjectStart();
			Dictionary<string, List<string>> dictionary = new Dictionary<string, List<string>>();
			foreach (Principal principal in principals)
			{
				List<string> list;
				if (!dictionary.TryGetValue(principal.Provider, out list))
				{
					list = new List<string>();
					dictionary[principal.Provider] = list;
				}
				list.Add(principal.Id);
			}
			foreach (string text in dictionary.Keys)
			{
				generator.WritePropertyName(text);
				if (dictionary[text].Count > 1)
				{
					generator.WriteArrayStart();
				}
				foreach (string str in dictionary[text])
				{
					generator.Write(str);
				}
				if (dictionary[text].Count > 1)
				{
					generator.WriteArrayEnd();
				}
			}
			generator.WriteObjectEnd();
		}

		// Token: 0x06000E08 RID: 3592 RVA: 0x00022184 File Offset: 0x00020384
		private static void writeActions(Statement statement, JsonWriter generator)
		{
			IList<ActionIdentifier> actions = statement.Actions;
			if (actions == null || actions.Count == 0)
			{
				return;
			}
			generator.WritePropertyName("Action");
			if (actions.Count > 1)
			{
				generator.WriteArrayStart();
			}
			foreach (ActionIdentifier actionIdentifier in actions)
			{
				generator.Write(actionIdentifier.ActionName);
			}
			if (actions.Count > 1)
			{
				generator.WriteArrayEnd();
			}
		}

		// Token: 0x06000E09 RID: 3593 RVA: 0x00022210 File Offset: 0x00020410
		private static void writeResources(Statement statement, JsonWriter generator)
		{
			IList<Resource> resources = statement.Resources;
			if (resources == null || resources.Count == 0)
			{
				return;
			}
			generator.WritePropertyName("Resource");
			if (resources.Count > 1)
			{
				generator.WriteArrayStart();
			}
			foreach (Resource resource in resources)
			{
				generator.Write(resource.Id);
			}
			if (resources.Count > 1)
			{
				generator.WriteArrayEnd();
			}
		}

		// Token: 0x06000E0A RID: 3594 RVA: 0x0002229C File Offset: 0x0002049C
		private static void writeConditions(Statement statement, JsonWriter generator)
		{
			IList<Condition> conditions = statement.Conditions;
			if (conditions == null || conditions.Count == 0)
			{
				return;
			}
			Dictionary<string, Dictionary<string, List<string>>> dictionary = JsonPolicyWriter.sortConditionsByTypeAndKey(conditions);
			generator.WritePropertyName("Condition");
			generator.WriteObjectStart();
			foreach (KeyValuePair<string, Dictionary<string, List<string>>> keyValuePair in dictionary)
			{
				generator.WritePropertyName(keyValuePair.Key);
				generator.WriteObjectStart();
				foreach (KeyValuePair<string, List<string>> keyValuePair2 in keyValuePair.Value)
				{
					IList<string> value = keyValuePair2.Value;
					if (value.Count != 0)
					{
						generator.WritePropertyName(keyValuePair2.Key);
						if (value.Count > 1)
						{
							generator.WriteArrayStart();
						}
						if (value != null && value.Count != 0)
						{
							foreach (string str in value)
							{
								generator.Write(str);
							}
						}
						if (value.Count > 1)
						{
							generator.WriteArrayEnd();
						}
					}
				}
				generator.WriteObjectEnd();
			}
			generator.WriteObjectEnd();
		}

		// Token: 0x06000E0B RID: 3595 RVA: 0x00022400 File Offset: 0x00020600
		private static Dictionary<string, Dictionary<string, List<string>>> sortConditionsByTypeAndKey(IList<Condition> conditions)
		{
			Dictionary<string, Dictionary<string, List<string>>> dictionary = new Dictionary<string, Dictionary<string, List<string>>>();
			foreach (Condition condition in conditions)
			{
				string type = condition.Type;
				string conditionKey = condition.ConditionKey;
				Dictionary<string, List<string>> dictionary2;
				if (!dictionary.TryGetValue(type, out dictionary2))
				{
					dictionary2 = new Dictionary<string, List<string>>();
					dictionary[type] = dictionary2;
				}
				List<string> list;
				if (!dictionary2.TryGetValue(conditionKey, out list))
				{
					list = new List<string>();
					dictionary2[conditionKey] = list;
				}
				if (condition.Values != null)
				{
					foreach (string item in condition.Values)
					{
						list.Add(item);
					}
				}
			}
			return dictionary;
		}

		// Token: 0x06000E0C RID: 3596 RVA: 0x000224CC File Offset: 0x000206CC
		private static void writePropertyValue(JsonWriter generator, string propertyName, string value)
		{
			generator.WritePropertyName(propertyName);
			generator.Write(value);
		}
	}
}
