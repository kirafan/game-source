﻿using System;

namespace Amazon.Auth.AccessControlPolicy
{
	// Token: 0x02000176 RID: 374
	public class Principal
	{
		// Token: 0x06000DDE RID: 3550 RVA: 0x000214AB File Offset: 0x0001F6AB
		public Principal(string accountId) : this("AWS", accountId)
		{
			if (accountId == null)
			{
				throw new ArgumentNullException("accountId");
			}
		}

		// Token: 0x06000DDF RID: 3551 RVA: 0x000214C7 File Offset: 0x0001F6C7
		public Principal(string provider, string id) : this(provider, id, provider == "AWS")
		{
		}

		// Token: 0x06000DE0 RID: 3552 RVA: 0x000214DC File Offset: 0x0001F6DC
		public Principal(string provider, string id, bool stripHyphen)
		{
			this.provider = provider;
			if (stripHyphen)
			{
				id = id.Replace("-", "");
			}
			this.id = id;
		}

		// Token: 0x17000407 RID: 1031
		// (get) Token: 0x06000DE1 RID: 3553 RVA: 0x00021507 File Offset: 0x0001F707
		// (set) Token: 0x06000DE2 RID: 3554 RVA: 0x0002150F File Offset: 0x0001F70F
		public string Provider
		{
			get
			{
				return this.provider;
			}
			set
			{
				this.provider = value;
			}
		}

		// Token: 0x17000408 RID: 1032
		// (get) Token: 0x06000DE3 RID: 3555 RVA: 0x00021518 File Offset: 0x0001F718
		public string Id
		{
			get
			{
				return this.id;
			}
		}

		// Token: 0x04000527 RID: 1319
		public static readonly Principal AllUsers = new Principal("*");

		// Token: 0x04000528 RID: 1320
		public static readonly Principal Anonymous = new Principal("__ANONYMOUS__", "*");

		// Token: 0x04000529 RID: 1321
		public const string AWS_PROVIDER = "AWS";

		// Token: 0x0400052A RID: 1322
		public const string CANONICAL_USER_PROVIDER = "CanonicalUser";

		// Token: 0x0400052B RID: 1323
		public const string FEDERATED_PROVIDER = "Federated";

		// Token: 0x0400052C RID: 1324
		public const string SERVICE_PROVIDER = "Service";

		// Token: 0x0400052D RID: 1325
		public const string ANONYMOUS_PROVIDER = "__ANONYMOUS__";

		// Token: 0x0400052E RID: 1326
		private string id;

		// Token: 0x0400052F RID: 1327
		private string provider;
	}
}
