﻿using System;

namespace Amazon.Auth.AccessControlPolicy
{
	// Token: 0x02000173 RID: 371
	public class Condition
	{
		// Token: 0x06000DB7 RID: 3511 RVA: 0x0000584A File Offset: 0x00003A4A
		public Condition()
		{
		}

		// Token: 0x06000DB8 RID: 3512 RVA: 0x00020FE0 File Offset: 0x0001F1E0
		public Condition(string type, string conditionKey, params string[] values)
		{
			this.type = type;
			this.conditionKey = conditionKey;
			this.values = values;
		}

		// Token: 0x17000401 RID: 1025
		// (get) Token: 0x06000DB9 RID: 3513 RVA: 0x00020FFD File Offset: 0x0001F1FD
		// (set) Token: 0x06000DBA RID: 3514 RVA: 0x00021005 File Offset: 0x0001F205
		public string Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		// Token: 0x17000402 RID: 1026
		// (get) Token: 0x06000DBB RID: 3515 RVA: 0x0002100E File Offset: 0x0001F20E
		// (set) Token: 0x06000DBC RID: 3516 RVA: 0x00021016 File Offset: 0x0001F216
		public string ConditionKey
		{
			get
			{
				return this.conditionKey;
			}
			set
			{
				this.conditionKey = value;
			}
		}

		// Token: 0x17000403 RID: 1027
		// (get) Token: 0x06000DBD RID: 3517 RVA: 0x0002101F File Offset: 0x0001F21F
		// (set) Token: 0x06000DBE RID: 3518 RVA: 0x00021027 File Offset: 0x0001F227
		public string[] Values
		{
			get
			{
				return this.values;
			}
			set
			{
				this.values = value;
			}
		}

		// Token: 0x0400050F RID: 1295
		private string type;

		// Token: 0x04000510 RID: 1296
		private string conditionKey;

		// Token: 0x04000511 RID: 1297
		private string[] values;
	}
}
