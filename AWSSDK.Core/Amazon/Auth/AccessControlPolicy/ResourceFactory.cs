﻿using System;
using System.Globalization;

namespace Amazon.Auth.AccessControlPolicy
{
	// Token: 0x02000178 RID: 376
	public static class ResourceFactory
	{
		// Token: 0x06000DE7 RID: 3559 RVA: 0x0002155C File Offset: 0x0001F75C
		public static Resource NewS3BucketResource(string bucketName)
		{
			if (bucketName == null)
			{
				throw new ArgumentNullException("bucketName");
			}
			return new Resource("arn:aws:s3:::" + bucketName);
		}

		// Token: 0x06000DE8 RID: 3560 RVA: 0x0002157C File Offset: 0x0001F77C
		public static Resource NewS3ObjectResource(string bucketName, string keyPattern)
		{
			if (bucketName == null)
			{
				throw new ArgumentNullException("bucketName");
			}
			if (keyPattern == null)
			{
				throw new ArgumentNullException("keyPattern");
			}
			return new Resource(string.Format(CultureInfo.InvariantCulture, "arn:aws:s3:::{0}/{1}", new object[]
			{
				bucketName,
				keyPattern
			}));
		}

		// Token: 0x06000DE9 RID: 3561 RVA: 0x000215BC File Offset: 0x0001F7BC
		public static Resource NewSQSQueueResource(string accountId, string queueName)
		{
			return new Resource("/" + ResourceFactory.FormatAccountId(accountId) + "/" + queueName);
		}

		// Token: 0x06000DEA RID: 3562 RVA: 0x000215D9 File Offset: 0x0001F7D9
		private static string FormatAccountId(string accountId)
		{
			if (accountId == null)
			{
				throw new ArgumentNullException("accountId");
			}
			return accountId.Trim().Replace("-", "");
		}
	}
}
