﻿using System;

namespace Amazon
{
	// Token: 0x02000068 RID: 104
	public enum LogMetricsFormatOption
	{
		// Token: 0x04000191 RID: 401
		Standard,
		// Token: 0x04000192 RID: 402
		JSON
	}
}
