﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000D8 RID: 216
	public interface IHttpRequestFactory<TRequestContent> : IDisposable
	{
		// Token: 0x06000887 RID: 2183
		IHttpRequest<TRequestContent> CreateHttpRequest(Uri requestUri);
	}
}
