﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime
{
	// Token: 0x020000B7 RID: 183
	public interface IRequestMetrics
	{
		// Token: 0x1700025A RID: 602
		// (get) Token: 0x060007C8 RID: 1992
		Dictionary<Metric, List<object>> Properties { get; }

		// Token: 0x1700025B RID: 603
		// (get) Token: 0x060007C9 RID: 1993
		Dictionary<Metric, List<IMetricsTiming>> Timings { get; }

		// Token: 0x1700025C RID: 604
		// (get) Token: 0x060007CA RID: 1994
		Dictionary<Metric, long> Counters { get; }

		// Token: 0x1700025D RID: 605
		// (get) Token: 0x060007CB RID: 1995
		bool IsEnabled { get; }

		// Token: 0x060007CC RID: 1996
		string ToJSON();
	}
}
