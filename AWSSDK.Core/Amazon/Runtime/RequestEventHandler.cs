﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000B6 RID: 182
	// (Invoke) Token: 0x060007C5 RID: 1989
	public delegate void RequestEventHandler(object sender, RequestEventArgs e);
}
