﻿using System;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000D5 RID: 213
	public interface IPipelineHandler
	{
		// Token: 0x1700029F RID: 671
		// (get) Token: 0x0600087C RID: 2172
		// (set) Token: 0x0600087D RID: 2173
		ILogger Logger { get; set; }

		// Token: 0x170002A0 RID: 672
		// (get) Token: 0x0600087E RID: 2174
		// (set) Token: 0x0600087F RID: 2175
		IPipelineHandler InnerHandler { get; set; }

		// Token: 0x170002A1 RID: 673
		// (get) Token: 0x06000880 RID: 2176
		// (set) Token: 0x06000881 RID: 2177
		IPipelineHandler OuterHandler { get; set; }

		// Token: 0x06000882 RID: 2178
		void InvokeSync(IExecutionContext executionContext);

		// Token: 0x06000883 RID: 2179
		IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext);

		// Token: 0x06000884 RID: 2180
		void AsyncCallback(IAsyncExecutionContext executionContext);
	}
}
