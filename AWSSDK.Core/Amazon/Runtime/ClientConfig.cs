﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000A4 RID: 164
	public abstract class ClientConfig : IClientConfig
	{
		// Token: 0x17000208 RID: 520
		// (get) Token: 0x0600070E RID: 1806
		public abstract string ServiceVersion { get; }

		// Token: 0x17000209 RID: 521
		// (get) Token: 0x0600070F RID: 1807 RVA: 0x00012C30 File Offset: 0x00010E30
		// (set) Token: 0x06000710 RID: 1808 RVA: 0x00012C38 File Offset: 0x00010E38
		public SigningAlgorithm SignatureMethod
		{
			get
			{
				return this.signatureMethod;
			}
			set
			{
				this.signatureMethod = value;
			}
		}

		// Token: 0x1700020A RID: 522
		// (get) Token: 0x06000711 RID: 1809 RVA: 0x00012C41 File Offset: 0x00010E41
		// (set) Token: 0x06000712 RID: 1810 RVA: 0x00012C49 File Offset: 0x00010E49
		public string SignatureVersion
		{
			get
			{
				return this.signatureVersion;
			}
			set
			{
				this.signatureVersion = value;
			}
		}

		// Token: 0x1700020B RID: 523
		// (get) Token: 0x06000713 RID: 1811
		public abstract string UserAgent { get; }

		// Token: 0x1700020C RID: 524
		// (get) Token: 0x06000714 RID: 1812 RVA: 0x00012C52 File Offset: 0x00010E52
		// (set) Token: 0x06000715 RID: 1813 RVA: 0x00012C5C File Offset: 0x00010E5C
		public RegionEndpoint RegionEndpoint
		{
			get
			{
				return this.regionEndpoint;
			}
			set
			{
				this.serviceURL = null;
				this.regionEndpoint = value;
				if (this.regionEndpoint != null)
				{
					this.probeForRegionEndpoint = false;
					RegionEndpoint.Endpoint endpointForService = this.regionEndpoint.GetEndpointForService(this.RegionEndpointServiceName, this.UseDualstackEndpoint);
					if (endpointForService != null && endpointForService.SignatureVersionOverride != null)
					{
						this.SignatureVersion = endpointForService.SignatureVersionOverride;
						return;
					}
				}
				else
				{
					this.probeForRegionEndpoint = true;
				}
			}
		}

		// Token: 0x1700020D RID: 525
		// (get) Token: 0x06000716 RID: 1814
		public abstract string RegionEndpointServiceName { get; }

		// Token: 0x1700020E RID: 526
		// (get) Token: 0x06000717 RID: 1815 RVA: 0x00012CBD File Offset: 0x00010EBD
		// (set) Token: 0x06000718 RID: 1816 RVA: 0x00012CC5 File Offset: 0x00010EC5
		public string ServiceURL
		{
			get
			{
				return this.serviceURL;
			}
			set
			{
				this.regionEndpoint = null;
				this.probeForRegionEndpoint = false;
				this.serviceURL = value;
			}
		}

		// Token: 0x1700020F RID: 527
		// (get) Token: 0x06000719 RID: 1817 RVA: 0x00012CDC File Offset: 0x00010EDC
		// (set) Token: 0x0600071A RID: 1818 RVA: 0x00012CE4 File Offset: 0x00010EE4
		public bool UseHttp
		{
			get
			{
				return this.useHttp;
			}
			set
			{
				this.useHttp = value;
			}
		}

		// Token: 0x0600071B RID: 1819 RVA: 0x00012CF0 File Offset: 0x00010EF0
		public string DetermineServiceURL()
		{
			string url;
			if (this.ServiceURL != null)
			{
				url = this.ServiceURL;
			}
			else
			{
				url = ClientConfig.GetUrl(this.RegionEndpoint, this.RegionEndpointServiceName, this.UseHttp, this.UseDualstackEndpoint);
			}
			return url;
		}

		// Token: 0x0600071C RID: 1820 RVA: 0x00012D30 File Offset: 0x00010F30
		internal static string GetUrl(RegionEndpoint regionEndpoint, string regionEndpointServiceName, bool useHttp, bool useDualStack)
		{
			RegionEndpoint.Endpoint endpointForService = regionEndpoint.GetEndpointForService(regionEndpointServiceName, useDualStack);
			return new Uri(string.Format(CultureInfo.InvariantCulture, "{0}{1}", new object[]
			{
				useHttp ? "http://" : "https://",
				endpointForService.Hostname
			})).AbsoluteUri;
		}

		// Token: 0x17000210 RID: 528
		// (get) Token: 0x0600071D RID: 1821 RVA: 0x00012D80 File Offset: 0x00010F80
		// (set) Token: 0x0600071E RID: 1822 RVA: 0x00012D88 File Offset: 0x00010F88
		public string AuthenticationRegion
		{
			get
			{
				return this.authRegion;
			}
			set
			{
				this.authRegion = value;
			}
		}

		// Token: 0x17000211 RID: 529
		// (get) Token: 0x0600071F RID: 1823 RVA: 0x00012D91 File Offset: 0x00010F91
		// (set) Token: 0x06000720 RID: 1824 RVA: 0x00012D99 File Offset: 0x00010F99
		public string AuthenticationServiceName
		{
			get
			{
				return this.authServiceName;
			}
			set
			{
				this.authServiceName = value;
			}
		}

		// Token: 0x17000212 RID: 530
		// (get) Token: 0x06000721 RID: 1825 RVA: 0x00012DA2 File Offset: 0x00010FA2
		// (set) Token: 0x06000722 RID: 1826 RVA: 0x00012DAA File Offset: 0x00010FAA
		public int MaxErrorRetry
		{
			get
			{
				return this.maxErrorRetry;
			}
			set
			{
				this.maxErrorRetry = value;
			}
		}

		// Token: 0x17000213 RID: 531
		// (get) Token: 0x06000723 RID: 1827 RVA: 0x00012DB3 File Offset: 0x00010FB3
		// (set) Token: 0x06000724 RID: 1828 RVA: 0x00012DBB File Offset: 0x00010FBB
		public bool LogResponse
		{
			get
			{
				return this.logResponse;
			}
			set
			{
				this.logResponse = value;
			}
		}

		// Token: 0x17000214 RID: 532
		// (get) Token: 0x06000725 RID: 1829 RVA: 0x00012DC4 File Offset: 0x00010FC4
		// (set) Token: 0x06000726 RID: 1830 RVA: 0x00012DCC File Offset: 0x00010FCC
		[Obsolete("This property does not effect response processing and is deprecated.To enable response logging, the ClientConfig.LogResponse and AWSConfigs.LoggingConfig.LogResponses properties can be used.")]
		public bool ReadEntireResponse
		{
			get
			{
				return this.readEntireResponse;
			}
			set
			{
				this.readEntireResponse = value;
			}
		}

		// Token: 0x17000215 RID: 533
		// (get) Token: 0x06000727 RID: 1831 RVA: 0x00012DD5 File Offset: 0x00010FD5
		// (set) Token: 0x06000728 RID: 1832 RVA: 0x00012DDD File Offset: 0x00010FDD
		public int BufferSize
		{
			get
			{
				return this.bufferSize;
			}
			set
			{
				this.bufferSize = value;
			}
		}

		// Token: 0x17000216 RID: 534
		// (get) Token: 0x06000729 RID: 1833 RVA: 0x00012DE6 File Offset: 0x00010FE6
		// (set) Token: 0x0600072A RID: 1834 RVA: 0x00012DEE File Offset: 0x00010FEE
		public long ProgressUpdateInterval
		{
			get
			{
				return this.progressUpdateInterval;
			}
			set
			{
				this.progressUpdateInterval = value;
			}
		}

		// Token: 0x17000217 RID: 535
		// (get) Token: 0x0600072B RID: 1835 RVA: 0x00012DF7 File Offset: 0x00010FF7
		// (set) Token: 0x0600072C RID: 1836 RVA: 0x00012DFF File Offset: 0x00010FFF
		public bool ResignRetries
		{
			get
			{
				return this.resignRetries;
			}
			set
			{
				this.resignRetries = value;
			}
		}

		// Token: 0x17000218 RID: 536
		// (get) Token: 0x0600072D RID: 1837 RVA: 0x00012E08 File Offset: 0x00011008
		// (set) Token: 0x0600072E RID: 1838 RVA: 0x00012E10 File Offset: 0x00011010
		public bool AllowAutoRedirect
		{
			get
			{
				return this.allowAutoRedirect;
			}
			set
			{
				this.allowAutoRedirect = value;
			}
		}

		// Token: 0x17000219 RID: 537
		// (get) Token: 0x0600072F RID: 1839 RVA: 0x00012E19 File Offset: 0x00011019
		// (set) Token: 0x06000730 RID: 1840 RVA: 0x00012E21 File Offset: 0x00011021
		public bool LogMetrics
		{
			get
			{
				return this.logMetrics;
			}
			set
			{
				this.logMetrics = value;
			}
		}

		// Token: 0x1700021A RID: 538
		// (get) Token: 0x06000731 RID: 1841 RVA: 0x00012E2A File Offset: 0x0001102A
		// (set) Token: 0x06000732 RID: 1842 RVA: 0x00012E32 File Offset: 0x00011032
		public bool DisableLogging
		{
			get
			{
				return this.disableLogging;
			}
			set
			{
				this.disableLogging = value;
			}
		}

		// Token: 0x1700021B RID: 539
		// (get) Token: 0x06000733 RID: 1843 RVA: 0x00012E3C File Offset: 0x0001103C
		// (set) Token: 0x06000734 RID: 1844 RVA: 0x00012E9C File Offset: 0x0001109C
		public ICredentials ProxyCredentials
		{
			get
			{
				if (this.proxyCredentials == null && (!string.IsNullOrEmpty(AWSConfigs.ProxyConfig.Username) || !string.IsNullOrEmpty(AWSConfigs.ProxyConfig.Password)))
				{
					return new NetworkCredential(AWSConfigs.ProxyConfig.Username, AWSConfigs.ProxyConfig.Password ?? string.Empty);
				}
				return this.proxyCredentials;
			}
			set
			{
				this.proxyCredentials = value;
			}
		}

		// Token: 0x06000735 RID: 1845 RVA: 0x00012EA8 File Offset: 0x000110A8
		public ClientConfig()
		{
			this.Initialize();
		}

		// Token: 0x06000736 RID: 1846 RVA: 0x00005805 File Offset: 0x00003A05
		protected virtual void Initialize()
		{
		}

		// Token: 0x1700021C RID: 540
		// (get) Token: 0x06000737 RID: 1847 RVA: 0x00012F1D File Offset: 0x0001111D
		// (set) Token: 0x06000738 RID: 1848 RVA: 0x00012F25 File Offset: 0x00011125
		public TimeSpan? Timeout
		{
			get
			{
				return this.timeout;
			}
			set
			{
				ClientConfig.ValidateTimeout(value);
				this.timeout = value;
			}
		}

		// Token: 0x1700021D RID: 541
		// (get) Token: 0x06000739 RID: 1849 RVA: 0x00012F34 File Offset: 0x00011134
		// (set) Token: 0x0600073A RID: 1850 RVA: 0x00012F3C File Offset: 0x0001113C
		public bool UseDualstackEndpoint
		{
			get
			{
				return this.useDualstackEndpoint;
			}
			set
			{
				this.useDualstackEndpoint = value;
			}
		}

		// Token: 0x1700021E RID: 542
		// (get) Token: 0x0600073B RID: 1851 RVA: 0x00012F45 File Offset: 0x00011145
		// (set) Token: 0x0600073C RID: 1852 RVA: 0x00012F4D File Offset: 0x0001114D
		public bool ThrottleRetries
		{
			get
			{
				return this.throttleRetries;
			}
			set
			{
				this.throttleRetries = value;
			}
		}

		// Token: 0x0600073D RID: 1853 RVA: 0x00005805 File Offset: 0x00003A05
		public void SetUseNagleIfAvailable(bool useNagle)
		{
		}

		// Token: 0x0600073E RID: 1854 RVA: 0x00012F56 File Offset: 0x00011156
		public virtual void Validate()
		{
			if (this.RegionEndpoint == null && string.IsNullOrEmpty(this.ServiceURL))
			{
				throw new AmazonClientException("No RegionEndpoint or ServiceURL configured");
			}
		}

		// Token: 0x0600073F RID: 1855 RVA: 0x00012F78 File Offset: 0x00011178
		public static void ValidateTimeout(TimeSpan? timeout)
		{
			if (timeout == null)
			{
				throw new ArgumentNullException("timeout");
			}
			TimeSpan? timeSpan = timeout;
			TimeSpan infiniteTimeout = ClientConfig.InfiniteTimeout;
			if ((timeSpan == null || (timeSpan != null && timeSpan.GetValueOrDefault() != infiniteTimeout)) && (timeout <= TimeSpan.Zero || timeout > ClientConfig.MaxTimeout))
			{
				throw new ArgumentOutOfRangeException("timeout");
			}
		}

		// Token: 0x06000740 RID: 1856 RVA: 0x0001301C File Offset: 0x0001121C
		public static TimeSpan? GetTimeoutValue(TimeSpan? clientTimeout, TimeSpan? requestTimeout)
		{
			if (requestTimeout != null)
			{
				return requestTimeout;
			}
			if (clientTimeout == null)
			{
				return null;
			}
			return clientTimeout;
		}

		// Token: 0x06000741 RID: 1857 RVA: 0x00013048 File Offset: 0x00011248
		private static RegionEndpoint GetDefaultRegionEndpoint()
		{
			return FallbackRegionFactory.GetRegionEndpoint();
		}

		// Token: 0x1700021F RID: 543
		// (get) Token: 0x06000742 RID: 1858 RVA: 0x0001304F File Offset: 0x0001124F
		// (set) Token: 0x06000743 RID: 1859 RVA: 0x0001306F File Offset: 0x0001126F
		public string ProxyHost
		{
			get
			{
				if (string.IsNullOrEmpty(this.proxyHost))
				{
					return AWSConfigs.ProxyConfig.Host;
				}
				return this.proxyHost;
			}
			set
			{
				this.proxyHost = value;
			}
		}

		// Token: 0x17000220 RID: 544
		// (get) Token: 0x06000744 RID: 1860 RVA: 0x00013078 File Offset: 0x00011278
		// (set) Token: 0x06000745 RID: 1861 RVA: 0x000130A7 File Offset: 0x000112A7
		public int ProxyPort
		{
			get
			{
				if (this.proxyPort <= 0)
				{
					return AWSConfigs.ProxyConfig.Port.GetValueOrDefault();
				}
				return this.proxyPort;
			}
			set
			{
				this.proxyPort = value;
			}
		}

		// Token: 0x17000221 RID: 545
		// (get) Token: 0x06000746 RID: 1862 RVA: 0x000130B0 File Offset: 0x000112B0
		// (set) Token: 0x06000747 RID: 1863 RVA: 0x000130CB File Offset: 0x000112CB
		public List<string> ProxyBypassList
		{
			get
			{
				if (this.proxyBypassList == null)
				{
					return AWSConfigs.ProxyConfig.BypassList;
				}
				return this.proxyBypassList;
			}
			set
			{
				this.proxyBypassList = ((value != null) ? new List<string>(value) : null);
			}
		}

		// Token: 0x17000222 RID: 546
		// (get) Token: 0x06000748 RID: 1864 RVA: 0x000130DF File Offset: 0x000112DF
		// (set) Token: 0x06000749 RID: 1865 RVA: 0x000130E7 File Offset: 0x000112E7
		public bool ProxyBypassOnLocal { get; set; }

		// Token: 0x0600074A RID: 1866 RVA: 0x000130F0 File Offset: 0x000112F0
		public WebProxy GetWebProxy()
		{
			WebProxy webProxy = null;
			if (!string.IsNullOrEmpty(this.ProxyHost) && this.ProxyPort > 0)
			{
				webProxy = new WebProxy(this.ProxyHost.StartsWith("http://", StringComparison.OrdinalIgnoreCase) ? this.ProxyHost.Substring("http://".Length) : this.ProxyHost, this.ProxyPort);
				if (this.ProxyCredentials != null)
				{
					webProxy.Credentials = this.ProxyCredentials;
				}
				if (this.ProxyBypassList != null)
				{
					webProxy.BypassList = this.ProxyBypassList.ToArray();
				}
				webProxy.BypassProxyOnLocal = this.ProxyBypassOnLocal;
			}
			return webProxy;
		}

		// Token: 0x0600074B RID: 1867 RVA: 0x0001318C File Offset: 0x0001138C
		public void SetWebProxy(WebProxy proxy)
		{
			if (proxy == null)
			{
				throw new ArgumentNullException("proxy");
			}
			Uri address = proxy.Address;
			this.ProxyHost = address.Host;
			this.ProxyPort = address.Port;
			this.ProxyBypassList = new List<string>(proxy.BypassList);
			this.ProxyBypassOnLocal = proxy.BypassProxyOnLocal;
			this.ProxyCredentials = proxy.Credentials;
		}

		// Token: 0x17000223 RID: 547
		// (get) Token: 0x0600074C RID: 1868 RVA: 0x000131EF File Offset: 0x000113EF
		// (set) Token: 0x0600074D RID: 1869 RVA: 0x000131FC File Offset: 0x000113FC
		public int MaxIdleTime
		{
			get
			{
				return AWSSDKUtils.GetMaxIdleTime(this.maxIdleTime);
			}
			set
			{
				this.maxIdleTime = new int?(value);
			}
		}

		// Token: 0x17000224 RID: 548
		// (get) Token: 0x0600074E RID: 1870 RVA: 0x0001320A File Offset: 0x0001140A
		// (set) Token: 0x0600074F RID: 1871 RVA: 0x00013217 File Offset: 0x00011417
		public int ConnectionLimit
		{
			get
			{
				return AWSSDKUtils.GetConnectionLimit(this.connectionLimit);
			}
			set
			{
				this.connectionLimit = new int?(value);
			}
		}

		// Token: 0x17000225 RID: 549
		// (get) Token: 0x06000750 RID: 1872 RVA: 0x00013225 File Offset: 0x00011425
		// (set) Token: 0x06000751 RID: 1873 RVA: 0x0001322D File Offset: 0x0001142D
		public bool UseNagleAlgorithm
		{
			get
			{
				return this.useNagleAlgorithm;
			}
			set
			{
				this.useNagleAlgorithm = value;
			}
		}

		// Token: 0x17000226 RID: 550
		// (get) Token: 0x06000752 RID: 1874 RVA: 0x00013236 File Offset: 0x00011436
		// (set) Token: 0x06000753 RID: 1875 RVA: 0x0001323E File Offset: 0x0001143E
		public TimeSpan? ReadWriteTimeout
		{
			get
			{
				return this.readWriteTimeout;
			}
			set
			{
				ClientConfig.ValidateTimeout(value);
				this.readWriteTimeout = value;
			}
		}

		// Token: 0x040002C9 RID: 713
		internal static readonly TimeSpan InfiniteTimeout = TimeSpan.FromMilliseconds(-1.0);

		// Token: 0x040002CA RID: 714
		public static readonly TimeSpan MaxTimeout = TimeSpan.FromMilliseconds(2147483647.0);

		// Token: 0x040002CB RID: 715
		private RegionEndpoint regionEndpoint;

		// Token: 0x040002CC RID: 716
		private bool probeForRegionEndpoint = true;

		// Token: 0x040002CD RID: 717
		private bool throttleRetries = true;

		// Token: 0x040002CE RID: 718
		private bool useHttp;

		// Token: 0x040002CF RID: 719
		private string serviceURL;

		// Token: 0x040002D0 RID: 720
		private string authRegion;

		// Token: 0x040002D1 RID: 721
		private string authServiceName;

		// Token: 0x040002D2 RID: 722
		private string signatureVersion = "2";

		// Token: 0x040002D3 RID: 723
		private SigningAlgorithm signatureMethod = SigningAlgorithm.HmacSHA256;

		// Token: 0x040002D4 RID: 724
		private int maxErrorRetry = 4;

		// Token: 0x040002D5 RID: 725
		private bool readEntireResponse;

		// Token: 0x040002D6 RID: 726
		private bool logResponse;

		// Token: 0x040002D7 RID: 727
		private int bufferSize = 8192;

		// Token: 0x040002D8 RID: 728
		private long progressUpdateInterval = 102400L;

		// Token: 0x040002D9 RID: 729
		private bool resignRetries;

		// Token: 0x040002DA RID: 730
		private ICredentials proxyCredentials;

		// Token: 0x040002DB RID: 731
		private bool logMetrics = AWSConfigs.LoggingConfig.LogMetrics;

		// Token: 0x040002DC RID: 732
		private bool disableLogging;

		// Token: 0x040002DD RID: 733
		private TimeSpan? timeout;

		// Token: 0x040002DE RID: 734
		private bool allowAutoRedirect = true;

		// Token: 0x040002DF RID: 735
		private bool useDualstackEndpoint;

		// Token: 0x040002E0 RID: 736
		private string proxyHost;

		// Token: 0x040002E1 RID: 737
		private int proxyPort = -1;

		// Token: 0x040002E2 RID: 738
		private List<string> proxyBypassList;

		// Token: 0x040002E3 RID: 739
		private int? connectionLimit;

		// Token: 0x040002E4 RID: 740
		private int? maxIdleTime;

		// Token: 0x040002E5 RID: 741
		private bool useNagleAlgorithm;

		// Token: 0x040002E6 RID: 742
		private TimeSpan? readWriteTimeout;
	}
}
