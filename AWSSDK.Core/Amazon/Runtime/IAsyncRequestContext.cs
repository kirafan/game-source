﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000D1 RID: 209
	public interface IAsyncRequestContext : IRequestContext
	{
		// Token: 0x17000295 RID: 661
		// (get) Token: 0x06000870 RID: 2160
		AsyncCallback Callback { get; }

		// Token: 0x17000296 RID: 662
		// (get) Token: 0x06000871 RID: 2161
		object State { get; }

		// Token: 0x17000297 RID: 663
		// (get) Token: 0x06000872 RID: 2162
		AsyncOptions AsyncOptions { get; }

		// Token: 0x17000298 RID: 664
		// (get) Token: 0x06000873 RID: 2163
		Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> Action { get; }
	}
}
