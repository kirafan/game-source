﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000AF RID: 175
	public class StringParameterValue : ParameterValue
	{
		// Token: 0x1700024F RID: 591
		// (get) Token: 0x060007A0 RID: 1952 RVA: 0x0001364D File Offset: 0x0001184D
		// (set) Token: 0x060007A1 RID: 1953 RVA: 0x00013655 File Offset: 0x00011855
		public string Value { get; set; }

		// Token: 0x060007A2 RID: 1954 RVA: 0x0001365E File Offset: 0x0001185E
		public StringParameterValue(string value)
		{
			this.Value = value;
		}

		// Token: 0x060007A3 RID: 1955 RVA: 0x0001366D File Offset: 0x0001186D
		internal StringParameterValue()
		{
		}
	}
}
