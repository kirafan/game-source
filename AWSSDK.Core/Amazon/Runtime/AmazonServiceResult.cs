﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000DD RID: 221
	public class AmazonServiceResult<TRequest, TResponse> where TRequest : AmazonWebServiceRequest where TResponse : AmazonWebServiceResponse
	{
		// Token: 0x170002AF RID: 687
		// (get) Token: 0x060008BB RID: 2235 RVA: 0x00014E03 File Offset: 0x00013003
		// (set) Token: 0x060008BC RID: 2236 RVA: 0x00014E0B File Offset: 0x0001300B
		public TRequest Request { get; internal set; }

		// Token: 0x170002B0 RID: 688
		// (get) Token: 0x060008BD RID: 2237 RVA: 0x00014E14 File Offset: 0x00013014
		// (set) Token: 0x060008BE RID: 2238 RVA: 0x00014E1C File Offset: 0x0001301C
		public TResponse Response { get; internal set; }

		// Token: 0x170002B1 RID: 689
		// (get) Token: 0x060008BF RID: 2239 RVA: 0x00014E25 File Offset: 0x00013025
		// (set) Token: 0x060008C0 RID: 2240 RVA: 0x00014E2D File Offset: 0x0001302D
		public Exception Exception { get; internal set; }

		// Token: 0x170002B2 RID: 690
		// (get) Token: 0x060008C1 RID: 2241 RVA: 0x00014E36 File Offset: 0x00013036
		// (set) Token: 0x060008C2 RID: 2242 RVA: 0x00014E3E File Offset: 0x0001303E
		public object state { get; internal set; }

		// Token: 0x060008C3 RID: 2243 RVA: 0x00014E47 File Offset: 0x00013047
		public AmazonServiceResult(TRequest request, TResponse response, Exception exception, object state)
		{
			this.Request = request;
			this.Response = response;
			this.Exception = exception;
			this.state = state;
		}
	}
}
