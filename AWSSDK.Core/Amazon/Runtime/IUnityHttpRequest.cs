﻿using System;
using System.Collections.Generic;
using System.Threading;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime
{
	// Token: 0x020000DA RID: 218
	public interface IUnityHttpRequest
	{
		// Token: 0x170002A4 RID: 676
		// (get) Token: 0x06000897 RID: 2199
		byte[] RequestContent { get; }

		// Token: 0x170002A5 RID: 677
		// (get) Token: 0x06000898 RID: 2200
		Dictionary<string, string> Headers { get; }

		// Token: 0x170002A6 RID: 678
		// (get) Token: 0x06000899 RID: 2201
		AsyncCallback Callback { get; }

		// Token: 0x170002A7 RID: 679
		// (get) Token: 0x0600089A RID: 2202
		IAsyncResult AsyncResult { get; }

		// Token: 0x170002A8 RID: 680
		// (get) Token: 0x0600089B RID: 2203
		ManualResetEvent WaitHandle { get; }

		// Token: 0x170002A9 RID: 681
		// (get) Token: 0x0600089C RID: 2204
		// (set) Token: 0x0600089D RID: 2205
		bool IsSync { get; set; }

		// Token: 0x170002AA RID: 682
		// (get) Token: 0x0600089E RID: 2206
		// (set) Token: 0x0600089F RID: 2207
		Exception Exception { get; set; }

		// Token: 0x170002AB RID: 683
		// (get) Token: 0x060008A0 RID: 2208
		// (set) Token: 0x060008A1 RID: 2209
		IDisposable WwwRequest { get; set; }

		// Token: 0x170002AC RID: 684
		// (get) Token: 0x060008A2 RID: 2210
		// (set) Token: 0x060008A3 RID: 2211
		IWebResponseData Response { get; set; }

		// Token: 0x060008A4 RID: 2212
		void OnUploadProgressChanged(float progress);
	}
}
