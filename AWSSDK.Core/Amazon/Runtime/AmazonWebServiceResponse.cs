﻿using System;
using System.Net;

namespace Amazon.Runtime
{
	// Token: 0x020000A0 RID: 160
	[Serializable]
	public class AmazonWebServiceResponse
	{
		// Token: 0x17000202 RID: 514
		// (get) Token: 0x060006FA RID: 1786 RVA: 0x000129E5 File Offset: 0x00010BE5
		// (set) Token: 0x060006FB RID: 1787 RVA: 0x000129ED File Offset: 0x00010BED
		public ResponseMetadata ResponseMetadata
		{
			get
			{
				return this.responseMetadataField;
			}
			set
			{
				this.responseMetadataField = value;
			}
		}

		// Token: 0x17000203 RID: 515
		// (get) Token: 0x060006FC RID: 1788 RVA: 0x000129F6 File Offset: 0x00010BF6
		// (set) Token: 0x060006FD RID: 1789 RVA: 0x000129FE File Offset: 0x00010BFE
		public long ContentLength
		{
			get
			{
				return this.contentLength;
			}
			set
			{
				this.contentLength = value;
			}
		}

		// Token: 0x17000204 RID: 516
		// (get) Token: 0x060006FE RID: 1790 RVA: 0x00012A07 File Offset: 0x00010C07
		// (set) Token: 0x060006FF RID: 1791 RVA: 0x00012A0F File Offset: 0x00010C0F
		public HttpStatusCode HttpStatusCode
		{
			get
			{
				return this.httpStatusCode;
			}
			set
			{
				this.httpStatusCode = value;
			}
		}

		// Token: 0x040002C1 RID: 705
		private ResponseMetadata responseMetadataField;

		// Token: 0x040002C2 RID: 706
		private long contentLength;

		// Token: 0x040002C3 RID: 707
		private HttpStatusCode httpStatusCode;
	}
}
