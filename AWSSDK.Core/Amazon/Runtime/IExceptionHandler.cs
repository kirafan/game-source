﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000D6 RID: 214
	public interface IExceptionHandler
	{
		// Token: 0x06000885 RID: 2181
		bool Handle(IExecutionContext executionContext, Exception exception);
	}
}
