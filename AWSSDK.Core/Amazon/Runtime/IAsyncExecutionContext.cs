﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000D4 RID: 212
	public interface IAsyncExecutionContext
	{
		// Token: 0x1700029C RID: 668
		// (get) Token: 0x06000878 RID: 2168
		IAsyncResponseContext ResponseContext { get; }

		// Token: 0x1700029D RID: 669
		// (get) Token: 0x06000879 RID: 2169
		IAsyncRequestContext RequestContext { get; }

		// Token: 0x1700029E RID: 670
		// (get) Token: 0x0600087A RID: 2170
		// (set) Token: 0x0600087B RID: 2171
		object RuntimeState { get; set; }
	}
}
