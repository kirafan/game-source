﻿using System;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Auth;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000CF RID: 207
	public interface IRequestContext
	{
		// Token: 0x17000287 RID: 647
		// (get) Token: 0x0600085C RID: 2140
		AmazonWebServiceRequest OriginalRequest { get; }

		// Token: 0x17000288 RID: 648
		// (get) Token: 0x0600085D RID: 2141
		string RequestName { get; }

		// Token: 0x17000289 RID: 649
		// (get) Token: 0x0600085E RID: 2142
		IMarshaller<IRequest, AmazonWebServiceRequest> Marshaller { get; }

		// Token: 0x1700028A RID: 650
		// (get) Token: 0x0600085F RID: 2143
		ResponseUnmarshaller Unmarshaller { get; }

		// Token: 0x1700028B RID: 651
		// (get) Token: 0x06000860 RID: 2144
		RequestMetrics Metrics { get; }

		// Token: 0x1700028C RID: 652
		// (get) Token: 0x06000861 RID: 2145
		AbstractAWSSigner Signer { get; }

		// Token: 0x1700028D RID: 653
		// (get) Token: 0x06000862 RID: 2146
		IClientConfig ClientConfig { get; }

		// Token: 0x1700028E RID: 654
		// (get) Token: 0x06000863 RID: 2147
		// (set) Token: 0x06000864 RID: 2148
		ImmutableCredentials ImmutableCredentials { get; set; }

		// Token: 0x1700028F RID: 655
		// (get) Token: 0x06000865 RID: 2149
		// (set) Token: 0x06000866 RID: 2150
		IRequest Request { get; set; }

		// Token: 0x17000290 RID: 656
		// (get) Token: 0x06000867 RID: 2151
		// (set) Token: 0x06000868 RID: 2152
		bool IsSigned { get; set; }

		// Token: 0x17000291 RID: 657
		// (get) Token: 0x06000869 RID: 2153
		bool IsAsync { get; }

		// Token: 0x17000292 RID: 658
		// (get) Token: 0x0600086A RID: 2154
		// (set) Token: 0x0600086B RID: 2155
		int Retries { get; set; }
	}
}
