﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Amazon.Util.Internal;

namespace Amazon.Runtime
{
	// Token: 0x020000A5 RID: 165
	public class ConstantClass
	{
		// Token: 0x06000755 RID: 1877 RVA: 0x00013275 File Offset: 0x00011475
		protected ConstantClass(string value)
		{
			this.Value = value;
		}

		// Token: 0x17000227 RID: 551
		// (get) Token: 0x06000756 RID: 1878 RVA: 0x00013284 File Offset: 0x00011484
		// (set) Token: 0x06000757 RID: 1879 RVA: 0x0001328C File Offset: 0x0001148C
		public string Value { get; private set; }

		// Token: 0x06000758 RID: 1880 RVA: 0x00013295 File Offset: 0x00011495
		public override string ToString()
		{
			return this.Intern().Value;
		}

		// Token: 0x06000759 RID: 1881 RVA: 0x00013295 File Offset: 0x00011495
		public string ToString(IFormatProvider provider)
		{
			return this.Intern().Value;
		}

		// Token: 0x0600075A RID: 1882 RVA: 0x000132A2 File Offset: 0x000114A2
		public static implicit operator string(ConstantClass value)
		{
			if (value == null)
			{
				return null;
			}
			return value.Intern().Value;
		}

		// Token: 0x0600075B RID: 1883 RVA: 0x000132BC File Offset: 0x000114BC
		internal ConstantClass Intern()
		{
			if (!ConstantClass.staticFields.ContainsKey(base.GetType()))
			{
				ConstantClass.LoadFields(base.GetType());
			}
			ConstantClass result;
			if (!ConstantClass.staticFields[base.GetType()].TryGetValue(this.Value, out result))
			{
				return this;
			}
			return result;
		}

		// Token: 0x0600075C RID: 1884 RVA: 0x00013308 File Offset: 0x00011508
		protected static T FindValue<T>(string value) where T : ConstantClass
		{
			if (value == null)
			{
				return default(T);
			}
			if (!ConstantClass.staticFields.ContainsKey(typeof(T)))
			{
				ConstantClass.LoadFields(typeof(T));
			}
			ConstantClass constantClass;
			if (!ConstantClass.staticFields[typeof(T)].TryGetValue(value, out constantClass))
			{
				return TypeFactory.GetTypeInfo(typeof(T)).GetConstructor(new ITypeInfo[]
				{
					TypeFactory.GetTypeInfo(typeof(string))
				}).Invoke(new object[]
				{
					value
				}) as T;
			}
			return constantClass as T;
		}

		// Token: 0x0600075D RID: 1885 RVA: 0x000133B8 File Offset: 0x000115B8
		private static void LoadFields(Type t)
		{
			if (ConstantClass.staticFields.ContainsKey(t))
			{
				return;
			}
			object obj = ConstantClass.staticFieldsLock;
			lock (obj)
			{
				if (!ConstantClass.staticFields.ContainsKey(t))
				{
					Dictionary<string, ConstantClass> dictionary = new Dictionary<string, ConstantClass>(StringComparer.OrdinalIgnoreCase);
					foreach (FieldInfo fieldInfo in TypeFactory.GetTypeInfo(t).GetFields())
					{
						if (fieldInfo.IsStatic && fieldInfo.FieldType == t)
						{
							ConstantClass constantClass = fieldInfo.GetValue(null) as ConstantClass;
							dictionary[constantClass.Value] = constantClass;
						}
					}
					Dictionary<Type, Dictionary<string, ConstantClass>> dictionary2 = new Dictionary<Type, Dictionary<string, ConstantClass>>(ConstantClass.staticFields);
					dictionary2[t] = dictionary;
					ConstantClass.staticFields = dictionary2;
				}
			}
		}

		// Token: 0x0600075E RID: 1886 RVA: 0x00013494 File Offset: 0x00011694
		public override int GetHashCode()
		{
			return this.Value.GetHashCode();
		}

		// Token: 0x0600075F RID: 1887 RVA: 0x000134A4 File Offset: 0x000116A4
		public override bool Equals(object obj)
		{
			if (obj == null)
			{
				return false;
			}
			if (this == obj)
			{
				return true;
			}
			ConstantClass obj2 = obj as ConstantClass;
			if (this.Equals(obj2))
			{
				return true;
			}
			string text = obj as string;
			return text != null && StringComparer.OrdinalIgnoreCase.Equals(this.Value, text);
		}

		// Token: 0x06000760 RID: 1888 RVA: 0x000134EB File Offset: 0x000116EB
		public bool Equals(ConstantClass obj)
		{
			return obj != null && StringComparer.OrdinalIgnoreCase.Equals(this.Value, obj.Value);
		}

		// Token: 0x06000761 RID: 1889 RVA: 0x00013508 File Offset: 0x00011708
		public static bool operator ==(ConstantClass a, ConstantClass b)
		{
			return a == b || (a != null && a.Equals(b));
		}

		// Token: 0x06000762 RID: 1890 RVA: 0x0001351C File Offset: 0x0001171C
		public static bool operator !=(ConstantClass a, ConstantClass b)
		{
			return !(a == b);
		}

		// Token: 0x06000763 RID: 1891 RVA: 0x00013528 File Offset: 0x00011728
		public static bool operator ==(ConstantClass a, string b)
		{
			return (a == null && b == null) || (a != null && a.Equals(b));
		}

		// Token: 0x06000764 RID: 1892 RVA: 0x0001353E File Offset: 0x0001173E
		public static bool operator ==(string a, ConstantClass b)
		{
			return b == a;
		}

		// Token: 0x06000765 RID: 1893 RVA: 0x00013547 File Offset: 0x00011747
		public static bool operator !=(ConstantClass a, string b)
		{
			return !(a == b);
		}

		// Token: 0x06000766 RID: 1894 RVA: 0x00013553 File Offset: 0x00011753
		public static bool operator !=(string a, ConstantClass b)
		{
			return !(a == b);
		}

		// Token: 0x040002E8 RID: 744
		private static readonly object staticFieldsLock = new object();

		// Token: 0x040002E9 RID: 745
		private static Dictionary<Type, Dictionary<string, ConstantClass>> staticFields = new Dictionary<Type, Dictionary<string, ConstantClass>>();
	}
}
