﻿using System;
using System.Runtime.Serialization;
using System.Security;

namespace Amazon.Runtime
{
	// Token: 0x0200009B RID: 155
	[Serializable]
	public class AmazonDateTimeUnmarshallingException : AmazonUnmarshallingException
	{
		// Token: 0x170001F0 RID: 496
		// (get) Token: 0x0600069E RID: 1694 RVA: 0x00011B52 File Offset: 0x0000FD52
		// (set) Token: 0x0600069F RID: 1695 RVA: 0x00011B5A File Offset: 0x0000FD5A
		public string InvalidDateTimeToken { get; private set; }

		// Token: 0x060006A0 RID: 1696 RVA: 0x00011B63 File Offset: 0x0000FD63
		public AmazonDateTimeUnmarshallingException(string requestId, string lastKnownLocation, string invalidDateTimeToken, Exception innerException) : base(requestId, lastKnownLocation, innerException)
		{
			this.InvalidDateTimeToken = invalidDateTimeToken;
		}

		// Token: 0x060006A1 RID: 1697 RVA: 0x00011B76 File Offset: 0x0000FD76
		public AmazonDateTimeUnmarshallingException(string requestId, string lastKnownLocation, string responseBody, string invalidDateTimeToken, Exception innerException) : base(requestId, lastKnownLocation, responseBody, innerException)
		{
			this.InvalidDateTimeToken = invalidDateTimeToken;
		}

		// Token: 0x060006A2 RID: 1698 RVA: 0x00011B8B File Offset: 0x0000FD8B
		public AmazonDateTimeUnmarshallingException(string requestId, string lastKnownLocation, string responseBody, string invalidDateTimeToken, string message, Exception innerException) : base(requestId, lastKnownLocation, responseBody, message, innerException)
		{
			this.InvalidDateTimeToken = invalidDateTimeToken;
		}

		// Token: 0x060006A3 RID: 1699 RVA: 0x00011BA2 File Offset: 0x0000FDA2
		protected AmazonDateTimeUnmarshallingException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if (info != null)
			{
				this.InvalidDateTimeToken = info.GetString("InvalidDateTimeToken");
			}
		}

		// Token: 0x060006A4 RID: 1700 RVA: 0x00011BC0 File Offset: 0x0000FDC0
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			if (info != null)
			{
				info.AddValue("InvalidDateTimeToken", this.InvalidDateTimeToken);
			}
		}
	}
}
