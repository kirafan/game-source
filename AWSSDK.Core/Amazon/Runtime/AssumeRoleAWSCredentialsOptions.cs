﻿using System;
using System.Net;

namespace Amazon.Runtime
{
	// Token: 0x020000C3 RID: 195
	public class AssumeRoleAWSCredentialsOptions
	{
		// Token: 0x17000272 RID: 626
		// (get) Token: 0x06000800 RID: 2048 RVA: 0x00013B5C File Offset: 0x00011D5C
		// (set) Token: 0x06000801 RID: 2049 RVA: 0x00013B64 File Offset: 0x00011D64
		public string ExternalId { get; set; }

		// Token: 0x17000273 RID: 627
		// (get) Token: 0x06000802 RID: 2050 RVA: 0x00013B6D File Offset: 0x00011D6D
		// (set) Token: 0x06000803 RID: 2051 RVA: 0x00013B75 File Offset: 0x00011D75
		public string Policy { get; set; }

		// Token: 0x17000274 RID: 628
		// (get) Token: 0x06000804 RID: 2052 RVA: 0x00013B7E File Offset: 0x00011D7E
		// (set) Token: 0x06000805 RID: 2053 RVA: 0x00013B86 File Offset: 0x00011D86
		public int? DurationSeconds { get; set; }

		// Token: 0x17000275 RID: 629
		// (get) Token: 0x06000806 RID: 2054 RVA: 0x00013B8F File Offset: 0x00011D8F
		// (set) Token: 0x06000807 RID: 2055 RVA: 0x00013B97 File Offset: 0x00011D97
		public WebProxy ProxySettings { get; set; }

		// Token: 0x17000276 RID: 630
		// (get) Token: 0x06000808 RID: 2056 RVA: 0x00013BA0 File Offset: 0x00011DA0
		// (set) Token: 0x06000809 RID: 2057 RVA: 0x00013BA8 File Offset: 0x00011DA8
		public string MfaSerialNumber { get; set; }

		// Token: 0x17000277 RID: 631
		// (get) Token: 0x0600080A RID: 2058 RVA: 0x00013BB1 File Offset: 0x00011DB1
		public string MfaTokenCode
		{
			get
			{
				if (string.IsNullOrEmpty(this.MfaSerialNumber))
				{
					return null;
				}
				if (this.MfaTokenCodeCallback == null)
				{
					throw new InvalidOperationException("The MfaSerialNumber has been set but the MfaTokenCodeCallback hasn't.  MfaTokenCodeCallback is required in order to determine the MfaTokenCode when MfaSerialNumber is set.");
				}
				return this.MfaTokenCodeCallback();
			}
		}

		// Token: 0x17000278 RID: 632
		// (get) Token: 0x0600080B RID: 2059 RVA: 0x00013BE0 File Offset: 0x00011DE0
		// (set) Token: 0x0600080C RID: 2060 RVA: 0x00013BE8 File Offset: 0x00011DE8
		public Func<string> MfaTokenCodeCallback { get; set; }
	}
}
