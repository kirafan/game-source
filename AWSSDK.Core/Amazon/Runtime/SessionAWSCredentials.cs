﻿using System;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000CD RID: 205
	public class SessionAWSCredentials : AWSCredentials
	{
		// Token: 0x06000853 RID: 2131 RVA: 0x00014900 File Offset: 0x00012B00
		public SessionAWSCredentials(string awsAccessKeyId, string awsSecretAccessKey, string token)
		{
			if (string.IsNullOrEmpty(awsAccessKeyId))
			{
				throw new ArgumentNullException("awsAccessKeyId");
			}
			if (string.IsNullOrEmpty(awsSecretAccessKey))
			{
				throw new ArgumentNullException("awsSecretAccessKey");
			}
			if (string.IsNullOrEmpty(token))
			{
				throw new ArgumentNullException("token");
			}
			this._lastCredentials = new ImmutableCredentials(awsAccessKeyId, awsSecretAccessKey, token);
		}

		// Token: 0x06000854 RID: 2132 RVA: 0x0001495A File Offset: 0x00012B5A
		public override ImmutableCredentials GetCredentials()
		{
			return this._lastCredentials.Copy();
		}

		// Token: 0x06000855 RID: 2133 RVA: 0x00014968 File Offset: 0x00012B68
		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			SessionAWSCredentials sessionAWSCredentials = obj as SessionAWSCredentials;
			return sessionAWSCredentials != null && AWSSDKUtils.AreEqual(new object[]
			{
				this._lastCredentials
			}, new object[]
			{
				sessionAWSCredentials._lastCredentials
			});
		}

		// Token: 0x06000856 RID: 2134 RVA: 0x000149AA File Offset: 0x00012BAA
		public override int GetHashCode()
		{
			return Hashing.Hash(new object[]
			{
				this._lastCredentials
			});
		}

		// Token: 0x04000352 RID: 850
		private ImmutableCredentials _lastCredentials;
	}
}
