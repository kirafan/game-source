﻿using System;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime
{
	// Token: 0x020000D0 RID: 208
	public interface IResponseContext
	{
		// Token: 0x17000293 RID: 659
		// (get) Token: 0x0600086C RID: 2156
		// (set) Token: 0x0600086D RID: 2157
		AmazonWebServiceResponse Response { get; set; }

		// Token: 0x17000294 RID: 660
		// (get) Token: 0x0600086E RID: 2158
		// (set) Token: 0x0600086F RID: 2159
		IWebResponseData HttpResponse { get; set; }
	}
}
