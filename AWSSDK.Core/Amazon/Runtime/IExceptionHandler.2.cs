﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000D7 RID: 215
	public interface IExceptionHandler<T> : IExceptionHandler where T : Exception
	{
		// Token: 0x06000886 RID: 2182
		bool HandleException(IExecutionContext executionContext, T exception);
	}
}
