﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000BA RID: 186
	public enum Metric
	{
		// Token: 0x04000303 RID: 771
		AWSErrorCode,
		// Token: 0x04000304 RID: 772
		AWSRequestID,
		// Token: 0x04000305 RID: 773
		AmzId2,
		// Token: 0x04000306 RID: 774
		BytesProcessed,
		// Token: 0x04000307 RID: 775
		Exception,
		// Token: 0x04000308 RID: 776
		RedirectLocation,
		// Token: 0x04000309 RID: 777
		ResponseProcessingTime,
		// Token: 0x0400030A RID: 778
		ResponseUnmarshallTime,
		// Token: 0x0400030B RID: 779
		ResponseReadTime,
		// Token: 0x0400030C RID: 780
		StatusCode,
		// Token: 0x0400030D RID: 781
		AttemptCount,
		// Token: 0x0400030E RID: 782
		CredentialsRequestTime,
		// Token: 0x0400030F RID: 783
		HttpRequestTime,
		// Token: 0x04000310 RID: 784
		ProxyHost,
		// Token: 0x04000311 RID: 785
		ProxyPort,
		// Token: 0x04000312 RID: 786
		RequestSigningTime,
		// Token: 0x04000313 RID: 787
		RetryPauseTime,
		// Token: 0x04000314 RID: 788
		StringToSign,
		// Token: 0x04000315 RID: 789
		CanonicalRequest,
		// Token: 0x04000316 RID: 790
		AsyncCall,
		// Token: 0x04000317 RID: 791
		ClientExecuteTime,
		// Token: 0x04000318 RID: 792
		MethodName,
		// Token: 0x04000319 RID: 793
		ServiceEndpoint,
		// Token: 0x0400031A RID: 794
		ServiceName,
		// Token: 0x0400031B RID: 795
		RequestSize,
		// Token: 0x0400031C RID: 796
		AmzCfId
	}
}
