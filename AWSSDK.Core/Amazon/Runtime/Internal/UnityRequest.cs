﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000107 RID: 263
	public sealed class UnityRequest : IHttpRequest<string>, IDisposable, IUnityHttpRequest
	{
		// Token: 0x1700031B RID: 795
		// (get) Token: 0x06000A28 RID: 2600 RVA: 0x00017F02 File Offset: 0x00016102
		// (set) Token: 0x06000A29 RID: 2601 RVA: 0x00017F0A File Offset: 0x0001610A
		public Uri RequestUri { get; private set; }

		// Token: 0x1700031C RID: 796
		// (get) Token: 0x06000A2A RID: 2602 RVA: 0x00017F13 File Offset: 0x00016113
		// (set) Token: 0x06000A2B RID: 2603 RVA: 0x00017F1B File Offset: 0x0001611B
		public IDisposable WwwRequest { get; set; }

		// Token: 0x1700031D RID: 797
		// (get) Token: 0x06000A2C RID: 2604 RVA: 0x00017F24 File Offset: 0x00016124
		// (set) Token: 0x06000A2D RID: 2605 RVA: 0x00017F2C File Offset: 0x0001612C
		public byte[] RequestContent { get; private set; }

		// Token: 0x1700031E RID: 798
		// (get) Token: 0x06000A2E RID: 2606 RVA: 0x00017F35 File Offset: 0x00016135
		// (set) Token: 0x06000A2F RID: 2607 RVA: 0x00017F3D File Offset: 0x0001613D
		public Dictionary<string, string> Headers { get; private set; }

		// Token: 0x1700031F RID: 799
		// (get) Token: 0x06000A30 RID: 2608 RVA: 0x00017F46 File Offset: 0x00016146
		// (set) Token: 0x06000A31 RID: 2609 RVA: 0x00017F4E File Offset: 0x0001614E
		public AsyncCallback Callback { get; private set; }

		// Token: 0x17000320 RID: 800
		// (get) Token: 0x06000A32 RID: 2610 RVA: 0x00017F57 File Offset: 0x00016157
		// (set) Token: 0x06000A33 RID: 2611 RVA: 0x00017F5F File Offset: 0x0001615F
		public IAsyncResult AsyncResult { get; private set; }

		// Token: 0x17000321 RID: 801
		// (get) Token: 0x06000A34 RID: 2612 RVA: 0x00017F68 File Offset: 0x00016168
		// (set) Token: 0x06000A35 RID: 2613 RVA: 0x00017F70 File Offset: 0x00016170
		public ManualResetEvent WaitHandle { get; private set; }

		// Token: 0x17000322 RID: 802
		// (get) Token: 0x06000A36 RID: 2614 RVA: 0x00017F79 File Offset: 0x00016179
		// (set) Token: 0x06000A37 RID: 2615 RVA: 0x00017F81 File Offset: 0x00016181
		public bool IsSync { get; set; }

		// Token: 0x17000323 RID: 803
		// (get) Token: 0x06000A38 RID: 2616 RVA: 0x00017F8A File Offset: 0x0001618A
		// (set) Token: 0x06000A39 RID: 2617 RVA: 0x00017F92 File Offset: 0x00016192
		public IWebResponseData Response { get; set; }

		// Token: 0x17000324 RID: 804
		// (get) Token: 0x06000A3A RID: 2618 RVA: 0x00017F9B File Offset: 0x0001619B
		// (set) Token: 0x06000A3B RID: 2619 RVA: 0x00017FA3 File Offset: 0x000161A3
		public Exception Exception { get; set; }

		// Token: 0x17000325 RID: 805
		// (get) Token: 0x06000A3C RID: 2620 RVA: 0x00017FAC File Offset: 0x000161AC
		// (set) Token: 0x06000A3D RID: 2621 RVA: 0x00017FB4 File Offset: 0x000161B4
		public string Method { get; set; }

		// Token: 0x06000A3E RID: 2622 RVA: 0x00017FBD File Offset: 0x000161BD
		public UnityRequest(Uri requestUri)
		{
			this.RequestUri = requestUri;
			this.Headers = new Dictionary<string, string>();
		}

		// Token: 0x06000A3F RID: 2623 RVA: 0x00005805 File Offset: 0x00003A05
		public void ConfigureRequest(IRequestContext requestContext)
		{
		}

		// Token: 0x06000A40 RID: 2624 RVA: 0x00017FD8 File Offset: 0x000161D8
		public void SetRequestHeaders(IDictionary<string, string> headers)
		{
			foreach (KeyValuePair<string, string> item in headers)
			{
				if (!item.Key.Equals("host", StringComparison.InvariantCultureIgnoreCase) && !item.Key.Equals("Content-Length", StringComparison.InvariantCultureIgnoreCase) && !item.Key.Equals("User-Agent", StringComparison.InvariantCultureIgnoreCase))
				{
					this.Headers.Add(item);
				}
			}
		}

		// Token: 0x06000A41 RID: 2625 RVA: 0x00018064 File Offset: 0x00016264
		public string GetRequestContent()
		{
			return string.Empty;
		}

		// Token: 0x06000A42 RID: 2626 RVA: 0x0001806C File Offset: 0x0001626C
		public IWebResponseData GetResponse()
		{
			if (UnityInitializer.IsMainThread())
			{
				throw new Exception("Cannot execute synchronous calls on game thread");
			}
			this.IsSync = true;
			this.WaitHandle = new ManualResetEvent(false);
			IWebResponseData response;
			try
			{
				UnityRequestQueue.Instance.EnqueueRequest(this);
				this.WaitHandle.WaitOne();
				if (this.Exception != null)
				{
					throw this.Exception;
				}
				if (this.Exception == null && this.Response == null)
				{
					throw new WebException("Request timed out", WebExceptionStatus.Timeout);
				}
				response = this.Response;
			}
			finally
			{
				this.WaitHandle.Close();
			}
			return response;
		}

		// Token: 0x06000A43 RID: 2627 RVA: 0x00018108 File Offset: 0x00016308
		public void WriteToRequestBody(string requestContent, Stream contentStream, IDictionary<string, string> contentHeaders, IRequestContext requestContext)
		{
			byte[] array = new byte[8192];
			using (MemoryStream memoryStream = new MemoryStream())
			{
				int count;
				while ((count = contentStream.Read(array, 0, array.Length)) > 0)
				{
					memoryStream.Write(array, 0, count);
				}
				this.RequestContent = memoryStream.ToArray();
			}
		}

		// Token: 0x06000A44 RID: 2628 RVA: 0x0001816C File Offset: 0x0001636C
		public void WriteToRequestBody(string requestContent, byte[] content, IDictionary<string, string> contentHeaders)
		{
			this.RequestContent = content;
		}

		// Token: 0x06000A45 RID: 2629 RVA: 0x00005805 File Offset: 0x00003A05
		public void Abort()
		{
		}

		// Token: 0x06000A46 RID: 2630 RVA: 0x00018178 File Offset: 0x00016378
		public IAsyncResult BeginGetRequestContent(AsyncCallback callback, object state)
		{
			SimpleAsyncResult simpleAsyncResult = new SimpleAsyncResult(state);
			callback(simpleAsyncResult);
			return simpleAsyncResult;
		}

		// Token: 0x06000A47 RID: 2631 RVA: 0x00018064 File Offset: 0x00016264
		public string EndGetRequestContent(IAsyncResult asyncResult)
		{
			return string.Empty;
		}

		// Token: 0x06000A48 RID: 2632 RVA: 0x00018194 File Offset: 0x00016394
		public IAsyncResult BeginGetResponse(AsyncCallback callback, object state)
		{
			this.Callback = callback;
			this.AsyncResult = new SimpleAsyncResult(state);
			UnityRequestQueue.Instance.EnqueueRequest(this);
			return this.AsyncResult;
		}

		// Token: 0x06000A49 RID: 2633 RVA: 0x000181BA File Offset: 0x000163BA
		public IWebResponseData EndGetResponse(IAsyncResult asyncResult)
		{
			if (this.Exception != null)
			{
				throw this.Exception;
			}
			return this.Response;
		}

		// Token: 0x06000A4A RID: 2634 RVA: 0x000181D1 File Offset: 0x000163D1
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x06000A4B RID: 2635 RVA: 0x000181DA File Offset: 0x000163DA
		private void Dispose(bool disposing)
		{
			if (this._disposed)
			{
				return;
			}
			if (disposing)
			{
				UnityRequestQueue.Instance.ExecuteOnMainThread(delegate
				{
					this._disposed = true;
				});
			}
		}

		// Token: 0x17000326 RID: 806
		// (get) Token: 0x06000A4C RID: 2636 RVA: 0x000181FE File Offset: 0x000163FE
		// (set) Token: 0x06000A4D RID: 2637 RVA: 0x00018206 File Offset: 0x00016406
		private StreamReadTracker Tracker { get; set; }

		// Token: 0x06000A4E RID: 2638 RVA: 0x0001820F File Offset: 0x0001640F
		public Stream SetupProgressListeners(Stream originalStream, long progressUpdateInterval, object sender, EventHandler<StreamTransferProgressArgs> callback)
		{
			this.Tracker = new StreamReadTracker(sender, callback, originalStream.Length, progressUpdateInterval);
			return originalStream;
		}

		// Token: 0x06000A4F RID: 2639 RVA: 0x00018227 File Offset: 0x00016427
		public void OnUploadProgressChanged(float progress)
		{
			if (this.Tracker != null)
			{
				this.Tracker.UpdateProgress(progress);
			}
		}

		// Token: 0x040003E5 RID: 997
		private bool _disposed;
	}
}
