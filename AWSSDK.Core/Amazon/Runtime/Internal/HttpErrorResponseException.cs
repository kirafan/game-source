﻿using System;
using System.Runtime.Serialization;
using System.Security;
using System.Security.Permissions;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F6 RID: 246
	[Serializable]
	public class HttpErrorResponseException : Exception
	{
		// Token: 0x1700030E RID: 782
		// (get) Token: 0x060009AB RID: 2475 RVA: 0x0001617E File Offset: 0x0001437E
		// (set) Token: 0x060009AC RID: 2476 RVA: 0x00016186 File Offset: 0x00014386
		public IWebResponseData Response { get; private set; }

		// Token: 0x060009AD RID: 2477 RVA: 0x0001618F File Offset: 0x0001438F
		public HttpErrorResponseException(IWebResponseData response)
		{
			this.Response = response;
		}

		// Token: 0x060009AE RID: 2478 RVA: 0x0001619E File Offset: 0x0001439E
		public HttpErrorResponseException(string message, IWebResponseData response) : base(message)
		{
			this.Response = response;
		}

		// Token: 0x060009AF RID: 2479 RVA: 0x000161AE File Offset: 0x000143AE
		public HttpErrorResponseException(string message, Exception innerException, IWebResponseData response) : base(message, innerException)
		{
			this.Response = response;
		}

		// Token: 0x060009B0 RID: 2480 RVA: 0x000161BF File Offset: 0x000143BF
		protected HttpErrorResponseException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if (info != null)
			{
				this.Response = (IWebResponseData)info.GetValue("Response", typeof(IWebResponseData));
			}
		}

		// Token: 0x060009B1 RID: 2481 RVA: 0x000161EC File Offset: 0x000143EC
		[SecurityCritical]
		[SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			if (info != null)
			{
				info.AddValue("Response", this.Response);
			}
		}
	}
}
