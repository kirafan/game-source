﻿using System;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000104 RID: 260
	public class Unmarshaller : PipelineHandler
	{
		// Token: 0x06000A10 RID: 2576 RVA: 0x00017550 File Offset: 0x00015750
		public Unmarshaller(bool supportsResponseLogging)
		{
			this._supportsResponseLogging = supportsResponseLogging;
		}

		// Token: 0x06000A11 RID: 2577 RVA: 0x0001755F File Offset: 0x0001575F
		public override void InvokeSync(IExecutionContext executionContext)
		{
			base.InvokeSync(executionContext);
			if (executionContext.ResponseContext.HttpResponse.IsSuccessStatusCode)
			{
				this.Unmarshall(executionContext);
			}
		}

		// Token: 0x06000A12 RID: 2578 RVA: 0x00017581 File Offset: 0x00015781
		protected override void InvokeAsyncCallback(IAsyncExecutionContext executionContext)
		{
			if (executionContext.ResponseContext.AsyncResult.Exception == null)
			{
				this.Unmarshall(ExecutionContext.CreateFromAsyncContext(executionContext));
			}
			base.InvokeAsyncCallback(executionContext);
		}

		// Token: 0x06000A13 RID: 2579 RVA: 0x000175A8 File Offset: 0x000157A8
		private void Unmarshall(IExecutionContext executionContext)
		{
			IRequestContext requestContext = executionContext.RequestContext;
			IResponseContext responseContext = executionContext.ResponseContext;
			using (requestContext.Metrics.StartEvent(Metric.ResponseProcessingTime))
			{
				ResponseUnmarshaller unmarshaller = requestContext.Unmarshaller;
				try
				{
					bool supportsResponseLogging = this._supportsResponseLogging;
					UnmarshallerContext unmarshallerContext = unmarshaller.CreateContext(responseContext.HttpResponse, supportsResponseLogging, responseContext.HttpResponse.ResponseBody.OpenResponse(), requestContext.Metrics);
					try
					{
						AmazonWebServiceResponse response = this.UnmarshallResponse(unmarshallerContext, requestContext);
						responseContext.Response = response;
					}
					catch (Exception ex)
					{
						if (ex is AmazonServiceException || ex is AmazonClientException)
						{
							throw;
						}
						string headerValue = responseContext.HttpResponse.GetHeaderValue("x-amzn-RequestId");
						string responseBody = unmarshallerContext.ResponseBody;
						throw new AmazonUnmarshallingException(headerValue, null, responseBody, ex);
					}
				}
				finally
				{
					if (!unmarshaller.HasStreamingProperty)
					{
						responseContext.HttpResponse.ResponseBody.Dispose();
					}
				}
			}
		}

		// Token: 0x06000A14 RID: 2580 RVA: 0x000176A0 File Offset: 0x000158A0
		private AmazonWebServiceResponse UnmarshallResponse(UnmarshallerContext context, IRequestContext requestContext)
		{
			AmazonWebServiceResponse result;
			try
			{
				ResponseUnmarshaller unmarshaller = requestContext.Unmarshaller;
				AmazonWebServiceResponse amazonWebServiceResponse = null;
				using (requestContext.Metrics.StartEvent(Metric.ResponseUnmarshallTime))
				{
					amazonWebServiceResponse = unmarshaller.UnmarshallResponse(context);
				}
				requestContext.Metrics.AddProperty(Metric.StatusCode, amazonWebServiceResponse.HttpStatusCode);
				requestContext.Metrics.AddProperty(Metric.BytesProcessed, amazonWebServiceResponse.ContentLength);
				if (amazonWebServiceResponse.ResponseMetadata != null)
				{
					requestContext.Metrics.AddProperty(Metric.AWSRequestID, amazonWebServiceResponse.ResponseMetadata.RequestId);
				}
				context.ValidateCRC32IfAvailable();
				result = amazonWebServiceResponse;
			}
			finally
			{
				if (Unmarshaller.ShouldLogResponseBody(this._supportsResponseLogging, requestContext))
				{
					this.Logger.DebugFormat("Received response (truncated to {0} bytes): [{1}]", new object[]
					{
						AWSConfigs.LoggingConfig.LogResponsesSizeLimit,
						context.ResponseBody
					});
				}
			}
			return result;
		}

		// Token: 0x06000A15 RID: 2581 RVA: 0x0001778C File Offset: 0x0001598C
		private static bool ShouldLogResponseBody(bool supportsResponseLogging, IRequestContext requestContext)
		{
			return supportsResponseLogging && (requestContext.ClientConfig.LogResponse || AWSConfigs.LoggingConfig.LogResponses == ResponseLoggingOption.Always);
		}

		// Token: 0x040003D5 RID: 981
		private bool _supportsResponseLogging;
	}
}
