﻿using System;
using System.Globalization;
using System.Reflection;
using Amazon.Util.Internal;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000EE RID: 238
	public static class ServiceClientHelpers
	{
		// Token: 0x06000968 RID: 2408 RVA: 0x00015BBC File Offset: 0x00013DBC
		public static TClient CreateServiceFromAnother<TClient, TConfig>(AmazonServiceClient originalServiceClient) where TClient : AmazonServiceClient where TConfig : ClientConfig, new()
		{
			AWSCredentials credentials = originalServiceClient.Credentials;
			TConfig tconfig = originalServiceClient.CloneConfig<TConfig>();
			return TypeFactory.GetTypeInfo(typeof(TClient)).GetConstructor(new ITypeInfo[]
			{
				TypeFactory.GetTypeInfo(typeof(AWSCredentials)),
				TypeFactory.GetTypeInfo(tconfig.GetType())
			}).Invoke(new object[]
			{
				credentials,
				tconfig
			}) as TClient;
		}

		// Token: 0x06000969 RID: 2409 RVA: 0x00015C38 File Offset: 0x00013E38
		public static TClient CreateServiceFromAssembly<TClient>(string assemblyName, string serviceClientClassName, RegionEndpoint region) where TClient : class
		{
			return ServiceClientHelpers.LoadServiceClientType(assemblyName, serviceClientClassName).GetConstructor(new ITypeInfo[]
			{
				TypeFactory.GetTypeInfo(typeof(RegionEndpoint))
			}).Invoke(new object[]
			{
				region
			}) as TClient;
		}

		// Token: 0x0600096A RID: 2410 RVA: 0x00015C78 File Offset: 0x00013E78
		public static TClient CreateServiceFromAssembly<TClient>(string assemblyName, string serviceClientClassName, AWSCredentials credentials, RegionEndpoint region) where TClient : class
		{
			return ServiceClientHelpers.LoadServiceClientType(assemblyName, serviceClientClassName).GetConstructor(new ITypeInfo[]
			{
				TypeFactory.GetTypeInfo(typeof(AWSCredentials)),
				TypeFactory.GetTypeInfo(typeof(RegionEndpoint))
			}).Invoke(new object[]
			{
				credentials,
				region
			}) as TClient;
		}

		// Token: 0x0600096B RID: 2411 RVA: 0x00015CD8 File Offset: 0x00013ED8
		public static TClient CreateServiceFromAssembly<TClient>(string assemblyName, string serviceClientClassName, AWSCredentials credentials, ClientConfig config) where TClient : class
		{
			return ServiceClientHelpers.LoadServiceClientType(assemblyName, serviceClientClassName).GetConstructor(new ITypeInfo[]
			{
				TypeFactory.GetTypeInfo(typeof(AWSCredentials)),
				TypeFactory.GetTypeInfo(config.GetType())
			}).Invoke(new object[]
			{
				credentials,
				config
			}) as TClient;
		}

		// Token: 0x0600096C RID: 2412 RVA: 0x00015D34 File Offset: 0x00013F34
		public static TClient CreateServiceFromAssembly<TClient>(string assemblyName, string serviceClientClassName, AmazonServiceClient originalServiceClient) where TClient : class
		{
			ITypeInfo typeInfo = ServiceClientHelpers.LoadServiceClientType(assemblyName, serviceClientClassName);
			ClientConfig clientConfig = ServiceClientHelpers.CreateServiceConfig(assemblyName, serviceClientClassName.Replace("Client", "Config"));
			originalServiceClient.CloneConfig(clientConfig);
			return typeInfo.GetConstructor(new ITypeInfo[]
			{
				TypeFactory.GetTypeInfo(typeof(AWSCredentials)),
				TypeFactory.GetTypeInfo(clientConfig.GetType())
			}).Invoke(new object[]
			{
				originalServiceClient.Credentials,
				clientConfig
			}) as TClient;
		}

		// Token: 0x0600096D RID: 2413 RVA: 0x00015DB3 File Offset: 0x00013FB3
		public static ClientConfig CreateServiceConfig(string assemblyName, string serviceConfigClassName)
		{
			return ServiceClientHelpers.LoadServiceConfigType(assemblyName, serviceConfigClassName).GetConstructor(new ITypeInfo[0]).Invoke(new object[0]) as ClientConfig;
		}

		// Token: 0x0600096E RID: 2414 RVA: 0x00015DD7 File Offset: 0x00013FD7
		private static ITypeInfo LoadServiceClientType(string assemblyName, string serviceClientClassName)
		{
			return TypeFactory.GetTypeInfo(ServiceClientHelpers.GetSDKAssembly(assemblyName).GetType(serviceClientClassName));
		}

		// Token: 0x0600096F RID: 2415 RVA: 0x00015DD7 File Offset: 0x00013FD7
		private static ITypeInfo LoadServiceConfigType(string assemblyName, string serviceConfigClassName)
		{
			return TypeFactory.GetTypeInfo(ServiceClientHelpers.GetSDKAssembly(assemblyName).GetType(serviceConfigClassName));
		}

		// Token: 0x06000970 RID: 2416 RVA: 0x00015DEC File Offset: 0x00013FEC
		private static Assembly GetSDKAssembly(string assemblyName)
		{
			Assembly assembly = null;
			if (assembly == null)
			{
				assembly = Assembly.Load(new AssemblyName(assemblyName));
			}
			if (assembly == null)
			{
				throw new AmazonClientException(string.Format(CultureInfo.InvariantCulture, "Failed to load assembly. Be sure to include a reference to {0}.", new object[]
				{
					assemblyName
				}));
			}
			return assembly;
		}

		// Token: 0x040003A6 RID: 934
		public const string S3_ASSEMBLY_NAME = "AWSSDK.S3";

		// Token: 0x040003A7 RID: 935
		public const string S3_SERVICE_CLASS_NAME = "Amazon.S3.AmazonS3Client";

		// Token: 0x040003A8 RID: 936
		public const string STS_ASSEMBLY_NAME = "AWSSDK.SecurityToken";

		// Token: 0x040003A9 RID: 937
		public const string STS_SERVICE_CLASS_NAME = "Amazon.SecurityToken.AmazonSecurityTokenServiceClient";

		// Token: 0x040003AA RID: 938
		public const string STS_SERVICE_CONFIG_NAME = "Amazon.SecurityToken.AmazonSecurityTokenServiceConfig";
	}
}
