﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000118 RID: 280
	internal class BackgroundDispatcher<T> : IDisposable
	{
		// Token: 0x1700035D RID: 861
		// (get) Token: 0x06000B09 RID: 2825 RVA: 0x0001A0A9 File Offset: 0x000182A9
		// (set) Token: 0x06000B0A RID: 2826 RVA: 0x0001A0B1 File Offset: 0x000182B1
		public bool IsRunning { get; private set; }

		// Token: 0x06000B0B RID: 2827 RVA: 0x0001A0BC File Offset: 0x000182BC
		public BackgroundDispatcher(Action<T> action)
		{
			if (action == null)
			{
				throw new ArgumentNullException("action");
			}
			this.queue = new Queue<T>();
			this.resetEvent = new AutoResetEvent(false);
			this.shouldStop = false;
			this.action = action;
			this.backgroundThread = new Thread(new ThreadStart(this.Run));
			this.backgroundThread.IsBackground = true;
			this.backgroundThread.Start();
		}

		// Token: 0x06000B0C RID: 2828 RVA: 0x0001A130 File Offset: 0x00018330
		~BackgroundDispatcher()
		{
			this.Stop();
			this.Dispose(false);
		}

		// Token: 0x06000B0D RID: 2829 RVA: 0x0001A164 File Offset: 0x00018364
		protected virtual void Dispose(bool disposing)
		{
			if (!this.isDisposed)
			{
				if (disposing && this.resetEvent != null)
				{
					this.resetEvent.Close();
					this.resetEvent = null;
				}
				this.isDisposed = true;
			}
		}

		// Token: 0x06000B0E RID: 2830 RVA: 0x0001A192 File Offset: 0x00018392
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000B0F RID: 2831 RVA: 0x0001A1A4 File Offset: 0x000183A4
		public void Dispatch(T data)
		{
			if (this.queue.Count < 100)
			{
				Queue<T> obj = this.queue;
				lock (obj)
				{
					if (this.queue.Count < 100)
					{
						this.queue.Enqueue(data);
					}
				}
			}
			this.resetEvent.Set();
		}

		// Token: 0x06000B10 RID: 2832 RVA: 0x0001A210 File Offset: 0x00018410
		public void Stop()
		{
			this.shouldStop = true;
			this.resetEvent.Set();
		}

		// Token: 0x06000B11 RID: 2833 RVA: 0x0001A225 File Offset: 0x00018425
		private void Run()
		{
			this.IsRunning = true;
			while (!this.shouldStop)
			{
				this.HandleInvoked();
				this.resetEvent.WaitOne();
			}
			this.HandleInvoked();
			this.IsRunning = false;
		}

		// Token: 0x06000B12 RID: 2834 RVA: 0x0001A258 File Offset: 0x00018458
		private void HandleInvoked()
		{
			for (;;)
			{
				bool flag = false;
				T obj = default(T);
				Queue<T> obj2 = this.queue;
				lock (obj2)
				{
					if (this.queue.Count > 0)
					{
						obj = this.queue.Dequeue();
						flag = true;
					}
				}
				if (flag)
				{
					try
					{
						this.action(obj);
						continue;
					}
					catch
					{
						continue;
					}
					break;
				}
				break;
			}
		}

		// Token: 0x0400044C RID: 1100
		private bool isDisposed;

		// Token: 0x0400044D RID: 1101
		private Action<T> action;

		// Token: 0x0400044E RID: 1102
		private Queue<T> queue;

		// Token: 0x0400044F RID: 1103
		private Thread backgroundThread;

		// Token: 0x04000450 RID: 1104
		private AutoResetEvent resetEvent;

		// Token: 0x04000451 RID: 1105
		private bool shouldStop;

		// Token: 0x04000453 RID: 1107
		private const int MAX_QUEUE_SIZE = 100;
	}
}
