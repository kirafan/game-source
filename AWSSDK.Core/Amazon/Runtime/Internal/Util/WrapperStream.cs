﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200013A RID: 314
	public class WrapperStream : Stream
	{
		// Token: 0x170003A7 RID: 935
		// (get) Token: 0x06000C40 RID: 3136 RVA: 0x0001D3E5 File Offset: 0x0001B5E5
		// (set) Token: 0x06000C41 RID: 3137 RVA: 0x0001D3ED File Offset: 0x0001B5ED
		private protected Stream BaseStream { protected get; private set; }

		// Token: 0x06000C42 RID: 3138 RVA: 0x0001D3F6 File Offset: 0x0001B5F6
		public WrapperStream(Stream baseStream)
		{
			if (baseStream == null)
			{
				throw new ArgumentNullException("baseStream");
			}
			this.BaseStream = baseStream;
		}

		// Token: 0x06000C43 RID: 3139 RVA: 0x0001D414 File Offset: 0x0001B614
		public Stream GetNonWrapperBaseStream()
		{
			Stream stream = this;
			PartialWrapperStream partialWrapperStream;
			for (;;)
			{
				partialWrapperStream = (stream as PartialWrapperStream);
				if (partialWrapperStream != null)
				{
					break;
				}
				stream = (stream as WrapperStream).BaseStream;
				if (!(stream is WrapperStream))
				{
					return stream;
				}
			}
			return partialWrapperStream;
		}

		// Token: 0x06000C44 RID: 3140 RVA: 0x0001D444 File Offset: 0x0001B644
		public Stream GetSeekableBaseStream()
		{
			Stream stream = this;
			while (!stream.CanSeek)
			{
				stream = (stream as WrapperStream).BaseStream;
				if (!(stream is WrapperStream))
				{
					if (!stream.CanSeek)
					{
						throw new InvalidOperationException("Unable to find seekable stream");
					}
					return stream;
				}
			}
			return stream;
		}

		// Token: 0x06000C45 RID: 3141 RVA: 0x0001D488 File Offset: 0x0001B688
		public static Stream GetNonWrapperBaseStream(Stream stream)
		{
			WrapperStream wrapperStream = stream as WrapperStream;
			if (wrapperStream == null)
			{
				return stream;
			}
			return wrapperStream.GetNonWrapperBaseStream();
		}

		// Token: 0x06000C46 RID: 3142 RVA: 0x0001D4A8 File Offset: 0x0001B6A8
		public Stream SearchWrappedStream(Func<Stream, bool> condition)
		{
			Stream stream = this;
			while (!condition(stream))
			{
				if (!(stream is WrapperStream))
				{
					return null;
				}
				stream = (stream as WrapperStream).BaseStream;
				if (stream == null)
				{
					return stream;
				}
			}
			return stream;
		}

		// Token: 0x06000C47 RID: 3143 RVA: 0x0001D4DC File Offset: 0x0001B6DC
		public static Stream SearchWrappedStream(Stream stream, Func<Stream, bool> condition)
		{
			WrapperStream wrapperStream = stream as WrapperStream;
			if (wrapperStream != null)
			{
				return wrapperStream.SearchWrappedStream(condition);
			}
			if (!condition(stream))
			{
				return null;
			}
			return stream;
		}

		// Token: 0x170003A8 RID: 936
		// (get) Token: 0x06000C48 RID: 3144 RVA: 0x0001A379 File Offset: 0x00018579
		public override bool CanRead
		{
			get
			{
				return this.BaseStream.CanRead;
			}
		}

		// Token: 0x170003A9 RID: 937
		// (get) Token: 0x06000C49 RID: 3145 RVA: 0x0001A386 File Offset: 0x00018586
		public override bool CanSeek
		{
			get
			{
				return this.BaseStream.CanSeek;
			}
		}

		// Token: 0x170003AA RID: 938
		// (get) Token: 0x06000C4A RID: 3146 RVA: 0x0001A3A0 File Offset: 0x000185A0
		public override bool CanWrite
		{
			get
			{
				return this.BaseStream.CanWrite;
			}
		}

		// Token: 0x06000C4B RID: 3147 RVA: 0x0001D507 File Offset: 0x0001B707
		public override void Close()
		{
			this.BaseStream.Close();
		}

		// Token: 0x170003AB RID: 939
		// (get) Token: 0x06000C4C RID: 3148 RVA: 0x0001A3AD File Offset: 0x000185AD
		public override long Length
		{
			get
			{
				return this.BaseStream.Length;
			}
		}

		// Token: 0x170003AC RID: 940
		// (get) Token: 0x06000C4D RID: 3149 RVA: 0x0001A3BA File Offset: 0x000185BA
		// (set) Token: 0x06000C4E RID: 3150 RVA: 0x0001A3C7 File Offset: 0x000185C7
		public override long Position
		{
			get
			{
				return this.BaseStream.Position;
			}
			set
			{
				this.BaseStream.Position = value;
			}
		}

		// Token: 0x170003AD RID: 941
		// (get) Token: 0x06000C4F RID: 3151 RVA: 0x0001A3D5 File Offset: 0x000185D5
		// (set) Token: 0x06000C50 RID: 3152 RVA: 0x0001A3E2 File Offset: 0x000185E2
		public override int ReadTimeout
		{
			get
			{
				return this.BaseStream.ReadTimeout;
			}
			set
			{
				this.BaseStream.ReadTimeout = value;
			}
		}

		// Token: 0x170003AE RID: 942
		// (get) Token: 0x06000C51 RID: 3153 RVA: 0x0001A3F0 File Offset: 0x000185F0
		// (set) Token: 0x06000C52 RID: 3154 RVA: 0x0001A3FD File Offset: 0x000185FD
		public override int WriteTimeout
		{
			get
			{
				return this.BaseStream.WriteTimeout;
			}
			set
			{
				this.BaseStream.WriteTimeout = value;
			}
		}

		// Token: 0x06000C53 RID: 3155 RVA: 0x0001A40B File Offset: 0x0001860B
		public override void Flush()
		{
			this.BaseStream.Flush();
		}

		// Token: 0x06000C54 RID: 3156 RVA: 0x0001D514 File Offset: 0x0001B714
		public override int Read(byte[] buffer, int offset, int count)
		{
			return this.BaseStream.Read(buffer, offset, count);
		}

		// Token: 0x06000C55 RID: 3157 RVA: 0x0001A449 File Offset: 0x00018649
		public override long Seek(long offset, SeekOrigin origin)
		{
			return this.BaseStream.Seek(offset, origin);
		}

		// Token: 0x06000C56 RID: 3158 RVA: 0x0001A458 File Offset: 0x00018658
		public override void SetLength(long value)
		{
			this.BaseStream.SetLength(value);
		}

		// Token: 0x06000C57 RID: 3159 RVA: 0x0001D524 File Offset: 0x0001B724
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.BaseStream.Write(buffer, offset, count);
		}
	}
}
