﻿using System;
using ThirdParty.MD5;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200013B RID: 315
	public class HashingWrapperMD5 : HashingWrapper
	{
		// Token: 0x06000C58 RID: 3160 RVA: 0x0001D534 File Offset: 0x0001B734
		public HashingWrapperMD5() : base(typeof(MD5Managed).FullName)
		{
		}
	}
}
