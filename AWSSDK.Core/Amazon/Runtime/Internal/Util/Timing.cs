﻿using System;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200012D RID: 301
	public class Timing : IMetricsTiming
	{
		// Token: 0x06000BD0 RID: 3024 RVA: 0x0001C520 File Offset: 0x0001A720
		public Timing()
		{
			this.startTime = (this.endTime = 0L);
			this.IsFinished = true;
		}

		// Token: 0x06000BD1 RID: 3025 RVA: 0x0001C54B File Offset: 0x0001A74B
		public Timing(long currentTime)
		{
			this.startTime = currentTime;
			this.IsFinished = false;
		}

		// Token: 0x06000BD2 RID: 3026 RVA: 0x0001C561 File Offset: 0x0001A761
		public void Stop(long currentTime)
		{
			this.endTime = currentTime;
			this.IsFinished = true;
		}

		// Token: 0x17000388 RID: 904
		// (get) Token: 0x06000BD3 RID: 3027 RVA: 0x0001C571 File Offset: 0x0001A771
		// (set) Token: 0x06000BD4 RID: 3028 RVA: 0x0001C579 File Offset: 0x0001A779
		public bool IsFinished { get; private set; }

		// Token: 0x17000389 RID: 905
		// (get) Token: 0x06000BD5 RID: 3029 RVA: 0x0001C582 File Offset: 0x0001A782
		public long ElapsedTicks
		{
			get
			{
				if (this.IsFinished)
				{
					return this.endTime - this.startTime;
				}
				return 0L;
			}
		}

		// Token: 0x1700038A RID: 906
		// (get) Token: 0x06000BD6 RID: 3030 RVA: 0x0001C59C File Offset: 0x0001A79C
		public TimeSpan ElapsedTime
		{
			get
			{
				return TimeSpan.FromTicks(this.ElapsedTicks);
			}
		}

		// Token: 0x0400048F RID: 1167
		private long startTime;

		// Token: 0x04000490 RID: 1168
		private long endTime;
	}
}
