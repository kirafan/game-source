﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000131 RID: 305
	public class PartialWrapperStream : WrapperStream
	{
		// Token: 0x06000BE8 RID: 3048 RVA: 0x0001C6FC File Offset: 0x0001A8FC
		public PartialWrapperStream(Stream stream, long partSize) : base(stream)
		{
			if (!stream.CanSeek)
			{
				throw new InvalidOperationException("Base stream of PartialWrapperStream must be seekable");
			}
			this.initialPosition = stream.Position;
			long num = stream.Length - stream.Position;
			if (partSize == 0L || num < partSize)
			{
				this.partSize = num;
				return;
			}
			this.partSize = partSize;
		}

		// Token: 0x1700038F RID: 911
		// (get) Token: 0x06000BE9 RID: 3049 RVA: 0x0001C753 File Offset: 0x0001A953
		private long RemainingPartSize
		{
			get
			{
				return this.partSize - this.Position;
			}
		}

		// Token: 0x17000390 RID: 912
		// (get) Token: 0x06000BEA RID: 3050 RVA: 0x0001C764 File Offset: 0x0001A964
		public override long Length
		{
			get
			{
				long num = base.Length - this.initialPosition;
				if (num > this.partSize)
				{
					num = this.partSize;
				}
				return num;
			}
		}

		// Token: 0x17000391 RID: 913
		// (get) Token: 0x06000BEB RID: 3051 RVA: 0x0001C790 File Offset: 0x0001A990
		// (set) Token: 0x06000BEC RID: 3052 RVA: 0x0001C79F File Offset: 0x0001A99F
		public override long Position
		{
			get
			{
				return base.Position - this.initialPosition;
			}
			set
			{
				base.Position = this.initialPosition + value;
			}
		}

		// Token: 0x06000BED RID: 3053 RVA: 0x0001C7B0 File Offset: 0x0001A9B0
		public override int Read(byte[] buffer, int offset, int count)
		{
			int num = ((long)count < this.RemainingPartSize) ? count : ((int)this.RemainingPartSize);
			if (num < 0)
			{
				return 0;
			}
			return base.Read(buffer, offset, num);
		}

		// Token: 0x06000BEE RID: 3054 RVA: 0x0001C7E4 File Offset: 0x0001A9E4
		public override long Seek(long offset, SeekOrigin origin)
		{
			long num = 0L;
			switch (origin)
			{
			case SeekOrigin.Begin:
				num = this.initialPosition + offset;
				break;
			case SeekOrigin.Current:
				num = base.Position + offset;
				break;
			case SeekOrigin.End:
				num = base.Position + this.partSize + offset;
				break;
			}
			if (num < this.initialPosition)
			{
				num = this.initialPosition;
			}
			else if (num > this.initialPosition + this.partSize)
			{
				num = this.initialPosition + this.partSize;
			}
			base.Position = num;
			return this.Position;
		}

		// Token: 0x06000BEF RID: 3055 RVA: 0x00003EFE File Offset: 0x000020FE
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000BF0 RID: 3056 RVA: 0x00003EFE File Offset: 0x000020FE
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000BF1 RID: 3057 RVA: 0x00003EFE File Offset: 0x000020FE
		public override void WriteByte(byte value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x04000499 RID: 1177
		private long initialPosition;

		// Token: 0x0400049A RID: 1178
		private long partSize;
	}
}
