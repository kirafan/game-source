﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000116 RID: 278
	internal class AlwaysSendList<T> : List<T>
	{
		// Token: 0x06000B04 RID: 2820 RVA: 0x0001A06C File Offset: 0x0001826C
		public AlwaysSendList()
		{
		}

		// Token: 0x06000B05 RID: 2821 RVA: 0x0001A074 File Offset: 0x00018274
		public AlwaysSendList(IEnumerable<T> collection) : base(collection ?? new List<T>())
		{
		}
	}
}
