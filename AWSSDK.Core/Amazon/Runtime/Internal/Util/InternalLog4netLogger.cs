﻿using System;
using System.Globalization;
using System.Reflection;
using Amazon.Util.Internal;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000127 RID: 295
	internal class InternalLog4netLogger : InternalLogger
	{
		// Token: 0x06000B8A RID: 2954 RVA: 0x0001B020 File Offset: 0x00019220
		private static void loadStatics()
		{
			object @lock = InternalLog4netLogger.LOCK;
			lock (@lock)
			{
				if (InternalLog4netLogger.loadState == InternalLog4netLogger.LoadState.Uninitialized)
				{
					InternalLog4netLogger.loadState = InternalLog4netLogger.LoadState.Loading;
					try
					{
						InternalLog4netLogger.loggerType = Type.GetType("Amazon.Runtime.Internal.Util.Logger");
						InternalLog4netLogger.logMangerType = Type.GetType("log4net.Core.LoggerManager, log4net");
						InternalLog4netLogger.logMangerTypeInfo = TypeFactory.GetTypeInfo(InternalLog4netLogger.logMangerType);
						if (InternalLog4netLogger.logMangerType == null)
						{
							InternalLog4netLogger.loadState = InternalLog4netLogger.LoadState.Failed;
						}
						else
						{
							InternalLog4netLogger.getLoggerWithTypeMethod = InternalLog4netLogger.logMangerTypeInfo.GetMethod("GetLogger", new ITypeInfo[]
							{
								TypeFactory.GetTypeInfo(typeof(Assembly)),
								TypeFactory.GetTypeInfo(typeof(Type))
							});
							InternalLog4netLogger.logType = Type.GetType("log4net.Core.ILogger, log4net");
							InternalLog4netLogger.logTypeInfo = TypeFactory.GetTypeInfo(InternalLog4netLogger.logType);
							InternalLog4netLogger.levelType = Type.GetType("log4net.Core.Level, log4net");
							InternalLog4netLogger.levelTypeInfo = TypeFactory.GetTypeInfo(InternalLog4netLogger.levelType);
							InternalLog4netLogger.debugLevelPropertyValue = InternalLog4netLogger.levelTypeInfo.GetField("Debug").GetValue(null);
							InternalLog4netLogger.infoLevelPropertyValue = InternalLog4netLogger.levelTypeInfo.GetField("Info").GetValue(null);
							InternalLog4netLogger.errorLevelPropertyValue = InternalLog4netLogger.levelTypeInfo.GetField("Error").GetValue(null);
							InternalLog4netLogger.systemStringFormatType = Type.GetType("log4net.Util.SystemStringFormat, log4net");
							InternalLog4netLogger.logMethod = InternalLog4netLogger.logTypeInfo.GetMethod("Log", new ITypeInfo[]
							{
								TypeFactory.GetTypeInfo(typeof(Type)),
								InternalLog4netLogger.levelTypeInfo,
								TypeFactory.GetTypeInfo(typeof(object)),
								TypeFactory.GetTypeInfo(typeof(Exception))
							});
							InternalLog4netLogger.isEnabledForMethod = InternalLog4netLogger.logTypeInfo.GetMethod("IsEnabledFor", new ITypeInfo[]
							{
								InternalLog4netLogger.levelTypeInfo
							});
							if (InternalLog4netLogger.getLoggerWithTypeMethod == null || InternalLog4netLogger.isEnabledForMethod == null || InternalLog4netLogger.logType == null || InternalLog4netLogger.levelType == null || InternalLog4netLogger.logMethod == null)
							{
								InternalLog4netLogger.loadState = InternalLog4netLogger.LoadState.Failed;
							}
							else
							{
								if (AWSConfigs.XmlSectionExists("log4net") && (AWSConfigs.LoggingConfig.LogTo & LoggingOptions.Log4Net) == LoggingOptions.Log4Net)
								{
									ITypeInfo typeInfo = TypeFactory.GetTypeInfo(Type.GetType("log4net.Config.XmlConfigurator, log4net"));
									if (typeInfo != null)
									{
										MethodInfo method = typeInfo.GetMethod("Configure", new ITypeInfo[0]);
										if (method != null)
										{
											method.Invoke(null, null);
										}
									}
								}
								InternalLog4netLogger.loadState = InternalLog4netLogger.LoadState.Success;
							}
						}
					}
					catch
					{
						InternalLog4netLogger.loadState = InternalLog4netLogger.LoadState.Failed;
					}
				}
			}
		}

		// Token: 0x06000B8B RID: 2955 RVA: 0x0001B2A0 File Offset: 0x000194A0
		public InternalLog4netLogger(Type declaringType) : base(declaringType)
		{
			if (InternalLog4netLogger.loadState == InternalLog4netLogger.LoadState.Uninitialized)
			{
				InternalLog4netLogger.loadStatics();
			}
			if (InternalLog4netLogger.logMangerType == null)
			{
				return;
			}
			this.internalLogger = InternalLog4netLogger.getLoggerWithTypeMethod.Invoke(null, new object[]
			{
				TypeFactory.GetTypeInfo(declaringType).Assembly,
				declaringType
			});
		}

		// Token: 0x06000B8C RID: 2956 RVA: 0x00005805 File Offset: 0x00003A05
		public override void Flush()
		{
		}

		// Token: 0x17000375 RID: 885
		// (get) Token: 0x06000B8D RID: 2957 RVA: 0x0001B2F4 File Offset: 0x000194F4
		public override bool IsErrorEnabled
		{
			get
			{
				if (this.isErrorEnabled == null)
				{
					if (InternalLog4netLogger.loadState != InternalLog4netLogger.LoadState.Success || this.internalLogger == null || InternalLog4netLogger.loggerType == null || InternalLog4netLogger.systemStringFormatType == null || InternalLog4netLogger.errorLevelPropertyValue == null)
					{
						this.isErrorEnabled = new bool?(false);
					}
					else
					{
						this.isErrorEnabled = new bool?(Convert.ToBoolean(InternalLog4netLogger.isEnabledForMethod.Invoke(this.internalLogger, new object[]
						{
							InternalLog4netLogger.errorLevelPropertyValue
						}), CultureInfo.InvariantCulture));
					}
				}
				return this.isErrorEnabled.Value;
			}
		}

		// Token: 0x06000B8E RID: 2958 RVA: 0x0001B37F File Offset: 0x0001957F
		public override void Error(Exception exception, string messageFormat, params object[] args)
		{
			InternalLog4netLogger.logMethod.Invoke(this.internalLogger, new object[]
			{
				InternalLog4netLogger.loggerType,
				InternalLog4netLogger.errorLevelPropertyValue,
				new LogMessage(CultureInfo.InvariantCulture, messageFormat, args),
				exception
			});
		}

		// Token: 0x17000376 RID: 886
		// (get) Token: 0x06000B8F RID: 2959 RVA: 0x0001B3BC File Offset: 0x000195BC
		public override bool IsDebugEnabled
		{
			get
			{
				if (this.isDebugEnabled == null)
				{
					if (InternalLog4netLogger.loadState != InternalLog4netLogger.LoadState.Success || this.internalLogger == null || InternalLog4netLogger.loggerType == null || InternalLog4netLogger.systemStringFormatType == null || InternalLog4netLogger.debugLevelPropertyValue == null)
					{
						this.isDebugEnabled = new bool?(false);
					}
					else
					{
						this.isDebugEnabled = new bool?(Convert.ToBoolean(InternalLog4netLogger.isEnabledForMethod.Invoke(this.internalLogger, new object[]
						{
							InternalLog4netLogger.debugLevelPropertyValue
						}), CultureInfo.InvariantCulture));
					}
				}
				return this.isDebugEnabled.Value;
			}
		}

		// Token: 0x06000B90 RID: 2960 RVA: 0x0001B447 File Offset: 0x00019647
		public override void Debug(Exception exception, string messageFormat, params object[] args)
		{
			InternalLog4netLogger.logMethod.Invoke(this.internalLogger, new object[]
			{
				InternalLog4netLogger.loggerType,
				InternalLog4netLogger.debugLevelPropertyValue,
				new LogMessage(CultureInfo.InvariantCulture, messageFormat, args),
				exception
			});
		}

		// Token: 0x06000B91 RID: 2961 RVA: 0x0001B483 File Offset: 0x00019683
		public override void DebugFormat(string message, params object[] arguments)
		{
			MethodBase methodBase = InternalLog4netLogger.logMethod;
			object obj = this.internalLogger;
			object[] array = new object[4];
			array[0] = InternalLog4netLogger.loggerType;
			array[1] = InternalLog4netLogger.debugLevelPropertyValue;
			array[2] = new LogMessage(CultureInfo.InvariantCulture, message, arguments);
			methodBase.Invoke(obj, array);
		}

		// Token: 0x17000377 RID: 887
		// (get) Token: 0x06000B92 RID: 2962 RVA: 0x0001B4BC File Offset: 0x000196BC
		public override bool IsInfoEnabled
		{
			get
			{
				if (this.isInfoEnabled == null)
				{
					if (InternalLog4netLogger.loadState != InternalLog4netLogger.LoadState.Success || this.internalLogger == null || InternalLog4netLogger.loggerType == null || InternalLog4netLogger.systemStringFormatType == null || InternalLog4netLogger.infoLevelPropertyValue == null)
					{
						this.isInfoEnabled = new bool?(false);
					}
					else
					{
						this.isInfoEnabled = new bool?(Convert.ToBoolean(InternalLog4netLogger.isEnabledForMethod.Invoke(this.internalLogger, new object[]
						{
							InternalLog4netLogger.infoLevelPropertyValue
						}), CultureInfo.InvariantCulture));
					}
				}
				return this.isInfoEnabled.Value;
			}
		}

		// Token: 0x06000B93 RID: 2963 RVA: 0x0001B547 File Offset: 0x00019747
		public override void InfoFormat(string message, params object[] arguments)
		{
			MethodBase methodBase = InternalLog4netLogger.logMethod;
			object obj = this.internalLogger;
			object[] array = new object[4];
			array[0] = InternalLog4netLogger.loggerType;
			array[1] = InternalLog4netLogger.infoLevelPropertyValue;
			array[2] = new LogMessage(CultureInfo.InvariantCulture, message, arguments);
			methodBase.Invoke(obj, array);
		}

		// Token: 0x04000466 RID: 1126
		private static InternalLog4netLogger.LoadState loadState = InternalLog4netLogger.LoadState.Uninitialized;

		// Token: 0x04000467 RID: 1127
		private static readonly object LOCK = new object();

		// Token: 0x04000468 RID: 1128
		private static Type logMangerType;

		// Token: 0x04000469 RID: 1129
		private static ITypeInfo logMangerTypeInfo;

		// Token: 0x0400046A RID: 1130
		private static MethodInfo getLoggerWithTypeMethod;

		// Token: 0x0400046B RID: 1131
		private static Type logType;

		// Token: 0x0400046C RID: 1132
		private static ITypeInfo logTypeInfo;

		// Token: 0x0400046D RID: 1133
		private static MethodInfo logMethod;

		// Token: 0x0400046E RID: 1134
		private static Type levelType;

		// Token: 0x0400046F RID: 1135
		private static ITypeInfo levelTypeInfo;

		// Token: 0x04000470 RID: 1136
		private static object debugLevelPropertyValue;

		// Token: 0x04000471 RID: 1137
		private static object infoLevelPropertyValue;

		// Token: 0x04000472 RID: 1138
		private static object errorLevelPropertyValue;

		// Token: 0x04000473 RID: 1139
		private static MethodInfo isEnabledForMethod;

		// Token: 0x04000474 RID: 1140
		private static Type systemStringFormatType;

		// Token: 0x04000475 RID: 1141
		private static Type loggerType;

		// Token: 0x04000476 RID: 1142
		private object internalLogger;

		// Token: 0x04000477 RID: 1143
		private bool? isErrorEnabled;

		// Token: 0x04000478 RID: 1144
		private bool? isDebugEnabled;

		// Token: 0x04000479 RID: 1145
		private bool? isInfoEnabled;

		// Token: 0x020001ED RID: 493
		private enum LoadState
		{
			// Token: 0x040009F5 RID: 2549
			Uninitialized,
			// Token: 0x040009F6 RID: 2550
			Failed,
			// Token: 0x040009F7 RID: 2551
			Loading,
			// Token: 0x040009F8 RID: 2552
			Success
		}
	}
}
