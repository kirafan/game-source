﻿using System;
using UnityEngine;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200013D RID: 317
	internal class UnityDebugLogger : InternalLogger
	{
		// Token: 0x06000C61 RID: 3169 RVA: 0x0001AAD4 File Offset: 0x00018CD4
		public UnityDebugLogger(Type declaringType) : base(declaringType)
		{
		}

		// Token: 0x06000C62 RID: 3170 RVA: 0x00005805 File Offset: 0x00003A05
		public override void Flush()
		{
		}

		// Token: 0x06000C63 RID: 3171 RVA: 0x0001D6E8 File Offset: 0x0001B8E8
		public override void Error(Exception exception, string messageFormat, params object[] args)
		{
			if (exception != null)
			{
				UnityEngine.Debug.LogException(exception);
			}
			if (!string.IsNullOrEmpty(messageFormat))
			{
				UnityEngine.Debug.LogError(string.Format(messageFormat, args));
			}
		}

		// Token: 0x06000C64 RID: 3172 RVA: 0x0001D707 File Offset: 0x0001B907
		public override void Debug(Exception exception, string messageFormat, params object[] args)
		{
			if (exception != null)
			{
				UnityEngine.Debug.LogException(exception);
			}
			if (!string.IsNullOrEmpty(messageFormat))
			{
				UnityEngine.Debug.Log(string.Format(messageFormat, args));
			}
		}

		// Token: 0x06000C65 RID: 3173 RVA: 0x0001D726 File Offset: 0x0001B926
		public override void DebugFormat(string messageFormat, params object[] args)
		{
			if (!string.IsNullOrEmpty(messageFormat))
			{
				UnityEngine.Debug.Log(string.Format(messageFormat, args));
			}
		}

		// Token: 0x06000C66 RID: 3174 RVA: 0x0001D726 File Offset: 0x0001B926
		public override void InfoFormat(string message, params object[] arguments)
		{
			if (!string.IsNullOrEmpty(message))
			{
				UnityEngine.Debug.Log(string.Format(message, arguments));
			}
		}

		// Token: 0x170003B1 RID: 945
		// (get) Token: 0x06000C67 RID: 3175 RVA: 0x00011C11 File Offset: 0x0000FE11
		public override bool IsDebugEnabled
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170003B2 RID: 946
		// (get) Token: 0x06000C68 RID: 3176 RVA: 0x00011C11 File Offset: 0x0000FE11
		public override bool IsErrorEnabled
		{
			get
			{
				return true;
			}
		}

		// Token: 0x170003B3 RID: 947
		// (get) Token: 0x06000C69 RID: 3177 RVA: 0x00011C11 File Offset: 0x0000FE11
		public override bool IsInfoEnabled
		{
			get
			{
				return true;
			}
		}
	}
}
