﻿using System;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000123 RID: 291
	public interface ILogger
	{
		// Token: 0x06000B64 RID: 2916
		void InfoFormat(string messageFormat, params object[] args);

		// Token: 0x06000B65 RID: 2917
		void Debug(Exception exception, string messageFormat, params object[] args);

		// Token: 0x06000B66 RID: 2918
		void DebugFormat(string messageFormat, params object[] args);

		// Token: 0x06000B67 RID: 2919
		void Error(Exception exception, string messageFormat, params object[] args);

		// Token: 0x06000B68 RID: 2920
		void Flush();
	}
}
