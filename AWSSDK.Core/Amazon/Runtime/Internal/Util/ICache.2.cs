﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000136 RID: 310
	public interface ICache<TKey, TValue> : ICache
	{
		// Token: 0x06000C18 RID: 3096
		TValue GetValue(TKey key, Func<TKey, TValue> creator);

		// Token: 0x06000C19 RID: 3097
		TValue GetValue(TKey key, Func<TKey, TValue> creator, out bool isStaleItem);

		// Token: 0x06000C1A RID: 3098
		void Clear(TKey key);

		// Token: 0x170003A1 RID: 929
		// (get) Token: 0x06000C1B RID: 3099
		List<TKey> Keys { get; }

		// Token: 0x06000C1C RID: 3100
		TOut UseCache<TOut>(TKey key, Func<TOut> operation, Action onError, Predicate<Exception> shouldRetryForException);
	}
}
