﻿using System;
using System.IO;
using System.Security.Cryptography;
using ThirdParty.MD5;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200011E RID: 286
	public class HashingWrapper : IHashingWrapper, IDisposable
	{
		// Token: 0x06000B36 RID: 2870 RVA: 0x0001A695 File Offset: 0x00018895
		public HashingWrapper(string algorithmName)
		{
			if (string.IsNullOrEmpty(algorithmName))
			{
				throw new ArgumentNullException("algorithmName");
			}
			this.Init(algorithmName);
		}

		// Token: 0x06000B37 RID: 2871 RVA: 0x0001A6B7 File Offset: 0x000188B7
		public void AppendBlock(byte[] buffer)
		{
			this.AppendBlock(buffer, 0, buffer.Length);
		}

		// Token: 0x06000B38 RID: 2872 RVA: 0x0001A6C4 File Offset: 0x000188C4
		public byte[] AppendLastBlock(byte[] buffer)
		{
			return this.AppendLastBlock(buffer, 0, buffer.Length);
		}

		// Token: 0x06000B39 RID: 2873 RVA: 0x0001A6D1 File Offset: 0x000188D1
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000B3A RID: 2874 RVA: 0x0001A6E0 File Offset: 0x000188E0
		private void Init(string algorithmName)
		{
			if (string.Equals(HashingWrapper.MD5ManagedName, algorithmName, StringComparison.Ordinal))
			{
				this._algorithm = new MD5Managed();
				return;
			}
			throw new ArgumentOutOfRangeException(algorithmName, "Unsupported hashing algorithm");
		}

		// Token: 0x06000B3B RID: 2875 RVA: 0x0001A707 File Offset: 0x00018907
		public void Clear()
		{
			this._algorithm.Initialize();
		}

		// Token: 0x06000B3C RID: 2876 RVA: 0x0001A714 File Offset: 0x00018914
		public byte[] ComputeHash(byte[] buffer)
		{
			return this._algorithm.ComputeHash(buffer);
		}

		// Token: 0x06000B3D RID: 2877 RVA: 0x0001A722 File Offset: 0x00018922
		public byte[] ComputeHash(Stream stream)
		{
			return this._algorithm.ComputeHash(stream);
		}

		// Token: 0x06000B3E RID: 2878 RVA: 0x0001A730 File Offset: 0x00018930
		public void AppendBlock(byte[] buffer, int offset, int count)
		{
			this._algorithm.TransformBlock(buffer, offset, count, null, 0);
		}

		// Token: 0x06000B3F RID: 2879 RVA: 0x0001A743 File Offset: 0x00018943
		public byte[] AppendLastBlock(byte[] buffer, int offset, int count)
		{
			this._algorithm.TransformFinalBlock(buffer, offset, count);
			return this._algorithm.Hash;
		}

		// Token: 0x06000B40 RID: 2880 RVA: 0x0001A760 File Offset: 0x00018960
		protected virtual void Dispose(bool disposing)
		{
			IDisposable algorithm = this._algorithm;
			if (disposing && algorithm != null)
			{
				algorithm.Dispose();
				this._algorithm = null;
			}
		}

		// Token: 0x04000458 RID: 1112
		private static string MD5ManagedName = typeof(MD5Managed).FullName;

		// Token: 0x04000459 RID: 1113
		private HashAlgorithm _algorithm;
	}
}
