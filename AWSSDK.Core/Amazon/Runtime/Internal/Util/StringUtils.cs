﻿using System;
using System.Globalization;
using System.IO;
using System.Text;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000139 RID: 313
	public static class StringUtils
	{
		// Token: 0x06000C34 RID: 3124 RVA: 0x00011605 File Offset: 0x0000F805
		public static string FromString(string value)
		{
			return value;
		}

		// Token: 0x06000C35 RID: 3125 RVA: 0x0001D314 File Offset: 0x0001B514
		public static string FromString(ConstantClass value)
		{
			if (!(value == null))
			{
				return value.Intern().Value;
			}
			return "";
		}

		// Token: 0x06000C36 RID: 3126 RVA: 0x0001D330 File Offset: 0x0001B530
		public static string FromMemoryStream(MemoryStream value)
		{
			return Convert.ToBase64String(value.ToArray());
		}

		// Token: 0x06000C37 RID: 3127 RVA: 0x0001D33D File Offset: 0x0001B53D
		public static string FromInt(int value)
		{
			return value.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x06000C38 RID: 3128 RVA: 0x0001D34C File Offset: 0x0001B54C
		public static string FromInt(int? value)
		{
			if (value != null)
			{
				return value.Value.ToString(CultureInfo.InvariantCulture);
			}
			return null;
		}

		// Token: 0x06000C39 RID: 3129 RVA: 0x0001D378 File Offset: 0x0001B578
		public static string FromLong(long value)
		{
			return value.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x06000C3A RID: 3130 RVA: 0x0001D386 File Offset: 0x0001B586
		public static string FromBool(bool value)
		{
			if (!value)
			{
				return "false";
			}
			return "true";
		}

		// Token: 0x06000C3B RID: 3131 RVA: 0x0001D396 File Offset: 0x0001B596
		public static string FromDateTime(DateTime value)
		{
			return value.ToString("yyyy-MM-dd\\THH:mm:ss.fff\\Z", CultureInfo.InvariantCulture);
		}

		// Token: 0x06000C3C RID: 3132 RVA: 0x0001D3A9 File Offset: 0x0001B5A9
		public static string FromDouble(double value)
		{
			return value.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x06000C3D RID: 3133 RVA: 0x0001D3B7 File Offset: 0x0001B5B7
		public static string FromDecimal(decimal value)
		{
			return value.ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x06000C3E RID: 3134 RVA: 0x0001D3C5 File Offset: 0x0001B5C5
		public static long Utf8ByteLength(string value)
		{
			if (value == null)
			{
				return 0L;
			}
			return (long)StringUtils.UTF_8.GetByteCount(value);
		}

		// Token: 0x040004AB RID: 1195
		private static readonly Encoding UTF_8 = Encoding.UTF8;
	}
}
