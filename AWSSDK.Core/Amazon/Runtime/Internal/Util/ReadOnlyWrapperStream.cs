﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000132 RID: 306
	public class ReadOnlyWrapperStream : WrapperStream
	{
		// Token: 0x06000BF2 RID: 3058 RVA: 0x0001C86B File Offset: 0x0001AA6B
		public ReadOnlyWrapperStream(Stream baseStream) : base(baseStream)
		{
			if (!baseStream.CanRead)
			{
				throw new InvalidOperationException("Base stream must be readable");
			}
		}

		// Token: 0x06000BF3 RID: 3059 RVA: 0x00003EFE File Offset: 0x000020FE
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000BF4 RID: 3060 RVA: 0x00003EFE File Offset: 0x000020FE
		public override void EndWrite(IAsyncResult asyncResult)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000392 RID: 914
		// (get) Token: 0x06000BF5 RID: 3061 RVA: 0x00011C11 File Offset: 0x0000FE11
		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000393 RID: 915
		// (get) Token: 0x06000BF6 RID: 3062 RVA: 0x000129CA File Offset: 0x00010BCA
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000394 RID: 916
		// (get) Token: 0x06000BF7 RID: 3063 RVA: 0x000129CA File Offset: 0x00010BCA
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000BF8 RID: 3064 RVA: 0x00003EFE File Offset: 0x000020FE
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000BF9 RID: 3065 RVA: 0x00003EFE File Offset: 0x000020FE
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException();
		}

		// Token: 0x06000BFA RID: 3066 RVA: 0x00003EFE File Offset: 0x000020FE
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x17000395 RID: 917
		// (get) Token: 0x06000BFB RID: 3067 RVA: 0x00003EFE File Offset: 0x000020FE
		public override long Length
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000396 RID: 918
		// (get) Token: 0x06000BFC RID: 3068 RVA: 0x00003EFE File Offset: 0x000020FE
		// (set) Token: 0x06000BFD RID: 3069 RVA: 0x00003EFE File Offset: 0x000020FE
		public override long Position
		{
			get
			{
				throw new NotSupportedException();
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x06000BFE RID: 3070 RVA: 0x00003EFE File Offset: 0x000020FE
		public override void Flush()
		{
			throw new NotSupportedException();
		}
	}
}
