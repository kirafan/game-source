﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000130 RID: 304
	public class NonDisposingWrapperStream : WrapperStream
	{
		// Token: 0x06000BE5 RID: 3045 RVA: 0x0001C6F0 File Offset: 0x0001A8F0
		public NonDisposingWrapperStream(Stream baseStream) : base(baseStream)
		{
		}

		// Token: 0x06000BE6 RID: 3046 RVA: 0x00005805 File Offset: 0x00003A05
		public override void Close()
		{
		}

		// Token: 0x06000BE7 RID: 3047 RVA: 0x00005805 File Offset: 0x00003A05
		protected override void Dispose(bool disposing)
		{
		}
	}
}
