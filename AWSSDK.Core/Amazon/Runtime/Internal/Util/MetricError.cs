﻿using System;
using System.Globalization;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200012F RID: 303
	public class MetricError
	{
		// Token: 0x1700038B RID: 907
		// (get) Token: 0x06000BDB RID: 3035 RVA: 0x0001C624 File Offset: 0x0001A824
		// (set) Token: 0x06000BDC RID: 3036 RVA: 0x0001C62C File Offset: 0x0001A82C
		public Metric Metric { get; private set; }

		// Token: 0x1700038C RID: 908
		// (get) Token: 0x06000BDD RID: 3037 RVA: 0x0001C635 File Offset: 0x0001A835
		// (set) Token: 0x06000BDE RID: 3038 RVA: 0x0001C63D File Offset: 0x0001A83D
		public string Message { get; private set; }

		// Token: 0x1700038D RID: 909
		// (get) Token: 0x06000BDF RID: 3039 RVA: 0x0001C646 File Offset: 0x0001A846
		// (set) Token: 0x06000BE0 RID: 3040 RVA: 0x0001C64E File Offset: 0x0001A84E
		public Exception Exception { get; private set; }

		// Token: 0x1700038E RID: 910
		// (get) Token: 0x06000BE1 RID: 3041 RVA: 0x0001C657 File Offset: 0x0001A857
		// (set) Token: 0x06000BE2 RID: 3042 RVA: 0x0001C65F File Offset: 0x0001A85F
		public DateTime Time { get; private set; }

		// Token: 0x06000BE3 RID: 3043 RVA: 0x0001C668 File Offset: 0x0001A868
		public MetricError(Metric metric, string messageFormat, params object[] args) : this(metric, null, messageFormat, args)
		{
		}

		// Token: 0x06000BE4 RID: 3044 RVA: 0x0001C674 File Offset: 0x0001A874
		public MetricError(Metric metric, Exception exception, string messageFormat, params object[] args)
		{
			this.Time = AWSSDKUtils.CorrectedUtcNow.ToLocalTime();
			try
			{
				this.Message = string.Format(CultureInfo.InvariantCulture, messageFormat, args);
			}
			catch
			{
				this.Message = string.Format(CultureInfo.InvariantCulture, "Error message: {0}", new object[]
				{
					messageFormat
				});
			}
			this.Exception = exception;
			this.Metric = metric;
		}
	}
}
