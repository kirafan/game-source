﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000120 RID: 288
	public class HashStream<T> : HashStream where T : IHashingWrapper, new()
	{
		// Token: 0x06000B5A RID: 2906 RVA: 0x0001AA05 File Offset: 0x00018C05
		public HashStream(Stream baseStream, byte[] expectedHash, long expectedLength) : base(baseStream, expectedHash, expectedLength)
		{
			base.Algorithm = Activator.CreateInstance<T>();
		}
	}
}
