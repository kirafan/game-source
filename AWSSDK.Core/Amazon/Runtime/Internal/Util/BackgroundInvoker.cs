﻿using System;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000119 RID: 281
	internal class BackgroundInvoker : BackgroundDispatcher<Action>
	{
		// Token: 0x06000B13 RID: 2835 RVA: 0x0001A2D4 File Offset: 0x000184D4
		public BackgroundInvoker() : base(delegate(Action action)
		{
			action();
		})
		{
		}
	}
}
