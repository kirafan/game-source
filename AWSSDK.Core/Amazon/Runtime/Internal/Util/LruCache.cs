﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000129 RID: 297
	public class LruCache<TKey, TValue> where TKey : class where TValue : class
	{
		// Token: 0x1700037B RID: 891
		// (get) Token: 0x06000B9F RID: 2975 RVA: 0x0001B61D File Offset: 0x0001981D
		// (set) Token: 0x06000BA0 RID: 2976 RVA: 0x0001B625 File Offset: 0x00019825
		public int MaxEntries { get; private set; }

		// Token: 0x1700037C RID: 892
		// (get) Token: 0x06000BA1 RID: 2977 RVA: 0x0001B630 File Offset: 0x00019830
		public int Count
		{
			get
			{
				object obj = this.cacheLock;
				int count;
				lock (obj)
				{
					count = this.cache.Count;
				}
				return count;
			}
		}

		// Token: 0x06000BA2 RID: 2978 RVA: 0x0001B670 File Offset: 0x00019870
		public LruCache(int maxEntries)
		{
			if (maxEntries < 1)
			{
				throw new ArgumentException("maxEntries must be greater than zero.");
			}
			this.MaxEntries = maxEntries;
			this.cache = new Dictionary<TKey, LruListItem<TKey, TValue>>();
			this.lruList = new LruList<TKey, TValue>();
		}

		// Token: 0x06000BA3 RID: 2979 RVA: 0x0001B6B0 File Offset: 0x000198B0
		public void AddOrUpdate(TKey key, TValue value)
		{
			object obj = this.cacheLock;
			lock (obj)
			{
				LruListItem<TKey, TValue> lruListItem;
				if (this.cache.TryGetValue(key, out lruListItem))
				{
					lruListItem.Value = value;
					this.lruList.Touch(lruListItem);
				}
				else
				{
					LruListItem<TKey, TValue> lruListItem2 = new LruListItem<TKey, TValue>(key, value);
					while (this.cache.Count >= this.MaxEntries)
					{
						this.cache.Remove(this.lruList.EvictOldest());
					}
					this.lruList.Add(lruListItem2);
					this.cache.Add(key, lruListItem2);
				}
			}
		}

		// Token: 0x06000BA4 RID: 2980 RVA: 0x0001B758 File Offset: 0x00019958
		public bool TryGetValue(TKey key, out TValue value)
		{
			object obj = this.cacheLock;
			bool result;
			lock (obj)
			{
				LruListItem<TKey, TValue> lruListItem;
				if (this.cache.TryGetValue(key, out lruListItem))
				{
					this.lruList.Touch(lruListItem);
					value = lruListItem.Value;
					result = true;
				}
				else
				{
					value = default(TValue);
					result = false;
				}
			}
			return result;
		}

		// Token: 0x06000BA5 RID: 2981 RVA: 0x0001B7C4 File Offset: 0x000199C4
		public void Clear()
		{
			object obj = this.cacheLock;
			lock (obj)
			{
				this.cache.Clear();
				this.lruList.Clear();
			}
		}

		// Token: 0x0400047D RID: 1149
		private readonly object cacheLock = new object();

		// Token: 0x0400047E RID: 1150
		private Dictionary<TKey, LruListItem<TKey, TValue>> cache;

		// Token: 0x0400047F RID: 1151
		private LruList<TKey, TValue> lruList;
	}
}
