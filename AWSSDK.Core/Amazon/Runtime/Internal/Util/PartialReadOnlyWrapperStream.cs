﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000133 RID: 307
	public class PartialReadOnlyWrapperStream : ReadOnlyWrapperStream
	{
		// Token: 0x06000BFF RID: 3071 RVA: 0x0001C887 File Offset: 0x0001AA87
		public PartialReadOnlyWrapperStream(Stream baseStream, long size) : base(baseStream)
		{
			this._currentPosition = 0L;
			this._size = size;
		}

		// Token: 0x17000397 RID: 919
		// (get) Token: 0x06000C00 RID: 3072 RVA: 0x0001C89F File Offset: 0x0001AA9F
		private long RemainingSize
		{
			get
			{
				return this._size - this._currentPosition;
			}
		}

		// Token: 0x06000C01 RID: 3073 RVA: 0x0001C8B0 File Offset: 0x0001AAB0
		public override int Read(byte[] buffer, int offset, int count)
		{
			long num = Math.Min((long)count, this.RemainingSize);
			if (num <= 0L)
			{
				return 0;
			}
			num = Math.Min(num, 2147483647L);
			int num2 = base.Read(buffer, offset, (int)num);
			this._currentPosition += (long)num2;
			return num2;
		}

		// Token: 0x17000398 RID: 920
		// (get) Token: 0x06000C02 RID: 3074 RVA: 0x0001C8FA File Offset: 0x0001AAFA
		public override long Length
		{
			get
			{
				return this._size;
			}
		}

		// Token: 0x17000399 RID: 921
		// (get) Token: 0x06000C03 RID: 3075 RVA: 0x0001C902 File Offset: 0x0001AB02
		public override long Position
		{
			get
			{
				return this._currentPosition;
			}
		}

		// Token: 0x0400049B RID: 1179
		private long _currentPosition;

		// Token: 0x0400049C RID: 1180
		private long _size;
	}
}
