﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000137 RID: 311
	public static class SdkCache
	{
		// Token: 0x06000C1D RID: 3101 RVA: 0x0001CCFF File Offset: 0x0001AEFF
		public static void Clear()
		{
			SdkCache.cache.Clear();
		}

		// Token: 0x06000C1E RID: 3102 RVA: 0x0001CD0C File Offset: 0x0001AF0C
		public static void Clear(object cacheType)
		{
			object obj = SdkCache.cacheLock;
			lock (obj)
			{
				foreach (SdkCache.CacheKey cacheKey in SdkCache.cache.Keys)
				{
					if (AWSSDKUtils.AreEqual(cacheKey.CacheType, cacheType))
					{
						SdkCache.cache.GetValue(cacheKey, null).Clear();
					}
				}
			}
		}

		// Token: 0x06000C1F RID: 3103 RVA: 0x0001CD9C File Offset: 0x0001AF9C
		public static ICache<TKey, TValue> GetCache<TKey, TValue>(object client, object cacheIdentifier, IEqualityComparer<TKey> keyComparer)
		{
			return SdkCache.GetCache<TKey, TValue>(client as AmazonServiceClient, cacheIdentifier, keyComparer);
		}

		// Token: 0x06000C20 RID: 3104 RVA: 0x0001CDAC File Offset: 0x0001AFAC
		public static ICache<TKey, TValue> GetCache<TKey, TValue>(AmazonServiceClient client, object cacheIdentifier, IEqualityComparer<TKey> keyComparer)
		{
			SdkCache.CacheKey key;
			if (client == null)
			{
				key = SdkCache.CacheKey.Create(cacheIdentifier);
			}
			else
			{
				key = SdkCache.CacheKey.Create(client, cacheIdentifier);
			}
			ICache cache = null;
			object obj = SdkCache.cacheLock;
			lock (obj)
			{
				cache = SdkCache.cache.GetValue(key, (SdkCache.CacheKey k) => new Cache<TKey, TValue>(keyComparer));
			}
			Cache<TKey, TValue> cache2 = cache as Cache<TKey, TValue>;
			if (cache != null && cache2 == null)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Unable to cast cache of type {0} as cache of type {1}", new object[]
				{
					cache.GetType().FullName,
					typeof(Cache<TKey, TValue>).FullName
				}));
			}
			return cache2;
		}

		// Token: 0x040004A2 RID: 1186
		private static object cacheLock = new object();

		// Token: 0x040004A3 RID: 1187
		private static Cache<SdkCache.CacheKey, ICache> cache = new Cache<SdkCache.CacheKey, ICache>(null);

		// Token: 0x020001EF RID: 495
		internal class CacheKey
		{
			// Token: 0x17000452 RID: 1106
			// (get) Token: 0x06000FB2 RID: 4018 RVA: 0x0002811D File Offset: 0x0002631D
			// (set) Token: 0x06000FB3 RID: 4019 RVA: 0x00028125 File Offset: 0x00026325
			public ImmutableCredentials ImmutableCredentials { get; private set; }

			// Token: 0x17000453 RID: 1107
			// (get) Token: 0x06000FB4 RID: 4020 RVA: 0x0002812E File Offset: 0x0002632E
			// (set) Token: 0x06000FB5 RID: 4021 RVA: 0x00028136 File Offset: 0x00026336
			public RegionEndpoint RegionEndpoint { get; private set; }

			// Token: 0x17000454 RID: 1108
			// (get) Token: 0x06000FB6 RID: 4022 RVA: 0x0002813F File Offset: 0x0002633F
			// (set) Token: 0x06000FB7 RID: 4023 RVA: 0x00028147 File Offset: 0x00026347
			public string ServiceUrl { get; private set; }

			// Token: 0x17000455 RID: 1109
			// (get) Token: 0x06000FB8 RID: 4024 RVA: 0x00028150 File Offset: 0x00026350
			// (set) Token: 0x06000FB9 RID: 4025 RVA: 0x00028158 File Offset: 0x00026358
			public object CacheType { get; private set; }

			// Token: 0x06000FBA RID: 4026 RVA: 0x00028161 File Offset: 0x00026361
			private CacheKey()
			{
				this.ImmutableCredentials = null;
				this.RegionEndpoint = null;
				this.ServiceUrl = null;
				this.CacheType = null;
			}

			// Token: 0x06000FBB RID: 4027 RVA: 0x00028188 File Offset: 0x00026388
			public static SdkCache.CacheKey Create(AmazonServiceClient client, object cacheType)
			{
				if (client == null)
				{
					throw new ArgumentNullException("client");
				}
				SdkCache.CacheKey cacheKey = new SdkCache.CacheKey();
				AWSCredentials credentials = client.Credentials;
				cacheKey.ImmutableCredentials = ((credentials == null) ? null : credentials.GetCredentials());
				cacheKey.RegionEndpoint = client.Config.RegionEndpoint;
				cacheKey.ServiceUrl = client.Config.ServiceURL;
				cacheKey.CacheType = cacheType;
				return cacheKey;
			}

			// Token: 0x06000FBC RID: 4028 RVA: 0x000281EA File Offset: 0x000263EA
			public static SdkCache.CacheKey Create(object cacheType)
			{
				return new SdkCache.CacheKey
				{
					CacheType = cacheType
				};
			}

			// Token: 0x06000FBD RID: 4029 RVA: 0x000281F8 File Offset: 0x000263F8
			public override int GetHashCode()
			{
				return Hashing.Hash(new object[]
				{
					this.ImmutableCredentials,
					this.RegionEndpoint,
					this.ServiceUrl,
					this.CacheType
				});
			}

			// Token: 0x06000FBE RID: 4030 RVA: 0x0002822C File Offset: 0x0002642C
			public override bool Equals(object obj)
			{
				if (this == obj)
				{
					return true;
				}
				SdkCache.CacheKey cacheKey = obj as SdkCache.CacheKey;
				return cacheKey != null && AWSSDKUtils.AreEqual(new object[]
				{
					this.ImmutableCredentials,
					this.RegionEndpoint,
					this.ServiceUrl,
					this.CacheType
				}, new object[]
				{
					cacheKey.ImmutableCredentials,
					cacheKey.RegionEndpoint,
					cacheKey.ServiceUrl,
					cacheKey.CacheType
				});
			}
		}
	}
}
