﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200012C RID: 300
	public class RequestMetrics : IRequestMetrics
	{
		// Token: 0x17000383 RID: 899
		// (get) Token: 0x06000BB9 RID: 3001 RVA: 0x0001B9E0 File Offset: 0x00019BE0
		private long CurrentTime
		{
			get
			{
				return this.stopWatch.GetElapsedDateTimeTicks();
			}
		}

		// Token: 0x06000BBA RID: 3002 RVA: 0x0001B9ED File Offset: 0x00019BED
		private void LogError_Locked(Metric metric, string messageFormat, params object[] args)
		{
			this.errors.Add(new MetricError(metric, messageFormat, args));
		}

		// Token: 0x06000BBB RID: 3003 RVA: 0x0001BA02 File Offset: 0x00019C02
		private static void Log(StringBuilder builder, Metric metric, object metricValue)
		{
			RequestMetrics.LogHelper(builder, metric, new object[]
			{
				metricValue
			});
		}

		// Token: 0x06000BBC RID: 3004 RVA: 0x0001BA15 File Offset: 0x00019C15
		private static void Log(StringBuilder builder, Metric metric, List<object> metricValues)
		{
			if (metricValues == null || metricValues.Count == 0)
			{
				RequestMetrics.LogHelper(builder, metric, new object[0]);
				return;
			}
			RequestMetrics.LogHelper(builder, metric, metricValues.ToArray());
		}

		// Token: 0x06000BBD RID: 3005 RVA: 0x0001BA40 File Offset: 0x00019C40
		private static void LogHelper(StringBuilder builder, Metric metric, params object[] metricValues)
		{
			builder.AppendFormat(CultureInfo.InvariantCulture, "{0} = ", new object[]
			{
				metric
			});
			if (metricValues == null)
			{
				builder.Append(RequestMetrics.ObjectToString(metricValues));
			}
			else
			{
				for (int i = 0; i < metricValues.Length; i++)
				{
					string value = RequestMetrics.ObjectToString(metricValues[i]);
					if (i > 0)
					{
						builder.Append(", ");
					}
					builder.Append(value);
				}
			}
			builder.Append("; ");
		}

		// Token: 0x06000BBE RID: 3006 RVA: 0x0001BABA File Offset: 0x00019CBA
		private static string ObjectToString(object data)
		{
			if (data == null)
			{
				return "NULL";
			}
			return data.ToString();
		}

		// Token: 0x17000384 RID: 900
		// (get) Token: 0x06000BBF RID: 3007 RVA: 0x0001BACB File Offset: 0x00019CCB
		// (set) Token: 0x06000BC0 RID: 3008 RVA: 0x0001BAD3 File Offset: 0x00019CD3
		public Dictionary<Metric, List<object>> Properties { get; set; }

		// Token: 0x17000385 RID: 901
		// (get) Token: 0x06000BC1 RID: 3009 RVA: 0x0001BADC File Offset: 0x00019CDC
		// (set) Token: 0x06000BC2 RID: 3010 RVA: 0x0001BAE4 File Offset: 0x00019CE4
		public Dictionary<Metric, List<IMetricsTiming>> Timings { get; set; }

		// Token: 0x17000386 RID: 902
		// (get) Token: 0x06000BC3 RID: 3011 RVA: 0x0001BAED File Offset: 0x00019CED
		// (set) Token: 0x06000BC4 RID: 3012 RVA: 0x0001BAF5 File Offset: 0x00019CF5
		public Dictionary<Metric, long> Counters { get; set; }

		// Token: 0x17000387 RID: 903
		// (get) Token: 0x06000BC5 RID: 3013 RVA: 0x0001BAFE File Offset: 0x00019CFE
		// (set) Token: 0x06000BC6 RID: 3014 RVA: 0x0001BB06 File Offset: 0x00019D06
		public bool IsEnabled { get; internal set; }

		// Token: 0x06000BC7 RID: 3015 RVA: 0x0001BB10 File Offset: 0x00019D10
		public RequestMetrics()
		{
			this.stopWatch = Stopwatch.StartNew();
			this.Properties = new Dictionary<Metric, List<object>>();
			this.Timings = new Dictionary<Metric, List<IMetricsTiming>>();
			this.Counters = new Dictionary<Metric, long>();
			this.inFlightTimings = new Dictionary<Metric, Timing>();
			this.IsEnabled = false;
		}

		// Token: 0x06000BC8 RID: 3016 RVA: 0x0001BB78 File Offset: 0x00019D78
		public TimingEvent StartEvent(Metric metric)
		{
			object obj = this.metricsLock;
			lock (obj)
			{
				if (this.inFlightTimings.ContainsKey(metric))
				{
					this.LogError_Locked(metric, "Starting multiple events for the same metric", new object[0]);
				}
				this.inFlightTimings[metric] = new Timing(this.CurrentTime);
			}
			return new TimingEvent(this, metric);
		}

		// Token: 0x06000BC9 RID: 3017 RVA: 0x0001BBEC File Offset: 0x00019DEC
		public Timing StopEvent(Metric metric)
		{
			object obj = this.metricsLock;
			Timing timing;
			lock (obj)
			{
				if (!this.inFlightTimings.TryGetValue(metric, out timing))
				{
					this.LogError_Locked(metric, "Trying to stop event that has not been started", new object[0]);
					return new Timing();
				}
				this.inFlightTimings.Remove(metric);
				timing.Stop(this.CurrentTime);
				if (this.IsEnabled)
				{
					List<IMetricsTiming> list;
					if (!this.Timings.TryGetValue(metric, out list))
					{
						list = new List<IMetricsTiming>();
						this.Timings[metric] = list;
					}
					list.Add(timing);
				}
			}
			return timing;
		}

		// Token: 0x06000BCA RID: 3018 RVA: 0x0001BC98 File Offset: 0x00019E98
		public void AddProperty(Metric metric, object property)
		{
			if (!this.IsEnabled)
			{
				return;
			}
			object obj = this.metricsLock;
			lock (obj)
			{
				List<object> list;
				if (!this.Properties.TryGetValue(metric, out list))
				{
					list = new List<object>();
					this.Properties[metric] = list;
				}
				list.Add(property);
			}
		}

		// Token: 0x06000BCB RID: 3019 RVA: 0x0001BD00 File Offset: 0x00019F00
		public void SetCounter(Metric metric, long value)
		{
			if (!this.IsEnabled)
			{
				return;
			}
			object obj = this.metricsLock;
			lock (obj)
			{
				this.Counters[metric] = value;
			}
		}

		// Token: 0x06000BCC RID: 3020 RVA: 0x0001BD4C File Offset: 0x00019F4C
		public void IncrementCounter(Metric metric)
		{
			if (!this.IsEnabled)
			{
				return;
			}
			object obj = this.metricsLock;
			lock (obj)
			{
				long num;
				if (!this.Counters.TryGetValue(metric, out num))
				{
					num = 1L;
				}
				else
				{
					num += 1L;
				}
				this.Counters[metric] = num;
			}
		}

		// Token: 0x06000BCD RID: 3021 RVA: 0x0001BDB0 File Offset: 0x00019FB0
		public string GetErrors()
		{
			string result;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				object obj = this.metricsLock;
				lock (obj)
				{
					if (this.inFlightTimings.Count > 0)
					{
						string arg = string.Join(", ", (from k in this.inFlightTimings.Keys
						select k.ToString()).ToArray<string>());
						stringWriter.Write("Timings are still in flight: {0}.", arg);
					}
					if (this.errors.Count > 0)
					{
						stringWriter.Write("Logged {0} metrics errors: ", this.errors.Count);
						foreach (MetricError metricError in this.errors)
						{
							if (metricError.Exception != null || !string.IsNullOrEmpty(metricError.Message))
							{
								stringWriter.Write("{0} - {1} - ", metricError.Time.ToString("yyyy-MM-dd\\THH:mm:ss.fff\\Z", CultureInfo.InvariantCulture), metricError.Metric);
								if (!string.IsNullOrEmpty(metricError.Message))
								{
									stringWriter.Write(metricError.Message);
									stringWriter.Write(";");
								}
								if (metricError.Exception != null)
								{
									stringWriter.Write(metricError.Exception);
									stringWriter.Write("; ");
								}
							}
						}
					}
				}
				result = stringWriter.ToString();
			}
			return result;
		}

		// Token: 0x06000BCE RID: 3022 RVA: 0x0001BF8C File Offset: 0x0001A18C
		public override string ToString()
		{
			if (!this.IsEnabled)
			{
				return "Metrics logging disabled";
			}
			StringBuilder stringBuilder = new StringBuilder();
			object obj;
			if (AWSConfigs.LoggingConfig.LogMetricsCustomFormatter != null)
			{
				try
				{
					obj = this.metricsLock;
					lock (obj)
					{
						stringBuilder.Append(AWSConfigs.LoggingConfig.LogMetricsCustomFormatter.FormatMetrics(this));
					}
					return stringBuilder.ToString();
				}
				catch (Exception ex)
				{
					stringBuilder.Append("[Custom metrics formatter failed: ").Append(ex.Message).Append("] ");
				}
			}
			if (AWSConfigs.LoggingConfig.LogMetricsFormat == LogMetricsFormatOption.JSON)
			{
				obj = this.metricsLock;
				lock (obj)
				{
					stringBuilder.Append(this.ToJSON());
				}
				return stringBuilder.ToString();
			}
			obj = this.metricsLock;
			lock (obj)
			{
				foreach (KeyValuePair<Metric, List<object>> keyValuePair in this.Properties)
				{
					Metric key = keyValuePair.Key;
					List<object> value = keyValuePair.Value;
					RequestMetrics.Log(stringBuilder, key, value);
				}
				foreach (KeyValuePair<Metric, List<IMetricsTiming>> keyValuePair2 in this.Timings)
				{
					Metric key2 = keyValuePair2.Key;
					foreach (IMetricsTiming metricsTiming in keyValuePair2.Value)
					{
						if (metricsTiming.IsFinished)
						{
							RequestMetrics.Log(stringBuilder, key2, metricsTiming.ElapsedTime);
						}
					}
				}
				foreach (KeyValuePair<Metric, long> keyValuePair3 in this.Counters)
				{
					Metric key3 = keyValuePair3.Key;
					long value2 = keyValuePair3.Value;
					RequestMetrics.Log(stringBuilder, key3, value2);
				}
			}
			stringBuilder.Replace("\r", "\\r").Replace("\n", "\\n");
			return stringBuilder.ToString();
		}

		// Token: 0x06000BCF RID: 3023 RVA: 0x0001C278 File Offset: 0x0001A478
		public string ToJSON()
		{
			if (!this.IsEnabled)
			{
				return "{ }";
			}
			StringBuilder stringBuilder = new StringBuilder();
			JsonWriter jsonWriter = new JsonWriter(stringBuilder);
			jsonWriter.WriteObjectStart();
			jsonWriter.WritePropertyName("properties");
			jsonWriter.WriteObjectStart();
			foreach (KeyValuePair<Metric, List<object>> keyValuePair in this.Properties)
			{
				jsonWriter.WritePropertyName(keyValuePair.Key.ToString());
				List<object> value = keyValuePair.Value;
				if (value.Count > 1)
				{
					jsonWriter.WriteArrayStart();
				}
				foreach (object obj in value)
				{
					if (obj == null)
					{
						jsonWriter.Write(null);
					}
					else
					{
						jsonWriter.Write(obj.ToString());
					}
				}
				if (value.Count > 1)
				{
					jsonWriter.WriteArrayEnd();
				}
			}
			jsonWriter.WriteObjectEnd();
			jsonWriter.WritePropertyName("timings");
			jsonWriter.WriteObjectStart();
			foreach (KeyValuePair<Metric, List<IMetricsTiming>> keyValuePair2 in this.Timings)
			{
				jsonWriter.WritePropertyName(keyValuePair2.Key.ToString());
				List<IMetricsTiming> value2 = keyValuePair2.Value;
				if (value2.Count > 1)
				{
					jsonWriter.WriteArrayStart();
				}
				foreach (IMetricsTiming metricsTiming in keyValuePair2.Value)
				{
					if (metricsTiming.IsFinished)
					{
						jsonWriter.Write(metricsTiming.ElapsedTime.TotalMilliseconds);
					}
				}
				if (value2.Count > 1)
				{
					jsonWriter.WriteArrayEnd();
				}
			}
			jsonWriter.WriteObjectEnd();
			jsonWriter.WritePropertyName("counters");
			jsonWriter.WriteObjectStart();
			foreach (KeyValuePair<Metric, long> keyValuePair3 in this.Counters)
			{
				jsonWriter.WritePropertyName(keyValuePair3.Key.ToString());
				jsonWriter.Write(keyValuePair3.Value);
			}
			jsonWriter.WriteObjectEnd();
			jsonWriter.WriteObjectEnd();
			return stringBuilder.ToString();
		}

		// Token: 0x04000487 RID: 1159
		private object metricsLock = new object();

		// Token: 0x04000488 RID: 1160
		private Stopwatch stopWatch;

		// Token: 0x04000489 RID: 1161
		private Dictionary<Metric, Timing> inFlightTimings;

		// Token: 0x0400048A RID: 1162
		private List<MetricError> errors = new List<MetricError>();
	}
}
