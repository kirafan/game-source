﻿using System;
using System.Collections.Generic;
using System.ComponentModel;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000125 RID: 293
	public class Logger : ILogger
	{
		// Token: 0x06000B70 RID: 2928 RVA: 0x0001AC06 File Offset: 0x00018E06
		private Logger()
		{
			this.loggers = new List<InternalLogger>();
		}

		// Token: 0x06000B71 RID: 2929 RVA: 0x0001AC1C File Offset: 0x00018E1C
		private Logger(Type type)
		{
			this.loggers = new List<InternalLogger>();
			InternalLog4netLogger item = new InternalLog4netLogger(type);
			this.loggers.Add(item);
			this.loggers.Add(new InternalConsoleLogger(type));
			UnityDebugLogger item2 = new UnityDebugLogger(type);
			this.loggers.Add(item2);
			this.ConfigureLoggers();
			AWSConfigs.PropertyChanged += this.ConfigsChanged;
		}

		// Token: 0x06000B72 RID: 2930 RVA: 0x0001AC88 File Offset: 0x00018E88
		private void ConfigsChanged(object sender, PropertyChangedEventArgs e)
		{
			if (e != null && string.Equals(e.PropertyName, "LogTo", StringComparison.Ordinal))
			{
				this.ConfigureLoggers();
			}
		}

		// Token: 0x06000B73 RID: 2931 RVA: 0x0001ACA8 File Offset: 0x00018EA8
		private void ConfigureLoggers()
		{
			LoggingOptions logTo = AWSConfigs.LoggingConfig.LogTo;
			foreach (InternalLogger internalLogger in this.loggers)
			{
				if (internalLogger is InternalLog4netLogger)
				{
					internalLogger.IsEnabled = ((logTo & LoggingOptions.Log4Net) == LoggingOptions.Log4Net);
				}
				if (internalLogger is InternalConsoleLogger)
				{
					internalLogger.IsEnabled = ((logTo & LoggingOptions.Console) == LoggingOptions.Console);
				}
				if (internalLogger is UnityDebugLogger)
				{
					internalLogger.IsEnabled = ((logTo & LoggingOptions.UnityLogger) == LoggingOptions.UnityLogger);
				}
			}
		}

		// Token: 0x06000B74 RID: 2932 RVA: 0x0001AD40 File Offset: 0x00018F40
		public static Logger GetLogger(Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}
			IDictionary<Type, Logger> obj = Logger.cachedLoggers;
			Logger logger;
			lock (obj)
			{
				if (!Logger.cachedLoggers.TryGetValue(type, out logger))
				{
					logger = new Logger(type);
					Logger.cachedLoggers[type] = logger;
				}
			}
			return logger;
		}

		// Token: 0x06000B75 RID: 2933 RVA: 0x0001ADA4 File Offset: 0x00018FA4
		public static void ClearLoggerCache()
		{
			IDictionary<Type, Logger> obj = Logger.cachedLoggers;
			lock (obj)
			{
				Logger.cachedLoggers = new Dictionary<Type, Logger>();
			}
		}

		// Token: 0x1700036F RID: 879
		// (get) Token: 0x06000B76 RID: 2934 RVA: 0x0001ADE0 File Offset: 0x00018FE0
		public static Logger EmptyLogger
		{
			get
			{
				return Logger.emptyLogger;
			}
		}

		// Token: 0x06000B77 RID: 2935 RVA: 0x0001ADE8 File Offset: 0x00018FE8
		public void Flush()
		{
			foreach (InternalLogger internalLogger in this.loggers)
			{
				internalLogger.Flush();
			}
		}

		// Token: 0x06000B78 RID: 2936 RVA: 0x0001AE38 File Offset: 0x00019038
		public void Error(Exception exception, string messageFormat, params object[] args)
		{
			foreach (InternalLogger internalLogger in this.loggers)
			{
				if (internalLogger.IsEnabled && internalLogger.IsErrorEnabled)
				{
					internalLogger.Error(exception, messageFormat, args);
				}
			}
		}

		// Token: 0x06000B79 RID: 2937 RVA: 0x0001AEA0 File Offset: 0x000190A0
		public void Debug(Exception exception, string messageFormat, params object[] args)
		{
			foreach (InternalLogger internalLogger in this.loggers)
			{
				if (internalLogger.IsEnabled && internalLogger.IsDebugEnabled)
				{
					internalLogger.Debug(exception, messageFormat, args);
				}
			}
		}

		// Token: 0x06000B7A RID: 2938 RVA: 0x0001AF08 File Offset: 0x00019108
		public void DebugFormat(string messageFormat, params object[] args)
		{
			foreach (InternalLogger internalLogger in this.loggers)
			{
				if (internalLogger.IsEnabled && internalLogger.IsDebugEnabled)
				{
					internalLogger.DebugFormat(messageFormat, args);
				}
			}
		}

		// Token: 0x06000B7B RID: 2939 RVA: 0x0001AF6C File Offset: 0x0001916C
		public void InfoFormat(string messageFormat, params object[] args)
		{
			foreach (InternalLogger internalLogger in this.loggers)
			{
				if (internalLogger.IsEnabled && internalLogger.IsInfoEnabled)
				{
					internalLogger.InfoFormat(messageFormat, args);
				}
			}
		}

		// Token: 0x04000461 RID: 1121
		private static IDictionary<Type, Logger> cachedLoggers = new Dictionary<Type, Logger>();

		// Token: 0x04000462 RID: 1122
		private List<InternalLogger> loggers;

		// Token: 0x04000463 RID: 1123
		private static Logger emptyLogger = new Logger();
	}
}
