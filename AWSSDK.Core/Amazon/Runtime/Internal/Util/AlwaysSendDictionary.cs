﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000117 RID: 279
	internal class AlwaysSendDictionary<TKey, TValue> : Dictionary<TKey, TValue>
	{
		// Token: 0x06000B06 RID: 2822 RVA: 0x0001A086 File Offset: 0x00018286
		public AlwaysSendDictionary()
		{
		}

		// Token: 0x06000B07 RID: 2823 RVA: 0x0001A08E File Offset: 0x0001828E
		public AlwaysSendDictionary(IEqualityComparer<TKey> comparer) : base(comparer)
		{
		}

		// Token: 0x06000B08 RID: 2824 RVA: 0x0001A097 File Offset: 0x00018297
		public AlwaysSendDictionary(IDictionary<TKey, TValue> dictionary) : base(dictionary ?? new Dictionary<TKey, TValue>())
		{
		}
	}
}
