﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200011F RID: 287
	public abstract class HashStream : WrapperStream
	{
		// Token: 0x17000366 RID: 870
		// (get) Token: 0x06000B42 RID: 2882 RVA: 0x0001A79D File Offset: 0x0001899D
		// (set) Token: 0x06000B43 RID: 2883 RVA: 0x0001A7A5 File Offset: 0x000189A5
		protected IHashingWrapper Algorithm { get; set; }

		// Token: 0x17000367 RID: 871
		// (get) Token: 0x06000B44 RID: 2884 RVA: 0x0001A7AE File Offset: 0x000189AE
		protected bool FinishedHashing
		{
			get
			{
				return this.CalculatedHash != null;
			}
		}

		// Token: 0x17000368 RID: 872
		// (get) Token: 0x06000B45 RID: 2885 RVA: 0x0001A7B9 File Offset: 0x000189B9
		// (set) Token: 0x06000B46 RID: 2886 RVA: 0x0001A7C1 File Offset: 0x000189C1
		private protected long CurrentPosition { protected get; private set; }

		// Token: 0x17000369 RID: 873
		// (get) Token: 0x06000B47 RID: 2887 RVA: 0x0001A7CA File Offset: 0x000189CA
		// (set) Token: 0x06000B48 RID: 2888 RVA: 0x0001A7D2 File Offset: 0x000189D2
		public byte[] CalculatedHash { get; protected set; }

		// Token: 0x1700036A RID: 874
		// (get) Token: 0x06000B49 RID: 2889 RVA: 0x0001A7DB File Offset: 0x000189DB
		// (set) Token: 0x06000B4A RID: 2890 RVA: 0x0001A7E3 File Offset: 0x000189E3
		public byte[] ExpectedHash { get; private set; }

		// Token: 0x1700036B RID: 875
		// (get) Token: 0x06000B4B RID: 2891 RVA: 0x0001A7EC File Offset: 0x000189EC
		// (set) Token: 0x06000B4C RID: 2892 RVA: 0x0001A7F4 File Offset: 0x000189F4
		public long ExpectedLength { get; protected set; }

		// Token: 0x06000B4D RID: 2893 RVA: 0x0001A7FD File Offset: 0x000189FD
		protected HashStream(Stream baseStream, byte[] expectedHash, long expectedLength) : base(baseStream)
		{
			this.ExpectedHash = expectedHash;
			this.ExpectedLength = expectedLength;
			this.ValidateBaseStream();
			this.Reset();
		}

		// Token: 0x06000B4E RID: 2894 RVA: 0x0001A820 File Offset: 0x00018A20
		public override int Read(byte[] buffer, int offset, int count)
		{
			int num = base.Read(buffer, offset, count);
			this.CurrentPosition += (long)num;
			if (!this.FinishedHashing)
			{
				this.Algorithm.AppendBlock(buffer, offset, num);
			}
			return num;
		}

		// Token: 0x06000B4F RID: 2895 RVA: 0x0001A85D File Offset: 0x00018A5D
		public override void Close()
		{
			this.CalculateHash();
			base.Close();
		}

		// Token: 0x06000B50 RID: 2896 RVA: 0x0001A86C File Offset: 0x00018A6C
		protected override void Dispose(bool disposing)
		{
			try
			{
				this.CalculateHash();
				if (disposing && this.Algorithm != null)
				{
					this.Algorithm.Dispose();
					this.Algorithm = null;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		// Token: 0x1700036C RID: 876
		// (get) Token: 0x06000B51 RID: 2897 RVA: 0x000129CA File Offset: 0x00010BCA
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700036D RID: 877
		// (get) Token: 0x06000B52 RID: 2898 RVA: 0x0001A8B8 File Offset: 0x00018AB8
		// (set) Token: 0x06000B53 RID: 2899 RVA: 0x0001A8B8 File Offset: 0x00018AB8
		public override long Position
		{
			get
			{
				throw new NotSupportedException("HashStream does not support seeking");
			}
			set
			{
				throw new NotSupportedException("HashStream does not support seeking");
			}
		}

		// Token: 0x06000B54 RID: 2900 RVA: 0x0001A8B8 File Offset: 0x00018AB8
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException("HashStream does not support seeking");
		}

		// Token: 0x1700036E RID: 878
		// (get) Token: 0x06000B55 RID: 2901 RVA: 0x0001A8C4 File Offset: 0x00018AC4
		public override long Length
		{
			get
			{
				return this.ExpectedLength;
			}
		}

		// Token: 0x06000B56 RID: 2902 RVA: 0x0001A8CC File Offset: 0x00018ACC
		public virtual void CalculateHash()
		{
			if (!this.FinishedHashing)
			{
				if (this.ExpectedLength < 0L || this.CurrentPosition == this.ExpectedLength)
				{
					this.CalculatedHash = this.Algorithm.AppendLastBlock(new byte[0]);
				}
				else
				{
					this.CalculatedHash = new byte[0];
				}
				if (this.CalculatedHash.Length != 0 && this.ExpectedHash != null && this.ExpectedHash.Length != 0 && !HashStream.CompareHashes(this.ExpectedHash, this.CalculatedHash))
				{
					throw new AmazonClientException("Expected hash not equal to calculated hash");
				}
			}
		}

		// Token: 0x06000B57 RID: 2903 RVA: 0x0001A958 File Offset: 0x00018B58
		public void Reset()
		{
			this.CurrentPosition = 0L;
			this.CalculatedHash = null;
			if (this.Algorithm != null)
			{
				this.Algorithm.Clear();
			}
			HashStream hashStream = base.BaseStream as HashStream;
			if (hashStream != null)
			{
				hashStream.Reset();
			}
		}

		// Token: 0x06000B58 RID: 2904 RVA: 0x0001A99C File Offset: 0x00018B9C
		private void ValidateBaseStream()
		{
			if (!base.BaseStream.CanRead && !base.BaseStream.CanWrite)
			{
				throw new System.IO.InvalidDataException("HashStream does not support base streams that are not capable of reading or writing");
			}
		}

		// Token: 0x06000B59 RID: 2905 RVA: 0x0001A9C4 File Offset: 0x00018BC4
		protected static bool CompareHashes(byte[] expected, byte[] actual)
		{
			if (expected == actual)
			{
				return true;
			}
			if (expected == null || actual == null)
			{
				return expected == actual;
			}
			if (expected.Length != actual.Length)
			{
				return false;
			}
			for (int i = 0; i < expected.Length; i++)
			{
				if (expected[i] != actual[i])
				{
					return false;
				}
			}
			return true;
		}
	}
}
