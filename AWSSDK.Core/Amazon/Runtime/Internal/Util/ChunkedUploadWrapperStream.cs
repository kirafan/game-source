﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime.Internal.Auth;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000115 RID: 277
	public class ChunkedUploadWrapperStream : WrapperStream
	{
		// Token: 0x06000AF7 RID: 2807 RVA: 0x00019BD0 File Offset: 0x00017DD0
		internal ChunkedUploadWrapperStream(Stream stream, int wrappedStreamBufferSize, AWS4SigningResult headerSigningResult) : base(stream)
		{
			this.HeaderSigningResult = headerSigningResult;
			this.PreviousChunkSignature = headerSigningResult.Signature;
			this._wrappedStreamBufferSize = wrappedStreamBufferSize;
			this._inputBuffer = new byte[ChunkedUploadWrapperStream.DefaultChunkSize];
			this._outputBuffer = new byte[ChunkedUploadWrapperStream.CalculateChunkHeaderLength((long)ChunkedUploadWrapperStream.DefaultChunkSize)];
		}

		// Token: 0x06000AF8 RID: 2808 RVA: 0x00019C34 File Offset: 0x00017E34
		public override int Read(byte[] buffer, int offset, int count)
		{
			if (this._outputBufferPos == -1)
			{
				if (this._wrappedStreamConsumed && this._outputBufferIsTerminatingChunk)
				{
					return 0;
				}
				int num = this.FillInputBuffer();
				this.ConstructOutputBufferChunk(num);
				this._outputBufferIsTerminatingChunk = (this._wrappedStreamConsumed && num == 0);
			}
			int num2 = this._outputBufferDataLen - this._outputBufferPos;
			if (num2 < count)
			{
				count = num2;
			}
			Buffer.BlockCopy(this._outputBuffer, this._outputBufferPos, buffer, offset, count);
			this._outputBufferPos += count;
			if (this._outputBufferPos >= this._outputBufferDataLen)
			{
				this._outputBufferPos = -1;
			}
			return count;
		}

		// Token: 0x17000359 RID: 857
		// (get) Token: 0x06000AF9 RID: 2809 RVA: 0x00019CCC File Offset: 0x00017ECC
		// (set) Token: 0x06000AFA RID: 2810 RVA: 0x00019CD4 File Offset: 0x00017ED4
		private AWS4SigningResult HeaderSigningResult { get; set; }

		// Token: 0x1700035A RID: 858
		// (get) Token: 0x06000AFB RID: 2811 RVA: 0x00019CDD File Offset: 0x00017EDD
		// (set) Token: 0x06000AFC RID: 2812 RVA: 0x00019CE5 File Offset: 0x00017EE5
		private string PreviousChunkSignature { get; set; }

		// Token: 0x06000AFD RID: 2813 RVA: 0x00019CF0 File Offset: 0x00017EF0
		private void ConstructOutputBufferChunk(int dataLen)
		{
			if (dataLen > 0 && dataLen < this._inputBuffer.Length)
			{
				byte[] array = new byte[dataLen];
				Buffer.BlockCopy(this._inputBuffer, 0, array, 0, dataLen);
				this._inputBuffer = array;
			}
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(dataLen.ToString("X", CultureInfo.InvariantCulture));
			string data = string.Concat(new string[]
			{
				"AWS4-HMAC-SHA256-PAYLOAD\n",
				this.HeaderSigningResult.ISO8601DateTime,
				"\n",
				this.HeaderSigningResult.Scope,
				"\n",
				this.PreviousChunkSignature,
				"\n",
				AWSSDKUtils.ToHex(AWS4Signer.ComputeHash(""), true),
				"\n",
				(dataLen > 0) ? AWSSDKUtils.ToHex(AWS4Signer.ComputeHash(this._inputBuffer), true) : "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
			});
			string text = AWSSDKUtils.ToHex(AWS4Signer.SignBlob(this.HeaderSigningResult.SigningKey, data), true);
			this.PreviousChunkSignature = text;
			stringBuilder.Append(";chunk-signature=" + text);
			stringBuilder.Append("\r\n");
			try
			{
				byte[] bytes = Encoding.UTF8.GetBytes(stringBuilder.ToString());
				byte[] bytes2 = Encoding.UTF8.GetBytes("\r\n");
				int num = 0;
				Buffer.BlockCopy(bytes, 0, this._outputBuffer, num, bytes.Length);
				num += bytes.Length;
				if (dataLen > 0)
				{
					Buffer.BlockCopy(this._inputBuffer, 0, this._outputBuffer, num, dataLen);
					num += dataLen;
				}
				Buffer.BlockCopy(bytes2, 0, this._outputBuffer, num, bytes2.Length);
				this._outputBufferPos = 0;
				this._outputBufferDataLen = bytes.Length + dataLen + bytes2.Length;
			}
			catch (Exception ex)
			{
				throw new AmazonClientException("Unable to sign the chunked data. " + ex.Message, ex);
			}
		}

		// Token: 0x1700035B RID: 859
		// (get) Token: 0x06000AFE RID: 2814 RVA: 0x00019ECC File Offset: 0x000180CC
		public override long Length
		{
			get
			{
				if (base.BaseStream != null)
				{
					return ChunkedUploadWrapperStream.ComputeChunkedContentLength(base.BaseStream.Length);
				}
				return 0L;
			}
		}

		// Token: 0x1700035C RID: 860
		// (get) Token: 0x06000AFF RID: 2815 RVA: 0x000129CA File Offset: 0x00010BCA
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000B00 RID: 2816 RVA: 0x00019EEC File Offset: 0x000180EC
		public static long ComputeChunkedContentLength(long originalLength)
		{
			if (originalLength < 0L)
			{
				throw new ArgumentOutOfRangeException("originalLength", "Expected 0 or greater value for originalLength.");
			}
			if (originalLength == 0L)
			{
				return ChunkedUploadWrapperStream.CalculateChunkHeaderLength(0L);
			}
			long num = originalLength / (long)ChunkedUploadWrapperStream.DefaultChunkSize;
			long num2 = originalLength % (long)ChunkedUploadWrapperStream.DefaultChunkSize;
			return num * ChunkedUploadWrapperStream.CalculateChunkHeaderLength((long)ChunkedUploadWrapperStream.DefaultChunkSize) + ((num2 > 0L) ? ChunkedUploadWrapperStream.CalculateChunkHeaderLength(num2) : 0L) + ChunkedUploadWrapperStream.CalculateChunkHeaderLength(0L);
		}

		// Token: 0x06000B01 RID: 2817 RVA: 0x00019F4E File Offset: 0x0001814E
		private static long CalculateChunkHeaderLength(long chunkDataSize)
		{
			return (long)(chunkDataSize.ToString("X", CultureInfo.InvariantCulture).Length + ";chunk-signature=".Length + 64 + "\r\n".Length) + chunkDataSize + (long)"\r\n".Length;
		}

		// Token: 0x06000B02 RID: 2818 RVA: 0x00019F90 File Offset: 0x00018190
		private int FillInputBuffer()
		{
			if (this._wrappedStreamConsumed)
			{
				return 0;
			}
			int i = 0;
			if (this._readStrategy == ChunkedUploadWrapperStream.ReadStrategy.ReadDirect)
			{
				while (i < this._inputBuffer.Length)
				{
					if (this._wrappedStreamConsumed)
					{
						break;
					}
					int num = this._inputBuffer.Length - i;
					if (num > this._wrappedStreamBufferSize)
					{
						num = this._wrappedStreamBufferSize;
					}
					int num2 = base.BaseStream.Read(this._inputBuffer, i, num);
					if (num2 == 0)
					{
						this._wrappedStreamConsumed = true;
					}
					else
					{
						i += num2;
					}
				}
			}
			else
			{
				byte[] array = new byte[this._wrappedStreamBufferSize];
				while (i < this._inputBuffer.Length && !this._wrappedStreamConsumed)
				{
					int num3 = base.BaseStream.Read(array, 0, this._wrappedStreamBufferSize);
					if (num3 == 0)
					{
						this._wrappedStreamConsumed = true;
					}
					else
					{
						Buffer.BlockCopy(array, 0, this._inputBuffer, i, num3);
						i += num3;
					}
				}
			}
			return i;
		}

		// Token: 0x0400043D RID: 1085
		public static readonly int DefaultChunkSize = 131072;

		// Token: 0x0400043E RID: 1086
		private const string CLRF = "\r\n";

		// Token: 0x0400043F RID: 1087
		private const string CHUNK_STRING_TO_SIGN_PREFIX = "AWS4-HMAC-SHA256-PAYLOAD";

		// Token: 0x04000440 RID: 1088
		private const string CHUNK_SIGNATURE_HEADER = ";chunk-signature=";

		// Token: 0x04000441 RID: 1089
		private const int SIGNATURE_LENGTH = 64;

		// Token: 0x04000442 RID: 1090
		private byte[] _inputBuffer;

		// Token: 0x04000443 RID: 1091
		private readonly byte[] _outputBuffer;

		// Token: 0x04000444 RID: 1092
		private int _outputBufferPos = -1;

		// Token: 0x04000445 RID: 1093
		private int _outputBufferDataLen = -1;

		// Token: 0x04000446 RID: 1094
		private readonly int _wrappedStreamBufferSize;

		// Token: 0x04000447 RID: 1095
		private bool _wrappedStreamConsumed;

		// Token: 0x04000448 RID: 1096
		private bool _outputBufferIsTerminatingChunk;

		// Token: 0x04000449 RID: 1097
		private readonly ChunkedUploadWrapperStream.ReadStrategy _readStrategy;

		// Token: 0x020001E7 RID: 487
		private enum ReadStrategy
		{
			// Token: 0x040009E2 RID: 2530
			ReadDirect,
			// Token: 0x040009E3 RID: 2531
			ReadAndCopy
		}
	}
}
