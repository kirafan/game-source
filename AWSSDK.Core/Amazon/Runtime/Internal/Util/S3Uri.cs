﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000134 RID: 308
	public class S3Uri
	{
		// Token: 0x1700039A RID: 922
		// (get) Token: 0x06000C04 RID: 3076 RVA: 0x0001C90A File Offset: 0x0001AB0A
		// (set) Token: 0x06000C05 RID: 3077 RVA: 0x0001C912 File Offset: 0x0001AB12
		public bool IsPathStyle { get; private set; }

		// Token: 0x1700039B RID: 923
		// (get) Token: 0x06000C06 RID: 3078 RVA: 0x0001C91B File Offset: 0x0001AB1B
		// (set) Token: 0x06000C07 RID: 3079 RVA: 0x0001C923 File Offset: 0x0001AB23
		public string Bucket { get; private set; }

		// Token: 0x1700039C RID: 924
		// (get) Token: 0x06000C08 RID: 3080 RVA: 0x0001C92C File Offset: 0x0001AB2C
		// (set) Token: 0x06000C09 RID: 3081 RVA: 0x0001C934 File Offset: 0x0001AB34
		public string Key { get; private set; }

		// Token: 0x1700039D RID: 925
		// (get) Token: 0x06000C0A RID: 3082 RVA: 0x0001C93D File Offset: 0x0001AB3D
		// (set) Token: 0x06000C0B RID: 3083 RVA: 0x0001C945 File Offset: 0x0001AB45
		public RegionEndpoint Region { get; set; }

		// Token: 0x06000C0C RID: 3084 RVA: 0x0001C94E File Offset: 0x0001AB4E
		public S3Uri(string uri) : this(new Uri(uri))
		{
		}

		// Token: 0x06000C0D RID: 3085 RVA: 0x0001C95C File Offset: 0x0001AB5C
		public S3Uri(Uri uri)
		{
			if (uri == null)
			{
				throw new ArgumentNullException("uri");
			}
			if (string.IsNullOrEmpty(uri.Host))
			{
				throw new ArgumentException("Invalid URI - no hostname present");
			}
			Match match = new Regex("^(.+\\.)?s3[.-]([a-z0-9-]+)\\.").Match(uri.Host);
			if (!match.Success)
			{
				throw new ArgumentException("Invalid S3 URI - hostname does not appear to be a valid S3 endpoint");
			}
			Group group = match.Groups[1];
			if (string.IsNullOrEmpty(group.Value))
			{
				this.IsPathStyle = true;
				string absolutePath = uri.AbsolutePath;
				if (absolutePath.Equals("/"))
				{
					this.Bucket = null;
					this.Key = null;
				}
				else
				{
					int num = absolutePath.IndexOf('/', 1);
					if (num == -1)
					{
						this.Bucket = S3Uri.Decode(absolutePath.Substring(1));
						this.Key = null;
					}
					else if (num == absolutePath.Length - 1)
					{
						this.Bucket = S3Uri.Decode(absolutePath.Substring(1, num)).TrimEnd(new char[]
						{
							'/'
						});
						this.Key = null;
					}
					else
					{
						this.Bucket = S3Uri.Decode(absolutePath.Substring(1, num)).TrimEnd(new char[]
						{
							'/'
						});
						this.Key = S3Uri.Decode(absolutePath.Substring(num + 1));
					}
				}
			}
			else
			{
				this.IsPathStyle = false;
				this.Bucket = group.Value.TrimEnd(new char[]
				{
					'.'
				});
				this.Key = (uri.AbsolutePath.Equals("/") ? null : uri.AbsolutePath.Substring(1));
			}
			if (match.Groups.Count > 2)
			{
				string value = match.Groups[2].Value;
				if (value.Equals("amazonaws", StringComparison.Ordinal) || value.Equals("external-1", StringComparison.Ordinal))
				{
					this.Region = RegionEndpoint.USEast1;
					return;
				}
				this.Region = RegionEndpoint.GetBySystemName(value);
			}
		}

		// Token: 0x06000C0E RID: 3086 RVA: 0x0001CB50 File Offset: 0x0001AD50
		private static string Decode(string s)
		{
			if (s == null)
			{
				return null;
			}
			for (int i = 0; i < s.Length; i++)
			{
				if (s[i] == '%')
				{
					return S3Uri.Decode(s, i);
				}
			}
			return s;
		}

		// Token: 0x06000C0F RID: 3087 RVA: 0x0001CB88 File Offset: 0x0001AD88
		private static string Decode(string s, int firstPercent)
		{
			StringBuilder stringBuilder = new StringBuilder(s.Substring(0, firstPercent));
			S3Uri.AppendDecoded(stringBuilder, s, firstPercent);
			for (int i = firstPercent + 3; i < s.Length; i++)
			{
				if (s[i] == '%')
				{
					S3Uri.AppendDecoded(stringBuilder, s, i);
					i += 2;
				}
				else
				{
					stringBuilder.Append(s[i]);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000C10 RID: 3088 RVA: 0x0001CBEC File Offset: 0x0001ADEC
		private static void AppendDecoded(StringBuilder builder, string s, int index)
		{
			if (index > s.Length - 3)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Invalid percent-encoded string '{0}'", new object[]
				{
					s
				}));
			}
			char c = s[index + 1];
			char c2 = s[index + 2];
			char value = (char)(S3Uri.FromHex(c) << 4 | S3Uri.FromHex(c2));
			builder.Append(value);
		}

		// Token: 0x06000C11 RID: 3089 RVA: 0x0001CC50 File Offset: 0x0001AE50
		private static int FromHex(char c)
		{
			if (c < '0')
			{
				throw new InvalidOperationException("Invalid percent-encoded string: bad character '" + c.ToString() + "' in escape sequence.");
			}
			if (c <= '9')
			{
				return (int)(c - '0');
			}
			if (c < 'A')
			{
				throw new InvalidOperationException("Invalid percent-encoded string: bad character '" + c.ToString() + "' in escape sequence.");
			}
			if (c <= 'F')
			{
				return (int)(c - 'A' + '\n');
			}
			if (c < 'a')
			{
				throw new InvalidOperationException("Invalid percent-encoded string: bad character '" + c.ToString() + "' in escape sequence.");
			}
			if (c <= 'f')
			{
				return (int)(c - 'a' + '\n');
			}
			throw new InvalidOperationException("Invalid percent-encoded string: bad character '" + c.ToString() + "' in escape sequence.");
		}

		// Token: 0x0400049D RID: 1181
		private const string EndpointPattern = "^(.+\\.)?s3[.-]([a-z0-9-]+)\\.";
	}
}
