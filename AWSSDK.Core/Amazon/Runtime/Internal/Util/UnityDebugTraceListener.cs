﻿using System;
using System.Diagnostics;
using UnityEngine;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200013E RID: 318
	public class UnityDebugTraceListener : TraceListener
	{
		// Token: 0x06000C6A RID: 3178 RVA: 0x0001D73C File Offset: 0x0001B93C
		public UnityDebugTraceListener()
		{
		}

		// Token: 0x06000C6B RID: 3179 RVA: 0x0001D744 File Offset: 0x0001B944
		public UnityDebugTraceListener(string name) : base(name)
		{
		}

		// Token: 0x06000C6C RID: 3180 RVA: 0x0001D74D File Offset: 0x0001B94D
		public override void Write(string message)
		{
			UnityEngine.Debug.Log(message);
		}

		// Token: 0x06000C6D RID: 3181 RVA: 0x0001D74D File Offset: 0x0001B94D
		public override void WriteLine(string message)
		{
			UnityEngine.Debug.Log(message);
		}

		// Token: 0x06000C6E RID: 3182 RVA: 0x0001D755 File Offset: 0x0001B955
		public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
		{
			this.LogMessage(string.Format(format, args), eventType);
		}

		// Token: 0x06000C6F RID: 3183 RVA: 0x0001D767 File Offset: 0x0001B967
		public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
		{
			this.LogMessage(data.ToString(), eventType);
		}

		// Token: 0x06000C70 RID: 3184 RVA: 0x0001D777 File Offset: 0x0001B977
		public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
		{
			this.LogMessage(message, eventType);
		}

		// Token: 0x06000C71 RID: 3185 RVA: 0x0001D784 File Offset: 0x0001B984
		public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, params object[] data)
		{
			foreach (object obj in data)
			{
				if (obj != null)
				{
					this.LogMessage(obj.ToString(), eventType);
				}
			}
		}

		// Token: 0x06000C72 RID: 3186 RVA: 0x0001D7B6 File Offset: 0x0001B9B6
		public override void Fail(string message)
		{
			UnityEngine.Debug.LogError(message);
		}

		// Token: 0x06000C73 RID: 3187 RVA: 0x0001D7BE File Offset: 0x0001B9BE
		public override void Fail(string message, string detailMessage)
		{
			UnityEngine.Debug.LogError(message + " " + detailMessage);
		}

		// Token: 0x06000C74 RID: 3188 RVA: 0x0001D7D1 File Offset: 0x0001B9D1
		public override void Write(object o)
		{
			UnityEngine.Debug.Log(o.ToString());
		}

		// Token: 0x06000C75 RID: 3189 RVA: 0x0001D7D1 File Offset: 0x0001B9D1
		public override void WriteLine(object o)
		{
			UnityEngine.Debug.Log(o.ToString());
		}

		// Token: 0x06000C76 RID: 3190 RVA: 0x0001D7D1 File Offset: 0x0001B9D1
		public override void WriteLine(object o, string category)
		{
			UnityEngine.Debug.Log(o.ToString());
		}

		// Token: 0x170003B4 RID: 948
		// (get) Token: 0x06000C77 RID: 3191 RVA: 0x00011C11 File Offset: 0x0000FE11
		public override bool IsThreadSafe
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000C78 RID: 3192 RVA: 0x0001D7E0 File Offset: 0x0001B9E0
		private void LogMessage(string message, TraceEventType eventType)
		{
			if (eventType.Equals(TraceEventType.Critical) || eventType.Equals(TraceEventType.Error))
			{
				UnityEngine.Debug.LogError(message);
				return;
			}
			if (eventType.Equals(TraceEventType.Warning))
			{
				UnityEngine.Debug.LogWarning(message);
				return;
			}
			UnityEngine.Debug.Log(message);
		}
	}
}
