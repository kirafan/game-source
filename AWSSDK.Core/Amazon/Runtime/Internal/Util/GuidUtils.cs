﻿using System;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200011C RID: 284
	public static class GuidUtils
	{
		// Token: 0x06000B31 RID: 2865 RVA: 0x0001A5BC File Offset: 0x000187BC
		public static bool TryParseNullableGuid(string value, out Guid? result)
		{
			Guid value2;
			if (GuidUtils.TryParseGuid(value, out value2))
			{
				result = new Guid?(value2);
				return true;
			}
			result = null;
			return false;
		}

		// Token: 0x06000B32 RID: 2866 RVA: 0x0001A5EC File Offset: 0x000187EC
		public static bool TryParseGuid(string value, out Guid result)
		{
			bool result2;
			try
			{
				result = new Guid(value);
				result2 = true;
			}
			catch (Exception)
			{
				result = Guid.Empty;
				result2 = false;
			}
			return result2;
		}
	}
}
