﻿using System;
using System.Globalization;
using System.Threading;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000124 RID: 292
	internal class InternalConsoleLogger : InternalLogger
	{
		// Token: 0x06000B69 RID: 2921 RVA: 0x0001AAD4 File Offset: 0x00018CD4
		public InternalConsoleLogger(Type declaringType) : base(declaringType)
		{
		}

		// Token: 0x06000B6A RID: 2922 RVA: 0x00005805 File Offset: 0x00003A05
		public override void Flush()
		{
		}

		// Token: 0x06000B6B RID: 2923 RVA: 0x0001AADD File Offset: 0x00018CDD
		public override void Error(Exception exception, string messageFormat, params object[] args)
		{
			this.Log(InternalConsoleLogger.LogLevel.Error, string.Format(CultureInfo.CurrentCulture, messageFormat, args), exception);
		}

		// Token: 0x06000B6C RID: 2924 RVA: 0x0001AAF3 File Offset: 0x00018CF3
		public override void Debug(Exception exception, string messageFormat, params object[] args)
		{
			this.Log(InternalConsoleLogger.LogLevel.Debug, string.Format(CultureInfo.CurrentCulture, messageFormat, args), exception);
		}

		// Token: 0x06000B6D RID: 2925 RVA: 0x0001AB09 File Offset: 0x00018D09
		public override void DebugFormat(string message, params object[] arguments)
		{
			this.Log(InternalConsoleLogger.LogLevel.Debug, string.Format(CultureInfo.CurrentCulture, message, arguments), null);
		}

		// Token: 0x06000B6E RID: 2926 RVA: 0x0001AB1F File Offset: 0x00018D1F
		public override void InfoFormat(string message, params object[] arguments)
		{
			this.Log(InternalConsoleLogger.LogLevel.Info, string.Format(CultureInfo.CurrentCulture, message, arguments), null);
		}

		// Token: 0x06000B6F RID: 2927 RVA: 0x0001AB38 File Offset: 0x00018D38
		private void Log(InternalConsoleLogger.LogLevel logLevel, string message, Exception ex)
		{
			long num = Interlocked.Increment(ref InternalConsoleLogger._sequanceId);
			string text = AWSSDKUtils.CorrectedUtcNow.ToLocalTime().ToString("yyyy-MM-dd\\THH:mm:ss.fff\\Z", CultureInfo.InvariantCulture);
			string text2 = logLevel.ToString().ToUpper(CultureInfo.InvariantCulture);
			string arg;
			if (ex != null)
			{
				arg = string.Format(CultureInfo.CurrentCulture, "{0}|{1}|{2}|{3} --> {4}", new object[]
				{
					num,
					text,
					text2,
					message,
					ex.ToString()
				});
			}
			else
			{
				arg = string.Format(CultureInfo.CurrentCulture, "{0}|{1}|{2}|{3}", new object[]
				{
					num,
					text,
					text2,
					message
				});
			}
			Console.WriteLine("{0} {1}", base.DeclaringType.Name, arg);
		}

		// Token: 0x04000460 RID: 1120
		public static long _sequanceId;

		// Token: 0x020001EC RID: 492
		private enum LogLevel
		{
			// Token: 0x040009EE RID: 2542
			Verbose = 2,
			// Token: 0x040009EF RID: 2543
			Debug,
			// Token: 0x040009F0 RID: 2544
			Info,
			// Token: 0x040009F1 RID: 2545
			Warn,
			// Token: 0x040009F2 RID: 2546
			Error,
			// Token: 0x040009F3 RID: 2547
			Assert
		}
	}
}
