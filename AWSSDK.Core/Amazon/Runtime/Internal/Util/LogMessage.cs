﻿using System;
using System.Globalization;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000128 RID: 296
	public class LogMessage : ILogMessage
	{
		// Token: 0x17000378 RID: 888
		// (get) Token: 0x06000B95 RID: 2965 RVA: 0x0001B591 File Offset: 0x00019791
		// (set) Token: 0x06000B96 RID: 2966 RVA: 0x0001B599 File Offset: 0x00019799
		public object[] Args { get; private set; }

		// Token: 0x17000379 RID: 889
		// (get) Token: 0x06000B97 RID: 2967 RVA: 0x0001B5A2 File Offset: 0x000197A2
		// (set) Token: 0x06000B98 RID: 2968 RVA: 0x0001B5AA File Offset: 0x000197AA
		public IFormatProvider Provider { get; private set; }

		// Token: 0x1700037A RID: 890
		// (get) Token: 0x06000B99 RID: 2969 RVA: 0x0001B5B3 File Offset: 0x000197B3
		// (set) Token: 0x06000B9A RID: 2970 RVA: 0x0001B5BB File Offset: 0x000197BB
		public string Format { get; private set; }

		// Token: 0x06000B9B RID: 2971 RVA: 0x0001B5C4 File Offset: 0x000197C4
		public LogMessage(string message) : this(CultureInfo.InvariantCulture, message, new object[0])
		{
		}

		// Token: 0x06000B9C RID: 2972 RVA: 0x0001B5D8 File Offset: 0x000197D8
		public LogMessage(string format, params object[] args) : this(CultureInfo.InvariantCulture, format, args)
		{
		}

		// Token: 0x06000B9D RID: 2973 RVA: 0x0001B5E7 File Offset: 0x000197E7
		public LogMessage(IFormatProvider provider, string format, params object[] args)
		{
			this.Args = args;
			this.Format = format;
			this.Provider = provider;
		}

		// Token: 0x06000B9E RID: 2974 RVA: 0x0001B604 File Offset: 0x00019804
		public override string ToString()
		{
			return string.Format(this.Provider, this.Format, this.Args);
		}
	}
}
