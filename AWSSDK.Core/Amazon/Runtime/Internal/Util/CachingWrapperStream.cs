﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000114 RID: 276
	public class CachingWrapperStream : WrapperStream
	{
		// Token: 0x17000356 RID: 854
		// (get) Token: 0x06000AEF RID: 2799 RVA: 0x00019B2C File Offset: 0x00017D2C
		// (set) Token: 0x06000AF0 RID: 2800 RVA: 0x00019B34 File Offset: 0x00017D34
		public List<byte> AllReadBytes { get; private set; }

		// Token: 0x06000AF1 RID: 2801 RVA: 0x00019B3D File Offset: 0x00017D3D
		public CachingWrapperStream(Stream baseStream, int cacheLimit) : base(baseStream)
		{
			this._cacheLimit = cacheLimit;
			this.AllReadBytes = new List<byte>(cacheLimit);
		}

		// Token: 0x06000AF2 RID: 2802 RVA: 0x00019B5C File Offset: 0x00017D5C
		public override int Read(byte[] buffer, int offset, int count)
		{
			int num = base.Read(buffer, offset, count);
			if (this._cachedBytes < this._cacheLimit)
			{
				int val = this._cacheLimit - this._cachedBytes;
				int num2 = Math.Min(num, val);
				byte[] array = new byte[num2];
				Array.Copy(buffer, offset, array, 0, num2);
				this.AllReadBytes.AddRange(array);
				this._cachedBytes += num2;
			}
			return num;
		}

		// Token: 0x17000357 RID: 855
		// (get) Token: 0x06000AF3 RID: 2803 RVA: 0x000129CA File Offset: 0x00010BCA
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000358 RID: 856
		// (get) Token: 0x06000AF4 RID: 2804 RVA: 0x00019BC3 File Offset: 0x00017DC3
		// (set) Token: 0x06000AF5 RID: 2805 RVA: 0x00019BC3 File Offset: 0x00017DC3
		public override long Position
		{
			get
			{
				throw new NotSupportedException("CachingWrapperStream does not support seeking");
			}
			set
			{
				throw new NotSupportedException("CachingWrapperStream does not support seeking");
			}
		}

		// Token: 0x06000AF6 RID: 2806 RVA: 0x00019BC3 File Offset: 0x00017DC3
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException("CachingWrapperStream does not support seeking");
		}

		// Token: 0x0400043A RID: 1082
		private int _cacheLimit;

		// Token: 0x0400043B RID: 1083
		private int _cachedBytes;
	}
}
