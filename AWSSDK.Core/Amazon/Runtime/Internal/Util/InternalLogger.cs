﻿using System;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000126 RID: 294
	internal abstract class InternalLogger
	{
		// Token: 0x17000370 RID: 880
		// (get) Token: 0x06000B7D RID: 2941 RVA: 0x0001AFE6 File Offset: 0x000191E6
		// (set) Token: 0x06000B7E RID: 2942 RVA: 0x0001AFEE File Offset: 0x000191EE
		public Type DeclaringType { get; private set; }

		// Token: 0x17000371 RID: 881
		// (get) Token: 0x06000B7F RID: 2943 RVA: 0x0001AFF7 File Offset: 0x000191F7
		// (set) Token: 0x06000B80 RID: 2944 RVA: 0x0001AFFF File Offset: 0x000191FF
		public bool IsEnabled { get; set; }

		// Token: 0x06000B81 RID: 2945 RVA: 0x0001B008 File Offset: 0x00019208
		public InternalLogger(Type declaringType)
		{
			this.DeclaringType = declaringType;
			this.IsEnabled = true;
		}

		// Token: 0x06000B82 RID: 2946
		public abstract void Flush();

		// Token: 0x17000372 RID: 882
		// (get) Token: 0x06000B83 RID: 2947 RVA: 0x00011C11 File Offset: 0x0000FE11
		public virtual bool IsErrorEnabled
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000373 RID: 883
		// (get) Token: 0x06000B84 RID: 2948 RVA: 0x00011C11 File Offset: 0x0000FE11
		public virtual bool IsDebugEnabled
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000374 RID: 884
		// (get) Token: 0x06000B85 RID: 2949 RVA: 0x00011C11 File Offset: 0x0000FE11
		public virtual bool IsInfoEnabled
		{
			get
			{
				return true;
			}
		}

		// Token: 0x06000B86 RID: 2950
		public abstract void Error(Exception exception, string messageFormat, params object[] args);

		// Token: 0x06000B87 RID: 2951
		public abstract void Debug(Exception exception, string messageFormat, params object[] args);

		// Token: 0x06000B88 RID: 2952
		public abstract void DebugFormat(string message, params object[] arguments);

		// Token: 0x06000B89 RID: 2953
		public abstract void InfoFormat(string message, params object[] arguments);
	}
}
