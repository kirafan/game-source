﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200013C RID: 316
	public class ThreadPoolThrottler<T>
	{
		// Token: 0x170003AF RID: 943
		// (get) Token: 0x06000C59 RID: 3161 RVA: 0x0001D54B File Offset: 0x0001B74B
		// (set) Token: 0x06000C5A RID: 3162 RVA: 0x0001D553 File Offset: 0x0001B753
		public int MaxConcurentRequest { get; private set; }

		// Token: 0x170003B0 RID: 944
		// (get) Token: 0x06000C5B RID: 3163 RVA: 0x0001D55C File Offset: 0x0001B75C
		public int RequestCount
		{
			get
			{
				return Thread.VolatileRead(ref this._requestCount);
			}
		}

		// Token: 0x06000C5C RID: 3164 RVA: 0x0001D569 File Offset: 0x0001B769
		public ThreadPoolThrottler(int maxConcurrentRequests)
		{
			this.MaxConcurentRequest = maxConcurrentRequests;
		}

		// Token: 0x06000C5D RID: 3165 RVA: 0x0001D584 File Offset: 0x0001B784
		public void Enqueue(T executionContext, Action<T> callback, Action<Exception, T> errorCallback)
		{
			ThreadPoolThrottler<T>.ThreadPoolOptions<T> threadPoolOptions = new ThreadPoolThrottler<T>.ThreadPoolOptions<T>
			{
				Callback = callback,
				ErrorCallback = errorCallback,
				State = executionContext
			};
			if (Interlocked.Increment(ref this._requestCount) <= this.MaxConcurentRequest)
			{
				ThreadPool.QueueUserWorkItem(new WaitCallback(this.Callback), threadPoolOptions);
				return;
			}
			object queueLock = ThreadPoolThrottler<T>._queueLock;
			lock (queueLock)
			{
				this._queuedRequests.Enqueue(threadPoolOptions);
			}
		}

		// Token: 0x06000C5E RID: 3166 RVA: 0x0001D604 File Offset: 0x0001B804
		private void Callback(object state)
		{
			ThreadPoolThrottler<T>.ThreadPoolOptions<T> threadPoolOptions = (ThreadPoolThrottler<T>.ThreadPoolOptions<T>)state;
			try
			{
				threadPoolOptions.Callback(threadPoolOptions.State);
			}
			catch (Exception arg)
			{
				threadPoolOptions.ErrorCallback(arg, threadPoolOptions.State);
			}
			finally
			{
				this.SignalCompletion();
			}
		}

		// Token: 0x06000C5F RID: 3167 RVA: 0x0001D664 File Offset: 0x0001B864
		private void SignalCompletion()
		{
			ThreadPoolThrottler<T>.ThreadPoolOptions<T> state = null;
			int num = 0;
			object queueLock = ThreadPoolThrottler<T>._queueLock;
			lock (queueLock)
			{
				num = this._queuedRequests.Count;
				if (num > 0)
				{
					state = this._queuedRequests.Dequeue();
				}
			}
			if (num > 0)
			{
				ThreadPool.QueueUserWorkItem(new WaitCallback(this.Callback), state);
			}
			Interlocked.Decrement(ref this._requestCount);
		}

		// Token: 0x040004AD RID: 1197
		private static object _queueLock = new object();

		// Token: 0x040004AE RID: 1198
		private int _requestCount;

		// Token: 0x040004AF RID: 1199
		private Queue<ThreadPoolThrottler<T>.ThreadPoolOptions<T>> _queuedRequests = new Queue<ThreadPoolThrottler<T>.ThreadPoolOptions<T>>();

		// Token: 0x020001F2 RID: 498
		public class ThreadPoolOptions<Q>
		{
			// Token: 0x17000458 RID: 1112
			// (get) Token: 0x06000FC6 RID: 4038 RVA: 0x00028321 File Offset: 0x00026521
			// (set) Token: 0x06000FC7 RID: 4039 RVA: 0x00028329 File Offset: 0x00026529
			public Action<Q> Callback { get; set; }

			// Token: 0x17000459 RID: 1113
			// (get) Token: 0x06000FC8 RID: 4040 RVA: 0x00028332 File Offset: 0x00026532
			// (set) Token: 0x06000FC9 RID: 4041 RVA: 0x0002833A File Offset: 0x0002653A
			public Action<Exception, Q> ErrorCallback { get; set; }

			// Token: 0x1700045A RID: 1114
			// (get) Token: 0x06000FCA RID: 4042 RVA: 0x00028343 File Offset: 0x00026543
			// (set) Token: 0x06000FCB RID: 4043 RVA: 0x0002834B File Offset: 0x0002654B
			public Q State { get; set; }
		}
	}
}
