﻿using System;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000135 RID: 309
	public interface ICache
	{
		// Token: 0x06000C12 RID: 3090
		void Clear();

		// Token: 0x1700039E RID: 926
		// (get) Token: 0x06000C13 RID: 3091
		// (set) Token: 0x06000C14 RID: 3092
		TimeSpan MaximumItemLifespan { get; set; }

		// Token: 0x1700039F RID: 927
		// (get) Token: 0x06000C15 RID: 3093
		// (set) Token: 0x06000C16 RID: 3094
		TimeSpan CacheClearPeriod { get; set; }

		// Token: 0x170003A0 RID: 928
		// (get) Token: 0x06000C17 RID: 3095
		int ItemCount { get; }
	}
}
