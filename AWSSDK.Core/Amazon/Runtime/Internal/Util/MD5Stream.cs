﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000121 RID: 289
	public class MD5Stream : HashStream<HashingWrapperMD5>
	{
		// Token: 0x06000B5B RID: 2907 RVA: 0x0001AA20 File Offset: 0x00018C20
		public MD5Stream(Stream baseStream, byte[] expectedHash, long expectedLength) : base(baseStream, expectedHash, expectedLength)
		{
			this._logger = Logger.GetLogger(base.GetType());
		}

		// Token: 0x06000B5C RID: 2908 RVA: 0x0001AA3C File Offset: 0x00018C3C
		public override void CalculateHash()
		{
			if (!base.FinishedHashing)
			{
				if (base.ExpectedLength < 0L || base.CurrentPosition == base.ExpectedLength)
				{
					base.CalculatedHash = base.Algorithm.AppendLastBlock(new byte[0]);
				}
				else
				{
					base.CalculatedHash = new byte[0];
				}
				if (base.CalculatedHash.Length != 0 && base.ExpectedHash != null && base.ExpectedHash.Length != 0 && !HashStream.CompareHashes(base.ExpectedHash, base.CalculatedHash))
				{
					this._logger.InfoFormat("The expected hash is not equal to the calculated hash. This can occur when the Http client decompresses a response body from gzip format before the hash is calculated, in which case thisis not an error.", new object[0]);
				}
			}
		}

		// Token: 0x0400045F RID: 1119
		private Logger _logger;
	}
}
