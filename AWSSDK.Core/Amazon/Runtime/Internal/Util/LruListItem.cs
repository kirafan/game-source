﻿using System;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200012B RID: 299
	public class LruListItem<TKey, TValue>
	{
		// Token: 0x1700037F RID: 895
		// (get) Token: 0x06000BB0 RID: 2992 RVA: 0x0001B986 File Offset: 0x00019B86
		// (set) Token: 0x06000BB1 RID: 2993 RVA: 0x0001B98E File Offset: 0x00019B8E
		public TValue Value { get; set; }

		// Token: 0x17000380 RID: 896
		// (get) Token: 0x06000BB2 RID: 2994 RVA: 0x0001B997 File Offset: 0x00019B97
		// (set) Token: 0x06000BB3 RID: 2995 RVA: 0x0001B99F File Offset: 0x00019B9F
		public TKey Key { get; private set; }

		// Token: 0x17000381 RID: 897
		// (get) Token: 0x06000BB4 RID: 2996 RVA: 0x0001B9A8 File Offset: 0x00019BA8
		// (set) Token: 0x06000BB5 RID: 2997 RVA: 0x0001B9B0 File Offset: 0x00019BB0
		public LruListItem<TKey, TValue> Next { get; set; }

		// Token: 0x17000382 RID: 898
		// (get) Token: 0x06000BB6 RID: 2998 RVA: 0x0001B9B9 File Offset: 0x00019BB9
		// (set) Token: 0x06000BB7 RID: 2999 RVA: 0x0001B9C1 File Offset: 0x00019BC1
		public LruListItem<TKey, TValue> Previous { get; set; }

		// Token: 0x06000BB8 RID: 3000 RVA: 0x0001B9CA File Offset: 0x00019BCA
		public LruListItem(TKey key, TValue value)
		{
			this.Key = key;
			this.Value = value;
		}
	}
}
