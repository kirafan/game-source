﻿using System;
using System.Collections.Generic;
using System.Linq;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000138 RID: 312
	internal class Cache<TKey, TValue> : ICache<TKey, TValue>, ICache
	{
		// Token: 0x06000C22 RID: 3106 RVA: 0x0001CE7F File Offset: 0x0001B07F
		public Cache(IEqualityComparer<TKey> keyComparer = null)
		{
			this.Contents = new Dictionary<TKey, Cache<TKey, TValue>.CacheItem<TValue>>(keyComparer);
			this.MaximumItemLifespan = Cache<TKey, TValue>.DefaultMaximumItemLifespan;
			this.CacheClearPeriod = Cache<TKey, TValue>.DefaultCacheClearPeriod;
		}

		// Token: 0x170003A2 RID: 930
		// (get) Token: 0x06000C23 RID: 3107 RVA: 0x0001CEB4 File Offset: 0x0001B0B4
		// (set) Token: 0x06000C24 RID: 3108 RVA: 0x0001CEBC File Offset: 0x0001B0BC
		public DateTime LastCacheClean { get; private set; }

		// Token: 0x06000C25 RID: 3109 RVA: 0x0001CEC8 File Offset: 0x0001B0C8
		public TValue GetValue(TKey key, Func<TKey, TValue> creator)
		{
			bool flag;
			return this.GetValueHelper(key, out flag, creator);
		}

		// Token: 0x06000C26 RID: 3110 RVA: 0x0001CEDF File Offset: 0x0001B0DF
		public TValue GetValue(TKey key, Func<TKey, TValue> creator, out bool isStaleItem)
		{
			return this.GetValueHelper(key, out isStaleItem, creator);
		}

		// Token: 0x06000C27 RID: 3111 RVA: 0x0001CEEC File Offset: 0x0001B0EC
		public void Clear(TKey key)
		{
			object cacheLock = this.CacheLock;
			lock (cacheLock)
			{
				this.Contents.Remove(key);
			}
		}

		// Token: 0x06000C28 RID: 3112 RVA: 0x0001CF2C File Offset: 0x0001B12C
		public void Clear()
		{
			object cacheLock = this.CacheLock;
			lock (cacheLock)
			{
				this.Contents.Clear();
				this.LastCacheClean = AWSSDKUtils.CorrectedUtcNow.ToLocalTime();
			}
		}

		// Token: 0x170003A3 RID: 931
		// (get) Token: 0x06000C29 RID: 3113 RVA: 0x0001CF80 File Offset: 0x0001B180
		public List<TKey> Keys
		{
			get
			{
				object cacheLock = this.CacheLock;
				List<TKey> result;
				lock (cacheLock)
				{
					result = this.Contents.Keys.ToList<TKey>();
				}
				return result;
			}
		}

		// Token: 0x170003A4 RID: 932
		// (get) Token: 0x06000C2A RID: 3114 RVA: 0x0001CFC8 File Offset: 0x0001B1C8
		// (set) Token: 0x06000C2B RID: 3115 RVA: 0x0001CFD0 File Offset: 0x0001B1D0
		public TimeSpan MaximumItemLifespan
		{
			get
			{
				return this.maximumItemLifespan;
			}
			set
			{
				if (value < TimeSpan.Zero)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.maximumItemLifespan = value;
			}
		}

		// Token: 0x170003A5 RID: 933
		// (get) Token: 0x06000C2C RID: 3116 RVA: 0x0001CFF1 File Offset: 0x0001B1F1
		// (set) Token: 0x06000C2D RID: 3117 RVA: 0x0001CFF9 File Offset: 0x0001B1F9
		public TimeSpan CacheClearPeriod
		{
			get
			{
				return this.cacheClearPeriod;
			}
			set
			{
				if (value < TimeSpan.Zero)
				{
					throw new ArgumentOutOfRangeException("value");
				}
				this.cacheClearPeriod = value;
			}
		}

		// Token: 0x170003A6 RID: 934
		// (get) Token: 0x06000C2E RID: 3118 RVA: 0x0001D01C File Offset: 0x0001B21C
		public int ItemCount
		{
			get
			{
				object cacheLock = this.CacheLock;
				int count;
				lock (cacheLock)
				{
					count = this.Contents.Count;
				}
				return count;
			}
		}

		// Token: 0x06000C2F RID: 3119 RVA: 0x0001D05C File Offset: 0x0001B25C
		public TOut UseCache<TOut>(TKey key, Func<TOut> operation, Action onError, Predicate<Exception> shouldRetryForException)
		{
			TOut result = default(TOut);
			try
			{
				result = operation();
			}
			catch (Exception obj)
			{
				if (shouldRetryForException != null && !shouldRetryForException(obj))
				{
					throw;
				}
				this.Clear(key);
				if (onError != null)
				{
					onError();
				}
				result = operation();
			}
			return result;
		}

		// Token: 0x06000C30 RID: 3120 RVA: 0x0001D0BC File Offset: 0x0001B2BC
		private TValue GetValueHelper(TKey key, out bool isStaleItem, Func<TKey, TValue> creator = null)
		{
			isStaleItem = true;
			Cache<TKey, TValue>.CacheItem<TValue> cacheItem = null;
			if (AWSConfigs.UseSdkCache)
			{
				object cacheLock = this.CacheLock;
				lock (cacheLock)
				{
					if (!this.Contents.TryGetValue(key, out cacheItem) || !this.IsValidItem(cacheItem))
					{
						if (creator == null)
						{
							throw new InvalidOperationException("Unable to calculate value for key " + key);
						}
						TValue value = creator(key);
						isStaleItem = false;
						cacheItem = new Cache<TKey, TValue>.CacheItem<TValue>(value);
						this.Contents[key] = cacheItem;
						this.RemoveOldItems_Locked();
					}
					goto IL_A0;
				}
			}
			if (creator == null)
			{
				throw new InvalidOperationException("Unable to calculate value for key " + key);
			}
			cacheItem = new Cache<TKey, TValue>.CacheItem<TValue>(creator(key));
			isStaleItem = false;
			IL_A0:
			if (cacheItem == null)
			{
				throw new InvalidOperationException("Unable to find value for key " + key);
			}
			return cacheItem.Value;
		}

		// Token: 0x06000C31 RID: 3121 RVA: 0x0001D198 File Offset: 0x0001B398
		private bool IsValidItem(Cache<TKey, TValue>.CacheItem<TValue> item)
		{
			if (item == null)
			{
				return false;
			}
			DateTime t = AWSSDKUtils.CorrectedUtcNow.ToLocalTime() - this.MaximumItemLifespan;
			return !(item.LastUseTime < t);
		}

		// Token: 0x06000C32 RID: 3122 RVA: 0x0001D1D4 File Offset: 0x0001B3D4
		private void RemoveOldItems_Locked()
		{
			if (this.LastCacheClean + this.CacheClearPeriod > AWSSDKUtils.CorrectedUtcNow.ToLocalTime())
			{
				return;
			}
			DateTime t = AWSSDKUtils.CorrectedUtcNow.ToLocalTime() - this.MaximumItemLifespan;
			List<TKey> list = new List<TKey>();
			foreach (KeyValuePair<TKey, Cache<TKey, TValue>.CacheItem<TValue>> keyValuePair in this.Contents)
			{
				TKey key = keyValuePair.Key;
				Cache<TKey, TValue>.CacheItem<TValue> value = keyValuePair.Value;
				if (value == null || value.LastUseTime < t)
				{
					list.Add(key);
				}
			}
			foreach (TKey key2 in list)
			{
				this.Contents.Remove(key2);
			}
			this.LastCacheClean = AWSSDKUtils.CorrectedUtcNow.ToLocalTime();
		}

		// Token: 0x040004A4 RID: 1188
		private Dictionary<TKey, Cache<TKey, TValue>.CacheItem<TValue>> Contents;

		// Token: 0x040004A5 RID: 1189
		private readonly object CacheLock = new object();

		// Token: 0x040004A6 RID: 1190
		public static TimeSpan DefaultMaximumItemLifespan = TimeSpan.FromHours(6.0);

		// Token: 0x040004A7 RID: 1191
		public static TimeSpan DefaultCacheClearPeriod = TimeSpan.FromHours(1.0);

		// Token: 0x040004A9 RID: 1193
		private TimeSpan maximumItemLifespan;

		// Token: 0x040004AA RID: 1194
		private TimeSpan cacheClearPeriod;

		// Token: 0x020001F1 RID: 497
		private class CacheItem<T>
		{
			// Token: 0x17000456 RID: 1110
			// (get) Token: 0x06000FC1 RID: 4033 RVA: 0x000282B4 File Offset: 0x000264B4
			// (set) Token: 0x06000FC2 RID: 4034 RVA: 0x000282DA File Offset: 0x000264DA
			public T Value
			{
				get
				{
					this.LastUseTime = AWSSDKUtils.CorrectedUtcNow.ToLocalTime();
					return this._value;
				}
				private set
				{
					this._value = value;
				}
			}

			// Token: 0x17000457 RID: 1111
			// (get) Token: 0x06000FC3 RID: 4035 RVA: 0x000282E3 File Offset: 0x000264E3
			// (set) Token: 0x06000FC4 RID: 4036 RVA: 0x000282EB File Offset: 0x000264EB
			public DateTime LastUseTime { get; private set; }

			// Token: 0x06000FC5 RID: 4037 RVA: 0x000282F4 File Offset: 0x000264F4
			public CacheItem(T value)
			{
				this.Value = value;
				this.LastUseTime = AWSSDKUtils.CorrectedUtcNow.ToLocalTime();
			}

			// Token: 0x04000A00 RID: 2560
			private T _value;
		}
	}
}
