﻿using System;
using System.Globalization;
using System.IO;
using System.Threading;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200011A RID: 282
	internal class EventStream : WrapperStream
	{
		// Token: 0x14000020 RID: 32
		// (add) Token: 0x06000B14 RID: 2836 RVA: 0x0001A2FC File Offset: 0x000184FC
		// (remove) Token: 0x06000B15 RID: 2837 RVA: 0x0001A334 File Offset: 0x00018534
		internal event EventStream.ReadProgress OnRead;

		// Token: 0x06000B16 RID: 2838 RVA: 0x0001A369 File Offset: 0x00018569
		internal EventStream(Stream stream, bool disableClose) : base(stream)
		{
			this.disableClose = disableClose;
		}

		// Token: 0x06000B17 RID: 2839 RVA: 0x00005805 File Offset: 0x00003A05
		protected override void Dispose(bool disposing)
		{
		}

		// Token: 0x1700035E RID: 862
		// (get) Token: 0x06000B18 RID: 2840 RVA: 0x0001A379 File Offset: 0x00018579
		public override bool CanRead
		{
			get
			{
				return base.BaseStream.CanRead;
			}
		}

		// Token: 0x1700035F RID: 863
		// (get) Token: 0x06000B19 RID: 2841 RVA: 0x0001A386 File Offset: 0x00018586
		public override bool CanSeek
		{
			get
			{
				return base.BaseStream.CanSeek;
			}
		}

		// Token: 0x17000360 RID: 864
		// (get) Token: 0x06000B1A RID: 2842 RVA: 0x0001A393 File Offset: 0x00018593
		public override bool CanTimeout
		{
			get
			{
				return base.BaseStream.CanTimeout;
			}
		}

		// Token: 0x17000361 RID: 865
		// (get) Token: 0x06000B1B RID: 2843 RVA: 0x0001A3A0 File Offset: 0x000185A0
		public override bool CanWrite
		{
			get
			{
				return base.BaseStream.CanWrite;
			}
		}

		// Token: 0x17000362 RID: 866
		// (get) Token: 0x06000B1C RID: 2844 RVA: 0x0001A3AD File Offset: 0x000185AD
		public override long Length
		{
			get
			{
				return base.BaseStream.Length;
			}
		}

		// Token: 0x17000363 RID: 867
		// (get) Token: 0x06000B1D RID: 2845 RVA: 0x0001A3BA File Offset: 0x000185BA
		// (set) Token: 0x06000B1E RID: 2846 RVA: 0x0001A3C7 File Offset: 0x000185C7
		public override long Position
		{
			get
			{
				return base.BaseStream.Position;
			}
			set
			{
				base.BaseStream.Position = value;
			}
		}

		// Token: 0x17000364 RID: 868
		// (get) Token: 0x06000B1F RID: 2847 RVA: 0x0001A3D5 File Offset: 0x000185D5
		// (set) Token: 0x06000B20 RID: 2848 RVA: 0x0001A3E2 File Offset: 0x000185E2
		public override int ReadTimeout
		{
			get
			{
				return base.BaseStream.ReadTimeout;
			}
			set
			{
				base.BaseStream.ReadTimeout = value;
			}
		}

		// Token: 0x17000365 RID: 869
		// (get) Token: 0x06000B21 RID: 2849 RVA: 0x0001A3F0 File Offset: 0x000185F0
		// (set) Token: 0x06000B22 RID: 2850 RVA: 0x0001A3FD File Offset: 0x000185FD
		public override int WriteTimeout
		{
			get
			{
				return base.BaseStream.WriteTimeout;
			}
			set
			{
				base.BaseStream.WriteTimeout = value;
			}
		}

		// Token: 0x06000B23 RID: 2851 RVA: 0x0001A40B File Offset: 0x0001860B
		public override void Flush()
		{
			base.BaseStream.Flush();
		}

		// Token: 0x06000B24 RID: 2852 RVA: 0x0001A418 File Offset: 0x00018618
		public override int Read(byte[] buffer, int offset, int count)
		{
			int num = base.BaseStream.Read(buffer, offset, count);
			if (this.OnRead != null)
			{
				this.OnRead(num);
			}
			return num;
		}

		// Token: 0x06000B25 RID: 2853 RVA: 0x0001A449 File Offset: 0x00018649
		public override long Seek(long offset, SeekOrigin origin)
		{
			return base.BaseStream.Seek(offset, origin);
		}

		// Token: 0x06000B26 RID: 2854 RVA: 0x0001A458 File Offset: 0x00018658
		public override void SetLength(long value)
		{
			base.BaseStream.SetLength(value);
		}

		// Token: 0x06000B27 RID: 2855 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000B28 RID: 2856 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		public override void WriteByte(byte value)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000B29 RID: 2857 RVA: 0x0001A468 File Offset: 0x00018668
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			EventStream.AsyncResult asyncResult = new EventStream.AsyncResult
			{
				AsyncState = state,
				CompletedSynchronously = true,
				IsCompleted = true,
				AsyncWaitHandle = new ManualResetEvent(true)
			};
			try
			{
				int num = this.Read(buffer, offset, count);
				asyncResult.Return = num;
			}
			catch (Exception @return)
			{
				asyncResult.Return = @return;
			}
			if (callback != null)
			{
				callback(asyncResult);
			}
			return asyncResult;
		}

		// Token: 0x06000B2A RID: 2858 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000B2B RID: 2859 RVA: 0x0001A4DC File Offset: 0x000186DC
		public override void Close()
		{
			if (!this.disableClose)
			{
				base.BaseStream.Close();
			}
		}

		// Token: 0x06000B2C RID: 2860 RVA: 0x0001A4F4 File Offset: 0x000186F4
		public override int EndRead(IAsyncResult asyncResult)
		{
			EventStream.AsyncResult asyncResult2 = asyncResult as EventStream.AsyncResult;
			if (asyncResult2 == null)
			{
				throw new ArgumentException("Parameter asyncResult was not of type Amazon.Runtime.Internal.Util.AsyncResult", "asyncResult");
			}
			if (asyncResult2.Return is Exception)
			{
				throw (Exception)asyncResult2.Return;
			}
			return Convert.ToInt32(asyncResult2.Return, CultureInfo.InvariantCulture);
		}

		// Token: 0x06000B2D RID: 2861 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		public override void EndWrite(IAsyncResult asyncResult)
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000455 RID: 1109
		private bool disableClose;

		// Token: 0x020001E9 RID: 489
		// (Invoke) Token: 0x06000F9E RID: 3998
		internal delegate void ReadProgress(int bytesRead);

		// Token: 0x020001EA RID: 490
		private class AsyncResult : IAsyncResult
		{
			// Token: 0x1700044D RID: 1101
			// (get) Token: 0x06000FA1 RID: 4001 RVA: 0x00028094 File Offset: 0x00026294
			// (set) Token: 0x06000FA2 RID: 4002 RVA: 0x0002809C File Offset: 0x0002629C
			public object AsyncState { get; internal set; }

			// Token: 0x1700044E RID: 1102
			// (get) Token: 0x06000FA3 RID: 4003 RVA: 0x000280A5 File Offset: 0x000262A5
			// (set) Token: 0x06000FA4 RID: 4004 RVA: 0x000280AD File Offset: 0x000262AD
			public WaitHandle AsyncWaitHandle { get; internal set; }

			// Token: 0x1700044F RID: 1103
			// (get) Token: 0x06000FA5 RID: 4005 RVA: 0x000280B6 File Offset: 0x000262B6
			// (set) Token: 0x06000FA6 RID: 4006 RVA: 0x000280BE File Offset: 0x000262BE
			public bool CompletedSynchronously { get; internal set; }

			// Token: 0x17000450 RID: 1104
			// (get) Token: 0x06000FA7 RID: 4007 RVA: 0x000280C7 File Offset: 0x000262C7
			// (set) Token: 0x06000FA8 RID: 4008 RVA: 0x000280CF File Offset: 0x000262CF
			public bool IsCompleted { get; internal set; }

			// Token: 0x17000451 RID: 1105
			// (get) Token: 0x06000FA9 RID: 4009 RVA: 0x000280D8 File Offset: 0x000262D8
			// (set) Token: 0x06000FAA RID: 4010 RVA: 0x000280E0 File Offset: 0x000262E0
			internal object Return { get; set; }
		}
	}
}
