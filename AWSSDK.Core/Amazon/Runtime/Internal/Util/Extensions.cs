﻿using System;
using System.Diagnostics;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200011B RID: 283
	public static class Extensions
	{
		// Token: 0x06000B2E RID: 2862 RVA: 0x0001A544 File Offset: 0x00018744
		public static long GetElapsedDateTimeTicks(this Stopwatch self)
		{
			return (long)((double)self.ElapsedTicks * Extensions.tickFrequency);
		}

		// Token: 0x06000B2F RID: 2863 RVA: 0x0001A554 File Offset: 0x00018754
		public static bool HasRequestData(this IRequest request)
		{
			return request != null && (request.ContentStream != null || request.Content != null || request.Parameters.Count > 0);
		}

		// Token: 0x04000456 RID: 1110
		private static readonly long ticksPerSecond = TimeSpan.FromSeconds(1.0).Ticks;

		// Token: 0x04000457 RID: 1111
		private static readonly double tickFrequency = (double)Extensions.ticksPerSecond / (double)Stopwatch.Frequency;
	}
}
