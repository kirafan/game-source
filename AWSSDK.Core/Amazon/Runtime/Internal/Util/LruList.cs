﻿using System;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200012A RID: 298
	public class LruList<TKey, TValue>
	{
		// Token: 0x1700037D RID: 893
		// (get) Token: 0x06000BA6 RID: 2982 RVA: 0x0001B810 File Offset: 0x00019A10
		// (set) Token: 0x06000BA7 RID: 2983 RVA: 0x0001B818 File Offset: 0x00019A18
		public LruListItem<TKey, TValue> Head { get; private set; }

		// Token: 0x1700037E RID: 894
		// (get) Token: 0x06000BA8 RID: 2984 RVA: 0x0001B821 File Offset: 0x00019A21
		// (set) Token: 0x06000BA9 RID: 2985 RVA: 0x0001B829 File Offset: 0x00019A29
		public LruListItem<TKey, TValue> Tail { get; private set; }

		// Token: 0x06000BAA RID: 2986 RVA: 0x0001B834 File Offset: 0x00019A34
		public void Add(LruListItem<TKey, TValue> item)
		{
			if (this.Head == null)
			{
				this.Head = item;
				this.Tail = item;
				item.Previous = null;
				item.Next = null;
				return;
			}
			this.Head.Previous = item;
			item.Next = this.Head;
			item.Previous = null;
			this.Head = item;
		}

		// Token: 0x06000BAB RID: 2987 RVA: 0x0001B88C File Offset: 0x00019A8C
		public void Remove(LruListItem<TKey, TValue> item)
		{
			if (this.Head == item || this.Tail == item)
			{
				if (this.Head == item)
				{
					this.Head = item.Next;
					if (this.Head != null)
					{
						this.Head.Previous = null;
					}
				}
				if (this.Tail == item)
				{
					this.Tail = item.Previous;
					if (this.Tail != null)
					{
						this.Tail.Next = null;
					}
				}
			}
			else
			{
				item.Previous.Next = item.Next;
				item.Next.Previous = item.Previous;
			}
			item.Previous = null;
			item.Next = null;
		}

		// Token: 0x06000BAC RID: 2988 RVA: 0x0001B92F File Offset: 0x00019B2F
		public void Touch(LruListItem<TKey, TValue> item)
		{
			this.Remove(item);
			this.Add(item);
		}

		// Token: 0x06000BAD RID: 2989 RVA: 0x0001B940 File Offset: 0x00019B40
		public TKey EvictOldest()
		{
			TKey result = default(TKey);
			if (this.Tail != null)
			{
				result = this.Tail.Key;
				this.Remove(this.Tail);
			}
			return result;
		}

		// Token: 0x06000BAE RID: 2990 RVA: 0x0001B976 File Offset: 0x00019B76
		internal void Clear()
		{
			this.Head = null;
			this.Tail = null;
		}
	}
}
