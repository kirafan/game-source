﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x02000122 RID: 290
	public interface IHashingWrapper : IDisposable
	{
		// Token: 0x06000B5D RID: 2909
		void Clear();

		// Token: 0x06000B5E RID: 2910
		byte[] ComputeHash(byte[] buffer);

		// Token: 0x06000B5F RID: 2911
		byte[] ComputeHash(Stream stream);

		// Token: 0x06000B60 RID: 2912
		void AppendBlock(byte[] buffer);

		// Token: 0x06000B61 RID: 2913
		void AppendBlock(byte[] buffer, int offset, int count);

		// Token: 0x06000B62 RID: 2914
		byte[] AppendLastBlock(byte[] buffer);

		// Token: 0x06000B63 RID: 2915
		byte[] AppendLastBlock(byte[] buffer, int offset, int count);
	}
}
