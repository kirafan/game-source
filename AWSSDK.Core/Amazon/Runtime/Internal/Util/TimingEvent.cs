﻿using System;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200012E RID: 302
	public class TimingEvent : IDisposable
	{
		// Token: 0x06000BD7 RID: 3031 RVA: 0x0001C5A9 File Offset: 0x0001A7A9
		internal TimingEvent(RequestMetrics metrics, Metric metric)
		{
			this.metrics = metrics;
			this.metric = metric;
		}

		// Token: 0x06000BD8 RID: 3032 RVA: 0x0001C5BF File Offset: 0x0001A7BF
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					this.metrics.StopEvent(this.metric);
				}
				this.disposed = true;
			}
		}

		// Token: 0x06000BD9 RID: 3033 RVA: 0x0001C5E5 File Offset: 0x0001A7E5
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000BDA RID: 3034 RVA: 0x0001C5F4 File Offset: 0x0001A7F4
		~TimingEvent()
		{
			this.Dispose(false);
		}

		// Token: 0x04000492 RID: 1170
		private Metric metric;

		// Token: 0x04000493 RID: 1171
		private RequestMetrics metrics;

		// Token: 0x04000494 RID: 1172
		private bool disposed;
	}
}
