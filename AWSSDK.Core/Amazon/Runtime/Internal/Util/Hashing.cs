﻿using System;
using System.Linq;

namespace Amazon.Runtime.Internal.Util
{
	// Token: 0x0200011D RID: 285
	public static class Hashing
	{
		// Token: 0x06000B33 RID: 2867 RVA: 0x0001A62C File Offset: 0x0001882C
		public static int Hash(params object[] value)
		{
			return Hashing.CombineHashes(value.Select(delegate(object v)
			{
				if (v != null)
				{
					return v.GetHashCode();
				}
				return 0;
			}).ToArray<int>());
		}

		// Token: 0x06000B34 RID: 2868 RVA: 0x0001A660 File Offset: 0x00018860
		public static int CombineHashes(params int[] hashes)
		{
			int num = 0;
			foreach (int b in hashes)
			{
				num = Hashing.CombineHashesInternal(num, b);
			}
			return num;
		}

		// Token: 0x06000B35 RID: 2869 RVA: 0x0001A68C File Offset: 0x0001888C
		private static int CombineHashesInternal(int a, int b)
		{
			return (a << 5) + a ^ b;
		}
	}
}
