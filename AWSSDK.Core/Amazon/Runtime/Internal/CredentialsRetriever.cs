﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000FD RID: 253
	public class CredentialsRetriever : PipelineHandler
	{
		// Token: 0x060009EB RID: 2539 RVA: 0x00016E57 File Offset: 0x00015057
		public CredentialsRetriever(AWSCredentials credentials)
		{
			this.Credentials = credentials;
		}

		// Token: 0x17000318 RID: 792
		// (get) Token: 0x060009EC RID: 2540 RVA: 0x00016E66 File Offset: 0x00015066
		// (set) Token: 0x060009ED RID: 2541 RVA: 0x00016E6E File Offset: 0x0001506E
		private protected AWSCredentials Credentials { protected get; private set; }

		// Token: 0x060009EE RID: 2542 RVA: 0x00016E78 File Offset: 0x00015078
		protected virtual void PreInvoke(IExecutionContext executionContext)
		{
			ImmutableCredentials immutableCredentials = null;
			if (this.Credentials != null && !(this.Credentials is AnonymousAWSCredentials))
			{
				using (executionContext.RequestContext.Metrics.StartEvent(Metric.CredentialsRequestTime))
				{
					immutableCredentials = this.Credentials.GetCredentials();
				}
			}
			executionContext.RequestContext.ImmutableCredentials = immutableCredentials;
		}

		// Token: 0x060009EF RID: 2543 RVA: 0x00016EE4 File Offset: 0x000150E4
		public override void InvokeSync(IExecutionContext executionContext)
		{
			this.PreInvoke(executionContext);
			base.InvokeSync(executionContext);
		}

		// Token: 0x060009F0 RID: 2544 RVA: 0x00016EF4 File Offset: 0x000150F4
		public override IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			this.PreInvoke(ExecutionContext.CreateFromAsyncContext(executionContext));
			return base.InvokeAsync(executionContext);
		}
	}
}
