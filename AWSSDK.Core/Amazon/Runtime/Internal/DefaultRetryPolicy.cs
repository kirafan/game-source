﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using Amazon.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x0200010B RID: 267
	public class DefaultRetryPolicy : RetryPolicy
	{
		// Token: 0x17000337 RID: 823
		// (get) Token: 0x06000A83 RID: 2691 RVA: 0x00018594 File Offset: 0x00016794
		// (set) Token: 0x06000A84 RID: 2692 RVA: 0x0001859C File Offset: 0x0001679C
		public int MaxBackoffInMilliseconds
		{
			get
			{
				return this._maxBackoffInMilliseconds;
			}
			set
			{
				this._maxBackoffInMilliseconds = value;
			}
		}

		// Token: 0x17000338 RID: 824
		// (get) Token: 0x06000A85 RID: 2693 RVA: 0x000185A5 File Offset: 0x000167A5
		public ICollection<HttpStatusCode> HttpStatusCodesToRetryOn
		{
			get
			{
				return this._httpStatusCodesToRetryOn;
			}
		}

		// Token: 0x17000339 RID: 825
		// (get) Token: 0x06000A86 RID: 2694 RVA: 0x000185AD File Offset: 0x000167AD
		public ICollection<string> ErrorCodesToRetryOn
		{
			get
			{
				return this._errorCodesToRetryOn;
			}
		}

		// Token: 0x1700033A RID: 826
		// (get) Token: 0x06000A87 RID: 2695 RVA: 0x000185B5 File Offset: 0x000167B5
		public ICollection<WebExceptionStatus> WebExceptionStatusesToRetryOn
		{
			get
			{
				return this._webExceptionStatusesToRetryOn;
			}
		}

		// Token: 0x06000A88 RID: 2696 RVA: 0x000185C0 File Offset: 0x000167C0
		public DefaultRetryPolicy(int maxRetries)
		{
			base.MaxRetries = maxRetries;
		}

		// Token: 0x06000A89 RID: 2697 RVA: 0x000186AC File Offset: 0x000168AC
		public DefaultRetryPolicy(IClientConfig config)
		{
			base.MaxRetries = config.MaxErrorRetry;
			if (config.ThrottleRetries)
			{
				string serviceURL = config.DetermineServiceURL();
				this._retryCapacity = DefaultRetryPolicy._capacityManagerInstance.GetRetryCapacity(serviceURL);
			}
		}

		// Token: 0x06000A8A RID: 2698 RVA: 0x000187BA File Offset: 0x000169BA
		public override bool CanRetry(IExecutionContext executionContext)
		{
			return executionContext.RequestContext.Request.IsRequestStreamRewindable();
		}

		// Token: 0x06000A8B RID: 2699 RVA: 0x000187CC File Offset: 0x000169CC
		public override bool RetryForException(IExecutionContext executionContext, Exception exception)
		{
			return this.RetryForExceptionSync(exception);
		}

		// Token: 0x06000A8C RID: 2700 RVA: 0x000187D5 File Offset: 0x000169D5
		public override bool OnRetry(IExecutionContext executionContext)
		{
			return !executionContext.RequestContext.ClientConfig.ThrottleRetries || this._retryCapacity == null || DefaultRetryPolicy._capacityManagerInstance.TryAcquireCapacity(this._retryCapacity);
		}

		// Token: 0x06000A8D RID: 2701 RVA: 0x00018803 File Offset: 0x00016A03
		public override void NotifySuccess(IExecutionContext executionContext)
		{
			if (executionContext.RequestContext.ClientConfig.ThrottleRetries && this._retryCapacity != null)
			{
				DefaultRetryPolicy._capacityManagerInstance.TryReleaseCapacity(executionContext.RequestContext.Retries > 0, this._retryCapacity);
			}
		}

		// Token: 0x06000A8E RID: 2702 RVA: 0x00018844 File Offset: 0x00016A44
		private bool RetryForExceptionSync(Exception exception)
		{
			if (exception is IOException)
			{
				return !DefaultRetryPolicy.IsInnerException<ThreadAbortException>(exception);
			}
			AmazonServiceException ex = exception as AmazonServiceException;
			if (ex != null)
			{
				if (this.HttpStatusCodesToRetryOn.Contains(ex.StatusCode))
				{
					return true;
				}
				if (ex.StatusCode == HttpStatusCode.BadRequest || ex.StatusCode == HttpStatusCode.ServiceUnavailable)
				{
					string errorCode = ex.ErrorCode;
					if (this.ErrorCodesToRetryOn.Contains(errorCode))
					{
						return true;
					}
				}
				WebException ex2;
				if (DefaultRetryPolicy.IsInnerException<WebException>(exception, out ex2) && this.WebExceptionStatusesToRetryOn.Contains(ex2.Status))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000A8F RID: 2703 RVA: 0x000188D5 File Offset: 0x00016AD5
		public override bool RetryLimitReached(IExecutionContext executionContext)
		{
			return executionContext.RequestContext.Retries >= base.MaxRetries;
		}

		// Token: 0x06000A90 RID: 2704 RVA: 0x000188ED File Offset: 0x00016AED
		public override void WaitBeforeRetry(IExecutionContext executionContext)
		{
			DefaultRetryPolicy.WaitBeforeRetry(executionContext.RequestContext.Retries, this.MaxBackoffInMilliseconds);
		}

		// Token: 0x06000A91 RID: 2705 RVA: 0x00018905 File Offset: 0x00016B05
		public static void WaitBeforeRetry(int retries, int maxBackoffInMilliseconds)
		{
			AWSSDKUtils.Sleep(DefaultRetryPolicy.CalculateRetryDelay(retries, maxBackoffInMilliseconds));
		}

		// Token: 0x06000A92 RID: 2706 RVA: 0x00018914 File Offset: 0x00016B14
		private static int CalculateRetryDelay(int retries, int maxBackoffInMilliseconds)
		{
			int num;
			if (retries < 12)
			{
				num = Convert.ToInt32(Math.Pow(4.0, (double)retries) * 100.0);
			}
			else
			{
				num = int.MaxValue;
			}
			if (retries > 0 && (num > maxBackoffInMilliseconds || num <= 0))
			{
				num = maxBackoffInMilliseconds;
			}
			return num;
		}

		// Token: 0x06000A93 RID: 2707 RVA: 0x0001895D File Offset: 0x00016B5D
		protected static bool ContainErrorMessage(Exception exception)
		{
			return exception != null && (DefaultRetryPolicy._coreCLRRetryErrorMessages.Contains(exception.Message) || DefaultRetryPolicy.ContainErrorMessage(exception.InnerException));
		}

		// Token: 0x06000A94 RID: 2708 RVA: 0x00018984 File Offset: 0x00016B84
		protected static bool IsInnerException<T>(Exception exception) where T : Exception
		{
			T t;
			return DefaultRetryPolicy.IsInnerException<T>(exception, out t);
		}

		// Token: 0x06000A95 RID: 2709 RVA: 0x0001899C File Offset: 0x00016B9C
		protected static bool IsInnerException<T>(Exception exception, out T inner) where T : Exception
		{
			inner = default(T);
			Exception ex = exception;
			while (ex.InnerException != null)
			{
				inner = (ex.InnerException as T);
				if (inner != null)
				{
					return true;
				}
				ex = ex.InnerException;
			}
			return false;
		}

		// Token: 0x040003F6 RID: 1014
		private const int THROTTLE_RETRY_REQUEST_COST = 5;

		// Token: 0x040003F7 RID: 1015
		private const int THROTTLED_RETRIES = 100;

		// Token: 0x040003F8 RID: 1016
		private const int THROTTLE_REQUEST_COST = 1;

		// Token: 0x040003F9 RID: 1017
		private static readonly CapacityManager _capacityManagerInstance = new CapacityManager(100, 5, 1);

		// Token: 0x040003FA RID: 1018
		private int _maxBackoffInMilliseconds = (int)TimeSpan.FromSeconds(30.0).TotalMilliseconds;

		// Token: 0x040003FB RID: 1019
		private RetryCapacity _retryCapacity;

		// Token: 0x040003FC RID: 1020
		private ICollection<HttpStatusCode> _httpStatusCodesToRetryOn = new HashSet<HttpStatusCode>
		{
			HttpStatusCode.InternalServerError,
			HttpStatusCode.ServiceUnavailable,
			HttpStatusCode.BadGateway,
			HttpStatusCode.GatewayTimeout
		};

		// Token: 0x040003FD RID: 1021
		private ICollection<WebExceptionStatus> _webExceptionStatusesToRetryOn = new HashSet<WebExceptionStatus>
		{
			WebExceptionStatus.ConnectFailure,
			WebExceptionStatus.ConnectionClosed,
			WebExceptionStatus.KeepAliveFailure,
			WebExceptionStatus.NameResolutionFailure,
			WebExceptionStatus.ReceiveFailure,
			WebExceptionStatus.SendFailure
		};

		// Token: 0x040003FE RID: 1022
		private static readonly HashSet<string> _coreCLRRetryErrorMessages = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
		{
			"The server returned an invalid or unrecognized response",
			"The connection with the server was terminated abnormally",
			"An error occurred while sending the request.",
			"Failed sending data to the peer"
		};

		// Token: 0x040003FF RID: 1023
		private ICollection<string> _errorCodesToRetryOn = new HashSet<string>
		{
			"Throttling",
			"ThrottlingException",
			"ProvisionedThroughputExceededException",
			"RequestTimeout"
		};
	}
}
