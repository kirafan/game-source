﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000FC RID: 252
	public class CallbackHandler : PipelineHandler
	{
		// Token: 0x17000316 RID: 790
		// (get) Token: 0x060009DF RID: 2527 RVA: 0x00016D9C File Offset: 0x00014F9C
		// (set) Token: 0x060009E0 RID: 2528 RVA: 0x00016DA4 File Offset: 0x00014FA4
		public Action<IExecutionContext> OnPreInvoke { get; set; }

		// Token: 0x17000317 RID: 791
		// (get) Token: 0x060009E1 RID: 2529 RVA: 0x00016DAD File Offset: 0x00014FAD
		// (set) Token: 0x060009E2 RID: 2530 RVA: 0x00016DB5 File Offset: 0x00014FB5
		public Action<IExecutionContext> OnPostInvoke { get; set; }

		// Token: 0x060009E3 RID: 2531 RVA: 0x00016DBE File Offset: 0x00014FBE
		public override void InvokeSync(IExecutionContext executionContext)
		{
			this.PreInvoke(executionContext);
			base.InvokeSync(executionContext);
			this.PostInvoke(executionContext);
		}

		// Token: 0x060009E4 RID: 2532 RVA: 0x00016DD5 File Offset: 0x00014FD5
		public override IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			this.PreInvoke(ExecutionContext.CreateFromAsyncContext(executionContext));
			return base.InvokeAsync(executionContext);
		}

		// Token: 0x060009E5 RID: 2533 RVA: 0x00016DEA File Offset: 0x00014FEA
		protected override void InvokeAsyncCallback(IAsyncExecutionContext executionContext)
		{
			if (executionContext.ResponseContext.AsyncResult.Exception == null)
			{
				this.PostInvoke(ExecutionContext.CreateFromAsyncContext(executionContext));
			}
			base.InvokeAsyncCallback(executionContext);
		}

		// Token: 0x060009E6 RID: 2534 RVA: 0x00016E11 File Offset: 0x00015011
		protected void PreInvoke(IExecutionContext executionContext)
		{
			this.RaiseOnPreInvoke(executionContext);
		}

		// Token: 0x060009E7 RID: 2535 RVA: 0x00016E1A File Offset: 0x0001501A
		protected void PostInvoke(IExecutionContext executionContext)
		{
			this.RaiseOnPostInvoke(executionContext);
		}

		// Token: 0x060009E8 RID: 2536 RVA: 0x00016E23 File Offset: 0x00015023
		private void RaiseOnPreInvoke(IExecutionContext context)
		{
			if (this.OnPreInvoke != null)
			{
				this.OnPreInvoke(context);
			}
		}

		// Token: 0x060009E9 RID: 2537 RVA: 0x00016E39 File Offset: 0x00015039
		private void RaiseOnPostInvoke(IExecutionContext context)
		{
			if (this.OnPostInvoke != null)
			{
				this.OnPostInvoke(context);
			}
		}
	}
}
