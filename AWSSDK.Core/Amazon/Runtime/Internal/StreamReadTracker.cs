﻿using System;
using Amazon.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000EF RID: 239
	internal class StreamReadTracker
	{
		// Token: 0x06000971 RID: 2417 RVA: 0x00015E2D File Offset: 0x0001402D
		internal StreamReadTracker(object sender, EventHandler<StreamTransferProgressArgs> callback, long contentLength, long progressUpdateInterval)
		{
			this.sender = sender;
			this.callback = callback;
			this.contentLength = contentLength;
			this.progressUpdateInterval = progressUpdateInterval;
		}

		// Token: 0x06000972 RID: 2418 RVA: 0x00015E54 File Offset: 0x00014054
		public void ReadProgress(int bytesRead)
		{
			if (this.callback == null)
			{
				return;
			}
			if (bytesRead > 0)
			{
				this.totalBytesRead += (long)bytesRead;
				this.totalIncrementTransferred += (long)bytesRead;
				if (this.totalIncrementTransferred >= this.progressUpdateInterval || this.totalBytesRead == this.contentLength)
				{
					AWSSDKUtils.InvokeInBackground<StreamTransferProgressArgs>(this.callback, new StreamTransferProgressArgs(this.totalIncrementTransferred, this.totalBytesRead, this.contentLength), this.sender);
					this.totalIncrementTransferred = 0L;
				}
			}
		}

		// Token: 0x06000973 RID: 2419 RVA: 0x00015ED8 File Offset: 0x000140D8
		public void UpdateProgress(float progress)
		{
			int bytesRead = (int)((long)(progress * (float)this.contentLength) - this.totalBytesRead);
			this.ReadProgress(bytesRead);
		}

		// Token: 0x040003AB RID: 939
		private object sender;

		// Token: 0x040003AC RID: 940
		private EventHandler<StreamTransferProgressArgs> callback;

		// Token: 0x040003AD RID: 941
		private long contentLength;

		// Token: 0x040003AE RID: 942
		private long totalBytesRead;

		// Token: 0x040003AF RID: 943
		private long totalIncrementTransferred;

		// Token: 0x040003B0 RID: 944
		private long progressUpdateInterval;
	}
}
