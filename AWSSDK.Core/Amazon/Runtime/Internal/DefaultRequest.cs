﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.Runtime.Internal.Auth;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000E8 RID: 232
	public class DefaultRequest : IRequest
	{
		// Token: 0x060008E6 RID: 2278 RVA: 0x00015494 File Offset: 0x00013694
		public DefaultRequest(AmazonWebServiceRequest request, string serviceName)
		{
			if (request == null)
			{
				throw new ArgumentNullException("request");
			}
			if (string.IsNullOrEmpty(serviceName))
			{
				throw new ArgumentNullException("serviceName");
			}
			this.serviceName = serviceName;
			this.originalRequest = request;
			this.requestName = this.originalRequest.GetType().Name;
			this.UseSigV4 = ((IAmazonWebServiceRequest)this.originalRequest).UseSigV4;
			this.parametersCollection = new ParameterCollection();
			this.parametersFacade = new ParametersDictionaryFacade(this.parametersCollection);
		}

		// Token: 0x170002B8 RID: 696
		// (get) Token: 0x060008E7 RID: 2279 RVA: 0x00015544 File Offset: 0x00013744
		public string RequestName
		{
			get
			{
				return this.requestName;
			}
		}

		// Token: 0x170002B9 RID: 697
		// (get) Token: 0x060008E8 RID: 2280 RVA: 0x0001554C File Offset: 0x0001374C
		// (set) Token: 0x060008E9 RID: 2281 RVA: 0x00015554 File Offset: 0x00013754
		public string HttpMethod
		{
			get
			{
				return this.httpMethod;
			}
			set
			{
				this.httpMethod = value;
			}
		}

		// Token: 0x170002BA RID: 698
		// (get) Token: 0x060008EA RID: 2282 RVA: 0x0001555D File Offset: 0x0001375D
		// (set) Token: 0x060008EB RID: 2283 RVA: 0x00015579 File Offset: 0x00013779
		public bool UseQueryString
		{
			get
			{
				return this.HttpMethod == "GET" || this.useQueryString;
			}
			set
			{
				this.useQueryString = value;
			}
		}

		// Token: 0x170002BB RID: 699
		// (get) Token: 0x060008EC RID: 2284 RVA: 0x00015582 File Offset: 0x00013782
		public AmazonWebServiceRequest OriginalRequest
		{
			get
			{
				return this.originalRequest;
			}
		}

		// Token: 0x170002BC RID: 700
		// (get) Token: 0x060008ED RID: 2285 RVA: 0x0001558A File Offset: 0x0001378A
		public IDictionary<string, string> Headers
		{
			get
			{
				return this.headers;
			}
		}

		// Token: 0x170002BD RID: 701
		// (get) Token: 0x060008EE RID: 2286 RVA: 0x00015592 File Offset: 0x00013792
		public IDictionary<string, string> Parameters
		{
			get
			{
				return this.parametersFacade;
			}
		}

		// Token: 0x170002BE RID: 702
		// (get) Token: 0x060008EF RID: 2287 RVA: 0x0001559A File Offset: 0x0001379A
		public ParameterCollection ParameterCollection
		{
			get
			{
				return this.parametersCollection;
			}
		}

		// Token: 0x170002BF RID: 703
		// (get) Token: 0x060008F0 RID: 2288 RVA: 0x000155A2 File Offset: 0x000137A2
		public IDictionary<string, string> SubResources
		{
			get
			{
				return this.subResources;
			}
		}

		// Token: 0x060008F1 RID: 2289 RVA: 0x000155AA File Offset: 0x000137AA
		public void AddSubResource(string subResource)
		{
			this.AddSubResource(subResource, null);
		}

		// Token: 0x060008F2 RID: 2290 RVA: 0x000155B4 File Offset: 0x000137B4
		public void AddSubResource(string subResource, string value)
		{
			this.SubResources.Add(subResource, value);
		}

		// Token: 0x170002C0 RID: 704
		// (get) Token: 0x060008F3 RID: 2291 RVA: 0x000155C3 File Offset: 0x000137C3
		// (set) Token: 0x060008F4 RID: 2292 RVA: 0x000155CB File Offset: 0x000137CB
		public Uri Endpoint
		{
			get
			{
				return this.endpoint;
			}
			set
			{
				this.endpoint = value;
			}
		}

		// Token: 0x170002C1 RID: 705
		// (get) Token: 0x060008F5 RID: 2293 RVA: 0x000155D4 File Offset: 0x000137D4
		// (set) Token: 0x060008F6 RID: 2294 RVA: 0x000155DC File Offset: 0x000137DC
		public string ResourcePath
		{
			get
			{
				return this.resourcePath;
			}
			set
			{
				this.resourcePath = value;
			}
		}

		// Token: 0x170002C2 RID: 706
		// (get) Token: 0x060008F7 RID: 2295 RVA: 0x000155E5 File Offset: 0x000137E5
		// (set) Token: 0x060008F8 RID: 2296 RVA: 0x000155ED File Offset: 0x000137ED
		public string CanonicalResource
		{
			get
			{
				return this.canonicalResource;
			}
			set
			{
				this.canonicalResource = value;
			}
		}

		// Token: 0x170002C3 RID: 707
		// (get) Token: 0x060008F9 RID: 2297 RVA: 0x000155F6 File Offset: 0x000137F6
		// (set) Token: 0x060008FA RID: 2298 RVA: 0x000155FE File Offset: 0x000137FE
		public byte[] Content
		{
			get
			{
				return this.content;
			}
			set
			{
				this.content = value;
			}
		}

		// Token: 0x170002C4 RID: 708
		// (get) Token: 0x060008FB RID: 2299 RVA: 0x00015607 File Offset: 0x00013807
		// (set) Token: 0x060008FC RID: 2300 RVA: 0x0001560F File Offset: 0x0001380F
		public bool SetContentFromParameters { get; set; }

		// Token: 0x170002C5 RID: 709
		// (get) Token: 0x060008FD RID: 2301 RVA: 0x00015618 File Offset: 0x00013818
		// (set) Token: 0x060008FE RID: 2302 RVA: 0x00015620 File Offset: 0x00013820
		public Stream ContentStream
		{
			get
			{
				return this.contentStream;
			}
			set
			{
				this.contentStream = value;
				this.OriginalStreamPosition = -1L;
				if (this.contentStream != null)
				{
					Stream nonWrapperBaseStream = WrapperStream.GetNonWrapperBaseStream(this.contentStream);
					if (nonWrapperBaseStream.CanSeek)
					{
						this.OriginalStreamPosition = nonWrapperBaseStream.Position;
					}
				}
			}
		}

		// Token: 0x170002C6 RID: 710
		// (get) Token: 0x060008FF RID: 2303 RVA: 0x00015664 File Offset: 0x00013864
		// (set) Token: 0x06000900 RID: 2304 RVA: 0x0001566C File Offset: 0x0001386C
		public long OriginalStreamPosition
		{
			get
			{
				return this.originalStreamLength;
			}
			set
			{
				this.originalStreamLength = value;
			}
		}

		// Token: 0x06000901 RID: 2305 RVA: 0x00015678 File Offset: 0x00013878
		public string ComputeContentStreamHash()
		{
			if (this.contentStream == null)
			{
				return null;
			}
			if (this.contentStreamHash == null)
			{
				Stream stream = WrapperStream.SearchWrappedStream(this.contentStream, (Stream s) => s.CanSeek);
				if (stream != null)
				{
					long position = stream.Position;
					byte[] data = CryptoUtilFactory.CryptoInstance.ComputeSHA256Hash(stream);
					this.contentStreamHash = AWSSDKUtils.ToHex(data, true);
					stream.Seek(position, SeekOrigin.Begin);
				}
			}
			return this.contentStreamHash;
		}

		// Token: 0x170002C7 RID: 711
		// (get) Token: 0x06000902 RID: 2306 RVA: 0x000156F4 File Offset: 0x000138F4
		public string ServiceName
		{
			get
			{
				return this.serviceName;
			}
		}

		// Token: 0x170002C8 RID: 712
		// (get) Token: 0x06000903 RID: 2307 RVA: 0x000156FC File Offset: 0x000138FC
		// (set) Token: 0x06000904 RID: 2308 RVA: 0x00015704 File Offset: 0x00013904
		public RegionEndpoint AlternateEndpoint
		{
			get
			{
				return this.alternateRegion;
			}
			set
			{
				this.alternateRegion = value;
			}
		}

		// Token: 0x170002C9 RID: 713
		// (get) Token: 0x06000905 RID: 2309 RVA: 0x0001570D File Offset: 0x0001390D
		// (set) Token: 0x06000906 RID: 2310 RVA: 0x00015715 File Offset: 0x00013915
		public bool Suppress404Exceptions { get; set; }

		// Token: 0x170002CA RID: 714
		// (get) Token: 0x06000907 RID: 2311 RVA: 0x0001571E File Offset: 0x0001391E
		// (set) Token: 0x06000908 RID: 2312 RVA: 0x00015726 File Offset: 0x00013926
		public AWS4SigningResult AWS4SignerResult { get; set; }

		// Token: 0x170002CB RID: 715
		// (get) Token: 0x06000909 RID: 2313 RVA: 0x0001572F File Offset: 0x0001392F
		// (set) Token: 0x0600090A RID: 2314 RVA: 0x00015737 File Offset: 0x00013937
		public bool UseChunkEncoding { get; set; }

		// Token: 0x170002CC RID: 716
		// (get) Token: 0x0600090B RID: 2315 RVA: 0x00015740 File Offset: 0x00013940
		// (set) Token: 0x0600090C RID: 2316 RVA: 0x00015748 File Offset: 0x00013948
		public string CanonicalResourcePrefix { get; set; }

		// Token: 0x170002CD RID: 717
		// (get) Token: 0x0600090D RID: 2317 RVA: 0x00015751 File Offset: 0x00013951
		// (set) Token: 0x0600090E RID: 2318 RVA: 0x00015759 File Offset: 0x00013959
		public bool UseSigV4 { get; set; }

		// Token: 0x170002CE RID: 718
		// (get) Token: 0x0600090F RID: 2319 RVA: 0x00015762 File Offset: 0x00013962
		// (set) Token: 0x06000910 RID: 2320 RVA: 0x0001576A File Offset: 0x0001396A
		public string AuthenticationRegion { get; set; }

		// Token: 0x06000911 RID: 2321 RVA: 0x00015774 File Offset: 0x00013974
		public bool IsRequestStreamRewindable()
		{
			Stream nonWrapperBaseStream = this.ContentStream;
			if (nonWrapperBaseStream != null)
			{
				nonWrapperBaseStream = WrapperStream.GetNonWrapperBaseStream(nonWrapperBaseStream);
				return nonWrapperBaseStream.CanSeek;
			}
			return true;
		}

		// Token: 0x06000912 RID: 2322 RVA: 0x0001579A File Offset: 0x0001399A
		public bool MayContainRequestBody()
		{
			return this.HttpMethod == "POST" || this.HttpMethod == "PUT" || this.HttpMethod == "PATCH";
		}

		// Token: 0x06000913 RID: 2323 RVA: 0x000157D4 File Offset: 0x000139D4
		public bool HasRequestBody()
		{
			bool flag = this.HttpMethod == "POST" || this.HttpMethod == "PUT" || this.HttpMethod == "PATCH";
			bool flag2 = this.HasRequestData();
			return flag && flag2;
		}

		// Token: 0x04000389 RID: 905
		private readonly ParameterCollection parametersCollection;

		// Token: 0x0400038A RID: 906
		private readonly IDictionary<string, string> parametersFacade;

		// Token: 0x0400038B RID: 907
		private readonly IDictionary<string, string> headers = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

		// Token: 0x0400038C RID: 908
		private readonly IDictionary<string, string> subResources = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

		// Token: 0x0400038D RID: 909
		private Uri endpoint;

		// Token: 0x0400038E RID: 910
		private string resourcePath;

		// Token: 0x0400038F RID: 911
		private string serviceName;

		// Token: 0x04000390 RID: 912
		private readonly AmazonWebServiceRequest originalRequest;

		// Token: 0x04000391 RID: 913
		private byte[] content;

		// Token: 0x04000392 RID: 914
		private Stream contentStream;

		// Token: 0x04000393 RID: 915
		private string contentStreamHash;

		// Token: 0x04000394 RID: 916
		private string httpMethod = "POST";

		// Token: 0x04000395 RID: 917
		private bool useQueryString;

		// Token: 0x04000396 RID: 918
		private string requestName;

		// Token: 0x04000397 RID: 919
		private string canonicalResource;

		// Token: 0x04000398 RID: 920
		private RegionEndpoint alternateRegion;

		// Token: 0x04000399 RID: 921
		private long originalStreamLength;
	}
}
