﻿using System;
using System.Net;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000102 RID: 258
	public class RedirectHandler : PipelineHandler
	{
		// Token: 0x06000A05 RID: 2565 RVA: 0x000172B0 File Offset: 0x000154B0
		public override void InvokeSync(IExecutionContext executionContext)
		{
			do
			{
				base.InvokeSync(executionContext);
			}
			while (this.HandleRedirect(executionContext));
		}

		// Token: 0x06000A06 RID: 2566 RVA: 0x000172C2 File Offset: 0x000154C2
		protected override void InvokeAsyncCallback(IAsyncExecutionContext executionContext)
		{
			if (executionContext.ResponseContext.AsyncResult.Exception == null && this.HandleRedirect(ExecutionContext.CreateFromAsyncContext(executionContext)))
			{
				base.InvokeAsync(executionContext);
				return;
			}
			base.InvokeAsyncCallback(executionContext);
		}

		// Token: 0x06000A07 RID: 2567 RVA: 0x000172F4 File Offset: 0x000154F4
		private bool HandleRedirect(IExecutionContext executionContext)
		{
			IWebResponseData httpResponse = executionContext.ResponseContext.HttpResponse;
			if (httpResponse.StatusCode >= HttpStatusCode.MultipleChoices && httpResponse.StatusCode < HttpStatusCode.BadRequest)
			{
				if (httpResponse.StatusCode == HttpStatusCode.TemporaryRedirect && httpResponse.IsHeaderPresent("location"))
				{
					IRequestContext requestContext = executionContext.RequestContext;
					string headerValue = httpResponse.GetHeaderValue("location");
					requestContext.Metrics.AddProperty(Metric.RedirectLocation, headerValue);
					if (executionContext.RequestContext.Request.IsRequestStreamRewindable() && !string.IsNullOrEmpty(headerValue))
					{
						this.FinalizeForRedirect(executionContext, headerValue);
						if (httpResponse.ResponseBody != null)
						{
							httpResponse.ResponseBody.Dispose();
						}
						return true;
					}
				}
				executionContext.ResponseContext.HttpResponse = null;
				throw new HttpErrorResponseException(httpResponse);
			}
			return false;
		}

		// Token: 0x06000A08 RID: 2568 RVA: 0x000173B0 File Offset: 0x000155B0
		protected virtual void FinalizeForRedirect(IExecutionContext executionContext, string redirectedLocation)
		{
			this.Logger.InfoFormat("Request {0} is being redirected to {1}.", new object[]
			{
				executionContext.RequestContext.RequestName,
				redirectedLocation
			});
			Uri uri = new Uri(redirectedLocation);
			executionContext.RequestContext.Request.Endpoint = new UriBuilder(uri.Scheme, uri.Host).Uri;
			RetryHandler.PrepareForRetry(executionContext.RequestContext);
		}
	}
}
