﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000109 RID: 265
	public sealed class UnityWwwRequest : IHttpRequest<string>, IDisposable, IUnityHttpRequest
	{
		// Token: 0x17000327 RID: 807
		// (get) Token: 0x06000A54 RID: 2644 RVA: 0x0001826F File Offset: 0x0001646F
		// (set) Token: 0x06000A55 RID: 2645 RVA: 0x00018277 File Offset: 0x00016477
		public Uri RequestUri { get; private set; }

		// Token: 0x17000328 RID: 808
		// (get) Token: 0x06000A56 RID: 2646 RVA: 0x00018280 File Offset: 0x00016480
		// (set) Token: 0x06000A57 RID: 2647 RVA: 0x00018288 File Offset: 0x00016488
		public IDisposable WwwRequest { get; set; }

		// Token: 0x17000329 RID: 809
		// (get) Token: 0x06000A58 RID: 2648 RVA: 0x00018291 File Offset: 0x00016491
		// (set) Token: 0x06000A59 RID: 2649 RVA: 0x00018299 File Offset: 0x00016499
		public byte[] RequestContent { get; internal set; }

		// Token: 0x1700032A RID: 810
		// (get) Token: 0x06000A5A RID: 2650 RVA: 0x000182A2 File Offset: 0x000164A2
		// (set) Token: 0x06000A5B RID: 2651 RVA: 0x000182AA File Offset: 0x000164AA
		public Dictionary<string, string> Headers { get; private set; }

		// Token: 0x1700032B RID: 811
		// (get) Token: 0x06000A5C RID: 2652 RVA: 0x000182B3 File Offset: 0x000164B3
		// (set) Token: 0x06000A5D RID: 2653 RVA: 0x000182BB File Offset: 0x000164BB
		public AsyncCallback Callback { get; private set; }

		// Token: 0x1700032C RID: 812
		// (get) Token: 0x06000A5E RID: 2654 RVA: 0x000182C4 File Offset: 0x000164C4
		// (set) Token: 0x06000A5F RID: 2655 RVA: 0x000182CC File Offset: 0x000164CC
		public IAsyncResult AsyncResult { get; private set; }

		// Token: 0x1700032D RID: 813
		// (get) Token: 0x06000A60 RID: 2656 RVA: 0x000182D5 File Offset: 0x000164D5
		// (set) Token: 0x06000A61 RID: 2657 RVA: 0x000182DD File Offset: 0x000164DD
		public ManualResetEvent WaitHandle { get; private set; }

		// Token: 0x1700032E RID: 814
		// (get) Token: 0x06000A62 RID: 2658 RVA: 0x000182E6 File Offset: 0x000164E6
		// (set) Token: 0x06000A63 RID: 2659 RVA: 0x000182EE File Offset: 0x000164EE
		public bool IsSync { get; set; }

		// Token: 0x1700032F RID: 815
		// (get) Token: 0x06000A64 RID: 2660 RVA: 0x000182F7 File Offset: 0x000164F7
		// (set) Token: 0x06000A65 RID: 2661 RVA: 0x000182FF File Offset: 0x000164FF
		public IWebResponseData Response { get; set; }

		// Token: 0x17000330 RID: 816
		// (get) Token: 0x06000A66 RID: 2662 RVA: 0x00018308 File Offset: 0x00016508
		// (set) Token: 0x06000A67 RID: 2663 RVA: 0x00018310 File Offset: 0x00016510
		public Exception Exception { get; set; }

		// Token: 0x17000331 RID: 817
		// (get) Token: 0x06000A68 RID: 2664 RVA: 0x00018319 File Offset: 0x00016519
		// (set) Token: 0x06000A69 RID: 2665 RVA: 0x00018321 File Offset: 0x00016521
		public string Method { get; set; }

		// Token: 0x06000A6A RID: 2666 RVA: 0x0001832A File Offset: 0x0001652A
		public UnityWwwRequest(Uri requestUri)
		{
			this.RequestUri = requestUri;
			this.Headers = new Dictionary<string, string>();
		}

		// Token: 0x06000A6B RID: 2667 RVA: 0x00005805 File Offset: 0x00003A05
		public void ConfigureRequest(IRequestContext requestContext)
		{
		}

		// Token: 0x06000A6C RID: 2668 RVA: 0x00018344 File Offset: 0x00016544
		public void SetRequestHeaders(IDictionary<string, string> headers)
		{
			foreach (KeyValuePair<string, string> item in headers)
			{
				this.Headers.Add(item);
			}
		}

		// Token: 0x06000A6D RID: 2669 RVA: 0x00018064 File Offset: 0x00016264
		public string GetRequestContent()
		{
			return string.Empty;
		}

		// Token: 0x06000A6E RID: 2670 RVA: 0x00018394 File Offset: 0x00016594
		public IWebResponseData GetResponse()
		{
			if (UnityInitializer.IsMainThread())
			{
				throw new Exception("Cannot execute synchronous calls on game thread");
			}
			this.IsSync = true;
			this.WaitHandle = new ManualResetEvent(false);
			IWebResponseData response;
			try
			{
				UnityRequestQueue.Instance.EnqueueRequest(this);
				this.WaitHandle.WaitOne();
				if (this.Exception != null)
				{
					throw this.Exception;
				}
				if (this.Exception == null && this.Response == null)
				{
					throw new WebException("Request timed out", WebExceptionStatus.Timeout);
				}
				response = this.Response;
			}
			finally
			{
				this.WaitHandle.Close();
			}
			return response;
		}

		// Token: 0x06000A6F RID: 2671 RVA: 0x00018430 File Offset: 0x00016630
		public void WriteToRequestBody(string requestContent, Stream contentStream, IDictionary<string, string> contentHeaders, IRequestContext requestContext)
		{
			byte[] array = new byte[8192];
			using (MemoryStream memoryStream = new MemoryStream())
			{
				int count;
				while ((count = contentStream.Read(array, 0, array.Length)) > 0)
				{
					memoryStream.Write(array, 0, count);
				}
				this.RequestContent = memoryStream.ToArray();
			}
		}

		// Token: 0x06000A70 RID: 2672 RVA: 0x00018494 File Offset: 0x00016694
		public void WriteToRequestBody(string requestContent, byte[] content, IDictionary<string, string> contentHeaders)
		{
			this.RequestContent = content;
		}

		// Token: 0x06000A71 RID: 2673 RVA: 0x00005805 File Offset: 0x00003A05
		public void Abort()
		{
		}

		// Token: 0x06000A72 RID: 2674 RVA: 0x000184A0 File Offset: 0x000166A0
		public IAsyncResult BeginGetRequestContent(AsyncCallback callback, object state)
		{
			SimpleAsyncResult simpleAsyncResult = new SimpleAsyncResult(state);
			callback(simpleAsyncResult);
			return simpleAsyncResult;
		}

		// Token: 0x06000A73 RID: 2675 RVA: 0x00018064 File Offset: 0x00016264
		public string EndGetRequestContent(IAsyncResult asyncResult)
		{
			return string.Empty;
		}

		// Token: 0x06000A74 RID: 2676 RVA: 0x000184BC File Offset: 0x000166BC
		public IAsyncResult BeginGetResponse(AsyncCallback callback, object state)
		{
			this.Callback = callback;
			this.AsyncResult = new SimpleAsyncResult(state);
			UnityRequestQueue.Instance.EnqueueRequest(this);
			return this.AsyncResult;
		}

		// Token: 0x06000A75 RID: 2677 RVA: 0x000184E2 File Offset: 0x000166E2
		public IWebResponseData EndGetResponse(IAsyncResult asyncResult)
		{
			if (this.Exception != null)
			{
				throw this.Exception;
			}
			return this.Response;
		}

		// Token: 0x17000332 RID: 818
		// (get) Token: 0x06000A76 RID: 2678 RVA: 0x000184F9 File Offset: 0x000166F9
		// (set) Token: 0x06000A77 RID: 2679 RVA: 0x00018501 File Offset: 0x00016701
		private StreamReadTracker Tracker { get; set; }

		// Token: 0x06000A78 RID: 2680 RVA: 0x0001850A File Offset: 0x0001670A
		public Stream SetupProgressListeners(Stream originalStream, long progressUpdateInterval, object sender, EventHandler<StreamTransferProgressArgs> callback)
		{
			this.Tracker = new StreamReadTracker(sender, callback, originalStream.Length, progressUpdateInterval);
			return originalStream;
		}

		// Token: 0x06000A79 RID: 2681 RVA: 0x00018522 File Offset: 0x00016722
		public void OnUploadProgressChanged(float progress)
		{
			if (this.Tracker != null)
			{
				this.Tracker.UpdateProgress(progress);
			}
		}

		// Token: 0x06000A7A RID: 2682 RVA: 0x00018538 File Offset: 0x00016738
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000A7B RID: 2683 RVA: 0x00018547 File Offset: 0x00016747
		private void Dispose(bool disposing)
		{
			if (this._disposed)
			{
				return;
			}
			if (disposing)
			{
				UnityRequestQueue.Instance.ExecuteOnMainThread(delegate
				{
					this._disposed = true;
				});
			}
		}

		// Token: 0x040003F4 RID: 1012
		private bool _disposed;
	}
}
