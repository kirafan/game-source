﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000E3 RID: 227
	public class RetryCapacity
	{
		// Token: 0x170002B5 RID: 693
		// (get) Token: 0x060008D5 RID: 2261 RVA: 0x000150E4 File Offset: 0x000132E4
		// (set) Token: 0x060008D6 RID: 2262 RVA: 0x000150EC File Offset: 0x000132EC
		public int AvailableCapacity { get; set; }

		// Token: 0x170002B6 RID: 694
		// (get) Token: 0x060008D7 RID: 2263 RVA: 0x000150F5 File Offset: 0x000132F5
		public int MaxCapacity
		{
			get
			{
				return this._maxCapacity;
			}
		}

		// Token: 0x060008D8 RID: 2264 RVA: 0x000150FD File Offset: 0x000132FD
		public RetryCapacity(int maxCapacity)
		{
			this._maxCapacity = maxCapacity;
			this.AvailableCapacity = maxCapacity;
		}

		// Token: 0x04000369 RID: 873
		private readonly int _maxCapacity;
	}
}
