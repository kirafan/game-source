﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using System.Text;
using System.Threading;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000105 RID: 261
	public class HttpHandler<TRequestContent> : PipelineHandler, IDisposable
	{
		// Token: 0x1700031A RID: 794
		// (get) Token: 0x06000A16 RID: 2582 RVA: 0x000177AF File Offset: 0x000159AF
		// (set) Token: 0x06000A17 RID: 2583 RVA: 0x000177B7 File Offset: 0x000159B7
		public object CallbackSender { get; private set; }

		// Token: 0x06000A18 RID: 2584 RVA: 0x000177C0 File Offset: 0x000159C0
		public HttpHandler(IHttpRequestFactory<TRequestContent> requestFactory, object callbackSender)
		{
			this._requestFactory = requestFactory;
			this.CallbackSender = callbackSender;
		}

		// Token: 0x06000A19 RID: 2585 RVA: 0x000177D8 File Offset: 0x000159D8
		public override void InvokeSync(IExecutionContext executionContext)
		{
			IHttpRequest<TRequestContent> httpRequest = null;
			try
			{
				HttpHandler<TRequestContent>.SetMetrics(executionContext.RequestContext);
				IRequest request = executionContext.RequestContext.Request;
				httpRequest = this.CreateWebRequest(executionContext.RequestContext);
				httpRequest.SetRequestHeaders(request.Headers);
				using (executionContext.RequestContext.Metrics.StartEvent(Metric.HttpRequestTime))
				{
					if (request.HasRequestBody())
					{
						try
						{
							TRequestContent requestContent = httpRequest.GetRequestContent();
							this.WriteContentToRequestBody(requestContent, httpRequest, executionContext.RequestContext);
						}
						catch
						{
							HttpHandler<TRequestContent>.CompleteFailedRequest(httpRequest);
							throw;
						}
					}
					executionContext.ResponseContext.HttpResponse = httpRequest.GetResponse();
				}
			}
			finally
			{
				if (httpRequest != null)
				{
					httpRequest.Dispose();
				}
			}
		}

		// Token: 0x06000A1A RID: 2586 RVA: 0x000178A4 File Offset: 0x00015AA4
		private static void CompleteFailedRequest(IHttpRequest<TRequestContent> httpRequest)
		{
			try
			{
				IWebResponseData webResponseData = null;
				try
				{
					webResponseData = httpRequest.GetResponse();
				}
				catch (WebException ex)
				{
					if (ex.Response != null)
					{
						ex.Response.Close();
					}
				}
				catch (HttpErrorResponseException ex2)
				{
					if (ex2.Response != null && ex2.Response.ResponseBody != null)
					{
						ex2.Response.ResponseBody.Dispose();
					}
				}
				finally
				{
					if (webResponseData != null && webResponseData.ResponseBody != null)
					{
						webResponseData.ResponseBody.Dispose();
					}
				}
			}
			catch
			{
			}
		}

		// Token: 0x06000A1B RID: 2587 RVA: 0x0001794C File Offset: 0x00015B4C
		public override IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			IHttpRequest<TRequestContent> httpRequest = null;
			IAsyncResult asyncResult;
			try
			{
				HttpHandler<TRequestContent>.SetMetrics(executionContext.RequestContext);
				httpRequest = this.CreateWebRequest(executionContext.RequestContext);
				executionContext.RuntimeState = httpRequest;
				IRequest request = executionContext.RequestContext.Request;
				if (executionContext.RequestContext.Retries == 0)
				{
					executionContext.ResponseContext.AsyncResult = new RuntimeAsyncResult(executionContext.RequestContext.Callback, executionContext.RequestContext.State);
					executionContext.ResponseContext.AsyncResult.AsyncOptions = executionContext.RequestContext.AsyncOptions;
					executionContext.ResponseContext.AsyncResult.Action = executionContext.RequestContext.Action;
					executionContext.ResponseContext.AsyncResult.Request = executionContext.RequestContext.OriginalRequest;
				}
				httpRequest.SetRequestHeaders(executionContext.RequestContext.Request.Headers);
				executionContext.RequestContext.Metrics.StartEvent(Metric.HttpRequestTime);
				if (request.HasRequestBody())
				{
					httpRequest.BeginGetRequestContent(new AsyncCallback(this.GetRequestStreamCallback), executionContext);
				}
				else
				{
					httpRequest.BeginGetResponse(new AsyncCallback(this.GetResponseCallback), executionContext);
				}
				asyncResult = executionContext.ResponseContext.AsyncResult;
			}
			catch (Exception exception)
			{
				if (executionContext.ResponseContext.AsyncResult != null)
				{
					executionContext.ResponseContext.AsyncResult.Dispose();
					executionContext.ResponseContext.AsyncResult = null;
				}
				if (httpRequest != null)
				{
					httpRequest.Dispose();
				}
				this.Logger.Error(exception, "An exception occured while initiating an asynchronous HTTP request.", new object[0]);
				throw;
			}
			return asyncResult;
		}

		// Token: 0x06000A1C RID: 2588 RVA: 0x00017ADC File Offset: 0x00015CDC
		private void GetRequestStreamCallback(IAsyncResult result)
		{
			if (result.CompletedSynchronously)
			{
				ThreadPool.QueueUserWorkItem(new WaitCallback(this.GetRequestStreamCallbackHelper), result);
				return;
			}
			this.GetRequestStreamCallbackHelper(result);
		}

		// Token: 0x06000A1D RID: 2589 RVA: 0x00017B04 File Offset: 0x00015D04
		private void GetRequestStreamCallbackHelper(object state)
		{
			IAsyncResult asyncResult = state as IAsyncResult;
			IAsyncExecutionContext asyncExecutionContext = null;
			IHttpRequest<TRequestContent> httpRequest = null;
			try
			{
				asyncExecutionContext = (asyncResult.AsyncState as IAsyncExecutionContext);
				httpRequest = (asyncExecutionContext.RuntimeState as IHttpRequest<TRequestContent>);
				TRequestContent requestContent = httpRequest.EndGetRequestContent(asyncResult);
				this.WriteContentToRequestBody(requestContent, httpRequest, asyncExecutionContext.RequestContext);
				httpRequest.BeginGetResponse(new AsyncCallback(this.GetResponseCallback), asyncExecutionContext);
			}
			catch (Exception exception)
			{
				httpRequest.Dispose();
				asyncExecutionContext.ResponseContext.AsyncResult.Exception = exception;
				base.InvokeAsyncCallback(asyncExecutionContext);
			}
		}

		// Token: 0x06000A1E RID: 2590 RVA: 0x00017B94 File Offset: 0x00015D94
		private void GetResponseCallback(IAsyncResult result)
		{
			if (result.CompletedSynchronously)
			{
				ThreadPool.QueueUserWorkItem(new WaitCallback(this.GetResponseCallbackHelper), result);
				return;
			}
			this.GetResponseCallbackHelper(result);
		}

		// Token: 0x06000A1F RID: 2591 RVA: 0x00017BBC File Offset: 0x00015DBC
		private void GetResponseCallbackHelper(object state)
		{
			IAsyncResult asyncResult = state as IAsyncResult;
			IAsyncExecutionContext asyncExecutionContext = null;
			IHttpRequest<TRequestContent> httpRequest = null;
			try
			{
				asyncExecutionContext = (asyncResult.AsyncState as IAsyncExecutionContext);
				httpRequest = (asyncExecutionContext.RuntimeState as IHttpRequest<TRequestContent>);
				IWebResponseData httpResponse = httpRequest.EndGetResponse(asyncResult);
				asyncExecutionContext.ResponseContext.HttpResponse = httpResponse;
			}
			catch (Exception exception)
			{
				asyncExecutionContext.ResponseContext.AsyncResult.Exception = exception;
			}
			finally
			{
				asyncExecutionContext.RequestContext.Metrics.StopEvent(Metric.HttpRequestTime);
				httpRequest.Dispose();
				base.InvokeAsyncCallback(asyncExecutionContext);
			}
		}

		// Token: 0x06000A20 RID: 2592 RVA: 0x00017C58 File Offset: 0x00015E58
		private static void SetMetrics(IRequestContext requestContext)
		{
			requestContext.Metrics.AddProperty(Metric.ServiceName, requestContext.Request.ServiceName);
			requestContext.Metrics.AddProperty(Metric.ServiceEndpoint, requestContext.Request.Endpoint);
			requestContext.Metrics.AddProperty(Metric.MethodName, requestContext.Request.RequestName);
		}

		// Token: 0x06000A21 RID: 2593 RVA: 0x00017CB0 File Offset: 0x00015EB0
		private void WriteContentToRequestBody(TRequestContent requestContent, IHttpRequest<TRequestContent> httpRequest, IRequestContext requestContext)
		{
			IRequest request = requestContext.Request;
			if (request.Content != null && request.Content.Length != 0)
			{
				byte[] content = request.Content;
				requestContext.Metrics.AddProperty(Metric.RequestSize, content.Length);
				httpRequest.WriteToRequestBody(requestContent, content, requestContext.Request.Headers);
				return;
			}
			Stream stream;
			if (request.ContentStream == null)
			{
				stream = new MemoryStream();
				stream.Write(request.Content, 0, request.Content.Length);
				stream.Position = 0L;
			}
			else
			{
				stream = request.ContentStream;
			}
			EventHandler<StreamTransferProgressArgs> streamUploadProgressCallback = ((IAmazonWebServiceRequest)request.OriginalRequest).StreamUploadProgressCallback;
			if (streamUploadProgressCallback != null)
			{
				stream = httpRequest.SetupProgressListeners(stream, requestContext.ClientConfig.ProgressUpdateInterval, this.CallbackSender, streamUploadProgressCallback);
			}
			Stream contentStream = (request.UseChunkEncoding && request.AWS4SignerResult != null) ? new ChunkedUploadWrapperStream(stream, requestContext.ClientConfig.BufferSize, request.AWS4SignerResult) : stream;
			httpRequest.WriteToRequestBody(requestContent, contentStream, requestContext.Request.Headers, requestContext);
		}

		// Token: 0x06000A22 RID: 2594 RVA: 0x00017DA4 File Offset: 0x00015FA4
		protected virtual IHttpRequest<TRequestContent> CreateWebRequest(IRequestContext requestContext)
		{
			IRequest request = requestContext.Request;
			Uri requestUri = AmazonServiceClient.ComposeUrl(request);
			IHttpRequest<TRequestContent> httpRequest = this._requestFactory.CreateHttpRequest(requestUri);
			httpRequest.ConfigureRequest(requestContext);
			httpRequest.Method = request.HttpMethod;
			if (request.MayContainRequestBody())
			{
				byte[] array = request.Content;
				if (request.SetContentFromParameters || (array == null && request.ContentStream == null))
				{
					if (!request.UseQueryString)
					{
						string parametersAsString = AWSSDKUtils.GetParametersAsString(request);
						array = Encoding.UTF8.GetBytes(parametersAsString);
						request.Content = array;
						request.SetContentFromParameters = true;
					}
					else
					{
						request.Content = new byte[0];
					}
				}
				if (array != null)
				{
					request.Headers["Content-Length"] = array.Length.ToString(CultureInfo.InvariantCulture);
					return httpRequest;
				}
				if (request.ContentStream != null && !request.Headers.ContainsKey("Content-Length"))
				{
					request.Headers["Content-Length"] = request.ContentStream.Length.ToString(CultureInfo.InvariantCulture);
				}
			}
			return httpRequest;
		}

		// Token: 0x06000A23 RID: 2595 RVA: 0x00017EA2 File Offset: 0x000160A2
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x06000A24 RID: 2596 RVA: 0x00017EB1 File Offset: 0x000160B1
		protected virtual void Dispose(bool disposing)
		{
			if (this._disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this._requestFactory != null)
				{
					this._requestFactory.Dispose();
				}
				this._disposed = true;
			}
		}

		// Token: 0x040003D6 RID: 982
		private bool _disposed;

		// Token: 0x040003D7 RID: 983
		private IHttpRequestFactory<TRequestContent> _requestFactory;
	}
}
