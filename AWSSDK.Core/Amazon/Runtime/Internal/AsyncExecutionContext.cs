﻿using System;
using Amazon.Runtime.Internal.Auth;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F5 RID: 245
	public class AsyncExecutionContext : IAsyncExecutionContext
	{
		// Token: 0x1700030B RID: 779
		// (get) Token: 0x060009A3 RID: 2467 RVA: 0x00016115 File Offset: 0x00014315
		// (set) Token: 0x060009A4 RID: 2468 RVA: 0x0001611D File Offset: 0x0001431D
		public IAsyncResponseContext ResponseContext { get; private set; }

		// Token: 0x1700030C RID: 780
		// (get) Token: 0x060009A5 RID: 2469 RVA: 0x00016126 File Offset: 0x00014326
		// (set) Token: 0x060009A6 RID: 2470 RVA: 0x0001612E File Offset: 0x0001432E
		public IAsyncRequestContext RequestContext { get; private set; }

		// Token: 0x1700030D RID: 781
		// (get) Token: 0x060009A7 RID: 2471 RVA: 0x00016137 File Offset: 0x00014337
		// (set) Token: 0x060009A8 RID: 2472 RVA: 0x0001613F File Offset: 0x0001433F
		public object RuntimeState { get; set; }

		// Token: 0x060009A9 RID: 2473 RVA: 0x00016148 File Offset: 0x00014348
		public AsyncExecutionContext(bool enableMetrics, AbstractAWSSigner clientSigner)
		{
			this.RequestContext = new AsyncRequestContext(enableMetrics, clientSigner);
			this.ResponseContext = new AsyncResponseContext();
		}

		// Token: 0x060009AA RID: 2474 RVA: 0x00016168 File Offset: 0x00014368
		public AsyncExecutionContext(IAsyncRequestContext requestContext, IAsyncResponseContext responseContext)
		{
			this.RequestContext = requestContext;
			this.ResponseContext = responseContext;
		}
	}
}
