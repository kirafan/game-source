﻿using System;
using Amazon.Runtime.Internal.Auth;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F4 RID: 244
	public class ExecutionContext : IExecutionContext
	{
		// Token: 0x17000309 RID: 777
		// (get) Token: 0x0600099C RID: 2460 RVA: 0x000160AA File Offset: 0x000142AA
		// (set) Token: 0x0600099D RID: 2461 RVA: 0x000160B2 File Offset: 0x000142B2
		public IRequestContext RequestContext { get; private set; }

		// Token: 0x1700030A RID: 778
		// (get) Token: 0x0600099E RID: 2462 RVA: 0x000160BB File Offset: 0x000142BB
		// (set) Token: 0x0600099F RID: 2463 RVA: 0x000160C3 File Offset: 0x000142C3
		public IResponseContext ResponseContext { get; private set; }

		// Token: 0x060009A0 RID: 2464 RVA: 0x000160CC File Offset: 0x000142CC
		public ExecutionContext(bool enableMetrics, AbstractAWSSigner clientSigner)
		{
			this.RequestContext = new RequestContext(enableMetrics, clientSigner);
			this.ResponseContext = new ResponseContext();
		}

		// Token: 0x060009A1 RID: 2465 RVA: 0x000160EC File Offset: 0x000142EC
		public ExecutionContext(IRequestContext requestContext, IResponseContext responseContext)
		{
			this.RequestContext = requestContext;
			this.ResponseContext = responseContext;
		}

		// Token: 0x060009A2 RID: 2466 RVA: 0x00016102 File Offset: 0x00014302
		public static IExecutionContext CreateFromAsyncContext(IAsyncExecutionContext asyncContext)
		{
			return new ExecutionContext(asyncContext.RequestContext, asyncContext.ResponseContext);
		}
	}
}
