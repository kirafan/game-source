﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000110 RID: 272
	public sealed class UnityRequestQueue
	{
		// Token: 0x06000AC4 RID: 2756 RVA: 0x00019365 File Offset: 0x00017565
		private UnityRequestQueue()
		{
		}

		// Token: 0x17000347 RID: 839
		// (get) Token: 0x06000AC5 RID: 2757 RVA: 0x0001938E File Offset: 0x0001758E
		public static UnityRequestQueue Instance
		{
			get
			{
				return UnityRequestQueue._instance;
			}
		}

		// Token: 0x06000AC6 RID: 2758 RVA: 0x00019398 File Offset: 0x00017598
		public void EnqueueRequest(IUnityHttpRequest request)
		{
			object requestsLock = UnityRequestQueue._requestsLock;
			lock (requestsLock)
			{
				this._requests.Enqueue(request);
			}
		}

		// Token: 0x06000AC7 RID: 2759 RVA: 0x000193D8 File Offset: 0x000175D8
		public IUnityHttpRequest DequeueRequest()
		{
			IUnityHttpRequest result = null;
			object requestsLock = UnityRequestQueue._requestsLock;
			lock (requestsLock)
			{
				if (this._requests.Count > 0)
				{
					result = this._requests.Dequeue();
				}
			}
			return result;
		}

		// Token: 0x06000AC8 RID: 2760 RVA: 0x00019428 File Offset: 0x00017628
		public void EnqueueCallback(RuntimeAsyncResult asyncResult)
		{
			object callbacksLock = UnityRequestQueue._callbacksLock;
			lock (callbacksLock)
			{
				this._callbacks.Enqueue(asyncResult);
			}
		}

		// Token: 0x06000AC9 RID: 2761 RVA: 0x00019468 File Offset: 0x00017668
		public RuntimeAsyncResult DequeueCallback()
		{
			RuntimeAsyncResult result = null;
			object callbacksLock = UnityRequestQueue._callbacksLock;
			lock (callbacksLock)
			{
				if (this._callbacks.Count > 0)
				{
					result = this._callbacks.Dequeue();
				}
			}
			return result;
		}

		// Token: 0x06000ACA RID: 2762 RVA: 0x000194B8 File Offset: 0x000176B8
		public void ExecuteOnMainThread(Action action)
		{
			object mainThreadCallbackLock = UnityRequestQueue._mainThreadCallbackLock;
			lock (mainThreadCallbackLock)
			{
				this._mainThreadCallbacks.Enqueue(action);
			}
		}

		// Token: 0x06000ACB RID: 2763 RVA: 0x000194F8 File Offset: 0x000176F8
		public Action DequeueMainThreadOperation()
		{
			Action result = null;
			object mainThreadCallbackLock = UnityRequestQueue._mainThreadCallbackLock;
			lock (mainThreadCallbackLock)
			{
				if (this._mainThreadCallbacks.Count > 0)
				{
					result = this._mainThreadCallbacks.Dequeue();
				}
			}
			return result;
		}

		// Token: 0x04000416 RID: 1046
		private static readonly UnityRequestQueue _instance = new UnityRequestQueue();

		// Token: 0x04000417 RID: 1047
		private static readonly object _requestsLock = new object();

		// Token: 0x04000418 RID: 1048
		private static readonly object _callbacksLock = new object();

		// Token: 0x04000419 RID: 1049
		private static readonly object _mainThreadCallbackLock = new object();

		// Token: 0x0400041A RID: 1050
		private Queue<IUnityHttpRequest> _requests = new Queue<IUnityHttpRequest>();

		// Token: 0x0400041B RID: 1051
		private Queue<RuntimeAsyncResult> _callbacks = new Queue<RuntimeAsyncResult>();

		// Token: 0x0400041C RID: 1052
		private Queue<Action> _mainThreadCallbacks = new Queue<Action>();
	}
}
