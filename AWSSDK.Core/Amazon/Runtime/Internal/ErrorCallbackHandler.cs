﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000FF RID: 255
	public class ErrorCallbackHandler : PipelineHandler
	{
		// Token: 0x17000319 RID: 793
		// (get) Token: 0x060009F7 RID: 2551 RVA: 0x00016FB9 File Offset: 0x000151B9
		// (set) Token: 0x060009F8 RID: 2552 RVA: 0x00016FC1 File Offset: 0x000151C1
		public Action<IExecutionContext, Exception> OnError { get; set; }

		// Token: 0x060009F9 RID: 2553 RVA: 0x00016FCC File Offset: 0x000151CC
		public override void InvokeSync(IExecutionContext executionContext)
		{
			try
			{
				base.InvokeSync(executionContext);
			}
			catch (Exception exception)
			{
				this.HandleException(executionContext, exception);
				throw;
			}
		}

		// Token: 0x060009FA RID: 2554 RVA: 0x00017000 File Offset: 0x00015200
		protected override void InvokeAsyncCallback(IAsyncExecutionContext executionContext)
		{
			Exception exception = executionContext.ResponseContext.AsyncResult.Exception;
			if (executionContext.ResponseContext.AsyncResult.Exception != null)
			{
				this.HandleException(ExecutionContext.CreateFromAsyncContext(executionContext), exception);
			}
			base.InvokeAsyncCallback(executionContext);
		}

		// Token: 0x060009FB RID: 2555 RVA: 0x00017044 File Offset: 0x00015244
		protected void HandleException(IExecutionContext executionContext, Exception exception)
		{
			this.OnError(executionContext, exception);
		}
	}
}
