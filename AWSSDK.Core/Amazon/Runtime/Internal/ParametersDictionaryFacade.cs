﻿using System;
using System.Collections;
using System.Collections.Generic;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000ED RID: 237
	public class ParametersDictionaryFacade : IDictionary<string, string>, ICollection<KeyValuePair<string, string>>, IEnumerable<KeyValuePair<string, string>>, IEnumerable
	{
		// Token: 0x06000954 RID: 2388 RVA: 0x00015865 File Offset: 0x00013A65
		public ParametersDictionaryFacade(ParameterCollection collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			this._parameterCollection = collection;
		}

		// Token: 0x06000955 RID: 2389 RVA: 0x00015884 File Offset: 0x00013A84
		private static string ParameterValueToString(ParameterValue pv)
		{
			if (pv == null)
			{
				throw new ArgumentNullException("pv");
			}
			StringParameterValue stringParameterValue = pv as StringParameterValue;
			StringListParameterValue stringListParameterValue = pv as StringListParameterValue;
			if (stringParameterValue != null)
			{
				return stringParameterValue.Value;
			}
			if (stringListParameterValue != null)
			{
				return JsonMapper.ToJson(stringListParameterValue.Value);
			}
			throw new AmazonClientException("Unexpected parameter value type " + pv.GetType().FullName);
		}

		// Token: 0x06000956 RID: 2390 RVA: 0x000158E0 File Offset: 0x00013AE0
		private static void UpdateParameterValue(ParameterValue pv, string newValue)
		{
			if (pv == null)
			{
				throw new ArgumentNullException("pv");
			}
			StringParameterValue stringParameterValue = pv as StringParameterValue;
			StringListParameterValue stringListParameterValue = pv as StringListParameterValue;
			if (stringParameterValue != null)
			{
				stringParameterValue.Value = newValue;
				return;
			}
			if (stringListParameterValue != null)
			{
				List<string> value = JsonMapper.ToObject<List<string>>(newValue);
				stringListParameterValue.Value = value;
				return;
			}
			throw new AmazonClientException("Unexpected parameter value type " + pv.GetType().FullName);
		}

		// Token: 0x06000957 RID: 2391 RVA: 0x00015940 File Offset: 0x00013B40
		public void Add(string key, string value)
		{
			this._parameterCollection.Add(key, value);
		}

		// Token: 0x170002F1 RID: 753
		// (get) Token: 0x06000958 RID: 2392 RVA: 0x0001594F File Offset: 0x00013B4F
		public int Count
		{
			get
			{
				return this._parameterCollection.Count;
			}
		}

		// Token: 0x06000959 RID: 2393 RVA: 0x0001595C File Offset: 0x00013B5C
		public bool ContainsKey(string key)
		{
			return this._parameterCollection.ContainsKey(key);
		}

		// Token: 0x0600095A RID: 2394 RVA: 0x0001596A File Offset: 0x00013B6A
		public bool Remove(string key)
		{
			return this._parameterCollection.Remove(key);
		}

		// Token: 0x170002F2 RID: 754
		public string this[string key]
		{
			get
			{
				return ParametersDictionaryFacade.ParameterValueToString(this._parameterCollection[key]);
			}
			set
			{
				ParameterValue parameterValue;
				if (this._parameterCollection.TryGetValue(key, out parameterValue))
				{
					ParametersDictionaryFacade.UpdateParameterValue(parameterValue, value);
				}
				else
				{
					parameterValue = new StringParameterValue(value);
				}
				this._parameterCollection[key] = parameterValue;
			}
		}

		// Token: 0x170002F3 RID: 755
		// (get) Token: 0x0600095D RID: 2397 RVA: 0x000159C6 File Offset: 0x00013BC6
		public ICollection<string> Keys
		{
			get
			{
				return this._parameterCollection.Keys;
			}
		}

		// Token: 0x0600095E RID: 2398 RVA: 0x000159D4 File Offset: 0x00013BD4
		public bool TryGetValue(string key, out string value)
		{
			ParameterValue pv;
			if (this._parameterCollection.TryGetValue(key, out pv))
			{
				value = ParametersDictionaryFacade.ParameterValueToString(pv);
				return true;
			}
			value = null;
			return false;
		}

		// Token: 0x0600095F RID: 2399 RVA: 0x000159FF File Offset: 0x00013BFF
		public bool Remove(KeyValuePair<string, string> item)
		{
			return this.Contains(item) && this._parameterCollection.Remove(item.Key);
		}

		// Token: 0x170002F4 RID: 756
		// (get) Token: 0x06000960 RID: 2400 RVA: 0x00015A20 File Offset: 0x00013C20
		public ICollection<string> Values
		{
			get
			{
				List<string> list = new List<string>();
				foreach (KeyValuePair<string, ParameterValue> keyValuePair in this._parameterCollection)
				{
					string item = ParametersDictionaryFacade.ParameterValueToString(keyValuePair.Value);
					list.Add(item);
				}
				return list;
			}
		}

		// Token: 0x06000961 RID: 2401 RVA: 0x00015A88 File Offset: 0x00013C88
		public void Add(KeyValuePair<string, string> item)
		{
			StringParameterValue value = new StringParameterValue(item.Value);
			this._parameterCollection.Add(item.Key, value);
		}

		// Token: 0x06000962 RID: 2402 RVA: 0x00015AB8 File Offset: 0x00013CB8
		public bool Contains(KeyValuePair<string, string> item)
		{
			string key = item.Key;
			string value = item.Value;
			ParameterValue pv;
			return this._parameterCollection.TryGetValue(key, out pv) && string.Equals(ParametersDictionaryFacade.ParameterValueToString(pv), value, StringComparison.Ordinal);
		}

		// Token: 0x06000963 RID: 2403 RVA: 0x00015AF4 File Offset: 0x00013CF4
		public void CopyTo(KeyValuePair<string, string>[] array, int arrayIndex)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (arrayIndex < 0 || arrayIndex > array.Length)
			{
				throw new ArgumentOutOfRangeException("arrayIndex");
			}
			if (array.Length - arrayIndex < this._parameterCollection.Count)
			{
				throw new ArgumentOutOfRangeException("arrayIndex", "Not enough space in target array");
			}
			foreach (KeyValuePair<string, string> keyValuePair in this)
			{
				array[arrayIndex++] = keyValuePair;
			}
		}

		// Token: 0x170002F5 RID: 757
		// (get) Token: 0x06000964 RID: 2404 RVA: 0x00015B88 File Offset: 0x00013D88
		public bool IsReadOnly
		{
			get
			{
				return ((IDictionary)this._parameterCollection).IsReadOnly;
			}
		}

		// Token: 0x06000965 RID: 2405 RVA: 0x00015B95 File Offset: 0x00013D95
		public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
		{
			foreach (KeyValuePair<string, ParameterValue> keyValuePair in this._parameterCollection)
			{
				string key = keyValuePair.Key;
				string value = ParametersDictionaryFacade.ParameterValueToString(keyValuePair.Value);
				KeyValuePair<string, string> keyValuePair2 = new KeyValuePair<string, string>(key, value);
				yield return keyValuePair2;
			}
			SortedDictionary<string, ParameterValue>.Enumerator enumerator = default(SortedDictionary<string, ParameterValue>.Enumerator);
			yield break;
			yield break;
		}

		// Token: 0x06000966 RID: 2406 RVA: 0x00015BA4 File Offset: 0x00013DA4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06000967 RID: 2407 RVA: 0x00015BAC File Offset: 0x00013DAC
		public void Clear()
		{
			this._parameterCollection.Clear();
		}

		// Token: 0x040003A5 RID: 933
		private readonly ParameterCollection _parameterCollection;
	}
}
