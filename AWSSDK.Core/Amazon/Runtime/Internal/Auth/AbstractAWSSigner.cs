﻿using System;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x02000166 RID: 358
	public abstract class AbstractAWSSigner
	{
		// Token: 0x170003EC RID: 1004
		// (get) Token: 0x06000D54 RID: 3412 RVA: 0x0001F3CC File Offset: 0x0001D5CC
		private AWS4Signer AWS4SignerInstance
		{
			get
			{
				if (this._aws4Signer == null)
				{
					lock (this)
					{
						if (this._aws4Signer == null)
						{
							this._aws4Signer = new AWS4Signer();
						}
					}
				}
				return this._aws4Signer;
			}
		}

		// Token: 0x06000D55 RID: 3413 RVA: 0x0001F41C File Offset: 0x0001D61C
		protected static string ComputeHash(string data, string secretkey, SigningAlgorithm algorithm)
		{
			string result;
			try
			{
				result = CryptoUtilFactory.CryptoInstance.HMACSign(data, secretkey, algorithm);
			}
			catch (Exception ex)
			{
				throw new SignatureException("Failed to generate signature: " + ex.Message, ex);
			}
			return result;
		}

		// Token: 0x06000D56 RID: 3414 RVA: 0x0001F464 File Offset: 0x0001D664
		protected static string ComputeHash(byte[] data, string secretkey, SigningAlgorithm algorithm)
		{
			string result;
			try
			{
				result = CryptoUtilFactory.CryptoInstance.HMACSign(data, secretkey, algorithm);
			}
			catch (Exception ex)
			{
				throw new SignatureException("Failed to generate signature: " + ex.Message, ex);
			}
			return result;
		}

		// Token: 0x06000D57 RID: 3415
		public abstract void Sign(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey);

		// Token: 0x170003ED RID: 1005
		// (get) Token: 0x06000D58 RID: 3416
		public abstract ClientProtocol Protocol { get; }

		// Token: 0x06000D59 RID: 3417 RVA: 0x0001F4AC File Offset: 0x0001D6AC
		protected static bool UseV4Signing(bool useSigV4Setting, IRequest request, IClientConfig config)
		{
			if (useSigV4Setting || request.UseSigV4 || config.SignatureVersion == "4")
			{
				return true;
			}
			RegionEndpoint regionEndpoint = null;
			if (!string.IsNullOrEmpty(request.AuthenticationRegion))
			{
				regionEndpoint = RegionEndpoint.GetBySystemName(request.AuthenticationRegion);
			}
			if (regionEndpoint == null && !string.IsNullOrEmpty(config.ServiceURL))
			{
				string text = AWSSDKUtils.DetermineRegion(config.ServiceURL);
				if (!string.IsNullOrEmpty(text))
				{
					regionEndpoint = RegionEndpoint.GetBySystemName(text);
				}
			}
			if (regionEndpoint == null && config.RegionEndpoint != null)
			{
				regionEndpoint = config.RegionEndpoint;
			}
			if (regionEndpoint != null)
			{
				RegionEndpoint.Endpoint endpointForService = regionEndpoint.GetEndpointForService(config.RegionEndpointServiceName, config.UseDualstackEndpoint);
				if (endpointForService != null && (endpointForService.SignatureVersionOverride == "4" || string.IsNullOrEmpty(endpointForService.SignatureVersionOverride)))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000D5A RID: 3418 RVA: 0x0001F56B File Offset: 0x0001D76B
		protected AbstractAWSSigner SelectSigner(IRequest request, IClientConfig config)
		{
			return this.SelectSigner(this, false, request, config);
		}

		// Token: 0x06000D5B RID: 3419 RVA: 0x0001F577 File Offset: 0x0001D777
		protected AbstractAWSSigner SelectSigner(AbstractAWSSigner defaultSigner, bool useSigV4Setting, IRequest request, IClientConfig config)
		{
			if (AbstractAWSSigner.UseV4Signing(useSigV4Setting, request, config))
			{
				return this.AWS4SignerInstance;
			}
			return defaultSigner;
		}

		// Token: 0x040004EF RID: 1263
		private AWS4Signer _aws4Signer;
	}
}
