﻿using System;
using System.Collections.Generic;
using System.Text;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x02000167 RID: 359
	public class AWS3Signer : AbstractAWSSigner
	{
		// Token: 0x170003EE RID: 1006
		// (get) Token: 0x06000D5D RID: 3421 RVA: 0x0001F58C File Offset: 0x0001D78C
		// (set) Token: 0x06000D5E RID: 3422 RVA: 0x0001F594 File Offset: 0x0001D794
		private bool UseAws3Https { get; set; }

		// Token: 0x06000D5F RID: 3423 RVA: 0x0001F59D File Offset: 0x0001D79D
		public AWS3Signer(bool useAws3Https)
		{
			this.UseAws3Https = useAws3Https;
		}

		// Token: 0x06000D60 RID: 3424 RVA: 0x0001F5AC File Offset: 0x0001D7AC
		public AWS3Signer() : this(false)
		{
		}

		// Token: 0x170003EF RID: 1007
		// (get) Token: 0x06000D61 RID: 3425 RVA: 0x00011C11 File Offset: 0x0000FE11
		public override ClientProtocol Protocol
		{
			get
			{
				return ClientProtocol.RestProtocol;
			}
		}

		// Token: 0x06000D62 RID: 3426 RVA: 0x0001F5B8 File Offset: 0x0001D7B8
		public override void Sign(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
			AbstractAWSSigner abstractAWSSigner = base.SelectSigner(request, clientConfig);
			if (abstractAWSSigner is AWS4Signer)
			{
				abstractAWSSigner.Sign(request, clientConfig, metrics, awsAccessKeyId, awsSecretAccessKey);
				return;
			}
			if (this.UseAws3Https)
			{
				AWS3Signer.SignHttps(request, clientConfig, metrics, awsAccessKeyId, awsSecretAccessKey);
				return;
			}
			AWS3Signer.SignHttp(request, metrics, awsAccessKeyId, awsSecretAccessKey);
		}

		// Token: 0x06000D63 RID: 3427 RVA: 0x0001F608 File Offset: 0x0001D808
		private static void SignHttps(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
			string text = Guid.NewGuid().ToString();
			string formattedCurrentTimestampRFC = AWSSDKUtils.FormattedCurrentTimestampRFC822;
			string text2 = formattedCurrentTimestampRFC + text;
			metrics.AddProperty(Metric.StringToSign, text2);
			string str = AbstractAWSSigner.ComputeHash(text2, awsSecretAccessKey, clientConfig.SignatureMethod);
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("AWS3-HTTPS").Append(" ");
			stringBuilder.Append("AWSAccessKeyId=" + awsAccessKeyId + ",");
			stringBuilder.Append("Algorithm=" + clientConfig.SignatureMethod.ToString() + ",");
			stringBuilder.Append("SignedHeaders=x-amz-date;x-amz-nonce,");
			stringBuilder.Append("Signature=" + str);
			request.Headers["X-Amzn-Authorization"] = stringBuilder.ToString();
			request.Headers["x-amz-nonce"] = text;
			request.Headers["X-Amz-Date"] = formattedCurrentTimestampRFC;
		}

		// Token: 0x06000D64 RID: 3428 RVA: 0x0001F70C File Offset: 0x0001D90C
		private static void SignHttp(IRequest request, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
			SigningAlgorithm algorithm = SigningAlgorithm.HmacSHA256;
			string text = Guid.NewGuid().ToString();
			string formattedCurrentTimestampRFC = AWSSDKUtils.FormattedCurrentTimestampRFC822;
			bool flag = AWS3Signer.IsHttpsRequest(request);
			flag = false;
			request.Headers["Date"] = formattedCurrentTimestampRFC;
			request.Headers["X-Amz-Date"] = formattedCurrentTimestampRFC;
			request.Headers.Remove("X-Amzn-Authorization");
			string text2 = request.Endpoint.Host;
			if (!request.Endpoint.IsDefaultPort)
			{
				text2 = text2 + ":" + request.Endpoint.Port;
			}
			request.Headers["host"] = text2;
			string text3;
			byte[] data;
			if (flag)
			{
				request.Headers["x-amz-nonce"] = text;
				text3 = formattedCurrentTimestampRFC + text;
				data = Encoding.UTF8.GetBytes(text3);
			}
			else
			{
				Uri endpoint = request.Endpoint;
				if (!string.IsNullOrEmpty(request.ResourcePath))
				{
					endpoint = new Uri(request.Endpoint, request.ResourcePath);
				}
				text3 = string.Concat(new string[]
				{
					request.HttpMethod,
					"\n",
					AWS3Signer.GetCanonicalizedResourcePath(endpoint),
					"\n",
					AWS3Signer.GetCanonicalizedQueryString(request.Parameters),
					"\n",
					AWS3Signer.GetCanonicalizedHeadersForStringToSign(request),
					"\n",
					AWS3Signer.GetRequestPayload(request)
				});
				data = CryptoUtilFactory.CryptoInstance.ComputeSHA256Hash(Encoding.UTF8.GetBytes(text3));
			}
			metrics.AddProperty(Metric.StringToSign, text3);
			string str = AbstractAWSSigner.ComputeHash(data, awsSecretAccessKey, algorithm);
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append(flag ? "AWS3-HTTPS" : "AWS3");
			stringBuilder.Append(" ");
			stringBuilder.Append("AWSAccessKeyId=" + awsAccessKeyId + ",");
			stringBuilder.Append("Algorithm=" + algorithm.ToString() + ",");
			if (!flag)
			{
				stringBuilder.Append(AWS3Signer.GetSignedHeadersComponent(request) + ",");
			}
			stringBuilder.Append("Signature=" + str);
			string value = stringBuilder.ToString();
			request.Headers["X-Amzn-Authorization"] = value;
		}

		// Token: 0x06000D65 RID: 3429 RVA: 0x0001F958 File Offset: 0x0001DB58
		private static string GetCanonicalizedResourcePath(Uri endpoint)
		{
			string absolutePath = endpoint.AbsolutePath;
			if (string.IsNullOrEmpty(absolutePath))
			{
				return "/";
			}
			return AWSSDKUtils.UrlEncode(absolutePath, true);
		}

		// Token: 0x06000D66 RID: 3430 RVA: 0x0001F984 File Offset: 0x0001DB84
		private static bool IsHttpsRequest(IRequest request)
		{
			string scheme = request.Endpoint.Scheme;
			if (scheme.Equals("http", StringComparison.OrdinalIgnoreCase))
			{
				return false;
			}
			if (scheme.Equals("https", StringComparison.OrdinalIgnoreCase))
			{
				return true;
			}
			throw new AmazonServiceException("Unknown request endpoint protocol encountered while signing request: " + scheme);
		}

		// Token: 0x06000D67 RID: 3431 RVA: 0x0001F9D0 File Offset: 0x0001DBD0
		private static string GetCanonicalizedQueryString(IDictionary<string, string> parameters)
		{
			IEnumerable<KeyValuePair<string, string>> enumerable = new SortedDictionary<string, string>(parameters, StringComparer.Ordinal);
			StringBuilder stringBuilder = new StringBuilder();
			foreach (KeyValuePair<string, string> keyValuePair in enumerable)
			{
				if (keyValuePair.Value != null)
				{
					string key = keyValuePair.Key;
					string value = keyValuePair.Value;
					stringBuilder.Append(AWSSDKUtils.UrlEncode(key, false));
					stringBuilder.Append("=");
					stringBuilder.Append(AWSSDKUtils.UrlEncode(value, false));
					stringBuilder.Append("&");
				}
			}
			string text = stringBuilder.ToString();
			if (!string.IsNullOrEmpty(text))
			{
				return text.Substring(0, text.Length - 1);
			}
			return string.Empty;
		}

		// Token: 0x06000D68 RID: 3432 RVA: 0x0001FA98 File Offset: 0x0001DC98
		private static string GetRequestPayload(IRequest request)
		{
			if (request.Content == null)
			{
				return string.Empty;
			}
			return Encoding.UTF8.GetString(request.Content, 0, request.Content.Length);
		}

		// Token: 0x06000D69 RID: 3433 RVA: 0x0001FAC4 File Offset: 0x0001DCC4
		private static string GetSignedHeadersComponent(IRequest request)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.Append("SignedHeaders=");
			bool flag = true;
			foreach (string value in AWS3Signer.GetHeadersForStringToSign(request))
			{
				if (!flag)
				{
					stringBuilder.Append(";");
				}
				stringBuilder.Append(value);
				flag = false;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000D6A RID: 3434 RVA: 0x0001FB44 File Offset: 0x0001DD44
		private static List<string> GetHeadersForStringToSign(IRequest request)
		{
			List<string> list = new List<string>();
			foreach (KeyValuePair<string, string> keyValuePair in request.Headers)
			{
				string key = keyValuePair.Key;
				if (key.StartsWith("x-amz", StringComparison.OrdinalIgnoreCase) || key.Equals("content-encoding", StringComparison.OrdinalIgnoreCase) || key.Equals("host", StringComparison.OrdinalIgnoreCase))
				{
					list.Add(key);
				}
			}
			list.Sort(StringComparer.OrdinalIgnoreCase);
			return list;
		}

		// Token: 0x06000D6B RID: 3435 RVA: 0x0001FBD8 File Offset: 0x0001DDD8
		private static string GetCanonicalizedHeadersForStringToSign(IRequest request)
		{
			List<string> headersForStringToSign = AWS3Signer.GetHeadersForStringToSign(request);
			for (int i = 0; i < headersForStringToSign.Count; i++)
			{
				headersForStringToSign[i] = headersForStringToSign[i].ToLowerInvariant();
			}
			SortedDictionary<string, string> sortedDictionary = new SortedDictionary<string, string>();
			foreach (KeyValuePair<string, string> keyValuePair in request.Headers)
			{
				if (headersForStringToSign.Contains(keyValuePair.Key.ToLowerInvariant()))
				{
					sortedDictionary[keyValuePair.Key] = keyValuePair.Value;
				}
			}
			StringBuilder stringBuilder = new StringBuilder();
			foreach (KeyValuePair<string, string> keyValuePair2 in sortedDictionary)
			{
				stringBuilder.Append(keyValuePair2.Key.ToLowerInvariant());
				stringBuilder.Append(":");
				stringBuilder.Append(keyValuePair2.Value);
				stringBuilder.Append("\n");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x040004F0 RID: 1264
		private const string HTTP_SCHEME = "AWS3";

		// Token: 0x040004F1 RID: 1265
		private const string HTTPS_SCHEME = "AWS3-HTTPS";
	}
}
