﻿using System;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x02000168 RID: 360
	internal class AWS3HTTPSigner : AWS3Signer
	{
		// Token: 0x06000D6C RID: 3436 RVA: 0x0001F5AC File Offset: 0x0001D7AC
		public AWS3HTTPSigner() : base(false)
		{
		}
	}
}
