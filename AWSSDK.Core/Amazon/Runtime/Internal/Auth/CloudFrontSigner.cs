﻿using System;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x0200016C RID: 364
	public class CloudFrontSigner : AbstractAWSSigner
	{
		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x06000DA2 RID: 3490 RVA: 0x00011C11 File Offset: 0x0000FE11
		public override ClientProtocol Protocol
		{
			get
			{
				return ClientProtocol.RestProtocol;
			}
		}

		// Token: 0x06000DA3 RID: 3491 RVA: 0x00020E70 File Offset: 0x0001F070
		public override void Sign(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
			if (string.IsNullOrEmpty(awsAccessKeyId))
			{
				throw new ArgumentOutOfRangeException("awsAccessKeyId", "The AWS Access Key ID cannot be NULL or a Zero length string");
			}
			string formattedTimestampRFC = AWSSDKUtils.GetFormattedTimestampRFC822(0);
			request.Headers.Add("X-Amz-Date", formattedTimestampRFC);
			string str = AbstractAWSSigner.ComputeHash(formattedTimestampRFC, awsSecretAccessKey, SigningAlgorithm.HmacSHA1);
			request.Headers.Add("Authorization", "AWS " + awsAccessKeyId + ":" + str);
		}
	}
}
