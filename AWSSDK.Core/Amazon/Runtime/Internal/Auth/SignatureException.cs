﻿using System;
using System.Runtime.Serialization;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x0200016F RID: 367
	[Serializable]
	public class SignatureException : Exception
	{
		// Token: 0x06000DAB RID: 3499 RVA: 0x00008CC5 File Offset: 0x00006EC5
		public SignatureException(string message) : base(message)
		{
		}

		// Token: 0x06000DAC RID: 3500 RVA: 0x00008CCE File Offset: 0x00006ECE
		public SignatureException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000DAD RID: 3501 RVA: 0x00011B48 File Offset: 0x0000FD48
		protected SignatureException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
