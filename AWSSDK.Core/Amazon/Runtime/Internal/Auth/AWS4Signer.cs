﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x02000169 RID: 361
	public class AWS4Signer : AbstractAWSSigner
	{
		// Token: 0x06000D6D RID: 3437 RVA: 0x0001FCFC File Offset: 0x0001DEFC
		public AWS4Signer() : this(true)
		{
		}

		// Token: 0x06000D6E RID: 3438 RVA: 0x0001FD05 File Offset: 0x0001DF05
		public AWS4Signer(bool signPayload)
		{
			this.SignPayload = signPayload;
		}

		// Token: 0x170003F0 RID: 1008
		// (get) Token: 0x06000D6F RID: 3439 RVA: 0x0001FD14 File Offset: 0x0001DF14
		// (set) Token: 0x06000D70 RID: 3440 RVA: 0x0001FD1C File Offset: 0x0001DF1C
		public bool SignPayload { get; private set; }

		// Token: 0x170003F1 RID: 1009
		// (get) Token: 0x06000D71 RID: 3441 RVA: 0x00011C11 File Offset: 0x0000FE11
		public override ClientProtocol Protocol
		{
			get
			{
				return ClientProtocol.RestProtocol;
			}
		}

		// Token: 0x06000D72 RID: 3442 RVA: 0x0001FD28 File Offset: 0x0001DF28
		public override void Sign(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
			AWS4SigningResult aws4SigningResult = this.SignRequest(request, clientConfig, metrics, awsAccessKeyId, awsSecretAccessKey);
			request.Headers["Authorization"] = aws4SigningResult.ForAuthorizationHeader;
		}

		// Token: 0x06000D73 RID: 3443 RVA: 0x0001FD5C File Offset: 0x0001DF5C
		public AWS4SigningResult SignRequest(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
			DateTime signedAt = AWS4Signer.InitializeHeaders(request.Headers, request.Endpoint);
			string text = AWS4Signer.DetermineService(clientConfig);
			string region = AWS4Signer.DetermineSigningRegion(clientConfig, text, request.AlternateEndpoint, request);
			string canonicalQueryString = AWS4Signer.CanonicalizeQueryParameters(AWS4Signer.GetParametersToCanonicalize(request));
			string precomputedBodyHash = AWS4Signer.SetRequestBodyHash(request, this.SignPayload);
			IDictionary<string, string> sortedHeaders = AWS4Signer.SortAndPruneHeaders(request.Headers);
			string text2 = AWS4Signer.CanonicalizeRequest(request.Endpoint, request.ResourcePath, request.HttpMethod, sortedHeaders, canonicalQueryString, precomputedBodyHash);
			if (metrics != null)
			{
				metrics.AddProperty(Metric.CanonicalRequest, text2);
			}
			return AWS4Signer.ComputeSignature(awsAccessKeyId, awsSecretAccessKey, region, signedAt, text, AWS4Signer.CanonicalizeHeaderNames(sortedHeaders), text2, metrics);
		}

		// Token: 0x06000D74 RID: 3444 RVA: 0x0001FDF9 File Offset: 0x0001DFF9
		public static DateTime InitializeHeaders(IDictionary<string, string> headers, Uri requestEndpoint)
		{
			return AWS4Signer.InitializeHeaders(headers, requestEndpoint, AWSSDKUtils.CorrectedUtcNow);
		}

		// Token: 0x06000D75 RID: 3445 RVA: 0x0001FE08 File Offset: 0x0001E008
		public static DateTime InitializeHeaders(IDictionary<string, string> headers, Uri requestEndpoint, DateTime requestDateTime)
		{
			AWS4Signer.CleanHeaders(headers);
			if (!headers.ContainsKey("host"))
			{
				string text = requestEndpoint.Host;
				if (!requestEndpoint.IsDefaultPort)
				{
					text = text + ":" + requestEndpoint.Port;
				}
				headers.Add("host", text);
			}
			DateTime result = requestDateTime;
			headers["X-Amz-Date"] = result.ToString("yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture);
			return result;
		}

		// Token: 0x06000D76 RID: 3446 RVA: 0x0001FE7C File Offset: 0x0001E07C
		private static void CleanHeaders(IDictionary<string, string> headers)
		{
			headers.Remove("Authorization");
			headers.Remove("X-Amz-Content-SHA256");
			if (headers.ContainsKey("X-Amz-Decoded-Content-Length"))
			{
				headers["Content-Length"] = headers["X-Amz-Decoded-Content-Length"];
				headers.Remove("X-Amz-Decoded-Content-Length");
			}
		}

		// Token: 0x06000D77 RID: 3447 RVA: 0x0001FED0 File Offset: 0x0001E0D0
		public static AWS4SigningResult ComputeSignature(ImmutableCredentials credentials, string region, DateTime signedAt, string service, string signedHeaders, string canonicalRequest)
		{
			return AWS4Signer.ComputeSignature(credentials.AccessKey, credentials.SecretKey, region, signedAt, service, signedHeaders, canonicalRequest);
		}

		// Token: 0x06000D78 RID: 3448 RVA: 0x0001FEEA File Offset: 0x0001E0EA
		public static AWS4SigningResult ComputeSignature(string awsAccessKey, string awsSecretAccessKey, string region, DateTime signedAt, string service, string signedHeaders, string canonicalRequest)
		{
			return AWS4Signer.ComputeSignature(awsAccessKey, awsSecretAccessKey, region, signedAt, service, signedHeaders, canonicalRequest, null);
		}

		// Token: 0x06000D79 RID: 3449 RVA: 0x0001FEFC File Offset: 0x0001E0FC
		public static AWS4SigningResult ComputeSignature(string awsAccessKey, string awsSecretAccessKey, string region, DateTime signedAt, string service, string signedHeaders, string canonicalRequest, RequestMetrics metrics)
		{
			string text = AWS4Signer.FormatDateTime(signedAt, "yyyyMMdd");
			string text2 = string.Format(CultureInfo.InvariantCulture, "{0}/{1}/{2}/{3}", new object[]
			{
				text,
				region,
				service,
				"aws4_request"
			});
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat(CultureInfo.InvariantCulture, "{0}-{1}\n{2}\n{3}\n", new object[]
			{
				"AWS4",
				"HMAC-SHA256",
				AWS4Signer.FormatDateTime(signedAt, "yyyyMMddTHHmmssZ"),
				text2
			});
			byte[] data = AWS4Signer.ComputeHash(canonicalRequest);
			stringBuilder.Append(AWSSDKUtils.ToHex(data, true));
			if (metrics != null)
			{
				metrics.AddProperty(Metric.StringToSign, stringBuilder);
			}
			byte[] array = AWS4Signer.ComposeSigningKey(awsSecretAccessKey, region, text, service);
			string data2 = stringBuilder.ToString();
			byte[] signature = AWS4Signer.ComputeKeyedHash(SigningAlgorithm.HmacSHA256, array, data2);
			return new AWS4SigningResult(awsAccessKey, signedAt, signedHeaders, text2, array, signature);
		}

		// Token: 0x06000D7A RID: 3450 RVA: 0x0001FFD1 File Offset: 0x0001E1D1
		public static string FormatDateTime(DateTime dt, string formatString)
		{
			return dt.ToString(formatString, CultureInfo.InvariantCulture);
		}

		// Token: 0x06000D7B RID: 3451 RVA: 0x0001FFE0 File Offset: 0x0001E1E0
		public static byte[] ComposeSigningKey(string awsSecretAccessKey, string region, string date, string service)
		{
			char[] array = null;
			byte[] result;
			try
			{
				array = ("AWS4" + awsSecretAccessKey).ToCharArray();
				byte[] key = AWS4Signer.ComputeKeyedHash(SigningAlgorithm.HmacSHA256, Encoding.UTF8.GetBytes(array), Encoding.UTF8.GetBytes(date));
				byte[] key2 = AWS4Signer.ComputeKeyedHash(SigningAlgorithm.HmacSHA256, key, Encoding.UTF8.GetBytes(region));
				byte[] key3 = AWS4Signer.ComputeKeyedHash(SigningAlgorithm.HmacSHA256, key2, Encoding.UTF8.GetBytes(service));
				result = AWS4Signer.ComputeKeyedHash(SigningAlgorithm.HmacSHA256, key3, AWS4Signer.TerminatorBytes);
			}
			finally
			{
				if (array != null)
				{
					Array.Clear(array, 0, array.Length);
				}
			}
			return result;
		}

		// Token: 0x06000D7C RID: 3452 RVA: 0x00020074 File Offset: 0x0001E274
		public static string SetRequestBodyHash(IRequest request)
		{
			return AWS4Signer.SetRequestBodyHash(request, true);
		}

		// Token: 0x06000D7D RID: 3453 RVA: 0x00020080 File Offset: 0x0001E280
		public static string SetRequestBodyHash(IRequest request, bool signPayload)
		{
			if (!signPayload)
			{
				return AWS4Signer.SetPayloadSignatureHeader(request, "UNSIGNED-PAYLOAD");
			}
			string text = null;
			if (request.Headers.TryGetValue("X-Amz-Content-SHA256", out text) && !request.UseChunkEncoding)
			{
				return text;
			}
			if (request.UseChunkEncoding)
			{
				text = "STREAMING-AWS4-HMAC-SHA256-PAYLOAD";
				if (request.Headers.ContainsKey("Content-Length"))
				{
					request.Headers["X-Amz-Decoded-Content-Length"] = request.Headers["Content-Length"];
					long originalLength = long.Parse(request.Headers["Content-Length"], CultureInfo.InvariantCulture);
					request.Headers["Content-Length"] = ChunkedUploadWrapperStream.ComputeChunkedContentLength(originalLength).ToString(CultureInfo.InvariantCulture);
				}
				if (request.Headers.ContainsKey("Content-Encoding"))
				{
					string text2 = request.Headers["Content-Encoding"];
					if (!text2.Contains("aws-chunked"))
					{
						request.Headers["Content-Encoding"] = text2 + ", " + "aws-chunked";
					}
				}
			}
			else if (request.ContentStream != null)
			{
				text = request.ComputeContentStreamHash();
			}
			else
			{
				byte[] requestPayloadBytes = AWS4Signer.GetRequestPayloadBytes(request);
				text = AWSSDKUtils.ToHex(CryptoUtilFactory.CryptoInstance.ComputeSHA256Hash(requestPayloadBytes), true);
			}
			if (text == null)
			{
				return null;
			}
			return AWS4Signer.SetPayloadSignatureHeader(request, text);
		}

		// Token: 0x06000D7E RID: 3454 RVA: 0x000201C7 File Offset: 0x0001E3C7
		public static byte[] SignBlob(byte[] key, string data)
		{
			return AWS4Signer.SignBlob(key, Encoding.UTF8.GetBytes(data));
		}

		// Token: 0x06000D7F RID: 3455 RVA: 0x000201DA File Offset: 0x0001E3DA
		public static byte[] SignBlob(byte[] key, byte[] data)
		{
			return CryptoUtilFactory.CryptoInstance.HMACSignBinary(data, key, SigningAlgorithm.HmacSHA256);
		}

		// Token: 0x06000D80 RID: 3456 RVA: 0x000201E9 File Offset: 0x0001E3E9
		public static byte[] ComputeKeyedHash(SigningAlgorithm algorithm, byte[] key, string data)
		{
			return AWS4Signer.ComputeKeyedHash(algorithm, key, Encoding.UTF8.GetBytes(data));
		}

		// Token: 0x06000D81 RID: 3457 RVA: 0x000201FD File Offset: 0x0001E3FD
		public static byte[] ComputeKeyedHash(SigningAlgorithm algorithm, byte[] key, byte[] data)
		{
			return CryptoUtilFactory.CryptoInstance.HMACSignBinary(data, key, algorithm);
		}

		// Token: 0x06000D82 RID: 3458 RVA: 0x0002020C File Offset: 0x0001E40C
		public static byte[] ComputeHash(string data)
		{
			return AWS4Signer.ComputeHash(Encoding.UTF8.GetBytes(data));
		}

		// Token: 0x06000D83 RID: 3459 RVA: 0x0002021E File Offset: 0x0001E41E
		public static byte[] ComputeHash(byte[] data)
		{
			return CryptoUtilFactory.CryptoInstance.ComputeSHA256Hash(data);
		}

		// Token: 0x06000D84 RID: 3460 RVA: 0x0002022B File Offset: 0x0001E42B
		private static string SetPayloadSignatureHeader(IRequest request, string payloadHash)
		{
			if (request.Headers.ContainsKey("X-Amz-Content-SHA256"))
			{
				request.Headers["X-Amz-Content-SHA256"] = payloadHash;
			}
			else
			{
				request.Headers.Add("X-Amz-Content-SHA256", payloadHash);
			}
			return payloadHash;
		}

		// Token: 0x06000D85 RID: 3461 RVA: 0x00020264 File Offset: 0x0001E464
		public static string DetermineSigningRegion(IClientConfig clientConfig, string serviceName, RegionEndpoint alternateEndpoint, IRequest request)
		{
			if (alternateEndpoint != null)
			{
				RegionEndpoint.Endpoint endpointForService = alternateEndpoint.GetEndpointForService(serviceName, clientConfig.UseDualstackEndpoint);
				if (endpointForService.AuthRegion != null)
				{
					return endpointForService.AuthRegion;
				}
				return alternateEndpoint.SystemName;
			}
			else
			{
				string authenticationRegion = clientConfig.AuthenticationRegion;
				if (request != null && request.AuthenticationRegion != null)
				{
					authenticationRegion = request.AuthenticationRegion;
				}
				if (!string.IsNullOrEmpty(authenticationRegion))
				{
					return authenticationRegion.ToLowerInvariant();
				}
				if (!string.IsNullOrEmpty(clientConfig.ServiceURL))
				{
					string text = AWSSDKUtils.DetermineRegion(clientConfig.ServiceURL);
					if (!string.IsNullOrEmpty(text))
					{
						return text.ToLowerInvariant();
					}
				}
				RegionEndpoint regionEndpoint = clientConfig.RegionEndpoint;
				if (regionEndpoint == null)
				{
					return string.Empty;
				}
				RegionEndpoint.Endpoint endpointForService2 = regionEndpoint.GetEndpointForService(serviceName, clientConfig.UseDualstackEndpoint);
				if (!string.IsNullOrEmpty(endpointForService2.AuthRegion))
				{
					return endpointForService2.AuthRegion;
				}
				return regionEndpoint.SystemName;
			}
		}

		// Token: 0x06000D86 RID: 3462 RVA: 0x00020323 File Offset: 0x0001E523
		internal static string DetermineService(IClientConfig clientConfig)
		{
			if (string.IsNullOrEmpty(clientConfig.AuthenticationServiceName))
			{
				return AWSSDKUtils.DetermineService(clientConfig.DetermineServiceURL());
			}
			return clientConfig.AuthenticationServiceName;
		}

		// Token: 0x06000D87 RID: 3463 RVA: 0x00020344 File Offset: 0x0001E544
		protected static string CanonicalizeRequest(Uri endpoint, string resourcePath, string httpMethod, IDictionary<string, string> sortedHeaders, string canonicalQueryString, string precomputedBodyHash)
		{
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("{0}\n", httpMethod);
			stringBuilder.AppendFormat("{0}\n", AWSSDKUtils.CanonicalizeResourcePath(endpoint, resourcePath));
			stringBuilder.AppendFormat("{0}\n", canonicalQueryString);
			stringBuilder.AppendFormat("{0}\n", AWS4Signer.CanonicalizeHeaders(sortedHeaders));
			stringBuilder.AppendFormat("{0}\n", AWS4Signer.CanonicalizeHeaderNames(sortedHeaders));
			string value;
			if (precomputedBodyHash != null)
			{
				stringBuilder.Append(precomputedBodyHash);
			}
			else if (sortedHeaders.TryGetValue("X-Amz-Content-SHA256", out value))
			{
				stringBuilder.Append(value);
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000D88 RID: 3464 RVA: 0x000203D8 File Offset: 0x0001E5D8
		protected static IDictionary<string, string> SortAndPruneHeaders(IEnumerable<KeyValuePair<string, string>> requestHeaders)
		{
			SortedDictionary<string, string> sortedDictionary = new SortedDictionary<string, string>(StringComparer.OrdinalIgnoreCase);
			foreach (KeyValuePair<string, string> keyValuePair in requestHeaders)
			{
				if (!AWS4Signer._headersToIgnoreWhenSigning.Contains(keyValuePair.Key))
				{
					sortedDictionary.Add(keyValuePair.Key, keyValuePair.Value);
				}
			}
			return sortedDictionary;
		}

		// Token: 0x06000D89 RID: 3465 RVA: 0x0002044C File Offset: 0x0001E64C
		protected static string CanonicalizeHeaders(IEnumerable<KeyValuePair<string, string>> sortedHeaders)
		{
			if (sortedHeaders == null || sortedHeaders.Count<KeyValuePair<string, string>>() == 0)
			{
				return string.Empty;
			}
			StringBuilder stringBuilder = new StringBuilder();
			if (AWSSDKUtils.IsIL2CPP)
			{
				foreach (KeyValuePair<string, string> keyValuePair in from kvp in sortedHeaders
				orderby kvp.Key.ToLowerInvariant()
				select kvp)
				{
					stringBuilder.Append(keyValuePair.Key.ToLowerInvariant());
					stringBuilder.Append(":");
					stringBuilder.Append(AWS4Signer.CompressSpaces(keyValuePair.Value));
					stringBuilder.Append("\n");
				}
				return stringBuilder.ToString();
			}
			foreach (KeyValuePair<string, string> keyValuePair2 in sortedHeaders)
			{
				stringBuilder.Append(keyValuePair2.Key.ToLowerInvariant());
				stringBuilder.Append(":");
				stringBuilder.Append(AWS4Signer.CompressSpaces(keyValuePair2.Value));
				stringBuilder.Append("\n");
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000D8A RID: 3466 RVA: 0x0002058C File Offset: 0x0001E78C
		protected static string CanonicalizeHeaderNames(IEnumerable<KeyValuePair<string, string>> sortedHeaders)
		{
			StringBuilder stringBuilder = new StringBuilder();
			if (AWSSDKUtils.IsIL2CPP)
			{
				foreach (KeyValuePair<string, string> keyValuePair in from kvp in sortedHeaders
				orderby kvp.Key.ToLowerInvariant()
				select kvp)
				{
					if (stringBuilder.Length > 0)
					{
						stringBuilder.Append(";");
					}
					stringBuilder.Append(keyValuePair.Key.ToLowerInvariant());
				}
				return stringBuilder.ToString();
			}
			foreach (KeyValuePair<string, string> keyValuePair2 in sortedHeaders)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(";");
				}
				stringBuilder.Append(keyValuePair2.Key.ToLowerInvariant());
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000D8B RID: 3467 RVA: 0x0002068C File Offset: 0x0001E88C
		protected static List<KeyValuePair<string, string>> GetParametersToCanonicalize(IRequest request)
		{
			List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
			if (request.SubResources != null && request.SubResources.Count > 0)
			{
				foreach (KeyValuePair<string, string> keyValuePair in request.SubResources)
				{
					list.Add(new KeyValuePair<string, string>(keyValuePair.Key, keyValuePair.Value));
				}
			}
			if (request.UseQueryString && request.Parameters != null && request.Parameters.Count > 0)
			{
				foreach (KeyValuePair<string, string> keyValuePair2 in from queryParameter in request.ParameterCollection.GetSortedParametersList()
				where queryParameter.Value != null
				select queryParameter)
				{
					list.Add(new KeyValuePair<string, string>(keyValuePair2.Key, keyValuePair2.Value));
				}
			}
			return list;
		}

		// Token: 0x06000D8C RID: 3468 RVA: 0x000207A0 File Offset: 0x0001E9A0
		protected static string CanonicalizeQueryParameters(string queryString)
		{
			return AWS4Signer.CanonicalizeQueryParameters(queryString, true);
		}

		// Token: 0x06000D8D RID: 3469 RVA: 0x000207AC File Offset: 0x0001E9AC
		protected static string CanonicalizeQueryParameters(string queryString, bool uriEncodeParameters)
		{
			if (string.IsNullOrEmpty(queryString))
			{
				return string.Empty;
			}
			Dictionary<string, string> dictionary = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
			int num = queryString.IndexOf('?');
			string text = queryString.Substring(num + 1);
			int num2 = 0;
			int num3 = text.IndexOfAny(new char[]
			{
				'&',
				';'
			}, 0);
			if (num3 == -1 && num2 < text.Length)
			{
				num3 = text.Length;
			}
			while (num3 != -1)
			{
				string text2 = text.Substring(num2, num3 - num2);
				if (num3 + 1 >= text.Length || text[num3 + 1] != ' ')
				{
					int num4 = text2.IndexOf('=');
					if (num4 == -1)
					{
						dictionary.Add(text2, null);
					}
					else
					{
						dictionary.Add(text2.Substring(0, num4), text2.Substring(num4 + 1));
					}
					num2 = num3 + 1;
				}
				if (text.Length <= num3 + 1)
				{
					break;
				}
				num3 = text.IndexOfAny(new char[]
				{
					'&',
					';'
				}, num3 + 1);
				if (num3 == -1 && num2 < text.Length)
				{
					num3 = text.Length;
				}
			}
			return AWS4Signer.CanonicalizeQueryParameters(dictionary, uriEncodeParameters);
		}

		// Token: 0x06000D8E RID: 3470 RVA: 0x000208D2 File Offset: 0x0001EAD2
		protected static string CanonicalizeQueryParameters(IEnumerable<KeyValuePair<string, string>> parameters)
		{
			return AWS4Signer.CanonicalizeQueryParameters(parameters, true);
		}

		// Token: 0x06000D8F RID: 3471 RVA: 0x000208DC File Offset: 0x0001EADC
		protected static string CanonicalizeQueryParameters(IEnumerable<KeyValuePair<string, string>> parameters, bool uriEncodeParameters)
		{
			if (parameters == null)
			{
				return string.Empty;
			}
			List<KeyValuePair<string, string>> list = parameters.OrderBy((KeyValuePair<string, string> kvp) => kvp.Key, StringComparer.Ordinal).ToList<KeyValuePair<string, string>>();
			StringBuilder stringBuilder = new StringBuilder();
			foreach (KeyValuePair<string, string> keyValuePair in list)
			{
				string key = keyValuePair.Key;
				string value = keyValuePair.Value;
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append("&");
				}
				if (uriEncodeParameters)
				{
					if (string.IsNullOrEmpty(value))
					{
						stringBuilder.AppendFormat("{0}=", AWSSDKUtils.UrlEncode(key, false));
					}
					else
					{
						stringBuilder.AppendFormat("{0}={1}", AWSSDKUtils.UrlEncode(key, false), AWSSDKUtils.UrlEncode(value, false));
					}
				}
				else if (string.IsNullOrEmpty(value))
				{
					stringBuilder.AppendFormat("{0}=", key);
				}
				else
				{
					stringBuilder.AppendFormat("{0}={1}", key, value);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000D90 RID: 3472 RVA: 0x000209F8 File Offset: 0x0001EBF8
		private static string CompressSpaces(string data)
		{
			if (data == null || !data.Contains(" "))
			{
				return data;
			}
			return AWS4Signer.CompressWhitespaceRegex.Replace(data, " ");
		}

		// Token: 0x06000D91 RID: 3473 RVA: 0x00020A1C File Offset: 0x0001EC1C
		private static byte[] GetRequestPayloadBytes(IRequest request)
		{
			if (request.Content != null)
			{
				return request.Content;
			}
			string s = request.UseQueryString ? string.Empty : AWSSDKUtils.GetParametersAsString(request);
			return Encoding.UTF8.GetBytes(s);
		}

		// Token: 0x040004F3 RID: 1267
		public const string Scheme = "AWS4";

		// Token: 0x040004F4 RID: 1268
		public const string Algorithm = "HMAC-SHA256";

		// Token: 0x040004F5 RID: 1269
		public const string AWS4AlgorithmTag = "AWS4-HMAC-SHA256";

		// Token: 0x040004F6 RID: 1270
		public const string Terminator = "aws4_request";

		// Token: 0x040004F7 RID: 1271
		public static readonly byte[] TerminatorBytes = Encoding.UTF8.GetBytes("aws4_request");

		// Token: 0x040004F8 RID: 1272
		public const string Credential = "Credential";

		// Token: 0x040004F9 RID: 1273
		public const string SignedHeaders = "SignedHeaders";

		// Token: 0x040004FA RID: 1274
		public const string Signature = "Signature";

		// Token: 0x040004FB RID: 1275
		public const string EmptyBodySha256 = "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855";

		// Token: 0x040004FC RID: 1276
		public const string StreamingBodySha256 = "STREAMING-AWS4-HMAC-SHA256-PAYLOAD";

		// Token: 0x040004FD RID: 1277
		public const string AWSChunkedEncoding = "aws-chunked";

		// Token: 0x040004FE RID: 1278
		public const string UnsignedPayload = "UNSIGNED-PAYLOAD";

		// Token: 0x040004FF RID: 1279
		private static readonly Regex CompressWhitespaceRegex = new Regex("\\s+");

		// Token: 0x04000500 RID: 1280
		private const SigningAlgorithm SignerAlgorithm = SigningAlgorithm.HmacSHA256;

		// Token: 0x04000501 RID: 1281
		private static IEnumerable<string> _headersToIgnoreWhenSigning = new HashSet<string>
		{
			" x-amzn-trace-id"
		};
	}
}
