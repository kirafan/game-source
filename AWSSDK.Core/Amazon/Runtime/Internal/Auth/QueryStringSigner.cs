﻿using System;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x0200016E RID: 366
	public class QueryStringSigner : AbstractAWSSigner
	{
		// Token: 0x170003FE RID: 1022
		// (get) Token: 0x06000DA9 RID: 3497 RVA: 0x000129CA File Offset: 0x00010BCA
		public override ClientProtocol Protocol
		{
			get
			{
				return ClientProtocol.QueryStringProtocol;
			}
		}

		// Token: 0x06000DAA RID: 3498 RVA: 0x00020EE8 File Offset: 0x0001F0E8
		public override void Sign(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
			if (string.IsNullOrEmpty(awsAccessKeyId))
			{
				throw new ArgumentOutOfRangeException("awsAccessKeyId", "The AWS Access Key ID cannot be NULL or a Zero length string");
			}
			request.Parameters["AWSAccessKeyId"] = awsAccessKeyId;
			request.Parameters["SignatureVersion"] = clientConfig.SignatureVersion;
			request.Parameters["SignatureMethod"] = clientConfig.SignatureMethod.ToString();
			request.Parameters["Timestamp"] = AWSSDKUtils.FormattedCurrentTimestampISO8601;
			request.Parameters.Remove("Signature");
			string text = AWSSDKUtils.CalculateStringToSignV2(request.ParameterCollection, request.Endpoint.AbsoluteUri);
			metrics.AddProperty(Metric.StringToSign, text);
			string value = AbstractAWSSigner.ComputeHash(text, awsSecretAccessKey, clientConfig.SignatureMethod);
			request.Parameters["Signature"] = value;
		}
	}
}
