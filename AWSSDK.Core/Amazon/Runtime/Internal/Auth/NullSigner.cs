﻿using System;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x0200016D RID: 365
	public class NullSigner : AbstractAWSSigner
	{
		// Token: 0x06000DA5 RID: 3493 RVA: 0x00005805 File Offset: 0x00003A05
		public override void Sign(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
		}

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x06000DA6 RID: 3494 RVA: 0x00020EE2 File Offset: 0x0001F0E2
		public override ClientProtocol Protocol
		{
			get
			{
				return ClientProtocol.Unknown;
			}
		}
	}
}
