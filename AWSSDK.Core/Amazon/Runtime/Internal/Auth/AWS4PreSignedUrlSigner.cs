﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x0200016A RID: 362
	public class AWS4PreSignedUrlSigner : AWS4Signer
	{
		// Token: 0x06000D93 RID: 3475 RVA: 0x00020A94 File Offset: 0x0001EC94
		public override void Sign(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
			throw new InvalidOperationException("PreSignedUrl signature computation is not supported by this method; use SignRequest instead.");
		}

		// Token: 0x06000D94 RID: 3476 RVA: 0x00020AA0 File Offset: 0x0001ECA0
		public new AWS4SigningResult SignRequest(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey)
		{
			return AWS4PreSignedUrlSigner.SignRequest(request, clientConfig, metrics, awsAccessKeyId, awsSecretAccessKey, "s3", null);
		}

		// Token: 0x06000D95 RID: 3477 RVA: 0x00020AB4 File Offset: 0x0001ECB4
		public static AWS4SigningResult SignRequest(IRequest request, IClientConfig clientConfig, RequestMetrics metrics, string awsAccessKeyId, string awsSecretAccessKey, string service, string overrideSigningRegion)
		{
			request.Headers.Remove("Authorization");
			if (!request.Headers.ContainsKey("host"))
			{
				string text = request.Endpoint.Host;
				if (!request.Endpoint.IsDefaultPort)
				{
					text = text + ":" + request.Endpoint.Port;
				}
				request.Headers.Add("host", text);
			}
			DateTime correctedUtcNow = AWSSDKUtils.CorrectedUtcNow;
			string text2 = overrideSigningRegion ?? AWS4Signer.DetermineSigningRegion(clientConfig, service, request.AlternateEndpoint, request);
			if (request.Headers.ContainsKey("X-Amz-Content-SHA256"))
			{
				request.Headers.Remove("X-Amz-Content-SHA256");
			}
			IDictionary<string, string> sortedHeaders = AWS4Signer.SortAndPruneHeaders(request.Headers);
			string text3 = AWS4Signer.CanonicalizeHeaderNames(sortedHeaders);
			List<KeyValuePair<string, string>> parametersToCanonicalize = AWS4Signer.GetParametersToCanonicalize(request);
			parametersToCanonicalize.Add(new KeyValuePair<string, string>("X-Amz-Algorithm", "AWS4-HMAC-SHA256"));
			string value = string.Format(CultureInfo.InvariantCulture, "{0}/{1}/{2}/{3}/{4}", new object[]
			{
				awsAccessKeyId,
				AWS4Signer.FormatDateTime(correctedUtcNow, "yyyyMMdd"),
				text2,
				service,
				"aws4_request"
			});
			parametersToCanonicalize.Add(new KeyValuePair<string, string>("X-Amz-Credential", value));
			parametersToCanonicalize.Add(new KeyValuePair<string, string>("X-Amz-Date", AWS4Signer.FormatDateTime(correctedUtcNow, "yyyyMMddTHHmmssZ")));
			parametersToCanonicalize.Add(new KeyValuePair<string, string>("X-Amz-SignedHeaders", text3));
			string canonicalQueryString = AWS4Signer.CanonicalizeQueryParameters(parametersToCanonicalize);
			string text4 = AWS4Signer.CanonicalizeRequest(request.Endpoint, request.ResourcePath, request.HttpMethod, sortedHeaders, canonicalQueryString, (service == "s3") ? "UNSIGNED-PAYLOAD" : "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855");
			if (metrics != null)
			{
				metrics.AddProperty(Metric.CanonicalRequest, text4);
			}
			return AWS4Signer.ComputeSignature(awsAccessKeyId, awsSecretAccessKey, text2, correctedUtcNow, service, text3, text4, metrics);
		}

		// Token: 0x04000503 RID: 1283
		public const long MaxAWS4PreSignedUrlExpiry = 604800L;

		// Token: 0x04000504 RID: 1284
		internal const string XAmzSignature = "X-Amz-Signature";

		// Token: 0x04000505 RID: 1285
		internal const string XAmzAlgorithm = "X-Amz-Algorithm";

		// Token: 0x04000506 RID: 1286
		internal const string XAmzCredential = "X-Amz-Credential";

		// Token: 0x04000507 RID: 1287
		internal const string XAmzExpires = "X-Amz-Expires";
	}
}
