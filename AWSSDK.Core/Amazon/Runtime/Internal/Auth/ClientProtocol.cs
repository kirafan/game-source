﻿using System;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x02000165 RID: 357
	public enum ClientProtocol
	{
		// Token: 0x040004EC RID: 1260
		QueryStringProtocol,
		// Token: 0x040004ED RID: 1261
		RestProtocol,
		// Token: 0x040004EE RID: 1262
		Unknown
	}
}
