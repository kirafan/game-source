﻿using System;
using System.Globalization;
using System.Text;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Auth
{
	// Token: 0x0200016B RID: 363
	public class AWS4SigningResult
	{
		// Token: 0x06000D97 RID: 3479 RVA: 0x00020C7D File Offset: 0x0001EE7D
		public AWS4SigningResult(string awsAccessKeyId, DateTime signedAt, string signedHeaders, string scope, byte[] signingKey, byte[] signature)
		{
			this._awsAccessKeyId = awsAccessKeyId;
			this._originalDateTime = signedAt;
			this._signedHeaders = signedHeaders;
			this._scope = scope;
			this._signingKey = signingKey;
			this._signature = signature;
		}

		// Token: 0x170003F2 RID: 1010
		// (get) Token: 0x06000D98 RID: 3480 RVA: 0x00020CB2 File Offset: 0x0001EEB2
		public string AccessKeyId
		{
			get
			{
				return this._awsAccessKeyId;
			}
		}

		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x06000D99 RID: 3481 RVA: 0x00020CBA File Offset: 0x0001EEBA
		public string ISO8601DateTime
		{
			get
			{
				return AWS4Signer.FormatDateTime(this._originalDateTime, "yyyyMMddTHHmmssZ");
			}
		}

		// Token: 0x170003F4 RID: 1012
		// (get) Token: 0x06000D9A RID: 3482 RVA: 0x00020CCC File Offset: 0x0001EECC
		public string ISO8601Date
		{
			get
			{
				return AWS4Signer.FormatDateTime(this._originalDateTime, "yyyyMMdd");
			}
		}

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x06000D9B RID: 3483 RVA: 0x00020CDE File Offset: 0x0001EEDE
		public string SignedHeaders
		{
			get
			{
				return this._signedHeaders;
			}
		}

		// Token: 0x170003F6 RID: 1014
		// (get) Token: 0x06000D9C RID: 3484 RVA: 0x00020CE6 File Offset: 0x0001EEE6
		public string Scope
		{
			get
			{
				return this._scope;
			}
		}

		// Token: 0x170003F7 RID: 1015
		// (get) Token: 0x06000D9D RID: 3485 RVA: 0x00020CF0 File Offset: 0x0001EEF0
		public byte[] SigningKey
		{
			get
			{
				byte[] array = new byte[this._signingKey.Length];
				this._signingKey.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x170003F8 RID: 1016
		// (get) Token: 0x06000D9E RID: 3486 RVA: 0x00020D19 File Offset: 0x0001EF19
		public string Signature
		{
			get
			{
				return AWSSDKUtils.ToHex(this._signature, true);
			}
		}

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x06000D9F RID: 3487 RVA: 0x00020D28 File Offset: 0x0001EF28
		public byte[] SignatureBytes
		{
			get
			{
				byte[] array = new byte[this._signature.Length];
				this._signature.CopyTo(array, 0);
				return array;
			}
		}

		// Token: 0x170003FA RID: 1018
		// (get) Token: 0x06000DA0 RID: 3488 RVA: 0x00020D54 File Offset: 0x0001EF54
		public string ForAuthorizationHeader
		{
			get
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.Append("AWS4-HMAC-SHA256");
				stringBuilder.AppendFormat(" {0}={1}/{2},", "Credential", this.AccessKeyId, this.Scope);
				stringBuilder.AppendFormat(" {0}={1},", "SignedHeaders", this.SignedHeaders);
				stringBuilder.AppendFormat(" {0}={1}", "Signature", this.Signature);
				return stringBuilder.ToString();
			}
		}

		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x06000DA1 RID: 3489 RVA: 0x00020DC4 File Offset: 0x0001EFC4
		public string ForQueryParameters
		{
			get
			{
				StringBuilder stringBuilder = new StringBuilder();
				stringBuilder.AppendFormat("{0}={1}", "X-Amz-Algorithm", "AWS4-HMAC-SHA256");
				stringBuilder.AppendFormat("&{0}={1}", "X-Amz-Credential", string.Format(CultureInfo.InvariantCulture, "{0}/{1}", new object[]
				{
					this.AccessKeyId,
					this.Scope
				}));
				stringBuilder.AppendFormat("&{0}={1}", "X-Amz-Date", this.ISO8601DateTime);
				stringBuilder.AppendFormat("&{0}={1}", "X-Amz-SignedHeaders", this.SignedHeaders);
				stringBuilder.AppendFormat("&{0}={1}", "X-Amz-Signature", this.Signature);
				return stringBuilder.ToString();
			}
		}

		// Token: 0x04000508 RID: 1288
		private readonly string _awsAccessKeyId;

		// Token: 0x04000509 RID: 1289
		private readonly DateTime _originalDateTime;

		// Token: 0x0400050A RID: 1290
		private readonly string _signedHeaders;

		// Token: 0x0400050B RID: 1291
		private readonly string _scope;

		// Token: 0x0400050C RID: 1292
		private readonly byte[] _signingKey;

		// Token: 0x0400050D RID: 1293
		private readonly byte[] _signature;
	}
}
