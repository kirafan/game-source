﻿using System;
using System.Globalization;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000100 RID: 256
	public class Marshaller : PipelineHandler
	{
		// Token: 0x060009FD RID: 2557 RVA: 0x00017053 File Offset: 0x00015253
		public override void InvokeSync(IExecutionContext executionContext)
		{
			Marshaller.PreInvoke(executionContext);
			base.InvokeSync(executionContext);
		}

		// Token: 0x060009FE RID: 2558 RVA: 0x00017062 File Offset: 0x00015262
		public override IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			Marshaller.PreInvoke(ExecutionContext.CreateFromAsyncContext(executionContext));
			return base.InvokeAsync(executionContext);
		}

		// Token: 0x060009FF RID: 2559 RVA: 0x00017078 File Offset: 0x00015278
		protected static void PreInvoke(IExecutionContext executionContext)
		{
			IRequestContext requestContext = executionContext.RequestContext;
			requestContext.Request = requestContext.Marshaller.Marshall(requestContext.OriginalRequest);
			requestContext.Request.AuthenticationRegion = requestContext.ClientConfig.AuthenticationRegion;
			if (AWSConfigs.HttpClient == AWSConfigs.HttpClientOption.UnityWWW)
			{
				requestContext.Request.Headers["User-Agent"] = requestContext.ClientConfig.UserAgent + " " + (executionContext.RequestContext.IsAsync ? "ClientAsync" : "ClientSync") + " UnityWWW";
			}
			else
			{
				requestContext.Request.Headers["x-amz-user-agent"] = requestContext.ClientConfig.UserAgent + " " + (executionContext.RequestContext.IsAsync ? "ClientAsync" : "ClientSync") + " UnityWebRequest";
			}
			string a = requestContext.Request.HttpMethod.ToUpper(CultureInfo.InvariantCulture);
			if (a != "GET" && a != "DELETE" && a != "HEAD" && !requestContext.Request.Headers.ContainsKey("Content-Type"))
			{
				if (requestContext.Request.UseQueryString)
				{
					requestContext.Request.Headers["Content-Type"] = "application/x-amz-json-1.0";
					return;
				}
				requestContext.Request.Headers["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";
			}
		}
	}
}
