﻿using System;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F2 RID: 242
	public class ResponseContext : IResponseContext
	{
		// Token: 0x17000306 RID: 774
		// (get) Token: 0x06000994 RID: 2452 RVA: 0x0001606F File Offset: 0x0001426F
		// (set) Token: 0x06000995 RID: 2453 RVA: 0x00016077 File Offset: 0x00014277
		public AmazonWebServiceResponse Response { get; set; }

		// Token: 0x17000307 RID: 775
		// (get) Token: 0x06000996 RID: 2454 RVA: 0x00016080 File Offset: 0x00014280
		// (set) Token: 0x06000997 RID: 2455 RVA: 0x00016088 File Offset: 0x00014288
		public IWebResponseData HttpResponse { get; set; }
	}
}
