﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000113 RID: 275
	public class UploadHandlerRawWrapper : IDisposable
	{
		// Token: 0x17000355 RID: 853
		// (get) Token: 0x06000AE9 RID: 2793 RVA: 0x00019A8C File Offset: 0x00017C8C
		// (set) Token: 0x06000AEA RID: 2794 RVA: 0x00019A94 File Offset: 0x00017C94
		public object Instance { get; private set; }

		// Token: 0x06000AEB RID: 2795 RVA: 0x00019A9D File Offset: 0x00017C9D
		static UploadHandlerRawWrapper()
		{
			if (UploadHandlerRawWrapper.uploadHandlerRawType == null)
			{
				UploadHandlerRawWrapper.uploadHandlerRawType = Type.GetType("UnityEngine.Experimental.Networking.UploadHandlerRaw, UnityEngine");
			}
		}

		// Token: 0x06000AEC RID: 2796 RVA: 0x00019AC4 File Offset: 0x00017CC4
		public UploadHandlerRawWrapper(byte[] data)
		{
			this.Instance = Activator.CreateInstance(UploadHandlerRawWrapper.uploadHandlerRawType, new object[]
			{
				data
			});
		}

		// Token: 0x06000AED RID: 2797 RVA: 0x00019AE8 File Offset: 0x00017CE8
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposedValue)
			{
				if (disposing)
				{
					IDisposable disposable = this.Instance as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				this.Instance = null;
				this.disposedValue = true;
			}
		}

		// Token: 0x06000AEE RID: 2798 RVA: 0x00019B23 File Offset: 0x00017D23
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x04000437 RID: 1079
		private static Type uploadHandlerRawType = Type.GetType("UnityEngine.Networking.UploadHandlerRaw, UnityEngine");

		// Token: 0x04000439 RID: 1081
		private bool disposedValue;
	}
}
