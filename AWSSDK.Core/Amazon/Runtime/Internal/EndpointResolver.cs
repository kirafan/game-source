﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000FE RID: 254
	public class EndpointResolver : PipelineHandler
	{
		// Token: 0x060009F1 RID: 2545 RVA: 0x00016F09 File Offset: 0x00015109
		public override void InvokeSync(IExecutionContext executionContext)
		{
			this.PreInvoke(executionContext);
			base.InvokeSync(executionContext);
		}

		// Token: 0x060009F2 RID: 2546 RVA: 0x00016F19 File Offset: 0x00015119
		public override IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			this.PreInvoke(ExecutionContext.CreateFromAsyncContext(executionContext));
			return base.InvokeAsync(executionContext);
		}

		// Token: 0x060009F3 RID: 2547 RVA: 0x00016F30 File Offset: 0x00015130
		protected void PreInvoke(IExecutionContext executionContext)
		{
			IRequestContext requestContext = executionContext.RequestContext;
			if (requestContext.Request.Endpoint == null)
			{
				requestContext.Request.Endpoint = this.DetermineEndpoint(executionContext.RequestContext);
			}
		}

		// Token: 0x060009F4 RID: 2548 RVA: 0x00016F6E File Offset: 0x0001516E
		public virtual Uri DetermineEndpoint(IRequestContext requestContext)
		{
			return EndpointResolver.DetermineEndpoint(requestContext.ClientConfig, requestContext.Request);
		}

		// Token: 0x060009F5 RID: 2549 RVA: 0x00016F81 File Offset: 0x00015181
		public static Uri DetermineEndpoint(IClientConfig config, IRequest request)
		{
			if (request.AlternateEndpoint == null)
			{
				return new Uri(config.DetermineServiceURL());
			}
			return new Uri(ClientConfig.GetUrl(request.AlternateEndpoint, config.RegionEndpointServiceName, config.UseHttp, config.UseDualstackEndpoint));
		}
	}
}
