﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F8 RID: 248
	public class RuntimePipeline : IDisposable
	{
		// Token: 0x17000312 RID: 786
		// (get) Token: 0x060009BE RID: 2494 RVA: 0x000163CE File Offset: 0x000145CE
		public IPipelineHandler Handler
		{
			get
			{
				return this._handler;
			}
		}

		// Token: 0x060009BF RID: 2495 RVA: 0x000163D8 File Offset: 0x000145D8
		public RuntimePipeline(IPipelineHandler handler)
		{
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}
			Logger logger = Logger.GetLogger(typeof(RuntimePipeline));
			this._handler = handler;
			this._handler.Logger = logger;
			this._logger = logger;
		}

		// Token: 0x060009C0 RID: 2496 RVA: 0x00016423 File Offset: 0x00014623
		public RuntimePipeline(IList<IPipelineHandler> handlers) : this(handlers, Logger.GetLogger(typeof(RuntimePipeline)))
		{
		}

		// Token: 0x060009C1 RID: 2497 RVA: 0x0001643C File Offset: 0x0001463C
		public RuntimePipeline(IList<IPipelineHandler> handlers, ILogger logger)
		{
			if (handlers == null || handlers.Count == 0)
			{
				throw new ArgumentNullException("handlers");
			}
			if (logger == null)
			{
				throw new ArgumentNullException("logger");
			}
			this._logger = logger;
			foreach (IPipelineHandler handler in handlers)
			{
				this.AddHandler(handler);
			}
		}

		// Token: 0x060009C2 RID: 2498 RVA: 0x000164B8 File Offset: 0x000146B8
		public RuntimePipeline(IPipelineHandler handler, ILogger logger)
		{
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}
			if (logger == null)
			{
				throw new ArgumentNullException("logger");
			}
			this._handler = handler;
			this._handler.Logger = logger;
			this._logger = logger;
		}

		// Token: 0x060009C3 RID: 2499 RVA: 0x000164F6 File Offset: 0x000146F6
		public IResponseContext InvokeSync(IExecutionContext executionContext)
		{
			this.ThrowIfDisposed();
			this._handler.InvokeSync(executionContext);
			return executionContext.ResponseContext;
		}

		// Token: 0x060009C4 RID: 2500 RVA: 0x00016510 File Offset: 0x00014710
		public IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			this.ThrowIfDisposed();
			return this._handler.InvokeAsync(executionContext);
		}

		// Token: 0x060009C5 RID: 2501 RVA: 0x00016524 File Offset: 0x00014724
		public void AddHandler(IPipelineHandler handler)
		{
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}
			this.ThrowIfDisposed();
			IPipelineHandler innermostHandler = RuntimePipeline.GetInnermostHandler(handler);
			if (this._handler != null)
			{
				innermostHandler.InnerHandler = this._handler;
				this._handler.OuterHandler = innermostHandler;
			}
			this._handler = handler;
			this.SetHandlerProperties(handler);
		}

		// Token: 0x060009C6 RID: 2502 RVA: 0x0001657C File Offset: 0x0001477C
		public void AddHandlerAfter<T>(IPipelineHandler handler) where T : IPipelineHandler
		{
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}
			this.ThrowIfDisposed();
			Type typeFromHandle = typeof(T);
			for (IPipelineHandler pipelineHandler = this._handler; pipelineHandler != null; pipelineHandler = pipelineHandler.InnerHandler)
			{
				if (pipelineHandler.GetType() == typeFromHandle)
				{
					RuntimePipeline.InsertHandler(handler, pipelineHandler);
					this.SetHandlerProperties(handler);
					return;
				}
			}
			throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Cannot find a handler of type {0}", new object[]
			{
				typeFromHandle.Name
			}));
		}

		// Token: 0x060009C7 RID: 2503 RVA: 0x000165F8 File Offset: 0x000147F8
		public void AddHandlerBefore<T>(IPipelineHandler handler) where T : IPipelineHandler
		{
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}
			this.ThrowIfDisposed();
			Type typeFromHandle = typeof(T);
			if (this._handler.GetType() == typeFromHandle)
			{
				this.AddHandler(handler);
				this.SetHandlerProperties(handler);
				return;
			}
			for (IPipelineHandler pipelineHandler = this._handler; pipelineHandler != null; pipelineHandler = pipelineHandler.InnerHandler)
			{
				if (pipelineHandler.InnerHandler != null && pipelineHandler.InnerHandler.GetType() == typeFromHandle)
				{
					RuntimePipeline.InsertHandler(handler, pipelineHandler);
					this.SetHandlerProperties(handler);
					return;
				}
			}
			throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Cannot find a handler of type {0}", new object[]
			{
				typeFromHandle.Name
			}));
		}

		// Token: 0x060009C8 RID: 2504 RVA: 0x0001669C File Offset: 0x0001489C
		public void RemoveHandler<T>()
		{
			this.ThrowIfDisposed();
			Type typeFromHandle = typeof(T);
			IPipelineHandler pipelineHandler = this._handler;
			while (pipelineHandler != null)
			{
				if (pipelineHandler.GetType() == typeFromHandle)
				{
					if (pipelineHandler == this._handler && this._handler.InnerHandler == null)
					{
						throw new InvalidOperationException("The pipeline contains a single handler, cannot remove the only handler in the pipeline.");
					}
					if (pipelineHandler == this._handler)
					{
						this._handler = pipelineHandler.InnerHandler;
					}
					if (pipelineHandler.OuterHandler != null)
					{
						pipelineHandler.OuterHandler.InnerHandler = pipelineHandler.InnerHandler;
					}
					if (pipelineHandler.InnerHandler != null)
					{
						pipelineHandler.InnerHandler.OuterHandler = pipelineHandler.OuterHandler;
					}
					pipelineHandler.InnerHandler = null;
					pipelineHandler.OuterHandler = null;
					return;
				}
				else
				{
					pipelineHandler = pipelineHandler.InnerHandler;
				}
			}
			throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Cannot find a handler of type {0}", new object[]
			{
				typeFromHandle.Name
			}));
		}

		// Token: 0x060009C9 RID: 2505 RVA: 0x00016778 File Offset: 0x00014978
		public void ReplaceHandler<T>(IPipelineHandler handler) where T : IPipelineHandler
		{
			if (handler == null)
			{
				throw new ArgumentNullException("handler");
			}
			this.ThrowIfDisposed();
			Type typeFromHandle = typeof(T);
			IPipelineHandler pipelineHandler = null;
			for (IPipelineHandler pipelineHandler2 = this._handler; pipelineHandler2 != null; pipelineHandler2 = pipelineHandler2.InnerHandler)
			{
				if (pipelineHandler2.GetType() == typeFromHandle)
				{
					handler.InnerHandler = pipelineHandler2.InnerHandler;
					handler.OuterHandler = pipelineHandler2.OuterHandler;
					if (pipelineHandler != null)
					{
						pipelineHandler.InnerHandler = handler;
					}
					else
					{
						this._handler = handler;
					}
					if (pipelineHandler2.InnerHandler != null)
					{
						pipelineHandler2.InnerHandler.OuterHandler = handler;
					}
					pipelineHandler2.InnerHandler = null;
					pipelineHandler2.OuterHandler = null;
					this.SetHandlerProperties(handler);
					return;
				}
				pipelineHandler = pipelineHandler2;
			}
			throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Cannot find a handler of type {0}", new object[]
			{
				typeFromHandle.Name
			}));
		}

		// Token: 0x060009CA RID: 2506 RVA: 0x0001683C File Offset: 0x00014A3C
		private static void InsertHandler(IPipelineHandler handler, IPipelineHandler current)
		{
			IPipelineHandler innerHandler = current.InnerHandler;
			current.InnerHandler = handler;
			handler.OuterHandler = current;
			if (innerHandler != null)
			{
				IPipelineHandler innermostHandler = RuntimePipeline.GetInnermostHandler(handler);
				innermostHandler.InnerHandler = innerHandler;
				innerHandler.OuterHandler = innermostHandler;
			}
		}

		// Token: 0x060009CB RID: 2507 RVA: 0x00016878 File Offset: 0x00014A78
		private static IPipelineHandler GetInnermostHandler(IPipelineHandler handler)
		{
			IPipelineHandler pipelineHandler = handler;
			while (pipelineHandler.InnerHandler != null)
			{
				pipelineHandler = pipelineHandler.InnerHandler;
			}
			return pipelineHandler;
		}

		// Token: 0x060009CC RID: 2508 RVA: 0x00016899 File Offset: 0x00014A99
		private void SetHandlerProperties(IPipelineHandler handler)
		{
			this.ThrowIfDisposed();
			handler.Logger = this._logger;
		}

		// Token: 0x17000313 RID: 787
		// (get) Token: 0x060009CD RID: 2509 RVA: 0x000168AD File Offset: 0x00014AAD
		public List<IPipelineHandler> Handlers
		{
			get
			{
				return this.EnumerateHandlers().ToList<IPipelineHandler>();
			}
		}

		// Token: 0x060009CE RID: 2510 RVA: 0x000168BA File Offset: 0x00014ABA
		public IEnumerable<IPipelineHandler> EnumerateHandlers()
		{
			for (IPipelineHandler handler = this.Handler; handler != null; handler = handler.InnerHandler)
			{
				yield return handler;
			}
			yield break;
		}

		// Token: 0x060009CF RID: 2511 RVA: 0x000168CA File Offset: 0x00014ACA
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060009D0 RID: 2512 RVA: 0x000168DC File Offset: 0x00014ADC
		protected virtual void Dispose(bool disposing)
		{
			if (this._disposed)
			{
				return;
			}
			if (disposing)
			{
				IPipelineHandler innerHandler;
				for (IPipelineHandler pipelineHandler = this.Handler; pipelineHandler != null; pipelineHandler = innerHandler)
				{
					innerHandler = pipelineHandler.InnerHandler;
					IDisposable disposable = pipelineHandler as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				this._disposed = true;
			}
		}

		// Token: 0x060009D1 RID: 2513 RVA: 0x00016921 File Offset: 0x00014B21
		private void ThrowIfDisposed()
		{
			if (this._disposed)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
		}

		// Token: 0x040003CC RID: 972
		private bool _disposed;

		// Token: 0x040003CD RID: 973
		private ILogger _logger;

		// Token: 0x040003CE RID: 974
		private IPipelineHandler _handler;
	}
}
