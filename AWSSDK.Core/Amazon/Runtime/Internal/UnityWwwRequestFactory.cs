﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000108 RID: 264
	public sealed class UnityWwwRequestFactory : IHttpRequestFactory<string>, IDisposable
	{
		// Token: 0x06000A51 RID: 2641 RVA: 0x00018246 File Offset: 0x00016446
		public IHttpRequest<string> CreateHttpRequest(Uri requestUri)
		{
			this._unityWwwRequest = new UnityWwwRequest(requestUri);
			return this._unityWwwRequest;
		}

		// Token: 0x06000A52 RID: 2642 RVA: 0x0001825A File Offset: 0x0001645A
		public void Dispose()
		{
			if (this._unityWwwRequest != null)
			{
				this._unityWwwRequest.Dispose();
			}
		}

		// Token: 0x040003E7 RID: 999
		private UnityWwwRequest _unityWwwRequest;
	}
}
