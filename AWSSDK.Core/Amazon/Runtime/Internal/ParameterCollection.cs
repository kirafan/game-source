﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000E5 RID: 229
	public class ParameterCollection : SortedDictionary<string, ParameterValue>
	{
		// Token: 0x060008DF RID: 2271 RVA: 0x0001543A File Offset: 0x0001363A
		public ParameterCollection() : base(StringComparer.Ordinal)
		{
		}

		// Token: 0x060008E0 RID: 2272 RVA: 0x00015447 File Offset: 0x00013647
		public void Add(string key, string value)
		{
			base.Add(key, new StringParameterValue(value));
		}

		// Token: 0x060008E1 RID: 2273 RVA: 0x00015456 File Offset: 0x00013656
		public void Add(string key, List<string> values)
		{
			base.Add(key, new StringListParameterValue(values));
		}

		// Token: 0x060008E2 RID: 2274 RVA: 0x00015465 File Offset: 0x00013665
		public List<KeyValuePair<string, string>> GetSortedParametersList()
		{
			return this.GetParametersEnumerable().ToList<KeyValuePair<string, string>>();
		}

		// Token: 0x060008E3 RID: 2275 RVA: 0x00015472 File Offset: 0x00013672
		private IEnumerable<KeyValuePair<string, string>> GetParametersEnumerable()
		{
			foreach (KeyValuePair<string, ParameterValue> keyValuePair in this)
			{
				string name = keyValuePair.Key;
				ParameterValue value = keyValuePair.Value;
				StringParameterValue stringParameterValue = value as StringParameterValue;
				StringListParameterValue slpv = value as StringListParameterValue;
				if (stringParameterValue != null)
				{
					yield return new KeyValuePair<string, string>(name, stringParameterValue.Value);
				}
				else
				{
					if (slpv == null)
					{
						throw new AmazonClientException("Unsupported parameter value type '" + value.GetType().FullName + "'");
					}
					List<string> value2 = slpv.Value;
					value2.Sort(StringComparer.Ordinal);
					foreach (string value3 in value2)
					{
						yield return new KeyValuePair<string, string>(name, value3);
					}
					List<string>.Enumerator enumerator2 = default(List<string>.Enumerator);
				}
				name = null;
				value = null;
				slpv = null;
			}
			SortedDictionary<string, ParameterValue>.Enumerator enumerator = default(SortedDictionary<string, ParameterValue>.Enumerator);
			yield break;
			yield break;
		}
	}
}
