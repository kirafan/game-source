﻿using System;
using System.Threading;

namespace Amazon.Runtime.Internal
{
	// Token: 0x0200010A RID: 266
	internal class SimpleAsyncResult : IAsyncResult
	{
		// Token: 0x06000A7D RID: 2685 RVA: 0x00018574 File Offset: 0x00016774
		public SimpleAsyncResult(object state)
		{
			this.AsyncState = state;
		}

		// Token: 0x17000333 RID: 819
		// (get) Token: 0x06000A7E RID: 2686 RVA: 0x00018583 File Offset: 0x00016783
		// (set) Token: 0x06000A7F RID: 2687 RVA: 0x0001858B File Offset: 0x0001678B
		public object AsyncState { get; private set; }

		// Token: 0x17000334 RID: 820
		// (get) Token: 0x06000A80 RID: 2688 RVA: 0x00003EFE File Offset: 0x000020FE
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x17000335 RID: 821
		// (get) Token: 0x06000A81 RID: 2689 RVA: 0x00011C11 File Offset: 0x0000FE11
		public bool CompletedSynchronously
		{
			get
			{
				return true;
			}
		}

		// Token: 0x17000336 RID: 822
		// (get) Token: 0x06000A82 RID: 2690 RVA: 0x00011C11 File Offset: 0x0000FE11
		public bool IsCompleted
		{
			get
			{
				return true;
			}
		}
	}
}
