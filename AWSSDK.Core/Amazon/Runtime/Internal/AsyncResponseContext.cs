﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F3 RID: 243
	public class AsyncResponseContext : ResponseContext, IAsyncResponseContext, IResponseContext
	{
		// Token: 0x17000308 RID: 776
		// (get) Token: 0x06000999 RID: 2457 RVA: 0x00016091 File Offset: 0x00014291
		// (set) Token: 0x0600099A RID: 2458 RVA: 0x00016099 File Offset: 0x00014299
		public RuntimeAsyncResult AsyncResult { get; set; }
	}
}
