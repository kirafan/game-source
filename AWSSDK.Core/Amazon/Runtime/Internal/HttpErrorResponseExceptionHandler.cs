﻿using System;
using System.Net;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000FB RID: 251
	public class HttpErrorResponseExceptionHandler : ExceptionHandler<HttpErrorResponseException>
	{
		// Token: 0x060009DC RID: 2524 RVA: 0x00016B1A File Offset: 0x00014D1A
		public HttpErrorResponseExceptionHandler(ILogger logger) : base(logger)
		{
		}

		// Token: 0x060009DD RID: 2525 RVA: 0x00016B24 File Offset: 0x00014D24
		public override bool HandleException(IExecutionContext executionContext, HttpErrorResponseException exception)
		{
			IRequestContext requestContext = executionContext.RequestContext;
			IWebResponseData response = exception.Response;
			if (this.HandleSuppressed404(executionContext, response))
			{
				return false;
			}
			requestContext.Metrics.AddProperty(Metric.StatusCode, response.StatusCode);
			AmazonServiceException ex = null;
			try
			{
				using (response.ResponseBody)
				{
					ResponseUnmarshaller unmarshaller = requestContext.Unmarshaller;
					bool readEntireResponse = true;
					UnmarshallerContext unmarshallerContext = unmarshaller.CreateContext(response, readEntireResponse, response.ResponseBody.OpenResponse(), requestContext.Metrics);
					try
					{
						ex = unmarshaller.UnmarshallException(unmarshallerContext, exception, response.StatusCode);
					}
					catch (Exception ex2)
					{
						if (ex2 is AmazonServiceException || ex2 is AmazonClientException)
						{
							throw;
						}
						string headerValue = response.GetHeaderValue("x-amzn-RequestId");
						string responseBody2 = unmarshallerContext.ResponseBody;
						throw new AmazonUnmarshallingException(headerValue, null, responseBody2, ex2);
					}
					requestContext.Metrics.AddProperty(Metric.AWSRequestID, ex.RequestId);
					requestContext.Metrics.AddProperty(Metric.AWSErrorCode, ex.ErrorCode);
					if (requestContext.ClientConfig.LogResponse || AWSConfigs.LoggingConfig.LogResponses > ResponseLoggingOption.Never)
					{
						base.Logger.Error(ex, "Received error response: [{0}]", new object[]
						{
							unmarshallerContext.ResponseBody
						});
					}
				}
			}
			catch (Exception exception2)
			{
				base.Logger.Error(exception2, "Failed to unmarshall a service error response.", new object[0]);
				throw;
			}
			throw ex;
		}

		// Token: 0x060009DE RID: 2526 RVA: 0x00016C94 File Offset: 0x00014E94
		private bool HandleSuppressed404(IExecutionContext executionContext, IWebResponseData httpErrorResponse)
		{
			IRequestContext requestContext = executionContext.RequestContext;
			IResponseContext responseContext = executionContext.ResponseContext;
			if (httpErrorResponse != null && httpErrorResponse.StatusCode == HttpStatusCode.NotFound && requestContext.Request.Suppress404Exceptions)
			{
				using (httpErrorResponse.ResponseBody)
				{
					ResponseUnmarshaller unmarshaller = requestContext.Unmarshaller;
					bool readEntireResponse = requestContext.ClientConfig.LogResponse || AWSConfigs.LoggingConfig.LogResponses > ResponseLoggingOption.Never;
					UnmarshallerContext input = unmarshaller.CreateContext(httpErrorResponse, readEntireResponse, httpErrorResponse.ResponseBody.OpenResponse(), requestContext.Metrics);
					try
					{
						responseContext.Response = unmarshaller.Unmarshall(input);
						responseContext.Response.ContentLength = httpErrorResponse.ContentLength;
						responseContext.Response.HttpStatusCode = httpErrorResponse.StatusCode;
						return true;
					}
					catch (Exception exception)
					{
						base.Logger.Debug(exception, "Failed to unmarshall 404 response when it was supressed.", new object[0]);
					}
				}
				return false;
			}
			return false;
		}
	}
}
