﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;
using Amazon.Util.Internal.PlatformServices;
using UnityEngine;

namespace Amazon.Runtime.Internal
{
	// Token: 0x0200010F RID: 271
	public class UnityMainThreadDispatcher : MonoBehaviour
	{
		// Token: 0x06000ABF RID: 2751 RVA: 0x000191CF File Offset: 0x000173CF
		public void Awake()
		{
			this._logger = Amazon.Runtime.Internal.Util.Logger.GetLogger(base.GetType());
			this._nextUpdateTime = Time.unscaledTime;
			this._nextUpdateTime += this._updateInterval;
		}

		// Token: 0x06000AC0 RID: 2752 RVA: 0x00019200 File Offset: 0x00017400
		private void Update()
		{
			if (Time.unscaledTime >= this._nextUpdateTime)
			{
				this.ProcessRequests();
				this._nextUpdateTime += this._updateInterval;
			}
		}

		// Token: 0x06000AC1 RID: 2753 RVA: 0x00019228 File Offset: 0x00017428
		private void ProcessRequests()
		{
			IUnityHttpRequest unityHttpRequest = UnityRequestQueue.Instance.DequeueRequest();
			if (unityHttpRequest != null)
			{
				base.StartCoroutine(this.InvokeRequest(unityHttpRequest));
			}
			RuntimeAsyncResult runtimeAsyncResult = UnityRequestQueue.Instance.DequeueCallback();
			if (runtimeAsyncResult != null && runtimeAsyncResult.Action != null)
			{
				try
				{
					runtimeAsyncResult.Action(runtimeAsyncResult.Request, runtimeAsyncResult.Response, runtimeAsyncResult.Exception, runtimeAsyncResult.AsyncOptions);
				}
				catch (Exception exception)
				{
					this._logger.Error(exception, "An unhandled exception was thrown from the callback method {0}.", new object[]
					{
						runtimeAsyncResult.Request.ToString()
					});
				}
			}
			Action action = UnityRequestQueue.Instance.DequeueMainThreadOperation();
			if (action != null)
			{
				try
				{
					action();
				}
				catch (Exception exception2)
				{
					this._logger.Error(exception2, "An unhandled exception was thrown from the callback method", new object[0]);
				}
			}
			Amazon.Util.Internal.PlatformServices.NetworkReachability networkReachability = ServiceFactory.Instance.GetService<INetworkReachability>() as Amazon.Util.Internal.PlatformServices.NetworkReachability;
			if (this._currentNetworkStatus != networkReachability.NetworkStatus)
			{
				this._currentNetworkStatus = networkReachability.NetworkStatus;
				networkReachability.OnNetworkReachabilityChanged(this._currentNetworkStatus);
			}
		}

		// Token: 0x06000AC2 RID: 2754 RVA: 0x0001933C File Offset: 0x0001753C
		private IEnumerator InvokeRequest(IUnityHttpRequest request)
		{
			if ((ServiceFactory.Instance.GetService<INetworkReachability>() as Amazon.Util.Internal.PlatformServices.NetworkReachability).NetworkStatus != NetworkStatus.NotReachable)
			{
				if (request is UnityWwwRequest)
				{
					WWW wwwRequest = new WWW((request as UnityWwwRequest).RequestUri.AbsoluteUri, request.RequestContent, request.Headers);
					if (wwwRequest == null)
					{
						yield return null;
					}
					bool uploadCompleted = false;
					while (!wwwRequest.isDone)
					{
						float uploadProgress = wwwRequest.uploadProgress;
						if (!uploadCompleted)
						{
							request.OnUploadProgressChanged(uploadProgress);
							if (uploadProgress == 1f)
							{
								uploadCompleted = true;
							}
						}
						yield return null;
					}
					request.WwwRequest = wwwRequest;
					request.Response = new UnityWebResponseData(wwwRequest);
					wwwRequest = null;
				}
				else
				{
					UnityRequest unityRequest = request as UnityRequest;
					if (unityRequest == null)
					{
						yield return null;
					}
					UnityWebRequestWrapper unityWebRequest = new UnityWebRequestWrapper(unityRequest.RequestUri.AbsoluteUri, unityRequest.Method);
					unityWebRequest.DownloadHandler = new DownloadHandlerBufferWrapper();
					if (request.RequestContent != null && request.RequestContent.Length != 0)
					{
						unityWebRequest.UploadHandler = new UploadHandlerRawWrapper(request.RequestContent);
					}
					bool uploadCompleted2 = false;
					foreach (KeyValuePair<string, string> keyValuePair in request.Headers)
					{
						unityWebRequest.SetRequestHeader(keyValuePair.Key, keyValuePair.Value);
					}
					AsyncOperation operation = unityWebRequest.Send();
					while (!operation.isDone)
					{
						float progress = operation.progress;
						if (!uploadCompleted2)
						{
							request.OnUploadProgressChanged(progress);
							if (progress == 1f)
							{
								uploadCompleted2 = true;
							}
						}
						yield return null;
					}
					request.WwwRequest = unityWebRequest;
					request.Response = new UnityWebResponseData(unityWebRequest);
					unityRequest = null;
					unityWebRequest = null;
					operation = null;
				}
			}
			else
			{
				request.Exception = new WebException("Network Unavailable", WebExceptionStatus.ConnectFailure);
			}
			if (request.IsSync)
			{
				if (request.Response != null && !request.Response.IsSuccessStatusCode)
				{
					request.Exception = new HttpErrorResponseException(request.Response);
				}
				request.WaitHandle.Set();
			}
			else
			{
				if (request.Response != null && !request.Response.IsSuccessStatusCode)
				{
					request.Exception = new HttpErrorResponseException(request.Response);
				}
				ThreadPool.QueueUserWorkItem(delegate(object state)
				{
					try
					{
						request.Callback(request.AsyncResult);
					}
					catch (Exception exception)
					{
						this._logger.Error(exception, "An exception was thrown from the callback method executed fromUnityMainThreadDispatcher.InvokeRequest method.", new object[0]);
					}
				});
			}
			yield break;
		}

		// Token: 0x04000412 RID: 1042
		private Amazon.Runtime.Internal.Util.Logger _logger;

		// Token: 0x04000413 RID: 1043
		private float _nextUpdateTime;

		// Token: 0x04000414 RID: 1044
		private float _updateInterval = 0.1f;

		// Token: 0x04000415 RID: 1045
		private NetworkStatus _currentNetworkStatus;
	}
}
