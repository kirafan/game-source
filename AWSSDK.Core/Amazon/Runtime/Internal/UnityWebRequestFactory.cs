﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000106 RID: 262
	public sealed class UnityWebRequestFactory : IHttpRequestFactory<string>, IDisposable
	{
		// Token: 0x06000A25 RID: 2597 RVA: 0x00017ED9 File Offset: 0x000160D9
		public IHttpRequest<string> CreateHttpRequest(Uri requestUri)
		{
			this._unityRequest = new UnityRequest(requestUri);
			return this._unityRequest;
		}

		// Token: 0x06000A26 RID: 2598 RVA: 0x00017EED File Offset: 0x000160ED
		public void Dispose()
		{
			if (this._unityRequest != null)
			{
				this._unityRequest.Dispose();
			}
		}

		// Token: 0x040003D9 RID: 985
		private UnityRequest _unityRequest;
	}
}
