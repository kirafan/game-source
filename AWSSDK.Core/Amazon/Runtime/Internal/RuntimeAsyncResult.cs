﻿using System;
using System.Threading;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x0200010D RID: 269
	public class RuntimeAsyncResult : IAsyncResult, IDisposable
	{
		// Token: 0x06000AA1 RID: 2721 RVA: 0x00018DD4 File Offset: 0x00016FD4
		public RuntimeAsyncResult(AsyncCallback asyncCallback, object asyncState)
		{
			this._lockObj = new object();
			this._callbackInvoked = false;
			this._logger = Logger.GetLogger(typeof(RuntimeAsyncResult));
			this.AsyncState = asyncState;
			this.IsCompleted = false;
			this.AsyncCallback = asyncCallback;
			this.CompletedSynchronously = false;
			this._logger = Logger.GetLogger(base.GetType());
		}

		// Token: 0x1700033D RID: 829
		// (get) Token: 0x06000AA2 RID: 2722 RVA: 0x00018E3B File Offset: 0x0001703B
		// (set) Token: 0x06000AA3 RID: 2723 RVA: 0x00018E43 File Offset: 0x00017043
		private AsyncCallback AsyncCallback { get; set; }

		// Token: 0x1700033E RID: 830
		// (get) Token: 0x06000AA4 RID: 2724 RVA: 0x00018E4C File Offset: 0x0001704C
		// (set) Token: 0x06000AA5 RID: 2725 RVA: 0x00018E54 File Offset: 0x00017054
		public object AsyncState { get; private set; }

		// Token: 0x1700033F RID: 831
		// (get) Token: 0x06000AA6 RID: 2726 RVA: 0x00018E60 File Offset: 0x00017060
		public WaitHandle AsyncWaitHandle
		{
			get
			{
				if (this._waitHandle != null)
				{
					return this._waitHandle;
				}
				object lockObj = this._lockObj;
				lock (lockObj)
				{
					if (this._waitHandle == null)
					{
						this._waitHandle = new ManualResetEvent(this.IsCompleted);
					}
				}
				return this._waitHandle;
			}
		}

		// Token: 0x17000340 RID: 832
		// (get) Token: 0x06000AA7 RID: 2727 RVA: 0x00018EC4 File Offset: 0x000170C4
		// (set) Token: 0x06000AA8 RID: 2728 RVA: 0x00018ECC File Offset: 0x000170CC
		public bool CompletedSynchronously { get; private set; }

		// Token: 0x17000341 RID: 833
		// (get) Token: 0x06000AA9 RID: 2729 RVA: 0x00018ED5 File Offset: 0x000170D5
		// (set) Token: 0x06000AAA RID: 2730 RVA: 0x00018EDD File Offset: 0x000170DD
		public bool IsCompleted { get; private set; }

		// Token: 0x17000342 RID: 834
		// (get) Token: 0x06000AAB RID: 2731 RVA: 0x00018EE6 File Offset: 0x000170E6
		// (set) Token: 0x06000AAC RID: 2732 RVA: 0x00018EEE File Offset: 0x000170EE
		public Exception Exception { get; set; }

		// Token: 0x17000343 RID: 835
		// (get) Token: 0x06000AAD RID: 2733 RVA: 0x00018EF7 File Offset: 0x000170F7
		// (set) Token: 0x06000AAE RID: 2734 RVA: 0x00018EFF File Offset: 0x000170FF
		public AmazonWebServiceResponse Response { get; set; }

		// Token: 0x17000344 RID: 836
		// (get) Token: 0x06000AAF RID: 2735 RVA: 0x00018F08 File Offset: 0x00017108
		// (set) Token: 0x06000AB0 RID: 2736 RVA: 0x00018F10 File Offset: 0x00017110
		public AmazonWebServiceRequest Request { get; set; }

		// Token: 0x17000345 RID: 837
		// (get) Token: 0x06000AB1 RID: 2737 RVA: 0x00018F19 File Offset: 0x00017119
		// (set) Token: 0x06000AB2 RID: 2738 RVA: 0x00018F21 File Offset: 0x00017121
		public Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> Action { get; set; }

		// Token: 0x17000346 RID: 838
		// (get) Token: 0x06000AB3 RID: 2739 RVA: 0x00018F2A File Offset: 0x0001712A
		// (set) Token: 0x06000AB4 RID: 2740 RVA: 0x00018F32 File Offset: 0x00017132
		public AsyncOptions AsyncOptions { get; set; }

		// Token: 0x06000AB5 RID: 2741 RVA: 0x00018F3B File Offset: 0x0001713B
		internal void SignalWaitHandle()
		{
			this.IsCompleted = true;
			if (this._waitHandle != null)
			{
				this._waitHandle.Set();
			}
		}

		// Token: 0x06000AB6 RID: 2742 RVA: 0x00018F58 File Offset: 0x00017158
		internal void HandleException(Exception exception)
		{
			this.Exception = exception;
			this.InvokeCallback();
		}

		// Token: 0x06000AB7 RID: 2743 RVA: 0x00018F68 File Offset: 0x00017168
		public void InvokeCallback()
		{
			this.SignalWaitHandle();
			if (!this._callbackInvoked)
			{
				this._callbackInvoked = true;
				try
				{
					if (this.AsyncOptions.ExecuteCallbackOnMainThread)
					{
						UnityRequestQueue.Instance.EnqueueCallback(this);
					}
					else if (this.Action != null)
					{
						this.Action(this.Request, this.Response, this.Exception, this.AsyncOptions);
					}
				}
				catch (Exception exception)
				{
					this._logger.Error(exception, "An unhandled exception occurred in the user callback.", new object[0]);
				}
			}
		}

		// Token: 0x06000AB8 RID: 2744 RVA: 0x00018FFC File Offset: 0x000171FC
		protected virtual void Dispose(bool disposing)
		{
			if (!this._disposed)
			{
				if (disposing && this._waitHandle != null)
				{
					this._waitHandle.Close();
					this._waitHandle = null;
				}
				this._disposed = true;
			}
		}

		// Token: 0x06000AB9 RID: 2745 RVA: 0x0001902A File Offset: 0x0001722A
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x04000402 RID: 1026
		private object _lockObj;

		// Token: 0x04000403 RID: 1027
		private ManualResetEvent _waitHandle;

		// Token: 0x04000404 RID: 1028
		private bool _disposed;

		// Token: 0x04000405 RID: 1029
		private bool _callbackInvoked;

		// Token: 0x04000406 RID: 1030
		private ILogger _logger;
	}
}
