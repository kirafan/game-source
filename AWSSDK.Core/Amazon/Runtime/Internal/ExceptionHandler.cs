﻿using System;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000FA RID: 250
	public abstract class ExceptionHandler<T> : IExceptionHandler<T>, IExceptionHandler where T : Exception
	{
		// Token: 0x17000315 RID: 789
		// (get) Token: 0x060009D8 RID: 2520 RVA: 0x00016AEF File Offset: 0x00014CEF
		protected ILogger Logger
		{
			get
			{
				return this._logger;
			}
		}

		// Token: 0x060009D9 RID: 2521 RVA: 0x00016AF7 File Offset: 0x00014CF7
		protected ExceptionHandler(ILogger logger)
		{
			this._logger = logger;
		}

		// Token: 0x060009DA RID: 2522 RVA: 0x00016B06 File Offset: 0x00014D06
		public bool Handle(IExecutionContext executionContext, Exception exception)
		{
			return this.HandleException(executionContext, exception as T);
		}

		// Token: 0x060009DB RID: 2523
		public abstract bool HandleException(IExecutionContext executionContext, T exception);

		// Token: 0x040003D0 RID: 976
		private ILogger _logger;
	}
}
