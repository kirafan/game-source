﻿using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal.Util;
using Amazon.Util.Internal;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F9 RID: 249
	public class ErrorHandler : PipelineHandler
	{
		// Token: 0x17000314 RID: 788
		// (get) Token: 0x060009D2 RID: 2514 RVA: 0x0001693C File Offset: 0x00014B3C
		public IDictionary<Type, IExceptionHandler> ExceptionHandlers
		{
			get
			{
				return this._exceptionHandlers;
			}
		}

		// Token: 0x060009D3 RID: 2515 RVA: 0x00016944 File Offset: 0x00014B44
		public ErrorHandler(ILogger logger)
		{
			this.Logger = logger;
			this._exceptionHandlers = new Dictionary<Type, IExceptionHandler>
			{
				{
					typeof(HttpErrorResponseException),
					new HttpErrorResponseExceptionHandler(this.Logger)
				}
			};
		}

		// Token: 0x060009D4 RID: 2516 RVA: 0x0001697C File Offset: 0x00014B7C
		public override void InvokeSync(IExecutionContext executionContext)
		{
			try
			{
				base.InvokeSync(executionContext);
			}
			catch (Exception exception)
			{
				ErrorHandler.DisposeReponse(executionContext.ResponseContext);
				if (this.ProcessException(executionContext, exception))
				{
					throw;
				}
			}
		}

		// Token: 0x060009D5 RID: 2517 RVA: 0x000169BC File Offset: 0x00014BBC
		protected override void InvokeAsyncCallback(IAsyncExecutionContext executionContext)
		{
			IAsyncRequestContext requestContext = executionContext.RequestContext;
			IAsyncResponseContext responseContext = executionContext.ResponseContext;
			Exception exception = responseContext.AsyncResult.Exception;
			if (exception != null)
			{
				try
				{
					ErrorHandler.DisposeReponse(executionContext.ResponseContext);
					if (!this.ProcessException(ExecutionContext.CreateFromAsyncContext(executionContext), exception))
					{
						responseContext.AsyncResult.Exception = null;
					}
				}
				catch (Exception exception2)
				{
					responseContext.AsyncResult.Exception = exception2;
				}
			}
			base.InvokeAsyncCallback(executionContext);
		}

		// Token: 0x060009D6 RID: 2518 RVA: 0x00016A34 File Offset: 0x00014C34
		private static void DisposeReponse(IResponseContext responseContext)
		{
			if (responseContext.HttpResponse != null && responseContext.HttpResponse.ResponseBody != null)
			{
				responseContext.HttpResponse.ResponseBody.Dispose();
			}
		}

		// Token: 0x060009D7 RID: 2519 RVA: 0x00016A5C File Offset: 0x00014C5C
		private bool ProcessException(IExecutionContext executionContext, Exception exception)
		{
			this.Logger.Error(exception, "An exception of type {0} was handled in ErrorHandler.", new object[]
			{
				exception.GetType().Name
			});
			executionContext.RequestContext.Metrics.AddProperty(Metric.Exception, exception);
			Type type = exception.GetType();
			ITypeInfo typeInfo = TypeFactory.GetTypeInfo(exception.GetType());
			IExceptionHandler exceptionHandler;
			for (;;)
			{
				exceptionHandler = null;
				if (this.ExceptionHandlers.TryGetValue(type, out exceptionHandler))
				{
					break;
				}
				type = typeInfo.BaseType;
				typeInfo = TypeFactory.GetTypeInfo(typeInfo.BaseType);
				if (type == typeof(Exception))
				{
					return true;
				}
			}
			return exceptionHandler.Handle(executionContext, exception);
		}

		// Token: 0x040003CF RID: 975
		private IDictionary<Type, IExceptionHandler> _exceptionHandlers;
	}
}
