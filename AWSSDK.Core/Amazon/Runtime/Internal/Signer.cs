﻿using System;
using System.IO;
using Amazon.Runtime.Internal.Auth;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000103 RID: 259
	public class Signer : PipelineHandler
	{
		// Token: 0x06000A0A RID: 2570 RVA: 0x0001741D File Offset: 0x0001561D
		public override void InvokeSync(IExecutionContext executionContext)
		{
			Signer.PreInvoke(executionContext);
			base.InvokeSync(executionContext);
		}

		// Token: 0x06000A0B RID: 2571 RVA: 0x0001742C File Offset: 0x0001562C
		public override IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			Signer.PreInvoke(ExecutionContext.CreateFromAsyncContext(executionContext));
			return base.InvokeAsync(executionContext);
		}

		// Token: 0x06000A0C RID: 2572 RVA: 0x00017440 File Offset: 0x00015640
		protected static void PreInvoke(IExecutionContext executionContext)
		{
			if (Signer.ShouldSign(executionContext.RequestContext))
			{
				Signer.SignRequest(executionContext.RequestContext);
				executionContext.RequestContext.IsSigned = true;
			}
		}

		// Token: 0x06000A0D RID: 2573 RVA: 0x00017466 File Offset: 0x00015666
		private static bool ShouldSign(IRequestContext requestContext)
		{
			return !requestContext.IsSigned || requestContext.ClientConfig.ResignRetries;
		}

		// Token: 0x06000A0E RID: 2574 RVA: 0x00017480 File Offset: 0x00015680
		public static void SignRequest(IRequestContext requestContext)
		{
			ImmutableCredentials immutableCredentials = requestContext.ImmutableCredentials;
			if (immutableCredentials == null)
			{
				return;
			}
			using (requestContext.Metrics.StartEvent(Metric.RequestSigningTime))
			{
				if (immutableCredentials.UseToken)
				{
					ClientProtocol protocol = requestContext.Signer.Protocol;
					if (protocol != ClientProtocol.QueryStringProtocol)
					{
						if (protocol != ClientProtocol.RestProtocol)
						{
							throw new System.IO.InvalidDataException("Cannot determine protocol");
						}
						requestContext.Request.Headers["x-amz-security-token"] = immutableCredentials.Token;
					}
					else
					{
						requestContext.Request.Parameters["SecurityToken"] = immutableCredentials.Token;
					}
				}
				requestContext.Signer.Sign(requestContext.Request, requestContext.ClientConfig, requestContext.Metrics, immutableCredentials.AccessKey, immutableCredentials.SecretKey);
			}
		}
	}
}
