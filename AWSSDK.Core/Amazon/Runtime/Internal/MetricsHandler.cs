﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000101 RID: 257
	public class MetricsHandler : PipelineHandler
	{
		// Token: 0x06000A01 RID: 2561 RVA: 0x000171E8 File Offset: 0x000153E8
		public override void InvokeSync(IExecutionContext executionContext)
		{
			executionContext.RequestContext.Metrics.AddProperty(Metric.AsyncCall, false);
			try
			{
				executionContext.RequestContext.Metrics.StartEvent(Metric.ClientExecuteTime);
				base.InvokeSync(executionContext);
			}
			finally
			{
				executionContext.RequestContext.Metrics.StopEvent(Metric.ClientExecuteTime);
				base.LogMetrics(executionContext);
			}
		}

		// Token: 0x06000A02 RID: 2562 RVA: 0x00017254 File Offset: 0x00015454
		public override IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			executionContext.RequestContext.Metrics.AddProperty(Metric.AsyncCall, true);
			executionContext.RequestContext.Metrics.StartEvent(Metric.ClientExecuteTime);
			return base.InvokeAsync(executionContext);
		}

		// Token: 0x06000A03 RID: 2563 RVA: 0x00017288 File Offset: 0x00015488
		protected override void InvokeAsyncCallback(IAsyncExecutionContext executionContext)
		{
			executionContext.RequestContext.Metrics.StopEvent(Metric.ClientExecuteTime);
			base.LogMetrics(ExecutionContext.CreateFromAsyncContext(executionContext));
			base.InvokeAsyncCallback(executionContext);
		}
	}
}
