﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000EA RID: 234
	public interface IAmazonWebServiceRequest
	{
		// Token: 0x170002D3 RID: 723
		// (get) Token: 0x0600091D RID: 2333
		// (set) Token: 0x0600091E RID: 2334
		EventHandler<StreamTransferProgressArgs> StreamUploadProgressCallback { get; set; }

		// Token: 0x0600091F RID: 2335
		void AddBeforeRequestHandler(RequestEventHandler handler);

		// Token: 0x06000920 RID: 2336
		void RemoveBeforeRequestHandler(RequestEventHandler handler);

		// Token: 0x170002D4 RID: 724
		// (get) Token: 0x06000921 RID: 2337
		Dictionary<string, object> RequestState { get; }

		// Token: 0x170002D5 RID: 725
		// (get) Token: 0x06000922 RID: 2338
		// (set) Token: 0x06000923 RID: 2339
		bool UseSigV4 { get; set; }
	}
}
