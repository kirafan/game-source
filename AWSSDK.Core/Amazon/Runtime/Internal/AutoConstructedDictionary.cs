﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000E7 RID: 231
	[Serializable]
	public class AutoConstructedDictionary<K, V> : Dictionary<K, V>
	{
		// Token: 0x060008E5 RID: 2277 RVA: 0x0001548A File Offset: 0x0001368A
		protected AutoConstructedDictionary(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
