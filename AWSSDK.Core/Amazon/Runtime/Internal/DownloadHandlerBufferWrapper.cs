﻿using System;
using System.Reflection;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000112 RID: 274
	public class DownloadHandlerBufferWrapper : IDisposable
	{
		// Token: 0x17000353 RID: 851
		// (get) Token: 0x06000AE2 RID: 2786 RVA: 0x00019990 File Offset: 0x00017B90
		// (set) Token: 0x06000AE3 RID: 2787 RVA: 0x00019998 File Offset: 0x00017B98
		public object Instance { get; private set; }

		// Token: 0x06000AE4 RID: 2788 RVA: 0x000199A4 File Offset: 0x00017BA4
		static DownloadHandlerBufferWrapper()
		{
			if (DownloadHandlerBufferWrapper.downloadHandlerBufferType == null)
			{
				DownloadHandlerBufferWrapper.downloadHandlerBufferType = Type.GetType("UnityEngine.Experimental.Networking.DownloadHandlerBuffer, UnityEngine");
			}
			DownloadHandlerBufferWrapper.downloadHandlerBufferMethods = DownloadHandlerBufferWrapper.downloadHandlerBufferType.GetMethods();
			DownloadHandlerBufferWrapper.downloadHandlerBufferProperties = DownloadHandlerBufferWrapper.downloadHandlerBufferType.GetProperties();
			DownloadHandlerBufferWrapper.dataProperty = DownloadHandlerBufferWrapper.downloadHandlerBufferType.GetProperty("data");
			DownloadHandlerBufferWrapper.dataGetMethod = DownloadHandlerBufferWrapper.dataProperty.GetGetMethod();
		}

		// Token: 0x06000AE5 RID: 2789 RVA: 0x00019A17 File Offset: 0x00017C17
		public DownloadHandlerBufferWrapper()
		{
			this.Instance = Activator.CreateInstance(DownloadHandlerBufferWrapper.downloadHandlerBufferType);
		}

		// Token: 0x17000354 RID: 852
		// (get) Token: 0x06000AE6 RID: 2790 RVA: 0x00019A2F File Offset: 0x00017C2F
		public byte[] Data
		{
			get
			{
				return (byte[])DownloadHandlerBufferWrapper.dataGetMethod.Invoke(this.Instance, null);
			}
		}

		// Token: 0x06000AE7 RID: 2791 RVA: 0x00019A48 File Offset: 0x00017C48
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposedValue)
			{
				if (disposing)
				{
					IDisposable disposable = this.Instance as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				this.Instance = null;
				this.disposedValue = true;
			}
		}

		// Token: 0x06000AE8 RID: 2792 RVA: 0x00019A83 File Offset: 0x00017C83
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x04000430 RID: 1072
		private static Type downloadHandlerBufferType = Type.GetType("UnityEngine.Networking.DownloadHandlerBuffer, UnityEngine");

		// Token: 0x04000431 RID: 1073
		private static PropertyInfo[] downloadHandlerBufferProperties;

		// Token: 0x04000432 RID: 1074
		private static MethodInfo[] downloadHandlerBufferMethods;

		// Token: 0x04000433 RID: 1075
		private static PropertyInfo dataProperty;

		// Token: 0x04000434 RID: 1076
		private static MethodInfo dataGetMethod;

		// Token: 0x04000436 RID: 1078
		private bool disposedValue;
	}
}
