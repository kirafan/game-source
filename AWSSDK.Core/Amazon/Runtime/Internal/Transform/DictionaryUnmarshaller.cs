﻿using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal.Util;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200015F RID: 351
	public class DictionaryUnmarshaller<TKey, TValue, TKeyUnmarshaller, TValueUnmarshaller> : IUnmarshaller<Dictionary<TKey, TValue>, XmlUnmarshallerContext>, IUnmarshaller<Dictionary<TKey, TValue>, JsonUnmarshallerContext> where TKeyUnmarshaller : IUnmarshaller<TKey, XmlUnmarshallerContext>, IUnmarshaller<TKey, JsonUnmarshallerContext> where TValueUnmarshaller : IUnmarshaller<TValue, XmlUnmarshallerContext>, IUnmarshaller<TValue, JsonUnmarshallerContext>
	{
		// Token: 0x06000D0E RID: 3342 RVA: 0x0001E849 File Offset: 0x0001CA49
		public DictionaryUnmarshaller(TKeyUnmarshaller kUnmarshaller, TValueUnmarshaller vUnmarshaller)
		{
			this.KVUnmarshaller = new KeyValueUnmarshaller<TKey, TValue, TKeyUnmarshaller, TValueUnmarshaller>(kUnmarshaller, vUnmarshaller);
		}

		// Token: 0x06000D0F RID: 3343 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		Dictionary<TKey, TValue> IUnmarshaller<Dictionary<TKey, TValue>, XmlUnmarshallerContext>.Unmarshall(XmlUnmarshallerContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000D10 RID: 3344 RVA: 0x0001E860 File Offset: 0x0001CA60
		public Dictionary<TKey, TValue> Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return new Dictionary<TKey, TValue>();
			}
			Dictionary<TKey, TValue> dictionary = new AlwaysSendDictionary<TKey, TValue>();
			while (!context.Peek(JsonToken.ObjectEnd))
			{
				KeyValuePair<TKey, TValue> keyValuePair = this.KVUnmarshaller.Unmarshall(context);
				dictionary.Add(keyValuePair.Key, keyValuePair.Value);
			}
			context.Read();
			return dictionary;
		}

		// Token: 0x040004CD RID: 1229
		private KeyValueUnmarshaller<TKey, TValue, TKeyUnmarshaller, TValueUnmarshaller> KVUnmarshaller;
	}
}
