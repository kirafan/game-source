﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200014E RID: 334
	public abstract class EC2ResponseUnmarshaller : XmlResponseUnmarshaller
	{
		// Token: 0x06000CB4 RID: 3252 RVA: 0x0001E17C File Offset: 0x0001C37C
		public override AmazonWebServiceResponse Unmarshall(UnmarshallerContext input)
		{
			AmazonWebServiceResponse amazonWebServiceResponse = base.Unmarshall(input);
			if (amazonWebServiceResponse.ResponseMetadata == null)
			{
				amazonWebServiceResponse.ResponseMetadata = new ResponseMetadata();
			}
			EC2UnmarshallerContext ec2UnmarshallerContext = input as EC2UnmarshallerContext;
			if (ec2UnmarshallerContext != null && !string.IsNullOrEmpty(ec2UnmarshallerContext.RequestId))
			{
				amazonWebServiceResponse.ResponseMetadata.RequestId = ec2UnmarshallerContext.RequestId;
			}
			return amazonWebServiceResponse;
		}

		// Token: 0x06000CB5 RID: 3253 RVA: 0x0001E1CD File Offset: 0x0001C3CD
		protected override UnmarshallerContext ConstructUnmarshallerContext(Stream responseStream, bool maintainResponseBody, IWebResponseData response)
		{
			return new EC2UnmarshallerContext(responseStream, maintainResponseBody, response);
		}
	}
}
