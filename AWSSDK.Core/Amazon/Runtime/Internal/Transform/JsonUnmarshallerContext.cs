﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime.Internal.Util;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200014B RID: 331
	public class JsonUnmarshallerContext : UnmarshallerContext
	{
		// Token: 0x06000C96 RID: 3222 RVA: 0x0001DB50 File Offset: 0x0001BD50
		public JsonUnmarshallerContext(Stream responseStream, bool maintainResponseBody, IWebResponseData responseData)
		{
			if (maintainResponseBody)
			{
				base.WrappingStream = new CachingWrapperStream(responseStream, AWSConfigs.LoggingConfig.LogResponsesSizeLimit);
				responseStream = base.WrappingStream;
			}
			base.WebResponseData = responseData;
			base.MaintainResponseBody = maintainResponseBody;
			long num;
			if (responseData != null && long.TryParse(responseData.GetHeaderValue("Content-Length"), out num) && responseData.ContentLength.Equals(num) && string.IsNullOrEmpty(responseData.GetHeaderValue("Content-Encoding")))
			{
				base.SetupCRCStream(responseData, responseStream, num);
			}
			if (base.CrcStream != null)
			{
				this.streamReader = new StreamReader(base.CrcStream);
			}
			else
			{
				this.streamReader = new StreamReader(responseStream);
			}
			this.jsonReader = new JsonReader(this.streamReader);
		}

		// Token: 0x170003BD RID: 957
		// (get) Token: 0x06000C97 RID: 3223 RVA: 0x0001DC18 File Offset: 0x0001BE18
		public override bool IsStartOfDocument
		{
			get
			{
				return this.CurrentTokenType == JsonToken.None && !this.streamReader.EndOfStream;
			}
		}

		// Token: 0x170003BE RID: 958
		// (get) Token: 0x06000C98 RID: 3224 RVA: 0x0001DC32 File Offset: 0x0001BE32
		public override bool IsEndElement
		{
			get
			{
				return this.CurrentTokenType == JsonToken.ObjectEnd;
			}
		}

		// Token: 0x170003BF RID: 959
		// (get) Token: 0x06000C99 RID: 3225 RVA: 0x0001DC3D File Offset: 0x0001BE3D
		public override bool IsStartElement
		{
			get
			{
				return this.CurrentTokenType == JsonToken.ObjectStart;
			}
		}

		// Token: 0x170003C0 RID: 960
		// (get) Token: 0x06000C9A RID: 3226 RVA: 0x0001DC48 File Offset: 0x0001BE48
		public override int CurrentDepth
		{
			get
			{
				return this.stack.CurrentDepth;
			}
		}

		// Token: 0x170003C1 RID: 961
		// (get) Token: 0x06000C9B RID: 3227 RVA: 0x0001DC55 File Offset: 0x0001BE55
		public override string CurrentPath
		{
			get
			{
				return this.stack.CurrentPath;
			}
		}

		// Token: 0x06000C9C RID: 3228 RVA: 0x0001DC64 File Offset: 0x0001BE64
		public override bool Read()
		{
			if (this.wasPeeked)
			{
				this.wasPeeked = false;
				return this.currentToken == null;
			}
			bool flag = this.jsonReader.Read();
			if (flag)
			{
				this.currentToken = new JsonToken?(this.jsonReader.Token);
				this.UpdateContext();
			}
			else
			{
				this.currentToken = null;
			}
			this.wasPeeked = false;
			return flag;
		}

		// Token: 0x06000C9D RID: 3229 RVA: 0x0001DCD0 File Offset: 0x0001BED0
		public bool Peek(JsonToken token)
		{
			if (this.wasPeeked)
			{
				return this.currentToken != null && this.currentToken == token;
			}
			if (this.Read())
			{
				this.wasPeeked = true;
				return this.currentToken == token;
			}
			return false;
		}

		// Token: 0x06000C9E RID: 3230 RVA: 0x0001DD40 File Offset: 0x0001BF40
		public override string ReadText()
		{
			object value = this.jsonReader.Value;
			JsonToken? jsonToken = this.currentToken;
			if (jsonToken != null)
			{
				string result;
				switch (jsonToken.GetValueOrDefault())
				{
				case JsonToken.PropertyName:
				case JsonToken.String:
					result = (value as string);
					break;
				case JsonToken.ObjectEnd:
				case JsonToken.ArrayStart:
				case JsonToken.ArrayEnd:
					goto IL_BC;
				case JsonToken.Int:
				case JsonToken.UInt:
				case JsonToken.Long:
				case JsonToken.ULong:
				case JsonToken.Boolean:
				{
					IFormattable formattable = value as IFormattable;
					if (formattable != null)
					{
						result = formattable.ToString(null, CultureInfo.InvariantCulture);
					}
					else
					{
						result = value.ToString();
					}
					break;
				}
				case JsonToken.Double:
				{
					IFormattable formattable2 = value as IFormattable;
					if (formattable2 != null)
					{
						result = formattable2.ToString("R", CultureInfo.InvariantCulture);
					}
					else
					{
						result = value.ToString();
					}
					break;
				}
				case JsonToken.Null:
					result = null;
					break;
				default:
					goto IL_BC;
				}
				return result;
			}
			IL_BC:
			throw new AmazonClientException("We expected a VALUE token but got: " + this.currentToken);
		}

		// Token: 0x170003C2 RID: 962
		// (get) Token: 0x06000C9F RID: 3231 RVA: 0x0001DE25 File Offset: 0x0001C025
		public JsonToken CurrentTokenType
		{
			get
			{
				return this.currentToken.Value;
			}
		}

		// Token: 0x170003C3 RID: 963
		// (get) Token: 0x06000CA0 RID: 3232 RVA: 0x0001DE32 File Offset: 0x0001C032
		public Stream Stream
		{
			get
			{
				return this.streamReader.BaseStream;
			}
		}

		// Token: 0x06000CA1 RID: 3233 RVA: 0x0001DE3F File Offset: 0x0001C03F
		public int Peek()
		{
			while (char.IsWhiteSpace((char)this.StreamPeek()))
			{
				this.streamReader.Read();
			}
			return this.StreamPeek();
		}

		// Token: 0x06000CA2 RID: 3234 RVA: 0x0001DE64 File Offset: 0x0001C064
		private int StreamPeek()
		{
			int num = this.streamReader.Peek();
			if (num == -1)
			{
				this.streamReader.DiscardBufferedData();
				num = this.streamReader.Peek();
			}
			return num;
		}

		// Token: 0x06000CA3 RID: 3235 RVA: 0x0001DE9C File Offset: 0x0001C09C
		private void UpdateContext()
		{
			if (this.currentToken == null)
			{
				return;
			}
			if (this.currentToken.Value == JsonToken.ObjectStart || this.currentToken.Value == JsonToken.ArrayStart)
			{
				this.stack.Push("/");
				return;
			}
			if (this.currentToken.Value == JsonToken.ObjectEnd || this.currentToken.Value == JsonToken.ArrayEnd)
			{
				if (this.stack.Peek() == "/")
				{
					this.stack.Pop();
					if (this.stack.Count > 0 && this.stack.Peek() != "/")
					{
						this.stack.Pop();
					}
				}
				this.currentField = null;
				return;
			}
			if (this.currentToken.Value == JsonToken.PropertyName)
			{
				string text = this.ReadText();
				this.currentField = text;
				this.stack.Push(this.currentField);
				return;
			}
			if (this.currentToken.Value != JsonToken.None && !this.stack.CurrentPath.EndsWith("/", StringComparison.OrdinalIgnoreCase))
			{
				this.stack.Pop();
			}
		}

		// Token: 0x06000CA4 RID: 3236 RVA: 0x0001DFB1 File Offset: 0x0001C1B1
		protected override void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing && this.streamReader != null)
				{
					this.streamReader.Dispose();
					this.streamReader = null;
				}
				this.disposed = true;
			}
			base.Dispose(disposing);
		}

		// Token: 0x040004B6 RID: 1206
		private const string DELIMITER = "/";

		// Token: 0x040004B7 RID: 1207
		private StreamReader streamReader;

		// Token: 0x040004B8 RID: 1208
		private JsonReader jsonReader;

		// Token: 0x040004B9 RID: 1209
		private JsonUnmarshallerContext.JsonPathStack stack = new JsonUnmarshallerContext.JsonPathStack();

		// Token: 0x040004BA RID: 1210
		private string currentField;

		// Token: 0x040004BB RID: 1211
		private JsonToken? currentToken;

		// Token: 0x040004BC RID: 1212
		private bool disposed;

		// Token: 0x040004BD RID: 1213
		private bool wasPeeked;

		// Token: 0x020001F3 RID: 499
		private class JsonPathStack
		{
			// Token: 0x1700045B RID: 1115
			// (get) Token: 0x06000FCD RID: 4045 RVA: 0x00028354 File Offset: 0x00026554
			public int CurrentDepth
			{
				get
				{
					return this.currentDepth;
				}
			}

			// Token: 0x1700045C RID: 1116
			// (get) Token: 0x06000FCE RID: 4046 RVA: 0x0002835C File Offset: 0x0002655C
			public string CurrentPath
			{
				get
				{
					if (this.stackString == null)
					{
						this.stackString = this.stackStringBuilder.ToString();
					}
					return this.stackString;
				}
			}

			// Token: 0x06000FCF RID: 4047 RVA: 0x0002837D File Offset: 0x0002657D
			public void Push(string value)
			{
				if (value == "/")
				{
					this.currentDepth++;
				}
				this.stackStringBuilder.Append(value);
				this.stackString = null;
				this.stack.Push(value);
			}

			// Token: 0x06000FD0 RID: 4048 RVA: 0x000283BC File Offset: 0x000265BC
			public string Pop()
			{
				string text = this.stack.Pop();
				if (text == "/")
				{
					this.currentDepth--;
				}
				this.stackStringBuilder.Remove(this.stackStringBuilder.Length - text.Length, text.Length);
				this.stackString = null;
				return text;
			}

			// Token: 0x06000FD1 RID: 4049 RVA: 0x0002841C File Offset: 0x0002661C
			public string Peek()
			{
				return this.stack.Peek();
			}

			// Token: 0x1700045D RID: 1117
			// (get) Token: 0x06000FD2 RID: 4050 RVA: 0x00028429 File Offset: 0x00026629
			public int Count
			{
				get
				{
					return this.stack.Count;
				}
			}

			// Token: 0x04000A05 RID: 2565
			private Stack<string> stack = new Stack<string>();

			// Token: 0x04000A06 RID: 2566
			private int currentDepth;

			// Token: 0x04000A07 RID: 2567
			private StringBuilder stackStringBuilder = new StringBuilder(128);

			// Token: 0x04000A08 RID: 2568
			private string stackString;
		}
	}
}
