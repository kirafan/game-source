﻿using System;
using System.IO;
using System.Net;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200014D RID: 333
	public abstract class XmlResponseUnmarshaller : ResponseUnmarshaller
	{
		// Token: 0x06000CAE RID: 3246 RVA: 0x0001E060 File Offset: 0x0001C260
		public override AmazonWebServiceResponse Unmarshall(UnmarshallerContext input)
		{
			XmlUnmarshallerContext xmlUnmarshallerContext = input as XmlUnmarshallerContext;
			if (xmlUnmarshallerContext == null)
			{
				throw new InvalidOperationException("Unsupported UnmarshallerContext");
			}
			AmazonWebServiceResponse amazonWebServiceResponse = this.Unmarshall(xmlUnmarshallerContext);
			if (xmlUnmarshallerContext.ResponseData.IsHeaderPresent("x-amzn-RequestId") && !string.IsNullOrEmpty(xmlUnmarshallerContext.ResponseData.GetHeaderValue("x-amzn-RequestId")))
			{
				if (amazonWebServiceResponse.ResponseMetadata == null)
				{
					amazonWebServiceResponse.ResponseMetadata = new ResponseMetadata();
				}
				amazonWebServiceResponse.ResponseMetadata.RequestId = xmlUnmarshallerContext.ResponseData.GetHeaderValue("x-amzn-RequestId");
			}
			else if (xmlUnmarshallerContext.ResponseData.IsHeaderPresent("x-amz-request-id") && !string.IsNullOrEmpty(xmlUnmarshallerContext.ResponseData.GetHeaderValue("x-amz-request-id")))
			{
				if (amazonWebServiceResponse.ResponseMetadata == null)
				{
					amazonWebServiceResponse.ResponseMetadata = new ResponseMetadata();
				}
				amazonWebServiceResponse.ResponseMetadata.RequestId = xmlUnmarshallerContext.ResponseData.GetHeaderValue("x-amz-request-id");
			}
			return amazonWebServiceResponse;
		}

		// Token: 0x06000CAF RID: 3247 RVA: 0x0001E13C File Offset: 0x0001C33C
		public override AmazonServiceException UnmarshallException(UnmarshallerContext input, Exception innerException, HttpStatusCode statusCode)
		{
			XmlUnmarshallerContext xmlUnmarshallerContext = input as XmlUnmarshallerContext;
			if (xmlUnmarshallerContext == null)
			{
				throw new InvalidOperationException("Unsupported UnmarshallerContext");
			}
			return this.UnmarshallException(xmlUnmarshallerContext, innerException, statusCode);
		}

		// Token: 0x06000CB0 RID: 3248
		public abstract AmazonWebServiceResponse Unmarshall(XmlUnmarshallerContext input);

		// Token: 0x06000CB1 RID: 3249
		public abstract AmazonServiceException UnmarshallException(XmlUnmarshallerContext input, Exception innerException, HttpStatusCode statusCode);

		// Token: 0x06000CB2 RID: 3250 RVA: 0x0001E167 File Offset: 0x0001C367
		protected override UnmarshallerContext ConstructUnmarshallerContext(Stream responseStream, bool maintainResponseBody, IWebResponseData response)
		{
			return new XmlUnmarshallerContext(responseStream, maintainResponseBody, response);
		}
	}
}
