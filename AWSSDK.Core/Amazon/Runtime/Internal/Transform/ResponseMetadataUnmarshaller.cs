﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200015C RID: 348
	public class ResponseMetadataUnmarshaller : IUnmarshaller<ResponseMetadata, XmlUnmarshallerContext>, IUnmarshaller<ResponseMetadata, JsonUnmarshallerContext>
	{
		// Token: 0x06000D02 RID: 3330 RVA: 0x0000584A File Offset: 0x00003A4A
		private ResponseMetadataUnmarshaller()
		{
		}

		// Token: 0x170003D0 RID: 976
		// (get) Token: 0x06000D03 RID: 3331 RVA: 0x0001E595 File Offset: 0x0001C795
		public static ResponseMetadataUnmarshaller Instance
		{
			get
			{
				return ResponseMetadataUnmarshaller._instance;
			}
		}

		// Token: 0x06000D04 RID: 3332 RVA: 0x0001E59C File Offset: 0x0001C79C
		public static ResponseMetadataUnmarshaller GetInstance()
		{
			return ResponseMetadataUnmarshaller.Instance;
		}

		// Token: 0x06000D05 RID: 3333 RVA: 0x0001E5A4 File Offset: 0x0001C7A4
		public ResponseMetadata Unmarshall(XmlUnmarshallerContext context)
		{
			ResponseMetadata responseMetadata = new ResponseMetadata();
			int i = context.CurrentDepth;
			while (i <= context.CurrentDepth)
			{
				context.Read();
				if (context.IsStartElement)
				{
					if (context.TestExpression("ResponseMetadata/RequestId"))
					{
						responseMetadata.RequestId = StringUnmarshaller.GetInstance().Unmarshall(context);
					}
					else
					{
						responseMetadata.Metadata.Add(context.CurrentPath.Substring(context.CurrentPath.LastIndexOf('/') + 1), StringUnmarshaller.GetInstance().Unmarshall(context));
					}
				}
			}
			return responseMetadata;
		}

		// Token: 0x06000D06 RID: 3334 RVA: 0x0001E62C File Offset: 0x0001C82C
		public ResponseMetadata Unmarshall(JsonUnmarshallerContext context)
		{
			ResponseMetadata responseMetadata = new ResponseMetadata();
			int currentDepth = context.CurrentDepth;
			while (context.CurrentDepth >= currentDepth)
			{
				context.Read();
				if (context.TestExpression("ResponseMetadata/RequestId"))
				{
					responseMetadata.RequestId = StringUnmarshaller.GetInstance().Unmarshall(context);
				}
			}
			return responseMetadata;
		}

		// Token: 0x040004C9 RID: 1225
		private static ResponseMetadataUnmarshaller _instance = new ResponseMetadataUnmarshaller();
	}
}
