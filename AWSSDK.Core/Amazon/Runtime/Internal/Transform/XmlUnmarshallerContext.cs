﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000162 RID: 354
	public class XmlUnmarshallerContext : UnmarshallerContext
	{
		// Token: 0x170003DD RID: 989
		// (get) Token: 0x06000D2F RID: 3375 RVA: 0x0001EAEA File Offset: 0x0001CCEA
		public Stream Stream
		{
			get
			{
				return this.streamReader.BaseStream;
			}
		}

		// Token: 0x170003DE RID: 990
		// (get) Token: 0x06000D30 RID: 3376 RVA: 0x0001EAF7 File Offset: 0x0001CCF7
		private XmlReader XmlReader
		{
			get
			{
				if (this._xmlReader == null)
				{
					this._xmlReader = XmlReader.Create(this.streamReader, XmlUnmarshallerContext.READER_SETTINGS);
				}
				return this._xmlReader;
			}
		}

		// Token: 0x06000D31 RID: 3377 RVA: 0x0001EB20 File Offset: 0x0001CD20
		public XmlUnmarshallerContext(Stream responseStream, bool maintainResponseBody, IWebResponseData responseData)
		{
			if (maintainResponseBody)
			{
				base.WrappingStream = new CachingWrapperStream(responseStream, AWSConfigs.LoggingConfig.LogResponsesSizeLimit);
				responseStream = base.WrappingStream;
			}
			this.streamReader = new StreamReader(responseStream);
			base.WebResponseData = responseData;
			base.MaintainResponseBody = maintainResponseBody;
		}

		// Token: 0x170003DF RID: 991
		// (get) Token: 0x06000D32 RID: 3378 RVA: 0x0001EB8F File Offset: 0x0001CD8F
		public override string CurrentPath
		{
			get
			{
				return this.stackString;
			}
		}

		// Token: 0x170003E0 RID: 992
		// (get) Token: 0x06000D33 RID: 3379 RVA: 0x0001EB97 File Offset: 0x0001CD97
		public override int CurrentDepth
		{
			get
			{
				return this.stack.Count;
			}
		}

		// Token: 0x06000D34 RID: 3380 RVA: 0x0001EBA4 File Offset: 0x0001CDA4
		public override bool Read()
		{
			if (this.attributeEnumerator != null && this.attributeEnumerator.MoveNext())
			{
				this.nodeType = XmlNodeType.Attribute;
				this.stackString = string.Format(CultureInfo.InvariantCulture, "{0}/@{1}", new object[]
				{
					XmlUnmarshallerContext.StackToPath(this.stack),
					this.attributeEnumerator.Current
				});
			}
			else
			{
				if (XmlUnmarshallerContext.nodesToSkip.Contains(this.XmlReader.NodeType))
				{
					this.XmlReader.Read();
				}
				while (this.XmlReader.IsEmptyElement)
				{
					this.XmlReader.Read();
				}
				XmlNodeType xmlNodeType = this.XmlReader.NodeType;
				if (xmlNodeType != XmlNodeType.Element)
				{
					if (xmlNodeType == XmlNodeType.EndElement)
					{
						this.nodeType = XmlNodeType.EndElement;
						this.stack.Pop();
						this.stackString = XmlUnmarshallerContext.StackToPath(this.stack);
						this.XmlReader.Read();
					}
				}
				else
				{
					this.nodeType = XmlNodeType.Element;
					this.stack.Push(this.XmlReader.LocalName);
					this.stackString = XmlUnmarshallerContext.StackToPath(this.stack);
					this.ReadElement();
				}
			}
			return this.XmlReader.ReadState != ReadState.EndOfFile && this.XmlReader.ReadState != ReadState.Error && this.XmlReader.ReadState != ReadState.Closed;
		}

		// Token: 0x06000D35 RID: 3381 RVA: 0x0001ECF2 File Offset: 0x0001CEF2
		public override string ReadText()
		{
			if (this.nodeType == XmlNodeType.Attribute)
			{
				return this.attributeValues[this.attributeEnumerator.Current];
			}
			return this.nodeContent;
		}

		// Token: 0x170003E1 RID: 993
		// (get) Token: 0x06000D36 RID: 3382 RVA: 0x0001ED1A File Offset: 0x0001CF1A
		public override bool IsStartElement
		{
			get
			{
				return this.nodeType == XmlNodeType.Element;
			}
		}

		// Token: 0x170003E2 RID: 994
		// (get) Token: 0x06000D37 RID: 3383 RVA: 0x0001ED25 File Offset: 0x0001CF25
		public override bool IsEndElement
		{
			get
			{
				return this.nodeType == XmlNodeType.EndElement;
			}
		}

		// Token: 0x170003E3 RID: 995
		// (get) Token: 0x06000D38 RID: 3384 RVA: 0x0001ED31 File Offset: 0x0001CF31
		public override bool IsStartOfDocument
		{
			get
			{
				return this.XmlReader.ReadState == ReadState.Initial;
			}
		}

		// Token: 0x170003E4 RID: 996
		// (get) Token: 0x06000D39 RID: 3385 RVA: 0x0001ED41 File Offset: 0x0001CF41
		public bool IsAttribute
		{
			get
			{
				return this.nodeType == XmlNodeType.Attribute;
			}
		}

		// Token: 0x06000D3A RID: 3386 RVA: 0x0001ED4C File Offset: 0x0001CF4C
		private static string StackToPath(Stack<string> stack)
		{
			string text = null;
			foreach (string text2 in stack.ToArray())
			{
				text = ((text == null) ? text2 : string.Format(CultureInfo.InvariantCulture, "{0}/{1}", new object[]
				{
					text2,
					text
				}));
			}
			return "/" + text;
		}

		// Token: 0x06000D3B RID: 3387 RVA: 0x0001EDA4 File Offset: 0x0001CFA4
		private void ReadElement()
		{
			if (this.XmlReader.HasAttributes)
			{
				this.attributeValues = new Dictionary<string, string>();
				this.attributeNames = new List<string>();
				while (this.XmlReader.MoveToNextAttribute())
				{
					this.attributeValues.Add(this.XmlReader.LocalName, this.XmlReader.Value);
					this.attributeNames.Add(this.XmlReader.LocalName);
				}
				this.attributeEnumerator = this.attributeNames.GetEnumerator();
			}
			this.XmlReader.MoveToElement();
			this.XmlReader.Read();
			if (this.XmlReader.NodeType == XmlNodeType.Text)
			{
				this.nodeContent = this.XmlReader.ReadContentAsString();
				return;
			}
			this.nodeContent = string.Empty;
		}

		// Token: 0x06000D3C RID: 3388 RVA: 0x0001EE74 File Offset: 0x0001D074
		protected override void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					if (this.streamReader != null)
					{
						this.streamReader.Dispose();
						this.streamReader = null;
					}
					if (this._xmlReader != null)
					{
						this._xmlReader.Close();
						this._xmlReader = null;
					}
				}
				this.disposed = true;
			}
			base.Dispose(disposing);
		}

		// Token: 0x040004D4 RID: 1236
		private static readonly XmlReaderSettings READER_SETTINGS = new XmlReaderSettings
		{
			ProhibitDtd = false,
			IgnoreWhitespace = true
		};

		// Token: 0x040004D5 RID: 1237
		private static HashSet<XmlNodeType> nodesToSkip = new HashSet<XmlNodeType>
		{
			XmlNodeType.None,
			XmlNodeType.XmlDeclaration,
			XmlNodeType.Comment,
			XmlNodeType.DocumentType
		};

		// Token: 0x040004D6 RID: 1238
		private StreamReader streamReader;

		// Token: 0x040004D7 RID: 1239
		private XmlReader _xmlReader;

		// Token: 0x040004D8 RID: 1240
		private Stack<string> stack = new Stack<string>();

		// Token: 0x040004D9 RID: 1241
		private string stackString = "";

		// Token: 0x040004DA RID: 1242
		private Dictionary<string, string> attributeValues;

		// Token: 0x040004DB RID: 1243
		private List<string> attributeNames;

		// Token: 0x040004DC RID: 1244
		private IEnumerator<string> attributeEnumerator;

		// Token: 0x040004DD RID: 1245
		private XmlNodeType nodeType;

		// Token: 0x040004DE RID: 1246
		private string nodeContent = string.Empty;

		// Token: 0x040004DF RID: 1247
		private bool disposed;
	}
}
