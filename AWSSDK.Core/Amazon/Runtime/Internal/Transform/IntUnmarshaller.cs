﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000151 RID: 337
	public class IntUnmarshaller : IUnmarshaller<int, XmlUnmarshallerContext>, IUnmarshaller<int, JsonUnmarshallerContext>
	{
		// Token: 0x06000CC0 RID: 3264 RVA: 0x0000584A File Offset: 0x00003A4A
		private IntUnmarshaller()
		{
		}

		// Token: 0x170003C5 RID: 965
		// (get) Token: 0x06000CC1 RID: 3265 RVA: 0x0001E31A File Offset: 0x0001C51A
		public static IntUnmarshaller Instance
		{
			get
			{
				return IntUnmarshaller._instance;
			}
		}

		// Token: 0x06000CC2 RID: 3266 RVA: 0x0001E321 File Offset: 0x0001C521
		public static IntUnmarshaller GetInstance()
		{
			return IntUnmarshaller.Instance;
		}

		// Token: 0x06000CC3 RID: 3267 RVA: 0x0001E328 File Offset: 0x0001C528
		public int Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<int>.Unmarshall(context);
		}

		// Token: 0x06000CC4 RID: 3268 RVA: 0x0001E330 File Offset: 0x0001C530
		public int Unmarshall(JsonUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<int>.Unmarshall(context);
		}

		// Token: 0x040004BE RID: 1214
		private static IntUnmarshaller _instance = new IntUnmarshaller();
	}
}
