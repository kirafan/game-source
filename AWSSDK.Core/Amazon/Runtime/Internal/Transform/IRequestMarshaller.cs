﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000142 RID: 322
	public interface IRequestMarshaller<R, T> where T : MarshallerContext
	{
		// Token: 0x06000C7E RID: 3198
		void Marshall(R requestObject, T context);
	}
}
