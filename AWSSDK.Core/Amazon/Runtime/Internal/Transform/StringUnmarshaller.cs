﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000157 RID: 343
	public class StringUnmarshaller : IUnmarshaller<string, XmlUnmarshallerContext>, IUnmarshaller<string, JsonUnmarshallerContext>
	{
		// Token: 0x06000CE4 RID: 3300 RVA: 0x0000584A File Offset: 0x00003A4A
		private StringUnmarshaller()
		{
		}

		// Token: 0x170003CB RID: 971
		// (get) Token: 0x06000CE5 RID: 3301 RVA: 0x0001E416 File Offset: 0x0001C616
		public static StringUnmarshaller Instance
		{
			get
			{
				return StringUnmarshaller._instance;
			}
		}

		// Token: 0x06000CE6 RID: 3302 RVA: 0x0001E41D File Offset: 0x0001C61D
		public static StringUnmarshaller GetInstance()
		{
			return StringUnmarshaller.Instance;
		}

		// Token: 0x06000CE7 RID: 3303 RVA: 0x0001E424 File Offset: 0x0001C624
		public string Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<string>.Unmarshall(context);
		}

		// Token: 0x06000CE8 RID: 3304 RVA: 0x0001E42C File Offset: 0x0001C62C
		public string Unmarshall(JsonUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<string>.Unmarshall(context);
		}

		// Token: 0x040004C4 RID: 1220
		private static StringUnmarshaller _instance = new StringUnmarshaller();
	}
}
