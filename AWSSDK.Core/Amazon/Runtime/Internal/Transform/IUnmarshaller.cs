﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000147 RID: 327
	public interface IUnmarshaller<T, R>
	{
		// Token: 0x06000C89 RID: 3209
		T Unmarshall(R input);
	}
}
