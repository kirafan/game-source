﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000163 RID: 355
	public class EC2UnmarshallerContext : XmlUnmarshallerContext
	{
		// Token: 0x06000D3E RID: 3390 RVA: 0x0001EF21 File Offset: 0x0001D121
		public EC2UnmarshallerContext(Stream responseStream, bool maintainResponseBody, IWebResponseData responseData) : base(responseStream, maintainResponseBody, responseData)
		{
		}

		// Token: 0x170003E5 RID: 997
		// (get) Token: 0x06000D3F RID: 3391 RVA: 0x0001EF2C File Offset: 0x0001D12C
		// (set) Token: 0x06000D40 RID: 3392 RVA: 0x0001EF34 File Offset: 0x0001D134
		public string RequestId { get; private set; }

		// Token: 0x06000D41 RID: 3393 RVA: 0x0001EF3D File Offset: 0x0001D13D
		public override bool Read()
		{
			bool result = base.Read();
			if (this.RequestId == null && this.IsStartElement && base.TestExpression("RequestId", 2))
			{
				this.RequestId = StringUnmarshaller.GetInstance().Unmarshall(this);
			}
			return result;
		}
	}
}
