﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000154 RID: 340
	public class DoubleUnmarshaller : IUnmarshaller<double, XmlUnmarshallerContext>, IUnmarshaller<double, JsonUnmarshallerContext>
	{
		// Token: 0x06000CD2 RID: 3282 RVA: 0x0000584A File Offset: 0x00003A4A
		private DoubleUnmarshaller()
		{
		}

		// Token: 0x170003C8 RID: 968
		// (get) Token: 0x06000CD3 RID: 3283 RVA: 0x0001E398 File Offset: 0x0001C598
		public static DoubleUnmarshaller Instance
		{
			get
			{
				return DoubleUnmarshaller._instance;
			}
		}

		// Token: 0x06000CD4 RID: 3284 RVA: 0x0001E39F File Offset: 0x0001C59F
		public static DoubleUnmarshaller GetInstance()
		{
			return DoubleUnmarshaller.Instance;
		}

		// Token: 0x06000CD5 RID: 3285 RVA: 0x0001E3A6 File Offset: 0x0001C5A6
		public double Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<double>.Unmarshall(context);
		}

		// Token: 0x06000CD6 RID: 3286 RVA: 0x0001E3AE File Offset: 0x0001C5AE
		public double Unmarshall(JsonUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<double>.Unmarshall(context);
		}

		// Token: 0x040004C1 RID: 1217
		private static DoubleUnmarshaller _instance = new DoubleUnmarshaller();
	}
}
