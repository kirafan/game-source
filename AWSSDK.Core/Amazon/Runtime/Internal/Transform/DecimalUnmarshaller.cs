﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000155 RID: 341
	public class DecimalUnmarshaller : IUnmarshaller<decimal, XmlUnmarshallerContext>, IUnmarshaller<decimal, JsonUnmarshallerContext>
	{
		// Token: 0x06000CD8 RID: 3288 RVA: 0x0000584A File Offset: 0x00003A4A
		private DecimalUnmarshaller()
		{
		}

		// Token: 0x170003C9 RID: 969
		// (get) Token: 0x06000CD9 RID: 3289 RVA: 0x0001E3C2 File Offset: 0x0001C5C2
		public static DecimalUnmarshaller Instance
		{
			get
			{
				return DecimalUnmarshaller._instance;
			}
		}

		// Token: 0x06000CDA RID: 3290 RVA: 0x0001E3C9 File Offset: 0x0001C5C9
		public static DecimalUnmarshaller GetInstance()
		{
			return DecimalUnmarshaller.Instance;
		}

		// Token: 0x06000CDB RID: 3291 RVA: 0x0001E3D0 File Offset: 0x0001C5D0
		public decimal Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<decimal>.Unmarshall(context);
		}

		// Token: 0x06000CDC RID: 3292 RVA: 0x0001E3D8 File Offset: 0x0001C5D8
		public decimal Unmarshall(JsonUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<decimal>.Unmarshall(context);
		}

		// Token: 0x040004C2 RID: 1218
		private static DecimalUnmarshaller _instance = new DecimalUnmarshaller();
	}
}
