﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000140 RID: 320
	public class ErrorResponseUnmarshaller : IUnmarshaller<ErrorResponse, XmlUnmarshallerContext>
	{
		// Token: 0x06000C7A RID: 3194 RVA: 0x0001D87C File Offset: 0x0001BA7C
		public ErrorResponse Unmarshall(XmlUnmarshallerContext context)
		{
			ErrorResponse errorResponse = new ErrorResponse();
			while (context.Read())
			{
				if (context.IsStartElement)
				{
					if (context.TestExpression("Error/Type"))
					{
						try
						{
							errorResponse.Type = (ErrorType)Enum.Parse(typeof(ErrorType), StringUnmarshaller.GetInstance().Unmarshall(context), true);
							continue;
						}
						catch (ArgumentException)
						{
							errorResponse.Type = ErrorType.Unknown;
							continue;
						}
					}
					if (context.TestExpression("Error/Code"))
					{
						errorResponse.Code = StringUnmarshaller.GetInstance().Unmarshall(context);
					}
					else if (context.TestExpression("Error/Message"))
					{
						errorResponse.Message = StringUnmarshaller.GetInstance().Unmarshall(context);
					}
					else if (context.TestExpression("RequestId"))
					{
						errorResponse.RequestId = StringUnmarshaller.GetInstance().Unmarshall(context);
					}
				}
			}
			return errorResponse;
		}

		// Token: 0x06000C7B RID: 3195 RVA: 0x0001D958 File Offset: 0x0001BB58
		public static ErrorResponseUnmarshaller GetInstance()
		{
			if (ErrorResponseUnmarshaller.instance == null)
			{
				ErrorResponseUnmarshaller.instance = new ErrorResponseUnmarshaller();
			}
			return ErrorResponseUnmarshaller.instance;
		}

		// Token: 0x040004B1 RID: 1201
		private static ErrorResponseUnmarshaller instance;
	}
}
