﻿using System;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000145 RID: 325
	public class JsonMarshallerContext : MarshallerContext
	{
		// Token: 0x170003B7 RID: 951
		// (get) Token: 0x06000C85 RID: 3205 RVA: 0x0001D9B1 File Offset: 0x0001BBB1
		// (set) Token: 0x06000C86 RID: 3206 RVA: 0x0001D9B9 File Offset: 0x0001BBB9
		public JsonWriter Writer { get; private set; }

		// Token: 0x06000C87 RID: 3207 RVA: 0x0001D9C2 File Offset: 0x0001BBC2
		public JsonMarshallerContext(IRequest request, JsonWriter writer) : base(request)
		{
			this.Writer = writer;
		}
	}
}
