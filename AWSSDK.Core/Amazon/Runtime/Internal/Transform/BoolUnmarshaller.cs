﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000156 RID: 342
	public class BoolUnmarshaller : IUnmarshaller<bool, XmlUnmarshallerContext>, IUnmarshaller<bool, JsonUnmarshallerContext>
	{
		// Token: 0x06000CDE RID: 3294 RVA: 0x0000584A File Offset: 0x00003A4A
		private BoolUnmarshaller()
		{
		}

		// Token: 0x170003CA RID: 970
		// (get) Token: 0x06000CDF RID: 3295 RVA: 0x0001E3EC File Offset: 0x0001C5EC
		public static BoolUnmarshaller Instance
		{
			get
			{
				return BoolUnmarshaller._instance;
			}
		}

		// Token: 0x06000CE0 RID: 3296 RVA: 0x0001E3F3 File Offset: 0x0001C5F3
		public static BoolUnmarshaller GetInstance()
		{
			return BoolUnmarshaller.Instance;
		}

		// Token: 0x06000CE1 RID: 3297 RVA: 0x0001E3FA File Offset: 0x0001C5FA
		public bool Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<bool>.Unmarshall(context);
		}

		// Token: 0x06000CE2 RID: 3298 RVA: 0x0001E402 File Offset: 0x0001C602
		public bool Unmarshall(JsonUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<bool>.Unmarshall(context);
		}

		// Token: 0x040004C3 RID: 1219
		private static BoolUnmarshaller _instance = new BoolUnmarshaller();
	}
}
