﻿using System;
using System.Globalization;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000159 RID: 345
	public class DateTimeUnmarshaller : IUnmarshaller<DateTime, XmlUnmarshallerContext>, IUnmarshaller<DateTime, JsonUnmarshallerContext>
	{
		// Token: 0x06000CF0 RID: 3312 RVA: 0x0000584A File Offset: 0x00003A4A
		private DateTimeUnmarshaller()
		{
		}

		// Token: 0x170003CD RID: 973
		// (get) Token: 0x06000CF1 RID: 3313 RVA: 0x0001E46A File Offset: 0x0001C66A
		public static DateTimeUnmarshaller Instance
		{
			get
			{
				return DateTimeUnmarshaller._instance;
			}
		}

		// Token: 0x06000CF2 RID: 3314 RVA: 0x0001E471 File Offset: 0x0001C671
		public static DateTimeUnmarshaller GetInstance()
		{
			return DateTimeUnmarshaller.Instance;
		}

		// Token: 0x06000CF3 RID: 3315 RVA: 0x0001E478 File Offset: 0x0001C678
		public DateTime Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<DateTime>.Unmarshall(context);
		}

		// Token: 0x06000CF4 RID: 3316 RVA: 0x0001E480 File Offset: 0x0001C680
		public DateTime Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			string text = context.ReadText();
			double value;
			if (double.TryParse(text, NumberStyles.Any, CultureInfo.InvariantCulture, out value))
			{
				DateTime result = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
				result = result.AddSeconds(value);
				return result;
			}
			if (text == null)
			{
				return default(DateTime);
			}
			return (DateTime)Convert.ChangeType(text, typeof(DateTime), CultureInfo.InvariantCulture);
		}

		// Token: 0x040004C6 RID: 1222
		private static DateTimeUnmarshaller _instance = new DateTimeUnmarshaller();
	}
}
