﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200015D RID: 349
	public class KeyValueUnmarshaller<K, V, KUnmarshaller, VUnmarshaller> : IUnmarshaller<KeyValuePair<K, V>, XmlUnmarshallerContext>, IUnmarshaller<KeyValuePair<K, V>, JsonUnmarshallerContext> where KUnmarshaller : IUnmarshaller<K, XmlUnmarshallerContext>, IUnmarshaller<K, JsonUnmarshallerContext> where VUnmarshaller : IUnmarshaller<V, XmlUnmarshallerContext>, IUnmarshaller<V, JsonUnmarshallerContext>
	{
		// Token: 0x06000D08 RID: 3336 RVA: 0x0001E683 File Offset: 0x0001C883
		public KeyValueUnmarshaller(KUnmarshaller keyUnmarshaller, VUnmarshaller valueUnmarshaller)
		{
			this.keyUnmarshaller = keyUnmarshaller;
			this.valueUnmarshaller = valueUnmarshaller;
		}

		// Token: 0x06000D09 RID: 3337 RVA: 0x0001E69C File Offset: 0x0001C89C
		public KeyValuePair<K, V> Unmarshall(XmlUnmarshallerContext context)
		{
			K key = default(K);
			V value = default(V);
			int currentDepth = context.CurrentDepth;
			int startingStackDepth = currentDepth + 1;
			while (context.Read())
			{
				if (context.TestExpression("key", startingStackDepth))
				{
					key = this.keyUnmarshaller.Unmarshall(context);
				}
				else if (context.TestExpression("name", startingStackDepth))
				{
					key = this.keyUnmarshaller.Unmarshall(context);
				}
				else if (context.TestExpression("value", startingStackDepth))
				{
					value = this.valueUnmarshaller.Unmarshall(context);
				}
				else if (context.IsEndElement && context.CurrentDepth < currentDepth)
				{
					break;
				}
			}
			return new KeyValuePair<K, V>(key, value);
		}

		// Token: 0x06000D0A RID: 3338 RVA: 0x0001E754 File Offset: 0x0001C954
		public KeyValuePair<K, V> Unmarshall(JsonUnmarshallerContext context)
		{
			K key = this.keyUnmarshaller.Unmarshall(context);
			V value = this.valueUnmarshaller.Unmarshall(context);
			return new KeyValuePair<K, V>(key, value);
		}

		// Token: 0x040004CA RID: 1226
		private KUnmarshaller keyUnmarshaller;

		// Token: 0x040004CB RID: 1227
		private VUnmarshaller valueUnmarshaller;
	}
}
