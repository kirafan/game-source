﻿using System;
using System.IO;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200015B RID: 347
	public class MemoryStreamUnmarshaller : IUnmarshaller<MemoryStream, XmlUnmarshallerContext>, IUnmarshaller<MemoryStream, JsonUnmarshallerContext>
	{
		// Token: 0x06000CFC RID: 3324 RVA: 0x0000584A File Offset: 0x00003A4A
		private MemoryStreamUnmarshaller()
		{
		}

		// Token: 0x170003CF RID: 975
		// (get) Token: 0x06000CFD RID: 3325 RVA: 0x0001E544 File Offset: 0x0001C744
		public static MemoryStreamUnmarshaller Instance
		{
			get
			{
				return MemoryStreamUnmarshaller._instance;
			}
		}

		// Token: 0x06000CFE RID: 3326 RVA: 0x0001E54B File Offset: 0x0001C74B
		public static MemoryStreamUnmarshaller GetInstance()
		{
			return MemoryStreamUnmarshaller.Instance;
		}

		// Token: 0x06000CFF RID: 3327 RVA: 0x0001E552 File Offset: 0x0001C752
		public MemoryStream Unmarshall(XmlUnmarshallerContext context)
		{
			return new MemoryStream(Convert.FromBase64String(context.ReadText()));
		}

		// Token: 0x06000D00 RID: 3328 RVA: 0x0001E564 File Offset: 0x0001C764
		public MemoryStream Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return null;
			}
			return new MemoryStream(Convert.FromBase64String(context.ReadText()));
		}

		// Token: 0x040004C8 RID: 1224
		private static MemoryStreamUnmarshaller _instance = new MemoryStreamUnmarshaller();
	}
}
