﻿using System;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200013F RID: 319
	public static class CustomMarshallTransformations
	{
		// Token: 0x06000C79 RID: 3193 RVA: 0x0001D840 File Offset: 0x0001BA40
		public static long ConvertDateTimeToEpochMilliseconds(DateTime dateTime)
		{
			TimeSpan timeSpan = new TimeSpan(dateTime.ToUniversalTime().Ticks - AWSSDKUtils.EPOCH_START.Ticks);
			return (long)timeSpan.TotalMilliseconds;
		}
	}
}
