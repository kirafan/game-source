﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200014A RID: 330
	public class JsonErrorResponseUnmarshaller : IUnmarshaller<ErrorResponse, JsonUnmarshallerContext>
	{
		// Token: 0x06000C93 RID: 3219 RVA: 0x0001D9D4 File Offset: 0x0001BBD4
		public ErrorResponse Unmarshall(JsonUnmarshallerContext context)
		{
			ErrorResponse result;
			if (context.Peek() == 60)
			{
				ErrorResponseUnmarshaller errorResponseUnmarshaller = new ErrorResponseUnmarshaller();
				XmlUnmarshallerContext context2 = new XmlUnmarshallerContext(context.Stream, false, null);
				result = errorResponseUnmarshaller.Unmarshall(context2);
			}
			else
			{
				string text = null;
				string message = null;
				string text2 = null;
				while (context.Read())
				{
					if (context.TestExpression("__type"))
					{
						text = StringUnmarshaller.GetInstance().Unmarshall(context);
					}
					else if (context.TestExpression("message"))
					{
						message = StringUnmarshaller.GetInstance().Unmarshall(context);
					}
					else if (context.TestExpression("code"))
					{
						text2 = StringUnmarshaller.GetInstance().Unmarshall(context);
					}
				}
				if (string.IsNullOrEmpty(text) && context.ResponseData.IsHeaderPresent("x-amzn-ErrorType"))
				{
					string text3 = context.ResponseData.GetHeaderValue("x-amzn-ErrorType");
					if (!string.IsNullOrEmpty(text3))
					{
						int num = text3.IndexOf(":", StringComparison.Ordinal);
						if (num != -1)
						{
							text3 = text3.Substring(0, num);
						}
						text = text3;
					}
				}
				if (context.ResponseData.IsHeaderPresent("x-amzn-error-message"))
				{
					string headerValue = context.ResponseData.GetHeaderValue("x-amzn-error-message");
					if (!string.IsNullOrEmpty(headerValue))
					{
						message = headerValue;
					}
				}
				if (string.IsNullOrEmpty(text) && !string.IsNullOrEmpty(text2))
				{
					text = text2;
				}
				text = text.Substring(text.LastIndexOf("#", StringComparison.Ordinal) + 1);
				result = new ErrorResponse
				{
					Code = text,
					Message = message,
					Type = ErrorType.Unknown
				};
			}
			return result;
		}

		// Token: 0x06000C94 RID: 3220 RVA: 0x0001DB38 File Offset: 0x0001BD38
		public static JsonErrorResponseUnmarshaller GetInstance()
		{
			if (JsonErrorResponseUnmarshaller.instance == null)
			{
				JsonErrorResponseUnmarshaller.instance = new JsonErrorResponseUnmarshaller();
			}
			return JsonErrorResponseUnmarshaller.instance;
		}

		// Token: 0x040004B5 RID: 1205
		private static JsonErrorResponseUnmarshaller instance;
	}
}
