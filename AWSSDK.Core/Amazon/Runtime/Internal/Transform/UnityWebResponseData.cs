﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Amazon.Runtime.Internal.Util;
using UnityEngine;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000164 RID: 356
	public sealed class UnityWebResponseData : IWebResponseData, IHttpResponseBody, IDisposable
	{
		// Token: 0x06000D42 RID: 3394 RVA: 0x0001EF74 File Offset: 0x0001D174
		public UnityWebResponseData(UnityWebRequestWrapper unityWebRequest)
		{
			this.CopyHeaderValues(unityWebRequest.ResponseHeaders);
			if (!unityWebRequest.IsError)
			{
				this._responseBody = unityWebRequest.DownloadHandler.Data;
				if (this._responseBody == null)
				{
					this._responseBody = new byte[0];
				}
				this._responseStream = new MemoryStream(this._responseBody);
				this.ContentLength = (long)this._responseBody.Length;
				string contentType = null;
				this._headers.TryGetValue("Content-Type", out contentType);
				this.ContentType = contentType;
				if (unityWebRequest.StatusCode != null)
				{
					this.StatusCode = unityWebRequest.StatusCode.Value;
				}
				this.IsSuccessStatusCode = (this.StatusCode >= HttpStatusCode.OK && this.StatusCode <= (HttpStatusCode)299);
				return;
			}
			this.IsSuccessStatusCode = false;
			this._responseBody = Encoding.UTF8.GetBytes(unityWebRequest.Error);
			this._responseStream = new MemoryStream(this._responseBody);
			if (unityWebRequest.DownloadedBytes > 0UL)
			{
				this.ContentLength = (long)unityWebRequest.DownloadedBytes;
			}
			else
			{
				string s = null;
				if (this._headers.TryGetValue("Content-Length", out s))
				{
					long contentLength;
					if (long.TryParse(s, out contentLength))
					{
						this.ContentLength = contentLength;
					}
					else
					{
						this.ContentLength = 0L;
					}
				}
				else
				{
					this.ContentLength = (long)this._responseBody.Length;
				}
			}
			if (unityWebRequest.StatusCode != null)
			{
				this.StatusCode = unityWebRequest.StatusCode.Value;
				return;
			}
			string value = null;
			if (this._headers.TryGetValue("Status", out value))
			{
				this.StatusCode = (HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), value);
				return;
			}
			this.StatusCode = (HttpStatusCode)0;
		}

		// Token: 0x06000D43 RID: 3395 RVA: 0x0001F134 File Offset: 0x0001D334
		public UnityWebResponseData(WWW wwwRequest)
		{
			this._logger = Amazon.Runtime.Internal.Util.Logger.GetLogger(base.GetType());
			this.CopyHeaderValues(wwwRequest.responseHeaders);
			try
			{
				this._responseBody = wwwRequest.bytes;
			}
			catch (Exception)
			{
				this._responseBody = null;
			}
			if ((this._responseBody != null && this._responseBody.Length != 0) || (this._responseBody.Length == 0 && wwwRequest.error == null))
			{
				this._responseStream = new MemoryStream(this._responseBody);
			}
			this.ContentLength = (long)wwwRequest.bytesDownloaded;
			string contentType = null;
			this._headers.TryGetValue("Content-Type", out contentType);
			this.ContentType = contentType;
			try
			{
				int num;
				if (string.IsNullOrEmpty(wwwRequest.error))
				{
					string empty = string.Empty;
					if (this._headers.TryGetValue("Status", out empty))
					{
						this.StatusCode = (HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), empty.Substring(9, 3).Trim());
					}
					else
					{
						this.StatusCode = (HttpStatusCode)0;
					}
				}
				else if (int.TryParse(wwwRequest.error.Substring(0, 3), out num))
				{
					this.StatusCode = (HttpStatusCode)Enum.Parse(typeof(HttpStatusCode), wwwRequest.error.Substring(3).Replace(" ", "").Replace(":", "").Trim(), true);
				}
				else
				{
					this.StatusCode = (HttpStatusCode)0;
				}
			}
			catch
			{
				this.StatusCode = (HttpStatusCode)0;
			}
			this.IsSuccessStatusCode = (wwwRequest.error == null);
		}

		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x06000D44 RID: 3396 RVA: 0x0001F2D8 File Offset: 0x0001D4D8
		// (set) Token: 0x06000D45 RID: 3397 RVA: 0x0001F2E0 File Offset: 0x0001D4E0
		public long ContentLength { get; private set; }

		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x06000D46 RID: 3398 RVA: 0x0001F2E9 File Offset: 0x0001D4E9
		// (set) Token: 0x06000D47 RID: 3399 RVA: 0x0001F2F1 File Offset: 0x0001D4F1
		public string ContentType { get; private set; }

		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x06000D48 RID: 3400 RVA: 0x0001F2FA File Offset: 0x0001D4FA
		// (set) Token: 0x06000D49 RID: 3401 RVA: 0x0001F302 File Offset: 0x0001D502
		public HttpStatusCode StatusCode { get; private set; }

		// Token: 0x170003E9 RID: 1001
		// (get) Token: 0x06000D4A RID: 3402 RVA: 0x0001F30B File Offset: 0x0001D50B
		// (set) Token: 0x06000D4B RID: 3403 RVA: 0x0001F313 File Offset: 0x0001D513
		public bool IsSuccessStatusCode { get; private set; }

		// Token: 0x06000D4C RID: 3404 RVA: 0x0001F31C File Offset: 0x0001D51C
		public bool IsHeaderPresent(string headerName)
		{
			return this._headerNamesSet.Contains(headerName);
		}

		// Token: 0x06000D4D RID: 3405 RVA: 0x0001F32A File Offset: 0x0001D52A
		public string[] GetHeaderNames()
		{
			return this._headerNames;
		}

		// Token: 0x06000D4E RID: 3406 RVA: 0x0001F334 File Offset: 0x0001D534
		public string GetHeaderValue(string name)
		{
			string result;
			if (this._headers.TryGetValue(name, out result))
			{
				return result;
			}
			return string.Empty;
		}

		// Token: 0x06000D4F RID: 3407 RVA: 0x0001F358 File Offset: 0x0001D558
		private void CopyHeaderValues(Dictionary<string, string> headers)
		{
			this._headers = new Dictionary<string, string>(headers, StringComparer.OrdinalIgnoreCase);
			this._headerNames = headers.Keys.ToArray<string>();
			this._headerNamesSet = new HashSet<string>(this._headerNames, StringComparer.OrdinalIgnoreCase);
		}

		// Token: 0x170003EA RID: 1002
		// (get) Token: 0x06000D50 RID: 3408 RVA: 0x00011605 File Offset: 0x0000F805
		public IHttpResponseBody ResponseBody
		{
			get
			{
				return this;
			}
		}

		// Token: 0x170003EB RID: 1003
		// (get) Token: 0x06000D51 RID: 3409 RVA: 0x0001F392 File Offset: 0x0001D592
		public bool IsResponseBodyPresent
		{
			get
			{
				return this._responseBody != null && this._responseBody.Length != 0;
			}
		}

		// Token: 0x06000D52 RID: 3410 RVA: 0x0001F3A8 File Offset: 0x0001D5A8
		public Stream OpenResponse()
		{
			return this._responseStream;
		}

		// Token: 0x06000D53 RID: 3411 RVA: 0x0001F3B0 File Offset: 0x0001D5B0
		public void Dispose()
		{
			if (this._responseStream != null)
			{
				this._responseStream.Dispose();
				this._responseStream = null;
			}
		}

		// Token: 0x040004E1 RID: 1249
		private Dictionary<string, string> _headers;

		// Token: 0x040004E2 RID: 1250
		private string[] _headerNames;

		// Token: 0x040004E3 RID: 1251
		private HashSet<string> _headerNamesSet;

		// Token: 0x040004E4 RID: 1252
		private Stream _responseStream;

		// Token: 0x040004E5 RID: 1253
		private byte[] _responseBody;

		// Token: 0x040004E6 RID: 1254
		private Amazon.Runtime.Internal.Util.ILogger _logger;
	}
}
