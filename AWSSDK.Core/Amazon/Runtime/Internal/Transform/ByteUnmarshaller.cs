﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000158 RID: 344
	public class ByteUnmarshaller : IUnmarshaller<byte, XmlUnmarshallerContext>, IUnmarshaller<byte, JsonUnmarshallerContext>
	{
		// Token: 0x06000CEA RID: 3306 RVA: 0x0000584A File Offset: 0x00003A4A
		private ByteUnmarshaller()
		{
		}

		// Token: 0x170003CC RID: 972
		// (get) Token: 0x06000CEB RID: 3307 RVA: 0x0001E440 File Offset: 0x0001C640
		public static ByteUnmarshaller Instance
		{
			get
			{
				return ByteUnmarshaller._instance;
			}
		}

		// Token: 0x06000CEC RID: 3308 RVA: 0x0001E447 File Offset: 0x0001C647
		public static ByteUnmarshaller GetInstance()
		{
			return ByteUnmarshaller.Instance;
		}

		// Token: 0x06000CED RID: 3309 RVA: 0x0001E44E File Offset: 0x0001C64E
		public byte Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<byte>.Unmarshall(context);
		}

		// Token: 0x06000CEE RID: 3310 RVA: 0x0001E456 File Offset: 0x0001C656
		public byte Unmarshall(JsonUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<byte>.Unmarshall(context);
		}

		// Token: 0x040004C5 RID: 1221
		private static ByteUnmarshaller _instance = new ByteUnmarshaller();
	}
}
