﻿using System;
using System.Globalization;
using System.IO;
using System.Net;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200014C RID: 332
	public abstract class ResponseUnmarshaller : IResponseUnmarshaller<AmazonWebServiceResponse, UnmarshallerContext>, IUnmarshaller<AmazonWebServiceResponse, UnmarshallerContext>
	{
		// Token: 0x06000CA5 RID: 3237 RVA: 0x0001DFE6 File Offset: 0x0001C1E6
		public virtual UnmarshallerContext CreateContext(IWebResponseData response, bool readEntireResponse, Stream stream, RequestMetrics metrics)
		{
			if (response == null)
			{
				throw new AmazonServiceException("The Web Response for a successful request is null!");
			}
			return this.ConstructUnmarshallerContext(stream, this.ShouldReadEntireResponse(response, readEntireResponse), response);
		}

		// Token: 0x170003C4 RID: 964
		// (get) Token: 0x06000CA6 RID: 3238 RVA: 0x000129CA File Offset: 0x00010BCA
		public virtual bool HasStreamingProperty
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000CA7 RID: 3239 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		public virtual AmazonServiceException UnmarshallException(UnmarshallerContext input, Exception innerException, HttpStatusCode statusCode)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000CA8 RID: 3240 RVA: 0x0001E006 File Offset: 0x0001C206
		public AmazonWebServiceResponse UnmarshallResponse(UnmarshallerContext context)
		{
			AmazonWebServiceResponse amazonWebServiceResponse = this.Unmarshall(context);
			amazonWebServiceResponse.ContentLength = context.ResponseData.ContentLength;
			amazonWebServiceResponse.HttpStatusCode = context.ResponseData.StatusCode;
			return amazonWebServiceResponse;
		}

		// Token: 0x06000CA9 RID: 3241
		public abstract AmazonWebServiceResponse Unmarshall(UnmarshallerContext input);

		// Token: 0x06000CAA RID: 3242 RVA: 0x0001E031 File Offset: 0x0001C231
		public static string GetDefaultErrorMessage<T>() where T : Exception
		{
			return string.Format(CultureInfo.InvariantCulture, "An exception of type {0}, please check the error log for mode details.", new object[]
			{
				typeof(T).Name
			});
		}

		// Token: 0x06000CAB RID: 3243
		protected abstract UnmarshallerContext ConstructUnmarshallerContext(Stream responseStream, bool maintainResponseBody, IWebResponseData response);

		// Token: 0x06000CAC RID: 3244 RVA: 0x0001E05A File Offset: 0x0001C25A
		protected virtual bool ShouldReadEntireResponse(IWebResponseData response, bool readEntireResponse)
		{
			return readEntireResponse;
		}
	}
}
