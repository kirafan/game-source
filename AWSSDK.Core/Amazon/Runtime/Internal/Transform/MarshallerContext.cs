﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000143 RID: 323
	public abstract class MarshallerContext
	{
		// Token: 0x170003B5 RID: 949
		// (get) Token: 0x06000C7F RID: 3199 RVA: 0x0001D970 File Offset: 0x0001BB70
		// (set) Token: 0x06000C80 RID: 3200 RVA: 0x0001D978 File Offset: 0x0001BB78
		public IRequest Request { get; private set; }

		// Token: 0x06000C81 RID: 3201 RVA: 0x0001D981 File Offset: 0x0001BB81
		protected MarshallerContext(IRequest request)
		{
			this.Request = request;
		}
	}
}
