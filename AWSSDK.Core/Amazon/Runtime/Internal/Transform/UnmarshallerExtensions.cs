﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000160 RID: 352
	public static class UnmarshallerExtensions
	{
		// Token: 0x06000D11 RID: 3345 RVA: 0x0001E8BE File Offset: 0x0001CABE
		public static void Add<TKey, TValue>(this Dictionary<TKey, TValue> dict, KeyValuePair<TKey, TValue> item)
		{
			dict.Add(item.Key, item.Value);
		}
	}
}
