﻿using System;
using System.Globalization;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000150 RID: 336
	internal static class SimpleTypeUnmarshaller<T>
	{
		// Token: 0x06000CBE RID: 3262 RVA: 0x0001E2B6 File Offset: 0x0001C4B6
		public static T Unmarshall(XmlUnmarshallerContext context)
		{
			return (T)((object)Convert.ChangeType(context.ReadText(), typeof(T), CultureInfo.InvariantCulture));
		}

		// Token: 0x06000CBF RID: 3263 RVA: 0x0001E2D8 File Offset: 0x0001C4D8
		public static T Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			string text = context.ReadText();
			if (text == null)
			{
				return default(T);
			}
			return (T)((object)Convert.ChangeType(text, typeof(T), CultureInfo.InvariantCulture));
		}
	}
}
