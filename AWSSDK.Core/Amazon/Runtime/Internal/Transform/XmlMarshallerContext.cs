﻿using System;
using System.Xml;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000144 RID: 324
	public class XmlMarshallerContext : MarshallerContext
	{
		// Token: 0x170003B6 RID: 950
		// (get) Token: 0x06000C82 RID: 3202 RVA: 0x0001D990 File Offset: 0x0001BB90
		// (set) Token: 0x06000C83 RID: 3203 RVA: 0x0001D998 File Offset: 0x0001BB98
		public XmlWriter Writer { get; private set; }

		// Token: 0x06000C84 RID: 3204 RVA: 0x0001D9A1 File Offset: 0x0001BBA1
		public XmlMarshallerContext(IRequest request, XmlWriter writer) : base(request)
		{
			this.Writer = writer;
		}
	}
}
