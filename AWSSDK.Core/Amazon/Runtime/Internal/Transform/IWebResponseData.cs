﻿using System;
using System.Net;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000148 RID: 328
	public interface IWebResponseData
	{
		// Token: 0x170003B8 RID: 952
		// (get) Token: 0x06000C8A RID: 3210
		long ContentLength { get; }

		// Token: 0x170003B9 RID: 953
		// (get) Token: 0x06000C8B RID: 3211
		string ContentType { get; }

		// Token: 0x170003BA RID: 954
		// (get) Token: 0x06000C8C RID: 3212
		HttpStatusCode StatusCode { get; }

		// Token: 0x170003BB RID: 955
		// (get) Token: 0x06000C8D RID: 3213
		bool IsSuccessStatusCode { get; }

		// Token: 0x06000C8E RID: 3214
		string[] GetHeaderNames();

		// Token: 0x06000C8F RID: 3215
		bool IsHeaderPresent(string headerName);

		// Token: 0x06000C90 RID: 3216
		string GetHeaderValue(string headerName);

		// Token: 0x170003BC RID: 956
		// (get) Token: 0x06000C91 RID: 3217
		IHttpResponseBody ResponseBody { get; }
	}
}
