﻿using System;
using Amazon.Util;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200015A RID: 346
	public class DateTimeEpochLongMillisecondsUnmarshaller : IUnmarshaller<DateTime, XmlUnmarshallerContext>, IUnmarshaller<DateTime, JsonUnmarshallerContext>
	{
		// Token: 0x06000CF6 RID: 3318 RVA: 0x0000584A File Offset: 0x00003A4A
		private DateTimeEpochLongMillisecondsUnmarshaller()
		{
		}

		// Token: 0x170003CE RID: 974
		// (get) Token: 0x06000CF7 RID: 3319 RVA: 0x0001E502 File Offset: 0x0001C702
		public static DateTimeEpochLongMillisecondsUnmarshaller Instance
		{
			get
			{
				return DateTimeEpochLongMillisecondsUnmarshaller._instance;
			}
		}

		// Token: 0x06000CF8 RID: 3320 RVA: 0x0001E509 File Offset: 0x0001C709
		public static DateTimeEpochLongMillisecondsUnmarshaller GetInstance()
		{
			return DateTimeEpochLongMillisecondsUnmarshaller.Instance;
		}

		// Token: 0x06000CF9 RID: 3321 RVA: 0x0001E478 File Offset: 0x0001C678
		public DateTime Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<DateTime>.Unmarshall(context);
		}

		// Token: 0x06000CFA RID: 3322 RVA: 0x0001E510 File Offset: 0x0001C710
		public DateTime Unmarshall(JsonUnmarshallerContext context)
		{
			long num = LongUnmarshaller.Instance.Unmarshall(context);
			return AWSSDKUtils.EPOCH_START.AddMilliseconds((double)num);
		}

		// Token: 0x040004C7 RID: 1223
		private static DateTimeEpochLongMillisecondsUnmarshaller _instance = new DateTimeEpochLongMillisecondsUnmarshaller();
	}
}
