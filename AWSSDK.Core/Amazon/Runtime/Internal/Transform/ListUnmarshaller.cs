﻿using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal.Util;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200015E RID: 350
	public class ListUnmarshaller<I, IUnmarshaller> : IUnmarshaller<List<I>, XmlUnmarshallerContext>, IUnmarshaller<List<I>, JsonUnmarshallerContext> where IUnmarshaller : IUnmarshaller<I, XmlUnmarshallerContext>, IUnmarshaller<I, JsonUnmarshallerContext>
	{
		// Token: 0x06000D0B RID: 3339 RVA: 0x0001E78C File Offset: 0x0001C98C
		public ListUnmarshaller(IUnmarshaller iUnmarshaller)
		{
			this.iUnmarshaller = iUnmarshaller;
		}

		// Token: 0x06000D0C RID: 3340 RVA: 0x0001E79C File Offset: 0x0001C99C
		public List<I> Unmarshall(XmlUnmarshallerContext context)
		{
			int startingStackDepth = context.CurrentDepth + 1;
			List<I> list = new List<I>();
			while (context.Read())
			{
				if (context.IsStartElement && context.TestExpression("member", startingStackDepth))
				{
					I item = this.iUnmarshaller.Unmarshall(context);
					list.Add(item);
				}
			}
			return list;
		}

		// Token: 0x06000D0D RID: 3341 RVA: 0x0001E7F4 File Offset: 0x0001C9F4
		public List<I> Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return new List<I>();
			}
			List<I> list = new AlwaysSendList<I>();
			while (!context.Peek(JsonToken.ArrayEnd))
			{
				list.Add(this.iUnmarshaller.Unmarshall(context));
			}
			context.Read();
			return list;
		}

		// Token: 0x040004CC RID: 1228
		private IUnmarshaller iUnmarshaller;
	}
}
