﻿using System;
using System.IO;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000149 RID: 329
	public interface IHttpResponseBody : IDisposable
	{
		// Token: 0x06000C92 RID: 3218
		Stream OpenResponse();
	}
}
