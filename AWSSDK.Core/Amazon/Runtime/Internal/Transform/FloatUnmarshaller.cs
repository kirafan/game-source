﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000153 RID: 339
	public class FloatUnmarshaller : IUnmarshaller<float, XmlUnmarshallerContext>, IUnmarshaller<float, JsonUnmarshallerContext>
	{
		// Token: 0x06000CCC RID: 3276 RVA: 0x0000584A File Offset: 0x00003A4A
		private FloatUnmarshaller()
		{
		}

		// Token: 0x170003C7 RID: 967
		// (get) Token: 0x06000CCD RID: 3277 RVA: 0x0001E36E File Offset: 0x0001C56E
		public static FloatUnmarshaller Instance
		{
			get
			{
				return FloatUnmarshaller._instance;
			}
		}

		// Token: 0x06000CCE RID: 3278 RVA: 0x0001E375 File Offset: 0x0001C575
		public static FloatUnmarshaller GetInstance()
		{
			return FloatUnmarshaller.Instance;
		}

		// Token: 0x06000CCF RID: 3279 RVA: 0x0001E37C File Offset: 0x0001C57C
		public float Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<float>.Unmarshall(context);
		}

		// Token: 0x06000CD0 RID: 3280 RVA: 0x0001E384 File Offset: 0x0001C584
		public float Unmarshall(JsonUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<float>.Unmarshall(context);
		}

		// Token: 0x040004C0 RID: 1216
		private static FloatUnmarshaller _instance = new FloatUnmarshaller();
	}
}
