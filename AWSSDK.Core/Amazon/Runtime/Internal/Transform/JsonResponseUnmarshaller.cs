﻿using System;
using System.IO;
using System.Net;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x0200014F RID: 335
	public abstract class JsonResponseUnmarshaller : ResponseUnmarshaller
	{
		// Token: 0x06000CB7 RID: 3255 RVA: 0x0001E1E0 File Offset: 0x0001C3E0
		public override AmazonWebServiceResponse Unmarshall(UnmarshallerContext input)
		{
			JsonUnmarshallerContext jsonUnmarshallerContext = input as JsonUnmarshallerContext;
			if (jsonUnmarshallerContext == null)
			{
				throw new InvalidOperationException("Unsupported UnmarshallerContext");
			}
			string headerValue = jsonUnmarshallerContext.ResponseData.GetHeaderValue("x-amzn-RequestId");
			AmazonWebServiceResponse result;
			try
			{
				AmazonWebServiceResponse amazonWebServiceResponse = this.Unmarshall(jsonUnmarshallerContext);
				amazonWebServiceResponse.ResponseMetadata = new ResponseMetadata();
				amazonWebServiceResponse.ResponseMetadata.RequestId = headerValue;
				result = amazonWebServiceResponse;
			}
			catch (Exception innerException)
			{
				throw new AmazonUnmarshallingException(headerValue, jsonUnmarshallerContext.CurrentPath, innerException);
			}
			return result;
		}

		// Token: 0x06000CB8 RID: 3256 RVA: 0x0001E254 File Offset: 0x0001C454
		public override AmazonServiceException UnmarshallException(UnmarshallerContext input, Exception innerException, HttpStatusCode statusCode)
		{
			JsonUnmarshallerContext jsonUnmarshallerContext = input as JsonUnmarshallerContext;
			if (jsonUnmarshallerContext == null)
			{
				throw new InvalidOperationException("Unsupported UnmarshallerContext");
			}
			AmazonServiceException ex = this.UnmarshallException(jsonUnmarshallerContext, innerException, statusCode);
			ex.RequestId = jsonUnmarshallerContext.ResponseData.GetHeaderValue("x-amzn-RequestId");
			return ex;
		}

		// Token: 0x06000CB9 RID: 3257
		public abstract AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext input);

		// Token: 0x06000CBA RID: 3258
		public abstract AmazonServiceException UnmarshallException(JsonUnmarshallerContext input, Exception innerException, HttpStatusCode statusCode);

		// Token: 0x06000CBB RID: 3259 RVA: 0x0001E295 File Offset: 0x0001C495
		protected override UnmarshallerContext ConstructUnmarshallerContext(Stream responseStream, bool maintainResponseBody, IWebResponseData response)
		{
			return new JsonUnmarshallerContext(responseStream, maintainResponseBody, response);
		}

		// Token: 0x06000CBC RID: 3260 RVA: 0x0001E29F File Offset: 0x0001C49F
		protected override bool ShouldReadEntireResponse(IWebResponseData response, bool readEntireResponse)
		{
			return readEntireResponse && response.ContentType != "application/octet-stream";
		}
	}
}
