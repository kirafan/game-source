﻿using System;
using System.Net;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000146 RID: 326
	public interface IResponseUnmarshaller<T, R> : IUnmarshaller<T, R>
	{
		// Token: 0x06000C88 RID: 3208
		AmazonServiceException UnmarshallException(R input, Exception innerException, HttpStatusCode statusCode);
	}
}
