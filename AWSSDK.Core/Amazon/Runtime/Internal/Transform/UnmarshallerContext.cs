﻿using System;
using System.IO;
using System.Text;
using Amazon.Runtime.Internal.Util;
using ThirdParty.Ionic.Zlib;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000161 RID: 353
	public abstract class UnmarshallerContext : IDisposable
	{
		// Token: 0x170003D1 RID: 977
		// (get) Token: 0x06000D12 RID: 3346 RVA: 0x0001E8D4 File Offset: 0x0001CAD4
		// (set) Token: 0x06000D13 RID: 3347 RVA: 0x0001E8DC File Offset: 0x0001CADC
		protected bool MaintainResponseBody { get; set; }

		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x06000D14 RID: 3348 RVA: 0x0001E8E5 File Offset: 0x0001CAE5
		// (set) Token: 0x06000D15 RID: 3349 RVA: 0x0001E8ED File Offset: 0x0001CAED
		protected CrcCalculatorStream CrcStream { get; set; }

		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x06000D16 RID: 3350 RVA: 0x0001E8F6 File Offset: 0x0001CAF6
		// (set) Token: 0x06000D17 RID: 3351 RVA: 0x0001E8FE File Offset: 0x0001CAFE
		protected int Crc32Result { get; set; }

		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x06000D18 RID: 3352 RVA: 0x0001E907 File Offset: 0x0001CB07
		// (set) Token: 0x06000D19 RID: 3353 RVA: 0x0001E90F File Offset: 0x0001CB0F
		protected IWebResponseData WebResponseData { get; set; }

		// Token: 0x170003D5 RID: 981
		// (get) Token: 0x06000D1A RID: 3354 RVA: 0x0001E918 File Offset: 0x0001CB18
		// (set) Token: 0x06000D1B RID: 3355 RVA: 0x0001E920 File Offset: 0x0001CB20
		protected CachingWrapperStream WrappingStream { get; set; }

		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x06000D1C RID: 3356 RVA: 0x0001E92C File Offset: 0x0001CB2C
		public string ResponseBody
		{
			get
			{
				if (this.MaintainResponseBody)
				{
					byte[] array = this.WrappingStream.AllReadBytes.ToArray();
					return Encoding.UTF8.GetString(array, 0, array.Length);
				}
				return string.Empty;
			}
		}

		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x06000D1D RID: 3357 RVA: 0x0001E967 File Offset: 0x0001CB67
		public IWebResponseData ResponseData
		{
			get
			{
				return this.WebResponseData;
			}
		}

		// Token: 0x06000D1E RID: 3358 RVA: 0x0001E96F File Offset: 0x0001CB6F
		internal void ValidateCRC32IfAvailable()
		{
			if (this.CrcStream != null && this.CrcStream.Crc32 != this.Crc32Result)
			{
				throw new IOException("CRC value returned with response does not match the computed CRC value for the returned response body.");
			}
		}

		// Token: 0x06000D1F RID: 3359 RVA: 0x0001E998 File Offset: 0x0001CB98
		protected void SetupCRCStream(IWebResponseData responseData, Stream responseStream, long contentLength)
		{
			this.CrcStream = null;
			uint crc32Result;
			if (responseData != null && uint.TryParse(responseData.GetHeaderValue("x-amz-crc32"), out crc32Result))
			{
				this.Crc32Result = (int)crc32Result;
				this.CrcStream = new CrcCalculatorStream(responseStream, contentLength);
			}
		}

		// Token: 0x06000D20 RID: 3360 RVA: 0x0001E9D7 File Offset: 0x0001CBD7
		public bool TestExpression(string expression)
		{
			return UnmarshallerContext.TestExpression(expression, this.CurrentPath);
		}

		// Token: 0x06000D21 RID: 3361 RVA: 0x0001E9E5 File Offset: 0x0001CBE5
		public bool TestExpression(string expression, int startingStackDepth)
		{
			return UnmarshallerContext.TestExpression(expression, startingStackDepth, this.CurrentPath, this.CurrentDepth);
		}

		// Token: 0x06000D22 RID: 3362 RVA: 0x0001E9FA File Offset: 0x0001CBFA
		public bool ReadAtDepth(int targetDepth)
		{
			return this.Read() && this.CurrentDepth >= targetDepth;
		}

		// Token: 0x06000D23 RID: 3363 RVA: 0x0001EA12 File Offset: 0x0001CC12
		private static bool TestExpression(string expression, string currentPath)
		{
			return expression.Equals(".") || currentPath.EndsWith(expression, StringComparison.OrdinalIgnoreCase);
		}

		// Token: 0x06000D24 RID: 3364 RVA: 0x0001EA2C File Offset: 0x0001CC2C
		private static bool TestExpression(string expression, int startingStackDepth, string currentPath, int currentDepth)
		{
			if (expression.Equals("."))
			{
				return true;
			}
			int num = -1;
			while ((num = expression.IndexOf("/", num + 1, StringComparison.Ordinal)) > -1)
			{
				if (expression[0] != '@')
				{
					startingStackDepth++;
				}
			}
			return startingStackDepth == currentDepth && currentPath.EndsWith("/" + expression, StringComparison.OrdinalIgnoreCase);
		}

		// Token: 0x170003D8 RID: 984
		// (get) Token: 0x06000D25 RID: 3365
		public abstract string CurrentPath { get; }

		// Token: 0x170003D9 RID: 985
		// (get) Token: 0x06000D26 RID: 3366
		public abstract int CurrentDepth { get; }

		// Token: 0x06000D27 RID: 3367
		public abstract bool Read();

		// Token: 0x06000D28 RID: 3368
		public abstract string ReadText();

		// Token: 0x170003DA RID: 986
		// (get) Token: 0x06000D29 RID: 3369
		public abstract bool IsStartElement { get; }

		// Token: 0x170003DB RID: 987
		// (get) Token: 0x06000D2A RID: 3370
		public abstract bool IsEndElement { get; }

		// Token: 0x170003DC RID: 988
		// (get) Token: 0x06000D2B RID: 3371
		public abstract bool IsStartOfDocument { get; }

		// Token: 0x06000D2C RID: 3372 RVA: 0x0001EA88 File Offset: 0x0001CC88
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposed)
			{
				if (disposing)
				{
					if (this.CrcStream != null)
					{
						this.CrcStream.Dispose();
						this.CrcStream = null;
					}
					if (this.WrappingStream != null)
					{
						this.WrappingStream.Dispose();
						this.WrappingStream = null;
					}
				}
				this.disposed = true;
			}
		}

		// Token: 0x06000D2D RID: 3373 RVA: 0x0001EADB File Offset: 0x0001CCDB
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x040004CE RID: 1230
		private bool disposed;
	}
}
