﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000141 RID: 321
	public interface IMarshaller<T, R>
	{
		// Token: 0x06000C7D RID: 3197
		T Marshall(R input);
	}
}
