﻿using System;

namespace Amazon.Runtime.Internal.Transform
{
	// Token: 0x02000152 RID: 338
	public class LongUnmarshaller : IUnmarshaller<long, XmlUnmarshallerContext>, IUnmarshaller<long, JsonUnmarshallerContext>
	{
		// Token: 0x06000CC6 RID: 3270 RVA: 0x0000584A File Offset: 0x00003A4A
		private LongUnmarshaller()
		{
		}

		// Token: 0x170003C6 RID: 966
		// (get) Token: 0x06000CC7 RID: 3271 RVA: 0x0001E344 File Offset: 0x0001C544
		public static LongUnmarshaller Instance
		{
			get
			{
				return LongUnmarshaller._instance;
			}
		}

		// Token: 0x06000CC8 RID: 3272 RVA: 0x0001E34B File Offset: 0x0001C54B
		public static LongUnmarshaller GetInstance()
		{
			return LongUnmarshaller.Instance;
		}

		// Token: 0x06000CC9 RID: 3273 RVA: 0x0001E352 File Offset: 0x0001C552
		public long Unmarshall(XmlUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<long>.Unmarshall(context);
		}

		// Token: 0x06000CCA RID: 3274 RVA: 0x0001E35A File Offset: 0x0001C55A
		public long Unmarshall(JsonUnmarshallerContext context)
		{
			return SimpleTypeUnmarshaller<long>.Unmarshall(context);
		}

		// Token: 0x040004BF RID: 1215
		private static LongUnmarshaller _instance = new LongUnmarshaller();
	}
}
