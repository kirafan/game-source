﻿using System;
using Amazon.Runtime.Internal.Auth;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F1 RID: 241
	public class AsyncRequestContext : RequestContext, IAsyncRequestContext, IRequestContext
	{
		// Token: 0x0600098B RID: 2443 RVA: 0x00016021 File Offset: 0x00014221
		public AsyncRequestContext(bool enableMetrics, AbstractAWSSigner clientSigner) : base(enableMetrics, clientSigner)
		{
		}

		// Token: 0x17000302 RID: 770
		// (get) Token: 0x0600098C RID: 2444 RVA: 0x0001602B File Offset: 0x0001422B
		// (set) Token: 0x0600098D RID: 2445 RVA: 0x00016033 File Offset: 0x00014233
		public AsyncCallback Callback { get; set; }

		// Token: 0x17000303 RID: 771
		// (get) Token: 0x0600098E RID: 2446 RVA: 0x0001603C File Offset: 0x0001423C
		// (set) Token: 0x0600098F RID: 2447 RVA: 0x00016044 File Offset: 0x00014244
		public object State { get; set; }

		// Token: 0x17000304 RID: 772
		// (get) Token: 0x06000990 RID: 2448 RVA: 0x0001604D File Offset: 0x0001424D
		// (set) Token: 0x06000991 RID: 2449 RVA: 0x00016055 File Offset: 0x00014255
		public AsyncOptions AsyncOptions { get; set; }

		// Token: 0x17000305 RID: 773
		// (get) Token: 0x06000992 RID: 2450 RVA: 0x0001605E File Offset: 0x0001425E
		// (set) Token: 0x06000993 RID: 2451 RVA: 0x00016066 File Offset: 0x00014266
		public Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> Action { get; set; }
	}
}
