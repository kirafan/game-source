﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Reflection;
using UnityEngine;

namespace Amazon.Runtime.Internal
{
	// Token: 0x02000111 RID: 273
	public class UnityWebRequestWrapper : IDisposable
	{
		// Token: 0x06000ACD RID: 2765 RVA: 0x00019574 File Offset: 0x00017774
		static UnityWebRequestWrapper()
		{
			if (UnityWebRequestWrapper.unityWebRequestType == null)
			{
				UnityWebRequestWrapper.unityWebRequestType = Type.GetType("UnityEngine.Experimental.Networking.UnityWebRequest, UnityEngine");
			}
			UnityWebRequestWrapper.unityWebRequestMethods = UnityWebRequestWrapper.unityWebRequestType.GetMethods();
			UnityWebRequestWrapper.unityWebRequestProperties = UnityWebRequestWrapper.unityWebRequestType.GetProperties();
			PropertyInfo property = UnityWebRequestWrapper.unityWebRequestType.GetProperty("isDone");
			PropertyInfo property2 = UnityWebRequestWrapper.unityWebRequestType.GetProperty("downloadProgress");
			PropertyInfo property3 = UnityWebRequestWrapper.unityWebRequestType.GetProperty("uploadProgress");
			PropertyInfo property4 = UnityWebRequestWrapper.unityWebRequestType.GetProperty("isError");
			PropertyInfo property5 = UnityWebRequestWrapper.unityWebRequestType.GetProperty("downloadedBytes");
			PropertyInfo property6 = UnityWebRequestWrapper.unityWebRequestType.GetProperty("responseCode");
			PropertyInfo property7 = UnityWebRequestWrapper.unityWebRequestType.GetProperty("downloadHandler");
			PropertyInfo property8 = UnityWebRequestWrapper.unityWebRequestType.GetProperty("uploadHandler");
			PropertyInfo property9 = UnityWebRequestWrapper.unityWebRequestType.GetProperty("error");
			UnityWebRequestWrapper.setRequestHeaderMethod = UnityWebRequestWrapper.unityWebRequestType.GetMethod("SetRequestHeader");
			UnityWebRequestWrapper.sendMethod = UnityWebRequestWrapper.unityWebRequestType.GetMethod("Send");
			UnityWebRequestWrapper.getResponseHeadersMethod = UnityWebRequestWrapper.unityWebRequestType.GetMethod("GetResponseHeaders");
			UnityWebRequestWrapper.isDoneGetMethod = property.GetGetMethod();
			UnityWebRequestWrapper.isErrorGetMethod = property4.GetGetMethod();
			UnityWebRequestWrapper.uploadProgressGetMethod = property3.GetGetMethod();
			UnityWebRequestWrapper.downloadProgressGetMethod = property2.GetGetMethod();
			UnityWebRequestWrapper.downloadedBytesGetMethod = property5.GetGetMethod();
			UnityWebRequestWrapper.responseCodeGetMethod = property6.GetGetMethod();
			UnityWebRequestWrapper.downloadHandlerSetMethod = property7.GetSetMethod();
			UnityWebRequestWrapper.uploadHandlerSetMethod = property8.GetSetMethod();
			UnityWebRequestWrapper.errorGetMethod = property9.GetGetMethod();
		}

		// Token: 0x06000ACE RID: 2766 RVA: 0x000196F1 File Offset: 0x000178F1
		public UnityWebRequestWrapper()
		{
			if (!AWSConfigs.UnityWebRequestInitialized)
			{
				throw new InvalidOperationException("UnityWebRequest is not supported in the current version of unity");
			}
			this.unityWebRequestInstance = Activator.CreateInstance(UnityWebRequestWrapper.unityWebRequestType);
		}

		// Token: 0x06000ACF RID: 2767 RVA: 0x0001971B File Offset: 0x0001791B
		public UnityWebRequestWrapper(string url, string method)
		{
			this.unityWebRequestInstance = Activator.CreateInstance(UnityWebRequestWrapper.unityWebRequestType, new object[]
			{
				url,
				method
			});
		}

		// Token: 0x06000AD0 RID: 2768 RVA: 0x00019744 File Offset: 0x00017944
		public UnityWebRequestWrapper(string url, string method, DownloadHandlerBufferWrapper downloadHandler, UploadHandlerRawWrapper uploadHandler)
		{
			if (downloadHandler == null)
			{
				throw new ArgumentNullException("downloadHandler");
			}
			if (uploadHandler == null)
			{
				throw new ArgumentNullException("uploadHandler");
			}
			this.unityWebRequestInstance = Activator.CreateInstance(UnityWebRequestWrapper.unityWebRequestType, new object[]
			{
				url,
				method,
				downloadHandler.Instance,
				uploadHandler.Instance
			});
		}

		// Token: 0x17000348 RID: 840
		// (get) Token: 0x06000AD1 RID: 2769 RVA: 0x000197A5 File Offset: 0x000179A5
		internal static bool IsUnityWebRequestSupported
		{
			get
			{
				return UnityWebRequestWrapper.unityWebRequestType != null;
			}
		}

		// Token: 0x17000349 RID: 841
		// (get) Token: 0x06000AD3 RID: 2771 RVA: 0x000197E6 File Offset: 0x000179E6
		// (set) Token: 0x06000AD2 RID: 2770 RVA: 0x000197AF File Offset: 0x000179AF
		public DownloadHandlerBufferWrapper DownloadHandler
		{
			get
			{
				return this.downloadHandler;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				UnityWebRequestWrapper.downloadHandlerSetMethod.Invoke(this.unityWebRequestInstance, new object[]
				{
					value.Instance
				});
				this.downloadHandler = value;
			}
		}

		// Token: 0x1700034A RID: 842
		// (get) Token: 0x06000AD5 RID: 2773 RVA: 0x00019825 File Offset: 0x00017A25
		// (set) Token: 0x06000AD4 RID: 2772 RVA: 0x000197EE File Offset: 0x000179EE
		public UploadHandlerRawWrapper UploadHandler
		{
			get
			{
				return this.uploadHandler;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				UnityWebRequestWrapper.uploadHandlerSetMethod.Invoke(this.unityWebRequestInstance, new object[]
				{
					value.Instance
				});
				this.uploadHandler = value;
			}
		}

		// Token: 0x06000AD6 RID: 2774 RVA: 0x0001982D File Offset: 0x00017A2D
		public void SetRequestHeader(string key, string value)
		{
			UnityWebRequestWrapper.setRequestHeaderMethod.Invoke(this.unityWebRequestInstance, new object[]
			{
				key,
				value
			});
		}

		// Token: 0x06000AD7 RID: 2775 RVA: 0x0001984E File Offset: 0x00017A4E
		public AsyncOperation Send()
		{
			return (AsyncOperation)UnityWebRequestWrapper.sendMethod.Invoke(this.unityWebRequestInstance, null);
		}

		// Token: 0x1700034B RID: 843
		// (get) Token: 0x06000AD8 RID: 2776 RVA: 0x00019866 File Offset: 0x00017A66
		public bool IsDone
		{
			get
			{
				return (bool)UnityWebRequestWrapper.isDoneGetMethod.Invoke(this.unityWebRequestInstance, null);
			}
		}

		// Token: 0x1700034C RID: 844
		// (get) Token: 0x06000AD9 RID: 2777 RVA: 0x0001987E File Offset: 0x00017A7E
		public float DownloadProgress
		{
			get
			{
				return (float)UnityWebRequestWrapper.downloadProgressGetMethod.Invoke(this.unityWebRequestInstance, null);
			}
		}

		// Token: 0x1700034D RID: 845
		// (get) Token: 0x06000ADA RID: 2778 RVA: 0x00019896 File Offset: 0x00017A96
		public float UploadProgress
		{
			get
			{
				return (float)UnityWebRequestWrapper.uploadProgressGetMethod.Invoke(this.unityWebRequestInstance, null);
			}
		}

		// Token: 0x1700034E RID: 846
		// (get) Token: 0x06000ADB RID: 2779 RVA: 0x000198AE File Offset: 0x00017AAE
		public ulong DownloadedBytes
		{
			get
			{
				return (ulong)UnityWebRequestWrapper.downloadedBytesGetMethod.Invoke(this.unityWebRequestInstance, null);
			}
		}

		// Token: 0x1700034F RID: 847
		// (get) Token: 0x06000ADC RID: 2780 RVA: 0x000198C6 File Offset: 0x00017AC6
		public Dictionary<string, string> ResponseHeaders
		{
			get
			{
				return (Dictionary<string, string>)UnityWebRequestWrapper.getResponseHeadersMethod.Invoke(this.unityWebRequestInstance, null);
			}
		}

		// Token: 0x17000350 RID: 848
		// (get) Token: 0x06000ADD RID: 2781 RVA: 0x000198E0 File Offset: 0x00017AE0
		public HttpStatusCode? StatusCode
		{
			get
			{
				long num = (long)UnityWebRequestWrapper.responseCodeGetMethod.Invoke(this.unityWebRequestInstance, null);
				if (num == -1L)
				{
					return null;
				}
				return new HttpStatusCode?((HttpStatusCode)num);
			}
		}

		// Token: 0x17000351 RID: 849
		// (get) Token: 0x06000ADE RID: 2782 RVA: 0x0001991A File Offset: 0x00017B1A
		public bool IsError
		{
			get
			{
				return (bool)UnityWebRequestWrapper.isErrorGetMethod.Invoke(this.unityWebRequestInstance, null);
			}
		}

		// Token: 0x17000352 RID: 850
		// (get) Token: 0x06000ADF RID: 2783 RVA: 0x00019932 File Offset: 0x00017B32
		public string Error
		{
			get
			{
				return (string)UnityWebRequestWrapper.errorGetMethod.Invoke(this.unityWebRequestInstance, null);
			}
		}

		// Token: 0x06000AE0 RID: 2784 RVA: 0x0001994C File Offset: 0x00017B4C
		protected virtual void Dispose(bool disposing)
		{
			if (!this.disposedValue)
			{
				if (disposing)
				{
					IDisposable disposable = this.unityWebRequestInstance as IDisposable;
					if (disposable != null)
					{
						disposable.Dispose();
					}
				}
				this.unityWebRequestInstance = null;
				this.disposedValue = true;
			}
		}

		// Token: 0x06000AE1 RID: 2785 RVA: 0x00019987 File Offset: 0x00017B87
		public void Dispose()
		{
			this.Dispose(true);
		}

		// Token: 0x0400041D RID: 1053
		private static Type unityWebRequestType = Type.GetType("UnityEngine.Networking.UnityWebRequest, UnityEngine");

		// Token: 0x0400041E RID: 1054
		private static PropertyInfo[] unityWebRequestProperties;

		// Token: 0x0400041F RID: 1055
		private static MethodInfo[] unityWebRequestMethods;

		// Token: 0x04000420 RID: 1056
		private static MethodInfo setRequestHeaderMethod;

		// Token: 0x04000421 RID: 1057
		private static MethodInfo sendMethod;

		// Token: 0x04000422 RID: 1058
		private static MethodInfo getResponseHeadersMethod;

		// Token: 0x04000423 RID: 1059
		private static MethodInfo isDoneGetMethod;

		// Token: 0x04000424 RID: 1060
		private static MethodInfo downloadProgressGetMethod;

		// Token: 0x04000425 RID: 1061
		private static MethodInfo uploadProgressGetMethod;

		// Token: 0x04000426 RID: 1062
		private static MethodInfo isErrorGetMethod;

		// Token: 0x04000427 RID: 1063
		private static MethodInfo downloadedBytesGetMethod;

		// Token: 0x04000428 RID: 1064
		private static MethodInfo responseCodeGetMethod;

		// Token: 0x04000429 RID: 1065
		private static MethodInfo downloadHandlerSetMethod;

		// Token: 0x0400042A RID: 1066
		private static MethodInfo uploadHandlerSetMethod;

		// Token: 0x0400042B RID: 1067
		private static MethodInfo errorGetMethod;

		// Token: 0x0400042C RID: 1068
		private object unityWebRequestInstance;

		// Token: 0x0400042D RID: 1069
		private DownloadHandlerBufferWrapper downloadHandler;

		// Token: 0x0400042E RID: 1070
		private UploadHandlerRawWrapper uploadHandler;

		// Token: 0x0400042F RID: 1071
		private bool disposedValue;
	}
}
