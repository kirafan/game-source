﻿using System;
using Amazon.Runtime.Internal.Auth;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F0 RID: 240
	public class RequestContext : IRequestContext
	{
		// Token: 0x06000974 RID: 2420 RVA: 0x00015EFF File Offset: 0x000140FF
		public RequestContext(bool enableMetrics, AbstractAWSSigner clientSigner)
		{
			if (clientSigner == null)
			{
				throw new ArgumentNullException("clientSigner");
			}
			this.clientSigner = clientSigner;
			this.Metrics = new RequestMetrics();
			this.Metrics.IsEnabled = enableMetrics;
		}

		// Token: 0x170002F6 RID: 758
		// (get) Token: 0x06000975 RID: 2421 RVA: 0x00015F33 File Offset: 0x00014133
		// (set) Token: 0x06000976 RID: 2422 RVA: 0x00015F3B File Offset: 0x0001413B
		public IRequest Request { get; set; }

		// Token: 0x170002F7 RID: 759
		// (get) Token: 0x06000977 RID: 2423 RVA: 0x00015F44 File Offset: 0x00014144
		// (set) Token: 0x06000978 RID: 2424 RVA: 0x00015F4C File Offset: 0x0001414C
		public RequestMetrics Metrics { get; private set; }

		// Token: 0x170002F8 RID: 760
		// (get) Token: 0x06000979 RID: 2425 RVA: 0x00015F55 File Offset: 0x00014155
		// (set) Token: 0x0600097A RID: 2426 RVA: 0x00015F5D File Offset: 0x0001415D
		public IClientConfig ClientConfig { get; set; }

		// Token: 0x170002F9 RID: 761
		// (get) Token: 0x0600097B RID: 2427 RVA: 0x00015F66 File Offset: 0x00014166
		// (set) Token: 0x0600097C RID: 2428 RVA: 0x00015F6E File Offset: 0x0001416E
		public int Retries { get; set; }

		// Token: 0x170002FA RID: 762
		// (get) Token: 0x0600097D RID: 2429 RVA: 0x00015F77 File Offset: 0x00014177
		// (set) Token: 0x0600097E RID: 2430 RVA: 0x00015F7F File Offset: 0x0001417F
		public bool IsSigned { get; set; }

		// Token: 0x170002FB RID: 763
		// (get) Token: 0x0600097F RID: 2431 RVA: 0x00015F88 File Offset: 0x00014188
		// (set) Token: 0x06000980 RID: 2432 RVA: 0x00015F90 File Offset: 0x00014190
		public bool IsAsync { get; set; }

		// Token: 0x170002FC RID: 764
		// (get) Token: 0x06000981 RID: 2433 RVA: 0x00015F99 File Offset: 0x00014199
		// (set) Token: 0x06000982 RID: 2434 RVA: 0x00015FA1 File Offset: 0x000141A1
		public AmazonWebServiceRequest OriginalRequest { get; set; }

		// Token: 0x170002FD RID: 765
		// (get) Token: 0x06000983 RID: 2435 RVA: 0x00015FAA File Offset: 0x000141AA
		// (set) Token: 0x06000984 RID: 2436 RVA: 0x00015FB2 File Offset: 0x000141B2
		public IMarshaller<IRequest, AmazonWebServiceRequest> Marshaller { get; set; }

		// Token: 0x170002FE RID: 766
		// (get) Token: 0x06000985 RID: 2437 RVA: 0x00015FBB File Offset: 0x000141BB
		// (set) Token: 0x06000986 RID: 2438 RVA: 0x00015FC3 File Offset: 0x000141C3
		public ResponseUnmarshaller Unmarshaller { get; set; }

		// Token: 0x170002FF RID: 767
		// (get) Token: 0x06000987 RID: 2439 RVA: 0x00015FCC File Offset: 0x000141CC
		// (set) Token: 0x06000988 RID: 2440 RVA: 0x00015FD4 File Offset: 0x000141D4
		public ImmutableCredentials ImmutableCredentials { get; set; }

		// Token: 0x17000300 RID: 768
		// (get) Token: 0x06000989 RID: 2441 RVA: 0x00015FE0 File Offset: 0x000141E0
		public AbstractAWSSigner Signer
		{
			get
			{
				AbstractAWSSigner abstractAWSSigner = (this.OriginalRequest == null) ? null : this.OriginalRequest.GetSigner();
				if (abstractAWSSigner == null)
				{
					return this.clientSigner;
				}
				return abstractAWSSigner;
			}
		}

		// Token: 0x17000301 RID: 769
		// (get) Token: 0x0600098A RID: 2442 RVA: 0x0001600F File Offset: 0x0001420F
		public string RequestName
		{
			get
			{
				return this.OriginalRequest.GetType().Name;
			}
		}

		// Token: 0x040003B1 RID: 945
		private AbstractAWSSigner clientSigner;
	}
}
