﻿using System;
using Amazon.Runtime.Internal.Auth;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000EC RID: 236
	public interface IRequestData
	{
		// Token: 0x170002EC RID: 748
		// (get) Token: 0x0600094F RID: 2383
		ResponseUnmarshaller Unmarshaller { get; }

		// Token: 0x170002ED RID: 749
		// (get) Token: 0x06000950 RID: 2384
		RequestMetrics Metrics { get; }

		// Token: 0x170002EE RID: 750
		// (get) Token: 0x06000951 RID: 2385
		IRequest Request { get; }

		// Token: 0x170002EF RID: 751
		// (get) Token: 0x06000952 RID: 2386
		AbstractAWSSigner Signer { get; }

		// Token: 0x170002F0 RID: 752
		// (get) Token: 0x06000953 RID: 2387
		int RetriesAttempt { get; }
	}
}
