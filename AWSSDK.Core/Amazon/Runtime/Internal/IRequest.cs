﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.Runtime.Internal.Auth;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000EB RID: 235
	public interface IRequest
	{
		// Token: 0x170002D6 RID: 726
		// (get) Token: 0x06000924 RID: 2340
		string RequestName { get; }

		// Token: 0x170002D7 RID: 727
		// (get) Token: 0x06000925 RID: 2341
		IDictionary<string, string> Headers { get; }

		// Token: 0x170002D8 RID: 728
		// (get) Token: 0x06000926 RID: 2342
		// (set) Token: 0x06000927 RID: 2343
		bool UseQueryString { get; set; }

		// Token: 0x170002D9 RID: 729
		// (get) Token: 0x06000928 RID: 2344
		IDictionary<string, string> Parameters { get; }

		// Token: 0x170002DA RID: 730
		// (get) Token: 0x06000929 RID: 2345
		ParameterCollection ParameterCollection { get; }

		// Token: 0x170002DB RID: 731
		// (get) Token: 0x0600092A RID: 2346
		IDictionary<string, string> SubResources { get; }

		// Token: 0x0600092B RID: 2347
		void AddSubResource(string subResource);

		// Token: 0x0600092C RID: 2348
		void AddSubResource(string subResource, string value);

		// Token: 0x170002DC RID: 732
		// (get) Token: 0x0600092D RID: 2349
		// (set) Token: 0x0600092E RID: 2350
		string HttpMethod { get; set; }

		// Token: 0x170002DD RID: 733
		// (get) Token: 0x0600092F RID: 2351
		// (set) Token: 0x06000930 RID: 2352
		Uri Endpoint { get; set; }

		// Token: 0x170002DE RID: 734
		// (get) Token: 0x06000931 RID: 2353
		// (set) Token: 0x06000932 RID: 2354
		string ResourcePath { get; set; }

		// Token: 0x170002DF RID: 735
		// (get) Token: 0x06000933 RID: 2355
		// (set) Token: 0x06000934 RID: 2356
		byte[] Content { get; set; }

		// Token: 0x170002E0 RID: 736
		// (get) Token: 0x06000935 RID: 2357
		// (set) Token: 0x06000936 RID: 2358
		bool SetContentFromParameters { get; set; }

		// Token: 0x170002E1 RID: 737
		// (get) Token: 0x06000937 RID: 2359
		// (set) Token: 0x06000938 RID: 2360
		Stream ContentStream { get; set; }

		// Token: 0x170002E2 RID: 738
		// (get) Token: 0x06000939 RID: 2361
		// (set) Token: 0x0600093A RID: 2362
		long OriginalStreamPosition { get; set; }

		// Token: 0x0600093B RID: 2363
		string ComputeContentStreamHash();

		// Token: 0x170002E3 RID: 739
		// (get) Token: 0x0600093C RID: 2364
		string ServiceName { get; }

		// Token: 0x170002E4 RID: 740
		// (get) Token: 0x0600093D RID: 2365
		AmazonWebServiceRequest OriginalRequest { get; }

		// Token: 0x170002E5 RID: 741
		// (get) Token: 0x0600093E RID: 2366
		// (set) Token: 0x0600093F RID: 2367
		RegionEndpoint AlternateEndpoint { get; set; }

		// Token: 0x170002E6 RID: 742
		// (get) Token: 0x06000940 RID: 2368
		// (set) Token: 0x06000941 RID: 2369
		bool Suppress404Exceptions { get; set; }

		// Token: 0x170002E7 RID: 743
		// (get) Token: 0x06000942 RID: 2370
		// (set) Token: 0x06000943 RID: 2371
		AWS4SigningResult AWS4SignerResult { get; set; }

		// Token: 0x170002E8 RID: 744
		// (get) Token: 0x06000944 RID: 2372
		// (set) Token: 0x06000945 RID: 2373
		bool UseChunkEncoding { get; set; }

		// Token: 0x170002E9 RID: 745
		// (get) Token: 0x06000946 RID: 2374
		// (set) Token: 0x06000947 RID: 2375
		string CanonicalResourcePrefix { get; set; }

		// Token: 0x170002EA RID: 746
		// (get) Token: 0x06000948 RID: 2376
		// (set) Token: 0x06000949 RID: 2377
		bool UseSigV4 { get; set; }

		// Token: 0x170002EB RID: 747
		// (get) Token: 0x0600094A RID: 2378
		// (set) Token: 0x0600094B RID: 2379
		string AuthenticationRegion { get; set; }

		// Token: 0x0600094C RID: 2380
		bool IsRequestStreamRewindable();

		// Token: 0x0600094D RID: 2381
		bool MayContainRequestBody();

		// Token: 0x0600094E RID: 2382
		bool HasRequestBody();
	}
}
