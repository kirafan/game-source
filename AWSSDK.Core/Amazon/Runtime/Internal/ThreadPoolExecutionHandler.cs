﻿using System;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x0200010E RID: 270
	public class ThreadPoolExecutionHandler : PipelineHandler
	{
		// Token: 0x06000ABA RID: 2746 RVA: 0x0001903C File Offset: 0x0001723C
		public ThreadPoolExecutionHandler(int concurrentRequests)
		{
			object @lock = ThreadPoolExecutionHandler._lock;
			lock (@lock)
			{
				if (ThreadPoolExecutionHandler._throttler == null)
				{
					ThreadPoolExecutionHandler._throttler = new ThreadPoolThrottler<IAsyncExecutionContext>(concurrentRequests);
				}
			}
		}

		// Token: 0x06000ABB RID: 2747 RVA: 0x00019088 File Offset: 0x00017288
		public override IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			if (UnityInitializer.IsMainThread())
			{
				ThreadPoolExecutionHandler._throttler.Enqueue(executionContext, new Action<IAsyncExecutionContext>(this.InvokeAsyncHelper), new Action<Exception, IAsyncExecutionContext>(this.ErrorCallback));
				return null;
			}
			return base.InvokeAsync(executionContext);
		}

		// Token: 0x06000ABC RID: 2748 RVA: 0x000190BD File Offset: 0x000172BD
		private void InvokeAsyncHelper(IAsyncExecutionContext executionContext)
		{
			base.InvokeAsync(executionContext);
		}

		// Token: 0x06000ABD RID: 2749 RVA: 0x000190C8 File Offset: 0x000172C8
		private void ErrorCallback(Exception exception, IAsyncExecutionContext executionContext)
		{
			this.Logger.Error(exception, "An exception of type {0} was thrown from InvokeAsyncCallback().", new object[]
			{
				exception.GetType().Name
			});
			executionContext.RequestContext.Metrics.AddProperty(Metric.Exception, exception);
			executionContext.RequestContext.Metrics.StopEvent(Metric.ClientExecuteTime);
			base.LogMetrics(ExecutionContext.CreateFromAsyncContext(executionContext));
			executionContext.ResponseContext.AsyncResult = new RuntimeAsyncResult(executionContext.RequestContext.Callback, executionContext.RequestContext.State);
			executionContext.ResponseContext.AsyncResult.Exception = exception;
			executionContext.ResponseContext.AsyncResult.AsyncOptions = executionContext.RequestContext.AsyncOptions;
			executionContext.ResponseContext.AsyncResult.Action = executionContext.RequestContext.Action;
			executionContext.ResponseContext.AsyncResult.Request = executionContext.RequestContext.OriginalRequest;
			executionContext.ResponseContext.AsyncResult.InvokeCallback();
		}

		// Token: 0x04000410 RID: 1040
		private static ThreadPoolThrottler<IAsyncExecutionContext> _throttler;

		// Token: 0x04000411 RID: 1041
		private static object _lock = new object();
	}
}
