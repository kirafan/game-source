﻿using System;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000E9 RID: 233
	public class ErrorResponse
	{
		// Token: 0x170002CF RID: 719
		// (get) Token: 0x06000914 RID: 2324 RVA: 0x00015821 File Offset: 0x00013A21
		// (set) Token: 0x06000915 RID: 2325 RVA: 0x00015829 File Offset: 0x00013A29
		public ErrorType Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.type = value;
			}
		}

		// Token: 0x170002D0 RID: 720
		// (get) Token: 0x06000916 RID: 2326 RVA: 0x00015832 File Offset: 0x00013A32
		// (set) Token: 0x06000917 RID: 2327 RVA: 0x0001583A File Offset: 0x00013A3A
		public string Code
		{
			get
			{
				return this.code;
			}
			set
			{
				this.code = value;
			}
		}

		// Token: 0x170002D1 RID: 721
		// (get) Token: 0x06000918 RID: 2328 RVA: 0x00015843 File Offset: 0x00013A43
		// (set) Token: 0x06000919 RID: 2329 RVA: 0x0001584B File Offset: 0x00013A4B
		public string Message
		{
			get
			{
				return this.message;
			}
			set
			{
				this.message = value;
			}
		}

		// Token: 0x170002D2 RID: 722
		// (get) Token: 0x0600091A RID: 2330 RVA: 0x00015854 File Offset: 0x00013A54
		// (set) Token: 0x0600091B RID: 2331 RVA: 0x0001585C File Offset: 0x00013A5C
		public string RequestId
		{
			get
			{
				return this.requestId;
			}
			set
			{
				this.requestId = value;
			}
		}

		// Token: 0x040003A1 RID: 929
		private ErrorType type;

		// Token: 0x040003A2 RID: 930
		private string code;

		// Token: 0x040003A3 RID: 931
		private string message;

		// Token: 0x040003A4 RID: 932
		private string requestId;
	}
}
