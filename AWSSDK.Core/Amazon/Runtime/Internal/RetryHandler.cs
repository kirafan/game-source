﻿using System;
using System.IO;
using System.Net;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x0200010C RID: 268
	public class RetryHandler : PipelineHandler
	{
		// Token: 0x1700033B RID: 827
		// (get) Token: 0x06000A97 RID: 2711 RVA: 0x00018A46 File Offset: 0x00016C46
		// (set) Token: 0x06000A98 RID: 2712 RVA: 0x00018A4E File Offset: 0x00016C4E
		public override ILogger Logger
		{
			get
			{
				return this._logger;
			}
			set
			{
				this._logger = value;
				this.RetryPolicy.Logger = value;
			}
		}

		// Token: 0x1700033C RID: 828
		// (get) Token: 0x06000A99 RID: 2713 RVA: 0x00018A63 File Offset: 0x00016C63
		// (set) Token: 0x06000A9A RID: 2714 RVA: 0x00018A6B File Offset: 0x00016C6B
		public RetryPolicy RetryPolicy { get; private set; }

		// Token: 0x06000A9B RID: 2715 RVA: 0x00018A74 File Offset: 0x00016C74
		public RetryHandler(RetryPolicy retryPolicy)
		{
			this.RetryPolicy = retryPolicy;
		}

		// Token: 0x06000A9C RID: 2716 RVA: 0x00018A84 File Offset: 0x00016C84
		public override void InvokeSync(IExecutionContext executionContext)
		{
			IRequestContext requestContext = executionContext.RequestContext;
			IResponseContext responseContext = executionContext.ResponseContext;
			bool flag = false;
			do
			{
				try
				{
					base.InvokeSync(executionContext);
					this.RetryPolicy.NotifySuccess(executionContext);
					break;
				}
				catch (Exception exception)
				{
					flag = this.RetryPolicy.Retry(executionContext, exception);
					if (!flag)
					{
						this.LogForError(requestContext, exception);
						throw;
					}
					IRequestContext requestContext2 = requestContext;
					int retries = requestContext2.Retries;
					requestContext2.Retries = retries + 1;
					requestContext.Metrics.SetCounter(Metric.AttemptCount, (long)requestContext.Retries);
					this.LogForRetry(requestContext, exception);
				}
				RetryHandler.PrepareForRetry(requestContext);
				using (requestContext.Metrics.StartEvent(Metric.RetryPauseTime))
				{
					this.RetryPolicy.WaitBeforeRetry(executionContext);
				}
			}
			while (flag);
		}

		// Token: 0x06000A9D RID: 2717 RVA: 0x00018B54 File Offset: 0x00016D54
		protected override void InvokeAsyncCallback(IAsyncExecutionContext executionContext)
		{
			IAsyncRequestContext requestContext = executionContext.RequestContext;
			IAsyncResponseContext responseContext = executionContext.ResponseContext;
			Exception exception = responseContext.AsyncResult.Exception;
			IExecutionContext executionContext2 = ExecutionContext.CreateFromAsyncContext(executionContext);
			if (exception != null)
			{
				if (this.RetryPolicy.Retry(executionContext2, exception))
				{
					IAsyncRequestContext asyncRequestContext = requestContext;
					int retries = asyncRequestContext.Retries;
					asyncRequestContext.Retries = retries + 1;
					requestContext.Metrics.SetCounter(Metric.AttemptCount, (long)requestContext.Retries);
					this.LogForRetry(requestContext, exception);
					RetryHandler.PrepareForRetry(requestContext);
					responseContext.AsyncResult.Exception = null;
					using (requestContext.Metrics.StartEvent(Metric.RetryPauseTime))
					{
						this.RetryPolicy.WaitBeforeRetry(executionContext2);
					}
					this.InvokeAsync(executionContext);
					return;
				}
				this.LogForError(requestContext, exception);
			}
			else
			{
				this.RetryPolicy.NotifySuccess(executionContext2);
			}
			base.InvokeAsyncCallback(executionContext);
		}

		// Token: 0x06000A9E RID: 2718 RVA: 0x00018C38 File Offset: 0x00016E38
		internal static void PrepareForRetry(IRequestContext requestContext)
		{
			if (requestContext.Request.ContentStream != null && requestContext.Request.OriginalStreamPosition >= 0L)
			{
				Stream stream = requestContext.Request.ContentStream;
				HashStream hashStream = stream as HashStream;
				if (hashStream != null)
				{
					hashStream.Reset();
					stream = hashStream.GetSeekableBaseStream();
				}
				stream.Position = requestContext.Request.OriginalStreamPosition;
			}
		}

		// Token: 0x06000A9F RID: 2719 RVA: 0x00018C98 File Offset: 0x00016E98
		private void LogForRetry(IRequestContext requestContext, Exception exception)
		{
			WebException ex = exception as WebException;
			if (ex != null)
			{
				this.Logger.InfoFormat("WebException ({1}) making request {2} to {3}. Attempting retry {4} of {5}.", new object[]
				{
					ex.Status,
					requestContext.RequestName,
					requestContext.Request.Endpoint.ToString(),
					requestContext.Retries,
					this.RetryPolicy.MaxRetries
				});
				return;
			}
			this.Logger.InfoFormat("{0} making request {1} to {2}. Attempting retry {3} of {4}.", new object[]
			{
				exception.GetType().Name,
				requestContext.RequestName,
				requestContext.Request.Endpoint.ToString(),
				requestContext.Retries,
				this.RetryPolicy.MaxRetries
			});
		}

		// Token: 0x06000AA0 RID: 2720 RVA: 0x00018D74 File Offset: 0x00016F74
		private void LogForError(IRequestContext requestContext, Exception exception)
		{
			this.Logger.Error(exception, "{0} making request {1} to {2}. Attempt {3}.", new object[]
			{
				exception.GetType().Name,
				requestContext.RequestName,
				requestContext.Request.Endpoint.ToString(),
				requestContext.Retries + 1
			});
		}

		// Token: 0x04000400 RID: 1024
		private ILogger _logger;
	}
}
