﻿using System;
using System.Collections;
using System.Collections.Generic;
using Amazon.Util.Internal.PlatformServices;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000E4 RID: 228
	public class ClientContext
	{
		// Token: 0x170002B7 RID: 695
		// (get) Token: 0x060008D9 RID: 2265 RVA: 0x00015113 File Offset: 0x00013313
		// (set) Token: 0x060008DA RID: 2266 RVA: 0x0001511B File Offset: 0x0001331B
		public string AppID { get; set; }

		// Token: 0x060008DB RID: 2267 RVA: 0x00015124 File Offset: 0x00013324
		public void AddCustomAttributes(string key, string value)
		{
			object @lock = ClientContext._lock;
			lock (@lock)
			{
				if (this._custom == null)
				{
					this._custom = new Dictionary<string, string>();
				}
				this._custom.Add(key, value);
			}
		}

		// Token: 0x060008DC RID: 2268 RVA: 0x00015178 File Offset: 0x00013378
		public string ToJsonString()
		{
			object @lock = ClientContext._lock;
			string result;
			lock (@lock)
			{
				this._client = new Dictionary<string, string>();
				this._env = new Dictionary<string, string>();
				this._services = new Dictionary<string, IDictionary>();
				this._client.Add("client_id", ClientContext._clientID);
				this._client.Add("app_title", ClientContext._appInfo.AppTitle);
				this._client.Add("app_version_name", ClientContext._appInfo.AppVersionName);
				this._client.Add("app_version_code", ClientContext._appInfo.AppVersionCode);
				this._client.Add("app_package_name", ClientContext._appInfo.PackageName);
				this._env.Add("platform", ClientContext._envInfo.Platform);
				this._env.Add("platform_version", ClientContext._envInfo.PlatformVersion);
				this._env.Add("locale", ClientContext._envInfo.Locale);
				this._env.Add("make", ClientContext._envInfo.Make);
				this._env.Add("model", ClientContext._envInfo.Model);
				if (!string.IsNullOrEmpty(this.AppID))
				{
					IDictionary dictionary = new Dictionary<string, string>();
					dictionary.Add("app_id", this.AppID);
					this._services.Add("mobile_analytics", dictionary);
				}
				this._clientContext = new Dictionary<string, IDictionary>();
				this._clientContext.Add("client", this._client);
				this._clientContext.Add("env", this._env);
				this._clientContext.Add("custom", this._custom);
				this._clientContext.Add("services", this._services);
				result = JsonMapper.ToJson(this._clientContext);
			}
			return result;
		}

		// Token: 0x060008DD RID: 2269 RVA: 0x0001537C File Offset: 0x0001357C
		public ClientContext(string appId)
		{
			this.AppID = appId;
			this._custom = new Dictionary<string, string>();
			if (string.IsNullOrEmpty(ClientContext._clientID))
			{
				ClientContext._clientID = ClientContext._appSetting.GetValue("mobile_analytics_client_id", ApplicationSettingsMode.Local);
				if (string.IsNullOrEmpty(ClientContext._clientID))
				{
					ClientContext._clientID = Guid.NewGuid().ToString();
					ClientContext._appSetting.SetValue("mobile_analytics_client_id", ClientContext._clientID, ApplicationSettingsMode.Local);
				}
			}
		}

		// Token: 0x0400036B RID: 875
		private const string CLIENT_KEY = "client";

		// Token: 0x0400036C RID: 876
		private const string CLIENT_ID_KEY = "client_id";

		// Token: 0x0400036D RID: 877
		private const string CLIENT_APP_TITLE_KEY = "app_title";

		// Token: 0x0400036E RID: 878
		private const string CLIENT_APP_VERSION_NAME_KEY = "app_version_name";

		// Token: 0x0400036F RID: 879
		private const string CLIENT_APP_VERSION_CODE_KEY = "app_version_code";

		// Token: 0x04000370 RID: 880
		private const string CLIENT_APP_PACKAGE_NAME_KEY = "app_package_name";

		// Token: 0x04000371 RID: 881
		private const string CUSTOM_KEY = "custom";

		// Token: 0x04000372 RID: 882
		private const string ENV_KEY = "env";

		// Token: 0x04000373 RID: 883
		private const string ENV_PLATFORM_KEY = "platform";

		// Token: 0x04000374 RID: 884
		private const string ENV_MODEL_KEY = "model";

		// Token: 0x04000375 RID: 885
		private const string ENV_MAKE_KEY = "make";

		// Token: 0x04000376 RID: 886
		private const string ENV_PLATFORM_VERSION_KEY = "platform_version";

		// Token: 0x04000377 RID: 887
		private const string ENV_LOCALE_KEY = "locale";

		// Token: 0x04000378 RID: 888
		private const string SERVICES_KEY = "services";

		// Token: 0x04000379 RID: 889
		private const string SERVICE_MOBILE_ANALYTICS_KEY = "mobile_analytics";

		// Token: 0x0400037A RID: 890
		private const string SERVICE_MOBILE_ANALYTICS_APP_ID_KEY = "app_id";

		// Token: 0x0400037B RID: 891
		private IDictionary<string, string> _client;

		// Token: 0x0400037C RID: 892
		private IDictionary<string, string> _custom;

		// Token: 0x0400037D RID: 893
		private IDictionary<string, string> _env;

		// Token: 0x0400037E RID: 894
		private IDictionary<string, IDictionary> _services;

		// Token: 0x0400037F RID: 895
		private IDictionary _clientContext;

		// Token: 0x04000380 RID: 896
		private static object _lock = new object();

		// Token: 0x04000381 RID: 897
		private static string _clientID = null;

		// Token: 0x04000382 RID: 898
		private const string APP_ID_KEY = "APP_ID_KEY";

		// Token: 0x04000383 RID: 899
		private const string CLIENT_ID_CACHE_FILENAME = "client-ID-cache";

		// Token: 0x04000385 RID: 901
		private const string APP_CLIENT_ID_KEY = "mobile_analytics_client_id";

		// Token: 0x04000386 RID: 902
		private static IApplicationSettings _appSetting = ServiceFactory.Instance.GetService<IApplicationSettings>();

		// Token: 0x04000387 RID: 903
		private static IApplicationInfo _appInfo = ServiceFactory.Instance.GetService<IApplicationInfo>();

		// Token: 0x04000388 RID: 904
		private static IEnvironmentInfo _envInfo = ServiceFactory.Instance.GetService<IEnvironmentInfo>();
	}
}
