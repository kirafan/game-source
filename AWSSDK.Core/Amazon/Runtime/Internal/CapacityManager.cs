﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000E2 RID: 226
	public class CapacityManager : IDisposable
	{
		// Token: 0x060008CB RID: 2251 RVA: 0x00014E9D File Offset: 0x0001309D
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060008CC RID: 2252 RVA: 0x00014EAC File Offset: 0x000130AC
		protected virtual void Dispose(bool disposing)
		{
			if (this._disposed)
			{
				return;
			}
			if (disposing)
			{
				this._rwlock.Dispose();
				this._disposed = true;
			}
		}

		// Token: 0x060008CD RID: 2253 RVA: 0x00014ECC File Offset: 0x000130CC
		public CapacityManager(int throttleRetryCount, int throttleRetryCost, int throttleCost)
		{
			this.THROTTLE_RETRY_REQUEST_COST = throttleRetryCost;
			this.THROTTLED_RETRIES = throttleRetryCount;
			this.THROTTLE_REQUEST_COST = throttleCost;
		}

		// Token: 0x060008CE RID: 2254 RVA: 0x00014EF4 File Offset: 0x000130F4
		public bool TryAcquireCapacity(RetryCapacity retryCapacity)
		{
			if (this.THROTTLE_RETRY_REQUEST_COST < 0)
			{
				return false;
			}
			bool result;
			lock (retryCapacity)
			{
				if (retryCapacity.AvailableCapacity - this.THROTTLE_RETRY_REQUEST_COST >= 0)
				{
					retryCapacity.AvailableCapacity -= this.THROTTLE_RETRY_REQUEST_COST;
					result = true;
				}
				else
				{
					result = false;
				}
			}
			return result;
		}

		// Token: 0x060008CF RID: 2255 RVA: 0x00014F58 File Offset: 0x00013158
		public void TryReleaseCapacity(bool isRetryRequest, RetryCapacity retryCapacity)
		{
			if (isRetryRequest)
			{
				CapacityManager.ReleaseCapacity(this.THROTTLE_RETRY_REQUEST_COST, retryCapacity);
				return;
			}
			CapacityManager.ReleaseCapacity(this.THROTTLE_REQUEST_COST, retryCapacity);
		}

		// Token: 0x060008D0 RID: 2256 RVA: 0x00014F78 File Offset: 0x00013178
		public RetryCapacity GetRetryCapacity(string serviceURL)
		{
			RetryCapacity result;
			if (!this.TryGetRetryCapacity(serviceURL, out result))
			{
				result = this.AddNewRetryCapacity(serviceURL);
			}
			return result;
		}

		// Token: 0x060008D1 RID: 2257 RVA: 0x00014F9C File Offset: 0x0001319C
		private bool TryGetRetryCapacity(string key, out RetryCapacity value)
		{
			this._rwlock.EnterReadLock();
			bool result;
			try
			{
				if (CapacityManager._serviceUrlToCapacityMap.TryGetValue(key, out value))
				{
					result = true;
				}
				else
				{
					result = false;
				}
			}
			finally
			{
				this._rwlock.ExitReadLock();
			}
			return result;
		}

		// Token: 0x060008D2 RID: 2258 RVA: 0x00014FE8 File Offset: 0x000131E8
		private RetryCapacity AddNewRetryCapacity(string serviceURL)
		{
			this._rwlock.EnterUpgradeableReadLock();
			RetryCapacity result;
			try
			{
				RetryCapacity retryCapacity;
				if (!CapacityManager._serviceUrlToCapacityMap.TryGetValue(serviceURL, out retryCapacity))
				{
					this._rwlock.EnterWriteLock();
					try
					{
						retryCapacity = new RetryCapacity(this.THROTTLE_RETRY_REQUEST_COST * this.THROTTLED_RETRIES);
						CapacityManager._serviceUrlToCapacityMap.Add(serviceURL, retryCapacity);
						return retryCapacity;
					}
					finally
					{
						this._rwlock.ExitWriteLock();
					}
				}
				result = retryCapacity;
			}
			finally
			{
				this._rwlock.ExitUpgradeableReadLock();
			}
			return result;
		}

		// Token: 0x060008D3 RID: 2259 RVA: 0x00015078 File Offset: 0x00013278
		private static void ReleaseCapacity(int capacity, RetryCapacity retryCapacity)
		{
			if (retryCapacity.AvailableCapacity >= 0 && retryCapacity.AvailableCapacity < retryCapacity.MaxCapacity)
			{
				lock (retryCapacity)
				{
					retryCapacity.AvailableCapacity = Math.Min(retryCapacity.AvailableCapacity + capacity, retryCapacity.MaxCapacity);
				}
			}
		}

		// Token: 0x04000363 RID: 867
		private bool _disposed;

		// Token: 0x04000364 RID: 868
		private static Dictionary<string, RetryCapacity> _serviceUrlToCapacityMap = new Dictionary<string, RetryCapacity>();

		// Token: 0x04000365 RID: 869
		private ReaderWriterLockSlim _rwlock = new ReaderWriterLockSlim();

		// Token: 0x04000366 RID: 870
		private readonly int THROTTLE_RETRY_REQUEST_COST;

		// Token: 0x04000367 RID: 871
		private readonly int THROTTLED_RETRIES;

		// Token: 0x04000368 RID: 872
		private readonly int THROTTLE_REQUEST_COST;
	}
}
