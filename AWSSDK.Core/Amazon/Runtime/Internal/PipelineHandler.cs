﻿using System;
using System.Diagnostics;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime.Internal
{
	// Token: 0x020000F7 RID: 247
	public abstract class PipelineHandler : IPipelineHandler
	{
		// Token: 0x1700030F RID: 783
		// (get) Token: 0x060009B2 RID: 2482 RVA: 0x0001620A File Offset: 0x0001440A
		// (set) Token: 0x060009B3 RID: 2483 RVA: 0x00016212 File Offset: 0x00014412
		public virtual ILogger Logger { get; set; }

		// Token: 0x17000310 RID: 784
		// (get) Token: 0x060009B4 RID: 2484 RVA: 0x0001621B File Offset: 0x0001441B
		// (set) Token: 0x060009B5 RID: 2485 RVA: 0x00016223 File Offset: 0x00014423
		public IPipelineHandler InnerHandler { get; set; }

		// Token: 0x17000311 RID: 785
		// (get) Token: 0x060009B6 RID: 2486 RVA: 0x0001622C File Offset: 0x0001442C
		// (set) Token: 0x060009B7 RID: 2487 RVA: 0x00016234 File Offset: 0x00014434
		public IPipelineHandler OuterHandler { get; set; }

		// Token: 0x060009B8 RID: 2488 RVA: 0x0001623D File Offset: 0x0001443D
		public virtual void InvokeSync(IExecutionContext executionContext)
		{
			if (this.InnerHandler != null)
			{
				this.InnerHandler.InvokeSync(executionContext);
				return;
			}
			throw new InvalidOperationException("Cannot invoke InnerHandler. InnerHandler is not set.");
		}

		// Token: 0x060009B9 RID: 2489 RVA: 0x0001625E File Offset: 0x0001445E
		[DebuggerHidden]
		public virtual IAsyncResult InvokeAsync(IAsyncExecutionContext executionContext)
		{
			if (this.InnerHandler != null)
			{
				return this.InnerHandler.InvokeAsync(executionContext);
			}
			throw new InvalidOperationException("Cannot invoke InnerHandler. InnerHandler is not set.");
		}

		// Token: 0x060009BA RID: 2490 RVA: 0x00016280 File Offset: 0x00014480
		[DebuggerHidden]
		public void AsyncCallback(IAsyncExecutionContext executionContext)
		{
			try
			{
				this.InvokeAsyncCallback(executionContext);
			}
			catch (Exception ex)
			{
				this.Logger.Error(ex, "An exception of type {0} was thrown from InvokeAsyncCallback().", new object[]
				{
					ex.GetType().Name
				});
				executionContext.RequestContext.Metrics.AddProperty(Metric.Exception, ex);
				executionContext.RequestContext.Metrics.StopEvent(Metric.ClientExecuteTime);
				this.LogMetrics(ExecutionContext.CreateFromAsyncContext(executionContext));
				executionContext.ResponseContext.AsyncResult.HandleException(ex);
			}
		}

		// Token: 0x060009BB RID: 2491 RVA: 0x00016310 File Offset: 0x00014510
		[DebuggerHidden]
		protected virtual void InvokeAsyncCallback(IAsyncExecutionContext executionContext)
		{
			if (this.OuterHandler != null)
			{
				this.OuterHandler.AsyncCallback(executionContext);
				return;
			}
			executionContext.ResponseContext.AsyncResult.Response = executionContext.ResponseContext.Response;
			executionContext.ResponseContext.AsyncResult.InvokeCallback();
		}

		// Token: 0x060009BC RID: 2492 RVA: 0x00016360 File Offset: 0x00014560
		protected void LogMetrics(IExecutionContext executionContext)
		{
			RequestMetrics metrics = executionContext.RequestContext.Metrics;
			if (executionContext.RequestContext.ClientConfig.LogMetrics)
			{
				string errors = metrics.GetErrors();
				if (!string.IsNullOrEmpty(errors))
				{
					this.Logger.InfoFormat("Request metrics errors: {0}", new object[]
					{
						errors
					});
				}
				this.Logger.InfoFormat("Request metrics: {0}", new object[]
				{
					metrics
				});
			}
		}
	}
}
