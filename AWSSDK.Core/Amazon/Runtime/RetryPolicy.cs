﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000DB RID: 219
	public abstract class RetryPolicy
	{
		// Token: 0x170002AD RID: 685
		// (get) Token: 0x060008A5 RID: 2213 RVA: 0x00014A65 File Offset: 0x00012C65
		// (set) Token: 0x060008A6 RID: 2214 RVA: 0x00014A6D File Offset: 0x00012C6D
		public int MaxRetries { get; protected set; }

		// Token: 0x170002AE RID: 686
		// (get) Token: 0x060008A7 RID: 2215 RVA: 0x00014A76 File Offset: 0x00012C76
		// (set) Token: 0x060008A8 RID: 2216 RVA: 0x00014A7E File Offset: 0x00012C7E
		public ILogger Logger { get; set; }

		// Token: 0x060008A9 RID: 2217 RVA: 0x00014A88 File Offset: 0x00012C88
		public bool Retry(IExecutionContext executionContext, Exception exception)
		{
			bool? flag = this.RetrySync(executionContext, exception);
			bool flag2;
			if (flag != null)
			{
				flag2 = flag.Value;
			}
			else
			{
				flag2 = this.RetryForException(executionContext, exception);
			}
			return flag2 && this.OnRetry(executionContext);
		}

		// Token: 0x060008AA RID: 2218
		public abstract bool CanRetry(IExecutionContext executionContext);

		// Token: 0x060008AB RID: 2219
		public abstract bool RetryForException(IExecutionContext executionContext, Exception exception);

		// Token: 0x060008AC RID: 2220
		public abstract bool RetryLimitReached(IExecutionContext executionContext);

		// Token: 0x060008AD RID: 2221
		public abstract void WaitBeforeRetry(IExecutionContext executionContext);

		// Token: 0x060008AE RID: 2222 RVA: 0x00005805 File Offset: 0x00003A05
		public virtual void NotifySuccess(IExecutionContext executionContext)
		{
		}

		// Token: 0x060008AF RID: 2223 RVA: 0x00011C11 File Offset: 0x0000FE11
		public virtual bool OnRetry(IExecutionContext executionContext)
		{
			return true;
		}

		// Token: 0x060008B0 RID: 2224 RVA: 0x00014AC8 File Offset: 0x00012CC8
		private bool? RetrySync(IExecutionContext executionContext, Exception exception)
		{
			if (this.RetryLimitReached(executionContext) || !this.CanRetry(executionContext))
			{
				return new bool?(false);
			}
			if (this.IsClockskew(executionContext, exception))
			{
				return new bool?(true);
			}
			return null;
		}

		// Token: 0x060008B1 RID: 2225 RVA: 0x00014B08 File Offset: 0x00012D08
		private bool IsClockskew(IExecutionContext executionContext, Exception exception)
		{
			AmazonServiceException ex = exception as AmazonServiceException;
			bool flag = executionContext.RequestContext.Request != null && string.Equals(executionContext.RequestContext.Request.HttpMethod, "HEAD", StringComparison.Ordinal);
			bool flag2 = ex != null && (ex.ErrorCode == null || RetryPolicy.clockSkewErrorCodes.Contains(ex.ErrorCode));
			if (flag || flag2)
			{
				DateTime dateTime = AWSConfigs.utcNowSource();
				DateTime correctedUtcNow = AWSSDKUtils.CorrectedUtcNow;
				DateTime dateTime2;
				bool flag3 = RetryPolicy.TryParseDateHeader(ex, out dateTime2);
				if (!flag3)
				{
					flag3 = RetryPolicy.TryParseExceptionMessage(ex, out dateTime2);
				}
				if (flag3)
				{
					dateTime2 = dateTime2.ToUniversalTime();
					TimeSpan timeSpan = correctedUtcNow - dateTime2;
					if (((timeSpan.Ticks < 0L) ? (-timeSpan) : timeSpan) > RetryPolicy.clockSkewMaxThreshold)
					{
						TimeSpan timeSpan2 = dateTime2 - dateTime;
						this.Logger.InfoFormat("Identified clock skew: local time = {0}, local time with correction = {1}, current clock skew correction = {2}, server time = {3}.", new object[]
						{
							dateTime,
							correctedUtcNow,
							AWSConfigs.ClockOffset,
							dateTime2
						});
						AWSConfigs.ClockOffset = timeSpan2;
						if (AWSConfigs.CorrectForClockSkew && AWSConfigs.ManualClockCorrection == null)
						{
							this.Logger.InfoFormat("Setting clock skew correction: new clock skew correction = {0}.", new object[]
							{
								timeSpan2
							});
							executionContext.RequestContext.IsSigned = false;
							return true;
						}
					}
				}
			}
			return false;
		}

		// Token: 0x060008B2 RID: 2226 RVA: 0x00014C74 File Offset: 0x00012E74
		private static bool TryParseDateHeader(AmazonServiceException ase, out DateTime serverTime)
		{
			IWebResponseData webData = RetryPolicy.GetWebData(ase);
			if (webData != null)
			{
				string headerValue = webData.GetHeaderValue("Date");
				if (!string.IsNullOrEmpty(headerValue) && DateTime.TryParseExact(headerValue, "ddd, dd MMM yyyy HH:mm:ss \\G\\M\\T", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out serverTime))
				{
					return true;
				}
			}
			serverTime = DateTime.MinValue;
			return false;
		}

		// Token: 0x060008B3 RID: 2227 RVA: 0x00014CC4 File Offset: 0x00012EC4
		private static bool TryParseExceptionMessage(AmazonServiceException ase, out DateTime serverTime)
		{
			if (ase != null && !string.IsNullOrEmpty(ase.Message))
			{
				string message = ase.Message;
				int num = message.IndexOf("(", StringComparison.Ordinal);
				if (num >= 0)
				{
					num++;
					int num2 = message.IndexOf(" + ", num, StringComparison.Ordinal);
					if (num2 < 0)
					{
						num2 = message.IndexOf(" - ", num, StringComparison.Ordinal);
					}
					if (num2 > num && DateTime.TryParseExact(message.Substring(num, num2 - num), "yyyyMMddTHHmmssZ", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out serverTime))
					{
						return true;
					}
				}
			}
			serverTime = DateTime.MinValue;
			return false;
		}

		// Token: 0x060008B4 RID: 2228 RVA: 0x00014D50 File Offset: 0x00012F50
		private static IWebResponseData GetWebData(AmazonServiceException ase)
		{
			if (ase != null)
			{
				Exception ex = ase;
				HttpErrorResponseException ex2;
				for (;;)
				{
					ex2 = (ex as HttpErrorResponseException);
					if (ex2 != null)
					{
						break;
					}
					ex = ex.InnerException;
					if (ex == null)
					{
						goto IL_20;
					}
				}
				return ex2.Response;
			}
			IL_20:
			return null;
		}

		// Token: 0x04000356 RID: 854
		private static HashSet<string> clockSkewErrorCodes = new HashSet<string>(StringComparer.OrdinalIgnoreCase)
		{
			"RequestTimeTooSkewed",
			"RequestExpired",
			"InvalidSignatureException",
			"SignatureDoesNotMatch",
			"AuthFailure",
			"RequestExpired",
			"RequestInTheFuture"
		};

		// Token: 0x04000357 RID: 855
		private const string clockSkewMessageFormat = "Identified clock skew: local time = {0}, local time with correction = {1}, current clock skew correction = {2}, server time = {3}.";

		// Token: 0x04000358 RID: 856
		private const string clockSkewUpdatedFormat = "Setting clock skew correction: new clock skew correction = {0}.";

		// Token: 0x04000359 RID: 857
		private const string clockSkewMessageParen = "(";

		// Token: 0x0400035A RID: 858
		private const string clockSkewMessagePlusSeparator = " + ";

		// Token: 0x0400035B RID: 859
		private const string clockSkewMessageMinusSeparator = " - ";

		// Token: 0x0400035C RID: 860
		private static TimeSpan clockSkewMaxThreshold = TimeSpan.FromMinutes(5.0);
	}
}
