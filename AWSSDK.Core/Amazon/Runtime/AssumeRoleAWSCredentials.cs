﻿using System;
using System.Globalization;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Util;
using Amazon.Runtime.SharedInterfaces;

namespace Amazon.Runtime
{
	// Token: 0x020000C2 RID: 194
	public class AssumeRoleAWSCredentials : RefreshingAWSCredentials
	{
		// Token: 0x1700026E RID: 622
		// (get) Token: 0x060007F5 RID: 2037 RVA: 0x000139BA File Offset: 0x00011BBA
		// (set) Token: 0x060007F6 RID: 2038 RVA: 0x000139C2 File Offset: 0x00011BC2
		public AWSCredentials SourceCredentials { get; private set; }

		// Token: 0x1700026F RID: 623
		// (get) Token: 0x060007F7 RID: 2039 RVA: 0x000139CB File Offset: 0x00011BCB
		// (set) Token: 0x060007F8 RID: 2040 RVA: 0x000139D3 File Offset: 0x00011BD3
		public string RoleArn { get; private set; }

		// Token: 0x17000270 RID: 624
		// (get) Token: 0x060007F9 RID: 2041 RVA: 0x000139DC File Offset: 0x00011BDC
		// (set) Token: 0x060007FA RID: 2042 RVA: 0x000139E4 File Offset: 0x00011BE4
		public string RoleSessionName { get; private set; }

		// Token: 0x17000271 RID: 625
		// (get) Token: 0x060007FB RID: 2043 RVA: 0x000139ED File Offset: 0x00011BED
		// (set) Token: 0x060007FC RID: 2044 RVA: 0x000139F5 File Offset: 0x00011BF5
		public AssumeRoleAWSCredentialsOptions Options { get; private set; }

		// Token: 0x060007FD RID: 2045 RVA: 0x000139FE File Offset: 0x00011BFE
		public AssumeRoleAWSCredentials(AWSCredentials sourceCredentials, string roleArn, string roleSessionName) : this(sourceCredentials, roleArn, roleSessionName, new AssumeRoleAWSCredentialsOptions())
		{
		}

		// Token: 0x060007FE RID: 2046 RVA: 0x00013A0E File Offset: 0x00011C0E
		public AssumeRoleAWSCredentials(AWSCredentials sourceCredentials, string roleArn, string roleSessionName, AssumeRoleAWSCredentialsOptions options)
		{
			if (options == null)
			{
				throw new ArgumentNullException("options");
			}
			this.SourceCredentials = sourceCredentials;
			this.RoleArn = roleArn;
			this.RoleSessionName = roleSessionName;
			this.Options = options;
		}

		// Token: 0x060007FF RID: 2047 RVA: 0x00013A50 File Offset: 0x00011C50
		protected override RefreshingAWSCredentials.CredentialsRefreshState GenerateNewCredentials()
		{
			string awsregion = AWSConfigs.AWSRegion;
			RegionEndpoint regionEndpoint = string.IsNullOrEmpty(awsregion) ? this.DefaultSTSClientRegion : RegionEndpoint.GetBySystemName(awsregion);
			ICoreAmazonSTS coreAmazonSTS = null;
			try
			{
				ClientConfig clientConfig = ServiceClientHelpers.CreateServiceConfig("AWSSDK.SecurityToken", "Amazon.SecurityToken.AmazonSecurityTokenServiceConfig");
				clientConfig.RegionEndpoint = regionEndpoint;
				if (this.Options != null && this.Options.ProxySettings != null)
				{
					clientConfig.SetWebProxy(this.Options.ProxySettings);
				}
				coreAmazonSTS = ServiceClientHelpers.CreateServiceFromAssembly<ICoreAmazonSTS>("AWSSDK.SecurityToken", "Amazon.SecurityToken.AmazonSecurityTokenServiceClient", this.SourceCredentials, clientConfig);
			}
			catch (Exception innerException)
			{
				InvalidOperationException ex = new InvalidOperationException(string.Format(CultureInfo.CurrentCulture, "Assembly {0} could not be found or loaded. This assembly must be available at runtime to use Amazon.Runtime.AssumeRoleAWSCredentials.", new object[]
				{
					"AWSSDK.SecurityToken"
				}), innerException);
				Logger.GetLogger(typeof(AssumeRoleAWSCredentials)).Error(ex, ex.Message, new object[0]);
				throw ex;
			}
			AssumeRoleImmutableCredentials assumeRoleImmutableCredentials = coreAmazonSTS.CredentialsFromAssumeRoleAuthentication(this.RoleArn, this.RoleSessionName, this.Options);
			return new RefreshingAWSCredentials.CredentialsRefreshState(assumeRoleImmutableCredentials, assumeRoleImmutableCredentials.Expiration);
		}

		// Token: 0x04000329 RID: 809
		private RegionEndpoint DefaultSTSClientRegion = RegionEndpoint.USEast1;
	}
}
