﻿using System;

namespace Amazon.Runtime.SharedInterfaces
{
	// Token: 0x020000E1 RID: 225
	public interface ICoreAmazonSTS
	{
		// Token: 0x060008CA RID: 2250
		AssumeRoleImmutableCredentials CredentialsFromAssumeRoleAuthentication(string roleArn, string roleSessionName, AssumeRoleAWSCredentialsOptions options);
	}
}
