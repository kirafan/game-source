﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime.SharedInterfaces
{
	// Token: 0x020000DF RID: 223
	public interface ICoreAmazonS3
	{
		// Token: 0x060008C9 RID: 2249
		string GeneratePreSignedURL(string bucketName, string objectKey, DateTime expiration, IDictionary<string, object> additionalProperties);
	}
}
