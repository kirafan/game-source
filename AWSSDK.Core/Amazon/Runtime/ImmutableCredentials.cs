﻿using System;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000C9 RID: 201
	public class ImmutableCredentials
	{
		// Token: 0x1700027B RID: 635
		// (get) Token: 0x06000825 RID: 2085 RVA: 0x00014084 File Offset: 0x00012284
		// (set) Token: 0x06000826 RID: 2086 RVA: 0x0001408C File Offset: 0x0001228C
		public string AccessKey { get; private set; }

		// Token: 0x1700027C RID: 636
		// (get) Token: 0x06000827 RID: 2087 RVA: 0x00014095 File Offset: 0x00012295
		// (set) Token: 0x06000828 RID: 2088 RVA: 0x0001409D File Offset: 0x0001229D
		public string SecretKey { get; private set; }

		// Token: 0x1700027D RID: 637
		// (get) Token: 0x06000829 RID: 2089 RVA: 0x000140A6 File Offset: 0x000122A6
		// (set) Token: 0x0600082A RID: 2090 RVA: 0x000140AE File Offset: 0x000122AE
		public string Token { get; private set; }

		// Token: 0x1700027E RID: 638
		// (get) Token: 0x0600082B RID: 2091 RVA: 0x000140B7 File Offset: 0x000122B7
		public bool UseToken
		{
			get
			{
				return !string.IsNullOrEmpty(this.Token);
			}
		}

		// Token: 0x0600082C RID: 2092 RVA: 0x000140C8 File Offset: 0x000122C8
		public ImmutableCredentials(string awsAccessKeyId, string awsSecretAccessKey, string token)
		{
			if (string.IsNullOrEmpty(awsAccessKeyId))
			{
				throw new ArgumentNullException("awsAccessKeyId");
			}
			if (string.IsNullOrEmpty(awsSecretAccessKey))
			{
				throw new ArgumentNullException("awsSecretAccessKey");
			}
			this.AccessKey = awsAccessKeyId;
			this.SecretKey = awsSecretAccessKey;
			this.Token = (token ?? string.Empty);
		}

		// Token: 0x0600082D RID: 2093 RVA: 0x0000584A File Offset: 0x00003A4A
		private ImmutableCredentials()
		{
		}

		// Token: 0x0600082E RID: 2094 RVA: 0x0001411F File Offset: 0x0001231F
		public virtual ImmutableCredentials Copy()
		{
			return new ImmutableCredentials
			{
				AccessKey = this.AccessKey,
				SecretKey = this.SecretKey,
				Token = this.Token
			};
		}

		// Token: 0x0600082F RID: 2095 RVA: 0x0001414A File Offset: 0x0001234A
		public override int GetHashCode()
		{
			return Hashing.Hash(new object[]
			{
				this.AccessKey,
				this.SecretKey,
				this.Token
			});
		}

		// Token: 0x06000830 RID: 2096 RVA: 0x00014174 File Offset: 0x00012374
		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			ImmutableCredentials immutableCredentials = obj as ImmutableCredentials;
			return immutableCredentials != null && AWSSDKUtils.AreEqual(new object[]
			{
				this.AccessKey,
				this.SecretKey,
				this.Token
			}, new object[]
			{
				immutableCredentials.AccessKey,
				immutableCredentials.SecretKey,
				immutableCredentials.Token
			});
		}
	}
}
