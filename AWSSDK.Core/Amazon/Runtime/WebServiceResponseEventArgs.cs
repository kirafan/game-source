﻿using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime
{
	// Token: 0x020000BD RID: 189
	public class WebServiceResponseEventArgs : ResponseEventArgs
	{
		// Token: 0x060007D6 RID: 2006 RVA: 0x000137A5 File Offset: 0x000119A5
		protected WebServiceResponseEventArgs()
		{
		}

		// Token: 0x17000261 RID: 609
		// (get) Token: 0x060007D7 RID: 2007 RVA: 0x000137AD File Offset: 0x000119AD
		// (set) Token: 0x060007D8 RID: 2008 RVA: 0x000137B5 File Offset: 0x000119B5
		public IDictionary<string, string> RequestHeaders { get; private set; }

		// Token: 0x17000262 RID: 610
		// (get) Token: 0x060007D9 RID: 2009 RVA: 0x000137BE File Offset: 0x000119BE
		// (set) Token: 0x060007DA RID: 2010 RVA: 0x000137C6 File Offset: 0x000119C6
		public IDictionary<string, string> ResponseHeaders { get; private set; }

		// Token: 0x17000263 RID: 611
		// (get) Token: 0x060007DB RID: 2011 RVA: 0x000137CF File Offset: 0x000119CF
		// (set) Token: 0x060007DC RID: 2012 RVA: 0x000137D7 File Offset: 0x000119D7
		public IDictionary<string, string> Parameters { get; private set; }

		// Token: 0x17000264 RID: 612
		// (get) Token: 0x060007DD RID: 2013 RVA: 0x000137E0 File Offset: 0x000119E0
		// (set) Token: 0x060007DE RID: 2014 RVA: 0x000137E8 File Offset: 0x000119E8
		public string ServiceName { get; private set; }

		// Token: 0x17000265 RID: 613
		// (get) Token: 0x060007DF RID: 2015 RVA: 0x000137F1 File Offset: 0x000119F1
		// (set) Token: 0x060007E0 RID: 2016 RVA: 0x000137F9 File Offset: 0x000119F9
		public Uri Endpoint { get; private set; }

		// Token: 0x17000266 RID: 614
		// (get) Token: 0x060007E1 RID: 2017 RVA: 0x00013802 File Offset: 0x00011A02
		// (set) Token: 0x060007E2 RID: 2018 RVA: 0x0001380A File Offset: 0x00011A0A
		public AmazonWebServiceRequest Request { get; private set; }

		// Token: 0x17000267 RID: 615
		// (get) Token: 0x060007E3 RID: 2019 RVA: 0x00013813 File Offset: 0x00011A13
		// (set) Token: 0x060007E4 RID: 2020 RVA: 0x0001381B File Offset: 0x00011A1B
		public AmazonWebServiceResponse Response { get; private set; }

		// Token: 0x060007E5 RID: 2021 RVA: 0x00013824 File Offset: 0x00011A24
		internal static WebServiceResponseEventArgs Create(AmazonWebServiceResponse response, IRequest request, IWebResponseData webResponseData)
		{
			WebServiceResponseEventArgs webServiceResponseEventArgs = new WebServiceResponseEventArgs
			{
				RequestHeaders = request.Headers,
				Parameters = request.Parameters,
				ServiceName = request.ServiceName,
				Request = request.OriginalRequest,
				Endpoint = request.Endpoint,
				Response = response
			};
			webServiceResponseEventArgs.ResponseHeaders = new Dictionary<string, string>();
			if (webResponseData != null)
			{
				foreach (string text in webResponseData.GetHeaderNames())
				{
					string headerValue = webResponseData.GetHeaderValue(text);
					webServiceResponseEventArgs.ResponseHeaders[text] = headerValue;
				}
			}
			return webServiceResponseEventArgs;
		}
	}
}
