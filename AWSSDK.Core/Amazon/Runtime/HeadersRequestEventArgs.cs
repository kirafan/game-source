﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime
{
	// Token: 0x020000B5 RID: 181
	public class HeadersRequestEventArgs : RequestEventArgs
	{
		// Token: 0x060007C0 RID: 1984 RVA: 0x000136B4 File Offset: 0x000118B4
		protected HeadersRequestEventArgs()
		{
		}

		// Token: 0x17000259 RID: 601
		// (get) Token: 0x060007C1 RID: 1985 RVA: 0x00013786 File Offset: 0x00011986
		// (set) Token: 0x060007C2 RID: 1986 RVA: 0x0001378E File Offset: 0x0001198E
		public IDictionary<string, string> Headers { get; protected set; }

		// Token: 0x060007C3 RID: 1987 RVA: 0x00013797 File Offset: 0x00011997
		internal static HeadersRequestEventArgs Create(IDictionary<string, string> headers)
		{
			return new HeadersRequestEventArgs
			{
				Headers = headers
			};
		}
	}
}
