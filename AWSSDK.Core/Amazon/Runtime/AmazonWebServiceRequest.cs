﻿using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Auth;

namespace Amazon.Runtime
{
	// Token: 0x0200009F RID: 159
	public abstract class AmazonWebServiceRequest : IAmazonWebServiceRequest
	{
		// Token: 0x1400001F RID: 31
		// (add) Token: 0x060006E9 RID: 1769 RVA: 0x000128D4 File Offset: 0x00010AD4
		// (remove) Token: 0x060006EA RID: 1770 RVA: 0x0001291C File Offset: 0x00010B1C
		internal event RequestEventHandler BeforeRequestEvent
		{
			add
			{
				lock (this)
				{
					this.mBeforeRequestEvent = (RequestEventHandler)Delegate.Combine(this.mBeforeRequestEvent, value);
				}
			}
			remove
			{
				lock (this)
				{
					this.mBeforeRequestEvent = (RequestEventHandler)Delegate.Remove(this.mBeforeRequestEvent, value);
				}
			}
		}

		// Token: 0x170001FD RID: 509
		// (get) Token: 0x060006EB RID: 1771 RVA: 0x00012964 File Offset: 0x00010B64
		// (set) Token: 0x060006EC RID: 1772 RVA: 0x0001296C File Offset: 0x00010B6C
		EventHandler<StreamTransferProgressArgs> IAmazonWebServiceRequest.StreamUploadProgressCallback { get; set; }

		// Token: 0x170001FE RID: 510
		// (get) Token: 0x060006ED RID: 1773 RVA: 0x00012975 File Offset: 0x00010B75
		Dictionary<string, object> IAmazonWebServiceRequest.RequestState
		{
			get
			{
				if (this.requestState == null)
				{
					this.requestState = new Dictionary<string, object>();
				}
				return this.requestState;
			}
		}

		// Token: 0x060006EF RID: 1775 RVA: 0x00012990 File Offset: 0x00010B90
		void IAmazonWebServiceRequest.AddBeforeRequestHandler(RequestEventHandler handler)
		{
			this.BeforeRequestEvent += handler;
		}

		// Token: 0x060006F0 RID: 1776 RVA: 0x00012999 File Offset: 0x00010B99
		void IAmazonWebServiceRequest.RemoveBeforeRequestHandler(RequestEventHandler handler)
		{
			this.BeforeRequestEvent -= handler;
		}

		// Token: 0x060006F1 RID: 1777 RVA: 0x000129A2 File Offset: 0x00010BA2
		internal void FireBeforeRequestEvent(object sender, RequestEventArgs args)
		{
			if (this.mBeforeRequestEvent != null)
			{
				this.mBeforeRequestEvent(sender, args);
			}
		}

		// Token: 0x170001FF RID: 511
		// (get) Token: 0x060006F2 RID: 1778 RVA: 0x000129B9 File Offset: 0x00010BB9
		// (set) Token: 0x060006F3 RID: 1779 RVA: 0x000129C1 File Offset: 0x00010BC1
		bool IAmazonWebServiceRequest.UseSigV4 { get; set; }

		// Token: 0x17000200 RID: 512
		// (get) Token: 0x060006F4 RID: 1780 RVA: 0x000129CA File Offset: 0x00010BCA
		protected virtual bool Expect100Continue
		{
			get
			{
				return false;
			}
		}

		// Token: 0x060006F5 RID: 1781 RVA: 0x000129CD File Offset: 0x00010BCD
		internal bool GetExpect100Continue()
		{
			return this.Expect100Continue;
		}

		// Token: 0x17000201 RID: 513
		// (get) Token: 0x060006F6 RID: 1782 RVA: 0x00011C11 File Offset: 0x0000FE11
		protected virtual bool IncludeSHA256Header
		{
			get
			{
				return true;
			}
		}

		// Token: 0x060006F7 RID: 1783 RVA: 0x000129D5 File Offset: 0x00010BD5
		internal bool GetIncludeSHA256Header()
		{
			return this.IncludeSHA256Header;
		}

		// Token: 0x060006F8 RID: 1784 RVA: 0x0000CD44 File Offset: 0x0000AF44
		protected virtual AbstractAWSSigner CreateSigner()
		{
			return null;
		}

		// Token: 0x060006F9 RID: 1785 RVA: 0x000129DD File Offset: 0x00010BDD
		internal AbstractAWSSigner GetSigner()
		{
			return this.CreateSigner();
		}

		// Token: 0x040002BD RID: 701
		internal RequestEventHandler mBeforeRequestEvent;

		// Token: 0x040002BF RID: 703
		private Dictionary<string, object> requestState;
	}
}
