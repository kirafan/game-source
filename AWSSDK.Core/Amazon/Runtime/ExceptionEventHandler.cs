﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000A8 RID: 168
	// (Invoke) Token: 0x06000769 RID: 1897
	public delegate void ExceptionEventHandler(object sender, ExceptionEventArgs e);
}
