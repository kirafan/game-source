﻿using System;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000C4 RID: 196
	public class AssumeRoleImmutableCredentials : ImmutableCredentials
	{
		// Token: 0x17000279 RID: 633
		// (get) Token: 0x0600080E RID: 2062 RVA: 0x00013BF1 File Offset: 0x00011DF1
		// (set) Token: 0x0600080F RID: 2063 RVA: 0x00013BF9 File Offset: 0x00011DF9
		public DateTime Expiration { get; private set; }

		// Token: 0x06000810 RID: 2064 RVA: 0x00013C02 File Offset: 0x00011E02
		public AssumeRoleImmutableCredentials(string awsAccessKeyId, string awsSecretAccessKey, string token, DateTime expiration) : base(awsAccessKeyId, awsSecretAccessKey, token)
		{
			if (string.IsNullOrEmpty(token))
			{
				throw new ArgumentNullException("token");
			}
			this.Expiration = expiration;
		}

		// Token: 0x06000811 RID: 2065 RVA: 0x00013C28 File Offset: 0x00011E28
		public new AssumeRoleImmutableCredentials Copy()
		{
			return new AssumeRoleImmutableCredentials(base.AccessKey, base.SecretKey, base.Token, this.Expiration);
		}

		// Token: 0x06000812 RID: 2066 RVA: 0x00013C47 File Offset: 0x00011E47
		public override int GetHashCode()
		{
			return Hashing.Hash(new object[]
			{
				base.AccessKey,
				base.SecretKey,
				base.Token,
				this.Expiration
			});
		}

		// Token: 0x06000813 RID: 2067 RVA: 0x00013C80 File Offset: 0x00011E80
		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			AssumeRoleImmutableCredentials assumeRoleImmutableCredentials = obj as AssumeRoleImmutableCredentials;
			return assumeRoleImmutableCredentials != null && AWSSDKUtils.AreEqual(new object[]
			{
				base.AccessKey,
				base.SecretKey,
				base.Token,
				this.Expiration
			}, new object[]
			{
				assumeRoleImmutableCredentials.AccessKey,
				assumeRoleImmutableCredentials.SecretKey,
				assumeRoleImmutableCredentials.Token,
				this.Expiration
			});
		}
	}
}
