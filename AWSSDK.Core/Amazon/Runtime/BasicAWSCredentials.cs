﻿using System;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000C6 RID: 198
	public class BasicAWSCredentials : AWSCredentials
	{
		// Token: 0x06000817 RID: 2071 RVA: 0x00013D02 File Offset: 0x00011F02
		public BasicAWSCredentials(string accessKey, string secretKey)
		{
			if (!string.IsNullOrEmpty(accessKey))
			{
				this._credentials = new ImmutableCredentials(accessKey, secretKey, null);
			}
		}

		// Token: 0x06000818 RID: 2072 RVA: 0x00013D20 File Offset: 0x00011F20
		public override ImmutableCredentials GetCredentials()
		{
			if (this._credentials == null)
			{
				return null;
			}
			return this._credentials.Copy();
		}

		// Token: 0x06000819 RID: 2073 RVA: 0x00013D38 File Offset: 0x00011F38
		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			BasicAWSCredentials basicAWSCredentials = obj as BasicAWSCredentials;
			return basicAWSCredentials != null && AWSSDKUtils.AreEqual(new object[]
			{
				this._credentials
			}, new object[]
			{
				basicAWSCredentials._credentials
			});
		}

		// Token: 0x0600081A RID: 2074 RVA: 0x00013D7A File Offset: 0x00011F7A
		public override int GetHashCode()
		{
			return Hashing.Hash(new object[]
			{
				this._credentials
			});
		}

		// Token: 0x04000335 RID: 821
		private ImmutableCredentials _credentials;
	}
}
