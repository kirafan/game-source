﻿using System;
using System.Globalization;
using System.Net;
using System.Runtime.Serialization;
using System.Security;

namespace Amazon.Runtime
{
	// Token: 0x0200009D RID: 157
	[Serializable]
	public class AmazonServiceException : Exception
	{
		// Token: 0x060006CC RID: 1740 RVA: 0x00008C59 File Offset: 0x00006E59
		public AmazonServiceException()
		{
		}

		// Token: 0x060006CD RID: 1741 RVA: 0x00008CC5 File Offset: 0x00006EC5
		public AmazonServiceException(string message) : base(message)
		{
		}

		// Token: 0x060006CE RID: 1742 RVA: 0x00008CCE File Offset: 0x00006ECE
		public AmazonServiceException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060006CF RID: 1743 RVA: 0x0001258D File Offset: 0x0001078D
		public AmazonServiceException(string message, Exception innerException, HttpStatusCode statusCode) : base(message, innerException)
		{
			this.statusCode = statusCode;
		}

		// Token: 0x060006D0 RID: 1744 RVA: 0x0001259E File Offset: 0x0001079E
		public AmazonServiceException(Exception innerException) : base(innerException.Message, innerException)
		{
		}

		// Token: 0x060006D1 RID: 1745 RVA: 0x000125AD File Offset: 0x000107AD
		public AmazonServiceException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message ?? AmazonServiceException.BuildGenericErrorMessage(errorCode, statusCode))
		{
			this.errorCode = errorCode;
			this.errorType = errorType;
			this.requestId = requestId;
			this.statusCode = statusCode;
		}

		// Token: 0x060006D2 RID: 1746 RVA: 0x000125E0 File Offset: 0x000107E0
		public AmazonServiceException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message ?? AmazonServiceException.BuildGenericErrorMessage(errorCode, statusCode), innerException)
		{
			this.errorCode = errorCode;
			this.errorType = errorType;
			this.requestId = requestId;
			this.statusCode = statusCode;
		}

		// Token: 0x060006D3 RID: 1747 RVA: 0x00012616 File Offset: 0x00010816
		private static string BuildGenericErrorMessage(string errorCode, HttpStatusCode statusCode)
		{
			return string.Format(CultureInfo.InvariantCulture, "Error making request with Error Code {0} and Http Status Code {1}. No further error information was returned by the service.", new object[]
			{
				errorCode,
				statusCode
			});
		}

		// Token: 0x170001F6 RID: 502
		// (get) Token: 0x060006D4 RID: 1748 RVA: 0x0001263A File Offset: 0x0001083A
		// (set) Token: 0x060006D5 RID: 1749 RVA: 0x00012642 File Offset: 0x00010842
		public ErrorType ErrorType
		{
			get
			{
				return this.errorType;
			}
			set
			{
				this.errorType = value;
			}
		}

		// Token: 0x170001F7 RID: 503
		// (get) Token: 0x060006D6 RID: 1750 RVA: 0x0001264B File Offset: 0x0001084B
		// (set) Token: 0x060006D7 RID: 1751 RVA: 0x00012653 File Offset: 0x00010853
		public string ErrorCode
		{
			get
			{
				return this.errorCode;
			}
			set
			{
				this.errorCode = value;
			}
		}

		// Token: 0x170001F8 RID: 504
		// (get) Token: 0x060006D8 RID: 1752 RVA: 0x0001265C File Offset: 0x0001085C
		// (set) Token: 0x060006D9 RID: 1753 RVA: 0x00012664 File Offset: 0x00010864
		public string RequestId
		{
			get
			{
				return this.requestId;
			}
			set
			{
				this.requestId = value;
			}
		}

		// Token: 0x170001F9 RID: 505
		// (get) Token: 0x060006DA RID: 1754 RVA: 0x0001266D File Offset: 0x0001086D
		// (set) Token: 0x060006DB RID: 1755 RVA: 0x00012675 File Offset: 0x00010875
		public HttpStatusCode StatusCode
		{
			get
			{
				return this.statusCode;
			}
			set
			{
				this.statusCode = value;
			}
		}

		// Token: 0x060006DC RID: 1756 RVA: 0x00012680 File Offset: 0x00010880
		protected AmazonServiceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if (info != null)
			{
				this.errorCode = info.GetString("errorCode");
				this.errorType = (ErrorType)info.GetValue("errorType", typeof(ErrorType));
				this.requestId = info.GetString("requestId");
				this.statusCode = (HttpStatusCode)info.GetValue("statusCode", typeof(HttpStatusCode));
			}
		}

		// Token: 0x060006DD RID: 1757 RVA: 0x000126FC File Offset: 0x000108FC
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			if (info != null)
			{
				info.AddValue("errorCode", this.errorCode);
				info.AddValue("errorType", this.errorType);
				info.AddValue("requestId", this.requestId);
				info.AddValue("statusCode", this.statusCode);
			}
		}

		// Token: 0x040002B7 RID: 695
		private ErrorType errorType;

		// Token: 0x040002B8 RID: 696
		private string errorCode;

		// Token: 0x040002B9 RID: 697
		private string requestId;

		// Token: 0x040002BA RID: 698
		private HttpStatusCode statusCode;
	}
}
