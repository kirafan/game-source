﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000C1 RID: 193
	public class AnonymousAWSCredentials : AWSCredentials
	{
		// Token: 0x060007F3 RID: 2035 RVA: 0x000139A6 File Offset: 0x00011BA6
		public override ImmutableCredentials GetCredentials()
		{
			throw new NotSupportedException("AnonymousAWSCredentials do not support this operation");
		}
	}
}
