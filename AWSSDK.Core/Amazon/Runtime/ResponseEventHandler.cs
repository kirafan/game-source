﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000BB RID: 187
	// (Invoke) Token: 0x060007D2 RID: 2002
	public delegate void ResponseEventHandler(object sender, ResponseEventArgs e);
}
