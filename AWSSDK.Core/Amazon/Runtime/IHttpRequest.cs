﻿using System;
using System.Collections.Generic;
using System.IO;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.Runtime
{
	// Token: 0x020000D9 RID: 217
	public interface IHttpRequest<TRequestContent> : IDisposable
	{
		// Token: 0x170002A2 RID: 674
		// (get) Token: 0x06000888 RID: 2184
		// (set) Token: 0x06000889 RID: 2185
		string Method { get; set; }

		// Token: 0x170002A3 RID: 675
		// (get) Token: 0x0600088A RID: 2186
		Uri RequestUri { get; }

		// Token: 0x0600088B RID: 2187
		void ConfigureRequest(IRequestContext requestContext);

		// Token: 0x0600088C RID: 2188
		void SetRequestHeaders(IDictionary<string, string> headers);

		// Token: 0x0600088D RID: 2189
		TRequestContent GetRequestContent();

		// Token: 0x0600088E RID: 2190
		IWebResponseData GetResponse();

		// Token: 0x0600088F RID: 2191
		void WriteToRequestBody(TRequestContent requestContent, Stream contentStream, IDictionary<string, string> contentHeaders, IRequestContext requestContext);

		// Token: 0x06000890 RID: 2192
		void WriteToRequestBody(TRequestContent requestContent, byte[] content, IDictionary<string, string> contentHeaders);

		// Token: 0x06000891 RID: 2193
		Stream SetupProgressListeners(Stream originalStream, long progressUpdateInterval, object sender, EventHandler<StreamTransferProgressArgs> callback);

		// Token: 0x06000892 RID: 2194
		void Abort();

		// Token: 0x06000893 RID: 2195
		IAsyncResult BeginGetRequestContent(AsyncCallback callback, object state);

		// Token: 0x06000894 RID: 2196
		TRequestContent EndGetRequestContent(IAsyncResult asyncResult);

		// Token: 0x06000895 RID: 2197
		IAsyncResult BeginGetResponse(AsyncCallback callback, object state);

		// Token: 0x06000896 RID: 2198
		IWebResponseData EndGetResponse(IAsyncResult asyncResult);
	}
}
