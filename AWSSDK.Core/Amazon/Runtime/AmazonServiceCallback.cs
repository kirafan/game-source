﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000DC RID: 220
	// (Invoke) Token: 0x060008B8 RID: 2232
	public delegate void AmazonServiceCallback<TRequest, TResponse>(AmazonServiceResult<TRequest, TResponse> responseObject) where TRequest : AmazonWebServiceRequest where TResponse : AmazonWebServiceResponse;
}
