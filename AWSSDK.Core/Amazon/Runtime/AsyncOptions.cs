﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000DE RID: 222
	public class AsyncOptions
	{
		// Token: 0x060008C4 RID: 2244 RVA: 0x00014E6C File Offset: 0x0001306C
		public AsyncOptions()
		{
			this.ExecuteCallbackOnMainThread = true;
		}

		// Token: 0x170002B3 RID: 691
		// (get) Token: 0x060008C5 RID: 2245 RVA: 0x00014E7B File Offset: 0x0001307B
		// (set) Token: 0x060008C6 RID: 2246 RVA: 0x00014E83 File Offset: 0x00013083
		public bool ExecuteCallbackOnMainThread { get; set; }

		// Token: 0x170002B4 RID: 692
		// (get) Token: 0x060008C7 RID: 2247 RVA: 0x00014E8C File Offset: 0x0001308C
		// (set) Token: 0x060008C8 RID: 2248 RVA: 0x00014E94 File Offset: 0x00013094
		public object State { get; set; }
	}
}
