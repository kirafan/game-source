﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000AD RID: 173
	public interface ILogMessage
	{
		// Token: 0x1700024C RID: 588
		// (get) Token: 0x0600079C RID: 1948
		string Format { get; }

		// Token: 0x1700024D RID: 589
		// (get) Token: 0x0600079D RID: 1949
		object[] Args { get; }

		// Token: 0x1700024E RID: 590
		// (get) Token: 0x0600079E RID: 1950
		IFormatProvider Provider { get; }
	}
}
