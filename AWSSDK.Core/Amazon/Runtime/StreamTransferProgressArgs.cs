﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000C0 RID: 192
	public class StreamTransferProgressArgs : EventArgs
	{
		// Token: 0x060007ED RID: 2029 RVA: 0x00013902 File Offset: 0x00011B02
		public StreamTransferProgressArgs(long incrementTransferred, long transferred, long total)
		{
			this._incrementTransferred = incrementTransferred;
			this._transferred = transferred;
			this._total = total;
		}

		// Token: 0x1700026A RID: 618
		// (get) Token: 0x060007EE RID: 2030 RVA: 0x0001391F File Offset: 0x00011B1F
		public int PercentDone
		{
			get
			{
				return (int)(this._transferred * 100L / this._total);
			}
		}

		// Token: 0x1700026B RID: 619
		// (get) Token: 0x060007EF RID: 2031 RVA: 0x00013933 File Offset: 0x00011B33
		public long IncrementTransferred
		{
			get
			{
				return this._incrementTransferred;
			}
		}

		// Token: 0x1700026C RID: 620
		// (get) Token: 0x060007F0 RID: 2032 RVA: 0x0001393B File Offset: 0x00011B3B
		public long TransferredBytes
		{
			get
			{
				return this._transferred;
			}
		}

		// Token: 0x1700026D RID: 621
		// (get) Token: 0x060007F1 RID: 2033 RVA: 0x00013943 File Offset: 0x00011B43
		public long TotalBytes
		{
			get
			{
				return this._total;
			}
		}

		// Token: 0x060007F2 RID: 2034 RVA: 0x0001394C File Offset: 0x00011B4C
		public override string ToString()
		{
			return string.Concat(new object[]
			{
				"Transfer Statistics. Percentage completed: ",
				this.PercentDone,
				", Bytes transferred: ",
				this._transferred,
				", Total bytes to transfer: ",
				this._total
			});
		}

		// Token: 0x04000326 RID: 806
		private long _incrementTransferred;

		// Token: 0x04000327 RID: 807
		private long _total;

		// Token: 0x04000328 RID: 808
		private long _transferred;
	}
}
