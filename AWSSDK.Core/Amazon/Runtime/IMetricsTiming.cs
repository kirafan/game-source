﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000B8 RID: 184
	public interface IMetricsTiming
	{
		// Token: 0x1700025E RID: 606
		// (get) Token: 0x060007CD RID: 1997
		bool IsFinished { get; }

		// Token: 0x1700025F RID: 607
		// (get) Token: 0x060007CE RID: 1998
		long ElapsedTicks { get; }

		// Token: 0x17000260 RID: 608
		// (get) Token: 0x060007CF RID: 1999
		TimeSpan ElapsedTime { get; }
	}
}
