﻿using System;
using System.Globalization;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000A2 RID: 162
	public class AppConfigAWSRegion : AWSRegion
	{
		// Token: 0x06000705 RID: 1797 RVA: 0x00012A38 File Offset: 0x00010C38
		public AppConfigAWSRegion()
		{
			RegionEndpoint regionEndpoint = AWSConfigs.RegionEndpoint;
			if (regionEndpoint != null)
			{
				base.Region = regionEndpoint;
				Logger.GetLogger(typeof(AppConfigAWSRegion)).InfoFormat("Region {0} found using {1} setting in application configuration file.", new object[]
				{
					regionEndpoint.SystemName,
					"AWSRegion"
				});
				return;
			}
			throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "The app.config/web.config files for the application did not contain region information", new object[0]));
		}
	}
}
