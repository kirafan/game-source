﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000B2 RID: 178
	// (Invoke) Token: 0x060007AD RID: 1965
	public delegate void PreRequestEventHandler(object sender, PreRequestEventArgs e);
}
