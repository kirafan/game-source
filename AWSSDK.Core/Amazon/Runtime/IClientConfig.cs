﻿using System;
using System.Net;

namespace Amazon.Runtime
{
	// Token: 0x020000AC RID: 172
	public interface IClientConfig
	{
		// Token: 0x1700022F RID: 559
		// (get) Token: 0x0600077C RID: 1916
		RegionEndpoint RegionEndpoint { get; }

		// Token: 0x17000230 RID: 560
		// (get) Token: 0x0600077D RID: 1917
		string RegionEndpointServiceName { get; }

		// Token: 0x17000231 RID: 561
		// (get) Token: 0x0600077E RID: 1918
		string ServiceURL { get; }

		// Token: 0x17000232 RID: 562
		// (get) Token: 0x0600077F RID: 1919
		bool UseHttp { get; }

		// Token: 0x17000233 RID: 563
		// (get) Token: 0x06000780 RID: 1920
		string ServiceVersion { get; }

		// Token: 0x17000234 RID: 564
		// (get) Token: 0x06000781 RID: 1921
		SigningAlgorithm SignatureMethod { get; }

		// Token: 0x17000235 RID: 565
		// (get) Token: 0x06000782 RID: 1922
		string SignatureVersion { get; }

		// Token: 0x17000236 RID: 566
		// (get) Token: 0x06000783 RID: 1923
		string AuthenticationRegion { get; }

		// Token: 0x17000237 RID: 567
		// (get) Token: 0x06000784 RID: 1924
		string AuthenticationServiceName { get; }

		// Token: 0x17000238 RID: 568
		// (get) Token: 0x06000785 RID: 1925
		string UserAgent { get; }

		// Token: 0x17000239 RID: 569
		// (get) Token: 0x06000786 RID: 1926
		bool DisableLogging { get; }

		// Token: 0x1700023A RID: 570
		// (get) Token: 0x06000787 RID: 1927
		bool LogMetrics { get; }

		// Token: 0x1700023B RID: 571
		// (get) Token: 0x06000788 RID: 1928
		bool LogResponse { get; }

		// Token: 0x1700023C RID: 572
		// (get) Token: 0x06000789 RID: 1929
		bool ReadEntireResponse { get; }

		// Token: 0x1700023D RID: 573
		// (get) Token: 0x0600078A RID: 1930
		bool AllowAutoRedirect { get; }

		// Token: 0x1700023E RID: 574
		// (get) Token: 0x0600078B RID: 1931
		int BufferSize { get; }

		// Token: 0x1700023F RID: 575
		// (get) Token: 0x0600078C RID: 1932
		int MaxErrorRetry { get; }

		// Token: 0x17000240 RID: 576
		// (get) Token: 0x0600078D RID: 1933
		long ProgressUpdateInterval { get; }

		// Token: 0x17000241 RID: 577
		// (get) Token: 0x0600078E RID: 1934
		bool ResignRetries { get; }

		// Token: 0x17000242 RID: 578
		// (get) Token: 0x0600078F RID: 1935
		ICredentials ProxyCredentials { get; }

		// Token: 0x17000243 RID: 579
		// (get) Token: 0x06000790 RID: 1936
		TimeSpan? Timeout { get; }

		// Token: 0x17000244 RID: 580
		// (get) Token: 0x06000791 RID: 1937
		bool UseDualstackEndpoint { get; }

		// Token: 0x17000245 RID: 581
		// (get) Token: 0x06000792 RID: 1938
		bool ThrottleRetries { get; }

		// Token: 0x06000793 RID: 1939
		string DetermineServiceURL();

		// Token: 0x06000794 RID: 1940
		void Validate();

		// Token: 0x17000246 RID: 582
		// (get) Token: 0x06000795 RID: 1941
		string ProxyHost { get; }

		// Token: 0x17000247 RID: 583
		// (get) Token: 0x06000796 RID: 1942
		int ProxyPort { get; }

		// Token: 0x17000248 RID: 584
		// (get) Token: 0x06000797 RID: 1943
		int MaxIdleTime { get; }

		// Token: 0x17000249 RID: 585
		// (get) Token: 0x06000798 RID: 1944
		TimeSpan? ReadWriteTimeout { get; }

		// Token: 0x1700024A RID: 586
		// (get) Token: 0x06000799 RID: 1945
		int ConnectionLimit { get; }

		// Token: 0x1700024B RID: 587
		// (get) Token: 0x0600079A RID: 1946
		bool UseNagleAlgorithm { get; }

		// Token: 0x0600079B RID: 1947
		WebProxy GetWebProxy();
	}
}
