﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime
{
	// Token: 0x020000CC RID: 204
	public class SAMLImmutableCredentials : ImmutableCredentials
	{
		// Token: 0x17000285 RID: 645
		// (get) Token: 0x06000848 RID: 2120 RVA: 0x0001466A File Offset: 0x0001286A
		// (set) Token: 0x06000849 RID: 2121 RVA: 0x00014672 File Offset: 0x00012872
		public DateTime Expires { get; private set; }

		// Token: 0x17000286 RID: 646
		// (get) Token: 0x0600084A RID: 2122 RVA: 0x0001467B File Offset: 0x0001287B
		// (set) Token: 0x0600084B RID: 2123 RVA: 0x00014683 File Offset: 0x00012883
		public string Subject { get; private set; }

		// Token: 0x0600084C RID: 2124 RVA: 0x0001468C File Offset: 0x0001288C
		public SAMLImmutableCredentials(string awsAccessKeyId, string awsSecretAccessKey, string token, DateTime expires, string subject) : base(awsAccessKeyId, awsSecretAccessKey, token)
		{
			this.Expires = expires;
			this.Subject = subject;
		}

		// Token: 0x0600084D RID: 2125 RVA: 0x000146A7 File Offset: 0x000128A7
		public SAMLImmutableCredentials(ImmutableCredentials credentials, DateTime expires, string subject) : base(credentials.AccessKey, credentials.SecretKey, credentials.Token)
		{
			this.Expires = expires;
			this.Subject = subject;
		}

		// Token: 0x0600084E RID: 2126 RVA: 0x000146CF File Offset: 0x000128CF
		public override int GetHashCode()
		{
			return Hashing.Hash(new object[]
			{
				base.AccessKey,
				base.SecretKey,
				base.Token,
				this.Subject,
				this.Expires
			});
		}

		// Token: 0x0600084F RID: 2127 RVA: 0x00014710 File Offset: 0x00012910
		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			SAMLImmutableCredentials samlimmutableCredentials = obj as SAMLImmutableCredentials;
			return samlimmutableCredentials != null && base.Equals(obj) && string.Equals(this.Subject, samlimmutableCredentials.Subject, StringComparison.Ordinal) && DateTime.Equals(this.Expires, samlimmutableCredentials.Expires);
		}

		// Token: 0x06000850 RID: 2128 RVA: 0x00014761 File Offset: 0x00012961
		public override ImmutableCredentials Copy()
		{
			return new SAMLImmutableCredentials(base.AccessKey, base.SecretKey, base.Token, this.Expires, this.Subject);
		}

		// Token: 0x06000851 RID: 2129 RVA: 0x00014788 File Offset: 0x00012988
		internal string ToJson()
		{
			return JsonMapper.ToJson(new Dictionary<string, string>
			{
				{
					"AccessKey",
					base.AccessKey
				},
				{
					"SecretKey",
					base.SecretKey
				},
				{
					"Token",
					base.Token
				},
				{
					"Expires",
					this.Expires.ToString("u", CultureInfo.InvariantCulture)
				},
				{
					"Subject",
					this.Subject
				}
			});
		}

		// Token: 0x06000852 RID: 2130 RVA: 0x00014808 File Offset: 0x00012A08
		internal static SAMLImmutableCredentials FromJson(string json)
		{
			try
			{
				JsonData jsonData = JsonMapper.ToObject(json);
				DateTime dateTime = DateTime.Parse((string)jsonData["Expires"], CultureInfo.InvariantCulture).ToUniversalTime();
				if (dateTime <= AWSSDKUtils.CorrectedUtcNow)
				{
					Logger.GetLogger(typeof(SAMLImmutableCredentials)).InfoFormat("Skipping serialized credentials due to expiry.", new object[0]);
					return null;
				}
				string awsAccessKeyId = (string)jsonData["AccessKey"];
				string awsSecretAccessKey = (string)jsonData["SecretKey"];
				string token = (string)jsonData["Token"];
				string subject = (string)jsonData["Subject"];
				return new SAMLImmutableCredentials(awsAccessKeyId, awsSecretAccessKey, token, dateTime, subject);
			}
			catch (Exception exception)
			{
				Logger.GetLogger(typeof(SAMLImmutableCredentials)).Error(exception, "Error during deserialization", new object[0]);
			}
			return null;
		}

		// Token: 0x0400034D RID: 845
		private const string AccessKeyProperty = "AccessKey";

		// Token: 0x0400034E RID: 846
		private const string SecretKeyProperty = "SecretKey";

		// Token: 0x0400034F RID: 847
		private const string TokenProperty = "Token";

		// Token: 0x04000350 RID: 848
		private const string ExpiresProperty = "Expires";

		// Token: 0x04000351 RID: 849
		private const string SubjectProperty = "Subject";
	}
}
