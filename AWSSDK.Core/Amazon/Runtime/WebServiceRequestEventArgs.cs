﻿using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal;

namespace Amazon.Runtime
{
	// Token: 0x020000B4 RID: 180
	public class WebServiceRequestEventArgs : RequestEventArgs
	{
		// Token: 0x060007B1 RID: 1969 RVA: 0x000136B4 File Offset: 0x000118B4
		protected WebServiceRequestEventArgs()
		{
		}

		// Token: 0x17000252 RID: 594
		// (get) Token: 0x060007B2 RID: 1970 RVA: 0x000136BC File Offset: 0x000118BC
		// (set) Token: 0x060007B3 RID: 1971 RVA: 0x000136C4 File Offset: 0x000118C4
		public IDictionary<string, string> Headers { get; protected set; }

		// Token: 0x17000253 RID: 595
		// (get) Token: 0x060007B4 RID: 1972 RVA: 0x000136CD File Offset: 0x000118CD
		// (set) Token: 0x060007B5 RID: 1973 RVA: 0x000136D5 File Offset: 0x000118D5
		[Obsolete("Parameters property has been deprecated in favor of the ParameterCollection property")]
		public IDictionary<string, string> Parameters { get; protected set; }

		// Token: 0x17000254 RID: 596
		// (get) Token: 0x060007B6 RID: 1974 RVA: 0x000136DE File Offset: 0x000118DE
		// (set) Token: 0x060007B7 RID: 1975 RVA: 0x000136E6 File Offset: 0x000118E6
		public ParameterCollection ParameterCollection { get; protected set; }

		// Token: 0x17000255 RID: 597
		// (get) Token: 0x060007B8 RID: 1976 RVA: 0x000136EF File Offset: 0x000118EF
		// (set) Token: 0x060007B9 RID: 1977 RVA: 0x000136F7 File Offset: 0x000118F7
		public string ServiceName { get; protected set; }

		// Token: 0x17000256 RID: 598
		// (get) Token: 0x060007BA RID: 1978 RVA: 0x00013700 File Offset: 0x00011900
		// (set) Token: 0x060007BB RID: 1979 RVA: 0x00013708 File Offset: 0x00011908
		public Uri Endpoint { get; protected set; }

		// Token: 0x17000257 RID: 599
		// (get) Token: 0x060007BC RID: 1980 RVA: 0x00013711 File Offset: 0x00011911
		// (set) Token: 0x060007BD RID: 1981 RVA: 0x00013719 File Offset: 0x00011919
		public AmazonWebServiceRequest Request { get; protected set; }

		// Token: 0x17000258 RID: 600
		// (get) Token: 0x060007BE RID: 1982 RVA: 0x00013722 File Offset: 0x00011922
		[Obsolete("OriginalRequest property has been deprecated in favor of the Request property")]
		public AmazonWebServiceRequest OriginalRequest
		{
			get
			{
				return this.Request;
			}
		}

		// Token: 0x060007BF RID: 1983 RVA: 0x0001372C File Offset: 0x0001192C
		internal static WebServiceRequestEventArgs Create(IRequest request)
		{
			return new WebServiceRequestEventArgs
			{
				Headers = request.Headers,
				Parameters = request.Parameters,
				ParameterCollection = request.ParameterCollection,
				ServiceName = request.ServiceName,
				Request = request.OriginalRequest,
				Endpoint = request.Endpoint
			};
		}
	}
}
