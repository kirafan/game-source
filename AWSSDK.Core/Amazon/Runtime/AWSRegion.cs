﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000A1 RID: 161
	public abstract class AWSRegion
	{
		// Token: 0x17000205 RID: 517
		// (get) Token: 0x06000701 RID: 1793 RVA: 0x00012A18 File Offset: 0x00010C18
		// (set) Token: 0x06000702 RID: 1794 RVA: 0x00012A20 File Offset: 0x00010C20
		public RegionEndpoint Region { get; protected set; }

		// Token: 0x06000703 RID: 1795 RVA: 0x00012A29 File Offset: 0x00010C29
		protected void SetRegionFromName(string regionSystemName)
		{
			this.Region = RegionEndpoint.GetBySystemName(regionSystemName);
		}
	}
}
