﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000B1 RID: 177
	public class PreRequestEventArgs : EventArgs
	{
		// Token: 0x060007A8 RID: 1960 RVA: 0x00003A12 File Offset: 0x00001C12
		protected PreRequestEventArgs()
		{
		}

		// Token: 0x17000251 RID: 593
		// (get) Token: 0x060007A9 RID: 1961 RVA: 0x00013695 File Offset: 0x00011895
		// (set) Token: 0x060007AA RID: 1962 RVA: 0x0001369D File Offset: 0x0001189D
		public AmazonWebServiceRequest Request { get; protected set; }

		// Token: 0x060007AB RID: 1963 RVA: 0x000136A6 File Offset: 0x000118A6
		internal static PreRequestEventArgs Create(AmazonWebServiceRequest request)
		{
			return new PreRequestEventArgs
			{
				Request = request
			};
		}
	}
}
