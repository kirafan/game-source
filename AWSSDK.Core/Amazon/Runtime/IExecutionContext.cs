﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000D3 RID: 211
	public interface IExecutionContext
	{
		// Token: 0x1700029A RID: 666
		// (get) Token: 0x06000876 RID: 2166
		IResponseContext ResponseContext { get; }

		// Token: 0x1700029B RID: 667
		// (get) Token: 0x06000877 RID: 2167
		IRequestContext RequestContext { get; }
	}
}
