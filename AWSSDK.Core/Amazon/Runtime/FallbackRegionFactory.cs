﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime
{
	// Token: 0x020000A3 RID: 163
	public static class FallbackRegionFactory
	{
		// Token: 0x06000706 RID: 1798 RVA: 0x00012AA6 File Offset: 0x00010CA6
		static FallbackRegionFactory()
		{
			FallbackRegionFactory.Reset();
		}

		// Token: 0x17000206 RID: 518
		// (get) Token: 0x06000707 RID: 1799 RVA: 0x00012AB7 File Offset: 0x00010CB7
		// (set) Token: 0x06000708 RID: 1800 RVA: 0x00012ABE File Offset: 0x00010CBE
		private static List<FallbackRegionFactory.RegionGenerator> AllGenerators { get; set; }

		// Token: 0x17000207 RID: 519
		// (get) Token: 0x06000709 RID: 1801 RVA: 0x00012AC6 File Offset: 0x00010CC6
		// (set) Token: 0x0600070A RID: 1802 RVA: 0x00012ACD File Offset: 0x00010CCD
		private static List<FallbackRegionFactory.RegionGenerator> NonMetadataGenerators { get; set; }

		// Token: 0x0600070B RID: 1803 RVA: 0x00012AD8 File Offset: 0x00010CD8
		public static void Reset()
		{
			FallbackRegionFactory.cachedRegion = null;
			List<FallbackRegionFactory.RegionGenerator> list = new List<FallbackRegionFactory.RegionGenerator>();
			list.Add(() => new AppConfigAWSRegion());
			FallbackRegionFactory.AllGenerators = list;
			List<FallbackRegionFactory.RegionGenerator> list2 = new List<FallbackRegionFactory.RegionGenerator>();
			list2.Add(() => new AppConfigAWSRegion());
			FallbackRegionFactory.NonMetadataGenerators = list2;
		}

		// Token: 0x0600070C RID: 1804 RVA: 0x00012B49 File Offset: 0x00010D49
		public static RegionEndpoint GetRegionEndpoint()
		{
			return FallbackRegionFactory.GetRegionEndpoint(true);
		}

		// Token: 0x0600070D RID: 1805 RVA: 0x00012B54 File Offset: 0x00010D54
		public static RegionEndpoint GetRegionEndpoint(bool includeInstanceMetadata)
		{
			object @lock = FallbackRegionFactory._lock;
			RegionEndpoint result;
			lock (@lock)
			{
				if (FallbackRegionFactory.cachedRegion != null)
				{
					result = FallbackRegionFactory.cachedRegion.Region;
				}
				else
				{
					List<Exception> list = new List<Exception>();
					foreach (FallbackRegionFactory.RegionGenerator regionGenerator in ((IEnumerable<FallbackRegionFactory.RegionGenerator>)(includeInstanceMetadata ? FallbackRegionFactory.AllGenerators : FallbackRegionFactory.NonMetadataGenerators)))
					{
						try
						{
							FallbackRegionFactory.cachedRegion = regionGenerator();
						}
						catch (Exception item)
						{
							FallbackRegionFactory.cachedRegion = null;
							list.Add(item);
						}
						if (FallbackRegionFactory.cachedRegion != null)
						{
							break;
						}
					}
					result = ((FallbackRegionFactory.cachedRegion != null) ? FallbackRegionFactory.cachedRegion.Region : null);
				}
			}
			return result;
		}

		// Token: 0x040002C5 RID: 709
		private static object _lock = new object();

		// Token: 0x040002C8 RID: 712
		private static AWSRegion cachedRegion;

		// Token: 0x020001D9 RID: 473
		// (Invoke) Token: 0x06000F44 RID: 3908
		private delegate AWSRegion RegionGenerator();
	}
}
