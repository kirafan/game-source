﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000CA RID: 202
	public class InstanceProfileAWSCredentials : URIBasedRefreshingCredentialHelper
	{
		// Token: 0x1700027F RID: 639
		// (get) Token: 0x06000831 RID: 2097 RVA: 0x000141DA File Offset: 0x000123DA
		// (set) Token: 0x06000832 RID: 2098 RVA: 0x000141E2 File Offset: 0x000123E2
		public string Role { get; set; }

		// Token: 0x06000833 RID: 2099 RVA: 0x000141EC File Offset: 0x000123EC
		protected override RefreshingAWSCredentials.CredentialsRefreshState GenerateNewCredentials()
		{
			RefreshingAWSCredentials.CredentialsRefreshState credentialsRefreshState = null;
			try
			{
				credentialsRefreshState = this.GetRefreshState();
			}
			catch (Exception ex)
			{
				Logger.GetLogger(typeof(InstanceProfileAWSCredentials)).InfoFormat("Error getting credentials from Instance Profile service: {0}", new object[]
				{
					ex
				});
			}
			if (credentialsRefreshState != null)
			{
				this._currentRefreshState = credentialsRefreshState;
			}
			if (this._currentRefreshState == null)
			{
				this._currentRefreshState = this.GetRefreshState();
			}
			return this.GetEarlyRefreshState(this._currentRefreshState);
		}

		// Token: 0x06000834 RID: 2100 RVA: 0x00014264 File Offset: 0x00012464
		public InstanceProfileAWSCredentials(string role)
		{
			this.Role = role;
			base.PreemptExpiryTime = InstanceProfileAWSCredentials._preemptExpiryTime;
		}

		// Token: 0x06000835 RID: 2101 RVA: 0x0001427E File Offset: 0x0001247E
		public InstanceProfileAWSCredentials() : this(InstanceProfileAWSCredentials.GetFirstRole())
		{
		}

		// Token: 0x06000836 RID: 2102 RVA: 0x0001428B File Offset: 0x0001248B
		public static IEnumerable<string> GetAvailableRoles()
		{
			string contents = URIBasedRefreshingCredentialHelper.GetContents(InstanceProfileAWSCredentials.RolesUri);
			if (string.IsNullOrEmpty(contents))
			{
				yield break;
			}
			string[] array = contents.Split(InstanceProfileAWSCredentials.AliasSeparators, StringSplitOptions.RemoveEmptyEntries);
			string[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				string text = array2[i].Trim();
				if (!string.IsNullOrEmpty(text))
				{
					yield return text;
				}
			}
			array2 = null;
			yield break;
		}

		// Token: 0x17000280 RID: 640
		// (get) Token: 0x06000837 RID: 2103 RVA: 0x00014294 File Offset: 0x00012494
		private static Uri RolesUri
		{
			get
			{
				return new Uri(InstanceProfileAWSCredentials.Server + InstanceProfileAWSCredentials.RolesPath);
			}
		}

		// Token: 0x17000281 RID: 641
		// (get) Token: 0x06000838 RID: 2104 RVA: 0x000142AA File Offset: 0x000124AA
		private Uri CurrentRoleUri
		{
			get
			{
				return new Uri(InstanceProfileAWSCredentials.Server + InstanceProfileAWSCredentials.RolesPath + this.Role);
			}
		}

		// Token: 0x17000282 RID: 642
		// (get) Token: 0x06000839 RID: 2105 RVA: 0x000142C6 File Offset: 0x000124C6
		private static Uri InfoUri
		{
			get
			{
				return new Uri(InstanceProfileAWSCredentials.Server + InstanceProfileAWSCredentials.InfoPath);
			}
		}

		// Token: 0x0600083A RID: 2106 RVA: 0x000142DC File Offset: 0x000124DC
		private RefreshingAWSCredentials.CredentialsRefreshState GetEarlyRefreshState(RefreshingAWSCredentials.CredentialsRefreshState state)
		{
			DateTime expiration = AWSSDKUtils.CorrectedUtcNow.ToLocalTime() + InstanceProfileAWSCredentials._refreshAttemptPeriod + base.PreemptExpiryTime;
			if (expiration.ToUniversalTime() > state.Expiration.ToUniversalTime())
			{
				expiration = state.Expiration;
			}
			return new RefreshingAWSCredentials.CredentialsRefreshState
			{
				Credentials = state.Credentials.Copy(),
				Expiration = expiration
			};
		}

		// Token: 0x0600083B RID: 2107 RVA: 0x0001434C File Offset: 0x0001254C
		private RefreshingAWSCredentials.CredentialsRefreshState GetRefreshState()
		{
			URIBasedRefreshingCredentialHelper.SecurityInfo serviceInfo = InstanceProfileAWSCredentials.GetServiceInfo();
			if (!string.IsNullOrEmpty(serviceInfo.Message))
			{
				throw new AmazonServiceException(string.Format(CultureInfo.InvariantCulture, "Unable to retrieve credentials. Message = \"{0}\".", new object[]
				{
					serviceInfo.Message
				}));
			}
			URIBasedRefreshingCredentialHelper.SecurityCredentials roleCredentials = this.GetRoleCredentials();
			return new RefreshingAWSCredentials.CredentialsRefreshState
			{
				Credentials = new ImmutableCredentials(roleCredentials.AccessKeyId, roleCredentials.SecretAccessKey, roleCredentials.Token),
				Expiration = roleCredentials.Expiration
			};
		}

		// Token: 0x0600083C RID: 2108 RVA: 0x000143C5 File Offset: 0x000125C5
		private static URIBasedRefreshingCredentialHelper.SecurityInfo GetServiceInfo()
		{
			return URIBasedRefreshingCredentialHelper.GetObjectFromResponse<URIBasedRefreshingCredentialHelper.SecurityInfo>(InstanceProfileAWSCredentials.InfoUri);
		}

		// Token: 0x0600083D RID: 2109 RVA: 0x000143D1 File Offset: 0x000125D1
		private URIBasedRefreshingCredentialHelper.SecurityCredentials GetRoleCredentials()
		{
			return URIBasedRefreshingCredentialHelper.GetObjectFromResponse<URIBasedRefreshingCredentialHelper.SecurityCredentials>(this.CurrentRoleUri);
		}

		// Token: 0x0600083E RID: 2110 RVA: 0x000143E0 File Offset: 0x000125E0
		private static string GetFirstRole()
		{
			using (IEnumerator<string> enumerator = InstanceProfileAWSCredentials.GetAvailableRoles().GetEnumerator())
			{
				if (enumerator.MoveNext())
				{
					return enumerator.Current;
				}
			}
			throw new InvalidOperationException("No roles found");
		}

		// Token: 0x04000340 RID: 832
		private static TimeSpan _preemptExpiryTime = TimeSpan.FromMinutes(15.0);

		// Token: 0x04000341 RID: 833
		private RefreshingAWSCredentials.CredentialsRefreshState _currentRefreshState;

		// Token: 0x04000342 RID: 834
		private static TimeSpan _refreshAttemptPeriod = TimeSpan.FromHours(1.0);

		// Token: 0x04000344 RID: 836
		private static string[] AliasSeparators = new string[]
		{
			"<br/>"
		};

		// Token: 0x04000345 RID: 837
		private static string Server = "http://169.254.169.254";

		// Token: 0x04000346 RID: 838
		private static string RolesPath = "/latest/meta-data/iam/security-credentials/";

		// Token: 0x04000347 RID: 839
		private static string InfoPath = "/latest/meta-data/iam/info";
	}
}
