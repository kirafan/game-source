﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000A7 RID: 167
	public enum ErrorType
	{
		// Token: 0x040002EF RID: 751
		Sender,
		// Token: 0x040002F0 RID: 752
		Receiver,
		// Token: 0x040002F1 RID: 753
		Unknown
	}
}
