﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000AB RID: 171
	public interface IAmazonService
	{
		// Token: 0x1700022E RID: 558
		// (get) Token: 0x0600077B RID: 1915
		IClientConfig Config { get; }
	}
}
