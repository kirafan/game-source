﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime
{
	// Token: 0x020000B0 RID: 176
	public class StringListParameterValue : ParameterValue
	{
		// Token: 0x17000250 RID: 592
		// (get) Token: 0x060007A4 RID: 1956 RVA: 0x00013675 File Offset: 0x00011875
		// (set) Token: 0x060007A5 RID: 1957 RVA: 0x0001367D File Offset: 0x0001187D
		public List<string> Value { get; set; }

		// Token: 0x060007A6 RID: 1958 RVA: 0x00013686 File Offset: 0x00011886
		public StringListParameterValue(List<string> values)
		{
			this.Value = values;
		}

		// Token: 0x060007A7 RID: 1959 RVA: 0x0001366D File Offset: 0x0001186D
		internal StringListParameterValue()
		{
		}
	}
}
