﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Auth;
using Amazon.Runtime.Internal.Transform;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x0200009C RID: 156
	public abstract class AmazonServiceClient : IDisposable
	{
		// Token: 0x170001F1 RID: 497
		// (get) Token: 0x060006A5 RID: 1701 RVA: 0x00011BDE File Offset: 0x0000FDDE
		// (set) Token: 0x060006A6 RID: 1702 RVA: 0x00011BE6 File Offset: 0x0000FDE6
		protected RuntimePipeline RuntimePipeline { get; set; }

		// Token: 0x170001F2 RID: 498
		// (get) Token: 0x060006A7 RID: 1703 RVA: 0x00011BEF File Offset: 0x0000FDEF
		// (set) Token: 0x060006A8 RID: 1704 RVA: 0x00011BF7 File Offset: 0x0000FDF7
		protected internal AWSCredentials Credentials { get; private set; }

		// Token: 0x170001F3 RID: 499
		// (get) Token: 0x060006A9 RID: 1705 RVA: 0x00011C00 File Offset: 0x0000FE00
		// (set) Token: 0x060006AA RID: 1706 RVA: 0x00011C08 File Offset: 0x0000FE08
		public IClientConfig Config { get; private set; }

		// Token: 0x170001F4 RID: 500
		// (get) Token: 0x060006AB RID: 1707 RVA: 0x00011C11 File Offset: 0x0000FE11
		protected virtual bool SupportResponseLogging
		{
			get
			{
				return true;
			}
		}

		// Token: 0x1400001B RID: 27
		// (add) Token: 0x060006AC RID: 1708 RVA: 0x00011C14 File Offset: 0x0000FE14
		// (remove) Token: 0x060006AD RID: 1709 RVA: 0x00011C5C File Offset: 0x0000FE5C
		internal event PreRequestEventHandler BeforeMarshallingEvent
		{
			add
			{
				lock (this)
				{
					this.mBeforeMarshallingEvent = (PreRequestEventHandler)Delegate.Combine(this.mBeforeMarshallingEvent, value);
				}
			}
			remove
			{
				lock (this)
				{
					this.mBeforeMarshallingEvent = (PreRequestEventHandler)Delegate.Remove(this.mBeforeMarshallingEvent, value);
				}
			}
		}

		// Token: 0x1400001C RID: 28
		// (add) Token: 0x060006AE RID: 1710 RVA: 0x00011CA4 File Offset: 0x0000FEA4
		// (remove) Token: 0x060006AF RID: 1711 RVA: 0x00011CEC File Offset: 0x0000FEEC
		public event RequestEventHandler BeforeRequestEvent
		{
			add
			{
				lock (this)
				{
					this.mBeforeRequestEvent = (RequestEventHandler)Delegate.Combine(this.mBeforeRequestEvent, value);
				}
			}
			remove
			{
				lock (this)
				{
					this.mBeforeRequestEvent = (RequestEventHandler)Delegate.Remove(this.mBeforeRequestEvent, value);
				}
			}
		}

		// Token: 0x1400001D RID: 29
		// (add) Token: 0x060006B0 RID: 1712 RVA: 0x00011D34 File Offset: 0x0000FF34
		// (remove) Token: 0x060006B1 RID: 1713 RVA: 0x00011D7C File Offset: 0x0000FF7C
		public event ResponseEventHandler AfterResponseEvent
		{
			add
			{
				lock (this)
				{
					this.mAfterResponseEvent = (ResponseEventHandler)Delegate.Combine(this.mAfterResponseEvent, value);
				}
			}
			remove
			{
				lock (this)
				{
					this.mAfterResponseEvent = (ResponseEventHandler)Delegate.Remove(this.mAfterResponseEvent, value);
				}
			}
		}

		// Token: 0x1400001E RID: 30
		// (add) Token: 0x060006B2 RID: 1714 RVA: 0x00011DC4 File Offset: 0x0000FFC4
		// (remove) Token: 0x060006B3 RID: 1715 RVA: 0x00011E0C File Offset: 0x0001000C
		public event ExceptionEventHandler ExceptionEvent
		{
			add
			{
				lock (this)
				{
					this.mExceptionEvent = (ExceptionEventHandler)Delegate.Combine(this.mExceptionEvent, value);
				}
			}
			remove
			{
				lock (this)
				{
					this.mExceptionEvent = (ExceptionEventHandler)Delegate.Remove(this.mExceptionEvent, value);
				}
			}
		}

		// Token: 0x060006B4 RID: 1716 RVA: 0x00011E54 File Offset: 0x00010054
		protected AmazonServiceClient(AWSCredentials credentials, ClientConfig config)
		{
			if (config.DisableLogging)
			{
				this._logger = Logger.EmptyLogger;
			}
			else
			{
				this._logger = Logger.GetLogger(base.GetType());
			}
			config.Validate();
			this.Config = config;
			this.Credentials = credentials;
			this.Signer = this.CreateSigner();
			this.Initialize();
			this.BuildRuntimePipeline();
		}

		// Token: 0x170001F5 RID: 501
		// (get) Token: 0x060006B5 RID: 1717 RVA: 0x00011EB9 File Offset: 0x000100B9
		// (set) Token: 0x060006B6 RID: 1718 RVA: 0x00011EC1 File Offset: 0x000100C1
		private protected AbstractAWSSigner Signer { protected get; private set; }

		// Token: 0x060006B7 RID: 1719 RVA: 0x00011ECA File Offset: 0x000100CA
		protected AmazonServiceClient(string awsAccessKeyId, string awsSecretAccessKey, string awsSessionToken, ClientConfig config) : this(new SessionAWSCredentials(awsAccessKeyId, awsSecretAccessKey, awsSessionToken), config)
		{
		}

		// Token: 0x060006B8 RID: 1720 RVA: 0x00011EDC File Offset: 0x000100DC
		protected AmazonServiceClient(string awsAccessKeyId, string awsSecretAccessKey, ClientConfig config) : this(new BasicAWSCredentials(awsAccessKeyId, awsSecretAccessKey), config)
		{
		}

		// Token: 0x060006B9 RID: 1721 RVA: 0x00005805 File Offset: 0x00003A05
		protected virtual void Initialize()
		{
		}

		// Token: 0x060006BA RID: 1722 RVA: 0x00011EEC File Offset: 0x000100EC
		protected TResponse Invoke<TRequest, TResponse>(TRequest request, IMarshaller<IRequest, AmazonWebServiceRequest> marshaller, ResponseUnmarshaller unmarshaller) where TRequest : AmazonWebServiceRequest where TResponse : AmazonWebServiceResponse
		{
			this.ThrowIfDisposed();
			ExecutionContext executionContext = new ExecutionContext(new RequestContext(this.Config.LogMetrics, this.Signer)
			{
				ClientConfig = this.Config,
				Marshaller = marshaller,
				OriginalRequest = request,
				Unmarshaller = unmarshaller,
				IsAsync = false
			}, new ResponseContext());
			return (TResponse)((object)this.RuntimePipeline.InvokeSync(executionContext).Response);
		}

		// Token: 0x060006BB RID: 1723 RVA: 0x00011F64 File Offset: 0x00010164
		protected IAsyncResult BeginInvoke<TRequest>(TRequest request, IMarshaller<IRequest, AmazonWebServiceRequest> marshaller, ResponseUnmarshaller unmarshaller, AsyncOptions asyncOptions, Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper) where TRequest : AmazonWebServiceRequest
		{
			this.ThrowIfDisposed();
			asyncOptions = (asyncOptions ?? new AsyncOptions());
			AsyncExecutionContext executionContext = new AsyncExecutionContext(new AsyncRequestContext(this.Config.LogMetrics, this.Signer)
			{
				ClientConfig = this.Config,
				Marshaller = marshaller,
				OriginalRequest = request,
				Unmarshaller = unmarshaller,
				Action = callbackHelper,
				AsyncOptions = asyncOptions,
				IsAsync = true
			}, new AsyncResponseContext());
			return this.RuntimePipeline.InvokeAsync(executionContext);
		}

		// Token: 0x060006BC RID: 1724 RVA: 0x00011FF0 File Offset: 0x000101F0
		protected IAsyncResult BeginInvoke<TRequest>(TRequest request, IMarshaller<IRequest, AmazonWebServiceRequest> marshaller, ResponseUnmarshaller unmarshaller, AsyncCallback callback, object state) where TRequest : AmazonWebServiceRequest
		{
			this.ThrowIfDisposed();
			AsyncExecutionContext executionContext = new AsyncExecutionContext(new AsyncRequestContext(this.Config.LogMetrics, this.Signer)
			{
				ClientConfig = this.Config,
				Marshaller = marshaller,
				OriginalRequest = request,
				Unmarshaller = unmarshaller,
				Callback = callback,
				State = state,
				IsAsync = true
			}, new AsyncResponseContext());
			return this.RuntimePipeline.InvokeAsync(executionContext);
		}

		// Token: 0x060006BD RID: 1725 RVA: 0x00012070 File Offset: 0x00010270
		protected static TResponse EndInvoke<TResponse>(IAsyncResult result) where TResponse : AmazonWebServiceResponse
		{
			if (result == null)
			{
				throw new ArgumentNullException("result", "Parameter result cannot be null.");
			}
			RuntimeAsyncResult runtimeAsyncResult = result as RuntimeAsyncResult;
			if (runtimeAsyncResult == null)
			{
				throw new ArgumentOutOfRangeException("result", "Parameter result is not of type RuntimeAsyncResult.");
			}
			TResponse result2;
			using (runtimeAsyncResult)
			{
				if (!runtimeAsyncResult.IsCompleted)
				{
					runtimeAsyncResult.AsyncWaitHandle.WaitOne();
				}
				if (runtimeAsyncResult.Exception != null)
				{
					AWSSDKUtils.PreserveStackTrace(runtimeAsyncResult.Exception);
					throw runtimeAsyncResult.Exception;
				}
				result2 = (TResponse)((object)runtimeAsyncResult.Response);
			}
			return result2;
		}

		// Token: 0x060006BE RID: 1726 RVA: 0x00012104 File Offset: 0x00010304
		protected void ProcessPreRequestHandlers(IExecutionContext executionContext)
		{
			if (this.mBeforeMarshallingEvent == null)
			{
				return;
			}
			PreRequestEventArgs e = PreRequestEventArgs.Create(executionContext.RequestContext.OriginalRequest);
			this.mBeforeMarshallingEvent(this, e);
		}

		// Token: 0x060006BF RID: 1727 RVA: 0x00012138 File Offset: 0x00010338
		protected void ProcessRequestHandlers(IExecutionContext executionContext)
		{
			IRequest request = executionContext.RequestContext.Request;
			WebServiceRequestEventArgs webServiceRequestEventArgs = WebServiceRequestEventArgs.Create(request);
			if (request.OriginalRequest != null)
			{
				request.OriginalRequest.FireBeforeRequestEvent(this, webServiceRequestEventArgs);
			}
			if (this.mBeforeRequestEvent != null)
			{
				this.mBeforeRequestEvent(this, webServiceRequestEventArgs);
			}
		}

		// Token: 0x060006C0 RID: 1728 RVA: 0x00012184 File Offset: 0x00010384
		protected void ProcessResponseHandlers(IExecutionContext executionContext)
		{
			if (this.mAfterResponseEvent == null)
			{
				return;
			}
			WebServiceResponseEventArgs e = WebServiceResponseEventArgs.Create(executionContext.ResponseContext.Response, executionContext.RequestContext.Request, executionContext.ResponseContext.HttpResponse);
			this.mAfterResponseEvent(this, e);
		}

		// Token: 0x060006C1 RID: 1729 RVA: 0x000121D0 File Offset: 0x000103D0
		protected virtual void ProcessExceptionHandlers(IExecutionContext executionContext, Exception exception)
		{
			if (this.mExceptionEvent == null)
			{
				return;
			}
			WebServiceExceptionEventArgs e = WebServiceExceptionEventArgs.Create(exception, executionContext.RequestContext.Request);
			this.mExceptionEvent(this, e);
		}

		// Token: 0x060006C2 RID: 1730 RVA: 0x00012205 File Offset: 0x00010405
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		// Token: 0x060006C3 RID: 1731 RVA: 0x00012214 File Offset: 0x00010414
		protected virtual void Dispose(bool disposing)
		{
			if (this._disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.RuntimePipeline != null)
				{
					this.RuntimePipeline.Dispose();
				}
				this._disposed = true;
			}
		}

		// Token: 0x060006C4 RID: 1732 RVA: 0x0001223C File Offset: 0x0001043C
		private void ThrowIfDisposed()
		{
			if (this._disposed)
			{
				throw new ObjectDisposedException(base.GetType().FullName);
			}
		}

		// Token: 0x060006C5 RID: 1733
		protected abstract AbstractAWSSigner CreateSigner();

		// Token: 0x060006C6 RID: 1734 RVA: 0x00005805 File Offset: 0x00003A05
		protected virtual void CustomizeRuntimePipeline(RuntimePipeline pipeline)
		{
		}

		// Token: 0x060006C7 RID: 1735 RVA: 0x00012258 File Offset: 0x00010458
		private void BuildRuntimePipeline()
		{
			HttpHandler<string> item;
			if (AWSConfigs.HttpClient == AWSConfigs.HttpClientOption.UnityWWW)
			{
				item = new HttpHandler<string>(new UnityWwwRequestFactory(), this);
			}
			else
			{
				item = new HttpHandler<string>(new UnityWebRequestFactory(), this);
			}
			CallbackHandler callbackHandler = new CallbackHandler();
			callbackHandler.OnPreInvoke = new Action<IExecutionContext>(this.ProcessPreRequestHandlers);
			CallbackHandler callbackHandler2 = new CallbackHandler();
			callbackHandler2.OnPreInvoke = new Action<IExecutionContext>(this.ProcessRequestHandlers);
			CallbackHandler callbackHandler3 = new CallbackHandler();
			callbackHandler3.OnPostInvoke = new Action<IExecutionContext>(this.ProcessResponseHandlers);
			ErrorCallbackHandler errorCallbackHandler = new ErrorCallbackHandler();
			errorCallbackHandler.OnError = new Action<IExecutionContext, Exception>(this.ProcessExceptionHandlers);
			this.RuntimePipeline = new RuntimePipeline(new List<IPipelineHandler>
			{
				item,
				new Unmarshaller(this.SupportResponseLogging),
				new ErrorHandler(this._logger),
				callbackHandler3,
				new Signer(),
				new CredentialsRetriever(this.Credentials),
				new RetryHandler(new DefaultRetryPolicy(this.Config)),
				callbackHandler2,
				new EndpointResolver(),
				new Marshaller(),
				callbackHandler,
				errorCallbackHandler,
				new MetricsHandler(),
				new ThreadPoolExecutionHandler(10)
			}, this._logger);
			this.CustomizeRuntimePipeline(this.RuntimePipeline);
		}

		// Token: 0x060006C8 RID: 1736 RVA: 0x000123B4 File Offset: 0x000105B4
		public static Uri ComposeUrl(IRequest iRequest)
		{
			Uri endpoint = iRequest.Endpoint;
			string text = iRequest.ResourcePath;
			if (text == null)
			{
				text = string.Empty;
			}
			else if (text.StartsWith("/", StringComparison.Ordinal))
			{
				text = text.Substring(1);
			}
			string arg = "?";
			StringBuilder stringBuilder = new StringBuilder();
			if (iRequest.SubResources.Count > 0)
			{
				foreach (KeyValuePair<string, string> keyValuePair in iRequest.SubResources)
				{
					stringBuilder.AppendFormat("{0}{1}", arg, keyValuePair.Key);
					if (keyValuePair.Value != null)
					{
						stringBuilder.AppendFormat("={0}", keyValuePair.Value);
					}
					arg = "&";
				}
			}
			if (iRequest.UseQueryString && iRequest.Parameters.Count > 0)
			{
				string parametersAsString = AWSSDKUtils.GetParametersAsString(iRequest);
				stringBuilder.AppendFormat("{0}{1}", arg, parametersAsString);
			}
			if (AWSSDKUtils.HasBidiControlCharacters(text))
			{
				throw new AmazonClientException(string.Format(CultureInfo.InvariantCulture, "Target resource path [{0}] has bidirectional characters, which are not supportedby System.Uri and thus cannot be handled by the .NET SDK.", new object[]
				{
					text
				}));
			}
			string str = AWSSDKUtils.UrlEncode(text, true) + stringBuilder;
			Uri uri = new Uri(endpoint.AbsoluteUri + str);
			AmazonServiceClient.DontUnescapePathDotsAndSlashes(uri);
			return uri;
		}

		// Token: 0x060006C9 RID: 1737 RVA: 0x00005805 File Offset: 0x00003A05
		private static void DontUnescapePathDotsAndSlashes(Uri uri)
		{
		}

		// Token: 0x060006CA RID: 1738 RVA: 0x000124FC File Offset: 0x000106FC
		internal C CloneConfig<C>() where C : ClientConfig, new()
		{
			C c = Activator.CreateInstance<C>();
			this.CloneConfig(c);
			return c;
		}

		// Token: 0x060006CB RID: 1739 RVA: 0x0001251C File Offset: 0x0001071C
		internal void CloneConfig(ClientConfig newConfig)
		{
			if (!string.IsNullOrEmpty(this.Config.ServiceURL))
			{
				RegionEndpoint bySystemName = RegionEndpoint.GetBySystemName(AWSSDKUtils.DetermineRegion(this.Config.ServiceURL));
				newConfig.RegionEndpoint = bySystemName;
			}
			else
			{
				newConfig.RegionEndpoint = this.Config.RegionEndpoint;
			}
			newConfig.UseHttp = this.Config.UseHttp;
			newConfig.ProxyCredentials = this.Config.ProxyCredentials;
		}

		// Token: 0x040002AD RID: 685
		private bool _disposed;

		// Token: 0x040002AE RID: 686
		private Logger _logger;

		// Token: 0x040002B2 RID: 690
		private PreRequestEventHandler mBeforeMarshallingEvent;

		// Token: 0x040002B3 RID: 691
		private RequestEventHandler mBeforeRequestEvent;

		// Token: 0x040002B4 RID: 692
		private ResponseEventHandler mAfterResponseEvent;

		// Token: 0x040002B5 RID: 693
		private ExceptionEventHandler mExceptionEvent;
	}
}
