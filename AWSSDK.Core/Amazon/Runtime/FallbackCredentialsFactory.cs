﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000C8 RID: 200
	public static class FallbackCredentialsFactory
	{
		// Token: 0x0600081E RID: 2078 RVA: 0x00013E94 File Offset: 0x00012094
		static FallbackCredentialsFactory()
		{
			FallbackCredentialsFactory.Reset();
		}

		// Token: 0x1700027A RID: 634
		// (get) Token: 0x0600081F RID: 2079 RVA: 0x00013E9B File Offset: 0x0001209B
		// (set) Token: 0x06000820 RID: 2080 RVA: 0x00013EA2 File Offset: 0x000120A2
		public static List<FallbackCredentialsFactory.CredentialsGenerator> CredentialsGenerators { get; set; }

		// Token: 0x06000821 RID: 2081 RVA: 0x00013EAA File Offset: 0x000120AA
		public static void Reset()
		{
			FallbackCredentialsFactory.cachedCredentials = null;
			FallbackCredentialsFactory.CredentialsGenerators = new List<FallbackCredentialsFactory.CredentialsGenerator>
			{
				new FallbackCredentialsFactory.CredentialsGenerator(FallbackCredentialsFactory.ECSEC2CredentialsWrapper)
			};
		}

		// Token: 0x06000822 RID: 2082 RVA: 0x00013ED0 File Offset: 0x000120D0
		private static AWSCredentials ECSEC2CredentialsWrapper()
		{
			try
			{
				if (!string.IsNullOrEmpty(Environment.GetEnvironmentVariable("AWS_CONTAINER_CREDENTIALS_RELATIVE_URI")))
				{
					return new ECSTaskCredentials();
				}
			}
			catch (SecurityException exception)
			{
				Logger.GetLogger(typeof(ECSTaskCredentials)).Error(exception, "Failed to access environment variable {0}", new object[]
				{
					"AWS_CONTAINER_CREDENTIALS_RELATIVE_URI"
				});
			}
			return new InstanceProfileAWSCredentials();
		}

		// Token: 0x06000823 RID: 2083 RVA: 0x00013F3C File Offset: 0x0001213C
		public static AWSCredentials GetCredentials()
		{
			return FallbackCredentialsFactory.GetCredentials(false);
		}

		// Token: 0x06000824 RID: 2084 RVA: 0x00013F44 File Offset: 0x00012144
		public static AWSCredentials GetCredentials(bool fallbackToAnonymous)
		{
			if (FallbackCredentialsFactory.cachedCredentials != null)
			{
				return FallbackCredentialsFactory.cachedCredentials;
			}
			List<Exception> list = new List<Exception>();
			foreach (FallbackCredentialsFactory.CredentialsGenerator credentialsGenerator in FallbackCredentialsFactory.CredentialsGenerators)
			{
				try
				{
					FallbackCredentialsFactory.cachedCredentials = credentialsGenerator();
				}
				catch (Exception item)
				{
					FallbackCredentialsFactory.cachedCredentials = null;
					list.Add(item);
				}
				if (FallbackCredentialsFactory.cachedCredentials != null)
				{
					break;
				}
			}
			if (FallbackCredentialsFactory.cachedCredentials == null)
			{
				if (fallbackToAnonymous)
				{
					return new AnonymousAWSCredentials();
				}
				using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
				{
					stringWriter.WriteLine("Unable to find credentials");
					stringWriter.WriteLine();
					for (int i = 0; i < list.Count; i++)
					{
						Exception ex = list[i];
						stringWriter.WriteLine("Exception {0} of {1}:", i + 1, list.Count);
						stringWriter.WriteLine(ex.ToString());
						stringWriter.WriteLine();
					}
					throw new AmazonServiceException(stringWriter.ToString());
				}
			}
			return FallbackCredentialsFactory.cachedCredentials;
		}

		// Token: 0x0400033C RID: 828
		private static AWSCredentials cachedCredentials;

		// Token: 0x020001DB RID: 475
		// (Invoke) Token: 0x06000F4C RID: 3916
		public delegate AWSCredentials CredentialsGenerator();
	}
}
