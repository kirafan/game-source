﻿using System;
using System.Runtime.Serialization;

namespace Amazon.Runtime
{
	// Token: 0x0200009A RID: 154
	[Serializable]
	public class AmazonClientException : Exception
	{
		// Token: 0x0600069B RID: 1691 RVA: 0x00008CC5 File Offset: 0x00006EC5
		public AmazonClientException(string message) : base(message)
		{
		}

		// Token: 0x0600069C RID: 1692 RVA: 0x00008CCE File Offset: 0x00006ECE
		public AmazonClientException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600069D RID: 1693 RVA: 0x00011B48 File Offset: 0x0000FD48
		protected AmazonClientException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
