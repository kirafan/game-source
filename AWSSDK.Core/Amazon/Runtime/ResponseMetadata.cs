﻿using System;
using System.Collections.Generic;

namespace Amazon.Runtime
{
	// Token: 0x020000BE RID: 190
	[Serializable]
	public class ResponseMetadata
	{
		// Token: 0x17000268 RID: 616
		// (get) Token: 0x060007E6 RID: 2022 RVA: 0x000138B9 File Offset: 0x00011AB9
		// (set) Token: 0x060007E7 RID: 2023 RVA: 0x000138C1 File Offset: 0x00011AC1
		public string RequestId
		{
			get
			{
				return this.requestIdField;
			}
			set
			{
				this.requestIdField = value;
			}
		}

		// Token: 0x17000269 RID: 617
		// (get) Token: 0x060007E8 RID: 2024 RVA: 0x000138CA File Offset: 0x00011ACA
		public IDictionary<string, string> Metadata
		{
			get
			{
				if (this._metadata == null)
				{
					this._metadata = new Dictionary<string, string>();
				}
				return this._metadata;
			}
		}

		// Token: 0x04000324 RID: 804
		private string requestIdField;

		// Token: 0x04000325 RID: 805
		private IDictionary<string, string> _metadata;
	}
}
