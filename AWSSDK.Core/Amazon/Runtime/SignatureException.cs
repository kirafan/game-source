﻿using System;
using System.Runtime.Serialization;
using Amazon.Runtime.Internal.Auth;

namespace Amazon.Runtime
{
	// Token: 0x020000BF RID: 191
	[Serializable]
	public class SignatureException : SignatureException
	{
		// Token: 0x060007EA RID: 2026 RVA: 0x000138E5 File Offset: 0x00011AE5
		public SignatureException(string message) : base(message)
		{
		}

		// Token: 0x060007EB RID: 2027 RVA: 0x000138EE File Offset: 0x00011AEE
		public SignatureException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060007EC RID: 2028 RVA: 0x000138F8 File Offset: 0x00011AF8
		protected SignatureException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
