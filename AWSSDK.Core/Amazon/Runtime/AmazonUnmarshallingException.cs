﻿using System;
using System.Runtime.Serialization;
using System.Security;
using System.Text;

namespace Amazon.Runtime
{
	// Token: 0x0200009E RID: 158
	[Serializable]
	public class AmazonUnmarshallingException : AmazonServiceException
	{
		// Token: 0x060006DE RID: 1758 RVA: 0x00012762 File Offset: 0x00010962
		public AmazonUnmarshallingException(string requestId, string lastKnownLocation, Exception innerException) : base("Error unmarshalling response back from AWS.", innerException)
		{
			base.RequestId = requestId;
			this.LastKnownLocation = lastKnownLocation;
		}

		// Token: 0x060006DF RID: 1759 RVA: 0x0001277E File Offset: 0x0001097E
		public AmazonUnmarshallingException(string requestId, string lastKnownLocation, string responseBody, Exception innerException) : base("Error unmarshalling response back from AWS.", innerException)
		{
			base.RequestId = requestId;
			this.LastKnownLocation = lastKnownLocation;
			this.ResponseBody = responseBody;
		}

		// Token: 0x060006E0 RID: 1760 RVA: 0x000127A2 File Offset: 0x000109A2
		public AmazonUnmarshallingException(string requestId, string lastKnownLocation, string responseBody, string message, Exception innerException) : base("Error unmarshalling response back from AWS. " + message, innerException)
		{
			base.RequestId = requestId;
			this.LastKnownLocation = lastKnownLocation;
			this.ResponseBody = responseBody;
		}

		// Token: 0x170001FA RID: 506
		// (get) Token: 0x060006E1 RID: 1761 RVA: 0x000127CD File Offset: 0x000109CD
		// (set) Token: 0x060006E2 RID: 1762 RVA: 0x000127D5 File Offset: 0x000109D5
		public string LastKnownLocation { get; private set; }

		// Token: 0x170001FB RID: 507
		// (get) Token: 0x060006E3 RID: 1763 RVA: 0x000127DE File Offset: 0x000109DE
		// (set) Token: 0x060006E4 RID: 1764 RVA: 0x000127E6 File Offset: 0x000109E6
		public string ResponseBody { get; private set; }

		// Token: 0x170001FC RID: 508
		// (get) Token: 0x060006E5 RID: 1765 RVA: 0x000127F0 File Offset: 0x000109F0
		public override string Message
		{
			get
			{
				StringBuilder stringBuilder = new StringBuilder();
				AmazonUnmarshallingException.AppendFormat(stringBuilder, "Request ID: {0}", base.RequestId);
				AmazonUnmarshallingException.AppendFormat(stringBuilder, "Response Body: {0}", this.ResponseBody);
				AmazonUnmarshallingException.AppendFormat(stringBuilder, "Last Parsed Path: {0}", this.LastKnownLocation);
				string str = stringBuilder.ToString();
				return base.Message + " " + str;
			}
		}

		// Token: 0x060006E6 RID: 1766 RVA: 0x0001284C File Offset: 0x00010A4C
		private static void AppendFormat(StringBuilder sb, string format, string value)
		{
			if (string.IsNullOrEmpty(value))
			{
				return;
			}
			if (sb.Length > 0)
			{
				sb.Append(", ");
			}
			sb.AppendFormat(format, value);
		}

		// Token: 0x060006E7 RID: 1767 RVA: 0x00012875 File Offset: 0x00010A75
		protected AmazonUnmarshallingException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			if (info != null)
			{
				this.LastKnownLocation = info.GetString("LastKnownLocation");
				this.ResponseBody = info.GetString("ResponseBody");
			}
		}

		// Token: 0x060006E8 RID: 1768 RVA: 0x000128A4 File Offset: 0x00010AA4
		[SecurityCritical]
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			if (info != null)
			{
				info.AddValue("LastKnownLocation", this.LastKnownLocation);
				info.AddValue("ResponseBody", this.ResponseBody);
			}
		}
	}
}
