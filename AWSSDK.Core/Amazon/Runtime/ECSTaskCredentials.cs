﻿using System;
using System.Globalization;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000C7 RID: 199
	public class ECSTaskCredentials : URIBasedRefreshingCredentialHelper
	{
		// Token: 0x0600081B RID: 2075 RVA: 0x00013D90 File Offset: 0x00011F90
		public ECSTaskCredentials()
		{
			this.Uri = Environment.GetEnvironmentVariable("AWS_CONTAINER_CREDENTIALS_RELATIVE_URI");
			this.Server = "http://169.254.170.2";
		}

		// Token: 0x0600081C RID: 2076 RVA: 0x00013DB4 File Offset: 0x00011FB4
		protected override RefreshingAWSCredentials.CredentialsRefreshState GenerateNewCredentials()
		{
			URIBasedRefreshingCredentialHelper.SecurityCredentials securityCredentials = null;
			Uri uri = new Uri(this.Server + this.Uri);
			JitteredDelay jitteredDelay = new JitteredDelay(new TimeSpan(0, 0, 0, 0, 200), new TimeSpan(0, 0, 0, 0, 50));
			int num = 1;
			for (;;)
			{
				try
				{
					securityCredentials = URIBasedRefreshingCredentialHelper.GetObjectFromResponse<URIBasedRefreshingCredentialHelper.SecurityCredentials>(uri);
					if (securityCredentials != null)
					{
						break;
					}
				}
				catch (Exception ex)
				{
					if (num == ECSTaskCredentials.MaxRetries)
					{
						throw new AmazonServiceException(string.Format(CultureInfo.InvariantCulture, "Unable to retrieve credentials. Message = \"{0}\".", new object[]
						{
							ex.Message
						}));
					}
				}
				AWSSDKUtils.Sleep(jitteredDelay.Next());
				num++;
			}
			return new RefreshingAWSCredentials.CredentialsRefreshState
			{
				Credentials = new ImmutableCredentials(securityCredentials.AccessKeyId, securityCredentials.SecretAccessKey, securityCredentials.Token),
				Expiration = securityCredentials.Expiration
			};
		}

		// Token: 0x04000336 RID: 822
		public const string ContainerCredentialsURIEnvVariable = "AWS_CONTAINER_CREDENTIALS_RELATIVE_URI";

		// Token: 0x04000337 RID: 823
		public const string EndpointAddress = "http://169.254.170.2";

		// Token: 0x04000338 RID: 824
		private string Uri;

		// Token: 0x04000339 RID: 825
		private string Server;

		// Token: 0x0400033A RID: 826
		private static int MaxRetries = 5;
	}
}
