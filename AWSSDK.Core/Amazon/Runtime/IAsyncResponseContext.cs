﻿using System;
using Amazon.Runtime.Internal;

namespace Amazon.Runtime
{
	// Token: 0x020000D2 RID: 210
	public interface IAsyncResponseContext : IResponseContext
	{
		// Token: 0x17000299 RID: 665
		// (get) Token: 0x06000874 RID: 2164
		// (set) Token: 0x06000875 RID: 2165
		RuntimeAsyncResult AsyncResult { get; set; }
	}
}
