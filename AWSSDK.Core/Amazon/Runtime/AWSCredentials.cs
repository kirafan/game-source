﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000C5 RID: 197
	public abstract class AWSCredentials
	{
		// Token: 0x06000814 RID: 2068
		public abstract ImmutableCredentials GetCredentials();

		// Token: 0x06000815 RID: 2069 RVA: 0x00005805 File Offset: 0x00003A05
		protected virtual void Validate()
		{
		}
	}
}
