﻿using System;
using System.Collections.Generic;
using Amazon.Runtime.Internal;

namespace Amazon.Runtime
{
	// Token: 0x020000AA RID: 170
	public class WebServiceExceptionEventArgs : ExceptionEventArgs
	{
		// Token: 0x0600076D RID: 1901 RVA: 0x00013575 File Offset: 0x00011775
		protected WebServiceExceptionEventArgs()
		{
		}

		// Token: 0x17000228 RID: 552
		// (get) Token: 0x0600076E RID: 1902 RVA: 0x0001357D File Offset: 0x0001177D
		// (set) Token: 0x0600076F RID: 1903 RVA: 0x00013585 File Offset: 0x00011785
		public IDictionary<string, string> Headers { get; protected set; }

		// Token: 0x17000229 RID: 553
		// (get) Token: 0x06000770 RID: 1904 RVA: 0x0001358E File Offset: 0x0001178E
		// (set) Token: 0x06000771 RID: 1905 RVA: 0x00013596 File Offset: 0x00011796
		public IDictionary<string, string> Parameters { get; protected set; }

		// Token: 0x1700022A RID: 554
		// (get) Token: 0x06000772 RID: 1906 RVA: 0x0001359F File Offset: 0x0001179F
		// (set) Token: 0x06000773 RID: 1907 RVA: 0x000135A7 File Offset: 0x000117A7
		public string ServiceName { get; protected set; }

		// Token: 0x1700022B RID: 555
		// (get) Token: 0x06000774 RID: 1908 RVA: 0x000135B0 File Offset: 0x000117B0
		// (set) Token: 0x06000775 RID: 1909 RVA: 0x000135B8 File Offset: 0x000117B8
		public Uri Endpoint { get; protected set; }

		// Token: 0x1700022C RID: 556
		// (get) Token: 0x06000776 RID: 1910 RVA: 0x000135C1 File Offset: 0x000117C1
		// (set) Token: 0x06000777 RID: 1911 RVA: 0x000135C9 File Offset: 0x000117C9
		public AmazonWebServiceRequest Request { get; protected set; }

		// Token: 0x1700022D RID: 557
		// (get) Token: 0x06000778 RID: 1912 RVA: 0x000135D2 File Offset: 0x000117D2
		// (set) Token: 0x06000779 RID: 1913 RVA: 0x000135DA File Offset: 0x000117DA
		public Exception Exception { get; protected set; }

		// Token: 0x0600077A RID: 1914 RVA: 0x000135E4 File Offset: 0x000117E4
		internal static WebServiceExceptionEventArgs Create(Exception exception, IRequest request)
		{
			WebServiceExceptionEventArgs result;
			if (request == null)
			{
				result = new WebServiceExceptionEventArgs
				{
					Exception = exception
				};
			}
			else
			{
				result = new WebServiceExceptionEventArgs
				{
					Headers = request.Headers,
					Parameters = request.Parameters,
					ServiceName = request.ServiceName,
					Request = request.OriginalRequest,
					Endpoint = request.Endpoint,
					Exception = exception
				};
			}
			return result;
		}
	}
}
