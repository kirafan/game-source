﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000A6 RID: 166
	public enum SigningAlgorithm
	{
		// Token: 0x040002EC RID: 748
		HmacSHA1,
		// Token: 0x040002ED RID: 749
		HmacSHA256
	}
}
