﻿using System;
using System.Globalization;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;

namespace Amazon.Runtime
{
	// Token: 0x020000CB RID: 203
	public abstract class RefreshingAWSCredentials : AWSCredentials
	{
		// Token: 0x17000283 RID: 643
		// (get) Token: 0x06000840 RID: 2112 RVA: 0x00014498 File Offset: 0x00012698
		// (set) Token: 0x06000841 RID: 2113 RVA: 0x000144A0 File Offset: 0x000126A0
		public TimeSpan PreemptExpiryTime
		{
			get
			{
				return this._preemptExpiryTime;
			}
			set
			{
				if (value < TimeSpan.Zero)
				{
					throw new ArgumentOutOfRangeException("value", "PreemptExpiryTime cannot be negative");
				}
				this._preemptExpiryTime = value;
			}
		}

		// Token: 0x06000842 RID: 2114 RVA: 0x000144C8 File Offset: 0x000126C8
		public override ImmutableCredentials GetCredentials()
		{
			object refreshLock = this._refreshLock;
			ImmutableCredentials result;
			lock (refreshLock)
			{
				if (this.ShouldUpdate)
				{
					this.currentState = this.GenerateNewCredentials();
					this.UpdateToGeneratedCredentials(this.currentState);
				}
				result = this.currentState.Credentials.Copy();
			}
			return result;
		}

		// Token: 0x06000843 RID: 2115 RVA: 0x00014530 File Offset: 0x00012730
		private void UpdateToGeneratedCredentials(RefreshingAWSCredentials.CredentialsRefreshState state)
		{
			if (this.ShouldUpdate)
			{
				string message;
				if (state == null)
				{
					message = "Unable to generate temporary credentials";
				}
				else
				{
					message = string.Format(CultureInfo.InvariantCulture, "The retrieved credentials have already expired: Now = {0}, Credentials expiration = {1}", new object[]
					{
						AWSSDKUtils.CorrectedUtcNow.ToLocalTime(),
						state.Expiration
					});
				}
				throw new AmazonClientException(message);
			}
			state.Expiration -= this.PreemptExpiryTime;
			if (this.ShouldUpdate)
			{
				Logger.GetLogger(typeof(RefreshingAWSCredentials)).InfoFormat("The preempt expiry time is set too high: Current time = {0}, Credentials expiry time = {1}, Preempt expiry time = {2}.", new object[]
				{
					AWSSDKUtils.CorrectedUtcNow.ToLocalTime(),
					this.currentState.Expiration,
					this.PreemptExpiryTime
				});
			}
		}

		// Token: 0x17000284 RID: 644
		// (get) Token: 0x06000844 RID: 2116 RVA: 0x00014604 File Offset: 0x00012804
		private bool ShouldUpdate
		{
			get
			{
				if (this.currentState == null)
				{
					return true;
				}
				DateTime correctedUtcNow = AWSSDKUtils.CorrectedUtcNow;
				DateTime t = this.currentState.Expiration.ToUniversalTime();
				return correctedUtcNow > t;
			}
		}

		// Token: 0x06000845 RID: 2117 RVA: 0x0000C8E9 File Offset: 0x0000AAE9
		protected virtual RefreshingAWSCredentials.CredentialsRefreshState GenerateNewCredentials()
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000846 RID: 2118 RVA: 0x0001463A File Offset: 0x0001283A
		public virtual void ClearCredentials()
		{
			this.currentState = null;
		}

		// Token: 0x04000348 RID: 840
		protected RefreshingAWSCredentials.CredentialsRefreshState currentState;

		// Token: 0x04000349 RID: 841
		private object _refreshLock = new object();

		// Token: 0x0400034A RID: 842
		private TimeSpan _preemptExpiryTime = TimeSpan.FromMinutes(0.0);

		// Token: 0x020001DD RID: 477
		public class CredentialsRefreshState
		{
			// Token: 0x17000438 RID: 1080
			// (get) Token: 0x06000F57 RID: 3927 RVA: 0x00027560 File Offset: 0x00025760
			// (set) Token: 0x06000F58 RID: 3928 RVA: 0x00027568 File Offset: 0x00025768
			public ImmutableCredentials Credentials { get; set; }

			// Token: 0x17000439 RID: 1081
			// (get) Token: 0x06000F59 RID: 3929 RVA: 0x00027571 File Offset: 0x00025771
			// (set) Token: 0x06000F5A RID: 3930 RVA: 0x00027579 File Offset: 0x00025779
			public DateTime Expiration { get; set; }

			// Token: 0x06000F5B RID: 3931 RVA: 0x0000584A File Offset: 0x00003A4A
			public CredentialsRefreshState()
			{
			}

			// Token: 0x06000F5C RID: 3932 RVA: 0x00027582 File Offset: 0x00025782
			public CredentialsRefreshState(ImmutableCredentials credentials, DateTime expiration)
			{
				this.Credentials = credentials;
				this.Expiration = expiration;
			}
		}
	}
}
