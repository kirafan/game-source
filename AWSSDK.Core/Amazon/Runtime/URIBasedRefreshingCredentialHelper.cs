﻿using System;
using System.Globalization;
using System.Net;
using Amazon.Util;
using ThirdParty.Json.LitJson;

namespace Amazon.Runtime
{
	// Token: 0x020000CE RID: 206
	public class URIBasedRefreshingCredentialHelper : RefreshingAWSCredentials
	{
		// Token: 0x06000857 RID: 2135 RVA: 0x000149C0 File Offset: 0x00012BC0
		protected static string GetContents(Uri uri)
		{
			string result;
			try
			{
				result = AWSSDKUtils.DownloadStringContent(uri);
			}
			catch (WebException)
			{
				throw new AmazonServiceException("Unable to reach credentials server");
			}
			return result;
		}

		// Token: 0x06000858 RID: 2136 RVA: 0x000149F4 File Offset: 0x00012BF4
		protected static T GetObjectFromResponse<T>(Uri uri)
		{
			return JsonMapper.ToObject<T>(URIBasedRefreshingCredentialHelper.GetContents(uri));
		}

		// Token: 0x06000859 RID: 2137 RVA: 0x00014A04 File Offset: 0x00012C04
		protected static void ValidateResponse(URIBasedRefreshingCredentialHelper.SecurityBase response)
		{
			if (!string.Equals(response.Code, URIBasedRefreshingCredentialHelper.SuccessCode, StringComparison.OrdinalIgnoreCase))
			{
				throw new AmazonServiceException(string.Format(CultureInfo.InvariantCulture, "Unable to retrieve credentials. Code = \"{0}\". Message = \"{1}\".", new object[]
				{
					response.Code,
					response.Message
				}));
			}
		}

		// Token: 0x04000353 RID: 851
		private static string SuccessCode = "Success";

		// Token: 0x020001DE RID: 478
		protected class SecurityBase
		{
			// Token: 0x1700043A RID: 1082
			// (get) Token: 0x06000F5D RID: 3933 RVA: 0x00027598 File Offset: 0x00025798
			// (set) Token: 0x06000F5E RID: 3934 RVA: 0x000275A0 File Offset: 0x000257A0
			public string Code { get; set; }

			// Token: 0x1700043B RID: 1083
			// (get) Token: 0x06000F5F RID: 3935 RVA: 0x000275A9 File Offset: 0x000257A9
			// (set) Token: 0x06000F60 RID: 3936 RVA: 0x000275B1 File Offset: 0x000257B1
			public string Message { get; set; }

			// Token: 0x1700043C RID: 1084
			// (get) Token: 0x06000F61 RID: 3937 RVA: 0x000275BA File Offset: 0x000257BA
			// (set) Token: 0x06000F62 RID: 3938 RVA: 0x000275C2 File Offset: 0x000257C2
			public DateTime LastUpdated { get; set; }
		}

		// Token: 0x020001DF RID: 479
		protected class SecurityInfo : URIBasedRefreshingCredentialHelper.SecurityBase
		{
			// Token: 0x1700043D RID: 1085
			// (get) Token: 0x06000F64 RID: 3940 RVA: 0x000275CB File Offset: 0x000257CB
			// (set) Token: 0x06000F65 RID: 3941 RVA: 0x000275D3 File Offset: 0x000257D3
			public string InstanceProfileArn { get; set; }

			// Token: 0x1700043E RID: 1086
			// (get) Token: 0x06000F66 RID: 3942 RVA: 0x000275DC File Offset: 0x000257DC
			// (set) Token: 0x06000F67 RID: 3943 RVA: 0x000275E4 File Offset: 0x000257E4
			public string InstanceProfileId { get; set; }
		}

		// Token: 0x020001E0 RID: 480
		protected class SecurityCredentials : URIBasedRefreshingCredentialHelper.SecurityBase
		{
			// Token: 0x1700043F RID: 1087
			// (get) Token: 0x06000F69 RID: 3945 RVA: 0x000275F5 File Offset: 0x000257F5
			// (set) Token: 0x06000F6A RID: 3946 RVA: 0x000275FD File Offset: 0x000257FD
			public string Type { get; set; }

			// Token: 0x17000440 RID: 1088
			// (get) Token: 0x06000F6B RID: 3947 RVA: 0x00027606 File Offset: 0x00025806
			// (set) Token: 0x06000F6C RID: 3948 RVA: 0x0002760E File Offset: 0x0002580E
			public string AccessKeyId { get; set; }

			// Token: 0x17000441 RID: 1089
			// (get) Token: 0x06000F6D RID: 3949 RVA: 0x00027617 File Offset: 0x00025817
			// (set) Token: 0x06000F6E RID: 3950 RVA: 0x0002761F File Offset: 0x0002581F
			public string SecretAccessKey { get; set; }

			// Token: 0x17000442 RID: 1090
			// (get) Token: 0x06000F6F RID: 3951 RVA: 0x00027628 File Offset: 0x00025828
			// (set) Token: 0x06000F70 RID: 3952 RVA: 0x00027630 File Offset: 0x00025830
			public string Token { get; set; }

			// Token: 0x17000443 RID: 1091
			// (get) Token: 0x06000F71 RID: 3953 RVA: 0x00027639 File Offset: 0x00025839
			// (set) Token: 0x06000F72 RID: 3954 RVA: 0x00027641 File Offset: 0x00025841
			public DateTime Expiration { get; set; }

			// Token: 0x17000444 RID: 1092
			// (get) Token: 0x06000F73 RID: 3955 RVA: 0x0002764A File Offset: 0x0002584A
			// (set) Token: 0x06000F74 RID: 3956 RVA: 0x00027652 File Offset: 0x00025852
			public string RoleArn { get; set; }
		}
	}
}
