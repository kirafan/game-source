﻿using System;

namespace Amazon.Runtime
{
	// Token: 0x020000B9 RID: 185
	public interface IMetricsFormatter
	{
		// Token: 0x060007D0 RID: 2000
		string FormatMetrics(IRequestMetrics metrics);
	}
}
