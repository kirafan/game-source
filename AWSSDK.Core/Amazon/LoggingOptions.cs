﻿using System;

namespace Amazon
{
	// Token: 0x02000066 RID: 102
	[Flags]
	public enum LoggingOptions
	{
		// Token: 0x04000187 RID: 391
		None = 0,
		// Token: 0x04000188 RID: 392
		Log4Net = 1,
		// Token: 0x04000189 RID: 393
		SystemDiagnostics = 2,
		// Token: 0x0400018A RID: 394
		Console = 16,
		// Token: 0x0400018B RID: 395
		UnityLogger = 8
	}
}
