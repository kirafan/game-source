﻿using System;

namespace Amazon
{
	// Token: 0x0200006B RID: 107
	internal class LoggingSection
	{
		// Token: 0x17000171 RID: 369
		// (get) Token: 0x060004F3 RID: 1267 RVA: 0x0000D622 File Offset: 0x0000B822
		// (set) Token: 0x060004F4 RID: 1268 RVA: 0x0000D62A File Offset: 0x0000B82A
		public LoggingOptions LogTo { get; set; }

		// Token: 0x17000172 RID: 370
		// (get) Token: 0x060004F5 RID: 1269 RVA: 0x0000D633 File Offset: 0x0000B833
		// (set) Token: 0x060004F6 RID: 1270 RVA: 0x0000D63B File Offset: 0x0000B83B
		public ResponseLoggingOption LogResponses { get; set; }

		// Token: 0x17000173 RID: 371
		// (get) Token: 0x060004F7 RID: 1271 RVA: 0x0000D644 File Offset: 0x0000B844
		// (set) Token: 0x060004F8 RID: 1272 RVA: 0x0000D64C File Offset: 0x0000B84C
		public int? LogResponsesSizeLimit { get; set; }

		// Token: 0x17000174 RID: 372
		// (get) Token: 0x060004F9 RID: 1273 RVA: 0x0000D655 File Offset: 0x0000B855
		// (set) Token: 0x060004FA RID: 1274 RVA: 0x0000D65D File Offset: 0x0000B85D
		public bool? LogMetrics { get; set; }

		// Token: 0x17000175 RID: 373
		// (get) Token: 0x060004FB RID: 1275 RVA: 0x0000D666 File Offset: 0x0000B866
		// (set) Token: 0x060004FC RID: 1276 RVA: 0x0000D66E File Offset: 0x0000B86E
		public LogMetricsFormatOption LogMetricsFormat { get; set; }

		// Token: 0x17000176 RID: 374
		// (get) Token: 0x060004FD RID: 1277 RVA: 0x0000D677 File Offset: 0x0000B877
		// (set) Token: 0x060004FE RID: 1278 RVA: 0x0000D67F File Offset: 0x0000B87F
		public Type LogMetricsCustomFormatter { get; set; }

		// Token: 0x040001A5 RID: 421
		public const string logToKey = "logTo";

		// Token: 0x040001A6 RID: 422
		public const string logResponsesKey = "logResponses";

		// Token: 0x040001A7 RID: 423
		public const string logMetricsKey = "logMetrics";

		// Token: 0x040001A8 RID: 424
		public const string logMetricsFormatKey = "logMetricsFormat";

		// Token: 0x040001A9 RID: 425
		public const string logMetricsCustomFormatterKey = "logMetricsCustomFormatter";

		// Token: 0x040001AA RID: 426
		public const string logResponsesSizeLimitKey = "logResponsesSizeLimit";
	}
}
