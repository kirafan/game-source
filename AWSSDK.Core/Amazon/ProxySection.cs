﻿using System;

namespace Amazon
{
	// Token: 0x0200006A RID: 106
	internal class ProxySection
	{
		// Token: 0x1700016D RID: 365
		// (get) Token: 0x060004EA RID: 1258 RVA: 0x0000D5DE File Offset: 0x0000B7DE
		// (set) Token: 0x060004EB RID: 1259 RVA: 0x0000D5E6 File Offset: 0x0000B7E6
		public string Host { get; set; }

		// Token: 0x1700016E RID: 366
		// (get) Token: 0x060004EC RID: 1260 RVA: 0x0000D5EF File Offset: 0x0000B7EF
		// (set) Token: 0x060004ED RID: 1261 RVA: 0x0000D5F7 File Offset: 0x0000B7F7
		public int? Port { get; set; }

		// Token: 0x1700016F RID: 367
		// (get) Token: 0x060004EE RID: 1262 RVA: 0x0000D600 File Offset: 0x0000B800
		// (set) Token: 0x060004EF RID: 1263 RVA: 0x0000D608 File Offset: 0x0000B808
		public string Username { get; set; }

		// Token: 0x17000170 RID: 368
		// (get) Token: 0x060004F0 RID: 1264 RVA: 0x0000D611 File Offset: 0x0000B811
		// (set) Token: 0x060004F1 RID: 1265 RVA: 0x0000D619 File Offset: 0x0000B819
		public string Password { get; set; }

		// Token: 0x0400019D RID: 413
		public const string hostSection = "host";

		// Token: 0x0400019E RID: 414
		public const string portSection = "port";

		// Token: 0x0400019F RID: 415
		public const string usernameSection = "username";

		// Token: 0x040001A0 RID: 416
		public const string passwordSection = "password";
	}
}
