﻿using System;
using System.Collections.Generic;

namespace Amazon.Internal
{
	// Token: 0x0200006F RID: 111
	public interface IRegionEndpointProvider
	{
		// Token: 0x1700017F RID: 383
		// (get) Token: 0x06000517 RID: 1303
		IEnumerable<IRegionEndpoint> AllRegionEndpoints { get; }

		// Token: 0x06000518 RID: 1304
		IRegionEndpoint GetRegionEndpoint(string regionName);
	}
}
