﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using Amazon.Runtime;
using Amazon.Util.Internal;
using ThirdParty.Json.LitJson;

namespace Amazon.Internal
{
	// Token: 0x02000072 RID: 114
	public class RegionEndpointProviderV3 : IRegionEndpointProvider
	{
		// Token: 0x0600052B RID: 1323 RVA: 0x0000E020 File Offset: 0x0000C220
		public RegionEndpointProviderV3()
		{
			using (Stream endpointJsonSourceStream = RegionEndpointProviderV3.GetEndpointJsonSourceStream())
			{
				using (StreamReader streamReader = new StreamReader(endpointJsonSourceStream))
				{
					this._root = JsonMapper.ToObject(streamReader);
				}
			}
		}

		// Token: 0x0600052C RID: 1324 RVA: 0x0000E0A0 File Offset: 0x0000C2A0
		public RegionEndpointProviderV3(JsonData root)
		{
			this._root = root;
		}

		// Token: 0x0600052D RID: 1325 RVA: 0x0000E0D0 File Offset: 0x0000C2D0
		private static Stream GetEndpointJsonSourceStream()
		{
			return TypeFactory.GetTypeInfo(typeof(RegionEndpointProviderV3)).Assembly.GetManifestResourceStream("Amazon.endpoints.json");
		}

		// Token: 0x17000184 RID: 388
		// (get) Token: 0x0600052E RID: 1326 RVA: 0x0000E0F0 File Offset: 0x0000C2F0
		public IEnumerable<IRegionEndpoint> AllRegionEndpoints
		{
			get
			{
				object allRegionEndpointsLock = this._allRegionEndpointsLock;
				lock (allRegionEndpointsLock)
				{
					object regionEndpointMapLock = this._regionEndpointMapLock;
					lock (regionEndpointMapLock)
					{
						if (this._allRegionEndpoints == null)
						{
							IEnumerable enumerable = this._root["partitions"];
							List<IRegionEndpoint> list = new List<IRegionEndpoint>();
							foreach (object obj in enumerable)
							{
								JsonData jsonData = (JsonData)obj;
								JsonData jsonData2 = jsonData["regions"];
								foreach (string text in jsonData2.PropertyNames)
								{
									IRegionEndpoint regionEndpoint;
									if (!this._regionEndpointMap.TryGetValue(text, out regionEndpoint))
									{
										regionEndpoint = new RegionEndpointV3(text, (string)jsonData2[text]["description"], jsonData, jsonData["services"]);
										this._regionEndpointMap.Add(text, regionEndpoint);
									}
									list.Add(regionEndpoint);
								}
							}
							this._allRegionEndpoints = list;
						}
					}
				}
				return this._allRegionEndpoints;
			}
		}

		// Token: 0x0600052F RID: 1327 RVA: 0x0000E288 File Offset: 0x0000C488
		private static string GetUnknownRegionDescription(string regionName)
		{
			if (regionName.StartsWith("cn-", StringComparison.OrdinalIgnoreCase) || regionName.EndsWith("cn-global", StringComparison.OrdinalIgnoreCase))
			{
				return "China (Unknown)";
			}
			return "Unknown";
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x0000E2B4 File Offset: 0x0000C4B4
		private static bool IsRegionInPartition(string regionName, JsonData partition, out string description)
		{
			JsonData jsonData = partition["regions"];
			string pattern = (string)partition["regionRegex"];
			if (jsonData[regionName] != null)
			{
				description = (string)jsonData[regionName]["description"];
				return true;
			}
			if (regionName.Equals((string)partition["partition"] + "-global", StringComparison.OrdinalIgnoreCase))
			{
				description = "Global";
				return true;
			}
			if (new Regex(pattern).Match(regionName).Success)
			{
				description = RegionEndpointProviderV3.GetUnknownRegionDescription(regionName);
				return true;
			}
			description = RegionEndpointProviderV3.GetUnknownRegionDescription(regionName);
			return false;
		}

		// Token: 0x06000531 RID: 1329 RVA: 0x0000E354 File Offset: 0x0000C554
		public IRegionEndpoint GetRegionEndpoint(string regionName)
		{
			try
			{
				object regionEndpointMapLock = this._regionEndpointMapLock;
				lock (regionEndpointMapLock)
				{
					IRegionEndpoint regionEndpoint;
					if (this._regionEndpointMap.TryGetValue(regionName, out regionEndpoint))
					{
						return regionEndpoint;
					}
					foreach (object obj in ((IEnumerable)this._root["partitions"]))
					{
						JsonData jsonData = (JsonData)obj;
						string displayName;
						if (RegionEndpointProviderV3.IsRegionInPartition(regionName, jsonData, out displayName))
						{
							regionEndpoint = new RegionEndpointV3(regionName, displayName, jsonData, jsonData["services"]);
							this._regionEndpointMap.Add(regionName, regionEndpoint);
							return regionEndpoint;
						}
					}
				}
			}
			catch (Exception)
			{
				throw new AmazonClientException("Invalid endpoint.json format.");
			}
			return this.GetNonstandardRegionEndpoint(regionName);
		}

		// Token: 0x06000532 RID: 1330 RVA: 0x0000E444 File Offset: 0x0000C644
		private IRegionEndpoint GetNonstandardRegionEndpoint(string regionName)
		{
			JsonData partition = this._root["partitions"][0];
			string unknownRegionDescription = RegionEndpointProviderV3.GetUnknownRegionDescription(regionName);
			JsonData services = RegionEndpointProviderV3._emptyDictionaryJsonData;
			foreach (object obj in ((IEnumerable)this._root["partitions"]))
			{
				JsonData jsonData = (JsonData)obj;
				JsonData jsonData2 = jsonData["services"];
				foreach (string prop_name in jsonData2.PropertyNames)
				{
					JsonData jsonData3 = jsonData2[prop_name];
					if (jsonData3 != null && jsonData3["endpoints"][regionName] != null)
					{
						partition = jsonData;
						services = jsonData2;
						break;
					}
				}
			}
			return new RegionEndpointV3(regionName, unknownRegionDescription, partition, services);
		}

		// Token: 0x040001CE RID: 462
		private const string ENDPOINT_JSON_RESOURCE = "Amazon.endpoints.json";

		// Token: 0x040001CF RID: 463
		private const string ENDPOINT_JSON = "endpoints.json";

		// Token: 0x040001D0 RID: 464
		private JsonData _root;

		// Token: 0x040001D1 RID: 465
		private Dictionary<string, IRegionEndpoint> _regionEndpointMap = new Dictionary<string, IRegionEndpoint>();

		// Token: 0x040001D2 RID: 466
		private object _regionEndpointMapLock = new object();

		// Token: 0x040001D3 RID: 467
		private object _allRegionEndpointsLock = new object();

		// Token: 0x040001D4 RID: 468
		private IEnumerable<IRegionEndpoint> _allRegionEndpoints;

		// Token: 0x040001D5 RID: 469
		private static JsonData _emptyDictionaryJsonData = JsonMapper.ToObject("{}");
	}
}
