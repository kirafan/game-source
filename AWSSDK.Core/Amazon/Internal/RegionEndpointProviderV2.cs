﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;
using Amazon.Util.Internal;
using ThirdParty.Json.LitJson;

namespace Amazon.Internal
{
	// Token: 0x02000070 RID: 112
	public class RegionEndpointProviderV2 : IRegionEndpointProvider
	{
		// Token: 0x17000180 RID: 384
		// (get) Token: 0x06000519 RID: 1305 RVA: 0x0000DAAD File Offset: 0x0000BCAD
		public IEnumerable<IRegionEndpoint> AllRegionEndpoints
		{
			get
			{
				return RegionEndpointProviderV2.RegionEndpoint.EnumerableAllRegions as IEnumerable<IRegionEndpoint>;
			}
		}

		// Token: 0x0600051A RID: 1306 RVA: 0x0000DAB9 File Offset: 0x0000BCB9
		public IRegionEndpoint GetRegionEndpoint(string regionName)
		{
			return RegionEndpointProviderV2.RegionEndpoint.GetBySystemName(regionName);
		}

		// Token: 0x020001BE RID: 446
		public class RegionEndpoint : IRegionEndpoint
		{
			// Token: 0x06000EA9 RID: 3753 RVA: 0x00026700 File Offset: 0x00024900
			public Amazon.RegionEndpoint.Endpoint GetEndpointForService(string serviceName, bool dualStack)
			{
				if (!RegionEndpointProviderV2.RegionEndpoint.loaded)
				{
					RegionEndpointProviderV2.RegionEndpoint.LoadEndpointDefinitions();
				}
				JsonData endpointRule = this.GetEndpointRule(serviceName);
				string text = endpointRule["endpoint"].ToString();
				if (dualStack)
				{
					if (serviceName.Equals("s3", StringComparison.OrdinalIgnoreCase))
					{
						if (text.Equals("s3.amazonaws.com", StringComparison.OrdinalIgnoreCase))
						{
							text = "s3.dualstack.us-east-1.amazonaws.com";
						}
						else if (!text.StartsWith("s3-external-", StringComparison.OrdinalIgnoreCase))
						{
							if (text.StartsWith("s3-", StringComparison.OrdinalIgnoreCase))
							{
								text = "s3." + text.Substring(3);
							}
							if (text.StartsWith("s3.", StringComparison.OrdinalIgnoreCase))
							{
								text = text.Replace("s3.", "s3.dualstack.");
							}
						}
					}
					else
					{
						text = text.Replace("{region}", "dualstack.{region}");
					}
				}
				string text2 = text.Replace("{region}", this.SystemName).Replace("{service}", serviceName);
				string signatureVersionOverride = null;
				if (endpointRule["signature-version"] != null)
				{
					signatureVersionOverride = endpointRule["signature-version"].ToString();
				}
				string text3;
				if (endpointRule["auth-region"] != null)
				{
					text3 = endpointRule["auth-region"].ToString();
				}
				else
				{
					text3 = AWSSDKUtils.DetermineRegion(text2);
				}
				if (string.Equals(text3, this.SystemName, StringComparison.OrdinalIgnoreCase))
				{
					text3 = null;
				}
				return new Amazon.RegionEndpoint.Endpoint(text2, text3, signatureVersionOverride);
			}

			// Token: 0x06000EAA RID: 3754 RVA: 0x00026840 File Offset: 0x00024A40
			private JsonData GetEndpointRule(string serviceName)
			{
				JsonData result = null;
				if (RegionEndpointProviderV2.RegionEndpoint._documentEndpoints.TryGetValue(string.Format(CultureInfo.InvariantCulture, "{0}/{1}", new object[]
				{
					this.SystemName,
					serviceName
				}), out result))
				{
					return result;
				}
				if (RegionEndpointProviderV2.RegionEndpoint._documentEndpoints.TryGetValue(string.Format(CultureInfo.InvariantCulture, "{0}/*", new object[]
				{
					this.SystemName
				}), out result))
				{
					return result;
				}
				if (RegionEndpointProviderV2.RegionEndpoint._documentEndpoints.TryGetValue(string.Format(CultureInfo.InvariantCulture, "*/{0}", new object[]
				{
					serviceName
				}), out result))
				{
					return result;
				}
				return RegionEndpointProviderV2.RegionEndpoint._documentEndpoints["*/*"];
			}

			// Token: 0x06000EAB RID: 3755 RVA: 0x000268E8 File Offset: 0x00024AE8
			private static RegionEndpointProviderV2.RegionEndpoint NewEndpoint(string systemName, string displayName)
			{
				RegionEndpointProviderV2.RegionEndpoint regionEndpoint = new RegionEndpointProviderV2.RegionEndpoint(systemName, displayName);
				RegionEndpointProviderV2.RegionEndpoint.hashBySystemName.Add(regionEndpoint.SystemName, regionEndpoint);
				return regionEndpoint;
			}

			// Token: 0x17000415 RID: 1045
			// (get) Token: 0x06000EAC RID: 3756 RVA: 0x0002690F File Offset: 0x00024B0F
			public static IEnumerable<RegionEndpointProviderV2.RegionEndpoint> EnumerableAllRegions
			{
				get
				{
					if (!RegionEndpointProviderV2.RegionEndpoint.loaded)
					{
						RegionEndpointProviderV2.RegionEndpoint.LoadEndpointDefinitions();
					}
					return RegionEndpointProviderV2.RegionEndpoint.hashBySystemName.Values;
				}
			}

			// Token: 0x06000EAD RID: 3757 RVA: 0x00026928 File Offset: 0x00024B28
			public static RegionEndpointProviderV2.RegionEndpoint GetBySystemName(string systemName)
			{
				if (!RegionEndpointProviderV2.RegionEndpoint.loaded)
				{
					RegionEndpointProviderV2.RegionEndpoint.LoadEndpointDefinitions();
				}
				RegionEndpointProviderV2.RegionEndpoint result = null;
				if (RegionEndpointProviderV2.RegionEndpoint.hashBySystemName.TryGetValue(systemName, out result))
				{
					return result;
				}
				Logger.GetLogger(typeof(RegionEndpointProviderV2.RegionEndpoint)).InfoFormat("Region system name {0} was not found in region data bundled with SDK; assuming new region.", new object[]
				{
					systemName
				});
				if (systemName.StartsWith("cn-", StringComparison.Ordinal))
				{
					return RegionEndpointProviderV2.RegionEndpoint.NewEndpoint(systemName, "China (Unknown)");
				}
				return RegionEndpointProviderV2.RegionEndpoint.NewEndpoint(systemName, "Unknown");
			}

			// Token: 0x06000EAE RID: 3758 RVA: 0x0002699C File Offset: 0x00024B9C
			private static void LoadEndpointDefinitions()
			{
				RegionEndpointProviderV2.RegionEndpoint.LoadEndpointDefinitions(AWSConfigs.EndpointDefinition);
			}

			// Token: 0x06000EAF RID: 3759 RVA: 0x000269A8 File Offset: 0x00024BA8
			public static void LoadEndpointDefinitions(string endpointsPath)
			{
				object lock_OBJECT = RegionEndpointProviderV2.RegionEndpoint.LOCK_OBJECT;
				lock (lock_OBJECT)
				{
					if (!RegionEndpointProviderV2.RegionEndpoint.loaded)
					{
						RegionEndpointProviderV2.RegionEndpoint._documentEndpoints = new Dictionary<string, JsonData>();
						if (string.IsNullOrEmpty(endpointsPath))
						{
							RegionEndpointProviderV2.RegionEndpoint.LoadEndpointDefinitionsFromEmbeddedResource();
						}
						RegionEndpointProviderV2.RegionEndpoint.loaded = true;
					}
				}
			}

			// Token: 0x06000EB0 RID: 3760 RVA: 0x00026A00 File Offset: 0x00024C00
			private static void ReadEndpointFile(Stream stream)
			{
				using (StreamReader streamReader = new StreamReader(stream))
				{
					JsonData jsonData = JsonMapper.ToObject(streamReader)["endpoints"];
					foreach (string text in jsonData.PropertyNames)
					{
						RegionEndpointProviderV2.RegionEndpoint._documentEndpoints[text] = jsonData[text];
					}
				}
			}

			// Token: 0x06000EB1 RID: 3761 RVA: 0x00026A88 File Offset: 0x00024C88
			private static void LoadEndpointDefinitionsFromEmbeddedResource()
			{
				using (Stream manifestResourceStream = TypeFactory.GetTypeInfo(typeof(RegionEndpointProviderV2.RegionEndpoint)).Assembly.GetManifestResourceStream("Amazon.endpoints.json"))
				{
					RegionEndpointProviderV2.RegionEndpoint.ReadEndpointFile(manifestResourceStream);
				}
				using (Stream manifestResourceStream2 = TypeFactory.GetTypeInfo(typeof(RegionEndpointProviderV2.RegionEndpoint)).Assembly.GetManifestResourceStream("Amazon.endpoints.customizations.json"))
				{
					RegionEndpointProviderV2.RegionEndpoint.ReadEndpointFile(manifestResourceStream2);
				}
			}

			// Token: 0x06000EB2 RID: 3762 RVA: 0x00026B14 File Offset: 0x00024D14
			public static void UnloadEndpointDefinitions()
			{
				object lock_OBJECT = RegionEndpointProviderV2.RegionEndpoint.LOCK_OBJECT;
				lock (lock_OBJECT)
				{
					RegionEndpointProviderV2.RegionEndpoint._documentEndpoints.Clear();
					RegionEndpointProviderV2.RegionEndpoint.loaded = false;
				}
			}

			// Token: 0x06000EB3 RID: 3763 RVA: 0x00026B58 File Offset: 0x00024D58
			private RegionEndpoint(string systemName, string displayName)
			{
				this.SystemName = systemName;
				this.DisplayName = displayName;
			}

			// Token: 0x17000416 RID: 1046
			// (get) Token: 0x06000EB4 RID: 3764 RVA: 0x00026B6E File Offset: 0x00024D6E
			// (set) Token: 0x06000EB5 RID: 3765 RVA: 0x00026B76 File Offset: 0x00024D76
			public string SystemName { get; private set; }

			// Token: 0x17000417 RID: 1047
			// (get) Token: 0x06000EB6 RID: 3766 RVA: 0x00026B7F File Offset: 0x00024D7F
			// (set) Token: 0x06000EB7 RID: 3767 RVA: 0x00026B87 File Offset: 0x00024D87
			public string DisplayName { get; private set; }

			// Token: 0x17000418 RID: 1048
			// (get) Token: 0x06000EB8 RID: 3768 RVA: 0x00026B90 File Offset: 0x00024D90
			public string RegionName
			{
				get
				{
					return this.SystemName;
				}
			}

			// Token: 0x06000EB9 RID: 3769 RVA: 0x00026B98 File Offset: 0x00024D98
			public override string ToString()
			{
				return string.Format(CultureInfo.InvariantCulture, "{0} ({1})", new object[]
				{
					this.DisplayName,
					this.SystemName
				});
			}

			// Token: 0x04000967 RID: 2407
			private const string REGIONS_FILE = "Amazon.endpoints.json";

			// Token: 0x04000968 RID: 2408
			private const string REGIONS_CUSTOMIZATIONS_FILE = "Amazon.endpoints.customizations.json";

			// Token: 0x04000969 RID: 2409
			private const string DEFAULT_RULE = "*/*";

			// Token: 0x0400096A RID: 2410
			private static Dictionary<string, JsonData> _documentEndpoints;

			// Token: 0x0400096B RID: 2411
			private const int MAX_DOWNLOAD_RETRIES = 3;

			// Token: 0x0400096C RID: 2412
			private static bool loaded = false;

			// Token: 0x0400096D RID: 2413
			private static readonly object LOCK_OBJECT = new object();

			// Token: 0x0400096E RID: 2414
			private static Dictionary<string, RegionEndpointProviderV2.RegionEndpoint> hashBySystemName = new Dictionary<string, RegionEndpointProviderV2.RegionEndpoint>(StringComparer.OrdinalIgnoreCase);
		}
	}
}
