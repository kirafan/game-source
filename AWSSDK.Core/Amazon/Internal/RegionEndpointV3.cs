﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using ThirdParty.Json.LitJson;

namespace Amazon.Internal
{
	// Token: 0x02000071 RID: 113
	public class RegionEndpointV3 : IRegionEndpoint
	{
		// Token: 0x17000181 RID: 385
		// (get) Token: 0x0600051C RID: 1308 RVA: 0x0000DAC1 File Offset: 0x0000BCC1
		// (set) Token: 0x0600051D RID: 1309 RVA: 0x0000DAC9 File Offset: 0x0000BCC9
		public string RegionName { get; private set; }

		// Token: 0x17000182 RID: 386
		// (get) Token: 0x0600051E RID: 1310 RVA: 0x0000DAD2 File Offset: 0x0000BCD2
		// (set) Token: 0x0600051F RID: 1311 RVA: 0x0000DADA File Offset: 0x0000BCDA
		public string DisplayName { get; private set; }

		// Token: 0x17000183 RID: 387
		// (get) Token: 0x06000520 RID: 1312 RVA: 0x0000DAE3 File Offset: 0x0000BCE3
		public string PartitionName
		{
			get
			{
				return (string)this._partitionJsonData["partition"];
			}
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x0000DAFA File Offset: 0x0000BCFA
		public RegionEndpointV3(string regionName, string displayName, JsonData partition, JsonData services)
		{
			this.RegionName = regionName;
			this.DisplayName = displayName;
			this._partitionJsonData = partition;
			this._servicesJsonData = services;
		}

		// Token: 0x06000522 RID: 1314 RVA: 0x0000DB2C File Offset: 0x0000BD2C
		public RegionEndpoint.Endpoint GetEndpointForService(string serviceName, bool dualStack)
		{
			RegionEndpoint.Endpoint result = null;
			RegionEndpointV3.ServiceMap serviceMap = this._serviceMap;
			lock (serviceMap)
			{
				if (!this._servicesLoaded)
				{
					this.ParseAllServices();
					this._servicesLoaded = true;
				}
				if (!this._serviceMap.TryGetEndpoint(serviceName, dualStack, out result))
				{
					result = this.CreateUnknownEndpoint(serviceName, dualStack);
				}
			}
			return result;
		}

		// Token: 0x06000523 RID: 1315 RVA: 0x0000DB94 File Offset: 0x0000BD94
		private RegionEndpoint.Endpoint CreateUnknownEndpoint(string serviceName, bool dualStack)
		{
			string text = (string)this._partitionJsonData["defaults"]["hostname"];
			if (dualStack)
			{
				text = text.Replace("{region}", "dualstack.{region}");
			}
			return new RegionEndpoint.Endpoint(text.Replace("{service}", serviceName).Replace("{region}", this.RegionName).Replace("{dnsSuffix}", (string)this._partitionJsonData["dnsSuffix"]), null, null);
		}

		// Token: 0x06000524 RID: 1316 RVA: 0x0000DC18 File Offset: 0x0000BE18
		private void ParseAllServices()
		{
			foreach (string text in this._servicesJsonData.PropertyNames)
			{
				this.AddServiceToMap(this._servicesJsonData[text], text);
			}
		}

		// Token: 0x06000525 RID: 1317 RVA: 0x0000DC78 File Offset: 0x0000BE78
		private void AddServiceToMap(JsonData service, string serviceName)
		{
			string text = (service["partitionEndpoint"] != null) ? ((string)service["partitionEndpoint"]) : "";
			bool flag = service["isRegionalized"] == null || (bool)service["isRegionalized"];
			string prop_name = this.RegionName;
			if (!flag && !string.IsNullOrEmpty(text))
			{
				prop_name = text;
			}
			JsonData jsonData = service["endpoints"][prop_name];
			if (jsonData == null)
			{
				return;
			}
			JsonData jsonData2 = new JsonData();
			RegionEndpointV3.MergeJsonData(jsonData2, jsonData);
			RegionEndpointV3.MergeJsonData(jsonData2, service["defaults"]);
			RegionEndpointV3.MergeJsonData(jsonData2, this._partitionJsonData["defaults"]);
			this.CreateEndpointAndAddToServiceMap(jsonData2, this.RegionName, serviceName);
		}

		// Token: 0x06000526 RID: 1318 RVA: 0x0000DD38 File Offset: 0x0000BF38
		private static void MergeJsonData(JsonData target, JsonData source)
		{
			if (source == null || target == null)
			{
				return;
			}
			foreach (string prop_name in source.PropertyNames)
			{
				if (target[prop_name] == null)
				{
					target[prop_name] = source[prop_name];
				}
			}
		}

		// Token: 0x06000527 RID: 1319 RVA: 0x0000DD9C File Offset: 0x0000BF9C
		private void CreateEndpointAndAddToServiceMap(JsonData result, string regionName, string serviceName)
		{
			this.CreateEndpointAndAddToServiceMap(result, regionName, serviceName, false);
			this.CreateEndpointAndAddToServiceMap(result, regionName, serviceName, true);
		}

		// Token: 0x06000528 RID: 1320 RVA: 0x0000DDB4 File Offset: 0x0000BFB4
		private void CreateEndpointAndAddToServiceMap(JsonData result, string regionName, string serviceName, bool dualStack)
		{
			string text = ((string)result["hostname"]).Replace("{service}", serviceName).Replace("{region}", regionName).Replace("{dnsSuffix}", (string)this._partitionJsonData["dnsSuffix"]);
			if (dualStack)
			{
				if (serviceName.Equals("s3", StringComparison.OrdinalIgnoreCase))
				{
					if (text.Equals("s3.amazonaws.com", StringComparison.OrdinalIgnoreCase))
					{
						text = "s3.dualstack.us-east-1.amazonaws.com";
					}
					else if (!text.StartsWith("s3-external-", StringComparison.OrdinalIgnoreCase))
					{
						if (text.StartsWith("s3-", StringComparison.OrdinalIgnoreCase))
						{
							text = "s3." + text.Substring(3);
						}
						if (text.StartsWith("s3.", StringComparison.OrdinalIgnoreCase))
						{
							text = text.Replace("s3.", "s3.dualstack.");
						}
					}
				}
				else
				{
					text = string.Format(CultureInfo.InvariantCulture, "{0}.{1}.{2}", new object[]
					{
						serviceName,
						"dualstack." + regionName,
						(string)this._partitionJsonData["dnsSuffix"]
					});
				}
			}
			string authregion = null;
			string text2 = null;
			JsonData jsonData = result["credentialScope"];
			if (jsonData != null)
			{
				authregion = RegionEndpointV3.DetermineAuthRegion(jsonData);
				if (jsonData["service"] != null && string.Compare((string)jsonData["service"], serviceName, StringComparison.OrdinalIgnoreCase) != 0)
				{
					text2 = (string)jsonData["service"];
				}
			}
			string signatureVersionOverride = RegionEndpointV3.DetermineSignatureOverride(result, serviceName);
			RegionEndpoint.Endpoint endpoint = new RegionEndpoint.Endpoint(text, authregion, signatureVersionOverride);
			this._serviceMap.Add(serviceName, dualStack, endpoint);
			if (!string.IsNullOrEmpty(text2) && !this._serviceMap.ContainsKey(text2))
			{
				this._serviceMap.Add(text2, dualStack, endpoint);
			}
		}

		// Token: 0x06000529 RID: 1321 RVA: 0x0000DF64 File Offset: 0x0000C164
		private static string DetermineSignatureOverride(JsonData defaults, string serviceName)
		{
			if (!string.Equals(serviceName, "s3", StringComparison.OrdinalIgnoreCase))
			{
				return null;
			}
			bool flag = false;
			using (IEnumerator enumerator = ((IEnumerable)defaults["signatureVersions"]).GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (string.Equals((string)((JsonData)enumerator.Current), "s3", StringComparison.OrdinalIgnoreCase))
					{
						flag = true;
						break;
					}
				}
			}
			if (!flag)
			{
				return "4";
			}
			return "2";
		}

		// Token: 0x0600052A RID: 1322 RVA: 0x0000DFF0 File Offset: 0x0000C1F0
		private static string DetermineAuthRegion(JsonData credentialScope)
		{
			string result = null;
			if (credentialScope["region"] != null)
			{
				result = (string)credentialScope["region"];
			}
			return result;
		}

		// Token: 0x040001C8 RID: 456
		private RegionEndpointV3.ServiceMap _serviceMap = new RegionEndpointV3.ServiceMap();

		// Token: 0x040001CB RID: 459
		private JsonData _partitionJsonData;

		// Token: 0x040001CC RID: 460
		private JsonData _servicesJsonData;

		// Token: 0x040001CD RID: 461
		private bool _servicesLoaded;

		// Token: 0x020001BF RID: 447
		private class ServiceMap
		{
			// Token: 0x06000EBB RID: 3771 RVA: 0x00026BE2 File Offset: 0x00024DE2
			private Dictionary<string, RegionEndpoint.Endpoint> GetMap(bool dualStack)
			{
				if (!dualStack)
				{
					return this._serviceMap;
				}
				return this._dualServiceMap;
			}

			// Token: 0x06000EBC RID: 3772 RVA: 0x00026BF4 File Offset: 0x00024DF4
			public bool ContainsKey(string servicName)
			{
				return this._serviceMap.ContainsKey(servicName);
			}

			// Token: 0x06000EBD RID: 3773 RVA: 0x00026C02 File Offset: 0x00024E02
			public void Add(string serviceName, bool dualStack, RegionEndpoint.Endpoint endpoint)
			{
				if (!dualStack)
				{
					Dictionary<string, RegionEndpoint.Endpoint> serviceMap = this._serviceMap;
				}
				else
				{
					Dictionary<string, RegionEndpoint.Endpoint> dualServiceMap = this._dualServiceMap;
				}
				this.GetMap(dualStack).Add(serviceName, endpoint);
			}

			// Token: 0x06000EBE RID: 3774 RVA: 0x00026C25 File Offset: 0x00024E25
			public bool TryGetEndpoint(string serviceName, bool dualStack, out RegionEndpoint.Endpoint endpoint)
			{
				return this.GetMap(dualStack).TryGetValue(serviceName, out endpoint);
			}

			// Token: 0x04000971 RID: 2417
			private Dictionary<string, RegionEndpoint.Endpoint> _serviceMap = new Dictionary<string, RegionEndpoint.Endpoint>();

			// Token: 0x04000972 RID: 2418
			private Dictionary<string, RegionEndpoint.Endpoint> _dualServiceMap = new Dictionary<string, RegionEndpoint.Endpoint>();
		}
	}
}
