﻿using System;

namespace Amazon.Internal
{
	// Token: 0x0200006E RID: 110
	public interface IRegionEndpoint
	{
		// Token: 0x1700017D RID: 381
		// (get) Token: 0x06000514 RID: 1300
		string RegionName { get; }

		// Token: 0x1700017E RID: 382
		// (get) Token: 0x06000515 RID: 1301
		string DisplayName { get; }

		// Token: 0x06000516 RID: 1302
		RegionEndpoint.Endpoint GetEndpointForService(string serviceName, bool dualStack);
	}
}
