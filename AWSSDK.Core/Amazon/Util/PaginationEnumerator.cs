﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Amazon.Util
{
	// Token: 0x0200007E RID: 126
	internal class PaginationEnumerator<U> : IEnumerator<U>, IDisposable, IEnumerator
	{
		// Token: 0x060005AA RID: 1450 RVA: 0x0000FB25 File Offset: 0x0000DD25
		internal PaginationEnumerator(PaginatedResource<U> paginatedResource)
		{
			this.paginatedResource = paginatedResource;
		}

		// Token: 0x060005AB RID: 1451 RVA: 0x0000FB48 File Offset: 0x0000DD48
		public bool MoveNext()
		{
			this.position++;
			while (this.position == this.currentSpot.Data.Count)
			{
				if (!this.started || !string.IsNullOrEmpty(this.currentSpot.NextToken))
				{
					this.currentSpot = this.paginatedResource.fetcher(this.currentSpot.NextToken);
					this.position = 0;
					this.started = true;
				}
				else
				{
					this.currentSpot = PaginationEnumerator<U>.blankSpot;
					this.position = -1;
				}
			}
			return this.position != -1;
		}

		// Token: 0x060005AC RID: 1452 RVA: 0x0000FBE6 File Offset: 0x0000DDE6
		public void Reset()
		{
			this.position = -1;
			this.currentSpot = new Marker<U>(new List<U>(), null);
			this.started = false;
		}

		// Token: 0x170001A1 RID: 417
		// (get) Token: 0x060005AD RID: 1453 RVA: 0x0000FC07 File Offset: 0x0000DE07
		object IEnumerator.Current
		{
			get
			{
				return this.Current;
			}
		}

		// Token: 0x170001A2 RID: 418
		// (get) Token: 0x060005AE RID: 1454 RVA: 0x0000FC14 File Offset: 0x0000DE14
		public U Current
		{
			get
			{
				U result;
				try
				{
					result = this.currentSpot.Data[this.position];
				}
				catch (IndexOutOfRangeException)
				{
					throw new InvalidOperationException();
				}
				return result;
			}
		}

		// Token: 0x060005AF RID: 1455 RVA: 0x00005805 File Offset: 0x00003A05
		public void Dispose()
		{
		}

		// Token: 0x0400025E RID: 606
		private PaginatedResource<U> paginatedResource;

		// Token: 0x0400025F RID: 607
		private int position = -1;

		// Token: 0x04000260 RID: 608
		private static Marker<U> blankSpot = new Marker<U>(new List<U>(), null);

		// Token: 0x04000261 RID: 609
		private Marker<U> currentSpot = PaginationEnumerator<U>.blankSpot;

		// Token: 0x04000262 RID: 610
		private bool started;
	}
}
