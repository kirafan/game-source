﻿using System;

namespace Amazon.Util
{
	// Token: 0x02000076 RID: 118
	public class AWSPublicIpAddressRange
	{
		// Token: 0x17000194 RID: 404
		// (get) Token: 0x0600055D RID: 1373 RVA: 0x0000EA3F File Offset: 0x0000CC3F
		// (set) Token: 0x0600055E RID: 1374 RVA: 0x0000EA47 File Offset: 0x0000CC47
		public string IpPrefix { get; internal set; }

		// Token: 0x17000195 RID: 405
		// (get) Token: 0x0600055F RID: 1375 RVA: 0x0000EA50 File Offset: 0x0000CC50
		// (set) Token: 0x06000560 RID: 1376 RVA: 0x0000EA58 File Offset: 0x0000CC58
		public AWSPublicIpAddressRange.AddressFormat IpAddressFormat { get; internal set; }

		// Token: 0x17000196 RID: 406
		// (get) Token: 0x06000561 RID: 1377 RVA: 0x0000EA61 File Offset: 0x0000CC61
		// (set) Token: 0x06000562 RID: 1378 RVA: 0x0000EA69 File Offset: 0x0000CC69
		public string Region { get; internal set; }

		// Token: 0x17000197 RID: 407
		// (get) Token: 0x06000563 RID: 1379 RVA: 0x0000EA72 File Offset: 0x0000CC72
		// (set) Token: 0x06000564 RID: 1380 RVA: 0x0000EA7A File Offset: 0x0000CC7A
		public string Service { get; internal set; }

		// Token: 0x020001C3 RID: 451
		public enum AddressFormat
		{
			// Token: 0x04000978 RID: 2424
			Ipv4,
			// Token: 0x04000979 RID: 2425
			Ipv6
		}
	}
}
