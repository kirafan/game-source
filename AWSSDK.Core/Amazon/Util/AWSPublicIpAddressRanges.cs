﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using Amazon.Runtime;
using ThirdParty.Json.LitJson;

namespace Amazon.Util
{
	// Token: 0x02000075 RID: 117
	public class AWSPublicIpAddressRanges
	{
		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06000551 RID: 1361 RVA: 0x0000E71C File Offset: 0x0000C91C
		public IEnumerable<string> ServiceKeys
		{
			get
			{
				HashSet<string> hashSet = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
				foreach (AWSPublicIpAddressRange awspublicIpAddressRange in this.AllAddressRanges)
				{
					hashSet.Add(awspublicIpAddressRange.Service);
				}
				return hashSet;
			}
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06000552 RID: 1362 RVA: 0x0000E77C File Offset: 0x0000C97C
		// (set) Token: 0x06000553 RID: 1363 RVA: 0x0000E784 File Offset: 0x0000C984
		public DateTime CreateDate { get; private set; }

		// Token: 0x17000193 RID: 403
		// (get) Token: 0x06000554 RID: 1364 RVA: 0x0000E78D File Offset: 0x0000C98D
		// (set) Token: 0x06000555 RID: 1365 RVA: 0x0000E795 File Offset: 0x0000C995
		public IEnumerable<AWSPublicIpAddressRange> AllAddressRanges { get; private set; }

		// Token: 0x06000556 RID: 1366 RVA: 0x0000E7A0 File Offset: 0x0000C9A0
		public IEnumerable<AWSPublicIpAddressRange> AddressRangesByServiceKey(string serviceKey)
		{
			if (!this.AllAddressRanges.Any<AWSPublicIpAddressRange>())
			{
				throw new InvalidOperationException("No address range data has been loaded and parsed.");
			}
			return (from ar in this.AllAddressRanges
			where ar.Service.Equals(serviceKey, StringComparison.OrdinalIgnoreCase)
			select ar).ToList<AWSPublicIpAddressRange>();
		}

		// Token: 0x06000557 RID: 1367 RVA: 0x0000E7F0 File Offset: 0x0000C9F0
		public IEnumerable<AWSPublicIpAddressRange> AddressRangesByRegion(string regionIdentifier)
		{
			if (!this.AllAddressRanges.Any<AWSPublicIpAddressRange>())
			{
				throw new InvalidOperationException("No address range data has been loaded and parsed.");
			}
			return (from ar in this.AllAddressRanges
			where ar.Region.Equals(regionIdentifier, StringComparison.OrdinalIgnoreCase)
			select ar).ToList<AWSPublicIpAddressRange>();
		}

		// Token: 0x06000558 RID: 1368 RVA: 0x0000E840 File Offset: 0x0000CA40
		public static AWSPublicIpAddressRanges Load()
		{
			int i = 0;
			while (i < 3)
			{
				try
				{
					return AWSPublicIpAddressRanges.Parse(AWSSDKUtils.DownloadStringContent(AWSPublicIpAddressRanges.IpAddressRangeEndpoint));
				}
				catch (Exception innerException)
				{
					i++;
					if (i == 3)
					{
						throw new AmazonServiceException(string.Format(CultureInfo.InvariantCulture, "Error downloading public IP address ranges file from {0}.", new object[]
						{
							AWSPublicIpAddressRanges.IpAddressRangeEndpoint
						}), innerException);
					}
				}
				AWSSDKUtils.Sleep(Math.Min((int)(Math.Pow(4.0, (double)i) * 100.0), 30000));
			}
			return null;
		}

		// Token: 0x06000559 RID: 1369 RVA: 0x0000E8D4 File Offset: 0x0000CAD4
		private static AWSPublicIpAddressRanges Parse(string fileContent)
		{
			AWSPublicIpAddressRanges result;
			try
			{
				AWSPublicIpAddressRanges awspublicIpAddressRanges = new AWSPublicIpAddressRanges();
				JsonData jsonData = JsonMapper.ToObject(new JsonReader(fileContent));
				DateTime? dateTime = null;
				try
				{
					string s = (string)jsonData["createDate"];
					dateTime = new DateTime?(DateTime.ParseExact(s, "yyyy-MM-dd-HH-mm-ss", null));
				}
				catch (FormatException)
				{
				}
				catch (ArgumentNullException)
				{
				}
				awspublicIpAddressRanges.CreateDate = dateTime.GetValueOrDefault(AWSSDKUtils.CorrectedUtcNow);
				List<AWSPublicIpAddressRange> list = new List<AWSPublicIpAddressRange>();
				JsonData jsonData2 = jsonData["prefixes"];
				JsonData jsonData3 = jsonData["ipv6_prefixes"];
				if (!jsonData2.IsArray || !jsonData3.IsArray)
				{
					throw new System.IO.InvalidDataException("Expected array content for ip_prefixes and/or ipv6_prefixes keys.");
				}
				list.AddRange(AWSPublicIpAddressRanges.ParseRange(jsonData2, AWSPublicIpAddressRange.AddressFormat.Ipv4));
				list.AddRange(AWSPublicIpAddressRanges.ParseRange(jsonData3, AWSPublicIpAddressRange.AddressFormat.Ipv6));
				awspublicIpAddressRanges.AllAddressRanges = list;
				result = awspublicIpAddressRanges;
			}
			catch (Exception innerException)
			{
				throw new System.IO.InvalidDataException("IP address ranges content in unexpected/invalid format.", innerException);
			}
			return result;
		}

		// Token: 0x0600055A RID: 1370 RVA: 0x0000E9D8 File Offset: 0x0000CBD8
		private static IEnumerable<AWSPublicIpAddressRange> ParseRange(JsonData ranges, AWSPublicIpAddressRange.AddressFormat addressFormat)
		{
			string prefixKey = (addressFormat == AWSPublicIpAddressRange.AddressFormat.Ipv4) ? "ip_prefix" : "ipv6_prefix";
			List<AWSPublicIpAddressRange> list = new List<AWSPublicIpAddressRange>();
			list.AddRange(from JsonData range in ranges
			select new AWSPublicIpAddressRange
			{
				IpAddressFormat = addressFormat,
				IpPrefix = (string)range[prefixKey],
				Region = (string)range["region"],
				Service = (string)range["service"]
			});
			return list;
		}

		// Token: 0x0600055B RID: 1371 RVA: 0x0000584A File Offset: 0x00003A4A
		private AWSPublicIpAddressRanges()
		{
		}

		// Token: 0x040001E3 RID: 483
		public const string AmazonServiceKey = "AMAZON";

		// Token: 0x040001E4 RID: 484
		public const string EC2ServiceKey = "EC2";

		// Token: 0x040001E5 RID: 485
		public const string CloudFrontServiceKey = "CLOUDFRONT";

		// Token: 0x040001E6 RID: 486
		public const string Route53ServiceKey = "ROUTE53";

		// Token: 0x040001E7 RID: 487
		public const string Route53HealthChecksServiceKey = "ROUTE53_HEALTHCHECKS";

		// Token: 0x040001E8 RID: 488
		public const string GlobalRegionIdentifier = "GLOBAL";

		// Token: 0x040001E9 RID: 489
		private const string createDateKey = "createDate";

		// Token: 0x040001EA RID: 490
		private const string ipv4PrefixesKey = "prefixes";

		// Token: 0x040001EB RID: 491
		private const string ipv4PrefixKey = "ip_prefix";

		// Token: 0x040001EC RID: 492
		private const string ipv6PrefixesKey = "ipv6_prefixes";

		// Token: 0x040001ED RID: 493
		private const string ipv6PrefixKey = "ipv6_prefix";

		// Token: 0x040001EE RID: 494
		private const string regionKey = "region";

		// Token: 0x040001EF RID: 495
		private const string serviceKey = "service";

		// Token: 0x040001F0 RID: 496
		private const string createDateFormatString = "yyyy-MM-dd-HH-mm-ss";

		// Token: 0x040001F3 RID: 499
		private static readonly Uri IpAddressRangeEndpoint = new Uri("https://ip-ranges.amazonaws.com/ip-ranges.json");
	}
}
