﻿using System;
using System.Collections.Generic;

namespace Amazon.Util
{
	// Token: 0x0200007D RID: 125
	internal class Marker<U>
	{
		// Token: 0x060005A7 RID: 1447 RVA: 0x0000FAFF File Offset: 0x0000DCFF
		internal Marker(List<U> data, string nextToken)
		{
			this.data = data;
			this.nextToken = nextToken;
		}

		// Token: 0x1700019F RID: 415
		// (get) Token: 0x060005A8 RID: 1448 RVA: 0x0000FB15 File Offset: 0x0000DD15
		internal List<U> Data
		{
			get
			{
				return this.data;
			}
		}

		// Token: 0x170001A0 RID: 416
		// (get) Token: 0x060005A9 RID: 1449 RVA: 0x0000FB1D File Offset: 0x0000DD1D
		internal string NextToken
		{
			get
			{
				return this.nextToken;
			}
		}

		// Token: 0x0400025C RID: 604
		private List<U> data;

		// Token: 0x0400025D RID: 605
		private string nextToken;
	}
}
