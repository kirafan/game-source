﻿using System;

namespace Amazon.Util.Storage
{
	// Token: 0x02000082 RID: 130
	public abstract class KVStore
	{
		// Token: 0x060005D3 RID: 1491
		public abstract void Clear(string key);

		// Token: 0x060005D4 RID: 1492
		public abstract void Put(string key, string value);

		// Token: 0x060005D5 RID: 1493
		public abstract string Get(string key);
	}
}
