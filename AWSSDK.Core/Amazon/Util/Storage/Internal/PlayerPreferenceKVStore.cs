﻿using System;
using System.Threading;
using Amazon.Runtime.Internal;
using UnityEngine;

namespace Amazon.Util.Storage.Internal
{
	// Token: 0x02000084 RID: 132
	public class PlayerPreferenceKVStore : KVStore
	{
		// Token: 0x060005D9 RID: 1497 RVA: 0x000103D0 File Offset: 0x0000E5D0
		public override void Clear(string key)
		{
			if (UnityInitializer.IsMainThread())
			{
				this.ClearHelper(key);
				return;
			}
			UnityRequestQueue.Instance.ExecuteOnMainThread(delegate
			{
				this.ClearHelper(key);
			});
		}

		// Token: 0x060005DA RID: 1498 RVA: 0x0001041C File Offset: 0x0000E61C
		public override void Put(string key, string value)
		{
			PlayerPreferenceKVStore.<>c__DisplayClass1_0 CS$<>8__locals1 = new PlayerPreferenceKVStore.<>c__DisplayClass1_0();
			CS$<>8__locals1.<>4__this = this;
			CS$<>8__locals1.key = key;
			CS$<>8__locals1.value = value;
			if (UnityInitializer.IsMainThread())
			{
				this.PutHelper(CS$<>8__locals1.key, CS$<>8__locals1.value);
				return;
			}
			AutoResetEvent asyncEvent = new AutoResetEvent(false);
			UnityRequestQueue.Instance.ExecuteOnMainThread(delegate
			{
				CS$<>8__locals1.<>4__this.PutHelper(CS$<>8__locals1.key, CS$<>8__locals1.value);
				asyncEvent.Set();
			});
			asyncEvent.WaitOne();
		}

		// Token: 0x060005DB RID: 1499 RVA: 0x0001049C File Offset: 0x0000E69C
		public override string Get(string key)
		{
			PlayerPreferenceKVStore.<>c__DisplayClass2_1 CS$<>8__locals1 = new PlayerPreferenceKVStore.<>c__DisplayClass2_1();
			CS$<>8__locals1.<>4__this = this;
			CS$<>8__locals1.key = key;
			if (UnityInitializer.IsMainThread())
			{
				return this.GetHelper(CS$<>8__locals1.key);
			}
			string value = string.Empty;
			AutoResetEvent asyncEvent = new AutoResetEvent(false);
			UnityRequestQueue.Instance.ExecuteOnMainThread(delegate
			{
				value = CS$<>8__locals1.<>4__this.GetHelper(CS$<>8__locals1.key);
				asyncEvent.Set();
			});
			asyncEvent.WaitOne();
			return value;
		}

		// Token: 0x060005DC RID: 1500 RVA: 0x0001051D File Offset: 0x0000E71D
		private void PutHelper(string key, string value)
		{
			PlayerPrefs.SetString(key, value);
			PlayerPrefs.Save();
		}

		// Token: 0x060005DD RID: 1501 RVA: 0x0001052B File Offset: 0x0000E72B
		private void ClearHelper(string key)
		{
			PlayerPrefs.DeleteKey(key);
			PlayerPrefs.Save();
		}

		// Token: 0x060005DE RID: 1502 RVA: 0x00010538 File Offset: 0x0000E738
		private string GetHelper(string key)
		{
			string result = string.Empty;
			if (PlayerPrefs.HasKey(key))
			{
				result = PlayerPrefs.GetString(key);
			}
			return result;
		}
	}
}
