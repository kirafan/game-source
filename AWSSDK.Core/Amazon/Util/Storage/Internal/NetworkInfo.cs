﻿using System;
using System.Threading;
using Amazon.Runtime.Internal;
using UnityEngine;

namespace Amazon.Util.Storage.Internal
{
	// Token: 0x02000083 RID: 131
	public class NetworkInfo
	{
		// Token: 0x170001A9 RID: 425
		// (get) Token: 0x060005D7 RID: 1495 RVA: 0x00010374 File Offset: 0x0000E574
		public static NetworkReachability Reachability
		{
			get
			{
				if (UnityInitializer.IsMainThread())
				{
					return Application.internetReachability;
				}
				NetworkReachability _networkReachability = NetworkReachability.NotReachable;
				AutoResetEvent asyncEvent = new AutoResetEvent(false);
				UnityRequestQueue.Instance.ExecuteOnMainThread(delegate
				{
					_networkReachability = Application.internetReachability;
					asyncEvent.Set();
				});
				asyncEvent.WaitOne();
				return _networkReachability;
			}
		}
	}
}
