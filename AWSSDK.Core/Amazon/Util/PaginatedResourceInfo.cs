﻿using System;
using System.Globalization;
using System.Reflection;
using Amazon.Util.Internal;

namespace Amazon.Util
{
	// Token: 0x02000080 RID: 128
	public class PaginatedResourceInfo
	{
		// Token: 0x170001A3 RID: 419
		// (get) Token: 0x060005B9 RID: 1465 RVA: 0x0000FE72 File Offset: 0x0000E072
		// (set) Token: 0x060005BA RID: 1466 RVA: 0x0000FE7A File Offset: 0x0000E07A
		internal object Client { get; set; }

		// Token: 0x170001A4 RID: 420
		// (get) Token: 0x060005BB RID: 1467 RVA: 0x0000FE83 File Offset: 0x0000E083
		// (set) Token: 0x060005BC RID: 1468 RVA: 0x0000FE8B File Offset: 0x0000E08B
		internal string MethodName { get; set; }

		// Token: 0x170001A5 RID: 421
		// (get) Token: 0x060005BD RID: 1469 RVA: 0x0000FE94 File Offset: 0x0000E094
		// (set) Token: 0x060005BE RID: 1470 RVA: 0x0000FE9C File Offset: 0x0000E09C
		internal object Request { get; set; }

		// Token: 0x170001A6 RID: 422
		// (get) Token: 0x060005BF RID: 1471 RVA: 0x0000FEA8 File Offset: 0x0000E0A8
		// (set) Token: 0x060005C0 RID: 1472 RVA: 0x0000FECB File Offset: 0x0000E0CB
		internal string TokenRequestPropertyPath
		{
			get
			{
				string text = this.tokenRequestPropertyPath;
				if (string.IsNullOrEmpty(text))
				{
					text = "NextToken";
				}
				return text;
			}
			set
			{
				this.tokenRequestPropertyPath = value;
			}
		}

		// Token: 0x170001A7 RID: 423
		// (get) Token: 0x060005C1 RID: 1473 RVA: 0x0000FED4 File Offset: 0x0000E0D4
		// (set) Token: 0x060005C2 RID: 1474 RVA: 0x0000FFD7 File Offset: 0x0000E1D7
		internal string TokenResponsePropertyPath
		{
			get
			{
				string text = this.tokenResponsePropertyPath;
				if (string.IsNullOrEmpty(text))
				{
					text = "{0}";
					if (this.Client != null && !string.IsNullOrEmpty(this.MethodName))
					{
						MethodInfo method = TypeFactory.GetTypeInfo(this.Client.GetType()).GetMethod(this.MethodName);
						if (method != null)
						{
							Type returnType = method.ReturnType;
							string text2 = returnType.Name;
							if (text2.EndsWith("Response", StringComparison.Ordinal))
							{
								text2 = text2.Substring(0, text2.Length - 8);
							}
							if (TypeFactory.GetTypeInfo(returnType).GetProperty(string.Format(CultureInfo.InvariantCulture, "{0}Result", new object[]
							{
								text2
							})) != null)
							{
								text = string.Format(CultureInfo.InvariantCulture, text, new object[]
								{
									string.Format(CultureInfo.InvariantCulture, "{0}Result.{1}", new object[]
									{
										text2,
										"{0}"
									})
								});
							}
						}
					}
					text = string.Format(CultureInfo.InvariantCulture, text, new object[]
					{
						"NextToken"
					});
				}
				return text;
			}
			set
			{
				this.tokenResponsePropertyPath = value;
			}
		}

		// Token: 0x170001A8 RID: 424
		// (get) Token: 0x060005C3 RID: 1475 RVA: 0x0000FFE0 File Offset: 0x0000E1E0
		// (set) Token: 0x060005C4 RID: 1476 RVA: 0x0000FFE8 File Offset: 0x0000E1E8
		internal string ItemListPropertyPath { get; set; }

		// Token: 0x060005C5 RID: 1477 RVA: 0x0000FFF1 File Offset: 0x0000E1F1
		public PaginatedResourceInfo WithClient(object client)
		{
			this.Client = client;
			return this;
		}

		// Token: 0x060005C6 RID: 1478 RVA: 0x0000FFFB File Offset: 0x0000E1FB
		public PaginatedResourceInfo WithMethodName(string methodName)
		{
			this.MethodName = methodName;
			return this;
		}

		// Token: 0x060005C7 RID: 1479 RVA: 0x00010005 File Offset: 0x0000E205
		public PaginatedResourceInfo WithRequest(object request)
		{
			this.Request = request;
			return this;
		}

		// Token: 0x060005C8 RID: 1480 RVA: 0x0001000F File Offset: 0x0000E20F
		public PaginatedResourceInfo WithTokenRequestPropertyPath(string tokenRequestPropertyPath)
		{
			this.TokenRequestPropertyPath = tokenRequestPropertyPath;
			return this;
		}

		// Token: 0x060005C9 RID: 1481 RVA: 0x00010019 File Offset: 0x0000E219
		public PaginatedResourceInfo WithTokenResponsePropertyPath(string tokenResponsePropertyPath)
		{
			this.TokenResponsePropertyPath = tokenResponsePropertyPath;
			return this;
		}

		// Token: 0x060005CA RID: 1482 RVA: 0x00010023 File Offset: 0x0000E223
		public PaginatedResourceInfo WithItemListPropertyPath(string itemListPropertyPath)
		{
			this.ItemListPropertyPath = itemListPropertyPath;
			return this;
		}

		// Token: 0x060005CB RID: 1483 RVA: 0x00010030 File Offset: 0x0000E230
		internal void Verify()
		{
			if (this.Client == null)
			{
				throw new ArgumentException("PaginatedResourceInfo.Client needs to be set.");
			}
			Type type = this.Client.GetType();
			MethodInfo method = TypeFactory.GetTypeInfo(type).GetMethod(this.MethodName, new ITypeInfo[]
			{
				TypeFactory.GetTypeInfo(this.Request.GetType())
			});
			if (method == null)
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "{0} has no method called {1}", new object[]
				{
					type.Name,
					this.MethodName
				}));
			}
			Type parameterType = method.GetParameters()[0].ParameterType;
			try
			{
				Convert.ChangeType(this.Request, parameterType, CultureInfo.InvariantCulture);
			}
			catch (Exception)
			{
				throw new ArgumentException("PaginatedResourcInfo.Request is an incompatible type.");
			}
			Type returnType = method.ReturnType;
			PaginatedResourceInfo.VerifyProperty("TokenRequestPropertyPath", parameterType, this.TokenRequestPropertyPath, typeof(string));
			PaginatedResourceInfo.VerifyProperty("TokenResponsePropertyPath", returnType, this.TokenResponsePropertyPath, typeof(string));
			PaginatedResourceInfo.VerifyProperty("ItemListPropertyPath", returnType, this.ItemListPropertyPath, typeof(string), true);
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x00010150 File Offset: 0x0000E350
		private static void VerifyProperty(string propName, Type start, string path, Type expectedType)
		{
			PaginatedResourceInfo.VerifyProperty(propName, start, path, expectedType, false);
		}

		// Token: 0x060005CD RID: 1485 RVA: 0x0001015C File Offset: 0x0000E35C
		private static void VerifyProperty(string propName, Type start, string path, Type expectedType, bool skipTypecheck)
		{
			Type type = null;
			if (string.IsNullOrEmpty(path))
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "{0} must contain a value.", new object[]
				{
					propName
				}));
			}
			try
			{
				type = PaginatedResourceFactory.GetPropertyTypeFromPath(start, path);
			}
			catch (Exception)
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "{0} does not exist on {1}", new object[]
				{
					path,
					start.Name
				}));
			}
			if (!skipTypecheck && type != expectedType)
			{
				throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "{0} on {1} is not of type {2}", new object[]
				{
					path,
					start.Name,
					expectedType.Name
				}));
			}
		}

		// Token: 0x04000263 RID: 611
		private string tokenRequestPropertyPath;

		// Token: 0x04000264 RID: 612
		private string tokenResponsePropertyPath;
	}
}
