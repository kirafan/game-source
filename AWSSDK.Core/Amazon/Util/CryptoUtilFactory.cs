﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Amazon.Runtime;
using ThirdParty.MD5;

namespace Amazon.Util
{
	// Token: 0x02000079 RID: 121
	public static class CryptoUtilFactory
	{
		// Token: 0x1700019E RID: 414
		// (get) Token: 0x0600059A RID: 1434 RVA: 0x0000FAB9 File Offset: 0x0000DCB9
		public static ICryptoUtil CryptoInstance
		{
			get
			{
				return CryptoUtilFactory.util;
			}
		}

		// Token: 0x0400021A RID: 538
		private static CryptoUtilFactory.CryptoUtil util = new CryptoUtilFactory.CryptoUtil();

		// Token: 0x0400021B RID: 539
		private static HashSet<string> _initializedAlgorithmNames = new HashSet<string>();

		// Token: 0x0400021C RID: 540
		private static object _keyedHashAlgorithmCreationLock = new object();

		// Token: 0x020001C7 RID: 455
		private class CryptoUtil : ICryptoUtil
		{
			// Token: 0x06000ECC RID: 3788 RVA: 0x0000584A File Offset: 0x00003A4A
			internal CryptoUtil()
			{
			}

			// Token: 0x06000ECD RID: 3789 RVA: 0x00026D18 File Offset: 0x00024F18
			public string HMACSign(string data, string key, SigningAlgorithm algorithmName)
			{
				byte[] bytes = Encoding.UTF8.GetBytes(data);
				return this.HMACSign(bytes, key, algorithmName);
			}

			// Token: 0x06000ECE RID: 3790 RVA: 0x00026D3C File Offset: 0x00024F3C
			private KeyedHashAlgorithm ThreadSafeCreateKeyedHashedAlgorithm(SigningAlgorithm algorithmName)
			{
				string text = algorithmName.ToString().ToUpper(CultureInfo.InvariantCulture);
				KeyedHashAlgorithm result = null;
				bool flag = true;
				object keyedHashAlgorithmCreationLock = CryptoUtilFactory._keyedHashAlgorithmCreationLock;
				lock (keyedHashAlgorithmCreationLock)
				{
					flag = !CryptoUtilFactory._initializedAlgorithmNames.Contains(text);
					if (flag)
					{
						result = KeyedHashAlgorithm.Create(text);
						CryptoUtilFactory._initializedAlgorithmNames.Add(text);
					}
				}
				if (!flag)
				{
					result = KeyedHashAlgorithm.Create(text);
				}
				return result;
			}

			// Token: 0x06000ECF RID: 3791 RVA: 0x00026DBC File Offset: 0x00024FBC
			public string HMACSign(byte[] data, string key, SigningAlgorithm algorithmName)
			{
				if (string.IsNullOrEmpty(key))
				{
					throw new ArgumentNullException("key", "Please specify a Secret Signing Key.");
				}
				if (data == null || data.Length == 0)
				{
					throw new ArgumentNullException("data", "Please specify data to sign.");
				}
				KeyedHashAlgorithm keyedHashAlgorithm = this.ThreadSafeCreateKeyedHashedAlgorithm(algorithmName);
				if (keyedHashAlgorithm == null)
				{
					throw new InvalidOperationException("Please specify a KeyedHashAlgorithm to use.");
				}
				string result;
				try
				{
					keyedHashAlgorithm.Key = Encoding.UTF8.GetBytes(key);
					result = Convert.ToBase64String(keyedHashAlgorithm.ComputeHash(data));
				}
				finally
				{
					keyedHashAlgorithm.Clear();
				}
				return result;
			}

			// Token: 0x06000ED0 RID: 3792 RVA: 0x00026E48 File Offset: 0x00025048
			public byte[] ComputeSHA256Hash(byte[] data)
			{
				return CryptoUtilFactory.CryptoUtil.SHA256HashAlgorithmInstance.ComputeHash(data);
			}

			// Token: 0x06000ED1 RID: 3793 RVA: 0x00026E55 File Offset: 0x00025055
			public byte[] ComputeSHA256Hash(Stream steam)
			{
				return CryptoUtilFactory.CryptoUtil.SHA256HashAlgorithmInstance.ComputeHash(steam);
			}

			// Token: 0x06000ED2 RID: 3794 RVA: 0x00026E62 File Offset: 0x00025062
			public byte[] ComputeMD5Hash(byte[] data)
			{
				return new MD5Managed().ComputeHash(data);
			}

			// Token: 0x06000ED3 RID: 3795 RVA: 0x00026E6F File Offset: 0x0002506F
			public byte[] ComputeMD5Hash(Stream steam)
			{
				return new MD5Managed().ComputeHash(steam);
			}

			// Token: 0x06000ED4 RID: 3796 RVA: 0x00026E7C File Offset: 0x0002507C
			public byte[] HMACSignBinary(byte[] data, byte[] key, SigningAlgorithm algorithmName)
			{
				if (key == null || key.Length == 0)
				{
					throw new ArgumentNullException("key", "Please specify a Secret Signing Key.");
				}
				if (data == null || data.Length == 0)
				{
					throw new ArgumentNullException("data", "Please specify data to sign.");
				}
				KeyedHashAlgorithm keyedHashAlgorithm = this.ThreadSafeCreateKeyedHashedAlgorithm(algorithmName);
				if (keyedHashAlgorithm == null)
				{
					throw new InvalidOperationException("Please specify a KeyedHashAlgorithm to use.");
				}
				byte[] result;
				try
				{
					keyedHashAlgorithm.Key = key;
					result = keyedHashAlgorithm.ComputeHash(data);
				}
				finally
				{
					keyedHashAlgorithm.Clear();
				}
				return result;
			}

			// Token: 0x17000419 RID: 1049
			// (get) Token: 0x06000ED5 RID: 3797 RVA: 0x00026EF8 File Offset: 0x000250F8
			private static HashAlgorithm SHA256HashAlgorithmInstance
			{
				get
				{
					if (CryptoUtilFactory.CryptoUtil._hashAlgorithm == null)
					{
						CryptoUtilFactory.CryptoUtil._hashAlgorithm = SHA256.Create();
					}
					return CryptoUtilFactory.CryptoUtil._hashAlgorithm;
				}
			}

			// Token: 0x04000980 RID: 2432
			[ThreadStatic]
			private static HashAlgorithm _hashAlgorithm;
		}
	}
}
