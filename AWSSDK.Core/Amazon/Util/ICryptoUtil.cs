﻿using System;
using System.IO;
using Amazon.Runtime;

namespace Amazon.Util
{
	// Token: 0x0200007B RID: 123
	public interface ICryptoUtil
	{
		// Token: 0x0600059D RID: 1437
		string HMACSign(string data, string key, SigningAlgorithm algorithmName);

		// Token: 0x0600059E RID: 1438
		string HMACSign(byte[] data, string key, SigningAlgorithm algorithmName);

		// Token: 0x0600059F RID: 1439
		byte[] ComputeSHA256Hash(byte[] data);

		// Token: 0x060005A0 RID: 1440
		byte[] ComputeSHA256Hash(Stream steam);

		// Token: 0x060005A1 RID: 1441
		byte[] ComputeMD5Hash(byte[] data);

		// Token: 0x060005A2 RID: 1442
		byte[] ComputeMD5Hash(Stream steam);

		// Token: 0x060005A3 RID: 1443
		byte[] HMACSignBinary(byte[] data, byte[] key, SigningAlgorithm algorithmName);
	}
}
