﻿using System;
using System.Collections.Generic;

namespace Amazon.Util
{
	// Token: 0x02000073 RID: 115
	public class ProxyConfig
	{
		// Token: 0x17000185 RID: 389
		// (get) Token: 0x06000534 RID: 1332 RVA: 0x0000E559 File Offset: 0x0000C759
		// (set) Token: 0x06000535 RID: 1333 RVA: 0x0000E561 File Offset: 0x0000C761
		public string Host { get; set; }

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x06000536 RID: 1334 RVA: 0x0000E56A File Offset: 0x0000C76A
		// (set) Token: 0x06000537 RID: 1335 RVA: 0x0000E572 File Offset: 0x0000C772
		public int? Port { get; set; }

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x06000538 RID: 1336 RVA: 0x0000E57B File Offset: 0x0000C77B
		// (set) Token: 0x06000539 RID: 1337 RVA: 0x0000E583 File Offset: 0x0000C783
		public string Username { get; set; }

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x0600053A RID: 1338 RVA: 0x0000E58C File Offset: 0x0000C78C
		// (set) Token: 0x0600053B RID: 1339 RVA: 0x0000E594 File Offset: 0x0000C794
		public string Password { get; set; }

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x0600053C RID: 1340 RVA: 0x0000E59D File Offset: 0x0000C79D
		// (set) Token: 0x0600053D RID: 1341 RVA: 0x0000E5A5 File Offset: 0x0000C7A5
		public List<string> BypassList { get; set; }

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x0600053E RID: 1342 RVA: 0x0000E5AE File Offset: 0x0000C7AE
		// (set) Token: 0x0600053F RID: 1343 RVA: 0x0000E5B6 File Offset: 0x0000C7B6
		public bool BypassOnLocal { get; set; }

		// Token: 0x06000540 RID: 1344 RVA: 0x0000584A File Offset: 0x00003A4A
		internal ProxyConfig()
		{
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x00005805 File Offset: 0x00003A05
		internal void Configure(ProxySection section)
		{
		}
	}
}
