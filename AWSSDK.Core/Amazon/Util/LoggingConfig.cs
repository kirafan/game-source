﻿using System;
using Amazon.Runtime;

namespace Amazon.Util
{
	// Token: 0x02000074 RID: 116
	public class LoggingConfig
	{
		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000542 RID: 1346 RVA: 0x0000E5BF File Offset: 0x0000C7BF
		// (set) Token: 0x06000543 RID: 1347 RVA: 0x0000E5C7 File Offset: 0x0000C7C7
		public LoggingOptions LogTo
		{
			get
			{
				return this._logTo;
			}
			set
			{
				this._logTo = value;
				AWSConfigs.OnPropertyChanged("LogTo");
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000544 RID: 1348 RVA: 0x0000E5DA File Offset: 0x0000C7DA
		// (set) Token: 0x06000545 RID: 1349 RVA: 0x0000E5E2 File Offset: 0x0000C7E2
		public ResponseLoggingOption LogResponses { get; set; }

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x06000546 RID: 1350 RVA: 0x0000E5EB File Offset: 0x0000C7EB
		// (set) Token: 0x06000547 RID: 1351 RVA: 0x0000E5F3 File Offset: 0x0000C7F3
		public int LogResponsesSizeLimit { get; set; }

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000548 RID: 1352 RVA: 0x0000E5FC File Offset: 0x0000C7FC
		// (set) Token: 0x06000549 RID: 1353 RVA: 0x0000E604 File Offset: 0x0000C804
		public bool LogMetrics { get; set; }

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x0600054A RID: 1354 RVA: 0x0000E60D File Offset: 0x0000C80D
		// (set) Token: 0x0600054B RID: 1355 RVA: 0x0000E615 File Offset: 0x0000C815
		public LogMetricsFormatOption LogMetricsFormat { get; set; }

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x0600054C RID: 1356 RVA: 0x0000E61E File Offset: 0x0000C81E
		// (set) Token: 0x0600054D RID: 1357 RVA: 0x0000E626 File Offset: 0x0000C826
		public IMetricsFormatter LogMetricsCustomFormatter { get; set; }

		// Token: 0x0600054E RID: 1358 RVA: 0x0000E62F File Offset: 0x0000C82F
		internal LoggingConfig()
		{
			this.LogTo = AWSConfigs._logging;
			this.LogResponses = AWSConfigs._responseLogging;
			this.LogResponsesSizeLimit = LoggingConfig.DefaultLogResponsesSizeLimit;
			this.LogMetrics = AWSConfigs._logMetrics;
		}

		// Token: 0x0600054F RID: 1359 RVA: 0x0000E664 File Offset: 0x0000C864
		internal void Configure(LoggingSection section)
		{
			if (section != null)
			{
				this.LogTo = section.LogTo;
				this.LogResponses = section.LogResponses;
				this.LogMetrics = section.LogMetrics.GetValueOrDefault(false);
				this.LogMetricsFormat = section.LogMetricsFormat;
				this.LogResponsesSizeLimit = ((section.LogResponsesSizeLimit != null) ? section.LogResponsesSizeLimit.Value : 1024);
				if (section.LogMetricsCustomFormatter != null && typeof(IMetricsFormatter).IsAssignableFrom(section.LogMetricsCustomFormatter))
				{
					this.LogMetricsCustomFormatter = (Activator.CreateInstance(section.LogMetricsCustomFormatter) as IMetricsFormatter);
				}
			}
		}

		// Token: 0x040001DC RID: 476
		public static readonly int DefaultLogResponsesSizeLimit = 1024;

		// Token: 0x040001DD RID: 477
		private LoggingOptions _logTo;
	}
}
