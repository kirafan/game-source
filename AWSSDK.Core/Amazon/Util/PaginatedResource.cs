﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Amazon.Util
{
	// Token: 0x0200007C RID: 124
	internal class PaginatedResource<U> : IEnumerable<U>, IEnumerable
	{
		// Token: 0x060005A4 RID: 1444 RVA: 0x0000FAE0 File Offset: 0x0000DCE0
		internal PaginatedResource(Func<string, Marker<U>> fetcher)
		{
			this.fetcher = fetcher;
		}

		// Token: 0x060005A5 RID: 1445 RVA: 0x0000FAEF File Offset: 0x0000DCEF
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x060005A6 RID: 1446 RVA: 0x0000FAF7 File Offset: 0x0000DCF7
		public IEnumerator<U> GetEnumerator()
		{
			return new PaginationEnumerator<U>(this);
		}

		// Token: 0x0400025B RID: 603
		internal Func<string, Marker<U>> fetcher;
	}
}
