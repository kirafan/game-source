﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Util;

namespace Amazon.Util
{
	// Token: 0x02000077 RID: 119
	public static class AWSSDKUtils
	{
		// Token: 0x06000566 RID: 1382 RVA: 0x0000EA84 File Offset: 0x0000CC84
		private static string DetermineValidPathCharacters()
		{
			StringBuilder stringBuilder = new StringBuilder();
			foreach (char c in "/:'()!*[]$")
			{
				string text2 = Uri.EscapeUriString(c.ToString());
				if (text2.Length == 1 && text2[0] == c)
				{
					stringBuilder.Append(c);
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000567 RID: 1383 RVA: 0x0000EAE8 File Offset: 0x0000CCE8
		public static string GetExtension(string path)
		{
			if (path == null)
			{
				return null;
			}
			int length = path.Length;
			int num = length;
			while (--num >= 0)
			{
				char c = path[num];
				if (c == '.')
				{
					if (num != length - 1)
					{
						return path.Substring(num, length - num);
					}
					return string.Empty;
				}
				else if (AWSSDKUtils.IsPathSeparator(c))
				{
					break;
				}
			}
			return string.Empty;
		}

		// Token: 0x06000568 RID: 1384 RVA: 0x0000EB3E File Offset: 0x0000CD3E
		private static bool IsPathSeparator(char ch)
		{
			return ch == '\\' || ch == '/' || ch == ':';
		}

		// Token: 0x06000569 RID: 1385 RVA: 0x0000EB54 File Offset: 0x0000CD54
		internal static string CalculateStringToSignV2(ParameterCollection parameterCollection, string serviceUrl)
		{
			StringBuilder stringBuilder = new StringBuilder("POST\n", 512);
			List<KeyValuePair<string, string>> sortedParametersList = parameterCollection.GetSortedParametersList();
			Uri uri = new Uri(serviceUrl);
			stringBuilder.Append(uri.Host);
			stringBuilder.Append("\n");
			string text = uri.AbsolutePath;
			if (text == null || text.Length == 0)
			{
				text = "/";
			}
			stringBuilder.Append(AWSSDKUtils.UrlEncode(text, true));
			stringBuilder.Append("\n");
			foreach (KeyValuePair<string, string> keyValuePair in sortedParametersList)
			{
				if (keyValuePair.Value != null)
				{
					stringBuilder.Append(AWSSDKUtils.UrlEncode(keyValuePair.Key, false));
					stringBuilder.Append("=");
					stringBuilder.Append(AWSSDKUtils.UrlEncode(keyValuePair.Value, false));
					stringBuilder.Append("&");
				}
			}
			string text2 = stringBuilder.ToString();
			return text2.Remove(text2.Length - 1);
		}

		// Token: 0x0600056A RID: 1386 RVA: 0x0000EC64 File Offset: 0x0000CE64
		internal static string GetParametersAsString(IRequest request)
		{
			return AWSSDKUtils.GetParametersAsString(request.ParameterCollection);
		}

		// Token: 0x0600056B RID: 1387 RVA: 0x0000EC74 File Offset: 0x0000CE74
		internal static string GetParametersAsString(ParameterCollection parameterCollection)
		{
			List<KeyValuePair<string, string>> sortedParametersList = parameterCollection.GetSortedParametersList();
			StringBuilder stringBuilder = new StringBuilder(512);
			foreach (KeyValuePair<string, string> keyValuePair in sortedParametersList)
			{
				string key = keyValuePair.Key;
				string value = keyValuePair.Value;
				if (value != null)
				{
					stringBuilder.Append(key);
					stringBuilder.Append('=');
					stringBuilder.Append(AWSSDKUtils.UrlEncode(value, false));
					stringBuilder.Append('&');
				}
			}
			string text = stringBuilder.ToString();
			if (text.Length == 0)
			{
				return string.Empty;
			}
			return text.Remove(text.Length - 1);
		}

		// Token: 0x0600056C RID: 1388 RVA: 0x0000ED30 File Offset: 0x0000CF30
		public static string CanonicalizeResourcePath(Uri endpoint, string resourcePath)
		{
			if (endpoint != null)
			{
				string text = endpoint.AbsolutePath;
				if (string.IsNullOrEmpty(text) || string.Equals(text, "/", StringComparison.Ordinal))
				{
					text = string.Empty;
				}
				if (!string.IsNullOrEmpty(resourcePath) && resourcePath.StartsWith("/", StringComparison.Ordinal))
				{
					resourcePath = resourcePath.Substring(1);
				}
				if (!string.IsNullOrEmpty(resourcePath))
				{
					text = text + "/" + resourcePath;
				}
				resourcePath = text;
			}
			if (string.IsNullOrEmpty(resourcePath))
			{
				return "/";
			}
			string[] value = (from segment in resourcePath.Split(new char[]
			{
				'/'
			}, StringSplitOptions.None)
			select AWSSDKUtils.UrlEncode(segment, false)).ToArray<string>();
			return string.Join("/", value);
		}

		// Token: 0x0600056D RID: 1389 RVA: 0x0000EDF8 File Offset: 0x0000CFF8
		public static string Join(List<string> strings)
		{
			StringBuilder stringBuilder = new StringBuilder();
			bool flag = true;
			foreach (string value in strings)
			{
				if (!flag)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append(value);
				flag = false;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x0600056E RID: 1390 RVA: 0x0000EE68 File Offset: 0x0000D068
		public static string DetermineRegion(string url)
		{
			int num = url.IndexOf("//", StringComparison.Ordinal);
			if (num >= 0)
			{
				url = url.Substring(num + 2);
			}
			if (url.EndsWith("/", StringComparison.Ordinal))
			{
				url = url.Substring(0, url.Length - 1);
			}
			int num2 = url.IndexOf(".amazonaws.com", StringComparison.Ordinal);
			if (num2 < 0)
			{
				return "us-east-1";
			}
			string text = url.Substring(0, num2);
			int num3 = url.IndexOf(".cloudsearch.amazonaws.com", StringComparison.Ordinal);
			if (num3 > 0)
			{
				text = url.Substring(0, num3);
			}
			int num4 = text.IndexOf("queue", StringComparison.Ordinal);
			if (num4 == 0)
			{
				return "us-east-1";
			}
			if (num4 > 0)
			{
				return text.Substring(0, num4 - 1);
			}
			if (text.StartsWith("s3-", StringComparison.Ordinal))
			{
				if (text.Equals("s3-accelerate", StringComparison.Ordinal))
				{
					return null;
				}
				text = "s3." + text.Substring(3);
			}
			int num5 = text.LastIndexOf('.');
			if (num5 == -1)
			{
				return "us-east-1";
			}
			string text2 = text.Substring(num5 + 1);
			if (text2.Equals("external-1"))
			{
				return RegionEndpoint.USEast1.SystemName;
			}
			if (string.Equals(text2, "us-gov", StringComparison.Ordinal))
			{
				return "us-gov-west-1";
			}
			return text2;
		}

		// Token: 0x0600056F RID: 1391 RVA: 0x0000EF94 File Offset: 0x0000D194
		public static string DetermineService(string url)
		{
			int num = url.IndexOf("//", StringComparison.Ordinal);
			if (num >= 0)
			{
				url = url.Substring(num + 2);
			}
			string[] array = url.Split(new char[]
			{
				'.'
			}, StringSplitOptions.RemoveEmptyEntries);
			if (array == null || array.Length == 0)
			{
				return string.Empty;
			}
			string text = array[0];
			int num2 = text.IndexOf('-');
			string text2;
			if (num2 < 0)
			{
				text2 = text;
			}
			else
			{
				text2 = text.Substring(0, num2);
			}
			if (text2.Equals("queue"))
			{
				return "sqs";
			}
			return text2;
		}

		// Token: 0x06000570 RID: 1392 RVA: 0x0000F014 File Offset: 0x0000D214
		public static DateTime ConvertFromUnixEpochSeconds(int seconds)
		{
			return new DateTime((long)seconds * 10000000L + AWSSDKUtils.EPOCH_START.Ticks, DateTimeKind.Utc).ToLocalTime();
		}

		// Token: 0x06000571 RID: 1393 RVA: 0x0000F048 File Offset: 0x0000D248
		public static int ConvertToUnixEpochSeconds(DateTime dateTime)
		{
			TimeSpan timeSpan = new TimeSpan(dateTime.ToUniversalTime().Ticks - AWSSDKUtils.EPOCH_START.Ticks);
			return Convert.ToInt32(timeSpan.TotalSeconds);
		}

		// Token: 0x06000572 RID: 1394 RVA: 0x0000F088 File Offset: 0x0000D288
		public static string ConvertToUnixEpochSecondsString(DateTime dateTime)
		{
			TimeSpan timeSpan = new TimeSpan(dateTime.ToUniversalTime().Ticks - AWSSDKUtils.EPOCH_START.Ticks);
			return Convert.ToInt64(timeSpan.TotalSeconds).ToString(CultureInfo.InvariantCulture);
		}

		// Token: 0x06000573 RID: 1395 RVA: 0x0000F0D4 File Offset: 0x0000D2D4
		public static double ConvertToUnixEpochMilliSeconds(DateTime dateTime)
		{
			TimeSpan timeSpan = new TimeSpan(dateTime.ToUniversalTime().Ticks - AWSSDKUtils.EPOCH_START.Ticks);
			return Math.Round(timeSpan.TotalMilliseconds, 0) / 1000.0;
		}

		// Token: 0x06000574 RID: 1396 RVA: 0x0000F11C File Offset: 0x0000D31C
		public static string ToHex(byte[] data, bool lowercase)
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < data.Length; i++)
			{
				stringBuilder.Append(data[i].ToString(lowercase ? "x2" : "X2", CultureInfo.InvariantCulture));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000575 RID: 1397 RVA: 0x0000F16C File Offset: 0x0000D36C
		public static void InvokeInBackground<T>(EventHandler<T> handler, T args, object sender) where T : EventArgs
		{
			if (handler == null)
			{
				return;
			}
			Delegate[] invocationList = handler.GetInvocationList();
			for (int i = 0; i < invocationList.Length; i++)
			{
				Delegate @delegate = invocationList[i];
				EventHandler<T> eventHandler = (EventHandler<T>)@delegate;
				if (eventHandler != null)
				{
					AWSSDKUtils.Dispatcher.Dispatch(delegate
					{
						eventHandler(sender, args);
					});
				}
			}
		}

		// Token: 0x17000198 RID: 408
		// (get) Token: 0x06000576 RID: 1398 RVA: 0x0000F1E6 File Offset: 0x0000D3E6
		private static BackgroundInvoker Dispatcher
		{
			get
			{
				if (AWSSDKUtils._dispatcher == null)
				{
					AWSSDKUtils._dispatcher = new BackgroundInvoker();
				}
				return AWSSDKUtils._dispatcher;
			}
		}

		// Token: 0x06000577 RID: 1399 RVA: 0x0000F200 File Offset: 0x0000D400
		public static Dictionary<string, string> ParseQueryParameters(string url)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			if (!string.IsNullOrEmpty(url))
			{
				int num = url.IndexOf('?');
				if (num >= 0)
				{
					foreach (string text in url.Substring(num + 1).Split(new char[]
					{
						'&'
					}, StringSplitOptions.None))
					{
						if (!string.IsNullOrEmpty(text))
						{
							string[] array2 = text.Split(new char[]
							{
								'='
							}, 2);
							string key = array2[0];
							string value = (array2.Length == 1) ? null : array2[1];
							dictionary[key] = value;
						}
					}
				}
			}
			return dictionary;
		}

		// Token: 0x06000578 RID: 1400 RVA: 0x0000F298 File Offset: 0x0000D498
		internal static bool AreEqual(object[] itemsA, object[] itemsB)
		{
			if (itemsA == null || itemsB == null)
			{
				return itemsA == itemsB;
			}
			if (itemsA.Length != itemsB.Length)
			{
				return false;
			}
			int num = itemsA.Length;
			for (int i = 0; i < num; i++)
			{
				object a = itemsA[i];
				object b = itemsB[i];
				if (!AWSSDKUtils.AreEqual(a, b))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000579 RID: 1401 RVA: 0x0000F2DC File Offset: 0x0000D4DC
		internal static bool AreEqual(object a, object b)
		{
			if (a == null || b == null)
			{
				return a == b;
			}
			return a == b || a.Equals(b);
		}

		// Token: 0x0600057A RID: 1402 RVA: 0x0000F2F6 File Offset: 0x0000D4F6
		internal static bool DictionariesAreEqual<K, V>(Dictionary<K, V> a, Dictionary<K, V> b)
		{
			if (a == null || b == null)
			{
				return a == b;
			}
			return a == b || (a.Count == b.Count && !a.Except(b).Any<KeyValuePair<K, V>>());
		}

		// Token: 0x0600057B RID: 1403 RVA: 0x0000F328 File Offset: 0x0000D528
		public static MemoryStream GenerateMemoryStreamFromString(string s)
		{
			MemoryStream memoryStream = new MemoryStream();
			StreamWriter streamWriter = new StreamWriter(memoryStream);
			streamWriter.Write(s);
			streamWriter.Flush();
			memoryStream.Position = 0L;
			return memoryStream;
		}

		// Token: 0x0600057C RID: 1404 RVA: 0x0000F349 File Offset: 0x0000D549
		public static void CopyStream(Stream source, Stream destination)
		{
			AWSSDKUtils.CopyStream(source, destination, 8192);
		}

		// Token: 0x0600057D RID: 1405 RVA: 0x0000F358 File Offset: 0x0000D558
		public static void CopyStream(Stream source, Stream destination, int bufferSize)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			if (destination == null)
			{
				throw new ArgumentNullException("destination");
			}
			if (bufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize");
			}
			byte[] array = new byte[bufferSize];
			int count;
			while ((count = source.Read(array, 0, array.Length)) != 0)
			{
				destination.Write(array, 0, count);
			}
		}

		// Token: 0x17000199 RID: 409
		// (get) Token: 0x0600057E RID: 1406 RVA: 0x0000F3B4 File Offset: 0x0000D5B4
		public static string FormattedCurrentTimestampGMT
		{
			get
			{
				DateTime correctedUtcNow = AWSSDKUtils.CorrectedUtcNow;
				DateTime dateTime = new DateTime(correctedUtcNow.Year, correctedUtcNow.Month, correctedUtcNow.Day, correctedUtcNow.Hour, correctedUtcNow.Minute, correctedUtcNow.Second, correctedUtcNow.Millisecond, DateTimeKind.Local);
				return dateTime.ToString("ddd, dd MMM yyyy HH:mm:ss \\G\\M\\T", CultureInfo.InvariantCulture);
			}
		}

		// Token: 0x1700019A RID: 410
		// (get) Token: 0x0600057F RID: 1407 RVA: 0x0000F411 File Offset: 0x0000D611
		public static string FormattedCurrentTimestampISO8601
		{
			get
			{
				return AWSSDKUtils.GetFormattedTimestampISO8601(0);
			}
		}

		// Token: 0x06000580 RID: 1408 RVA: 0x0000F41C File Offset: 0x0000D61C
		public static string GetFormattedTimestampISO8601(int minutesFromNow)
		{
			DateTime dateTime = AWSSDKUtils.CorrectedUtcNow.AddMinutes((double)minutesFromNow);
			DateTime dateTime2 = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Millisecond, DateTimeKind.Local);
			return dateTime2.ToString("yyyy-MM-dd\\THH:mm:ss.fff\\Z", CultureInfo.InvariantCulture);
		}

		// Token: 0x1700019B RID: 411
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x0000F483 File Offset: 0x0000D683
		public static string FormattedCurrentTimestampRFC822
		{
			get
			{
				return AWSSDKUtils.GetFormattedTimestampRFC822(0);
			}
		}

		// Token: 0x06000582 RID: 1410 RVA: 0x0000F48C File Offset: 0x0000D68C
		public static string GetFormattedTimestampRFC822(int minutesFromNow)
		{
			DateTime dateTime = AWSSDKUtils.CorrectedUtcNow.AddMinutes((double)minutesFromNow);
			DateTime dateTime2 = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day, dateTime.Hour, dateTime.Minute, dateTime.Second, dateTime.Millisecond, DateTimeKind.Local);
			return dateTime2.ToString("ddd, dd MMM yyyy HH:mm:ss \\G\\M\\T", CultureInfo.InvariantCulture);
		}

		// Token: 0x06000583 RID: 1411 RVA: 0x0000F4F3 File Offset: 0x0000D6F3
		public static string UrlEncode(string data, bool path)
		{
			return AWSSDKUtils.UrlEncode(3986, data, path);
		}

		// Token: 0x06000584 RID: 1412 RVA: 0x0000F504 File Offset: 0x0000D704
		public static string UrlEncode(int rfcNumber, string data, bool path)
		{
			StringBuilder stringBuilder = new StringBuilder(data.Length * 2);
			string str;
			if (!AWSSDKUtils.RFCEncodingSchemes.TryGetValue(rfcNumber, out str))
			{
				str = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";
			}
			string text = str + (path ? AWSSDKUtils.ValidPathCharacters : "");
			foreach (char c in Encoding.UTF8.GetBytes(data))
			{
				if (text.IndexOf(c) != -1)
				{
					stringBuilder.Append(c);
				}
				else
				{
					stringBuilder.Append("%").Append(string.Format(CultureInfo.InvariantCulture, "{0:X2}", new object[]
					{
						(int)c
					}));
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x06000585 RID: 1413 RVA: 0x0000F5BA File Offset: 0x0000D7BA
		public static void Sleep(TimeSpan ts)
		{
			AWSSDKUtils.Sleep((int)ts.TotalMilliseconds);
		}

		// Token: 0x06000586 RID: 1414 RVA: 0x0000F5C9 File Offset: 0x0000D7C9
		public static string BytesToHexString(byte[] value)
		{
			return BitConverter.ToString(value).Replace("-", string.Empty);
		}

		// Token: 0x06000587 RID: 1415 RVA: 0x0000F5E0 File Offset: 0x0000D7E0
		public static byte[] HexStringToBytes(string hex)
		{
			if (string.IsNullOrEmpty(hex) || hex.Length % 2 == 1)
			{
				throw new ArgumentOutOfRangeException("hex");
			}
			int num = 0;
			byte[] array = new byte[hex.Length / 2];
			for (int i = 0; i < hex.Length; i += 2)
			{
				byte b = Convert.ToByte(hex.Substring(i, 2), 16);
				array[num] = b;
				num++;
			}
			return array;
		}

		// Token: 0x1700019C RID: 412
		// (get) Token: 0x06000588 RID: 1416 RVA: 0x0000F648 File Offset: 0x0000D848
		public static DateTime CorrectedUtcNow
		{
			get
			{
				DateTime dateTime = AWSConfigs.utcNowSource();
				if (AWSConfigs.ManualClockCorrection != null)
				{
					dateTime += AWSConfigs.ManualClockCorrection.Value;
				}
				else if (AWSConfigs.CorrectForClockSkew)
				{
					dateTime += AWSConfigs.ClockOffset;
				}
				return dateTime;
			}
		}

		// Token: 0x06000589 RID: 1417 RVA: 0x0000F69C File Offset: 0x0000D89C
		public static bool HasBidiControlCharacters(string input)
		{
			if (string.IsNullOrEmpty(input))
			{
				return false;
			}
			for (int i = 0; i < input.Length; i++)
			{
				if (AWSSDKUtils.IsBidiControlChar(input[i]))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600058A RID: 1418 RVA: 0x0000F6D8 File Offset: 0x0000D8D8
		private static bool IsBidiControlChar(char c)
		{
			return c >= '‎' && c <= '‮' && (c == '‎' || c == '‏' || c == '‪' || c == '‫' || c == '‬' || c == '‭' || c == '‮');
		}

		// Token: 0x0600058B RID: 1419 RVA: 0x0000F731 File Offset: 0x0000D931
		public static string DownloadStringContent(Uri uri)
		{
			return AWSSDKUtils.DownloadStringContent(uri, TimeSpan.Zero);
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x0000F740 File Offset: 0x0000D940
		public static string DownloadStringContent(Uri uri, TimeSpan timeout)
		{
			HttpWebRequest httpWebRequest = WebRequest.Create(uri) as HttpWebRequest;
			if (timeout > TimeSpan.Zero)
			{
				httpWebRequest.Timeout = (int)timeout.TotalMilliseconds;
			}
			string result;
			using (HttpWebResponse httpWebResponse = httpWebRequest.GetResponse() as HttpWebResponse)
			{
				using (StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream()))
				{
					result = streamReader.ReadToEnd();
				}
			}
			return result;
		}

		// Token: 0x0600058D RID: 1421 RVA: 0x0000F7C8 File Offset: 0x0000D9C8
		public static Stream OpenStream(Uri uri)
		{
			HttpWebRequest httpWebRequest = WebRequest.Create(uri) as HttpWebRequest;
			IAsyncResult asyncResult = httpWebRequest.BeginGetResponse(null, null);
			return (httpWebRequest.EndGetResponse(asyncResult) as HttpWebResponse).GetResponseStream();
		}

		// Token: 0x0600058E RID: 1422 RVA: 0x0000F7FC File Offset: 0x0000D9FC
		public static void ForceCanonicalPathAndQuery(Uri uri)
		{
			try
			{
				FieldInfo field = typeof(Uri).GetField("m_Flags", BindingFlags.Instance | BindingFlags.NonPublic);
				ulong num = (ulong)field.GetValue(uri);
				num &= 18446744073709551567UL;
				field.SetValue(uri, num);
			}
			catch
			{
			}
		}

		// Token: 0x0600058F RID: 1423 RVA: 0x0000F854 File Offset: 0x0000DA54
		public static void PreserveStackTrace(Exception exception)
		{
			if (!AWSSDKUtils._preserveStackTraceLookup)
			{
				object preserveStackTraceLookupLock = AWSSDKUtils._preserveStackTraceLookupLock;
				lock (preserveStackTraceLookupLock)
				{
					AWSSDKUtils._preserveStackTraceLookup = true;
					try
					{
						AWSSDKUtils._preserveStackTrace = typeof(Exception).GetMethod("InternalPreserveStackTrace", BindingFlags.Instance | BindingFlags.NonPublic);
					}
					catch
					{
					}
				}
			}
			if (AWSSDKUtils._preserveStackTrace != null)
			{
				AWSSDKUtils._preserveStackTrace.Invoke(exception, null);
			}
		}

		// Token: 0x06000590 RID: 1424 RVA: 0x0000F8D4 File Offset: 0x0000DAD4
		internal static int GetConnectionLimit(int? clientConfigValue)
		{
			if (clientConfigValue != null)
			{
				return clientConfigValue.Value;
			}
			return 50;
		}

		// Token: 0x06000591 RID: 1425 RVA: 0x0000F8E9 File Offset: 0x0000DAE9
		internal static int GetMaxIdleTime(int? clientConfigValue)
		{
			if (clientConfigValue != null)
			{
				return clientConfigValue.Value;
			}
			return 50000;
		}

		// Token: 0x06000592 RID: 1426 RVA: 0x0000F901 File Offset: 0x0000DB01
		public static void Sleep(int ms)
		{
			Thread.Sleep(ms);
		}

		// Token: 0x1700019D RID: 413
		// (get) Token: 0x06000593 RID: 1427 RVA: 0x0000F90C File Offset: 0x0000DB0C
		internal static bool IsIL2CPP
		{
			get
			{
				Type type = Type.GetType("Mono.Runtime");
				if (type != null)
				{
					MethodInfo method = type.GetMethod("GetDisplayName", BindingFlags.Static | BindingFlags.NonPublic);
					if (method != null)
					{
						string text = null;
						try
						{
							text = method.Invoke(null, null).ToString();
						}
						catch (Exception)
						{
							return false;
						}
						if (text != null && text.ToUpper().Contains("IL2CPP"))
						{
							return true;
						}
						return false;
					}
				}
				return false;
			}
		}

		// Token: 0x040001F8 RID: 504
		internal const string DefaultRegion = "us-east-1";

		// Token: 0x040001F9 RID: 505
		internal const string DefaultGovRegion = "us-gov-west-1";

		// Token: 0x040001FA RID: 506
		private const char SlashChar = '/';

		// Token: 0x040001FB RID: 507
		private const string Slash = "/";

		// Token: 0x040001FC RID: 508
		internal const int DefaultMaxRetry = 3;

		// Token: 0x040001FD RID: 509
		private const int DefaultConnectionLimit = 50;

		// Token: 0x040001FE RID: 510
		private const int DefaultMaxIdleTime = 50000;

		// Token: 0x040001FF RID: 511
		public static readonly DateTime EPOCH_START = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		// Token: 0x04000200 RID: 512
		public const int DefaultBufferSize = 8192;

		// Token: 0x04000201 RID: 513
		public const long DefaultProgressUpdateInterval = 102400L;

		// Token: 0x04000202 RID: 514
		internal static Dictionary<int, string> RFCEncodingSchemes = new Dictionary<int, string>
		{
			{
				3986,
				"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~"
			},
			{
				1738,
				"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_."
			}
		};

		// Token: 0x04000203 RID: 515
		internal const string S3Accelerate = "s3-accelerate";

		// Token: 0x04000204 RID: 516
		public const string UserAgentHeader = "User-Agent";

		// Token: 0x04000205 RID: 517
		public const string ValidUrlCharacters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.~";

		// Token: 0x04000206 RID: 518
		public const string ValidUrlCharactersRFC1738 = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_.";

		// Token: 0x04000207 RID: 519
		private static string ValidPathCharacters = AWSSDKUtils.DetermineValidPathCharacters();

		// Token: 0x04000208 RID: 520
		public const string UrlEncodedContent = "application/x-www-form-urlencoded; charset=utf-8";

		// Token: 0x04000209 RID: 521
		public const string GMTDateFormat = "ddd, dd MMM yyyy HH:mm:ss \\G\\M\\T";

		// Token: 0x0400020A RID: 522
		public const string ISO8601DateFormat = "yyyy-MM-dd\\THH:mm:ss.fff\\Z";

		// Token: 0x0400020B RID: 523
		public const string ISO8601DateFormatNoMS = "yyyy-MM-dd\\THH:mm:ss\\Z";

		// Token: 0x0400020C RID: 524
		public const string ISO8601BasicDateTimeFormat = "yyyyMMddTHHmmssZ";

		// Token: 0x0400020D RID: 525
		public const string ISO8601BasicDateFormat = "yyyyMMdd";

		// Token: 0x0400020E RID: 526
		public const string RFC822DateFormat = "ddd, dd MMM yyyy HH:mm:ss \\G\\M\\T";

		// Token: 0x0400020F RID: 527
		private static BackgroundInvoker _dispatcher;

		// Token: 0x04000210 RID: 528
		private static readonly object _preserveStackTraceLookupLock = new object();

		// Token: 0x04000211 RID: 529
		private static bool _preserveStackTraceLookup = false;

		// Token: 0x04000212 RID: 530
		private static MethodInfo _preserveStackTrace;

		// Token: 0x04000213 RID: 531
		private const int _defaultDefaultConnectionLimit = 2;

		// Token: 0x04000214 RID: 532
		private const int _defaultMaxIdleTime = 100000;
	}
}
