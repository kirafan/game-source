﻿using System;

namespace Amazon.Util
{
	// Token: 0x0200007A RID: 122
	public abstract class HeaderKeys
	{
		// Token: 0x0400021D RID: 541
		public const string IfModifiedSinceHeader = "If-Modified-Since";

		// Token: 0x0400021E RID: 542
		public const string IfMatchHeader = "If-Match";

		// Token: 0x0400021F RID: 543
		public const string IfNoneMatchHeader = "If-None-Match";

		// Token: 0x04000220 RID: 544
		public const string IfUnmodifiedSinceHeader = "If-Unmodified-Since";

		// Token: 0x04000221 RID: 545
		public const string ContentRangeHeader = "Content-Range";

		// Token: 0x04000222 RID: 546
		public const string ContentTypeHeader = "Content-Type";

		// Token: 0x04000223 RID: 547
		public const string ContentLengthHeader = "Content-Length";

		// Token: 0x04000224 RID: 548
		public const string ContentMD5Header = "Content-MD5";

		// Token: 0x04000225 RID: 549
		public const string ContentEncodingHeader = "Content-Encoding";

		// Token: 0x04000226 RID: 550
		public const string ContentDispositionHeader = "Content-Disposition";

		// Token: 0x04000227 RID: 551
		public const string ETagHeader = "ETag";

		// Token: 0x04000228 RID: 552
		public const string Expires = "Expires";

		// Token: 0x04000229 RID: 553
		public const string AuthorizationHeader = "Authorization";

		// Token: 0x0400022A RID: 554
		public const string HostHeader = "host";

		// Token: 0x0400022B RID: 555
		public const string UserAgentHeader = "User-Agent";

		// Token: 0x0400022C RID: 556
		public const string LocationHeader = "location";

		// Token: 0x0400022D RID: 557
		public const string DateHeader = "Date";

		// Token: 0x0400022E RID: 558
		public const string RangeHeader = "Range";

		// Token: 0x0400022F RID: 559
		public const string ExpectHeader = "Expect";

		// Token: 0x04000230 RID: 560
		public const string AcceptHeader = "Accept";

		// Token: 0x04000231 RID: 561
		public const string ConnectionHeader = "Connection";

		// Token: 0x04000232 RID: 562
		public const string StatusHeader = "Status";

		// Token: 0x04000233 RID: 563
		public const string XHttpMethodOverrideHeader = "X-HTTP-Method-Override";

		// Token: 0x04000234 RID: 564
		public const string RequestIdHeader = "x-amzn-RequestId";

		// Token: 0x04000235 RID: 565
		public const string XAmzId2Header = "x-amz-id-2";

		// Token: 0x04000236 RID: 566
		public const string XAmzCloudFrontIdHeader = "X-Amz-Cf-Id";

		// Token: 0x04000237 RID: 567
		public const string XAmzRequestIdHeader = "x-amz-request-id";

		// Token: 0x04000238 RID: 568
		public const string XAmzDateHeader = "X-Amz-Date";

		// Token: 0x04000239 RID: 569
		public const string XAmzErrorType = "x-amzn-ErrorType";

		// Token: 0x0400023A RID: 570
		public const string XAmznErrorMessage = "x-amzn-error-message";

		// Token: 0x0400023B RID: 571
		public const string XAmzSignedHeadersHeader = "X-Amz-SignedHeaders";

		// Token: 0x0400023C RID: 572
		public const string XAmzContentSha256Header = "X-Amz-Content-SHA256";

		// Token: 0x0400023D RID: 573
		public const string XAmzDecodedContentLengthHeader = "X-Amz-Decoded-Content-Length";

		// Token: 0x0400023E RID: 574
		public const string XAmzSecurityTokenHeader = "x-amz-security-token";

		// Token: 0x0400023F RID: 575
		public const string XAmzAuthorizationHeader = "X-Amzn-Authorization";

		// Token: 0x04000240 RID: 576
		public const string XAmzNonceHeader = "x-amz-nonce";

		// Token: 0x04000241 RID: 577
		public const string XAmzServerSideEncryptionHeader = "x-amz-server-side-encryption";

		// Token: 0x04000242 RID: 578
		public const string XAmzServerSideEncryptionAwsKmsKeyIdHeader = "x-amz-server-side-encryption-aws-kms-key-id";

		// Token: 0x04000243 RID: 579
		public const string XAmzBucketRegion = "x-amz-bucket-region";

		// Token: 0x04000244 RID: 580
		public const string XAmzSSECustomerAlgorithmHeader = "x-amz-server-side-encryption-customer-algorithm";

		// Token: 0x04000245 RID: 581
		public const string XAmzSSECustomerKeyHeader = "x-amz-server-side-encryption-customer-key";

		// Token: 0x04000246 RID: 582
		public const string XAmzSSECustomerKeyMD5Header = "x-amz-server-side-encryption-customer-key-MD5";

		// Token: 0x04000247 RID: 583
		public const string XAmzCopySourceSSECustomerAlgorithmHeader = "x-amz-copy-source-server-side-encryption-customer-algorithm";

		// Token: 0x04000248 RID: 584
		public const string XAmzCopySourceSSECustomerKeyHeader = "x-amz-copy-source-server-side-encryption-customer-key";

		// Token: 0x04000249 RID: 585
		public const string XAmzCopySourceSSECustomerKeyMD5Header = "x-amz-copy-source-server-side-encryption-customer-key-MD5";

		// Token: 0x0400024A RID: 586
		public const string XAmzStorageClassHeader = "x-amz-storage-class";

		// Token: 0x0400024B RID: 587
		public const string XAmzWebsiteRedirectLocationHeader = "x-amz-website-redirect-location";

		// Token: 0x0400024C RID: 588
		public const string XAmzContentLengthHeader = "x-amz-content-length";

		// Token: 0x0400024D RID: 589
		public const string XAmzAclHeader = "x-amz-acl";

		// Token: 0x0400024E RID: 590
		public const string XAmzCopySourceHeader = "x-amz-copy-source";

		// Token: 0x0400024F RID: 591
		public const string XAmzCopySourceRangeHeader = "x-amz-copy-source-range";

		// Token: 0x04000250 RID: 592
		public const string XAmzCopySourceIfMatchHeader = "x-amz-copy-source-if-match";

		// Token: 0x04000251 RID: 593
		public const string XAmzCopySourceIfModifiedSinceHeader = "x-amz-copy-source-if-modified-since";

		// Token: 0x04000252 RID: 594
		public const string XAmzCopySourceIfNoneMatchHeader = "x-amz-copy-source-if-none-match";

		// Token: 0x04000253 RID: 595
		public const string XAmzCopySourceIfUnmodifiedSinceHeader = "x-amz-copy-source-if-unmodified-since";

		// Token: 0x04000254 RID: 596
		public const string XAmzMetadataDirectiveHeader = "x-amz-metadata-directive";

		// Token: 0x04000255 RID: 597
		public const string XAmzMfaHeader = "x-amz-mfa";

		// Token: 0x04000256 RID: 598
		public const string XAmzVersionIdHeader = "x-amz-version-id";

		// Token: 0x04000257 RID: 599
		public const string XAmzUserAgentHeader = "x-amz-user-agent";

		// Token: 0x04000258 RID: 600
		public const string XAmzAbortDateHeader = "x-amz-abort-date";

		// Token: 0x04000259 RID: 601
		public const string XAmzAbortRuleIdHeader = "x-amz-abort-rule-id";

		// Token: 0x0400025A RID: 602
		public const string XAmznTraceIdHeader = " x-amzn-trace-id";
	}
}
