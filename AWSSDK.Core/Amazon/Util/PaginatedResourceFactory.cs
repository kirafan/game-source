﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Amazon.Util.Internal;

namespace Amazon.Util
{
	// Token: 0x0200007F RID: 127
	public static class PaginatedResourceFactory
	{
		// Token: 0x060005B1 RID: 1457 RVA: 0x0000FC66 File Offset: 0x0000DE66
		public static object Create<TItemType, TRequestType, TResponseType>(PaginatedResourceInfo pri)
		{
			pri.Verify();
			return PaginatedResourceFactory.Create<TItemType, TRequestType, TResponseType>(pri.Client, pri.MethodName, pri.Request, pri.TokenRequestPropertyPath, pri.TokenResponsePropertyPath, pri.ItemListPropertyPath);
		}

		// Token: 0x060005B2 RID: 1458 RVA: 0x0000FC98 File Offset: 0x0000DE98
		private static PaginatedResource<ItemType> Create<ItemType, TRequestType, TResponseType>(object client, string methodName, object request, string tokenRequestPropertyPath, string tokenResponsePropertyPath, string itemListPropertyPath)
		{
			ITypeInfo typeInfo = TypeFactory.GetTypeInfo(client.GetType());
			MethodInfo fetcherMethod = typeInfo.GetMethod(methodName, new ITypeInfo[]
			{
				TypeFactory.GetTypeInfo(typeof(TRequestType))
			});
			PaginatedResourceFactory.GetFuncType<TRequestType, TResponseType>();
			return PaginatedResourceFactory.Create<ItemType, TRequestType, TResponseType>((TRequestType req) => (TResponseType)((object)fetcherMethod.Invoke(client, new object[]
			{
				req
			})), (TRequestType)((object)request), tokenRequestPropertyPath, tokenResponsePropertyPath, itemListPropertyPath);
		}

		// Token: 0x060005B3 RID: 1459 RVA: 0x0000FD0A File Offset: 0x0000DF0A
		private static PaginatedResource<ItemType> Create<ItemType, TRequestType, TResponseType>(Func<TRequestType, TResponseType> call, TRequestType request, string tokenRequestPropertyPath, string tokenResponsePropertyPath, string itemListPropertyPath)
		{
			return new PaginatedResource<ItemType>(delegate(string token)
			{
				PaginatedResourceFactory.SetPropertyValueAtPath(request, tokenRequestPropertyPath, token);
				TResponseType tresponseType = call(request);
				string propertyValueFromPath = PaginatedResourceFactory.GetPropertyValueFromPath<string>(tresponseType, tokenResponsePropertyPath);
				return new Marker<ItemType>(PaginatedResourceFactory.GetPropertyValueFromPath<List<ItemType>>(tresponseType, itemListPropertyPath), propertyValueFromPath);
			});
		}

		// Token: 0x060005B4 RID: 1460 RVA: 0x0000FD48 File Offset: 0x0000DF48
		private static void SetPropertyValueAtPath(object instance, string path, string value)
		{
			string[] array = path.Split(new char[]
			{
				'.'
			});
			object obj = instance;
			Type type = instance.GetType();
			int i;
			for (i = 0; i < array.Length - 1; i++)
			{
				string name = array[i];
				PropertyInfo property = TypeFactory.GetTypeInfo(type).GetProperty(name);
				obj = property.GetValue(obj, null);
				type = property.PropertyType;
			}
			TypeFactory.GetTypeInfo(type).GetProperty(array[i]).SetValue(obj, value, null);
		}

		// Token: 0x060005B5 RID: 1461 RVA: 0x0000FDB8 File Offset: 0x0000DFB8
		private static T GetPropertyValueFromPath<T>(object instance, string path)
		{
			string[] array = path.Split(new char[]
			{
				'.'
			});
			object obj = instance;
			Type type = instance.GetType();
			foreach (string name in array)
			{
				PropertyInfo property = TypeFactory.GetTypeInfo(type).GetProperty(name);
				obj = property.GetValue(obj, null);
				type = property.PropertyType;
			}
			return (T)((object)obj);
		}

		// Token: 0x060005B6 RID: 1462 RVA: 0x0000FE18 File Offset: 0x0000E018
		internal static Type GetPropertyTypeFromPath(Type start, string path)
		{
			string[] array = path.Split(new char[]
			{
				'.'
			});
			Type type = start;
			foreach (string name in array)
			{
				type = TypeFactory.GetTypeInfo(type).GetProperty(name).PropertyType;
			}
			return type;
		}

		// Token: 0x060005B7 RID: 1463 RVA: 0x0000FE5E File Offset: 0x0000E05E
		private static Type GetFuncType<T, U>()
		{
			return typeof(Func<T, U>);
		}

		// Token: 0x060005B8 RID: 1464 RVA: 0x0000FE6A File Offset: 0x0000E06A
		internal static T Cast<T>(object o)
		{
			return (T)((object)o);
		}
	}
}
