﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Amazon.Util.Internal
{
	// Token: 0x02000086 RID: 134
	public class RootConfig
	{
		// Token: 0x170001AC RID: 428
		// (get) Token: 0x060005F7 RID: 1527 RVA: 0x00010B3C File Offset: 0x0000ED3C
		// (set) Token: 0x060005F8 RID: 1528 RVA: 0x00010B44 File Offset: 0x0000ED44
		public LoggingConfig Logging { get; private set; }

		// Token: 0x170001AD RID: 429
		// (get) Token: 0x060005F9 RID: 1529 RVA: 0x00010B4D File Offset: 0x0000ED4D
		// (set) Token: 0x060005FA RID: 1530 RVA: 0x00010B55 File Offset: 0x0000ED55
		public ProxyConfig Proxy { get; private set; }

		// Token: 0x170001AE RID: 430
		// (get) Token: 0x060005FB RID: 1531 RVA: 0x00010B5E File Offset: 0x0000ED5E
		// (set) Token: 0x060005FC RID: 1532 RVA: 0x00010B66 File Offset: 0x0000ED66
		public string EndpointDefinition { get; set; }

		// Token: 0x170001AF RID: 431
		// (get) Token: 0x060005FD RID: 1533 RVA: 0x00010B6F File Offset: 0x0000ED6F
		// (set) Token: 0x060005FE RID: 1534 RVA: 0x00010B77 File Offset: 0x0000ED77
		public string Region { get; set; }

		// Token: 0x170001B0 RID: 432
		// (get) Token: 0x060005FF RID: 1535 RVA: 0x00010B80 File Offset: 0x0000ED80
		// (set) Token: 0x06000600 RID: 1536 RVA: 0x00010B88 File Offset: 0x0000ED88
		public string ProfileName { get; set; }

		// Token: 0x170001B1 RID: 433
		// (get) Token: 0x06000601 RID: 1537 RVA: 0x00010B91 File Offset: 0x0000ED91
		// (set) Token: 0x06000602 RID: 1538 RVA: 0x00010B99 File Offset: 0x0000ED99
		public string ProfilesLocation { get; set; }

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x06000603 RID: 1539 RVA: 0x00010BA2 File Offset: 0x0000EDA2
		// (set) Token: 0x06000604 RID: 1540 RVA: 0x00010BBE File Offset: 0x0000EDBE
		public RegionEndpoint RegionEndpoint
		{
			get
			{
				if (string.IsNullOrEmpty(this.Region))
				{
					return null;
				}
				return RegionEndpoint.GetBySystemName(this.Region);
			}
			set
			{
				if (value == null)
				{
					this.Region = null;
					return;
				}
				this.Region = value.SystemName;
			}
		}

		// Token: 0x170001B3 RID: 435
		// (get) Token: 0x06000605 RID: 1541 RVA: 0x00010BD7 File Offset: 0x0000EDD7
		// (set) Token: 0x06000606 RID: 1542 RVA: 0x00010BDF File Offset: 0x0000EDDF
		public bool UseSdkCache { get; set; }

		// Token: 0x170001B4 RID: 436
		// (get) Token: 0x06000607 RID: 1543 RVA: 0x00010BE8 File Offset: 0x0000EDE8
		// (set) Token: 0x06000608 RID: 1544 RVA: 0x00010BF0 File Offset: 0x0000EDF0
		public bool CorrectForClockSkew { get; set; }

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06000609 RID: 1545 RVA: 0x00010BF9 File Offset: 0x0000EDF9
		// (set) Token: 0x0600060A RID: 1546 RVA: 0x00010C01 File Offset: 0x0000EE01
		public string ApplicationName { get; set; }

		// Token: 0x0600060B RID: 1547 RVA: 0x00010C0C File Offset: 0x0000EE0C
		public RootConfig()
		{
			this.Logging = new LoggingConfig();
			this.Proxy = new ProxyConfig();
			this.EndpointDefinition = AWSConfigs._endpointDefinition;
			this.Region = AWSConfigs._awsRegion;
			this.ProfileName = AWSConfigs._awsProfileName;
			this.ProfilesLocation = AWSConfigs._awsAccountsLocation;
			this.UseSdkCache = AWSConfigs._useSdkCache;
			this.CorrectForClockSkew = true;
			AWSSection section = AWSConfigs.GetSection<AWSSection>("aws");
			this.Logging.Configure(section.Logging);
			this.Proxy.Configure(section.Proxy);
			this.ServiceSections = section.ServiceSections;
			if (section.UseSdkCache != null)
			{
				this.UseSdkCache = section.UseSdkCache.Value;
			}
			this.EndpointDefinition = RootConfig.Choose(this.EndpointDefinition, section.EndpointDefinition);
			this.Region = RootConfig.Choose(this.Region, section.Region);
			this.ProfileName = RootConfig.Choose(this.ProfileName, section.ProfileName);
			this.ProfilesLocation = RootConfig.Choose(this.ProfilesLocation, section.ProfilesLocation);
			this.ApplicationName = RootConfig.Choose(this.ApplicationName, section.ApplicationName);
			if (section.CorrectForClockSkew != null)
			{
				this.CorrectForClockSkew = section.CorrectForClockSkew.Value;
			}
		}

		// Token: 0x0600060C RID: 1548 RVA: 0x00010D67 File Offset: 0x0000EF67
		private static string Choose(string a, string b)
		{
			if (!string.IsNullOrEmpty(a))
			{
				return a;
			}
			return b;
		}

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x0600060D RID: 1549 RVA: 0x00010D74 File Offset: 0x0000EF74
		// (set) Token: 0x0600060E RID: 1550 RVA: 0x00010D7C File Offset: 0x0000EF7C
		private IDictionary<string, XElement> ServiceSections { get; set; }

		// Token: 0x0600060F RID: 1551 RVA: 0x00010D88 File Offset: 0x0000EF88
		public XElement GetServiceSection(string service)
		{
			XElement result;
			if (this.ServiceSections.TryGetValue(service, out result))
			{
				return result;
			}
			return null;
		}

		// Token: 0x0400027D RID: 637
		private const string _rootAwsSectionName = "aws";
	}
}
