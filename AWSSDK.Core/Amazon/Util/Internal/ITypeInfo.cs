﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Amazon.Util.Internal
{
	// Token: 0x02000087 RID: 135
	public interface ITypeInfo
	{
		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000610 RID: 1552
		Type BaseType { get; }

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x06000611 RID: 1553
		Type Type { get; }

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x06000612 RID: 1554
		Assembly Assembly { get; }

		// Token: 0x170001BA RID: 442
		// (get) Token: 0x06000613 RID: 1555
		bool IsArray { get; }

		// Token: 0x06000614 RID: 1556
		Array ArrayCreateInstance(int length);

		// Token: 0x06000615 RID: 1557
		Type GetInterface(string name);

		// Token: 0x06000616 RID: 1558
		Type[] GetInterfaces();

		// Token: 0x06000617 RID: 1559
		IEnumerable<PropertyInfo> GetProperties();

		// Token: 0x06000618 RID: 1560
		IEnumerable<FieldInfo> GetFields();

		// Token: 0x06000619 RID: 1561
		FieldInfo GetField(string name);

		// Token: 0x0600061A RID: 1562
		MethodInfo GetMethod(string name);

		// Token: 0x0600061B RID: 1563
		MethodInfo GetMethod(string name, ITypeInfo[] paramTypes);

		// Token: 0x0600061C RID: 1564
		MemberInfo[] GetMembers();

		// Token: 0x0600061D RID: 1565
		ConstructorInfo GetConstructor(ITypeInfo[] paramTypes);

		// Token: 0x0600061E RID: 1566
		PropertyInfo GetProperty(string name);

		// Token: 0x0600061F RID: 1567
		bool IsAssignableFrom(ITypeInfo typeInfo);

		// Token: 0x170001BB RID: 443
		// (get) Token: 0x06000620 RID: 1568
		bool IsEnum { get; }

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x06000621 RID: 1569
		bool IsClass { get; }

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06000622 RID: 1570
		bool IsValueType { get; }

		// Token: 0x170001BE RID: 446
		// (get) Token: 0x06000623 RID: 1571
		bool IsInterface { get; }

		// Token: 0x170001BF RID: 447
		// (get) Token: 0x06000624 RID: 1572
		bool IsAbstract { get; }

		// Token: 0x170001C0 RID: 448
		// (get) Token: 0x06000625 RID: 1573
		bool IsSealed { get; }

		// Token: 0x06000626 RID: 1574
		object EnumToObject(object value);

		// Token: 0x06000627 RID: 1575
		ITypeInfo EnumGetUnderlyingType();

		// Token: 0x06000628 RID: 1576
		object CreateInstance();

		// Token: 0x06000629 RID: 1577
		ITypeInfo GetElementType();

		// Token: 0x0600062A RID: 1578
		bool IsType(Type type);

		// Token: 0x170001C1 RID: 449
		// (get) Token: 0x0600062B RID: 1579
		string FullName { get; }

		// Token: 0x170001C2 RID: 450
		// (get) Token: 0x0600062C RID: 1580
		string Name { get; }

		// Token: 0x170001C3 RID: 451
		// (get) Token: 0x0600062D RID: 1581
		bool IsGenericTypeDefinition { get; }

		// Token: 0x170001C4 RID: 452
		// (get) Token: 0x0600062E RID: 1582
		bool IsGenericType { get; }

		// Token: 0x170001C5 RID: 453
		// (get) Token: 0x0600062F RID: 1583
		bool ContainsGenericParameters { get; }

		// Token: 0x06000630 RID: 1584
		Type GetGenericTypeDefinition();

		// Token: 0x06000631 RID: 1585
		Type[] GetGenericArguments();

		// Token: 0x06000632 RID: 1586
		object[] GetCustomAttributes(bool inherit);

		// Token: 0x06000633 RID: 1587
		object[] GetCustomAttributes(ITypeInfo attributeType, bool inherit);
	}
}
