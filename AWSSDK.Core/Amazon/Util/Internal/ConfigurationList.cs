﻿using System;
using System.Collections.Generic;

namespace Amazon.Util.Internal
{
	// Token: 0x0200008D RID: 141
	public abstract class ConfigurationList<T> : List<T>
	{
		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x0600065F RID: 1631 RVA: 0x00011605 File Offset: 0x0000F805
		public IEnumerable<T> Items
		{
			get
			{
				return this;
			}
		}
	}
}
