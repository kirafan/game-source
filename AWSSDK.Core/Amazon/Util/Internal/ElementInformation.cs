﻿using System;

namespace Amazon.Util.Internal
{
	// Token: 0x0200008C RID: 140
	public class ElementInformation
	{
		// Token: 0x0600065C RID: 1628 RVA: 0x000115E5 File Offset: 0x0000F7E5
		public ElementInformation(bool isPresent)
		{
			this.IsPresent = isPresent;
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x0600065D RID: 1629 RVA: 0x000115F4 File Offset: 0x0000F7F4
		// (set) Token: 0x0600065E RID: 1630 RVA: 0x000115FC File Offset: 0x0000F7FC
		public bool IsPresent { get; private set; }
	}
}
