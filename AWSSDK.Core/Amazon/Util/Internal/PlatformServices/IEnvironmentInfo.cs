﻿using System;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000091 RID: 145
	public interface IEnvironmentInfo
	{
		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x06000668 RID: 1640
		string Platform { get; }

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06000669 RID: 1641
		string PlatformVersion { get; }

		// Token: 0x170001DB RID: 475
		// (get) Token: 0x0600066A RID: 1642
		string PclPlatform { get; }

		// Token: 0x170001DC RID: 476
		// (get) Token: 0x0600066B RID: 1643
		string PlatformUserAgent { get; }

		// Token: 0x170001DD RID: 477
		// (get) Token: 0x0600066C RID: 1644
		string FrameworkUserAgent { get; }

		// Token: 0x170001DE RID: 478
		// (get) Token: 0x0600066D RID: 1645
		string Model { get; }

		// Token: 0x170001DF RID: 479
		// (get) Token: 0x0600066E RID: 1646
		string Make { get; }

		// Token: 0x170001E0 RID: 480
		// (get) Token: 0x0600066F RID: 1647
		string Locale { get; }
	}
}
