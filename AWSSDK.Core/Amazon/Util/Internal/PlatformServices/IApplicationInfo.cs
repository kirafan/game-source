﻿using System;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x0200008E RID: 142
	public interface IApplicationInfo
	{
		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x06000661 RID: 1633
		string AppTitle { get; }

		// Token: 0x170001D6 RID: 470
		// (get) Token: 0x06000662 RID: 1634
		string AppVersionName { get; }

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x06000663 RID: 1635
		string AppVersionCode { get; }

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x06000664 RID: 1636
		string PackageName { get; }
	}
}
