﻿using System;
using System.Collections.Generic;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000095 RID: 149
	public class ServiceFactory
	{
		// Token: 0x06000676 RID: 1654 RVA: 0x00011630 File Offset: 0x0000F830
		private ServiceFactory()
		{
			foreach (KeyValuePair<Type, ServiceFactory.InstantiationModel> keyValuePair in this._instantationMappings)
			{
				Type key = keyValuePair.Key;
				if (keyValuePair.Value == ServiceFactory.InstantiationModel.Singleton)
				{
					object value = Activator.CreateInstance(ServiceFactory._mappings[key]);
					this._singletonServices.Add(key, value);
				}
			}
			ServiceFactory._factoryInitialized = true;
		}

		// Token: 0x06000677 RID: 1655 RVA: 0x0001170C File Offset: 0x0000F90C
		public static void RegisterService<T>(Type serviceType)
		{
			if (ServiceFactory._factoryInitialized)
			{
				throw new InvalidOperationException("New services can only be registered before ServiceFactory is accessed by calling ServiceFactory.Instance .");
			}
			object @lock = ServiceFactory._lock;
			lock (@lock)
			{
				ServiceFactory._mappings[typeof(T)] = serviceType;
			}
		}

		// Token: 0x06000678 RID: 1656 RVA: 0x00011768 File Offset: 0x0000F968
		public T GetService<T>()
		{
			Type typeFromHandle = typeof(T);
			if (this._instantationMappings[typeFromHandle] == ServiceFactory.InstantiationModel.Singleton)
			{
				return (T)((object)this._singletonServices[typeFromHandle]);
			}
			return (T)((object)Activator.CreateInstance(ServiceFactory.GetServiceType<T>()));
		}

		// Token: 0x06000679 RID: 1657 RVA: 0x000117B0 File Offset: 0x0000F9B0
		private static Type GetServiceType<T>()
		{
			object @lock = ServiceFactory._lock;
			Type result;
			lock (@lock)
			{
				result = ServiceFactory._mappings[typeof(T)];
			}
			return result;
		}

		// Token: 0x0400029A RID: 666
		internal const string NotImplementedErrorMessage = "This functionality is not implemented in the portable version of this assembly. You should reference the AWSSDK.Core NuGet package from your main application project in order to reference the platform-specific implementation.";

		// Token: 0x0400029B RID: 667
		private static readonly object _lock = new object();

		// Token: 0x0400029C RID: 668
		private static bool _factoryInitialized = false;

		// Token: 0x0400029D RID: 669
		private static IDictionary<Type, Type> _mappings = new Dictionary<Type, Type>
		{
			{
				typeof(IApplicationSettings),
				typeof(ApplicationSettings)
			},
			{
				typeof(INetworkReachability),
				typeof(NetworkReachability)
			},
			{
				typeof(IApplicationInfo),
				typeof(ApplicationInfo)
			},
			{
				typeof(IEnvironmentInfo),
				typeof(EnvironmentInfo)
			}
		};

		// Token: 0x0400029E RID: 670
		private IDictionary<Type, ServiceFactory.InstantiationModel> _instantationMappings = new Dictionary<Type, ServiceFactory.InstantiationModel>
		{
			{
				typeof(IApplicationSettings),
				ServiceFactory.InstantiationModel.InstancePerCall
			},
			{
				typeof(INetworkReachability),
				ServiceFactory.InstantiationModel.Singleton
			},
			{
				typeof(IApplicationInfo),
				ServiceFactory.InstantiationModel.Singleton
			},
			{
				typeof(IEnvironmentInfo),
				ServiceFactory.InstantiationModel.Singleton
			}
		};

		// Token: 0x0400029F RID: 671
		private IDictionary<Type, object> _singletonServices = new Dictionary<Type, object>();

		// Token: 0x040002A0 RID: 672
		public static ServiceFactory Instance = new ServiceFactory();

		// Token: 0x020001D8 RID: 472
		private enum InstantiationModel
		{
			// Token: 0x040009A9 RID: 2473
			Singleton,
			// Token: 0x040009AA RID: 2474
			InstancePerCall
		}
	}
}
