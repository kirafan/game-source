﻿using System;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000090 RID: 144
	public interface IApplicationSettings
	{
		// Token: 0x06000665 RID: 1637
		void SetValue(string key, string value, ApplicationSettingsMode mode);

		// Token: 0x06000666 RID: 1638
		string GetValue(string key, ApplicationSettingsMode mode);

		// Token: 0x06000667 RID: 1639
		void RemoveValue(string key, ApplicationSettingsMode mode);
	}
}
