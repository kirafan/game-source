﻿using System;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000092 RID: 146
	public enum NetworkStatus
	{
		// Token: 0x04000296 RID: 662
		NotReachable,
		// Token: 0x04000297 RID: 663
		ReachableViaCarrierDataNetwork,
		// Token: 0x04000298 RID: 664
		ReachableViaWiFiNetwork
	}
}
