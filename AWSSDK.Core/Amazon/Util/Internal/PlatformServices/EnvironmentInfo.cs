﻿using System;
using System.Globalization;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000098 RID: 152
	public class EnvironmentInfo : IEnvironmentInfo
	{
		// Token: 0x06000684 RID: 1668 RVA: 0x00011900 File Offset: 0x0000FB00
		public EnvironmentInfo()
		{
			this.Platform = AmazonHookedPlatformInfo.Instance.Platform;
			this.PlatformVersion = AmazonHookedPlatformInfo.Instance.PlatformVersion;
			this.Model = AmazonHookedPlatformInfo.Instance.Model;
			this.Make = AmazonHookedPlatformInfo.Instance.Make;
			this.Locale = AmazonHookedPlatformInfo.Instance.Locale;
			this.FrameworkUserAgent = string.Format(CultureInfo.InvariantCulture, ".NET_Runtime/{0}.{1} UnityVersion/{2}", new object[]
			{
				Environment.Version.Major,
				Environment.Version.MajorRevision,
				AmazonHookedPlatformInfo.Instance.UnityVersion
			});
			this.PclPlatform = "Unity";
			this.PlatformUserAgent = string.Format("unity_{0}_{1}", this.Platform, this.PlatformVersion);
		}

		// Token: 0x170001E7 RID: 487
		// (get) Token: 0x06000685 RID: 1669 RVA: 0x000119D6 File Offset: 0x0000FBD6
		// (set) Token: 0x06000686 RID: 1670 RVA: 0x000119DE File Offset: 0x0000FBDE
		public string Platform { get; private set; }

		// Token: 0x170001E8 RID: 488
		// (get) Token: 0x06000687 RID: 1671 RVA: 0x000119E7 File Offset: 0x0000FBE7
		// (set) Token: 0x06000688 RID: 1672 RVA: 0x000119EF File Offset: 0x0000FBEF
		public string Model { get; private set; }

		// Token: 0x170001E9 RID: 489
		// (get) Token: 0x06000689 RID: 1673 RVA: 0x000119F8 File Offset: 0x0000FBF8
		// (set) Token: 0x0600068A RID: 1674 RVA: 0x00011A00 File Offset: 0x0000FC00
		public string Make { get; private set; }

		// Token: 0x170001EA RID: 490
		// (get) Token: 0x0600068B RID: 1675 RVA: 0x00011A09 File Offset: 0x0000FC09
		// (set) Token: 0x0600068C RID: 1676 RVA: 0x00011A11 File Offset: 0x0000FC11
		public string PlatformVersion { get; private set; }

		// Token: 0x170001EB RID: 491
		// (get) Token: 0x0600068D RID: 1677 RVA: 0x00011A1A File Offset: 0x0000FC1A
		// (set) Token: 0x0600068E RID: 1678 RVA: 0x00011A22 File Offset: 0x0000FC22
		public string Locale { get; private set; }

		// Token: 0x170001EC RID: 492
		// (get) Token: 0x0600068F RID: 1679 RVA: 0x00011A2B File Offset: 0x0000FC2B
		// (set) Token: 0x06000690 RID: 1680 RVA: 0x00011A33 File Offset: 0x0000FC33
		public string FrameworkUserAgent { get; private set; }

		// Token: 0x170001ED RID: 493
		// (get) Token: 0x06000691 RID: 1681 RVA: 0x00011A3C File Offset: 0x0000FC3C
		// (set) Token: 0x06000692 RID: 1682 RVA: 0x00011A44 File Offset: 0x0000FC44
		public string PclPlatform { get; private set; }

		// Token: 0x170001EE RID: 494
		// (get) Token: 0x06000693 RID: 1683 RVA: 0x00011A4D File Offset: 0x0000FC4D
		// (set) Token: 0x06000694 RID: 1684 RVA: 0x00011A55 File Offset: 0x0000FC55
		public string PlatformUserAgent { get; private set; }
	}
}
