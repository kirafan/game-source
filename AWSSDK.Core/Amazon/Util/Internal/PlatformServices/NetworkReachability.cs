﻿using System;
using Amazon.Util.Storage.Internal;
using UnityEngine;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000099 RID: 153
	public class NetworkReachability : INetworkReachability
	{
		// Token: 0x170001EF RID: 495
		// (get) Token: 0x06000695 RID: 1685 RVA: 0x00011A60 File Offset: 0x0000FC60
		public NetworkStatus NetworkStatus
		{
			get
			{
				NetworkReachability reachability = NetworkInfo.Reachability;
				if (reachability == NetworkReachability.ReachableViaCarrierDataNetwork)
				{
					return NetworkStatus.ReachableViaCarrierDataNetwork;
				}
				if (reachability == NetworkReachability.ReachableViaLocalAreaNetwork)
				{
					return NetworkStatus.ReachableViaWiFiNetwork;
				}
				return NetworkStatus.NotReachable;
			}
		}

		// Token: 0x1400001A RID: 26
		// (add) Token: 0x06000696 RID: 1686 RVA: 0x00011A80 File Offset: 0x0000FC80
		// (remove) Token: 0x06000697 RID: 1687 RVA: 0x00011ACC File Offset: 0x0000FCCC
		public event EventHandler<NetworkStatusEventArgs> NetworkReachabilityChanged
		{
			add
			{
				object obj = NetworkReachability.reachabilityChangedLock;
				lock (obj)
				{
					this.mNetworkReachabilityChanged = (EventHandler<NetworkStatusEventArgs>)Delegate.Combine(this.mNetworkReachabilityChanged, value);
				}
			}
			remove
			{
				object obj = NetworkReachability.reachabilityChangedLock;
				lock (obj)
				{
					this.mNetworkReachabilityChanged = (EventHandler<NetworkStatusEventArgs>)Delegate.Remove(this.mNetworkReachabilityChanged, value);
				}
			}
		}

		// Token: 0x06000698 RID: 1688 RVA: 0x00011B18 File Offset: 0x0000FD18
		internal void OnNetworkReachabilityChanged(NetworkStatus status)
		{
			EventHandler<NetworkStatusEventArgs> eventHandler = this.mNetworkReachabilityChanged;
			if (eventHandler != null)
			{
				eventHandler(null, new NetworkStatusEventArgs(status));
			}
		}

		// Token: 0x040002AA RID: 682
		internal EventHandler<NetworkStatusEventArgs> mNetworkReachabilityChanged;

		// Token: 0x040002AB RID: 683
		internal static readonly object reachabilityChangedLock = new object();
	}
}
