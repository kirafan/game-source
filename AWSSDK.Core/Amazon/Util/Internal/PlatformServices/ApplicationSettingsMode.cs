﻿using System;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x0200008F RID: 143
	public enum ApplicationSettingsMode
	{
		// Token: 0x04000292 RID: 658
		None,
		// Token: 0x04000293 RID: 659
		Local,
		// Token: 0x04000294 RID: 660
		Roaming
	}
}
