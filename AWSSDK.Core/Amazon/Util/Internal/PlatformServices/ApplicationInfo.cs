﻿using System;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000096 RID: 150
	public class ApplicationInfo : IApplicationInfo
	{
		// Token: 0x170001E3 RID: 483
		// (get) Token: 0x0600067B RID: 1659 RVA: 0x00011891 File Offset: 0x0000FA91
		public string AppTitle
		{
			get
			{
				return AmazonHookedPlatformInfo.Instance.Title;
			}
		}

		// Token: 0x170001E4 RID: 484
		// (get) Token: 0x0600067C RID: 1660 RVA: 0x0001189D File Offset: 0x0000FA9D
		public string AppVersionName
		{
			get
			{
				return AmazonHookedPlatformInfo.Instance.VersionName;
			}
		}

		// Token: 0x170001E5 RID: 485
		// (get) Token: 0x0600067D RID: 1661 RVA: 0x000118A9 File Offset: 0x0000FAA9
		public string AppVersionCode
		{
			get
			{
				return AmazonHookedPlatformInfo.Instance.VersionCode;
			}
		}

		// Token: 0x170001E6 RID: 486
		// (get) Token: 0x0600067E RID: 1662 RVA: 0x000118B5 File Offset: 0x0000FAB5
		public string PackageName
		{
			get
			{
				return AmazonHookedPlatformInfo.Instance.PackageName;
			}
		}
	}
}
