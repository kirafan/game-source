﻿using System;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000093 RID: 147
	public class NetworkStatusEventArgs : EventArgs
	{
		// Token: 0x170001E1 RID: 481
		// (get) Token: 0x06000670 RID: 1648 RVA: 0x00011610 File Offset: 0x0000F810
		// (set) Token: 0x06000671 RID: 1649 RVA: 0x00011618 File Offset: 0x0000F818
		public NetworkStatus Status { get; private set; }

		// Token: 0x06000672 RID: 1650 RVA: 0x00011621 File Offset: 0x0000F821
		public NetworkStatusEventArgs(NetworkStatus status)
		{
			this.Status = status;
		}
	}
}
