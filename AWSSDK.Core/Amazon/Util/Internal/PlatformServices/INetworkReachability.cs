﻿using System;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000094 RID: 148
	public interface INetworkReachability
	{
		// Token: 0x170001E2 RID: 482
		// (get) Token: 0x06000673 RID: 1651
		NetworkStatus NetworkStatus { get; }

		// Token: 0x14000019 RID: 25
		// (add) Token: 0x06000674 RID: 1652
		// (remove) Token: 0x06000675 RID: 1653
		event EventHandler<NetworkStatusEventArgs> NetworkReachabilityChanged;
	}
}
