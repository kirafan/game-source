﻿using System;
using Amazon.Util.Storage;
using Amazon.Util.Storage.Internal;

namespace Amazon.Util.Internal.PlatformServices
{
	// Token: 0x02000097 RID: 151
	public class ApplicationSettings : IApplicationSettings
	{
		// Token: 0x06000680 RID: 1664 RVA: 0x000118C1 File Offset: 0x0000FAC1
		public ApplicationSettings()
		{
			this.kvStore = new PlayerPreferenceKVStore();
		}

		// Token: 0x06000681 RID: 1665 RVA: 0x000118D4 File Offset: 0x0000FAD4
		public void SetValue(string key, string value, ApplicationSettingsMode mode)
		{
			this.kvStore.Put(key, value);
		}

		// Token: 0x06000682 RID: 1666 RVA: 0x000118E3 File Offset: 0x0000FAE3
		public string GetValue(string key, ApplicationSettingsMode mode)
		{
			return this.kvStore.Get(key);
		}

		// Token: 0x06000683 RID: 1667 RVA: 0x000118F1 File Offset: 0x0000FAF1
		public void RemoveValue(string key, ApplicationSettingsMode mode)
		{
			this.kvStore.Clear(key);
		}

		// Token: 0x040002A1 RID: 673
		private KVStore kvStore;
	}
}
