﻿using System;

namespace Amazon.Util.Internal
{
	// Token: 0x0200008B RID: 139
	public abstract class ConfigurationElement
	{
		// Token: 0x06000659 RID: 1625 RVA: 0x000115C0 File Offset: 0x0000F7C0
		public ConfigurationElement()
		{
			this.ElementInformation = new ElementInformation(true);
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x0600065A RID: 1626 RVA: 0x000115D4 File Offset: 0x0000F7D4
		// (set) Token: 0x0600065B RID: 1627 RVA: 0x000115DC File Offset: 0x0000F7DC
		public ElementInformation ElementInformation { get; set; }
	}
}
