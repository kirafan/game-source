﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using System.Threading;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Util;
using Amazon.Util.Internal.PlatformServices;
using UnityEngine;

namespace Amazon.Util.Internal
{
	// Token: 0x02000085 RID: 133
	public static class InternalSDKUtils
	{
		// Token: 0x060005E0 RID: 1504 RVA: 0x00010563 File Offset: 0x0000E763
		public static void SetUserAgent(string productName, string versionNumber)
		{
			InternalSDKUtils.SetUserAgent(productName, versionNumber, null);
		}

		// Token: 0x060005E1 RID: 1505 RVA: 0x0001056D File Offset: 0x0000E76D
		public static void SetUserAgent(string productName, string versionNumber, string customData)
		{
			InternalSDKUtils._userAgentBaseName = productName;
			InternalSDKUtils._versionNumber = versionNumber;
			InternalSDKUtils._customData = customData;
			InternalSDKUtils.BuildCustomUserAgentString();
		}

		// Token: 0x060005E2 RID: 1506 RVA: 0x00010588 File Offset: 0x0000E788
		private static void BuildCustomUserAgentString()
		{
			if (InternalSDKUtils._versionNumber == null)
			{
				InternalSDKUtils._versionNumber = "3.3.14.0";
			}
			IEnvironmentInfo service = ServiceFactory.Instance.GetService<IEnvironmentInfo>();
			string text = "";
			if (string.IsNullOrEmpty(text))
			{
				InternalSDKUtils._customSdkUserAgent = string.Format(CultureInfo.InvariantCulture, "{0}/{1} {2} OS/{3} {4}", new object[]
				{
					InternalSDKUtils._userAgentBaseName,
					InternalSDKUtils._versionNumber,
					service.FrameworkUserAgent,
					service.PlatformUserAgent,
					InternalSDKUtils._customData
				}).Trim();
				return;
			}
			InternalSDKUtils._customSdkUserAgent = string.Format(CultureInfo.InvariantCulture, "{0}/{1} {2} OS/{3} {4} {5}", new object[]
			{
				InternalSDKUtils._userAgentBaseName,
				InternalSDKUtils._versionNumber,
				service.FrameworkUserAgent,
				service.PlatformUserAgent,
				text,
				InternalSDKUtils._customData
			}).Trim();
		}

		// Token: 0x060005E3 RID: 1507 RVA: 0x00010658 File Offset: 0x0000E858
		public static string BuildUserAgentString(string serviceSdkVersion)
		{
			if (!string.IsNullOrEmpty(InternalSDKUtils._customSdkUserAgent))
			{
				return InternalSDKUtils._customSdkUserAgent;
			}
			IEnvironmentInfo service = ServiceFactory.Instance.GetService<IEnvironmentInfo>();
			return string.Format(CultureInfo.InvariantCulture, "{0}/{1} aws-sdk-core/{2} {3} OS/{4} {5}", new object[]
			{
				InternalSDKUtils._userAgentBaseName,
				serviceSdkVersion,
				"3.3.14.0",
				service.FrameworkUserAgent,
				service.PlatformUserAgent,
				InternalSDKUtils._customData
			}).Trim();
		}

		// Token: 0x060005E4 RID: 1508 RVA: 0x000106CC File Offset: 0x0000E8CC
		public static void ApplyValues(object target, IDictionary<string, object> propertyValues)
		{
			if (propertyValues == null || propertyValues.Count == 0)
			{
				return;
			}
			ITypeInfo typeInfo = TypeFactory.GetTypeInfo(target.GetType());
			foreach (KeyValuePair<string, object> keyValuePair in propertyValues)
			{
				PropertyInfo property = typeInfo.GetProperty(keyValuePair.Key);
				if (property == null)
				{
					throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Unable to find property {0} on type {1}.", new object[]
					{
						keyValuePair.Key,
						typeInfo.FullName
					}));
				}
				try
				{
					if (TypeFactory.GetTypeInfo(property.PropertyType).IsEnum)
					{
						object value = Enum.Parse(property.PropertyType, keyValuePair.Value.ToString(), true);
						property.SetValue(target, value, null);
					}
					else
					{
						property.SetValue(target, keyValuePair.Value, null);
					}
				}
				catch (Exception ex)
				{
					throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, "Unable to set property {0} on type {1}: {2}", new object[]
					{
						keyValuePair.Key,
						typeInfo.FullName,
						ex.Message
					}));
				}
			}
		}

		// Token: 0x060005E5 RID: 1509 RVA: 0x000107FC File Offset: 0x0000E9FC
		public static void AddToDictionary<TKey, TValue>(Dictionary<TKey, TValue> dictionary, TKey key, TValue value)
		{
			if (dictionary.ContainsKey(key))
			{
				throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Dictionary already contains item with key {0}", new object[]
				{
					key
				}));
			}
			dictionary[key] = value;
		}

		// Token: 0x060005E6 RID: 1510 RVA: 0x00010834 File Offset: 0x0000EA34
		public static void FillDictionary<T, TKey, TValue>(IEnumerable<T> items, Func<T, TKey> keyGenerator, Func<T, TValue> valueGenerator, Dictionary<TKey, TValue> targetDictionary)
		{
			foreach (T arg in items)
			{
				TKey key = keyGenerator(arg);
				TValue value = valueGenerator(arg);
				InternalSDKUtils.AddToDictionary<TKey, TValue>(targetDictionary, key, value);
			}
		}

		// Token: 0x060005E7 RID: 1511 RVA: 0x00010890 File Offset: 0x0000EA90
		public static Dictionary<TKey, TValue> ToDictionary<T, TKey, TValue>(IEnumerable<T> items, Func<T, TKey> keyGenerator, Func<T, TValue> valueGenerator)
		{
			return InternalSDKUtils.ToDictionary<T, TKey, TValue>(items, keyGenerator, valueGenerator, null);
		}

		// Token: 0x060005E8 RID: 1512 RVA: 0x0001089C File Offset: 0x0000EA9C
		public static Dictionary<TKey, TValue> ToDictionary<T, TKey, TValue>(IEnumerable<T> items, Func<T, TKey> keyGenerator, Func<T, TValue> valueGenerator, IEqualityComparer<TKey> comparer)
		{
			Dictionary<TKey, TValue> dictionary;
			if (comparer == null)
			{
				dictionary = new Dictionary<TKey, TValue>();
			}
			else
			{
				dictionary = new Dictionary<TKey, TValue>(comparer);
			}
			InternalSDKUtils.FillDictionary<T, TKey, TValue>(items, keyGenerator, valueGenerator, dictionary);
			return dictionary;
		}

		// Token: 0x060005E9 RID: 1513 RVA: 0x000108C8 File Offset: 0x0000EAC8
		public static bool TryFindByValue<TKey, TValue>(IDictionary<TKey, TValue> dictionary, TValue value, IEqualityComparer<TValue> valueComparer, out TKey key)
		{
			foreach (KeyValuePair<TKey, TValue> keyValuePair in dictionary)
			{
				TValue value2 = keyValuePair.Value;
				if (valueComparer.Equals(value, value2))
				{
					key = keyValuePair.Key;
					return true;
				}
			}
			key = default(TKey);
			return false;
		}

		// Token: 0x060005EA RID: 1514 RVA: 0x00010938 File Offset: 0x0000EB38
		public static void SetIsSet<T>(bool isSet, ref T? field) where T : struct
		{
			if (isSet)
			{
				field = new T?(default(T));
				return;
			}
			field = null;
		}

		// Token: 0x060005EB RID: 1515 RVA: 0x00010964 File Offset: 0x0000EB64
		public static void SetIsSet<T>(bool isSet, ref List<T> field)
		{
			if (isSet)
			{
				field = new AlwaysSendList<T>(field);
				return;
			}
			field = new List<T>();
		}

		// Token: 0x060005EC RID: 1516 RVA: 0x0001097A File Offset: 0x0000EB7A
		public static void SetIsSet<TKey, TValue>(bool isSet, ref Dictionary<TKey, TValue> field)
		{
			if (isSet)
			{
				field = new AlwaysSendDictionary<TKey, TValue>(field);
				return;
			}
			field = new Dictionary<TKey, TValue>();
		}

		// Token: 0x060005ED RID: 1517 RVA: 0x00010990 File Offset: 0x0000EB90
		public static bool GetIsSet<T>(T? field) where T : struct
		{
			return field != null;
		}

		// Token: 0x060005EE RID: 1518 RVA: 0x00010999 File Offset: 0x0000EB99
		public static bool GetIsSet<T>(List<T> field)
		{
			return field != null && (field.Count > 0 || field is AlwaysSendList<T>);
		}

		// Token: 0x060005EF RID: 1519 RVA: 0x000109B6 File Offset: 0x0000EBB6
		public static bool GetIsSet<TKey, TVvalue>(Dictionary<TKey, TVvalue> field)
		{
			return field != null && (field.Count > 0 || field is AlwaysSendDictionary<TKey, TVvalue>);
		}

		// Token: 0x060005F0 RID: 1520 RVA: 0x000109D4 File Offset: 0x0000EBD4
		public static string GetMonoRuntimeVersion()
		{
			Type type = Type.GetType("Mono.Runtime");
			if (type != null)
			{
				MethodInfo method = type.GetMethod("GetDisplayName");
				if (method != null)
				{
					string text = (string)method.Invoke(null, null);
					text = text.Replace("/", ":").Replace(" ", string.Empty);
					return "Mono/" + text;
				}
			}
			return "Mono/Unknown";
		}

		// Token: 0x170001AA RID: 426
		// (get) Token: 0x060005F1 RID: 1521 RVA: 0x00010A3D File Offset: 0x0000EC3D
		public static bool IsAndroid
		{
			get
			{
				return Application.platform == RuntimePlatform.Android;
			}
		}

		// Token: 0x170001AB RID: 427
		// (get) Token: 0x060005F2 RID: 1522 RVA: 0x00010A48 File Offset: 0x0000EC48
		public static bool IsiOS
		{
			get
			{
				return Application.platform == RuntimePlatform.IPhonePlayer;
			}
		}

		// Token: 0x060005F3 RID: 1523 RVA: 0x00010A52 File Offset: 0x0000EC52
		internal static Type GetTypeFromUnityEngine(string typeName)
		{
			return Type.GetType(string.Format("UnityEngine.{0}, UnityEngine", typeName));
		}

		// Token: 0x060005F4 RID: 1524 RVA: 0x00010A64 File Offset: 0x0000EC64
		public static void AsyncExecutor(Action action, AsyncOptions options)
		{
			if (options.ExecuteCallbackOnMainThread)
			{
				if (UnityInitializer.IsMainThread())
				{
					InternalSDKUtils.SafeExecute(action);
					return;
				}
				UnityRequestQueue.Instance.ExecuteOnMainThread(action);
				return;
			}
			else
			{
				if (!UnityInitializer.IsMainThread())
				{
					InternalSDKUtils.SafeExecute(action);
					return;
				}
				ThreadPool.QueueUserWorkItem(delegate(object state)
				{
					InternalSDKUtils.SafeExecute(action);
				});
				return;
			}
		}

		// Token: 0x060005F5 RID: 1525 RVA: 0x00010AD0 File Offset: 0x0000ECD0
		public static void SafeExecute(Action action)
		{
			try
			{
				action();
			}
			catch (Exception exception)
			{
				InternalSDKUtils._logger.Error(exception, "An unhandled exception was thrown from the callback method {0}.", new object[]
				{
					action.Method.Name
				});
			}
		}

		// Token: 0x0400026B RID: 619
		internal const string UnknownVersion = "Unknown";

		// Token: 0x0400026C RID: 620
		internal const string UnknownNetFrameworkVersion = ".NET_Runtime/Unknown .NET_Framework/Unknown";

		// Token: 0x0400026D RID: 621
		private static string _versionNumber;

		// Token: 0x0400026E RID: 622
		private static string _customSdkUserAgent;

		// Token: 0x0400026F RID: 623
		private static string _customData;

		// Token: 0x04000270 RID: 624
		internal const string CoreVersionNumber = "3.3.14.0";

		// Token: 0x04000271 RID: 625
		private static string _userAgentBaseName = "aws-sdk-unity";

		// Token: 0x04000272 RID: 626
		private const string UnknownMonoVersion = "Mono/Unknown";

		// Token: 0x04000273 RID: 627
		private static Amazon.Runtime.Internal.Util.Logger _logger = Amazon.Runtime.Internal.Util.Logger.GetLogger(typeof(InternalSDKUtils));
	}
}
