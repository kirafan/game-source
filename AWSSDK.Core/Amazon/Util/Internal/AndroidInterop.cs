﻿using System;
using System.Linq;
using System.Reflection;

namespace Amazon.Util.Internal
{
	// Token: 0x02000089 RID: 137
	public class AndroidInterop
	{
		// Token: 0x06000636 RID: 1590 RVA: 0x00010DC4 File Offset: 0x0000EFC4
		public static T CallStaticJavaMethod<T>(string className, string methodName, params object[] parameters)
		{
			Type typeFromUnityEngine = InternalSDKUtils.GetTypeFromUnityEngine("AndroidJavaClass");
			if (typeFromUnityEngine != null)
			{
				object obj = Activator.CreateInstance(typeFromUnityEngine, new object[]
				{
					className
				});
				MethodInfo methodInfo = (from x in typeFromUnityEngine.GetMethods()
				where x.Name == "CallStatic"
				select x).FirstOrDefault<MethodInfo>();
				if (methodInfo != null)
				{
					return (T)((object)methodInfo.Invoke(obj, new object[]
					{
						methodName,
						parameters
					}));
				}
			}
			return default(T);
		}

		// Token: 0x06000637 RID: 1591 RVA: 0x00010E48 File Offset: 0x0000F048
		public static object GetJavaObjectStatically(string className, string methodName)
		{
			Type typeFromUnityEngine = InternalSDKUtils.GetTypeFromUnityEngine("AndroidJavaClass");
			Type typeFromUnityEngine2 = InternalSDKUtils.GetTypeFromUnityEngine("AndroidJavaObject");
			if (typeFromUnityEngine != null)
			{
				object obj = Activator.CreateInstance(typeFromUnityEngine, new object[]
				{
					className
				});
				return (from x in typeFromUnityEngine.GetMethods()
				where x.Name == "CallStatic"
				select x).First((MethodInfo x) => x.ContainsGenericParameters).MakeGenericMethod(new Type[]
				{
					typeFromUnityEngine2
				}).Invoke(obj, new object[]
				{
					methodName,
					new object[0]
				});
			}
			return null;
		}

		// Token: 0x06000638 RID: 1592 RVA: 0x00010EFC File Offset: 0x0000F0FC
		public static T CallMethod<T>(object androidJavaObject, string methodName, params object[] parameters)
		{
			return (T)((object)(from x in androidJavaObject.GetType().GetMethods()
			where x.Name == "Call"
			select x).First((MethodInfo x) => x.ContainsGenericParameters).MakeGenericMethod(new Type[]
			{
				typeof(T)
			}).Invoke(androidJavaObject, new object[]
			{
				methodName,
				parameters
			}));
		}

		// Token: 0x06000639 RID: 1593 RVA: 0x00010F90 File Offset: 0x0000F190
		public static object CallMethod(object androidJavaObject, string methodName, params object[] parameters)
		{
			Type typeFromUnityEngine = InternalSDKUtils.GetTypeFromUnityEngine("AndroidJavaObject");
			return (from x in androidJavaObject.GetType().GetMethods()
			where x.Name == "Call"
			select x).First((MethodInfo x) => x.ContainsGenericParameters).MakeGenericMethod(new Type[]
			{
				typeFromUnityEngine
			}).Invoke(androidJavaObject, new object[]
			{
				methodName,
				parameters
			});
		}

		// Token: 0x0600063A RID: 1594 RVA: 0x00011020 File Offset: 0x0000F220
		public static T GetStaticJavaField<T>(string className, string methodName)
		{
			Type typeFromUnityEngine = InternalSDKUtils.GetTypeFromUnityEngine("AndroidJavaClass");
			if (typeFromUnityEngine != null)
			{
				object obj = Activator.CreateInstance(typeFromUnityEngine, new object[]
				{
					className
				});
				MethodInfo methodInfo = typeFromUnityEngine.GetMethod("GetStatic").MakeGenericMethod(new Type[]
				{
					typeof(T)
				});
				if (methodInfo != null)
				{
					return (T)((object)methodInfo.Invoke(obj, new object[]
					{
						methodName
					}));
				}
			}
			return default(T);
		}

		// Token: 0x0600063B RID: 1595 RVA: 0x00011094 File Offset: 0x0000F294
		public static object GetStaticJavaField(string className, string methodName)
		{
			Type typeFromUnityEngine = InternalSDKUtils.GetTypeFromUnityEngine("AndroidJavaClass");
			Type typeFromUnityEngine2 = InternalSDKUtils.GetTypeFromUnityEngine("AndroidJavaObject");
			if (typeFromUnityEngine != null)
			{
				object obj = Activator.CreateInstance(typeFromUnityEngine, new object[]
				{
					className
				});
				MethodInfo methodInfo = typeFromUnityEngine.GetMethod("GetStatic").MakeGenericMethod(new Type[]
				{
					typeFromUnityEngine2
				});
				if (methodInfo != null)
				{
					return methodInfo.Invoke(obj, new object[]
					{
						methodName
					});
				}
			}
			return null;
		}

		// Token: 0x0600063C RID: 1596 RVA: 0x000110FC File Offset: 0x0000F2FC
		public static T GetJavaField<T>(object androidJavaObject, string methodName)
		{
			return (T)((object)(from x in androidJavaObject.GetType().GetMethods()
			where x.Name == "Get"
			select x).First((MethodInfo x) => x.ContainsGenericParameters).MakeGenericMethod(new Type[]
			{
				typeof(T)
			}).Invoke(androidJavaObject, new object[]
			{
				methodName
			}));
		}

		// Token: 0x0600063D RID: 1597 RVA: 0x00011189 File Offset: 0x0000F389
		public static object GetAndroidContext()
		{
			return AndroidInterop.GetStaticJavaField("com.unity3d.player.UnityPlayer", "currentActivity");
		}
	}
}
