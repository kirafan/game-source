﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Amazon.Util.Internal
{
	// Token: 0x02000088 RID: 136
	public static class TypeFactory
	{
		// Token: 0x06000634 RID: 1588 RVA: 0x00010DA8 File Offset: 0x0000EFA8
		public static ITypeInfo GetTypeInfo(Type type)
		{
			if (type == null)
			{
				return null;
			}
			return new TypeFactory.TypeInfoWrapper(type);
		}

		// Token: 0x0400027F RID: 639
		public static readonly ITypeInfo[] EmptyTypes = new ITypeInfo[0];

		// Token: 0x020001D2 RID: 466
		private abstract class AbstractTypeInfo : ITypeInfo
		{
			// Token: 0x06000EF0 RID: 3824 RVA: 0x000270E8 File Offset: 0x000252E8
			internal AbstractTypeInfo(Type type)
			{
				this._type = type;
			}

			// Token: 0x1700041C RID: 1052
			// (get) Token: 0x06000EF1 RID: 3825 RVA: 0x000270F7 File Offset: 0x000252F7
			public Type Type
			{
				get
				{
					return this._type;
				}
			}

			// Token: 0x06000EF2 RID: 3826 RVA: 0x000270FF File Offset: 0x000252FF
			public override int GetHashCode()
			{
				return this._type.GetHashCode();
			}

			// Token: 0x06000EF3 RID: 3827 RVA: 0x0002710C File Offset: 0x0002530C
			public override bool Equals(object obj)
			{
				TypeFactory.AbstractTypeInfo abstractTypeInfo = obj as TypeFactory.AbstractTypeInfo;
				return abstractTypeInfo != null && this._type.Equals(abstractTypeInfo._type);
			}

			// Token: 0x06000EF4 RID: 3828 RVA: 0x00027136 File Offset: 0x00025336
			public bool IsType(Type type)
			{
				return this._type == type;
			}

			// Token: 0x1700041D RID: 1053
			// (get) Token: 0x06000EF5 RID: 3829
			public abstract Type BaseType { get; }

			// Token: 0x1700041E RID: 1054
			// (get) Token: 0x06000EF6 RID: 3830
			public abstract Assembly Assembly { get; }

			// Token: 0x06000EF7 RID: 3831
			public abstract Type GetInterface(string name);

			// Token: 0x06000EF8 RID: 3832
			public abstract Type[] GetInterfaces();

			// Token: 0x06000EF9 RID: 3833
			public abstract IEnumerable<PropertyInfo> GetProperties();

			// Token: 0x06000EFA RID: 3834
			public abstract IEnumerable<FieldInfo> GetFields();

			// Token: 0x06000EFB RID: 3835
			public abstract FieldInfo GetField(string name);

			// Token: 0x06000EFC RID: 3836
			public abstract MethodInfo GetMethod(string name);

			// Token: 0x06000EFD RID: 3837
			public abstract MethodInfo GetMethod(string name, ITypeInfo[] paramTypes);

			// Token: 0x06000EFE RID: 3838
			public abstract MemberInfo[] GetMembers();

			// Token: 0x06000EFF RID: 3839
			public abstract PropertyInfo GetProperty(string name);

			// Token: 0x06000F00 RID: 3840
			public abstract bool IsAssignableFrom(ITypeInfo typeInfo);

			// Token: 0x1700041F RID: 1055
			// (get) Token: 0x06000F01 RID: 3841
			public abstract bool IsClass { get; }

			// Token: 0x17000420 RID: 1056
			// (get) Token: 0x06000F02 RID: 3842
			public abstract bool IsInterface { get; }

			// Token: 0x17000421 RID: 1057
			// (get) Token: 0x06000F03 RID: 3843
			public abstract bool IsAbstract { get; }

			// Token: 0x17000422 RID: 1058
			// (get) Token: 0x06000F04 RID: 3844
			public abstract bool IsSealed { get; }

			// Token: 0x17000423 RID: 1059
			// (get) Token: 0x06000F05 RID: 3845
			public abstract bool IsEnum { get; }

			// Token: 0x17000424 RID: 1060
			// (get) Token: 0x06000F06 RID: 3846
			public abstract bool IsValueType { get; }

			// Token: 0x06000F07 RID: 3847
			public abstract ConstructorInfo GetConstructor(ITypeInfo[] paramTypes);

			// Token: 0x06000F08 RID: 3848
			public abstract object[] GetCustomAttributes(bool inherit);

			// Token: 0x06000F09 RID: 3849
			public abstract object[] GetCustomAttributes(ITypeInfo attributeType, bool inherit);

			// Token: 0x17000425 RID: 1061
			// (get) Token: 0x06000F0A RID: 3850
			public abstract bool ContainsGenericParameters { get; }

			// Token: 0x17000426 RID: 1062
			// (get) Token: 0x06000F0B RID: 3851
			public abstract bool IsGenericTypeDefinition { get; }

			// Token: 0x17000427 RID: 1063
			// (get) Token: 0x06000F0C RID: 3852
			public abstract bool IsGenericType { get; }

			// Token: 0x06000F0D RID: 3853
			public abstract Type GetGenericTypeDefinition();

			// Token: 0x06000F0E RID: 3854
			public abstract Type[] GetGenericArguments();

			// Token: 0x17000428 RID: 1064
			// (get) Token: 0x06000F0F RID: 3855 RVA: 0x00027141 File Offset: 0x00025341
			public bool IsArray
			{
				get
				{
					return this._type.IsArray;
				}
			}

			// Token: 0x06000F10 RID: 3856 RVA: 0x0002714E File Offset: 0x0002534E
			public object EnumToObject(object value)
			{
				return Enum.ToObject(this._type, value);
			}

			// Token: 0x06000F11 RID: 3857 RVA: 0x0002715C File Offset: 0x0002535C
			public ITypeInfo EnumGetUnderlyingType()
			{
				return TypeFactory.GetTypeInfo(Enum.GetUnderlyingType(this._type));
			}

			// Token: 0x06000F12 RID: 3858 RVA: 0x0002716E File Offset: 0x0002536E
			public object CreateInstance()
			{
				return Activator.CreateInstance(this._type);
			}

			// Token: 0x06000F13 RID: 3859 RVA: 0x0002717B File Offset: 0x0002537B
			public Array ArrayCreateInstance(int length)
			{
				return Array.CreateInstance(this._type, length);
			}

			// Token: 0x06000F14 RID: 3860 RVA: 0x00027189 File Offset: 0x00025389
			public ITypeInfo GetElementType()
			{
				return TypeFactory.GetTypeInfo(this._type.GetElementType());
			}

			// Token: 0x17000429 RID: 1065
			// (get) Token: 0x06000F15 RID: 3861 RVA: 0x0002719B File Offset: 0x0002539B
			public string FullName
			{
				get
				{
					return this._type.FullName;
				}
			}

			// Token: 0x1700042A RID: 1066
			// (get) Token: 0x06000F16 RID: 3862 RVA: 0x000271A8 File Offset: 0x000253A8
			public string Name
			{
				get
				{
					return this._type.Name;
				}
			}

			// Token: 0x0400099A RID: 2458
			protected Type _type;
		}

		// Token: 0x020001D3 RID: 467
		private class TypeInfoWrapper : TypeFactory.AbstractTypeInfo
		{
			// Token: 0x06000F17 RID: 3863 RVA: 0x000271B5 File Offset: 0x000253B5
			internal TypeInfoWrapper(Type type) : base(type)
			{
			}

			// Token: 0x1700042B RID: 1067
			// (get) Token: 0x06000F18 RID: 3864 RVA: 0x000271BE File Offset: 0x000253BE
			public override Type BaseType
			{
				get
				{
					return this._type.BaseType;
				}
			}

			// Token: 0x06000F19 RID: 3865 RVA: 0x000271CB File Offset: 0x000253CB
			public override FieldInfo GetField(string name)
			{
				return this._type.GetField(name);
			}

			// Token: 0x06000F1A RID: 3866 RVA: 0x000271DC File Offset: 0x000253DC
			public override Type GetInterface(string name)
			{
				return this._type.GetInterfaces().FirstOrDefault((Type x) => x.Namespace + "." + x.Name == name);
			}

			// Token: 0x06000F1B RID: 3867 RVA: 0x00027212 File Offset: 0x00025412
			public override Type[] GetInterfaces()
			{
				return this._type.GetInterfaces();
			}

			// Token: 0x06000F1C RID: 3868 RVA: 0x0002721F File Offset: 0x0002541F
			public override IEnumerable<PropertyInfo> GetProperties()
			{
				return this._type.GetProperties();
			}

			// Token: 0x06000F1D RID: 3869 RVA: 0x0002722C File Offset: 0x0002542C
			public override IEnumerable<FieldInfo> GetFields()
			{
				return this._type.GetFields();
			}

			// Token: 0x06000F1E RID: 3870 RVA: 0x00027239 File Offset: 0x00025439
			public override MemberInfo[] GetMembers()
			{
				return this._type.GetMembers();
			}

			// Token: 0x1700042C RID: 1068
			// (get) Token: 0x06000F1F RID: 3871 RVA: 0x00027246 File Offset: 0x00025446
			public override bool IsClass
			{
				get
				{
					return this._type.IsClass;
				}
			}

			// Token: 0x1700042D RID: 1069
			// (get) Token: 0x06000F20 RID: 3872 RVA: 0x00027253 File Offset: 0x00025453
			public override bool IsValueType
			{
				get
				{
					return this._type.IsValueType;
				}
			}

			// Token: 0x1700042E RID: 1070
			// (get) Token: 0x06000F21 RID: 3873 RVA: 0x00027260 File Offset: 0x00025460
			public override bool IsInterface
			{
				get
				{
					return this._type.IsInterface;
				}
			}

			// Token: 0x1700042F RID: 1071
			// (get) Token: 0x06000F22 RID: 3874 RVA: 0x0002726D File Offset: 0x0002546D
			public override bool IsAbstract
			{
				get
				{
					return this._type.IsAbstract;
				}
			}

			// Token: 0x17000430 RID: 1072
			// (get) Token: 0x06000F23 RID: 3875 RVA: 0x0002727A File Offset: 0x0002547A
			public override bool IsSealed
			{
				get
				{
					return this._type.IsSealed;
				}
			}

			// Token: 0x17000431 RID: 1073
			// (get) Token: 0x06000F24 RID: 3876 RVA: 0x00027287 File Offset: 0x00025487
			public override bool IsEnum
			{
				get
				{
					return this._type.IsEnum;
				}
			}

			// Token: 0x06000F25 RID: 3877 RVA: 0x00027294 File Offset: 0x00025494
			public override MethodInfo GetMethod(string name)
			{
				return this._type.GetMethod(name);
			}

			// Token: 0x06000F26 RID: 3878 RVA: 0x000272A4 File Offset: 0x000254A4
			public override MethodInfo GetMethod(string name, ITypeInfo[] paramTypes)
			{
				Type[] array = new Type[paramTypes.Length];
				for (int i = 0; i < paramTypes.Length; i++)
				{
					array[i] = ((TypeFactory.AbstractTypeInfo)paramTypes[i]).Type;
				}
				return this._type.GetMethod(name, array);
			}

			// Token: 0x06000F27 RID: 3879 RVA: 0x000272E8 File Offset: 0x000254E8
			public override ConstructorInfo GetConstructor(ITypeInfo[] paramTypes)
			{
				Type[] array = new Type[paramTypes.Length];
				for (int i = 0; i < paramTypes.Length; i++)
				{
					array[i] = ((TypeFactory.AbstractTypeInfo)paramTypes[i]).Type;
				}
				return this._type.GetConstructor(array);
			}

			// Token: 0x06000F28 RID: 3880 RVA: 0x00027328 File Offset: 0x00025528
			public override PropertyInfo GetProperty(string name)
			{
				return this._type.GetProperty(name);
			}

			// Token: 0x06000F29 RID: 3881 RVA: 0x00027336 File Offset: 0x00025536
			public override bool IsAssignableFrom(ITypeInfo typeInfo)
			{
				return this._type.IsAssignableFrom(((TypeFactory.AbstractTypeInfo)typeInfo).Type);
			}

			// Token: 0x17000432 RID: 1074
			// (get) Token: 0x06000F2A RID: 3882 RVA: 0x0002734E File Offset: 0x0002554E
			public override bool ContainsGenericParameters
			{
				get
				{
					return this._type.ContainsGenericParameters;
				}
			}

			// Token: 0x17000433 RID: 1075
			// (get) Token: 0x06000F2B RID: 3883 RVA: 0x0002735B File Offset: 0x0002555B
			public override bool IsGenericTypeDefinition
			{
				get
				{
					return this._type.IsGenericTypeDefinition;
				}
			}

			// Token: 0x17000434 RID: 1076
			// (get) Token: 0x06000F2C RID: 3884 RVA: 0x00027368 File Offset: 0x00025568
			public override bool IsGenericType
			{
				get
				{
					return this._type.IsGenericType;
				}
			}

			// Token: 0x06000F2D RID: 3885 RVA: 0x00027375 File Offset: 0x00025575
			public override Type GetGenericTypeDefinition()
			{
				return this._type.GetGenericTypeDefinition();
			}

			// Token: 0x06000F2E RID: 3886 RVA: 0x00027382 File Offset: 0x00025582
			public override Type[] GetGenericArguments()
			{
				return this._type.GetGenericArguments();
			}

			// Token: 0x06000F2F RID: 3887 RVA: 0x0002738F File Offset: 0x0002558F
			public override object[] GetCustomAttributes(bool inherit)
			{
				return this._type.GetCustomAttributes(inherit);
			}

			// Token: 0x06000F30 RID: 3888 RVA: 0x0002739D File Offset: 0x0002559D
			public override object[] GetCustomAttributes(ITypeInfo attributeType, bool inherit)
			{
				return this._type.GetCustomAttributes(((TypeFactory.TypeInfoWrapper)attributeType)._type, inherit);
			}

			// Token: 0x17000435 RID: 1077
			// (get) Token: 0x06000F31 RID: 3889 RVA: 0x000273B6 File Offset: 0x000255B6
			public override Assembly Assembly
			{
				get
				{
					return this._type.Assembly;
				}
			}
		}
	}
}
