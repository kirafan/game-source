﻿using System;
using Amazon.Runtime.Internal.Util;
using ThirdParty.iOS4Unity;
using UnityEngine;

namespace Amazon.Util.Internal
{
	// Token: 0x0200008A RID: 138
	public class AmazonHookedPlatformInfo
	{
		// Token: 0x0600063F RID: 1599 RVA: 0x0000584A File Offset: 0x00003A4A
		private AmazonHookedPlatformInfo()
		{
		}

		// Token: 0x170001C6 RID: 454
		// (get) Token: 0x06000640 RID: 1600 RVA: 0x0001119A File Offset: 0x0000F39A
		// (set) Token: 0x06000641 RID: 1601 RVA: 0x000111A4 File Offset: 0x0000F3A4
		public string Platform
		{
			get
			{
				return this.device_platform;
			}
			internal set
			{
				if (value.Equals(RuntimePlatform.IPhonePlayer.ToString(), StringComparison.OrdinalIgnoreCase) || value.Contains("iPhoneOS") || value.Contains("iPhone"))
				{
					this.device_platform = "iPhone OS";
					return;
				}
				if (value.Equals(RuntimePlatform.Android.ToString(), StringComparison.OrdinalIgnoreCase) || value.Contains("Android") || value.Contains("android"))
				{
					this.device_platform = "Android";
					return;
				}
				this.device_platform = value;
			}
		}

		// Token: 0x170001C7 RID: 455
		// (get) Token: 0x06000642 RID: 1602 RVA: 0x00011237 File Offset: 0x0000F437
		// (set) Token: 0x06000643 RID: 1603 RVA: 0x0001123F File Offset: 0x0000F43F
		public string Model
		{
			get
			{
				return this.device_model;
			}
			internal set
			{
				this.device_model = value;
			}
		}

		// Token: 0x170001C8 RID: 456
		// (get) Token: 0x06000644 RID: 1604 RVA: 0x00011248 File Offset: 0x0000F448
		// (set) Token: 0x06000645 RID: 1605 RVA: 0x00011250 File Offset: 0x0000F450
		public string Make
		{
			get
			{
				return this.device_make;
			}
			internal set
			{
				this.device_make = value;
			}
		}

		// Token: 0x170001C9 RID: 457
		// (get) Token: 0x06000646 RID: 1606 RVA: 0x00011259 File Offset: 0x0000F459
		// (set) Token: 0x06000647 RID: 1607 RVA: 0x00011261 File Offset: 0x0000F461
		public string PlatformVersion
		{
			get
			{
				return this.device_platformVersion;
			}
			internal set
			{
				this.device_platformVersion = value;
			}
		}

		// Token: 0x170001CA RID: 458
		// (get) Token: 0x06000648 RID: 1608 RVA: 0x0001126A File Offset: 0x0000F46A
		// (set) Token: 0x06000649 RID: 1609 RVA: 0x00011272 File Offset: 0x0000F472
		public string PersistentDataPath { get; set; }

		// Token: 0x170001CB RID: 459
		// (get) Token: 0x0600064A RID: 1610 RVA: 0x0001127B File Offset: 0x0000F47B
		// (set) Token: 0x0600064B RID: 1611 RVA: 0x00011283 File Offset: 0x0000F483
		public string UnityVersion { get; private set; }

		// Token: 0x170001CC RID: 460
		// (get) Token: 0x0600064C RID: 1612 RVA: 0x0001128C File Offset: 0x0000F48C
		// (set) Token: 0x0600064D RID: 1613 RVA: 0x00011294 File Offset: 0x0000F494
		public string Locale
		{
			get
			{
				return this.device_locale;
			}
			set
			{
				this.device_locale = value;
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x0600064E RID: 1614 RVA: 0x0001129D File Offset: 0x0000F49D
		public static AmazonHookedPlatformInfo Instance
		{
			get
			{
				if (AmazonHookedPlatformInfo.instance == null)
				{
					AmazonHookedPlatformInfo.instance = new AmazonHookedPlatformInfo();
					AmazonHookedPlatformInfo.instance.Init();
				}
				return AmazonHookedPlatformInfo.instance;
			}
		}

		// Token: 0x170001CE RID: 462
		// (get) Token: 0x0600064F RID: 1615 RVA: 0x000112BF File Offset: 0x0000F4BF
		// (set) Token: 0x06000650 RID: 1616 RVA: 0x000112C7 File Offset: 0x0000F4C7
		public string PackageName
		{
			get
			{
				return this.app_package_name;
			}
			internal set
			{
				this.app_package_name = value;
			}
		}

		// Token: 0x170001CF RID: 463
		// (get) Token: 0x06000651 RID: 1617 RVA: 0x000112D0 File Offset: 0x0000F4D0
		// (set) Token: 0x06000652 RID: 1618 RVA: 0x000112D8 File Offset: 0x0000F4D8
		public string VersionName
		{
			get
			{
				return this.app_version_name;
			}
			internal set
			{
				this.app_version_name = value;
			}
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x06000653 RID: 1619 RVA: 0x000112E1 File Offset: 0x0000F4E1
		// (set) Token: 0x06000654 RID: 1620 RVA: 0x000112E9 File Offset: 0x0000F4E9
		public string VersionCode
		{
			get
			{
				return this.app_version_code;
			}
			internal set
			{
				this.app_version_code = value;
			}
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000655 RID: 1621 RVA: 0x000112F2 File Offset: 0x0000F4F2
		// (set) Token: 0x06000656 RID: 1622 RVA: 0x000112FA File Offset: 0x0000F4FA
		public string Title
		{
			get
			{
				return this.app_title;
			}
			internal set
			{
				this.app_title = value;
			}
		}

		// Token: 0x06000657 RID: 1623 RVA: 0x00011304 File Offset: 0x0000F504
		public void Init()
		{
			this.PersistentDataPath = Application.persistentDataPath;
			this.UnityVersion = Application.unityVersion;
			if (InternalSDKUtils.IsAndroid)
			{
				this.PlatformVersion = AndroidInterop.GetStaticJavaField<string>("android.os.Build$VERSION", "RELEASE");
				this.Platform = "Android";
				this.Model = AndroidInterop.GetStaticJavaField<string>("android.os.Build", "MODEL");
				this.Make = AndroidInterop.GetStaticJavaField<string>("android.os.Build", "MANUFACTURER");
				object javaObjectStatically = AndroidInterop.GetJavaObjectStatically("java.util.Locale", "getDefault");
				this.Locale = AndroidInterop.CallMethod<string>(javaObjectStatically, "toString", new object[0]);
				object androidContext = AndroidInterop.GetAndroidContext();
				this.PackageName = AndroidInterop.CallMethod<string>(androidContext, "getPackageName", new object[0]);
				object androidJavaObject = AndroidInterop.CallMethod(androidContext, "getPackageManager", new object[0]);
				object androidJavaObject2 = AndroidInterop.CallMethod(androidJavaObject, "getPackageInfo", new object[]
				{
					this.PackageName,
					0
				});
				object obj = AndroidInterop.CallMethod(androidJavaObject, "getApplicationInfo", new object[]
				{
					this.PackageName,
					0
				});
				this.VersionCode = Convert.ToString(AndroidInterop.GetJavaField<int>(androidJavaObject2, "versionCode"));
				this.VersionName = AndroidInterop.GetJavaField<string>(androidJavaObject2, "versionName");
				this.Title = AndroidInterop.CallMethod<string>(androidJavaObject, "getApplicationLabel", new object[]
				{
					obj
				});
				return;
			}
			if (InternalSDKUtils.IsiOS)
			{
				if (!string.IsNullOrEmpty(NSLocale.AutoUpdatingCurrentLocale.Identifier))
				{
					this.Locale = NSLocale.AutoUpdatingCurrentLocale.Identifier;
				}
				else
				{
					this.Locale = NSLocale.AutoUpdatingCurrentLocale.LocaleIdentifier;
				}
				using (UIDevice currentDevice = UIDevice.CurrentDevice)
				{
					this.Platform = currentDevice.SystemName;
					this.PlatformVersion = currentDevice.SystemVersion;
					this.Model = currentDevice.Model;
				}
				this.Make = "apple";
				using (NSBundle mainBundle = NSBundle.MainBundle)
				{
					using (NSDictionary nsobject = Runtime.GetNSObject<NSDictionary>(ObjC.MessageSendIntPtr(mainBundle.Handle, Selector.GetHandle("infoDictionary"))))
					{
						this.Title = nsobject.ObjectForKey("CFBundleDisplayName").ToString();
						this.VersionCode = nsobject.ObjectForKey("CFBundleVersion").ToString();
						this.VersionName = nsobject.ObjectForKey("CFBundleShortVersionString").ToString();
						this.PackageName = nsobject.ObjectForKey("CFBundleIdentifier").ToString();
					}
				}
			}
		}

		// Token: 0x04000280 RID: 640
		private static Amazon.Runtime.Internal.Util.Logger _logger = Amazon.Runtime.Internal.Util.Logger.GetLogger(typeof(AmazonHookedPlatformInfo));

		// Token: 0x04000281 RID: 641
		private const string IPHONE_OS = "iPhone OS";

		// Token: 0x04000282 RID: 642
		private const string ANDROID_OS = "Android";

		// Token: 0x04000283 RID: 643
		private static AmazonHookedPlatformInfo instance = null;

		// Token: 0x04000284 RID: 644
		private string device_platform;

		// Token: 0x04000285 RID: 645
		private string device_model;

		// Token: 0x04000286 RID: 646
		private string device_make;

		// Token: 0x04000287 RID: 647
		private string device_platformVersion;

		// Token: 0x04000288 RID: 648
		private string device_locale;

		// Token: 0x04000289 RID: 649
		private string app_version_name;

		// Token: 0x0400028A RID: 650
		private string app_version_code;

		// Token: 0x0400028B RID: 651
		private string app_package_name;

		// Token: 0x0400028C RID: 652
		private string app_title;
	}
}
