﻿using System;

namespace Amazon.Util
{
	// Token: 0x02000078 RID: 120
	public class JitteredDelay
	{
		// Token: 0x06000595 RID: 1429 RVA: 0x0000F9E2 File Offset: 0x0000DBE2
		public JitteredDelay(TimeSpan baseIncrement, TimeSpan variance) : this(baseIncrement, variance, new TimeSpan(0, 0, 30))
		{
		}

		// Token: 0x06000596 RID: 1430 RVA: 0x0000F9F5 File Offset: 0x0000DBF5
		public JitteredDelay(TimeSpan baseIncrement, TimeSpan variance, TimeSpan maxDelay)
		{
			this._baseIncrement = baseIncrement;
			this._variance = variance;
			this._maxDelay = maxDelay;
			this._rand = new Random();
		}

		// Token: 0x06000597 RID: 1431 RVA: 0x0000FA1D File Offset: 0x0000DC1D
		public TimeSpan GetRetryDelay(int attemptCount)
		{
			return new TimeSpan(this._baseIncrement.Ticks * (long)Math.Pow(2.0, (double)attemptCount) + (long)(this._rand.NextDouble() * (double)this._variance.Ticks));
		}

		// Token: 0x06000598 RID: 1432 RVA: 0x0000FA5C File Offset: 0x0000DC5C
		public TimeSpan Next()
		{
			long ticks = this.GetRetryDelay(this._count + 1).Ticks;
			if (ticks < this._maxDelay.Ticks)
			{
				this._count++;
			}
			else
			{
				ticks = this._maxDelay.Ticks;
			}
			return new TimeSpan(ticks);
		}

		// Token: 0x06000599 RID: 1433 RVA: 0x0000FAB0 File Offset: 0x0000DCB0
		public void Reset()
		{
			this._count = 0;
		}

		// Token: 0x04000215 RID: 533
		private TimeSpan _maxDelay;

		// Token: 0x04000216 RID: 534
		private TimeSpan _variance;

		// Token: 0x04000217 RID: 535
		private TimeSpan _baseIncrement;

		// Token: 0x04000218 RID: 536
		private Random _rand;

		// Token: 0x04000219 RID: 537
		private int _count;
	}
}
