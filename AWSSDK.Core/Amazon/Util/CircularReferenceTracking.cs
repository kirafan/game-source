﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace Amazon.Util
{
	// Token: 0x02000081 RID: 129
	public class CircularReferenceTracking
	{
		// Token: 0x060005CF RID: 1487 RVA: 0x00010210 File Offset: 0x0000E410
		public IDisposable Track(object target)
		{
			if (target == null)
			{
				throw new ArgumentNullException("target");
			}
			object obj = this.referenceTrackersLock;
			IDisposable result;
			lock (obj)
			{
				if (this.TrackerExists(target))
				{
					throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Circular reference detected with object [{0}] of type {1}", new object[]
					{
						target,
						target.GetType().FullName
					}));
				}
				CircularReferenceTracking.Tracker tracker = new CircularReferenceTracking.Tracker(this, target);
				this.referenceTrackers.Push(tracker);
				result = tracker;
			}
			return result;
		}

		// Token: 0x060005D0 RID: 1488 RVA: 0x000102A0 File Offset: 0x0000E4A0
		private void PopTracker(CircularReferenceTracking.Tracker tracker)
		{
			object obj = this.referenceTrackersLock;
			lock (obj)
			{
				if (this.referenceTrackers.Peek() != tracker)
				{
					throw new InvalidOperationException("Tracker being released is not the latest one. Make sure to release child trackers before releasing parent.");
				}
				this.referenceTrackers.Pop();
			}
		}

		// Token: 0x060005D1 RID: 1489 RVA: 0x000102F8 File Offset: 0x0000E4F8
		private bool TrackerExists(object target)
		{
			using (Stack<CircularReferenceTracking.Tracker>.Enumerator enumerator = this.referenceTrackers.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					if (enumerator.Current.Target == target)
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x04000269 RID: 617
		private object referenceTrackersLock = new object();

		// Token: 0x0400026A RID: 618
		private Stack<CircularReferenceTracking.Tracker> referenceTrackers = new Stack<CircularReferenceTracking.Tracker>();

		// Token: 0x020001CA RID: 458
		private class Tracker : IDisposable
		{
			// Token: 0x1700041A RID: 1050
			// (get) Token: 0x06000EDB RID: 3803 RVA: 0x00026F95 File Offset: 0x00025195
			// (set) Token: 0x06000EDC RID: 3804 RVA: 0x00026F9D File Offset: 0x0002519D
			public object Target { get; private set; }

			// Token: 0x1700041B RID: 1051
			// (get) Token: 0x06000EDD RID: 3805 RVA: 0x00026FA6 File Offset: 0x000251A6
			// (set) Token: 0x06000EDE RID: 3806 RVA: 0x00026FAE File Offset: 0x000251AE
			private CircularReferenceTracking State { get; set; }

			// Token: 0x06000EDF RID: 3807 RVA: 0x00026FB7 File Offset: 0x000251B7
			public Tracker(CircularReferenceTracking state, object target)
			{
				this.State = state;
				this.Target = target;
			}

			// Token: 0x06000EE0 RID: 3808 RVA: 0x00026FCD File Offset: 0x000251CD
			public override string ToString()
			{
				return string.Format(CultureInfo.InvariantCulture, "Tracking {0}", new object[]
				{
					this.Target
				});
			}

			// Token: 0x06000EE1 RID: 3809 RVA: 0x00026FED File Offset: 0x000251ED
			protected virtual void Dispose(bool disposing)
			{
				if (!this.disposed)
				{
					if (disposing)
					{
						this.State.PopTracker(this);
					}
					this.disposed = true;
				}
			}

			// Token: 0x06000EE2 RID: 3810 RVA: 0x0002700D File Offset: 0x0002520D
			public void Dispose()
			{
				this.Dispose(true);
				GC.SuppressFinalize(this);
			}

			// Token: 0x06000EE3 RID: 3811 RVA: 0x0002701C File Offset: 0x0002521C
			~Tracker()
			{
				this.Dispose(false);
			}

			// Token: 0x0400098A RID: 2442
			private bool disposed;
		}
	}
}
