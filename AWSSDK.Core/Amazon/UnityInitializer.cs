﻿using System;
using System.Diagnostics;
using System.Threading;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Util;
using Amazon.Util.Internal;
using UnityEngine;

namespace Amazon
{
	// Token: 0x0200006D RID: 109
	public class UnityInitializer : MonoBehaviour
	{
		// Token: 0x0600050E RID: 1294 RVA: 0x0000D980 File Offset: 0x0000BB80
		private UnityInitializer()
		{
		}

		// Token: 0x0600050F RID: 1295 RVA: 0x0000D988 File Offset: 0x0000BB88
		public static void AttachToGameObject(GameObject gameObject)
		{
			if (gameObject != null)
			{
				gameObject.AddComponent<UnityInitializer>();
				UnityEngine.Debug.Log(string.Format("Attached unity initializer to {0}", gameObject.name));
				return;
			}
			throw new ArgumentNullException("gameObject");
		}

		// Token: 0x1700017C RID: 380
		// (get) Token: 0x06000510 RID: 1296 RVA: 0x0000D9BA File Offset: 0x0000BBBA
		public static UnityInitializer Instance
		{
			get
			{
				return UnityInitializer._instance;
			}
		}

		// Token: 0x06000511 RID: 1297 RVA: 0x0000D9C4 File Offset: 0x0000BBC4
		public void Awake()
		{
			object @lock = UnityInitializer._lock;
			lock (@lock)
			{
				if (UnityInitializer._instance == null)
				{
					UnityInitializer._instance = this;
					if (UnityInitializer._mainThread == null || !UnityInitializer._mainThread.Equals(Thread.CurrentThread))
					{
						UnityInitializer._mainThread = Thread.CurrentThread;
					}
					AmazonHookedPlatformInfo.Instance.Init();
					UnityEngine.Object.DontDestroyOnLoad(this);
					TraceListener listener = new UnityDebugTraceListener("UnityDebug");
					AWSConfigs.AddTraceListener("Amazon", listener);
					UnityInitializer._instance.gameObject.AddComponent<UnityMainThreadDispatcher>();
				}
				else if (this != UnityInitializer._instance)
				{
					UnityEngine.Object.DestroyObject(this);
				}
			}
		}

		// Token: 0x06000512 RID: 1298 RVA: 0x0000DA78 File Offset: 0x0000BC78
		public static bool IsMainThread()
		{
			if (UnityInitializer._mainThread == null)
			{
				throw new Exception("Main thread has not been set, is the AWSPrefab on the scene?");
			}
			return Thread.CurrentThread.Equals(UnityInitializer._mainThread);
		}

		// Token: 0x040001C5 RID: 453
		private static UnityInitializer _instance = null;

		// Token: 0x040001C6 RID: 454
		private static object _lock = new object();

		// Token: 0x040001C7 RID: 455
		private static Thread _mainThread;
	}
}
