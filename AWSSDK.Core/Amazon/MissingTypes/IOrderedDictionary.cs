﻿using System;
using System.Collections;

namespace Amazon.MissingTypes
{
	// Token: 0x02000171 RID: 369
	public interface IOrderedDictionary : IDictionary, ICollection, IEnumerable
	{
		// Token: 0x170003FF RID: 1023
		object this[int index]
		{
			get;
			set;
		}

		// Token: 0x06000DB1 RID: 3505
		IDictionaryEnumerator GetEnumerator();

		// Token: 0x06000DB2 RID: 3506
		void Insert(int index, object key, object value);

		// Token: 0x06000DB3 RID: 3507
		void RemoveAt(int index);
	}
}
