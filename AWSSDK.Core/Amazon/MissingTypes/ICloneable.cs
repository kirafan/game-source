﻿using System;

namespace Amazon.MissingTypes
{
	// Token: 0x02000170 RID: 368
	public interface ICloneable
	{
		// Token: 0x06000DAE RID: 3502
		object Clone();
	}
}
