﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Xml.Linq;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Util;
using Amazon.Util;
using Amazon.Util.Internal;
using UnityEngine;

namespace Amazon
{
	// Token: 0x02000065 RID: 101
	public static class AWSConfigs
	{
		// Token: 0x17000153 RID: 339
		// (get) Token: 0x060004A2 RID: 1186 RVA: 0x0000C918 File Offset: 0x0000AB18
		// (set) Token: 0x060004A3 RID: 1187 RVA: 0x0000C954 File Offset: 0x0000AB54
		public static TimeSpan? ManualClockCorrection
		{
			get
			{
				object obj = AWSConfigs.manualClockCorrectionLock;
				TimeSpan? result;
				lock (obj)
				{
					result = AWSConfigs.manualClockCorrection;
				}
				return result;
			}
			set
			{
				object obj = AWSConfigs.manualClockCorrectionLock;
				lock (obj)
				{
					AWSConfigs.manualClockCorrection = value;
				}
			}
		}

		// Token: 0x17000154 RID: 340
		// (get) Token: 0x060004A4 RID: 1188 RVA: 0x0000C98C File Offset: 0x0000AB8C
		// (set) Token: 0x060004A5 RID: 1189 RVA: 0x0000C998 File Offset: 0x0000AB98
		public static bool CorrectForClockSkew
		{
			get
			{
				return AWSConfigs._rootConfig.CorrectForClockSkew;
			}
			set
			{
				AWSConfigs._rootConfig.CorrectForClockSkew = value;
			}
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060004A6 RID: 1190 RVA: 0x0000C9A5 File Offset: 0x0000ABA5
		// (set) Token: 0x060004A7 RID: 1191 RVA: 0x0000C9AC File Offset: 0x0000ABAC
		public static TimeSpan ClockOffset { get; internal set; }

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060004A8 RID: 1192 RVA: 0x0000C9B4 File Offset: 0x0000ABB4
		// (set) Token: 0x060004A9 RID: 1193 RVA: 0x0000C9C0 File Offset: 0x0000ABC0
		public static string AWSRegion
		{
			get
			{
				return AWSConfigs._rootConfig.Region;
			}
			set
			{
				AWSConfigs._rootConfig.Region = value;
			}
		}

		// Token: 0x17000157 RID: 343
		// (get) Token: 0x060004AA RID: 1194 RVA: 0x0000C9CD File Offset: 0x0000ABCD
		// (set) Token: 0x060004AB RID: 1195 RVA: 0x0000C9D9 File Offset: 0x0000ABD9
		public static string AWSProfileName
		{
			get
			{
				return AWSConfigs._rootConfig.ProfileName;
			}
			set
			{
				AWSConfigs._rootConfig.ProfileName = value;
			}
		}

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060004AC RID: 1196 RVA: 0x0000C9E6 File Offset: 0x0000ABE6
		// (set) Token: 0x060004AD RID: 1197 RVA: 0x0000C9F2 File Offset: 0x0000ABF2
		public static string AWSProfilesLocation
		{
			get
			{
				return AWSConfigs._rootConfig.ProfilesLocation;
			}
			set
			{
				AWSConfigs._rootConfig.ProfilesLocation = value;
			}
		}

		// Token: 0x17000159 RID: 345
		// (get) Token: 0x060004AE RID: 1198 RVA: 0x0000C9FF File Offset: 0x0000ABFF
		// (set) Token: 0x060004AF RID: 1199 RVA: 0x0000CA10 File Offset: 0x0000AC10
		[Obsolete("This property is obsolete. Use LoggingConfig.LogTo instead.")]
		public static LoggingOptions Logging
		{
			get
			{
				return AWSConfigs._rootConfig.Logging.LogTo;
			}
			set
			{
				AWSConfigs._rootConfig.Logging.LogTo = value;
			}
		}

		// Token: 0x060004B0 RID: 1200 RVA: 0x0000CA24 File Offset: 0x0000AC24
		private static LoggingOptions GetLoggingSetting()
		{
			string config = AWSConfigs.GetConfig("AWSLogging");
			if (string.IsNullOrEmpty(config))
			{
				return LoggingOptions.None;
			}
			string[] array = config.Split(AWSConfigs.validSeparators, StringSplitOptions.RemoveEmptyEntries);
			if (array == null || array.Length == 0)
			{
				return LoggingOptions.None;
			}
			LoggingOptions loggingOptions = LoggingOptions.None;
			string[] array2 = array;
			for (int i = 0; i < array2.Length; i++)
			{
				LoggingOptions loggingOptions2 = AWSConfigs.ParseEnum<LoggingOptions>(array2[i]);
				loggingOptions |= loggingOptions2;
			}
			return loggingOptions;
		}

		// Token: 0x1700015A RID: 346
		// (get) Token: 0x060004B1 RID: 1201 RVA: 0x0000CA83 File Offset: 0x0000AC83
		// (set) Token: 0x060004B2 RID: 1202 RVA: 0x0000CA94 File Offset: 0x0000AC94
		[Obsolete("This property is obsolete. Use LoggingConfig.LogResponses instead.")]
		public static ResponseLoggingOption ResponseLogging
		{
			get
			{
				return AWSConfigs._rootConfig.Logging.LogResponses;
			}
			set
			{
				AWSConfigs._rootConfig.Logging.LogResponses = value;
			}
		}

		// Token: 0x1700015B RID: 347
		// (get) Token: 0x060004B3 RID: 1203 RVA: 0x0000CAA6 File Offset: 0x0000ACA6
		// (set) Token: 0x060004B4 RID: 1204 RVA: 0x0000CAB7 File Offset: 0x0000ACB7
		[Obsolete("This property is obsolete. Use LoggingConfig.LogMetrics instead.")]
		public static bool LogMetrics
		{
			get
			{
				return AWSConfigs._rootConfig.Logging.LogMetrics;
			}
			set
			{
				AWSConfigs._rootConfig.Logging.LogMetrics = value;
			}
		}

		// Token: 0x1700015C RID: 348
		// (get) Token: 0x060004B5 RID: 1205 RVA: 0x0000CAC9 File Offset: 0x0000ACC9
		// (set) Token: 0x060004B6 RID: 1206 RVA: 0x0000CAD5 File Offset: 0x0000ACD5
		public static string EndpointDefinition
		{
			get
			{
				return AWSConfigs._rootConfig.EndpointDefinition;
			}
			set
			{
				AWSConfigs._rootConfig.EndpointDefinition = value;
			}
		}

		// Token: 0x1700015D RID: 349
		// (get) Token: 0x060004B7 RID: 1207 RVA: 0x0000CAE2 File Offset: 0x0000ACE2
		// (set) Token: 0x060004B8 RID: 1208 RVA: 0x0000CAEE File Offset: 0x0000ACEE
		public static bool UseSdkCache
		{
			get
			{
				return AWSConfigs._rootConfig.UseSdkCache;
			}
			set
			{
				AWSConfigs._rootConfig.UseSdkCache = value;
			}
		}

		// Token: 0x1700015E RID: 350
		// (get) Token: 0x060004B9 RID: 1209 RVA: 0x0000CAFB File Offset: 0x0000ACFB
		public static LoggingConfig LoggingConfig
		{
			get
			{
				return AWSConfigs._rootConfig.Logging;
			}
		}

		// Token: 0x1700015F RID: 351
		// (get) Token: 0x060004BA RID: 1210 RVA: 0x0000CB07 File Offset: 0x0000AD07
		public static ProxyConfig ProxyConfig
		{
			get
			{
				return AWSConfigs._rootConfig.Proxy;
			}
		}

		// Token: 0x17000160 RID: 352
		// (get) Token: 0x060004BB RID: 1211 RVA: 0x0000CB13 File Offset: 0x0000AD13
		// (set) Token: 0x060004BC RID: 1212 RVA: 0x0000CB1F File Offset: 0x0000AD1F
		public static RegionEndpoint RegionEndpoint
		{
			get
			{
				return AWSConfigs._rootConfig.RegionEndpoint;
			}
			set
			{
				AWSConfigs._rootConfig.RegionEndpoint = value;
			}
		}

		// Token: 0x14000018 RID: 24
		// (add) Token: 0x060004BD RID: 1213 RVA: 0x0000CB2C File Offset: 0x0000AD2C
		// (remove) Token: 0x060004BE RID: 1214 RVA: 0x0000CB74 File Offset: 0x0000AD74
		internal static event PropertyChangedEventHandler PropertyChanged
		{
			add
			{
				object obj = AWSConfigs.propertyChangedLock;
				lock (obj)
				{
					AWSConfigs.mPropertyChanged = (PropertyChangedEventHandler)Delegate.Combine(AWSConfigs.mPropertyChanged, value);
				}
			}
			remove
			{
				object obj = AWSConfigs.propertyChangedLock;
				lock (obj)
				{
					AWSConfigs.mPropertyChanged = (PropertyChangedEventHandler)Delegate.Remove(AWSConfigs.mPropertyChanged, value);
				}
			}
		}

		// Token: 0x060004BF RID: 1215 RVA: 0x0000CBBC File Offset: 0x0000ADBC
		internal static void OnPropertyChanged(string name)
		{
			PropertyChangedEventHandler propertyChangedEventHandler = AWSConfigs.mPropertyChanged;
			if (propertyChangedEventHandler != null)
			{
				propertyChangedEventHandler(null, new PropertyChangedEventArgs(name));
			}
		}

		// Token: 0x060004C0 RID: 1216 RVA: 0x0000CBE0 File Offset: 0x0000ADE0
		private static bool GetConfigBool(string name, bool defaultValue = false)
		{
			bool result;
			if (bool.TryParse(AWSConfigs.GetConfig(name), out result))
			{
				return result;
			}
			return defaultValue;
		}

		// Token: 0x060004C1 RID: 1217 RVA: 0x0000CC00 File Offset: 0x0000AE00
		private static T GetConfigEnum<T>(string name)
		{
			ITypeInfo typeInfo = TypeFactory.GetTypeInfo(typeof(T));
			if (!typeInfo.IsEnum)
			{
				throw new InvalidOperationException(string.Format(CultureInfo.InvariantCulture, "Type {0} must be enum", new object[]
				{
					typeInfo.FullName
				}));
			}
			string config = AWSConfigs.GetConfig(name);
			if (string.IsNullOrEmpty(config))
			{
				return default(T);
			}
			return AWSConfigs.ParseEnum<T>(config);
		}

		// Token: 0x060004C2 RID: 1218 RVA: 0x0000CC68 File Offset: 0x0000AE68
		private static T ParseEnum<T>(string value)
		{
			T result;
			if (AWSConfigs.TryParseEnum<T>(value, out result))
			{
				return result;
			}
			Type typeFromHandle = typeof(T);
			string format = "Unable to parse value {0} as enum of type {1}. Valid values are: {2}";
			string text = string.Join(", ", Enum.GetNames(typeFromHandle));
			throw new ArgumentException(string.Format(CultureInfo.InvariantCulture, format, new object[]
			{
				value,
				typeFromHandle.FullName,
				text
			}));
		}

		// Token: 0x060004C3 RID: 1219 RVA: 0x0000CCCC File Offset: 0x0000AECC
		private static bool TryParseEnum<T>(string value, out T result)
		{
			result = default(T);
			if (string.IsNullOrEmpty(value))
			{
				return false;
			}
			bool result2;
			try
			{
				T t = (T)((object)Enum.Parse(typeof(T), value, true));
				result = t;
				result2 = true;
			}
			catch (ArgumentException)
			{
				result2 = false;
			}
			return result2;
		}

		// Token: 0x060004C4 RID: 1220 RVA: 0x0000CD24 File Offset: 0x0000AF24
		private static DateTime GetUtcNow()
		{
			return DateTime.UtcNow;
		}

		// Token: 0x17000161 RID: 353
		// (get) Token: 0x060004C5 RID: 1221 RVA: 0x0000CD2B File Offset: 0x0000AF2B
		// (set) Token: 0x060004C6 RID: 1222 RVA: 0x0000CD37 File Offset: 0x0000AF37
		public static string ApplicationName
		{
			get
			{
				return AWSConfigs._rootConfig.ApplicationName;
			}
			set
			{
				AWSConfigs._rootConfig.ApplicationName = value;
			}
		}

		// Token: 0x060004C7 RID: 1223 RVA: 0x0000CD44 File Offset: 0x0000AF44
		public static string GetConfig(string name)
		{
			return null;
		}

		// Token: 0x17000162 RID: 354
		// (get) Token: 0x060004C8 RID: 1224 RVA: 0x0000CD47 File Offset: 0x0000AF47
		// (set) Token: 0x060004C9 RID: 1225 RVA: 0x0000CD4E File Offset: 0x0000AF4E
		public static AWSConfigs.HttpClientOption HttpClient
		{
			get
			{
				return AWSConfigs._httpClient;
			}
			set
			{
				if (value == AWSConfigs.HttpClientOption.UnityWebRequest)
				{
					if (!UnityWebRequestWrapper.IsUnityWebRequestSupported)
					{
						AWSConfigs.UnityWebRequestInitialized = false;
						throw new InvalidOperationException("UnityWebRequest is not supported in the current version of unity");
					}
					AWSConfigs.UnityWebRequestInitialized = true;
				}
				AWSConfigs._httpClient = value;
			}
		}

		// Token: 0x060004CA RID: 1226 RVA: 0x0000CD78 File Offset: 0x0000AF78
		internal static T GetSection<T>(string sectionName) where T : AWSSection, new()
		{
			if (!AWSConfigs.configPresent)
			{
				return Activator.CreateInstance<T>();
			}
			if (AWSConfigs.xmlDoc == null)
			{
				object @lock = AWSConfigs._lock;
				lock (@lock)
				{
					if (AWSConfigs.xmlDoc == null)
					{
						AWSConfigs.xmlDoc = AWSConfigs.LoadConfigFromResource();
						AWSConfigs.configPresent = (AWSConfigs.xmlDoc != null);
					}
				}
			}
			if (AWSConfigs.configPresent)
			{
				XElement xelement = AWSConfigs.xmlDoc.Element(sectionName);
				T t = Activator.CreateInstance<T>();
				t.Logging = AWSConfigs.GetObject<LoggingSection>(xelement, "logging");
				t.Region = ((xelement.Attribute("region") == null) ? string.Empty : xelement.Attribute("region").Value);
				t.CorrectForClockSkew = new bool?(bool.Parse(xelement.Attribute("correctForClockSkew").Value));
				t.ServiceSections = AWSConfigs.GetUnresolvedElements(xelement);
				return t;
			}
			return Activator.CreateInstance<T>();
		}

		// Token: 0x060004CB RID: 1227 RVA: 0x0000CE8C File Offset: 0x0000B08C
		internal static bool XmlSectionExists(string sectionName)
		{
			return AWSConfigs.configPresent && AWSConfigs.xmlDoc.Element(sectionName) != null;
		}

		// Token: 0x060004CC RID: 1228 RVA: 0x0000CEAC File Offset: 0x0000B0AC
		public static void AddTraceListener(string source, TraceListener listener)
		{
			if (string.IsNullOrEmpty(source))
			{
				throw new ArgumentException("Source cannot be null or empty", "source");
			}
			if (listener == null)
			{
				throw new ArgumentException("Listener cannot be null", "listener");
			}
			Dictionary<string, List<TraceListener>> traceListeners = AWSConfigs._traceListeners;
			lock (traceListeners)
			{
				if (!AWSConfigs._traceListeners.ContainsKey(source))
				{
					AWSConfigs._traceListeners.Add(source, new List<TraceListener>());
				}
				AWSConfigs._traceListeners[source].Add(listener);
			}
		}

		// Token: 0x060004CD RID: 1229 RVA: 0x0000CF38 File Offset: 0x0000B138
		public static void RemoveTraceListener(string source, string name)
		{
			if (string.IsNullOrEmpty(source))
			{
				throw new ArgumentException("Source cannot be null or empty", "source");
			}
			if (string.IsNullOrEmpty(name))
			{
				throw new ArgumentException("Name cannot be null or empty", "name");
			}
			Dictionary<string, List<TraceListener>> traceListeners = AWSConfigs._traceListeners;
			lock (traceListeners)
			{
				if (AWSConfigs._traceListeners.ContainsKey(source))
				{
					foreach (TraceListener traceListener in AWSConfigs._traceListeners[source])
					{
						if (traceListener.Name.Equals(name, StringComparison.Ordinal))
						{
							AWSConfigs._traceListeners[source].Remove(traceListener);
							break;
						}
					}
				}
			}
			Amazon.Runtime.Internal.Util.Logger.ClearLoggerCache();
		}

		// Token: 0x060004CE RID: 1230 RVA: 0x0000D010 File Offset: 0x0000B210
		internal static TraceListener[] TraceListeners(string source)
		{
			Dictionary<string, List<TraceListener>> traceListeners = AWSConfigs._traceListeners;
			TraceListener[] result;
			lock (traceListeners)
			{
				List<TraceListener> list;
				if (AWSConfigs._traceListeners.TryGetValue(source, out list))
				{
					result = list.ToArray();
				}
				else
				{
					result = new TraceListener[0];
				}
			}
			return result;
		}

		// Token: 0x060004CF RID: 1231 RVA: 0x0000D064 File Offset: 0x0000B264
		private static XDocument LoadConfigFromResource()
		{
			XDocument xDoc = null;
			TextAsset awsConfig = null;
			Action action = delegate()
			{
				awsConfig = (Resources.Load("awsconfig") as TextAsset);
				if (awsConfig != null && awsConfig.bytes.Count<byte>() > 0)
				{
					using (Stream stream = new MemoryStream(awsConfig.bytes))
					{
						using (StreamReader streamReader = new StreamReader(stream))
						{
							xDoc = XDocument.Load(streamReader);
						}
					}
				}
			};
			if (UnityInitializer.IsMainThread())
			{
				action();
			}
			else
			{
				ManualResetEvent e = new ManualResetEvent(false);
				UnityRequestQueue.Instance.ExecuteOnMainThread(delegate
				{
					action();
					e.Set();
				});
				e.WaitOne();
			}
			return xDoc;
		}

		// Token: 0x060004D0 RID: 1232 RVA: 0x0000D0EC File Offset: 0x0000B2EC
		public static T GetObject<T>(XElement rootElement, string propertyName) where T : class, new()
		{
			return (T)((object)AWSConfigs.GetObject(rootElement, propertyName, typeof(T)));
		}

		// Token: 0x060004D1 RID: 1233 RVA: 0x0000D104 File Offset: 0x0000B304
		private static object GetObject(XElement rootElement, string propertyName, Type type)
		{
			object obj = Activator.CreateInstance(type);
			PropertyInfo[] properties = obj.GetType().GetProperties();
			rootElement = ((rootElement.Elements(propertyName).Count<XElement>() == 0) ? rootElement : rootElement.Elements(propertyName).First<XElement>());
			PropertyInfo[] array = properties;
			for (int i = 0; i < array.Length; i++)
			{
				PropertyInfo propertyInfo = array[i];
				if (propertyInfo.CanWrite)
				{
					Type propertyType = propertyInfo.PropertyType;
					XAttribute xattribute = rootElement.Attributes().SingleOrDefault((XAttribute a) => a.Name.ToString().Equals(propertyInfo.Name, StringComparison.OrdinalIgnoreCase));
					if (xattribute != null)
					{
						if (propertyType.BaseType.Equals(typeof(Enum)))
						{
							propertyInfo.SetValue(obj, Enum.Parse(propertyType, xattribute.Value, true), null);
						}
						else
						{
							Type underlyingType = Nullable.GetUnderlyingType(propertyType);
							object value;
							if (underlyingType != null)
							{
								value = Convert.ChangeType(xattribute.Value, underlyingType);
							}
							else if (propertyType == typeof(Type))
							{
								value = Type.GetType(xattribute.Value, true);
							}
							else
							{
								value = Convert.ChangeType(xattribute.Value, propertyType);
							}
							propertyInfo.SetValue(obj, value, null);
						}
					}
					else
					{
						XElement xelement = rootElement.Elements().SingleOrDefault((XElement e) => e.Name.ToString().Equals(propertyInfo.Name, StringComparison.OrdinalIgnoreCase));
						if (typeof(IList).IsAssignableFrom(propertyType))
						{
							string propertyName2 = (xelement == null) ? null : xelement.Name.ToString();
							IEnumerable list = AWSConfigs.GetList(rootElement, propertyType, propertyName2);
							propertyInfo.SetValue(obj, list, null);
						}
						else if (xelement != null)
						{
							object @object = AWSConfigs.GetObject(xelement, xelement.Name.ToString(), propertyType);
							propertyInfo.SetValue(obj, @object, null);
						}
					}
				}
			}
			return obj;
		}

		// Token: 0x060004D2 RID: 1234 RVA: 0x0000D2D4 File Offset: 0x0000B4D4
		private static IEnumerable GetList(XElement rootElement, Type listType, string propertyName)
		{
			IList list = (IList)Activator.CreateInstance(listType);
			string propertyName2 = (string)listType.GetProperty("ItemPropertyName").GetValue(list, null);
			Type propertyType = listType.GetProperty("Item").PropertyType;
			if (!string.IsNullOrEmpty(propertyName))
			{
				rootElement = ((rootElement.Elements(propertyName).Count<XElement>() == 0) ? rootElement : rootElement.Elements(propertyName).First<XElement>());
			}
			foreach (XElement rootElement2 in rootElement.Elements())
			{
				object @object = AWSConfigs.GetObject(rootElement2, propertyName2, propertyType);
				list.Add(@object);
			}
			return list;
		}

		// Token: 0x060004D3 RID: 1235 RVA: 0x0000D394 File Offset: 0x0000B594
		private static IDictionary<string, XElement> GetUnresolvedElements(XElement parent)
		{
			IDictionary<string, XElement> dictionary = new Dictionary<string, XElement>();
			foreach (XElement xelement in parent.Elements())
			{
				if (!AWSConfigs.standardConfigs.Contains(xelement.Name.ToString()))
				{
					dictionary.Add(xelement.Name.ToString(), xelement);
				}
			}
			return dictionary;
		}

		// Token: 0x04000165 RID: 357
		private static char[] validSeparators = new char[]
		{
			' ',
			','
		};

		// Token: 0x04000166 RID: 358
		private static TimeSpan? manualClockCorrection;

		// Token: 0x04000167 RID: 359
		private static object manualClockCorrectionLock = new object();

		// Token: 0x04000168 RID: 360
		internal static Func<DateTime> utcNowSource = new Func<DateTime>(AWSConfigs.GetUtcNow);

		// Token: 0x04000169 RID: 361
		internal static string _awsRegion = AWSConfigs.GetConfig("AWSRegion");

		// Token: 0x0400016A RID: 362
		internal static LoggingOptions _logging = AWSConfigs.GetLoggingSetting();

		// Token: 0x0400016B RID: 363
		internal static ResponseLoggingOption _responseLogging = AWSConfigs.GetConfigEnum<ResponseLoggingOption>("AWSResponseLogging");

		// Token: 0x0400016C RID: 364
		internal static bool _logMetrics = AWSConfigs.GetConfigBool("AWSLogMetrics", false);

		// Token: 0x0400016D RID: 365
		internal static string _endpointDefinition = AWSConfigs.GetConfig("AWSEndpointDefinition");

		// Token: 0x0400016E RID: 366
		internal static string _awsProfileName = AWSConfigs.GetConfig("AWSProfileName");

		// Token: 0x0400016F RID: 367
		internal static string _awsAccountsLocation = AWSConfigs.GetConfig("AWSProfilesLocation");

		// Token: 0x04000170 RID: 368
		internal static bool _useSdkCache = AWSConfigs.GetConfigBool("AWSCache", true);

		// Token: 0x04000171 RID: 369
		private static object _lock = new object();

		// Token: 0x04000172 RID: 370
		private static List<string> standardConfigs = new List<string>
		{
			"region",
			"logging",
			"correctForClockSkew"
		};

		// Token: 0x04000173 RID: 371
		private static bool configPresent = true;

		// Token: 0x04000174 RID: 372
		private static RootConfig _rootConfig = new RootConfig();

		// Token: 0x04000176 RID: 374
		public const string AWSRegionKey = "AWSRegion";

		// Token: 0x04000177 RID: 375
		public const string AWSProfileNameKey = "AWSProfileName";

		// Token: 0x04000178 RID: 376
		public const string AWSProfilesLocationKey = "AWSProfilesLocation";

		// Token: 0x04000179 RID: 377
		public const string LoggingKey = "AWSLogging";

		// Token: 0x0400017A RID: 378
		public const string ResponseLoggingKey = "AWSResponseLogging";

		// Token: 0x0400017B RID: 379
		public const string LogMetricsKey = "AWSLogMetrics";

		// Token: 0x0400017C RID: 380
		public const string EndpointDefinitionKey = "AWSEndpointDefinition";

		// Token: 0x0400017D RID: 381
		public const string UseSdkCacheKey = "AWSCache";

		// Token: 0x0400017E RID: 382
		internal const string LoggingDestinationProperty = "LogTo";

		// Token: 0x0400017F RID: 383
		internal static PropertyChangedEventHandler mPropertyChanged;

		// Token: 0x04000180 RID: 384
		internal static readonly object propertyChangedLock = new object();

		// Token: 0x04000181 RID: 385
		private static AWSConfigs.HttpClientOption _httpClient;

		// Token: 0x04000182 RID: 386
		internal static bool UnityWebRequestInitialized;

		// Token: 0x04000183 RID: 387
		internal static Dictionary<string, List<TraceListener>> _traceListeners = new Dictionary<string, List<TraceListener>>(StringComparer.OrdinalIgnoreCase);

		// Token: 0x04000184 RID: 388
		private const string CONFIG_FILE = "awsconfig";

		// Token: 0x04000185 RID: 389
		internal static XDocument xmlDoc;

		// Token: 0x020001B9 RID: 441
		public enum HttpClientOption
		{
			// Token: 0x0400095C RID: 2396
			UnityWWW,
			// Token: 0x0400095D RID: 2397
			UnityWebRequest
		}
	}
}
