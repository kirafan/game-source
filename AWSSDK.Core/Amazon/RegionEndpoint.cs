﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Amazon.Internal;

namespace Amazon
{
	// Token: 0x0200006C RID: 108
	public class RegionEndpoint
	{
		// Token: 0x17000177 RID: 375
		// (get) Token: 0x06000500 RID: 1280 RVA: 0x0000D688 File Offset: 0x0000B888
		public static IEnumerable<RegionEndpoint> EnumerableAllRegions
		{
			get
			{
				List<RegionEndpoint> list = new List<RegionEndpoint>();
				foreach (IRegionEndpoint regionEndpoint in RegionEndpoint.RegionEndpointProvider.AllRegionEndpoints)
				{
					RegionEndpoint item;
					if (!RegionEndpoint._hashBySystemName.TryGetValue(regionEndpoint.RegionName, out item))
					{
						item = RegionEndpoint.NewEndpoint(regionEndpoint.RegionName, regionEndpoint.DisplayName);
					}
					list.Add(item);
				}
				return list;
			}
		}

		// Token: 0x06000501 RID: 1281 RVA: 0x0000D708 File Offset: 0x0000B908
		public static RegionEndpoint GetBySystemName(string systemName)
		{
			RegionEndpoint result = null;
			if (RegionEndpoint._hashBySystemName.TryGetValue(systemName, out result))
			{
				return result;
			}
			IRegionEndpoint regionEndpoint = RegionEndpoint.RegionEndpointProvider.GetRegionEndpoint(systemName);
			return RegionEndpoint.NewEndpoint(systemName, regionEndpoint.DisplayName);
		}

		// Token: 0x06000502 RID: 1282 RVA: 0x0000D740 File Offset: 0x0000B940
		private static RegionEndpoint NewEndpoint(string systemName, string displayName)
		{
			RegionEndpoint regionEndpoint = new RegionEndpoint(systemName, displayName);
			RegionEndpoint._hashBySystemName.Add(regionEndpoint.SystemName, regionEndpoint);
			return regionEndpoint;
		}

		// Token: 0x17000178 RID: 376
		// (get) Token: 0x06000503 RID: 1283 RVA: 0x0000D767 File Offset: 0x0000B967
		private static IRegionEndpointProvider RegionEndpointProvider
		{
			get
			{
				if (RegionEndpoint._regionEndpointProvider == null)
				{
					if (!string.IsNullOrEmpty(AWSConfigs.EndpointDefinition))
					{
						RegionEndpoint._regionEndpointProvider = new RegionEndpointProviderV2();
					}
					else
					{
						RegionEndpoint._regionEndpointProvider = new RegionEndpointProviderV3();
					}
				}
				return RegionEndpoint._regionEndpointProvider;
			}
		}

		// Token: 0x06000504 RID: 1284 RVA: 0x0000D797 File Offset: 0x0000B997
		private RegionEndpoint(string systemName, string displayName)
		{
			this.SystemName = systemName;
			this.DisplayName = displayName;
		}

		// Token: 0x17000179 RID: 377
		// (get) Token: 0x06000505 RID: 1285 RVA: 0x0000D7AD File Offset: 0x0000B9AD
		// (set) Token: 0x06000506 RID: 1286 RVA: 0x0000D7B5 File Offset: 0x0000B9B5
		public string SystemName { get; private set; }

		// Token: 0x1700017A RID: 378
		// (get) Token: 0x06000507 RID: 1287 RVA: 0x0000D7BE File Offset: 0x0000B9BE
		// (set) Token: 0x06000508 RID: 1288 RVA: 0x0000D7C6 File Offset: 0x0000B9C6
		public string DisplayName { get; private set; }

		// Token: 0x1700017B RID: 379
		// (get) Token: 0x06000509 RID: 1289 RVA: 0x0000D7CF File Offset: 0x0000B9CF
		private IRegionEndpoint InternedRegionEndpoint
		{
			get
			{
				return RegionEndpoint.RegionEndpointProvider.GetRegionEndpoint(this.SystemName);
			}
		}

		// Token: 0x0600050A RID: 1290 RVA: 0x0000D7E1 File Offset: 0x0000B9E1
		public RegionEndpoint.Endpoint GetEndpointForService(string serviceName)
		{
			return this.GetEndpointForService(serviceName, false);
		}

		// Token: 0x0600050B RID: 1291 RVA: 0x0000D7EB File Offset: 0x0000B9EB
		public RegionEndpoint.Endpoint GetEndpointForService(string serviceName, bool dualStack)
		{
			return this.InternedRegionEndpoint.GetEndpointForService(serviceName, dualStack);
		}

		// Token: 0x0600050C RID: 1292 RVA: 0x0000D7FA File Offset: 0x0000B9FA
		public override string ToString()
		{
			return string.Format(CultureInfo.InvariantCulture, "{0} ({1})", new object[]
			{
				this.DisplayName,
				this.SystemName
			});
		}

		// Token: 0x040001B1 RID: 433
		private static Dictionary<string, RegionEndpoint> _hashBySystemName = new Dictionary<string, RegionEndpoint>(StringComparer.OrdinalIgnoreCase);

		// Token: 0x040001B2 RID: 434
		public static readonly RegionEndpoint USEast1 = RegionEndpoint.NewEndpoint("us-east-1", "US East (Virginia)");

		// Token: 0x040001B3 RID: 435
		public static readonly RegionEndpoint USEast2 = RegionEndpoint.NewEndpoint("us-east-2", "US East (Ohio)");

		// Token: 0x040001B4 RID: 436
		public static readonly RegionEndpoint USWest1 = RegionEndpoint.NewEndpoint("us-west-1", "US West (N. California)");

		// Token: 0x040001B5 RID: 437
		public static readonly RegionEndpoint USWest2 = RegionEndpoint.NewEndpoint("us-west-2", "US West (Oregon)");

		// Token: 0x040001B6 RID: 438
		public static readonly RegionEndpoint EUWest1 = RegionEndpoint.NewEndpoint("eu-west-1", "EU West (Ireland)");

		// Token: 0x040001B7 RID: 439
		public static readonly RegionEndpoint EUWest2 = RegionEndpoint.NewEndpoint("eu-west-2", "EU West (London)");

		// Token: 0x040001B8 RID: 440
		public static readonly RegionEndpoint EUCentral1 = RegionEndpoint.NewEndpoint("eu-central-1", "EU Central (Frankfurt)");

		// Token: 0x040001B9 RID: 441
		public static readonly RegionEndpoint APNortheast1 = RegionEndpoint.NewEndpoint("ap-northeast-1", "Asia Pacific (Tokyo)");

		// Token: 0x040001BA RID: 442
		public static readonly RegionEndpoint APNortheast2 = RegionEndpoint.NewEndpoint("ap-northeast-2", "Asia Pacific (Seoul)");

		// Token: 0x040001BB RID: 443
		public static readonly RegionEndpoint APSouth1 = RegionEndpoint.NewEndpoint("ap-south-1", "Asia Pacific (Mumbai)");

		// Token: 0x040001BC RID: 444
		public static readonly RegionEndpoint APSoutheast1 = RegionEndpoint.NewEndpoint("ap-southeast-1", "Asia Pacific (Singapore)");

		// Token: 0x040001BD RID: 445
		public static readonly RegionEndpoint APSoutheast2 = RegionEndpoint.NewEndpoint("ap-southeast-2", "Asia Pacific (Sydney)");

		// Token: 0x040001BE RID: 446
		public static readonly RegionEndpoint SAEast1 = RegionEndpoint.NewEndpoint("sa-east-1", "South America (Sao Paulo)");

		// Token: 0x040001BF RID: 447
		public static readonly RegionEndpoint USGovCloudWest1 = RegionEndpoint.NewEndpoint("us-gov-west-1", "US GovCloud West (Oregon)");

		// Token: 0x040001C0 RID: 448
		public static readonly RegionEndpoint CNNorth1 = RegionEndpoint.NewEndpoint("cn-north-1", "China (Beijing)");

		// Token: 0x040001C1 RID: 449
		public static readonly RegionEndpoint CACentral1 = RegionEndpoint.NewEndpoint("ca-central-1", "Canada (Central)");

		// Token: 0x040001C2 RID: 450
		private static IRegionEndpointProvider _regionEndpointProvider;

		// Token: 0x020001BD RID: 445
		public class Endpoint
		{
			// Token: 0x06000EA1 RID: 3745 RVA: 0x000266A6 File Offset: 0x000248A6
			internal Endpoint(string hostname, string authregion, string signatureVersionOverride)
			{
				this.Hostname = hostname;
				this.AuthRegion = authregion;
				this.SignatureVersionOverride = signatureVersionOverride;
			}

			// Token: 0x17000412 RID: 1042
			// (get) Token: 0x06000EA2 RID: 3746 RVA: 0x000266C3 File Offset: 0x000248C3
			// (set) Token: 0x06000EA3 RID: 3747 RVA: 0x000266CB File Offset: 0x000248CB
			public string Hostname { get; private set; }

			// Token: 0x17000413 RID: 1043
			// (get) Token: 0x06000EA4 RID: 3748 RVA: 0x000266D4 File Offset: 0x000248D4
			// (set) Token: 0x06000EA5 RID: 3749 RVA: 0x000266DC File Offset: 0x000248DC
			public string AuthRegion { get; private set; }

			// Token: 0x06000EA6 RID: 3750 RVA: 0x000266E5 File Offset: 0x000248E5
			public override string ToString()
			{
				return this.Hostname;
			}

			// Token: 0x17000414 RID: 1044
			// (get) Token: 0x06000EA7 RID: 3751 RVA: 0x000266ED File Offset: 0x000248ED
			// (set) Token: 0x06000EA8 RID: 3752 RVA: 0x000266F5 File Offset: 0x000248F5
			public string SignatureVersionOverride { get; private set; }
		}
	}
}
