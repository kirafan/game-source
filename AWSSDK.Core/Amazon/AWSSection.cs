﻿using System;
using System.Collections.Generic;
using System.Xml.Linq;

namespace Amazon
{
	// Token: 0x02000069 RID: 105
	internal class AWSSection
	{
		// Token: 0x17000163 RID: 355
		// (get) Token: 0x060004D5 RID: 1237 RVA: 0x0000D51C File Offset: 0x0000B71C
		// (set) Token: 0x060004D6 RID: 1238 RVA: 0x0000D524 File Offset: 0x0000B724
		public LoggingSection Logging { get; set; }

		// Token: 0x17000164 RID: 356
		// (get) Token: 0x060004D7 RID: 1239 RVA: 0x0000D52D File Offset: 0x0000B72D
		// (set) Token: 0x060004D8 RID: 1240 RVA: 0x0000D535 File Offset: 0x0000B735
		public string EndpointDefinition { get; set; }

		// Token: 0x17000165 RID: 357
		// (get) Token: 0x060004D9 RID: 1241 RVA: 0x0000D53E File Offset: 0x0000B73E
		// (set) Token: 0x060004DA RID: 1242 RVA: 0x0000D546 File Offset: 0x0000B746
		public string Region { get; set; }

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x060004DB RID: 1243 RVA: 0x0000D54F File Offset: 0x0000B74F
		// (set) Token: 0x060004DC RID: 1244 RVA: 0x0000D557 File Offset: 0x0000B757
		public bool? UseSdkCache { get; set; }

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x060004DD RID: 1245 RVA: 0x0000D560 File Offset: 0x0000B760
		// (set) Token: 0x060004DE RID: 1246 RVA: 0x0000D568 File Offset: 0x0000B768
		public bool? CorrectForClockSkew { get; set; }

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x060004DF RID: 1247 RVA: 0x0000D571 File Offset: 0x0000B771
		// (set) Token: 0x060004E0 RID: 1248 RVA: 0x0000D579 File Offset: 0x0000B779
		public ProxySection Proxy { get; set; }

		// Token: 0x17000169 RID: 361
		// (get) Token: 0x060004E1 RID: 1249 RVA: 0x0000D582 File Offset: 0x0000B782
		// (set) Token: 0x060004E2 RID: 1250 RVA: 0x0000D58A File Offset: 0x0000B78A
		public string ProfileName { get; set; }

		// Token: 0x1700016A RID: 362
		// (get) Token: 0x060004E3 RID: 1251 RVA: 0x0000D593 File Offset: 0x0000B793
		// (set) Token: 0x060004E4 RID: 1252 RVA: 0x0000D59B File Offset: 0x0000B79B
		public string ProfilesLocation { get; set; }

		// Token: 0x1700016B RID: 363
		// (get) Token: 0x060004E5 RID: 1253 RVA: 0x0000D5A4 File Offset: 0x0000B7A4
		// (set) Token: 0x060004E6 RID: 1254 RVA: 0x0000D5AC File Offset: 0x0000B7AC
		public string ApplicationName { get; set; }

		// Token: 0x1700016C RID: 364
		// (get) Token: 0x060004E7 RID: 1255 RVA: 0x0000D5B5 File Offset: 0x0000B7B5
		// (set) Token: 0x060004E8 RID: 1256 RVA: 0x0000D5D5 File Offset: 0x0000B7D5
		public IDictionary<string, XElement> ServiceSections
		{
			get
			{
				if (this._serviceSections == null)
				{
					this._serviceSections = new Dictionary<string, XElement>(StringComparer.Ordinal);
				}
				return this._serviceSections;
			}
			set
			{
				this._serviceSections = value;
			}
		}

		// Token: 0x0400019C RID: 412
		private IDictionary<string, XElement> _serviceSections;
	}
}
