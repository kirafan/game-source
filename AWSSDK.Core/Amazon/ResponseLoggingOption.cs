﻿using System;

namespace Amazon
{
	// Token: 0x02000067 RID: 103
	public enum ResponseLoggingOption
	{
		// Token: 0x0400018D RID: 397
		Never,
		// Token: 0x0400018E RID: 398
		OnError,
		// Token: 0x0400018F RID: 399
		Always
	}
}
