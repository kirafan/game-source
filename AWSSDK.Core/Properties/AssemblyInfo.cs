﻿using System;
using System.Diagnostics;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Security;
using System.Security.Permissions;

[assembly: AssemblyVersion("3.3.0.0")]
[assembly: AssemblyTitle("AWSSDK.Core")]
[assembly: AssemblyDescription("The Amazon Web Services SDK for .NET (Unity)- Core Runtime")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyProduct("Amazon Web Services SDK for .NET")]
[assembly: AssemblyCompany("Amazon.com, Inc")]
[assembly: AssemblyCopyright("Copyright 2009-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.")]
[assembly: AssemblyTrademark("")]
[assembly: ComVisible(false)]
[assembly: AssemblyFileVersion("3.3.14.0")]
[assembly: CLSCompliant(false)]
[assembly: SecurityPermission(SecurityAction.RequestMinimum, SkipVerification = true)]
