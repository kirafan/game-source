﻿using System;
using System.Runtime.Serialization;

namespace System.IO
{
	// Token: 0x02000064 RID: 100
	[Serializable]
	public sealed class InvalidDataException : SystemException
	{
		// Token: 0x0600049E RID: 1182 RVA: 0x0000C8F0 File Offset: 0x0000AAF0
		public InvalidDataException()
		{
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x0000C8F8 File Offset: 0x0000AAF8
		public InvalidDataException(string message) : base(message)
		{
		}

		// Token: 0x060004A0 RID: 1184 RVA: 0x0000C901 File Offset: 0x0000AB01
		public InvalidDataException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060004A1 RID: 1185 RVA: 0x0000C90B File Offset: 0x0000AB0B
		internal InvalidDataException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
