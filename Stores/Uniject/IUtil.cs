﻿using System;
using System.Collections;
using UnityEngine;

namespace Uniject
{
	// Token: 0x0200004E RID: 78
	internal interface IUtil
	{
		// Token: 0x1700001A RID: 26
		// (get) Token: 0x06000119 RID: 281
		RuntimePlatform platform { get; }

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600011A RID: 282
		bool isEditor { get; }

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600011B RID: 283
		string persistentDataPath { get; }

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600011C RID: 284
		DateTime currentTime { get; }

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600011D RID: 285
		string deviceModel { get; }

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x0600011E RID: 286
		string deviceName { get; }

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x0600011F RID: 287
		DeviceType deviceType { get; }

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x06000120 RID: 288
		string operatingSystem { get; }

		// Token: 0x06000121 RID: 289
		T[] GetAnyComponentsOfType<T>() where T : class;

		// Token: 0x06000122 RID: 290
		object InitiateCoroutine(IEnumerator start);

		// Token: 0x06000123 RID: 291
		object GetWaitForSeconds(int seconds);

		// Token: 0x06000124 RID: 292
		void InitiateCoroutine(IEnumerator start, int delayInSeconds);

		// Token: 0x06000125 RID: 293
		void RunOnMainThread(Action runnable);

		// Token: 0x06000126 RID: 294
		void AddPauseListener(Action<bool> runnable);
	}
}
