﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003A RID: 58
	public enum RestoreTransactionIDState
	{
		// Token: 0x04000057 RID: 87
		NoTransactionRestore,
		// Token: 0x04000058 RID: 88
		RestoreSucceed,
		// Token: 0x04000059 RID: 89
		RestoreFailed,
		// Token: 0x0400005A RID: 90
		NotKnown
	}
}
