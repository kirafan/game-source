﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000054 RID: 84
	[Serializable]
	public class ProductCatalogItem
	{
		// Token: 0x06000137 RID: 311 RVA: 0x0000751C File Offset: 0x0000591C
		public ProductCatalogItem Clone()
		{
			ProductCatalogItem productCatalogItem = new ProductCatalogItem();
			productCatalogItem.id = this.id;
			productCatalogItem.type = this.type;
			productCatalogItem.SetStoreIDs(this.allStoreIDs);
			productCatalogItem.defaultDescription = this.defaultDescription.Clone();
			productCatalogItem.screenshotPath = this.screenshotPath;
			productCatalogItem.applePriceTier = this.applePriceTier;
			productCatalogItem.googlePrice.value = this.googlePrice.value;
			productCatalogItem.pricingTemplateID = this.pricingTemplateID;
			foreach (LocalizedProductDescription localizedProductDescription in this.descriptions)
			{
				productCatalogItem.descriptions.Add(localizedProductDescription.Clone());
			}
			return productCatalogItem;
		}

		// Token: 0x06000138 RID: 312 RVA: 0x00007604 File Offset: 0x00005A04
		public void SetStoreID(string aStore, string aId)
		{
			this.storeIDs.RemoveAll((StoreID obj) => obj.store == aStore);
			if (!string.IsNullOrEmpty(aId))
			{
				this.storeIDs.Add(new StoreID(aStore, aId));
			}
		}

		// Token: 0x06000139 RID: 313 RVA: 0x0000765C File Offset: 0x00005A5C
		public string GetStoreID(string store)
		{
			StoreID storeID = this.storeIDs.Find((StoreID obj) => obj.store == store);
			return (storeID != null) ? storeID.id : null;
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x0600013A RID: 314 RVA: 0x000076A8 File Offset: 0x00005AA8
		public ICollection<StoreID> allStoreIDs
		{
			get
			{
				return this.storeIDs;
			}
		}

		// Token: 0x0600013B RID: 315 RVA: 0x000076C4 File Offset: 0x00005AC4
		public void SetStoreIDs(ICollection<StoreID> storeIds)
		{
			using (IEnumerator<StoreID> enumerator = storeIds.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					StoreID storeId = enumerator.Current;
					this.storeIDs.RemoveAll((StoreID obj) => obj.store == storeId.store);
					if (!string.IsNullOrEmpty(storeId.id))
					{
						this.storeIDs.Add(new StoreID(storeId.store, storeId.id));
					}
				}
			}
		}

		// Token: 0x0600013C RID: 316 RVA: 0x00007774 File Offset: 0x00005B74
		public LocalizedProductDescription GetDescription(TranslationLocale locale)
		{
			return this.descriptions.Find((LocalizedProductDescription obj) => obj.googleLocale == locale);
		}

		// Token: 0x0600013D RID: 317 RVA: 0x000077B0 File Offset: 0x00005BB0
		public LocalizedProductDescription GetOrCreateDescription(TranslationLocale locale)
		{
			return this.GetDescription(locale) ?? this.AddDescription(locale);
		}

		// Token: 0x0600013E RID: 318 RVA: 0x000077DC File Offset: 0x00005BDC
		public LocalizedProductDescription AddDescription(TranslationLocale locale)
		{
			this.RemoveDescription(locale);
			LocalizedProductDescription localizedProductDescription = new LocalizedProductDescription();
			localizedProductDescription.googleLocale = locale;
			this.descriptions.Add(localizedProductDescription);
			return localizedProductDescription;
		}

		// Token: 0x0600013F RID: 319 RVA: 0x00007814 File Offset: 0x00005C14
		public void RemoveDescription(TranslationLocale locale)
		{
			this.descriptions.RemoveAll((LocalizedProductDescription obj) => obj.googleLocale == locale);
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x06000140 RID: 320 RVA: 0x00007848 File Offset: 0x00005C48
		public bool HasAvailableGoogleLocale
		{
			get
			{
				return Enum.GetValues(typeof(TranslationLocale)).Length > this.descriptions.Count + 1;
			}
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000141 RID: 321 RVA: 0x00007880 File Offset: 0x00005C80
		public TranslationLocale NextAvailableGoogleLocale
		{
			get
			{
				IEnumerator enumerator = Enum.GetValues(typeof(TranslationLocale)).GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						TranslationLocale translationLocale = (TranslationLocale)obj;
						if (this.GetDescription(translationLocale) == null)
						{
							return translationLocale;
						}
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				return TranslationLocale.en_US;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000142 RID: 322 RVA: 0x00007908 File Offset: 0x00005D08
		public ICollection<LocalizedProductDescription> translatedDescriptions
		{
			get
			{
				return this.descriptions;
			}
		}

		// Token: 0x040000C6 RID: 198
		public string id;

		// Token: 0x040000C7 RID: 199
		public ProductType type;

		// Token: 0x040000C8 RID: 200
		[SerializeField]
		private List<StoreID> storeIDs = new List<StoreID>();

		// Token: 0x040000C9 RID: 201
		public LocalizedProductDescription defaultDescription = new LocalizedProductDescription();

		// Token: 0x040000CA RID: 202
		public string screenshotPath;

		// Token: 0x040000CB RID: 203
		public int applePriceTier = 0;

		// Token: 0x040000CC RID: 204
		public int xiaomiPriceTier = 0;

		// Token: 0x040000CD RID: 205
		public Price googlePrice = new Price();

		// Token: 0x040000CE RID: 206
		public string pricingTemplateID;

		// Token: 0x040000CF RID: 207
		[SerializeField]
		private List<LocalizedProductDescription> descriptions = new List<LocalizedProductDescription>();
	}
}
