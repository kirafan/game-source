﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000015 RID: 21
	public interface ISamsungAppsConfiguration : IStoreConfiguration
	{
		// Token: 0x0600003E RID: 62
		void SetMode(SamsungAppsMode mode);
	}
}
