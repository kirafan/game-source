﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200005B RID: 91
	internal class StoreConfiguration
	{
		// Token: 0x0600016F RID: 367 RVA: 0x000084BD File Offset: 0x000068BD
		public StoreConfiguration(AndroidStore store)
		{
			this.androidStore = store;
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x06000170 RID: 368 RVA: 0x000084D0 File Offset: 0x000068D0
		// (set) Token: 0x06000171 RID: 369 RVA: 0x000084EA File Offset: 0x000068EA
		public AndroidStore androidStore { get; private set; }

		// Token: 0x06000172 RID: 370 RVA: 0x000084F4 File Offset: 0x000068F4
		public static string Serialize(StoreConfiguration store)
		{
			Dictionary<string, object> json = new Dictionary<string, object>
			{
				{
					"androidStore",
					store.androidStore.ToString()
				}
			};
			return MiniJson.JsonEncode(json);
		}

		// Token: 0x06000173 RID: 371 RVA: 0x00008538 File Offset: 0x00006938
		public static StoreConfiguration Deserialize(string json)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(json);
			string value = (string)dictionary["androidStore"];
			AndroidStore store;
			if (!Enum.IsDefined(typeof(AndroidStore), value))
			{
				store = AndroidStore.GooglePlay;
			}
			else
			{
				store = (AndroidStore)Enum.Parse(typeof(AndroidStore), value, true);
			}
			return new StoreConfiguration(store);
		}
	}
}
