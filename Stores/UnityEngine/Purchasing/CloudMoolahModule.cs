﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000032 RID: 50
	public class CloudMoolahModule : IPurchasingModule
	{
		// Token: 0x0600009A RID: 154 RVA: 0x000038DE File Offset: 0x00001CDE
		public void Configure(IPurchasingBinder binder)
		{
			Debug.Log("CloudMoolah Configure...");
			binder.RegisterStore("MoolahAppStore", this.InstantiateMoolahAppStore(binder));
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00003900 File Offset: 0x00001D00
		private IStore InstantiateMoolahAppStore(IPurchasingBinder binder)
		{
			IStore result;
			if (this.IsSupportPlatform())
			{
				GameObject gameObject = GameObject.Find("IAPUtil");
				MoolahStoreImpl moolahStoreImpl = gameObject.AddComponent<MoolahStoreImpl>();
				binder.RegisterExtension<IMoolahExtension>(moolahStoreImpl);
				binder.RegisterConfiguration<IMoolahConfiguration>(moolahStoreImpl);
				result = moolahStoreImpl;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x0600009C RID: 156 RVA: 0x0000394C File Offset: 0x00001D4C
		private bool IsSupportPlatform()
		{
			return Application.platform == RuntimePlatform.Android;
		}
	}
}
