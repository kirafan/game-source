﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000044 RID: 68
	internal class FakeUnityChannelConfiguration : IUnityChannelConfiguration, IStoreConfiguration
	{
		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060000EC RID: 236 RVA: 0x00006350 File Offset: 0x00004750
		// (set) Token: 0x060000ED RID: 237 RVA: 0x0000636A File Offset: 0x0000476A
		public bool fetchReceiptPayloadOnPurchase { get; set; }
	}
}
