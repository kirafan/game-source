﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003C RID: 60
	internal enum TradeSeqState
	{
		// Token: 0x04000061 RID: 97
		PAY_FAILED,
		// Token: 0x04000062 RID: 98
		PAY_CONFIRM,
		// Token: 0x04000063 RID: 99
		ORDER_SUCCEED,
		// Token: 0x04000064 RID: 100
		PAY_INIT,
		// Token: 0x04000065 RID: 101
		PAY_SELECTED_CHANNEL,
		// Token: 0x04000066 RID: 102
		NOTKNOWN
	}
}
