﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004C RID: 76
	internal interface IFakeExtensions : IStoreExtension
	{
		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000112 RID: 274
		// (set) Token: 0x06000113 RID: 275
		string unavailableProductId { get; set; }
	}
}
