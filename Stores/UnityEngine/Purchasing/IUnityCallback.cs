﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000004 RID: 4
	internal interface IUnityCallback
	{
		// Token: 0x06000007 RID: 7
		void OnSetupFailed(string json);

		// Token: 0x06000008 RID: 8
		void OnProductsRetrieved(string json);

		// Token: 0x06000009 RID: 9
		void OnPurchaseSucceeded(string id, string receipt, string transactionID);

		// Token: 0x0600000A RID: 10
		void OnPurchaseFailed(string json);
	}
}
