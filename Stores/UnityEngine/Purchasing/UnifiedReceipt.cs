﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000022 RID: 34
	[Serializable]
	public class UnifiedReceipt
	{
		// Token: 0x0400001F RID: 31
		public string Payload;

		// Token: 0x04000020 RID: 32
		public string Store;

		// Token: 0x04000021 RID: 33
		public string TransactionID;
	}
}
