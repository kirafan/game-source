﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000052 RID: 82
	public static class LocaleExtensions
	{
		// Token: 0x0600012B RID: 299 RVA: 0x00007094 File Offset: 0x00005494
		public static string[] GetLabelsWithSupportedPlatforms()
		{
			string[] labelsWithSupportedPlatforms;
			if (LocaleExtensions.LabelsWithSupportedPlatforms != null)
			{
				labelsWithSupportedPlatforms = LocaleExtensions.LabelsWithSupportedPlatforms;
			}
			else
			{
				LocaleExtensions.LabelsWithSupportedPlatforms = new string[Enum.GetValues(typeof(TranslationLocale)).Length];
				List<TranslationLocale> list = LocaleExtensions.GoogleLocales.ToList<TranslationLocale>();
				List<TranslationLocale> list2 = LocaleExtensions.AppleLocales.ToList<TranslationLocale>();
				List<TranslationLocale> list3 = LocaleExtensions.XiaomiLocales.ToList<TranslationLocale>();
				int num = 0;
				IEnumerator enumerator = Enum.GetValues(typeof(TranslationLocale)).GetEnumerator();
				try
				{
					while (enumerator.MoveNext())
					{
						object obj = enumerator.Current;
						TranslationLocale item = (TranslationLocale)obj;
						List<string> list4 = new List<string>();
						if (list.Contains(item))
						{
							list4.Add("Google Play");
						}
						if (list2.Contains(item))
						{
							list4.Add("Apple");
						}
						if (list3.Contains(item))
						{
							list4.Add(LocaleExtensions.XiaomiMiGamePay);
						}
						string str = string.Join(", ", list4.ToArray());
						LocaleExtensions.LabelsWithSupportedPlatforms[num] = LocaleExtensions.Labels[num] + " (" + str + ")";
						num++;
					}
				}
				finally
				{
					IDisposable disposable;
					if ((disposable = (enumerator as IDisposable)) != null)
					{
						disposable.Dispose();
					}
				}
				labelsWithSupportedPlatforms = LocaleExtensions.LabelsWithSupportedPlatforms;
			}
			return labelsWithSupportedPlatforms;
		}

		// Token: 0x040000BC RID: 188
		public static readonly string XiaomiMiGamePay = "Xiaomi Mi Game Pay";

		// Token: 0x040000BD RID: 189
		private static readonly string[] Labels = new string[]
		{
			"Chinese",
			"Czech",
			"Danish",
			"Dutch",
			"English",
			"French",
			"Finnish",
			"German",
			"Hebrew",
			"Hindi",
			"Italian",
			"Japanese",
			"Korean",
			"Norwegian",
			"Polish",
			"Portuguese",
			"Russian",
			"Spanish",
			"Swedish",
			"Chinese (Simplified)"
		};

		// Token: 0x040000BE RID: 190
		private static readonly TranslationLocale[] GoogleLocales = new TranslationLocale[]
		{
			TranslationLocale.zh_TW,
			TranslationLocale.cs_CZ,
			TranslationLocale.da_DK,
			TranslationLocale.nl_NL,
			TranslationLocale.en_US,
			TranslationLocale.fr_FR,
			TranslationLocale.fi_FI,
			TranslationLocale.de_DE,
			TranslationLocale.iw_IL,
			TranslationLocale.hi_IN,
			TranslationLocale.it_IT,
			TranslationLocale.ja_JP,
			TranslationLocale.ko_KR,
			TranslationLocale.no_NO,
			TranslationLocale.pl_PL,
			TranslationLocale.pt_PT,
			TranslationLocale.ru_RU,
			TranslationLocale.es_ES,
			TranslationLocale.sv_SE
		};

		// Token: 0x040000BF RID: 191
		private static readonly TranslationLocale[] AppleLocales = new TranslationLocale[0];

		// Token: 0x040000C0 RID: 192
		private static readonly TranslationLocale[] XiaomiLocales = new TranslationLocale[]
		{
			TranslationLocale.zh_CN
		};

		// Token: 0x040000C1 RID: 193
		private static string[] LabelsWithSupportedPlatforms;
	}
}
