﻿using System;
using Uniject;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000009 RID: 9
	internal class ScriptingUnityCallback : IUnityCallback
	{
		// Token: 0x06000023 RID: 35 RVA: 0x000026BA File Offset: 0x00000ABA
		public ScriptingUnityCallback(IUnityCallback forwardTo, IUtil util)
		{
			this.forwardTo = forwardTo;
			this.util = util;
		}

		// Token: 0x06000024 RID: 36 RVA: 0x000026D4 File Offset: 0x00000AD4
		public void OnSetupFailed(string json)
		{
			this.util.RunOnMainThread(delegate
			{
				this.forwardTo.OnSetupFailed(json);
			});
		}

		// Token: 0x06000025 RID: 37 RVA: 0x00002710 File Offset: 0x00000B10
		public void OnProductsRetrieved(string json)
		{
			this.util.RunOnMainThread(delegate
			{
				this.forwardTo.OnProductsRetrieved(json);
			});
		}

		// Token: 0x06000026 RID: 38 RVA: 0x0000274C File Offset: 0x00000B4C
		public void OnPurchaseSucceeded(string id, string receipt, string transactionID)
		{
			this.util.RunOnMainThread(delegate
			{
				this.forwardTo.OnPurchaseSucceeded(id, receipt, transactionID);
			});
		}

		// Token: 0x06000027 RID: 39 RVA: 0x00002794 File Offset: 0x00000B94
		public void OnPurchaseFailed(string json)
		{
			this.util.RunOnMainThread(delegate
			{
				this.forwardTo.OnPurchaseFailed(json);
			});
		}

		// Token: 0x04000005 RID: 5
		private IUnityCallback forwardTo;

		// Token: 0x04000006 RID: 6
		private IUtil util;
	}
}
