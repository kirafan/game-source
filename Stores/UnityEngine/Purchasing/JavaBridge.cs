﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000007 RID: 7
	internal class JavaBridge : AndroidJavaProxy, IUnityCallback
	{
		// Token: 0x06000013 RID: 19 RVA: 0x00002575 File Offset: 0x00000975
		public JavaBridge(IUnityCallback forwardTo) : base("com.unity.purchasing.common.IUnityCallback")
		{
			this.forwardTo = forwardTo;
		}

		// Token: 0x06000014 RID: 20 RVA: 0x0000258A File Offset: 0x0000098A
		public JavaBridge(IUnityCallback forwardTo, string javaInterface) : base(javaInterface)
		{
			this.forwardTo = forwardTo;
		}

		// Token: 0x06000015 RID: 21 RVA: 0x0000259B File Offset: 0x0000099B
		public void OnSetupFailed(string json)
		{
			this.forwardTo.OnSetupFailed(json);
		}

		// Token: 0x06000016 RID: 22 RVA: 0x000025AA File Offset: 0x000009AA
		public void OnProductsRetrieved(string json)
		{
			this.forwardTo.OnProductsRetrieved(json);
		}

		// Token: 0x06000017 RID: 23 RVA: 0x000025B9 File Offset: 0x000009B9
		public void OnPurchaseSucceeded(string id, string receipt, string transactionID)
		{
			this.forwardTo.OnPurchaseSucceeded(id, receipt, transactionID);
		}

		// Token: 0x06000018 RID: 24 RVA: 0x000025CA File Offset: 0x000009CA
		public void OnPurchaseFailed(string json)
		{
			this.forwardTo.OnPurchaseFailed(json);
		}

		// Token: 0x04000002 RID: 2
		private IUnityCallback forwardTo;
	}
}
