﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003D RID: 61
	public interface IMoolahExtension : IStoreExtension
	{
		// Token: 0x060000AE RID: 174
		void Login(string CMUserName, string CMPassword, Action<LoginResultState, string> LoginResult);

		// Token: 0x060000AF RID: 175
		void FastRegister(string CMPassword, Action<string> registerSucceed, Action<FastRegisterError, string> registerFailed);

		// Token: 0x060000B0 RID: 176
		void RestoreTransactionID(Action<RestoreTransactionIDState> result);

		// Token: 0x060000B1 RID: 177
		void ValidateReceipt(string transactionId, string receipt, Action<string, ValidateReceiptState, string> result);

		// Token: 0x060000B2 RID: 178
		void RequestPayOut(string transactionId, Action<string, RequestPayOutState, string> result);
	}
}
