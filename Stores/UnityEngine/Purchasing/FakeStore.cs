﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004A RID: 74
	internal class FakeStore : AbstractStore, IFakeExtensions, IStoreExtension
	{
		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000109 RID: 265 RVA: 0x00006C00 File Offset: 0x00005000
		// (set) Token: 0x0600010A RID: 266 RVA: 0x00006C1A File Offset: 0x0000501A
		public string unavailableProductId { get; set; }

		// Token: 0x0600010B RID: 267 RVA: 0x00006C23 File Offset: 0x00005023
		public override void Initialize(IStoreCallback biller)
		{
			this.m_Biller = biller;
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00006C30 File Offset: 0x00005030
		public override void RetrieveProducts(ReadOnlyCollection<ProductDefinition> productDefinitions)
		{
			List<ProductDescription> products = new List<ProductDescription>();
			foreach (ProductDefinition productDefinition in productDefinitions)
			{
				if (this.unavailableProductId != productDefinition.id)
				{
					ProductMetadata metadata = new ProductMetadata("$0.01", "Fake title for " + productDefinition.id, "Fake description", "USD", 0.01m);
					ProductCatalog productCatalog = ProductCatalog.LoadDefaultCatalog();
					if (productCatalog != null)
					{
						foreach (ProductCatalogItem productCatalogItem in productCatalog.allProducts)
						{
							if (productCatalogItem.id == productDefinition.id)
							{
								metadata = new ProductMetadata(productCatalogItem.googlePrice.value.ToString(), productCatalogItem.defaultDescription.Title, productCatalogItem.defaultDescription.Description, string.Empty, productCatalogItem.googlePrice.value);
							}
						}
					}
					products.Add(new ProductDescription(productDefinition.storeSpecificId, metadata));
				}
			}
			Action<bool, InitializationFailureReason> action = delegate(bool allow, InitializationFailureReason failureReason)
			{
				if (allow)
				{
					this.m_Biller.OnProductsRetrieved(products);
				}
				else
				{
					this.m_Biller.OnSetupFailed(failureReason);
				}
			};
			if (this.UIMode != FakeStoreUIMode.DeveloperUser || !this.StartUI<InitializationFailureReason>(productDefinitions, FakeStore.DialogType.RetrieveProducts, action))
			{
				action(true, InitializationFailureReason.AppNotKnown);
			}
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00006E00 File Offset: 0x00005200
		public override void Purchase(ProductDefinition product, string developerPayload)
		{
			this.purchaseCalled = true;
			if (product.type != ProductType.Consumable)
			{
				this.m_PurchasedProducts.Add(product.storeSpecificId);
			}
			Action<bool, PurchaseFailureReason> action = delegate(bool allow, PurchaseFailureReason failureReason)
			{
				if (allow)
				{
					this.m_Biller.OnPurchaseSucceeded(product.storeSpecificId, "{ \"this\" : \"is a fake receipt\" }", Guid.NewGuid().ToString());
				}
				else
				{
					if (failureReason == (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"))
					{
						failureReason = PurchaseFailureReason.UserCancelled;
					}
					PurchaseFailureDescription desc = new PurchaseFailureDescription(product.id, failureReason, "failed a fake store purchase");
					this.m_Biller.OnPurchaseFailed(desc);
				}
			};
			if (!this.StartUI<PurchaseFailureReason>(product, FakeStore.DialogType.Purchase, action))
			{
				action(true, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"));
			}
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00006E94 File Offset: 0x00005294
		public void RestoreTransactions(Action<bool> callback)
		{
			this.restoreCalled = true;
			foreach (string storeSpecificId in this.m_PurchasedProducts)
			{
				this.m_Biller.OnPurchaseSucceeded(storeSpecificId, "{ \"this\" : \"is a fake receipt\" }", "1");
			}
			callback(true);
		}

		// Token: 0x0600010F RID: 271 RVA: 0x00006F10 File Offset: 0x00005310
		public override void FinishTransaction(ProductDefinition product, string transactionId)
		{
		}

		// Token: 0x06000110 RID: 272 RVA: 0x00006F13 File Offset: 0x00005313
		public void RegisterPurchaseForRestore(string productId)
		{
			this.m_PurchasedProducts.Add(productId);
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00006F24 File Offset: 0x00005324
		protected virtual bool StartUI<T>(object model, FakeStore.DialogType dialogType, Action<bool, T> callback)
		{
			return false;
		}

		// Token: 0x04000098 RID: 152
		public const string Name = "fake";

		// Token: 0x04000099 RID: 153
		private IStoreCallback m_Biller;

		// Token: 0x0400009A RID: 154
		private List<string> m_PurchasedProducts = new List<string>();

		// Token: 0x0400009B RID: 155
		public bool purchaseCalled;

		// Token: 0x0400009C RID: 156
		public bool restoreCalled;

		// Token: 0x0400009E RID: 158
		public FakeStoreUIMode UIMode = FakeStoreUIMode.Default;

		// Token: 0x0200004B RID: 75
		protected enum DialogType
		{
			// Token: 0x040000A0 RID: 160
			Purchase,
			// Token: 0x040000A1 RID: 161
			RetrieveProducts
		}
	}
}
