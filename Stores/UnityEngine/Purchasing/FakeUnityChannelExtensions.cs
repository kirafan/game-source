﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000043 RID: 67
	public class FakeUnityChannelExtensions : IUnityChannelExtensions, IStoreExtension
	{
		// Token: 0x060000E8 RID: 232 RVA: 0x00006304 File Offset: 0x00004704
		public void ConfirmPurchase(string transactionId, Action<bool, string, string> callback)
		{
			callback(true, "fakeTransactionId", "fakeMessage");
		}

		// Token: 0x060000E9 RID: 233 RVA: 0x00006318 File Offset: 0x00004718
		public void ValidateReceipt(string transactionId, Action<bool, string, string> callback)
		{
			callback(true, "fakeSignData", "fakeSignature");
		}

		// Token: 0x060000EA RID: 234 RVA: 0x0000632C File Offset: 0x0000472C
		public string GetLastPurchaseError()
		{
			return "{ \"error\": \"DuplicateTransaction\" }";
		}
	}
}
