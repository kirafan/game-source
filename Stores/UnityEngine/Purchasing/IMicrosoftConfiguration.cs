﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000026 RID: 38
	public interface IMicrosoftConfiguration : IStoreConfiguration
	{
		// Token: 0x1700000E RID: 14
		// (get) Token: 0x06000073 RID: 115
		// (set) Token: 0x06000074 RID: 116
		bool useMockBillingSystem { get; set; }
	}
}
