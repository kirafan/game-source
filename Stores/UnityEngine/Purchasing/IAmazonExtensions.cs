﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000E RID: 14
	public interface IAmazonExtensions : IStoreExtension
	{
		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000032 RID: 50
		string amazonUserId { get; }

		// Token: 0x06000033 RID: 51
		void NotifyUnableToFulfillUnavailableProduct(string transactionID);
	}
}
