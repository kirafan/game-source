﻿using System;
using System.Collections.Generic;
using MiniJSON;
using UnityEngine.ChannelPurchase;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000046 RID: 70
	internal class UnityChannelBindings : IPurchaseListener
	{
		// Token: 0x060000F2 RID: 242 RVA: 0x0000637C File Offset: 0x0000477C
		public void OnPurchase(PurchaseInfo purchaseInfo)
		{
			Dictionary<string, string> obj = UnityChannelBindings.PurchaseInfoToDictionary(purchaseInfo);
			string arg = obj.toJson();
			this.m_PurchaseCallback(true, arg);
			this.m_PurchaseCallback = null;
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x000063AC File Offset: 0x000047AC
		public void OnPurchaseFailed(string message, PurchaseInfo purchaseInfo)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["error"] = message;
			if (purchaseInfo != null)
			{
				dictionary["purchaseInfo"] = UnityChannelBindings.PurchaseInfoToDictionary(purchaseInfo);
			}
			string arg = dictionary.toJson();
			this.m_PurchaseCallback(false, arg);
			this.m_PurchaseCallback = null;
		}

		// Token: 0x060000F4 RID: 244 RVA: 0x00006400 File Offset: 0x00004800
		public void OnPurchaseRepeated(string productCode)
		{
			Dictionary<string, object> dictionary = new Dictionary<string, object>();
			dictionary["error"] = "repeat";
			dictionary["isRepeat"] = true;
			string arg = dictionary.toJson();
			this.m_PurchaseCallback(false, arg);
			this.m_PurchaseCallback = null;
		}

		// Token: 0x060000F5 RID: 245 RVA: 0x00006450 File Offset: 0x00004850
		public void OnPurchaseConfirm(string transactionId)
		{
			this.m_PurchaseConfirmCallback(true, transactionId, "");
			this.m_PurchaseConfirmCallback = null;
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x0000646C File Offset: 0x0000486C
		public void OnPurchaseConfirmFailed(string transactionId, string message)
		{
			this.m_PurchaseConfirmCallback(false, transactionId, message);
			this.m_PurchaseConfirmCallback = null;
		}

		// Token: 0x060000F7 RID: 247 RVA: 0x00006484 File Offset: 0x00004884
		public void OnReceiptValidate(ReceiptInfo receiptInfo)
		{
			this.m_ValidateCallback(true, receiptInfo.signData, receiptInfo.signature);
			this.m_ValidateCallback = null;
		}

		// Token: 0x060000F8 RID: 248 RVA: 0x000064A6 File Offset: 0x000048A6
		public void OnReceiptValidateFailed(string transactionId, string message)
		{
			this.m_ValidateCallback(false, message, null);
			this.m_ValidateCallback = null;
		}

		// Token: 0x060000F9 RID: 249 RVA: 0x000064C0 File Offset: 0x000048C0
		public void Purchase(string productId, Action<bool, string> callback)
		{
			if (callback != null)
			{
				if (this.m_PurchaseCallback != null)
				{
					callback(false, "{ \"error\" : \"already purchasing\" }");
				}
				else
				{
					this.m_PurchaseCallback = callback;
					this.m_PurchaseGuid = Guid.NewGuid().ToString();
					PurchaseService.Purchase(productId, this.m_PurchaseGuid, this);
				}
			}
		}

		// Token: 0x060000FA RID: 250 RVA: 0x00006524 File Offset: 0x00004924
		public void ValidateReceipt(string transactionId, Action<bool, string, string> callback)
		{
			if (callback != null)
			{
				if (this.m_ValidateCallback != null)
				{
					callback(false, "{ \"error\" : \"already validating\" }", null);
				}
				else
				{
					this.m_ValidateCallback = callback;
					PurchaseService.ValidateReceipt(transactionId, this);
				}
			}
		}

		// Token: 0x060000FB RID: 251 RVA: 0x0000655F File Offset: 0x0000495F
		public void ConfirmPurchase(string transactionId, Action<bool, string, string> callback)
		{
			if (callback != null)
			{
				if (this.m_PurchaseConfirmCallback != null)
				{
					callback(false, transactionId, "{ \"error\" : \"already confirming purchase\" }");
				}
				else
				{
					this.m_PurchaseConfirmCallback = callback;
					PurchaseService.ConfirmPurchase(transactionId, this);
				}
			}
		}

		// Token: 0x060000FC RID: 252 RVA: 0x0000659C File Offset: 0x0000499C
		private static Dictionary<string, string> PurchaseInfoToDictionary(PurchaseInfo purchaseInfo)
		{
			Dictionary<string, string> dictionary = new Dictionary<string, string>();
			dictionary["gameOrderId"] = purchaseInfo.gameOrderId;
			dictionary["productCode"] = purchaseInfo.productCode;
			dictionary["orderQueryToken"] = purchaseInfo.orderQueryToken;
			return dictionary;
		}

		// Token: 0x04000082 RID: 130
		protected Action<bool, string> m_PurchaseCallback;

		// Token: 0x04000083 RID: 131
		protected string m_PurchaseGuid;

		// Token: 0x04000084 RID: 132
		protected Action<bool, string, string> m_ValidateCallback;

		// Token: 0x04000085 RID: 133
		protected Action<bool, string, string> m_PurchaseConfirmCallback;
	}
}
