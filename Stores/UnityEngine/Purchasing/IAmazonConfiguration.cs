﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000D RID: 13
	public interface IAmazonConfiguration : IStoreConfiguration
	{
		// Token: 0x06000031 RID: 49
		void WriteSandboxJSON(HashSet<ProductDefinition> products);
	}
}
