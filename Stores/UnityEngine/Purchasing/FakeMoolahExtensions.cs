﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000034 RID: 52
	internal class FakeMoolahExtensions : IMoolahExtension, IStoreExtension
	{
		// Token: 0x060000A4 RID: 164 RVA: 0x000039E6 File Offset: 0x00001DE6
		public void Login(string CMUserName, string CMPassword, Action<LoginResultState, string> LoginResult)
		{
			this.m_isLogined = !this.m_isLogined;
			if (this.m_isLogined)
			{
				LoginResult(LoginResultState.LoginSucceed, "Fake LoginSucceed");
			}
			else
			{
				LoginResult(LoginResultState.NotKnown, "Fake NotKnown error");
			}
		}

		// Token: 0x060000A5 RID: 165 RVA: 0x00003A24 File Offset: 0x00001E24
		public void FastRegister(string CMPassword, Action<string> RegisterSucceed, Action<FastRegisterError, string> registerFailed)
		{
			this.m_register = !this.m_register;
			if (this.m_register)
			{
				RegisterSucceed("Fake CMUserName");
			}
			else
			{
				registerFailed(FastRegisterError.NotKnown, "Fake NotKnown error");
			}
		}

		// Token: 0x060000A6 RID: 166 RVA: 0x00003A61 File Offset: 0x00001E61
		public void RestoreTransactionID(Action<RestoreTransactionIDState> result)
		{
			result(RestoreTransactionIDState.RestoreSucceed);
		}

		// Token: 0x060000A7 RID: 167 RVA: 0x00003A6B File Offset: 0x00001E6B
		public void ValidateReceipt(string transactionId, string receipt, Action<string, ValidateReceiptState, string> result)
		{
			result(transactionId, ValidateReceiptState.ValidateSucceed, "Fake Validate Receipt Succeed");
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x00003A7B File Offset: 0x00001E7B
		public void RequestPayOut(string transactionId, Action<string, RequestPayOutState, string> result)
		{
			result(transactionId, RequestPayOutState.RequestPayOutSucceed, "Fake RequestPayOutState Succeed");
		}

		// Token: 0x0400003F RID: 63
		private bool m_isLogined = false;

		// Token: 0x04000040 RID: 64
		private bool m_register = false;
	}
}
