﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000018 RID: 24
	internal class SamsungAppsJavaBridge : AndroidJavaProxy, ISamsungAppsCallback
	{
		// Token: 0x06000041 RID: 65 RVA: 0x000029C2 File Offset: 0x00000DC2
		public SamsungAppsJavaBridge(ISamsungAppsCallback forwardTo) : base("com.unity.purchasing.samsung.ISamsungAppsCallback")
		{
			this.forwardTo = forwardTo;
		}

		// Token: 0x06000042 RID: 66 RVA: 0x000029D7 File Offset: 0x00000DD7
		public void OnTransactionsRestored(bool result)
		{
			this.forwardTo.OnTransactionsRestored(result);
		}

		// Token: 0x0400000B RID: 11
		private ISamsungAppsCallback forwardTo;
	}
}
