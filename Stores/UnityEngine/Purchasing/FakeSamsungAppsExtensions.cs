﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000013 RID: 19
	public class FakeSamsungAppsExtensions : ISamsungAppsExtensions, ISamsungAppsConfiguration, IStoreExtension, IStoreConfiguration
	{
		// Token: 0x0600003B RID: 59 RVA: 0x000029AD File Offset: 0x00000DAD
		public void SetMode(SamsungAppsMode mode)
		{
		}

		// Token: 0x0600003C RID: 60 RVA: 0x000029B0 File Offset: 0x00000DB0
		public void RestoreTransactions(Action<bool> callback)
		{
			callback(true);
		}
	}
}
