﻿using System;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000053 RID: 83
	[Serializable]
	public class LocalizedProductDescription
	{
		// Token: 0x0600012E RID: 302 RVA: 0x00007308 File Offset: 0x00005708
		public LocalizedProductDescription Clone()
		{
			return new LocalizedProductDescription
			{
				googleLocale = this.googleLocale,
				Title = this.Title,
				Description = this.Description
			};
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x0600012F RID: 303 RVA: 0x00007348 File Offset: 0x00005748
		// (set) Token: 0x06000130 RID: 304 RVA: 0x00007368 File Offset: 0x00005768
		public string Title
		{
			get
			{
				return LocalizedProductDescription.DecodeNonLatinCharacters(this.title);
			}
			set
			{
				this.title = LocalizedProductDescription.EncodeNonLatinCharacters(value);
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000131 RID: 305 RVA: 0x00007378 File Offset: 0x00005778
		// (set) Token: 0x06000132 RID: 306 RVA: 0x00007398 File Offset: 0x00005798
		public string Description
		{
			get
			{
				return LocalizedProductDescription.DecodeNonLatinCharacters(this.description);
			}
			set
			{
				this.description = LocalizedProductDescription.EncodeNonLatinCharacters(value);
			}
		}

		// Token: 0x06000133 RID: 307 RVA: 0x000073A8 File Offset: 0x000057A8
		private static string EncodeNonLatinCharacters(string s)
		{
			string result;
			if (s == null)
			{
				result = s;
			}
			else
			{
				StringBuilder stringBuilder = new StringBuilder();
				foreach (char c in s)
				{
					if (c > '\u007f')
					{
						string str = "\\u";
						int num = (int)c;
						string value = str + num.ToString("x4");
						stringBuilder.Append(value);
					}
					else
					{
						stringBuilder.Append(c);
					}
				}
				result = stringBuilder.ToString();
			}
			return result;
		}

		// Token: 0x06000134 RID: 308 RVA: 0x0000743C File Offset: 0x0000583C
		private static string DecodeNonLatinCharacters(string s)
		{
			string result;
			if (s == null)
			{
				result = s;
			}
			else
			{
				result = Regex.Replace(s, "\\\\u(?<Value>[a-zA-Z0-9]{4})", (Match m) => ((char)int.Parse(m.Groups["Value"].Value, NumberStyles.HexNumber)).ToString());
			}
			return result;
		}

		// Token: 0x040000C2 RID: 194
		public TranslationLocale googleLocale = TranslationLocale.en_US;

		// Token: 0x040000C3 RID: 195
		[SerializeField]
		private string title;

		// Token: 0x040000C4 RID: 196
		[SerializeField]
		private string description;
	}
}
