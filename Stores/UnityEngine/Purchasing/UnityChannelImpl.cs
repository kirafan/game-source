﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using MiniJSON;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000047 RID: 71
	internal class UnityChannelImpl : AbstractStore, IUnityChannelConfiguration, IUnityChannelExtensions, IStoreConfiguration, IStoreExtension
	{
		// Token: 0x060000FD RID: 253 RVA: 0x000065EB File Offset: 0x000049EB
		public UnityChannelImpl()
		{
			this.m_Bindings = new UnityChannelBindings();
		}

		// Token: 0x060000FE RID: 254 RVA: 0x0000660A File Offset: 0x00004A0A
		public override void Initialize(IStoreCallback callback)
		{
			this.m_Unity = callback;
		}

		// Token: 0x060000FF RID: 255 RVA: 0x00006614 File Offset: 0x00004A14
		public override void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products)
		{
			Debug.Log("RetrieveProducts");
			List<ProductDescription> list = new List<ProductDescription>();
			ProductCatalog productCatalog = ProductCatalog.LoadDefaultCatalog();
			foreach (ProductCatalogItem productCatalogItem in productCatalog.allProducts)
			{
				foreach (ProductDefinition productDefinition in products)
				{
					if (string.Equals(productCatalogItem.id, productDefinition.id))
					{
						int num = UnityChannelImpl.XiaomiPriceTierPrices[productCatalogItem.xiaomiPriceTier];
						string priceString = string.Format("¥{0:0.00}", num);
						LocalizedProductDescription localizedProductDescription = productCatalogItem.defaultDescription;
						LocalizedProductDescription description = productCatalogItem.GetDescription(TranslationLocale.zh_CN);
						localizedProductDescription = (description ?? localizedProductDescription);
						ProductMetadata metadata = new ProductMetadata(priceString, localizedProductDescription.Title, localizedProductDescription.Description, "CNY", num);
						ProductDescription item = new ProductDescription(productDefinition.storeSpecificId, metadata);
						list.Add(item);
					}
				}
			}
			this.m_Unity.OnProductsRetrieved(list);
		}

		// Token: 0x06000100 RID: 256 RVA: 0x00006768 File Offset: 0x00004B68
		public override void Purchase(ProductDefinition product, string developerPayload)
		{
			this.m_Bindings.Purchase(product.storeSpecificId, delegate(bool purchaseSuccess, string message)
			{
				this.m_LastPurchaseError = "";
				if (purchaseSuccess)
				{
					Dictionary<string, object> dic = message.HashtableFromJson();
					string transactionId = dic.GetString("gameOrderId", "");
					string @string = dic.GetString("productCode", "");
					if (!string.IsNullOrEmpty(transactionId))
					{
						dic["transactionId"] = transactionId;
					}
					if (!string.IsNullOrEmpty(@string))
					{
						dic["storeSpecificId"] = @string;
					}
					if (!product.storeSpecificId.Equals(@string))
					{
						Debug.LogWarningFormat("UnityChannelImpl received mismatching product code for purchase. Expected {0}, received {1}.", new object[]
						{
							product.storeSpecificId,
							@string
						});
					}
					if (this.fetchReceiptPayloadOnPurchase)
					{
						this.ValidateReceipt(transactionId, delegate(bool success, string signData, string signature)
						{
							if (success)
							{
								dic["json"] = signData;
								dic["signature"] = signature;
							}
							else
							{
								dic["json"] = (signData ?? "ValidateReceipt error");
								dic["signature"] = (signature ?? "ValidateReceipt error");
								dic["error"] = "ValidateReceipt";
							}
							string receipt2 = dic.toJson();
							<Purchase>c__AnonStorey.m_Unity.OnPurchaseSucceeded(product.storeSpecificId, receipt2, transactionId);
						});
					}
					else
					{
						string receipt = dic.toJson();
						this.m_Unity.OnPurchaseSucceeded(product.storeSpecificId, receipt, transactionId);
					}
				}
				else
				{
					PurchaseFailureReason reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown");
					string value = reason.ToString();
					Dictionary<string, object> dictionary = message.HashtableFromJson();
					string string2 = dictionary.GetString("isRepeat", "");
					bool flag;
					bool.TryParse(string2, out flag);
					if (flag)
					{
						if (Enum.IsDefined(typeof(PurchaseFailureReason), "DuplicateTransaction"))
						{
							reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "DuplicateTransaction");
						}
						value = "DuplicateTransaction";
					}
					Dictionary<string, object> dictionary2 = new Dictionary<string, object>();
					dictionary2["error"] = value;
					if (dictionary.ContainsKey("purchaseInfo"))
					{
						dictionary2["purchaseInfo"] = dictionary["purchaseInfo"];
					}
					string lastPurchaseError = dictionary2.toJson();
					this.m_LastPurchaseError = lastPurchaseError;
					PurchaseFailureDescription desc = new PurchaseFailureDescription(product.storeSpecificId, reason, message);
					this.m_Unity.OnPurchaseFailed(desc);
				}
			});
		}

		// Token: 0x06000101 RID: 257 RVA: 0x000067AC File Offset: 0x00004BAC
		public override void FinishTransaction(ProductDefinition product, string transactionId)
		{
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000102 RID: 258 RVA: 0x000067B0 File Offset: 0x00004BB0
		// (set) Token: 0x06000103 RID: 259 RVA: 0x000067CA File Offset: 0x00004BCA
		public bool fetchReceiptPayloadOnPurchase { get; set; }

		// Token: 0x06000104 RID: 260 RVA: 0x000067D4 File Offset: 0x00004BD4
		public void ConfirmPurchase(string transactionId, Action<bool, string, string> callback)
		{
			this.m_Bindings.ConfirmPurchase(transactionId, delegate(bool result, string resultingTransactionId, string message)
			{
				callback(result, resultingTransactionId, message);
			});
		}

		// Token: 0x06000105 RID: 261 RVA: 0x00006808 File Offset: 0x00004C08
		public void ValidateReceipt(string transactionIdentifier, Action<bool, string, string> callback)
		{
			this.m_Bindings.ValidateReceipt(transactionIdentifier, delegate(bool result, string signData, string signature)
			{
				callback(result, signData, signature);
			});
		}

		// Token: 0x06000106 RID: 262 RVA: 0x0000683C File Offset: 0x00004C3C
		public string GetLastPurchaseError()
		{
			return this.m_LastPurchaseError;
		}

		// Token: 0x04000086 RID: 134
		private const string k_DuplicateTransaction = "DuplicateTransaction";

		// Token: 0x04000087 RID: 135
		private const string k_Unknown = "Unknown";

		// Token: 0x04000088 RID: 136
		protected UnityChannelBindings m_Bindings;

		// Token: 0x04000089 RID: 137
		protected IStoreCallback m_Unity;

		// Token: 0x0400008A RID: 138
		protected string m_LastPurchaseError = "";

		// Token: 0x0400008B RID: 139
		public static int[] XiaomiPriceTierPrices = new int[]
		{
			0,
			1,
			3,
			6,
			8,
			12,
			18,
			25,
			28,
			30,
			40,
			45,
			50,
			60,
			68,
			73,
			78,
			88,
			93,
			98,
			108,
			113,
			118,
			123,
			128,
			138,
			148,
			153,
			158,
			163,
			168,
			178,
			188,
			193,
			198,
			208,
			218,
			223,
			228,
			233,
			238,
			243,
			248,
			253,
			258,
			263,
			268,
			273,
			278,
			283,
			288,
			298,
			308,
			318,
			328,
			348,
			388,
			418,
			448,
			488,
			518,
			548,
			588,
			618,
			648,
			698,
			798,
			818,
			848,
			898,
			998,
			1048,
			1098,
			1148,
			1198,
			1248,
			1298,
			1398,
			1448,
			1498,
			1598,
			1648,
			1998,
			2298,
			2598,
			2998,
			3298,
			3998,
			4498,
			4998,
			5898,
			6498
		};
	}
}
