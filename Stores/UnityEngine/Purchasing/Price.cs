﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004F RID: 79
	[Serializable]
	public class Price : ISerializationCallbackReceiver
	{
		// Token: 0x06000128 RID: 296 RVA: 0x00007028 File Offset: 0x00005428
		public void OnBeforeSerialize()
		{
			this.data = decimal.GetBits(this.value);
			this.num = decimal.ToDouble(this.value);
		}

		// Token: 0x06000129 RID: 297 RVA: 0x0000704D File Offset: 0x0000544D
		public void OnAfterDeserialize()
		{
			if (this.data != null && this.data.Length == 4)
			{
				this.value = new decimal(this.data);
			}
		}

		// Token: 0x040000A2 RID: 162
		public decimal value;

		// Token: 0x040000A3 RID: 163
		[SerializeField]
		private int[] data;

		// Token: 0x040000A4 RID: 164
		[SerializeField]
		private double num;
	}
}
