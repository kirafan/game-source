﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000014 RID: 20
	internal interface ISamsungAppsCallback
	{
		// Token: 0x0600003D RID: 61
		void OnTransactionsRestored(bool result);
	}
}
