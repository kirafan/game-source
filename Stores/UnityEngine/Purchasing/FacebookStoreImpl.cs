﻿using System;
using System.Runtime.CompilerServices;
using AOT;
using Uniject;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000031 RID: 49
	internal class FacebookStoreImpl : NativeJSONStore
	{
		// Token: 0x06000094 RID: 148 RVA: 0x0000374C File Offset: 0x00001B4C
		public FacebookStoreImpl(IUtil util)
		{
			FacebookStoreImpl.util = util;
			FacebookStoreImpl.instance = this;
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00003761 File Offset: 0x00001B61
		public void SetNativeStore(INativeFacebookStore facebook)
		{
			base.SetNativeStore(facebook);
			this.m_Native = facebook;
			facebook.Init();
			if (FacebookStoreImpl.<>f__mg$cache0 == null)
			{
				FacebookStoreImpl.<>f__mg$cache0 = new UnityPurchasingCallback(FacebookStoreImpl.MessageCallback);
			}
			facebook.SetUnityPurchasingCallback(FacebookStoreImpl.<>f__mg$cache0);
		}

		// Token: 0x06000096 RID: 150 RVA: 0x0000379C File Offset: 0x00001B9C
		public bool consumeItem(string item)
		{
			return this.m_Native.ConsumeItem(item);
		}

		// Token: 0x06000097 RID: 151 RVA: 0x000037C0 File Offset: 0x00001BC0
		[MonoPInvokeCallback(typeof(UnityPurchasingCallback))]
		private static void MessageCallback(string subject, string payload, string receipt, string transactionId)
		{
			FacebookStoreImpl.util.RunOnMainThread(delegate
			{
				FacebookStoreImpl.instance.ProcessMessage(subject, payload, receipt, transactionId);
			});
		}

		// Token: 0x06000098 RID: 152 RVA: 0x00003808 File Offset: 0x00001C08
		private void ProcessMessage(string subject, string payload, string receipt, string transactionId)
		{
			if (subject != null)
			{
				if (!(subject == "OnSetupFailed"))
				{
					if (!(subject == "OnProductsRetrieved"))
					{
						if (!(subject == "OnPurchaseSucceeded"))
						{
							if (!(subject == "OnPurchaseFailed"))
							{
								if (!(subject == "SendPurchasingEvent"))
								{
								}
							}
							else
							{
								base.OnPurchaseFailed(payload);
							}
						}
						else
						{
							base.OnPurchaseSucceeded(payload, receipt, transactionId);
						}
					}
					else
					{
						this.OnProductsRetrieved(payload);
					}
				}
				else
				{
					base.OnSetupFailed(payload);
				}
			}
		}

		// Token: 0x04000039 RID: 57
		private INativeFacebookStore m_Native;

		// Token: 0x0400003A RID: 58
		private static IUtil util;

		// Token: 0x0400003B RID: 59
		private static FacebookStoreImpl instance;

		// Token: 0x0400003C RID: 60
		[CompilerGenerated]
		private static UnityPurchasingCallback <>f__mg$cache0;
	}
}
