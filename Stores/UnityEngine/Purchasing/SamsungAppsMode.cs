﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000019 RID: 25
	public enum SamsungAppsMode
	{
		// Token: 0x0400000D RID: 13
		Production,
		// Token: 0x0400000E RID: 14
		AlwaysSucceed,
		// Token: 0x0400000F RID: 15
		AlwaysFail
	}
}
