﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200002F RID: 47
	public class WindowsPhone8
	{
		// Token: 0x04000037 RID: 55
		[Obsolete("Use WindowsStore.Name for Universal Windows Builds")]
		public const string Name = "WinRT";
	}
}
