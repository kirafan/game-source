﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000033 RID: 51
	internal class FakeMoolahConfiguration : IMoolahConfiguration, IStoreConfiguration
	{
		// Token: 0x1700000F RID: 15
		// (get) Token: 0x0600009E RID: 158 RVA: 0x00003980 File Offset: 0x00001D80
		// (set) Token: 0x0600009F RID: 159 RVA: 0x0000399B File Offset: 0x00001D9B
		public string appKey
		{
			get
			{
				return this.m_appKey;
			}
			set
			{
				this.m_appKey = value;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x000039A8 File Offset: 0x00001DA8
		// (set) Token: 0x060000A1 RID: 161 RVA: 0x000039C3 File Offset: 0x00001DC3
		public string hashKey
		{
			get
			{
				return this.m_hashKey;
			}
			set
			{
				this.m_hashKey = value;
			}
		}

		// Token: 0x060000A2 RID: 162 RVA: 0x000039CD File Offset: 0x00001DCD
		public void SetMode(CloudMoolahMode mode)
		{
		}

		// Token: 0x0400003D RID: 61
		private string m_appKey;

		// Token: 0x0400003E RID: 62
		private string m_hashKey;
	}
}
