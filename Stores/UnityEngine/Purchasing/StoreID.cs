﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000050 RID: 80
	[Serializable]
	public class StoreID
	{
		// Token: 0x0600012A RID: 298 RVA: 0x0000707C File Offset: 0x0000547C
		public StoreID(string store_, string id_)
		{
			this.store = store_;
			this.id = id_;
		}

		// Token: 0x040000A5 RID: 165
		public string store;

		// Token: 0x040000A6 RID: 166
		public string id;
	}
}
