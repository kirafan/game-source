﻿using System;
using System.Collections.Generic;
using Uniject;
using UnityEngine.Purchasing.Default;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000058 RID: 88
	public class StandardPurchasingModule : AbstractPurchasingModule, IAndroidStoreSelection, IStoreConfiguration
	{
		// Token: 0x06000153 RID: 339 RVA: 0x00007D72 File Offset: 0x00006172
		internal StandardPurchasingModule(IUtil util, ILogger logger, IRawStoreProvider platformProvider, RuntimePlatform platform, AndroidStore android)
		{
			this.m_Util = util;
			this.m_Logger = logger;
			this.m_PlatformProvider = platformProvider;
			this.m_RuntimePlatform = platform;
			this.useFakeStoreUIMode = FakeStoreUIMode.Default;
			this.useFakeStoreAlways = false;
			this.m_AndroidPlatform = android;
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000154 RID: 340 RVA: 0x00007DB0 File Offset: 0x000061B0
		public AndroidStore androidStore
		{
			get
			{
				return this.m_AndroidPlatform;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000155 RID: 341 RVA: 0x00007DCC File Offset: 0x000061CC
		// (set) Token: 0x06000156 RID: 342 RVA: 0x00007DE7 File Offset: 0x000061E7
		[Obsolete("Use IMicrosoftConfiguration to toggle use of the Microsoft IAP simulator.")]
		public bool useMockBillingSystem
		{
			get
			{
				return this.usingMockMicrosoft;
			}
			set
			{
				this.UseMockWindowsStore(value);
				this.usingMockMicrosoft = value;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x06000157 RID: 343 RVA: 0x00007DF8 File Offset: 0x000061F8
		// (set) Token: 0x06000158 RID: 344 RVA: 0x00007E12 File Offset: 0x00006212
		public FakeStoreUIMode useFakeStoreUIMode { get; set; }

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x06000159 RID: 345 RVA: 0x00007E1C File Offset: 0x0000621C
		// (set) Token: 0x0600015A RID: 346 RVA: 0x00007E36 File Offset: 0x00006236
		public bool useFakeStoreAlways { get; set; }

		// Token: 0x0600015B RID: 347 RVA: 0x00007E40 File Offset: 0x00006240
		public static StandardPurchasingModule Instance()
		{
			return StandardPurchasingModule.Instance(AndroidStore.NotSpecified);
		}

		// Token: 0x0600015C RID: 348 RVA: 0x00007E5C File Offset: 0x0000625C
		public static StandardPurchasingModule Instance(AndroidStore androidStore)
		{
			if (StandardPurchasingModule.ModuleInstance == null)
			{
				GameObject gameObject = new GameObject("IAPUtil");
				Object.DontDestroyOnLoad(gameObject);
				gameObject.hideFlags = (HideFlags.HideInHierarchy | HideFlags.HideInInspector);
				UnityUtil util = gameObject.AddComponent<UnityUtil>();
				TextAsset textAsset = Resources.Load("BillingMode") as TextAsset;
				StoreConfiguration storeConfiguration = null;
				if (null != textAsset)
				{
					storeConfiguration = StoreConfiguration.Deserialize(textAsset.text);
				}
				if (androidStore == AndroidStore.NotSpecified)
				{
					androidStore = AndroidStore.GooglePlay;
					if (storeConfiguration != null)
					{
						AndroidStore androidStore2 = storeConfiguration.androidStore;
						if (androidStore2 != AndroidStore.NotSpecified)
						{
							androidStore = androidStore2;
						}
					}
				}
				StandardPurchasingModule.ModuleInstance = new StandardPurchasingModule(util, Debug.logger, new RawStoreProvider(), Application.platform, androidStore);
			}
			return StandardPurchasingModule.ModuleInstance;
		}

		// Token: 0x0600015D RID: 349 RVA: 0x00007F14 File Offset: 0x00006314
		public override void Configure()
		{
			base.BindConfiguration<IGooglePlayConfiguration>(new FakeGooglePlayConfiguration());
			base.BindConfiguration<IAppleConfiguration>(new FakeAppleConfiguation());
			base.BindExtension<IAppleExtensions>(new FakeAppleExtensions());
			base.BindConfiguration<IAmazonConfiguration>(new FakeAmazonExtensions());
			base.BindExtension<IAmazonExtensions>(new FakeAmazonExtensions());
			base.BindConfiguration<ISamsungAppsConfiguration>(new FakeSamsungAppsExtensions());
			base.BindExtension<ISamsungAppsExtensions>(new FakeSamsungAppsExtensions());
			base.BindConfiguration<IMoolahConfiguration>(new FakeMoolahConfiguration());
			base.BindExtension<IMoolahExtension>(new FakeMoolahExtensions());
			base.BindConfiguration<IUnityChannelConfiguration>(new FakeUnityChannelConfiguration());
			base.BindExtension<IUnityChannelExtensions>(new FakeUnityChannelExtensions());
			base.BindConfiguration<IMicrosoftConfiguration>(new StandardPurchasingModule.MicrosoftConfiguration(this));
			base.BindExtension<IMicrosoftExtensions>(new FakeMicrosoftExtensions());
			base.BindConfiguration<ITizenStoreConfiguration>(new FakeTizenStoreConfiguration());
			base.BindConfiguration<IAndroidStoreSelection>(this);
			if (this.m_StoreInstance == null)
			{
				this.m_StoreInstance = this.InstantiateStore();
			}
			base.RegisterStore(this.m_StoreInstance.storeName, this.m_StoreInstance.instance);
		}

		// Token: 0x0600015E RID: 350 RVA: 0x00007FFC File Offset: 0x000063FC
		private StandardPurchasingModule.StoreInstance InstantiateStore()
		{
			StandardPurchasingModule.StoreInstance result;
			if (this.useFakeStoreAlways)
			{
				result = new StandardPurchasingModule.StoreInstance("fake", this.InstantiateFakeStore());
			}
			else
			{
				RuntimePlatform runtimePlatform = this.m_RuntimePlatform;
				switch (runtimePlatform)
				{
				case RuntimePlatform.WebGLPlayer:
					break;
				case RuntimePlatform.MetroPlayerX86:
				case RuntimePlatform.MetroPlayerX64:
				case RuntimePlatform.MetroPlayerARM:
					return new StandardPurchasingModule.StoreInstance("WinRT", this.instantiateWindowsStore());
				default:
					if (runtimePlatform == RuntimePlatform.OSXPlayer)
					{
						return new StandardPurchasingModule.StoreInstance("MacAppStore", this.InstantiateApple());
					}
					if (runtimePlatform != RuntimePlatform.WindowsPlayer)
					{
						switch (runtimePlatform)
						{
						case RuntimePlatform.IPhonePlayer:
							break;
						default:
							if (runtimePlatform != RuntimePlatform.tvOS)
							{
								return new StandardPurchasingModule.StoreInstance("fake", this.InstantiateFakeStore());
							}
							break;
						case RuntimePlatform.Android:
						{
							AndroidStore androidPlatform = this.m_AndroidPlatform;
							if (androidPlatform == AndroidStore.CloudMoolah)
							{
								return new StandardPurchasingModule.StoreInstance("MoolahAppStore", this.InstantiateCloudMoolah());
							}
							if (androidPlatform != AndroidStore.XiaomiMiPay)
							{
								return new StandardPurchasingModule.StoreInstance(StandardPurchasingModule.AndroidStoreNameMap[this.m_AndroidPlatform], this.InstantiateAndroid());
							}
							return new StandardPurchasingModule.StoreInstance("XiaomiMiPay", this.InstantiateXiaomiMiPay());
						}
						}
						return new StandardPurchasingModule.StoreInstance("AppleAppStore", this.InstantiateApple());
					}
					break;
				case RuntimePlatform.TizenPlayer:
					return new StandardPurchasingModule.StoreInstance("TizenStore", this.InstantiateTizen());
				}
				result = new StandardPurchasingModule.StoreInstance("FacebookStore", this.InstantiateFacebook());
			}
			return result;
		}

		// Token: 0x0600015F RID: 351 RVA: 0x00008178 File Offset: 0x00006578
		private IStore InstantiateAndroid()
		{
			NativeJSONStore nativeJSONStore = new NativeJSONStore();
			nativeJSONStore.SetNativeStore(this.m_PlatformProvider.GetAndroidStore(nativeJSONStore, this.m_AndroidPlatform, this.m_Binder, this.m_Util));
			return nativeJSONStore;
		}

		// Token: 0x06000160 RID: 352 RVA: 0x000081B8 File Offset: 0x000065B8
		private IStore InstantiateCloudMoolah()
		{
			GameObject gameObject = GameObject.Find("IAPUtil");
			MoolahStoreImpl moolahStoreImpl = gameObject.AddComponent<MoolahStoreImpl>();
			base.BindConfiguration<IMoolahConfiguration>(moolahStoreImpl);
			base.BindExtension<IMoolahExtension>(moolahStoreImpl);
			return moolahStoreImpl;
		}

		// Token: 0x06000161 RID: 353 RVA: 0x000081F0 File Offset: 0x000065F0
		private IStore InstantiateXiaomiMiPay()
		{
			UnityChannelImpl unityChannelImpl = new UnityChannelImpl();
			base.BindConfiguration<IUnityChannelConfiguration>(unityChannelImpl);
			base.BindExtension<IUnityChannelExtensions>(unityChannelImpl);
			Debug.Log("XiaomiMiPayImpl store " + unityChannelImpl);
			return unityChannelImpl;
		}

		// Token: 0x06000162 RID: 354 RVA: 0x0000822C File Offset: 0x0000662C
		private IStore InstantiateApple()
		{
			AppleStoreImpl appleStoreImpl = new AppleStoreImpl(this.m_Util);
			INativeAppleStore storekit = this.m_PlatformProvider.GetStorekit(appleStoreImpl);
			appleStoreImpl.SetNativeStore(storekit);
			base.BindExtension<IAppleExtensions>(appleStoreImpl);
			return appleStoreImpl;
		}

		// Token: 0x06000163 RID: 355 RVA: 0x0000826C File Offset: 0x0000666C
		private void UseMockWindowsStore(bool value)
		{
			if (this.windowsStore != null)
			{
				IWindowsIAP windowsIAP = Factory.Create(value);
				this.windowsStore.SetWindowsIAP(windowsIAP);
			}
		}

		// Token: 0x06000164 RID: 356 RVA: 0x0000829C File Offset: 0x0000669C
		private IStore instantiateWindowsStore()
		{
			IWindowsIAP win = Factory.Create(false);
			this.windowsStore = new WinRTStore(win, this.m_Util, this.m_Logger);
			this.m_Util.AddPauseListener(new Action<bool>(this.windowsStore.restoreTransactions));
			return this.windowsStore;
		}

		// Token: 0x06000165 RID: 357 RVA: 0x000082F4 File Offset: 0x000066F4
		private IStore InstantiateTizen()
		{
			TizenStoreImpl tizenStoreImpl = new TizenStoreImpl(this.m_Util);
			tizenStoreImpl.SetNativeStore(this.m_PlatformProvider.GetTizenStore(tizenStoreImpl, this.m_Binder));
			base.BindConfiguration<ITizenStoreConfiguration>(tizenStoreImpl);
			return tizenStoreImpl;
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00008338 File Offset: 0x00006738
		private IStore InstantiateFacebook()
		{
			INativeFacebookStore facebookStore = this.m_PlatformProvider.GetFacebookStore();
			IStore result;
			if (facebookStore.Check())
			{
				FacebookStoreImpl facebookStoreImpl = new FacebookStoreImpl(this.m_Util);
				facebookStoreImpl.SetNativeStore(facebookStore);
				result = facebookStoreImpl;
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000167 RID: 359 RVA: 0x00008384 File Offset: 0x00006784
		private IStore InstantiateFakeStore()
		{
			FakeStore fakeStore = null;
			if (this.useFakeStoreUIMode != FakeStoreUIMode.Default)
			{
				fakeStore = new UIFakeStore();
				fakeStore.UIMode = this.useFakeStoreUIMode;
			}
			if (fakeStore == null)
			{
				fakeStore = new FakeStore();
			}
			return fakeStore;
		}

		// Token: 0x040000D4 RID: 212
		private AndroidStore m_AndroidPlatform;

		// Token: 0x040000D5 RID: 213
		private IRawStoreProvider m_PlatformProvider;

		// Token: 0x040000D6 RID: 214
		private RuntimePlatform m_RuntimePlatform;

		// Token: 0x040000D7 RID: 215
		private IUtil m_Util;

		// Token: 0x040000D8 RID: 216
		private ILogger m_Logger;

		// Token: 0x040000D9 RID: 217
		private static StandardPurchasingModule ModuleInstance;

		// Token: 0x040000DA RID: 218
		private StandardPurchasingModule.StoreInstance m_StoreInstance;

		// Token: 0x040000DB RID: 219
		private static Dictionary<AndroidStore, string> AndroidStoreNameMap = new Dictionary<AndroidStore, string>
		{
			{
				AndroidStore.AmazonAppStore,
				"AmazonApps"
			},
			{
				AndroidStore.GooglePlay,
				"GooglePlay"
			},
			{
				AndroidStore.SamsungApps,
				"SamsungApps"
			},
			{
				AndroidStore.CloudMoolah,
				"MoolahAppStore"
			},
			{
				AndroidStore.XiaomiMiPay,
				"XiaomiMiPay"
			},
			{
				AndroidStore.NotSpecified,
				"GooglePlay"
			}
		};

		// Token: 0x040000DC RID: 220
		private bool usingMockMicrosoft;

		// Token: 0x040000DF RID: 223
		private WinRTStore windowsStore;

		// Token: 0x02000059 RID: 89
		internal class StoreInstance
		{
			// Token: 0x06000169 RID: 361 RVA: 0x0000842D File Offset: 0x0000682D
			internal StoreInstance(string name, IStore instance)
			{
				this.storeName = name;
				this.instance = instance;
			}

			// Token: 0x1700002D RID: 45
			// (get) Token: 0x0600016A RID: 362 RVA: 0x00008444 File Offset: 0x00006844
			internal string storeName { get; }

			// Token: 0x1700002E RID: 46
			// (get) Token: 0x0600016B RID: 363 RVA: 0x00008460 File Offset: 0x00006860
			internal IStore instance { get; }
		}

		// Token: 0x0200005A RID: 90
		private class MicrosoftConfiguration : IMicrosoftConfiguration, IStoreConfiguration
		{
			// Token: 0x0600016C RID: 364 RVA: 0x0000847A File Offset: 0x0000687A
			public MicrosoftConfiguration(StandardPurchasingModule module)
			{
				this.module = module;
			}

			// Token: 0x1700002F RID: 47
			// (get) Token: 0x0600016D RID: 365 RVA: 0x0000848C File Offset: 0x0000688C
			// (set) Token: 0x0600016E RID: 366 RVA: 0x000084A7 File Offset: 0x000068A7
			public bool useMockBillingSystem
			{
				get
				{
					return this.useMock;
				}
				set
				{
					this.module.UseMockWindowsStore(value);
					this.useMock = value;
				}
			}

			// Token: 0x040000E2 RID: 226
			private bool useMock;

			// Token: 0x040000E3 RID: 227
			private StandardPurchasingModule module;
		}
	}
}
