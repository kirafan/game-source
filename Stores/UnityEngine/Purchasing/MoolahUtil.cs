﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000040 RID: 64
	internal class MoolahUtil
	{
		// Token: 0x060000E1 RID: 225 RVA: 0x00006190 File Offset: 0x00004590
		public static string GetResponseString(HttpWebResponse webresponse)
		{
			string result;
			if (webresponse == null)
			{
				result = null;
			}
			else
			{
				using (Stream responseStream = webresponse.GetResponseStream())
				{
					StreamReader streamReader = new StreamReader(responseStream, Encoding.UTF8);
					result = streamReader.ReadToEnd();
				}
			}
			return result;
		}
	}
}
