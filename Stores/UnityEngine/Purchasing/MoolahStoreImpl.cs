﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003F RID: 63
	[AddComponentMenu("")]
	internal class MoolahStoreImpl : MonoBehaviour, IStore, IMoolahExtension, IMoolahConfiguration, IStoreExtension, IStoreConfiguration
	{
		// Token: 0x060000B5 RID: 181 RVA: 0x00003AC0 File Offset: 0x00001EC0
		public void Initialize(IStoreCallback m_callback)
		{
			Debug.Log("CloudMoolah Initialize...");
			this.m_callback = m_callback;
			this.authCodeDic = new Dictionary<string, string>();
			if (string.IsNullOrEmpty(this.m_appKey))
			{
				throw new Exception("IMoolahConfiguration.appkey is null!");
			}
			if (string.IsNullOrEmpty(this.m_hashKey))
			{
				throw new Exception("IMoolahConfiguration.hashKey is null!");
			}
		}

		// Token: 0x060000B6 RID: 182 RVA: 0x00003B24 File Offset: 0x00001F24
		public void RetrieveProducts(ReadOnlyCollection<ProductDefinition> productDefinitions)
		{
			this.m_productDefinitions = productDefinitions;
			if (this.IsTestMode())
			{
				List<ProductDescription> list = new List<ProductDescription>();
				foreach (ProductDefinition productDefinition in productDefinitions)
				{
					ProductMetadata metadata = new ProductMetadata("$0.01", "CloudMoolah title for " + productDefinition.storeSpecificId, "CloudMoolah description", "USD", 0.01m);
					list.Add(new ProductDescription(productDefinition.storeSpecificId, metadata));
				}
				this.RetrieveProductsSucceeded(list);
			}
			else
			{
				List<object> list2 = new List<object>();
				foreach (ProductDefinition productDefinition2 in productDefinitions)
				{
					list2.Add(new Dictionary<string, object>
					{
						{
							"pid",
							this.GetStringMD5(productDefinition2.storeSpecificId)
						},
						{
							"productType",
							this.getProductTypeIndex(productDefinition2.type)
						}
					});
				}
				string productInfo = MiniJson.JsonEncode(list2);
				base.StartCoroutine(this.VaildateProduct(this.m_appKey, productInfo, delegate(bool state, string result)
				{
					Debug.Log("CloudMoolah VaildateProduct state : " + state.ToString());
					this.VaildateProductProcess(state, result);
				}));
			}
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x00003C98 File Offset: 0x00002098
		private int getProductTypeIndex(ProductType type)
		{
			int result;
			if (type != ProductType.Consumable)
			{
				if (type != ProductType.NonConsumable)
				{
					if (type != ProductType.Subscription)
					{
						result = 1;
					}
					else
					{
						result = 3;
					}
				}
				else
				{
					result = 2;
				}
			}
			else
			{
				result = 1;
			}
			return result;
		}

		// Token: 0x060000B8 RID: 184 RVA: 0x00003CDC File Offset: 0x000020DC
		private void VaildateProductProcess(bool state, string result)
		{
			if (state)
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(result) as Dictionary<string, object>;
				string a = dictionary["code"].ToString();
				if (a == "1" || a == "0")
				{
					List<object> list = dictionary["values"] as List<object>;
					List<ProductDescription> list2 = new List<ProductDescription>();
					foreach (object obj in list)
					{
						Dictionary<string, object> dictionary2 = obj as Dictionary<string, object>;
						string currentString = this.GetCurrentString(dictionary2["pid"]);
						string storeSpecificId = this.GetStoreSpecificId(currentString);
						if (!string.IsNullOrEmpty(storeSpecificId))
						{
							string currentString2 = this.GetCurrentString(dictionary2["priceString"]);
							if (!string.IsNullOrEmpty(currentString2))
							{
								string text = Convert.ToDouble(currentString2).ToString("f2");
								string currentString3 = this.GetCurrentString(dictionary2["title"]);
								string currentString4 = this.GetCurrentString(dictionary2["description"]);
								string currentString5 = this.GetCurrentString(dictionary2["currencyCode"]);
								string value = "0.00";
								if (dictionary2["localizedPrice"] == null)
								{
									value = text;
								}
								decimal localizedPrice = Convert.ToDecimal(value);
								ProductMetadata metadata = new ProductMetadata(text, currentString3, currentString4, currentString5, localizedPrice);
								list2.Add(new ProductDescription(storeSpecificId, metadata));
							}
						}
					}
					this.RetrieveProductsSucceeded(list2);
					Debug.Log("CloudMoolah ProductList.length is " + list2.Count.ToString());
				}
				else
				{
					this.RetrieveProductsFailed(InitializationFailureReason.NoProductsAvailable);
				}
			}
			else
			{
				this.RetrieveProductsFailed(InitializationFailureReason.PurchasingUnavailable);
			}
		}

		// Token: 0x060000B9 RID: 185 RVA: 0x00003EDC File Offset: 0x000022DC
		private string GetCurrentString(object obj)
		{
			string result;
			if (obj == null)
			{
				result = null;
			}
			else
			{
				result = obj.ToString();
			}
			return result;
		}

		// Token: 0x060000BA RID: 186 RVA: 0x00003F08 File Offset: 0x00002308
		private IEnumerator VaildateProduct(string appkey, string productInfo, Action<bool, string> result)
		{
			string sign = this.GetStringMD5(appkey + this.m_hashKey);
			WWWForm wf = new WWWForm();
			wf.AddField("appId", appkey);
			wf.AddField("productInfo", productInfo);
			wf.AddField("sign", sign);
			WWW w = new WWW(MoolahStoreImpl.requestProductValidateUrl, wf);
			yield return w;
			if (!string.IsNullOrEmpty(w.error))
			{
				result(false, w.error);
			}
			else
			{
				result(true, w.text);
			}
			yield break;
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00003F3F File Offset: 0x0000233F
		private void RetrieveProductsSucceeded(List<ProductDescription> products)
		{
			this.m_callback.OnProductsRetrieved(products);
		}

		// Token: 0x060000BC RID: 188 RVA: 0x00003F4E File Offset: 0x0000234E
		private void RetrieveProductsFailed(InitializationFailureReason reason)
		{
			this.m_callback.OnSetupFailed(reason);
		}

		// Token: 0x060000BD RID: 189 RVA: 0x00003F5D File Offset: 0x0000235D
		public void ClosePayWebView(string result)
		{
			Debug.Log("CloudMoolah ClosePayWebView...");
			if (this.isNeedPolling)
			{
				this.isNeedPolling = false;
				this.PurchaseFailed(this.GetStoreSpecificId(this.m_md5StoreProductID), PurchaseFailureReason.UserCancelled, "UserCancelled");
			}
		}

		// Token: 0x060000BE RID: 190 RVA: 0x00003F98 File Offset: 0x00002398
		private void PurchaseRusult(string resultJson)
		{
			this.isNeedPolling = false;
			Dictionary<string, object> dictionary = MiniJson.JsonDecode(resultJson) as Dictionary<string, object>;
			string a = dictionary["code"].ToString();
			if (a == "1")
			{
				Dictionary<string, object> dictionary2 = dictionary["values"] as Dictionary<string, object>;
				string a2 = dictionary2["state"].ToString();
				string transactionId = dictionary2["tradeSeq"].ToString();
				string md5productID = dictionary2["productId"].ToString();
				string msg = dictionary["msg"].ToString();
				if (a2 == TradeSeqState.PAY_CONFIRM.ToString())
				{
					string receipt = dictionary2["receipt"].ToString();
					this.PurchaseSucceed(this.GetStoreSpecificId(md5productID), receipt, transactionId);
				}
				else if (a2 == TradeSeqState.PAY_FAILED.ToString())
				{
					this.PurchaseFailed(this.GetStoreSpecificId(md5productID), (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"), msg);
				}
			}
		}

		// Token: 0x060000BF RID: 191 RVA: 0x000040C0 File Offset: 0x000024C0
		public void Purchase(ProductDefinition product, string developerPayload)
		{
			Debug.Log("CloudMoolah Purchase:" + product.storeSpecificId);
			if (this.IsTestMode())
			{
				if (this.GetMode() == CloudMoolahMode.AlwaysSucceed)
				{
					this.PurchaseSucceed(product.storeSpecificId, "CloudMoolah TestMode receipt", Guid.NewGuid().ToString());
				}
				else if (this.GetMode() == CloudMoolahMode.AlwaysFailed)
				{
					this.PurchaseFailed(product.storeSpecificId, PurchaseFailureReason.UserCancelled, "TestMode UserCancelled");
				}
			}
			else
			{
				if (this.isNeedPolling)
				{
					throw new Exception("Purchase can be only one !");
				}
				bool flag = false;
				if (string.IsNullOrEmpty(this.m_CustomerID))
				{
					flag = true;
				}
				Action<string, string, string> purchaseSucceed = delegate(string productid, string receipt, string transactionId)
				{
					this.PurchaseSucceed(this.GetStoreSpecificId(productid), receipt, transactionId);
				};
				Action<string, PurchaseFailureReason, string> purchaseFailed = delegate(string storeSpecificId, PurchaseFailureReason failureReason, string msg)
				{
					if (failureReason == (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"))
					{
						failureReason = PurchaseFailureReason.UserCancelled;
					}
					this.PurchaseFailed(storeSpecificId, failureReason, msg);
				};
				Action<string, string, string> succeed = delegate(string transactionId, string authGlobal, string paymentUrl)
				{
					if (string.IsNullOrEmpty(paymentUrl))
					{
						throw new Exception("authGlobal is null!");
					}
					if (string.IsNullOrEmpty(authGlobal))
					{
						throw new Exception("authGlobal is null! ");
					}
					if (string.IsNullOrEmpty(transactionId))
					{
						throw new Exception("transactionId is null! ");
					}
					this.isNeedPolling = true;
					this.AddAuthCode(transactionId, authGlobal);
					string customerID = this.m_CustomerID;
					if (Application.platform == RuntimePlatform.Android)
					{
						PayMethod.showPayWebView(paymentUrl, authGlobal, transactionId, this.m_hashKey, customerID);
					}
					else
					{
						Application.OpenURL(paymentUrl);
						this.StartCoroutine(this.StartPurchasePolling(authGlobal, transactionId, purchaseSucceed, purchaseFailed));
					}
				};
				Action<string, string> action = delegate(string productID, string msg)
				{
					this.PurchaseFailed(this.GetStoreSpecificId(productID), PurchaseFailureReason.PaymentDeclined, "request MoolahStoreAuthCode failed !");
				};
				this.m_md5StoreProductID = this.GetStringMD5(product.storeSpecificId);
				if (!flag)
				{
					this.RequestAuthCode(this.m_md5StoreProductID, developerPayload, succeed, action);
				}
				else
				{
					action(this.m_md5StoreProductID, "customerId is null, use SystemInfo.deviceUniqueIdentifier or other user identification");
				}
			}
		}

		// Token: 0x060000C0 RID: 192 RVA: 0x00004208 File Offset: 0x00002608
		private void RequestAuthCode(string productID, string payload, Action<string, string, string> succeed, Action<string, string> failed)
		{
			if (this.isRequestAuthCodeing)
			{
				failed(productID, "RequestAuthCode repeat");
				throw new Exception("RequestAuthCode repeat");
			}
			string customerID = this.m_CustomerID;
			string text = this.m_LoginToken;
			if (string.IsNullOrEmpty(customerID))
			{
				failed(productID, "customerId is null");
				throw new Exception("customerId or m_UniqueID is null!");
			}
			if (text == null)
			{
				text = Guid.NewGuid().ToString();
			}
			string text2 = "1";
			string text3 = Guid.NewGuid().ToString();
			string md5String = string.Concat(new string[]
			{
				this.m_appKey,
				customerID,
				productID,
				text3,
				text2,
				this.m_hashKey
			});
			string stringMD = this.GetStringMD5(md5String);
			WWWForm wwwform = new WWWForm();
			wwwform.AddField("APPId", this.m_appKey);
			wwwform.AddField("customerId", customerID);
			wwwform.AddField("productId", productID);
			wwwform.AddField("token", text);
			wwwform.AddField("tradeSeq", text3);
			wwwform.AddField("tradeType", text2);
			wwwform.AddField("payload", payload);
			wwwform.AddField("sign", stringMD);
			this.isRequestAuthCodeing = true;
			base.StartCoroutine(this.RequestAuthCode(wwwform, productID, text3, succeed, failed));
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00004370 File Offset: 0x00002770
		private IEnumerator RequestAuthCode(WWWForm wf, string productID, string transactionId, Action<string, string, string> succeed, Action<string, string> failed)
		{
			WWW w = new WWW(MoolahStoreImpl.requestAuthCodePath, wf);
			yield return w;
			this.isRequestAuthCodeing = false;
			if (!string.IsNullOrEmpty(w.error))
			{
				Debug.Log("RequestAuthCode error :" + w.error);
				failed(productID, w.error);
			}
			else
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(w.text) as Dictionary<string, object>;
				if (dictionary["code"].ToString() != "1")
				{
					failed(productID, this.GetCurrentString(dictionary["msg"]));
				}
				else
				{
					Dictionary<string, object> dictionary2 = dictionary["values"] as Dictionary<string, object>;
					string arg = dictionary2["authCode"].ToString();
					string arg2 = dictionary2["requestUrl"].ToString();
					succeed(transactionId, arg, arg2);
				}
			}
			yield break;
		}

		// Token: 0x060000C2 RID: 194 RVA: 0x000043B8 File Offset: 0x000027B8
		private IEnumerator StartPurchasePolling(string authGlobal, string transactionId, Action<string, string, string> purchaseSucceed, Action<string, PurchaseFailureReason, string> purchaseFailed)
		{
			yield return new WaitForSeconds(6f);
			if (!this.isNeedPolling)
			{
				yield break;
			}
			string orderSuccess = "0";
			string signstr = authGlobal + orderSuccess + transactionId + this.m_hashKey;
			string sign = this.GetStringMD5(signstr);
			string param = string.Concat(new string[]
			{
				"?authCode=",
				authGlobal,
				"&orderSuccess=",
				orderSuccess,
				"&tradeSeq=",
				transactionId,
				"&sign=",
				sign
			});
			string url = MoolahStoreImpl.pollingPath + ((param != null) ? param : "");
			WWW pollingstr = new WWW(url);
			yield return pollingstr;
			if (string.IsNullOrEmpty(pollingstr.error))
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(pollingstr.text) as Dictionary<string, object>;
				string a = dictionary["code"].ToString();
				if (a == "1")
				{
					Dictionary<string, object> dictionary2 = dictionary["values"] as Dictionary<string, object>;
					string arg = dictionary2["tradeSeq"].ToString();
					string a2 = dictionary2["state"].ToString();
					string text = dictionary2["productId"].ToString();
					string arg2 = dictionary["msg"].ToString();
					if (a2 == TradeSeqState.PAY_CONFIRM.ToString())
					{
						this.isNeedPolling = false;
						string arg3 = dictionary2["receipt"].ToString();
						purchaseSucceed(text, arg3, arg);
						yield break;
					}
					if (a2 == TradeSeqState.PAY_FAILED.ToString())
					{
						this.isNeedPolling = false;
						purchaseFailed(this.GetStoreSpecificId(text), PurchaseFailureReason.UserCancelled, arg2);
						yield break;
					}
				}
			}
			base.StartCoroutine(this.StartPurchasePolling(authGlobal, transactionId, purchaseSucceed, purchaseFailed));
			yield break;
		}

		// Token: 0x060000C3 RID: 195 RVA: 0x000043F7 File Offset: 0x000027F7
		public void PurchaseSucceed(string storeSpecificId, string receipt, string transactionId)
		{
			Debug.Log("--- CloudMoolah PurchaseSucceed ---");
			this.m_callback.OnPurchaseSucceeded(storeSpecificId, receipt, transactionId);
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00004414 File Offset: 0x00002814
		public void PurchaseFailed(string storeSpecificId, PurchaseFailureReason reason, string msg)
		{
			PurchaseFailureDescription desc = new PurchaseFailureDescription(storeSpecificId, reason, msg);
			this.m_callback.OnPurchaseFailed(desc);
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x00004437 File Offset: 0x00002837
		public void FinishTransaction(ProductDefinition product, string transactionId)
		{
			Debug.Log("--- CloudMoolah FinishTransaction ---");
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x00004444 File Offset: 0x00002844
		private string GetStringMD5(string md5String)
		{
			byte[] bytes = Encoding.UTF8.GetBytes(md5String);
			MD5CryptoServiceProvider md5CryptoServiceProvider = new MD5CryptoServiceProvider();
			byte[] array = md5CryptoServiceProvider.ComputeHash(bytes);
			string text = "";
			for (int i = 0; i < array.Length; i++)
			{
				text += Convert.ToString(array[i], 16).PadLeft(2, '0');
			}
			return text.PadLeft(32, '0');
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x000044BC File Offset: 0x000028BC
		private string GetStoreSpecificId(string MD5productID)
		{
			foreach (ProductDefinition productDefinition in this.m_productDefinitions)
			{
				if (this.GetStringMD5(productDefinition.storeSpecificId) == MD5productID)
				{
					return productDefinition.storeSpecificId;
				}
			}
			return null;
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x060000C8 RID: 200 RVA: 0x00004540 File Offset: 0x00002940
		// (set) Token: 0x060000C9 RID: 201 RVA: 0x0000455B File Offset: 0x0000295B
		public string appKey
		{
			get
			{
				return this.m_appKey;
			}
			set
			{
				this.m_appKey = value;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x060000CA RID: 202 RVA: 0x00004568 File Offset: 0x00002968
		// (set) Token: 0x060000CB RID: 203 RVA: 0x00004583 File Offset: 0x00002983
		public string hashKey
		{
			get
			{
				return this.m_hashKey;
			}
			set
			{
				this.m_hashKey = value;
			}
		}

		// Token: 0x060000CC RID: 204 RVA: 0x0000458D File Offset: 0x0000298D
		public void SetMode(CloudMoolahMode mode)
		{
			this.m_IsTestMode = (mode != CloudMoolahMode.Production);
			this.m_mode = mode;
		}

		// Token: 0x060000CD RID: 205 RVA: 0x000045A4 File Offset: 0x000029A4
		private CloudMoolahMode GetMode()
		{
			return this.m_mode;
		}

		// Token: 0x060000CE RID: 206 RVA: 0x000045C0 File Offset: 0x000029C0
		private bool IsTestMode()
		{
			return this.m_IsTestMode;
		}

		// Token: 0x060000CF RID: 207 RVA: 0x000045DC File Offset: 0x000029DC
		public void Login(string CMUserName, string CMPassword, Action<LoginResultState, string> LoginResult)
		{
			if (!this.isLogining)
			{
				this.m_LoginToken = null;
				this.m_CustomerID = null;
				this.isLogining = true;
				if (LoginResult == null)
				{
					this.isLogining = false;
					LoginResult(LoginResultState.LoginCallBackIsNull, "LoginSucceed or LoginFailed CallBack is null!");
				}
				else
				{
					if (this.httpManager == null)
					{
						this.httpManager = new HttpManager();
					}
					if (!string.IsNullOrEmpty(CMUserName) && !string.IsNullOrEmpty(CMPassword))
					{
						if (this.IsTestMode())
						{
							this.isLogining = false;
							if (this.GetMode() == CloudMoolahMode.AlwaysSucceed)
							{
								LoginResult(LoginResultState.LoginSucceed, "TestMode LoginSucceed.");
							}
							else if (this.GetMode() == CloudMoolahMode.AlwaysFailed)
							{
								LoginResult(LoginResultState.NotKnown, "TestMode NotKnown error!");
							}
						}
						else
						{
							Action<IAsyncResult> asyncCompleate = delegate(IAsyncResult asyncResult)
							{
								if (asyncResult == null)
								{
									this.isLogining = false;
									LoginResult(LoginResultState.NotKnown, "NotKnown error!");
								}
								else
								{
									HttpWebRequest httpWebRequest = asyncResult.AsyncState as HttpWebRequest;
									HttpWebResponse httpWebResponse = null;
									try
									{
										httpWebResponse = (httpWebRequest.EndGetResponse(asyncResult) as HttpWebResponse);
									}
									catch (WebException ex)
									{
										this.isLogining = false;
										LoginResult(LoginResultState.NetworkError, "Network error!");
										throw new Exception("login Exception msg is " + ex.Message);
									}
									if (httpWebResponse.StatusCode != HttpStatusCode.OK)
									{
										this.isLogining = false;
										LoginResult(LoginResultState.NetworkError, "Network error!");
										throw new Exception(httpWebResponse.StatusCode + ":login HttpStatus异常:" + httpWebResponse.StatusDescription);
									}
									string responseString = MoolahUtil.GetResponseString(httpWebResponse);
									this.isLogining = false;
									this.LoginProcess(responseString, LoginResult);
								}
							};
							Action<object, bool> asyncTimeout = delegate(object state, bool timeOut)
							{
								this.isLogining = false;
							};
							string param = string.Concat(new string[]
							{
								"?name=",
								CMUserName,
								"&password=",
								CMPassword,
								"&belongsToGame=",
								this.m_appKey
							});
							this.httpManager.GetAsyncHttpWebData(MoolahStoreImpl.CM_PATH + MoolahStoreImpl.LOGIN_PATH, asyncCompleate, asyncTimeout, param, "POST", 10000);
						}
					}
					else
					{
						LoginResult(LoginResultState.UserOrPasswordEmpty, "Username or Password is empty !");
						this.isLogining = false;
					}
				}
			}
		}

		// Token: 0x060000D0 RID: 208 RVA: 0x0000476C File Offset: 0x00002B6C
		private void LoginProcess(string result, Action<LoginResultState, string> LoginResult)
		{
			if (result == null)
			{
				LoginResult(LoginResultState.NotKnown, "NotKnown error!");
			}
			else
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(result) as Dictionary<string, object>;
				string a = dictionary["code"].ToString();
				if (a == "200")
				{
					Dictionary<string, object> dictionary2 = dictionary["values"] as Dictionary<string, object>;
					string loginToken = dictionary2["token"].ToString();
					Dictionary<string, object> dictionary3 = dictionary2["account"] as Dictionary<string, object>;
					string customerID = dictionary3["uniqueSequence"].ToString();
					this.m_LoginToken = loginToken;
					this.m_CustomerID = customerID;
					LoginResult(LoginResultState.LoginSucceed, dictionary["message"].ToString());
				}
				else if (a == "2001")
				{
					LoginResult(LoginResultState.UserNotExists, dictionary["message"].ToString());
				}
				else if (a == "2002")
				{
					LoginResult(LoginResultState.PasswordError, dictionary["message"].ToString());
				}
				else
				{
					LoginResult(LoginResultState.NotKnown, dictionary["message"].ToString());
				}
			}
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x000048A8 File Offset: 0x00002CA8
		public void FastRegister(string CMpassword, Action<string> RegisterSucceed, Action<FastRegisterError, string> RegisterFailed)
		{
			if (!this.isRegister)
			{
				this.isRegister = true;
				if (RegisterSucceed == null || RegisterFailed == null)
				{
					this.isRegister = false;
					RegisterFailed(FastRegisterError.FastRegisterCallBackIsNull, "FastRegister Callback is null!");
				}
				else
				{
					if (this.httpManager == null)
					{
						this.httpManager = new HttpManager();
					}
					if (!string.IsNullOrEmpty(CMpassword))
					{
						if (this.IsTestMode())
						{
							this.isRegister = false;
							if (this.GetMode() == CloudMoolahMode.AlwaysSucceed)
							{
								RegisterSucceed("TestMode cmUserName");
							}
							else if (this.GetMode() == CloudMoolahMode.AlwaysFailed)
							{
								RegisterFailed(FastRegisterError.NotKnown, "TestMode NotKnown error!");
							}
						}
						else
						{
							Action<IAsyncResult> asyncCompleate = delegate(IAsyncResult asyncResult)
							{
								if (asyncResult == null)
								{
									this.isRegister = false;
									RegisterFailed(FastRegisterError.NetworkError, "Network error!");
								}
								else
								{
									HttpWebRequest httpWebRequest = asyncResult.AsyncState as HttpWebRequest;
									HttpWebResponse httpWebResponse = null;
									try
									{
										httpWebResponse = (httpWebRequest.EndGetResponse(asyncResult) as HttpWebResponse);
									}
									catch (WebException ex)
									{
										this.isRegister = false;
										RegisterFailed(FastRegisterError.NetworkError, "Network error!");
										throw new Exception("Register Exception msg is " + ex.Message);
									}
									if (httpWebResponse.StatusCode != HttpStatusCode.OK)
									{
										this.isRegister = false;
										RegisterFailed(FastRegisterError.NetworkError, "Network error!");
										throw new Exception(httpWebResponse.StatusCode + ":Register HttpStatus异常:" + httpWebResponse.StatusDescription);
									}
									string responseString = MoolahUtil.GetResponseString(httpWebResponse);
									this.isRegister = false;
									this.RegisterProcess(responseString, RegisterSucceed, RegisterFailed);
								}
							};
							Action<object, bool> asyncTimeout = delegate(object state, bool timeOut)
							{
							};
							string param = string.Concat(new string[]
							{
								"?password=",
								CMpassword,
								"&belongsToGame=",
								this.m_appKey,
								"&imei=",
								CMpassword
							});
							this.httpManager.GetAsyncHttpWebData(MoolahStoreImpl.CM_PATH + MoolahStoreImpl.REGISTER_OR_BINDING_PATH, asyncCompleate, asyncTimeout, param, "POST", 10000);
						}
					}
					else
					{
						this.isRegister = false;
						RegisterFailed(FastRegisterError.PasswordEmpty, "Password is empty !");
					}
				}
			}
		}

		// Token: 0x060000D2 RID: 210 RVA: 0x00004A40 File Offset: 0x00002E40
		private void RegisterProcess(string result, Action<string> RegisterSucceed, Action<FastRegisterError, string> RegisterFailed)
		{
			if (result == null)
			{
				RegisterFailed(FastRegisterError.NetworkError, "Network error!");
			}
			else
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(result) as Dictionary<string, object>;
				string a = dictionary["code"].ToString();
				if (a == "1")
				{
					Dictionary<string, object> dictionary2 = dictionary["values"] as Dictionary<string, object>;
					string obj = dictionary2["name"].ToString();
					RegisterSucceed(obj);
				}
				else
				{
					RegisterFailed(FastRegisterError.NotKnown, dictionary["message"].ToString());
				}
			}
		}

		// Token: 0x060000D3 RID: 211 RVA: 0x00004ADC File Offset: 0x00002EDC
		public void RestoreTransactionID(Action<RestoreTransactionIDState> result)
		{
			if (this.IsTestMode())
			{
				if (this.GetMode() == CloudMoolahMode.AlwaysSucceed)
				{
					result(RestoreTransactionIDState.RestoreSucceed);
				}
				else if (this.GetMode() == CloudMoolahMode.AlwaysFailed)
				{
					result(RestoreTransactionIDState.RestoreFailed);
				}
			}
			else
			{
				base.StartCoroutine(this.RestoreTransactionIDProcess(result));
			}
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x00004B38 File Offset: 0x00002F38
		private IEnumerator RestoreTransactionIDProcess(Action<RestoreTransactionIDState> result)
		{
			string customID = this.m_CustomerID;
			if (string.IsNullOrEmpty(customID))
			{
				Debug.LogError("customerId is null, use SystemInfo.deviceUniqueIdentifier or other user identification");
				result(RestoreTransactionIDState.RestoreFailed);
				yield break;
			}
			WWWForm wf = new WWWForm();
			wf.AddField("appId", this.m_appKey);
			wf.AddField("customerId", customID);
			DateTime now = DateTime.Now;
			string endDate = now.ToString("yyyy/MM/dd");
			string startDate = now.AddDays(-7.0).ToString("yyyy/MM/dd");
			wf.AddField("startDate", startDate);
			wf.AddField("endDate", endDate);
			string sign = this.GetStringMD5(this.m_appKey + customID + this.m_hashKey);
			wf.AddField("Sign", sign);
			WWW w = new WWW(MoolahStoreImpl.requestRestoreTransactionUrl, wf);
			yield return w;
			if (!string.IsNullOrEmpty(w.error))
			{
				Debug.LogError("RestoreTransactionID error : " + w.error);
				result(RestoreTransactionIDState.NotKnown);
			}
			else
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(w.text) as Dictionary<string, object>;
				string a = dictionary["code"].ToString();
				if (a == "1")
				{
					List<object> list = dictionary["values"] as List<object>;
					foreach (object obj in list)
					{
						Dictionary<string, object> dictionary2 = (Dictionary<string, object>)obj;
						string md5productID = dictionary2["productId"].ToString();
						string text = dictionary2["tradeSeq"].ToString();
						string receipt = dictionary2["receipt"].ToString();
						string value = dictionary2["authCode"].ToString();
						this.AddAuthCode(text, value);
						this.PurchaseSucceed(this.GetStoreSpecificId(md5productID), receipt, text);
					}
					result(RestoreTransactionIDState.RestoreSucceed);
				}
				else if (a == "CMB0000147")
				{
					result(RestoreTransactionIDState.NoTransactionRestore);
				}
				else
				{
					result(RestoreTransactionIDState.RestoreFailed);
				}
			}
			yield break;
		}

		// Token: 0x060000D5 RID: 213 RVA: 0x00004B64 File Offset: 0x00002F64
		public void ValidateReceipt(string transactionId, string receipt, Action<string, ValidateReceiptState, string> result)
		{
			if (string.IsNullOrEmpty(transactionId) || string.IsNullOrEmpty(receipt))
			{
				result(transactionId, ValidateReceiptState.ValidateFailed, "transactionId or receipt is null");
			}
			else if (this.IsTestMode())
			{
				if (this.GetMode() == CloudMoolahMode.AlwaysSucceed)
				{
					result(transactionId, ValidateReceiptState.ValidateSucceed, "TestMode ValidateSucceed");
				}
				else if (this.GetMode() == CloudMoolahMode.AlwaysFailed)
				{
					result(transactionId, ValidateReceiptState.ValidateFailed, "TestMode ValidateFailed");
				}
			}
			else
			{
				base.StartCoroutine(this.ValidateReceiptProcess(transactionId, receipt, result));
			}
		}

		// Token: 0x060000D6 RID: 214 RVA: 0x00004BF8 File Offset: 0x00002FF8
		private IEnumerator ValidateReceiptProcess(string transactionId, string receipt, Action<string, ValidateReceiptState, string> result)
		{
			Dictionary<string, object> tempJson = MiniJson.JsonDecode(receipt) as Dictionary<string, object>;
			if (tempJson != null)
			{
				if (tempJson["Payload"] != null)
				{
					receipt = tempJson["Payload"].ToString();
				}
			}
			WWWForm wf = new WWWForm();
			wf.AddField("appId", this.m_appKey);
			wf.AddField("receipt", receipt);
			string sign = this.GetStringMD5(this.m_appKey + receipt + this.m_hashKey);
			wf.AddField("sign", sign);
			WWW w = new WWW(MoolahStoreImpl.requestValidateReceiptUrl, wf);
			yield return w;
			if (!string.IsNullOrEmpty(w.error))
			{
				Debug.LogError("ValidateReceipt error : " + w.error);
				result(transactionId, ValidateReceiptState.NotKnown, "ValidateReceiptState NotKnown");
			}
			else
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(w.text) as Dictionary<string, object>;
				string a = dictionary["code"].ToString();
				string arg = dictionary["msg"].ToString();
				if (a == "1")
				{
					result(transactionId, ValidateReceiptState.ValidateSucceed, "ValidateReceiptState ValidateSucceeded");
				}
				else
				{
					result(transactionId, ValidateReceiptState.ValidateFailed, arg);
				}
			}
			yield break;
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00004C30 File Offset: 0x00003030
		public void RequestPayOut(string transactionId, Action<string, RequestPayOutState, string> result)
		{
			if (string.IsNullOrEmpty(transactionId))
			{
				result(transactionId, RequestPayOutState.RequestPayOutFailed, "transactionId is null!");
			}
			else if (this.IsTestMode())
			{
				if (this.GetMode() == CloudMoolahMode.AlwaysSucceed)
				{
					result(transactionId, RequestPayOutState.RequestPayOutSucceed, "TestMode RequestPayOutSucceed!");
				}
				else if (this.GetMode() == CloudMoolahMode.AlwaysFailed)
				{
					result(transactionId, RequestPayOutState.RequestPayOutFailed, "TestMode RequestPayOutFailed!");
				}
			}
			else
			{
				base.StartCoroutine(this.PayOutProcess(transactionId, result));
			}
		}

		// Token: 0x060000D8 RID: 216 RVA: 0x00004CB8 File Offset: 0x000030B8
		private IEnumerator PayOutProcess(string transactionId, Action<string, RequestPayOutState, string> result)
		{
			string authGlobal = this.GetAuthCode(transactionId);
			if (string.IsNullOrEmpty(authGlobal))
			{
				result(transactionId, RequestPayOutState.RequestPayOutFailed, "authGlobal is null");
				yield break;
			}
			string orderSuccess = "1";
			string signstr = authGlobal + orderSuccess + transactionId + this.m_hashKey;
			string sign = this.GetStringMD5(signstr);
			string param = string.Concat(new string[]
			{
				"?authCode=",
				authGlobal,
				"&orderSuccess=",
				orderSuccess,
				"&tradeSeq=",
				transactionId,
				"&sign=",
				sign
			});
			string url = MoolahStoreImpl.pollingPath + ((param != null) ? param : "");
			WWW w = new WWW(url);
			yield return w;
			if (!string.IsNullOrEmpty(w.error))
			{
				Debug.Log("RequestPayOut error : " + w.error);
				result(transactionId, RequestPayOutState.RequestPayOutNetworkError, "RequestPayOutNetworkError");
			}
			else
			{
				Dictionary<string, object> dictionary = MiniJson.JsonDecode(w.text) as Dictionary<string, object>;
				string a = dictionary["code"].ToString();
				string arg = dictionary["msg"].ToString();
				if (a == "1")
				{
					result(transactionId, RequestPayOutState.RequestPayOutSucceed, "RequestPayOutSucceeded");
				}
				else
				{
					result(transactionId, RequestPayOutState.RequestPayOutFailed, arg);
				}
			}
			yield break;
		}

		// Token: 0x060000D9 RID: 217 RVA: 0x00004CE8 File Offset: 0x000030E8
		private void RemoveAuthCode(string key)
		{
			if (!string.IsNullOrEmpty(key))
			{
				if (this.authCodeDic != null && this.authCodeDic.Count > 0)
				{
					if (this.authCodeDic.ContainsKey(key))
					{
						this.authCodeDic.Remove(key);
					}
				}
			}
		}

		// Token: 0x060000DA RID: 218 RVA: 0x00004D48 File Offset: 0x00003148
		private void AddAuthCode(string key, string value)
		{
			if (!string.IsNullOrEmpty(key) && !string.IsNullOrEmpty(value))
			{
				if (this.authCodeDic == null)
				{
					this.authCodeDic = new Dictionary<string, string>();
				}
				if (this.authCodeDic.ContainsKey(key))
				{
					this.authCodeDic.Remove(key);
				}
				this.authCodeDic.Add(key, value);
			}
		}

		// Token: 0x060000DB RID: 219 RVA: 0x00004DB4 File Offset: 0x000031B4
		private string GetAuthCode(string key)
		{
			if (this.authCodeDic == null)
			{
				this.authCodeDic = new Dictionary<string, string>();
			}
			string empty = string.Empty;
			string result;
			if (string.IsNullOrEmpty(key))
			{
				result = empty;
			}
			else if (this.authCodeDic.TryGetValue(key, out empty))
			{
				result = empty;
			}
			else
			{
				result = empty;
			}
			return result;
		}

		// Token: 0x060000DC RID: 220 RVA: 0x00004E14 File Offset: 0x00003214
		private void ClearAuthCode()
		{
			if (this.authCodeDic != null)
			{
				this.authCodeDic.Clear();
			}
		}

		// Token: 0x04000068 RID: 104
		private static readonly string pollingPath = "https://api.cloudmoolah.com/CMPayment/api/polling";

		// Token: 0x04000069 RID: 105
		private static readonly string requestAuthCodePath = "https://api.cloudmoolah.com/CMPayment/api/authGlobal";

		// Token: 0x0400006A RID: 106
		private static readonly string requestRestoreTransactionUrl = "https://api.cloudmoolah.com/CMPayment/recover";

		// Token: 0x0400006B RID: 107
		private static readonly string requestValidateReceiptUrl = "https://api.cloudmoolah.com/CMPayment/receipt/validate";

		// Token: 0x0400006C RID: 108
		private static readonly string requestProductValidateUrl = "https://api.cloudmoolah.com/CMPayment/product/validate";

		// Token: 0x0400006D RID: 109
		private IStoreCallback m_callback;

		// Token: 0x0400006E RID: 110
		private bool isNeedPolling = false;

		// Token: 0x0400006F RID: 111
		private ReadOnlyCollection<ProductDefinition> m_productDefinitions;

		// Token: 0x04000070 RID: 112
		private Dictionary<string, string> authCodeDic;

		// Token: 0x04000071 RID: 113
		private string m_md5StoreProductID;

		// Token: 0x04000072 RID: 114
		private bool isRequestAuthCodeing = false;

		// Token: 0x04000073 RID: 115
		private string m_appKey;

		// Token: 0x04000074 RID: 116
		private string m_hashKey;

		// Token: 0x04000075 RID: 117
		private CloudMoolahMode m_mode;

		// Token: 0x04000076 RID: 118
		private bool m_IsTestMode = false;

		// Token: 0x04000077 RID: 119
		private const int timeOutMiddle = 10000;

		// Token: 0x04000078 RID: 120
		private static readonly string CM_PATH = "http://m.cloudmoolah.com";

		// Token: 0x04000079 RID: 121
		private static readonly string REGISTER_OR_BINDING_PATH = "/registry/fast";

		// Token: 0x0400007A RID: 122
		private static readonly string LOGIN_PATH = "/login";

		// Token: 0x0400007B RID: 123
		private HttpManager httpManager;

		// Token: 0x0400007C RID: 124
		private bool isLogining = false;

		// Token: 0x0400007D RID: 125
		private bool isRegister = false;

		// Token: 0x0400007E RID: 126
		private string m_LoginToken;

		// Token: 0x0400007F RID: 127
		private string m_CustomerID;
	}
}
