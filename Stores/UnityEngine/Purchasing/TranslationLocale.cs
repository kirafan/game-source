﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000051 RID: 81
	public enum TranslationLocale
	{
		// Token: 0x040000A8 RID: 168
		zh_TW,
		// Token: 0x040000A9 RID: 169
		cs_CZ,
		// Token: 0x040000AA RID: 170
		da_DK,
		// Token: 0x040000AB RID: 171
		nl_NL,
		// Token: 0x040000AC RID: 172
		en_US,
		// Token: 0x040000AD RID: 173
		fr_FR,
		// Token: 0x040000AE RID: 174
		fi_FI,
		// Token: 0x040000AF RID: 175
		de_DE,
		// Token: 0x040000B0 RID: 176
		iw_IL,
		// Token: 0x040000B1 RID: 177
		hi_IN,
		// Token: 0x040000B2 RID: 178
		it_IT,
		// Token: 0x040000B3 RID: 179
		ja_JP,
		// Token: 0x040000B4 RID: 180
		ko_KR,
		// Token: 0x040000B5 RID: 181
		no_NO,
		// Token: 0x040000B6 RID: 182
		pl_PL,
		// Token: 0x040000B7 RID: 183
		pt_PT,
		// Token: 0x040000B8 RID: 184
		ru_RU,
		// Token: 0x040000B9 RID: 185
		es_ES,
		// Token: 0x040000BA RID: 186
		sv_SE,
		// Token: 0x040000BB RID: 187
		zh_CN
	}
}
