﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using AOT;
using Uniject;
using UnityEngine.Purchasing.Extension;
using UnityEngine.Purchasing.Security;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001C RID: 28
	internal class AppleStoreImpl : NativeJSONStore, IAppleExtensions, IAppleConfiguration, IStoreExtension, IStoreConfiguration
	{
		// Token: 0x06000049 RID: 73 RVA: 0x00002A6B File Offset: 0x00000E6B
		public AppleStoreImpl(IUtil util)
		{
			AppleStoreImpl.util = util;
			AppleStoreImpl.instance = this;
		}

		// Token: 0x0600004A RID: 74 RVA: 0x00002A80 File Offset: 0x00000E80
		public void SetNativeStore(INativeAppleStore apple)
		{
			base.SetNativeStore(apple);
			this.m_Native = apple;
			if (AppleStoreImpl.<>f__mg$cache0 == null)
			{
				AppleStoreImpl.<>f__mg$cache0 = new UnityPurchasingCallback(AppleStoreImpl.MessageCallback);
			}
			apple.SetUnityPurchasingCallback(AppleStoreImpl.<>f__mg$cache0);
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x0600004B RID: 75 RVA: 0x00002AB4 File Offset: 0x00000EB4
		public string appReceipt
		{
			get
			{
				return this.m_Native.appReceipt;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600004C RID: 76 RVA: 0x00002AD4 File Offset: 0x00000ED4
		public bool canMakePayments
		{
			get
			{
				return this.m_Native.canMakePayments;
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600004D RID: 77 RVA: 0x00002AF4 File Offset: 0x00000EF4
		// (set) Token: 0x0600004E RID: 78 RVA: 0x00002B14 File Offset: 0x00000F14
		public bool simulateAskToBuy
		{
			get
			{
				return this.m_Native.simulateAskToBuy;
			}
			set
			{
				this.m_Native.simulateAskToBuy = value;
			}
		}

		// Token: 0x0600004F RID: 79 RVA: 0x00002B23 File Offset: 0x00000F23
		public void SetApplicationUsername(string applicationUsername)
		{
			this.m_Native.SetApplicationUsername(applicationUsername);
		}

		// Token: 0x06000050 RID: 80 RVA: 0x00002B34 File Offset: 0x00000F34
		public override void OnProductsRetrieved(string json)
		{
			List<ProductDescription> list = JSONSerializer.DeserializeProductDescriptions(json);
			List<ProductDescription> list2 = null;
			if (this.m_Native != null)
			{
				string appReceipt = this.m_Native.appReceipt;
				if (!string.IsNullOrEmpty(appReceipt))
				{
					AppleReceiptParser appleReceiptParser = new AppleReceiptParser();
					AppleReceipt appleReceipt = null;
					try
					{
						appleReceipt = appleReceiptParser.Parse(Convert.FromBase64String(appReceipt));
					}
					catch (ArgumentException)
					{
						appleReceipt = null;
					}
					if (appleReceipt != null && appleReceipt.inAppPurchaseReceipts != null && appleReceipt.inAppPurchaseReceipts.Length > 0)
					{
						list2 = new List<ProductDescription>();
						using (List<ProductDescription>.Enumerator enumerator = list.GetEnumerator())
						{
							while (enumerator.MoveNext())
							{
								ProductDescription productDescription = enumerator.Current;
								if (Array.Find<AppleInAppPurchaseReceipt>(appleReceipt.inAppPurchaseReceipts, (AppleInAppPurchaseReceipt r) => r.productID == productDescription.storeSpecificId) == null)
								{
									list2.Add(productDescription);
								}
								else
								{
									list2.Add(new ProductDescription(productDescription.storeSpecificId, productDescription.metadata, appReceipt, productDescription.transactionId));
								}
							}
						}
					}
				}
			}
			this.unity.OnProductsRetrieved(list2 ?? list);
			this.m_Native.AddTransactionObserver();
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00002CAC File Offset: 0x000010AC
		public void RestoreTransactions(Action<bool> callback)
		{
			this.m_RestoreCallback = callback;
			this.m_Native.RestoreTransactions();
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00002CC1 File Offset: 0x000010C1
		public void RefreshAppReceipt(Action<string> successCallback, Action errorCallback)
		{
			this.m_RefreshReceiptSuccess = successCallback;
			this.m_RefreshReceiptError = errorCallback;
			this.m_Native.RefreshAppReceipt();
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00002CDD File Offset: 0x000010DD
		public void RegisterPurchaseDeferredListener(Action<Product> callback)
		{
			this.m_DeferredCallback = callback;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00002CE8 File Offset: 0x000010E8
		public void OnPurchaseDeferred(string productId)
		{
			if (this.m_DeferredCallback != null)
			{
				Product product = this.unity.products.WithStoreSpecificID(productId);
				if (product != null)
				{
					this.m_DeferredCallback(product);
				}
			}
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00002D27 File Offset: 0x00001127
		public void OnTransactionsRestoredSuccess()
		{
			if (this.m_RestoreCallback != null)
			{
				this.m_RestoreCallback(true);
			}
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00002D41 File Offset: 0x00001141
		public void OnTransactionsRestoredFail(string error)
		{
			if (this.m_RestoreCallback != null)
			{
				this.m_RestoreCallback(false);
			}
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00002D5B File Offset: 0x0000115B
		public void OnAppReceiptRetrieved(string receipt)
		{
			if (receipt != null)
			{
				if (this.m_RefreshReceiptSuccess != null)
				{
					this.m_RefreshReceiptSuccess(receipt);
				}
			}
		}

		// Token: 0x06000058 RID: 88 RVA: 0x00002D7D File Offset: 0x0000117D
		public void OnAppReceiptRefreshedFailed()
		{
			if (this.m_RefreshReceiptError != null)
			{
				this.m_RefreshReceiptError();
			}
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00002D98 File Offset: 0x00001198
		[MonoPInvokeCallback(typeof(UnityPurchasingCallback))]
		private static void MessageCallback(string subject, string payload, string receipt, string transactionId)
		{
			AppleStoreImpl.util.RunOnMainThread(delegate
			{
				AppleStoreImpl.instance.ProcessMessage(subject, payload, receipt, transactionId);
			});
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00002DE0 File Offset: 0x000011E0
		private void ProcessMessage(string subject, string payload, string receipt, string transactionId)
		{
			switch (subject)
			{
			case "OnSetupFailed":
				base.OnSetupFailed(payload);
				break;
			case "OnProductsRetrieved":
				this.OnProductsRetrieved(payload);
				break;
			case "OnPurchaseSucceeded":
				base.OnPurchaseSucceeded(payload, receipt, transactionId);
				break;
			case "OnPurchaseFailed":
				base.OnPurchaseFailed(payload);
				break;
			case "onProductPurchaseDeferred":
				this.OnPurchaseDeferred(payload);
				break;
			case "onTransactionsRestoredSuccess":
				this.OnTransactionsRestoredSuccess();
				break;
			case "onTransactionsRestoredFail":
				this.OnTransactionsRestoredFail(payload);
				break;
			case "onAppReceiptRefreshed":
				this.OnAppReceiptRetrieved(payload);
				break;
			case "onAppReceiptRefreshFailed":
				this.OnAppReceiptRefreshedFailed();
				break;
			}
		}

		// Token: 0x04000013 RID: 19
		private Action<Product> m_DeferredCallback;

		// Token: 0x04000014 RID: 20
		private Action m_RefreshReceiptError;

		// Token: 0x04000015 RID: 21
		private Action<string> m_RefreshReceiptSuccess;

		// Token: 0x04000016 RID: 22
		private Action<bool> m_RestoreCallback;

		// Token: 0x04000017 RID: 23
		private INativeAppleStore m_Native;

		// Token: 0x04000018 RID: 24
		private static IUtil util;

		// Token: 0x04000019 RID: 25
		private static AppleStoreImpl instance;

		// Token: 0x0400001A RID: 26
		[CompilerGenerated]
		private static UnityPurchasingCallback <>f__mg$cache0;
	}
}
