﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000003 RID: 3
	public interface IAndroidStoreSelection : IStoreConfiguration
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000006 RID: 6
		AndroidStore androidStore { get; }
	}
}
