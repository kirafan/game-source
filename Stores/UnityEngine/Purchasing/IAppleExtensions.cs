﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000020 RID: 32
	public interface IAppleExtensions : IStoreExtension
	{
		// Token: 0x06000067 RID: 103
		void RefreshAppReceipt(Action<string> successCallback, Action errorCallback);

		// Token: 0x06000068 RID: 104
		void RestoreTransactions(Action<bool> callback);

		// Token: 0x06000069 RID: 105
		void RegisterPurchaseDeferredListener(Action<Product> callback);

		// Token: 0x0600006A RID: 106
		void SetApplicationUsername(string applicationUsername);

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600006B RID: 107
		// (set) Token: 0x0600006C RID: 108
		bool simulateAskToBuy { get; set; }
	}
}
