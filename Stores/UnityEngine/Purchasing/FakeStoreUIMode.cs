﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000049 RID: 73
	public enum FakeStoreUIMode
	{
		// Token: 0x04000095 RID: 149
		Default,
		// Token: 0x04000096 RID: 150
		StandardUser,
		// Token: 0x04000097 RID: 151
		DeveloperUser
	}
}
