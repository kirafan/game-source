﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001F RID: 31
	public interface IAppleConfiguration : IStoreConfiguration
	{
		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000065 RID: 101
		string appReceipt { get; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000066 RID: 102
		bool canMakePayments { get; }
	}
}
