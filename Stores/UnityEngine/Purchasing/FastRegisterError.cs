﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000038 RID: 56
	public enum FastRegisterError
	{
		// Token: 0x0400004E RID: 78
		PasswordEmpty,
		// Token: 0x0400004F RID: 79
		FastRegisterCallBackIsNull,
		// Token: 0x04000050 RID: 80
		NetworkError,
		// Token: 0x04000051 RID: 81
		NotKnown
	}
}
