﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000029 RID: 41
	[Obsolete("Use WindowsStore.Name for Universal Windows Apps")]
	public class WinRT
	{
		// Token: 0x04000031 RID: 49
		public const string Name = "WinRT";
	}
}
