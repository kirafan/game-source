﻿using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000056 RID: 86
	internal static class ProductDefinitionExtensions
	{
		// Token: 0x0600014B RID: 331 RVA: 0x00007AFC File Offset: 0x00005EFC
		public static string[] Identifiers(this ReadOnlyCollection<ProductDefinition> identifiers)
		{
			return (from x in identifiers
			select x.storeSpecificId).ToArray<string>();
		}
	}
}
