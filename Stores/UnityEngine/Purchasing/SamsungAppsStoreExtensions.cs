﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001A RID: 26
	internal class SamsungAppsStoreExtensions : AndroidJavaProxy, ISamsungAppsCallback, ISamsungAppsExtensions, ISamsungAppsConfiguration, IStoreExtension, IStoreConfiguration
	{
		// Token: 0x06000043 RID: 67 RVA: 0x000029E6 File Offset: 0x00000DE6
		public SamsungAppsStoreExtensions() : base("com.unity.purchasing.samsung.ISamsungAppsStoreCallback")
		{
		}

		// Token: 0x06000044 RID: 68 RVA: 0x000029F4 File Offset: 0x00000DF4
		public void SetAndroidJavaObject(AndroidJavaObject java)
		{
			this.m_Java = java;
		}

		// Token: 0x06000045 RID: 69 RVA: 0x000029FE File Offset: 0x00000DFE
		public void SetMode(SamsungAppsMode mode)
		{
			this.m_Java.Call("setMode", new object[]
			{
				mode.ToString()
			});
		}

		// Token: 0x06000046 RID: 70 RVA: 0x00002A27 File Offset: 0x00000E27
		public void RestoreTransactions(Action<bool> callback)
		{
			this.m_RestoreCallback = callback;
			this.m_Java.Call("restoreTransactions", new object[0]);
		}

		// Token: 0x06000047 RID: 71 RVA: 0x00002A47 File Offset: 0x00000E47
		public void OnTransactionsRestored(bool result)
		{
			if (this.m_RestoreCallback != null)
			{
				this.m_RestoreCallback(result);
			}
		}

		// Token: 0x04000010 RID: 16
		private Action<bool> m_RestoreCallback;

		// Token: 0x04000011 RID: 17
		private AndroidJavaObject m_Java;
	}
}
