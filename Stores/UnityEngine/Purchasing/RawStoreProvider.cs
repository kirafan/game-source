﻿using System;
using Uniject;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000057 RID: 87
	internal class RawStoreProvider : IRawStoreProvider
	{
		// Token: 0x0600014E RID: 334 RVA: 0x00007B60 File Offset: 0x00005F60
		public INativeStore GetAndroidStore(IUnityCallback callback, AndroidStore store, IPurchasingBinder binder, IUtil util)
		{
			if (store != AndroidStore.AmazonAppStore)
			{
				if (store != AndroidStore.GooglePlay)
				{
					if (store != AndroidStore.SamsungApps)
					{
						goto IL_142;
					}
					goto IL_CB;
				}
			}
			else
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity.purchasing.amazon.AmazonPurchasing"))
				{
					JavaBridge javaBridge = new JavaBridge(new ScriptingUnityCallback(callback, util));
					AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("instance", new object[]
					{
						javaBridge
					});
					AmazonAppStoreStoreExtensions instance = new AmazonAppStoreStoreExtensions(androidJavaObject);
					binder.RegisterExtension<IAmazonExtensions>(instance);
					binder.RegisterConfiguration<IAmazonConfiguration>(instance);
					return new AndroidJavaStore(androidJavaObject);
				}
			}
			using (AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("com.unity.purchasing.googleplay.GooglePlayPurchasing"))
			{
				JavaBridge javaBridge2 = new JavaBridge(new ScriptingUnityCallback(callback, util));
				AndroidJavaObject store2 = androidJavaClass2.CallStatic<AndroidJavaObject>("instance", new object[]
				{
					javaBridge2
				});
				return new GooglePlayAndroidJavaStore(store2);
			}
			IL_CB:
			using (AndroidJavaClass androidJavaClass3 = new AndroidJavaClass("com.unity.purchasing.samsung.SamsungPurchasing"))
			{
				SamsungAppsStoreExtensions samsungAppsStoreExtensions = new SamsungAppsStoreExtensions();
				JavaBridge javaBridge3 = new JavaBridge(new ScriptingUnityCallback(callback, util));
				AndroidJavaObject androidJavaObject2 = androidJavaClass3.CallStatic<AndroidJavaObject>("instance", new object[]
				{
					javaBridge3,
					samsungAppsStoreExtensions
				});
				samsungAppsStoreExtensions.SetAndroidJavaObject(androidJavaObject2);
				binder.RegisterExtension<ISamsungAppsExtensions>(samsungAppsStoreExtensions);
				binder.RegisterConfiguration<ISamsungAppsConfiguration>(samsungAppsStoreExtensions);
				return new AndroidJavaStore(androidJavaObject2);
			}
			IL_142:
			throw new NotImplementedException();
		}

		// Token: 0x0600014F RID: 335 RVA: 0x00007CE0 File Offset: 0x000060E0
		public INativeAppleStore GetStorekit(IUnityCallback callback)
		{
			INativeAppleStore result;
			if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.tvOS)
			{
				result = new iOSStoreBindings();
			}
			else
			{
				result = new OSXStoreBindings();
			}
			return result;
		}

		// Token: 0x06000150 RID: 336 RVA: 0x00007D20 File Offset: 0x00006120
		public INativeTizenStore GetTizenStore(IUnityCallback callback, IPurchasingBinder binder)
		{
			return new TizenStoreBindings();
		}

		// Token: 0x06000151 RID: 337 RVA: 0x00007D3C File Offset: 0x0000613C
		public INativeFacebookStore GetFacebookStore()
		{
			return new FacebookStoreBindings();
		}

		// Token: 0x06000152 RID: 338 RVA: 0x00007D58 File Offset: 0x00006158
		public INativeFacebookStore GetFacebookStore(IUnityCallback callback, IPurchasingBinder binder)
		{
			return new FacebookStoreBindings();
		}
	}
}
