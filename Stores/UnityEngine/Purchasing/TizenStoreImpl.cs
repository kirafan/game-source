﻿using System;
using System.Runtime.CompilerServices;
using AOT;
using Uniject;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200002D RID: 45
	internal class TizenStoreImpl : NativeJSONStore, ITizenStoreConfiguration, IStoreConfiguration
	{
		// Token: 0x0600008B RID: 139 RVA: 0x000035FA File Offset: 0x000019FA
		public TizenStoreImpl(IUtil util)
		{
			TizenStoreImpl.instance = this;
		}

		// Token: 0x0600008C RID: 140 RVA: 0x00003609 File Offset: 0x00001A09
		public void SetNativeStore(INativeTizenStore tizen)
		{
			base.SetNativeStore(tizen);
			this.m_Native = tizen;
			INativeTizenStore native = this.m_Native;
			if (TizenStoreImpl.<>f__mg$cache0 == null)
			{
				TizenStoreImpl.<>f__mg$cache0 = new UnityNativePurchasingCallback(TizenStoreImpl.MessageCallback);
			}
			native.SetUnityPurchasingCallback(TizenStoreImpl.<>f__mg$cache0);
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00003642 File Offset: 0x00001A42
		public void SetGroupId(string group)
		{
			this.m_Native.SetGroupId(group);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x00003651 File Offset: 0x00001A51
		[MonoPInvokeCallback(typeof(UnityNativePurchasingCallback))]
		private static void MessageCallback(string subject, string payload, string receipt, string transactionId)
		{
			TizenStoreImpl.instance.ProcessMessage(subject, payload, receipt, transactionId);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00003664 File Offset: 0x00001A64
		private void ProcessMessage(string subject, string payload, string receipt, string transactionId)
		{
			Debug.Log(string.Concat(new string[]
			{
				"[UnityIAP] ProcessMessage subject: ",
				subject,
				" payload: ",
				payload,
				" receipt: ",
				receipt,
				" transactionId: ",
				transactionId
			}));
			if (subject != null)
			{
				if (!(subject == "OnSetupFailed"))
				{
					if (!(subject == "OnProductsRetrieved"))
					{
						if (!(subject == "OnPurchaseSucceeded"))
						{
							if (subject == "OnPurchaseFailed")
							{
								base.OnPurchaseFailed(payload);
							}
						}
						else
						{
							base.OnPurchaseSucceeded(payload, receipt, transactionId);
						}
					}
					else
					{
						this.OnProductsRetrieved(payload);
					}
				}
				else
				{
					base.OnSetupFailed(payload);
				}
			}
		}

		// Token: 0x04000034 RID: 52
		private static TizenStoreImpl instance;

		// Token: 0x04000035 RID: 53
		private INativeTizenStore m_Native;

		// Token: 0x04000036 RID: 54
		[CompilerGenerated]
		private static UnityNativePurchasingCallback <>f__mg$cache0;
	}
}
