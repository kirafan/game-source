﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000048 RID: 72
	public enum AndroidStore
	{
		// Token: 0x0400008E RID: 142
		GooglePlay,
		// Token: 0x0400008F RID: 143
		AmazonAppStore,
		// Token: 0x04000090 RID: 144
		CloudMoolah,
		// Token: 0x04000091 RID: 145
		SamsungApps,
		// Token: 0x04000092 RID: 146
		XiaomiMiPay,
		// Token: 0x04000093 RID: 147
		NotSpecified
	}
}
