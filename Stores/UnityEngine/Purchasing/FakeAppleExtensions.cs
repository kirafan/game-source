﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001E RID: 30
	internal class FakeAppleExtensions : IAppleExtensions, IStoreExtension
	{
		// Token: 0x0600005F RID: 95 RVA: 0x00002FCA File Offset: 0x000013CA
		public void RefreshAppReceipt(Action<string> successCallback, Action errorCallback)
		{
			if (this.m_FailRefresh)
			{
				errorCallback();
			}
			else
			{
				successCallback("A fake refreshed receipt!");
			}
			this.m_FailRefresh = !this.m_FailRefresh;
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00002FFD File Offset: 0x000013FD
		public void RestoreTransactions(Action<bool> callback)
		{
			callback(true);
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00003007 File Offset: 0x00001407
		public void RegisterPurchaseDeferredListener(Action<Product> callback)
		{
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000062 RID: 98 RVA: 0x0000300C File Offset: 0x0000140C
		// (set) Token: 0x06000063 RID: 99 RVA: 0x00003026 File Offset: 0x00001426
		public bool simulateAskToBuy { get; set; }

		// Token: 0x06000064 RID: 100 RVA: 0x0000302F File Offset: 0x0000142F
		public void SetApplicationUsername(string applicationUsername)
		{
		}

		// Token: 0x0400001C RID: 28
		private bool m_FailRefresh;
	}
}
