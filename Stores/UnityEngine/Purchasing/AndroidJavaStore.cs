﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000002 RID: 2
	internal class AndroidJavaStore : INativeStore
	{
		// Token: 0x06000001 RID: 1 RVA: 0x000021CA File Offset: 0x000005CA
		public AndroidJavaStore(AndroidJavaObject store)
		{
			this.m_Store = store;
		}

		// Token: 0x06000002 RID: 2 RVA: 0x000021DC File Offset: 0x000005DC
		protected AndroidJavaObject GetStore()
		{
			return this.m_Store;
		}

		// Token: 0x06000003 RID: 3 RVA: 0x000021F7 File Offset: 0x000005F7
		public void RetrieveProducts(string json)
		{
			this.m_Store.Call("RetrieveProducts", new object[]
			{
				json
			});
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002214 File Offset: 0x00000614
		public virtual void Purchase(string productJSON, string developerPayload)
		{
			this.m_Store.Call("Purchase", new object[]
			{
				productJSON,
				developerPayload
			});
		}

		// Token: 0x06000005 RID: 5 RVA: 0x00002235 File Offset: 0x00000635
		public void FinishTransaction(string productJSON, string transactionID)
		{
			this.m_Store.Call("FinishTransaction", new object[]
			{
				productJSON,
				transactionID
			});
		}

		// Token: 0x04000001 RID: 1
		private AndroidJavaObject m_Store;
	}
}
