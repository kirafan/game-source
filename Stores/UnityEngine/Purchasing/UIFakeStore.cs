﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Uniject;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200005C RID: 92
	internal class UIFakeStore : FakeStore
	{
		// Token: 0x06000174 RID: 372 RVA: 0x000085A3 File Offset: 0x000069A3
		public UIFakeStore()
		{
		}

		// Token: 0x06000175 RID: 373 RVA: 0x000085AC File Offset: 0x000069AC
		public UIFakeStore(IUtil util)
		{
			this.m_Util = util;
		}

		// Token: 0x06000176 RID: 374 RVA: 0x000085BC File Offset: 0x000069BC
		protected override bool StartUI<T>(object model, FakeStore.DialogType dialogType, Action<bool, T> callback)
		{
			List<string> list = new List<string>();
			list.Add("Success");
			IEnumerator enumerator = Enum.GetValues(typeof(T)).GetEnumerator();
			try
			{
				while (enumerator.MoveNext())
				{
					object obj = enumerator.Current;
					T t = (T)((object)obj);
					list.Add(t.ToString());
				}
			}
			finally
			{
				IDisposable disposable;
				if ((disposable = (enumerator as IDisposable)) != null)
				{
					disposable.Dispose();
				}
			}
			Action<bool, int> callback2 = delegate(bool result, int codeValue)
			{
				T arg = (T)((object)codeValue);
				callback(result, arg);
			};
			string queryText = null;
			string okayButtonText = null;
			if (dialogType == FakeStore.DialogType.Purchase)
			{
				queryText = this.CreatePurchaseQuestion((ProductDefinition)model);
				if (this.UIMode == FakeStoreUIMode.DeveloperUser)
				{
					okayButtonText = "OK";
				}
				else
				{
					okayButtonText = "Buy";
				}
			}
			else if (dialogType == FakeStore.DialogType.RetrieveProducts)
			{
				queryText = this.CreateRetrieveProductsQuestion((ReadOnlyCollection<ProductDefinition>)model);
				okayButtonText = "OK";
			}
			else
			{
				Debug.LogError("Unrecognized DialogType " + dialogType);
			}
			string cancelButtonText = "Cancel";
			return this.StartUI(queryText, okayButtonText, cancelButtonText, list, callback2);
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00008708 File Offset: 0x00006B08
		private bool StartUI(string queryText, string okayButtonText, string cancelButtonText, List<string> options, Action<bool, int> callback)
		{
			bool result;
			if (this.IsShowingDialog())
			{
				result = false;
			}
			else
			{
				this.m_CurrentDialog = new UIFakeStore.DialogRequest
				{
					QueryText = queryText,
					OkayButtonText = okayButtonText,
					CancelButtonText = cancelButtonText,
					Options = options,
					Callback = callback
				};
				this.InstantiateDialog();
				result = true;
			}
			return result;
		}

		// Token: 0x06000178 RID: 376 RVA: 0x0000876C File Offset: 0x00006B6C
		private void InstantiateDialog()
		{
			if (this.m_CurrentDialog == null)
			{
				Debug.LogError(this + " requires m_CurrentDialog. Not showing dialog.");
			}
			else
			{
				if (this.UIFakeStoreCanvasPrefab == null)
				{
					this.UIFakeStoreCanvasPrefab = (Resources.Load("UIFakeStoreCanvas") as GameObject);
				}
				Canvas component = this.UIFakeStoreCanvasPrefab.GetComponent<Canvas>();
				this.m_Canvas = Object.Instantiate<Canvas>(component);
				UIFakeStore.LifecycleNotifier lifecycleNotifier = this.m_Canvas.gameObject.AddComponent<UIFakeStore.LifecycleNotifier>();
				lifecycleNotifier.OnDestroyCallback = delegate()
				{
					this.m_CurrentDialog = null;
				};
				this.m_ParentGameObjectPath = this.m_Canvas.name + "/Panel/";
				if (Object.FindObjectOfType<EventSystem>() == null)
				{
					this.m_EventSystem = new GameObject("EventSystem", new Type[]
					{
						typeof(EventSystem)
					});
					this.m_EventSystem.AddComponent<StandaloneInputModule>();
					this.m_EventSystem.transform.parent = this.m_Canvas.transform;
				}
				GameObject gameObject = GameObject.Find(this.m_ParentGameObjectPath + "HeaderText");
				Text component2 = gameObject.GetComponent<Text>();
				component2.text = this.m_CurrentDialog.QueryText;
				Text okayButtonText = this.GetOkayButtonText();
				okayButtonText.text = this.m_CurrentDialog.OkayButtonText;
				Text cancelButtonText = this.GetCancelButtonText();
				cancelButtonText.text = this.m_CurrentDialog.CancelButtonText;
				this.GetDropdown().options.Clear();
				foreach (string text in this.m_CurrentDialog.Options)
				{
					this.GetDropdown().options.Add(new Dropdown.OptionData(text));
				}
				if (this.m_CurrentDialog.Options.Count > 0)
				{
					this.m_LastSelectedDropdownIndex = 0;
				}
				this.GetDropdown().RefreshShownValue();
				this.GetOkayButton().onClick.AddListener(delegate()
				{
					this.OkayButtonClicked();
				});
				this.GetCancelButton().onClick.AddListener(delegate()
				{
					this.CancelButtonClicked();
				});
				this.GetDropdown().onValueChanged.AddListener(delegate(int selectedItem)
				{
					this.DropdownValueChanged(selectedItem);
				});
				if (this.UIMode == FakeStoreUIMode.StandardUser)
				{
					this.GetDropdown().onValueChanged.RemoveAllListeners();
					Object.Destroy(this.GetDropdownContainerGameObject());
				}
				else if (this.UIMode == FakeStoreUIMode.DeveloperUser)
				{
					this.GetCancelButton().onClick.RemoveAllListeners();
					Object.Destroy(this.GetCancelButtonGameObject());
				}
			}
		}

		// Token: 0x06000179 RID: 377 RVA: 0x00008A28 File Offset: 0x00006E28
		private string CreatePurchaseQuestion(ProductDefinition definition)
		{
			return "Do you want to Purchase " + definition.id + "?\n\n[Environment: FakeStore]";
		}

		// Token: 0x0600017A RID: 378 RVA: 0x00008A54 File Offset: 0x00006E54
		private string CreateRetrieveProductsQuestion(ReadOnlyCollection<ProductDefinition> definitions)
		{
			string str = "Do you want to initialize purchasing for products {";
			str += string.Join(", ", (from pid in definitions.Take(2)
			select pid.id).ToArray<string>());
			if (definitions.Count > 2)
			{
				str += ", ...";
			}
			return str + "}?\n\n[Environment: FakeStore]";
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00008AD8 File Offset: 0x00006ED8
		private Button GetOkayButton()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Button1").GetComponent<Button>();
		}

		// Token: 0x0600017C RID: 380 RVA: 0x00008B08 File Offset: 0x00006F08
		private Button GetCancelButton()
		{
			GameObject gameObject = GameObject.Find(this.m_ParentGameObjectPath + "Button2");
			Button result;
			if (gameObject != null)
			{
				result = gameObject.GetComponent<Button>();
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00008B50 File Offset: 0x00006F50
		private GameObject GetCancelButtonGameObject()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Button2");
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00008B7C File Offset: 0x00006F7C
		private Text GetOkayButtonText()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Button1/Text").GetComponent<Text>();
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00008BAC File Offset: 0x00006FAC
		private Text GetCancelButtonText()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Button2/Text").GetComponent<Text>();
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00008BDC File Offset: 0x00006FDC
		private Dropdown GetDropdown()
		{
			GameObject gameObject = GameObject.Find(this.m_ParentGameObjectPath + "Panel2/Panel3/Dropdown");
			Dropdown result;
			if (gameObject != null)
			{
				result = gameObject.GetComponent<Dropdown>();
			}
			else
			{
				result = null;
			}
			return result;
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00008C24 File Offset: 0x00007024
		private GameObject GetDropdownContainerGameObject()
		{
			return GameObject.Find(this.m_ParentGameObjectPath + "Panel2");
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00008C50 File Offset: 0x00007050
		private void OkayButtonClicked()
		{
			bool arg = false;
			if (this.m_LastSelectedDropdownIndex == 0 || this.UIMode != FakeStoreUIMode.DeveloperUser)
			{
				arg = true;
			}
			int arg2 = Math.Max(0, this.m_LastSelectedDropdownIndex - 1);
			this.m_CurrentDialog.Callback(arg, arg2);
			this.CloseDialog();
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00008CA4 File Offset: 0x000070A4
		private void CancelButtonClicked()
		{
			int arg = Math.Max(0, this.m_LastSelectedDropdownIndex - 1);
			this.m_CurrentDialog.Callback(false, arg);
			this.CloseDialog();
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00008CD9 File Offset: 0x000070D9
		private void DropdownValueChanged(int selectedItem)
		{
			this.m_LastSelectedDropdownIndex = selectedItem;
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00008CE4 File Offset: 0x000070E4
		private void CloseDialog()
		{
			this.m_CurrentDialog = null;
			this.GetOkayButton().onClick.RemoveAllListeners();
			if (this.GetCancelButton())
			{
				this.GetCancelButton().onClick.RemoveAllListeners();
			}
			if (this.GetDropdown() != null)
			{
				this.GetDropdown().onValueChanged.RemoveAllListeners();
			}
			Object.Destroy(this.m_Canvas.gameObject);
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00008D60 File Offset: 0x00007160
		public bool IsShowingDialog()
		{
			return this.m_CurrentDialog != null;
		}

		// Token: 0x040000E5 RID: 229
		private const string EnvironmentDescriptionPostfix = "\n\n[Environment: FakeStore]";

		// Token: 0x040000E6 RID: 230
		private const string SuccessString = "Success";

		// Token: 0x040000E7 RID: 231
		private const int RetrieveProductsDescriptionCount = 2;

		// Token: 0x040000E8 RID: 232
		private UIFakeStore.DialogRequest m_CurrentDialog;

		// Token: 0x040000E9 RID: 233
		private int m_LastSelectedDropdownIndex;

		// Token: 0x040000EA RID: 234
		private GameObject UIFakeStoreCanvasPrefab;

		// Token: 0x040000EB RID: 235
		private Canvas m_Canvas;

		// Token: 0x040000EC RID: 236
		private GameObject m_EventSystem;

		// Token: 0x040000ED RID: 237
		private string m_ParentGameObjectPath;

		// Token: 0x040000EE RID: 238
		private IUtil m_Util;

		// Token: 0x0200005D RID: 93
		protected class DialogRequest
		{
			// Token: 0x040000F0 RID: 240
			public string QueryText;

			// Token: 0x040000F1 RID: 241
			public string OkayButtonText;

			// Token: 0x040000F2 RID: 242
			public string CancelButtonText;

			// Token: 0x040000F3 RID: 243
			public List<string> Options;

			// Token: 0x040000F4 RID: 244
			public Action<bool, int> Callback;
		}

		// Token: 0x0200005E RID: 94
		protected class LifecycleNotifier : MonoBehaviour
		{
			// Token: 0x0600018E RID: 398 RVA: 0x00008DD2 File Offset: 0x000071D2
			private void OnDestroy()
			{
				if (this.OnDestroyCallback != null)
				{
					this.OnDestroyCallback();
				}
			}

			// Token: 0x040000F5 RID: 245
			public Action OnDestroyCallback;
		}
	}
}
