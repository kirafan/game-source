﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000016 RID: 22
	public interface ISamsungAppsExtensions : IStoreExtension
	{
		// Token: 0x0600003F RID: 63
		void RestoreTransactions(Action<bool> callback);
	}
}
