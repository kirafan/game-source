﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000012 RID: 18
	public interface IGooglePlayConfiguration : IStoreConfiguration
	{
		// Token: 0x06000039 RID: 57
		void SetPublicKey(string key);
	}
}
