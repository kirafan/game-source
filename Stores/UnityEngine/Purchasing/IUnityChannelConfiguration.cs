﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000042 RID: 66
	public interface IUnityChannelConfiguration : IStoreConfiguration
	{
		// Token: 0x17000015 RID: 21
		// (get) Token: 0x060000E5 RID: 229
		// (set) Token: 0x060000E6 RID: 230
		bool fetchReceiptPayloadOnPurchase { get; set; }
	}
}
