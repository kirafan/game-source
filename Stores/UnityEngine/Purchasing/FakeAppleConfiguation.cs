﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200001D RID: 29
	internal class FakeAppleConfiguation : IAppleConfiguration, IStoreConfiguration
	{
		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600005C RID: 92 RVA: 0x00002F90 File Offset: 0x00001390
		public string appReceipt
		{
			get
			{
				return "This is a fake receipt. When running on an Apple store, a base64 encoded App Receipt would be returned";
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600005D RID: 93 RVA: 0x00002FAC File Offset: 0x000013AC
		public bool canMakePayments
		{
			get
			{
				return true;
			}
		}
	}
}
