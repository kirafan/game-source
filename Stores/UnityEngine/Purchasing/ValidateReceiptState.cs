﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000039 RID: 57
	public enum ValidateReceiptState
	{
		// Token: 0x04000053 RID: 83
		ValidateSucceed,
		// Token: 0x04000054 RID: 84
		ValidateFailed,
		// Token: 0x04000055 RID: 85
		NotKnown
	}
}
