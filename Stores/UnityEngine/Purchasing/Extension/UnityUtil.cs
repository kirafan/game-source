﻿using System;
using System.Collections;
using System.Collections.Generic;
using Uniject;

namespace UnityEngine.Purchasing.Extension
{
	// Token: 0x0200005F RID: 95
	[HideInInspector]
	[AddComponentMenu("")]
	internal class UnityUtil : MonoBehaviour, IUtil
	{
		// Token: 0x06000190 RID: 400 RVA: 0x00008E34 File Offset: 0x00007234
		public T[] GetAnyComponentsOfType<T>() where T : class
		{
			GameObject[] array = (GameObject[])Object.FindObjectsOfType(typeof(GameObject));
			List<T> list = new List<T>();
			foreach (GameObject gameObject in array)
			{
				foreach (MonoBehaviour monoBehaviour in gameObject.GetComponents<MonoBehaviour>())
				{
					if (monoBehaviour is T)
					{
						list.Add(monoBehaviour as T);
					}
				}
			}
			return list.ToArray();
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x06000191 RID: 401 RVA: 0x00008EDC File Offset: 0x000072DC
		public DateTime currentTime
		{
			get
			{
				return DateTime.Now;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000192 RID: 402 RVA: 0x00008EF8 File Offset: 0x000072F8
		public string persistentDataPath
		{
			get
			{
				return Application.persistentDataPath;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000193 RID: 403 RVA: 0x00008F14 File Offset: 0x00007314
		public RuntimePlatform platform
		{
			get
			{
				return Application.platform;
			}
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000194 RID: 404 RVA: 0x00008F30 File Offset: 0x00007330
		public bool isEditor
		{
			get
			{
				return Application.isEditor;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x06000195 RID: 405 RVA: 0x00008F4C File Offset: 0x0000734C
		public string deviceModel
		{
			get
			{
				return SystemInfo.deviceModel;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x06000196 RID: 406 RVA: 0x00008F68 File Offset: 0x00007368
		public string deviceName
		{
			get
			{
				return SystemInfo.deviceName;
			}
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x06000197 RID: 407 RVA: 0x00008F84 File Offset: 0x00007384
		public DeviceType deviceType
		{
			get
			{
				return SystemInfo.deviceType;
			}
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000198 RID: 408 RVA: 0x00008FA0 File Offset: 0x000073A0
		public string operatingSystem
		{
			get
			{
				return SystemInfo.operatingSystem;
			}
		}

		// Token: 0x06000199 RID: 409 RVA: 0x00008FBC File Offset: 0x000073BC
		object IUtil.InitiateCoroutine(IEnumerator start)
		{
			return base.StartCoroutine(start);
		}

		// Token: 0x0600019A RID: 410 RVA: 0x00008FD8 File Offset: 0x000073D8
		void IUtil.InitiateCoroutine(IEnumerator start, int delay)
		{
			this.DelayedCoroutine(start, delay);
		}

		// Token: 0x0600019B RID: 411 RVA: 0x00008FE4 File Offset: 0x000073E4
		public void RunOnMainThread(Action runnable)
		{
			object obj = UnityUtil.s_Callbacks;
			lock (obj)
			{
				UnityUtil.s_Callbacks.Add(runnable);
				UnityUtil.s_CallbacksPending = true;
			}
		}

		// Token: 0x0600019C RID: 412 RVA: 0x00009030 File Offset: 0x00007430
		public object GetWaitForSeconds(int seconds)
		{
			return new WaitForSeconds((float)seconds);
		}

		// Token: 0x0600019D RID: 413 RVA: 0x0000904C File Offset: 0x0000744C
		private void Start()
		{
			Object.DontDestroyOnLoad(base.gameObject);
		}

		// Token: 0x0600019E RID: 414 RVA: 0x0000905C File Offset: 0x0000745C
		public static T FindInstanceOfType<T>() where T : MonoBehaviour
		{
			return (T)((object)Object.FindObjectOfType(typeof(T)));
		}

		// Token: 0x0600019F RID: 415 RVA: 0x00009088 File Offset: 0x00007488
		public static T LoadResourceInstanceOfType<T>() where T : MonoBehaviour
		{
			return ((GameObject)Object.Instantiate(Resources.Load(typeof(T).ToString()))).GetComponent<T>();
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x000090C0 File Offset: 0x000074C0
		public static bool PcPlatform()
		{
			return UnityUtil.s_PcControlledPlatforms.Contains(Application.platform);
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x000090E4 File Offset: 0x000074E4
		public static void DebugLog(string message, params object[] args)
		{
			try
			{
				Debug.Log(string.Format("com.ballatergames.debug - {0}", string.Format(message, args)));
			}
			catch (ArgumentNullException message2)
			{
				Debug.Log(message2);
			}
			catch (FormatException message3)
			{
				Debug.Log(message3);
			}
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x00009148 File Offset: 0x00007548
		private IEnumerator DelayedCoroutine(IEnumerator coroutine, int delay)
		{
			yield return new WaitForSeconds((float)delay);
			base.StartCoroutine(coroutine);
			yield break;
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x00009178 File Offset: 0x00007578
		private void Update()
		{
			if (UnityUtil.s_CallbacksPending)
			{
				object obj = UnityUtil.s_Callbacks;
				Action[] array;
				lock (obj)
				{
					if (UnityUtil.s_Callbacks.Count == 0)
					{
						return;
					}
					array = new Action[UnityUtil.s_Callbacks.Count];
					UnityUtil.s_Callbacks.CopyTo(array);
					UnityUtil.s_Callbacks.Clear();
					UnityUtil.s_CallbacksPending = false;
				}
				foreach (Action action in array)
				{
					action();
				}
			}
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00009228 File Offset: 0x00007628
		public void AddPauseListener(Action<bool> runnable)
		{
			this.pauseListeners.Add(runnable);
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x00009238 File Offset: 0x00007638
		public void OnApplicationPause(bool paused)
		{
			foreach (Action<bool> action in this.pauseListeners)
			{
				action(paused);
			}
		}

		// Token: 0x040000F6 RID: 246
		private static List<Action> s_Callbacks = new List<Action>();

		// Token: 0x040000F7 RID: 247
		private static volatile bool s_CallbacksPending;

		// Token: 0x040000F8 RID: 248
		private static List<RuntimePlatform> s_PcControlledPlatforms = new List<RuntimePlatform>
		{
			RuntimePlatform.LinuxPlayer,
			RuntimePlatform.OSXDashboardPlayer,
			RuntimePlatform.OSXEditor,
			RuntimePlatform.OSXPlayer,
			RuntimePlatform.WindowsEditor,
			RuntimePlatform.WindowsPlayer
		};

		// Token: 0x040000F9 RID: 249
		private List<Action<bool>> pauseListeners = new List<Action<bool>>();
	}
}
