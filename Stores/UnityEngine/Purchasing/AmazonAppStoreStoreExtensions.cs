﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000A RID: 10
	public class AmazonAppStoreStoreExtensions : IAmazonExtensions, IAmazonConfiguration, IStoreExtension, IStoreConfiguration
	{
		// Token: 0x06000028 RID: 40 RVA: 0x00002859 File Offset: 0x00000C59
		public AmazonAppStoreStoreExtensions(AndroidJavaObject a)
		{
			this.android = a;
		}

		// Token: 0x06000029 RID: 41 RVA: 0x00002869 File Offset: 0x00000C69
		public void WriteSandboxJSON(HashSet<ProductDefinition> products)
		{
			this.android.Call("writeSandboxJSON", new object[]
			{
				JSONSerializer.SerializeProductDefs(products)
			});
		}

		// Token: 0x0600002A RID: 42 RVA: 0x0000288B File Offset: 0x00000C8B
		public void NotifyUnableToFulfillUnavailableProduct(string transactionID)
		{
			this.android.Call("notifyUnableToFulfillUnavailableProduct", new object[]
			{
				transactionID
			});
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600002B RID: 43 RVA: 0x000028A8 File Offset: 0x00000CA8
		public string amazonUserId
		{
			get
			{
				return this.android.Call<string>("getAmazonUserId", new object[0]);
			}
		}

		// Token: 0x04000007 RID: 7
		private AndroidJavaObject android;
	}
}
