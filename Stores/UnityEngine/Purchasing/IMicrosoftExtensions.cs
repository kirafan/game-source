﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000027 RID: 39
	public interface IMicrosoftExtensions : IStoreExtension
	{
		// Token: 0x06000075 RID: 117
		void RestoreTransactions();
	}
}
