﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000036 RID: 54
	public interface IMoolahConfiguration : IStoreConfiguration
	{
		// Token: 0x17000011 RID: 17
		// (get) Token: 0x060000A9 RID: 169
		// (set) Token: 0x060000AA RID: 170
		string appKey { get; set; }

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x060000AB RID: 171
		// (set) Token: 0x060000AC RID: 172
		string hashKey { get; set; }

		// Token: 0x060000AD RID: 173
		void SetMode(CloudMoolahMode mode);
	}
}
