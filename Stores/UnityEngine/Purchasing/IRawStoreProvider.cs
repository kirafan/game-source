﻿using System;
using Uniject;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200004D RID: 77
	internal interface IRawStoreProvider
	{
		// Token: 0x06000114 RID: 276
		INativeStore GetAndroidStore(IUnityCallback callback, AndroidStore store, IPurchasingBinder binder, IUtil util);

		// Token: 0x06000115 RID: 277
		INativeAppleStore GetStorekit(IUnityCallback callback);

		// Token: 0x06000116 RID: 278
		INativeTizenStore GetTizenStore(IUnityCallback callback, IPurchasingBinder binder);

		// Token: 0x06000117 RID: 279
		INativeFacebookStore GetFacebookStore();

		// Token: 0x06000118 RID: 280
		INativeFacebookStore GetFacebookStore(IUnityCallback callback, IPurchasingBinder binder);
	}
}
