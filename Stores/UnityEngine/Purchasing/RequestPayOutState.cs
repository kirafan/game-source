﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200003B RID: 59
	public enum RequestPayOutState
	{
		// Token: 0x0400005C RID: 92
		RequestPayOutSucceed,
		// Token: 0x0400005D RID: 93
		RequestPayOutNetworkError,
		// Token: 0x0400005E RID: 94
		RequestPayOutFailed,
		// Token: 0x0400005F RID: 95
		NotKnown
	}
}
