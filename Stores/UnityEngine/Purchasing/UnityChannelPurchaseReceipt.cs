﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000023 RID: 35
	[Serializable]
	public class UnityChannelPurchaseReceipt
	{
		// Token: 0x04000022 RID: 34
		public string storeSpecificId;

		// Token: 0x04000023 RID: 35
		public string transactionId;

		// Token: 0x04000024 RID: 36
		public string orderQueryToken;

		// Token: 0x04000025 RID: 37
		public string json;

		// Token: 0x04000026 RID: 38
		public string signature;

		// Token: 0x04000027 RID: 39
		public string error;
	}
}
