﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000041 RID: 65
	internal class PayMethod
	{
		// Token: 0x060000E3 RID: 227 RVA: 0x000061F4 File Offset: 0x000045F4
		public static void showPayWebView(string paymentURL, string authGlobal, string transactionId, string hashKey, string customID)
		{
			Debug.Log("PayWebView is open.");
			if (Application.platform == RuntimePlatform.Android)
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.cm.androidforunity.PaymentActivity"))
				{
					AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("instance", new object[0]);
					androidJavaObject.Call("JavaShowPayWebView", new object[]
					{
						paymentURL,
						authGlobal,
						transactionId,
						hashKey,
						customID
					});
				}
			}
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x00006284 File Offset: 0x00004684
		public static void showPaySuccess(string title, string msg)
		{
			if (Application.platform == RuntimePlatform.Android)
			{
				using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.cm.androidforunity.PaymentActivity"))
				{
					AndroidJavaObject androidJavaObject = androidJavaClass.CallStatic<AndroidJavaObject>("instance", new object[0]);
					androidJavaObject.Call("showPaySuccess", new object[]
					{
						title,
						msg
					});
				}
			}
		}
	}
}
