﻿using System;
using System.Collections.ObjectModel;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000008 RID: 8
	internal class NativeJSONStore : AbstractStore, IUnityCallback
	{
		// Token: 0x0600001A RID: 26 RVA: 0x000025E2 File Offset: 0x000009E2
		public void SetNativeStore(INativeStore native)
		{
			this.store = native;
		}

		// Token: 0x0600001B RID: 27 RVA: 0x000025EC File Offset: 0x000009EC
		public override void Initialize(IStoreCallback callback)
		{
			this.unity = callback;
		}

		// Token: 0x0600001C RID: 28 RVA: 0x000025F6 File Offset: 0x000009F6
		public override void RetrieveProducts(ReadOnlyCollection<ProductDefinition> products)
		{
			this.store.RetrieveProducts(JSONSerializer.SerializeProductDefs(products));
		}

		// Token: 0x0600001D RID: 29 RVA: 0x0000260A File Offset: 0x00000A0A
		public override void Purchase(ProductDefinition product, string developerPayload)
		{
			this.store.Purchase(JSONSerializer.SerializeProductDef(product), developerPayload);
		}

		// Token: 0x0600001E RID: 30 RVA: 0x00002620 File Offset: 0x00000A20
		public override void FinishTransaction(ProductDefinition product, string transactionId)
		{
			string productJSON = (product != null) ? JSONSerializer.SerializeProductDef(product) : null;
			this.store.FinishTransaction(productJSON, transactionId);
		}

		// Token: 0x0600001F RID: 31 RVA: 0x00002650 File Offset: 0x00000A50
		public void OnSetupFailed(string reason)
		{
			InitializationFailureReason reason2 = (InitializationFailureReason)Enum.Parse(typeof(InitializationFailureReason), reason, true);
			this.unity.OnSetupFailed(reason2);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x00002681 File Offset: 0x00000A81
		public virtual void OnProductsRetrieved(string json)
		{
			this.unity.OnProductsRetrieved(JSONSerializer.DeserializeProductDescriptions(json));
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002695 File Offset: 0x00000A95
		public void OnPurchaseSucceeded(string id, string receipt, string transactionID)
		{
			this.unity.OnPurchaseSucceeded(id, receipt, transactionID);
		}

		// Token: 0x06000022 RID: 34 RVA: 0x000026A6 File Offset: 0x00000AA6
		public void OnPurchaseFailed(string json)
		{
			this.unity.OnPurchaseFailed(JSONSerializer.DeserializeFailureReason(json));
		}

		// Token: 0x04000003 RID: 3
		protected IStoreCallback unity;

		// Token: 0x04000004 RID: 4
		private INativeStore store;
	}
}
