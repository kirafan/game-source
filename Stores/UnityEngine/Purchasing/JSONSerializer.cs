﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000006 RID: 6
	internal class JSONSerializer
	{
		// Token: 0x0600000D RID: 13 RVA: 0x000022A4 File Offset: 0x000006A4
		public static string SerializeProductDef(ProductDefinition product)
		{
			return MiniJson.JsonEncode(JSONSerializer.EncodeProductDef(product));
		}

		// Token: 0x0600000E RID: 14 RVA: 0x000022C4 File Offset: 0x000006C4
		public static string SerializeProductDefs(IEnumerable<ProductDefinition> products)
		{
			List<object> list = new List<object>();
			foreach (ProductDefinition product in products)
			{
				list.Add(JSONSerializer.EncodeProductDef(product));
			}
			return MiniJson.JsonEncode(list);
		}

		// Token: 0x0600000F RID: 15 RVA: 0x00002334 File Offset: 0x00000734
		public static List<ProductDescription> DeserializeProductDescriptions(string json)
		{
			List<object> list = (List<object>)MiniJson.JsonDecode(json);
			List<ProductDescription> list2 = new List<ProductDescription>();
			foreach (object obj in list)
			{
				Dictionary<string, object> dictionary = (Dictionary<string, object>)obj;
				ProductMetadata metadata = JSONSerializer.DeserializeMetadata((Dictionary<string, object>)dictionary["metadata"]);
				ProductDescription item = new ProductDescription((string)dictionary["storeSpecificId"], metadata, dictionary.TryGetString("receipt"), dictionary.TryGetString("transactionId"));
				list2.Add(item);
			}
			return list2;
		}

		// Token: 0x06000010 RID: 16 RVA: 0x000023F8 File Offset: 0x000007F8
		public static PurchaseFailureDescription DeserializeFailureReason(string json)
		{
			Dictionary<string, object> dictionary = (Dictionary<string, object>)MiniJson.JsonDecode(json);
			PurchaseFailureReason reason = PurchaseFailureReason.Unknown;
			if (Enum.IsDefined(typeof(PurchaseFailureReason), (string)dictionary["reason"]))
			{
				reason = (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), (string)dictionary["reason"]);
			}
			return new PurchaseFailureDescription((string)dictionary["productId"], reason, dictionary.TryGetString("message"));
		}

		// Token: 0x06000011 RID: 17 RVA: 0x00002488 File Offset: 0x00000888
		private static ProductMetadata DeserializeMetadata(Dictionary<string, object> data)
		{
			decimal localizedPrice = 0.0m;
			try
			{
				localizedPrice = Convert.ToDecimal(data["localizedPrice"]);
			}
			catch
			{
				localizedPrice = 0.0m;
			}
			return new ProductMetadata(data.TryGetString("localizedPriceString"), data.TryGetString("localizedTitle"), data.TryGetString("localizedDescription"), data.TryGetString("isoCurrencyCode"), localizedPrice);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x00002518 File Offset: 0x00000918
		private static Dictionary<string, object> EncodeProductDef(ProductDefinition product)
		{
			return new Dictionary<string, object>
			{
				{
					"id",
					product.id
				},
				{
					"storeSpecificId",
					product.storeSpecificId
				},
				{
					"type",
					product.type.ToString()
				}
			};
		}
	}
}
