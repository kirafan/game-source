﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000005 RID: 5
	internal static class SerializationExtensions
	{
		// Token: 0x0600000B RID: 11 RVA: 0x00002258 File Offset: 0x00000658
		public static string TryGetString(this Dictionary<string, object> dic, string key)
		{
			if (dic.ContainsKey(key))
			{
				if (dic[key] != null)
				{
					return dic[key].ToString();
				}
			}
			return null;
		}
	}
}
