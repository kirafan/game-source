﻿using System;
using System.Collections.Generic;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200000C RID: 12
	public class FakeAmazonExtensions : IAmazonExtensions, IAmazonConfiguration, IStoreExtension, IStoreConfiguration
	{
		// Token: 0x0600002E RID: 46 RVA: 0x000028E3 File Offset: 0x00000CE3
		public void WriteSandboxJSON(HashSet<ProductDefinition> products)
		{
		}

		// Token: 0x0600002F RID: 47 RVA: 0x000028E6 File Offset: 0x00000CE6
		public void NotifyUnableToFulfillUnavailableProduct(string transactionID)
		{
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000030 RID: 48 RVA: 0x000028EC File Offset: 0x00000CEC
		public string amazonUserId
		{
			get
			{
				return "fakeid";
			}
		}
	}
}
