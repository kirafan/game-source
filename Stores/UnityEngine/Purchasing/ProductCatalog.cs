﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000055 RID: 85
	[Serializable]
	public class ProductCatalog
	{
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000144 RID: 324 RVA: 0x00007A24 File Offset: 0x00005E24
		public ICollection<ProductCatalogItem> allProducts
		{
			get
			{
				return this.products;
			}
		}

		// Token: 0x06000145 RID: 325 RVA: 0x00007A3F File Offset: 0x00005E3F
		public void Add(ProductCatalogItem item)
		{
			this.products.Add(item);
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00007A4E File Offset: 0x00005E4E
		public void Remove(ProductCatalogItem item)
		{
			this.products.Remove(item);
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00007A60 File Offset: 0x00005E60
		public static string Serialize(ProductCatalog catalog)
		{
			return JsonUtility.ToJson(catalog);
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00007A7C File Offset: 0x00005E7C
		public static ProductCatalog Deserialize(string catalogJSON)
		{
			return JsonUtility.FromJson<ProductCatalog>(catalogJSON);
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00007A98 File Offset: 0x00005E98
		public static ProductCatalog FromTextAsset(TextAsset asset)
		{
			return ProductCatalog.Deserialize(asset.text);
		}

		// Token: 0x0600014A RID: 330 RVA: 0x00007AB8 File Offset: 0x00005EB8
		public static ProductCatalog LoadDefaultCatalog()
		{
			TextAsset textAsset = Resources.Load("IAPProductCatalog") as TextAsset;
			ProductCatalog result;
			if (textAsset != null)
			{
				result = ProductCatalog.FromTextAsset(textAsset);
			}
			else
			{
				result = new ProductCatalog();
			}
			return result;
		}

		// Token: 0x040000D0 RID: 208
		public string appleSKU;

		// Token: 0x040000D1 RID: 209
		[SerializeField]
		private List<ProductCatalogItem> products = new List<ProductCatalogItem>();

		// Token: 0x040000D2 RID: 210
		public const string kCatalogPath = "Assets/Plugins/UnityPurchasing/Resources/IAPProductCatalog.json";
	}
}
