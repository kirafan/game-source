﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000045 RID: 69
	public interface IUnityChannelExtensions : IStoreExtension
	{
		// Token: 0x060000EE RID: 238
		void ConfirmPurchase(string transactionId, Action<bool, string, string> callback);

		// Token: 0x060000EF RID: 239
		void ValidateReceipt(string transactionId, Action<bool, string, string> callback);

		// Token: 0x060000F0 RID: 240
		string GetLastPurchaseError();
	}
}
