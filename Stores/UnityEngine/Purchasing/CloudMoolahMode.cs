﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000035 RID: 53
	public enum CloudMoolahMode
	{
		// Token: 0x04000042 RID: 66
		Production,
		// Token: 0x04000043 RID: 67
		AlwaysSucceed,
		// Token: 0x04000044 RID: 68
		AlwaysFailed
	}
}
