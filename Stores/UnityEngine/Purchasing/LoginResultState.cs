﻿using System;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000037 RID: 55
	public enum LoginResultState
	{
		// Token: 0x04000046 RID: 70
		LoginSucceed,
		// Token: 0x04000047 RID: 71
		UserNotExists,
		// Token: 0x04000048 RID: 72
		PasswordError,
		// Token: 0x04000049 RID: 73
		UserOrPasswordEmpty,
		// Token: 0x0400004A RID: 74
		LoginCallBackIsNull,
		// Token: 0x0400004B RID: 75
		NetworkError,
		// Token: 0x0400004C RID: 76
		NotKnown
	}
}
