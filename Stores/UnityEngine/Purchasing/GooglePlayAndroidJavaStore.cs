﻿using System;
using UnityEngine.VR;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000010 RID: 16
	internal class GooglePlayAndroidJavaStore : AndroidJavaStore
	{
		// Token: 0x06000035 RID: 53 RVA: 0x00002910 File Offset: 0x00000D10
		public GooglePlayAndroidJavaStore(AndroidJavaObject store) : base(store)
		{
			string text = "";
			if (Enum.IsDefined(typeof(PurchaseFailureReason), "DuplicateTransaction"))
			{
				text += "supportsPurchaseFailureReasonDuplicateTransaction";
			}
			base.GetStore().Call("SetFeatures", new object[]
			{
				text
			});
		}

		// Token: 0x06000036 RID: 54 RVA: 0x0000296C File Offset: 0x00000D6C
		public override void Purchase(string productJSON, string developerPayload)
		{
			base.GetStore().Call("SetUnityVrEnabled", new object[]
			{
				VRSettings.enabled
			});
			base.Purchase(productJSON, developerPayload);
		}
	}
}
