﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Uniject;
using UnityEngine.Purchasing.Default;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x02000028 RID: 40
	internal class WinRTStore : AbstractStore, IWindowsIAPCallback, IMicrosoftExtensions, IStoreExtension
	{
		// Token: 0x06000076 RID: 118 RVA: 0x00003062 File Offset: 0x00001462
		public WinRTStore(IWindowsIAP win8, IUtil util, ILogger logger)
		{
			this.win8 = win8;
			this.util = util;
			this.logger = logger;
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00003087 File Offset: 0x00001487
		public void SetWindowsIAP(IWindowsIAP iap)
		{
			this.win8 = iap;
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00003091 File Offset: 0x00001491
		public override void Initialize(IStoreCallback biller)
		{
			this.callback = biller;
		}

		// Token: 0x06000079 RID: 121 RVA: 0x0000309C File Offset: 0x0000149C
		public override void RetrieveProducts(ReadOnlyCollection<ProductDefinition> productDefs)
		{
			IEnumerable<WinProductDescription> source = from def in productDefs
			where def.type != ProductType.Subscription
			select new WinProductDescription(def.storeSpecificId, "$0.01", "Fake title - " + def.storeSpecificId, "Fake description - " + def.storeSpecificId, "USD", 0.01m, null, null, def.type == ProductType.Consumable);
			this.win8.BuildDummyProducts(source.ToList<WinProductDescription>());
			this.init(0);
		}

		// Token: 0x0600007A RID: 122 RVA: 0x00003108 File Offset: 0x00001508
		public override void FinishTransaction(ProductDefinition product, string transactionId)
		{
			this.win8.FinaliseTransaction(transactionId);
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00003117 File Offset: 0x00001517
		private void init(int delay)
		{
			this.win8.Initialize(this);
			this.win8.RetrieveProducts(true);
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00003132 File Offset: 0x00001532
		public override void Purchase(ProductDefinition product, string developerPayload)
		{
			this.win8.Purchase(product.storeSpecificId);
		}

		// Token: 0x0600007D RID: 125 RVA: 0x00003146 File Offset: 0x00001546
		public void restoreTransactions(bool pausing)
		{
			if (!pausing)
			{
				if (this.m_CanReceivePurchases)
				{
					this.win8.RetrieveProducts(false);
				}
			}
		}

		// Token: 0x0600007E RID: 126 RVA: 0x0000316A File Offset: 0x0000156A
		public void RestoreTransactions()
		{
			this.logger.Log("Explicit RestoreTransactions()");
			this.win8.RetrieveProducts(false);
			this.m_CanReceivePurchases = true;
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00003190 File Offset: 0x00001590
		public void logError(string error)
		{
			this.logger.LogError("Unity Purchasing", error);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x000031A4 File Offset: 0x000015A4
		public void OnProductListReceived(WinProductDescription[] winProducts)
		{
			this.util.RunOnMainThread(delegate
			{
				IEnumerable<ProductDescription> source = from product in winProducts
				select new
				{
					product = product,
					metadata = new ProductMetadata(product.price, product.title, product.description, product.ISOCurrencyCode, product.priceDecimal)
				} into <>__TranspIdent0
				select new ProductDescription(<>__TranspIdent0.product.platformSpecificID, <>__TranspIdent0.metadata, <>__TranspIdent0.product.receipt, <>__TranspIdent0.product.transactionID);
				this.callback.OnProductsRetrieved(source.ToList<ProductDescription>());
			});
		}

		// Token: 0x06000081 RID: 129 RVA: 0x000031E0 File Offset: 0x000015E0
		public void log(string message)
		{
			this.util.RunOnMainThread(delegate
			{
				this.logger.Log(message);
			});
		}

		// Token: 0x06000082 RID: 130 RVA: 0x0000321C File Offset: 0x0000161C
		public void OnPurchaseFailed(string productId, string error)
		{
			this.util.RunOnMainThread(delegate
			{
				this.logger.LogFormat(LogType.Error, "Purchase failed: {0}, {1}", new object[]
				{
					productId,
					error
				});
				this.callback.OnPurchaseFailed(new PurchaseFailureDescription(productId, (PurchaseFailureReason)Enum.Parse(typeof(PurchaseFailureReason), "Unknown"), error));
			});
		}

		// Token: 0x06000083 RID: 131 RVA: 0x0000325C File Offset: 0x0000165C
		public void OnPurchaseSucceeded(string productId, string receipt, string tranId)
		{
			this.util.RunOnMainThread(delegate
			{
				this.logger.Log("PURCHASE SUCCEEDED!:{0}", WinRTStore.count++);
				this.m_CanReceivePurchases = true;
				this.callback.OnPurchaseSucceeded(productId, receipt, tranId);
			});
		}

		// Token: 0x06000084 RID: 132 RVA: 0x000032A4 File Offset: 0x000016A4
		public void OnProductListError(string message)
		{
			this.util.RunOnMainThread(delegate
			{
				if (message.Contains("801900CC"))
				{
					this.callback.OnSetupFailed(InitializationFailureReason.AppNotKnown);
				}
				else
				{
					this.logError("Unable to retrieve product listings. UnityIAP will automatically retry...");
					this.logError(message);
					this.init(3000);
				}
			});
		}

		// Token: 0x04000029 RID: 41
		private IWindowsIAP win8;

		// Token: 0x0400002A RID: 42
		private IStoreCallback callback;

		// Token: 0x0400002B RID: 43
		private IUtil util;

		// Token: 0x0400002C RID: 44
		private ILogger logger;

		// Token: 0x0400002D RID: 45
		private bool m_CanReceivePurchases = false;

		// Token: 0x0400002E RID: 46
		private static int count;
	}
}
