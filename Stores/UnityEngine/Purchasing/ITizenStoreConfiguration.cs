﻿using System;
using UnityEngine.Purchasing.Extension;

namespace UnityEngine.Purchasing
{
	// Token: 0x0200002B RID: 43
	public interface ITizenStoreConfiguration : IStoreConfiguration
	{
		// Token: 0x06000089 RID: 137
		void SetGroupId(string group);
	}
}
