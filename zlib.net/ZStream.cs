﻿using System;

namespace zlib
{
	// Token: 0x0200000F RID: 15
	public sealed class ZStream
	{
		// Token: 0x06000087 RID: 135 RVA: 0x0000AF84 File Offset: 0x00009F84
		public int inflateInit()
		{
			return this.inflateInit(ZStream.DEF_WBITS);
		}

		// Token: 0x06000088 RID: 136 RVA: 0x0000AF9C File Offset: 0x00009F9C
		public int inflateInit(int w)
		{
			this.istate = new Inflate();
			return this.istate.inflateInit(this, w);
		}

		// Token: 0x06000089 RID: 137 RVA: 0x0000AFC4 File Offset: 0x00009FC4
		public int inflate(int f)
		{
			if (this.istate == null)
			{
				return -2;
			}
			return this.istate.inflate(this, f);
		}

		// Token: 0x0600008A RID: 138 RVA: 0x0000AFEC File Offset: 0x00009FEC
		public int inflateEnd()
		{
			if (this.istate == null)
			{
				return -2;
			}
			int result = this.istate.inflateEnd(this);
			this.istate = null;
			return result;
		}

		// Token: 0x0600008B RID: 139 RVA: 0x0000B01C File Offset: 0x0000A01C
		public int inflateSync()
		{
			if (this.istate == null)
			{
				return -2;
			}
			return this.istate.inflateSync(this);
		}

		// Token: 0x0600008C RID: 140 RVA: 0x0000B040 File Offset: 0x0000A040
		public int inflateSetDictionary(byte[] dictionary, int dictLength)
		{
			if (this.istate == null)
			{
				return -2;
			}
			return this.istate.inflateSetDictionary(this, dictionary, dictLength);
		}

		// Token: 0x0600008D RID: 141 RVA: 0x0000B068 File Offset: 0x0000A068
		public int deflateInit(int level)
		{
			return this.deflateInit(level, 15);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x0000B080 File Offset: 0x0000A080
		public int deflateInit(int level, int bits)
		{
			this.dstate = new Deflate();
			return this.dstate.deflateInit(this, level, bits);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x0000B0A8 File Offset: 0x0000A0A8
		public int deflate(int flush)
		{
			if (this.dstate == null)
			{
				return -2;
			}
			return this.dstate.deflate(this, flush);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x0000B0D0 File Offset: 0x0000A0D0
		public int deflateEnd()
		{
			if (this.dstate == null)
			{
				return -2;
			}
			int result = this.dstate.deflateEnd();
			this.dstate = null;
			return result;
		}

		// Token: 0x06000091 RID: 145 RVA: 0x0000B0FC File Offset: 0x0000A0FC
		public int deflateParams(int level, int strategy)
		{
			if (this.dstate == null)
			{
				return -2;
			}
			return this.dstate.deflateParams(this, level, strategy);
		}

		// Token: 0x06000092 RID: 146 RVA: 0x0000B124 File Offset: 0x0000A124
		public int deflateSetDictionary(byte[] dictionary, int dictLength)
		{
			if (this.dstate == null)
			{
				return -2;
			}
			return this.dstate.deflateSetDictionary(this, dictionary, dictLength);
		}

		// Token: 0x06000093 RID: 147 RVA: 0x0000B14C File Offset: 0x0000A14C
		internal void flush_pending()
		{
			int pending = this.dstate.pending;
			if (pending > this.avail_out)
			{
				pending = this.avail_out;
			}
			if (pending == 0)
			{
				return;
			}
			if (this.dstate.pending_buf.Length <= this.dstate.pending_out || this.next_out.Length <= this.next_out_index || this.dstate.pending_buf.Length < this.dstate.pending_out + pending || this.next_out.Length < this.next_out_index + pending)
			{
				Console.Out.WriteLine(string.Concat(new object[]
				{
					this.dstate.pending_buf.Length,
					", ",
					this.dstate.pending_out,
					", ",
					this.next_out.Length,
					", ",
					this.next_out_index,
					", ",
					pending
				}));
				Console.Out.WriteLine("avail_out=" + this.avail_out);
			}
			Array.Copy(this.dstate.pending_buf, this.dstate.pending_out, this.next_out, this.next_out_index, pending);
			this.next_out_index += pending;
			this.dstate.pending_out += pending;
			this.total_out += (long)pending;
			this.avail_out -= pending;
			this.dstate.pending -= pending;
			if (this.dstate.pending == 0)
			{
				this.dstate.pending_out = 0;
			}
		}

		// Token: 0x06000094 RID: 148 RVA: 0x0000B30C File Offset: 0x0000A30C
		internal int read_buf(byte[] buf, int start, int size)
		{
			int num = this.avail_in;
			if (num > size)
			{
				num = size;
			}
			if (num == 0)
			{
				return 0;
			}
			this.avail_in -= num;
			if (this.dstate.noheader == 0)
			{
				this.adler = this._adler.adler32(this.adler, this.next_in, this.next_in_index, num);
			}
			Array.Copy(this.next_in, this.next_in_index, buf, start, num);
			this.next_in_index += num;
			this.total_in += (long)num;
			return num;
		}

		// Token: 0x06000095 RID: 149 RVA: 0x0000B39C File Offset: 0x0000A39C
		public void free()
		{
			this.next_in = null;
			this.next_out = null;
			this.msg = null;
			this._adler = null;
		}

		// Token: 0x0400014D RID: 333
		private const int MAX_WBITS = 15;

		// Token: 0x0400014E RID: 334
		private const int Z_NO_FLUSH = 0;

		// Token: 0x0400014F RID: 335
		private const int Z_PARTIAL_FLUSH = 1;

		// Token: 0x04000150 RID: 336
		private const int Z_SYNC_FLUSH = 2;

		// Token: 0x04000151 RID: 337
		private const int Z_FULL_FLUSH = 3;

		// Token: 0x04000152 RID: 338
		private const int Z_FINISH = 4;

		// Token: 0x04000153 RID: 339
		private const int MAX_MEM_LEVEL = 9;

		// Token: 0x04000154 RID: 340
		private const int Z_OK = 0;

		// Token: 0x04000155 RID: 341
		private const int Z_STREAM_END = 1;

		// Token: 0x04000156 RID: 342
		private const int Z_NEED_DICT = 2;

		// Token: 0x04000157 RID: 343
		private const int Z_ERRNO = -1;

		// Token: 0x04000158 RID: 344
		private const int Z_STREAM_ERROR = -2;

		// Token: 0x04000159 RID: 345
		private const int Z_DATA_ERROR = -3;

		// Token: 0x0400015A RID: 346
		private const int Z_MEM_ERROR = -4;

		// Token: 0x0400015B RID: 347
		private const int Z_BUF_ERROR = -5;

		// Token: 0x0400015C RID: 348
		private const int Z_VERSION_ERROR = -6;

		// Token: 0x0400015D RID: 349
		private static readonly int DEF_WBITS = 15;

		// Token: 0x0400015E RID: 350
		public byte[] next_in;

		// Token: 0x0400015F RID: 351
		public int next_in_index;

		// Token: 0x04000160 RID: 352
		public int avail_in;

		// Token: 0x04000161 RID: 353
		public long total_in;

		// Token: 0x04000162 RID: 354
		public byte[] next_out;

		// Token: 0x04000163 RID: 355
		public int next_out_index;

		// Token: 0x04000164 RID: 356
		public int avail_out;

		// Token: 0x04000165 RID: 357
		public long total_out;

		// Token: 0x04000166 RID: 358
		public string msg;

		// Token: 0x04000167 RID: 359
		internal Deflate dstate;

		// Token: 0x04000168 RID: 360
		internal Inflate istate;

		// Token: 0x04000169 RID: 361
		internal int data_type;

		// Token: 0x0400016A RID: 362
		public long adler;

		// Token: 0x0400016B RID: 363
		internal Adler32 _adler = new Adler32();
	}
}
