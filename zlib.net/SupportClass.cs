﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace zlib
{
	// Token: 0x0200000A RID: 10
	public class SupportClass
	{
		// Token: 0x0600004B RID: 75 RVA: 0x00009980 File Offset: 0x00008980
		public static long Identity(long literal)
		{
			return literal;
		}

		// Token: 0x0600004C RID: 76 RVA: 0x00009990 File Offset: 0x00008990
		public static ulong Identity(ulong literal)
		{
			return literal;
		}

		// Token: 0x0600004D RID: 77 RVA: 0x000099A0 File Offset: 0x000089A0
		public static float Identity(float literal)
		{
			return literal;
		}

		// Token: 0x0600004E RID: 78 RVA: 0x000099B0 File Offset: 0x000089B0
		public static double Identity(double literal)
		{
			return literal;
		}

		// Token: 0x0600004F RID: 79 RVA: 0x000099C0 File Offset: 0x000089C0
		public static int URShift(int number, int bits)
		{
			if (number >= 0)
			{
				return number >> bits;
			}
			return (number >> bits) + (2 << ~bits);
		}

		// Token: 0x06000050 RID: 80 RVA: 0x000099E8 File Offset: 0x000089E8
		public static int URShift(int number, long bits)
		{
			return SupportClass.URShift(number, (int)bits);
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00009A00 File Offset: 0x00008A00
		public static long URShift(long number, int bits)
		{
			if (number >= 0L)
			{
				return number >> bits;
			}
			return (number >> bits) + (2L << ~bits);
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00009A28 File Offset: 0x00008A28
		public static long URShift(long number, long bits)
		{
			return SupportClass.URShift(number, (int)bits);
		}

		// Token: 0x06000053 RID: 83 RVA: 0x00009A40 File Offset: 0x00008A40
		public static int ReadInput(Stream sourceStream, byte[] target, int start, int count)
		{
			if (target.Length == 0)
			{
				return 0;
			}
			byte[] array = new byte[target.Length];
			int num = sourceStream.Read(array, start, count);
			if (num == 0)
			{
				return -1;
			}
			for (int i = start; i < start + num; i++)
			{
				target[i] = array[i];
			}
			return num;
		}

		// Token: 0x06000054 RID: 84 RVA: 0x00009A84 File Offset: 0x00008A84
		public static int ReadInput(TextReader sourceTextReader, byte[] target, int start, int count)
		{
			if (target.Length == 0)
			{
				return 0;
			}
			char[] array = new char[target.Length];
			int num = sourceTextReader.Read(array, start, count);
			if (num == 0)
			{
				return -1;
			}
			for (int i = start; i < start + num; i++)
			{
				target[i] = (byte)array[i];
			}
			return num;
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00009AC8 File Offset: 0x00008AC8
		public static byte[] ToByteArray(string sourceString)
		{
			return Encoding.UTF8.GetBytes(sourceString);
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00009AE0 File Offset: 0x00008AE0
		public static char[] ToCharArray(byte[] byteArray)
		{
			return Encoding.UTF8.GetChars(byteArray);
		}

		// Token: 0x06000057 RID: 87 RVA: 0x00009AF8 File Offset: 0x00008AF8
		public static void Serialize(Stream stream, object objectToSend)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(stream, objectToSend);
		}

		// Token: 0x06000058 RID: 88 RVA: 0x00009B14 File Offset: 0x00008B14
		public static void Serialize(BinaryWriter binaryWriter, object objectToSend)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			binaryFormatter.Serialize(binaryWriter.BaseStream, objectToSend);
		}

		// Token: 0x06000059 RID: 89 RVA: 0x00009B34 File Offset: 0x00008B34
		public static object Deserialize(BinaryReader binaryReader)
		{
			BinaryFormatter binaryFormatter = new BinaryFormatter();
			return binaryFormatter.Deserialize(binaryReader.BaseStream);
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00009B54 File Offset: 0x00008B54
		public static void WriteStackTrace(Exception throwable, TextWriter stream)
		{
			stream.Write(throwable.StackTrace);
			stream.Flush();
		}
	}
}
