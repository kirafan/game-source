﻿using System;
using System.IO;

namespace zlib
{
	// Token: 0x02000010 RID: 16
	[Serializable]
	public class ZStreamException : IOException
	{
		// Token: 0x06000098 RID: 152 RVA: 0x0000B3FC File Offset: 0x0000A3FC
		public ZStreamException()
		{
		}

		// Token: 0x06000099 RID: 153 RVA: 0x0000B410 File Offset: 0x0000A410
		public ZStreamException(string s) : base(s)
		{
		}
	}
}
