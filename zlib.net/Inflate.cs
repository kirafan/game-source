﻿using System;

namespace zlib
{
	// Token: 0x02000007 RID: 7
	internal sealed class Inflate
	{
		// Token: 0x0600003A RID: 58 RVA: 0x000068A0 File Offset: 0x000058A0
		internal int inflateReset(ZStream z)
		{
			if (z == null || z.istate == null)
			{
				return -2;
			}
			z.total_in = (z.total_out = 0L);
			z.msg = null;
			z.istate.mode = ((z.istate.nowrap != 0) ? 7 : 0);
			z.istate.blocks.reset(z, null);
			return 0;
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00006904 File Offset: 0x00005904
		internal int inflateEnd(ZStream z)
		{
			if (this.blocks != null)
			{
				this.blocks.free(z);
			}
			this.blocks = null;
			return 0;
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00006930 File Offset: 0x00005930
		internal int inflateInit(ZStream z, int w)
		{
			z.msg = null;
			this.blocks = null;
			this.nowrap = 0;
			if (w < 0)
			{
				w = -w;
				this.nowrap = 1;
			}
			if (w < 8 || w > 15)
			{
				this.inflateEnd(z);
				return -2;
			}
			this.wbits = w;
			z.istate.blocks = new InfBlocks(z, (z.istate.nowrap != 0) ? null : this, 1 << w);
			this.inflateReset(z);
			return 0;
		}

		// Token: 0x0600003D RID: 61 RVA: 0x000069B0 File Offset: 0x000059B0
		internal int inflate(ZStream z, int f)
		{
			if (z == null || z.istate == null || z.next_in == null)
			{
				return -2;
			}
			f = ((f == 4) ? -5 : 0);
			int num = -5;
			for (;;)
			{
				switch (z.istate.mode)
				{
				case 0:
					if (z.avail_in == 0)
					{
						return num;
					}
					num = f;
					z.avail_in--;
					z.total_in += 1L;
					if (((z.istate.method = (int)z.next_in[z.next_in_index++]) & 15) != 8)
					{
						z.istate.mode = 13;
						z.msg = "unknown compression method";
						z.istate.marker = 5;
						continue;
					}
					if ((z.istate.method >> 4) + 8 > z.istate.wbits)
					{
						z.istate.mode = 13;
						z.msg = "invalid window size";
						z.istate.marker = 5;
						continue;
					}
					z.istate.mode = 1;
					goto IL_142;
				case 1:
					goto IL_142;
				case 2:
					goto IL_1EA;
				case 3:
					goto IL_252;
				case 4:
					goto IL_2C2;
				case 5:
					goto IL_331;
				case 6:
					goto IL_3AB;
				case 7:
					num = z.istate.blocks.proc(z, num);
					if (num == -3)
					{
						z.istate.mode = 13;
						z.istate.marker = 0;
						continue;
					}
					if (num == 0)
					{
						num = f;
					}
					if (num != 1)
					{
						return num;
					}
					num = f;
					z.istate.blocks.reset(z, z.istate.was);
					if (z.istate.nowrap != 0)
					{
						z.istate.mode = 12;
						continue;
					}
					z.istate.mode = 8;
					goto IL_45C;
				case 8:
					goto IL_45C;
				case 9:
					goto IL_4C5;
				case 10:
					goto IL_536;
				case 11:
					goto IL_5A6;
				case 12:
					return 1;
				case 13:
					return -3;
				}
				break;
				IL_142:
				if (z.avail_in == 0)
				{
					return num;
				}
				num = f;
				z.avail_in--;
				z.total_in += 1L;
				int num2 = (int)(z.next_in[z.next_in_index++] & byte.MaxValue);
				if (((z.istate.method << 8) + num2) % 31 != 0)
				{
					z.istate.mode = 13;
					z.msg = "incorrect header check";
					z.istate.marker = 5;
					continue;
				}
				if ((num2 & 32) == 0)
				{
					z.istate.mode = 7;
					continue;
				}
				goto IL_1DE;
				IL_5A6:
				if (z.avail_in == 0)
				{
					return num;
				}
				num = f;
				z.avail_in--;
				z.total_in += 1L;
				z.istate.need += (long)((ulong)z.next_in[z.next_in_index++] & 255UL);
				if ((int)z.istate.was[0] != (int)z.istate.need)
				{
					z.istate.mode = 13;
					z.msg = "incorrect data check";
					z.istate.marker = 5;
					continue;
				}
				goto IL_646;
				IL_536:
				if (z.avail_in == 0)
				{
					return num;
				}
				num = f;
				z.avail_in--;
				z.total_in += 1L;
				z.istate.need += ((long)((long)(z.next_in[z.next_in_index++] & byte.MaxValue) << 8) & 65280L);
				z.istate.mode = 11;
				goto IL_5A6;
				IL_4C5:
				if (z.avail_in == 0)
				{
					return num;
				}
				num = f;
				z.avail_in--;
				z.total_in += 1L;
				z.istate.need += ((long)((long)(z.next_in[z.next_in_index++] & byte.MaxValue) << 16) & 16711680L);
				z.istate.mode = 10;
				goto IL_536;
				IL_45C:
				if (z.avail_in == 0)
				{
					return num;
				}
				num = f;
				z.avail_in--;
				z.total_in += 1L;
				z.istate.need = (long)((int)(z.next_in[z.next_in_index++] & byte.MaxValue) << 24 & -16777216);
				z.istate.mode = 9;
				goto IL_4C5;
			}
			return -2;
			IL_1DE:
			z.istate.mode = 2;
			IL_1EA:
			if (z.avail_in == 0)
			{
				return num;
			}
			num = f;
			z.avail_in--;
			z.total_in += 1L;
			z.istate.need = (long)((int)(z.next_in[z.next_in_index++] & byte.MaxValue) << 24 & -16777216);
			z.istate.mode = 3;
			IL_252:
			if (z.avail_in == 0)
			{
				return num;
			}
			num = f;
			z.avail_in--;
			z.total_in += 1L;
			z.istate.need += ((long)((long)(z.next_in[z.next_in_index++] & byte.MaxValue) << 16) & 16711680L);
			z.istate.mode = 4;
			IL_2C2:
			if (z.avail_in == 0)
			{
				return num;
			}
			num = f;
			z.avail_in--;
			z.total_in += 1L;
			z.istate.need += ((long)((long)(z.next_in[z.next_in_index++] & byte.MaxValue) << 8) & 65280L);
			z.istate.mode = 5;
			IL_331:
			if (z.avail_in == 0)
			{
				return num;
			}
			z.avail_in--;
			z.total_in += 1L;
			z.istate.need += (long)((ulong)z.next_in[z.next_in_index++] & 255UL);
			z.adler = z.istate.need;
			z.istate.mode = 6;
			return 2;
			IL_3AB:
			z.istate.mode = 13;
			z.msg = "need dictionary";
			z.istate.marker = 0;
			return -2;
			IL_646:
			z.istate.mode = 12;
			return 1;
		}

		// Token: 0x0600003E RID: 62 RVA: 0x00007018 File Offset: 0x00006018
		internal int inflateSetDictionary(ZStream z, byte[] dictionary, int dictLength)
		{
			int start = 0;
			int num = dictLength;
			if (z == null || z.istate == null || z.istate.mode != 6)
			{
				return -2;
			}
			if (z._adler.adler32(1L, dictionary, 0, dictLength) != z.adler)
			{
				return -3;
			}
			z.adler = z._adler.adler32(0L, null, 0, 0);
			if (num >= 1 << z.istate.wbits)
			{
				num = (1 << z.istate.wbits) - 1;
				start = dictLength - num;
			}
			z.istate.blocks.set_dictionary(dictionary, start, num);
			z.istate.mode = 7;
			return 0;
		}

		// Token: 0x0600003F RID: 63 RVA: 0x000070C0 File Offset: 0x000060C0
		internal int inflateSync(ZStream z)
		{
			if (z == null || z.istate == null)
			{
				return -2;
			}
			if (z.istate.mode != 13)
			{
				z.istate.mode = 13;
				z.istate.marker = 0;
			}
			int num;
			if ((num = z.avail_in) == 0)
			{
				return -5;
			}
			int num2 = z.next_in_index;
			int num3 = z.istate.marker;
			while (num != 0 && num3 < 4)
			{
				if (z.next_in[num2] == Inflate.mark[num3])
				{
					num3++;
				}
				else if (z.next_in[num2] != 0)
				{
					num3 = 0;
				}
				else
				{
					num3 = 4 - num3;
				}
				num2++;
				num--;
			}
			z.total_in += (long)(num2 - z.next_in_index);
			z.next_in_index = num2;
			z.avail_in = num;
			z.istate.marker = num3;
			if (num3 != 4)
			{
				return -3;
			}
			long total_in = z.total_in;
			long total_out = z.total_out;
			this.inflateReset(z);
			z.total_in = total_in;
			z.total_out = total_out;
			z.istate.mode = 7;
			return 0;
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000071C8 File Offset: 0x000061C8
		internal int inflateSyncPoint(ZStream z)
		{
			if (z == null || z.istate == null || z.istate.blocks == null)
			{
				return -2;
			}
			return z.istate.blocks.sync_point();
		}

		// Token: 0x040000C3 RID: 195
		private const int MAX_WBITS = 15;

		// Token: 0x040000C4 RID: 196
		private const int PRESET_DICT = 32;

		// Token: 0x040000C5 RID: 197
		internal const int Z_NO_FLUSH = 0;

		// Token: 0x040000C6 RID: 198
		internal const int Z_PARTIAL_FLUSH = 1;

		// Token: 0x040000C7 RID: 199
		internal const int Z_SYNC_FLUSH = 2;

		// Token: 0x040000C8 RID: 200
		internal const int Z_FULL_FLUSH = 3;

		// Token: 0x040000C9 RID: 201
		internal const int Z_FINISH = 4;

		// Token: 0x040000CA RID: 202
		private const int Z_DEFLATED = 8;

		// Token: 0x040000CB RID: 203
		private const int Z_OK = 0;

		// Token: 0x040000CC RID: 204
		private const int Z_STREAM_END = 1;

		// Token: 0x040000CD RID: 205
		private const int Z_NEED_DICT = 2;

		// Token: 0x040000CE RID: 206
		private const int Z_ERRNO = -1;

		// Token: 0x040000CF RID: 207
		private const int Z_STREAM_ERROR = -2;

		// Token: 0x040000D0 RID: 208
		private const int Z_DATA_ERROR = -3;

		// Token: 0x040000D1 RID: 209
		private const int Z_MEM_ERROR = -4;

		// Token: 0x040000D2 RID: 210
		private const int Z_BUF_ERROR = -5;

		// Token: 0x040000D3 RID: 211
		private const int Z_VERSION_ERROR = -6;

		// Token: 0x040000D4 RID: 212
		private const int METHOD = 0;

		// Token: 0x040000D5 RID: 213
		private const int FLAG = 1;

		// Token: 0x040000D6 RID: 214
		private const int DICT4 = 2;

		// Token: 0x040000D7 RID: 215
		private const int DICT3 = 3;

		// Token: 0x040000D8 RID: 216
		private const int DICT2 = 4;

		// Token: 0x040000D9 RID: 217
		private const int DICT1 = 5;

		// Token: 0x040000DA RID: 218
		private const int DICT0 = 6;

		// Token: 0x040000DB RID: 219
		private const int BLOCKS = 7;

		// Token: 0x040000DC RID: 220
		private const int CHECK4 = 8;

		// Token: 0x040000DD RID: 221
		private const int CHECK3 = 9;

		// Token: 0x040000DE RID: 222
		private const int CHECK2 = 10;

		// Token: 0x040000DF RID: 223
		private const int CHECK1 = 11;

		// Token: 0x040000E0 RID: 224
		private const int DONE = 12;

		// Token: 0x040000E1 RID: 225
		private const int BAD = 13;

		// Token: 0x040000E2 RID: 226
		internal int mode;

		// Token: 0x040000E3 RID: 227
		internal int method;

		// Token: 0x040000E4 RID: 228
		internal long[] was = new long[1];

		// Token: 0x040000E5 RID: 229
		internal long need;

		// Token: 0x040000E6 RID: 230
		internal int marker;

		// Token: 0x040000E7 RID: 231
		internal int nowrap;

		// Token: 0x040000E8 RID: 232
		internal int wbits;

		// Token: 0x040000E9 RID: 233
		internal InfBlocks blocks;

		// Token: 0x040000EA RID: 234
		private static byte[] mark = new byte[]
		{
			0,
			0,
			(byte)SupportClass.Identity(255L),
			(byte)SupportClass.Identity(255L)
		};
	}
}
