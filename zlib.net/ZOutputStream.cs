﻿using System;
using System.IO;

namespace zlib
{
	// Token: 0x0200000E RID: 14
	public class ZOutputStream : Stream
	{
		// Token: 0x06000070 RID: 112 RVA: 0x0000AA8C File Offset: 0x00009A8C
		private void InitBlock()
		{
			this.flush_Renamed_Field = 0;
			this.buf = new byte[this.bufsize];
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000071 RID: 113 RVA: 0x0000AAB4 File Offset: 0x00009AB4
		// (set) Token: 0x06000072 RID: 114 RVA: 0x0000AAC8 File Offset: 0x00009AC8
		public virtual int FlushMode
		{
			get
			{
				return this.flush_Renamed_Field;
			}
			set
			{
				this.flush_Renamed_Field = value;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000073 RID: 115 RVA: 0x0000AADC File Offset: 0x00009ADC
		public virtual long TotalIn
		{
			get
			{
				return this.z.total_in;
			}
		}

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x06000074 RID: 116 RVA: 0x0000AAF4 File Offset: 0x00009AF4
		public virtual long TotalOut
		{
			get
			{
				return this.z.total_out;
			}
		}

		// Token: 0x06000075 RID: 117 RVA: 0x0000AB0C File Offset: 0x00009B0C
		public ZOutputStream(Stream out_Renamed)
		{
			this.InitBlock();
			this.out_Renamed = out_Renamed;
			this.z.inflateInit();
			this.compress = false;
		}

		// Token: 0x06000076 RID: 118 RVA: 0x0000AB64 File Offset: 0x00009B64
		public ZOutputStream(Stream out_Renamed, int level)
		{
			this.InitBlock();
			this.out_Renamed = out_Renamed;
			this.z.deflateInit(level);
			this.compress = true;
		}

		// Token: 0x06000077 RID: 119 RVA: 0x0000ABBC File Offset: 0x00009BBC
		public void WriteByte(int b)
		{
			this.buf1[0] = (byte)b;
			this.Write(this.buf1, 0, 1);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x0000ABE4 File Offset: 0x00009BE4
		public override void WriteByte(byte b)
		{
			this.WriteByte((int)b);
		}

		// Token: 0x06000079 RID: 121 RVA: 0x0000ABF8 File Offset: 0x00009BF8
		public override void Write(byte[] b1, int off, int len)
		{
			if (len == 0)
			{
				return;
			}
			byte[] array = new byte[b1.Length];
			Array.Copy(b1, array, b1.Length);
			this.z.next_in = array;
			this.z.next_in_index = off;
			this.z.avail_in = len;
			for (;;)
			{
				this.z.next_out = this.buf;
				this.z.next_out_index = 0;
				this.z.avail_out = this.bufsize;
				int num;
				if (this.compress)
				{
					num = this.z.deflate(this.flush_Renamed_Field);
				}
				else
				{
					num = this.z.inflate(this.flush_Renamed_Field);
				}
				if (num != 0 && num != 1)
				{
					break;
				}
				this.out_Renamed.Write(this.buf, 0, this.bufsize - this.z.avail_out);
				if (this.z.avail_in <= 0 && this.z.avail_out != 0)
				{
					return;
				}
			}
			throw new ZStreamException((this.compress ? "de" : "in") + "flating: " + this.z.msg);
		}

		// Token: 0x0600007A RID: 122 RVA: 0x0000AD18 File Offset: 0x00009D18
		public virtual void finish()
		{
			for (;;)
			{
				this.z.next_out = this.buf;
				this.z.next_out_index = 0;
				this.z.avail_out = this.bufsize;
				int num;
				if (this.compress)
				{
					num = this.z.deflate(4);
				}
				else
				{
					num = this.z.inflate(4);
				}
				if (num != 1 && num != 0)
				{
					break;
				}
				if (this.bufsize - this.z.avail_out > 0)
				{
					this.out_Renamed.Write(this.buf, 0, this.bufsize - this.z.avail_out);
				}
				if (this.z.avail_in <= 0 && this.z.avail_out != 0)
				{
					goto Block_6;
				}
			}
			throw new ZStreamException((this.compress ? "de" : "in") + "flating: " + this.z.msg);
			Block_6:
			try
			{
				this.Flush();
			}
			catch
			{
			}
		}

		// Token: 0x0600007B RID: 123 RVA: 0x0000AE30 File Offset: 0x00009E30
		public virtual void end()
		{
			if (this.compress)
			{
				this.z.deflateEnd();
			}
			else
			{
				this.z.inflateEnd();
			}
			this.z.free();
			this.z = null;
		}

		// Token: 0x0600007C RID: 124 RVA: 0x0000AE74 File Offset: 0x00009E74
		public override void Close()
		{
			try
			{
				this.finish();
			}
			catch
			{
			}
			finally
			{
				this.end();
				this.out_Renamed.Close();
				this.out_Renamed = null;
			}
		}

		// Token: 0x0600007D RID: 125 RVA: 0x0000AEDC File Offset: 0x00009EDC
		public override void Flush()
		{
			this.out_Renamed.Flush();
		}

		// Token: 0x0600007E RID: 126 RVA: 0x0000AEF4 File Offset: 0x00009EF4
		public override int Read(byte[] buffer, int offset, int count)
		{
			return 0;
		}

		// Token: 0x0600007F RID: 127 RVA: 0x0000AF04 File Offset: 0x00009F04
		public override void SetLength(long value)
		{
		}

		// Token: 0x06000080 RID: 128 RVA: 0x0000AF14 File Offset: 0x00009F14
		public override long Seek(long offset, SeekOrigin origin)
		{
			return 0L;
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000081 RID: 129 RVA: 0x0000AF24 File Offset: 0x00009F24
		public override bool CanRead
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000082 RID: 130 RVA: 0x0000AF34 File Offset: 0x00009F34
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000083 RID: 131 RVA: 0x0000AF44 File Offset: 0x00009F44
		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000084 RID: 132 RVA: 0x0000AF54 File Offset: 0x00009F54
		public override long Length
		{
			get
			{
				return 0L;
			}
		}

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000085 RID: 133 RVA: 0x0000AF64 File Offset: 0x00009F64
		// (set) Token: 0x06000086 RID: 134 RVA: 0x0000AF74 File Offset: 0x00009F74
		public override long Position
		{
			get
			{
				return 0L;
			}
			set
			{
			}
		}

		// Token: 0x04000146 RID: 326
		protected internal ZStream z = new ZStream();

		// Token: 0x04000147 RID: 327
		protected internal int bufsize = 4096;

		// Token: 0x04000148 RID: 328
		protected internal int flush_Renamed_Field;

		// Token: 0x04000149 RID: 329
		protected internal byte[] buf;

		// Token: 0x0400014A RID: 330
		protected internal byte[] buf1 = new byte[1];

		// Token: 0x0400014B RID: 331
		protected internal bool compress;

		// Token: 0x0400014C RID: 332
		private Stream out_Renamed;
	}
}
