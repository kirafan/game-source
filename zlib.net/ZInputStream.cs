﻿using System;
using System.IO;

namespace zlib
{
	// Token: 0x0200000C RID: 12
	public class ZInputStream : BinaryReader
	{
		// Token: 0x06000063 RID: 99 RVA: 0x0000A700 File Offset: 0x00009700
		private void InitBlock()
		{
			this.flush = 0;
			this.buf = new byte[this.bufsize];
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000064 RID: 100 RVA: 0x0000A728 File Offset: 0x00009728
		// (set) Token: 0x06000065 RID: 101 RVA: 0x0000A73C File Offset: 0x0000973C
		public virtual int FlushMode
		{
			get
			{
				return this.flush;
			}
			set
			{
				this.flush = value;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000066 RID: 102 RVA: 0x0000A750 File Offset: 0x00009750
		public virtual long TotalIn
		{
			get
			{
				return this.z.total_in;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000067 RID: 103 RVA: 0x0000A768 File Offset: 0x00009768
		public virtual long TotalOut
		{
			get
			{
				return this.z.total_out;
			}
		}

		// Token: 0x06000068 RID: 104 RVA: 0x0000A780 File Offset: 0x00009780
		public ZInputStream(Stream in_Renamed) : base(in_Renamed)
		{
			this.InitBlock();
			this.in_Renamed = in_Renamed;
			this.z.inflateInit();
			this.compress = false;
			this.z.next_in = this.buf;
			this.z.next_in_index = 0;
			this.z.avail_in = 0;
		}

		// Token: 0x06000069 RID: 105 RVA: 0x0000A810 File Offset: 0x00009810
		public ZInputStream(Stream in_Renamed, int level) : base(in_Renamed)
		{
			this.InitBlock();
			this.in_Renamed = in_Renamed;
			this.z.deflateInit(level);
			this.compress = true;
			this.z.next_in = this.buf;
			this.z.next_in_index = 0;
			this.z.avail_in = 0;
		}

		// Token: 0x0600006A RID: 106 RVA: 0x0000A8A0 File Offset: 0x000098A0
		public override int Read()
		{
			if (this.read(this.buf1, 0, 1) == -1)
			{
				return -1;
			}
			return (int)(this.buf1[0] & byte.MaxValue);
		}

		// Token: 0x0600006B RID: 107 RVA: 0x0000A8D0 File Offset: 0x000098D0
		public int read(byte[] b, int off, int len)
		{
			if (len == 0)
			{
				return 0;
			}
			this.z.next_out = b;
			this.z.next_out_index = off;
			this.z.avail_out = len;
			for (;;)
			{
				if (this.z.avail_in == 0 && !this.nomoreinput)
				{
					this.z.next_in_index = 0;
					this.z.avail_in = SupportClass.ReadInput(this.in_Renamed, this.buf, 0, this.bufsize);
					if (this.z.avail_in == -1)
					{
						this.z.avail_in = 0;
						this.nomoreinput = true;
					}
				}
				int num;
				if (this.compress)
				{
					num = this.z.deflate(this.flush);
				}
				else
				{
					num = this.z.inflate(this.flush);
				}
				if (this.nomoreinput && num == -5)
				{
					break;
				}
				if (num != 0 && num != 1)
				{
					goto Block_9;
				}
				if (this.nomoreinput && this.z.avail_out == len)
				{
					return -1;
				}
				if (this.z.avail_out != len || num != 0)
				{
					goto IL_12D;
				}
			}
			return -1;
			Block_9:
			throw new ZStreamException((this.compress ? "de" : "in") + "flating: " + this.z.msg);
			IL_12D:
			return len - this.z.avail_out;
		}

		// Token: 0x0600006C RID: 108 RVA: 0x0000AA18 File Offset: 0x00009A18
		public long skip(long n)
		{
			int num = 512;
			if (n < (long)num)
			{
				num = (int)n;
			}
			byte[] array = new byte[num];
			return (long)SupportClass.ReadInput(this.BaseStream, array, 0, array.Length);
		}

		// Token: 0x0600006D RID: 109 RVA: 0x0000AA4C File Offset: 0x00009A4C
		public override void Close()
		{
			this.in_Renamed.Close();
		}

		// Token: 0x04000128 RID: 296
		protected internal ZStream z = new ZStream();

		// Token: 0x04000129 RID: 297
		protected internal int bufsize = 512;

		// Token: 0x0400012A RID: 298
		protected internal int flush;

		// Token: 0x0400012B RID: 299
		protected internal byte[] buf;

		// Token: 0x0400012C RID: 300
		protected internal byte[] buf1 = new byte[1];

		// Token: 0x0400012D RID: 301
		protected internal bool compress;

		// Token: 0x0400012E RID: 302
		private Stream in_Renamed = null;

		// Token: 0x0400012F RID: 303
		private bool nomoreinput = false;
	}
}
