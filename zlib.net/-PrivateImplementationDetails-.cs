﻿using System;
using System.Runtime.InteropServices;

// Token: 0x02000011 RID: 17
internal class <PrivateImplementationDetails>
{
	// Token: 0x0400016C RID: 364 RVA: 0x000055C0 File Offset: 0x000045C0
	internal static <PrivateImplementationDetails>.$$struct0x6000033-1 $$method0x6000033-1;

	// Token: 0x0400016D RID: 365 RVA: 0x00005608 File Offset: 0x00004608
	internal static <PrivateImplementationDetails>.$$struct0x6000033-2 $$method0x6000033-2;

	// Token: 0x0400016E RID: 366 RVA: 0x00006838 File Offset: 0x00005838
	internal static <PrivateImplementationDetails>.$$struct0x6000039-1 $$method0x6000039-1;

	// Token: 0x0400016F RID: 367 RVA: 0x00007798 File Offset: 0x00006798
	internal static <PrivateImplementationDetails>.$$struct0x6000047-1 $$method0x6000047-1;

	// Token: 0x04000170 RID: 368 RVA: 0x00008F98 File Offset: 0x00007F98
	internal static <PrivateImplementationDetails>.$$struct0x6000047-2 $$method0x6000047-2;

	// Token: 0x04000171 RID: 369 RVA: 0x00009118 File Offset: 0x00008118
	internal static <PrivateImplementationDetails>.$$struct0x6000047-3 $$method0x6000047-3;

	// Token: 0x04000172 RID: 370 RVA: 0x00009198 File Offset: 0x00008198
	internal static <PrivateImplementationDetails>.$$struct0x6000047-4 $$method0x6000047-4;

	// Token: 0x04000173 RID: 371 RVA: 0x00009218 File Offset: 0x00008218
	internal static <PrivateImplementationDetails>.$$struct0x6000047-5 $$method0x6000047-5;

	// Token: 0x04000174 RID: 372 RVA: 0x00009290 File Offset: 0x00008290
	internal static <PrivateImplementationDetails>.$$struct0x6000047-6 $$method0x6000047-6;

	// Token: 0x04000175 RID: 373 RVA: 0x000093F0 File Offset: 0x000083F0
	internal static <PrivateImplementationDetails>.$$struct0x600004a-1 $$method0x600004a-1;

	// Token: 0x04000176 RID: 374 RVA: 0x00009870 File Offset: 0x00008870
	internal static <PrivateImplementationDetails>.$$struct0x600004a-2 $$method0x600004a-2;

	// Token: 0x04000177 RID: 375 RVA: 0x0000A0C0 File Offset: 0x000090C0
	internal static <PrivateImplementationDetails>.$$struct0x6000061-1 $$method0x6000061-1;

	// Token: 0x04000178 RID: 376 RVA: 0x0000A138 File Offset: 0x00009138
	internal static <PrivateImplementationDetails>.$$struct0x6000061-2 $$method0x6000061-2;

	// Token: 0x04000179 RID: 377 RVA: 0x0000A1B0 File Offset: 0x000091B0
	internal static <PrivateImplementationDetails>.$$struct0x6000061-3 $$method0x6000061-3;

	// Token: 0x0400017A RID: 378 RVA: 0x0000A200 File Offset: 0x00009200
	internal static <PrivateImplementationDetails>.$$struct0x6000061-4 $$method0x6000061-4;

	// Token: 0x0400017B RID: 379 RVA: 0x0000A218 File Offset: 0x00009218
	internal static <PrivateImplementationDetails>.$$struct0x6000061-5 $$method0x6000061-5;

	// Token: 0x0400017C RID: 380 RVA: 0x0000A418 File Offset: 0x00009418
	internal static <PrivateImplementationDetails>.$$struct0x6000061-6 $$method0x6000061-6;

	// Token: 0x0400017D RID: 381 RVA: 0x0000A518 File Offset: 0x00009518
	internal static <PrivateImplementationDetails>.$$struct0x6000061-7 $$method0x6000061-7;

	// Token: 0x0400017E RID: 382 RVA: 0x0000A590 File Offset: 0x00009590
	internal static <PrivateImplementationDetails>.$$struct0x6000061-8 $$method0x6000061-8;

	// Token: 0x02000012 RID: 18
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 68)]
	private struct $$struct0x6000033-1
	{
	}

	// Token: 0x02000013 RID: 19
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 76)]
	private struct $$struct0x6000033-2
	{
	}

	// Token: 0x02000014 RID: 20
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 68)]
	private struct $$struct0x6000039-1
	{
	}

	// Token: 0x02000015 RID: 21
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 6144)]
	private struct $$struct0x6000047-1
	{
	}

	// Token: 0x02000016 RID: 22
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 384)]
	private struct $$struct0x6000047-2
	{
	}

	// Token: 0x02000017 RID: 23
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 124)]
	private struct $$struct0x6000047-3
	{
	}

	// Token: 0x02000018 RID: 24
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 124)]
	private struct $$struct0x6000047-4
	{
	}

	// Token: 0x02000019 RID: 25
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 120)]
	private struct $$struct0x6000047-5
	{
	}

	// Token: 0x0200001A RID: 26
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 120)]
	private struct $$struct0x6000047-6
	{
	}

	// Token: 0x0200001B RID: 27
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 1152)]
	private struct $$struct0x600004a-1
	{
	}

	// Token: 0x0200001C RID: 28
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 120)]
	private struct $$struct0x600004a-2
	{
	}

	// Token: 0x0200001D RID: 29
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 116)]
	private struct $$struct0x6000061-1
	{
	}

	// Token: 0x0200001E RID: 30
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 120)]
	private struct $$struct0x6000061-2
	{
	}

	// Token: 0x0200001F RID: 31
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 76)]
	private struct $$struct0x6000061-3
	{
	}

	// Token: 0x02000020 RID: 32
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 19)]
	private struct $$struct0x6000061-4
	{
	}

	// Token: 0x02000021 RID: 33
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 512)]
	private struct $$struct0x6000061-5
	{
	}

	// Token: 0x02000022 RID: 34
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 256)]
	private struct $$struct0x6000061-6
	{
	}

	// Token: 0x02000023 RID: 35
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 116)]
	private struct $$struct0x6000061-7
	{
	}

	// Token: 0x02000024 RID: 36
	[StructLayout(LayoutKind.Explicit, Pack = 1, Size = 120)]
	private struct $$struct0x6000061-8
	{
	}
}
