﻿using System;
using System.Diagnostics;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x02000089 RID: 137
	internal class DebugHelper
	{
		// Token: 0x06000519 RID: 1305 RVA: 0x0001E254 File Offset: 0x0001C454
		[Conditional("DEBUG")]
		public static void Initialize()
		{
			if (!DebugHelper.isInitialized)
			{
				Debug.Listeners.Add(new TextWriterTraceListener(Console.Out));
				Debug.AutoFlush = true;
				DebugHelper.isInitialized = true;
			}
		}

		// Token: 0x0600051A RID: 1306 RVA: 0x0001E284 File Offset: 0x0001C484
		[Conditional("DEBUG")]
		public static void WriteLine(string format, params object[] args)
		{
		}

		// Token: 0x0600051B RID: 1307 RVA: 0x0001E288 File Offset: 0x0001C488
		[Conditional("DEBUG")]
		public static void WriteLine(string message)
		{
		}

		// Token: 0x0600051C RID: 1308 RVA: 0x0001E28C File Offset: 0x0001C48C
		[Conditional("DEBUG")]
		public static void WriteLine(string message, byte[] buffer)
		{
		}

		// Token: 0x0600051D RID: 1309 RVA: 0x0001E290 File Offset: 0x0001C490
		[Conditional("DEBUG")]
		public static void WriteBuffer(byte[] buffer)
		{
		}

		// Token: 0x0600051E RID: 1310 RVA: 0x0001E294 File Offset: 0x0001C494
		[Conditional("DEBUG")]
		public static void WriteBuffer(byte[] buffer, int index, int length)
		{
			for (int i = index; i < length; i += 16)
			{
				int num = (length - i < 16) ? (length - i) : 16;
				string str = string.Empty;
				for (int j = 0; j < num; j++)
				{
					str = str + buffer[i + j].ToString("x2") + " ";
				}
			}
		}

		// Token: 0x04000285 RID: 645
		private static bool isInitialized;
	}
}
