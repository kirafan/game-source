﻿using System;

namespace Mono.Security.Protocol.Tls.Handshake.Client
{
	// Token: 0x020000B0 RID: 176
	internal class TlsServerHelloDone : HandshakeMessage
	{
		// Token: 0x06000691 RID: 1681 RVA: 0x00024C34 File Offset: 0x00022E34
		public TlsServerHelloDone(Context context, byte[] buffer) : base(context, HandshakeType.ServerHelloDone, buffer)
		{
		}

		// Token: 0x06000692 RID: 1682 RVA: 0x00024C40 File Offset: 0x00022E40
		protected override void ProcessAsSsl3()
		{
		}

		// Token: 0x06000693 RID: 1683 RVA: 0x00024C44 File Offset: 0x00022E44
		protected override void ProcessAsTls1()
		{
		}
	}
}
