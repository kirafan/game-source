﻿using System;
using System.Runtime.Serialization;

namespace Mono.Security.Protocol.Tls
{
	// Token: 0x020000A1 RID: 161
	[Serializable]
	internal sealed class TlsException : Exception
	{
		// Token: 0x0600061D RID: 1565 RVA: 0x00022FC8 File Offset: 0x000211C8
		internal TlsException(string message) : base(message)
		{
		}

		// Token: 0x0600061E RID: 1566 RVA: 0x00022FD4 File Offset: 0x000211D4
		internal TlsException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0600061F RID: 1567 RVA: 0x00022FE0 File Offset: 0x000211E0
		internal TlsException(string message, Exception ex) : base(message, ex)
		{
		}

		// Token: 0x06000620 RID: 1568 RVA: 0x00022FEC File Offset: 0x000211EC
		internal TlsException(AlertLevel level, AlertDescription description) : this(level, description, Alert.GetAlertMessage(description))
		{
		}

		// Token: 0x06000621 RID: 1569 RVA: 0x00022FFC File Offset: 0x000211FC
		internal TlsException(AlertLevel level, AlertDescription description, string message) : base(message)
		{
			this.alert = new Alert(level, description);
		}

		// Token: 0x06000622 RID: 1570 RVA: 0x00023014 File Offset: 0x00021214
		internal TlsException(AlertDescription description) : this(description, Alert.GetAlertMessage(description))
		{
		}

		// Token: 0x06000623 RID: 1571 RVA: 0x00023024 File Offset: 0x00021224
		internal TlsException(AlertDescription description, string message) : base(message)
		{
			this.alert = new Alert(description);
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06000624 RID: 1572 RVA: 0x0002303C File Offset: 0x0002123C
		public Alert Alert
		{
			get
			{
				return this.alert;
			}
		}

		// Token: 0x040002F7 RID: 759
		private Alert alert;
	}
}
