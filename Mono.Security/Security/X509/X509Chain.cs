﻿using System;
using System.Net;
using System.Security.Permissions;
using Mono.Security.X509.Extensions;

namespace Mono.Security.X509
{
	// Token: 0x02000047 RID: 71
	public class X509Chain
	{
		// Token: 0x06000332 RID: 818 RVA: 0x00016894 File Offset: 0x00014A94
		public X509Chain()
		{
			this.certs = new X509CertificateCollection();
		}

		// Token: 0x06000333 RID: 819 RVA: 0x000168A8 File Offset: 0x00014AA8
		public X509Chain(X509CertificateCollection chain) : this()
		{
			this._chain = new X509CertificateCollection();
			this._chain.AddRange(chain);
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x06000334 RID: 820 RVA: 0x000168C8 File Offset: 0x00014AC8
		public X509CertificateCollection Chain
		{
			get
			{
				return this._chain;
			}
		}

		// Token: 0x170000AC RID: 172
		// (get) Token: 0x06000335 RID: 821 RVA: 0x000168D0 File Offset: 0x00014AD0
		public X509Certificate Root
		{
			get
			{
				return this._root;
			}
		}

		// Token: 0x170000AD RID: 173
		// (get) Token: 0x06000336 RID: 822 RVA: 0x000168D8 File Offset: 0x00014AD8
		public X509ChainStatusFlags Status
		{
			get
			{
				return this._status;
			}
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x06000337 RID: 823 RVA: 0x000168E0 File Offset: 0x00014AE0
		// (set) Token: 0x06000338 RID: 824 RVA: 0x00016918 File Offset: 0x00014B18
		public X509CertificateCollection TrustAnchors
		{
			get
			{
				if (this.roots == null)
				{
					this.roots = new X509CertificateCollection();
					this.roots.AddRange(X509StoreManager.TrustedRootCertificates);
					return this.roots;
				}
				return this.roots;
			}
			[PermissionSet(SecurityAction.Demand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"ControlPolicy\"/>\n</PermissionSet>\n")]
			set
			{
				this.roots = value;
			}
		}

		// Token: 0x06000339 RID: 825 RVA: 0x00016924 File Offset: 0x00014B24
		public void LoadCertificate(X509Certificate x509)
		{
			this.certs.Add(x509);
		}

		// Token: 0x0600033A RID: 826 RVA: 0x00016934 File Offset: 0x00014B34
		public void LoadCertificates(X509CertificateCollection collection)
		{
			this.certs.AddRange(collection);
		}

		// Token: 0x0600033B RID: 827 RVA: 0x00016944 File Offset: 0x00014B44
		public X509Certificate FindByIssuerName(string issuerName)
		{
			foreach (X509Certificate x509Certificate in this.certs)
			{
				if (x509Certificate.IssuerName == issuerName)
				{
					return x509Certificate;
				}
			}
			return null;
		}

		// Token: 0x0600033C RID: 828 RVA: 0x000169C4 File Offset: 0x00014BC4
		public bool Build(X509Certificate leaf)
		{
			this._status = X509ChainStatusFlags.NoError;
			if (this._chain == null)
			{
				this._chain = new X509CertificateCollection();
				X509Certificate x509Certificate = leaf;
				X509Certificate potentialRoot = x509Certificate;
				while (x509Certificate != null && !x509Certificate.IsSelfSigned)
				{
					potentialRoot = x509Certificate;
					this._chain.Add(x509Certificate);
					x509Certificate = this.FindCertificateParent(x509Certificate);
				}
				this._root = this.FindCertificateRoot(potentialRoot);
			}
			else
			{
				int count = this._chain.Count;
				if (count > 0)
				{
					if (this.IsParent(leaf, this._chain[0]))
					{
						int i;
						for (i = 1; i < count; i++)
						{
							if (!this.IsParent(this._chain[i - 1], this._chain[i]))
							{
								break;
							}
						}
						if (i == count)
						{
							this._root = this.FindCertificateRoot(this._chain[count - 1]);
						}
					}
				}
				else
				{
					this._root = this.FindCertificateRoot(leaf);
				}
			}
			if (this._chain != null && this._status == X509ChainStatusFlags.NoError)
			{
				foreach (X509Certificate cert in this._chain)
				{
					if (!this.IsValid(cert))
					{
						return false;
					}
				}
				if (!this.IsValid(leaf))
				{
					if (this._status == X509ChainStatusFlags.NotTimeNested)
					{
						this._status = X509ChainStatusFlags.NotTimeValid;
					}
					return false;
				}
				if (this._root != null && !this.IsValid(this._root))
				{
					return false;
				}
			}
			return this._status == X509ChainStatusFlags.NoError;
		}

		// Token: 0x0600033D RID: 829 RVA: 0x00016BA0 File Offset: 0x00014DA0
		public void Reset()
		{
			this._status = X509ChainStatusFlags.NoError;
			this.roots = null;
			this.certs.Clear();
			if (this._chain != null)
			{
				this._chain.Clear();
			}
		}

		// Token: 0x0600033E RID: 830 RVA: 0x00016BD4 File Offset: 0x00014DD4
		private bool IsValid(X509Certificate cert)
		{
			if (!cert.IsCurrent)
			{
				this._status = X509ChainStatusFlags.NotTimeNested;
				return false;
			}
			if (ServicePointManager.CheckCertificateRevocationList)
			{
			}
			return true;
		}

		// Token: 0x0600033F RID: 831 RVA: 0x00016BF8 File Offset: 0x00014DF8
		private X509Certificate FindCertificateParent(X509Certificate child)
		{
			foreach (X509Certificate x509Certificate in this.certs)
			{
				if (this.IsParent(child, x509Certificate))
				{
					return x509Certificate;
				}
			}
			return null;
		}

		// Token: 0x06000340 RID: 832 RVA: 0x00016C74 File Offset: 0x00014E74
		private X509Certificate FindCertificateRoot(X509Certificate potentialRoot)
		{
			if (potentialRoot == null)
			{
				this._status = X509ChainStatusFlags.PartialChain;
				return null;
			}
			if (this.IsTrusted(potentialRoot))
			{
				return potentialRoot;
			}
			foreach (X509Certificate x509Certificate in this.TrustAnchors)
			{
				if (this.IsParent(potentialRoot, x509Certificate))
				{
					return x509Certificate;
				}
			}
			if (potentialRoot.IsSelfSigned)
			{
				this._status = X509ChainStatusFlags.UntrustedRoot;
				return potentialRoot;
			}
			this._status = X509ChainStatusFlags.PartialChain;
			return null;
		}

		// Token: 0x06000341 RID: 833 RVA: 0x00016D30 File Offset: 0x00014F30
		private bool IsTrusted(X509Certificate potentialTrusted)
		{
			return this.TrustAnchors.Contains(potentialTrusted);
		}

		// Token: 0x06000342 RID: 834 RVA: 0x00016D40 File Offset: 0x00014F40
		private bool IsParent(X509Certificate child, X509Certificate parent)
		{
			if (child.IssuerName != parent.SubjectName)
			{
				return false;
			}
			if (parent.Version > 2 && !this.IsTrusted(parent))
			{
				X509Extension x509Extension = parent.Extensions["2.5.29.19"];
				if (x509Extension != null)
				{
					BasicConstraintsExtension basicConstraintsExtension = new BasicConstraintsExtension(x509Extension);
					if (!basicConstraintsExtension.CertificateAuthority)
					{
						this._status = X509ChainStatusFlags.InvalidBasicConstraints;
					}
				}
				else
				{
					this._status = X509ChainStatusFlags.InvalidBasicConstraints;
				}
			}
			if (!child.VerifySignature(parent.RSA))
			{
				this._status = X509ChainStatusFlags.NotSignatureValid;
				return false;
			}
			return true;
		}

		// Token: 0x04000183 RID: 387
		private X509CertificateCollection roots;

		// Token: 0x04000184 RID: 388
		private X509CertificateCollection certs;

		// Token: 0x04000185 RID: 389
		private X509Certificate _root;

		// Token: 0x04000186 RID: 390
		private X509CertificateCollection _chain;

		// Token: 0x04000187 RID: 391
		private X509ChainStatusFlags _status;
	}
}
