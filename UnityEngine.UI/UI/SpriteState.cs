﻿using System;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	// Token: 0x0200007A RID: 122
	[Serializable]
	public struct SpriteState : IEquatable<SpriteState>
	{
		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06000484 RID: 1156 RVA: 0x00018500 File Offset: 0x00016900
		// (set) Token: 0x06000485 RID: 1157 RVA: 0x0001851B File Offset: 0x0001691B
		public Sprite highlightedSprite
		{
			get
			{
				return this.m_HighlightedSprite;
			}
			set
			{
				this.m_HighlightedSprite = value;
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x06000486 RID: 1158 RVA: 0x00018528 File Offset: 0x00016928
		// (set) Token: 0x06000487 RID: 1159 RVA: 0x00018543 File Offset: 0x00016943
		public Sprite pressedSprite
		{
			get
			{
				return this.m_PressedSprite;
			}
			set
			{
				this.m_PressedSprite = value;
			}
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x06000488 RID: 1160 RVA: 0x00018550 File Offset: 0x00016950
		// (set) Token: 0x06000489 RID: 1161 RVA: 0x0001856B File Offset: 0x0001696B
		public Sprite disabledSprite
		{
			get
			{
				return this.m_DisabledSprite;
			}
			set
			{
				this.m_DisabledSprite = value;
			}
		}

		// Token: 0x0600048A RID: 1162 RVA: 0x00018578 File Offset: 0x00016978
		public bool Equals(SpriteState other)
		{
			return this.highlightedSprite == other.highlightedSprite && this.pressedSprite == other.pressedSprite && this.disabledSprite == other.disabledSprite;
		}

		// Token: 0x04000234 RID: 564
		[FormerlySerializedAs("highlightedSprite")]
		[FormerlySerializedAs("m_SelectedSprite")]
		[SerializeField]
		private Sprite m_HighlightedSprite;

		// Token: 0x04000235 RID: 565
		[FormerlySerializedAs("pressedSprite")]
		[SerializeField]
		private Sprite m_PressedSprite;

		// Token: 0x04000236 RID: 566
		[FormerlySerializedAs("disabledSprite")]
		[SerializeField]
		private Sprite m_DisabledSprite;
	}
}
