﻿using System;
using System.Collections.Generic;

namespace UnityEngine.UI
{
	// Token: 0x02000075 RID: 117
	internal static class SetPropertyUtility
	{
		// Token: 0x06000452 RID: 1106 RVA: 0x000176EC File Offset: 0x00015AEC
		public static bool SetColor(ref Color currentValue, Color newValue)
		{
			bool result;
			if (currentValue.r == newValue.r && currentValue.g == newValue.g && currentValue.b == newValue.b && currentValue.a == newValue.a)
			{
				result = false;
			}
			else
			{
				currentValue = newValue;
				result = true;
			}
			return result;
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x00017758 File Offset: 0x00015B58
		public static bool SetStruct<T>(ref T currentValue, T newValue) where T : struct
		{
			bool result;
			if (EqualityComparer<T>.Default.Equals(currentValue, newValue))
			{
				result = false;
			}
			else
			{
				currentValue = newValue;
				result = true;
			}
			return result;
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x00017794 File Offset: 0x00015B94
		public static bool SetClass<T>(ref T currentValue, T newValue) where T : class
		{
			bool result;
			if ((currentValue == null && newValue == null) || (currentValue != null && currentValue.Equals(newValue)))
			{
				result = false;
			}
			else
			{
				currentValue = newValue;
				result = true;
			}
			return result;
		}
	}
}
