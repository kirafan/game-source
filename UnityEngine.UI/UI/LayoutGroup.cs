﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Serialization;

namespace UnityEngine.UI
{
	// Token: 0x0200009B RID: 155
	[DisallowMultipleComponent]
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public abstract class LayoutGroup : UIBehaviour, ILayoutElement, ILayoutGroup, ILayoutController
	{
		// Token: 0x06000577 RID: 1399 RVA: 0x0001AC44 File Offset: 0x00019044
		protected LayoutGroup()
		{
			if (this.m_Padding == null)
			{
				this.m_Padding = new RectOffset();
			}
		}

		// Token: 0x17000184 RID: 388
		// (get) Token: 0x06000578 RID: 1400 RVA: 0x0001ACAC File Offset: 0x000190AC
		// (set) Token: 0x06000579 RID: 1401 RVA: 0x0001ACC7 File Offset: 0x000190C7
		public RectOffset padding
		{
			get
			{
				return this.m_Padding;
			}
			set
			{
				this.SetProperty<RectOffset>(ref this.m_Padding, value);
			}
		}

		// Token: 0x17000185 RID: 389
		// (get) Token: 0x0600057A RID: 1402 RVA: 0x0001ACD8 File Offset: 0x000190D8
		// (set) Token: 0x0600057B RID: 1403 RVA: 0x0001ACF3 File Offset: 0x000190F3
		public TextAnchor childAlignment
		{
			get
			{
				return this.m_ChildAlignment;
			}
			set
			{
				this.SetProperty<TextAnchor>(ref this.m_ChildAlignment, value);
			}
		}

		// Token: 0x17000186 RID: 390
		// (get) Token: 0x0600057C RID: 1404 RVA: 0x0001AD04 File Offset: 0x00019104
		protected RectTransform rectTransform
		{
			get
			{
				if (this.m_Rect == null)
				{
					this.m_Rect = base.GetComponent<RectTransform>();
				}
				return this.m_Rect;
			}
		}

		// Token: 0x17000187 RID: 391
		// (get) Token: 0x0600057D RID: 1405 RVA: 0x0001AD3C File Offset: 0x0001913C
		protected List<RectTransform> rectChildren
		{
			get
			{
				return this.m_RectChildren;
			}
		}

		// Token: 0x0600057E RID: 1406 RVA: 0x0001AD58 File Offset: 0x00019158
		public virtual void CalculateLayoutInputHorizontal()
		{
			this.m_RectChildren.Clear();
			List<Component> list = ListPool<Component>.Get();
			for (int i = 0; i < this.rectTransform.childCount; i++)
			{
				RectTransform rectTransform = this.rectTransform.GetChild(i) as RectTransform;
				if (!(rectTransform == null) && rectTransform.gameObject.activeInHierarchy)
				{
					rectTransform.GetComponents(typeof(ILayoutIgnorer), list);
					if (list.Count == 0)
					{
						this.m_RectChildren.Add(rectTransform);
					}
					else
					{
						for (int j = 0; j < list.Count; j++)
						{
							ILayoutIgnorer layoutIgnorer = (ILayoutIgnorer)list[j];
							if (!layoutIgnorer.ignoreLayout)
							{
								this.m_RectChildren.Add(rectTransform);
								break;
							}
						}
					}
				}
			}
			ListPool<Component>.Release(list);
			this.m_Tracker.Clear();
		}

		// Token: 0x0600057F RID: 1407
		public abstract void CalculateLayoutInputVertical();

		// Token: 0x17000188 RID: 392
		// (get) Token: 0x06000580 RID: 1408 RVA: 0x0001AE4C File Offset: 0x0001924C
		public virtual float minWidth
		{
			get
			{
				return this.GetTotalMinSize(0);
			}
		}

		// Token: 0x17000189 RID: 393
		// (get) Token: 0x06000581 RID: 1409 RVA: 0x0001AE68 File Offset: 0x00019268
		public virtual float preferredWidth
		{
			get
			{
				return this.GetTotalPreferredSize(0);
			}
		}

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000582 RID: 1410 RVA: 0x0001AE84 File Offset: 0x00019284
		public virtual float flexibleWidth
		{
			get
			{
				return this.GetTotalFlexibleSize(0);
			}
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000583 RID: 1411 RVA: 0x0001AEA0 File Offset: 0x000192A0
		public virtual float minHeight
		{
			get
			{
				return this.GetTotalMinSize(1);
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000584 RID: 1412 RVA: 0x0001AEBC File Offset: 0x000192BC
		public virtual float preferredHeight
		{
			get
			{
				return this.GetTotalPreferredSize(1);
			}
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x06000585 RID: 1413 RVA: 0x0001AED8 File Offset: 0x000192D8
		public virtual float flexibleHeight
		{
			get
			{
				return this.GetTotalFlexibleSize(1);
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000586 RID: 1414 RVA: 0x0001AEF4 File Offset: 0x000192F4
		public virtual int layoutPriority
		{
			get
			{
				return 0;
			}
		}

		// Token: 0x06000587 RID: 1415
		public abstract void SetLayoutHorizontal();

		// Token: 0x06000588 RID: 1416
		public abstract void SetLayoutVertical();

		// Token: 0x06000589 RID: 1417 RVA: 0x0001AF0A File Offset: 0x0001930A
		protected override void OnEnable()
		{
			base.OnEnable();
			this.SetDirty();
		}

		// Token: 0x0600058A RID: 1418 RVA: 0x0001AF19 File Offset: 0x00019319
		protected override void OnDisable()
		{
			this.m_Tracker.Clear();
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			base.OnDisable();
		}

		// Token: 0x0600058B RID: 1419 RVA: 0x0001AF38 File Offset: 0x00019338
		protected override void OnDidApplyAnimationProperties()
		{
			this.SetDirty();
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x0001AF44 File Offset: 0x00019344
		protected float GetTotalMinSize(int axis)
		{
			return this.m_TotalMinSize[axis];
		}

		// Token: 0x0600058D RID: 1421 RVA: 0x0001AF68 File Offset: 0x00019368
		protected float GetTotalPreferredSize(int axis)
		{
			return this.m_TotalPreferredSize[axis];
		}

		// Token: 0x0600058E RID: 1422 RVA: 0x0001AF8C File Offset: 0x0001938C
		protected float GetTotalFlexibleSize(int axis)
		{
			return this.m_TotalFlexibleSize[axis];
		}

		// Token: 0x0600058F RID: 1423 RVA: 0x0001AFB0 File Offset: 0x000193B0
		protected float GetStartOffset(int axis, float requiredSpaceWithoutPadding)
		{
			float num = requiredSpaceWithoutPadding + (float)((axis != 0) ? this.padding.vertical : this.padding.horizontal);
			float num2 = this.rectTransform.rect.size[axis];
			float num3 = num2 - num;
			float alignmentOnAxis = this.GetAlignmentOnAxis(axis);
			return (float)((axis != 0) ? this.padding.top : this.padding.left) + num3 * alignmentOnAxis;
		}

		// Token: 0x06000590 RID: 1424 RVA: 0x0001B040 File Offset: 0x00019440
		protected float GetAlignmentOnAxis(int axis)
		{
			float result;
			if (axis == 0)
			{
				result = (float)(this.childAlignment % TextAnchor.MiddleLeft) * 0.5f;
			}
			else
			{
				result = (float)(this.childAlignment / TextAnchor.MiddleLeft) * 0.5f;
			}
			return result;
		}

		// Token: 0x06000591 RID: 1425 RVA: 0x0001B07F File Offset: 0x0001947F
		protected void SetLayoutInputForAxis(float totalMin, float totalPreferred, float totalFlexible, int axis)
		{
			this.m_TotalMinSize[axis] = totalMin;
			this.m_TotalPreferredSize[axis] = totalPreferred;
			this.m_TotalFlexibleSize[axis] = totalFlexible;
		}

		// Token: 0x06000592 RID: 1426 RVA: 0x0001B0AC File Offset: 0x000194AC
		protected void SetChildAlongAxis(RectTransform rect, int axis, float pos)
		{
			if (!(rect == null))
			{
				this.m_Tracker.Add(this, rect, DrivenTransformProperties.Anchors | ((axis != 0) ? DrivenTransformProperties.AnchoredPositionY : DrivenTransformProperties.AnchoredPositionX));
				rect.SetInsetAndSizeFromParentEdge((axis != 0) ? RectTransform.Edge.Top : RectTransform.Edge.Left, pos, rect.sizeDelta[axis]);
			}
		}

		// Token: 0x06000593 RID: 1427 RVA: 0x0001B110 File Offset: 0x00019510
		protected void SetChildAlongAxis(RectTransform rect, int axis, float pos, float size)
		{
			if (!(rect == null))
			{
				this.m_Tracker.Add(this, rect, DrivenTransformProperties.Anchors | ((axis != 0) ? (DrivenTransformProperties.AnchoredPositionY | DrivenTransformProperties.SizeDeltaY) : (DrivenTransformProperties.AnchoredPositionX | DrivenTransformProperties.SizeDeltaX)));
				rect.SetInsetAndSizeFromParentEdge((axis != 0) ? RectTransform.Edge.Top : RectTransform.Edge.Left, pos, size);
			}
		}

		// Token: 0x1700018F RID: 399
		// (get) Token: 0x06000594 RID: 1428 RVA: 0x0001B170 File Offset: 0x00019570
		private bool isRootLayoutGroup
		{
			get
			{
				Transform parent = base.transform.parent;
				return parent == null || base.transform.parent.GetComponent(typeof(ILayoutGroup)) == null;
			}
		}

		// Token: 0x06000595 RID: 1429 RVA: 0x0001B1C4 File Offset: 0x000195C4
		protected override void OnRectTransformDimensionsChange()
		{
			base.OnRectTransformDimensionsChange();
			if (this.isRootLayoutGroup)
			{
				this.SetDirty();
			}
		}

		// Token: 0x06000596 RID: 1430 RVA: 0x0001B1DE File Offset: 0x000195DE
		protected virtual void OnTransformChildrenChanged()
		{
			this.SetDirty();
		}

		// Token: 0x06000597 RID: 1431 RVA: 0x0001B1E8 File Offset: 0x000195E8
		protected void SetProperty<T>(ref T currentValue, T newValue)
		{
			if ((currentValue != null || newValue != null) && (currentValue == null || !currentValue.Equals(newValue)))
			{
				currentValue = newValue;
				this.SetDirty();
			}
		}

		// Token: 0x06000598 RID: 1432 RVA: 0x0001B24A File Offset: 0x0001964A
		protected void SetDirty()
		{
			if (this.IsActive())
			{
				if (!CanvasUpdateRegistry.IsRebuildingLayout())
				{
					LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
				}
				else
				{
					base.StartCoroutine(this.DelayedSetDirty(this.rectTransform));
				}
			}
		}

		// Token: 0x06000599 RID: 1433 RVA: 0x0001B28C File Offset: 0x0001968C
		private IEnumerator DelayedSetDirty(RectTransform rectTransform)
		{
			yield return null;
			LayoutRebuilder.MarkLayoutForRebuild(rectTransform);
			yield break;
		}

		// Token: 0x040002A5 RID: 677
		[SerializeField]
		protected RectOffset m_Padding = new RectOffset();

		// Token: 0x040002A6 RID: 678
		[FormerlySerializedAs("m_Alignment")]
		[SerializeField]
		protected TextAnchor m_ChildAlignment = TextAnchor.UpperLeft;

		// Token: 0x040002A7 RID: 679
		[NonSerialized]
		private RectTransform m_Rect;

		// Token: 0x040002A8 RID: 680
		protected DrivenRectTransformTracker m_Tracker;

		// Token: 0x040002A9 RID: 681
		private Vector2 m_TotalMinSize = Vector2.zero;

		// Token: 0x040002AA RID: 682
		private Vector2 m_TotalPreferredSize = Vector2.zero;

		// Token: 0x040002AB RID: 683
		private Vector2 m_TotalFlexibleSize = Vector2.zero;

		// Token: 0x040002AC RID: 684
		[NonSerialized]
		private List<RectTransform> m_RectChildren = new List<RectTransform>();
	}
}
