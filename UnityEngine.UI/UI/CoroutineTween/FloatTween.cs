﻿using System;
using UnityEngine.Events;

namespace UnityEngine.UI.CoroutineTween
{
	// Token: 0x02000036 RID: 54
	internal struct FloatTween : ITweenValue
	{
		// Token: 0x1700005F RID: 95
		// (get) Token: 0x06000162 RID: 354 RVA: 0x00006750 File Offset: 0x00004B50
		// (set) Token: 0x06000163 RID: 355 RVA: 0x0000676B File Offset: 0x00004B6B
		public float startValue
		{
			get
			{
				return this.m_StartValue;
			}
			set
			{
				this.m_StartValue = value;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x06000164 RID: 356 RVA: 0x00006778 File Offset: 0x00004B78
		// (set) Token: 0x06000165 RID: 357 RVA: 0x00006793 File Offset: 0x00004B93
		public float targetValue
		{
			get
			{
				return this.m_TargetValue;
			}
			set
			{
				this.m_TargetValue = value;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x06000166 RID: 358 RVA: 0x000067A0 File Offset: 0x00004BA0
		// (set) Token: 0x06000167 RID: 359 RVA: 0x000067BB File Offset: 0x00004BBB
		public float duration
		{
			get
			{
				return this.m_Duration;
			}
			set
			{
				this.m_Duration = value;
			}
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x06000168 RID: 360 RVA: 0x000067C8 File Offset: 0x00004BC8
		// (set) Token: 0x06000169 RID: 361 RVA: 0x000067E3 File Offset: 0x00004BE3
		public bool ignoreTimeScale
		{
			get
			{
				return this.m_IgnoreTimeScale;
			}
			set
			{
				this.m_IgnoreTimeScale = value;
			}
		}

		// Token: 0x0600016A RID: 362 RVA: 0x000067F0 File Offset: 0x00004BF0
		public void TweenValue(float floatPercentage)
		{
			if (this.ValidTarget())
			{
				float arg = Mathf.Lerp(this.m_StartValue, this.m_TargetValue, floatPercentage);
				this.m_Target.Invoke(arg);
			}
		}

		// Token: 0x0600016B RID: 363 RVA: 0x0000682D File Offset: 0x00004C2D
		public void AddOnChangedCallback(UnityAction<float> callback)
		{
			if (this.m_Target == null)
			{
				this.m_Target = new FloatTween.FloatTweenCallback();
			}
			this.m_Target.AddListener(callback);
		}

		// Token: 0x0600016C RID: 364 RVA: 0x00006854 File Offset: 0x00004C54
		public bool GetIgnoreTimescale()
		{
			return this.m_IgnoreTimeScale;
		}

		// Token: 0x0600016D RID: 365 RVA: 0x00006870 File Offset: 0x00004C70
		public float GetDuration()
		{
			return this.m_Duration;
		}

		// Token: 0x0600016E RID: 366 RVA: 0x0000688C File Offset: 0x00004C8C
		public bool ValidTarget()
		{
			return this.m_Target != null;
		}

		// Token: 0x040000AF RID: 175
		private FloatTween.FloatTweenCallback m_Target;

		// Token: 0x040000B0 RID: 176
		private float m_StartValue;

		// Token: 0x040000B1 RID: 177
		private float m_TargetValue;

		// Token: 0x040000B2 RID: 178
		private float m_Duration;

		// Token: 0x040000B3 RID: 179
		private bool m_IgnoreTimeScale;

		// Token: 0x02000037 RID: 55
		public class FloatTweenCallback : UnityEvent<float>
		{
		}
	}
}
