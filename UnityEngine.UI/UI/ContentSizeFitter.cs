﻿using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x0200008D RID: 141
	[AddComponentMenu("Layout/Content Size Fitter", 141)]
	[ExecuteInEditMode]
	[RequireComponent(typeof(RectTransform))]
	public class ContentSizeFitter : UIBehaviour, ILayoutSelfController, ILayoutController
	{
		// Token: 0x06000521 RID: 1313 RVA: 0x0001AA63 File Offset: 0x00018E63
		protected ContentSizeFitter()
		{
		}

		// Token: 0x17000166 RID: 358
		// (get) Token: 0x06000522 RID: 1314 RVA: 0x0001AA7C File Offset: 0x00018E7C
		// (set) Token: 0x06000523 RID: 1315 RVA: 0x0001AA97 File Offset: 0x00018E97
		public ContentSizeFitter.FitMode horizontalFit
		{
			get
			{
				return this.m_HorizontalFit;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<ContentSizeFitter.FitMode>(ref this.m_HorizontalFit, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x17000167 RID: 359
		// (get) Token: 0x06000524 RID: 1316 RVA: 0x0001AAB4 File Offset: 0x00018EB4
		// (set) Token: 0x06000525 RID: 1317 RVA: 0x0001AACF File Offset: 0x00018ECF
		public ContentSizeFitter.FitMode verticalFit
		{
			get
			{
				return this.m_VerticalFit;
			}
			set
			{
				if (SetPropertyUtility.SetStruct<ContentSizeFitter.FitMode>(ref this.m_VerticalFit, value))
				{
					this.SetDirty();
				}
			}
		}

		// Token: 0x17000168 RID: 360
		// (get) Token: 0x06000526 RID: 1318 RVA: 0x0001AAEC File Offset: 0x00018EEC
		private RectTransform rectTransform
		{
			get
			{
				if (this.m_Rect == null)
				{
					this.m_Rect = base.GetComponent<RectTransform>();
				}
				return this.m_Rect;
			}
		}

		// Token: 0x06000527 RID: 1319 RVA: 0x0001AB24 File Offset: 0x00018F24
		protected override void OnEnable()
		{
			base.OnEnable();
			this.SetDirty();
		}

		// Token: 0x06000528 RID: 1320 RVA: 0x0001AB33 File Offset: 0x00018F33
		protected override void OnDisable()
		{
			this.m_Tracker.Clear();
			LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			base.OnDisable();
		}

		// Token: 0x06000529 RID: 1321 RVA: 0x0001AB52 File Offset: 0x00018F52
		protected override void OnRectTransformDimensionsChange()
		{
			this.SetDirty();
		}

		// Token: 0x0600052A RID: 1322 RVA: 0x0001AB5C File Offset: 0x00018F5C
		private void HandleSelfFittingAlongAxis(int axis)
		{
			ContentSizeFitter.FitMode fitMode = (axis != 0) ? this.verticalFit : this.horizontalFit;
			if (fitMode == ContentSizeFitter.FitMode.Unconstrained)
			{
				this.m_Tracker.Add(this, this.rectTransform, DrivenTransformProperties.None);
			}
			else
			{
				this.m_Tracker.Add(this, this.rectTransform, (axis != 0) ? DrivenTransformProperties.SizeDeltaY : DrivenTransformProperties.SizeDeltaX);
				if (fitMode == ContentSizeFitter.FitMode.MinSize)
				{
					this.rectTransform.SetSizeWithCurrentAnchors((RectTransform.Axis)axis, LayoutUtility.GetMinSize(this.m_Rect, axis));
				}
				else
				{
					this.rectTransform.SetSizeWithCurrentAnchors((RectTransform.Axis)axis, LayoutUtility.GetPreferredSize(this.m_Rect, axis));
				}
			}
		}

		// Token: 0x0600052B RID: 1323 RVA: 0x0001AC04 File Offset: 0x00019004
		public virtual void SetLayoutHorizontal()
		{
			this.m_Tracker.Clear();
			this.HandleSelfFittingAlongAxis(0);
		}

		// Token: 0x0600052C RID: 1324 RVA: 0x0001AC19 File Offset: 0x00019019
		public virtual void SetLayoutVertical()
		{
			this.HandleSelfFittingAlongAxis(1);
		}

		// Token: 0x0600052D RID: 1325 RVA: 0x0001AC23 File Offset: 0x00019023
		protected void SetDirty()
		{
			if (this.IsActive())
			{
				LayoutRebuilder.MarkLayoutForRebuild(this.rectTransform);
			}
		}

		// Token: 0x0400027F RID: 639
		[SerializeField]
		protected ContentSizeFitter.FitMode m_HorizontalFit = ContentSizeFitter.FitMode.Unconstrained;

		// Token: 0x04000280 RID: 640
		[SerializeField]
		protected ContentSizeFitter.FitMode m_VerticalFit = ContentSizeFitter.FitMode.Unconstrained;

		// Token: 0x04000281 RID: 641
		[NonSerialized]
		private RectTransform m_Rect;

		// Token: 0x04000282 RID: 642
		private DrivenRectTransformTracker m_Tracker;

		// Token: 0x0200008E RID: 142
		public enum FitMode
		{
			// Token: 0x04000284 RID: 644
			Unconstrained,
			// Token: 0x04000285 RID: 645
			MinSize,
			// Token: 0x04000286 RID: 646
			PreferredSize
		}
	}
}
