﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000085 RID: 133
	public interface IClippable
	{
		// Token: 0x17000157 RID: 343
		// (get) Token: 0x060004EC RID: 1260
		GameObject gameObject { get; }

		// Token: 0x060004ED RID: 1261
		void RecalculateClipping();

		// Token: 0x17000158 RID: 344
		// (get) Token: 0x060004EE RID: 1262
		RectTransform rectTransform { get; }

		// Token: 0x060004EF RID: 1263
		void Cull(Rect clipRect, bool validRect);

		// Token: 0x060004F0 RID: 1264
		void SetClipRect(Rect value, bool validRect);
	}
}
