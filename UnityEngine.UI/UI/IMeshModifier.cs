﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x020000AC RID: 172
	public interface IMeshModifier
	{
		// Token: 0x0600061A RID: 1562
		[Obsolete("use IMeshModifier.ModifyMesh (VertexHelper verts) instead", false)]
		void ModifyMesh(Mesh mesh);

		// Token: 0x0600061B RID: 1563
		void ModifyMesh(VertexHelper verts);
	}
}
