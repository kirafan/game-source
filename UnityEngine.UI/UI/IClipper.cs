﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000084 RID: 132
	public interface IClipper
	{
		// Token: 0x060004EB RID: 1259
		void PerformClipping();
	}
}
