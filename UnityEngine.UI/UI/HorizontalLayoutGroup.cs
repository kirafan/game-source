﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000093 RID: 147
	[AddComponentMenu("Layout/Horizontal Layout Group", 150)]
	public class HorizontalLayoutGroup : HorizontalOrVerticalLayoutGroup
	{
		// Token: 0x06000540 RID: 1344 RVA: 0x0001C136 File Offset: 0x0001A536
		protected HorizontalLayoutGroup()
		{
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x0001C13F File Offset: 0x0001A53F
		public override void CalculateLayoutInputHorizontal()
		{
			base.CalculateLayoutInputHorizontal();
			base.CalcAlongAxis(0, false);
		}

		// Token: 0x06000542 RID: 1346 RVA: 0x0001C150 File Offset: 0x0001A550
		public override void CalculateLayoutInputVertical()
		{
			base.CalcAlongAxis(1, false);
		}

		// Token: 0x06000543 RID: 1347 RVA: 0x0001C15B File Offset: 0x0001A55B
		public override void SetLayoutHorizontal()
		{
			base.SetChildrenAlongAxis(0, false);
		}

		// Token: 0x06000544 RID: 1348 RVA: 0x0001C166 File Offset: 0x0001A566
		public override void SetLayoutVertical()
		{
			base.SetChildrenAlongAxis(1, false);
		}
	}
}
