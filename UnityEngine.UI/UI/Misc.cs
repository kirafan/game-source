﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000065 RID: 101
	internal static class Misc
	{
		// Token: 0x0600036F RID: 879 RVA: 0x00014314 File Offset: 0x00012714
		public static void Destroy(Object obj)
		{
			if (obj != null)
			{
				if (Application.isPlaying)
				{
					if (obj is GameObject)
					{
						GameObject gameObject = obj as GameObject;
						gameObject.transform.parent = null;
					}
					Object.Destroy(obj);
				}
				else
				{
					Object.DestroyImmediate(obj);
				}
			}
		}

		// Token: 0x06000370 RID: 880 RVA: 0x0001436D File Offset: 0x0001276D
		public static void DestroyImmediate(Object obj)
		{
			if (obj != null)
			{
				if (Application.isEditor)
				{
					Object.DestroyImmediate(obj);
				}
				else
				{
					Object.Destroy(obj);
				}
			}
		}
	}
}
