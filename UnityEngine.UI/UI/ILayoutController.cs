﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000096 RID: 150
	public interface ILayoutController
	{
		// Token: 0x0600055C RID: 1372
		void SetLayoutHorizontal();

		// Token: 0x0600055D RID: 1373
		void SetLayoutVertical();
	}
}
