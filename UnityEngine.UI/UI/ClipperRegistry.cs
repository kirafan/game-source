﻿using System;
using UnityEngine.UI.Collections;

namespace UnityEngine.UI
{
	// Token: 0x02000082 RID: 130
	public class ClipperRegistry
	{
		// Token: 0x060004E4 RID: 1252 RVA: 0x00019D24 File Offset: 0x00018124
		protected ClipperRegistry()
		{
		}

		// Token: 0x17000156 RID: 342
		// (get) Token: 0x060004E5 RID: 1253 RVA: 0x00019D44 File Offset: 0x00018144
		public static ClipperRegistry instance
		{
			get
			{
				if (ClipperRegistry.s_Instance == null)
				{
					ClipperRegistry.s_Instance = new ClipperRegistry();
				}
				return ClipperRegistry.s_Instance;
			}
		}

		// Token: 0x060004E6 RID: 1254 RVA: 0x00019D74 File Offset: 0x00018174
		public void Cull()
		{
			for (int i = 0; i < this.m_Clippers.Count; i++)
			{
				this.m_Clippers[i].PerformClipping();
			}
		}

		// Token: 0x060004E7 RID: 1255 RVA: 0x00019DB1 File Offset: 0x000181B1
		public static void Register(IClipper c)
		{
			if (c != null)
			{
				ClipperRegistry.instance.m_Clippers.AddUnique(c);
			}
		}

		// Token: 0x060004E8 RID: 1256 RVA: 0x00019DD0 File Offset: 0x000181D0
		public static void Unregister(IClipper c)
		{
			ClipperRegistry.instance.m_Clippers.Remove(c);
		}

		// Token: 0x04000255 RID: 597
		private static ClipperRegistry s_Instance;

		// Token: 0x04000256 RID: 598
		private readonly IndexedSet<IClipper> m_Clippers = new IndexedSet<IClipper>();
	}
}
