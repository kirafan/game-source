﻿using System;
using System.Collections.Generic;
using UnityEngine.UI.Collections;

namespace UnityEngine.UI
{
	// Token: 0x0200004C RID: 76
	public class GraphicRegistry
	{
		// Token: 0x06000269 RID: 617 RVA: 0x0000C5F0 File Offset: 0x0000A9F0
		protected GraphicRegistry()
		{
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x0600026A RID: 618 RVA: 0x0000C610 File Offset: 0x0000AA10
		public static GraphicRegistry instance
		{
			get
			{
				if (GraphicRegistry.s_Instance == null)
				{
					GraphicRegistry.s_Instance = new GraphicRegistry();
				}
				return GraphicRegistry.s_Instance;
			}
		}

		// Token: 0x0600026B RID: 619 RVA: 0x0000C640 File Offset: 0x0000AA40
		public static void RegisterGraphicForCanvas(Canvas c, Graphic graphic)
		{
			if (!(c == null))
			{
				IndexedSet<Graphic> indexedSet;
				GraphicRegistry.instance.m_Graphics.TryGetValue(c, out indexedSet);
				if (indexedSet != null)
				{
					indexedSet.AddUnique(graphic);
				}
				else
				{
					indexedSet = new IndexedSet<Graphic>();
					indexedSet.Add(graphic);
					GraphicRegistry.instance.m_Graphics.Add(c, indexedSet);
				}
			}
		}

		// Token: 0x0600026C RID: 620 RVA: 0x0000C6A4 File Offset: 0x0000AAA4
		public static void UnregisterGraphicForCanvas(Canvas c, Graphic graphic)
		{
			if (!(c == null))
			{
				IndexedSet<Graphic> indexedSet;
				if (GraphicRegistry.instance.m_Graphics.TryGetValue(c, out indexedSet))
				{
					indexedSet.Remove(graphic);
					if (indexedSet.Count == 0)
					{
						GraphicRegistry.instance.m_Graphics.Remove(c);
					}
				}
			}
		}

		// Token: 0x0600026D RID: 621 RVA: 0x0000C700 File Offset: 0x0000AB00
		public static IList<Graphic> GetGraphicsForCanvas(Canvas canvas)
		{
			IndexedSet<Graphic> indexedSet;
			IList<Graphic> result;
			if (GraphicRegistry.instance.m_Graphics.TryGetValue(canvas, out indexedSet))
			{
				result = indexedSet;
			}
			else
			{
				result = GraphicRegistry.s_EmptyList;
			}
			return result;
		}

		// Token: 0x04000125 RID: 293
		private static GraphicRegistry s_Instance;

		// Token: 0x04000126 RID: 294
		private readonly Dictionary<Canvas, IndexedSet<Graphic>> m_Graphics = new Dictionary<Canvas, IndexedSet<Graphic>>();

		// Token: 0x04000127 RID: 295
		private static readonly List<Graphic> s_EmptyList = new List<Graphic>();
	}
}
