﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000056 RID: 86
	[Obsolete("Not supported anymore.", true)]
	public interface IMask
	{
		// Token: 0x060002A9 RID: 681
		bool Enabled();

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x060002AA RID: 682
		RectTransform rectTransform { get; }
	}
}
