﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x02000081 RID: 129
	[AddComponentMenu("UI/Toggle Group", 32)]
	[DisallowMultipleComponent]
	public class ToggleGroup : UIBehaviour
	{
		// Token: 0x060004D8 RID: 1240 RVA: 0x00019AF0 File Offset: 0x00017EF0
		protected ToggleGroup()
		{
		}

		// Token: 0x17000155 RID: 341
		// (get) Token: 0x060004D9 RID: 1241 RVA: 0x00019B0C File Offset: 0x00017F0C
		// (set) Token: 0x060004DA RID: 1242 RVA: 0x00019B27 File Offset: 0x00017F27
		public bool allowSwitchOff
		{
			get
			{
				return this.m_AllowSwitchOff;
			}
			set
			{
				this.m_AllowSwitchOff = value;
			}
		}

		// Token: 0x060004DB RID: 1243 RVA: 0x00019B31 File Offset: 0x00017F31
		private void ValidateToggleIsInGroup(Toggle toggle)
		{
			if (toggle == null || !this.m_Toggles.Contains(toggle))
			{
				throw new ArgumentException(string.Format("Toggle {0} is not part of ToggleGroup {1}", new object[]
				{
					toggle,
					this
				}));
			}
		}

		// Token: 0x060004DC RID: 1244 RVA: 0x00019B70 File Offset: 0x00017F70
		public void NotifyToggleOn(Toggle toggle)
		{
			this.ValidateToggleIsInGroup(toggle);
			for (int i = 0; i < this.m_Toggles.Count; i++)
			{
				if (!(this.m_Toggles[i] == toggle))
				{
					this.m_Toggles[i].isOn = false;
				}
			}
		}

		// Token: 0x060004DD RID: 1245 RVA: 0x00019BD1 File Offset: 0x00017FD1
		public void UnregisterToggle(Toggle toggle)
		{
			if (this.m_Toggles.Contains(toggle))
			{
				this.m_Toggles.Remove(toggle);
			}
		}

		// Token: 0x060004DE RID: 1246 RVA: 0x00019BF2 File Offset: 0x00017FF2
		public void RegisterToggle(Toggle toggle)
		{
			if (!this.m_Toggles.Contains(toggle))
			{
				this.m_Toggles.Add(toggle);
			}
		}

		// Token: 0x060004DF RID: 1247 RVA: 0x00019C14 File Offset: 0x00018014
		public bool AnyTogglesOn()
		{
			return this.m_Toggles.Find((Toggle x) => x.isOn) != null;
		}

		// Token: 0x060004E0 RID: 1248 RVA: 0x00019C58 File Offset: 0x00018058
		public IEnumerable<Toggle> ActiveToggles()
		{
			return from x in this.m_Toggles
			where x.isOn
			select x;
		}

		// Token: 0x060004E1 RID: 1249 RVA: 0x00019C98 File Offset: 0x00018098
		public void SetAllTogglesOff()
		{
			bool allowSwitchOff = this.m_AllowSwitchOff;
			this.m_AllowSwitchOff = true;
			for (int i = 0; i < this.m_Toggles.Count; i++)
			{
				this.m_Toggles[i].isOn = false;
			}
			this.m_AllowSwitchOff = allowSwitchOff;
		}

		// Token: 0x04000251 RID: 593
		[SerializeField]
		private bool m_AllowSwitchOff = false;

		// Token: 0x04000252 RID: 594
		private List<Toggle> m_Toggles = new List<Toggle>();
	}
}
