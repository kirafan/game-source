﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x0200009F RID: 159
	public interface IMaterialModifier
	{
		// Token: 0x060005CD RID: 1485
		Material GetModifiedMaterial(Material baseMaterial);
	}
}
