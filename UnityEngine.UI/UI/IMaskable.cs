﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000057 RID: 87
	public interface IMaskable
	{
		// Token: 0x060002AB RID: 683
		void RecalculateMasking();
	}
}
