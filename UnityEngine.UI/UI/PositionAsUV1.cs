﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x020000AE RID: 174
	[AddComponentMenu("UI/Effects/Position As UV1", 16)]
	public class PositionAsUV1 : BaseMeshEffect
	{
		// Token: 0x0600061E RID: 1566 RVA: 0x0001E0F6 File Offset: 0x0001C4F6
		protected PositionAsUV1()
		{
		}

		// Token: 0x0600061F RID: 1567 RVA: 0x0001E100 File Offset: 0x0001C500
		public override void ModifyMesh(VertexHelper vh)
		{
			UIVertex vertex = default(UIVertex);
			for (int i = 0; i < vh.currentVertCount; i++)
			{
				vh.PopulateUIVertex(ref vertex, i);
				vertex.uv1 = new Vector2(vertex.position.x, vertex.position.y);
				vh.SetUIVertex(vertex, i);
			}
		}
	}
}
