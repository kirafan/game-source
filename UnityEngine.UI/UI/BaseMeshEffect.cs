﻿using System;
using UnityEngine.EventSystems;

namespace UnityEngine.UI
{
	// Token: 0x020000AA RID: 170
	[ExecuteInEditMode]
	public abstract class BaseMeshEffect : UIBehaviour, IMeshModifier
	{
		// Token: 0x1700019A RID: 410
		// (get) Token: 0x06000613 RID: 1555 RVA: 0x0001DB9C File Offset: 0x0001BF9C
		protected Graphic graphic
		{
			get
			{
				if (this.m_Graphic == null)
				{
					this.m_Graphic = base.GetComponent<Graphic>();
				}
				return this.m_Graphic;
			}
		}

		// Token: 0x06000614 RID: 1556 RVA: 0x0001DBD4 File Offset: 0x0001BFD4
		protected override void OnEnable()
		{
			base.OnEnable();
			if (this.graphic != null)
			{
				this.graphic.SetVerticesDirty();
			}
		}

		// Token: 0x06000615 RID: 1557 RVA: 0x0001DBF9 File Offset: 0x0001BFF9
		protected override void OnDisable()
		{
			if (this.graphic != null)
			{
				this.graphic.SetVerticesDirty();
			}
			base.OnDisable();
		}

		// Token: 0x06000616 RID: 1558 RVA: 0x0001DC1E File Offset: 0x0001C01E
		protected override void OnDidApplyAnimationProperties()
		{
			if (this.graphic != null)
			{
				this.graphic.SetVerticesDirty();
			}
			base.OnDidApplyAnimationProperties();
		}

		// Token: 0x06000617 RID: 1559 RVA: 0x0001DC44 File Offset: 0x0001C044
		public virtual void ModifyMesh(Mesh mesh)
		{
			using (VertexHelper vertexHelper = new VertexHelper(mesh))
			{
				this.ModifyMesh(vertexHelper);
				vertexHelper.FillMesh(mesh);
			}
		}

		// Token: 0x06000618 RID: 1560
		public abstract void ModifyMesh(VertexHelper vh);

		// Token: 0x040002D3 RID: 723
		[NonSerialized]
		private Graphic m_Graphic;
	}
}
