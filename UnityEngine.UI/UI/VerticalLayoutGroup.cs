﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x0200009E RID: 158
	[AddComponentMenu("Layout/Vertical Layout Group", 151)]
	public class VerticalLayoutGroup : HorizontalOrVerticalLayoutGroup
	{
		// Token: 0x060005C8 RID: 1480 RVA: 0x0001CDEA File Offset: 0x0001B1EA
		protected VerticalLayoutGroup()
		{
		}

		// Token: 0x060005C9 RID: 1481 RVA: 0x0001CDF3 File Offset: 0x0001B1F3
		public override void CalculateLayoutInputHorizontal()
		{
			base.CalculateLayoutInputHorizontal();
			base.CalcAlongAxis(0, true);
		}

		// Token: 0x060005CA RID: 1482 RVA: 0x0001CE04 File Offset: 0x0001B204
		public override void CalculateLayoutInputVertical()
		{
			base.CalcAlongAxis(1, true);
		}

		// Token: 0x060005CB RID: 1483 RVA: 0x0001CE0F File Offset: 0x0001B20F
		public override void SetLayoutHorizontal()
		{
			base.SetChildrenAlongAxis(0, true);
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x0001CE1A File Offset: 0x0001B21A
		public override void SetLayoutVertical()
		{
			base.SetChildrenAlongAxis(1, true);
		}
	}
}
