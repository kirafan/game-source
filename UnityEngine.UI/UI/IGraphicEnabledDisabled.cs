﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x0200004D RID: 77
	[Obsolete("Not supported anymore")]
	internal interface IGraphicEnabledDisabled
	{
		// Token: 0x0600026F RID: 623
		void OnSiblingGraphicEnabledDisabled();
	}
}
