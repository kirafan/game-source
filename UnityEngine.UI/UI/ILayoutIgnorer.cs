﻿using System;

namespace UnityEngine.UI
{
	// Token: 0x02000099 RID: 153
	public interface ILayoutIgnorer
	{
		// Token: 0x1700017B RID: 379
		// (get) Token: 0x0600055E RID: 1374
		bool ignoreLayout { get; }
	}
}
