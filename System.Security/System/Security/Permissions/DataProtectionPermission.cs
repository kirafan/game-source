﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x02000071 RID: 113
	[Serializable]
	public sealed class DataProtectionPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x06000346 RID: 838 RVA: 0x0000EBFC File Offset: 0x0000CDFC
		public DataProtectionPermission(PermissionState state)
		{
			if (PermissionHelper.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this._flags = DataProtectionPermissionFlags.AllFlags;
			}
		}

		// Token: 0x06000347 RID: 839 RVA: 0x0000EC1C File Offset: 0x0000CE1C
		public DataProtectionPermission(DataProtectionPermissionFlags flags)
		{
			this.Flags = flags;
		}

		// Token: 0x170000D9 RID: 217
		// (get) Token: 0x06000348 RID: 840 RVA: 0x0000EC2C File Offset: 0x0000CE2C
		// (set) Token: 0x06000349 RID: 841 RVA: 0x0000EC34 File Offset: 0x0000CE34
		public DataProtectionPermissionFlags Flags
		{
			get
			{
				return this._flags;
			}
			set
			{
				if ((value & ~(DataProtectionPermissionFlags.ProtectData | DataProtectionPermissionFlags.UnprotectData | DataProtectionPermissionFlags.ProtectMemory | DataProtectionPermissionFlags.UnprotectMemory)) != DataProtectionPermissionFlags.NoFlags)
				{
					string message = string.Format(Locale.GetText("Invalid enum {0}"), value);
					throw new ArgumentException(message, "DataProtectionPermissionFlags");
				}
				this._flags = value;
			}
		}

		// Token: 0x0600034A RID: 842 RVA: 0x0000EC74 File Offset: 0x0000CE74
		public bool IsUnrestricted()
		{
			return this._flags == DataProtectionPermissionFlags.AllFlags;
		}

		// Token: 0x0600034B RID: 843 RVA: 0x0000EC80 File Offset: 0x0000CE80
		public override IPermission Copy()
		{
			return new DataProtectionPermission(this._flags);
		}

		// Token: 0x0600034C RID: 844 RVA: 0x0000EC90 File Offset: 0x0000CE90
		public override IPermission Intersect(IPermission target)
		{
			DataProtectionPermission dataProtectionPermission = this.Cast(target);
			if (dataProtectionPermission == null)
			{
				return null;
			}
			if (this.IsUnrestricted() && dataProtectionPermission.IsUnrestricted())
			{
				return new DataProtectionPermission(PermissionState.Unrestricted);
			}
			if (this.IsUnrestricted())
			{
				return dataProtectionPermission.Copy();
			}
			if (dataProtectionPermission.IsUnrestricted())
			{
				return this.Copy();
			}
			return new DataProtectionPermission(this._flags & dataProtectionPermission._flags);
		}

		// Token: 0x0600034D RID: 845 RVA: 0x0000ED00 File Offset: 0x0000CF00
		public override IPermission Union(IPermission target)
		{
			DataProtectionPermission dataProtectionPermission = this.Cast(target);
			if (dataProtectionPermission == null)
			{
				return this.Copy();
			}
			if (this.IsUnrestricted() || dataProtectionPermission.IsUnrestricted())
			{
				return new SecurityPermission(PermissionState.Unrestricted);
			}
			return new DataProtectionPermission(this._flags | dataProtectionPermission._flags);
		}

		// Token: 0x0600034E RID: 846 RVA: 0x0000ED54 File Offset: 0x0000CF54
		public override bool IsSubsetOf(IPermission target)
		{
			DataProtectionPermission dataProtectionPermission = this.Cast(target);
			if (dataProtectionPermission == null)
			{
				return this._flags == DataProtectionPermissionFlags.NoFlags;
			}
			return dataProtectionPermission.IsUnrestricted() || (!this.IsUnrestricted() && (this._flags & ~dataProtectionPermission._flags) == DataProtectionPermissionFlags.NoFlags);
		}

		// Token: 0x0600034F RID: 847 RVA: 0x0000EDA4 File Offset: 0x0000CFA4
		public override void FromXml(SecurityElement e)
		{
			PermissionHelper.CheckSecurityElement(e, "e", 1, 1);
			this._flags = (DataProtectionPermissionFlags)((int)Enum.Parse(typeof(DataProtectionPermissionFlags), e.Attribute("Flags")));
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000EDDC File Offset: 0x0000CFDC
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = PermissionHelper.Element(typeof(DataProtectionPermission), 1);
			securityElement.AddAttribute("Flags", this._flags.ToString());
			return securityElement;
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000EE18 File Offset: 0x0000D018
		private DataProtectionPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			DataProtectionPermission dataProtectionPermission = target as DataProtectionPermission;
			if (dataProtectionPermission == null)
			{
				PermissionHelper.ThrowInvalidPermission(target, typeof(DataProtectionPermission));
			}
			return dataProtectionPermission;
		}

		// Token: 0x040001A2 RID: 418
		private const int version = 1;

		// Token: 0x040001A3 RID: 419
		private DataProtectionPermissionFlags _flags;
	}
}
