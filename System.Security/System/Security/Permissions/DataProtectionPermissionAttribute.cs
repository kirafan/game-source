﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x02000072 RID: 114
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class DataProtectionPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06000352 RID: 850 RVA: 0x0000EE4C File Offset: 0x0000D04C
		public DataProtectionPermissionAttribute(SecurityAction action) : base(action)
		{
		}

		// Token: 0x170000DA RID: 218
		// (get) Token: 0x06000353 RID: 851 RVA: 0x0000EE58 File Offset: 0x0000D058
		// (set) Token: 0x06000354 RID: 852 RVA: 0x0000EE60 File Offset: 0x0000D060
		public DataProtectionPermissionFlags Flags
		{
			get
			{
				return this._flags;
			}
			set
			{
				if ((value & DataProtectionPermissionFlags.AllFlags) != value)
				{
					string message = string.Format(Locale.GetText("Invalid flags {0}"), value);
					throw new ArgumentException(message, "DataProtectionPermissionFlags");
				}
				this._flags = value;
			}
		}

		// Token: 0x170000DB RID: 219
		// (get) Token: 0x06000355 RID: 853 RVA: 0x0000EEA0 File Offset: 0x0000D0A0
		// (set) Token: 0x06000356 RID: 854 RVA: 0x0000EEB0 File Offset: 0x0000D0B0
		public bool ProtectData
		{
			get
			{
				return (this._flags & DataProtectionPermissionFlags.ProtectData) != DataProtectionPermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= DataProtectionPermissionFlags.ProtectData;
				}
				else
				{
					this._flags &= ~DataProtectionPermissionFlags.ProtectData;
				}
			}
		}

		// Token: 0x170000DC RID: 220
		// (get) Token: 0x06000357 RID: 855 RVA: 0x0000EEE8 File Offset: 0x0000D0E8
		// (set) Token: 0x06000358 RID: 856 RVA: 0x0000EEF8 File Offset: 0x0000D0F8
		public bool UnprotectData
		{
			get
			{
				return (this._flags & DataProtectionPermissionFlags.UnprotectData) != DataProtectionPermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= DataProtectionPermissionFlags.UnprotectData;
				}
				else
				{
					this._flags &= ~DataProtectionPermissionFlags.UnprotectData;
				}
			}
		}

		// Token: 0x170000DD RID: 221
		// (get) Token: 0x06000359 RID: 857 RVA: 0x0000EF30 File Offset: 0x0000D130
		// (set) Token: 0x0600035A RID: 858 RVA: 0x0000EF40 File Offset: 0x0000D140
		public bool ProtectMemory
		{
			get
			{
				return (this._flags & DataProtectionPermissionFlags.ProtectMemory) != DataProtectionPermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= DataProtectionPermissionFlags.ProtectMemory;
				}
				else
				{
					this._flags &= ~DataProtectionPermissionFlags.ProtectMemory;
				}
			}
		}

		// Token: 0x170000DE RID: 222
		// (get) Token: 0x0600035B RID: 859 RVA: 0x0000EF78 File Offset: 0x0000D178
		// (set) Token: 0x0600035C RID: 860 RVA: 0x0000EF88 File Offset: 0x0000D188
		public bool UnprotectMemory
		{
			get
			{
				return (this._flags & DataProtectionPermissionFlags.UnprotectMemory) != DataProtectionPermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= DataProtectionPermissionFlags.UnprotectMemory;
				}
				else
				{
					this._flags &= ~DataProtectionPermissionFlags.UnprotectMemory;
				}
			}
		}

		// Token: 0x0600035D RID: 861 RVA: 0x0000EFC0 File Offset: 0x0000D1C0
		public override IPermission CreatePermission()
		{
			DataProtectionPermission result;
			if (base.Unrestricted)
			{
				result = new DataProtectionPermission(PermissionState.Unrestricted);
			}
			else
			{
				result = new DataProtectionPermission(this._flags);
			}
			return result;
		}

		// Token: 0x040001A4 RID: 420
		private DataProtectionPermissionFlags _flags;
	}
}
