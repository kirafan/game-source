﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x02000073 RID: 115
	[Flags]
	[Serializable]
	public enum DataProtectionPermissionFlags
	{
		// Token: 0x040001A6 RID: 422
		NoFlags = 0,
		// Token: 0x040001A7 RID: 423
		ProtectData = 1,
		// Token: 0x040001A8 RID: 424
		UnprotectData = 2,
		// Token: 0x040001A9 RID: 425
		ProtectMemory = 4,
		// Token: 0x040001AA RID: 426
		UnprotectMemory = 8,
		// Token: 0x040001AB RID: 427
		AllFlags = 15
	}
}
