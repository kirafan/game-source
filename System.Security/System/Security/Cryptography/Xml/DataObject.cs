﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000036 RID: 54
	public class DataObject
	{
		// Token: 0x06000138 RID: 312 RVA: 0x000066E4 File Offset: 0x000048E4
		public DataObject()
		{
			this.Build(null, null, null, null);
		}

		// Token: 0x06000139 RID: 313 RVA: 0x000066F8 File Offset: 0x000048F8
		public DataObject(string id, string mimeType, string encoding, XmlElement data)
		{
			if (data == null)
			{
				throw new ArgumentNullException("data");
			}
			this.Build(id, mimeType, encoding, data);
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00006720 File Offset: 0x00004920
		private void Build(string id, string mimeType, string encoding, XmlElement data)
		{
			XmlDocument xmlDocument = new XmlDocument();
			XmlElement xmlElement = xmlDocument.CreateElement("Object", "http://www.w3.org/2000/09/xmldsig#");
			if (id != null)
			{
				xmlElement.SetAttribute("Id", id);
			}
			if (mimeType != null)
			{
				xmlElement.SetAttribute("MimeType", mimeType);
			}
			if (encoding != null)
			{
				xmlElement.SetAttribute("Encoding", encoding);
			}
			if (data != null)
			{
				XmlNode newChild = xmlDocument.ImportNode(data, true);
				xmlElement.AppendChild(newChild);
			}
			this.element = xmlElement;
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x0600013B RID: 315 RVA: 0x0000679C File Offset: 0x0000499C
		// (set) Token: 0x0600013C RID: 316 RVA: 0x000067AC File Offset: 0x000049AC
		public XmlNodeList Data
		{
			get
			{
				return this.element.ChildNodes;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("value");
				}
				XmlDocument xmlDocument = new XmlDocument();
				XmlElement xmlElement = (XmlElement)xmlDocument.ImportNode(this.element, true);
				while (xmlElement.LastChild != null)
				{
					xmlElement.RemoveChild(xmlElement.LastChild);
				}
				foreach (object obj in value)
				{
					XmlNode node = (XmlNode)obj;
					xmlElement.AppendChild(xmlDocument.ImportNode(node, true));
				}
				this.element = xmlElement;
				this.propertyModified = true;
			}
		}

		// Token: 0x1700005B RID: 91
		// (get) Token: 0x0600013D RID: 317 RVA: 0x00006878 File Offset: 0x00004A78
		// (set) Token: 0x0600013E RID: 318 RVA: 0x00006888 File Offset: 0x00004A88
		public string Encoding
		{
			get
			{
				return this.GetField("Encoding");
			}
			set
			{
				this.SetField("Encoding", value);
			}
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x0600013F RID: 319 RVA: 0x00006898 File Offset: 0x00004A98
		// (set) Token: 0x06000140 RID: 320 RVA: 0x000068A8 File Offset: 0x00004AA8
		public string Id
		{
			get
			{
				return this.GetField("Id");
			}
			set
			{
				this.SetField("Id", value);
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x06000141 RID: 321 RVA: 0x000068B8 File Offset: 0x00004AB8
		// (set) Token: 0x06000142 RID: 322 RVA: 0x000068C8 File Offset: 0x00004AC8
		public string MimeType
		{
			get
			{
				return this.GetField("MimeType");
			}
			set
			{
				this.SetField("MimeType", value);
			}
		}

		// Token: 0x06000143 RID: 323 RVA: 0x000068D8 File Offset: 0x00004AD8
		private string GetField(string attribute)
		{
			XmlNode xmlNode = this.element.Attributes[attribute];
			return (xmlNode == null) ? null : xmlNode.Value;
		}

		// Token: 0x06000144 RID: 324 RVA: 0x0000690C File Offset: 0x00004B0C
		private void SetField(string attribute, string value)
		{
			if (value == null)
			{
				return;
			}
			if (this.propertyModified)
			{
				this.element.SetAttribute(attribute, value);
			}
			else
			{
				XmlDocument xmlDocument = new XmlDocument();
				XmlElement xmlElement = xmlDocument.ImportNode(this.element, true) as XmlElement;
				xmlElement.SetAttribute(attribute, value);
				this.element = xmlElement;
				this.propertyModified = true;
			}
		}

		// Token: 0x06000145 RID: 325 RVA: 0x0000696C File Offset: 0x00004B6C
		public XmlElement GetXml()
		{
			if (this.propertyModified)
			{
				XmlElement xmlElement = this.element;
				XmlDocument xmlDocument = new XmlDocument();
				this.element = xmlDocument.CreateElement("Object", "http://www.w3.org/2000/09/xmldsig#");
				foreach (object obj in xmlElement.Attributes)
				{
					XmlAttribute xmlAttribute = (XmlAttribute)obj;
					string name = xmlAttribute.Name;
					if (name != null)
					{
						if (DataObject.<>f__switch$map4 == null)
						{
							DataObject.<>f__switch$map4 = new Dictionary<string, int>(3)
							{
								{
									"Id",
									0
								},
								{
									"Encoding",
									0
								},
								{
									"MimeType",
									0
								}
							};
						}
						int num;
						if (DataObject.<>f__switch$map4.TryGetValue(name, out num))
						{
							if (num == 0)
							{
								this.element.SetAttribute(xmlAttribute.Name, xmlAttribute.Value);
							}
						}
					}
				}
				foreach (object obj2 in xmlElement.ChildNodes)
				{
					XmlNode node = (XmlNode)obj2;
					this.element.AppendChild(xmlDocument.ImportNode(node, true));
				}
			}
			return this.element;
		}

		// Token: 0x06000146 RID: 326 RVA: 0x00006B0C File Offset: 0x00004D0C
		public void LoadXml(XmlElement value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			this.element = value;
			this.propertyModified = false;
		}

		// Token: 0x040000AB RID: 171
		private XmlElement element;

		// Token: 0x040000AC RID: 172
		private bool propertyModified;
	}
}
