﻿using System;
using System.Runtime.InteropServices;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000047 RID: 71
	public class KeyInfoRetrievalMethod : KeyInfoClause
	{
		// Token: 0x060001F3 RID: 499 RVA: 0x0000958C File Offset: 0x0000778C
		public KeyInfoRetrievalMethod()
		{
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x00009594 File Offset: 0x00007794
		public KeyInfoRetrievalMethod(string strUri)
		{
			this.URI = strUri;
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x000095A4 File Offset: 0x000077A4
		public KeyInfoRetrievalMethod(string strUri, string strType) : this(strUri)
		{
			this.Type = strType;
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x060001F6 RID: 502 RVA: 0x000095B4 File Offset: 0x000077B4
		// (set) Token: 0x060001F7 RID: 503 RVA: 0x000095BC File Offset: 0x000077BC
		[ComVisible(false)]
		public string Type
		{
			get
			{
				return this.type;
			}
			set
			{
				this.element = null;
				this.type = value;
			}
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x060001F8 RID: 504 RVA: 0x000095CC File Offset: 0x000077CC
		// (set) Token: 0x060001F9 RID: 505 RVA: 0x000095D4 File Offset: 0x000077D4
		public string Uri
		{
			get
			{
				return this.URI;
			}
			set
			{
				this.element = null;
				this.URI = value;
			}
		}

		// Token: 0x060001FA RID: 506 RVA: 0x000095E4 File Offset: 0x000077E4
		public override XmlElement GetXml()
		{
			if (this.element != null)
			{
				return this.element;
			}
			XmlDocument xmlDocument = new XmlDocument();
			XmlElement xmlElement = xmlDocument.CreateElement("RetrievalMethod", "http://www.w3.org/2000/09/xmldsig#");
			if (this.URI != null && this.URI.Length > 0)
			{
				xmlElement.SetAttribute("URI", this.URI);
			}
			if (this.Type != null)
			{
				xmlElement.SetAttribute("Type", this.Type);
			}
			return xmlElement;
		}

		// Token: 0x060001FB RID: 507 RVA: 0x00009664 File Offset: 0x00007864
		public override void LoadXml(XmlElement value)
		{
			if (value == null)
			{
				throw new ArgumentNullException();
			}
			if (value.LocalName != "RetrievalMethod" || value.NamespaceURI != "http://www.w3.org/2000/09/xmldsig#")
			{
				this.URI = string.Empty;
			}
			else
			{
				this.URI = value.Attributes["URI"].Value;
				if (value.HasAttribute("Type"))
				{
					this.Type = value.Attributes["Type"].Value;
				}
				this.element = value;
			}
		}

		// Token: 0x040000EC RID: 236
		private string URI;

		// Token: 0x040000ED RID: 237
		private XmlElement element;

		// Token: 0x040000EE RID: 238
		private string type;
	}
}
