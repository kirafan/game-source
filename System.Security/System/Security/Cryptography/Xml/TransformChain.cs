﻿using System;
using System.Collections;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000052 RID: 82
	public class TransformChain
	{
		// Token: 0x060002A7 RID: 679 RVA: 0x0000CC98 File Offset: 0x0000AE98
		public TransformChain()
		{
			this.chain = new ArrayList();
		}

		// Token: 0x170000B5 RID: 181
		// (get) Token: 0x060002A8 RID: 680 RVA: 0x0000CCAC File Offset: 0x0000AEAC
		public int Count
		{
			get
			{
				return this.chain.Count;
			}
		}

		// Token: 0x170000B6 RID: 182
		public Transform this[int index]
		{
			get
			{
				return (Transform)this.chain[index];
			}
		}

		// Token: 0x060002AA RID: 682 RVA: 0x0000CCD0 File Offset: 0x0000AED0
		public void Add(Transform transform)
		{
			this.chain.Add(transform);
		}

		// Token: 0x060002AB RID: 683 RVA: 0x0000CCE0 File Offset: 0x0000AEE0
		public IEnumerator GetEnumerator()
		{
			return this.chain.GetEnumerator();
		}

		// Token: 0x0400012C RID: 300
		private ArrayList chain;
	}
}
