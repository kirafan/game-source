﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000034 RID: 52
	public sealed class CipherData
	{
		// Token: 0x06000128 RID: 296 RVA: 0x000062B4 File Offset: 0x000044B4
		public CipherData()
		{
		}

		// Token: 0x06000129 RID: 297 RVA: 0x000062BC File Offset: 0x000044BC
		public CipherData(byte[] cipherValue)
		{
			this.CipherValue = cipherValue;
		}

		// Token: 0x0600012A RID: 298 RVA: 0x000062CC File Offset: 0x000044CC
		public CipherData(CipherReference cipherReference)
		{
			this.CipherReference = cipherReference;
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600012B RID: 299 RVA: 0x000062DC File Offset: 0x000044DC
		// (set) Token: 0x0600012C RID: 300 RVA: 0x000062E4 File Offset: 0x000044E4
		public CipherReference CipherReference
		{
			get
			{
				return this.cipherReference;
			}
			set
			{
				if (this.CipherValue != null)
				{
					throw new CryptographicException("A Cipher Data element should have either a CipherValue or a CipherReference element.");
				}
				this.cipherReference = value;
			}
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x0600012D RID: 301 RVA: 0x00006304 File Offset: 0x00004504
		// (set) Token: 0x0600012E RID: 302 RVA: 0x0000630C File Offset: 0x0000450C
		public byte[] CipherValue
		{
			get
			{
				return this.cipherValue;
			}
			set
			{
				if (this.CipherReference != null)
				{
					throw new CryptographicException("A Cipher Data element should have either a CipherValue or a CipherReference element.");
				}
				this.cipherValue = value;
			}
		}

		// Token: 0x0600012F RID: 303 RVA: 0x0000632C File Offset: 0x0000452C
		public XmlElement GetXml()
		{
			return this.GetXml(new XmlDocument());
		}

		// Token: 0x06000130 RID: 304 RVA: 0x0000633C File Offset: 0x0000453C
		internal XmlElement GetXml(XmlDocument document)
		{
			if (this.CipherReference == null && this.CipherValue == null)
			{
				throw new CryptographicException("A Cipher Data element should have either a CipherValue or a CipherReference element.");
			}
			XmlElement xmlElement = document.CreateElement("CipherData", "http://www.w3.org/2001/04/xmlenc#");
			if (this.CipherReference != null)
			{
				xmlElement.AppendChild(document.ImportNode(this.cipherReference.GetXml(), true));
			}
			if (this.CipherValue != null)
			{
				XmlElement xmlElement2 = document.CreateElement("CipherValue", "http://www.w3.org/2001/04/xmlenc#");
				StreamReader streamReader = new StreamReader(new CryptoStream(new MemoryStream(this.cipherValue), new ToBase64Transform(), CryptoStreamMode.Read));
				xmlElement2.InnerText = streamReader.ReadToEnd();
				streamReader.Close();
				xmlElement.AppendChild(xmlElement2);
			}
			return xmlElement;
		}

		// Token: 0x06000131 RID: 305 RVA: 0x000063F4 File Offset: 0x000045F4
		public void LoadXml(XmlElement value)
		{
			this.CipherReference = null;
			this.CipherValue = null;
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (value.LocalName != "CipherData" || value.NamespaceURI != "http://www.w3.org/2001/04/xmlenc#")
			{
				throw new CryptographicException("Malformed Cipher Data element.");
			}
			foreach (object obj in value.ChildNodes)
			{
				XmlNode xmlNode = (XmlNode)obj;
				if (!(xmlNode is XmlWhitespace))
				{
					string localName = xmlNode.LocalName;
					if (localName != null)
					{
						if (CipherData.<>f__switch$map1 == null)
						{
							CipherData.<>f__switch$map1 = new Dictionary<string, int>(2)
							{
								{
									"CipherReference",
									0
								},
								{
									"CipherValue",
									1
								}
							};
						}
						int num;
						if (CipherData.<>f__switch$map1.TryGetValue(localName, out num))
						{
							if (num != 0)
							{
								if (num == 1)
								{
									this.CipherValue = Convert.FromBase64String(xmlNode.InnerText);
								}
							}
							else
							{
								this.cipherReference = new CipherReference();
								this.cipherReference.LoadXml((XmlElement)xmlNode);
							}
						}
					}
				}
			}
			if (this.CipherReference == null && this.CipherValue == null)
			{
				throw new CryptographicException("A Cipher Data element should have either a CipherValue or a CipherReference element.");
			}
		}

		// Token: 0x040000A8 RID: 168
		private byte[] cipherValue;

		// Token: 0x040000A9 RID: 169
		private CipherReference cipherReference;
	}
}
