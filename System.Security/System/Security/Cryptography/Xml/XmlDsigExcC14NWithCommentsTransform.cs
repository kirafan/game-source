﻿using System;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x0200005B RID: 91
	public class XmlDsigExcC14NWithCommentsTransform : XmlDsigExcC14NTransform
	{
		// Token: 0x060002FB RID: 763 RVA: 0x0000DDF4 File Offset: 0x0000BFF4
		public XmlDsigExcC14NWithCommentsTransform() : base(true)
		{
		}

		// Token: 0x060002FC RID: 764 RVA: 0x0000DE00 File Offset: 0x0000C000
		public XmlDsigExcC14NWithCommentsTransform(string inclusiveNamespacesPrefixList) : base(true, inclusiveNamespacesPrefixList)
		{
		}
	}
}
