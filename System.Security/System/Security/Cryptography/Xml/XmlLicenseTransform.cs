﻿using System;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000064 RID: 100
	public class XmlLicenseTransform : Transform
	{
		// Token: 0x06000325 RID: 805 RVA: 0x0000E754 File Offset: 0x0000C954
		public XmlLicenseTransform()
		{
			base.Algorithm = "urn:mpeg:mpeg21:2003:01-REL-R-NS:licenseTransform";
		}

		// Token: 0x170000D5 RID: 213
		// (get) Token: 0x06000326 RID: 806 RVA: 0x0000E768 File Offset: 0x0000C968
		// (set) Token: 0x06000327 RID: 807 RVA: 0x0000E770 File Offset: 0x0000C970
		public IRelDecryptor Decryptor
		{
			get
			{
				return this._decryptor;
			}
			set
			{
				this._decryptor = value;
			}
		}

		// Token: 0x170000D6 RID: 214
		// (get) Token: 0x06000328 RID: 808 RVA: 0x0000E77C File Offset: 0x0000C97C
		public override Type[] InputTypes
		{
			get
			{
				if (this.inputTypes == null)
				{
					this.inputTypes = new Type[]
					{
						typeof(XmlDocument)
					};
				}
				return this.inputTypes;
			}
		}

		// Token: 0x170000D7 RID: 215
		// (get) Token: 0x06000329 RID: 809 RVA: 0x0000E7B4 File Offset: 0x0000C9B4
		public override Type[] OutputTypes
		{
			get
			{
				if (this.outputTypes == null)
				{
					this.outputTypes = new Type[]
					{
						typeof(XmlDocument)
					};
				}
				return this.outputTypes;
			}
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000E7EC File Offset: 0x0000C9EC
		[MonoTODO]
		protected override XmlNodeList GetInnerXml()
		{
			return null;
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000E7F0 File Offset: 0x0000C9F0
		[MonoTODO]
		public override object GetOutput()
		{
			return null;
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000E7F4 File Offset: 0x0000C9F4
		public override object GetOutput(Type type)
		{
			if (type != typeof(XmlDocument))
			{
				throw new ArgumentException("type");
			}
			return this.GetOutput();
		}

		// Token: 0x0600032D RID: 813 RVA: 0x0000E818 File Offset: 0x0000CA18
		public override void LoadInnerXml(XmlNodeList nodeList)
		{
		}

		// Token: 0x0600032E RID: 814 RVA: 0x0000E81C File Offset: 0x0000CA1C
		[MonoTODO]
		public override void LoadInput(object obj)
		{
			if (obj != typeof(XmlDocument))
			{
				throw new ArgumentException("obj");
			}
			if (this._decryptor == null)
			{
				throw new CryptographicException(Locale.GetText("missing decryptor"));
			}
		}

		// Token: 0x0400016D RID: 365
		private IRelDecryptor _decryptor;

		// Token: 0x0400016E RID: 366
		private Type[] inputTypes;

		// Token: 0x0400016F RID: 367
		private Type[] outputTypes;
	}
}
