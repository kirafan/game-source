﻿using System;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000035 RID: 53
	public sealed class CipherReference : EncryptedReference
	{
		// Token: 0x06000132 RID: 306 RVA: 0x00006580 File Offset: 0x00004780
		public CipherReference()
		{
		}

		// Token: 0x06000133 RID: 307 RVA: 0x00006588 File Offset: 0x00004788
		public CipherReference(string uri) : base(uri)
		{
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00006594 File Offset: 0x00004794
		public CipherReference(string uri, TransformChain tc) : base(uri, tc)
		{
		}

		// Token: 0x06000135 RID: 309 RVA: 0x000065A0 File Offset: 0x000047A0
		public override XmlElement GetXml()
		{
			return this.GetXml(new XmlDocument());
		}

		// Token: 0x06000136 RID: 310 RVA: 0x000065B0 File Offset: 0x000047B0
		internal override XmlElement GetXml(XmlDocument document)
		{
			XmlElement xmlElement = document.CreateElement("CipherReference", "http://www.w3.org/2001/04/xmlenc#");
			xmlElement.SetAttribute("URI", base.Uri);
			if (base.TransformChain != null && base.TransformChain.Count > 0)
			{
				XmlElement xmlElement2 = document.CreateElement("Transforms", "http://www.w3.org/2001/04/xmlenc#");
				foreach (object obj in base.TransformChain)
				{
					Transform transform = (Transform)obj;
					xmlElement2.AppendChild(document.ImportNode(transform.GetXml(), true));
				}
				xmlElement.AppendChild(xmlElement2);
			}
			return xmlElement;
		}

		// Token: 0x06000137 RID: 311 RVA: 0x00006688 File Offset: 0x00004888
		public override void LoadXml(XmlElement value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (value.LocalName != "CipherReference" || value.NamespaceURI != "http://www.w3.org/2001/04/xmlenc#")
			{
				throw new CryptographicException("Malformed CipherReference element.");
			}
			base.LoadXml(value);
		}
	}
}
