﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;
using Mono.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000057 RID: 87
	public class XmlDsigC14NTransform : Transform
	{
		// Token: 0x060002D7 RID: 727 RVA: 0x0000D594 File Offset: 0x0000B794
		public XmlDsigC14NTransform() : this(false)
		{
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x0000D5A0 File Offset: 0x0000B7A0
		public XmlDsigC14NTransform(bool includeComments)
		{
			if (includeComments)
			{
				base.Algorithm = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments";
			}
			else
			{
				base.Algorithm = "http://www.w3.org/TR/2001/REC-xml-c14n-20010315";
			}
			this.canonicalizer = new XmlCanonicalizer(includeComments, false, base.PropagatedNamespaces);
		}

		// Token: 0x170000C4 RID: 196
		// (get) Token: 0x060002D9 RID: 729 RVA: 0x0000D5E8 File Offset: 0x0000B7E8
		public override Type[] InputTypes
		{
			get
			{
				if (this.input == null)
				{
					this.input = new Type[3];
					this.input[0] = typeof(Stream);
					this.input[1] = typeof(XmlDocument);
					this.input[2] = typeof(XmlNodeList);
				}
				return this.input;
			}
		}

		// Token: 0x170000C5 RID: 197
		// (get) Token: 0x060002DA RID: 730 RVA: 0x0000D648 File Offset: 0x0000B848
		public override Type[] OutputTypes
		{
			get
			{
				if (this.output == null)
				{
					this.output = new Type[1];
					this.output[0] = typeof(Stream);
				}
				return this.output;
			}
		}

		// Token: 0x060002DB RID: 731 RVA: 0x0000D67C File Offset: 0x0000B87C
		protected override XmlNodeList GetInnerXml()
		{
			return null;
		}

		// Token: 0x060002DC RID: 732 RVA: 0x0000D680 File Offset: 0x0000B880
		[ComVisible(false)]
		public override byte[] GetDigestedOutput(HashAlgorithm hash)
		{
			return hash.ComputeHash((Stream)this.GetOutput());
		}

		// Token: 0x060002DD RID: 733 RVA: 0x0000D694 File Offset: 0x0000B894
		public override object GetOutput()
		{
			return this.s;
		}

		// Token: 0x060002DE RID: 734 RVA: 0x0000D69C File Offset: 0x0000B89C
		public override object GetOutput(Type type)
		{
			if (type == typeof(Stream))
			{
				return this.GetOutput();
			}
			throw new ArgumentException("type");
		}

		// Token: 0x060002DF RID: 735 RVA: 0x0000D6C0 File Offset: 0x0000B8C0
		public override void LoadInnerXml(XmlNodeList nodeList)
		{
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x0000D6C4 File Offset: 0x0000B8C4
		public override void LoadInput(object obj)
		{
			Stream stream = obj as Stream;
			if (stream != null)
			{
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.PreserveWhitespace = true;
				xmlDocument.XmlResolver = base.GetResolver();
				xmlDocument.Load(new XmlSignatureStreamReader(new StreamReader(stream)));
				this.s = this.canonicalizer.Canonicalize(xmlDocument);
				return;
			}
			XmlDocument xmlDocument2 = obj as XmlDocument;
			if (xmlDocument2 != null)
			{
				this.s = this.canonicalizer.Canonicalize(xmlDocument2);
				return;
			}
			XmlNodeList xmlNodeList = obj as XmlNodeList;
			if (xmlNodeList != null)
			{
				this.s = this.canonicalizer.Canonicalize(xmlNodeList);
				return;
			}
			throw new ArgumentException("obj");
		}

		// Token: 0x0400013B RID: 315
		private Type[] input;

		// Token: 0x0400013C RID: 316
		private Type[] output;

		// Token: 0x0400013D RID: 317
		private XmlCanonicalizer canonicalizer;

		// Token: 0x0400013E RID: 318
		private Stream s;
	}
}
