﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x0200004C RID: 76
	public sealed class ReferenceList : IEnumerable, IList, ICollection
	{
		// Token: 0x0600022D RID: 557 RVA: 0x0000A590 File Offset: 0x00008790
		public ReferenceList()
		{
			this.list = new ArrayList();
		}

		// Token: 0x17000094 RID: 148
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				this[index] = (EncryptedReference)value;
			}
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x06000230 RID: 560 RVA: 0x0000A5C0 File Offset: 0x000087C0
		bool IList.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x06000231 RID: 561 RVA: 0x0000A5C4 File Offset: 0x000087C4
		bool IList.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x06000232 RID: 562 RVA: 0x0000A5C8 File Offset: 0x000087C8
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x06000233 RID: 563 RVA: 0x0000A5D8 File Offset: 0x000087D8
		public bool IsSynchronized
		{
			get
			{
				return this.list.IsSynchronized;
			}
		}

		// Token: 0x17000099 RID: 153
		[IndexerName("ItemOf")]
		public EncryptedReference this[int index]
		{
			get
			{
				return (EncryptedReference)this.list[index];
			}
			set
			{
				this.list[index] = value;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x06000236 RID: 566 RVA: 0x0000A60C File Offset: 0x0000880C
		public object SyncRoot
		{
			get
			{
				return this.list.SyncRoot;
			}
		}

		// Token: 0x06000237 RID: 567 RVA: 0x0000A61C File Offset: 0x0000881C
		public int Add(object value)
		{
			if (!(value is EncryptedReference))
			{
				throw new ArgumentException("value");
			}
			return this.list.Add(value);
		}

		// Token: 0x06000238 RID: 568 RVA: 0x0000A64C File Offset: 0x0000884C
		public void Clear()
		{
			this.list.Clear();
		}

		// Token: 0x06000239 RID: 569 RVA: 0x0000A65C File Offset: 0x0000885C
		public bool Contains(object value)
		{
			return this.list.Contains(value);
		}

		// Token: 0x0600023A RID: 570 RVA: 0x0000A66C File Offset: 0x0000886C
		public void CopyTo(Array array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x0600023B RID: 571 RVA: 0x0000A67C File Offset: 0x0000887C
		public IEnumerator GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x0600023C RID: 572 RVA: 0x0000A68C File Offset: 0x0000888C
		public EncryptedReference Item(int index)
		{
			return (EncryptedReference)this.list[index];
		}

		// Token: 0x0600023D RID: 573 RVA: 0x0000A6A0 File Offset: 0x000088A0
		public int IndexOf(object value)
		{
			return this.list.IndexOf(value);
		}

		// Token: 0x0600023E RID: 574 RVA: 0x0000A6B0 File Offset: 0x000088B0
		public void Insert(int index, object value)
		{
			if (!(value is EncryptedReference))
			{
				throw new ArgumentException("value");
			}
			this.list.Insert(index, value);
		}

		// Token: 0x0600023F RID: 575 RVA: 0x0000A6D8 File Offset: 0x000088D8
		public void Remove(object value)
		{
			this.list.Remove(value);
		}

		// Token: 0x06000240 RID: 576 RVA: 0x0000A6E8 File Offset: 0x000088E8
		public void RemoveAt(int index)
		{
			this.list.RemoveAt(index);
		}

		// Token: 0x040000FF RID: 255
		private ArrayList list;
	}
}
