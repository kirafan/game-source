﻿using System;
using System.Collections;
using System.IO;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000059 RID: 89
	public class XmlDsigEnvelopedSignatureTransform : Transform
	{
		// Token: 0x060002E2 RID: 738 RVA: 0x0000D778 File Offset: 0x0000B978
		public XmlDsigEnvelopedSignatureTransform() : this(false)
		{
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0000D784 File Offset: 0x0000B984
		public XmlDsigEnvelopedSignatureTransform(bool includeComments)
		{
			base.Algorithm = "http://www.w3.org/2000/09/xmldsig#enveloped-signature";
			this.comments = includeComments;
		}

		// Token: 0x170000C6 RID: 198
		// (get) Token: 0x060002E4 RID: 740 RVA: 0x0000D7A0 File Offset: 0x0000B9A0
		public override Type[] InputTypes
		{
			get
			{
				if (this.input == null)
				{
					this.input = new Type[3];
					this.input[0] = typeof(Stream);
					this.input[1] = typeof(XmlDocument);
					this.input[2] = typeof(XmlNodeList);
				}
				return this.input;
			}
		}

		// Token: 0x170000C7 RID: 199
		// (get) Token: 0x060002E5 RID: 741 RVA: 0x0000D800 File Offset: 0x0000BA00
		public override Type[] OutputTypes
		{
			get
			{
				if (this.output == null)
				{
					this.output = new Type[2];
					this.output[0] = typeof(XmlDocument);
					this.output[1] = typeof(XmlNodeList);
				}
				return this.output;
			}
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x0000D850 File Offset: 0x0000BA50
		protected override XmlNodeList GetInnerXml()
		{
			return null;
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x0000D854 File Offset: 0x0000BA54
		public override object GetOutput()
		{
			if (this.inputObj is Stream)
			{
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.PreserveWhitespace = true;
				xmlDocument.XmlResolver = base.GetResolver();
				xmlDocument.Load(new XmlSignatureStreamReader(new StreamReader(this.inputObj as Stream)));
				return this.GetOutputFromNode(xmlDocument, this.GetNamespaceManager(xmlDocument), true);
			}
			if (this.inputObj is XmlDocument)
			{
				XmlDocument xmlDocument = this.inputObj as XmlDocument;
				return this.GetOutputFromNode(xmlDocument, this.GetNamespaceManager(xmlDocument), true);
			}
			if (this.inputObj is XmlNodeList)
			{
				ArrayList arrayList = new ArrayList();
				XmlNodeList xmlNodeList = (XmlNodeList)this.inputObj;
				if (xmlNodeList.Count > 0)
				{
					XmlNamespaceManager namespaceManager = this.GetNamespaceManager(xmlNodeList.Item(0));
					ArrayList arrayList2 = new ArrayList();
					foreach (object obj in xmlNodeList)
					{
						XmlNode value = (XmlNode)obj;
						arrayList2.Add(value);
					}
					foreach (object obj2 in arrayList2)
					{
						XmlNode xmlNode = (XmlNode)obj2;
						if (xmlNode.SelectNodes("ancestor-or-self::dsig:Signature", namespaceManager).Count == 0)
						{
							arrayList.Add(this.GetOutputFromNode(xmlNode, namespaceManager, false));
						}
					}
				}
				return new XmlDsigNodeList(arrayList);
			}
			if (this.inputObj is XmlElement)
			{
				XmlElement xmlElement = this.inputObj as XmlElement;
				XmlNamespaceManager namespaceManager2 = this.GetNamespaceManager(xmlElement);
				if (xmlElement.SelectNodes("ancestor-or-self::dsig:Signature", namespaceManager2).Count == 0)
				{
					return this.GetOutputFromNode(xmlElement, namespaceManager2, true);
				}
			}
			throw new NullReferenceException();
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x0000DA68 File Offset: 0x0000BC68
		private XmlNamespaceManager GetNamespaceManager(XmlNode n)
		{
			XmlDocument xmlDocument = (!(n is XmlDocument)) ? n.OwnerDocument : (n as XmlDocument);
			XmlNamespaceManager xmlNamespaceManager = new XmlNamespaceManager(xmlDocument.NameTable);
			xmlNamespaceManager.AddNamespace("dsig", "http://www.w3.org/2000/09/xmldsig#");
			return xmlNamespaceManager;
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x0000DAB0 File Offset: 0x0000BCB0
		private XmlNode GetOutputFromNode(XmlNode input, XmlNamespaceManager nsmgr, bool remove)
		{
			if (remove)
			{
				XmlNodeList xmlNodeList = input.SelectNodes("descendant-or-self::dsig:Signature", nsmgr);
				ArrayList arrayList = new ArrayList();
				foreach (object obj in xmlNodeList)
				{
					XmlNode value = (XmlNode)obj;
					arrayList.Add(value);
				}
				foreach (object obj2 in arrayList)
				{
					XmlNode xmlNode = (XmlNode)obj2;
					xmlNode.ParentNode.RemoveChild(xmlNode);
				}
			}
			return input;
		}

		// Token: 0x060002EA RID: 746 RVA: 0x0000DBA4 File Offset: 0x0000BDA4
		public override object GetOutput(Type type)
		{
			if (type == typeof(Stream))
			{
				return this.GetOutput();
			}
			throw new ArgumentException("type");
		}

		// Token: 0x060002EB RID: 747 RVA: 0x0000DBC8 File Offset: 0x0000BDC8
		public override void LoadInnerXml(XmlNodeList nodeList)
		{
		}

		// Token: 0x060002EC RID: 748 RVA: 0x0000DBCC File Offset: 0x0000BDCC
		public override void LoadInput(object obj)
		{
			this.inputObj = obj;
		}

		// Token: 0x0400013F RID: 319
		private Type[] input;

		// Token: 0x04000140 RID: 320
		private Type[] output;

		// Token: 0x04000141 RID: 321
		private bool comments;

		// Token: 0x04000142 RID: 322
		private object inputObj;
	}
}
