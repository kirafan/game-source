﻿using System;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000058 RID: 88
	public class XmlDsigC14NWithCommentsTransform : XmlDsigC14NTransform
	{
		// Token: 0x060002E1 RID: 737 RVA: 0x0000D76C File Offset: 0x0000B96C
		public XmlDsigC14NWithCommentsTransform() : base(true)
		{
		}
	}
}
