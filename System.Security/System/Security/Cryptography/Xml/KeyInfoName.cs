﻿using System;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000045 RID: 69
	public class KeyInfoName : KeyInfoClause
	{
		// Token: 0x060001E7 RID: 487 RVA: 0x0000948C File Offset: 0x0000768C
		public KeyInfoName()
		{
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x00009494 File Offset: 0x00007694
		public KeyInfoName(string keyName)
		{
			this.name = keyName;
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x060001E9 RID: 489 RVA: 0x000094A4 File Offset: 0x000076A4
		// (set) Token: 0x060001EA RID: 490 RVA: 0x000094AC File Offset: 0x000076AC
		public string Value
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x060001EB RID: 491 RVA: 0x000094B8 File Offset: 0x000076B8
		public override XmlElement GetXml()
		{
			XmlDocument xmlDocument = new XmlDocument();
			XmlElement xmlElement = xmlDocument.CreateElement("KeyName", "http://www.w3.org/2000/09/xmldsig#");
			xmlElement.InnerText = this.name;
			return xmlElement;
		}

		// Token: 0x060001EC RID: 492 RVA: 0x000094EC File Offset: 0x000076EC
		public override void LoadXml(XmlElement value)
		{
			if (value == null)
			{
				throw new ArgumentNullException();
			}
			if (value.LocalName != "KeyName" || value.NamespaceURI != "http://www.w3.org/2000/09/xmldsig#")
			{
				this.name = string.Empty;
			}
			else
			{
				this.name = value.InnerText;
			}
		}

		// Token: 0x040000EA RID: 234
		private string name;
	}
}
