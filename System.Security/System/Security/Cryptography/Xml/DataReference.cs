﻿using System;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000037 RID: 55
	public sealed class DataReference : EncryptedReference
	{
		// Token: 0x06000147 RID: 327 RVA: 0x00006B30 File Offset: 0x00004D30
		public DataReference()
		{
			base.ReferenceType = "DataReference";
		}

		// Token: 0x06000148 RID: 328 RVA: 0x00006B44 File Offset: 0x00004D44
		public DataReference(string uri) : base(uri)
		{
			base.ReferenceType = "DataReference";
		}

		// Token: 0x06000149 RID: 329 RVA: 0x00006B58 File Offset: 0x00004D58
		public DataReference(string uri, TransformChain tc) : base(uri, tc)
		{
			base.ReferenceType = "DataReference";
		}
	}
}
