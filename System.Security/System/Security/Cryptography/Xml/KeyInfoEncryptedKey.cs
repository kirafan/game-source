﻿using System;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000044 RID: 68
	public class KeyInfoEncryptedKey : KeyInfoClause
	{
		// Token: 0x060001E0 RID: 480 RVA: 0x00009418 File Offset: 0x00007618
		public KeyInfoEncryptedKey()
		{
		}

		// Token: 0x060001E1 RID: 481 RVA: 0x00009420 File Offset: 0x00007620
		public KeyInfoEncryptedKey(EncryptedKey ek)
		{
			this.EncryptedKey = ek;
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x00009430 File Offset: 0x00007630
		// (set) Token: 0x060001E3 RID: 483 RVA: 0x00009438 File Offset: 0x00007638
		public EncryptedKey EncryptedKey
		{
			get
			{
				return this.encryptedKey;
			}
			set
			{
				this.encryptedKey = value;
			}
		}

		// Token: 0x060001E4 RID: 484 RVA: 0x00009444 File Offset: 0x00007644
		public override XmlElement GetXml()
		{
			return this.GetXml(new XmlDocument());
		}

		// Token: 0x060001E5 RID: 485 RVA: 0x00009454 File Offset: 0x00007654
		internal XmlElement GetXml(XmlDocument document)
		{
			if (this.encryptedKey != null)
			{
				return this.encryptedKey.GetXml(document);
			}
			return null;
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x00009470 File Offset: 0x00007670
		public override void LoadXml(XmlElement value)
		{
			this.EncryptedKey = new EncryptedKey();
			this.EncryptedKey.LoadXml(value);
		}

		// Token: 0x040000E9 RID: 233
		private EncryptedKey encryptedKey;
	}
}
