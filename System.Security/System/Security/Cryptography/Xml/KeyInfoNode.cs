﻿using System;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000046 RID: 70
	public class KeyInfoNode : KeyInfoClause
	{
		// Token: 0x060001ED RID: 493 RVA: 0x0000954C File Offset: 0x0000774C
		public KeyInfoNode()
		{
		}

		// Token: 0x060001EE RID: 494 RVA: 0x00009554 File Offset: 0x00007754
		public KeyInfoNode(XmlElement node)
		{
			this.LoadXml(node);
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x060001EF RID: 495 RVA: 0x00009564 File Offset: 0x00007764
		// (set) Token: 0x060001F0 RID: 496 RVA: 0x0000956C File Offset: 0x0000776C
		public XmlElement Value
		{
			get
			{
				return this.Node;
			}
			set
			{
				this.Node = value;
			}
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x00009578 File Offset: 0x00007778
		public override XmlElement GetXml()
		{
			return this.Node;
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x00009580 File Offset: 0x00007780
		public override void LoadXml(XmlElement value)
		{
			this.Node = value;
		}

		// Token: 0x040000EB RID: 235
		private XmlElement Node;
	}
}
