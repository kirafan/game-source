﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x0200003E RID: 62
	public class EncryptionMethod
	{
		// Token: 0x060001A7 RID: 423 RVA: 0x00008AA4 File Offset: 0x00006CA4
		public EncryptionMethod()
		{
			this.KeyAlgorithm = null;
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x00008AB4 File Offset: 0x00006CB4
		public EncryptionMethod(string strAlgorithm)
		{
			this.KeyAlgorithm = strAlgorithm;
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x060001A9 RID: 425 RVA: 0x00008AC4 File Offset: 0x00006CC4
		// (set) Token: 0x060001AA RID: 426 RVA: 0x00008ACC File Offset: 0x00006CCC
		public string KeyAlgorithm
		{
			get
			{
				return this.algorithm;
			}
			set
			{
				this.algorithm = value;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x060001AB RID: 427 RVA: 0x00008AD8 File Offset: 0x00006CD8
		// (set) Token: 0x060001AC RID: 428 RVA: 0x00008AE0 File Offset: 0x00006CE0
		public int KeySize
		{
			get
			{
				return this.keySize;
			}
			set
			{
				if (value <= 0)
				{
					throw new ArgumentOutOfRangeException("The key size should be a non negative integer.");
				}
				this.keySize = value;
			}
		}

		// Token: 0x060001AD RID: 429 RVA: 0x00008AFC File Offset: 0x00006CFC
		public XmlElement GetXml()
		{
			return this.GetXml(new XmlDocument());
		}

		// Token: 0x060001AE RID: 430 RVA: 0x00008B0C File Offset: 0x00006D0C
		internal XmlElement GetXml(XmlDocument document)
		{
			XmlElement xmlElement = document.CreateElement("EncryptionMethod", "http://www.w3.org/2001/04/xmlenc#");
			if (this.KeySize != 0)
			{
				XmlElement xmlElement2 = document.CreateElement("KeySize", "http://www.w3.org/2001/04/xmlenc#");
				xmlElement2.InnerText = string.Format("{0}", this.keySize);
				xmlElement.AppendChild(xmlElement2);
			}
			if (this.KeyAlgorithm != null)
			{
				xmlElement.SetAttribute("Algorithm", this.KeyAlgorithm);
			}
			return xmlElement;
		}

		// Token: 0x060001AF RID: 431 RVA: 0x00008B88 File Offset: 0x00006D88
		public void LoadXml(XmlElement value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (value.LocalName != "EncryptionMethod" || value.NamespaceURI != "http://www.w3.org/2001/04/xmlenc#")
			{
				throw new CryptographicException("Malformed EncryptionMethod element.");
			}
			this.KeyAlgorithm = null;
			foreach (object obj in value.ChildNodes)
			{
				XmlNode xmlNode = (XmlNode)obj;
				if (!(xmlNode is XmlWhitespace))
				{
					string localName = xmlNode.LocalName;
					if (localName != null)
					{
						if (EncryptionMethod.<>f__switch$mapA == null)
						{
							EncryptionMethod.<>f__switch$mapA = new Dictionary<string, int>(1)
							{
								{
									"KeySize",
									0
								}
							};
						}
						int num;
						if (EncryptionMethod.<>f__switch$mapA.TryGetValue(localName, out num))
						{
							if (num == 0)
							{
								this.KeySize = int.Parse(xmlNode.InnerText);
							}
						}
					}
				}
			}
			if (value.HasAttribute("Algorithm"))
			{
				this.KeyAlgorithm = value.Attributes["Algorithm"].Value;
			}
		}

		// Token: 0x040000DE RID: 222
		private string algorithm;

		// Token: 0x040000DF RID: 223
		private int keySize;
	}
}
