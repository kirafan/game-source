﻿using System;
using System.IO;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000041 RID: 65
	public interface IRelDecryptor
	{
		// Token: 0x060001D3 RID: 467
		Stream Decrypt(EncryptionMethod encryptionMethod, KeyInfo keyInfo, Stream toDecrypt);
	}
}
