﻿using System;
using System.Collections;
using System.Runtime.CompilerServices;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x0200003F RID: 63
	public sealed class EncryptionPropertyCollection : IEnumerable, IList, ICollection
	{
		// Token: 0x060001B0 RID: 432 RVA: 0x00008CE0 File Offset: 0x00006EE0
		public EncryptionPropertyCollection()
		{
			this.list = new ArrayList();
		}

		// Token: 0x17000076 RID: 118
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				this[index] = (EncryptionProperty)value;
			}
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x00008D10 File Offset: 0x00006F10
		bool IList.Contains(object value)
		{
			return this.Contains((EncryptionProperty)value);
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x00008D20 File Offset: 0x00006F20
		int IList.Add(object value)
		{
			return this.Add((EncryptionProperty)value);
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x00008D30 File Offset: 0x00006F30
		int IList.IndexOf(object value)
		{
			return this.IndexOf((EncryptionProperty)value);
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00008D40 File Offset: 0x00006F40
		void IList.Insert(int index, object value)
		{
			this.Insert(index, (EncryptionProperty)value);
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x00008D50 File Offset: 0x00006F50
		void IList.Remove(object value)
		{
			this.Remove((EncryptionProperty)value);
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x060001B8 RID: 440 RVA: 0x00008D60 File Offset: 0x00006F60
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x060001B9 RID: 441 RVA: 0x00008D70 File Offset: 0x00006F70
		public bool IsFixedSize
		{
			get
			{
				return this.list.IsFixedSize;
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x060001BA RID: 442 RVA: 0x00008D80 File Offset: 0x00006F80
		public bool IsReadOnly
		{
			get
			{
				return this.list.IsReadOnly;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x060001BB RID: 443 RVA: 0x00008D90 File Offset: 0x00006F90
		public bool IsSynchronized
		{
			get
			{
				return this.list.IsSynchronized;
			}
		}

		// Token: 0x1700007B RID: 123
		[IndexerName("ItemOf")]
		public EncryptionProperty this[int index]
		{
			get
			{
				return (EncryptionProperty)this.list[index];
			}
			set
			{
				this.list[index] = value;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x060001BE RID: 446 RVA: 0x00008DC4 File Offset: 0x00006FC4
		public object SyncRoot
		{
			get
			{
				return this.list.SyncRoot;
			}
		}

		// Token: 0x060001BF RID: 447 RVA: 0x00008DD4 File Offset: 0x00006FD4
		public int Add(EncryptionProperty value)
		{
			return this.list.Add(value);
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x00008DE4 File Offset: 0x00006FE4
		public void Clear()
		{
			this.list.Clear();
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x00008DF4 File Offset: 0x00006FF4
		public bool Contains(EncryptionProperty value)
		{
			return this.list.Contains(value);
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x00008E04 File Offset: 0x00007004
		public void CopyTo(Array array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x00008E14 File Offset: 0x00007014
		public void CopyTo(EncryptionProperty[] array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x00008E24 File Offset: 0x00007024
		public IEnumerator GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x00008E34 File Offset: 0x00007034
		public int IndexOf(EncryptionProperty value)
		{
			return this.list.IndexOf(value);
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x00008E44 File Offset: 0x00007044
		public void Insert(int index, EncryptionProperty value)
		{
			this.list.Insert(index, value);
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x00008E54 File Offset: 0x00007054
		public EncryptionProperty Item(int index)
		{
			return (EncryptionProperty)this.list[index];
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x00008E68 File Offset: 0x00007068
		public void Remove(EncryptionProperty value)
		{
			this.list.Remove(value);
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x00008E78 File Offset: 0x00007078
		public void RemoveAt(int index)
		{
			this.list.RemoveAt(index);
		}

		// Token: 0x040000E1 RID: 225
		private ArrayList list;
	}
}
