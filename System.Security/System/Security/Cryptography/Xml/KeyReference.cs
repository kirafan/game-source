﻿using System;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000049 RID: 73
	public sealed class KeyReference : EncryptedReference
	{
		// Token: 0x0600020E RID: 526 RVA: 0x00009EB8 File Offset: 0x000080B8
		public KeyReference()
		{
			base.ReferenceType = "KeyReference";
		}

		// Token: 0x0600020F RID: 527 RVA: 0x00009ECC File Offset: 0x000080CC
		public KeyReference(string uri) : base(uri)
		{
			base.ReferenceType = "KeyReference";
		}

		// Token: 0x06000210 RID: 528 RVA: 0x00009EE0 File Offset: 0x000080E0
		public KeyReference(string uri, TransformChain tc) : base(uri, tc)
		{
			base.ReferenceType = "KeyReference";
		}
	}
}
