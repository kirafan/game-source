﻿using System;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000038 RID: 56
	public class DSAKeyValue : KeyInfoClause
	{
		// Token: 0x0600014A RID: 330 RVA: 0x00006B70 File Offset: 0x00004D70
		public DSAKeyValue()
		{
			this.dsa = DSA.Create();
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00006B84 File Offset: 0x00004D84
		public DSAKeyValue(DSA key)
		{
			this.dsa = key;
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x0600014C RID: 332 RVA: 0x00006B94 File Offset: 0x00004D94
		// (set) Token: 0x0600014D RID: 333 RVA: 0x00006B9C File Offset: 0x00004D9C
		public DSA Key
		{
			get
			{
				return this.dsa;
			}
			set
			{
				this.dsa = value;
			}
		}

		// Token: 0x0600014E RID: 334 RVA: 0x00006BA8 File Offset: 0x00004DA8
		public override XmlElement GetXml()
		{
			XmlDocument xmlDocument = new XmlDocument();
			XmlElement xmlElement = xmlDocument.CreateElement("KeyValue", "http://www.w3.org/2000/09/xmldsig#");
			xmlElement.SetAttribute("xmlns", "http://www.w3.org/2000/09/xmldsig#");
			xmlElement.InnerXml = this.dsa.ToXmlString(false);
			return xmlElement;
		}

		// Token: 0x0600014F RID: 335 RVA: 0x00006BF0 File Offset: 0x00004DF0
		public override void LoadXml(XmlElement value)
		{
			if (value == null)
			{
				throw new ArgumentNullException();
			}
			if (value.LocalName != "KeyValue" || value.NamespaceURI != "http://www.w3.org/2000/09/xmldsig#")
			{
				throw new CryptographicException("value");
			}
			this.dsa.FromXmlString(value.InnerXml);
		}

		// Token: 0x040000AE RID: 174
		private DSA dsa;
	}
}
