﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Security.Policy;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000053 RID: 83
	public abstract class Transform
	{
		// Token: 0x060002AC RID: 684 RVA: 0x0000CCF0 File Offset: 0x0000AEF0
		protected Transform()
		{
			if (SecurityManager.SecurityEnabled)
			{
				this.xmlResolver = new XmlSecureResolver(new XmlUrlResolver(), new Evidence());
			}
			else
			{
				this.xmlResolver = new XmlUrlResolver();
			}
		}

		// Token: 0x170000B7 RID: 183
		// (get) Token: 0x060002AD RID: 685 RVA: 0x0000CD40 File Offset: 0x0000AF40
		// (set) Token: 0x060002AE RID: 686 RVA: 0x0000CD48 File Offset: 0x0000AF48
		public string Algorithm
		{
			get
			{
				return this.algo;
			}
			set
			{
				this.algo = value;
			}
		}

		// Token: 0x170000B8 RID: 184
		// (get) Token: 0x060002AF RID: 687
		public abstract Type[] InputTypes { get; }

		// Token: 0x170000B9 RID: 185
		// (get) Token: 0x060002B0 RID: 688
		public abstract Type[] OutputTypes { get; }

		// Token: 0x170000BA RID: 186
		// (set) Token: 0x060002B1 RID: 689 RVA: 0x0000CD54 File Offset: 0x0000AF54
		[ComVisible(false)]
		public XmlResolver Resolver
		{
			set
			{
				this.xmlResolver = value;
			}
		}

		// Token: 0x170000BB RID: 187
		// (get) Token: 0x060002B2 RID: 690 RVA: 0x0000CD60 File Offset: 0x0000AF60
		// (set) Token: 0x060002B3 RID: 691 RVA: 0x0000CD68 File Offset: 0x0000AF68
		[ComVisible(false)]
		[MonoTODO]
		public XmlElement Context
		{
			get
			{
				throw new NotImplementedException();
			}
			set
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170000BC RID: 188
		// (get) Token: 0x060002B4 RID: 692 RVA: 0x0000CD70 File Offset: 0x0000AF70
		[ComVisible(false)]
		public Hashtable PropagatedNamespaces
		{
			get
			{
				return this.propagated_namespaces;
			}
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x0000CD78 File Offset: 0x0000AF78
		[ComVisible(false)]
		public virtual byte[] GetDigestedOutput(HashAlgorithm hash)
		{
			return hash.ComputeHash((Stream)this.GetOutput(typeof(Stream)));
		}

		// Token: 0x060002B6 RID: 694
		protected abstract XmlNodeList GetInnerXml();

		// Token: 0x060002B7 RID: 695
		public abstract object GetOutput();

		// Token: 0x060002B8 RID: 696
		public abstract object GetOutput(Type type);

		// Token: 0x060002B9 RID: 697 RVA: 0x0000CD98 File Offset: 0x0000AF98
		public XmlElement GetXml()
		{
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.XmlResolver = this.GetResolver();
			XmlElement xmlElement = xmlDocument.CreateElement("Transform", "http://www.w3.org/2000/09/xmldsig#");
			xmlElement.SetAttribute("Algorithm", this.algo);
			XmlNodeList innerXml = this.GetInnerXml();
			if (innerXml != null)
			{
				foreach (object obj in innerXml)
				{
					XmlNode node = (XmlNode)obj;
					XmlNode newChild = xmlDocument.ImportNode(node, true);
					xmlElement.AppendChild(newChild);
				}
			}
			return xmlElement;
		}

		// Token: 0x060002BA RID: 698
		public abstract void LoadInnerXml(XmlNodeList nodeList);

		// Token: 0x060002BB RID: 699
		public abstract void LoadInput(object obj);

		// Token: 0x060002BC RID: 700 RVA: 0x0000CE58 File Offset: 0x0000B058
		internal XmlResolver GetResolver()
		{
			return this.xmlResolver;
		}

		// Token: 0x0400012D RID: 301
		private string algo;

		// Token: 0x0400012E RID: 302
		private XmlResolver xmlResolver;

		// Token: 0x0400012F RID: 303
		private Hashtable propagated_namespaces = new Hashtable();
	}
}
