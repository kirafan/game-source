﻿using System;
using System.IO;
using System.Xml;
using Mono.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x0200005A RID: 90
	public class XmlDsigExcC14NTransform : Transform
	{
		// Token: 0x060002ED RID: 749 RVA: 0x0000DBD8 File Offset: 0x0000BDD8
		public XmlDsigExcC14NTransform() : this(false, null)
		{
		}

		// Token: 0x060002EE RID: 750 RVA: 0x0000DBE4 File Offset: 0x0000BDE4
		public XmlDsigExcC14NTransform(bool includeComments) : this(includeComments, null)
		{
		}

		// Token: 0x060002EF RID: 751 RVA: 0x0000DBF0 File Offset: 0x0000BDF0
		public XmlDsigExcC14NTransform(string inclusiveNamespacesPrefixList) : this(false, inclusiveNamespacesPrefixList)
		{
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x0000DBFC File Offset: 0x0000BDFC
		public XmlDsigExcC14NTransform(bool includeComments, string inclusiveNamespacesPrefixList)
		{
			if (includeComments)
			{
				base.Algorithm = "http://www.w3.org/2001/10/xml-exc-c14n#WithComments";
			}
			else
			{
				base.Algorithm = "http://www.w3.org/2001/10/xml-exc-c14n#";
			}
			this.inclusiveNamespacesPrefixList = inclusiveNamespacesPrefixList;
			this.canonicalizer = new XmlCanonicalizer(includeComments, true, base.PropagatedNamespaces);
		}

		// Token: 0x170000C8 RID: 200
		// (get) Token: 0x060002F1 RID: 753 RVA: 0x0000DC4C File Offset: 0x0000BE4C
		// (set) Token: 0x060002F2 RID: 754 RVA: 0x0000DC54 File Offset: 0x0000BE54
		public string InclusiveNamespacesPrefixList
		{
			get
			{
				return this.inclusiveNamespacesPrefixList;
			}
			set
			{
				this.inclusiveNamespacesPrefixList = value;
			}
		}

		// Token: 0x170000C9 RID: 201
		// (get) Token: 0x060002F3 RID: 755 RVA: 0x0000DC60 File Offset: 0x0000BE60
		public override Type[] InputTypes
		{
			get
			{
				if (this.input == null)
				{
					this.input = new Type[3];
					this.input[0] = typeof(Stream);
					this.input[1] = typeof(XmlDocument);
					this.input[2] = typeof(XmlNodeList);
				}
				return this.input;
			}
		}

		// Token: 0x170000CA RID: 202
		// (get) Token: 0x060002F4 RID: 756 RVA: 0x0000DCC0 File Offset: 0x0000BEC0
		public override Type[] OutputTypes
		{
			get
			{
				if (this.output == null)
				{
					this.output = new Type[1];
					this.output[0] = typeof(Stream);
				}
				return this.output;
			}
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x0000DCF4 File Offset: 0x0000BEF4
		protected override XmlNodeList GetInnerXml()
		{
			return null;
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x0000DCF8 File Offset: 0x0000BEF8
		public override byte[] GetDigestedOutput(HashAlgorithm hash)
		{
			return hash.ComputeHash((Stream)this.GetOutput());
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x0000DD0C File Offset: 0x0000BF0C
		public override object GetOutput()
		{
			return this.s;
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x0000DD14 File Offset: 0x0000BF14
		public override object GetOutput(Type type)
		{
			if (type == typeof(Stream))
			{
				return this.GetOutput();
			}
			throw new ArgumentException("type");
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x0000DD38 File Offset: 0x0000BF38
		public override void LoadInnerXml(XmlNodeList nodeList)
		{
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000DD3C File Offset: 0x0000BF3C
		public override void LoadInput(object obj)
		{
			this.canonicalizer.InclusiveNamespacesPrefixList = this.InclusiveNamespacesPrefixList;
			Stream stream = obj as Stream;
			if (stream != null)
			{
				XmlDocument xmlDocument = new XmlDocument();
				xmlDocument.PreserveWhitespace = true;
				xmlDocument.XmlResolver = base.GetResolver();
				xmlDocument.Load(new XmlSignatureStreamReader(new StreamReader(stream)));
				this.s = this.canonicalizer.Canonicalize(xmlDocument);
				return;
			}
			XmlDocument xmlDocument2 = obj as XmlDocument;
			if (xmlDocument2 != null)
			{
				this.s = this.canonicalizer.Canonicalize(xmlDocument2);
				return;
			}
			XmlNodeList xmlNodeList = obj as XmlNodeList;
			if (xmlNodeList != null)
			{
				this.s = this.canonicalizer.Canonicalize(xmlNodeList);
				return;
			}
			throw new ArgumentException("obj");
		}

		// Token: 0x04000143 RID: 323
		private Type[] input;

		// Token: 0x04000144 RID: 324
		private Type[] output;

		// Token: 0x04000145 RID: 325
		private XmlCanonicalizer canonicalizer;

		// Token: 0x04000146 RID: 326
		private Stream s;

		// Token: 0x04000147 RID: 327
		private string inclusiveNamespacesPrefixList;
	}
}
