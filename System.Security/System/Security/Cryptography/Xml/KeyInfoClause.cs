﻿using System;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x02000042 RID: 66
	public abstract class KeyInfoClause
	{
		// Token: 0x060001D5 RID: 469
		public abstract XmlElement GetXml();

		// Token: 0x060001D6 RID: 470
		public abstract void LoadXml(XmlElement element);
	}
}
