﻿using System;
using System.Xml;

namespace System.Security.Cryptography.Xml
{
	// Token: 0x0200004D RID: 77
	public class RSAKeyValue : KeyInfoClause
	{
		// Token: 0x06000241 RID: 577 RVA: 0x0000A6F8 File Offset: 0x000088F8
		public RSAKeyValue()
		{
			this.rsa = RSA.Create();
		}

		// Token: 0x06000242 RID: 578 RVA: 0x0000A70C File Offset: 0x0000890C
		public RSAKeyValue(RSA key)
		{
			this.rsa = key;
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x06000243 RID: 579 RVA: 0x0000A71C File Offset: 0x0000891C
		// (set) Token: 0x06000244 RID: 580 RVA: 0x0000A724 File Offset: 0x00008924
		public RSA Key
		{
			get
			{
				return this.rsa;
			}
			set
			{
				this.rsa = value;
			}
		}

		// Token: 0x06000245 RID: 581 RVA: 0x0000A730 File Offset: 0x00008930
		public override XmlElement GetXml()
		{
			XmlDocument xmlDocument = new XmlDocument();
			XmlElement xmlElement = xmlDocument.CreateElement("KeyValue", "http://www.w3.org/2000/09/xmldsig#");
			xmlElement.SetAttribute("xmlns", "http://www.w3.org/2000/09/xmldsig#");
			xmlElement.InnerXml = this.rsa.ToXmlString(false);
			return xmlElement;
		}

		// Token: 0x06000246 RID: 582 RVA: 0x0000A778 File Offset: 0x00008978
		public override void LoadXml(XmlElement value)
		{
			if (value == null)
			{
				throw new ArgumentNullException();
			}
			if (value.LocalName != "KeyValue" || value.NamespaceURI != "http://www.w3.org/2000/09/xmldsig#")
			{
				throw new CryptographicException("value");
			}
			this.rsa.FromXmlString(value.InnerXml);
		}

		// Token: 0x04000100 RID: 256
		private RSA rsa;
	}
}
