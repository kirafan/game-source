﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200000C RID: 12
	public sealed class CryptographicAttributeObject
	{
		// Token: 0x0600002C RID: 44 RVA: 0x00003E54 File Offset: 0x00002054
		public CryptographicAttributeObject(Oid oid)
		{
			if (oid == null)
			{
				throw new ArgumentNullException("oid");
			}
			this._oid = new Oid(oid);
			this._list = new AsnEncodedDataCollection();
		}

		// Token: 0x0600002D RID: 45 RVA: 0x00003E90 File Offset: 0x00002090
		public CryptographicAttributeObject(Oid oid, AsnEncodedDataCollection values)
		{
			if (oid == null)
			{
				throw new ArgumentNullException("oid");
			}
			this._oid = new Oid(oid);
			if (values == null)
			{
				this._list = new AsnEncodedDataCollection();
			}
			else
			{
				this._list = values;
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x0600002E RID: 46 RVA: 0x00003EE0 File Offset: 0x000020E0
		public Oid Oid
		{
			get
			{
				return this._oid;
			}
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x0600002F RID: 47 RVA: 0x00003EE8 File Offset: 0x000020E8
		public AsnEncodedDataCollection Values
		{
			get
			{
				return this._list;
			}
		}

		// Token: 0x04000036 RID: 54
		private Oid _oid;

		// Token: 0x04000037 RID: 55
		private AsnEncodedDataCollection _list;
	}
}
