﻿using System;
using System.Collections;

namespace System.Security.Cryptography
{
	// Token: 0x0200000E RID: 14
	public sealed class CryptographicAttributeObjectEnumerator : IEnumerator
	{
		// Token: 0x0600003D RID: 61 RVA: 0x000040B4 File Offset: 0x000022B4
		internal CryptographicAttributeObjectEnumerator(IEnumerable enumerable)
		{
			this.enumerator = enumerable.GetEnumerator();
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x0600003E RID: 62 RVA: 0x000040C8 File Offset: 0x000022C8
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x0600003F RID: 63 RVA: 0x000040D8 File Offset: 0x000022D8
		public CryptographicAttributeObject Current
		{
			get
			{
				return (CryptographicAttributeObject)this.enumerator.Current;
			}
		}

		// Token: 0x06000040 RID: 64 RVA: 0x000040EC File Offset: 0x000022EC
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		// Token: 0x06000041 RID: 65 RVA: 0x000040FC File Offset: 0x000022FC
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x04000039 RID: 57
		private IEnumerator enumerator;
	}
}
