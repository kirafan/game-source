﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000010 RID: 16
	public enum MemoryProtectionScope
	{
		// Token: 0x0400003E RID: 62
		SameProcess,
		// Token: 0x0400003F RID: 63
		CrossProcess,
		// Token: 0x04000040 RID: 64
		SameLogon
	}
}
