﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000019 RID: 25
	[MonoTODO]
	public sealed class KeyAgreeRecipientInfo : RecipientInfo
	{
		// Token: 0x06000074 RID: 116 RVA: 0x00004D38 File Offset: 0x00002F38
		internal KeyAgreeRecipientInfo() : base(RecipientInfoType.KeyAgreement)
		{
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x06000075 RID: 117 RVA: 0x00004D44 File Offset: 0x00002F44
		public DateTime Date
		{
			get
			{
				return DateTime.MinValue;
			}
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x06000076 RID: 118 RVA: 0x00004D4C File Offset: 0x00002F4C
		public override byte[] EncryptedKey
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000017 RID: 23
		// (get) Token: 0x06000077 RID: 119 RVA: 0x00004D50 File Offset: 0x00002F50
		public override AlgorithmIdentifier KeyEncryptionAlgorithm
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x06000078 RID: 120 RVA: 0x00004D54 File Offset: 0x00002F54
		public SubjectIdentifierOrKey OriginatorIdentifierOrKey
		{
			get
			{
				return null;
			}
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x06000079 RID: 121 RVA: 0x00004D58 File Offset: 0x00002F58
		public CryptographicAttributeObject OtherKeyAttribute
		{
			get
			{
				return null;
			}
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x0600007A RID: 122 RVA: 0x00004D5C File Offset: 0x00002F5C
		public override SubjectIdentifier RecipientIdentifier
		{
			get
			{
				return null;
			}
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x0600007B RID: 123 RVA: 0x00004D60 File Offset: 0x00002F60
		public override int Version
		{
			get
			{
				return 0;
			}
		}
	}
}
