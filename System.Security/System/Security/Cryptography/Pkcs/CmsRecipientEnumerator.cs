﻿using System;
using System.Collections;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200001D RID: 29
	public sealed class CmsRecipientEnumerator : IEnumerator
	{
		// Token: 0x06000092 RID: 146 RVA: 0x00004F44 File Offset: 0x00003144
		internal CmsRecipientEnumerator(IEnumerable enumerable)
		{
			this.enumerator = enumerable.GetEnumerator();
		}

		// Token: 0x17000026 RID: 38
		// (get) Token: 0x06000093 RID: 147 RVA: 0x00004F58 File Offset: 0x00003158
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		// Token: 0x17000027 RID: 39
		// (get) Token: 0x06000094 RID: 148 RVA: 0x00004F68 File Offset: 0x00003168
		public CmsRecipient Current
		{
			get
			{
				return (CmsRecipient)this.enumerator.Current;
			}
		}

		// Token: 0x06000095 RID: 149 RVA: 0x00004F7C File Offset: 0x0000317C
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		// Token: 0x06000096 RID: 150 RVA: 0x00004F8C File Offset: 0x0000318C
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x04000066 RID: 102
		private IEnumerator enumerator;
	}
}
