﻿using System;
using System.Collections;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200002C RID: 44
	public sealed class SignerInfoCollection : IEnumerable, ICollection
	{
		// Token: 0x0600010E RID: 270 RVA: 0x0000608C File Offset: 0x0000428C
		internal SignerInfoCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x0600010F RID: 271 RVA: 0x000060A0 File Offset: 0x000042A0
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new SignerInfoEnumerator(this._list);
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x06000110 RID: 272 RVA: 0x000060B0 File Offset: 0x000042B0
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000111 RID: 273 RVA: 0x000060C0 File Offset: 0x000042C0
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000050 RID: 80
		public SignerInfo this[int index]
		{
			get
			{
				return (SignerInfo)this._list[index];
			}
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000113 RID: 275 RVA: 0x000060D8 File Offset: 0x000042D8
		public object SyncRoot
		{
			get
			{
				return this._list.SyncRoot;
			}
		}

		// Token: 0x06000114 RID: 276 RVA: 0x000060E8 File Offset: 0x000042E8
		internal void Add(SignerInfo signer)
		{
			this._list.Add(signer);
		}

		// Token: 0x06000115 RID: 277 RVA: 0x000060F8 File Offset: 0x000042F8
		public void CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0 || index >= array.Length)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			this._list.CopyTo(array, index);
		}

		// Token: 0x06000116 RID: 278 RVA: 0x00006144 File Offset: 0x00004344
		public void CopyTo(SignerInfo[] array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0 || index >= array.Length)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			this._list.CopyTo(array, index);
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00006180 File Offset: 0x00004380
		public SignerInfoEnumerator GetEnumerator()
		{
			return new SignerInfoEnumerator(this._list);
		}

		// Token: 0x04000095 RID: 149
		private ArrayList _list;
	}
}
