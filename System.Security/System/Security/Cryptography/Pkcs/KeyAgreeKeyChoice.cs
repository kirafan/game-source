﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000018 RID: 24
	public enum KeyAgreeKeyChoice
	{
		// Token: 0x0400005C RID: 92
		Unknown,
		// Token: 0x0400005D RID: 93
		EphemeralKey,
		// Token: 0x0400005E RID: 94
		StaticKey
	}
}
