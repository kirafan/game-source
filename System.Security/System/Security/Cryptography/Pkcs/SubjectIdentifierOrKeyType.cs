﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000030 RID: 48
	public enum SubjectIdentifierOrKeyType
	{
		// Token: 0x0400009C RID: 156
		Unknown,
		// Token: 0x0400009D RID: 157
		IssuerAndSerialNumber,
		// Token: 0x0400009E RID: 158
		SubjectKeyIdentifier,
		// Token: 0x0400009F RID: 159
		PublicKeyInfo
	}
}
