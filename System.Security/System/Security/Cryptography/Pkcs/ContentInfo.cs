﻿using System;
using System.Collections.Generic;
using Mono.Security;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000016 RID: 22
	public sealed class ContentInfo
	{
		// Token: 0x06000059 RID: 89 RVA: 0x0000475C File Offset: 0x0000295C
		public ContentInfo(byte[] content) : this(new Oid("1.2.840.113549.1.7.1"), content)
		{
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00004770 File Offset: 0x00002970
		public ContentInfo(Oid oid, byte[] content)
		{
			if (oid == null)
			{
				throw new ArgumentNullException("oid");
			}
			if (content == null)
			{
				throw new ArgumentNullException("content");
			}
			this._oid = oid;
			this._content = content;
		}

		// Token: 0x0600005B RID: 91 RVA: 0x000047B4 File Offset: 0x000029B4
		~ContentInfo()
		{
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600005C RID: 92 RVA: 0x000047EC File Offset: 0x000029EC
		public byte[] Content
		{
			get
			{
				return (byte[])this._content.Clone();
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600005D RID: 93 RVA: 0x00004800 File Offset: 0x00002A00
		public Oid ContentType
		{
			get
			{
				return this._oid;
			}
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00004808 File Offset: 0x00002A08
		[MonoTODO("MS is stricter than us about the content structure")]
		public static Oid GetContentType(byte[] encodedMessage)
		{
			if (encodedMessage == null)
			{
				throw new ArgumentNullException("algorithm");
			}
			try
			{
				PKCS7.ContentInfo contentInfo = new PKCS7.ContentInfo(encodedMessage);
				string contentType = contentInfo.ContentType;
				if (contentType != null)
				{
					if (ContentInfo.<>f__switch$map0 == null)
					{
						ContentInfo.<>f__switch$map0 = new Dictionary<string, int>(5)
						{
							{
								"1.2.840.113549.1.7.1",
								0
							},
							{
								"1.2.840.113549.1.7.2",
								0
							},
							{
								"1.2.840.113549.1.7.3",
								0
							},
							{
								"1.2.840.113549.1.7.5",
								0
							},
							{
								"1.2.840.113549.1.7.6",
								0
							}
						};
					}
					int num;
					if (ContentInfo.<>f__switch$map0.TryGetValue(contentType, out num))
					{
						if (num == 0)
						{
							return new Oid(contentInfo.ContentType);
						}
					}
				}
				string text = Locale.GetText("Bad ASN1 - invalid OID '{0}'");
				throw new CryptographicException(string.Format(text, contentInfo.ContentType));
			}
			catch (Exception inner)
			{
				throw new CryptographicException(Locale.GetText("Bad ASN1 - invalid structure"), inner);
			}
			Oid result;
			return result;
		}

		// Token: 0x04000051 RID: 81
		private Oid _oid;

		// Token: 0x04000052 RID: 82
		private byte[] _content;
	}
}
