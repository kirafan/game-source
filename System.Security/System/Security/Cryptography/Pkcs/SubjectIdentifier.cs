﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200002E RID: 46
	public sealed class SubjectIdentifier
	{
		// Token: 0x0600011D RID: 285 RVA: 0x000061E8 File Offset: 0x000043E8
		internal SubjectIdentifier(SubjectIdentifierType type, object value)
		{
			this._type = type;
			this._value = value;
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x0600011E RID: 286 RVA: 0x00006200 File Offset: 0x00004400
		public SubjectIdentifierType Type
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600011F RID: 287 RVA: 0x00006208 File Offset: 0x00004408
		public object Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x04000097 RID: 151
		private SubjectIdentifierType _type;

		// Token: 0x04000098 RID: 152
		private object _value;
	}
}
