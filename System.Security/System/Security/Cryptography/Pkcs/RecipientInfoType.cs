﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000029 RID: 41
	public enum RecipientInfoType
	{
		// Token: 0x04000085 RID: 133
		Unknown,
		// Token: 0x04000086 RID: 134
		KeyTransport,
		// Token: 0x04000087 RID: 135
		KeyAgreement
	}
}
