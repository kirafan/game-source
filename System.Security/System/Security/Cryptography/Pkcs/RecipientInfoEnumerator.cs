﻿using System;
using System.Collections;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000028 RID: 40
	public sealed class RecipientInfoEnumerator : IEnumerator
	{
		// Token: 0x060000E3 RID: 227 RVA: 0x00005988 File Offset: 0x00003B88
		internal RecipientInfoEnumerator(IEnumerable enumerable)
		{
			this.enumerator = enumerable.GetEnumerator();
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x060000E4 RID: 228 RVA: 0x0000599C File Offset: 0x00003B9C
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x060000E5 RID: 229 RVA: 0x000059AC File Offset: 0x00003BAC
		public RecipientInfo Current
		{
			get
			{
				return (RecipientInfo)this.enumerator.Current;
			}
		}

		// Token: 0x060000E6 RID: 230 RVA: 0x000059C0 File Offset: 0x00003BC0
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x000059D0 File Offset: 0x00003BD0
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x04000083 RID: 131
		private IEnumerator enumerator;
	}
}
