﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000031 RID: 49
	public enum SubjectIdentifierType
	{
		// Token: 0x040000A1 RID: 161
		Unknown,
		// Token: 0x040000A2 RID: 162
		IssuerAndSerialNumber,
		// Token: 0x040000A3 RID: 163
		SubjectKeyIdentifier,
		// Token: 0x040000A4 RID: 164
		NoSignature
	}
}
