﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200001E RID: 30
	public sealed class CmsSigner
	{
		// Token: 0x06000097 RID: 151 RVA: 0x00004F9C File Offset: 0x0000319C
		public CmsSigner()
		{
			this._signer = SubjectIdentifierType.IssuerAndSerialNumber;
			this._digest = new Oid("1.3.14.3.2.26");
			this._options = X509IncludeOption.ExcludeRoot;
			this._signed = new CryptographicAttributeObjectCollection();
			this._unsigned = new CryptographicAttributeObjectCollection();
			this._coll = new X509Certificate2Collection();
		}

		// Token: 0x06000098 RID: 152 RVA: 0x00004FF0 File Offset: 0x000031F0
		public CmsSigner(SubjectIdentifierType signerIdentifierType) : this()
		{
			if (signerIdentifierType == SubjectIdentifierType.Unknown)
			{
				this._signer = SubjectIdentifierType.IssuerAndSerialNumber;
			}
			else
			{
				this._signer = signerIdentifierType;
			}
		}

		// Token: 0x06000099 RID: 153 RVA: 0x00005014 File Offset: 0x00003214
		public CmsSigner(SubjectIdentifierType signerIdentifierType, X509Certificate2 certificate) : this(signerIdentifierType)
		{
			this._certificate = certificate;
		}

		// Token: 0x0600009A RID: 154 RVA: 0x00005024 File Offset: 0x00003224
		public CmsSigner(X509Certificate2 certificate) : this()
		{
			this._certificate = certificate;
		}

		// Token: 0x0600009B RID: 155 RVA: 0x00005034 File Offset: 0x00003234
		[MonoTODO]
		public CmsSigner(CspParameters parameters) : this()
		{
		}

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x0600009C RID: 156 RVA: 0x0000503C File Offset: 0x0000323C
		public CryptographicAttributeObjectCollection SignedAttributes
		{
			get
			{
				return this._signed;
			}
		}

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x0600009D RID: 157 RVA: 0x00005044 File Offset: 0x00003244
		// (set) Token: 0x0600009E RID: 158 RVA: 0x0000504C File Offset: 0x0000324C
		public X509Certificate2 Certificate
		{
			get
			{
				return this._certificate;
			}
			set
			{
				this._certificate = value;
			}
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x0600009F RID: 159 RVA: 0x00005058 File Offset: 0x00003258
		public X509Certificate2Collection Certificates
		{
			get
			{
				return this._coll;
			}
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x060000A0 RID: 160 RVA: 0x00005060 File Offset: 0x00003260
		// (set) Token: 0x060000A1 RID: 161 RVA: 0x00005068 File Offset: 0x00003268
		public Oid DigestAlgorithm
		{
			get
			{
				return this._digest;
			}
			set
			{
				this._digest = value;
			}
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060000A2 RID: 162 RVA: 0x00005074 File Offset: 0x00003274
		// (set) Token: 0x060000A3 RID: 163 RVA: 0x0000507C File Offset: 0x0000327C
		public X509IncludeOption IncludeOption
		{
			get
			{
				return this._options;
			}
			set
			{
				this._options = value;
			}
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x060000A4 RID: 164 RVA: 0x00005088 File Offset: 0x00003288
		// (set) Token: 0x060000A5 RID: 165 RVA: 0x00005090 File Offset: 0x00003290
		public SubjectIdentifierType SignerIdentifierType
		{
			get
			{
				return this._signer;
			}
			set
			{
				if (value == SubjectIdentifierType.Unknown)
				{
					throw new ArgumentException("value");
				}
				this._signer = value;
			}
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x060000A6 RID: 166 RVA: 0x000050AC File Offset: 0x000032AC
		public CryptographicAttributeObjectCollection UnsignedAttributes
		{
			get
			{
				return this._unsigned;
			}
		}

		// Token: 0x04000067 RID: 103
		private SubjectIdentifierType _signer;

		// Token: 0x04000068 RID: 104
		private X509Certificate2 _certificate;

		// Token: 0x04000069 RID: 105
		private X509Certificate2Collection _coll;

		// Token: 0x0400006A RID: 106
		private Oid _digest;

		// Token: 0x0400006B RID: 107
		private X509IncludeOption _options;

		// Token: 0x0400006C RID: 108
		private CryptographicAttributeObjectCollection _signed;

		// Token: 0x0400006D RID: 109
		private CryptographicAttributeObjectCollection _unsigned;
	}
}
