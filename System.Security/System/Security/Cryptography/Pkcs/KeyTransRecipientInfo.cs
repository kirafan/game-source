﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200001A RID: 26
	public sealed class KeyTransRecipientInfo : RecipientInfo
	{
		// Token: 0x0600007C RID: 124 RVA: 0x00004D64 File Offset: 0x00002F64
		internal KeyTransRecipientInfo(byte[] encryptedKey, AlgorithmIdentifier keyEncryptionAlgorithm, SubjectIdentifier recipientIdentifier, int version) : base(RecipientInfoType.KeyTransport)
		{
			this._encryptedKey = encryptedKey;
			this._keyEncryptionAlgorithm = keyEncryptionAlgorithm;
			this._recipientIdentifier = recipientIdentifier;
			this._version = version;
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x0600007D RID: 125 RVA: 0x00004D98 File Offset: 0x00002F98
		public override byte[] EncryptedKey
		{
			get
			{
				return this._encryptedKey;
			}
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x0600007E RID: 126 RVA: 0x00004DA0 File Offset: 0x00002FA0
		public override AlgorithmIdentifier KeyEncryptionAlgorithm
		{
			get
			{
				return this._keyEncryptionAlgorithm;
			}
		}

		// Token: 0x1700001E RID: 30
		// (get) Token: 0x0600007F RID: 127 RVA: 0x00004DA8 File Offset: 0x00002FA8
		public override SubjectIdentifier RecipientIdentifier
		{
			get
			{
				return this._recipientIdentifier;
			}
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x06000080 RID: 128 RVA: 0x00004DB0 File Offset: 0x00002FB0
		public override int Version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x0400005F RID: 95
		private byte[] _encryptedKey;

		// Token: 0x04000060 RID: 96
		private AlgorithmIdentifier _keyEncryptionAlgorithm;

		// Token: 0x04000061 RID: 97
		private SubjectIdentifier _recipientIdentifier;

		// Token: 0x04000062 RID: 98
		private int _version;
	}
}
