﻿using System;
using System.Collections;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200002D RID: 45
	public sealed class SignerInfoEnumerator : IEnumerator
	{
		// Token: 0x06000118 RID: 280 RVA: 0x00006190 File Offset: 0x00004390
		internal SignerInfoEnumerator(IEnumerable enumerable)
		{
			this.enumerator = enumerable.GetEnumerator();
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000119 RID: 281 RVA: 0x000061A4 File Offset: 0x000043A4
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600011A RID: 282 RVA: 0x000061B4 File Offset: 0x000043B4
		public SignerInfo Current
		{
			get
			{
				return (SignerInfo)this.enumerator.Current;
			}
		}

		// Token: 0x0600011B RID: 283 RVA: 0x000061C8 File Offset: 0x000043C8
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		// Token: 0x0600011C RID: 284 RVA: 0x000061D8 File Offset: 0x000043D8
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x04000096 RID: 150
		private IEnumerator enumerator;
	}
}
