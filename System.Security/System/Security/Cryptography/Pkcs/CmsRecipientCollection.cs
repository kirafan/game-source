﻿using System;
using System.Collections;
using System.Security.Cryptography.X509Certificates;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200001C RID: 28
	public sealed class CmsRecipientCollection : IEnumerable, ICollection
	{
		// Token: 0x06000085 RID: 133 RVA: 0x00004E2C File Offset: 0x0000302C
		public CmsRecipientCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x06000086 RID: 134 RVA: 0x00004E40 File Offset: 0x00003040
		public CmsRecipientCollection(CmsRecipient recipient)
		{
			this._list.Add(recipient);
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00004E58 File Offset: 0x00003058
		public CmsRecipientCollection(SubjectIdentifierType recipientIdentifierType, X509Certificate2Collection certificates)
		{
			foreach (X509Certificate2 certificate in certificates)
			{
				CmsRecipient value = new CmsRecipient(recipientIdentifierType, certificate);
				this._list.Add(value);
			}
		}

		// Token: 0x06000088 RID: 136 RVA: 0x00004EA0 File Offset: 0x000030A0
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new CmsRecipientEnumerator(this._list);
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000089 RID: 137 RVA: 0x00004EB0 File Offset: 0x000030B0
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x0600008A RID: 138 RVA: 0x00004EC0 File Offset: 0x000030C0
		public bool IsSynchronized
		{
			get
			{
				return this._list.IsSynchronized;
			}
		}

		// Token: 0x17000024 RID: 36
		public CmsRecipient this[int index]
		{
			get
			{
				return (CmsRecipient)this._list[index];
			}
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600008C RID: 140 RVA: 0x00004EE4 File Offset: 0x000030E4
		public object SyncRoot
		{
			get
			{
				return this._list.SyncRoot;
			}
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00004EF4 File Offset: 0x000030F4
		public int Add(CmsRecipient recipient)
		{
			return this._list.Add(recipient);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x00004F04 File Offset: 0x00003104
		public void CopyTo(Array array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x00004F14 File Offset: 0x00003114
		public void CopyTo(CmsRecipient[] array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x00004F24 File Offset: 0x00003124
		public CmsRecipientEnumerator GetEnumerator()
		{
			return new CmsRecipientEnumerator(this._list);
		}

		// Token: 0x06000091 RID: 145 RVA: 0x00004F34 File Offset: 0x00003134
		public void Remove(CmsRecipient recipient)
		{
			this._list.Remove(recipient);
		}

		// Token: 0x04000065 RID: 101
		private ArrayList _list;
	}
}
