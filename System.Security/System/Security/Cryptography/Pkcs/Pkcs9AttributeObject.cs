﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200001F RID: 31
	public class Pkcs9AttributeObject : AsnEncodedData
	{
		// Token: 0x060000A7 RID: 167 RVA: 0x000050B4 File Offset: 0x000032B4
		public Pkcs9AttributeObject()
		{
		}

		// Token: 0x060000A8 RID: 168 RVA: 0x000050BC File Offset: 0x000032BC
		public Pkcs9AttributeObject(AsnEncodedData asnEncodedData) : base(asnEncodedData)
		{
		}

		// Token: 0x060000A9 RID: 169 RVA: 0x000050C8 File Offset: 0x000032C8
		public Pkcs9AttributeObject(Oid oid, byte[] encodedData)
		{
			if (oid == null)
			{
				throw new ArgumentNullException("oid");
			}
			base.Oid = oid;
			base.RawData = encodedData;
		}

		// Token: 0x060000AA RID: 170 RVA: 0x000050F0 File Offset: 0x000032F0
		public Pkcs9AttributeObject(string oid, byte[] encodedData) : base(oid, encodedData)
		{
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x060000AB RID: 171 RVA: 0x000050FC File Offset: 0x000032FC
		// (set) Token: 0x060000AC RID: 172 RVA: 0x00005104 File Offset: 0x00003304
		public new Oid Oid
		{
			get
			{
				return base.Oid;
			}
			internal set
			{
				base.Oid = value;
			}
		}

		// Token: 0x060000AD RID: 173 RVA: 0x00005110 File Offset: 0x00003310
		public override void CopyFrom(AsnEncodedData asnEncodedData)
		{
			if (asnEncodedData == null)
			{
				throw new ArgumentNullException("asnEncodedData");
			}
			throw new ArgumentException("Cannot convert the PKCS#9 attribute.");
		}
	}
}
