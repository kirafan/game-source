﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Xml;
using System.Text;
using Mono.Security;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000017 RID: 23
	public sealed class EnvelopedCms
	{
		// Token: 0x0600005F RID: 95 RVA: 0x0000491C File Offset: 0x00002B1C
		public EnvelopedCms()
		{
			this._certs = new X509Certificate2Collection();
			this._recipients = new RecipientInfoCollection();
			this._uattribs = new CryptographicAttributeObjectCollection();
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00004948 File Offset: 0x00002B48
		public EnvelopedCms(ContentInfo content) : this()
		{
			if (content == null)
			{
				throw new ArgumentNullException("content");
			}
			this._content = content;
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00004968 File Offset: 0x00002B68
		public EnvelopedCms(ContentInfo contentInfo, AlgorithmIdentifier encryptionAlgorithm) : this(contentInfo)
		{
			if (encryptionAlgorithm == null)
			{
				throw new ArgumentNullException("encryptionAlgorithm");
			}
			this._identifier = encryptionAlgorithm;
		}

		// Token: 0x06000062 RID: 98 RVA: 0x0000498C File Offset: 0x00002B8C
		public EnvelopedCms(SubjectIdentifierType recipientIdentifierType, ContentInfo contentInfo) : this(contentInfo)
		{
			this._idType = recipientIdentifierType;
			if (this._idType == SubjectIdentifierType.SubjectKeyIdentifier)
			{
				this._version = 2;
			}
		}

		// Token: 0x06000063 RID: 99 RVA: 0x000049B0 File Offset: 0x00002BB0
		public EnvelopedCms(SubjectIdentifierType recipientIdentifierType, ContentInfo contentInfo, AlgorithmIdentifier encryptionAlgorithm) : this(contentInfo, encryptionAlgorithm)
		{
			this._idType = recipientIdentifierType;
			if (this._idType == SubjectIdentifierType.SubjectKeyIdentifier)
			{
				this._version = 2;
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000064 RID: 100 RVA: 0x000049E0 File Offset: 0x00002BE0
		public X509Certificate2Collection Certificates
		{
			get
			{
				return this._certs;
			}
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x06000065 RID: 101 RVA: 0x000049E8 File Offset: 0x00002BE8
		public AlgorithmIdentifier ContentEncryptionAlgorithm
		{
			get
			{
				if (this._identifier == null)
				{
					this._identifier = new AlgorithmIdentifier();
				}
				return this._identifier;
			}
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x06000066 RID: 102 RVA: 0x00004A08 File Offset: 0x00002C08
		public ContentInfo ContentInfo
		{
			get
			{
				if (this._content == null)
				{
					Oid oid = new Oid("1.2.840.113549.1.7.1");
					this._content = new ContentInfo(oid, new byte[0]);
				}
				return this._content;
			}
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x06000067 RID: 103 RVA: 0x00004A44 File Offset: 0x00002C44
		public RecipientInfoCollection RecipientInfos
		{
			get
			{
				return this._recipients;
			}
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x06000068 RID: 104 RVA: 0x00004A4C File Offset: 0x00002C4C
		public CryptographicAttributeObjectCollection UnprotectedAttributes
		{
			get
			{
				return this._uattribs;
			}
		}

		// Token: 0x17000014 RID: 20
		// (get) Token: 0x06000069 RID: 105 RVA: 0x00004A54 File Offset: 0x00002C54
		public int Version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00004A5C File Offset: 0x00002C5C
		private X509IssuerSerial GetIssuerSerial(string issuer, byte[] serial)
		{
			X509IssuerSerial result = default(X509IssuerSerial);
			result.IssuerName = issuer;
			StringBuilder stringBuilder = new StringBuilder();
			foreach (byte b in serial)
			{
				stringBuilder.Append(b.ToString("X2"));
			}
			result.SerialNumber = stringBuilder.ToString();
			return result;
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00004AC0 File Offset: 0x00002CC0
		[MonoTODO]
		public void Decode(byte[] encodedMessage)
		{
			if (encodedMessage == null)
			{
				throw new ArgumentNullException("encodedMessage");
			}
			PKCS7.ContentInfo contentInfo = new PKCS7.ContentInfo(encodedMessage);
			if (contentInfo.ContentType != "1.2.840.113549.1.7.3")
			{
				throw new Exception(string.Empty);
			}
			PKCS7.EnvelopedData envelopedData = new PKCS7.EnvelopedData(contentInfo.Content);
			Oid oid = new Oid(envelopedData.ContentInfo.ContentType);
			this._content = new ContentInfo(oid, new byte[0]);
			foreach (object obj in envelopedData.RecipientInfos)
			{
				PKCS7.RecipientInfo recipientInfo = (PKCS7.RecipientInfo)obj;
				Oid algorithm = new Oid(recipientInfo.Oid);
				AlgorithmIdentifier keyEncryptionAlgorithm = new AlgorithmIdentifier(algorithm);
				SubjectIdentifier recipientIdentifier = null;
				if (recipientInfo.SubjectKeyIdentifier != null)
				{
					recipientIdentifier = new SubjectIdentifier(SubjectIdentifierType.SubjectKeyIdentifier, recipientInfo.SubjectKeyIdentifier);
				}
				else if (recipientInfo.Issuer != null && recipientInfo.Serial != null)
				{
					X509IssuerSerial issuerSerial = this.GetIssuerSerial(recipientInfo.Issuer, recipientInfo.Serial);
					recipientIdentifier = new SubjectIdentifier(SubjectIdentifierType.IssuerAndSerialNumber, issuerSerial);
				}
				KeyTransRecipientInfo ri = new KeyTransRecipientInfo(recipientInfo.Key, keyEncryptionAlgorithm, recipientIdentifier, recipientInfo.Version);
				this._recipients.Add(ri);
			}
			this._version = (int)envelopedData.Version;
		}

		// Token: 0x0600006C RID: 108 RVA: 0x00004C38 File Offset: 0x00002E38
		[MonoTODO]
		public void Decrypt()
		{
			throw new InvalidOperationException("not encrypted");
		}

		// Token: 0x0600006D RID: 109 RVA: 0x00004C44 File Offset: 0x00002E44
		[MonoTODO]
		public void Decrypt(RecipientInfo recipientInfo)
		{
			if (recipientInfo == null)
			{
				throw new ArgumentNullException("recipientInfo");
			}
			this.Decrypt();
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00004C60 File Offset: 0x00002E60
		[MonoTODO]
		public void Decrypt(RecipientInfo recipientInfo, X509Certificate2Collection extraStore)
		{
			if (recipientInfo == null)
			{
				throw new ArgumentNullException("recipientInfo");
			}
			if (extraStore == null)
			{
				throw new ArgumentNullException("extraStore");
			}
			this.Decrypt();
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00004C98 File Offset: 0x00002E98
		[MonoTODO]
		public void Decrypt(X509Certificate2Collection extraStore)
		{
			if (extraStore == null)
			{
				throw new ArgumentNullException("extraStore");
			}
			this.Decrypt();
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00004CB4 File Offset: 0x00002EB4
		[MonoTODO]
		public byte[] Encode()
		{
			throw new InvalidOperationException("not encrypted");
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00004CC0 File Offset: 0x00002EC0
		[MonoTODO]
		public void Encrypt()
		{
			if (this._content == null || this._content.Content == null || this._content.Content.Length == 0)
			{
				throw new CryptographicException("no content to encrypt");
			}
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00004D08 File Offset: 0x00002F08
		[MonoTODO]
		public void Encrypt(CmsRecipient recipient)
		{
			if (recipient == null)
			{
				throw new ArgumentNullException("recipient");
			}
			this.Encrypt();
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00004D24 File Offset: 0x00002F24
		[MonoTODO]
		public void Encrypt(CmsRecipientCollection recipients)
		{
			if (recipients == null)
			{
				throw new ArgumentNullException("recipients");
			}
		}

		// Token: 0x04000054 RID: 84
		private ContentInfo _content;

		// Token: 0x04000055 RID: 85
		private AlgorithmIdentifier _identifier;

		// Token: 0x04000056 RID: 86
		private X509Certificate2Collection _certs;

		// Token: 0x04000057 RID: 87
		private RecipientInfoCollection _recipients;

		// Token: 0x04000058 RID: 88
		private CryptographicAttributeObjectCollection _uattribs;

		// Token: 0x04000059 RID: 89
		private SubjectIdentifierType _idType;

		// Token: 0x0400005A RID: 90
		private int _version;
	}
}
