﻿using System;
using System.Collections;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000027 RID: 39
	public sealed class RecipientInfoCollection : IEnumerable, ICollection
	{
		// Token: 0x060000D9 RID: 217 RVA: 0x000058E0 File Offset: 0x00003AE0
		internal RecipientInfoCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x060000DA RID: 218 RVA: 0x000058F4 File Offset: 0x00003AF4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new RecipientInfoEnumerator(this._list);
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x060000DB RID: 219 RVA: 0x00005904 File Offset: 0x00003B04
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x060000DC RID: 220 RVA: 0x00005914 File Offset: 0x00003B14
		public bool IsSynchronized
		{
			get
			{
				return this._list.IsSynchronized;
			}
		}

		// Token: 0x1700003E RID: 62
		public RecipientInfo this[int index]
		{
			get
			{
				return (RecipientInfo)this._list[index];
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x060000DE RID: 222 RVA: 0x00005938 File Offset: 0x00003B38
		public object SyncRoot
		{
			get
			{
				return this._list.SyncRoot;
			}
		}

		// Token: 0x060000DF RID: 223 RVA: 0x00005948 File Offset: 0x00003B48
		internal int Add(RecipientInfo ri)
		{
			return this._list.Add(ri);
		}

		// Token: 0x060000E0 RID: 224 RVA: 0x00005958 File Offset: 0x00003B58
		public void CopyTo(Array array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x00005968 File Offset: 0x00003B68
		public void CopyTo(RecipientInfo[] array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x060000E2 RID: 226 RVA: 0x00005978 File Offset: 0x00003B78
		public RecipientInfoEnumerator GetEnumerator()
		{
			return new RecipientInfoEnumerator(this._list);
		}

		// Token: 0x04000082 RID: 130
		private ArrayList _list;
	}
}
