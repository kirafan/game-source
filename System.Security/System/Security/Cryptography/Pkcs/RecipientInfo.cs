﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x02000026 RID: 38
	public abstract class RecipientInfo
	{
		// Token: 0x060000D3 RID: 211 RVA: 0x000058C8 File Offset: 0x00003AC8
		internal RecipientInfo(RecipientInfoType recipInfoType)
		{
			this._type = recipInfoType;
		}

		// Token: 0x17000037 RID: 55
		// (get) Token: 0x060000D4 RID: 212
		public abstract byte[] EncryptedKey { get; }

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x060000D5 RID: 213
		public abstract AlgorithmIdentifier KeyEncryptionAlgorithm { get; }

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x060000D6 RID: 214
		public abstract SubjectIdentifier RecipientIdentifier { get; }

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x060000D7 RID: 215 RVA: 0x000058D8 File Offset: 0x00003AD8
		public RecipientInfoType Type
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x060000D8 RID: 216
		public abstract int Version { get; }

		// Token: 0x04000081 RID: 129
		private RecipientInfoType _type;
	}
}
