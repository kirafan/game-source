﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200002B RID: 43
	public sealed class SignerInfo
	{
		// Token: 0x060000FF RID: 255 RVA: 0x00005FD4 File Offset: 0x000041D4
		internal SignerInfo(string hashName, X509Certificate2 certificate, SubjectIdentifierType type, object o, int version)
		{
			this._digest = new Oid(CryptoConfig.MapNameToOID(hashName));
			this._certificate = certificate;
			this._counter = new SignerInfoCollection();
			this._signed = new CryptographicAttributeObjectCollection();
			this._unsigned = new CryptographicAttributeObjectCollection();
			this._signer = new SubjectIdentifier(type, o);
			this._version = version;
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000100 RID: 256 RVA: 0x00006038 File Offset: 0x00004238
		public CryptographicAttributeObjectCollection SignedAttributes
		{
			get
			{
				return this._signed;
			}
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000101 RID: 257 RVA: 0x00006040 File Offset: 0x00004240
		public X509Certificate2 Certificate
		{
			get
			{
				return this._certificate;
			}
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x06000102 RID: 258 RVA: 0x00006048 File Offset: 0x00004248
		public SignerInfoCollection CounterSignerInfos
		{
			get
			{
				return this._counter;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000103 RID: 259 RVA: 0x00006050 File Offset: 0x00004250
		public Oid DigestAlgorithm
		{
			get
			{
				return this._digest;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000104 RID: 260 RVA: 0x00006058 File Offset: 0x00004258
		public SubjectIdentifier SignerIdentifier
		{
			get
			{
				return this._signer;
			}
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000105 RID: 261 RVA: 0x00006060 File Offset: 0x00004260
		public CryptographicAttributeObjectCollection UnsignedAttributes
		{
			get
			{
				return this._unsigned;
			}
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000106 RID: 262 RVA: 0x00006068 File Offset: 0x00004268
		public int Version
		{
			get
			{
				return this._version;
			}
		}

		// Token: 0x06000107 RID: 263 RVA: 0x00006070 File Offset: 0x00004270
		[MonoTODO]
		public void CheckHash()
		{
		}

		// Token: 0x06000108 RID: 264 RVA: 0x00006074 File Offset: 0x00004274
		[MonoTODO]
		public void CheckSignature(bool verifySignatureOnly)
		{
		}

		// Token: 0x06000109 RID: 265 RVA: 0x00006078 File Offset: 0x00004278
		[MonoTODO]
		public void CheckSignature(X509Certificate2Collection extraStore, bool verifySignatureOnly)
		{
		}

		// Token: 0x0600010A RID: 266 RVA: 0x0000607C File Offset: 0x0000427C
		[MonoTODO]
		public void ComputeCounterSignature()
		{
		}

		// Token: 0x0600010B RID: 267 RVA: 0x00006080 File Offset: 0x00004280
		[MonoTODO]
		public void ComputeCounterSignature(CmsSigner signer)
		{
		}

		// Token: 0x0600010C RID: 268 RVA: 0x00006084 File Offset: 0x00004284
		[MonoTODO]
		public void RemoveCounterSignature(SignerInfo counterSignerInfo)
		{
		}

		// Token: 0x0600010D RID: 269 RVA: 0x00006088 File Offset: 0x00004288
		[MonoTODO]
		public void RemoveCounterSignature(int index)
		{
		}

		// Token: 0x0400008E RID: 142
		private SubjectIdentifier _signer;

		// Token: 0x0400008F RID: 143
		private X509Certificate2 _certificate;

		// Token: 0x04000090 RID: 144
		private Oid _digest;

		// Token: 0x04000091 RID: 145
		private SignerInfoCollection _counter;

		// Token: 0x04000092 RID: 146
		private CryptographicAttributeObjectCollection _signed;

		// Token: 0x04000093 RID: 147
		private CryptographicAttributeObjectCollection _unsigned;

		// Token: 0x04000094 RID: 148
		private int _version;
	}
}
