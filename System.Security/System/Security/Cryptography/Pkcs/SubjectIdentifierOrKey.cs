﻿using System;

namespace System.Security.Cryptography.Pkcs
{
	// Token: 0x0200002F RID: 47
	public sealed class SubjectIdentifierOrKey
	{
		// Token: 0x06000120 RID: 288 RVA: 0x00006210 File Offset: 0x00004410
		internal SubjectIdentifierOrKey(SubjectIdentifierOrKeyType type, object value)
		{
			this._type = type;
			this._value = value;
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x06000121 RID: 289 RVA: 0x00006228 File Offset: 0x00004428
		public SubjectIdentifierOrKeyType Type
		{
			get
			{
				return this._type;
			}
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000122 RID: 290 RVA: 0x00006230 File Offset: 0x00004430
		public object Value
		{
			get
			{
				return this._value;
			}
		}

		// Token: 0x04000099 RID: 153
		private SubjectIdentifierOrKeyType _type;

		// Token: 0x0400009A RID: 154
		private object _value;
	}
}
