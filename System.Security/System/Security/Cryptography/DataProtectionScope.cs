﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200000F RID: 15
	public enum DataProtectionScope
	{
		// Token: 0x0400003B RID: 59
		CurrentUser,
		// Token: 0x0400003C RID: 60
		LocalMachine
	}
}
