﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000033 RID: 51
	public enum X509SelectionFlag
	{
		// Token: 0x040000A6 RID: 166
		SingleSelection,
		// Token: 0x040000A7 RID: 167
		MultiSelection
	}
}
