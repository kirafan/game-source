﻿using System;
using System.Collections;

namespace System.Security.Cryptography
{
	// Token: 0x0200000D RID: 13
	public sealed class CryptographicAttributeObjectCollection : IEnumerable, ICollection
	{
		// Token: 0x06000030 RID: 48 RVA: 0x00003EF0 File Offset: 0x000020F0
		public CryptographicAttributeObjectCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x06000031 RID: 49 RVA: 0x00003F04 File Offset: 0x00002104
		public CryptographicAttributeObjectCollection(CryptographicAttributeObject attribute) : this()
		{
			this._list.Add(attribute);
		}

		// Token: 0x06000032 RID: 50 RVA: 0x00003F1C File Offset: 0x0000211C
		void ICollection.CopyTo(Array array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x06000033 RID: 51 RVA: 0x00003F2C File Offset: 0x0000212C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new CryptographicAttributeObjectEnumerator(this._list);
		}

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000034 RID: 52 RVA: 0x00003F3C File Offset: 0x0000213C
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000035 RID: 53 RVA: 0x00003F4C File Offset: 0x0000214C
		public bool IsSynchronized
		{
			get
			{
				return this._list.IsSynchronized;
			}
		}

		// Token: 0x17000006 RID: 6
		public CryptographicAttributeObject this[int index]
		{
			get
			{
				return (CryptographicAttributeObject)this._list[index];
			}
		}

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x06000037 RID: 55 RVA: 0x00003F70 File Offset: 0x00002170
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06000038 RID: 56 RVA: 0x00003F74 File Offset: 0x00002174
		public int Add(AsnEncodedData asnEncodedData)
		{
			if (asnEncodedData == null)
			{
				throw new ArgumentNullException("asnEncodedData");
			}
			AsnEncodedDataCollection values = new AsnEncodedDataCollection(asnEncodedData);
			return this.Add(new CryptographicAttributeObject(asnEncodedData.Oid, values));
		}

		// Token: 0x06000039 RID: 57 RVA: 0x00003FAC File Offset: 0x000021AC
		public int Add(CryptographicAttributeObject attribute)
		{
			if (attribute == null)
			{
				throw new ArgumentNullException("attribute");
			}
			int num = -1;
			string value = attribute.Oid.Value;
			for (int i = 0; i < this._list.Count; i++)
			{
				if ((this._list[i] as CryptographicAttributeObject).Oid.Value == value)
				{
					num = i;
					break;
				}
			}
			if (num >= 0)
			{
				CryptographicAttributeObject cryptographicAttributeObject = this[num];
				foreach (AsnEncodedData asnEncodedData in attribute.Values)
				{
					cryptographicAttributeObject.Values.Add(asnEncodedData);
				}
				return num;
			}
			return this._list.Add(attribute);
		}

		// Token: 0x0600003A RID: 58 RVA: 0x00004074 File Offset: 0x00002274
		public void CopyTo(CryptographicAttributeObject[] array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x0600003B RID: 59 RVA: 0x00004084 File Offset: 0x00002284
		public CryptographicAttributeObjectEnumerator GetEnumerator()
		{
			return new CryptographicAttributeObjectEnumerator(this._list);
		}

		// Token: 0x0600003C RID: 60 RVA: 0x00004094 File Offset: 0x00002294
		public void Remove(CryptographicAttributeObject attribute)
		{
			if (attribute == null)
			{
				throw new ArgumentNullException("attribute");
			}
			this._list.Remove(attribute);
		}

		// Token: 0x04000038 RID: 56
		private ArrayList _list;
	}
}
