﻿using System;
using System.Collections.Generic;
using Amazon.CognitoIdentity.Model;
using Amazon.CognitoIdentity.Model.Internal.MarshallTransformations;
using Amazon.Runtime;
using Amazon.Runtime.Internal.Auth;

namespace Amazon.CognitoIdentity
{
	// Token: 0x0200000D RID: 13
	public class AmazonCognitoIdentityClient : AmazonServiceClient, IAmazonCognitoIdentity, IAmazonService, IDisposable
	{
		// Token: 0x06000059 RID: 89 RVA: 0x00002AA9 File Offset: 0x00000CA9
		public AmazonCognitoIdentityClient(AWSCredentials credentials) : this(credentials, new AmazonCognitoIdentityConfig())
		{
		}

		// Token: 0x0600005A RID: 90 RVA: 0x00002AB7 File Offset: 0x00000CB7
		public AmazonCognitoIdentityClient(AWSCredentials credentials, RegionEndpoint region) : this(credentials, new AmazonCognitoIdentityConfig
		{
			RegionEndpoint = region
		})
		{
		}

		// Token: 0x0600005B RID: 91 RVA: 0x00002ACC File Offset: 0x00000CCC
		public AmazonCognitoIdentityClient(AWSCredentials credentials, AmazonCognitoIdentityConfig clientConfig) : base(credentials, clientConfig)
		{
		}

		// Token: 0x0600005C RID: 92 RVA: 0x00002AD6 File Offset: 0x00000CD6
		public AmazonCognitoIdentityClient(string awsAccessKeyId, string awsSecretAccessKey) : this(awsAccessKeyId, awsSecretAccessKey, new AmazonCognitoIdentityConfig())
		{
		}

		// Token: 0x0600005D RID: 93 RVA: 0x00002AE5 File Offset: 0x00000CE5
		public AmazonCognitoIdentityClient(string awsAccessKeyId, string awsSecretAccessKey, RegionEndpoint region) : this(awsAccessKeyId, awsSecretAccessKey, new AmazonCognitoIdentityConfig
		{
			RegionEndpoint = region
		})
		{
		}

		// Token: 0x0600005E RID: 94 RVA: 0x00002AFB File Offset: 0x00000CFB
		public AmazonCognitoIdentityClient(string awsAccessKeyId, string awsSecretAccessKey, AmazonCognitoIdentityConfig clientConfig) : base(awsAccessKeyId, awsSecretAccessKey, clientConfig)
		{
		}

		// Token: 0x0600005F RID: 95 RVA: 0x00002B06 File Offset: 0x00000D06
		public AmazonCognitoIdentityClient(string awsAccessKeyId, string awsSecretAccessKey, string awsSessionToken) : this(awsAccessKeyId, awsSecretAccessKey, awsSessionToken, new AmazonCognitoIdentityConfig())
		{
		}

		// Token: 0x06000060 RID: 96 RVA: 0x00002B16 File Offset: 0x00000D16
		public AmazonCognitoIdentityClient(string awsAccessKeyId, string awsSecretAccessKey, string awsSessionToken, RegionEndpoint region) : this(awsAccessKeyId, awsSecretAccessKey, awsSessionToken, new AmazonCognitoIdentityConfig
		{
			RegionEndpoint = region
		})
		{
		}

		// Token: 0x06000061 RID: 97 RVA: 0x00002B2E File Offset: 0x00000D2E
		public AmazonCognitoIdentityClient(string awsAccessKeyId, string awsSecretAccessKey, string awsSessionToken, AmazonCognitoIdentityConfig clientConfig) : base(awsAccessKeyId, awsSecretAccessKey, awsSessionToken, clientConfig)
		{
		}

		// Token: 0x06000062 RID: 98 RVA: 0x00002B3B File Offset: 0x00000D3B
		protected override AbstractAWSSigner CreateSigner()
		{
			return new AWS4Signer();
		}

		// Token: 0x06000063 RID: 99 RVA: 0x00002B42 File Offset: 0x00000D42
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}

		// Token: 0x06000064 RID: 100 RVA: 0x00002B4C File Offset: 0x00000D4C
		internal CreateIdentityPoolResponse CreateIdentityPool(CreateIdentityPoolRequest request)
		{
			CreateIdentityPoolRequestMarshaller marshaller = new CreateIdentityPoolRequestMarshaller();
			CreateIdentityPoolResponseUnmarshaller instance = CreateIdentityPoolResponseUnmarshaller.Instance;
			return base.Invoke<CreateIdentityPoolRequest, CreateIdentityPoolResponse>(request, marshaller, instance);
		}

		// Token: 0x06000065 RID: 101 RVA: 0x00002B70 File Offset: 0x00000D70
		public void CreateIdentityPoolAsync(CreateIdentityPoolRequest request, AmazonServiceCallback<CreateIdentityPoolRequest, CreateIdentityPoolResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			CreateIdentityPoolRequestMarshaller marshaller = new CreateIdentityPoolRequestMarshaller();
			CreateIdentityPoolResponseUnmarshaller instance = CreateIdentityPoolResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<CreateIdentityPoolRequest, CreateIdentityPoolResponse> responseObject = new AmazonServiceResult<CreateIdentityPoolRequest, CreateIdentityPoolResponse>((CreateIdentityPoolRequest)req, (CreateIdentityPoolResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<CreateIdentityPoolRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000066 RID: 102 RVA: 0x00002BC8 File Offset: 0x00000DC8
		internal DeleteIdentitiesResponse DeleteIdentities(DeleteIdentitiesRequest request)
		{
			DeleteIdentitiesRequestMarshaller marshaller = new DeleteIdentitiesRequestMarshaller();
			DeleteIdentitiesResponseUnmarshaller instance = DeleteIdentitiesResponseUnmarshaller.Instance;
			return base.Invoke<DeleteIdentitiesRequest, DeleteIdentitiesResponse>(request, marshaller, instance);
		}

		// Token: 0x06000067 RID: 103 RVA: 0x00002BEC File Offset: 0x00000DEC
		public void DeleteIdentitiesAsync(DeleteIdentitiesRequest request, AmazonServiceCallback<DeleteIdentitiesRequest, DeleteIdentitiesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			DeleteIdentitiesRequestMarshaller marshaller = new DeleteIdentitiesRequestMarshaller();
			DeleteIdentitiesResponseUnmarshaller instance = DeleteIdentitiesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<DeleteIdentitiesRequest, DeleteIdentitiesResponse> responseObject = new AmazonServiceResult<DeleteIdentitiesRequest, DeleteIdentitiesResponse>((DeleteIdentitiesRequest)req, (DeleteIdentitiesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<DeleteIdentitiesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000068 RID: 104 RVA: 0x00002C44 File Offset: 0x00000E44
		internal DeleteIdentityPoolResponse DeleteIdentityPool(DeleteIdentityPoolRequest request)
		{
			DeleteIdentityPoolRequestMarshaller marshaller = new DeleteIdentityPoolRequestMarshaller();
			DeleteIdentityPoolResponseUnmarshaller instance = DeleteIdentityPoolResponseUnmarshaller.Instance;
			return base.Invoke<DeleteIdentityPoolRequest, DeleteIdentityPoolResponse>(request, marshaller, instance);
		}

		// Token: 0x06000069 RID: 105 RVA: 0x00002C68 File Offset: 0x00000E68
		public void DeleteIdentityPoolAsync(string identityPoolId, AmazonServiceCallback<DeleteIdentityPoolRequest, DeleteIdentityPoolResponse> callback, AsyncOptions options = null)
		{
			this.DeleteIdentityPoolAsync(new DeleteIdentityPoolRequest
			{
				IdentityPoolId = identityPoolId
			}, callback, options);
		}

		// Token: 0x0600006A RID: 106 RVA: 0x00002C8C File Offset: 0x00000E8C
		public void DeleteIdentityPoolAsync(DeleteIdentityPoolRequest request, AmazonServiceCallback<DeleteIdentityPoolRequest, DeleteIdentityPoolResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			DeleteIdentityPoolRequestMarshaller marshaller = new DeleteIdentityPoolRequestMarshaller();
			DeleteIdentityPoolResponseUnmarshaller instance = DeleteIdentityPoolResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<DeleteIdentityPoolRequest, DeleteIdentityPoolResponse> responseObject = new AmazonServiceResult<DeleteIdentityPoolRequest, DeleteIdentityPoolResponse>((DeleteIdentityPoolRequest)req, (DeleteIdentityPoolResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<DeleteIdentityPoolRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600006B RID: 107 RVA: 0x00002CE4 File Offset: 0x00000EE4
		internal DescribeIdentityResponse DescribeIdentity(DescribeIdentityRequest request)
		{
			DescribeIdentityRequestMarshaller marshaller = new DescribeIdentityRequestMarshaller();
			DescribeIdentityResponseUnmarshaller instance = DescribeIdentityResponseUnmarshaller.Instance;
			return base.Invoke<DescribeIdentityRequest, DescribeIdentityResponse>(request, marshaller, instance);
		}

		// Token: 0x0600006C RID: 108 RVA: 0x00002D08 File Offset: 0x00000F08
		public void DescribeIdentityAsync(string identityId, AmazonServiceCallback<DescribeIdentityRequest, DescribeIdentityResponse> callback, AsyncOptions options = null)
		{
			this.DescribeIdentityAsync(new DescribeIdentityRequest
			{
				IdentityId = identityId
			}, callback, options);
		}

		// Token: 0x0600006D RID: 109 RVA: 0x00002D2C File Offset: 0x00000F2C
		public void DescribeIdentityAsync(DescribeIdentityRequest request, AmazonServiceCallback<DescribeIdentityRequest, DescribeIdentityResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			DescribeIdentityRequestMarshaller marshaller = new DescribeIdentityRequestMarshaller();
			DescribeIdentityResponseUnmarshaller instance = DescribeIdentityResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<DescribeIdentityRequest, DescribeIdentityResponse> responseObject = new AmazonServiceResult<DescribeIdentityRequest, DescribeIdentityResponse>((DescribeIdentityRequest)req, (DescribeIdentityResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<DescribeIdentityRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600006E RID: 110 RVA: 0x00002D84 File Offset: 0x00000F84
		internal DescribeIdentityPoolResponse DescribeIdentityPool(DescribeIdentityPoolRequest request)
		{
			DescribeIdentityPoolRequestMarshaller marshaller = new DescribeIdentityPoolRequestMarshaller();
			DescribeIdentityPoolResponseUnmarshaller instance = DescribeIdentityPoolResponseUnmarshaller.Instance;
			return base.Invoke<DescribeIdentityPoolRequest, DescribeIdentityPoolResponse>(request, marshaller, instance);
		}

		// Token: 0x0600006F RID: 111 RVA: 0x00002DA8 File Offset: 0x00000FA8
		public void DescribeIdentityPoolAsync(string identityPoolId, AmazonServiceCallback<DescribeIdentityPoolRequest, DescribeIdentityPoolResponse> callback, AsyncOptions options = null)
		{
			this.DescribeIdentityPoolAsync(new DescribeIdentityPoolRequest
			{
				IdentityPoolId = identityPoolId
			}, callback, options);
		}

		// Token: 0x06000070 RID: 112 RVA: 0x00002DCC File Offset: 0x00000FCC
		public void DescribeIdentityPoolAsync(DescribeIdentityPoolRequest request, AmazonServiceCallback<DescribeIdentityPoolRequest, DescribeIdentityPoolResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			DescribeIdentityPoolRequestMarshaller marshaller = new DescribeIdentityPoolRequestMarshaller();
			DescribeIdentityPoolResponseUnmarshaller instance = DescribeIdentityPoolResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<DescribeIdentityPoolRequest, DescribeIdentityPoolResponse> responseObject = new AmazonServiceResult<DescribeIdentityPoolRequest, DescribeIdentityPoolResponse>((DescribeIdentityPoolRequest)req, (DescribeIdentityPoolResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<DescribeIdentityPoolRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000071 RID: 113 RVA: 0x00002E24 File Offset: 0x00001024
		internal GetCredentialsForIdentityResponse GetCredentialsForIdentity(GetCredentialsForIdentityRequest request)
		{
			GetCredentialsForIdentityRequestMarshaller marshaller = new GetCredentialsForIdentityRequestMarshaller();
			GetCredentialsForIdentityResponseUnmarshaller instance = GetCredentialsForIdentityResponseUnmarshaller.Instance;
			return base.Invoke<GetCredentialsForIdentityRequest, GetCredentialsForIdentityResponse>(request, marshaller, instance);
		}

		// Token: 0x06000072 RID: 114 RVA: 0x00002E48 File Offset: 0x00001048
		public void GetCredentialsForIdentityAsync(string identityId, AmazonServiceCallback<GetCredentialsForIdentityRequest, GetCredentialsForIdentityResponse> callback, AsyncOptions options = null)
		{
			this.GetCredentialsForIdentityAsync(new GetCredentialsForIdentityRequest
			{
				IdentityId = identityId
			}, callback, options);
		}

		// Token: 0x06000073 RID: 115 RVA: 0x00002E6C File Offset: 0x0000106C
		public void GetCredentialsForIdentityAsync(string identityId, Dictionary<string, string> logins, AmazonServiceCallback<GetCredentialsForIdentityRequest, GetCredentialsForIdentityResponse> callback, AsyncOptions options = null)
		{
			this.GetCredentialsForIdentityAsync(new GetCredentialsForIdentityRequest
			{
				IdentityId = identityId,
				Logins = logins
			}, callback, options);
		}

		// Token: 0x06000074 RID: 116 RVA: 0x00002E98 File Offset: 0x00001098
		public void GetCredentialsForIdentityAsync(GetCredentialsForIdentityRequest request, AmazonServiceCallback<GetCredentialsForIdentityRequest, GetCredentialsForIdentityResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetCredentialsForIdentityRequestMarshaller marshaller = new GetCredentialsForIdentityRequestMarshaller();
			GetCredentialsForIdentityResponseUnmarshaller instance = GetCredentialsForIdentityResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetCredentialsForIdentityRequest, GetCredentialsForIdentityResponse> responseObject = new AmazonServiceResult<GetCredentialsForIdentityRequest, GetCredentialsForIdentityResponse>((GetCredentialsForIdentityRequest)req, (GetCredentialsForIdentityResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetCredentialsForIdentityRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000075 RID: 117 RVA: 0x00002EF0 File Offset: 0x000010F0
		internal GetIdResponse GetId(GetIdRequest request)
		{
			GetIdRequestMarshaller marshaller = new GetIdRequestMarshaller();
			GetIdResponseUnmarshaller instance = GetIdResponseUnmarshaller.Instance;
			return base.Invoke<GetIdRequest, GetIdResponse>(request, marshaller, instance);
		}

		// Token: 0x06000076 RID: 118 RVA: 0x00002F14 File Offset: 0x00001114
		public void GetIdAsync(GetIdRequest request, AmazonServiceCallback<GetIdRequest, GetIdResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetIdRequestMarshaller marshaller = new GetIdRequestMarshaller();
			GetIdResponseUnmarshaller instance = GetIdResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetIdRequest, GetIdResponse> responseObject = new AmazonServiceResult<GetIdRequest, GetIdResponse>((GetIdRequest)req, (GetIdResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetIdRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000077 RID: 119 RVA: 0x00002F6C File Offset: 0x0000116C
		internal GetIdentityPoolRolesResponse GetIdentityPoolRoles(GetIdentityPoolRolesRequest request)
		{
			GetIdentityPoolRolesRequestMarshaller marshaller = new GetIdentityPoolRolesRequestMarshaller();
			GetIdentityPoolRolesResponseUnmarshaller instance = GetIdentityPoolRolesResponseUnmarshaller.Instance;
			return base.Invoke<GetIdentityPoolRolesRequest, GetIdentityPoolRolesResponse>(request, marshaller, instance);
		}

		// Token: 0x06000078 RID: 120 RVA: 0x00002F90 File Offset: 0x00001190
		public void GetIdentityPoolRolesAsync(string identityPoolId, AmazonServiceCallback<GetIdentityPoolRolesRequest, GetIdentityPoolRolesResponse> callback, AsyncOptions options = null)
		{
			this.GetIdentityPoolRolesAsync(new GetIdentityPoolRolesRequest
			{
				IdentityPoolId = identityPoolId
			}, callback, options);
		}

		// Token: 0x06000079 RID: 121 RVA: 0x00002FB4 File Offset: 0x000011B4
		public void GetIdentityPoolRolesAsync(GetIdentityPoolRolesRequest request, AmazonServiceCallback<GetIdentityPoolRolesRequest, GetIdentityPoolRolesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetIdentityPoolRolesRequestMarshaller marshaller = new GetIdentityPoolRolesRequestMarshaller();
			GetIdentityPoolRolesResponseUnmarshaller instance = GetIdentityPoolRolesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetIdentityPoolRolesRequest, GetIdentityPoolRolesResponse> responseObject = new AmazonServiceResult<GetIdentityPoolRolesRequest, GetIdentityPoolRolesResponse>((GetIdentityPoolRolesRequest)req, (GetIdentityPoolRolesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetIdentityPoolRolesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600007A RID: 122 RVA: 0x0000300C File Offset: 0x0000120C
		internal GetOpenIdTokenResponse GetOpenIdToken(GetOpenIdTokenRequest request)
		{
			GetOpenIdTokenRequestMarshaller marshaller = new GetOpenIdTokenRequestMarshaller();
			GetOpenIdTokenResponseUnmarshaller instance = GetOpenIdTokenResponseUnmarshaller.Instance;
			return base.Invoke<GetOpenIdTokenRequest, GetOpenIdTokenResponse>(request, marshaller, instance);
		}

		// Token: 0x0600007B RID: 123 RVA: 0x00003030 File Offset: 0x00001230
		public void GetOpenIdTokenAsync(string identityId, AmazonServiceCallback<GetOpenIdTokenRequest, GetOpenIdTokenResponse> callback, AsyncOptions options = null)
		{
			this.GetOpenIdTokenAsync(new GetOpenIdTokenRequest
			{
				IdentityId = identityId
			}, callback, options);
		}

		// Token: 0x0600007C RID: 124 RVA: 0x00003054 File Offset: 0x00001254
		public void GetOpenIdTokenAsync(GetOpenIdTokenRequest request, AmazonServiceCallback<GetOpenIdTokenRequest, GetOpenIdTokenResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetOpenIdTokenRequestMarshaller marshaller = new GetOpenIdTokenRequestMarshaller();
			GetOpenIdTokenResponseUnmarshaller instance = GetOpenIdTokenResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetOpenIdTokenRequest, GetOpenIdTokenResponse> responseObject = new AmazonServiceResult<GetOpenIdTokenRequest, GetOpenIdTokenResponse>((GetOpenIdTokenRequest)req, (GetOpenIdTokenResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetOpenIdTokenRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600007D RID: 125 RVA: 0x000030AC File Offset: 0x000012AC
		internal GetOpenIdTokenForDeveloperIdentityResponse GetOpenIdTokenForDeveloperIdentity(GetOpenIdTokenForDeveloperIdentityRequest request)
		{
			GetOpenIdTokenForDeveloperIdentityRequestMarshaller marshaller = new GetOpenIdTokenForDeveloperIdentityRequestMarshaller();
			GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller instance = GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller.Instance;
			return base.Invoke<GetOpenIdTokenForDeveloperIdentityRequest, GetOpenIdTokenForDeveloperIdentityResponse>(request, marshaller, instance);
		}

		// Token: 0x0600007E RID: 126 RVA: 0x000030D0 File Offset: 0x000012D0
		public void GetOpenIdTokenForDeveloperIdentityAsync(GetOpenIdTokenForDeveloperIdentityRequest request, AmazonServiceCallback<GetOpenIdTokenForDeveloperIdentityRequest, GetOpenIdTokenForDeveloperIdentityResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			GetOpenIdTokenForDeveloperIdentityRequestMarshaller marshaller = new GetOpenIdTokenForDeveloperIdentityRequestMarshaller();
			GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller instance = GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<GetOpenIdTokenForDeveloperIdentityRequest, GetOpenIdTokenForDeveloperIdentityResponse> responseObject = new AmazonServiceResult<GetOpenIdTokenForDeveloperIdentityRequest, GetOpenIdTokenForDeveloperIdentityResponse>((GetOpenIdTokenForDeveloperIdentityRequest)req, (GetOpenIdTokenForDeveloperIdentityResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<GetOpenIdTokenForDeveloperIdentityRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600007F RID: 127 RVA: 0x00003128 File Offset: 0x00001328
		internal ListIdentitiesResponse ListIdentities(ListIdentitiesRequest request)
		{
			ListIdentitiesRequestMarshaller marshaller = new ListIdentitiesRequestMarshaller();
			ListIdentitiesResponseUnmarshaller instance = ListIdentitiesResponseUnmarshaller.Instance;
			return base.Invoke<ListIdentitiesRequest, ListIdentitiesResponse>(request, marshaller, instance);
		}

		// Token: 0x06000080 RID: 128 RVA: 0x0000314C File Offset: 0x0000134C
		public void ListIdentitiesAsync(ListIdentitiesRequest request, AmazonServiceCallback<ListIdentitiesRequest, ListIdentitiesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			ListIdentitiesRequestMarshaller marshaller = new ListIdentitiesRequestMarshaller();
			ListIdentitiesResponseUnmarshaller instance = ListIdentitiesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<ListIdentitiesRequest, ListIdentitiesResponse> responseObject = new AmazonServiceResult<ListIdentitiesRequest, ListIdentitiesResponse>((ListIdentitiesRequest)req, (ListIdentitiesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<ListIdentitiesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000081 RID: 129 RVA: 0x000031A4 File Offset: 0x000013A4
		internal ListIdentityPoolsResponse ListIdentityPools(ListIdentityPoolsRequest request)
		{
			ListIdentityPoolsRequestMarshaller marshaller = new ListIdentityPoolsRequestMarshaller();
			ListIdentityPoolsResponseUnmarshaller instance = ListIdentityPoolsResponseUnmarshaller.Instance;
			return base.Invoke<ListIdentityPoolsRequest, ListIdentityPoolsResponse>(request, marshaller, instance);
		}

		// Token: 0x06000082 RID: 130 RVA: 0x000031C8 File Offset: 0x000013C8
		public void ListIdentityPoolsAsync(ListIdentityPoolsRequest request, AmazonServiceCallback<ListIdentityPoolsRequest, ListIdentityPoolsResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			ListIdentityPoolsRequestMarshaller marshaller = new ListIdentityPoolsRequestMarshaller();
			ListIdentityPoolsResponseUnmarshaller instance = ListIdentityPoolsResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<ListIdentityPoolsRequest, ListIdentityPoolsResponse> responseObject = new AmazonServiceResult<ListIdentityPoolsRequest, ListIdentityPoolsResponse>((ListIdentityPoolsRequest)req, (ListIdentityPoolsResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<ListIdentityPoolsRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000083 RID: 131 RVA: 0x00003220 File Offset: 0x00001420
		internal LookupDeveloperIdentityResponse LookupDeveloperIdentity(LookupDeveloperIdentityRequest request)
		{
			LookupDeveloperIdentityRequestMarshaller marshaller = new LookupDeveloperIdentityRequestMarshaller();
			LookupDeveloperIdentityResponseUnmarshaller instance = LookupDeveloperIdentityResponseUnmarshaller.Instance;
			return base.Invoke<LookupDeveloperIdentityRequest, LookupDeveloperIdentityResponse>(request, marshaller, instance);
		}

		// Token: 0x06000084 RID: 132 RVA: 0x00003244 File Offset: 0x00001444
		public void LookupDeveloperIdentityAsync(LookupDeveloperIdentityRequest request, AmazonServiceCallback<LookupDeveloperIdentityRequest, LookupDeveloperIdentityResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			LookupDeveloperIdentityRequestMarshaller marshaller = new LookupDeveloperIdentityRequestMarshaller();
			LookupDeveloperIdentityResponseUnmarshaller instance = LookupDeveloperIdentityResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<LookupDeveloperIdentityRequest, LookupDeveloperIdentityResponse> responseObject = new AmazonServiceResult<LookupDeveloperIdentityRequest, LookupDeveloperIdentityResponse>((LookupDeveloperIdentityRequest)req, (LookupDeveloperIdentityResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<LookupDeveloperIdentityRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000085 RID: 133 RVA: 0x0000329C File Offset: 0x0000149C
		internal MergeDeveloperIdentitiesResponse MergeDeveloperIdentities(MergeDeveloperIdentitiesRequest request)
		{
			MergeDeveloperIdentitiesRequestMarshaller marshaller = new MergeDeveloperIdentitiesRequestMarshaller();
			MergeDeveloperIdentitiesResponseUnmarshaller instance = MergeDeveloperIdentitiesResponseUnmarshaller.Instance;
			return base.Invoke<MergeDeveloperIdentitiesRequest, MergeDeveloperIdentitiesResponse>(request, marshaller, instance);
		}

		// Token: 0x06000086 RID: 134 RVA: 0x000032C0 File Offset: 0x000014C0
		public void MergeDeveloperIdentitiesAsync(MergeDeveloperIdentitiesRequest request, AmazonServiceCallback<MergeDeveloperIdentitiesRequest, MergeDeveloperIdentitiesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			MergeDeveloperIdentitiesRequestMarshaller marshaller = new MergeDeveloperIdentitiesRequestMarshaller();
			MergeDeveloperIdentitiesResponseUnmarshaller instance = MergeDeveloperIdentitiesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<MergeDeveloperIdentitiesRequest, MergeDeveloperIdentitiesResponse> responseObject = new AmazonServiceResult<MergeDeveloperIdentitiesRequest, MergeDeveloperIdentitiesResponse>((MergeDeveloperIdentitiesRequest)req, (MergeDeveloperIdentitiesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<MergeDeveloperIdentitiesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000087 RID: 135 RVA: 0x00003318 File Offset: 0x00001518
		internal SetIdentityPoolRolesResponse SetIdentityPoolRoles(SetIdentityPoolRolesRequest request)
		{
			SetIdentityPoolRolesRequestMarshaller marshaller = new SetIdentityPoolRolesRequestMarshaller();
			SetIdentityPoolRolesResponseUnmarshaller instance = SetIdentityPoolRolesResponseUnmarshaller.Instance;
			return base.Invoke<SetIdentityPoolRolesRequest, SetIdentityPoolRolesResponse>(request, marshaller, instance);
		}

		// Token: 0x06000088 RID: 136 RVA: 0x0000333C File Offset: 0x0000153C
		public void SetIdentityPoolRolesAsync(string identityPoolId, Dictionary<string, string> roles, AmazonServiceCallback<SetIdentityPoolRolesRequest, SetIdentityPoolRolesResponse> callback, AsyncOptions options = null)
		{
			this.SetIdentityPoolRolesAsync(new SetIdentityPoolRolesRequest
			{
				IdentityPoolId = identityPoolId,
				Roles = roles
			}, callback, options);
		}

		// Token: 0x06000089 RID: 137 RVA: 0x00003368 File Offset: 0x00001568
		public void SetIdentityPoolRolesAsync(SetIdentityPoolRolesRequest request, AmazonServiceCallback<SetIdentityPoolRolesRequest, SetIdentityPoolRolesResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			SetIdentityPoolRolesRequestMarshaller marshaller = new SetIdentityPoolRolesRequestMarshaller();
			SetIdentityPoolRolesResponseUnmarshaller instance = SetIdentityPoolRolesResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<SetIdentityPoolRolesRequest, SetIdentityPoolRolesResponse> responseObject = new AmazonServiceResult<SetIdentityPoolRolesRequest, SetIdentityPoolRolesResponse>((SetIdentityPoolRolesRequest)req, (SetIdentityPoolRolesResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<SetIdentityPoolRolesRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600008A RID: 138 RVA: 0x000033C0 File Offset: 0x000015C0
		internal UnlinkDeveloperIdentityResponse UnlinkDeveloperIdentity(UnlinkDeveloperIdentityRequest request)
		{
			UnlinkDeveloperIdentityRequestMarshaller marshaller = new UnlinkDeveloperIdentityRequestMarshaller();
			UnlinkDeveloperIdentityResponseUnmarshaller instance = UnlinkDeveloperIdentityResponseUnmarshaller.Instance;
			return base.Invoke<UnlinkDeveloperIdentityRequest, UnlinkDeveloperIdentityResponse>(request, marshaller, instance);
		}

		// Token: 0x0600008B RID: 139 RVA: 0x000033E4 File Offset: 0x000015E4
		public void UnlinkDeveloperIdentityAsync(UnlinkDeveloperIdentityRequest request, AmazonServiceCallback<UnlinkDeveloperIdentityRequest, UnlinkDeveloperIdentityResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			UnlinkDeveloperIdentityRequestMarshaller marshaller = new UnlinkDeveloperIdentityRequestMarshaller();
			UnlinkDeveloperIdentityResponseUnmarshaller instance = UnlinkDeveloperIdentityResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<UnlinkDeveloperIdentityRequest, UnlinkDeveloperIdentityResponse> responseObject = new AmazonServiceResult<UnlinkDeveloperIdentityRequest, UnlinkDeveloperIdentityResponse>((UnlinkDeveloperIdentityRequest)req, (UnlinkDeveloperIdentityResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<UnlinkDeveloperIdentityRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600008C RID: 140 RVA: 0x0000343C File Offset: 0x0000163C
		internal UnlinkIdentityResponse UnlinkIdentity(UnlinkIdentityRequest request)
		{
			UnlinkIdentityRequestMarshaller marshaller = new UnlinkIdentityRequestMarshaller();
			UnlinkIdentityResponseUnmarshaller instance = UnlinkIdentityResponseUnmarshaller.Instance;
			return base.Invoke<UnlinkIdentityRequest, UnlinkIdentityResponse>(request, marshaller, instance);
		}

		// Token: 0x0600008D RID: 141 RVA: 0x00003460 File Offset: 0x00001660
		public void UnlinkIdentityAsync(UnlinkIdentityRequest request, AmazonServiceCallback<UnlinkIdentityRequest, UnlinkIdentityResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			UnlinkIdentityRequestMarshaller marshaller = new UnlinkIdentityRequestMarshaller();
			UnlinkIdentityResponseUnmarshaller instance = UnlinkIdentityResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<UnlinkIdentityRequest, UnlinkIdentityResponse> responseObject = new AmazonServiceResult<UnlinkIdentityRequest, UnlinkIdentityResponse>((UnlinkIdentityRequest)req, (UnlinkIdentityResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<UnlinkIdentityRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x0600008E RID: 142 RVA: 0x000034B8 File Offset: 0x000016B8
		internal UpdateIdentityPoolResponse UpdateIdentityPool(UpdateIdentityPoolRequest request)
		{
			UpdateIdentityPoolRequestMarshaller marshaller = new UpdateIdentityPoolRequestMarshaller();
			UpdateIdentityPoolResponseUnmarshaller instance = UpdateIdentityPoolResponseUnmarshaller.Instance;
			return base.Invoke<UpdateIdentityPoolRequest, UpdateIdentityPoolResponse>(request, marshaller, instance);
		}

		// Token: 0x0600008F RID: 143 RVA: 0x000034DC File Offset: 0x000016DC
		public void UpdateIdentityPoolAsync(UpdateIdentityPoolRequest request, AmazonServiceCallback<UpdateIdentityPoolRequest, UpdateIdentityPoolResponse> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			UpdateIdentityPoolRequestMarshaller marshaller = new UpdateIdentityPoolRequestMarshaller();
			UpdateIdentityPoolResponseUnmarshaller instance = UpdateIdentityPoolResponseUnmarshaller.Instance;
			Action<AmazonWebServiceRequest, AmazonWebServiceResponse, Exception, AsyncOptions> callbackHelper = null;
			if (callback != null)
			{
				callbackHelper = delegate(AmazonWebServiceRequest req, AmazonWebServiceResponse res, Exception ex, AsyncOptions ao)
				{
					AmazonServiceResult<UpdateIdentityPoolRequest, UpdateIdentityPoolResponse> responseObject = new AmazonServiceResult<UpdateIdentityPoolRequest, UpdateIdentityPoolResponse>((UpdateIdentityPoolRequest)req, (UpdateIdentityPoolResponse)res, ex, ao.State);
					callback(responseObject);
				};
			}
			base.BeginInvoke<UpdateIdentityPoolRequest>(request, marshaller, instance, options, callbackHelper);
		}

		// Token: 0x06000090 RID: 144 RVA: 0x00003532 File Offset: 0x00001732
		IClientConfig IAmazonService.get_Config()
		{
			return base.Config;
		}
	}
}
