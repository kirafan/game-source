﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity
{
	// Token: 0x02000007 RID: 7
	[Serializable]
	public class AmazonCognitoIdentityException : AmazonServiceException
	{
		// Token: 0x06000042 RID: 66 RVA: 0x0000296E File Offset: 0x00000B6E
		public AmazonCognitoIdentityException(string message) : base(message)
		{
		}

		// Token: 0x06000043 RID: 67 RVA: 0x00002977 File Offset: 0x00000B77
		public AmazonCognitoIdentityException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000044 RID: 68 RVA: 0x00002981 File Offset: 0x00000B81
		public AmazonCognitoIdentityException(Exception innerException) : base(innerException.Message, innerException)
		{
		}

		// Token: 0x06000045 RID: 69 RVA: 0x00002990 File Offset: 0x00000B90
		public AmazonCognitoIdentityException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000046 RID: 70 RVA: 0x0000299F File Offset: 0x00000B9F
		public AmazonCognitoIdentityException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000047 RID: 71 RVA: 0x000029B0 File Offset: 0x00000BB0
		protected AmazonCognitoIdentityException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
