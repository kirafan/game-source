﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity
{
	// Token: 0x0200000A RID: 10
	public class MappingRuleMatchType : ConstantClass
	{
		// Token: 0x06000050 RID: 80 RVA: 0x000029BA File Offset: 0x00000BBA
		public MappingRuleMatchType(string value) : base(value)
		{
		}

		// Token: 0x06000051 RID: 81 RVA: 0x00002A23 File Offset: 0x00000C23
		public static MappingRuleMatchType FindValue(string value)
		{
			return ConstantClass.FindValue<MappingRuleMatchType>(value);
		}

		// Token: 0x06000052 RID: 82 RVA: 0x00002A2B File Offset: 0x00000C2B
		public static implicit operator MappingRuleMatchType(string value)
		{
			return MappingRuleMatchType.FindValue(value);
		}

		// Token: 0x04000019 RID: 25
		public static readonly MappingRuleMatchType Contains = new MappingRuleMatchType("Contains");

		// Token: 0x0400001A RID: 26
		public new static readonly MappingRuleMatchType Equals = new MappingRuleMatchType("Equals");

		// Token: 0x0400001B RID: 27
		public static readonly MappingRuleMatchType NotEqual = new MappingRuleMatchType("NotEqual");

		// Token: 0x0400001C RID: 28
		public static readonly MappingRuleMatchType StartsWith = new MappingRuleMatchType("StartsWith");
	}
}
