﻿using System;

namespace Amazon.CognitoIdentity
{
	// Token: 0x02000004 RID: 4
	public class AmazonCognitoIdentityResult<TResponse>
	{
		// Token: 0x1700000A RID: 10
		// (get) Token: 0x06000032 RID: 50 RVA: 0x0000287F File Offset: 0x00000A7F
		// (set) Token: 0x06000033 RID: 51 RVA: 0x00002887 File Offset: 0x00000A87
		public TResponse Response { get; internal set; }

		// Token: 0x1700000B RID: 11
		// (get) Token: 0x06000034 RID: 52 RVA: 0x00002890 File Offset: 0x00000A90
		// (set) Token: 0x06000035 RID: 53 RVA: 0x00002898 File Offset: 0x00000A98
		public Exception Exception { get; internal set; }

		// Token: 0x1700000C RID: 12
		// (get) Token: 0x06000036 RID: 54 RVA: 0x000028A1 File Offset: 0x00000AA1
		// (set) Token: 0x06000037 RID: 55 RVA: 0x000028A9 File Offset: 0x00000AA9
		public object State { get; internal set; }

		// Token: 0x06000038 RID: 56 RVA: 0x000028B2 File Offset: 0x00000AB2
		public AmazonCognitoIdentityResult(object state)
		{
			this.State = state;
		}

		// Token: 0x06000039 RID: 57 RVA: 0x000028C1 File Offset: 0x00000AC1
		public AmazonCognitoIdentityResult(TResponse response, Exception exception, object state)
		{
			this.Response = response;
			this.Exception = exception;
			this.State = state;
		}
	}
}
