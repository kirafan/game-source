﻿using System;
using Amazon.Runtime;
using Amazon.Util.Internal;

namespace Amazon.CognitoIdentity
{
	// Token: 0x02000006 RID: 6
	public class AmazonCognitoIdentityConfig : ClientConfig
	{
		// Token: 0x0600003D RID: 61 RVA: 0x00002929 File Offset: 0x00000B29
		public AmazonCognitoIdentityConfig()
		{
			base.AuthenticationServiceName = "cognito-identity";
		}

		// Token: 0x1700000D RID: 13
		// (get) Token: 0x0600003E RID: 62 RVA: 0x00002947 File Offset: 0x00000B47
		public override string RegionEndpointServiceName
		{
			get
			{
				return "cognito-identity";
			}
		}

		// Token: 0x1700000E RID: 14
		// (get) Token: 0x0600003F RID: 63 RVA: 0x0000294E File Offset: 0x00000B4E
		public override string ServiceVersion
		{
			get
			{
				return "2014-06-30";
			}
		}

		// Token: 0x1700000F RID: 15
		// (get) Token: 0x06000040 RID: 64 RVA: 0x00002955 File Offset: 0x00000B55
		public override string UserAgent
		{
			get
			{
				return this._userAgent;
			}
		}

		// Token: 0x04000013 RID: 19
		private static readonly string UserAgentString = InternalSDKUtils.BuildUserAgentString("3.3.2.10");

		// Token: 0x04000014 RID: 20
		private string _userAgent = AmazonCognitoIdentityConfig.UserAgentString;
	}
}
