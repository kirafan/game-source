﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity
{
	// Token: 0x02000009 RID: 9
	public class ErrorCode : ConstantClass
	{
		// Token: 0x0600004C RID: 76 RVA: 0x000029BA File Offset: 0x00000BBA
		public ErrorCode(string value) : base(value)
		{
		}

		// Token: 0x0600004D RID: 77 RVA: 0x000029F3 File Offset: 0x00000BF3
		public static ErrorCode FindValue(string value)
		{
			return ConstantClass.FindValue<ErrorCode>(value);
		}

		// Token: 0x0600004E RID: 78 RVA: 0x000029FB File Offset: 0x00000BFB
		public static implicit operator ErrorCode(string value)
		{
			return ErrorCode.FindValue(value);
		}

		// Token: 0x04000017 RID: 23
		public static readonly ErrorCode AccessDenied = new ErrorCode("AccessDenied");

		// Token: 0x04000018 RID: 24
		public static readonly ErrorCode InternalServerError = new ErrorCode("InternalServerError");
	}
}
