﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity
{
	// Token: 0x0200000B RID: 11
	public class RoleMappingType : ConstantClass
	{
		// Token: 0x06000054 RID: 84 RVA: 0x000029BA File Offset: 0x00000BBA
		public RoleMappingType(string value) : base(value)
		{
		}

		// Token: 0x06000055 RID: 85 RVA: 0x00002A71 File Offset: 0x00000C71
		public static RoleMappingType FindValue(string value)
		{
			return ConstantClass.FindValue<RoleMappingType>(value);
		}

		// Token: 0x06000056 RID: 86 RVA: 0x00002A79 File Offset: 0x00000C79
		public static implicit operator RoleMappingType(string value)
		{
			return RoleMappingType.FindValue(value);
		}

		// Token: 0x0400001D RID: 29
		public static readonly RoleMappingType Rules = new RoleMappingType("Rules");

		// Token: 0x0400001E RID: 30
		public static readonly RoleMappingType Token = new RoleMappingType("Token");
	}
}
