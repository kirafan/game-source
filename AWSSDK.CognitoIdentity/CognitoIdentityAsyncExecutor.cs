﻿using System;
using System.Threading;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Util;

namespace Amazon.CognitoIdentity
{
	// Token: 0x02000005 RID: 5
	internal class CognitoIdentityAsyncExecutor
	{
		// Token: 0x0600003A RID: 58 RVA: 0x000028DE File Offset: 0x00000ADE
		public static void ExecuteAsync<T>(Func<T> function, AsyncOptions options, AmazonCognitoIdentityCallback<T> callback)
		{
			ThreadPool.QueueUserWorkItem(delegate(object state)
			{
				T result = default(T);
				Exception exception = null;
				try
				{
					result = function();
				}
				catch (Exception exception)
				{
					Exception exception3;
					exception = exception3;
				}
				if (callback != null)
				{
					if (options.ExecuteCallbackOnMainThread)
					{
						UnityRequestQueue.Instance.ExecuteOnMainThread(delegate
						{
							callback(new AmazonCognitoIdentityResult<T>(result, exception, options.State));
						});
						return;
					}
					try
					{
						callback(new AmazonCognitoIdentityResult<T>(result, exception, options.State));
					}
					catch (Exception exception2)
					{
						CognitoIdentityAsyncExecutor.Logger.Error(exception2, "An unhandled exception was thrown from the callback method {0}.", new object[]
						{
							callback.Method.Name
						});
					}
				}
			});
		}

		// Token: 0x04000012 RID: 18
		private static Logger Logger = Logger.GetLogger(typeof(CognitoIdentityAsyncExecutor));
	}
}
