﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000033 RID: 51
	public class LookupDeveloperIdentityResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060001EF RID: 495 RVA: 0x00004282 File Offset: 0x00002482
		// (set) Token: 0x060001F0 RID: 496 RVA: 0x0000428A File Offset: 0x0000248A
		public List<string> DeveloperUserIdentifierList
		{
			get
			{
				return this._developerUserIdentifierList;
			}
			set
			{
				this._developerUserIdentifierList = value;
			}
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x00004293 File Offset: 0x00002493
		internal bool IsSetDeveloperUserIdentifierList()
		{
			return this._developerUserIdentifierList != null && this._developerUserIdentifierList.Count > 0;
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060001F2 RID: 498 RVA: 0x000042AD File Offset: 0x000024AD
		// (set) Token: 0x060001F3 RID: 499 RVA: 0x000042B5 File Offset: 0x000024B5
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x000042BE File Offset: 0x000024BE
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060001F5 RID: 501 RVA: 0x000042C9 File Offset: 0x000024C9
		// (set) Token: 0x060001F6 RID: 502 RVA: 0x000042D1 File Offset: 0x000024D1
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x000042DA File Offset: 0x000024DA
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x04000074 RID: 116
		private List<string> _developerUserIdentifierList = new List<string>();

		// Token: 0x04000075 RID: 117
		private string _identityId;

		// Token: 0x04000076 RID: 118
		private string _nextToken;
	}
}
