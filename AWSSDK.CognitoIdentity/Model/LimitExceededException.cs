﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200002D RID: 45
	[Serializable]
	public class LimitExceededException : AmazonCognitoIdentityException
	{
		// Token: 0x060001B4 RID: 436 RVA: 0x00003650 File Offset: 0x00001850
		public LimitExceededException(string message) : base(message)
		{
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x00003659 File Offset: 0x00001859
		public LimitExceededException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00003663 File Offset: 0x00001863
		public LimitExceededException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x0000366C File Offset: 0x0000186C
		public LimitExceededException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x0000367D File Offset: 0x0000187D
		public LimitExceededException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x0000368C File Offset: 0x0000188C
		protected LimitExceededException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
