﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200001F RID: 31
	public class GetCredentialsForIdentityResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000149 RID: 329 RVA: 0x00003C8E File Offset: 0x00001E8E
		// (set) Token: 0x0600014A RID: 330 RVA: 0x00003C96 File Offset: 0x00001E96
		public Credentials Credentials
		{
			get
			{
				return this._credentials;
			}
			set
			{
				this._credentials = value;
			}
		}

		// Token: 0x0600014B RID: 331 RVA: 0x00003C9F File Offset: 0x00001E9F
		internal bool IsSetCredentials()
		{
			return this._credentials != null;
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x0600014C RID: 332 RVA: 0x00003CAA File Offset: 0x00001EAA
		// (set) Token: 0x0600014D RID: 333 RVA: 0x00003CB2 File Offset: 0x00001EB2
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x0600014E RID: 334 RVA: 0x00003CBB File Offset: 0x00001EBB
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x0400004A RID: 74
		private Credentials _credentials;

		// Token: 0x0400004B RID: 75
		private string _identityId;
	}
}
