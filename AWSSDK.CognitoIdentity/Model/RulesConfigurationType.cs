﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200003B RID: 59
	public class RulesConfigurationType
	{
		// Token: 0x17000073 RID: 115
		// (get) Token: 0x06000233 RID: 563 RVA: 0x00004451 File Offset: 0x00002651
		// (set) Token: 0x06000234 RID: 564 RVA: 0x00004459 File Offset: 0x00002659
		public List<MappingRule> Rules
		{
			get
			{
				return this._rules;
			}
			set
			{
				this._rules = value;
			}
		}

		// Token: 0x06000235 RID: 565 RVA: 0x00004462 File Offset: 0x00002662
		internal bool IsSetRules()
		{
			return this._rules != null && this._rules.Count > 0;
		}

		// Token: 0x04000083 RID: 131
		private List<MappingRule> _rules = new List<MappingRule>();
	}
}
