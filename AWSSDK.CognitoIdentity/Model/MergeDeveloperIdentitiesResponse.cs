﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000036 RID: 54
	public class MergeDeveloperIdentitiesResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700006F RID: 111
		// (get) Token: 0x06000213 RID: 531 RVA: 0x000043DB File Offset: 0x000025DB
		// (set) Token: 0x06000214 RID: 532 RVA: 0x000043E3 File Offset: 0x000025E3
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x06000215 RID: 533 RVA: 0x000043EC File Offset: 0x000025EC
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x0400007F RID: 127
		private string _identityId;
	}
}
