﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200001A RID: 26
	public class DescribeIdentityRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000032 RID: 50
		// (get) Token: 0x06000122 RID: 290 RVA: 0x00003B52 File Offset: 0x00001D52
		// (set) Token: 0x06000123 RID: 291 RVA: 0x00003B5A File Offset: 0x00001D5A
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x06000124 RID: 292 RVA: 0x00003B63 File Offset: 0x00001D63
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x04000042 RID: 66
		private string _identityId;
	}
}
