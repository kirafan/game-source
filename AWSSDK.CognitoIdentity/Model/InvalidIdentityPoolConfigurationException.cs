﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200002B RID: 43
	[Serializable]
	public class InvalidIdentityPoolConfigurationException : AmazonCognitoIdentityException
	{
		// Token: 0x060001A8 RID: 424 RVA: 0x00003650 File Offset: 0x00001850
		public InvalidIdentityPoolConfigurationException(string message) : base(message)
		{
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x00003659 File Offset: 0x00001859
		public InvalidIdentityPoolConfigurationException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060001AA RID: 426 RVA: 0x00003663 File Offset: 0x00001863
		public InvalidIdentityPoolConfigurationException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x060001AB RID: 427 RVA: 0x0000366C File Offset: 0x0000186C
		public InvalidIdentityPoolConfigurationException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060001AC RID: 428 RVA: 0x0000367D File Offset: 0x0000187D
		public InvalidIdentityPoolConfigurationException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060001AD RID: 429 RVA: 0x0000368C File Offset: 0x0000188C
		protected InvalidIdentityPoolConfigurationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
