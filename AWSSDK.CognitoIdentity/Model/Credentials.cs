﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200000F RID: 15
	public class Credentials : AWSCredentials
	{
		// Token: 0x060000AB RID: 171 RVA: 0x0000353A File Offset: 0x0000173A
		public override ImmutableCredentials GetCredentials()
		{
			if (this._credentials == null)
			{
				this._credentials = new ImmutableCredentials(this.AccessKeyId, this.SecretKey, this.SessionToken);
			}
			return this._credentials.Copy();
		}

		// Token: 0x17000010 RID: 16
		// (get) Token: 0x060000AC RID: 172 RVA: 0x0000356C File Offset: 0x0000176C
		// (set) Token: 0x060000AD RID: 173 RVA: 0x00003574 File Offset: 0x00001774
		public string AccessKeyId
		{
			get
			{
				return this._accessKeyId;
			}
			set
			{
				this._accessKeyId = value;
			}
		}

		// Token: 0x060000AE RID: 174 RVA: 0x0000357D File Offset: 0x0000177D
		internal bool IsSetAccessKeyId()
		{
			return this._accessKeyId != null;
		}

		// Token: 0x17000011 RID: 17
		// (get) Token: 0x060000AF RID: 175 RVA: 0x00003588 File Offset: 0x00001788
		// (set) Token: 0x060000B0 RID: 176 RVA: 0x00003595 File Offset: 0x00001795
		public DateTime Expiration
		{
			get
			{
				return this._expiration.GetValueOrDefault();
			}
			set
			{
				this._expiration = new DateTime?(value);
			}
		}

		// Token: 0x060000B1 RID: 177 RVA: 0x000035A3 File Offset: 0x000017A3
		internal bool IsSetExpiration()
		{
			return this._expiration != null;
		}

		// Token: 0x17000012 RID: 18
		// (get) Token: 0x060000B2 RID: 178 RVA: 0x000035B0 File Offset: 0x000017B0
		// (set) Token: 0x060000B3 RID: 179 RVA: 0x000035B8 File Offset: 0x000017B8
		public string SecretKey
		{
			get
			{
				return this._secretKey;
			}
			set
			{
				this._secretKey = value;
			}
		}

		// Token: 0x060000B4 RID: 180 RVA: 0x000035C1 File Offset: 0x000017C1
		internal bool IsSetSecretKey()
		{
			return this._secretKey != null;
		}

		// Token: 0x17000013 RID: 19
		// (get) Token: 0x060000B5 RID: 181 RVA: 0x000035CC File Offset: 0x000017CC
		// (set) Token: 0x060000B6 RID: 182 RVA: 0x000035D4 File Offset: 0x000017D4
		public string SessionToken
		{
			get
			{
				return this._sessionToken;
			}
			set
			{
				this._sessionToken = value;
			}
		}

		// Token: 0x060000B7 RID: 183 RVA: 0x000035DD File Offset: 0x000017DD
		internal bool IsSetSessionToken()
		{
			return this._sessionToken != null;
		}

		// Token: 0x0400001F RID: 31
		private ImmutableCredentials _credentials;

		// Token: 0x04000020 RID: 32
		private string _accessKeyId;

		// Token: 0x04000021 RID: 33
		private DateTime? _expiration;

		// Token: 0x04000022 RID: 34
		private string _secretKey;

		// Token: 0x04000023 RID: 35
		private string _sessionToken;
	}
}
