﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000034 RID: 52
	public class MappingRule
	{
		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060001F9 RID: 505 RVA: 0x000042F8 File Offset: 0x000024F8
		// (set) Token: 0x060001FA RID: 506 RVA: 0x00004300 File Offset: 0x00002500
		public string Claim
		{
			get
			{
				return this._claim;
			}
			set
			{
				this._claim = value;
			}
		}

		// Token: 0x060001FB RID: 507 RVA: 0x00004309 File Offset: 0x00002509
		internal bool IsSetClaim()
		{
			return this._claim != null;
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060001FC RID: 508 RVA: 0x00004314 File Offset: 0x00002514
		// (set) Token: 0x060001FD RID: 509 RVA: 0x0000431C File Offset: 0x0000251C
		public MappingRuleMatchType MatchType
		{
			get
			{
				return this._matchType;
			}
			set
			{
				this._matchType = value;
			}
		}

		// Token: 0x060001FE RID: 510 RVA: 0x00004325 File Offset: 0x00002525
		internal bool IsSetMatchType()
		{
			return this._matchType != null;
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060001FF RID: 511 RVA: 0x00004333 File Offset: 0x00002533
		// (set) Token: 0x06000200 RID: 512 RVA: 0x0000433B File Offset: 0x0000253B
		public string RoleARN
		{
			get
			{
				return this._roleARN;
			}
			set
			{
				this._roleARN = value;
			}
		}

		// Token: 0x06000201 RID: 513 RVA: 0x00004344 File Offset: 0x00002544
		internal bool IsSetRoleARN()
		{
			return this._roleARN != null;
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x06000202 RID: 514 RVA: 0x0000434F File Offset: 0x0000254F
		// (set) Token: 0x06000203 RID: 515 RVA: 0x00004357 File Offset: 0x00002557
		public string Value
		{
			get
			{
				return this._value;
			}
			set
			{
				this._value = value;
			}
		}

		// Token: 0x06000204 RID: 516 RVA: 0x00004360 File Offset: 0x00002560
		internal bool IsSetValue()
		{
			return this._value != null;
		}

		// Token: 0x04000077 RID: 119
		private string _claim;

		// Token: 0x04000078 RID: 120
		private MappingRuleMatchType _matchType;

		// Token: 0x04000079 RID: 121
		private string _roleARN;

		// Token: 0x0400007A RID: 122
		private string _value;
	}
}
