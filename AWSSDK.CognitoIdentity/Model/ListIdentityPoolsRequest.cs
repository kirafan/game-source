﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000030 RID: 48
	public class ListIdentityPoolsRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x1700005B RID: 91
		// (get) Token: 0x060001D1 RID: 465 RVA: 0x0000414C File Offset: 0x0000234C
		// (set) Token: 0x060001D2 RID: 466 RVA: 0x00004159 File Offset: 0x00002359
		public int MaxResults
		{
			get
			{
				return this._maxResults.GetValueOrDefault();
			}
			set
			{
				this._maxResults = new int?(value);
			}
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x00004167 File Offset: 0x00002367
		internal bool IsSetMaxResults()
		{
			return this._maxResults != null;
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060001D4 RID: 468 RVA: 0x00004174 File Offset: 0x00002374
		// (set) Token: 0x060001D5 RID: 469 RVA: 0x0000417C File Offset: 0x0000237C
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x00004185 File Offset: 0x00002385
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x0400006B RID: 107
		private int? _maxResults;

		// Token: 0x0400006C RID: 108
		private string _nextToken;
	}
}
