﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000026 RID: 38
	public class GetOpenIdTokenRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x1700004A RID: 74
		// (get) Token: 0x06000180 RID: 384 RVA: 0x00003EDA File Offset: 0x000020DA
		// (set) Token: 0x06000181 RID: 385 RVA: 0x00003EE2 File Offset: 0x000020E2
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00003EEB File Offset: 0x000020EB
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x06000183 RID: 387 RVA: 0x00003EF6 File Offset: 0x000020F6
		// (set) Token: 0x06000184 RID: 388 RVA: 0x00003EFE File Offset: 0x000020FE
		public Dictionary<string, string> Logins
		{
			get
			{
				return this._logins;
			}
			set
			{
				this._logins = value;
			}
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00003F07 File Offset: 0x00002107
		internal bool IsSetLogins()
		{
			return this._logins != null && this._logins.Count > 0;
		}

		// Token: 0x0400005A RID: 90
		private string _identityId;

		// Token: 0x0400005B RID: 91
		private Dictionary<string, string> _logins = new Dictionary<string, string>();
	}
}
