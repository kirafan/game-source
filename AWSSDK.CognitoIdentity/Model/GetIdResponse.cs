﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000023 RID: 35
	public class GetIdResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000043 RID: 67
		// (get) Token: 0x06000168 RID: 360 RVA: 0x00003DE8 File Offset: 0x00001FE8
		// (set) Token: 0x06000169 RID: 361 RVA: 0x00003DF0 File Offset: 0x00001FF0
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x0600016A RID: 362 RVA: 0x00003DF9 File Offset: 0x00001FF9
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x04000053 RID: 83
		private string _identityId;
	}
}
