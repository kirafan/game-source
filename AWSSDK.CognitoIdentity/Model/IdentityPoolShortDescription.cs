﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000029 RID: 41
	public class IdentityPoolShortDescription
	{
		// Token: 0x17000052 RID: 82
		// (get) Token: 0x0600019B RID: 411 RVA: 0x00004016 File Offset: 0x00002216
		// (set) Token: 0x0600019C RID: 412 RVA: 0x0000401E File Offset: 0x0000221E
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x0600019D RID: 413 RVA: 0x00004027 File Offset: 0x00002227
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x0600019E RID: 414 RVA: 0x00004032 File Offset: 0x00002232
		// (set) Token: 0x0600019F RID: 415 RVA: 0x0000403A File Offset: 0x0000223A
		public string IdentityPoolName
		{
			get
			{
				return this._identityPoolName;
			}
			set
			{
				this._identityPoolName = value;
			}
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x00004043 File Offset: 0x00002243
		internal bool IsSetIdentityPoolName()
		{
			return this._identityPoolName != null;
		}

		// Token: 0x04000062 RID: 98
		private string _identityPoolId;

		// Token: 0x04000063 RID: 99
		private string _identityPoolName;
	}
}
