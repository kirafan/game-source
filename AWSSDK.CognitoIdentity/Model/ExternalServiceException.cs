﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200001D RID: 29
	[Serializable]
	public class ExternalServiceException : AmazonCognitoIdentityException
	{
		// Token: 0x06000139 RID: 313 RVA: 0x00003650 File Offset: 0x00001850
		public ExternalServiceException(string message) : base(message)
		{
		}

		// Token: 0x0600013A RID: 314 RVA: 0x00003659 File Offset: 0x00001859
		public ExternalServiceException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600013B RID: 315 RVA: 0x00003663 File Offset: 0x00001863
		public ExternalServiceException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x0600013C RID: 316 RVA: 0x0000366C File Offset: 0x0000186C
		public ExternalServiceException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600013D RID: 317 RVA: 0x0000367D File Offset: 0x0000187D
		public ExternalServiceException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600013E RID: 318 RVA: 0x0000368C File Offset: 0x0000188C
		protected ExternalServiceException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
