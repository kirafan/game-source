﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000043 RID: 67
	public class UnprocessedIdentityId
	{
		// Token: 0x1700007E RID: 126
		// (get) Token: 0x06000261 RID: 609 RVA: 0x0000461F File Offset: 0x0000281F
		// (set) Token: 0x06000262 RID: 610 RVA: 0x00004627 File Offset: 0x00002827
		public ErrorCode ErrorCode
		{
			get
			{
				return this._errorCode;
			}
			set
			{
				this._errorCode = value;
			}
		}

		// Token: 0x06000263 RID: 611 RVA: 0x00004630 File Offset: 0x00002830
		internal bool IsSetErrorCode()
		{
			return this._errorCode != null;
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000264 RID: 612 RVA: 0x0000463E File Offset: 0x0000283E
		// (set) Token: 0x06000265 RID: 613 RVA: 0x00004646 File Offset: 0x00002846
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x06000266 RID: 614 RVA: 0x0000464F File Offset: 0x0000284F
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x0400008E RID: 142
		private ErrorCode _errorCode;

		// Token: 0x0400008F RID: 143
		private string _identityId;
	}
}
