﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000045 RID: 69
	public class UpdateIdentityPoolResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000088 RID: 136
		// (get) Token: 0x06000281 RID: 641 RVA: 0x000047B6 File Offset: 0x000029B6
		// (set) Token: 0x06000282 RID: 642 RVA: 0x000047C3 File Offset: 0x000029C3
		public bool AllowUnauthenticatedIdentities
		{
			get
			{
				return this._allowUnauthenticatedIdentities.GetValueOrDefault();
			}
			set
			{
				this._allowUnauthenticatedIdentities = new bool?(value);
			}
		}

		// Token: 0x06000283 RID: 643 RVA: 0x000047D1 File Offset: 0x000029D1
		internal bool IsSetAllowUnauthenticatedIdentities()
		{
			return this._allowUnauthenticatedIdentities != null;
		}

		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000284 RID: 644 RVA: 0x000047DE File Offset: 0x000029DE
		// (set) Token: 0x06000285 RID: 645 RVA: 0x000047E6 File Offset: 0x000029E6
		public List<CognitoIdentityProviderInfo> CognitoIdentityProviders
		{
			get
			{
				return this._cognitoIdentityProviders;
			}
			set
			{
				this._cognitoIdentityProviders = value;
			}
		}

		// Token: 0x06000286 RID: 646 RVA: 0x000047EF File Offset: 0x000029EF
		internal bool IsSetCognitoIdentityProviders()
		{
			return this._cognitoIdentityProviders != null && this._cognitoIdentityProviders.Count > 0;
		}

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000287 RID: 647 RVA: 0x00004809 File Offset: 0x00002A09
		// (set) Token: 0x06000288 RID: 648 RVA: 0x00004811 File Offset: 0x00002A11
		public string DeveloperProviderName
		{
			get
			{
				return this._developerProviderName;
			}
			set
			{
				this._developerProviderName = value;
			}
		}

		// Token: 0x06000289 RID: 649 RVA: 0x0000481A File Offset: 0x00002A1A
		internal bool IsSetDeveloperProviderName()
		{
			return this._developerProviderName != null;
		}

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x0600028A RID: 650 RVA: 0x00004825 File Offset: 0x00002A25
		// (set) Token: 0x0600028B RID: 651 RVA: 0x0000482D File Offset: 0x00002A2D
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x0600028C RID: 652 RVA: 0x00004836 File Offset: 0x00002A36
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600028D RID: 653 RVA: 0x00004841 File Offset: 0x00002A41
		// (set) Token: 0x0600028E RID: 654 RVA: 0x00004849 File Offset: 0x00002A49
		public string IdentityPoolName
		{
			get
			{
				return this._identityPoolName;
			}
			set
			{
				this._identityPoolName = value;
			}
		}

		// Token: 0x0600028F RID: 655 RVA: 0x00004852 File Offset: 0x00002A52
		internal bool IsSetIdentityPoolName()
		{
			return this._identityPoolName != null;
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x06000290 RID: 656 RVA: 0x0000485D File Offset: 0x00002A5D
		// (set) Token: 0x06000291 RID: 657 RVA: 0x00004865 File Offset: 0x00002A65
		public List<string> OpenIdConnectProviderARNs
		{
			get
			{
				return this._openIdConnectProviderARNs;
			}
			set
			{
				this._openIdConnectProviderARNs = value;
			}
		}

		// Token: 0x06000292 RID: 658 RVA: 0x0000486E File Offset: 0x00002A6E
		internal bool IsSetOpenIdConnectProviderARNs()
		{
			return this._openIdConnectProviderARNs != null && this._openIdConnectProviderARNs.Count > 0;
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x06000293 RID: 659 RVA: 0x00004888 File Offset: 0x00002A88
		// (set) Token: 0x06000294 RID: 660 RVA: 0x00004890 File Offset: 0x00002A90
		public List<string> SamlProviderARNs
		{
			get
			{
				return this._samlProviderARNs;
			}
			set
			{
				this._samlProviderARNs = value;
			}
		}

		// Token: 0x06000295 RID: 661 RVA: 0x00004899 File Offset: 0x00002A99
		internal bool IsSetSamlProviderARNs()
		{
			return this._samlProviderARNs != null && this._samlProviderARNs.Count > 0;
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x06000296 RID: 662 RVA: 0x000048B3 File Offset: 0x00002AB3
		// (set) Token: 0x06000297 RID: 663 RVA: 0x000048BB File Offset: 0x00002ABB
		public Dictionary<string, string> SupportedLoginProviders
		{
			get
			{
				return this._supportedLoginProviders;
			}
			set
			{
				this._supportedLoginProviders = value;
			}
		}

		// Token: 0x06000298 RID: 664 RVA: 0x000048C4 File Offset: 0x00002AC4
		internal bool IsSetSupportedLoginProviders()
		{
			return this._supportedLoginProviders != null && this._supportedLoginProviders.Count > 0;
		}

		// Token: 0x04000098 RID: 152
		private bool? _allowUnauthenticatedIdentities;

		// Token: 0x04000099 RID: 153
		private List<CognitoIdentityProviderInfo> _cognitoIdentityProviders = new List<CognitoIdentityProviderInfo>();

		// Token: 0x0400009A RID: 154
		private string _developerProviderName;

		// Token: 0x0400009B RID: 155
		private string _identityPoolId;

		// Token: 0x0400009C RID: 156
		private string _identityPoolName;

		// Token: 0x0400009D RID: 157
		private List<string> _openIdConnectProviderARNs = new List<string>();

		// Token: 0x0400009E RID: 158
		private List<string> _samlProviderARNs = new List<string>();

		// Token: 0x0400009F RID: 159
		private Dictionary<string, string> _supportedLoginProviders = new Dictionary<string, string>();
	}
}
