﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000025 RID: 37
	public class GetOpenIdTokenForDeveloperIdentityResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000179 RID: 377 RVA: 0x00003EA2 File Offset: 0x000020A2
		// (set) Token: 0x0600017A RID: 378 RVA: 0x00003EAA File Offset: 0x000020AA
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x0600017B RID: 379 RVA: 0x00003EB3 File Offset: 0x000020B3
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600017C RID: 380 RVA: 0x00003EBE File Offset: 0x000020BE
		// (set) Token: 0x0600017D RID: 381 RVA: 0x00003EC6 File Offset: 0x000020C6
		public string Token
		{
			get
			{
				return this._token;
			}
			set
			{
				this._token = value;
			}
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00003ECF File Offset: 0x000020CF
		internal bool IsSetToken()
		{
			return this._token != null;
		}

		// Token: 0x04000058 RID: 88
		private string _identityId;

		// Token: 0x04000059 RID: 89
		private string _token;
	}
}
