﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000015 RID: 21
	public class DeleteIdentitiesResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060000FC RID: 252 RVA: 0x00003970 File Offset: 0x00001B70
		// (set) Token: 0x060000FD RID: 253 RVA: 0x00003978 File Offset: 0x00001B78
		public List<UnprocessedIdentityId> UnprocessedIdentityIds
		{
			get
			{
				return this._unprocessedIdentityIds;
			}
			set
			{
				this._unprocessedIdentityIds = value;
			}
		}

		// Token: 0x060000FE RID: 254 RVA: 0x00003981 File Offset: 0x00001B81
		internal bool IsSetUnprocessedIdentityIds()
		{
			return this._unprocessedIdentityIds != null && this._unprocessedIdentityIds.Count > 0;
		}

		// Token: 0x04000037 RID: 55
		private List<UnprocessedIdentityId> _unprocessedIdentityIds = new List<UnprocessedIdentityId>();
	}
}
