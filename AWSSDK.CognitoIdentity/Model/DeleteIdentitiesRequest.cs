﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000014 RID: 20
	public class DeleteIdentitiesRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x060000F8 RID: 248 RVA: 0x00003932 File Offset: 0x00001B32
		// (set) Token: 0x060000F9 RID: 249 RVA: 0x0000393A File Offset: 0x00001B3A
		public List<string> IdentityIdsToDelete
		{
			get
			{
				return this._identityIdsToDelete;
			}
			set
			{
				this._identityIdsToDelete = value;
			}
		}

		// Token: 0x060000FA RID: 250 RVA: 0x00003943 File Offset: 0x00001B43
		internal bool IsSetIdentityIdsToDelete()
		{
			return this._identityIdsToDelete != null && this._identityIdsToDelete.Count > 0;
		}

		// Token: 0x04000036 RID: 54
		private List<string> _identityIdsToDelete = new List<string>();
	}
}
