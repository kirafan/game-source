﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200002C RID: 44
	[Serializable]
	public class InvalidParameterException : AmazonCognitoIdentityException
	{
		// Token: 0x060001AE RID: 430 RVA: 0x00003650 File Offset: 0x00001850
		public InvalidParameterException(string message) : base(message)
		{
		}

		// Token: 0x060001AF RID: 431 RVA: 0x00003659 File Offset: 0x00001859
		public InvalidParameterException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x00003663 File Offset: 0x00001863
		public InvalidParameterException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x060001B1 RID: 433 RVA: 0x0000366C File Offset: 0x0000186C
		public InvalidParameterException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x0000367D File Offset: 0x0000187D
		public InvalidParameterException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x0000368C File Offset: 0x0000188C
		protected InvalidParameterException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
