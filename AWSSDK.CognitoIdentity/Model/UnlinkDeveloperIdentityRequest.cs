﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200003F RID: 63
	public class UnlinkDeveloperIdentityRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000248 RID: 584 RVA: 0x0000451F File Offset: 0x0000271F
		// (set) Token: 0x06000249 RID: 585 RVA: 0x00004527 File Offset: 0x00002727
		public string DeveloperProviderName
		{
			get
			{
				return this._developerProviderName;
			}
			set
			{
				this._developerProviderName = value;
			}
		}

		// Token: 0x0600024A RID: 586 RVA: 0x00004530 File Offset: 0x00002730
		internal bool IsSetDeveloperProviderName()
		{
			return this._developerProviderName != null;
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x0600024B RID: 587 RVA: 0x0000453B File Offset: 0x0000273B
		// (set) Token: 0x0600024C RID: 588 RVA: 0x00004543 File Offset: 0x00002743
		public string DeveloperUserIdentifier
		{
			get
			{
				return this._developerUserIdentifier;
			}
			set
			{
				this._developerUserIdentifier = value;
			}
		}

		// Token: 0x0600024D RID: 589 RVA: 0x0000454C File Offset: 0x0000274C
		internal bool IsSetDeveloperUserIdentifier()
		{
			return this._developerUserIdentifier != null;
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x0600024E RID: 590 RVA: 0x00004557 File Offset: 0x00002757
		// (set) Token: 0x0600024F RID: 591 RVA: 0x0000455F File Offset: 0x0000275F
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x06000250 RID: 592 RVA: 0x00004568 File Offset: 0x00002768
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000251 RID: 593 RVA: 0x00004573 File Offset: 0x00002773
		// (set) Token: 0x06000252 RID: 594 RVA: 0x0000457B File Offset: 0x0000277B
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000253 RID: 595 RVA: 0x00004584 File Offset: 0x00002784
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x04000087 RID: 135
		private string _developerProviderName;

		// Token: 0x04000088 RID: 136
		private string _developerUserIdentifier;

		// Token: 0x04000089 RID: 137
		private string _identityId;

		// Token: 0x0400008A RID: 138
		private string _identityPoolId;
	}
}
