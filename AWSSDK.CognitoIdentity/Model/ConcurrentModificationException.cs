﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000011 RID: 17
	[Serializable]
	public class ConcurrentModificationException : AmazonCognitoIdentityException
	{
		// Token: 0x060000C3 RID: 195 RVA: 0x00003650 File Offset: 0x00001850
		public ConcurrentModificationException(string message) : base(message)
		{
		}

		// Token: 0x060000C4 RID: 196 RVA: 0x00003659 File Offset: 0x00001859
		public ConcurrentModificationException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060000C5 RID: 197 RVA: 0x00003663 File Offset: 0x00001863
		public ConcurrentModificationException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x060000C6 RID: 198 RVA: 0x0000366C File Offset: 0x0000186C
		public ConcurrentModificationException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060000C7 RID: 199 RVA: 0x0000367D File Offset: 0x0000187D
		public ConcurrentModificationException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060000C8 RID: 200 RVA: 0x0000368C File Offset: 0x0000188C
		protected ConcurrentModificationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
