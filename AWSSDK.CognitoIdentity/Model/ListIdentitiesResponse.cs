﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200002F RID: 47
	public class ListIdentitiesResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000058 RID: 88
		// (get) Token: 0x060001C7 RID: 455 RVA: 0x000040D6 File Offset: 0x000022D6
		// (set) Token: 0x060001C8 RID: 456 RVA: 0x000040DE File Offset: 0x000022DE
		public List<IdentityDescription> Identities
		{
			get
			{
				return this._identities;
			}
			set
			{
				this._identities = value;
			}
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x000040E7 File Offset: 0x000022E7
		internal bool IsSetIdentities()
		{
			return this._identities != null && this._identities.Count > 0;
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x060001CA RID: 458 RVA: 0x00004101 File Offset: 0x00002301
		// (set) Token: 0x060001CB RID: 459 RVA: 0x00004109 File Offset: 0x00002309
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x060001CC RID: 460 RVA: 0x00004112 File Offset: 0x00002312
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x1700005A RID: 90
		// (get) Token: 0x060001CD RID: 461 RVA: 0x0000411D File Offset: 0x0000231D
		// (set) Token: 0x060001CE RID: 462 RVA: 0x00004125 File Offset: 0x00002325
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x060001CF RID: 463 RVA: 0x0000412E File Offset: 0x0000232E
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x04000068 RID: 104
		private List<IdentityDescription> _identities = new List<IdentityDescription>();

		// Token: 0x04000069 RID: 105
		private string _identityPoolId;

		// Token: 0x0400006A RID: 106
		private string _nextToken;
	}
}
