﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000027 RID: 39
	public class GetOpenIdTokenResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000187 RID: 391 RVA: 0x00003F34 File Offset: 0x00002134
		// (set) Token: 0x06000188 RID: 392 RVA: 0x00003F3C File Offset: 0x0000213C
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x06000189 RID: 393 RVA: 0x00003F45 File Offset: 0x00002145
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x0600018A RID: 394 RVA: 0x00003F50 File Offset: 0x00002150
		// (set) Token: 0x0600018B RID: 395 RVA: 0x00003F58 File Offset: 0x00002158
		public string Token
		{
			get
			{
				return this._token;
			}
			set
			{
				this._token = value;
			}
		}

		// Token: 0x0600018C RID: 396 RVA: 0x00003F61 File Offset: 0x00002161
		internal bool IsSetToken()
		{
			return this._token != null;
		}

		// Token: 0x0400005C RID: 92
		private string _identityId;

		// Token: 0x0400005D RID: 93
		private string _token;
	}
}
