﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000024 RID: 36
	public class GetOpenIdTokenForDeveloperIdentityRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600016C RID: 364 RVA: 0x00003E04 File Offset: 0x00002004
		// (set) Token: 0x0600016D RID: 365 RVA: 0x00003E0C File Offset: 0x0000200C
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x0600016E RID: 366 RVA: 0x00003E15 File Offset: 0x00002015
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x0600016F RID: 367 RVA: 0x00003E20 File Offset: 0x00002020
		// (set) Token: 0x06000170 RID: 368 RVA: 0x00003E28 File Offset: 0x00002028
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000171 RID: 369 RVA: 0x00003E31 File Offset: 0x00002031
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000172 RID: 370 RVA: 0x00003E3C File Offset: 0x0000203C
		// (set) Token: 0x06000173 RID: 371 RVA: 0x00003E44 File Offset: 0x00002044
		public Dictionary<string, string> Logins
		{
			get
			{
				return this._logins;
			}
			set
			{
				this._logins = value;
			}
		}

		// Token: 0x06000174 RID: 372 RVA: 0x00003E4D File Offset: 0x0000204D
		internal bool IsSetLogins()
		{
			return this._logins != null && this._logins.Count > 0;
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000175 RID: 373 RVA: 0x00003E67 File Offset: 0x00002067
		// (set) Token: 0x06000176 RID: 374 RVA: 0x00003E74 File Offset: 0x00002074
		public long TokenDuration
		{
			get
			{
				return this._tokenDuration.GetValueOrDefault();
			}
			set
			{
				this._tokenDuration = new long?(value);
			}
		}

		// Token: 0x06000177 RID: 375 RVA: 0x00003E82 File Offset: 0x00002082
		internal bool IsSetTokenDuration()
		{
			return this._tokenDuration != null;
		}

		// Token: 0x04000054 RID: 84
		private string _identityId;

		// Token: 0x04000055 RID: 85
		private string _identityPoolId;

		// Token: 0x04000056 RID: 86
		private Dictionary<string, string> _logins = new Dictionary<string, string>();

		// Token: 0x04000057 RID: 87
		private long? _tokenDuration;
	}
}
