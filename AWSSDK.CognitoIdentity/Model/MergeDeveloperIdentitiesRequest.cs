﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000035 RID: 53
	public class MergeDeveloperIdentitiesRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x1700006B RID: 107
		// (get) Token: 0x06000206 RID: 518 RVA: 0x0000436B File Offset: 0x0000256B
		// (set) Token: 0x06000207 RID: 519 RVA: 0x00004373 File Offset: 0x00002573
		public string DestinationUserIdentifier
		{
			get
			{
				return this._destinationUserIdentifier;
			}
			set
			{
				this._destinationUserIdentifier = value;
			}
		}

		// Token: 0x06000208 RID: 520 RVA: 0x0000437C File Offset: 0x0000257C
		internal bool IsSetDestinationUserIdentifier()
		{
			return this._destinationUserIdentifier != null;
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x06000209 RID: 521 RVA: 0x00004387 File Offset: 0x00002587
		// (set) Token: 0x0600020A RID: 522 RVA: 0x0000438F File Offset: 0x0000258F
		public string DeveloperProviderName
		{
			get
			{
				return this._developerProviderName;
			}
			set
			{
				this._developerProviderName = value;
			}
		}

		// Token: 0x0600020B RID: 523 RVA: 0x00004398 File Offset: 0x00002598
		internal bool IsSetDeveloperProviderName()
		{
			return this._developerProviderName != null;
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x0600020C RID: 524 RVA: 0x000043A3 File Offset: 0x000025A3
		// (set) Token: 0x0600020D RID: 525 RVA: 0x000043AB File Offset: 0x000025AB
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x0600020E RID: 526 RVA: 0x000043B4 File Offset: 0x000025B4
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x0600020F RID: 527 RVA: 0x000043BF File Offset: 0x000025BF
		// (set) Token: 0x06000210 RID: 528 RVA: 0x000043C7 File Offset: 0x000025C7
		public string SourceUserIdentifier
		{
			get
			{
				return this._sourceUserIdentifier;
			}
			set
			{
				this._sourceUserIdentifier = value;
			}
		}

		// Token: 0x06000211 RID: 529 RVA: 0x000043D0 File Offset: 0x000025D0
		internal bool IsSetSourceUserIdentifier()
		{
			return this._sourceUserIdentifier != null;
		}

		// Token: 0x0400007B RID: 123
		private string _destinationUserIdentifier;

		// Token: 0x0400007C RID: 124
		private string _developerProviderName;

		// Token: 0x0400007D RID: 125
		private string _identityPoolId;

		// Token: 0x0400007E RID: 126
		private string _sourceUserIdentifier;
	}
}
