﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000039 RID: 57
	[Serializable]
	public class ResourceNotFoundException : AmazonCognitoIdentityException
	{
		// Token: 0x06000223 RID: 547 RVA: 0x00003650 File Offset: 0x00001850
		public ResourceNotFoundException(string message) : base(message)
		{
		}

		// Token: 0x06000224 RID: 548 RVA: 0x00003659 File Offset: 0x00001859
		public ResourceNotFoundException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000225 RID: 549 RVA: 0x00003663 File Offset: 0x00001863
		public ResourceNotFoundException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000226 RID: 550 RVA: 0x0000366C File Offset: 0x0000186C
		public ResourceNotFoundException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000227 RID: 551 RVA: 0x0000367D File Offset: 0x0000187D
		public ResourceNotFoundException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000228 RID: 552 RVA: 0x0000368C File Offset: 0x0000188C
		protected ResourceNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
