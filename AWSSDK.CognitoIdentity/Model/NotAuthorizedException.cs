﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000037 RID: 55
	[Serializable]
	public class NotAuthorizedException : AmazonCognitoIdentityException
	{
		// Token: 0x06000217 RID: 535 RVA: 0x00003650 File Offset: 0x00001850
		public NotAuthorizedException(string message) : base(message)
		{
		}

		// Token: 0x06000218 RID: 536 RVA: 0x00003659 File Offset: 0x00001859
		public NotAuthorizedException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000219 RID: 537 RVA: 0x00003663 File Offset: 0x00001863
		public NotAuthorizedException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x0600021A RID: 538 RVA: 0x0000366C File Offset: 0x0000186C
		public NotAuthorizedException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600021B RID: 539 RVA: 0x0000367D File Offset: 0x0000187D
		public NotAuthorizedException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x0600021C RID: 540 RVA: 0x0000368C File Offset: 0x0000188C
		protected NotAuthorizedException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
