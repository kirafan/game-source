﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000020 RID: 32
	public class GetIdentityPoolRolesRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000150 RID: 336 RVA: 0x00003CC6 File Offset: 0x00001EC6
		// (set) Token: 0x06000151 RID: 337 RVA: 0x00003CCE File Offset: 0x00001ECE
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000152 RID: 338 RVA: 0x00003CD7 File Offset: 0x00001ED7
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x0400004C RID: 76
		private string _identityPoolId;
	}
}
