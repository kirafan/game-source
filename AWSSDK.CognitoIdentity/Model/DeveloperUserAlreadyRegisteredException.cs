﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200001C RID: 28
	[Serializable]
	public class DeveloperUserAlreadyRegisteredException : AmazonCognitoIdentityException
	{
		// Token: 0x06000133 RID: 307 RVA: 0x00003650 File Offset: 0x00001850
		public DeveloperUserAlreadyRegisteredException(string message) : base(message)
		{
		}

		// Token: 0x06000134 RID: 308 RVA: 0x00003659 File Offset: 0x00001859
		public DeveloperUserAlreadyRegisteredException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000135 RID: 309 RVA: 0x00003663 File Offset: 0x00001863
		public DeveloperUserAlreadyRegisteredException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000136 RID: 310 RVA: 0x0000366C File Offset: 0x0000186C
		public DeveloperUserAlreadyRegisteredException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000137 RID: 311 RVA: 0x0000367D File Offset: 0x0000187D
		public DeveloperUserAlreadyRegisteredException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000138 RID: 312 RVA: 0x0000368C File Offset: 0x0000188C
		protected DeveloperUserAlreadyRegisteredException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
