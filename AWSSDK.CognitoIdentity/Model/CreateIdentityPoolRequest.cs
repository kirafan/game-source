﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000012 RID: 18
	public class CreateIdentityPoolRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000017 RID: 23
		// (get) Token: 0x060000C9 RID: 201 RVA: 0x00003696 File Offset: 0x00001896
		// (set) Token: 0x060000CA RID: 202 RVA: 0x000036A3 File Offset: 0x000018A3
		public bool AllowUnauthenticatedIdentities
		{
			get
			{
				return this._allowUnauthenticatedIdentities.GetValueOrDefault();
			}
			set
			{
				this._allowUnauthenticatedIdentities = new bool?(value);
			}
		}

		// Token: 0x060000CB RID: 203 RVA: 0x000036B1 File Offset: 0x000018B1
		internal bool IsSetAllowUnauthenticatedIdentities()
		{
			return this._allowUnauthenticatedIdentities != null;
		}

		// Token: 0x17000018 RID: 24
		// (get) Token: 0x060000CC RID: 204 RVA: 0x000036BE File Offset: 0x000018BE
		// (set) Token: 0x060000CD RID: 205 RVA: 0x000036C6 File Offset: 0x000018C6
		public List<CognitoIdentityProviderInfo> CognitoIdentityProviders
		{
			get
			{
				return this._cognitoIdentityProviders;
			}
			set
			{
				this._cognitoIdentityProviders = value;
			}
		}

		// Token: 0x060000CE RID: 206 RVA: 0x000036CF File Offset: 0x000018CF
		internal bool IsSetCognitoIdentityProviders()
		{
			return this._cognitoIdentityProviders != null && this._cognitoIdentityProviders.Count > 0;
		}

		// Token: 0x17000019 RID: 25
		// (get) Token: 0x060000CF RID: 207 RVA: 0x000036E9 File Offset: 0x000018E9
		// (set) Token: 0x060000D0 RID: 208 RVA: 0x000036F1 File Offset: 0x000018F1
		public string DeveloperProviderName
		{
			get
			{
				return this._developerProviderName;
			}
			set
			{
				this._developerProviderName = value;
			}
		}

		// Token: 0x060000D1 RID: 209 RVA: 0x000036FA File Offset: 0x000018FA
		internal bool IsSetDeveloperProviderName()
		{
			return this._developerProviderName != null;
		}

		// Token: 0x1700001A RID: 26
		// (get) Token: 0x060000D2 RID: 210 RVA: 0x00003705 File Offset: 0x00001905
		// (set) Token: 0x060000D3 RID: 211 RVA: 0x0000370D File Offset: 0x0000190D
		public string IdentityPoolName
		{
			get
			{
				return this._identityPoolName;
			}
			set
			{
				this._identityPoolName = value;
			}
		}

		// Token: 0x060000D4 RID: 212 RVA: 0x00003716 File Offset: 0x00001916
		internal bool IsSetIdentityPoolName()
		{
			return this._identityPoolName != null;
		}

		// Token: 0x1700001B RID: 27
		// (get) Token: 0x060000D5 RID: 213 RVA: 0x00003721 File Offset: 0x00001921
		// (set) Token: 0x060000D6 RID: 214 RVA: 0x00003729 File Offset: 0x00001929
		public List<string> OpenIdConnectProviderARNs
		{
			get
			{
				return this._openIdConnectProviderARNs;
			}
			set
			{
				this._openIdConnectProviderARNs = value;
			}
		}

		// Token: 0x060000D7 RID: 215 RVA: 0x00003732 File Offset: 0x00001932
		internal bool IsSetOpenIdConnectProviderARNs()
		{
			return this._openIdConnectProviderARNs != null && this._openIdConnectProviderARNs.Count > 0;
		}

		// Token: 0x1700001C RID: 28
		// (get) Token: 0x060000D8 RID: 216 RVA: 0x0000374C File Offset: 0x0000194C
		// (set) Token: 0x060000D9 RID: 217 RVA: 0x00003754 File Offset: 0x00001954
		public List<string> SamlProviderARNs
		{
			get
			{
				return this._samlProviderARNs;
			}
			set
			{
				this._samlProviderARNs = value;
			}
		}

		// Token: 0x060000DA RID: 218 RVA: 0x0000375D File Offset: 0x0000195D
		internal bool IsSetSamlProviderARNs()
		{
			return this._samlProviderARNs != null && this._samlProviderARNs.Count > 0;
		}

		// Token: 0x1700001D RID: 29
		// (get) Token: 0x060000DB RID: 219 RVA: 0x00003777 File Offset: 0x00001977
		// (set) Token: 0x060000DC RID: 220 RVA: 0x0000377F File Offset: 0x0000197F
		public Dictionary<string, string> SupportedLoginProviders
		{
			get
			{
				return this._supportedLoginProviders;
			}
			set
			{
				this._supportedLoginProviders = value;
			}
		}

		// Token: 0x060000DD RID: 221 RVA: 0x00003788 File Offset: 0x00001988
		internal bool IsSetSupportedLoginProviders()
		{
			return this._supportedLoginProviders != null && this._supportedLoginProviders.Count > 0;
		}

		// Token: 0x04000027 RID: 39
		private bool? _allowUnauthenticatedIdentities;

		// Token: 0x04000028 RID: 40
		private List<CognitoIdentityProviderInfo> _cognitoIdentityProviders = new List<CognitoIdentityProviderInfo>();

		// Token: 0x04000029 RID: 41
		private string _developerProviderName;

		// Token: 0x0400002A RID: 42
		private string _identityPoolName;

		// Token: 0x0400002B RID: 43
		private List<string> _openIdConnectProviderARNs = new List<string>();

		// Token: 0x0400002C RID: 44
		private List<string> _samlProviderARNs = new List<string>();

		// Token: 0x0400002D RID: 45
		private Dictionary<string, string> _supportedLoginProviders = new Dictionary<string, string>();
	}
}
