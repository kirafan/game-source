﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000028 RID: 40
	public class IdentityDescription
	{
		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600018E RID: 398 RVA: 0x00003F6C File Offset: 0x0000216C
		// (set) Token: 0x0600018F RID: 399 RVA: 0x00003F79 File Offset: 0x00002179
		public DateTime CreationDate
		{
			get
			{
				return this._creationDate.GetValueOrDefault();
			}
			set
			{
				this._creationDate = new DateTime?(value);
			}
		}

		// Token: 0x06000190 RID: 400 RVA: 0x00003F87 File Offset: 0x00002187
		internal bool IsSetCreationDate()
		{
			return this._creationDate != null;
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x06000191 RID: 401 RVA: 0x00003F94 File Offset: 0x00002194
		// (set) Token: 0x06000192 RID: 402 RVA: 0x00003F9C File Offset: 0x0000219C
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x06000193 RID: 403 RVA: 0x00003FA5 File Offset: 0x000021A5
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x06000194 RID: 404 RVA: 0x00003FB0 File Offset: 0x000021B0
		// (set) Token: 0x06000195 RID: 405 RVA: 0x00003FBD File Offset: 0x000021BD
		public DateTime LastModifiedDate
		{
			get
			{
				return this._lastModifiedDate.GetValueOrDefault();
			}
			set
			{
				this._lastModifiedDate = new DateTime?(value);
			}
		}

		// Token: 0x06000196 RID: 406 RVA: 0x00003FCB File Offset: 0x000021CB
		internal bool IsSetLastModifiedDate()
		{
			return this._lastModifiedDate != null;
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000197 RID: 407 RVA: 0x00003FD8 File Offset: 0x000021D8
		// (set) Token: 0x06000198 RID: 408 RVA: 0x00003FE0 File Offset: 0x000021E0
		public List<string> Logins
		{
			get
			{
				return this._logins;
			}
			set
			{
				this._logins = value;
			}
		}

		// Token: 0x06000199 RID: 409 RVA: 0x00003FE9 File Offset: 0x000021E9
		internal bool IsSetLogins()
		{
			return this._logins != null && this._logins.Count > 0;
		}

		// Token: 0x0400005E RID: 94
		private DateTime? _creationDate;

		// Token: 0x0400005F RID: 95
		private string _identityId;

		// Token: 0x04000060 RID: 96
		private DateTime? _lastModifiedDate;

		// Token: 0x04000061 RID: 97
		private List<string> _logins = new List<string>();
	}
}
