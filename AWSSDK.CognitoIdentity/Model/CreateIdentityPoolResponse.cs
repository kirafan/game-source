﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000013 RID: 19
	public class CreateIdentityPoolResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700001E RID: 30
		// (get) Token: 0x060000DF RID: 223 RVA: 0x000037D6 File Offset: 0x000019D6
		// (set) Token: 0x060000E0 RID: 224 RVA: 0x000037E3 File Offset: 0x000019E3
		public bool AllowUnauthenticatedIdentities
		{
			get
			{
				return this._allowUnauthenticatedIdentities.GetValueOrDefault();
			}
			set
			{
				this._allowUnauthenticatedIdentities = new bool?(value);
			}
		}

		// Token: 0x060000E1 RID: 225 RVA: 0x000037F1 File Offset: 0x000019F1
		internal bool IsSetAllowUnauthenticatedIdentities()
		{
			return this._allowUnauthenticatedIdentities != null;
		}

		// Token: 0x1700001F RID: 31
		// (get) Token: 0x060000E2 RID: 226 RVA: 0x000037FE File Offset: 0x000019FE
		// (set) Token: 0x060000E3 RID: 227 RVA: 0x00003806 File Offset: 0x00001A06
		public List<CognitoIdentityProviderInfo> CognitoIdentityProviders
		{
			get
			{
				return this._cognitoIdentityProviders;
			}
			set
			{
				this._cognitoIdentityProviders = value;
			}
		}

		// Token: 0x060000E4 RID: 228 RVA: 0x0000380F File Offset: 0x00001A0F
		internal bool IsSetCognitoIdentityProviders()
		{
			return this._cognitoIdentityProviders != null && this._cognitoIdentityProviders.Count > 0;
		}

		// Token: 0x17000020 RID: 32
		// (get) Token: 0x060000E5 RID: 229 RVA: 0x00003829 File Offset: 0x00001A29
		// (set) Token: 0x060000E6 RID: 230 RVA: 0x00003831 File Offset: 0x00001A31
		public string DeveloperProviderName
		{
			get
			{
				return this._developerProviderName;
			}
			set
			{
				this._developerProviderName = value;
			}
		}

		// Token: 0x060000E7 RID: 231 RVA: 0x0000383A File Offset: 0x00001A3A
		internal bool IsSetDeveloperProviderName()
		{
			return this._developerProviderName != null;
		}

		// Token: 0x17000021 RID: 33
		// (get) Token: 0x060000E8 RID: 232 RVA: 0x00003845 File Offset: 0x00001A45
		// (set) Token: 0x060000E9 RID: 233 RVA: 0x0000384D File Offset: 0x00001A4D
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x060000EA RID: 234 RVA: 0x00003856 File Offset: 0x00001A56
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x060000EB RID: 235 RVA: 0x00003861 File Offset: 0x00001A61
		// (set) Token: 0x060000EC RID: 236 RVA: 0x00003869 File Offset: 0x00001A69
		public string IdentityPoolName
		{
			get
			{
				return this._identityPoolName;
			}
			set
			{
				this._identityPoolName = value;
			}
		}

		// Token: 0x060000ED RID: 237 RVA: 0x00003872 File Offset: 0x00001A72
		internal bool IsSetIdentityPoolName()
		{
			return this._identityPoolName != null;
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x060000EE RID: 238 RVA: 0x0000387D File Offset: 0x00001A7D
		// (set) Token: 0x060000EF RID: 239 RVA: 0x00003885 File Offset: 0x00001A85
		public List<string> OpenIdConnectProviderARNs
		{
			get
			{
				return this._openIdConnectProviderARNs;
			}
			set
			{
				this._openIdConnectProviderARNs = value;
			}
		}

		// Token: 0x060000F0 RID: 240 RVA: 0x0000388E File Offset: 0x00001A8E
		internal bool IsSetOpenIdConnectProviderARNs()
		{
			return this._openIdConnectProviderARNs != null && this._openIdConnectProviderARNs.Count > 0;
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x060000F1 RID: 241 RVA: 0x000038A8 File Offset: 0x00001AA8
		// (set) Token: 0x060000F2 RID: 242 RVA: 0x000038B0 File Offset: 0x00001AB0
		public List<string> SamlProviderARNs
		{
			get
			{
				return this._samlProviderARNs;
			}
			set
			{
				this._samlProviderARNs = value;
			}
		}

		// Token: 0x060000F3 RID: 243 RVA: 0x000038B9 File Offset: 0x00001AB9
		internal bool IsSetSamlProviderARNs()
		{
			return this._samlProviderARNs != null && this._samlProviderARNs.Count > 0;
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x060000F4 RID: 244 RVA: 0x000038D3 File Offset: 0x00001AD3
		// (set) Token: 0x060000F5 RID: 245 RVA: 0x000038DB File Offset: 0x00001ADB
		public Dictionary<string, string> SupportedLoginProviders
		{
			get
			{
				return this._supportedLoginProviders;
			}
			set
			{
				this._supportedLoginProviders = value;
			}
		}

		// Token: 0x060000F6 RID: 246 RVA: 0x000038E4 File Offset: 0x00001AE4
		internal bool IsSetSupportedLoginProviders()
		{
			return this._supportedLoginProviders != null && this._supportedLoginProviders.Count > 0;
		}

		// Token: 0x0400002E RID: 46
		private bool? _allowUnauthenticatedIdentities;

		// Token: 0x0400002F RID: 47
		private List<CognitoIdentityProviderInfo> _cognitoIdentityProviders = new List<CognitoIdentityProviderInfo>();

		// Token: 0x04000030 RID: 48
		private string _developerProviderName;

		// Token: 0x04000031 RID: 49
		private string _identityPoolId;

		// Token: 0x04000032 RID: 50
		private string _identityPoolName;

		// Token: 0x04000033 RID: 51
		private List<string> _openIdConnectProviderARNs = new List<string>();

		// Token: 0x04000034 RID: 52
		private List<string> _samlProviderARNs = new List<string>();

		// Token: 0x04000035 RID: 53
		private Dictionary<string, string> _supportedLoginProviders = new Dictionary<string, string>();
	}
}
