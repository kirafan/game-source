﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000032 RID: 50
	public class LookupDeveloperIdentityRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060001DF RID: 479 RVA: 0x000041EA File Offset: 0x000023EA
		// (set) Token: 0x060001E0 RID: 480 RVA: 0x000041F2 File Offset: 0x000023F2
		public string DeveloperUserIdentifier
		{
			get
			{
				return this._developerUserIdentifier;
			}
			set
			{
				this._developerUserIdentifier = value;
			}
		}

		// Token: 0x060001E1 RID: 481 RVA: 0x000041FB File Offset: 0x000023FB
		internal bool IsSetDeveloperUserIdentifier()
		{
			return this._developerUserIdentifier != null;
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060001E2 RID: 482 RVA: 0x00004206 File Offset: 0x00002406
		// (set) Token: 0x060001E3 RID: 483 RVA: 0x0000420E File Offset: 0x0000240E
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x060001E4 RID: 484 RVA: 0x00004217 File Offset: 0x00002417
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060001E5 RID: 485 RVA: 0x00004222 File Offset: 0x00002422
		// (set) Token: 0x060001E6 RID: 486 RVA: 0x0000422A File Offset: 0x0000242A
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x00004233 File Offset: 0x00002433
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060001E8 RID: 488 RVA: 0x0000423E File Offset: 0x0000243E
		// (set) Token: 0x060001E9 RID: 489 RVA: 0x0000424B File Offset: 0x0000244B
		public int MaxResults
		{
			get
			{
				return this._maxResults.GetValueOrDefault();
			}
			set
			{
				this._maxResults = new int?(value);
			}
		}

		// Token: 0x060001EA RID: 490 RVA: 0x00004259 File Offset: 0x00002459
		internal bool IsSetMaxResults()
		{
			return this._maxResults != null;
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060001EB RID: 491 RVA: 0x00004266 File Offset: 0x00002466
		// (set) Token: 0x060001EC RID: 492 RVA: 0x0000426E File Offset: 0x0000246E
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x060001ED RID: 493 RVA: 0x00004277 File Offset: 0x00002477
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x0400006F RID: 111
		private string _developerUserIdentifier;

		// Token: 0x04000070 RID: 112
		private string _identityId;

		// Token: 0x04000071 RID: 113
		private string _identityPoolId;

		// Token: 0x04000072 RID: 114
		private int? _maxResults;

		// Token: 0x04000073 RID: 115
		private string _nextToken;
	}
}
