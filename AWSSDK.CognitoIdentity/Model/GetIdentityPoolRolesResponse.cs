﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000021 RID: 33
	public class GetIdentityPoolRolesResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000154 RID: 340 RVA: 0x00003CE2 File Offset: 0x00001EE2
		// (set) Token: 0x06000155 RID: 341 RVA: 0x00003CEA File Offset: 0x00001EEA
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000156 RID: 342 RVA: 0x00003CF3 File Offset: 0x00001EF3
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000157 RID: 343 RVA: 0x00003CFE File Offset: 0x00001EFE
		// (set) Token: 0x06000158 RID: 344 RVA: 0x00003D06 File Offset: 0x00001F06
		public Dictionary<string, RoleMapping> RoleMappings
		{
			get
			{
				return this._roleMappings;
			}
			set
			{
				this._roleMappings = value;
			}
		}

		// Token: 0x06000159 RID: 345 RVA: 0x00003D0F File Offset: 0x00001F0F
		internal bool IsSetRoleMappings()
		{
			return this._roleMappings != null && this._roleMappings.Count > 0;
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x0600015A RID: 346 RVA: 0x00003D29 File Offset: 0x00001F29
		// (set) Token: 0x0600015B RID: 347 RVA: 0x00003D31 File Offset: 0x00001F31
		public Dictionary<string, string> Roles
		{
			get
			{
				return this._roles;
			}
			set
			{
				this._roles = value;
			}
		}

		// Token: 0x0600015C RID: 348 RVA: 0x00003D3A File Offset: 0x00001F3A
		internal bool IsSetRoles()
		{
			return this._roles != null && this._roles.Count > 0;
		}

		// Token: 0x0400004D RID: 77
		private string _identityPoolId;

		// Token: 0x0400004E RID: 78
		private Dictionary<string, RoleMapping> _roleMappings = new Dictionary<string, RoleMapping>();

		// Token: 0x0400004F RID: 79
		private Dictionary<string, string> _roles = new Dictionary<string, string>();
	}
}
