﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000041 RID: 65
	public class UnlinkIdentityRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x1700007B RID: 123
		// (get) Token: 0x06000256 RID: 598 RVA: 0x0000458F File Offset: 0x0000278F
		// (set) Token: 0x06000257 RID: 599 RVA: 0x00004597 File Offset: 0x00002797
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x06000258 RID: 600 RVA: 0x000045A0 File Offset: 0x000027A0
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x06000259 RID: 601 RVA: 0x000045AB File Offset: 0x000027AB
		// (set) Token: 0x0600025A RID: 602 RVA: 0x000045B3 File Offset: 0x000027B3
		public Dictionary<string, string> Logins
		{
			get
			{
				return this._logins;
			}
			set
			{
				this._logins = value;
			}
		}

		// Token: 0x0600025B RID: 603 RVA: 0x000045BC File Offset: 0x000027BC
		internal bool IsSetLogins()
		{
			return this._logins != null && this._logins.Count > 0;
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600025C RID: 604 RVA: 0x000045D6 File Offset: 0x000027D6
		// (set) Token: 0x0600025D RID: 605 RVA: 0x000045DE File Offset: 0x000027DE
		public List<string> LoginsToRemove
		{
			get
			{
				return this._loginsToRemove;
			}
			set
			{
				this._loginsToRemove = value;
			}
		}

		// Token: 0x0600025E RID: 606 RVA: 0x000045E7 File Offset: 0x000027E7
		internal bool IsSetLoginsToRemove()
		{
			return this._loginsToRemove != null && this._loginsToRemove.Count > 0;
		}

		// Token: 0x0400008B RID: 139
		private string _identityId;

		// Token: 0x0400008C RID: 140
		private Dictionary<string, string> _logins = new Dictionary<string, string>();

		// Token: 0x0400008D RID: 141
		private List<string> _loginsToRemove = new List<string>();
	}
}
