﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000010 RID: 16
	public class CognitoIdentityProviderInfo
	{
		// Token: 0x17000014 RID: 20
		// (get) Token: 0x060000B9 RID: 185 RVA: 0x000035F0 File Offset: 0x000017F0
		// (set) Token: 0x060000BA RID: 186 RVA: 0x000035F8 File Offset: 0x000017F8
		public string ClientId
		{
			get
			{
				return this._clientId;
			}
			set
			{
				this._clientId = value;
			}
		}

		// Token: 0x060000BB RID: 187 RVA: 0x00003601 File Offset: 0x00001801
		internal bool IsSetClientId()
		{
			return this._clientId != null;
		}

		// Token: 0x17000015 RID: 21
		// (get) Token: 0x060000BC RID: 188 RVA: 0x0000360C File Offset: 0x0000180C
		// (set) Token: 0x060000BD RID: 189 RVA: 0x00003614 File Offset: 0x00001814
		public string ProviderName
		{
			get
			{
				return this._providerName;
			}
			set
			{
				this._providerName = value;
			}
		}

		// Token: 0x060000BE RID: 190 RVA: 0x0000361D File Offset: 0x0000181D
		internal bool IsSetProviderName()
		{
			return this._providerName != null;
		}

		// Token: 0x17000016 RID: 22
		// (get) Token: 0x060000BF RID: 191 RVA: 0x00003628 File Offset: 0x00001828
		// (set) Token: 0x060000C0 RID: 192 RVA: 0x00003635 File Offset: 0x00001835
		public bool ServerSideTokenCheck
		{
			get
			{
				return this._serverSideTokenCheck.GetValueOrDefault();
			}
			set
			{
				this._serverSideTokenCheck = new bool?(value);
			}
		}

		// Token: 0x060000C1 RID: 193 RVA: 0x00003643 File Offset: 0x00001843
		internal bool IsSetServerSideTokenCheck()
		{
			return this._serverSideTokenCheck != null;
		}

		// Token: 0x04000024 RID: 36
		private string _clientId;

		// Token: 0x04000025 RID: 37
		private string _providerName;

		// Token: 0x04000026 RID: 38
		private bool? _serverSideTokenCheck;
	}
}
