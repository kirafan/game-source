﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000038 RID: 56
	[Serializable]
	public class ResourceConflictException : AmazonCognitoIdentityException
	{
		// Token: 0x0600021D RID: 541 RVA: 0x00003650 File Offset: 0x00001850
		public ResourceConflictException(string message) : base(message)
		{
		}

		// Token: 0x0600021E RID: 542 RVA: 0x00003659 File Offset: 0x00001859
		public ResourceConflictException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600021F RID: 543 RVA: 0x00003663 File Offset: 0x00001863
		public ResourceConflictException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000220 RID: 544 RVA: 0x0000366C File Offset: 0x0000186C
		public ResourceConflictException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000221 RID: 545 RVA: 0x0000367D File Offset: 0x0000187D
		public ResourceConflictException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000222 RID: 546 RVA: 0x0000368C File Offset: 0x0000188C
		protected ResourceConflictException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
