﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200002E RID: 46
	public class ListIdentitiesRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000054 RID: 84
		// (get) Token: 0x060001BA RID: 442 RVA: 0x0000404E File Offset: 0x0000224E
		// (set) Token: 0x060001BB RID: 443 RVA: 0x0000405B File Offset: 0x0000225B
		public bool HideDisabled
		{
			get
			{
				return this._hideDisabled.GetValueOrDefault();
			}
			set
			{
				this._hideDisabled = new bool?(value);
			}
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00004069 File Offset: 0x00002269
		internal bool IsSetHideDisabled()
		{
			return this._hideDisabled != null;
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x060001BD RID: 445 RVA: 0x00004076 File Offset: 0x00002276
		// (set) Token: 0x060001BE RID: 446 RVA: 0x0000407E File Offset: 0x0000227E
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x060001BF RID: 447 RVA: 0x00004087 File Offset: 0x00002287
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x060001C0 RID: 448 RVA: 0x00004092 File Offset: 0x00002292
		// (set) Token: 0x060001C1 RID: 449 RVA: 0x0000409F File Offset: 0x0000229F
		public int MaxResults
		{
			get
			{
				return this._maxResults.GetValueOrDefault();
			}
			set
			{
				this._maxResults = new int?(value);
			}
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x000040AD File Offset: 0x000022AD
		internal bool IsSetMaxResults()
		{
			return this._maxResults != null;
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x060001C3 RID: 451 RVA: 0x000040BA File Offset: 0x000022BA
		// (set) Token: 0x060001C4 RID: 452 RVA: 0x000040C2 File Offset: 0x000022C2
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x000040CB File Offset: 0x000022CB
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x04000064 RID: 100
		private bool? _hideDisabled;

		// Token: 0x04000065 RID: 101
		private string _identityPoolId;

		// Token: 0x04000066 RID: 102
		private int? _maxResults;

		// Token: 0x04000067 RID: 103
		private string _nextToken;
	}
}
