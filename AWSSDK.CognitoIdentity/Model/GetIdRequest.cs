﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000022 RID: 34
	public class GetIdRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000040 RID: 64
		// (get) Token: 0x0600015E RID: 350 RVA: 0x00003D72 File Offset: 0x00001F72
		// (set) Token: 0x0600015F RID: 351 RVA: 0x00003D7A File Offset: 0x00001F7A
		public string AccountId
		{
			get
			{
				return this._accountId;
			}
			set
			{
				this._accountId = value;
			}
		}

		// Token: 0x06000160 RID: 352 RVA: 0x00003D83 File Offset: 0x00001F83
		internal bool IsSetAccountId()
		{
			return this._accountId != null;
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000161 RID: 353 RVA: 0x00003D8E File Offset: 0x00001F8E
		// (set) Token: 0x06000162 RID: 354 RVA: 0x00003D96 File Offset: 0x00001F96
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000163 RID: 355 RVA: 0x00003D9F File Offset: 0x00001F9F
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000164 RID: 356 RVA: 0x00003DAA File Offset: 0x00001FAA
		// (set) Token: 0x06000165 RID: 357 RVA: 0x00003DB2 File Offset: 0x00001FB2
		public Dictionary<string, string> Logins
		{
			get
			{
				return this._logins;
			}
			set
			{
				this._logins = value;
			}
		}

		// Token: 0x06000166 RID: 358 RVA: 0x00003DBB File Offset: 0x00001FBB
		internal bool IsSetLogins()
		{
			return this._logins != null && this._logins.Count > 0;
		}

		// Token: 0x04000050 RID: 80
		private string _accountId;

		// Token: 0x04000051 RID: 81
		private string _identityPoolId;

		// Token: 0x04000052 RID: 82
		private Dictionary<string, string> _logins = new Dictionary<string, string>();
	}
}
