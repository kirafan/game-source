﻿using System;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000065 RID: 101
	public class MappingRuleMarshaller : IRequestMarshaller<MappingRule, JsonMarshallerContext>
	{
		// Token: 0x06000326 RID: 806 RVA: 0x00007E5C File Offset: 0x0000605C
		public void Marshall(MappingRule requestObject, JsonMarshallerContext context)
		{
			if (requestObject.IsSetClaim())
			{
				context.Writer.WritePropertyName("Claim");
				context.Writer.Write(requestObject.Claim);
			}
			if (requestObject.IsSetMatchType())
			{
				context.Writer.WritePropertyName("MatchType");
				context.Writer.Write(requestObject.MatchType);
			}
			if (requestObject.IsSetRoleARN())
			{
				context.Writer.WritePropertyName("RoleARN");
				context.Writer.Write(requestObject.RoleARN);
			}
			if (requestObject.IsSetValue())
			{
				context.Writer.WritePropertyName("Value");
				context.Writer.Write(requestObject.Value);
			}
		}

		// Token: 0x040000B2 RID: 178
		public static readonly MappingRuleMarshaller Instance = new MappingRuleMarshaller();
	}
}
