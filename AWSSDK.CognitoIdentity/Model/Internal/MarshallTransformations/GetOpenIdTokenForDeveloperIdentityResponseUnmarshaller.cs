﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005A RID: 90
	public class GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x060002F2 RID: 754 RVA: 0x00006C50 File Offset: 0x00004E50
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			GetOpenIdTokenForDeveloperIdentityResponse getOpenIdTokenForDeveloperIdentityResponse = new GetOpenIdTokenForDeveloperIdentityResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("IdentityId", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					getOpenIdTokenForDeveloperIdentityResponse.IdentityId = instance.Unmarshall(context);
				}
				else if (context.TestExpression("Token", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					getOpenIdTokenForDeveloperIdentityResponse.Token = instance2.Unmarshall(context);
				}
			}
			return getOpenIdTokenForDeveloperIdentityResponse;
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x00006CC4 File Offset: 0x00004EC4
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("DeveloperUserAlreadyRegisteredException"))
			{
				return new DeveloperUserAlreadyRegisteredException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceConflictException"))
			{
				return new ResourceConflictException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x00006E92 File Offset: 0x00005092
		internal static GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller GetInstance()
		{
			return GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller._instance;
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x060002F5 RID: 757 RVA: 0x00006E92 File Offset: 0x00005092
		public static GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller Instance
		{
			get
			{
				return GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000AB RID: 171
		private static GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller _instance = new GetOpenIdTokenForDeveloperIdentityResponseUnmarshaller();
	}
}
