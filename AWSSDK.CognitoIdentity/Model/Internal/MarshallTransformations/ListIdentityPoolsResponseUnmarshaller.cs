﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000062 RID: 98
	public class ListIdentityPoolsResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x06000317 RID: 791 RVA: 0x000078B8 File Offset: 0x00005AB8
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			ListIdentityPoolsResponse listIdentityPoolsResponse = new ListIdentityPoolsResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("IdentityPools", currentDepth))
				{
					ListUnmarshaller<IdentityPoolShortDescription, IdentityPoolShortDescriptionUnmarshaller> listUnmarshaller = new ListUnmarshaller<IdentityPoolShortDescription, IdentityPoolShortDescriptionUnmarshaller>(IdentityPoolShortDescriptionUnmarshaller.Instance);
					listIdentityPoolsResponse.IdentityPools = listUnmarshaller.Unmarshall(context);
				}
				else if (context.TestExpression("NextToken", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					listIdentityPoolsResponse.NextToken = instance.Unmarshall(context);
				}
			}
			return listIdentityPoolsResponse;
		}

		// Token: 0x06000318 RID: 792 RVA: 0x00007930 File Offset: 0x00005B30
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000319 RID: 793 RVA: 0x00007A50 File Offset: 0x00005C50
		internal static ListIdentityPoolsResponseUnmarshaller GetInstance()
		{
			return ListIdentityPoolsResponseUnmarshaller._instance;
		}

		// Token: 0x1700009F RID: 159
		// (get) Token: 0x0600031A RID: 794 RVA: 0x00007A50 File Offset: 0x00005C50
		public static ListIdentityPoolsResponseUnmarshaller Instance
		{
			get
			{
				return ListIdentityPoolsResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000B0 RID: 176
		private static ListIdentityPoolsResponseUnmarshaller _instance = new ListIdentityPoolsResponseUnmarshaller();
	}
}
