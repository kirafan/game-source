﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000055 RID: 85
	public class GetIdentityPoolRolesRequestMarshaller : IMarshaller<IRequest, GetIdentityPoolRolesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002DD RID: 733 RVA: 0x000062B3 File Offset: 0x000044B3
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((GetIdentityPoolRolesRequest)input);
		}

		// Token: 0x060002DE RID: 734 RVA: 0x000062C4 File Offset: 0x000044C4
		public IRequest Marshall(GetIdentityPoolRolesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.CognitoIdentity");
			string value = "AWSCognitoIdentityService.GetIdentityPoolRoles";
			request.Headers["X-Amz-Target"] = value;
			request.Headers["Content-Type"] = "application/x-amz-json-1.1";
			request.HttpMethod = "POST";
			string resourcePath = "/";
			request.ResourcePath = resourcePath;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter);
				jsonWriter.WriteObjectStart();
				JsonMarshallerContext jsonMarshallerContext = new JsonMarshallerContext(request, jsonWriter);
				if (publicRequest.IsSetIdentityPoolId())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityPoolId");
					jsonMarshallerContext.Writer.Write(publicRequest.IdentityPoolId);
				}
				jsonWriter.WriteObjectEnd();
				string s = stringWriter.ToString();
				request.Content = Encoding.UTF8.GetBytes(s);
			}
			return request;
		}
	}
}
