﻿using System;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006C RID: 108
	public class RulesConfigurationTypeUnmarshaller : IUnmarshaller<RulesConfigurationType, XmlUnmarshallerContext>, IUnmarshaller<RulesConfigurationType, JsonUnmarshallerContext>
	{
		// Token: 0x06000342 RID: 834 RVA: 0x000049A8 File Offset: 0x00002BA8
		RulesConfigurationType IUnmarshaller<RulesConfigurationType, XmlUnmarshallerContext>.Unmarshall(XmlUnmarshallerContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000343 RID: 835 RVA: 0x00008594 File Offset: 0x00006794
		public RulesConfigurationType Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return null;
			}
			RulesConfigurationType rulesConfigurationType = new RulesConfigurationType();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("Rules", currentDepth))
				{
					ListUnmarshaller<MappingRule, MappingRuleUnmarshaller> listUnmarshaller = new ListUnmarshaller<MappingRule, MappingRuleUnmarshaller>(MappingRuleUnmarshaller.Instance);
					rulesConfigurationType.Rules = listUnmarshaller.Unmarshall(context);
				}
			}
			return rulesConfigurationType;
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x06000344 RID: 836 RVA: 0x000085F3 File Offset: 0x000067F3
		public static RulesConfigurationTypeUnmarshaller Instance
		{
			get
			{
				return RulesConfigurationTypeUnmarshaller._instance;
			}
		}

		// Token: 0x040000B8 RID: 184
		private static RulesConfigurationTypeUnmarshaller _instance = new RulesConfigurationTypeUnmarshaller();
	}
}
