﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000056 RID: 86
	public class GetIdentityPoolRolesResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x060002E0 RID: 736 RVA: 0x000063B0 File Offset: 0x000045B0
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			GetIdentityPoolRolesResponse getIdentityPoolRolesResponse = new GetIdentityPoolRolesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("IdentityPoolId", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					getIdentityPoolRolesResponse.IdentityPoolId = instance.Unmarshall(context);
				}
				else if (context.TestExpression("RoleMappings", currentDepth))
				{
					DictionaryUnmarshaller<string, RoleMapping, StringUnmarshaller, RoleMappingUnmarshaller> dictionaryUnmarshaller = new DictionaryUnmarshaller<string, RoleMapping, StringUnmarshaller, RoleMappingUnmarshaller>(StringUnmarshaller.Instance, RoleMappingUnmarshaller.Instance);
					getIdentityPoolRolesResponse.RoleMappings = dictionaryUnmarshaller.Unmarshall(context);
				}
				else if (context.TestExpression("Roles", currentDepth))
				{
					DictionaryUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller> dictionaryUnmarshaller2 = new DictionaryUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller>(StringUnmarshaller.Instance, StringUnmarshaller.Instance);
					getIdentityPoolRolesResponse.Roles = dictionaryUnmarshaller2.Unmarshall(context);
				}
			}
			return getIdentityPoolRolesResponse;
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x00006460 File Offset: 0x00004660
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceConflictException"))
			{
				return new ResourceConflictException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x000065F4 File Offset: 0x000047F4
		internal static GetIdentityPoolRolesResponseUnmarshaller GetInstance()
		{
			return GetIdentityPoolRolesResponseUnmarshaller._instance;
		}

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x060002E3 RID: 739 RVA: 0x000065F4 File Offset: 0x000047F4
		public static GetIdentityPoolRolesResponseUnmarshaller Instance
		{
			get
			{
				return GetIdentityPoolRolesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000A9 RID: 169
		private static GetIdentityPoolRolesResponseUnmarshaller _instance = new GetIdentityPoolRolesResponseUnmarshaller();
	}
}
