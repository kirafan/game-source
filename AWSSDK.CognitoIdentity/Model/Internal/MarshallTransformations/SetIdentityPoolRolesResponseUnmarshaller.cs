﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006E RID: 110
	public class SetIdentityPoolRolesResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x0600034A RID: 842 RVA: 0x00008870 File Offset: 0x00006A70
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			return new SetIdentityPoolRolesResponse();
		}

		// Token: 0x0600034B RID: 843 RVA: 0x00008878 File Offset: 0x00006A78
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("ConcurrentModificationException"))
			{
				return new ConcurrentModificationException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceConflictException"))
			{
				return new ResourceConflictException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600034C RID: 844 RVA: 0x00008A46 File Offset: 0x00006C46
		internal static SetIdentityPoolRolesResponseUnmarshaller GetInstance()
		{
			return SetIdentityPoolRolesResponseUnmarshaller._instance;
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x0600034D RID: 845 RVA: 0x00008A46 File Offset: 0x00006C46
		public static SetIdentityPoolRolesResponseUnmarshaller Instance
		{
			get
			{
				return SetIdentityPoolRolesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000B9 RID: 185
		private static SetIdentityPoolRolesResponseUnmarshaller _instance = new SetIdentityPoolRolesResponseUnmarshaller();
	}
}
