﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000057 RID: 87
	public class GetIdRequestMarshaller : IMarshaller<IRequest, GetIdRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002E6 RID: 742 RVA: 0x00006607 File Offset: 0x00004807
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((GetIdRequest)input);
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x00006618 File Offset: 0x00004818
		public IRequest Marshall(GetIdRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.CognitoIdentity");
			string value = "AWSCognitoIdentityService.GetId";
			request.Headers["X-Amz-Target"] = value;
			request.Headers["Content-Type"] = "application/x-amz-json-1.1";
			request.HttpMethod = "POST";
			string resourcePath = "/";
			request.ResourcePath = resourcePath;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter);
				jsonWriter.WriteObjectStart();
				JsonMarshallerContext jsonMarshallerContext = new JsonMarshallerContext(request, jsonWriter);
				if (publicRequest.IsSetAccountId())
				{
					jsonMarshallerContext.Writer.WritePropertyName("AccountId");
					jsonMarshallerContext.Writer.Write(publicRequest.AccountId);
				}
				if (publicRequest.IsSetIdentityPoolId())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityPoolId");
					jsonMarshallerContext.Writer.Write(publicRequest.IdentityPoolId);
				}
				if (publicRequest.IsSetLogins())
				{
					jsonMarshallerContext.Writer.WritePropertyName("Logins");
					jsonMarshallerContext.Writer.WriteObjectStart();
					foreach (KeyValuePair<string, string> keyValuePair in publicRequest.Logins)
					{
						jsonMarshallerContext.Writer.WritePropertyName(keyValuePair.Key);
						string value2 = keyValuePair.Value;
						jsonMarshallerContext.Writer.Write(value2);
					}
					jsonMarshallerContext.Writer.WriteObjectEnd();
				}
				jsonWriter.WriteObjectEnd();
				string s = stringWriter.ToString();
				request.Content = Encoding.UTF8.GetBytes(s);
			}
			return request;
		}
	}
}
