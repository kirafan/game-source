﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200004E RID: 78
	public class DeleteIdentityPoolResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x060002BC RID: 700 RVA: 0x000055C0 File Offset: 0x000037C0
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			return new DeleteIdentityPoolResponse();
		}

		// Token: 0x060002BD RID: 701 RVA: 0x000055C8 File Offset: 0x000037C8
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002BE RID: 702 RVA: 0x00005722 File Offset: 0x00003922
		internal static DeleteIdentityPoolResponseUnmarshaller GetInstance()
		{
			return DeleteIdentityPoolResponseUnmarshaller._instance;
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060002BF RID: 703 RVA: 0x00005722 File Offset: 0x00003922
		public static DeleteIdentityPoolResponseUnmarshaller Instance
		{
			get
			{
				return DeleteIdentityPoolResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000A5 RID: 165
		private static DeleteIdentityPoolResponseUnmarshaller _instance = new DeleteIdentityPoolResponseUnmarshaller();
	}
}
