﻿using System;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005E RID: 94
	public class IdentityPoolShortDescriptionUnmarshaller : IUnmarshaller<IdentityPoolShortDescription, XmlUnmarshallerContext>, IUnmarshaller<IdentityPoolShortDescription, JsonUnmarshallerContext>
	{
		// Token: 0x06000306 RID: 774 RVA: 0x000049A8 File Offset: 0x00002BA8
		IdentityPoolShortDescription IUnmarshaller<IdentityPoolShortDescription, XmlUnmarshallerContext>.Unmarshall(XmlUnmarshallerContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000307 RID: 775 RVA: 0x0000737C File Offset: 0x0000557C
		public IdentityPoolShortDescription Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return null;
			}
			IdentityPoolShortDescription identityPoolShortDescription = new IdentityPoolShortDescription();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("IdentityPoolId", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					identityPoolShortDescription.IdentityPoolId = instance.Unmarshall(context);
				}
				else if (context.TestExpression("IdentityPoolName", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					identityPoolShortDescription.IdentityPoolName = instance2.Unmarshall(context);
				}
			}
			return identityPoolShortDescription;
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x06000308 RID: 776 RVA: 0x000073F9 File Offset: 0x000055F9
		public static IdentityPoolShortDescriptionUnmarshaller Instance
		{
			get
			{
				return IdentityPoolShortDescriptionUnmarshaller._instance;
			}
		}

		// Token: 0x040000AE RID: 174
		private static IdentityPoolShortDescriptionUnmarshaller _instance = new IdentityPoolShortDescriptionUnmarshaller();
	}
}
