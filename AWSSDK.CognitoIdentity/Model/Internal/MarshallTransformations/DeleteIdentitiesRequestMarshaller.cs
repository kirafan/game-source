﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200004B RID: 75
	public class DeleteIdentitiesRequestMarshaller : IMarshaller<IRequest, DeleteIdentitiesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002B0 RID: 688 RVA: 0x0000522C File Offset: 0x0000342C
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((DeleteIdentitiesRequest)input);
		}

		// Token: 0x060002B1 RID: 689 RVA: 0x0000523C File Offset: 0x0000343C
		public IRequest Marshall(DeleteIdentitiesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.CognitoIdentity");
			string value = "AWSCognitoIdentityService.DeleteIdentities";
			request.Headers["X-Amz-Target"] = value;
			request.Headers["Content-Type"] = "application/x-amz-json-1.1";
			request.HttpMethod = "POST";
			string resourcePath = "/";
			request.ResourcePath = resourcePath;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter);
				jsonWriter.WriteObjectStart();
				JsonMarshallerContext jsonMarshallerContext = new JsonMarshallerContext(request, jsonWriter);
				if (publicRequest.IsSetIdentityIdsToDelete())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityIdsToDelete");
					jsonMarshallerContext.Writer.WriteArrayStart();
					foreach (string str in publicRequest.IdentityIdsToDelete)
					{
						jsonMarshallerContext.Writer.Write(str);
					}
					jsonMarshallerContext.Writer.WriteArrayEnd();
				}
				jsonWriter.WriteObjectEnd();
				string s = stringWriter.ToString();
				request.Content = Encoding.UTF8.GetBytes(s);
			}
			return request;
		}
	}
}
