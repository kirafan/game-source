﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000051 RID: 81
	public class DescribeIdentityRequestMarshaller : IMarshaller<IRequest, DescribeIdentityRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002CB RID: 715 RVA: 0x00005B1D File Offset: 0x00003D1D
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((DescribeIdentityRequest)input);
		}

		// Token: 0x060002CC RID: 716 RVA: 0x00005B2C File Offset: 0x00003D2C
		public IRequest Marshall(DescribeIdentityRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.CognitoIdentity");
			string value = "AWSCognitoIdentityService.DescribeIdentity";
			request.Headers["X-Amz-Target"] = value;
			request.Headers["Content-Type"] = "application/x-amz-json-1.1";
			request.HttpMethod = "POST";
			string resourcePath = "/";
			request.ResourcePath = resourcePath;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter);
				jsonWriter.WriteObjectStart();
				JsonMarshallerContext jsonMarshallerContext = new JsonMarshallerContext(request, jsonWriter);
				if (publicRequest.IsSetIdentityId())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityId");
					jsonMarshallerContext.Writer.Write(publicRequest.IdentityId);
				}
				jsonWriter.WriteObjectEnd();
				string s = stringWriter.ToString();
				request.Content = Encoding.UTF8.GetBytes(s);
			}
			return request;
		}
	}
}
