﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000054 RID: 84
	public class GetCredentialsForIdentityResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x060002D7 RID: 727 RVA: 0x00006024 File Offset: 0x00004224
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			GetCredentialsForIdentityResponse getCredentialsForIdentityResponse = new GetCredentialsForIdentityResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("Credentials", currentDepth))
				{
					CredentialsUnmarshaller instance = CredentialsUnmarshaller.Instance;
					getCredentialsForIdentityResponse.Credentials = instance.Unmarshall(context);
				}
				else if (context.TestExpression("IdentityId", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					getCredentialsForIdentityResponse.IdentityId = instance2.Unmarshall(context);
				}
			}
			return getCredentialsForIdentityResponse;
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x00006098 File Offset: 0x00004298
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("ExternalServiceException"))
			{
				return new ExternalServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidIdentityPoolConfigurationException"))
			{
				return new InvalidIdentityPoolConfigurationException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceConflictException"))
			{
				return new ResourceConflictException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x000062A0 File Offset: 0x000044A0
		internal static GetCredentialsForIdentityResponseUnmarshaller GetInstance()
		{
			return GetCredentialsForIdentityResponseUnmarshaller._instance;
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060002DA RID: 730 RVA: 0x000062A0 File Offset: 0x000044A0
		public static GetCredentialsForIdentityResponseUnmarshaller Instance
		{
			get
			{
				return GetCredentialsForIdentityResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000A8 RID: 168
		private static GetCredentialsForIdentityResponseUnmarshaller _instance = new GetCredentialsForIdentityResponseUnmarshaller();
	}
}
