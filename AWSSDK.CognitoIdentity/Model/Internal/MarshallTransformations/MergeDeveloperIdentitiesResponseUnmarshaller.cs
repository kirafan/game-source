﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000068 RID: 104
	public class MergeDeveloperIdentitiesResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x06000331 RID: 817 RVA: 0x00008180 File Offset: 0x00006380
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			MergeDeveloperIdentitiesResponse mergeDeveloperIdentitiesResponse = new MergeDeveloperIdentitiesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("IdentityId", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					mergeDeveloperIdentitiesResponse.IdentityId = instance.Unmarshall(context);
				}
			}
			return mergeDeveloperIdentitiesResponse;
		}

		// Token: 0x06000332 RID: 818 RVA: 0x000081D0 File Offset: 0x000063D0
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceConflictException"))
			{
				return new ResourceConflictException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000333 RID: 819 RVA: 0x00008364 File Offset: 0x00006564
		internal static MergeDeveloperIdentitiesResponseUnmarshaller GetInstance()
		{
			return MergeDeveloperIdentitiesResponseUnmarshaller._instance;
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x06000334 RID: 820 RVA: 0x00008364 File Offset: 0x00006564
		public static MergeDeveloperIdentitiesResponseUnmarshaller Instance
		{
			get
			{
				return MergeDeveloperIdentitiesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000B4 RID: 180
		private static MergeDeveloperIdentitiesResponseUnmarshaller _instance = new MergeDeveloperIdentitiesResponseUnmarshaller();
	}
}
