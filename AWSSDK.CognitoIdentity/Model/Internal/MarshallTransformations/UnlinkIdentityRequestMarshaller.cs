﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000071 RID: 113
	public class UnlinkIdentityRequestMarshaller : IMarshaller<IRequest, UnlinkIdentityRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000359 RID: 857 RVA: 0x00008D83 File Offset: 0x00006F83
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((UnlinkIdentityRequest)input);
		}

		// Token: 0x0600035A RID: 858 RVA: 0x00008D94 File Offset: 0x00006F94
		public IRequest Marshall(UnlinkIdentityRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.CognitoIdentity");
			string value = "AWSCognitoIdentityService.UnlinkIdentity";
			request.Headers["X-Amz-Target"] = value;
			request.Headers["Content-Type"] = "application/x-amz-json-1.1";
			request.HttpMethod = "POST";
			string resourcePath = "/";
			request.ResourcePath = resourcePath;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter);
				jsonWriter.WriteObjectStart();
				JsonMarshallerContext jsonMarshallerContext = new JsonMarshallerContext(request, jsonWriter);
				if (publicRequest.IsSetIdentityId())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityId");
					jsonMarshallerContext.Writer.Write(publicRequest.IdentityId);
				}
				if (publicRequest.IsSetLogins())
				{
					jsonMarshallerContext.Writer.WritePropertyName("Logins");
					jsonMarshallerContext.Writer.WriteObjectStart();
					foreach (KeyValuePair<string, string> keyValuePair in publicRequest.Logins)
					{
						jsonMarshallerContext.Writer.WritePropertyName(keyValuePair.Key);
						string value2 = keyValuePair.Value;
						jsonMarshallerContext.Writer.Write(value2);
					}
					jsonMarshallerContext.Writer.WriteObjectEnd();
				}
				if (publicRequest.IsSetLoginsToRemove())
				{
					jsonMarshallerContext.Writer.WritePropertyName("LoginsToRemove");
					jsonMarshallerContext.Writer.WriteArrayStart();
					foreach (string str in publicRequest.LoginsToRemove)
					{
						jsonMarshallerContext.Writer.Write(str);
					}
					jsonMarshallerContext.Writer.WriteArrayEnd();
				}
				jsonWriter.WriteObjectEnd();
				string s = stringWriter.ToString();
				request.Content = Encoding.UTF8.GetBytes(s);
			}
			return request;
		}
	}
}
