﻿using System;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005D RID: 93
	public class IdentityDescriptionUnmarshaller : IUnmarshaller<IdentityDescription, XmlUnmarshallerContext>, IUnmarshaller<IdentityDescription, JsonUnmarshallerContext>
	{
		// Token: 0x06000301 RID: 769 RVA: 0x000049A8 File Offset: 0x00002BA8
		IdentityDescription IUnmarshaller<IdentityDescription, XmlUnmarshallerContext>.Unmarshall(XmlUnmarshallerContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000302 RID: 770 RVA: 0x00007294 File Offset: 0x00005494
		public IdentityDescription Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return null;
			}
			IdentityDescription identityDescription = new IdentityDescription();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("CreationDate", currentDepth))
				{
					DateTimeUnmarshaller instance = DateTimeUnmarshaller.Instance;
					identityDescription.CreationDate = instance.Unmarshall(context);
				}
				else if (context.TestExpression("IdentityId", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					identityDescription.IdentityId = instance2.Unmarshall(context);
				}
				else if (context.TestExpression("LastModifiedDate", currentDepth))
				{
					DateTimeUnmarshaller instance3 = DateTimeUnmarshaller.Instance;
					identityDescription.LastModifiedDate = instance3.Unmarshall(context);
				}
				else if (context.TestExpression("Logins", currentDepth))
				{
					ListUnmarshaller<string, StringUnmarshaller> listUnmarshaller = new ListUnmarshaller<string, StringUnmarshaller>(StringUnmarshaller.Instance);
					identityDescription.Logins = listUnmarshaller.Unmarshall(context);
				}
			}
			return identityDescription;
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x06000303 RID: 771 RVA: 0x00007366 File Offset: 0x00005566
		public static IdentityDescriptionUnmarshaller Instance
		{
			get
			{
				return IdentityDescriptionUnmarshaller._instance;
			}
		}

		// Token: 0x040000AD RID: 173
		private static IdentityDescriptionUnmarshaller _instance = new IdentityDescriptionUnmarshaller();
	}
}
