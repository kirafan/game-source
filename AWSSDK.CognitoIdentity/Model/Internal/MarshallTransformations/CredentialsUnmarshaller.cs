﻿using System;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200004A RID: 74
	public class CredentialsUnmarshaller : IUnmarshaller<Credentials, XmlUnmarshallerContext>, IUnmarshaller<Credentials, JsonUnmarshallerContext>
	{
		// Token: 0x060002AB RID: 683 RVA: 0x000049A8 File Offset: 0x00002BA8
		Credentials IUnmarshaller<Credentials, XmlUnmarshallerContext>.Unmarshall(XmlUnmarshallerContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060002AC RID: 684 RVA: 0x0000514C File Offset: 0x0000334C
		public Credentials Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return null;
			}
			Credentials credentials = new Credentials();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("AccessKeyId", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					credentials.AccessKeyId = instance.Unmarshall(context);
				}
				else if (context.TestExpression("Expiration", currentDepth))
				{
					DateTimeUnmarshaller instance2 = DateTimeUnmarshaller.Instance;
					credentials.Expiration = instance2.Unmarshall(context);
				}
				else if (context.TestExpression("SecretKey", currentDepth))
				{
					StringUnmarshaller instance3 = StringUnmarshaller.Instance;
					credentials.SecretKey = instance3.Unmarshall(context);
				}
				else if (context.TestExpression("SessionToken", currentDepth))
				{
					StringUnmarshaller instance4 = StringUnmarshaller.Instance;
					credentials.SessionToken = instance4.Unmarshall(context);
				}
			}
			return credentials;
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060002AD RID: 685 RVA: 0x00005219 File Offset: 0x00003419
		public static CredentialsUnmarshaller Instance
		{
			get
			{
				return CredentialsUnmarshaller._instance;
			}
		}

		// Token: 0x040000A3 RID: 163
		private static CredentialsUnmarshaller _instance = new CredentialsUnmarshaller();
	}
}
