﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000052 RID: 82
	public class DescribeIdentityResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x060002CE RID: 718 RVA: 0x00005C18 File Offset: 0x00003E18
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			DescribeIdentityResponse describeIdentityResponse = new DescribeIdentityResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("CreationDate", currentDepth))
				{
					DateTimeUnmarshaller instance = DateTimeUnmarshaller.Instance;
					describeIdentityResponse.CreationDate = instance.Unmarshall(context);
				}
				else if (context.TestExpression("IdentityId", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					describeIdentityResponse.IdentityId = instance2.Unmarshall(context);
				}
				else if (context.TestExpression("LastModifiedDate", currentDepth))
				{
					DateTimeUnmarshaller instance3 = DateTimeUnmarshaller.Instance;
					describeIdentityResponse.LastModifiedDate = instance3.Unmarshall(context);
				}
				else if (context.TestExpression("Logins", currentDepth))
				{
					ListUnmarshaller<string, StringUnmarshaller> listUnmarshaller = new ListUnmarshaller<string, StringUnmarshaller>(StringUnmarshaller.Instance);
					describeIdentityResponse.Logins = listUnmarshaller.Unmarshall(context);
				}
			}
			return describeIdentityResponse;
		}

		// Token: 0x060002CF RID: 719 RVA: 0x00005CE0 File Offset: 0x00003EE0
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002D0 RID: 720 RVA: 0x00005E3A File Offset: 0x0000403A
		internal static DescribeIdentityResponseUnmarshaller GetInstance()
		{
			return DescribeIdentityResponseUnmarshaller._instance;
		}

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060002D1 RID: 721 RVA: 0x00005E3A File Offset: 0x0000403A
		public static DescribeIdentityResponseUnmarshaller Instance
		{
			get
			{
				return DescribeIdentityResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000A7 RID: 167
		private static DescribeIdentityResponseUnmarshaller _instance = new DescribeIdentityResponseUnmarshaller();
	}
}
