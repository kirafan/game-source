﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000074 RID: 116
	public class UpdateIdentityPoolRequestMarshaller : IMarshaller<IRequest, UpdateIdentityPoolRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000367 RID: 871 RVA: 0x0000923D File Offset: 0x0000743D
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((UpdateIdentityPoolRequest)input);
		}

		// Token: 0x06000368 RID: 872 RVA: 0x0000924C File Offset: 0x0000744C
		public IRequest Marshall(UpdateIdentityPoolRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.CognitoIdentity");
			string value = "AWSCognitoIdentityService.UpdateIdentityPool";
			request.Headers["X-Amz-Target"] = value;
			request.Headers["Content-Type"] = "application/x-amz-json-1.1";
			request.HttpMethod = "POST";
			string resourcePath = "/";
			request.ResourcePath = resourcePath;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter);
				jsonWriter.WriteObjectStart();
				JsonMarshallerContext jsonMarshallerContext = new JsonMarshallerContext(request, jsonWriter);
				if (publicRequest.IsSetAllowUnauthenticatedIdentities())
				{
					jsonMarshallerContext.Writer.WritePropertyName("AllowUnauthenticatedIdentities");
					jsonMarshallerContext.Writer.Write(publicRequest.AllowUnauthenticatedIdentities);
				}
				if (publicRequest.IsSetCognitoIdentityProviders())
				{
					jsonMarshallerContext.Writer.WritePropertyName("CognitoIdentityProviders");
					jsonMarshallerContext.Writer.WriteArrayStart();
					foreach (CognitoIdentityProviderInfo requestObject in publicRequest.CognitoIdentityProviders)
					{
						jsonMarshallerContext.Writer.WriteObjectStart();
						CognitoIdentityProviderInfoMarshaller.Instance.Marshall(requestObject, jsonMarshallerContext);
						jsonMarshallerContext.Writer.WriteObjectEnd();
					}
					jsonMarshallerContext.Writer.WriteArrayEnd();
				}
				if (publicRequest.IsSetDeveloperProviderName())
				{
					jsonMarshallerContext.Writer.WritePropertyName("DeveloperProviderName");
					jsonMarshallerContext.Writer.Write(publicRequest.DeveloperProviderName);
				}
				if (publicRequest.IsSetIdentityPoolId())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityPoolId");
					jsonMarshallerContext.Writer.Write(publicRequest.IdentityPoolId);
				}
				if (publicRequest.IsSetIdentityPoolName())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityPoolName");
					jsonMarshallerContext.Writer.Write(publicRequest.IdentityPoolName);
				}
				if (publicRequest.IsSetOpenIdConnectProviderARNs())
				{
					jsonMarshallerContext.Writer.WritePropertyName("OpenIdConnectProviderARNs");
					jsonMarshallerContext.Writer.WriteArrayStart();
					foreach (string str in publicRequest.OpenIdConnectProviderARNs)
					{
						jsonMarshallerContext.Writer.Write(str);
					}
					jsonMarshallerContext.Writer.WriteArrayEnd();
				}
				if (publicRequest.IsSetSamlProviderARNs())
				{
					jsonMarshallerContext.Writer.WritePropertyName("SamlProviderARNs");
					jsonMarshallerContext.Writer.WriteArrayStart();
					foreach (string str2 in publicRequest.SamlProviderARNs)
					{
						jsonMarshallerContext.Writer.Write(str2);
					}
					jsonMarshallerContext.Writer.WriteArrayEnd();
				}
				if (publicRequest.IsSetSupportedLoginProviders())
				{
					jsonMarshallerContext.Writer.WritePropertyName("SupportedLoginProviders");
					jsonMarshallerContext.Writer.WriteObjectStart();
					foreach (KeyValuePair<string, string> keyValuePair in publicRequest.SupportedLoginProviders)
					{
						jsonMarshallerContext.Writer.WritePropertyName(keyValuePair.Key);
						string value2 = keyValuePair.Value;
						jsonMarshallerContext.Writer.Write(value2);
					}
					jsonMarshallerContext.Writer.WriteObjectEnd();
				}
				jsonWriter.WriteObjectEnd();
				string s = stringWriter.ToString();
				request.Content = Encoding.UTF8.GetBytes(s);
			}
			return request;
		}
	}
}
