﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200005C RID: 92
	public class GetOpenIdTokenResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x060002FB RID: 763 RVA: 0x0000703C File Offset: 0x0000523C
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			GetOpenIdTokenResponse getOpenIdTokenResponse = new GetOpenIdTokenResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("IdentityId", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					getOpenIdTokenResponse.IdentityId = instance.Unmarshall(context);
				}
				else if (context.TestExpression("Token", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					getOpenIdTokenResponse.Token = instance2.Unmarshall(context);
				}
			}
			return getOpenIdTokenResponse;
		}

		// Token: 0x060002FC RID: 764 RVA: 0x000070B0 File Offset: 0x000052B0
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("ExternalServiceException"))
			{
				return new ExternalServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceConflictException"))
			{
				return new ResourceConflictException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002FD RID: 765 RVA: 0x0000727E File Offset: 0x0000547E
		internal static GetOpenIdTokenResponseUnmarshaller GetInstance()
		{
			return GetOpenIdTokenResponseUnmarshaller._instance;
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x060002FE RID: 766 RVA: 0x0000727E File Offset: 0x0000547E
		public static GetOpenIdTokenResponseUnmarshaller Instance
		{
			get
			{
				return GetOpenIdTokenResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000AC RID: 172
		private static GetOpenIdTokenResponseUnmarshaller _instance = new GetOpenIdTokenResponseUnmarshaller();
	}
}
