﻿using System;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000069 RID: 105
	public class RoleMappingMarshaller : IRequestMarshaller<RoleMapping, JsonMarshallerContext>
	{
		// Token: 0x06000337 RID: 823 RVA: 0x00008378 File Offset: 0x00006578
		public void Marshall(RoleMapping requestObject, JsonMarshallerContext context)
		{
			if (requestObject.IsSetAmbiguousRoleResolution())
			{
				context.Writer.WritePropertyName("AmbiguousRoleResolution");
				context.Writer.Write(requestObject.AmbiguousRoleResolution);
			}
			if (requestObject.IsSetRulesConfiguration())
			{
				context.Writer.WritePropertyName("RulesConfiguration");
				context.Writer.WriteObjectStart();
				RulesConfigurationTypeMarshaller.Instance.Marshall(requestObject.RulesConfiguration, context);
				context.Writer.WriteObjectEnd();
			}
			if (requestObject.IsSetType())
			{
				context.Writer.WritePropertyName("Type");
				context.Writer.Write(requestObject.Type);
			}
		}

		// Token: 0x040000B5 RID: 181
		public static readonly RoleMappingMarshaller Instance = new RoleMappingMarshaller();
	}
}
