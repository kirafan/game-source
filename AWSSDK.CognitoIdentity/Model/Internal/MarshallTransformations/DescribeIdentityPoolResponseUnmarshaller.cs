﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000050 RID: 80
	public class DescribeIdentityPoolResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x060002C5 RID: 709 RVA: 0x00005830 File Offset: 0x00003A30
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			DescribeIdentityPoolResponse describeIdentityPoolResponse = new DescribeIdentityPoolResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("AllowUnauthenticatedIdentities", currentDepth))
				{
					BoolUnmarshaller instance = BoolUnmarshaller.Instance;
					describeIdentityPoolResponse.AllowUnauthenticatedIdentities = instance.Unmarshall(context);
				}
				else if (context.TestExpression("CognitoIdentityProviders", currentDepth))
				{
					ListUnmarshaller<CognitoIdentityProviderInfo, CognitoIdentityProviderInfoUnmarshaller> listUnmarshaller = new ListUnmarshaller<CognitoIdentityProviderInfo, CognitoIdentityProviderInfoUnmarshaller>(CognitoIdentityProviderInfoUnmarshaller.Instance);
					describeIdentityPoolResponse.CognitoIdentityProviders = listUnmarshaller.Unmarshall(context);
				}
				else if (context.TestExpression("DeveloperProviderName", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					describeIdentityPoolResponse.DeveloperProviderName = instance2.Unmarshall(context);
				}
				else if (context.TestExpression("IdentityPoolId", currentDepth))
				{
					StringUnmarshaller instance3 = StringUnmarshaller.Instance;
					describeIdentityPoolResponse.IdentityPoolId = instance3.Unmarshall(context);
				}
				else if (context.TestExpression("IdentityPoolName", currentDepth))
				{
					StringUnmarshaller instance4 = StringUnmarshaller.Instance;
					describeIdentityPoolResponse.IdentityPoolName = instance4.Unmarshall(context);
				}
				else if (context.TestExpression("OpenIdConnectProviderARNs", currentDepth))
				{
					ListUnmarshaller<string, StringUnmarshaller> listUnmarshaller2 = new ListUnmarshaller<string, StringUnmarshaller>(StringUnmarshaller.Instance);
					describeIdentityPoolResponse.OpenIdConnectProviderARNs = listUnmarshaller2.Unmarshall(context);
				}
				else if (context.TestExpression("SamlProviderARNs", currentDepth))
				{
					ListUnmarshaller<string, StringUnmarshaller> listUnmarshaller3 = new ListUnmarshaller<string, StringUnmarshaller>(StringUnmarshaller.Instance);
					describeIdentityPoolResponse.SamlProviderARNs = listUnmarshaller3.Unmarshall(context);
				}
				else if (context.TestExpression("SupportedLoginProviders", currentDepth))
				{
					DictionaryUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller> dictionaryUnmarshaller = new DictionaryUnmarshaller<string, string, StringUnmarshaller, StringUnmarshaller>(StringUnmarshaller.Instance, StringUnmarshaller.Instance);
					describeIdentityPoolResponse.SupportedLoginProviders = dictionaryUnmarshaller.Unmarshall(context);
				}
			}
			return describeIdentityPoolResponse;
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x000059B0 File Offset: 0x00003BB0
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002C7 RID: 711 RVA: 0x00005B0A File Offset: 0x00003D0A
		internal static DescribeIdentityPoolResponseUnmarshaller GetInstance()
		{
			return DescribeIdentityPoolResponseUnmarshaller._instance;
		}

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060002C8 RID: 712 RVA: 0x00005B0A File Offset: 0x00003D0A
		public static DescribeIdentityPoolResponseUnmarshaller Instance
		{
			get
			{
				return DescribeIdentityPoolResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000A6 RID: 166
		private static DescribeIdentityPoolResponseUnmarshaller _instance = new DescribeIdentityPoolResponseUnmarshaller();
	}
}
