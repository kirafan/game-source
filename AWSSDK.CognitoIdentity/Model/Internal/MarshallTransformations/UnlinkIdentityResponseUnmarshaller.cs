﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000072 RID: 114
	public class UnlinkIdentityResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x0600035C RID: 860 RVA: 0x00008FBC File Offset: 0x000071BC
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			return new UnlinkIdentityResponse();
		}

		// Token: 0x0600035D RID: 861 RVA: 0x00008FC4 File Offset: 0x000071C4
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("ExternalServiceException"))
			{
				return new ExternalServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceConflictException"))
			{
				return new ResourceConflictException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x0600035E RID: 862 RVA: 0x00009192 File Offset: 0x00007392
		internal static UnlinkIdentityResponseUnmarshaller GetInstance()
		{
			return UnlinkIdentityResponseUnmarshaller._instance;
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x0600035F RID: 863 RVA: 0x00009192 File Offset: 0x00007392
		public static UnlinkIdentityResponseUnmarshaller Instance
		{
			get
			{
				return UnlinkIdentityResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000BB RID: 187
		private static UnlinkIdentityResponseUnmarshaller _instance = new UnlinkIdentityResponseUnmarshaller();
	}
}
