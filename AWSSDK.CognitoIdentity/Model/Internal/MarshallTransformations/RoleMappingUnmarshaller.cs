﻿using System;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006A RID: 106
	public class RoleMappingUnmarshaller : IUnmarshaller<RoleMapping, XmlUnmarshallerContext>, IUnmarshaller<RoleMapping, JsonUnmarshallerContext>
	{
		// Token: 0x0600033A RID: 826 RVA: 0x000049A8 File Offset: 0x00002BA8
		RoleMapping IUnmarshaller<RoleMapping, XmlUnmarshallerContext>.Unmarshall(XmlUnmarshallerContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600033B RID: 827 RVA: 0x0000842C File Offset: 0x0000662C
		public RoleMapping Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return null;
			}
			RoleMapping roleMapping = new RoleMapping();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("AmbiguousRoleResolution", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					roleMapping.AmbiguousRoleResolution = instance.Unmarshall(context);
				}
				else if (context.TestExpression("RulesConfiguration", currentDepth))
				{
					RulesConfigurationTypeUnmarshaller instance2 = RulesConfigurationTypeUnmarshaller.Instance;
					roleMapping.RulesConfiguration = instance2.Unmarshall(context);
				}
				else if (context.TestExpression("Type", currentDepth))
				{
					StringUnmarshaller instance3 = StringUnmarshaller.Instance;
					roleMapping.Type = instance3.Unmarshall(context);
				}
			}
			return roleMapping;
		}

		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x0600033C RID: 828 RVA: 0x000084D8 File Offset: 0x000066D8
		public static RoleMappingUnmarshaller Instance
		{
			get
			{
				return RoleMappingUnmarshaller._instance;
			}
		}

		// Token: 0x040000B6 RID: 182
		private static RoleMappingUnmarshaller _instance = new RoleMappingUnmarshaller();
	}
}
