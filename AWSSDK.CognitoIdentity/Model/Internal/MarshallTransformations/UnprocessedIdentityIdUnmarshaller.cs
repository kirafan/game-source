﻿using System;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000073 RID: 115
	public class UnprocessedIdentityIdUnmarshaller : IUnmarshaller<UnprocessedIdentityId, XmlUnmarshallerContext>, IUnmarshaller<UnprocessedIdentityId, JsonUnmarshallerContext>
	{
		// Token: 0x06000362 RID: 866 RVA: 0x000049A8 File Offset: 0x00002BA8
		UnprocessedIdentityId IUnmarshaller<UnprocessedIdentityId, XmlUnmarshallerContext>.Unmarshall(XmlUnmarshallerContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06000363 RID: 867 RVA: 0x000091A8 File Offset: 0x000073A8
		public UnprocessedIdentityId Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return null;
			}
			UnprocessedIdentityId unprocessedIdentityId = new UnprocessedIdentityId();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("ErrorCode", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					unprocessedIdentityId.ErrorCode = instance.Unmarshall(context);
				}
				else if (context.TestExpression("IdentityId", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					unprocessedIdentityId.IdentityId = instance2.Unmarshall(context);
				}
			}
			return unprocessedIdentityId;
		}

		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x06000364 RID: 868 RVA: 0x0000922A File Offset: 0x0000742A
		public static UnprocessedIdentityIdUnmarshaller Instance
		{
			get
			{
				return UnprocessedIdentityIdUnmarshaller._instance;
			}
		}

		// Token: 0x040000BC RID: 188
		private static UnprocessedIdentityIdUnmarshaller _instance = new UnprocessedIdentityIdUnmarshaller();
	}
}
