﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200004C RID: 76
	public class DeleteIdentitiesResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x060002B3 RID: 691 RVA: 0x00005378 File Offset: 0x00003578
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			DeleteIdentitiesResponse deleteIdentitiesResponse = new DeleteIdentitiesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("UnprocessedIdentityIds", currentDepth))
				{
					ListUnmarshaller<UnprocessedIdentityId, UnprocessedIdentityIdUnmarshaller> listUnmarshaller = new ListUnmarshaller<UnprocessedIdentityId, UnprocessedIdentityIdUnmarshaller>(UnprocessedIdentityIdUnmarshaller.Instance);
					deleteIdentitiesResponse.UnprocessedIdentityIds = listUnmarshaller.Unmarshall(context);
				}
			}
			return deleteIdentitiesResponse;
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x000053CC File Offset: 0x000035CC
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x000054B2 File Offset: 0x000036B2
		internal static DeleteIdentitiesResponseUnmarshaller GetInstance()
		{
			return DeleteIdentitiesResponseUnmarshaller._instance;
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060002B6 RID: 694 RVA: 0x000054B2 File Offset: 0x000036B2
		public static DeleteIdentitiesResponseUnmarshaller Instance
		{
			get
			{
				return DeleteIdentitiesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000A4 RID: 164
		private static DeleteIdentitiesResponseUnmarshaller _instance = new DeleteIdentitiesResponseUnmarshaller();
	}
}
