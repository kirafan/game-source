﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000070 RID: 112
	public class UnlinkDeveloperIdentityResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x06000353 RID: 851 RVA: 0x00008BD4 File Offset: 0x00006DD4
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			return new UnlinkDeveloperIdentityResponse();
		}

		// Token: 0x06000354 RID: 852 RVA: 0x00008BDC File Offset: 0x00006DDC
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceConflictException"))
			{
				return new ResourceConflictException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000355 RID: 853 RVA: 0x00008D70 File Offset: 0x00006F70
		internal static UnlinkDeveloperIdentityResponseUnmarshaller GetInstance()
		{
			return UnlinkDeveloperIdentityResponseUnmarshaller._instance;
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x06000356 RID: 854 RVA: 0x00008D70 File Offset: 0x00006F70
		public static UnlinkDeveloperIdentityResponseUnmarshaller Instance
		{
			get
			{
				return UnlinkDeveloperIdentityResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000BA RID: 186
		private static UnlinkDeveloperIdentityResponseUnmarshaller _instance = new UnlinkDeveloperIdentityResponseUnmarshaller();
	}
}
