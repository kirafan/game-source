﻿using System;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000046 RID: 70
	public class CognitoIdentityProviderInfoMarshaller : IRequestMarshaller<CognitoIdentityProviderInfo, JsonMarshallerContext>
	{
		// Token: 0x0600029A RID: 666 RVA: 0x00004914 File Offset: 0x00002B14
		public void Marshall(CognitoIdentityProviderInfo requestObject, JsonMarshallerContext context)
		{
			if (requestObject.IsSetClientId())
			{
				context.Writer.WritePropertyName("ClientId");
				context.Writer.Write(requestObject.ClientId);
			}
			if (requestObject.IsSetProviderName())
			{
				context.Writer.WritePropertyName("ProviderName");
				context.Writer.Write(requestObject.ProviderName);
			}
			if (requestObject.IsSetServerSideTokenCheck())
			{
				context.Writer.WritePropertyName("ServerSideTokenCheck");
				context.Writer.Write(requestObject.ServerSideTokenCheck);
			}
		}

		// Token: 0x040000A0 RID: 160
		public static readonly CognitoIdentityProviderInfoMarshaller Instance = new CognitoIdentityProviderInfoMarshaller();
	}
}
