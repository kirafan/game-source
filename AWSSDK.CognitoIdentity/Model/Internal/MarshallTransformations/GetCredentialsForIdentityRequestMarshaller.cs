﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000053 RID: 83
	public class GetCredentialsForIdentityRequestMarshaller : IMarshaller<IRequest, GetCredentialsForIdentityRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002D4 RID: 724 RVA: 0x00005E4D File Offset: 0x0000404D
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((GetCredentialsForIdentityRequest)input);
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x00005E5C File Offset: 0x0000405C
		public IRequest Marshall(GetCredentialsForIdentityRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.CognitoIdentity");
			string value = "AWSCognitoIdentityService.GetCredentialsForIdentity";
			request.Headers["X-Amz-Target"] = value;
			request.Headers["Content-Type"] = "application/x-amz-json-1.1";
			request.HttpMethod = "POST";
			string resourcePath = "/";
			request.ResourcePath = resourcePath;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter);
				jsonWriter.WriteObjectStart();
				JsonMarshallerContext jsonMarshallerContext = new JsonMarshallerContext(request, jsonWriter);
				if (publicRequest.IsSetCustomRoleArn())
				{
					jsonMarshallerContext.Writer.WritePropertyName("CustomRoleArn");
					jsonMarshallerContext.Writer.Write(publicRequest.CustomRoleArn);
				}
				if (publicRequest.IsSetIdentityId())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityId");
					jsonMarshallerContext.Writer.Write(publicRequest.IdentityId);
				}
				if (publicRequest.IsSetLogins())
				{
					jsonMarshallerContext.Writer.WritePropertyName("Logins");
					jsonMarshallerContext.Writer.WriteObjectStart();
					foreach (KeyValuePair<string, string> keyValuePair in publicRequest.Logins)
					{
						jsonMarshallerContext.Writer.WritePropertyName(keyValuePair.Key);
						string value2 = keyValuePair.Value;
						jsonMarshallerContext.Writer.Write(value2);
					}
					jsonMarshallerContext.Writer.WriteObjectEnd();
				}
				jsonWriter.WriteObjectEnd();
				string s = stringWriter.ToString();
				request.Content = Encoding.UTF8.GetBytes(s);
			}
			return request;
		}
	}
}
