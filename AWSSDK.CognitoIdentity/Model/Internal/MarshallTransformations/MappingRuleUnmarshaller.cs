﻿using System;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000066 RID: 102
	public class MappingRuleUnmarshaller : IUnmarshaller<MappingRule, XmlUnmarshallerContext>, IUnmarshaller<MappingRule, JsonUnmarshallerContext>
	{
		// Token: 0x06000329 RID: 809 RVA: 0x000049A8 File Offset: 0x00002BA8
		MappingRule IUnmarshaller<MappingRule, XmlUnmarshallerContext>.Unmarshall(XmlUnmarshallerContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600032A RID: 810 RVA: 0x00007F20 File Offset: 0x00006120
		public MappingRule Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return null;
			}
			MappingRule mappingRule = new MappingRule();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("Claim", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					mappingRule.Claim = instance.Unmarshall(context);
				}
				else if (context.TestExpression("MatchType", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					mappingRule.MatchType = instance2.Unmarshall(context);
				}
				else if (context.TestExpression("RoleARN", currentDepth))
				{
					StringUnmarshaller instance3 = StringUnmarshaller.Instance;
					mappingRule.RoleARN = instance3.Unmarshall(context);
				}
				else if (context.TestExpression("Value", currentDepth))
				{
					StringUnmarshaller instance4 = StringUnmarshaller.Instance;
					mappingRule.Value = instance4.Unmarshall(context);
				}
			}
			return mappingRule;
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x0600032B RID: 811 RVA: 0x00007FF2 File Offset: 0x000061F2
		public static MappingRuleUnmarshaller Instance
		{
			get
			{
				return MappingRuleUnmarshaller._instance;
			}
		}

		// Token: 0x040000B3 RID: 179
		private static MappingRuleUnmarshaller _instance = new MappingRuleUnmarshaller();
	}
}
