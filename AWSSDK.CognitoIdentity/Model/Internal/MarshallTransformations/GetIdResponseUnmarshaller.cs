﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000058 RID: 88
	public class GetIdResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x060002E9 RID: 745 RVA: 0x000067E0 File Offset: 0x000049E0
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			GetIdResponse getIdResponse = new GetIdResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("IdentityId", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					getIdResponse.IdentityId = instance.Unmarshall(context);
				}
			}
			return getIdResponse;
		}

		// Token: 0x060002EA RID: 746 RVA: 0x00006830 File Offset: 0x00004A30
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("ExternalServiceException"))
			{
				return new ExternalServiceException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("LimitExceededException"))
			{
				return new LimitExceededException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceConflictException"))
			{
				return new ResourceConflictException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x060002EB RID: 747 RVA: 0x00006A38 File Offset: 0x00004C38
		internal static GetIdResponseUnmarshaller GetInstance()
		{
			return GetIdResponseUnmarshaller._instance;
		}

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x060002EC RID: 748 RVA: 0x00006A38 File Offset: 0x00004C38
		public static GetIdResponseUnmarshaller Instance
		{
			get
			{
				return GetIdResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000AA RID: 170
		private static GetIdResponseUnmarshaller _instance = new GetIdResponseUnmarshaller();
	}
}
