﻿using System;
using System.Net;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000060 RID: 96
	public class ListIdentitiesResponseUnmarshaller : JsonResponseUnmarshaller
	{
		// Token: 0x0600030E RID: 782 RVA: 0x00007588 File Offset: 0x00005788
		public override AmazonWebServiceResponse Unmarshall(JsonUnmarshallerContext context)
		{
			ListIdentitiesResponse listIdentitiesResponse = new ListIdentitiesResponse();
			context.Read();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("Identities", currentDepth))
				{
					ListUnmarshaller<IdentityDescription, IdentityDescriptionUnmarshaller> listUnmarshaller = new ListUnmarshaller<IdentityDescription, IdentityDescriptionUnmarshaller>(IdentityDescriptionUnmarshaller.Instance);
					listIdentitiesResponse.Identities = listUnmarshaller.Unmarshall(context);
				}
				else if (context.TestExpression("IdentityPoolId", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					listIdentitiesResponse.IdentityPoolId = instance.Unmarshall(context);
				}
				else if (context.TestExpression("NextToken", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					listIdentitiesResponse.NextToken = instance2.Unmarshall(context);
				}
			}
			return listIdentitiesResponse;
		}

		// Token: 0x0600030F RID: 783 RVA: 0x00007624 File Offset: 0x00005824
		public override AmazonServiceException UnmarshallException(JsonUnmarshallerContext context, Exception innerException, HttpStatusCode statusCode)
		{
			ErrorResponse errorResponse = JsonErrorResponseUnmarshaller.GetInstance().Unmarshall(context);
			if (errorResponse.Code != null && errorResponse.Code.Equals("InternalErrorException"))
			{
				return new InternalErrorException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("InvalidParameterException"))
			{
				return new InvalidParameterException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("NotAuthorizedException"))
			{
				return new NotAuthorizedException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("ResourceNotFoundException"))
			{
				return new ResourceNotFoundException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			if (errorResponse.Code != null && errorResponse.Code.Equals("TooManyRequestsException"))
			{
				return new TooManyRequestsException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
			}
			return new AmazonCognitoIdentityException(errorResponse.Message, innerException, errorResponse.Type, errorResponse.Code, errorResponse.RequestId, statusCode);
		}

		// Token: 0x06000310 RID: 784 RVA: 0x0000777E File Offset: 0x0000597E
		internal static ListIdentitiesResponseUnmarshaller GetInstance()
		{
			return ListIdentitiesResponseUnmarshaller._instance;
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x06000311 RID: 785 RVA: 0x0000777E File Offset: 0x0000597E
		public static ListIdentitiesResponseUnmarshaller Instance
		{
			get
			{
				return ListIdentitiesResponseUnmarshaller._instance;
			}
		}

		// Token: 0x040000AF RID: 175
		private static ListIdentitiesResponseUnmarshaller _instance = new ListIdentitiesResponseUnmarshaller();
	}
}
