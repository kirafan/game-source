﻿using System;
using Amazon.Runtime.Internal.Transform;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006B RID: 107
	public class RulesConfigurationTypeMarshaller : IRequestMarshaller<RulesConfigurationType, JsonMarshallerContext>
	{
		// Token: 0x0600033F RID: 831 RVA: 0x000084EC File Offset: 0x000066EC
		public void Marshall(RulesConfigurationType requestObject, JsonMarshallerContext context)
		{
			if (requestObject.IsSetRules())
			{
				context.Writer.WritePropertyName("Rules");
				context.Writer.WriteArrayStart();
				foreach (MappingRule requestObject2 in requestObject.Rules)
				{
					context.Writer.WriteObjectStart();
					MappingRuleMarshaller.Instance.Marshall(requestObject2, context);
					context.Writer.WriteObjectEnd();
				}
				context.Writer.WriteArrayEnd();
			}
		}

		// Token: 0x040000B7 RID: 183
		public static readonly RulesConfigurationTypeMarshaller Instance = new RulesConfigurationTypeMarshaller();
	}
}
