﻿using System;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200004D RID: 77
	public class DeleteIdentityPoolRequestMarshaller : IMarshaller<IRequest, DeleteIdentityPoolRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x060002B9 RID: 697 RVA: 0x000054C5 File Offset: 0x000036C5
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((DeleteIdentityPoolRequest)input);
		}

		// Token: 0x060002BA RID: 698 RVA: 0x000054D4 File Offset: 0x000036D4
		public IRequest Marshall(DeleteIdentityPoolRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.CognitoIdentity");
			string value = "AWSCognitoIdentityService.DeleteIdentityPool";
			request.Headers["X-Amz-Target"] = value;
			request.Headers["Content-Type"] = "application/x-amz-json-1.1";
			request.HttpMethod = "POST";
			string resourcePath = "/";
			request.ResourcePath = resourcePath;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter);
				jsonWriter.WriteObjectStart();
				JsonMarshallerContext jsonMarshallerContext = new JsonMarshallerContext(request, jsonWriter);
				if (publicRequest.IsSetIdentityPoolId())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityPoolId");
					jsonMarshallerContext.Writer.Write(publicRequest.IdentityPoolId);
				}
				jsonWriter.WriteObjectEnd();
				string s = stringWriter.ToString();
				request.Content = Encoding.UTF8.GetBytes(s);
			}
			return request;
		}
	}
}
