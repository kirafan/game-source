﻿using System;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x02000047 RID: 71
	public class CognitoIdentityProviderInfoUnmarshaller : IUnmarshaller<CognitoIdentityProviderInfo, XmlUnmarshallerContext>, IUnmarshaller<CognitoIdentityProviderInfo, JsonUnmarshallerContext>
	{
		// Token: 0x0600029D RID: 669 RVA: 0x000049A8 File Offset: 0x00002BA8
		CognitoIdentityProviderInfo IUnmarshaller<CognitoIdentityProviderInfo, XmlUnmarshallerContext>.Unmarshall(XmlUnmarshallerContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600029E RID: 670 RVA: 0x000049B0 File Offset: 0x00002BB0
		public CognitoIdentityProviderInfo Unmarshall(JsonUnmarshallerContext context)
		{
			context.Read();
			if (context.CurrentTokenType == JsonToken.Null)
			{
				return null;
			}
			CognitoIdentityProviderInfo cognitoIdentityProviderInfo = new CognitoIdentityProviderInfo();
			int currentDepth = context.CurrentDepth;
			while (context.ReadAtDepth(currentDepth))
			{
				if (context.TestExpression("ClientId", currentDepth))
				{
					StringUnmarshaller instance = StringUnmarshaller.Instance;
					cognitoIdentityProviderInfo.ClientId = instance.Unmarshall(context);
				}
				else if (context.TestExpression("ProviderName", currentDepth))
				{
					StringUnmarshaller instance2 = StringUnmarshaller.Instance;
					cognitoIdentityProviderInfo.ProviderName = instance2.Unmarshall(context);
				}
				else if (context.TestExpression("ServerSideTokenCheck", currentDepth))
				{
					BoolUnmarshaller instance3 = BoolUnmarshaller.Instance;
					cognitoIdentityProviderInfo.ServerSideTokenCheck = instance3.Unmarshall(context);
				}
			}
			return cognitoIdentityProviderInfo;
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x0600029F RID: 671 RVA: 0x00004A52 File Offset: 0x00002C52
		public static CognitoIdentityProviderInfoUnmarshaller Instance
		{
			get
			{
				return CognitoIdentityProviderInfoUnmarshaller._instance;
			}
		}

		// Token: 0x040000A1 RID: 161
		private static CognitoIdentityProviderInfoUnmarshaller _instance = new CognitoIdentityProviderInfoUnmarshaller();
	}
}
