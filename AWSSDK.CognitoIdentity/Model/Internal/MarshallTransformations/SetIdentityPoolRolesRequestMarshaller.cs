﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Text;
using Amazon.Runtime;
using Amazon.Runtime.Internal;
using Amazon.Runtime.Internal.Transform;
using ThirdParty.Json.LitJson;

namespace Amazon.CognitoIdentity.Model.Internal.MarshallTransformations
{
	// Token: 0x0200006D RID: 109
	public class SetIdentityPoolRolesRequestMarshaller : IMarshaller<IRequest, SetIdentityPoolRolesRequest>, IMarshaller<IRequest, AmazonWebServiceRequest>
	{
		// Token: 0x06000347 RID: 839 RVA: 0x00008606 File Offset: 0x00006806
		public IRequest Marshall(AmazonWebServiceRequest input)
		{
			return this.Marshall((SetIdentityPoolRolesRequest)input);
		}

		// Token: 0x06000348 RID: 840 RVA: 0x00008614 File Offset: 0x00006814
		public IRequest Marshall(SetIdentityPoolRolesRequest publicRequest)
		{
			IRequest request = new DefaultRequest(publicRequest, "Amazon.CognitoIdentity");
			string value = "AWSCognitoIdentityService.SetIdentityPoolRoles";
			request.Headers["X-Amz-Target"] = value;
			request.Headers["Content-Type"] = "application/x-amz-json-1.1";
			request.HttpMethod = "POST";
			string resourcePath = "/";
			request.ResourcePath = resourcePath;
			using (StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture))
			{
				JsonWriter jsonWriter = new JsonWriter(stringWriter);
				jsonWriter.WriteObjectStart();
				JsonMarshallerContext jsonMarshallerContext = new JsonMarshallerContext(request, jsonWriter);
				if (publicRequest.IsSetIdentityPoolId())
				{
					jsonMarshallerContext.Writer.WritePropertyName("IdentityPoolId");
					jsonMarshallerContext.Writer.Write(publicRequest.IdentityPoolId);
				}
				if (publicRequest.IsSetRoleMappings())
				{
					jsonMarshallerContext.Writer.WritePropertyName("RoleMappings");
					jsonMarshallerContext.Writer.WriteObjectStart();
					foreach (KeyValuePair<string, RoleMapping> keyValuePair in publicRequest.RoleMappings)
					{
						jsonMarshallerContext.Writer.WritePropertyName(keyValuePair.Key);
						RoleMapping value2 = keyValuePair.Value;
						jsonMarshallerContext.Writer.WriteObjectStart();
						RoleMappingMarshaller.Instance.Marshall(value2, jsonMarshallerContext);
						jsonMarshallerContext.Writer.WriteObjectEnd();
					}
					jsonMarshallerContext.Writer.WriteObjectEnd();
				}
				if (publicRequest.IsSetRoles())
				{
					jsonMarshallerContext.Writer.WritePropertyName("Roles");
					jsonMarshallerContext.Writer.WriteObjectStart();
					foreach (KeyValuePair<string, string> keyValuePair2 in publicRequest.Roles)
					{
						jsonMarshallerContext.Writer.WritePropertyName(keyValuePair2.Key);
						string value3 = keyValuePair2.Value;
						jsonMarshallerContext.Writer.Write(value3);
					}
					jsonMarshallerContext.Writer.WriteObjectEnd();
				}
				jsonWriter.WriteObjectEnd();
				string s = stringWriter.ToString();
				request.Content = Encoding.UTF8.GetBytes(s);
			}
			return request;
		}
	}
}
