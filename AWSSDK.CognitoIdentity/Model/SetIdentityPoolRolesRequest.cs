﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200003C RID: 60
	public class SetIdentityPoolRolesRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000237 RID: 567 RVA: 0x0000448F File Offset: 0x0000268F
		// (set) Token: 0x06000238 RID: 568 RVA: 0x00004497 File Offset: 0x00002697
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000239 RID: 569 RVA: 0x000044A0 File Offset: 0x000026A0
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x0600023A RID: 570 RVA: 0x000044AB File Offset: 0x000026AB
		// (set) Token: 0x0600023B RID: 571 RVA: 0x000044B3 File Offset: 0x000026B3
		public Dictionary<string, RoleMapping> RoleMappings
		{
			get
			{
				return this._roleMappings;
			}
			set
			{
				this._roleMappings = value;
			}
		}

		// Token: 0x0600023C RID: 572 RVA: 0x000044BC File Offset: 0x000026BC
		internal bool IsSetRoleMappings()
		{
			return this._roleMappings != null && this._roleMappings.Count > 0;
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x0600023D RID: 573 RVA: 0x000044D6 File Offset: 0x000026D6
		// (set) Token: 0x0600023E RID: 574 RVA: 0x000044DE File Offset: 0x000026DE
		public Dictionary<string, string> Roles
		{
			get
			{
				return this._roles;
			}
			set
			{
				this._roles = value;
			}
		}

		// Token: 0x0600023F RID: 575 RVA: 0x000044E7 File Offset: 0x000026E7
		internal bool IsSetRoles()
		{
			return this._roles != null && this._roles.Count > 0;
		}

		// Token: 0x04000084 RID: 132
		private string _identityPoolId;

		// Token: 0x04000085 RID: 133
		private Dictionary<string, RoleMapping> _roleMappings = new Dictionary<string, RoleMapping>();

		// Token: 0x04000086 RID: 134
		private Dictionary<string, string> _roles = new Dictionary<string, string>();
	}
}
