﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000018 RID: 24
	public class DescribeIdentityPoolRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000029 RID: 41
		// (get) Token: 0x06000105 RID: 261 RVA: 0x000039DA File Offset: 0x00001BDA
		// (set) Token: 0x06000106 RID: 262 RVA: 0x000039E2 File Offset: 0x00001BE2
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000107 RID: 263 RVA: 0x000039EB File Offset: 0x00001BEB
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x04000039 RID: 57
		private string _identityPoolId;
	}
}
