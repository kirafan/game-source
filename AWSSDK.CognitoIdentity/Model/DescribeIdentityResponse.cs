﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200001B RID: 27
	public class DescribeIdentityResponse : AmazonWebServiceResponse
	{
		// Token: 0x17000033 RID: 51
		// (get) Token: 0x06000126 RID: 294 RVA: 0x00003B6E File Offset: 0x00001D6E
		// (set) Token: 0x06000127 RID: 295 RVA: 0x00003B7B File Offset: 0x00001D7B
		public DateTime CreationDate
		{
			get
			{
				return this._creationDate.GetValueOrDefault();
			}
			set
			{
				this._creationDate = new DateTime?(value);
			}
		}

		// Token: 0x06000128 RID: 296 RVA: 0x00003B89 File Offset: 0x00001D89
		internal bool IsSetCreationDate()
		{
			return this._creationDate != null;
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x06000129 RID: 297 RVA: 0x00003B96 File Offset: 0x00001D96
		// (set) Token: 0x0600012A RID: 298 RVA: 0x00003B9E File Offset: 0x00001D9E
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x0600012B RID: 299 RVA: 0x00003BA7 File Offset: 0x00001DA7
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600012C RID: 300 RVA: 0x00003BB2 File Offset: 0x00001DB2
		// (set) Token: 0x0600012D RID: 301 RVA: 0x00003BBF File Offset: 0x00001DBF
		public DateTime LastModifiedDate
		{
			get
			{
				return this._lastModifiedDate.GetValueOrDefault();
			}
			set
			{
				this._lastModifiedDate = new DateTime?(value);
			}
		}

		// Token: 0x0600012E RID: 302 RVA: 0x00003BCD File Offset: 0x00001DCD
		internal bool IsSetLastModifiedDate()
		{
			return this._lastModifiedDate != null;
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600012F RID: 303 RVA: 0x00003BDA File Offset: 0x00001DDA
		// (set) Token: 0x06000130 RID: 304 RVA: 0x00003BE2 File Offset: 0x00001DE2
		public List<string> Logins
		{
			get
			{
				return this._logins;
			}
			set
			{
				this._logins = value;
			}
		}

		// Token: 0x06000131 RID: 305 RVA: 0x00003BEB File Offset: 0x00001DEB
		internal bool IsSetLogins()
		{
			return this._logins != null && this._logins.Count > 0;
		}

		// Token: 0x04000043 RID: 67
		private DateTime? _creationDate;

		// Token: 0x04000044 RID: 68
		private string _identityId;

		// Token: 0x04000045 RID: 69
		private DateTime? _lastModifiedDate;

		// Token: 0x04000046 RID: 70
		private List<string> _logins = new List<string>();
	}
}
