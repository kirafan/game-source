﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000019 RID: 25
	public class DescribeIdentityPoolResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700002A RID: 42
		// (get) Token: 0x06000109 RID: 265 RVA: 0x000039F6 File Offset: 0x00001BF6
		// (set) Token: 0x0600010A RID: 266 RVA: 0x00003A03 File Offset: 0x00001C03
		public bool AllowUnauthenticatedIdentities
		{
			get
			{
				return this._allowUnauthenticatedIdentities.GetValueOrDefault();
			}
			set
			{
				this._allowUnauthenticatedIdentities = new bool?(value);
			}
		}

		// Token: 0x0600010B RID: 267 RVA: 0x00003A11 File Offset: 0x00001C11
		internal bool IsSetAllowUnauthenticatedIdentities()
		{
			return this._allowUnauthenticatedIdentities != null;
		}

		// Token: 0x1700002B RID: 43
		// (get) Token: 0x0600010C RID: 268 RVA: 0x00003A1E File Offset: 0x00001C1E
		// (set) Token: 0x0600010D RID: 269 RVA: 0x00003A26 File Offset: 0x00001C26
		public List<CognitoIdentityProviderInfo> CognitoIdentityProviders
		{
			get
			{
				return this._cognitoIdentityProviders;
			}
			set
			{
				this._cognitoIdentityProviders = value;
			}
		}

		// Token: 0x0600010E RID: 270 RVA: 0x00003A2F File Offset: 0x00001C2F
		internal bool IsSetCognitoIdentityProviders()
		{
			return this._cognitoIdentityProviders != null && this._cognitoIdentityProviders.Count > 0;
		}

		// Token: 0x1700002C RID: 44
		// (get) Token: 0x0600010F RID: 271 RVA: 0x00003A49 File Offset: 0x00001C49
		// (set) Token: 0x06000110 RID: 272 RVA: 0x00003A51 File Offset: 0x00001C51
		public string DeveloperProviderName
		{
			get
			{
				return this._developerProviderName;
			}
			set
			{
				this._developerProviderName = value;
			}
		}

		// Token: 0x06000111 RID: 273 RVA: 0x00003A5A File Offset: 0x00001C5A
		internal bool IsSetDeveloperProviderName()
		{
			return this._developerProviderName != null;
		}

		// Token: 0x1700002D RID: 45
		// (get) Token: 0x06000112 RID: 274 RVA: 0x00003A65 File Offset: 0x00001C65
		// (set) Token: 0x06000113 RID: 275 RVA: 0x00003A6D File Offset: 0x00001C6D
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000114 RID: 276 RVA: 0x00003A76 File Offset: 0x00001C76
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x06000115 RID: 277 RVA: 0x00003A81 File Offset: 0x00001C81
		// (set) Token: 0x06000116 RID: 278 RVA: 0x00003A89 File Offset: 0x00001C89
		public string IdentityPoolName
		{
			get
			{
				return this._identityPoolName;
			}
			set
			{
				this._identityPoolName = value;
			}
		}

		// Token: 0x06000117 RID: 279 RVA: 0x00003A92 File Offset: 0x00001C92
		internal bool IsSetIdentityPoolName()
		{
			return this._identityPoolName != null;
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x06000118 RID: 280 RVA: 0x00003A9D File Offset: 0x00001C9D
		// (set) Token: 0x06000119 RID: 281 RVA: 0x00003AA5 File Offset: 0x00001CA5
		public List<string> OpenIdConnectProviderARNs
		{
			get
			{
				return this._openIdConnectProviderARNs;
			}
			set
			{
				this._openIdConnectProviderARNs = value;
			}
		}

		// Token: 0x0600011A RID: 282 RVA: 0x00003AAE File Offset: 0x00001CAE
		internal bool IsSetOpenIdConnectProviderARNs()
		{
			return this._openIdConnectProviderARNs != null && this._openIdConnectProviderARNs.Count > 0;
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600011B RID: 283 RVA: 0x00003AC8 File Offset: 0x00001CC8
		// (set) Token: 0x0600011C RID: 284 RVA: 0x00003AD0 File Offset: 0x00001CD0
		public List<string> SamlProviderARNs
		{
			get
			{
				return this._samlProviderARNs;
			}
			set
			{
				this._samlProviderARNs = value;
			}
		}

		// Token: 0x0600011D RID: 285 RVA: 0x00003AD9 File Offset: 0x00001CD9
		internal bool IsSetSamlProviderARNs()
		{
			return this._samlProviderARNs != null && this._samlProviderARNs.Count > 0;
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600011E RID: 286 RVA: 0x00003AF3 File Offset: 0x00001CF3
		// (set) Token: 0x0600011F RID: 287 RVA: 0x00003AFB File Offset: 0x00001CFB
		public Dictionary<string, string> SupportedLoginProviders
		{
			get
			{
				return this._supportedLoginProviders;
			}
			set
			{
				this._supportedLoginProviders = value;
			}
		}

		// Token: 0x06000120 RID: 288 RVA: 0x00003B04 File Offset: 0x00001D04
		internal bool IsSetSupportedLoginProviders()
		{
			return this._supportedLoginProviders != null && this._supportedLoginProviders.Count > 0;
		}

		// Token: 0x0400003A RID: 58
		private bool? _allowUnauthenticatedIdentities;

		// Token: 0x0400003B RID: 59
		private List<CognitoIdentityProviderInfo> _cognitoIdentityProviders = new List<CognitoIdentityProviderInfo>();

		// Token: 0x0400003C RID: 60
		private string _developerProviderName;

		// Token: 0x0400003D RID: 61
		private string _identityPoolId;

		// Token: 0x0400003E RID: 62
		private string _identityPoolName;

		// Token: 0x0400003F RID: 63
		private List<string> _openIdConnectProviderARNs = new List<string>();

		// Token: 0x04000040 RID: 64
		private List<string> _samlProviderARNs = new List<string>();

		// Token: 0x04000041 RID: 65
		private Dictionary<string, string> _supportedLoginProviders = new Dictionary<string, string>();
	}
}
