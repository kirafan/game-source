﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200002A RID: 42
	[Serializable]
	public class InternalErrorException : AmazonCognitoIdentityException
	{
		// Token: 0x060001A2 RID: 418 RVA: 0x00003650 File Offset: 0x00001850
		public InternalErrorException(string message) : base(message)
		{
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x00003659 File Offset: 0x00001859
		public InternalErrorException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x00003663 File Offset: 0x00001863
		public InternalErrorException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x0000366C File Offset: 0x0000186C
		public InternalErrorException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x0000367D File Offset: 0x0000187D
		public InternalErrorException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x0000368C File Offset: 0x0000188C
		protected InternalErrorException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
