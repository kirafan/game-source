﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200001E RID: 30
	public class GetCredentialsForIdentityRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000037 RID: 55
		// (get) Token: 0x0600013F RID: 319 RVA: 0x00003C18 File Offset: 0x00001E18
		// (set) Token: 0x06000140 RID: 320 RVA: 0x00003C20 File Offset: 0x00001E20
		public string CustomRoleArn
		{
			get
			{
				return this._customRoleArn;
			}
			set
			{
				this._customRoleArn = value;
			}
		}

		// Token: 0x06000141 RID: 321 RVA: 0x00003C29 File Offset: 0x00001E29
		internal bool IsSetCustomRoleArn()
		{
			return this._customRoleArn != null;
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000142 RID: 322 RVA: 0x00003C34 File Offset: 0x00001E34
		// (set) Token: 0x06000143 RID: 323 RVA: 0x00003C3C File Offset: 0x00001E3C
		public string IdentityId
		{
			get
			{
				return this._identityId;
			}
			set
			{
				this._identityId = value;
			}
		}

		// Token: 0x06000144 RID: 324 RVA: 0x00003C45 File Offset: 0x00001E45
		internal bool IsSetIdentityId()
		{
			return this._identityId != null;
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000145 RID: 325 RVA: 0x00003C50 File Offset: 0x00001E50
		// (set) Token: 0x06000146 RID: 326 RVA: 0x00003C58 File Offset: 0x00001E58
		public Dictionary<string, string> Logins
		{
			get
			{
				return this._logins;
			}
			set
			{
				this._logins = value;
			}
		}

		// Token: 0x06000147 RID: 327 RVA: 0x00003C61 File Offset: 0x00001E61
		internal bool IsSetLogins()
		{
			return this._logins != null && this._logins.Count > 0;
		}

		// Token: 0x04000047 RID: 71
		private string _customRoleArn;

		// Token: 0x04000048 RID: 72
		private string _identityId;

		// Token: 0x04000049 RID: 73
		private Dictionary<string, string> _logins = new Dictionary<string, string>();
	}
}
