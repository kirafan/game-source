﻿using System;
using System.Net;
using System.Runtime.Serialization;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200003E RID: 62
	[Serializable]
	public class TooManyRequestsException : AmazonCognitoIdentityException
	{
		// Token: 0x06000242 RID: 578 RVA: 0x00003650 File Offset: 0x00001850
		public TooManyRequestsException(string message) : base(message)
		{
		}

		// Token: 0x06000243 RID: 579 RVA: 0x00003659 File Offset: 0x00001859
		public TooManyRequestsException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06000244 RID: 580 RVA: 0x00003663 File Offset: 0x00001863
		public TooManyRequestsException(Exception innerException) : base(innerException)
		{
		}

		// Token: 0x06000245 RID: 581 RVA: 0x0000366C File Offset: 0x0000186C
		public TooManyRequestsException(string message, Exception innerException, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, innerException, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000246 RID: 582 RVA: 0x0000367D File Offset: 0x0000187D
		public TooManyRequestsException(string message, ErrorType errorType, string errorCode, string requestId, HttpStatusCode statusCode) : base(message, errorType, errorCode, requestId, statusCode)
		{
		}

		// Token: 0x06000247 RID: 583 RVA: 0x0000368C File Offset: 0x0000188C
		protected TooManyRequestsException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}
	}
}
