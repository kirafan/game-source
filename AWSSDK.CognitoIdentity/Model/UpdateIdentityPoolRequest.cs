﻿using System;
using System.Collections.Generic;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000044 RID: 68
	public class UpdateIdentityPoolRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000080 RID: 128
		// (get) Token: 0x06000268 RID: 616 RVA: 0x0000465A File Offset: 0x0000285A
		// (set) Token: 0x06000269 RID: 617 RVA: 0x00004667 File Offset: 0x00002867
		public bool AllowUnauthenticatedIdentities
		{
			get
			{
				return this._allowUnauthenticatedIdentities.GetValueOrDefault();
			}
			set
			{
				this._allowUnauthenticatedIdentities = new bool?(value);
			}
		}

		// Token: 0x0600026A RID: 618 RVA: 0x00004675 File Offset: 0x00002875
		internal bool IsSetAllowUnauthenticatedIdentities()
		{
			return this._allowUnauthenticatedIdentities != null;
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x0600026B RID: 619 RVA: 0x00004682 File Offset: 0x00002882
		// (set) Token: 0x0600026C RID: 620 RVA: 0x0000468A File Offset: 0x0000288A
		public List<CognitoIdentityProviderInfo> CognitoIdentityProviders
		{
			get
			{
				return this._cognitoIdentityProviders;
			}
			set
			{
				this._cognitoIdentityProviders = value;
			}
		}

		// Token: 0x0600026D RID: 621 RVA: 0x00004693 File Offset: 0x00002893
		internal bool IsSetCognitoIdentityProviders()
		{
			return this._cognitoIdentityProviders != null && this._cognitoIdentityProviders.Count > 0;
		}

		// Token: 0x17000082 RID: 130
		// (get) Token: 0x0600026E RID: 622 RVA: 0x000046AD File Offset: 0x000028AD
		// (set) Token: 0x0600026F RID: 623 RVA: 0x000046B5 File Offset: 0x000028B5
		public string DeveloperProviderName
		{
			get
			{
				return this._developerProviderName;
			}
			set
			{
				this._developerProviderName = value;
			}
		}

		// Token: 0x06000270 RID: 624 RVA: 0x000046BE File Offset: 0x000028BE
		internal bool IsSetDeveloperProviderName()
		{
			return this._developerProviderName != null;
		}

		// Token: 0x17000083 RID: 131
		// (get) Token: 0x06000271 RID: 625 RVA: 0x000046C9 File Offset: 0x000028C9
		// (set) Token: 0x06000272 RID: 626 RVA: 0x000046D1 File Offset: 0x000028D1
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000273 RID: 627 RVA: 0x000046DA File Offset: 0x000028DA
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x17000084 RID: 132
		// (get) Token: 0x06000274 RID: 628 RVA: 0x000046E5 File Offset: 0x000028E5
		// (set) Token: 0x06000275 RID: 629 RVA: 0x000046ED File Offset: 0x000028ED
		public string IdentityPoolName
		{
			get
			{
				return this._identityPoolName;
			}
			set
			{
				this._identityPoolName = value;
			}
		}

		// Token: 0x06000276 RID: 630 RVA: 0x000046F6 File Offset: 0x000028F6
		internal bool IsSetIdentityPoolName()
		{
			return this._identityPoolName != null;
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000277 RID: 631 RVA: 0x00004701 File Offset: 0x00002901
		// (set) Token: 0x06000278 RID: 632 RVA: 0x00004709 File Offset: 0x00002909
		public List<string> OpenIdConnectProviderARNs
		{
			get
			{
				return this._openIdConnectProviderARNs;
			}
			set
			{
				this._openIdConnectProviderARNs = value;
			}
		}

		// Token: 0x06000279 RID: 633 RVA: 0x00004712 File Offset: 0x00002912
		internal bool IsSetOpenIdConnectProviderARNs()
		{
			return this._openIdConnectProviderARNs != null && this._openIdConnectProviderARNs.Count > 0;
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x0600027A RID: 634 RVA: 0x0000472C File Offset: 0x0000292C
		// (set) Token: 0x0600027B RID: 635 RVA: 0x00004734 File Offset: 0x00002934
		public List<string> SamlProviderARNs
		{
			get
			{
				return this._samlProviderARNs;
			}
			set
			{
				this._samlProviderARNs = value;
			}
		}

		// Token: 0x0600027C RID: 636 RVA: 0x0000473D File Offset: 0x0000293D
		internal bool IsSetSamlProviderARNs()
		{
			return this._samlProviderARNs != null && this._samlProviderARNs.Count > 0;
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x0600027D RID: 637 RVA: 0x00004757 File Offset: 0x00002957
		// (set) Token: 0x0600027E RID: 638 RVA: 0x0000475F File Offset: 0x0000295F
		public Dictionary<string, string> SupportedLoginProviders
		{
			get
			{
				return this._supportedLoginProviders;
			}
			set
			{
				this._supportedLoginProviders = value;
			}
		}

		// Token: 0x0600027F RID: 639 RVA: 0x00004768 File Offset: 0x00002968
		internal bool IsSetSupportedLoginProviders()
		{
			return this._supportedLoginProviders != null && this._supportedLoginProviders.Count > 0;
		}

		// Token: 0x04000090 RID: 144
		private bool? _allowUnauthenticatedIdentities;

		// Token: 0x04000091 RID: 145
		private List<CognitoIdentityProviderInfo> _cognitoIdentityProviders = new List<CognitoIdentityProviderInfo>();

		// Token: 0x04000092 RID: 146
		private string _developerProviderName;

		// Token: 0x04000093 RID: 147
		private string _identityPoolId;

		// Token: 0x04000094 RID: 148
		private string _identityPoolName;

		// Token: 0x04000095 RID: 149
		private List<string> _openIdConnectProviderARNs = new List<string>();

		// Token: 0x04000096 RID: 150
		private List<string> _samlProviderARNs = new List<string>();

		// Token: 0x04000097 RID: 151
		private Dictionary<string, string> _supportedLoginProviders = new Dictionary<string, string>();
	}
}
