﻿using System;
using System.Collections.Generic;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000031 RID: 49
	public class ListIdentityPoolsResponse : AmazonWebServiceResponse
	{
		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060001D8 RID: 472 RVA: 0x00004190 File Offset: 0x00002390
		// (set) Token: 0x060001D9 RID: 473 RVA: 0x00004198 File Offset: 0x00002398
		public List<IdentityPoolShortDescription> IdentityPools
		{
			get
			{
				return this._identityPools;
			}
			set
			{
				this._identityPools = value;
			}
		}

		// Token: 0x060001DA RID: 474 RVA: 0x000041A1 File Offset: 0x000023A1
		internal bool IsSetIdentityPools()
		{
			return this._identityPools != null && this._identityPools.Count > 0;
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060001DB RID: 475 RVA: 0x000041BB File Offset: 0x000023BB
		// (set) Token: 0x060001DC RID: 476 RVA: 0x000041C3 File Offset: 0x000023C3
		public string NextToken
		{
			get
			{
				return this._nextToken;
			}
			set
			{
				this._nextToken = value;
			}
		}

		// Token: 0x060001DD RID: 477 RVA: 0x000041CC File Offset: 0x000023CC
		internal bool IsSetNextToken()
		{
			return this._nextToken != null;
		}

		// Token: 0x0400006D RID: 109
		private List<IdentityPoolShortDescription> _identityPools = new List<IdentityPoolShortDescription>();

		// Token: 0x0400006E RID: 110
		private string _nextToken;
	}
}
