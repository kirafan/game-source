﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x0200003A RID: 58
	public class RoleMapping
	{
		// Token: 0x17000070 RID: 112
		// (get) Token: 0x06000229 RID: 553 RVA: 0x000043F7 File Offset: 0x000025F7
		// (set) Token: 0x0600022A RID: 554 RVA: 0x000043FF File Offset: 0x000025FF
		public AmbiguousRoleResolutionType AmbiguousRoleResolution
		{
			get
			{
				return this._ambiguousRoleResolution;
			}
			set
			{
				this._ambiguousRoleResolution = value;
			}
		}

		// Token: 0x0600022B RID: 555 RVA: 0x00004408 File Offset: 0x00002608
		internal bool IsSetAmbiguousRoleResolution()
		{
			return this._ambiguousRoleResolution != null;
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x0600022C RID: 556 RVA: 0x00004416 File Offset: 0x00002616
		// (set) Token: 0x0600022D RID: 557 RVA: 0x0000441E File Offset: 0x0000261E
		public RulesConfigurationType RulesConfiguration
		{
			get
			{
				return this._rulesConfiguration;
			}
			set
			{
				this._rulesConfiguration = value;
			}
		}

		// Token: 0x0600022E RID: 558 RVA: 0x00004427 File Offset: 0x00002627
		internal bool IsSetRulesConfiguration()
		{
			return this._rulesConfiguration != null;
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x0600022F RID: 559 RVA: 0x00004432 File Offset: 0x00002632
		// (set) Token: 0x06000230 RID: 560 RVA: 0x0000443A File Offset: 0x0000263A
		public RoleMappingType Type
		{
			get
			{
				return this._type;
			}
			set
			{
				this._type = value;
			}
		}

		// Token: 0x06000231 RID: 561 RVA: 0x00004443 File Offset: 0x00002643
		internal bool IsSetType()
		{
			return this._type != null;
		}

		// Token: 0x04000080 RID: 128
		private AmbiguousRoleResolutionType _ambiguousRoleResolution;

		// Token: 0x04000081 RID: 129
		private RulesConfigurationType _rulesConfiguration;

		// Token: 0x04000082 RID: 130
		private RoleMappingType _type;
	}
}
