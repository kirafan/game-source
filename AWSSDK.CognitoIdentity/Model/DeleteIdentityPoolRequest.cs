﻿using System;

namespace Amazon.CognitoIdentity.Model
{
	// Token: 0x02000016 RID: 22
	public class DeleteIdentityPoolRequest : AmazonCognitoIdentityRequest
	{
		// Token: 0x17000028 RID: 40
		// (get) Token: 0x06000100 RID: 256 RVA: 0x000039AE File Offset: 0x00001BAE
		// (set) Token: 0x06000101 RID: 257 RVA: 0x000039B6 File Offset: 0x00001BB6
		public string IdentityPoolId
		{
			get
			{
				return this._identityPoolId;
			}
			set
			{
				this._identityPoolId = value;
			}
		}

		// Token: 0x06000102 RID: 258 RVA: 0x000039BF File Offset: 0x00001BBF
		internal bool IsSetIdentityPoolId()
		{
			return this._identityPoolId != null;
		}

		// Token: 0x04000038 RID: 56
		private string _identityPoolId;
	}
}
