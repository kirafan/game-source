﻿using System;
using Amazon.Runtime;

namespace Amazon.CognitoIdentity
{
	// Token: 0x02000008 RID: 8
	public class AmbiguousRoleResolutionType : ConstantClass
	{
		// Token: 0x06000048 RID: 72 RVA: 0x000029BA File Offset: 0x00000BBA
		public AmbiguousRoleResolutionType(string value) : base(value)
		{
		}

		// Token: 0x06000049 RID: 73 RVA: 0x000029C3 File Offset: 0x00000BC3
		public static AmbiguousRoleResolutionType FindValue(string value)
		{
			return ConstantClass.FindValue<AmbiguousRoleResolutionType>(value);
		}

		// Token: 0x0600004A RID: 74 RVA: 0x000029CB File Offset: 0x00000BCB
		public static implicit operator AmbiguousRoleResolutionType(string value)
		{
			return AmbiguousRoleResolutionType.FindValue(value);
		}

		// Token: 0x04000015 RID: 21
		public static readonly AmbiguousRoleResolutionType AuthenticatedRole = new AmbiguousRoleResolutionType("AuthenticatedRole");

		// Token: 0x04000016 RID: 22
		public static readonly AmbiguousRoleResolutionType Deny = new AmbiguousRoleResolutionType("Deny");
	}
}
