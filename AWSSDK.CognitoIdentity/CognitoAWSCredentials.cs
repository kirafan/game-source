﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using Amazon.CognitoIdentity.Model;
using Amazon.Runtime;
using Amazon.SecurityToken;
using Amazon.SecurityToken.Model;
using Amazon.Util.Internal.PlatformServices;

namespace Amazon.CognitoIdentity
{
	// Token: 0x02000002 RID: 2
	public class CognitoAWSCredentials : RefreshingAWSCredentials
	{
		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000001 RID: 1 RVA: 0x00002050 File Offset: 0x00000250
		private bool IsIdentitySet
		{
			get
			{
				if (string.IsNullOrEmpty(this.identityId))
				{
					this.identityId = this.GetCachedIdentityId();
				}
				return !string.IsNullOrEmpty(this.identityId);
			}
		}

		// Token: 0x06000002 RID: 2 RVA: 0x0000207C File Offset: 0x0000027C
		private void UpdateIdentity(string newIdentityId)
		{
			if (string.Equals(this.identityId, newIdentityId, StringComparison.Ordinal))
			{
				return;
			}
			this.CacheIdentityId(newIdentityId);
			this.ClearCredentials();
			string oldIdentityId = this.identityId;
			this.identityId = newIdentityId;
			EventHandler<CognitoAWSCredentials.IdentityChangedArgs> eventHandler = this.mIdentityChangedEvent;
			if (eventHandler != null)
			{
				CognitoAWSCredentials.IdentityChangedArgs e = new CognitoAWSCredentials.IdentityChangedArgs(oldIdentityId, newIdentityId);
				eventHandler(this, e);
			}
		}

		// Token: 0x17000002 RID: 2
		// (get) Token: 0x06000003 RID: 3 RVA: 0x000020D0 File Offset: 0x000002D0
		protected Dictionary<string, string> CloneLogins
		{
			get
			{
				Dictionary<string, string> dictionary = new Dictionary<string, string>(this.Logins.Count, this.Logins.Comparer);
				foreach (KeyValuePair<string, string> keyValuePair in this.Logins)
				{
					dictionary.Add(keyValuePair.Key, keyValuePair.Value);
				}
				return dictionary;
			}
		}

		// Token: 0x06000004 RID: 4 RVA: 0x00002150 File Offset: 0x00000350
		protected string GetNamespacedKey(string key)
		{
			return key + ":" + this.IdentityPoolId;
		}

		// Token: 0x17000003 RID: 3
		// (get) Token: 0x06000005 RID: 5 RVA: 0x00002163 File Offset: 0x00000363
		// (set) Token: 0x06000006 RID: 6 RVA: 0x0000216B File Offset: 0x0000036B
		public string AccountId { get; private set; }

		// Token: 0x17000004 RID: 4
		// (get) Token: 0x06000007 RID: 7 RVA: 0x00002174 File Offset: 0x00000374
		// (set) Token: 0x06000008 RID: 8 RVA: 0x0000217C File Offset: 0x0000037C
		public string IdentityPoolId { get; private set; }

		// Token: 0x17000005 RID: 5
		// (get) Token: 0x06000009 RID: 9 RVA: 0x00002185 File Offset: 0x00000385
		// (set) Token: 0x0600000A RID: 10 RVA: 0x0000218D File Offset: 0x0000038D
		public string UnAuthRoleArn { get; private set; }

		// Token: 0x17000006 RID: 6
		// (get) Token: 0x0600000B RID: 11 RVA: 0x00002196 File Offset: 0x00000396
		// (set) Token: 0x0600000C RID: 12 RVA: 0x0000219E File Offset: 0x0000039E
		public string AuthRoleArn { get; private set; }

		// Token: 0x17000007 RID: 7
		// (get) Token: 0x0600000D RID: 13 RVA: 0x000021A7 File Offset: 0x000003A7
		// (set) Token: 0x0600000E RID: 14 RVA: 0x000021AF File Offset: 0x000003AF
		private Dictionary<string, string> Logins { get; set; }

		// Token: 0x0600000F RID: 15 RVA: 0x000021B8 File Offset: 0x000003B8
		public void Clear()
		{
			this.identityId = null;
			this.ClearCredentials();
			this.ClearIdentityCache();
			this.Logins.Clear();
		}

		// Token: 0x17000008 RID: 8
		// (get) Token: 0x06000010 RID: 16 RVA: 0x000021D8 File Offset: 0x000003D8
		public string[] CurrentLoginProviders
		{
			get
			{
				return this.Logins.Keys.ToArray<string>();
			}
		}

		// Token: 0x06000011 RID: 17 RVA: 0x000021EA File Offset: 0x000003EA
		public bool ContainsProvider(string providerName)
		{
			return this.Logins.ContainsKey(providerName);
		}

		// Token: 0x06000012 RID: 18 RVA: 0x000021F8 File Offset: 0x000003F8
		public void RemoveLogin(string providerName)
		{
			this.Logins.Remove(providerName);
			this.ClearCredentials();
		}

		// Token: 0x06000013 RID: 19 RVA: 0x0000220D File Offset: 0x0000040D
		public void AddLogin(string providerName, string token)
		{
			this.Logins[providerName] = token;
			this.ClearCredentials();
		}

		// Token: 0x17000009 RID: 9
		// (get) Token: 0x06000014 RID: 20 RVA: 0x00002222 File Offset: 0x00000422
		public int LoginsCount
		{
			get
			{
				return this.Logins.Count;
			}
		}

		// Token: 0x06000015 RID: 21 RVA: 0x0000222F File Offset: 0x0000042F
		public string GetIdentityId()
		{
			return this.GetIdentityId(CognitoAWSCredentials.RefreshIdentityOptions.None);
		}

		// Token: 0x06000016 RID: 22 RVA: 0x00002238 File Offset: 0x00000438
		private string GetIdentityId(CognitoAWSCredentials.RefreshIdentityOptions options)
		{
			object obj = CognitoAWSCredentials.refreshIdLock;
			lock (obj)
			{
				if (!this.IsIdentitySet || options == CognitoAWSCredentials.RefreshIdentityOptions.Refresh)
				{
					this._identityState = this.RefreshIdentity();
					if (!string.IsNullOrEmpty(this._identityState.LoginProvider))
					{
						this.Logins[this._identityState.LoginProvider] = this._identityState.LoginToken;
					}
					this.UpdateIdentity(this._identityState.IdentityId);
				}
			}
			return this.identityId;
		}

		// Token: 0x06000017 RID: 23 RVA: 0x000022CC File Offset: 0x000004CC
		protected virtual CognitoAWSCredentials.IdentityState RefreshIdentity()
		{
			bool fromCache = true;
			if (!this.IsIdentitySet)
			{
				GetIdRequest request = new GetIdRequest
				{
					AccountId = this.AccountId,
					IdentityPoolId = this.IdentityPoolId,
					Logins = this.Logins
				};
				GetIdResponse id = this.cib.GetId(request);
				fromCache = false;
				this.UpdateIdentity(id.IdentityId);
			}
			return new CognitoAWSCredentials.IdentityState(this.identityId, fromCache);
		}

		// Token: 0x06000018 RID: 24 RVA: 0x00002334 File Offset: 0x00000534
		private bool ShouldRetry(AmazonCognitoIdentityException e)
		{
			if (this._identityState.LoginSpecified && ((e is NotAuthorizedException && e.Message.StartsWith("Access to Identity", StringComparison.OrdinalIgnoreCase)) || e is ResourceNotFoundException))
			{
				this.identityId = null;
				this.ClearIdentityCache();
				return true;
			}
			return false;
		}

		// Token: 0x14000001 RID: 1
		// (add) Token: 0x06000019 RID: 25 RVA: 0x00002384 File Offset: 0x00000584
		// (remove) Token: 0x0600001A RID: 26 RVA: 0x000023CC File Offset: 0x000005CC
		public event EventHandler<CognitoAWSCredentials.IdentityChangedArgs> IdentityChangedEvent
		{
			add
			{
				lock (this)
				{
					this.mIdentityChangedEvent = (EventHandler<CognitoAWSCredentials.IdentityChangedArgs>)Delegate.Combine(this.mIdentityChangedEvent, value);
				}
			}
			remove
			{
				lock (this)
				{
					this.mIdentityChangedEvent = (EventHandler<CognitoAWSCredentials.IdentityChangedArgs>)Delegate.Remove(this.mIdentityChangedEvent, value);
				}
			}
		}

		// Token: 0x0600001B RID: 27 RVA: 0x00002414 File Offset: 0x00000614
		public CognitoAWSCredentials(string identityPoolId, RegionEndpoint region) : this(null, identityPoolId, null, null, region)
		{
		}

		// Token: 0x0600001C RID: 28 RVA: 0x00002421 File Offset: 0x00000621
		public CognitoAWSCredentials(string accountId, string identityPoolId, string unAuthRoleArn, string authRoleArn, RegionEndpoint region) : this(accountId, identityPoolId, unAuthRoleArn, authRoleArn, new AmazonCognitoIdentityClient(new AnonymousAWSCredentials(), region), new AmazonSecurityTokenServiceClient(new AnonymousAWSCredentials(), region))
		{
		}

		// Token: 0x0600001D RID: 29 RVA: 0x00002448 File Offset: 0x00000648
		public CognitoAWSCredentials(string accountId, string identityPoolId, string unAuthRoleArn, string authRoleArn, IAmazonCognitoIdentity cibClient, IAmazonSecurityTokenService stsClient)
		{
			if (string.IsNullOrEmpty(identityPoolId))
			{
				throw new ArgumentNullException("identityPoolId");
			}
			if (cibClient == null)
			{
				throw new ArgumentNullException("cibClient");
			}
			if ((unAuthRoleArn != null || authRoleArn != null) && stsClient == null)
			{
				throw new ArgumentNullException("stsClient");
			}
			this.AccountId = accountId;
			this.IdentityPoolId = identityPoolId;
			this.UnAuthRoleArn = unAuthRoleArn;
			this.AuthRoleArn = authRoleArn;
			this.Logins = new Dictionary<string, string>(StringComparer.Ordinal);
			this.cib = (AmazonCognitoIdentityClient)cibClient;
			this.sts = (AmazonSecurityTokenServiceClient)stsClient;
			string cachedIdentityId = this.GetCachedIdentityId();
			if (!string.IsNullOrEmpty(cachedIdentityId))
			{
				this.UpdateIdentity(cachedIdentityId);
				this.currentState = this.GetCachedCredentials();
			}
		}

		// Token: 0x0600001E RID: 30 RVA: 0x000024FC File Offset: 0x000006FC
		protected override RefreshingAWSCredentials.CredentialsRefreshState GenerateNewCredentials()
		{
			string text = this.UnAuthRoleArn;
			if (this.Logins.Count > 0)
			{
				text = this.AuthRoleArn;
			}
			RefreshingAWSCredentials.CredentialsRefreshState credentialsRefreshState;
			if (!string.IsNullOrEmpty(text))
			{
				credentialsRefreshState = this.GetCredentialsForRole(text);
			}
			else
			{
				credentialsRefreshState = this.GetPoolCredentials();
			}
			this.CacheCredentials(credentialsRefreshState);
			return credentialsRefreshState;
		}

		// Token: 0x0600001F RID: 31 RVA: 0x0000254C File Offset: 0x0000074C
		private RefreshingAWSCredentials.CredentialsRefreshState GetPoolCredentials()
		{
			string text = this.GetIdentityId(CognitoAWSCredentials.RefreshIdentityOptions.Refresh);
			GetCredentialsForIdentityRequest getCredentialsForIdentityRequest = new GetCredentialsForIdentityRequest
			{
				IdentityId = text
			};
			if (this.Logins.Count > 0)
			{
				getCredentialsForIdentityRequest.Logins = this.Logins;
			}
			if (this._identityState != null && !string.IsNullOrEmpty(this._identityState.LoginToken))
			{
				getCredentialsForIdentityRequest.Logins = new Dictionary<string, string>();
				getCredentialsForIdentityRequest.Logins["cognito-identity.amazonaws.com"] = this._identityState.LoginToken;
			}
			bool flag = false;
			GetCredentialsForIdentityResponse getCredentialsForIdentityResponse = null;
			try
			{
				getCredentialsForIdentityResponse = this.GetCredentialsForIdentity(getCredentialsForIdentityRequest);
			}
			catch (AmazonCognitoIdentityException e)
			{
				if (!this.ShouldRetry(e))
				{
					throw;
				}
				flag = true;
			}
			if (flag)
			{
				return this.GetPoolCredentials();
			}
			this.UpdateIdentity(getCredentialsForIdentityResponse.IdentityId);
			Amazon.CognitoIdentity.Model.Credentials credentials = getCredentialsForIdentityResponse.Credentials;
			return new RefreshingAWSCredentials.CredentialsRefreshState(credentials.GetCredentials(), credentials.Expiration);
		}

		// Token: 0x06000020 RID: 32 RVA: 0x0000262C File Offset: 0x0000082C
		private RefreshingAWSCredentials.CredentialsRefreshState GetCredentialsForRole(string roleArn)
		{
			string text = this.GetIdentityId(CognitoAWSCredentials.RefreshIdentityOptions.Refresh);
			GetOpenIdTokenRequest getOpenIdTokenRequest = new GetOpenIdTokenRequest
			{
				IdentityId = text
			};
			if (this.Logins.Count > 0)
			{
				getOpenIdTokenRequest.Logins = this.Logins;
			}
			bool flag = false;
			GetOpenIdTokenResponse getOpenIdTokenResponse = null;
			try
			{
				getOpenIdTokenResponse = this.GetOpenId(getOpenIdTokenRequest);
			}
			catch (AmazonCognitoIdentityException e)
			{
				if (!this.ShouldRetry(e))
				{
					throw;
				}
				flag = true;
			}
			if (flag)
			{
				return this.GetCredentialsForRole(roleArn);
			}
			string token = getOpenIdTokenResponse.Token;
			this.UpdateIdentity(getOpenIdTokenResponse.IdentityId);
			AssumeRoleWithWebIdentityRequest assumeRequest = new AssumeRoleWithWebIdentityRequest
			{
				WebIdentityToken = token,
				RoleArn = roleArn,
				RoleSessionName = "NetProviderSession",
				DurationSeconds = CognitoAWSCredentials.DefaultDurationSeconds
			};
			Amazon.SecurityToken.Model.Credentials stsCredentials = this.GetStsCredentials(assumeRequest);
			return new RefreshingAWSCredentials.CredentialsRefreshState(stsCredentials.GetCredentials(), stsCredentials.Expiration);
		}

		// Token: 0x06000021 RID: 33 RVA: 0x00002704 File Offset: 0x00000904
		private Amazon.SecurityToken.Model.Credentials GetStsCredentials(AssumeRoleWithWebIdentityRequest assumeRequest)
		{
			AutoResetEvent ars = new AutoResetEvent(false);
			Amazon.SecurityToken.Model.Credentials credentials = null;
			Exception exception = null;
			this.sts.AssumeRoleWithWebIdentityAsync(assumeRequest, delegate(AmazonServiceResult<AssumeRoleWithWebIdentityRequest, AssumeRoleWithWebIdentityResponse> assumeResult)
			{
				if (assumeResult.Exception != null)
				{
					exception = assumeResult.Exception;
				}
				else
				{
					credentials = assumeResult.Response.Credentials;
				}
				ars.Set();
			}, null);
			ars.WaitOne();
			if (exception != null)
			{
				throw exception;
			}
			return credentials;
		}

		// Token: 0x06000022 RID: 34 RVA: 0x0000276B File Offset: 0x0000096B
		private GetCredentialsForIdentityResponse GetCredentialsForIdentity(GetCredentialsForIdentityRequest getCredentialsRequest)
		{
			return this.cib.GetCredentialsForIdentity(getCredentialsRequest);
		}

		// Token: 0x06000023 RID: 35 RVA: 0x00002779 File Offset: 0x00000979
		private GetOpenIdTokenResponse GetOpenId(GetOpenIdTokenRequest getTokenRequest)
		{
			return this.cib.GetOpenIdToken(getTokenRequest);
		}

		// Token: 0x06000024 RID: 36 RVA: 0x00002787 File Offset: 0x00000987
		public virtual string GetCachedIdentityId()
		{
			return ServiceFactory.Instance.GetService<IApplicationSettings>().GetValue(this.GetNamespacedKey(CognitoAWSCredentials.IDENTITY_ID_CACHE_KEY), ApplicationSettingsMode.Local);
		}

		// Token: 0x06000025 RID: 37 RVA: 0x000027A4 File Offset: 0x000009A4
		public virtual void CacheIdentityId(string identityId)
		{
			ServiceFactory.Instance.GetService<IApplicationSettings>().SetValue(this.GetNamespacedKey(CognitoAWSCredentials.IDENTITY_ID_CACHE_KEY), identityId, ApplicationSettingsMode.Local);
		}

		// Token: 0x06000026 RID: 38 RVA: 0x000027C2 File Offset: 0x000009C2
		public virtual void ClearIdentityCache()
		{
			ServiceFactory.Instance.GetService<IApplicationSettings>().RemoveValue(this.GetNamespacedKey(CognitoAWSCredentials.IDENTITY_ID_CACHE_KEY), ApplicationSettingsMode.Local);
		}

		// Token: 0x06000027 RID: 39 RVA: 0x000027DF File Offset: 0x000009DF
		internal void CacheCredentials(RefreshingAWSCredentials.CredentialsRefreshState credentialsState)
		{
		}

		// Token: 0x06000028 RID: 40 RVA: 0x000027E1 File Offset: 0x000009E1
		internal RefreshingAWSCredentials.CredentialsRefreshState GetCachedCredentials()
		{
			return null;
		}

		// Token: 0x06000029 RID: 41 RVA: 0x000027E4 File Offset: 0x000009E4
		public void GetIdentityIdAsync(AmazonCognitoIdentityCallback<string> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			CognitoIdentityAsyncExecutor.ExecuteAsync<string>(() => this.GetIdentityId(), options, callback);
		}

		// Token: 0x0600002A RID: 42 RVA: 0x00002806 File Offset: 0x00000A06
		public void GetCredentialsAsync(AmazonCognitoIdentityCallback<ImmutableCredentials> callback, AsyncOptions options = null)
		{
			options = ((options == null) ? new AsyncOptions() : options);
			CognitoIdentityAsyncExecutor.ExecuteAsync<ImmutableCredentials>(() => this.GetCredentials(), options, callback);
		}

		// Token: 0x04000001 RID: 1
		private static object refreshIdLock = new object();

		// Token: 0x04000002 RID: 2
		private string identityId;

		// Token: 0x04000003 RID: 3
		private static int DefaultDurationSeconds = (int)TimeSpan.FromHours(1.0).TotalSeconds;

		// Token: 0x04000004 RID: 4
		private AmazonCognitoIdentityClient cib;

		// Token: 0x04000005 RID: 5
		private AmazonSecurityTokenServiceClient sts;

		// Token: 0x0400000B RID: 11
		private CognitoAWSCredentials.IdentityState _identityState;

		// Token: 0x0400000C RID: 12
		private EventHandler<CognitoAWSCredentials.IdentityChangedArgs> mIdentityChangedEvent;

		// Token: 0x0400000D RID: 13
		private static readonly string IDENTITY_ID_CACHE_KEY = "CognitoIdentity:IdentityId";

		// Token: 0x0400000E RID: 14
		private static object _lock = new object();

		// Token: 0x02000076 RID: 118
		[Flags]
		private enum RefreshIdentityOptions
		{
			// Token: 0x040000BF RID: 191
			None = 0,
			// Token: 0x040000C0 RID: 192
			Refresh = 1
		}

		// Token: 0x02000077 RID: 119
		public class IdentityChangedArgs : EventArgs
		{
			// Token: 0x170000AA RID: 170
			// (get) Token: 0x06000370 RID: 880 RVA: 0x000099BB File Offset: 0x00007BBB
			// (set) Token: 0x06000371 RID: 881 RVA: 0x000099C3 File Offset: 0x00007BC3
			public string OldIdentityId { get; private set; }

			// Token: 0x170000AB RID: 171
			// (get) Token: 0x06000372 RID: 882 RVA: 0x000099CC File Offset: 0x00007BCC
			// (set) Token: 0x06000373 RID: 883 RVA: 0x000099D4 File Offset: 0x00007BD4
			public string NewIdentityId { get; private set; }

			// Token: 0x06000374 RID: 884 RVA: 0x000099DD File Offset: 0x00007BDD
			internal IdentityChangedArgs(string oldIdentityId, string newIdentityId)
			{
				this.OldIdentityId = oldIdentityId;
				this.NewIdentityId = newIdentityId;
			}
		}

		// Token: 0x02000078 RID: 120
		public class IdentityState
		{
			// Token: 0x170000AC RID: 172
			// (get) Token: 0x06000375 RID: 885 RVA: 0x000099F3 File Offset: 0x00007BF3
			// (set) Token: 0x06000376 RID: 886 RVA: 0x000099FB File Offset: 0x00007BFB
			public string IdentityId { get; private set; }

			// Token: 0x170000AD RID: 173
			// (get) Token: 0x06000377 RID: 887 RVA: 0x00009A04 File Offset: 0x00007C04
			// (set) Token: 0x06000378 RID: 888 RVA: 0x00009A0C File Offset: 0x00007C0C
			public string LoginProvider { get; private set; }

			// Token: 0x170000AE RID: 174
			// (get) Token: 0x06000379 RID: 889 RVA: 0x00009A15 File Offset: 0x00007C15
			// (set) Token: 0x0600037A RID: 890 RVA: 0x00009A1D File Offset: 0x00007C1D
			public string LoginToken { get; private set; }

			// Token: 0x170000AF RID: 175
			// (get) Token: 0x0600037B RID: 891 RVA: 0x00009A26 File Offset: 0x00007C26
			// (set) Token: 0x0600037C RID: 892 RVA: 0x00009A2E File Offset: 0x00007C2E
			public bool FromCache { get; private set; }

			// Token: 0x0600037D RID: 893 RVA: 0x00009A37 File Offset: 0x00007C37
			public IdentityState(string identityId, string provider, string token, bool fromCache)
			{
				this.IdentityId = identityId;
				this.LoginProvider = provider;
				this.LoginToken = token;
				this.FromCache = fromCache;
			}

			// Token: 0x0600037E RID: 894 RVA: 0x00009A5C File Offset: 0x00007C5C
			public IdentityState(string identityId, bool fromCache)
			{
				this.IdentityId = identityId;
				this.FromCache = fromCache;
			}

			// Token: 0x170000B0 RID: 176
			// (get) Token: 0x0600037F RID: 895 RVA: 0x00009A72 File Offset: 0x00007C72
			public bool LoginSpecified
			{
				get
				{
					return !string.IsNullOrEmpty(this.LoginProvider) && string.IsNullOrEmpty(this.LoginToken);
				}
			}
		}
	}
}
