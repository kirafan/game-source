﻿using System;

namespace Amazon.CognitoIdentity
{
	// Token: 0x02000003 RID: 3
	// (Invoke) Token: 0x0600002F RID: 47
	public delegate void AmazonCognitoIdentityCallback<TResponse>(AmazonCognitoIdentityResult<TResponse> result);
}
