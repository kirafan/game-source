﻿using System;

namespace System
{
	// Token: 0x0200008B RID: 139
	// (Invoke) Token: 0x0600062B RID: 1579
	public delegate TResult Func<TResult>();
}
