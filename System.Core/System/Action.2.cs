﻿using System;

namespace System
{
	// Token: 0x02000088 RID: 136
	// (Invoke) Token: 0x0600061F RID: 1567
	public delegate void Action<T1, T2>(T1 arg1, T2 arg2);
}
