﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000054 RID: 84
	public abstract class Aes : SymmetricAlgorithm
	{
		// Token: 0x060004B9 RID: 1209 RVA: 0x00016254 File Offset: 0x00014454
		protected Aes()
		{
			this.KeySizeValue = 256;
			this.BlockSizeValue = 128;
			this.FeedbackSizeValue = 128;
			this.LegalKeySizesValue = new KeySizes[1];
			this.LegalKeySizesValue[0] = new KeySizes(128, 256, 64);
			this.LegalBlockSizesValue = new KeySizes[1];
			this.LegalBlockSizesValue[0] = new KeySizes(128, 128, 0);
		}

		// Token: 0x060004BA RID: 1210 RVA: 0x000162D4 File Offset: 0x000144D4
		public new static Aes Create()
		{
			return Aes.Create("System.Security.Cryptography.AesManaged, System.Core, Version=3.5.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089");
		}

		// Token: 0x060004BB RID: 1211 RVA: 0x000162E0 File Offset: 0x000144E0
		public new static Aes Create(string algName)
		{
			return (Aes)CryptoConfig.CreateFromName(algName);
		}
	}
}
