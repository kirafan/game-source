﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000058 RID: 88
	[Serializable]
	public sealed class CngAlgorithm : IEquatable<CngAlgorithm>
	{
		// Token: 0x060004DF RID: 1247 RVA: 0x000182A4 File Offset: 0x000164A4
		public CngAlgorithm(string algorithm)
		{
			if (algorithm == null)
			{
				throw new ArgumentNullException("algorithm");
			}
			if (algorithm.Length == 0)
			{
				throw new ArgumentException("algorithm");
			}
			this.algo = algorithm;
		}

		// Token: 0x17000062 RID: 98
		// (get) Token: 0x060004E0 RID: 1248 RVA: 0x000182E8 File Offset: 0x000164E8
		public string Algorithm
		{
			get
			{
				return this.algo;
			}
		}

		// Token: 0x060004E1 RID: 1249 RVA: 0x000182F0 File Offset: 0x000164F0
		public bool Equals(CngAlgorithm other)
		{
			return !(other == null) && this.algo == other.algo;
		}

		// Token: 0x060004E2 RID: 1250 RVA: 0x00018314 File Offset: 0x00016514
		public override bool Equals(object obj)
		{
			return this.Equals(obj as CngAlgorithm);
		}

		// Token: 0x060004E3 RID: 1251 RVA: 0x00018324 File Offset: 0x00016524
		public override int GetHashCode()
		{
			return this.algo.GetHashCode();
		}

		// Token: 0x060004E4 RID: 1252 RVA: 0x00018334 File Offset: 0x00016534
		public override string ToString()
		{
			return this.algo;
		}

		// Token: 0x17000063 RID: 99
		// (get) Token: 0x060004E5 RID: 1253 RVA: 0x0001833C File Offset: 0x0001653C
		public static CngAlgorithm ECDiffieHellmanP256
		{
			get
			{
				if (CngAlgorithm.dh256 == null)
				{
					CngAlgorithm.dh256 = new CngAlgorithm("ECDH_P256");
				}
				return CngAlgorithm.dh256;
			}
		}

		// Token: 0x17000064 RID: 100
		// (get) Token: 0x060004E6 RID: 1254 RVA: 0x00018370 File Offset: 0x00016570
		public static CngAlgorithm ECDiffieHellmanP384
		{
			get
			{
				if (CngAlgorithm.dh384 == null)
				{
					CngAlgorithm.dh384 = new CngAlgorithm("ECDH_P384");
				}
				return CngAlgorithm.dh384;
			}
		}

		// Token: 0x17000065 RID: 101
		// (get) Token: 0x060004E7 RID: 1255 RVA: 0x000183A4 File Offset: 0x000165A4
		public static CngAlgorithm ECDiffieHellmanP521
		{
			get
			{
				if (CngAlgorithm.dh521 == null)
				{
					CngAlgorithm.dh521 = new CngAlgorithm("ECDH_P521");
				}
				return CngAlgorithm.dh521;
			}
		}

		// Token: 0x17000066 RID: 102
		// (get) Token: 0x060004E8 RID: 1256 RVA: 0x000183D8 File Offset: 0x000165D8
		public static CngAlgorithm ECDsaP256
		{
			get
			{
				if (CngAlgorithm.dsa256 == null)
				{
					CngAlgorithm.dsa256 = new CngAlgorithm("ECDSA_P256");
				}
				return CngAlgorithm.dsa256;
			}
		}

		// Token: 0x17000067 RID: 103
		// (get) Token: 0x060004E9 RID: 1257 RVA: 0x0001840C File Offset: 0x0001660C
		public static CngAlgorithm ECDsaP384
		{
			get
			{
				if (CngAlgorithm.dsa384 == null)
				{
					CngAlgorithm.dsa384 = new CngAlgorithm("ECDSA_P384");
				}
				return CngAlgorithm.dsa384;
			}
		}

		// Token: 0x17000068 RID: 104
		// (get) Token: 0x060004EA RID: 1258 RVA: 0x00018440 File Offset: 0x00016640
		public static CngAlgorithm ECDsaP521
		{
			get
			{
				if (CngAlgorithm.dsa521 == null)
				{
					CngAlgorithm.dsa521 = new CngAlgorithm("ECDSA_P521");
				}
				return CngAlgorithm.dsa521;
			}
		}

		// Token: 0x17000069 RID: 105
		// (get) Token: 0x060004EB RID: 1259 RVA: 0x00018474 File Offset: 0x00016674
		public static CngAlgorithm MD5
		{
			get
			{
				if (CngAlgorithm.md5 == null)
				{
					CngAlgorithm.md5 = new CngAlgorithm("MD5");
				}
				return CngAlgorithm.md5;
			}
		}

		// Token: 0x1700006A RID: 106
		// (get) Token: 0x060004EC RID: 1260 RVA: 0x000184A8 File Offset: 0x000166A8
		public static CngAlgorithm Sha1
		{
			get
			{
				if (CngAlgorithm.sha1 == null)
				{
					CngAlgorithm.sha1 = new CngAlgorithm("SHA1");
				}
				return CngAlgorithm.sha1;
			}
		}

		// Token: 0x1700006B RID: 107
		// (get) Token: 0x060004ED RID: 1261 RVA: 0x000184DC File Offset: 0x000166DC
		public static CngAlgorithm Sha256
		{
			get
			{
				if (CngAlgorithm.sha256 == null)
				{
					CngAlgorithm.sha256 = new CngAlgorithm("SHA256");
				}
				return CngAlgorithm.sha256;
			}
		}

		// Token: 0x1700006C RID: 108
		// (get) Token: 0x060004EE RID: 1262 RVA: 0x00018510 File Offset: 0x00016710
		public static CngAlgorithm Sha384
		{
			get
			{
				if (CngAlgorithm.sha384 == null)
				{
					CngAlgorithm.sha384 = new CngAlgorithm("SHA384");
				}
				return CngAlgorithm.sha384;
			}
		}

		// Token: 0x1700006D RID: 109
		// (get) Token: 0x060004EF RID: 1263 RVA: 0x00018544 File Offset: 0x00016744
		public static CngAlgorithm Sha512
		{
			get
			{
				if (CngAlgorithm.sha512 == null)
				{
					CngAlgorithm.sha512 = new CngAlgorithm("SHA512");
				}
				return CngAlgorithm.sha512;
			}
		}

		// Token: 0x060004F0 RID: 1264 RVA: 0x00018578 File Offset: 0x00016778
		public static bool operator ==(CngAlgorithm left, CngAlgorithm right)
		{
			if (left == null)
			{
				return right == null;
			}
			return right != null && left.algo == right.algo;
		}

		// Token: 0x060004F1 RID: 1265 RVA: 0x000185AC File Offset: 0x000167AC
		public static bool operator !=(CngAlgorithm left, CngAlgorithm right)
		{
			if (left == null)
			{
				return right != null;
			}
			return right == null || left.algo != right.algo;
		}

		// Token: 0x0400012C RID: 300
		private string algo;

		// Token: 0x0400012D RID: 301
		private static CngAlgorithm dh256;

		// Token: 0x0400012E RID: 302
		private static CngAlgorithm dh384;

		// Token: 0x0400012F RID: 303
		private static CngAlgorithm dh521;

		// Token: 0x04000130 RID: 304
		private static CngAlgorithm dsa256;

		// Token: 0x04000131 RID: 305
		private static CngAlgorithm dsa384;

		// Token: 0x04000132 RID: 306
		private static CngAlgorithm dsa521;

		// Token: 0x04000133 RID: 307
		private static CngAlgorithm md5;

		// Token: 0x04000134 RID: 308
		private static CngAlgorithm sha1;

		// Token: 0x04000135 RID: 309
		private static CngAlgorithm sha256;

		// Token: 0x04000136 RID: 310
		private static CngAlgorithm sha384;

		// Token: 0x04000137 RID: 311
		private static CngAlgorithm sha512;
	}
}
