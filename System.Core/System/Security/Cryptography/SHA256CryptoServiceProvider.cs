﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200005D RID: 93
	public sealed class SHA256CryptoServiceProvider : SHA256
	{
		// Token: 0x06000511 RID: 1297 RVA: 0x00018978 File Offset: 0x00016B78
		[SecurityCritical]
		public SHA256CryptoServiceProvider()
		{
			this.hash = new SHA256Managed();
		}

		// Token: 0x06000513 RID: 1299 RVA: 0x0001899C File Offset: 0x00016B9C
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x06000514 RID: 1300 RVA: 0x000189AC File Offset: 0x00016BAC
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x06000515 RID: 1301 RVA: 0x000189C0 File Offset: 0x00016BC0
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA256CryptoServiceProvider.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x06000516 RID: 1302 RVA: 0x000189F8 File Offset: 0x00016BF8
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x04000144 RID: 324
		private static byte[] Empty = new byte[0];

		// Token: 0x04000145 RID: 325
		private SHA256 hash;
	}
}
