﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000060 RID: 96
	public sealed class SHA512Cng : SHA512
	{
		// Token: 0x06000523 RID: 1315 RVA: 0x00018B34 File Offset: 0x00016D34
		[SecurityCritical]
		public SHA512Cng()
		{
			this.hash = new SHA512Managed();
		}

		// Token: 0x06000525 RID: 1317 RVA: 0x00018B58 File Offset: 0x00016D58
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x06000526 RID: 1318 RVA: 0x00018B68 File Offset: 0x00016D68
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x06000527 RID: 1319 RVA: 0x00018B7C File Offset: 0x00016D7C
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA512Cng.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x06000528 RID: 1320 RVA: 0x00018BB4 File Offset: 0x00016DB4
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x0400014A RID: 330
		private static byte[] Empty = new byte[0];

		// Token: 0x0400014B RID: 331
		private SHA512 hash;
	}
}
