﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200005C RID: 92
	public sealed class SHA256Cng : SHA256
	{
		// Token: 0x0600050B RID: 1291 RVA: 0x000188E4 File Offset: 0x00016AE4
		[SecurityCritical]
		public SHA256Cng()
		{
			this.hash = new SHA256Managed();
		}

		// Token: 0x0600050D RID: 1293 RVA: 0x00018908 File Offset: 0x00016B08
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x0600050E RID: 1294 RVA: 0x00018918 File Offset: 0x00016B18
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x0600050F RID: 1295 RVA: 0x0001892C File Offset: 0x00016B2C
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA256Cng.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x06000510 RID: 1296 RVA: 0x00018964 File Offset: 0x00016B64
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x04000142 RID: 322
		private static byte[] Empty = new byte[0];

		// Token: 0x04000143 RID: 323
		private SHA256 hash;
	}
}
