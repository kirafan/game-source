﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000061 RID: 97
	public sealed class SHA512CryptoServiceProvider : SHA512
	{
		// Token: 0x06000529 RID: 1321 RVA: 0x00018BC8 File Offset: 0x00016DC8
		[SecurityCritical]
		public SHA512CryptoServiceProvider()
		{
			this.hash = new SHA512Managed();
		}

		// Token: 0x0600052B RID: 1323 RVA: 0x00018BEC File Offset: 0x00016DEC
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x0600052C RID: 1324 RVA: 0x00018BFC File Offset: 0x00016DFC
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x0600052D RID: 1325 RVA: 0x00018C10 File Offset: 0x00016E10
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA512CryptoServiceProvider.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x0600052E RID: 1326 RVA: 0x00018C48 File Offset: 0x00016E48
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x0400014C RID: 332
		private static byte[] Empty = new byte[0];

		// Token: 0x0400014D RID: 333
		private SHA512 hash;
	}
}
