﻿using System;
using System.Security.Permissions;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x02000056 RID: 86
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	public sealed class AesCryptoServiceProvider : Aes
	{
		// Token: 0x060004CB RID: 1227 RVA: 0x000163B8 File Offset: 0x000145B8
		public override void GenerateIV()
		{
			this.IVValue = KeyBuilder.IV(this.BlockSizeValue >> 3);
		}

		// Token: 0x060004CC RID: 1228 RVA: 0x000163D0 File Offset: 0x000145D0
		public override void GenerateKey()
		{
			this.KeyValue = KeyBuilder.Key(this.KeySizeValue >> 3);
		}

		// Token: 0x060004CD RID: 1229 RVA: 0x000163E8 File Offset: 0x000145E8
		public override ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new AesTransform(this, false, rgbKey, rgbIV);
		}

		// Token: 0x060004CE RID: 1230 RVA: 0x000163F4 File Offset: 0x000145F4
		public override ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new AesTransform(this, true, rgbKey, rgbIV);
		}

		// Token: 0x1700005F RID: 95
		// (get) Token: 0x060004CF RID: 1231 RVA: 0x00016400 File Offset: 0x00014600
		// (set) Token: 0x060004D0 RID: 1232 RVA: 0x00016408 File Offset: 0x00014608
		public override byte[] IV
		{
			get
			{
				return base.IV;
			}
			set
			{
				base.IV = value;
			}
		}

		// Token: 0x17000060 RID: 96
		// (get) Token: 0x060004D1 RID: 1233 RVA: 0x00016414 File Offset: 0x00014614
		// (set) Token: 0x060004D2 RID: 1234 RVA: 0x0001641C File Offset: 0x0001461C
		public override byte[] Key
		{
			get
			{
				return base.Key;
			}
			set
			{
				base.Key = value;
			}
		}

		// Token: 0x17000061 RID: 97
		// (get) Token: 0x060004D3 RID: 1235 RVA: 0x00016428 File Offset: 0x00014628
		// (set) Token: 0x060004D4 RID: 1236 RVA: 0x00016430 File Offset: 0x00014630
		public override int KeySize
		{
			get
			{
				return base.KeySize;
			}
			set
			{
				base.KeySize = value;
			}
		}

		// Token: 0x060004D5 RID: 1237 RVA: 0x0001643C File Offset: 0x0001463C
		public override ICryptoTransform CreateDecryptor()
		{
			return this.CreateDecryptor(this.Key, this.IV);
		}

		// Token: 0x060004D6 RID: 1238 RVA: 0x00016450 File Offset: 0x00014650
		public override ICryptoTransform CreateEncryptor()
		{
			return this.CreateEncryptor(this.Key, this.IV);
		}

		// Token: 0x060004D7 RID: 1239 RVA: 0x00016464 File Offset: 0x00014664
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}
	}
}
