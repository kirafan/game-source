﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200005B RID: 91
	public sealed class SHA1Cng : SHA1
	{
		// Token: 0x06000505 RID: 1285 RVA: 0x00018850 File Offset: 0x00016A50
		[SecurityCritical]
		public SHA1Cng()
		{
			this.hash = new SHA1Managed();
		}

		// Token: 0x06000507 RID: 1287 RVA: 0x00018874 File Offset: 0x00016A74
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x06000508 RID: 1288 RVA: 0x00018884 File Offset: 0x00016A84
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x06000509 RID: 1289 RVA: 0x00018898 File Offset: 0x00016A98
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA1Cng.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x0600050A RID: 1290 RVA: 0x000188D0 File Offset: 0x00016AD0
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x04000140 RID: 320
		private static byte[] Empty = new byte[0];

		// Token: 0x04000141 RID: 321
		private SHA1 hash;
	}
}
