﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200005E RID: 94
	public sealed class SHA384Cng : SHA384
	{
		// Token: 0x06000517 RID: 1303 RVA: 0x00018A0C File Offset: 0x00016C0C
		[SecurityCritical]
		public SHA384Cng()
		{
			this.hash = new SHA384Managed();
		}

		// Token: 0x06000519 RID: 1305 RVA: 0x00018A30 File Offset: 0x00016C30
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x0600051A RID: 1306 RVA: 0x00018A40 File Offset: 0x00016C40
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x0600051B RID: 1307 RVA: 0x00018A54 File Offset: 0x00016C54
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA384Cng.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x0600051C RID: 1308 RVA: 0x00018A8C File Offset: 0x00016C8C
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x04000146 RID: 326
		private static byte[] Empty = new byte[0];

		// Token: 0x04000147 RID: 327
		private SHA384 hash;
	}
}
