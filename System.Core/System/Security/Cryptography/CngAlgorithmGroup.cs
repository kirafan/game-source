﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x02000059 RID: 89
	[Serializable]
	public sealed class CngAlgorithmGroup : IEquatable<CngAlgorithmGroup>
	{
		// Token: 0x060004F2 RID: 1266 RVA: 0x000185D8 File Offset: 0x000167D8
		public CngAlgorithmGroup(string algorithmGroup)
		{
			if (algorithmGroup == null)
			{
				throw new ArgumentNullException("algorithmGroup");
			}
			if (algorithmGroup.Length == 0)
			{
				throw new ArgumentException("algorithmGroup");
			}
			this.group = algorithmGroup;
		}

		// Token: 0x1700006E RID: 110
		// (get) Token: 0x060004F3 RID: 1267 RVA: 0x0001861C File Offset: 0x0001681C
		public string AlgorithmGroup
		{
			get
			{
				return this.group;
			}
		}

		// Token: 0x060004F4 RID: 1268 RVA: 0x00018624 File Offset: 0x00016824
		public bool Equals(CngAlgorithmGroup other)
		{
			return this == other;
		}

		// Token: 0x060004F5 RID: 1269 RVA: 0x00018630 File Offset: 0x00016830
		public override bool Equals(object obj)
		{
			return this.Equals(obj as CngAlgorithmGroup);
		}

		// Token: 0x060004F6 RID: 1270 RVA: 0x00018640 File Offset: 0x00016840
		public override int GetHashCode()
		{
			return this.group.GetHashCode();
		}

		// Token: 0x060004F7 RID: 1271 RVA: 0x00018650 File Offset: 0x00016850
		public override string ToString()
		{
			return this.group;
		}

		// Token: 0x1700006F RID: 111
		// (get) Token: 0x060004F8 RID: 1272 RVA: 0x00018658 File Offset: 0x00016858
		public static CngAlgorithmGroup DiffieHellman
		{
			get
			{
				if (CngAlgorithmGroup.dh == null)
				{
					CngAlgorithmGroup.dh = new CngAlgorithmGroup("DH");
				}
				return CngAlgorithmGroup.dh;
			}
		}

		// Token: 0x17000070 RID: 112
		// (get) Token: 0x060004F9 RID: 1273 RVA: 0x0001868C File Offset: 0x0001688C
		public static CngAlgorithmGroup Dsa
		{
			get
			{
				if (CngAlgorithmGroup.dsa == null)
				{
					CngAlgorithmGroup.dsa = new CngAlgorithmGroup("DSA");
				}
				return CngAlgorithmGroup.dsa;
			}
		}

		// Token: 0x17000071 RID: 113
		// (get) Token: 0x060004FA RID: 1274 RVA: 0x000186C0 File Offset: 0x000168C0
		public static CngAlgorithmGroup ECDiffieHellman
		{
			get
			{
				if (CngAlgorithmGroup.ecdh == null)
				{
					CngAlgorithmGroup.ecdh = new CngAlgorithmGroup("ECDH");
				}
				return CngAlgorithmGroup.ecdh;
			}
		}

		// Token: 0x17000072 RID: 114
		// (get) Token: 0x060004FB RID: 1275 RVA: 0x000186F4 File Offset: 0x000168F4
		public static CngAlgorithmGroup ECDsa
		{
			get
			{
				if (CngAlgorithmGroup.ecdsa == null)
				{
					CngAlgorithmGroup.ecdsa = new CngAlgorithmGroup("ECDSA");
				}
				return CngAlgorithmGroup.ecdsa;
			}
		}

		// Token: 0x17000073 RID: 115
		// (get) Token: 0x060004FC RID: 1276 RVA: 0x00018728 File Offset: 0x00016928
		public static CngAlgorithmGroup Rsa
		{
			get
			{
				if (CngAlgorithmGroup.rsa == null)
				{
					CngAlgorithmGroup.rsa = new CngAlgorithmGroup("RSA");
				}
				return CngAlgorithmGroup.rsa;
			}
		}

		// Token: 0x060004FD RID: 1277 RVA: 0x0001875C File Offset: 0x0001695C
		public static bool operator ==(CngAlgorithmGroup left, CngAlgorithmGroup right)
		{
			if (left == null)
			{
				return right == null;
			}
			return right != null && left.group == right.group;
		}

		// Token: 0x060004FE RID: 1278 RVA: 0x00018790 File Offset: 0x00016990
		public static bool operator !=(CngAlgorithmGroup left, CngAlgorithmGroup right)
		{
			if (left == null)
			{
				return right != null;
			}
			return right == null || left.group != right.group;
		}

		// Token: 0x04000138 RID: 312
		private string group;

		// Token: 0x04000139 RID: 313
		private static CngAlgorithmGroup dh;

		// Token: 0x0400013A RID: 314
		private static CngAlgorithmGroup dsa;

		// Token: 0x0400013B RID: 315
		private static CngAlgorithmGroup ecdh;

		// Token: 0x0400013C RID: 316
		private static CngAlgorithmGroup ecdsa;

		// Token: 0x0400013D RID: 317
		private static CngAlgorithmGroup rsa;
	}
}
