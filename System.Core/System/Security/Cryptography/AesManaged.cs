﻿using System;
using System.Security.Permissions;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x02000055 RID: 85
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	public sealed class AesManaged : Aes
	{
		// Token: 0x060004BD RID: 1213 RVA: 0x000162F8 File Offset: 0x000144F8
		public override void GenerateIV()
		{
			this.IVValue = KeyBuilder.IV(this.BlockSizeValue >> 3);
		}

		// Token: 0x060004BE RID: 1214 RVA: 0x00016310 File Offset: 0x00014510
		public override void GenerateKey()
		{
			this.KeyValue = KeyBuilder.Key(this.KeySizeValue >> 3);
		}

		// Token: 0x060004BF RID: 1215 RVA: 0x00016328 File Offset: 0x00014528
		public override ICryptoTransform CreateDecryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new AesTransform(this, false, rgbKey, rgbIV);
		}

		// Token: 0x060004C0 RID: 1216 RVA: 0x00016334 File Offset: 0x00014534
		public override ICryptoTransform CreateEncryptor(byte[] rgbKey, byte[] rgbIV)
		{
			return new AesTransform(this, true, rgbKey, rgbIV);
		}

		// Token: 0x1700005C RID: 92
		// (get) Token: 0x060004C1 RID: 1217 RVA: 0x00016340 File Offset: 0x00014540
		// (set) Token: 0x060004C2 RID: 1218 RVA: 0x00016348 File Offset: 0x00014548
		public override byte[] IV
		{
			get
			{
				return base.IV;
			}
			set
			{
				base.IV = value;
			}
		}

		// Token: 0x1700005D RID: 93
		// (get) Token: 0x060004C3 RID: 1219 RVA: 0x00016354 File Offset: 0x00014554
		// (set) Token: 0x060004C4 RID: 1220 RVA: 0x0001635C File Offset: 0x0001455C
		public override byte[] Key
		{
			get
			{
				return base.Key;
			}
			set
			{
				base.Key = value;
			}
		}

		// Token: 0x1700005E RID: 94
		// (get) Token: 0x060004C5 RID: 1221 RVA: 0x00016368 File Offset: 0x00014568
		// (set) Token: 0x060004C6 RID: 1222 RVA: 0x00016370 File Offset: 0x00014570
		public override int KeySize
		{
			get
			{
				return base.KeySize;
			}
			set
			{
				base.KeySize = value;
			}
		}

		// Token: 0x060004C7 RID: 1223 RVA: 0x0001637C File Offset: 0x0001457C
		public override ICryptoTransform CreateDecryptor()
		{
			return this.CreateDecryptor(this.Key, this.IV);
		}

		// Token: 0x060004C8 RID: 1224 RVA: 0x00016390 File Offset: 0x00014590
		public override ICryptoTransform CreateEncryptor()
		{
			return this.CreateEncryptor(this.Key, this.IV);
		}

		// Token: 0x060004C9 RID: 1225 RVA: 0x000163A4 File Offset: 0x000145A4
		protected override void Dispose(bool disposing)
		{
			base.Dispose(disposing);
		}
	}
}
