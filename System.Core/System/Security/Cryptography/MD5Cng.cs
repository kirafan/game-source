﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200005A RID: 90
	public sealed class MD5Cng : MD5
	{
		// Token: 0x060004FF RID: 1279 RVA: 0x000187BC File Offset: 0x000169BC
		[SecurityCritical]
		public MD5Cng()
		{
			this.hash = new MD5CryptoServiceProvider();
		}

		// Token: 0x06000501 RID: 1281 RVA: 0x000187E0 File Offset: 0x000169E0
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x06000502 RID: 1282 RVA: 0x000187F0 File Offset: 0x000169F0
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x06000503 RID: 1283 RVA: 0x00018804 File Offset: 0x00016A04
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(MD5Cng.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x06000504 RID: 1284 RVA: 0x0001883C File Offset: 0x00016A3C
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x0400013E RID: 318
		private static byte[] Empty = new byte[0];

		// Token: 0x0400013F RID: 319
		private MD5 hash;
	}
}
