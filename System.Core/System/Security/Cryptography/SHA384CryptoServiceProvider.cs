﻿using System;

namespace System.Security.Cryptography
{
	// Token: 0x0200005F RID: 95
	public sealed class SHA384CryptoServiceProvider : SHA384
	{
		// Token: 0x0600051D RID: 1309 RVA: 0x00018AA0 File Offset: 0x00016CA0
		[SecurityCritical]
		public SHA384CryptoServiceProvider()
		{
			this.hash = new SHA384Managed();
		}

		// Token: 0x0600051F RID: 1311 RVA: 0x00018AC4 File Offset: 0x00016CC4
		[SecurityCritical]
		public override void Initialize()
		{
			this.hash.Initialize();
		}

		// Token: 0x06000520 RID: 1312 RVA: 0x00018AD4 File Offset: 0x00016CD4
		[SecurityCritical]
		protected override void HashCore(byte[] array, int ibStart, int cbSize)
		{
			this.hash.TransformBlock(array, ibStart, cbSize, null, 0);
		}

		// Token: 0x06000521 RID: 1313 RVA: 0x00018AE8 File Offset: 0x00016CE8
		[SecurityCritical]
		protected override byte[] HashFinal()
		{
			this.hash.TransformFinalBlock(SHA384CryptoServiceProvider.Empty, 0, 0);
			this.HashValue = this.hash.Hash;
			return this.HashValue;
		}

		// Token: 0x06000522 RID: 1314 RVA: 0x00018B20 File Offset: 0x00016D20
		[SecurityCritical]
		protected override void Dispose(bool disposing)
		{
			((IDisposable)this.hash).Dispose();
			base.Dispose(disposing);
		}

		// Token: 0x04000148 RID: 328
		private static byte[] Empty = new byte[0];

		// Token: 0x04000149 RID: 329
		private SHA384 hash;
	}
}
