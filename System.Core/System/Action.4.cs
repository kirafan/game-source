﻿using System;

namespace System
{
	// Token: 0x0200008A RID: 138
	// (Invoke) Token: 0x06000627 RID: 1575
	public delegate void Action<T1, T2, T3, T4>(T1 arg1, T2 arg2, T3 arg3, T4 arg4);
}
