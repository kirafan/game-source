﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace System.Linq
{
	// Token: 0x0200001B RID: 27
	public static class Enumerable
	{
		// Token: 0x06000175 RID: 373 RVA: 0x000086E0 File Offset: 0x000068E0
		public static TSource Aggregate<TSource>(this IEnumerable<TSource> source, Func<TSource, TSource, TSource> func)
		{
			Check.SourceAndFunc(source, func);
			TSource result;
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				if (!enumerator.MoveNext())
				{
					throw new InvalidOperationException("No elements in source list");
				}
				TSource tsource = enumerator.Current;
				while (enumerator.MoveNext())
				{
					TSource arg = enumerator.Current;
					tsource = func(tsource, arg);
				}
				result = tsource;
			}
			return result;
		}

		// Token: 0x06000176 RID: 374 RVA: 0x0000876C File Offset: 0x0000696C
		public static TAccumulate Aggregate<TSource, TAccumulate>(this IEnumerable<TSource> source, TAccumulate seed, Func<TAccumulate, TSource, TAccumulate> func)
		{
			Check.SourceAndFunc(source, func);
			TAccumulate taccumulate = seed;
			foreach (TSource arg in source)
			{
				taccumulate = func(taccumulate, arg);
			}
			return taccumulate;
		}

		// Token: 0x06000177 RID: 375 RVA: 0x000087D8 File Offset: 0x000069D8
		public static TResult Aggregate<TSource, TAccumulate, TResult>(this IEnumerable<TSource> source, TAccumulate seed, Func<TAccumulate, TSource, TAccumulate> func, Func<TAccumulate, TResult> resultSelector)
		{
			Check.SourceAndFunc(source, func);
			if (resultSelector == null)
			{
				throw new ArgumentNullException("resultSelector");
			}
			TAccumulate arg = seed;
			foreach (TSource arg2 in source)
			{
				arg = func(arg, arg2);
			}
			return resultSelector(arg);
		}

		// Token: 0x06000178 RID: 376 RVA: 0x0000885C File Offset: 0x00006A5C
		public static bool All<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			foreach (TSource arg in source)
			{
				if (!predicate(arg))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000179 RID: 377 RVA: 0x000088D0 File Offset: 0x00006AD0
		public static bool Any<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			ICollection<TSource> collection = source as ICollection<TSource>;
			if (collection != null)
			{
				return collection.Count > 0;
			}
			bool result;
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				result = enumerator.MoveNext();
			}
			return result;
		}

		// Token: 0x0600017A RID: 378 RVA: 0x0000893C File Offset: 0x00006B3C
		public static bool Any<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			foreach (TSource arg in source)
			{
				if (predicate(arg))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600017B RID: 379 RVA: 0x000089B0 File Offset: 0x00006BB0
		public static IEnumerable<TSource> AsEnumerable<TSource>(this IEnumerable<TSource> source)
		{
			return source;
		}

		// Token: 0x0600017C RID: 380 RVA: 0x000089B4 File Offset: 0x00006BB4
		public static double Average(this IEnumerable<int> source)
		{
			Check.Source(source);
			long num = 0L;
			int num2 = 0;
			foreach (int num3 in source)
			{
				checked
				{
					num += unchecked((long)num3);
				}
				num2++;
			}
			if (num2 == 0)
			{
				throw new InvalidOperationException();
			}
			return (double)num / (double)num2;
		}

		// Token: 0x0600017D RID: 381 RVA: 0x00008A30 File Offset: 0x00006C30
		public static double Average(this IEnumerable<long> source)
		{
			Check.Source(source);
			long num = 0L;
			long num2 = 0L;
			foreach (long num3 in source)
			{
				num += num3;
				num2 += 1L;
			}
			if (num2 == 0L)
			{
				throw new InvalidOperationException();
			}
			return (double)num / (double)num2;
		}

		// Token: 0x0600017E RID: 382 RVA: 0x00008AB0 File Offset: 0x00006CB0
		public static double Average(this IEnumerable<double> source)
		{
			Check.Source(source);
			double num = 0.0;
			long num2 = 0L;
			foreach (double num3 in source)
			{
				double num4 = num3;
				num += num4;
				num2 += 1L;
			}
			if (num2 == 0L)
			{
				throw new InvalidOperationException();
			}
			return num / (double)num2;
		}

		// Token: 0x0600017F RID: 383 RVA: 0x00008B34 File Offset: 0x00006D34
		public static float Average(this IEnumerable<float> source)
		{
			Check.Source(source);
			float num = 0f;
			long num2 = 0L;
			foreach (float num3 in source)
			{
				float num4 = num3;
				num += num4;
				num2 += 1L;
			}
			if (num2 == 0L)
			{
				throw new InvalidOperationException();
			}
			return num / (float)num2;
		}

		// Token: 0x06000180 RID: 384 RVA: 0x00008BB4 File Offset: 0x00006DB4
		public static decimal Average(this IEnumerable<decimal> source)
		{
			Check.Source(source);
			decimal d = 0m;
			long num = 0L;
			foreach (decimal d2 in source)
			{
				d += d2;
				num += 1L;
			}
			if (num == 0L)
			{
				throw new InvalidOperationException();
			}
			return d / num;
		}

		// Token: 0x06000181 RID: 385 RVA: 0x00008C40 File Offset: 0x00006E40
		public static double? Average(this IEnumerable<int?> source)
		{
			Check.Source(source);
			long num = 0L;
			long num2 = 0L;
			foreach (int? num3 in source)
			{
				if (num3 != null)
				{
					num += (long)num3.Value;
					num2 += 1L;
				}
			}
			if (num2 == 0L)
			{
				return null;
			}
			return new double?((double)num / (double)num2);
		}

		// Token: 0x06000182 RID: 386 RVA: 0x00008CE0 File Offset: 0x00006EE0
		public static double? Average(this IEnumerable<long?> source)
		{
			Check.Source(source);
			long num = 0L;
			long num2 = 0L;
			foreach (long? num3 in source)
			{
				if (num3 != null)
				{
					checked
					{
						num += num3.Value;
					}
					num2 += 1L;
				}
			}
			if (num2 == 0L)
			{
				return null;
			}
			return new double?((double)num / (double)num2);
		}

		// Token: 0x06000183 RID: 387 RVA: 0x00008D80 File Offset: 0x00006F80
		public static double? Average(this IEnumerable<double?> source)
		{
			Check.Source(source);
			double num = 0.0;
			long num2 = 0L;
			foreach (double? num3 in source)
			{
				if (num3 != null)
				{
					num += num3.Value;
					num2 += 1L;
				}
			}
			if (num2 == 0L)
			{
				return null;
			}
			return new double?(num / (double)num2);
		}

		// Token: 0x06000184 RID: 388 RVA: 0x00008E24 File Offset: 0x00007024
		public static decimal? Average(this IEnumerable<decimal?> source)
		{
			Check.Source(source);
			decimal d = 0m;
			long num = 0L;
			foreach (decimal? num2 in source)
			{
				if (num2 != null)
				{
					d += num2.Value;
					num += 1L;
				}
			}
			if (num == 0L)
			{
				return null;
			}
			return new decimal?(d / num);
		}

		// Token: 0x06000185 RID: 389 RVA: 0x00008ED4 File Offset: 0x000070D4
		public static float? Average(this IEnumerable<float?> source)
		{
			Check.Source(source);
			float num = 0f;
			long num2 = 0L;
			foreach (float? num3 in source)
			{
				if (num3 != null)
				{
					num += num3.Value;
					num2 += 1L;
				}
			}
			if (num2 == 0L)
			{
				return null;
			}
			return new float?(num / (float)num2);
		}

		// Token: 0x06000186 RID: 390 RVA: 0x00008F74 File Offset: 0x00007174
		public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			Check.SourceAndSelector(source, selector);
			long num = 0L;
			long num2 = 0L;
			foreach (TSource arg in source)
			{
				num += (long)selector(arg);
				num2 += 1L;
			}
			if (num2 == 0L)
			{
				throw new InvalidOperationException();
			}
			return (double)num / (double)num2;
		}

		// Token: 0x06000187 RID: 391 RVA: 0x00008FFC File Offset: 0x000071FC
		public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			Check.SourceAndSelector(source, selector);
			long num = 0L;
			long num2 = 0L;
			foreach (TSource arg in source)
			{
				int? num3 = selector(arg);
				if (num3 != null)
				{
					num += (long)num3.Value;
					num2 += 1L;
				}
			}
			if (num2 == 0L)
			{
				return null;
			}
			return new double?((double)num / (double)num2);
		}

		// Token: 0x06000188 RID: 392 RVA: 0x000090A8 File Offset: 0x000072A8
		public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			Check.SourceAndSelector(source, selector);
			long num = 0L;
			long num2 = 0L;
			foreach (TSource arg in source)
			{
				checked
				{
					num += selector(arg);
				}
				num2 += 1L;
			}
			if (num2 == 0L)
			{
				throw new InvalidOperationException();
			}
			return (double)num / (double)num2;
		}

		// Token: 0x06000189 RID: 393 RVA: 0x0000912C File Offset: 0x0000732C
		public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			Check.SourceAndSelector(source, selector);
			long num = 0L;
			long num2 = 0L;
			foreach (TSource arg in source)
			{
				long? num3 = selector(arg);
				if (num3 != null)
				{
					checked
					{
						num += num3.Value;
					}
					num2 += 1L;
				}
			}
			if (num2 == 0L)
			{
				return null;
			}
			return new double?((double)num / (double)num2);
		}

		// Token: 0x0600018A RID: 394 RVA: 0x000091D4 File Offset: 0x000073D4
		public static double Average<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			Check.SourceAndSelector(source, selector);
			double num = 0.0;
			long num2 = 0L;
			foreach (TSource arg in source)
			{
				num += selector(arg);
				num2 += 1L;
			}
			if (num2 == 0L)
			{
				throw new InvalidOperationException();
			}
			return num / (double)num2;
		}

		// Token: 0x0600018B RID: 395 RVA: 0x00009260 File Offset: 0x00007460
		public static double? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			Check.SourceAndSelector(source, selector);
			double num = 0.0;
			long num2 = 0L;
			foreach (TSource arg in source)
			{
				double? num3 = selector(arg);
				if (num3 != null)
				{
					num += num3.Value;
					num2 += 1L;
				}
			}
			if (num2 == 0L)
			{
				return null;
			}
			return new double?(num / (double)num2);
		}

		// Token: 0x0600018C RID: 396 RVA: 0x00009310 File Offset: 0x00007510
		public static float Average<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			Check.SourceAndSelector(source, selector);
			float num = 0f;
			long num2 = 0L;
			foreach (TSource arg in source)
			{
				num += selector(arg);
				num2 += 1L;
			}
			if (num2 == 0L)
			{
				throw new InvalidOperationException();
			}
			return num / (float)num2;
		}

		// Token: 0x0600018D RID: 397 RVA: 0x00009398 File Offset: 0x00007598
		public static float? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			Check.SourceAndSelector(source, selector);
			float num = 0f;
			long num2 = 0L;
			foreach (TSource arg in source)
			{
				float? num3 = selector(arg);
				if (num3 != null)
				{
					num += num3.Value;
					num2 += 1L;
				}
			}
			if (num2 == 0L)
			{
				return null;
			}
			return new float?(num / (float)num2);
		}

		// Token: 0x0600018E RID: 398 RVA: 0x00009444 File Offset: 0x00007644
		public static decimal Average<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			Check.SourceAndSelector(source, selector);
			decimal d = 0m;
			long num = 0L;
			foreach (TSource arg in source)
			{
				d += selector(arg);
				num += 1L;
			}
			if (num == 0L)
			{
				throw new InvalidOperationException();
			}
			return d / num;
		}

		// Token: 0x0600018F RID: 399 RVA: 0x000094D8 File Offset: 0x000076D8
		public static decimal? Average<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			Check.SourceAndSelector(source, selector);
			decimal d = 0m;
			long num = 0L;
			foreach (TSource arg in source)
			{
				decimal? num2 = selector(arg);
				if (num2 != null)
				{
					d += num2.Value;
					num += 1L;
				}
			}
			if (num == 0L)
			{
				return null;
			}
			return new decimal?(d / num);
		}

		// Token: 0x06000190 RID: 400 RVA: 0x00009590 File Offset: 0x00007790
		public static IEnumerable<TResult> Cast<TResult>(this IEnumerable source)
		{
			Check.Source(source);
			IEnumerable<TResult> enumerable = source as IEnumerable<TResult>;
			if (enumerable != null)
			{
				return enumerable;
			}
			return Enumerable.CreateCastIterator<TResult>(source);
		}

		// Token: 0x06000191 RID: 401 RVA: 0x000095B8 File Offset: 0x000077B8
		private static IEnumerable<TResult> CreateCastIterator<TResult>(IEnumerable source)
		{
			foreach (object obj in source)
			{
				TResult element = (TResult)((object)obj);
				yield return element;
			}
			yield break;
		}

		// Token: 0x06000192 RID: 402 RVA: 0x000095E4 File Offset: 0x000077E4
		public static IEnumerable<TSource> Concat<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			Check.FirstAndSecond(first, second);
			return Enumerable.CreateConcatIterator<TSource>(first, second);
		}

		// Token: 0x06000193 RID: 403 RVA: 0x000095F4 File Offset: 0x000077F4
		private static IEnumerable<TSource> CreateConcatIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			foreach (TSource element in first)
			{
				yield return element;
			}
			foreach (TSource element2 in second)
			{
				yield return element2;
			}
			yield break;
		}

		// Token: 0x06000194 RID: 404 RVA: 0x0000962C File Offset: 0x0000782C
		public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value)
		{
			ICollection<TSource> collection = source as ICollection<TSource>;
			if (collection != null)
			{
				return collection.Contains(value);
			}
			return source.Contains(value, null);
		}

		// Token: 0x06000195 RID: 405 RVA: 0x00009658 File Offset: 0x00007858
		public static bool Contains<TSource>(this IEnumerable<TSource> source, TSource value, IEqualityComparer<TSource> comparer)
		{
			Check.Source(source);
			if (comparer == null)
			{
				comparer = EqualityComparer<TSource>.Default;
			}
			foreach (TSource x in source)
			{
				if (comparer.Equals(x, value))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000196 RID: 406 RVA: 0x000096DC File Offset: 0x000078DC
		public static int Count<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			ICollection<TSource> collection = source as ICollection<TSource>;
			if (collection != null)
			{
				return collection.Count;
			}
			int num = 0;
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x06000197 RID: 407 RVA: 0x00009750 File Offset: 0x00007950
		public static int Count<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> selector)
		{
			Check.SourceAndSelector(source, selector);
			int num = 0;
			foreach (TSource arg in source)
			{
				if (selector(arg))
				{
					num++;
				}
			}
			return num;
		}

		// Token: 0x06000198 RID: 408 RVA: 0x000097C4 File Offset: 0x000079C4
		public static IEnumerable<TSource> DefaultIfEmpty<TSource>(this IEnumerable<TSource> source)
		{
			return source.DefaultIfEmpty(default(TSource));
		}

		// Token: 0x06000199 RID: 409 RVA: 0x000097E0 File Offset: 0x000079E0
		public static IEnumerable<TSource> DefaultIfEmpty<TSource>(this IEnumerable<TSource> source, TSource defaultValue)
		{
			Check.Source(source);
			return Enumerable.CreateDefaultIfEmptyIterator<TSource>(source, defaultValue);
		}

		// Token: 0x0600019A RID: 410 RVA: 0x000097F0 File Offset: 0x000079F0
		private static IEnumerable<TSource> CreateDefaultIfEmptyIterator<TSource>(IEnumerable<TSource> source, TSource defaultValue)
		{
			bool empty = true;
			foreach (TSource item in source)
			{
				empty = false;
				yield return item;
			}
			if (empty)
			{
				yield return defaultValue;
			}
			yield break;
		}

		// Token: 0x0600019B RID: 411 RVA: 0x00009828 File Offset: 0x00007A28
		public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source)
		{
			return source.Distinct(null);
		}

		// Token: 0x0600019C RID: 412 RVA: 0x00009834 File Offset: 0x00007A34
		public static IEnumerable<TSource> Distinct<TSource>(this IEnumerable<TSource> source, IEqualityComparer<TSource> comparer)
		{
			Check.Source(source);
			if (comparer == null)
			{
				comparer = EqualityComparer<TSource>.Default;
			}
			return Enumerable.CreateDistinctIterator<TSource>(source, comparer);
		}

		// Token: 0x0600019D RID: 413 RVA: 0x00009850 File Offset: 0x00007A50
		private static IEnumerable<TSource> CreateDistinctIterator<TSource>(IEnumerable<TSource> source, IEqualityComparer<TSource> comparer)
		{
			HashSet<TSource> items = new HashSet<TSource>(comparer);
			foreach (TSource element in source)
			{
				if (!items.Contains(element))
				{
					items.Add(element);
					yield return element;
				}
			}
			yield break;
		}

		// Token: 0x0600019E RID: 414 RVA: 0x00009888 File Offset: 0x00007A88
		private static TSource ElementAt<TSource>(this IEnumerable<TSource> source, int index, Enumerable.Fallback fallback)
		{
			long num = 0L;
			foreach (TSource result in source)
			{
				long num2 = (long)index;
				long num3 = num;
				num = num3 + 1L;
				if (num2 == num3)
				{
					return result;
				}
			}
			if (fallback == Enumerable.Fallback.Throw)
			{
				throw new ArgumentOutOfRangeException();
			}
			return default(TSource);
		}

		// Token: 0x0600019F RID: 415 RVA: 0x00009910 File Offset: 0x00007B10
		public static TSource ElementAt<TSource>(this IEnumerable<TSource> source, int index)
		{
			Check.Source(source);
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				return list[index];
			}
			return source.ElementAt(index, Enumerable.Fallback.Throw);
		}

		// Token: 0x060001A0 RID: 416 RVA: 0x00009950 File Offset: 0x00007B50
		public static TSource ElementAtOrDefault<TSource>(this IEnumerable<TSource> source, int index)
		{
			Check.Source(source);
			if (index < 0)
			{
				return default(TSource);
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				return (index >= list.Count) ? default(TSource) : list[index];
			}
			return source.ElementAt(index, Enumerable.Fallback.Default);
		}

		// Token: 0x060001A1 RID: 417 RVA: 0x000099AC File Offset: 0x00007BAC
		public static IEnumerable<TResult> Empty<TResult>()
		{
			return new TResult[0];
		}

		// Token: 0x060001A2 RID: 418 RVA: 0x000099B4 File Offset: 0x00007BB4
		public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			return first.Except(second, null);
		}

		// Token: 0x060001A3 RID: 419 RVA: 0x000099C0 File Offset: 0x00007BC0
		public static IEnumerable<TSource> Except<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			Check.FirstAndSecond(first, second);
			if (comparer == null)
			{
				comparer = EqualityComparer<TSource>.Default;
			}
			return Enumerable.CreateExceptIterator<TSource>(first, second, comparer);
		}

		// Token: 0x060001A4 RID: 420 RVA: 0x000099E0 File Offset: 0x00007BE0
		private static IEnumerable<TSource> CreateExceptIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			HashSet<TSource> items = new HashSet<TSource>(second, comparer);
			foreach (TSource element in first)
			{
				if (!items.Contains(element, comparer))
				{
					yield return element;
				}
			}
			yield break;
		}

		// Token: 0x060001A5 RID: 421 RVA: 0x00009A28 File Offset: 0x00007C28
		private static TSource First<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate, Enumerable.Fallback fallback)
		{
			foreach (TSource tsource in source)
			{
				if (predicate(tsource))
				{
					return tsource;
				}
			}
			if (fallback == Enumerable.Fallback.Throw)
			{
				throw new InvalidOperationException();
			}
			return default(TSource);
		}

		// Token: 0x060001A6 RID: 422 RVA: 0x00009AAC File Offset: 0x00007CAC
		public static TSource First<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			IList<TSource> list = source as IList<TSource>;
			if (list == null)
			{
				using (IEnumerator<TSource> enumerator = source.GetEnumerator())
				{
					if (enumerator.MoveNext())
					{
						return enumerator.Current;
					}
				}
				throw new InvalidOperationException();
			}
			if (list.Count != 0)
			{
				return list[0];
			}
			throw new InvalidOperationException();
		}

		// Token: 0x060001A7 RID: 423 RVA: 0x00009B38 File Offset: 0x00007D38
		public static TSource First<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.First(predicate, Enumerable.Fallback.Throw);
		}

		// Token: 0x060001A8 RID: 424 RVA: 0x00009B4C File Offset: 0x00007D4C
		public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				if (enumerator.MoveNext())
				{
					return enumerator.Current;
				}
			}
			return default(TSource);
		}

		// Token: 0x060001A9 RID: 425 RVA: 0x00009BBC File Offset: 0x00007DBC
		public static TSource FirstOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.First(predicate, Enumerable.Fallback.Default);
		}

		// Token: 0x060001AA RID: 426 RVA: 0x00009BD0 File Offset: 0x00007DD0
		private static List<T> ContainsGroup<K, T>(Dictionary<K, List<T>> items, K key, IEqualityComparer<K> comparer)
		{
			IEqualityComparer<K> equalityComparer = comparer ?? EqualityComparer<K>.Default;
			foreach (KeyValuePair<K, List<T>> keyValuePair in items)
			{
				if (equalityComparer.Equals(keyValuePair.Key, key))
				{
					return keyValuePair.Value;
				}
			}
			return null;
		}

		// Token: 0x060001AB RID: 427 RVA: 0x00009C5C File Offset: 0x00007E5C
		public static IEnumerable<IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return source.GroupBy(keySelector, null);
		}

		// Token: 0x060001AC RID: 428 RVA: 0x00009C68 File Offset: 0x00007E68
		public static IEnumerable<IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return source.CreateGroupByIterator(keySelector, comparer);
		}

		// Token: 0x060001AD RID: 429 RVA: 0x00009C7C File Offset: 0x00007E7C
		private static IEnumerable<IGrouping<TKey, TSource>> CreateGroupByIterator<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			Dictionary<TKey, List<TSource>> groups = new Dictionary<TKey, List<TSource>>();
			List<TSource> nullList = new List<TSource>();
			int counter = 0;
			int nullCounter = -1;
			foreach (TSource element in source)
			{
				TKey key = keySelector(element);
				if (key == null)
				{
					nullList.Add(element);
					if (nullCounter == -1)
					{
						nullCounter = counter;
						counter++;
					}
				}
				else
				{
					List<TSource> group = Enumerable.ContainsGroup<TKey, TSource>(groups, key, comparer);
					if (group == null)
					{
						group = new List<TSource>();
						groups.Add(key, group);
						counter++;
					}
					group.Add(element);
				}
			}
			counter = 0;
			foreach (KeyValuePair<TKey, List<TSource>> group2 in groups)
			{
				if (counter == nullCounter)
				{
					yield return new Grouping<TKey, TSource>(default(TKey), nullList);
					counter++;
				}
				yield return new Grouping<TKey, TSource>(group2.Key, group2.Value);
				counter++;
			}
			if (counter == nullCounter)
			{
				yield return new Grouping<TKey, TSource>(default(TKey), nullList);
				counter++;
			}
			yield break;
		}

		// Token: 0x060001AE RID: 430 RVA: 0x00009CC4 File Offset: 0x00007EC4
		public static IEnumerable<IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
		{
			return source.GroupBy(keySelector, elementSelector, null);
		}

		// Token: 0x060001AF RID: 431 RVA: 0x00009CD0 File Offset: 0x00007ED0
		public static IEnumerable<IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			Check.SourceAndKeyElementSelectors(source, keySelector, elementSelector);
			return source.CreateGroupByIterator(keySelector, elementSelector, comparer);
		}

		// Token: 0x060001B0 RID: 432 RVA: 0x00009CE4 File Offset: 0x00007EE4
		private static IEnumerable<IGrouping<TKey, TElement>> CreateGroupByIterator<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			Dictionary<TKey, List<TElement>> groups = new Dictionary<TKey, List<TElement>>();
			List<TElement> nullList = new List<TElement>();
			int counter = 0;
			int nullCounter = -1;
			foreach (TSource item in source)
			{
				TKey key = keySelector(item);
				TElement element = elementSelector(item);
				if (key == null)
				{
					nullList.Add(element);
					if (nullCounter == -1)
					{
						nullCounter = counter;
						counter++;
					}
				}
				else
				{
					List<TElement> group = Enumerable.ContainsGroup<TKey, TElement>(groups, key, comparer);
					if (group == null)
					{
						group = new List<TElement>();
						groups.Add(key, group);
						counter++;
					}
					group.Add(element);
				}
			}
			counter = 0;
			foreach (KeyValuePair<TKey, List<TElement>> group2 in groups)
			{
				if (counter == nullCounter)
				{
					yield return new Grouping<TKey, TElement>(default(TKey), nullList);
					counter++;
				}
				yield return new Grouping<TKey, TElement>(group2.Key, group2.Value);
				counter++;
			}
			if (counter == nullCounter)
			{
				yield return new Grouping<TKey, TElement>(default(TKey), nullList);
				counter++;
			}
			yield break;
		}

		// Token: 0x060001B1 RID: 433 RVA: 0x00009D38 File Offset: 0x00007F38
		public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector)
		{
			return source.GroupBy(keySelector, elementSelector, resultSelector, null);
		}

		// Token: 0x060001B2 RID: 434 RVA: 0x00009D44 File Offset: 0x00007F44
		public static IEnumerable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			Check.GroupBySelectors(source, keySelector, elementSelector, resultSelector);
			return source.CreateGroupByIterator(keySelector, elementSelector, resultSelector, comparer);
		}

		// Token: 0x060001B3 RID: 435 RVA: 0x00009D5C File Offset: 0x00007F5C
		private static IEnumerable<TResult> CreateGroupByIterator<TSource, TKey, TElement, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, Func<TKey, IEnumerable<TElement>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			IEnumerable<IGrouping<TKey, TElement>> groups = source.GroupBy(keySelector, elementSelector, comparer);
			foreach (IGrouping<TKey, TElement> group in groups)
			{
				yield return resultSelector(group.Key, group);
			}
			yield break;
		}

		// Token: 0x060001B4 RID: 436 RVA: 0x00009DC0 File Offset: 0x00007FC0
		public static IEnumerable<TResult> GroupBy<TSource, TKey, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector)
		{
			return source.GroupBy(keySelector, resultSelector, null);
		}

		// Token: 0x060001B5 RID: 437 RVA: 0x00009DCC File Offset: 0x00007FCC
		public static IEnumerable<TResult> GroupBy<TSource, TKey, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			Check.SourceAndKeyResultSelectors(source, keySelector, resultSelector);
			return source.CreateGroupByIterator(keySelector, resultSelector, comparer);
		}

		// Token: 0x060001B6 RID: 438 RVA: 0x00009DE0 File Offset: 0x00007FE0
		private static IEnumerable<TResult> CreateGroupByIterator<TSource, TKey, TResult>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TKey, IEnumerable<TSource>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			IEnumerable<IGrouping<TKey, TSource>> groups = source.GroupBy(keySelector, comparer);
			foreach (IGrouping<TKey, TSource> group in groups)
			{
				yield return resultSelector(group.Key, group);
			}
			yield break;
		}

		// Token: 0x060001B7 RID: 439 RVA: 0x00009E34 File Offset: 0x00008034
		public static IEnumerable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector)
		{
			return outer.GroupJoin(inner, outerKeySelector, innerKeySelector, resultSelector, null);
		}

		// Token: 0x060001B8 RID: 440 RVA: 0x00009E44 File Offset: 0x00008044
		public static IEnumerable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			Check.JoinSelectors(outer, inner, outerKeySelector, innerKeySelector, resultSelector);
			if (comparer == null)
			{
				comparer = EqualityComparer<TKey>.Default;
			}
			return outer.CreateGroupJoinIterator(inner, outerKeySelector, innerKeySelector, resultSelector, comparer);
		}

		// Token: 0x060001B9 RID: 441 RVA: 0x00009E78 File Offset: 0x00008078
		private static IEnumerable<TResult> CreateGroupJoinIterator<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, IEnumerable<TInner>, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			ILookup<TKey, TInner> innerKeys = inner.ToLookup(innerKeySelector, comparer);
			foreach (TOuter element in outer)
			{
				TKey outerKey = outerKeySelector(element);
				if (innerKeys.Contains(outerKey))
				{
					yield return resultSelector(element, innerKeys[outerKey]);
				}
				else
				{
					yield return resultSelector(element, Enumerable.Empty<TInner>());
				}
			}
			yield break;
		}

		// Token: 0x060001BA RID: 442 RVA: 0x00009EEC File Offset: 0x000080EC
		public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			return first.Intersect(second, null);
		}

		// Token: 0x060001BB RID: 443 RVA: 0x00009EF8 File Offset: 0x000080F8
		public static IEnumerable<TSource> Intersect<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			Check.FirstAndSecond(first, second);
			if (comparer == null)
			{
				comparer = EqualityComparer<TSource>.Default;
			}
			return Enumerable.CreateIntersectIterator<TSource>(first, second, comparer);
		}

		// Token: 0x060001BC RID: 444 RVA: 0x00009F18 File Offset: 0x00008118
		private static IEnumerable<TSource> CreateIntersectIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			HashSet<TSource> items = new HashSet<TSource>(second, comparer);
			foreach (TSource element in first)
			{
				if (items.Remove(element))
				{
					yield return element;
				}
			}
			yield break;
		}

		// Token: 0x060001BD RID: 445 RVA: 0x00009F60 File Offset: 0x00008160
		public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			Check.JoinSelectors(outer, inner, outerKeySelector, innerKeySelector, resultSelector);
			if (comparer == null)
			{
				comparer = EqualityComparer<TKey>.Default;
			}
			return outer.CreateJoinIterator(inner, outerKeySelector, innerKeySelector, resultSelector, comparer);
		}

		// Token: 0x060001BE RID: 446 RVA: 0x00009F94 File Offset: 0x00008194
		private static IEnumerable<TResult> CreateJoinIterator<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector, IEqualityComparer<TKey> comparer)
		{
			ILookup<TKey, TInner> innerKeys = inner.ToLookup(innerKeySelector, comparer);
			foreach (TOuter element in outer)
			{
				TKey outerKey = outerKeySelector(element);
				if (innerKeys.Contains(outerKey))
				{
					foreach (TInner innerElement in innerKeys[outerKey])
					{
						yield return resultSelector(element, innerElement);
					}
				}
			}
			yield break;
		}

		// Token: 0x060001BF RID: 447 RVA: 0x0000A008 File Offset: 0x00008208
		public static IEnumerable<TResult> Join<TOuter, TInner, TKey, TResult>(this IEnumerable<TOuter> outer, IEnumerable<TInner> inner, Func<TOuter, TKey> outerKeySelector, Func<TInner, TKey> innerKeySelector, Func<TOuter, TInner, TResult> resultSelector)
		{
			return outer.Join(inner, outerKeySelector, innerKeySelector, resultSelector, null);
		}

		// Token: 0x060001C0 RID: 448 RVA: 0x0000A018 File Offset: 0x00008218
		private static TSource Last<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate, Enumerable.Fallback fallback)
		{
			bool flag = true;
			TSource result = default(TSource);
			foreach (TSource tsource in source)
			{
				if (predicate(tsource))
				{
					result = tsource;
					flag = false;
				}
			}
			if (!flag)
			{
				return result;
			}
			if (fallback == Enumerable.Fallback.Throw)
			{
				throw new InvalidOperationException();
			}
			return result;
		}

		// Token: 0x060001C1 RID: 449 RVA: 0x0000A0A8 File Offset: 0x000082A8
		public static TSource Last<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			ICollection<TSource> collection = source as ICollection<TSource>;
			if (collection != null && collection.Count == 0)
			{
				throw new InvalidOperationException();
			}
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				return list[list.Count - 1];
			}
			bool flag = true;
			TSource result = default(TSource);
			foreach (TSource tsource in source)
			{
				result = tsource;
				flag = false;
			}
			if (!flag)
			{
				return result;
			}
			throw new InvalidOperationException();
		}

		// Token: 0x060001C2 RID: 450 RVA: 0x0000A164 File Offset: 0x00008364
		public static TSource Last<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Last(predicate, Enumerable.Fallback.Throw);
		}

		// Token: 0x060001C3 RID: 451 RVA: 0x0000A178 File Offset: 0x00008378
		public static TSource LastOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			IList<TSource> list = source as IList<TSource>;
			if (list != null)
			{
				return (list.Count <= 0) ? default(TSource) : list[list.Count - 1];
			}
			bool flag = true;
			TSource result = default(TSource);
			foreach (TSource tsource in source)
			{
				result = tsource;
				flag = false;
			}
			if (!flag)
			{
				return result;
			}
			return result;
		}

		// Token: 0x060001C4 RID: 452 RVA: 0x0000A22C File Offset: 0x0000842C
		public static TSource LastOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Last(predicate, Enumerable.Fallback.Default);
		}

		// Token: 0x060001C5 RID: 453 RVA: 0x0000A240 File Offset: 0x00008440
		public static long LongCount<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			TSource[] array = source as TSource[];
			if (array != null)
			{
				return array.LongLength;
			}
			long num = 0L;
			using (IEnumerator<TSource> enumerator = source.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					num += 1L;
				}
			}
			return num;
		}

		// Token: 0x060001C6 RID: 454 RVA: 0x0000A2B8 File Offset: 0x000084B8
		public static long LongCount<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> selector)
		{
			Check.SourceAndSelector(source, selector);
			long num = 0L;
			foreach (TSource arg in source)
			{
				if (selector(arg))
				{
					num += 1L;
				}
			}
			return num;
		}

		// Token: 0x060001C7 RID: 455 RVA: 0x0000A32C File Offset: 0x0000852C
		public static int Max(this IEnumerable<int> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<int, int>(source, int.MinValue, (int a, int b) => Math.Max(a, b));
		}

		// Token: 0x060001C8 RID: 456 RVA: 0x0000A368 File Offset: 0x00008568
		public static long Max(this IEnumerable<long> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<long, long>(source, long.MinValue, (long a, long b) => Math.Max(a, b));
		}

		// Token: 0x060001C9 RID: 457 RVA: 0x0000A3A8 File Offset: 0x000085A8
		public static double Max(this IEnumerable<double> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<double, double>(source, double.MinValue, (double a, double b) => Math.Max(a, b));
		}

		// Token: 0x060001CA RID: 458 RVA: 0x0000A3E8 File Offset: 0x000085E8
		public static float Max(this IEnumerable<float> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<float, float>(source, float.MinValue, (float a, float b) => Math.Max(a, b));
		}

		// Token: 0x060001CB RID: 459 RVA: 0x0000A424 File Offset: 0x00008624
		public static decimal Max(this IEnumerable<decimal> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<decimal, decimal>(source, decimal.MinValue, (decimal a, decimal b) => Math.Max(a, b));
		}

		// Token: 0x060001CC RID: 460 RVA: 0x0000A45C File Offset: 0x0000865C
		public static int? Max(this IEnumerable<int?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<int>(source, (int a, int b) => Math.Max(a, b));
		}

		// Token: 0x060001CD RID: 461 RVA: 0x0000A488 File Offset: 0x00008688
		public static long? Max(this IEnumerable<long?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<long>(source, (long a, long b) => Math.Max(a, b));
		}

		// Token: 0x060001CE RID: 462 RVA: 0x0000A4B4 File Offset: 0x000086B4
		public static double? Max(this IEnumerable<double?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<double>(source, (double a, double b) => Math.Max(a, b));
		}

		// Token: 0x060001CF RID: 463 RVA: 0x0000A4E0 File Offset: 0x000086E0
		public static float? Max(this IEnumerable<float?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<float>(source, (float a, float b) => Math.Max(a, b));
		}

		// Token: 0x060001D0 RID: 464 RVA: 0x0000A50C File Offset: 0x0000870C
		public static decimal? Max(this IEnumerable<decimal?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<decimal>(source, (decimal a, decimal b) => Math.Max(a, b));
		}

		// Token: 0x060001D1 RID: 465 RVA: 0x0000A538 File Offset: 0x00008738
		private static T? IterateNullable<T>(IEnumerable<T?> source, Func<T, T, T> selector) where T : struct
		{
			bool flag = true;
			T? result = null;
			foreach (T? t in source)
			{
				if (t != null)
				{
					if (result == null)
					{
						result = new T?(t.Value);
					}
					else
					{
						result = new T?(selector(t.Value, result.Value));
					}
					flag = false;
				}
			}
			if (flag)
			{
				return null;
			}
			return result;
		}

		// Token: 0x060001D2 RID: 466 RVA: 0x0000A5FC File Offset: 0x000087FC
		private static TRet? IterateNullable<TSource, TRet>(IEnumerable<TSource> source, Func<TSource, TRet?> source_selector, Func<TRet?, TRet?, bool> selector) where TRet : struct
		{
			bool flag = true;
			TRet? tret = null;
			foreach (TSource arg in source)
			{
				TRet? tret2 = source_selector(arg);
				if (tret == null)
				{
					tret = tret2;
				}
				else if (selector(tret2, tret))
				{
					tret = tret2;
				}
				flag = false;
			}
			if (flag)
			{
				return null;
			}
			return tret;
		}

		// Token: 0x060001D3 RID: 467 RVA: 0x0000A6A4 File Offset: 0x000088A4
		private static TSource IterateNullable<TSource>(IEnumerable<TSource> source, Func<TSource, TSource, bool> selector)
		{
			TSource tsource = default(TSource);
			foreach (TSource tsource2 in source)
			{
				if (tsource2 != null)
				{
					if (tsource == null || selector(tsource2, tsource))
					{
						tsource = tsource2;
					}
				}
			}
			return tsource;
		}

		// Token: 0x060001D4 RID: 468 RVA: 0x0000A730 File Offset: 0x00008930
		private static TSource IterateNonNullable<TSource>(IEnumerable<TSource> source, Func<TSource, TSource, bool> selector)
		{
			TSource tsource = default(TSource);
			bool flag = true;
			foreach (TSource tsource2 in source)
			{
				if (flag)
				{
					tsource = tsource2;
					flag = false;
				}
				else if (selector(tsource2, tsource))
				{
					tsource = tsource2;
				}
			}
			if (flag)
			{
				throw new InvalidOperationException();
			}
			return tsource;
		}

		// Token: 0x060001D5 RID: 469 RVA: 0x0000A7C0 File Offset: 0x000089C0
		public static TSource Max<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			Comparer<TSource> comparer = Comparer<TSource>.Default;
			Func<TSource, TSource, bool> selector = (TSource a, TSource b) => comparer.Compare(a, b) > 0;
			if (default(TSource) == null)
			{
				return Enumerable.IterateNullable<TSource>(source, selector);
			}
			return Enumerable.IterateNonNullable<TSource>(source, selector);
		}

		// Token: 0x060001D6 RID: 470 RVA: 0x0000A814 File Offset: 0x00008A14
		public static int Max<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, int>(source, int.MinValue, (TSource a, int b) => Math.Max(selector(a), b));
		}

		// Token: 0x060001D7 RID: 471 RVA: 0x0000A854 File Offset: 0x00008A54
		public static long Max<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, long>(source, long.MinValue, (TSource a, long b) => Math.Max(selector(a), b));
		}

		// Token: 0x060001D8 RID: 472 RVA: 0x0000A898 File Offset: 0x00008A98
		public static double Max<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, double>(source, double.MinValue, (TSource a, double b) => Math.Max(selector(a), b));
		}

		// Token: 0x060001D9 RID: 473 RVA: 0x0000A8DC File Offset: 0x00008ADC
		public static float Max<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, float>(source, float.MinValue, (TSource a, float b) => Math.Max(selector(a), b));
		}

		// Token: 0x060001DA RID: 474 RVA: 0x0000A91C File Offset: 0x00008B1C
		public static decimal Max<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, decimal>(source, decimal.MinValue, (TSource a, decimal b) => Math.Max(selector(a), b));
		}

		// Token: 0x060001DB RID: 475 RVA: 0x0000A960 File Offset: 0x00008B60
		private static U Iterate<T, U>(IEnumerable<T> source, U initValue, Func<T, U, U> selector)
		{
			bool flag = true;
			foreach (T arg in source)
			{
				initValue = selector(arg, initValue);
				flag = false;
			}
			if (flag)
			{
				throw new InvalidOperationException();
			}
			return initValue;
		}

		// Token: 0x060001DC RID: 476 RVA: 0x0000A9D4 File Offset: 0x00008BD4
		public static int? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, int>(source, selector, (int? a, int? b) => a != null && b != null && a.Value > b.Value);
		}

		// Token: 0x060001DD RID: 477 RVA: 0x0000A9F0 File Offset: 0x00008BF0
		public static long? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, long>(source, selector, (long? a, long? b) => a != null && b != null && a.Value > b.Value);
		}

		// Token: 0x060001DE RID: 478 RVA: 0x0000AA0C File Offset: 0x00008C0C
		public static double? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, double>(source, selector, (double? a, double? b) => a != null && b != null && a.Value > b.Value);
		}

		// Token: 0x060001DF RID: 479 RVA: 0x0000AA28 File Offset: 0x00008C28
		public static float? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, float>(source, selector, (float? a, float? b) => a != null && b != null && a.Value > b.Value);
		}

		// Token: 0x060001E0 RID: 480 RVA: 0x0000AA44 File Offset: 0x00008C44
		public static decimal? Max<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, decimal>(source, selector, (decimal? a, decimal? b) => a != null && b != null && a.Value > b.Value);
		}

		// Token: 0x060001E1 RID: 481 RVA: 0x0000AA60 File Offset: 0x00008C60
		public static TResult Max<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Select(selector).Max<TResult>();
		}

		// Token: 0x060001E2 RID: 482 RVA: 0x0000AA78 File Offset: 0x00008C78
		public static int Min(this IEnumerable<int> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<int, int>(source, int.MaxValue, (int a, int b) => Math.Min(a, b));
		}

		// Token: 0x060001E3 RID: 483 RVA: 0x0000AAB4 File Offset: 0x00008CB4
		public static long Min(this IEnumerable<long> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<long, long>(source, long.MaxValue, (long a, long b) => Math.Min(a, b));
		}

		// Token: 0x060001E4 RID: 484 RVA: 0x0000AAF4 File Offset: 0x00008CF4
		public static double Min(this IEnumerable<double> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<double, double>(source, double.MaxValue, (double a, double b) => Math.Min(a, b));
		}

		// Token: 0x060001E5 RID: 485 RVA: 0x0000AB34 File Offset: 0x00008D34
		public static float Min(this IEnumerable<float> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<float, float>(source, float.MaxValue, (float a, float b) => Math.Min(a, b));
		}

		// Token: 0x060001E6 RID: 486 RVA: 0x0000AB70 File Offset: 0x00008D70
		public static decimal Min(this IEnumerable<decimal> source)
		{
			Check.Source(source);
			return Enumerable.Iterate<decimal, decimal>(source, decimal.MaxValue, (decimal a, decimal b) => Math.Min(a, b));
		}

		// Token: 0x060001E7 RID: 487 RVA: 0x0000ABA8 File Offset: 0x00008DA8
		public static int? Min(this IEnumerable<int?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<int>(source, (int a, int b) => Math.Min(a, b));
		}

		// Token: 0x060001E8 RID: 488 RVA: 0x0000ABD4 File Offset: 0x00008DD4
		public static long? Min(this IEnumerable<long?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<long>(source, (long a, long b) => Math.Min(a, b));
		}

		// Token: 0x060001E9 RID: 489 RVA: 0x0000AC00 File Offset: 0x00008E00
		public static double? Min(this IEnumerable<double?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<double>(source, (double a, double b) => Math.Min(a, b));
		}

		// Token: 0x060001EA RID: 490 RVA: 0x0000AC2C File Offset: 0x00008E2C
		public static float? Min(this IEnumerable<float?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<float>(source, (float a, float b) => Math.Min(a, b));
		}

		// Token: 0x060001EB RID: 491 RVA: 0x0000AC58 File Offset: 0x00008E58
		public static decimal? Min(this IEnumerable<decimal?> source)
		{
			Check.Source(source);
			return Enumerable.IterateNullable<decimal>(source, (decimal a, decimal b) => Math.Min(a, b));
		}

		// Token: 0x060001EC RID: 492 RVA: 0x0000AC84 File Offset: 0x00008E84
		public static TSource Min<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			Comparer<TSource> comparer = Comparer<TSource>.Default;
			Func<TSource, TSource, bool> selector = (TSource a, TSource b) => comparer.Compare(a, b) < 0;
			if (default(TSource) == null)
			{
				return Enumerable.IterateNullable<TSource>(source, selector);
			}
			return Enumerable.IterateNonNullable<TSource>(source, selector);
		}

		// Token: 0x060001ED RID: 493 RVA: 0x0000ACD8 File Offset: 0x00008ED8
		public static int Min<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, int>(source, int.MaxValue, (TSource a, int b) => Math.Min(selector(a), b));
		}

		// Token: 0x060001EE RID: 494 RVA: 0x0000AD18 File Offset: 0x00008F18
		public static long Min<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, long>(source, long.MaxValue, (TSource a, long b) => Math.Min(selector(a), b));
		}

		// Token: 0x060001EF RID: 495 RVA: 0x0000AD5C File Offset: 0x00008F5C
		public static double Min<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, double>(source, double.MaxValue, (TSource a, double b) => Math.Min(selector(a), b));
		}

		// Token: 0x060001F0 RID: 496 RVA: 0x0000ADA0 File Offset: 0x00008FA0
		public static float Min<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, float>(source, float.MaxValue, (TSource a, float b) => Math.Min(selector(a), b));
		}

		// Token: 0x060001F1 RID: 497 RVA: 0x0000ADE0 File Offset: 0x00008FE0
		public static decimal Min<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.Iterate<TSource, decimal>(source, decimal.MaxValue, (TSource a, decimal b) => Math.Min(selector(a), b));
		}

		// Token: 0x060001F2 RID: 498 RVA: 0x0000AE24 File Offset: 0x00009024
		public static int? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, int>(source, selector, (int? a, int? b) => a != null && b != null && a.Value < b.Value);
		}

		// Token: 0x060001F3 RID: 499 RVA: 0x0000AE40 File Offset: 0x00009040
		public static long? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, long>(source, selector, (long? a, long? b) => a != null && b != null && a.Value < b.Value);
		}

		// Token: 0x060001F4 RID: 500 RVA: 0x0000AE5C File Offset: 0x0000905C
		public static float? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, float>(source, selector, (float? a, float? b) => a != null && b != null && a.Value < b.Value);
		}

		// Token: 0x060001F5 RID: 501 RVA: 0x0000AE78 File Offset: 0x00009078
		public static double? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, double>(source, selector, (double? a, double? b) => a != null && b != null && a.Value < b.Value);
		}

		// Token: 0x060001F6 RID: 502 RVA: 0x0000AE94 File Offset: 0x00009094
		public static decimal? Min<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.IterateNullable<TSource, decimal>(source, selector, (decimal? a, decimal? b) => a != null && b != null && a.Value < b.Value);
		}

		// Token: 0x060001F7 RID: 503 RVA: 0x0000AEB0 File Offset: 0x000090B0
		public static TResult Min<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Select(selector).Min<TResult>();
		}

		// Token: 0x060001F8 RID: 504 RVA: 0x0000AEC8 File Offset: 0x000090C8
		public static IEnumerable<TResult> OfType<TResult>(this IEnumerable source)
		{
			Check.Source(source);
			return Enumerable.CreateOfTypeIterator<TResult>(source);
		}

		// Token: 0x060001F9 RID: 505 RVA: 0x0000AED8 File Offset: 0x000090D8
		private static IEnumerable<TResult> CreateOfTypeIterator<TResult>(IEnumerable source)
		{
			foreach (object element in source)
			{
				if (element is TResult)
				{
					yield return (TResult)((object)element);
				}
			}
			yield break;
		}

		// Token: 0x060001FA RID: 506 RVA: 0x0000AF04 File Offset: 0x00009104
		public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return source.OrderBy(keySelector, null);
		}

		// Token: 0x060001FB RID: 507 RVA: 0x0000AF10 File Offset: 0x00009110
		public static IOrderedEnumerable<TSource> OrderBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return new OrderedSequence<TSource, TKey>(source, keySelector, comparer, SortDirection.Ascending);
		}

		// Token: 0x060001FC RID: 508 RVA: 0x0000AF24 File Offset: 0x00009124
		public static IOrderedEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return source.OrderByDescending(keySelector, null);
		}

		// Token: 0x060001FD RID: 509 RVA: 0x0000AF30 File Offset: 0x00009130
		public static IOrderedEnumerable<TSource> OrderByDescending<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return new OrderedSequence<TSource, TKey>(source, keySelector, comparer, SortDirection.Descending);
		}

		// Token: 0x060001FE RID: 510 RVA: 0x0000AF44 File Offset: 0x00009144
		public static IEnumerable<int> Range(int start, int count)
		{
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			long num = (long)start + (long)count - 1L;
			if (num > 2147483647L)
			{
				throw new ArgumentOutOfRangeException();
			}
			return Enumerable.CreateRangeIterator(start, (int)num);
		}

		// Token: 0x060001FF RID: 511 RVA: 0x0000AF88 File Offset: 0x00009188
		private static IEnumerable<int> CreateRangeIterator(int start, int upto)
		{
			for (int i = start; i <= upto; i++)
			{
				yield return i;
			}
			yield break;
		}

		// Token: 0x06000200 RID: 512 RVA: 0x0000AFC0 File Offset: 0x000091C0
		public static IEnumerable<TResult> Repeat<TResult>(TResult element, int count)
		{
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException();
			}
			return Enumerable.CreateRepeatIterator<TResult>(element, count);
		}

		// Token: 0x06000201 RID: 513 RVA: 0x0000AFD8 File Offset: 0x000091D8
		private static IEnumerable<TResult> CreateRepeatIterator<TResult>(TResult element, int count)
		{
			for (int i = 0; i < count; i++)
			{
				yield return element;
			}
			yield break;
		}

		// Token: 0x06000202 RID: 514 RVA: 0x0000B010 File Offset: 0x00009210
		public static IEnumerable<TSource> Reverse<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			return Enumerable.CreateReverseIterator<TSource>(source);
		}

		// Token: 0x06000203 RID: 515 RVA: 0x0000B020 File Offset: 0x00009220
		private static IEnumerable<TSource> CreateReverseIterator<TSource>(IEnumerable<TSource> source)
		{
			IList<TSource> list = source as IList<TSource>;
			if (list == null)
			{
				list = new List<TSource>(source);
			}
			for (int i = list.Count - 1; i >= 0; i--)
			{
				yield return list[i];
			}
			yield break;
		}

		// Token: 0x06000204 RID: 516 RVA: 0x0000B04C File Offset: 0x0000924C
		public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.CreateSelectIterator<TSource, TResult>(source, selector);
		}

		// Token: 0x06000205 RID: 517 RVA: 0x0000B05C File Offset: 0x0000925C
		private static IEnumerable<TResult> CreateSelectIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, TResult> selector)
		{
			foreach (TSource element in source)
			{
				yield return selector(element);
			}
			yield break;
		}

		// Token: 0x06000206 RID: 518 RVA: 0x0000B094 File Offset: 0x00009294
		public static IEnumerable<TResult> Select<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.CreateSelectIterator<TSource, TResult>(source, selector);
		}

		// Token: 0x06000207 RID: 519 RVA: 0x0000B0A4 File Offset: 0x000092A4
		private static IEnumerable<TResult> CreateSelectIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, int, TResult> selector)
		{
			int counter = 0;
			foreach (TSource element in source)
			{
				yield return selector(element, counter);
				counter++;
			}
			yield break;
		}

		// Token: 0x06000208 RID: 520 RVA: 0x0000B0DC File Offset: 0x000092DC
		public static IEnumerable<TResult> SelectMany<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TResult>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.CreateSelectManyIterator<TSource, TResult>(source, selector);
		}

		// Token: 0x06000209 RID: 521 RVA: 0x0000B0EC File Offset: 0x000092EC
		private static IEnumerable<TResult> CreateSelectManyIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, IEnumerable<TResult>> selector)
		{
			foreach (TSource element in source)
			{
				foreach (TResult item in selector(element))
				{
					yield return item;
				}
			}
			yield break;
		}

		// Token: 0x0600020A RID: 522 RVA: 0x0000B124 File Offset: 0x00009324
		public static IEnumerable<TResult> SelectMany<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TResult>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return Enumerable.CreateSelectManyIterator<TSource, TResult>(source, selector);
		}

		// Token: 0x0600020B RID: 523 RVA: 0x0000B134 File Offset: 0x00009334
		private static IEnumerable<TResult> CreateSelectManyIterator<TSource, TResult>(IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TResult>> selector)
		{
			int counter = 0;
			foreach (TSource element in source)
			{
				foreach (TResult item in selector(element, counter))
				{
					yield return item;
				}
				counter++;
			}
			yield break;
		}

		// Token: 0x0600020C RID: 524 RVA: 0x0000B16C File Offset: 0x0000936C
		public static IEnumerable<TResult> SelectMany<TSource, TCollection, TResult>(this IEnumerable<TSource> source, Func<TSource, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> selector)
		{
			Check.SourceAndCollectionSelectors(source, collectionSelector, selector);
			return Enumerable.CreateSelectManyIterator<TSource, TCollection, TResult>(source, collectionSelector, selector);
		}

		// Token: 0x0600020D RID: 525 RVA: 0x0000B180 File Offset: 0x00009380
		private static IEnumerable<TResult> CreateSelectManyIterator<TSource, TCollection, TResult>(IEnumerable<TSource> source, Func<TSource, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> selector)
		{
			foreach (TSource element in source)
			{
				foreach (TCollection collection in collectionSelector(element))
				{
					yield return selector(element, collection);
				}
			}
			yield break;
		}

		// Token: 0x0600020E RID: 526 RVA: 0x0000B1C8 File Offset: 0x000093C8
		public static IEnumerable<TResult> SelectMany<TSource, TCollection, TResult>(this IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> selector)
		{
			Check.SourceAndCollectionSelectors(source, collectionSelector, selector);
			return Enumerable.CreateSelectManyIterator<TSource, TCollection, TResult>(source, collectionSelector, selector);
		}

		// Token: 0x0600020F RID: 527 RVA: 0x0000B1DC File Offset: 0x000093DC
		private static IEnumerable<TResult> CreateSelectManyIterator<TSource, TCollection, TResult>(IEnumerable<TSource> source, Func<TSource, int, IEnumerable<TCollection>> collectionSelector, Func<TSource, TCollection, TResult> selector)
		{
			int counter = 0;
			foreach (TSource element in source)
			{
				TSource arg = element;
				int arg2;
				counter = (arg2 = counter) + 1;
				foreach (TCollection collection in collectionSelector(arg, arg2))
				{
					yield return selector(element, collection);
				}
			}
			yield break;
		}

		// Token: 0x06000210 RID: 528 RVA: 0x0000B224 File Offset: 0x00009424
		private static TSource Single<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate, Enumerable.Fallback fallback)
		{
			bool flag = false;
			TSource result = default(TSource);
			foreach (TSource tsource in source)
			{
				if (predicate(tsource))
				{
					if (flag)
					{
						throw new InvalidOperationException();
					}
					flag = true;
					result = tsource;
				}
			}
			if (!flag && fallback == Enumerable.Fallback.Throw)
			{
				throw new InvalidOperationException();
			}
			return result;
		}

		// Token: 0x06000211 RID: 529 RVA: 0x0000B2C0 File Offset: 0x000094C0
		public static TSource Single<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			bool flag = false;
			TSource result = default(TSource);
			foreach (TSource tsource in source)
			{
				if (flag)
				{
					throw new InvalidOperationException();
				}
				flag = true;
				result = tsource;
			}
			if (!flag)
			{
				throw new InvalidOperationException();
			}
			return result;
		}

		// Token: 0x06000212 RID: 530 RVA: 0x0000B348 File Offset: 0x00009548
		public static TSource Single<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Single(predicate, Enumerable.Fallback.Throw);
		}

		// Token: 0x06000213 RID: 531 RVA: 0x0000B35C File Offset: 0x0000955C
		public static TSource SingleOrDefault<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			bool flag = false;
			TSource result = default(TSource);
			foreach (TSource tsource in source)
			{
				if (flag)
				{
					throw new InvalidOperationException();
				}
				flag = true;
				result = tsource;
			}
			return result;
		}

		// Token: 0x06000214 RID: 532 RVA: 0x0000B3D8 File Offset: 0x000095D8
		public static TSource SingleOrDefault<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Single(predicate, Enumerable.Fallback.Default);
		}

		// Token: 0x06000215 RID: 533 RVA: 0x0000B3EC File Offset: 0x000095EC
		public static IEnumerable<TSource> Skip<TSource>(this IEnumerable<TSource> source, int count)
		{
			Check.Source(source);
			return Enumerable.CreateSkipIterator<TSource>(source, count);
		}

		// Token: 0x06000216 RID: 534 RVA: 0x0000B3FC File Offset: 0x000095FC
		private static IEnumerable<TSource> CreateSkipIterator<TSource>(IEnumerable<TSource> source, int count)
		{
			IEnumerator<TSource> enumerator = source.GetEnumerator();
			try
			{
				do
				{
					int num;
					count = (num = count) - 1;
					if (num <= 0)
					{
						goto Block_4;
					}
				}
				while (enumerator.MoveNext());
				yield break;
				Block_4:
				while (enumerator.MoveNext())
				{
					TSource tsource = enumerator.Current;
					yield return tsource;
				}
			}
			finally
			{
				enumerator.Dispose();
			}
			yield break;
		}

		// Token: 0x06000217 RID: 535 RVA: 0x0000B434 File Offset: 0x00009634
		public static IEnumerable<TSource> SkipWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return Enumerable.CreateSkipWhileIterator<TSource>(source, predicate);
		}

		// Token: 0x06000218 RID: 536 RVA: 0x0000B444 File Offset: 0x00009644
		private static IEnumerable<TSource> CreateSkipWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			bool yield = false;
			foreach (TSource element in source)
			{
				if (yield)
				{
					yield return element;
				}
				else if (!predicate(element))
				{
					yield return element;
					yield = true;
				}
			}
			yield break;
		}

		// Token: 0x06000219 RID: 537 RVA: 0x0000B47C File Offset: 0x0000967C
		public static IEnumerable<TSource> SkipWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return Enumerable.CreateSkipWhileIterator<TSource>(source, predicate);
		}

		// Token: 0x0600021A RID: 538 RVA: 0x0000B48C File Offset: 0x0000968C
		private static IEnumerable<TSource> CreateSkipWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			int counter = 0;
			bool yield = false;
			foreach (TSource element in source)
			{
				if (yield)
				{
					yield return element;
				}
				else if (!predicate(element, counter))
				{
					yield return element;
					yield = true;
				}
				counter++;
			}
			yield break;
		}

		// Token: 0x0600021B RID: 539 RVA: 0x0000B4C4 File Offset: 0x000096C4
		public static int Sum(this IEnumerable<int> source)
		{
			Check.Source(source);
			return source.Sum((int a, int b) => checked(a + b));
		}

		// Token: 0x0600021C RID: 540 RVA: 0x0000B4F0 File Offset: 0x000096F0
		public static int? Sum(this IEnumerable<int?> source)
		{
			Check.Source(source);
			return source.SumNullable(new int?(0), (int? total, int? element) => (element == null) ? total : ((total == null || element == null) ? null : new int?(checked(total.Value + element.Value))));
		}

		// Token: 0x0600021D RID: 541 RVA: 0x0000B524 File Offset: 0x00009724
		public static int Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int> selector)
		{
			Check.SourceAndSelector(source, selector);
			int num = 0;
			checked
			{
				foreach (TSource arg in source)
				{
					num += selector(arg);
				}
				return num;
			}
		}

		// Token: 0x0600021E RID: 542 RVA: 0x0000B590 File Offset: 0x00009790
		public static int? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, int?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.SumNullable(new int?(0), delegate(int? a, TSource b)
			{
				int? num = selector(b);
				return (num == null) ? a : ((a == null) ? null : new int?(checked(a.Value + num.Value)));
			});
		}

		// Token: 0x0600021F RID: 543 RVA: 0x0000B5D0 File Offset: 0x000097D0
		public static long Sum(this IEnumerable<long> source)
		{
			Check.Source(source);
			return source.Sum((long a, long b) => checked(a + b));
		}

		// Token: 0x06000220 RID: 544 RVA: 0x0000B5FC File Offset: 0x000097FC
		public static long? Sum(this IEnumerable<long?> source)
		{
			Check.Source(source);
			return source.SumNullable(new long?(0L), (long? total, long? element) => (element == null) ? total : ((total == null || element == null) ? null : new long?(checked(total.Value + element.Value))));
		}

		// Token: 0x06000221 RID: 545 RVA: 0x0000B63C File Offset: 0x0000983C
		public static long Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
		{
			Check.SourceAndSelector(source, selector);
			long num = 0L;
			checked
			{
				foreach (TSource arg in source)
				{
					num += selector(arg);
				}
				return num;
			}
		}

		// Token: 0x06000222 RID: 546 RVA: 0x0000B6A8 File Offset: 0x000098A8
		public static long? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, long?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.SumNullable(new long?(0L), delegate(long? a, TSource b)
			{
				long? num = selector(b);
				return (num == null) ? a : ((a == null) ? null : new long?(checked(a.Value + num.Value)));
			});
		}

		// Token: 0x06000223 RID: 547 RVA: 0x0000B6E8 File Offset: 0x000098E8
		public static double Sum(this IEnumerable<double> source)
		{
			Check.Source(source);
			return source.Sum((double a, double b) => a + b);
		}

		// Token: 0x06000224 RID: 548 RVA: 0x0000B714 File Offset: 0x00009914
		public static double? Sum(this IEnumerable<double?> source)
		{
			Check.Source(source);
			return source.SumNullable(new double?(0.0), (double? total, double? element) => (element == null) ? total : ((total == null || element == null) ? null : new double?(total.Value + element.Value)));
		}

		// Token: 0x06000225 RID: 549 RVA: 0x0000B750 File Offset: 0x00009950
		public static double Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, double> selector)
		{
			Check.SourceAndSelector(source, selector);
			double num = 0.0;
			foreach (TSource arg in source)
			{
				num += selector(arg);
			}
			return num;
		}

		// Token: 0x06000226 RID: 550 RVA: 0x0000B7C4 File Offset: 0x000099C4
		public static double? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, double?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.SumNullable(new double?(0.0), delegate(double? a, TSource b)
			{
				double? num = selector(b);
				return (num == null) ? a : ((a == null) ? null : new double?(a.Value + num.Value));
			});
		}

		// Token: 0x06000227 RID: 551 RVA: 0x0000B80C File Offset: 0x00009A0C
		public static float Sum(this IEnumerable<float> source)
		{
			Check.Source(source);
			return source.Sum((float a, float b) => a + b);
		}

		// Token: 0x06000228 RID: 552 RVA: 0x0000B838 File Offset: 0x00009A38
		public static float? Sum(this IEnumerable<float?> source)
		{
			Check.Source(source);
			return source.SumNullable(new float?(0f), (float? total, float? element) => (element == null) ? total : ((total == null || element == null) ? null : new float?(total.Value + element.Value)));
		}

		// Token: 0x06000229 RID: 553 RVA: 0x0000B870 File Offset: 0x00009A70
		public static float Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, float> selector)
		{
			Check.SourceAndSelector(source, selector);
			float num = 0f;
			foreach (TSource arg in source)
			{
				num += selector(arg);
			}
			return num;
		}

		// Token: 0x0600022A RID: 554 RVA: 0x0000B8E0 File Offset: 0x00009AE0
		public static float? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, float?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.SumNullable(new float?(0f), delegate(float? a, TSource b)
			{
				float? num = selector(b);
				return (num == null) ? a : ((a == null) ? null : new float?(a.Value + num.Value));
			});
		}

		// Token: 0x0600022B RID: 555 RVA: 0x0000B924 File Offset: 0x00009B24
		public static decimal Sum(this IEnumerable<decimal> source)
		{
			Check.Source(source);
			return source.Sum((decimal a, decimal b) => a + b);
		}

		// Token: 0x0600022C RID: 556 RVA: 0x0000B950 File Offset: 0x00009B50
		public static decimal? Sum(this IEnumerable<decimal?> source)
		{
			Check.Source(source);
			return source.SumNullable(new decimal?(0m), (decimal? total, decimal? element) => (element == null) ? total : ((total == null || element == null) ? null : new decimal?(total.Value + element.Value)));
		}

		// Token: 0x0600022D RID: 557 RVA: 0x0000B994 File Offset: 0x00009B94
		public static decimal Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal> selector)
		{
			Check.SourceAndSelector(source, selector);
			decimal num = 0m;
			foreach (TSource arg in source)
			{
				num += selector(arg);
			}
			return num;
		}

		// Token: 0x0600022E RID: 558 RVA: 0x0000BA08 File Offset: 0x00009C08
		public static decimal? Sum<TSource>(this IEnumerable<TSource> source, Func<TSource, decimal?> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.SumNullable(new decimal?(0m), delegate(decimal? a, TSource b)
			{
				decimal? num = selector(b);
				return (num == null) ? a : ((a == null) ? null : new decimal?(a.Value + num.Value));
			});
		}

		// Token: 0x0600022F RID: 559 RVA: 0x0000BA4C File Offset: 0x00009C4C
		private static TR Sum<TA, TR>(this IEnumerable<TA> source, Func<TR, TA, TR> selector)
		{
			TR tr = default(TR);
			long num = 0L;
			foreach (TA arg in source)
			{
				tr = selector(tr, arg);
				num += 1L;
			}
			return tr;
		}

		// Token: 0x06000230 RID: 560 RVA: 0x0000BAC0 File Offset: 0x00009CC0
		private static TR SumNullable<TA, TR>(this IEnumerable<TA> source, TR zero, Func<TR, TA, TR> selector)
		{
			TR tr = zero;
			foreach (TA arg in source)
			{
				tr = selector(tr, arg);
			}
			return tr;
		}

		// Token: 0x06000231 RID: 561 RVA: 0x0000BB24 File Offset: 0x00009D24
		public static IEnumerable<TSource> Take<TSource>(this IEnumerable<TSource> source, int count)
		{
			Check.Source(source);
			return Enumerable.CreateTakeIterator<TSource>(source, count);
		}

		// Token: 0x06000232 RID: 562 RVA: 0x0000BB34 File Offset: 0x00009D34
		private static IEnumerable<TSource> CreateTakeIterator<TSource>(IEnumerable<TSource> source, int count)
		{
			if (count <= 0)
			{
				yield break;
			}
			int counter = 0;
			foreach (TSource element in source)
			{
				yield return element;
				if (++counter == count)
				{
					yield break;
				}
			}
			yield break;
		}

		// Token: 0x06000233 RID: 563 RVA: 0x0000BB6C File Offset: 0x00009D6C
		public static IEnumerable<TSource> TakeWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return Enumerable.CreateTakeWhileIterator<TSource>(source, predicate);
		}

		// Token: 0x06000234 RID: 564 RVA: 0x0000BB7C File Offset: 0x00009D7C
		private static IEnumerable<TSource> CreateTakeWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			foreach (TSource element in source)
			{
				if (!predicate(element))
				{
					yield break;
				}
				yield return element;
			}
			yield break;
		}

		// Token: 0x06000235 RID: 565 RVA: 0x0000BBB4 File Offset: 0x00009DB4
		public static IEnumerable<TSource> TakeWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return Enumerable.CreateTakeWhileIterator<TSource>(source, predicate);
		}

		// Token: 0x06000236 RID: 566 RVA: 0x0000BBC4 File Offset: 0x00009DC4
		private static IEnumerable<TSource> CreateTakeWhileIterator<TSource>(IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			int counter = 0;
			foreach (TSource element in source)
			{
				if (!predicate(element, counter))
				{
					yield break;
				}
				yield return element;
				counter++;
			}
			yield break;
		}

		// Token: 0x06000237 RID: 567 RVA: 0x0000BBFC File Offset: 0x00009DFC
		public static IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return source.ThenBy(keySelector, null);
		}

		// Token: 0x06000238 RID: 568 RVA: 0x0000BC08 File Offset: 0x00009E08
		public static IOrderedEnumerable<TSource> ThenBy<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return source.CreateOrderedEnumerable<TKey>(keySelector, comparer, false);
		}

		// Token: 0x06000239 RID: 569 RVA: 0x0000BC1C File Offset: 0x00009E1C
		public static IOrderedEnumerable<TSource> ThenByDescending<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return source.ThenByDescending(keySelector, null);
		}

		// Token: 0x0600023A RID: 570 RVA: 0x0000BC28 File Offset: 0x00009E28
		public static IOrderedEnumerable<TSource> ThenByDescending<TSource, TKey>(this IOrderedEnumerable<TSource> source, Func<TSource, TKey> keySelector, IComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return source.CreateOrderedEnumerable<TKey>(keySelector, comparer, true);
		}

		// Token: 0x0600023B RID: 571 RVA: 0x0000BC3C File Offset: 0x00009E3C
		public static TSource[] ToArray<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			ICollection<TSource> collection = source as ICollection<TSource>;
			if (collection != null)
			{
				TSource[] array = new TSource[collection.Count];
				collection.CopyTo(array, 0);
				return array;
			}
			return new List<TSource>(source).ToArray();
		}

		// Token: 0x0600023C RID: 572 RVA: 0x0000BC80 File Offset: 0x00009E80
		public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
		{
			return source.ToDictionary(keySelector, elementSelector, null);
		}

		// Token: 0x0600023D RID: 573 RVA: 0x0000BC8C File Offset: 0x00009E8C
		public static Dictionary<TKey, TElement> ToDictionary<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			Check.SourceAndKeyElementSelectors(source, keySelector, elementSelector);
			if (comparer == null)
			{
				comparer = EqualityComparer<TKey>.Default;
			}
			Dictionary<TKey, TElement> dictionary = new Dictionary<TKey, TElement>(comparer);
			foreach (TSource arg in source)
			{
				dictionary.Add(keySelector(arg), elementSelector(arg));
			}
			return dictionary;
		}

		// Token: 0x0600023E RID: 574 RVA: 0x0000BD14 File Offset: 0x00009F14
		public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return source.ToDictionary(keySelector, null);
		}

		// Token: 0x0600023F RID: 575 RVA: 0x0000BD20 File Offset: 0x00009F20
		public static Dictionary<TKey, TSource> ToDictionary<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			return source.ToDictionary(keySelector, Enumerable.Function<TSource>.Identity, comparer);
		}

		// Token: 0x06000240 RID: 576 RVA: 0x0000BD30 File Offset: 0x00009F30
		public static List<TSource> ToList<TSource>(this IEnumerable<TSource> source)
		{
			Check.Source(source);
			return new List<TSource>(source);
		}

		// Token: 0x06000241 RID: 577 RVA: 0x0000BD40 File Offset: 0x00009F40
		public static ILookup<TKey, TSource> ToLookup<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
		{
			return source.ToLookup(keySelector, Enumerable.Function<TSource>.Identity, null);
		}

		// Token: 0x06000242 RID: 578 RVA: 0x0000BD50 File Offset: 0x00009F50
		public static ILookup<TKey, TSource> ToLookup<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, IEqualityComparer<TKey> comparer)
		{
			return source.ToLookup(keySelector, (TSource element) => element, comparer);
		}

		// Token: 0x06000243 RID: 579 RVA: 0x0000BD68 File Offset: 0x00009F68
		public static ILookup<TKey, TElement> ToLookup<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector)
		{
			return source.ToLookup(keySelector, elementSelector, null);
		}

		// Token: 0x06000244 RID: 580 RVA: 0x0000BD74 File Offset: 0x00009F74
		public static ILookup<TKey, TElement> ToLookup<TSource, TKey, TElement>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector, Func<TSource, TElement> elementSelector, IEqualityComparer<TKey> comparer)
		{
			Check.SourceAndKeyElementSelectors(source, keySelector, elementSelector);
			List<TElement> list = null;
			Dictionary<TKey, List<TElement>> dictionary = new Dictionary<TKey, List<TElement>>(comparer ?? EqualityComparer<TKey>.Default);
			foreach (TSource arg in source)
			{
				TKey tkey = keySelector(arg);
				List<TElement> list2;
				if (tkey == null)
				{
					if (list == null)
					{
						list = new List<TElement>();
					}
					list2 = list;
				}
				else if (!dictionary.TryGetValue(tkey, out list2))
				{
					list2 = new List<TElement>();
					dictionary.Add(tkey, list2);
				}
				list2.Add(elementSelector(arg));
			}
			return new Lookup<TKey, TElement>(dictionary, list);
		}

		// Token: 0x06000245 RID: 581 RVA: 0x0000BE48 File Offset: 0x0000A048
		public static bool SequenceEqual<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			return first.SequenceEqual(second, null);
		}

		// Token: 0x06000246 RID: 582 RVA: 0x0000BE54 File Offset: 0x0000A054
		public static bool SequenceEqual<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			Check.FirstAndSecond(first, second);
			if (comparer == null)
			{
				comparer = EqualityComparer<TSource>.Default;
			}
			bool result;
			using (IEnumerator<TSource> enumerator = first.GetEnumerator())
			{
				using (IEnumerator<TSource> enumerator2 = second.GetEnumerator())
				{
					while (enumerator.MoveNext())
					{
						if (!enumerator2.MoveNext())
						{
							return false;
						}
						if (!comparer.Equals(enumerator.Current, enumerator2.Current))
						{
							return false;
						}
					}
					result = !enumerator2.MoveNext();
				}
			}
			return result;
		}

		// Token: 0x06000247 RID: 583 RVA: 0x0000BF2C File Offset: 0x0000A12C
		public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second)
		{
			Check.FirstAndSecond(first, second);
			return first.Union(second, null);
		}

		// Token: 0x06000248 RID: 584 RVA: 0x0000BF40 File Offset: 0x0000A140
		public static IEnumerable<TSource> Union<TSource>(this IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			Check.FirstAndSecond(first, second);
			if (comparer == null)
			{
				comparer = EqualityComparer<TSource>.Default;
			}
			return Enumerable.CreateUnionIterator<TSource>(first, second, comparer);
		}

		// Token: 0x06000249 RID: 585 RVA: 0x0000BF60 File Offset: 0x0000A160
		private static IEnumerable<TSource> CreateUnionIterator<TSource>(IEnumerable<TSource> first, IEnumerable<TSource> second, IEqualityComparer<TSource> comparer)
		{
			HashSet<TSource> items = new HashSet<TSource>(comparer);
			foreach (TSource element in first)
			{
				if (!items.Contains(element))
				{
					items.Add(element);
					yield return element;
				}
			}
			foreach (TSource element2 in second)
			{
				if (!items.Contains(element2, comparer))
				{
					items.Add(element2);
					yield return element2;
				}
			}
			yield break;
		}

		// Token: 0x0600024A RID: 586 RVA: 0x0000BFA8 File Offset: 0x0000A1A8
		public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return Enumerable.CreateWhereIterator<TSource>(source, predicate);
		}

		// Token: 0x0600024B RID: 587 RVA: 0x0000BFB8 File Offset: 0x0000A1B8
		private static IEnumerable<TSource> CreateWhereIterator<TSource>(IEnumerable<TSource> source, Func<TSource, bool> predicate)
		{
			foreach (TSource element in source)
			{
				if (predicate(element))
				{
					yield return element;
				}
			}
			yield break;
		}

		// Token: 0x0600024C RID: 588 RVA: 0x0000BFF0 File Offset: 0x0000A1F0
		public static IEnumerable<TSource> Where<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.CreateWhereIterator(predicate);
		}

		// Token: 0x0600024D RID: 589 RVA: 0x0000C000 File Offset: 0x0000A200
		private static IEnumerable<TSource> CreateWhereIterator<TSource>(this IEnumerable<TSource> source, Func<TSource, int, bool> predicate)
		{
			int counter = 0;
			foreach (TSource element in source)
			{
				if (predicate(element, counter))
				{
					yield return element;
				}
				counter++;
			}
			yield break;
		}

		// Token: 0x0600024E RID: 590 RVA: 0x0000C038 File Offset: 0x0000A238
		internal static ReadOnlyCollection<TSource> ToReadOnlyCollection<TSource>(this IEnumerable<TSource> source)
		{
			if (source == null)
			{
				return Enumerable.ReadOnlyCollectionOf<TSource>.Empty;
			}
			ReadOnlyCollection<TSource> readOnlyCollection = source as ReadOnlyCollection<TSource>;
			if (readOnlyCollection != null)
			{
				return readOnlyCollection;
			}
			return new ReadOnlyCollection<TSource>(source.ToArray<TSource>());
		}

		// Token: 0x0200001C RID: 28
		private enum Fallback
		{
			// Token: 0x04000082 RID: 130
			Default,
			// Token: 0x04000083 RID: 131
			Throw
		}

		// Token: 0x0200001D RID: 29
		private class Function<T>
		{
			// Token: 0x04000084 RID: 132
			public static readonly Func<T, T> Identity = (T t) => t;
		}

		// Token: 0x0200001E RID: 30
		private class ReadOnlyCollectionOf<T>
		{
			// Token: 0x04000086 RID: 134
			public static readonly ReadOnlyCollection<T> Empty = new ReadOnlyCollection<T>(new T[0]);
		}
	}
}
