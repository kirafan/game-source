﻿using System;
using System.Collections;
using System.Linq.Expressions;

namespace System.Linq
{
	// Token: 0x02000028 RID: 40
	public interface IQueryable : IEnumerable
	{
		// Token: 0x17000027 RID: 39
		// (get) Token: 0x060002A0 RID: 672
		Type ElementType { get; }

		// Token: 0x17000028 RID: 40
		// (get) Token: 0x060002A1 RID: 673
		Expression Expression { get; }

		// Token: 0x17000029 RID: 41
		// (get) Token: 0x060002A2 RID: 674
		IQueryProvider Provider { get; }
	}
}
