﻿using System;

namespace System.Linq
{
	// Token: 0x02000031 RID: 49
	internal enum SortDirection
	{
		// Token: 0x04000098 RID: 152
		Ascending,
		// Token: 0x04000099 RID: 153
		Descending
	}
}
