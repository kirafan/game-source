﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x0200002C RID: 44
	internal abstract class OrderedEnumerable<TElement> : IEnumerable, IEnumerable<TElement>, IOrderedEnumerable<TElement>
	{
		// Token: 0x060002AD RID: 685 RVA: 0x0000CCFC File Offset: 0x0000AEFC
		protected OrderedEnumerable(IEnumerable<TElement> source)
		{
			this.source = source;
		}

		// Token: 0x060002AE RID: 686 RVA: 0x0000CD0C File Offset: 0x0000AF0C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x060002AF RID: 687 RVA: 0x0000CD14 File Offset: 0x0000AF14
		public IEnumerator<TElement> GetEnumerator()
		{
			return this.Sort(this.source).GetEnumerator();
		}

		// Token: 0x060002B0 RID: 688
		public abstract SortContext<TElement> CreateContext(SortContext<TElement> current);

		// Token: 0x060002B1 RID: 689
		protected abstract IEnumerable<TElement> Sort(IEnumerable<TElement> source);

		// Token: 0x060002B2 RID: 690 RVA: 0x0000CD28 File Offset: 0x0000AF28
		public IOrderedEnumerable<TElement> CreateOrderedEnumerable<TKey>(Func<TElement, TKey> selector, IComparer<TKey> comparer, bool descending)
		{
			return new OrderedSequence<TElement, TKey>(this, this.source, selector, comparer, (!descending) ? SortDirection.Ascending : SortDirection.Descending);
		}

		// Token: 0x0400008D RID: 141
		private IEnumerable<TElement> source;
	}
}
