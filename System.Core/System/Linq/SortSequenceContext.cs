﻿using System;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x02000032 RID: 50
	internal class SortSequenceContext<TElement, TKey> : SortContext<TElement>
	{
		// Token: 0x06000340 RID: 832 RVA: 0x0000FDFC File Offset: 0x0000DFFC
		public SortSequenceContext(Func<TElement, TKey> selector, IComparer<TKey> comparer, SortDirection direction, SortContext<TElement> child_context) : base(direction, child_context)
		{
			this.selector = selector;
			this.comparer = comparer;
		}

		// Token: 0x06000341 RID: 833 RVA: 0x0000FE18 File Offset: 0x0000E018
		public override void Initialize(TElement[] elements)
		{
			if (this.child_context != null)
			{
				this.child_context.Initialize(elements);
			}
			this.keys = new TKey[elements.Length];
			for (int i = 0; i < this.keys.Length; i++)
			{
				this.keys[i] = this.selector(elements[i]);
			}
		}

		// Token: 0x06000342 RID: 834 RVA: 0x0000FE84 File Offset: 0x0000E084
		public override int Compare(int first_index, int second_index)
		{
			int num = this.comparer.Compare(this.keys[first_index], this.keys[second_index]);
			if (num == 0)
			{
				if (this.child_context != null)
				{
					return this.child_context.Compare(first_index, second_index);
				}
				num = ((this.direction != SortDirection.Descending) ? (first_index - second_index) : (second_index - first_index));
			}
			return (this.direction != SortDirection.Descending) ? num : (-num);
		}

		// Token: 0x0400009A RID: 154
		private Func<TElement, TKey> selector;

		// Token: 0x0400009B RID: 155
		private IComparer<TKey> comparer;

		// Token: 0x0400009C RID: 156
		private TKey[] keys;
	}
}
