﻿using System;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x0200002D RID: 45
	internal class OrderedSequence<TElement, TKey> : OrderedEnumerable<TElement>
	{
		// Token: 0x060002B3 RID: 691 RVA: 0x0000CD48 File Offset: 0x0000AF48
		internal OrderedSequence(IEnumerable<TElement> source, Func<TElement, TKey> key_selector, IComparer<TKey> comparer, SortDirection direction) : base(source)
		{
			this.selector = key_selector;
			this.comparer = (comparer ?? Comparer<TKey>.Default);
			this.direction = direction;
		}

		// Token: 0x060002B4 RID: 692 RVA: 0x0000CD74 File Offset: 0x0000AF74
		internal OrderedSequence(OrderedEnumerable<TElement> parent, IEnumerable<TElement> source, Func<TElement, TKey> keySelector, IComparer<TKey> comparer, SortDirection direction) : this(source, keySelector, comparer, direction)
		{
			this.parent = parent;
		}

		// Token: 0x060002B5 RID: 693 RVA: 0x0000CD8C File Offset: 0x0000AF8C
		public override SortContext<TElement> CreateContext(SortContext<TElement> current)
		{
			SortContext<TElement> sortContext = new SortSequenceContext<TElement, TKey>(this.selector, this.comparer, this.direction, current);
			if (this.parent != null)
			{
				return this.parent.CreateContext(sortContext);
			}
			return sortContext;
		}

		// Token: 0x060002B6 RID: 694 RVA: 0x0000CDCC File Offset: 0x0000AFCC
		protected override IEnumerable<TElement> Sort(IEnumerable<TElement> source)
		{
			return QuickSort<TElement>.Sort(source, this.CreateContext(null));
		}

		// Token: 0x0400008E RID: 142
		private OrderedEnumerable<TElement> parent;

		// Token: 0x0400008F RID: 143
		private Func<TElement, TKey> selector;

		// Token: 0x04000090 RID: 144
		private IComparer<TKey> comparer;

		// Token: 0x04000091 RID: 145
		private SortDirection direction;
	}
}
