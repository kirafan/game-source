﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x02000029 RID: 41
	public interface IQueryable<T> : IEnumerable, IQueryable, IEnumerable<T>
	{
	}
}
