﻿using System;
using System.Collections;

namespace System.Linq
{
	// Token: 0x0200001F RID: 31
	internal interface IQueryableEnumerable : IEnumerable, IQueryable
	{
		// Token: 0x0600027D RID: 637
		IEnumerable GetEnumerable();
	}
}
