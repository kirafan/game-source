﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x0200002B RID: 43
	public interface ILookup<TKey, TElement> : IEnumerable, IEnumerable<IGrouping<TKey, TElement>>
	{
		// Token: 0x1700002C RID: 44
		// (get) Token: 0x060002AA RID: 682
		int Count { get; }

		// Token: 0x1700002D RID: 45
		IEnumerable<TElement> this[TKey key]
		{
			get;
		}

		// Token: 0x060002AC RID: 684
		bool Contains(TKey key);
	}
}
