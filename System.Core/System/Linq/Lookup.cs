﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x0200002A RID: 42
	public class Lookup<TKey, TElement> : IEnumerable, IEnumerable<IGrouping<TKey, TElement>>, ILookup<TKey, TElement>
	{
		// Token: 0x060002A3 RID: 675 RVA: 0x0000CB30 File Offset: 0x0000AD30
		internal Lookup(Dictionary<TKey, List<TElement>> lookup, IEnumerable<TElement> nullKeyElements)
		{
			this.groups = new Dictionary<TKey, IGrouping<TKey, TElement>>(lookup.Comparer);
			foreach (KeyValuePair<TKey, List<TElement>> keyValuePair in lookup)
			{
				this.groups.Add(keyValuePair.Key, new Grouping<TKey, TElement>(keyValuePair.Key, keyValuePair.Value));
			}
			if (nullKeyElements != null)
			{
				this.nullGrouping = new Grouping<TKey, TElement>(default(TKey), nullKeyElements);
			}
		}

		// Token: 0x060002A4 RID: 676 RVA: 0x0000CBE4 File Offset: 0x0000ADE4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x1700002A RID: 42
		// (get) Token: 0x060002A5 RID: 677 RVA: 0x0000CBEC File Offset: 0x0000ADEC
		public int Count
		{
			get
			{
				return (this.nullGrouping != null) ? (this.groups.Count + 1) : this.groups.Count;
			}
		}

		// Token: 0x1700002B RID: 43
		public IEnumerable<TElement> this[TKey key]
		{
			get
			{
				if (key == null && this.nullGrouping != null)
				{
					return this.nullGrouping;
				}
				IGrouping<TKey, TElement> result;
				if (key != null && this.groups.TryGetValue(key, out result))
				{
					return result;
				}
				return new TElement[0];
			}
		}

		// Token: 0x060002A7 RID: 679 RVA: 0x0000CC74 File Offset: 0x0000AE74
		public IEnumerable<TResult> ApplyResultSelector<TResult>(Func<TKey, IEnumerable<TElement>, TResult> selector)
		{
			if (this.nullGrouping != null)
			{
				yield return selector(this.nullGrouping.Key, this.nullGrouping);
			}
			foreach (IGrouping<TKey, TElement> group in this.groups.Values)
			{
				yield return selector(group.Key, group);
			}
			yield break;
		}

		// Token: 0x060002A8 RID: 680 RVA: 0x0000CCA8 File Offset: 0x0000AEA8
		public bool Contains(TKey key)
		{
			return (key == null) ? (this.nullGrouping != null) : this.groups.ContainsKey(key);
		}

		// Token: 0x060002A9 RID: 681 RVA: 0x0000CCE0 File Offset: 0x0000AEE0
		public IEnumerator<IGrouping<TKey, TElement>> GetEnumerator()
		{
			if (this.nullGrouping != null)
			{
				yield return this.nullGrouping;
			}
			foreach (IGrouping<TKey, TElement> g in this.groups.Values)
			{
				yield return g;
			}
			yield break;
		}

		// Token: 0x0400008B RID: 139
		private IGrouping<TKey, TElement> nullGrouping;

		// Token: 0x0400008C RID: 140
		private Dictionary<TKey, IGrouping<TKey, TElement>> groups;
	}
}
