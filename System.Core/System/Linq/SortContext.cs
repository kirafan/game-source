﻿using System;

namespace System.Linq
{
	// Token: 0x02000030 RID: 48
	internal abstract class SortContext<TElement>
	{
		// Token: 0x0600033D RID: 829 RVA: 0x0000FDE4 File Offset: 0x0000DFE4
		protected SortContext(SortDirection direction, SortContext<TElement> child_context)
		{
			this.direction = direction;
			this.child_context = child_context;
		}

		// Token: 0x0600033E RID: 830
		public abstract void Initialize(TElement[] elements);

		// Token: 0x0600033F RID: 831
		public abstract int Compare(int first_index, int second_index);

		// Token: 0x04000095 RID: 149
		protected SortDirection direction;

		// Token: 0x04000096 RID: 150
		protected SortContext<TElement> child_context;
	}
}
