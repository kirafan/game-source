﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x02000024 RID: 36
	public interface IGrouping<TKey, TElement> : IEnumerable, IEnumerable<TElement>
	{
		// Token: 0x17000026 RID: 38
		// (get) Token: 0x0600029E RID: 670
		TKey Key { get; }
	}
}
