﻿using System;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x0200002F RID: 47
	internal class QuickSort<TElement>
	{
		// Token: 0x06000334 RID: 820 RVA: 0x0000FB20 File Offset: 0x0000DD20
		private QuickSort(IEnumerable<TElement> source, SortContext<TElement> context)
		{
			this.elements = source.ToArray<TElement>();
			this.indexes = QuickSort<TElement>.CreateIndexes(this.elements.Length);
			this.context = context;
		}

		// Token: 0x06000335 RID: 821 RVA: 0x0000FB5C File Offset: 0x0000DD5C
		private static int[] CreateIndexes(int length)
		{
			int[] array = new int[length];
			for (int i = 0; i < length; i++)
			{
				array[i] = i;
			}
			return array;
		}

		// Token: 0x06000336 RID: 822 RVA: 0x0000FB88 File Offset: 0x0000DD88
		private void PerformSort()
		{
			if (this.elements.Length <= 1)
			{
				return;
			}
			this.context.Initialize(this.elements);
			this.Sort(0, this.indexes.Length - 1);
		}

		// Token: 0x06000337 RID: 823 RVA: 0x0000FBBC File Offset: 0x0000DDBC
		private int CompareItems(int first_index, int second_index)
		{
			return this.context.Compare(first_index, second_index);
		}

		// Token: 0x06000338 RID: 824 RVA: 0x0000FBCC File Offset: 0x0000DDCC
		private int MedianOfThree(int left, int right)
		{
			int num = (left + right) / 2;
			if (this.CompareItems(this.indexes[num], this.indexes[left]) < 0)
			{
				this.Swap(left, num);
			}
			if (this.CompareItems(this.indexes[right], this.indexes[left]) < 0)
			{
				this.Swap(left, right);
			}
			if (this.CompareItems(this.indexes[right], this.indexes[num]) < 0)
			{
				this.Swap(num, right);
			}
			this.Swap(num, right - 1);
			return this.indexes[right - 1];
		}

		// Token: 0x06000339 RID: 825 RVA: 0x0000FC60 File Offset: 0x0000DE60
		private void Sort(int left, int right)
		{
			if (left + 3 <= right)
			{
				int num = left;
				int num2 = right - 1;
				int second_index = this.MedianOfThree(left, right);
				for (;;)
				{
					while (this.CompareItems(this.indexes[++num], second_index) < 0)
					{
					}
					while (this.CompareItems(this.indexes[--num2], second_index) > 0)
					{
					}
					if (num >= num2)
					{
						break;
					}
					this.Swap(num, num2);
				}
				this.Swap(num, right - 1);
				this.Sort(left, num - 1);
				this.Sort(num + 1, right);
			}
			else
			{
				this.InsertionSort(left, right);
			}
		}

		// Token: 0x0600033A RID: 826 RVA: 0x0000FD0C File Offset: 0x0000DF0C
		private void InsertionSort(int left, int right)
		{
			for (int i = left + 1; i <= right; i++)
			{
				int num = this.indexes[i];
				int num2 = i;
				while (num2 > left && this.CompareItems(num, this.indexes[num2 - 1]) < 0)
				{
					this.indexes[num2] = this.indexes[num2 - 1];
					num2--;
				}
				this.indexes[num2] = num;
			}
		}

		// Token: 0x0600033B RID: 827 RVA: 0x0000FD7C File Offset: 0x0000DF7C
		private void Swap(int left, int right)
		{
			int num = this.indexes[right];
			this.indexes[right] = this.indexes[left];
			this.indexes[left] = num;
		}

		// Token: 0x0600033C RID: 828 RVA: 0x0000FDAC File Offset: 0x0000DFAC
		public static IEnumerable<TElement> Sort(IEnumerable<TElement> source, SortContext<TElement> context)
		{
			QuickSort<TElement> sorter = new QuickSort<TElement>(source, context);
			sorter.PerformSort();
			for (int i = 0; i < sorter.indexes.Length; i++)
			{
				yield return sorter.elements[sorter.indexes[i]];
			}
			yield break;
		}

		// Token: 0x04000092 RID: 146
		private TElement[] elements;

		// Token: 0x04000093 RID: 147
		private int[] indexes;

		// Token: 0x04000094 RID: 148
		private SortContext<TElement> context;
	}
}
