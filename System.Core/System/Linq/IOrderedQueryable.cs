﻿using System;
using System.Collections;

namespace System.Linq
{
	// Token: 0x02000025 RID: 37
	public interface IOrderedQueryable : IEnumerable, IQueryable
	{
	}
}
