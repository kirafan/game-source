﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x02000027 RID: 39
	public interface IOrderedEnumerable<TElement> : IEnumerable, IEnumerable<TElement>
	{
		// Token: 0x0600029F RID: 671
		IOrderedEnumerable<TElement> CreateOrderedEnumerable<TKey>(Func<TElement, TKey> selector, IComparer<TKey> comparer, bool descending);
	}
}
