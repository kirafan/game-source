﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x02000023 RID: 35
	internal class Grouping<K, T> : IEnumerable, IEnumerable<T>, IGrouping<K, T>
	{
		// Token: 0x06000299 RID: 665 RVA: 0x0000CAE4 File Offset: 0x0000ACE4
		public Grouping(K key, IEnumerable<T> group)
		{
			this.group = group;
			this.key = key;
		}

		// Token: 0x0600029A RID: 666 RVA: 0x0000CAFC File Offset: 0x0000ACFC
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.group.GetEnumerator();
		}

		// Token: 0x17000025 RID: 37
		// (get) Token: 0x0600029B RID: 667 RVA: 0x0000CB0C File Offset: 0x0000AD0C
		// (set) Token: 0x0600029C RID: 668 RVA: 0x0000CB14 File Offset: 0x0000AD14
		public K Key
		{
			get
			{
				return this.key;
			}
			set
			{
				this.key = value;
			}
		}

		// Token: 0x0600029D RID: 669 RVA: 0x0000CB20 File Offset: 0x0000AD20
		public IEnumerator<T> GetEnumerator()
		{
			return this.group.GetEnumerator();
		}

		// Token: 0x04000089 RID: 137
		private K key;

		// Token: 0x0400008A RID: 138
		private IEnumerable<T> group;
	}
}
