﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;

namespace System.Linq
{
	// Token: 0x0200002E RID: 46
	public static class Queryable
	{
		// Token: 0x060002B7 RID: 695 RVA: 0x0000CDDC File Offset: 0x0000AFDC
		private static MethodInfo MakeGeneric(MethodBase method, params Type[] parameters)
		{
			return ((MethodInfo)method).MakeGenericMethod(parameters);
		}

		// Token: 0x060002B8 RID: 696 RVA: 0x0000CDEC File Offset: 0x0000AFEC
		private static Expression StaticCall(MethodInfo method, params Expression[] expressions)
		{
			return Expression.Call(null, method, expressions);
		}

		// Token: 0x060002B9 RID: 697 RVA: 0x0000CDF8 File Offset: 0x0000AFF8
		private static TRet Execute<TRet, TSource>(this IQueryable<TSource> source, MethodBase current)
		{
			return source.Provider.Execute<TRet>(Queryable.StaticCall(Queryable.MakeGeneric(current, new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002BA RID: 698 RVA: 0x0000CE40 File Offset: 0x0000B040
		public static TSource Aggregate<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, TSource, TSource>> func)
		{
			Check.SourceAndFunc(source, func);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(func)
			}));
		}

		// Token: 0x060002BB RID: 699 RVA: 0x0000CE9C File Offset: 0x0000B09C
		public static TAccumulate Aggregate<TSource, TAccumulate>(this IQueryable<TSource> source, TAccumulate seed, Expression<Func<TAccumulate, TSource, TAccumulate>> func)
		{
			Check.SourceAndFunc(source, func);
			return source.Provider.Execute<TAccumulate>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TAccumulate)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(seed),
				Expression.Quote(func)
			}));
		}

		// Token: 0x060002BC RID: 700 RVA: 0x0000CF10 File Offset: 0x0000B110
		public static TResult Aggregate<TSource, TAccumulate, TResult>(this IQueryable<TSource> source, TAccumulate seed, Expression<Func<TAccumulate, TSource, TAccumulate>> func, Expression<Func<TAccumulate, TResult>> selector)
		{
			Check.SourceAndFuncAndSelector(source, func, selector);
			return source.Provider.Execute<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TAccumulate),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(seed),
				Expression.Quote(func),
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002BD RID: 701 RVA: 0x0000CF9C File Offset: 0x0000B19C
		public static bool All<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<bool>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x060002BE RID: 702 RVA: 0x0000CFF8 File Offset: 0x0000B1F8
		public static bool Any<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.Execute<bool>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002BF RID: 703 RVA: 0x0000D048 File Offset: 0x0000B248
		public static bool Any<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<bool>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x060002C0 RID: 704 RVA: 0x0000D0A4 File Offset: 0x0000B2A4
		public static IQueryable<TElement> AsQueryable<TElement>(this IEnumerable<TElement> source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			IQueryable<TElement> queryable = source as IQueryable<TElement>;
			if (queryable != null)
			{
				return queryable;
			}
			return new QueryableEnumerable<TElement>(source);
		}

		// Token: 0x060002C1 RID: 705 RVA: 0x0000D0D8 File Offset: 0x0000B2D8
		public static IQueryable AsQueryable(this IEnumerable source)
		{
			if (source == null)
			{
				throw new ArgumentNullException("source");
			}
			IQueryable queryable = source as IQueryable;
			if (queryable != null)
			{
				return queryable;
			}
			Type type = source.GetType();
			if (!type.IsGenericImplementationOf(typeof(IEnumerable<>)))
			{
				throw new ArgumentException("source is not IEnumerable<>");
			}
			return (IQueryable)Activator.CreateInstance(typeof(QueryableEnumerable<>).MakeGenericType(new Type[]
			{
				type.GetFirstGenericArgument()
			}), new object[]
			{
				source
			});
		}

		// Token: 0x060002C2 RID: 706 RVA: 0x0000D160 File Offset: 0x0000B360
		public static double Average(this IQueryable<int> source)
		{
			Check.Source(source);
			return source.Provider.Execute<double>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002C3 RID: 707 RVA: 0x0000D19C File Offset: 0x0000B39C
		public static double? Average(this IQueryable<int?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<double?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002C4 RID: 708 RVA: 0x0000D1D8 File Offset: 0x0000B3D8
		public static double Average(this IQueryable<long> source)
		{
			Check.Source(source);
			return source.Provider.Execute<double>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002C5 RID: 709 RVA: 0x0000D214 File Offset: 0x0000B414
		public static double? Average(this IQueryable<long?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<double?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002C6 RID: 710 RVA: 0x0000D250 File Offset: 0x0000B450
		public static float Average(this IQueryable<float> source)
		{
			Check.Source(source);
			return source.Provider.Execute<float>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002C7 RID: 711 RVA: 0x0000D28C File Offset: 0x0000B48C
		public static float? Average(this IQueryable<float?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<float?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002C8 RID: 712 RVA: 0x0000D2C8 File Offset: 0x0000B4C8
		public static double Average(this IQueryable<double> source)
		{
			Check.Source(source);
			return source.Provider.Execute<double>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002C9 RID: 713 RVA: 0x0000D304 File Offset: 0x0000B504
		public static double? Average(this IQueryable<double?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<double?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002CA RID: 714 RVA: 0x0000D340 File Offset: 0x0000B540
		public static decimal Average(this IQueryable<decimal> source)
		{
			Check.Source(source);
			return source.Provider.Execute<decimal>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002CB RID: 715 RVA: 0x0000D37C File Offset: 0x0000B57C
		public static decimal? Average(this IQueryable<decimal?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<decimal?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002CC RID: 716 RVA: 0x0000D3B8 File Offset: 0x0000B5B8
		public static double Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<double>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002CD RID: 717 RVA: 0x0000D414 File Offset: 0x0000B614
		public static double? Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<double?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002CE RID: 718 RVA: 0x0000D470 File Offset: 0x0000B670
		public static double Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<double>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002CF RID: 719 RVA: 0x0000D4CC File Offset: 0x0000B6CC
		public static double? Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<double?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002D0 RID: 720 RVA: 0x0000D528 File Offset: 0x0000B728
		public static float Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<float>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002D1 RID: 721 RVA: 0x0000D584 File Offset: 0x0000B784
		public static float? Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<float?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002D2 RID: 722 RVA: 0x0000D5E0 File Offset: 0x0000B7E0
		public static double Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<double>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002D3 RID: 723 RVA: 0x0000D63C File Offset: 0x0000B83C
		public static double? Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<double?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002D4 RID: 724 RVA: 0x0000D698 File Offset: 0x0000B898
		public static decimal Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<decimal>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002D5 RID: 725 RVA: 0x0000D6F4 File Offset: 0x0000B8F4
		public static decimal? Average<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<decimal?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002D6 RID: 726 RVA: 0x0000D750 File Offset: 0x0000B950
		public static IQueryable<TResult> Cast<TResult>(this IQueryable source)
		{
			Check.Source(source);
			return (IQueryable<TResult>)source.Provider.CreateQuery(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002D7 RID: 727 RVA: 0x0000D7A4 File Offset: 0x0000B9A4
		public static IQueryable<TSource> Concat<TSource>(this IQueryable<TSource> source1, IEnumerable<TSource> source2)
		{
			Check.Source1AndSource2(source1, source2);
			return source1.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source1.Expression,
				Expression.Constant(source2)
			}));
		}

		// Token: 0x060002D8 RID: 728 RVA: 0x0000D800 File Offset: 0x0000BA00
		public static bool Contains<TSource>(this IQueryable<TSource> source, TSource item)
		{
			Check.Source(source);
			return source.Provider.Execute<bool>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(item)
			}));
		}

		// Token: 0x060002D9 RID: 729 RVA: 0x0000D860 File Offset: 0x0000BA60
		public static bool Contains<TSource>(this IQueryable<TSource> source, TSource item, IEqualityComparer<TSource> comparer)
		{
			Check.Source(source);
			return source.Provider.Execute<bool>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(item),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002DA RID: 730 RVA: 0x0000D8C8 File Offset: 0x0000BAC8
		public static int Count<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Execute(MethodBase.GetCurrentMethod());
		}

		// Token: 0x060002DB RID: 731 RVA: 0x0000D8DC File Offset: 0x0000BADC
		public static int Count<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<int>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x060002DC RID: 732 RVA: 0x0000D938 File Offset: 0x0000BB38
		public static IQueryable<TSource> DefaultIfEmpty<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002DD RID: 733 RVA: 0x0000D988 File Offset: 0x0000BB88
		public static IQueryable<TSource> DefaultIfEmpty<TSource>(this IQueryable<TSource> source, TSource defaultValue)
		{
			Check.Source(source);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(defaultValue)
			}));
		}

		// Token: 0x060002DE RID: 734 RVA: 0x0000D9E8 File Offset: 0x0000BBE8
		public static IQueryable<TSource> Distinct<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002DF RID: 735 RVA: 0x0000DA38 File Offset: 0x0000BC38
		public static IQueryable<TSource> Distinct<TSource>(this IQueryable<TSource> source, IEqualityComparer<TSource> comparer)
		{
			Check.Source(source);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002E0 RID: 736 RVA: 0x0000DA90 File Offset: 0x0000BC90
		public static TSource ElementAt<TSource>(this IQueryable<TSource> source, int index)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(index)
			}));
		}

		// Token: 0x060002E1 RID: 737 RVA: 0x0000DAF0 File Offset: 0x0000BCF0
		public static TSource ElementAtOrDefault<TSource>(this IQueryable<TSource> source, int index)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(index)
			}));
		}

		// Token: 0x060002E2 RID: 738 RVA: 0x0000DB50 File Offset: 0x0000BD50
		public static IQueryable<TSource> Except<TSource>(this IQueryable<TSource> source1, IEnumerable<TSource> source2)
		{
			Check.Source1AndSource2(source1, source2);
			return source1.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source1.Expression,
				Expression.Constant(source2)
			}));
		}

		// Token: 0x060002E3 RID: 739 RVA: 0x0000DBAC File Offset: 0x0000BDAC
		public static IQueryable<TSource> Except<TSource>(this IQueryable<TSource> source1, IEnumerable<TSource> source2, IEqualityComparer<TSource> comparer)
		{
			Check.Source1AndSource2(source1, source2);
			return source1.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source1.Expression,
				Expression.Constant(source2),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002E4 RID: 740 RVA: 0x0000DC10 File Offset: 0x0000BE10
		public static TSource First<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002E5 RID: 741 RVA: 0x0000DC60 File Offset: 0x0000BE60
		public static TSource First<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x060002E6 RID: 742 RVA: 0x0000DCBC File Offset: 0x0000BEBC
		public static TSource FirstOrDefault<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002E7 RID: 743 RVA: 0x0000DD0C File Offset: 0x0000BF0C
		public static TSource FirstOrDefault<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x060002E8 RID: 744 RVA: 0x0000DD68 File Offset: 0x0000BF68
		public static IQueryable<IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return source.Provider.CreateQuery<IGrouping<TKey, TSource>>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector)
			}));
		}

		// Token: 0x060002E9 RID: 745 RVA: 0x0000DDD0 File Offset: 0x0000BFD0
		public static IQueryable<IGrouping<TKey, TSource>> GroupBy<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IEqualityComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return source.Provider.CreateQuery<IGrouping<TKey, TSource>>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002EA RID: 746 RVA: 0x0000DE40 File Offset: 0x0000C040
		public static IQueryable<IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TSource, TElement>> elementSelector)
		{
			Check.SourceAndKeyElementSelectors(source, keySelector, elementSelector);
			return source.Provider.CreateQuery<IGrouping<TKey, TElement>>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey),
				typeof(TElement)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Quote(elementSelector)
			}));
		}

		// Token: 0x060002EB RID: 747 RVA: 0x0000DEC0 File Offset: 0x0000C0C0
		public static IQueryable<TResult> GroupBy<TSource, TKey, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TKey, IEnumerable<TSource>, TResult>> resultSelector)
		{
			Check.SourceAndKeyResultSelectors(source, keySelector, resultSelector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Quote(resultSelector)
			}));
		}

		// Token: 0x060002EC RID: 748 RVA: 0x0000DF40 File Offset: 0x0000C140
		public static IQueryable<IGrouping<TKey, TElement>> GroupBy<TSource, TKey, TElement>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TSource, TElement>> elementSelector, IEqualityComparer<TKey> comparer)
		{
			Check.SourceAndKeyElementSelectors(source, keySelector, elementSelector);
			return source.Provider.CreateQuery<IGrouping<TKey, TElement>>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey),
				typeof(TElement)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Quote(elementSelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002ED RID: 749 RVA: 0x0000DFC8 File Offset: 0x0000C1C8
		public static IQueryable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TSource, TElement>> elementSelector, Expression<Func<TKey, IEnumerable<TElement>, TResult>> resultSelector)
		{
			Check.GroupBySelectors(source, keySelector, elementSelector, resultSelector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey),
				typeof(TElement),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Quote(elementSelector),
				Expression.Quote(resultSelector)
			}));
		}

		// Token: 0x060002EE RID: 750 RVA: 0x0000E05C File Offset: 0x0000C25C
		public static IQueryable<TResult> GroupBy<TSource, TKey, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TKey, IEnumerable<TSource>, TResult>> resultSelector, IEqualityComparer<TKey> comparer)
		{
			Check.SourceAndKeyResultSelectors(source, keySelector, resultSelector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Quote(resultSelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002EF RID: 751 RVA: 0x0000E0E4 File Offset: 0x0000C2E4
		public static IQueryable<TResult> GroupBy<TSource, TKey, TElement, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, Expression<Func<TSource, TElement>> elementSelector, Expression<Func<TKey, IEnumerable<TElement>, TResult>> resultSelector, IEqualityComparer<TKey> comparer)
		{
			Check.GroupBySelectors(source, keySelector, elementSelector, resultSelector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey),
				typeof(TElement),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Quote(elementSelector),
				Expression.Quote(resultSelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002F0 RID: 752 RVA: 0x0000E184 File Offset: 0x0000C384
		public static IQueryable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IQueryable<TOuter> outer, IEnumerable<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector, Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, IEnumerable<TInner>, TResult>> resultSelector)
		{
			if (outer == null)
			{
				throw new ArgumentNullException("outer");
			}
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (outerKeySelector == null)
			{
				throw new ArgumentNullException("outerKeySelector");
			}
			if (innerKeySelector == null)
			{
				throw new ArgumentNullException("innerKeySelector");
			}
			if (resultSelector == null)
			{
				throw new ArgumentNullException("resultSelector");
			}
			return outer.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TOuter),
				typeof(TInner),
				typeof(TKey),
				typeof(TResult)
			}), new Expression[]
			{
				outer.Expression,
				Expression.Constant(inner),
				Expression.Quote(outerKeySelector),
				Expression.Quote(innerKeySelector),
				Expression.Quote(resultSelector)
			}));
		}

		// Token: 0x060002F1 RID: 753 RVA: 0x0000E270 File Offset: 0x0000C470
		public static IQueryable<TResult> GroupJoin<TOuter, TInner, TKey, TResult>(this IQueryable<TOuter> outer, IEnumerable<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector, Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, IEnumerable<TInner>, TResult>> resultSelector, IEqualityComparer<TKey> comparer)
		{
			if (outer == null)
			{
				throw new ArgumentNullException("outer");
			}
			if (inner == null)
			{
				throw new ArgumentNullException("inner");
			}
			if (outerKeySelector == null)
			{
				throw new ArgumentNullException("outerKeySelector");
			}
			if (innerKeySelector == null)
			{
				throw new ArgumentNullException("innerKeySelector");
			}
			if (resultSelector == null)
			{
				throw new ArgumentNullException("resultSelector");
			}
			return outer.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TOuter),
				typeof(TInner),
				typeof(TKey),
				typeof(TResult)
			}), new Expression[]
			{
				outer.Expression,
				Expression.Constant(inner),
				Expression.Quote(outerKeySelector),
				Expression.Quote(innerKeySelector),
				Expression.Quote(resultSelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002F2 RID: 754 RVA: 0x0000E368 File Offset: 0x0000C568
		public static IQueryable<TSource> Intersect<TSource>(this IQueryable<TSource> source1, IEnumerable<TSource> source2)
		{
			Check.Source1AndSource2(source1, source2);
			return source1.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source1.Expression,
				Expression.Constant(source2)
			}));
		}

		// Token: 0x060002F3 RID: 755 RVA: 0x0000E3C4 File Offset: 0x0000C5C4
		public static IQueryable<TSource> Intersect<TSource>(this IQueryable<TSource> source1, IEnumerable<TSource> source2, IEqualityComparer<TSource> comparer)
		{
			Check.Source1AndSource2(source1, source2);
			return source1.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source1.Expression,
				Expression.Constant(source2),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002F4 RID: 756 RVA: 0x0000E428 File Offset: 0x0000C628
		public static IQueryable<TResult> Join<TOuter, TInner, TKey, TResult>(this IQueryable<TOuter> outer, IEnumerable<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector, Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, TInner, TResult>> resultSelector)
		{
			Check.JoinSelectors(outer, inner, outerKeySelector, innerKeySelector, resultSelector);
			return outer.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TOuter),
				typeof(TInner),
				typeof(TKey),
				typeof(TResult)
			}), new Expression[]
			{
				outer.Expression,
				Expression.Constant(inner),
				Expression.Quote(outerKeySelector),
				Expression.Quote(innerKeySelector),
				Expression.Quote(resultSelector)
			}));
		}

		// Token: 0x060002F5 RID: 757 RVA: 0x0000E4C8 File Offset: 0x0000C6C8
		public static IQueryable<TResult> Join<TOuter, TInner, TKey, TResult>(this IQueryable<TOuter> outer, IEnumerable<TInner> inner, Expression<Func<TOuter, TKey>> outerKeySelector, Expression<Func<TInner, TKey>> innerKeySelector, Expression<Func<TOuter, TInner, TResult>> resultSelector, IEqualityComparer<TKey> comparer)
		{
			Check.JoinSelectors(outer, inner, outerKeySelector, innerKeySelector, resultSelector);
			return outer.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TOuter),
				typeof(TInner),
				typeof(TKey),
				typeof(TResult)
			}), new Expression[]
			{
				outer.Expression,
				Expression.Constant(inner),
				Expression.Quote(outerKeySelector),
				Expression.Quote(innerKeySelector),
				Expression.Quote(resultSelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x060002F6 RID: 758 RVA: 0x0000E574 File Offset: 0x0000C774
		public static TSource Last<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002F7 RID: 759 RVA: 0x0000E5C4 File Offset: 0x0000C7C4
		public static TSource Last<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x060002F8 RID: 760 RVA: 0x0000E620 File Offset: 0x0000C820
		public static TSource LastOrDefault<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002F9 RID: 761 RVA: 0x0000E670 File Offset: 0x0000C870
		public static TSource LastOrDefault<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x060002FA RID: 762 RVA: 0x0000E6CC File Offset: 0x0000C8CC
		public static long LongCount<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Execute(MethodBase.GetCurrentMethod());
		}

		// Token: 0x060002FB RID: 763 RVA: 0x0000E6E0 File Offset: 0x0000C8E0
		public static long LongCount<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<long>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x060002FC RID: 764 RVA: 0x0000E73C File Offset: 0x0000C93C
		public static TSource Max<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002FD RID: 765 RVA: 0x0000E78C File Offset: 0x0000C98C
		public static TResult Max<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
		{
			Check.Source(source);
			return source.Provider.Execute<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x060002FE RID: 766 RVA: 0x0000E7F4 File Offset: 0x0000C9F4
		public static TSource Min<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x060002FF RID: 767 RVA: 0x0000E844 File Offset: 0x0000CA44
		public static TResult Min<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x06000300 RID: 768 RVA: 0x0000E8AC File Offset: 0x0000CAAC
		public static IQueryable<TResult> OfType<TResult>(this IQueryable source)
		{
			Check.Source(source);
			return (IQueryable<TResult>)source.Provider.CreateQuery(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000301 RID: 769 RVA: 0x0000E900 File Offset: 0x0000CB00
		public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return (IOrderedQueryable<TSource>)source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector)
			}));
		}

		// Token: 0x06000302 RID: 770 RVA: 0x0000E96C File Offset: 0x0000CB6C
		public static IOrderedQueryable<TSource> OrderBy<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return (IOrderedQueryable<TSource>)source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x06000303 RID: 771 RVA: 0x0000E9E0 File Offset: 0x0000CBE0
		public static IOrderedQueryable<TSource> OrderByDescending<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return (IOrderedQueryable<TSource>)source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector)
			}));
		}

		// Token: 0x06000304 RID: 772 RVA: 0x0000EA4C File Offset: 0x0000CC4C
		public static IOrderedQueryable<TSource> OrderByDescending<TSource, TKey>(this IQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return (IOrderedQueryable<TSource>)source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x06000305 RID: 773 RVA: 0x0000EAC0 File Offset: 0x0000CCC0
		public static IQueryable<TSource> Reverse<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000306 RID: 774 RVA: 0x0000EB10 File Offset: 0x0000CD10
		public static IQueryable<TResult> Select<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, TResult>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x06000307 RID: 775 RVA: 0x0000EB78 File Offset: 0x0000CD78
		public static IQueryable<TResult> Select<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, int, TResult>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x06000308 RID: 776 RVA: 0x0000EBE0 File Offset: 0x0000CDE0
		public static IQueryable<TResult> SelectMany<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, IEnumerable<TResult>>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x06000309 RID: 777 RVA: 0x0000EC48 File Offset: 0x0000CE48
		public static IQueryable<TResult> SelectMany<TSource, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, int, IEnumerable<TResult>>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x0600030A RID: 778 RVA: 0x0000ECB0 File Offset: 0x0000CEB0
		public static IQueryable<TResult> SelectMany<TSource, TCollection, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, int, IEnumerable<TCollection>>> collectionSelector, Expression<Func<TSource, TCollection, TResult>> resultSelector)
		{
			Check.SourceAndCollectionSelectorAndResultSelector(source, collectionSelector, resultSelector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TCollection),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(collectionSelector),
				Expression.Quote(resultSelector)
			}));
		}

		// Token: 0x0600030B RID: 779 RVA: 0x0000ED30 File Offset: 0x0000CF30
		public static IQueryable<TResult> SelectMany<TSource, TCollection, TResult>(this IQueryable<TSource> source, Expression<Func<TSource, IEnumerable<TCollection>>> collectionSelector, Expression<Func<TSource, TCollection, TResult>> resultSelector)
		{
			Check.SourceAndCollectionSelectorAndResultSelector(source, collectionSelector, resultSelector);
			return source.Provider.CreateQuery<TResult>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TCollection),
				typeof(TResult)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(collectionSelector),
				Expression.Quote(resultSelector)
			}));
		}

		// Token: 0x0600030C RID: 780 RVA: 0x0000EDB0 File Offset: 0x0000CFB0
		public static bool SequenceEqual<TSource>(this IQueryable<TSource> source1, IEnumerable<TSource> source2)
		{
			Check.Source1AndSource2(source1, source2);
			return source1.Provider.Execute<bool>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source1.Expression,
				Expression.Constant(source2)
			}));
		}

		// Token: 0x0600030D RID: 781 RVA: 0x0000EE0C File Offset: 0x0000D00C
		public static bool SequenceEqual<TSource>(this IQueryable<TSource> source1, IEnumerable<TSource> source2, IEqualityComparer<TSource> comparer)
		{
			Check.Source1AndSource2(source1, source2);
			return source1.Provider.Execute<bool>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source1.Expression,
				Expression.Constant(source2),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x0600030E RID: 782 RVA: 0x0000EE70 File Offset: 0x0000D070
		public static TSource Single<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x0600030F RID: 783 RVA: 0x0000EEC0 File Offset: 0x0000D0C0
		public static TSource Single<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x06000310 RID: 784 RVA: 0x0000EF1C File Offset: 0x0000D11C
		public static TSource SingleOrDefault<TSource>(this IQueryable<TSource> source)
		{
			Check.Source(source);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000311 RID: 785 RVA: 0x0000EF6C File Offset: 0x0000D16C
		public static TSource SingleOrDefault<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.Execute<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x06000312 RID: 786 RVA: 0x0000EFC8 File Offset: 0x0000D1C8
		public static IQueryable<TSource> Skip<TSource>(this IQueryable<TSource> source, int count)
		{
			Check.Source(source);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(count)
			}));
		}

		// Token: 0x06000313 RID: 787 RVA: 0x0000F028 File Offset: 0x0000D228
		public static IQueryable<TSource> SkipWhile<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x06000314 RID: 788 RVA: 0x0000F084 File Offset: 0x0000D284
		public static IQueryable<TSource> SkipWhile<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x06000315 RID: 789 RVA: 0x0000F0E0 File Offset: 0x0000D2E0
		public static int Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<int>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x06000316 RID: 790 RVA: 0x0000F13C File Offset: 0x0000D33C
		public static int? Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<int?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x06000317 RID: 791 RVA: 0x0000F198 File Offset: 0x0000D398
		public static long Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<long>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x06000318 RID: 792 RVA: 0x0000F1F4 File Offset: 0x0000D3F4
		public static long? Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, long?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<long?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x06000319 RID: 793 RVA: 0x0000F250 File Offset: 0x0000D450
		public static float Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<float>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x0600031A RID: 794 RVA: 0x0000F2AC File Offset: 0x0000D4AC
		public static float? Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, float?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<float?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x0600031B RID: 795 RVA: 0x0000F308 File Offset: 0x0000D508
		public static double Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<double>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x0600031C RID: 796 RVA: 0x0000F364 File Offset: 0x0000D564
		public static double? Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, double?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<double?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x0600031D RID: 797 RVA: 0x0000F3C0 File Offset: 0x0000D5C0
		public static decimal Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<decimal>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x0600031E RID: 798 RVA: 0x0000F41C File Offset: 0x0000D61C
		public static decimal? Sum<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, decimal?>> selector)
		{
			Check.SourceAndSelector(source, selector);
			return source.Provider.Execute<decimal?>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(selector)
			}));
		}

		// Token: 0x0600031F RID: 799 RVA: 0x0000F478 File Offset: 0x0000D678
		public static int Sum(this IQueryable<int> source)
		{
			Check.Source(source);
			return source.Provider.Execute<int>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000320 RID: 800 RVA: 0x0000F4B4 File Offset: 0x0000D6B4
		public static int? Sum(this IQueryable<int?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<int?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000321 RID: 801 RVA: 0x0000F4F0 File Offset: 0x0000D6F0
		public static long Sum(this IQueryable<long> source)
		{
			Check.Source(source);
			return source.Provider.Execute<long>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000322 RID: 802 RVA: 0x0000F52C File Offset: 0x0000D72C
		public static long? Sum(this IQueryable<long?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<long?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000323 RID: 803 RVA: 0x0000F568 File Offset: 0x0000D768
		public static float Sum(this IQueryable<float> source)
		{
			Check.Source(source);
			return source.Provider.Execute<float>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000324 RID: 804 RVA: 0x0000F5A4 File Offset: 0x0000D7A4
		public static float? Sum(this IQueryable<float?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<float?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000325 RID: 805 RVA: 0x0000F5E0 File Offset: 0x0000D7E0
		public static double Sum(this IQueryable<double> source)
		{
			Check.Source(source);
			return source.Provider.Execute<double>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000326 RID: 806 RVA: 0x0000F61C File Offset: 0x0000D81C
		public static double? Sum(this IQueryable<double?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<double?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000327 RID: 807 RVA: 0x0000F658 File Offset: 0x0000D858
		public static decimal Sum(this IQueryable<decimal> source)
		{
			Check.Source(source);
			return source.Provider.Execute<decimal>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000328 RID: 808 RVA: 0x0000F694 File Offset: 0x0000D894
		public static decimal? Sum(this IQueryable<decimal?> source)
		{
			Check.Source(source);
			return source.Provider.Execute<decimal?>(Queryable.StaticCall((MethodInfo)MethodBase.GetCurrentMethod(), new Expression[]
			{
				source.Expression
			}));
		}

		// Token: 0x06000329 RID: 809 RVA: 0x0000F6D0 File Offset: 0x0000D8D0
		public static IQueryable<TSource> Take<TSource>(this IQueryable<TSource> source, int count)
		{
			Check.Source(source);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Constant(count)
			}));
		}

		// Token: 0x0600032A RID: 810 RVA: 0x0000F730 File Offset: 0x0000D930
		public static IQueryable<TSource> TakeWhile<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x0600032B RID: 811 RVA: 0x0000F78C File Offset: 0x0000D98C
		public static IQueryable<TSource> TakeWhile<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x0600032C RID: 812 RVA: 0x0000F7E8 File Offset: 0x0000D9E8
		public static IOrderedQueryable<TSource> ThenBy<TSource, TKey>(this IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return (IOrderedQueryable<TSource>)source.Provider.CreateQuery(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector)
			}));
		}

		// Token: 0x0600032D RID: 813 RVA: 0x0000F854 File Offset: 0x0000DA54
		public static IOrderedQueryable<TSource> ThenBy<TSource, TKey>(this IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return (IOrderedQueryable<TSource>)source.Provider.CreateQuery(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x0600032E RID: 814 RVA: 0x0000F8C8 File Offset: 0x0000DAC8
		public static IOrderedQueryable<TSource> ThenByDescending<TSource, TKey>(this IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return (IOrderedQueryable<TSource>)source.Provider.CreateQuery(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector)
			}));
		}

		// Token: 0x0600032F RID: 815 RVA: 0x0000F934 File Offset: 0x0000DB34
		public static IOrderedQueryable<TSource> ThenByDescending<TSource, TKey>(this IOrderedQueryable<TSource> source, Expression<Func<TSource, TKey>> keySelector, IComparer<TKey> comparer)
		{
			Check.SourceAndKeySelector(source, keySelector);
			return (IOrderedQueryable<TSource>)source.Provider.CreateQuery(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource),
				typeof(TKey)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(keySelector),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x06000330 RID: 816 RVA: 0x0000F9A8 File Offset: 0x0000DBA8
		public static IQueryable<TSource> Union<TSource>(this IQueryable<TSource> source1, IEnumerable<TSource> source2)
		{
			Check.Source1AndSource2(source1, source2);
			return source1.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source1.Expression,
				Expression.Constant(source2)
			}));
		}

		// Token: 0x06000331 RID: 817 RVA: 0x0000FA04 File Offset: 0x0000DC04
		public static IQueryable<TSource> Union<TSource>(this IQueryable<TSource> source1, IEnumerable<TSource> source2, IEqualityComparer<TSource> comparer)
		{
			Check.Source1AndSource2(source1, source2);
			return source1.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source1.Expression,
				Expression.Constant(source2),
				Expression.Constant(comparer)
			}));
		}

		// Token: 0x06000332 RID: 818 RVA: 0x0000FA68 File Offset: 0x0000DC68
		public static IQueryable<TSource> Where<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}

		// Token: 0x06000333 RID: 819 RVA: 0x0000FAC4 File Offset: 0x0000DCC4
		public static IQueryable<TSource> Where<TSource>(this IQueryable<TSource> source, Expression<Func<TSource, int, bool>> predicate)
		{
			Check.SourceAndPredicate(source, predicate);
			return source.Provider.CreateQuery<TSource>(Queryable.StaticCall(Queryable.MakeGeneric(MethodBase.GetCurrentMethod(), new Type[]
			{
				typeof(TSource)
			}), new Expression[]
			{
				source.Expression,
				Expression.Quote(predicate)
			}));
		}
	}
}
