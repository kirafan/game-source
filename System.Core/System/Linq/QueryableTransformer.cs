﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq
{
	// Token: 0x02000022 RID: 34
	internal class QueryableTransformer : ExpressionTransformer
	{
		// Token: 0x0600028C RID: 652 RVA: 0x0000C700 File Offset: 0x0000A900
		protected override Expression VisitMethodCall(MethodCallExpression methodCall)
		{
			if (QueryableTransformer.IsQueryableExtension(methodCall.Method))
			{
				return this.ReplaceQueryableMethod(methodCall);
			}
			return base.VisitMethodCall(methodCall);
		}

		// Token: 0x0600028D RID: 653 RVA: 0x0000C72C File Offset: 0x0000A92C
		protected override Expression VisitLambda(LambdaExpression lambda)
		{
			return lambda;
		}

		// Token: 0x0600028E RID: 654 RVA: 0x0000C730 File Offset: 0x0000A930
		protected override Expression VisitConstant(ConstantExpression constant)
		{
			IQueryableEnumerable queryableEnumerable = constant.Value as IQueryableEnumerable;
			if (queryableEnumerable == null)
			{
				return constant;
			}
			return Expression.Constant(queryableEnumerable.GetEnumerable());
		}

		// Token: 0x0600028F RID: 655 RVA: 0x0000C75C File Offset: 0x0000A95C
		private static bool IsQueryableExtension(MethodInfo method)
		{
			return QueryableTransformer.HasExtensionAttribute(method) && method.GetParameters()[0].ParameterType.IsAssignableTo(typeof(IQueryable));
		}

		// Token: 0x06000290 RID: 656 RVA: 0x0000C794 File Offset: 0x0000A994
		private static bool HasExtensionAttribute(MethodInfo method)
		{
			return method.GetCustomAttributes(typeof(ExtensionAttribute), false).Length > 0;
		}

		// Token: 0x06000291 RID: 657 RVA: 0x0000C7AC File Offset: 0x0000A9AC
		private MethodCallExpression ReplaceQueryableMethod(MethodCallExpression old)
		{
			Expression obj = null;
			if (old.Object != null)
			{
				obj = this.Visit(old.Object);
			}
			MethodInfo methodInfo = QueryableTransformer.ReplaceQueryableMethod(old.Method);
			ParameterInfo[] parameters = methodInfo.GetParameters();
			Expression[] array = new Expression[old.Arguments.Count];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = QueryableTransformer.UnquoteIfNeeded(this.Visit(old.Arguments[i]), parameters[i].ParameterType);
			}
			return new MethodCallExpression(obj, methodInfo, array.ToReadOnlyCollection<Expression>());
		}

		// Token: 0x06000292 RID: 658 RVA: 0x0000C844 File Offset: 0x0000AA44
		private static Expression UnquoteIfNeeded(Expression expression, Type delegateType)
		{
			if (expression.NodeType != ExpressionType.Quote)
			{
				return expression;
			}
			LambdaExpression lambdaExpression = (LambdaExpression)((UnaryExpression)expression).Operand;
			if (lambdaExpression.Type == delegateType)
			{
				return lambdaExpression;
			}
			return expression;
		}

		// Token: 0x06000293 RID: 659 RVA: 0x0000C880 File Offset: 0x0000AA80
		private static Type GetTargetDeclaringType(MethodInfo method)
		{
			return (method.DeclaringType != typeof(Queryable)) ? method.DeclaringType : typeof(Enumerable);
		}

		// Token: 0x06000294 RID: 660 RVA: 0x0000C8B8 File Offset: 0x0000AAB8
		private static MethodInfo ReplaceQueryableMethod(MethodInfo method)
		{
			MethodInfo matchingMethod = QueryableTransformer.GetMatchingMethod(method, QueryableTransformer.GetTargetDeclaringType(method));
			if (matchingMethod != null)
			{
				return matchingMethod;
			}
			throw new InvalidOperationException(string.Format("There is no method {0} on type {1} that matches the specified arguments", method.Name, method.DeclaringType.FullName));
		}

		// Token: 0x06000295 RID: 661 RVA: 0x0000C8FC File Offset: 0x0000AAFC
		private static MethodInfo GetMatchingMethod(MethodInfo method, Type declaring)
		{
			MethodInfo[] methods = declaring.GetMethods();
			int i = 0;
			while (i < methods.Length)
			{
				MethodInfo methodInfo = methods[i];
				if (!QueryableTransformer.MethodMatch(methodInfo, method))
				{
					i++;
				}
				else
				{
					if (method.IsGenericMethod)
					{
						return methodInfo.MakeGenericMethodFrom(method);
					}
					return methodInfo;
				}
			}
			return null;
		}

		// Token: 0x06000296 RID: 662 RVA: 0x0000C950 File Offset: 0x0000AB50
		private static bool MethodMatch(MethodInfo candidate, MethodInfo method)
		{
			if (candidate.Name != method.Name)
			{
				return false;
			}
			if (!QueryableTransformer.HasExtensionAttribute(candidate))
			{
				return false;
			}
			Type[] parameterTypes = method.GetParameterTypes();
			if (parameterTypes.Length != candidate.GetParameters().Length)
			{
				return false;
			}
			if (method.IsGenericMethod)
			{
				if (!candidate.IsGenericMethod)
				{
					return false;
				}
				if (candidate.GetGenericArguments().Length != method.GetGenericArguments().Length)
				{
					return false;
				}
				candidate = candidate.MakeGenericMethodFrom(method);
			}
			if (!QueryableTransformer.TypeMatch(candidate.ReturnType, method.ReturnType))
			{
				return false;
			}
			Type[] parameterTypes2 = candidate.GetParameterTypes();
			if (parameterTypes2[0] != QueryableTransformer.GetComparableType(parameterTypes[0]))
			{
				return false;
			}
			for (int i = 1; i < parameterTypes2.Length; i++)
			{
				if (!QueryableTransformer.TypeMatch(parameterTypes2[i], parameterTypes[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06000297 RID: 663 RVA: 0x0000CA2C File Offset: 0x0000AC2C
		private static bool TypeMatch(Type candidate, Type type)
		{
			return candidate == type || candidate == QueryableTransformer.GetComparableType(type);
		}

		// Token: 0x06000298 RID: 664 RVA: 0x0000CA40 File Offset: 0x0000AC40
		private static Type GetComparableType(Type type)
		{
			if (type.IsGenericInstanceOf(typeof(IQueryable<>)))
			{
				type = typeof(IEnumerable<>).MakeGenericTypeFrom(type);
			}
			else if (type.IsGenericInstanceOf(typeof(IOrderedQueryable<>)))
			{
				type = typeof(IOrderedEnumerable<>).MakeGenericTypeFrom(type);
			}
			else if (type.IsGenericInstanceOf(typeof(Expression<>)))
			{
				type = type.GetFirstGenericArgument();
			}
			else if (type == typeof(IQueryable))
			{
				type = typeof(IEnumerable);
			}
			return type;
		}
	}
}
