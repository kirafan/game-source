﻿using System;
using System.Linq.Expressions;

namespace System.Linq
{
	// Token: 0x02000033 RID: 51
	public interface IQueryProvider
	{
		// Token: 0x06000343 RID: 835
		IQueryable CreateQuery(Expression expression);

		// Token: 0x06000344 RID: 836
		object Execute(Expression expression);

		// Token: 0x06000345 RID: 837
		IQueryable<TElement> CreateQuery<TElement>(Expression expression);

		// Token: 0x06000346 RID: 838
		TResult Execute<TResult>(Expression expression);
	}
}
