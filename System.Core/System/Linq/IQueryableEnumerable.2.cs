﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace System.Linq
{
	// Token: 0x02000020 RID: 32
	internal interface IQueryableEnumerable<TElement> : IQueryableEnumerable, IEnumerable, IOrderedQueryable, IQueryable, IQueryable<TElement>, IEnumerable<TElement>, IOrderedQueryable<TElement>
	{
	}
}
