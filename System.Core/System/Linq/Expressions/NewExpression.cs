﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x0200004C RID: 76
	public sealed class NewExpression : Expression
	{
		// Token: 0x06000457 RID: 1111 RVA: 0x00014634 File Offset: 0x00012834
		internal NewExpression(Type type, ReadOnlyCollection<Expression> arguments) : base(ExpressionType.New, type)
		{
			this.arguments = arguments;
		}

		// Token: 0x06000458 RID: 1112 RVA: 0x00014648 File Offset: 0x00012848
		internal NewExpression(ConstructorInfo constructor, ReadOnlyCollection<Expression> arguments, ReadOnlyCollection<MemberInfo> members) : base(ExpressionType.New, constructor.DeclaringType)
		{
			this.constructor = constructor;
			this.arguments = arguments;
			this.members = members;
		}

		// Token: 0x1700004D RID: 77
		// (get) Token: 0x06000459 RID: 1113 RVA: 0x00014670 File Offset: 0x00012870
		public ConstructorInfo Constructor
		{
			get
			{
				return this.constructor;
			}
		}

		// Token: 0x1700004E RID: 78
		// (get) Token: 0x0600045A RID: 1114 RVA: 0x00014678 File Offset: 0x00012878
		public ReadOnlyCollection<Expression> Arguments
		{
			get
			{
				return this.arguments;
			}
		}

		// Token: 0x1700004F RID: 79
		// (get) Token: 0x0600045B RID: 1115 RVA: 0x00014680 File Offset: 0x00012880
		public ReadOnlyCollection<MemberInfo> Members
		{
			get
			{
				return this.members;
			}
		}

		// Token: 0x0600045C RID: 1116 RVA: 0x00014688 File Offset: 0x00012888
		internal override void Emit(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			Type type = base.Type;
			LocalBuilder local = null;
			if (type.IsValueType)
			{
				local = ig.DeclareLocal(type);
				ig.Emit(OpCodes.Ldloca, local);
				if (this.constructor == null)
				{
					ig.Emit(OpCodes.Initobj, type);
					ig.Emit(OpCodes.Ldloc, local);
					return;
				}
			}
			ec.EmitCollection<Expression>(this.arguments);
			if (type.IsValueType)
			{
				ig.Emit(OpCodes.Call, this.constructor);
				ig.Emit(OpCodes.Ldloc, local);
			}
			else
			{
				ig.Emit(OpCodes.Newobj, this.constructor ?? NewExpression.GetDefaultConstructor(type));
			}
		}

		// Token: 0x0600045D RID: 1117 RVA: 0x00014740 File Offset: 0x00012940
		private static ConstructorInfo GetDefaultConstructor(Type type)
		{
			return type.GetConstructor(Type.EmptyTypes);
		}

		// Token: 0x040000FF RID: 255
		private ConstructorInfo constructor;

		// Token: 0x04000100 RID: 256
		private ReadOnlyCollection<Expression> arguments;

		// Token: 0x04000101 RID: 257
		private ReadOnlyCollection<MemberInfo> members;
	}
}
