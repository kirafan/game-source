﻿using System;
using System.Collections.ObjectModel;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000047 RID: 71
	public sealed class MemberInitExpression : Expression
	{
		// Token: 0x0600043F RID: 1087 RVA: 0x000142AC File Offset: 0x000124AC
		internal MemberInitExpression(NewExpression new_expression, ReadOnlyCollection<MemberBinding> bindings) : base(ExpressionType.MemberInit, new_expression.Type)
		{
			this.new_expression = new_expression;
			this.bindings = bindings;
		}

		// Token: 0x17000045 RID: 69
		// (get) Token: 0x06000440 RID: 1088 RVA: 0x000142CC File Offset: 0x000124CC
		public NewExpression NewExpression
		{
			get
			{
				return this.new_expression;
			}
		}

		// Token: 0x17000046 RID: 70
		// (get) Token: 0x06000441 RID: 1089 RVA: 0x000142D4 File Offset: 0x000124D4
		public ReadOnlyCollection<MemberBinding> Bindings
		{
			get
			{
				return this.bindings;
			}
		}

		// Token: 0x06000442 RID: 1090 RVA: 0x000142DC File Offset: 0x000124DC
		internal override void Emit(EmitContext ec)
		{
			LocalBuilder local = ec.EmitStored(this.new_expression);
			ec.EmitCollection(this.bindings, local);
			ec.EmitLoad(local);
		}

		// Token: 0x040000F7 RID: 247
		private NewExpression new_expression;

		// Token: 0x040000F8 RID: 248
		private ReadOnlyCollection<MemberBinding> bindings;
	}
}
