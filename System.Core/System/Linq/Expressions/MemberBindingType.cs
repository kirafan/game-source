﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x02000045 RID: 69
	public enum MemberBindingType
	{
		// Token: 0x040000F2 RID: 242
		Assignment,
		// Token: 0x040000F3 RID: 243
		MemberBinding,
		// Token: 0x040000F4 RID: 244
		ListBinding
	}
}
