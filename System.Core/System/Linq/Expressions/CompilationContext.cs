﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000038 RID: 56
	internal class CompilationContext
	{
		// Token: 0x0600037D RID: 893 RVA: 0x000118F8 File Offset: 0x0000FAF8
		public int AddGlobal(object global)
		{
			return CompilationContext.AddItemToList<object>(global, this.globals);
		}

		// Token: 0x0600037E RID: 894 RVA: 0x00011908 File Offset: 0x0000FB08
		public object[] GetGlobals()
		{
			return this.globals.ToArray();
		}

		// Token: 0x0600037F RID: 895 RVA: 0x00011918 File Offset: 0x0000FB18
		private static int AddItemToList<T>(T item, IList<T> list)
		{
			list.Add(item);
			return list.Count - 1;
		}

		// Token: 0x06000380 RID: 896 RVA: 0x0001192C File Offset: 0x0000FB2C
		public int AddCompilationUnit(LambdaExpression lambda)
		{
			this.DetectHoistedVariables(lambda);
			return this.AddCompilationUnit(null, lambda);
		}

		// Token: 0x06000381 RID: 897 RVA: 0x00011940 File Offset: 0x0000FB40
		public int AddCompilationUnit(EmitContext parent, LambdaExpression lambda)
		{
			EmitContext emitContext = new EmitContext(this, parent, lambda);
			int result = CompilationContext.AddItemToList<EmitContext>(emitContext, this.units);
			emitContext.Emit();
			return result;
		}

		// Token: 0x06000382 RID: 898 RVA: 0x0001196C File Offset: 0x0000FB6C
		private void DetectHoistedVariables(LambdaExpression lambda)
		{
			this.hoisted_map = new CompilationContext.HoistedVariableDetector().Process(lambda);
		}

		// Token: 0x06000383 RID: 899 RVA: 0x00011980 File Offset: 0x0000FB80
		public List<ParameterExpression> GetHoistedLocals(LambdaExpression lambda)
		{
			if (this.hoisted_map == null)
			{
				return null;
			}
			List<ParameterExpression> result;
			this.hoisted_map.TryGetValue(lambda, out result);
			return result;
		}

		// Token: 0x06000384 RID: 900 RVA: 0x000119AC File Offset: 0x0000FBAC
		public object[] CreateHoistedLocals(int unit)
		{
			List<ParameterExpression> hoistedLocals = this.GetHoistedLocals(this.units[unit].Lambda);
			return new object[(hoistedLocals != null) ? hoistedLocals.Count : 0];
		}

		// Token: 0x06000385 RID: 901 RVA: 0x000119E8 File Offset: 0x0000FBE8
		public Expression IsolateExpression(ExecutionScope scope, object[] locals, Expression expression)
		{
			return new CompilationContext.ParameterReplacer(this, scope, locals).Transform(expression);
		}

		// Token: 0x06000386 RID: 902 RVA: 0x000119F8 File Offset: 0x0000FBF8
		public Delegate CreateDelegate()
		{
			return this.CreateDelegate(0, new ExecutionScope(this));
		}

		// Token: 0x06000387 RID: 903 RVA: 0x00011A08 File Offset: 0x0000FC08
		public Delegate CreateDelegate(int unit, ExecutionScope scope)
		{
			return this.units[unit].CreateDelegate(scope);
		}

		// Token: 0x040000A9 RID: 169
		private List<object> globals = new List<object>();

		// Token: 0x040000AA RID: 170
		private List<EmitContext> units = new List<EmitContext>();

		// Token: 0x040000AB RID: 171
		private Dictionary<LambdaExpression, List<ParameterExpression>> hoisted_map;

		// Token: 0x02000039 RID: 57
		private class ParameterReplacer : ExpressionTransformer
		{
			// Token: 0x06000388 RID: 904 RVA: 0x00011A1C File Offset: 0x0000FC1C
			public ParameterReplacer(CompilationContext context, ExecutionScope scope, object[] locals)
			{
				this.context = context;
				this.scope = scope;
				this.locals = locals;
			}

			// Token: 0x06000389 RID: 905 RVA: 0x00011A3C File Offset: 0x0000FC3C
			protected override Expression VisitParameter(ParameterExpression parameter)
			{
				ExecutionScope parent = this.scope;
				object[] array = this.locals;
				while (parent != null)
				{
					int num = this.IndexOfHoistedLocal(parent, parameter);
					if (num != -1)
					{
						return this.ReadHoistedLocalFromArray(array, num);
					}
					array = parent.Locals;
					parent = parent.Parent;
				}
				return parameter;
			}

			// Token: 0x0600038A RID: 906 RVA: 0x00011A8C File Offset: 0x0000FC8C
			private Expression ReadHoistedLocalFromArray(object[] locals, int position)
			{
				return Expression.Field(Expression.Convert(Expression.ArrayIndex(Expression.Constant(locals), Expression.Constant(position)), locals[position].GetType()), "Value");
			}

			// Token: 0x0600038B RID: 907 RVA: 0x00011AC8 File Offset: 0x0000FCC8
			private int IndexOfHoistedLocal(ExecutionScope scope, ParameterExpression parameter)
			{
				return this.context.units[scope.compilation_unit].IndexOfHoistedLocal(parameter);
			}

			// Token: 0x040000AC RID: 172
			private CompilationContext context;

			// Token: 0x040000AD RID: 173
			private ExecutionScope scope;

			// Token: 0x040000AE RID: 174
			private object[] locals;
		}

		// Token: 0x0200003A RID: 58
		private class HoistedVariableDetector : ExpressionVisitor
		{
			// Token: 0x0600038D RID: 909 RVA: 0x00011AFC File Offset: 0x0000FCFC
			public Dictionary<LambdaExpression, List<ParameterExpression>> Process(LambdaExpression lambda)
			{
				this.Visit(lambda);
				return this.hoisted_map;
			}

			// Token: 0x0600038E RID: 910 RVA: 0x00011B0C File Offset: 0x0000FD0C
			protected override void VisitLambda(LambdaExpression lambda)
			{
				this.lambda = lambda;
				foreach (ParameterExpression key in lambda.Parameters)
				{
					this.parameter_to_lambda[key] = lambda;
				}
				base.VisitLambda(lambda);
			}

			// Token: 0x0600038F RID: 911 RVA: 0x00011B84 File Offset: 0x0000FD84
			protected override void VisitParameter(ParameterExpression parameter)
			{
				if (this.lambda.Parameters.Contains(parameter))
				{
					return;
				}
				this.Hoist(parameter);
			}

			// Token: 0x06000390 RID: 912 RVA: 0x00011BA4 File Offset: 0x0000FDA4
			private void Hoist(ParameterExpression parameter)
			{
				LambdaExpression key;
				if (!this.parameter_to_lambda.TryGetValue(parameter, out key))
				{
					return;
				}
				if (this.hoisted_map == null)
				{
					this.hoisted_map = new Dictionary<LambdaExpression, List<ParameterExpression>>();
				}
				List<ParameterExpression> list;
				if (!this.hoisted_map.TryGetValue(key, out list))
				{
					list = new List<ParameterExpression>();
					this.hoisted_map[key] = list;
				}
				list.Add(parameter);
			}

			// Token: 0x040000AF RID: 175
			private Dictionary<ParameterExpression, LambdaExpression> parameter_to_lambda = new Dictionary<ParameterExpression, LambdaExpression>();

			// Token: 0x040000B0 RID: 176
			private Dictionary<LambdaExpression, List<ParameterExpression>> hoisted_map;

			// Token: 0x040000B1 RID: 177
			private LambdaExpression lambda;
		}
	}
}
