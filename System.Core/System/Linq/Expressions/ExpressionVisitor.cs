﻿using System;
using System.Collections.ObjectModel;

namespace System.Linq.Expressions
{
	// Token: 0x0200003E RID: 62
	internal abstract class ExpressionVisitor
	{
		// Token: 0x060003E2 RID: 994 RVA: 0x00013044 File Offset: 0x00011244
		protected virtual void Visit(Expression expression)
		{
			if (expression == null)
			{
				return;
			}
			switch (expression.NodeType)
			{
			case ExpressionType.Add:
			case ExpressionType.AddChecked:
			case ExpressionType.And:
			case ExpressionType.AndAlso:
			case ExpressionType.ArrayIndex:
			case ExpressionType.Coalesce:
			case ExpressionType.Divide:
			case ExpressionType.Equal:
			case ExpressionType.ExclusiveOr:
			case ExpressionType.GreaterThan:
			case ExpressionType.GreaterThanOrEqual:
			case ExpressionType.LeftShift:
			case ExpressionType.LessThan:
			case ExpressionType.LessThanOrEqual:
			case ExpressionType.Modulo:
			case ExpressionType.Multiply:
			case ExpressionType.MultiplyChecked:
			case ExpressionType.NotEqual:
			case ExpressionType.Or:
			case ExpressionType.OrElse:
			case ExpressionType.Power:
			case ExpressionType.RightShift:
			case ExpressionType.Subtract:
			case ExpressionType.SubtractChecked:
				this.VisitBinary((BinaryExpression)expression);
				break;
			case ExpressionType.ArrayLength:
			case ExpressionType.Convert:
			case ExpressionType.ConvertChecked:
			case ExpressionType.Negate:
			case ExpressionType.UnaryPlus:
			case ExpressionType.NegateChecked:
			case ExpressionType.Not:
			case ExpressionType.Quote:
			case ExpressionType.TypeAs:
				this.VisitUnary((UnaryExpression)expression);
				break;
			case ExpressionType.Call:
				this.VisitMethodCall((MethodCallExpression)expression);
				break;
			case ExpressionType.Conditional:
				this.VisitConditional((ConditionalExpression)expression);
				break;
			case ExpressionType.Constant:
				this.VisitConstant((ConstantExpression)expression);
				break;
			case ExpressionType.Invoke:
				this.VisitInvocation((InvocationExpression)expression);
				break;
			case ExpressionType.Lambda:
				this.VisitLambda((LambdaExpression)expression);
				break;
			case ExpressionType.ListInit:
				this.VisitListInit((ListInitExpression)expression);
				break;
			case ExpressionType.MemberAccess:
				this.VisitMemberAccess((MemberExpression)expression);
				break;
			case ExpressionType.MemberInit:
				this.VisitMemberInit((MemberInitExpression)expression);
				break;
			case ExpressionType.New:
				this.VisitNew((NewExpression)expression);
				break;
			case ExpressionType.NewArrayInit:
			case ExpressionType.NewArrayBounds:
				this.VisitNewArray((NewArrayExpression)expression);
				break;
			case ExpressionType.Parameter:
				this.VisitParameter((ParameterExpression)expression);
				break;
			case ExpressionType.TypeIs:
				this.VisitTypeIs((TypeBinaryExpression)expression);
				break;
			default:
				throw new ArgumentException(string.Format("Unhandled expression type: '{0}'", expression.NodeType));
			}
		}

		// Token: 0x060003E3 RID: 995 RVA: 0x0001322C File Offset: 0x0001142C
		protected virtual void VisitBinding(MemberBinding binding)
		{
			switch (binding.BindingType)
			{
			case MemberBindingType.Assignment:
				this.VisitMemberAssignment((MemberAssignment)binding);
				break;
			case MemberBindingType.MemberBinding:
				this.VisitMemberMemberBinding((MemberMemberBinding)binding);
				break;
			case MemberBindingType.ListBinding:
				this.VisitMemberListBinding((MemberListBinding)binding);
				break;
			default:
				throw new ArgumentException(string.Format("Unhandled binding type '{0}'", binding.BindingType));
			}
		}

		// Token: 0x060003E4 RID: 996 RVA: 0x000132A8 File Offset: 0x000114A8
		protected virtual void VisitElementInitializer(ElementInit initializer)
		{
			this.VisitExpressionList(initializer.Arguments);
		}

		// Token: 0x060003E5 RID: 997 RVA: 0x000132B8 File Offset: 0x000114B8
		protected virtual void VisitUnary(UnaryExpression unary)
		{
			this.Visit(unary.Operand);
		}

		// Token: 0x060003E6 RID: 998 RVA: 0x000132C8 File Offset: 0x000114C8
		protected virtual void VisitBinary(BinaryExpression binary)
		{
			this.Visit(binary.Left);
			this.Visit(binary.Right);
			this.Visit(binary.Conversion);
		}

		// Token: 0x060003E7 RID: 999 RVA: 0x000132FC File Offset: 0x000114FC
		protected virtual void VisitTypeIs(TypeBinaryExpression type)
		{
			this.Visit(type.Expression);
		}

		// Token: 0x060003E8 RID: 1000 RVA: 0x0001330C File Offset: 0x0001150C
		protected virtual void VisitConstant(ConstantExpression constant)
		{
		}

		// Token: 0x060003E9 RID: 1001 RVA: 0x00013310 File Offset: 0x00011510
		protected virtual void VisitConditional(ConditionalExpression conditional)
		{
			this.Visit(conditional.Test);
			this.Visit(conditional.IfTrue);
			this.Visit(conditional.IfFalse);
		}

		// Token: 0x060003EA RID: 1002 RVA: 0x00013344 File Offset: 0x00011544
		protected virtual void VisitParameter(ParameterExpression parameter)
		{
		}

		// Token: 0x060003EB RID: 1003 RVA: 0x00013348 File Offset: 0x00011548
		protected virtual void VisitMemberAccess(MemberExpression member)
		{
			this.Visit(member.Expression);
		}

		// Token: 0x060003EC RID: 1004 RVA: 0x00013358 File Offset: 0x00011558
		protected virtual void VisitMethodCall(MethodCallExpression methodCall)
		{
			this.Visit(methodCall.Object);
			this.VisitExpressionList(methodCall.Arguments);
		}

		// Token: 0x060003ED RID: 1005 RVA: 0x00013380 File Offset: 0x00011580
		protected virtual void VisitList<T>(ReadOnlyCollection<T> list, Action<T> visitor)
		{
			foreach (T obj in list)
			{
				visitor(obj);
			}
		}

		// Token: 0x060003EE RID: 1006 RVA: 0x000133E0 File Offset: 0x000115E0
		protected virtual void VisitExpressionList(ReadOnlyCollection<Expression> list)
		{
			this.VisitList<Expression>(list, new Action<Expression>(this.Visit));
		}

		// Token: 0x060003EF RID: 1007 RVA: 0x000133F8 File Offset: 0x000115F8
		protected virtual void VisitMemberAssignment(MemberAssignment assignment)
		{
			this.Visit(assignment.Expression);
		}

		// Token: 0x060003F0 RID: 1008 RVA: 0x00013408 File Offset: 0x00011608
		protected virtual void VisitMemberMemberBinding(MemberMemberBinding binding)
		{
			this.VisitBindingList(binding.Bindings);
		}

		// Token: 0x060003F1 RID: 1009 RVA: 0x00013418 File Offset: 0x00011618
		protected virtual void VisitMemberListBinding(MemberListBinding binding)
		{
			this.VisitElementInitializerList(binding.Initializers);
		}

		// Token: 0x060003F2 RID: 1010 RVA: 0x00013428 File Offset: 0x00011628
		protected virtual void VisitBindingList(ReadOnlyCollection<MemberBinding> list)
		{
			this.VisitList<MemberBinding>(list, new Action<MemberBinding>(this.VisitBinding));
		}

		// Token: 0x060003F3 RID: 1011 RVA: 0x00013440 File Offset: 0x00011640
		protected virtual void VisitElementInitializerList(ReadOnlyCollection<ElementInit> list)
		{
			this.VisitList<ElementInit>(list, new Action<ElementInit>(this.VisitElementInitializer));
		}

		// Token: 0x060003F4 RID: 1012 RVA: 0x00013458 File Offset: 0x00011658
		protected virtual void VisitLambda(LambdaExpression lambda)
		{
			this.Visit(lambda.Body);
		}

		// Token: 0x060003F5 RID: 1013 RVA: 0x00013468 File Offset: 0x00011668
		protected virtual void VisitNew(NewExpression nex)
		{
			this.VisitExpressionList(nex.Arguments);
		}

		// Token: 0x060003F6 RID: 1014 RVA: 0x00013478 File Offset: 0x00011678
		protected virtual void VisitMemberInit(MemberInitExpression init)
		{
			this.VisitNew(init.NewExpression);
			this.VisitBindingList(init.Bindings);
		}

		// Token: 0x060003F7 RID: 1015 RVA: 0x000134A0 File Offset: 0x000116A0
		protected virtual void VisitListInit(ListInitExpression init)
		{
			this.VisitNew(init.NewExpression);
			this.VisitElementInitializerList(init.Initializers);
		}

		// Token: 0x060003F8 RID: 1016 RVA: 0x000134C8 File Offset: 0x000116C8
		protected virtual void VisitNewArray(NewArrayExpression newArray)
		{
			this.VisitExpressionList(newArray.Expressions);
		}

		// Token: 0x060003F9 RID: 1017 RVA: 0x000134D8 File Offset: 0x000116D8
		protected virtual void VisitInvocation(InvocationExpression invocation)
		{
			this.VisitExpressionList(invocation.Arguments);
			this.Visit(invocation.Expression);
		}
	}
}
