﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000034 RID: 52
	public sealed class BinaryExpression : Expression
	{
		// Token: 0x06000347 RID: 839 RVA: 0x0000FF00 File Offset: 0x0000E100
		internal BinaryExpression(ExpressionType node_type, Type type, Expression left, Expression right) : base(node_type, type)
		{
			this.left = left;
			this.right = right;
		}

		// Token: 0x06000348 RID: 840 RVA: 0x0000FF1C File Offset: 0x0000E11C
		internal BinaryExpression(ExpressionType node_type, Type type, Expression left, Expression right, MethodInfo method) : base(node_type, type)
		{
			this.left = left;
			this.right = right;
			this.method = method;
		}

		// Token: 0x06000349 RID: 841 RVA: 0x0000FF40 File Offset: 0x0000E140
		internal BinaryExpression(ExpressionType node_type, Type type, Expression left, Expression right, bool lift_to_null, bool is_lifted, MethodInfo method, LambdaExpression conversion) : base(node_type, type)
		{
			this.left = left;
			this.right = right;
			this.method = method;
			this.conversion = conversion;
			this.lift_to_null = lift_to_null;
			this.is_lifted = is_lifted;
		}

		// Token: 0x1700002E RID: 46
		// (get) Token: 0x0600034A RID: 842 RVA: 0x0000FF7C File Offset: 0x0000E17C
		public Expression Left
		{
			get
			{
				return this.left;
			}
		}

		// Token: 0x1700002F RID: 47
		// (get) Token: 0x0600034B RID: 843 RVA: 0x0000FF84 File Offset: 0x0000E184
		public Expression Right
		{
			get
			{
				return this.right;
			}
		}

		// Token: 0x17000030 RID: 48
		// (get) Token: 0x0600034C RID: 844 RVA: 0x0000FF8C File Offset: 0x0000E18C
		public MethodInfo Method
		{
			get
			{
				return this.method;
			}
		}

		// Token: 0x17000031 RID: 49
		// (get) Token: 0x0600034D RID: 845 RVA: 0x0000FF94 File Offset: 0x0000E194
		public bool IsLifted
		{
			get
			{
				return this.is_lifted;
			}
		}

		// Token: 0x17000032 RID: 50
		// (get) Token: 0x0600034E RID: 846 RVA: 0x0000FF9C File Offset: 0x0000E19C
		public bool IsLiftedToNull
		{
			get
			{
				return this.lift_to_null;
			}
		}

		// Token: 0x17000033 RID: 51
		// (get) Token: 0x0600034F RID: 847 RVA: 0x0000FFA4 File Offset: 0x0000E1A4
		public LambdaExpression Conversion
		{
			get
			{
				return this.conversion;
			}
		}

		// Token: 0x06000350 RID: 848 RVA: 0x0000FFAC File Offset: 0x0000E1AC
		private void EmitArrayAccess(EmitContext ec)
		{
			this.left.Emit(ec);
			this.right.Emit(ec);
			ec.ig.Emit(OpCodes.Ldelem, base.Type);
		}

		// Token: 0x06000351 RID: 849 RVA: 0x0000FFE8 File Offset: 0x0000E1E8
		private void EmitLogicalBinary(EmitContext ec)
		{
			ExpressionType nodeType = base.NodeType;
			if (nodeType != ExpressionType.And)
			{
				if (nodeType != ExpressionType.AndAlso)
				{
					if (nodeType == ExpressionType.Or)
					{
						goto IL_2A;
					}
					if (nodeType != ExpressionType.OrElse)
					{
						return;
					}
				}
				if (!this.IsLifted)
				{
					this.EmitLogicalShortCircuit(ec);
				}
				else
				{
					this.EmitLiftedLogicalShortCircuit(ec);
				}
				return;
			}
			IL_2A:
			if (!this.IsLifted)
			{
				this.EmitLogical(ec);
			}
			else if (base.Type == typeof(bool?))
			{
				this.EmitLiftedLogical(ec);
			}
			else
			{
				this.EmitLiftedArithmeticBinary(ec);
			}
		}

		// Token: 0x06000352 RID: 850 RVA: 0x00010088 File Offset: 0x0000E288
		private void EmitLogical(EmitContext ec)
		{
			this.EmitNonLiftedBinary(ec);
		}

		// Token: 0x06000353 RID: 851 RVA: 0x00010094 File Offset: 0x0000E294
		private void EmitLiftedLogical(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			bool flag = base.NodeType == ExpressionType.And;
			LocalBuilder localBuilder = ec.EmitStored(this.left);
			LocalBuilder localBuilder2 = ec.EmitStored(this.right);
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			Label label3 = ig.DefineLabel();
			ec.EmitNullableGetValueOrDefault(localBuilder);
			ig.Emit(OpCodes.Brtrue, label);
			ec.EmitNullableGetValueOrDefault(localBuilder2);
			ig.Emit(OpCodes.Brtrue, label2);
			ec.EmitNullableHasValue(localBuilder);
			ig.Emit(OpCodes.Brfalse, label);
			ig.MarkLabel(label2);
			ec.EmitLoad((!flag) ? localBuilder2 : localBuilder);
			ig.Emit(OpCodes.Br, label3);
			ig.MarkLabel(label);
			ec.EmitLoad((!flag) ? localBuilder : localBuilder2);
			ig.MarkLabel(label3);
		}

		// Token: 0x06000354 RID: 852 RVA: 0x0001016C File Offset: 0x0000E36C
		private void EmitLogicalShortCircuit(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			bool flag = base.NodeType == ExpressionType.AndAlso;
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			ec.Emit(this.left);
			ig.Emit((!flag) ? OpCodes.Brtrue : OpCodes.Brfalse, label);
			ec.Emit(this.right);
			ig.Emit(OpCodes.Br, label2);
			ig.MarkLabel(label);
			ig.Emit((!flag) ? OpCodes.Ldc_I4_1 : OpCodes.Ldc_I4_0);
			ig.MarkLabel(label2);
		}

		// Token: 0x06000355 RID: 853 RVA: 0x00010204 File Offset: 0x0000E404
		private MethodInfo GetFalseOperator()
		{
			return Expression.GetFalseOperator(this.left.Type.GetNotNullableType());
		}

		// Token: 0x06000356 RID: 854 RVA: 0x0001021C File Offset: 0x0000E41C
		private MethodInfo GetTrueOperator()
		{
			return Expression.GetTrueOperator(this.left.Type.GetNotNullableType());
		}

		// Token: 0x06000357 RID: 855 RVA: 0x00010234 File Offset: 0x0000E434
		private void EmitUserDefinedLogicalShortCircuit(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			bool flag = base.NodeType == ExpressionType.AndAlso;
			Label label = ig.DefineLabel();
			LocalBuilder local = ec.EmitStored(this.left);
			ec.EmitLoad(local);
			ig.Emit(OpCodes.Dup);
			ec.EmitCall((!flag) ? this.GetTrueOperator() : this.GetFalseOperator());
			ig.Emit(OpCodes.Brtrue, label);
			ec.Emit(this.right);
			ec.EmitCall(this.method);
			ig.MarkLabel(label);
		}

		// Token: 0x06000358 RID: 856 RVA: 0x000102C0 File Offset: 0x0000E4C0
		private void EmitLiftedLogicalShortCircuit(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			bool flag = base.NodeType == ExpressionType.AndAlso;
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			Label label3 = ig.DefineLabel();
			Label label4 = ig.DefineLabel();
			Label label5 = ig.DefineLabel();
			LocalBuilder local = ec.EmitStored(this.left);
			ec.EmitNullableHasValue(local);
			ig.Emit(OpCodes.Brfalse, label);
			ec.EmitNullableGetValueOrDefault(local);
			ig.Emit(OpCodes.Ldc_I4_0);
			ig.Emit(OpCodes.Ceq);
			ig.Emit((!flag) ? OpCodes.Brfalse : OpCodes.Brtrue, label2);
			ig.MarkLabel(label);
			LocalBuilder local2 = ec.EmitStored(this.right);
			ec.EmitNullableHasValue(local2);
			ig.Emit(OpCodes.Brfalse_S, label3);
			ec.EmitNullableGetValueOrDefault(local2);
			ig.Emit(OpCodes.Ldc_I4_0);
			ig.Emit(OpCodes.Ceq);
			ig.Emit((!flag) ? OpCodes.Brfalse : OpCodes.Brtrue, label2);
			ec.EmitNullableHasValue(local);
			ig.Emit(OpCodes.Brfalse, label3);
			ig.Emit((!flag) ? OpCodes.Ldc_I4_0 : OpCodes.Ldc_I4_1);
			ig.Emit(OpCodes.Br_S, label4);
			ig.MarkLabel(label2);
			ig.Emit((!flag) ? OpCodes.Ldc_I4_1 : OpCodes.Ldc_I4_0);
			ig.MarkLabel(label4);
			ec.EmitNullableNew(base.Type);
			ig.Emit(OpCodes.Br, label5);
			ig.MarkLabel(label3);
			LocalBuilder local3 = ig.DeclareLocal(base.Type);
			ec.EmitNullableInitialize(local3);
			ig.MarkLabel(label5);
		}

		// Token: 0x06000359 RID: 857 RVA: 0x0001046C File Offset: 0x0000E66C
		private void EmitCoalesce(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			LocalBuilder localBuilder = ec.EmitStored(this.left);
			bool flag = localBuilder.LocalType.IsNullable();
			if (flag)
			{
				ec.EmitNullableHasValue(localBuilder);
			}
			else
			{
				ec.EmitLoad(localBuilder);
			}
			ig.Emit(OpCodes.Brfalse, label2);
			if (flag && !base.Type.IsNullable())
			{
				ec.EmitNullableGetValue(localBuilder);
			}
			else
			{
				ec.EmitLoad(localBuilder);
			}
			ig.Emit(OpCodes.Br, label);
			ig.MarkLabel(label2);
			ec.Emit(this.right);
			ig.MarkLabel(label);
		}

		// Token: 0x0600035A RID: 858 RVA: 0x00010520 File Offset: 0x0000E720
		private void EmitConvertedCoalesce(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			LocalBuilder localBuilder = ec.EmitStored(this.left);
			if (localBuilder.LocalType.IsNullable())
			{
				ec.EmitNullableHasValue(localBuilder);
			}
			else
			{
				ec.EmitLoad(localBuilder);
			}
			ig.Emit(OpCodes.Brfalse, label2);
			ec.Emit(this.conversion);
			ec.EmitLoad(localBuilder);
			ig.Emit(OpCodes.Callvirt, this.conversion.Type.GetInvokeMethod());
			ig.Emit(OpCodes.Br, label);
			ig.MarkLabel(label2);
			ec.Emit(this.right);
			ig.MarkLabel(label);
		}

		// Token: 0x0600035B RID: 859 RVA: 0x000105D4 File Offset: 0x0000E7D4
		private static bool IsInt32OrInt64(Type type)
		{
			return type == typeof(int) || type == typeof(long);
		}

		// Token: 0x0600035C RID: 860 RVA: 0x00010604 File Offset: 0x0000E804
		private static bool IsSingleOrDouble(Type type)
		{
			return type == typeof(float) || type == typeof(double);
		}

		// Token: 0x0600035D RID: 861 RVA: 0x00010634 File Offset: 0x0000E834
		private void EmitBinaryOperator(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			bool flag = Expression.IsUnsigned(this.left.Type);
			ExpressionType nodeType = base.NodeType;
			switch (nodeType)
			{
			case ExpressionType.Divide:
				ig.Emit((!flag) ? OpCodes.Div : OpCodes.Div_Un);
				break;
			case ExpressionType.Equal:
				ig.Emit(OpCodes.Ceq);
				break;
			case ExpressionType.ExclusiveOr:
				ig.Emit(OpCodes.Xor);
				break;
			case ExpressionType.GreaterThan:
				ig.Emit((!flag) ? OpCodes.Cgt : OpCodes.Cgt_Un);
				break;
			case ExpressionType.GreaterThanOrEqual:
				if (flag || BinaryExpression.IsSingleOrDouble(this.left.Type))
				{
					ig.Emit(OpCodes.Clt_Un);
				}
				else
				{
					ig.Emit(OpCodes.Clt);
				}
				ig.Emit(OpCodes.Ldc_I4_0);
				ig.Emit(OpCodes.Ceq);
				break;
			default:
				switch (nodeType)
				{
				case ExpressionType.Add:
					ig.Emit(OpCodes.Add);
					break;
				case ExpressionType.AddChecked:
					if (BinaryExpression.IsInt32OrInt64(this.left.Type))
					{
						ig.Emit(OpCodes.Add_Ovf);
					}
					else
					{
						ig.Emit((!flag) ? OpCodes.Add : OpCodes.Add_Ovf_Un);
					}
					break;
				case ExpressionType.And:
					ig.Emit(OpCodes.And);
					break;
				default:
					throw new InvalidOperationException(string.Format("Internal error: BinaryExpression contains non-Binary nodetype {0}", base.NodeType));
				}
				break;
			case ExpressionType.LeftShift:
			case ExpressionType.RightShift:
				ig.Emit(OpCodes.Ldc_I4, (this.left.Type != typeof(int)) ? 63 : 31);
				ig.Emit(OpCodes.And);
				if (base.NodeType == ExpressionType.RightShift)
				{
					ig.Emit((!flag) ? OpCodes.Shr : OpCodes.Shr_Un);
				}
				else
				{
					ig.Emit(OpCodes.Shl);
				}
				break;
			case ExpressionType.LessThan:
				ig.Emit((!flag) ? OpCodes.Clt : OpCodes.Clt_Un);
				break;
			case ExpressionType.LessThanOrEqual:
				if (flag || BinaryExpression.IsSingleOrDouble(this.left.Type))
				{
					ig.Emit(OpCodes.Cgt_Un);
				}
				else
				{
					ig.Emit(OpCodes.Cgt);
				}
				ig.Emit(OpCodes.Ldc_I4_0);
				ig.Emit(OpCodes.Ceq);
				break;
			case ExpressionType.Modulo:
				ig.Emit((!flag) ? OpCodes.Rem : OpCodes.Rem_Un);
				break;
			case ExpressionType.Multiply:
				ig.Emit(OpCodes.Mul);
				break;
			case ExpressionType.MultiplyChecked:
				if (BinaryExpression.IsInt32OrInt64(this.left.Type))
				{
					ig.Emit(OpCodes.Mul_Ovf);
				}
				else
				{
					ig.Emit((!flag) ? OpCodes.Mul : OpCodes.Mul_Ovf_Un);
				}
				break;
			case ExpressionType.NotEqual:
				ig.Emit(OpCodes.Ceq);
				ig.Emit(OpCodes.Ldc_I4_0);
				ig.Emit(OpCodes.Ceq);
				break;
			case ExpressionType.Or:
				ig.Emit(OpCodes.Or);
				break;
			case ExpressionType.Power:
				ig.Emit(OpCodes.Call, typeof(Math).GetMethod("Pow"));
				break;
			case ExpressionType.Subtract:
				ig.Emit(OpCodes.Sub);
				break;
			case ExpressionType.SubtractChecked:
				if (BinaryExpression.IsInt32OrInt64(this.left.Type))
				{
					ig.Emit(OpCodes.Sub_Ovf);
				}
				else
				{
					ig.Emit((!flag) ? OpCodes.Sub : OpCodes.Sub_Ovf_Un);
				}
				break;
			}
		}

		// Token: 0x0600035E RID: 862 RVA: 0x00010A3C File Offset: 0x0000EC3C
		private bool IsLeftLiftedBinary()
		{
			return this.left.Type.IsNullable() && !this.right.Type.IsNullable();
		}

		// Token: 0x0600035F RID: 863 RVA: 0x00010A74 File Offset: 0x0000EC74
		private void EmitLeftLiftedToNullBinary(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			LocalBuilder local = ec.EmitStored(this.left);
			ec.EmitNullableHasValue(local);
			ig.Emit(OpCodes.Brfalse, label);
			ec.EmitNullableGetValueOrDefault(local);
			ec.Emit(this.right);
			this.EmitBinaryOperator(ec);
			ec.EmitNullableNew(base.Type);
			ig.Emit(OpCodes.Br, label2);
			ig.MarkLabel(label);
			LocalBuilder local2 = ig.DeclareLocal(base.Type);
			ec.EmitNullableInitialize(local2);
			ig.MarkLabel(label2);
		}

		// Token: 0x06000360 RID: 864 RVA: 0x00010B0C File Offset: 0x0000ED0C
		private void EmitLiftedArithmeticBinary(EmitContext ec)
		{
			if (this.IsLeftLiftedBinary())
			{
				this.EmitLeftLiftedToNullBinary(ec);
			}
			else
			{
				this.EmitLiftedToNullBinary(ec);
			}
		}

		// Token: 0x06000361 RID: 865 RVA: 0x00010B2C File Offset: 0x0000ED2C
		private void EmitLiftedToNullBinary(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			LocalBuilder local = ec.EmitStored(this.left);
			LocalBuilder local2 = ec.EmitStored(this.right);
			LocalBuilder localBuilder = ig.DeclareLocal(base.Type);
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			ec.EmitNullableHasValue(local);
			ec.EmitNullableHasValue(local2);
			ig.Emit(OpCodes.And);
			ig.Emit(OpCodes.Brtrue, label);
			ec.EmitNullableInitialize(localBuilder);
			ig.Emit(OpCodes.Br, label2);
			ig.MarkLabel(label);
			ec.EmitNullableGetValueOrDefault(local);
			ec.EmitNullableGetValueOrDefault(local2);
			this.EmitBinaryOperator(ec);
			ec.EmitNullableNew(localBuilder.LocalType);
			ig.MarkLabel(label2);
		}

		// Token: 0x06000362 RID: 866 RVA: 0x00010BE4 File Offset: 0x0000EDE4
		private void EmitLiftedRelationalBinary(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			LocalBuilder local = ec.EmitStored(this.left);
			LocalBuilder local2 = ec.EmitStored(this.right);
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			ec.EmitNullableGetValueOrDefault(local);
			ec.EmitNullableGetValueOrDefault(local2);
			ExpressionType nodeType = base.NodeType;
			if (nodeType != ExpressionType.Equal && nodeType != ExpressionType.NotEqual)
			{
				this.EmitBinaryOperator(ec);
				ig.Emit(OpCodes.Brfalse, label);
			}
			else
			{
				ig.Emit(OpCodes.Bne_Un, label);
			}
			ec.EmitNullableHasValue(local);
			ec.EmitNullableHasValue(local2);
			nodeType = base.NodeType;
			if (nodeType != ExpressionType.Equal)
			{
				if (nodeType != ExpressionType.NotEqual)
				{
					ig.Emit(OpCodes.And);
				}
				else
				{
					ig.Emit(OpCodes.Ceq);
					ig.Emit(OpCodes.Ldc_I4_0);
					ig.Emit(OpCodes.Ceq);
				}
			}
			else
			{
				ig.Emit(OpCodes.Ceq);
			}
			ig.Emit(OpCodes.Br, label2);
			ig.MarkLabel(label);
			ig.Emit((base.NodeType != ExpressionType.NotEqual) ? OpCodes.Ldc_I4_0 : OpCodes.Ldc_I4_1);
			ig.MarkLabel(label2);
		}

		// Token: 0x06000363 RID: 867 RVA: 0x00010D28 File Offset: 0x0000EF28
		private void EmitArithmeticBinary(EmitContext ec)
		{
			if (!this.IsLifted)
			{
				this.EmitNonLiftedBinary(ec);
			}
			else
			{
				this.EmitLiftedArithmeticBinary(ec);
			}
		}

		// Token: 0x06000364 RID: 868 RVA: 0x00010D48 File Offset: 0x0000EF48
		private void EmitNonLiftedBinary(EmitContext ec)
		{
			ec.Emit(this.left);
			ec.Emit(this.right);
			this.EmitBinaryOperator(ec);
		}

		// Token: 0x06000365 RID: 869 RVA: 0x00010D6C File Offset: 0x0000EF6C
		private void EmitRelationalBinary(EmitContext ec)
		{
			if (!this.IsLifted)
			{
				this.EmitNonLiftedBinary(ec);
			}
			else if (this.IsLiftedToNull)
			{
				this.EmitLiftedToNullBinary(ec);
			}
			else
			{
				this.EmitLiftedRelationalBinary(ec);
			}
		}

		// Token: 0x06000366 RID: 870 RVA: 0x00010DB0 File Offset: 0x0000EFB0
		private void EmitLiftedUserDefinedOperator(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			Label label3 = ig.DefineLabel();
			LocalBuilder local = ec.EmitStored(this.left);
			LocalBuilder local2 = ec.EmitStored(this.right);
			ec.EmitNullableHasValue(local);
			ec.EmitNullableHasValue(local2);
			ExpressionType nodeType = base.NodeType;
			if (nodeType != ExpressionType.Equal)
			{
				if (nodeType != ExpressionType.NotEqual)
				{
					ig.Emit(OpCodes.And);
					ig.Emit(OpCodes.Brfalse, label2);
				}
				else
				{
					ig.Emit(OpCodes.Bne_Un, label);
					ec.EmitNullableHasValue(local);
					ig.Emit(OpCodes.Brfalse, label2);
				}
			}
			else
			{
				ig.Emit(OpCodes.Bne_Un, label2);
				ec.EmitNullableHasValue(local);
				ig.Emit(OpCodes.Brfalse, label);
			}
			ec.EmitNullableGetValueOrDefault(local);
			ec.EmitNullableGetValueOrDefault(local2);
			ec.EmitCall(this.method);
			ig.Emit(OpCodes.Br, label3);
			ig.MarkLabel(label);
			ig.Emit(OpCodes.Ldc_I4_1);
			ig.Emit(OpCodes.Br, label3);
			ig.MarkLabel(label2);
			ig.Emit(OpCodes.Ldc_I4_0);
			ig.Emit(OpCodes.Br, label3);
			ig.MarkLabel(label3);
		}

		// Token: 0x06000367 RID: 871 RVA: 0x00010EF8 File Offset: 0x0000F0F8
		private void EmitLiftedToNullUserDefinedOperator(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			LocalBuilder local = ec.EmitStored(this.left);
			LocalBuilder local2 = ec.EmitStored(this.right);
			ec.EmitNullableHasValue(local);
			ec.EmitNullableHasValue(local2);
			ig.Emit(OpCodes.And);
			ig.Emit(OpCodes.Brfalse, label);
			ec.EmitNullableGetValueOrDefault(local);
			ec.EmitNullableGetValueOrDefault(local2);
			ec.EmitCall(this.method);
			ec.EmitNullableNew(base.Type);
			ig.Emit(OpCodes.Br, label2);
			ig.MarkLabel(label);
			LocalBuilder local3 = ig.DeclareLocal(base.Type);
			ec.EmitNullableInitialize(local3);
			ig.MarkLabel(label2);
		}

		// Token: 0x06000368 RID: 872 RVA: 0x00010FB4 File Offset: 0x0000F1B4
		private void EmitUserDefinedLiftedLogicalShortCircuit(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			bool flag = base.NodeType == ExpressionType.AndAlso;
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			Label label3 = ig.DefineLabel();
			Label label4 = ig.DefineLabel();
			LocalBuilder local = ec.EmitStored(this.left);
			ec.EmitNullableHasValue(local);
			ig.Emit(OpCodes.Brfalse, (!flag) ? label : label3);
			ec.EmitNullableGetValueOrDefault(local);
			ec.EmitCall((!flag) ? this.GetTrueOperator() : this.GetFalseOperator());
			ig.Emit(OpCodes.Brtrue, label2);
			ig.MarkLabel(label);
			LocalBuilder local2 = ec.EmitStored(this.right);
			ec.EmitNullableHasValue(local2);
			ig.Emit(OpCodes.Brfalse, label3);
			ec.EmitNullableGetValueOrDefault(local);
			ec.EmitNullableGetValueOrDefault(local2);
			ec.EmitCall(this.method);
			ec.EmitNullableNew(base.Type);
			ig.Emit(OpCodes.Br, label4);
			ig.MarkLabel(label2);
			ec.EmitLoad(local);
			ig.Emit(OpCodes.Br, label4);
			ig.MarkLabel(label3);
			LocalBuilder local3 = ig.DeclareLocal(base.Type);
			ec.EmitNullableInitialize(local3);
			ig.MarkLabel(label4);
		}

		// Token: 0x06000369 RID: 873 RVA: 0x000110F4 File Offset: 0x0000F2F4
		private void EmitUserDefinedOperator(EmitContext ec)
		{
			if (!this.IsLifted)
			{
				ExpressionType nodeType = base.NodeType;
				if (nodeType != ExpressionType.AndAlso && nodeType != ExpressionType.OrElse)
				{
					this.left.Emit(ec);
					this.right.Emit(ec);
					ec.EmitCall(this.method);
				}
				else
				{
					this.EmitUserDefinedLogicalShortCircuit(ec);
				}
			}
			else if (this.IsLiftedToNull)
			{
				ExpressionType nodeType = base.NodeType;
				if (nodeType != ExpressionType.AndAlso && nodeType != ExpressionType.OrElse)
				{
					this.EmitLiftedToNullUserDefinedOperator(ec);
				}
				else
				{
					this.EmitUserDefinedLiftedLogicalShortCircuit(ec);
				}
			}
			else
			{
				this.EmitLiftedUserDefinedOperator(ec);
			}
		}

		// Token: 0x0600036A RID: 874 RVA: 0x000111AC File Offset: 0x0000F3AC
		internal override void Emit(EmitContext ec)
		{
			if (this.method != null)
			{
				this.EmitUserDefinedOperator(ec);
				return;
			}
			switch (base.NodeType)
			{
			case ExpressionType.Add:
			case ExpressionType.AddChecked:
			case ExpressionType.Divide:
			case ExpressionType.ExclusiveOr:
			case ExpressionType.LeftShift:
			case ExpressionType.Modulo:
			case ExpressionType.Multiply:
			case ExpressionType.MultiplyChecked:
			case ExpressionType.Power:
			case ExpressionType.RightShift:
			case ExpressionType.Subtract:
			case ExpressionType.SubtractChecked:
				this.EmitArithmeticBinary(ec);
				return;
			case ExpressionType.And:
			case ExpressionType.AndAlso:
			case ExpressionType.Or:
			case ExpressionType.OrElse:
				this.EmitLogicalBinary(ec);
				return;
			case ExpressionType.ArrayIndex:
				this.EmitArrayAccess(ec);
				return;
			case ExpressionType.Coalesce:
				if (this.conversion != null)
				{
					this.EmitConvertedCoalesce(ec);
				}
				else
				{
					this.EmitCoalesce(ec);
				}
				return;
			case ExpressionType.Equal:
			case ExpressionType.GreaterThan:
			case ExpressionType.GreaterThanOrEqual:
			case ExpressionType.LessThan:
			case ExpressionType.LessThanOrEqual:
			case ExpressionType.NotEqual:
				this.EmitRelationalBinary(ec);
				return;
			}
			throw new NotSupportedException(base.NodeType.ToString());
		}

		// Token: 0x0400009D RID: 157
		private Expression left;

		// Token: 0x0400009E RID: 158
		private Expression right;

		// Token: 0x0400009F RID: 159
		private LambdaExpression conversion;

		// Token: 0x040000A0 RID: 160
		private MethodInfo method;

		// Token: 0x040000A1 RID: 161
		private bool lift_to_null;

		// Token: 0x040000A2 RID: 162
		private bool is_lifted;
	}
}
