﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000044 RID: 68
	public abstract class MemberBinding
	{
		// Token: 0x06000431 RID: 1073 RVA: 0x000140AC File Offset: 0x000122AC
		protected MemberBinding(MemberBindingType binding_type, MemberInfo member)
		{
			this.binding_type = binding_type;
			this.member = member;
		}

		// Token: 0x17000041 RID: 65
		// (get) Token: 0x06000432 RID: 1074 RVA: 0x000140C4 File Offset: 0x000122C4
		public MemberBindingType BindingType
		{
			get
			{
				return this.binding_type;
			}
		}

		// Token: 0x17000042 RID: 66
		// (get) Token: 0x06000433 RID: 1075 RVA: 0x000140CC File Offset: 0x000122CC
		public MemberInfo Member
		{
			get
			{
				return this.member;
			}
		}

		// Token: 0x06000434 RID: 1076 RVA: 0x000140D4 File Offset: 0x000122D4
		public override string ToString()
		{
			return ExpressionPrinter.ToString(this);
		}

		// Token: 0x06000435 RID: 1077
		internal abstract void Emit(EmitContext ec, LocalBuilder local);

		// Token: 0x06000436 RID: 1078 RVA: 0x000140DC File Offset: 0x000122DC
		internal LocalBuilder EmitLoadMember(EmitContext ec, LocalBuilder local)
		{
			ec.EmitLoadSubject(local);
			return this.member.OnFieldOrProperty((FieldInfo field) => this.EmitLoadField(ec, field), (PropertyInfo prop) => this.EmitLoadProperty(ec, prop));
		}

		// Token: 0x06000437 RID: 1079 RVA: 0x0001412C File Offset: 0x0001232C
		private LocalBuilder EmitLoadProperty(EmitContext ec, PropertyInfo property)
		{
			MethodInfo getMethod = property.GetGetMethod(true);
			if (getMethod == null)
			{
				throw new NotSupportedException();
			}
			LocalBuilder localBuilder = ec.ig.DeclareLocal(property.PropertyType);
			ec.EmitCall(getMethod);
			ec.ig.Emit(OpCodes.Stloc, localBuilder);
			return localBuilder;
		}

		// Token: 0x06000438 RID: 1080 RVA: 0x00014178 File Offset: 0x00012378
		private LocalBuilder EmitLoadField(EmitContext ec, FieldInfo field)
		{
			LocalBuilder localBuilder = ec.ig.DeclareLocal(field.FieldType);
			ec.ig.Emit(OpCodes.Ldfld, field);
			ec.ig.Emit(OpCodes.Stloc, localBuilder);
			return localBuilder;
		}

		// Token: 0x040000EF RID: 239
		private MemberBindingType binding_type;

		// Token: 0x040000F0 RID: 240
		private MemberInfo member;
	}
}
