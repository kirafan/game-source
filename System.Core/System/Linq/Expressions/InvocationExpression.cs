﻿using System;
using System.Collections.ObjectModel;

namespace System.Linq.Expressions
{
	// Token: 0x02000041 RID: 65
	public sealed class InvocationExpression : Expression
	{
		// Token: 0x06000424 RID: 1060 RVA: 0x00013F18 File Offset: 0x00012118
		internal InvocationExpression(Expression expression, Type type, ReadOnlyCollection<Expression> arguments) : base(ExpressionType.Invoke, type)
		{
			this.expression = expression;
			this.arguments = arguments;
		}

		// Token: 0x1700003C RID: 60
		// (get) Token: 0x06000425 RID: 1061 RVA: 0x00013F34 File Offset: 0x00012134
		public Expression Expression
		{
			get
			{
				return this.expression;
			}
		}

		// Token: 0x1700003D RID: 61
		// (get) Token: 0x06000426 RID: 1062 RVA: 0x00013F3C File Offset: 0x0001213C
		public ReadOnlyCollection<Expression> Arguments
		{
			get
			{
				return this.arguments;
			}
		}

		// Token: 0x06000427 RID: 1063 RVA: 0x00013F44 File Offset: 0x00012144
		internal override void Emit(EmitContext ec)
		{
			ec.EmitCall(this.expression, this.arguments, this.expression.Type.GetInvokeMethod());
		}

		// Token: 0x040000EA RID: 234
		private Expression expression;

		// Token: 0x040000EB RID: 235
		private ReadOnlyCollection<Expression> arguments;
	}
}
