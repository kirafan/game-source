﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x0200004F RID: 79
	public sealed class UnaryExpression : Expression
	{
		// Token: 0x06000467 RID: 1127 RVA: 0x000148CC File Offset: 0x00012ACC
		internal UnaryExpression(ExpressionType node_type, Expression operand, Type type) : base(node_type, type)
		{
			this.operand = operand;
		}

		// Token: 0x06000468 RID: 1128 RVA: 0x000148E0 File Offset: 0x00012AE0
		internal UnaryExpression(ExpressionType node_type, Expression operand, Type type, MethodInfo method, bool is_lifted) : base(node_type, type)
		{
			this.operand = operand;
			this.method = method;
			this.is_lifted = is_lifted;
		}

		// Token: 0x17000053 RID: 83
		// (get) Token: 0x06000469 RID: 1129 RVA: 0x00014904 File Offset: 0x00012B04
		public Expression Operand
		{
			get
			{
				return this.operand;
			}
		}

		// Token: 0x17000054 RID: 84
		// (get) Token: 0x0600046A RID: 1130 RVA: 0x0001490C File Offset: 0x00012B0C
		public MethodInfo Method
		{
			get
			{
				return this.method;
			}
		}

		// Token: 0x17000055 RID: 85
		// (get) Token: 0x0600046B RID: 1131 RVA: 0x00014914 File Offset: 0x00012B14
		public bool IsLifted
		{
			get
			{
				return this.is_lifted;
			}
		}

		// Token: 0x17000056 RID: 86
		// (get) Token: 0x0600046C RID: 1132 RVA: 0x0001491C File Offset: 0x00012B1C
		public bool IsLiftedToNull
		{
			get
			{
				return this.is_lifted && base.Type.IsNullable();
			}
		}

		// Token: 0x0600046D RID: 1133 RVA: 0x00014938 File Offset: 0x00012B38
		private void EmitArrayLength(EmitContext ec)
		{
			this.operand.Emit(ec);
			ec.ig.Emit(OpCodes.Ldlen);
		}

		// Token: 0x0600046E RID: 1134 RVA: 0x00014958 File Offset: 0x00012B58
		private void EmitTypeAs(EmitContext ec)
		{
			Type type = base.Type;
			ec.EmitIsInst(this.operand, type);
			if (type.IsNullable())
			{
				ec.ig.Emit(OpCodes.Unbox_Any, type);
			}
		}

		// Token: 0x0600046F RID: 1135 RVA: 0x00014998 File Offset: 0x00012B98
		private void EmitLiftedUnary(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			LocalBuilder local = ec.EmitStored(this.operand);
			LocalBuilder local2 = ig.DeclareLocal(base.Type);
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			ec.EmitNullableHasValue(local);
			ig.Emit(OpCodes.Brtrue, label);
			ec.EmitNullableInitialize(local2);
			ig.Emit(OpCodes.Br, label2);
			ig.MarkLabel(label);
			ec.EmitNullableGetValueOrDefault(local);
			this.EmitUnaryOperator(ec);
			ec.EmitNullableNew(base.Type);
			ig.MarkLabel(label2);
		}

		// Token: 0x06000470 RID: 1136 RVA: 0x00014A28 File Offset: 0x00012C28
		private void EmitUnaryOperator(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			ExpressionType nodeType = base.NodeType;
			switch (nodeType)
			{
			case ExpressionType.Negate:
				ig.Emit(OpCodes.Neg);
				break;
			default:
				if (nodeType != ExpressionType.Convert && nodeType != ExpressionType.ConvertChecked)
				{
					if (nodeType == ExpressionType.Not)
					{
						if (this.operand.Type.GetNotNullableType() == typeof(bool))
						{
							ig.Emit(OpCodes.Ldc_I4_0);
							ig.Emit(OpCodes.Ceq);
						}
						else
						{
							ig.Emit(OpCodes.Not);
						}
					}
				}
				else
				{
					this.EmitPrimitiveConversion(ec, this.operand.Type.GetNotNullableType(), base.Type.GetNotNullableType());
				}
				break;
			case ExpressionType.NegateChecked:
				ig.Emit(OpCodes.Ldc_I4_M1);
				ig.Emit((!Expression.IsUnsigned(this.operand.Type)) ? OpCodes.Mul_Ovf : OpCodes.Mul_Ovf_Un);
				break;
			}
		}

		// Token: 0x06000471 RID: 1137 RVA: 0x00014B30 File Offset: 0x00012D30
		private void EmitConvert(EmitContext ec)
		{
			Type type = this.operand.Type;
			Type type2 = base.Type;
			if (type == type2)
			{
				this.operand.Emit(ec);
			}
			else if (type.IsNullable() && !type2.IsNullable())
			{
				this.EmitConvertFromNullable(ec);
			}
			else if (!type.IsNullable() && type2.IsNullable())
			{
				this.EmitConvertToNullable(ec);
			}
			else if (type.IsNullable() && type2.IsNullable())
			{
				this.EmitConvertFromNullableToNullable(ec);
			}
			else if (Expression.IsReferenceConversion(type, type2))
			{
				this.EmitCast(ec);
			}
			else
			{
				if (!Expression.IsPrimitiveConversion(type, type2))
				{
					throw new NotImplementedException();
				}
				this.EmitPrimitiveConversion(ec);
			}
		}

		// Token: 0x06000472 RID: 1138 RVA: 0x00014C04 File Offset: 0x00012E04
		private void EmitConvertFromNullableToNullable(EmitContext ec)
		{
			this.EmitLiftedUnary(ec);
		}

		// Token: 0x06000473 RID: 1139 RVA: 0x00014C10 File Offset: 0x00012E10
		private void EmitConvertToNullable(EmitContext ec)
		{
			ec.Emit(this.operand);
			if (this.IsUnBoxing())
			{
				this.EmitUnbox(ec);
				return;
			}
			if (this.operand.Type != base.Type.GetNotNullableType())
			{
				this.EmitPrimitiveConversion(ec, this.operand.Type, base.Type.GetNotNullableType());
			}
			ec.EmitNullableNew(base.Type);
		}

		// Token: 0x06000474 RID: 1140 RVA: 0x00014C80 File Offset: 0x00012E80
		private void EmitConvertFromNullable(EmitContext ec)
		{
			if (this.IsBoxing())
			{
				ec.Emit(this.operand);
				this.EmitBox(ec);
				return;
			}
			ec.EmitCall(this.operand, this.operand.Type.GetMethod("get_Value"));
			if (this.operand.Type.GetNotNullableType() != base.Type)
			{
				this.EmitPrimitiveConversion(ec, this.operand.Type.GetNotNullableType(), base.Type);
			}
		}

		// Token: 0x06000475 RID: 1141 RVA: 0x00014D08 File Offset: 0x00012F08
		private bool IsBoxing()
		{
			return this.operand.Type.IsValueType && !base.Type.IsValueType;
		}

		// Token: 0x06000476 RID: 1142 RVA: 0x00014D3C File Offset: 0x00012F3C
		private void EmitBox(EmitContext ec)
		{
			ec.ig.Emit(OpCodes.Box, this.operand.Type);
		}

		// Token: 0x06000477 RID: 1143 RVA: 0x00014D5C File Offset: 0x00012F5C
		private bool IsUnBoxing()
		{
			return !this.operand.Type.IsValueType && base.Type.IsValueType;
		}

		// Token: 0x06000478 RID: 1144 RVA: 0x00014D8C File Offset: 0x00012F8C
		private void EmitUnbox(EmitContext ec)
		{
			ec.ig.Emit(OpCodes.Unbox_Any, base.Type);
		}

		// Token: 0x06000479 RID: 1145 RVA: 0x00014DA4 File Offset: 0x00012FA4
		private void EmitCast(EmitContext ec)
		{
			this.operand.Emit(ec);
			if (this.IsBoxing())
			{
				this.EmitBox(ec);
			}
			else if (this.IsUnBoxing())
			{
				this.EmitUnbox(ec);
			}
			else
			{
				ec.ig.Emit(OpCodes.Castclass, base.Type);
			}
		}

		// Token: 0x0600047A RID: 1146 RVA: 0x00014E04 File Offset: 0x00013004
		private void EmitPrimitiveConversion(EmitContext ec, bool is_unsigned, OpCode signed, OpCode unsigned, OpCode signed_checked, OpCode unsigned_checked)
		{
			if (base.NodeType != ExpressionType.ConvertChecked)
			{
				ec.ig.Emit((!is_unsigned) ? signed : unsigned);
			}
			else
			{
				ec.ig.Emit((!is_unsigned) ? signed_checked : unsigned_checked);
			}
		}

		// Token: 0x0600047B RID: 1147 RVA: 0x00014E58 File Offset: 0x00013058
		private void EmitPrimitiveConversion(EmitContext ec)
		{
			this.operand.Emit(ec);
			this.EmitPrimitiveConversion(ec, this.operand.Type, base.Type);
		}

		// Token: 0x0600047C RID: 1148 RVA: 0x00014E8C File Offset: 0x0001308C
		private void EmitPrimitiveConversion(EmitContext ec, Type from, Type to)
		{
			bool flag = Expression.IsUnsigned(from);
			switch (Type.GetTypeCode(to))
			{
			case TypeCode.SByte:
				this.EmitPrimitiveConversion(ec, flag, OpCodes.Conv_I1, OpCodes.Conv_U1, OpCodes.Conv_Ovf_I1, OpCodes.Conv_Ovf_I1_Un);
				return;
			case TypeCode.Byte:
				this.EmitPrimitiveConversion(ec, flag, OpCodes.Conv_I1, OpCodes.Conv_U1, OpCodes.Conv_Ovf_U1, OpCodes.Conv_Ovf_U1_Un);
				return;
			case TypeCode.Int16:
				this.EmitPrimitiveConversion(ec, flag, OpCodes.Conv_I2, OpCodes.Conv_U2, OpCodes.Conv_Ovf_I2, OpCodes.Conv_Ovf_I2_Un);
				return;
			case TypeCode.UInt16:
				this.EmitPrimitiveConversion(ec, flag, OpCodes.Conv_I2, OpCodes.Conv_U2, OpCodes.Conv_Ovf_U2, OpCodes.Conv_Ovf_U2_Un);
				return;
			case TypeCode.Int32:
				this.EmitPrimitiveConversion(ec, flag, OpCodes.Conv_I4, OpCodes.Conv_U4, OpCodes.Conv_Ovf_I4, OpCodes.Conv_Ovf_I4_Un);
				return;
			case TypeCode.UInt32:
				this.EmitPrimitiveConversion(ec, flag, OpCodes.Conv_I4, OpCodes.Conv_U4, OpCodes.Conv_Ovf_U4, OpCodes.Conv_Ovf_U4_Un);
				return;
			case TypeCode.Int64:
				this.EmitPrimitiveConversion(ec, flag, OpCodes.Conv_I8, OpCodes.Conv_U8, OpCodes.Conv_Ovf_I8, OpCodes.Conv_Ovf_I8_Un);
				return;
			case TypeCode.UInt64:
				this.EmitPrimitiveConversion(ec, flag, OpCodes.Conv_I8, OpCodes.Conv_U8, OpCodes.Conv_Ovf_U8, OpCodes.Conv_Ovf_U8_Un);
				return;
			case TypeCode.Single:
				if (flag)
				{
					ec.ig.Emit(OpCodes.Conv_R_Un);
				}
				ec.ig.Emit(OpCodes.Conv_R4);
				return;
			case TypeCode.Double:
				if (flag)
				{
					ec.ig.Emit(OpCodes.Conv_R_Un);
				}
				ec.ig.Emit(OpCodes.Conv_R8);
				return;
			default:
				throw new NotImplementedException(base.Type.ToString());
			}
		}

		// Token: 0x0600047D RID: 1149 RVA: 0x00015024 File Offset: 0x00013224
		private void EmitArithmeticUnary(EmitContext ec)
		{
			if (!this.IsLifted)
			{
				this.operand.Emit(ec);
				this.EmitUnaryOperator(ec);
			}
			else
			{
				this.EmitLiftedUnary(ec);
			}
		}

		// Token: 0x0600047E RID: 1150 RVA: 0x0001505C File Offset: 0x0001325C
		private void EmitUserDefinedLiftedToNullOperator(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			LocalBuilder local = ec.EmitStored(this.operand);
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			ec.EmitNullableHasValue(local);
			ig.Emit(OpCodes.Brfalse, label);
			ec.EmitNullableGetValueOrDefault(local);
			ec.EmitCall(this.method);
			ec.EmitNullableNew(base.Type);
			ig.Emit(OpCodes.Br, label2);
			ig.MarkLabel(label);
			LocalBuilder local2 = ig.DeclareLocal(base.Type);
			ec.EmitNullableInitialize(local2);
			ig.MarkLabel(label2);
		}

		// Token: 0x0600047F RID: 1151 RVA: 0x000150F0 File Offset: 0x000132F0
		private void EmitUserDefinedLiftedOperator(EmitContext ec)
		{
			LocalBuilder local = ec.EmitStored(this.operand);
			ec.EmitNullableGetValue(local);
			ec.EmitCall(this.method);
		}

		// Token: 0x06000480 RID: 1152 RVA: 0x00015120 File Offset: 0x00013320
		private void EmitUserDefinedOperator(EmitContext ec)
		{
			if (!this.IsLifted)
			{
				ec.Emit(this.operand);
				ec.EmitCall(this.method);
			}
			else if (this.IsLiftedToNull)
			{
				this.EmitUserDefinedLiftedToNullOperator(ec);
			}
			else
			{
				this.EmitUserDefinedLiftedOperator(ec);
			}
		}

		// Token: 0x06000481 RID: 1153 RVA: 0x00015174 File Offset: 0x00013374
		private void EmitQuote(EmitContext ec)
		{
			ec.EmitScope();
			ec.EmitReadGlobal(this.operand, typeof(Expression));
			if (ec.HasHoistedLocals)
			{
				ec.EmitLoadHoistedLocalsStore();
			}
			else
			{
				ec.ig.Emit(OpCodes.Ldnull);
			}
			ec.EmitIsolateExpression();
		}

		// Token: 0x06000482 RID: 1154 RVA: 0x000151CC File Offset: 0x000133CC
		internal override void Emit(EmitContext ec)
		{
			if (this.method != null)
			{
				this.EmitUserDefinedOperator(ec);
				return;
			}
			ExpressionType nodeType = base.NodeType;
			switch (nodeType)
			{
			case ExpressionType.Negate:
			case ExpressionType.UnaryPlus:
			case ExpressionType.NegateChecked:
			case ExpressionType.Not:
				this.EmitArithmeticUnary(ec);
				return;
			default:
				if (nodeType == ExpressionType.Convert || nodeType == ExpressionType.ConvertChecked)
				{
					this.EmitConvert(ec);
					return;
				}
				if (nodeType == ExpressionType.ArrayLength)
				{
					this.EmitArrayLength(ec);
					return;
				}
				if (nodeType == ExpressionType.Quote)
				{
					this.EmitQuote(ec);
					return;
				}
				if (nodeType != ExpressionType.TypeAs)
				{
					throw new NotImplementedException(base.NodeType.ToString());
				}
				this.EmitTypeAs(ec);
				return;
			}
		}

		// Token: 0x04000105 RID: 261
		private Expression operand;

		// Token: 0x04000106 RID: 262
		private MethodInfo method;

		// Token: 0x04000107 RID: 263
		private bool is_lifted;
	}
}
