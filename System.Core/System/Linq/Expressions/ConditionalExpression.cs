﻿using System;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000035 RID: 53
	public sealed class ConditionalExpression : Expression
	{
		// Token: 0x0600036B RID: 875 RVA: 0x000112E4 File Offset: 0x0000F4E4
		internal ConditionalExpression(Expression test, Expression if_true, Expression if_false) : base(ExpressionType.Conditional, if_true.Type)
		{
			this.test = test;
			this.if_true = if_true;
			this.if_false = if_false;
		}

		// Token: 0x17000034 RID: 52
		// (get) Token: 0x0600036C RID: 876 RVA: 0x00011314 File Offset: 0x0000F514
		public Expression Test
		{
			get
			{
				return this.test;
			}
		}

		// Token: 0x17000035 RID: 53
		// (get) Token: 0x0600036D RID: 877 RVA: 0x0001131C File Offset: 0x0000F51C
		public Expression IfTrue
		{
			get
			{
				return this.if_true;
			}
		}

		// Token: 0x17000036 RID: 54
		// (get) Token: 0x0600036E RID: 878 RVA: 0x00011324 File Offset: 0x0000F524
		public Expression IfFalse
		{
			get
			{
				return this.if_false;
			}
		}

		// Token: 0x0600036F RID: 879 RVA: 0x0001132C File Offset: 0x0000F52C
		internal override void Emit(EmitContext ec)
		{
			ILGenerator ig = ec.ig;
			Label label = ig.DefineLabel();
			Label label2 = ig.DefineLabel();
			this.test.Emit(ec);
			ig.Emit(OpCodes.Brfalse, label);
			this.if_true.Emit(ec);
			ig.Emit(OpCodes.Br, label2);
			ig.MarkLabel(label);
			this.if_false.Emit(ec);
			ig.MarkLabel(label2);
		}

		// Token: 0x040000A3 RID: 163
		private Expression test;

		// Token: 0x040000A4 RID: 164
		private Expression if_true;

		// Token: 0x040000A5 RID: 165
		private Expression if_false;
	}
}
