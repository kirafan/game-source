﻿using System;
using System.Collections.ObjectModel;
using System.Text;

namespace System.Linq.Expressions
{
	// Token: 0x0200003C RID: 60
	internal class ExpressionPrinter : ExpressionVisitor
	{
		// Token: 0x060003C2 RID: 962 RVA: 0x000125B0 File Offset: 0x000107B0
		private ExpressionPrinter(StringBuilder builder)
		{
			this.builder = builder;
		}

		// Token: 0x060003C3 RID: 963 RVA: 0x000125C0 File Offset: 0x000107C0
		private ExpressionPrinter() : this(new StringBuilder())
		{
		}

		// Token: 0x060003C4 RID: 964 RVA: 0x000125D0 File Offset: 0x000107D0
		public static string ToString(Expression expression)
		{
			ExpressionPrinter expressionPrinter = new ExpressionPrinter();
			expressionPrinter.Visit(expression);
			return expressionPrinter.builder.ToString();
		}

		// Token: 0x060003C5 RID: 965 RVA: 0x000125F8 File Offset: 0x000107F8
		public static string ToString(ElementInit init)
		{
			ExpressionPrinter expressionPrinter = new ExpressionPrinter();
			expressionPrinter.VisitElementInitializer(init);
			return expressionPrinter.builder.ToString();
		}

		// Token: 0x060003C6 RID: 966 RVA: 0x00012620 File Offset: 0x00010820
		public static string ToString(MemberBinding binding)
		{
			ExpressionPrinter expressionPrinter = new ExpressionPrinter();
			expressionPrinter.VisitBinding(binding);
			return expressionPrinter.builder.ToString();
		}

		// Token: 0x060003C7 RID: 967 RVA: 0x00012648 File Offset: 0x00010848
		private void Print(string str)
		{
			this.builder.Append(str);
		}

		// Token: 0x060003C8 RID: 968 RVA: 0x00012658 File Offset: 0x00010858
		private void Print(object obj)
		{
			this.builder.Append(obj);
		}

		// Token: 0x060003C9 RID: 969 RVA: 0x00012668 File Offset: 0x00010868
		private void Print(string str, params object[] objs)
		{
			this.builder.AppendFormat(str, objs);
		}

		// Token: 0x060003CA RID: 970 RVA: 0x00012678 File Offset: 0x00010878
		protected override void VisitElementInitializer(ElementInit initializer)
		{
			this.Print(initializer.AddMethod);
			this.Print("(");
			this.VisitExpressionList(initializer.Arguments);
			this.Print(")");
		}

		// Token: 0x060003CB RID: 971 RVA: 0x000126B4 File Offset: 0x000108B4
		protected override void VisitUnary(UnaryExpression unary)
		{
			ExpressionType nodeType = unary.NodeType;
			if (nodeType != ExpressionType.Convert && nodeType != ExpressionType.ConvertChecked)
			{
				if (nodeType == ExpressionType.Negate)
				{
					this.Print("-");
					this.Visit(unary.Operand);
					return;
				}
				if (nodeType == ExpressionType.UnaryPlus)
				{
					this.Print("+");
					this.Visit(unary.Operand);
					return;
				}
				if (nodeType != ExpressionType.ArrayLength && nodeType != ExpressionType.Not)
				{
					if (nodeType == ExpressionType.Quote)
					{
						this.Visit(unary.Operand);
						return;
					}
					if (nodeType != ExpressionType.TypeAs)
					{
						throw new NotImplementedException();
					}
					this.Print("(");
					this.Visit(unary.Operand);
					this.Print(" As {0})", new object[]
					{
						unary.Type.Name
					});
					return;
				}
			}
			this.Print("{0}(", new object[]
			{
				unary.NodeType
			});
			this.Visit(unary.Operand);
			this.Print(")");
		}

		// Token: 0x060003CC RID: 972 RVA: 0x000127BC File Offset: 0x000109BC
		private static string OperatorToString(BinaryExpression binary)
		{
			switch (binary.NodeType)
			{
			case ExpressionType.Add:
			case ExpressionType.AddChecked:
				return "+";
			case ExpressionType.And:
				return (!ExpressionPrinter.IsBoolean(binary)) ? "&" : "And";
			case ExpressionType.AndAlso:
				return "&&";
			case ExpressionType.Coalesce:
				return "??";
			case ExpressionType.Divide:
				return "/";
			case ExpressionType.Equal:
				return "=";
			case ExpressionType.ExclusiveOr:
				return "^";
			case ExpressionType.GreaterThan:
				return ">";
			case ExpressionType.GreaterThanOrEqual:
				return ">=";
			case ExpressionType.LeftShift:
				return "<<";
			case ExpressionType.LessThan:
				return "<";
			case ExpressionType.LessThanOrEqual:
				return "<=";
			case ExpressionType.Modulo:
				return "%";
			case ExpressionType.Multiply:
			case ExpressionType.MultiplyChecked:
				return "*";
			case ExpressionType.NotEqual:
				return "!=";
			case ExpressionType.Or:
				return (!ExpressionPrinter.IsBoolean(binary)) ? "|" : "Or";
			case ExpressionType.OrElse:
				return "||";
			case ExpressionType.Power:
				return "^";
			case ExpressionType.RightShift:
				return ">>";
			case ExpressionType.Subtract:
			case ExpressionType.SubtractChecked:
				return "-";
			}
			return null;
		}

		// Token: 0x060003CD RID: 973 RVA: 0x00012930 File Offset: 0x00010B30
		private static bool IsBoolean(Expression expression)
		{
			return expression.Type == typeof(bool) || expression.Type == typeof(bool?);
		}

		// Token: 0x060003CE RID: 974 RVA: 0x00012968 File Offset: 0x00010B68
		private void PrintArrayIndex(BinaryExpression index)
		{
			this.Visit(index.Left);
			this.Print("[");
			this.Visit(index.Right);
			this.Print("]");
		}

		// Token: 0x060003CF RID: 975 RVA: 0x000129A4 File Offset: 0x00010BA4
		protected override void VisitBinary(BinaryExpression binary)
		{
			ExpressionType nodeType = binary.NodeType;
			if (nodeType != ExpressionType.ArrayIndex)
			{
				this.Print("(");
				this.Visit(binary.Left);
				this.Print(" {0} ", new object[]
				{
					ExpressionPrinter.OperatorToString(binary)
				});
				this.Visit(binary.Right);
				this.Print(")");
				return;
			}
			this.PrintArrayIndex(binary);
		}

		// Token: 0x060003D0 RID: 976 RVA: 0x00012A14 File Offset: 0x00010C14
		protected override void VisitTypeIs(TypeBinaryExpression type)
		{
			ExpressionType nodeType = type.NodeType;
			if (nodeType != ExpressionType.TypeIs)
			{
				throw new NotImplementedException();
			}
			this.Print("(");
			this.Visit(type.Expression);
			this.Print(" Is {0})", new object[]
			{
				type.TypeOperand.Name
			});
		}

		// Token: 0x060003D1 RID: 977 RVA: 0x00012A74 File Offset: 0x00010C74
		protected override void VisitConstant(ConstantExpression constant)
		{
			object value = constant.Value;
			if (value == null)
			{
				this.Print("null");
			}
			else if (value is string)
			{
				this.Print("\"");
				this.Print(value);
				this.Print("\"");
			}
			else if (!ExpressionPrinter.HasStringRepresentation(value))
			{
				this.Print("value(");
				this.Print(value);
				this.Print(")");
			}
			else
			{
				this.Print(value);
			}
		}

		// Token: 0x060003D2 RID: 978 RVA: 0x00012B00 File Offset: 0x00010D00
		private static bool HasStringRepresentation(object obj)
		{
			return obj.ToString() != obj.GetType().ToString();
		}

		// Token: 0x060003D3 RID: 979 RVA: 0x00012B24 File Offset: 0x00010D24
		protected override void VisitConditional(ConditionalExpression conditional)
		{
			this.Print("IIF(");
			this.Visit(conditional.Test);
			this.Print(", ");
			this.Visit(conditional.IfTrue);
			this.Print(", ");
			this.Visit(conditional.IfFalse);
			this.Print(")");
		}

		// Token: 0x060003D4 RID: 980 RVA: 0x00012B84 File Offset: 0x00010D84
		protected override void VisitParameter(ParameterExpression parameter)
		{
			this.Print(parameter.Name ?? "<param>");
		}

		// Token: 0x060003D5 RID: 981 RVA: 0x00012BA0 File Offset: 0x00010DA0
		protected override void VisitMemberAccess(MemberExpression access)
		{
			if (access.Expression == null)
			{
				this.Print(access.Member.DeclaringType.Name);
			}
			else
			{
				this.Visit(access.Expression);
			}
			this.Print(".{0}", new object[]
			{
				access.Member.Name
			});
		}

		// Token: 0x060003D6 RID: 982 RVA: 0x00012C00 File Offset: 0x00010E00
		protected override void VisitMethodCall(MethodCallExpression call)
		{
			if (call.Object != null)
			{
				this.Visit(call.Object);
				this.Print(".");
			}
			this.Print(call.Method.Name);
			this.Print("(");
			this.VisitExpressionList(call.Arguments);
			this.Print(")");
		}

		// Token: 0x060003D7 RID: 983 RVA: 0x00012C64 File Offset: 0x00010E64
		protected override void VisitMemberAssignment(MemberAssignment assignment)
		{
			this.Print("{0} = ", new object[]
			{
				assignment.Member.Name
			});
			this.Visit(assignment.Expression);
		}

		// Token: 0x060003D8 RID: 984 RVA: 0x00012C9C File Offset: 0x00010E9C
		protected override void VisitMemberMemberBinding(MemberMemberBinding binding)
		{
			this.Print(binding.Member.Name);
			this.Print(" = {");
			this.VisitList<MemberBinding>(binding.Bindings, new Action<MemberBinding>(this.VisitBinding));
			this.Print("}");
		}

		// Token: 0x060003D9 RID: 985 RVA: 0x00012CEC File Offset: 0x00010EEC
		protected override void VisitMemberListBinding(MemberListBinding binding)
		{
			this.Print(binding.Member.Name);
			this.Print(" = {");
			this.VisitList<ElementInit>(binding.Initializers, new Action<ElementInit>(this.VisitElementInitializer));
			this.Print("}");
		}

		// Token: 0x060003DA RID: 986 RVA: 0x00012D3C File Offset: 0x00010F3C
		protected override void VisitList<T>(ReadOnlyCollection<T> list, Action<T> visitor)
		{
			for (int i = 0; i < list.Count; i++)
			{
				if (i > 0)
				{
					this.Print(", ");
				}
				visitor(list[i]);
			}
		}

		// Token: 0x060003DB RID: 987 RVA: 0x00012D80 File Offset: 0x00010F80
		protected override void VisitLambda(LambdaExpression lambda)
		{
			if (lambda.Parameters.Count != 1)
			{
				this.Print("(");
				this.VisitList<ParameterExpression>(lambda.Parameters, new Action<ParameterExpression>(this.Visit));
				this.Print(")");
			}
			else
			{
				this.Visit(lambda.Parameters[0]);
			}
			this.Print(" => ");
			this.Visit(lambda.Body);
		}

		// Token: 0x060003DC RID: 988 RVA: 0x00012DFC File Offset: 0x00010FFC
		protected override void VisitNew(NewExpression nex)
		{
			this.Print("new {0}(", new object[]
			{
				nex.Type.Name
			});
			if (nex.Members != null && nex.Members.Count > 0)
			{
				for (int i = 0; i < nex.Members.Count; i++)
				{
					if (i > 0)
					{
						this.Print(", ");
					}
					this.Print("{0} = ", new object[]
					{
						nex.Members[i].Name
					});
					this.Visit(nex.Arguments[i]);
				}
			}
			else
			{
				this.VisitExpressionList(nex.Arguments);
			}
			this.Print(")");
		}

		// Token: 0x060003DD RID: 989 RVA: 0x00012EC8 File Offset: 0x000110C8
		protected override void VisitMemberInit(MemberInitExpression init)
		{
			this.Visit(init.NewExpression);
			this.Print(" {");
			this.VisitList<MemberBinding>(init.Bindings, new Action<MemberBinding>(this.VisitBinding));
			this.Print("}");
		}

		// Token: 0x060003DE RID: 990 RVA: 0x00012F10 File Offset: 0x00011110
		protected override void VisitListInit(ListInitExpression init)
		{
			this.Visit(init.NewExpression);
			this.Print(" {");
			this.VisitList<ElementInit>(init.Initializers, new Action<ElementInit>(this.VisitElementInitializer));
			this.Print("}");
		}

		// Token: 0x060003DF RID: 991 RVA: 0x00012F58 File Offset: 0x00011158
		protected override void VisitNewArray(NewArrayExpression newArray)
		{
			this.Print("new ");
			ExpressionType nodeType = newArray.NodeType;
			if (nodeType == ExpressionType.NewArrayInit)
			{
				this.Print("[] {");
				this.VisitExpressionList(newArray.Expressions);
				this.Print("}");
				return;
			}
			if (nodeType != ExpressionType.NewArrayBounds)
			{
				throw new NotSupportedException();
			}
			this.Print(newArray.Type);
			this.Print("(");
			this.VisitExpressionList(newArray.Expressions);
			this.Print(")");
		}

		// Token: 0x060003E0 RID: 992 RVA: 0x00012FE4 File Offset: 0x000111E4
		protected override void VisitInvocation(InvocationExpression invocation)
		{
			this.Print("Invoke(");
			this.Visit(invocation.Expression);
			if (invocation.Arguments.Count != 0)
			{
				this.Print(", ");
				this.VisitExpressionList(invocation.Arguments);
			}
			this.Print(")");
		}

		// Token: 0x040000B9 RID: 185
		private const string ListSeparator = ", ";

		// Token: 0x040000BA RID: 186
		private StringBuilder builder;
	}
}
