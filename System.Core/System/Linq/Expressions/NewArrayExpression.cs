﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x0200004B RID: 75
	public sealed class NewArrayExpression : Expression
	{
		// Token: 0x0600044F RID: 1103 RVA: 0x000144A8 File Offset: 0x000126A8
		internal NewArrayExpression(ExpressionType et, Type type, ReadOnlyCollection<Expression> expressions) : base(et, type)
		{
			this.expressions = expressions;
		}

		// Token: 0x1700004C RID: 76
		// (get) Token: 0x06000450 RID: 1104 RVA: 0x000144BC File Offset: 0x000126BC
		public ReadOnlyCollection<Expression> Expressions
		{
			get
			{
				return this.expressions;
			}
		}

		// Token: 0x06000451 RID: 1105 RVA: 0x000144C4 File Offset: 0x000126C4
		private void EmitNewArrayInit(EmitContext ec, Type type)
		{
			int count = this.expressions.Count;
			ec.ig.Emit(OpCodes.Ldc_I4, count);
			ec.ig.Emit(OpCodes.Newarr, type);
			for (int i = 0; i < count; i++)
			{
				ec.ig.Emit(OpCodes.Dup);
				ec.ig.Emit(OpCodes.Ldc_I4, i);
				this.expressions[i].Emit(ec);
				ec.ig.Emit(OpCodes.Stelem, type);
			}
		}

		// Token: 0x06000452 RID: 1106 RVA: 0x00014558 File Offset: 0x00012758
		private void EmitNewArrayBounds(EmitContext ec, Type type)
		{
			int count = this.expressions.Count;
			ec.EmitCollection<Expression>(this.expressions);
			if (count == 1)
			{
				ec.ig.Emit(OpCodes.Newarr, type);
				return;
			}
			ec.ig.Emit(OpCodes.Newobj, NewArrayExpression.GetArrayConstructor(type, count));
		}

		// Token: 0x06000453 RID: 1107 RVA: 0x000145B0 File Offset: 0x000127B0
		private static ConstructorInfo GetArrayConstructor(Type type, int rank)
		{
			return NewArrayExpression.CreateArray(type, rank).GetConstructor(NewArrayExpression.CreateTypeParameters(rank));
		}

		// Token: 0x06000454 RID: 1108 RVA: 0x000145C4 File Offset: 0x000127C4
		private static Type[] CreateTypeParameters(int rank)
		{
			return Enumerable.Repeat<Type>(typeof(int), rank).ToArray<Type>();
		}

		// Token: 0x06000455 RID: 1109 RVA: 0x000145DC File Offset: 0x000127DC
		private static Type CreateArray(Type type, int rank)
		{
			return type.MakeArrayType(rank);
		}

		// Token: 0x06000456 RID: 1110 RVA: 0x000145E8 File Offset: 0x000127E8
		internal override void Emit(EmitContext ec)
		{
			Type elementType = base.Type.GetElementType();
			ExpressionType nodeType = base.NodeType;
			if (nodeType == ExpressionType.NewArrayInit)
			{
				this.EmitNewArrayInit(ec, elementType);
				return;
			}
			if (nodeType != ExpressionType.NewArrayBounds)
			{
				throw new NotSupportedException();
			}
			this.EmitNewArrayBounds(ec, elementType);
		}

		// Token: 0x040000FE RID: 254
		private ReadOnlyCollection<Expression> expressions;
	}
}
