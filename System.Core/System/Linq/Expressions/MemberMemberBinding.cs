﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000049 RID: 73
	public sealed class MemberMemberBinding : MemberBinding
	{
		// Token: 0x06000446 RID: 1094 RVA: 0x00014398 File Offset: 0x00012598
		internal MemberMemberBinding(MemberInfo member, ReadOnlyCollection<MemberBinding> bindings) : base(MemberBindingType.MemberBinding, member)
		{
			this.bindings = bindings;
		}

		// Token: 0x17000048 RID: 72
		// (get) Token: 0x06000447 RID: 1095 RVA: 0x000143AC File Offset: 0x000125AC
		public ReadOnlyCollection<MemberBinding> Bindings
		{
			get
			{
				return this.bindings;
			}
		}

		// Token: 0x06000448 RID: 1096 RVA: 0x000143B4 File Offset: 0x000125B4
		internal override void Emit(EmitContext ec, LocalBuilder local)
		{
			LocalBuilder local2 = base.EmitLoadMember(ec, local);
			foreach (MemberBinding memberBinding in this.bindings)
			{
				memberBinding.Emit(ec, local2);
			}
		}

		// Token: 0x040000FA RID: 250
		private ReadOnlyCollection<MemberBinding> bindings;
	}
}
