﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000046 RID: 70
	public sealed class MemberExpression : Expression
	{
		// Token: 0x06000439 RID: 1081 RVA: 0x000141BC File Offset: 0x000123BC
		internal MemberExpression(Expression expression, MemberInfo member, Type type) : base(ExpressionType.MemberAccess, type)
		{
			this.expression = expression;
			this.member = member;
		}

		// Token: 0x17000043 RID: 67
		// (get) Token: 0x0600043A RID: 1082 RVA: 0x000141D8 File Offset: 0x000123D8
		public Expression Expression
		{
			get
			{
				return this.expression;
			}
		}

		// Token: 0x17000044 RID: 68
		// (get) Token: 0x0600043B RID: 1083 RVA: 0x000141E0 File Offset: 0x000123E0
		public MemberInfo Member
		{
			get
			{
				return this.member;
			}
		}

		// Token: 0x0600043C RID: 1084 RVA: 0x000141E8 File Offset: 0x000123E8
		internal override void Emit(EmitContext ec)
		{
			this.member.OnFieldOrProperty(delegate(FieldInfo field)
			{
				this.EmitFieldAccess(ec, field);
			}, delegate(PropertyInfo prop)
			{
				this.EmitPropertyAccess(ec, prop);
			});
		}

		// Token: 0x0600043D RID: 1085 RVA: 0x0001422C File Offset: 0x0001242C
		private void EmitPropertyAccess(EmitContext ec, PropertyInfo property)
		{
			MethodInfo getMethod = property.GetGetMethod(true);
			if (!getMethod.IsStatic)
			{
				ec.EmitLoadSubject(this.expression);
			}
			ec.EmitCall(getMethod);
		}

		// Token: 0x0600043E RID: 1086 RVA: 0x00014260 File Offset: 0x00012460
		private void EmitFieldAccess(EmitContext ec, FieldInfo field)
		{
			if (!field.IsStatic)
			{
				ec.EmitLoadSubject(this.expression);
				ec.ig.Emit(OpCodes.Ldfld, field);
			}
			else
			{
				ec.ig.Emit(OpCodes.Ldsfld, field);
			}
		}

		// Token: 0x040000F5 RID: 245
		private Expression expression;

		// Token: 0x040000F6 RID: 246
		private MemberInfo member;
	}
}
