﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;

namespace System.Linq.Expressions
{
	// Token: 0x0200004A RID: 74
	public sealed class MethodCallExpression : Expression
	{
		// Token: 0x06000449 RID: 1097 RVA: 0x00014424 File Offset: 0x00012624
		internal MethodCallExpression(MethodInfo method, ReadOnlyCollection<Expression> arguments) : base(ExpressionType.Call, method.ReturnType)
		{
			this.method = method;
			this.arguments = arguments;
		}

		// Token: 0x0600044A RID: 1098 RVA: 0x00014444 File Offset: 0x00012644
		internal MethodCallExpression(Expression obj, MethodInfo method, ReadOnlyCollection<Expression> arguments) : base(ExpressionType.Call, method.ReturnType)
		{
			this.obj = obj;
			this.method = method;
			this.arguments = arguments;
		}

		// Token: 0x17000049 RID: 73
		// (get) Token: 0x0600044B RID: 1099 RVA: 0x00014474 File Offset: 0x00012674
		public Expression Object
		{
			get
			{
				return this.obj;
			}
		}

		// Token: 0x1700004A RID: 74
		// (get) Token: 0x0600044C RID: 1100 RVA: 0x0001447C File Offset: 0x0001267C
		public MethodInfo Method
		{
			get
			{
				return this.method;
			}
		}

		// Token: 0x1700004B RID: 75
		// (get) Token: 0x0600044D RID: 1101 RVA: 0x00014484 File Offset: 0x00012684
		public ReadOnlyCollection<Expression> Arguments
		{
			get
			{
				return this.arguments;
			}
		}

		// Token: 0x0600044E RID: 1102 RVA: 0x0001448C File Offset: 0x0001268C
		internal override void Emit(EmitContext ec)
		{
			ec.EmitCall(this.obj, this.arguments, this.method);
		}

		// Token: 0x040000FB RID: 251
		private Expression obj;

		// Token: 0x040000FC RID: 252
		private MethodInfo method;

		// Token: 0x040000FD RID: 253
		private ReadOnlyCollection<Expression> arguments;
	}
}
