﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Reflection.Emit;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x0200003B RID: 59
	internal class EmitContext
	{
		// Token: 0x06000391 RID: 913 RVA: 0x00011C08 File Offset: 0x0000FE08
		public EmitContext(CompilationContext context, EmitContext parent, LambdaExpression lambda)
		{
			this.context = context;
			this.parent = parent;
			this.lambda = lambda;
			this.hoisted = context.GetHoistedLocals(lambda);
			this.method = new DynamicMethod("lambda_method", lambda.GetReturnType(), EmitContext.CreateParameterTypes(lambda.Parameters), typeof(ExecutionScope), true);
			this.ig = this.method.GetILGenerator();
		}

		// Token: 0x1700003A RID: 58
		// (get) Token: 0x06000392 RID: 914 RVA: 0x00011C7C File Offset: 0x0000FE7C
		public bool HasHoistedLocals
		{
			get
			{
				return this.hoisted != null && this.hoisted.Count > 0;
			}
		}

		// Token: 0x1700003B RID: 59
		// (get) Token: 0x06000393 RID: 915 RVA: 0x00011C9C File Offset: 0x0000FE9C
		public LambdaExpression Lambda
		{
			get
			{
				return this.lambda;
			}
		}

		// Token: 0x06000394 RID: 916 RVA: 0x00011CA4 File Offset: 0x0000FEA4
		public void Emit()
		{
			if (this.HasHoistedLocals)
			{
				this.EmitStoreHoistedLocals();
			}
			this.lambda.EmitBody(this);
		}

		// Token: 0x06000395 RID: 917 RVA: 0x00011CC4 File Offset: 0x0000FEC4
		private static Type[] CreateParameterTypes(IList<ParameterExpression> parameters)
		{
			Type[] array = new Type[parameters.Count + 1];
			array[0] = typeof(ExecutionScope);
			for (int i = 0; i < parameters.Count; i++)
			{
				array[i + 1] = parameters[i].Type;
			}
			return array;
		}

		// Token: 0x06000396 RID: 918 RVA: 0x00011D18 File Offset: 0x0000FF18
		public bool IsLocalParameter(ParameterExpression parameter, ref int position)
		{
			position = this.lambda.Parameters.IndexOf(parameter);
			if (position > -1)
			{
				position++;
				return true;
			}
			return false;
		}

		// Token: 0x06000397 RID: 919 RVA: 0x00011D4C File Offset: 0x0000FF4C
		public Delegate CreateDelegate(ExecutionScope scope)
		{
			return this.method.CreateDelegate(this.lambda.Type, scope);
		}

		// Token: 0x06000398 RID: 920 RVA: 0x00011D68 File Offset: 0x0000FF68
		public void Emit(Expression expression)
		{
			expression.Emit(this);
		}

		// Token: 0x06000399 RID: 921 RVA: 0x00011D74 File Offset: 0x0000FF74
		public LocalBuilder EmitStored(Expression expression)
		{
			LocalBuilder localBuilder = this.ig.DeclareLocal(expression.Type);
			expression.Emit(this);
			this.ig.Emit(OpCodes.Stloc, localBuilder);
			return localBuilder;
		}

		// Token: 0x0600039A RID: 922 RVA: 0x00011DAC File Offset: 0x0000FFAC
		public void EmitLoadAddress(Expression expression)
		{
			this.ig.Emit(OpCodes.Ldloca, this.EmitStored(expression));
		}

		// Token: 0x0600039B RID: 923 RVA: 0x00011DC8 File Offset: 0x0000FFC8
		public void EmitLoadSubject(Expression expression)
		{
			if (expression.Type.IsValueType)
			{
				this.EmitLoadAddress(expression);
				return;
			}
			this.Emit(expression);
		}

		// Token: 0x0600039C RID: 924 RVA: 0x00011DF4 File Offset: 0x0000FFF4
		public void EmitLoadSubject(LocalBuilder local)
		{
			if (local.LocalType.IsValueType)
			{
				this.EmitLoadAddress(local);
				return;
			}
			this.EmitLoad(local);
		}

		// Token: 0x0600039D RID: 925 RVA: 0x00011E20 File Offset: 0x00010020
		public void EmitLoadAddress(LocalBuilder local)
		{
			this.ig.Emit(OpCodes.Ldloca, local);
		}

		// Token: 0x0600039E RID: 926 RVA: 0x00011E34 File Offset: 0x00010034
		public void EmitLoad(LocalBuilder local)
		{
			this.ig.Emit(OpCodes.Ldloc, local);
		}

		// Token: 0x0600039F RID: 927 RVA: 0x00011E48 File Offset: 0x00010048
		public void EmitCall(LocalBuilder local, IList<Expression> arguments, MethodInfo method)
		{
			this.EmitLoadSubject(local);
			this.EmitArguments(method, arguments);
			this.EmitCall(method);
		}

		// Token: 0x060003A0 RID: 928 RVA: 0x00011E60 File Offset: 0x00010060
		public void EmitCall(LocalBuilder local, MethodInfo method)
		{
			this.EmitLoadSubject(local);
			this.EmitCall(method);
		}

		// Token: 0x060003A1 RID: 929 RVA: 0x00011E70 File Offset: 0x00010070
		public void EmitCall(Expression expression, MethodInfo method)
		{
			if (!method.IsStatic)
			{
				this.EmitLoadSubject(expression);
			}
			this.EmitCall(method);
		}

		// Token: 0x060003A2 RID: 930 RVA: 0x00011E8C File Offset: 0x0001008C
		public void EmitCall(Expression expression, IList<Expression> arguments, MethodInfo method)
		{
			if (!method.IsStatic)
			{
				this.EmitLoadSubject(expression);
			}
			this.EmitArguments(method, arguments);
			this.EmitCall(method);
		}

		// Token: 0x060003A3 RID: 931 RVA: 0x00011EBC File Offset: 0x000100BC
		private void EmitArguments(MethodInfo method, IList<Expression> arguments)
		{
			ParameterInfo[] parameters = method.GetParameters();
			for (int i = 0; i < parameters.Length; i++)
			{
				ParameterInfo parameterInfo = parameters[i];
				Expression expression = arguments[i];
				if (parameterInfo.ParameterType.IsByRef)
				{
					this.ig.Emit(OpCodes.Ldloca, this.EmitStored(expression));
				}
				else
				{
					this.Emit(arguments[i]);
				}
			}
		}

		// Token: 0x060003A4 RID: 932 RVA: 0x00011F2C File Offset: 0x0001012C
		public void EmitCall(MethodInfo method)
		{
			this.ig.Emit((!method.IsVirtual) ? OpCodes.Call : OpCodes.Callvirt, method);
		}

		// Token: 0x060003A5 RID: 933 RVA: 0x00011F60 File Offset: 0x00010160
		public void EmitNullableHasValue(LocalBuilder local)
		{
			this.EmitCall(local, "get_HasValue");
		}

		// Token: 0x060003A6 RID: 934 RVA: 0x00011F70 File Offset: 0x00010170
		public void EmitNullableInitialize(LocalBuilder local)
		{
			this.ig.Emit(OpCodes.Ldloca, local);
			this.ig.Emit(OpCodes.Initobj, local.LocalType);
			this.ig.Emit(OpCodes.Ldloc, local);
		}

		// Token: 0x060003A7 RID: 935 RVA: 0x00011FB8 File Offset: 0x000101B8
		public void EmitNullableGetValue(LocalBuilder local)
		{
			this.EmitCall(local, "get_Value");
		}

		// Token: 0x060003A8 RID: 936 RVA: 0x00011FC8 File Offset: 0x000101C8
		public void EmitNullableGetValueOrDefault(LocalBuilder local)
		{
			this.EmitCall(local, "GetValueOrDefault");
		}

		// Token: 0x060003A9 RID: 937 RVA: 0x00011FD8 File Offset: 0x000101D8
		private void EmitCall(LocalBuilder local, string method_name)
		{
			this.EmitCall(local, local.LocalType.GetMethod(method_name, Type.EmptyTypes));
		}

		// Token: 0x060003AA RID: 938 RVA: 0x00011FF4 File Offset: 0x000101F4
		public void EmitNullableNew(Type of)
		{
			this.ig.Emit(OpCodes.Newobj, of.GetConstructor(new Type[]
			{
				of.GetFirstGenericArgument()
			}));
		}

		// Token: 0x060003AB RID: 939 RVA: 0x0001201C File Offset: 0x0001021C
		public void EmitCollection<T>(IEnumerable<T> collection) where T : Expression
		{
			foreach (T t in collection)
			{
				t.Emit(this);
			}
		}

		// Token: 0x060003AC RID: 940 RVA: 0x00012084 File Offset: 0x00010284
		public void EmitCollection(IEnumerable<ElementInit> initializers, LocalBuilder local)
		{
			foreach (ElementInit elementInit in initializers)
			{
				elementInit.Emit(this, local);
			}
		}

		// Token: 0x060003AD RID: 941 RVA: 0x000120E4 File Offset: 0x000102E4
		public void EmitCollection(IEnumerable<MemberBinding> bindings, LocalBuilder local)
		{
			foreach (MemberBinding memberBinding in bindings)
			{
				memberBinding.Emit(this, local);
			}
		}

		// Token: 0x060003AE RID: 942 RVA: 0x00012144 File Offset: 0x00010344
		public void EmitIsInst(Expression expression, Type candidate)
		{
			expression.Emit(this);
			Type type = expression.Type;
			if (type.IsValueType)
			{
				this.ig.Emit(OpCodes.Box, type);
			}
			this.ig.Emit(OpCodes.Isinst, candidate);
		}

		// Token: 0x060003AF RID: 943 RVA: 0x0001218C File Offset: 0x0001038C
		public void EmitScope()
		{
			this.ig.Emit(OpCodes.Ldarg_0);
		}

		// Token: 0x060003B0 RID: 944 RVA: 0x000121A0 File Offset: 0x000103A0
		public void EmitReadGlobal(object global)
		{
			this.EmitReadGlobal(global, global.GetType());
		}

		// Token: 0x060003B1 RID: 945 RVA: 0x000121B0 File Offset: 0x000103B0
		public void EmitLoadGlobals()
		{
			this.EmitScope();
			this.ig.Emit(OpCodes.Ldfld, typeof(ExecutionScope).GetField("Globals"));
		}

		// Token: 0x060003B2 RID: 946 RVA: 0x000121E8 File Offset: 0x000103E8
		public void EmitReadGlobal(object global, Type type)
		{
			this.EmitLoadGlobals();
			this.ig.Emit(OpCodes.Ldc_I4, this.AddGlobal(global, type));
			this.ig.Emit(OpCodes.Ldelem, typeof(object));
			this.EmitLoadStrongBoxValue(type);
		}

		// Token: 0x060003B3 RID: 947 RVA: 0x00012234 File Offset: 0x00010434
		public void EmitLoadStrongBoxValue(Type type)
		{
			Type type2 = type.MakeStrongBoxType();
			this.ig.Emit(OpCodes.Isinst, type2);
			this.ig.Emit(OpCodes.Ldfld, type2.GetField("Value"));
		}

		// Token: 0x060003B4 RID: 948 RVA: 0x00012274 File Offset: 0x00010474
		private int AddGlobal(object value, Type type)
		{
			return this.context.AddGlobal(EmitContext.CreateStrongBox(value, type));
		}

		// Token: 0x060003B5 RID: 949 RVA: 0x00012288 File Offset: 0x00010488
		public void EmitCreateDelegate(LambdaExpression lambda)
		{
			this.EmitScope();
			this.ig.Emit(OpCodes.Ldc_I4, this.AddChildContext(lambda));
			if (this.hoisted_store != null)
			{
				this.ig.Emit(OpCodes.Ldloc, this.hoisted_store);
			}
			else
			{
				this.ig.Emit(OpCodes.Ldnull);
			}
			this.ig.Emit(OpCodes.Callvirt, typeof(ExecutionScope).GetMethod("CreateDelegate"));
			this.ig.Emit(OpCodes.Castclass, lambda.Type);
		}

		// Token: 0x060003B6 RID: 950 RVA: 0x00012324 File Offset: 0x00010524
		private void EmitStoreHoistedLocals()
		{
			this.EmitHoistedLocalsStore();
			for (int i = 0; i < this.hoisted.Count; i++)
			{
				this.EmitStoreHoistedLocal(i, this.hoisted[i]);
			}
		}

		// Token: 0x060003B7 RID: 951 RVA: 0x00012368 File Offset: 0x00010568
		private void EmitStoreHoistedLocal(int position, ParameterExpression parameter)
		{
			this.ig.Emit(OpCodes.Ldloc, this.hoisted_store);
			this.ig.Emit(OpCodes.Ldc_I4, position);
			parameter.Emit(this);
			this.EmitCreateStrongBox(parameter.Type);
			this.ig.Emit(OpCodes.Stelem, typeof(object));
		}

		// Token: 0x060003B8 RID: 952 RVA: 0x000123CC File Offset: 0x000105CC
		public void EmitLoadHoistedLocalsStore()
		{
			this.ig.Emit(OpCodes.Ldloc, this.hoisted_store);
		}

		// Token: 0x060003B9 RID: 953 RVA: 0x000123E4 File Offset: 0x000105E4
		private void EmitCreateStrongBox(Type type)
		{
			this.ig.Emit(OpCodes.Newobj, type.MakeStrongBoxType().GetConstructor(new Type[]
			{
				type
			}));
		}

		// Token: 0x060003BA RID: 954 RVA: 0x0001240C File Offset: 0x0001060C
		private void EmitHoistedLocalsStore()
		{
			this.EmitScope();
			this.hoisted_store = this.ig.DeclareLocal(typeof(object[]));
			this.ig.Emit(OpCodes.Callvirt, typeof(ExecutionScope).GetMethod("CreateHoistedLocals"));
			this.ig.Emit(OpCodes.Stloc, this.hoisted_store);
		}

		// Token: 0x060003BB RID: 955 RVA: 0x00012474 File Offset: 0x00010674
		public void EmitLoadLocals()
		{
			this.ig.Emit(OpCodes.Ldfld, typeof(ExecutionScope).GetField("Locals"));
		}

		// Token: 0x060003BC RID: 956 RVA: 0x000124A8 File Offset: 0x000106A8
		public void EmitParentScope()
		{
			this.ig.Emit(OpCodes.Ldfld, typeof(ExecutionScope).GetField("Parent"));
		}

		// Token: 0x060003BD RID: 957 RVA: 0x000124DC File Offset: 0x000106DC
		public void EmitIsolateExpression()
		{
			this.ig.Emit(OpCodes.Callvirt, typeof(ExecutionScope).GetMethod("IsolateExpression"));
		}

		// Token: 0x060003BE RID: 958 RVA: 0x00012510 File Offset: 0x00010710
		public int IndexOfHoistedLocal(ParameterExpression parameter)
		{
			if (!this.HasHoistedLocals)
			{
				return -1;
			}
			return this.hoisted.IndexOf(parameter);
		}

		// Token: 0x060003BF RID: 959 RVA: 0x0001252C File Offset: 0x0001072C
		public bool IsHoistedLocal(ParameterExpression parameter, ref int level, ref int position)
		{
			if (this.parent == null)
			{
				return false;
			}
			if (this.parent.hoisted != null)
			{
				position = this.parent.hoisted.IndexOf(parameter);
				if (position > -1)
				{
					return true;
				}
			}
			level++;
			return this.parent.IsHoistedLocal(parameter, ref level, ref position);
		}

		// Token: 0x060003C0 RID: 960 RVA: 0x00012588 File Offset: 0x00010788
		private int AddChildContext(LambdaExpression lambda)
		{
			return this.context.AddCompilationUnit(this, lambda);
		}

		// Token: 0x060003C1 RID: 961 RVA: 0x00012598 File Offset: 0x00010798
		private static object CreateStrongBox(object value, Type type)
		{
			return Activator.CreateInstance(type.MakeStrongBoxType(), new object[]
			{
				value
			});
		}

		// Token: 0x040000B2 RID: 178
		private CompilationContext context;

		// Token: 0x040000B3 RID: 179
		private EmitContext parent;

		// Token: 0x040000B4 RID: 180
		private LambdaExpression lambda;

		// Token: 0x040000B5 RID: 181
		private DynamicMethod method;

		// Token: 0x040000B6 RID: 182
		private LocalBuilder hoisted_store;

		// Token: 0x040000B7 RID: 183
		private List<ParameterExpression> hoisted;

		// Token: 0x040000B8 RID: 184
		public readonly ILGenerator ig;
	}
}
