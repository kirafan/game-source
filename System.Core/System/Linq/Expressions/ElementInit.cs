﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000037 RID: 55
	public sealed class ElementInit
	{
		// Token: 0x06000376 RID: 886 RVA: 0x0001185C File Offset: 0x0000FA5C
		internal ElementInit(MethodInfo add_method, ReadOnlyCollection<Expression> arguments)
		{
			this.add_method = add_method;
			this.arguments = arguments;
		}

		// Token: 0x17000038 RID: 56
		// (get) Token: 0x06000377 RID: 887 RVA: 0x00011874 File Offset: 0x0000FA74
		public MethodInfo AddMethod
		{
			get
			{
				return this.add_method;
			}
		}

		// Token: 0x17000039 RID: 57
		// (get) Token: 0x06000378 RID: 888 RVA: 0x0001187C File Offset: 0x0000FA7C
		public ReadOnlyCollection<Expression> Arguments
		{
			get
			{
				return this.arguments;
			}
		}

		// Token: 0x06000379 RID: 889 RVA: 0x00011884 File Offset: 0x0000FA84
		public override string ToString()
		{
			return ExpressionPrinter.ToString(this);
		}

		// Token: 0x0600037A RID: 890 RVA: 0x0001188C File Offset: 0x0000FA8C
		private void EmitPopIfNeeded(EmitContext ec)
		{
			if (this.add_method.ReturnType == typeof(void))
			{
				return;
			}
			ec.ig.Emit(OpCodes.Pop);
		}

		// Token: 0x0600037B RID: 891 RVA: 0x000118BC File Offset: 0x0000FABC
		internal void Emit(EmitContext ec, LocalBuilder local)
		{
			ec.EmitCall(local, this.arguments, this.add_method);
			this.EmitPopIfNeeded(ec);
		}

		// Token: 0x040000A7 RID: 167
		private MethodInfo add_method;

		// Token: 0x040000A8 RID: 168
		private ReadOnlyCollection<Expression> arguments;
	}
}
