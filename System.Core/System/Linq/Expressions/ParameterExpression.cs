﻿using System;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x0200004D RID: 77
	public sealed class ParameterExpression : Expression
	{
		// Token: 0x0600045E RID: 1118 RVA: 0x00014750 File Offset: 0x00012950
		internal ParameterExpression(Type type, string name) : base(ExpressionType.Parameter, type)
		{
			this.name = name;
		}

		// Token: 0x17000050 RID: 80
		// (get) Token: 0x0600045F RID: 1119 RVA: 0x00014764 File Offset: 0x00012964
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x06000460 RID: 1120 RVA: 0x0001476C File Offset: 0x0001296C
		private void EmitLocalParameter(EmitContext ec, int position)
		{
			ec.ig.Emit(OpCodes.Ldarg, position);
		}

		// Token: 0x06000461 RID: 1121 RVA: 0x00014780 File Offset: 0x00012980
		private void EmitHoistedLocal(EmitContext ec, int level, int position)
		{
			ec.EmitScope();
			for (int i = 0; i < level; i++)
			{
				ec.EmitParentScope();
			}
			ec.EmitLoadLocals();
			ec.ig.Emit(OpCodes.Ldc_I4, position);
			ec.ig.Emit(OpCodes.Ldelem, typeof(object));
			ec.EmitLoadStrongBoxValue(base.Type);
		}

		// Token: 0x06000462 RID: 1122 RVA: 0x000147E8 File Offset: 0x000129E8
		internal override void Emit(EmitContext ec)
		{
			int position = -1;
			if (ec.IsLocalParameter(this, ref position))
			{
				this.EmitLocalParameter(ec, position);
				return;
			}
			int level = 0;
			if (ec.IsHoistedLocal(this, ref level, ref position))
			{
				this.EmitHoistedLocal(ec, level, position);
				return;
			}
			throw new InvalidOperationException("Parameter out of scope");
		}

		// Token: 0x04000102 RID: 258
		private string name;
	}
}
