﻿using System;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000043 RID: 67
	public sealed class MemberAssignment : MemberBinding
	{
		// Token: 0x0600042C RID: 1068 RVA: 0x00013FD4 File Offset: 0x000121D4
		internal MemberAssignment(MemberInfo member, Expression expression) : base(MemberBindingType.Assignment, member)
		{
			this.expression = expression;
		}

		// Token: 0x17000040 RID: 64
		// (get) Token: 0x0600042D RID: 1069 RVA: 0x00013FE8 File Offset: 0x000121E8
		public Expression Expression
		{
			get
			{
				return this.expression;
			}
		}

		// Token: 0x0600042E RID: 1070 RVA: 0x00013FF0 File Offset: 0x000121F0
		internal override void Emit(EmitContext ec, LocalBuilder local)
		{
			base.Member.OnFieldOrProperty(delegate(FieldInfo field)
			{
				this.EmitFieldAssignment(ec, field, local);
			}, delegate(PropertyInfo prop)
			{
				this.EmitPropertyAssignment(ec, prop, local);
			});
		}

		// Token: 0x0600042F RID: 1071 RVA: 0x0001403C File Offset: 0x0001223C
		private void EmitFieldAssignment(EmitContext ec, FieldInfo field, LocalBuilder local)
		{
			ec.EmitLoadSubject(local);
			this.expression.Emit(ec);
			ec.ig.Emit(OpCodes.Stfld, field);
		}

		// Token: 0x06000430 RID: 1072 RVA: 0x00014070 File Offset: 0x00012270
		private void EmitPropertyAssignment(EmitContext ec, PropertyInfo property, LocalBuilder local)
		{
			MethodInfo setMethod = property.GetSetMethod(true);
			if (setMethod == null)
			{
				throw new InvalidOperationException();
			}
			ec.EmitLoadSubject(local);
			this.expression.Emit(ec);
			ec.EmitCall(setMethod);
		}

		// Token: 0x040000EE RID: 238
		private Expression expression;
	}
}
