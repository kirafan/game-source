﻿using System;
using System.Reflection;
using System.Runtime.CompilerServices;

namespace System.Linq.Expressions
{
	// Token: 0x02000040 RID: 64
	internal static class Extensions
	{
		// Token: 0x06000414 RID: 1044 RVA: 0x00013CB8 File Offset: 0x00011EB8
		public static bool IsGenericInstanceOf(this Type self, Type type)
		{
			return self.IsGenericType && self.GetGenericTypeDefinition() == type;
		}

		// Token: 0x06000415 RID: 1045 RVA: 0x00013CD0 File Offset: 0x00011ED0
		public static bool IsNullable(this Type self)
		{
			return self.IsValueType && self.IsGenericInstanceOf(typeof(Nullable<>));
		}

		// Token: 0x06000416 RID: 1046 RVA: 0x00013CF0 File Offset: 0x00011EF0
		public static bool IsExpression(this Type self)
		{
			return self == typeof(Expression) || self.IsSubclassOf(typeof(Expression));
		}

		// Token: 0x06000417 RID: 1047 RVA: 0x00013D18 File Offset: 0x00011F18
		public static bool IsGenericImplementationOf(this Type self, Type type)
		{
			foreach (Type self2 in self.GetInterfaces())
			{
				if (self2.IsGenericInstanceOf(type))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000418 RID: 1048 RVA: 0x00013D54 File Offset: 0x00011F54
		public static bool IsAssignableTo(this Type self, Type type)
		{
			return type.IsAssignableFrom(self) || Extensions.ArrayTypeIsAssignableTo(self, type);
		}

		// Token: 0x06000419 RID: 1049 RVA: 0x00013D6C File Offset: 0x00011F6C
		public static Type GetFirstGenericArgument(this Type self)
		{
			return self.GetGenericArguments()[0];
		}

		// Token: 0x0600041A RID: 1050 RVA: 0x00013D78 File Offset: 0x00011F78
		public static Type MakeGenericTypeFrom(this Type self, Type type)
		{
			return self.MakeGenericType(type.GetGenericArguments());
		}

		// Token: 0x0600041B RID: 1051 RVA: 0x00013D88 File Offset: 0x00011F88
		public static Type MakeNullableType(this Type self)
		{
			return typeof(Nullable<>).MakeGenericType(new Type[]
			{
				self
			});
		}

		// Token: 0x0600041C RID: 1052 RVA: 0x00013DA4 File Offset: 0x00011FA4
		public static Type GetNotNullableType(this Type self)
		{
			return (!self.IsNullable()) ? self : self.GetFirstGenericArgument();
		}

		// Token: 0x0600041D RID: 1053 RVA: 0x00013DC0 File Offset: 0x00011FC0
		public static MethodInfo GetInvokeMethod(this Type self)
		{
			return self.GetMethod("Invoke", BindingFlags.Instance | BindingFlags.Public);
		}

		// Token: 0x0600041E RID: 1054 RVA: 0x00013DD0 File Offset: 0x00011FD0
		public static MethodInfo MakeGenericMethodFrom(this MethodInfo self, MethodInfo method)
		{
			return self.MakeGenericMethod(method.GetGenericArguments());
		}

		// Token: 0x0600041F RID: 1055 RVA: 0x00013DE0 File Offset: 0x00011FE0
		public static Type[] GetParameterTypes(this MethodBase self)
		{
			ParameterInfo[] parameters = self.GetParameters();
			Type[] array = new Type[parameters.Length];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = parameters[i].ParameterType;
			}
			return array;
		}

		// Token: 0x06000420 RID: 1056 RVA: 0x00013E20 File Offset: 0x00012020
		private static bool ArrayTypeIsAssignableTo(Type type, Type candidate)
		{
			return type.IsArray && candidate.IsArray && type.GetArrayRank() == candidate.GetArrayRank() && type.GetElementType().IsAssignableTo(candidate.GetElementType());
		}

		// Token: 0x06000421 RID: 1057 RVA: 0x00013E6C File Offset: 0x0001206C
		public static void OnFieldOrProperty(this MemberInfo self, Action<FieldInfo> onfield, Action<PropertyInfo> onprop)
		{
			MemberTypes memberType = self.MemberType;
			if (memberType == MemberTypes.Field)
			{
				onfield((FieldInfo)self);
				return;
			}
			if (memberType != MemberTypes.Property)
			{
				throw new ArgumentException();
			}
			onprop((PropertyInfo)self);
		}

		// Token: 0x06000422 RID: 1058 RVA: 0x00013EB4 File Offset: 0x000120B4
		public static T OnFieldOrProperty<T>(this MemberInfo self, Func<FieldInfo, T> onfield, Func<PropertyInfo, T> onprop)
		{
			MemberTypes memberType = self.MemberType;
			if (memberType == MemberTypes.Field)
			{
				return onfield((FieldInfo)self);
			}
			if (memberType != MemberTypes.Property)
			{
				throw new ArgumentException();
			}
			return onprop((PropertyInfo)self);
		}

		// Token: 0x06000423 RID: 1059 RVA: 0x00013EFC File Offset: 0x000120FC
		public static Type MakeStrongBoxType(this Type self)
		{
			return typeof(StrongBox<>).MakeGenericType(new Type[]
			{
				self
			});
		}
	}
}
