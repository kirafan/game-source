﻿using System;
using System.Collections.ObjectModel;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000042 RID: 66
	public sealed class ListInitExpression : Expression
	{
		// Token: 0x06000428 RID: 1064 RVA: 0x00013F74 File Offset: 0x00012174
		internal ListInitExpression(NewExpression new_expression, ReadOnlyCollection<ElementInit> initializers) : base(ExpressionType.ListInit, new_expression.Type)
		{
			this.new_expression = new_expression;
			this.initializers = initializers;
		}

		// Token: 0x1700003E RID: 62
		// (get) Token: 0x06000429 RID: 1065 RVA: 0x00013F94 File Offset: 0x00012194
		public NewExpression NewExpression
		{
			get
			{
				return this.new_expression;
			}
		}

		// Token: 0x1700003F RID: 63
		// (get) Token: 0x0600042A RID: 1066 RVA: 0x00013F9C File Offset: 0x0001219C
		public ReadOnlyCollection<ElementInit> Initializers
		{
			get
			{
				return this.initializers;
			}
		}

		// Token: 0x0600042B RID: 1067 RVA: 0x00013FA4 File Offset: 0x000121A4
		internal override void Emit(EmitContext ec)
		{
			LocalBuilder local = ec.EmitStored(this.new_expression);
			ec.EmitCollection(this.initializers, local);
			ec.EmitLoad(local);
		}

		// Token: 0x040000EC RID: 236
		private NewExpression new_expression;

		// Token: 0x040000ED RID: 237
		private ReadOnlyCollection<ElementInit> initializers;
	}
}
