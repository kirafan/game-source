﻿using System;

namespace System.Linq.Expressions
{
	// Token: 0x0200003D RID: 61
	public enum ExpressionType
	{
		// Token: 0x040000BC RID: 188
		Add,
		// Token: 0x040000BD RID: 189
		AddChecked,
		// Token: 0x040000BE RID: 190
		And,
		// Token: 0x040000BF RID: 191
		AndAlso,
		// Token: 0x040000C0 RID: 192
		ArrayLength,
		// Token: 0x040000C1 RID: 193
		ArrayIndex,
		// Token: 0x040000C2 RID: 194
		Call,
		// Token: 0x040000C3 RID: 195
		Coalesce,
		// Token: 0x040000C4 RID: 196
		Conditional,
		// Token: 0x040000C5 RID: 197
		Constant,
		// Token: 0x040000C6 RID: 198
		Convert,
		// Token: 0x040000C7 RID: 199
		ConvertChecked,
		// Token: 0x040000C8 RID: 200
		Divide,
		// Token: 0x040000C9 RID: 201
		Equal,
		// Token: 0x040000CA RID: 202
		ExclusiveOr,
		// Token: 0x040000CB RID: 203
		GreaterThan,
		// Token: 0x040000CC RID: 204
		GreaterThanOrEqual,
		// Token: 0x040000CD RID: 205
		Invoke,
		// Token: 0x040000CE RID: 206
		Lambda,
		// Token: 0x040000CF RID: 207
		LeftShift,
		// Token: 0x040000D0 RID: 208
		LessThan,
		// Token: 0x040000D1 RID: 209
		LessThanOrEqual,
		// Token: 0x040000D2 RID: 210
		ListInit,
		// Token: 0x040000D3 RID: 211
		MemberAccess,
		// Token: 0x040000D4 RID: 212
		MemberInit,
		// Token: 0x040000D5 RID: 213
		Modulo,
		// Token: 0x040000D6 RID: 214
		Multiply,
		// Token: 0x040000D7 RID: 215
		MultiplyChecked,
		// Token: 0x040000D8 RID: 216
		Negate,
		// Token: 0x040000D9 RID: 217
		UnaryPlus,
		// Token: 0x040000DA RID: 218
		NegateChecked,
		// Token: 0x040000DB RID: 219
		New,
		// Token: 0x040000DC RID: 220
		NewArrayInit,
		// Token: 0x040000DD RID: 221
		NewArrayBounds,
		// Token: 0x040000DE RID: 222
		Not,
		// Token: 0x040000DF RID: 223
		NotEqual,
		// Token: 0x040000E0 RID: 224
		Or,
		// Token: 0x040000E1 RID: 225
		OrElse,
		// Token: 0x040000E2 RID: 226
		Parameter,
		// Token: 0x040000E3 RID: 227
		Power,
		// Token: 0x040000E4 RID: 228
		Quote,
		// Token: 0x040000E5 RID: 229
		RightShift,
		// Token: 0x040000E6 RID: 230
		Subtract,
		// Token: 0x040000E7 RID: 231
		SubtractChecked,
		// Token: 0x040000E8 RID: 232
		TypeAs,
		// Token: 0x040000E9 RID: 233
		TypeIs
	}
}
