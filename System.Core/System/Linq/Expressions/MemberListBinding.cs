﻿using System;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x02000048 RID: 72
	public sealed class MemberListBinding : MemberBinding
	{
		// Token: 0x06000443 RID: 1091 RVA: 0x0001430C File Offset: 0x0001250C
		internal MemberListBinding(MemberInfo member, ReadOnlyCollection<ElementInit> initializers) : base(MemberBindingType.ListBinding, member)
		{
			this.initializers = initializers;
		}

		// Token: 0x17000047 RID: 71
		// (get) Token: 0x06000444 RID: 1092 RVA: 0x00014320 File Offset: 0x00012520
		public ReadOnlyCollection<ElementInit> Initializers
		{
			get
			{
				return this.initializers;
			}
		}

		// Token: 0x06000445 RID: 1093 RVA: 0x00014328 File Offset: 0x00012528
		internal override void Emit(EmitContext ec, LocalBuilder local)
		{
			LocalBuilder local2 = base.EmitLoadMember(ec, local);
			foreach (ElementInit elementInit in this.initializers)
			{
				elementInit.Emit(ec, local2);
			}
		}

		// Token: 0x040000F9 RID: 249
		private ReadOnlyCollection<ElementInit> initializers;
	}
}
