﻿using System;
using System.Reflection.Emit;

namespace System.Linq.Expressions
{
	// Token: 0x0200004E RID: 78
	public sealed class TypeBinaryExpression : Expression
	{
		// Token: 0x06000463 RID: 1123 RVA: 0x00014834 File Offset: 0x00012A34
		internal TypeBinaryExpression(ExpressionType node_type, Expression expression, Type type_operand, Type type) : base(node_type, type)
		{
			this.expression = expression;
			this.type_operand = type_operand;
		}

		// Token: 0x17000051 RID: 81
		// (get) Token: 0x06000464 RID: 1124 RVA: 0x00014850 File Offset: 0x00012A50
		public Expression Expression
		{
			get
			{
				return this.expression;
			}
		}

		// Token: 0x17000052 RID: 82
		// (get) Token: 0x06000465 RID: 1125 RVA: 0x00014858 File Offset: 0x00012A58
		public Type TypeOperand
		{
			get
			{
				return this.type_operand;
			}
		}

		// Token: 0x06000466 RID: 1126 RVA: 0x00014860 File Offset: 0x00012A60
		internal override void Emit(EmitContext ec)
		{
			if (this.expression.Type == typeof(void))
			{
				ec.ig.Emit(OpCodes.Ldc_I4_0);
				return;
			}
			ec.EmitIsInst(this.expression, this.type_operand);
			ec.ig.Emit(OpCodes.Ldnull);
			ec.ig.Emit(OpCodes.Cgt_Un);
		}

		// Token: 0x04000103 RID: 259
		private Expression expression;

		// Token: 0x04000104 RID: 260
		private Type type_operand;
	}
}
