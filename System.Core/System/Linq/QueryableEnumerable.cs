﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace System.Linq
{
	// Token: 0x02000021 RID: 33
	internal class QueryableEnumerable<TElement> : IQueryableEnumerable, IEnumerable, IOrderedQueryable, IQueryable, IQueryProvider, IEnumerable<TElement>, IQueryableEnumerable<TElement>, IQueryable<TElement>, IOrderedQueryable<TElement>
	{
		// Token: 0x0600027E RID: 638 RVA: 0x0000C5D8 File Offset: 0x0000A7D8
		public QueryableEnumerable(IEnumerable<TElement> enumerable)
		{
			this.expression = Expression.Constant(this);
			this.enumerable = enumerable;
		}

		// Token: 0x0600027F RID: 639 RVA: 0x0000C5F4 File Offset: 0x0000A7F4
		public QueryableEnumerable(Expression expression)
		{
			this.expression = expression;
		}

		// Token: 0x06000280 RID: 640 RVA: 0x0000C604 File Offset: 0x0000A804
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x17000022 RID: 34
		// (get) Token: 0x06000281 RID: 641 RVA: 0x0000C60C File Offset: 0x0000A80C
		public Type ElementType
		{
			get
			{
				return typeof(TElement);
			}
		}

		// Token: 0x17000023 RID: 35
		// (get) Token: 0x06000282 RID: 642 RVA: 0x0000C618 File Offset: 0x0000A818
		public Expression Expression
		{
			get
			{
				return this.expression;
			}
		}

		// Token: 0x17000024 RID: 36
		// (get) Token: 0x06000283 RID: 643 RVA: 0x0000C620 File Offset: 0x0000A820
		public IQueryProvider Provider
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06000284 RID: 644 RVA: 0x0000C624 File Offset: 0x0000A824
		public IEnumerable GetEnumerable()
		{
			return this.enumerable;
		}

		// Token: 0x06000285 RID: 645 RVA: 0x0000C62C File Offset: 0x0000A82C
		public IEnumerator<TElement> GetEnumerator()
		{
			return this.Execute<IEnumerable<TElement>>(this.expression).GetEnumerator();
		}

		// Token: 0x06000286 RID: 646 RVA: 0x0000C640 File Offset: 0x0000A840
		public IQueryable CreateQuery(Expression expression)
		{
			return (IQueryable)Activator.CreateInstance(typeof(QueryableEnumerable<>).MakeGenericType(new Type[]
			{
				expression.Type.GetFirstGenericArgument()
			}), new object[]
			{
				expression
			});
		}

		// Token: 0x06000287 RID: 647 RVA: 0x0000C684 File Offset: 0x0000A884
		public object Execute(Expression expression)
		{
			LambdaExpression lambdaExpression = Expression.Lambda(QueryableEnumerable<TElement>.TransformQueryable(expression), new ParameterExpression[0]);
			return lambdaExpression.Compile().DynamicInvoke(new object[0]);
		}

		// Token: 0x06000288 RID: 648 RVA: 0x0000C6B4 File Offset: 0x0000A8B4
		private static Expression TransformQueryable(Expression expression)
		{
			return new QueryableTransformer().Transform(expression);
		}

		// Token: 0x06000289 RID: 649 RVA: 0x0000C6C4 File Offset: 0x0000A8C4
		public IQueryable<TElem> CreateQuery<TElem>(Expression expression)
		{
			return new QueryableEnumerable<TElem>(expression);
		}

		// Token: 0x0600028A RID: 650 RVA: 0x0000C6CC File Offset: 0x0000A8CC
		public TResult Execute<TResult>(Expression expression)
		{
			Expression<Func<TResult>> expression2 = Expression.Lambda<Func<TResult>>(QueryableEnumerable<TElement>.TransformQueryable(expression), new ParameterExpression[0]);
			return expression2.Compile()();
		}

		// Token: 0x04000087 RID: 135
		private Expression expression;

		// Token: 0x04000088 RID: 136
		private IEnumerable<TElement> enumerable;
	}
}
