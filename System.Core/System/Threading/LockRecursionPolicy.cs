﻿using System;

namespace System.Threading
{
	// Token: 0x02000063 RID: 99
	[Serializable]
	public enum LockRecursionPolicy
	{
		// Token: 0x0400014F RID: 335
		NoRecursion,
		// Token: 0x04000150 RID: 336
		SupportsRecursion
	}
}
