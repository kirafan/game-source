﻿using System;
using System.Security.Permissions;

namespace System.Threading
{
	// Token: 0x02000064 RID: 100
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	public class ReaderWriterLockSlim : IDisposable
	{
		// Token: 0x06000533 RID: 1331 RVA: 0x00018C88 File Offset: 0x00016E88
		public ReaderWriterLockSlim()
		{
		}

		// Token: 0x06000534 RID: 1332 RVA: 0x00018C9C File Offset: 0x00016E9C
		public ReaderWriterLockSlim(LockRecursionPolicy recursionPolicy)
		{
			this.recursionPolicy = recursionPolicy;
			if (recursionPolicy != LockRecursionPolicy.NoRecursion)
			{
				throw new NotImplementedException("recursionPolicy != NoRecursion not currently implemented");
			}
		}

		// Token: 0x06000536 RID: 1334 RVA: 0x00018CE4 File Offset: 0x00016EE4
		public void EnterReadLock()
		{
			this.TryEnterReadLock(-1);
		}

		// Token: 0x06000537 RID: 1335 RVA: 0x00018CF0 File Offset: 0x00016EF0
		public bool TryEnterReadLock(int millisecondsTimeout)
		{
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout");
			}
			if (this.read_locks == null)
			{
				throw new ObjectDisposedException(null);
			}
			if (Thread.CurrentThread == this.write_thread)
			{
				throw new LockRecursionException("Read lock cannot be acquired while write lock is held");
			}
			this.EnterMyLock();
			ReaderWriterLockSlim.LockDetails readLockDetails = this.GetReadLockDetails(Thread.CurrentThread.ManagedThreadId, true);
			if (readLockDetails.ReadLocks != 0)
			{
				this.ExitMyLock();
				throw new LockRecursionException("Recursive read lock can only be aquired in SupportsRecursion mode");
			}
			readLockDetails.ReadLocks++;
			while (this.owners < 0 || this.numWriteWaiters != 0U)
			{
				if (millisecondsTimeout == 0)
				{
					this.ExitMyLock();
					return false;
				}
				if (this.readEvent == null)
				{
					this.LazyCreateEvent(ref this.readEvent, false);
				}
				else if (!this.WaitOnEvent(this.readEvent, ref this.numReadWaiters, millisecondsTimeout))
				{
					return false;
				}
			}
			this.owners++;
			this.ExitMyLock();
			return true;
		}

		// Token: 0x06000538 RID: 1336 RVA: 0x00018DFC File Offset: 0x00016FFC
		public bool TryEnterReadLock(TimeSpan timeout)
		{
			return this.TryEnterReadLock(ReaderWriterLockSlim.CheckTimeout(timeout));
		}

		// Token: 0x06000539 RID: 1337 RVA: 0x00018E0C File Offset: 0x0001700C
		public void ExitReadLock()
		{
			this.EnterMyLock();
			if (this.owners < 1)
			{
				this.ExitMyLock();
				throw new SynchronizationLockException("Releasing lock and no read lock taken");
			}
			this.owners--;
			this.GetReadLockDetails(Thread.CurrentThread.ManagedThreadId, false).ReadLocks--;
			this.ExitAndWakeUpAppropriateWaiters();
		}

		// Token: 0x0600053A RID: 1338 RVA: 0x00018E70 File Offset: 0x00017070
		public void EnterWriteLock()
		{
			this.TryEnterWriteLock(-1);
		}

		// Token: 0x0600053B RID: 1339 RVA: 0x00018E7C File Offset: 0x0001707C
		public bool TryEnterWriteLock(int millisecondsTimeout)
		{
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout");
			}
			if (this.read_locks == null)
			{
				throw new ObjectDisposedException(null);
			}
			if (this.IsWriteLockHeld)
			{
				throw new LockRecursionException();
			}
			this.EnterMyLock();
			ReaderWriterLockSlim.LockDetails readLockDetails = this.GetReadLockDetails(Thread.CurrentThread.ManagedThreadId, false);
			if (readLockDetails != null && readLockDetails.ReadLocks > 0)
			{
				this.ExitMyLock();
				throw new LockRecursionException("Write lock cannot be acquired while read lock is held");
			}
			while (this.owners != 0)
			{
				if (this.owners == 1 && this.upgradable_thread == Thread.CurrentThread)
				{
					this.owners = -1;
					this.write_thread = Thread.CurrentThread;
					IL_178:
					this.ExitMyLock();
					return true;
				}
				if (millisecondsTimeout == 0)
				{
					this.ExitMyLock();
					return false;
				}
				if (this.upgradable_thread == Thread.CurrentThread)
				{
					if (this.upgradeEvent == null)
					{
						this.LazyCreateEvent(ref this.upgradeEvent, false);
					}
					else
					{
						if (this.numUpgradeWaiters > 0U)
						{
							this.ExitMyLock();
							throw new ApplicationException("Upgrading lock to writer lock already in process, deadlock");
						}
						if (!this.WaitOnEvent(this.upgradeEvent, ref this.numUpgradeWaiters, millisecondsTimeout))
						{
							return false;
						}
					}
				}
				else if (this.writeEvent == null)
				{
					this.LazyCreateEvent(ref this.writeEvent, true);
				}
				else if (!this.WaitOnEvent(this.writeEvent, ref this.numWriteWaiters, millisecondsTimeout))
				{
					return false;
				}
			}
			this.owners = -1;
			this.write_thread = Thread.CurrentThread;
			goto IL_178;
		}

		// Token: 0x0600053C RID: 1340 RVA: 0x00019008 File Offset: 0x00017208
		public bool TryEnterWriteLock(TimeSpan timeout)
		{
			return this.TryEnterWriteLock(ReaderWriterLockSlim.CheckTimeout(timeout));
		}

		// Token: 0x0600053D RID: 1341 RVA: 0x00019018 File Offset: 0x00017218
		public void ExitWriteLock()
		{
			this.EnterMyLock();
			if (this.owners != -1)
			{
				this.ExitMyLock();
				throw new SynchronizationLockException("Calling ExitWriterLock when no write lock is held");
			}
			if (this.upgradable_thread == Thread.CurrentThread)
			{
				this.owners = 1;
			}
			else
			{
				this.owners = 0;
			}
			this.write_thread = null;
			this.ExitAndWakeUpAppropriateWaiters();
		}

		// Token: 0x0600053E RID: 1342 RVA: 0x00019078 File Offset: 0x00017278
		public void EnterUpgradeableReadLock()
		{
			this.TryEnterUpgradeableReadLock(-1);
		}

		// Token: 0x0600053F RID: 1343 RVA: 0x00019084 File Offset: 0x00017284
		public bool TryEnterUpgradeableReadLock(int millisecondsTimeout)
		{
			if (millisecondsTimeout < -1)
			{
				throw new ArgumentOutOfRangeException("millisecondsTimeout");
			}
			if (this.read_locks == null)
			{
				throw new ObjectDisposedException(null);
			}
			if (this.IsUpgradeableReadLockHeld)
			{
				throw new LockRecursionException();
			}
			if (this.IsWriteLockHeld)
			{
				throw new LockRecursionException();
			}
			this.EnterMyLock();
			while (this.owners != 0 || this.numWriteWaiters != 0U || this.upgradable_thread != null)
			{
				if (millisecondsTimeout == 0)
				{
					this.ExitMyLock();
					return false;
				}
				if (this.readEvent == null)
				{
					this.LazyCreateEvent(ref this.readEvent, false);
				}
				else if (!this.WaitOnEvent(this.readEvent, ref this.numReadWaiters, millisecondsTimeout))
				{
					return false;
				}
			}
			this.owners++;
			this.upgradable_thread = Thread.CurrentThread;
			this.ExitMyLock();
			return true;
		}

		// Token: 0x06000540 RID: 1344 RVA: 0x00019170 File Offset: 0x00017370
		public bool TryEnterUpgradeableReadLock(TimeSpan timeout)
		{
			return this.TryEnterUpgradeableReadLock(ReaderWriterLockSlim.CheckTimeout(timeout));
		}

		// Token: 0x06000541 RID: 1345 RVA: 0x00019180 File Offset: 0x00017380
		public void ExitUpgradeableReadLock()
		{
			this.EnterMyLock();
			this.owners--;
			this.upgradable_thread = null;
			this.ExitAndWakeUpAppropriateWaiters();
		}

		// Token: 0x06000542 RID: 1346 RVA: 0x000191A4 File Offset: 0x000173A4
		public void Dispose()
		{
			this.read_locks = null;
		}

		// Token: 0x17000074 RID: 116
		// (get) Token: 0x06000543 RID: 1347 RVA: 0x000191B0 File Offset: 0x000173B0
		public bool IsReadLockHeld
		{
			get
			{
				return this.RecursiveReadCount != 0;
			}
		}

		// Token: 0x17000075 RID: 117
		// (get) Token: 0x06000544 RID: 1348 RVA: 0x000191C0 File Offset: 0x000173C0
		public bool IsWriteLockHeld
		{
			get
			{
				return this.RecursiveWriteCount != 0;
			}
		}

		// Token: 0x17000076 RID: 118
		// (get) Token: 0x06000545 RID: 1349 RVA: 0x000191D0 File Offset: 0x000173D0
		public bool IsUpgradeableReadLockHeld
		{
			get
			{
				return this.RecursiveUpgradeCount != 0;
			}
		}

		// Token: 0x17000077 RID: 119
		// (get) Token: 0x06000546 RID: 1350 RVA: 0x000191E0 File Offset: 0x000173E0
		public int CurrentReadCount
		{
			get
			{
				return this.owners & 268435455;
			}
		}

		// Token: 0x17000078 RID: 120
		// (get) Token: 0x06000547 RID: 1351 RVA: 0x000191F0 File Offset: 0x000173F0
		public int RecursiveReadCount
		{
			get
			{
				this.EnterMyLock();
				ReaderWriterLockSlim.LockDetails readLockDetails = this.GetReadLockDetails(Thread.CurrentThread.ManagedThreadId, false);
				int result = (readLockDetails != null) ? readLockDetails.ReadLocks : 0;
				this.ExitMyLock();
				return result;
			}
		}

		// Token: 0x17000079 RID: 121
		// (get) Token: 0x06000548 RID: 1352 RVA: 0x00019230 File Offset: 0x00017430
		public int RecursiveUpgradeCount
		{
			get
			{
				return (this.upgradable_thread != Thread.CurrentThread) ? 0 : 1;
			}
		}

		// Token: 0x1700007A RID: 122
		// (get) Token: 0x06000549 RID: 1353 RVA: 0x0001924C File Offset: 0x0001744C
		public int RecursiveWriteCount
		{
			get
			{
				return (this.write_thread != Thread.CurrentThread) ? 0 : 1;
			}
		}

		// Token: 0x1700007B RID: 123
		// (get) Token: 0x0600054A RID: 1354 RVA: 0x00019268 File Offset: 0x00017468
		public int WaitingReadCount
		{
			get
			{
				return (int)this.numReadWaiters;
			}
		}

		// Token: 0x1700007C RID: 124
		// (get) Token: 0x0600054B RID: 1355 RVA: 0x00019270 File Offset: 0x00017470
		public int WaitingUpgradeCount
		{
			get
			{
				return (int)this.numUpgradeWaiters;
			}
		}

		// Token: 0x1700007D RID: 125
		// (get) Token: 0x0600054C RID: 1356 RVA: 0x00019278 File Offset: 0x00017478
		public int WaitingWriteCount
		{
			get
			{
				return (int)this.numWriteWaiters;
			}
		}

		// Token: 0x1700007E RID: 126
		// (get) Token: 0x0600054D RID: 1357 RVA: 0x00019280 File Offset: 0x00017480
		public LockRecursionPolicy RecursionPolicy
		{
			get
			{
				return this.recursionPolicy;
			}
		}

		// Token: 0x0600054E RID: 1358 RVA: 0x00019288 File Offset: 0x00017488
		private void EnterMyLock()
		{
			if (Interlocked.CompareExchange(ref this.myLock, 1, 0) != 0)
			{
				this.EnterMyLockSpin();
			}
		}

		// Token: 0x0600054F RID: 1359 RVA: 0x000192A4 File Offset: 0x000174A4
		private void EnterMyLockSpin()
		{
			int num = 0;
			for (;;)
			{
				if (num < 3 && ReaderWriterLockSlim.smp)
				{
					Thread.SpinWait(20);
				}
				else
				{
					Thread.Sleep(0);
				}
				if (Interlocked.CompareExchange(ref this.myLock, 1, 0) == 0)
				{
					break;
				}
				num++;
			}
		}

		// Token: 0x06000550 RID: 1360 RVA: 0x000192F8 File Offset: 0x000174F8
		private void ExitMyLock()
		{
			this.myLock = 0;
		}

		// Token: 0x1700007F RID: 127
		// (get) Token: 0x06000551 RID: 1361 RVA: 0x00019304 File Offset: 0x00017504
		private bool MyLockHeld
		{
			get
			{
				return this.myLock != 0;
			}
		}

		// Token: 0x06000552 RID: 1362 RVA: 0x00019314 File Offset: 0x00017514
		private void ExitAndWakeUpAppropriateWaiters()
		{
			if (this.owners == 1 && this.numUpgradeWaiters != 0U)
			{
				this.ExitMyLock();
				this.upgradeEvent.Set();
			}
			else if (this.owners == 0 && this.numWriteWaiters > 0U)
			{
				this.ExitMyLock();
				this.writeEvent.Set();
			}
			else if (this.owners >= 0 && this.numReadWaiters != 0U)
			{
				this.ExitMyLock();
				this.readEvent.Set();
			}
			else
			{
				this.ExitMyLock();
			}
		}

		// Token: 0x06000553 RID: 1363 RVA: 0x000193B4 File Offset: 0x000175B4
		private void LazyCreateEvent(ref EventWaitHandle waitEvent, bool makeAutoResetEvent)
		{
			this.ExitMyLock();
			EventWaitHandle eventWaitHandle;
			if (makeAutoResetEvent)
			{
				eventWaitHandle = new AutoResetEvent(false);
			}
			else
			{
				eventWaitHandle = new ManualResetEvent(false);
			}
			this.EnterMyLock();
			if (waitEvent == null)
			{
				waitEvent = eventWaitHandle;
			}
		}

		// Token: 0x06000554 RID: 1364 RVA: 0x000193F0 File Offset: 0x000175F0
		private bool WaitOnEvent(EventWaitHandle waitEvent, ref uint numWaiters, int millisecondsTimeout)
		{
			waitEvent.Reset();
			numWaiters += 1U;
			bool flag = false;
			this.ExitMyLock();
			try
			{
				flag = waitEvent.WaitOne(millisecondsTimeout, false);
			}
			finally
			{
				this.EnterMyLock();
				numWaiters -= 1U;
				if (!flag)
				{
					this.ExitMyLock();
				}
			}
			return flag;
		}

		// Token: 0x06000555 RID: 1365 RVA: 0x00019458 File Offset: 0x00017658
		private static int CheckTimeout(TimeSpan timeout)
		{
			int result;
			try
			{
				result = checked((int)timeout.TotalMilliseconds);
			}
			catch (OverflowException)
			{
				throw new ArgumentOutOfRangeException("timeout");
			}
			return result;
		}

		// Token: 0x06000556 RID: 1366 RVA: 0x000194A8 File Offset: 0x000176A8
		private ReaderWriterLockSlim.LockDetails GetReadLockDetails(int threadId, bool create)
		{
			int i;
			ReaderWriterLockSlim.LockDetails lockDetails;
			for (i = 0; i < this.read_locks.Length; i++)
			{
				lockDetails = this.read_locks[i];
				if (lockDetails == null)
				{
					break;
				}
				if (lockDetails.ThreadId == threadId)
				{
					return lockDetails;
				}
			}
			if (!create)
			{
				return null;
			}
			if (i == this.read_locks.Length)
			{
				Array.Resize<ReaderWriterLockSlim.LockDetails>(ref this.read_locks, this.read_locks.Length * 2);
			}
			lockDetails = (this.read_locks[i] = new ReaderWriterLockSlim.LockDetails());
			lockDetails.ThreadId = threadId;
			return lockDetails;
		}

		// Token: 0x04000151 RID: 337
		private static readonly bool smp = Environment.ProcessorCount > 1;

		// Token: 0x04000152 RID: 338
		private int myLock;

		// Token: 0x04000153 RID: 339
		private int owners;

		// Token: 0x04000154 RID: 340
		private Thread upgradable_thread;

		// Token: 0x04000155 RID: 341
		private Thread write_thread;

		// Token: 0x04000156 RID: 342
		private uint numWriteWaiters;

		// Token: 0x04000157 RID: 343
		private uint numReadWaiters;

		// Token: 0x04000158 RID: 344
		private uint numUpgradeWaiters;

		// Token: 0x04000159 RID: 345
		private EventWaitHandle writeEvent;

		// Token: 0x0400015A RID: 346
		private EventWaitHandle readEvent;

		// Token: 0x0400015B RID: 347
		private EventWaitHandle upgradeEvent;

		// Token: 0x0400015C RID: 348
		private readonly LockRecursionPolicy recursionPolicy;

		// Token: 0x0400015D RID: 349
		private ReaderWriterLockSlim.LockDetails[] read_locks = new ReaderWriterLockSlim.LockDetails[8];

		// Token: 0x02000065 RID: 101
		private sealed class LockDetails
		{
			// Token: 0x0400015E RID: 350
			public int ThreadId;

			// Token: 0x0400015F RID: 351
			public int ReadLocks;
		}
	}
}
