﻿using System;
using System.Runtime.Serialization;

namespace System.Threading
{
	// Token: 0x02000062 RID: 98
	[Serializable]
	public class LockRecursionException : Exception
	{
		// Token: 0x0600052F RID: 1327 RVA: 0x00018C5C File Offset: 0x00016E5C
		public LockRecursionException()
		{
		}

		// Token: 0x06000530 RID: 1328 RVA: 0x00018C64 File Offset: 0x00016E64
		public LockRecursionException(string message) : base(message)
		{
		}

		// Token: 0x06000531 RID: 1329 RVA: 0x00018C70 File Offset: 0x00016E70
		public LockRecursionException(string message, Exception e) : base(message, e)
		{
		}

		// Token: 0x06000532 RID: 1330 RVA: 0x00018C7C File Offset: 0x00016E7C
		protected LockRecursionException(SerializationInfo info, StreamingContext sc) : base(info, sc)
		{
		}
	}
}
