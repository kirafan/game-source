﻿using System;
using System.Linq;
using System.Runtime.Serialization;
using System.Security.Permissions;

namespace System.Collections.Generic
{
	// Token: 0x02000050 RID: 80
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	[Serializable]
	public class HashSet<T> : IEnumerable, ISerializable, IDeserializationCallback, ICollection<T>, IEnumerable<T>
	{
		// Token: 0x06000483 RID: 1155 RVA: 0x00015284 File Offset: 0x00013484
		public HashSet()
		{
			this.Init(10, null);
		}

		// Token: 0x06000484 RID: 1156 RVA: 0x00015298 File Offset: 0x00013498
		public HashSet(IEqualityComparer<T> comparer)
		{
			this.Init(10, comparer);
		}

		// Token: 0x06000485 RID: 1157 RVA: 0x000152AC File Offset: 0x000134AC
		public HashSet(IEnumerable<T> collection) : this(collection, null)
		{
		}

		// Token: 0x06000486 RID: 1158 RVA: 0x000152B8 File Offset: 0x000134B8
		public HashSet(IEnumerable<T> collection, IEqualityComparer<T> comparer)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			int capacity = 0;
			ICollection<T> collection2 = collection as ICollection<T>;
			if (collection2 != null)
			{
				capacity = collection2.Count;
			}
			this.Init(capacity, comparer);
			foreach (T item in collection)
			{
				this.Add(item);
			}
		}

		// Token: 0x06000487 RID: 1159 RVA: 0x0001534C File Offset: 0x0001354C
		protected HashSet(SerializationInfo info, StreamingContext context)
		{
			this.si = info;
		}

		// Token: 0x06000488 RID: 1160 RVA: 0x0001535C File Offset: 0x0001355C
		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return new HashSet<T>.Enumerator(this);
		}

		// Token: 0x17000057 RID: 87
		// (get) Token: 0x06000489 RID: 1161 RVA: 0x0001536C File Offset: 0x0001356C
		bool ICollection<T>.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x0600048A RID: 1162 RVA: 0x00015370 File Offset: 0x00013570
		void ICollection<T>.CopyTo(T[] array, int index)
		{
			this.CopyTo(array, index);
		}

		// Token: 0x0600048B RID: 1163 RVA: 0x0001537C File Offset: 0x0001357C
		void ICollection<T>.Add(T item)
		{
			this.Add(item);
		}

		// Token: 0x0600048C RID: 1164 RVA: 0x00015388 File Offset: 0x00013588
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new HashSet<T>.Enumerator(this);
		}

		// Token: 0x17000058 RID: 88
		// (get) Token: 0x0600048D RID: 1165 RVA: 0x00015398 File Offset: 0x00013598
		public int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x0600048E RID: 1166 RVA: 0x000153A0 File Offset: 0x000135A0
		private void Init(int capacity, IEqualityComparer<T> comparer)
		{
			if (capacity < 0)
			{
				throw new ArgumentOutOfRangeException("capacity");
			}
			this.comparer = (comparer ?? EqualityComparer<T>.Default);
			if (capacity == 0)
			{
				capacity = 10;
			}
			capacity = (int)((float)capacity / 0.9f) + 1;
			this.InitArrays(capacity);
			this.generation = 0;
		}

		// Token: 0x0600048F RID: 1167 RVA: 0x000153F8 File Offset: 0x000135F8
		private void InitArrays(int size)
		{
			this.table = new int[size];
			this.links = new HashSet<T>.Link[size];
			this.empty_slot = -1;
			this.slots = new T[size];
			this.touched = 0;
			this.threshold = (int)((float)this.table.Length * 0.9f);
			if (this.threshold == 0 && this.table.Length > 0)
			{
				this.threshold = 1;
			}
		}

		// Token: 0x06000490 RID: 1168 RVA: 0x00015470 File Offset: 0x00013670
		private bool SlotsContainsAt(int index, int hash, T item)
		{
			HashSet<T>.Link link;
			for (int num = this.table[index] - 1; num != -1; num = link.Next)
			{
				link = this.links[num];
				if (link.HashCode == hash && ((hash != -2147483648 || (item != null && this.slots[num] != null)) ? this.comparer.Equals(item, this.slots[num]) : (item == null && null == this.slots[num])))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06000491 RID: 1169 RVA: 0x00015530 File Offset: 0x00013730
		public void CopyTo(T[] array)
		{
			this.CopyTo(array, 0, this.count);
		}

		// Token: 0x06000492 RID: 1170 RVA: 0x00015540 File Offset: 0x00013740
		public void CopyTo(T[] array, int index)
		{
			this.CopyTo(array, index, this.count);
		}

		// Token: 0x06000493 RID: 1171 RVA: 0x00015550 File Offset: 0x00013750
		public void CopyTo(T[] array, int index, int count)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index");
			}
			if (index > array.Length)
			{
				throw new ArgumentException("index larger than largest valid index of array");
			}
			if (array.Length - index < count)
			{
				throw new ArgumentException("Destination array cannot hold the requested elements!");
			}
			int num = 0;
			int num2 = 0;
			while (num < this.touched && num2 < count)
			{
				if (this.GetLinkHashCode(num) != 0)
				{
					array[index++] = this.slots[num];
				}
				num++;
			}
		}

		// Token: 0x06000494 RID: 1172 RVA: 0x000155F0 File Offset: 0x000137F0
		private void Resize()
		{
			int num = HashSet<T>.PrimeHelper.ToPrime(this.table.Length << 1 | 1);
			int[] array = new int[num];
			HashSet<T>.Link[] array2 = new HashSet<T>.Link[num];
			for (int i = 0; i < this.table.Length; i++)
			{
				for (int num2 = this.table[i] - 1; num2 != -1; num2 = this.links[num2].Next)
				{
					int num3 = array2[num2].HashCode = this.GetItemHashCode(this.slots[num2]);
					int num4 = (num3 & int.MaxValue) % num;
					array2[num2].Next = array[num4] - 1;
					array[num4] = num2 + 1;
				}
			}
			this.table = array;
			this.links = array2;
			T[] destinationArray = new T[num];
			Array.Copy(this.slots, 0, destinationArray, 0, this.touched);
			this.slots = destinationArray;
			this.threshold = (int)((float)num * 0.9f);
		}

		// Token: 0x06000495 RID: 1173 RVA: 0x000156F4 File Offset: 0x000138F4
		private int GetLinkHashCode(int index)
		{
			return this.links[index].HashCode & int.MinValue;
		}

		// Token: 0x06000496 RID: 1174 RVA: 0x00015710 File Offset: 0x00013910
		private int GetItemHashCode(T item)
		{
			if (item == null)
			{
				return int.MinValue;
			}
			return this.comparer.GetHashCode(item) | int.MinValue;
		}

		// Token: 0x06000497 RID: 1175 RVA: 0x00015738 File Offset: 0x00013938
		public bool Add(T item)
		{
			int itemHashCode = this.GetItemHashCode(item);
			int num = (itemHashCode & int.MaxValue) % this.table.Length;
			if (this.SlotsContainsAt(num, itemHashCode, item))
			{
				return false;
			}
			if (++this.count > this.threshold)
			{
				this.Resize();
				num = (itemHashCode & int.MaxValue) % this.table.Length;
			}
			int num2 = this.empty_slot;
			if (num2 == -1)
			{
				num2 = this.touched++;
			}
			else
			{
				this.empty_slot = this.links[num2].Next;
			}
			this.links[num2].HashCode = itemHashCode;
			this.links[num2].Next = this.table[num] - 1;
			this.table[num] = num2 + 1;
			this.slots[num2] = item;
			this.generation++;
			return true;
		}

		// Token: 0x17000059 RID: 89
		// (get) Token: 0x06000498 RID: 1176 RVA: 0x00015834 File Offset: 0x00013A34
		public IEqualityComparer<T> Comparer
		{
			get
			{
				return this.comparer;
			}
		}

		// Token: 0x06000499 RID: 1177 RVA: 0x0001583C File Offset: 0x00013A3C
		public void Clear()
		{
			this.count = 0;
			Array.Clear(this.table, 0, this.table.Length);
			Array.Clear(this.slots, 0, this.slots.Length);
			Array.Clear(this.links, 0, this.links.Length);
			this.empty_slot = -1;
			this.touched = 0;
			this.generation++;
		}

		// Token: 0x0600049A RID: 1178 RVA: 0x000158A8 File Offset: 0x00013AA8
		public bool Contains(T item)
		{
			int itemHashCode = this.GetItemHashCode(item);
			int index = (itemHashCode & int.MaxValue) % this.table.Length;
			return this.SlotsContainsAt(index, itemHashCode, item);
		}

		// Token: 0x0600049B RID: 1179 RVA: 0x000158D8 File Offset: 0x00013AD8
		public bool Remove(T item)
		{
			int itemHashCode = this.GetItemHashCode(item);
			int num = (itemHashCode & int.MaxValue) % this.table.Length;
			int num2 = this.table[num] - 1;
			if (num2 == -1)
			{
				return false;
			}
			int num3 = -1;
			do
			{
				HashSet<T>.Link link = this.links[num2];
				if (link.HashCode == itemHashCode && ((itemHashCode != -2147483648 || (item != null && this.slots[num2] != null)) ? this.comparer.Equals(this.slots[num2], item) : (item == null && null == this.slots[num2])))
				{
					break;
				}
				num3 = num2;
				num2 = link.Next;
			}
			while (num2 != -1);
			if (num2 == -1)
			{
				return false;
			}
			this.count--;
			if (num3 == -1)
			{
				this.table[num] = this.links[num2].Next + 1;
			}
			else
			{
				this.links[num3].Next = this.links[num2].Next;
			}
			this.links[num2].Next = this.empty_slot;
			this.empty_slot = num2;
			this.links[num2].HashCode = 0;
			this.slots[num2] = default(T);
			this.generation++;
			return true;
		}

		// Token: 0x0600049C RID: 1180 RVA: 0x00015A70 File Offset: 0x00013C70
		public int RemoveWhere(Predicate<T> predicate)
		{
			if (predicate == null)
			{
				throw new ArgumentNullException("predicate");
			}
			int num = 0;
			T[] array = new T[this.count];
			this.CopyTo(array, 0);
			foreach (T t in array)
			{
				if (predicate(t))
				{
					this.Remove(t);
					num++;
				}
			}
			return num;
		}

		// Token: 0x0600049D RID: 1181 RVA: 0x00015AE0 File Offset: 0x00013CE0
		public void TrimExcess()
		{
			this.Resize();
		}

		// Token: 0x0600049E RID: 1182 RVA: 0x00015AE8 File Offset: 0x00013CE8
		public void IntersectWith(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			T[] array = new T[this.count];
			this.CopyTo(array, 0);
			foreach (T t in array)
			{
				if (!other.Contains(t))
				{
					this.Remove(t);
				}
			}
			foreach (T item in other)
			{
				if (!this.Contains(item))
				{
					this.Remove(item);
				}
			}
		}

		// Token: 0x0600049F RID: 1183 RVA: 0x00015BB4 File Offset: 0x00013DB4
		public void ExceptWith(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			foreach (T item in other)
			{
				this.Remove(item);
			}
		}

		// Token: 0x060004A0 RID: 1184 RVA: 0x00015C24 File Offset: 0x00013E24
		public bool Overlaps(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			foreach (T item in other)
			{
				if (this.Contains(item))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x060004A1 RID: 1185 RVA: 0x00015CA4 File Offset: 0x00013EA4
		public bool SetEquals(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			if (this.count != other.Count<T>())
			{
				return false;
			}
			foreach (T value in this)
			{
				if (!other.Contains(value))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060004A2 RID: 1186 RVA: 0x00015D38 File Offset: 0x00013F38
		public void SymmetricExceptWith(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			foreach (T item in other)
			{
				if (this.Contains(item))
				{
					this.Remove(item);
				}
				else
				{
					this.Add(item);
				}
			}
		}

		// Token: 0x060004A3 RID: 1187 RVA: 0x00015DC4 File Offset: 0x00013FC4
		public void UnionWith(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			foreach (T item in other)
			{
				this.Add(item);
			}
		}

		// Token: 0x060004A4 RID: 1188 RVA: 0x00015E34 File Offset: 0x00014034
		private bool CheckIsSubsetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			foreach (T value in this)
			{
				if (!other.Contains(value))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060004A5 RID: 1189 RVA: 0x00015EB8 File Offset: 0x000140B8
		public bool IsSubsetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			return this.count == 0 || (this.count <= other.Count<T>() && this.CheckIsSubsetOf(other));
		}

		// Token: 0x060004A6 RID: 1190 RVA: 0x00015F00 File Offset: 0x00014100
		public bool IsProperSubsetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			return this.count == 0 || (this.count < other.Count<T>() && this.CheckIsSubsetOf(other));
		}

		// Token: 0x060004A7 RID: 1191 RVA: 0x00015F48 File Offset: 0x00014148
		private bool CheckIsSupersetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			foreach (T item in other)
			{
				if (!this.Contains(item))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x060004A8 RID: 1192 RVA: 0x00015FC8 File Offset: 0x000141C8
		public bool IsSupersetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			return this.count >= other.Count<T>() && this.CheckIsSupersetOf(other);
		}

		// Token: 0x060004A9 RID: 1193 RVA: 0x00015FF8 File Offset: 0x000141F8
		public bool IsProperSupersetOf(IEnumerable<T> other)
		{
			if (other == null)
			{
				throw new ArgumentNullException("other");
			}
			return this.count > other.Count<T>() && this.CheckIsSupersetOf(other);
		}

		// Token: 0x060004AA RID: 1194 RVA: 0x00016028 File Offset: 0x00014228
		[MonoTODO]
		public static IEqualityComparer<HashSet<T>> CreateSetComparer()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060004AB RID: 1195 RVA: 0x00016030 File Offset: 0x00014230
		[MonoTODO]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"SerializationFormatter\"/>\n</PermissionSet>\n")]
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060004AC RID: 1196 RVA: 0x00016038 File Offset: 0x00014238
		[MonoTODO]
		public virtual void OnDeserialization(object sender)
		{
			if (this.si == null)
			{
				return;
			}
			throw new NotImplementedException();
		}

		// Token: 0x060004AD RID: 1197 RVA: 0x0001604C File Offset: 0x0001424C
		public HashSet<T>.Enumerator GetEnumerator()
		{
			return new HashSet<T>.Enumerator(this);
		}

		// Token: 0x04000108 RID: 264
		private const int INITIAL_SIZE = 10;

		// Token: 0x04000109 RID: 265
		private const float DEFAULT_LOAD_FACTOR = 0.9f;

		// Token: 0x0400010A RID: 266
		private const int NO_SLOT = -1;

		// Token: 0x0400010B RID: 267
		private const int HASH_FLAG = -2147483648;

		// Token: 0x0400010C RID: 268
		private int[] table;

		// Token: 0x0400010D RID: 269
		private HashSet<T>.Link[] links;

		// Token: 0x0400010E RID: 270
		private T[] slots;

		// Token: 0x0400010F RID: 271
		private int touched;

		// Token: 0x04000110 RID: 272
		private int empty_slot;

		// Token: 0x04000111 RID: 273
		private int count;

		// Token: 0x04000112 RID: 274
		private int threshold;

		// Token: 0x04000113 RID: 275
		private IEqualityComparer<T> comparer;

		// Token: 0x04000114 RID: 276
		private SerializationInfo si;

		// Token: 0x04000115 RID: 277
		private int generation;

		// Token: 0x02000051 RID: 81
		private struct Link
		{
			// Token: 0x04000116 RID: 278
			public int HashCode;

			// Token: 0x04000117 RID: 279
			public int Next;
		}

		// Token: 0x02000052 RID: 82
		[Serializable]
		public struct Enumerator : IEnumerator, IDisposable, IEnumerator<T>
		{
			// Token: 0x060004AE RID: 1198 RVA: 0x00016054 File Offset: 0x00014254
			internal Enumerator(HashSet<T> hashset)
			{
				this.hashset = hashset;
				this.stamp = hashset.generation;
			}

			// Token: 0x1700005A RID: 90
			// (get) Token: 0x060004AF RID: 1199 RVA: 0x0001606C File Offset: 0x0001426C
			object IEnumerator.Current
			{
				get
				{
					this.CheckState();
					if (this.next <= 0)
					{
						throw new InvalidOperationException("Current is not valid");
					}
					return this.current;
				}
			}

			// Token: 0x060004B0 RID: 1200 RVA: 0x000160A4 File Offset: 0x000142A4
			void IEnumerator.Reset()
			{
				this.CheckState();
				this.next = 0;
			}

			// Token: 0x060004B1 RID: 1201 RVA: 0x000160B4 File Offset: 0x000142B4
			public bool MoveNext()
			{
				this.CheckState();
				if (this.next < 0)
				{
					return false;
				}
				while (this.next < this.hashset.touched)
				{
					int num = this.next++;
					if (this.hashset.GetLinkHashCode(num) != 0)
					{
						this.current = this.hashset.slots[num];
						return true;
					}
				}
				this.next = -1;
				return false;
			}

			// Token: 0x1700005B RID: 91
			// (get) Token: 0x060004B2 RID: 1202 RVA: 0x00016134 File Offset: 0x00014334
			public T Current
			{
				get
				{
					return this.current;
				}
			}

			// Token: 0x060004B3 RID: 1203 RVA: 0x0001613C File Offset: 0x0001433C
			public void Dispose()
			{
				this.hashset = null;
			}

			// Token: 0x060004B4 RID: 1204 RVA: 0x00016148 File Offset: 0x00014348
			private void CheckState()
			{
				if (this.hashset == null)
				{
					throw new ObjectDisposedException(null);
				}
				if (this.hashset.generation != this.stamp)
				{
					throw new InvalidOperationException("HashSet have been modified while it was iterated over");
				}
			}

			// Token: 0x04000118 RID: 280
			private HashSet<T> hashset;

			// Token: 0x04000119 RID: 281
			private int next;

			// Token: 0x0400011A RID: 282
			private int stamp;

			// Token: 0x0400011B RID: 283
			private T current;
		}

		// Token: 0x02000053 RID: 83
		private static class PrimeHelper
		{
			// Token: 0x060004B6 RID: 1206 RVA: 0x0001619C File Offset: 0x0001439C
			private static bool TestPrime(int x)
			{
				if ((x & 1) != 0)
				{
					int num = (int)Math.Sqrt((double)x);
					for (int i = 3; i < num; i += 2)
					{
						if (x % i == 0)
						{
							return false;
						}
					}
					return true;
				}
				return x == 2;
			}

			// Token: 0x060004B7 RID: 1207 RVA: 0x000161DC File Offset: 0x000143DC
			private static int CalcPrime(int x)
			{
				for (int i = (x & -2) - 1; i < 2147483647; i += 2)
				{
					if (HashSet<T>.PrimeHelper.TestPrime(i))
					{
						return i;
					}
				}
				return x;
			}

			// Token: 0x060004B8 RID: 1208 RVA: 0x00016214 File Offset: 0x00014414
			public static int ToPrime(int x)
			{
				for (int i = 0; i < HashSet<T>.PrimeHelper.primes_table.Length; i++)
				{
					if (x <= HashSet<T>.PrimeHelper.primes_table[i])
					{
						return HashSet<T>.PrimeHelper.primes_table[i];
					}
				}
				return HashSet<T>.PrimeHelper.CalcPrime(x);
			}

			// Token: 0x0400011C RID: 284
			private static readonly int[] primes_table = new int[]
			{
				11,
				19,
				37,
				73,
				109,
				163,
				251,
				367,
				557,
				823,
				1237,
				1861,
				2777,
				4177,
				6247,
				9371,
				14057,
				21089,
				31627,
				47431,
				71143,
				106721,
				160073,
				240101,
				360163,
				540217,
				810343,
				1215497,
				1823231,
				2734867,
				4102283,
				6153409,
				9230113,
				13845163
			};
		}
	}
}
