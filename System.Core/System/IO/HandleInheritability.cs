﻿using System;

namespace System.IO
{
	// Token: 0x02000086 RID: 134
	public enum HandleInheritability
	{
		// Token: 0x040001A9 RID: 425
		None,
		// Token: 0x040001AA RID: 426
		Inheritable
	}
}
