﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x0200007F RID: 127
	internal class Win32AnonymousPipeClient : Win32AnonymousPipe, IPipe, IAnonymousPipeClient
	{
		// Token: 0x060005FA RID: 1530 RVA: 0x0001A504 File Offset: 0x00018704
		public Win32AnonymousPipeClient(AnonymousPipeClientStream owner, SafePipeHandle handle)
		{
			this.handle = handle;
		}

		// Token: 0x170000A9 RID: 169
		// (get) Token: 0x060005FB RID: 1531 RVA: 0x0001A514 File Offset: 0x00018714
		public override SafePipeHandle Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x0400019B RID: 411
		private SafePipeHandle handle;
	}
}
