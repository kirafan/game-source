﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x02000090 RID: 144
	// (Invoke) Token: 0x0600063F RID: 1599
	public delegate void PipeStreamImpersonationWorker();
}
