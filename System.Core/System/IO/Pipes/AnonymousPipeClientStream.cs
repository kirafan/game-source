﻿using System;
using System.Globalization;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000067 RID: 103
	[MonoTODO("Anonymous pipes are not working even on win32, due to some access authorization issue")]
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	public sealed class AnonymousPipeClientStream : PipeStream
	{
		// Token: 0x0600055A RID: 1370 RVA: 0x0001959C File Offset: 0x0001779C
		public AnonymousPipeClientStream(string pipeHandleAsString) : this(PipeDirection.In, pipeHandleAsString)
		{
		}

		// Token: 0x0600055B RID: 1371 RVA: 0x000195A8 File Offset: 0x000177A8
		public AnonymousPipeClientStream(PipeDirection direction, string pipeHandleAsString) : this(direction, AnonymousPipeClientStream.ToSafePipeHandle(pipeHandleAsString))
		{
		}

		// Token: 0x0600055C RID: 1372 RVA: 0x000195B8 File Offset: 0x000177B8
		public AnonymousPipeClientStream(PipeDirection direction, SafePipeHandle safePipeHandle) : base(direction, 1024)
		{
			base.InitializeHandle(safePipeHandle, false, false);
			base.IsConnected = true;
		}

		// Token: 0x0600055D RID: 1373 RVA: 0x000195D8 File Offset: 0x000177D8
		private static SafePipeHandle ToSafePipeHandle(string pipeHandleAsString)
		{
			if (pipeHandleAsString == null)
			{
				throw new ArgumentNullException("pipeHandleAsString");
			}
			return new SafePipeHandle(new IntPtr(long.Parse(pipeHandleAsString, NumberFormatInfo.InvariantInfo)), false);
		}

		// Token: 0x17000080 RID: 128
		// (set) Token: 0x0600055E RID: 1374 RVA: 0x00019604 File Offset: 0x00017804
		public override PipeTransmissionMode ReadMode
		{
			set
			{
				if (value == PipeTransmissionMode.Message)
				{
					throw new NotSupportedException();
				}
			}
		}

		// Token: 0x17000081 RID: 129
		// (get) Token: 0x0600055F RID: 1375 RVA: 0x00019614 File Offset: 0x00017814
		public override PipeTransmissionMode TransmissionMode
		{
			get
			{
				return PipeTransmissionMode.Byte;
			}
		}
	}
}
