﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000083 RID: 131
	internal class Win32NamedPipeServer : Win32NamedPipe, IPipe, INamedPipeServer
	{
		// Token: 0x0600060C RID: 1548 RVA: 0x0001A7BC File Offset: 0x000189BC
		public Win32NamedPipeServer(NamedPipeServerStream owner, SafePipeHandle safePipeHandle)
		{
			this.handle = safePipeHandle;
		}

		// Token: 0x0600060D RID: 1549 RVA: 0x0001A7CC File Offset: 0x000189CC
		public Win32NamedPipeServer(NamedPipeServerStream owner, string pipeName, int maxNumberOfServerInstances, PipeTransmissionMode transmissionMode, PipeAccessRights rights, PipeOptions options, int inBufferSize, int outBufferSize, HandleInheritability inheritability)
		{
			string name = string.Format("\\\\.\\pipe\\{0}", pipeName);
			uint num = 0U;
			if ((rights & PipeAccessRights.ReadData) != (PipeAccessRights)0)
			{
				num |= 1U;
			}
			if ((rights & PipeAccessRights.WriteData) != (PipeAccessRights)0)
			{
				num |= 2U;
			}
			if ((options & PipeOptions.WriteThrough) != PipeOptions.None)
			{
				num |= 2147483648U;
			}
			int num2 = 0;
			if ((owner.TransmissionMode & PipeTransmissionMode.Message) != PipeTransmissionMode.Byte)
			{
				num2 |= 4;
			}
			if ((options & PipeOptions.Asynchronous) != PipeOptions.None)
			{
				num2 |= 1;
			}
			SecurityAttributesHack securityAttributesHack = new SecurityAttributesHack(inheritability == HandleInheritability.Inheritable);
			IntPtr intPtr = Win32Marshal.CreateNamedPipe(name, num, num2, maxNumberOfServerInstances, outBufferSize, inBufferSize, 0, ref securityAttributesHack, IntPtr.Zero);
			if (intPtr == new IntPtr(-1L))
			{
				throw new Win32Exception(Marshal.GetLastWin32Error());
			}
			this.handle = new SafePipeHandle(intPtr, true);
		}

		// Token: 0x170000B1 RID: 177
		// (get) Token: 0x0600060E RID: 1550 RVA: 0x0001A888 File Offset: 0x00018A88
		public override SafePipeHandle Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x0600060F RID: 1551 RVA: 0x0001A890 File Offset: 0x00018A90
		public void Disconnect()
		{
			Win32Marshal.DisconnectNamedPipe(this.Handle);
		}

		// Token: 0x06000610 RID: 1552 RVA: 0x0001A8A0 File Offset: 0x00018AA0
		public void WaitForConnection()
		{
			if (!Win32Marshal.ConnectNamedPipe(this.Handle, IntPtr.Zero))
			{
				throw new Win32Exception(Marshal.GetLastWin32Error());
			}
		}

		// Token: 0x040001A4 RID: 420
		private SafePipeHandle handle;
	}
}
