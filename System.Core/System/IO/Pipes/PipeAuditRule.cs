﻿using System;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Security.Principal;

namespace System.IO.Pipes
{
	// Token: 0x0200006D RID: 109
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	public sealed class PipeAuditRule : AuditRule
	{
		// Token: 0x0600058B RID: 1419 RVA: 0x00019BE8 File Offset: 0x00017DE8
		[MonoNotSupported("ACL is not supported in Mono")]
		public PipeAuditRule(IdentityReference identity, PipeAccessRights rights, AuditFlags flags) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, flags)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x0600058C RID: 1420 RVA: 0x00019C00 File Offset: 0x00017E00
		[MonoNotSupported("ACL is not supported in Mono")]
		public PipeAuditRule(string identity, PipeAccessRights rights, AuditFlags flags) : this(null, rights, flags)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x17000087 RID: 135
		// (get) Token: 0x0600058D RID: 1421 RVA: 0x00019C18 File Offset: 0x00017E18
		// (set) Token: 0x0600058E RID: 1422 RVA: 0x00019C20 File Offset: 0x00017E20
		[MonoNotSupported("ACL is not supported in Mono")]
		public PipeAccessRights PipeAccessRights { get; private set; }
	}
}
