﻿using System;
using Microsoft.Win32.SafeHandles;
using Mono.Unix.Native;

namespace System.IO.Pipes
{
	// Token: 0x0200007B RID: 123
	internal abstract class UnixNamedPipe : IPipe
	{
		// Token: 0x170000A3 RID: 163
		// (get) Token: 0x060005E5 RID: 1509
		public abstract SafePipeHandle Handle { get; }

		// Token: 0x060005E6 RID: 1510 RVA: 0x0001A1B4 File Offset: 0x000183B4
		public void WaitForPipeDrain()
		{
			throw new NotImplementedException();
		}

		// Token: 0x060005E7 RID: 1511 RVA: 0x0001A1BC File Offset: 0x000183BC
		public void EnsureTargetFile(string name)
		{
			if (!File.Exists(name))
			{
				int num = Syscall.mknod(name, FilePermissions.S_ISUID | FilePermissions.S_ISGID | FilePermissions.S_ISVTX | FilePermissions.S_IRUSR | FilePermissions.S_IWUSR | FilePermissions.S_IXUSR | FilePermissions.S_IRGRP | FilePermissions.S_IWGRP | FilePermissions.S_IXGRP | FilePermissions.S_IROTH | FilePermissions.S_IWOTH | FilePermissions.S_IXOTH | FilePermissions.S_IFIFO, 0UL);
				if (num != 0)
				{
					throw new IOException(string.Format("Error on creating named pipe: error code {0}", num));
				}
			}
		}

		// Token: 0x060005E8 RID: 1512 RVA: 0x0001A200 File Offset: 0x00018400
		protected void ValidateOptions(PipeOptions options, PipeTransmissionMode mode)
		{
			if ((options & PipeOptions.WriteThrough) != PipeOptions.None)
			{
				throw new NotImplementedException("WriteThrough is not supported");
			}
			if ((mode & PipeTransmissionMode.Message) != PipeTransmissionMode.Byte)
			{
				throw new NotImplementedException("Message transmission mode is not supported");
			}
			if ((options & PipeOptions.Asynchronous) != PipeOptions.None)
			{
				throw new NotImplementedException("Asynchronous pipe mode is not supported");
			}
		}

		// Token: 0x060005E9 RID: 1513 RVA: 0x0001A23C File Offset: 0x0001843C
		protected string RightsToAccess(PipeAccessRights rights)
		{
			string result;
			if ((rights & PipeAccessRights.ReadData) != (PipeAccessRights)0)
			{
				if ((rights & PipeAccessRights.WriteData) != (PipeAccessRights)0)
				{
					result = "r+";
				}
				else
				{
					result = "r";
				}
			}
			else
			{
				if ((rights & PipeAccessRights.WriteData) == (PipeAccessRights)0)
				{
					throw new InvalidOperationException("The pipe must be opened to either read or write");
				}
				result = "w";
			}
			return result;
		}

		// Token: 0x060005EA RID: 1514 RVA: 0x0001A290 File Offset: 0x00018490
		protected FileAccess RightsToFileAccess(PipeAccessRights rights)
		{
			if ((rights & PipeAccessRights.ReadData) != (PipeAccessRights)0)
			{
				if ((rights & PipeAccessRights.WriteData) != (PipeAccessRights)0)
				{
					return FileAccess.ReadWrite;
				}
				return FileAccess.Read;
			}
			else
			{
				if ((rights & PipeAccessRights.WriteData) != (PipeAccessRights)0)
				{
					return FileAccess.Write;
				}
				throw new InvalidOperationException("The pipe must be opened to either read or write");
			}
		}
	}
}
