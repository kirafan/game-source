﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000079 RID: 121
	internal class UnixAnonymousPipeClient : UnixAnonymousPipe, IPipe, IAnonymousPipeClient
	{
		// Token: 0x060005DD RID: 1501 RVA: 0x0001A150 File Offset: 0x00018350
		public UnixAnonymousPipeClient(AnonymousPipeClientStream owner, SafePipeHandle handle)
		{
			this.handle = handle;
		}

		// Token: 0x170000A0 RID: 160
		// (get) Token: 0x060005DE RID: 1502 RVA: 0x0001A160 File Offset: 0x00018360
		public override SafePipeHandle Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x04000192 RID: 402
		private SafePipeHandle handle;
	}
}
