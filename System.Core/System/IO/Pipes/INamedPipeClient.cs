﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x02000072 RID: 114
	internal interface INamedPipeClient : IPipe
	{
		// Token: 0x06000593 RID: 1427
		void Connect();

		// Token: 0x06000594 RID: 1428
		void Connect(int timeout);

		// Token: 0x1700008A RID: 138
		// (get) Token: 0x06000595 RID: 1429
		int NumberOfServerInstances { get; }

		// Token: 0x1700008B RID: 139
		// (get) Token: 0x06000596 RID: 1430
		bool IsAsync { get; }
	}
}
