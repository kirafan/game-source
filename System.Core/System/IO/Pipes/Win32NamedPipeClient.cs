﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000082 RID: 130
	internal class Win32NamedPipeClient : Win32NamedPipe, IPipe, INamedPipeClient
	{
		// Token: 0x06000605 RID: 1541 RVA: 0x0001A66C File Offset: 0x0001886C
		public Win32NamedPipeClient(NamedPipeClientStream owner, SafePipeHandle safePipeHandle)
		{
			this.handle = safePipeHandle;
			this.owner = owner;
		}

		// Token: 0x06000606 RID: 1542 RVA: 0x0001A684 File Offset: 0x00018884
		public Win32NamedPipeClient(NamedPipeClientStream owner, string serverName, string pipeName, PipeAccessRights desiredAccessRights, PipeOptions options, HandleInheritability inheritability)
		{
			Win32NamedPipeClient <>f__this = this;
			this.name = string.Format("\\\\{0}\\pipe\\{1}", serverName, pipeName);
			SecurityAttributesHack att = new SecurityAttributesHack(inheritability == HandleInheritability.Inheritable);
			this.is_async = ((options & PipeOptions.Asynchronous) != PipeOptions.None);
			this.opener = delegate()
			{
				IntPtr intPtr = Win32Marshal.CreateFile(<>f__this.name, desiredAccessRights, FileShare.None, ref att, 3, 0, IntPtr.Zero);
				if (intPtr == new IntPtr(-1L))
				{
					throw new Win32Exception(Marshal.GetLastWin32Error());
				}
				return new SafePipeHandle(intPtr, true);
			};
			this.owner = owner;
		}

		// Token: 0x170000AE RID: 174
		// (get) Token: 0x06000607 RID: 1543 RVA: 0x0001A6F8 File Offset: 0x000188F8
		public override SafePipeHandle Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x170000AF RID: 175
		// (get) Token: 0x06000608 RID: 1544 RVA: 0x0001A700 File Offset: 0x00018900
		public bool IsAsync
		{
			get
			{
				return this.is_async;
			}
		}

		// Token: 0x06000609 RID: 1545 RVA: 0x0001A708 File Offset: 0x00018908
		public void Connect()
		{
			if (this.owner.IsConnected)
			{
				throw new InvalidOperationException("The named pipe is already connected");
			}
			this.handle = this.opener();
		}

		// Token: 0x0600060A RID: 1546 RVA: 0x0001A744 File Offset: 0x00018944
		public void Connect(int timeout)
		{
			if (this.owner.IsConnected)
			{
				throw new InvalidOperationException("The named pipe is already connected");
			}
			if (!Win32Marshal.WaitNamedPipe(this.name, timeout))
			{
				throw new Win32Exception(Marshal.GetLastWin32Error());
			}
			this.Connect();
		}

		// Token: 0x170000B0 RID: 176
		// (get) Token: 0x0600060B RID: 1547 RVA: 0x0001A784 File Offset: 0x00018984
		public int NumberOfServerInstances
		{
			get
			{
				byte[] userName = null;
				int num;
				int result;
				int num2;
				int num3;
				if (!Win32Marshal.GetNamedPipeHandleState(this.Handle, out num, out result, out num2, out num3, userName, 0))
				{
					throw new Win32Exception(Marshal.GetLastWin32Error());
				}
				return result;
			}
		}

		// Token: 0x0400019F RID: 415
		private NamedPipeClientStream owner;

		// Token: 0x040001A0 RID: 416
		private Func<SafePipeHandle> opener;

		// Token: 0x040001A1 RID: 417
		private bool is_async;

		// Token: 0x040001A2 RID: 418
		private string name;

		// Token: 0x040001A3 RID: 419
		private SafePipeHandle handle;
	}
}
