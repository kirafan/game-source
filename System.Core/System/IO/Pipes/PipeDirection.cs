﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x0200006E RID: 110
	[Serializable]
	public enum PipeDirection
	{
		// Token: 0x0400017B RID: 379
		In = 1,
		// Token: 0x0400017C RID: 380
		Out,
		// Token: 0x0400017D RID: 381
		InOut
	}
}
