﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x02000084 RID: 132
	internal struct SecurityAttributesHack
	{
		// Token: 0x06000611 RID: 1553 RVA: 0x0001A8D0 File Offset: 0x00018AD0
		public SecurityAttributesHack(bool inheritable)
		{
			this.Length = 0;
			this.SecurityDescriptor = IntPtr.Zero;
			this.Inheritable = inheritable;
		}

		// Token: 0x040001A5 RID: 421
		public readonly int Length;

		// Token: 0x040001A6 RID: 422
		public readonly IntPtr SecurityDescriptor;

		// Token: 0x040001A7 RID: 423
		public readonly bool Inheritable;
	}
}
