﻿using System;
using Microsoft.Win32.SafeHandles;
using Mono.Unix.Native;

namespace System.IO.Pipes
{
	// Token: 0x0200007D RID: 125
	internal class UnixNamedPipeServer : UnixNamedPipe, IPipe, INamedPipeServer
	{
		// Token: 0x060005F2 RID: 1522 RVA: 0x0001A448 File Offset: 0x00018648
		public UnixNamedPipeServer(NamedPipeServerStream owner, SafePipeHandle safePipeHandle)
		{
			this.handle = safePipeHandle;
		}

		// Token: 0x060005F3 RID: 1523 RVA: 0x0001A458 File Offset: 0x00018658
		public UnixNamedPipeServer(NamedPipeServerStream owner, string pipeName, int maxNumberOfServerInstances, PipeTransmissionMode transmissionMode, PipeAccessRights rights, PipeOptions options, int inBufferSize, int outBufferSize, HandleInheritability inheritability)
		{
			string text = Path.Combine("/var/tmp/", pipeName);
			base.EnsureTargetFile(text);
			string text2 = base.RightsToAccess(rights);
			base.ValidateOptions(options, owner.TransmissionMode);
			FileStream fileStream = new FileStream(text, FileMode.Open, base.RightsToFileAccess(rights), FileShare.ReadWrite);
			this.handle = new SafePipeHandle(fileStream.Handle, false);
			owner.Stream = fileStream;
			this.should_close_handle = true;
		}

		// Token: 0x170000A7 RID: 167
		// (get) Token: 0x060005F4 RID: 1524 RVA: 0x0001A4C8 File Offset: 0x000186C8
		public override SafePipeHandle Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x060005F5 RID: 1525 RVA: 0x0001A4D0 File Offset: 0x000186D0
		public void Disconnect()
		{
			if (this.should_close_handle)
			{
				Stdlib.fclose(this.handle.DangerousGetHandle());
			}
		}

		// Token: 0x060005F6 RID: 1526 RVA: 0x0001A4F0 File Offset: 0x000186F0
		public void WaitForConnection()
		{
		}

		// Token: 0x04000199 RID: 409
		private SafePipeHandle handle;

		// Token: 0x0400019A RID: 410
		private bool should_close_handle;
	}
}
