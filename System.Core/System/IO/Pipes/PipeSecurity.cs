﻿using System;
using System.Runtime.InteropServices;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Security.Principal;

namespace System.IO.Pipes
{
	// Token: 0x02000075 RID: 117
	[MonoNotSupported("ACL is not supported in Mono")]
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	public class PipeSecurity : NativeObjectSecurity
	{
		// Token: 0x06000599 RID: 1433 RVA: 0x00019C2C File Offset: 0x00017E2C
		[MonoNotSupported("ACL is not supported in Mono")]
		public PipeSecurity() : base(false, ResourceType.FileObject)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x1700008C RID: 140
		// (get) Token: 0x0600059A RID: 1434 RVA: 0x00019C40 File Offset: 0x00017E40
		public override Type AccessRightType
		{
			get
			{
				return typeof(PipeAccessRights);
			}
		}

		// Token: 0x1700008D RID: 141
		// (get) Token: 0x0600059B RID: 1435 RVA: 0x00019C4C File Offset: 0x00017E4C
		public override Type AccessRuleType
		{
			get
			{
				return typeof(PipeAccessRule);
			}
		}

		// Token: 0x1700008E RID: 142
		// (get) Token: 0x0600059C RID: 1436 RVA: 0x00019C58 File Offset: 0x00017E58
		public override Type AuditRuleType
		{
			get
			{
				return typeof(PipeAuditRule);
			}
		}

		// Token: 0x0600059D RID: 1437 RVA: 0x00019C64 File Offset: 0x00017E64
		[MonoNotSupported("ACL is not supported in Mono")]
		public override AccessRule AccessRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x0600059E RID: 1438 RVA: 0x00019C70 File Offset: 0x00017E70
		[MonoNotSupported("ACL is not supported in Mono")]
		public void AddAccessRule(PipeAccessRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x0600059F RID: 1439 RVA: 0x00019C7C File Offset: 0x00017E7C
		[MonoNotSupported("ACL is not supported in Mono")]
		public void AddAuditRule(PipeAuditRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A0 RID: 1440 RVA: 0x00019C88 File Offset: 0x00017E88
		[MonoNotSupported("ACL is not supported in Mono")]
		public sealed override AuditRule AuditRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A1 RID: 1441 RVA: 0x00019C94 File Offset: 0x00017E94
		[MonoNotSupported("ACL is not supported in Mono")]
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		protected internal void Persist(SafeHandle handle)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A2 RID: 1442 RVA: 0x00019CA0 File Offset: 0x00017EA0
		[MonoNotSupported("ACL is not supported in Mono")]
		[PermissionSet(SecurityAction.Assert, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		protected internal void Persist(string name)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A3 RID: 1443 RVA: 0x00019CAC File Offset: 0x00017EAC
		[MonoNotSupported("ACL is not supported in Mono")]
		public bool RemoveAccessRule(PipeAccessRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A4 RID: 1444 RVA: 0x00019CB8 File Offset: 0x00017EB8
		[MonoNotSupported("ACL is not supported in Mono")]
		public void RemoveAccessRuleSpecific(PipeAccessRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A5 RID: 1445 RVA: 0x00019CC4 File Offset: 0x00017EC4
		[MonoNotSupported("ACL is not supported in Mono")]
		public bool RemoveAuditRule(PipeAuditRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A6 RID: 1446 RVA: 0x00019CD0 File Offset: 0x00017ED0
		[MonoNotSupported("ACL is not supported in Mono")]
		public void RemoveAuditRuleAll(PipeAuditRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A7 RID: 1447 RVA: 0x00019CDC File Offset: 0x00017EDC
		[MonoNotSupported("ACL is not supported in Mono")]
		public void RemoveAuditRuleSpecific(PipeAuditRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A8 RID: 1448 RVA: 0x00019CE8 File Offset: 0x00017EE8
		[MonoNotSupported("ACL is not supported in Mono")]
		public void ResetAccessRule(PipeAccessRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005A9 RID: 1449 RVA: 0x00019CF4 File Offset: 0x00017EF4
		[MonoNotSupported("ACL is not supported in Mono")]
		public void SetAccessRule(PipeAccessRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005AA RID: 1450 RVA: 0x00019D00 File Offset: 0x00017F00
		[MonoNotSupported("ACL is not supported in Mono")]
		public void SetAuditRule(PipeAuditRule rule)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}
	}
}
