﻿using System;
using System.Security.Permissions;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000076 RID: 118
	[PermissionSet((SecurityAction)15, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\"/>\n")]
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	public abstract class PipeStream : Stream
	{
		// Token: 0x060005AB RID: 1451 RVA: 0x00019D0C File Offset: 0x00017F0C
		protected PipeStream(PipeDirection direction, int bufferSize) : this(direction, PipeTransmissionMode.Byte, bufferSize)
		{
		}

		// Token: 0x060005AC RID: 1452 RVA: 0x00019D18 File Offset: 0x00017F18
		protected PipeStream(PipeDirection direction, PipeTransmissionMode transmissionMode, int outBufferSize)
		{
			this.direction = direction;
			this.transmission_mode = transmissionMode;
			this.read_trans_mode = transmissionMode;
			if (outBufferSize <= 0)
			{
				throw new ArgumentOutOfRangeException("bufferSize must be greater than 0");
			}
			this.buffer_size = outBufferSize;
		}

		// Token: 0x1700008F RID: 143
		// (get) Token: 0x060005AD RID: 1453 RVA: 0x00019D5C File Offset: 0x00017F5C
		internal static bool IsWindows
		{
			get
			{
				return Win32Marshal.IsWindows;
			}
		}

		// Token: 0x060005AE RID: 1454 RVA: 0x00019D64 File Offset: 0x00017F64
		internal Exception ThrowACLException()
		{
			return new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x060005AF RID: 1455 RVA: 0x00019D70 File Offset: 0x00017F70
		internal static PipeAccessRights ToAccessRights(PipeDirection direction)
		{
			switch (direction)
			{
			case PipeDirection.In:
				return PipeAccessRights.ReadData;
			case PipeDirection.Out:
				return PipeAccessRights.WriteData;
			case PipeDirection.InOut:
				return PipeAccessRights.ReadData | PipeAccessRights.WriteData;
			default:
				throw new ArgumentOutOfRangeException();
			}
		}

		// Token: 0x060005B0 RID: 1456 RVA: 0x00019DA4 File Offset: 0x00017FA4
		internal static PipeDirection ToDirection(PipeAccessRights rights)
		{
			bool flag = (rights & PipeAccessRights.ReadData) != (PipeAccessRights)0;
			bool flag2 = (rights & PipeAccessRights.WriteData) != (PipeAccessRights)0;
			if (flag)
			{
				if (flag2)
				{
					return PipeDirection.InOut;
				}
				return PipeDirection.In;
			}
			else
			{
				if (flag2)
				{
					return PipeDirection.Out;
				}
				throw new ArgumentOutOfRangeException();
			}
		}

		// Token: 0x17000090 RID: 144
		// (get) Token: 0x060005B1 RID: 1457 RVA: 0x00019DE4 File Offset: 0x00017FE4
		public override bool CanRead
		{
			get
			{
				return (this.direction & PipeDirection.In) != (PipeDirection)0;
			}
		}

		// Token: 0x17000091 RID: 145
		// (get) Token: 0x060005B2 RID: 1458 RVA: 0x00019DF4 File Offset: 0x00017FF4
		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000092 RID: 146
		// (get) Token: 0x060005B3 RID: 1459 RVA: 0x00019DF8 File Offset: 0x00017FF8
		public override bool CanWrite
		{
			get
			{
				return (this.direction & PipeDirection.Out) != (PipeDirection)0;
			}
		}

		// Token: 0x17000093 RID: 147
		// (get) Token: 0x060005B4 RID: 1460 RVA: 0x00019E08 File Offset: 0x00018008
		public virtual int InBufferSize
		{
			get
			{
				return this.buffer_size;
			}
		}

		// Token: 0x17000094 RID: 148
		// (get) Token: 0x060005B5 RID: 1461 RVA: 0x00019E10 File Offset: 0x00018010
		// (set) Token: 0x060005B6 RID: 1462 RVA: 0x00019E18 File Offset: 0x00018018
		public bool IsAsync { get; private set; }

		// Token: 0x17000095 RID: 149
		// (get) Token: 0x060005B7 RID: 1463 RVA: 0x00019E24 File Offset: 0x00018024
		// (set) Token: 0x060005B8 RID: 1464 RVA: 0x00019E2C File Offset: 0x0001802C
		public bool IsConnected { get; protected set; }

		// Token: 0x17000096 RID: 150
		// (get) Token: 0x060005B9 RID: 1465 RVA: 0x00019E38 File Offset: 0x00018038
		// (set) Token: 0x060005BA RID: 1466 RVA: 0x00019EB4 File Offset: 0x000180B4
		internal Stream Stream
		{
			get
			{
				if (!this.IsConnected)
				{
					throw new InvalidOperationException("Pipe is not connected");
				}
				if (this.stream == null)
				{
					this.stream = new FileStream(this.handle.DangerousGetHandle(), (!this.CanRead) ? FileAccess.Write : ((!this.CanWrite) ? FileAccess.Read : FileAccess.ReadWrite), true, this.buffer_size, this.IsAsync);
				}
				return this.stream;
			}
			set
			{
				this.stream = value;
			}
		}

		// Token: 0x17000097 RID: 151
		// (get) Token: 0x060005BB RID: 1467 RVA: 0x00019EC0 File Offset: 0x000180C0
		// (set) Token: 0x060005BC RID: 1468 RVA: 0x00019EC8 File Offset: 0x000180C8
		private protected bool IsHandleExposed { protected get; private set; }

		// Token: 0x17000098 RID: 152
		// (get) Token: 0x060005BD RID: 1469 RVA: 0x00019ED4 File Offset: 0x000180D4
		// (set) Token: 0x060005BE RID: 1470 RVA: 0x00019EDC File Offset: 0x000180DC
		[MonoTODO]
		public bool IsMessageComplete { get; private set; }

		// Token: 0x17000099 RID: 153
		// (get) Token: 0x060005BF RID: 1471 RVA: 0x00019EE8 File Offset: 0x000180E8
		[MonoTODO]
		public virtual int OutBufferSize
		{
			get
			{
				return this.buffer_size;
			}
		}

		// Token: 0x1700009A RID: 154
		// (get) Token: 0x060005C0 RID: 1472 RVA: 0x00019EF0 File Offset: 0x000180F0
		// (set) Token: 0x060005C1 RID: 1473 RVA: 0x00019F00 File Offset: 0x00018100
		public virtual PipeTransmissionMode ReadMode
		{
			get
			{
				this.CheckPipePropertyOperations();
				return this.read_trans_mode;
			}
			set
			{
				this.CheckPipePropertyOperations();
				this.read_trans_mode = value;
			}
		}

		// Token: 0x1700009B RID: 155
		// (get) Token: 0x060005C2 RID: 1474 RVA: 0x00019F10 File Offset: 0x00018110
		public SafePipeHandle SafePipeHandle
		{
			get
			{
				this.CheckPipePropertyOperations();
				return this.handle;
			}
		}

		// Token: 0x1700009C RID: 156
		// (get) Token: 0x060005C3 RID: 1475 RVA: 0x00019F20 File Offset: 0x00018120
		public virtual PipeTransmissionMode TransmissionMode
		{
			get
			{
				this.CheckPipePropertyOperations();
				return this.transmission_mode;
			}
		}

		// Token: 0x060005C4 RID: 1476 RVA: 0x00019F30 File Offset: 0x00018130
		[MonoTODO]
		protected internal virtual void CheckPipePropertyOperations()
		{
		}

		// Token: 0x060005C5 RID: 1477 RVA: 0x00019F34 File Offset: 0x00018134
		[MonoTODO]
		protected internal void CheckReadOperations()
		{
			if (!this.IsConnected)
			{
				throw new InvalidOperationException("Pipe is not connected");
			}
			if (!this.CanRead)
			{
				throw new NotSupportedException("The pipe stream does not support read operations");
			}
		}

		// Token: 0x060005C6 RID: 1478 RVA: 0x00019F70 File Offset: 0x00018170
		[MonoTODO]
		protected internal void CheckWriteOperations()
		{
			if (!this.IsConnected)
			{
				throw new InvalidOperationException("Pipe us not connected");
			}
			if (!this.CanWrite)
			{
				throw new NotSupportedException("The pipe stream does not support write operations");
			}
		}

		// Token: 0x060005C7 RID: 1479 RVA: 0x00019FAC File Offset: 0x000181AC
		protected void InitializeHandle(SafePipeHandle handle, bool isExposed, bool isAsync)
		{
			this.handle = handle;
			this.IsHandleExposed = isExposed;
			this.IsAsync = isAsync;
		}

		// Token: 0x060005C8 RID: 1480 RVA: 0x00019FC4 File Offset: 0x000181C4
		protected override void Dispose(bool disposing)
		{
			if (this.handle != null && disposing)
			{
				this.handle.Dispose();
			}
		}

		// Token: 0x1700009D RID: 157
		// (get) Token: 0x060005C9 RID: 1481 RVA: 0x00019FE4 File Offset: 0x000181E4
		public override long Length
		{
			get
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x1700009E RID: 158
		// (get) Token: 0x060005CA RID: 1482 RVA: 0x00019FEC File Offset: 0x000181EC
		// (set) Token: 0x060005CB RID: 1483 RVA: 0x00019FF0 File Offset: 0x000181F0
		public override long Position
		{
			get
			{
				return 0L;
			}
			set
			{
				throw new NotSupportedException();
			}
		}

		// Token: 0x060005CC RID: 1484 RVA: 0x00019FF8 File Offset: 0x000181F8
		public override void SetLength(long value)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060005CD RID: 1485 RVA: 0x0001A000 File Offset: 0x00018200
		public override long Seek(long offset, SeekOrigin origin)
		{
			throw new NotSupportedException();
		}

		// Token: 0x060005CE RID: 1486 RVA: 0x0001A008 File Offset: 0x00018208
		[MonoNotSupported("ACL is not supported in Mono")]
		public PipeSecurity GetAccessControl()
		{
			throw this.ThrowACLException();
		}

		// Token: 0x060005CF RID: 1487 RVA: 0x0001A010 File Offset: 0x00018210
		[MonoNotSupported("ACL is not supported in Mono")]
		public void SetAccessControl(PipeSecurity pipeSecurity)
		{
			throw this.ThrowACLException();
		}

		// Token: 0x060005D0 RID: 1488 RVA: 0x0001A018 File Offset: 0x00018218
		public void WaitForPipeDrain()
		{
		}

		// Token: 0x060005D1 RID: 1489 RVA: 0x0001A01C File Offset: 0x0001821C
		[MonoTODO]
		public override int Read(byte[] buffer, int offset, int count)
		{
			this.CheckReadOperations();
			return this.Stream.Read(buffer, offset, count);
		}

		// Token: 0x060005D2 RID: 1490 RVA: 0x0001A040 File Offset: 0x00018240
		[MonoTODO]
		public override int ReadByte()
		{
			this.CheckReadOperations();
			return this.Stream.ReadByte();
		}

		// Token: 0x060005D3 RID: 1491 RVA: 0x0001A054 File Offset: 0x00018254
		[MonoTODO]
		public override void Write(byte[] buffer, int offset, int count)
		{
			this.CheckWriteOperations();
			this.Stream.Write(buffer, offset, count);
		}

		// Token: 0x060005D4 RID: 1492 RVA: 0x0001A078 File Offset: 0x00018278
		[MonoTODO]
		public override void WriteByte(byte value)
		{
			this.CheckWriteOperations();
			this.Stream.WriteByte(value);
		}

		// Token: 0x060005D5 RID: 1493 RVA: 0x0001A08C File Offset: 0x0001828C
		[MonoTODO]
		public override void Flush()
		{
			this.CheckWriteOperations();
			this.Stream.Flush();
		}

		// Token: 0x060005D6 RID: 1494 RVA: 0x0001A0A0 File Offset: 0x000182A0
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
		public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (this.read_delegate == null)
			{
				this.read_delegate = new Func<byte[], int, int, int>(this.Read);
			}
			return this.read_delegate.BeginInvoke(buffer, offset, count, callback, state);
		}

		// Token: 0x060005D7 RID: 1495 RVA: 0x0001A0E0 File Offset: 0x000182E0
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
		public override IAsyncResult BeginWrite(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		{
			if (this.write_delegate == null)
			{
				this.write_delegate = new Action<byte[], int, int>(this.Write);
			}
			return this.write_delegate.BeginInvoke(buffer, offset, count, callback, state);
		}

		// Token: 0x060005D8 RID: 1496 RVA: 0x0001A120 File Offset: 0x00018320
		public override int EndRead(IAsyncResult asyncResult)
		{
			return this.read_delegate.EndInvoke(asyncResult);
		}

		// Token: 0x060005D9 RID: 1497 RVA: 0x0001A130 File Offset: 0x00018330
		public override void EndWrite(IAsyncResult asyncResult)
		{
			this.write_delegate.EndInvoke(asyncResult);
		}

		// Token: 0x04000182 RID: 386
		internal const int DefaultBufferSize = 1024;

		// Token: 0x04000183 RID: 387
		private PipeDirection direction;

		// Token: 0x04000184 RID: 388
		private PipeTransmissionMode transmission_mode;

		// Token: 0x04000185 RID: 389
		private PipeTransmissionMode read_trans_mode;

		// Token: 0x04000186 RID: 390
		private int buffer_size;

		// Token: 0x04000187 RID: 391
		private SafePipeHandle handle;

		// Token: 0x04000188 RID: 392
		private Stream stream;

		// Token: 0x04000189 RID: 393
		private Func<byte[], int, int, int> read_delegate;

		// Token: 0x0400018A RID: 394
		private Action<byte[], int, int> write_delegate;
	}
}
