﻿using System;
using System.Security.AccessControl;
using System.Security.Permissions;
using System.Security.Principal;

namespace System.IO.Pipes
{
	// Token: 0x0200006C RID: 108
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	public sealed class PipeAccessRule : AccessRule
	{
		// Token: 0x06000587 RID: 1415 RVA: 0x00019BA4 File Offset: 0x00017DA4
		[MonoNotSupported("ACL is not supported in Mono")]
		public PipeAccessRule(IdentityReference identity, PipeAccessRights rights, AccessControlType type) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, type)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x06000588 RID: 1416 RVA: 0x00019BBC File Offset: 0x00017DBC
		[MonoNotSupported("ACL is not supported in Mono")]
		public PipeAccessRule(string identity, PipeAccessRights rights, AccessControlType type) : this(null, rights, type)
		{
			throw new NotImplementedException("ACL is not supported in Mono");
		}

		// Token: 0x17000086 RID: 134
		// (get) Token: 0x06000589 RID: 1417 RVA: 0x00019BD4 File Offset: 0x00017DD4
		// (set) Token: 0x0600058A RID: 1418 RVA: 0x00019BDC File Offset: 0x00017DDC
		[MonoNotSupported("ACL is not supported in Mono")]
		public PipeAccessRights PipeAccessRights { get; private set; }
	}
}
