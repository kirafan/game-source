﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000078 RID: 120
	internal abstract class UnixAnonymousPipe : IPipe
	{
		// Token: 0x1700009F RID: 159
		// (get) Token: 0x060005DB RID: 1499
		public abstract SafePipeHandle Handle { get; }

		// Token: 0x060005DC RID: 1500 RVA: 0x0001A148 File Offset: 0x00018348
		public void WaitForPipeDrain()
		{
			throw new NotImplementedException();
		}
	}
}
