﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000071 RID: 113
	internal interface IAnonymousPipeServer : IPipe
	{
		// Token: 0x17000089 RID: 137
		// (get) Token: 0x06000591 RID: 1425
		SafePipeHandle ClientHandle { get; }

		// Token: 0x06000592 RID: 1426
		void DisposeLocalCopyOfClientHandle();
	}
}
