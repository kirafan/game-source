﻿using System;
using System.Linq;
using System.Net;
using System.Threading;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x0200007C RID: 124
	internal class UnixNamedPipeClient : UnixNamedPipe, IPipe, INamedPipeClient
	{
		// Token: 0x060005EB RID: 1515 RVA: 0x0001A2C8 File Offset: 0x000184C8
		public UnixNamedPipeClient(NamedPipeClientStream owner, SafePipeHandle safePipeHandle)
		{
			this.owner = owner;
			this.handle = safePipeHandle;
		}

		// Token: 0x060005EC RID: 1516 RVA: 0x0001A2E0 File Offset: 0x000184E0
		public UnixNamedPipeClient(NamedPipeClientStream owner, string serverName, string pipeName, PipeAccessRights desiredAccessRights, PipeOptions options, HandleInheritability inheritability)
		{
			UnixNamedPipeClient <>f__this = this;
			this.owner = owner;
			if (serverName != "." && !Dns.GetHostEntry(serverName).AddressList.Contains(IPAddress.Loopback))
			{
				throw new NotImplementedException("Unix fifo does not support remote server connection");
			}
			string name = Path.Combine("/var/tmp/", pipeName);
			base.EnsureTargetFile(name);
			string text = base.RightsToAccess(desiredAccessRights);
			base.ValidateOptions(options, owner.TransmissionMode);
			this.opener = delegate()
			{
				FileStream fileStream = new FileStream(name, FileMode.Open, <>f__this.RightsToFileAccess(desiredAccessRights), FileShare.ReadWrite);
				owner.Stream = fileStream;
				<>f__this.handle = new SafePipeHandle(fileStream.Handle, false);
			};
		}

		// Token: 0x170000A4 RID: 164
		// (get) Token: 0x060005ED RID: 1517 RVA: 0x0001A3A0 File Offset: 0x000185A0
		public override SafePipeHandle Handle
		{
			get
			{
				return this.handle;
			}
		}

		// Token: 0x060005EE RID: 1518 RVA: 0x0001A3A8 File Offset: 0x000185A8
		public void Connect()
		{
			if (this.owner.IsConnected)
			{
				throw new InvalidOperationException("The named pipe is already connected");
			}
			this.opener();
		}

		// Token: 0x060005EF RID: 1519 RVA: 0x0001A3DC File Offset: 0x000185DC
		public void Connect(int timeout)
		{
			AutoResetEvent waitHandle = new AutoResetEvent(false);
			this.opener.BeginInvoke(delegate(IAsyncResult result)
			{
				this.opener.EndInvoke(result);
				waitHandle.Set();
			}, null);
			if (!waitHandle.WaitOne(TimeSpan.FromMilliseconds((double)timeout)))
			{
				throw new TimeoutException();
			}
		}

		// Token: 0x170000A5 RID: 165
		// (get) Token: 0x060005F0 RID: 1520 RVA: 0x0001A438 File Offset: 0x00018638
		public bool IsAsync
		{
			get
			{
				return this.is_async;
			}
		}

		// Token: 0x170000A6 RID: 166
		// (get) Token: 0x060005F1 RID: 1521 RVA: 0x0001A440 File Offset: 0x00018640
		public int NumberOfServerInstances
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x04000195 RID: 405
		private NamedPipeClientStream owner;

		// Token: 0x04000196 RID: 406
		private bool is_async;

		// Token: 0x04000197 RID: 407
		private SafePipeHandle handle;

		// Token: 0x04000198 RID: 408
		private Action opener;
	}
}
