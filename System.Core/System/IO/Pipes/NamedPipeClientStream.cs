﻿using System;
using System.Security.Permissions;
using System.Security.Principal;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000069 RID: 105
	[MonoTODO("working only on win32 right now")]
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n</PermissionSet>\n")]
	public sealed class NamedPipeClientStream : PipeStream
	{
		// Token: 0x0600056C RID: 1388 RVA: 0x000197C4 File Offset: 0x000179C4
		public NamedPipeClientStream(string pipeName) : this(".", pipeName)
		{
		}

		// Token: 0x0600056D RID: 1389 RVA: 0x000197D4 File Offset: 0x000179D4
		public NamedPipeClientStream(string serverName, string pipeName) : this(serverName, pipeName, PipeDirection.InOut)
		{
		}

		// Token: 0x0600056E RID: 1390 RVA: 0x000197E0 File Offset: 0x000179E0
		public NamedPipeClientStream(string serverName, string pipeName, PipeDirection direction) : this(serverName, pipeName, direction, PipeOptions.None)
		{
		}

		// Token: 0x0600056F RID: 1391 RVA: 0x000197EC File Offset: 0x000179EC
		public NamedPipeClientStream(string serverName, string pipeName, PipeDirection direction, PipeOptions options) : this(serverName, pipeName, direction, options, TokenImpersonationLevel.None)
		{
		}

		// Token: 0x06000570 RID: 1392 RVA: 0x000197FC File Offset: 0x000179FC
		public NamedPipeClientStream(string serverName, string pipeName, PipeDirection direction, PipeOptions options, TokenImpersonationLevel impersonationLevel) : this(serverName, pipeName, direction, options, impersonationLevel, HandleInheritability.None)
		{
		}

		// Token: 0x06000571 RID: 1393 RVA: 0x0001980C File Offset: 0x00017A0C
		public NamedPipeClientStream(string serverName, string pipeName, PipeDirection direction, PipeOptions options, TokenImpersonationLevel impersonationLevel, HandleInheritability inheritability) : this(serverName, pipeName, PipeStream.ToAccessRights(direction), options, impersonationLevel, inheritability)
		{
		}

		// Token: 0x06000572 RID: 1394 RVA: 0x00019830 File Offset: 0x00017A30
		public NamedPipeClientStream(PipeDirection direction, bool isAsync, bool isConnected, SafePipeHandle safePipeHandle) : base(direction, 1024)
		{
			if (PipeStream.IsWindows)
			{
				this.impl = new Win32NamedPipeClient(this, safePipeHandle);
			}
			else
			{
				this.impl = new UnixNamedPipeClient(this, safePipeHandle);
			}
			base.IsConnected = isConnected;
			base.InitializeHandle(safePipeHandle, true, isAsync);
		}

		// Token: 0x06000573 RID: 1395 RVA: 0x00019888 File Offset: 0x00017A88
		public NamedPipeClientStream(string serverName, string pipeName, PipeAccessRights desiredAccessRights, PipeOptions options, TokenImpersonationLevel impersonationLevel, HandleInheritability inheritability) : base(PipeStream.ToDirection(desiredAccessRights), 1024)
		{
			if (impersonationLevel != TokenImpersonationLevel.None || inheritability != HandleInheritability.None)
			{
				throw base.ThrowACLException();
			}
			if (PipeStream.IsWindows)
			{
				this.impl = new Win32NamedPipeClient(this, serverName, pipeName, desiredAccessRights, options, inheritability);
			}
			else
			{
				this.impl = new UnixNamedPipeClient(this, serverName, pipeName, desiredAccessRights, options, inheritability);
			}
		}

		// Token: 0x06000574 RID: 1396 RVA: 0x000198F0 File Offset: 0x00017AF0
		public void Connect()
		{
			this.impl.Connect();
			base.InitializeHandle(this.impl.Handle, false, this.impl.IsAsync);
			base.IsConnected = true;
		}

		// Token: 0x06000575 RID: 1397 RVA: 0x0001992C File Offset: 0x00017B2C
		public void Connect(int timeout)
		{
			this.impl.Connect(timeout);
			base.InitializeHandle(this.impl.Handle, false, this.impl.IsAsync);
			base.IsConnected = true;
		}

		// Token: 0x17000085 RID: 133
		// (get) Token: 0x06000576 RID: 1398 RVA: 0x0001996C File Offset: 0x00017B6C
		public int NumberOfServerInstances
		{
			get
			{
				this.CheckPipePropertyOperations();
				return this.impl.NumberOfServerInstances;
			}
		}

		// Token: 0x04000162 RID: 354
		private INamedPipeClient impl;
	}
}
