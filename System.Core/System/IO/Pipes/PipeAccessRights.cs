﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x0200006B RID: 107
	[Flags]
	public enum PipeAccessRights
	{
		// Token: 0x04000167 RID: 359
		ReadData = 1,
		// Token: 0x04000168 RID: 360
		WriteData = 2,
		// Token: 0x04000169 RID: 361
		ReadAttributes = 4,
		// Token: 0x0400016A RID: 362
		WriteAttributes = 8,
		// Token: 0x0400016B RID: 363
		ReadExtendedAttributes = 16,
		// Token: 0x0400016C RID: 364
		WriteExtendedAttributes = 32,
		// Token: 0x0400016D RID: 365
		CreateNewInstance = 64,
		// Token: 0x0400016E RID: 366
		Delete = 128,
		// Token: 0x0400016F RID: 367
		ReadPermissions = 256,
		// Token: 0x04000170 RID: 368
		ChangePermissions = 512,
		// Token: 0x04000171 RID: 369
		TakeOwnership = 1024,
		// Token: 0x04000172 RID: 370
		Synchronize = 2048,
		// Token: 0x04000173 RID: 371
		FullControl = 1855,
		// Token: 0x04000174 RID: 372
		Read = 277,
		// Token: 0x04000175 RID: 373
		Write = 554,
		// Token: 0x04000176 RID: 374
		ReadWrite = 831,
		// Token: 0x04000177 RID: 375
		AccessSystemSecurity = 1792
	}
}
