﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x02000077 RID: 119
	[Serializable]
	public enum PipeTransmissionMode
	{
		// Token: 0x04000190 RID: 400
		Byte,
		// Token: 0x04000191 RID: 401
		Message
	}
}
