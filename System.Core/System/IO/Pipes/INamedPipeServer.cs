﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x02000073 RID: 115
	internal interface INamedPipeServer : IPipe
	{
		// Token: 0x06000597 RID: 1431
		void Disconnect();

		// Token: 0x06000598 RID: 1432
		void WaitForConnection();
	}
}
