﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x0200006F RID: 111
	internal interface IPipe
	{
		// Token: 0x17000088 RID: 136
		// (get) Token: 0x0600058F RID: 1423
		SafePipeHandle Handle { get; }

		// Token: 0x06000590 RID: 1424
		void WaitForPipeDrain();
	}
}
