﻿using System;
using System.ComponentModel;
using System.Runtime.InteropServices;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x02000080 RID: 128
	internal class Win32AnonymousPipeServer : Win32AnonymousPipe, IPipe, IAnonymousPipeServer
	{
		// Token: 0x060005FC RID: 1532 RVA: 0x0001A51C File Offset: 0x0001871C
		public Win32AnonymousPipeServer(AnonymousPipeServerStream owner, PipeDirection direction, HandleInheritability inheritability, int bufferSize)
		{
			SecurityAttributesHack securityAttributesHack = new SecurityAttributesHack(inheritability == HandleInheritability.Inheritable);
			IntPtr preexistingHandle;
			IntPtr preexistingHandle2;
			if (!Win32Marshal.CreatePipe(out preexistingHandle, out preexistingHandle2, ref securityAttributesHack, bufferSize))
			{
				throw new Win32Exception(Marshal.GetLastWin32Error());
			}
			SafePipeHandle safePipeHandle = new SafePipeHandle(preexistingHandle, true);
			SafePipeHandle safePipeHandle2 = new SafePipeHandle(preexistingHandle2, true);
			if (direction == PipeDirection.Out)
			{
				this.server_handle = safePipeHandle2;
				this.client_handle = safePipeHandle;
			}
			else
			{
				this.server_handle = safePipeHandle;
				this.client_handle = safePipeHandle2;
			}
		}

		// Token: 0x060005FD RID: 1533 RVA: 0x0001A594 File Offset: 0x00018794
		public Win32AnonymousPipeServer(AnonymousPipeServerStream owner, SafePipeHandle serverHandle, SafePipeHandle clientHandle)
		{
			this.server_handle = serverHandle;
			this.client_handle = clientHandle;
		}

		// Token: 0x170000AA RID: 170
		// (get) Token: 0x060005FE RID: 1534 RVA: 0x0001A5AC File Offset: 0x000187AC
		public override SafePipeHandle Handle
		{
			get
			{
				return this.server_handle;
			}
		}

		// Token: 0x170000AB RID: 171
		// (get) Token: 0x060005FF RID: 1535 RVA: 0x0001A5B4 File Offset: 0x000187B4
		public SafePipeHandle ClientHandle
		{
			get
			{
				return this.client_handle;
			}
		}

		// Token: 0x06000600 RID: 1536 RVA: 0x0001A5BC File Offset: 0x000187BC
		public void DisposeLocalCopyOfClientHandle()
		{
			throw new NotImplementedException();
		}

		// Token: 0x0400019C RID: 412
		private SafePipeHandle server_handle;

		// Token: 0x0400019D RID: 413
		private SafePipeHandle client_handle;
	}
}
