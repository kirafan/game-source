﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x0200007E RID: 126
	internal abstract class Win32AnonymousPipe : IPipe
	{
		// Token: 0x170000A8 RID: 168
		// (get) Token: 0x060005F8 RID: 1528
		public abstract SafePipeHandle Handle { get; }

		// Token: 0x060005F9 RID: 1529 RVA: 0x0001A4FC File Offset: 0x000186FC
		public void WaitForPipeDrain()
		{
			throw new NotImplementedException();
		}
	}
}
