﻿using System;
using Microsoft.Win32.SafeHandles;

namespace System.IO.Pipes
{
	// Token: 0x0200007A RID: 122
	internal class UnixAnonymousPipeServer : UnixAnonymousPipe, IPipe, IAnonymousPipeServer
	{
		// Token: 0x060005DF RID: 1503 RVA: 0x0001A168 File Offset: 0x00018368
		public UnixAnonymousPipeServer(AnonymousPipeServerStream owner, PipeDirection direction, HandleInheritability inheritability, int bufferSize)
		{
			throw new NotImplementedException();
		}

		// Token: 0x060005E0 RID: 1504 RVA: 0x0001A178 File Offset: 0x00018378
		public UnixAnonymousPipeServer(AnonymousPipeServerStream owner, SafePipeHandle serverHandle, SafePipeHandle clientHandle)
		{
			this.server_handle = serverHandle;
			this.client_handle = clientHandle;
			throw new NotImplementedException();
		}

		// Token: 0x170000A1 RID: 161
		// (get) Token: 0x060005E1 RID: 1505 RVA: 0x0001A194 File Offset: 0x00018394
		public override SafePipeHandle Handle
		{
			get
			{
				return this.server_handle;
			}
		}

		// Token: 0x170000A2 RID: 162
		// (get) Token: 0x060005E2 RID: 1506 RVA: 0x0001A19C File Offset: 0x0001839C
		public SafePipeHandle ClientHandle
		{
			get
			{
				return this.client_handle;
			}
		}

		// Token: 0x060005E3 RID: 1507 RVA: 0x0001A1A4 File Offset: 0x000183A4
		public void DisposeLocalCopyOfClientHandle()
		{
			throw new NotImplementedException();
		}

		// Token: 0x04000193 RID: 403
		private SafePipeHandle server_handle;

		// Token: 0x04000194 RID: 404
		private SafePipeHandle client_handle;
	}
}
