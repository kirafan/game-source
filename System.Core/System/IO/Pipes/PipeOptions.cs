﻿using System;

namespace System.IO.Pipes
{
	// Token: 0x02000074 RID: 116
	[Flags]
	[Serializable]
	public enum PipeOptions
	{
		// Token: 0x0400017F RID: 383
		None = 0,
		// Token: 0x04000180 RID: 384
		WriteThrough = 1,
		// Token: 0x04000181 RID: 385
		Asynchronous = 2
	}
}
