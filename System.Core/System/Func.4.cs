﻿using System;

namespace System
{
	// Token: 0x0200008E RID: 142
	// (Invoke) Token: 0x06000637 RID: 1591
	public delegate TResult Func<T1, T2, T3, TResult>(T1 arg1, T2 arg2, T3 arg3);
}
