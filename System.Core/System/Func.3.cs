﻿using System;

namespace System
{
	// Token: 0x0200008D RID: 141
	// (Invoke) Token: 0x06000633 RID: 1587
	public delegate TResult Func<T1, T2, TResult>(T1 arg1, T2 arg2);
}
