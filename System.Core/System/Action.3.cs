﻿using System;

namespace System
{
	// Token: 0x02000089 RID: 137
	// (Invoke) Token: 0x06000623 RID: 1571
	public delegate void Action<T1, T2, T3>(T1 arg1, T2 arg2, T3 arg3);
}
