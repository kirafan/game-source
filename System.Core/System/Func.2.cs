﻿using System;

namespace System
{
	// Token: 0x0200008C RID: 140
	// (Invoke) Token: 0x0600062F RID: 1583
	public delegate TResult Func<T, TResult>(T arg1);
}
