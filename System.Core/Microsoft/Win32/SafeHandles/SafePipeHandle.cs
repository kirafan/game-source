﻿using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;

namespace Microsoft.Win32.SafeHandles
{
	// Token: 0x02000066 RID: 102
	[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.HostProtectionPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nResources=\"None\"/>\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
	public sealed class SafePipeHandle : SafeHandleZeroOrMinusOneIsInvalid
	{
		// Token: 0x06000558 RID: 1368 RVA: 0x0001953C File Offset: 0x0001773C
		public SafePipeHandle(IntPtr preexistingHandle, bool ownsHandle) : base(ownsHandle)
		{
			this.handle = preexistingHandle;
		}

		// Token: 0x06000559 RID: 1369 RVA: 0x0001954C File Offset: 0x0001774C
		protected override bool ReleaseHandle()
		{
			bool result;
			try
			{
				Marshal.FreeHGlobal(this.handle);
				result = true;
			}
			catch (ArgumentException)
			{
				result = false;
			}
			return result;
		}
	}
}
