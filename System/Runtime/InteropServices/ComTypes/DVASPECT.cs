﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004CE RID: 1230
	[Flags]
	public enum DVASPECT
	{
		// Token: 0x04001BB2 RID: 7090
		DVASPECT_CONTENT = 1,
		// Token: 0x04001BB3 RID: 7091
		DVASPECT_THUMBNAIL = 2,
		// Token: 0x04001BB4 RID: 7092
		DVASPECT_ICON = 4,
		// Token: 0x04001BB5 RID: 7093
		DVASPECT_DOCPRINT = 8
	}
}
