﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004D2 RID: 1234
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00000103-0000-0000-C000-000000000046")]
	[ComImport]
	public interface IEnumFORMATETC
	{
		// Token: 0x06002BF0 RID: 11248
		void Clone(out IEnumFORMATETC newEnum);

		// Token: 0x06002BF1 RID: 11249
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray)] [Out] FORMATETC[] rgelt, [MarshalAs(UnmanagedType.LPArray)] [Out] int[] pceltFetched);

		// Token: 0x06002BF2 RID: 11250
		[PreserveSig]
		int Reset();

		// Token: 0x06002BF3 RID: 11251
		[PreserveSig]
		int Skip(int celt);
	}
}
