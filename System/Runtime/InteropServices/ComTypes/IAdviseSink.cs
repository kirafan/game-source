﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004D0 RID: 1232
	[Guid("0000010F-0000-0000-C000-000000000046")]
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[ComImport]
	public interface IAdviseSink
	{
		// Token: 0x06002BE2 RID: 11234
		[PreserveSig]
		void OnClose();

		// Token: 0x06002BE3 RID: 11235
		[PreserveSig]
		void OnDataChange([In] ref FORMATETC format, [In] ref STGMEDIUM stgmedium);

		// Token: 0x06002BE4 RID: 11236
		[PreserveSig]
		void OnRename(IMoniker moniker);

		// Token: 0x06002BE5 RID: 11237
		[PreserveSig]
		void OnSave();

		// Token: 0x06002BE6 RID: 11238
		[PreserveSig]
		void OnViewChange(int aspect, int index);
	}
}
