﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004D3 RID: 1235
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("00000103-0000-0000-C000-000000000046")]
	[ComImport]
	public interface IEnumSTATDATA
	{
		// Token: 0x06002BF4 RID: 11252
		void Clone(out IEnumSTATDATA newEnum);

		// Token: 0x06002BF5 RID: 11253
		[PreserveSig]
		int Next(int celt, [MarshalAs(UnmanagedType.LPArray)] [Out] STATDATA[] rgelt, [MarshalAs(UnmanagedType.LPArray)] [Out] int[] pceltFetched);

		// Token: 0x06002BF6 RID: 11254
		[PreserveSig]
		int Reset();

		// Token: 0x06002BF7 RID: 11255
		[PreserveSig]
		int Skip(int celt);
	}
}
