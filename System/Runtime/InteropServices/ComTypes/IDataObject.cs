﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004D1 RID: 1233
	[InterfaceType(ComInterfaceType.InterfaceIsIUnknown)]
	[Guid("0000010e-0000-0000-C000-000000000046")]
	[ComImport]
	public interface IDataObject
	{
		// Token: 0x06002BE7 RID: 11239
		[PreserveSig]
		int DAdvise([In] ref FORMATETC pFormatetc, ADVF advf, IAdviseSink adviseSink, out int connection);

		// Token: 0x06002BE8 RID: 11240
		void DUnadvise(int connection);

		// Token: 0x06002BE9 RID: 11241
		[PreserveSig]
		int EnumDAdvise(out IEnumSTATDATA enumAdvise);

		// Token: 0x06002BEA RID: 11242
		IEnumFORMATETC EnumFormatEtc(DATADIR direction);

		// Token: 0x06002BEB RID: 11243
		[PreserveSig]
		int GetCanonicalFormatEtc([In] ref FORMATETC formatIn, out FORMATETC formatOut);

		// Token: 0x06002BEC RID: 11244
		void GetData([In] ref FORMATETC format, out STGMEDIUM medium);

		// Token: 0x06002BED RID: 11245
		void GetDataHere([In] ref FORMATETC format, ref STGMEDIUM medium);

		// Token: 0x06002BEE RID: 11246
		[PreserveSig]
		int QueryGetData([In] ref FORMATETC format);

		// Token: 0x06002BEF RID: 11247
		void SetData([In] ref FORMATETC formatIn, [In] ref STGMEDIUM medium, [MarshalAs(UnmanagedType.Bool)] bool release);
	}
}
