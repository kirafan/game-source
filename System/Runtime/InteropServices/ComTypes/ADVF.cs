﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004CC RID: 1228
	[Flags]
	public enum ADVF
	{
		// Token: 0x04001BA7 RID: 7079
		ADVF_NODATA = 1,
		// Token: 0x04001BA8 RID: 7080
		ADVF_PRIMEFIRST = 2,
		// Token: 0x04001BA9 RID: 7081
		ADVF_ONLYONCE = 4,
		// Token: 0x04001BAA RID: 7082
		ADVFCACHE_NOHANDLER = 8,
		// Token: 0x04001BAB RID: 7083
		ADVFCACHE_FORCEBUILTIN = 16,
		// Token: 0x04001BAC RID: 7084
		ADVFCACHE_ONSAVE = 32,
		// Token: 0x04001BAD RID: 7085
		ADVF_DATAONSTOP = 64
	}
}
