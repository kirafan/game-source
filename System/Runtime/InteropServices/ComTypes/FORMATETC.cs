﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004CF RID: 1231
	public struct FORMATETC
	{
		// Token: 0x04001BB6 RID: 7094
		[MarshalAs(UnmanagedType.U2)]
		public short cfFormat;

		// Token: 0x04001BB7 RID: 7095
		[MarshalAs(UnmanagedType.U4)]
		public DVASPECT dwAspect;

		// Token: 0x04001BB8 RID: 7096
		public int lindex;

		// Token: 0x04001BB9 RID: 7097
		public IntPtr ptd;

		// Token: 0x04001BBA RID: 7098
		[MarshalAs(UnmanagedType.U4)]
		public TYMED tymed;
	}
}
