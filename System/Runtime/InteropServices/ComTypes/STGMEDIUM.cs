﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004D5 RID: 1237
	public struct STGMEDIUM
	{
		// Token: 0x04001BBF RID: 7103
		[MarshalAs(UnmanagedType.IUnknown)]
		public object pUnkForRelease;

		// Token: 0x04001BC0 RID: 7104
		public TYMED tymed;

		// Token: 0x04001BC1 RID: 7105
		public IntPtr unionmember;
	}
}
