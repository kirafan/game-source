﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004D6 RID: 1238
	[Flags]
	public enum TYMED
	{
		// Token: 0x04001BC3 RID: 7107
		TYMED_HGLOBAL = 1,
		// Token: 0x04001BC4 RID: 7108
		TYMED_FILE = 2,
		// Token: 0x04001BC5 RID: 7109
		TYMED_ISTREAM = 4,
		// Token: 0x04001BC6 RID: 7110
		TYMED_ISTORAGE = 8,
		// Token: 0x04001BC7 RID: 7111
		TYMED_GDI = 16,
		// Token: 0x04001BC8 RID: 7112
		TYMED_MFPICT = 32,
		// Token: 0x04001BC9 RID: 7113
		TYMED_ENHMF = 64,
		// Token: 0x04001BCA RID: 7114
		TYMED_NULL = 0
	}
}
