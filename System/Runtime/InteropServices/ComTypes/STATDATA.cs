﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004D4 RID: 1236
	public struct STATDATA
	{
		// Token: 0x04001BBB RID: 7099
		public ADVF advf;

		// Token: 0x04001BBC RID: 7100
		public IAdviseSink advSink;

		// Token: 0x04001BBD RID: 7101
		public int connection;

		// Token: 0x04001BBE RID: 7102
		public FORMATETC formatetc;
	}
}
