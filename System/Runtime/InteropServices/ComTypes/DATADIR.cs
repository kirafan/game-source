﻿using System;

namespace System.Runtime.InteropServices.ComTypes
{
	// Token: 0x020004CD RID: 1229
	public enum DATADIR
	{
		// Token: 0x04001BAF RID: 7087
		DATADIR_GET = 1,
		// Token: 0x04001BB0 RID: 7088
		DATADIR_SET
	}
}
