﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x02000002 RID: 2
	[AttributeUsage(AttributeTargets.Parameter)]
	public sealed class DefaultParameterValueAttribute : Attribute
	{
		// Token: 0x06000001 RID: 1 RVA: 0x000020EC File Offset: 0x000002EC
		public DefaultParameterValueAttribute(object value)
		{
			this.value = value;
		}

		// Token: 0x17000001 RID: 1
		// (get) Token: 0x06000002 RID: 2 RVA: 0x000020FC File Offset: 0x000002FC
		public object Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x04000001 RID: 1
		private object value;
	}
}
