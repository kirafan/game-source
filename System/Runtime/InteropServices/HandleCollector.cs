﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020004CA RID: 1226
	public sealed class HandleCollector
	{
		// Token: 0x06002BD9 RID: 11225 RVA: 0x00098DFC File Offset: 0x00096FFC
		public HandleCollector(string name, int initialThreshold) : this(name, initialThreshold, int.MaxValue)
		{
		}

		// Token: 0x06002BDA RID: 11226 RVA: 0x00098E0C File Offset: 0x0009700C
		public HandleCollector(string name, int initialThreshold, int maximumThreshold)
		{
			if (initialThreshold < 0)
			{
				throw new ArgumentOutOfRangeException("initialThreshold", "initialThreshold must not be less than zero");
			}
			if (maximumThreshold < 0)
			{
				throw new ArgumentOutOfRangeException("maximumThreshold", "maximumThreshold must not be less than zero");
			}
			if (maximumThreshold < initialThreshold)
			{
				throw new ArgumentException("maximumThreshold must not be less than initialThreshold");
			}
			this.name = name;
			this.init = initialThreshold;
			this.max = maximumThreshold;
		}

		// Token: 0x17000C04 RID: 3076
		// (get) Token: 0x06002BDB RID: 11227 RVA: 0x00098E80 File Offset: 0x00097080
		public int Count
		{
			get
			{
				return this.count;
			}
		}

		// Token: 0x17000C05 RID: 3077
		// (get) Token: 0x06002BDC RID: 11228 RVA: 0x00098E88 File Offset: 0x00097088
		public int InitialThreshold
		{
			get
			{
				return this.init;
			}
		}

		// Token: 0x17000C06 RID: 3078
		// (get) Token: 0x06002BDD RID: 11229 RVA: 0x00098E90 File Offset: 0x00097090
		public int MaximumThreshold
		{
			get
			{
				return this.max;
			}
		}

		// Token: 0x17000C07 RID: 3079
		// (get) Token: 0x06002BDE RID: 11230 RVA: 0x00098E98 File Offset: 0x00097098
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x06002BDF RID: 11231 RVA: 0x00098EA0 File Offset: 0x000970A0
		public void Add()
		{
			if (++this.count >= this.max)
			{
				GC.Collect(GC.MaxGeneration);
			}
			else if (this.count >= this.init && DateTime.Now - this.previous_collection > TimeSpan.FromSeconds(5.0))
			{
				GC.Collect(GC.MaxGeneration);
				this.previous_collection = DateTime.Now;
			}
		}

		// Token: 0x06002BE0 RID: 11232 RVA: 0x00098F28 File Offset: 0x00097128
		public void Remove()
		{
			if (this.count == 0)
			{
				throw new InvalidOperationException("Cannot call Remove method when Count is 0");
			}
			this.count--;
		}

		// Token: 0x04001BA1 RID: 7073
		private int count;

		// Token: 0x04001BA2 RID: 7074
		private readonly int init;

		// Token: 0x04001BA3 RID: 7075
		private readonly int max;

		// Token: 0x04001BA4 RID: 7076
		private readonly string name;

		// Token: 0x04001BA5 RID: 7077
		private DateTime previous_collection = DateTime.MinValue;
	}
}
