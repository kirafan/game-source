﻿using System;

namespace System.Runtime.InteropServices
{
	// Token: 0x020004CB RID: 1227
	[MonoLimitation("The runtime does nothing special apart from what it already does with marshal-by-ref objects")]
	[ComVisible(true)]
	public class StandardOleMarshalObject : MarshalByRefObject
	{
		// Token: 0x06002BE1 RID: 11233 RVA: 0x00098F5C File Offset: 0x0009715C
		protected StandardOleMarshalObject()
		{
		}
	}
}
