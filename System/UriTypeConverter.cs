﻿using System;
using System.ComponentModel;
using System.ComponentModel.Design.Serialization;
using System.Globalization;
using System.Reflection;

namespace System
{
	// Token: 0x020004BB RID: 1211
	public class UriTypeConverter : System.ComponentModel.TypeConverter
	{
		// Token: 0x06002BA7 RID: 11175 RVA: 0x00098710 File Offset: 0x00096910
		private bool CanConvert(Type type)
		{
			return type == typeof(string) || type == typeof(System.Uri) || type == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor);
		}

		// Token: 0x06002BA8 RID: 11176 RVA: 0x00098744 File Offset: 0x00096944
		public override bool CanConvertFrom(System.ComponentModel.ITypeDescriptorContext context, Type sourceType)
		{
			if (sourceType == null)
			{
				throw new ArgumentNullException("sourceType");
			}
			return this.CanConvert(sourceType);
		}

		// Token: 0x06002BA9 RID: 11177 RVA: 0x00098760 File Offset: 0x00096960
		public override bool CanConvertTo(System.ComponentModel.ITypeDescriptorContext context, Type destinationType)
		{
			return destinationType != null && this.CanConvert(destinationType);
		}

		// Token: 0x06002BAA RID: 11178 RVA: 0x00098774 File Offset: 0x00096974
		public override object ConvertFrom(System.ComponentModel.ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (!this.CanConvertFrom(context, value.GetType()))
			{
				throw new NotSupportedException(Locale.GetText("Cannot convert from value."));
			}
			if (value is System.Uri)
			{
				return value;
			}
			string text = value as string;
			if (text != null)
			{
				return new System.Uri(text, System.UriKind.RelativeOrAbsolute);
			}
			System.ComponentModel.Design.Serialization.InstanceDescriptor instanceDescriptor = value as System.ComponentModel.Design.Serialization.InstanceDescriptor;
			if (instanceDescriptor != null)
			{
				return instanceDescriptor.Invoke();
			}
			return base.ConvertFrom(context, culture, value);
		}

		// Token: 0x06002BAB RID: 11179 RVA: 0x000987F4 File Offset: 0x000969F4
		public override object ConvertTo(System.ComponentModel.ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (!this.CanConvertTo(context, destinationType))
			{
				throw new NotSupportedException(Locale.GetText("Cannot convert to destination type."));
			}
			System.Uri uri = value as System.Uri;
			if (uri != null)
			{
				if (destinationType == typeof(string))
				{
					return uri.ToString();
				}
				if (destinationType == typeof(System.Uri))
				{
					return uri;
				}
				if (destinationType == typeof(System.ComponentModel.Design.Serialization.InstanceDescriptor))
				{
					ConstructorInfo constructor = typeof(System.Uri).GetConstructor(new Type[]
					{
						typeof(string),
						typeof(System.UriKind)
					});
					return new System.ComponentModel.Design.Serialization.InstanceDescriptor(constructor, new object[]
					{
						uri.ToString(),
						(!uri.IsAbsoluteUri) ? System.UriKind.Relative : System.UriKind.Absolute
					});
				}
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}

		// Token: 0x06002BAC RID: 11180 RVA: 0x000988DC File Offset: 0x00096ADC
		public override bool IsValid(System.ComponentModel.ITypeDescriptorContext context, object value)
		{
			return value != null && (value is string || value is System.Uri);
		}
	}
}
