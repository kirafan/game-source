﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Web
{
	// Token: 0x020004BD RID: 1213
	[Serializable]
	public sealed class AspNetHostingPermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x06002BB1 RID: 11185 RVA: 0x00098988 File Offset: 0x00096B88
		public AspNetHostingPermission(AspNetHostingPermissionLevel level)
		{
			this.Level = level;
		}

		// Token: 0x06002BB2 RID: 11186 RVA: 0x00098998 File Offset: 0x00096B98
		public AspNetHostingPermission(PermissionState state)
		{
			if (PermissionHelper.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this._level = AspNetHostingPermissionLevel.Unrestricted;
			}
			else
			{
				this._level = AspNetHostingPermissionLevel.None;
			}
		}

		// Token: 0x17000BF3 RID: 3059
		// (get) Token: 0x06002BB3 RID: 11187 RVA: 0x000989C8 File Offset: 0x00096BC8
		// (set) Token: 0x06002BB4 RID: 11188 RVA: 0x000989D0 File Offset: 0x00096BD0
		public AspNetHostingPermissionLevel Level
		{
			get
			{
				return this._level;
			}
			set
			{
				if (value < AspNetHostingPermissionLevel.None || value > AspNetHostingPermissionLevel.Unrestricted)
				{
					string text = Locale.GetText("Invalid enum {0}.");
					throw new ArgumentException(string.Format(text, value), "Level");
				}
				this._level = value;
			}
		}

		// Token: 0x06002BB5 RID: 11189 RVA: 0x00098A1C File Offset: 0x00096C1C
		public bool IsUnrestricted()
		{
			return this._level == AspNetHostingPermissionLevel.Unrestricted;
		}

		// Token: 0x06002BB6 RID: 11190 RVA: 0x00098A2C File Offset: 0x00096C2C
		public override IPermission Copy()
		{
			return new AspNetHostingPermission(this._level);
		}

		// Token: 0x06002BB7 RID: 11191 RVA: 0x00098A3C File Offset: 0x00096C3C
		public override void FromXml(SecurityElement securityElement)
		{
			PermissionHelper.CheckSecurityElement(securityElement, "securityElement", 1, 1);
			if (securityElement.Tag != "IPermission")
			{
				string text = Locale.GetText("Invalid tag '{0}' for permission.");
				throw new ArgumentException(string.Format(text, securityElement.Tag), "securityElement");
			}
			if (securityElement.Attribute("version") == null)
			{
				string text2 = Locale.GetText("Missing version attribute.");
				throw new ArgumentException(text2, "securityElement");
			}
			if (PermissionHelper.IsUnrestricted(securityElement))
			{
				this._level = AspNetHostingPermissionLevel.Unrestricted;
			}
			else
			{
				string text3 = securityElement.Attribute("Level");
				if (text3 != null)
				{
					this._level = (AspNetHostingPermissionLevel)((int)Enum.Parse(typeof(AspNetHostingPermissionLevel), text3));
				}
				else
				{
					this._level = AspNetHostingPermissionLevel.None;
				}
			}
		}

		// Token: 0x06002BB8 RID: 11192 RVA: 0x00098B0C File Offset: 0x00096D0C
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = PermissionHelper.Element(typeof(AspNetHostingPermission), 1);
			if (this.IsUnrestricted())
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			securityElement.AddAttribute("Level", this._level.ToString());
			return securityElement;
		}

		// Token: 0x06002BB9 RID: 11193 RVA: 0x00098B64 File Offset: 0x00096D64
		public override IPermission Intersect(IPermission target)
		{
			AspNetHostingPermission aspNetHostingPermission = this.Cast(target);
			if (aspNetHostingPermission == null)
			{
				return null;
			}
			return new AspNetHostingPermission((this._level > aspNetHostingPermission.Level) ? aspNetHostingPermission.Level : this._level);
		}

		// Token: 0x06002BBA RID: 11194 RVA: 0x00098BA8 File Offset: 0x00096DA8
		public override bool IsSubsetOf(IPermission target)
		{
			AspNetHostingPermission aspNetHostingPermission = this.Cast(target);
			if (aspNetHostingPermission == null)
			{
				return this.IsEmpty();
			}
			return this._level <= aspNetHostingPermission._level;
		}

		// Token: 0x06002BBB RID: 11195 RVA: 0x00098BDC File Offset: 0x00096DDC
		public override IPermission Union(IPermission target)
		{
			AspNetHostingPermission aspNetHostingPermission = this.Cast(target);
			if (aspNetHostingPermission == null)
			{
				return this.Copy();
			}
			return new AspNetHostingPermission((this._level <= aspNetHostingPermission.Level) ? aspNetHostingPermission.Level : this._level);
		}

		// Token: 0x06002BBC RID: 11196 RVA: 0x00098C28 File Offset: 0x00096E28
		private bool IsEmpty()
		{
			return this._level == AspNetHostingPermissionLevel.None;
		}

		// Token: 0x06002BBD RID: 11197 RVA: 0x00098C34 File Offset: 0x00096E34
		private AspNetHostingPermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			AspNetHostingPermission aspNetHostingPermission = target as AspNetHostingPermission;
			if (aspNetHostingPermission == null)
			{
				PermissionHelper.ThrowInvalidPermission(target, typeof(AspNetHostingPermission));
			}
			return aspNetHostingPermission;
		}

		// Token: 0x04001B88 RID: 7048
		private const int version = 1;

		// Token: 0x04001B89 RID: 7049
		private AspNetHostingPermissionLevel _level;
	}
}
