﻿using System;

namespace System.Web
{
	// Token: 0x020004BE RID: 1214
	[Serializable]
	public enum AspNetHostingPermissionLevel
	{
		// Token: 0x04001B8B RID: 7051
		None = 100,
		// Token: 0x04001B8C RID: 7052
		Minimal = 200,
		// Token: 0x04001B8D RID: 7053
		Low = 300,
		// Token: 0x04001B8E RID: 7054
		Medium = 400,
		// Token: 0x04001B8F RID: 7055
		High = 500,
		// Token: 0x04001B90 RID: 7056
		Unrestricted = 600
	}
}
