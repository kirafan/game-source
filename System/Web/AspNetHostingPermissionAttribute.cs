﻿using System;
using System.Security;
using System.Security.Permissions;

namespace System.Web
{
	// Token: 0x020004BC RID: 1212
	[AttributeUsage(AttributeTargets.All, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class AspNetHostingPermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06002BAD RID: 11181 RVA: 0x00098900 File Offset: 0x00096B00
		public AspNetHostingPermissionAttribute(SecurityAction action) : base(action)
		{
			this._level = AspNetHostingPermissionLevel.None;
		}

		// Token: 0x06002BAE RID: 11182 RVA: 0x00098914 File Offset: 0x00096B14
		public override IPermission CreatePermission()
		{
			if (base.Unrestricted)
			{
				return new AspNetHostingPermission(PermissionState.Unrestricted);
			}
			return new AspNetHostingPermission(this._level);
		}

		// Token: 0x17000BF2 RID: 3058
		// (get) Token: 0x06002BAF RID: 11183 RVA: 0x00098934 File Offset: 0x00096B34
		// (set) Token: 0x06002BB0 RID: 11184 RVA: 0x0009893C File Offset: 0x00096B3C
		public AspNetHostingPermissionLevel Level
		{
			get
			{
				return this._level;
			}
			set
			{
				if (value < AspNetHostingPermissionLevel.None || value > AspNetHostingPermissionLevel.Unrestricted)
				{
					string text = Locale.GetText("Invalid enum {0}.");
					throw new ArgumentException(string.Format(text, value), "Level");
				}
				this._level = value;
			}
		}

		// Token: 0x04001B87 RID: 7047
		private AspNetHostingPermissionLevel _level;
	}
}
