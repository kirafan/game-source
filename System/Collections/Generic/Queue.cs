﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections.Generic
{
	// Token: 0x02000099 RID: 153
	[ComVisible(false)]
	[Serializable]
	public class Queue<T> : IEnumerable<T>, ICollection, IEnumerable
	{
		// Token: 0x06000652 RID: 1618 RVA: 0x00013D50 File Offset: 0x00011F50
		public Queue()
		{
			this._array = new T[0];
		}

		// Token: 0x06000653 RID: 1619 RVA: 0x00013D64 File Offset: 0x00011F64
		public Queue(int count)
		{
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			this._array = new T[count];
		}

		// Token: 0x06000654 RID: 1620 RVA: 0x00013D98 File Offset: 0x00011F98
		public Queue(IEnumerable<T> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			ICollection<T> collection2 = collection as ICollection<T>;
			int num = (collection2 == null) ? 0 : collection2.Count;
			this._array = new T[num];
			foreach (T item in collection)
			{
				this.Enqueue(item);
			}
		}

		// Token: 0x06000655 RID: 1621 RVA: 0x00013E34 File Offset: 0x00012034
		void ICollection.CopyTo(Array array, int idx)
		{
			if (array == null)
			{
				throw new ArgumentNullException();
			}
			if (idx > array.Length)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (array.Length - idx < this._size)
			{
				throw new ArgumentOutOfRangeException();
			}
			if (this._size == 0)
			{
				return;
			}
			try
			{
				int num = this._array.Length;
				int num2 = num - this._head;
				Array.Copy(this._array, this._head, array, idx, Math.Min(this._size, num2));
				if (this._size > num2)
				{
					Array.Copy(this._array, 0, array, idx + num2, this._size - num2);
				}
			}
			catch (ArrayTypeMismatchException)
			{
				throw new ArgumentException();
			}
		}

		// Token: 0x17000138 RID: 312
		// (get) Token: 0x06000656 RID: 1622 RVA: 0x00013F08 File Offset: 0x00012108
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000139 RID: 313
		// (get) Token: 0x06000657 RID: 1623 RVA: 0x00013F0C File Offset: 0x0001210C
		object ICollection.SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06000658 RID: 1624 RVA: 0x00013F10 File Offset: 0x00012110
		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x06000659 RID: 1625 RVA: 0x00013F20 File Offset: 0x00012120
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x0600065A RID: 1626 RVA: 0x00013F30 File Offset: 0x00012130
		public void Clear()
		{
			Array.Clear(this._array, 0, this._array.Length);
			this._head = (this._tail = (this._size = 0));
			this._version++;
		}

		// Token: 0x0600065B RID: 1627 RVA: 0x00013F78 File Offset: 0x00012178
		public bool Contains(T item)
		{
			if (item == null)
			{
				foreach (T t in this)
				{
					if (t == null)
					{
						return true;
					}
				}
			}
			else
			{
				foreach (T t2 in this)
				{
					if (item.Equals(t2))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Token: 0x0600065C RID: 1628 RVA: 0x00014064 File Offset: 0x00012264
		public void CopyTo(T[] array, int idx)
		{
			if (array == null)
			{
				throw new ArgumentNullException();
			}
			((ICollection)this).CopyTo(array, idx);
		}

		// Token: 0x0600065D RID: 1629 RVA: 0x0001407C File Offset: 0x0001227C
		public T Dequeue()
		{
			T result = this.Peek();
			this._array[this._head] = default(T);
			if (++this._head == this._array.Length)
			{
				this._head = 0;
			}
			this._size--;
			this._version++;
			return result;
		}

		// Token: 0x0600065E RID: 1630 RVA: 0x000140EC File Offset: 0x000122EC
		public T Peek()
		{
			if (this._size == 0)
			{
				throw new InvalidOperationException();
			}
			return this._array[this._head];
		}

		// Token: 0x0600065F RID: 1631 RVA: 0x0001411C File Offset: 0x0001231C
		public void Enqueue(T item)
		{
			if (this._size == this._array.Length || this._tail == this._array.Length)
			{
				this.SetCapacity(Math.Max(Math.Max(this._size, this._tail) * 2, 4));
			}
			this._array[this._tail] = item;
			if (++this._tail == this._array.Length)
			{
				this._tail = 0;
			}
			this._size++;
			this._version++;
		}

		// Token: 0x06000660 RID: 1632 RVA: 0x000141C4 File Offset: 0x000123C4
		public T[] ToArray()
		{
			T[] array = new T[this._size];
			this.CopyTo(array, 0);
			return array;
		}

		// Token: 0x06000661 RID: 1633 RVA: 0x000141E8 File Offset: 0x000123E8
		public void TrimExcess()
		{
			if ((double)this._size < (double)this._array.Length * 0.9)
			{
				this.SetCapacity(this._size);
			}
		}

		// Token: 0x06000662 RID: 1634 RVA: 0x00014218 File Offset: 0x00012418
		private void SetCapacity(int new_size)
		{
			if (new_size == this._array.Length)
			{
				return;
			}
			if (new_size < this._size)
			{
				throw new InvalidOperationException("shouldnt happen");
			}
			T[] array = new T[new_size];
			if (this._size > 0)
			{
				this.CopyTo(array, 0);
			}
			this._array = array;
			this._tail = this._size;
			this._head = 0;
			this._version++;
		}

		// Token: 0x1700013A RID: 314
		// (get) Token: 0x06000663 RID: 1635 RVA: 0x00014290 File Offset: 0x00012490
		public int Count
		{
			get
			{
				return this._size;
			}
		}

		// Token: 0x06000664 RID: 1636 RVA: 0x00014298 File Offset: 0x00012498
		public Queue<T>.Enumerator GetEnumerator()
		{
			return new Queue<T>.Enumerator(this);
		}

		// Token: 0x040001C0 RID: 448
		private T[] _array;

		// Token: 0x040001C1 RID: 449
		private int _head;

		// Token: 0x040001C2 RID: 450
		private int _tail;

		// Token: 0x040001C3 RID: 451
		private int _size;

		// Token: 0x040001C4 RID: 452
		private int _version;

		// Token: 0x0200009A RID: 154
		[Serializable]
		public struct Enumerator : IEnumerator, IDisposable, IEnumerator<T>
		{
			// Token: 0x06000665 RID: 1637 RVA: 0x000142A0 File Offset: 0x000124A0
			internal Enumerator(Queue<T> q)
			{
				this.q = q;
				this.idx = -2;
				this.ver = q._version;
			}

			// Token: 0x06000666 RID: 1638 RVA: 0x000142C0 File Offset: 0x000124C0
			void IEnumerator.Reset()
			{
				if (this.ver != this.q._version)
				{
					throw new InvalidOperationException();
				}
				this.idx = -2;
			}

			// Token: 0x1700013B RID: 315
			// (get) Token: 0x06000667 RID: 1639 RVA: 0x000142F4 File Offset: 0x000124F4
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x06000668 RID: 1640 RVA: 0x00014304 File Offset: 0x00012504
			public void Dispose()
			{
				this.idx = -2;
			}

			// Token: 0x06000669 RID: 1641 RVA: 0x00014310 File Offset: 0x00012510
			public bool MoveNext()
			{
				if (this.ver != this.q._version)
				{
					throw new InvalidOperationException();
				}
				if (this.idx == -2)
				{
					this.idx = this.q._size;
				}
				return this.idx != -1 && --this.idx != -1;
			}

			// Token: 0x1700013C RID: 316
			// (get) Token: 0x0600066A RID: 1642 RVA: 0x00014380 File Offset: 0x00012580
			public T Current
			{
				get
				{
					if (this.idx < 0)
					{
						throw new InvalidOperationException();
					}
					return this.q._array[(this.q._size - 1 - this.idx + this.q._head) % this.q._array.Length];
				}
			}

			// Token: 0x040001C5 RID: 453
			private const int NOT_STARTED = -2;

			// Token: 0x040001C6 RID: 454
			private const int FINISHED = -1;

			// Token: 0x040001C7 RID: 455
			private Queue<T> q;

			// Token: 0x040001C8 RID: 456
			private int idx;

			// Token: 0x040001C9 RID: 457
			private int ver;
		}
	}
}
