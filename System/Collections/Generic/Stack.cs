﻿using System;
using System.Runtime.InteropServices;

namespace System.Collections.Generic
{
	// Token: 0x020000AA RID: 170
	[ComVisible(false)]
	[Serializable]
	public class Stack<T> : ICollection, IEnumerable<T>, IEnumerable
	{
		// Token: 0x06000744 RID: 1860 RVA: 0x000166EC File Offset: 0x000148EC
		public Stack()
		{
		}

		// Token: 0x06000745 RID: 1861 RVA: 0x000166F4 File Offset: 0x000148F4
		public Stack(int count)
		{
			if (count < 0)
			{
				throw new ArgumentOutOfRangeException("count");
			}
			this._array = new T[count];
		}

		// Token: 0x06000746 RID: 1862 RVA: 0x00016728 File Offset: 0x00014928
		public Stack(IEnumerable<T> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			ICollection<T> collection2 = collection as ICollection<T>;
			if (collection2 != null)
			{
				this._size = collection2.Count;
				this._array = new T[this._size];
				collection2.CopyTo(this._array, 0);
			}
			else
			{
				foreach (T t in collection)
				{
					this.Push(t);
				}
			}
		}

		// Token: 0x17000180 RID: 384
		// (get) Token: 0x06000747 RID: 1863 RVA: 0x000167DC File Offset: 0x000149DC
		bool ICollection.IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x17000181 RID: 385
		// (get) Token: 0x06000748 RID: 1864 RVA: 0x000167E0 File Offset: 0x000149E0
		object ICollection.SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x06000749 RID: 1865 RVA: 0x000167E4 File Offset: 0x000149E4
		void ICollection.CopyTo(Array dest, int idx)
		{
			try
			{
				if (this._array != null)
				{
					this._array.CopyTo(dest, idx);
					Array.Reverse(dest, idx, this._size);
				}
			}
			catch (ArrayTypeMismatchException)
			{
				throw new ArgumentException();
			}
		}

		// Token: 0x0600074A RID: 1866 RVA: 0x00016844 File Offset: 0x00014A44
		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x0600074B RID: 1867 RVA: 0x00016854 File Offset: 0x00014A54
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x0600074C RID: 1868 RVA: 0x00016864 File Offset: 0x00014A64
		public void Clear()
		{
			if (this._array != null)
			{
				Array.Clear(this._array, 0, this._array.Length);
			}
			this._size = 0;
			this._version++;
		}

		// Token: 0x0600074D RID: 1869 RVA: 0x000168A8 File Offset: 0x00014AA8
		public bool Contains(T t)
		{
			return this._array != null && Array.IndexOf<T>(this._array, t, 0, this._size) != -1;
		}

		// Token: 0x0600074E RID: 1870 RVA: 0x000168D4 File Offset: 0x00014AD4
		public void CopyTo(T[] dest, int idx)
		{
			if (dest == null)
			{
				throw new ArgumentNullException("dest");
			}
			if (idx < 0)
			{
				throw new ArgumentOutOfRangeException("idx");
			}
			if (this._array != null)
			{
				Array.Copy(this._array, 0, dest, idx, this._size);
				Array.Reverse(dest, idx, this._size);
			}
		}

		// Token: 0x0600074F RID: 1871 RVA: 0x00016930 File Offset: 0x00014B30
		public T Peek()
		{
			if (this._size == 0)
			{
				throw new InvalidOperationException();
			}
			return this._array[this._size - 1];
		}

		// Token: 0x06000750 RID: 1872 RVA: 0x00016964 File Offset: 0x00014B64
		public T Pop()
		{
			if (this._size == 0)
			{
				throw new InvalidOperationException();
			}
			this._version++;
			T result = this._array[--this._size];
			this._array[this._size] = default(T);
			return result;
		}

		// Token: 0x06000751 RID: 1873 RVA: 0x000169C8 File Offset: 0x00014BC8
		public void Push(T t)
		{
			if (this._array == null || this._size == this._array.Length)
			{
				Array.Resize<T>(ref this._array, (this._size != 0) ? (2 * this._size) : 16);
			}
			this._version++;
			this._array[this._size++] = t;
		}

		// Token: 0x06000752 RID: 1874 RVA: 0x00016A44 File Offset: 0x00014C44
		public T[] ToArray()
		{
			T[] array = new T[this._size];
			this.CopyTo(array, 0);
			return array;
		}

		// Token: 0x06000753 RID: 1875 RVA: 0x00016A68 File Offset: 0x00014C68
		public void TrimExcess()
		{
			if (this._array != null && (double)this._size < (double)this._array.Length * 0.9)
			{
				Array.Resize<T>(ref this._array, this._size);
			}
			this._version++;
		}

		// Token: 0x17000182 RID: 386
		// (get) Token: 0x06000754 RID: 1876 RVA: 0x00016AC0 File Offset: 0x00014CC0
		public int Count
		{
			get
			{
				return this._size;
			}
		}

		// Token: 0x06000755 RID: 1877 RVA: 0x00016AC8 File Offset: 0x00014CC8
		public Stack<T>.Enumerator GetEnumerator()
		{
			return new Stack<T>.Enumerator(this);
		}

		// Token: 0x040001F7 RID: 503
		private const int INITIAL_SIZE = 16;

		// Token: 0x040001F8 RID: 504
		private T[] _array;

		// Token: 0x040001F9 RID: 505
		private int _size;

		// Token: 0x040001FA RID: 506
		private int _version;

		// Token: 0x020000AB RID: 171
		[Serializable]
		public struct Enumerator : IEnumerator, IDisposable, IEnumerator<T>
		{
			// Token: 0x06000756 RID: 1878 RVA: 0x00016AD0 File Offset: 0x00014CD0
			internal Enumerator(Stack<T> t)
			{
				this.parent = t;
				this.idx = -2;
				this._version = t._version;
			}

			// Token: 0x06000757 RID: 1879 RVA: 0x00016AF0 File Offset: 0x00014CF0
			void IEnumerator.Reset()
			{
				if (this._version != this.parent._version)
				{
					throw new InvalidOperationException();
				}
				this.idx = -2;
			}

			// Token: 0x17000183 RID: 387
			// (get) Token: 0x06000758 RID: 1880 RVA: 0x00016B24 File Offset: 0x00014D24
			object IEnumerator.Current
			{
				get
				{
					return this.Current;
				}
			}

			// Token: 0x06000759 RID: 1881 RVA: 0x00016B34 File Offset: 0x00014D34
			public void Dispose()
			{
				this.idx = -2;
			}

			// Token: 0x0600075A RID: 1882 RVA: 0x00016B40 File Offset: 0x00014D40
			public bool MoveNext()
			{
				if (this._version != this.parent._version)
				{
					throw new InvalidOperationException();
				}
				if (this.idx == -2)
				{
					this.idx = this.parent._size;
				}
				return this.idx != -1 && --this.idx != -1;
			}

			// Token: 0x17000184 RID: 388
			// (get) Token: 0x0600075B RID: 1883 RVA: 0x00016BB0 File Offset: 0x00014DB0
			public T Current
			{
				get
				{
					if (this.idx < 0)
					{
						throw new InvalidOperationException();
					}
					return this.parent._array[this.idx];
				}
			}

			// Token: 0x040001FB RID: 507
			private const int NOT_STARTED = -2;

			// Token: 0x040001FC RID: 508
			private const int FINISHED = -1;

			// Token: 0x040001FD RID: 509
			private Stack<T> parent;

			// Token: 0x040001FE RID: 510
			private int idx;

			// Token: 0x040001FF RID: 511
			private int _version;
		}
	}
}
