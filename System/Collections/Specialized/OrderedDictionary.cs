﻿using System;
using System.Runtime.Serialization;

namespace System.Collections.Specialized
{
	// Token: 0x020000BB RID: 187
	[Serializable]
	public class OrderedDictionary : IDictionary, ICollection, IEnumerable, IDeserializationCallback, IOrderedDictionary, ISerializable
	{
		// Token: 0x0600080E RID: 2062 RVA: 0x000189B0 File Offset: 0x00016BB0
		public OrderedDictionary()
		{
			this.list = new ArrayList();
			this.hash = new Hashtable();
		}

		// Token: 0x0600080F RID: 2063 RVA: 0x000189D0 File Offset: 0x00016BD0
		public OrderedDictionary(int capacity)
		{
			this.initialCapacity = ((capacity >= 0) ? capacity : 0);
			this.list = new ArrayList(this.initialCapacity);
			this.hash = new Hashtable(this.initialCapacity);
		}

		// Token: 0x06000810 RID: 2064 RVA: 0x00018A1C File Offset: 0x00016C1C
		public OrderedDictionary(IEqualityComparer equalityComparer)
		{
			this.list = new ArrayList();
			this.hash = new Hashtable(equalityComparer);
			this.comparer = equalityComparer;
		}

		// Token: 0x06000811 RID: 2065 RVA: 0x00018A50 File Offset: 0x00016C50
		public OrderedDictionary(int capacity, IEqualityComparer equalityComparer)
		{
			this.initialCapacity = ((capacity >= 0) ? capacity : 0);
			this.list = new ArrayList(this.initialCapacity);
			this.hash = new Hashtable(this.initialCapacity, equalityComparer);
			this.comparer = equalityComparer;
		}

		// Token: 0x06000812 RID: 2066 RVA: 0x00018AA4 File Offset: 0x00016CA4
		protected OrderedDictionary(SerializationInfo info, StreamingContext context)
		{
			this.serializationInfo = info;
		}

		// Token: 0x06000813 RID: 2067 RVA: 0x00018AB4 File Offset: 0x00016CB4
		void IDeserializationCallback.OnDeserialization(object sender)
		{
			if (this.serializationInfo == null)
			{
				return;
			}
			this.comparer = (IEqualityComparer)this.serializationInfo.GetValue("KeyComparer", typeof(IEqualityComparer));
			this.readOnly = this.serializationInfo.GetBoolean("ReadOnly");
			this.initialCapacity = this.serializationInfo.GetInt32("InitialCapacity");
			if (this.list == null)
			{
				this.list = new ArrayList();
			}
			else
			{
				this.list.Clear();
			}
			this.hash = new Hashtable(this.comparer);
			object[] array = (object[])this.serializationInfo.GetValue("ArrayList", typeof(object[]));
			foreach (DictionaryEntry dictionaryEntry in array)
			{
				this.hash.Add(dictionaryEntry.Key, dictionaryEntry.Value);
				this.list.Add(dictionaryEntry);
			}
		}

		// Token: 0x06000814 RID: 2068 RVA: 0x00018BC0 File Offset: 0x00016DC0
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.list.GetEnumerator();
		}

		// Token: 0x170001B5 RID: 437
		// (get) Token: 0x06000815 RID: 2069 RVA: 0x00018BD0 File Offset: 0x00016DD0
		bool ICollection.IsSynchronized
		{
			get
			{
				return this.list.IsSynchronized;
			}
		}

		// Token: 0x170001B6 RID: 438
		// (get) Token: 0x06000816 RID: 2070 RVA: 0x00018BE0 File Offset: 0x00016DE0
		object ICollection.SyncRoot
		{
			get
			{
				return this.list.SyncRoot;
			}
		}

		// Token: 0x170001B7 RID: 439
		// (get) Token: 0x06000817 RID: 2071 RVA: 0x00018BF0 File Offset: 0x00016DF0
		bool IDictionary.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000818 RID: 2072 RVA: 0x00018BF4 File Offset: 0x00016DF4
		protected virtual void OnDeserialization(object sender)
		{
			((IDeserializationCallback)this).OnDeserialization(sender);
		}

		// Token: 0x06000819 RID: 2073 RVA: 0x00018C00 File Offset: 0x00016E00
		public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			if (info == null)
			{
				throw new ArgumentNullException("info");
			}
			info.AddValue("KeyComparer", this.comparer, typeof(IEqualityComparer));
			info.AddValue("ReadOnly", this.readOnly);
			info.AddValue("InitialCapacity", this.initialCapacity);
			object[] array = new object[this.hash.Count];
			this.hash.CopyTo(array, 0);
			info.AddValue("ArrayList", array);
		}

		// Token: 0x170001B8 RID: 440
		// (get) Token: 0x0600081A RID: 2074 RVA: 0x00018C88 File Offset: 0x00016E88
		public int Count
		{
			get
			{
				return this.list.Count;
			}
		}

		// Token: 0x0600081B RID: 2075 RVA: 0x00018C98 File Offset: 0x00016E98
		public void CopyTo(Array array, int index)
		{
			this.list.CopyTo(array, index);
		}

		// Token: 0x170001B9 RID: 441
		// (get) Token: 0x0600081C RID: 2076 RVA: 0x00018CA8 File Offset: 0x00016EA8
		public bool IsReadOnly
		{
			get
			{
				return this.readOnly;
			}
		}

		// Token: 0x170001BA RID: 442
		public object this[object key]
		{
			get
			{
				return this.hash[key];
			}
			set
			{
				this.WriteCheck();
				if (this.hash.Contains(key))
				{
					int index = this.FindListEntry(key);
					this.list[index] = new DictionaryEntry(key, value);
				}
				else
				{
					this.list.Add(new DictionaryEntry(key, value));
				}
				this.hash[key] = value;
			}
		}

		// Token: 0x170001BB RID: 443
		public object this[int index]
		{
			get
			{
				return ((DictionaryEntry)this.list[index]).Value;
			}
			set
			{
				this.WriteCheck();
				DictionaryEntry dictionaryEntry = (DictionaryEntry)this.list[index];
				dictionaryEntry.Value = value;
				this.list[index] = dictionaryEntry;
				this.hash[dictionaryEntry.Key] = value;
			}
		}

		// Token: 0x170001BC RID: 444
		// (get) Token: 0x06000821 RID: 2081 RVA: 0x00018DAC File Offset: 0x00016FAC
		public ICollection Keys
		{
			get
			{
				return new OrderedDictionary.OrderedCollection(this.list, true);
			}
		}

		// Token: 0x170001BD RID: 445
		// (get) Token: 0x06000822 RID: 2082 RVA: 0x00018DBC File Offset: 0x00016FBC
		public ICollection Values
		{
			get
			{
				return new OrderedDictionary.OrderedCollection(this.list, false);
			}
		}

		// Token: 0x06000823 RID: 2083 RVA: 0x00018DCC File Offset: 0x00016FCC
		public void Add(object key, object value)
		{
			this.WriteCheck();
			this.hash.Add(key, value);
			this.list.Add(new DictionaryEntry(key, value));
		}

		// Token: 0x06000824 RID: 2084 RVA: 0x00018DFC File Offset: 0x00016FFC
		public void Clear()
		{
			this.WriteCheck();
			this.hash.Clear();
			this.list.Clear();
		}

		// Token: 0x06000825 RID: 2085 RVA: 0x00018E1C File Offset: 0x0001701C
		public bool Contains(object key)
		{
			return this.hash.Contains(key);
		}

		// Token: 0x06000826 RID: 2086 RVA: 0x00018E2C File Offset: 0x0001702C
		public virtual IDictionaryEnumerator GetEnumerator()
		{
			return new OrderedDictionary.OrderedEntryCollectionEnumerator(this.list.GetEnumerator());
		}

		// Token: 0x06000827 RID: 2087 RVA: 0x00018E40 File Offset: 0x00017040
		public void Remove(object key)
		{
			this.WriteCheck();
			if (this.hash.Contains(key))
			{
				this.hash.Remove(key);
				int index = this.FindListEntry(key);
				this.list.RemoveAt(index);
			}
		}

		// Token: 0x06000828 RID: 2088 RVA: 0x00018E84 File Offset: 0x00017084
		private int FindListEntry(object key)
		{
			for (int i = 0; i < this.list.Count; i++)
			{
				DictionaryEntry dictionaryEntry = (DictionaryEntry)this.list[i];
				if ((this.comparer == null) ? dictionaryEntry.Key.Equals(key) : this.comparer.Equals(dictionaryEntry.Key, key))
				{
					return i;
				}
			}
			return -1;
		}

		// Token: 0x06000829 RID: 2089 RVA: 0x00018EF8 File Offset: 0x000170F8
		private void WriteCheck()
		{
			if (this.readOnly)
			{
				throw new NotSupportedException("Collection is read only");
			}
		}

		// Token: 0x0600082A RID: 2090 RVA: 0x00018F10 File Offset: 0x00017110
		public OrderedDictionary AsReadOnly()
		{
			return new OrderedDictionary
			{
				list = this.list,
				hash = this.hash,
				comparer = this.comparer,
				readOnly = true
			};
		}

		// Token: 0x0600082B RID: 2091 RVA: 0x00018F50 File Offset: 0x00017150
		public void Insert(int index, object key, object value)
		{
			this.WriteCheck();
			this.hash.Add(key, value);
			this.list.Insert(index, new DictionaryEntry(key, value));
		}

		// Token: 0x0600082C RID: 2092 RVA: 0x00018F88 File Offset: 0x00017188
		public void RemoveAt(int index)
		{
			this.WriteCheck();
			DictionaryEntry dictionaryEntry = (DictionaryEntry)this.list[index];
			this.list.RemoveAt(index);
			this.hash.Remove(dictionaryEntry.Key);
		}

		// Token: 0x04000227 RID: 551
		private ArrayList list;

		// Token: 0x04000228 RID: 552
		private Hashtable hash;

		// Token: 0x04000229 RID: 553
		private bool readOnly;

		// Token: 0x0400022A RID: 554
		private int initialCapacity;

		// Token: 0x0400022B RID: 555
		private SerializationInfo serializationInfo;

		// Token: 0x0400022C RID: 556
		private IEqualityComparer comparer;

		// Token: 0x020000BC RID: 188
		private class OrderedEntryCollectionEnumerator : IEnumerator, IDictionaryEnumerator
		{
			// Token: 0x0600082D RID: 2093 RVA: 0x00018FCC File Offset: 0x000171CC
			public OrderedEntryCollectionEnumerator(IEnumerator listEnumerator)
			{
				this.listEnumerator = listEnumerator;
			}

			// Token: 0x0600082E RID: 2094 RVA: 0x00018FDC File Offset: 0x000171DC
			public bool MoveNext()
			{
				return this.listEnumerator.MoveNext();
			}

			// Token: 0x0600082F RID: 2095 RVA: 0x00018FEC File Offset: 0x000171EC
			public void Reset()
			{
				this.listEnumerator.Reset();
			}

			// Token: 0x170001BE RID: 446
			// (get) Token: 0x06000830 RID: 2096 RVA: 0x00018FFC File Offset: 0x000171FC
			public object Current
			{
				get
				{
					return this.listEnumerator.Current;
				}
			}

			// Token: 0x170001BF RID: 447
			// (get) Token: 0x06000831 RID: 2097 RVA: 0x0001900C File Offset: 0x0001720C
			public DictionaryEntry Entry
			{
				get
				{
					return (DictionaryEntry)this.listEnumerator.Current;
				}
			}

			// Token: 0x170001C0 RID: 448
			// (get) Token: 0x06000832 RID: 2098 RVA: 0x00019020 File Offset: 0x00017220
			public object Key
			{
				get
				{
					return this.Entry.Key;
				}
			}

			// Token: 0x170001C1 RID: 449
			// (get) Token: 0x06000833 RID: 2099 RVA: 0x0001903C File Offset: 0x0001723C
			public object Value
			{
				get
				{
					return this.Entry.Value;
				}
			}

			// Token: 0x0400022D RID: 557
			private IEnumerator listEnumerator;
		}

		// Token: 0x020000BD RID: 189
		private class OrderedCollection : ICollection, IEnumerable
		{
			// Token: 0x06000834 RID: 2100 RVA: 0x00019058 File Offset: 0x00017258
			public OrderedCollection(ArrayList list, bool isKeyList)
			{
				this.list = list;
				this.isKeyList = isKeyList;
			}

			// Token: 0x170001C2 RID: 450
			// (get) Token: 0x06000835 RID: 2101 RVA: 0x00019070 File Offset: 0x00017270
			public int Count
			{
				get
				{
					return this.list.Count;
				}
			}

			// Token: 0x170001C3 RID: 451
			// (get) Token: 0x06000836 RID: 2102 RVA: 0x00019080 File Offset: 0x00017280
			public bool IsSynchronized
			{
				get
				{
					return false;
				}
			}

			// Token: 0x170001C4 RID: 452
			// (get) Token: 0x06000837 RID: 2103 RVA: 0x00019084 File Offset: 0x00017284
			public object SyncRoot
			{
				get
				{
					return this.list.SyncRoot;
				}
			}

			// Token: 0x06000838 RID: 2104 RVA: 0x00019094 File Offset: 0x00017294
			public void CopyTo(Array array, int index)
			{
				for (int i = 0; i < this.list.Count; i++)
				{
					DictionaryEntry dictionaryEntry = (DictionaryEntry)this.list[i];
					if (this.isKeyList)
					{
						array.SetValue(dictionaryEntry.Key, index + i);
					}
					else
					{
						array.SetValue(dictionaryEntry.Value, index + i);
					}
				}
			}

			// Token: 0x06000839 RID: 2105 RVA: 0x00019100 File Offset: 0x00017300
			public IEnumerator GetEnumerator()
			{
				return new OrderedDictionary.OrderedCollection.OrderedCollectionEnumerator(this.list.GetEnumerator(), this.isKeyList);
			}

			// Token: 0x0400022E RID: 558
			private ArrayList list;

			// Token: 0x0400022F RID: 559
			private bool isKeyList;

			// Token: 0x020000BE RID: 190
			private class OrderedCollectionEnumerator : IEnumerator
			{
				// Token: 0x0600083A RID: 2106 RVA: 0x00019118 File Offset: 0x00017318
				public OrderedCollectionEnumerator(IEnumerator listEnumerator, bool isKeyList)
				{
					this.listEnumerator = listEnumerator;
					this.isKeyList = isKeyList;
				}

				// Token: 0x170001C5 RID: 453
				// (get) Token: 0x0600083B RID: 2107 RVA: 0x00019130 File Offset: 0x00017330
				public object Current
				{
					get
					{
						DictionaryEntry dictionaryEntry = (DictionaryEntry)this.listEnumerator.Current;
						return (!this.isKeyList) ? dictionaryEntry.Value : dictionaryEntry.Key;
					}
				}

				// Token: 0x0600083C RID: 2108 RVA: 0x0001916C File Offset: 0x0001736C
				public bool MoveNext()
				{
					return this.listEnumerator.MoveNext();
				}

				// Token: 0x0600083D RID: 2109 RVA: 0x0001917C File Offset: 0x0001737C
				public void Reset()
				{
					this.listEnumerator.Reset();
				}

				// Token: 0x04000230 RID: 560
				private bool isKeyList;

				// Token: 0x04000231 RID: 561
				private IEnumerator listEnumerator;
			}
		}
	}
}
