﻿using System;

namespace System.Collections.Specialized
{
	// Token: 0x020000AF RID: 175
	[Serializable]
	public class HybridDictionary : IDictionary, ICollection, IEnumerable
	{
		// Token: 0x0600077B RID: 1915 RVA: 0x0001703C File Offset: 0x0001523C
		public HybridDictionary() : this(0, false)
		{
		}

		// Token: 0x0600077C RID: 1916 RVA: 0x00017048 File Offset: 0x00015248
		public HybridDictionary(bool caseInsensitive) : this(0, caseInsensitive)
		{
		}

		// Token: 0x0600077D RID: 1917 RVA: 0x00017054 File Offset: 0x00015254
		public HybridDictionary(int initialSize) : this(initialSize, false)
		{
		}

		// Token: 0x0600077E RID: 1918 RVA: 0x00017060 File Offset: 0x00015260
		public HybridDictionary(int initialSize, bool caseInsensitive)
		{
			this.caseInsensitive = caseInsensitive;
			IComparer comparer = (!caseInsensitive) ? null : CaseInsensitiveComparer.DefaultInvariant;
			IHashCodeProvider hcp = (!caseInsensitive) ? null : CaseInsensitiveHashCodeProvider.DefaultInvariant;
			if (initialSize <= 10)
			{
				this.list = new ListDictionary(comparer);
			}
			else
			{
				this.hashtable = new Hashtable(initialSize, hcp, comparer);
			}
		}

		// Token: 0x0600077F RID: 1919 RVA: 0x000170C8 File Offset: 0x000152C8
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.GetEnumerator();
		}

		// Token: 0x1700018A RID: 394
		// (get) Token: 0x06000780 RID: 1920 RVA: 0x000170D0 File Offset: 0x000152D0
		private IDictionary inner
		{
			get
			{
				IDictionary result;
				if (this.list == null)
				{
					IDictionary dictionary = this.hashtable;
					result = dictionary;
				}
				else
				{
					result = this.list;
				}
				return result;
			}
		}

		// Token: 0x1700018B RID: 395
		// (get) Token: 0x06000781 RID: 1921 RVA: 0x000170FC File Offset: 0x000152FC
		public int Count
		{
			get
			{
				return this.inner.Count;
			}
		}

		// Token: 0x1700018C RID: 396
		// (get) Token: 0x06000782 RID: 1922 RVA: 0x0001710C File Offset: 0x0001530C
		public bool IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700018D RID: 397
		// (get) Token: 0x06000783 RID: 1923 RVA: 0x00017110 File Offset: 0x00015310
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700018E RID: 398
		// (get) Token: 0x06000784 RID: 1924 RVA: 0x00017114 File Offset: 0x00015314
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x1700018F RID: 399
		public object this[object key]
		{
			get
			{
				return this.inner[key];
			}
			set
			{
				this.inner[key] = value;
				if (this.list != null && this.Count > 10)
				{
					this.Switch();
				}
			}
		}

		// Token: 0x17000190 RID: 400
		// (get) Token: 0x06000787 RID: 1927 RVA: 0x00017158 File Offset: 0x00015358
		public ICollection Keys
		{
			get
			{
				return this.inner.Keys;
			}
		}

		// Token: 0x17000191 RID: 401
		// (get) Token: 0x06000788 RID: 1928 RVA: 0x00017168 File Offset: 0x00015368
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000192 RID: 402
		// (get) Token: 0x06000789 RID: 1929 RVA: 0x0001716C File Offset: 0x0001536C
		public ICollection Values
		{
			get
			{
				return this.inner.Values;
			}
		}

		// Token: 0x0600078A RID: 1930 RVA: 0x0001717C File Offset: 0x0001537C
		public void Add(object key, object value)
		{
			this.inner.Add(key, value);
			if (this.list != null && this.Count > 10)
			{
				this.Switch();
			}
		}

		// Token: 0x0600078B RID: 1931 RVA: 0x000171AC File Offset: 0x000153AC
		public void Clear()
		{
			this.inner.Clear();
		}

		// Token: 0x0600078C RID: 1932 RVA: 0x000171BC File Offset: 0x000153BC
		public bool Contains(object key)
		{
			return this.inner.Contains(key);
		}

		// Token: 0x0600078D RID: 1933 RVA: 0x000171CC File Offset: 0x000153CC
		public void CopyTo(Array array, int index)
		{
			this.inner.CopyTo(array, index);
		}

		// Token: 0x0600078E RID: 1934 RVA: 0x000171DC File Offset: 0x000153DC
		public IDictionaryEnumerator GetEnumerator()
		{
			return this.inner.GetEnumerator();
		}

		// Token: 0x0600078F RID: 1935 RVA: 0x000171EC File Offset: 0x000153EC
		public void Remove(object key)
		{
			this.inner.Remove(key);
		}

		// Token: 0x06000790 RID: 1936 RVA: 0x000171FC File Offset: 0x000153FC
		private void Switch()
		{
			IComparer comparer = (!this.caseInsensitive) ? null : CaseInsensitiveComparer.DefaultInvariant;
			IHashCodeProvider hcp = (!this.caseInsensitive) ? null : CaseInsensitiveHashCodeProvider.DefaultInvariant;
			this.hashtable = new Hashtable(this.list, hcp, comparer);
			this.list.Clear();
			this.list = null;
		}

		// Token: 0x04000203 RID: 515
		private const int switchAfter = 10;

		// Token: 0x04000204 RID: 516
		private bool caseInsensitive;

		// Token: 0x04000205 RID: 517
		private Hashtable hashtable;

		// Token: 0x04000206 RID: 518
		private ListDictionary list;
	}
}
