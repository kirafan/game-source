﻿using System;

namespace System.Collections.Specialized
{
	// Token: 0x020000AE RID: 174
	public class CollectionsUtil
	{
		// Token: 0x06000777 RID: 1911 RVA: 0x00016FF4 File Offset: 0x000151F4
		public static Hashtable CreateCaseInsensitiveHashtable()
		{
			return new Hashtable(CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default);
		}

		// Token: 0x06000778 RID: 1912 RVA: 0x00017008 File Offset: 0x00015208
		public static Hashtable CreateCaseInsensitiveHashtable(IDictionary d)
		{
			return new Hashtable(d, CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default);
		}

		// Token: 0x06000779 RID: 1913 RVA: 0x0001701C File Offset: 0x0001521C
		public static Hashtable CreateCaseInsensitiveHashtable(int capacity)
		{
			return new Hashtable(capacity, CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default);
		}

		// Token: 0x0600077A RID: 1914 RVA: 0x00017030 File Offset: 0x00015230
		public static SortedList CreateCaseInsensitiveSortedList()
		{
			return new SortedList(CaseInsensitiveComparer.Default);
		}
	}
}
