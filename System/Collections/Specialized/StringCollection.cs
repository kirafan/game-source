﻿using System;

namespace System.Collections.Specialized
{
	// Token: 0x020000C0 RID: 192
	[Serializable]
	public class StringCollection : IList, ICollection, IEnumerable
	{
		// Token: 0x170001CC RID: 460
		// (get) Token: 0x0600084E RID: 2126 RVA: 0x000192C8 File Offset: 0x000174C8
		bool IList.IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170001CD RID: 461
		// (get) Token: 0x0600084F RID: 2127 RVA: 0x000192CC File Offset: 0x000174CC
		bool IList.IsFixedSize
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170001CE RID: 462
		object IList.this[int index]
		{
			get
			{
				return this[index];
			}
			set
			{
				this[index] = (string)value;
			}
		}

		// Token: 0x06000852 RID: 2130 RVA: 0x000192EC File Offset: 0x000174EC
		int IList.Add(object value)
		{
			return this.Add((string)value);
		}

		// Token: 0x06000853 RID: 2131 RVA: 0x000192FC File Offset: 0x000174FC
		bool IList.Contains(object value)
		{
			return this.Contains((string)value);
		}

		// Token: 0x06000854 RID: 2132 RVA: 0x0001930C File Offset: 0x0001750C
		int IList.IndexOf(object value)
		{
			return this.IndexOf((string)value);
		}

		// Token: 0x06000855 RID: 2133 RVA: 0x0001931C File Offset: 0x0001751C
		void IList.Insert(int index, object value)
		{
			this.Insert(index, (string)value);
		}

		// Token: 0x06000856 RID: 2134 RVA: 0x0001932C File Offset: 0x0001752C
		void IList.Remove(object value)
		{
			this.Remove((string)value);
		}

		// Token: 0x06000857 RID: 2135 RVA: 0x0001933C File Offset: 0x0001753C
		void ICollection.CopyTo(Array array, int index)
		{
			this.data.CopyTo(array, index);
		}

		// Token: 0x06000858 RID: 2136 RVA: 0x0001934C File Offset: 0x0001754C
		IEnumerator IEnumerable.GetEnumerator()
		{
			return this.data.GetEnumerator();
		}

		// Token: 0x170001CF RID: 463
		public string this[int index]
		{
			get
			{
				return (string)this.data[index];
			}
			set
			{
				this.data[index] = value;
			}
		}

		// Token: 0x170001D0 RID: 464
		// (get) Token: 0x0600085B RID: 2139 RVA: 0x00019380 File Offset: 0x00017580
		public int Count
		{
			get
			{
				return this.data.Count;
			}
		}

		// Token: 0x0600085C RID: 2140 RVA: 0x00019390 File Offset: 0x00017590
		public int Add(string value)
		{
			return this.data.Add(value);
		}

		// Token: 0x0600085D RID: 2141 RVA: 0x000193A0 File Offset: 0x000175A0
		public void AddRange(string[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			this.data.AddRange(value);
		}

		// Token: 0x0600085E RID: 2142 RVA: 0x000193C0 File Offset: 0x000175C0
		public void Clear()
		{
			this.data.Clear();
		}

		// Token: 0x0600085F RID: 2143 RVA: 0x000193D0 File Offset: 0x000175D0
		public bool Contains(string value)
		{
			return this.data.Contains(value);
		}

		// Token: 0x06000860 RID: 2144 RVA: 0x000193E0 File Offset: 0x000175E0
		public void CopyTo(string[] array, int index)
		{
			this.data.CopyTo(array, index);
		}

		// Token: 0x06000861 RID: 2145 RVA: 0x000193F0 File Offset: 0x000175F0
		public StringEnumerator GetEnumerator()
		{
			return new StringEnumerator(this);
		}

		// Token: 0x06000862 RID: 2146 RVA: 0x000193F8 File Offset: 0x000175F8
		public int IndexOf(string value)
		{
			return this.data.IndexOf(value);
		}

		// Token: 0x06000863 RID: 2147 RVA: 0x00019408 File Offset: 0x00017608
		public void Insert(int index, string value)
		{
			this.data.Insert(index, value);
		}

		// Token: 0x170001D1 RID: 465
		// (get) Token: 0x06000864 RID: 2148 RVA: 0x00019418 File Offset: 0x00017618
		public bool IsReadOnly
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170001D2 RID: 466
		// (get) Token: 0x06000865 RID: 2149 RVA: 0x0001941C File Offset: 0x0001761C
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x06000866 RID: 2150 RVA: 0x00019420 File Offset: 0x00017620
		public void Remove(string value)
		{
			this.data.Remove(value);
		}

		// Token: 0x06000867 RID: 2151 RVA: 0x00019430 File Offset: 0x00017630
		public void RemoveAt(int index)
		{
			this.data.RemoveAt(index);
		}

		// Token: 0x170001D3 RID: 467
		// (get) Token: 0x06000868 RID: 2152 RVA: 0x00019440 File Offset: 0x00017640
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x04000233 RID: 563
		private ArrayList data = new ArrayList();
	}
}
