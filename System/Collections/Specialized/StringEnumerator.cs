﻿using System;

namespace System.Collections.Specialized
{
	// Token: 0x020000C2 RID: 194
	public class StringEnumerator
	{
		// Token: 0x06000878 RID: 2168 RVA: 0x000195E0 File Offset: 0x000177E0
		internal StringEnumerator(StringCollection coll)
		{
			this.enumerable = ((IEnumerable)coll).GetEnumerator();
		}

		// Token: 0x170001DA RID: 474
		// (get) Token: 0x06000879 RID: 2169 RVA: 0x000195F4 File Offset: 0x000177F4
		public string Current
		{
			get
			{
				return (string)this.enumerable.Current;
			}
		}

		// Token: 0x0600087A RID: 2170 RVA: 0x00019608 File Offset: 0x00017808
		public bool MoveNext()
		{
			return this.enumerable.MoveNext();
		}

		// Token: 0x0600087B RID: 2171 RVA: 0x00019618 File Offset: 0x00017818
		public void Reset()
		{
			this.enumerable.Reset();
		}

		// Token: 0x04000235 RID: 565
		private IEnumerator enumerable;
	}
}
