﻿using System;

namespace System.Collections.Specialized
{
	// Token: 0x020000B0 RID: 176
	public interface IOrderedDictionary : IDictionary, ICollection, IEnumerable
	{
		// Token: 0x06000791 RID: 1937
		IDictionaryEnumerator GetEnumerator();

		// Token: 0x06000792 RID: 1938
		void Insert(int idx, object key, object value);

		// Token: 0x06000793 RID: 1939
		void RemoveAt(int idx);

		// Token: 0x17000193 RID: 403
		object this[int idx]
		{
			get;
			set;
		}
	}
}
