﻿using System;
using System.ComponentModel.Design.Serialization;
using System.Globalization;

namespace System.Collections.Specialized
{
	// Token: 0x020000C1 RID: 193
	[System.ComponentModel.Design.Serialization.DesignerSerializer("System.Diagnostics.Design.StringDictionaryCodeDomSerializer, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a", "System.ComponentModel.Design.Serialization.CodeDomSerializer, System.Design, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a")]
	[Serializable]
	public class StringDictionary : IEnumerable
	{
		// Token: 0x06000869 RID: 2153 RVA: 0x00019444 File Offset: 0x00017644
		public StringDictionary()
		{
			this.contents = new Hashtable();
		}

		// Token: 0x170001D4 RID: 468
		// (get) Token: 0x0600086A RID: 2154 RVA: 0x00019458 File Offset: 0x00017658
		public virtual int Count
		{
			get
			{
				return this.contents.Count;
			}
		}

		// Token: 0x170001D5 RID: 469
		// (get) Token: 0x0600086B RID: 2155 RVA: 0x00019468 File Offset: 0x00017668
		public virtual bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170001D6 RID: 470
		public virtual string this[string key]
		{
			get
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				return (string)this.contents[key.ToLower(CultureInfo.InvariantCulture)];
			}
			set
			{
				if (key == null)
				{
					throw new ArgumentNullException("key");
				}
				this.contents[key.ToLower(CultureInfo.InvariantCulture)] = value;
			}
		}

		// Token: 0x170001D7 RID: 471
		// (get) Token: 0x0600086E RID: 2158 RVA: 0x000194E0 File Offset: 0x000176E0
		public virtual ICollection Keys
		{
			get
			{
				return this.contents.Keys;
			}
		}

		// Token: 0x170001D8 RID: 472
		// (get) Token: 0x0600086F RID: 2159 RVA: 0x000194F0 File Offset: 0x000176F0
		public virtual ICollection Values
		{
			get
			{
				return this.contents.Values;
			}
		}

		// Token: 0x170001D9 RID: 473
		// (get) Token: 0x06000870 RID: 2160 RVA: 0x00019500 File Offset: 0x00017700
		public virtual object SyncRoot
		{
			get
			{
				return this.contents.SyncRoot;
			}
		}

		// Token: 0x06000871 RID: 2161 RVA: 0x00019510 File Offset: 0x00017710
		public virtual void Add(string key, string value)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.contents.Add(key.ToLower(CultureInfo.InvariantCulture), value);
		}

		// Token: 0x06000872 RID: 2162 RVA: 0x00019548 File Offset: 0x00017748
		public virtual void Clear()
		{
			this.contents.Clear();
		}

		// Token: 0x06000873 RID: 2163 RVA: 0x00019558 File Offset: 0x00017758
		public virtual bool ContainsKey(string key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			return this.contents.ContainsKey(key.ToLower(CultureInfo.InvariantCulture));
		}

		// Token: 0x06000874 RID: 2164 RVA: 0x00019584 File Offset: 0x00017784
		public virtual bool ContainsValue(string value)
		{
			return this.contents.ContainsValue(value);
		}

		// Token: 0x06000875 RID: 2165 RVA: 0x00019594 File Offset: 0x00017794
		public virtual void CopyTo(Array array, int index)
		{
			this.contents.CopyTo(array, index);
		}

		// Token: 0x06000876 RID: 2166 RVA: 0x000195A4 File Offset: 0x000177A4
		public virtual IEnumerator GetEnumerator()
		{
			return this.contents.GetEnumerator();
		}

		// Token: 0x06000877 RID: 2167 RVA: 0x000195B4 File Offset: 0x000177B4
		public virtual void Remove(string key)
		{
			if (key == null)
			{
				throw new ArgumentNullException("key");
			}
			this.contents.Remove(key.ToLower(CultureInfo.InvariantCulture));
		}

		// Token: 0x04000234 RID: 564
		private Hashtable contents;
	}
}
