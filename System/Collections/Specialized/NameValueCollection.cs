﻿using System;
using System.Runtime.Serialization;
using System.Text;

namespace System.Collections.Specialized
{
	// Token: 0x020000BA RID: 186
	[Serializable]
	public class NameValueCollection : NameObjectCollectionBase
	{
		// Token: 0x060007F1 RID: 2033 RVA: 0x0001840C File Offset: 0x0001660C
		public NameValueCollection()
		{
		}

		// Token: 0x060007F2 RID: 2034 RVA: 0x00018414 File Offset: 0x00016614
		public NameValueCollection(int capacity) : base(capacity)
		{
		}

		// Token: 0x060007F3 RID: 2035 RVA: 0x00018420 File Offset: 0x00016620
		public NameValueCollection(NameValueCollection col)
		{
			IEqualityComparer equalityComparer2;
			if (col == null)
			{
				IEqualityComparer equalityComparer = null;
				equalityComparer2 = equalityComparer;
			}
			else
			{
				equalityComparer2 = col.EqualityComparer;
			}
			IComparer comparer2;
			if (col == null)
			{
				IComparer comparer = null;
				comparer2 = comparer;
			}
			else
			{
				comparer2 = col.Comparer;
			}
			IHashCodeProvider hcp;
			if (col == null)
			{
				IHashCodeProvider hashCodeProvider = null;
				hcp = hashCodeProvider;
			}
			else
			{
				hcp = col.HashCodeProvider;
			}
			base..ctor(equalityComparer2, comparer2, hcp);
			if (col == null)
			{
				throw new ArgumentNullException("col");
			}
			this.Add(col);
		}

		// Token: 0x060007F4 RID: 2036 RVA: 0x00018488 File Offset: 0x00016688
		[Obsolete("Use NameValueCollection (IEqualityComparer)")]
		public NameValueCollection(IHashCodeProvider hashProvider, IComparer comparer) : base(hashProvider, comparer)
		{
		}

		// Token: 0x060007F5 RID: 2037 RVA: 0x00018494 File Offset: 0x00016694
		public NameValueCollection(int capacity, NameValueCollection col)
		{
			IHashCodeProvider hashProvider;
			if (col == null)
			{
				IHashCodeProvider hashCodeProvider = null;
				hashProvider = hashCodeProvider;
			}
			else
			{
				hashProvider = col.HashCodeProvider;
			}
			IComparer comparer2;
			if (col == null)
			{
				IComparer comparer = null;
				comparer2 = comparer;
			}
			else
			{
				comparer2 = col.Comparer;
			}
			base..ctor(capacity, hashProvider, comparer2);
			this.Add(col);
		}

		// Token: 0x060007F6 RID: 2038 RVA: 0x000184D8 File Offset: 0x000166D8
		protected NameValueCollection(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x060007F7 RID: 2039 RVA: 0x000184E4 File Offset: 0x000166E4
		[Obsolete("Use NameValueCollection (IEqualityComparer)")]
		public NameValueCollection(int capacity, IHashCodeProvider hashProvider, IComparer comparer) : base(capacity, hashProvider, comparer)
		{
		}

		// Token: 0x060007F8 RID: 2040 RVA: 0x000184F0 File Offset: 0x000166F0
		public NameValueCollection(IEqualityComparer equalityComparer) : base(equalityComparer)
		{
		}

		// Token: 0x060007F9 RID: 2041 RVA: 0x000184FC File Offset: 0x000166FC
		public NameValueCollection(int capacity, IEqualityComparer equalityComparer) : base(capacity, equalityComparer)
		{
		}

		// Token: 0x170001B2 RID: 434
		// (get) Token: 0x060007FA RID: 2042 RVA: 0x00018508 File Offset: 0x00016708
		public virtual string[] AllKeys
		{
			get
			{
				if (this.cachedAllKeys == null)
				{
					this.cachedAllKeys = base.BaseGetAllKeys();
				}
				return this.cachedAllKeys;
			}
		}

		// Token: 0x170001B3 RID: 435
		public string this[int index]
		{
			get
			{
				return this.Get(index);
			}
		}

		// Token: 0x170001B4 RID: 436
		public string this[string name]
		{
			get
			{
				return this.Get(name);
			}
			set
			{
				this.Set(name, value);
			}
		}

		// Token: 0x060007FE RID: 2046 RVA: 0x0001854C File Offset: 0x0001674C
		public void Add(NameValueCollection c)
		{
			if (base.IsReadOnly)
			{
				throw new NotSupportedException("Collection is read-only");
			}
			if (c == null)
			{
				throw new ArgumentNullException("c");
			}
			this.InvalidateCachedArrays();
			int count = c.Count;
			for (int i = 0; i < count; i++)
			{
				string key = c.GetKey(i);
				ArrayList arrayList = (ArrayList)c.BaseGet(i);
				ArrayList arrayList2 = (ArrayList)base.BaseGet(key);
				if (arrayList2 != null && arrayList != null)
				{
					arrayList2.AddRange(arrayList);
				}
				else if (arrayList != null)
				{
					arrayList2 = new ArrayList(arrayList);
				}
				base.BaseSet(key, arrayList2);
			}
		}

		// Token: 0x060007FF RID: 2047 RVA: 0x000185F4 File Offset: 0x000167F4
		public virtual void Add(string name, string val)
		{
			if (base.IsReadOnly)
			{
				throw new NotSupportedException("Collection is read-only");
			}
			this.InvalidateCachedArrays();
			ArrayList arrayList = (ArrayList)base.BaseGet(name);
			if (arrayList == null)
			{
				arrayList = new ArrayList();
				if (val != null)
				{
					arrayList.Add(val);
				}
				base.BaseAdd(name, arrayList);
			}
			else if (val != null)
			{
				arrayList.Add(val);
			}
		}

		// Token: 0x06000800 RID: 2048 RVA: 0x00018660 File Offset: 0x00016860
		public virtual void Clear()
		{
			if (base.IsReadOnly)
			{
				throw new NotSupportedException("Collection is read-only");
			}
			this.InvalidateCachedArrays();
			base.BaseClear();
		}

		// Token: 0x06000801 RID: 2049 RVA: 0x00018690 File Offset: 0x00016890
		public void CopyTo(Array dest, int index)
		{
			if (dest == null)
			{
				throw new ArgumentNullException("dest", "Null argument - dest");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("index", "index is less than 0");
			}
			if (dest.Rank > 1)
			{
				throw new ArgumentException("dest", "multidim");
			}
			if (this.cachedAll == null)
			{
				this.RefreshCachedAll();
			}
			try
			{
				this.cachedAll.CopyTo(dest, index);
			}
			catch (ArrayTypeMismatchException)
			{
				throw new InvalidCastException();
			}
		}

		// Token: 0x06000802 RID: 2050 RVA: 0x00018734 File Offset: 0x00016934
		private void RefreshCachedAll()
		{
			this.cachedAll = null;
			int count = this.Count;
			this.cachedAll = new string[count];
			for (int i = 0; i < count; i++)
			{
				this.cachedAll[i] = this.Get(i);
			}
		}

		// Token: 0x06000803 RID: 2051 RVA: 0x0001877C File Offset: 0x0001697C
		public virtual string Get(int index)
		{
			ArrayList values = (ArrayList)base.BaseGet(index);
			return NameValueCollection.AsSingleString(values);
		}

		// Token: 0x06000804 RID: 2052 RVA: 0x0001879C File Offset: 0x0001699C
		public virtual string Get(string name)
		{
			ArrayList values = (ArrayList)base.BaseGet(name);
			return NameValueCollection.AsSingleString(values);
		}

		// Token: 0x06000805 RID: 2053 RVA: 0x000187BC File Offset: 0x000169BC
		private static string AsSingleString(ArrayList values)
		{
			if (values == null)
			{
				return null;
			}
			int count = values.Count;
			switch (count)
			{
			case 0:
				return null;
			case 1:
				return (string)values[0];
			case 2:
				return (string)values[0] + ',' + (string)values[1];
			default:
			{
				int num = count;
				for (int i = 0; i < count; i++)
				{
					num += ((string)values[i]).Length;
				}
				StringBuilder stringBuilder = new StringBuilder((string)values[0], num);
				for (int j = 1; j < count; j++)
				{
					stringBuilder.Append(',');
					stringBuilder.Append(values[j]);
				}
				return stringBuilder.ToString();
			}
			}
		}

		// Token: 0x06000806 RID: 2054 RVA: 0x0001889C File Offset: 0x00016A9C
		public virtual string GetKey(int index)
		{
			return base.BaseGetKey(index);
		}

		// Token: 0x06000807 RID: 2055 RVA: 0x000188A8 File Offset: 0x00016AA8
		public virtual string[] GetValues(int index)
		{
			ArrayList values = (ArrayList)base.BaseGet(index);
			return NameValueCollection.AsStringArray(values);
		}

		// Token: 0x06000808 RID: 2056 RVA: 0x000188C8 File Offset: 0x00016AC8
		public virtual string[] GetValues(string name)
		{
			ArrayList values = (ArrayList)base.BaseGet(name);
			return NameValueCollection.AsStringArray(values);
		}

		// Token: 0x06000809 RID: 2057 RVA: 0x000188E8 File Offset: 0x00016AE8
		private static string[] AsStringArray(ArrayList values)
		{
			if (values == null)
			{
				return null;
			}
			int count = values.Count;
			if (count == 0)
			{
				return null;
			}
			string[] array = new string[count];
			values.CopyTo(array);
			return array;
		}

		// Token: 0x0600080A RID: 2058 RVA: 0x0001891C File Offset: 0x00016B1C
		public bool HasKeys()
		{
			return base.BaseHasKeys();
		}

		// Token: 0x0600080B RID: 2059 RVA: 0x00018924 File Offset: 0x00016B24
		public virtual void Remove(string name)
		{
			if (base.IsReadOnly)
			{
				throw new NotSupportedException("Collection is read-only");
			}
			this.InvalidateCachedArrays();
			base.BaseRemove(name);
		}

		// Token: 0x0600080C RID: 2060 RVA: 0x0001894C File Offset: 0x00016B4C
		public virtual void Set(string name, string value)
		{
			if (base.IsReadOnly)
			{
				throw new NotSupportedException("Collection is read-only");
			}
			this.InvalidateCachedArrays();
			ArrayList arrayList = new ArrayList();
			if (value != null)
			{
				arrayList.Add(value);
				base.BaseSet(name, arrayList);
			}
			else
			{
				base.BaseSet(name, null);
			}
		}

		// Token: 0x0600080D RID: 2061 RVA: 0x000189A0 File Offset: 0x00016BA0
		protected void InvalidateCachedArrays()
		{
			this.cachedAllKeys = null;
			this.cachedAll = null;
		}

		// Token: 0x04000225 RID: 549
		private string[] cachedAllKeys;

		// Token: 0x04000226 RID: 550
		private string[] cachedAll;
	}
}
