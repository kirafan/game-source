﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x02000460 RID: 1120
	[Flags]
	[Serializable]
	public enum StorePermissionFlags
	{
		// Token: 0x040018C8 RID: 6344
		NoFlags = 0,
		// Token: 0x040018C9 RID: 6345
		CreateStore = 1,
		// Token: 0x040018CA RID: 6346
		DeleteStore = 2,
		// Token: 0x040018CB RID: 6347
		EnumerateStores = 4,
		// Token: 0x040018CC RID: 6348
		OpenStore = 16,
		// Token: 0x040018CD RID: 6349
		AddToStore = 32,
		// Token: 0x040018CE RID: 6350
		RemoveFromStore = 64,
		// Token: 0x040018CF RID: 6351
		EnumerateCertificates = 128,
		// Token: 0x040018D0 RID: 6352
		AllFlags = 247
	}
}
