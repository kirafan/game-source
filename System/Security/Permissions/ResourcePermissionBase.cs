﻿using System;
using System.Collections;

namespace System.Security.Permissions
{
	// Token: 0x0200045C RID: 1116
	[Serializable]
	public abstract class ResourcePermissionBase : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x06002807 RID: 10247 RVA: 0x0007EC80 File Offset: 0x0007CE80
		protected ResourcePermissionBase()
		{
			this._list = new ArrayList();
		}

		// Token: 0x06002808 RID: 10248 RVA: 0x0007EC94 File Offset: 0x0007CE94
		protected ResourcePermissionBase(PermissionState state) : this()
		{
			PermissionHelper.CheckPermissionState(state, true);
			this._unrestricted = (state == PermissionState.Unrestricted);
		}

		// Token: 0x17000B3A RID: 2874
		// (get) Token: 0x0600280A RID: 10250 RVA: 0x0007ECC8 File Offset: 0x0007CEC8
		// (set) Token: 0x0600280B RID: 10251 RVA: 0x0007ECD0 File Offset: 0x0007CED0
		protected Type PermissionAccessType
		{
			get
			{
				return this._type;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("PermissionAccessType");
				}
				if (!value.IsEnum)
				{
					throw new ArgumentException("!Enum", "PermissionAccessType");
				}
				this._type = value;
			}
		}

		// Token: 0x17000B3B RID: 2875
		// (get) Token: 0x0600280C RID: 10252 RVA: 0x0007ED08 File Offset: 0x0007CF08
		// (set) Token: 0x0600280D RID: 10253 RVA: 0x0007ED10 File Offset: 0x0007CF10
		protected string[] TagNames
		{
			get
			{
				return this._tags;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("TagNames");
				}
				if (value.Length == 0)
				{
					throw new ArgumentException("Length==0", "TagNames");
				}
				this._tags = value;
			}
		}

		// Token: 0x0600280E RID: 10254 RVA: 0x0007ED50 File Offset: 0x0007CF50
		protected void AddPermissionAccess(ResourcePermissionBaseEntry entry)
		{
			this.CheckEntry(entry);
			if (this.Exists(entry))
			{
				string text = Locale.GetText("Entry already exists.");
				throw new InvalidOperationException(text);
			}
			this._list.Add(entry);
		}

		// Token: 0x0600280F RID: 10255 RVA: 0x0007ED90 File Offset: 0x0007CF90
		protected void Clear()
		{
			this._list.Clear();
		}

		// Token: 0x06002810 RID: 10256 RVA: 0x0007EDA0 File Offset: 0x0007CFA0
		public override IPermission Copy()
		{
			ResourcePermissionBase resourcePermissionBase = ResourcePermissionBase.CreateFromType(base.GetType(), this._unrestricted);
			if (this._tags != null)
			{
				resourcePermissionBase._tags = (string[])this._tags.Clone();
			}
			resourcePermissionBase._type = this._type;
			resourcePermissionBase._list.AddRange(this._list);
			return resourcePermissionBase;
		}

		// Token: 0x06002811 RID: 10257 RVA: 0x0007EE00 File Offset: 0x0007D000
		[MonoTODO("incomplete - need more test")]
		public override void FromXml(SecurityElement securityElement)
		{
			if (securityElement == null)
			{
				throw new ArgumentNullException("securityElement");
			}
			this.CheckSecurityElement(securityElement, "securityElement", 1, 1);
			this._list.Clear();
			this._unrestricted = PermissionHelper.IsUnrestricted(securityElement);
			if (securityElement.Children == null || securityElement.Children.Count < 1)
			{
				return;
			}
			string[] array = new string[1];
			foreach (object obj in securityElement.Children)
			{
				SecurityElement securityElement2 = (SecurityElement)obj;
				array[0] = securityElement2.Attribute("name");
				int permissionAccess = (int)Enum.Parse(this.PermissionAccessType, securityElement2.Attribute("access"));
				ResourcePermissionBaseEntry entry = new ResourcePermissionBaseEntry(permissionAccess, array);
				this.AddPermissionAccess(entry);
			}
		}

		// Token: 0x06002812 RID: 10258 RVA: 0x0007EF04 File Offset: 0x0007D104
		protected ResourcePermissionBaseEntry[] GetPermissionEntries()
		{
			ResourcePermissionBaseEntry[] array = new ResourcePermissionBaseEntry[this._list.Count];
			this._list.CopyTo(array, 0);
			return array;
		}

		// Token: 0x06002813 RID: 10259 RVA: 0x0007EF30 File Offset: 0x0007D130
		public override IPermission Intersect(IPermission target)
		{
			ResourcePermissionBase resourcePermissionBase = this.Cast(target);
			if (resourcePermissionBase == null)
			{
				return null;
			}
			bool flag = this.IsUnrestricted();
			bool flag2 = resourcePermissionBase.IsUnrestricted();
			if (this.IsEmpty() && !flag2)
			{
				return null;
			}
			if (resourcePermissionBase.IsEmpty() && !flag)
			{
				return null;
			}
			ResourcePermissionBase resourcePermissionBase2 = ResourcePermissionBase.CreateFromType(base.GetType(), flag && flag2);
			foreach (object obj in this._list)
			{
				ResourcePermissionBaseEntry entry = (ResourcePermissionBaseEntry)obj;
				if (flag2 || resourcePermissionBase.Exists(entry))
				{
					resourcePermissionBase2.AddPermissionAccess(entry);
				}
			}
			foreach (object obj2 in resourcePermissionBase._list)
			{
				ResourcePermissionBaseEntry entry2 = (ResourcePermissionBaseEntry)obj2;
				if ((flag || this.Exists(entry2)) && !resourcePermissionBase2.Exists(entry2))
				{
					resourcePermissionBase2.AddPermissionAccess(entry2);
				}
			}
			return resourcePermissionBase2;
		}

		// Token: 0x06002814 RID: 10260 RVA: 0x0007F0A0 File Offset: 0x0007D2A0
		public override bool IsSubsetOf(IPermission target)
		{
			if (target == null)
			{
				return true;
			}
			ResourcePermissionBase resourcePermissionBase = target as ResourcePermissionBase;
			if (resourcePermissionBase == null)
			{
				return false;
			}
			if (resourcePermissionBase.IsUnrestricted())
			{
				return true;
			}
			if (this.IsUnrestricted())
			{
				return resourcePermissionBase.IsUnrestricted();
			}
			foreach (object obj in this._list)
			{
				ResourcePermissionBaseEntry entry = (ResourcePermissionBaseEntry)obj;
				if (!resourcePermissionBase.Exists(entry))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x06002815 RID: 10261 RVA: 0x0007F158 File Offset: 0x0007D358
		public bool IsUnrestricted()
		{
			return this._unrestricted;
		}

		// Token: 0x06002816 RID: 10262 RVA: 0x0007F160 File Offset: 0x0007D360
		protected void RemovePermissionAccess(ResourcePermissionBaseEntry entry)
		{
			this.CheckEntry(entry);
			for (int i = 0; i < this._list.Count; i++)
			{
				ResourcePermissionBaseEntry entry2 = (ResourcePermissionBaseEntry)this._list[i];
				if (this.Equals(entry, entry2))
				{
					this._list.RemoveAt(i);
					return;
				}
			}
			string text = Locale.GetText("Entry doesn't exists.");
			throw new InvalidOperationException(text);
		}

		// Token: 0x06002817 RID: 10263 RVA: 0x0007F1D0 File Offset: 0x0007D3D0
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = PermissionHelper.Element(base.GetType(), 1);
			if (this.IsUnrestricted())
			{
				securityElement.AddAttribute("Unrestricted", "true");
			}
			else
			{
				foreach (object obj in this._list)
				{
					ResourcePermissionBaseEntry resourcePermissionBaseEntry = (ResourcePermissionBaseEntry)obj;
					SecurityElement securityElement2 = securityElement;
					string text = null;
					if (this.PermissionAccessType != null)
					{
						text = Enum.Format(this.PermissionAccessType, resourcePermissionBaseEntry.PermissionAccess, "g");
					}
					for (int i = 0; i < this._tags.Length; i++)
					{
						SecurityElement securityElement3 = new SecurityElement(this._tags[i]);
						securityElement3.AddAttribute("name", resourcePermissionBaseEntry.PermissionAccessPath[i]);
						if (text != null)
						{
							securityElement3.AddAttribute("access", text);
						}
						securityElement2.AddChild(securityElement3);
					}
				}
			}
			return securityElement;
		}

		// Token: 0x06002818 RID: 10264 RVA: 0x0007F2F8 File Offset: 0x0007D4F8
		public override IPermission Union(IPermission target)
		{
			ResourcePermissionBase resourcePermissionBase = this.Cast(target);
			if (resourcePermissionBase == null)
			{
				return this.Copy();
			}
			if (this.IsEmpty() && resourcePermissionBase.IsEmpty())
			{
				return null;
			}
			if (resourcePermissionBase.IsEmpty())
			{
				return this.Copy();
			}
			if (this.IsEmpty())
			{
				return resourcePermissionBase.Copy();
			}
			bool flag = this.IsUnrestricted() || resourcePermissionBase.IsUnrestricted();
			ResourcePermissionBase resourcePermissionBase2 = ResourcePermissionBase.CreateFromType(base.GetType(), flag);
			if (!flag)
			{
				foreach (object obj in this._list)
				{
					ResourcePermissionBaseEntry entry = (ResourcePermissionBaseEntry)obj;
					resourcePermissionBase2.AddPermissionAccess(entry);
				}
				foreach (object obj2 in resourcePermissionBase._list)
				{
					ResourcePermissionBaseEntry entry2 = (ResourcePermissionBaseEntry)obj2;
					if (!resourcePermissionBase2.Exists(entry2))
					{
						resourcePermissionBase2.AddPermissionAccess(entry2);
					}
				}
			}
			return resourcePermissionBase2;
		}

		// Token: 0x06002819 RID: 10265 RVA: 0x0007F45C File Offset: 0x0007D65C
		private bool IsEmpty()
		{
			return !this._unrestricted && this._list.Count == 0;
		}

		// Token: 0x0600281A RID: 10266 RVA: 0x0007F47C File Offset: 0x0007D67C
		private ResourcePermissionBase Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			ResourcePermissionBase resourcePermissionBase = target as ResourcePermissionBase;
			if (resourcePermissionBase == null)
			{
				PermissionHelper.ThrowInvalidPermission(target, typeof(ResourcePermissionBase));
			}
			return resourcePermissionBase;
		}

		// Token: 0x0600281B RID: 10267 RVA: 0x0007F4B0 File Offset: 0x0007D6B0
		internal void CheckEntry(ResourcePermissionBaseEntry entry)
		{
			if (entry == null)
			{
				throw new ArgumentNullException("entry");
			}
			if (entry.PermissionAccessPath == null || entry.PermissionAccessPath.Length != this._tags.Length)
			{
				string text = Locale.GetText("Entry doesn't match TagNames");
				throw new InvalidOperationException(text);
			}
		}

		// Token: 0x0600281C RID: 10268 RVA: 0x0007F500 File Offset: 0x0007D700
		internal bool Equals(ResourcePermissionBaseEntry entry1, ResourcePermissionBaseEntry entry2)
		{
			if (entry1.PermissionAccess != entry2.PermissionAccess)
			{
				return false;
			}
			if (entry1.PermissionAccessPath.Length != entry2.PermissionAccessPath.Length)
			{
				return false;
			}
			for (int i = 0; i < entry1.PermissionAccessPath.Length; i++)
			{
				if (entry1.PermissionAccessPath[i] != entry2.PermissionAccessPath[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x0600281D RID: 10269 RVA: 0x0007F570 File Offset: 0x0007D770
		internal bool Exists(ResourcePermissionBaseEntry entry)
		{
			if (this._list.Count == 0)
			{
				return false;
			}
			foreach (object obj in this._list)
			{
				ResourcePermissionBaseEntry entry2 = (ResourcePermissionBaseEntry)obj;
				if (this.Equals(entry2, entry))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0600281E RID: 10270 RVA: 0x0007F604 File Offset: 0x0007D804
		internal int CheckSecurityElement(SecurityElement se, string parameterName, int minimumVersion, int maximumVersion)
		{
			if (se == null)
			{
				throw new ArgumentNullException(parameterName);
			}
			if (se.Tag != "IPermission")
			{
				string message = string.Format(Locale.GetText("Invalid tag {0}"), se.Tag);
				throw new ArgumentException(message, parameterName);
			}
			int num = minimumVersion;
			string text = se.Attribute("version");
			if (text != null)
			{
				try
				{
					num = int.Parse(text);
				}
				catch (Exception innerException)
				{
					string text2 = Locale.GetText("Couldn't parse version from '{0}'.");
					text2 = string.Format(text2, text);
					throw new ArgumentException(text2, parameterName, innerException);
				}
			}
			if (num < minimumVersion || num > maximumVersion)
			{
				string text3 = Locale.GetText("Unknown version '{0}', expected versions between ['{1}','{2}'].");
				text3 = string.Format(text3, num, minimumVersion, maximumVersion);
				throw new ArgumentException(text3, parameterName);
			}
			return num;
		}

		// Token: 0x0600281F RID: 10271 RVA: 0x0007F6F4 File Offset: 0x0007D8F4
		internal static void ValidateMachineName(string name)
		{
			if (name == null || name.Length == 0 || name.IndexOfAny(ResourcePermissionBase.invalidChars) != -1)
			{
				string text = Locale.GetText("Invalid machine name '{0}'.");
				if (name == null)
				{
					name = "(null)";
				}
				text = string.Format(text, name);
				throw new ArgumentException(text, "MachineName");
			}
		}

		// Token: 0x06002820 RID: 10272 RVA: 0x0007F750 File Offset: 0x0007D950
		internal static ResourcePermissionBase CreateFromType(Type type, bool unrestricted)
		{
			return (ResourcePermissionBase)Activator.CreateInstance(type, new object[]
			{
				(!unrestricted) ? PermissionState.None : PermissionState.Unrestricted
			});
		}

		// Token: 0x040018BA RID: 6330
		private const int version = 1;

		// Token: 0x040018BB RID: 6331
		public const string Any = "*";

		// Token: 0x040018BC RID: 6332
		public const string Local = ".";

		// Token: 0x040018BD RID: 6333
		private ArrayList _list;

		// Token: 0x040018BE RID: 6334
		private bool _unrestricted;

		// Token: 0x040018BF RID: 6335
		private Type _type;

		// Token: 0x040018C0 RID: 6336
		private string[] _tags;

		// Token: 0x040018C1 RID: 6337
		private static char[] invalidChars = new char[]
		{
			'\t',
			'\n',
			'\v',
			'\f',
			'\r',
			' ',
			'\\',
			'Š'
		};
	}
}
