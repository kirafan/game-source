﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x0200045D RID: 1117
	[Serializable]
	public class ResourcePermissionBaseEntry
	{
		// Token: 0x06002821 RID: 10273 RVA: 0x0007F788 File Offset: 0x0007D988
		public ResourcePermissionBaseEntry()
		{
			this.permissionAccessPath = new string[0];
		}

		// Token: 0x06002822 RID: 10274 RVA: 0x0007F79C File Offset: 0x0007D99C
		public ResourcePermissionBaseEntry(int permissionAccess, string[] permissionAccessPath)
		{
			if (permissionAccessPath == null)
			{
				throw new ArgumentNullException("permissionAccessPath");
			}
			this.permissionAccess = permissionAccess;
			this.permissionAccessPath = permissionAccessPath;
		}

		// Token: 0x17000B3C RID: 2876
		// (get) Token: 0x06002823 RID: 10275 RVA: 0x0007F7C4 File Offset: 0x0007D9C4
		public int PermissionAccess
		{
			get
			{
				return this.permissionAccess;
			}
		}

		// Token: 0x17000B3D RID: 2877
		// (get) Token: 0x06002824 RID: 10276 RVA: 0x0007F7CC File Offset: 0x0007D9CC
		public string[] PermissionAccessPath
		{
			get
			{
				return this.permissionAccessPath;
			}
		}

		// Token: 0x040018C2 RID: 6338
		private int permissionAccess;

		// Token: 0x040018C3 RID: 6339
		private string[] permissionAccessPath;
	}
}
