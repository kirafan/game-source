﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x0200045E RID: 1118
	[AttributeUsage(AttributeTargets.Assembly | AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Constructor | AttributeTargets.Method, AllowMultiple = true, Inherited = false)]
	[Serializable]
	public sealed class StorePermissionAttribute : CodeAccessSecurityAttribute
	{
		// Token: 0x06002825 RID: 10277 RVA: 0x0007F7D4 File Offset: 0x0007D9D4
		public StorePermissionAttribute(SecurityAction action) : base(action)
		{
			this._flags = StorePermissionFlags.NoFlags;
		}

		// Token: 0x17000B3E RID: 2878
		// (get) Token: 0x06002826 RID: 10278 RVA: 0x0007F7E4 File Offset: 0x0007D9E4
		// (set) Token: 0x06002827 RID: 10279 RVA: 0x0007F7EC File Offset: 0x0007D9EC
		public StorePermissionFlags Flags
		{
			get
			{
				return this._flags;
			}
			set
			{
				if ((value & StorePermissionFlags.AllFlags) != value)
				{
					string message = string.Format(Locale.GetText("Invalid flags {0}"), value);
					throw new ArgumentException(message, "StorePermissionFlags");
				}
				this._flags = value;
			}
		}

		// Token: 0x17000B3F RID: 2879
		// (get) Token: 0x06002828 RID: 10280 RVA: 0x0007F830 File Offset: 0x0007DA30
		// (set) Token: 0x06002829 RID: 10281 RVA: 0x0007F844 File Offset: 0x0007DA44
		public bool AddToStore
		{
			get
			{
				return (this._flags & StorePermissionFlags.AddToStore) != StorePermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= StorePermissionFlags.AddToStore;
				}
				else
				{
					this._flags &= ~StorePermissionFlags.AddToStore;
				}
			}
		}

		// Token: 0x17000B40 RID: 2880
		// (get) Token: 0x0600282A RID: 10282 RVA: 0x0007F870 File Offset: 0x0007DA70
		// (set) Token: 0x0600282B RID: 10283 RVA: 0x0007F880 File Offset: 0x0007DA80
		public bool CreateStore
		{
			get
			{
				return (this._flags & StorePermissionFlags.CreateStore) != StorePermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= StorePermissionFlags.CreateStore;
				}
				else
				{
					this._flags &= ~StorePermissionFlags.CreateStore;
				}
			}
		}

		// Token: 0x17000B41 RID: 2881
		// (get) Token: 0x0600282C RID: 10284 RVA: 0x0007F8B8 File Offset: 0x0007DAB8
		// (set) Token: 0x0600282D RID: 10285 RVA: 0x0007F8C8 File Offset: 0x0007DAC8
		public bool DeleteStore
		{
			get
			{
				return (this._flags & StorePermissionFlags.DeleteStore) != StorePermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= StorePermissionFlags.DeleteStore;
				}
				else
				{
					this._flags &= ~StorePermissionFlags.DeleteStore;
				}
			}
		}

		// Token: 0x17000B42 RID: 2882
		// (get) Token: 0x0600282E RID: 10286 RVA: 0x0007F900 File Offset: 0x0007DB00
		// (set) Token: 0x0600282F RID: 10287 RVA: 0x0007F914 File Offset: 0x0007DB14
		public bool EnumerateCertificates
		{
			get
			{
				return (this._flags & StorePermissionFlags.EnumerateCertificates) != StorePermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= StorePermissionFlags.EnumerateCertificates;
				}
				else
				{
					this._flags &= ~StorePermissionFlags.EnumerateCertificates;
				}
			}
		}

		// Token: 0x17000B43 RID: 2883
		// (get) Token: 0x06002830 RID: 10288 RVA: 0x0007F948 File Offset: 0x0007DB48
		// (set) Token: 0x06002831 RID: 10289 RVA: 0x0007F958 File Offset: 0x0007DB58
		public bool EnumerateStores
		{
			get
			{
				return (this._flags & StorePermissionFlags.EnumerateStores) != StorePermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= StorePermissionFlags.EnumerateStores;
				}
				else
				{
					this._flags &= ~StorePermissionFlags.EnumerateStores;
				}
			}
		}

		// Token: 0x17000B44 RID: 2884
		// (get) Token: 0x06002832 RID: 10290 RVA: 0x0007F990 File Offset: 0x0007DB90
		// (set) Token: 0x06002833 RID: 10291 RVA: 0x0007F9A4 File Offset: 0x0007DBA4
		public bool OpenStore
		{
			get
			{
				return (this._flags & StorePermissionFlags.OpenStore) != StorePermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= StorePermissionFlags.OpenStore;
				}
				else
				{
					this._flags &= ~StorePermissionFlags.OpenStore;
				}
			}
		}

		// Token: 0x17000B45 RID: 2885
		// (get) Token: 0x06002834 RID: 10292 RVA: 0x0007F9D0 File Offset: 0x0007DBD0
		// (set) Token: 0x06002835 RID: 10293 RVA: 0x0007F9E4 File Offset: 0x0007DBE4
		public bool RemoveFromStore
		{
			get
			{
				return (this._flags & StorePermissionFlags.RemoveFromStore) != StorePermissionFlags.NoFlags;
			}
			set
			{
				if (value)
				{
					this._flags |= StorePermissionFlags.RemoveFromStore;
				}
				else
				{
					this._flags &= ~StorePermissionFlags.RemoveFromStore;
				}
			}
		}

		// Token: 0x06002836 RID: 10294 RVA: 0x0007FA10 File Offset: 0x0007DC10
		public override IPermission CreatePermission()
		{
			StorePermission result;
			if (base.Unrestricted)
			{
				result = new StorePermission(PermissionState.Unrestricted);
			}
			else
			{
				result = new StorePermission(this._flags);
			}
			return result;
		}

		// Token: 0x040018C4 RID: 6340
		private StorePermissionFlags _flags;
	}
}
