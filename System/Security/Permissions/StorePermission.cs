﻿using System;

namespace System.Security.Permissions
{
	// Token: 0x0200045F RID: 1119
	[Serializable]
	public sealed class StorePermission : CodeAccessPermission, IUnrestrictedPermission
	{
		// Token: 0x06002837 RID: 10295 RVA: 0x0007FA44 File Offset: 0x0007DC44
		public StorePermission(PermissionState state)
		{
			if (PermissionHelper.CheckPermissionState(state, true) == PermissionState.Unrestricted)
			{
				this._flags = StorePermissionFlags.AllFlags;
			}
			else
			{
				this._flags = StorePermissionFlags.NoFlags;
			}
		}

		// Token: 0x06002838 RID: 10296 RVA: 0x0007FA7C File Offset: 0x0007DC7C
		public StorePermission(StorePermissionFlags flags)
		{
			this.Flags = flags;
		}

		// Token: 0x17000B46 RID: 2886
		// (get) Token: 0x06002839 RID: 10297 RVA: 0x0007FA8C File Offset: 0x0007DC8C
		// (set) Token: 0x0600283A RID: 10298 RVA: 0x0007FA94 File Offset: 0x0007DC94
		public StorePermissionFlags Flags
		{
			get
			{
				return this._flags;
			}
			set
			{
				if (value != StorePermissionFlags.NoFlags && (value & StorePermissionFlags.AllFlags) == StorePermissionFlags.NoFlags)
				{
					string message = string.Format(Locale.GetText("Invalid enum {0}"), value);
					throw new ArgumentException(message, "StorePermissionFlags");
				}
				this._flags = value;
			}
		}

		// Token: 0x0600283B RID: 10299 RVA: 0x0007FADC File Offset: 0x0007DCDC
		public bool IsUnrestricted()
		{
			return this._flags == StorePermissionFlags.AllFlags;
		}

		// Token: 0x0600283C RID: 10300 RVA: 0x0007FAEC File Offset: 0x0007DCEC
		public override IPermission Copy()
		{
			if (this._flags == StorePermissionFlags.NoFlags)
			{
				return null;
			}
			return new StorePermission(this._flags);
		}

		// Token: 0x0600283D RID: 10301 RVA: 0x0007FB08 File Offset: 0x0007DD08
		public override IPermission Intersect(IPermission target)
		{
			StorePermission storePermission = this.Cast(target);
			if (storePermission == null)
			{
				return null;
			}
			if (this.IsUnrestricted() && storePermission.IsUnrestricted())
			{
				return new StorePermission(PermissionState.Unrestricted);
			}
			if (this.IsUnrestricted())
			{
				return storePermission.Copy();
			}
			if (storePermission.IsUnrestricted())
			{
				return this.Copy();
			}
			StorePermissionFlags storePermissionFlags = this._flags & storePermission._flags;
			if (storePermissionFlags == StorePermissionFlags.NoFlags)
			{
				return null;
			}
			return new StorePermission(storePermissionFlags);
		}

		// Token: 0x0600283E RID: 10302 RVA: 0x0007FB84 File Offset: 0x0007DD84
		public override IPermission Union(IPermission target)
		{
			StorePermission storePermission = this.Cast(target);
			if (storePermission == null)
			{
				return this.Copy();
			}
			if (this.IsUnrestricted() || storePermission.IsUnrestricted())
			{
				return new StorePermission(PermissionState.Unrestricted);
			}
			StorePermissionFlags storePermissionFlags = this._flags | storePermission._flags;
			if (storePermissionFlags == StorePermissionFlags.NoFlags)
			{
				return null;
			}
			return new StorePermission(storePermissionFlags);
		}

		// Token: 0x0600283F RID: 10303 RVA: 0x0007FBE0 File Offset: 0x0007DDE0
		public override bool IsSubsetOf(IPermission target)
		{
			StorePermission storePermission = this.Cast(target);
			if (storePermission == null)
			{
				return this._flags == StorePermissionFlags.NoFlags;
			}
			return storePermission.IsUnrestricted() || (!this.IsUnrestricted() && (this._flags & ~storePermission._flags) == StorePermissionFlags.NoFlags);
		}

		// Token: 0x06002840 RID: 10304 RVA: 0x0007FC30 File Offset: 0x0007DE30
		public override void FromXml(SecurityElement e)
		{
			PermissionHelper.CheckSecurityElement(e, "e", 1, 1);
			string text = e.Attribute("Flags");
			if (text == null)
			{
				this._flags = StorePermissionFlags.NoFlags;
			}
			else
			{
				this._flags = (StorePermissionFlags)((int)Enum.Parse(typeof(StorePermissionFlags), text));
			}
		}

		// Token: 0x06002841 RID: 10305 RVA: 0x0007FC84 File Offset: 0x0007DE84
		public override SecurityElement ToXml()
		{
			SecurityElement securityElement = PermissionHelper.Element(typeof(StorePermission), 1);
			if (this.IsUnrestricted())
			{
				securityElement.AddAttribute("Unrestricted", bool.TrueString);
			}
			else
			{
				securityElement.AddAttribute("Flags", this._flags.ToString());
			}
			return securityElement;
		}

		// Token: 0x06002842 RID: 10306 RVA: 0x0007FCE0 File Offset: 0x0007DEE0
		private StorePermission Cast(IPermission target)
		{
			if (target == null)
			{
				return null;
			}
			StorePermission storePermission = target as StorePermission;
			if (storePermission == null)
			{
				PermissionHelper.ThrowInvalidPermission(target, typeof(StorePermission));
			}
			return storePermission;
		}

		// Token: 0x040018C5 RID: 6341
		private const int version = 1;

		// Token: 0x040018C6 RID: 6342
		private StorePermissionFlags _flags;
	}
}
