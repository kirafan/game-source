﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000426 RID: 1062
	[ComVisible(false)]
	public sealed class SemaphoreAccessRule : AccessRule
	{
		// Token: 0x0600266D RID: 9837 RVA: 0x00077518 File Offset: 0x00075718
		public SemaphoreAccessRule(IdentityReference identity, SemaphoreRights semaphoreRights, AccessControlType type) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, type)
		{
			this.semaphoreRights = semaphoreRights;
		}

		// Token: 0x0600266E RID: 9838 RVA: 0x00077530 File Offset: 0x00075730
		public SemaphoreAccessRule(string identity, SemaphoreRights semaphoreRights, AccessControlType type) : base(null, 0, false, InheritanceFlags.None, PropagationFlags.None, type)
		{
			this.semaphoreRights = semaphoreRights;
		}

		// Token: 0x17000AD7 RID: 2775
		// (get) Token: 0x0600266F RID: 9839 RVA: 0x00077548 File Offset: 0x00075748
		public SemaphoreRights SemaphoreRights
		{
			get
			{
				return this.semaphoreRights;
			}
		}

		// Token: 0x04001796 RID: 6038
		private SemaphoreRights semaphoreRights;
	}
}
