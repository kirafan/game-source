﻿using System;
using System.Runtime.InteropServices;

namespace System.Security.AccessControl
{
	// Token: 0x02000428 RID: 1064
	[ComVisible(false)]
	[Flags]
	public enum SemaphoreRights
	{
		// Token: 0x04001799 RID: 6041
		Modify = 2,
		// Token: 0x0400179A RID: 6042
		Delete = 65536,
		// Token: 0x0400179B RID: 6043
		ReadPermissions = 131072,
		// Token: 0x0400179C RID: 6044
		ChangePermissions = 262144,
		// Token: 0x0400179D RID: 6045
		TakeOwnership = 524288,
		// Token: 0x0400179E RID: 6046
		Synchronize = 1048576,
		// Token: 0x0400179F RID: 6047
		FullControl = 2031619
	}
}
