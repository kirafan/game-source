﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000429 RID: 1065
	[ComVisible(false)]
	public sealed class SemaphoreSecurity : NativeObjectSecurity
	{
		// Token: 0x06002672 RID: 9842 RVA: 0x00077570 File Offset: 0x00075770
		public SemaphoreSecurity() : base(false, ResourceType.Unknown)
		{
		}

		// Token: 0x06002673 RID: 9843 RVA: 0x0007757C File Offset: 0x0007577C
		public SemaphoreSecurity(string name, AccessControlSections includesections) : base(false, ResourceType.Unknown, name, includesections)
		{
		}

		// Token: 0x17000AD9 RID: 2777
		// (get) Token: 0x06002674 RID: 9844 RVA: 0x00077588 File Offset: 0x00075788
		public override Type AccessRightType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000ADA RID: 2778
		// (get) Token: 0x06002675 RID: 9845 RVA: 0x00077590 File Offset: 0x00075790
		public override Type AccessRuleType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x17000ADB RID: 2779
		// (get) Token: 0x06002676 RID: 9846 RVA: 0x00077598 File Offset: 0x00075798
		public override Type AuditRuleType
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x06002677 RID: 9847 RVA: 0x000775A0 File Offset: 0x000757A0
		public override AccessRule AccessRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AccessControlType type)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002678 RID: 9848 RVA: 0x000775A8 File Offset: 0x000757A8
		public void AddAccessRule(SemaphoreAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002679 RID: 9849 RVA: 0x000775B0 File Offset: 0x000757B0
		public void AddAuditRule(SemaphoreAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600267A RID: 9850 RVA: 0x000775B8 File Offset: 0x000757B8
		public override AuditRule AuditRuleFactory(IdentityReference identityReference, int accessMask, bool isInherited, InheritanceFlags inheritanceFlags, PropagationFlags propagationFlags, AuditFlags flags)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600267B RID: 9851 RVA: 0x000775C0 File Offset: 0x000757C0
		public bool RemoveAccessRule(SemaphoreAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600267C RID: 9852 RVA: 0x000775C8 File Offset: 0x000757C8
		public void RemoveAccessRuleAll(SemaphoreAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600267D RID: 9853 RVA: 0x000775D0 File Offset: 0x000757D0
		public void RemoveAccessRuleSpecific(SemaphoreAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600267E RID: 9854 RVA: 0x000775D8 File Offset: 0x000757D8
		public bool RemoveAuditRule(SemaphoreAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x0600267F RID: 9855 RVA: 0x000775E0 File Offset: 0x000757E0
		public void RemoveAuditRuleAll(SemaphoreAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002680 RID: 9856 RVA: 0x000775E8 File Offset: 0x000757E8
		public void RemoveAuditRuleSpecific(SemaphoreAuditRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002681 RID: 9857 RVA: 0x000775F0 File Offset: 0x000757F0
		public void ResetAccessRule(SemaphoreAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002682 RID: 9858 RVA: 0x000775F8 File Offset: 0x000757F8
		public void SetAccessRule(SemaphoreAccessRule rule)
		{
			throw new NotImplementedException();
		}

		// Token: 0x06002683 RID: 9859 RVA: 0x00077600 File Offset: 0x00075800
		public void SetAuditRule(SemaphoreAuditRule rule)
		{
			throw new NotImplementedException();
		}
	}
}
