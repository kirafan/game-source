﻿using System;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace System.Security.AccessControl
{
	// Token: 0x02000427 RID: 1063
	[ComVisible(false)]
	public sealed class SemaphoreAuditRule : AuditRule
	{
		// Token: 0x06002670 RID: 9840 RVA: 0x00077550 File Offset: 0x00075750
		public SemaphoreAuditRule(IdentityReference identity, SemaphoreRights semaphoreRights, AuditFlags flags) : base(identity, 0, false, InheritanceFlags.None, PropagationFlags.None, flags)
		{
			this.semaphoreRights = semaphoreRights;
		}

		// Token: 0x17000AD8 RID: 2776
		// (get) Token: 0x06002671 RID: 9841 RVA: 0x00077568 File Offset: 0x00075768
		public SemaphoreRights SemaphoreRights
		{
			get
			{
				return this.semaphoreRights;
			}
		}

		// Token: 0x04001797 RID: 6039
		private SemaphoreRights semaphoreRights;
	}
}
