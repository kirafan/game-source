﻿using System;

namespace System.Security.Authentication
{
	// Token: 0x0200042C RID: 1068
	public enum ExchangeAlgorithmType
	{
		// Token: 0x040017AB RID: 6059
		None,
		// Token: 0x040017AC RID: 6060
		DiffieHellman = 43522,
		// Token: 0x040017AD RID: 6061
		RsaKeyX = 41984,
		// Token: 0x040017AE RID: 6062
		RsaSign = 9216
	}
}
