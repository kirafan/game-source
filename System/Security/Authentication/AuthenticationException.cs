﻿using System;
using System.Runtime.Serialization;

namespace System.Security.Authentication
{
	// Token: 0x0200042A RID: 1066
	[Serializable]
	public class AuthenticationException : SystemException
	{
		// Token: 0x06002684 RID: 9860 RVA: 0x00077608 File Offset: 0x00075808
		public AuthenticationException() : base(Locale.GetText("Authentication exception."))
		{
		}

		// Token: 0x06002685 RID: 9861 RVA: 0x0007761C File Offset: 0x0007581C
		public AuthenticationException(string message) : base(message)
		{
		}

		// Token: 0x06002686 RID: 9862 RVA: 0x00077628 File Offset: 0x00075828
		public AuthenticationException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x06002687 RID: 9863 RVA: 0x00077634 File Offset: 0x00075834
		protected AuthenticationException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
		{
		}
	}
}
