﻿using System;
using System.Runtime.Serialization;

namespace System.Security.Authentication
{
	// Token: 0x0200042F RID: 1071
	[Serializable]
	public class InvalidCredentialException : AuthenticationException
	{
		// Token: 0x06002688 RID: 9864 RVA: 0x00077640 File Offset: 0x00075840
		public InvalidCredentialException() : base(Locale.GetText("Invalid credentials exception."))
		{
		}

		// Token: 0x06002689 RID: 9865 RVA: 0x00077654 File Offset: 0x00075854
		public InvalidCredentialException(string message) : base(message)
		{
		}

		// Token: 0x0600268A RID: 9866 RVA: 0x00077660 File Offset: 0x00075860
		public InvalidCredentialException(string message, Exception innerException) : base(message, innerException)
		{
		}

		// Token: 0x0600268B RID: 9867 RVA: 0x0007766C File Offset: 0x0007586C
		protected InvalidCredentialException(SerializationInfo serializationInfo, StreamingContext streamingContext) : base(serializationInfo, streamingContext)
		{
		}
	}
}
