﻿using System;

namespace System.Security.Authentication
{
	// Token: 0x0200042E RID: 1070
	[Flags]
	public enum SslProtocols
	{
		// Token: 0x040017B4 RID: 6068
		None = 0,
		// Token: 0x040017B5 RID: 6069
		Ssl2 = 12,
		// Token: 0x040017B6 RID: 6070
		Ssl3 = 48,
		// Token: 0x040017B7 RID: 6071
		Tls = 192,
		// Token: 0x040017B8 RID: 6072
		Default = 240
	}
}
