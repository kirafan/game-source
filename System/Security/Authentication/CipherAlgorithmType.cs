﻿using System;

namespace System.Security.Authentication
{
	// Token: 0x0200042B RID: 1067
	public enum CipherAlgorithmType
	{
		// Token: 0x040017A1 RID: 6049
		None,
		// Token: 0x040017A2 RID: 6050
		Aes = 26129,
		// Token: 0x040017A3 RID: 6051
		Aes128 = 26126,
		// Token: 0x040017A4 RID: 6052
		Aes192,
		// Token: 0x040017A5 RID: 6053
		Aes256,
		// Token: 0x040017A6 RID: 6054
		Des = 26113,
		// Token: 0x040017A7 RID: 6055
		Rc2,
		// Token: 0x040017A8 RID: 6056
		Rc4 = 26625,
		// Token: 0x040017A9 RID: 6057
		TripleDes = 26115
	}
}
