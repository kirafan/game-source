﻿using System;

namespace System.Security.Authentication
{
	// Token: 0x0200042D RID: 1069
	public enum HashAlgorithmType
	{
		// Token: 0x040017B0 RID: 6064
		None,
		// Token: 0x040017B1 RID: 6065
		Md5 = 32771,
		// Token: 0x040017B2 RID: 6066
		Sha1
	}
}
