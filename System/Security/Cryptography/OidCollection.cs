﻿using System;
using System.Collections;

namespace System.Security.Cryptography
{
	// Token: 0x02000434 RID: 1076
	public sealed class OidCollection : ICollection, IEnumerable
	{
		// Token: 0x060026B0 RID: 9904 RVA: 0x00077F74 File Offset: 0x00076174
		public OidCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x060026B1 RID: 9905 RVA: 0x00077F88 File Offset: 0x00076188
		void ICollection.CopyTo(Array array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x060026B2 RID: 9906 RVA: 0x00077F98 File Offset: 0x00076198
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new OidEnumerator(this);
		}

		// Token: 0x17000AE4 RID: 2788
		// (get) Token: 0x060026B3 RID: 9907 RVA: 0x00077FA0 File Offset: 0x000761A0
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x17000AE5 RID: 2789
		// (get) Token: 0x060026B4 RID: 9908 RVA: 0x00077FB0 File Offset: 0x000761B0
		public bool IsSynchronized
		{
			get
			{
				return this._list.IsSynchronized;
			}
		}

		// Token: 0x17000AE6 RID: 2790
		public Oid this[int index]
		{
			get
			{
				return (Oid)this._list[index];
			}
		}

		// Token: 0x17000AE7 RID: 2791
		public Oid this[string oid]
		{
			get
			{
				foreach (object obj in this._list)
				{
					Oid oid2 = (Oid)obj;
					if (oid2.Value == oid)
					{
						return oid2;
					}
				}
				return null;
			}
		}

		// Token: 0x17000AE8 RID: 2792
		// (get) Token: 0x060026B7 RID: 9911 RVA: 0x00078058 File Offset: 0x00076258
		public object SyncRoot
		{
			get
			{
				return this._list.SyncRoot;
			}
		}

		// Token: 0x060026B8 RID: 9912 RVA: 0x00078068 File Offset: 0x00076268
		public int Add(Oid oid)
		{
			return (!this._readOnly) ? this._list.Add(oid) : 0;
		}

		// Token: 0x060026B9 RID: 9913 RVA: 0x00078088 File Offset: 0x00076288
		public void CopyTo(Oid[] array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x060026BA RID: 9914 RVA: 0x00078098 File Offset: 0x00076298
		public OidEnumerator GetEnumerator()
		{
			return new OidEnumerator(this);
		}

		// Token: 0x17000AE9 RID: 2793
		// (get) Token: 0x060026BB RID: 9915 RVA: 0x000780A0 File Offset: 0x000762A0
		// (set) Token: 0x060026BC RID: 9916 RVA: 0x000780A8 File Offset: 0x000762A8
		internal bool ReadOnly
		{
			get
			{
				return this._readOnly;
			}
			set
			{
				this._readOnly = value;
			}
		}

		// Token: 0x060026BD RID: 9917 RVA: 0x000780B4 File Offset: 0x000762B4
		internal OidCollection ReadOnlyCopy()
		{
			OidCollection oidCollection = new OidCollection();
			foreach (object obj in this._list)
			{
				Oid oid = (Oid)obj;
				oidCollection.Add(oid);
			}
			oidCollection._readOnly = true;
			return oidCollection;
		}

		// Token: 0x040017C6 RID: 6086
		private ArrayList _list;

		// Token: 0x040017C7 RID: 6087
		private bool _readOnly;
	}
}
