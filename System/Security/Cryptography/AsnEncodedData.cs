﻿using System;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using Mono.Security;
using Mono.Security.Cryptography;

namespace System.Security.Cryptography
{
	// Token: 0x02000432 RID: 1074
	public class AsnEncodedData
	{
		// Token: 0x06002698 RID: 9880 RVA: 0x00077740 File Offset: 0x00075940
		protected AsnEncodedData()
		{
		}

		// Token: 0x06002699 RID: 9881 RVA: 0x00077748 File Offset: 0x00075948
		public AsnEncodedData(string oid, byte[] rawData)
		{
			this._oid = new Oid(oid);
			this.RawData = rawData;
		}

		// Token: 0x0600269A RID: 9882 RVA: 0x00077764 File Offset: 0x00075964
		public AsnEncodedData(Oid oid, byte[] rawData)
		{
			this.Oid = oid;
			this.RawData = rawData;
		}

		// Token: 0x0600269B RID: 9883 RVA: 0x0007777C File Offset: 0x0007597C
		public AsnEncodedData(AsnEncodedData asnEncodedData)
		{
			if (asnEncodedData == null)
			{
				throw new ArgumentNullException("asnEncodedData");
			}
			if (asnEncodedData._oid != null)
			{
				this.Oid = new Oid(asnEncodedData._oid);
			}
			this.RawData = asnEncodedData._raw;
		}

		// Token: 0x0600269C RID: 9884 RVA: 0x000777C8 File Offset: 0x000759C8
		public AsnEncodedData(byte[] rawData)
		{
			this.RawData = rawData;
		}

		// Token: 0x17000AE0 RID: 2784
		// (get) Token: 0x0600269D RID: 9885 RVA: 0x000777D8 File Offset: 0x000759D8
		// (set) Token: 0x0600269E RID: 9886 RVA: 0x000777E0 File Offset: 0x000759E0
		public Oid Oid
		{
			get
			{
				return this._oid;
			}
			set
			{
				if (value == null)
				{
					this._oid = null;
				}
				else
				{
					this._oid = new Oid(value);
				}
			}
		}

		// Token: 0x17000AE1 RID: 2785
		// (get) Token: 0x0600269F RID: 9887 RVA: 0x00077800 File Offset: 0x00075A00
		// (set) Token: 0x060026A0 RID: 9888 RVA: 0x00077808 File Offset: 0x00075A08
		public byte[] RawData
		{
			get
			{
				return this._raw;
			}
			set
			{
				if (value == null)
				{
					throw new ArgumentNullException("RawData");
				}
				this._raw = (byte[])value.Clone();
			}
		}

		// Token: 0x060026A1 RID: 9889 RVA: 0x00077838 File Offset: 0x00075A38
		public virtual void CopyFrom(AsnEncodedData asnEncodedData)
		{
			if (asnEncodedData == null)
			{
				throw new ArgumentNullException("asnEncodedData");
			}
			if (asnEncodedData._oid == null)
			{
				this.Oid = null;
			}
			else
			{
				this.Oid = new Oid(asnEncodedData._oid);
			}
			this.RawData = asnEncodedData._raw;
		}

		// Token: 0x060026A2 RID: 9890 RVA: 0x0007788C File Offset: 0x00075A8C
		public virtual string Format(bool multiLine)
		{
			if (this._raw == null)
			{
				return string.Empty;
			}
			if (this._oid == null)
			{
				return this.Default(multiLine);
			}
			return this.ToString(multiLine);
		}

		// Token: 0x060026A3 RID: 9891 RVA: 0x000778BC File Offset: 0x00075ABC
		internal virtual string ToString(bool multiLine)
		{
			string value = this._oid.Value;
			switch (value)
			{
			case "2.5.29.19":
				return this.BasicConstraintsExtension(multiLine);
			case "2.5.29.37":
				return this.EnhancedKeyUsageExtension(multiLine);
			case "2.5.29.15":
				return this.KeyUsageExtension(multiLine);
			case "2.5.29.14":
				return this.SubjectKeyIdentifierExtension(multiLine);
			case "2.5.29.17":
				return this.SubjectAltName(multiLine);
			case "2.16.840.1.113730.1.1":
				return this.NetscapeCertType(multiLine);
			}
			return this.Default(multiLine);
		}

		// Token: 0x060026A4 RID: 9892 RVA: 0x000779A8 File Offset: 0x00075BA8
		internal string Default(bool multiLine)
		{
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < this._raw.Length; i++)
			{
				stringBuilder.Append(this._raw[i].ToString("x2"));
				if (i != this._raw.Length - 1)
				{
					stringBuilder.Append(" ");
				}
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060026A5 RID: 9893 RVA: 0x00077A14 File Offset: 0x00075C14
		internal string BasicConstraintsExtension(bool multiLine)
		{
			string result;
			try
			{
				System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension x509BasicConstraintsExtension = new System.Security.Cryptography.X509Certificates.X509BasicConstraintsExtension(this, false);
				result = x509BasicConstraintsExtension.ToString(multiLine);
			}
			catch
			{
				result = string.Empty;
			}
			return result;
		}

		// Token: 0x060026A6 RID: 9894 RVA: 0x00077A6C File Offset: 0x00075C6C
		internal string EnhancedKeyUsageExtension(bool multiLine)
		{
			string result;
			try
			{
				System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension x509EnhancedKeyUsageExtension = new System.Security.Cryptography.X509Certificates.X509EnhancedKeyUsageExtension(this, false);
				result = x509EnhancedKeyUsageExtension.ToString(multiLine);
			}
			catch
			{
				result = string.Empty;
			}
			return result;
		}

		// Token: 0x060026A7 RID: 9895 RVA: 0x00077AC4 File Offset: 0x00075CC4
		internal string KeyUsageExtension(bool multiLine)
		{
			string result;
			try
			{
				System.Security.Cryptography.X509Certificates.X509KeyUsageExtension x509KeyUsageExtension = new System.Security.Cryptography.X509Certificates.X509KeyUsageExtension(this, false);
				result = x509KeyUsageExtension.ToString(multiLine);
			}
			catch
			{
				result = string.Empty;
			}
			return result;
		}

		// Token: 0x060026A8 RID: 9896 RVA: 0x00077B1C File Offset: 0x00075D1C
		internal string SubjectKeyIdentifierExtension(bool multiLine)
		{
			string result;
			try
			{
				System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension x509SubjectKeyIdentifierExtension = new System.Security.Cryptography.X509Certificates.X509SubjectKeyIdentifierExtension(this, false);
				result = x509SubjectKeyIdentifierExtension.ToString(multiLine);
			}
			catch
			{
				result = string.Empty;
			}
			return result;
		}

		// Token: 0x060026A9 RID: 9897 RVA: 0x00077B74 File Offset: 0x00075D74
		internal string SubjectAltName(bool multiLine)
		{
			if (this._raw.Length < 5)
			{
				return "Information Not Available";
			}
			string result;
			try
			{
				ASN1 asn = new ASN1(this._raw);
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 0; i < asn.Count; i++)
				{
					ASN1 asn2 = asn[i];
					byte tag = asn2.Tag;
					string value;
					string value2;
					if (tag != 129)
					{
						if (tag != 130)
						{
							value = string.Format("Unknown ({0})=", asn2.Tag);
							value2 = CryptoConvert.ToHex(asn2.Value);
						}
						else
						{
							value = "DNS Name=";
							value2 = Encoding.ASCII.GetString(asn2.Value);
						}
					}
					else
					{
						value = "RFC822 Name=";
						value2 = Encoding.ASCII.GetString(asn2.Value);
					}
					stringBuilder.Append(value);
					stringBuilder.Append(value2);
					if (multiLine)
					{
						stringBuilder.Append(Environment.NewLine);
					}
					else if (i < asn.Count - 1)
					{
						stringBuilder.Append(", ");
					}
				}
				result = stringBuilder.ToString();
			}
			catch
			{
				result = string.Empty;
			}
			return result;
		}

		// Token: 0x060026AA RID: 9898 RVA: 0x00077CDC File Offset: 0x00075EDC
		internal string NetscapeCertType(bool multiLine)
		{
			if (this._raw.Length < 4 || this._raw[0] != 3 || this._raw[1] != 2)
			{
				return "Information Not Available";
			}
			int num = this._raw[3] >> (int)this._raw[2] << (int)this._raw[2];
			StringBuilder stringBuilder = new StringBuilder();
			if ((num & 128) == 128)
			{
				stringBuilder.Append("SSL Client Authentication");
			}
			if ((num & 64) == 64)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append("SSL Server Authentication");
			}
			if ((num & 32) == 32)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append("SMIME");
			}
			if ((num & 16) == 16)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append("Signature");
			}
			if ((num & 8) == 8)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append("Unknown cert type");
			}
			if ((num & 4) == 4)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append("SSL CA");
			}
			if ((num & 2) == 2)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append("SMIME CA");
			}
			if ((num & 1) == 1)
			{
				if (stringBuilder.Length > 0)
				{
					stringBuilder.Append(", ");
				}
				stringBuilder.Append("Signature CA");
			}
			stringBuilder.AppendFormat(" ({0})", num.ToString("x2"));
			return stringBuilder.ToString();
		}

		// Token: 0x040017C1 RID: 6081
		internal Oid _oid;

		// Token: 0x040017C2 RID: 6082
		internal byte[] _raw;
	}
}
