﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200044B RID: 1099
	[Flags]
	public enum X509ChainStatusFlags
	{
		// Token: 0x04001849 RID: 6217
		NoError = 0,
		// Token: 0x0400184A RID: 6218
		NotTimeValid = 1,
		// Token: 0x0400184B RID: 6219
		NotTimeNested = 2,
		// Token: 0x0400184C RID: 6220
		Revoked = 4,
		// Token: 0x0400184D RID: 6221
		NotSignatureValid = 8,
		// Token: 0x0400184E RID: 6222
		NotValidForUsage = 16,
		// Token: 0x0400184F RID: 6223
		UntrustedRoot = 32,
		// Token: 0x04001850 RID: 6224
		RevocationStatusUnknown = 64,
		// Token: 0x04001851 RID: 6225
		Cyclic = 128,
		// Token: 0x04001852 RID: 6226
		InvalidExtension = 256,
		// Token: 0x04001853 RID: 6227
		InvalidPolicyConstraints = 512,
		// Token: 0x04001854 RID: 6228
		InvalidBasicConstraints = 1024,
		// Token: 0x04001855 RID: 6229
		InvalidNameConstraints = 2048,
		// Token: 0x04001856 RID: 6230
		HasNotSupportedNameConstraint = 4096,
		// Token: 0x04001857 RID: 6231
		HasNotDefinedNameConstraint = 8192,
		// Token: 0x04001858 RID: 6232
		HasNotPermittedNameConstraint = 16384,
		// Token: 0x04001859 RID: 6233
		HasExcludedNameConstraint = 32768,
		// Token: 0x0400185A RID: 6234
		PartialChain = 65536,
		// Token: 0x0400185B RID: 6235
		CtlNotTimeValid = 131072,
		// Token: 0x0400185C RID: 6236
		CtlNotSignatureValid = 262144,
		// Token: 0x0400185D RID: 6237
		CtlNotValidForUsage = 524288,
		// Token: 0x0400185E RID: 6238
		OfflineRevocation = 16777216,
		// Token: 0x0400185F RID: 6239
		NoIssuanceChainPolicy = 33554432
	}
}
