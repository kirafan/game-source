﻿using System;
using System.Collections;
using Mono.Security;
using Mono.Security.X509;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200044D RID: 1101
	public sealed class X509ExtensionCollection : ICollection, IEnumerable
	{
		// Token: 0x060027BB RID: 10171 RVA: 0x0007D300 File Offset: 0x0007B500
		public X509ExtensionCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x060027BC RID: 10172 RVA: 0x0007D314 File Offset: 0x0007B514
		internal X509ExtensionCollection(X509Certificate cert)
		{
			this._list = new ArrayList(cert.Extensions.Count);
			if (cert.Extensions.Count == 0)
			{
				return;
			}
			object[] array = new object[2];
			foreach (object obj in cert.Extensions)
			{
				X509Extension x509Extension = (X509Extension)obj;
				bool critical = x509Extension.Critical;
				string oid = x509Extension.Oid;
				byte[] rawData = null;
				ASN1 value = x509Extension.Value;
				if (value.Tag == 4 && value.Count > 0)
				{
					rawData = value[0].GetBytes();
				}
				array[0] = new AsnEncodedData(oid, rawData);
				array[1] = critical;
				X509Extension x509Extension2 = (X509Extension)CryptoConfig.CreateFromName(oid, array);
				if (x509Extension2 == null)
				{
					x509Extension2 = new X509Extension(oid, rawData, critical);
				}
				this._list.Add(x509Extension2);
			}
		}

		// Token: 0x060027BD RID: 10173 RVA: 0x0007D43C File Offset: 0x0007B63C
		void ICollection.CopyTo(Array array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("negative index");
			}
			if (index >= array.Length)
			{
				throw new ArgumentOutOfRangeException("index >= array.Length");
			}
			this._list.CopyTo(array, index);
		}

		// Token: 0x060027BE RID: 10174 RVA: 0x0007D490 File Offset: 0x0007B690
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new X509ExtensionEnumerator(this._list);
		}

		// Token: 0x17000B28 RID: 2856
		// (get) Token: 0x060027BF RID: 10175 RVA: 0x0007D4A0 File Offset: 0x0007B6A0
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x17000B29 RID: 2857
		// (get) Token: 0x060027C0 RID: 10176 RVA: 0x0007D4B0 File Offset: 0x0007B6B0
		public bool IsSynchronized
		{
			get
			{
				return this._list.IsSynchronized;
			}
		}

		// Token: 0x17000B2A RID: 2858
		// (get) Token: 0x060027C1 RID: 10177 RVA: 0x0007D4C0 File Offset: 0x0007B6C0
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x17000B2B RID: 2859
		public X509Extension this[int index]
		{
			get
			{
				if (index < 0)
				{
					throw new InvalidOperationException("index");
				}
				return (X509Extension)this._list[index];
			}
		}

		// Token: 0x17000B2C RID: 2860
		public X509Extension this[string oid]
		{
			get
			{
				if (oid == null)
				{
					throw new ArgumentNullException("oid");
				}
				if (this._list.Count == 0 || oid.Length == 0)
				{
					return null;
				}
				foreach (object obj in this._list)
				{
					X509Extension x509Extension = (X509Extension)obj;
					if (x509Extension.Oid.Value.Equals(oid))
					{
						return x509Extension;
					}
				}
				return null;
			}
		}

		// Token: 0x060027C4 RID: 10180 RVA: 0x0007D5A4 File Offset: 0x0007B7A4
		public int Add(X509Extension extension)
		{
			if (extension == null)
			{
				throw new ArgumentNullException("extension");
			}
			return this._list.Add(extension);
		}

		// Token: 0x060027C5 RID: 10181 RVA: 0x0007D5C4 File Offset: 0x0007B7C4
		public void CopyTo(X509Extension[] array, int index)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array");
			}
			if (index < 0)
			{
				throw new ArgumentOutOfRangeException("negative index");
			}
			if (index >= array.Length)
			{
				throw new ArgumentOutOfRangeException("index >= array.Length");
			}
			this._list.CopyTo(array, index);
		}

		// Token: 0x060027C6 RID: 10182 RVA: 0x0007D618 File Offset: 0x0007B818
		public X509ExtensionEnumerator GetEnumerator()
		{
			return new X509ExtensionEnumerator(this._list);
		}

		// Token: 0x04001865 RID: 6245
		private ArrayList _list;
	}
}
