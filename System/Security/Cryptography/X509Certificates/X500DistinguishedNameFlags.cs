﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200043E RID: 1086
	[Flags]
	public enum X500DistinguishedNameFlags
	{
		// Token: 0x0400180A RID: 6154
		None = 0,
		// Token: 0x0400180B RID: 6155
		Reversed = 1,
		// Token: 0x0400180C RID: 6156
		UseSemicolons = 16,
		// Token: 0x0400180D RID: 6157
		DoNotUsePlusSign = 32,
		// Token: 0x0400180E RID: 6158
		DoNotUseQuotes = 64,
		// Token: 0x0400180F RID: 6159
		UseCommas = 128,
		// Token: 0x04001810 RID: 6160
		UseNewLines = 256,
		// Token: 0x04001811 RID: 6161
		UseUTF8Encoding = 4096,
		// Token: 0x04001812 RID: 6162
		UseT61Encoding = 8192,
		// Token: 0x04001813 RID: 6163
		ForceUTF8Encoding = 16384
	}
}
