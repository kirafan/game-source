﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000453 RID: 1107
	[Flags]
	public enum X509KeyUsageFlags
	{
		// Token: 0x04001883 RID: 6275
		None = 0,
		// Token: 0x04001884 RID: 6276
		EncipherOnly = 1,
		// Token: 0x04001885 RID: 6277
		CrlSign = 2,
		// Token: 0x04001886 RID: 6278
		KeyCertSign = 4,
		// Token: 0x04001887 RID: 6279
		KeyAgreement = 8,
		// Token: 0x04001888 RID: 6280
		DataEncipherment = 16,
		// Token: 0x04001889 RID: 6281
		KeyEncipherment = 32,
		// Token: 0x0400188A RID: 6282
		NonRepudiation = 64,
		// Token: 0x0400188B RID: 6283
		DigitalSignature = 128,
		// Token: 0x0400188C RID: 6284
		DecipherOnly = 32768
	}
}
