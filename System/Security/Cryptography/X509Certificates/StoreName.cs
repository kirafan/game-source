﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200043C RID: 1084
	public enum StoreName
	{
		// Token: 0x040017FF RID: 6143
		AddressBook = 1,
		// Token: 0x04001800 RID: 6144
		AuthRoot,
		// Token: 0x04001801 RID: 6145
		CertificateAuthority,
		// Token: 0x04001802 RID: 6146
		Disallowed,
		// Token: 0x04001803 RID: 6147
		My,
		// Token: 0x04001804 RID: 6148
		Root,
		// Token: 0x04001805 RID: 6149
		TrustedPeople,
		// Token: 0x04001806 RID: 6150
		TrustedPublisher
	}
}
