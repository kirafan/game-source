﻿using System;
using System.Collections;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000442 RID: 1090
	public sealed class X509Certificate2Enumerator : IEnumerator
	{
		// Token: 0x06002742 RID: 10050 RVA: 0x0007B088 File Offset: 0x00079288
		internal X509Certificate2Enumerator(X509Certificate2Collection collection)
		{
			this.enumerator = ((IEnumerable)collection).GetEnumerator();
		}

		// Token: 0x17000B07 RID: 2823
		// (get) Token: 0x06002743 RID: 10051 RVA: 0x0007B09C File Offset: 0x0007929C
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		// Token: 0x06002744 RID: 10052 RVA: 0x0007B0AC File Offset: 0x000792AC
		bool IEnumerator.MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		// Token: 0x06002745 RID: 10053 RVA: 0x0007B0BC File Offset: 0x000792BC
		void IEnumerator.Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x17000B08 RID: 2824
		// (get) Token: 0x06002746 RID: 10054 RVA: 0x0007B0CC File Offset: 0x000792CC
		public X509Certificate2 Current
		{
			get
			{
				return (X509Certificate2)this.enumerator.Current;
			}
		}

		// Token: 0x06002747 RID: 10055 RVA: 0x0007B0E0 File Offset: 0x000792E0
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		// Token: 0x06002748 RID: 10056 RVA: 0x0007B0F0 File Offset: 0x000792F0
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x04001827 RID: 6183
		private IEnumerator enumerator;
	}
}
