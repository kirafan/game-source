﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000451 RID: 1105
	public enum X509IncludeOption
	{
		// Token: 0x04001879 RID: 6265
		None,
		// Token: 0x0400187A RID: 6266
		ExcludeRoot,
		// Token: 0x0400187B RID: 6267
		EndCertOnly,
		// Token: 0x0400187C RID: 6268
		WholeChain
	}
}
