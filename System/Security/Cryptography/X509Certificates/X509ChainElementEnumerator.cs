﻿using System;
using System.Collections;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000448 RID: 1096
	public sealed class X509ChainElementEnumerator : IEnumerator
	{
		// Token: 0x06002799 RID: 10137 RVA: 0x0007CBA0 File Offset: 0x0007ADA0
		internal X509ChainElementEnumerator(IEnumerable enumerable)
		{
			this.enumerator = enumerable.GetEnumerator();
		}

		// Token: 0x17000B1B RID: 2843
		// (get) Token: 0x0600279A RID: 10138 RVA: 0x0007CBB4 File Offset: 0x0007ADB4
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		// Token: 0x17000B1C RID: 2844
		// (get) Token: 0x0600279B RID: 10139 RVA: 0x0007CBC4 File Offset: 0x0007ADC4
		public X509ChainElement Current
		{
			get
			{
				return (X509ChainElement)this.enumerator.Current;
			}
		}

		// Token: 0x0600279C RID: 10140 RVA: 0x0007CBD8 File Offset: 0x0007ADD8
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		// Token: 0x0600279D RID: 10141 RVA: 0x0007CBE8 File Offset: 0x0007ADE8
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x0400183D RID: 6205
		private IEnumerator enumerator;
	}
}
