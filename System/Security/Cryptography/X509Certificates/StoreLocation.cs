﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200043B RID: 1083
	public enum StoreLocation
	{
		// Token: 0x040017FC RID: 6140
		CurrentUser = 1,
		// Token: 0x040017FD RID: 6141
		LocalMachine
	}
}
