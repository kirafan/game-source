﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000437 RID: 1079
	[Flags]
	public enum OpenFlags
	{
		// Token: 0x040017E3 RID: 6115
		ReadOnly = 0,
		// Token: 0x040017E4 RID: 6116
		ReadWrite = 1,
		// Token: 0x040017E5 RID: 6117
		MaxAllowed = 2,
		// Token: 0x040017E6 RID: 6118
		OpenExistingOnly = 4,
		// Token: 0x040017E7 RID: 6119
		IncludeArchived = 8
	}
}
