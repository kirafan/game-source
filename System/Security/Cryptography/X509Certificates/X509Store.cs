﻿using System;
using System.Collections.Generic;
using System.Security.Permissions;
using Mono.Security.X509;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000457 RID: 1111
	public sealed class X509Store
	{
		// Token: 0x060027DD RID: 10205 RVA: 0x0007DD9C File Offset: 0x0007BF9C
		public X509Store() : this("MY", StoreLocation.CurrentUser)
		{
		}

		// Token: 0x060027DE RID: 10206 RVA: 0x0007DDAC File Offset: 0x0007BFAC
		public X509Store(string storeName) : this(storeName, StoreLocation.CurrentUser)
		{
		}

		// Token: 0x060027DF RID: 10207 RVA: 0x0007DDB8 File Offset: 0x0007BFB8
		public X509Store(StoreName storeName) : this(storeName, StoreLocation.CurrentUser)
		{
		}

		// Token: 0x060027E0 RID: 10208 RVA: 0x0007DDC4 File Offset: 0x0007BFC4
		public X509Store(StoreLocation storeLocation) : this("MY", storeLocation)
		{
		}

		// Token: 0x060027E1 RID: 10209 RVA: 0x0007DDD4 File Offset: 0x0007BFD4
		public X509Store(StoreName storeName, StoreLocation storeLocation)
		{
			if (storeName < StoreName.AddressBook || storeName > StoreName.TrustedPublisher)
			{
				throw new ArgumentException("storeName");
			}
			if (storeLocation < StoreLocation.CurrentUser || storeLocation > StoreLocation.LocalMachine)
			{
				throw new ArgumentException("storeLocation");
			}
			if (storeName != StoreName.CertificateAuthority)
			{
				this._name = storeName.ToString();
			}
			else
			{
				this._name = "CA";
			}
			this._location = storeLocation;
		}

		// Token: 0x060027E2 RID: 10210 RVA: 0x0007DE54 File Offset: 0x0007C054
		[MonoTODO("Mono's stores are fully managed. All handles are invalid.")]
		[PermissionSet(SecurityAction.LinkDemand, XML = "<PermissionSet class=\"System.Security.PermissionSet\"\nversion=\"1\">\n<IPermission class=\"System.Security.Permissions.SecurityPermission, mscorlib, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089\"\nversion=\"1\"\nFlags=\"UnmanagedCode\"/>\n</PermissionSet>\n")]
		public X509Store(IntPtr storeHandle)
		{
			if (storeHandle == IntPtr.Zero)
			{
				throw new ArgumentNullException("storeHandle");
			}
			throw new CryptographicException("Invalid handle.");
		}

		// Token: 0x060027E3 RID: 10211 RVA: 0x0007DE84 File Offset: 0x0007C084
		public X509Store(string storeName, StoreLocation storeLocation)
		{
			if (storeLocation < StoreLocation.CurrentUser || storeLocation > StoreLocation.LocalMachine)
			{
				throw new ArgumentException("storeLocation");
			}
			this._name = storeName;
			this._location = storeLocation;
		}

		// Token: 0x17000B31 RID: 2865
		// (get) Token: 0x060027E4 RID: 10212 RVA: 0x0007DEB4 File Offset: 0x0007C0B4
		public X509Certificate2Collection Certificates
		{
			get
			{
				if (this.list == null)
				{
					this.list = new X509Certificate2Collection();
				}
				else if (this.store == null)
				{
					this.list.Clear();
				}
				return this.list;
			}
		}

		// Token: 0x17000B32 RID: 2866
		// (get) Token: 0x060027E5 RID: 10213 RVA: 0x0007DEF0 File Offset: 0x0007C0F0
		public StoreLocation Location
		{
			get
			{
				return this._location;
			}
		}

		// Token: 0x17000B33 RID: 2867
		// (get) Token: 0x060027E6 RID: 10214 RVA: 0x0007DEF8 File Offset: 0x0007C0F8
		public string Name
		{
			get
			{
				return this._name;
			}
		}

		// Token: 0x17000B34 RID: 2868
		// (get) Token: 0x060027E7 RID: 10215 RVA: 0x0007DF00 File Offset: 0x0007C100
		private X509Stores Factory
		{
			get
			{
				if (this._location == StoreLocation.CurrentUser)
				{
					return X509StoreManager.CurrentUser;
				}
				return X509StoreManager.LocalMachine;
			}
		}

		// Token: 0x17000B35 RID: 2869
		// (get) Token: 0x060027E8 RID: 10216 RVA: 0x0007DF1C File Offset: 0x0007C11C
		private bool IsOpen
		{
			get
			{
				return this.store != null;
			}
		}

		// Token: 0x17000B36 RID: 2870
		// (get) Token: 0x060027E9 RID: 10217 RVA: 0x0007DF2C File Offset: 0x0007C12C
		private bool IsReadOnly
		{
			get
			{
				return Environment.UnityWebSecurityEnabled || (this._flags & OpenFlags.ReadWrite) == OpenFlags.ReadOnly;
			}
		}

		// Token: 0x17000B37 RID: 2871
		// (get) Token: 0x060027EA RID: 10218 RVA: 0x0007DF48 File Offset: 0x0007C148
		internal X509Store Store
		{
			get
			{
				return this.store;
			}
		}

		// Token: 0x17000B38 RID: 2872
		// (get) Token: 0x060027EB RID: 10219 RVA: 0x0007DF50 File Offset: 0x0007C150
		[MonoTODO("Mono's stores are fully managed. Always returns IntPtr.Zero.")]
		public IntPtr StoreHandle
		{
			get
			{
				return IntPtr.Zero;
			}
		}

		// Token: 0x060027EC RID: 10220 RVA: 0x0007DF58 File Offset: 0x0007C158
		public void Add(X509Certificate2 certificate)
		{
			if (certificate == null)
			{
				throw new ArgumentNullException("certificate");
			}
			if (!this.IsOpen)
			{
				throw new CryptographicException(Locale.GetText("Store isn't opened."));
			}
			if (this.IsReadOnly)
			{
				throw new CryptographicException(Locale.GetText("Store is read-only."));
			}
			if (!this.Exists(certificate))
			{
				try
				{
					this.store.Import(new X509Certificate(certificate.RawData));
				}
				finally
				{
					this.Certificates.Add(certificate);
				}
			}
		}

		// Token: 0x060027ED RID: 10221 RVA: 0x0007E000 File Offset: 0x0007C200
		[MonoTODO("Method isn't transactional (like documented)")]
		public void AddRange(X509Certificate2Collection certificates)
		{
			if (certificates == null)
			{
				throw new ArgumentNullException("certificates");
			}
			if (certificates.Count == 0)
			{
				return;
			}
			if (!this.IsOpen)
			{
				throw new CryptographicException(Locale.GetText("Store isn't opened."));
			}
			if (this.IsReadOnly)
			{
				throw new CryptographicException(Locale.GetText("Store is read-only."));
			}
			foreach (X509Certificate2 x509Certificate in certificates)
			{
				if (!this.Exists(x509Certificate))
				{
					try
					{
						this.store.Import(new X509Certificate(x509Certificate.RawData));
					}
					finally
					{
						this.Certificates.Add(x509Certificate);
					}
				}
			}
		}

		// Token: 0x060027EE RID: 10222 RVA: 0x0007E0D0 File Offset: 0x0007C2D0
		public void Close()
		{
			this.store = null;
			if (this.list != null)
			{
				this.list.Clear();
			}
		}

		// Token: 0x060027EF RID: 10223 RVA: 0x0007E0F0 File Offset: 0x0007C2F0
		public void Open(OpenFlags flags)
		{
			if (string.IsNullOrEmpty(this._name))
			{
				throw new CryptographicException(Locale.GetText("Invalid store name (null or empty)."));
			}
			string name = this._name;
			string storeName;
			if (name != null)
			{
				if (X509Store.<>f__switch$map1B == null)
				{
					X509Store.<>f__switch$map1B = new Dictionary<string, int>(1)
					{
						{
							"Root",
							0
						}
					};
				}
				int num;
				if (X509Store.<>f__switch$map1B.TryGetValue(name, out num))
				{
					if (num == 0)
					{
						storeName = "Trust";
						goto IL_8B;
					}
				}
			}
			storeName = this._name;
			IL_8B:
			bool create = (flags & OpenFlags.OpenExistingOnly) != OpenFlags.OpenExistingOnly;
			this.store = this.Factory.Open(storeName, create);
			if (this.store == null)
			{
				throw new CryptographicException(Locale.GetText("Store {0} doesn't exists.", new object[]
				{
					this._name
				}));
			}
			this._flags = flags;
			foreach (X509Certificate x509Certificate in this.store.Certificates)
			{
				this.Certificates.Add(new X509Certificate2(x509Certificate.RawData));
			}
		}

		// Token: 0x060027F0 RID: 10224 RVA: 0x0007E24C File Offset: 0x0007C44C
		public void Remove(X509Certificate2 certificate)
		{
			if (certificate == null)
			{
				throw new ArgumentNullException("certificate");
			}
			if (!this.IsOpen)
			{
				throw new CryptographicException(Locale.GetText("Store isn't opened."));
			}
			if (!this.Exists(certificate))
			{
				return;
			}
			if (this.IsReadOnly)
			{
				throw new CryptographicException(Locale.GetText("Store is read-only."));
			}
			try
			{
				this.store.Remove(new X509Certificate(certificate.RawData));
			}
			finally
			{
				this.Certificates.Remove(certificate);
			}
		}

		// Token: 0x060027F1 RID: 10225 RVA: 0x0007E2F4 File Offset: 0x0007C4F4
		[MonoTODO("Method isn't transactional (like documented)")]
		public void RemoveRange(X509Certificate2Collection certificates)
		{
			if (certificates == null)
			{
				throw new ArgumentNullException("certificates");
			}
			if (certificates.Count == 0)
			{
				return;
			}
			if (!this.IsOpen)
			{
				throw new CryptographicException(Locale.GetText("Store isn't opened."));
			}
			bool flag = false;
			foreach (X509Certificate2 certificate in certificates)
			{
				if (this.Exists(certificate))
				{
					flag = true;
				}
			}
			if (!flag)
			{
				return;
			}
			if (this.IsReadOnly)
			{
				throw new CryptographicException(Locale.GetText("Store is read-only."));
			}
			try
			{
				foreach (X509Certificate2 x509Certificate in certificates)
				{
					this.store.Remove(new X509Certificate(x509Certificate.RawData));
				}
			}
			finally
			{
				this.Certificates.RemoveRange(certificates);
			}
		}

		// Token: 0x060027F2 RID: 10226 RVA: 0x0007E3F0 File Offset: 0x0007C5F0
		private bool Exists(X509Certificate2 certificate)
		{
			if (this.store == null || this.list == null || certificate == null)
			{
				return false;
			}
			foreach (X509Certificate2 other in this.list)
			{
				if (certificate.Equals(other))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x0400189C RID: 6300
		private string _name;

		// Token: 0x0400189D RID: 6301
		private StoreLocation _location;

		// Token: 0x0400189E RID: 6302
		private X509Certificate2Collection list;

		// Token: 0x0400189F RID: 6303
		private OpenFlags _flags;

		// Token: 0x040018A0 RID: 6304
		private X509Store store;
	}
}
