﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200045A RID: 1114
	[Flags]
	public enum X509VerificationFlags
	{
		// Token: 0x040018AC RID: 6316
		NoFlag = 0,
		// Token: 0x040018AD RID: 6317
		IgnoreNotTimeValid = 1,
		// Token: 0x040018AE RID: 6318
		IgnoreCtlNotTimeValid = 2,
		// Token: 0x040018AF RID: 6319
		IgnoreNotTimeNested = 4,
		// Token: 0x040018B0 RID: 6320
		IgnoreInvalidBasicConstraints = 8,
		// Token: 0x040018B1 RID: 6321
		AllowUnknownCertificateAuthority = 16,
		// Token: 0x040018B2 RID: 6322
		IgnoreWrongUsage = 32,
		// Token: 0x040018B3 RID: 6323
		IgnoreInvalidName = 64,
		// Token: 0x040018B4 RID: 6324
		IgnoreInvalidPolicy = 128,
		// Token: 0x040018B5 RID: 6325
		IgnoreEndRevocationUnknown = 256,
		// Token: 0x040018B6 RID: 6326
		IgnoreCtlSignerRevocationUnknown = 512,
		// Token: 0x040018B7 RID: 6327
		IgnoreCertificateAuthorityRevocationUnknown = 1024,
		// Token: 0x040018B8 RID: 6328
		IgnoreRootRevocationUnknown = 2048,
		// Token: 0x040018B9 RID: 6329
		AllFlags = 4095
	}
}
