﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000459 RID: 1113
	public enum X509SubjectKeyIdentifierHashAlgorithm
	{
		// Token: 0x040018A8 RID: 6312
		Sha1,
		// Token: 0x040018A9 RID: 6313
		ShortSha1,
		// Token: 0x040018AA RID: 6314
		CapiSha1
	}
}
