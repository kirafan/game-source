﻿using System;
using System.IO;
using System.Text;
using Mono.Security;
using Mono.Security.Cryptography;
using Mono.Security.X509;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000441 RID: 1089
	public class X509Certificate2 : X509Certificate
	{
		// Token: 0x0600270F RID: 9999 RVA: 0x0007A024 File Offset: 0x00078224
		public X509Certificate2()
		{
			this._cert = null;
		}

		// Token: 0x06002710 RID: 10000 RVA: 0x0007A040 File Offset: 0x00078240
		public X509Certificate2(byte[] rawData)
		{
			this.Import(rawData, null, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06002711 RID: 10001 RVA: 0x0007A05C File Offset: 0x0007825C
		public X509Certificate2(byte[] rawData, string password)
		{
			this.Import(rawData, password, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06002712 RID: 10002 RVA: 0x0007A078 File Offset: 0x00078278
		public X509Certificate2(byte[] rawData, SecureString password)
		{
			this.Import(rawData, password, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06002713 RID: 10003 RVA: 0x0007A094 File Offset: 0x00078294
		public X509Certificate2(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(rawData, password, keyStorageFlags);
		}

		// Token: 0x06002714 RID: 10004 RVA: 0x0007A0B0 File Offset: 0x000782B0
		public X509Certificate2(byte[] rawData, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(rawData, password, keyStorageFlags);
		}

		// Token: 0x06002715 RID: 10005 RVA: 0x0007A0CC File Offset: 0x000782CC
		public X509Certificate2(string fileName)
		{
			this.Import(fileName, string.Empty, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06002716 RID: 10006 RVA: 0x0007A0EC File Offset: 0x000782EC
		public X509Certificate2(string fileName, string password)
		{
			this.Import(fileName, password, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06002717 RID: 10007 RVA: 0x0007A108 File Offset: 0x00078308
		public X509Certificate2(string fileName, SecureString password)
		{
			this.Import(fileName, password, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06002718 RID: 10008 RVA: 0x0007A124 File Offset: 0x00078324
		public X509Certificate2(string fileName, string password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(fileName, password, keyStorageFlags);
		}

		// Token: 0x06002719 RID: 10009 RVA: 0x0007A140 File Offset: 0x00078340
		public X509Certificate2(string fileName, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(fileName, password, keyStorageFlags);
		}

		// Token: 0x0600271A RID: 10010 RVA: 0x0007A15C File Offset: 0x0007835C
		public X509Certificate2(IntPtr handle) : base(handle)
		{
			this._cert = new X509Certificate(base.GetRawCertData());
		}

		// Token: 0x0600271B RID: 10011 RVA: 0x0007A184 File Offset: 0x00078384
		public X509Certificate2(X509Certificate certificate) : base(certificate)
		{
			this._cert = new X509Certificate(base.GetRawCertData());
		}

		// Token: 0x17000AF7 RID: 2807
		// (get) Token: 0x0600271D RID: 10013 RVA: 0x0007A210 File Offset: 0x00078410
		// (set) Token: 0x0600271E RID: 10014 RVA: 0x0007A230 File Offset: 0x00078430
		public bool Archived
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				return this._archived;
			}
			set
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				this._archived = value;
			}
		}

		// Token: 0x17000AF8 RID: 2808
		// (get) Token: 0x0600271F RID: 10015 RVA: 0x0007A250 File Offset: 0x00078450
		public X509ExtensionCollection Extensions
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				if (this._extensions == null)
				{
					this._extensions = new X509ExtensionCollection(this._cert);
				}
				return this._extensions;
			}
		}

		// Token: 0x17000AF9 RID: 2809
		// (get) Token: 0x06002720 RID: 10016 RVA: 0x0007A298 File Offset: 0x00078498
		// (set) Token: 0x06002721 RID: 10017 RVA: 0x0007A2B8 File Offset: 0x000784B8
		public string FriendlyName
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				return this._name;
			}
			set
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				this._name = value;
			}
		}

		// Token: 0x17000AFA RID: 2810
		// (get) Token: 0x06002722 RID: 10018 RVA: 0x0007A2D8 File Offset: 0x000784D8
		public bool HasPrivateKey
		{
			get
			{
				return this.PrivateKey != null;
			}
		}

		// Token: 0x17000AFB RID: 2811
		// (get) Token: 0x06002723 RID: 10019 RVA: 0x0007A2E8 File Offset: 0x000784E8
		public X500DistinguishedName IssuerName
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				if (this.issuer_name == null)
				{
					this.issuer_name = new X500DistinguishedName(this._cert.GetIssuerName().GetBytes());
				}
				return this.issuer_name;
			}
		}

		// Token: 0x17000AFC RID: 2812
		// (get) Token: 0x06002724 RID: 10020 RVA: 0x0007A338 File Offset: 0x00078538
		public DateTime NotAfter
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				return this._cert.ValidUntil.ToLocalTime();
			}
		}

		// Token: 0x17000AFD RID: 2813
		// (get) Token: 0x06002725 RID: 10021 RVA: 0x0007A370 File Offset: 0x00078570
		public DateTime NotBefore
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				return this._cert.ValidFrom.ToLocalTime();
			}
		}

		// Token: 0x17000AFE RID: 2814
		// (get) Token: 0x06002726 RID: 10022 RVA: 0x0007A3A8 File Offset: 0x000785A8
		// (set) Token: 0x06002727 RID: 10023 RVA: 0x0007A4E8 File Offset: 0x000786E8
		public AsymmetricAlgorithm PrivateKey
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				try
				{
					if (this._cert.RSA != null)
					{
						RSACryptoServiceProvider rsacryptoServiceProvider = this._cert.RSA as RSACryptoServiceProvider;
						if (rsacryptoServiceProvider != null)
						{
							return (!rsacryptoServiceProvider.PublicOnly) ? rsacryptoServiceProvider : null;
						}
						RSAManaged rsamanaged = this._cert.RSA as RSAManaged;
						if (rsamanaged != null)
						{
							return (!rsamanaged.PublicOnly) ? rsamanaged : null;
						}
						this._cert.RSA.ExportParameters(true);
						return this._cert.RSA;
					}
					else if (this._cert.DSA != null)
					{
						DSACryptoServiceProvider dsacryptoServiceProvider = this._cert.DSA as DSACryptoServiceProvider;
						if (dsacryptoServiceProvider != null)
						{
							return (!dsacryptoServiceProvider.PublicOnly) ? dsacryptoServiceProvider : null;
						}
						this._cert.DSA.ExportParameters(true);
						return this._cert.DSA;
					}
				}
				catch
				{
				}
				return null;
			}
			set
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				if (value == null)
				{
					this._cert.RSA = null;
					this._cert.DSA = null;
				}
				else if (value is RSA)
				{
					this._cert.RSA = (RSA)value;
				}
				else
				{
					if (!(value is DSA))
					{
						throw new NotSupportedException();
					}
					this._cert.DSA = (DSA)value;
				}
			}
		}

		// Token: 0x17000AFF RID: 2815
		// (get) Token: 0x06002728 RID: 10024 RVA: 0x0007A578 File Offset: 0x00078778
		public PublicKey PublicKey
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				if (this._publicKey == null)
				{
					try
					{
						this._publicKey = new PublicKey(this._cert);
					}
					catch (Exception inner)
					{
						string text = Locale.GetText("Unable to decode public key.");
						throw new CryptographicException(text, inner);
					}
				}
				return this._publicKey;
			}
		}

		// Token: 0x17000B00 RID: 2816
		// (get) Token: 0x06002729 RID: 10025 RVA: 0x0007A5F8 File Offset: 0x000787F8
		public byte[] RawData
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				return base.GetRawCertData();
			}
		}

		// Token: 0x17000B01 RID: 2817
		// (get) Token: 0x0600272A RID: 10026 RVA: 0x0007A618 File Offset: 0x00078818
		public string SerialNumber
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				if (this._serial == null)
				{
					StringBuilder stringBuilder = new StringBuilder();
					byte[] serialNumber = this._cert.SerialNumber;
					for (int i = serialNumber.Length - 1; i >= 0; i--)
					{
						stringBuilder.Append(serialNumber[i].ToString("X2"));
					}
					this._serial = stringBuilder.ToString();
				}
				return this._serial;
			}
		}

		// Token: 0x17000B02 RID: 2818
		// (get) Token: 0x0600272B RID: 10027 RVA: 0x0007A698 File Offset: 0x00078898
		public Oid SignatureAlgorithm
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				if (this.signature_algorithm == null)
				{
					this.signature_algorithm = new Oid(this._cert.SignatureAlgorithm);
				}
				return this.signature_algorithm;
			}
		}

		// Token: 0x17000B03 RID: 2819
		// (get) Token: 0x0600272C RID: 10028 RVA: 0x0007A6D8 File Offset: 0x000788D8
		public X500DistinguishedName SubjectName
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				if (this.subject_name == null)
				{
					this.subject_name = new X500DistinguishedName(this._cert.GetSubjectName().GetBytes());
				}
				return this.subject_name;
			}
		}

		// Token: 0x17000B04 RID: 2820
		// (get) Token: 0x0600272D RID: 10029 RVA: 0x0007A728 File Offset: 0x00078928
		public string Thumbprint
		{
			get
			{
				return base.GetCertHashString();
			}
		}

		// Token: 0x17000B05 RID: 2821
		// (get) Token: 0x0600272E RID: 10030 RVA: 0x0007A730 File Offset: 0x00078930
		public int Version
		{
			get
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				return this._cert.Version;
			}
		}

		// Token: 0x0600272F RID: 10031 RVA: 0x0007A754 File Offset: 0x00078954
		[MonoTODO("always return String.Empty for UpnName, DnsFromAlternativeName and UrlName")]
		public string GetNameInfo(X509NameType nameType, bool forIssuer)
		{
			switch (nameType)
			{
			case X509NameType.SimpleName:
			{
				if (this._cert == null)
				{
					throw new CryptographicException(X509Certificate2.empty_error);
				}
				ASN1 asn = (!forIssuer) ? this._cert.GetSubjectName() : this._cert.GetIssuerName();
				ASN1 asn2 = this.Find(X509Certificate2.commonName, asn);
				if (asn2 != null)
				{
					return this.GetValueAsString(asn2);
				}
				if (asn.Count == 0)
				{
					return string.Empty;
				}
				ASN1 asn3 = asn[asn.Count - 1];
				if (asn3.Count == 0)
				{
					return string.Empty;
				}
				return this.GetValueAsString(asn3[0]);
			}
			case X509NameType.EmailName:
			{
				ASN1 asn4 = this.Find(X509Certificate2.email, (!forIssuer) ? this._cert.GetSubjectName() : this._cert.GetIssuerName());
				if (asn4 != null)
				{
					return this.GetValueAsString(asn4);
				}
				return string.Empty;
			}
			case X509NameType.UpnName:
				return string.Empty;
			case X509NameType.DnsName:
			{
				ASN1 asn5 = this.Find(X509Certificate2.commonName, (!forIssuer) ? this._cert.GetSubjectName() : this._cert.GetIssuerName());
				if (asn5 != null)
				{
					return this.GetValueAsString(asn5);
				}
				return string.Empty;
			}
			case X509NameType.DnsFromAlternativeName:
				return string.Empty;
			case X509NameType.UrlName:
				return string.Empty;
			default:
				throw new ArgumentException("nameType");
			}
		}

		// Token: 0x06002730 RID: 10032 RVA: 0x0007A8BC File Offset: 0x00078ABC
		private ASN1 Find(byte[] oid, ASN1 dn)
		{
			if (dn.Count == 0)
			{
				return null;
			}
			for (int i = 0; i < dn.Count; i++)
			{
				ASN1 asn = dn[i];
				for (int j = 0; j < asn.Count; j++)
				{
					ASN1 asn2 = asn[j];
					if (asn2.Count == 2)
					{
						ASN1 asn3 = asn2[0];
						if (asn3 != null)
						{
							if (asn3.CompareValue(oid))
							{
								return asn2;
							}
						}
					}
				}
			}
			return null;
		}

		// Token: 0x06002731 RID: 10033 RVA: 0x0007A94C File Offset: 0x00078B4C
		private string GetValueAsString(ASN1 pair)
		{
			if (pair.Count != 2)
			{
				return string.Empty;
			}
			ASN1 asn = pair[1];
			if (asn.Value == null || asn.Length == 0)
			{
				return string.Empty;
			}
			if (asn.Tag == 30)
			{
				StringBuilder stringBuilder = new StringBuilder();
				for (int i = 1; i < asn.Value.Length; i += 2)
				{
					stringBuilder.Append((char)asn.Value[i]);
				}
				return stringBuilder.ToString();
			}
			return Encoding.UTF8.GetString(asn.Value);
		}

		// Token: 0x06002732 RID: 10034 RVA: 0x0007A9E4 File Offset: 0x00078BE4
		private void ImportPkcs12(byte[] rawData, string password)
		{
			PKCS12 pkcs = (password != null) ? new PKCS12(rawData, password) : new PKCS12(rawData);
			if (pkcs.Certificates.Count > 0)
			{
				this._cert = pkcs.Certificates[0];
			}
			else
			{
				this._cert = null;
			}
			if (pkcs.Keys.Count > 0)
			{
				this._cert.RSA = (pkcs.Keys[0] as RSA);
				this._cert.DSA = (pkcs.Keys[0] as DSA);
			}
		}

		// Token: 0x06002733 RID: 10035 RVA: 0x0007AA84 File Offset: 0x00078C84
		public override void Import(byte[] rawData)
		{
			this.Import(rawData, null, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06002734 RID: 10036 RVA: 0x0007AA90 File Offset: 0x00078C90
		[MonoTODO("missing KeyStorageFlags support")]
		public override void Import(byte[] rawData, string password, X509KeyStorageFlags keyStorageFlags)
		{
			base.Import(rawData, password, keyStorageFlags);
			if (password == null)
			{
				try
				{
					this._cert = new X509Certificate(rawData);
				}
				catch (Exception inner)
				{
					try
					{
						this.ImportPkcs12(rawData, null);
					}
					catch
					{
						string text = Locale.GetText("Unable to decode certificate.");
						throw new CryptographicException(text, inner);
					}
				}
			}
			else
			{
				try
				{
					this.ImportPkcs12(rawData, password);
				}
				catch
				{
					this._cert = new X509Certificate(rawData);
				}
			}
		}

		// Token: 0x06002735 RID: 10037 RVA: 0x0007AB5C File Offset: 0x00078D5C
		[MonoTODO("SecureString is incomplete")]
		public override void Import(byte[] rawData, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			this.Import(rawData, null, keyStorageFlags);
		}

		// Token: 0x06002736 RID: 10038 RVA: 0x0007AB68 File Offset: 0x00078D68
		public override void Import(string fileName)
		{
			byte[] rawData = X509Certificate2.Load(fileName);
			this.Import(rawData, null, X509KeyStorageFlags.DefaultKeySet);
		}

		// Token: 0x06002737 RID: 10039 RVA: 0x0007AB88 File Offset: 0x00078D88
		[MonoTODO("missing KeyStorageFlags support")]
		public override void Import(string fileName, string password, X509KeyStorageFlags keyStorageFlags)
		{
			byte[] rawData = X509Certificate2.Load(fileName);
			this.Import(rawData, password, keyStorageFlags);
		}

		// Token: 0x06002738 RID: 10040 RVA: 0x0007ABA8 File Offset: 0x00078DA8
		[MonoTODO("SecureString is incomplete")]
		public override void Import(string fileName, SecureString password, X509KeyStorageFlags keyStorageFlags)
		{
			byte[] rawData = X509Certificate2.Load(fileName);
			this.Import(rawData, null, keyStorageFlags);
		}

		// Token: 0x06002739 RID: 10041 RVA: 0x0007ABC8 File Offset: 0x00078DC8
		private static byte[] Load(string fileName)
		{
			byte[] array = null;
			using (FileStream fileStream = File.OpenRead(fileName))
			{
				array = new byte[fileStream.Length];
				fileStream.Read(array, 0, array.Length);
				fileStream.Close();
			}
			return array;
		}

		// Token: 0x0600273A RID: 10042 RVA: 0x0007AC2C File Offset: 0x00078E2C
		public override void Reset()
		{
			this._cert = null;
			this._archived = false;
			this._extensions = null;
			this._name = string.Empty;
			this._serial = null;
			this._publicKey = null;
			this.issuer_name = null;
			this.subject_name = null;
			this.signature_algorithm = null;
			base.Reset();
		}

		// Token: 0x0600273B RID: 10043 RVA: 0x0007AC84 File Offset: 0x00078E84
		public override string ToString()
		{
			if (this._cert == null)
			{
				return "System.Security.Cryptography.X509Certificates.X509Certificate2";
			}
			return base.ToString(true);
		}

		// Token: 0x0600273C RID: 10044 RVA: 0x0007ACA0 File Offset: 0x00078EA0
		public override string ToString(bool verbose)
		{
			if (this._cert == null)
			{
				return "System.Security.Cryptography.X509Certificates.X509Certificate2";
			}
			if (!verbose)
			{
				return base.ToString(true);
			}
			string newLine = Environment.NewLine;
			StringBuilder stringBuilder = new StringBuilder();
			stringBuilder.AppendFormat("[Version]{0}  V{1}{0}{0}", newLine, this.Version);
			stringBuilder.AppendFormat("[Subject]{0}  {1}{0}{0}", newLine, base.Subject);
			stringBuilder.AppendFormat("[Issuer]{0}  {1}{0}{0}", newLine, base.Issuer);
			stringBuilder.AppendFormat("[Serial Number]{0}  {1}{0}{0}", newLine, this.SerialNumber);
			stringBuilder.AppendFormat("[Not Before]{0}  {1}{0}{0}", newLine, this.NotBefore);
			stringBuilder.AppendFormat("[Not After]{0}  {1}{0}{0}", newLine, this.NotAfter);
			stringBuilder.AppendFormat("[Thumbprint]{0}  {1}{0}{0}", newLine, this.Thumbprint);
			stringBuilder.AppendFormat("[Signature Algorithm]{0}  {1}({2}){0}{0}", newLine, this.SignatureAlgorithm.FriendlyName, this.SignatureAlgorithm.Value);
			AsymmetricAlgorithm key = this.PublicKey.Key;
			stringBuilder.AppendFormat("[Public Key]{0}  Algorithm: ", newLine);
			if (key is RSA)
			{
				stringBuilder.Append("RSA");
			}
			else if (key is DSA)
			{
				stringBuilder.Append("DSA");
			}
			else
			{
				stringBuilder.Append(key.ToString());
			}
			stringBuilder.AppendFormat("{0}  Length: {1}{0}  Key Blob: ", newLine, key.KeySize);
			X509Certificate2.AppendBuffer(stringBuilder, this.PublicKey.EncodedKeyValue.RawData);
			stringBuilder.AppendFormat("{0}  Parameters: ", newLine);
			X509Certificate2.AppendBuffer(stringBuilder, this.PublicKey.EncodedParameters.RawData);
			stringBuilder.Append(newLine);
			return stringBuilder.ToString();
		}

		// Token: 0x0600273D RID: 10045 RVA: 0x0007AE4C File Offset: 0x0007904C
		private static void AppendBuffer(StringBuilder sb, byte[] buffer)
		{
			if (buffer == null)
			{
				return;
			}
			for (int i = 0; i < buffer.Length; i++)
			{
				sb.Append(buffer[i].ToString("x2"));
				if (i < buffer.Length - 1)
				{
					sb.Append(" ");
				}
			}
		}

		// Token: 0x0600273E RID: 10046 RVA: 0x0007AEA4 File Offset: 0x000790A4
		[MonoTODO("by default this depends on the incomplete X509Chain")]
		public bool Verify()
		{
			if (this._cert == null)
			{
				throw new CryptographicException(X509Certificate2.empty_error);
			}
			X509Chain x509Chain = (X509Chain)CryptoConfig.CreateFromName("X509Chain");
			return x509Chain.Build(this);
		}

		// Token: 0x0600273F RID: 10047 RVA: 0x0007AEE8 File Offset: 0x000790E8
		[MonoTODO("Detection limited to Cert, Pfx, Pkcs12, Pkcs7 and Unknown")]
		public static X509ContentType GetCertContentType(byte[] rawData)
		{
			if (rawData == null || rawData.Length == 0)
			{
				throw new ArgumentException("rawData");
			}
			X509ContentType result = X509ContentType.Unknown;
			try
			{
				ASN1 asn = new ASN1(rawData);
				if (asn.Tag != 48)
				{
					string text = Locale.GetText("Unable to decode certificate.");
					throw new CryptographicException(text);
				}
				if (asn.Count == 0)
				{
					return result;
				}
				if (asn.Count == 3)
				{
					byte tag = asn[0].Tag;
					if (tag != 2)
					{
						if (tag == 48)
						{
							if (asn[1].Tag == 48 && asn[2].Tag == 3)
							{
								result = X509ContentType.Cert;
							}
						}
					}
					else if (asn[1].Tag == 48 && asn[2].Tag == 48)
					{
						result = X509ContentType.Pfx;
					}
				}
				if (asn[0].Tag == 6 && asn[0].CompareValue(X509Certificate2.signedData))
				{
					result = X509ContentType.Pkcs7;
				}
			}
			catch (Exception inner)
			{
				string text2 = Locale.GetText("Unable to decode certificate.");
				throw new CryptographicException(text2, inner);
			}
			return result;
		}

		// Token: 0x06002740 RID: 10048 RVA: 0x0007B03C File Offset: 0x0007923C
		[MonoTODO("Detection limited to Cert, Pfx, Pkcs12 and Unknown")]
		public static X509ContentType GetCertContentType(string fileName)
		{
			if (fileName == null)
			{
				throw new ArgumentNullException("fileName");
			}
			if (fileName.Length == 0)
			{
				throw new ArgumentException("fileName");
			}
			byte[] rawData = X509Certificate2.Load(fileName);
			return X509Certificate2.GetCertContentType(rawData);
		}

		// Token: 0x17000B06 RID: 2822
		// (get) Token: 0x06002741 RID: 10049 RVA: 0x0007B080 File Offset: 0x00079280
		internal X509Certificate MonoCertificate
		{
			get
			{
				return this._cert;
			}
		}

		// Token: 0x0400181A RID: 6170
		private bool _archived;

		// Token: 0x0400181B RID: 6171
		private X509ExtensionCollection _extensions;

		// Token: 0x0400181C RID: 6172
		private string _name = string.Empty;

		// Token: 0x0400181D RID: 6173
		private string _serial;

		// Token: 0x0400181E RID: 6174
		private PublicKey _publicKey;

		// Token: 0x0400181F RID: 6175
		private X500DistinguishedName issuer_name;

		// Token: 0x04001820 RID: 6176
		private X500DistinguishedName subject_name;

		// Token: 0x04001821 RID: 6177
		private Oid signature_algorithm;

		// Token: 0x04001822 RID: 6178
		private X509Certificate _cert;

		// Token: 0x04001823 RID: 6179
		private static string empty_error = Locale.GetText("Certificate instance is empty.");

		// Token: 0x04001824 RID: 6180
		private static byte[] commonName = new byte[]
		{
			85,
			4,
			3
		};

		// Token: 0x04001825 RID: 6181
		private static byte[] email = new byte[]
		{
			42,
			134,
			72,
			134,
			247,
			13,
			1,
			9,
			1
		};

		// Token: 0x04001826 RID: 6182
		private static byte[] signedData = new byte[]
		{
			42,
			134,
			72,
			134,
			247,
			13,
			1,
			7,
			2
		};
	}
}
