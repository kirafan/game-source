﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000454 RID: 1108
	public enum X509NameType
	{
		// Token: 0x0400188E RID: 6286
		SimpleName,
		// Token: 0x0400188F RID: 6287
		EmailName,
		// Token: 0x04001890 RID: 6288
		UpnName,
		// Token: 0x04001891 RID: 6289
		DnsName,
		// Token: 0x04001892 RID: 6290
		DnsFromAlternativeName,
		// Token: 0x04001893 RID: 6291
		UrlName
	}
}
