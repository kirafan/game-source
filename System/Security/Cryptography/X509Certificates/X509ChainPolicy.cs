﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000449 RID: 1097
	public sealed class X509ChainPolicy
	{
		// Token: 0x0600279E RID: 10142 RVA: 0x0007CBF8 File Offset: 0x0007ADF8
		public X509ChainPolicy()
		{
			this.Reset();
		}

		// Token: 0x17000B1D RID: 2845
		// (get) Token: 0x0600279F RID: 10143 RVA: 0x0007CC08 File Offset: 0x0007AE08
		public OidCollection ApplicationPolicy
		{
			get
			{
				return this.apps;
			}
		}

		// Token: 0x17000B1E RID: 2846
		// (get) Token: 0x060027A0 RID: 10144 RVA: 0x0007CC10 File Offset: 0x0007AE10
		public OidCollection CertificatePolicy
		{
			get
			{
				return this.cert;
			}
		}

		// Token: 0x17000B1F RID: 2847
		// (get) Token: 0x060027A1 RID: 10145 RVA: 0x0007CC18 File Offset: 0x0007AE18
		public X509Certificate2Collection ExtraStore
		{
			get
			{
				return this.store;
			}
		}

		// Token: 0x17000B20 RID: 2848
		// (get) Token: 0x060027A2 RID: 10146 RVA: 0x0007CC20 File Offset: 0x0007AE20
		// (set) Token: 0x060027A3 RID: 10147 RVA: 0x0007CC28 File Offset: 0x0007AE28
		public X509RevocationFlag RevocationFlag
		{
			get
			{
				return this.rflag;
			}
			set
			{
				if (value < X509RevocationFlag.EndCertificateOnly || value > X509RevocationFlag.ExcludeRoot)
				{
					throw new ArgumentException("RevocationFlag");
				}
				this.rflag = value;
			}
		}

		// Token: 0x17000B21 RID: 2849
		// (get) Token: 0x060027A4 RID: 10148 RVA: 0x0007CC58 File Offset: 0x0007AE58
		// (set) Token: 0x060027A5 RID: 10149 RVA: 0x0007CC60 File Offset: 0x0007AE60
		public X509RevocationMode RevocationMode
		{
			get
			{
				return this.mode;
			}
			set
			{
				if (value < X509RevocationMode.NoCheck || value > X509RevocationMode.Offline)
				{
					throw new ArgumentException("RevocationMode");
				}
				this.mode = value;
			}
		}

		// Token: 0x17000B22 RID: 2850
		// (get) Token: 0x060027A6 RID: 10150 RVA: 0x0007CC90 File Offset: 0x0007AE90
		// (set) Token: 0x060027A7 RID: 10151 RVA: 0x0007CC98 File Offset: 0x0007AE98
		public TimeSpan UrlRetrievalTimeout
		{
			get
			{
				return this.timeout;
			}
			set
			{
				this.timeout = value;
			}
		}

		// Token: 0x17000B23 RID: 2851
		// (get) Token: 0x060027A8 RID: 10152 RVA: 0x0007CCA4 File Offset: 0x0007AEA4
		// (set) Token: 0x060027A9 RID: 10153 RVA: 0x0007CCAC File Offset: 0x0007AEAC
		public X509VerificationFlags VerificationFlags
		{
			get
			{
				return this.vflags;
			}
			set
			{
				if ((value | X509VerificationFlags.AllFlags) != X509VerificationFlags.AllFlags)
				{
					throw new ArgumentException("VerificationFlags");
				}
				this.vflags = value;
			}
		}

		// Token: 0x17000B24 RID: 2852
		// (get) Token: 0x060027AA RID: 10154 RVA: 0x0007CCD4 File Offset: 0x0007AED4
		// (set) Token: 0x060027AB RID: 10155 RVA: 0x0007CCDC File Offset: 0x0007AEDC
		public DateTime VerificationTime
		{
			get
			{
				return this.vtime;
			}
			set
			{
				this.vtime = value;
			}
		}

		// Token: 0x060027AC RID: 10156 RVA: 0x0007CCE8 File Offset: 0x0007AEE8
		public void Reset()
		{
			this.apps = new OidCollection();
			this.cert = new OidCollection();
			this.store = new X509Certificate2Collection();
			this.rflag = X509RevocationFlag.ExcludeRoot;
			this.mode = X509RevocationMode.Online;
			this.timeout = TimeSpan.Zero;
			this.vflags = X509VerificationFlags.NoFlag;
			this.vtime = DateTime.Now;
		}

		// Token: 0x0400183E RID: 6206
		private OidCollection apps;

		// Token: 0x0400183F RID: 6207
		private OidCollection cert;

		// Token: 0x04001840 RID: 6208
		private X509Certificate2Collection store;

		// Token: 0x04001841 RID: 6209
		private X509RevocationFlag rflag;

		// Token: 0x04001842 RID: 6210
		private X509RevocationMode mode;

		// Token: 0x04001843 RID: 6211
		private TimeSpan timeout;

		// Token: 0x04001844 RID: 6212
		private X509VerificationFlags vflags;

		// Token: 0x04001845 RID: 6213
		private DateTime vtime;
	}
}
