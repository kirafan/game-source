﻿using System;
using System.Text;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200044E RID: 1102
	public class X509Extension : AsnEncodedData
	{
		// Token: 0x060027C7 RID: 10183 RVA: 0x0007D628 File Offset: 0x0007B828
		protected X509Extension()
		{
		}

		// Token: 0x060027C8 RID: 10184 RVA: 0x0007D630 File Offset: 0x0007B830
		public X509Extension(AsnEncodedData encodedExtension, bool critical)
		{
			if (encodedExtension.Oid == null)
			{
				throw new ArgumentNullException("encodedExtension.Oid");
			}
			base.Oid = encodedExtension.Oid;
			base.RawData = encodedExtension.RawData;
			this._critical = critical;
		}

		// Token: 0x060027C9 RID: 10185 RVA: 0x0007D678 File Offset: 0x0007B878
		public X509Extension(Oid oid, byte[] rawData, bool critical)
		{
			if (oid == null)
			{
				throw new ArgumentNullException("oid");
			}
			base.Oid = oid;
			base.RawData = rawData;
			this._critical = critical;
		}

		// Token: 0x060027CA RID: 10186 RVA: 0x0007D6B4 File Offset: 0x0007B8B4
		public X509Extension(string oid, byte[] rawData, bool critical) : base(oid, rawData)
		{
			this._critical = critical;
		}

		// Token: 0x17000B2D RID: 2861
		// (get) Token: 0x060027CB RID: 10187 RVA: 0x0007D6C8 File Offset: 0x0007B8C8
		// (set) Token: 0x060027CC RID: 10188 RVA: 0x0007D6D0 File Offset: 0x0007B8D0
		public bool Critical
		{
			get
			{
				return this._critical;
			}
			set
			{
				this._critical = value;
			}
		}

		// Token: 0x060027CD RID: 10189 RVA: 0x0007D6DC File Offset: 0x0007B8DC
		public override void CopyFrom(AsnEncodedData asnEncodedData)
		{
			if (asnEncodedData == null)
			{
				throw new ArgumentNullException("encodedData");
			}
			X509Extension x509Extension = asnEncodedData as X509Extension;
			if (x509Extension == null)
			{
				throw new ArgumentException(Locale.GetText("Expected a X509Extension instance."));
			}
			base.CopyFrom(asnEncodedData);
			this._critical = x509Extension.Critical;
		}

		// Token: 0x060027CE RID: 10190 RVA: 0x0007D72C File Offset: 0x0007B92C
		internal string FormatUnkownData(byte[] data)
		{
			if (data == null || data.Length == 0)
			{
				return string.Empty;
			}
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < data.Length; i++)
			{
				stringBuilder.Append(data[i].ToString("X2"));
			}
			return stringBuilder.ToString();
		}

		// Token: 0x04001866 RID: 6246
		private bool _critical;
	}
}
