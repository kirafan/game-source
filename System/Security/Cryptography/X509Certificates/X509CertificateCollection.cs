﻿using System;
using System.Collections;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000443 RID: 1091
	[Serializable]
	public class X509CertificateCollection : CollectionBase
	{
		// Token: 0x06002749 RID: 10057 RVA: 0x0007B100 File Offset: 0x00079300
		public X509CertificateCollection()
		{
		}

		// Token: 0x0600274A RID: 10058 RVA: 0x0007B108 File Offset: 0x00079308
		public X509CertificateCollection(X509Certificate[] value)
		{
			this.AddRange(value);
		}

		// Token: 0x0600274B RID: 10059 RVA: 0x0007B118 File Offset: 0x00079318
		public X509CertificateCollection(X509CertificateCollection value)
		{
			this.AddRange(value);
		}

		// Token: 0x17000B09 RID: 2825
		public X509Certificate this[int index]
		{
			get
			{
				return (X509Certificate)base.InnerList[index];
			}
			set
			{
				base.InnerList[index] = value;
			}
		}

		// Token: 0x0600274E RID: 10062 RVA: 0x0007B14C File Offset: 0x0007934C
		public int Add(X509Certificate value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			return base.InnerList.Add(value);
		}

		// Token: 0x0600274F RID: 10063 RVA: 0x0007B16C File Offset: 0x0007936C
		public void AddRange(X509Certificate[] value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.Length; i++)
			{
				base.InnerList.Add(value[i]);
			}
		}

		// Token: 0x06002750 RID: 10064 RVA: 0x0007B1B0 File Offset: 0x000793B0
		public void AddRange(X509CertificateCollection value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			for (int i = 0; i < value.InnerList.Count; i++)
			{
				base.InnerList.Add(value[i]);
			}
		}

		// Token: 0x06002751 RID: 10065 RVA: 0x0007B200 File Offset: 0x00079400
		public bool Contains(X509Certificate value)
		{
			if (value == null)
			{
				return false;
			}
			byte[] certHash = value.GetCertHash();
			for (int i = 0; i < base.InnerList.Count; i++)
			{
				X509Certificate x509Certificate = (X509Certificate)base.InnerList[i];
				if (this.Compare(x509Certificate.GetCertHash(), certHash))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x06002752 RID: 10066 RVA: 0x0007B260 File Offset: 0x00079460
		public void CopyTo(X509Certificate[] array, int index)
		{
			base.InnerList.CopyTo(array, index);
		}

		// Token: 0x06002753 RID: 10067 RVA: 0x0007B270 File Offset: 0x00079470
		public new X509CertificateCollection.X509CertificateEnumerator GetEnumerator()
		{
			return new X509CertificateCollection.X509CertificateEnumerator(this);
		}

		// Token: 0x06002754 RID: 10068 RVA: 0x0007B278 File Offset: 0x00079478
		public override int GetHashCode()
		{
			return base.InnerList.GetHashCode();
		}

		// Token: 0x06002755 RID: 10069 RVA: 0x0007B288 File Offset: 0x00079488
		public int IndexOf(X509Certificate value)
		{
			return base.InnerList.IndexOf(value);
		}

		// Token: 0x06002756 RID: 10070 RVA: 0x0007B298 File Offset: 0x00079498
		public void Insert(int index, X509Certificate value)
		{
			base.InnerList.Insert(index, value);
		}

		// Token: 0x06002757 RID: 10071 RVA: 0x0007B2A8 File Offset: 0x000794A8
		public void Remove(X509Certificate value)
		{
			if (value == null)
			{
				throw new ArgumentNullException("value");
			}
			if (this.IndexOf(value) == -1)
			{
				throw new ArgumentException("value", Locale.GetText("Not part of the collection."));
			}
			base.InnerList.Remove(value);
		}

		// Token: 0x06002758 RID: 10072 RVA: 0x0007B2F4 File Offset: 0x000794F4
		private bool Compare(byte[] array1, byte[] array2)
		{
			if (array1 == null && array2 == null)
			{
				return true;
			}
			if (array1 == null || array2 == null)
			{
				return false;
			}
			if (array1.Length != array2.Length)
			{
				return false;
			}
			for (int i = 0; i < array1.Length; i++)
			{
				if (array1[i] != array2[i])
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x02000444 RID: 1092
		public class X509CertificateEnumerator : IEnumerator
		{
			// Token: 0x06002759 RID: 10073 RVA: 0x0007B34C File Offset: 0x0007954C
			public X509CertificateEnumerator(X509CertificateCollection mappings)
			{
				this.enumerator = ((IEnumerable)mappings).GetEnumerator();
			}

			// Token: 0x17000B0A RID: 2826
			// (get) Token: 0x0600275A RID: 10074 RVA: 0x0007B360 File Offset: 0x00079560
			object IEnumerator.Current
			{
				get
				{
					return this.enumerator.Current;
				}
			}

			// Token: 0x0600275B RID: 10075 RVA: 0x0007B370 File Offset: 0x00079570
			bool IEnumerator.MoveNext()
			{
				return this.enumerator.MoveNext();
			}

			// Token: 0x0600275C RID: 10076 RVA: 0x0007B380 File Offset: 0x00079580
			void IEnumerator.Reset()
			{
				this.enumerator.Reset();
			}

			// Token: 0x17000B0B RID: 2827
			// (get) Token: 0x0600275D RID: 10077 RVA: 0x0007B390 File Offset: 0x00079590
			public X509Certificate Current
			{
				get
				{
					return (X509Certificate)this.enumerator.Current;
				}
			}

			// Token: 0x0600275E RID: 10078 RVA: 0x0007B3A4 File Offset: 0x000795A4
			public bool MoveNext()
			{
				return this.enumerator.MoveNext();
			}

			// Token: 0x0600275F RID: 10079 RVA: 0x0007B3B4 File Offset: 0x000795B4
			public void Reset()
			{
				this.enumerator.Reset();
			}

			// Token: 0x04001828 RID: 6184
			private IEnumerator enumerator;
		}
	}
}
