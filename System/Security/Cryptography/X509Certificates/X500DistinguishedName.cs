﻿using System;
using System.Text;
using Mono.Security;
using Mono.Security.X509;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200043D RID: 1085
	[MonoTODO("Some X500DistinguishedNameFlags options aren't supported, like DoNotUsePlusSign, DoNotUseQuotes and ForceUTF8Encoding")]
	public sealed class X500DistinguishedName : AsnEncodedData
	{
		// Token: 0x060026E3 RID: 9955 RVA: 0x00078DB4 File Offset: 0x00076FB4
		public X500DistinguishedName(AsnEncodedData encodedDistinguishedName)
		{
			if (encodedDistinguishedName == null)
			{
				throw new ArgumentNullException("encodedDistinguishedName");
			}
			base.RawData = encodedDistinguishedName.RawData;
			if (base.RawData.Length > 0)
			{
				this.DecodeRawData();
			}
			else
			{
				this.name = string.Empty;
			}
		}

		// Token: 0x060026E4 RID: 9956 RVA: 0x00078E08 File Offset: 0x00077008
		public X500DistinguishedName(byte[] encodedDistinguishedName)
		{
			if (encodedDistinguishedName == null)
			{
				throw new ArgumentNullException("encodedDistinguishedName");
			}
			base.Oid = new Oid();
			base.RawData = encodedDistinguishedName;
			if (encodedDistinguishedName.Length > 0)
			{
				this.DecodeRawData();
			}
			else
			{
				this.name = string.Empty;
			}
		}

		// Token: 0x060026E5 RID: 9957 RVA: 0x00078E60 File Offset: 0x00077060
		public X500DistinguishedName(string distinguishedName) : this(distinguishedName, X500DistinguishedNameFlags.Reversed)
		{
		}

		// Token: 0x060026E6 RID: 9958 RVA: 0x00078E6C File Offset: 0x0007706C
		public X500DistinguishedName(string distinguishedName, X500DistinguishedNameFlags flag)
		{
			if (distinguishedName == null)
			{
				throw new ArgumentNullException("distinguishedName");
			}
			if (flag != X500DistinguishedNameFlags.None && (flag & (X500DistinguishedNameFlags.Reversed | X500DistinguishedNameFlags.UseSemicolons | X500DistinguishedNameFlags.DoNotUsePlusSign | X500DistinguishedNameFlags.DoNotUseQuotes | X500DistinguishedNameFlags.UseCommas | X500DistinguishedNameFlags.UseNewLines | X500DistinguishedNameFlags.UseUTF8Encoding | X500DistinguishedNameFlags.UseT61Encoding | X500DistinguishedNameFlags.ForceUTF8Encoding)) == X500DistinguishedNameFlags.None)
			{
				throw new ArgumentException("flag");
			}
			base.Oid = new Oid();
			if (distinguishedName.Length == 0)
			{
				byte[] array = new byte[2];
				array[0] = 48;
				base.RawData = array;
				this.DecodeRawData();
			}
			else
			{
				ASN1 asn = X501.FromString(distinguishedName);
				if ((flag & X500DistinguishedNameFlags.Reversed) != X500DistinguishedNameFlags.None)
				{
					ASN1 asn2 = new ASN1(48);
					for (int i = asn.Count - 1; i >= 0; i--)
					{
						asn2.Add(asn[i]);
					}
					asn = asn2;
				}
				base.RawData = asn.GetBytes();
				if (flag == X500DistinguishedNameFlags.None)
				{
					this.name = distinguishedName;
				}
				else
				{
					this.name = this.Decode(flag);
				}
			}
		}

		// Token: 0x060026E7 RID: 9959 RVA: 0x00078F4C File Offset: 0x0007714C
		public X500DistinguishedName(X500DistinguishedName distinguishedName)
		{
			if (distinguishedName == null)
			{
				throw new ArgumentNullException("distinguishedName");
			}
			base.Oid = new Oid();
			base.RawData = distinguishedName.RawData;
			this.name = distinguishedName.name;
		}

		// Token: 0x17000AF2 RID: 2802
		// (get) Token: 0x060026E8 RID: 9960 RVA: 0x00078F94 File Offset: 0x00077194
		public string Name
		{
			get
			{
				return this.name;
			}
		}

		// Token: 0x060026E9 RID: 9961 RVA: 0x00078F9C File Offset: 0x0007719C
		public string Decode(X500DistinguishedNameFlags flag)
		{
			if (flag != X500DistinguishedNameFlags.None && (flag & (X500DistinguishedNameFlags.Reversed | X500DistinguishedNameFlags.UseSemicolons | X500DistinguishedNameFlags.DoNotUsePlusSign | X500DistinguishedNameFlags.DoNotUseQuotes | X500DistinguishedNameFlags.UseCommas | X500DistinguishedNameFlags.UseNewLines | X500DistinguishedNameFlags.UseUTF8Encoding | X500DistinguishedNameFlags.UseT61Encoding | X500DistinguishedNameFlags.ForceUTF8Encoding)) == X500DistinguishedNameFlags.None)
			{
				throw new ArgumentException("flag");
			}
			if (base.RawData.Length == 0)
			{
				return string.Empty;
			}
			bool reversed = (flag & X500DistinguishedNameFlags.Reversed) != X500DistinguishedNameFlags.None;
			bool quotes = (flag & X500DistinguishedNameFlags.DoNotUseQuotes) == X500DistinguishedNameFlags.None;
			string separator = X500DistinguishedName.GetSeparator(flag);
			ASN1 seq = new ASN1(base.RawData);
			return X501.ToString(seq, reversed, separator, quotes);
		}

		// Token: 0x060026EA RID: 9962 RVA: 0x00079008 File Offset: 0x00077208
		public override string Format(bool multiLine)
		{
			if (!multiLine)
			{
				return this.Decode(X500DistinguishedNameFlags.UseCommas);
			}
			string text = this.Decode(X500DistinguishedNameFlags.UseNewLines);
			if (text.Length > 0)
			{
				return text + Environment.NewLine;
			}
			return text;
		}

		// Token: 0x060026EB RID: 9963 RVA: 0x0007904C File Offset: 0x0007724C
		private static string GetSeparator(X500DistinguishedNameFlags flag)
		{
			if ((flag & X500DistinguishedNameFlags.UseSemicolons) != X500DistinguishedNameFlags.None)
			{
				return "; ";
			}
			if ((flag & X500DistinguishedNameFlags.UseCommas) != X500DistinguishedNameFlags.None)
			{
				return ", ";
			}
			if ((flag & X500DistinguishedNameFlags.UseNewLines) != X500DistinguishedNameFlags.None)
			{
				return Environment.NewLine;
			}
			return ", ";
		}

		// Token: 0x060026EC RID: 9964 RVA: 0x00079094 File Offset: 0x00077294
		private void DecodeRawData()
		{
			if (base.RawData == null || base.RawData.Length < 3)
			{
				this.name = string.Empty;
				return;
			}
			ASN1 seq = new ASN1(base.RawData);
			this.name = X501.ToString(seq, true, ", ", true);
		}

		// Token: 0x060026ED RID: 9965 RVA: 0x000790E8 File Offset: 0x000772E8
		private static string Canonize(string s)
		{
			int i = s.IndexOf('=');
			StringBuilder stringBuilder = new StringBuilder(s.Substring(0, i + 1));
			while (char.IsWhiteSpace(s, ++i))
			{
			}
			s = s.TrimEnd(new char[0]);
			bool flag = false;
			while (i < s.Length)
			{
				if (!flag)
				{
					goto IL_5C;
				}
				flag = char.IsWhiteSpace(s, i);
				if (!flag)
				{
					goto IL_5C;
				}
				IL_7D:
				i++;
				continue;
				IL_5C:
				if (char.IsWhiteSpace(s, i))
				{
					flag = true;
				}
				stringBuilder.Append(char.ToUpperInvariant(s[i]));
				goto IL_7D;
			}
			return stringBuilder.ToString();
		}

		// Token: 0x060026EE RID: 9966 RVA: 0x00079188 File Offset: 0x00077388
		internal static bool AreEqual(X500DistinguishedName name1, X500DistinguishedName name2)
		{
			if (name1 == null)
			{
				return name2 == null;
			}
			if (name2 == null)
			{
				return false;
			}
			X500DistinguishedNameFlags flag = X500DistinguishedNameFlags.DoNotUseQuotes | X500DistinguishedNameFlags.UseNewLines;
			string[] separator = new string[]
			{
				Environment.NewLine
			};
			string[] array = name1.Decode(flag).Split(separator, StringSplitOptions.RemoveEmptyEntries);
			string[] array2 = name2.Decode(flag).Split(separator, StringSplitOptions.RemoveEmptyEntries);
			if (array.Length != array2.Length)
			{
				return false;
			}
			for (int i = 0; i < array.Length; i++)
			{
				if (X500DistinguishedName.Canonize(array[i]) != X500DistinguishedName.Canonize(array2[i]))
				{
					return false;
				}
			}
			return true;
		}

		// Token: 0x04001807 RID: 6151
		private const X500DistinguishedNameFlags AllFlags = X500DistinguishedNameFlags.Reversed | X500DistinguishedNameFlags.UseSemicolons | X500DistinguishedNameFlags.DoNotUsePlusSign | X500DistinguishedNameFlags.DoNotUseQuotes | X500DistinguishedNameFlags.UseCommas | X500DistinguishedNameFlags.UseNewLines | X500DistinguishedNameFlags.UseUTF8Encoding | X500DistinguishedNameFlags.UseT61Encoding | X500DistinguishedNameFlags.ForceUTF8Encoding;

		// Token: 0x04001808 RID: 6152
		private string name;
	}
}
