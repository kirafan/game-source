﻿using System;
using System.Collections;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200044F RID: 1103
	public sealed class X509ExtensionEnumerator : IEnumerator
	{
		// Token: 0x060027CF RID: 10191 RVA: 0x0007D788 File Offset: 0x0007B988
		internal X509ExtensionEnumerator(ArrayList list)
		{
			this.enumerator = list.GetEnumerator();
		}

		// Token: 0x17000B2E RID: 2862
		// (get) Token: 0x060027D0 RID: 10192 RVA: 0x0007D79C File Offset: 0x0007B99C
		object IEnumerator.Current
		{
			get
			{
				return this.enumerator.Current;
			}
		}

		// Token: 0x17000B2F RID: 2863
		// (get) Token: 0x060027D1 RID: 10193 RVA: 0x0007D7AC File Offset: 0x0007B9AC
		public X509Extension Current
		{
			get
			{
				return (X509Extension)this.enumerator.Current;
			}
		}

		// Token: 0x060027D2 RID: 10194 RVA: 0x0007D7C0 File Offset: 0x0007B9C0
		public bool MoveNext()
		{
			return this.enumerator.MoveNext();
		}

		// Token: 0x060027D3 RID: 10195 RVA: 0x0007D7D0 File Offset: 0x0007B9D0
		public void Reset()
		{
			this.enumerator.Reset();
		}

		// Token: 0x04001867 RID: 6247
		private IEnumerator enumerator;
	}
}
