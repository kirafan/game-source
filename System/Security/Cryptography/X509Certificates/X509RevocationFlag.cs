﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000455 RID: 1109
	public enum X509RevocationFlag
	{
		// Token: 0x04001895 RID: 6293
		EndCertificateOnly,
		// Token: 0x04001896 RID: 6294
		EntireChain,
		// Token: 0x04001897 RID: 6295
		ExcludeRoot
	}
}
