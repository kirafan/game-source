﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000450 RID: 1104
	public enum X509FindType
	{
		// Token: 0x04001869 RID: 6249
		FindByThumbprint,
		// Token: 0x0400186A RID: 6250
		FindBySubjectName,
		// Token: 0x0400186B RID: 6251
		FindBySubjectDistinguishedName,
		// Token: 0x0400186C RID: 6252
		FindByIssuerName,
		// Token: 0x0400186D RID: 6253
		FindByIssuerDistinguishedName,
		// Token: 0x0400186E RID: 6254
		FindBySerialNumber,
		// Token: 0x0400186F RID: 6255
		FindByTimeValid,
		// Token: 0x04001870 RID: 6256
		FindByTimeNotYetValid,
		// Token: 0x04001871 RID: 6257
		FindByTimeExpired,
		// Token: 0x04001872 RID: 6258
		FindByTemplateName,
		// Token: 0x04001873 RID: 6259
		FindByApplicationPolicy,
		// Token: 0x04001874 RID: 6260
		FindByCertificatePolicy,
		// Token: 0x04001875 RID: 6261
		FindByExtension,
		// Token: 0x04001876 RID: 6262
		FindByKeyUsage,
		// Token: 0x04001877 RID: 6263
		FindBySubjectKeyIdentifier
	}
}
