﻿using System;
using System.Collections;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000446 RID: 1094
	public sealed class X509ChainElementCollection : ICollection, IEnumerable
	{
		// Token: 0x06002784 RID: 10116 RVA: 0x0007C780 File Offset: 0x0007A980
		internal X509ChainElementCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x06002785 RID: 10117 RVA: 0x0007C794 File Offset: 0x0007A994
		void ICollection.CopyTo(Array array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x06002786 RID: 10118 RVA: 0x0007C7A4 File Offset: 0x0007A9A4
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new X509ChainElementEnumerator(this._list);
		}

		// Token: 0x17000B13 RID: 2835
		// (get) Token: 0x06002787 RID: 10119 RVA: 0x0007C7B4 File Offset: 0x0007A9B4
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x17000B14 RID: 2836
		// (get) Token: 0x06002788 RID: 10120 RVA: 0x0007C7C4 File Offset: 0x0007A9C4
		public bool IsSynchronized
		{
			get
			{
				return this._list.IsSynchronized;
			}
		}

		// Token: 0x17000B15 RID: 2837
		public X509ChainElement this[int index]
		{
			get
			{
				return (X509ChainElement)this._list[index];
			}
		}

		// Token: 0x17000B16 RID: 2838
		// (get) Token: 0x0600278A RID: 10122 RVA: 0x0007C7E8 File Offset: 0x0007A9E8
		public object SyncRoot
		{
			get
			{
				return this._list.SyncRoot;
			}
		}

		// Token: 0x0600278B RID: 10123 RVA: 0x0007C7F8 File Offset: 0x0007A9F8
		public void CopyTo(X509ChainElement[] array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x0600278C RID: 10124 RVA: 0x0007C808 File Offset: 0x0007AA08
		public X509ChainElementEnumerator GetEnumerator()
		{
			return new X509ChainElementEnumerator(this._list);
		}

		// Token: 0x0600278D RID: 10125 RVA: 0x0007C818 File Offset: 0x0007AA18
		internal void Add(X509Certificate2 certificate)
		{
			this._list.Add(new X509ChainElement(certificate));
		}

		// Token: 0x0600278E RID: 10126 RVA: 0x0007C82C File Offset: 0x0007AA2C
		internal void Clear()
		{
			this._list.Clear();
		}

		// Token: 0x0600278F RID: 10127 RVA: 0x0007C83C File Offset: 0x0007AA3C
		internal bool Contains(X509Certificate2 certificate)
		{
			for (int i = 0; i < this._list.Count; i++)
			{
				if (certificate.Equals((this._list[i] as X509ChainElement).Certificate))
				{
					return true;
				}
			}
			return false;
		}

		// Token: 0x04001838 RID: 6200
		private ArrayList _list;
	}
}
