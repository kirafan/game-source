﻿using System;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x02000456 RID: 1110
	public enum X509RevocationMode
	{
		// Token: 0x04001899 RID: 6297
		NoCheck,
		// Token: 0x0400189A RID: 6298
		Online,
		// Token: 0x0400189B RID: 6299
		Offline
	}
}
