﻿using System;
using System.Collections.Generic;
using System.Text;
using Mono.Security;

namespace System.Security.Cryptography.X509Certificates
{
	// Token: 0x0200044C RID: 1100
	public sealed class X509EnhancedKeyUsageExtension : X509Extension
	{
		// Token: 0x060027B3 RID: 10163 RVA: 0x0007CE98 File Offset: 0x0007B098
		public X509EnhancedKeyUsageExtension()
		{
			this._oid = new Oid("2.5.29.37", "Enhanced Key Usage");
		}

		// Token: 0x060027B4 RID: 10164 RVA: 0x0007CEB8 File Offset: 0x0007B0B8
		public X509EnhancedKeyUsageExtension(AsnEncodedData encodedEnhancedKeyUsages, bool critical)
		{
			this._oid = new Oid("2.5.29.37", "Enhanced Key Usage");
			this._raw = encodedEnhancedKeyUsages.RawData;
			base.Critical = critical;
			this._status = this.Decode(base.RawData);
		}

		// Token: 0x060027B5 RID: 10165 RVA: 0x0007CF08 File Offset: 0x0007B108
		public X509EnhancedKeyUsageExtension(OidCollection enhancedKeyUsages, bool critical)
		{
			if (enhancedKeyUsages == null)
			{
				throw new ArgumentNullException("enhancedKeyUsages");
			}
			this._oid = new Oid("2.5.29.37", "Enhanced Key Usage");
			base.Critical = critical;
			this._enhKeyUsage = enhancedKeyUsages.ReadOnlyCopy();
			base.RawData = this.Encode();
		}

		// Token: 0x17000B27 RID: 2855
		// (get) Token: 0x060027B6 RID: 10166 RVA: 0x0007CF60 File Offset: 0x0007B160
		public OidCollection EnhancedKeyUsages
		{
			get
			{
				AsnDecodeStatus status = this._status;
				if (status != AsnDecodeStatus.Ok && status != AsnDecodeStatus.InformationNotAvailable)
				{
					throw new CryptographicException("Badly encoded extension.");
				}
				if (this._enhKeyUsage == null)
				{
					this._enhKeyUsage = new OidCollection();
				}
				this._enhKeyUsage.ReadOnly = true;
				return this._enhKeyUsage;
			}
		}

		// Token: 0x060027B7 RID: 10167 RVA: 0x0007CFBC File Offset: 0x0007B1BC
		public override void CopyFrom(AsnEncodedData asnEncodedData)
		{
			if (asnEncodedData == null)
			{
				throw new ArgumentNullException("encodedData");
			}
			X509Extension x509Extension = asnEncodedData as X509Extension;
			if (x509Extension == null)
			{
				throw new ArgumentException(Locale.GetText("Wrong type."), "asnEncodedData");
			}
			if (x509Extension._oid == null)
			{
				this._oid = new Oid("2.5.29.37", "Enhanced Key Usage");
			}
			else
			{
				this._oid = new Oid(x509Extension._oid);
			}
			base.RawData = x509Extension.RawData;
			base.Critical = x509Extension.Critical;
			this._status = this.Decode(base.RawData);
		}

		// Token: 0x060027B8 RID: 10168 RVA: 0x0007D05C File Offset: 0x0007B25C
		internal AsnDecodeStatus Decode(byte[] extension)
		{
			if (extension == null || extension.Length == 0)
			{
				return AsnDecodeStatus.BadAsn;
			}
			if (extension[0] != 48)
			{
				return AsnDecodeStatus.BadTag;
			}
			if (this._enhKeyUsage == null)
			{
				this._enhKeyUsage = new OidCollection();
			}
			try
			{
				ASN1 asn = new ASN1(extension);
				if (asn.Tag != 48)
				{
					throw new CryptographicException(Locale.GetText("Invalid ASN.1 Tag"));
				}
				for (int i = 0; i < asn.Count; i++)
				{
					this._enhKeyUsage.Add(new Oid(ASN1Convert.ToOid(asn[i])));
				}
			}
			catch
			{
				return AsnDecodeStatus.BadAsn;
			}
			return AsnDecodeStatus.Ok;
		}

		// Token: 0x060027B9 RID: 10169 RVA: 0x0007D124 File Offset: 0x0007B324
		internal byte[] Encode()
		{
			ASN1 asn = new ASN1(48);
			foreach (Oid oid in this._enhKeyUsage)
			{
				asn.Add(ASN1Convert.FromOid(oid.Value));
			}
			return asn.GetBytes();
		}

		// Token: 0x060027BA RID: 10170 RVA: 0x0007D174 File Offset: 0x0007B374
		internal override string ToString(bool multiLine)
		{
			switch (this._status)
			{
			case AsnDecodeStatus.BadAsn:
				return string.Empty;
			case AsnDecodeStatus.BadTag:
			case AsnDecodeStatus.BadLength:
				return base.FormatUnkownData(this._raw);
			case AsnDecodeStatus.InformationNotAvailable:
				return "Information Not Available";
			default:
			{
				if (this._oid.Value != "2.5.29.37")
				{
					return string.Format("Unknown Key Usage ({0})", this._oid.Value);
				}
				if (this._enhKeyUsage.Count == 0)
				{
					return "Information Not Available";
				}
				StringBuilder stringBuilder = new StringBuilder();
				int i = 0;
				while (i < this._enhKeyUsage.Count)
				{
					Oid oid = this._enhKeyUsage[i];
					string value = oid.Value;
					if (value == null)
					{
						goto IL_102;
					}
					if (X509EnhancedKeyUsageExtension.<>f__switch$map1A == null)
					{
						X509EnhancedKeyUsageExtension.<>f__switch$map1A = new Dictionary<string, int>(1)
						{
							{
								"1.3.6.1.5.5.7.3.1",
								0
							}
						};
					}
					int num;
					if (!X509EnhancedKeyUsageExtension.<>f__switch$map1A.TryGetValue(value, out num))
					{
						goto IL_102;
					}
					if (num != 0)
					{
						goto IL_102;
					}
					stringBuilder.Append("Server Authentication (");
					IL_113:
					stringBuilder.Append(oid.Value);
					stringBuilder.Append(")");
					if (multiLine)
					{
						stringBuilder.Append(Environment.NewLine);
					}
					else if (i != this._enhKeyUsage.Count - 1)
					{
						stringBuilder.Append(", ");
					}
					i++;
					continue;
					IL_102:
					stringBuilder.Append("Unknown Key Usage (");
					goto IL_113;
				}
				return stringBuilder.ToString();
			}
			}
		}

		// Token: 0x04001860 RID: 6240
		internal const string oid = "2.5.29.37";

		// Token: 0x04001861 RID: 6241
		internal const string friendlyName = "Enhanced Key Usage";

		// Token: 0x04001862 RID: 6242
		private OidCollection _enhKeyUsage;

		// Token: 0x04001863 RID: 6243
		private AsnDecodeStatus _status;
	}
}
