﻿using System;
using System.Collections;

namespace System.Security.Cryptography
{
	// Token: 0x02000436 RID: 1078
	public sealed class OidEnumerator : IEnumerator
	{
		// Token: 0x060026C8 RID: 9928 RVA: 0x00078518 File Offset: 0x00076718
		internal OidEnumerator(OidCollection collection)
		{
			this._collection = collection;
			this._position = -1;
		}

		// Token: 0x17000AEC RID: 2796
		// (get) Token: 0x060026C9 RID: 9929 RVA: 0x00078530 File Offset: 0x00076730
		object IEnumerator.Current
		{
			get
			{
				if (this._position < 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				return this._collection[this._position];
			}
		}

		// Token: 0x17000AED RID: 2797
		// (get) Token: 0x060026CA RID: 9930 RVA: 0x00078558 File Offset: 0x00076758
		public Oid Current
		{
			get
			{
				if (this._position < 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				return this._collection[this._position];
			}
		}

		// Token: 0x060026CB RID: 9931 RVA: 0x00078580 File Offset: 0x00076780
		public bool MoveNext()
		{
			if (++this._position < this._collection.Count)
			{
				return true;
			}
			this._position = this._collection.Count - 1;
			return false;
		}

		// Token: 0x060026CC RID: 9932 RVA: 0x000785C4 File Offset: 0x000767C4
		public void Reset()
		{
			this._position = -1;
		}

		// Token: 0x040017E0 RID: 6112
		private OidCollection _collection;

		// Token: 0x040017E1 RID: 6113
		private int _position;
	}
}
