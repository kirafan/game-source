﻿using System;
using System.Collections;

namespace System.Security.Cryptography
{
	// Token: 0x02000433 RID: 1075
	public sealed class AsnEncodedDataEnumerator : IEnumerator
	{
		// Token: 0x060026AB RID: 9899 RVA: 0x00077EBC File Offset: 0x000760BC
		internal AsnEncodedDataEnumerator(AsnEncodedDataCollection collection)
		{
			this._collection = collection;
			this._position = -1;
		}

		// Token: 0x17000AE2 RID: 2786
		// (get) Token: 0x060026AC RID: 9900 RVA: 0x00077ED4 File Offset: 0x000760D4
		object IEnumerator.Current
		{
			get
			{
				if (this._position < 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				return this._collection[this._position];
			}
		}

		// Token: 0x17000AE3 RID: 2787
		// (get) Token: 0x060026AD RID: 9901 RVA: 0x00077EFC File Offset: 0x000760FC
		public AsnEncodedData Current
		{
			get
			{
				if (this._position < 0)
				{
					throw new ArgumentOutOfRangeException();
				}
				return this._collection[this._position];
			}
		}

		// Token: 0x060026AE RID: 9902 RVA: 0x00077F24 File Offset: 0x00076124
		public bool MoveNext()
		{
			if (++this._position < this._collection.Count)
			{
				return true;
			}
			this._position = this._collection.Count - 1;
			return false;
		}

		// Token: 0x060026AF RID: 9903 RVA: 0x00077F68 File Offset: 0x00076168
		public void Reset()
		{
			this._position = -1;
		}

		// Token: 0x040017C4 RID: 6084
		private AsnEncodedDataCollection _collection;

		// Token: 0x040017C5 RID: 6085
		private int _position;
	}
}
