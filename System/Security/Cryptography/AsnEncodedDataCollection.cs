﻿using System;
using System.Collections;

namespace System.Security.Cryptography
{
	// Token: 0x02000430 RID: 1072
	public sealed class AsnEncodedDataCollection : ICollection, IEnumerable
	{
		// Token: 0x0600268C RID: 9868 RVA: 0x00077678 File Offset: 0x00075878
		public AsnEncodedDataCollection()
		{
			this._list = new ArrayList();
		}

		// Token: 0x0600268D RID: 9869 RVA: 0x0007768C File Offset: 0x0007588C
		public AsnEncodedDataCollection(AsnEncodedData asnEncodedData)
		{
			this._list = new ArrayList();
			this._list.Add(asnEncodedData);
		}

		// Token: 0x0600268E RID: 9870 RVA: 0x000776AC File Offset: 0x000758AC
		void ICollection.CopyTo(Array array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x0600268F RID: 9871 RVA: 0x000776BC File Offset: 0x000758BC
		IEnumerator IEnumerable.GetEnumerator()
		{
			return new AsnEncodedDataEnumerator(this);
		}

		// Token: 0x17000ADC RID: 2780
		// (get) Token: 0x06002690 RID: 9872 RVA: 0x000776C4 File Offset: 0x000758C4
		public int Count
		{
			get
			{
				return this._list.Count;
			}
		}

		// Token: 0x17000ADD RID: 2781
		// (get) Token: 0x06002691 RID: 9873 RVA: 0x000776D4 File Offset: 0x000758D4
		public bool IsSynchronized
		{
			get
			{
				return this._list.IsSynchronized;
			}
		}

		// Token: 0x17000ADE RID: 2782
		public AsnEncodedData this[int index]
		{
			get
			{
				return (AsnEncodedData)this._list[index];
			}
		}

		// Token: 0x17000ADF RID: 2783
		// (get) Token: 0x06002693 RID: 9875 RVA: 0x000776F8 File Offset: 0x000758F8
		public object SyncRoot
		{
			get
			{
				return this._list.SyncRoot;
			}
		}

		// Token: 0x06002694 RID: 9876 RVA: 0x00077708 File Offset: 0x00075908
		public int Add(AsnEncodedData asnEncodedData)
		{
			return this._list.Add(asnEncodedData);
		}

		// Token: 0x06002695 RID: 9877 RVA: 0x00077718 File Offset: 0x00075918
		public void CopyTo(AsnEncodedData[] array, int index)
		{
			this._list.CopyTo(array, index);
		}

		// Token: 0x06002696 RID: 9878 RVA: 0x00077728 File Offset: 0x00075928
		public AsnEncodedDataEnumerator GetEnumerator()
		{
			return new AsnEncodedDataEnumerator(this);
		}

		// Token: 0x06002697 RID: 9879 RVA: 0x00077730 File Offset: 0x00075930
		public void Remove(AsnEncodedData asnEncodedData)
		{
			this._list.Remove(asnEncodedData);
		}

		// Token: 0x040017B9 RID: 6073
		private ArrayList _list;
	}
}
