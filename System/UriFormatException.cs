﻿using System;
using System.Runtime.Serialization;

namespace System
{
	// Token: 0x020004B5 RID: 1205
	[Serializable]
	public class UriFormatException : FormatException, ISerializable
	{
		// Token: 0x06002B8D RID: 11149 RVA: 0x00097E98 File Offset: 0x00096098
		public UriFormatException() : base(Locale.GetText("Invalid URI format"))
		{
		}

		// Token: 0x06002B8E RID: 11150 RVA: 0x00097EAC File Offset: 0x000960AC
		public UriFormatException(string message) : base(message)
		{
		}

		// Token: 0x06002B8F RID: 11151 RVA: 0x00097EB8 File Offset: 0x000960B8
		protected UriFormatException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x06002B90 RID: 11152 RVA: 0x00097EC4 File Offset: 0x000960C4
		void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
		}
	}
}
