﻿using System;

namespace System.Configuration
{
	// Token: 0x02000509 RID: 1289
	// (Invoke) Token: 0x06002CD0 RID: 11472
	public delegate void SettingChangingEventHandler(object sender, SettingChangingEventArgs e);
}
