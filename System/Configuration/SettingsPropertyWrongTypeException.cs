﻿using System;
using System.Runtime.Serialization;

namespace System.Configuration
{
	// Token: 0x020001F9 RID: 505
	[Serializable]
	public class SettingsPropertyWrongTypeException : Exception
	{
		// Token: 0x0600115B RID: 4443 RVA: 0x0002E84C File Offset: 0x0002CA4C
		public SettingsPropertyWrongTypeException()
		{
		}

		// Token: 0x0600115C RID: 4444 RVA: 0x0002E854 File Offset: 0x0002CA54
		public SettingsPropertyWrongTypeException(string message) : base(message)
		{
		}

		// Token: 0x0600115D RID: 4445 RVA: 0x0002E860 File Offset: 0x0002CA60
		protected SettingsPropertyWrongTypeException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0600115E RID: 4446 RVA: 0x0002E86C File Offset: 0x0002CA6C
		public SettingsPropertyWrongTypeException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
