﻿using System;
using System.Collections.Specialized;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x020001E9 RID: 489
	public class NameValueSectionHandler : IConfigurationSectionHandler
	{
		// Token: 0x060010DD RID: 4317 RVA: 0x0002D6DC File Offset: 0x0002B8DC
		public object Create(object parent, object context, XmlNode section)
		{
			return ConfigHelper.GetNameValueCollection(parent as System.Collections.Specialized.NameValueCollection, section, this.KeyAttributeName, this.ValueAttributeName);
		}

		// Token: 0x170003C7 RID: 967
		// (get) Token: 0x060010DE RID: 4318 RVA: 0x0002D704 File Offset: 0x0002B904
		protected virtual string KeyAttributeName
		{
			get
			{
				return "key";
			}
		}

		// Token: 0x170003C8 RID: 968
		// (get) Token: 0x060010DF RID: 4319 RVA: 0x0002D70C File Offset: 0x0002B90C
		protected virtual string ValueAttributeName
		{
			get
			{
				return "value";
			}
		}
	}
}
