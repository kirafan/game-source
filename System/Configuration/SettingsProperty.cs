﻿using System;

namespace System.Configuration
{
	// Token: 0x020001F4 RID: 500
	public class SettingsProperty
	{
		// Token: 0x06001123 RID: 4387 RVA: 0x0002E040 File Offset: 0x0002C240
		public SettingsProperty(SettingsProperty propertyToCopy) : this(propertyToCopy.Name, propertyToCopy.PropertyType, propertyToCopy.Provider, propertyToCopy.IsReadOnly, propertyToCopy.DefaultValue, propertyToCopy.SerializeAs, new SettingsAttributeDictionary(propertyToCopy.Attributes), propertyToCopy.ThrowOnErrorDeserializing, propertyToCopy.ThrowOnErrorSerializing)
		{
		}

		// Token: 0x06001124 RID: 4388 RVA: 0x0002E090 File Offset: 0x0002C290
		public SettingsProperty(string name) : this(name, null, null, false, null, SettingsSerializeAs.String, new SettingsAttributeDictionary(), false, false)
		{
		}

		// Token: 0x06001125 RID: 4389 RVA: 0x0002E0B0 File Offset: 0x0002C2B0
		public SettingsProperty(string name, Type propertyType, SettingsProvider provider, bool isReadOnly, object defaultValue, SettingsSerializeAs serializeAs, SettingsAttributeDictionary attributes, bool throwOnErrorDeserializing, bool throwOnErrorSerializing)
		{
			this.name = name;
			this.propertyType = propertyType;
			this.provider = provider;
			this.isReadOnly = isReadOnly;
			this.defaultValue = defaultValue;
			this.serializeAs = serializeAs;
			this.attributes = attributes;
			this.throwOnErrorDeserializing = throwOnErrorDeserializing;
			this.throwOnErrorSerializing = throwOnErrorSerializing;
		}

		// Token: 0x170003DF RID: 991
		// (get) Token: 0x06001126 RID: 4390 RVA: 0x0002E108 File Offset: 0x0002C308
		public virtual SettingsAttributeDictionary Attributes
		{
			get
			{
				return this.attributes;
			}
		}

		// Token: 0x170003E0 RID: 992
		// (get) Token: 0x06001127 RID: 4391 RVA: 0x0002E110 File Offset: 0x0002C310
		// (set) Token: 0x06001128 RID: 4392 RVA: 0x0002E118 File Offset: 0x0002C318
		public virtual object DefaultValue
		{
			get
			{
				return this.defaultValue;
			}
			set
			{
				this.defaultValue = value;
			}
		}

		// Token: 0x170003E1 RID: 993
		// (get) Token: 0x06001129 RID: 4393 RVA: 0x0002E124 File Offset: 0x0002C324
		// (set) Token: 0x0600112A RID: 4394 RVA: 0x0002E12C File Offset: 0x0002C32C
		public virtual bool IsReadOnly
		{
			get
			{
				return this.isReadOnly;
			}
			set
			{
				this.isReadOnly = value;
			}
		}

		// Token: 0x170003E2 RID: 994
		// (get) Token: 0x0600112B RID: 4395 RVA: 0x0002E138 File Offset: 0x0002C338
		// (set) Token: 0x0600112C RID: 4396 RVA: 0x0002E140 File Offset: 0x0002C340
		public virtual string Name
		{
			get
			{
				return this.name;
			}
			set
			{
				this.name = value;
			}
		}

		// Token: 0x170003E3 RID: 995
		// (get) Token: 0x0600112D RID: 4397 RVA: 0x0002E14C File Offset: 0x0002C34C
		// (set) Token: 0x0600112E RID: 4398 RVA: 0x0002E154 File Offset: 0x0002C354
		public virtual Type PropertyType
		{
			get
			{
				return this.propertyType;
			}
			set
			{
				this.propertyType = value;
			}
		}

		// Token: 0x170003E4 RID: 996
		// (get) Token: 0x0600112F RID: 4399 RVA: 0x0002E160 File Offset: 0x0002C360
		// (set) Token: 0x06001130 RID: 4400 RVA: 0x0002E168 File Offset: 0x0002C368
		public virtual SettingsProvider Provider
		{
			get
			{
				return this.provider;
			}
			set
			{
				this.provider = value;
			}
		}

		// Token: 0x170003E5 RID: 997
		// (get) Token: 0x06001131 RID: 4401 RVA: 0x0002E174 File Offset: 0x0002C374
		// (set) Token: 0x06001132 RID: 4402 RVA: 0x0002E17C File Offset: 0x0002C37C
		public virtual SettingsSerializeAs SerializeAs
		{
			get
			{
				return this.serializeAs;
			}
			set
			{
				this.serializeAs = value;
			}
		}

		// Token: 0x170003E6 RID: 998
		// (get) Token: 0x06001133 RID: 4403 RVA: 0x0002E188 File Offset: 0x0002C388
		// (set) Token: 0x06001134 RID: 4404 RVA: 0x0002E190 File Offset: 0x0002C390
		public bool ThrowOnErrorDeserializing
		{
			get
			{
				return this.throwOnErrorDeserializing;
			}
			set
			{
				this.throwOnErrorDeserializing = value;
			}
		}

		// Token: 0x170003E7 RID: 999
		// (get) Token: 0x06001135 RID: 4405 RVA: 0x0002E19C File Offset: 0x0002C39C
		// (set) Token: 0x06001136 RID: 4406 RVA: 0x0002E1A4 File Offset: 0x0002C3A4
		public bool ThrowOnErrorSerializing
		{
			get
			{
				return this.throwOnErrorSerializing;
			}
			set
			{
				this.throwOnErrorSerializing = value;
			}
		}

		// Token: 0x040004E1 RID: 1249
		private string name;

		// Token: 0x040004E2 RID: 1250
		private Type propertyType;

		// Token: 0x040004E3 RID: 1251
		private SettingsProvider provider;

		// Token: 0x040004E4 RID: 1252
		private bool isReadOnly;

		// Token: 0x040004E5 RID: 1253
		private object defaultValue;

		// Token: 0x040004E6 RID: 1254
		private SettingsSerializeAs serializeAs;

		// Token: 0x040004E7 RID: 1255
		private SettingsAttributeDictionary attributes;

		// Token: 0x040004E8 RID: 1256
		private bool throwOnErrorDeserializing;

		// Token: 0x040004E9 RID: 1257
		private bool throwOnErrorSerializing;
	}
}
