﻿using System;
using System.Collections;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x02000205 RID: 517
	public class SingleTagSectionHandler : IConfigurationSectionHandler
	{
		// Token: 0x06001181 RID: 4481 RVA: 0x0002EA38 File Offset: 0x0002CC38
		public virtual object Create(object parent, object context, XmlNode section)
		{
			Hashtable hashtable;
			if (parent == null)
			{
				hashtable = new Hashtable();
			}
			else
			{
				hashtable = (Hashtable)parent;
			}
			if (section.HasChildNodes)
			{
				throw new ConfigurationException("Child Nodes not allowed.");
			}
			XmlAttributeCollection attributes = section.Attributes;
			for (int i = 0; i < attributes.Count; i++)
			{
				hashtable.Add(attributes[i].Name, attributes[i].Value);
			}
			return hashtable;
		}
	}
}
