﻿using System;

namespace System.Configuration
{
	// Token: 0x020001EA RID: 490
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class NoSettingsVersionUpgradeAttribute : Attribute
	{
	}
}
