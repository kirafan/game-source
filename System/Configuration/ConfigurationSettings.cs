﻿using System;
using System.Collections.Specialized;

namespace System.Configuration
{
	// Token: 0x020001CF RID: 463
	public sealed class ConfigurationSettings
	{
		// Token: 0x0600102D RID: 4141 RVA: 0x0002ADCC File Offset: 0x00028FCC
		private ConfigurationSettings()
		{
		}

		// Token: 0x0600102F RID: 4143 RVA: 0x0002ADEC File Offset: 0x00028FEC
		[Obsolete("This method is obsolete, it has been replaced by System.Configuration!System.Configuration.ConfigurationManager.GetSection")]
		public static object GetConfig(string sectionName)
		{
			return ConfigurationManager.GetSection(sectionName);
		}

		// Token: 0x17000398 RID: 920
		// (get) Token: 0x06001030 RID: 4144 RVA: 0x0002ADF4 File Offset: 0x00028FF4
		[Obsolete("This property is obsolete.  Please use System.Configuration.ConfigurationManager.AppSettings")]
		public static System.Collections.Specialized.NameValueCollection AppSettings
		{
			get
			{
				object obj = ConfigurationManager.GetSection("appSettings");
				if (obj == null)
				{
					obj = new System.Collections.Specialized.NameValueCollection();
				}
				return (System.Collections.Specialized.NameValueCollection)obj;
			}
		}

		// Token: 0x06001031 RID: 4145 RVA: 0x0002AE20 File Offset: 0x00029020
		internal static IConfigurationSystem ChangeConfigurationSystem(IConfigurationSystem newSystem)
		{
			if (newSystem == null)
			{
				throw new ArgumentNullException("newSystem");
			}
			object obj = ConfigurationSettings.lockobj;
			IConfigurationSystem result;
			lock (obj)
			{
				IConfigurationSystem configurationSystem = ConfigurationSettings.config;
				ConfigurationSettings.config = newSystem;
				result = configurationSystem;
			}
			return result;
		}

		// Token: 0x04000480 RID: 1152
		private static IConfigurationSystem config = DefaultConfig.GetInstance();

		// Token: 0x04000481 RID: 1153
		private static object lockobj = new object();
	}
}
