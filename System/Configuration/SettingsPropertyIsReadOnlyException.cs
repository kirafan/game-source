﻿using System;
using System.Runtime.Serialization;

namespace System.Configuration
{
	// Token: 0x020001F5 RID: 501
	[Serializable]
	public class SettingsPropertyIsReadOnlyException : Exception
	{
		// Token: 0x06001137 RID: 4407 RVA: 0x0002E1B0 File Offset: 0x0002C3B0
		public SettingsPropertyIsReadOnlyException()
		{
		}

		// Token: 0x06001138 RID: 4408 RVA: 0x0002E1B8 File Offset: 0x0002C3B8
		public SettingsPropertyIsReadOnlyException(string message) : base(message)
		{
		}

		// Token: 0x06001139 RID: 4409 RVA: 0x0002E1C4 File Offset: 0x0002C3C4
		protected SettingsPropertyIsReadOnlyException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
		}

		// Token: 0x0600113A RID: 4410 RVA: 0x0002E1D0 File Offset: 0x0002C3D0
		public SettingsPropertyIsReadOnlyException(string message, Exception innerException) : base(message, innerException)
		{
		}
	}
}
