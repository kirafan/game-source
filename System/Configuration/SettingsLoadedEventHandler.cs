﻿using System;

namespace System.Configuration
{
	// Token: 0x0200050A RID: 1290
	// (Invoke) Token: 0x06002CD4 RID: 11476
	public delegate void SettingsLoadedEventHandler(object sender, SettingsLoadedEventArgs e);
}
