﻿using System;
using System.Configuration.Provider;

namespace System.Configuration
{
	// Token: 0x020001FB RID: 507
	public class SettingsProviderCollection : ProviderCollection
	{
		// Token: 0x06001163 RID: 4451 RVA: 0x0002E8D0 File Offset: 0x0002CAD0
		public override void Add(ProviderBase provider)
		{
			if (!(provider is SettingsProvider))
			{
				throw new ArgumentException("SettingsProvider is expected");
			}
			if (string.IsNullOrEmpty(provider.Name))
			{
				throw new ArgumentException("Provider name cannot be null or empty");
			}
			base.Add(provider);
		}

		// Token: 0x170003F4 RID: 1012
		public SettingsProvider this[string name]
		{
			get
			{
				return (SettingsProvider)base[name];
			}
		}
	}
}
