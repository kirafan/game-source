﻿using System;
using System.Runtime.InteropServices;

namespace System.Configuration
{
	// Token: 0x020001E0 RID: 480
	[ComVisible(false)]
	public interface IConfigurationSystem
	{
		// Token: 0x060010B5 RID: 4277
		object GetConfig(string configKey);

		// Token: 0x060010B6 RID: 4278
		void Init();
	}
}
