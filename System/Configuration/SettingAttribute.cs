﻿using System;

namespace System.Configuration
{
	// Token: 0x020001EB RID: 491
	[AttributeUsage(AttributeTargets.Property)]
	public class SettingAttribute : Attribute
	{
	}
}
