﻿using System;

namespace System.Configuration
{
	// Token: 0x02000206 RID: 518
	public enum SpecialSetting
	{
		// Token: 0x04000503 RID: 1283
		ConnectionString,
		// Token: 0x04000504 RID: 1284
		WebServiceUrl
	}
}
