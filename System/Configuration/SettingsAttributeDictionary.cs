﻿using System;
using System.Collections;

namespace System.Configuration
{
	// Token: 0x020001EF RID: 495
	[Serializable]
	public class SettingsAttributeDictionary : Hashtable
	{
		// Token: 0x060010FC RID: 4348 RVA: 0x0002DA44 File Offset: 0x0002BC44
		public SettingsAttributeDictionary()
		{
		}

		// Token: 0x060010FD RID: 4349 RVA: 0x0002DA4C File Offset: 0x0002BC4C
		public SettingsAttributeDictionary(SettingsAttributeDictionary attributes) : base(attributes)
		{
		}
	}
}
