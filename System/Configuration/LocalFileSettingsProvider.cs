﻿using System;
using System.Collections.Specialized;

namespace System.Configuration
{
	// Token: 0x020001E7 RID: 487
	public class LocalFileSettingsProvider : SettingsProvider, IApplicationSettingsProvider
	{
		// Token: 0x060010D1 RID: 4305 RVA: 0x0002D52C File Offset: 0x0002B72C
		public LocalFileSettingsProvider()
		{
			this.impl = new CustomizableFileSettingsProvider();
		}

		// Token: 0x060010D2 RID: 4306 RVA: 0x0002D540 File Offset: 0x0002B740
		[MonoTODO]
		public SettingsPropertyValue GetPreviousVersion(SettingsContext context, SettingsProperty property)
		{
			return this.impl.GetPreviousVersion(context, property);
		}

		// Token: 0x060010D3 RID: 4307 RVA: 0x0002D550 File Offset: 0x0002B750
		[MonoTODO]
		public override SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection properties)
		{
			return this.impl.GetPropertyValues(context, properties);
		}

		// Token: 0x060010D4 RID: 4308 RVA: 0x0002D560 File Offset: 0x0002B760
		public override void Initialize(string name, System.Collections.Specialized.NameValueCollection values)
		{
			if (name == null)
			{
				name = "LocalFileSettingsProvider";
			}
			if (values != null)
			{
				this.impl.ApplicationName = values["applicationName"];
			}
			base.Initialize(name, values);
		}

		// Token: 0x060010D5 RID: 4309 RVA: 0x0002D594 File Offset: 0x0002B794
		[MonoTODO]
		public void Reset(SettingsContext context)
		{
			this.impl.Reset(context);
		}

		// Token: 0x060010D6 RID: 4310 RVA: 0x0002D5A4 File Offset: 0x0002B7A4
		[MonoTODO]
		public override void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection values)
		{
			this.impl.SetPropertyValues(context, values);
		}

		// Token: 0x060010D7 RID: 4311 RVA: 0x0002D5B4 File Offset: 0x0002B7B4
		[MonoTODO]
		public void Upgrade(SettingsContext context, SettingsPropertyCollection properties)
		{
			this.impl.Upgrade(context, properties);
		}

		// Token: 0x170003C6 RID: 966
		// (get) Token: 0x060010D8 RID: 4312 RVA: 0x0002D5C4 File Offset: 0x0002B7C4
		// (set) Token: 0x060010D9 RID: 4313 RVA: 0x0002D5D4 File Offset: 0x0002B7D4
		public override string ApplicationName
		{
			get
			{
				return this.impl.ApplicationName;
			}
			set
			{
				this.impl.ApplicationName = value;
			}
		}

		// Token: 0x040004CF RID: 1231
		private CustomizableFileSettingsProvider impl;
	}
}
