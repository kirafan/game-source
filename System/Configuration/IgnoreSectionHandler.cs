﻿using System;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x020001E3 RID: 483
	public class IgnoreSectionHandler : IConfigurationSectionHandler
	{
		// Token: 0x060010C1 RID: 4289 RVA: 0x0002D478 File Offset: 0x0002B678
		public virtual object Create(object parent, object configContext, XmlNode section)
		{
			return null;
		}
	}
}
