﻿using System;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x020001DF RID: 479
	public interface IConfigurationSectionHandler
	{
		// Token: 0x060010B4 RID: 4276
		object Create(object parent, object configContext, XmlNode section);
	}
}
