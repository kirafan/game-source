﻿using System;
using System.ComponentModel;

namespace System.Configuration
{
	// Token: 0x0200050B RID: 1291
	// (Invoke) Token: 0x06002CD8 RID: 11480
	public delegate void SettingsSavingEventHandler(object sender, System.ComponentModel.CancelEventArgs e);
}
