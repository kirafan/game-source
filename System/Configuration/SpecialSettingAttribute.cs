﻿using System;

namespace System.Configuration
{
	// Token: 0x02000207 RID: 519
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
	public sealed class SpecialSettingAttribute : Attribute
	{
		// Token: 0x06001182 RID: 4482 RVA: 0x0002EAB0 File Offset: 0x0002CCB0
		public SpecialSettingAttribute(SpecialSetting setting)
		{
			this.setting = setting;
		}

		// Token: 0x170003FD RID: 1021
		// (get) Token: 0x06001183 RID: 4483 RVA: 0x0002EAC0 File Offset: 0x0002CCC0
		public SpecialSetting SpecialSetting
		{
			get
			{
				return this.setting;
			}
		}

		// Token: 0x04000505 RID: 1285
		private SpecialSetting setting;
	}
}
