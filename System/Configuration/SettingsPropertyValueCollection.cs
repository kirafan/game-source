﻿using System;
using System.Collections;

namespace System.Configuration
{
	// Token: 0x020001F7 RID: 503
	public class SettingsPropertyValueCollection : ICollection, IEnumerable, ICloneable
	{
		// Token: 0x0600113F RID: 4415 RVA: 0x0002E208 File Offset: 0x0002C408
		public SettingsPropertyValueCollection()
		{
			this.items = new Hashtable();
		}

		// Token: 0x06001140 RID: 4416 RVA: 0x0002E21C File Offset: 0x0002C41C
		public void Add(SettingsPropertyValue property)
		{
			if (this.isReadOnly)
			{
				throw new NotSupportedException();
			}
			this.items.Add(property.Name, property);
		}

		// Token: 0x06001141 RID: 4417 RVA: 0x0002E244 File Offset: 0x0002C444
		internal void Add(SettingsPropertyValueCollection vals)
		{
			foreach (object obj in vals)
			{
				SettingsPropertyValue property = (SettingsPropertyValue)obj;
				this.Add(property);
			}
		}

		// Token: 0x06001142 RID: 4418 RVA: 0x0002E2B0 File Offset: 0x0002C4B0
		public void Clear()
		{
			if (this.isReadOnly)
			{
				throw new NotSupportedException();
			}
			this.items.Clear();
		}

		// Token: 0x06001143 RID: 4419 RVA: 0x0002E2D0 File Offset: 0x0002C4D0
		public object Clone()
		{
			return new SettingsPropertyValueCollection
			{
				items = (Hashtable)this.items.Clone()
			};
		}

		// Token: 0x06001144 RID: 4420 RVA: 0x0002E2FC File Offset: 0x0002C4FC
		public void CopyTo(Array array, int index)
		{
			this.items.Values.CopyTo(array, index);
		}

		// Token: 0x06001145 RID: 4421 RVA: 0x0002E310 File Offset: 0x0002C510
		public IEnumerator GetEnumerator()
		{
			return this.items.Values.GetEnumerator();
		}

		// Token: 0x06001146 RID: 4422 RVA: 0x0002E324 File Offset: 0x0002C524
		public void Remove(string name)
		{
			if (this.isReadOnly)
			{
				throw new NotSupportedException();
			}
			this.items.Remove(name);
		}

		// Token: 0x06001147 RID: 4423 RVA: 0x0002E344 File Offset: 0x0002C544
		public void SetReadOnly()
		{
			this.isReadOnly = true;
		}

		// Token: 0x170003E8 RID: 1000
		// (get) Token: 0x06001148 RID: 4424 RVA: 0x0002E350 File Offset: 0x0002C550
		public int Count
		{
			get
			{
				return this.items.Count;
			}
		}

		// Token: 0x170003E9 RID: 1001
		// (get) Token: 0x06001149 RID: 4425 RVA: 0x0002E360 File Offset: 0x0002C560
		public bool IsSynchronized
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x170003EA RID: 1002
		public SettingsPropertyValue this[string name]
		{
			get
			{
				return (SettingsPropertyValue)this.items[name];
			}
		}

		// Token: 0x170003EB RID: 1003
		// (get) Token: 0x0600114B RID: 4427 RVA: 0x0002E37C File Offset: 0x0002C57C
		public object SyncRoot
		{
			get
			{
				throw new NotImplementedException();
			}
		}

		// Token: 0x040004EA RID: 1258
		private Hashtable items;

		// Token: 0x040004EB RID: 1259
		private bool isReadOnly;
	}
}
