﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x020001E8 RID: 488
	public class NameValueFileSectionHandler : IConfigurationSectionHandler
	{
		// Token: 0x060010DB RID: 4315 RVA: 0x0002D5EC File Offset: 0x0002B7EC
		public object Create(object parent, object configContext, XmlNode section)
		{
			XmlNode xmlNode = null;
			if (section.Attributes != null)
			{
				xmlNode = section.Attributes.RemoveNamedItem("file");
			}
			System.Collections.Specialized.NameValueCollection nameValueCollection = ConfigHelper.GetNameValueCollection(parent as System.Collections.Specialized.NameValueCollection, section, "key", "value");
			if (xmlNode != null && xmlNode.Value != string.Empty)
			{
				string path = ((IConfigXmlNode)section).Filename;
				path = Path.GetFullPath(path);
				string text = Path.Combine(Path.GetDirectoryName(path), xmlNode.Value);
				if (!File.Exists(text))
				{
					return nameValueCollection;
				}
				ConfigXmlDocument configXmlDocument = new ConfigXmlDocument();
				configXmlDocument.Load(text);
				if (configXmlDocument.DocumentElement.Name != section.Name)
				{
					throw new ConfigurationException("Invalid root element", configXmlDocument.DocumentElement);
				}
				nameValueCollection = ConfigHelper.GetNameValueCollection(nameValueCollection, configXmlDocument.DocumentElement, "key", "value");
			}
			return nameValueCollection;
		}
	}
}
