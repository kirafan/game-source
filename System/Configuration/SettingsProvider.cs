﻿using System;
using System.Configuration.Provider;

namespace System.Configuration
{
	// Token: 0x020001FC RID: 508
	public abstract class SettingsProvider : ProviderBase
	{
		// Token: 0x06001166 RID: 4454
		public abstract SettingsPropertyValueCollection GetPropertyValues(SettingsContext context, SettingsPropertyCollection collection);

		// Token: 0x06001167 RID: 4455
		public abstract void SetPropertyValues(SettingsContext context, SettingsPropertyValueCollection collection);

		// Token: 0x170003F5 RID: 1013
		// (get) Token: 0x06001168 RID: 4456
		// (set) Token: 0x06001169 RID: 4457
		public abstract string ApplicationName { get; set; }
	}
}
