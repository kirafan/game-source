﻿using System;

namespace System.Configuration
{
	// Token: 0x020001FE RID: 510
	public enum SettingsSerializeAs
	{
		// Token: 0x040004F7 RID: 1271
		String,
		// Token: 0x040004F8 RID: 1272
		Xml,
		// Token: 0x040004F9 RID: 1273
		Binary,
		// Token: 0x040004FA RID: 1274
		ProviderSpecific
	}
}
