﻿using System;

namespace System.Configuration
{
	// Token: 0x02000201 RID: 513
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
	public sealed class SettingsManageabilityAttribute : Attribute
	{
		// Token: 0x06001178 RID: 4472 RVA: 0x0002E9D0 File Offset: 0x0002CBD0
		public SettingsManageabilityAttribute(SettingsManageability manageability)
		{
			this.manageability = manageability;
		}

		// Token: 0x170003F9 RID: 1017
		// (get) Token: 0x06001179 RID: 4473 RVA: 0x0002E9E0 File Offset: 0x0002CBE0
		public SettingsManageability Manageability
		{
			get
			{
				return this.manageability;
			}
		}

		// Token: 0x040004FE RID: 1278
		private SettingsManageability manageability;
	}
}
