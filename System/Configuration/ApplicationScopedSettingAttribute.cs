﻿using System;

namespace System.Configuration
{
	// Token: 0x020001C6 RID: 454
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class ApplicationScopedSettingAttribute : SettingAttribute
	{
	}
}
