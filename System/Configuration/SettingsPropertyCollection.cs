﻿using System;
using System.Collections;

namespace System.Configuration
{
	// Token: 0x020001F3 RID: 499
	public class SettingsPropertyCollection : ICollection, IEnumerable, ICloneable
	{
		// Token: 0x06001111 RID: 4369 RVA: 0x0002DEC4 File Offset: 0x0002C0C4
		public SettingsPropertyCollection()
		{
			this.items = new Hashtable();
		}

		// Token: 0x06001112 RID: 4370 RVA: 0x0002DED8 File Offset: 0x0002C0D8
		public void Add(SettingsProperty property)
		{
			if (this.isReadOnly)
			{
				throw new NotSupportedException();
			}
			this.OnAdd(property);
			this.items.Add(property.Name, property);
			this.OnAddComplete(property);
		}

		// Token: 0x06001113 RID: 4371 RVA: 0x0002DF18 File Offset: 0x0002C118
		public void Clear()
		{
			if (this.isReadOnly)
			{
				throw new NotSupportedException();
			}
			this.OnClear();
			this.items.Clear();
			this.OnClearComplete();
		}

		// Token: 0x06001114 RID: 4372 RVA: 0x0002DF50 File Offset: 0x0002C150
		public object Clone()
		{
			return new SettingsPropertyCollection
			{
				items = (Hashtable)this.items.Clone()
			};
		}

		// Token: 0x06001115 RID: 4373 RVA: 0x0002DF7C File Offset: 0x0002C17C
		public void CopyTo(Array array, int index)
		{
			this.items.Values.CopyTo(array, index);
		}

		// Token: 0x06001116 RID: 4374 RVA: 0x0002DF90 File Offset: 0x0002C190
		public IEnumerator GetEnumerator()
		{
			return this.items.Values.GetEnumerator();
		}

		// Token: 0x06001117 RID: 4375 RVA: 0x0002DFA4 File Offset: 0x0002C1A4
		public void Remove(string name)
		{
			if (this.isReadOnly)
			{
				throw new NotSupportedException();
			}
			SettingsProperty property = (SettingsProperty)this.items[name];
			this.OnRemove(property);
			this.items.Remove(name);
			this.OnRemoveComplete(property);
		}

		// Token: 0x06001118 RID: 4376 RVA: 0x0002DFF0 File Offset: 0x0002C1F0
		public void SetReadOnly()
		{
			this.isReadOnly = true;
		}

		// Token: 0x06001119 RID: 4377 RVA: 0x0002DFFC File Offset: 0x0002C1FC
		protected virtual void OnAdd(SettingsProperty property)
		{
		}

		// Token: 0x0600111A RID: 4378 RVA: 0x0002E000 File Offset: 0x0002C200
		protected virtual void OnAddComplete(SettingsProperty property)
		{
		}

		// Token: 0x0600111B RID: 4379 RVA: 0x0002E004 File Offset: 0x0002C204
		protected virtual void OnClear()
		{
		}

		// Token: 0x0600111C RID: 4380 RVA: 0x0002E008 File Offset: 0x0002C208
		protected virtual void OnClearComplete()
		{
		}

		// Token: 0x0600111D RID: 4381 RVA: 0x0002E00C File Offset: 0x0002C20C
		protected virtual void OnRemove(SettingsProperty property)
		{
		}

		// Token: 0x0600111E RID: 4382 RVA: 0x0002E010 File Offset: 0x0002C210
		protected virtual void OnRemoveComplete(SettingsProperty property)
		{
		}

		// Token: 0x170003DB RID: 987
		// (get) Token: 0x0600111F RID: 4383 RVA: 0x0002E014 File Offset: 0x0002C214
		public int Count
		{
			get
			{
				return this.items.Count;
			}
		}

		// Token: 0x170003DC RID: 988
		// (get) Token: 0x06001120 RID: 4384 RVA: 0x0002E024 File Offset: 0x0002C224
		public bool IsSynchronized
		{
			get
			{
				return false;
			}
		}

		// Token: 0x170003DD RID: 989
		public SettingsProperty this[string name]
		{
			get
			{
				return (SettingsProperty)this.items[name];
			}
		}

		// Token: 0x170003DE RID: 990
		// (get) Token: 0x06001122 RID: 4386 RVA: 0x0002E03C File Offset: 0x0002C23C
		public object SyncRoot
		{
			get
			{
				return this;
			}
		}

		// Token: 0x040004DF RID: 1247
		private Hashtable items;

		// Token: 0x040004E0 RID: 1248
		private bool isReadOnly;
	}
}
