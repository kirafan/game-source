﻿using System;

namespace System.Configuration
{
	// Token: 0x020001EE RID: 494
	public sealed class SettingElement : ConfigurationElement
	{
		// Token: 0x060010F0 RID: 4336 RVA: 0x0002D84C File Offset: 0x0002BA4C
		public SettingElement()
		{
		}

		// Token: 0x060010F1 RID: 4337 RVA: 0x0002D854 File Offset: 0x0002BA54
		public SettingElement(string name, SettingsSerializeAs serializeAs)
		{
			this.Name = name;
			this.SerializeAs = serializeAs;
		}

		// Token: 0x060010F2 RID: 4338 RVA: 0x0002D86C File Offset: 0x0002BA6C
		static SettingElement()
		{
			SettingElement.properties = new ConfigurationPropertyCollection();
			SettingElement.properties.Add(SettingElement.name_prop);
			SettingElement.properties.Add(SettingElement.serialize_as_prop);
			SettingElement.properties.Add(SettingElement.value_prop);
		}

		// Token: 0x170003CF RID: 975
		// (get) Token: 0x060010F3 RID: 4339 RVA: 0x0002D908 File Offset: 0x0002BB08
		// (set) Token: 0x060010F4 RID: 4340 RVA: 0x0002D91C File Offset: 0x0002BB1C
		[ConfigurationProperty("name", DefaultValue = "", Options = (ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey))]
		public string Name
		{
			get
			{
				return (string)base[SettingElement.name_prop];
			}
			set
			{
				base[SettingElement.name_prop] = value;
			}
		}

		// Token: 0x170003D0 RID: 976
		// (get) Token: 0x060010F5 RID: 4341 RVA: 0x0002D92C File Offset: 0x0002BB2C
		// (set) Token: 0x060010F6 RID: 4342 RVA: 0x0002D940 File Offset: 0x0002BB40
		[ConfigurationProperty("value", DefaultValue = null, Options = ConfigurationPropertyOptions.IsRequired)]
		public SettingValueElement Value
		{
			get
			{
				return (SettingValueElement)base[SettingElement.value_prop];
			}
			set
			{
				base[SettingElement.value_prop] = value;
			}
		}

		// Token: 0x170003D1 RID: 977
		// (get) Token: 0x060010F7 RID: 4343 RVA: 0x0002D950 File Offset: 0x0002BB50
		// (set) Token: 0x060010F8 RID: 4344 RVA: 0x0002D984 File Offset: 0x0002BB84
		[ConfigurationProperty("serializeAs", DefaultValue = SettingsSerializeAs.String, Options = ConfigurationPropertyOptions.IsRequired)]
		public SettingsSerializeAs SerializeAs
		{
			get
			{
				return (SettingsSerializeAs)((base[SettingElement.serialize_as_prop] == null) ? 0 : ((int)base[SettingElement.serialize_as_prop]));
			}
			set
			{
				base[SettingElement.serialize_as_prop] = value;
			}
		}

		// Token: 0x170003D2 RID: 978
		// (get) Token: 0x060010F9 RID: 4345 RVA: 0x0002D998 File Offset: 0x0002BB98
		protected override ConfigurationPropertyCollection Properties
		{
			get
			{
				return SettingElement.properties;
			}
		}

		// Token: 0x060010FA RID: 4346 RVA: 0x0002D9A0 File Offset: 0x0002BBA0
		public override bool Equals(object o)
		{
			SettingElement settingElement = o as SettingElement;
			return settingElement != null && (settingElement.SerializeAs == this.SerializeAs && settingElement.Value == this.Value) && settingElement.Name == this.Name;
		}

		// Token: 0x060010FB RID: 4347 RVA: 0x0002D9F4 File Offset: 0x0002BBF4
		public override int GetHashCode()
		{
			int num = (int)(this.SerializeAs ^ (SettingsSerializeAs)127);
			if (this.Name != null)
			{
				num += (this.Name.GetHashCode() ^ 127);
			}
			if (this.Value != null)
			{
				num += this.Value.GetHashCode();
			}
			return num;
		}

		// Token: 0x040004D4 RID: 1236
		private static ConfigurationPropertyCollection properties;

		// Token: 0x040004D5 RID: 1237
		private static ConfigurationProperty name_prop = new ConfigurationProperty("name", typeof(string), string.Empty, ConfigurationPropertyOptions.IsRequired | ConfigurationPropertyOptions.IsKey);

		// Token: 0x040004D6 RID: 1238
		private static ConfigurationProperty serialize_as_prop = new ConfigurationProperty("serializeAs", typeof(SettingsSerializeAs), null, ConfigurationPropertyOptions.IsRequired);

		// Token: 0x040004D7 RID: 1239
		private static ConfigurationProperty value_prop = new ConfigurationProperty("value", typeof(SettingValueElement), null, ConfigurationPropertyOptions.IsRequired);
	}
}
