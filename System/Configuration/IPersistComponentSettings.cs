﻿using System;

namespace System.Configuration
{
	// Token: 0x020001E5 RID: 485
	public interface IPersistComponentSettings
	{
		// Token: 0x170003C4 RID: 964
		// (get) Token: 0x060010C9 RID: 4297
		// (set) Token: 0x060010CA RID: 4298
		bool SaveSettings { get; set; }

		// Token: 0x170003C5 RID: 965
		// (get) Token: 0x060010CB RID: 4299
		// (set) Token: 0x060010CC RID: 4300
		string SettingsKey { get; set; }

		// Token: 0x060010CD RID: 4301
		void LoadComponentSettings();

		// Token: 0x060010CE RID: 4302
		void ResetComponentSettings();

		// Token: 0x060010CF RID: 4303
		void SaveComponentSettings();
	}
}
