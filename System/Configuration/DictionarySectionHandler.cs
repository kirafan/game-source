﻿using System;
using System.Collections;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x020001DD RID: 477
	public class DictionarySectionHandler : IConfigurationSectionHandler
	{
		// Token: 0x060010AE RID: 4270 RVA: 0x0002D38C File Offset: 0x0002B58C
		public virtual object Create(object parent, object context, XmlNode section)
		{
			return ConfigHelper.GetDictionary(parent as IDictionary, section, this.KeyAttributeName, this.ValueAttributeName);
		}

		// Token: 0x170003BC RID: 956
		// (get) Token: 0x060010AF RID: 4271 RVA: 0x0002D3B4 File Offset: 0x0002B5B4
		protected virtual string KeyAttributeName
		{
			get
			{
				return "key";
			}
		}

		// Token: 0x170003BD RID: 957
		// (get) Token: 0x060010B0 RID: 4272 RVA: 0x0002D3BC File Offset: 0x0002B5BC
		protected virtual string ValueAttributeName
		{
			get
			{
				return "value";
			}
		}
	}
}
