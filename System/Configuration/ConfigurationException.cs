﻿using System;
using System.Runtime.Serialization;
using System.Xml;

namespace System.Configuration
{
	// Token: 0x020001CE RID: 462
	[Serializable]
	public class ConfigurationException : SystemException
	{
		// Token: 0x0600101E RID: 4126 RVA: 0x0002AB68 File Offset: 0x00028D68
		[Obsolete("This class is obsolete.  Use System.Configuration.ConfigurationErrorsException")]
		public ConfigurationException() : this(null)
		{
			this.filename = null;
			this.line = 0;
		}

		// Token: 0x0600101F RID: 4127 RVA: 0x0002AB80 File Offset: 0x00028D80
		[Obsolete("This class is obsolete.  Use System.Configuration.ConfigurationErrorsException")]
		public ConfigurationException(string message) : base(message)
		{
		}

		// Token: 0x06001020 RID: 4128 RVA: 0x0002AB8C File Offset: 0x00028D8C
		protected ConfigurationException(SerializationInfo info, StreamingContext context) : base(info, context)
		{
			this.filename = info.GetString("filename");
			this.line = info.GetInt32("line");
		}

		// Token: 0x06001021 RID: 4129 RVA: 0x0002ABC4 File Offset: 0x00028DC4
		[Obsolete("This class is obsolete.  Use System.Configuration.ConfigurationErrorsException")]
		public ConfigurationException(string message, Exception inner) : base(message, inner)
		{
		}

		// Token: 0x06001022 RID: 4130 RVA: 0x0002ABD0 File Offset: 0x00028DD0
		[Obsolete("This class is obsolete.  Use System.Configuration.ConfigurationErrorsException")]
		public ConfigurationException(string message, XmlNode node) : base(message)
		{
			this.filename = ConfigurationException.GetXmlNodeFilename(node);
			this.line = ConfigurationException.GetXmlNodeLineNumber(node);
		}

		// Token: 0x06001023 RID: 4131 RVA: 0x0002ABF4 File Offset: 0x00028DF4
		[Obsolete("This class is obsolete.  Use System.Configuration.ConfigurationErrorsException")]
		public ConfigurationException(string message, Exception inner, XmlNode node) : base(message, inner)
		{
			this.filename = ConfigurationException.GetXmlNodeFilename(node);
			this.line = ConfigurationException.GetXmlNodeLineNumber(node);
		}

		// Token: 0x06001024 RID: 4132 RVA: 0x0002AC24 File Offset: 0x00028E24
		[Obsolete("This class is obsolete.  Use System.Configuration.ConfigurationErrorsException")]
		public ConfigurationException(string message, string filename, int line) : base(message)
		{
			this.filename = filename;
			this.line = line;
		}

		// Token: 0x06001025 RID: 4133 RVA: 0x0002AC3C File Offset: 0x00028E3C
		[Obsolete("This class is obsolete.  Use System.Configuration.ConfigurationErrorsException")]
		public ConfigurationException(string message, Exception inner, string filename, int line) : base(message, inner)
		{
			this.filename = filename;
			this.line = line;
		}

		// Token: 0x17000394 RID: 916
		// (get) Token: 0x06001026 RID: 4134 RVA: 0x0002AC58 File Offset: 0x00028E58
		public virtual string BareMessage
		{
			get
			{
				return base.Message;
			}
		}

		// Token: 0x17000395 RID: 917
		// (get) Token: 0x06001027 RID: 4135 RVA: 0x0002AC60 File Offset: 0x00028E60
		public virtual string Filename
		{
			get
			{
				return this.filename;
			}
		}

		// Token: 0x17000396 RID: 918
		// (get) Token: 0x06001028 RID: 4136 RVA: 0x0002AC68 File Offset: 0x00028E68
		public virtual int Line
		{
			get
			{
				return this.line;
			}
		}

		// Token: 0x17000397 RID: 919
		// (get) Token: 0x06001029 RID: 4137 RVA: 0x0002AC70 File Offset: 0x00028E70
		public override string Message
		{
			get
			{
				string result;
				if (this.filename != null && this.filename.Length != 0)
				{
					if (this.line != 0)
					{
						result = string.Concat(new object[]
						{
							this.BareMessage,
							" (",
							this.filename,
							" line ",
							this.line,
							")"
						});
					}
					else
					{
						result = this.BareMessage + " (" + this.filename + ")";
					}
				}
				else if (this.line != 0)
				{
					result = string.Concat(new object[]
					{
						this.BareMessage,
						" (line ",
						this.line,
						")"
					});
				}
				else
				{
					result = this.BareMessage;
				}
				return result;
			}
		}

		// Token: 0x0600102A RID: 4138 RVA: 0x0002AD58 File Offset: 0x00028F58
		[Obsolete("This class is obsolete.  Use System.Configuration.ConfigurationErrorsException")]
		public static string GetXmlNodeFilename(XmlNode node)
		{
			if (!(node is IConfigXmlNode))
			{
				return string.Empty;
			}
			return ((IConfigXmlNode)node).Filename;
		}

		// Token: 0x0600102B RID: 4139 RVA: 0x0002AD78 File Offset: 0x00028F78
		[Obsolete("This class is obsolete.  Use System.Configuration.ConfigurationErrorsException")]
		public static int GetXmlNodeLineNumber(XmlNode node)
		{
			if (!(node is IConfigXmlNode))
			{
				return 0;
			}
			return ((IConfigXmlNode)node).LineNumber;
		}

		// Token: 0x0600102C RID: 4140 RVA: 0x0002AD94 File Offset: 0x00028F94
		public override void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			base.GetObjectData(info, context);
			info.AddValue("filename", this.filename);
			info.AddValue("line", this.line);
		}

		// Token: 0x0400047E RID: 1150
		private readonly string filename;

		// Token: 0x0400047F RID: 1151
		private readonly int line;
	}
}
