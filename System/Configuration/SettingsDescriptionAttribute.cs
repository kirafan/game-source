﻿using System;

namespace System.Configuration
{
	// Token: 0x02000203 RID: 515
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class SettingsDescriptionAttribute : Attribute
	{
		// Token: 0x0600117C RID: 4476 RVA: 0x0002EA00 File Offset: 0x0002CC00
		public SettingsDescriptionAttribute(string description)
		{
			this.desc = description;
		}

		// Token: 0x170003FB RID: 1019
		// (get) Token: 0x0600117D RID: 4477 RVA: 0x0002EA10 File Offset: 0x0002CC10
		public string Description
		{
			get
			{
				return this.desc;
			}
		}

		// Token: 0x04000500 RID: 1280
		private string desc;
	}
}
