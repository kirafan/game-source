﻿using System;

namespace System.Configuration
{
	// Token: 0x02000204 RID: 516
	[AttributeUsage(AttributeTargets.Class)]
	public sealed class SettingsGroupNameAttribute : Attribute
	{
		// Token: 0x0600117E RID: 4478 RVA: 0x0002EA18 File Offset: 0x0002CC18
		public SettingsGroupNameAttribute(string groupName)
		{
			this.group_name = groupName;
		}

		// Token: 0x170003FC RID: 1020
		// (get) Token: 0x0600117F RID: 4479 RVA: 0x0002EA28 File Offset: 0x0002CC28
		public string GroupName
		{
			get
			{
				return this.group_name;
			}
		}

		// Token: 0x04000501 RID: 1281
		private string group_name;
	}
}
