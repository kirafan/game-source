﻿using System;

namespace System.Configuration
{
	// Token: 0x020001FA RID: 506
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Property)]
	public sealed class SettingsProviderAttribute : Attribute
	{
		// Token: 0x0600115F RID: 4447 RVA: 0x0002E878 File Offset: 0x0002CA78
		public SettingsProviderAttribute(string providerTypeName)
		{
			if (providerTypeName == null)
			{
				throw new ArgumentNullException("providerTypeName");
			}
			this.providerTypeName = providerTypeName;
		}

		// Token: 0x06001160 RID: 4448 RVA: 0x0002E898 File Offset: 0x0002CA98
		public SettingsProviderAttribute(Type providerType)
		{
			if (providerType == null)
			{
				throw new ArgumentNullException("providerType");
			}
			this.providerTypeName = providerType.AssemblyQualifiedName;
		}

		// Token: 0x170003F3 RID: 1011
		// (get) Token: 0x06001161 RID: 4449 RVA: 0x0002E8C0 File Offset: 0x0002CAC0
		public string ProviderTypeName
		{
			get
			{
				return this.providerTypeName;
			}
		}

		// Token: 0x040004F4 RID: 1268
		private string providerTypeName;
	}
}
