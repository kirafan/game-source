﻿using System;

namespace System.Configuration
{
	// Token: 0x020001DE RID: 478
	public interface IApplicationSettingsProvider
	{
		// Token: 0x060010B1 RID: 4273
		SettingsPropertyValue GetPreviousVersion(SettingsContext context, SettingsProperty property);

		// Token: 0x060010B2 RID: 4274
		void Reset(SettingsContext context);

		// Token: 0x060010B3 RID: 4275
		void Upgrade(SettingsContext context, SettingsPropertyCollection properties);
	}
}
