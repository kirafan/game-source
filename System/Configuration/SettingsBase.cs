﻿using System;
using System.ComponentModel;

namespace System.Configuration
{
	// Token: 0x020001F0 RID: 496
	public abstract class SettingsBase
	{
		// Token: 0x060010FF RID: 4351 RVA: 0x0002DA6C File Offset: 0x0002BC6C
		public void Initialize(SettingsContext context, SettingsPropertyCollection properties, SettingsProviderCollection providers)
		{
			this.context = context;
			this.properties = properties;
			this.providers = providers;
		}

		// Token: 0x06001100 RID: 4352 RVA: 0x0002DA84 File Offset: 0x0002BC84
		public virtual void Save()
		{
			if (this.sync)
			{
				lock (this)
				{
					this.SaveCore();
				}
			}
			else
			{
				this.SaveCore();
			}
		}

		// Token: 0x06001101 RID: 4353 RVA: 0x0002DAE0 File Offset: 0x0002BCE0
		private void SaveCore()
		{
			foreach (object obj in this.Providers)
			{
				SettingsProvider settingsProvider = (SettingsProvider)obj;
				SettingsPropertyValueCollection settingsPropertyValueCollection = new SettingsPropertyValueCollection();
				foreach (object obj2 in this.PropertyValues)
				{
					SettingsPropertyValue settingsPropertyValue = (SettingsPropertyValue)obj2;
					if (settingsPropertyValue.Property.Provider == settingsProvider)
					{
						settingsPropertyValueCollection.Add(settingsPropertyValue);
					}
				}
				if (settingsPropertyValueCollection.Count > 0)
				{
					settingsProvider.SetPropertyValues(this.Context, settingsPropertyValueCollection);
				}
			}
		}

		// Token: 0x06001102 RID: 4354 RVA: 0x0002DBE0 File Offset: 0x0002BDE0
		public static SettingsBase Synchronized(SettingsBase settingsBase)
		{
			settingsBase.sync = true;
			return settingsBase;
		}

		// Token: 0x170003D3 RID: 979
		// (get) Token: 0x06001103 RID: 4355 RVA: 0x0002DBEC File Offset: 0x0002BDEC
		public virtual SettingsContext Context
		{
			get
			{
				return this.context;
			}
		}

		// Token: 0x170003D4 RID: 980
		// (get) Token: 0x06001104 RID: 4356 RVA: 0x0002DBF4 File Offset: 0x0002BDF4
		[System.ComponentModel.Browsable(false)]
		public bool IsSynchronized
		{
			get
			{
				return this.sync;
			}
		}

		// Token: 0x170003D5 RID: 981
		public virtual object this[string propertyName]
		{
			get
			{
				if (this.sync)
				{
					lock (this)
					{
						return this.GetPropertyValue(propertyName);
					}
				}
				return this.GetPropertyValue(propertyName);
			}
			set
			{
				if (this.sync)
				{
					lock (this)
					{
						this.SetPropertyValue(propertyName, value);
					}
				}
				else
				{
					this.SetPropertyValue(propertyName, value);
				}
			}
		}

		// Token: 0x170003D6 RID: 982
		// (get) Token: 0x06001107 RID: 4359 RVA: 0x0002DCBC File Offset: 0x0002BEBC
		public virtual SettingsPropertyCollection Properties
		{
			get
			{
				return this.properties;
			}
		}

		// Token: 0x170003D7 RID: 983
		// (get) Token: 0x06001108 RID: 4360 RVA: 0x0002DCC4 File Offset: 0x0002BEC4
		public virtual SettingsPropertyValueCollection PropertyValues
		{
			get
			{
				if (this.sync)
				{
					lock (this)
					{
						return this.values;
					}
				}
				return this.values;
			}
		}

		// Token: 0x170003D8 RID: 984
		// (get) Token: 0x06001109 RID: 4361 RVA: 0x0002DD20 File Offset: 0x0002BF20
		public virtual SettingsProviderCollection Providers
		{
			get
			{
				return this.providers;
			}
		}

		// Token: 0x0600110A RID: 4362 RVA: 0x0002DD28 File Offset: 0x0002BF28
		private object GetPropertyValue(string propertyName)
		{
			SettingsProperty settingsProperty;
			if (this.Properties == null || (settingsProperty = this.Properties[propertyName]) == null)
			{
				throw new SettingsPropertyNotFoundException(string.Format("The settings property '{0}' was not found", propertyName));
			}
			if (this.values[propertyName] == null)
			{
				foreach (object obj in settingsProperty.Provider.GetPropertyValues(this.Context, this.Properties))
				{
					SettingsPropertyValue property = (SettingsPropertyValue)obj;
					this.values.Add(property);
				}
			}
			return this.PropertyValues[propertyName].PropertyValue;
		}

		// Token: 0x0600110B RID: 4363 RVA: 0x0002DE00 File Offset: 0x0002C000
		private void SetPropertyValue(string propertyName, object value)
		{
			SettingsProperty settingsProperty;
			if (this.Properties == null || (settingsProperty = this.Properties[propertyName]) == null)
			{
				throw new SettingsPropertyNotFoundException(string.Format("The settings property '{0}' was not found", propertyName));
			}
			if (settingsProperty.IsReadOnly)
			{
				throw new SettingsPropertyIsReadOnlyException(string.Format("The settings property '{0}' is read only", propertyName));
			}
			if (settingsProperty.PropertyType != value.GetType())
			{
				throw new SettingsPropertyWrongTypeException(string.Format("The value supplied is of a type incompatible with the settings property '{0}'", propertyName));
			}
			this.PropertyValues[propertyName].PropertyValue = value;
		}

		// Token: 0x040004D8 RID: 1240
		private bool sync;

		// Token: 0x040004D9 RID: 1241
		private SettingsContext context;

		// Token: 0x040004DA RID: 1242
		private SettingsPropertyCollection properties;

		// Token: 0x040004DB RID: 1243
		private SettingsProviderCollection providers;

		// Token: 0x040004DC RID: 1244
		private SettingsPropertyValueCollection values = new SettingsPropertyValueCollection();
	}
}
