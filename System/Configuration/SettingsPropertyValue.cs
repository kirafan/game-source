﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;

namespace System.Configuration
{
	// Token: 0x020001F8 RID: 504
	public class SettingsPropertyValue
	{
		// Token: 0x0600114C RID: 4428 RVA: 0x0002E384 File Offset: 0x0002C584
		public SettingsPropertyValue(SettingsProperty property)
		{
			this.property = property;
			this.needPropertyValue = true;
		}

		// Token: 0x170003EC RID: 1004
		// (get) Token: 0x0600114D RID: 4429 RVA: 0x0002E39C File Offset: 0x0002C59C
		// (set) Token: 0x0600114E RID: 4430 RVA: 0x0002E3A4 File Offset: 0x0002C5A4
		public bool Deserialized
		{
			get
			{
				return this.deserialized;
			}
			set
			{
				this.deserialized = value;
			}
		}

		// Token: 0x170003ED RID: 1005
		// (get) Token: 0x0600114F RID: 4431 RVA: 0x0002E3B0 File Offset: 0x0002C5B0
		// (set) Token: 0x06001150 RID: 4432 RVA: 0x0002E3B8 File Offset: 0x0002C5B8
		public bool IsDirty
		{
			get
			{
				return this.dirty;
			}
			set
			{
				this.dirty = value;
			}
		}

		// Token: 0x170003EE RID: 1006
		// (get) Token: 0x06001151 RID: 4433 RVA: 0x0002E3C4 File Offset: 0x0002C5C4
		public string Name
		{
			get
			{
				return this.property.Name;
			}
		}

		// Token: 0x170003EF RID: 1007
		// (get) Token: 0x06001152 RID: 4434 RVA: 0x0002E3D4 File Offset: 0x0002C5D4
		public SettingsProperty Property
		{
			get
			{
				return this.property;
			}
		}

		// Token: 0x170003F0 RID: 1008
		// (get) Token: 0x06001153 RID: 4435 RVA: 0x0002E3DC File Offset: 0x0002C5DC
		// (set) Token: 0x06001154 RID: 4436 RVA: 0x0002E478 File Offset: 0x0002C678
		public object PropertyValue
		{
			get
			{
				if (this.needPropertyValue)
				{
					this.propertyValue = this.GetDeserializedValue(this.serializedValue);
					if (this.propertyValue == null)
					{
						this.propertyValue = this.GetDeserializedDefaultValue();
						this.defaulted = true;
					}
					this.needPropertyValue = false;
				}
				if (this.propertyValue != null && !(this.propertyValue is string) && !(this.propertyValue is DateTime) && !this.property.PropertyType.IsPrimitive)
				{
					this.dirty = true;
				}
				return this.propertyValue;
			}
			set
			{
				this.propertyValue = value;
				this.dirty = true;
				this.needPropertyValue = false;
				this.needSerializedValue = true;
				this.defaulted = false;
			}
		}

		// Token: 0x170003F1 RID: 1009
		// (get) Token: 0x06001155 RID: 4437 RVA: 0x0002E4A0 File Offset: 0x0002C6A0
		// (set) Token: 0x06001156 RID: 4438 RVA: 0x0002E5B0 File Offset: 0x0002C7B0
		public object SerializedValue
		{
			get
			{
				if (this.needSerializedValue)
				{
					this.needSerializedValue = false;
					switch (this.property.SerializeAs)
					{
					case SettingsSerializeAs.String:
						this.serializedValue = System.ComponentModel.TypeDescriptor.GetConverter(this.property.PropertyType).ConvertToInvariantString(this.propertyValue);
						break;
					case SettingsSerializeAs.Xml:
						if (this.propertyValue != null)
						{
							XmlSerializer xmlSerializer = new XmlSerializer(this.propertyValue.GetType());
							StringWriter stringWriter = new StringWriter(CultureInfo.InvariantCulture);
							xmlSerializer.Serialize(stringWriter, this.propertyValue);
							this.serializedValue = stringWriter.ToString();
						}
						else
						{
							this.serializedValue = null;
						}
						break;
					case SettingsSerializeAs.Binary:
						if (this.propertyValue != null)
						{
							BinaryFormatter binaryFormatter = new BinaryFormatter();
							MemoryStream memoryStream = new MemoryStream();
							binaryFormatter.Serialize(memoryStream, this.propertyValue);
							this.serializedValue = memoryStream.ToArray();
						}
						else
						{
							this.serializedValue = null;
						}
						break;
					default:
						this.serializedValue = null;
						break;
					}
				}
				return this.serializedValue;
			}
			set
			{
				this.serializedValue = value;
				this.needPropertyValue = true;
			}
		}

		// Token: 0x170003F2 RID: 1010
		// (get) Token: 0x06001157 RID: 4439 RVA: 0x0002E5C0 File Offset: 0x0002C7C0
		public bool UsingDefaultValue
		{
			get
			{
				return this.defaulted;
			}
		}

		// Token: 0x06001158 RID: 4440 RVA: 0x0002E5C8 File Offset: 0x0002C7C8
		internal object Reset()
		{
			this.propertyValue = this.GetDeserializedDefaultValue();
			this.dirty = true;
			this.defaulted = true;
			this.needPropertyValue = true;
			return this.propertyValue;
		}

		// Token: 0x06001159 RID: 4441 RVA: 0x0002E5F4 File Offset: 0x0002C7F4
		private object GetDeserializedDefaultValue()
		{
			if (this.property.DefaultValue == null)
			{
				if (this.property.PropertyType.IsValueType)
				{
					return Activator.CreateInstance(this.property.PropertyType);
				}
				return null;
			}
			else if (this.property.DefaultValue is string && ((string)this.property.DefaultValue).Length == 0)
			{
				if (this.property.PropertyType != typeof(string))
				{
					return Activator.CreateInstance(this.property.PropertyType);
				}
				return string.Empty;
			}
			else
			{
				if (this.property.DefaultValue is string && ((string)this.property.DefaultValue).Length > 0)
				{
					return this.GetDeserializedValue(this.property.DefaultValue);
				}
				if (!this.property.PropertyType.IsAssignableFrom(this.property.DefaultValue.GetType()))
				{
					System.ComponentModel.TypeConverter converter = System.ComponentModel.TypeDescriptor.GetConverter(this.property.PropertyType);
					return converter.ConvertFrom(null, CultureInfo.InvariantCulture, this.property.DefaultValue);
				}
				return this.property.DefaultValue;
			}
		}

		// Token: 0x0600115A RID: 4442 RVA: 0x0002E734 File Offset: 0x0002C934
		private object GetDeserializedValue(object serializedValue)
		{
			if (serializedValue == null)
			{
				return null;
			}
			object result = null;
			try
			{
				switch (this.property.SerializeAs)
				{
				case SettingsSerializeAs.String:
					if (serializedValue is string)
					{
						result = System.ComponentModel.TypeDescriptor.GetConverter(this.property.PropertyType).ConvertFromInvariantString((string)serializedValue);
					}
					break;
				case SettingsSerializeAs.Xml:
				{
					XmlSerializer xmlSerializer = new XmlSerializer(this.property.PropertyType);
					StringReader reader = new StringReader((string)serializedValue);
					result = xmlSerializer.Deserialize(XmlReader.Create(reader));
					break;
				}
				case SettingsSerializeAs.Binary:
				{
					BinaryFormatter binaryFormatter = new BinaryFormatter();
					MemoryStream serializationStream;
					if (serializedValue is string)
					{
						serializationStream = new MemoryStream(Convert.FromBase64String((string)serializedValue));
					}
					else
					{
						serializationStream = new MemoryStream((byte[])serializedValue);
					}
					result = binaryFormatter.Deserialize(serializationStream);
					break;
				}
				}
			}
			catch (Exception ex)
			{
				if (this.property.ThrowOnErrorDeserializing)
				{
					throw ex;
				}
			}
			return result;
		}

		// Token: 0x040004EC RID: 1260
		private readonly SettingsProperty property;

		// Token: 0x040004ED RID: 1261
		private object propertyValue;

		// Token: 0x040004EE RID: 1262
		private object serializedValue;

		// Token: 0x040004EF RID: 1263
		private bool needSerializedValue;

		// Token: 0x040004F0 RID: 1264
		private bool needPropertyValue;

		// Token: 0x040004F1 RID: 1265
		private bool dirty;

		// Token: 0x040004F2 RID: 1266
		private bool defaulted;

		// Token: 0x040004F3 RID: 1267
		private bool deserialized;
	}
}
