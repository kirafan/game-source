﻿using System;

namespace System.Configuration
{
	// Token: 0x020001E6 RID: 486
	public interface ISettingsProviderService
	{
		// Token: 0x060010D0 RID: 4304
		SettingsProvider GetSettingsProvider(SettingsProperty property);
	}
}
