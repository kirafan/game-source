﻿using System;

namespace System.Configuration
{
	// Token: 0x020001DC RID: 476
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class DefaultSettingValueAttribute : Attribute
	{
		// Token: 0x060010AB RID: 4267 RVA: 0x0002D36C File Offset: 0x0002B56C
		public DefaultSettingValueAttribute(string value)
		{
			this.value = value;
		}

		// Token: 0x170003BB RID: 955
		// (get) Token: 0x060010AC RID: 4268 RVA: 0x0002D37C File Offset: 0x0002B57C
		public string Value
		{
			get
			{
				return this.value;
			}
		}

		// Token: 0x040004CA RID: 1226
		private string value;
	}
}
