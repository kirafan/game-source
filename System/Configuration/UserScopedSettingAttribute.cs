﻿using System;

namespace System.Configuration
{
	// Token: 0x0200020A RID: 522
	[AttributeUsage(AttributeTargets.Property)]
	public sealed class UserScopedSettingAttribute : SettingAttribute
	{
	}
}
