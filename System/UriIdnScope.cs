﻿using System;

namespace System
{
	// Token: 0x020004B7 RID: 1207
	public enum UriIdnScope
	{
		// Token: 0x04001B75 RID: 7029
		None,
		// Token: 0x04001B76 RID: 7030
		AllExceptIntranet,
		// Token: 0x04001B77 RID: 7031
		All
	}
}
