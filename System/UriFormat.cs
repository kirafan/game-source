﻿using System;

namespace System
{
	// Token: 0x020004B4 RID: 1204
	public enum UriFormat
	{
		// Token: 0x04001B6B RID: 7019
		UriEscaped = 1,
		// Token: 0x04001B6C RID: 7020
		Unescaped,
		// Token: 0x04001B6D RID: 7021
		SafeUnescaped
	}
}
