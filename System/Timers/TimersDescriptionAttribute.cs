﻿using System;
using System.ComponentModel;

namespace System.Timers
{
	// Token: 0x020004AF RID: 1199
	[AttributeUsage(AttributeTargets.All)]
	public class TimersDescriptionAttribute : System.ComponentModel.DescriptionAttribute
	{
		// Token: 0x06002B0E RID: 11022 RVA: 0x00093EAC File Offset: 0x000920AC
		public TimersDescriptionAttribute(string description) : base(description)
		{
		}

		// Token: 0x17000BD0 RID: 3024
		// (get) Token: 0x06002B0F RID: 11023 RVA: 0x00093EB8 File Offset: 0x000920B8
		public override string Description
		{
			get
			{
				return base.Description;
			}
		}
	}
}
