﻿using System;

namespace System.Timers
{
	// Token: 0x02000520 RID: 1312
	// (Invoke) Token: 0x06002D2C RID: 11564
	public delegate void ElapsedEventHandler(object sender, ElapsedEventArgs e);
}
